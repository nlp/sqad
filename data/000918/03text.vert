<s>
Zlín	Zlín	k1gInSc1	Zlín
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Zlin	Zlin	k1gNnSc1	Zlin
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1949	[number]	k4	1949
<g/>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
Gottwaldov	Gottwaldov	k1gInSc1	Gottwaldov
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
statutární	statutární	k2eAgMnSc1d1	statutární
město	město	k1gNnSc4	město
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
řeky	řeka	k1gFnSc2	řeka
Dřevnice	Dřevnice	k1gFnSc2	Dřevnice
na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
Hostýnských	hostýnský	k2eAgInPc2d1	hostýnský
a	a	k8xC	a
Vizovických	vizovický	k2eAgInPc2d1	vizovický
vrchů	vrch	k1gInPc2	vrch
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
centrem	centrum	k1gNnSc7	centrum
Zlínského	zlínský	k2eAgInSc2d1	zlínský
kraje	kraj	k1gInSc2	kraj
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
75	[number]	k4	75
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
osadě	osada	k1gFnSc6	osada
Zlín	Zlín	k1gInSc1	Zlín
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1322	[number]	k4	1322
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jej	on	k3xPp3gNnSc4	on
koupila	koupit	k5eAaPmAgFnS	koupit
královna	královna	k1gFnSc1	královna
Eliška	Eliška	k1gFnSc1	Eliška
Rejčka	Rejčka	k1gFnSc1	Rejčka
a	a	k8xC	a
věnovala	věnovat	k5eAaPmAgFnS	věnovat
brněnskému	brněnský	k2eAgInSc3d1	brněnský
klášteru	klášter	k1gInSc3	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Zlín	Zlín	k1gInSc1	Zlín
byl	být	k5eAaImAgInS	být
řemeslnicko-cechovním	řemeslnickoechovní	k2eAgNnSc7d1	řemeslnicko-cechovní
střediskem	středisko	k1gNnSc7	středisko
okolního	okolní	k2eAgNnSc2d1	okolní
valašského	valašský	k2eAgNnSc2d1	Valašské
osídlení	osídlení	k1gNnSc2	osídlení
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgNnPc4d1	Městské
práva	právo	k1gNnPc4	právo
dostal	dostat	k5eAaPmAgMnS	dostat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1397	[number]	k4	1397
-	-	kIx~	-
patřilo	patřit	k5eAaImAgNnS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gNnSc4	on
právo	právo	k1gNnSc4	právo
pořádat	pořádat	k5eAaImF	pořádat
trhy	trh	k1gInPc4	trh
<g/>
,	,	kIx,	,
vařit	vařit	k5eAaImF	vařit
pivo	pivo	k1gNnSc4	pivo
<g/>
,	,	kIx,	,
i	i	k8xC	i
právo	právo	k1gNnSc1	právo
hrdelní	hrdelní	k2eAgNnSc1d1	hrdelní
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Zlína	Zlín	k1gInSc2	Zlín
účastnili	účastnit	k5eAaImAgMnP	účastnit
valašského	valašský	k2eAgNnSc2d1	Valašské
protihabsburského	protihabsburský	k2eAgNnSc2d1	protihabsburské
povstání	povstání	k1gNnSc2	povstání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1622	[number]	k4	1622
byl	být	k5eAaImAgInS	být
vypálen	vypálen	k2eAgInSc1d1	vypálen
zlínský	zlínský	k2eAgInSc1d1	zlínský
zámek	zámek	k1gInSc1	zámek
a	a	k8xC	a
okolní	okolní	k2eAgNnSc1d1	okolní
stavení	stavení	k1gNnSc1	stavení
a	a	k8xC	a
statky	statek	k1gInPc1	statek
zplundrovány	zplundrován	k2eAgInPc1d1	zplundrován
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
nacházelo	nacházet	k5eAaImAgNnS	nacházet
na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
tří	tři	k4xCgFnPc2	tři
moravských	moravský	k2eAgFnPc2d1	Moravská
národopisných	národopisný	k2eAgFnPc2d1	národopisná
oblastí	oblast	k1gFnPc2	oblast
Valašska	Valašsko	k1gNnSc2	Valašsko
<g/>
,	,	kIx,	,
Slovácka	Slovácko	k1gNnSc2	Slovácko
a	a	k8xC	a
Hané	Haná	k1gFnSc2	Haná
<g/>
.	.	kIx.	.
</s>
<s>
Nejblíže	blízce	k6eAd3	blízce
mělo	mít	k5eAaImAgNnS	mít
svým	svůj	k3xOyFgInSc7	svůj
charakterem	charakter	k1gInSc7	charakter
k	k	k7c3	k
Valašsku	Valašsko	k1gNnSc3	Valašsko
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Zlín	Zlín	k1gInSc1	Zlín
příliš	příliš	k6eAd1	příliš
nelišil	lišit	k5eNaImAgInS	lišit
od	od	k7c2	od
jiných	jiný	k2eAgFnPc2d1	jiná
malých	malá	k1gFnPc2	malá
valašských	valašský	k2eAgNnPc2d1	Valašské
středisek	středisko	k1gNnPc2	středisko
<g/>
,	,	kIx,	,
např.	např.	kA	např.
sousedního	sousední	k2eAgNnSc2d1	sousední
města	město	k1gNnSc2	město
Vizovice	Vizovice	k1gFnPc1	Vizovice
<g/>
.	.	kIx.	.
</s>
<s>
Zlín	Zlín	k1gInSc1	Zlín
byl	být	k5eAaImAgInS	být
sídlem	sídlo	k1gNnSc7	sídlo
panství	panství	k1gNnSc1	panství
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
panské	panský	k2eAgNnSc1d1	panské
sídlo	sídlo	k1gNnSc1	sídlo
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
v	v	k7c6	v
tehdy	tehdy	k6eAd1	tehdy
samostatném	samostatný	k2eAgNnSc6d1	samostatné
městečku	městečko	k1gNnSc6	městečko
Malenovice	Malenovice	k1gFnSc2	Malenovice
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
městskou	městský	k2eAgFnSc7d1	městská
částí	část	k1gFnSc7	část
Zlína	Zlín	k1gInSc2	Zlín
<g/>
.	.	kIx.	.
</s>
<s>
Zlínské	zlínský	k2eAgNnSc1d1	zlínské
panství	panství	k1gNnSc1	panství
vystřídalo	vystřídat	k5eAaPmAgNnS	vystřídat
řadu	řada	k1gFnSc4	řada
majitelů	majitel	k1gMnPc2	majitel
-	-	kIx~	-
k	k	k7c3	k
těm	ten	k3xDgMnPc3	ten
známějším	známý	k2eAgMnPc3d2	známější
patří	patřit	k5eAaImIp3nS	patřit
Šternberkové	Šternberková	k1gFnPc4	Šternberková
v	v	k7c6	v
době	doba	k1gFnSc6	doba
předhusitské	předhusitský	k2eAgFnSc6d1	předhusitská
<g/>
,	,	kIx,	,
Tetourové	Tetourový	k2eAgFnSc6d1	Tetourová
v	v	k7c4	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
Rottalové	Rottal	k1gMnPc1	Rottal
po	po	k7c6	po
třicetileté	třicetiletý	k2eAgFnSc6d1	třicetiletá
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Szerenyové	Szerenyové	k2eAgFnPc1d1	Szerenyové
a	a	k8xC	a
Brettonové	Brettonový	k2eAgFnPc1d1	Brettonový
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgMnSc7d1	poslední
vlastníkem	vlastník	k1gMnSc7	vlastník
zlínského	zlínský	k2eAgInSc2d1	zlínský
zámku	zámek	k1gInSc2	zámek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
centrem	centrum	k1gNnSc7	centrum
panství	panství	k1gNnPc2	panství
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
brněnský	brněnský	k2eAgMnSc1d1	brněnský
továrník	továrník	k1gMnSc1	továrník
Leopold	Leopold	k1gMnSc1	Leopold
Haupt	Haupt	k1gMnSc1	Haupt
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterého	který	k3yIgNnSc2	který
jej	on	k3xPp3gInSc4	on
odkoupilo	odkoupit	k5eAaPmAgNnS	odkoupit
město	město	k1gNnSc1	město
Zlín	Zlín	k1gInSc1	Zlín
v	v	k7c6	v
r.	r.	kA	r.
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zřízení	zřízení	k1gNnSc6	zřízení
okresů	okres	k1gInPc2	okres
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
byl	být	k5eAaImAgInS	být
Zlín	Zlín	k1gInSc1	Zlín
přičleněn	přičlenit	k5eAaPmNgInS	přičlenit
k	k	k7c3	k
soudnímu	soudní	k2eAgInSc3d1	soudní
okresu	okres	k1gInSc3	okres
Napajedla	napajedlo	k1gNnSc2	napajedlo
a	a	k8xC	a
politickému	politický	k2eAgInSc3d1	politický
okresu	okres	k1gInSc3	okres
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnSc1	obyvatel
nepřesáhl	přesáhnout	k5eNaPmAgMnS	přesáhnout
3000	[number]	k4	3000
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
další	další	k2eAgInSc4d1	další
rozvoj	rozvoj	k1gInSc4	rozvoj
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
důležitý	důležitý	k2eAgInSc4d1	důležitý
rok	rok	k1gInSc4	rok
1894	[number]	k4	1894
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zde	zde	k6eAd1	zde
založil	založit	k5eAaPmAgMnS	založit
svou	svůj	k3xOyFgFnSc4	svůj
obuvnickou	obuvnický	k2eAgFnSc4d1	obuvnická
firmu	firma	k1gFnSc4	firma
podnikatel	podnikatel	k1gMnSc1	podnikatel
Tomáš	Tomáš	k1gMnSc1	Tomáš
Baťa	Baťa	k1gMnSc1	Baťa
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
následujících	následující	k2eAgNnPc2d1	následující
25	[number]	k4	25
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
během	během	k7c2	během
nichž	jenž	k3xRgFnPc2	jenž
několikrát	několikrát	k6eAd1	několikrát
zkrachoval	zkrachovat	k5eAaPmAgInS	zkrachovat
<g/>
)	)	kIx)	)
vybudoval	vybudovat	k5eAaPmAgInS	vybudovat
prosperující	prosperující	k2eAgFnSc4d1	prosperující
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
rostoucí	rostoucí	k2eAgInSc4d1	rostoucí
podnik	podnik	k1gInSc4	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
skutečnosti	skutečnost	k1gFnSc3	skutečnost
se	se	k3xPyFc4	se
do	do	k7c2	do
města	město	k1gNnSc2	město
přistěhovalo	přistěhovat	k5eAaPmAgNnS	přistěhovat
hodně	hodně	k6eAd1	hodně
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
především	především	k9	především
z	z	k7c2	z
chudého	chudý	k2eAgNnSc2d1	chudé
Valašska	Valašsko	k1gNnSc2	Valašsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ze	z	k7c2	z
Slovácka	Slovácko	k1gNnSc2	Slovácko
<g/>
,	,	kIx,	,
Hané	Haná	k1gFnSc2	Haná
a	a	k8xC	a
vzdálenějších	vzdálený	k2eAgFnPc2d2	vzdálenější
oblastí	oblast	k1gFnPc2	oblast
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
Zlína	Zlín	k1gInSc2	Zlín
zároveň	zároveň	k6eAd1	zároveň
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
pozvolnému	pozvolný	k2eAgNnSc3d1	pozvolné
ukončení	ukončení	k1gNnSc3	ukončení
masivního	masivní	k2eAgNnSc2d1	masivní
valašského	valašský	k2eAgNnSc2d1	Valašské
vystěhovalectví	vystěhovalectví	k1gNnSc2	vystěhovalectví
do	do	k7c2	do
Texasu	Texas	k1gInSc2	Texas
<g/>
.	.	kIx.	.
</s>
<s>
Baťově	Baťův	k2eAgFnSc3d1	Baťova
továrně	továrna	k1gFnSc3	továrna
pomohla	pomoct	k5eAaPmAgFnS	pomoct
I.	I.	kA	I.
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
důležitým	důležitý	k2eAgMnSc7d1	důležitý
dodavatelem	dodavatel	k1gMnSc7	dodavatel
obuvi	obuv	k1gFnSc2	obuv
pro	pro	k7c4	pro
rakouskou	rakouský	k2eAgFnSc4d1	rakouská
armádu	armáda	k1gFnSc4	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Vítězně	vítězně	k6eAd1	vítězně
se	se	k3xPyFc4	se
popral	poprat	k5eAaPmAgInS	poprat
s	s	k7c7	s
hospodářskými	hospodářský	k2eAgFnPc7d1	hospodářská
krizemi	krize	k1gFnPc7	krize
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1922	[number]	k4	1922
<g/>
-	-	kIx~	-
<g/>
23	[number]	k4	23
a	a	k8xC	a
1929	[number]	k4	1929
<g/>
-	-	kIx~	-
<g/>
33	[number]	k4	33
a	a	k8xC	a
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
se	se	k3xPyFc4	se
v	v	k7c4	v
největšího	veliký	k2eAgMnSc4d3	veliký
světového	světový	k2eAgMnSc4d1	světový
výrobce	výrobce	k1gMnSc4	výrobce
obuvi	obuv	k1gFnSc2	obuv
se	s	k7c7	s
závody	závod	k1gInPc7	závod
a	a	k8xC	a
prodejnami	prodejna	k1gFnPc7	prodejna
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
desítkách	desítka	k1gFnPc6	desítka
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1910	[number]	k4	1910
<g/>
-	-	kIx~	-
<g/>
1938	[number]	k4	1938
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
z	z	k7c2	z
12	[number]	k4	12
912	[number]	k4	912
na	na	k7c4	na
34	[number]	k4	34
348	[number]	k4	348
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
se	se	k3xPyFc4	se
Zlín	Zlín	k1gInSc1	Zlín
stal	stát	k5eAaPmAgInS	stát
moderním	moderní	k2eAgInSc7d1	moderní
městským	městský	k2eAgInSc7d1	městský
centrem	centr	k1gInSc7	centr
<g/>
.	.	kIx.	.
</s>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Baťa	Baťa	k1gMnSc1	Baťa
pozval	pozvat	k5eAaPmAgMnS	pozvat
do	do	k7c2	do
Zlína	Zlín	k1gInSc2	Zlín
mnoho	mnoho	k4c4	mnoho
renomovaných	renomovaný	k2eAgMnPc2d1	renomovaný
architektů	architekt	k1gMnPc2	architekt
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Le	Le	k1gMnSc2	Le
Corbusiera	Corbusier	k1gMnSc2	Corbusier
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
změnilo	změnit	k5eAaPmAgNnS	změnit
v	v	k7c6	v
aglomeraci	aglomerace	k1gFnSc6	aglomerace
plnou	plný	k2eAgFnSc4d1	plná
funkcionalistické	funkcionalistický	k2eAgFnPc4d1	funkcionalistická
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Rostly	růst	k5eAaImAgInP	růst
kolonie	kolonie	k1gFnPc4	kolonie
typických	typický	k2eAgFnPc2d1	typická
"	"	kIx"	"
<g/>
baťovských	baťovský	k2eAgFnPc2d1	baťovská
<g/>
"	"	kIx"	"
staveb	stavba	k1gFnPc2	stavba
-	-	kIx~	-
rodinných	rodinný	k2eAgInPc2d1	rodinný
domků	domek	k1gInPc2	domek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
výškových	výškový	k2eAgInPc2d1	výškový
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Nejproslulejší	proslulý	k2eAgFnSc7d3	nejproslulejší
stavbou	stavba	k1gFnSc7	stavba
z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
77,5	[number]	k4	77,5
m	m	kA	m
vysoký	vysoký	k2eAgInSc4d1	vysoký
mrakodrap	mrakodrap	k1gInSc4	mrakodrap
"	"	kIx"	"
<g/>
Jednadvacítka	Jednadvacítko	k1gNnSc2	Jednadvacítko
<g/>
"	"	kIx"	"
-	-	kIx~	-
tehdejší	tehdejší	k2eAgNnSc1d1	tehdejší
sídlo	sídlo	k1gNnSc1	sídlo
ředitelství	ředitelství	k1gNnSc2	ředitelství
firmy	firma	k1gFnSc2	firma
Baťa	Baťa	k1gMnSc1	Baťa
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
budova	budova	k1gFnSc1	budova
byla	být	k5eAaImAgFnS	být
vystavěna	vystavět	k5eAaPmNgFnS	vystavět
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1937	[number]	k4	1937
<g/>
-	-	kIx~	-
<g/>
38	[number]	k4	38
podle	podle	k7c2	podle
projektu	projekt	k1gInSc2	projekt
architekta	architekt	k1gMnSc2	architekt
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Karfíka	Karfík	k1gMnSc2	Karfík
a	a	k8xC	a
kromě	kromě	k7c2	kromě
mnoha	mnoho	k4c2	mnoho
pokročilých	pokročilý	k2eAgNnPc2d1	pokročilé
technických	technický	k2eAgNnPc2d1	technické
řešení	řešení	k1gNnPc2	řešení
(	(	kIx(	(
<g/>
šestnáctipatrový	šestnáctipatrový	k2eAgInSc1d1	šestnáctipatrový
železobetonový	železobetonový	k2eAgInSc1d1	železobetonový
skelet	skelet	k1gInSc4	skelet
postavený	postavený	k2eAgInSc4d1	postavený
za	za	k7c4	za
160	[number]	k4	160
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
centrální	centrální	k2eAgFnSc2d1	centrální
klimatizace	klimatizace	k1gFnSc2	klimatizace
s	s	k7c7	s
ovládáním	ovládání	k1gNnSc7	ovládání
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
kancelářích	kancelář	k1gFnPc6	kancelář
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
hlavně	hlavně	k6eAd1	hlavně
"	"	kIx"	"
<g/>
pojízdnou	pojízdný	k2eAgFnSc7d1	pojízdná
<g/>
"	"	kIx"	"
ředitelskou	ředitelský	k2eAgFnSc7d1	ředitelská
kanceláří	kancelář	k1gFnSc7	kancelář
v	v	k7c6	v
jednom	jeden	k4xCgMnSc6	jeden
z	z	k7c2	z
výtahů	výtah	k1gInPc2	výtah
(	(	kIx(	(
<g/>
s	s	k7c7	s
telefonem	telefon	k1gInSc7	telefon
a	a	k8xC	a
tekoucí	tekoucí	k2eAgFnSc7d1	tekoucí
vodou	voda	k1gFnSc7	voda
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
vzniku	vznik	k1gInSc2	vznik
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
druhá	druhý	k4xOgFnSc1	druhý
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
necírkevní	církevní	k2eNgFnSc1d1	necírkevní
stavba	stavba	k1gFnSc1	stavba
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
tehdy	tehdy	k6eAd1	tehdy
několikrát	několikrát	k6eAd1	několikrát
navštívil	navštívit	k5eAaPmAgMnS	navštívit
i	i	k9	i
prezident	prezident	k1gMnSc1	prezident
Československa	Československo	k1gNnSc2	Československo
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigu	k1gFnSc2	Garrigu
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Baťa	Baťa	k1gMnSc1	Baťa
zastával	zastávat	k5eAaImAgMnS	zastávat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
při	při	k7c6	při
letecké	letecký	k2eAgFnSc6d1	letecká
havárii	havárie	k1gFnSc6	havárie
v	v	k7c6	v
r.	r.	kA	r.
1932	[number]	k4	1932
úřad	úřad	k1gInSc4	úřad
starosty	starosta	k1gMnSc2	starosta
Zlína	Zlín	k1gInSc2	Zlín
a	a	k8xC	a
cílevědomě	cílevědomě	k6eAd1	cílevědomě
budoval	budovat	k5eAaImAgMnS	budovat
moderní	moderní	k2eAgNnSc4d1	moderní
město	město	k1gNnSc4	město
"	"	kIx"	"
<g/>
pro	pro	k7c4	pro
50	[number]	k4	50
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Neorientoval	orientovat	k5eNaBmAgInS	orientovat
se	se	k3xPyFc4	se
jen	jen	k9	jen
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
továrnu	továrna	k1gFnSc4	továrna
a	a	k8xC	a
bydlení	bydlení	k1gNnSc4	bydlení
pro	pro	k7c4	pro
dělníky	dělník	k1gMnPc4	dělník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c4	na
obchodní	obchodní	k2eAgFnSc4d1	obchodní
a	a	k8xC	a
dopravní	dopravní	k2eAgFnSc4d1	dopravní
síť	síť	k1gFnSc4	síť
<g/>
,	,	kIx,	,
služby	služba	k1gFnPc4	služba
<g/>
,	,	kIx,	,
kulturní	kulturní	k2eAgNnSc4d1	kulturní
a	a	k8xC	a
sportovní	sportovní	k2eAgNnSc4d1	sportovní
vyžití	vyžití	k1gNnSc4	vyžití
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgFnPc1d1	mnohá
budovy	budova	k1gFnPc1	budova
tehdy	tehdy	k6eAd1	tehdy
postavené	postavený	k2eAgFnPc4d1	postavená
se	se	k3xPyFc4	se
pyšnily	pyšnit	k5eAaImAgFnP	pyšnit
přívlastkem	přívlastek	k1gInSc7	přívlastek
"	"	kIx"	"
<g/>
nej	nej	k?	nej
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
např.	např.	kA	např.
největší	veliký	k2eAgInSc1d3	veliký
obchodní	obchodní	k2eAgInSc1d1	obchodní
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInSc1d3	veliký
hotel	hotel	k1gInSc1	hotel
<g/>
,	,	kIx,	,
největší	veliký	k2eAgNnSc4d3	veliký
kino	kino	k1gNnSc4	kino
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
2	[number]	k4	2
500	[number]	k4	500
sedících	sedící	k2eAgMnPc2d1	sedící
diváků	divák	k1gMnPc2	divák
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
všechno	všechen	k3xTgNnSc1	všechen
bráno	brán	k2eAgNnSc1d1	bráno
"	"	kIx"	"
<g/>
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
Frankfurtu	Frankfurt	k1gInSc2	Frankfurt
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
obdivuhodné	obdivuhodný	k2eAgNnSc1d1	obdivuhodné
<g/>
,	,	kIx,	,
že	že	k8xS	že
takovéto	takovýto	k3xDgFnPc1	takovýto
budovy	budova	k1gFnPc1	budova
vznikaly	vznikat	k5eAaImAgFnP	vznikat
v	v	k7c6	v
relativně	relativně	k6eAd1	relativně
malém	malý	k2eAgNnSc6d1	malé
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
a	a	k8xC	a
ne	ne	k9	ne
v	v	k7c6	v
metropolích	metropol	k1gFnPc6	metropol
jako	jako	k8xS	jako
Vídeň	Vídeň	k1gFnSc4	Vídeň
či	či	k8xC	či
Berlín	Berlín	k1gInSc4	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Zlín	Zlín	k1gInSc1	Zlín
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
"	"	kIx"	"
<g/>
architektonický	architektonický	k2eAgInSc4d1	architektonický
zázrak	zázrak	k1gInSc4	zázrak
<g/>
"	"	kIx"	"
a	a	k8xC	a
živou	živý	k2eAgFnSc4d1	živá
učebnici	učebnice	k1gFnSc4	učebnice
funkcionalismu	funkcionalismus	k1gInSc2	funkcionalismus
<g/>
.	.	kIx.	.
</s>
<s>
Bohužel	bohužel	k9	bohužel
<g/>
,	,	kIx,	,
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgInPc2d1	další
plánů	plán	k1gInPc2	plán
překazila	překazit	k5eAaPmAgFnS	překazit
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
dostavbu	dostavba	k1gFnSc4	dostavba
centrálního	centrální	k2eAgNnSc2d1	centrální
zlínského	zlínský	k2eAgNnSc2d1	zlínské
Náměstí	náměstí	k1gNnSc2	náměstí
Práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
železnici	železnice	k1gFnSc6	železnice
Praha	Praha	k1gFnSc1	Praha
<g/>
–	–	k?	–
<g/>
Zlín	Zlín	k1gInSc4	Zlín
<g/>
–	–	k?	–
<g/>
Košice	Košice	k1gInPc4	Košice
nebo	nebo	k8xC	nebo
dálnici	dálnice	k1gFnSc4	dálnice
Praha	Praha	k1gFnSc1	Praha
<g/>
–	–	k?	–
<g/>
Zlín	Zlín	k1gInSc1	Zlín
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yQgFnSc3	který
na	na	k7c6	na
Zlínsku	Zlínsko	k1gNnSc6	Zlínsko
existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc4	několik
torz	torzo	k1gNnPc2	torzo
mostů	most	k1gInPc2	most
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1935	[number]	k4	1935
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
podle	podle	k7c2	podle
vládního	vládní	k2eAgNnSc2d1	vládní
nařízení	nařízení	k1gNnSc2	nařízení
č.	č.	k?	č.
104	[number]	k4	104
<g/>
/	/	kIx~	/
<g/>
1935	[number]	k4	1935
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
kterým	který	k3yIgNnSc7	který
se	se	k3xPyFc4	se
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
České	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
Moravskoslezské	moravskoslezský	k2eAgFnPc1d1	Moravskoslezská
provádějí	provádět	k5eAaImIp3nP	provádět
změny	změna	k1gFnPc1	změna
obvodů	obvod	k1gInPc2	obvod
některých	některý	k3yIgInPc2	některý
okresních	okresní	k2eAgInPc2d1	okresní
úřadů	úřad	k1gInPc2	úřad
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
nový	nový	k2eAgInSc1d1	nový
politický	politický	k2eAgInSc1d1	politický
okres	okres	k1gInSc1	okres
Zlín	Zlín	k1gInSc1	Zlín
a	a	k8xC	a
město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
okresním	okresní	k2eAgInSc7d1	okresní
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
bombardováno	bombardovat	k5eAaImNgNnS	bombardovat
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
byly	být	k5eAaImAgInP	být
Baťovy	Baťův	k2eAgInPc1d1	Baťův
závody	závod	k1gInPc1	závod
znárodněny	znárodněn	k2eAgInPc1d1	znárodněn
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1949	[number]	k4	1949
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
připojováním	připojování	k1gNnSc7	připojování
dalších	další	k2eAgFnPc2d1	další
obcí	obec	k1gFnPc2	obec
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c4	na
Gottwaldov	Gottwaldov	k1gInSc4	Gottwaldov
podle	podle	k7c2	podle
prvního	první	k4xOgMnSc2	první
komunistického	komunistický	k2eAgMnSc2d1	komunistický
prezidenta	prezident	k1gMnSc2	prezident
Československa	Československo	k1gNnSc2	Československo
Klementa	Klement	k1gMnSc2	Klement
Gottwalda	Gottwald	k1gMnSc2	Gottwald
<g/>
,	,	kIx,	,
centrální	centrální	k2eAgFnSc1d1	centrální
městská	městský	k2eAgFnSc1d1	městská
čtvrť	čtvrť	k1gFnSc1	čtvrť
si	se	k3xPyFc3	se
však	však	k9	však
ponechala	ponechat	k5eAaPmAgFnS	ponechat
název	název	k1gInSc4	název
Zlín	Zlín	k1gInSc1	Zlín
(	(	kIx(	(
<g/>
plným	plný	k2eAgNnSc7d1	plné
označením	označení	k1gNnSc7	označení
Gottwaldov	Gottwaldov	k1gInSc1	Gottwaldov
I-Zlín	I-Zlín	k1gInSc1	I-Zlín
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tentýž	týž	k3xTgInSc4	týž
rok	rok	k1gInSc4	rok
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
stalo	stát	k5eAaPmAgNnS	stát
sídlem	sídlo	k1gNnSc7	sídlo
Gottwaldovského	gottwaldovský	k2eAgInSc2d1	gottwaldovský
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ale	ale	k9	ale
neměl	mít	k5eNaImAgInS	mít
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
trvání	trvání	k1gNnSc2	trvání
a	a	k8xC	a
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
při	při	k7c6	při
další	další	k2eAgFnSc6d1	další
reformě	reforma	k1gFnSc6	reforma
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
byl	být	k5eAaImAgInS	být
okres	okres	k1gInSc1	okres
Gottwaldov	Gottwaldov	k1gInSc1	Gottwaldov
začleněn	začleněn	k2eAgInSc1d1	začleněn
do	do	k7c2	do
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úspěšné	úspěšný	k2eAgFnSc6d1	úspěšná
činnosti	činnost	k1gFnSc6	činnost
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
instituce	instituce	k1gFnPc1	instituce
s	s	k7c7	s
baťovskou	baťovský	k2eAgFnSc7d1	baťovská
historií	historie	k1gFnSc7	historie
-	-	kIx~	-
například	například	k6eAd1	například
zlínské	zlínský	k2eAgInPc4d1	zlínský
filmové	filmový	k2eAgInPc4d1	filmový
ateliéry	ateliér	k1gInPc4	ateliér
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
proslavili	proslavit	k5eAaPmAgMnP	proslavit
svými	svůj	k3xOyFgNnPc7	svůj
animovanými	animovaný	k2eAgNnPc7d1	animované
díly	dílo	k1gNnPc7	dílo
především	především	k9	především
Karel	Karel	k1gMnSc1	Karel
Zeman	Zeman	k1gMnSc1	Zeman
a	a	k8xC	a
Hermína	Hermína	k1gFnSc1	Hermína
Týrlová	Týrlová	k1gFnSc1	Týrlová
<g/>
.	.	kIx.	.
</s>
<s>
Jiným	jiný	k2eAgInSc7d1	jiný
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
českého	český	k2eAgInSc2d1	český
průmyslového	průmyslový	k2eAgInSc2d1	průmyslový
designu	design	k1gInSc2	design
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Kovář	Kovář	k1gMnSc1	Kovář
(	(	kIx(	(
<g/>
ergonomické	ergonomický	k2eAgInPc1d1	ergonomický
nástroje	nástroj	k1gInPc1	nástroj
a	a	k8xC	a
stroje	stroj	k1gInPc1	stroj
<g/>
,	,	kIx,	,
automobily	automobil	k1gInPc1	automobil
Tatra	Tatra	k1gFnSc1	Tatra
603	[number]	k4	603
a	a	k8xC	a
Tatra	Tatra	k1gFnSc1	Tatra
138	[number]	k4	138
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zlín	Zlín	k1gInSc1	Zlín
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
také	také	k9	také
domovem	domov	k1gInSc7	domov
cestovatelské	cestovatelský	k2eAgFnSc2d1	cestovatelská
dvojice	dvojice	k1gFnSc2	dvojice
Zikmund	Zikmund	k1gMnSc1	Zikmund
<g/>
-	-	kIx~	-
<g/>
Hanzelka	Hanzelka	k1gMnSc1	Hanzelka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
začala	začít	k5eAaPmAgFnS	začít
výstavba	výstavba	k1gFnSc1	výstavba
největšího	veliký	k2eAgNnSc2d3	veliký
zlínského	zlínský	k2eAgNnSc2d1	zlínské
sídliště	sídliště	k1gNnSc2	sídliště
Jižní	jižní	k2eAgInPc4d1	jižní
Svahy	svah	k1gInPc4	svah
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
až	až	k9	až
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
zde	zde	k6eAd1	zde
žije	žít	k5eAaImIp3nS	žít
přes	přes	k7c4	přes
25	[number]	k4	25
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Baťovy	Baťův	k2eAgInPc1d1	Baťův
závody	závod	k1gInPc1	závod
pokračovaly	pokračovat	k5eAaImAgInP	pokračovat
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
obuvi	obuv	k1gFnSc2	obuv
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Svit	svit	k1gInSc1	svit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byly	být	k5eAaImAgInP	být
koncernovým	koncernový	k2eAgInSc7d1	koncernový
podnikem	podnik	k1gInSc7	podnik
<g/>
,	,	kIx,	,
pod	pod	k7c4	pod
který	který	k3yQgInSc4	který
spadaly	spadat	k5eAaImAgInP	spadat
i	i	k9	i
závody	závod	k1gInPc1	závod
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
<g/>
,	,	kIx,	,
Zruči	Zruč	k1gInSc6	Zruč
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
a	a	k8xC	a
Botana	Botana	k1gFnSc1	Botana
Skuteč	Skuteč	k1gFnSc1	Skuteč
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
tehdy	tehdy	k6eAd1	tehdy
v	v	k7c6	v
koncernu	koncern	k1gInSc6	koncern
pracovalo	pracovat	k5eAaImAgNnS	pracovat
přes	přes	k7c4	přes
20	[number]	k4	20
000	[number]	k4	000
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
a	a	k8xC	a
vyrobili	vyrobit	k5eAaPmAgMnP	vyrobit
přes	přes	k7c4	přes
20	[number]	k4	20
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
párů	pár	k1gInPc2	pár
obuvi	obuv	k1gFnSc2	obuv
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
produkce	produkce	k1gFnSc2	produkce
mířila	mířit	k5eAaImAgFnS	mířit
na	na	k7c4	na
export	export	k1gInSc4	export
do	do	k7c2	do
zemí	zem	k1gFnPc2	zem
RVHP	RVHP	kA	RVHP
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
do	do	k7c2	do
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
strojní	strojní	k2eAgFnSc2d1	strojní
výroby	výroba	k1gFnSc2	výroba
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
v	v	k7c6	v
ZPS	ZPS	kA	ZPS
(	(	kIx(	(
<g/>
Závodech	závod	k1gInPc6	závod
přesného	přesný	k2eAgNnSc2d1	přesné
strojírenství	strojírenství	k1gNnSc2	strojírenství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
dobré	dobrý	k2eAgNnSc4d1	dobré
jméno	jméno	k1gNnSc4	jméno
především	především	k9	především
v	v	k7c6	v
obráběcích	obráběcí	k2eAgInPc6d1	obráběcí
strojích	stroj	k1gInPc6	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Výrobu	výroba	k1gFnSc4	výroba
pneumatik	pneumatika	k1gFnPc2	pneumatika
provozoval	provozovat	k5eAaImAgInS	provozovat
národní	národní	k2eAgInSc1d1	národní
podnik	podnik	k1gInSc1	podnik
Rudý	rudý	k2eAgInSc4d1	rudý
řijen	řijen	k1gInSc4	řijen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
r.	r.	kA	r.
1975	[number]	k4	1975
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
nedalekých	daleký	k2eNgFnPc6d1	nedaleká
Otrokovicích	Otrokovice	k1gFnPc6	Otrokovice
dokončena	dokončen	k2eAgFnSc1d1	dokončena
výstavba	výstavba	k1gFnSc1	výstavba
největší	veliký	k2eAgFnSc2d3	veliký
pneumatikárny	pneumatikárna	k1gFnSc2	pneumatikárna
v	v	k7c6	v
socialistických	socialistický	k2eAgFnPc6d1	socialistická
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
podnik	podnik	k1gInSc1	podnik
vrátil	vrátit	k5eAaPmAgInS	vrátit
k	k	k7c3	k
původnímu	původní	k2eAgInSc3d1	původní
názvu	název	k1gInSc3	název
BARUM	barum	k1gInSc4	barum
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
BAta	BAta	k1gFnSc1	BAta
RUbber	RUbber	k1gMnSc1	RUbber
Manufacturing	Manufacturing	k1gInSc1	Manufacturing
<g/>
)	)	kIx)	)
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
koncernu	koncern	k1gInSc2	koncern
Continental	Continental	k1gMnSc1	Continental
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
téměř	téměř	k6eAd1	téměř
90	[number]	k4	90
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1989	[number]	k4	1989
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Gottwaldov	Gottwaldov	k1gInSc4	Gottwaldov
Tomáš	Tomáš	k1gMnSc1	Tomáš
Baťa	Baťa	k1gMnSc1	Baťa
mladší	mladý	k2eAgMnSc1d2	mladší
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c4	o
navrácení	navrácení	k1gNnSc4	navrácení
původního	původní	k2eAgInSc2d1	původní
názvu	název	k1gInSc2	název
Zlín	Zlín	k1gInSc1	Zlín
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yQnSc3	což
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1990	[number]	k4	1990
je	být	k5eAaImIp3nS	být
Zlín	Zlín	k1gInSc1	Zlín
statutárním	statutární	k2eAgNnSc7d1	statutární
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
docházelo	docházet	k5eAaImAgNnS	docházet
ve	v	k7c6	v
městě	město	k1gNnSc6	město
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
soukromého	soukromý	k2eAgNnSc2d1	soukromé
podnikání	podnikání	k1gNnSc2	podnikání
(	(	kIx(	(
<g/>
zlínský	zlínský	k2eAgInSc1d1	zlínský
region	region	k1gInSc1	region
tehdy	tehdy	k6eAd1	tehdy
měl	mít	k5eAaImAgInS	mít
nejvíce	hodně	k6eAd3	hodně
podnikatelů	podnikatel	k1gMnPc2	podnikatel
na	na	k7c4	na
počet	počet	k1gInSc4	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
také	také	k6eAd1	také
zanikly	zaniknout	k5eAaPmAgInP	zaniknout
některé	některý	k3yIgInPc1	některý
významné	významný	k2eAgInPc1d1	významný
průmyslové	průmyslový	k2eAgInPc1d1	průmyslový
závody	závod	k1gInPc1	závod
(	(	kIx(	(
<g/>
Svit	svit	k1gInSc1	svit
<g/>
,	,	kIx,	,
ZPS	ZPS	kA	ZPS
<g/>
)	)	kIx)	)
i	i	k8xC	i
řada	řada	k1gFnSc1	řada
menších	malý	k2eAgInPc2d2	menší
podniků	podnik	k1gInPc2	podnik
(	(	kIx(	(
<g/>
mlékárna	mlékárna	k1gFnSc1	mlékárna
Lacrum	lacrum	k1gInSc1	lacrum
<g/>
,	,	kIx,	,
jatky	jatka	k1gFnPc1	jatka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
dříve	dříve	k6eAd2	dříve
centrem	centrum	k1gNnSc7	centrum
obuvnického	obuvnický	k2eAgInSc2d1	obuvnický
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
výrobou	výroba	k1gFnSc7	výroba
obuvi	obuv	k1gFnSc2	obuv
zabývá	zabývat	k5eAaImIp3nS	zabývat
jen	jen	k9	jen
hrstka	hrstka	k1gFnSc1	hrstka
firem	firma	k1gFnPc2	firma
s	s	k7c7	s
několika	několik	k4yIc7	několik
stovkami	stovka	k1gFnPc7	stovka
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Významně	významně	k6eAd1	významně
negativní	negativní	k2eAgInSc4d1	negativní
vliv	vliv	k1gInSc4	vliv
mělo	mít	k5eAaImAgNnS	mít
také	také	k9	také
rozdělení	rozdělení	k1gNnSc1	rozdělení
Československa	Československo	k1gNnSc2	Československo
<g/>
:	:	kIx,	:
Zlínsko	Zlínsko	k1gNnSc1	Zlínsko
se	se	k3xPyFc4	se
z	z	k7c2	z
centrálního	centrální	k2eAgInSc2d1	centrální
regionu	region	k1gInSc2	region
stalo	stát	k5eAaPmAgNnS	stát
příhraniční	příhraniční	k2eAgNnSc1d1	příhraniční
oblastí	oblast	k1gFnSc7	oblast
<g/>
,	,	kIx,	,
obchod	obchod	k1gInSc1	obchod
a	a	k8xC	a
doprava	doprava	k6eAd1	doprava
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
východ	východ	k1gInSc4	východ
prudce	prudko	k6eAd1	prudko
poklesly	poklesnout	k5eAaPmAgFnP	poklesnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
regionu	region	k1gInSc6	region
prudce	prudko	k6eAd1	prudko
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
však	však	k9	však
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
pod	pod	k7c7	pod
celostátním	celostátní	k2eAgInSc7d1	celostátní
průměrem	průměr	k1gInSc7	průměr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
napojeno	napojit	k5eAaPmNgNnS	napojit
na	na	k7c4	na
dálniční	dálniční	k2eAgFnSc4d1	dálniční
síť	síť	k1gFnSc4	síť
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
rychlostní	rychlostní	k2eAgFnSc2d1	rychlostní
silnice	silnice	k1gFnSc2	silnice
R55	R55	k1gFnSc2	R55
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
jako	jako	k8xS	jako
dálnice	dálnice	k1gFnSc1	dálnice
D	D	kA	D
<g/>
55	[number]	k4	55
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
Univerzita	univerzita	k1gFnSc1	univerzita
Tomáše	Tomáš	k1gMnSc2	Tomáš
Bati	Baťa	k1gMnSc2	Baťa
(	(	kIx(	(
<g/>
má	mít	k5eAaImIp3nS	mít
přes	přes	k7c4	přes
10	[number]	k4	10
000	[number]	k4	000
studentů	student	k1gMnPc2	student
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
zaměstnavatelů	zaměstnavatel	k1gMnPc2	zaměstnavatel
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
)	)	kIx)	)
a	a	k8xC	a
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
samosprávný	samosprávný	k2eAgInSc1d1	samosprávný
Zlínský	zlínský	k2eAgInSc1d1	zlínský
kraj	kraj	k1gInSc1	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
se	se	k3xPyFc4	se
od	od	k7c2	od
města	město	k1gNnSc2	město
osamostatnilo	osamostatnit	k5eAaPmAgNnS	osamostatnit
několik	několik	k4yIc1	několik
bývalých	bývalý	k2eAgFnPc2d1	bývalá
částí	část	k1gFnPc2	část
Zlína	Zlín	k1gInSc2	Zlín
(	(	kIx(	(
<g/>
Lhota	Lhota	k1gFnSc1	Lhota
<g/>
,	,	kIx,	,
Lípa	lípa	k1gFnSc1	lípa
<g/>
,	,	kIx,	,
Tečovice	Tečovice	k1gFnSc1	Tečovice
<g/>
,	,	kIx,	,
Karlovice	Karlovice	k1gFnPc1	Karlovice
<g/>
,	,	kIx,	,
Březnice	Březnice	k1gFnSc1	Březnice
a	a	k8xC	a
Ostrata	Ostrata	k1gFnSc1	Ostrata
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc6	leden
2009	[number]	k4	2009
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
na	na	k7c6	na
základě	základ	k1gInSc6	základ
referenda	referendum	k1gNnSc2	referendum
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2008	[number]	k4	2008
osamostatnily	osamostatnit	k5eAaPmAgFnP	osamostatnit
Želechovice	Želechovice	k1gFnPc1	Želechovice
nad	nad	k7c7	nad
Dřevnicí	Dřevnice	k1gFnSc7	Dřevnice
<g/>
.	.	kIx.	.
</s>
<s>
Sílily	sílit	k5eAaImAgFnP	sílit
požadavky	požadavek	k1gInPc4	požadavek
na	na	k7c4	na
zřízení	zřízení	k1gNnSc4	zřízení
osadních	osadní	k2eAgInPc2d1	osadní
výborů	výbor	k1gInPc2	výbor
či	či	k8xC	či
přímo	přímo	k6eAd1	přímo
samosprávných	samosprávný	k2eAgFnPc2d1	samosprávná
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
v	v	k7c6	v
okrajových	okrajový	k2eAgFnPc6d1	okrajová
částech	část	k1gFnPc6	část
Zlína	Zlín	k1gInSc2	Zlín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
do	do	k7c2	do
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
dostalo	dostat	k5eAaPmAgNnS	dostat
hnutí	hnutí	k1gNnSc1	hnutí
M.	M.	kA	M.
O.	O.	kA	O.
R.	R.	kA	R.
(	(	kIx(	(
<g/>
Morální	morální	k2eAgFnSc4d1	morální
očistu	očista	k1gFnSc4	očista
radnice	radnice	k1gFnSc2	radnice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
vznik	vznik	k1gInSc4	vznik
samosprávných	samosprávný	k2eAgFnPc2d1	samosprávná
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
v	v	k7c6	v
Přílukách	Příluek	k1gInPc6	Příluek
<g/>
,	,	kIx,	,
Malenovicích	Malenovice	k1gFnPc6	Malenovice
<g/>
,	,	kIx,	,
Vršavě	Vršava	k1gFnSc3	Vršava
<g/>
,	,	kIx,	,
Klečůvce	Klečůvka	k1gFnSc6	Klečůvka
či	či	k8xC	či
v	v	k7c6	v
Lužkovicích	Lužkovice	k1gFnPc6	Lužkovice
jako	jako	k8xC	jako
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
klíčových	klíčový	k2eAgInPc2d1	klíčový
bodů	bod	k1gInPc2	bod
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
volebním	volební	k2eAgInSc6d1	volební
programu	program	k1gInSc6	program
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
neprosadilo	prosadit	k5eNaPmAgNnS	prosadit
tento	tento	k3xDgInSc4	tento
záměr	záměr	k1gInSc4	záměr
u	u	k7c2	u
svých	svůj	k3xOyFgMnPc2	svůj
koaličních	koaliční	k2eAgMnPc2d1	koaliční
partnerů	partner	k1gMnPc2	partner
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
do	do	k7c2	do
programového	programový	k2eAgNnSc2d1	programové
prohlášení	prohlášení	k1gNnSc2	prohlášení
rady	rada	k1gFnSc2	rada
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
již	již	k6eAd1	již
jen	jen	k9	jen
neurčité	určitý	k2eNgNnSc1d1	neurčité
posílení	posílení	k1gNnSc1	posílení
pravomocí	pravomoc	k1gFnPc2	pravomoc
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
žádným	žádný	k3yNgFnPc3	žádný
změnám	změna	k1gFnPc3	změna
<g/>
,	,	kIx,	,
v	v	k7c6	v
zastupitelstvu	zastupitelstvo	k1gNnSc6	zastupitelstvo
nakonec	nakonec	k6eAd1	nakonec
neprošel	projít	k5eNaPmAgInS	projít
ani	ani	k8xC	ani
kompromisní	kompromisní	k2eAgInSc1d1	kompromisní
návrh	návrh	k1gInSc1	návrh
na	na	k7c4	na
posílení	posílení	k1gNnSc4	posílení
pravomocí	pravomoc	k1gFnPc2	pravomoc
komisí	komise	k1gFnPc2	komise
místních	místní	k2eAgFnPc2d1	místní
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Hnutí	hnutí	k1gNnSc1	hnutí
M.	M.	kA	M.
O.	O.	kA	O.
R.	R.	kA	R.
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
tohoto	tento	k3xDgInSc2	tento
neúspěchu	neúspěch	k1gInSc2	neúspěch
rozpadlo	rozpadnout	k5eAaPmAgNnS	rozpadnout
a	a	k8xC	a
část	část	k1gFnSc4	část
jeho	jeho	k3xOp3gMnPc2	jeho
politiků	politik	k1gMnPc2	politik
odešla	odejít	k5eAaPmAgFnS	odejít
z	z	k7c2	z
hnutí	hnutí	k1gNnSc2	hnutí
i	i	k9	i
z	z	k7c2	z
rady	rada	k1gFnSc2	rada
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
prosazovali	prosazovat	k5eAaImAgMnP	prosazovat
například	například	k6eAd1	například
náměstek	náměstek	k1gMnSc1	náměstek
primátora	primátor	k1gMnSc2	primátor
Bedřich	Bedřich	k1gMnSc1	Bedřich
Landsfeld	Landsfeld	k1gMnSc1	Landsfeld
či	či	k8xC	či
Eva	Eva	k1gFnSc1	Eva
Štauderová	Štauderová	k1gFnSc1	Štauderová
a	a	k8xC	a
Pavel	Pavel	k1gMnSc1	Pavel
Jungmann	Jungmann	k1gMnSc1	Jungmann
<g/>
,	,	kIx,	,
odpůrcem	odpůrce	k1gMnSc7	odpůrce
byl	být	k5eAaImAgMnS	být
například	například	k6eAd1	například
primátor	primátor	k1gMnSc1	primátor
Miroslav	Miroslav	k1gMnSc1	Miroslav
Adámek	Adámek	k1gMnSc1	Adámek
(	(	kIx(	(
<g/>
nestraník	nestraník	k1gMnSc1	nestraník
za	za	k7c4	za
TOP09	TOP09	k1gFnSc4	TOP09
a	a	k8xC	a
STAN	stan	k1gInSc4	stan
<g/>
)	)	kIx)	)
či	či	k8xC	či
opoziční	opoziční	k2eAgMnSc1d1	opoziční
zastupitel	zastupitel	k1gMnSc1	zastupitel
Martin	Martin	k1gMnSc1	Martin
Mikeska	Mikeska	k1gFnSc1	Mikeska
z	z	k7c2	z
ČSSD	ČSSD	kA	ČSSD
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
uváděn	uvádět	k5eAaImNgInS	uvádět
za	za	k7c4	za
Zlín	Zlín	k1gInSc4	Zlín
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
Gottwaldov	Gottwaldov	k1gInSc1	Gottwaldov
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
výsledků	výsledek	k1gInPc2	výsledek
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
včetně	včetně	k7c2	včetně
místních	místní	k2eAgFnPc2d1	místní
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
v	v	k7c6	v
konkrétní	konkrétní	k2eAgFnSc6d1	konkrétní
době	doba	k1gFnSc6	doba
patřily	patřit	k5eAaImAgInP	patřit
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
patrné	patrný	k2eAgNnSc1d1	patrné
<g/>
,	,	kIx,	,
že	že	k8xS	že
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
jiných	jiný	k2eAgNnPc6d1	jiné
velkých	velký	k2eAgNnPc6d1	velké
městech	město	k1gNnPc6	město
Česka	Česko	k1gNnSc2	Česko
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
zlínské	zlínský	k2eAgFnSc6d1	zlínská
aglomeraci	aglomerace	k1gFnSc6	aglomerace
nicméně	nicméně	k8xC	nicméně
žije	žít	k5eAaImIp3nS	žít
přes	přes	k7c4	přes
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
Zlín	Zlín	k1gInSc4	Zlín
sídlem	sídlo	k1gNnSc7	sídlo
Zlínského	zlínský	k2eAgInSc2d1	zlínský
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zejména	zejména	k9	zejména
významným	významný	k2eAgNnSc7d1	významné
průmyslovým	průmyslový	k2eAgNnSc7d1	průmyslové
<g/>
,	,	kIx,	,
obchodním	obchodní	k2eAgNnSc7d1	obchodní
a	a	k8xC	a
kulturním	kulturní	k2eAgNnSc7d1	kulturní
centrem	centrum	k1gNnSc7	centrum
východní	východní	k2eAgFnSc2d1	východní
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
soustředěno	soustředit	k5eAaPmNgNnS	soustředit
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
středních	střední	k2eAgFnPc2d1	střední
škol	škola	k1gFnPc2	škola
a	a	k8xC	a
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
tři	tři	k4xCgFnPc4	tři
nemocnice	nemocnice	k1gFnPc1	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
krajskému	krajský	k2eAgNnSc3d1	krajské
postavení	postavení	k1gNnSc3	postavení
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
kromě	kromě	k7c2	kromě
okresního	okresní	k2eAgInSc2d1	okresní
soudu	soud	k1gInSc2	soud
i	i	k8xC	i
pobočka	pobočka	k1gFnSc1	pobočka
brněnského	brněnský	k2eAgInSc2d1	brněnský
krajského	krajský	k2eAgInSc2d1	krajský
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Zlín	Zlín	k1gInSc1	Zlín
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
prezentoval	prezentovat	k5eAaBmAgMnS	prezentovat
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
výstavy	výstava	k1gFnSc2	výstava
ZLíNY	Zlín	k1gInPc1	Zlín
(	(	kIx(	(
<g/>
Zlín	Zlín	k1gInSc1	Zlín
in	in	k?	in
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
této	tento	k3xDgFnSc3	tento
výstavě	výstava	k1gFnSc3	výstava
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
i	i	k9	i
propagační	propagační	k2eAgInSc1d1	propagační
film	film	k1gInSc1	film
Zlína	Zlín	k1gInSc2	Zlín
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
mapoval	mapovat	k5eAaImAgMnS	mapovat
jeho	jeho	k3xOp3gFnSc4	jeho
současnost	současnost	k1gFnSc4	současnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2008	[number]	k4	2008
bylo	být	k5eAaImAgNnS	být
dostavěno	dostavět	k5eAaPmNgNnS	dostavět
na	na	k7c6	na
zlínském	zlínský	k2eAgNnSc6d1	zlínské
náměstí	náměstí	k1gNnSc6	náměstí
Míru	mír	k1gInSc2	mír
obchodní	obchodní	k2eAgMnSc1d1	obchodní
a	a	k8xC	a
zábavní	zábavní	k2eAgNnSc1d1	zábavní
centrum	centrum	k1gNnSc1	centrum
Zlaté	zlatý	k2eAgNnSc1d1	Zlaté
jablko	jablko	k1gNnSc1	jablko
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
bývalé	bývalý	k2eAgFnSc2d1	bývalá
historické	historický	k2eAgFnSc2d1	historická
budovy	budova	k1gFnSc2	budova
Záložny	záložna	k1gFnSc2	záložna
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
tohoto	tento	k3xDgNnSc2	tento
centra	centrum	k1gNnSc2	centrum
byla	být	k5eAaImAgFnS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
podmínek	podmínka	k1gFnPc2	podmínka
zachování	zachování	k1gNnSc2	zachování
přední	přední	k2eAgFnSc2d1	přední
části	část	k1gFnSc2	část
domu	dům	k1gInSc2	dům
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
historické	historický	k2eAgFnSc2d1	historická
významnosti	významnost	k1gFnSc2	významnost
pro	pro	k7c4	pro
město	město	k1gNnSc4	město
Zlín	Zlín	k1gInSc1	Zlín
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Městské	městský	k2eAgNnSc1d1	Městské
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
Filharmonie	filharmonie	k1gFnSc1	filharmonie
Bohuslava	Bohuslava	k1gFnSc1	Bohuslava
Martinů	Martinů	k1gFnSc1	Martinů
<g/>
,	,	kIx,	,
Velké	velký	k2eAgNnSc1d1	velké
kino	kino	k1gNnSc1	kino
<g/>
,	,	kIx,	,
Krajská	krajský	k2eAgFnSc1d1	krajská
knihovna	knihovna	k1gFnSc1	knihovna
Františka	František	k1gMnSc2	František
Bartoše	Bartoš	k1gMnSc2	Bartoš
<g/>
,	,	kIx,	,
Krajská	krajský	k2eAgFnSc1d1	krajská
galerie	galerie	k1gFnSc1	galerie
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
expozicemi	expozice	k1gFnPc7	expozice
v	v	k7c6	v
Domě	dům	k1gInSc6	dům
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
patře	patro	k1gNnSc6	patro
zámku	zámek	k1gInSc2	zámek
<g/>
,	,	kIx,	,
ve	v	k7c6	v
zlínském	zlínský	k2eAgInSc6d1	zlínský
zámku	zámek	k1gInSc6	zámek
sídlí	sídlet	k5eAaImIp3nS	sídlet
Muzeum	muzeum	k1gNnSc1	muzeum
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Moravy	Morava	k1gFnSc2	Morava
s	s	k7c7	s
expozicí	expozice	k1gFnSc7	expozice
S	s	k7c7	s
inženýry	inženýr	k1gMnPc7	inženýr
Hanzelkou	Hanzelka	k1gMnSc7	Hanzelka
a	a	k8xC	a
Zikmundem	Zikmund	k1gMnSc7	Zikmund
pěti	pět	k4xCc7	pět
světadíly	světadíl	k1gInPc7	světadíl
<g/>
,	,	kIx,	,
Archiv	archiv	k1gInSc1	archiv
H	H	kA	H
<g/>
+	+	kIx~	+
<g/>
Z	Z	kA	Z
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
spravuje	spravovat	k5eAaImIp3nS	spravovat
cestovatelský	cestovatelský	k2eAgInSc1d1	cestovatelský
odkaz	odkaz	k1gInSc1	odkaz
Hanzelky	Hanzelka	k1gMnSc2	Hanzelka
a	a	k8xC	a
Zikmunda	Zikmund	k1gMnSc2	Zikmund
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
Jana	Jan	k1gMnSc4	Jan
Havlasy	Havlasa	k1gFnSc2	Havlasa
<g/>
,	,	kIx,	,
Stanislava	Stanislav	k1gMnSc2	Stanislav
Škuliny	škulina	k1gFnSc2	škulina
<g/>
,	,	kIx,	,
Heleny	Helena	k1gFnSc2	Helena
Šťastné-Bübelové	Šťastné-Bübelová	k1gFnSc2	Šťastné-Bübelová
a	a	k8xC	a
Eduarda	Eduard	k1gMnSc2	Eduard
Ingriše	Ingriš	k1gMnSc2	Ingriš
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejnojmenné	stejnojmenný	k2eAgFnSc6d1	stejnojmenná
okrajové	okrajový	k2eAgFnSc6d1	okrajová
části	část	k1gFnSc6	část
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
hrad	hrad	k1gInSc1	hrad
Malenovice	Malenovice	k1gFnSc2	Malenovice
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
je	být	k5eAaImIp3nS	být
také	také	k9	také
Muzeum	muzeum	k1gNnSc1	muzeum
obuvi	obuv	k1gFnSc2	obuv
<g/>
,	,	kIx,	,
divadlo	divadlo	k1gNnSc4	divadlo
Malá	malý	k2eAgFnSc1d1	malá
scéna	scéna	k1gFnSc1	scéna
a	a	k8xC	a
folklorní	folklorní	k2eAgInPc1d1	folklorní
soubory	soubor	k1gInPc1	soubor
Kašava	Kašava	k1gFnSc1	Kašava
<g/>
,	,	kIx,	,
Vonica	Vonica	k1gFnSc1	Vonica
a	a	k8xC	a
Bartošův	Bartošův	k2eAgInSc1d1	Bartošův
soubor	soubor	k1gInSc1	soubor
písní	píseň	k1gFnPc2	píseň
a	a	k8xC	a
tanců	tanec	k1gInPc2	tanec
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
konají	konat	k5eAaImIp3nP	konat
festivaly	festival	k1gInPc4	festival
vážné	vážný	k2eAgFnSc2d1	vážná
hudby	hudba	k1gFnSc2	hudba
Harmonia	harmonium	k1gNnSc2	harmonium
Moraviae	Moravia	k1gFnSc2	Moravia
a	a	k8xC	a
Talentinum	Talentinum	k1gNnSc4	Talentinum
<g/>
,	,	kIx,	,
festivaly	festival	k1gInPc4	festival
folklorních	folklorní	k2eAgInPc2d1	folklorní
souborů	soubor	k1gInPc2	soubor
a	a	k8xC	a
dechových	dechový	k2eAgInPc2d1	dechový
orchestrů	orchestr	k1gInPc2	orchestr
FEDO	FEDO	kA	FEDO
(	(	kIx(	(
<g/>
který	který	k3yIgInSc1	který
pořádá	pořádat	k5eAaImIp3nS	pořádat
Velký	velký	k2eAgInSc1d1	velký
dechový	dechový	k2eAgInSc1d1	dechový
orchestr	orchestr	k1gInSc1	orchestr
města	město	k1gNnSc2	město
Zlína	Zlín	k1gInSc2	Zlín
<g/>
)	)	kIx)	)
a	a	k8xC	a
Zlínské	zlínský	k2eAgNnSc4d1	zlínské
besedování	besedování	k1gNnSc4	besedování
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgInSc4d1	divadelní
festival	festival	k1gInSc4	festival
Setkání-Stretnutie	Setkání-Stretnutie	k1gFnSc2	Setkání-Stretnutie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
festival	festival	k1gInSc1	festival
cestování	cestování	k1gNnSc2	cestování
<g/>
,	,	kIx,	,
poznávání	poznávání	k1gNnSc3	poznávání
a	a	k8xC	a
sbližování	sbližování	k1gNnSc3	sbližování
kultur	kultura	k1gFnPc2	kultura
Neznámá	známý	k2eNgFnSc1d1	neznámá
Země	země	k1gFnSc1	země
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc7d3	nejznámější
akcí	akce	k1gFnSc7	akce
je	být	k5eAaImIp3nS	být
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
festival	festival	k1gInSc1	festival
filmů	film	k1gInPc2	film
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
mládež	mládež	k1gFnSc4	mládež
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
části	část	k1gFnSc6	část
Zlína	Zlín	k1gInSc2	Zlín
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Štípě	štípa	k1gFnSc6	štípa
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
krásná	krásný	k2eAgFnSc1d1	krásná
zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
,	,	kIx,	,
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
nejnavštěvovanější	navštěvovaný	k2eAgFnSc1d3	nejnavštěvovanější
turistická	turistický	k2eAgFnSc1d1	turistická
atrakce	atrakce	k1gFnSc1	atrakce
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Zlínská	zlínský	k2eAgFnSc1d1	zlínská
zoo	zoo	k1gFnSc1	zoo
je	být	k5eAaImIp3nS	být
jedinečným	jedinečný	k2eAgNnSc7d1	jedinečné
místem	místo	k1gNnSc7	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mohou	moct	k5eAaImIp3nP	moct
návštěvníci	návštěvník	k1gMnPc1	návštěvník
vidět	vidět	k5eAaImF	vidět
zvířata	zvíře	k1gNnPc4	zvíře
všech	všecek	k3xTgMnPc2	všecek
kontinentů	kontinent	k1gInPc2	kontinent
v	v	k7c6	v
přírodním	přírodní	k2eAgInSc6d1	přírodní
bioparku	biopark	k1gInSc6	biopark
s	s	k7c7	s
volným	volný	k2eAgInSc7d1	volný
výběhem	výběh	k1gInSc7	výběh
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
se	se	k3xPyFc4	se
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
pravidelně	pravidelně	k6eAd1	pravidelně
koná	konat	k5eAaImIp3nS	konat
Film	film	k1gInSc1	film
festival	festival	k1gInSc1	festival
Zlín	Zlín	k1gInSc1	Zlín
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
návštěvnost	návštěvnost	k1gFnSc1	návštěvnost
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
100	[number]	k4	100
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Festival	festival	k1gInSc1	festival
je	být	k5eAaImIp3nS	být
podporován	podporovat	k5eAaImNgInS	podporovat
Ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
kultury	kultura	k1gFnSc2	kultura
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
Státním	státní	k2eAgInSc7d1	státní
fondem	fond	k1gInSc7	fond
kultury	kultura	k1gFnSc2	kultura
ČR	ČR	kA	ČR
a	a	k8xC	a
Evropskou	evropský	k2eAgFnSc7d1	Evropská
unií	unie	k1gFnSc7	unie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
stadion	stadion	k1gInSc1	stadion
Letná	Letná	k1gFnSc1	Letná
<g/>
,	,	kIx,	,
Zimní	zimní	k2eAgInSc1d1	zimní
stadion	stadion	k1gInSc1	stadion
Luďka	Luděk	k1gMnSc2	Luděk
Čajky	čajka	k1gFnSc2	čajka
<g/>
,	,	kIx,	,
PSG	PSG	kA	PSG
Arena	Aren	k1gInSc2	Aren
<g/>
,	,	kIx,	,
atletický	atletický	k2eAgInSc1d1	atletický
stadion	stadion	k1gInSc1	stadion
<g/>
,	,	kIx,	,
městská	městský	k2eAgFnSc1d1	městská
hala	hala	k1gFnSc1	hala
<g/>
,	,	kIx,	,
krytý	krytý	k2eAgInSc1d1	krytý
bazén	bazén	k1gInSc1	bazén
<g/>
,	,	kIx,	,
koupaliště	koupaliště	k1gNnSc1	koupaliště
<g/>
,	,	kIx,	,
turistické	turistický	k2eAgFnPc1d1	turistická
a	a	k8xC	a
cykloturistické	cykloturistický	k2eAgFnPc1d1	cykloturistická
stezky	stezka	k1gFnPc1	stezka
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
menší	malý	k2eAgNnPc1d2	menší
sportoviště	sportoviště	k1gNnPc1	sportoviště
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
období	období	k1gNnSc6	období
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
lyžařská	lyžařský	k2eAgFnSc1d1	lyžařská
sjezdovka	sjezdovka	k1gFnSc1	sjezdovka
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
a	a	k8xC	a
okolí	okolí	k1gNnSc6	okolí
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
jezdí	jezdit	k5eAaImIp3nP	jezdit
motoristický	motoristický	k2eAgInSc4d1	motoristický
závod	závod	k1gInSc4	závod
Barum	barum	k1gInSc1	barum
Rally	Ralla	k1gFnSc2	Ralla
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
významné	významný	k2eAgInPc4d1	významný
sportovní	sportovní	k2eAgInPc4d1	sportovní
kluby	klub	k1gInPc4	klub
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
FC	FC	kA	FC
Fastav	Fastav	k1gFnSc1	Fastav
Zlín	Zlín	k1gInSc1	Zlín
-	-	kIx~	-
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
PSG	PSG	kA	PSG
Zlín	Zlín	k1gInSc1	Zlín
-	-	kIx~	-
hokejový	hokejový	k2eAgInSc1d1	hokejový
klub	klub	k1gInSc1	klub
VSC	VSC	kA	VSC
Fatra	Fatra	k1gFnSc1	Fatra
Zlín	Zlín	k1gInSc1	Zlín
-	-	kIx~	-
volejbalový	volejbalový	k2eAgInSc1d1	volejbalový
klub	klub	k1gInSc1	klub
SKB	SKB	kA	SKB
Proton	proton	k1gInSc1	proton
Zlín	Zlín	k1gInSc1	Zlín
-	-	kIx~	-
basketbalový	basketbalový	k2eAgInSc1d1	basketbalový
klub	klub	k1gInSc1	klub
HC	HC	kA	HC
Zlín	Zlín	k1gInSc1	Zlín
-	-	kIx~	-
házenkářský	házenkářský	k2eAgInSc1d1	házenkářský
klub	klub	k1gInSc1	klub
RC	RC	kA	RC
Zlín	Zlín	k1gInSc1	Zlín
-	-	kIx~	-
ragbyový	ragbyový	k2eAgInSc1d1	ragbyový
klub	klub	k1gInSc1	klub
SKOB	skoba	k1gFnPc2	skoba
Zlín	Zlín	k1gInSc1	Zlín
-	-	kIx~	-
klub	klub	k1gInSc1	klub
orientačního	orientační	k2eAgInSc2d1	orientační
běhu	běh	k1gInSc2	běh
Tiger	Tiger	k1gInSc1	Tiger
Club	club	k1gInSc1	club
Zlín	Zlín	k1gInSc1	Zlín
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
kickboxerský	kickboxerský	k2eAgInSc1d1	kickboxerský
klub	klub	k1gInSc1	klub
TJ	tj	kA	tj
SOKOL	Sokol	k1gInSc1	Sokol
Zlín	Zlín	k1gInSc1	Zlín
-	-	kIx~	-
vzpěračský	vzpěračský	k2eAgInSc1d1	vzpěračský
klub	klub	k1gInSc1	klub
PK	PK	kA	PK
Zlín	Zlín	k1gInSc1	Zlín
-	-	kIx~	-
plavecký	plavecký	k2eAgInSc1d1	plavecký
klub	klub	k1gInSc1	klub
ŠK	ŠK	kA	ŠK
Zlín	Zlín	k1gInSc1	Zlín
-	-	kIx~	-
šachový	šachový	k2eAgInSc1d1	šachový
klub	klub	k1gInSc1	klub
KCK	KCK	kA	KCK
Zlín	Zlín	k1gInSc1	Zlín
-	-	kIx~	-
cyklistický	cyklistický	k2eAgInSc1d1	cyklistický
klub	klub	k1gInSc1	klub
AK	AK	kA	AK
Zlín	Zlín	k1gInSc1	Zlín
-	-	kIx~	-
atletický	atletický	k2eAgInSc1d1	atletický
klub	klub	k1gInSc1	klub
KC	KC	kA	KC
Zlín	Zlín	k1gInSc1	Zlín
-	-	kIx~	-
kuželkářský	kuželkářský	k2eAgInSc1d1	kuželkářský
klub	klub	k1gInSc1	klub
TJ	tj	kA	tj
Sokol	Sokol	k1gInSc1	Sokol
Zlín	Zlín	k1gInSc1	Zlín
-	-	kIx~	-
klub	klub	k1gInSc1	klub
sportovní	sportovní	k2eAgFnSc2d1	sportovní
gymnastiky	gymnastika	k1gFnSc2	gymnastika
TJ	tj	kA	tj
Sokol	Sokol	k1gInSc1	Sokol
Zlín	Zlín	k1gInSc1	Zlín
-	-	kIx~	-
klub	klub	k1gInSc1	klub
sálové	sálový	k2eAgFnSc2d1	sálová
cyklistiky	cyklistika	k1gFnSc2	cyklistika
WTN	WTN	kA	WTN
JJ	JJ	kA	JJ
Zlín	Zlín	k1gInSc1	Zlín
-	-	kIx~	-
klub	klub	k1gInSc1	klub
jiu-jitsu	jiuits	k1gInSc2	jiu-jits
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Trolejbusová	trolejbusový	k2eAgFnSc1d1	trolejbusová
doprava	doprava	k1gFnSc1	doprava
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
a	a	k8xC	a
v	v	k7c6	v
Otrokovicích	Otrokovice	k1gFnPc6	Otrokovice
a	a	k8xC	a
Městská	městský	k2eAgFnSc1d1	městská
autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
a	a	k8xC	a
v	v	k7c6	v
Otrokovicích	Otrokovice	k1gFnPc6	Otrokovice
<g/>
.	.	kIx.	.
</s>
<s>
Zlín	Zlín	k1gInSc1	Zlín
leží	ležet	k5eAaImIp3nS	ležet
relativně	relativně	k6eAd1	relativně
stranou	stranou	k6eAd1	stranou
od	od	k7c2	od
hlavních	hlavní	k2eAgInPc2d1	hlavní
dopravních	dopravní	k2eAgInPc2d1	dopravní
tahů	tah	k1gInPc2	tah
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
dáno	dát	k5eAaPmNgNnS	dát
jednak	jednak	k8xC	jednak
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
města	město	k1gNnSc2	město
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
již	již	k6eAd1	již
existovala	existovat	k5eAaImAgFnS	existovat
hlavní	hlavní	k2eAgFnSc1d1	hlavní
železniční	železniční	k2eAgFnSc1d1	železniční
síť	síť	k1gFnSc1	síť
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vinou	vinou	k7c2	vinou
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
neuskutečnily	uskutečnit	k5eNaPmAgInP	uskutečnit
dopravní	dopravní	k2eAgInPc1d1	dopravní
projekty	projekt	k1gInPc1	projekt
plánované	plánovaný	k2eAgInPc1d1	plánovaný
firmou	firma	k1gFnSc7	firma
Baťa	Baťa	k1gMnSc1	Baťa
<g/>
.	.	kIx.	.
</s>
<s>
Třetím	třetí	k4xOgInSc7	třetí
důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
rozpad	rozpad	k1gInSc1	rozpad
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
Zlínsko	Zlínsko	k1gNnSc1	Zlínsko
stalo	stát	k5eAaPmAgNnS	stát
pohraniční	pohraniční	k2eAgFnSc7d1	pohraniční
oblastí	oblast	k1gFnSc7	oblast
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
dopravních	dopravní	k2eAgFnPc2d1	dopravní
tras	trasa	k1gFnPc2	trasa
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
regionu	region	k1gInSc6	region
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c4	na
okraj	okraj	k1gInSc4	okraj
zájmu	zájem	k1gInSc2	zájem
centrálních	centrální	k2eAgInPc2d1	centrální
orgánů	orgán	k1gInPc2	orgán
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Ve	v	k7c6	v
východo-západním	východoápadní	k2eAgInSc6d1	východo-západní
směru	směr	k1gInSc6	směr
město	město	k1gNnSc1	město
protíná	protínat	k5eAaImIp3nS	protínat
silnice	silnice	k1gFnPc4	silnice
I	i	k9	i
<g/>
/	/	kIx~	/
<g/>
49	[number]	k4	49
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
silnice	silnice	k1gFnSc1	silnice
spojuje	spojovat	k5eAaImIp3nS	spojovat
Otrokovice	Otrokovice	k1gFnPc4	Otrokovice
s	s	k7c7	s
Valašskou	valašský	k2eAgFnSc7d1	Valašská
Polankou	polanka	k1gFnSc7	polanka
na	na	k7c6	na
Vsetínsku	Vsetínsko	k1gNnSc6	Vsetínsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
však	však	k9	však
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
rychlostní	rychlostní	k2eAgFnSc1d1	rychlostní
silnice	silnice	k1gFnSc1	silnice
R55	R55	k1gFnSc2	R55
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
značena	značit	k5eAaImNgFnS	značit
jako	jako	k9	jako
dálnice	dálnice	k1gFnSc1	dálnice
D	D	kA	D
<g/>
55	[number]	k4	55
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
napojuje	napojovat	k5eAaImIp3nS	napojovat
Zlín	Zlín	k1gInSc4	Zlín
na	na	k7c4	na
dálniční	dálniční	k2eAgFnSc4d1	dálniční
síť	síť	k1gFnSc4	síť
D	D	kA	D
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
zrychlilo	zrychlit	k5eAaPmAgNnS	zrychlit
dopravní	dopravní	k2eAgNnSc1d1	dopravní
spojení	spojení	k1gNnSc1	spojení
např.	např.	kA	např.
s	s	k7c7	s
městem	město	k1gNnSc7	město
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Zlín	Zlín	k1gInSc1	Zlín
neleží	ležet	k5eNaImIp3nS	ležet
na	na	k7c6	na
žádném	žádný	k3yNgInSc6	žádný
významném	významný	k2eAgInSc6d1	významný
železničním	železniční	k2eAgInSc6d1	železniční
tahu	tah	k1gInSc6	tah
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
tratí	trať	k1gFnSc7	trať
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
protíná	protínat	k5eAaImIp3nS	protínat
město	město	k1gNnSc4	město
rovnoběžně	rovnoběžně	k6eAd1	rovnoběžně
s	s	k7c7	s
hlavní	hlavní	k2eAgFnSc7d1	hlavní
silnicí	silnice	k1gFnSc7	silnice
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jednokolejná	jednokolejný	k2eAgFnSc1d1	jednokolejná
trať	trať	k1gFnSc1	trať
Otrokovice	Otrokovice	k1gFnPc1	Otrokovice
<g/>
-	-	kIx~	-
<g/>
Vizovice	Vizovice	k1gFnPc1	Vizovice
(	(	kIx(	(
<g/>
trať	trať	k1gFnSc1	trať
331	[number]	k4	331
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
stanice	stanice	k1gFnSc1	stanice
v	v	k7c6	v
Otrokovicích	Otrokovice	k1gFnPc6	Otrokovice
(	(	kIx(	(
<g/>
trať	trať	k1gFnSc1	trať
330	[number]	k4	330
<g/>
)	)	kIx)	)
tak	tak	k6eAd1	tak
plní	plnit	k5eAaImIp3nS	plnit
úlohu	úloha	k1gFnSc4	úloha
hlavního	hlavní	k2eAgInSc2d1	hlavní
železničního	železniční	k2eAgInSc2d1	železniční
terminálu	terminál	k1gInSc2	terminál
pro	pro	k7c4	pro
zlínskou	zlínský	k2eAgFnSc4d1	zlínská
aglomeraci	aglomerace	k1gFnSc4	aglomerace
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
Zlínem	Zlín	k1gInSc7	Zlín
je	být	k5eAaImIp3nS	být
dopravně	dopravně	k6eAd1	dopravně
propojena	propojit	k5eAaPmNgFnS	propojit
nejen	nejen	k6eAd1	nejen
místní	místní	k2eAgFnSc1d1	místní
dráhou	dráha	k1gFnSc7	dráha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
trolejbusovou	trolejbusový	k2eAgFnSc7d1	trolejbusová
dopravou	doprava	k1gFnSc7	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Městskou	městský	k2eAgFnSc4d1	městská
hromadnou	hromadný	k2eAgFnSc4d1	hromadná
dopravu	doprava	k1gFnSc4	doprava
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
trolejbusy	trolejbus	k1gInPc1	trolejbus
a	a	k8xC	a
autobusy	autobus	k1gInPc1	autobus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
byly	být	k5eAaImAgInP	být
k	k	k7c3	k
městu	město	k1gNnSc3	město
Zlín	Zlín	k1gInSc1	Zlín
připojeny	připojit	k5eAaPmNgFnP	připojit
obce	obec	k1gFnPc1	obec
Prštné	Prštný	k2eAgFnPc1d1	Prštný
<g/>
,	,	kIx,	,
Louky	louka	k1gFnPc1	louka
nad	nad	k7c7	nad
Dřevnicí	Dřevnice	k1gFnSc7	Dřevnice
<g/>
,	,	kIx,	,
Mladcová	Mladcová	k1gFnSc1	Mladcová
<g/>
,	,	kIx,	,
Příluky	Příluk	k1gInPc1	Příluk
a	a	k8xC	a
Kudlov	Kudlov	k1gInSc1	Kudlov
a	a	k8xC	a
městské	městský	k2eAgFnPc1d1	městská
čtvrti	čtvrt	k1gFnPc1	čtvrt
byly	být	k5eAaImAgFnP	být
pojmenovány	pojmenován	k2eAgFnPc1d1	pojmenována
<g/>
:	:	kIx,	:
Zlín	Zlín	k1gInSc1	Zlín
I	i	k9	i
-	-	kIx~	-
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
město	město	k1gNnSc4	město
Zlín	Zlín	k1gInSc1	Zlín
II	II	kA	II
-	-	kIx~	-
Prštné	Prštný	k2eAgFnSc2d1	Prštný
Zlín	Zlín	k1gInSc1	Zlín
III	III	kA	III
-	-	kIx~	-
Louky	louka	k1gFnSc2	louka
Zlín	Zlín	k1gInSc1	Zlín
IV	IV	kA	IV
-	-	kIx~	-
Mladcová	Mladcová	k1gFnSc1	Mladcová
Zlín	Zlín	k1gInSc1	Zlín
V	V	kA	V
-	-	kIx~	-
Příluky	Příluk	k1gInPc4	Příluk
Zlín	Zlín	k1gInSc1	Zlín
VI	VI	kA	VI
-	-	kIx~	-
Kudlov	Kudlov	k1gInSc1	Kudlov
K	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1949	[number]	k4	1949
byly	být	k5eAaImAgFnP	být
k	k	k7c3	k
městu	město	k1gNnSc3	město
připojeny	připojit	k5eAaPmNgFnP	připojit
obce	obec	k1gFnPc1	obec
Jaroslavice	Jaroslavice	k1gFnSc2	Jaroslavice
<g/>
,	,	kIx,	,
Malenovice	Malenovice	k1gFnPc1	Malenovice
<g/>
,	,	kIx,	,
Tečovice	Tečovice	k1gFnPc1	Tečovice
<g/>
,	,	kIx,	,
Kvítkovice	Kvítkovice	k1gFnPc1	Kvítkovice
a	a	k8xC	a
Otrokovice	Otrokovice	k1gFnPc1	Otrokovice
a	a	k8xC	a
sloučené	sloučený	k2eAgNnSc1d1	sloučené
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
Gottwaldov	Gottwaldov	k1gInSc4	Gottwaldov
<g/>
.	.	kIx.	.
</s>
<s>
Městské	městský	k2eAgFnPc1d1	městská
čtvrti	čtvrt	k1gFnPc1	čtvrt
nesly	nést	k5eAaImAgFnP	nést
názvy	název	k1gInPc4	název
<g/>
:	:	kIx,	:
Gottwaldov	Gottwaldov	k1gInSc4	Gottwaldov
I	I	kA	I
-	-	kIx~	-
Zlín	Zlín	k1gInSc1	Zlín
Gottwaldov	Gottwaldov	k1gInSc1	Gottwaldov
II	II	kA	II
-	-	kIx~	-
Prštné	Prštný	k2eAgFnSc2d1	Prštný
Gottwaldov	Gottwaldov	k1gInSc1	Gottwaldov
III	III	kA	III
-	-	kIx~	-
Louky	louka	k1gFnSc2	louka
Gottwaldov	Gottwaldov	k1gInSc1	Gottwaldov
IV	IV	kA	IV
-	-	kIx~	-
Mladcová	Mladcová	k1gFnSc1	Mladcová
Gottwaldov	Gottwaldov	k1gInSc1	Gottwaldov
V	V	kA	V
-	-	kIx~	-
Příluky	Příluk	k1gInPc4	Příluk
Gottwaldov	Gottwaldov	k1gInSc1	Gottwaldov
VI	VI	kA	VI
-	-	kIx~	-
Jaroslavice	Jaroslavice	k1gFnSc1	Jaroslavice
Gottwaldov	Gottwaldov	k1gInSc1	Gottwaldov
VII	VII	kA	VII
-	-	kIx~	-
Kudlov	Kudlov	k1gInSc1	Kudlov
(	(	kIx(	(
<g/>
přečíslován	přečíslován	k2eAgMnSc1d1	přečíslován
z	z	k7c2	z
VI	VI	kA	VI
na	na	k7c6	na
VII	VII	kA	VII
<g/>
)	)	kIx)	)
Gottwaldov	Gottwaldov	k1gInSc1	Gottwaldov
VIII	VIII	kA	VIII
-	-	kIx~	-
Malenovice	Malenovice	k1gFnSc1	Malenovice
Gottwaldov	Gottwaldov	k1gInSc1	Gottwaldov
IX	IX	kA	IX
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
Tečovice	Tečovice	k1gFnSc1	Tečovice
Gottwaldov	Gottwaldov	k1gInSc1	Gottwaldov
X	X	kA	X
-	-	kIx~	-
Kvítkovice	Kvítkovice	k1gFnSc1	Kvítkovice
Gottwaldov	Gottwaldov	k1gInSc1	Gottwaldov
XI	XI	kA	XI
-	-	kIx~	-
Otrokovice	Otrokovice	k1gFnPc1	Otrokovice
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
nejsou	být	k5eNaImIp3nP	být
Otrokovice	Otrokovice	k1gFnPc4	Otrokovice
ani	ani	k8xC	ani
Kvítkovice	Kvítkovice	k1gFnPc4	Kvítkovice
zmiňovány	zmiňován	k2eAgFnPc4d1	zmiňována
mezi	mezi	k7c7	mezi
osadami	osada	k1gFnPc7	osada
obce	obec	k1gFnSc2	obec
Gottwaldov	Gottwaldov	k1gInSc4	Gottwaldov
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
uváděny	uvádět	k5eAaImNgFnP	uvádět
jako	jako	k9	jako
samostatné	samostatný	k2eAgFnPc1d1	samostatná
obce	obec	k1gFnPc1	obec
-	-	kIx~	-
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
byly	být	k5eAaImAgFnP	být
Kvítkovice	Kvítkovice	k1gFnPc1	Kvítkovice
připojeny	připojen	k2eAgFnPc1d1	připojena
k	k	k7c3	k
Otrokovicím	Otrokovice	k1gFnPc3	Otrokovice
a	a	k8xC	a
ty	ten	k3xDgFnPc1	ten
byly	být	k5eAaImAgFnP	být
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
"	"	kIx"	"
<g/>
povýšeny	povýšit	k5eAaPmNgInP	povýšit
<g/>
"	"	kIx"	"
na	na	k7c4	na
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1950	[number]	k4	1950
<g/>
-	-	kIx~	-
<g/>
1964	[number]	k4	1964
se	se	k3xPyFc4	se
Gottwaldov	Gottwaldov	k1gInSc1	Gottwaldov
členil	členit	k5eAaImAgInS	členit
na	na	k7c4	na
7	[number]	k4	7
osad	osada	k1gFnPc2	osada
<g/>
:	:	kIx,	:
Zlín	Zlín	k1gInSc1	Zlín
<g/>
,	,	kIx,	,
Zlínské	zlínský	k2eAgFnPc1d1	zlínská
Paseky	paseka	k1gFnPc1	paseka
<g/>
,	,	kIx,	,
Prštné	Prštný	k2eAgFnPc1d1	Prštný
<g/>
,	,	kIx,	,
Louky	louka	k1gFnPc1	louka
<g/>
,	,	kIx,	,
Mladcová	Mladcová	k1gFnSc1	Mladcová
<g/>
,	,	kIx,	,
Příluky	Příluk	k1gInPc1	Příluk
<g/>
,	,	kIx,	,
Malenovice	Malenovice	k1gFnPc1	Malenovice
(	(	kIx(	(
<g/>
v	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
osad	osada	k1gFnPc2	osada
nejsou	být	k5eNaImIp3nP	být
zmíněny	zmíněn	k2eAgFnPc1d1	zmíněna
dříve	dříve	k6eAd2	dříve
připojené	připojený	k2eAgFnPc1d1	připojená
Jaroslavice	Jaroslavice	k1gFnPc1	Jaroslavice
<g/>
,	,	kIx,	,
Kudlov	Kudlov	k1gInSc1	Kudlov
<g/>
,	,	kIx,	,
Tečovice	Tečovice	k1gFnPc1	Tečovice
<g/>
,	,	kIx,	,
Kvítkovice	Kvítkovice	k1gFnPc1	Kvítkovice
ani	ani	k8xC	ani
Otrokovice	Otrokovice	k1gFnPc1	Otrokovice
<g/>
,	,	kIx,	,
nově	nově	k6eAd1	nově
jsou	být	k5eAaImIp3nP	být
zmiňované	zmiňovaný	k2eAgFnPc4d1	zmiňovaná
Zlínské	zlínský	k2eAgFnPc4d1	zlínská
Paseky	paseka	k1gFnPc4	paseka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1964	[number]	k4	1964
se	se	k3xPyFc4	se
Gottwaldov	Gottwaldov	k1gInSc1	Gottwaldov
členil	členit	k5eAaImAgInS	členit
na	na	k7c4	na
12	[number]	k4	12
částí	část	k1gFnPc2	část
obce	obec	k1gFnSc2	obec
<g/>
:	:	kIx,	:
kromě	kromě	k7c2	kromě
dosavadních	dosavadní	k2eAgInPc2d1	dosavadní
sedmi	sedm	k4xCc2	sedm
(	(	kIx(	(
<g/>
Zlín	Zlín	k1gInSc1	Zlín
<g/>
,	,	kIx,	,
Zlínské	zlínský	k2eAgFnPc1d1	zlínská
Paseky	paseka	k1gFnPc1	paseka
<g/>
,	,	kIx,	,
Prštné	Prštný	k2eAgFnPc1d1	Prštný
<g/>
,	,	kIx,	,
Louky	louka	k1gFnPc1	louka
<g/>
,	,	kIx,	,
Mladcová	Mladcová	k1gFnSc1	Mladcová
<g/>
,	,	kIx,	,
Příluky	Příluk	k1gInPc1	Příluk
<g/>
,	,	kIx,	,
Malenovice	Malenovice	k1gFnPc1	Malenovice
<g/>
)	)	kIx)	)
nově	nově	k6eAd1	nově
ještě	ještě	k9	ještě
Jaroslavice	Jaroslavice	k1gFnSc1	Jaroslavice
<g/>
,	,	kIx,	,
Kudlov	Kudlov	k1gInSc1	Kudlov
a	a	k8xC	a
Tečovice	Tečovice	k1gFnSc1	Tečovice
(	(	kIx(	(
<g/>
obce	obec	k1gFnSc2	obec
těchto	tento	k3xDgNnPc2	tento
jmen	jméno	k1gNnPc2	jméno
však	však	k9	však
byly	být	k5eAaImAgFnP	být
podle	podle	k7c2	podle
vyhlášky	vyhláška	k1gFnSc2	vyhláška
připojeny	připojit	k5eAaPmNgFnP	připojit
již	již	k6eAd1	již
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
a	a	k8xC	a
Lhotka	Lhotka	k1gFnSc1	Lhotka
a	a	k8xC	a
Chlum	chlum	k1gInSc1	chlum
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1976	[number]	k4	1976
k	k	k7c3	k
dosavadním	dosavadní	k2eAgFnPc3d1	dosavadní
12	[number]	k4	12
přibylo	přibýt	k5eAaPmAgNnS	přibýt
8	[number]	k4	8
nových	nový	k2eAgFnPc2d1	nová
částí	část	k1gFnPc2	část
obce	obec	k1gFnSc2	obec
<g/>
:	:	kIx,	:
Březnice	Březnice	k1gFnSc1	Březnice
<g/>
,	,	kIx,	,
Klečůvka	Klečůvka	k1gFnSc1	Klečůvka
<g/>
,	,	kIx,	,
Kostelec	Kostelec	k1gInSc1	Kostelec
<g/>
,	,	kIx,	,
Lípa	lípa	k1gFnSc1	lípa
<g/>
,	,	kIx,	,
Lužkovice	Lužkovice	k1gFnSc1	Lužkovice
<g/>
,	,	kIx,	,
Salaš	salaš	k1gFnSc1	salaš
<g/>
,	,	kIx,	,
Štípa	štípa	k1gFnSc1	štípa
a	a	k8xC	a
Želechovice	Želechovice	k1gFnPc1	Želechovice
nad	nad	k7c7	nad
Dřevnicí	Dřevnice	k1gFnSc7	Dřevnice
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc6	leden
1980	[number]	k4	1980
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
částí	část	k1gFnPc2	část
obce	obec	k1gFnSc2	obec
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
na	na	k7c4	na
23	[number]	k4	23
<g/>
;	;	kIx,	;
přibyly	přibýt	k5eAaPmAgFnP	přibýt
Karlovice	Karlovice	k1gFnPc1	Karlovice
<g/>
,	,	kIx,	,
Ostrata	Ostrata	k1gFnSc1	Ostrata
a	a	k8xC	a
Velíková	Velíková	k1gFnSc1	Velíková
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
červenci	červenec	k1gInSc6	červenec
1980	[number]	k4	1980
přibyla	přibýt	k5eAaPmAgFnS	přibýt
Lhota	Lhota	k1gFnSc1	Lhota
a	a	k8xC	a
počet	počet	k1gInSc1	počet
částí	část	k1gFnPc2	část
obce	obec	k1gFnSc2	obec
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
na	na	k7c4	na
24	[number]	k4	24
<g/>
,	,	kIx,	,
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc6	leden
1990	[number]	k4	1990
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c4	na
Zlín	Zlín	k1gInSc4	Zlín
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1990	[number]	k4	1990
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
částí	část	k1gFnPc2	část
obce	obec	k1gFnSc2	obec
snížil	snížit	k5eAaPmAgInS	snížit
na	na	k7c4	na
21	[number]	k4	21
<g/>
:	:	kIx,	:
osamostatnily	osamostatnit	k5eAaPmAgInP	osamostatnit
se	se	k3xPyFc4	se
Lhota	Lhota	k1gFnSc1	Lhota
<g/>
,	,	kIx,	,
Lípa	lípa	k1gFnSc1	lípa
a	a	k8xC	a
Tečovice	Tečovice	k1gFnSc1	Tečovice
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc6	leden
1992	[number]	k4	1992
se	se	k3xPyFc4	se
osamostatnily	osamostatnit	k5eAaPmAgFnP	osamostatnit
Karlovice	Karlovice	k1gFnPc1	Karlovice
<g/>
,	,	kIx,	,
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc6	leden
1993	[number]	k4	1993
Březnice	Březnice	k1gFnSc2	Březnice
<g/>
,	,	kIx,	,
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc6	leden
2001	[number]	k4	2001
Ostrata	Ostrata	k1gFnSc1	Ostrata
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
22	[number]	k4	22
<g/>
.	.	kIx.	.
květnu	květen	k1gInSc6	květen
2003	[number]	k4	2003
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
část	část	k1gFnSc1	část
obce	obec	k1gFnSc2	obec
Zlínské	zlínský	k2eAgFnSc2d1	zlínská
Paseky	paseka	k1gFnSc2	paseka
a	a	k8xC	a
počet	počet	k1gInSc1	počet
částí	část	k1gFnPc2	část
obce	obec	k1gFnSc2	obec
tak	tak	k6eAd1	tak
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
na	na	k7c4	na
17	[number]	k4	17
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc6	leden
2009	[number]	k4	2009
se	se	k3xPyFc4	se
na	na	k7c6	na
základě	základ	k1gInSc6	základ
referenda	referendum	k1gNnSc2	referendum
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2008	[number]	k4	2008
osamostatnily	osamostatnit	k5eAaPmAgFnP	osamostatnit
Želechovice	Želechovice	k1gFnPc1	Želechovice
nad	nad	k7c7	nad
Dřevnicí	Dřevnice	k1gFnSc7	Dřevnice
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
15	[number]	k4	15
katastrálních	katastrální	k2eAgNnPc2d1	katastrální
území	území	k1gNnPc2	území
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
až	až	k9	až
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
výjimky	výjimka	k1gFnPc4	výjimka
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
členění	členění	k1gNnSc4	členění
města	město	k1gNnSc2	město
na	na	k7c4	na
16	[number]	k4	16
místních	místní	k2eAgFnPc2d1	místní
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
Zlín	Zlín	k1gInSc1	Zlín
(	(	kIx(	(
<g/>
stejnojmenná	stejnojmenný	k2eAgFnSc1d1	stejnojmenná
místní	místní	k2eAgFnSc1d1	místní
část	část	k1gFnSc1	část
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
též	též	k9	též
část	část	k1gFnSc1	část
k.	k.	k?	k.
ú.	ú.	k?	ú.
Příluky	Příluk	k1gInPc4	Příluk
u	u	k7c2	u
Zlína	Zlín	k1gInSc2	Zlín
<g/>
)	)	kIx)	)
Prštné	Prštné	k1gNnSc1	Prštné
(	(	kIx(	(
<g/>
též	též	k9	též
místní	místní	k2eAgFnSc1d1	místní
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Louky	louka	k1gFnPc1	louka
nad	nad	k7c7	nad
Dřevnicí	Dřevnice	k1gFnSc7	Dřevnice
(	(	kIx(	(
<g/>
místní	místní	k2eAgFnSc1d1	místní
část	část	k1gFnSc1	část
Louky	louka	k1gFnSc2	louka
<g/>
)	)	kIx)	)
Mladcová	Mladcová	k1gFnSc1	Mladcová
(	(	kIx(	(
<g/>
též	též	k6eAd1	též
místní	místní	k2eAgFnSc4d1	místní
část	část	k1gFnSc4	část
<g/>
)	)	kIx)	)
Příluky	Příluk	k1gMnPc4	Příluk
u	u	k7c2	u
Zlína	Zlín	k1gInSc2	Zlín
(	(	kIx(	(
<g/>
místní	místní	k2eAgFnSc1d1	místní
část	část	k1gFnSc1	část
Příluky	Příluk	k1gInPc4	Příluk
<g/>
,	,	kIx,	,
část	část	k1gFnSc4	část
k.	k.	k?	k.
ú.	ú.	k?	ú.
spadá	spadat	k5eAaImIp3nS	spadat
do	do	k7c2	do
místní	místní	k2eAgFnSc2d1	místní
části	část	k1gFnSc2	část
Zlín	Zlín	k1gInSc1	Zlín
<g/>
)	)	kIx)	)
Jaroslavice	Jaroslavice	k1gFnSc1	Jaroslavice
u	u	k7c2	u
Zlína	Zlín	k1gInSc2	Zlín
(	(	kIx(	(
<g/>
místní	místní	k2eAgFnSc4d1	místní
část	část	k1gFnSc4	část
Jaroslavice	Jaroslavice	k1gFnSc2	Jaroslavice
<g/>
)	)	kIx)	)
Kudlov	Kudlov	k1gInSc1	Kudlov
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
též	též	k9	též
místní	místní	k2eAgFnSc1d1	místní
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
Malenovice	Malenovice	k1gFnSc1	Malenovice
(	(	kIx(	(
<g/>
též	též	k9	též
místní	místní	k2eAgFnSc1d1	místní
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
Klečůvka	Klečůvka	k1gFnSc1	Klečůvka
(	(	kIx(	(
<g/>
též	též	k9	též
místní	místní	k2eAgFnSc1d1	místní
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
Kostelec	Kostelec	k1gInSc1	Kostelec
u	u	k7c2	u
Zlína	Zlín	k1gInSc2	Zlín
(	(	kIx(	(
<g/>
místní	místní	k2eAgFnSc1d1	místní
část	část	k1gFnSc1	část
Kostelec	Kostelec	k1gInSc1	Kostelec
<g/>
)	)	kIx)	)
-	-	kIx~	-
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
nesla	nést	k5eAaImAgFnS	nést
obec	obec	k1gFnSc1	obec
název	název	k1gInSc4	název
Kostelec	Kostelec	k1gInSc1	Kostelec
u	u	k7c2	u
Štípy	štípa	k1gFnSc2	štípa
Lhotka	Lhotka	k1gFnSc1	Lhotka
u	u	k7c2	u
Zlína	Zlín	k1gInSc2	Zlín
(	(	kIx(	(
<g/>
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
místní	místní	k2eAgFnPc4d1	místní
části	část	k1gFnPc4	část
<g />
.	.	kIx.	.
</s>
<s>
Lhotka	Lhotka	k1gFnSc1	Lhotka
a	a	k8xC	a
Chlum	chlum	k1gInSc1	chlum
<g/>
)	)	kIx)	)
Lužkovice	Lužkovice	k1gFnSc1	Lužkovice
(	(	kIx(	(
<g/>
též	též	k9	též
místní	místní	k2eAgFnSc1d1	místní
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
Salaš	salaš	k1gInSc1	salaš
u	u	k7c2	u
Zlína	Zlín	k1gInSc2	Zlín
(	(	kIx(	(
<g/>
místní	místní	k2eAgFnSc1d1	místní
část	část	k1gFnSc1	část
Salaš	salaš	k1gFnSc1	salaš
<g/>
)	)	kIx)	)
Štípa	štípa	k1gFnSc1	štípa
(	(	kIx(	(
<g/>
též	též	k9	též
místní	místní	k2eAgFnSc1d1	místní
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
Velíková	Velíková	k1gFnSc1	Velíková
(	(	kIx(	(
<g/>
též	též	k9	též
místní	místní	k2eAgFnSc1d1	místní
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
Altenburg	Altenburg	k1gInSc1	Altenburg
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
Groningen	Groningen	k1gInSc1	Groningen
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
Chorzów	Chorzów	k1gFnSc2	Chorzów
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Polsko	Polsko	k1gNnSc1	Polsko
Izegem	Izeg	k1gInSc7	Izeg
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc1	Belgie
Limbach-Oberfrohna	Limbach-Oberfrohna	k1gFnSc1	Limbach-Oberfrohna
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
Romans	romans	k1gInSc1	romans
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
Sesto	Sesto	k1gNnSc1	Sesto
San	San	k1gFnSc2	San
Giovanni	Giovanň	k1gMnSc3	Giovanň
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
Trenčín	Trenčín	k1gInSc1	Trenčín
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
Campo	Campa	k1gFnSc5	Campa
Grande	grand	k1gMnSc5	grand
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc1	Brazílie
Hynek	Hynek	k1gMnSc1	Hynek
Vojáček	Vojáček	k1gMnSc1	Vojáček
(	(	kIx(	(
<g/>
1825	[number]	k4	1825
<g/>
-	-	kIx~	-
<g/>
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
a	a	k8xC	a
publicista	publicista	k1gMnSc1	publicista
František	František	k1gMnSc1	František
<g />
.	.	kIx.	.
</s>
<s>
Bartoš	Bartoš	k1gMnSc1	Bartoš
(	(	kIx(	(
<g/>
1837	[number]	k4	1837
<g/>
-	-	kIx~	-
<g/>
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
moravský	moravský	k2eAgMnSc1d1	moravský
národopisec	národopisec	k1gMnSc1	národopisec
<g/>
,	,	kIx,	,
folklorista	folklorista	k1gMnSc1	folklorista
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
a	a	k8xC	a
jazykovědec	jazykovědec	k1gMnSc1	jazykovědec
Tomáš	Tomáš	k1gMnSc1	Tomáš
Baťa	Baťa	k1gMnSc1	Baťa
(	(	kIx(	(
<g/>
1876	[number]	k4	1876
<g/>
-	-	kIx~	-
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podnikatel	podnikatel	k1gMnSc1	podnikatel
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
obuvnického	obuvnický	k2eAgInSc2d1	obuvnický
koncernu	koncern	k1gInSc2	koncern
Baťa	Baťa	k1gMnSc1	Baťa
František	František	k1gMnSc1	František
Lydie	Lydie	k1gFnSc1	Lydie
Gahura	Gahura	k1gFnSc1	Gahura
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
Ilja	Ilja	k1gMnSc1	Ilja
Prachař	Prachař	k1gMnSc1	Prachař
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
-	-	kIx~	-
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Josef	Josef	k1gMnSc1	Josef
Hrejsemnou	Hrejsemný	k2eAgFnSc7d1	Hrejsemný
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
-	-	kIx~	-
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
Jan	Jan	k1gMnSc1	Jan
Libíček	Libíček	k1gMnSc1	Libíček
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
-	-	kIx~	-
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Sir	sir	k1gMnSc1	sir
John	John	k1gMnSc1	John
Tusa	Tusa	k1gMnSc1	Tusa
<g/>
,	,	kIx,	,
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Tůša	Tůša	k1gMnSc1	Tůša
(	(	kIx(	(
<g/>
*	*	kIx~	*
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
britský	britský	k2eAgMnSc1d1	britský
televizní	televizní	k2eAgMnSc1d1	televizní
žurnalista	žurnalista	k1gMnSc1	žurnalista
<g/>
,	,	kIx,	,
správce	správce	k1gMnSc1	správce
umění	umění	k1gNnSc2	umění
Petr	Petr	k1gMnSc1	Petr
Brauner	Brauner	k1gMnSc1	Brauner
(	(	kIx(	(
<g/>
*	*	kIx~	*
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
Sir	sir	k1gMnSc1	sir
Tom	Tom	k1gMnSc1	Tom
Stoppard	Stoppard	k1gMnSc1	Stoppard
<g/>
,	,	kIx,	,
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Straussler	Straussler	k1gMnSc1	Straussler
(	(	kIx(	(
<g/>
*	*	kIx~	*
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
britský	britský	k2eAgMnSc1d1	britský
dramatik	dramatik	k1gMnSc1	dramatik
Věra	Věra	k1gFnSc1	Věra
Galatíková	Galatíková	k1gFnSc1	Galatíková
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
-	-	kIx~	-
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
Josef	Josef	k1gMnSc1	Josef
Abrhám	Abrha	k1gFnPc3	Abrha
(	(	kIx(	(
<g/>
*	*	kIx~	*
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Eva	Eva	k1gFnSc1	Eva
Jiřičná	Jiřičný	k2eAgFnSc1d1	Jiřičná
(	(	kIx(	(
<g/>
*	*	kIx~	*
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architektka	architektka	k1gFnSc1	architektka
Jan	Jan	k1gMnSc1	Jan
Hraběta	Hraběta	k1gMnSc1	Hraběta
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
*	*	kIx~	*
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Jaromír	Jaromír	k1gMnSc1	Jaromír
Schneider	Schneider	k1gMnSc1	Schneider
(	(	kIx(	(
<g/>
*	*	kIx~	*
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
Antonín	Antonín	k1gMnSc1	Antonín
Bajaja	Bajaja	k1gMnSc1	Bajaja
(	(	kIx(	(
<g/>
*	*	kIx~	*
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
Felix	Felix	k1gMnSc1	Felix
Slováček	Slováček	k1gMnSc1	Slováček
(	(	kIx(	(
<g/>
*	*	kIx~	*
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudebník	hudebník	k1gMnSc1	hudebník
Benjamin	Benjamin	k1gMnSc1	Benjamin
Kuras	Kuras	k1gMnSc1	Kuras
(	(	kIx(	(
<g/>
*	*	kIx~	*
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
Bob	Bob	k1gMnSc1	Bob
Divílek	Divílek	k1gMnSc1	Divílek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kulturista	kulturista	k1gMnSc1	kulturista
<g/>
,	,	kIx,	,
iluzionista	iluzionista	k1gMnSc1	iluzionista
Vlasta	Vlasta	k1gFnSc1	Vlasta
Peterková	Peterková	k1gFnSc1	Peterková
(	(	kIx(	(
<g/>
*	*	kIx~	*
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
Ivana	Ivana	k1gFnSc1	Ivana
Trumpová	Trumpová	k1gFnSc1	Trumpová
<g/>
,	,	kIx,	,
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Zelníčková	Zelníčková	k1gFnSc1	Zelníčková
(	(	kIx(	(
<g/>
*	*	kIx~	*
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lyžařka	lyžařka	k1gFnSc1	lyžařka
a	a	k8xC	a
americká	americký	k2eAgFnSc1d1	americká
podnikatelka	podnikatelka	k1gFnSc1	podnikatelka
Zuzana	Zuzana	k1gFnSc1	Zuzana
Bubílková	Bubílková	k1gFnSc1	Bubílková
(	(	kIx(	(
<g/>
*	*	kIx~	*
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
publicistka	publicistka	k1gFnSc1	publicistka
a	a	k8xC	a
moderátorka	moderátorka	k1gFnSc1	moderátorka
Jan	Jan	k1gMnSc1	Jan
Antonín	Antonín	k1gMnSc1	Antonín
Pitínský	Pitínský	k2eAgMnSc1d1	Pitínský
(	(	kIx(	(
<g/>
*	*	kIx~	*
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
divadelní	divadelní	k2eAgMnSc1d1	divadelní
režisér	režisér	k1gMnSc1	režisér
Jan	Jan	k1gMnSc1	Jan
Ambrůz	Ambrůz	k1gMnSc1	Ambrůz
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
*	*	kIx~	*
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sochař	sochař	k1gMnSc1	sochař
Jana	Jana	k1gFnSc1	Jana
Kratochvílová	Kratochvílová	k1gFnSc1	Kratochvílová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Jiří	Jiří	k1gMnSc1	Jiří
Čunek	Čunek	k1gMnSc1	Čunek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
Olga	Olga	k1gFnSc1	Olga
Charvátová	Charvátová	k1gFnSc1	Charvátová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bývalá	bývalý	k2eAgFnSc1d1	bývalá
lyžařka	lyžařka	k1gFnSc1	lyžařka
Hana	Hana	k1gFnSc1	Hana
Andronikova	Andronikův	k2eAgFnSc1d1	Andronikova
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
-	-	kIx~	-
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Josef	Josef	k1gMnSc1	Josef
Polášek	Polášek	k1gMnSc1	Polášek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Jan	Jan	k1gMnSc1	Jan
Šťastný	Šťastný	k1gMnSc1	Šťastný
(	(	kIx(	(
<g/>
*	*	kIx~	*
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Leona	Leona	k1gFnSc1	Leona
Machálková	Machálková	k1gFnSc1	Machálková
(	(	kIx(	(
<g/>
*	*	kIx~	*
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Pavel	Pavel	k1gMnSc1	Pavel
Petr	Petr	k1gMnSc1	Petr
(	(	kIx(	(
<g/>
*	*	kIx~	*
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
<g />
.	.	kIx.	.
</s>
<s>
Roman	Roman	k1gMnSc1	Roman
Čechmánek	Čechmánek	k1gMnSc1	Čechmánek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejový	hokejový	k2eAgMnSc1d1	hokejový
brankář	brankář	k1gMnSc1	brankář
Tomáš	Tomáš	k1gMnSc1	Tomáš
Dvořák	Dvořák	k1gMnSc1	Dvořák
(	(	kIx(	(
<g/>
*	*	kIx~	*
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
atlet	atlet	k1gMnSc1	atlet
Martin	Martin	k1gMnSc1	Martin
Hamrlík	Hamrlík	k1gMnSc1	Hamrlík
(	(	kIx(	(
<g/>
*	*	kIx~	*
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
Roman	Roman	k1gMnSc1	Roman
Hamrlík	Hamrlík	k1gMnSc1	Hamrlík
(	(	kIx(	(
<g/>
*	*	kIx~	*
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
Petr	Petr	k1gMnSc1	Petr
Čajánek	Čajánek	k1gMnSc1	Čajánek
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g />
.	.	kIx.	.
</s>
<s>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
Petr	Petr	k1gMnSc1	Petr
Janda	Janda	k1gMnSc1	Janda
(	(	kIx(	(
<g/>
*	*	kIx~	*
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
Jiří	Jiří	k1gMnSc1	Jiří
Novák	Novák	k1gMnSc1	Novák
(	(	kIx(	(
<g/>
*	*	kIx~	*
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tenista	tenista	k1gMnSc1	tenista
Adam	Adam	k1gMnSc1	Adam
Gebrian	Gebrian	k1gMnSc1	Gebrian
(	(	kIx(	(
<g/>
*	*	kIx~	*
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
<g/>
,	,	kIx,	,
teoretik	teoretik	k1gMnSc1	teoretik
architektury	architektura	k1gFnSc2	architektura
Karel	Karel	k1gMnSc1	Karel
Rachůnek	Rachůnek	k1gMnSc1	Rachůnek
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
Michal	Michal	k1gMnSc1	Michal
Smola	Smola	k1gMnSc1	Smola
(	(	kIx(	(
<g/>
*	*	kIx~	*
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
orientační	orientační	k2eAgMnSc1d1	orientační
běžec	běžec	k1gMnSc1	běžec
Adéla	Adéla	k1gFnSc1	Adéla
Sýkorová	Sýkorová	k1gFnSc1	Sýkorová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sportovní	sportovní	k2eAgFnSc1d1	sportovní
střelkyně	střelkyně	k1gFnSc1	střelkyně
<g/>
,	,	kIx,	,
olympijská	olympijský	k2eAgFnSc1d1	olympijská
medailistka	medailistka	k1gFnSc1	medailistka
Richard	Richard	k1gMnSc1	Richard
Henzler	Henzler	k1gMnSc1	Henzler
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
-	-	kIx~	-
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spolupracovník	spolupracovník	k1gMnSc1	spolupracovník
Tomáše	Tomáš	k1gMnSc2	Tomáš
Bati	Baťa	k1gMnSc2	Baťa
v	v	k7c6	v
raných	raný	k2eAgInPc6d1	raný
letech	let	k1gInPc6	let
jeho	on	k3xPp3gNnSc2	on
podnikání	podnikání	k1gNnSc2	podnikání
a	a	k8xC	a
průkopník	průkopník	k1gMnSc1	průkopník
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
výroby	výroba	k1gFnSc2	výroba
zdviží	zdviž	k1gFnPc2	zdviž
<g/>
,	,	kIx,	,
vynálezce	vynálezce	k1gMnSc1	vynálezce
výtahu	výtah	k1gInSc2	výtah
páternoster	páternoster	k1gInSc1	páternoster
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Albert	Albert	k1gMnSc1	Albert
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
-	-	kIx~	-
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
ředitel	ředitel	k1gMnSc1	ředitel
Baťovy	Baťův	k2eAgFnSc2d1	Baťova
nemocnice	nemocnice	k1gFnSc2	nemocnice
Jan	Jan	k1gMnSc1	Jan
Antonín	Antonín	k1gMnSc1	Antonín
Baťa	Baťa	k1gMnSc1	Baťa
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
-	-	kIx~	-
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podnikatel	podnikatel	k1gMnSc1	podnikatel
<g/>
,	,	kIx,	,
vlastník	vlastník	k1gMnSc1	vlastník
Baťových	Baťových	k2eAgInPc2d1	Baťových
závodů	závod	k1gInPc2	závod
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
pokračovatel	pokračovatel	k1gMnSc1	pokračovatel
Tomáše	Tomáš	k1gMnSc2	Tomáš
Bati	Baťa	k1gMnSc2	Baťa
Hermína	Hermína	k1gFnSc1	Hermína
Týrlová	Týrlová	k1gFnSc1	Týrlová
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1900	[number]	k4	1900
<g/>
-	-	kIx~	-
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režisérka	režisérka	k1gFnSc1	režisérka
a	a	k8xC	a
producentka	producentka	k1gFnSc1	producentka
ve	v	k7c6	v
Zlínských	zlínský	k2eAgInPc6d1	zlínský
filmových	filmový	k2eAgInPc6d1	filmový
ateliérech	ateliér	k1gInPc6	ateliér
Karel	Karel	k1gMnSc1	Karel
Zeman	Zeman	k1gMnSc1	Zeman
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
-	-	kIx~	-
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
producent	producent	k1gMnSc1	producent
ve	v	k7c6	v
Zlínských	zlínský	k2eAgInPc6d1	zlínský
filmových	filmový	k2eAgInPc6d1	filmový
ateliérech	ateliér	k1gInPc6	ateliér
<g/>
,	,	kIx,	,
žil	žít	k5eAaImAgMnS	žít
a	a	k8xC	a
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Novotný	Novotný	k1gMnSc1	Novotný
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
Zlínského	zlínský	k2eAgInSc2d1	zlínský
filmového	filmový	k2eAgInSc2d1	filmový
ateliéru	ateliér	k1gInSc2	ateliér
Otto	Otto	k1gMnSc1	Otto
Wichterle	Wichterl	k1gMnSc2	Wichterl
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
-	-	kIx~	-
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vědec	vědec	k1gMnSc1	vědec
<g/>
,	,	kIx,	,
pracoval	pracovat	k5eAaImAgMnS	pracovat
ve	v	k7c6	v
výzkumném	výzkumný	k2eAgInSc6d1	výzkumný
ústavu	ústav	k1gInSc6	ústav
Baťových	Baťových	k2eAgInPc2d1	Baťových
závodů	závod	k1gInPc2	závod
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Kovář	Kovář	k1gMnSc1	Kovář
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
-	-	kIx~	-
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
průmyslový	průmyslový	k2eAgMnSc1d1	průmyslový
designér	designér	k1gMnSc1	designér
a	a	k8xC	a
sochař	sochař	k1gMnSc1	sochař
Miroslav	Miroslav	k1gMnSc1	Miroslav
Zikmund	Zikmund	k1gMnSc1	Zikmund
(	(	kIx(	(
<g/>
*	*	kIx~	*
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cestovatel	cestovatel	k1gMnSc1	cestovatel
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
Jiří	Jiří	k1gMnSc1	Jiří
Hanzelka	Hanzelka	k1gMnSc1	Hanzelka
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
-	-	kIx~	-
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cestovatel	cestovatel	k1gMnSc1	cestovatel
<g/>
,	,	kIx,	,
žil	žít	k5eAaImAgMnS	žít
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Miler	Miler	k1gMnSc1	Miler
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
-	-	kIx~	-
<g/>
2011	[number]	k4	2011
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
kreslených	kreslený	k2eAgInPc2d1	kreslený
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
pracoval	pracovat	k5eAaImAgInS	pracovat
ve	v	k7c6	v
Zlínských	zlínský	k2eAgInPc6d1	zlínský
filmových	filmový	k2eAgInPc6d1	filmový
ateliérech	ateliér	k1gInPc6	ateliér
Emil	Emil	k1gMnSc1	Emil
Zátopek	Zátopek	k1gInSc1	Zátopek
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
-	-	kIx~	-
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
atlet	atlet	k1gMnSc1	atlet
<g/>
,	,	kIx,	,
olympijský	olympijský	k2eAgMnSc1d1	olympijský
vítěz	vítěz	k1gMnSc1	vítěz
<g/>
,	,	kIx,	,
žil	žít	k5eAaImAgMnS	žít
<g/>
,	,	kIx,	,
pracoval	pracovat	k5eAaImAgMnS	pracovat
a	a	k8xC	a
trénoval	trénovat	k5eAaImAgMnS	trénovat
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
Karel	Karel	k1gMnSc1	Karel
Pavlištík	Pavlištík	k1gMnSc1	Pavlištík
(	(	kIx(	(
<g/>
*	*	kIx~	*
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
folklorista	folklorista	k1gMnSc1	folklorista
<g/>
,	,	kIx,	,
muzejník	muzejník	k1gMnSc1	muzejník
Alois	Alois	k1gMnSc1	Alois
Skoumal	Skoumal	k1gMnSc1	Skoumal
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
-	-	kIx~	-
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
umělecký	umělecký	k2eAgMnSc1d1	umělecký
vedoucí	vedoucí	k1gMnSc1	vedoucí
záv	záv	k?	záv
<g/>
.	.	kIx.	.
klubu	klub	k1gInSc2	klub
<g/>
,	,	kIx,	,
muzikolog	muzikolog	k1gMnSc1	muzikolog
<g/>
,	,	kIx,	,
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
se	se	k3xPyFc4	se
o	o	k7c4	o
rozvoj	rozvoj	k1gInSc4	rozvoj
folkloru	folklor	k1gInSc2	folklor
na	na	k7c6	na
Zlínsku	Zlínsko	k1gNnSc6	Zlínsko
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Robert	Robert	k1gMnSc1	Robert
Býček	býček	k1gMnSc1	býček
(	(	kIx(	(
<g/>
*	*	kIx~	*
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
v	v	k7c6	v
kickboxu	kickbox	k1gInSc6	kickbox
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
</s>
