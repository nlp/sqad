<p>
<s>
Swing	swing	k1gInSc1	swing
je	být	k5eAaImIp3nS	být
rodinou	rodina	k1gFnSc7	rodina
tanců	tanec	k1gInPc2	tanec
vyvinutých	vyvinutý	k2eAgInPc2d1	vyvinutý
souběžně	souběžně	k6eAd1	souběžně
se	s	k7c7	s
swingovou	swingový	k2eAgFnSc7d1	swingová
hudbou	hudba	k1gFnSc7	hudba
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
.	.	kIx.	.
a	a	k8xC	a
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgFnPc1d3	nejstarší
formy	forma	k1gFnPc1	forma
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
starší	starý	k2eAgMnPc1d2	starší
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgInSc1d3	nejznámější
swingovým	swingový	k2eAgInSc7d1	swingový
tancem	tanec	k1gInSc7	tanec
je	být	k5eAaImIp3nS	být
lindy	linda	k1gFnPc4	linda
hop	hop	k0	hop
<g/>
,	,	kIx,	,
populární	populární	k2eAgInSc1d1	populární
párový	párový	k2eAgInSc1d1	párový
tanec	tanec	k1gInSc1	tanec
vzešlý	vzešlý	k2eAgInSc1d1	vzešlý
z	z	k7c2	z
Harlemu	Harlem	k1gInSc2	Harlem
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
tancuje	tancovat	k5eAaImIp3nS	tancovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
swingových	swingový	k2eAgInPc2d1	swingový
tanců	tanec	k1gInPc2	tanec
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgInPc4	svůj
kořeny	kořen	k1gInPc4	kořen
v	v	k7c6	v
afroamerických	afroamerický	k2eAgFnPc6d1	afroamerická
komunitách	komunita	k1gFnPc6	komunita
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
však	však	k9	však
vzešly	vzejít	k5eAaPmAgFnP	vzejít
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
kulturních	kulturní	k2eAgFnPc2d1	kulturní
tradic	tradice	k1gFnPc2	tradice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Formy	forma	k1gFnSc2	forma
swingu	swing	k1gInSc2	swing
==	==	k?	==
</s>
</p>
<p>
<s>
Mimo	mimo	k7c4	mimo
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
se	se	k3xPyFc4	se
swing	swing	k1gInSc1	swing
používá	používat	k5eAaImIp3nS	používat
všeobecně	všeobecně	k6eAd1	všeobecně
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
tance	tanec	k1gInPc4	tanec
swingové	swingový	k2eAgFnSc2d1	swingová
éry	éra	k1gFnSc2	éra
<g/>
:	:	kIx,	:
lindy	linda	k1gFnSc2	linda
hop	hop	k0	hop
<g/>
,	,	kIx,	,
charleston	charleston	k1gInSc4	charleston
<g/>
,	,	kIx,	,
shag	shag	k1gInSc4	shag
a	a	k8xC	a
balbou	balbý	k2eAgFnSc4d1	balbý
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skupina	skupina	k1gFnSc1	skupina
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
ještě	ještě	k6eAd1	ještě
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
o	o	k7c4	o
West	West	k2eAgInSc4d1	West
Coast	Coast	k1gInSc4	Coast
Swing	swing	k1gInSc1	swing
<g/>
,	,	kIx,	,
East	East	k2eAgInSc1d1	East
Coast	Coast	k1gInSc1	Coast
Swing	swing	k1gInSc1	swing
<g/>
,	,	kIx,	,
Hand	Hand	k1gInSc1	Hand
Dancing	dancing	k1gInSc1	dancing
<g/>
,	,	kIx,	,
jive	jive	k1gInSc1	jive
<g/>
,	,	kIx,	,
rock	rock	k1gInSc1	rock
and	and	k?	and
roll	roll	k1gInSc1	roll
<g/>
,	,	kIx,	,
moderní	moderní	k2eAgInSc1d1	moderní
jive	jive	k1gInSc1	jive
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
tance	tanec	k1gInPc1	tanec
vyvinuté	vyvinutý	k2eAgInPc1d1	vyvinutý
ve	v	k7c4	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
často	často	k6eAd1	často
díky	díky	k7c3	díky
tradici	tradice	k1gFnSc3	tradice
počítají	počítat	k5eAaImIp3nP	počítat
mezi	mezi	k7c4	mezi
swingové	swingový	k2eAgInPc4d1	swingový
tance	tanec	k1gInPc4	tanec
i	i	k8xC	i
soutěžní	soutěžní	k2eAgFnPc4d1	soutěžní
boogie	boogie	k1gFnPc4	boogie
woogie	woogie	k1gFnSc1	woogie
a	a	k8xC	a
rock	rock	k1gInSc1	rock
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
roll	rolla	k1gFnPc2	rolla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rané	raný	k2eAgFnPc1d1	raná
formy	forma	k1gFnPc1	forma
30	[number]	k4	30
<g/>
.	.	kIx.	.
a	a	k8xC	a
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
===	===	k?	===
</s>
</p>
<p>
<s>
Lindy	Linda	k1gFnPc1	Linda
hop	hop	k0	hop
</s>
</p>
<p>
<s>
Balboa	Balboa	k6eAd1	Balboa
</s>
</p>
<p>
<s>
Shag	Shag	k1gMnSc1	Shag
</s>
</p>
<p>
<s>
Collegiate	Collegiat	k1gMnSc5	Collegiat
Shag	Shag	k1gMnSc1	Shag
</s>
</p>
<p>
<s>
St.	st.	kA	st.
Louis	Louis	k1gMnSc1	Louis
Shag	Shag	k1gMnSc1	Shag
</s>
</p>
<p>
<s>
===	===	k?	===
Formy	forma	k1gFnSc2	forma
40	[number]	k4	40
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
===	===	k?	===
</s>
</p>
<p>
<s>
Lindy	Linda	k1gFnPc1	Linda
Charleston	charleston	k1gInSc1	charleston
</s>
</p>
<p>
<s>
Eastern	Eastern	k1gInSc1	Eastern
Swing	swing	k1gInSc1	swing
<g/>
,	,	kIx,	,
variace	variace	k1gFnPc1	variace
foxtrotu	foxtrot	k1gInSc2	foxtrot
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
East	East	k2eAgInSc1d1	East
Coast	Coast	k1gInSc1	Coast
Swing	swing	k1gInSc1	swing
</s>
</p>
<p>
<s>
West	West	k2eAgInSc1d1	West
Coast	Coast	k1gInSc1	Coast
Swing	swing	k1gInSc1	swing
</s>
</p>
<p>
<s>
Western	Western	kA	Western
Swing	swing	k1gInSc1	swing
</s>
</p>
<p>
<s>
Boogie-woogie	boogieoogie	k1gNnSc1	boogie-woogie
</s>
</p>
<p>
<s>
Carolina	Carolina	k1gFnSc1	Carolina
Shag	Shaga	k1gFnPc2	Shaga
</s>
</p>
<p>
<s>
Imperial	Imperial	k1gInSc1	Imperial
Swing	swing	k1gInSc1	swing
</s>
</p>
<p>
<s>
Jive	jive	k1gInSc1	jive
</s>
</p>
<p>
<s>
Skip	skip	k1gInSc1	skip
jive	jive	k1gInSc1	jive
</s>
</p>
<p>
<s>
moderní	moderní	k2eAgInSc1d1	moderní
Jive	jive	k1gInSc1	jive
-	-	kIx~	-
také	také	k9	také
znám	znát	k5eAaImIp1nS	znát
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
LeRoc	LeRoc	k1gFnSc1	LeRoc
a	a	k8xC	a
Ceroc	Ceroc	k1gFnSc1	Ceroc
</s>
</p>
<p>
<s>
Rock	rock	k1gInSc1	rock
and	and	k?	and
Roll	Roll	k1gInSc1	Roll
</s>
</p>
<p>
<s>
Hand	Hand	k6eAd1	Hand
dancing	dancing	k1gInSc1	dancing
</s>
</p>
<p>
<s>
Push	Push	k1gMnSc1	Push
a	a	k8xC	a
Whip	Whip	k1gMnSc1	Whip
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Tanec	tanec	k1gInSc1	tanec
</s>
</p>
<p>
<s>
Swing	swing	k1gInSc1	swing
(	(	kIx(	(
<g/>
hudba	hudba	k1gFnSc1	hudba
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Swing	swing	k1gInSc1	swing
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
