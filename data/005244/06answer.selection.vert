<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
příčin	příčina	k1gFnPc2	příčina
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nedostatek	nedostatek	k1gInSc4	nedostatek
jódu	jód	k1gInSc2	jód
ve	v	k7c6	v
stravě	strava	k1gFnSc6	strava
matky	matka	k1gFnSc2	matka
během	během	k7c2	během
těhotenství	těhotenství	k1gNnSc2	těhotenství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
nedostatečná	dostatečný	k2eNgFnSc1d1	nedostatečná
funkce	funkce	k1gFnSc1	funkce
štítné	štítný	k2eAgFnSc2d1	štítná
žlázy	žláza	k1gFnSc2	žláza
matky	matka	k1gFnSc2	matka
z	z	k7c2	z
jiného	jiný	k2eAgInSc2d1	jiný
důvodu	důvod	k1gInSc2	důvod
<g/>
.	.	kIx.	.
</s>
