<p>
<s>
Den	den	k1gInSc1	den
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Independence	Independence	k1gFnSc1	Independence
Day	Day	k1gFnSc1	Day
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
v	v	k7c6	v
USA	USA	kA	USA
známý	známý	k2eAgInSc1d1	známý
spíše	spíše	k9	spíše
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
the	the	k?	the
Fourth	Fourth	k1gInSc1	Fourth
of	of	k?	of
July	Jula	k1gFnSc2	Jula
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
July	Jula	k1gFnSc2	Jula
Fourth	Fourth	k1gInSc1	Fourth
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
státní	státní	k2eAgInSc4d1	státní
svátek	svátek	k1gInSc4	svátek
oslavující	oslavující	k2eAgNnSc1d1	oslavující
přijetí	přijetí	k1gNnSc1	přijetí
Deklarace	deklarace	k1gFnSc2	deklarace
nezávislosti	nezávislost	k1gFnSc2	nezávislost
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1776	[number]	k4	1776
Druhým	druhý	k4xOgInSc7	druhý
kontinentálním	kontinentální	k2eAgInSc7d1	kontinentální
kongresem	kongres	k1gInSc7	kongres
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
deklarací	deklarace	k1gFnSc7	deklarace
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
uplatnily	uplatnit	k5eAaPmAgFnP	uplatnit
svou	svůj	k3xOyFgFnSc4	svůj
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
vyhlášením	vyhlášení	k1gNnSc7	vyhlášení
základních	základní	k2eAgNnPc2d1	základní
práv	právo	k1gNnPc2	právo
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
soupisem	soupis	k1gInSc7	soupis
stížností	stížnost	k1gFnPc2	stížnost
proti	proti	k7c3	proti
britské	britský	k2eAgFnSc3d1	britská
říši	říš	k1gFnSc3	říš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historické	historický	k2eAgNnSc4d1	historické
pozadí	pozadí	k1gNnSc4	pozadí
==	==	k?	==
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1776	[number]	k4	1776
schválil	schválit	k5eAaPmAgInS	schválit
Druhý	druhý	k4xOgInSc4	druhý
kontinentální	kontinentální	k2eAgInSc4d1	kontinentální
kongres	kongres	k1gInSc4	kongres
Rezoluci	rezoluce	k1gFnSc4	rezoluce
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
oddělení	oddělení	k1gNnSc1	oddělení
13	[number]	k4	13
původních	původní	k2eAgFnPc2d1	původní
amerických	americký	k2eAgFnPc2d1	americká
kolonií	kolonie	k1gFnPc2	kolonie
z	z	k7c2	z
nadvlády	nadvláda	k1gFnSc2	nadvláda
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
rezoluce	rezoluce	k1gFnSc2	rezoluce
byl	být	k5eAaImAgMnS	být
Richard	Richard	k1gMnSc1	Richard
Henry	henry	k1gInSc2	henry
Lee	Lea	k1gFnSc3	Lea
a	a	k8xC	a
rezoluce	rezoluce	k1gFnSc1	rezoluce
informuje	informovat	k5eAaBmIp3nS	informovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
kolonie	kolonie	k1gFnPc1	kolonie
již	již	k6eAd1	již
nejsou	být	k5eNaImIp3nP	být
pod	pod	k7c7	pod
britskou	britský	k2eAgFnSc7d1	britská
nadvládou	nadvláda	k1gFnSc7	nadvláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dva	dva	k4xCgInPc1	dva
dny	den	k1gInPc1	den
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1776	[number]	k4	1776
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
schválena	schválit	k5eAaPmNgFnS	schválit
i	i	k9	i
Deklarace	deklarace	k1gFnSc1	deklarace
nezávislosti	nezávislost	k1gFnSc2	nezávislost
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
hlavního	hlavní	k2eAgMnSc2d1	hlavní
autora	autor	k1gMnSc2	autor
deklarace	deklarace	k1gFnSc2	deklarace
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
třetí	třetí	k4xOgMnSc1	třetí
prezident	prezident	k1gMnSc1	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
Thomas	Thomas	k1gMnSc1	Thomas
Jefferson	Jefferson	k1gMnSc1	Jefferson
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
deklarace	deklarace	k1gFnSc2	deklarace
bylo	být	k5eAaImAgNnS	být
zdůvodnění	zdůvodnění	k1gNnSc1	zdůvodnění
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vyhlášení	vyhlášení	k1gNnSc3	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
uznala	uznat	k5eAaPmAgFnS	uznat
samostatnost	samostatnost	k1gFnSc4	samostatnost
USA	USA	kA	USA
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1783	[number]	k4	1783
Pařížským	pařížský	k2eAgInSc7d1	pařížský
mírem	mír	k1gInSc7	mír
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
byla	být	k5eAaImAgFnS	být
ukončena	ukončit	k5eAaPmNgFnS	ukončit
i	i	k9	i
válka	válka	k1gFnSc1	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Oslavy	oslava	k1gFnPc4	oslava
a	a	k8xC	a
zvyky	zvyk	k1gInPc4	zvyk
==	==	k?	==
</s>
</p>
<p>
<s>
Oficiálním	oficiální	k2eAgInSc7d1	oficiální
federálním	federální	k2eAgInSc7d1	federální
svátkem	svátek	k1gInSc7	svátek
byl	být	k5eAaImAgInS	být
Den	den	k1gInSc1	den
nezávislosti	nezávislost	k1gFnSc2	nezávislost
uznán	uznat	k5eAaPmNgInS	uznat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
v	v	k7c4	v
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
tak	tak	k6eAd1	tak
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgFnPc1	všechen
státní	státní	k2eAgFnPc1d1	státní
instituce	instituce	k1gFnPc1	instituce
uzavřeny	uzavřít	k5eAaPmNgFnP	uzavřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Američané	Američan	k1gMnPc1	Američan
oslavují	oslavovat	k5eAaImIp3nP	oslavovat
vznik	vznik	k1gInSc4	vznik
svobodných	svobodný	k2eAgInPc2d1	svobodný
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
přehlídkami	přehlídka	k1gFnPc7	přehlídka
<g/>
,	,	kIx,	,
společným	společný	k2eAgNnSc7d1	společné
grilováním	grilování	k1gNnSc7	grilování
masa	maso	k1gNnSc2	maso
<g/>
,	,	kIx,	,
celodenními	celodenní	k2eAgInPc7d1	celodenní
pikniky	piknik	k1gInPc7	piknik
<g/>
,	,	kIx,	,
baseballovými	baseballový	k2eAgInPc7d1	baseballový
zápasy	zápas	k1gInPc7	zápas
<g/>
,	,	kIx,	,
soutěžemi	soutěž	k1gFnPc7	soutěž
<g/>
,	,	kIx,	,
koncerty	koncert	k1gInPc7	koncert
a	a	k8xC	a
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
dne	den	k1gInSc2	den
velkolepými	velkolepý	k2eAgInPc7d1	velkolepý
ohňostroji	ohňostroj	k1gInPc7	ohňostroj
doprovázené	doprovázený	k2eAgInPc1d1	doprovázený
národní	národní	k2eAgFnSc7d1	národní
hudbou	hudba	k1gFnSc7	hudba
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
různými	různý	k2eAgFnPc7d1	různá
událostmi	událost	k1gFnPc7	událost
veřejnými	veřejný	k2eAgFnPc7d1	veřejná
i	i	k8xC	i
soukromými	soukromý	k2eAgFnPc7d1	soukromá
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
oslavují	oslavovat	k5eAaImIp3nP	oslavovat
historii	historie	k1gFnSc4	historie
<g/>
,	,	kIx,	,
vládu	vláda	k1gFnSc4	vláda
a	a	k8xC	a
tradice	tradice	k1gFnPc4	tradice
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Pyrotechnika	pyrotechnika	k1gFnSc1	pyrotechnika
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
koupit	koupit	k5eAaPmF	koupit
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
území	území	k1gNnSc2	území
USA	USA	kA	USA
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
státech	stát	k1gInPc6	stát
je	být	k5eAaImIp3nS	být
pyrotechnika	pyrotechnika	k1gFnSc1	pyrotechnika
zakázána	zakázat	k5eAaPmNgFnS	zakázat
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
omezována	omezován	k2eAgFnSc1d1	omezována
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
těchto	tento	k3xDgInPc2	tento
států	stát	k1gInPc2	stát
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
pyrotechnika	pyrotechnika	k1gFnSc1	pyrotechnika
pašuje	pašovat	k5eAaImIp3nS	pašovat
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
oslavy	oslava	k1gFnPc1	oslava
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
i	i	k9	i
další	další	k2eAgInPc4d1	další
dny	den	k1gInPc4	den
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
pokud	pokud	k8xS	pokud
4	[number]	k4	4
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc1	červenec
není	být	k5eNaImIp3nS	být
o	o	k7c6	o
víkendu	víkend	k1gInSc6	víkend
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
ale	ale	k8xC	ale
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
konkrétním	konkrétní	k2eAgInSc6d1	konkrétní
státu	stát	k1gInSc6	stát
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dekorace	dekorace	k1gFnPc1	dekorace
jsou	být	k5eAaImIp3nP	být
laděny	ladit	k5eAaImNgFnP	ladit
do	do	k7c2	do
trikolory	trikolora	k1gFnSc2	trikolora
(	(	kIx(	(
<g/>
červená	červený	k2eAgFnSc1d1	červená
<g/>
,	,	kIx,	,
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Nové	Nové	k2eAgFnSc6d1	Nové
Anglii	Anglie	k1gFnSc6	Anglie
staví	stavit	k5eAaImIp3nP	stavit
pyramidy	pyramida	k1gFnPc1	pyramida
ze	z	k7c2	z
sudů	sud	k1gInPc2	sud
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
po	po	k7c6	po
setmění	setmění	k1gNnSc6	setmění
zapalovány	zapalován	k2eAgInPc1d1	zapalován
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
týden	týden	k1gInSc4	týden
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
bývá	bývat	k5eAaImIp3nS	bývat
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejvíce	hodně	k6eAd3	hodně
vytížených	vytížený	k2eAgNnPc2d1	vytížené
období	období	k1gNnPc2	období
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
co	co	k9	co
se	se	k3xPyFc4	se
cestování	cestování	k1gNnSc1	cestování
týče	týkat	k5eAaImIp3nS	týkat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velvyslanectví	velvyslanectví	k1gNnSc1	velvyslanectví
USA	USA	kA	USA
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
hostí	hostit	k5eAaImIp3nP	hostit
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
Dne	den	k1gInSc2	den
nezávislosti	nezávislost	k1gFnSc2	nezávislost
tradiční	tradiční	k2eAgFnSc4d1	tradiční
recepci	recepce	k1gFnSc4	recepce
na	na	k7c6	na
rezidenci	rezidence	k1gFnSc6	rezidence
velvyslance	velvyslanec	k1gMnSc2	velvyslanec
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yQgFnSc4	který
jsou	být	k5eAaImIp3nP	být
zvány	zvát	k5eAaImNgFnP	zvát
významné	významný	k2eAgFnPc1d1	významná
osobnosti	osobnost	k1gFnPc1	osobnost
kulturního	kulturní	k2eAgInSc2d1	kulturní
i	i	k8xC	i
politického	politický	k2eAgInSc2d1	politický
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
komunismu	komunismus	k1gInSc2	komunismus
nechyběli	chybět	k5eNaImAgMnP	chybět
mezi	mezi	k7c7	mezi
pozvanými	pozvaný	k2eAgMnPc7d1	pozvaný
čeští	český	k2eAgMnPc1d1	český
disidenti	disident	k1gMnPc1	disident
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Independence	Independence	k1gFnSc2	Independence
Day	Day	k1gMnSc1	Day
United_States	United_States	k1gMnSc1	United_States
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Patriot	patriot	k1gMnSc1	patriot
day	day	k?	day
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Den	dna	k1gFnPc2	dna
nezávislosti	nezávislost	k1gFnSc2	nezávislost
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Den	den	k1gInSc1	den
nezávislosti	nezávislost	k1gFnSc2	nezávislost
–	–	k?	–
Velvyslanectví	velvyslanectví	k1gNnSc1	velvyslanectví
USA	USA	kA	USA
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
