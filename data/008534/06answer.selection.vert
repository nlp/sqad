<s>
Den	den	k1gInSc1	den
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Independence	Independence	k1gFnSc1	Independence
Day	Day	k1gFnSc1	Day
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
v	v	k7c6	v
USA	USA	kA	USA
známý	známý	k2eAgInSc1d1	známý
spíše	spíše	k9	spíše
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
the	the	k?	the
Fourth	Fourth	k1gInSc1	Fourth
of	of	k?	of
July	Jula	k1gFnSc2	Jula
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
July	Jula	k1gFnSc2	Jula
Fourth	Fourth	k1gInSc1	Fourth
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
státní	státní	k2eAgInSc4d1	státní
svátek	svátek	k1gInSc4	svátek
oslavující	oslavující	k2eAgNnSc1d1	oslavující
přijetí	přijetí	k1gNnSc1	přijetí
Deklarace	deklarace	k1gFnSc2	deklarace
nezávislosti	nezávislost	k1gFnSc2	nezávislost
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1776	[number]	k4	1776
Druhým	druhý	k4xOgInSc7	druhý
kontinentálním	kontinentální	k2eAgInSc7d1	kontinentální
kongresem	kongres	k1gInSc7	kongres
<g/>
.	.	kIx.	.
</s>
