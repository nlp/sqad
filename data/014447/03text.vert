<s>
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
</s>
<s>
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
logoNázev	logoNázet	k5eAaPmDgInS
</s>
<s>
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
Club	club	k1gInSc1
de	de	k?
Fútbol	Fútbol	k1gInSc1
Přezdívka	přezdívka	k1gFnSc1
</s>
<s>
Bílý	bílý	k2eAgInSc1d1
balet	balet	k1gInSc1
(	(	kIx(
<g/>
Los	los	k1gInSc1
Blancos	Blancos	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Pusinky	pusinka	k1gFnPc1
(	(	kIx(
<g/>
Los	los	k1gInSc1
Merengues	Merengues	k1gInSc1
<g/>
)	)	kIx)
Země	země	k1gFnSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
Španělsko	Španělsko	k1gNnSc1
Město	město	k1gNnSc1
</s>
<s>
Madrid	Madrid	k1gInSc1
Založen	založen	k2eAgInSc1d1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1902	#num#	k4
Asociace	asociace	k1gFnSc2
</s>
<s>
RFEF	RFEF	kA
Barvy	barva	k1gFnPc1
</s>
<s>
•	•	k?
bílá	bílý	k2eAgFnSc1d1
<g/>
,	,	kIx,
modrá	modrý	k2eAgFnSc1d1
a	a	k8xC
černá	černý	k2eAgFnSc1d1
(	(	kIx(
;	;	kIx,
1902	#num#	k4
<g/>
–	–	k?
<g/>
1911	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
•	•	k?
bílá	bílý	k2eAgFnSc1d1
a	a	k8xC
modrá	modrý	k2eAgFnSc1d1
(	(	kIx(
;	;	kIx,
1911	#num#	k4
<g/>
–	–	k?
<g/>
1925	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
•	•	k?
bílá	bílý	k2eAgFnSc1d1
a	a	k8xC
černá	černý	k2eAgFnSc1d1
(	(	kIx(
;	;	kIx,
1925	#num#	k4
<g/>
–	–	k?
<g/>
1926	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
•	•	k?
bílá	bílý	k2eAgFnSc1d1
a	a	k8xC
modrá	modrý	k2eAgFnSc1d1
(	(	kIx(
;	;	kIx,
od	od	k7c2
1926	#num#	k4
<g/>
,	,	kIx,
dresy	dres	k1gInPc4
bílé	bílý	k2eAgNnSc1d1
od	od	k7c2
1955	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_realmadrid	_realmadrid	k1gInSc1
<g/>
2021	#num#	k4
<g/>
h	h	k?
<g/>
|	|	kIx~
<g/>
link	link	k1gMnSc1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
Domácí	domácí	k2eAgInSc1d1
dres	dres	k1gInSc1
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_realmadrid	_realmadrid	k1gInSc1
<g/>
2021	#num#	k4
<g/>
a	a	k8xC
<g/>
|	|	kIx~
<g/>
link	link	k6eAd1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
Venkovní	venkovní	k2eAgInSc4d1
dres	dres	k1gInSc4
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_realmadridcf	_realmadridcf	k1gInSc1
<g/>
2021	#num#	k4
<g/>
t	t	k?
<g/>
|	|	kIx~
<g/>
link	link	k1gMnSc1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
Alternativní	alternativní	k2eAgInPc1d1
</s>
<s>
Soutěž	soutěž	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
španělská	španělský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
2020	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc1
Stadion	stadion	k1gNnSc1
</s>
<s>
Santiago	Santiago	k1gNnSc1
Bernabéu	Bernabéus	k1gInSc2
<g/>
,	,	kIx,
Madrid	Madrid	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
40	#num#	k4
<g/>
°	°	k?
<g/>
27	#num#	k4
<g/>
′	′	k?
<g/>
11	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
3	#num#	k4
<g/>
°	°	k?
<g/>
41	#num#	k4
<g/>
′	′	k?
<g/>
18	#num#	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
Kapacita	kapacita	k1gFnSc1
</s>
<s>
81	#num#	k4
0	#num#	k4
<g/>
44	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Vedení	vedení	k1gNnSc1
Předseda	předseda	k1gMnSc1
</s>
<s>
Florentino	Florentin	k2eAgNnSc1d1
Pérez	Péreza	k1gFnPc2
Trenér	trenér	k1gMnSc1
</s>
<s>
Zinédine	Zinédin	k1gMnSc5
Zidane	Zidan	k1gMnSc5
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1
webová	webový	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
Největší	veliký	k2eAgInPc1d3
úspěchy	úspěch	k1gInPc1
Ligové	ligový	k2eAgInPc1d1
tituly	titul	k1gInPc1
</s>
<s>
34	#num#	k4
<g/>
×	×	k?
mistr	mistr	k1gMnSc1
Španělska	Španělsko	k1gNnSc2
(	(	kIx(
<g/>
1931	#num#	k4
<g/>
/	/	kIx~
<g/>
32	#num#	k4
<g/>
,	,	kIx,
1932	#num#	k4
<g/>
/	/	kIx~
<g/>
33	#num#	k4
<g/>
,	,	kIx,
1953	#num#	k4
<g/>
/	/	kIx~
<g/>
54	#num#	k4
<g/>
,	,	kIx,
1954	#num#	k4
<g/>
/	/	kIx~
<g/>
55	#num#	k4
<g/>
,	,	kIx,
1956	#num#	k4
<g/>
/	/	kIx~
<g/>
57	#num#	k4
<g/>
,	,	kIx,
1957	#num#	k4
<g/>
/	/	kIx~
<g/>
58	#num#	k4
<g/>
,	,	kIx,
1960	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
/	/	kIx~
<g/>
61	#num#	k4
<g/>
,	,	kIx,
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
62	#num#	k4
<g/>
,	,	kIx,
1962	#num#	k4
<g/>
/	/	kIx~
<g/>
63	#num#	k4
<g/>
,	,	kIx,
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
64	#num#	k4
<g/>
,	,	kIx,
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
<g/>
,	,	kIx,
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
<g/>
,	,	kIx,
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
<g/>
,	,	kIx,
1968	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
69	#num#	k4
<g/>
,	,	kIx,
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
<g/>
,	,	kIx,
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
<g/>
,	,	kIx,
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
<g/>
,	,	kIx,
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
<g/>
,	,	kIx,
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
79	#num#	k4
<g/>
,	,	kIx,
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
86	#num#	k4
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
<g/>
)	)	kIx)
Domácí	domácí	k2eAgFnSc2d1
trofeje	trofej	k1gFnSc2
</s>
<s>
19	#num#	k4
<g/>
×	×	k?
Copa	Cop	k1gInSc2
del	del	k?
Rey	Rea	k1gFnSc2
<g/>
11	#num#	k4
<g/>
×	×	k?
Supercopa	Supercopa	k1gFnSc1
de	de	k?
Españ	Españ	k1gFnSc1
<g/>
1	#num#	k4
<g/>
×	×	k?
Copa	Cop	k1gInSc2
de	de	k?
la	la	k1gNnSc1
Liga	liga	k1gFnSc1
Mezinárodní	mezinárodní	k2eAgFnSc2d1
trofeje	trofej	k1gFnSc2
</s>
<s>
13	#num#	k4
<g/>
×	×	k?
PMEZ	PMEZ	kA
/	/	kIx~
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
<g/>
2	#num#	k4
<g/>
×	×	k?
Pohár	pohár	k1gInSc1
/	/	kIx~
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
<g/>
4	#num#	k4
<g/>
×	×	k?
Superpohár	superpohár	k1gInSc4
UEFA	UEFA	kA
<g/>
3	#num#	k4
<g/>
×	×	k?
Interkontinentální	interkontinentální	k2eAgInSc1d1
pohár	pohár	k1gInSc1
<g/>
4	#num#	k4
<g/>
×	×	k?
MS	MS	kA
ve	v	k7c6
fotbale	fotbal	k1gInSc6
klubů	klub	k1gInPc2
<g/>
2	#num#	k4
<g/>
×	×	k?
Latinský	latinský	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
(	(	kIx(
<g/>
celým	celý	k2eAgInSc7d1
názvem	název	k1gInSc7
<g/>
:	:	kIx,
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
Club	club	k1gInSc1
de	de	k?
Fútbol	Fútbol	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
španělský	španělský	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
sídlí	sídlet	k5eAaImIp3nS
v	v	k7c6
Madridu	Madrid	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
sezóny	sezóna	k1gFnSc2
1929	#num#	k4
hraje	hrát	k5eAaImIp3nS
v	v	k7c6
Primera	primera	k1gFnSc1
División	División	k1gMnSc1
<g/>
,	,	kIx,
španělské	španělský	k2eAgFnSc6d1
nejvyšší	vysoký	k2eAgFnSc6d3
fotbalové	fotbalový	k2eAgFnSc6d1
soutěži	soutěž	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největšími	veliký	k2eAgMnPc7d3
rivaly	rival	k1gMnPc7
Realu	Real	k1gInSc2
Madrid	Madrid	k1gInSc1
jsou	být	k5eAaImIp3nP
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
a	a	k8xC
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
fotbalu	fotbal	k1gInSc2
má	mít	k5eAaImIp3nS
klub	klub	k1gInSc1
také	také	k9
oddíly	oddíl	k1gInPc4
pro	pro	k7c4
basketbal	basketbal	k1gInSc4
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
článek	článek	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tenis	tenis	k1gInSc4
<g/>
,	,	kIx,
stolní	stolní	k2eAgInSc4d1
tenis	tenis	k1gInSc4
a	a	k8xC
vodní	vodní	k2eAgNnSc4d1
pólo	pólo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
přezdívku	přezdívka	k1gFnSc4
„	„	k?
<g/>
bílý	bílý	k2eAgInSc1d1
balet	balet	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Klub	klub	k1gInSc1
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1902	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
právo	právo	k1gNnSc1
nosit	nosit	k5eAaImF
titul	titul	k1gInSc4
Real	Real	k1gInSc1
(	(	kIx(
<g/>
královský	královský	k2eAgMnSc1d1
<g/>
)	)	kIx)
mu	on	k3xPp3gMnSc3
bylo	být	k5eAaImAgNnS
uděleno	udělit	k5eAaPmNgNnS
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
1920	#num#	k4
králem	král	k1gMnSc7
Alfonsem	Alfons	k1gMnSc7
XIII	XIII	kA
(	(	kIx(
<g/>
v	v	k7c6
letech	let	k1gInPc6
1931	#num#	k4
<g/>
–	–	k?
<g/>
40	#num#	k4
slovo	slovo	k1gNnSc4
Real	Real	k1gInSc1
odebráno	odebrat	k5eAaPmNgNnS
z	z	k7c2
důvodu	důvod	k1gInSc2
existence	existence	k1gFnSc2
Druhé	druhý	k4xOgFnSc2
Španělské	španělský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Poslední	poslední	k2eAgFnSc1d1
kosmetická	kosmetický	k2eAgFnSc1d1
změna	změna	k1gFnSc1
názvu	název	k1gInSc2
přišla	přijít	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1941	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
bylo	být	k5eAaImAgNnS
z	z	k7c2
názvu	název	k1gInSc2
odstraněno	odstraněn	k2eAgNnSc1d1
anglické	anglický	k2eAgNnSc1d1
Football	Footballa	k1gFnPc2
Club	club	k1gInSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
bylo	být	k5eAaImAgNnS
nahrazeno	nahradit	k5eAaPmNgNnS
španělským	španělský	k2eAgInSc7d1
Club	club	k1gInSc4
de	de	k?
Fútbol	Fútbol	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
tak	tak	k9
z	z	k7c2
důvodu	důvod	k1gInSc2
pečlivého	pečlivý	k2eAgNnSc2d1
frankistického	frankistický	k2eAgNnSc2d1
potírání	potírání	k1gNnSc2
všech	všecek	k3xTgInPc2
anglicismů	anglicismus	k1gInPc2
v	v	k7c6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Real	Real	k1gInSc1
hraje	hrát	k5eAaImIp3nS
na	na	k7c6
stadionu	stadion	k1gInSc6
Santiago	Santiago	k1gNnSc4
Bernabéu	Bernabéus	k1gInSc2
v	v	k7c6
Madridu	Madrid	k1gInSc6
s	s	k7c7
kapacitou	kapacita	k1gFnSc7
81	#num#	k4
044	#num#	k4
diváků	divák	k1gMnPc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1947	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klubovou	klubový	k2eAgFnSc4d1
historii	historie	k1gFnSc4
zdobí	zdobit	k5eAaImIp3nS
mnoho	mnoho	k4c1
titulů	titul	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Real	Real	k1gInSc1
vyhrál	vyhrát	k5eAaPmAgInS
třináctkrát	třináctkrát	k6eAd1
Ligu	liga	k1gFnSc4
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tedy	tedy	k9
vícekrát	vícekrát	k6eAd1
než	než	k8xS
kterýkoliv	kterýkoliv	k3yIgInSc1
jiný	jiný	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
v	v	k7c6
celé	celý	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
18	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2000	#num#	k4
zvolila	zvolit	k5eAaPmAgFnS
FIFA	FIFA	kA
Real	Real	k1gInSc4
Madrid	Madrid	k1gInSc1
za	za	k7c4
nejlepší	dobrý	k2eAgInSc4d3
fotbalový	fotbalový	k2eAgInSc4d1
klub	klub	k1gInSc4
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Fotbal	fotbal	k1gInSc4
přivedli	přivést	k5eAaPmAgMnP
do	do	k7c2
Madridu	Madrid	k1gInSc6
profesoři	profesor	k1gMnPc1
a	a	k8xC
studenti	student	k1gMnPc1
Institución	Institución	k1gMnSc1
Libre	Libr	k1gInSc5
de	de	k?
Enseñ	Enseñ	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1895	#num#	k4
založili	založit	k5eAaPmAgMnP
klub	klub	k1gInSc1
Football	Football	k1gMnSc1
Sky	Sky	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
každé	každý	k3xTgNnSc4
nedělní	nedělní	k2eAgNnSc4d1
ráno	ráno	k1gNnSc4
hrál	hrát	k5eAaImAgMnS
v	v	k7c6
Moncloe	Moncloe	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1900	#num#	k4
se	se	k3xPyFc4
tento	tento	k3xDgInSc1
klub	klub	k1gInSc1
rozpadl	rozpadnout	k5eAaPmAgInS
na	na	k7c4
dva	dva	k4xCgInPc4
různé	různý	k2eAgInPc4d1
kluby	klub	k1gInPc4
New	New	k1gFnSc2
Foot-Ball	Foot-Ball	k1gInSc1
de	de	k?
Madrid	Madrid	k1gInSc1
a	a	k8xC
Club	club	k1gInSc1
Españ	Españ	k1gInSc1
de	de	k?
Madrid	Madrid	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prezidentem	prezident	k1gMnSc7
pozdějších	pozdní	k2eAgInPc2d2
klubů	klub	k1gInPc2
byl	být	k5eAaImAgMnS
Julián	Julián	k1gMnSc1
Palacios	Palacios	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1902	#num#	k4
se	se	k3xPyFc4
klub	klub	k1gInSc1
rozpadl	rozpadnout	k5eAaPmAgInS
znova	znova	k6eAd1
na	na	k7c4
Sociedad	Sociedad	k1gInSc4
Madrid	Madrid	k1gInSc1
Foot-ball	Foot-ball	k1gInSc1
Club	club	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgMnSc7
prezidentem	prezident	k1gMnSc7
klubu	klub	k1gInSc2
byl	být	k5eAaImAgMnS
Juan	Juan	k1gMnSc1
Padrós	Padrósa	k1gFnPc2
Rubió	Rubió	k1gMnSc1
<g/>
,	,	kIx,
prvním	první	k4xOgInSc7
sekretářem	sekretář	k1gInSc7
klubu	klub	k1gInSc2
byl	být	k5eAaImAgMnS
Manuel	Manuel	k1gMnSc1
Mendía	Mendía	k1gMnSc1
a	a	k8xC
prvním	první	k4xOgMnSc6
klubovým	klubový	k2eAgMnSc7d1
pokladníkem	pokladník	k1gMnSc7
byl	být	k5eAaImAgMnS
José	Josá	k1gFnSc2
de	de	k?
Gorostizaga	Gorostizaga	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1905	#num#	k4
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
po	po	k7c6
založení	založení	k1gNnSc6
<g/>
,	,	kIx,
už	už	k6eAd1
Sociedad	Sociedad	k1gInSc1
Madrid	Madrid	k1gInSc1
Foot-ball	Foot-ball	k1gInSc1
Club	club	k1gInSc1
vyhrál	vyhrát	k5eAaPmAgInS
první	první	k4xOgFnSc7
ze	z	k7c2
čtyř	čtyři	k4xCgFnPc2
za	za	k7c2
sebou	se	k3xPyFc7
jdoucích	jdoucí	k2eAgMnPc2d1
titulů	titul	k1gInPc2
Copa	Cop	k1gInSc2
del	del	k?
Rey	Rea	k1gFnSc2
(	(	kIx(
<g/>
španělský	španělský	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1920	#num#	k4
se	se	k3xPyFc4
název	název	k1gInSc1
klubu	klub	k1gInSc2
změnil	změnit	k5eAaPmAgInS
se	s	k7c7
svolením	svolení	k1gNnSc7
krále	král	k1gMnSc2
na	na	k7c4
Real	Real	k1gInSc4
Madrid	Madrid	k1gInSc1
Club	club	k1gInSc1
de	de	k?
Fútbol	Fútbol	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Éra	éra	k1gFnSc1
Santiaga	Santiago	k1gNnSc2
Bernabéa	Bernabéus	k1gMnSc2
a	a	k8xC
Alfreda	Alfred	k1gMnSc2
Di	Di	k1gMnSc2
Stefana	Stefan	k1gMnSc2
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1905	#num#	k4
odehrál	odehrát	k5eAaPmAgInS
svůj	svůj	k3xOyFgInSc4
první	první	k4xOgInSc4
soutěžní	soutěžní	k2eAgInSc4d1
zápas	zápas	k1gInSc4
za	za	k7c4
Real	Real	k1gInSc4
Madrid	Madrid	k1gInSc4
tehdy	tehdy	k6eAd1
sedmnáctiletý	sedmnáctiletý	k2eAgInSc1d1
Santiago	Santiago	k1gNnSc4
Bernabéu	Bernabéus	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
později	pozdě	k6eAd2
stal	stát	k5eAaPmAgInS
legendou	legenda	k1gFnSc7
klubu	klub	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bernabéu	Bernabéus	k1gInSc2
odehrál	odehrát	k5eAaPmAgMnS
v	v	k7c6
Realu	Real	k1gInSc6
celou	celý	k2eAgFnSc4d1
svou	svůj	k3xOyFgFnSc4
fotbalovou	fotbalový	k2eAgFnSc4d1
kariéru	kariéra	k1gFnSc4
a	a	k8xC
byl	být	k5eAaImAgMnS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
nejlepších	dobrý	k2eAgMnPc2d3
hráčů	hráč	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
ve	v	k7c6
Španělsku	Španělsko	k1gNnSc6
hrávali	hrávat	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
ukončení	ukončení	k1gNnSc6
své	svůj	k3xOyFgFnSc2
fotbalové	fotbalový	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
ihned	ihned	k6eAd1
úředníkem	úředník	k1gMnSc7
klubu	klub	k1gInSc2
a	a	k8xC
Real	Real	k1gInSc1
začal	začít	k5eAaPmAgInS
vzkvétat	vzkvétat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1920	#num#	k4
byl	být	k5eAaImAgInS
tehdejší	tehdejší	k2eAgInSc1d1
Sociedad	Sociedad	k1gInSc1
Madrid	Madrid	k1gInSc1
Foot	Foot	k2eAgInSc1d1
Ball	Ball	k1gInSc1
Club	club	k1gInSc1
přejmenován	přejmenován	k2eAgInSc1d1
na	na	k7c4
královský	královský	k2eAgInSc4d1
Real	Real	k1gInSc4
Madrid	Madrid	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bernabéu	Bernabéus	k1gInSc2
měl	mít	k5eAaImAgMnS
v	v	k7c6
hlavě	hlava	k1gFnSc6
obrovskou	obrovský	k2eAgFnSc4d1
vizi	vize	k1gFnSc4
a	a	k8xC
chtěl	chtít	k5eAaImAgMnS
vybudovat	vybudovat	k5eAaPmF
nejlepší	dobrý	k2eAgInSc4d3
tým	tým	k1gInSc4
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Jeden	jeden	k4xCgInSc1
z	z	k7c2
nejvýznamnějších	významný	k2eAgInPc2d3
plánů	plán	k1gInPc2
Bernabéa	Bernabéum	k1gNnSc2
byla	být	k5eAaImAgFnS
samozřejmě	samozřejmě	k6eAd1
stavba	stavba	k1gFnSc1
nového	nový	k2eAgInSc2d1
stadionu	stadion	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgInSc4d3
přestup	přestup	k1gInSc4
za	za	k7c2
jeho	jeho	k3xOp3gFnPc2
dob	doba	k1gFnPc2
byl	být	k5eAaImAgInS
jednoznačně	jednoznačně	k6eAd1
příchod	příchod	k1gInSc1
Alfreda	Alfred	k1gMnSc2
di	di	k?
Stéfana	Stéfan	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
legendou	legenda	k1gFnSc7
klubu	klub	k1gInSc2
a	a	k8xC
nejlepším	dobrý	k2eAgMnSc7d3
hráčem	hráč	k1gMnSc7
v	v	k7c6
historii	historie	k1gFnSc6
klubu	klub	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Di	Di	k1gFnSc1
Stéfano	Stéfana	k1gFnSc5
přišel	přijít	k5eAaPmAgMnS
do	do	k7c2
Realu	Real	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1953	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okolo	okolo	k7c2
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
přestupu	přestup	k1gInSc2
bylo	být	k5eAaImAgNnS
plno	plno	k1gNnSc1
spekulací	spekulace	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
di	di	k?
Stéfano	Stéfana	k1gFnSc5
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
právoplatně	právoplatně	k6eAd1
stal	stát	k5eAaPmAgMnS
hráčem	hráč	k1gMnSc7
Realu	Real	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Ihned	ihned	k6eAd1
v	v	k7c6
první	první	k4xOgFnSc6
sezóně	sezóna	k1gFnSc6
s	s	k7c7
di	di	k?
Stéfanem	Stéfan	k1gMnSc7
v	v	k7c6
týmu	tým	k1gInSc6
ukončil	ukončit	k5eAaPmAgMnS
klub	klub	k1gInSc4
chudé	chudý	k2eAgNnSc4d1
období	období	k1gNnSc4
a	a	k8xC
dokázal	dokázat	k5eAaPmAgMnS
vyhrát	vyhrát	k5eAaPmF
titul	titul	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Di	Di	k1gFnSc1
Stéfano	Stéfana	k1gFnSc5
se	se	k3xPyFc4
pochopitelně	pochopitelně	k6eAd1
stal	stát	k5eAaPmAgInS
nejlepším	dobrý	k2eAgMnSc7d3
střelcem	střelec	k1gMnSc7
klubu	klub	k1gInSc2
i	i	k8xC
soutěže	soutěž	k1gFnSc2
<g/>
,	,	kIx,
když	když	k8xS
dokázal	dokázat	k5eAaPmAgInS
vstřelit	vstřelit	k5eAaPmF
29	#num#	k4
branek	branka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Kolem	kolem	k7c2
roku	rok	k1gInSc2
1955	#num#	k4
tlačil	tlačit	k5eAaImAgMnS
Bernabéu	Bernabéa	k1gFnSc4
na	na	k7c4
organizaci	organizace	k1gFnSc4
evropského	evropský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
Real	Real	k1gInSc1
mohl	moct	k5eAaImAgInS
konečně	konečně	k6eAd1
svou	svůj	k3xOyFgFnSc4
sílu	síla	k1gFnSc4
ukázat	ukázat	k5eAaPmF
i	i	k9
proti	proti	k7c3
evropským	evropský	k2eAgInPc3d1
celkům	celek	k1gInPc3
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Realu	Real	k1gInSc6
se	se	k3xPyFc4
zrodil	zrodit	k5eAaPmAgInS
nejlepší	dobrý	k2eAgInSc1d3
útok	útok	k1gInSc1
světa	svět	k1gInSc2
a	a	k8xC
s	s	k7c7
hráči	hráč	k1gMnPc7
jako	jako	k8xC,k8xS
Ferenc	Ferenc	k1gMnSc1
Puskás	Puskása	k1gFnPc2
(	(	kIx(
<g/>
1958	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Raymond	Raymond	k1gMnSc1
Kopa	kopa	k1gFnSc1
(	(	kIx(
<g/>
1956	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Francisco	Francisco	k6eAd1
Gento	Gento	k1gNnSc1
(	(	kIx(
<g/>
1953	#num#	k4
<g/>
)	)	kIx)
si	se	k3xPyFc3
vysloužil	vysloužit	k5eAaPmAgMnS
přezdívku	přezdívka	k1gFnSc4
„	„	k?
<g/>
Bílý	bílý	k2eAgInSc1d1
balet	balet	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1960	#num#	k4
Real	Real	k1gInSc1
vyhrál	vyhrát	k5eAaPmAgInS
popáté	popáté	k4xO
v	v	k7c6
řadě	řada	k1gFnSc6
Pohár	pohár	k1gInSc4
mistrů	mistr	k1gMnPc2
<g/>
,	,	kIx,
když	když	k8xS
si	se	k3xPyFc3
dokázal	dokázat	k5eAaPmAgMnS
poradit	poradit	k5eAaPmF
s	s	k7c7
Frankfurtem	Frankfurt	k1gInSc7
v	v	k7c6
poměru	poměr	k1gInSc6
7	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
finále	finále	k1gNnSc1
je	být	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
považováno	považován	k2eAgNnSc1d1
za	za	k7c4
jedno	jeden	k4xCgNnSc4
z	z	k7c2
nejlepších	dobrý	k2eAgInPc2d3
v	v	k7c6
historii	historie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
Real	Real	k1gInSc4
se	se	k3xPyFc4
čtyřikrát	čtyřikrát	k6eAd1
trefil	trefit	k5eAaPmAgMnS
Ferenc	Ferenc	k1gMnSc1
Puskás	Puskása	k1gFnPc2
a	a	k8xC
třikrát	třikrát	k6eAd1
Alfredo	Alfredo	k1gNnSc1
di	di	k?
Stéfano	Stéfana	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s>
I	i	k9
po	po	k7c6
odchodu	odchod	k1gInSc6
di	di	k?
Stéfana	Stéfan	k1gMnSc2
Real	Real	k1gInSc1
stále	stále	k6eAd1
vítězil	vítězit	k5eAaImAgInS
a	a	k8xC
přidával	přidávat	k5eAaImAgInS
na	na	k7c4
své	svůj	k3xOyFgNnSc4
konto	konto	k1gNnSc4
jedno	jeden	k4xCgNnSc4
vítězství	vítězství	k1gNnSc4
ve	v	k7c6
španělské	španělský	k2eAgFnSc6d1
lize	liga	k1gFnSc6
za	za	k7c7
druhým	druhý	k4xOgInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
Francisco	Francisco	k1gNnSc4
Gento	Gent	k2eAgNnSc4d1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
jediným	jediný	k2eAgMnSc7d1
hráčem	hráč	k1gMnSc7
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
dokázal	dokázat	k5eAaPmAgInS
šestkrát	šestkrát	k6eAd1
vyhrát	vyhrát	k5eAaPmF
Pohár	pohár	k1gInSc4
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
Real	Real	k1gInSc1
vyhrál	vyhrát	k5eAaPmAgInS
dalších	další	k2eAgInPc2d1
pět	pět	k4xCc4
titulů	titul	k1gInPc2
v	v	k7c6
lize	liga	k1gFnSc6
a	a	k8xC
ve	v	k7c6
Španělském	španělský	k2eAgInSc6d1
poháru	pohár	k1gInSc6
a	a	k8xC
nyní	nyní	k6eAd1
vládly	vládnout	k5eAaImAgFnP
hvězdy	hvězda	k1gFnPc1
jako	jako	k8xC,k8xS
Camacho	Camacha	k1gFnSc5
<g/>
,	,	kIx,
Juanito	Juanit	k2eAgNnSc1d1
nebo	nebo	k8xC
Santillana	Santillana	k1gFnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1978	#num#	k4
skončila	skončit	k5eAaPmAgFnS
éra	éra	k1gFnSc1
Santiaga	Santiago	k1gNnSc2
Bernabéa	Bernabéum	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
zemřel	zemřít	k5eAaPmAgMnS
přirozenou	přirozený	k2eAgFnSc7d1
smrtí	smrt	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
byl	být	k5eAaImAgInS
stadion	stadion	k1gInSc4
Realu	Real	k1gInSc2
Madrid	Madrid	k1gInSc1
přejmenován	přejmenován	k2eAgInSc1d1
na	na	k7c4
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
dnešní	dnešní	k2eAgInSc4d1
název	název	k1gInSc4
Estadio	Estadio	k6eAd1
Santiago	Santiago	k1gNnSc1
Bernabéu	Bernabéus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgInSc7
skončila	skončit	k5eAaPmAgFnS
jedna	jeden	k4xCgFnSc1
velká	velký	k2eAgFnSc1d1
a	a	k8xC
nejslavnější	slavný	k2eAgFnSc1d3
éra	éra	k1gFnSc1
madridského	madridský	k2eAgInSc2d1
velkoklubu	velkoklub	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Generace	generace	k1gFnSc1
„	„	k?
<g/>
supa	sup	k1gMnSc2
<g/>
“	“	k?
a	a	k8xC
začátek	začátek	k1gInSc1
novodobé	novodobý	k2eAgFnSc2d1
historie	historie	k1gFnSc2
</s>
<s>
Hugo	Hugo	k1gMnSc1
Sánchez	Sánchez	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1988	#num#	k4
</s>
<s>
V	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
vládlo	vládnout	k5eAaImAgNnS
španělskému	španělský	k2eAgInSc3d1
fotbalu	fotbal	k1gInSc3
Baskicko	Baskicko	k1gNnSc4
<g/>
,	,	kIx,
tedy	tedy	k9
Real	Real	k1gInSc1
Sociedad	Sociedad	k1gInSc1
a	a	k8xC
Athletic	Athletice	k1gFnPc2
Bilbao	Bilbao	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoliv	ačkoliv	k8xS
Real	Real	k1gInSc1
si	se	k3xPyFc3
na	na	k7c4
španělský	španělský	k2eAgInSc4d1
titul	titul	k1gInSc4
musel	muset	k5eAaImAgInS
počkat	počkat	k5eAaPmF
<g/>
,	,	kIx,
rezerva	rezerva	k1gFnSc1
v	v	k7c6
podobě	podoba	k1gFnSc6
Castilly	Castilla	k1gFnSc2
si	se	k3xPyFc3
získala	získat	k5eAaPmAgFnS
oblibu	obliba	k1gFnSc4
u	u	k7c2
fanoušků	fanoušek	k1gMnPc2
svým	svůj	k3xOyFgNnSc7
pojetím	pojetí	k1gNnSc7
fotbalu	fotbal	k1gInSc2
a	a	k8xC
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
utkala	utkat	k5eAaPmAgNnP
o	o	k7c4
národní	národní	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
,	,	kIx,
Copu	cop	k1gInSc2
del	del	k?
Rey	Rea	k1gFnSc2
<g/>
,	,	kIx,
se	s	k7c7
seniorským	seniorský	k2eAgNnSc7d1
mužstvem	mužstvo	k1gNnSc7
Realu	Real	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Castilla	Castilla	k1gFnSc1
posléze	posléze	k6eAd1
dodala	dodat	k5eAaPmAgFnS
hlavnímu	hlavní	k2eAgNnSc3d1
mužstvu	mužstvo	k1gNnSc3
budoucí	budoucí	k2eAgFnSc2d1
opory	opora	k1gFnSc2
–	–	k?
utvořila	utvořit	k5eAaPmAgFnS
se	se	k3xPyFc4
zde	zde	k6eAd1
osa	osa	k1gFnSc1
jménem	jméno	k1gNnSc7
La	la	k1gNnSc2
Quinta	Quinta	k1gFnSc1
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc7
součástí	součást	k1gFnSc7
byli	být	k5eAaImAgMnP
hráči	hráč	k1gMnPc1
Emilio	Emilio	k1gMnSc1
Butragueñ	Butragueñ	k1gMnSc1
<g/>
,	,	kIx,
Manuel	Manuel	k1gMnSc1
Sanchís	Sanchís	k1gInSc1
<g/>
,	,	kIx,
Martín	Martín	k1gMnSc1
Vázquez	Vázquez	k1gMnSc1
<g/>
,	,	kIx,
Miguel	Miguel	k1gMnSc1
Pardeza	Pardeza	k1gFnSc1
a	a	k8xC
Míchel	Míchel	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ti	ten	k3xDgMnPc1
se	se	k3xPyFc4
začali	začít	k5eAaPmAgMnP
prosazovat	prosazovat	k5eAaImF
na	na	k7c6
přelomu	přelom	k1gInSc6
let	léto	k1gNnPc2
1983	#num#	k4
a	a	k8xC
1984	#num#	k4
a	a	k8xC
v	v	k7c6
ročníku	ročník	k1gInSc6
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
se	se	k3xPyFc4
už	už	k6eAd1
La	la	k1gNnSc7
Quinta	Quint	k1gInSc2
mohla	moct	k5eAaImAgFnS
těšit	těšit	k5eAaImF
úspěchu	úspěch	k1gInSc2
v	v	k7c6
Poháru	pohár	k1gInSc6
UEFA	UEFA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
třetím	třetí	k4xOgNnSc6
kole	kolo	k1gNnSc6
čelil	čelit	k5eAaImAgInS
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
finalistovi	finalista	k1gMnSc3
posledního	poslední	k2eAgInSc2d1
ročníku	ročník	k1gInSc2
<g/>
,	,	kIx,
Anderlechtu	Anderlecht	k1gInSc2
a	a	k8xC
prohrál	prohrát	k5eAaPmAgInS
venku	venku	k6eAd1
0	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledek	výsledek	k1gInSc4
ale	ale	k8xC
fotbalisté	fotbalista	k1gMnPc1
Realu	Real	k1gInSc2
otočili	otočit	k5eAaPmAgMnP
<g/>
,	,	kIx,
když	když	k8xS
díky	díky	k7c3
Butragueñ	Butragueñ	k1gMnSc3
(	(	kIx(
<g/>
3	#num#	k4
góly	gól	k1gInPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Valdanovi	Valdanův	k2eAgMnPc1d1
(	(	kIx(
<g/>
2	#num#	k4
góly	gól	k1gInPc4
<g/>
)	)	kIx)
a	a	k8xC
Sanchísovi	Sanchís	k1gMnSc3
(	(	kIx(
<g/>
1	#num#	k4
gól	gól	k1gInSc1
<g/>
)	)	kIx)
vyhráli	vyhrát	k5eAaPmAgMnP
6	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finále	finále	k1gNnSc4
madridský	madridský	k2eAgInSc1d1
celek	celek	k1gInSc1
zvládl	zvládnout	k5eAaPmAgInS
triumfem	triumf	k1gInSc7
nad	nad	k7c7
maďarským	maďarský	k2eAgInSc7d1
Videotonem	Videoton	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Klubový	klubový	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Ramón	Ramón	k1gMnSc1
Mendoza	Mendoz	k1gMnSc4
podepřel	podepřít	k5eAaPmAgMnS
nadějnou	nadějný	k2eAgFnSc4d1
mládež	mládež	k1gFnSc4
akvizicí	akvizice	k1gFnPc2
Huga	Hugo	k1gMnSc2
Sáncheze	Sáncheze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trenér	trenér	k1gMnSc1
Luis	Luisa	k1gFnPc2
Molowny	Molowna	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
v	v	k7c6
průběhu	průběh	k1gInSc6
sezóny	sezóna	k1gFnSc2
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
nahradil	nahradit	k5eAaPmAgInS
Amancia	Amancius	k1gMnSc4
Amaru	Amara	k1gMnSc4
<g/>
,	,	kIx,
obhájil	obhájit	k5eAaPmAgMnS
Pohár	pohár	k1gInSc4
UEFA	UEFA	kA
a	a	k8xC
po	po	k7c6
šesti	šest	k4xCc6
letech	léto	k1gNnPc6
vrátil	vrátit	k5eAaPmAgInS
titul	titul	k1gInSc1
mistra	mistr	k1gMnSc2
do	do	k7c2
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trenérovo	trenérův	k2eAgNnSc1d1
rozestavení	rozestavení	k1gNnSc1
4	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
využívalo	využívat	k5eAaImAgNnS,k5eAaPmAgNnS
Huga	Hugo	k1gMnSc4
Sáncheze	Sáncheze	k1gFnSc2
na	na	k7c6
hrotu	hrot	k1gInSc6
podporovaného	podporovaný	k2eAgInSc2d1
ze	z	k7c2
stran	strana	k1gFnPc2
Valdanem	Valdan	k1gInSc7
a	a	k8xC
Butragueñ	Butragueñ	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
měl	mít	k5eAaImAgInS
přezdívku	přezdívka	k1gFnSc4
„	„	k?
<g/>
El	Ela	k1gFnPc2
Buitre	Buitr	k1gInSc5
<g/>
“	“	k?
(	(	kIx(
<g/>
Sup	sup	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
nimi	on	k3xPp3gMnPc7
hrál	hrát	k5eAaImAgMnS
Míchel	Míchel	k1gMnSc1
<g/>
,	,	kIx,
kterému	který	k3yIgMnSc3,k3yRgMnSc3,k3yQgMnSc3
v	v	k7c6
záloze	záloha	k1gFnSc6
defenzivně	defenzivně	k6eAd1
vypomáhal	vypomáhat	k5eAaImAgMnS
Ricardo	Ricardo	k1gNnSc4
Gallego	Gallego	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sánchez	Sánchez	k1gInSc1
přezdívaný	přezdívaný	k2eAgInSc4d1
Hugol	Hugol	k1gInSc4
vstřelil	vstřelit	k5eAaPmAgMnS
22	#num#	k4
gólů	gól	k1gInPc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
podepřel	podepřít	k5eAaPmAgMnS
rekordní	rekordní	k2eAgFnSc4d1
sezónu	sezóna	k1gFnSc4
se	s	k7c7
ziskem	zisk	k1gInSc7
56	#num#	k4
bodů	bod	k1gInPc2
napříč	napříč	k7c7
34	#num#	k4
zápasy	zápas	k1gInPc7
v	v	k7c6
éře	éra	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
za	za	k7c4
výhru	výhra	k1gFnSc4
udělovaly	udělovat	k5eAaImAgInP
body	bod	k1gInPc4
dva	dva	k4xCgInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Druhá	druhý	k4xOgFnSc1
Barcelona	Barcelona	k1gFnSc1
na	na	k7c4
Real	Real	k1gInSc4
ztrácela	ztrácet	k5eAaImAgFnS
11	#num#	k4
bodů	bod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
převzal	převzít	k5eAaPmAgMnS
trénování	trénování	k1gNnSc4
Los	los	k1gMnSc1
Blancos	Blancos	k1gMnSc1
Leo	Leo	k1gMnSc1
Beenhakker	Beenhakker	k1gMnSc1
<g/>
,	,	kIx,
do	do	k7c2
branky	branka	k1gFnSc2
se	se	k3xPyFc4
nově	nově	k6eAd1
postavil	postavit	k5eAaPmAgMnS
Francisco	Francisco	k1gMnSc1
Buyo	Buyo	k1gMnSc1
přezdívaný	přezdívaný	k2eAgMnSc1d1
„	„	k?
<g/>
Paco	Paco	k1gMnSc1
<g/>
“	“	k?
a	a	k8xC
do	do	k7c2
Madridu	Madrid	k1gInSc2
dorazily	dorazit	k5eAaPmAgInP
další	další	k2eAgInPc1d1
tituly	titul	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
posilou	posila	k1gFnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Bernd	Bernd	k1gMnSc1
Schuster	Schuster	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
dokázali	dokázat	k5eAaPmAgMnP
fotbalisté	fotbalista	k1gMnPc1
Realu	Real	k1gInSc2
vyhrát	vyhrát	k5eAaPmF
dvakrát	dvakrát	k6eAd1
Pohár	pohár	k1gInSc4
UEFA	UEFA	kA
<g/>
,	,	kIx,
pětkrát	pětkrát	k6eAd1
španělský	španělský	k2eAgInSc4d1
titul	titul	k1gInSc4
a	a	k8xC
čtyřikrát	čtyřikrát	k6eAd1
španělský	španělský	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
dokonce	dokonce	k9
Real	Real	k1gInSc1
vytvořil	vytvořit	k5eAaPmAgInS
rekord	rekord	k1gInSc4
v	v	k7c6
počtu	počet	k1gInSc6
vstřelených	vstřelený	k2eAgInPc2d1
gólů	gól	k1gInPc2
v	v	k7c6
jedné	jeden	k4xCgFnSc6
sezóně	sezóna	k1gFnSc6
–	–	k?
pod	pod	k7c7
tehdejším	tehdejší	k2eAgMnSc7d1
trenérem	trenér	k1gMnSc7
Johnem	John	k1gMnSc7
Toshackem	Toshack	k1gInSc7
vstřelil	vstřelit	k5eAaPmAgMnS
ve	v	k7c6
38	#num#	k4
ligových	ligový	k2eAgInPc6d1
zápasech	zápas	k1gInPc6
107	#num#	k4
gólů	gól	k1gInPc2
a	a	k8xC
jednoznačně	jednoznačně	k6eAd1
získal	získat	k5eAaPmAgMnS
titul	titul	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
konci	konec	k1gInSc6
této	tento	k3xDgFnSc2
generace	generace	k1gFnSc2
ale	ale	k8xC
do	do	k7c2
Madridu	Madrid	k1gInSc2
přišla	přijít	k5eAaPmAgFnS
krize	krize	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
vrcholila	vrcholit	k5eAaImAgFnS
častým	častý	k2eAgNnSc7d1
střídáním	střídání	k1gNnSc7
trenérů	trenér	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdejší	tehdejší	k2eAgMnPc1d1
prezident	prezident	k1gMnSc1
Ramón	Ramón	k1gMnSc1
Mendoza	Mendoz	k1gMnSc4
dokonce	dokonce	k9
v	v	k7c6
klubu	klub	k1gInSc6
vytvořil	vytvořit	k5eAaPmAgMnS
nepříjemné	příjemný	k2eNgInPc4d1
dluhy	dluh	k1gInPc4
a	a	k8xC
Real	Real	k1gInSc1
se	se	k3xPyFc4
potácel	potácet	k5eAaImAgInS
ve	v	k7c6
velmi	velmi	k6eAd1
špatné	špatný	k2eAgFnSc6d1
situaci	situace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	Nový	k1gMnSc1
prezident	prezident	k1gMnSc1
klubu	klub	k1gInSc2
Lorenzo	Lorenza	k1gFnSc5
Sanz	Sanz	k1gInSc1
však	však	k9
dokázal	dokázat	k5eAaPmAgInS
klub	klub	k1gInSc1
částečně	částečně	k6eAd1
dostat	dostat	k5eAaPmF
z	z	k7c2
dluhů	dluh	k1gInPc2
a	a	k8xC
„	„	k?
<g/>
Bílý	bílý	k2eAgInSc1d1
balet	balet	k1gInSc1
<g/>
“	“	k?
se	se	k3xPyFc4
chystal	chystat	k5eAaImAgMnS
opět	opět	k6eAd1
k	k	k7c3
výšinám	výšina	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
Florentino	Florentin	k2eAgNnSc1d1
Pérez	Pérez	k1gInSc4
opět	opět	k6eAd1
na	na	k7c6
scéně	scéna	k1gFnSc6
aneb	aneb	k?
Los	los	k1gInSc1
Galactícos	Galactícos	k1gInSc1
II	II	kA
<g/>
,	,	kIx,
konec	konec	k1gInSc1
Raúla	Raúla	k1gFnSc1
a	a	k8xC
Mourinhova	Mourinhův	k2eAgFnSc1d1
éra	éra	k1gFnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vítězství	vítězství	k1gNnSc1
v	v	k7c6
Primera	primera	k1gFnSc1
División	División	k1gInSc1
2008	#num#	k4
</s>
<s>
Ovšem	ovšem	k9
královský	královský	k2eAgInSc1d1
klub	klub	k1gInSc1
z	z	k7c2
Madridu	Madrid	k1gInSc2
toužil	toužit	k5eAaImAgInS
po	po	k7c6
svém	své	k1gNnSc6
10	#num#	k4
<g/>
.	.	kIx.
triumfu	triumf	k1gInSc2
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
a	a	k8xC
po	po	k7c6
</s>
<s>
nepovedené	povedený	k2eNgFnSc3d1
sezóně	sezóna	k1gFnSc3
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
<g/>
,	,	kIx,
mimochodem	mimochodem	k9
kvůli	kvůli	k7c3
údajným	údajný	k2eAgFnPc3d1
manipulacím	manipulace	k1gFnPc3
během	během	k7c2
valné	valný	k2eAgFnSc2d1
</s>
<s>
hromady	hromada	k1gFnSc2
<g/>
,	,	kIx,
odvolán	odvolán	k2eAgMnSc1d1
<g/>
,	,	kIx,
respektive	respektive	k9
podal	podat	k5eAaPmAgMnS
rezignaci	rezignace	k1gFnSc4
<g/>
,	,	kIx,
Ramon	Ramona	k1gFnPc2
Calderon	Calderon	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
půlroku	půlrok	k1gInSc6
</s>
<s>
přechodné	přechodný	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
Vicenteho	Vicente	k1gMnSc2
Boludy	Boluda	k1gMnSc2
se	se	k3xPyFc4
do	do	k7c2
čela	čelo	k1gNnSc2
Realu	Real	k1gInSc2
znovu	znovu	k6eAd1
dostal	dostat	k5eAaPmAgMnS
Florentino	Florentin	k2eAgNnSc4d1
</s>
<s>
Pérez	Pérez	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
začátkem	začátkem	k7c2
léta	léto	k1gNnSc2
2009	#num#	k4
rozjel	rozjet	k5eAaPmAgMnS
projekt	projekt	k1gInSc4
Galactícos	Galactícosa	k1gFnPc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
Realu	Real	k1gInSc2
Madrid	Madrid	k1gInSc1
přišla	přijít	k5eAaPmAgNnP
velká	velký	k2eAgNnPc1d1
světová	světový	k2eAgNnPc1d1
jména	jméno	k1gNnPc1
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
Cristianem	Cristian	k1gMnSc7
Ronaldem	Ronald	k1gMnSc7
</s>
<s>
(	(	kIx(
<g/>
94	#num#	k4
milionů	milion	k4xCgInPc2
eur	euro	k1gNnPc2
<g/>
)	)	kIx)
a	a	k8xC
Kaká	kakat	k5eAaImIp3nS
(	(	kIx(
<g/>
68	#num#	k4
milionů	milion	k4xCgInPc2
eur	euro	k1gNnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
týmu	tým	k1gInSc2
přibyli	přibýt	k5eAaPmAgMnP
i	i	k9
Karim	Karim	k1gInSc4
Benzema	Benzema	k1gFnSc1
<g/>
,	,	kIx,
Raúl	Raúl	k1gInSc1
Albiol	Albiola	k1gFnPc2
</s>
<s>
či	či	k8xC
Xabi	Xab	k1gMnPc1
Alonso	Alonsa	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedení	vedení	k1gNnSc1
klubu	klub	k1gInSc2
navíc	navíc	k6eAd1
všem	všecek	k3xTgMnPc3
příchozím	příchozí	k1gMnPc3
přichystalo	přichystat	k5eAaPmAgNnS
obrovské	obrovský	k2eAgNnSc1d1
</s>
<s>
prezentace	prezentace	k1gFnSc1
přímo	přímo	k6eAd1
na	na	k7c6
stadionu	stadion	k1gInSc6
Santiaga	Santiago	k1gNnSc2
Bernabéu	Bernabéus	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c4
které	který	k3yRgInPc4,k3yQgInPc4,k3yIgInPc4
dorazily	dorazit	k5eAaPmAgInP
tisíce	tisíc	k4xCgInPc1
</s>
<s>
fanoušků	fanoušek	k1gMnPc2
<g/>
,	,	kIx,
vrcholem	vrchol	k1gInSc7
bylo	být	k5eAaImAgNnS
představení	představení	k1gNnSc1
Portugalce	Portugalec	k1gMnSc2
Ronalda	Ronald	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
vyprodal	vyprodat	k5eAaPmAgMnS
sám	sám	k3xTgInSc4
celý	celý	k2eAgInSc4d1
</s>
<s>
stadion	stadion	k1gInSc1
(	(	kIx(
<g/>
80	#num#	k4
tisíc	tisíc	k4xCgInSc4
fanoušků	fanoušek	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
ostatní	ostatní	k2eAgMnPc4d1
<g/>
,	,	kIx,
nepotřebné	potřebný	k2eNgFnPc1d1
<g/>
,	,	kIx,
hráče	hráč	k1gMnSc2
to	ten	k3xDgNnSc1
však	však	k9
znamenalo	znamenat	k5eAaImAgNnS
</s>
<s>
konec	konec	k1gInSc1
působení	působení	k1gNnSc2
v	v	k7c6
Realu	Real	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Balili	balit	k5eAaImAgMnP
se	se	k3xPyFc4
tak	tak	k9
Klaas-Jan	Klaas-Jan	k1gMnSc1
Huntelaar	Huntelaar	k1gMnSc1
<g/>
,	,	kIx,
Wesley	Weslea	k1gFnPc1
Sneijder	Sneijder	k1gMnSc1
<g/>
,	,	kIx,
Arjen	Arjen	k2eAgMnSc1d1
Robben	Robben	k2eAgMnSc1d1
a	a	k8xC
v	v	k7c6
polovině	polovina	k1gFnSc6
</s>
<s>
sezóny	sezóna	k1gFnPc1
i	i	k8xC
Ruud	Ruud	k1gInSc1
van	vana	k1gFnPc2
Nistelrooy	Nistelrooa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
zvučné	zvučný	k2eAgFnPc1d1
posily	posila	k1gFnPc1
ovšem	ovšem	k9
nepřinesly	přinést	k5eNaPmAgFnP
kýžený	kýžený	k2eAgInSc4d1
úspěch	úspěch	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Copa	Copa	k1gFnSc1
del	del	k?
Rey	Rea	k1gFnSc2
Los	los	k1gMnSc1
Merengues	Merengues	k1gMnSc1
vybouchli	vybouchnout	k5eAaPmAgMnP
s	s	k7c7
Alcorcónem	Alcorcón	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
dokonce	dokonce	k9
u	u	k7c2
</s>
<s>
sebe	sebe	k3xPyFc4
doma	doma	k6eAd1
vyhrál	vyhrát	k5eAaPmAgInS
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
!	!	kIx.
</s>
<s desamb="1">
Potupu	potupa	k1gFnSc4
Real	Real	k1gInSc4
Madrid	Madrid	k1gInSc1
dokonal	dokonat	k5eAaPmAgInS
opětovným	opětovný	k2eAgInSc7d1
neúspěchem	neúspěch	k1gInSc7
v	v	k7c6
osmifinále	osmifinále	k1gNnSc6
</s>
<s>
Ligy	liga	k1gFnPc1
mistrů	mistr	k1gMnPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
ho	on	k3xPp3gMnSc4
vyřadil	vyřadit	k5eAaPmAgInS
Olympique	Olympique	k1gInSc1
Lyon	Lyon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
ligové	ligový	k2eAgFnSc6d1
půdě	půda	k1gFnSc6
to	ten	k3xDgNnSc1
dlouho	dlouho	k6eAd1
vypadalo	vypadat	k5eAaPmAgNnS,k5eAaImAgNnS
na	na	k7c4
zajímavý	zajímavý	k2eAgInSc4d1
</s>
<s>
konec	konec	k1gInSc1
soutěže	soutěž	k1gFnSc2
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
prohraná	prohraná	k1gFnSc1
El	Ela	k1gFnPc2
Clásica	Clásica	k1gFnSc1
znamenala	znamenat	k5eAaImAgFnS
<g/>
,	,	kIx,
že	že	k8xS
Barca	Barca	k1gFnSc1
obhájila	obhájit	k5eAaPmAgFnS
titul	titul	k1gInSc4
</s>
<s>
mistra	mistr	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prezident	prezident	k1gMnSc1
Pérez	Pérez	k1gMnSc1
tak	tak	k6eAd1
nelenil	lenit	k5eNaImAgMnS
<g/>
,	,	kIx,
odvolal	odvolat	k5eAaPmAgMnS
trenéra	trenér	k1gMnSc4
Pellegriniho	Pellegrini	k1gMnSc4
a	a	k8xC
na	na	k7c4
jeho	jeho	k3xOp3gNnSc4
</s>
<s>
místo	místo	k1gNnSc4
dosadil	dosadit	k5eAaPmAgMnS
Josého	Josého	k2eAgMnSc1d1
Mourinha	Mourinha	k1gMnSc1
<g/>
,	,	kIx,
kterého	který	k3yQgMnSc4,k3yIgMnSc4,k3yRgMnSc4
přetáhl	přetáhnout	k5eAaPmAgMnS
z	z	k7c2
Interu	Inter	k1gInSc2
Milán	Milán	k1gInSc1
<g/>
,	,	kIx,
s	s	k7c7
nímž	jenž	k3xRgInSc7
</s>
<s>
Portugalec	Portugalec	k1gMnSc1
zvítězil	zvítězit	k5eAaPmAgMnS
ve	v	k7c6
finále	finále	k1gNnSc6
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
na	na	k7c6
Santiagu	Santiago	k1gNnSc6
Bernabéu	Bernabéus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
A	a	k9
The	The	k1gMnSc1
Special	Special	k1gMnSc1
One	One	k1gMnSc1
začal	začít	k5eAaPmAgMnS
Real	Real	k1gInSc4
předělávat	předělávat	k5eAaImF
k	k	k7c3
obrazu	obraz	k1gInSc3
svému	svůj	k3xOyFgInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přišli	přijít	k5eAaPmAgMnP
Ángel	Ángel	k1gMnSc1
Di	Di	k1gMnSc1
María	María	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Mesut	Mesut	k2eAgInSc4d1
Özil	Özil	k1gInSc4
<g/>
,	,	kIx,
Ricardo	Ricardo	k1gNnSc4
Carvalho	Carval	k1gMnSc2
a	a	k8xC
Sami	sám	k3xTgMnPc1
Khedira	Khedir	k1gMnSc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
odešli	odejít	k5eAaPmAgMnP
Rafael	Rafael	k1gMnSc1
van	van	k1gInSc4
der	drát	k5eAaImRp2nS
Vaart	Vaarta	k1gFnPc2
<g/>
,	,	kIx,
Mahamadou	Mahamada	k1gFnSc7
Diarra	Diarr	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
</s>
<s>
hlavně	hlavně	k9
Raúl	Raúl	k1gMnSc1
González	González	k1gMnSc1
a	a	k8xC
Guti	Gut	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozloučení	rozloučení	k1gNnSc1
obou	dva	k4xCgMnPc2
hráčů	hráč	k1gMnPc2
na	na	k7c6
tiskových	tiskový	k2eAgFnPc6d1
konferencích	konference	k1gFnPc6
bylo	být	k5eAaImAgNnS
více	hodně	k6eAd2
</s>
<s>
než	než	k8xS
emotivní	emotivní	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Královský	královský	k2eAgInSc1d1
velkoklub	velkoklub	k1gInSc1
tímto	tento	k3xDgNnSc7
dopsal	dopsat	k5eAaPmAgInS
jednu	jeden	k4xCgFnSc4
ze	z	k7c2
svých	svůj	k3xOyFgFnPc2
dalších	další	k2eAgFnPc2d1
slavných	slavný	k2eAgFnPc2d1
</s>
<s>
kapitol	kapitola	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novým	nový	k2eAgMnSc7d1
vůdcem	vůdce	k1gMnSc7
bílé	bílý	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
další	další	k2eAgInSc1d1
klubový	klubový	k2eAgInSc1d1
symbol	symbol	k1gInSc1
–	–	k?
Iker	Iker	k1gMnSc1
Casillas	Casillas	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Bílý	bílý	k2eAgInSc1d1
balet	balet	k1gInSc1
následně	následně	k6eAd1
skutečně	skutečně	k6eAd1
prorazil	prorazit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
došel	dojít	k5eAaPmAgInS
až	až	k9
do	do	k7c2
semifinále	semifinále	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
padl	padnout	k5eAaImAgInS,k5eAaPmAgInS
po	po	k7c6
kontroverzním	kontroverzní	k2eAgInSc6d1
dvojzápase	dvojzápas	k1gInSc6
s	s	k7c7
Barcelonou	Barcelona	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
La	la	k1gNnSc1
Lize	liga	k1gFnSc6
tu	tu	k6eAd1
byl	být	k5eAaImAgInS
znovu	znovu	k6eAd1
prohraný	prohraný	k2eAgInSc1d1
souboj	souboj	k1gInSc1
s	s	k7c7
Barcelonou	Barcelona	k1gFnSc7
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
většina	většina	k1gFnSc1
odborníků	odborník	k1gMnPc2
shodla	shodnout	k5eAaBmAgFnS,k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
o	o	k7c4
titul	titul	k1gInSc4
Los	los	k1gMnSc1
Blancos	Blancos	k1gMnSc1
připravili	připravit	k5eAaPmAgMnP
sami	sám	k3xTgMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každému	každý	k3xTgMnSc3
ovšem	ovšem	k9
utkví	utkvět	k5eAaPmIp3nS
v	v	k7c6
paměti	paměť	k1gFnSc6
potupná	potupný	k2eAgFnSc1d1
prohra	prohra	k1gFnSc1
z	z	k7c2
Nou	Nou	k1gFnSc2
Campu	camp	k1gInSc2
<g/>
,	,	kIx,
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tohle	tenhle	k3xDgNnSc1
všechno	všechen	k3xTgNnSc4
si	se	k3xPyFc3
Mourinhova	Mourinhův	k2eAgFnSc1d1
družina	družina	k1gFnSc1
vynahradila	vynahradit	k5eAaPmAgFnS
v	v	k7c6
Královském	královský	k2eAgInSc6d1
poháru	pohár	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
své	svůj	k3xOyFgFnSc6
pouti	pouť	k1gFnSc6
vyřadila	vyřadit	k5eAaPmAgFnS
postupně	postupně	k6eAd1
Murcii	Murcie	k1gFnSc4
<g/>
,	,	kIx,
UD	UD	kA
Levante	Levant	k1gMnSc5
<g/>
,	,	kIx,
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc4
a	a	k8xC
Sevillu	Sevilla	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
velkém	velký	k2eAgNnSc6d1
finále	finále	k1gNnSc6
se	se	k3xPyFc4
utkala	utkat	k5eAaPmAgFnS
s	s	k7c7
Barcelonou	Barcelona	k1gFnSc7
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
po	po	k7c6
</s>
<s>
prodloužení	prodloužení	k1gNnSc4
porazila	porazit	k5eAaPmAgFnS
brankou	branka	k1gFnSc7
Cristiana	Cristian	k1gMnSc2
Ronalda	Ronald	k1gMnSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
ten	ten	k3xDgInSc1
rok	rok	k1gInSc1
překonal	překonat	k5eAaPmAgInS
všemožné	všemožný	k2eAgInPc4d1
střelecké	střelecký	k2eAgInPc4d1
rekordy	rekord	k1gInPc4
ve	v	k7c6
Španělsku	Španělsko	k1gNnSc6
(	(	kIx(
<g/>
hlavně	hlavně	k9
ten	ten	k3xDgMnSc1
v	v	k7c6
La	la	k1gNnSc6
Lize	liga	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
tuto	tento	k3xDgFnSc4
trofej	trofej	k1gFnSc4
se	se	k3xPyFc4
čekalo	čekat	k5eAaImAgNnS
celých	celý	k2eAgNnPc2d1
18	#num#	k4
let	léto	k1gNnPc2
<g/>
!	!	kIx.
</s>
<s>
V	v	k7c6
ročníku	ročník	k1gInSc6
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
se	se	k3xPyFc4
Bílý	bílý	k2eAgInSc1d1
balet	balet	k1gInSc1
prezentoval	prezentovat	k5eAaBmAgInS
fantastickým	fantastický	k2eAgInSc7d1
fotbalem	fotbal	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
přinesl	přinést	k5eAaPmAgInS
ligový	ligový	k2eAgInSc4d1
titul	titul	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
tom	ten	k3xDgNnSc6
se	se	k3xPyFc4
prakticky	prakticky	k6eAd1
rozhodl	rozhodnout	k5eAaPmAgInS
v	v	k7c4
Gran	Gran	k1gNnSc4
Derbi	Derb	k1gFnSc2
v	v	k7c6
</s>
<s>
Barceloně	Barcelona	k1gFnSc3
<g/>
,	,	kIx,
kde	kde	k6eAd1
rozhodoval	rozhodovat	k5eAaImAgInS
famózní	famózní	k2eAgNnSc4d1
Ronaldo	Ronaldo	k1gNnSc4
<g/>
,	,	kIx,
tahoun	tahoun	k1gMnSc1
celého	celý	k2eAgInSc2d1
týmu	tým	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
stanovil	stanovit	k5eAaPmAgInS
nový	nový	k2eAgInSc4d1
klubový	klubový	k2eAgInSc4d1
rekord	rekord	k1gInSc4
<g/>
,	,	kIx,
když	když	k8xS
v	v	k7c6
jedné	jeden	k4xCgFnSc6
sezoně	sezona	k1gFnSc6
nastřílel	nastřílet	k5eAaPmAgMnS
celkem	celek	k1gInSc7
60	#num#	k4
gólů	gól	k1gInPc2
a	a	k8xC
zároveň	zároveň	k6eAd1
rekord	rekord	k1gInSc4
La	la	k1gNnSc2
Ligy	liga	k1gFnSc2
za	za	k7c4
nejrychlejší	rychlý	k2eAgNnSc4d3
dosažení	dosažení	k1gNnSc4
mety	meta	k1gFnSc2
100	#num#	k4
gólů	gól	k1gInPc2
–	–	k?
stačilo	stačit	k5eAaBmAgNnS
mu	on	k3xPp3gMnSc3
na	na	k7c4
to	ten	k3xDgNnSc4
jen	jen	k9
92	#num#	k4
utkání	utkání	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Copa	Copa	k1gFnSc1
del	del	k?
Rey	Rea	k1gFnSc2
Barca	Barcum	k1gNnSc2
Realu	Real	k1gInSc6
oplatila	oplatit	k5eAaPmAgFnS
předchozí	předchozí	k2eAgFnSc4d1
finálovou	finálový	k2eAgFnSc4d1
porážku	porážka	k1gFnSc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
v	v	k7c6
pohárové	pohárový	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
Los	los	k1gInSc1
Merengues	Merenguesa	k1gFnPc2
narazili	narazit	k5eAaPmAgMnP
na	na	k7c4
neoblíbené	oblíbený	k2eNgMnPc4d1
Němce	Němec	k1gMnPc4
<g/>
,	,	kIx,
přesněji	přesně	k6eAd2
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nervy	nerv	k1gInPc1
drásající	drásající	k2eAgInSc1d1
penaltový	penaltový	k2eAgInSc4d1
rozstřel	rozstřel	k1gInSc4
v	v	k7c6
odvetě	odveta	k1gFnSc6
Madridistas	Madridistasa	k1gFnPc2
nezvládli	zvládnout	k5eNaPmAgMnP
a	a	k8xC
čekání	čekání	k1gNnSc4
na	na	k7c4
La	la	k1gNnSc4
Decimu	decima	k1gFnSc4
(	(	kIx(
<g/>
desátý	desátý	k4xOgInSc1
titul	titul	k1gInSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
prodloužilo	prodloužit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s>
Sezónu	sezóna	k1gFnSc4
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
začal	začít	k5eAaPmAgInS
Real	Real	k1gInSc1
úspěšně	úspěšně	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
ve	v	k7c6
španělském	španělský	k2eAgInSc6d1
superpoháru	superpohár	k1gInSc6
porazil	porazit	k5eAaPmAgMnS
Barcelonu	Barcelona	k1gFnSc4
díky	díky	k7c3
vice	vika	k1gFnSc3
gólům	gól	k1gInPc3
na	na	k7c6
hřišti	hřiště	k1gNnSc6
soupeře	soupeř	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc4
rok	rok	k1gInSc4
take	take	k6eAd1
doplnil	doplnit	k5eAaPmAgMnS
kádr	kádr	k1gInSc4
nadějný	nadějný	k2eAgMnSc1d1
Luka	luka	k1gNnPc1
Modrič	Modrič	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
přišel	přijít	k5eAaPmAgMnS
za	za	k7c4
33	#num#	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
£	£	k?
z	z	k7c2
Tottenhamu	Tottenham	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
byl	být	k5eAaImAgInS
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
nalosován	nalosovat	k5eAaPmNgInS,k5eAaBmNgInS,k5eAaImNgInS
do	do	k7c2
„	„	k?
<g/>
skupiny	skupina	k1gFnSc2
smrti	smrt	k1gFnSc2
<g/>
“	“	k?
společně	společně	k6eAd1
s	s	k7c7
Ajaxem	Ajax	k1gInSc7
Amsterdam	Amsterdam	k1gInSc1
<g/>
,	,	kIx,
Manchesterm	Manchesterm	k1gInSc1
City	City	k1gFnSc2
a	a	k8xC
Borussií	Borussie	k1gFnSc7
Dortumund	Dortumunda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
těžké	těžký	k2eAgFnSc6d1
konkurenci	konkurence	k1gFnSc6
</s>
<s>
skončil	skončit	k5eAaPmAgInS
nakonec	nakonec	k6eAd1
druhý	druhý	k4xOgInSc1
s	s	k7c7
Borussií	Borussie	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
skupinu	skupina	k1gFnSc4
vyhrála	vyhrát	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
se	se	k3xPyFc4
přes	přes	k7c4
Manchester	Manchester	k1gInSc4
United	United	k1gInSc1
a	a	k8xC
Galatasaray	Galatasara	k2eAgInPc1d1
Istanbul	Istanbul	k1gInSc1
dostal	dostat	k5eAaPmAgInS
do	do	k7c2
semifinále	semifinále	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
opět	opět	k6eAd1
nestačil	stačit	k5eNaBmAgInS
na	na	k7c4
vynikající	vynikající	k2eAgFnSc4d1
Borussii	Borussie	k1gFnSc4
Dortmund	Dortmund	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
v	v	k7c6
Copa	Copa	k1gFnSc1
del	del	k?
Rey	Rea	k1gFnSc2
se	se	k3xPyFc4
Realu	Real	k1gInSc3
nepodařilo	podařit	k5eNaPmAgNnS
vyhrát	vyhrát	k5eAaPmF
<g/>
,	,	kIx,
když	když	k8xS
ve	v	k7c6
finále	finále	k1gNnSc6
</s>
<s>
v	v	k7c6
prodloužení	prodloužení	k1gNnSc6
podlehl	podlehnout	k5eAaPmAgMnS
městskému	městský	k2eAgMnSc3d1
rivalovi	rival	k1gMnSc3
Atléticu	Atlétic	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
tomto	tento	k3xDgInSc6
neúspěchu	neúspěch	k1gInSc6
oznámil	oznámit	k5eAaPmAgMnS
president	president	k1gMnSc1
Peréz	Peréza	k1gFnPc2
odchod	odchod	k1gInSc4
kouče	kouč	k1gMnSc2
Mourinha	Mourinh	k1gMnSc2
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yIgNnSc6,k3yRgNnSc6,k3yQgNnSc6
se	se	k3xPyFc4
vzájemně	vzájemně	k6eAd1
dohodli	dohodnout	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s>
Carlo	Carlo	k1gNnSc1
Ancelotti	Ancelotť	k1gFnSc2
<g/>
,	,	kIx,
Zinedine	Zinedin	k1gMnSc5
Zidane	Zidan	k1gMnSc5
</s>
<s>
Novým	nový	k2eAgMnSc7d1
koučem	kouč	k1gMnSc7
byl	být	k5eAaImAgMnS
25	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2013	#num#	k4
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
Carlo	Carlo	k1gNnSc4
Ancelotti	Ancelott	k2eAgMnPc1d1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
s	s	k7c7
klubem	klub	k1gInSc7
podepsal	podepsat	k5eAaPmAgMnS
tříletou	tříletý	k2eAgFnSc4d1
smlouvu	smlouva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
asistenti	asistent	k1gMnPc1
byli	být	k5eAaImAgMnP
</s>
<s>
představeni	představen	k2eAgMnPc1d1
bývalá	bývalý	k2eAgFnSc1d1
hvězda	hvězda	k1gFnSc1
klubu	klub	k1gInSc2
Zinedine	Zinedin	k1gInSc5
Zidane	Zidan	k1gMnSc5
a	a	k8xC
Paul	Paul	k1gMnSc1
Clement	Clement	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
pak	pak	k6eAd1
byla	být	k5eAaImAgFnS
oznámena	oznámit	k5eAaPmNgFnS
dlouho	dlouho	k6eAd1
očekávaná	očekávaný	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
<g/>
,	,	kIx,
když	když	k8xS
klub	klub	k1gInSc1
dokončil	dokončit	k5eAaPmAgInS
další	další	k2eAgInSc4d1
velký	velký	k2eAgInSc4d1
přestup	přestup	k1gInSc4
–	–	k?
z	z	k7c2
Tottenhamu	Tottenham	k1gInSc2
přišel	přijít	k5eAaPmAgInS
za	za	k7c4
téměř	téměř	k6eAd1
100	#num#	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
€	€	k?
rychlík	rychlík	k1gInSc1
z	z	k7c2
Walesu	Wales	k1gInSc2
Gareth	Garetha	k1gFnPc2
Bale	bal	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nákup	nákup	k1gInSc1
se	se	k3xPyFc4
určitě	určitě	k6eAd1
vyplatil	vyplatit	k5eAaPmAgInS
<g/>
,	,	kIx,
Bale	bal	k1gInSc5
nastřílel	nastřílet	k5eAaPmAgMnS
nemálo	málo	k6eNd1
důležitých	důležitý	k2eAgInPc2d1
gólů	gól	k1gInPc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
jeden	jeden	k4xCgMnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
zajistil	zajistit	k5eAaPmAgInS
Realu	Real	k1gInSc3
vítězství	vítězství	k1gNnSc2
v	v	k7c6
Copa	Copa	k1gFnSc1
del	del	k?
Rey	Rea	k1gFnSc2
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
–	–	k?
ve	v	k7c6
finále	finále	k1gNnSc6
proti	proti	k7c3
Barceloně	Barcelona	k1gFnSc3
vstřelil	vstřelit	k5eAaPmAgMnS
Bale	bal	k1gInSc5
5	#num#	k4
minut	minuta	k1gFnPc2
před	před	k7c7
koncem	konec	k1gInSc7
zápasu	zápas	k1gInSc2
vítězný	vítězný	k2eAgInSc4d1
gól	gól	k1gInSc4
na	na	k7c4
2	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
,	,	kIx,
na	na	k7c4
který	který	k3yIgInSc4,k3yQgInSc4,k3yRgInSc4
již	již	k6eAd1
Barcelona	Barcelona	k1gFnSc1
nestihla	stihnout	k5eNaPmAgFnS
odpovědět	odpovědět	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Ligový	ligový	k2eAgInSc4d1
titul	titul	k1gInSc4
v	v	k7c6
sezóně	sezóna	k1gFnSc6
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
sice	sice	k8xC
Real	Real	k1gInSc1
nezískal	získat	k5eNaPmAgInS
(	(	kIx(
<g/>
vyhrálo	vyhrát	k5eAaPmAgNnS
jej	on	k3xPp3gNnSc4
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
vítězem	vítěz	k1gMnSc7
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
po	po	k7c6
finálové	finálový	k2eAgFnSc6d1
výhře	výhra	k1gFnSc6
4	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
po	po	k7c6
prodloužení	prodloužení	k1gNnSc6
v	v	k7c6
derby	derby	k1gNnSc6
právě	právě	k6eAd1
nad	nad	k7c7
Atléticem	Atlétic	k1gMnSc7
Madrid	Madrid	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
to	ten	k3xDgNnSc1
jeho	jeho	k3xOp3gNnSc4
jubilejní	jubilejní	k2eAgNnSc4d1
desáté	desátý	k4xOgNnSc4
vítězství	vítězství	k1gNnSc4
v	v	k7c6
této	tento	k3xDgFnSc6
nejprestižnější	prestižní	k2eAgFnSc6d3
evropské	evropský	k2eAgFnSc6d1
klubové	klubový	k2eAgFnSc6d1
soutěži	soutěž	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Následně	následně	k6eAd1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
podařilo	podařit	k5eAaPmAgNnS
získat	získat	k5eAaPmF
podruhé	podruhé	k6eAd1
v	v	k7c6
historii	historie	k1gFnSc6
získat	získat	k5eAaPmF
evropský	evropský	k2eAgInSc4d1
Superpohár	superpohár	k1gInSc4
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yQgInSc6,k3yIgInSc6,k3yRgInSc6
zvítězil	zvítězit	k5eAaPmAgInS
12	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2014	#num#	k4
přesvědčivě	přesvědčivě	k6eAd1
nad	nad	k7c7
Sevilla	Sevilla	k1gFnSc1
FC	FC	kA
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
(	(	kIx(
<g/>
oba	dva	k4xCgInPc4
góly	gól	k1gInPc4
vstřelil	vstřelit	k5eAaPmAgMnS
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
v	v	k7c6
historii	historie	k1gFnSc6
zvítězil	zvítězit	k5eAaPmAgInS
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
na	na	k7c6
MS	MS	kA
klubů	klub	k1gInPc2
<g/>
,	,	kIx,
když	když	k8xS
ve	v	k7c6
finále	finále	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
zvítězil	zvítězit	k5eAaPmAgInS
nad	nad	k7c7
argentinským	argentinský	k2eAgMnSc7d1
San	San	k1gMnSc7
Lorenzem	Lorenz	k1gMnSc7
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
sezóně	sezóna	k1gFnSc6
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
klubu	klub	k1gInSc2
titul	titul	k1gInSc1
v	v	k7c6
lize	liga	k1gFnSc6
unikl	uniknout	k5eAaPmAgMnS
o	o	k7c4
dva	dva	k4xCgInPc4
body	bod	k1gInPc4
<g/>
,	,	kIx,
když	když	k8xS
ho	on	k3xPp3gInSc4
získala	získat	k5eAaPmAgFnS
Barcelona	Barcelona	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
skončil	skončit	k5eAaPmAgInS
v	v	k7c6
semifinále	semifinále	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
krátkém	krátký	k2eAgNnSc6d1
působení	působení	k1gNnSc6
Rafaela	Rafaela	k1gFnSc1
Beníteze	Beníteze	k1gFnSc1
(	(	kIx(
<g/>
červen	červen	k1gInSc1
2015	#num#	k4
<g/>
–	–	k?
<g/>
leden	leden	k1gInSc1
2016	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
hlavním	hlavní	k2eAgMnSc7d1
trenérem	trenér	k1gMnSc7
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
Zinedine	Zinedin	k1gMnSc5
Zidane	Zidan	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
jeho	jeho	k3xOp3gNnSc7
vedením	vedení	k1gNnSc7
skončil	skončit	k5eAaPmAgInS
Real	Real	k1gInSc1
v	v	k7c6
květnu	květen	k1gInSc6
2016	#num#	k4
opět	opět	k6eAd1
druhý	druhý	k4xOgInSc1
za	za	k7c7
Barcelonou	Barcelona	k1gFnSc7
<g/>
,	,	kIx,
na	na	k7c4
vítěze	vítěz	k1gMnSc4
ztrácel	ztrácet	k5eAaImAgMnS
jeden	jeden	k4xCgInSc4
bod	bod	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podařilo	podařit	k5eAaPmAgNnS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
ale	ale	k9
vyhrát	vyhrát	k5eAaPmF
Ligu	liga	k1gFnSc4
mistrů	mistr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
sezóně	sezóna	k1gFnSc6
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
pak	pak	k6eAd1
klub	klub	k1gInSc1
vyhrál	vyhrát	k5eAaPmAgInS
po	po	k7c6
třicáté-třetí	třicáté-třetí	k1gNnSc6
španělskou	španělský	k2eAgFnSc4d1
ligu	liga	k1gFnSc4
a	a	k8xC
opět	opět	k6eAd1
trumfoval	trumfovat	k5eAaImAgInS
i	i	k9
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
<g/>
,	,	kIx,
když	když	k8xS
ve	v	k7c6
finále	finále	k1gNnSc6
porazil	porazit	k5eAaPmAgInS
Juventus	Juventus	k1gInSc1
Turín	Turín	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
se	se	k3xPyFc4
tak	tak	k6eAd1
stal	stát	k5eAaPmAgInS
prvním	první	k4xOgInSc7
klubem	klub	k1gInSc7
v	v	k7c6
novodobé	novodobý	k2eAgFnSc6d1
historii	historie	k1gFnSc6
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
dokázal	dokázat	k5eAaPmAgInS
triumf	triumf	k1gInSc4
obhájit	obhájit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zidanův	Zidanův	k2eAgInSc1d1
zápis	zápis	k1gInSc1
do	do	k7c2
historie	historie	k1gFnSc2
fotbalu	fotbal	k1gInSc2
tím	ten	k3xDgNnSc7
ale	ale	k9
nekončil	končit	k5eNaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc3
svěřenci	svěřenec	k1gMnSc3
zvítězilo	zvítězit	k5eAaPmAgNnS
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
i	i	k9
v	v	k7c6
následujícím	následující	k2eAgInSc6d1
ročníku	ročník	k1gInSc6
a	a	k8xC
zvedli	zvednout	k5eAaPmAgMnP
nad	nad	k7c4
hlavu	hlava	k1gFnSc4
pohár	pohár	k1gInSc4
pro	pro	k7c4
vítěze	vítěz	k1gMnPc4
nejprestižnější	prestižní	k2eAgFnSc2d3
soutěže	soutěž	k1gFnSc2
na	na	k7c6
světě	svět	k1gInSc6
potřetí	potřetí	k4xO
za	za	k7c7
sebou	se	k3xPyFc7
<g/>
.	.	kIx.
</s>
<s>
Éra	éra	k1gFnSc1
po	po	k7c6
Ronaldovi	Ronald	k1gMnSc6
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
létě	léto	k1gNnSc6
2018	#num#	k4
odešel	odejít	k5eAaPmAgMnS
z	z	k7c2
Realu	Real	k1gInSc2
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
v	v	k7c6
minulých	minulý	k2eAgFnPc6d1
sezónách	sezóna	k1gFnPc6
nastřílel	nastřílet	k5eAaPmAgInS
mnoho	mnoho	k4c4
gólů	gól	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
trenér	trenér	k1gMnSc1
Zinedine	Zinedin	k1gMnSc5
Zidane	Zidan	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
trenérského	trenérský	k2eAgNnSc2d1
křesla	křeslo	k1gNnSc2
usedl	usednout	k5eAaPmAgMnS
Julen	Julen	k1gInSc4
Lopetegui	Lopetegu	k1gFnSc2
<g/>
,	,	kIx,
kterého	který	k3yRgMnSc4,k3yIgMnSc4,k3yQgMnSc4
kvůli	kvůli	k7c3
domluvě	domluva	k1gFnSc3
s	s	k7c7
Realem	Real	k1gInSc7
propustili	propustit	k5eAaPmAgMnP
od	od	k7c2
španělské	španělský	k2eAgFnSc2d1
reprezentace	reprezentace	k1gFnSc2
pár	pár	k4xCyI
dnů	den	k1gInPc2
před	před	k7c7
začátkem	začátek	k1gInSc7
mistrovství	mistrovství	k1gNnSc2
světa	svět	k1gInSc2
v	v	k7c6
Rusku	Rusko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Madridskému	madridský	k2eAgInSc3d1
klubu	klub	k1gInSc3
se	se	k3xPyFc4
ale	ale	k9
v	v	k7c6
La	la	k1gNnSc1
Lize	liga	k1gFnSc6
2018	#num#	k4
zatím	zatím	k6eAd1
příliš	příliš	k6eAd1
nedařilo	dařit	k5eNaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Julen	Julen	k1gInSc1
Lopetegui	Lopetegu	k1gFnSc2
byl	být	k5eAaImAgInS
propuštěn	propustit	k5eAaPmNgInS
z	z	k7c2
Realu	Real	k1gInSc2
Madrid	Madrid	k1gInSc1
po	po	k7c6
prohře	prohra	k1gFnSc6
nad	nad	k7c7
Barcelonou	Barcelona	k1gFnSc7
(	(	kIx(
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
nahradil	nahradit	k5eAaPmAgInS
jej	on	k3xPp3gMnSc4
dočasný	dočasný	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
Santiago	Santiago	k1gNnSc1
Solari	Solari	k1gNnSc3
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
hrál	hrát	k5eAaImAgMnS
v	v	k7c6
Realu	Real	k1gInSc6
Madrid	Madrid	k1gInSc1
v	v	k7c6
letech	let	k1gInPc6
2000	#num#	k4
<g/>
–	–	k?
<g/>
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
jeho	jeho	k3xOp3gNnSc7
vedením	vedení	k1gNnSc7
se	se	k3xPyFc4
tým	tým	k1gInSc1
zvedl	zvednout	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
španělských	španělský	k2eAgNnPc2d1
pravidel	pravidlo	k1gNnPc2
musel	muset	k5eAaImAgMnS
klub	klub	k1gInSc4
stanovit	stanovit	k5eAaPmF
do	do	k7c2
patnácti	patnáct	k4xCc2
dnů	den	k1gInPc2
stálého	stálý	k2eAgMnSc2d1
trenéra	trenér	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c4
Solarim	Solarim	k1gInSc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
dočasným	dočasný	k2eAgMnSc7d1
koučem	kouč	k1gMnSc7
<g/>
,	,	kIx,
vyhrál	vyhrát	k5eAaPmAgInS
španělský	španělský	k2eAgInSc1d1
tým	tým	k1gInSc1
všechny	všechen	k3xTgInPc4
zápasy	zápas	k1gInPc4
<g/>
,	,	kIx,
mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
porazil	porazit	k5eAaPmAgMnS
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
Plzeň	Plzeň	k1gFnSc4
a	a	k8xC
Solari	Solare	k1gFnSc4
byl	být	k5eAaImAgInS
před	před	k7c7
reprezentační	reprezentační	k2eAgFnSc7d1
přestávkou	přestávka	k1gFnSc7
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
stálým	stálý	k2eAgMnSc7d1
trenérem	trenér	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smlouvu	smlouva	k1gFnSc4
podepsal	podepsat	k5eAaPmAgMnS
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
tomu	ten	k3xDgMnSc3
se	se	k3xPyFc4
však	však	k9
moc	moc	k6eAd1
nedaří	dařit	k5eNaImIp3nS
a	a	k8xC
do	do	k7c2
Realu	Real	k1gInSc2
se	se	k3xPyFc4
vrací	vracet	k5eAaImIp3nS
Zinedine	Zinedin	k1gMnSc5
Zidane	Zidan	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
příchodu	příchod	k1gInSc6
Zidana	Zidana	k1gFnSc1
zpátky	zpátky	k6eAd1
k	k	k7c3
týmu	tým	k1gInSc3
se	se	k3xPyFc4
výkony	výkon	k1gInPc1
zvedly	zvednout	k5eAaPmAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letním	letní	k2eAgNnSc6d1
přestupovém	přestupový	k2eAgNnSc6d1
oknu	okno	k1gNnSc6
do	do	k7c2
Realu	Real	k1gInSc2
Madrid	Madrid	k1gInSc1
přišla	přijít	k5eAaPmAgFnS
za	za	k7c4
100	#num#	k4
milionů	milion	k4xCgInPc2
eur	euro	k1gNnPc2
několikaletá	několikaletý	k2eAgFnSc1d1
hvězda	hvězda	k1gFnSc1
anglické	anglický	k2eAgFnSc2d1
Premier	Premiero	k1gNnPc2
League	League	k1gFnSc4
<g/>
,	,	kIx,
Eden	Eden	k1gInSc4
Hazard	hazard	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatím	zatím	k6eAd1
největší	veliký	k2eAgInSc4d3
úspěch	úspěch	k1gInSc4
druhé	druhý	k4xOgFnSc2
Zidanovy	Zidanův	k2eAgFnSc2d1
éry	éra	k1gFnSc2
je	být	k5eAaImIp3nS
mistrovský	mistrovský	k2eAgInSc4d1
titul	titul	k1gInSc4
ze	z	k7c2
sezóny	sezóna	k1gFnSc2
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Historické	historický	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1902	#num#	k4
–	–	k?
Sociedad	Sociedad	k1gInSc1
Madrid	Madrid	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
Sociedad	Sociedad	k1gInSc1
Madrid	Madrid	k1gInSc1
Foot-ball	Foot-ball	k1gInSc1
Club	club	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
1904	#num#	k4
–	–	k?
fúze	fúze	k1gFnSc1
s	s	k7c7
Moderno	Moderna	k1gFnSc5
Football	Football	k1gMnSc1
Club	club	k1gInSc1
(	(	kIx(
<g/>
1902	#num#	k4
<g/>
–	–	k?
<g/>
1904	#num#	k4
<g/>
)	)	kIx)
=	=	kIx~
<g/>
>	>	kIx)
Madrid	Madrid	k1gInSc1
<g/>
–	–	k?
<g/>
Moderno	Moderna	k1gFnSc5
FC	FC	kA
(	(	kIx(
<g/>
Madrid	Madrid	k1gInSc1
<g/>
–	–	k?
<g/>
Moderno	Moderna	k1gFnSc5
Foot-ball	Foot-ball	k1gMnSc1
Club	club	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1904	#num#	k4
–	–	k?
Madrid	Madrid	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
Madrid	Madrid	k1gInSc1
Foot-ball	Foot-ball	k1gMnSc1
Club	club	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1920	#num#	k4
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
Football	Football	k1gInSc1
Club	club	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
1931	#num#	k4
–	–	k?
Madrid	Madrid	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
Madrid	Madrid	k1gInSc1
Football	Football	k1gMnSc1
Club	club	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1940	#num#	k4
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
Football	Football	k1gInSc1
Club	club	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
1941	#num#	k4
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
(	(	kIx(
<g/>
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
Club	club	k1gInSc1
de	de	k?
Fútbol	Fútbol	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Klubové	klubový	k2eAgInPc4d1
symboly	symbol	k1gInPc4
</s>
<s>
Historická	historický	k2eAgNnPc1d1
loga	logo	k1gNnPc1
</s>
<s>
1902	#num#	k4
</s>
<s>
1908	#num#	k4
</s>
<s>
1931	#num#	k4
</s>
<s>
1941	#num#	k4
</s>
<s>
Vývoj	vývoj	k1gInSc1
domácích	domácí	k2eAgInPc2d1
dresů	dres	k1gInPc2
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
|	|	kIx~
<g/>
link	link	k6eAd1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
1902	#num#	k4
<g/>
–	–	k?
<g/>
1911	#num#	k4
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
|	|	kIx~
<g/>
link	link	k6eAd1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
1911	#num#	k4
<g/>
–	–	k?
<g/>
1925	#num#	k4
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
|	|	kIx~
<g/>
link	link	k6eAd1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
1925	#num#	k4
<g/>
–	–	k?
<g/>
1926	#num#	k4
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
|	|	kIx~
<g/>
link	link	k6eAd1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
1926	#num#	k4
<g/>
–	–	k?
<g/>
1931	#num#	k4
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
|	|	kIx~
<g/>
link	link	k6eAd1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
1931	#num#	k4
<g/>
–	–	k?
<g/>
1954	#num#	k4
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
|	|	kIx~
<g/>
link	link	k6eAd1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
1954	#num#	k4
<g/>
–	–	k?
<g/>
1955	#num#	k4
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
|	|	kIx~
<g/>
link	link	k6eAd1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
1955	#num#	k4
<g/>
–	–	k?
</s>
<s>
Úspěchy	úspěch	k1gInPc1
</s>
<s>
Úspěchy	úspěch	k1gInPc1
A	a	k8xC
<g/>
–	–	k?
<g/>
týmu	tým	k1gInSc2
</s>
<s>
Kontinentální	kontinentální	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
</s>
<s>
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
/	/	kIx~
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
evropská	evropský	k2eAgFnSc1d1
výkonnostní	výkonnostní	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
<g/>
;	;	kIx,
13	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
:	:	kIx,
1955	#num#	k4
<g/>
/	/	kIx~
<g/>
56	#num#	k4
<g/>
,	,	kIx,
1956	#num#	k4
<g/>
/	/	kIx~
<g/>
57	#num#	k4
<g/>
,	,	kIx,
1957	#num#	k4
<g/>
/	/	kIx~
<g/>
58	#num#	k4
<g/>
,	,	kIx,
1958	#num#	k4
<g/>
/	/	kIx~
<g/>
59	#num#	k4
<g/>
,	,	kIx,
1959	#num#	k4
<g/>
/	/	kIx~
<g/>
60	#num#	k4
<g/>
,	,	kIx,
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
</s>
<s>
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
:	:	kIx,
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
62	#num#	k4
<g/>
,	,	kIx,
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
64	#num#	k4
<g/>
,	,	kIx,
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
evropská	evropský	k2eAgFnSc1d1
výkonnostní	výkonnostní	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
:	:	kIx,
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
71	#num#	k4
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
/	/	kIx~
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
evropská	evropský	k2eAgFnSc1d1
–	–	k?
1971	#num#	k4
<g/>
–	–	k?
<g/>
99	#num#	k4
<g/>
,	,	kIx,
později	pozdě	k6eAd2
2	#num#	k4
<g/>
.	.	kIx.
evropská	evropský	k2eAgFnSc1d1
výkonnostní	výkonnostní	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
<g/>
;	;	kIx,
2	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
:	:	kIx,
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
</s>
<s>
Latinský	latinský	k2eAgInSc1d1
pohár	pohár	k1gInSc1
(	(	kIx(
<g/>
elitní	elitní	k2eAgFnSc1d1
poválečná	poválečný	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
<g/>
;	;	kIx,
2	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
:	:	kIx,
1955	#num#	k4
<g/>
,	,	kIx,
1957	#num#	k4
</s>
<s>
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
(	(	kIx(
<g/>
4	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
:	:	kIx,
2002	#num#	k4
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
</s>
<s>
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
:	:	kIx,
1998	#num#	k4
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
,	,	kIx,
2018	#num#	k4
</s>
<s>
Interkontinentální	interkontinentální	k2eAgInSc1d1
pohár	pohár	k1gInSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
:	:	kIx,
1960	#num#	k4
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
</s>
<s>
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
:	:	kIx,
1966	#num#	k4
<g/>
,	,	kIx,
2000	#num#	k4
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
klubů	klub	k1gInPc2
(	(	kIx(
<g/>
4	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
:	:	kIx,
2014	#num#	k4
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Domácí	domácí	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
</s>
<s>
Španělská	španělský	k2eAgFnSc1d1
nejvyšší	vysoký	k2eAgFnSc1d3
fotbalová	fotbalový	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
(	(	kIx(
<g/>
Primera	primera	k1gFnSc1
División	División	k1gMnSc1
<g/>
;	;	kIx,
34	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Madrid	Madrid	k1gInSc1
FC	FC	kA
<g/>
:	:	kIx,
1931	#num#	k4
<g/>
/	/	kIx~
<g/>
32	#num#	k4
<g/>
,	,	kIx,
1932	#num#	k4
<g/>
/	/	kIx~
<g/>
33	#num#	k4
<g/>
;	;	kIx,
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
:	:	kIx,
1953	#num#	k4
<g/>
/	/	kIx~
<g/>
54	#num#	k4
<g/>
,	,	kIx,
1954	#num#	k4
<g/>
/	/	kIx~
<g/>
55	#num#	k4
<g/>
,	,	kIx,
1956	#num#	k4
<g/>
/	/	kIx~
<g/>
57	#num#	k4
<g/>
,	,	kIx,
1957	#num#	k4
<g/>
/	/	kIx~
<g/>
58	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
1960	#num#	k4
<g/>
/	/	kIx~
<g/>
61	#num#	k4
<g/>
,	,	kIx,
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
62	#num#	k4
<g/>
,	,	kIx,
1962	#num#	k4
<g/>
/	/	kIx~
<g/>
63	#num#	k4
<g/>
,	,	kIx,
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
64	#num#	k4
<g/>
,	,	kIx,
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
<g/>
,	,	kIx,
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
<g/>
,	,	kIx,
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
69	#num#	k4
<g/>
,	,	kIx,
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
<g/>
,	,	kIx,
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
<g/>
,	,	kIx,
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
<g/>
,	,	kIx,
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
<g/>
,	,	kIx,
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
79	#num#	k4
<g/>
,	,	kIx,
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
<g/>
,	,	kIx,
1985	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
86	#num#	k4
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
</s>
<s>
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
FC	FC	kA
<g/>
:	:	kIx,
1929	#num#	k4
<g/>
;	;	kIx,
Madrid	Madrid	k1gInSc1
FC	FC	kA
<g/>
:	:	kIx,
1933	#num#	k4
<g/>
/	/	kIx~
<g/>
34	#num#	k4
<g/>
,	,	kIx,
1934	#num#	k4
<g/>
/	/	kIx~
<g/>
35	#num#	k4
<g/>
,	,	kIx,
1935	#num#	k4
<g/>
/	/	kIx~
<g/>
36	#num#	k4
<g/>
;	;	kIx,
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
:	:	kIx,
1941	#num#	k4
<g/>
/	/	kIx~
<g/>
42	#num#	k4
<g/>
,	,	kIx,
1944	#num#	k4
<g/>
/	/	kIx~
<g/>
45	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
1958	#num#	k4
<g/>
/	/	kIx~
<g/>
59	#num#	k4
<g/>
,	,	kIx,
1959	#num#	k4
<g/>
/	/	kIx~
<g/>
60	#num#	k4
<g/>
,	,	kIx,
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
<g/>
,	,	kIx,
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
</s>
<s>
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
:	:	kIx,
1948	#num#	k4
<g/>
/	/	kIx~
<g/>
49	#num#	k4
<g/>
,	,	kIx,
1951	#num#	k4
<g/>
/	/	kIx~
<g/>
52	#num#	k4
<g/>
,	,	kIx,
1952	#num#	k4
<g/>
/	/	kIx~
<g/>
53	#num#	k4
<g/>
,	,	kIx,
1955	#num#	k4
<g/>
/	/	kIx~
<g/>
56	#num#	k4
<g/>
,	,	kIx,
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
</s>
<s>
Španělský	španělský	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
(	(	kIx(
<g/>
Copa	Copa	k1gFnSc1
del	del	k?
Generalísimo	Generalísima	k1gFnSc5
<g/>
,	,	kIx,
Copa	Copa	k1gMnSc1
del	del	k?
Rey	Rea	k1gFnSc2
<g/>
;	;	kIx,
19	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Madrid	Madrid	k1gInSc1
FC	FC	kA
<g/>
:	:	kIx,
1905	#num#	k4
<g/>
,	,	kIx,
1906	#num#	k4
<g/>
,	,	kIx,
1907	#num#	k4
<g/>
,	,	kIx,
1908	#num#	k4
<g/>
,	,	kIx,
1917	#num#	k4
<g/>
,	,	kIx,
1934	#num#	k4
<g/>
,	,	kIx,
1936	#num#	k4
<g/>
;	;	kIx,
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
:	:	kIx,
1946	#num#	k4
<g/>
,	,	kIx,
1947	#num#	k4
<g/>
,	,	kIx,
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
62	#num#	k4
<g/>
,	,	kIx,
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
74	#num#	k4
<g/>
,	,	kIx,
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
<g/>
,	,	kIx,
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
<g/>
,	,	kIx,
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
</s>
<s>
Španělský	španělský	k2eAgInSc1d1
ligový	ligový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
(	(	kIx(
<g/>
Copa	Copa	k1gFnSc1
de	de	k?
la	la	k1gNnSc1
Liga	liga	k1gFnSc1
<g/>
;	;	kIx,
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
:	:	kIx,
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
</s>
<s>
Španělský	španělský	k2eAgInSc1d1
Superpohár	superpohár	k1gInSc1
(	(	kIx(
<g/>
Supercopa	Supercopa	k1gFnSc1
de	de	k?
Españ	Españ	k1gFnSc1
<g/>
;	;	kIx,
11	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
:	:	kIx,
1988	#num#	k4
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Menší	malý	k2eAgInPc1d2
úspěchy	úspěch	k1gInPc1
</s>
<s>
Copa	Cop	k2eAgFnSc1d1
Eva	Eva	k1gFnSc1
Duarte	Duart	k1gInSc5
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
:	:	kIx,
1947	#num#	k4
</s>
<s>
Pequeñ	Pequeñ	k2eAgFnSc1d1
Copa	Copa	k1gFnSc1
del	del	k?
Mundo	Mundo	k1gNnSc1
de	de	k?
Clubes	Clubes	k1gMnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
:	:	kIx,
1952	#num#	k4
<g/>
,	,	kIx,
1956	#num#	k4
</s>
<s>
Copa	Cop	k2eAgFnSc1d1
Iberoamericana	Iberoamericana	k1gFnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
:	:	kIx,
1994	#num#	k4
</s>
<s>
Campeonato	Campeonato	k6eAd1
Regional	Regional	k1gFnSc7
Centro	Centro	k1gNnSc4
(	(	kIx(
<g/>
22	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Moderno	Moderna	k1gFnSc5
Football	Football	k1gMnSc1
Club	club	k1gInSc1
<g/>
:	:	kIx,
1902	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
;	;	kIx,
Madrid	Madrid	k1gInSc1
FC	FC	kA
<g/>
:	:	kIx,
1904	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
,	,	kIx,
1905	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
,	,	kIx,
1907	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
,	,	kIx,
1912	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
1915	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
<g/>
,	,	kIx,
1916	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
<g/>
,	,	kIx,
1917	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
,	,	kIx,
1919	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
<g/>
;	;	kIx,
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
FC	FC	kA
<g/>
:	:	kIx,
1921	#num#	k4
<g/>
/	/	kIx~
<g/>
22	#num#	k4
<g/>
,	,	kIx,
1922	#num#	k4
<g/>
/	/	kIx~
<g/>
23	#num#	k4
<g/>
,	,	kIx,
1923	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
24	#num#	k4
<g/>
,	,	kIx,
1925	#num#	k4
<g/>
/	/	kIx~
<g/>
26	#num#	k4
<g/>
,	,	kIx,
1926	#num#	k4
<g/>
/	/	kIx~
<g/>
27	#num#	k4
<g/>
,	,	kIx,
1928	#num#	k4
<g/>
/	/	kIx~
<g/>
29	#num#	k4
<g/>
,	,	kIx,
1929	#num#	k4
<g/>
/	/	kIx~
<g/>
30	#num#	k4
<g/>
,	,	kIx,
1930	#num#	k4
<g/>
/	/	kIx~
<g/>
31	#num#	k4
<g/>
;	;	kIx,
Madrid	Madrid	k1gInSc1
FC	FC	kA
<g/>
:	:	kIx,
1931	#num#	k4
<g/>
/	/	kIx~
<g/>
32	#num#	k4
<g/>
,	,	kIx,
1932	#num#	k4
<g/>
/	/	kIx~
<g/>
33	#num#	k4
<g/>
,	,	kIx,
1933	#num#	k4
<g/>
/	/	kIx~
<g/>
34	#num#	k4
<g/>
,	,	kIx,
1934	#num#	k4
<g/>
/	/	kIx~
<g/>
35	#num#	k4
<g/>
,	,	kIx,
1935	#num#	k4
<g/>
/	/	kIx~
<g/>
36	#num#	k4
</s>
<s>
–	–	k?
Madrid	Madrid	k1gInSc1
FC	FC	kA
<g/>
:	:	kIx,
1902	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
,	,	kIx,
1910	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
,	,	kIx,
1918	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
;	;	kIx,
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
FC	FC	kA
<g/>
:	:	kIx,
1924	#num#	k4
<g/>
/	/	kIx~
<g/>
25	#num#	k4
<g/>
,	,	kIx,
1927	#num#	k4
<g/>
/	/	kIx~
<g/>
28	#num#	k4
<g/>
;	;	kIx,
Madrid	Madrid	k1gInSc1
FC	FC	kA
<g/>
:	:	kIx,
1939	#num#	k4
<g/>
/	/	kIx~
<g/>
40	#num#	k4
</s>
<s>
Copa	Copa	k6eAd1
Federación	Federación	k1gInSc1
Centro	Centro	k1gNnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
FC	FC	kA
<g/>
:	:	kIx,
1922	#num#	k4
<g/>
/	/	kIx~
<g/>
23	#num#	k4
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1927	#num#	k4
<g/>
/	/	kIx~
<g/>
28	#num#	k4
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
;	;	kIx,
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
:	:	kIx,
1943	#num#	k4
<g/>
/	/	kIx~
<g/>
44	#num#	k4
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Trofeo	Trofeo	k6eAd1
Teresa	Teresa	k1gFnSc1
Herrera	Herrera	k1gFnSc1
(	(	kIx(
<g/>
9	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
:	:	kIx,
1949	#num#	k4
<g/>
,	,	kIx,
1953	#num#	k4
<g/>
,	,	kIx,
1966	#num#	k4
<g/>
,	,	kIx,
1976	#num#	k4
<g/>
,	,	kIx,
1978	#num#	k4
<g/>
,	,	kIx,
1979	#num#	k4
<g/>
,	,	kIx,
1980	#num#	k4
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
</s>
<s>
Trofeo	Trofeo	k6eAd1
Ramón	Ramón	k1gInSc1
de	de	k?
Carranza	Carranz	k1gMnSc2
(	(	kIx(
<g/>
6	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
:	:	kIx,
1958	#num#	k4
<g/>
,	,	kIx,
1959	#num#	k4
<g/>
,	,	kIx,
1960	#num#	k4
<g/>
,	,	kIx,
1966	#num#	k4
<g/>
,	,	kIx,
1970	#num#	k4
<g/>
,	,	kIx,
1982	#num#	k4
</s>
<s>
Coupe	coup	k1gInSc5
Mohammed	Mohammed	k1gMnSc1
V	V	kA
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
:	:	kIx,
1966	#num#	k4
</s>
<s>
Trofeo	Trofeo	k6eAd1
Colombino	Colombin	k2eAgNnSc1d1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
:	:	kIx,
1970	#num#	k4
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
,	,	kIx,
1989	#num#	k4
</s>
<s>
Trofeo	Trofeo	k6eAd1
Ciudad	Ciudad	k1gInSc1
de	de	k?
Alicante	Alicant	k1gMnSc5
(	(	kIx(
<g/>
11	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
:	:	kIx,
1972	#num#	k4
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
</s>
<s>
Trofeo	Trofeo	k6eAd1
Santiago	Santiago	k1gNnSc1
Bernabéu	Bernabéus	k1gInSc2
(	(	kIx(
<g/>
28	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
:	:	kIx,
1981	#num#	k4
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
,	,	kIx,
2018	#num#	k4
</s>
<s>
Trofeo	Trofeo	k6eAd1
Cidade	Cidad	k1gInSc5
de	de	k?
Vigo	Vigo	k1gNnSc4
(	(	kIx(
<g/>
2	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
:	:	kIx,
1981	#num#	k4
<g/>
,	,	kIx,
1982	#num#	k4
</s>
<s>
Trofeo	Trofeo	k1gMnSc1
Festa	Festa	k?
d	d	k?
<g/>
'	'	kIx"
<g/>
Elx	Elx	k1gMnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
:	:	kIx,
1984	#num#	k4
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
,	,	kIx,
1999	#num#	k4
</s>
<s>
Trofeo	Trofeo	k1gMnSc1
Naranja	Naranja	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
:	:	kIx,
1990	#num#	k4
</s>
<s>
Trofeo	Trofeo	k6eAd1
Bahía	Bahí	k2eAgFnSc1d1
de	de	k?
Cartagena	Cartagena	k1gFnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
:	:	kIx,
1994	#num#	k4
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
,	,	kIx,
2001	#num#	k4
</s>
<s>
Trofeo	Trofeo	k6eAd1
Pirelli	Pirell	k1gMnPc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
:	:	kIx,
1999	#num#	k4
</s>
<s>
Memorial	Memorial	k1gMnSc1
Jesús	Jesúsa	k1gFnPc2
Gil	Gil	k1gMnSc1
y	y	k?
Gil	Gil	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
:	:	kIx,
2005	#num#	k4
</s>
<s>
Franz	Franz	k1gMnSc1
Beckenbauer	Beckenbauer	k1gMnSc1
Cup	cup	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
:	:	kIx,
2010	#num#	k4
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
World	World	k6eAd1
Football	Football	k1gInSc1
Challenge	Challeng	k1gFnSc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
:	:	kIx,
2011	#num#	k4
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
International	Internationat	k5eAaImAgInS,k5eAaPmAgInS
Champions	Champions	k1gInSc1
Cup	cup	k1gInSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
:	:	kIx,
2013	#num#	k4
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2015	#num#	k4
Austrálie	Austrálie	k1gFnSc1
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2015	#num#	k4
Čína	Čína	k1gFnSc1
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Major	major	k1gMnSc1
League	League	k1gNnSc2
Soccer	Soccer	k1gMnSc1
All-Star	All-Star	k1gMnSc1
Game	game	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
:	:	kIx,
2017	#num#	k4
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Úspěchy	úspěch	k1gInPc1
mládeže	mládež	k1gFnSc2
</s>
<s>
Domácí	domácí	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
</s>
<s>
Španělská	španělský	k2eAgFnSc1d1
nejvyšší	vysoký	k2eAgFnSc1d3
fotbalová	fotbalový	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
do	do	k7c2
19	#num#	k4
let	léto	k1gNnPc2
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
ligová	ligový	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
/	/	kIx~
od	od	k7c2
roku	rok	k1gInSc2
1995	#num#	k4
jako	jako	k8xC,k8xS
turnaj	turnaj	k1gInSc1
Copa	Cop	k1gInSc2
de	de	k?
Campeones	Campeones	k1gMnSc1
<g/>
;	;	kIx,
11	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
:	:	kIx,
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
</s>
<s>
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
:	:	kIx,
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
</s>
<s>
Španělská	španělský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
ligová	ligový	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
do	do	k7c2
19	#num#	k4
let	léto	k1gNnPc2
(	(	kIx(
<g/>
kvalifikace	kvalifikace	k1gFnSc2
do	do	k7c2
soutěže	soutěž	k1gFnSc2
Copa	Copa	k1gMnSc1
de	de	k?
Campeones	Campeones	k1gMnSc1
<g/>
;	;	kIx,
12	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
:	:	kIx,
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
(	(	kIx(
<g/>
sk	sk	k?
<g/>
.	.	kIx.
5	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
(	(	kIx(
<g/>
sk	sk	k?
<g/>
.	.	kIx.
5	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
(	(	kIx(
<g/>
sk	sk	k?
<g/>
.	.	kIx.
5	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
(	(	kIx(
<g/>
sk	sk	k?
<g/>
.	.	kIx.
5	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
(	(	kIx(
<g/>
sk	sk	k?
<g/>
.	.	kIx.
5	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
(	(	kIx(
<g/>
sk	sk	k?
<g/>
.	.	kIx.
5	#num#	k4
<g/>
)	)	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
(	(	kIx(
<g/>
sk	sk	k?
<g/>
.	.	kIx.
5	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
(	(	kIx(
<g/>
sk	sk	k?
<g/>
.	.	kIx.
5	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
(	(	kIx(
<g/>
sk	sk	k?
<g/>
.	.	kIx.
5	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
(	(	kIx(
<g/>
sk	sk	k?
<g/>
.	.	kIx.
5	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
(	(	kIx(
<g/>
sk	sk	k?
<g/>
.	.	kIx.
5	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
(	(	kIx(
<g/>
sk	sk	k?
<g/>
.	.	kIx.
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Španělský	španělský	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
do	do	k7c2
19	#num#	k4
let	léto	k1gNnPc2
(	(	kIx(
<g/>
Copa	Copa	k1gFnSc1
del	del	k?
Rey	Rea	k1gFnSc2
Juvenil	Juvenil	k1gFnSc2
<g/>
;	;	kIx,
13	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
:	:	kIx,
1953	#num#	k4
<g/>
,	,	kIx,
1968	#num#	k4
<g/>
,	,	kIx,
1969	#num#	k4
<g/>
,	,	kIx,
1971	#num#	k4
<g/>
,	,	kIx,
1978	#num#	k4
<g/>
,	,	kIx,
1981	#num#	k4
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
</s>
<s>
Hráči	hráč	k1gMnPc1
</s>
<s>
Současná	současný	k2eAgFnSc1d1
soupiska	soupiska	k1gFnSc1
</s>
<s>
K	k	k7c3
5	#num#	k4
<g/>
.	.	kIx.
říjnu	říjen	k1gInSc3
2020	#num#	k4
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Španělské	španělský	k2eAgInPc1d1
kluby	klub	k1gInPc1
jsou	být	k5eAaImIp3nP
limitovány	limitovat	k5eAaBmNgInP
pravidly	pravidlo	k1gNnPc7
nařizující	nařizující	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
každý	každý	k3xTgInSc1
klub	klub	k1gInSc1
smí	smět	k5eAaImIp3nS
mít	mít	k5eAaImF
na	na	k7c6
soupisce	soupiska	k1gFnSc6
pouze	pouze	k6eAd1
tři	tři	k4xCgMnPc4
hráče	hráč	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
mají	mít	k5eAaImIp3nP
občanství	občanství	k1gNnSc4
ve	v	k7c6
státech	stát	k1gInPc6
mimo	mimo	k7c4
Evropskou	evropský	k2eAgFnSc4d1
unii	unie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
pravidlo	pravidlo	k1gNnSc1
se	se	k3xPyFc4
nevztahuje	vztahovat	k5eNaImIp3nS
na	na	k7c4
hráče	hráč	k1gMnPc4
s	s	k7c7
dvojím	dvojí	k4xRgInSc7
občanstvím	občanství	k1gNnSc7
a	a	k8xC
na	na	k7c4
hráče	hráč	k1gMnPc4
z	z	k7c2
tzv.	tzv.	kA
AKT	akt	k1gInSc1
(	(	kIx(
<g/>
Africká	africký	k2eAgFnSc1d1
<g/>
,	,	kIx,
Karibská	karibský	k2eAgFnSc1d1
a	a	k8xC
Pacifická	pacifický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
států	stát	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Číslo	číslo	k1gNnSc1
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
Hráč	hráč	k1gMnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
B	B	kA
</s>
<s>
Thibaut	Thibaut	k2eAgInSc1d1
Courtois	Courtois	k1gInSc1
</s>
<s>
2	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Dani	daň	k1gFnSc3
Carvajal	Carvajal	k1gMnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Éder	Éder	k1gMnSc1
Militã	Militã	k1gMnSc1
</s>
<s>
4	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Sergio	Sergio	k1gMnSc1
Ramos	Ramos	k1gMnSc1
(	(	kIx(
<g/>
kapitán	kapitán	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
5	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Raphaël	Raphaëlit	k5eAaPmRp2nS
Varane	varan	k1gMnSc5
(	(	kIx(
<g/>
čtvrtý	čtvrtý	k4xOgMnSc1
kapitán	kapitán	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
6	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Nacho	Nacze	k6eAd1
</s>
<s>
7	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Eden	Eden	k1gInSc1
Hazard	hazard	k2eAgFnSc2d1
</s>
<s>
8	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Toni	Toni	k1gMnSc1
Kroos	Kroos	k1gMnSc1
</s>
<s>
9	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Karim	Karim	k6eAd1
Benzema	Benzema	k1gNnSc1
(	(	kIx(
<g/>
třetí	třetí	k4xOgMnSc1
kapitán	kapitán	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
10	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Luka	luka	k1gNnPc1
Modrić	Modrić	k1gFnSc2
</s>
<s>
11	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Marco	Marco	k1gMnSc1
Asensio	Asensio	k1gMnSc1
</s>
<s>
12	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Marcelo	Marcela	k1gFnSc5
(	(	kIx(
<g/>
zástupce	zástupce	k1gMnSc4
kapitána	kapitán	k1gMnSc4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Číslo	číslo	k1gNnSc1
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
Hráč	hráč	k1gMnSc1
</s>
<s>
13	#num#	k4
</s>
<s>
B	B	kA
</s>
<s>
Andriy	Andria	k1gFnPc1
Lunin	Lunina	k1gFnPc2
</s>
<s>
14	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Casemiro	Casemiro	k6eAd1
</s>
<s>
15	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Federico	Federica	k1gMnSc5
Valverde	Valverd	k1gMnSc5
</s>
<s>
17	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Lucas	Lucas	k1gMnSc1
Vázquez	Vázquez	k1gMnSc1
</s>
<s>
18	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Luka	luka	k1gNnPc1
Jović	Jović	k1gFnSc2
</s>
<s>
19	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Álvaro	Álvara	k1gFnSc5
Odriozola	Odriozola	k1gFnSc1
</s>
<s>
20	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Vinícius	Vinícius	k1gMnSc1
Júnior	Júnior	k1gMnSc1
</s>
<s>
21	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Martin	Martin	k1gMnSc1
Ø	Ø	k1gMnSc1
</s>
<s>
22	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Isco	Isco	k6eAd1
</s>
<s>
23	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Ferland	Ferland	k1gInSc1
Mendy	Menda	k1gFnSc2
</s>
<s>
24	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Mariano	Mariana	k1gFnSc5
</s>
<s>
27	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Rodrygo	Rodrygo	k6eAd1
</s>
<s>
Hráči	hráč	k1gMnPc1
na	na	k7c6
hostování	hostování	k1gNnSc6
</s>
<s>
Číslo	číslo	k1gNnSc1
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
Hráč	hráč	k1gMnSc1
</s>
<s>
—	—	k?
</s>
<s>
O	o	k7c6
</s>
<s>
Fran	Fran	k1gMnSc1
García	García	k1gMnSc1
(	(	kIx(
<g/>
v	v	k7c6
Rayo	Rayo	k6eAd1
Vallecano	Vallecana	k1gFnSc5
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
—	—	k?
</s>
<s>
O	o	k7c6
</s>
<s>
Manu	manout	k5eAaImIp1nS
Hernando	Hernanda	k1gFnSc5
(	(	kIx(
<g/>
v	v	k7c6
SD	SD	kA
Ponferradina	Ponferradina	k1gFnSc1
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
—	—	k?
</s>
<s>
O	o	k7c6
</s>
<s>
Jesús	Jesús	k6eAd1
Vallejo	Vallejo	k6eAd1
(	(	kIx(
<g/>
v	v	k7c6
Granada	Granada	k1gFnSc1
CF	CF	kA
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
—	—	k?
</s>
<s>
Z	z	k7c2
</s>
<s>
Martín	Martín	k1gMnSc1
Calderón	Calderón	k1gMnSc1
(	(	kIx(
<g/>
v	v	k7c4
Paços	Paços	k1gInSc4
de	de	k?
Ferreira	Ferreir	k1gInSc2
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
—	—	k?
</s>
<s>
Z	z	k7c2
</s>
<s>
Dani	daň	k1gFnSc3
Ceballos	Ceballos	k1gMnSc1
(	(	kIx(
<g/>
v	v	k7c6
Arsenalu	Arsenal	k1gInSc6
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
—	—	k?
</s>
<s>
Z	z	k7c2
</s>
<s>
Franchu	Francha	k1gFnSc4
(	(	kIx(
<g/>
v	v	k7c6
CF	CF	kA
Fuenlabrada	Fuenlabrada	k1gFnSc1
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Číslo	číslo	k1gNnSc1
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
Hráč	hráč	k1gMnSc1
</s>
<s>
—	—	k?
</s>
<s>
Z	z	k7c2
</s>
<s>
Takefusa	Takefus	k1gMnSc2
Kubo	kubo	k1gNnSc1
(	(	kIx(
<g/>
ve	v	k7c6
Villarrealu	Villarreal	k1gInSc6
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
—	—	k?
</s>
<s>
Z	z	k7c2
</s>
<s>
Reinier	Reinier	k1gInSc1
(	(	kIx(
<g/>
v	v	k7c6
Borussii	Borussie	k1gFnSc6
Dortmund	Dortmund	k1gInSc1
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2022	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
—	—	k?
</s>
<s>
Ú	Ú	kA
</s>
<s>
Gareth	Gareth	k1gMnSc1
Bale	bal	k1gInSc5
(	(	kIx(
<g/>
v	v	k7c6
Tottenhamu	Tottenham	k1gInSc6
Hotspur	Hotspura	k1gFnPc2
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
—	—	k?
</s>
<s>
Ú	Ú	kA
</s>
<s>
Brahim	Brahim	k1gMnSc1
Díaz	Díaz	k1gMnSc1
(	(	kIx(
<g/>
v	v	k7c6
AC	AC	kA
Milán	Milán	k1gInSc1
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
—	—	k?
</s>
<s>
Ú	Ú	kA
</s>
<s>
Sergio	Sergio	k1gMnSc1
Díaz	Díaz	k1gMnSc1
(	(	kIx(
<g/>
v	v	k7c6
Clubu	club	k1gInSc6
América	Améric	k1gInSc2
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
—	—	k?
</s>
<s>
Ú	Ú	kA
</s>
<s>
Borja	Borj	k2eAgFnSc1d1
Mayoral	Mayoral	k1gFnSc1
(	(	kIx(
<g/>
v	v	k7c6
AS	as	k1gNnSc6
Řím	Řím	k1gInSc4
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2022	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Realizační	realizační	k2eAgInSc1d1
tým	tým	k1gInSc1
</s>
<s>
Trenérský	trenérský	k2eAgInSc1d1
tým	tým	k1gInSc1
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
Jméno	jméno	k1gNnSc1
</s>
<s>
Hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Zinedine	Zinedin	k1gMnSc5
Zidane	Zidan	k1gMnSc5
</s>
<s>
Asistent	asistent	k1gMnSc1
trenéra	trenér	k1gMnSc2
</s>
<s>
David	David	k1gMnSc1
Bettoni	Bettoň	k1gFnSc3
</s>
<s>
Hamidou	Hamida	k1gFnSc7
Msaidie	Msaidie	k1gFnSc2
</s>
<s>
Trenér	trenér	k1gMnSc1
brankářů	brankář	k1gMnPc2
</s>
<s>
Roberto	Roberta	k1gFnSc5
Vázquez	Vázquez	k1gInSc1
</s>
<s>
Kondiční	kondiční	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Grégory	Grégora	k1gFnPc1
Dupont	Duponta	k1gFnPc2
</s>
<s>
Umístění	umístění	k1gNnSc1
v	v	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
sezónách	sezóna	k1gFnPc6
</s>
<s>
1929	#num#	k4
<g/>
–	–	k?
:	:	kIx,
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
(	(	kIx(
<g/>
1929	#num#	k4
–	–	k?
<g/>
)	)	kIx)
</s>
<s>
Sezóny	sezóna	k1gFnPc1
</s>
<s>
Liga	liga	k1gFnSc1
</s>
<s>
Úroveň	úroveň	k1gFnSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
V	v	k7c6
</s>
<s>
R	R	kA
</s>
<s>
P	P	kA
</s>
<s>
VG	VG	kA
</s>
<s>
OG	OG	kA
</s>
<s>
+	+	kIx~
<g/>
/	/	kIx~
<g/>
-	-	kIx~
</s>
<s>
B	B	kA
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
1929	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
11811164027	#num#	k4
<g/>
+	+	kIx~
<g/>
1323	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1929	#num#	k4
<g/>
/	/	kIx~
<g/>
30	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
1187384542	#num#	k4
<g/>
+	+	kIx~
<g/>
317	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1930	#num#	k4
<g/>
/	/	kIx~
<g/>
31	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
1187472427-318	1187472427-318	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1931	#num#	k4
<g/>
/	/	kIx~
<g/>
32	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
11810803715	#num#	k4
<g/>
+	+	kIx~
<g/>
2228	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1932	#num#	k4
<g/>
/	/	kIx~
<g/>
33	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
11813234917	#num#	k4
<g/>
+	+	kIx~
<g/>
3228	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1933	#num#	k4
<g/>
/	/	kIx~
<g/>
34	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
11810264129	#num#	k4
<g/>
+	+	kIx~
<g/>
1222	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1934	#num#	k4
<g/>
/	/	kIx~
<g/>
35	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
12216156134	#num#	k4
<g/>
+	+	kIx~
<g/>
2733	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1935	#num#	k4
<g/>
/	/	kIx~
<g/>
36	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
12213366235	#num#	k4
<g/>
+	+	kIx~
<g/>
2729	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Španělské	španělský	k2eAgFnPc1d1
soutěže	soutěž	k1gFnPc1
se	se	k3xPyFc4
v	v	k7c6
letech	let	k1gInPc6
1936	#num#	k4
<g/>
–	–	k?
<g/>
39	#num#	k4
nehrály	hrát	k5eNaImAgInP
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
kvůli	kvůli	k7c3
právě	právě	k9
probíhající	probíhající	k2eAgFnSc3d1
občanské	občanský	k2eAgFnSc3d1
válce	válka	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
1939	#num#	k4
<g/>
/	/	kIx~
<g/>
40	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
12212194735	#num#	k4
<g/>
+	+	kIx~
<g/>
1225	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1940	#num#	k4
<g/>
/	/	kIx~
<g/>
41	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
12211295138	#num#	k4
<g/>
+	+	kIx~
<g/>
1324	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1941	#num#	k4
<g/>
/	/	kIx~
<g/>
42	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
12614576543	#num#	k4
<g/>
+	+	kIx~
<g/>
2233	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1942	#num#	k4
<g/>
/	/	kIx~
<g/>
43	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
126105115250	#num#	k4
<g/>
+	+	kIx~
<g/>
225	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1943	#num#	k4
<g/>
/	/	kIx~
<g/>
44	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
12611694838	#num#	k4
<g/>
+	+	kIx~
<g/>
1028	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1944	#num#	k4
<g/>
/	/	kIx~
<g/>
45	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
12618266835	#num#	k4
<g/>
+	+	kIx~
<g/>
3338	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1945	#num#	k4
<g/>
/	/	kIx~
<g/>
46	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
12611964630	#num#	k4
<g/>
+	+	kIx~
<g/>
1631	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1946	#num#	k4
<g/>
/	/	kIx~
<g/>
47	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
126115106256	#num#	k4
<g/>
+	+	kIx~
<g/>
627	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1947	#num#	k4
<g/>
/	/	kIx~
<g/>
48	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
12677124156-1521	12677124156-1521	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1948	#num#	k4
<g/>
/	/	kIx~
<g/>
49	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
12615476742	#num#	k4
<g/>
+	+	kIx~
<g/>
2534	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1949	#num#	k4
<g/>
/	/	kIx~
<g/>
50	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
12611966049	#num#	k4
<g/>
+	+	kIx~
<g/>
1131	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1950	#num#	k4
<g/>
/	/	kIx~
<g/>
51	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
132155128071	#num#	k4
<g/>
+	+	kIx~
<g/>
935	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1951	#num#	k4
<g/>
/	/	kIx~
<g/>
52	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13016687950	#num#	k4
<g/>
+	+	kIx~
<g/>
2938	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1952	#num#	k4
<g/>
/	/	kIx~
<g/>
53	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13018396749	#num#	k4
<g/>
+	+	kIx~
<g/>
1839	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1953	#num#	k4
<g/>
/	/	kIx~
<g/>
54	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13017677241	#num#	k4
<g/>
+	+	kIx~
<g/>
3140	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1954	#num#	k4
<g/>
/	/	kIx~
<g/>
55	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13020648031	#num#	k4
<g/>
+	+	kIx~
<g/>
4946	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1955	#num#	k4
<g/>
/	/	kIx~
<g/>
56	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
130182108139	#num#	k4
<g/>
+	+	kIx~
<g/>
4238	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1956	#num#	k4
<g/>
/	/	kIx~
<g/>
57	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13020467435	#num#	k4
<g/>
+	+	kIx~
<g/>
3944	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1957	#num#	k4
<g/>
/	/	kIx~
<g/>
58	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13020557126	#num#	k4
<g/>
+	+	kIx~
<g/>
4545	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1958	#num#	k4
<g/>
/	/	kIx~
<g/>
59	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13021548929	#num#	k4
<g/>
+	+	kIx~
<g/>
6047	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1959	#num#	k4
<g/>
/	/	kIx~
<g/>
60	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13021459636	#num#	k4
<g/>
+	+	kIx~
<g/>
6046	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1960	#num#	k4
<g/>
/	/	kIx~
<g/>
61	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13024428925	#num#	k4
<g/>
+	+	kIx~
<g/>
6452	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
62	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13019565824	#num#	k4
<g/>
+	+	kIx~
<g/>
3443	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1962	#num#	k4
<g/>
/	/	kIx~
<g/>
63	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13023348333	#num#	k4
<g/>
+	+	kIx~
<g/>
5049	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
64	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13022266123	#num#	k4
<g/>
+	+	kIx~
<g/>
3846	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13021546418	#num#	k4
<g/>
+	+	kIx~
<g/>
4647	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13019565330	#num#	k4
<g/>
+	+	kIx~
<g/>
2343	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13019925822	#num#	k4
<g/>
+	+	kIx~
<g/>
3647	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
130161045526	#num#	k4
<g/>
+	+	kIx~
<g/>
2942	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
69	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
130181114621	#num#	k4
<g/>
+	+	kIx~
<g/>
2547	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13013985042	#num#	k4
<g/>
+	+	kIx~
<g/>
835	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
71	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13017764624	#num#	k4
<g/>
+	+	kIx~
<g/>
2241	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13419965127	#num#	k4
<g/>
+	+	kIx~
<g/>
2447	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13417984529	#num#	k4
<g/>
+	+	kIx~
<g/>
1643	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
74	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
134138134838	#num#	k4
<g/>
+	+	kIx~
<g/>
1034	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
134201046634	#num#	k4
<g/>
+	+	kIx~
<g/>
3250	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13420865426	#num#	k4
<g/>
+	+	kIx~
<g/>
2848	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
1341210125753	#num#	k4
<g/>
+	+	kIx~
<g/>
434	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13422397740	#num#	k4
<g/>
+	+	kIx~
<g/>
3747	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
79	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
134161536136	#num#	k4
<g/>
+	+	kIx~
<g/>
2547	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13422937033	#num#	k4
<g/>
+	+	kIx~
<g/>
3753	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13420596637	#num#	k4
<g/>
+	+	kIx~
<g/>
2945	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13418885734	#num#	k4
<g/>
+	+	kIx~
<g/>
2344	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13420955725	#num#	k4
<g/>
+	+	kIx~
<g/>
3249	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13422575937	#num#	k4
<g/>
+	+	kIx~
<g/>
2249	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
1341310114636	#num#	k4
<g/>
+	+	kIx~
<g/>
1036	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13426448333	#num#	k4
<g/>
+	+	kIx~
<g/>
5056	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
144271258437	#num#	k4
<g/>
+	+	kIx~
<g/>
4766	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13828649526	#num#	k4
<g/>
+	+	kIx~
<g/>
6962	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138251219137	#num#	k4
<g/>
+	+	kIx~
<g/>
5462	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
1382610210738	#num#	k4
<g/>
+	+	kIx~
<g/>
6962	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138206126337	#num#	k4
<g/>
+	+	kIx~
<g/>
2646	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13823877832	#num#	k4
<g/>
+	+	kIx~
<g/>
4654	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13824957528	#num#	k4
<g/>
+	+	kIx~
<g/>
4757	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138197126150	#num#	k4
<g/>
+	+	kIx~
<g/>
1145	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13823967629	#num#	k4
<g/>
+	+	kIx~
<g/>
4755	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
1422010127551	#num#	k4
<g/>
+	+	kIx~
<g/>
2470	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
142271148536	#num#	k4
<g/>
+	+	kIx~
<g/>
4992	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138171296345	#num#	k4
<g/>
+	+	kIx~
<g/>
1863	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138215127762	#num#	k4
<g/>
+	+	kIx~
<g/>
1568	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138161485848	#num#	k4
<g/>
+	+	kIx~
<g/>
1062	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13824868140	#num#	k4
<g/>
+	+	kIx~
<g/>
4180	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138199106944	#num#	k4
<g/>
+	+	kIx~
<g/>
2566	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138221248642	#num#	k4
<g/>
+	+	kIx~
<g/>
4478	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138217107254	#num#	k4
<g/>
+	+	kIx~
<g/>
1870	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13825587132	#num#	k4
<g/>
+	+	kIx~
<g/>
3980	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138201087040	#num#	k4
<g/>
+	+	kIx~
<g/>
3070	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13823786640	#num#	k4
<g/>
+	+	kIx~
<g/>
2676	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13827478436	#num#	k4
<g/>
+	+	kIx~
<g/>
4885	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138253108352	#num#	k4
<g/>
+	+	kIx~
<g/>
3178	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138313410235	#num#	k4
<g/>
+	+	kIx~
<g/>
6796	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138295410233	#num#	k4
<g/>
+	+	kIx~
<g/>
6992	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138324212132	#num#	k4
<g/>
+	+	kIx~
<g/>
89100	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138267510342	#num#	k4
<g/>
+	+	kIx~
<g/>
6185	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138276510438	#num#	k4
<g/>
+	+	kIx~
<g/>
6687	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138302611838	#num#	k4
<g/>
+	+	kIx~
<g/>
8092	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138286411034	#num#	k4
<g/>
+	+	kIx~
<g/>
7690	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138296310641	#num#	k4
<g/>
+	+	kIx~
<g/>
6593	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138221069444	#num#	k4
<g/>
+	+	kIx~
<g/>
5076	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138215126346	#num#	k4
<g/>
+	+	kIx~
<g/>
1768	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13826937025	#num#	k4
<g/>
+	+	kIx~
<g/>
4587	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138	#num#	k4
</s>
<s>
Účast	účast	k1gFnSc1
v	v	k7c6
evropských	evropský	k2eAgInPc6d1
pohárech	pohár	k1gInPc6
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Výsledky	výsledek	k1gInPc1
Realu	Real	k1gInSc6
Madrid	Madrid	k1gInSc1
v	v	k7c6
evropských	evropský	k2eAgInPc6d1
fotbalových	fotbalový	k2eAgInPc6d1
pohárech	pohár	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Statistická	statistický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
</s>
<s>
Aktuální	aktuální	k2eAgInPc1d1
k	k	k7c3
7	#num#	k4
<g/>
.	.	kIx.
srpnu	srpen	k1gInSc3
2020	#num#	k4
</s>
<s>
Legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
VG	VG	kA
–	–	k?
Vstřelené	vstřelený	k2eAgInPc1d1
góly	gól	k1gInPc1
<g/>
,	,	kIx,
OG	OG	kA
–	–	k?
Obdržené	obdržený	k2eAgInPc1d1
góly	gól	k1gInPc1
<g/>
,	,	kIx,
GR	GR	kA
–	–	k?
Gólový	gólový	k2eAgInSc4d1
rozdíl	rozdíl	k1gInSc4
</s>
<s>
SoutěžZápasyVýhryRemízyProhryVGOGGRÚspěšnost	SoutěžZápasyVýhryRemízyProhryVGOGGRÚspěšnost	k1gFnSc1
%	%	kIx~
</s>
<s>
PMEZ	PMEZ	kA
<g/>
/	/	kIx~
<g/>
LM	LM	kA
</s>
<s>
43926276101973480	#num#	k4
<g/>
+	+	kIx~
<g/>
19359,68	19359,68	k4
</s>
<s>
PVP	PVP	kA
</s>
<s>
3116965724	#num#	k4
<g/>
+	+	kIx~
<g/>
3351,61	3351,61	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
6433102111175	#num#	k4
<g/>
+	+	kIx~
<g/>
3651,56	3651,56	k4
</s>
<s>
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
74031311	#num#	k4
<g/>
+	+	kIx~
<g/>
257,14	257,14	k4
</s>
<s>
Interkontinentální	interkontinentální	k2eAgInSc4d1
pohár	pohár	k1gInSc4
</s>
<s>
7	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
+2	+2	k4
</s>
<s>
42,86	42,86	k4
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
klubů	klub	k1gInPc2
</s>
<s>
12	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
31	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
+20	+20	k4
</s>
<s>
83,33	83,33	k4
</s>
<s>
Celkem	celkem	k6eAd1
</s>
<s>
560328981341195609	#num#	k4
<g/>
+	+	kIx~
<g/>
58658,57	58658,57	k4
</s>
<s>
1950	#num#	k4
<g/>
–	–	k?
<g/>
1970	#num#	k4
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
LaP	lap	k1gInSc1
–	–	k?
Latinský	latinský	k2eAgInSc1d1
pohár	pohár	k1gInSc1
<g/>
,	,	kIx,
VP	VP	kA
–	–	k?
Veletržní	veletržní	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
,	,	kIx,
PMEZ	PMEZ	kA
–	–	k?
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
PVP	PVP	kA
–	–	k?
Pohár	pohár	k1gInSc4
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
<g/>
,	,	kIx,
LM	LM	kA
–	–	k?
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
<g/>
,	,	kIx,
UEFA	UEFA	kA
–	–	k?
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
<g/>
,	,	kIx,
EL	Ela	k1gFnPc2
–	–	k?
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
<g/>
,	,	kIx,
SP	SP	kA
<g />
.	.	kIx.
</s>
<s hack="1">
–	–	k?
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
<g/>
,	,	kIx,
PI	pi	k0
–	–	k?
Pohár	pohár	k1gInSc4
Intertoto	Intertota	k1gFnSc5
<g/>
,	,	kIx,
IP	IP	kA
–	–	k?
Interkontinentální	interkontinentální	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
,	,	kIx,
MS	MS	kA
–	–	k?
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
klubů	klub	k1gInPc2
Pozn	pozn	kA
<g/>
.	.	kIx.
<g/>
:	:	kIx,
V	v	k7c6
závorce	závorka	k1gFnSc6
u	u	k7c2
daného	daný	k2eAgNnSc2d1
ročníkového	ročníkový	k2eAgNnSc2d1
maxima	maximum	k1gNnSc2
je	být	k5eAaImIp3nS
uveden	uveden	k2eAgMnSc1d1
přemožitel	přemožitel	k1gMnSc1
daného	daný	k2eAgInSc2d1
klubu	klub	k1gInSc2
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
<g/>
–	–	k?
<g/>
li	li	k8xS
v	v	k7c6
závorce	závorka	k1gFnSc6
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
LaP	lap	k1gInSc1
1955	#num#	k4
–	–	k?
Vítěz	vítěz	k1gMnSc1
(	(	kIx(
<g/>
poražený	poražený	k2eAgMnSc1d1
finalista	finalista	k1gMnSc1
Stade	Stad	k1gInSc5
de	de	k?
Reims	Reims	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
PMEZ	PMEZ	kA
1955	#num#	k4
<g/>
/	/	kIx~
<g/>
1956	#num#	k4
–	–	k?
Vítěz	vítěz	k1gMnSc1
(	(	kIx(
<g/>
poražený	poražený	k2eAgMnSc1d1
finalista	finalista	k1gMnSc1
Stade	Stad	k1gInSc5
de	de	k?
Reims	Reims	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
PMEZ	PMEZ	kA
1956	#num#	k4
<g/>
/	/	kIx~
<g/>
1957	#num#	k4
–	–	k?
Vítěz	vítěz	k1gMnSc1
(	(	kIx(
<g/>
poražený	poražený	k2eAgMnSc1d1
finalista	finalista	k1gMnSc1
ACF	ACF	kA
Fiorentina	Fiorentina	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
LaP	lap	k1gInSc1
1957	#num#	k4
–	–	k?
Vítěz	vítěz	k1gMnSc1
(	(	kIx(
<g/>
poražený	poražený	k2eAgMnSc1d1
finalista	finalista	k1gMnSc1
SL	SL	kA
Benfica	Benfica	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
PMEZ	PMEZ	kA
1957	#num#	k4
<g/>
/	/	kIx~
<g/>
1958	#num#	k4
–	–	k?
Vítěz	vítěz	k1gMnSc1
(	(	kIx(
<g/>
poražený	poražený	k2eAgMnSc1d1
finalista	finalista	k1gMnSc1
AC	AC	kA
Milán	Milán	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
PMEZ	PMEZ	kA
1958	#num#	k4
<g/>
/	/	kIx~
<g/>
1959	#num#	k4
–	–	k?
Vítěz	vítěz	k1gMnSc1
(	(	kIx(
<g/>
poražený	poražený	k2eAgMnSc1d1
finalista	finalista	k1gMnSc1
Stade	Stad	k1gInSc5
de	de	k?
Reims	Reims	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
PMEZ	PMEZ	kA
1959	#num#	k4
<g/>
/	/	kIx~
<g/>
1960	#num#	k4
–	–	k?
Vítěz	vítěz	k1gMnSc1
(	(	kIx(
<g/>
poražený	poražený	k2eAgMnSc1d1
finalista	finalista	k1gMnSc1
Eintracht	Eintracht	k2eAgInSc4d1
Frankfurt	Frankfurt	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
IP	IP	kA
1960	#num#	k4
–	–	k?
Vítěz	vítěz	k1gMnSc1
(	(	kIx(
<g/>
poražený	poražený	k2eAgMnSc1d1
finalista	finalista	k1gMnSc1
CA	ca	kA
Peñ	Peñ	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
PMEZ	PMEZ	kA
1960	#num#	k4
<g/>
/	/	kIx~
<g/>
1961	#num#	k4
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
(	(	kIx(
<g/>
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
PMEZ	PMEZ	kA
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
1962	#num#	k4
–	–	k?
Finále	finále	k1gNnSc1
(	(	kIx(
<g/>
SL	SL	kA
Benfica	Benfica	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
PMEZ	PMEZ	kA
1962	#num#	k4
<g/>
/	/	kIx~
<g/>
1963	#num#	k4
–	–	k?
Předkolo	předkolo	k1gNnSc4
(	(	kIx(
<g/>
RSC	RSC	kA
Anderlecht	Anderlecht	k2eAgInSc1d1
<g/>
)	)	kIx)
</s>
<s>
PMEZ	PMEZ	kA
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
1964	#num#	k4
–	–	k?
Finále	finále	k1gNnSc1
(	(	kIx(
<g/>
Inter	Inter	k1gInSc1
Milán	Milán	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
PMEZ	PMEZ	kA
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
1965	#num#	k4
–	–	k?
Čtvrtfinále	čtvrtfinále	k1gNnSc1
(	(	kIx(
<g/>
SL	SL	kA
Benfica	Benfica	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
PMEZ	PMEZ	kA
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
1966	#num#	k4
–	–	k?
Vítěz	vítěz	k1gMnSc1
(	(	kIx(
<g/>
poražený	poražený	k2eAgMnSc1d1
finalista	finalista	k1gMnSc1
FK	FK	kA
Partizan	Partizan	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
IP	IP	kA
1966	#num#	k4
–	–	k?
Finále	finále	k1gNnSc1
(	(	kIx(
<g/>
CA	ca	kA
Peñ	Peñ	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
PMEZ	PMEZ	kA
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
1967	#num#	k4
–	–	k?
Čtvrtfinále	čtvrtfinále	k1gNnSc1
(	(	kIx(
<g/>
Inter	Inter	k1gInSc1
Milán	Milán	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
PMEZ	PMEZ	kA
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
1968	#num#	k4
–	–	k?
Semifinále	semifinále	k1gNnSc1
(	(	kIx(
<g/>
Manchester	Manchester	k1gInSc1
United	United	k1gInSc1
FC	FC	kA
<g/>
)	)	kIx)
</s>
<s>
PMEZ	PMEZ	kA
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
1969	#num#	k4
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
(	(	kIx(
<g/>
SK	Sk	kA
Rapid	rapid	k1gInSc1
Wien	Wien	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
PMEZ	PMEZ	kA
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
1970	#num#	k4
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
(	(	kIx(
<g/>
Standard	standard	k1gInSc1
Liè	Liè	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
1970	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
VP	VP	kA
–	–	k?
Veletržní	veletržní	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
,	,	kIx,
PMEZ	PMEZ	kA
–	–	k?
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
PVP	PVP	kA
–	–	k?
Pohár	pohár	k1gInSc4
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
<g/>
,	,	kIx,
LM	LM	kA
–	–	k?
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
<g/>
,	,	kIx,
UEFA	UEFA	kA
–	–	k?
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
<g/>
,	,	kIx,
EL	Ela	k1gFnPc2
–	–	k?
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
<g/>
,	,	kIx,
SP	SP	kA
–	–	k?
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
<g/>
,	,	kIx,
PI	pi	k0
–	–	k?
Pohár	pohár	k1gInSc4
Intertoto	Intertota	k1gFnSc5
<g/>
,	,	kIx,
IP	IP	kA
–	–	k?
Interkontinentální	interkontinentální	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
,	,	kIx,
MS	MS	kA
–	–	k?
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
klubů	klub	k1gInPc2
Pozn	pozn	kA
<g/>
.	.	kIx.
<g/>
:	:	kIx,
V	v	k7c6
závorce	závorka	k1gFnSc6
u	u	k7c2
daného	daný	k2eAgNnSc2d1
ročníkového	ročníkový	k2eAgNnSc2d1
maxima	maximum	k1gNnSc2
je	být	k5eAaImIp3nS
uveden	uveden	k2eAgMnSc1d1
přemožitel	přemožitel	k1gMnSc1
daného	daný	k2eAgInSc2d1
klubu	klub	k1gInSc2
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
<g/>
–	–	k?
<g/>
li	li	k8xS
v	v	k7c6
závorce	závorka	k1gFnSc6
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
PVP	PVP	kA
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
1971	#num#	k4
–	–	k?
Finále	finále	k1gNnSc1
(	(	kIx(
<g/>
Chelsea	Chelse	k2eAgFnSc1d1
FC	FC	kA
<g/>
)	)	kIx)
</s>
<s>
UEFA	UEFA	kA
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
1972	#num#	k4
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
(	(	kIx(
<g/>
PSV	PSV	kA
Eindhoven	Eindhoven	k2eAgInSc1d1
<g/>
)	)	kIx)
</s>
<s>
PMEZ	PMEZ	kA
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
1973	#num#	k4
–	–	k?
Semifinále	semifinále	k1gNnSc4
(	(	kIx(
<g/>
AFC	AFC	kA
Ajax	Ajax	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
UEFA	UEFA	kA
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
1974	#num#	k4
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
(	(	kIx(
<g/>
Ipswich	Ipswich	k1gInSc1
Town	Town	k1gInSc1
FC	FC	kA
<g/>
)	)	kIx)
</s>
<s>
PVP	PVP	kA
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
1975	#num#	k4
–	–	k?
Čtvrtfinále	čtvrtfinále	k1gNnSc1
(	(	kIx(
<g/>
FK	FK	kA
Crvena	Crvena	k1gFnSc1
zvezda	zvezda	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
PMEZ	PMEZ	kA
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
1976	#num#	k4
–	–	k?
Semifinále	semifinále	k1gNnSc4
(	(	kIx(
<g/>
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
PMEZ	PMEZ	kA
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
1977	#num#	k4
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
(	(	kIx(
<g/>
Club	club	k1gInSc1
Brugge	Brugg	k1gInSc2
KV	KV	kA
<g/>
)	)	kIx)
</s>
<s>
PMEZ	PMEZ	kA
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
1979	#num#	k4
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
(	(	kIx(
<g/>
Grasshopper	Grasshopper	k1gInSc1
Club	club	k1gInSc1
Zürich	Zürich	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
PMEZ	PMEZ	kA
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
1980	#num#	k4
–	–	k?
Semifinále	semifinále	k1gNnSc1
(	(	kIx(
<g/>
Hamburger	hamburger	k1gInSc1
SV	sv	kA
<g/>
)	)	kIx)
</s>
<s>
PMEZ	PMEZ	kA
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
1981	#num#	k4
–	–	k?
Finále	finále	k1gNnSc1
(	(	kIx(
<g/>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
<g/>
)	)	kIx)
</s>
<s>
UEFA	UEFA	kA
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
1982	#num#	k4
–	–	k?
Čtvrtfinále	čtvrtfinále	k1gNnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Kaiserslautern	Kaiserslauterna	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
PVP	PVP	kA
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
1983	#num#	k4
–	–	k?
Finále	finále	k1gNnSc1
(	(	kIx(
<g/>
Aberdeen	Aberdeen	k1gInSc1
FC	FC	kA
<g/>
)	)	kIx)
</s>
<s>
UEFA	UEFA	kA
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
1984	#num#	k4
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
(	(	kIx(
<g/>
Sparta	Sparta	k1gFnSc1
ČKD	ČKD	kA
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
UEFA	UEFA	kA
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
1985	#num#	k4
–	–	k?
Vítěz	vítěz	k1gMnSc1
(	(	kIx(
<g/>
poražený	poražený	k2eAgMnSc1d1
finalista	finalista	k1gMnSc1
Videoton	Videoton	k1gInSc4
FC	FC	kA
<g/>
)	)	kIx)
</s>
<s>
UEFA	UEFA	kA
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
1986	#num#	k4
–	–	k?
Vítěz	vítěz	k1gMnSc1
(	(	kIx(
<g/>
poražený	poražený	k2eAgMnSc1d1
finalista	finalista	k1gMnSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Köln	Kölna	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
PMEZ	PMEZ	kA
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
1987	#num#	k4
–	–	k?
Semifinále	semifinále	k1gNnSc4
(	(	kIx(
<g/>
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
PMEZ	PMEZ	kA
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
1988	#num#	k4
–	–	k?
Semifinále	semifinále	k1gNnSc4
(	(	kIx(
<g/>
PSV	PSV	kA
Eindhoven	Eindhoven	k2eAgInSc1d1
<g/>
)	)	kIx)
</s>
<s>
PMEZ	PMEZ	kA
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
1989	#num#	k4
–	–	k?
Semifinále	semifinále	k1gNnSc4
(	(	kIx(
<g/>
AC	AC	kA
Milán	Milán	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
PMEZ	PMEZ	kA
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
1990	#num#	k4
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
(	(	kIx(
<g/>
AC	AC	kA
Milán	Milán	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1990	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
VP	VP	kA
–	–	k?
Veletržní	veletržní	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
,	,	kIx,
PMEZ	PMEZ	kA
–	–	k?
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
PVP	PVP	kA
–	–	k?
Pohár	pohár	k1gInSc4
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
<g/>
,	,	kIx,
LM	LM	kA
–	–	k?
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
<g/>
,	,	kIx,
UEFA	UEFA	kA
–	–	k?
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
<g/>
,	,	kIx,
EL	Ela	k1gFnPc2
–	–	k?
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
<g/>
,	,	kIx,
SP	SP	kA
–	–	k?
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
<g/>
,	,	kIx,
PI	pi	k0
–	–	k?
Pohár	pohár	k1gInSc4
Intertoto	Intertota	k1gFnSc5
<g/>
,	,	kIx,
IP	IP	kA
–	–	k?
Interkontinentální	interkontinentální	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
,	,	kIx,
MS	MS	kA
–	–	k?
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
klubů	klub	k1gInPc2
Pozn	pozn	kA
<g/>
.	.	kIx.
<g/>
:	:	kIx,
V	v	k7c6
závorce	závorka	k1gFnSc6
u	u	k7c2
daného	daný	k2eAgNnSc2d1
ročníkového	ročníkový	k2eAgNnSc2d1
maxima	maximum	k1gNnSc2
je	být	k5eAaImIp3nS
uveden	uveden	k2eAgMnSc1d1
přemožitel	přemožitel	k1gMnSc1
daného	daný	k2eAgInSc2d1
klubu	klub	k1gInSc2
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
<g/>
–	–	k?
<g/>
li	li	k8xS
v	v	k7c6
závorce	závorka	k1gFnSc6
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
PMEZ	PMEZ	kA
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
1991	#num#	k4
–	–	k?
Čtvrtfinále	čtvrtfinále	k1gNnSc1
(	(	kIx(
<g/>
FK	FK	kA
Spartak	Spartak	k1gInSc1
Moskva	Moskva	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
UEFA	UEFA	kA
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
1992	#num#	k4
–	–	k?
Semifinále	semifinále	k1gNnSc1
(	(	kIx(
<g/>
Turín	Turín	k1gInSc1
FC	FC	kA
<g/>
)	)	kIx)
</s>
<s>
UEFA	UEFA	kA
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
1993	#num#	k4
–	–	k?
Čtvrtfinále	čtvrtfinále	k1gNnSc1
(	(	kIx(
<g/>
Paris	Paris	k1gMnSc1
Saint-Germain	Saint-Germain	k1gMnSc1
FC	FC	kA
<g/>
)	)	kIx)
</s>
<s>
PVP	PVP	kA
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
1994	#num#	k4
–	–	k?
Čtvrtfinále	čtvrtfinále	k1gNnSc1
(	(	kIx(
<g/>
Paris	Paris	k1gMnSc1
Saint-Germain	Saint-Germain	k1gMnSc1
FC	FC	kA
<g/>
)	)	kIx)
</s>
<s>
UEFA	UEFA	kA
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
1995	#num#	k4
–	–	k?
3	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
(	(	kIx(
<g/>
Odense	Odense	k1gFnSc1
BK	BK	kA
<g/>
)	)	kIx)
</s>
<s>
LM	LM	kA
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
1996	#num#	k4
–	–	k?
Čtvrtfinále	čtvrtfinále	k1gNnSc1
(	(	kIx(
<g/>
Juventus	Juventus	k1gInSc1
FC	FC	kA
<g/>
)	)	kIx)
</s>
<s>
LM	LM	kA
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
1998	#num#	k4
–	–	k?
Vítěz	vítěz	k1gMnSc1
(	(	kIx(
<g/>
poražený	poražený	k2eAgMnSc1d1
finalista	finalista	k1gMnSc1
Juventus	Juventus	k1gInSc4
FC	FC	kA
<g/>
)	)	kIx)
</s>
<s>
SP	SP	kA
1998	#num#	k4
–	–	k?
Finále	finále	k1gNnSc1
(	(	kIx(
<g/>
Chelsea	Chelse	k2eAgFnSc1d1
FC	FC	kA
<g/>
)	)	kIx)
</s>
<s>
IP	IP	kA
1998	#num#	k4
–	–	k?
Vítěz	vítěz	k1gMnSc1
(	(	kIx(
<g/>
poražený	poražený	k2eAgMnSc1d1
finalista	finalista	k1gMnSc1
CR	cr	k0
Vasco	Vasco	k1gNnSc4
da	da	k?
Gama	gama	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
LM	LM	kA
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
1999	#num#	k4
–	–	k?
Čtvrtfinále	čtvrtfinále	k1gNnSc1
(	(	kIx(
<g/>
FK	FK	kA
Dynamo	dynamo	k1gNnSc1
Kyjev	Kyjev	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
LM	LM	kA
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
–	–	k?
Vítěz	vítěz	k1gMnSc1
(	(	kIx(
<g/>
poražený	poražený	k2eAgMnSc1d1
finalista	finalista	k1gMnSc1
Valencia	Valencium	k1gNnSc2
CF	CF	kA
<g/>
)	)	kIx)
</s>
<s>
SP	SP	kA
2000	#num#	k4
–	–	k?
Finále	finále	k1gNnSc4
(	(	kIx(
<g/>
Galatasaray	Galatasaraa	k1gFnSc2
SK	Sk	kA
<g/>
)	)	kIx)
</s>
<s>
MS	MS	kA
2000	#num#	k4
–	–	k?
Zápas	zápas	k1gInSc1
o	o	k7c4
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc1
(	(	kIx(
<g/>
prohra	prohra	k1gFnSc1
s	s	k7c7
Club	club	k1gInSc4
Necaxa	Necaxus	k1gMnSc4
<g/>
)	)	kIx)
</s>
<s>
IP	IP	kA
2000	#num#	k4
–	–	k?
Finále	finále	k1gNnSc1
(	(	kIx(
<g/>
CA	ca	kA
Boca	Boca	k1gFnSc1
Juniors	Juniors	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
LM	LM	kA
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
2001	#num#	k4
–	–	k?
Semifinále	semifinále	k1gNnSc4
(	(	kIx(
<g/>
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
LM	LM	kA
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
2002	#num#	k4
–	–	k?
Vítěz	vítěz	k1gMnSc1
(	(	kIx(
<g/>
poražený	poražený	k2eAgMnSc1d1
finalista	finalista	k1gMnSc1
Bayer	Bayer	k1gMnSc1
04	#num#	k4
Leverkusen	Leverkusen	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
SP	SP	kA
2002	#num#	k4
–	–	k?
Vítěz	vítěz	k1gMnSc1
(	(	kIx(
<g/>
poražený	poražený	k2eAgMnSc1d1
finalista	finalista	k1gMnSc1
Feyenoord	Feyenoord	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
IP	IP	kA
2002	#num#	k4
–	–	k?
Vítěz	vítěz	k1gMnSc1
(	(	kIx(
<g/>
poražený	poražený	k2eAgMnSc1d1
finalista	finalista	k1gMnSc1
Club	club	k1gInSc4
Olimpia	Olimpium	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
LM	LM	kA
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
2003	#num#	k4
–	–	k?
Semifinále	semifinále	k1gNnSc1
(	(	kIx(
<g/>
Juventus	Juventus	k1gInSc1
FC	FC	kA
<g/>
)	)	kIx)
</s>
<s>
LM	LM	kA
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
2004	#num#	k4
–	–	k?
Čtvrtfinále	čtvrtfinále	k1gNnSc1
(	(	kIx(
<g/>
AS	as	k9
Monaco	Monaco	k1gMnSc1
FC	FC	kA
<g/>
)	)	kIx)
</s>
<s>
LM	LM	kA
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
2005	#num#	k4
–	–	k?
Osmifinále	osmifinále	k1gNnSc1
(	(	kIx(
<g/>
Juventus	Juventus	k1gInSc1
FC	FC	kA
<g/>
)	)	kIx)
</s>
<s>
LM	LM	kA
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
2006	#num#	k4
–	–	k?
Osmifinále	osmifinále	k1gNnSc4
(	(	kIx(
<g/>
Arsenal	Arsenal	k1gFnSc2
FC	FC	kA
<g/>
)	)	kIx)
</s>
<s>
LM	LM	kA
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
2007	#num#	k4
–	–	k?
Osmifinále	osmifinále	k1gNnSc4
(	(	kIx(
<g/>
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
LM	LM	kA
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
2008	#num#	k4
–	–	k?
Osmifinále	osmifinále	k1gNnSc4
(	(	kIx(
<g/>
AS	as	k9
Řím	Řím	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
LM	LM	kA
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
–	–	k?
Osmifinále	osmifinále	k1gNnSc1
(	(	kIx(
<g/>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
<g/>
)	)	kIx)
</s>
<s>
LM	LM	kA
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
2010	#num#	k4
–	–	k?
Osmifinále	osmifinále	k1gNnSc1
(	(	kIx(
<g/>
Olympique	Olympique	k1gInSc1
Lyon	Lyon	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2010	#num#	k4
<g/>
–	–	k?
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
VP	VP	kA
–	–	k?
Veletržní	veletržní	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
,	,	kIx,
PMEZ	PMEZ	kA
–	–	k?
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
PVP	PVP	kA
–	–	k?
Pohár	pohár	k1gInSc4
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
<g/>
,	,	kIx,
LM	LM	kA
–	–	k?
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
<g/>
,	,	kIx,
UEFA	UEFA	kA
–	–	k?
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
<g/>
,	,	kIx,
EL	Ela	k1gFnPc2
–	–	k?
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
<g/>
,	,	kIx,
SP	SP	kA
–	–	k?
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
<g/>
,	,	kIx,
PI	pi	k0
–	–	k?
Pohár	pohár	k1gInSc4
Intertoto	Intertota	k1gFnSc5
<g/>
,	,	kIx,
IP	IP	kA
–	–	k?
Interkontinentální	interkontinentální	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
,	,	kIx,
MS	MS	kA
–	–	k?
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
klubů	klub	k1gInPc2
Pozn	pozn	kA
<g/>
.	.	kIx.
<g/>
:	:	kIx,
V	v	k7c6
závorce	závorka	k1gFnSc6
u	u	k7c2
daného	daný	k2eAgNnSc2d1
ročníkového	ročníkový	k2eAgNnSc2d1
maxima	maximum	k1gNnSc2
je	být	k5eAaImIp3nS
uveden	uveden	k2eAgMnSc1d1
přemožitel	přemožitel	k1gMnSc1
daného	daný	k2eAgInSc2d1
klubu	klub	k1gInSc2
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
<g/>
–	–	k?
<g/>
li	li	k8xS
v	v	k7c6
závorce	závorka	k1gFnSc6
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
LM	LM	kA
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
2011	#num#	k4
–	–	k?
Semifinále	semifinále	k1gNnSc4
(	(	kIx(
<g/>
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
LM	LM	kA
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
2012	#num#	k4
–	–	k?
Semifinále	semifinále	k1gNnSc4
(	(	kIx(
<g/>
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
LM	LM	kA
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
2013	#num#	k4
–	–	k?
Semifinále	semifinále	k1gNnSc1
(	(	kIx(
<g/>
Borussia	Borussia	k1gFnSc1
Dortmund	Dortmund	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
LM	LM	kA
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
2014	#num#	k4
–	–	k?
Vítěz	vítěz	k1gMnSc1
(	(	kIx(
<g/>
poražený	poražený	k2eAgMnSc1d1
finalista	finalista	k1gMnSc1
Atlético	Atlético	k1gMnSc1
Madrid	Madrid	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
SP	SP	kA
2014	#num#	k4
–	–	k?
Vítěz	vítěz	k1gMnSc1
(	(	kIx(
<g/>
poražený	poražený	k2eAgMnSc1d1
finalista	finalista	k1gMnSc1
Sevilla	Sevilla	k1gFnSc1
FC	FC	kA
<g/>
)	)	kIx)
</s>
<s>
MS	MS	kA
2014	#num#	k4
–	–	k?
Vítěz	vítěz	k1gMnSc1
(	(	kIx(
<g/>
poražený	poražený	k2eAgMnSc1d1
finalista	finalista	k1gMnSc1
CA	ca	kA
San	San	k1gMnSc1
Lorenzo	Lorenza	k1gFnSc5
de	de	k?
Almagro	Almagro	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
LM	LM	kA
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
2015	#num#	k4
–	–	k?
Semifinále	semifinále	k1gNnSc1
(	(	kIx(
<g/>
Juventus	Juventus	k1gInSc1
FC	FC	kA
<g/>
)	)	kIx)
</s>
<s>
LM	LM	kA
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
2016	#num#	k4
–	–	k?
Vítěz	vítěz	k1gMnSc1
(	(	kIx(
<g/>
poražený	poražený	k2eAgMnSc1d1
finalista	finalista	k1gMnSc1
Atlético	Atlético	k1gMnSc1
Madrid	Madrid	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
SP	SP	kA
2016	#num#	k4
–	–	k?
Vítěz	vítěz	k1gMnSc1
(	(	kIx(
<g/>
poražený	poražený	k2eAgMnSc1d1
finalista	finalista	k1gMnSc1
Sevilla	Sevilla	k1gFnSc1
FC	FC	kA
<g/>
)	)	kIx)
</s>
<s>
MS	MS	kA
2016	#num#	k4
–	–	k?
Vítěz	vítěz	k1gMnSc1
(	(	kIx(
<g/>
poražený	poražený	k2eAgMnSc1d1
finalista	finalista	k1gMnSc1
Kašima	Kašimum	k1gNnSc2
Antlers	Antlers	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
LM	LM	kA
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
2017	#num#	k4
–	–	k?
Vítěz	vítěz	k1gMnSc1
(	(	kIx(
<g/>
poražený	poražený	k2eAgMnSc1d1
finalista	finalista	k1gMnSc1
Juventus	Juventus	k1gInSc4
FC	FC	kA
<g/>
)	)	kIx)
</s>
<s>
SP	SP	kA
2017	#num#	k4
–	–	k?
Vítěz	vítěz	k1gMnSc1
(	(	kIx(
<g/>
poražený	poražený	k2eAgMnSc1d1
finalista	finalista	k1gMnSc1
Manchester	Manchester	k1gInSc1
United	United	k1gInSc4
FC	FC	kA
<g/>
)	)	kIx)
</s>
<s>
MS	MS	kA
2017	#num#	k4
–	–	k?
Vítěz	vítěz	k1gMnSc1
(	(	kIx(
<g/>
poražený	poražený	k2eAgMnSc1d1
finalista	finalista	k1gMnSc1
Grê	Grê	k1gMnSc1
Foot-Ball	Foot-Ball	k1gMnSc1
Porto	porto	k1gNnSc4
Alegrense	Alegrense	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
LM	LM	kA
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
2018	#num#	k4
–	–	k?
Vítěz	vítěz	k1gMnSc1
(	(	kIx(
<g/>
poražený	poražený	k2eAgMnSc1d1
finalista	finalista	k1gMnSc1
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
<g/>
)	)	kIx)
</s>
<s>
SP	SP	kA
2018	#num#	k4
–	–	k?
Finále	finále	k1gNnSc1
(	(	kIx(
<g/>
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
MS	MS	kA
2018	#num#	k4
–	–	k?
Vítěz	vítěz	k1gMnSc1
(	(	kIx(
<g/>
poražený	poražený	k2eAgMnSc1d1
finalista	finalista	k1gMnSc1
Al	ala	k1gFnPc2
Ain	Ain	k1gFnPc2
FC	FC	kA
<g/>
)	)	kIx)
</s>
<s>
LM	LM	kA
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
2019	#num#	k4
–	–	k?
Osmifinále	osmifinále	k1gNnSc4
(	(	kIx(
<g/>
AFC	AFC	kA
Ajax	Ajax	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
LM	LM	kA
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
2020	#num#	k4
–	–	k?
Osmifinále	osmifinále	k1gNnSc1
(	(	kIx(
<g/>
Manchester	Manchester	k1gInSc1
City	city	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Slavní	slavný	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
</s>
<s>
Santiago	Santiago	k1gNnSc1
Bernabéu	Bernabéus	k1gInSc2
</s>
<s>
Alfredo	Alfredo	k1gNnSc1
Di	Di	k1gFnSc2
Stéfano	Stéfana	k1gFnSc5
</s>
<s>
Peter	Peter	k1gMnSc1
Dubovský	Dubovský	k1gMnSc1
</s>
<s>
Ferenc	Ferenc	k1gMnSc1
Puskás	Puskása	k1gFnPc2
</s>
<s>
Raymond	Raymond	k1gMnSc1
Kopa	kopa	k1gFnSc1
</s>
<s>
Francisco	Francisco	k6eAd1
Gento	Gent	k2eAgNnSc1d1
</s>
<s>
Camacho	Camacho	k6eAd1
</s>
<s>
Juanito	Juanit	k2eAgNnSc1d1
</s>
<s>
Santillana	Santillana	k1gFnSc1
</s>
<s>
Emilio	Emilio	k1gMnSc1
Butragueňo	Butragueňo	k1gMnSc1
</s>
<s>
Martin	Martin	k1gMnSc1
Vazquez	Vazquez	k1gMnSc1
</s>
<s>
Manolo	Manola	k1gFnSc5
Sanchis	Sanchis	k1gFnSc5
</s>
<s>
Bernd	Bernd	k1gMnSc1
Schuster	Schuster	k1gMnSc1
</s>
<s>
Paco	Paco	k1gMnSc1
Buyo	Buyo	k1gMnSc1
</s>
<s>
Hugo	Hugo	k1gMnSc1
Sanchez	Sanchez	k1gMnSc1
</s>
<s>
Fernando	Fernando	k6eAd1
Hierro	Hierro	k1gNnSc1
</s>
<s>
Gheorghe	Gheorghe	k1gFnSc1
Hagi	Hag	k1gFnSc2
</s>
<s>
Fernando	Fernando	k6eAd1
Redondo	Redondo	k6eAd1
</s>
<s>
Roberto	Roberta	k1gFnSc5
Carlos	Carlos	k1gMnSc1
</s>
<s>
Fernando	Fernando	k6eAd1
Morientes	Morientes	k1gInSc1
</s>
<s>
Steve	Steve	k1gMnSc1
McManaman	McManaman	k1gMnSc1
</s>
<s>
Iván	Iván	k1gInSc1
Helguera	Helguero	k1gNnSc2
</s>
<s>
Claude	Claude	k6eAd1
Makélélé	Makélélý	k2eAgNnSc1d1
</s>
<s>
Luís	Luís	k6eAd1
Figo	Figo	k6eAd1
</s>
<s>
Zinedine	Zinedin	k1gMnSc5
Zidane	Zidan	k1gMnSc5
</s>
<s>
Ronaldo	Ronaldo	k1gNnSc1
</s>
<s>
David	David	k1gMnSc1
Beckham	Beckham	k1gInSc4
</s>
<s>
Michael	Michael	k1gMnSc1
Owen	Owen	k1gMnSc1
</s>
<s>
Esteban	Esteban	k1gMnSc1
Cambiasso	Cambiassa	k1gFnSc5
</s>
<s>
Robinho	Robinze	k6eAd1
</s>
<s>
Fabio	Fabia	k1gFnSc5
Cannavaro	Cannavara	k1gFnSc5
</s>
<s>
Míchel	Míchel	k1gMnSc1
Salgado	Salgada	k1gFnSc5
</s>
<s>
Ruud	Ruud	k6eAd1
van	van	k1gInSc1
Nistelrooy	Nistelrooa	k1gFnSc2
</s>
<s>
Guti	Guti	k6eAd1
</s>
<s>
Raúl	Raúl	k1gMnSc1
</s>
<s>
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronalda	k1gFnSc5
</s>
<s>
Iker	Iker	k1gMnSc1
Casillas	Casillas	k1gMnSc1
</s>
<s>
Kaká	kakat	k5eAaImIp3nS
</s>
<s>
Mesut	Mesut	k2eAgInSc1d1
Özil	Özil	k1gInSc1
</s>
<s>
Sergio	Sergio	k1gMnSc1
Ramos	Ramos	k1gMnSc1
</s>
<s>
Karim	Karim	k6eAd1
Benzema	Benzema	k1gFnSc1
</s>
<s>
Marcelo	Marcela	k1gFnSc5
</s>
<s>
James	James	k1gMnSc1
Rodriguez	Rodriguez	k1gMnSc1
</s>
<s>
Gareth	Gareth	k1gMnSc1
Bale	bal	k1gInSc5
</s>
<s>
Luka	luka	k1gNnPc1
Modrić	Modrić	k1gFnSc2
</s>
<s>
Davor	Davor	k1gMnSc1
Šuker	Šuker	k1gMnSc1
</s>
<s>
Historie	historie	k1gFnSc1
trenérů	trenér	k1gMnPc2
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1910	#num#	k4
<g/>
–	–	k?
<g/>
1920	#num#	k4
<g/>
:	:	kIx,
Arthur	Arthur	k1gMnSc1
Johnson	Johnson	k1gMnSc1
</s>
<s>
1920	#num#	k4
<g/>
–	–	k?
<g/>
1926	#num#	k4
<g/>
:	:	kIx,
Juan	Juan	k1gMnSc1
de	de	k?
Cácer	Cácer	k1gMnSc1
</s>
<s>
1926	#num#	k4
<g/>
:	:	kIx,
Pedro	Pedra	k1gMnSc5
Llorente	Llorent	k1gMnSc5
</s>
<s>
1926	#num#	k4
<g/>
–	–	k?
<g/>
1927	#num#	k4
<g/>
:	:	kIx,
Santiago	Santiago	k1gNnSc1
Bernabéu	Bernabéus	k1gInSc2
</s>
<s>
1927	#num#	k4
<g/>
–	–	k?
<g/>
1929	#num#	k4
<g/>
:	:	kIx,
José	Josý	k2eAgNnSc1d1
Berraondo	Berraondo	k1gNnSc1
</s>
<s>
1929	#num#	k4
<g/>
–	–	k?
<g/>
1930	#num#	k4
<g/>
:	:	kIx,
José	Josý	k2eAgFnSc2d1
Quirante	Quirant	k1gMnSc5
</s>
<s>
1930	#num#	k4
<g/>
–	–	k?
<g/>
1932	#num#	k4
<g/>
:	:	kIx,
Lippo	Lippa	k1gFnSc5
Hertza	Hertza	k1gFnSc1
</s>
<s>
1932	#num#	k4
<g/>
–	–	k?
<g/>
1934	#num#	k4
<g/>
:	:	kIx,
Robert	Robert	k1gMnSc1
Firth	Firth	k1gMnSc1
</s>
<s>
1934	#num#	k4
<g/>
–	–	k?
<g/>
1941	#num#	k4
<g/>
:	:	kIx,
Francisco	Francisco	k1gMnSc1
Bru	Bru	k1gMnSc1
</s>
<s>
1941	#num#	k4
<g/>
–	–	k?
<g/>
1943	#num#	k4
<g/>
:	:	kIx,
Juan	Juan	k1gMnSc1
Armet	Armet	k1gMnSc1
</s>
<s>
1943	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
<g/>
:	:	kIx,
Ramón	Ramón	k1gMnSc1
Encinas	Encinas	k1gMnSc1
</s>
<s>
1945	#num#	k4
<g/>
–	–	k?
<g/>
1946	#num#	k4
<g/>
:	:	kIx,
Jacinto	Jacinta	k1gFnSc5
Quincóces	Quincóces	k1gInSc1
</s>
<s>
1946	#num#	k4
<g/>
–	–	k?
<g/>
1947	#num#	k4
<g/>
:	:	kIx,
Baltasar	Baltasar	k1gMnSc1
Albéniz	Albéniz	k1gMnSc1
</s>
<s>
1947	#num#	k4
<g/>
–	–	k?
<g/>
1948	#num#	k4
<g/>
:	:	kIx,
Jacinto	Jacinta	k1gFnSc5
Quincóces	Quincóces	k1gInSc1
</s>
<s>
1948	#num#	k4
<g/>
–	–	k?
<g/>
1950	#num#	k4
<g/>
:	:	kIx,
Michael	Michael	k1gMnSc1
Keeping	Keeping	k1gInSc4
</s>
<s>
1950	#num#	k4
<g/>
–	–	k?
<g/>
1951	#num#	k4
<g/>
:	:	kIx,
Baltasar	Baltasar	k1gMnSc1
Albéniz	Albéniz	k1gMnSc1
</s>
<s>
1951	#num#	k4
<g/>
–	–	k?
<g/>
1952	#num#	k4
<g/>
:	:	kIx,
Hector	Hector	k1gMnSc1
Scarone	Scaron	k1gMnSc5
</s>
<s>
1952	#num#	k4
<g/>
–	–	k?
<g/>
1953	#num#	k4
<g/>
:	:	kIx,
Juan	Juan	k1gMnSc1
Antonio	Antonio	k1gMnSc1
Ipiñ	Ipiñ	k1gMnSc1
</s>
<s>
1953	#num#	k4
<g/>
–	–	k?
<g/>
1954	#num#	k4
<g/>
:	:	kIx,
Enrique	Enriqu	k1gInSc2
Fernández	Fernández	k1gInSc1
Viola	Viola	k1gFnSc1
</s>
<s>
1954	#num#	k4
<g/>
–	–	k?
<g/>
1957	#num#	k4
<g/>
:	:	kIx,
José	José	k1gNnSc4
Villalinga	Villaling	k1gMnSc2
</s>
<s>
1957	#num#	k4
<g/>
–	–	k?
<g/>
1959	#num#	k4
<g/>
:	:	kIx,
Luis	Luisa	k1gFnPc2
Carringlia	Carringlium	k1gNnSc2
</s>
<s>
1959	#num#	k4
<g/>
:	:	kIx,
Miguel	Miguel	k1gMnSc1
Muñ	Muñ	k1gMnSc1
</s>
<s>
1959	#num#	k4
<g/>
:	:	kIx,
Luis	Luisa	k1gFnPc2
Carringlia	Carringlium	k1gNnSc2
</s>
<s>
1959	#num#	k4
<g/>
–	–	k?
<g/>
1960	#num#	k4
<g/>
:	:	kIx,
Miguel	Miguel	k1gMnSc1
Fleitas	Fleitas	k1gMnSc1
</s>
<s>
1960	#num#	k4
<g/>
–	–	k?
<g/>
1974	#num#	k4
<g/>
:	:	kIx,
Miguel	Miguel	k1gMnSc1
Muñ	Muñ	k1gMnSc1
</s>
<s>
1974	#num#	k4
<g/>
:	:	kIx,
Luis	Luisa	k1gFnPc2
Molowny	Molowna	k1gFnSc2
</s>
<s>
1974	#num#	k4
<g/>
–	–	k?
<g/>
1977	#num#	k4
<g/>
:	:	kIx,
Milan	Milan	k1gMnSc1
Miljanič	Miljanič	k1gMnSc1
</s>
<s>
1977	#num#	k4
<g/>
–	–	k?
<g/>
1979	#num#	k4
<g/>
:	:	kIx,
Luis	Luisa	k1gFnPc2
Molowny	Molowna	k1gFnSc2
</s>
<s>
1979	#num#	k4
<g/>
–	–	k?
<g/>
1982	#num#	k4
<g/>
:	:	kIx,
Vujadin	Vujadin	k2eAgInSc1d1
Boškov	Boškov	k1gInSc1
</s>
<s>
1982	#num#	k4
<g/>
:	:	kIx,
Luis	Luisa	k1gFnPc2
Molowny	Molowna	k1gFnSc2
</s>
<s>
1982	#num#	k4
<g/>
–	–	k?
<g/>
1984	#num#	k4
<g/>
:	:	kIx,
Alfredo	Alfredo	k1gNnSc4
Di	Di	k1gFnSc2
Stéfano	Stéfana	k1gFnSc5
</s>
<s>
1984	#num#	k4
<g/>
–	–	k?
<g/>
1985	#num#	k4
<g/>
:	:	kIx,
Amancio	Amancio	k6eAd1
Amaro	Amaro	k1gNnSc1
</s>
<s>
1985	#num#	k4
<g/>
–	–	k?
<g/>
1986	#num#	k4
<g/>
:	:	kIx,
Luis	Luisa	k1gFnPc2
Molowny	Molowna	k1gFnSc2
</s>
<s>
1986	#num#	k4
<g/>
–	–	k?
<g/>
1989	#num#	k4
<g/>
:	:	kIx,
Leo	Leo	k1gMnSc1
Beenhakker	Beenhakker	k1gMnSc1
</s>
<s>
1989	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
:	:	kIx,
John	John	k1gMnSc1
Toshack	Toshack	k1gMnSc1
</s>
<s>
1990	#num#	k4
<g/>
–	–	k?
<g/>
1991	#num#	k4
<g/>
:	:	kIx,
Alfredo	Alfredo	k1gNnSc4
Di	Di	k1gFnSc2
Stéfano	Stéfana	k1gFnSc5
</s>
<s>
1991	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
<g/>
:	:	kIx,
Radomir	Radomir	k1gMnSc1
Antić	Antić	k1gMnSc1
</s>
<s>
1992	#num#	k4
<g/>
:	:	kIx,
Leo	Leo	k1gMnSc1
Beenhakker	Beenhakker	k1gMnSc1
</s>
<s>
1992	#num#	k4
<g/>
–	–	k?
<g/>
1994	#num#	k4
<g/>
:	:	kIx,
Benito	Benit	k2eAgNnSc1d1
Floro	Floro	k1gNnSc1
</s>
<s>
1994	#num#	k4
<g/>
:	:	kIx,
Vicente	Vicent	k1gInSc5
del	del	k?
Bosque	Bosque	k1gFnPc7
</s>
<s>
1994	#num#	k4
<g/>
–	–	k?
<g/>
1996	#num#	k4
<g/>
:	:	kIx,
Jorge	Jorg	k1gInSc2
Valdano	Valdana	k1gFnSc5
</s>
<s>
1996	#num#	k4
<g/>
:	:	kIx,
Vicente	Vicent	k1gInSc5
del	del	k?
Bosque	Bosqu	k1gInPc1
</s>
<s>
1996	#num#	k4
<g/>
:	:	kIx,
Arsenio	Arsenio	k1gMnSc1
Iglesias	Iglesias	k1gMnSc1
</s>
<s>
1996	#num#	k4
<g/>
–	–	k?
<g/>
1997	#num#	k4
<g/>
:	:	kIx,
Fabio	Fabio	k6eAd1
Capello	Capello	k1gNnSc1
</s>
<s>
1997	#num#	k4
<g/>
–	–	k?
<g/>
1998	#num#	k4
<g/>
:	:	kIx,
Jupp	Jupp	k1gMnSc1
Heynckes	Heynckes	k1gMnSc1
</s>
<s>
1998	#num#	k4
<g/>
:	:	kIx,
José	Josá	k1gFnSc2
Antonio	Antonio	k1gMnSc1
Camacho	Camacha	k1gMnSc5
</s>
<s>
1998	#num#	k4
<g/>
–	–	k?
<g/>
1999	#num#	k4
<g/>
:	:	kIx,
Guus	Guus	k1gInSc1
Hiddink	Hiddink	k1gInSc1
</s>
<s>
1999	#num#	k4
<g/>
:	:	kIx,
John	John	k1gMnSc1
Toshack	Toshack	k1gMnSc1
</s>
<s>
1999	#num#	k4
<g/>
–	–	k?
<g/>
2003	#num#	k4
<g/>
:	:	kIx,
Vicente	Vicent	k1gInSc5
del	del	k?
Bosque	Bosque	k1gNnSc7
</s>
<s>
2003	#num#	k4
<g/>
–	–	k?
<g/>
2004	#num#	k4
<g/>
:	:	kIx,
Carlos	Carlos	k1gMnSc1
Queiroz	Queiroz	k1gMnSc1
</s>
<s>
2004	#num#	k4
<g/>
:	:	kIx,
José	Josá	k1gFnSc2
Antonio	Antonio	k1gMnSc1
Camacho	Camacha	k1gMnSc5
</s>
<s>
2004	#num#	k4
<g/>
:	:	kIx,
Mariano	Mariana	k1gFnSc5
García	Garcí	k2eAgNnPc1d1
Remón	Remón	k1gInSc4
</s>
<s>
2004	#num#	k4
<g/>
–	–	k?
<g/>
2005	#num#	k4
<g/>
:	:	kIx,
Vanderlei	Vanderlei	k1gNnSc6
Luxemburgo	Luxemburgo	k6eAd1
</s>
<s>
2005	#num#	k4
<g/>
–	–	k?
<g/>
2006	#num#	k4
<g/>
:	:	kIx,
Juan	Juan	k1gMnSc1
Ramón	Ramón	k1gMnSc1
Lopez	Lopez	k1gMnSc1
Caro	Caro	k1gMnSc1
</s>
<s>
2006	#num#	k4
<g/>
–	–	k?
<g/>
2007	#num#	k4
<g/>
:	:	kIx,
Fabio	Fabio	k6eAd1
Capello	Capello	k1gNnSc1
</s>
<s>
2007	#num#	k4
<g/>
–	–	k?
<g/>
2008	#num#	k4
<g/>
:	:	kIx,
Bernd	Bernd	k1gMnSc1
Schuster	Schuster	k1gMnSc1
</s>
<s>
2008	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
<g/>
:	:	kIx,
Juande	Juand	k1gMnSc5
Ramos	Ramos	k1gMnSc1
</s>
<s>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
<g/>
:	:	kIx,
Manuel	Manuel	k1gMnSc1
Pellegrini	Pellegrin	k2eAgMnPc5d1
</s>
<s>
2010	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
:	:	kIx,
José	José	k1gNnSc4
Mourinho	Mourin	k1gMnSc2
</s>
<s>
2013	#num#	k4
<g/>
–	–	k?
<g/>
2015	#num#	k4
<g/>
:	:	kIx,
Carlo	Carlo	k1gNnSc4
Ancelotti	Ancelotť	k1gFnSc2
</s>
<s>
2015	#num#	k4
<g/>
–	–	k?
<g/>
2016	#num#	k4
<g/>
:	:	kIx,
Rafael	Rafael	k1gMnSc1
Benítez	Benítez	k1gMnSc1
</s>
<s>
2016	#num#	k4
<g/>
–	–	k?
<g/>
2018	#num#	k4
<g/>
:	:	kIx,
Zinedine	Zinedin	k1gMnSc5
Zidane	Zidan	k1gMnSc5
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2018	#num#	k4
<g/>
:	:	kIx,
Julen	Julen	k1gInSc1
Lopetegui	Lopetegu	k1gFnSc2
</s>
<s>
2018	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
<g/>
:	:	kIx,
Santiago	Santiago	k1gNnSc1
Solari	Solar	k1gFnSc2
</s>
<s>
2019	#num#	k4
<g/>
–	–	k?
:	:	kIx,
Zinedine	Zinedin	k1gMnSc5
Zidane	Zidan	k1gMnSc5
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
"	"	kIx"
<g/>
Dresy	dres	k1gInPc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
merengues	merengues	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
http://www.realmadrid.com/en/history/santiago-bernabeu-stadium1	http://www.realmadrid.com/en/history/santiago-bernabeu-stadium1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
"	"	kIx"
<g/>
Historia	Historium	k1gNnSc2
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
realmadrid	realmadrid	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Un	Un	k1gMnSc1
decreto	decreto	k1gNnSc4
españ	españ	k1gMnSc2
los	los	k1gMnSc1
nombres	nombres	k1gMnSc1
(	(	kIx(
<g/>
1940	#num#	k4
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
as	as	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
20.12	20.12	k4
<g/>
.2016	.2016	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
TEJWANI	TEJWANI	kA
<g/>
,	,	kIx,
Karan	Karan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
REAL	Real	k1gInSc1
MADRID	Madrid	k1gInSc4
IN	IN	kA
THE	THE	kA
1980	#num#	k4
<g/>
S	s	k7c7
<g/>
:	:	kIx,
FROM	FROM	kA
DESPAIR	DESPAIR	kA
TO	ten	k3xDgNnSc4
LA	la	k1gNnSc7
QUINTA	QUINTA	kA
AND	Anda	k1gFnPc2
LALIGA	LALIGA	kA
DOMINANCE	dominance	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
These	these	k1gFnSc1
Football	Footballa	k1gFnPc2
Times	Timesa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-01-07	2020-01-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Real	Real	k1gInSc1
slaví	slavit	k5eAaImIp3nS
desátý	desátý	k4xOgInSc1
triumf	triumf	k1gInSc1
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
<g/>
,	,	kIx,
Atlético	Atlético	k6eAd1
porazil	porazit	k5eAaPmAgMnS
v	v	k7c6
prodloužení	prodloužení	k1gNnSc6
<g/>
,	,	kIx,
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
citováno	citován	k2eAgNnSc4d1
25	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
↑	↑	k?
Real	Real	k1gInSc1
-	-	kIx~
Atlético	Atlético	k6eAd1
4	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
a.e.t.	a.e.t.	k?
<g/>
,	,	kIx,
UEFA	UEFA	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
"	"	kIx"
<g/>
European	European	k1gInSc1
Cups	Cupsa	k1gFnPc2
Archive	archiv	k1gInSc5
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
"	"	kIx"
<g/>
Latin	latina	k1gFnPc2
Cup	cup	k1gInSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Intercontinental	Intercontinental	k1gMnSc1
Club	club	k1gInSc4
Cup	cup	k1gInSc4
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
FIFA	FIFA	kA
Club	club	k1gInSc1
World	World	k1gInSc1
Cup	cup	k1gInSc4
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Real	Real	k1gInSc1
vyhral	vyhrat	k5eAaImAgMnS,k5eAaPmAgMnS
aj	aj	kA
MS	MS	kA
klubov	klubov	k1gInSc4
<g/>
,	,	kIx,
za	za	k7c4
rok	rok	k1gInSc4
získal	získat	k5eAaPmAgMnS
štyri	štyri	k6eAd1
trofeje	trofej	k1gFnPc4
<g/>
,	,	kIx,
SME	SME	k?
<g/>
.	.	kIx.
<g/>
sk	sk	k?
<g/>
,	,	kIx,
cit	cit	k1gInSc1
<g/>
.	.	kIx.
20	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
"	"	kIx"
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
Klubové	klubový	k2eAgFnPc4d1
MS	MS	kA
ovládl	ovládnout	k5eAaPmAgInS
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
před	před	k7c7
Al	ala	k1gFnPc2
Ajnem	Ajnem	k1gInSc1
<g/>
,	,	kIx,
třetí	třetí	k4xOgFnSc1
končí	končit	k5eAaImIp3nS
River	River	k1gInSc1
Plate	plat	k1gInSc5
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
idnes	idnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
22.12	22.12	k4
<g/>
.2018	.2018	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Spain	Spain	k1gMnSc1
-	-	kIx~
List	list	k1gInSc1
of	of	k?
Champions	Champions	k1gInSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Spain	Spain	k1gMnSc1
-	-	kIx~
List	list	k1gInSc1
of	of	k?
Cup	cup	k1gInSc1
Finals	Finals	k1gInSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Spain	Spain	k1gMnSc1
-	-	kIx~
List	list	k1gInSc1
of	of	k?
League	League	k1gInSc1
Cup	cup	k1gInSc1
Finals	Finals	k1gInSc4
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
"	"	kIx"
<g/>
Spain	Spain	k1gInSc1
-	-	kIx~
List	list	k1gInSc1
of	of	k?
Super	super	k2eAgInSc1d1
Cup	cup	k1gInSc1
Finals	Finalsa	k1gFnPc2
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Španělský	španělský	k2eAgInSc4d1
Superpohár	superpohár	k1gInSc4
pro	pro	k7c4
Real	Real	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Arábii	Arábie	k1gFnSc6
rozhodovaly	rozhodovat	k5eAaImAgFnP
až	až	k6eAd1
penalty	penalta	k1gFnPc4
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
idnes	idnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
12.01	12.01	k4
<g/>
.2020	.2020	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Pequeñ	Pequeñ	k1gMnSc1
Copa	Copa	k1gMnSc1
del	del	k?
Mundo	Mundo	k1gNnSc4
and	and	k?
Other	Othra	k1gFnPc2
International	International	k1gFnSc1
Club	club	k1gInSc1
Tournaments	Tournaments	k1gInSc1
in	in	k?
Caracas	Caracas	k1gInSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Copa	Cop	k2eAgFnSc1d1
Iberoamericana	Iberoamericana	k1gFnSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Spain	Spain	k1gMnSc1
-	-	kIx~
List	list	k1gInSc1
of	of	k?
Champions	Champions	k1gInSc1
of	of	k?
Centro	Centro	k1gNnSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
ABC	ABC	kA
Madrid	Madrid	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
29	#num#	k4
(	(	kIx(
<g/>
19	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1923	#num#	k4
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
hemeroteca	hemeroteca	k1gFnSc1
<g/>
.	.	kIx.
<g/>
abc	abc	k?
<g/>
.	.	kIx.
<g/>
es	es	k1gNnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
ABC	ABC	kA
Madrid	Madrid	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
36	#num#	k4
(	(	kIx(
<g/>
8	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1928	#num#	k4
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
hemeroteca	hemeroteca	k1gFnSc1
<g/>
.	.	kIx.
<g/>
abc	abc	k?
<g/>
.	.	kIx.
<g/>
es	es	k1gNnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
ABC	ABC	kA
Madrid	Madrid	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
17	#num#	k4
(	(	kIx(
<g/>
9	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1944	#num#	k4
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
hemeroteca	hemeroteca	k1gFnSc1
<g/>
.	.	kIx.
<g/>
abc	abc	k?
<g/>
.	.	kIx.
<g/>
es	es	k1gNnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Trofeo	Trofeo	k1gMnSc1
Teresa	Teresa	k1gFnSc1
Herrera	Herrera	k1gFnSc1
(	(	kIx(
<g/>
La	la	k1gNnSc1
Coruñ	Coruñ	k1gFnPc2
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Trofeo	Trofeo	k1gMnSc1
Ramón	Ramón	k1gMnSc1
de	de	k?
Carranza	Carranza	k1gFnSc1
(	(	kIx(
<g/>
Cádiz-Spain	Cádiz-Spain	k1gInSc1
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Coupe	coup	k1gInSc5
Mohamed	Mohamed	k1gMnSc1
V	V	kA
(	(	kIx(
<g/>
Casablanca	Casablanca	k1gFnSc1
-	-	kIx~
Morocco	Morocco	k1gMnSc1
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Trofeo	Trofeo	k6eAd1
Colombino	Colombin	k2eAgNnSc1d1
(	(	kIx(
<g/>
Huelva-Spain	Huelva-Spain	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Trofeo	Trofeo	k1gMnSc1
Alicante-Costa	Alicante-Costa	k1gMnSc1
Blanca-Ciudad	Blanca-Ciudad	k1gInSc1
de	de	k?
Alicante-Ayuntamiento	Alicante-Ayuntamiento	k1gNnSc1
(	(	kIx(
<g/>
Alicante-Spain	Alicante-Spain	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Trofeo	Trofeo	k6eAd1
Santiago	Santiago	k1gNnSc1
Bernabéu	Bernabéus	k1gInSc2
(	(	kIx(
<g/>
Madrid-Spain	Madrid-Spain	k1gInSc1
<g/>
)	)	kIx)
1979	#num#	k4
<g/>
-	-	kIx~
<g/>
2018	#num#	k4
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Trofeo	Trofeo	k6eAd1
Ciudad	Ciudad	k1gInSc1
de	de	k?
Vigo	Vigo	k1gNnSc1
(	(	kIx(
<g/>
Vigo-Pontevedra	Vigo-Pontevedra	k1gFnSc1
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Festa	Festa	k?
d	d	k?
<g/>
'	'	kIx"
<g/>
Elx	Elx	k1gMnSc1
(	(	kIx(
<g/>
Elche	Elche	k1gInSc1
<g/>
,	,	kIx,
Alicante-Spain	Alicante-Spain	k1gInSc1
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Trofeo	Trofeo	k1gMnSc1
Naranja	Naranja	k1gMnSc1
(	(	kIx(
<g/>
Valencia-Spain	Valencia-Spain	k1gMnSc1
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Trofeo	Trofeo	k1gMnSc1
Bahía	Bahía	k1gMnSc1
de	de	k?
Cartagena	Cartagena	k1gFnSc1
(	(	kIx(
<g/>
Cartagena-Spain	Cartagena-Spain	k1gInSc1
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Coppa	Copp	k1gMnSc2
Pirelli	Pirell	k1gMnSc3
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Trofeo	Trofeo	k1gMnSc1
Memorial	Memorial	k1gMnSc1
Jesús	Jesúsa	k1gFnPc2
Gil	Gil	k1gMnSc1
y	y	k?
Gil	Gil	k1gMnSc1
2005	#num#	k4
<g/>
-	-	kIx~
<g/>
2019	#num#	k4
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Real	Real	k1gInSc4
siegt	siegt	k2eAgInSc4d1
bei	bei	k?
Beckenbauer-Abschied	Beckenbauer-Abschied	k1gInSc4
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
fcbayern	fcbayern	k1gInSc1
<g/>
.	.	kIx.
<g/>
t-com	t-com	k1gInSc1
<g/>
.	.	kIx.
<g/>
de	de	k?
<g/>
,	,	kIx,
13.08	13.08	k4
<g/>
.2010	.2010	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
El	Ela	k1gFnPc2
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
se	se	k3xPyFc4
adjudica	adjudica	k1gMnSc1
el	ela	k1gFnPc2
World	World	k1gMnSc1
Football	Football	k1gMnSc1
Challenge	Challenge	k1gFnSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
as	as	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
31.07	31.07	k4
<g/>
.2011	.2011	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
WFC	WFC	kA
<g/>
:	:	kIx,
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
top	topit	k5eAaImRp2nS
Celtic	Celtice	k1gFnPc2
to	ten	k3xDgNnSc4
finish	finish	k1gMnSc1
US	US	kA
tour	tour	k1gMnSc1
perfect	perfect	k1gMnSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
mlssoccer	mlssoccer	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
11.08	11.08	k4
<g/>
.2012	.2012	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Chelsea	Chelsea	k1gMnSc1
1	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
3	#num#	k4
<g/>
:	:	kIx,
Lampard	Lamparda	k1gFnPc2
returns	returns	k6eAd1
to	ten	k3xDgNnSc4
Jose	Jose	k1gNnSc4
<g/>
'	'	kIx"
<g/>
s	s	k7c7
fold	fold	k6eAd1
<g/>
...	...	k?
but	but	k?
Ronaldo	Ronaldo	k1gNnSc4
steals	steals	k6eAd1
the	the	k?
show	show	k1gFnSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
dailymail	dailymail	k1gInSc1
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
<g/>
,	,	kIx,
0	#num#	k4
<g/>
8.08	8.08	k4
<g/>
.2013	.2013	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
"	"	kIx"
<g/>
2015	#num#	k4
INTERNATIONAL	INTERNATIONAL	kA
CHAMPIONS	CHAMPIONS	kA
CUP	cup	k1gInSc4
CHAMPIONS	CHAMPIONS	kA
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
internationalchampionscup	internationalchampionscup	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
2017	#num#	k4
MLS	mls	k1gInSc1
All-Star	All-Star	k1gInSc4
Game	game	k1gInSc1
Recap	Recap	k1gInSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
mlssoccer	mlssoccer	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
0	#num#	k4
<g/>
3.08	3.08	k4
<g/>
.2017	.2017	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Spain	Spain	k1gMnSc1
-	-	kIx~
U-19	U-19	k1gMnSc1
League	Leagu	k1gFnSc2
Champions	Champions	k1gInSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Spain	Spain	k1gMnSc1
-	-	kIx~
U-19	U-19	k1gFnSc1
Cup	cup	k1gInSc1
History	Histor	k1gInPc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Plantilla	Plantilla	k1gMnSc1
de	de	k?
Jugadores	Jugadores	k1gMnSc1
del	del	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
|	|	kIx~
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Realmadrid	Realmadrid	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Spanish	Spanish	k1gInSc1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Entrenadores	Entrenadores	k1gMnSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
realmadrid	realmadrid	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Sport	sport	k1gInSc1
magazín	magazín	k1gInSc1
č.	č.	k?
3	#num#	k4
<g/>
/	/	kIx~
<g/>
2016	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
10	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
↑	↑	k?
Sport	sport	k1gInSc1
magazín	magazín	k1gInSc1
č.	č.	k?
3	#num#	k4
<g/>
/	/	kIx~
<g/>
2016	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
10-15	10-15	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
Castilla	Castill	k1gMnSc2
</s>
<s>
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
„	„	k?
<g/>
C	C	kA
<g/>
“	“	k?
</s>
<s>
El	Ela	k1gFnPc2
Clásico	Clásico	k6eAd1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
České	český	k2eAgFnPc1d1
a	a	k8xC
slovenské	slovenský	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
o	o	k7c6
Realu	Real	k1gInSc6
Madrid	Madrid	k1gInSc1
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Informace	informace	k1gFnPc1
o	o	k7c6
stadionu	stadion	k1gInSc6
-	-	kIx~
FotbaloveStadiony	FotbaloveStadion	k1gInPc7
<g/>
.	.	kIx.
<g/>
cz	cz	k?
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Český	český	k2eAgInSc1d1
a	a	k8xC
slovenský	slovenský	k2eAgInSc1d1
web	web	k1gInSc1
o	o	k7c6
Realu	Real	k1gInSc6
Madrid	Madrid	k1gInSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Primera	primera	k1gFnSc1
División	División	k1gInSc1
–	–	k?
španělská	španělský	k2eAgFnSc1d1
nejvyšší	vysoký	k2eAgFnSc1d3
soutěž	soutěž	k1gFnSc1
ve	v	k7c6
fotbale	fotbal	k1gInSc6
(	(	kIx(
<g/>
ročníky	ročník	k1gInPc1
<g/>
,	,	kIx,
týmy	tým	k1gInPc1
<g/>
,	,	kIx,
ocenění	ocenění	k1gNnSc1
<g/>
,	,	kIx,
atd.	atd.	kA
<g/>
)	)	kIx)
Kluby	klub	k1gInPc7
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
2021	#num#	k4
(	(	kIx(
<g/>
20	#num#	k4
mužstev	mužstvo	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Deportivo	Deportiva	k1gFnSc5
Alavés	Alavésa	k1gFnPc2
•	•	k?
Athletic	Athletice	k1gFnPc2
Bilbao	Bilbao	k1gMnSc1
•	•	k?
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
•	•	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
Cádiz	Cádiz	k1gInSc1
CF	CF	kA
•	•	k?
Celta	celta	k1gFnSc1
de	de	k?
Vigo	Vigo	k1gMnSc1
•	•	k?
CA	ca	kA
Osasuna	Osasuna	k1gFnSc1
•	•	k?
SD	SD	kA
Eibar	Eibar	k1gMnSc1
•	•	k?
Elche	Elche	k1gNnPc2
CF	CF	kA
•	•	k?
Getafe	Getaf	k1gInSc5
CF	CF	kA
•	•	k?
Granada	Granada	k1gFnSc1
CF	CF	kA
•	•	k?
SD	SD	kA
Huesca	Huesc	k2eAgMnSc4d1
•	•	k?
Levante	Levant	k1gMnSc5
UD	UD	kA
•	•	k?
Real	Real	k1gInSc1
Valladolid	Valladolid	k1gInSc1
•	•	k?
Betis	Betis	k1gInSc1
Sevilla	Sevilla	k1gFnSc1
•	•	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
•	•	k?
Real	Real	k1gInSc1
Sociedad	Sociedad	k1gInSc1
•	•	k?
Sevilla	Sevilla	k1gFnSc1
FC	FC	kA
•	•	k?
Valencia	Valencia	k1gFnSc1
CF	CF	kA
•	•	k?
Villarreal	Villarreal	k1gInSc1
CF	CF	kA
Sezóny	sezóna	k1gFnSc2
</s>
<s>
1929	#num#	k4
•	•	k?
1929	#num#	k4
<g/>
/	/	kIx~
<g/>
30	#num#	k4
•	•	k?
1930	#num#	k4
<g/>
/	/	kIx~
<g/>
31	#num#	k4
•	•	k?
1931	#num#	k4
<g/>
/	/	kIx~
<g/>
32	#num#	k4
•	•	k?
1932	#num#	k4
<g/>
/	/	kIx~
<g/>
33	#num#	k4
•	•	k?
1933	#num#	k4
<g/>
/	/	kIx~
<g/>
34	#num#	k4
•	•	k?
1934	#num#	k4
<g/>
/	/	kIx~
<g/>
35	#num#	k4
•	•	k?
1935	#num#	k4
<g/>
/	/	kIx~
<g/>
36	#num#	k4
•	•	k?
1936	#num#	k4
<g/>
/	/	kIx~
<g/>
37	#num#	k4
•	•	k?
1937	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
38	#num#	k4
•	•	k?
1938	#num#	k4
<g/>
/	/	kIx~
<g/>
39	#num#	k4
•	•	k?
1939	#num#	k4
<g/>
/	/	kIx~
<g/>
40	#num#	k4
•	•	k?
1940	#num#	k4
<g/>
/	/	kIx~
<g/>
41	#num#	k4
•	•	k?
1941	#num#	k4
<g/>
/	/	kIx~
<g/>
42	#num#	k4
•	•	k?
1942	#num#	k4
<g/>
/	/	kIx~
<g/>
43	#num#	k4
•	•	k?
1943	#num#	k4
<g/>
/	/	kIx~
<g/>
44	#num#	k4
•	•	k?
1944	#num#	k4
<g/>
/	/	kIx~
<g/>
45	#num#	k4
•	•	k?
1945	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
46	#num#	k4
•	•	k?
1946	#num#	k4
<g/>
/	/	kIx~
<g/>
47	#num#	k4
•	•	k?
1947	#num#	k4
<g/>
/	/	kIx~
<g/>
48	#num#	k4
•	•	k?
1948	#num#	k4
<g/>
/	/	kIx~
<g/>
49	#num#	k4
•	•	k?
1949	#num#	k4
<g/>
/	/	kIx~
<g/>
50	#num#	k4
•	•	k?
1950	#num#	k4
<g/>
/	/	kIx~
<g/>
51	#num#	k4
•	•	k?
1951	#num#	k4
<g/>
/	/	kIx~
<g/>
52	#num#	k4
•	•	k?
1952	#num#	k4
<g/>
/	/	kIx~
<g/>
53	#num#	k4
•	•	k?
1953	#num#	k4
<g/>
/	/	kIx~
<g/>
54	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1954	#num#	k4
<g/>
/	/	kIx~
<g/>
55	#num#	k4
•	•	k?
1955	#num#	k4
<g/>
/	/	kIx~
<g/>
56	#num#	k4
•	•	k?
1956	#num#	k4
<g/>
/	/	kIx~
<g/>
57	#num#	k4
•	•	k?
1957	#num#	k4
<g/>
/	/	kIx~
<g/>
58	#num#	k4
•	•	k?
1958	#num#	k4
<g/>
/	/	kIx~
<g/>
59	#num#	k4
•	•	k?
1959	#num#	k4
<g/>
/	/	kIx~
<g/>
60	#num#	k4
•	•	k?
1960	#num#	k4
<g/>
/	/	kIx~
<g/>
61	#num#	k4
•	•	k?
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
62	#num#	k4
•	•	k?
1962	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
63	#num#	k4
•	•	k?
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
64	#num#	k4
•	•	k?
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
•	•	k?
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
•	•	k?
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
•	•	k?
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
•	•	k?
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
69	#num#	k4
•	•	k?
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
•	•	k?
1970	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
71	#num#	k4
•	•	k?
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
•	•	k?
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
•	•	k?
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
74	#num#	k4
•	•	k?
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
•	•	k?
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
•	•	k?
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
•	•	k?
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
•	•	k?
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
79	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
•	•	k?
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
•	•	k?
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
•	•	k?
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
•	•	k?
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
•	•	k?
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
•	•	k?
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
•	•	k?
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
•	•	k?
1987	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
88	#num#	k4
•	•	k?
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
•	•	k?
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
•	•	k?
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
•	•	k?
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
•	•	k?
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
•	•	k?
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
•	•	k?
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
•	•	k?
1995	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
96	#num#	k4
•	•	k?
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
•	•	k?
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
•	•	k?
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
•	•	k?
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
•	•	k?
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
•	•	k?
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
•	•	k?
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
3	#num#	k4
•	•	k?
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
•	•	k?
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
•	•	k?
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
•	•	k?
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
•	•	k?
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
•	•	k?
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
•	•	k?
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
•	•	k?
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
•	•	k?
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
•	•	k?
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
•	•	k?
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
•	•	k?
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
•	•	k?
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
•	•	k?
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
•	•	k?
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
•	•	k?
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
•	•	k?
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
Dřívější	dřívější	k2eAgMnPc1d1
účastníci	účastník	k1gMnPc1
(	(	kIx(
<g/>
poslední	poslední	k2eAgFnSc1d1
sezóna	sezóna	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
CE	CE	kA
Europa	Europa	k1gFnSc1
(	(	kIx(
<g/>
1930	#num#	k4
<g/>
/	/	kIx~
<g/>
31	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Real	Real	k1gInSc1
Unión	Unión	k1gInSc1
(	(	kIx(
<g/>
1931	#num#	k4
<g/>
/	/	kIx~
<g/>
32	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Arenas	Arenas	k1gInSc1
Club	club	k1gInSc1
de	de	k?
Getxo	Getxo	k1gMnSc1
(	(	kIx(
<g/>
1934	#num#	k4
<g/>
/	/	kIx~
<g/>
35	#num#	k4
<g/>
)	)	kIx)
•	•	k?
CD	CD	kA
Alcoyano	Alcoyana	k1gFnSc5
(	(	kIx(
<g/>
1950	#num#	k4
<g/>
/	/	kIx~
<g/>
51	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Atlético	Atlético	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Tetuán	Tetuán	k1gInSc1
(	(	kIx(
<g/>
1951	#num#	k4
<g/>
/	/	kIx~
<g/>
52	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Cultural	Cultural	k1gFnSc1
y	y	k?
Deportiva	Deportiva	k1gFnSc1
Leonesa	Leonesa	k1gFnSc1
(	(	kIx(
<g/>
1955	#num#	k4
<g/>
/	/	kIx~
<g/>
56	#num#	k4
<g/>
)	)	kIx)
•	•	k?
CD	CD	kA
Condal	Condal	k1gFnSc2
(	(	kIx(
<g/>
1956	#num#	k4
<g/>
/	/	kIx~
<g/>
57	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Real	Real	k1gInSc1
Jaén	Jaén	k1gInSc1
(	(	kIx(
<g/>
1957	#num#	k4
<g/>
/	/	kIx~
<g/>
58	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Pontevedra	Pontevedra	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
CF	CF	kA
(	(	kIx(
<g/>
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Burgos	Burgos	k1gInSc1
CF	CF	kA
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
<g/>
)	)	kIx)
•	•	k?
AD	ad	k7c4
Almería	Almería	k1gMnSc1
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
<g/>
)	)	kIx)
•	•	k?
CE	CE	kA
Sabadell	Sabadell	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
<g/>
)	)	kIx)
•	•	k?
CD	CD	kA
Castellón	Castellón	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Real	Real	k1gInSc1
Burgos	Burgos	k1gMnSc1
CF	CF	kA
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
<g/>
)	)	kIx)
•	•	k?
UE	UE	kA
Lleida	Lleida	k1gFnSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
)	)	kIx)
•	•	k?
CD	CD	kA
Logroñ	Logroñ	k1gInSc1
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SD	SD	kA
Compostela	Compostela	k1gFnSc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
<g/>
)	)	kIx)
•	•	k?
CP	CP	kA
Mérida	Mérida	k1gFnSc1
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
<g/>
)	)	kIx)
•	•	k?
CF	CF	kA
Extremadura	Extremadura	k1gFnSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
<g/>
)	)	kIx)
•	•	k?
UD	UD	kA
Salamanca	Salamanca	k1gMnSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Real	Real	k1gInSc1
Oviedo	Oviedo	k1gNnSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Albacete	Albace	k1gNnSc2
Balompié	Balompiá	k1gFnSc2
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Cádiz	Cádiz	k1gInSc1
CF	CF	kA
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Gimnà	Gimnà	k1gFnPc2
de	de	k?
Tarragona	Tarragon	k1gMnSc2
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Real	Real	k1gInSc1
Murcia	Murcia	k1gFnSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
)	)	kIx)
•	•	k?
CD	CD	kA
Numancia	Numancium	k1gNnSc2
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Recreativo	Recreativa	k1gFnSc5
de	de	k?
Huelva	Huelvo	k1gNnPc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
)	)	kIx)
•	•	k?
CD	CD	kA
Tenerife	Tenerif	k1gInSc5
(	(	kIx(
<g/>
2009	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Xerez	Xerez	k1gInSc1
CD	CD	kA
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Hércules	Hércules	k1gInSc1
CF	CF	kA
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Racing	Racing	k1gInSc1
de	de	k?
Santander	Santander	k1gInSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Real	Real	k1gInSc1
Zaragoza	Zaragoza	k1gFnSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
/	/	kIx~
<g/>
13	#num#	k4
<g/>
)	)	kIx)
•	•	k?
UD	UD	kA
Almería	Almería	k1gMnSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Córdoba	Córdoba	k1gFnSc1
CF	CF	kA
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Elche	Elche	k1gInSc1
CF	CF	kA
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sporting	Sporting	k1gInSc1
de	de	k?
Gijón	Gijón	k1gInSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
17	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Deportivo	Deportiva	k1gFnSc5
de	de	k?
La	la	k1gNnSc1
Coruñ	Coruñ	k1gMnSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
)	)	kIx)
•	•	k?
UD	UD	kA
Las	laso	k1gNnPc2
Palmas	Palmas	k1gInSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Málaga	Málaga	k1gFnSc1
CF	CF	kA
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Girona	Girona	k1gFnSc1
FC	FC	kA
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Rayo	Rayo	k6eAd1
Vallecano	Vallecana	k1gFnSc5
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SD	SD	kA
Huesca	Huesca	k1gMnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
)	)	kIx)
Trofeje	trofej	k1gInSc2
a	a	k8xC
ocenění	ocenění	k1gNnSc2
</s>
<s>
Premios	Premios	k1gMnSc1
LFP	LFP	kA
•	•	k?
Trofeo	Trofeo	k1gMnSc1
Pichichi	Pichich	k1gFnSc2
•	•	k?
Premio	Premio	k1gMnSc1
Don	Don	k1gMnSc1
Balón	balón	k1gInSc1
•	•	k?
Premios	Premios	k1gInSc1
Santander	Santander	k1gMnSc1
•	•	k?
Trofeo	Trofeo	k1gMnSc1
Zarra	Zarra	k1gMnSc1
•	•	k?
Trofeo	Trofeo	k6eAd1
Ricardo	Ricardo	k1gNnSc1
Zamora	Zamor	k1gMnSc2
•	•	k?
Trofeo	Trofeo	k1gMnSc1
Miguel	Miguel	k1gMnSc1
Muñ	Muñ	k1gMnSc1
•	•	k?
Trofeo	Trofeo	k1gMnSc1
Guruceta	Guruceto	k1gNnSc2
•	•	k?
Trofeo	Trofeo	k1gMnSc1
Guruceta	Gurucet	k1gMnSc2
Jiné	jiný	k2eAgInPc1d1
odkazy	odkaz	k1gInPc7
</s>
<s>
Španělský	španělský	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
•	•	k?
Španělský	španělský	k2eAgInSc1d1
ligový	ligový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
(	(	kIx(
<g/>
zanikl	zaniknout	k5eAaPmAgInS
<g/>
)	)	kIx)
•	•	k?
Španělský	španělský	k2eAgInSc4d1
superpohár	superpohár	k1gInSc4
Fotbal	fotbal	k1gInSc1
ženy	žena	k1gFnSc2
<g/>
:	:	kIx,
Primera	primera	k1gFnSc1
División	División	k1gInSc1
Femenina	Femenina	k1gFnSc1
de	de	k?
Españ	Españ	k2eAgFnSc1d1
•	•	k?
Copa	Copa	k1gFnSc1
de	de	k?
la	la	k1gNnSc1
Reina	Rein	k2eAgNnSc2d1
de	de	k?
Fútbol	Fútbol	k1gInSc1
•	•	k?
Španělská	španělský	k2eAgFnSc1d1
ženská	ženský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
</s>
<s>
Vítězové	vítěz	k1gMnPc1
-	-	kIx~
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1972	#num#	k4
Ajax	Ajax	k1gInSc1
Amsterdam	Amsterdam	k1gInSc4
•	•	k?
1973	#num#	k4
Ajax	Ajax	k1gInSc1
Amsterdam	Amsterdam	k1gInSc4
•	•	k?
1975	#num#	k4
FK	FK	kA
Dynamo	dynamo	k1gNnSc1
Kyjev	Kyjev	k1gInSc1
•	•	k?
1976	#num#	k4
RSC	RSC	kA
Anderlecht	Anderlechta	k1gFnPc2
•	•	k?
1977	#num#	k4
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
•	•	k?
1978	#num#	k4
RSC	RSC	kA
Anderlecht	Anderlechta	k1gFnPc2
•	•	k?
1979	#num#	k4
Nottingham	Nottingham	k1gInSc1
Forest	Forest	k1gInSc4
FC	FC	kA
•	•	k?
1980	#num#	k4
Valencia	Valencius	k1gMnSc2
CF	CF	kA
•	•	k?
1982	#num#	k4
Aston	Aston	k1gNnSc4
Villa	Villo	k1gNnSc2
FC	FC	kA
•	•	k?
1983	#num#	k4
Aberdeen	Aberdeen	k1gInSc1
FC	FC	kA
•	•	k?
1984	#num#	k4
Juventus	Juventus	k1gInSc1
FC	FC	kA
•	•	k?
1986	#num#	k4
FC	FC	kA
Steaua	Steau	k1gInSc2
Bucureș	Bucureș	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1987	#num#	k4
FC	FC	kA
Porto	porto	k1gNnSc1
•	•	k?
1988	#num#	k4
KV	KV	kA
Mechelen	Mechelna	k1gFnPc2
•	•	k?
1989	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1990	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1991	#num#	k4
Manchester	Manchester	k1gInSc1
United	United	k1gInSc4
FC	FC	kA
•	•	k?
1992	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
1993	#num#	k4
AC	AC	kA
Parma	Parma	k1gFnSc1
•	•	k?
1994	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1995	#num#	k4
Ajax	Ajax	k1gInSc1
Amsterdam	Amsterdam	k1gInSc4
•	•	k?
1996	#num#	k4
Juventus	Juventus	k1gInSc1
FC	FC	kA
•	•	k?
1997	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
1998	#num#	k4
Chelsea	Chelseus	k1gMnSc2
FC	FC	kA
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
1999	#num#	k4
Lazio	Lazio	k6eAd1
Řím	Řím	k1gInSc1
•	•	k?
2000	#num#	k4
Galatasaray	Galatasaraa	k1gMnSc2
SK	Sk	kA
•	•	k?
2001	#num#	k4
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
•	•	k?
2002	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2003	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
2004	#num#	k4
Valencia	Valencius	k1gMnSc2
CF	CF	kA
•	•	k?
2005	#num#	k4
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
•	•	k?
2006	#num#	k4
Sevilla	Sevilla	k1gFnSc1
FC	FC	kA
•	•	k?
2007	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
2008	#num#	k4
Zenit	zenit	k1gInSc1
Petrohrad	Petrohrad	k1gInSc4
•	•	k?
2009	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
2010	#num#	k4
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
•	•	k?
2011	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
2012	#num#	k4
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
•	•	k?
2013	#num#	k4
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
•	•	k?
2014	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2015	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
2016	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2017	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2018	#num#	k4
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
•	•	k?
2019	#num#	k4
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
•	•	k?
2020	#num#	k4
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
•	•	k?
2021	#num#	k4
•	•	k?
2022	#num#	k4
•	•	k?
2023	#num#	k4
</s>
<s>
Vítězové	vítěz	k1gMnPc1
-	-	kIx~
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
klubů	klub	k1gInPc2
Interkontinentální	interkontinentální	k2eAgInSc4d1
pohár	pohár	k1gInSc4
-	-	kIx~
předchůdce	předchůdce	k1gMnSc2
</s>
<s>
1960	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
1961	#num#	k4
CA	ca	kA
Peñ	Peñ	k1gFnPc2
•	•	k?
1962	#num#	k4
Santos	Santos	k1gInSc1
FC	FC	kA
•	•	k?
1963	#num#	k4
Santos	Santos	k1gInSc1
FC	FC	kA
•	•	k?
1964	#num#	k4
FC	FC	kA
Inter	Inter	k1gInSc1
Milán	Milán	k1gInSc1
•	•	k?
1965	#num#	k4
FC	FC	kA
Inter	Inter	k1gInSc1
Milán	Milán	k1gInSc1
•	•	k?
1966	#num#	k4
CA	ca	kA
Peñ	Peñ	k1gFnPc2
•	•	k?
1967	#num#	k4
Racing	Racing	k1gInSc1
Club	club	k1gInSc4
•	•	k?
1968	#num#	k4
Estudiantes	Estudiantes	k1gInSc1
de	de	k?
La	la	k1gNnSc1
Plata	plato	k1gNnSc2
•	•	k?
1969	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1970	#num#	k4
Feyenoord	Feyenoord	k1gInSc1
•	•	k?
1971	#num#	k4
Nacional	Nacional	k1gFnSc6
Montevideo	Montevideo	k1gNnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
1972	#num#	k4
AFC	AFC	kA
Ajax	Ajax	k1gInSc1
•	•	k?
1973	#num#	k4
CA	ca	kA
Independiente	Independient	k1gInSc5
•	•	k?
1974	#num#	k4
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
•	•	k?
1975	#num#	k4
Ročník	ročník	k1gInSc1
neodehrán	odehrát	k5eNaPmNgInS
•	•	k?
1976	#num#	k4
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc4
•	•	k?
1977	#num#	k4
CA	ca	kA
Boca	Boca	k1gFnSc1
Juniors	Juniors	k1gInSc1
•	•	k?
1978	#num#	k4
Ročník	ročník	k1gInSc1
neodehrán	odehrát	k5eNaPmNgInS
•	•	k?
1979	#num#	k4
Club	club	k1gInSc1
Olimpia	Olimpium	k1gNnSc2
•	•	k?
1980	#num#	k4
Nacional	Nacional	k1gFnSc6
Montevideo	Montevideo	k1gNnSc1
•	•	k?
1981	#num#	k4
Flamengo	flamengo	k1gNnSc4
•	•	k?
1982	#num#	k4
CA	ca	kA
Peñ	Peñ	k1gFnPc2
•	•	k?
1983	#num#	k4
Grê	Grê	k1gNnSc4
•	•	k?
1984	#num#	k4
CA	ca	kA
Independiente	Independient	k1gInSc5
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1985	#num#	k4
Juventus	Juventus	k1gInSc1
FC	FC	kA
•	•	k?
1986	#num#	k4
CA	ca	kA
River	Rivra	k1gFnPc2
Plate	plat	k1gInSc5
•	•	k?
1987	#num#	k4
FC	FC	kA
Porto	porto	k1gNnSc1
•	•	k?
1988	#num#	k4
Nacional	Nacional	k1gFnSc6
Montevideo	Montevideo	k1gNnSc1
•	•	k?
1989	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1990	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1991	#num#	k4
FK	FK	kA
Crvena	Crven	k2eAgFnSc1d1
zvezda	zvezda	k1gFnSc1
•	•	k?
1992	#num#	k4
Sã	Sã	k6eAd1
Paulo	Paula	k1gFnSc5
FC	FC	kA
•	•	k?
1993	#num#	k4
Sã	Sã	k6eAd1
Paulo	Paula	k1gFnSc5
FC	FC	kA
•	•	k?
1994	#num#	k4
CA	ca	kA
Vélez	Vélez	k1gInSc1
Sarsfield	Sarsfield	k1gInSc1
•	•	k?
1995	#num#	k4
AFC	AFC	kA
Ajax	Ajax	k1gInSc1
•	•	k?
1996	#num#	k4
Juventus	Juventus	k1gInSc1
FC	FC	kA
•	•	k?
1997	#num#	k4
Borussia	Borussium	k1gNnSc2
Dortmund	Dortmund	k1gInSc1
•	•	k?
1998	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
1999	#num#	k4
Manchester	Manchester	k1gInSc1
United	United	k1gInSc4
FC	FC	kA
•	•	k?
2000	#num#	k4
CA	ca	kA
Boca	Boca	k1gFnSc1
Juniors	Juniors	k1gInSc1
•	•	k?
2001	#num#	k4
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc4
•	•	k?
2002	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2003	#num#	k4
CA	ca	kA
Boca	Boca	k1gFnSc1
Juniors	Juniors	k1gInSc1
•	•	k?
2004	#num#	k4
FC	FC	kA
Porto	porto	k1gNnSc1
Mistrovství	mistrovství	k1gNnPc2
světa	svět	k1gInSc2
klubů	klub	k1gInPc2
</s>
<s>
2000	#num#	k4
SC	SC	kA
Corinthians	Corinthiansa	k1gFnPc2
Paulista	Paulista	k1gMnSc1
•	•	k?
2001-2004	2001-2004	k4
•	•	k?
2005	#num#	k4
Sã	Sã	k6eAd1
Paulo	Paula	k1gFnSc5
FC	FC	kA
•	•	k?
2006	#num#	k4
Sport	sport	k1gInSc1
Club	club	k1gInSc4
Internacional	Internacional	k1gFnSc2
•	•	k?
2007	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
2008	#num#	k4
Manchester	Manchester	k1gInSc1
United	United	k1gInSc4
FC	FC	kA
•	•	k?
2009	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
2010	#num#	k4
FC	FC	kA
Inter	Inter	k1gInSc1
Milán	Milán	k1gInSc1
•	•	k?
2011	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
2012	#num#	k4
SC	SC	kA
Corinthians	Corinthiansa	k1gFnPc2
Paulista	Paulista	k1gMnSc1
•	•	k?
2013	#num#	k4
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc4
•	•	k?
2014	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2015	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
2016	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2017	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2018	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2019	#num#	k4
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
•	•	k?
2020	#num#	k4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
olak	olak	k1gInSc1
<g/>
2004203592	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
6512235-5	6512235-5	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
8918	#num#	k4
8257	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
97052180	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
129926959	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
97052180	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fotbal	fotbal	k1gInSc1
|	|	kIx~
Španělsko	Španělsko	k1gNnSc1
</s>
