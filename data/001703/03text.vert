<s>
Giosuè	Giosuè	k?	Giosuè
Carducci	Carducce	k1gMnPc1	Carducce
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1835	[number]	k4	1835
<g/>
,	,	kIx,	,
Valdicastello	Valdicastello	k1gNnSc1	Valdicastello
<g/>
,	,	kIx,	,
Toskánsko	Toskánsko	k1gNnSc1	Toskánsko
-	-	kIx~	-
16	[number]	k4	16
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1907	[number]	k4	1907
<g/>
,	,	kIx,	,
Bologna	Bologna	k1gFnSc1	Bologna
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
italský	italský	k2eAgMnSc1d1	italský
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
literární	literární	k2eAgMnSc1d1	literární
kritik	kritik	k1gMnSc1	kritik
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1860	[number]	k4	1860
až	až	k9	až
1904	[number]	k4	1904
profesor	profesor	k1gMnSc1	profesor
italské	italský	k2eAgFnSc2d1	italská
literatury	literatura	k1gFnSc2	literatura
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Bologni	Bologna	k1gFnSc6	Bologna
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
<g/>
.	.	kIx.	.
</s>
<s>
Giosuè	Giosuè	k?	Giosuè
Carducci	Carducec	k1gInSc6	Carducec
začal	začít	k5eAaPmAgMnS	začít
psát	psát	k5eAaImF	psát
poezii	poezie	k1gFnSc4	poezie
již	již	k6eAd1	již
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
antických	antický	k2eAgInPc2d1	antický
vzorů	vzor	k1gInPc2	vzor
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
však	však	k9	však
začal	začít	k5eAaPmAgInS	začít
uplatňovat	uplatňovat	k5eAaImF	uplatňovat
revoluční	revoluční	k2eAgFnPc4d1	revoluční
tendence	tendence	k1gFnPc4	tendence
(	(	kIx(	(
<g/>
byl	být	k5eAaImAgMnS	být
synem	syn	k1gMnSc7	syn
venkovského	venkovský	k2eAgMnSc2d1	venkovský
lékaře	lékař	k1gMnSc2	lékař
<g/>
,	,	kIx,	,
člena	člen	k1gMnSc2	člen
hnutí	hnutí	k1gNnSc2	hnutí
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
a	a	k8xC	a
obnovu	obnova	k1gFnSc4	obnova
národní	národní	k2eAgFnSc2d1	národní
jednoty	jednota	k1gFnSc2	jednota
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
ve	v	k7c6	v
Florencii	Florencie	k1gFnSc6	Florencie
a	a	k8xC	a
v	v	k7c6	v
Pise	Pisa	k1gFnSc6	Pisa
<g/>
,	,	kIx,	,
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
jako	jako	k9	jako
radikální	radikální	k2eAgMnSc1d1	radikální
republikán	republikán	k1gMnSc1	republikán
a	a	k8xC	a
antiklerikál	antiklerikál	k1gMnSc1	antiklerikál
sympatizující	sympatizující	k2eAgMnSc1d1	sympatizující
se	s	k7c7	s
socialistickým	socialistický	k2eAgNnSc7d1	socialistické
a	a	k8xC	a
dělnickým	dělnický	k2eAgNnSc7d1	dělnické
hnutím	hnutí	k1gNnSc7	hnutí
a	a	k8xC	a
odpůrci	odpůrce	k1gMnPc1	odpůrce
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2	Rakouska-Uhersk
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
viděli	vidět	k5eAaImAgMnP	vidět
vzor	vzor	k1gInSc1	vzor
svých	svůj	k3xOyFgFnPc2	svůj
osvobozovacích	osvobozovací	k2eAgFnPc2d1	osvobozovací
tendencí	tendence	k1gFnPc2	tendence
<g/>
.	.	kIx.	.
</s>
<s>
Sjednocení	sjednocení	k1gNnSc1	sjednocení
Itálie	Itálie	k1gFnSc2	Itálie
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
mu	on	k3xPp3gMnSc3	on
dalo	dát	k5eAaPmAgNnS	dát
nové	nový	k2eAgFnSc3d1	nová
umělecké	umělecký	k2eAgFnSc3d1	umělecká
možnosti	možnost	k1gFnSc3	možnost
a	a	k8xC	a
prohloubilo	prohloubit	k5eAaPmAgNnS	prohloubit
jeho	jeho	k3xOp3gFnSc4	jeho
básnickou	básnický	k2eAgFnSc4d1	básnická
inspiraci	inspirace	k1gFnSc4	inspirace
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
setkání	setkání	k1gNnSc6	setkání
s	s	k7c7	s
královnou	královna	k1gFnSc7	královna
Margheritou	Margherita	k1gFnSc7	Margherita
roku	rok	k1gInSc2	rok
1878	[number]	k4	1878
se	se	k3xPyFc4	se
přiklonil	přiklonit	k5eAaPmAgInS	přiklonit
k	k	k7c3	k
monarchii	monarchie	k1gFnSc3	monarchie
a	a	k8xC	a
umělecky	umělecky	k6eAd1	umělecky
ke	k	k7c3	k
klasickému	klasický	k2eAgInSc3d1	klasický
realismu	realismus	k1gInSc3	realismus
(	(	kIx(	(
<g/>
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
odpůrcem	odpůrce	k1gMnSc7	odpůrce
nejen	nejen	k6eAd1	nejen
křesťanského	křesťanský	k2eAgInSc2d1	křesťanský
mysticismu	mysticismus	k1gInSc2	mysticismus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
romantismu	romantismus	k1gInSc2	romantismus
Alessandra	Alessandra	k1gFnSc1	Alessandra
Manzoniho	Manzoni	k1gMnSc2	Manzoni
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
je	být	k5eAaImIp3nS	být
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
národního	národní	k2eAgMnSc4d1	národní
klasika	klasik	k1gMnSc4	klasik
a	a	k8xC	a
nejvýznamnějšího	významný	k2eAgMnSc4d3	nejvýznamnější
básníka	básník	k1gMnSc4	básník
jednotné	jednotný	k2eAgFnSc2d1	jednotná
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
skladeb	skladba	k1gFnPc2	skladba
básnických	básnický	k2eAgInPc2d1	básnický
by	by	kYmCp3nS	by
Carducci	Carducce	k1gFnSc4	Carducce
také	také	k9	také
neúnavným	únavný	k2eNgMnSc7d1	neúnavný
badatelem	badatel	k1gMnSc7	badatel
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
literární	literární	k2eAgFnSc2d1	literární
historie	historie	k1gFnSc2	historie
a	a	k8xC	a
vynikajícím	vynikající	k2eAgMnSc7d1	vynikající
vydavatelem	vydavatel	k1gMnSc7	vydavatel
a	a	k8xC	a
komentátorem	komentátor	k1gMnSc7	komentátor
starých	starý	k2eAgInPc2d1	starý
klasických	klasický	k2eAgInPc2d1	klasický
děl	dělo	k1gNnPc2	dělo
italské	italský	k2eAgFnSc2d1	italská
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1860	[number]	k4	1860
až	až	k9	až
1904	[number]	k4	1904
byl	být	k5eAaImAgInS	být
profesorem	profesor	k1gMnSc7	profesor
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Bologni	Bologna	k1gFnSc6	Bologna
<g/>
.	.	kIx.	.
</s>
<s>
Vysokého	vysoký	k2eAgNnSc2d1	vysoké
ocenění	ocenění	k1gNnSc2	ocenění
se	se	k3xPyFc4	se
dočkal	dočkat	k5eAaPmAgInS	dočkat
i	i	k9	i
v	v	k7c6	v
aktivní	aktivní	k2eAgFnSc6d1	aktivní
politice	politika	k1gFnSc6	politika
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
čestným	čestný	k2eAgMnSc7d1	čestný
občanem	občan	k1gMnSc7	občan
v	v	k7c6	v
Bologni	Bologna	k1gFnSc6	Bologna
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1890	[number]	k4	1890
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgNnSc4d3	nejvyšší
literární	literární	k2eAgNnSc4d1	literární
ocenění	ocenění	k1gNnSc4	ocenění
v	v	k7c6	v
cizině	cizina	k1gFnSc6	cizina
<g/>
,	,	kIx,	,
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
udělenou	udělený	k2eAgFnSc4d1	udělená
mu	on	k3xPp3gMnSc3	on
"	"	kIx"	"
<g/>
...	...	k?	...
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
bohaté	bohatý	k2eAgInPc4d1	bohatý
vědecké	vědecký	k2eAgInPc4d1	vědecký
poznatky	poznatek	k1gInPc4	poznatek
a	a	k8xC	a
kritické	kritický	k2eAgInPc4d1	kritický
výzkumy	výzkum	k1gInPc4	výzkum
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k6eAd1	především
jako	jako	k8xS	jako
hold	hold	k1gInSc4	hold
tvůrčí	tvůrčí	k2eAgFnSc4d1	tvůrčí
energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
stylistické	stylistický	k2eAgFnPc1d1	stylistická
svěžesti	svěžest	k1gFnPc1	svěžest
a	a	k8xC	a
působivé	působivý	k2eAgFnPc1d1	působivá
lyričnosti	lyričnost	k1gFnPc1	lyričnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
charakterizují	charakterizovat	k5eAaBmIp3nP	charakterizovat
jeho	jeho	k3xOp3gNnPc4	jeho
poeticky	poeticky	k6eAd1	poeticky
mistrovská	mistrovský	k2eAgNnPc4d1	mistrovské
díla	dílo	k1gNnPc4	dílo
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
citace	citace	k1gFnPc1	citace
z	z	k7c2	z
odůvodnění	odůvodnění	k1gNnSc2	odůvodnění
Švédské	švédský	k2eAgFnSc2d1	švédská
akademie	akademie	k1gFnSc2	akademie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nemohl	moct	k5eNaImAgMnS	moct
již	již	k6eAd1	již
pro	pro	k7c4	pro
nemoc	nemoc	k1gFnSc4	nemoc
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
osobně	osobně	k6eAd1	osobně
převzít	převzít	k5eAaPmF	převzít
a	a	k8xC	a
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
později	pozdě	k6eAd2	pozdě
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1859	[number]	k4	1859
byl	být	k5eAaImAgMnS	být
ženatý	ženatý	k2eAgMnSc1d1	ženatý
s	s	k7c7	s
Elvirou	Elvira	k1gFnSc7	Elvira
Manicuciovou	Manicuciový	k2eAgFnSc7d1	Manicuciový
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
tři	tři	k4xCgFnPc4	tři
dcery	dcera	k1gFnPc4	dcera
a	a	k8xC	a
jednoho	jeden	k4xCgMnSc2	jeden
syna	syn	k1gMnSc2	syn
<g/>
.	.	kIx.	.
</s>
<s>
Rime	Rime	k1gFnSc1	Rime
(	(	kIx(	(
<g/>
1857	[number]	k4	1857
<g/>
,	,	kIx,	,
Verše	verš	k1gInSc2	verš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Juvenilia	juvenilia	k1gNnPc1	juvenilia
(	(	kIx(	(
<g/>
1857	[number]	k4	1857
<g/>
-	-	kIx~	-
<g/>
1880	[number]	k4	1880
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rime	Rime	k1gFnSc1	Rime
Nuove	Nuoev	k1gFnSc2	Nuoev
(	(	kIx(	(
<g/>
1861	[number]	k4	1861
<g/>
-	-	kIx~	-
<g/>
1887	[number]	k4	1887
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgInPc1d1	Nové
verše	verš	k1gInPc1	verš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
motivy	motiv	k1gInPc7	motiv
politickými	politický	k2eAgInPc7d1	politický
i	i	k8xC	i
intimními	intimní	k2eAgInPc7d1	intimní
<g/>
,	,	kIx,	,
Levia	Levius	k1gMnSc2	Levius
gravia	gravius	k1gMnSc2	gravius
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1861	[number]	k4	1861
<g/>
-	-	kIx~	-
<g/>
1871	[number]	k4	1871
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Inno	Inno	k1gMnSc1	Inno
a	a	k8xC	a
Satana	Satan	k1gMnSc2	Satan
(	(	kIx(	(
<g/>
1865	[number]	k4	1865
<g/>
,	,	kIx,	,
Hymnus	hymnus	k1gInSc1	hymnus
na	na	k7c4	na
Satana	Satan	k1gMnSc4	Satan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
báseň	báseň	k1gFnSc1	báseň
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
Carducci	Carducce	k1gFnSc6	Carducce
zobrazil	zobrazit	k5eAaPmAgMnS	zobrazit
Satana	Satan	k1gMnSc4	Satan
jako	jako	k9	jako
symbol	symbol	k1gInSc4	symbol
pokroku	pokrok	k1gInSc2	pokrok
<g/>
,	,	kIx,	,
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
pozemské	pozemský	k2eAgFnSc2d1	pozemská
radosti	radost	k1gFnSc2	radost
<g/>
,	,	kIx,	,
učinila	učinit	k5eAaImAgFnS	učinit
autora	autor	k1gMnSc4	autor
slavným	slavný	k2eAgInSc7d1	slavný
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
vzbudila	vzbudit	k5eAaPmAgFnS	vzbudit
též	též	k9	též
bouřlivé	bouřlivý	k2eAgFnPc4d1	bouřlivá
polemiky	polemika	k1gFnPc4	polemika
<g/>
.	.	kIx.	.
</s>
<s>
Giambi	Giambi	k6eAd1	Giambi
ed	ed	k?	ed
epodi	epod	k1gMnPc1	epod
(	(	kIx(	(
<g/>
1867	[number]	k4	1867
<g/>
,	,	kIx,	,
Jamby	jamb	k1gInPc4	jamb
a	a	k8xC	a
epody	epod	k1gInPc4	epod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Decennalia	Decennalia	k1gFnSc1	Decennalia
(	(	kIx(	(
<g/>
1871	[number]	k4	1871
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Poesie	poesie	k1gFnSc1	poesie
(	(	kIx(	(
<g/>
1871	[number]	k4	1871
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nuove	Nuoev	k1gFnSc2	Nuoev
poesie	poesie	k1gFnSc2	poesie
(	(	kIx(	(
<g/>
1873	[number]	k4	1873
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Studii	studie	k1gFnSc4	studie
letterarii	letterarie	k1gFnSc4	letterarie
(	(	kIx(	(
<g/>
1874	[number]	k4	1874
<g/>
-	-	kIx~	-
<g/>
1881	[number]	k4	1881
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Literární	literární	k2eAgFnPc1d1	literární
studie	studie	k1gFnPc1	studie
<g/>
)	)	kIx)	)
Odi	Odi	k1gMnSc5	Odi
Barbare	barbar	k1gMnSc5	barbar
(	(	kIx(	(
<g/>
1877	[number]	k4	1877
<g/>
,	,	kIx,	,
Ódy	óda	k1gFnPc1	óda
barbarské	barbarský	k2eAgFnPc1d1	barbarská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nuovi	Nuoev	k1gFnSc3	Nuoev
Odi	Odi	k1gFnSc2	Odi
Barbari	Barbar	k1gFnSc2	Barbar
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgInPc1d1	Nové
ódy	ód	k1gInPc1	ód
barbarské	barbarský	k2eAgInPc1d1	barbarský
<g/>
)	)	kIx)	)
a	a	k8xC	a
Terzi	Terze	k1gFnSc4	Terze
Odi	Odi	k1gMnSc5	Odi
Barbare	barbar	k1gMnSc5	barbar
(	(	kIx(	(
<g/>
1889	[number]	k4	1889
<g/>
,	,	kIx,	,
Třetí	třetí	k4xOgFnPc1	třetí
ódy	óda	k1gFnPc1	óda
barbarské	barbarský	k2eAgFnPc1d1	barbarská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tři	tři	k4xCgFnPc4	tři
sbírky	sbírka	k1gFnPc4	sbírka
lyrické	lyrický	k2eAgFnSc2d1	lyrická
poezie	poezie	k1gFnSc2	poezie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
geniálním	geniální	k2eAgInSc7d1	geniální
způsobem	způsob	k1gInSc7	způsob
a	a	k8xC	a
s	s	k7c7	s
velikou	veliký	k2eAgFnSc7d1	veliká
erudicí	erudice	k1gFnSc7	erudice
napodobil	napodobit	k5eAaPmAgInS	napodobit
staré	starý	k2eAgInPc4d1	starý
metrické	metrický	k2eAgInPc4d1	metrický
prvky	prvek	k1gInPc4	prvek
Horaciových	Horaciový	k2eAgFnPc2d1	Horaciový
ód	óda	k1gFnPc2	óda
a	a	k8xC	a
zpracoval	zpracovat	k5eAaPmAgMnS	zpracovat
je	být	k5eAaImIp3nS	být
v	v	k7c4	v
novou	nový	k2eAgFnSc4d1	nová
ryze	ryze	k6eAd1	ryze
moderní	moderní	k2eAgFnSc4d1	moderní
poezii	poezie	k1gFnSc4	poezie
<g/>
,	,	kIx,	,
plnou	plný	k2eAgFnSc4d1	plná
síly	síla	k1gFnPc4	síla
<g/>
,	,	kIx,	,
ohně	oheň	k1gInSc2	oheň
a	a	k8xC	a
hlubokých	hluboký	k2eAgInPc2d1	hluboký
citových	citový	k2eAgInPc2d1	citový
akcentů	akcent	k1gInPc2	akcent
<g/>
.	.	kIx.	.
</s>
<s>
La	la	k1gNnSc1	la
poesia	poesius	k1gMnSc2	poesius
barbara	barbar	k1gMnSc2	barbar
nei	nei	k?	nei
secoli	secole	k1gFnSc4	secole
XV	XV	kA	XV
<g/>
.	.	kIx.	.
e	e	k0	e
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1881	[number]	k4	1881
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odborné	odborný	k2eAgNnSc1d1	odborné
dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
Confessioni	Confession	k1gMnPc1	Confession
e	e	k0	e
battaglie	battaglie	k1gFnPc1	battaglie
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odborné	odborný	k2eAgNnSc1d1	odborné
dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
Ça	Ça	k1gFnSc1	Ça
ira	ira	k?	ira
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Conversationi	Conversatioň	k1gFnSc3	Conversatioň
critiche	critich	k1gFnSc2	critich
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1884	[number]	k4	1884
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odborné	odborný	k2eAgNnSc1d1	odborné
dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
Vite	Vite	k?	Vite
e	e	k0	e
ritratti	ritratti	k1gNnPc1	ritratti
(	(	kIx(	(
<g/>
1885	[number]	k4	1885
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odborné	odborný	k2eAgNnSc4d1	odborné
dílo	dílo	k1gNnSc4	dílo
Piemonte	Piemont	k1gInSc5	Piemont
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historicko-politická	historickoolitický	k2eAgFnSc1d1	historicko-politická
óda	óda	k1gFnSc1	óda
<g/>
,	,	kIx,	,
Rime	Rime	k1gFnSc1	Rime
e	e	k0	e
ritmi	rit	k1gFnPc7	rit
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
-	-	kIx~	-
<g/>
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
Výbor	výbor	k1gInSc1	výbor
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g />
.	.	kIx.	.
</s>
<s>
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1890	[number]	k4	1890
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Vrchlický	Vrchlický	k1gMnSc1	Vrchlický
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc1d1	nový
výbor	výbor	k1gInSc1	výbor
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Vrchlický	Vrchlický	k1gMnSc1	Vrchlický
<g/>
,	,	kIx,	,
Ódy	óda	k1gFnSc2	óda
barbarské	barbarský	k2eAgFnSc2d1	barbarská
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Vrchlický	Vrchlický	k1gMnSc1	Vrchlický
<g/>
,	,	kIx,	,
Satanu	satan	k1gInSc2	satan
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Váchal	Váchal	k1gMnSc1	Váchal
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
bibliofile	bibliofil	k1gMnSc5	bibliofil
s	s	k7c7	s
deseti	deset	k4xCc7	deset
barevnými	barevný	k2eAgInPc7d1	barevný
obrazy	obraz	k1gInPc7	obraz
dřevorytce	dřevorytec	k1gMnSc2	dřevorytec
Josefa	Josef	k1gMnSc2	Josef
Váchala	Váchal	k1gMnSc2	Váchal
<g/>
,	,	kIx,	,
Od	od	k7c2	od
pramenů	pramen	k1gInPc2	pramen
Clitumna	Clitumno	k1gNnSc2	Clitumno
<g/>
,	,	kIx,	,
Nakladatelské	nakladatelský	k2eAgNnSc1d1	nakladatelské
družstvo	družstvo	k1gNnSc1	družstvo
Máje	máje	k1gFnSc1	máje
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Kalista	Kalista	k1gMnSc1	Kalista
<g/>
,	,	kIx,	,
Hrozny	hrozen	k1gInPc1	hrozen
v	v	k7c6	v
sloupoví	sloupoví	k1gNnSc6	sloupoví
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Kalista	Kalista	k1gMnSc1	Kalista
<g/>
,	,	kIx,	,
Hněvy	hněv	k1gInPc1	hněv
a	a	k8xC	a
smutky	smutek	k1gInPc1	smutek
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Frýbort	Frýbort	k1gInSc1	Frýbort
<g/>
.	.	kIx.	.
</s>
