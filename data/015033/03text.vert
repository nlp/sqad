<s>
Vanilkové	vanilkový	k2eAgInPc1d1
rohlíčky	rohlíček	k1gInPc1
</s>
<s>
Vanilkové	vanilkový	k2eAgInPc1d1
rohlíčky	rohlíček	k1gInPc1
</s>
<s>
Vanilkové	vanilkový	k2eAgInPc1d1
rohlíčky	rohlíček	k1gInPc1
(	(	kIx(
<g/>
německý	německý	k2eAgInSc1d1
název	název	k1gInSc1
<g/>
:	:	kIx,
Vanillekipferl	Vanillekipferl	k1gInSc1
<g/>
,	,	kIx,
bavorský	bavorský	k2eAgInSc1d1
název	název	k1gInSc1
Vanüllekipfal	Vanüllekipfal	k1gFnSc2
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
jedním	jeden	k4xCgInSc7
z	z	k7c2
nejznámějších	známý	k2eAgNnPc2d3
vánočních	vánoční	k2eAgNnPc2d1
cukroví	cukroví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vanilkové	vanilkový	k2eAgInPc1d1
rohlíčky	rohlíček	k1gInPc1
pocházejí	pocházet	k5eAaImIp3nP
z	z	k7c2
Rakouska	Rakousko	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
jsou	být	k5eAaImIp3nP
známé	známý	k2eAgInPc1d1
na	na	k7c6
území	území	k1gNnSc6
celé	celý	k2eAgFnSc2d1
bývalé	bývalý	k2eAgFnSc2d1
Rakousko-Uherské	rakousko-uherský	k2eAgFnSc2d1
monarchie	monarchie	k1gFnSc2
(	(	kIx(
<g/>
kromě	kromě	k7c2
Česka	Česko	k1gNnSc2
také	také	k9
v	v	k7c6
Rakousku	Rakousko	k1gNnSc6
<g/>
,	,	kIx,
Polsku	Polsko	k1gNnSc6
<g/>
,	,	kIx,
Rumunsku	Rumunsko	k1gNnSc6
<g/>
,	,	kIx,
Maďarsku	Maďarsko	k1gNnSc6
<g/>
,	,	kIx,
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
a	a	k8xC
v	v	k7c6
Alto	Alto	k6eAd1
Adige	Adige	k1gFnSc6
v	v	k7c6
severní	severní	k2eAgFnSc6d1
Itálii	Itálie	k1gFnSc6
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
kromě	kromě	k7c2
toho	ten	k3xDgNnSc2
jsou	být	k5eAaImIp3nP
též	též	k9
známé	známý	k2eAgInPc1d1
v	v	k7c6
Bavorsku	Bavorsko	k1gNnSc6
(	(	kIx(
<g/>
vanilkové	vanilkový	k2eAgInPc1d1
rohlíčky	rohlíček	k1gInPc1
jsou	být	k5eAaImIp3nP
specialitou	specialita	k1gFnSc7
bavorského	bavorský	k2eAgNnSc2d1
města	město	k1gNnSc2
Nördlingen	Nördlingen	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
Mísa	mísa	k1gFnSc1
vánočního	vánoční	k2eAgNnSc2d1
cukroví	cukroví	k1gNnSc2
<g/>
,	,	kIx,
vanilkové	vanilkový	k2eAgInPc1d1
rohlíčky	rohlíček	k1gInPc1
jsou	být	k5eAaImIp3nP
vpravo	vpravo	k6eAd1
dole	dole	k6eAd1
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Vanilkové	vanilkový	k2eAgInPc1d1
rohlíčky	rohlíček	k1gInPc1
jsou	být	k5eAaImIp3nP
malé	malý	k2eAgInPc1d1
rohlíčky	rohlíček	k1gInPc1
z	z	k7c2
křehkého	křehký	k2eAgNnSc2d1
těsta	těsto	k1gNnSc2
ve	v	k7c6
tvaru	tvar	k1gInSc6
půlměsíce	půlměsíc	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těsto	těsto	k1gNnSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
hladké	hladký	k2eAgFnSc2d1
mouky	mouka	k1gFnSc2
<g/>
,	,	kIx,
cukru	cukr	k1gInSc2
<g/>
,	,	kIx,
namletých	namletý	k2eAgInPc2d1
vlašských	vlašský	k2eAgInPc2d1
ořechů	ořech	k1gInPc2
(	(	kIx(
<g/>
případně	případně	k6eAd1
se	se	k3xPyFc4
dají	dát	k5eAaPmIp3nP
použít	použít	k5eAaPmF
též	též	k9
lískové	lískový	k2eAgInPc4d1
ořechy	ořech	k1gInPc4
<g/>
,	,	kIx,
arašídy	arašíd	k1gInPc4
nebo	nebo	k8xC
mandle	mandle	k1gFnPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
másla	máslo	k1gNnSc2
<g/>
,	,	kIx,
žloutku	žloutek	k1gInSc2
<g/>
,	,	kIx,
citronové	citronový	k2eAgFnSc2d1
kůry	kůra	k1gFnSc2
a	a	k8xC
vanilky	vanilka	k1gFnSc2
(	(	kIx(
<g/>
případně	případně	k6eAd1
vanilkového	vanilkový	k2eAgInSc2d1
cukru	cukr	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c4
upečení	upečení	k1gNnSc4
se	se	k3xPyFc4
rohlíčky	rohlíček	k1gInPc7
obalují	obalovat	k5eAaImIp3nP
v	v	k7c6
moučkovém	moučkový	k2eAgInSc6d1
cukru	cukr	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výroba	výroba	k1gFnSc1
vanilkových	vanilkový	k2eAgInPc2d1
rohlíčků	rohlíček	k1gInPc2
</s>
<s>
Nejprve	nejprve	k6eAd1
se	se	k3xPyFc4
z	z	k7c2
těsta	těsto	k1gNnSc2
vyválí	vyválet	k5eAaPmIp3nS
dlouhý	dlouhý	k2eAgInSc1d1
úzký	úzký	k2eAgInSc1d1
váleček	váleček	k1gInSc1
</s>
<s>
Poté	poté	k6eAd1
se	se	k3xPyFc4
těsto	těsto	k1gNnSc1
rozkrájí	rozkrájet	k5eAaPmIp3nS
na	na	k7c4
menší	malý	k2eAgInPc4d2
kousky	kousek	k1gInPc4
</s>
<s>
Nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
tyto	tento	k3xDgInPc1
kousky	kousek	k1gInPc1
těsta	těsto	k1gNnSc2
vytvarují	vytvarovat	k5eAaPmIp3nP
do	do	k7c2
tvaru	tvar	k1gInSc2
půlměsíce	půlměsíc	k1gInSc2
a	a	k8xC
pečou	péct	k5eAaImIp3nP
se	se	k3xPyFc4
</s>
<s>
Po	po	k7c4
upečení	upečení	k1gNnSc4
se	se	k3xPyFc4
rohlíčky	rohlíček	k1gInPc7
obalují	obalovat	k5eAaImIp3nP
v	v	k7c6
moučkovém	moučkový	k2eAgInSc6d1
cukru	cukr	k1gInSc6
</s>
<s>
Vlajka	vlajka	k1gFnSc1
Osmanské	osmanský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Vanilkové	vanilkový	k2eAgInPc4d1
rohlíčky	rohlíček	k1gInPc4
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
Rakouska	Rakousko	k1gNnSc2
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
z	z	k7c2
Vídně	Vídeň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
pověsti	pověst	k1gFnSc2
se	se	k3xPyFc4
začaly	začít	k5eAaPmAgInP
péct	péct	k5eAaImF
na	na	k7c4
počest	počest	k1gFnSc4
porážky	porážka	k1gFnSc2
osmanských	osmanský	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
u	u	k7c2
Vídně	Vídeň	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1683	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osmanská	osmanský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
tehdy	tehdy	k6eAd1
měla	mít	k5eAaImAgFnS
na	na	k7c6
vlajce	vlajka	k1gFnSc6
půlměsíc	půlměsíc	k1gInSc4
<g/>
,	,	kIx,
proto	proto	k8xC
se	se	k3xPyFc4
začaly	začít	k5eAaPmAgFnP
péct	péct	k5eAaImF
vanilkové	vanilkový	k2eAgInPc4d1
půlměsíčky	půlměsíček	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ty	k3xPp2nSc1
se	se	k3xPyFc4
potom	potom	k6eAd1
rozšířily	rozšířit	k5eAaPmAgFnP
po	po	k7c6
celé	celý	k2eAgFnSc6d1
Rakouské	rakouský	k2eAgFnSc6d1
monarchii	monarchie	k1gFnSc6
<g/>
,	,	kIx,
mj.	mj.	kA
také	také	k9
do	do	k7c2
Česka	Česko	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
staly	stát	k5eAaPmAgFnP
typickým	typický	k2eAgNnSc7d1
vánočním	vánoční	k2eAgNnSc7d1
cukrovím	cukroví	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
původním	původní	k2eAgInSc6d1
rakouském	rakouský	k2eAgInSc6d1
receptu	recept	k1gInSc6
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
zásadně	zásadně	k6eAd1
mleté	mletý	k2eAgFnPc4d1
mandle	mandle	k1gFnPc4
<g/>
,	,	kIx,
v	v	k7c6
Česku	Česko	k1gNnSc6
se	se	k3xPyFc4
častěji	často	k6eAd2
používají	používat	k5eAaImIp3nP
vlašské	vlašský	k2eAgInPc1d1
ořechy	ořech	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Vanilkové	vanilkový	k2eAgInPc1d1
rohlíčky	rohlíček	k1gInPc1
</s>
<s>
Vanilkový	vanilkový	k2eAgInSc1d1
rohlíček	rohlíček	k1gInSc1
<g/>
,	,	kIx,
neobalený	obalený	k2eNgInSc1d1
v	v	k7c6
cukru	cukr	k1gInSc6
</s>
<s>
Bezlepkové	bezlepkový	k2eAgInPc1d1
vanilkové	vanilkový	k2eAgInPc1d1
rohlíčky	rohlíček	k1gInPc1
</s>
<s>
Talíř	talíř	k1gInSc1
s	s	k7c7
různými	různý	k2eAgInPc7d1
druhy	druh	k1gInPc7
vánočního	vánoční	k2eAgNnSc2d1
cukroví	cukroví	k1gNnSc2
<g/>
,	,	kIx,
vanilkové	vanilkový	k2eAgInPc1d1
rohlíčky	rohlíček	k1gInPc1
jsou	být	k5eAaImIp3nP
vpravo	vpravo	k6eAd1
</s>
<s>
Vanilkové	vanilkový	k2eAgInPc1d1
rohlíčky	rohlíček	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Cornetti	Cornetti	k1gNnSc1
alla	allus	k1gMnSc2
vaniglia	vaniglius	k1gMnSc2
<g/>
.	.	kIx.
www.suedtirol.info	www.suedtirol.info	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
italsky	italsky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
Vanillekipferl	Vanillekipferla	k1gFnPc2
<g/>
:	:	kIx,
The	The	k1gFnPc2
Austrian	Austrian	k1gInSc1
Crescent-shaped	Crescent-shaped	k1gMnSc1
Biscuits	Biscuits	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biscuit	Biscuita	k1gFnPc2
people	people	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-12-13	2019-12-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Vanillekipferl	Vanillekipferla	k1gFnPc2
-	-	kIx~
half-moon	half-moon	k1gInSc1
vanilla	vanilla	k6eAd1
biscuits	biscuits	k6eAd1
recipe	recipe	k1gNnSc1
<g/>
.	.	kIx.
www.telegraph.co.uk	www.telegraph.co.uk	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
307	#num#	k4
<g/>
-	-	kIx~
<g/>
1235	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Nejlepší	dobrý	k2eAgInPc4d3
vanilkové	vanilkový	k2eAgInPc4d1
rohlíčky	rohlíček	k1gInPc4
jako	jako	k8xC,k8xS
od	od	k7c2
babičky	babička	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prima	prima	k2eAgInSc1d1
Fresh	Fresh	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
VANILKOVÉ	vanilkový	k2eAgInPc1d1
ROHLÍČKY	rohlíček	k1gInPc1
–	–	k?
La	la	k1gNnSc1
Buchta	Buchta	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
ADMIN	ADMIN	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jimeto	Jimeto	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-12-15	2018-12-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
vanilkové	vanilkový	k2eAgInPc1d1
rohlíčky	rohlíček	k1gInPc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Gastronomie	gastronomie	k1gFnPc1
|	|	kIx~
Vánoce	Vánoce	k1gFnPc1
</s>
