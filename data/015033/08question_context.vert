<s desamb="1">
Osmanská	osmanský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
tehdy	tehdy	k6eAd1
měla	mít	k5eAaImAgFnS
na	na	k7c6
vlajce	vlajka	k1gFnSc6
půlměsíc	půlměsíc	k1gInSc4
<g/>
,	,	kIx,
proto	proto	k8xC
se	se	k3xPyFc4
začaly	začít	k5eAaPmAgFnP
péct	péct	k5eAaImF
vanilkové	vanilkový	k2eAgInPc4d1
půlměsíčky	půlměsíček	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Vanilkové	vanilkový	k2eAgInPc4d1
rohlíčky	rohlíček	k1gInPc4
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
Rakouska	Rakousko	k1gNnSc2
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
z	z	k7c2
Vídně	Vídeň	k1gFnSc2
<g/>
.	.	kIx.
</s>