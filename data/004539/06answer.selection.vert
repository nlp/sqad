<s>
Francie	Francie	k1gFnSc1	Francie
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
La	la	k1gNnSc1	la
France	Franc	k1gMnSc2	Franc
<g/>
,	,	kIx,	,
výslovnost	výslovnost	k1gFnSc1	výslovnost
/	/	kIx~	/
<g/>
fʀ	fʀ	k?	fʀ
<g/>
̃	̃	k?	̃
<g/>
s	s	k7c7	s
<g/>
/	/	kIx~	/
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oficiální	oficiální	k2eAgInSc1d1	oficiální
název	název	k1gInSc1	název
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
République	République	k1gFnSc1	République
française	française	k1gFnSc1	française
<g/>
,	,	kIx,	,
výslovnost	výslovnost	k1gFnSc1	výslovnost
/	/	kIx~	/
<g/>
ʀ	ʀ	k?	ʀ
fʀ	fʀ	k?	fʀ
<g/>
̃	̃	k?	̃
<g/>
sɛ	sɛ	k?	sɛ
<g/>
/	/	kIx~	/
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
demokratický	demokratický	k2eAgInSc1d1	demokratický
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
většina	většina	k1gFnSc1	většina
území	území	k1gNnSc2	území
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
označovaná	označovaný	k2eAgFnSc1d1	označovaná
jako	jako	k8xS	jako
Metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
