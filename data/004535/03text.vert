<s>
Drum	Drum	k1gInSc1	Drum
and	and	k?	and
bass	bass	k1gInSc1	bass
/	/	kIx~	/
<g/>
dɹ	dɹ	k?	dɹ
ə	ə	k?	ə
beɪ	beɪ	k?	beɪ
<g/>
/	/	kIx~	/
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
drum	drum	k6eAd1	drum
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
bass	bass	k1gInSc1	bass
<g/>
,	,	kIx,	,
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
b	b	k?	b
či	či	k8xC	či
dnb	dnb	k?	dnb
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hudební	hudební	k2eAgInSc4d1	hudební
žánr	žánr	k1gInSc4	žánr
elektronické	elektronický	k2eAgFnSc2d1	elektronická
taneční	taneční	k2eAgFnSc2d1	taneční
hudby	hudba	k1gFnSc2	hudba
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
styl	styl	k1gInSc4	styl
jsou	být	k5eAaImIp3nP	být
charakteristické	charakteristický	k2eAgFnPc1d1	charakteristická
převážně	převážně	k6eAd1	převážně
instrumentální	instrumentální	k2eAgFnPc1d1	instrumentální
skladby	skladba	k1gFnPc1	skladba
<g/>
,	,	kIx,	,
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
výrazné	výrazný	k2eAgFnSc6d1	výrazná
basové	basový	k2eAgFnSc6d1	basová
lince	linka	k1gFnSc6	linka
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
bicími	bicí	k2eAgMnPc7d1	bicí
–	–	k?	–
od	od	k7c2	od
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
odvíjí	odvíjet	k5eAaImIp3nS	odvíjet
i	i	k9	i
původ	původ	k1gInSc1	původ
jména	jméno	k1gNnSc2	jméno
stylu	styl	k1gInSc2	styl
–	–	k?	–
anglicky	anglicky	k6eAd1	anglicky
drum	drum	k6eAd1	drum
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
bass	bass	k1gInSc4	bass
=	=	kIx~	=
buben	buben	k1gInSc4	buben
a	a	k8xC	a
basy	bas	k1gInPc4	bas
(	(	kIx(	(
<g/>
basová	basový	k2eAgFnSc1d1	basová
linka	linka	k1gFnSc1	linka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
linky	linka	k1gFnPc1	linka
bývají	bývat	k5eAaImIp3nP	bývat
někdy	někdy	k6eAd1	někdy
doplněny	doplnit	k5eAaPmNgFnP	doplnit
o	o	k7c4	o
další	další	k2eAgFnPc4d1	další
vokální	vokální	k2eAgFnPc4d1	vokální
či	či	k8xC	či
instrumentální	instrumentální	k2eAgFnPc1d1	instrumentální
samply	sampnout	k5eAaPmAgFnP	sampnout
<g/>
.	.	kIx.	.
</s>
<s>
Drum	Drum	k1gInSc1	Drum
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
bass	bass	k1gInSc1	bass
je	být	k5eAaImIp3nS	být
oblíben	oblíbit	k5eAaPmNgInS	oblíbit
mezi	mezi	k7c7	mezi
diskžokeji	diskžokej	k1gMnPc7	diskžokej
<g/>
,	,	kIx,	,
pořádají	pořádat	k5eAaImIp3nP	pořádat
se	se	k3xPyFc4	se
taneční	taneční	k2eAgInPc1d1	taneční
party	part	k1gInPc1	part
zaměřené	zaměřený	k2eAgInPc1d1	zaměřený
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
styl	styl	k1gInSc4	styl
hudby	hudba	k1gFnSc2	hudba
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Let	let	k1gInSc1	let
It	It	k1gFnSc1	It
Roll	Roll	k1gMnSc1	Roll
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
raných	raný	k2eAgNnPc6d1	rané
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
začali	začít	k5eAaPmAgMnP	začít
rastafariáni	rastafarián	k1gMnPc1	rastafarián
na	na	k7c6	na
Jamajce	Jamajka	k1gFnSc6	Jamajka
tvořit	tvořit	k5eAaImF	tvořit
hudbu	hudba	k1gFnSc4	hudba
s	s	k7c7	s
prvky	prvek	k1gInPc7	prvek
z	z	k7c2	z
karibské	karibský	k2eAgFnSc2d1	karibská
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
soulu	soul	k1gInSc2	soul
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
nazvali	nazvat	k5eAaBmAgMnP	nazvat
ska	ska	k?	ska
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
tempo	tempo	k1gNnSc1	tempo
ska	ska	k?	ska
zpomaleno	zpomalen	k2eAgNnSc4d1	zpomaleno
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
vzniknul	vzniknout	k5eAaPmAgInS	vzniknout
nový	nový	k2eAgInSc4d1	nový
styl	styl	k1gInSc4	styl
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
reggae	reggae	k1gInSc1	reggae
<g/>
.	.	kIx.	.
</s>
<s>
Umělci	umělec	k1gMnPc1	umělec
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
hudby	hudba	k1gFnSc2	hudba
vkládali	vkládat	k5eAaImAgMnP	vkládat
své	svůj	k3xOyFgNnSc4	svůj
náboženství	náboženství	k1gNnSc4	náboženství
<g/>
,	,	kIx,	,
globální	globální	k2eAgInPc4d1	globální
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
lásku	láska	k1gFnSc4	láska
a	a	k8xC	a
ještě	ještě	k9	ještě
mnoho	mnoho	k4c4	mnoho
dalšího	další	k2eAgNnSc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
z	z	k7c2	z
reggae	regga	k1gInSc2	regga
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
nový	nový	k2eAgInSc1d1	nový
hudební	hudební	k2eAgInSc1d1	hudební
styl	styl	k1gInSc1	styl
nazývaný	nazývaný	k2eAgMnSc1d1	nazývaný
ragga	ragg	k1gMnSc4	ragg
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
více	hodně	k6eAd2	hodně
elektronický	elektronický	k2eAgInSc1d1	elektronický
a	a	k8xC	a
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
více	hodně	k6eAd2	hodně
skatových	skatův	k2eAgInPc2d1	skatův
vokálů	vokál	k1gInPc2	vokál
<g/>
.	.	kIx.	.
</s>
<s>
Ragga	Ragga	k1gFnSc1	Ragga
a	a	k8xC	a
reggae	reggae	k1gFnSc1	reggae
byly	být	k5eAaImAgFnP	být
prvními	první	k4xOgMnPc7	první
a	a	k8xC	a
hlavními	hlavní	k2eAgInPc7d1	hlavní
styly	styl	k1gInPc7	styl
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
daly	dát	k5eAaPmAgInP	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
drum	drum	k6eAd1	drum
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
bassu	bass	k1gInSc3	bass
<g/>
.	.	kIx.	.
</s>
<s>
Silně	silně	k6eAd1	silně
zapuštěné	zapuštěný	k2eAgInPc4d1	zapuštěný
kořeny	kořen	k1gInPc4	kořen
černé	černý	k2eAgFnSc2d1	černá
kultury	kultura	k1gFnSc2	kultura
z	z	k7c2	z
Jamajky	Jamajka	k1gFnSc2	Jamajka
se	se	k3xPyFc4	se
v	v	k7c6	v
dnb	dnb	k?	dnb
promítají	promítat	k5eAaImIp3nP	promítat
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
jeho	jeho	k3xOp3gFnSc2	jeho
existence	existence	k1gFnSc2	existence
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
raných	raný	k2eAgNnPc6d1	rané
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
hip-hop	hipop	k1gInSc1	hip-hop
znám	znám	k2eAgInSc1d1	znám
především	především	k9	především
ve	v	k7c6	v
velkých	velký	k2eAgNnPc6d1	velké
městech	město	k1gNnPc6	město
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Tancování	tancování	k1gNnSc1	tancování
na	na	k7c4	na
rapovou	rapový	k2eAgFnSc4d1	rapová
hudbu	hudba	k1gFnSc4	hudba
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
kulturou	kultura	k1gFnSc7	kultura
samo	sám	k3xTgNnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Někoho	někdo	k3yInSc4	někdo
tehdy	tehdy	k6eAd1	tehdy
napadlo	napadnout	k5eAaPmAgNnS	napadnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
bojovat	bojovat	k5eAaImF	bojovat
tancem	tanec	k1gInSc7	tanec
<g/>
,	,	kIx,	,
založeným	založený	k2eAgMnSc7d1	založený
na	na	k7c6	na
"	"	kIx"	"
<g/>
breakdownech	breakdown	k1gInPc6	breakdown
<g/>
"	"	kIx"	"
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
tanec	tanec	k1gInSc1	tanec
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
známým	známý	k2eAgMnSc7d1	známý
jako	jako	k8xS	jako
breakdance	breakdanec	k1gMnPc4	breakdanec
a	a	k8xC	a
vysunul	vysunout	k5eAaPmAgMnS	vysunout
do	do	k7c2	do
popředí	popředí	k1gNnSc2	popředí
právě	právě	k9	právě
ty	ten	k3xDgInPc1	ten
údery	úder	k1gInPc1	úder
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
"	"	kIx"	"
<g/>
breakdowny	breakdowen	k2eAgFnPc1d1	breakdowen
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
nový	nový	k2eAgInSc1d1	nový
hudební	hudební	k2eAgInSc1d1	hudební
styl	styl	k1gInSc1	styl
nazvaný	nazvaný	k2eAgInSc4d1	nazvaný
breakbeat	breakbeat	k1gInSc4	breakbeat
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
třetím	třetí	k4xOgMnSc6	třetí
a	a	k8xC	a
nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
prvkem	prvek	k1gInSc7	prvek
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
drum	drum	k6eAd1	drum
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
bassu	bass	k1gInSc3	bass
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
dali	dát	k5eAaPmAgMnP	dát
lidé	člověk	k1gMnPc1	člověk
okolo	okolo	k7c2	okolo
Warehouse	Warehouse	k1gFnSc2	Warehouse
klubu	klub	k1gInSc2	klub
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
vzniknout	vzniknout	k5eAaPmF	vzniknout
stylu	styl	k1gInSc3	styl
<g/>
,	,	kIx,	,
vycházejícímu	vycházející	k2eAgInSc3d1	vycházející
z	z	k7c2	z
disco	disco	k1gNnSc2	disco
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
housu	housa	k1gFnSc4	housa
<g/>
.	.	kIx.	.
</s>
<s>
House	house	k1gNnSc1	house
samotný	samotný	k2eAgMnSc1d1	samotný
nebyl	být	k5eNaImAgInS	být
pro	pro	k7c4	pro
zrod	zrod	k1gInSc4	zrod
drum	drum	k6eAd1	drum
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
bassu	bass	k1gInSc6	bass
příliš	příliš	k6eAd1	příliš
důležitý	důležitý	k2eAgInSc1d1	důležitý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dal	dát	k5eAaPmAgMnS	dát
vzniknou	vzniknout	k5eAaPmIp3nP	vzniknout
stylu	styl	k1gInSc3	styl
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
rozhodně	rozhodně	k6eAd1	rozhodně
byl	být	k5eAaImAgInS	být
–	–	k?	–
ravu	rava	k1gFnSc4	rava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
začali	začít	k5eAaPmAgMnP	začít
producenti	producent	k1gMnPc1	producent
housu	housa	k1gFnSc4	housa
experimentovat	experimentovat	k5eAaImF	experimentovat
s	s	k7c7	s
basovým	basový	k2eAgInSc7d1	basový
syntetizérem	syntetizér	k1gInSc7	syntetizér
<g/>
,	,	kIx,	,
známým	známý	k2eAgInSc7d1	známý
Rolandem	Roland	k1gInSc7	Roland
TB	TB	kA	TB
303	[number]	k4	303
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
mašinka	mašinka	k1gFnSc1	mašinka
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Rolandem	Rolando	k1gNnSc7	Rolando
TR-808	TR-808	k1gMnPc2	TR-808
a	a	k8xC	a
909	[number]	k4	909
stala	stát	k5eAaPmAgFnS	stát
páteří	páteř	k1gFnSc7	páteř
acid	acida	k1gFnPc2	acida
housu	hous	k1gInSc2	hous
<g/>
.	.	kIx.	.
</s>
<s>
Acid	Acid	k1gInSc1	Acid
house	house	k1gNnSc1	house
si	se	k3xPyFc3	se
svoje	svůj	k3xOyFgNnSc4	svůj
pojmenování	pojmenování	k1gNnSc4	pojmenování
vysloužil	vysloužit	k5eAaPmAgMnS	vysloužit
svým	svůj	k3xOyFgMnPc3	svůj
"	"	kIx"	"
<g/>
kyselým	kyselý	k2eAgInSc7d1	kyselý
<g/>
"	"	kIx"	"
zvukem	zvuk	k1gInSc7	zvuk
a	a	k8xC	a
také	také	k9	také
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
party	party	k1gFnSc4	party
byste	by	kYmCp2nP	by
těžko	těžko	k6eAd1	těžko
hledali	hledat	k5eAaImAgMnP	hledat
někoho	někdo	k3yInSc4	někdo
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
by	by	kYmCp3nS	by
v	v	k7c6	v
sobě	se	k3xPyFc3	se
neměl	mít	k5eNaImAgInS	mít
kyselinu	kyselina	k1gFnSc4	kyselina
LSD	LSD	kA	LSD
nebo	nebo	k8xC	nebo
MDMA	MDMA	kA	MDMA
<g/>
.	.	kIx.	.
</s>
<s>
Acid	Acid	k6eAd1	Acid
houseové	houseové	k2eAgFnPc1d1	houseové
party	parta	k1gFnPc1	parta
se	se	k3xPyFc4	se
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
za	za	k7c4	za
oceán	oceán	k1gInSc4	oceán
do	do	k7c2	do
staré	starý	k2eAgFnSc2d1	stará
dobré	dobrý	k2eAgFnSc2d1	dobrá
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
do	do	k7c2	do
klubů	klub	k1gInPc2	klub
jako	jako	k8xS	jako
např.	např.	kA	např.
Shoom	Shoom	k1gInSc1	Shoom
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začínala	začínat	k5eAaImAgFnS	začínat
spousta	spousta	k1gFnSc1	spousta
dnes	dnes	k6eAd1	dnes
dobře	dobře	k6eAd1	dobře
známých	známý	k2eAgFnPc2d1	známá
housových	housová	k1gFnPc2	housová
DJ	DJ	kA	DJ
<g/>
.	.	kIx.	.
</s>
<s>
Detaily	detail	k1gInPc1	detail
o	o	k7c6	o
místě	místo	k1gNnSc6	místo
konání	konání	k1gNnSc2	konání
se	se	k3xPyFc4	se
přenášely	přenášet	k5eAaImAgInP	přenášet
pouze	pouze	k6eAd1	pouze
ústně	ústně	k6eAd1	ústně
mezi	mezi	k7c7	mezi
clubbery	clubber	k1gMnPc7	clubber
<g/>
.	.	kIx.	.
</s>
<s>
Fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
o	o	k7c6	o
těchto	tento	k3xDgInPc6	tento
parties	parties	k1gInSc1	parties
věděli	vědět	k5eAaImAgMnP	vědět
pouze	pouze	k6eAd1	pouze
vybraní	vybraný	k2eAgMnPc1d1	vybraný
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
je	on	k3xPp3gMnPc4	on
činil	činit	k5eAaImAgInS	činit
velmi	velmi	k6eAd1	velmi
přitažlivými	přitažlivý	k2eAgInPc7d1	přitažlivý
<g/>
.	.	kIx.	.
</s>
<s>
Obrovské	obrovský	k2eAgNnSc1d1	obrovské
ilegální	ilegální	k2eAgNnSc1d1	ilegální
rave	rave	k1gNnSc1	rave
party	parta	k1gFnSc2	parta
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
lavinovitě	lavinovitě	k6eAd1	lavinovitě
šířit	šířit	k5eAaImF	šířit
a	a	k8xC	a
když	když	k8xS	když
se	se	k3xPyFc4	se
američtí	americký	k2eAgMnPc1d1	americký
producenti	producent	k1gMnPc1	producent
přeorientovali	přeorientovat	k5eAaPmAgMnP	přeorientovat
na	na	k7c4	na
dalšího	další	k2eAgMnSc4d1	další
potomka	potomek	k1gMnSc4	potomek
housu	hous	k1gInSc2	hous
–	–	k?	–
techno	techno	k1gNnSc1	techno
(	(	kIx(	(
<g/>
např.	např.	kA	např.
track	track	k1gInSc1	track
od	od	k7c2	od
Joeyho	Joey	k1gMnSc2	Joey
Beltrama	Beltram	k1gMnSc2	Beltram
"	"	kIx"	"
<g/>
Energy	Energ	k1gInPc1	Energ
flash	flasha	k1gFnPc2	flasha
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
se	se	k3xPyFc4	se
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
postupně	postupně	k6eAd1	postupně
měnit	měnit	k5eAaImF	měnit
acidový	acidový	k2eAgInSc4d1	acidový
zvuk	zvuk	k1gInSc4	zvuk
na	na	k7c4	na
hardcore	hardcor	k1gMnSc5	hardcor
rave	ravus	k1gMnSc5	ravus
<g/>
.	.	kIx.	.
</s>
<s>
Ostrovní	ostrovní	k2eAgMnPc1d1	ostrovní
producenti	producent	k1gMnPc1	producent
do	do	k7c2	do
svých	svůj	k3xOyFgInPc2	svůj
tracků	track	k1gInPc2	track
vkládali	vkládat	k5eAaImAgMnP	vkládat
spoustu	spousta	k1gFnSc4	spousta
breakbeatů	breakbeat	k1gInPc2	breakbeat
<g/>
,	,	kIx,	,
vysamplovaných	vysamplovaný	k2eAgInPc2d1	vysamplovaný
z	z	k7c2	z
hip-hopových	hipopový	k2eAgFnPc2d1	hip-hopová
nahrávek	nahrávka	k1gFnPc2	nahrávka
z	z	k7c2	z
USA	USA	kA	USA
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
jako	jako	k9	jako
sub	sub	k7c4	sub
basy	basa	k1gFnPc4	basa
<g/>
,	,	kIx,	,
piána	piána	k?	piána
s	s	k7c7	s
rostoucím	rostoucí	k2eAgInSc7d1	rostoucí
zvukem	zvuk	k1gInSc7	zvuk
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
nahrávkách	nahrávka	k1gFnPc6	nahrávka
se	se	k3xPyFc4	se
objevoval	objevovat	k5eAaImAgInS	objevovat
tvrdý	tvrdý	k2eAgMnSc1d1	tvrdý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
happy	happ	k1gInPc4	happ
znějící	znějící	k2eAgInSc4d1	znějící
vibe	vibe	k1gInSc4	vibe
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
stimulovat	stimulovat	k5eAaImF	stimulovat
účinky	účinek	k1gInPc4	účinek
obrovského	obrovský	k2eAgNnSc2d1	obrovské
množství	množství	k1gNnSc2	množství
extáze	extáze	k1gFnSc2	extáze
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
samozřejmou	samozřejmý	k2eAgFnSc4d1	samozřejmá
součásti	součást	k1gFnPc4	součást
každé	každý	k3xTgFnSc2	každý
této	tento	k3xDgFnSc2	tento
party	parta	k1gFnSc2	parta
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
byli	být	k5eAaImAgMnP	být
producenti	producent	k1gMnPc1	producent
jako	jako	k8xC	jako
The	The	k1gMnPc1	The
Prodigy	Prodiga	k1gFnSc2	Prodiga
<g/>
,	,	kIx,	,
Mickey	Mickea	k1gFnPc1	Mickea
Finn	Finn	k1gMnSc1	Finn
&	&	k?	&
Aphrodite	Aphrodit	k1gInSc5	Aphrodit
(	(	kIx(	(
<g/>
zakladatelé	zakladatel	k1gMnPc1	zakladatel
labelu	label	k1gInSc2	label
Urban	Urban	k1gMnSc1	Urban
Shakedown	Shakedown	k1gMnSc1	Shakedown
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
SL2	SL2	k1gFnSc1	SL2
(	(	kIx(	(
<g/>
Slipmatt	Slipmatt	k1gInSc1	Slipmatt
&	&	k?	&
Lime	Lime	k1gInSc1	Lime
<g/>
)	)	kIx)	)
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
žebříčku	žebříček	k1gInSc2	žebříček
"	"	kIx"	"
<g/>
Top	topit	k5eAaImRp2nS	topit
of	of	k?	of
the	the	k?	the
Pops	Pops	k1gInSc1	Pops
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgInPc1d1	státní
orgány	orgán	k1gInPc1	orgán
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
zajímat	zajímat	k5eAaImF	zajímat
o	o	k7c4	o
ilegální	ilegální	k2eAgFnPc4d1	ilegální
ravové	ravové	k2eAgFnPc4d1	ravové
party	parta	k1gFnPc4	parta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
viděly	vidět	k5eAaImAgFnP	vidět
užívat	užívat	k5eAaImF	užívat
účastníky	účastník	k1gMnPc4	účastník
rozličné	rozličný	k2eAgFnSc2d1	rozličná
drogy	droga	k1gFnSc2	droga
a	a	k8xC	a
začaly	začít	k5eAaPmAgFnP	začít
šílet	šílet	k5eAaImF	šílet
z	z	k7c2	z
tvrdých	tvrdý	k2eAgInPc2d1	tvrdý
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
úderů	úder	k1gInPc2	úder
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
The	The	k1gMnPc1	The
Prodigy	Prodiga	k1gFnSc2	Prodiga
známými	známý	k1gMnPc7	známý
a	a	k8xC	a
populárními	populární	k2eAgInPc7d1	populární
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
konat	konat	k5eAaImF	konat
zátahy	zátah	k1gInPc4	zátah
na	na	k7c4	na
velké	velký	k2eAgMnPc4d1	velký
ilegální	ilegální	k2eAgMnPc4d1	ilegální
free	fre	k1gMnPc4	fre
party	parta	k1gFnSc2	parta
a	a	k8xC	a
rave	ravat	k5eAaPmIp3nS	ravat
byl	být	k5eAaImAgMnS	být
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
pouze	pouze	k6eAd1	pouze
komerční	komerční	k2eAgInSc1d1	komerční
hudbou	hudba	k1gFnSc7	hudba
<g/>
,	,	kIx,	,
zatlačující	zatlačující	k2eAgFnSc7d1	zatlačující
pravou	pravá	k1gFnSc7	pravá
raveovou	raveův	k2eAgFnSc7d1	raveův
scénu	scéna	k1gFnSc4	scéna
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
undergroundu	underground	k1gInSc2	underground
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
touto	tento	k3xDgFnSc7	tento
dobou	doba	k1gFnSc7	doba
začaly	začít	k5eAaPmAgInP	začít
"	"	kIx"	"
<g/>
černé	černý	k2eAgInPc1d1	černý
<g/>
"	"	kIx"	"
projekty	projekt	k1gInPc1	projekt
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
samplovat	samplovat	k5eAaImF	samplovat
ragga	ragga	k1gFnSc1	ragga
a	a	k8xC	a
začaly	začít	k5eAaPmAgFnP	začít
ho	on	k3xPp3gInSc4	on
mixovat	mixovat	k5eAaImF	mixovat
s	s	k7c7	s
ravem	rav	k1gMnSc7	rav
<g/>
,	,	kIx,	,
breakbeatem	breakbeat	k1gMnSc7	breakbeat
a	a	k8xC	a
hip-hopem	hipop	k1gMnSc7	hip-hop
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
byly	být	k5eAaImAgInP	být
rychlé	rychlý	k2eAgInPc1d1	rychlý
zlomené	zlomený	k2eAgInPc1d1	zlomený
beaty	beat	k1gInPc1	beat
<g/>
,	,	kIx,	,
hluboký	hluboký	k2eAgInSc1d1	hluboký
<g/>
,	,	kIx,	,
pomalý	pomalý	k2eAgInSc1d1	pomalý
bas	bas	k1gInSc1	bas
a	a	k8xC	a
ostatními	ostatní	k2eAgInPc7d1	ostatní
prvky	prvek	k1gInPc7	prvek
byly	být	k5eAaImAgFnP	být
ragga	ragga	k1gFnSc1	ragga
samply	sampnout	k5eAaPmAgInP	sampnout
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
vynalezen	vynalezen	k2eAgInSc4d1	vynalezen
jungle	jungle	k1gInSc4	jungle
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgInSc4d1	poslední
<g/>
,	,	kIx,	,
nejvýznamnější	významný	k2eAgInSc4d3	nejvýznamnější
krok	krok	k1gInSc4	krok
k	k	k7c3	k
drum	drum	k1gFnPc3	drum
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
bassu	bass	k1gInSc3	bass
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
undergroundový	undergroundový	k2eAgInSc1d1	undergroundový
zvuk	zvuk	k1gInSc1	zvuk
byl	být	k5eAaImAgInS	být
tvrdší	tvrdý	k2eAgInSc1d2	tvrdší
<g/>
,	,	kIx,	,
temnější	temný	k2eAgInSc1d2	temnější
<g/>
,	,	kIx,	,
rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
a	a	k8xC	a
více	hodně	k6eAd2	hodně
podobný	podobný	k2eAgInSc4d1	podobný
breakbeatu	breakbeat	k1gInSc3	breakbeat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
se	se	k3xPyFc4	se
jungle	jungle	k6eAd1	jungle
silně	silně	k6eAd1	silně
projevil	projevit	k5eAaPmAgInS	projevit
<g/>
.	.	kIx.	.
</s>
<s>
Producenti	producent	k1gMnPc1	producent
jako	jako	k9	jako
Q-Project	Q-Project	k1gMnSc1	Q-Project
<g/>
,	,	kIx,	,
Deep	Deep	k1gMnSc1	Deep
Blue	Blu	k1gFnSc2	Blu
a	a	k8xC	a
Foul	Foula	k1gFnPc2	Foula
Play	play	k0	play
komponovali	komponovat	k5eAaImAgMnP	komponovat
junglové	junglový	k2eAgFnPc4d1	junglový
melodie	melodie	k1gFnPc4	melodie
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
lidé	člověk	k1gMnPc1	člověk
jako	jako	k8xC	jako
Slipmatt	Slipmatt	k1gInSc1	Slipmatt
<g/>
,	,	kIx,	,
Vibex	Vibex	k1gInSc1	Vibex
a	a	k8xC	a
Dougal	Dougal	k1gInSc1	Dougal
se	se	k3xPyFc4	se
vydali	vydat	k5eAaPmAgMnP	vydat
jiným	jiný	k2eAgInSc7d1	jiný
směrem	směr	k1gInSc7	směr
využívajícím	využívající	k2eAgInSc7d1	využívající
breakbeatu	breakbeata	k1gFnSc4	breakbeata
do	do	k7c2	do
happy	happa	k1gFnSc2	happa
hardcoru	hardcor	k1gInSc2	hardcor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
přetransformoval	přetransformovat	k5eAaPmAgInS	přetransformovat
do	do	k7c2	do
stylu	styl	k1gInSc2	styl
nazvaného	nazvaný	k2eAgInSc2d1	nazvaný
"	"	kIx"	"
<g/>
4	[number]	k4	4
–	–	k?	–
t	t	k?	t
o	o	k7c4	o
-	-	kIx~	-
the	the	k?	the
floor	floor	k1gInSc1	floor
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
jungle	jungle	k1gInSc1	jungle
stal	stát	k5eAaPmAgInS	stát
tvrdším	tvrdý	k2eAgInSc7d2	tvrdší
minimalistickým	minimalistický	k2eAgInSc7d1	minimalistický
stylem	styl	k1gInSc7	styl
s	s	k7c7	s
více	hodně	k6eAd2	hodně
komplexními	komplexní	k2eAgInPc7d1	komplexní
breaky	break	k1gInPc7	break
<g/>
.	.	kIx.	.
</s>
<s>
Jungle	Jungle	k6eAd1	Jungle
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
přímým	přímý	k2eAgMnSc7d1	přímý
předchůdcem	předchůdce	k1gMnSc7	předchůdce
drum	drum	k1gMnSc1	drum
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
bassu	bass	k1gInSc2	bass
a	a	k8xC	a
znamená	znamenat	k5eAaImIp3nS	znamenat
poslední	poslední	k2eAgInSc1d1	poslední
krůček	krůček	k1gInSc1	krůček
od	od	k7c2	od
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
jungle	jungle	k1gInSc1	jungle
<g/>
"	"	kIx"	"
vymyslel	vymyslet	k5eAaPmAgInS	vymyslet
anglický	anglický	k2eAgInSc4d1	anglický
MC	MC	kA	MC
Rebel	rebel	k1gMnSc1	rebel
(	(	kIx(	(
<g/>
mimochodem	mimochodem	k9	mimochodem
v	v	k7c6	v
Guinessově	Guinessův	k2eAgFnSc6d1	Guinessova
knize	kniha	k1gFnSc6	kniha
rekordů	rekord	k1gInPc2	rekord
byl	být	k5eAaImAgMnS	být
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
uveden	uvést	k5eAaPmNgInS	uvést
jako	jako	k8xS	jako
nejrychlejší	rychlý	k2eAgFnSc1d3	nejrychlejší
MC	MC	kA	MC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
tvořil	tvořit	k5eAaImAgInS	tvořit
reggae	reggae	k6eAd1	reggae
dub	dub	k1gInSc1	dub
s	s	k7c7	s
prvky	prvek	k1gInPc7	prvek
breakbeatu	breakbeat	k1gInSc2	breakbeat
a	a	k8xC	a
rave	rav	k1gFnSc2	rav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
tracků	track	k1gInPc2	track
můžeme	moct	k5eAaImIp1nP	moct
slyšet	slyšet	k5eAaImF	slyšet
vokál	vokál	k1gInSc4	vokál
"	"	kIx"	"
<g/>
Big	Big	k1gFnSc1	Big
up	up	k?	up
all	all	k?	all
of	of	k?	of
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
junglists	junglists	k1gInSc1	junglists
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
vysamplován	vysamplovat	k5eAaPmNgInS	vysamplovat
ze	z	k7c2	z
starého	starý	k2eAgInSc2d1	starý
Jamajského	jamajský	k2eAgInSc2d1	jamajský
dancehallového	dancehallový	k2eAgInSc2d1	dancehallový
tracku	track	k1gInSc2	track
<g/>
.	.	kIx.	.
</s>
<s>
MC	MC	kA	MC
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
na	na	k7c4	na
Jamajce	Jamajka	k1gFnSc3	Jamajka
svými	svůj	k3xOyFgInPc7	svůj
texty	text	k1gInPc7	text
opěvují	opěvovat	k5eAaImIp3nP	opěvovat
všechny	všechen	k3xTgFnPc4	všechen
různé	různý	k2eAgFnPc4d1	různá
části	část	k1gFnPc4	část
Kingstonu	Kingston	k1gInSc2	Kingston
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
jako	jako	k8xS	jako
dnes	dnes	k6eAd1	dnes
MC	MC	kA	MC
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
opěvují	opěvovat	k5eAaImIp3nP	opěvovat
různé	různý	k2eAgInPc1d1	různý
části	část	k1gFnPc4	část
měst	město	k1gNnPc2	město
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
částí	část	k1gFnPc2	část
Kingstonu	Kingston	k1gInSc2	Kingston
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
pokryta	pokryt	k2eAgFnSc1d1	pokryta
stromy	strom	k1gInPc7	strom
a	a	k8xC	a
různou	různý	k2eAgFnSc7d1	různá
vegetací	vegetace	k1gFnSc7	vegetace
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Jungle	Jungle	k1gFnSc2	Jungle
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dancehallovém	dancehallový	k2eAgInSc6d1	dancehallový
tracku	track	k1gInSc6	track
MC	MC	kA	MC
pobízí	pobízet	k5eAaImIp3nS	pobízet
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
pocházejí	pocházet	k5eAaImIp3nP	pocházet
právě	právě	k9	právě
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
části	část	k1gFnSc2	část
<g/>
...	...	k?	...
proto	proto	k8xC	proto
"	"	kIx"	"
<g/>
big	big	k?	big
up	up	k?	up
all	all	k?	all
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
junglists	junglists	k1gInSc1	junglists
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
jungle	jungle	k6eAd1	jungle
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
reggae	reggae	k1gFnSc7	reggae
a	a	k8xC	a
neznamená	znamenat	k5eNaImIp3nS	znamenat
"	"	kIx"	"
<g/>
urban	urban	k1gInSc1	urban
(	(	kIx(	(
<g/>
městská	městský	k2eAgFnSc1d1	městská
<g/>
)	)	kIx)	)
jungle	jungle	k1gFnSc1	jungle
<g/>
"	"	kIx"	"
–	–	k?	–
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
velkými	velký	k2eAgNnPc7d1	velké
městy	město	k1gNnPc7	město
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
si	se	k3xPyFc3	se
mnozí	mnohý	k2eAgMnPc1d1	mnohý
lidé	člověk	k1gMnPc1	člověk
myslí	myslet	k5eAaImIp3nP	myslet
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
junglové	junglový	k2eAgFnPc1d1	junglový
nahrávky	nahrávka	k1gFnPc1	nahrávka
pocházející	pocházející	k2eAgFnSc1d1	pocházející
od	od	k7c2	od
MC	MC	kA	MC
Rebela	rebel	k1gMnSc2	rebel
obsahovaly	obsahovat	k5eAaImAgFnP	obsahovat
ragga	ragg	k1gMnSc2	ragg
samply	sampnout	k5eAaPmAgInP	sampnout
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
zlomené	zlomený	k2eAgInPc4d1	zlomený
beaty	beat	k1gInPc4	beat
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
amenové	amenový	k2eAgInPc4d1	amenový
breaky	break	k1gInPc4	break
<g/>
,	,	kIx,	,
helicopter	helicopter	k1gInSc4	helicopter
beaty	beat	k1gInPc4	beat
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Basy	basa	k1gFnPc1	basa
byly	být	k5eAaImAgFnP	být
jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
<g/>
,	,	kIx,	,
s	s	k7c7	s
poutavou	poutavý	k2eAgFnSc7d1	poutavá
melodií	melodie	k1gFnSc7	melodie
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
vše	všechen	k3xTgNnSc1	všechen
<g/>
:	:	kIx,	:
ragga	ragg	k1gMnSc2	ragg
samply	sampnout	k5eAaPmAgFnP	sampnout
<g/>
,	,	kIx,	,
breaky	break	k1gInPc7	break
a	a	k8xC	a
basy	bas	k1gInPc7	bas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
hit	hit	k1gInSc1	hit
a	a	k8xC	a
nová	nový	k2eAgFnSc1d1	nová
naděje	naděje	k1gFnSc1	naděje
pro	pro	k7c4	pro
ravery	ravera	k1gFnPc4	ravera
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yIgInPc2	který
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
junglists	junglists	k6eAd1	junglists
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
junglettes	junglettes	k1gInSc4	junglettes
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
to	ten	k3xDgNnSc1	ten
byly	být	k5eAaImAgFnP	být
ženy	žena	k1gFnPc1	žena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jungle	Jungle	k1gInSc1	Jungle
ukazoval	ukazovat	k5eAaImAgInS	ukazovat
na	na	k7c4	na
gangstery	gangster	k1gMnPc4	gangster
<g/>
,	,	kIx,	,
kriminalitu	kriminalita	k1gFnSc4	kriminalita
a	a	k8xC	a
nesnadný	snadný	k2eNgInSc4d1	nesnadný
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Nahrávky	nahrávka	k1gFnPc1	nahrávka
byly	být	k5eAaImAgFnP	být
vždy	vždy	k6eAd1	vždy
kompletně	kompletně	k6eAd1	kompletně
stvořené	stvořený	k2eAgNnSc1d1	stvořené
ze	z	k7c2	z
starých	starý	k2eAgInPc2d1	starý
samplů	sampl	k1gInPc2	sampl
z	z	k7c2	z
minulosti	minulost	k1gFnSc2	minulost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
změnilo	změnit	k5eAaPmAgNnS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
se	se	k3xPyFc4	se
scéna	scéna	k1gFnSc1	scéna
úplně	úplně	k6eAd1	úplně
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
tábory	tábor	k1gInPc4	tábor
<g/>
,	,	kIx,	,
když	když	k8xS	když
jump-up	jumpp	k1gInSc1	jump-up
jungle	jungle	k6eAd1	jungle
přinesl	přinést	k5eAaPmAgInS	přinést
své	své	k1gNnSc4	své
ragga	ragg	k1gMnSc2	ragg
vokály	vokál	k1gInPc4	vokál
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
infrazvukové	infrazvukový	k2eAgFnPc4d1	infrazvuková
basové	basový	k2eAgFnPc4d1	basová
linky	linka	k1gFnPc4	linka
a	a	k8xC	a
ihned	ihned	k6eAd1	ihned
rozpoznatelné	rozpoznatelný	k2eAgInPc4d1	rozpoznatelný
breaky	break	k1gInPc4	break
<g/>
.	.	kIx.	.
</s>
<s>
Zvuky	zvuk	k1gInPc1	zvuk
začaly	začít	k5eAaPmAgInP	začít
být	být	k5eAaImF	být
oposlouchané	oposlouchaný	k2eAgInPc1d1	oposlouchaný
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
drsnému	drsný	k2eAgInSc3d1	drsný
zvuku	zvuk	k1gInSc3	zvuk
a	a	k8xC	a
ovlivnění	ovlivnění	k1gNnSc4	ovlivnění
hip-hopem	hipop	k1gInSc7	hip-hop
<g/>
,	,	kIx,	,
gangsta	gangsta	k1gFnSc1	gangsta
rapem	rap	k1gMnSc7	rap
a	a	k8xC	a
ragga	ragg	k1gMnSc2	ragg
zněl	znět	k5eAaImAgInS	znět
jungle	jungle	k6eAd1	jungle
tak	tak	k6eAd1	tak
trochu	trochu	k6eAd1	trochu
násilně	násilně	k6eAd1	násilně
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
dost	dost	k6eAd1	dost
lidí	člověk	k1gMnPc2	člověk
odradilo	odradit	k5eAaPmAgNnS	odradit
<g/>
.	.	kIx.	.
</s>
<s>
Zvuk	zvuk	k1gInSc1	zvuk
se	se	k3xPyFc4	se
však	však	k9	však
změnil	změnit	k5eAaPmAgInS	změnit
a	a	k8xC	a
beaty	beat	k1gInPc1	beat
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
jednodušší	jednoduchý	k2eAgInPc1d2	jednodušší
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
tvrdší	tvrdý	k2eAgInSc1d2	tvrdší
a	a	k8xC	a
důraz	důraz	k1gInSc1	důraz
byl	být	k5eAaImAgInS	být
kladen	klást	k5eAaImNgInS	klást
na	na	k7c4	na
tvoření	tvoření	k1gNnSc4	tvoření
vlastních	vlastní	k2eAgInPc2d1	vlastní
samplů	sampl	k1gInPc2	sampl
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
rozjel	rozjet	k5eAaPmAgMnS	rozjet
Rob	robit	k5eAaImRp2nS	robit
Playford	Playford	k1gMnSc1	Playford
label	label	k1gMnSc1	label
s	s	k7c7	s
názvem	název	k1gInSc7	název
Moving	Moving	k1gInSc1	Moving
Shadow	Shadow	k1gFnPc2	Shadow
<g/>
.	.	kIx.	.
</s>
<s>
Vycházel	vycházet	k5eAaImAgMnS	vycházet
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
jungle	jungl	k1gInSc6	jungl
bez	bez	k7c2	bez
ragga	ragg	k1gMnSc2	ragg
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Zato	zato	k6eAd1	zato
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
používal	používat	k5eAaImAgInS	používat
prvky	prvek	k1gInPc4	prvek
ambientu	ambient	k1gInSc2	ambient
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
otevřelo	otevřít	k5eAaPmAgNnS	otevřít
nový	nový	k2eAgInSc4d1	nový
prostor	prostor	k1gInSc4	prostor
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
tak	tak	k6eAd1	tak
vlastně	vlastně	k9	vlastně
idea	idea	k1gFnSc1	idea
<g/>
,	,	kIx,	,
že	že	k8xS	že
jungle	jungle	k1gFnSc1	jungle
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
moderní	moderní	k2eAgInPc4d1	moderní
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
zrodil	zrodit	k5eAaPmAgInS	zrodit
intelligent	intelligent	k1gInSc1	intelligent
jungle	jungle	k1gFnSc2	jungle
<g/>
.	.	kIx.	.
</s>
<s>
Ragga	Ragg	k1gMnSc4	Ragg
jungle	jungle	k1gInSc1	jungle
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
a	a	k8xC	a
britská	britský	k2eAgFnSc1d1	britská
scéna	scéna	k1gFnSc1	scéna
začala	začít	k5eAaPmAgFnS	začít
hledat	hledat	k5eAaImF	hledat
nové	nový	k2eAgFnPc4d1	nová
ingredience	ingredience	k1gFnPc4	ingredience
pro	pro	k7c4	pro
nový	nový	k2eAgInSc4d1	nový
jungle	jungle	k1gInSc4	jungle
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
ragga	ragg	k1gMnSc2	ragg
<g/>
,	,	kIx,	,
s	s	k7c7	s
vlastnoručně	vlastnoručně	k6eAd1	vlastnoručně
vytvořenými	vytvořený	k2eAgInPc7d1	vytvořený
prvky	prvek	k1gInPc7	prvek
<g/>
,	,	kIx,	,
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
nový	nový	k2eAgInSc1d1	nový
hudební	hudební	k2eAgInSc1d1	hudební
styl	styl	k1gInSc1	styl
drum	druma	k1gFnPc2	druma
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
bass	bassa	k1gFnPc2	bassa
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
směr	směr	k1gInSc1	směr
stále	stále	k6eAd1	stále
používal	používat	k5eAaImAgInS	používat
rychlé	rychlý	k2eAgInPc4d1	rychlý
tvrdé	tvrdý	k2eAgInPc4d1	tvrdý
breaky	break	k1gInPc4	break
vycházející	vycházející	k2eAgInPc4d1	vycházející
z	z	k7c2	z
hip-hopu	hipop	k1gInSc2	hip-hop
a	a	k8xC	a
těžké	těžký	k2eAgInPc4d1	těžký
basy	bas	k1gInPc4	bas
vycházející	vycházející	k2eAgInPc4d1	vycházející
z	z	k7c2	z
reggae	regga	k1gFnSc2	regga
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tentokrát	tentokrát	k6eAd1	tentokrát
s	s	k7c7	s
větším	veliký	k2eAgNnSc7d2	veliký
množstvím	množství	k1gNnSc7	množství
synthových	synthův	k2eAgInPc2d1	synthův
zvuků	zvuk	k1gInPc2	zvuk
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
v	v	k7c6	v
ravu	ravus	k1gInSc6	ravus
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
začali	začít	k5eAaPmAgMnP	začít
producenti	producent	k1gMnPc1	producent
jako	jako	k8xC	jako
Roni	Ron	k1gMnPc1	Ron
Size	Size	k1gNnSc2	Size
a	a	k8xC	a
Adam	Adam	k1gMnSc1	Adam
F	F	kA	F
používat	používat	k5eAaImF	používat
nesmírně	smírně	k6eNd1	smírně
živý	živý	k2eAgInSc4d1	živý
zvuk	zvuk	k1gInSc4	zvuk
a	a	k8xC	a
chtěli	chtít	k5eAaImAgMnP	chtít
lidi	člověk	k1gMnPc4	člověk
naučit	naučit	k5eAaPmF	naučit
rozpoznávat	rozpoznávat	k5eAaImF	rozpoznávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jungle	jungle	k1gFnSc1	jungle
–	–	k?	–
nebo	nebo	k8xC	nebo
drum	drum	k1gMnSc1	drum
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
bass	bass	k1gInSc4	bass
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
byl	být	k5eAaImAgMnS	být
znám	znám	k2eAgMnSc1d1	znám
–	–	k?	–
je	být	k5eAaImIp3nS	být
legitimní	legitimní	k2eAgFnSc1d1	legitimní
umělecká	umělecký	k2eAgFnSc1d1	umělecká
forma	forma	k1gFnSc1	forma
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
zrovna	zrovna	k6eAd1	zrovna
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
ho	on	k3xPp3gInSc4	on
tvořit	tvořit	k5eAaImF	tvořit
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jednoduchého	jednoduchý	k2eAgMnSc2d1	jednoduchý
ragga	ragg	k1gMnSc2	ragg
junglu	jungl	k1gInSc2	jungl
let	léto	k1gNnPc2	léto
1994	[number]	k4	1994
<g/>
-	-	kIx~	-
<g/>
95	[number]	k4	95
<g/>
.	.	kIx.	.
</s>
<s>
Pohodový	pohodový	k2eAgInSc4d1	pohodový
<g/>
,	,	kIx,	,
uklidňující	uklidňující	k2eAgInSc4d1	uklidňující
styl	styl	k1gInSc4	styl
junglu	jungl	k1gInSc2	jungl
(	(	kIx(	(
<g/>
intelligent	intelligent	k1gInSc1	intelligent
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
hlavním	hlavní	k2eAgMnSc7d1	hlavní
zástupcem	zástupce	k1gMnSc7	zástupce
je	být	k5eAaImIp3nS	být
LTJ	LTJ	kA	LTJ
Bukem	buk	k1gInSc7	buk
<g/>
,	,	kIx,	,
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
producenti	producent	k1gMnPc1	producent
chtěli	chtít	k5eAaImAgMnP	chtít
vzbudit	vzbudit	k5eAaPmF	vzbudit
zájem	zájem	k1gInSc4	zájem
u	u	k7c2	u
nových	nový	k2eAgInPc2d1	nový
zástupů	zástup	k1gInPc2	zástup
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
změnit	změnit	k5eAaPmF	změnit
myšlení	myšlení	k1gNnSc4	myšlení
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
si	se	k3xPyFc3	se
mysleli	myslet	k5eAaImAgMnP	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
dnb	dnb	k?	dnb
scéna	scéna	k1gFnSc1	scéna
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
jako	jako	k9	jako
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
gang	gang	k1gInSc1	gang
<g/>
.	.	kIx.	.
</s>
<s>
Rob	roba	k1gFnPc2	roba
Playford	Playforda	k1gFnPc2	Playforda
a	a	k8xC	a
Moving	Moving	k1gInSc4	Moving
Shadow	Shadow	k1gMnPc1	Shadow
nebyli	být	k5eNaImAgMnP	být
jediní	jediný	k2eAgMnPc1d1	jediný
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
pozvedli	pozvednout	k5eAaPmAgMnP	pozvednout
drum	drum	k6eAd1	drum
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
bass	bass	k1gInSc4	bass
na	na	k7c4	na
vyšší	vysoký	k2eAgFnSc4d2	vyšší
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čerstvých	čerstvý	k2eAgNnPc6d1	čerstvé
devatenácti	devatenáct	k4xCc6	devatenáct
letech	léto	k1gNnPc6	léto
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
i	i	k9	i
Danny	Danen	k2eAgInPc4d1	Danen
Bukem	buk	k1gInSc7	buk
(	(	kIx(	(
<g/>
LTJ	LTJ	kA	LTJ
Bukem	buk	k1gInSc7	buk
<g/>
)	)	kIx)	)
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
labelem	label	k1gInSc7	label
Good	Good	k1gInSc1	Good
Looking	Looking	k1gInSc1	Looking
Records	Recordsa	k1gFnPc2	Recordsa
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
jako	jako	k8xS	jako
Dego	Dego	k6eAd1	Dego
and	and	k?	and
Marc	Marc	k1gInSc1	Marc
Mac	Mac	kA	Mac
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
Hero	Hero	k1gNnSc1	Hero
<g/>
)	)	kIx)	)
vydávající	vydávající	k2eAgMnSc1d1	vydávající
na	na	k7c4	na
Reinforced	Reinforced	k1gInSc4	Reinforced
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
.	.	kIx.	.
</s>
<s>
Umělci	umělec	k1gMnPc1	umělec
jako	jako	k8xC	jako
Hype	Hyp	k1gFnPc1	Hyp
<g/>
,	,	kIx,	,
Grooverider	Grooverider	k1gInSc1	Grooverider
a	a	k8xC	a
Aphrodite	Aphrodit	k1gInSc5	Aphrodit
posunuli	posunout	k5eAaPmAgMnP	posunout
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
b	b	k?	b
ještě	ještě	k6eAd1	ještě
dále	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
čas	čas	k1gInSc1	čas
přišel	přijít	k5eAaPmAgInS	přijít
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
s	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
Goldieho	Goldie	k1gMnSc2	Goldie
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
dal	dát	k5eAaPmAgMnS	dát
dohromady	dohromady	k6eAd1	dohromady
s	s	k7c7	s
Robem	Robem	k?	Robem
Playfordem	Playford	k1gInSc7	Playford
a	a	k8xC	a
vydali	vydat	k5eAaPmAgMnP	vydat
album	album	k1gNnSc4	album
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
Timeless	Timeless	k1gInSc4	Timeless
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
velký	velký	k2eAgInSc1d1	velký
hit	hit	k1gInSc1	hit
–	–	k?	–
drum	drum	k1gInSc1	drum
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
bass	bass	k1gInSc1	bass
bylo	být	k5eAaImAgNnS	být
konečně	konečně	k6eAd1	konečně
slyšet	slyšet	k5eAaImF	slyšet
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
díky	díky	k7c3	díky
singlu	singl	k1gInSc3	singl
Inner	Innra	k1gFnPc2	Innra
City	City	k1gFnSc2	City
Life	Lif	k1gFnSc2	Lif
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Timeless	Timelessa	k1gFnPc2	Timelessa
bylo	být	k5eAaImAgNnS	být
první	první	k4xOgNnSc1	první
dnb	dnb	k?	dnb
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
oběhlo	oběhnout	k5eAaPmAgNnS	oběhnout
celý	celý	k2eAgInSc4d1	celý
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
labelů	label	k1gInPc2	label
jako	jako	k8xS	jako
Reinforced	Reinforced	k1gInSc1	Reinforced
<g/>
,	,	kIx,	,
Moving	Moving	k1gInSc1	Moving
Shadow	Shadow	k1gFnSc1	Shadow
<g/>
,	,	kIx,	,
Good	Good	k1gInSc1	Good
looking	looking	k1gInSc4	looking
<g/>
,	,	kIx,	,
Subrurban	Subrurban	k1gInSc4	Subrurban
base	basa	k1gFnSc3	basa
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
Goldieho	Goldie	k1gMnSc4	Goldie
labelu	label	k1gInSc2	label
Metalheadz	Metalheadz	k1gInSc1	Metalheadz
začal	začít	k5eAaPmAgInS	začít
prudce	prudko	k6eAd1	prudko
růst	růst	k1gInSc1	růst
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
malé	malý	k2eAgFnPc1d1	malá
undergroundové	undergroundový	k2eAgFnPc1d1	undergroundová
labely	labela	k1gFnPc1	labela
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
zdrojem	zdroj	k1gInSc7	zdroj
moderního	moderní	k2eAgMnSc2d1	moderní
drum	drum	k6eAd1	drum
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
bassu	bass	k1gInSc3	bass
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
vydal	vydat	k5eAaPmAgMnS	vydat
LTJ	LTJ	kA	LTJ
Bukem	buk	k1gInSc7	buk
album	album	k1gNnSc1	album
Logical	Logical	k1gFnSc2	Logical
Progression	Progression	k1gInSc1	Progression
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
intelligent	intelligent	k1gInSc4	intelligent
dnb	dnb	k?	dnb
album	album	k1gNnSc4	album
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
labely	labela	k1gFnSc2	labela
jako	jako	k8xC	jako
RAM	RAM	kA	RAM
records	records	k1gInSc1	records
a	a	k8xC	a
Formation	Formation	k1gInSc1	Formation
začaly	začít	k5eAaPmAgInP	začít
ukazovat	ukazovat	k5eAaImF	ukazovat
tvrdší	tvrdý	k2eAgFnSc4d2	tvrdší
stranu	strana	k1gFnSc4	strana
drum	druma	k1gFnPc2	druma
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
bassu	bassa	k1gFnSc4	bassa
s	s	k7c7	s
mohutnými	mohutný	k2eAgFnPc7d1	mohutná
basovými	basový	k2eAgFnPc7d1	basová
linkami	linka	k1gFnPc7	linka
a	a	k8xC	a
ocelově	ocelově	k6eAd1	ocelově
tvrdými	tvrdý	k2eAgInPc7d1	tvrdý
breaky	break	k1gInPc7	break
–	–	k?	–
hardstep	hardstep	k1gInSc1	hardstep
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
hudebník	hudebník	k1gMnSc1	hudebník
z	z	k7c2	z
Bristolu	Bristol	k1gInSc2	Bristol
jménem	jméno	k1gNnSc7	jméno
Roni	Ron	k1gFnSc2	Ron
Size	Siz	k1gInSc2	Siz
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
hrát	hrát	k5eAaImF	hrát
dnb	dnb	k?	dnb
naživo	naživo	k1gNnSc4	naživo
s	s	k7c7	s
profi	profi	k6eAd1	profi
muzikanty	muzikant	k1gMnPc7	muzikant
v	v	k7c6	v
mixu	mix	k1gInSc6	mix
dnb	dnb	k?	dnb
a	a	k8xC	a
jazzu	jazz	k1gInSc3	jazz
(	(	kIx(	(
<g/>
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
jazzjungle	jazzjungle	k1gInSc1	jazzjungle
<g/>
,	,	kIx,	,
jazzstep	jazzstep	k1gInSc1	jazzstep
<g/>
,	,	kIx,	,
jazz	jazz	k1gInSc1	jazz
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
bass	bass	k1gInSc1	bass
<g/>
,	,	kIx,	,
drum	drum	k1gInSc1	drum
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
jazz	jazz	k1gInSc4	jazz
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
Formace	formace	k1gFnSc1	formace
dostala	dostat	k5eAaPmAgFnS	dostat
název	název	k1gInSc4	název
Reprazent	Reprazent	k1gMnSc1	Reprazent
a	a	k8xC	a
když	když	k8xS	když
Size	Size	k1gInSc1	Size
vydal	vydat	k5eAaPmAgInS	vydat
album	album	k1gNnSc4	album
New	New	k1gMnSc2	New
Forms	Forms	k1gInSc4	Forms
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
hotová	hotový	k2eAgFnSc1d1	hotová
revoluce	revoluce	k1gFnSc1	revoluce
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
dnb	dnb	k?	dnb
<g/>
.	.	kIx.	.
</s>
<s>
Drum	Drum	k1gInSc1	Drum
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
bass	bass	k1gInSc1	bass
se	se	k3xPyFc4	se
stával	stávat	k5eAaImAgInS	stávat
stále	stále	k6eAd1	stále
populárnějším	populární	k2eAgNnSc7d2	populárnější
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
začal	začít	k5eAaPmAgInS	začít
také	také	k9	také
podstyl	podstyl	k1gInSc1	podstyl
dnb	dnb	k?	dnb
s	s	k7c7	s
názvem	název	k1gInSc7	název
techstep	techstep	k1gMnSc1	techstep
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
více	hodně	k6eAd2	hodně
inteligentní	inteligentní	k2eAgMnSc1d1	inteligentní
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
tvrdou	tvrdý	k2eAgFnSc4d1	tvrdá
<g/>
,	,	kIx,	,
temnou	temný	k2eAgFnSc4d1	temná
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
taneční	taneční	k2eAgInSc1d1	taneční
odnoží	odnož	k1gFnSc7	odnož
dnb	dnb	k?	dnb
<g/>
.	.	kIx.	.
</s>
<s>
Andy	Anda	k1gFnPc1	Anda
C	C	kA	C
<g/>
,	,	kIx,	,
Aphrodite	Aphrodit	k1gMnSc5	Aphrodit
<g/>
,	,	kIx,	,
Fred	Fred	k1gMnSc1	Fred
V	v	k7c6	v
&	&	k?	&
Grafix	Grafix	k1gInSc1	Grafix
<g/>
,	,	kIx,	,
Hedex	Hedex	k1gInSc1	Hedex
<g/>
,	,	kIx,	,
Tokyo	Tokyo	k6eAd1	Tokyo
prose	prosa	k1gFnSc3	prosa
<g/>
,	,	kIx,	,
Xilent	Xilent	k1gInSc1	Xilent
<g/>
,	,	kIx,	,
Rene	Rene	k1gFnSc1	Rene
LaVice	lavice	k1gFnSc1	lavice
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
prototypes	prototypes	k1gInSc1	prototypes
<g/>
,	,	kIx,	,
Dimension	Dimension	k1gInSc1	Dimension
<g/>
,	,	kIx,	,
Maduk	Maduk	k1gInSc1	Maduk
<g/>
,	,	kIx,	,
BCee	BCee	k1gNnPc1	BCee
<g/>
,	,	kIx,	,
Netsky	Netsek	k1gInPc1	Netsek
<g/>
,	,	kIx,	,
Chase	chasa	k1gFnSc3	chasa
and	and	k?	and
Status	status	k1gInSc1	status
<g/>
,	,	kIx,	,
Aquasky	Aquaska	k1gFnPc1	Aquaska
Black	Black	k1gMnSc1	Black
<g />
.	.	kIx.	.
</s>
<s>
Sun	Sun	kA	Sun
Empire	empir	k1gInSc5	empir
<g/>
,	,	kIx,	,
Camo	Camo	k1gMnSc1	Camo
and	and	k?	and
Krooked	Krooked	k1gMnSc1	Krooked
<g/>
,	,	kIx,	,
Concord	Concord	k1gMnSc1	Concord
Dawn	Dawn	k1gMnSc1	Dawn
<g/>
,	,	kIx,	,
Corrupt	Corrupt	k1gMnSc1	Corrupt
Souls	Soulsa	k1gFnPc2	Soulsa
<g/>
,	,	kIx,	,
Cyantific	Cyantifice	k1gFnPc2	Cyantifice
<g/>
,	,	kIx,	,
Kove	kov	k1gInSc5	kov
<g/>
,	,	kIx,	,
Dieselboy	Dieselboy	k1gInPc7	Dieselboy
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Plate	plat	k1gInSc5	plat
<g/>
,	,	kIx,	,
Jack	Jack	k1gMnSc1	Jack
Kaos	Kaos	k1gInSc1	Kaos
<g/>
,	,	kIx,	,
DJ	DJ	kA	DJ
Dara	Dar	k1gInSc2	Dar
<g/>
,	,	kIx,	,
D.	D.	kA	D.
<g/>
kay	kay	k?	kay
<g/>
,	,	kIx,	,
E-Z	E-Z	k1gFnSc1	E-Z
Rollers	Rollers	k1gInSc1	Rollers
<g/>
,	,	kIx,	,
Ed	Ed	k1gMnSc1	Ed
Rush	Rush	k1gMnSc1	Rush
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Evol	Evol	k1gMnSc1	Evol
Intent	Intent	k1gMnSc1	Intent
<g/>
,	,	kIx,	,
Ewun	Ewun	k1gMnSc1	Ewun
<g/>
,	,	kIx,	,
Feint	Feint	k1gMnSc1	Feint
<g/>
,	,	kIx,	,
High	High	k1gMnSc1	High
Contrast	Contrast	k1gMnSc1	Contrast
<g/>
,	,	kIx,	,
Mefjus	Mefjus	k1gMnSc1	Mefjus
<g/>
,	,	kIx,	,
<g/>
Technimatic	Technimatice	k1gFnPc2	Technimatice
<g/>
,	,	kIx,	,
Ill	Ill	k1gMnSc1	Ill
<g/>
.	.	kIx.	.
<g/>
Skillz	Skillz	k1gMnSc1	Skillz
<g/>
,	,	kIx,	,
J	J	kA	J
Majik	Majik	k1gInSc1	Majik
<g/>
,	,	kIx,	,
Klute	Klute	k?	Klute
<g/>
,	,	kIx,	,
Kosheen	Kosheen	k1gInSc1	Kosheen
<g/>
,	,	kIx,	,
Logistics	Logistics	k1gInSc1	Logistics
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
Elektricity	elektricita	k1gFnSc2	elektricita
<g/>
,	,	kIx,	,
LTJ	LTJ	kA	LTJ
Bukem	buk	k1gInSc7	buk
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
MoreBeat	MoreBeat	k1gInSc1	MoreBeat
<g/>
,	,	kIx,	,
Muffler	Muffler	k1gInSc1	Muffler
<g/>
,	,	kIx,	,
Muzzy	Muzz	k1gInPc1	Muzz
<g/>
,	,	kIx,	,
Noisia	Noisium	k1gNnPc1	Noisium
<g/>
,	,	kIx,	,
Nu	nu	k9	nu
<g/>
:	:	kIx,	:
<g/>
Tone	tonout	k5eAaImIp3nS	tonout
<g/>
,	,	kIx,	,
Omni	omnout	k5eAaPmRp2nS	omnout
Trio	trio	k1gNnSc1	trio
<g/>
,	,	kIx,	,
Optical	Optical	k1gFnSc1	Optical
<g/>
,	,	kIx,	,
Pendulum	Pendulum	k1gInSc1	Pendulum
<g/>
,	,	kIx,	,
Photek	Photek	k1gInSc1	Photek
<g/>
,	,	kIx,	,
Q	Q	kA	Q
Project	Project	k1gInSc1	Project
<g/>
,	,	kIx,	,
Roni	Roni	k1gNnSc1	Roni
Size	Size	k1gInSc1	Size
<g/>
,	,	kIx,	,
Spor	spor	k1gInSc1	spor
<g/>
,	,	kIx,	,
State	status	k1gInSc5	status
of	of	k?	of
Mind	Mind	k1gInSc1	Mind
<g/>
,	,	kIx,	,
Teebee	Teebee	k1gInSc1	Teebee
<g/>
,	,	kIx,	,
Zinc	Zinc	k1gInSc1	Zinc
Forbidden	Forbiddna	k1gFnPc2	Forbiddna
Society	societa	k1gFnSc2	societa
<g/>
,	,	kIx,	,
Rido	Rido	k1gMnSc1	Rido
<g/>
,	,	kIx,	,
Katcha	Katcha	k1gMnSc1	Katcha
<g/>
,	,	kIx,	,
Philip	Philip	k1gMnSc1	Philip
T.B.C.	T.B.C.	k1gMnSc1	T.B.C.
<g/>
,	,	kIx,	,
Ohm	ohm	k1gInSc1	ohm
Square	square	k1gInSc1	square
<g/>
,	,	kIx,	,
Pixie	Pixie	k1gFnSc1	Pixie
<g/>
,	,	kIx,	,
Symplex	symplex	k1gInSc1	symplex
<g/>
,	,	kIx,	,
A-Cray	A-Cray	k1gInPc1	A-Cray
<g/>
,	,	kIx,	,
Elwira	Elwiro	k1gNnPc1	Elwiro
<g/>
,	,	kIx,	,
One	One	k1gFnPc1	One
Blood	Blood	k1gInSc1	Blood
<g/>
,	,	kIx,	,
Apokain	Apokain	k1gInSc1	Apokain
<g/>
,	,	kIx,	,
Shmidoo	Shmidoo	k1gNnSc1	Shmidoo
<g/>
,	,	kIx,	,
Joshua	Joshua	k1gFnSc1	Joshua
B-Complex	B-Complex	k1gInSc1	B-Complex
L	L	kA	L
Plus	plus	k1gInSc1	plus
</s>
