<s>
Lithium	lithium	k1gNnSc1	lithium
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Li	li	k9	li
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Lithium	lithium	k1gNnSc4	lithium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejlehčí	lehký	k2eAgMnSc1d3	nejlehčí
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
alkalických	alkalický	k2eAgInPc2d1	alkalický
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
značně	značně	k6eAd1	značně
reaktivní	reaktivní	k2eAgInSc1d1	reaktivní
<g/>
,	,	kIx,	,
stříbřitě	stříbřitě	k6eAd1	stříbřitě
lesklého	lesklý	k2eAgInSc2d1	lesklý
vzhledu	vzhled	k1gInSc2	vzhled
<g/>
.	.	kIx.	.
</s>
