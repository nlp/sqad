<s>
Zimní	zimní	k2eAgFnPc1d1	zimní
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
(	(	kIx(	(
<g/>
též	též	k9	též
zimní	zimní	k2eAgFnSc1d1	zimní
olympiáda	olympiáda	k1gFnSc1	olympiáda
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
ZOH	ZOH	kA	ZOH
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
zimních	zimní	k2eAgFnPc2d1	zimní
sportovních	sportovní	k2eAgFnPc2d1	sportovní
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Konají	konat	k5eAaImIp3nP	konat
se	se	k3xPyFc4	se
každé	každý	k3xTgInPc4	každý
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
ve	v	k7c6	v
francouzském	francouzský	k2eAgNnSc6d1	francouzské
Chamonix	Chamonix	k1gNnSc6	Chamonix
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
byli	být	k5eAaImAgMnP	být
sezváni	sezván	k2eAgMnPc1d1	sezván
sportovci	sportovec	k1gMnPc1	sportovec
na	na	k7c4	na
"	"	kIx"	"
<g/>
Týden	týden	k1gInSc4	týden
zimních	zimní	k2eAgInPc2d1	zimní
sportů	sport	k1gInPc2	sport
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
klání	klání	k1gNnSc1	klání
bylo	být	k5eAaImAgNnS	být
zpětně	zpětně	k6eAd1	zpětně
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
označeno	označit	k5eAaPmNgNnS	označit
za	za	k7c4	za
1	[number]	k4	1
<g/>
.	.	kIx.	.
zimní	zimní	k2eAgFnSc2d1	zimní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
a	a	k8xC	a
1944	[number]	k4	1944
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgInP	mít
konat	konat	k5eAaImF	konat
v	v	k7c6	v
japonském	japonský	k2eAgInSc6d1	japonský
Sapporu	Sappor	k1gInSc6	Sappor
a	a	k8xC	a
v	v	k7c6	v
italské	italský	k2eAgFnSc6d1	italská
Cortině	Cortina	k1gFnSc6	Cortina
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Ampezzo	Ampezza	k1gFnSc5	Ampezza
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obě	dva	k4xCgFnPc1	dva
tyto	tento	k3xDgFnPc1	tento
akce	akce	k1gFnPc1	akce
byly	být	k5eAaImAgFnP	být
zrušeny	zrušit	k5eAaPmNgFnP	zrušit
kvůli	kvůli	k7c3	kvůli
probíhající	probíhající	k2eAgFnSc3d1	probíhající
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
měl	mít	k5eAaImAgInS	mít
původně	původně	k6eAd1	původně
pořádat	pořádat	k5eAaImF	pořádat
hry	hra	k1gFnPc4	hra
americký	americký	k2eAgInSc1d1	americký
Denver	Denver	k1gInSc1	Denver
<g/>
,	,	kIx,	,
jenže	jenže	k8xC	jenže
ten	ten	k3xDgMnSc1	ten
to	ten	k3xDgNnSc4	ten
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
státního	státní	k2eAgNnSc2d1	státní
referenda	referendum	k1gNnSc2	referendum
<g/>
.	.	kIx.	.
</s>
<s>
Pořadatelstvím	pořadatelství	k1gNnSc7	pořadatelství
her	hra	k1gFnPc2	hra
byl	být	k5eAaImAgMnS	být
tedy	tedy	k9	tedy
pověřen	pověřen	k2eAgMnSc1d1	pověřen
rakouský	rakouský	k2eAgMnSc1d1	rakouský
Innsbruck	Innsbruck	k1gMnSc1	Innsbruck
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
udržoval	udržovat	k5eAaImAgMnS	udržovat
infrastrukturu	infrastruktura	k1gFnSc4	infrastruktura
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
16	[number]	k4	16
<g/>
.	.	kIx.	.
zimní	zimní	k2eAgFnSc1d1	zimní
olympiáda	olympiáda	k1gFnSc1	olympiáda
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zimní	zimní	k2eAgFnPc1d1	zimní
hry	hra	k1gFnPc1	hra
konaly	konat	k5eAaImAgFnP	konat
vždy	vždy	k6eAd1	vždy
v	v	k7c4	v
týž	týž	k3xTgInSc4	týž
rok	rok	k1gInSc4	rok
jako	jako	k8xC	jako
letní	letní	k2eAgInSc4d1	letní
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
periodu	perioda	k1gFnSc4	perioda
zimních	zimní	k2eAgFnPc2d1	zimní
her	hra	k1gFnPc2	hra
posunout	posunout	k5eAaPmF	posunout
o	o	k7c4	o
polovinu	polovina	k1gFnSc4	polovina
vůči	vůči	k7c3	vůči
hrám	hra	k1gFnPc3	hra
letním	letnit	k5eAaImIp1nS	letnit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
se	s	k7c7	s
17	[number]	k4	17
<g/>
.	.	kIx.	.
</s>
<s>
ZOH	ZOH	kA	ZOH
konaly	konat	k5eAaImAgFnP	konat
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
hry	hra	k1gFnPc1	hra
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
konají	konat	k5eAaImIp3nP	konat
ve	v	k7c6	v
čtyřletém	čtyřletý	k2eAgInSc6d1	čtyřletý
cyklu	cyklus	k1gInSc6	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Zimní	zimní	k2eAgFnSc4d1	zimní
olympiádu	olympiáda	k1gFnSc4	olympiáda
zatím	zatím	k6eAd1	zatím
hostilo	hostit	k5eAaImAgNnS	hostit
11	[number]	k4	11
států	stát	k1gInPc2	stát
<g/>
:	:	kIx,	:
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
<g/>
,	,	kIx,	,
třikrát	třikrát	k6eAd1	třikrát
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
dvakrát	dvakrát	k6eAd1	dvakrát
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
a	a	k8xC	a
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
a	a	k8xC	a
jednou	jednou	k6eAd1	jednou
Jugoslávie	Jugoslávie	k1gFnSc1	Jugoslávie
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
a	a	k8xC	a
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
poslední	poslední	k2eAgFnPc1d1	poslední
hry	hra	k1gFnPc1	hra
byly	být	k5eAaImAgFnP	být
uspořádány	uspořádat	k5eAaPmNgFnP	uspořádat
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2014	[number]	k4	2014
v	v	k7c6	v
ruském	ruský	k2eAgNnSc6d1	ruské
Soči	Soči	k1gNnSc6	Soči
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
ZOH	ZOH	kA	ZOH
bude	být	k5eAaImBp3nS	být
hostit	hostit	k5eAaImF	hostit
jihokorejský	jihokorejský	k2eAgMnSc1d1	jihokorejský
Pchjongčchang	Pchjongčchang	k1gMnSc1	Pchjongčchang
(	(	kIx(	(
<g/>
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
a	a	k8xC	a
čínský	čínský	k2eAgInSc1d1	čínský
Peking	Peking	k1gInSc1	Peking
(	(	kIx(	(
<g/>
2022	[number]	k4	2022
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
nejúspěšnějších	úspěšný	k2eAgFnPc2d3	nejúspěšnější
zemí	zem	k1gFnPc2	zem
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
ZOH	ZOH	kA	ZOH
seřazený	seřazený	k2eAgInSc4d1	seřazený
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
získaných	získaný	k2eAgFnPc2d1	získaná
medailí	medaile	k1gFnPc2	medaile
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
jsou	být	k5eAaImIp3nP	být
uvedeny	uveden	k2eAgFnPc1d1	uvedena
země	zem	k1gFnPc1	zem
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
získaly	získat	k5eAaPmAgFnP	získat
nejméně	málo	k6eAd3	málo
10	[number]	k4	10
medailí	medaile	k1gFnPc2	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
nejúspěšnějších	úspěšný	k2eAgMnPc2d3	nejúspěšnější
sportovců	sportovec	k1gMnPc2	sportovec
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
zimních	zimní	k2eAgFnPc2d1	zimní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
podle	podle	k7c2	podle
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
získaných	získaný	k2eAgFnPc2d1	získaná
medailí	medaile	k1gFnPc2	medaile
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
jsou	být	k5eAaImIp3nP	být
uvedeni	uveden	k2eAgMnPc1d1	uveden
všichni	všechen	k3xTgMnPc1	všechen
závodníci	závodník	k1gMnPc1	závodník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
na	na	k7c6	na
ZOH	ZOH	kA	ZOH
získali	získat	k5eAaPmAgMnP	získat
nejméně	málo	k6eAd3	málo
osm	osm	k4xCc4	osm
medailí	medaile	k1gFnPc2	medaile
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
ZOH	ZOH	kA	ZOH
1924	[number]	k4	1924
Chamonix	Chamonix	k1gNnSc2	Chamonix
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
První	první	k4xOgFnSc7	první
ukončenou	ukončený	k2eAgFnSc7d1	ukončená
disciplínou	disciplína	k1gFnSc7	disciplína
zimních	zimní	k2eAgFnPc2d1	zimní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
byl	být	k5eAaImAgInS	být
rychlobruslařský	rychlobruslařský	k2eAgInSc1d1	rychlobruslařský
závod	závod	k1gInSc1	závod
mužů	muž	k1gMnPc2	muž
na	na	k7c4	na
500	[number]	k4	500
m	m	kA	m
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
prvním	první	k4xOgMnSc7	první
zlatým	zlatý	k2eAgMnSc7d1	zlatý
medailistou	medailista	k1gMnSc7	medailista
ZOH	ZOH	kA	ZOH
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Američan	Američan	k1gMnSc1	Američan
Charles	Charles	k1gMnSc1	Charles
Jewtraw	Jewtraw	k1gMnSc1	Jewtraw
<g/>
.	.	kIx.	.
</s>
<s>
Fin	Fin	k1gMnSc1	Fin
Clas	Clasa	k1gFnPc2	Clasa
Thunberg	Thunberg	k1gMnSc1	Thunberg
získal	získat	k5eAaPmAgMnS	získat
3	[number]	k4	3
medaile	medaile	k1gFnPc4	medaile
v	v	k7c4	v
rychlobruslení	rychlobruslení	k1gNnSc4	rychlobruslení
a	a	k8xC	a
Nor	Nor	k1gMnSc1	Nor
Thorleif	Thorleif	k1gMnSc1	Thorleif
Haug	Haug	k1gMnSc1	Haug
3	[number]	k4	3
zlaté	zlatý	k2eAgFnPc4d1	zlatá
medaile	medaile	k1gFnPc4	medaile
v	v	k7c6	v
lyžování	lyžování	k1gNnSc6	lyžování
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
ZOH	ZOH	kA	ZOH
1928	[number]	k4	1928
Svatý	svatý	k1gMnSc1	svatý
Mořic	Mořic	k1gMnSc1	Mořic
(	(	kIx(	(
<g/>
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
)	)	kIx)	)
Na	na	k7c6	na
programu	program	k1gInSc6	program
her	hra	k1gFnPc2	hra
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
nová	nový	k2eAgFnSc1d1	nová
disciplína	disciplína	k1gFnSc1	disciplína
-	-	kIx~	-
skeleton	skeleton	k1gInSc1	skeleton
<g/>
.	.	kIx.	.
</s>
<s>
Fin	Fin	k1gMnSc1	Fin
Clas	Clasa	k1gFnPc2	Clasa
Thunberg	Thunberg	k1gMnSc1	Thunberg
přidal	přidat	k5eAaPmAgMnS	přidat
ke	k	k7c3	k
3	[number]	k4	3
zlatým	zlatý	k2eAgFnPc3d1	zlatá
medailím	medaile	k1gFnPc3	medaile
s	s	k7c7	s
Chamonix	Chamonix	k1gNnSc7	Chamonix
další	další	k2eAgFnSc2d1	další
2	[number]	k4	2
zlaté	zlatá	k1gFnSc2	zlatá
v	v	k7c6	v
rychlobruslení	rychlobruslení	k1gNnSc6	rychlobruslení
<g/>
.	.	kIx.	.
</s>
<s>
Senzaci	senzace	k1gFnSc4	senzace
způsobila	způsobit	k5eAaPmAgFnS	způsobit
Norka	Norka	k1gFnSc1	Norka
Sonja	Sonja	k1gFnSc1	Sonja
Henie	Henie	k1gFnSc1	Henie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
krasobruslařskou	krasobruslařský	k2eAgFnSc4d1	krasobruslařská
soutěž	soutěž	k1gFnSc4	soutěž
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
15	[number]	k4	15
letech	let	k1gInPc6	let
a	a	k8xC	a
o	o	k7c4	o
primát	primát	k1gInSc4	primát
nejmladší	mladý	k2eAgFnSc2d3	nejmladší
olympijské	olympijský	k2eAgFnSc2d1	olympijská
vítězky	vítězka	k1gFnSc2	vítězka
přišla	přijít	k5eAaPmAgFnS	přijít
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
ZOH	ZOH	kA	ZOH
1932	[number]	k4	1932
Lake	Lak	k1gInSc2	Lak
Placid	Placid	k1gInSc1	Placid
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
Sonja	Sonj	k2eAgFnSc1d1	Sonja
Henie	Henie	k1gFnSc1	Henie
obhájila	obhájit	k5eAaPmAgFnS	obhájit
svou	svůj	k3xOyFgFnSc4	svůj
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
ze	z	k7c2	z
Svatého	svatý	k2eAgMnSc2d1	svatý
Mořice	Mořic	k1gMnSc2	Mořic
<g/>
.	.	kIx.	.
</s>
<s>
Členem	člen	k1gMnSc7	člen
vítězného	vítězný	k2eAgInSc2d1	vítězný
amerického	americký	k2eAgInSc2d1	americký
čtyřbobu	čtyřbob	k1gInSc2	čtyřbob
byl	být	k5eAaImAgInS	být
Eddie	Eddie	k1gFnSc2	Eddie
Eagan	Eagan	k1gInSc1	Eagan
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
již	již	k6eAd1	již
získal	získat	k5eAaPmAgInS	získat
zlatou	zlatý	k2eAgFnSc4d1	zlatá
olympijskou	olympijský	k2eAgFnSc4d1	olympijská
medaili	medaile	k1gFnSc4	medaile
na	na	k7c6	na
letních	letní	k2eAgFnPc6d1	letní
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
v	v	k7c6	v
Antverpách	Antverpy	k1gFnPc6	Antverpy
v	v	k7c6	v
boxu	box	k1gInSc6	box
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
jediným	jediný	k2eAgMnSc7d1	jediný
olympionikem	olympionik	k1gMnSc7	olympionik
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
na	na	k7c6	na
zimních	zimní	k2eAgFnPc6d1	zimní
i	i	k8xC	i
letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
ZOH	ZOH	kA	ZOH
1936	[number]	k4	1936
Garmisch-Partenkirchen	Garmisch-Partenkirchen	k1gInSc1	Garmisch-Partenkirchen
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
Poprvé	poprvé	k6eAd1	poprvé
byly	být	k5eAaImAgInP	být
na	na	k7c4	na
program	program	k1gInSc4	program
her	hra	k1gFnPc2	hra
zařazeny	zařadit	k5eAaPmNgFnP	zařadit
soutěže	soutěž	k1gFnPc1	soutěž
v	v	k7c6	v
alpském	alpský	k2eAgNnSc6d1	alpské
lyžování	lyžování	k1gNnSc6	lyžování
<g/>
.	.	kIx.	.
</s>
<s>
Rychlobruslař	rychlobruslař	k1gMnSc1	rychlobruslař
Ivar	Ivar	k1gMnSc1	Ivar
Ballangrud	Ballangrud	k1gMnSc1	Ballangrud
z	z	k7c2	z
Norska	Norsko	k1gNnSc2	Norsko
získal	získat	k5eAaPmAgInS	získat
3	[number]	k4	3
zlaté	zlatý	k2eAgFnPc4d1	zlatá
medaile	medaile	k1gFnPc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
turnaj	turnaj	k1gInSc4	turnaj
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
a	a	k8xC	a
sesadila	sesadit	k5eAaPmAgFnS	sesadit
Kanadu	Kanada	k1gFnSc4	Kanada
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dosud	dosud	k6eAd1	dosud
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
hrách	hra	k1gFnPc6	hra
<g/>
.	.	kIx.	.
</s>
<s>
Československo	Československo	k1gNnSc1	Československo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
výpravu	výprava	k1gFnSc4	výprava
vedl	vést	k5eAaImAgMnS	vést
František	František	k1gMnSc1	František
Widimský	Widimský	k2eAgMnSc1d1	Widimský
<g/>
,	,	kIx,	,
vyslalo	vyslat	k5eAaPmAgNnS	vyslat
na	na	k7c4	na
hry	hra	k1gFnPc4	hra
celkem	celkem	k6eAd1	celkem
55	[number]	k4	55
sportovců	sportovec	k1gMnPc2	sportovec
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
pět	pět	k4xCc1	pět
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
ZOH	ZOH	kA	ZOH
1948	[number]	k4	1948
Svatý	svatý	k1gMnSc1	svatý
Mořic	Mořic	k1gMnSc1	Mořic
(	(	kIx(	(
<g/>
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
)	)	kIx)	)
Olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgInP	mít
konat	konat	k5eAaImF	konat
v	v	k7c6	v
japonském	japonský	k2eAgInSc6d1	japonský
Sapporu	Sappor	k1gInSc6	Sappor
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
s	s	k7c7	s
Čínou	Čína	k1gFnSc7	Čína
však	však	k9	však
přinutila	přinutit	k5eAaPmAgFnS	přinutit
Japonsko	Japonsko	k1gNnSc4	Japonsko
se	se	k3xPyFc4	se
pořádání	pořádání	k1gNnSc1	pořádání
her	hra	k1gFnPc2	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
vzdát	vzdát	k5eAaPmF	vzdát
<g/>
,	,	kIx,	,
alternativou	alternativa	k1gFnSc7	alternativa
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
německý	německý	k2eAgInSc4d1	německý
Garmisch-Partenkirchen	Garmisch-Partenkirchen	k1gInSc4	Garmisch-Partenkirchen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
začátek	začátek	k1gInSc1	začátek
II	II	kA	II
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byly	být	k5eAaImAgFnP	být
hry	hra	k1gFnPc1	hra
zrušeny	zrušit	k5eAaPmNgFnP	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Nekonaly	konat	k5eNaImAgInP	konat
se	se	k3xPyFc4	se
ani	ani	k9	ani
hry	hra	k1gFnPc1	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
a	a	k8xC	a
na	na	k7c4	na
tradici	tradice	k1gFnSc4	tradice
navázaly	navázat	k5eAaPmAgInP	navázat
až	až	k9	až
hry	hra	k1gFnSc2	hra
ve	v	k7c6	v
Svatém	svatý	k1gMnSc6	svatý
Mořici	Mořic	k1gMnSc6	Mořic
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
soutěží	soutěž	k1gFnPc2	soutěž
byli	být	k5eAaImAgMnP	být
vyloučeni	vyloučen	k2eAgMnPc1d1	vyloučen
sportovci	sportovec	k1gMnPc1	sportovec
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
2	[number]	k4	2
sportovci	sportovec	k1gMnPc1	sportovec
dokázali	dokázat	k5eAaPmAgMnP	dokázat
získat	získat	k5eAaPmF	získat
2	[number]	k4	2
zlaté	zlatý	k2eAgFnPc4d1	zlatá
medaile	medaile	k1gFnPc4	medaile
-	-	kIx~	-
francouzský	francouzský	k2eAgMnSc1d1	francouzský
sjezdař	sjezdař	k1gMnSc1	sjezdař
Oreiller	Oreiller	k1gMnSc1	Oreiller
a	a	k8xC	a
švédský	švédský	k2eAgMnSc1d1	švédský
lyžař	lyžař	k1gMnSc1	lyžař
Lundström	Lundström	k1gMnSc1	Lundström
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
ZOH	ZOH	kA	ZOH
1952	[number]	k4	1952
Oslo	Oslo	k1gNnSc2	Oslo
(	(	kIx(	(
<g/>
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
)	)	kIx)	)
Norský	norský	k2eAgMnSc1d1	norský
rychlobruslař	rychlobruslař	k1gMnSc1	rychlobruslař
Andersen	Andersen	k1gMnSc1	Andersen
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
domácím	domácí	k2eAgNnSc6d1	domácí
prostředí	prostředí	k1gNnSc6	prostředí
3	[number]	k4	3
zlaté	zlatý	k2eAgFnPc4d1	zlatá
medaile	medaile	k1gFnPc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
mezi	mezi	k7c7	mezi
disciplínami	disciplína	k1gFnPc7	disciplína
ZOH	ZOH	kA	ZOH
obří	obří	k2eAgInSc4d1	obří
slalom	slalom	k1gInSc4	slalom
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byly	být	k5eAaImAgFnP	být
pořádány	pořádán	k2eAgFnPc1d1	pořádána
soutěže	soutěž	k1gFnPc1	soutěž
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Nejúspěšnějším	úspěšný	k2eAgMnSc7d3	nejúspěšnější
účastníkem	účastník	k1gMnSc7	účastník
byl	být	k5eAaImAgMnS	být
se	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
zlatými	zlatý	k2eAgFnPc7d1	zlatá
medailemi	medaile	k1gFnPc7	medaile
norský	norský	k2eAgMnSc1d1	norský
rychlobruslař	rychlobruslař	k1gMnSc1	rychlobruslař
Hjalmar	Hjalmar	k1gMnSc1	Hjalmar
Andersen	Andersen	k1gMnSc1	Andersen
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
ZOH	ZOH	kA	ZOH
1956	[number]	k4	1956
Cortina	Cortina	k1gFnSc1	Cortina
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Ampezzo	Ampezza	k1gFnSc5	Ampezza
(	(	kIx(	(
<g/>
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
)	)	kIx)	)
Poslední	poslední	k2eAgFnPc1d1	poslední
hry	hra	k1gFnPc1	hra
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yRgInPc6	který
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
krasobruslařské	krasobruslařský	k2eAgFnPc1d1	krasobruslařská
soutěže	soutěž	k1gFnPc1	soutěž
pod	pod	k7c7	pod
širým	širý	k2eAgNnSc7d1	širé
nebem	nebe	k1gNnSc7	nebe
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
hry	hra	k1gFnPc1	hra
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
vysílány	vysílat	k5eAaImNgInP	vysílat
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
televize	televize	k1gFnSc2	televize
<g/>
.	.	kIx.	.
</s>
<s>
Hry	hra	k1gFnPc1	hra
byly	být	k5eAaImAgFnP	být
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
dominance	dominance	k1gFnSc2	dominance
závodníků	závodník	k1gMnPc2	závodník
ze	z	k7c2	z
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
<g/>
Hry	hra	k1gFnPc1	hra
v	v	k7c6	v
italské	italský	k2eAgFnSc6d1	italská
Cortině	Cortina	k1gFnSc6	Cortina
vstoupily	vstoupit	k5eAaPmAgInP	vstoupit
do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
fenomenálním	fenomenální	k2eAgInSc7d1	fenomenální
výkonem	výkon	k1gInSc7	výkon
rakouského	rakouský	k2eAgMnSc2d1	rakouský
sjezdaře	sjezdař	k1gMnSc2	sjezdař
Antona	Anton	k1gMnSc2	Anton
Sailera	Sailer	k1gMnSc2	Sailer
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ve	v	k7c6	v
sjezdovém	sjezdový	k2eAgNnSc6d1	sjezdové
lyžování	lyžování	k1gNnSc6	lyžování
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
všechny	všechen	k3xTgFnPc4	všechen
tři	tři	k4xCgFnPc4	tři
disciplíny	disciplína	k1gFnPc4	disciplína
-	-	kIx~	-
sjezd	sjezd	k1gInSc1	sjezd
<g/>
,	,	kIx,	,
slalom	slalom	k1gInSc1	slalom
i	i	k8xC	i
obří	obří	k2eAgInSc1d1	obří
slalom	slalom	k1gInSc1	slalom
-	-	kIx~	-
a	a	k8xC	a
zastínil	zastínit	k5eAaPmAgMnS	zastínit
výkony	výkon	k1gInPc1	výkon
finského	finský	k2eAgMnSc2d1	finský
běžce	běžec	k1gMnSc2	běžec
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
Veikko	Veikko	k1gNnSc1	Veikko
Hakulinena	Hakulinen	k1gMnSc2	Hakulinen
a	a	k8xC	a
rychlobruslaře	rychlobruslař	k1gMnSc2	rychlobruslař
SSSR	SSSR	kA	SSSR
Jevgenije	Jevgenije	k1gFnSc2	Jevgenije
Grišina	Grišin	k2eAgInSc2d1	Grišin
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
ZOH	ZOH	kA	ZOH
1960	[number]	k4	1960
Squaw	squaw	k1gFnSc2	squaw
Valley	Vallea	k1gFnSc2	Vallea
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
Jediné	jediné	k1gNnSc1	jediné
ZOH	ZOH	kA	ZOH
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yRgInPc6	který
se	se	k3xPyFc4	se
nekonaly	konat	k5eNaImAgInP	konat
soutěže	soutěž	k1gFnSc2	soutěž
v	v	k7c6	v
jízdě	jízda	k1gFnSc6	jízda
na	na	k7c6	na
bobech	bob	k1gInPc6	bob
<g/>
.	.	kIx.	.
</s>
<s>
Američtí	americký	k2eAgMnPc1d1	americký
pořadatelé	pořadatel	k1gMnPc1	pořadatel
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
vystavět	vystavět	k5eAaPmF	vystavět
bobovou	bobový	k2eAgFnSc4d1	bobová
dráhu	dráha	k1gFnSc4	dráha
s	s	k7c7	s
odvoláním	odvolání	k1gNnSc7	odvolání
na	na	k7c4	na
nízký	nízký	k2eAgInSc4d1	nízký
počet	počet	k1gInSc4	počet
přihlášených	přihlášený	k2eAgMnPc2d1	přihlášený
závodníků	závodník	k1gMnPc2	závodník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
programu	program	k1gInSc6	program
her	hra	k1gFnPc2	hra
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevil	objevit	k5eAaPmAgInS	objevit
biatlon	biatlon	k1gInSc1	biatlon
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
také	také	k9	také
ženské	ženský	k2eAgFnPc4d1	ženská
soutěže	soutěž	k1gFnPc4	soutěž
v	v	k7c6	v
rychlobruslení	rychlobruslení	k1gNnSc6	rychlobruslení
<g/>
.	.	kIx.	.
</s>
<s>
Hokejový	hokejový	k2eAgInSc1d1	hokejový
turnaj	turnaj	k1gInSc1	turnaj
vyhrály	vyhrát	k5eAaPmAgInP	vyhrát
nečekaně	nečekaně	k6eAd1	nečekaně
USA	USA	kA	USA
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
dokázaly	dokázat	k5eAaPmAgFnP	dokázat
porazit	porazit	k5eAaPmF	porazit
Kanadu	Kanada	k1gFnSc4	Kanada
i	i	k9	i
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Organizací	organizace	k1gFnPc2	organizace
ceremoniálů	ceremoniál	k1gInPc2	ceremoniál
byl	být	k5eAaImAgMnS	být
pověřen	pověřen	k2eAgMnSc1d1	pověřen
Walt	Walt	k1gMnSc1	Walt
Disney	Disnea	k1gFnSc2	Disnea
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
ZOH	ZOH	kA	ZOH
1964	[number]	k4	1964
Innsbruck	Innsbruck	k1gInSc1	Innsbruck
(	(	kIx(	(
<g/>
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
)	)	kIx)	)
Hry	hra	k1gFnPc1	hra
v	v	k7c6	v
Innsbrucku	Innsbrucko	k1gNnSc6	Innsbrucko
byly	být	k5eAaImAgInP	být
poznamenány	poznamenat	k5eAaPmNgInP	poznamenat
nedostatkem	nedostatek	k1gInSc7	nedostatek
sněhu	sníh	k1gInSc2	sníh
<g/>
.	.	kIx.	.
</s>
<s>
Rakouská	rakouský	k2eAgFnSc1d1	rakouská
armáda	armáda	k1gFnSc1	armáda
svážela	svážet	k5eAaImAgFnS	svážet
sníh	sníh	k1gInSc4	sníh
a	a	k8xC	a
led	led	k1gInSc4	led
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
<g/>
.	.	kIx.	.
</s>
<s>
Lidija	Lidij	k2eAgFnSc1d1	Lidija
Skoblikovová	Skoblikovová	k1gFnSc1	Skoblikovová
získala	získat	k5eAaPmAgFnS	získat
4	[number]	k4	4
zlaté	zlatý	k2eAgFnPc4d1	zlatá
medaile	medaile	k1gFnPc4	medaile
v	v	k7c6	v
rychlobruslení	rychlobruslení	k1gNnSc6	rychlobruslení
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
prvním	první	k4xOgMnSc7	první
sportovcem	sportovec	k1gMnSc7	sportovec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dokázal	dokázat	k5eAaPmAgInS	dokázat
získat	získat	k5eAaPmF	získat
4	[number]	k4	4
zlaté	zlatý	k2eAgFnPc4d1	zlatá
medaile	medaile	k1gFnPc4	medaile
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
zimní	zimní	k2eAgFnSc6d1	zimní
olympiádě	olympiáda	k1gFnSc6	olympiáda
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
na	na	k7c6	na
hrách	hra	k1gFnPc6	hra
představila	představit	k5eAaPmAgFnS	představit
jízda	jízda	k1gFnSc1	jízda
na	na	k7c6	na
saních	saně	k1gFnPc6	saně
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
ZOH	ZOH	kA	ZOH
1968	[number]	k4	1968
Grenoble	Grenoble	k1gInSc1	Grenoble
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kuriózní	kuriózní	k2eAgInSc1d1	kuriózní
skandál	skandál	k1gInSc1	skandál
se	se	k3xPyFc4	se
odehrál	odehrát	k5eAaPmAgInS	odehrát
při	při	k7c6	při
závodech	závod	k1gInPc6	závod
v	v	k7c6	v
mužském	mužský	k2eAgInSc6d1	mužský
slalomu	slalom	k1gInSc6	slalom
<g/>
.	.	kIx.	.
</s>
<s>
Vítězem	vítěz	k1gMnSc7	vítěz
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Francouz	Francouz	k1gMnSc1	Francouz
Jean-Claude	Jean-Claud	k1gInSc5	Jean-Claud
Killy	Killa	k1gFnPc4	Killa
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
rival	rival	k1gMnSc1	rival
Schranz	Schranz	k1gMnSc1	Schranz
po	po	k7c6	po
protestu	protest	k1gInSc6	protest
na	na	k7c4	na
zkřížení	zkřížení	k1gNnSc4	zkřížení
tratě	trať	k1gFnSc2	trať
neznámým	známý	k2eNgMnSc7d1	neznámý
mužem	muž	k1gMnSc7	muž
dostal	dostat	k5eAaPmAgMnS	dostat
možnost	možnost	k1gFnSc4	možnost
opakované	opakovaný	k2eAgFnSc2d1	opakovaná
jízdy	jízda	k1gFnSc2	jízda
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgMnPc4	který
Francouze	Francouz	k1gMnPc4	Francouz
porazil	porazit	k5eAaPmAgInS	porazit
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodčí	rozhodčí	k1gMnPc1	rozhodčí
však	však	k9	však
po	po	k7c6	po
protestu	protest	k1gInSc6	protest
z	z	k7c2	z
francouzské	francouzský	k2eAgFnSc2d1	francouzská
strany	strana	k1gFnSc2	strana
přiřkli	přiřknout	k5eAaPmAgMnP	přiřknout
nakonec	nakonec	k6eAd1	nakonec
vítězství	vítězství	k1gNnSc4	vítězství
stejně	stejně	k6eAd1	stejně
Killymu	Killym	k1gInSc2	Killym
<g/>
.	.	kIx.	.
</s>
<s>
Skandál	skandál	k1gInSc1	skandál
se	se	k3xPyFc4	se
odehrál	odehrát	k5eAaPmAgInS	odehrát
při	při	k7c6	při
sáňkařských	sáňkařský	k2eAgInPc6d1	sáňkařský
závodech	závod	k1gInPc6	závod
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgInP	být
3	[number]	k4	3
závodnice	závodnice	k1gFnSc1	závodnice
z	z	k7c2	z
NDR	NDR	kA	NDR
diskvalifikovány	diskvalifikovat	k5eAaBmNgFnP	diskvalifikovat
pro	pro	k7c4	pro
zahřívání	zahřívání	k1gNnSc4	zahřívání
svých	svůj	k3xOyFgFnPc2	svůj
saní	saně	k1gFnPc2	saně
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Raška	Raška	k1gMnSc1	Raška
ve	v	k7c6	v
skoku	skok	k1gInSc6	skok
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
na	na	k7c6	na
středním	střední	k2eAgInSc6d1	střední
můstku	můstek	k1gInSc6	můstek
vybojoval	vybojovat	k5eAaPmAgMnS	vybojovat
pro	pro	k7c4	pro
Československo	Československo	k1gNnSc4	Československo
historicky	historicky	k6eAd1	historicky
první	první	k4xOgFnSc4	první
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
ve	v	k7c6	v
skoku	skok	k1gInSc6	skok
na	na	k7c6	na
velkém	velký	k2eAgInSc6d1	velký
můstku	můstek	k1gInSc6	můstek
přidal	přidat	k5eAaPmAgMnS	přidat
ještě	ještě	k9	ještě
medaili	medaile	k1gFnSc4	medaile
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
<g/>
.	.	kIx.	.
</s>
<s>
Bronzovou	bronzový	k2eAgFnSc7d1	bronzová
medailí	medaile	k1gFnSc7	medaile
přispěla	přispět	k5eAaPmAgFnS	přispět
do	do	k7c2	do
kolekce	kolekce	k1gFnSc2	kolekce
úspěchů	úspěch	k1gInPc2	úspěch
též	též	k9	též
krasobruslařka	krasobruslařka	k1gFnSc1	krasobruslařka
Hana	Hana	k1gFnSc1	Hana
Mašková	Mašková	k1gFnSc1	Mašková
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
ZOH	ZOH	kA	ZOH
1972	[number]	k4	1972
Sapporo	Sappora	k1gFnSc5	Sappora
(	(	kIx(	(
<g/>
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
)	)	kIx)	)
Galina	Galina	k1gFnSc1	Galina
Kulakova	kulakův	k2eAgFnSc1d1	kulakova
získala	získat	k5eAaPmAgFnS	získat
3	[number]	k4	3
zlaté	zlatý	k2eAgFnPc4d1	zlatá
medaile	medaile	k1gFnPc4	medaile
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
Nizozemec	Nizozemec	k1gMnSc1	Nizozemec
Ard	Ard	k1gMnSc1	Ard
Schenk	Schenk	k1gMnSc1	Schenk
získal	získat	k5eAaPmAgMnS	získat
3	[number]	k4	3
zlaté	zlatý	k2eAgFnPc4d1	zlatá
medaile	medaile	k1gFnPc4	medaile
v	v	k7c6	v
rychlobruslení	rychlobruslení	k1gNnSc6	rychlobruslení
a	a	k8xC	a
běžkyně	běžkyně	k1gFnSc1	běžkyně
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
Galina	Galina	k1gFnSc1	Galina
Kulakovová	Kulakovová	k1gFnSc1	Kulakovová
z	z	k7c2	z
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Skokan	Skokan	k1gMnSc1	Skokan
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
Kasaya	Kasayum	k1gNnSc2	Kasayum
vybojoval	vybojovat	k5eAaPmAgInS	vybojovat
první	první	k4xOgFnSc4	první
zimní	zimní	k2eAgFnSc4d1	zimní
zlatou	zlatý	k2eAgFnSc4d1	zlatá
olympijskou	olympijský	k2eAgFnSc4d1	olympijská
medaili	medaile	k1gFnSc4	medaile
pro	pro	k7c4	pro
Japonsko	Japonsko	k1gNnSc4	Japonsko
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
ZOH	ZOH	kA	ZOH
1976	[number]	k4	1976
Innsbruck	Innsbruck	k1gInSc1	Innsbruck
(	(	kIx(	(
<g/>
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
)	)	kIx)	)
Původně	původně	k6eAd1	původně
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
hry	hra	k1gFnPc1	hra
pořádány	pořádán	k2eAgFnPc1d1	pořádána
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
Denveru	Denver	k1gInSc6	Denver
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
odřeknutí	odřeknutí	k1gNnSc6	odřeknutí
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
podruhé	podruhé	k6eAd1	podruhé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
pořadatelem	pořadatel	k1gMnSc7	pořadatel
Innsbruck	Innsbrucka	k1gFnPc2	Innsbrucka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
krasobruslení	krasobruslení	k1gNnSc6	krasobruslení
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevila	objevit	k5eAaPmAgFnS	objevit
soutěž	soutěž	k1gFnSc1	soutěž
tanečních	taneční	k2eAgInPc2d1	taneční
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
<s>
Rosi	Rosi	k1gMnSc1	Rosi
Mittermaierová	Mittermaierová	k1gFnSc1	Mittermaierová
ze	z	k7c2	z
Spolkové	spolkový	k2eAgFnSc2d1	spolková
republiky	republika	k1gFnSc2	republika
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ve	v	k7c6	v
sjezdařských	sjezdařský	k2eAgFnPc6d1	sjezdařská
displínách	displína	k1gFnPc6	displína
žen	žena	k1gFnPc2	žena
získala	získat	k5eAaPmAgFnS	získat
dvě	dva	k4xCgFnPc4	dva
zlaté	zlatý	k2eAgFnPc4d1	zlatá
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
medaili	medaile	k1gFnSc4	medaile
<g/>
,	,	kIx,	,
a	a	k8xC	a
Raisa	Raisa	k1gFnSc1	Raisa
Smetaninová	Smetaninový	k2eAgFnSc1d1	Smetaninová
z	z	k7c2	z
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
stejnou	stejný	k2eAgFnSc4d1	stejná
kolekci	kolekce	k1gFnSc6	kolekce
medailí	medaile	k1gFnPc2	medaile
dobyla	dobýt	k5eAaPmAgFnS	dobýt
v	v	k7c6	v
ženských	ženský	k2eAgFnPc6d1	ženská
běžeckých	běžecký	k2eAgFnPc6d1	běžecká
disciplínách	disciplína	k1gFnPc6	disciplína
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
<s>
ZOH	ZOH	kA	ZOH
1980	[number]	k4	1980
Lake	Lak	k1gInSc2	Lak
Placid	Placid	k1gInSc1	Placid
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
Americký	americký	k2eAgMnSc1d1	americký
rychlobruslař	rychlobruslař	k1gMnSc1	rychlobruslař
Eric	Eric	k1gFnSc4	Eric
Heiden	Heidna	k1gFnPc2	Heidna
získal	získat	k5eAaPmAgMnS	získat
všech	všecek	k3xTgInPc6	všecek
5	[number]	k4	5
možných	možný	k2eAgFnPc2d1	možná
zlatých	zlatý	k2eAgFnPc2d1	zlatá
medailí	medaile	k1gFnPc2	medaile
v	v	k7c6	v
rychlobruslařských	rychlobruslařský	k2eAgFnPc6d1	rychlobruslařská
soutěžích	soutěž	k1gFnPc6	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Ruský	ruský	k2eAgMnSc1d1	ruský
biatlonista	biatlonista	k1gMnSc1	biatlonista
Tichonov	Tichonov	k1gInSc4	Tichonov
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
na	na	k7c6	na
čtvrtých	čtvrtá	k1gFnPc2	čtvrtá
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
získal	získat	k5eAaPmAgMnS	získat
3	[number]	k4	3
zlaté	zlatý	k2eAgFnSc2d1	zlatá
medaile	medaile	k1gFnSc2	medaile
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Zimjatov	Zimjatov	k1gInSc1	Zimjatov
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
USA	USA	kA	USA
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
nečekaně	nečekaně	k6eAd1	nečekaně
turnaj	turnaj	k1gInSc1	turnaj
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
<g/>
.	.	kIx.	.
<g/>
Své	své	k1gNnSc1	své
hrdiny	hrdina	k1gMnSc2	hrdina
našly	najít	k5eAaPmAgInP	najít
v	v	k7c6	v
Ericu	Eric	k1gMnSc6	Eric
Haidenovi	Haiden	k1gMnSc6	Haiden
z	z	k7c2	z
USA	USA	kA	USA
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
pěti	pět	k4xCc6	pět
rychlobruslařských	rychlobruslařský	k2eAgFnPc6d1	rychlobruslařská
disciplínách	disciplína	k1gFnPc6	disciplína
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
v	v	k7c6	v
lyžaři	lyžař	k1gMnSc6	lyžař
Ulrichu	Ulrich	k1gMnSc6	Ulrich
Wehlingovi	Wehling	k1gMnSc6	Wehling
z	z	k7c2	z
NDR	NDR	kA	NDR
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
potřetí	potřetí	k4xO	potřetí
za	za	k7c7	za
sebou	se	k3xPyFc7	se
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
závod	závod	k1gInSc4	závod
v	v	k7c6	v
severské	severský	k2eAgFnSc6d1	severská
kombinaci	kombinace	k1gFnSc6	kombinace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
točivých	točivý	k2eAgFnPc6d1	točivá
sjezdařských	sjezdařský	k2eAgFnPc6d1	sjezdařská
soutěžích	soutěž	k1gFnPc6	soutěž
kralovali	kralovat	k5eAaImAgMnP	kralovat
lyžaři	lyžař	k1gMnPc1	lyžař
Ingemar	Ingemara	k1gFnPc2	Ingemara
Stenmark	Stenmark	k1gInSc4	Stenmark
ze	z	k7c2	z
Švédska	Švédsko	k1gNnSc2	Švédsko
a	a	k8xC	a
Hanni	Hanň	k1gMnSc3	Hanň
Wenzelová	Wenzelová	k1gFnSc1	Wenzelová
z	z	k7c2	z
Lichtenštejnska	Lichtenštejnsko	k1gNnSc2	Lichtenštejnsko
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
</s>
<s>
ZOH	ZOH	kA	ZOH
1984	[number]	k4	1984
Sarajevo	Sarajevo	k1gNnSc1	Sarajevo
(	(	kIx(	(
<g/>
Jugoslávie	Jugoslávie	k1gFnSc1	Jugoslávie
<g/>
)	)	kIx)	)
Domácí	domácí	k2eAgMnPc1d1	domácí
lyžař	lyžař	k1gMnSc1	lyžař
Jure	Jur	k1gMnSc2	Jur
Franko	franko	k6eAd1	franko
vybojoval	vybojovat	k5eAaPmAgMnS	vybojovat
první	první	k4xOgFnSc4	první
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
na	na	k7c4	na
ZOH	ZOH	kA	ZOH
pro	pro	k7c4	pro
Jugoslávii	Jugoslávie	k1gFnSc4	Jugoslávie
<g/>
.	.	kIx.	.
</s>
<s>
Marja-Liisa	Marja-Liisa	k1gFnSc1	Marja-Liisa
Hämäläinen	Hämäläinna	k1gFnPc2	Hämäläinna
získala	získat	k5eAaPmAgFnS	získat
3	[number]	k4	3
zlaté	zlatý	k2eAgFnPc4d1	zlatá
medaile	medaile	k1gFnPc4	medaile
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
a	a	k8xC	a
druhé	druhý	k4xOgNnSc1	druhý
místo	místo	k1gNnSc1	místo
ve	v	k7c6	v
slalomu	slalom	k1gInSc6	slalom
vybojovala	vybojovat	k5eAaPmAgNnP	vybojovat
dvojčata	dvojče	k1gNnPc1	dvojče
z	z	k7c2	z
USA	USA	kA	USA
-	-	kIx~	-
Phil	Phil	k1gMnSc1	Phil
a	a	k8xC	a
Steve	Steve	k1gMnSc1	Steve
Mahreovi	Mahreus	k1gMnSc3	Mahreus
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
první	první	k4xOgFnSc4	první
olympijskou	olympijský	k2eAgFnSc4d1	olympijská
medaili	medaile	k1gFnSc4	medaile
v	v	k7c6	v
alpském	alpský	k2eAgNnSc6d1	alpské
lyžování	lyžování	k1gNnSc6	lyžování
pro	pro	k7c4	pro
Československo	Československo	k1gNnSc4	Československo
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
Olga	Olga	k1gFnSc1	Olga
Charvátová	Charvátová	k1gFnSc1	Charvátová
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
</s>
<s>
ZOH	ZOH	kA	ZOH
1988	[number]	k4	1988
Calgary	Calgary	k1gNnSc2	Calgary
(	(	kIx(	(
<g/>
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
)	)	kIx)	)
Na	na	k7c4	na
program	program	k1gInSc4	program
her	hra	k1gFnPc2	hra
zařazen	zařadit	k5eAaPmNgInS	zařadit
super	super	k2eAgInSc1d1	super
obří	obří	k2eAgInSc1d1	obří
slalom	slalom	k1gInSc1	slalom
a	a	k8xC	a
kombinace	kombinace	k1gFnSc1	kombinace
<g/>
.	.	kIx.	.
3	[number]	k4	3
zlaté	zlatý	k2eAgFnSc2d1	zlatá
medaile	medaile	k1gFnSc2	medaile
vybojoval	vybojovat	k5eAaPmAgMnS	vybojovat
finský	finský	k2eAgMnSc1d1	finský
skokan	skokan	k1gMnSc1	skokan
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
Matti	Matti	k1gNnSc2	Matti
Nykänen	Nykänna	k1gFnPc2	Nykänna
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mu	on	k3xPp3gMnSc3	on
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
i	i	k9	i
premiérové	premiérový	k2eAgNnSc1d1	premiérové
zařazení	zařazení	k1gNnSc1	zařazení
závodu	závod	k1gInSc2	závod
družstev	družstvo	k1gNnPc2	družstvo
ve	v	k7c6	v
skocích	skok	k1gInPc6	skok
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
<g/>
.	.	kIx.	.
</s>
<s>
Rychlobruslařské	rychlobruslařský	k2eAgFnPc1d1	rychlobruslařská
soutěže	soutěž	k1gFnPc1	soutěž
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
hale	hala	k1gFnSc6	hala
<g/>
.	.	kIx.	.
</s>
<s>
Yvonne	Yvonnout	k5eAaImIp3nS	Yvonnout
van	van	k1gInSc4	van
Gennipová	Gennipová	k1gFnSc1	Gennipová
získala	získat	k5eAaPmAgFnS	získat
3	[number]	k4	3
zlaté	zlatý	k2eAgFnPc4d1	zlatá
medaile	medaile	k1gFnPc4	medaile
v	v	k7c6	v
rychlobruslení	rychlobruslení	k1gNnSc6	rychlobruslení
<g/>
,	,	kIx,	,
Christa	Christa	k1gMnSc1	Christa
Rothenburgerová	Rothenburgerová	k1gFnSc1	Rothenburgerová
získala	získat	k5eAaPmAgFnS	získat
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
500	[number]	k4	500
m	m	kA	m
a	a	k8xC	a
o	o	k7c4	o
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
později	pozdě	k6eAd2	pozdě
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
i	i	k8xC	i
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
medaili	medaile	k1gFnSc4	medaile
v	v	k7c6	v
cyklistice	cyklistika	k1gFnSc6	cyklistika
na	na	k7c4	na
LOH	LOH	kA	LOH
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
tak	tak	k8xC	tak
prvním	první	k4xOgNnSc7	první
a	a	k8xC	a
doposud	doposud	k6eAd1	doposud
i	i	k9	i
jediným	jediný	k2eAgMnSc7d1	jediný
sportovcem	sportovec	k1gMnSc7	sportovec
<g/>
/	/	kIx~	/
<g/>
sportovkyní	sportovkyně	k1gFnSc7	sportovkyně
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dokázal	dokázat	k5eAaPmAgMnS	dokázat
získat	získat	k5eAaPmF	získat
olympijskou	olympijský	k2eAgFnSc4d1	olympijská
medaili	medaile	k1gFnSc4	medaile
na	na	k7c6	na
LOH	LOH	kA	LOH
i	i	k8xC	i
ZOH	ZOH	kA	ZOH
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
<s>
ZOH	ZOH	kA	ZOH
1992	[number]	k4	1992
Albertville	Albertville	k1gNnSc2	Albertville
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc2	Francie
<g/>
)	)	kIx)	)
Poslední	poslední	k2eAgMnSc1d1	poslední
ZOH	ZOH	kA	ZOH
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgInP	konat
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
jako	jako	k8xC	jako
hry	hra	k1gFnSc2	hra
letní	letní	k2eAgFnSc2d1	letní
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
programu	program	k1gInSc6	program
her	hra	k1gFnPc2	hra
poprvé	poprvé	k6eAd1	poprvé
závody	závod	k1gInPc1	závod
v	v	k7c4	v
short	short	k1gInSc4	short
tracku	track	k1gInSc2	track
a	a	k8xC	a
akrobatickém	akrobatický	k2eAgNnSc6d1	akrobatické
lyžování	lyžování	k1gNnSc6	lyžování
<g/>
.	.	kIx.	.
</s>
<s>
Norští	norský	k2eAgMnPc1d1	norský
lyžaři	lyžař	k1gMnPc1	lyžař
Bjø	Bjø	k1gMnSc1	Bjø
Dæ	Dæ	k1gMnSc1	Dæ
a	a	k8xC	a
Vegard	Vegard	k1gMnSc1	Vegard
Ulvang	Ulvang	k1gMnSc1	Ulvang
získali	získat	k5eAaPmAgMnP	získat
po	po	k7c6	po
3	[number]	k4	3
zlatých	zlatý	k2eAgFnPc6d1	zlatá
medailích	medaile	k1gFnPc6	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Finský	finský	k2eAgMnSc1d1	finský
skokan	skokan	k1gMnSc1	skokan
Toni	Toni	k1gMnSc1	Toni
Nieminen	Nieminen	k2eAgMnSc1d1	Nieminen
se	se	k3xPyFc4	se
v	v	k7c6	v
16	[number]	k4	16
letech	léto	k1gNnPc6	léto
stal	stát	k5eAaPmAgMnS	stát
nejmladším	mladý	k2eAgMnSc7d3	nejmladší
mužským	mužský	k2eAgMnSc7d1	mužský
olympijským	olympijský	k2eAgMnSc7d1	olympijský
vítězem	vítěz	k1gMnSc7	vítěz
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
ZOH	ZOH	kA	ZOH
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
</s>
<s>
ZOH	ZOH	kA	ZOH
1994	[number]	k4	1994
Lillehammer	Lillehammer	k1gInSc1	Lillehammer
(	(	kIx(	(
<g/>
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
)	)	kIx)	)
Domácí	domácí	k2eAgMnSc1d1	domácí
rychlobruslař	rychlobruslař	k1gMnSc1	rychlobruslař
Johann	Johann	k1gMnSc1	Johann
Olav	Olav	k1gMnSc1	Olav
Koss	Koss	k1gInSc4	Koss
získal	získat	k5eAaPmAgMnS	získat
3	[number]	k4	3
zlaté	zlatý	k2eAgFnPc4d1	zlatá
medaile	medaile	k1gFnPc4	medaile
a	a	k8xC	a
každou	každý	k3xTgFnSc4	každý
v	v	k7c6	v
novém	nový	k2eAgInSc6d1	nový
světovém	světový	k2eAgInSc6d1	světový
rekordu	rekord	k1gInSc6	rekord
<g/>
.	.	kIx.	.
</s>
<s>
Italská	italský	k2eAgFnSc1d1	italská
lyžařka	lyžařka	k1gFnSc1	lyžařka
Manuela	Manuela	k1gFnSc1	Manuela
Di	Di	k1gFnSc1	Di
Centaová	Centaová	k1gFnSc1	Centaová
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
5	[number]	k4	5
olympijských	olympijský	k2eAgFnPc2d1	olympijská
medailí	medaile	k1gFnPc2	medaile
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
</s>
<s>
ZOH	ZOH	kA	ZOH
1998	[number]	k4	1998
Nagano	Nagano	k1gNnSc1	Nagano
(	(	kIx(	(
<g/>
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
)	)	kIx)	)
Premiérové	premiérový	k2eAgNnSc1d1	premiérové
zařazení	zařazení	k1gNnSc1	zařazení
snowboardingu	snowboarding	k1gInSc2	snowboarding
na	na	k7c4	na
program	program	k1gInSc4	program
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Turnaje	turnaj	k1gInPc1	turnaj
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
nejlepší	dobrý	k2eAgMnPc1d3	nejlepší
hráči	hráč	k1gMnPc1	hráč
světa	svět	k1gInSc2	svět
včetně	včetně	k7c2	včetně
profesionálů	profesionál	k1gMnPc2	profesionál
a	a	k8xC	a
vítězem	vítěz	k1gMnSc7	vítěz
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Bjø	Bjø	k?	Bjø
Dæ	Dæ	k1gMnPc3	Dæ
získal	získat	k5eAaPmAgInS	získat
další	další	k2eAgInSc4d1	další
3	[number]	k4	3
zlaté	zlatý	k2eAgFnPc4d1	zlatá
medaile	medaile	k1gFnPc4	medaile
a	a	k8xC	a
celkem	celkem	k6eAd1	celkem
tak	tak	k9	tak
dovršil	dovršit	k5eAaPmAgMnS	dovršit
svojí	svojit	k5eAaImIp3nS	svojit
sbírku	sbírka	k1gFnSc4	sbírka
na	na	k7c4	na
12	[number]	k4	12
olympijských	olympijský	k2eAgFnPc2d1	olympijská
medailí	medaile	k1gFnPc2	medaile
-	-	kIx~	-
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
8	[number]	k4	8
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
.	.	kIx.	.
</s>
<s>
Krasobruslařka	krasobruslařka	k1gFnSc1	krasobruslařka
Tara	Tara	k1gFnSc1	Tara
Lipinská	Lipinský	k2eAgFnSc1d1	Lipinská
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
svou	svůj	k3xOyFgFnSc4	svůj
soutěž	soutěž	k1gFnSc4	soutěž
v	v	k7c6	v
15	[number]	k4	15
letech	let	k1gInPc6	let
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nejmladším	mladý	k2eAgMnSc7d3	nejmladší
olympijským	olympijský	k2eAgMnSc7d1	olympijský
vítězem	vítěz	k1gMnSc7	vítěz
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
hrdinům	hrdina	k1gMnPc3	hrdina
her	hra	k1gFnPc2	hra
se	se	k3xPyFc4	se
vedle	vedle	k7c2	vedle
fenomenálního	fenomenální	k2eAgMnSc2d1	fenomenální
Dähliho	Dähli	k1gMnSc2	Dähli
z	z	k7c2	z
Norska	Norsko	k1gNnSc2	Norsko
zařadili	zařadit	k5eAaPmAgMnP	zařadit
zejména	zejména	k9	zejména
bežkyně	bežkyně	k1gFnPc4	bežkyně
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
Larisa	Larisa	k1gFnSc1	Larisa
Lazutinová	Lazutinová	k1gFnSc1	Lazutinová
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
sáňkař	sáňkař	k1gMnSc1	sáňkař
Georg	Georg	k1gMnSc1	Georg
Hackl	Hackl	k1gInSc4	Hackl
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
třetí	třetí	k4xOgFnSc4	třetí
zlatou	zlatý	k2eAgFnSc4d1	zlatá
olympijskou	olympijský	k2eAgFnSc4d1	olympijská
medaili	medaile	k1gFnSc4	medaile
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
</s>
<s>
ZOH	ZOH	kA	ZOH
2002	[number]	k4	2002
Salt	salto	k1gNnPc2	salto
Lake	Lak	k1gFnSc2	Lak
City	city	k1gNnSc1	city
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
Na	na	k7c4	na
program	program	k1gInSc4	program
her	hra	k1gFnPc2	hra
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
skeleton	skeleton	k1gInSc1	skeleton
a	a	k8xC	a
nově	nově	k6eAd1	nově
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
závody	závod	k1gInPc1	závod
v	v	k7c6	v
jízdě	jízda	k1gFnSc6	jízda
na	na	k7c6	na
bobech	bob	k1gInPc6	bob
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Norský	norský	k2eAgMnSc1d1	norský
biatlonista	biatlonista	k1gMnSc1	biatlonista
Ole	Ola	k1gFnSc6	Ola
Einar	Einar	k1gMnSc1	Einar
Bjø	Bjø	k1gMnSc1	Bjø
získal	získat	k5eAaPmAgMnS	získat
všechny	všechen	k3xTgFnPc4	všechen
4	[number]	k4	4
možné	možný	k2eAgFnPc1d1	možná
zlaté	zlatý	k2eAgFnPc1d1	zlatá
medaile	medaile	k1gFnPc1	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Finský	finský	k2eAgMnSc1d1	finský
sdruženář	sdruženář	k1gMnSc1	sdruženář
Samppa	Sampp	k1gMnSc4	Sampp
Lajunen	Lajunna	k1gFnPc2	Lajunna
zase	zase	k9	zase
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
všechny	všechen	k3xTgFnPc4	všechen
3	[number]	k4	3
sdruženářské	sdruženářský	k2eAgInPc1d1	sdruženářský
závody	závod	k1gInPc1	závod
<g/>
.	.	kIx.	.
3	[number]	k4	3
zlaté	zlatý	k2eAgFnPc4d1	zlatá
medaile	medaile	k1gFnPc4	medaile
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
také	také	k9	také
chorvatská	chorvatský	k2eAgFnSc1d1	chorvatská
lyžařka	lyžařka	k1gFnSc1	lyžařka
Janica	Janica	k1gFnSc1	Janica
Kostelićová	Kostelićová	k1gFnSc1	Kostelićová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
přidala	přidat	k5eAaPmAgFnS	přidat
ještě	ještě	k6eAd1	ještě
jednu	jeden	k4xCgFnSc4	jeden
stříbrnou	stříbrná	k1gFnSc4	stříbrná
<g/>
.	.	kIx.	.
</s>
<s>
Stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
medaili	medaile	k1gFnSc4	medaile
v	v	k7c6	v
jízdě	jízda	k1gFnSc6	jízda
na	na	k7c6	na
saních	saně	k1gFnPc6	saně
získal	získat	k5eAaPmAgInS	získat
Georg	Georg	k1gInSc1	Georg
Hackl	Hackl	k1gInSc1	Hackl
a	a	k8xC	a
stal	stát	k5eAaPmAgInS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
prvním	první	k4xOgMnSc7	první
sportovcem	sportovec	k1gMnSc7	sportovec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
má	mít	k5eAaImIp3nS	mít
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
soutěže	soutěž	k1gFnSc2	soutěž
medaili	medaile	k1gFnSc4	medaile
z	z	k7c2	z
5	[number]	k4	5
po	po	k7c4	po
sobě	se	k3xPyFc3	se
jdoucích	jdoucí	k2eAgNnPc2d1	jdoucí
ZOH	ZOH	kA	ZOH
<g/>
.	.	kIx.	.
</s>
<s>
Yang	Yang	k1gMnSc1	Yang
Yang	Yang	k1gMnSc1	Yang
v	v	k7c4	v
short	short	k1gInSc4	short
tracku	track	k1gInSc2	track
vybojoval	vybojovat	k5eAaPmAgMnS	vybojovat
první	první	k4xOgNnSc4	první
zlato	zlato	k1gNnSc4	zlato
ze	z	k7c2	z
ZOH	ZOH	kA	ZOH
pro	pro	k7c4	pro
Čínu	Čína	k1gFnSc4	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Aleš	Aleš	k1gMnSc1	Aleš
Valenta	Valenta	k1gMnSc1	Valenta
skočil	skočit	k5eAaPmAgMnS	skočit
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
trojité	trojitý	k2eAgNnSc4d1	trojité
salto	salto	k1gNnSc4	salto
s	s	k7c7	s
pěti	pět	k4xCc7	pět
vruty	vrut	k1gInPc7	vrut
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
olympijským	olympijský	k2eAgMnSc7d1	olympijský
vítězem	vítěz	k1gMnSc7	vítěz
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
</s>
<s>
ZOH	ZOH	kA	ZOH
2006	[number]	k4	2006
Turín	Turín	k1gInSc1	Turín
(	(	kIx(	(
<g/>
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
)	)	kIx)	)
Na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
získala	získat	k5eAaPmAgFnS	získat
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
bronz	bronz	k1gInSc4	bronz
<g/>
.	.	kIx.	.
</s>
<s>
Kateřina	Kateřina	k1gFnSc1	Kateřina
Neumannová	Neumannová	k1gFnSc1	Neumannová
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
-	-	kIx~	-
30	[number]	k4	30
km	km	kA	km
volně	volně	k6eAd1	volně
s	s	k7c7	s
časem	čas	k1gInSc7	čas
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
22	[number]	k4	22
<g/>
:	:	kIx,	:
<g/>
25.4	[number]	k4	25.4
a	a	k8xC	a
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
medaili	medaile	k1gFnSc4	medaile
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
-	-	kIx~	-
15	[number]	k4	15
km	km	kA	km
kombinace	kombinace	k1gFnSc1	kombinace
s	s	k7c7	s
časem	čas	k1gInSc7	čas
42	[number]	k4	42
<g/>
:	:	kIx,	:
<g/>
50,6	[number]	k4	50,6
<g/>
.	.	kIx.	.
</s>
<s>
Stříbro	stříbro	k1gNnSc4	stříbro
získal	získat	k5eAaPmAgMnS	získat
ještě	ještě	k9	ještě
Lukáš	Lukáš	k1gMnSc1	Lukáš
Bauer	Bauer	k1gMnSc1	Bauer
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
-	-	kIx~	-
15	[number]	k4	15
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
s	s	k7c7	s
časem	čas	k1gInSc7	čas
38	[number]	k4	38
<g/>
:	:	kIx,	:
<g/>
15.8	[number]	k4	15.8
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
medailí	medaile	k1gFnPc2	medaile
získali	získat	k5eAaPmAgMnP	získat
němečtí	německý	k2eAgMnPc1d1	německý
sportovci	sportovec	k1gMnPc1	sportovec
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
ZOH	ZOH	kA	ZOH
2010	[number]	k4	2010
Vancouver	Vancouver	k1gInSc1	Vancouver
(	(	kIx(	(
<g/>
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
)	)	kIx)	)
Martina	Martina	k1gFnSc1	Martina
Sáblíková	Sáblíková	k1gFnSc1	Sáblíková
překvapivě	překvapivě	k6eAd1	překvapivě
triumfovala	triumfovat	k5eAaBmAgFnS	triumfovat
na	na	k7c6	na
kratší	krátký	k2eAgFnSc6d2	kratší
trati	trať	k1gFnSc6	trať
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
3000	[number]	k4	3000
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stupních	stupeň	k1gInPc6	stupeň
vítězů	vítěz	k1gMnPc2	vítěz
pak	pak	k6eAd1	pak
stanula	stanout	k5eAaPmAgFnS	stanout
Sáblíková	Sáblíková	k1gFnSc1	Sáblíková
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
her	hra	k1gFnPc2	hra
ještě	ještě	k6eAd1	ještě
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
rychlobruslařské	rychlobruslařský	k2eAgNnSc4d1	rychlobruslařské
umění	umění	k1gNnSc4	umění
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
i	i	k9	i
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
parádní	parádní	k2eAgFnSc6d1	parádní
disciplíně	disciplína	k1gFnSc6	disciplína
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
5000	[number]	k4	5000
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Dobruslila	Dobruslit	k5eAaPmAgFnS	Dobruslit
si	se	k3xPyFc3	se
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
pro	pro	k7c4	pro
druhé	druhý	k4xOgNnSc4	druhý
zlato	zlato	k1gNnSc4	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
medaile	medaile	k1gFnPc1	medaile
pro	pro	k7c4	pro
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
pocházely	pocházet	k5eAaImAgFnP	pocházet
z	z	k7c2	z
běhu	běh	k1gInSc2	běh
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
<g/>
.	.	kIx.	.
</s>
<s>
Lukáš	Lukáš	k1gMnSc1	Lukáš
Bauer	Bauer	k1gMnSc1	Bauer
vybojoval	vybojovat	k5eAaPmAgInS	vybojovat
bronz	bronz	k1gInSc4	bronz
strhujícím	strhující	k2eAgInSc7d1	strhující
finišem	finiš	k1gInSc7	finiš
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
15	[number]	k4	15
kilometrů	kilometr	k1gInPc2	kilometr
volnou	volný	k2eAgFnSc7d1	volná
technikou	technika	k1gFnSc7	technika
a	a	k8xC	a
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
i	i	k9	i
české	český	k2eAgNnSc1d1	české
kvarteto	kvarteto	k1gNnSc1	kvarteto
běžců	běžec	k1gMnPc2	běžec
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
Martin	Martin	k1gMnSc1	Martin
Jakš	Jakš	k1gMnSc1	Jakš
<g/>
,	,	kIx,	,
Lukáš	Lukáš	k1gMnSc1	Lukáš
Bauer	Bauer	k1gMnSc1	Bauer
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Magál	Magál	k1gMnSc1	Magál
a	a	k8xC	a
Martin	Martin	k1gMnSc1	Martin
Koukal	Koukal	k1gMnSc1	Koukal
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
štafet	štafeta	k1gFnPc2	štafeta
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
s	s	k7c7	s
bronzem	bronz	k1gInSc7	bronz
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
domů	dům	k1gInPc2	dům
i	i	k9	i
slalomářka	slalomářka	k1gFnSc1	slalomářka
Šárka	Šárka	k1gFnSc1	Šárka
Záhrobská	Záhrobský	k2eAgFnSc1d1	Záhrobská
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
olympijskou	olympijský	k2eAgFnSc4d1	olympijská
medaili	medaile	k1gFnSc4	medaile
za	za	k7c4	za
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
slalomu	slalom	k1gInSc6	slalom
si	se	k3xPyFc3	se
dojela	dojet	k5eAaPmAgFnS	dojet
i	i	k8xC	i
přes	přes	k7c4	přes
špatné	špatný	k2eAgFnPc4d1	špatná
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
sjezdové	sjezdový	k2eAgNnSc1d1	sjezdové
lyžování	lyžování	k1gNnSc1	lyžování
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
z	z	k7c2	z
olympijské	olympijský	k2eAgFnSc2d1	olympijská
medaile	medaile	k1gFnSc2	medaile
radovalo	radovat	k5eAaImAgNnS	radovat
po	po	k7c6	po
26	[number]	k4	26
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
naposledy	naposledy	k6eAd1	naposledy
získala	získat	k5eAaPmAgFnS	získat
bronz	bronz	k1gInSc4	bronz
ve	v	k7c6	v
sjezdu	sjezd	k1gInSc6	sjezd
v	v	k7c6	v
Sarajevu	Sarajevo	k1gNnSc6	Sarajevo
Olga	Olga	k1gFnSc1	Olga
Charvátová-Křížová	Charvátová-Křížový	k2eAgFnSc1d1	Charvátová-Křížová
<g/>
.	.	kIx.	.
</s>
<s>
Velkým	velký	k2eAgNnSc7d1	velké
zklamáním	zklamání	k1gNnSc7	zklamání
pro	pro	k7c4	pro
Čechy	Čech	k1gMnPc4	Čech
bylo	být	k5eAaImAgNnS	být
čtvrtfinálové	čtvrtfinálový	k2eAgNnSc4d1	čtvrtfinálové
vyřazení	vyřazení	k1gNnSc4	vyřazení
českých	český	k2eAgMnPc2d1	český
hokejistů	hokejista	k1gMnPc2	hokejista
<g/>
.	.	kIx.	.
</s>
<s>
Sen	sen	k1gInSc1	sen
o	o	k7c6	o
úspěchu	úspěch	k1gInSc6	úspěch
podobného	podobný	k2eAgNnSc2d1	podobné
ražení	ražení	k1gNnSc2	ražení
jako	jako	k8xS	jako
v	v	k7c6	v
Naganu	Nagano	k1gNnSc6	Nagano
vzal	vzít	k5eAaPmAgMnS	vzít
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
<g/>
,	,	kIx,	,
když	když	k8xS	když
Češi	Čech	k1gMnPc1	Čech
prohráli	prohrát	k5eAaPmAgMnP	prohrát
s	s	k7c7	s
Finskem	Finsko	k1gNnSc7	Finsko
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
