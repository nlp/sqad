<p>
<s>
Článek	článek	k1gInSc4	článek
česká	český	k2eAgNnPc4d1	české
slovesa	sloveso	k1gNnPc4	sloveso
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
slovesech	sloveso	k1gNnPc6	sloveso
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Sloveso	sloveso	k1gNnSc1	sloveso
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
větě	věta	k1gFnSc6	věta
zpravidla	zpravidla	k6eAd1	zpravidla
funkci	funkce	k1gFnSc4	funkce
přísudku	přísudek	k1gInSc2	přísudek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Časování	časování	k1gNnSc1	časování
(	(	kIx(	(
<g/>
konjugace	konjugace	k1gFnSc1	konjugace
<g/>
,	,	kIx,	,
ohýbání	ohýbání	k1gNnSc1	ohýbání
sloves	sloveso	k1gNnPc2	sloveso
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
především	především	k9	především
pomocí	pomocí	k7c2	pomocí
speciálních	speciální	k2eAgFnPc2d1	speciální
koncovek	koncovka	k1gFnPc2	koncovka
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
i	i	k9	i
hláskovou	hláskový	k2eAgFnSc7d1	hlásková
změnou	změna	k1gFnSc7	změna
kmene	kmen	k1gInSc2	kmen
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Časování	časování	k1gNnSc1	časování
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
osoby	osoba	k1gFnSc2	osoba
<g/>
,	,	kIx,	,
čísla	číslo	k1gNnSc2	číslo
<g/>
,	,	kIx,	,
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
vidu	vid	k1gInSc2	vid
<g/>
,	,	kIx,	,
rodu	rod	k1gInSc2	rod
a	a	k8xC	a
způsobu	způsob	k1gInSc2	způsob
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
k	k	k7c3	k
ději	děj	k1gInSc3	děj
či	či	k8xC	či
stavu	stav	k1gInSc3	stav
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
sloveso	sloveso	k1gNnSc4	sloveso
popisuje	popisovat	k5eAaImIp3nS	popisovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozeznáváme	rozeznávat	k5eAaImIp1nP	rozeznávat
slovesné	slovesný	k2eAgInPc1d1	slovesný
tvary	tvar	k1gInPc1	tvar
jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
(	(	kIx(	(
<g/>
např.	např.	kA	např.
dělat	dělat	k5eAaImF	dělat
<g/>
,	,	kIx,	,
dělám	dělat	k5eAaImIp1nS	dělat
<g/>
)	)	kIx)	)
a	a	k8xC	a
složené	složený	k2eAgInPc1d1	složený
(	(	kIx(	(
<g/>
např.	např.	kA	např.
dělal	dělat	k5eAaImAgMnS	dělat
jsem	být	k5eAaImIp1nS	být
<g/>
,	,	kIx,	,
budu	být	k5eAaImBp1nS	být
dělat	dělat	k5eAaImF	dělat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozdělení	rozdělení	k1gNnSc1	rozdělení
sloves	sloveso	k1gNnPc2	sloveso
==	==	k?	==
</s>
</p>
<p>
<s>
Slovesa	sloveso	k1gNnPc4	sloveso
dělíme	dělit	k5eAaImIp1nP	dělit
na	na	k7c4	na
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
plnovýznamová	plnovýznamový	k2eAgFnSc1d1	plnovýznamová
–	–	k?	–
mají	mít	k5eAaImIp3nP	mít
samostatný	samostatný	k2eAgInSc4d1	samostatný
věcný	věcný	k2eAgInSc4d1	věcný
význam	význam	k1gInSc4	význam
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
akční	akční	k2eAgMnSc1d1	akční
(	(	kIx(	(
<g/>
činnostní	činnostní	k2eAgInSc1d1	činnostní
<g/>
)	)	kIx)	)
–	–	k?	–
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
vycházející	vycházející	k2eAgFnPc1d1	vycházející
od	od	k7c2	od
nějakého	nějaký	k3yIgInSc2	nějaký
činitele	činitel	k1gInSc2	činitel
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
předmětová	předmětový	k2eAgFnSc1d1	předmětová
–	–	k?	–
váží	vážit	k5eAaImIp3nS	vážit
k	k	k7c3	k
sobě	se	k3xPyFc3	se
předmět	předmět	k1gInSc4	předmět
ustálenou	ustálený	k2eAgFnSc7d1	ustálená
vazbou	vazba	k1gFnSc7	vazba
(	(	kIx(	(
<g/>
předložkově	předložkově	k6eAd1	předložkově
<g/>
,	,	kIx,	,
bezpředložkově	bezpředložkově	k6eAd1	bezpředložkově
<g/>
,	,	kIx,	,
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
7	[number]	k4	7
<g/>
.	.	kIx.	.
pádě	pád	k1gInSc6	pád
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
psát	psát	k5eAaImF	psát
dopis	dopis	k1gInSc4	dopis
<g/>
,	,	kIx,	,
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
cíle	cíl	k1gInPc4	cíl
<g/>
,	,	kIx,	,
věnovat	věnovat	k5eAaPmF	věnovat
synovi	syn	k1gMnSc3	syn
peníze	peníz	k1gInPc4	peníz
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
bezpředmětová	bezpředmětový	k2eAgFnSc1d1	bezpředmětový
<g/>
:	:	kIx,	:
jít	jít	k5eAaImF	jít
<g/>
,	,	kIx,	,
slzet	slzet	k5eAaImF	slzet
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
zvratná	zvratný	k2eAgFnSc1d1	zvratná
–	–	k?	–
pojí	pojit	k5eAaImIp3nS	pojit
se	se	k3xPyFc4	se
s	s	k7c7	s
předmětem	předmět	k1gInSc7	předmět
zvratného	zvratný	k2eAgNnSc2d1	zvratné
(	(	kIx(	(
<g/>
reflexivního	reflexivní	k2eAgNnSc2d1	reflexivní
<g/>
)	)	kIx)	)
zájmena	zájmeno	k1gNnSc2	zájmeno
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
si	se	k3xPyFc3	se
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
4	[number]	k4	4
<g/>
.	.	kIx.	.
pádě	pád	k1gInSc6	pád
(	(	kIx(	(
<g/>
se	se	k3xPyFc4	se
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
myslet	myslet	k5eAaImF	myslet
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
posadit	posadit	k5eAaPmF	posadit
se	se	k3xPyFc4	se
<g/>
;	;	kIx,	;
některá	některý	k3yIgNnPc4	některý
slovesa	sloveso	k1gNnPc4	sloveso
nelze	lze	k6eNd1	lze
bez	bez	k7c2	bez
zvratného	zvratný	k2eAgNnSc2d1	zvratné
zájmena	zájmeno	k1gNnSc2	zájmeno
použít	použít	k5eAaPmF	použít
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
reflexiva	reflexivum	k1gNnSc2	reflexivum
tantum	tantum	k1gNnSc4	tantum
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
bát	bát	k5eAaImF	bát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
umínit	umínit	k5eAaPmF	umínit
si	se	k3xPyFc3	se
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
stavová	stavový	k2eAgFnSc1d1	stavová
–	–	k?	–
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
stav	stav	k1gInSc4	stav
nebo	nebo	k8xC	nebo
změnu	změna	k1gFnSc4	změna
stavu	stav	k1gInSc2	stav
<g/>
:	:	kIx,	:
stárnout	stárnout	k5eAaImF	stárnout
<g/>
,	,	kIx,	,
hořknout	hořknout	k5eAaImF	hořknout
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
pomocná	pomocný	k2eAgFnSc1d1	pomocná
–	–	k?	–
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
významovým	významový	k2eAgNnSc7d1	významové
slovesem	sloveso	k1gNnSc7	sloveso
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
vlastní	vlastnit	k5eAaImIp3nP	vlastnit
pomocná	pomocný	k2eAgNnPc1d1	pomocné
slovesa	sloveso	k1gNnPc1	sloveso
–	–	k?	–
být	být	k5eAaImF	být
<g/>
,	,	kIx,	,
bývat	bývat	k5eAaImF	bývat
<g/>
,	,	kIx,	,
mít	mít	k5eAaImF	mít
<g/>
,	,	kIx,	,
mívat	mívat	k5eAaImF	mívat
<g/>
:	:	kIx,	:
byl	být	k5eAaImAgMnS	být
bych	by	kYmCp1nS	by
zůstal	zůstat	k5eAaPmAgMnS	zůstat
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
sponová	sponový	k2eAgFnSc1d1	sponová
<g/>
:	:	kIx,	:
být	být	k5eAaImF	být
<g/>
,	,	kIx,	,
bývat	bývat	k5eAaImF	bývat
<g/>
,	,	kIx,	,
stát	stát	k5eAaPmF	stát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
stávat	stávat	k5eAaImF	stávat
se	se	k3xPyFc4	se
<g/>
:	:	kIx,	:
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
králem	král	k1gMnSc7	král
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
modální	modální	k2eAgInSc1d1	modální
–	–	k?	–
pojí	pojíst	k5eAaPmIp3nS	pojíst
se	se	k3xPyFc4	se
s	s	k7c7	s
infinitivem	infinitiv	k1gInSc7	infinitiv
významového	významový	k2eAgNnSc2d1	významové
slovesa	sloveso	k1gNnSc2	sloveso
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
vlastní	vlastnit	k5eAaImIp3nP	vlastnit
modální	modální	k2eAgNnPc4d1	modální
slovesa	sloveso	k1gNnPc4	sloveso
<g/>
:	:	kIx,	:
chtít	chtít	k5eAaImF	chtít
<g/>
,	,	kIx,	,
mít	mít	k5eAaImF	mít
(	(	kIx(	(
<g/>
povinnost	povinnost	k1gFnSc4	povinnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
moci	moct	k5eAaImF	moct
<g/>
,	,	kIx,	,
muset	muset	k5eAaImF	muset
<g/>
,	,	kIx,	,
smět	smět	k5eAaImF	smět
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
jiná	jiný	k2eAgNnPc1d1	jiné
slovesa	sloveso	k1gNnPc1	sloveso
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
modální	modální	k2eAgInSc4d1	modální
význam	význam	k1gInSc4	význam
<g/>
:	:	kIx,	:
hodlat	hodlat	k5eAaImF	hodlat
<g/>
,	,	kIx,	,
umět	umět	k5eAaImF	umět
<g/>
,	,	kIx,	,
dovést	dovést	k5eAaPmF	dovést
<g/>
,	,	kIx,	,
dokázat	dokázat	k5eAaPmF	dokázat
<g/>
,	,	kIx,	,
troufnout	troufnout	k5eAaPmF	troufnout
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
dát	dát	k5eAaPmF	dát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
nechat	nechat	k5eAaPmF	nechat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
aj.	aj.	kA	aj.
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
fázová	fázový	k2eAgFnSc1d1	fázová
–	–	k?	–
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
fáze	fáze	k1gFnPc4	fáze
děje	děj	k1gInSc2	děj
významového	významový	k2eAgNnSc2d1	významové
slovesa	sloveso	k1gNnSc2	sloveso
<g/>
:	:	kIx,	:
začít	začít	k5eAaPmF	začít
<g/>
,	,	kIx,	,
začínat	začínat	k5eAaImF	začínat
<g/>
,	,	kIx,	,
zůstat	zůstat	k5eAaPmF	zůstat
<g/>
,	,	kIx,	,
zůstávat	zůstávat	k5eAaImF	zůstávat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Slovesné	slovesný	k2eAgInPc1d1	slovesný
tvary	tvar	k1gInPc1	tvar
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Infinitiv	infinitiv	k1gInSc4	infinitiv
===	===	k?	===
</s>
</p>
<p>
<s>
Infinitiv	infinitiv	k1gInSc1	infinitiv
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
koncovkou	koncovka	k1gFnSc7	koncovka
-t	-t	k?	-t
<g/>
,	,	kIx,	,
knižně	knižně	k6eAd1	knižně
též	též	k9	též
-ti	i	k?	-ti
<g/>
;	;	kIx,	;
u	u	k7c2	u
některých	některý	k3yIgNnPc2	některý
sloves	sloveso	k1gNnPc2	sloveso
-ct	t	k?	-ct
(	(	kIx(	(
<g/>
-ci	i	k?	-ci
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
být	být	k5eAaImF	být
<g/>
/	/	kIx~	/
<g/>
býti	být	k5eAaImF	být
<g/>
,	,	kIx,	,
jít	jít	k5eAaImF	jít
<g/>
/	/	kIx~	/
<g/>
jíti	jít	k5eAaImF	jít
<g/>
,	,	kIx,	,
péct	péct	k5eAaImF	péct
<g/>
/	/	kIx~	/
<g/>
péci	péct	k5eAaImF	péct
</s>
</p>
<p>
<s>
===	===	k?	===
Příčestí	příčestí	k1gNnSc2	příčestí
===	===	k?	===
</s>
</p>
<p>
<s>
Příčestí	příčestí	k1gNnSc1	příčestí
se	se	k3xPyFc4	se
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
tvoření	tvoření	k1gNnSc3	tvoření
minulého	minulý	k2eAgInSc2d1	minulý
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
podmiňovacího	podmiňovací	k2eAgInSc2d1	podmiňovací
způsobu	způsob	k1gInSc2	způsob
a	a	k8xC	a
trpného	trpný	k2eAgInSc2d1	trpný
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Formálně	formálně	k6eAd1	formálně
se	se	k3xPyFc4	se
shodují	shodovat	k5eAaImIp3nP	shodovat
se	s	k7c7	s
jmennými	jmenný	k2eAgFnPc7d1	jmenná
(	(	kIx(	(
<g/>
krátkými	krátká	k1gFnPc7	krátká
<g/>
)	)	kIx)	)
tvary	tvar	k1gInPc1	tvar
přídavných	přídavný	k2eAgNnPc2d1	přídavné
jmen	jméno	k1gNnPc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
slovesných	slovesný	k2eAgFnPc2d1	slovesná
kategorií	kategorie	k1gFnPc2	kategorie
i	i	k9	i
jmenný	jmenný	k2eAgInSc4d1	jmenný
rod	rod	k1gInSc4	rod
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
shodovat	shodovat	k5eAaImF	shodovat
s	s	k7c7	s
rodem	rod	k1gInSc7	rod
podmětu	podmět	k1gInSc2	podmět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Příčestí	příčestí	k1gNnSc1	příčestí
činné	činný	k2eAgFnPc1d1	činná
====	====	k?	====
</s>
</p>
<p>
<s>
Příčestí	příčestí	k1gNnSc1	příčestí
činné	činný	k2eAgInPc1d1	činný
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
označované	označovaný	k2eAgFnPc1d1	označovaná
jako	jako	k8xC	jako
minulé	minulý	k2eAgFnPc1d1	minulá
nebo	nebo	k8xC	nebo
též	též	k9	též
l-ové	lvý	k2eAgFnPc1d1	l-ový
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
tvoření	tvoření	k1gNnSc3	tvoření
minulého	minulý	k2eAgInSc2d1	minulý
času	čas	k1gInSc2	čas
a	a	k8xC	a
podmiňovacího	podmiňovací	k2eAgInSc2d1	podmiňovací
způsobu	způsob	k1gInSc2	způsob
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Příčestí	příčestí	k1gNnSc4	příčestí
trpné	trpný	k2eAgFnSc2d1	trpná
====	====	k?	====
</s>
</p>
<p>
<s>
Příčestí	příčestí	k1gNnSc1	příčestí
trpné	trpný	k2eAgNnSc1d1	trpné
<g/>
,	,	kIx,	,
označované	označovaný	k2eAgNnSc1d1	označované
také	také	k9	také
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
n	n	k0	n
<g/>
/	/	kIx~	/
<g/>
t-ové	tvý	k2eAgInPc4d1	t-ový
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
opisnému	opisný	k2eAgNnSc3d1	opisné
tvoření	tvoření	k1gNnSc3	tvoření
trpného	trpný	k2eAgInSc2d1	trpný
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
se	s	k7c7	s
dvojím	dvojí	k4xRgInSc7	dvojí
typem	typ	k1gInSc7	typ
zakončení	zakončení	k1gNnSc2	zakončení
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Od	od	k7c2	od
příčestí	příčestí	k1gNnSc2	příčestí
trpného	trpný	k2eAgNnSc2d1	trpné
se	se	k3xPyFc4	se
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
podstatné	podstatný	k2eAgNnSc1d1	podstatné
jméno	jméno	k1gNnSc1	jméno
slovesené	slovesený	k2eAgNnSc1d1	slovesený
příponou	přípona	k1gFnSc7	přípona
-í	-í	k?	-í
<g/>
:	:	kIx,	:
volání	volání	k1gNnSc1	volání
<g/>
,	,	kIx,	,
stravování	stravování	k1gNnSc1	stravování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Přechodníky	přechodník	k1gInPc4	přechodník
===	===	k?	===
</s>
</p>
<p>
<s>
Přechodník	přechodník	k1gInSc1	přechodník
(	(	kIx(	(
<g/>
transgresiv	transgresiv	k1gInSc1	transgresiv
<g/>
)	)	kIx)	)
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
děj	děj	k1gInSc1	děj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
buď	buď	k8xC	buď
předcházel	předcházet	k5eAaImAgInS	předcházet
jinému	jiný	k2eAgMnSc3d1	jiný
ději	děj	k1gInPc7	děj
ve	v	k7c6	v
větě	věta	k1gFnSc6	věta
(	(	kIx(	(
<g/>
přechodník	přechodník	k1gInSc4	přechodník
minulý	minulý	k2eAgInSc4d1	minulý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
probíhal	probíhat	k5eAaImAgInS	probíhat
<g/>
/	/	kIx~	/
<g/>
probíhá	probíhat	k5eAaImIp3nS	probíhat
<g/>
/	/	kIx~	/
<g/>
bude	být	k5eAaImBp3nS	být
probíhat	probíhat	k5eAaImF	probíhat
současně	současně	k6eAd1	současně
s	s	k7c7	s
jiným	jiný	k2eAgInSc7d1	jiný
dějem	děj	k1gInSc7	děj
(	(	kIx(	(
<g/>
přechodník	přechodník	k1gInSc1	přechodník
přítomný	přítomný	k2eAgInSc1d1	přítomný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
přechodníků	přechodník	k1gInPc2	přechodník
lze	lze	k6eAd1	lze
nahrazovat	nahrazovat	k5eAaImF	nahrazovat
vedlejší	vedlejší	k2eAgFnPc4d1	vedlejší
věty	věta	k1gFnPc4	věta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
lze	lze	k6eAd1	lze
rozlišit	rozlišit	k5eAaPmF	rozlišit
tři	tři	k4xCgInPc1	tři
druhy	druh	k1gInPc1	druh
přechodníků	přechodník	k1gInPc2	přechodník
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Přechodník	přechodník	k1gInSc1	přechodník
přítomný	přítomný	k2eAgInSc1d1	přítomný
od	od	k7c2	od
nedokonavých	dokonavý	k2eNgNnPc2d1	nedokonavé
sloves	sloveso	k1gNnPc2	sloveso
-	-	kIx~	-
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
současnost	současnost	k1gFnSc4	současnost
dvou	dva	k4xCgInPc2	dva
dějů	děj	k1gInPc2	děj
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
každém	každý	k3xTgInSc6	každý
časovém	časový	k2eAgInSc6d1	časový
stupni	stupeň	k1gInSc6	stupeň
<g/>
:	:	kIx,	:
Loupaje	loupat	k5eAaImSgMnS	loupat
brambory	brambor	k1gInPc4	brambor
sleduje	sledovat	k5eAaImIp3nS	sledovat
televizi	televize	k1gFnSc4	televize
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
loupe	loupat	k5eAaImIp3nS	loupat
brambory	brambor	k1gInPc4	brambor
a	a	k8xC	a
sleduje	sledovat	k5eAaImIp3nS	sledovat
televizi	televize	k1gFnSc4	televize
<g/>
.	.	kIx.	.
–	–	k?	–
Loupaje	loupat	k5eAaImSgInS	loupat
brambory	brambora	k1gFnSc2	brambora
sledoval	sledovat	k5eAaImAgMnS	sledovat
televizi	televize	k1gFnSc4	televize
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
loupal	loupat	k5eAaImAgMnS	loupat
brambory	brambora	k1gFnPc4	brambora
a	a	k8xC	a
sledoval	sledovat	k5eAaImAgMnS	sledovat
televizi	televize	k1gFnSc4	televize
<g/>
.	.	kIx.	.
–	–	k?	–
Loupaje	loupat	k5eAaImSgInS	loupat
brambory	brambor	k1gInPc4	brambor
bude	být	k5eAaImBp3nS	být
sledovat	sledovat	k5eAaImF	sledovat
televizi	televize	k1gFnSc4	televize
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
bude	být	k5eAaImBp3nS	být
loupat	loupat	k5eAaImF	loupat
brambory	brambor	k1gInPc4	brambor
a	a	k8xC	a
současně	současně	k6eAd1	současně
bude	být	k5eAaImBp3nS	být
sledovat	sledovat	k5eAaImF	sledovat
televizi	televize	k1gFnSc4	televize
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přechodník	přechodník	k1gInSc1	přechodník
přítomný	přítomný	k2eAgInSc1d1	přítomný
od	od	k7c2	od
dokonavých	dokonavý	k2eAgNnPc2d1	dokonavé
sloves	sloveso	k1gNnPc2	sloveso
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
též	též	k9	též
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
přechodník	přechodník	k1gInSc1	přechodník
budoucí	budoucí	k2eAgInSc1d1	budoucí
<g/>
)	)	kIx)	)
-	-	kIx~	-
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
předčasnost	předčasnost	k1gFnSc1	předčasnost
budoucího	budoucí	k2eAgNnSc2d1	budoucí
nebo	nebo	k8xC	nebo
časově	časově	k6eAd1	časově
nezařazeného	zařazený	k2eNgInSc2d1	nezařazený
děje	děj	k1gInSc2	děj
<g/>
:	:	kIx,	:
Oloupaje	Oloupaje	k1gFnSc1	Oloupaje
brambory	brambora	k1gFnSc2	brambora
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
sledovat	sledovat	k5eAaImF	sledovat
televizi	televize	k1gFnSc4	televize
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Tj.	tj.	kA	tj.
Až	až	k6eAd1	až
oloupe	oloupat	k5eAaPmIp3nS	oloupat
brambory	brambor	k1gInPc4	brambor
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
sledovat	sledovat	k5eAaImF	sledovat
televizi	televize	k1gFnSc4	televize
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Přechodník	přechodník	k1gInSc1	přechodník
minulý	minulý	k2eAgInSc1d1	minulý
od	od	k7c2	od
dokonavých	dokonavý	k2eAgNnPc2d1	dokonavé
sloves	sloveso	k1gNnPc2	sloveso
-	-	kIx~	-
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
předčasnost	předčasnost	k1gFnSc1	předčasnost
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Oloupav	oloupat	k5eAaPmDgInS	oloupat
brambory	brambor	k1gInPc4	brambor
<g/>
,	,	kIx,	,
sledoval	sledovat	k5eAaImAgMnS	sledovat
televizi	televize	k1gFnSc4	televize
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Tj.	tj.	kA	tj.
Oloupal	oloupat	k5eAaPmAgMnS	oloupat
brambory	brambora	k1gFnPc4	brambora
a	a	k8xC	a
pak	pak	k6eAd1	pak
sledoval	sledovat	k5eAaImAgMnS	sledovat
televizi	televize	k1gFnSc4	televize
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Přechodník	přechodník	k1gInSc4	přechodník
minulý	minulý	k2eAgInSc4d1	minulý
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
jen	jen	k9	jen
od	od	k7c2	od
dokonavých	dokonavý	k2eAgNnPc2d1	dokonavé
sloves	sloveso	k1gNnPc2	sloveso
<g/>
.	.	kIx.	.
<g/>
Přechodníky	přechodník	k1gInPc4	přechodník
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
přechodníky	přechodník	k1gInPc1	přechodník
2	[number]	k4	2
<g/>
.	.	kIx.	.
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
češtině	čeština	k1gFnSc6	čeština
zastaralá	zastaralý	k2eAgFnSc1d1	zastaralá
forma	forma	k1gFnSc1	forma
slovesa	sloveso	k1gNnSc2	sloveso
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
jen	jen	k9	jen
ke	k	k7c3	k
zvláštním	zvláštní	k2eAgInPc3d1	zvláštní
účelům	účel	k1gInPc3	účel
např.	např.	kA	např.
v	v	k7c6	v
umělecké	umělecký	k2eAgFnSc6d1	umělecká
literatuře	literatura	k1gFnSc6	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
setkáváme	setkávat	k5eAaImIp1nP	setkávat
ve	v	k7c6	v
starší	starý	k2eAgFnSc6d2	starší
literatuře	literatura	k1gFnSc6	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
přechodníků	přechodník	k1gInPc2	přechodník
lze	lze	k6eAd1	lze
nahrazovat	nahrazovat	k5eAaImF	nahrazovat
vedlejší	vedlejší	k2eAgFnPc4d1	vedlejší
věty	věta	k1gFnPc4	věta
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
větě	věta	k1gFnSc6	věta
mají	mít	k5eAaImIp3nP	mít
zpravidla	zpravidla	k6eAd1	zpravidla
funkci	funkce	k1gFnSc4	funkce
doplňku	doplněk	k1gInSc2	doplněk
<g/>
,	,	kIx,	,
po	po	k7c6	po
obsahové	obsahový	k2eAgFnSc6d1	obsahová
stránce	stránka	k1gFnSc6	stránka
ale	ale	k8xC	ale
mají	mít	k5eAaImIp3nP	mít
platnost	platnost	k1gFnSc4	platnost
věty	věta	k1gFnSc2	věta
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
takzvané	takzvaný	k2eAgFnPc4d1	takzvaná
polovětné	polovětný	k2eAgFnPc4d1	polovětný
vazby	vazba	k1gFnPc4	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Přechodníkovou	přechodníkový	k2eAgFnSc4d1	přechodníková
konstrukci	konstrukce	k1gFnSc4	konstrukce
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
oddělit	oddělit	k5eAaPmF	oddělit
od	od	k7c2	od
zbylé	zbylý	k2eAgFnSc2d1	zbylá
části	část	k1gFnSc2	část
věty	věta	k1gFnSc2	věta
čárkou	čárka	k1gFnSc7	čárka
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
přechodník	přechodník	k1gInSc4	přechodník
rozvitý	rozvitý	k2eAgInSc4d1	rozvitý
o	o	k7c4	o
další	další	k2eAgMnPc4d1	další
členy	člen	k1gMnPc4	člen
<g/>
:	:	kIx,	:
Holčička	holčička	k1gFnSc1	holčička
<g/>
,	,	kIx,	,
usedavě	usedavě	k6eAd1	usedavě
plakajíc	plakat	k5eAaImSgNnS	plakat
<g/>
,	,	kIx,	,
hledala	hledat	k5eAaImAgFnS	hledat
ztraceného	ztracený	k2eAgMnSc4d1	ztracený
psa	pes	k1gMnSc4	pes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Tvary	tvar	k1gInPc1	tvar
přechodníků	přechodník	k1gInPc2	přechodník
====	====	k?	====
</s>
</p>
<p>
<s>
Přechodníky	přechodník	k1gInPc1	přechodník
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
jednotném	jednotný	k2eAgNnSc6d1	jednotné
čísle	číslo	k1gNnSc6	číslo
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
tvar	tvar	k1gInSc4	tvar
pro	pro	k7c4	pro
rod	rod	k1gInSc4	rod
mužský	mužský	k2eAgInSc4d1	mužský
a	a	k8xC	a
společný	společný	k2eAgInSc4d1	společný
tvar	tvar	k1gInSc4	tvar
pro	pro	k7c4	pro
rod	rod	k1gInSc4	rod
ženský	ženský	k2eAgInSc4d1	ženský
a	a	k8xC	a
střední	střední	k2eAgInSc4d1	střední
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
množném	množný	k2eAgNnSc6d1	množné
čísle	číslo	k1gNnSc6	číslo
je	být	k5eAaImIp3nS	být
stejný	stejný	k2eAgInSc1d1	stejný
tvar	tvar	k1gInSc1	tvar
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
tři	tři	k4xCgInPc4	tři
rody	rod	k1gInPc4	rod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zápor	zápor	k1gInSc4	zápor
===	===	k?	===
</s>
</p>
<p>
<s>
Zápor	zápor	k1gInSc1	zápor
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
pomocí	pomocí	k7c2	pomocí
předpony	předpona	k1gFnSc2	předpona
ne-	ne-	k?	ne-
<g/>
.	.	kIx.	.
</s>
<s>
Sloveso	sloveso	k1gNnSc1	sloveso
být	být	k5eAaImF	být
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
osobě	osoba	k1gFnSc3	osoba
jednotného	jednotný	k2eAgNnSc2d1	jednotné
čísla	číslo	k1gNnSc2	číslo
přítomného	přítomný	k2eAgInSc2d1	přítomný
času	čas	k1gInSc2	čas
tvoří	tvořit	k5eAaImIp3nS	tvořit
zápor	zápor	k1gInSc1	zápor
nepravidelně	pravidelně	k6eNd1	pravidelně
<g/>
:	:	kIx,	:
je	být	k5eAaImIp3nS	být
-	-	kIx~	-
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
budoucím	budoucí	k2eAgInSc6d1	budoucí
čase	čas	k1gInSc6	čas
nebo	nebo	k8xC	nebo
v	v	k7c6	v
trpném	trpný	k2eAgInSc6d1	trpný
rodě	rod	k1gInSc6	rod
se	se	k3xPyFc4	se
připojuje	připojovat	k5eAaImIp3nS	připojovat
k	k	k7c3	k
pomocnému	pomocný	k2eAgNnSc3d1	pomocné
slovesu	sloveso	k1gNnSc3	sloveso
být	být	k5eAaImF	být
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
nedělat	dělat	k5eNaImF	dělat
</s>
</p>
<p>
<s>
nedělám	dělat	k5eNaImIp1nS	dělat
</s>
</p>
<p>
<s>
nedělej	dělat	k5eNaImRp2nS	dělat
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
nedělal	dělat	k5eNaImAgMnS	dělat
jsem	být	k5eAaImIp1nS	být
</s>
</p>
<p>
<s>
nebudu	být	k5eNaImBp1nS	být
dělat	dělat	k5eAaImF	dělat
</s>
</p>
<p>
<s>
nedělal	dělat	k5eNaImAgMnS	dělat
bych	by	kYmCp1nS	by
</s>
</p>
<p>
<s>
byl	být	k5eAaImAgInS	být
bych	by	kYmCp1nS	by
neudělal	udělat	k5eNaPmAgMnS	udělat
nebo	nebo	k8xC	nebo
nebyl	být	k5eNaImAgMnS	být
bych	by	kYmCp1nS	by
udělal	udělat	k5eAaPmAgInS	udělat
</s>
</p>
<p>
<s>
není	být	k5eNaImIp3nS	být
dělánoV	dělánoV	k?	dělánoV
českých	český	k2eAgFnPc6d1	Česká
větách	věta	k1gFnPc6	věta
často	často	k6eAd1	často
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
kumulaci	kumulace	k1gFnSc3	kumulace
záporných	záporný	k2eAgNnPc2d1	záporné
slov	slovo	k1gNnPc2	slovo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Nic	nic	k6eAd1	nic
nemám	mít	k5eNaImIp1nS	mít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nikdy	nikdy	k6eAd1	nikdy
to	ten	k3xDgNnSc1	ten
nikomu	nikdo	k3yNnSc3	nikdo
neříkej	říkat	k5eNaImRp2nS	říkat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Shoda	shoda	k1gFnSc1	shoda
přísudku	přísudek	k1gInSc2	přísudek
s	s	k7c7	s
podmětem	podmět	k1gInSc7	podmět
==	==	k?	==
</s>
</p>
<p>
<s>
Přísudek	přísudek	k1gInSc1	přísudek
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
ve	v	k7c6	v
větě	věta	k1gFnSc6	věta
vždy	vždy	k6eAd1	vždy
ve	v	k7c6	v
shodě	shoda	k1gFnSc6	shoda
s	s	k7c7	s
podmětem	podmět	k1gInSc7	podmět
–	–	k?	–
v	v	k7c6	v
čísle	číslo	k1gNnSc6	číslo
a	a	k8xC	a
osobě	osoba	k1gFnSc3	osoba
(	(	kIx(	(
<g/>
osobní	osobní	k2eAgNnPc1d1	osobní
zájmena	zájmeno	k1gNnPc1	zájmeno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
příčestí	příčestí	k1gNnSc2	příčestí
také	také	k9	také
v	v	k7c6	v
rodě	rod	k1gInSc6	rod
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
mluvnické	mluvnický	k2eAgNnSc1d1	mluvnické
pravidlo	pravidlo	k1gNnSc1	pravidlo
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
pravopis	pravopis	k1gInSc4	pravopis
–	–	k?	–
zejména	zejména	k9	zejména
psaní	psaní	k1gNnSc1	psaní
i	i	k8xC	i
<g/>
/	/	kIx~	/
<g/>
y	y	k?	y
v	v	k7c6	v
koncovkách	koncovka	k1gFnPc6	koncovka
příčestí	příčestí	k1gNnSc2	příčestí
v	v	k7c6	v
množném	množný	k2eAgNnSc6d1	množné
čísle	číslo	k1gNnSc6	číslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Uvedený	uvedený	k2eAgInSc1d1	uvedený
příklad	příklad	k1gInSc1	příklad
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
jak	jak	k6eAd1	jak
činné	činný	k2eAgNnSc1d1	činné
(	(	kIx(	(
<g/>
byl	být	k5eAaImAgInS	být
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
trpné	trpný	k2eAgFnPc1d1	trpná
(	(	kIx(	(
<g/>
koupen	koupen	k2eAgInSc1d1	koupen
<g/>
,	,	kIx,	,
koupena	koupen	k2eAgFnSc1d1	koupena
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
příčestí	příčestí	k1gNnSc1	příčestí
<g/>
.	.	kIx.	.
</s>
<s>
Shoda	shoda	k1gFnSc1	shoda
ve	v	k7c6	v
jmenném	jmenný	k2eAgInSc6d1	jmenný
rodě	rod	k1gInSc6	rod
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
časech	čas	k1gInPc6	čas
v	v	k7c6	v
trpném	trpný	k2eAgInSc6d1	trpný
rodě	rod	k1gInSc6	rod
<g/>
,	,	kIx,	,
v	v	k7c6	v
minulém	minulý	k2eAgInSc6d1	minulý
čase	čas	k1gInSc6	čas
v	v	k7c6	v
činném	činný	k2eAgInSc6d1	činný
rodě	rod	k1gInSc6	rod
a	a	k8xC	a
u	u	k7c2	u
podmiňovacího	podmiňovací	k2eAgInSc2d1	podmiňovací
způsobu	způsob	k1gInSc2	způsob
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
podmět	podmět	k1gInSc1	podmět
složen	složit	k5eAaPmNgInS	složit
z	z	k7c2	z
podstatných	podstatný	k2eAgNnPc2d1	podstatné
jmen	jméno	k1gNnPc2	jméno
různých	různý	k2eAgInPc2d1	různý
rodů	rod	k1gInPc2	rod
<g/>
,	,	kIx,	,
rod	rod	k1gInSc1	rod
mužský	mužský	k2eAgInSc1d1	mužský
životný	životný	k2eAgInSc1d1	životný
má	mít	k5eAaImIp3nS	mít
přednost	přednost	k1gFnSc4	přednost
před	před	k7c7	před
ostatními	ostatní	k2eAgInPc7d1	ostatní
rody	rod	k1gInPc7	rod
<g/>
,	,	kIx,	,
rod	rod	k1gInSc1	rod
ženský	ženský	k2eAgInSc1d1	ženský
a	a	k8xC	a
mužský	mužský	k2eAgInSc1d1	mužský
neživotný	životný	k2eNgInSc1d1	neživotný
mají	mít	k5eAaImIp3nP	mít
přednost	přednost	k1gFnSc4	přednost
před	před	k7c7	před
rodem	rod	k1gInSc7	rod
středním	střední	k2eAgInSc7d1	střední
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
muži	muž	k1gMnPc1	muž
a	a	k8xC	a
ženy	žena	k1gFnPc1	žena
byli	být	k5eAaImAgMnP	být
</s>
</p>
<p>
<s>
kočky	kočka	k1gFnPc1	kočka
a	a	k8xC	a
koťata	kotě	k1gNnPc1	kotě
byly	být	k5eAaImAgFnP	být
</s>
</p>
<p>
<s>
my	my	k3xPp1nPc1	my
jsme	být	k5eAaImIp1nP	být
byli	být	k5eAaImAgMnP	být
(	(	kIx(	(
<g/>
my	my	k3xPp1nPc1	my
=	=	kIx~	=
my	my	k3xPp1nPc1	my
všichni	všechen	k3xTgMnPc1	všechen
<g/>
/	/	kIx~	/
<g/>
muži	muž	k1gMnPc1	muž
<g/>
)	)	kIx)	)
x	x	k?	x
my	my	k3xPp1nPc1	my
jsme	být	k5eAaImIp1nP	být
byly	být	k5eAaImAgFnP	být
(	(	kIx(	(
<g/>
my	my	k3xPp1nPc1	my
=	=	kIx~	=
ženy	žena	k1gFnPc1	žena
<g/>
)	)	kIx)	)
<g/>
Priorita	priorita	k1gFnSc1	priorita
rodů	rod	k1gInPc2	rod
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
mužský	mužský	k2eAgInSc1d1	mužský
životný	životný	k2eAgInSc1d1	životný
>	>	kIx)	>
mužský	mužský	k2eAgInSc1d1	mužský
neživotný	životný	k2eNgInSc1d1	neživotný
&	&	k?	&
ženský	ženský	k2eAgInSc4d1	ženský
>	>	kIx)	>
střední	střední	k2eAgInSc4d1	střední
</s>
</p>
<p>
<s>
==	==	k?	==
Slovesné	slovesný	k2eAgFnSc2d1	slovesná
kategorie	kategorie	k1gFnSc2	kategorie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Osoba	osoba	k1gFnSc1	osoba
===	===	k?	===
</s>
</p>
<p>
<s>
Slovesná	slovesný	k2eAgFnSc1d1	slovesná
osoba	osoba	k1gFnSc1	osoba
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
podmětem	podmět	k1gInSc7	podmět
věty	věta	k1gFnSc2	věta
mluvčí	mluvčí	k1gFnSc1	mluvčí
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
osoba	osoba	k1gFnSc1	osoba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
posluchač	posluchač	k1gMnSc1	posluchač
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
osoba	osoba	k1gFnSc1	osoba
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
někdo	někdo	k3yInSc1	někdo
jiný	jiný	k1gMnSc1	jiný
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
osoba	osoba	k1gFnSc1	osoba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
osoba	osoba	k1gFnSc1	osoba
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
také	také	k9	také
neosobní	osobní	k2eNgInSc1d1	neosobní
děj	děj	k1gInSc1	děj
(	(	kIx(	(
<g/>
mrzne	mrznout	k5eAaImIp3nS	mrznout
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
všeobecný	všeobecný	k2eAgInSc1d1	všeobecný
podmět	podmět	k1gInSc1	podmět
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
když	když	k8xS	když
do	do	k7c2	do
vrabců	vrabec	k1gMnPc2	vrabec
střelí	střelit	k5eAaPmIp3nS	střelit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Číslo	číslo	k1gNnSc4	číslo
===	===	k?	===
</s>
</p>
<p>
<s>
Číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
skloňování	skloňování	k1gNnSc2	skloňování
<g/>
,	,	kIx,	,
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
počet	počet	k1gInSc1	počet
účastníků	účastník	k1gMnPc2	účastník
děje	dít	k5eAaImIp3nS	dít
<g/>
:	:	kIx,	:
jeden	jeden	k4xCgMnSc1	jeden
-	-	kIx~	-
jednotné	jednotný	k2eAgNnSc1d1	jednotné
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
-	-	kIx~	-
množné	množný	k2eAgNnSc1d1	množné
číslo	číslo	k1gNnSc1	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
skloňování	skloňování	k1gNnSc2	skloňování
u	u	k7c2	u
slovesného	slovesný	k2eAgNnSc2d1	slovesné
čísla	číslo	k1gNnSc2	číslo
už	už	k6eAd1	už
zcela	zcela	k6eAd1	zcela
vymizelo	vymizet	k5eAaPmAgNnS	vymizet
dvojné	dvojný	k2eAgNnSc1d1	dvojné
číslo	číslo	k1gNnSc1	číslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vid	vid	k1gInSc4	vid
===	===	k?	===
</s>
</p>
<p>
<s>
Česká	český	k2eAgNnPc1d1	české
slovesa	sloveso	k1gNnPc1	sloveso
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
podle	podle	k7c2	podle
vidu	vid	k1gInSc2	vid
na	na	k7c4	na
dokonavá	dokonavý	k2eAgNnPc4d1	dokonavé
(	(	kIx(	(
<g/>
perfektiva	perfektivum	k1gNnPc4	perfektivum
<g/>
)	)	kIx)	)
a	a	k8xC	a
nedokonavá	dokonavý	k2eNgNnPc1d1	nedokonavé
(	(	kIx(	(
<g/>
imperfektiva	imperfektivum	k1gNnPc1	imperfektivum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgNnPc1	dva
slovesa	sloveso	k1gNnPc1	sloveso
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
pouze	pouze	k6eAd1	pouze
videm	vid	k1gInSc7	vid
<g/>
,	,	kIx,	,
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
tzv.	tzv.	kA	tzv.
vidové	vidový	k2eAgFnSc2d1	vidová
dvojice	dvojice	k1gFnSc2	dvojice
(	(	kIx(	(
<g/>
kopl	kopnout	k5eAaPmAgMnS	kopnout
-	-	kIx~	-
kopal	kopat	k5eAaImAgMnS	kopat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
sloves	sloveso	k1gNnPc2	sloveso
svůj	svůj	k3xOyFgInSc4	svůj
vidový	vidový	k2eAgInSc4d1	vidový
protějšek	protějšek	k1gInSc4	protějšek
nemá	mít	k5eNaImIp3nS	mít
(	(	kIx(	(
<g/>
muset	muset	k5eAaImF	muset
<g/>
,	,	kIx,	,
mít	mít	k5eAaImF	mít
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dokonavá	dokonavý	k2eAgNnPc4d1	dokonavé
slovesa	sloveso	k1gNnPc4	sloveso
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
ukončenost	ukončenost	k1gFnSc1	ukončenost
děje	děj	k1gInSc2	děj
<g/>
,	,	kIx,	,
nemohou	moct	k5eNaImIp3nP	moct
tedy	tedy	k9	tedy
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
přítomný	přítomný	k2eAgInSc4d1	přítomný
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Dokonavá	dokonavý	k2eAgNnPc1d1	dokonavé
slovesa	sloveso	k1gNnPc1	sloveso
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
tvoří	tvořit	k5eAaImIp3nP	tvořit
od	od	k7c2	od
nedokonavých	dokonavý	k2eNgFnPc2d1	nedokonavá
pomocí	pomoc	k1gFnPc2	pomoc
předpon	předpona	k1gFnPc2	předpona
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
předpony	předpona	k1gFnPc1	předpona
nemění	měnit	k5eNaImIp3nP	měnit
význam	význam	k1gInSc4	význam
slovesa	sloveso	k1gNnSc2	sloveso
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
funkci	funkce	k1gFnSc4	funkce
čistě	čistě	k6eAd1	čistě
perfektivizační	perfektivizační	k2eAgFnSc4d1	perfektivizační
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
psát	psát	k5eAaImF	psát
(	(	kIx(	(
<g/>
nedok	nedok	k1gInSc4	nedok
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
→	→	k?	→
napsat	napsat	k5eAaBmF	napsat
(	(	kIx(	(
<g/>
dok	dok	k1gInSc4	dok
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
Některá	některý	k3yIgNnPc1	některý
dokonavá	dokonavý	k2eAgNnPc1d1	dokonavé
slovesa	sloveso	k1gNnPc1	sloveso
nejsou	být	k5eNaImIp3nP	být
formálně	formálně	k6eAd1	formálně
odvozena	odvodit	k5eAaPmNgFnS	odvodit
od	od	k7c2	od
nedokonavých	dokonavý	k2eNgFnPc2d1	nedokonavá
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
brát	brát	k5eAaImF	brát
(	(	kIx(	(
<g/>
nedok	nedok	k1gInSc4	nedok
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
→	→	k?	→
vzít	vzít	k5eAaPmF	vzít
(	(	kIx(	(
<g/>
dok	dok	k1gInSc4	dok
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
Sekundárně	sekundárně	k6eAd1	sekundárně
je	být	k5eAaImIp3nS	být
též	též	k9	též
možné	možný	k2eAgNnSc1d1	možné
vytvořit	vytvořit	k5eAaPmF	vytvořit
pomocí	pomocí	k7c2	pomocí
přípony	přípona	k1gFnSc2	přípona
nedokonavé	dokonavý	k2eNgNnSc4d1	nedokonavé
sloveso	sloveso	k1gNnSc4	sloveso
z	z	k7c2	z
dokonavého	dokonavý	k2eAgNnSc2d1	dokonavé
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
předpona	předpona	k1gFnSc1	předpona
modifikuje	modifikovat	k5eAaBmIp3nS	modifikovat
význam	význam	k1gInSc4	význam
původního	původní	k2eAgNnSc2d1	původní
nedokonavého	dokonavý	k2eNgNnSc2d1	nedokonavé
slovesa	sloveso	k1gNnSc2	sloveso
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
psát	psát	k5eAaImF	psát
(	(	kIx(	(
<g/>
nedok	nedok	k1gInSc4	nedok
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
→	→	k?	→
přepsat	přepsat	k5eAaPmF	přepsat
(	(	kIx(	(
<g/>
dok	dok	k1gInSc4	dok
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
→	→	k?	→
přepisovat	přepisovat	k5eAaImF	přepisovat
(	(	kIx(	(
<g/>
nedok	nedok	k1gInSc4	nedok
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
Existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
slovesa	sloveso	k1gNnPc4	sloveso
obouvidová	obouvidový	k2eAgNnPc4d1	obouvidový
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
dokonavost	dokonavost	k1gFnSc4	dokonavost
i	i	k8xC	i
nedokonavost	nedokonavost	k1gFnSc4	nedokonavost
(	(	kIx(	(
<g/>
obětovat	obětovat	k5eAaBmF	obětovat
<g/>
,	,	kIx,	,
informovat	informovat	k5eAaBmF	informovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jiná	jiný	k2eAgNnPc1d1	jiné
hlediska	hledisko	k1gNnPc1	hledisko
===	===	k?	===
</s>
</p>
<p>
<s>
Podrobněji	podrobně	k6eAd2	podrobně
lze	lze	k6eAd1	lze
dále	daleko	k6eAd2	daleko
slovesa	sloveso	k1gNnSc2	sloveso
dělit	dělit	k5eAaImF	dělit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
podle	podle	k7c2	podle
opakování	opakování	k1gNnSc2	opakování
děje	děj	k1gInSc2	děj
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
nenásobená	násobený	k2eNgFnSc1d1	násobený
–	–	k?	–
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
jednorázově	jednorázově	k6eAd1	jednorázově
–	–	k?	–
nést	nést	k5eAaImF	nést
<g/>
,	,	kIx,	,
jít	jít	k5eAaImF	jít
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
násobená	násobený	k2eAgFnSc1d1	násobená
–	–	k?	–
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
opakuje	opakovat	k5eAaImIp3nS	opakovat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
opětovací	opětovací	k2eAgInSc1d1	opětovací
–	–	k?	–
děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
opakuje	opakovat	k5eAaImIp3nS	opakovat
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
–	–	k?	–
nosit	nosit	k5eAaImF	nosit
<g/>
,	,	kIx,	,
chodit	chodit	k5eAaImF	chodit
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
opakovací	opakovací	k2eAgInSc1d1	opakovací
–	–	k?	–
děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
mnohokrát	mnohokrát	k6eAd1	mnohokrát
opakuje	opakovat	k5eAaImIp3nS	opakovat
–	–	k?	–
nosívat	nosívat	k5eAaImF	nosívat
<g/>
,	,	kIx,	,
chodívat	chodívat	k5eAaImF	chodívat
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
podle	podle	k7c2	podle
fázovosti	fázovost	k1gFnSc2	fázovost
děje	děj	k1gInSc2	děj
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
počínavá	počínavý	k2eAgFnSc1d1	počínavý
–	–	k?	–
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
začátek	začátek	k1gInSc4	začátek
děje	děj	k1gInSc2	děj
–	–	k?	–
vyjít	vyjít	k5eAaPmF	vyjít
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
končící	končící	k2eAgInSc1d1	končící
–	–	k?	–
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
konec	konec	k1gInSc4	konec
děje	děj	k1gInSc2	děj
–	–	k?	–
dojít	dojít	k5eAaPmF	dojít
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
omezovací	omezovací	k2eAgInSc1d1	omezovací
–	–	k?	–
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
začátek	začátek	k1gInSc4	začátek
i	i	k8xC	i
konec	konec	k1gInSc4	konec
děje	dít	k5eAaImIp3nS	dít
–	–	k?	–
rozejít	rozejít	k5eAaPmF	rozejít
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
podle	podle	k7c2	podle
rozměru	rozměr	k1gInSc2	rozměr
děje	děj	k1gInSc2	děj
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
okamžitá	okamžitý	k2eAgFnSc1d1	okamžitá
–	–	k?	–
děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
–	–	k?	–
bodnout	bodnout	k5eAaPmF	bodnout
<g/>
,	,	kIx,	,
střelit	střelit	k5eAaPmF	střelit
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
trvací	trvací	k2eAgInSc1d1	trvací
–	–	k?	–
děj	děj	k1gInSc1	děj
probíhá	probíhat	k5eAaImIp3nS	probíhat
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
–	–	k?	–
číst	číst	k5eAaImF	číst
<g/>
,	,	kIx,	,
spát	spát	k5eAaImF	spát
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
podle	podle	k7c2	podle
míry	míra	k1gFnSc2	míra
děje	děj	k1gInSc2	děj
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
částečná	částečný	k2eAgFnSc1d1	částečná
–	–	k?	–
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
malou	malý	k2eAgFnSc4d1	malá
míru	míra	k1gFnSc4	míra
děje	dít	k5eAaImIp3nS	dít
–	–	k?	–
popostrčit	popostrčit	k5eAaPmF	popostrčit
<g/>
,	,	kIx,	,
pousmát	pousmát	k5eAaPmF	pousmát
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Čas	čas	k1gInSc1	čas
===	===	k?	===
</s>
</p>
<p>
<s>
Česká	český	k2eAgNnPc1d1	české
slovesa	sloveso	k1gNnPc1	sloveso
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
3	[number]	k4	3
absolutní	absolutní	k2eAgInPc1d1	absolutní
časy	čas	k1gInPc1	čas
–	–	k?	–
minulý	minulý	k2eAgInSc1d1	minulý
<g/>
,	,	kIx,	,
přítomný	přítomný	k2eAgInSc1d1	přítomný
a	a	k8xC	a
budoucí	budoucí	k2eAgMnSc1d1	budoucí
<g/>
.	.	kIx.	.
</s>
<s>
Relativita	relativita	k1gFnSc1	relativita
dějů	děj	k1gInPc2	děj
se	se	k3xPyFc4	se
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
pomocí	pomocí	k7c2	pomocí
vidu	vid	k1gInSc2	vid
<g/>
,	,	kIx,	,
větnými	větný	k2eAgFnPc7d1	větná
vazbami	vazba	k1gFnPc7	vazba
a	a	k8xC	a
pomocí	pomocí	k7c2	pomocí
přechodníků	přechodník	k1gInPc2	přechodník
a	a	k8xC	a
příčestí	příčestí	k1gNnSc2	příčestí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čeština	čeština	k1gFnSc1	čeština
má	mít	k5eAaImIp3nS	mít
ještě	ještě	k6eAd1	ještě
nepoužívaný	používaný	k2eNgInSc1d1	nepoužívaný
předminulý	předminulý	k2eAgInSc1d1	předminulý
čas-plusquamperfekt	časlusquamperfekt	k1gInSc1	čas-plusquamperfekt
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
fráze	fráze	k1gFnSc2	fráze
žili	žít	k5eAaImAgMnP	žít
byli	být	k5eAaImAgMnP	být
</s>
</p>
<p>
<s>
již	již	k6eAd1	již
prakticky	prakticky	k6eAd1	prakticky
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přítomný	přítomný	k2eAgInSc4d1	přítomný
čas	čas	k1gInSc4	čas
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
pouze	pouze	k6eAd1	pouze
nedokonavá	dokonavý	k2eNgNnPc4d1	nedokonavé
slovesa	sloveso	k1gNnPc4	sloveso
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Přítomný	přítomný	k2eAgInSc4d1	přítomný
čas	čas	k1gInSc4	čas
====	====	k?	====
</s>
</p>
<p>
<s>
Přítomný	přítomný	k2eAgInSc1d1	přítomný
čas	čas	k1gInSc1	čas
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
pomocí	pomocí	k7c2	pomocí
zvláštních	zvláštní	k2eAgFnPc2d1	zvláštní
koncovek	koncovka	k1gFnPc2	koncovka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Slovesa	sloveso	k1gNnPc1	sloveso
se	se	k3xPyFc4	se
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
do	do	k7c2	do
5	[number]	k4	5
tříd	třída	k1gFnPc2	třída
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
tvoří	tvořit	k5eAaImIp3nS	tvořit
přítomný	přítomný	k2eAgInSc1d1	přítomný
čas	čas	k1gInSc1	čas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Minulý	minulý	k2eAgInSc4d1	minulý
čas	čas	k1gInSc4	čas
====	====	k?	====
</s>
</p>
<p>
<s>
Minulý	minulý	k2eAgMnSc1d1	minulý
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
pomocí	pomocí	k7c2	pomocí
příčestí	příčestí	k1gNnSc2	příčestí
činného	činný	k2eAgNnSc2d1	činné
(	(	kIx(	(
<g/>
minulého	minulý	k2eAgNnSc2d1	Minulé
<g/>
;	;	kIx,	;
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
příslušného	příslušný	k2eAgInSc2d1	příslušný
rodu	rod	k1gInSc2	rod
<g/>
)	)	kIx)	)
a	a	k8xC	a
přítomných	přítomný	k2eAgInPc2d1	přítomný
tvarů	tvar	k1gInPc2	tvar
pomocného	pomocný	k2eAgNnSc2d1	pomocné
slovesa	sloveso	k1gNnSc2	sloveso
být	být	k5eAaImF	být
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
vypouštějí	vypouštět	k5eAaImIp3nP	vypouštět
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
osobě	osoba	k1gFnSc3	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
příklad	příklad	k1gInSc1	příklad
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
rod	rod	k1gInSc4	rod
mužský	mužský	k2eAgInSc4d1	mužský
(	(	kIx(	(
<g/>
životný	životný	k2eAgInSc4d1	životný
v	v	k7c6	v
množném	množný	k2eAgNnSc6d1	množné
čísle	číslo	k1gNnSc6	číslo
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
====	====	k?	====
Budoucí	budoucí	k2eAgInSc4d1	budoucí
čas	čas	k1gInSc4	čas
====	====	k?	====
</s>
</p>
<p>
<s>
U	u	k7c2	u
nedokonavých	dokonavý	k2eNgNnPc2d1	nedokonavé
sloves	sloveso	k1gNnPc2	sloveso
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
pomocí	pomocí	k7c2	pomocí
budoucích	budoucí	k2eAgInPc2d1	budoucí
tvarů	tvar	k1gInPc2	tvar
pomocného	pomocný	k2eAgNnSc2d1	pomocné
slovesa	sloveso	k1gNnSc2	sloveso
být	být	k5eAaImF	být
a	a	k8xC	a
infinitivu	infinitiv	k1gInSc3	infinitiv
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
U	u	k7c2	u
některých	některý	k3yIgNnPc2	některý
sloves	sloveso	k1gNnPc2	sloveso
(	(	kIx(	(
<g/>
v	v	k7c6	v
první	první	k4xOgFnSc6	první
slovesné	slovesný	k2eAgFnSc6d1	slovesná
třídě	třída	k1gFnSc6	třída
<g/>
)	)	kIx)	)
vyjadřujících	vyjadřující	k2eAgFnPc2d1	vyjadřující
pohyb	pohyb	k1gInSc4	pohyb
se	se	k3xPyFc4	se
budoucí	budoucí	k2eAgInSc1d1	budoucí
čas	čas	k1gInSc1	čas
tvoří	tvořit	k5eAaImIp3nS	tvořit
přidáním	přidání	k1gNnSc7	přidání
předpony	předpona	k1gFnSc2	předpona
po-	po-	k?	po-
<g/>
/	/	kIx~	/
<g/>
pů-	pů-	k?	pů-
k	k	k7c3	k
tvarům	tvar	k1gInPc3	tvar
přítomného	přítomný	k2eAgInSc2d1	přítomný
času	čas	k1gInSc2	čas
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
půjdu	jít	k5eAaImIp1nS	jít
<g/>
,	,	kIx,	,
ponesu	nést	k5eAaImIp1nS	nést
<g/>
,	,	kIx,	,
povezu	povézt	k5eAaPmIp1nS	povézt
</s>
</p>
<p>
<s>
U	u	k7c2	u
dokonavých	dokonavý	k2eAgNnPc2d1	dokonavé
sloves	sloveso	k1gNnPc2	sloveso
přítomné	přítomný	k2eAgInPc1d1	přítomný
tvary	tvar	k1gInPc4	tvar
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
budoucnost	budoucnost	k1gFnSc4	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Srovnej	srovnat	k5eAaPmRp2nS	srovnat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
budu	být	k5eAaImBp1nS	být
dělat	dělat	k5eAaImF	dělat
–	–	k?	–
nedok	nedok	k1gInSc4	nedok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
udělám	udělat	k5eAaPmIp1nS	udělat
–	–	k?	–
dok	dok	k1gInSc4	dok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Předminulý	předminulý	k2eAgInSc1d1	předminulý
čas	čas	k1gInSc1	čas
–	–	k?	–
plusquamperfektum	plusquamperfektum	k1gNnSc1	plusquamperfektum
====	====	k?	====
</s>
</p>
<p>
<s>
===	===	k?	===
Způsob	způsob	k1gInSc1	způsob
===	===	k?	===
</s>
</p>
<p>
<s>
Slovesa	sloveso	k1gNnPc1	sloveso
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
oznamovací	oznamovací	k2eAgInSc4d1	oznamovací
<g/>
,	,	kIx,	,
rozkazovací	rozkazovací	k2eAgInSc4d1	rozkazovací
a	a	k8xC	a
podmiňovací	podmiňovací	k2eAgInSc4d1	podmiňovací
způsob	způsob	k1gInSc4	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Základním	základní	k2eAgInSc7d1	základní
způsobem	způsob	k1gInSc7	způsob
je	být	k5eAaImIp3nS	být
oznamovací	oznamovací	k2eAgInSc1d1	oznamovací
způsob	způsob	k1gInSc1	způsob
(	(	kIx(	(
<g/>
indikativ	indikativ	k1gInSc1	indikativ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
děj	děj	k1gInSc4	děj
jako	jako	k8xC	jako
jistý	jistý	k2eAgInSc4d1	jistý
<g/>
,	,	kIx,	,
skutečný	skutečný	k2eAgInSc4d1	skutečný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Rozkazovací	rozkazovací	k2eAgInSc1d1	rozkazovací
způsob	způsob	k1gInSc1	způsob
====	====	k?	====
</s>
</p>
<p>
<s>
Rozkazovací	rozkazovací	k2eAgInSc1d1	rozkazovací
způsob	způsob	k1gInSc1	způsob
(	(	kIx(	(
<g/>
imperativ	imperativ	k1gInSc1	imperativ
<g/>
)	)	kIx)	)
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
rozkaz	rozkaz	k1gInSc4	rozkaz
<g/>
,	,	kIx,	,
výzvu	výzva	k1gFnSc4	výzva
nebo	nebo	k8xC	nebo
přání	přání	k1gNnSc4	přání
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nS	tvořit
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
2	[number]	k4	2
<g/>
.	.	kIx.	.
osobu	osoba	k1gFnSc4	osoba
v	v	k7c6	v
jednotném	jednotný	k2eAgNnSc6d1	jednotné
i	i	k8xC	i
množném	množný	k2eAgNnSc6d1	množné
čísle	číslo	k1gNnSc6	číslo
a	a	k8xC	a
pro	pro	k7c4	pro
1	[number]	k4	1
<g/>
.	.	kIx.	.
osobu	osoba	k1gFnSc4	osoba
v	v	k7c6	v
množném	množný	k2eAgNnSc6d1	množné
čísle	číslo	k1gNnSc6	číslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
osobě	osoba	k1gFnSc3	osoba
jednotného	jednotný	k2eAgNnSc2d1	jednotné
čísla	číslo	k1gNnSc2	číslo
má	mít	k5eAaImIp3nS	mít
buď	buď	k8xC	buď
nulové	nulový	k2eAgNnSc1d1	nulové
zakončení	zakončení	k1gNnSc1	zakončení
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
přibírá	přibírat	k5eAaImIp3nS	přibírat
koncovku	koncovka	k1gFnSc4	koncovka
-i	-i	k?	-i
<g/>
/	/	kIx~	/
<g/>
-ej	-ej	k?	-ej
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
třídy	třída	k1gFnSc2	třída
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
osobě	osoba	k1gFnSc3	osoba
množného	množný	k2eAgNnSc2d1	množné
čísla	číslo	k1gNnSc2	číslo
přibírá	přibírat	k5eAaImIp3nS	přibírat
koncovku	koncovka	k1gFnSc4	koncovka
-te	e	k?	-te
<g/>
/	/	kIx~	/
<g/>
-ete	-ete	k1gFnSc2	-et
<g/>
/	/	kIx~	/
<g/>
-ejte	-ejte	k5eAaPmIp2nP	-ejte
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
osobě	osoba	k1gFnSc3	osoba
množného	množný	k2eAgNnSc2d1	množné
čísla	číslo	k1gNnSc2	číslo
přibírá	přibírat	k5eAaImIp3nS	přibírat
koncovku	koncovka	k1gFnSc4	koncovka
-me	e	k?	-me
<g/>
/	/	kIx~	/
<g/>
-eme	-eme	k1gInSc1	-eme
<g/>
/	/	kIx~	/
<g/>
-ejme	-ejma	k5eAaPmIp3nS	-ejmat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
buď	buď	k8xC	buď
<g/>
!	!	kIx.	!
</s>
<s>
buďte	budit	k5eAaImRp2nP	budit
<g/>
!	!	kIx.	!
</s>
<s>
buďme	budit	k5eAaImRp1nP	budit
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
spi	spát	k5eAaImRp2nS	spát
<g/>
!	!	kIx.	!
</s>
<s>
spěte	spát	k5eAaImRp2nP	spát
<g/>
!	!	kIx.	!
</s>
<s>
spěme	spát	k5eAaImRp1nP	spát
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
dělej	dělat	k5eAaImRp2nS	dělat
<g/>
!	!	kIx.	!
</s>
<s>
dělejte	dělat	k5eAaImRp2nP	dělat
<g/>
!	!	kIx.	!
</s>
<s>
dělejme	dělat	k5eAaImRp1nP	dělat
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
Rozkazovací	rozkazovací	k2eAgInSc1d1	rozkazovací
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
<g/>
-li	i	k?	-li
příkaz	příkaz	k1gInSc4	příkaz
či	či	k8xC	či
výzvu	výzva	k1gFnSc4	výzva
k	k	k7c3	k
vykonání	vykonání	k1gNnSc3	vykonání
nějaké	nějaký	k3yIgFnSc2	nějaký
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
tvoří	tvořit	k5eAaImIp3nS	tvořit
od	od	k7c2	od
dokonavých	dokonavý	k2eAgNnPc2d1	dokonavé
sloves	sloveso	k1gNnPc2	sloveso
<g/>
:	:	kIx,	:
Udělej	udělat	k5eAaPmRp2nS	udělat
to	ten	k3xDgNnSc1	ten
<g/>
!	!	kIx.	!
</s>
<s>
Otevři	otevřít	k5eAaPmRp2nS	otevřít
okno	okno	k1gNnSc1	okno
<g/>
!	!	kIx.	!
</s>
<s>
Posaďte	posadit	k5eAaPmRp2nP	posadit
se	se	k3xPyFc4	se
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
Nedokonavým	dokonavý	k2eNgNnSc7d1	nedokonavé
slovesem	sloveso	k1gNnSc7	sloveso
se	se	k3xPyFc4	se
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
výzva	výzva	k1gFnSc1	výzva
k	k	k7c3	k
pokračování	pokračování	k1gNnSc3	pokračování
činnosti	činnost	k1gFnSc2	činnost
<g/>
:	:	kIx,	:
Pokračuj	pokračovat	k5eAaImRp2nS	pokračovat
<g/>
!	!	kIx.	!
</s>
<s>
Klidně	klidně	k6eAd1	klidně
dál	daleko	k6eAd2	daleko
spěte	spát	k5eAaImRp2nP	spát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
výzva	výzva	k1gFnSc1	výzva
k	k	k7c3	k
okamžitému	okamžitý	k2eAgNnSc3d1	okamžité
započetí	započetí	k1gNnSc3	započetí
činnosti	činnost	k1gFnSc2	činnost
<g/>
:	:	kIx,	:
Pište	psát	k5eAaImRp2nP	psát
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
=	=	kIx~	=
Začněte	začít	k5eAaPmRp2nP	začít
psát	psát	k5eAaImF	psát
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
výzva	výzva	k1gFnSc1	výzva
k	k	k7c3	k
opakované	opakovaný	k2eAgFnSc3d1	opakovaná
činnosti	činnost	k1gFnSc3	činnost
nebo	nebo	k8xC	nebo
postupné	postupný	k2eAgFnSc3d1	postupná
činnosti	činnost	k1gFnSc3	činnost
<g/>
:	:	kIx,	:
Volejte	volat	k5eAaImRp2nP	volat
nám	my	k3xPp1nPc3	my
každý	každý	k3xTgInSc4	každý
pátek	pátek	k1gInSc4	pátek
<g/>
.	.	kIx.	.
</s>
<s>
Kousej	kousat	k5eAaImRp2nS	kousat
pomalu	pomalu	k6eAd1	pomalu
<g/>
.	.	kIx.	.
</s>
<s>
Přecházejte	přecházet	k5eAaImRp2nP	přecházet
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
zelenou	zelená	k1gFnSc4	zelená
<g/>
.	.	kIx.	.
<g/>
Zákaz	zákaz	k1gInSc1	zákaz
(	(	kIx(	(
<g/>
záporný	záporný	k2eAgInSc1d1	záporný
protiklad	protiklad	k1gInSc1	protiklad
příkazu	příkaz	k1gInSc2	příkaz
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
tvoří	tvořit	k5eAaImIp3nS	tvořit
od	od	k7c2	od
nedokonavých	dokonavý	k2eNgNnPc2d1	nedokonavé
sloves	sloveso	k1gNnPc2	sloveso
<g/>
:	:	kIx,	:
Nedělej	dělat	k5eNaImRp2nS	dělat
to	ten	k3xDgNnSc1	ten
<g/>
!	!	kIx.	!
</s>
<s>
Neotvírej	otvírat	k5eNaImRp2nS	otvírat
okno	okno	k1gNnSc1	okno
<g/>
!	!	kIx.	!
</s>
<s>
Nesedejte	sedat	k5eNaImRp2nP	sedat
si	se	k3xPyFc3	se
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
Záporný	záporný	k2eAgInSc1d1	záporný
rozkazovací	rozkazovací	k2eAgInSc1d1	rozkazovací
způsob	způsob	k1gInSc1	způsob
od	od	k7c2	od
dokonavého	dokonavý	k2eAgNnSc2d1	dokonavé
slovesa	sloveso	k1gNnSc2	sloveso
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
varování	varování	k1gNnSc1	varování
<g/>
:	:	kIx,	:
Nerozlij	rozlít	k5eNaPmRp2nS	rozlít
to	ten	k3xDgNnSc1	ten
<g/>
!	!	kIx.	!
</s>
<s>
Neudělej	udělat	k5eNaPmRp2nS	udělat
chybu	chyba	k1gFnSc4	chyba
<g/>
!	!	kIx.	!
</s>
<s>
Neupadni	upadnout	k5eNaPmRp2nS	upadnout
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
oživení	oživení	k1gNnSc3	oživení
výpovědi	výpověď	k1gFnSc2	výpověď
<g/>
:	:	kIx,	:
Nekup	kupit	k5eNaImRp2nS	kupit
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tak	tak	k6eAd1	tak
levné	levný	k2eAgNnSc1d1	levné
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Tomu	Tom	k1gMnSc3	Tom
nelze	lze	k6eNd1	lze
odolat	odolat	k5eAaPmF	odolat
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
====	====	k?	====
Podmiňovací	podmiňovací	k2eAgInSc1d1	podmiňovací
způsob	způsob	k1gInSc1	způsob
====	====	k?	====
</s>
</p>
<p>
<s>
Podmiňovací	podmiňovací	k2eAgInSc1d1	podmiňovací
způsob	způsob	k1gInSc1	způsob
(	(	kIx(	(
<g/>
kondicionál	kondicionál	k1gInSc1	kondicionál
<g/>
)	)	kIx)	)
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
děj	děj	k1gInSc1	děj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
nastal	nastat	k5eAaPmAgInS	nastat
při	při	k7c6	při
splnění	splnění	k1gNnSc6	splnění
určité	určitý	k2eAgFnSc2d1	určitá
podmínky	podmínka	k1gFnSc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nS	tvořit
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
příčestí	příčestí	k1gNnSc2	příčestí
činného	činný	k2eAgNnSc2d1	činné
(	(	kIx(	(
<g/>
minulého	minulý	k2eAgNnSc2d1	Minulé
<g/>
)	)	kIx)	)
a	a	k8xC	a
zvláštních	zvláštní	k2eAgInPc2d1	zvláštní
(	(	kIx(	(
<g/>
aoristových	aoristový	k2eAgInPc2d1	aoristový
<g/>
)	)	kIx)	)
tvarů	tvar	k1gInPc2	tvar
pomocného	pomocný	k2eAgNnSc2d1	pomocné
slovesa	sloveso	k1gNnSc2	sloveso
být	být	k5eAaImF	být
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
příklad	příklad	k1gInSc1	příklad
přítomného	přítomný	k2eAgInSc2d1	přítomný
kondicionálu	kondicionál	k1gInSc2	kondicionál
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
rod	rod	k1gInSc4	rod
mužský	mužský	k2eAgInSc4d1	mužský
(	(	kIx(	(
<g/>
životný	životný	k2eAgInSc4d1	životný
v	v	k7c6	v
množném	množný	k2eAgNnSc6d1	množné
čísle	číslo	k1gNnSc6	číslo
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
rovněž	rovněž	k9	rovněž
minulý	minulý	k2eAgInSc4d1	minulý
kondicionál	kondicionál	k1gInSc4	kondicionál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
často	často	k6eAd1	často
nahrazován	nahrazovat	k5eAaImNgInS	nahrazovat
kondicionálem	kondicionál	k1gInSc7	kondicionál
přítomným	přítomný	k2eAgInSc7d1	přítomný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
By	by	k9	by
je	být	k5eAaImIp3nS	být
také	také	k9	také
součástí	součást	k1gFnSc7	součást
spojek	spojka	k1gFnPc2	spojka
aby	aby	kYmCp3nP	aby
a	a	k9	a
kdyby	kdyby	k9	kdyby
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
rovněž	rovněž	k9	rovněž
přijímají	přijímat	k5eAaImIp3nP	přijímat
stejné	stejný	k2eAgFnPc4d1	stejná
osobní	osobní	k2eAgFnPc4d1	osobní
koncovky	koncovka	k1gFnPc4	koncovka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Kdybych	kdyby	kYmCp1nS	kdyby
nepracoval	pracovat	k5eNaImAgMnS	pracovat
<g/>
,	,	kIx,	,
nedostal	dostat	k5eNaPmAgMnS	dostat
bych	by	kYmCp1nS	by
výplatu	výplata	k1gFnSc4	výplata
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rod	rod	k1gInSc4	rod
===	===	k?	===
</s>
</p>
<p>
<s>
Slovesný	slovesný	k2eAgInSc1d1	slovesný
rod	rod	k1gInSc1	rod
je	být	k5eAaImIp3nS	být
činný	činný	k2eAgMnSc1d1	činný
nebo	nebo	k8xC	nebo
trpný	trpný	k2eAgMnSc1d1	trpný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
existují	existovat	k5eAaImIp3nP	existovat
2	[number]	k4	2
způsoby	způsob	k1gInPc1	způsob
tvoření	tvoření	k1gNnSc1	tvoření
trpného	trpný	k2eAgInSc2d1	trpný
rodu	rod	k1gInSc2	rod
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Opisně	opisně	k6eAd1	opisně
pomocí	pomocí	k7c2	pomocí
slovesa	sloveso	k1gNnSc2	sloveso
být	být	k5eAaImF	být
a	a	k8xC	a
příčestí	příčestí	k1gNnSc2	příčestí
trpného	trpný	k2eAgNnSc2d1	trpné
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
ve	v	k7c4	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Záměnou	záměna	k1gFnSc7	záměna
činného	činný	k2eAgMnSc2d1	činný
a	a	k8xC	a
trpného	trpný	k2eAgInSc2d1	trpný
rodu	rod	k1gInSc2	rod
ve	v	k7c6	v
větě	věta	k1gFnSc6	věta
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
záměně	záměna	k1gFnSc3	záměna
podmětu	podmět	k1gInSc2	podmět
a	a	k8xC	a
předmětu	předmět	k1gInSc2	předmět
<g/>
:	:	kIx,	:
Petr	Petr	k1gMnSc1	Petr
maluje	malovat	k5eAaImIp3nS	malovat
obrázek	obrázek	k1gInSc4	obrázek
-	-	kIx~	-
Obrázek	obrázek	k1gInSc4	obrázek
je	být	k5eAaImIp3nS	být
malován	malovat	k5eAaImNgMnS	malovat
Petrem	Petr	k1gMnSc7	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Opisně	opisně	k6eAd1	opisně
proto	proto	k8xC	proto
nelze	lze	k6eNd1	lze
vytvořit	vytvořit	k5eAaPmF	vytvořit
trpný	trpný	k2eAgInSc4d1	trpný
rod	rod	k1gInSc4	rod
od	od	k7c2	od
sloves	sloveso	k1gNnPc2	sloveso
bezpředmětových	bezpředmětový	k2eAgNnPc2d1	bezpředmětové
(	(	kIx(	(
<g/>
slzet	slzet	k5eAaImF	slzet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
neosobním	osobní	k2eNgInSc7d1	neosobní
podmětem	podmět	k1gInSc7	podmět
(	(	kIx(	(
<g/>
pršet	pršet	k5eAaImF	pršet
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
modálních	modální	k2eAgInPc2d1	modální
(	(	kIx(	(
<g/>
muset	muset	k5eAaImF	muset
<g/>
)	)	kIx)	)
<g/>
.2	.2	k4	.2
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
zvratného	zvratný	k2eAgNnSc2d1	zvratné
zájmena	zájmeno	k1gNnSc2	zájmeno
se	s	k7c7	s
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Ono	onen	k3xDgNnSc1	onen
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
neudělalo	udělat	k5eNaPmAgNnS	udělat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
<g/>
Použití	použití	k1gNnSc1	použití
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
neznamená	znamenat	k5eNaImIp3nS	znamenat
vždy	vždy	k6eAd1	vždy
trpný	trpný	k2eAgInSc1d1	trpný
rod	rod	k1gInSc1	rod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Slovesné	slovesný	k2eAgFnPc4d1	slovesná
třídy	třída	k1gFnPc4	třída
a	a	k8xC	a
vzory	vzor	k1gInPc4	vzor
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
tvaru	tvar	k1gInSc2	tvar
3	[number]	k4	3
<g/>
.	.	kIx.	.
osoby	osoba	k1gFnPc1	osoba
jednotného	jednotný	k2eAgNnSc2d1	jednotné
čísla	číslo	k1gNnSc2	číslo
přítomného	přítomný	k2eAgInSc2d1	přítomný
času	čas	k1gInSc2	čas
oznamovacího	oznamovací	k2eAgInSc2d1	oznamovací
způsobu	způsob	k1gInSc2	způsob
se	se	k3xPyFc4	se
slovesa	sloveso	k1gNnPc1	sloveso
dělí	dělit	k5eAaImIp3nP	dělit
do	do	k7c2	do
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
tříd	třída	k1gFnPc2	třída
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
tvaru	tvar	k1gInSc2	tvar
3	[number]	k4	3
<g/>
.	.	kIx.	.
osoby	osoba	k1gFnPc1	osoba
jednotného	jednotný	k2eAgNnSc2d1	jednotné
čísla	číslo	k1gNnSc2	číslo
minulého	minulý	k2eAgInSc2d1	minulý
času	čas	k1gInSc2	čas
oznamovacího	oznamovací	k2eAgInSc2d1	oznamovací
způsobu	způsob	k1gInSc2	způsob
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
vzory	vzor	k1gInPc4	vzor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čeština	čeština	k1gFnSc1	čeština
má	mít	k5eAaImIp3nS	mít
celkem	celkem	k6eAd1	celkem
pět	pět	k4xCc4	pět
tříd	třída	k1gFnPc2	třída
vzorů	vzor	k1gInPc2	vzor
–	–	k?	–
výjimku	výjimek	k1gInSc2	výjimek
tvoří	tvořit	k5eAaImIp3nP	tvořit
slovesa	sloveso	k1gNnPc4	sloveso
být	být	k5eAaImF	být
<g/>
,	,	kIx,	,
jíst	jíst	k5eAaImF	jíst
<g/>
,	,	kIx,	,
vědět	vědět	k5eAaImF	vědět
<g/>
,	,	kIx,	,
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
mít	mít	k5eAaImF	mít
<g/>
,	,	kIx,	,
chtít	chtít	k5eAaImF	chtít
a	a	k8xC	a
jít	jít	k5eAaImF	jít
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
jsou	být	k5eAaImIp3nP	být
nepravidelná	pravidelný	k2eNgNnPc1d1	nepravidelné
<g/>
.	.	kIx.	.
</s>
<s>
Vzory	vzor	k1gInPc1	vzor
nepostihují	postihovat	k5eNaImIp3nP	postihovat
změny	změna	k1gFnPc1	změna
samohlásek	samohláska	k1gFnPc2	samohláska
ve	v	k7c6	v
kmeni	kmen	k1gInSc6	kmen
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
-e	-e	k?	-e
<g/>
:	:	kIx,	:
<g/>
Tvrdé	Tvrdé	k2eAgInPc1d1	Tvrdé
vzory	vzor	k1gInPc1	vzor
<g/>
:	:	kIx,	:
v	v	k7c6	v
kmeni	kmen	k1gInSc6	kmen
je	být	k5eAaImIp3nS	být
tvrdá	tvrdý	k2eAgFnSc1d1	tvrdá
či	či	k8xC	či
obojetná	obojetný	k2eAgFnSc1d1	obojetná
souhláska	souhláska	k1gFnSc1	souhláska
<g/>
,	,	kIx,	,
před	před	k7c7	před
"	"	kIx"	"
<g/>
l	l	kA	l
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
samohláska	samohláska	k1gFnSc1	samohláska
"	"	kIx"	"
<g/>
a	a	k8xC	a
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
<g/>
nese	nést	k5eAaImIp3nS	nést
–	–	k?	–
nesl	nést	k5eAaImAgMnS	nést
</s>
</p>
<p>
<s>
bere	brát	k5eAaImIp3nS	brát
–	–	k?	–
bral	brát	k5eAaImAgMnS	brát
</s>
</p>
<p>
<s>
Měkké	měkký	k2eAgInPc1d1	měkký
vzory	vzor	k1gInPc1	vzor
<g/>
:	:	kIx,	:
v	v	k7c6	v
kmeni	kmen	k1gInSc6	kmen
slova	slovo	k1gNnSc2	slovo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
měkká	měkký	k2eAgFnSc1d1	měkká
souhláska	souhláska	k1gFnSc1	souhláska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
maže	mazat	k5eAaImIp3nS	mazat
–	–	k?	–
mazal	mazal	k1gMnSc1	mazal
</s>
</p>
<p>
<s>
peče	péct	k5eAaImIp3nS	péct
–	–	k?	–
pekl	péct	k5eAaImAgInS	péct
</s>
</p>
<p>
<s>
umře	umřít	k5eAaPmIp3nS	umřít
–	–	k?	–
umřel	umřít	k5eAaPmAgMnS	umřít
(	(	kIx(	(
<g/>
tře	třít	k5eAaImIp3nS	třít
–	–	k?	–
třel	třít	k5eAaImAgMnS	třít
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
-ne	e	k?	-ne
</s>
</p>
<p>
<s>
tiskne	tisknout	k5eAaImIp3nS	tisknout
–	–	k?	–
tiskl	tisknout	k5eAaImAgInS	tisknout
</s>
</p>
<p>
<s>
mine	minout	k5eAaImIp3nS	minout
–	–	k?	–
minul	minulo	k1gNnPc2	minulo
</s>
</p>
<p>
<s>
začne	začít	k5eAaPmIp3nS	začít
–	–	k?	–
začal	začít	k5eAaPmAgInS	začít
</s>
</p>
<p>
<s>
-je	e	k?	-je
</s>
</p>
<p>
<s>
kryje	krýt	k5eAaImIp3nS	krýt
–	–	k?	–
kryl	krýt	k5eAaImAgMnS	krýt
</s>
</p>
<p>
<s>
kupuje	kupovat	k5eAaImIp3nS	kupovat
–	–	k?	–
kupoval	kupovat	k5eAaImAgMnS	kupovat
</s>
</p>
<p>
<s>
-í	-í	k?	-í
</s>
</p>
<p>
<s>
prosí	prosit	k5eAaImIp3nS	prosit
–	–	k?	–
prosil	prosit	k5eAaImAgMnS	prosit
</s>
</p>
<p>
<s>
trpí	trpět	k5eAaImIp3nS	trpět
–	–	k?	–
trpěl	trpět	k5eAaImAgMnS	trpět
</s>
</p>
<p>
<s>
sází	sázet	k5eAaImIp3nS	sázet
–	–	k?	–
sázel	sázet	k5eAaImAgMnS	sázet
</s>
</p>
<p>
<s>
-á	-á	k?	-á
</s>
</p>
<p>
<s>
dělá	dělat	k5eAaImIp3nS	dělat
–	–	k?	–
dělalMnemotechnická	dělalMnemotechnický	k2eAgFnSc1d1	dělalMnemotechnický
pomůcka	pomůcka	k1gFnSc1	pomůcka
<g/>
:	:	kIx,	:
Že-ne	Že	k1gInSc5	Že-n
je	on	k3xPp3gMnPc4	on
bída	bída	k1gFnSc1	bída
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1	[number]	k4	1
<g/>
.	.	kIx.	.
třída	třída	k1gFnSc1	třída
===	===	k?	===
</s>
</p>
<p>
<s>
Koncovky	koncovka	k1gFnPc1	koncovka
0	[number]	k4	0
<g/>
/	/	kIx~	/
<g/>
-te	e	k?	-te
<g/>
/	/	kIx~	/
<g/>
-me	e	k?	-me
v	v	k7c6	v
rozkazovacím	rozkazovací	k2eAgInSc6d1	rozkazovací
způsobu	způsob	k1gInSc6	způsob
má	mít	k5eAaImIp3nS	mít
většina	většina	k1gFnSc1	většina
sloves	sloveso	k1gNnPc2	sloveso
<g/>
,	,	kIx,	,
koncovky	koncovka	k1gFnSc2	koncovka
-i	-i	k?	-i
<g/>
/	/	kIx~	/
<g/>
-ete	-ete	k1gMnSc2	-et
<g/>
/	/	kIx~	/
<g/>
-eme	-eme	k1gMnSc2	-em
nebo	nebo	k8xC	nebo
-i	-i	k?	-i
<g/>
/	/	kIx~	/
<g/>
-ěte	-ěte	k1gMnSc2	-ět
<g/>
/	/	kIx~	/
<g/>
-ěme	-ěme	k6eAd1	-ěme
mají	mít	k5eAaImIp3nP	mít
slovesa	sloveso	k1gNnPc1	sloveso
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnPc4	jejichž
kmen	kmen	k1gInSc1	kmen
končí	končit	k5eAaImIp3nS	končit
dvěma	dva	k4xCgFnPc7	dva
souhláskami	souhláska	k1gFnPc7	souhláska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2	[number]	k4	2
<g/>
.	.	kIx.	.
třída	třída	k1gFnSc1	třída
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
3	[number]	k4	3
<g/>
.	.	kIx.	.
třída	třída	k1gFnSc1	třída
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
4	[number]	k4	4
<g/>
.	.	kIx.	.
třída	třída	k1gFnSc1	třída
===	===	k?	===
</s>
</p>
<p>
<s>
Koncovky	koncovka	k1gFnPc1	koncovka
0	[number]	k4	0
<g/>
/	/	kIx~	/
<g/>
-te	e	k?	-te
<g/>
/	/	kIx~	/
<g/>
-me	e	k?	-me
v	v	k7c6	v
rozkazovacím	rozkazovací	k2eAgInSc6d1	rozkazovací
způsobu	způsob	k1gInSc6	způsob
má	mít	k5eAaImIp3nS	mít
většina	většina	k1gFnSc1	většina
sloves	sloveso	k1gNnPc2	sloveso
<g/>
,	,	kIx,	,
koncovky	koncovka	k1gFnSc2	koncovka
-i	-i	k?	-i
<g/>
/	/	kIx~	/
<g/>
-ete	-ete	k1gMnSc2	-et
<g/>
/	/	kIx~	/
<g/>
-eme	-eme	k1gMnSc2	-em
nebo	nebo	k8xC	nebo
-i	-i	k?	-i
<g/>
/	/	kIx~	/
<g/>
-ěte	-ěte	k1gMnSc2	-ět
<g/>
/	/	kIx~	/
<g/>
-ěme	-ěme	k6eAd1	-ěme
mají	mít	k5eAaImIp3nP	mít
slovesa	sloveso	k1gNnPc1	sloveso
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnPc4	jejichž
kmen	kmen	k1gInSc1	kmen
končí	končit	k5eAaImIp3nS	končit
dvěma	dva	k4xCgFnPc7	dva
souhláskami	souhláska	k1gFnPc7	souhláska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
5	[number]	k4	5
<g/>
.	.	kIx.	.
třída	třída	k1gFnSc1	třída
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Nepravidelná	pravidelný	k2eNgNnPc1d1	nepravidelné
slovesa	sloveso	k1gNnPc1	sloveso
==	==	k?	==
</s>
</p>
<p>
<s>
*	*	kIx~	*
Tvary	tvar	k1gInPc4	tvar
měn	měna	k1gFnPc2	měna
<g/>
(	(	kIx(	(
<g/>
-a	-a	k?	-a
<g/>
,	,	kIx,	,
-o	-o	k?	-o
<g/>
,	,	kIx,	,
-i	-i	k?	-i
<g/>
,	,	kIx,	,
-y	-y	k?	-y
<g/>
,	,	kIx,	,
-a	-a	k?	-a
<g/>
)	)	kIx)	)
a	a	k8xC	a
stán	stán	k?	stán
<g/>
(	(	kIx(	(
<g/>
-a	-a	k?	-a
<g/>
,	,	kIx,	,
-o	-o	k?	-o
<g/>
,	,	kIx,	,
-i	-i	k?	-i
<g/>
,	,	kIx,	,
-y	-y	k?	-y
<g/>
,	,	kIx,	,
-a	-a	k?	-a
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
češtině	čeština	k1gFnSc6	čeština
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nepravidelný	pravidelný	k2eNgInSc4d1	nepravidelný
budoucí	budoucí	k2eAgInSc4d1	budoucí
čas	čas	k1gInSc4	čas
===	===	k?	===
</s>
</p>
<p>
<s>
Nepravidelný	pravidelný	k2eNgInSc4d1	nepravidelný
budoucí	budoucí	k2eAgInSc4d1	budoucí
čas	čas	k1gInSc4	čas
mají	mít	k5eAaImIp3nP	mít
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
sloveso	sloveso	k1gNnSc4	sloveso
být	být	k5eAaImF	být
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc4	jeho
přeponové	přeponový	k2eAgFnPc4d1	přeponový
odvozeniny	odvozenina	k1gFnPc4	odvozenina
</s>
</p>
<p>
<s>
některá	některý	k3yIgNnPc4	některý
slovesa	sloveso	k1gNnPc4	sloveso
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
slovesné	slovesný	k2eAgFnSc6d1	slovesná
třídě	třída	k1gFnSc6	třída
a	a	k8xC	a
některá	některý	k3yIgNnPc4	některý
slovesa	sloveso	k1gNnPc4	sloveso
vyjadřující	vyjadřující	k2eAgNnPc4d1	vyjadřující
pohyb	pohyb	k1gInSc4	pohyb
</s>
</p>
<p>
<s>
====	====	k?	====
Budoucí	budoucí	k2eAgInSc4d1	budoucí
čas	čas	k1gInSc4	čas
slovesa	sloveso	k1gNnSc2	sloveso
být	být	k5eAaImF	být
a	a	k8xC	a
sloves	sloveso	k1gNnPc2	sloveso
od	od	k7c2	od
něj	on	k3xPp3gNnSc2	on
odvozených	odvozený	k2eAgFnPc2d1	odvozená
předponou	předpona	k1gFnSc7	předpona
====	====	k?	====
</s>
</p>
<p>
<s>
Sloveso	sloveso	k1gNnSc4	sloveso
být	být	k5eAaImF	být
má	můj	k3xOp1gFnSc1	můj
v	v	k7c6	v
budoucím	budoucí	k2eAgInSc6d1	budoucí
čase	čas	k1gInSc6	čas
změnu	změna	k1gFnSc4	změna
ve	v	k7c6	v
kmeni	kmen	k1gInSc6	kmen
na	na	k7c6	na
bud-	bud-	k?	bud-
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
budu	být	k5eAaImBp1nS	být
<g/>
,	,	kIx,	,
budeš	být	k5eAaImBp2nS	být
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
<g/>
,	,	kIx,	,
budeme	být	k5eAaImBp1nP	být
<g/>
,	,	kIx,	,
budete	být	k5eAaImBp2nP	být
<g/>
,	,	kIx,	,
budouU	budouU	k?	budouU
sloves	sloveso	k1gNnPc2	sloveso
odvozených	odvozený	k2eAgNnPc2d1	odvozené
předponou	předpona	k1gFnSc7	předpona
od	od	k7c2	od
slovesa	sloveso	k1gNnSc2	sloveso
být	být	k5eAaImF	být
(	(	kIx(	(
<g/>
např.	např.	kA	např.
dobýt	dobýt	k5eAaPmF	dobýt
<g/>
,	,	kIx,	,
odbýt	odbýt	k5eAaPmF	odbýt
<g/>
,	,	kIx,	,
zbýt	zbýt	k5eAaPmF	zbýt
<g/>
,	,	kIx,	,
přibýt	přibýt	k5eAaPmF	přibýt
<g/>
,	,	kIx,	,
ubýt	ubýt	k5eAaPmF	ubýt
<g/>
,	,	kIx,	,
nabýt	nabýt	k5eAaPmF	nabýt
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
setkáváme	setkávat	k5eAaImIp1nP	setkávat
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
tvary	tvar	k1gInPc7	tvar
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
původní	původní	k2eAgInSc1d1	původní
tvar	tvar	k1gInSc1	tvar
s	s	k7c7	s
u	u	k7c2	u
<g/>
:	:	kIx,	:
dobudu	dobýt	k5eAaPmIp1nS	dobýt
<g/>
,	,	kIx,	,
odbudeš	odbýt	k5eAaPmIp2nS	odbýt
<g/>
,	,	kIx,	,
zbude	zbýt	k5eAaPmIp3nS	zbýt
<g/>
,	,	kIx,	,
přibudeme	přibýt	k5eAaPmIp1nP	přibýt
<g/>
,	,	kIx,	,
ubudete	ubýt	k5eAaPmIp2nP	ubýt
<g/>
,	,	kIx,	,
nabude	nabýt	k5eAaPmIp3nS	nabýt
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
tvar	tvar	k1gInSc1	tvar
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
základní	základní	k2eAgInSc4d1	základní
<g/>
,	,	kIx,	,
stylově	stylově	k6eAd1	stylově
vyšší	vysoký	k2eAgMnSc1d2	vyšší
a	a	k8xC	a
spisovný	spisovný	k2eAgMnSc1d1	spisovný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
nový	nový	k2eAgInSc1d1	nový
tvar	tvar	k1gInSc1	tvar
s	s	k7c7	s
y	y	k?	y
<g/>
:	:	kIx,	:
dobydu	dobýt	k5eAaPmIp1nS	dobýt
<g/>
,	,	kIx,	,
odbydeš	odbýt	k5eAaPmIp2nS	odbýt
<g/>
,	,	kIx,	,
zbyde	zbýt	k5eAaPmIp3nS	zbýt
<g/>
,	,	kIx,	,
přibydeme	přibýt	k5eAaPmIp1nP	přibýt
<g/>
,	,	kIx,	,
ubydete	ubýt	k5eAaPmIp2nP	ubýt
<g/>
,	,	kIx,	,
nabyde	nabýt	k5eAaPmIp3nS	nabýt
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
tvar	tvar	k1gInSc1	tvar
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
stylově	stylově	k6eAd1	stylově
nižší	nízký	k2eAgFnSc4d2	nižší
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
hovorový	hovorový	k2eAgMnSc1d1	hovorový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Nepravidelný	pravidelný	k2eNgInSc4d1	nepravidelný
budoucí	budoucí	k2eAgInSc4d1	budoucí
čas	čas	k1gInSc4	čas
některých	některý	k3yIgNnPc2	některý
sloves	sloveso	k1gNnPc2	sloveso
====	====	k?	====
</s>
</p>
<p>
<s>
Některá	některý	k3yIgNnPc1	některý
stará	starý	k2eAgNnPc1d1	staré
nedokonavá	dokonavý	k2eNgNnPc1d1	nedokonavé
slovesa	sloveso	k1gNnPc1	sloveso
v	v	k7c4	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
slovesné	slovesný	k2eAgFnSc3d1	slovesná
třídě	třída	k1gFnSc3	třída
(	(	kIx(	(
<g/>
nést	nést	k5eAaImF	nést
<g/>
,	,	kIx,	,
vézt	vézt	k5eAaImF	vézt
<g/>
,	,	kIx,	,
vést	vést	k5eAaImF	vést
<g/>
,	,	kIx,	,
kvést	kvést	k5eAaImF	kvést
<g/>
,	,	kIx,	,
růst	růst	k5eAaImF	růst
ap.	ap.	kA	ap.
<g/>
)	)	kIx)	)
a	a	k8xC	a
některá	některý	k3yIgNnPc4	některý
stará	starý	k2eAgNnPc4d1	staré
nedokonavá	dokonavý	k2eNgNnPc4d1	nedokonavé
slovesa	sloveso	k1gNnPc4	sloveso
vyjadřující	vyjadřující	k2eAgInSc4d1	vyjadřující
pohyb	pohyb	k1gInSc4	pohyb
(	(	kIx(	(
<g/>
jít	jít	k5eAaImF	jít
<g/>
,	,	kIx,	,
jet	jet	k5eAaImF	jet
<g/>
,	,	kIx,	,
letět	letět	k5eAaImF	letět
<g/>
,	,	kIx,	,
plout	plout	k5eAaImF	plout
<g/>
,	,	kIx,	,
hnát	hnát	k5eAaImF	hnát
se	se	k3xPyFc4	se
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
běžet	běžet	k5eAaImF	běžet
ap.	ap.	kA	ap.
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
vedle	vedle	k7c2	vedle
pravidelného	pravidelný	k2eAgInSc2d1	pravidelný
způsobu	způsob	k1gInSc2	způsob
tvoření	tvoření	k1gNnSc4	tvoření
budoucího	budoucí	k2eAgInSc2d1	budoucí
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
bude	být	k5eAaImBp3nS	být
kvést	kvést	k5eAaImF	kvést
<g/>
,	,	kIx,	,
budou	být	k5eAaImBp3nP	být
plout	plout	k5eAaImF	plout
<g/>
,	,	kIx,	,
budu	být	k5eAaImBp1nS	být
se	se	k3xPyFc4	se
hnát	hnát	k5eAaImF	hnát
<g/>
)	)	kIx)	)
také	také	k9	také
druhý	druhý	k4xOgInSc1	druhý
způsob	způsob	k1gInSc1	způsob
tvoření	tvoření	k1gNnSc2	tvoření
budoucího	budoucí	k2eAgInSc2d1	budoucí
času	čas	k1gInSc2	čas
pomocí	pomocí	k7c2	pomocí
předpony	předpona	k1gFnSc2	předpona
po-	po-	k?	po-
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
přidá	přidat	k5eAaPmIp3nS	přidat
ke	k	k7c3	k
tvaru	tvar	k1gInSc3	tvar
přítomného	přítomný	k2eAgInSc2d1	přítomný
času	čas	k1gInSc2	čas
daného	daný	k2eAgNnSc2d1	dané
slovesa	sloveso	k1gNnSc2	sloveso
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
po-	po-	k?	po-
+	+	kIx~	+
kvete	kvést	k5eAaImIp3nS	kvést
=	=	kIx~	=
pokvete	kvést	k5eAaImIp3nS	kvést
<g/>
;	;	kIx,	;
poplují	poplout	k5eAaPmIp3nP	poplout
<g/>
;	;	kIx,	;
poženu	hnát	k5eAaImIp1nS	hnát
se	se	k3xPyFc4	se
<g/>
;	;	kIx,	;
poletíš	letět	k5eAaImIp2nS	letět
<g/>
,	,	kIx,	,
pojede	pojet	k5eAaPmIp3nS	pojet
poběžíme	běžet	k5eAaImIp1nP	běžet
<g/>
,	,	kIx,	,
ponesu	nést	k5eAaImIp1nS	nést
<g/>
,	,	kIx,	,
povezeš	povézt	k5eAaPmIp2nS	povézt
<g/>
,	,	kIx,	,
povede	vést	k5eAaImIp3nS	vést
<g/>
,	,	kIx,	,
porostou	růst	k5eAaImIp3nP	růst
ap.	ap.	kA	ap.
</s>
</p>
<p>
<s>
u	u	k7c2	u
slovesa	sloveso	k1gNnSc2	sloveso
jít	jít	k5eAaImF	jít
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
hlásková	hláskový	k2eAgFnSc1d1	hlásková
změna	změna	k1gFnSc1	změna
o	o	k7c4	o
>	>	kIx)	>
ů	ů	k?	ů
<g/>
:	:	kIx,	:
půjduNěkterá	půjduNěkterý	k2eAgNnPc4d1	půjduNěkterý
slovesa	sloveso	k1gNnPc4	sloveso
pravidelný	pravidelný	k2eAgInSc4d1	pravidelný
způsob	způsob	k1gInSc4	způsob
vůbec	vůbec	k9	vůbec
nevyužívají	využívat	k5eNaImIp3nP	využívat
(	(	kIx(	(
<g/>
pojedu	pojet	k5eAaPmIp1nS	pojet
<g/>
,	,	kIx,	,
půjdu	jít	k5eAaImIp1nS	jít
×	×	k?	×
budu	být	k5eAaImBp1nS	být
jet	jet	k5eAaImF	jet
<g/>
,	,	kIx,	,
budu	být	k5eAaImBp1nS	být
jít	jít	k5eAaImF	jít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
ostatních	ostatní	k2eAgMnPc2d1	ostatní
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
(	(	kIx(	(
<g/>
mnohem	mnohem	k6eAd1	mnohem
<g/>
)	)	kIx)	)
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
než	než	k8xS	než
ten	ten	k3xDgInSc1	ten
nepravidelný	pravidelný	k2eNgMnSc1d1	nepravidelný
(	(	kIx(	(
<g/>
poletíme	poletět	k5eAaPmIp1nP	poletět
×	×	k?	×
budeme	být	k5eAaImBp1nP	být
letět	letět	k5eAaImF	letět
<g/>
;	;	kIx,	;
poplujete	poplout	k5eAaPmIp2nP	poplout
×	×	k?	×
budete	být	k5eAaImBp2nP	být
plout	plout	k5eAaImF	plout
<g/>
;	;	kIx,	;
potečou	téct	k5eAaImIp3nP	téct
×	×	k?	×
budou	být	k5eAaImBp3nP	být
téci	téct	k5eAaImF	téct
<g/>
,	,	kIx,	,
porostu	porůst	k5eAaPmIp1nS	porůst
×	×	k?	×
budu	být	k5eAaImBp1nS	být
růst	růst	k5eAaImF	růst
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
předpona	předpona	k1gFnSc1	předpona
po-	po-	k?	po-
proniká	pronikat	k5eAaImIp3nS	pronikat
i	i	k9	i
do	do	k7c2	do
rozkazovacího	rozkazovací	k2eAgInSc2d1	rozkazovací
způsobu	způsob	k1gInSc2	způsob
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
může	moct	k5eAaImIp3nS	moct
odlišit	odlišit	k5eAaPmF	odlišit
význam	význam	k1gInSc4	význam
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
jdi	jít	k5eAaImRp2nS	jít
(	(	kIx(	(
<g/>
pryč	pryč	k6eAd1	pryč
<g/>
)	)	kIx)	)
×	×	k?	×
pojď	jít	k5eAaImRp2nS	jít
(	(	kIx(	(
<g/>
sem	sem	k6eAd1	sem
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
běž	běžet	k5eAaImRp2nS	běžet
×	×	k?	×
poběž	běžet	k5eAaImRp2nS	běžet
<g/>
;	;	kIx,	;
leťte	letět	k5eAaImRp2nP	letět
×	×	k?	×
poleťte	letět	k5eAaImRp2nP	letět
</s>
</p>
<p>
<s>
Tyto	tento	k3xDgInPc1	tento
tvary	tvar	k1gInPc1	tvar
nesmíme	smět	k5eNaImIp1nP	smět
zaměňovat	zaměňovat	k5eAaImF	zaměňovat
se	s	k7c7	s
slovesy	sloveso	k1gNnPc7	sloveso
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc1	jenž
mají	mít	k5eAaImIp3nP	mít
předponu	předpona	k1gFnSc4	předpona
po-	po-	k?	po-
i	i	k8xC	i
v	v	k7c6	v
infinitivu	infinitiv	k1gInSc2	infinitiv
a	a	k8xC	a
ostatních	ostatní	k2eAgInPc2d1	ostatní
tvarec	tvarec	k1gInSc4	tvarec
<g/>
:	:	kIx,	:
půjde	jít	k5eAaImIp3nS	jít
(	(	kIx(	(
<g/>
bude	být	k5eAaImBp3nS	být
chodit	chodit	k5eAaImF	chodit
<g/>
)	)	kIx)	)
×	×	k?	×
pojde	pojít	k5eAaPmIp3nS	pojít
(	(	kIx(	(
<g/>
zhyne	zhynout	k5eAaPmIp3nS	zhynout
<g/>
/	/	kIx~	/
<g/>
vzejde	vzejít	k5eAaPmIp3nS	vzejít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pojď	jít	k5eAaImRp2nS	jít
×	×	k?	×
pojdi	pojít	k5eAaPmRp2nS	pojít
<g/>
,	,	kIx,	,
povede	vést	k5eAaImIp3nS	vést
(	(	kIx(	(
<g/>
bude	být	k5eAaImBp3nS	být
vést	vést	k5eAaImF	vést
<g/>
)	)	kIx)	)
×	×	k?	×
povede	povést	k5eAaPmIp3nS	povést
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
vydaří	vydařit	k5eAaPmIp3nS	vydařit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
inf.	inf.	k?	inf.
povést	povést	k5eAaPmF	povést
se	se	k3xPyFc4	se
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Nepravidelný	pravidelný	k2eNgInSc1d1	nepravidelný
zápor	zápor	k1gInSc1	zápor
===	===	k?	===
</s>
</p>
<p>
<s>
být	být	k5eAaImF	být
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
osoba	osoba	k1gFnSc1	osoba
jedn	jedn	k1gMnSc1	jedn
<g/>
.	.	kIx.	.
č.	č.	k?	č.
přít	přít	k5eAaImF	přít
<g/>
.	.	kIx.	.
času	čas	k1gInSc3	čas
<g/>
:	:	kIx,	:
není	být	k5eNaImIp3nS	být
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Přísudek	přísudek	k1gInSc1	přísudek
</s>
</p>
<p>
<s>
Sloveso	sloveso	k1gNnSc1	sloveso
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
latinských	latinský	k2eAgInPc2d1	latinský
gramatických	gramatický	k2eAgInPc2d1	gramatický
pojmů	pojem	k1gInPc2	pojem
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
LEHEČKOVÁ	LEHEČKOVÁ	kA	LEHEČKOVÁ
<g/>
,	,	kIx,	,
Helena	Helena	k1gFnSc1	Helena
<g/>
.	.	kIx.	.
</s>
<s>
Tšekkiä	Tšekkiä	k?	Tšekkiä
suomalaisille	suomalaisille	k1gInSc1	suomalaisille
<g/>
.	.	kIx.	.
</s>
<s>
Helsinky	Helsinky	k1gFnPc1	Helsinky
<g/>
:	:	kIx,	:
Finn	Finn	k1gNnSc1	Finn
Lectura	Lectura	k1gFnSc1	Lectura
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
951	[number]	k4	951
<g/>
-	-	kIx~	-
<g/>
792	[number]	k4	792
<g/>
-	-	kIx~	-
<g/>
235	[number]	k4	235
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NOVOTNÝ	Novotný	k1gMnSc1	Novotný
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
.	.	kIx.	.
</s>
<s>
Mluvnice	mluvnice	k1gFnSc1	mluvnice
češtiny	čeština	k1gFnSc2	čeština
pro	pro	k7c4	pro
střední	střední	k2eAgFnPc4d1	střední
školy	škola	k1gFnPc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Fortuna	Fortuna	k1gFnSc1	Fortuna
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85298	[number]	k4	85298
<g/>
-	-	kIx~	-
<g/>
32	[number]	k4	32
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příruční	příruční	k2eAgFnSc1d1	příruční
mluvnice	mluvnice	k1gFnSc1	mluvnice
češtiny	čeština	k1gFnSc2	čeština
<g/>
,	,	kIx,	,
NLN	NLN	kA	NLN
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1995	[number]	k4	1995
</s>
</p>
<p>
<s>
REŠKOVÁ	REŠKOVÁ	kA	REŠKOVÁ
<g/>
,	,	kIx,	,
Ivana	Ivana	k1gFnSc1	Ivana
<g/>
.	.	kIx.	.
</s>
<s>
Communicative	Communicativ	k1gInSc5	Communicativ
Czech	Czech	k1gMnSc1	Czech
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Karlova	Karlův	k2eAgFnSc1d1	Karlova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7184	[number]	k4	7184
<g/>
-	-	kIx~	-
<g/>
712	[number]	k4	712
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnSc2	kategorie
Česká	český	k2eAgNnPc1d1	české
slovesa	sloveso	k1gNnPc1	sloveso
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Frekvence	frekvence	k1gFnSc1	frekvence
vzorů	vzor	k1gInPc2	vzor
českých	český	k2eAgInPc2d1	český
sloves	sloveso	k1gNnPc2	sloveso
</s>
</p>
