<s>
Pražský	pražský	k2eAgInSc1d1	pražský
hrad	hrad	k1gInSc1	hrad
je	být	k5eAaImIp3nS	být
nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
český	český	k2eAgInSc1d1	český
hrad	hrad	k1gInSc1	hrad
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
raně	raně	k6eAd1	raně
středověké	středověký	k2eAgNnSc4d1	středověké
hradiště	hradiště	k1gNnSc4	hradiště
<g/>
)	)	kIx)	)
stojící	stojící	k2eAgMnSc1d1	stojící
na	na	k7c6	na
skalnatém	skalnatý	k2eAgInSc6d1	skalnatý
ostrohu	ostroh	k1gInSc6	ostroh
nad	nad	k7c7	nad
řekou	řeka	k1gFnSc7	řeka
Vltavou	Vltava	k1gFnSc7	Vltava
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
na	na	k7c6	na
vrchu	vrch	k1gInSc6	vrch
Opyš	Opyš	k1gInSc1	Opyš
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
býval	bývat	k5eAaImAgInS	bývat
sídlem	sídlo	k1gNnSc7	sídlo
českých	český	k2eAgNnPc2d1	české
knížat	kníže	k1gNnPc2	kníže
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
králů	král	k1gMnPc2	král
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Dvakrát	dvakrát	k6eAd1	dvakrát
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
hlavní	hlavní	k2eAgFnSc7d1	hlavní
rezidencí	rezidence	k1gFnSc7	rezidence
císaře	císař	k1gMnSc2	císař
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
<g/>
.	.	kIx.	.
</s>
<s>
Postupnými	postupný	k2eAgFnPc7d1	postupná
přístavbami	přístavba	k1gFnPc7	přístavba
a	a	k8xC	a
úpravami	úprava	k1gFnPc7	úprava
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
z	z	k7c2	z
hradiště	hradiště	k1gNnSc2	hradiště
založeného	založený	k2eAgNnSc2d1	založené
v	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
rozměry	rozměr	k1gInPc7	rozměr
570	[number]	k4	570
m	m	kA	m
délky	délka	k1gFnSc2	délka
a	a	k8xC	a
128	[number]	k4	128
m	m	kA	m
šířky	šířka	k1gFnSc2	šířka
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
hradních	hradní	k2eAgInPc2d1	hradní
komplexů	komplex	k1gInPc2	komplex
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
hradu	hrad	k1gInSc2	hrad
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
uvádí	uvádět	k5eAaImIp3nS	uvádět
7,28	[number]	k4	7,28
ha	ha	kA	ha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
předmětem	předmět	k1gInSc7	předmět
sporů	spor	k1gInPc2	spor
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
nejen	nejen	k6eAd1	nejen
za	za	k7c4	za
symbol	symbol	k1gInSc4	symbol
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
české	český	k2eAgFnSc2d1	Česká
státnosti	státnost	k1gFnSc2	státnost
a	a	k8xC	a
podle	podle	k7c2	podle
Guinessovy	Guinessův	k2eAgFnSc2d1	Guinessova
knihy	kniha	k1gFnSc2	kniha
rekordů	rekord	k1gInPc2	rekord
za	za	k7c4	za
největší	veliký	k2eAgInSc4d3	veliký
starobylý	starobylý	k2eAgInSc4d1	starobylý
hrad	hrad	k1gInSc4	hrad
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc4d3	veliký
doposud	doposud	k6eAd1	doposud
obývaný	obývaný	k2eAgInSc4d1	obývaný
hrad	hrad	k1gInSc4	hrad
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
za	za	k7c7	za
ním	on	k3xPp3gInSc7	on
je	být	k5eAaImIp3nS	být
Anglický	anglický	k2eAgInSc1d1	anglický
Windsor	Windsor	k1gInSc1	Windsor
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
hradu	hrad	k1gInSc2	hrad
je	být	k5eAaImIp3nS	být
Katedrála	katedrála	k1gFnSc1	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Víta	Vít	k1gMnSc2	Vít
<g/>
,	,	kIx,	,
tradiční	tradiční	k2eAgNnSc1d1	tradiční
místo	místo	k1gNnSc1	místo
korunovací	korunovace	k1gFnPc2	korunovace
českých	český	k2eAgMnPc2d1	český
králů	král	k1gMnPc2	král
i	i	k9	i
jejich	jejich	k3xOp3gInSc2	jejich
posledního	poslední	k2eAgInSc2d1	poslední
odpočinku	odpočinek	k1gInSc2	odpočinek
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
mnohých	mnohý	k2eAgMnPc2d1	mnohý
českých	český	k2eAgMnPc2d1	český
panovníků	panovník	k1gMnPc2	panovník
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
čtyř	čtyři	k4xCgMnPc2	čtyři
císařů	císař	k1gMnPc2	císař
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pražských	pražský	k2eAgMnPc2d1	pražský
biskupů	biskup	k1gMnPc2	biskup
a	a	k8xC	a
arcibiskupů	arcibiskup	k1gMnPc2	arcibiskup
<g/>
,	,	kIx,	,
duchovních	duchovní	k1gMnPc2	duchovní
<g/>
,	,	kIx,	,
šlechticů	šlechtic	k1gMnPc2	šlechtic
i	i	k8xC	i
jiných	jiný	k2eAgMnPc2d1	jiný
lidí	člověk	k1gMnPc2	člověk
spjatých	spjatý	k2eAgMnPc2d1	spjatý
s	s	k7c7	s
pražským	pražský	k2eAgInSc7d1	pražský
dvorem	dvůr	k1gInSc7	dvůr
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
pochováni	pochovat	k5eAaPmNgMnP	pochovat
i	i	k9	i
někteří	některý	k3yIgMnPc1	některý
světci	světec	k1gMnPc1	světec
-	-	kIx~	-
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
českých	český	k2eAgMnPc2d1	český
zemských	zemský	k2eAgMnPc2d1	zemský
patronů	patron	k1gMnPc2	patron
(	(	kIx(	(
<g/>
sv.	sv.	kA	sv.
Václav	Václav	k1gMnSc1	Václav
<g/>
,	,	kIx,	,
Vít	Vít	k1gMnSc1	Vít
<g/>
,	,	kIx,	,
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
<g/>
,	,	kIx,	,
Zikmund	Zikmund	k1gMnSc1	Zikmund
nebo	nebo	k8xC	nebo
Jan	Jan	k1gMnSc1	Jan
Nepomucký	Nepomucký	k1gMnSc1	Nepomucký
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
také	také	k6eAd1	také
uloženy	uložit	k5eAaPmNgInP	uložit
české	český	k2eAgInPc1d1	český
korunovační	korunovační	k2eAgInPc1d1	korunovační
klenoty	klenot	k1gInPc1	klenot
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
"	"	kIx"	"
vztahoval	vztahovat	k5eAaImAgInS	vztahovat
především	především	k9	především
právě	právě	k9	právě
na	na	k7c4	na
skalnatý	skalnatý	k2eAgInSc4d1	skalnatý
ostroh	ostroh	k1gInSc4	ostroh
s	s	k7c7	s
knížecím	knížecí	k2eAgNnSc7d1	knížecí
hradištěm	hradiště	k1gNnSc7	hradiště
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
však	však	k9	však
současně	současně	k6eAd1	současně
označovat	označovat	k5eAaImF	označovat
i	i	k9	i
okolní	okolní	k2eAgNnSc1d1	okolní
osídlení	osídlení	k1gNnSc1	osídlení
(	(	kIx(	(
<g/>
např.	např.	kA	např.
u	u	k7c2	u
Ibráhíma	Ibráhí	k2eAgFnPc7d1	Ibráhí
ibn	ibn	k?	ibn
Jakúba	Jakúb	k1gMnSc2	Jakúb
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
po	po	k7c6	po
staletí	staletí	k1gNnSc6	staletí
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
"	"	kIx"	"
vztahoval	vztahovat	k5eAaImAgInS	vztahovat
na	na	k7c4	na
hrad	hrad	k1gInSc4	hrad
i	i	k8xC	i
pražská	pražský	k2eAgNnPc4d1	Pražské
města	město	k1gNnPc4	město
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1784	[number]	k4	1784
byla	být	k5eAaImAgFnS	být
samostatná	samostatný	k2eAgFnSc1d1	samostatná
města	město	k1gNnSc2	město
administrativně	administrativně	k6eAd1	administrativně
spojena	spojit	k5eAaPmNgFnS	spojit
a	a	k8xC	a
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
"	"	kIx"	"
oficiálně	oficiálně	k6eAd1	oficiálně
dostalo	dostat	k5eAaPmAgNnS	dostat
takto	takto	k6eAd1	takto
vzniklé	vzniklý	k2eAgNnSc1d1	vzniklé
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Pražského	pražský	k2eAgInSc2d1	pražský
"	"	kIx"	"
<g/>
hradu	hrad	k1gInSc2	hrad
<g/>
"	"	kIx"	"
získalo	získat	k5eAaPmAgNnS	získat
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
Hradčany	Hradčany	k1gInPc1	Hradčany
<g/>
"	"	kIx"	"
osídlení	osídlení	k1gNnSc1	osídlení
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
západním	západní	k2eAgNnSc6d1	západní
předpolí	předpolí	k1gNnSc6	předpolí
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
poddanským	poddanský	k2eAgNnPc3d1	poddanské
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
královským	královský	k2eAgNnSc7d1	královské
městem	město	k1gNnSc7	město
spadající	spadající	k2eAgInSc4d1	spadající
pod	pod	k7c4	pod
zprávu	zpráva	k1gFnSc4	zpráva
purkrabího	purkrabí	k1gMnSc2	purkrabí
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
pražská	pražský	k2eAgFnSc1d1	Pražská
čtvrť	čtvrť	k1gFnSc1	čtvrť
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
kromě	kromě	k7c2	kromě
historického	historický	k2eAgNnSc2d1	historické
města	město	k1gNnSc2	město
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
území	území	k1gNnSc4	území
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Ostrožna	ostrožna	k1gFnSc1	ostrožna
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
byla	být	k5eAaImAgNnP	být
osídlena	osídlit	k5eAaPmNgNnP	osídlit
již	již	k6eAd1	již
v	v	k7c6	v
období	období	k1gNnSc6	období
neolitu	neolit	k1gInSc2	neolit
<g/>
.	.	kIx.	.
</s>
<s>
Archeologicky	archeologicky	k6eAd1	archeologicky
byla	být	k5eAaImAgNnP	být
prozkoumána	prozkoumat	k5eAaPmNgNnP	prozkoumat
sídliště	sídliště	k1gNnPc1	sídliště
kultury	kultura	k1gFnSc2	kultura
s	s	k7c7	s
lineární	lineární	k2eAgFnSc7d1	lineární
keramikou	keramika	k1gFnSc7	keramika
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
s	s	k7c7	s
vypíchanou	vypíchaný	k2eAgFnSc7d1	vypíchaná
keramikou	keramika	k1gFnSc7	keramika
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
Lumbeho	Lumbe	k1gMnSc2	Lumbe
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Lumbeho	Lumbe	k1gMnSc2	Lumbe
zahrady	zahrada	k1gFnSc2	zahrada
pocházejí	pocházet	k5eAaImIp3nP	pocházet
i	i	k9	i
doklady	doklad	k1gInPc1	doklad
o	o	k7c4	o
osídlení	osídlení	k1gNnSc4	osídlení
v	v	k7c6	v
období	období	k1gNnSc6	období
eneolitu	eneolit	k1gInSc2	eneolit
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
zde	zde	k6eAd1	zde
jsou	být	k5eAaImIp3nP	být
doloženy	doložen	k2eAgFnPc1d1	doložena
pohřební	pohřební	k2eAgFnPc1d1	pohřební
aktivity	aktivita	k1gFnPc1	aktivita
kultury	kultura	k1gFnSc2	kultura
se	s	k7c7	s
šňůrovou	šňůrový	k2eAgFnSc7d1	šňůrová
keramikou	keramika	k1gFnSc7	keramika
a	a	k8xC	a
únětické	únětický	k2eAgFnSc2d1	únětická
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
archeologicky	archeologicky	k6eAd1	archeologicky
podařilo	podařit	k5eAaPmAgNnS	podařit
prokázat	prokázat	k5eAaPmF	prokázat
existenci	existence	k1gFnSc4	existence
osídlení	osídlení	k1gNnSc2	osídlení
staršího	starší	k1gMnSc2	starší
než	než	k8xS	než
dokládají	dokládat	k5eAaImIp3nP	dokládat
písemné	písemný	k2eAgInPc4d1	písemný
prameny	pramen	k1gInPc4	pramen
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zmiňují	zmiňovat	k5eAaImIp3nP	zmiňovat
až	až	k6eAd1	až
kostel	kostel	k1gInSc4	kostel
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
postavený	postavený	k2eAgInSc4d1	postavený
přemyslovským	přemyslovský	k2eAgMnSc7d1	přemyslovský
knížetem	kníže	k1gMnSc7	kníže
Bořivojem	Bořivoj	k1gMnSc7	Bořivoj
někdy	někdy	k6eAd1	někdy
po	po	k7c6	po
roce	rok	k1gInSc6	rok
885	[number]	k4	885
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
snad	snad	k9	snad
už	už	k9	už
i	i	k9	i
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
ostrožna	ostrožna	k1gFnSc1	ostrožna
předělena	předělit	k5eAaPmNgFnS	předělit
příkopem	příkop	k1gInSc7	příkop
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
uvnitř	uvnitř	k7c2	uvnitř
tohoto	tento	k3xDgNnSc2	tento
opevnění	opevnění	k1gNnSc2	opevnění
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
příkopu	příkop	k1gInSc6	příkop
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
dřev	dřevo	k1gNnPc2	dřevo
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mohla	moct	k5eAaImAgFnS	moct
indikovat	indikovat	k5eAaBmF	indikovat
přítomnost	přítomnost	k1gFnSc1	přítomnost
dřevěné	dřevěný	k2eAgFnSc2d1	dřevěná
palisády	palisáda	k1gFnSc2	palisáda
<g/>
,	,	kIx,	,
našla	najít	k5eAaPmAgFnS	najít
i	i	k9	i
keramika	keramika	k1gFnSc1	keramika
a	a	k8xC	a
např.	např.	kA	např.
esovitá	esovitý	k2eAgFnSc1d1	esovitá
náušnice	náušnice	k1gFnSc1	náušnice
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spojitosti	spojitost	k1gFnSc6	spojitost
s	s	k7c7	s
osídlením	osídlení	k1gNnSc7	osídlení
areálu	areál	k1gInSc2	areál
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
před	před	k7c7	před
vznikem	vznik	k1gInSc7	vznik
přemyslovského	přemyslovský	k2eAgNnSc2d1	přemyslovské
sídla	sídlo	k1gNnSc2	sídlo
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
i	i	k9	i
archeologicky	archeologicky	k6eAd1	archeologicky
prozkoumané	prozkoumaný	k2eAgNnSc4d1	prozkoumané
pohřebiště	pohřebiště	k1gNnSc4	pohřebiště
na	na	k7c6	na
III	III	kA	III
<g/>
.	.	kIx.	.
nádvoří	nádvoří	k1gNnSc1	nádvoří
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
jako	jako	k9	jako
jediné	jediný	k2eAgNnSc1d1	jediné
neváže	vázat	k5eNaImIp3nS	vázat
na	na	k7c4	na
kostelní	kostelní	k2eAgFnSc4d1	kostelní
stavbu	stavba	k1gFnSc4	stavba
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
datováno	datovat	k5eAaImNgNnS	datovat
už	už	k6eAd1	už
do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
Ivanem	Ivan	k1gMnSc7	Ivan
Borkovským	Borkovský	k2eAgMnSc7d1	Borkovský
objeven	objevit	k5eAaPmNgInS	objevit
hrob	hrob	k1gInSc4	hrob
bojovníka	bojovník	k1gMnSc2	bojovník
s	s	k7c7	s
bohatou	bohatý	k2eAgFnSc7d1	bohatá
výbavou	výbava	k1gFnSc7	výbava
(	(	kIx(	(
<g/>
železný	železný	k2eAgInSc1d1	železný
meč	meč	k1gInSc1	meč
<g/>
,	,	kIx,	,
sekera	sekera	k1gFnSc1	sekera
<g/>
,	,	kIx,	,
dýka	dýka	k1gFnSc1	dýka
<g/>
,	,	kIx,	,
nůž	nůž	k1gInSc1	nůž
<g/>
,	,	kIx,	,
křesací	křesací	k2eAgFnSc1d1	křesací
souprava	souprava	k1gFnSc1	souprava
<g/>
,	,	kIx,	,
železná	železný	k2eAgFnSc1d1	železná
břitva	břitva	k1gFnSc1	břitva
<g/>
,	,	kIx,	,
dřevěné	dřevěný	k2eAgNnSc1d1	dřevěné
vědro	vědro	k1gNnSc1	vědro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
nález	nález	k1gInSc1	nález
dokládá	dokládat	k5eAaImIp3nS	dokládat
přítomnost	přítomnost	k1gFnSc4	přítomnost
elity	elita	k1gFnSc2	elita
na	na	k7c6	na
ostrožně	ostrožna	k1gFnSc6	ostrožna
již	již	k6eAd1	již
před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
Bořivoje	Bořivoj	k1gMnSc2	Bořivoj
<g/>
.	.	kIx.	.
</s>
<s>
Opevňování	opevňování	k1gNnSc1	opevňování
sídliště	sídliště	k1gNnSc2	sídliště
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
hradiště	hradiště	k1gNnSc2	hradiště
zahájil	zahájit	k5eAaPmAgMnS	zahájit
nejspíš	nejspíš	k9	nejspíš
Bořivojův	Bořivojův	k2eAgMnSc1d1	Bořivojův
syn	syn	k1gMnSc1	syn
Spytihněv	Spytihněv	k1gFnSc2	Spytihněv
I.	I.	kA	I.
Přemyslovské	přemyslovský	k2eAgNnSc1d1	přemyslovské
hradiště	hradiště	k1gNnSc1	hradiště
bylo	být	k5eAaImAgNnS	být
podobně	podobně	k6eAd1	podobně
velké	velký	k2eAgNnSc1d1	velké
jako	jako	k8xS	jako
dnešní	dnešní	k2eAgFnSc1d1	dnešní
rozloha	rozloha	k1gFnSc1	rozloha
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
západní	západní	k2eAgFnSc2d1	západní
strany	strana	k1gFnSc2	strana
byl	být	k5eAaImAgInS	být
obklopen	obklopit	k5eAaPmNgInS	obklopit
strmými	strmý	k2eAgInPc7d1	strmý
svahy	svah	k1gInPc7	svah
a	a	k8xC	a
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
Jeleního	jelení	k2eAgInSc2d1	jelení
příkopu	příkop	k1gInSc2	příkop
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
tekl	téct	k5eAaImAgMnS	téct
potok	potok	k1gInSc4	potok
Brusnice	brusnice	k1gFnSc2	brusnice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nynějším	nynější	k2eAgNnSc7d1	nynější
Hradčanským	hradčanský	k2eAgNnSc7d1	Hradčanské
náměstím	náměstí	k1gNnSc7	náměstí
a	a	k8xC	a
prvním	první	k4xOgMnSc6	první
hradním	hradní	k2eAgNnPc3d1	hradní
nádvořím	nádvoří	k1gNnPc3	nádvoří
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
z	z	k7c2	z
obranných	obranný	k2eAgInPc2d1	obranný
důvodů	důvod	k1gInPc2	důvod
vykopán	vykopat	k5eAaPmNgInS	vykopat
příkop	příkop	k1gInSc1	příkop
30	[number]	k4	30
metrů	metr	k1gInPc2	metr
hluboký	hluboký	k2eAgMnSc1d1	hluboký
a	a	k8xC	a
24	[number]	k4	24
metrů	metr	k1gInPc2	metr
široký	široký	k2eAgMnSc1d1	široký
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
který	který	k3yRgInSc4	který
vedl	vést	k5eAaImAgInS	vést
most	most	k1gInSc1	most
do	do	k7c2	do
hlavní	hlavní	k2eAgFnSc2d1	hlavní
brány	brána	k1gFnSc2	brána
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
kolem	kolem	k7c2	kolem
hradu	hrad	k1gInSc2	hrad
byly	být	k5eAaImAgFnP	být
dřevěné	dřevěný	k2eAgFnPc1d1	dřevěná
hradby	hradba	k1gFnPc1	hradba
na	na	k7c6	na
hliněných	hliněný	k2eAgInPc6d1	hliněný
valech	val	k1gInPc6	val
a	a	k8xC	a
několik	několik	k4yIc4	několik
strážních	strážní	k2eAgFnPc2d1	strážní
věží	věž	k1gFnPc2	věž
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
tři	tři	k4xCgInPc1	tři
sloužily	sloužit	k5eAaImAgInP	sloužit
také	také	k9	také
jako	jako	k9	jako
brány	brána	k1gFnPc1	brána
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
západní	západní	k2eAgNnSc4d1	západní
a	a	k8xC	a
východní	východní	k2eAgFnSc1d1	východní
vedla	vést	k5eAaImAgFnS	vést
dlážděná	dlážděný	k2eAgFnSc1d1	dlážděná
cesta	cesta	k1gFnSc1	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Vratislava	Vratislav	k1gMnSc2	Vratislav
I.	I.	kA	I.
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
postaven	postavit	k5eAaPmNgInS	postavit
druhý	druhý	k4xOgInSc1	druhý
kostel	kostel	k1gInSc1	kostel
a	a	k8xC	a
zasvěcen	zasvětit	k5eAaPmNgInS	zasvětit
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
plnil	plnit	k5eAaImAgInS	plnit
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
973	[number]	k4	973
funkci	funkce	k1gFnSc4	funkce
hlavního	hlavní	k2eAgInSc2d1	hlavní
kostela	kostel	k1gInSc2	kostel
knížecího	knížecí	k2eAgInSc2d1	knížecí
paláce	palác	k1gInSc2	palác
i	i	k8xC	i
celých	celý	k2eAgFnPc2d1	celá
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Kníže	kníže	k1gMnSc1	kníže
Václav	Václav	k1gMnSc1	Václav
zde	zde	k6eAd1	zde
nechal	nechat	k5eAaPmAgMnS	nechat
vystavět	vystavět	k5eAaPmF	vystavět
další	další	k2eAgInSc4d1	další
církevní	církevní	k2eAgInSc4d1	církevní
objekt	objekt	k1gInSc4	objekt
<g/>
,	,	kIx,	,
rotundu	rotunda	k1gFnSc4	rotunda
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
uložil	uložit	k5eAaPmAgInS	uložit
ostatky	ostatek	k1gInPc4	ostatek
světce	světec	k1gMnSc2	světec
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
získal	získat	k5eAaPmAgInS	získat
darem	dar	k1gInSc7	dar
od	od	k7c2	od
východofranského	východofranský	k2eAgMnSc2d1	východofranský
krále	král	k1gMnSc2	král
Jindřicha	Jindřich	k1gMnSc2	Jindřich
I.	I.	kA	I.
Ptáčníka	Ptáčník	k1gMnSc2	Ptáčník
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
založení	založení	k1gNnSc6	založení
pražského	pražský	k2eAgNnSc2d1	Pražské
biskupství	biskupství	k1gNnSc2	biskupství
roku	rok	k1gInSc2	rok
973	[number]	k4	973
se	se	k3xPyFc4	se
rotunda	rotunda	k1gFnSc1	rotunda
stala	stát	k5eAaPmAgFnS	stát
metropolitním	metropolitní	k2eAgInSc7d1	metropolitní
biskupským	biskupský	k2eAgInSc7d1	biskupský
kostelem	kostel	k1gInSc7	kostel
a	a	k8xC	a
nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
chrámem	chrám	k1gInSc7	chrám
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Církevní	církevní	k2eAgFnPc1d1	církevní
stavby	stavba	k1gFnPc1	stavba
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
budovány	budovat	k5eAaImNgFnP	budovat
z	z	k7c2	z
kamene	kámen	k1gInSc2	kámen
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
knížecí	knížecí	k2eAgInSc1d1	knížecí
palác	palác	k1gInSc1	palác
a	a	k8xC	a
sruby	srub	k1gInPc1	srub
pro	pro	k7c4	pro
služebnictvo	služebnictvo	k1gNnSc4	služebnictvo
byly	být	k5eAaImAgFnP	být
ještě	ještě	k6eAd1	ještě
dřevěné	dřevěný	k2eAgFnPc1d1	dřevěná
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
970	[number]	k4	970
byl	být	k5eAaImAgInS	být
u	u	k7c2	u
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc1	Jiří
založen	založit	k5eAaPmNgInS	založit
ženský	ženský	k2eAgInSc1d1	ženský
benediktinský	benediktinský	k2eAgInSc1d1	benediktinský
klášter	klášter	k1gInSc1	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
zahájil	zahájit	k5eAaPmAgMnS	zahájit
Boleslav	Boleslav	k1gMnSc1	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
velkolepou	velkolepý	k2eAgFnSc4d1	velkolepá
přestavbu	přestavba	k1gFnSc4	přestavba
kostela	kostel	k1gInSc2	kostel
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gFnSc6	jehož
místně	místně	k6eAd1	místně
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
monumentální	monumentální	k2eAgFnSc1d1	monumentální
předrománská	předrománský	k2eAgFnSc1d1	předrománská
bazilika	bazilika	k1gFnSc1	bazilika
o	o	k7c6	o
třech	tři	k4xCgFnPc6	tři
lodích	loď	k1gFnPc6	loď
<g/>
.	.	kIx.	.
</s>
<s>
Sloužila	sloužit	k5eAaImAgFnS	sloužit
také	také	k9	také
jako	jako	k9	jako
pohřebiště	pohřebiště	k1gNnSc4	pohřebiště
pro	pro	k7c4	pro
členy	člen	k1gMnPc4	člen
panovnické	panovnický	k2eAgFnSc2d1	panovnická
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
knížete	kníže	k1gNnSc2wR	kníže
a	a	k8xC	a
prvního	první	k4xOgMnSc2	první
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
Vratislava	Vratislav	k1gMnSc2	Vratislav
II	II	kA	II
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
jako	jako	k9	jako
rezidence	rezidence	k1gFnSc1	rezidence
panovníka	panovník	k1gMnSc2	panovník
uplatnil	uplatnit	k5eAaPmAgMnS	uplatnit
na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
prováděly	provádět	k5eAaImAgFnP	provádět
stavební	stavební	k2eAgFnPc1d1	stavební
úpravy	úprava	k1gFnPc1	úprava
<g/>
.	.	kIx.	.
</s>
<s>
Dřevěné	dřevěný	k2eAgNnSc1d1	dřevěné
opevnění	opevnění	k1gNnSc1	opevnění
vystřídalo	vystřídat	k5eAaPmAgNnS	vystřídat
kamenné	kamenný	k2eAgNnSc1d1	kamenné
s	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
branami	brána	k1gFnPc7	brána
<g/>
:	:	kIx,	:
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Černá	černý	k2eAgFnSc1d1	černá
věž	věž	k1gFnSc1	věž
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
Bílá	bílý	k2eAgFnSc1d1	bílá
a	a	k8xC	a
jako	jako	k9	jako
postranní	postranní	k2eAgInSc4d1	postranní
vchod	vchod	k1gInSc4	vchod
sloužila	sloužit	k5eAaImAgFnS	sloužit
věž	věž	k1gFnSc1	věž
Jižní	jižní	k2eAgFnSc1d1	jižní
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1060	[number]	k4	1060
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
příkazu	příkaz	k1gInSc2	příkaz
Spytihněva	Spytihněv	k1gMnSc2	Spytihněv
II	II	kA	II
<g/>
.	.	kIx.	.
zbourána	zbourán	k2eAgFnSc1d1	zbourána
rotunda	rotunda	k1gFnSc1	rotunda
sv.	sv.	kA	sv.
Víta	Víta	k1gFnSc1	Víta
a	a	k8xC	a
na	na	k7c4	na
její	její	k3xOp3gNnSc4	její
místo	místo	k1gNnSc4	místo
postavena	postaven	k2eAgFnSc1d1	postavena
bazilika	bazilika	k1gFnSc1	bazilika
<g/>
,	,	kIx,	,
předchůdkyně	předchůdkyně	k1gFnSc1	předchůdkyně
gotické	gotický	k2eAgFnSc2d1	gotická
katedrály	katedrála	k1gFnSc2	katedrála
<g/>
,	,	kIx,	,
zasvěcená	zasvěcený	k2eAgFnSc1d1	zasvěcená
Sv.	sv.	kA	sv.
Vítu	Víta	k1gFnSc4	Víta
<g/>
,	,	kIx,	,
Václavu	Václav	k1gMnSc3	Václav
a	a	k8xC	a
Vojtěchu	Vojtěch	k1gMnSc3	Vojtěch
<g/>
.	.	kIx.	.
</s>
<s>
Trojlodní	trojlodní	k2eAgInSc1d1	trojlodní
románský	románský	k2eAgInSc1d1	románský
chrám	chrám	k1gInSc1	chrám
z	z	k7c2	z
bílé	bílý	k2eAgFnSc2d1	bílá
opuky	opuka	k1gFnSc2	opuka
byl	být	k5eAaImAgInS	být
70	[number]	k4	70
metrů	metr	k1gInPc2	metr
dlouhý	dlouhý	k2eAgMnSc1d1	dlouhý
a	a	k8xC	a
na	na	k7c4	na
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
dobu	doba	k1gFnSc4	doba
v	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
zemích	zem	k1gFnPc6	zem
zcela	zcela	k6eAd1	zcela
nevídaný	vídaný	k2eNgMnSc1d1	nevídaný
<g/>
.	.	kIx.	.
</s>
<s>
Kostelu	kostel	k1gInSc3	kostel
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnPc3	Jiří
přibyly	přibýt	k5eAaPmAgFnP	přibýt
dvě	dva	k4xCgFnPc1	dva
románské	románský	k2eAgFnPc1d1	románská
věže	věž	k1gFnPc1	věž
a	a	k8xC	a
dřevěný	dřevěný	k2eAgInSc1d1	dřevěný
palác	palác	k1gInSc1	palác
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
zděným	zděný	k2eAgInSc7d1	zděný
<g/>
.	.	kIx.	.
</s>
<s>
Přestavbu	přestavba	k1gFnSc4	přestavba
hradiště	hradiště	k1gNnSc2	hradiště
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
kamenným	kamenný	k2eAgInSc7d1	kamenný
hradbami	hradba	k1gFnPc7	hradba
opevněného	opevněný	k2eAgInSc2d1	opevněný
hradu	hrad	k1gInSc2	hrad
zahájil	zahájit	k5eAaPmAgMnS	zahájit
1135	[number]	k4	1135
kníže	kníže	k1gMnSc1	kníže
Soběslav	Soběslav	k1gMnSc1	Soběslav
I.	I.	kA	I.
Západní	západní	k2eAgFnPc1d1	západní
hradby	hradba	k1gFnPc1	hradba
probíhaly	probíhat	k5eAaImAgFnP	probíhat
mezi	mezi	k7c7	mezi
dnešním	dnešní	k2eAgMnSc7d1	dnešní
druhým	druhý	k4xOgNnSc7	druhý
a	a	k8xC	a
třetím	třetí	k4xOgNnSc7	třetí
nádvořím	nádvoří	k1gNnSc7	nádvoří
<g/>
,	,	kIx,	,
ze	z	k7c2	z
západní	západní	k2eAgFnSc2d1	západní
části	část	k1gFnSc2	část
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
předhradí	předhradí	k1gNnSc1	předhradí
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejváženějších	vážený	k2eAgMnPc2d3	nejváženější
panovníků	panovník	k1gMnPc2	panovník
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgInSc3	ten
odpovídaly	odpovídat	k5eAaImAgFnP	odpovídat
i	i	k9	i
jeho	jeho	k3xOp3gFnPc1	jeho
přestavby	přestavba	k1gFnPc1	přestavba
na	na	k7c6	na
Hradě	hrad	k1gInSc6	hrad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgNnPc6	první
letech	léto	k1gNnPc6	léto
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
zaměřil	zaměřit	k5eAaPmAgInS	zaměřit
na	na	k7c4	na
zdokonalování	zdokonalování	k1gNnSc4	zdokonalování
opevnění	opevnění	k1gNnSc2	opevnění
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c6	na
té	ten	k3xDgFnSc6	ten
nejcitlivější	citlivý	k2eAgFnSc6d3	nejcitlivější
západní	západní	k2eAgFnSc6d1	západní
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgFnP	být
rozšířeny	rozšířit	k5eAaPmNgInP	rozšířit
příkopy	příkop	k1gInPc1	příkop
<g/>
,	,	kIx,	,
a	a	k8xC	a
vchod	vchod	k1gInSc1	vchod
v	v	k7c6	v
Černé	Černé	k2eAgFnSc6d1	Černé
věži	věž	k1gFnSc6	věž
na	na	k7c6	na
východě	východ	k1gInSc6	východ
zrušil	zrušit	k5eAaPmAgMnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Nechal	nechat	k5eAaPmAgMnS	nechat
přestavět	přestavět	k5eAaPmF	přestavět
královský	královský	k2eAgInSc4d1	královský
palác	palác	k1gInSc4	palác
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
reprezentace	reprezentace	k1gFnSc2	reprezentace
a	a	k8xC	a
bydlení	bydlení	k1gNnSc2	bydlení
<g/>
.	.	kIx.	.
</s>
<s>
Gotický	gotický	k2eAgInSc1d1	gotický
rozmach	rozmach	k1gInSc1	rozmach
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Lucemburků	Lucemburk	k1gInPc2	Lucemburk
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začala	začít	k5eAaPmAgFnS	začít
nákladná	nákladný	k2eAgFnSc1d1	nákladná
přestavba	přestavba	k1gFnSc1	přestavba
královského	královský	k2eAgInSc2d1	královský
paláce	palác	k1gInSc2	palác
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
francouzských	francouzský	k2eAgInPc2d1	francouzský
paláců	palác	k1gInPc2	palác
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
bylo	být	k5eAaImAgNnS	být
přestavěno	přestavěn	k2eAgNnSc1d1	přestavěno
románské	románský	k2eAgNnSc1d1	románské
patro	patro	k1gNnSc1	patro
a	a	k8xC	a
palác	palác	k1gInSc1	palác
byl	být	k5eAaImAgInS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
arkádami	arkáda	k1gFnPc7	arkáda
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
tak	tak	k6eAd1	tak
umožnily	umožnit	k5eAaPmAgFnP	umožnit
vybudování	vybudování	k1gNnSc4	vybudování
dalšího	další	k2eAgNnSc2d1	další
rozměrného	rozměrný	k2eAgNnSc2d1	rozměrné
reprezentačního	reprezentační	k2eAgNnSc2d1	reprezentační
patra	patro	k1gNnSc2	patro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
patře	patro	k1gNnSc6	patro
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
především	především	k9	především
tzv.	tzv.	kA	tzv.
Velký	velký	k2eAgInSc1d1	velký
sál	sál	k1gInSc1	sál
<g/>
,	,	kIx,	,
předchůdce	předchůdce	k1gMnSc1	předchůdce
Vladislavského	vladislavský	k2eAgMnSc2d1	vladislavský
a	a	k8xC	a
palácová	palácový	k2eAgFnSc1d1	palácová
arkýřová	arkýřový	k2eAgFnSc1d1	arkýřová
kaple	kaple	k1gFnSc1	kaple
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
zbytky	zbytek	k1gInPc4	zbytek
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
spatřit	spatřit	k5eAaPmF	spatřit
v	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
zdivu	zdivo	k1gNnSc6	zdivo
Vladislavského	vladislavský	k2eAgInSc2d1	vladislavský
sálu	sál	k1gInSc2	sál
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1344	[number]	k4	1344
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k9	ještě
kralevic	kralevic	k1gMnSc1	kralevic
a	a	k8xC	a
markrabě	markrabě	k1gMnSc1	markrabě
moravský	moravský	k2eAgMnSc1d1	moravský
položil	položit	k5eAaPmAgMnS	položit
s	s	k7c7	s
dlouholetým	dlouholetý	k2eAgMnSc7d1	dlouholetý
přítelem	přítel	k1gMnSc7	přítel
Arnoštem	Arnošt	k1gMnSc7	Arnošt
z	z	k7c2	z
Pardubic	Pardubice	k1gInPc2	Pardubice
a	a	k8xC	a
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
otcem	otec	k1gMnSc7	otec
<g/>
,	,	kIx,	,
králem	král	k1gMnSc7	král
Janem	Jan	k1gMnSc7	Jan
Lucemburským	lucemburský	k2eAgMnSc7d1	lucemburský
<g/>
,	,	kIx,	,
základní	základní	k2eAgInSc1d1	základní
kámen	kámen	k1gInSc1	kámen
katedrály	katedrála	k1gFnSc2	katedrála
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
povýšení	povýšení	k1gNnSc2	povýšení
pražského	pražský	k2eAgNnSc2d1	Pražské
biskupství	biskupství	k1gNnSc2	biskupství
na	na	k7c6	na
arcibiskupství	arcibiskupství	k1gNnSc6	arcibiskupství
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgMnPc7d1	hlavní
architekty	architekt	k1gMnPc7	architekt
byli	být	k5eAaImAgMnP	být
Matyáš	Matyáš	k1gMnSc1	Matyáš
z	z	k7c2	z
Arrasu	Arras	k1gInSc2	Arras
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
Petr	Petr	k1gMnSc1	Petr
Parléř	Parléř	k1gMnSc1	Parléř
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
trojlodní	trojlodní	k2eAgFnSc1d1	trojlodní
katedrála	katedrála	k1gFnSc1	katedrála
s	s	k7c7	s
příčnou	příčný	k2eAgFnSc7d1	příčná
lodí	loď	k1gFnSc7	loď
<g/>
,	,	kIx,	,
ochozem	ochoz	k1gInSc7	ochoz
a	a	k8xC	a
věncem	věnec	k1gInSc7	věnec
kaplí	kaple	k1gFnPc2	kaple
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgMnPc1d3	nejvýznamnější
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
Svatováclavská	svatováclavský	k2eAgFnSc1d1	Svatováclavská
kaple	kaple	k1gFnSc1	kaple
postavená	postavený	k2eAgFnSc1d1	postavená
nad	nad	k7c7	nad
hrobem	hrob	k1gInSc7	hrob
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
či	či	k8xC	či
Zvonová	zvonový	k2eAgFnSc1d1	zvonová
věž	věž	k1gFnSc1	věž
<g/>
,	,	kIx,	,
96,5	[number]	k4	96,5
metrů	metr	k1gInPc2	metr
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
Petrem	Petr	k1gMnSc7	Petr
Parléřem	Parléř	k1gMnSc7	Parléř
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
dokončena	dokončen	k2eAgFnSc1d1	dokončena
až	až	k9	až
roku	rok	k1gInSc2	rok
1554	[number]	k4	1554
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1770	[number]	k4	1770
opatřena	opatřit	k5eAaPmNgFnS	opatřit
barokní	barokní	k2eAgFnSc7d1	barokní
bání	báně	k1gFnSc7	báně
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
s	s	k7c7	s
osmdesátimetrovými	osmdesátimetrový	k2eAgFnPc7d1	osmdesátimetrová
věžemi	věž	k1gFnPc7	věž
byla	být	k5eAaImAgFnS	být
přistavěna	přistavěn	k2eAgFnSc1d1	přistavěna
až	až	k9	až
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
Josefa	Josef	k1gMnSc2	Josef
Mockera	Mocker	k1gMnSc2	Mocker
<g/>
.	.	kIx.	.
</s>
<s>
Katedrála	katedrála	k1gFnSc1	katedrála
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
za	za	k7c4	za
prezidenta	prezident	k1gMnSc4	prezident
T.G.	T.G.	k1gMnSc2	T.G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
a	a	k8xC	a
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
pražského	pražský	k2eAgMnSc2d1	pražský
Františka	František	k1gMnSc2	František
Kordače	Kordač	k1gMnSc2	Kordač
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
slavnostně	slavnostně	k6eAd1	slavnostně
otevřena	otevřen	k2eAgFnSc1d1	otevřena
ke	k	k7c3	k
Svatováclavskému	svatováclavský	k2eAgNnSc3d1	Svatováclavské
miléniu	milénium	k1gNnSc3	milénium
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
<s>
Vladislav	Vladislav	k1gMnSc1	Vladislav
Jagellonský	jagellonský	k2eAgMnSc1d1	jagellonský
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
po	po	k7c6	po
Jiřím	Jiří	k1gMnSc6	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
roku	rok	k1gInSc2	rok
1471	[number]	k4	1471
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
sídlil	sídlit	k5eAaImAgInS	sídlit
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
jeho	jeho	k3xOp3gMnPc1	jeho
předchůdci	předchůdce	k1gMnPc1	předchůdce
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
Václava	Václav	k1gMnSc2	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
na	na	k7c6	na
Starém	starý	k2eAgNnSc6d1	staré
městě	město	k1gNnSc6	město
pražském	pražský	k2eAgNnSc6d1	Pražské
v	v	k7c6	v
Králově	Králův	k2eAgInSc6d1	Králův
dvoře	dvůr	k1gInSc6	dvůr
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
dnešního	dnešní	k2eAgInSc2d1	dnešní
Obecního	obecní	k2eAgInSc2d1	obecní
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
však	však	k9	však
necítil	cítit	k5eNaImAgMnS	cítit
v	v	k7c6	v
bezpečí	bezpečí	k1gNnSc6	bezpečí
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1483	[number]	k4	1483
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
propukaly	propukat	k5eAaImAgInP	propukat
náboženské	náboženský	k2eAgInPc1d1	náboženský
nepokoje	nepokoj	k1gInPc1	nepokoj
vrcholící	vrcholící	k2eAgInPc1d1	vrcholící
defenestrací	defenestrace	k1gFnSc7	defenestrace
katolických	katolický	k2eAgMnPc2d1	katolický
konšelů	konšel	k1gMnPc2	konšel
na	na	k7c6	na
Novoměstské	novoměstský	k2eAgFnSc6d1	Novoměstská
radnici	radnice	k1gFnSc6	radnice
<g/>
,	,	kIx,	,
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
Pražský	pražský	k2eAgInSc4d1	pražský
hrad	hrad	k1gInSc4	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
ale	ale	k9	ale
nutné	nutný	k2eAgNnSc1d1	nutné
posílit	posílit	k5eAaPmF	posílit
zastaralý	zastaralý	k2eAgInSc1d1	zastaralý
románský	románský	k2eAgInSc1d1	románský
fortifikační	fortifikační	k2eAgInSc1d1	fortifikační
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
hrad	hrad	k1gInSc4	hrad
proto	proto	k8xC	proto
povolal	povolat	k5eAaPmAgMnS	povolat
architekta	architekt	k1gMnSc4	architekt
Benedikta	Benedikt	k1gMnSc4	Benedikt
Rieda	Ried	k1gMnSc4	Ried
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
záhy	záhy	k6eAd1	záhy
postavil	postavit	k5eAaPmAgMnS	postavit
úplně	úplně	k6eAd1	úplně
novou	nový	k2eAgFnSc4d1	nová
severní	severní	k2eAgFnSc4d1	severní
hradbu	hradba	k1gFnSc4	hradba
se	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
obrannými	obranný	k2eAgFnPc7d1	obranná
(	(	kIx(	(
<g/>
dělovými	dělový	k2eAgFnPc7d1	dělová
<g/>
)	)	kIx)	)
věžemi	věž	k1gFnPc7	věž
(	(	kIx(	(
<g/>
Prašná	prašný	k2eAgFnSc1d1	prašná
věž	věž	k1gFnSc1	věž
(	(	kIx(	(
<g/>
Mihulka	mihulka	k1gFnSc1	mihulka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nová	nový	k2eAgFnSc1d1	nová
Bílá	bílý	k2eAgFnSc1d1	bílá
věž	věž	k1gFnSc1	věž
<g/>
,	,	kIx,	,
Daliborka	Daliborka	k1gFnSc1	Daliborka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
nová	nový	k2eAgFnSc1d1	nová
hradba	hradba	k1gFnSc1	hradba
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
hrad	hrad	k1gInSc4	hrad
o	o	k7c4	o
několik	několik	k4yIc4	několik
metrů	metr	k1gInPc2	metr
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Jelenímu	jelení	k2eAgInSc3d1	jelení
příkopu	příkop	k1gInSc3	příkop
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
prostor	prostor	k1gInSc4	prostor
mezi	mezi	k7c7	mezi
starou	starý	k2eAgFnSc7d1	stará
románskou	románský	k2eAgFnSc7d1	románská
a	a	k8xC	a
novou	nový	k2eAgFnSc7d1	nová
pozdně	pozdně	k6eAd1	pozdně
gotickou	gotický	k2eAgFnSc7d1	gotická
hradební	hradební	k2eAgFnSc7d1	hradební
zdí	zeď	k1gFnSc7	zeď
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgNnSc6	který
později	pozdě	k6eAd2	pozdě
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
straně	strana	k1gFnSc6	strana
hradu	hrad	k1gInSc2	hrad
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
proslulá	proslulý	k2eAgFnSc1d1	proslulá
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
ulička	ulička	k1gFnSc1	ulička
<g/>
.	.	kIx.	.
</s>
<s>
Vladislav	Vladislav	k1gMnSc1	Vladislav
Jagellonský	jagellonský	k2eAgMnSc1d1	jagellonský
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
nákladně	nákladně	k6eAd1	nákladně
přestavět	přestavět	k5eAaPmF	přestavět
královský	královský	k2eAgInSc4d1	královský
palác	palác	k1gInSc4	palác
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
bylo	být	k5eAaImAgNnS	být
přestavěno	přestavěn	k2eAgNnSc4d1	přestavěno
západní	západní	k2eAgNnSc4d1	západní
křídlo	křídlo	k1gNnSc4	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
přestavby	přestavba	k1gFnSc2	přestavba
provedené	provedený	k2eAgFnSc2d1	provedená
frankfurtským	frankfurtský	k2eAgMnSc7d1	frankfurtský
architektem	architekt	k1gMnSc7	architekt
Hansem	Hans	k1gMnSc7	Hans
Spiessem	Spiess	k1gMnSc7	Spiess
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
v	v	k7c6	v
původní	původní	k2eAgFnSc6d1	původní
podobě	podoba	k1gFnSc6	podoba
zachovala	zachovat	k5eAaPmAgFnS	zachovat
Malá	malý	k2eAgFnSc1d1	malá
audienční	audienční	k2eAgFnSc1d1	audienční
síň	síň	k1gFnSc1	síň
(	(	kIx(	(
<g/>
také	také	k9	také
nesprávně	správně	k6eNd1	správně
označovaná	označovaný	k2eAgFnSc1d1	označovaná
jako	jako	k8xC	jako
Vladislavova	Vladislavův	k2eAgFnSc1d1	Vladislavova
ložnice	ložnice	k1gFnSc1	ložnice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
musela	muset	k5eAaImAgFnS	muset
vzniknout	vzniknout	k5eAaPmF	vzniknout
ještě	ještě	k9	ještě
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1490	[number]	k4	1490
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Vladislav	Vladislav	k1gMnSc1	Vladislav
stal	stát	k5eAaPmAgMnS	stát
králem	král	k1gMnSc7	král
uherským	uherský	k2eAgFnPc3d1	uherská
(	(	kIx(	(
<g/>
absence	absence	k1gFnSc1	absence
uherského	uherský	k2eAgInSc2d1	uherský
znaku	znak	k1gInSc2	znak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
byl	být	k5eAaImAgInS	být
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Benedikta	Benedikt	k1gMnSc2	Benedikt
Rieda	Ried	k1gMnSc2	Ried
na	na	k7c6	na
půdorysu	půdorys	k1gInSc6	půdorys
celého	celý	k2eAgNnSc2d1	celé
reprezentačního	reprezentační	k2eAgNnSc2d1	reprezentační
patra	patro	k1gNnSc2	patro
paláce	palác	k1gInSc2	palác
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
bývalého	bývalý	k2eAgInSc2d1	bývalý
Velkého	velký	k2eAgInSc2d1	velký
sálu	sál	k1gInSc2	sál
<g/>
,	,	kIx,	,
palácové	palácový	k2eAgFnSc2d1	palácová
kaple	kaple	k1gFnSc2	kaple
(	(	kIx(	(
<g/>
zachované	zachovaný	k2eAgInPc1d1	zachovaný
zbytky	zbytek	k1gInPc1	zbytek
arkýře	arkýř	k1gInSc2	arkýř
ve	v	k7c6	v
zdivu	zdivo	k1gNnSc6	zdivo
<g/>
)	)	kIx)	)
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
vrcholně	vrcholně	k6eAd1	vrcholně
gotických	gotický	k2eAgFnPc2d1	gotická
místností	místnost	k1gFnPc2	místnost
<g/>
)	)	kIx)	)
postaven	postaven	k2eAgInSc1d1	postaven
monumentální	monumentální	k2eAgInSc1d1	monumentální
Vladislavský	vladislavský	k2eAgInSc1d1	vladislavský
sál	sál	k1gInSc1	sál
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
největší	veliký	k2eAgInSc4d3	veliký
světský	světský	k2eAgInSc4d1	světský
klenutý	klenutý	k2eAgInSc4d1	klenutý
prostor	prostor	k1gInSc4	prostor
bez	bez	k7c2	bez
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
sloupů	sloup	k1gInPc2	sloup
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Sál	sál	k1gInSc1	sál
byl	být	k5eAaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1500	[number]	k4	1500
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
62	[number]	k4	62
m	m	kA	m
dlouhý	dlouhý	k2eAgMnSc1d1	dlouhý
<g/>
,	,	kIx,	,
16	[number]	k4	16
m	m	kA	m
široký	široký	k2eAgMnSc1d1	široký
a	a	k8xC	a
13	[number]	k4	13
m	m	kA	m
vysoký	vysoký	k2eAgInSc4d1	vysoký
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
důmyslně	důmyslně	k6eAd1	důmyslně
sklenut	sklenout	k5eAaPmNgMnS	sklenout
krouženou	kroužený	k2eAgFnSc7d1	kroužená
klenbou	klenba	k1gFnSc7	klenba
a	a	k8xC	a
vedou	vést	k5eAaImIp3nP	vést
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
slavné	slavný	k2eAgInPc1d1	slavný
Jezdecké	jezdecký	k2eAgInPc1d1	jezdecký
schody	schod	k1gInPc1	schod
<g/>
,	,	kIx,	,
určené	určený	k2eAgFnPc1d1	určená
pro	pro	k7c4	pro
vjezd	vjezd	k1gInSc4	vjezd
koní	kůň	k1gMnPc2	kůň
i	i	k8xC	i
s	s	k7c7	s
jezdci	jezdec	k1gMnPc7	jezdec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
důkazem	důkaz	k1gInSc7	důkaz
toho	ten	k3xDgMnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Vladislav	Vladislav	k1gMnSc1	Vladislav
i	i	k9	i
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
přestěhování	přestěhování	k1gNnSc6	přestěhování
do	do	k7c2	do
Budína	Budín	k1gInSc2	Budín
neztratil	ztratit	k5eNaPmAgMnS	ztratit
navzdory	navzdory	k7c3	navzdory
své	svůj	k3xOyFgFnSc3	svůj
tíživé	tíživý	k2eAgFnSc3d1	tíživá
finanční	finanční	k2eAgFnSc3d1	finanční
situaci	situace	k1gFnSc3	situace
o	o	k7c4	o
Pražský	pražský	k2eAgInSc4d1	pražský
hrad	hrad	k1gInSc4	hrad
zájem	zájem	k1gInSc1	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
tohoto	tento	k3xDgInSc2	tento
prostoru	prostor	k1gInSc2	prostor
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
uplatnila	uplatnit	k5eAaPmAgFnS	uplatnit
renesance	renesance	k1gFnSc1	renesance
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
v	v	k7c6	v
portálech	portál	k1gInPc6	portál
a	a	k8xC	a
oknech	okno	k1gNnPc6	okno
sálu	sál	k1gInSc2	sál
(	(	kIx(	(
<g/>
označena	označen	k2eAgFnSc1d1	označena
rokem	rok	k1gInSc7	rok
1493	[number]	k4	1493
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Palác	palác	k1gInSc1	palác
byl	být	k5eAaImAgInS	být
zastřešen	zastřešit	k5eAaPmNgInS	zastřešit
stanovou	stanový	k2eAgFnSc7d1	stanová
střechou	střecha	k1gFnSc7	střecha
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
však	však	k9	však
shořela	shořet	k5eAaPmAgFnS	shořet
při	při	k7c6	při
požáru	požár	k1gInSc6	požár
pražského	pražský	k2eAgNnSc2d1	Pražské
levobřeží	levobřeží	k1gNnSc2	levobřeží
roku	rok	k1gInSc2	rok
1541	[number]	k4	1541
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1508-1510	[number]	k4	1508-1510
bylo	být	k5eAaImAgNnS	být
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
nové	nový	k2eAgNnSc1d1	nové
jižní	jižní	k2eAgNnSc1d1	jižní
křídlo	křídlo	k1gNnSc1	křídlo
královského	královský	k2eAgInSc2d1	královský
paláce	palác	k1gInSc2	palác
určené	určený	k2eAgFnSc2d1	určená
k	k	k7c3	k
obytným	obytný	k2eAgInPc3d1	obytný
účelům	účel	k1gInPc3	účel
pojmenované	pojmenovaný	k2eAgFnPc1d1	pojmenovaná
podle	podle	k7c2	podle
následníka	následník	k1gMnSc2	následník
trůnu	trůn	k1gInSc2	trůn
Ludvíkovo	Ludvíkův	k2eAgNnSc4d1	Ludvíkovo
křídlo	křídlo	k1gNnSc4	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
paláce	palác	k1gInSc2	palác
již	již	k6eAd1	již
byla	být	k5eAaImAgNnP	být
postavena	postavit	k5eAaPmNgNnP	postavit
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
renesančním	renesanční	k2eAgInSc6d1	renesanční
slohu	sloh	k1gInSc6	sloh
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
klenby	klenba	k1gFnPc1	klenba
ještě	ještě	k6eAd1	ještě
měly	mít	k5eAaImAgFnP	mít
gotické	gotický	k2eAgNnSc4d1	gotické
žebrování	žebrování	k1gNnSc4	žebrování
<g/>
.	.	kIx.	.
</s>
<s>
Ludvíkovo	Ludvíkův	k2eAgNnSc1d1	Ludvíkovo
křídlo	křídlo	k1gNnSc1	křídlo
bylo	být	k5eAaImAgNnS	být
však	však	k9	však
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
účelu	účel	k1gInSc3	účel
využito	využít	k5eAaPmNgNnS	využít
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1522	[number]	k4	1522
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
král	král	k1gMnSc1	král
Ludvík	Ludvík	k1gMnSc1	Ludvík
s	s	k7c7	s
chotí	choť	k1gFnSc7	choť
Marií	Maria	k1gFnPc2	Maria
přijel	přijet	k5eAaPmAgMnS	přijet
na	na	k7c4	na
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
pobývat	pobývat	k5eAaImF	pobývat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
hutí	huť	k1gFnSc7	huť
Benedikta	Benedikt	k1gMnSc2	Benedikt
Rieda	Ried	k1gMnSc2	Ried
souvisí	souviset	k5eAaImIp3nS	souviset
i	i	k9	i
renesanční	renesanční	k2eAgInSc1d1	renesanční
jižní	jižní	k2eAgInSc1d1	jižní
portál	portál	k1gInSc1	portál
baziliky	bazilika	k1gFnSc2	bazilika
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc1	Jiří
a	a	k8xC	a
Královská	královský	k2eAgFnSc1d1	královská
oratoř	oratoř	k1gFnSc1	oratoř
v	v	k7c6	v
Katedrále	katedrála	k1gFnSc6	katedrála
sv.	sv.	kA	sv.
Víta	Víta	k1gFnSc1	Víta
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Hansem	Hans	k1gMnSc7	Hans
Spiessem	Spiess	k1gMnSc7	Spiess
(	(	kIx(	(
<g/>
motiv	motiv	k1gInSc4	motiv
osekaného	osekaný	k2eAgNnSc2d1	osekané
větvoví	větvoví	k1gNnSc2	větvoví
<g/>
)	)	kIx)	)
a	a	k8xC	a
spojená	spojený	k2eAgFnSc1d1	spojená
můstkem	můstek	k1gInSc7	můstek
se	s	k7c7	s
Starým	starý	k2eAgInSc7d1	starý
královským	královský	k2eAgInSc7d1	královský
palácem	palác	k1gInSc7	palác
<g/>
.	.	kIx.	.
</s>
<s>
Vladislav	Vladislav	k1gMnSc1	Vladislav
Jagellonský	jagellonský	k2eAgMnSc1d1	jagellonský
se	se	k3xPyFc4	se
také	také	k6eAd1	také
pokusil	pokusit	k5eAaPmAgMnS	pokusit
dostavět	dostavět	k5eAaPmF	dostavět
Katedrálu	katedrála	k1gFnSc4	katedrála
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
(	(	kIx(	(
<g/>
v	v	k7c6	v
pozdně	pozdně	k6eAd1	pozdně
gotickém	gotický	k2eAgInSc6d1	gotický
stylu	styl	k1gInSc6	styl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nechal	nechat	k5eAaPmAgMnS	nechat
opravit	opravit	k5eAaPmF	opravit
Svatováclavskou	svatováclavský	k2eAgFnSc4d1	Svatováclavská
kapli	kaple	k1gFnSc4	kaple
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
nově	nově	k6eAd1	nově
vymalována	vymalován	k2eAgFnSc1d1	vymalována
Mistrem	mistr	k1gMnSc7	mistr
Litoměřického	litoměřický	k2eAgInSc2d1	litoměřický
oltáře	oltář	k1gInSc2	oltář
a	a	k8xC	a
zahájil	zahájit	k5eAaPmAgMnS	zahájit
stavbu	stavba	k1gFnSc4	stavba
Severní	severní	k2eAgFnSc2d1	severní
věže	věž	k1gFnSc2	věž
katedrály	katedrála	k1gFnSc2	katedrála
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
vyvažovat	vyvažovat	k5eAaImF	vyvažovat
parléřovskou	parléřovský	k2eAgFnSc4d1	parléřovská
Jižní	jižní	k2eAgFnSc4d1	jižní
věž	věž	k1gFnSc4	věž
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
nedostatku	nedostatek	k1gInSc3	nedostatek
peněz	peníze	k1gInPc2	peníze
však	však	k9	však
byla	být	k5eAaImAgFnS	být
stavba	stavba	k1gFnSc1	stavba
přerušena	přerušit	k5eAaPmNgFnS	přerušit
a	a	k8xC	a
nedokončenou	dokončený	k2eNgFnSc4d1	nedokončená
práci	práce	k1gFnSc4	práce
nadobro	nadobro	k6eAd1	nadobro
zmařil	zmařit	k5eAaPmAgInS	zmařit
ničivý	ničivý	k2eAgInSc1d1	ničivý
požár	požár	k1gInSc1	požár
roku	rok	k1gInSc2	rok
1541	[number]	k4	1541
<g/>
.	.	kIx.	.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
Habsburský	habsburský	k2eAgInSc4d1	habsburský
se	se	k3xPyFc4	se
svojí	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
Annou	Anna	k1gFnSc7	Anna
pobývali	pobývat	k5eAaImAgMnP	pobývat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
poměrně	poměrně	k6eAd1	poměrně
často	často	k6eAd1	často
<g/>
.	.	kIx.	.
</s>
<s>
Snažili	snažit	k5eAaImAgMnP	snažit
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
Pražský	pražský	k2eAgInSc1d1	pražský
hrad	hrad	k1gInSc1	hrad
proměnit	proměnit	k5eAaPmF	proměnit
v	v	k7c4	v
reprezentativní	reprezentativní	k2eAgFnSc4d1	reprezentativní
moderní	moderní	k2eAgFnSc4d1	moderní
(	(	kIx(	(
<g/>
renesanční	renesanční	k2eAgFnSc4d1	renesanční
<g/>
)	)	kIx)	)
královskou	královský	k2eAgFnSc4d1	královská
rezidenci	rezidence	k1gFnSc4	rezidence
<g/>
.	.	kIx.	.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
založil	založit	k5eAaPmAgMnS	založit
roku	rok	k1gInSc2	rok
1534	[number]	k4	1534
na	na	k7c6	na
severním	severní	k2eAgNnSc6d1	severní
předpolí	předpolí	k1gNnSc6	předpolí
hradu	hrad	k1gInSc2	hrad
Královskou	královský	k2eAgFnSc4d1	královská
zahradu	zahrada	k1gFnSc4	zahrada
<g/>
,	,	kIx,	,
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
renesančních	renesanční	k2eAgFnPc2d1	renesanční
zahrad	zahrada	k1gFnPc2	zahrada
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
Alp	Alpy	k1gFnPc2	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
byly	být	k5eAaImAgFnP	být
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
<g/>
,	,	kIx,	,
pěstovány	pěstován	k2eAgFnPc4d1	pěstována
i	i	k8xC	i
exotické	exotický	k2eAgFnPc4d1	exotická
rostliny	rostlina	k1gFnPc4	rostlina
a	a	k8xC	a
ovoce	ovoce	k1gNnPc4	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
byly	být	k5eAaImAgFnP	být
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
pěstovány	pěstován	k2eAgInPc1d1	pěstován
tulipány	tulipán	k1gInPc1	tulipán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
renesanční	renesanční	k2eAgInSc1d1	renesanční
Královský	královský	k2eAgInSc1d1	královský
letohrádek	letohrádek	k1gInSc1	letohrádek
<g/>
,	,	kIx,	,
jejž	jenž	k3xRgInSc4	jenž
dal	dát	k5eAaPmAgInS	dát
prý	prý	k9	prý
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
vybudovat	vybudovat	k5eAaPmF	vybudovat
z	z	k7c2	z
lásky	láska	k1gFnSc2	láska
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
ženu	žena	k1gFnSc4	žena
Annu	Anna	k1gFnSc4	Anna
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c4	před
letohrádek	letohrádek	k1gInSc4	letohrádek
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
tzv.	tzv.	kA	tzv.
Zpívající	zpívající	k2eAgFnSc1d1	zpívající
fontána	fontána	k1gFnSc1	fontána
odlitá	odlitý	k2eAgFnSc1d1	odlitá
Tomášem	Tomáš	k1gMnSc7	Tomáš
Jarošem	Jaroš	k1gMnSc7	Jaroš
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
Francesca	Francescus	k1gMnSc2	Francescus
Terzia	Terzius	k1gMnSc2	Terzius
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
Velká	velký	k2eAgFnSc1d1	velká
míčovna	míčovna	k1gFnSc1	míčovna
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
Bonifáce	Bonifác	k1gMnSc2	Bonifác
Wohlmuta	Wohlmut	k1gMnSc2	Wohlmut
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1541	[number]	k4	1541
celé	celá	k1gFnSc6	celá
pražské	pražský	k2eAgFnSc6d1	Pražská
levobřeží	levobřež	k1gFnSc7	levobřež
zachvátil	zachvátit	k5eAaPmAgMnS	zachvátit
ničivý	ničivý	k2eAgInSc4d1	ničivý
požár	požár	k1gInSc4	požár
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
něm	on	k3xPp3gNnSc6	on
byla	být	k5eAaImAgFnS	být
katastrofálně	katastrofálně	k6eAd1	katastrofálně
poničena	poničen	k2eAgFnSc1d1	poničena
kaple	kaple	k1gFnSc1	kaple
Všech	všecek	k3xTgFnPc2	všecek
svatých	svatá	k1gFnPc2	svatá
<g/>
,	,	kIx,	,
poškozen	poškodit	k5eAaPmNgInS	poškodit
byl	být	k5eAaImAgInS	být
Starý	starý	k2eAgInSc1d1	starý
královský	královský	k2eAgInSc1d1	královský
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
pozdně	pozdně	k6eAd1	pozdně
gotická	gotický	k2eAgFnSc1d1	gotická
stanová	stanový	k2eAgFnSc1d1	stanová
střecha	střecha	k1gFnSc1	střecha
lehla	lehnout	k5eAaPmAgFnS	lehnout
popelem	popel	k1gInSc7	popel
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
Křídlo	křídlo	k1gNnSc4	křídlo
zemských	zemský	k2eAgFnPc2d1	zemská
desk	deska	k1gFnPc2	deska
se	s	k7c7	s
sněmovnou	sněmovna	k1gFnSc7	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
požáru	požár	k1gInSc6	požár
shořely	shořet	k5eAaPmAgFnP	shořet
i	i	k9	i
samotné	samotný	k2eAgFnPc4d1	samotná
zemské	zemský	k2eAgFnPc4d1	zemská
desky	deska	k1gFnPc4	deska
<g/>
.	.	kIx.	.
</s>
<s>
Poškozena	poškozen	k2eAgFnSc1d1	poškozena
byla	být	k5eAaImAgFnS	být
i	i	k9	i
katedrála	katedrála	k1gFnSc1	katedrála
<g/>
,	,	kIx,	,
zvl.	zvl.	kA	zvl.
její	její	k3xOp3gFnSc7	její
rozestavěné	rozestavěný	k2eAgFnPc4d1	rozestavěná
části	část	k1gFnPc4	část
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nebyly	být	k5eNaImAgFnP	být
obnoveny	obnovit	k5eAaPmNgInP	obnovit
(	(	kIx(	(
<g/>
Severní	severní	k2eAgFnSc1d1	severní
věž	věž	k1gFnSc1	věž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Shořel	shořet	k5eAaPmAgInS	shořet
i	i	k9	i
královnin	královnin	k2eAgInSc1d1	královnin
palác	palác	k1gInSc1	palác
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
stavební	stavební	k2eAgFnSc1d1	stavební
činnost	činnost	k1gFnSc1	činnost
soustředila	soustředit	k5eAaPmAgFnS	soustředit
na	na	k7c4	na
obnovu	obnova	k1gFnSc4	obnova
hradu	hrad	k1gInSc2	hrad
po	po	k7c6	po
strašlivém	strašlivý	k2eAgInSc6d1	strašlivý
požáru	požár	k1gInSc6	požár
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
věž	věž	k1gFnSc1	věž
katedrály	katedrála	k1gFnSc2	katedrála
sv.	sv.	kA	sv.
Víta	Víta	k1gFnSc1	Víta
dostala	dostat	k5eAaPmAgFnS	dostat
nový	nový	k2eAgInSc4d1	nový
ochoz	ochoz	k1gInSc4	ochoz
a	a	k8xC	a
střechu	střecha	k1gFnSc4	střecha
(	(	kIx(	(
<g/>
renesanční	renesanční	k2eAgFnSc4d1	renesanční
báň	báň	k1gFnSc4	báň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
věž	věž	k1gFnSc4	věž
byly	být	k5eAaImAgInP	být
Tomášem	Tomáš	k1gMnSc7	Tomáš
Jarošem	Jaroš	k1gMnSc7	Jaroš
odlity	odlit	k2eAgInPc4d1	odlit
nové	nový	k2eAgInPc4d1	nový
zvony	zvon	k1gInPc4	zvon
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
největšího	veliký	k2eAgInSc2d3	veliký
zvonu	zvon	k1gInSc2	zvon
Zikmund	Zikmund	k1gMnSc1	Zikmund
<g/>
)	)	kIx)	)
a	a	k8xC	a
katedrála	katedrála	k1gFnSc1	katedrála
byla	být	k5eAaImAgFnS	být
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
novou	nový	k2eAgFnSc7d1	nová
renesanční	renesanční	k2eAgFnSc7d1	renesanční
kruchtou	kruchta	k1gFnSc7	kruchta
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
Bonifáce	Bonifác	k1gMnSc2	Bonifác
Wohlmuta	Wohlmut	k1gMnSc2	Wohlmut
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
severním	severní	k2eAgInSc6d1	severní
transeptu	transept	k1gInSc6	transept
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
požáru	požár	k1gInSc6	požár
bylo	být	k5eAaImAgNnS	být
renesančně	renesančně	k6eAd1	renesančně
přestavěno	přestavěn	k2eAgNnSc1d1	přestavěno
a	a	k8xC	a
rozšířeno	rozšířen	k2eAgNnSc1d1	rozšířeno
Křídlo	křídlo	k1gNnSc1	křídlo
zemských	zemský	k2eAgFnPc2d1	zemská
desk	deska	k1gFnPc2	deska
Starého	Starého	k2eAgInSc2d1	Starého
královského	královský	k2eAgInSc2d1	královský
paláce	palác	k1gInSc2	palác
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
znovuvystavěna	znovuvystavěn	k2eAgFnSc1d1	znovuvystavěn
Sněmovna	sněmovna	k1gFnSc1	sněmovna
Českého	český	k2eAgInSc2d1	český
sněmu	sněm	k1gInSc2	sněm
podle	podle	k7c2	podle
gotizujícího	gotizující	k2eAgInSc2d1	gotizující
návrhu	návrh	k1gInSc2	návrh
Bonifáce	Bonifác	k1gMnSc4	Bonifác
Wohlmuta	Wohlmut	k1gMnSc4	Wohlmut
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
své	svůj	k3xOyFgFnSc2	svůj
manželky	manželka	k1gFnSc2	manželka
královny	královna	k1gFnSc2	královna
Anny	Anna	k1gFnSc2	Anna
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1547	[number]	k4	1547
<g/>
,	,	kIx,	,
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
král	král	k1gMnSc1	král
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
svého	svůj	k3xOyFgMnSc2	svůj
mladšího	mladý	k2eAgMnSc2d2	mladší
syna	syn	k1gMnSc2	syn
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Tyrolského	tyrolský	k2eAgMnSc2d1	tyrolský
místodržícím	místodržící	k1gMnPc3	místodržící
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
starat	starat	k5eAaImF	starat
nejen	nejen	k6eAd1	nejen
o	o	k7c6	o
státnické	státnický	k2eAgFnSc6d1	státnická
záležitosti	záležitost	k1gFnSc6	záležitost
Českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
a	a	k8xC	a
reprezentaci	reprezentace	k1gFnSc4	reprezentace
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
měl	mít	k5eAaImAgInS	mít
i	i	k9	i
dohlížet	dohlížet	k5eAaImF	dohlížet
na	na	k7c4	na
stavební	stavební	k2eAgFnSc4d1	stavební
činnost	činnost	k1gFnSc4	činnost
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
Ferdinandovi	Ferdinand	k1gMnSc3	Ferdinand
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
mnoho	mnoho	k4c1	mnoho
peněz	peníze	k1gInPc2	peníze
na	na	k7c4	na
chod	chod	k1gInSc4	chod
jeho	on	k3xPp3gInSc2	on
pražského	pražský	k2eAgInSc2d1	pražský
dvora	dvůr	k1gInSc2	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Hanse	Hans	k1gMnSc2	Hans
Tirola	Tirola	k1gFnSc1	Tirola
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1554	[number]	k4	1554
dokončen	dokončen	k2eAgInSc1d1	dokončen
nový	nový	k2eAgInSc1d1	nový
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Nové	Nové	k2eAgNnSc1d1	Nové
či	či	k8xC	či
Ferdinandovo	Ferdinandův	k2eAgNnSc1d1	Ferdinandovo
stavení	stavení	k1gNnSc1	stavení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
sloužilo	sloužit	k5eAaImAgNnS	sloužit
jako	jako	k9	jako
Ferdinandovy	Ferdinandův	k2eAgInPc4d1	Ferdinandův
osobní	osobní	k2eAgInPc4d1	osobní
obytné	obytný	k2eAgInPc4d1	obytný
prostory	prostor	k1gInPc4	prostor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
si	se	k3xPyFc3	se
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
Hradu	hrad	k1gInSc2	hrad
začala	začít	k5eAaPmAgFnS	začít
své	svůj	k3xOyFgInPc4	svůj
paláce	palác	k1gInPc4	palác
budovat	budovat	k5eAaImF	budovat
mocná	mocný	k2eAgFnSc1d1	mocná
šlechta	šlechta	k1gFnSc1	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
požáru	požár	k1gInSc6	požár
roku	rok	k1gInSc2	rok
1541	[number]	k4	1541
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
přepychový	přepychový	k2eAgInSc1d1	přepychový
renesanční	renesanční	k2eAgInSc1d1	renesanční
palác	palác	k1gInSc1	palác
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
přepychovější	přepychový	k2eAgInSc1d2	přepychový
palác	palác	k1gInSc1	palác
Rožmberků	Rožmberk	k1gInPc2	Rožmberk
<g/>
,	,	kIx,	,
paláce	palác	k1gInSc2	palác
dvou	dva	k4xCgInPc2	dva
nejmocnějších	mocný	k2eAgInPc2d3	nejmocnější
rodů	rod	k1gInPc2	rod
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Východnější	východní	k2eAgInSc1d2	východnější
Pernštejnský	pernštejnský	k2eAgInSc1d1	pernštejnský
palác	palác	k1gInSc1	palác
po	po	k7c6	po
vymření	vymření	k1gNnSc6	vymření
Pernštejnů	Pernštejn	k1gInPc2	Pernštejn
zdědili	zdědit	k5eAaPmAgMnP	zdědit
Lobkovicové	Lobkovicový	k2eAgMnPc4d1	Lobkovicový
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jej	on	k3xPp3gMnSc4	on
pak	pak	k6eAd1	pak
nechali	nechat	k5eAaPmAgMnP	nechat
přestavět	přestavět	k5eAaPmF	přestavět
v	v	k7c6	v
období	období	k1gNnSc6	období
baroka	baroko	k1gNnSc2	baroko
a	a	k8xC	a
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gNnSc6	jejichž
držení	držení	k1gNnSc6	držení
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Západnější	západní	k2eAgInSc1d2	západnější
Rožmberský	rožmberský	k2eAgInSc1d1	rožmberský
palác	palác	k1gInSc1	palác
získal	získat	k5eAaPmAgInS	získat
od	od	k7c2	od
Petra	Petr	k1gMnSc2	Petr
Voka	Vokus	k1gMnSc2	Vokus
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
palác	palác	k1gInSc4	palác
na	na	k7c6	na
Hradčanech	Hradčany	k1gInPc6	Hradčany
Rudolf	Rudolf	k1gMnSc1	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
majetku	majetek	k1gInSc6	majetek
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
setrval	setrvat	k5eAaPmAgInS	setrvat
až	až	k9	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jej	on	k3xPp3gNnSc4	on
nechala	nechat	k5eAaPmAgFnS	nechat
pozdně	pozdně	k6eAd1	pozdně
barokně	barokně	k6eAd1	barokně
přestavět	přestavět	k5eAaPmF	přestavět
svým	svůj	k3xOyFgMnSc7	svůj
architektem	architekt	k1gMnSc7	architekt
Niccolo	Niccola	k1gFnSc5	Niccola
Paccassim	Paccassim	k1gInSc1	Paccassim
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
nově	nově	k6eAd1	nově
založeného	založený	k2eAgInSc2d1	založený
Ústavu	ústav	k1gInSc2	ústav
šlechtičen	šlechtična	k1gFnPc2	šlechtična
<g/>
.	.	kIx.	.
</s>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
na	na	k7c4	na
český	český	k2eAgInSc4d1	český
trůn	trůn	k1gInSc4	trůn
roku	rok	k1gInSc2	rok
1576	[number]	k4	1576
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1583	[number]	k4	1583
si	se	k3xPyFc3	se
zvolil	zvolit	k5eAaPmAgInS	zvolit
Pražský	pražský	k2eAgInSc4d1	pražský
hrad	hrad	k1gInSc4	hrad
jako	jako	k8xS	jako
svou	svůj	k3xOyFgFnSc4	svůj
hlavní	hlavní	k2eAgFnSc4d1	hlavní
trvalou	trvalý	k2eAgFnSc4d1	trvalá
rezidenci	rezidence	k1gFnSc4	rezidence
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
tedy	tedy	k9	tedy
znamenalo	znamenat	k5eAaImAgNnS	znamenat
trvalé	trvalý	k2eAgNnSc4d1	trvalé
přesunutí	přesunutí	k1gNnSc4	přesunutí
císařského	císařský	k2eAgInSc2d1	císařský
dvora	dvůr	k1gInSc2	dvůr
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
mělo	mít	k5eAaImAgNnS	mít
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
další	další	k2eAgFnPc4d1	další
stavební	stavební	k2eAgFnPc4d1	stavební
úpravy	úprava	k1gFnPc4	úprava
<g/>
.	.	kIx.	.
</s>
<s>
Nastalo	nastat	k5eAaPmAgNnS	nastat
totiž	totiž	k9	totiž
období	období	k1gNnSc4	období
největších	veliký	k2eAgFnPc2d3	veliký
stavebních	stavební	k2eAgFnPc2d1	stavební
aktivit	aktivita	k1gFnPc2	aktivita
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterého	který	k3yRgNnSc2	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
dnešní	dnešní	k2eAgFnSc1d1	dnešní
II	II	kA	II
<g/>
.	.	kIx.	.
nádvoří	nádvoří	k1gNnSc4	nádvoří
s	s	k7c7	s
přiléhajícími	přiléhající	k2eAgFnPc7d1	přiléhající
budovami	budova	k1gFnPc7	budova
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
ubytování	ubytování	k1gNnSc2	ubytování
císaře	císař	k1gMnSc2	císař
byl	být	k5eAaImAgInS	být
vyhrazen	vyhradit	k5eAaPmNgInS	vyhradit
prostor	prostor	k1gInSc1	prostor
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
dnešního	dnešní	k2eAgInSc2d1	dnešní
II	II	kA	II
<g/>
.	.	kIx.	.
nádvoří	nádvoří	k1gNnSc6	nádvoří
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
tzv.	tzv.	kA	tzv.
letní	letní	k2eAgInSc4d1	letní
palác	palác	k1gInSc4	palác
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
spojený	spojený	k2eAgInSc1d1	spojený
se	s	k7c7	s
starším	starý	k2eAgInSc7d2	starší
palácem	palác	k1gInSc7	palác
Ferdinandovým	Ferdinandův	k2eAgInSc7d1	Ferdinandův
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
vedl	vést	k5eAaImAgMnS	vést
schodištěm	schodiště	k1gNnSc7	schodiště
napravo	napravo	k6eAd1	napravo
za	za	k7c7	za
Matyášovou	Matyášův	k2eAgFnSc7d1	Matyášova
bránou	brána	k1gFnSc7	brána
(	(	kIx(	(
<g/>
během	během	k7c2	během
tereziánských	tereziánský	k2eAgFnPc2d1	Tereziánská
úprav	úprava	k1gFnPc2	úprava
o	o	k7c4	o
patro	patro	k1gNnSc4	patro
snížené	snížený	k2eAgFnSc2d1	snížená
a	a	k8xC	a
barokně	barokně	k6eAd1	barokně
přestavěné	přestavěný	k2eAgNnSc1d1	přestavěné
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
jižní	jižní	k2eAgInPc1d1	jižní
objekty	objekt	k1gInPc1	objekt
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
základem	základ	k1gInSc7	základ
dnešních	dnešní	k2eAgFnPc2d1	dnešní
reprezentačních	reprezentační	k2eAgFnPc2d1	reprezentační
prostor	prostora	k1gFnPc2	prostora
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
II	II	kA	II
<g/>
.	.	kIx.	.
nádvoří	nádvoří	k1gNnSc1	nádvoří
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
severní	severní	k2eAgFnPc1d1	severní
brány	brána	k1gFnPc1	brána
(	(	kIx(	(
<g/>
vedoucí	vedoucí	k1gFnSc1	vedoucí
k	k	k7c3	k
Prašnému	prašný	k2eAgInSc3d1	prašný
mostu	most	k1gInSc3	most
a	a	k8xC	a
Královské	královský	k2eAgFnSc3d1	královská
zahradě	zahrada	k1gFnSc3	zahrada
<g/>
)	)	kIx)	)
tzv.	tzv.	kA	tzv.
Severní	severní	k2eAgNnSc1d1	severní
stavení	stavení	k1gNnSc1	stavení
obsahující	obsahující	k2eAgFnSc2d1	obsahující
obrovské	obrovský	k2eAgFnSc2d1	obrovská
reprezentativní	reprezentativní	k2eAgFnSc2d1	reprezentativní
stáje	stáj	k1gFnSc2	stáj
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
konírna	konírna	k1gFnSc1	konírna
byla	být	k5eAaImAgFnS	být
dokonce	dokonce	k9	dokonce
dvoupatrová	dvoupatrový	k2eAgFnSc1d1	dvoupatrová
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
dokázaly	dokázat	k5eAaPmAgFnP	dokázat
pojmout	pojmout	k5eAaPmF	pojmout
stovky	stovka	k1gFnPc4	stovka
koní	kůň	k1gMnPc2	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
stájemi	stáj	k1gFnPc7	stáj
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
dva	dva	k4xCgInPc1	dva
sály	sál	k1gInPc1	sál
vyhrazené	vyhrazený	k2eAgInPc1d1	vyhrazený
rudolfinským	rudolfinský	k2eAgFnPc3d1	Rudolfinská
uměleckým	umělecký	k2eAgFnPc3d1	umělecká
sbírkám	sbírka	k1gFnPc3	sbírka
-	-	kIx~	-
větším	veliký	k2eAgInPc3d2	veliký
obrazům	obraz	k1gInPc3	obraz
a	a	k8xC	a
sochám	socha	k1gFnPc3	socha
-	-	kIx~	-
dnešní	dnešní	k2eAgInSc1d1	dnešní
Španělský	španělský	k2eAgInSc1d1	španělský
sál	sál	k1gInSc1	sál
a	a	k8xC	a
Rudolfova	Rudolfův	k2eAgFnSc1d1	Rudolfova
galerie	galerie	k1gFnSc1	galerie
-	-	kIx~	-
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Obrazárna	obrazárna	k1gFnSc1	obrazárna
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
tzv.	tzv.	kA	tzv.
kunstkomory	kunstkomora	k1gFnSc2	kunstkomora
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
sbírek	sbírka	k1gFnPc2	sbírka
Rudolfa	Rudolf	k1gMnSc2	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
při	při	k7c6	při
východní	východní	k2eAgFnSc6d1	východní
(	(	kIx(	(
<g/>
vnější	vnější	k2eAgFnSc6d1	vnější
<g/>
)	)	kIx)	)
stěně	stěna	k1gFnSc3	stěna
staré	starý	k2eAgFnSc2d1	stará
románské	románský	k2eAgFnSc2d1	románská
hradby	hradba	k1gFnSc2	hradba
dvoupatrový	dvoupatrový	k2eAgInSc1d1	dvoupatrový
tzv.	tzv.	kA	tzv.
Střední	střední	k2eAgInSc1d1	střední
trakt	trakt	k1gInSc1	trakt
spojující	spojující	k2eAgInSc1d1	spojující
císařský	císařský	k2eAgInSc4d1	císařský
palác	palác	k1gInSc4	palác
se	s	k7c7	s
Severním	severní	k2eAgNnSc7d1	severní
stavením	stavení	k1gNnSc7	stavení
<g/>
.	.	kIx.	.
</s>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
tak	tak	k6eAd1	tak
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
jádro	jádro	k1gNnSc4	jádro
dnešního	dnešní	k2eAgInSc2d1	dnešní
Nového	Nového	k2eAgInSc2d1	Nového
královského	královský	k2eAgInSc2d1	královský
paláce	palác	k1gInSc2	palác
a	a	k8xC	a
z	z	k7c2	z
hradu	hrad	k1gInSc2	hrad
učinil	učinit	k5eAaImAgMnS	učinit
reprezentativní	reprezentativní	k2eAgNnSc4d1	reprezentativní
císařské	císařský	k2eAgNnSc4d1	císařské
sídlo	sídlo	k1gNnSc4	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Rudolfovy	Rudolfův	k2eAgFnSc2d1	Rudolfova
vlády	vláda	k1gFnSc2	vláda
byla	být	k5eAaImAgFnS	být
upravována	upravovat	k5eAaImNgFnS	upravovat
i	i	k9	i
katedrála	katedrála	k1gFnSc1	katedrála
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
mramorové	mramorový	k2eAgNnSc1d1	mramorové
mauzoleum	mauzoleum	k1gNnSc1	mauzoleum
uprostřed	uprostřed	k7c2	uprostřed
katedrály	katedrála	k1gFnSc2	katedrála
nizozemského	nizozemský	k2eAgMnSc2d1	nizozemský
sochaře	sochař	k1gMnSc2	sochař
Alexandra	Alexandr	k1gMnSc2	Alexandr
Colina	Colin	k1gMnSc2	Colin
(	(	kIx(	(
<g/>
objednané	objednaný	k2eAgFnPc1d1	objednaná
už	už	k6eAd1	už
císařem	císař	k1gMnSc7	císař
Maxmiliánem	Maxmilián	k1gMnSc7	Maxmilián
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
spočívá	spočívat	k5eAaImIp3nS	spočívat
Rudolfův	Rudolfův	k2eAgMnSc1d1	Rudolfův
otec	otec	k1gMnSc1	otec
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
rodiči	rodič	k1gMnPc7	rodič
Ferdinandem	Ferdinand	k1gMnSc7	Ferdinand
I.	I.	kA	I.
a	a	k8xC	a
Annou	Anna	k1gFnSc7	Anna
Jagellonskou	jagellonský	k2eAgFnSc7d1	Jagellonská
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
Královským	královský	k2eAgNnSc7d1	královské
mauzoleem	mauzoleum	k1gNnSc7	mauzoleum
byla	být	k5eAaImAgFnS	být
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
Nová	nový	k2eAgFnSc1d1	nová
královská	královský	k2eAgFnSc1d1	královská
hrobka	hrobka	k1gFnSc1	hrobka
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yIgFnSc2	který
byly	být	k5eAaImAgFnP	být
přesunuty	přesunout	k5eAaPmNgInP	přesunout
ostatky	ostatek	k1gInPc1	ostatek
českých	český	k2eAgMnPc2d1	český
králů	král	k1gMnPc2	král
ze	z	k7c2	z
Staré	Staré	k2eAgFnSc2d1	Staré
královské	královský	k2eAgFnSc2d1	královská
hrobky	hrobka	k1gFnSc2	hrobka
vybudované	vybudovaný	k2eAgFnSc2d1	vybudovaná
Karlem	Karel	k1gMnSc7	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
pod	pod	k7c7	pod
kněžištěm	kněžiště	k1gNnSc7	kněžiště
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
hrobce	hrobka	k1gFnSc6	hrobka
byl	být	k5eAaImAgMnS	být
pak	pak	k6eAd1	pak
Rudolf	Rudolf	k1gMnSc1	Rudolf
sám	sám	k3xTgMnSc1	sám
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
jeho	jeho	k3xOp3gInPc1	jeho
ostatky	ostatek	k1gInPc1	ostatek
nachází	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
původní	původní	k2eAgFnSc6d1	původní
cínové	cínový	k2eAgFnSc6d1	cínová
rakvi	rakev	k1gFnSc6	rakev
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
věž	věž	k1gFnSc1	věž
katedrály	katedrála	k1gFnSc2	katedrála
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
opatřena	opatřen	k2eAgFnSc1d1	opatřena
hodinami	hodina	k1gFnPc7	hodina
<g/>
,	,	kIx,	,
renesanční	renesanční	k2eAgFnSc7d1	renesanční
mříží	mříž	k1gFnSc7	mříž
a	a	k8xC	a
zlacenou	zlacený	k2eAgFnSc7d1	zlacená
Rudolfovou	Rudolfův	k2eAgFnSc7d1	Rudolfova
iniciálou	iniciála	k1gFnSc7	iniciála
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1576	[number]	k4	1576
byla	být	k5eAaImAgFnS	být
před	před	k7c7	před
katedrálou	katedrála	k1gFnSc7	katedrála
Ulricem	Ulric	k1gMnSc7	Ulric
Aostallim	Aostallim	k1gInSc1	Aostallim
postavena	postaven	k2eAgFnSc1d1	postavena
kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
stržena	strhnout	k5eAaPmNgFnS	strhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1879	[number]	k4	1879
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
dostavbou	dostavba	k1gFnSc7	dostavba
východní	východní	k2eAgFnSc2d1	východní
části	část	k1gFnSc2	část
katedrály	katedrála	k1gFnSc2	katedrála
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Královské	královský	k2eAgFnSc6d1	královská
zahradě	zahrada	k1gFnSc6	zahrada
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
Lví	lví	k2eAgInSc1d1	lví
dvůr	dvůr	k1gInSc1	dvůr
pro	pro	k7c4	pro
exotickou	exotický	k2eAgFnSc4d1	exotická
zvěř	zvěř	k1gFnSc4	zvěř
a	a	k8xC	a
do	do	k7c2	do
Jeleního	jelení	k2eAgInSc2d1	jelení
příkopu	příkop	k1gInSc2	příkop
vysazeni	vysazen	k2eAgMnPc1d1	vysazen
jeleni	jelen	k1gMnPc1	jelen
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mu	on	k3xPp3gMnSc3	on
dali	dát	k5eAaPmAgMnP	dát
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Rudolfovy	Rudolfův	k2eAgFnSc2d1	Rudolfova
vlády	vláda	k1gFnSc2	vláda
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
také	také	k9	také
slavná	slavný	k2eAgFnSc1d1	slavná
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
ulička	ulička	k1gFnSc1	ulička
<g/>
,	,	kIx,	,
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
době	doba	k1gFnSc6	doba
však	však	k9	však
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
obvyklou	obvyklý	k2eAgFnSc4d1	obvyklá
parazitní	parazitní	k2eAgFnSc4d1	parazitní
chudinskou	chudinský	k2eAgFnSc4d1	chudinská
zástavbu	zástavba	k1gFnSc4	zástavba
(	(	kIx(	(
<g/>
které	který	k3yQgNnSc1	který
byl	být	k5eAaImAgInS	být
hrad	hrad	k1gInSc1	hrad
plný	plný	k2eAgInSc1d1	plný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
dnes	dnes	k6eAd1	dnes
posledním	poslední	k2eAgMnSc7d1	poslední
svědkem	svědek	k1gMnSc7	svědek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Rudolfa	Rudolf	k1gMnSc2	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
1612	[number]	k4	1612
Pražský	pražský	k2eAgInSc1d1	pražský
hrad	hrad	k1gInSc1	hrad
přestal	přestat	k5eAaPmAgInS	přestat
být	být	k5eAaImF	být
hlavní	hlavní	k2eAgMnSc1d1	hlavní
a	a	k8xC	a
trvalou	trvalý	k2eAgFnSc7d1	trvalá
rezidencí	rezidence	k1gFnSc7	rezidence
panovníka	panovník	k1gMnSc2	panovník
a	a	k8xC	a
už	už	k6eAd1	už
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
také	také	k9	také
nikdy	nikdy	k6eAd1	nikdy
nestal	stát	k5eNaPmAgInS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
jeho	jeho	k3xOp3gInSc1	jeho
význam	význam	k1gInSc1	význam
nějak	nějak	k6eAd1	nějak
významně	významně	k6eAd1	významně
také	také	k9	také
neklesl	klesnout	k5eNaPmAgMnS	klesnout
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Vídni	Vídeň	k1gFnSc6	Vídeň
byla	být	k5eAaImAgFnS	být
Praha	Praha	k1gFnSc1	Praha
stále	stále	k6eAd1	stále
nejvýznamnějším	významný	k2eAgNnSc7d3	nejvýznamnější
městem	město	k1gNnSc7	město
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
monarchie	monarchie	k1gFnSc2	monarchie
a	a	k8xC	a
o	o	k7c4	o
Pražský	pražský	k2eAgInSc4d1	pražský
hrad	hrad	k1gInSc4	hrad
měli	mít	k5eAaImAgMnP	mít
panovníci	panovník	k1gMnPc1	panovník
nadále	nadále	k6eAd1	nadále
starost	starost	k1gFnSc4	starost
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
Rudolfův	Rudolfův	k2eAgMnSc1d1	Rudolfův
bratr	bratr	k1gMnSc1	bratr
Matyáš	Matyáš	k1gMnSc1	Matyáš
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
zvýšit	zvýšit	k5eAaPmF	zvýšit
a	a	k8xC	a
přestavět	přestavět	k5eAaPmF	přestavět
císařský	císařský	k2eAgInSc4d1	císařský
palác	palác	k1gInSc4	palác
(	(	kIx(	(
<g/>
jižní	jižní	k2eAgNnSc4d1	jižní
křídlo	křídlo	k1gNnSc4	křídlo
Nového	Nového	k2eAgInSc2d1	Nového
královského	královský	k2eAgInSc2d1	královský
paláce	palác	k1gInSc2	palác
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
opatřit	opatřit	k5eAaPmF	opatřit
západní	západní	k2eAgFnSc4d1	západní
bránu	brána	k1gFnSc4	brána
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Matyášova	Matyášův	k2eAgFnSc1d1	Matyášova
brána	brána	k1gFnSc1	brána
<g/>
)	)	kIx)	)
raně	raně	k6eAd1	raně
barokním	barokní	k2eAgInSc7d1	barokní
štítem	štít	k1gInSc7	štít
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
jménem	jméno	k1gNnSc7	jméno
a	a	k8xC	a
v	v	k7c6	v
Rajské	rajský	k2eAgFnSc6d1	rajská
zahradě	zahrada	k1gFnSc6	zahrada
pod	pod	k7c7	pod
okny	okno	k1gNnPc7	okno
královských	královský	k2eAgInPc2d1	královský
(	(	kIx(	(
<g/>
císařských	císařský	k2eAgInPc2d1	císařský
<g/>
)	)	kIx)	)
pokojů	pokoj	k1gInPc2	pokoj
nechal	nechat	k5eAaPmAgMnS	nechat
postavit	postavit	k5eAaPmF	postavit
pozdně	pozdně	k6eAd1	pozdně
renesanční	renesanční	k2eAgInSc4d1	renesanční
altánek	altánek	k1gInSc4	altánek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
propuknutí	propuknutí	k1gNnSc6	propuknutí
stavovského	stavovský	k2eAgNnSc2d1	Stavovské
povstání	povstání	k1gNnSc2	povstání
byl	být	k5eAaImAgMnS	být
zvolen	zvolen	k2eAgMnSc1d1	zvolen
nový	nový	k2eAgMnSc1d1	nový
král	král	k1gMnSc1	král
Fridrich	Fridrich	k1gMnSc1	Fridrich
Falcký	falcký	k2eAgMnSc1d1	falcký
<g/>
,	,	kIx,	,
kterému	který	k3yIgNnSc3	který
právě	právě	k9	právě
rekonstruovaný	rekonstruovaný	k2eAgInSc1d1	rekonstruovaný
Pražský	pražský	k2eAgInSc1d1	pražský
hrad	hrad	k1gInSc1	hrad
poskytoval	poskytovat	k5eAaImAgInS	poskytovat
komfortní	komfortní	k2eAgNnSc4d1	komfortní
bydlení	bydlení	k1gNnSc4	bydlení
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgInSc7d1	jediný
větším	veliký	k2eAgInSc7d2	veliký
počinem	počin	k1gInSc7	počin
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
se	se	k3xPyFc4	se
nový	nový	k2eAgMnSc1d1	nový
král	král	k1gMnSc1	král
zapsal	zapsat	k5eAaPmAgMnS	zapsat
do	do	k7c2	do
historie	historie	k1gFnSc2	historie
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
vyplenění	vyplenění	k1gNnSc1	vyplenění
katedrály	katedrála	k1gFnSc2	katedrála
<g/>
.	.	kIx.	.
</s>
<s>
Protestantský	protestantský	k2eAgMnSc1d1	protestantský
král	král	k1gMnSc1	král
totiž	totiž	k9	totiž
nechtěl	chtít	k5eNaImAgMnS	chtít
mít	mít	k5eAaImF	mít
svůj	svůj	k3xOyFgInSc4	svůj
hlavní	hlavní	k2eAgInSc4d1	hlavní
kostel	kostel	k1gInSc4	kostel
plný	plný	k2eAgInSc4d1	plný
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc2	on
modlářských	modlářský	k2eAgNnPc2d1	modlářské
vyobrazení	vyobrazení	k1gNnPc2	vyobrazení
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
velká	velký	k2eAgFnSc1d1	velká
většina	většina	k1gFnSc1	většina
vybavení	vybavení	k1gNnSc2	vybavení
<g/>
,	,	kIx,	,
oltářů	oltář	k1gInPc2	oltář
<g/>
,	,	kIx,	,
obrazů	obraz	k1gInPc2	obraz
a	a	k8xC	a
soch	socha	k1gFnPc2	socha
nenávratně	návratně	k6eNd1	návratně
zničena	zničit	k5eAaPmNgFnS	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
Fridricha	Fridrich	k1gMnSc2	Fridrich
Falckého	falcký	k2eAgMnSc2d1	falcký
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
katedrála	katedrál	k1gMnSc2	katedrál
postupně	postupně	k6eAd1	postupně
dostávala	dostávat	k5eAaImAgFnS	dostávat
nové	nový	k2eAgNnSc4d1	nové
vybavení	vybavení	k1gNnSc4	vybavení
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
kus	kus	k1gInSc4	kus
-	-	kIx~	-
kalvárii	kalvárie	k1gFnSc4	kalvárie
-	-	kIx~	-
věnoval	věnovat	k5eAaPmAgMnS	věnovat
sám	sám	k3xTgMnSc1	sám
císař	císař	k1gMnSc1	císař
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
koncem	konec	k1gInSc7	konec
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
císař	císař	k1gMnSc1	císař
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
III	III	kA	III
<g/>
.	.	kIx.	.
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
rozšířit	rozšířit	k5eAaPmF	rozšířit
Nový	nový	k2eAgInSc1d1	nový
královský	královský	k2eAgInSc1d1	královský
palác	palác	k1gInSc1	palác
o	o	k7c4	o
císařovnino	císařovnin	k2eAgNnSc4d1	císařovnino
křídlo	křídlo	k1gNnSc4	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
předtím	předtím	k6eAd1	předtím
byl	být	k5eAaImAgInS	být
byt	byt	k1gInSc1	byt
císařovny	císařovna	k1gFnSc2	císařovna
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
patře	patro	k1gNnSc6	patro
a	a	k8xC	a
byt	byt	k1gInSc1	byt
císařův	císařův	k2eAgInSc1d1	císařův
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
patře	patro	k1gNnSc6	patro
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
nové	nový	k2eAgNnSc1d1	nové
rozšíření	rozšíření	k1gNnSc1	rozšíření
přesunulo	přesunout	k5eAaPmAgNnS	přesunout
byt	byt	k1gInSc4	byt
císařovny	císařovna	k1gFnSc2	císařovna
do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
patra	patro	k1gNnSc2	patro
k	k	k7c3	k
bytu	byt	k1gInSc3	byt
císařovu	císařův	k2eAgInSc3d1	císařův
<g/>
.	.	kIx.	.
</s>
<s>
Centrální	centrální	k2eAgFnSc7d1	centrální
místností	místnost	k1gFnSc7	místnost
paláce	palác	k1gInSc2	palác
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
stala	stát	k5eAaPmAgFnS	stát
společná	společný	k2eAgFnSc1d1	společná
ložnice	ložnice	k1gFnSc1	ložnice
císařského	císařský	k2eAgInSc2d1	císařský
páru	pár	k1gInSc2	pár
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
velice	velice	k6eAd1	velice
moderní	moderní	k2eAgFnSc7d1	moderní
záležitostí	záležitost	k1gFnSc7	záležitost
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byly	být	k5eAaImAgFnP	být
ložnice	ložnice	k1gFnPc1	ložnice
českých	český	k2eAgMnPc2d1	český
královských	královský	k2eAgMnPc2d1	královský
či	či	k8xC	či
císařských	císařský	k2eAgInPc2d1	císařský
manželských	manželský	k2eAgInPc2d1	manželský
párů	pár	k1gInPc2	pár
oddělené	oddělený	k2eAgNnSc1d1	oddělené
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
ložnice	ložnice	k1gFnSc2	ložnice
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
dalo	dát	k5eAaPmAgNnS	dát
vyjít	vyjít	k5eAaPmF	vyjít
jedním	jeden	k4xCgInSc7	jeden
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
soukromých	soukromý	k2eAgInPc2d1	soukromý
a	a	k8xC	a
reprezentačních	reprezentační	k2eAgInPc2d1	reprezentační
pokojů	pokoj	k1gInPc2	pokoj
císaře	císař	k1gMnSc2	císař
a	a	k8xC	a
směrem	směr	k1gInSc7	směr
druhým	druhý	k4xOgInSc7	druhý
do	do	k7c2	do
soukromých	soukromý	k2eAgInPc2d1	soukromý
a	a	k8xC	a
reprezentačních	reprezentační	k2eAgInPc2d1	reprezentační
pokojů	pokoj	k1gInPc2	pokoj
císařovny	císařovna	k1gFnSc2	císařovna
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
řady	řada	k1gFnPc1	řada
místnosti	místnost	k1gFnSc2	místnost
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgFnP	být
zakončeny	zakončit	k5eAaPmNgFnP	zakončit
vstupním	vstupní	k2eAgNnSc7d1	vstupní
schodištěm	schodiště	k1gNnSc7	schodiště
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
nové	nový	k2eAgNnSc1d1	nové
pražské	pražský	k2eAgNnSc1d1	Pražské
schéma	schéma	k1gNnSc1	schéma
císařského	císařský	k2eAgNnSc2d1	císařské
obydlí	obydlí	k1gNnSc2	obydlí
pak	pak	k6eAd1	pak
kopírovaly	kopírovat	k5eAaImAgFnP	kopírovat
ostatní	ostatní	k2eAgFnPc1d1	ostatní
císařské	císařský	k2eAgFnPc1d1	císařská
rezidence	rezidence	k1gFnPc1	rezidence
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
vídeňského	vídeňský	k2eAgInSc2d1	vídeňský
Hofburgu	Hofburg	k1gInSc2	Hofburg
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
paláci	palác	k1gInSc6	palác
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
nová	nový	k2eAgFnSc1d1	nová
kaple	kaple	k1gFnSc1	kaple
<g/>
,	,	kIx,	,
na	na	k7c4	na
císařovo	císařův	k2eAgNnSc4d1	císařovo
přání	přání	k1gNnSc4	přání
zasvěcená	zasvěcený	k2eAgFnSc1d1	zasvěcená
sv.	sv.	kA	sv.
Václavu	Václava	k1gFnSc4	Václava
a	a	k8xC	a
rudolfinský	rudolfinský	k2eAgInSc4d1	rudolfinský
Střední	střední	k2eAgInSc4d1	střední
trakt	trakt	k1gInSc4	trakt
byl	být	k5eAaImAgInS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
tzv.	tzv.	kA	tzv.
novými	nový	k2eAgInPc7d1	nový
salony	salon	k1gInPc7	salon
pro	pro	k7c4	pro
doprovod	doprovod	k1gInSc4	doprovod
císařovny	císařovna	k1gFnSc2	císařovna
<g/>
.	.	kIx.	.
</s>
<s>
Stavební	stavební	k2eAgFnPc1d1	stavební
práce	práce	k1gFnPc1	práce
byly	být	k5eAaImAgFnP	být
ukončeny	ukončit	k5eAaPmNgFnP	ukončit
roku	rok	k1gInSc3	rok
1644	[number]	k4	1644
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1648	[number]	k4	1648
Švédové	Švéd	k1gMnPc1	Švéd
vtrhli	vtrhnout	k5eAaPmAgMnP	vtrhnout
na	na	k7c4	na
Pražský	pražský	k2eAgInSc4d1	pražský
hrad	hrad	k1gInSc4	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Poničili	poničit	k5eAaPmAgMnP	poničit
převážně	převážně	k6eAd1	převážně
interiéry	interiér	k1gInPc4	interiér
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavně	hlavně	k9	hlavně
ukradli	ukradnout	k5eAaPmAgMnP	ukradnout
většinu	většina	k1gFnSc4	většina
rudolfinských	rudolfinský	k2eAgFnPc2d1	Rudolfinská
sbírek	sbírka	k1gFnPc2	sbírka
<g/>
.	.	kIx.	.
</s>
<s>
Královská	královský	k2eAgFnSc1d1	královská
rodina	rodina	k1gFnSc1	rodina
(	(	kIx(	(
<g/>
především	především	k9	především
císař	císař	k1gMnSc1	císař
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
arcivévoda	arcivévoda	k1gMnSc1	arcivévoda
Leopold	Leopold	k1gMnSc1	Leopold
Vílém	Vílé	k1gNnSc6	Vílé
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
okamžitě	okamžitě	k6eAd1	okamžitě
snažila	snažit	k5eAaImAgFnS	snažit
rozkradenou	rozkradený	k2eAgFnSc4d1	rozkradená
obrazárnu	obrazárna	k1gFnSc4	obrazárna
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
obnovit	obnovit	k5eAaPmF	obnovit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1652	[number]	k4	1652
tedy	tedy	k9	tedy
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
nová	nový	k2eAgFnSc1d1	nová
obrazárna	obrazárna	k1gFnSc1	obrazárna
tvořená	tvořený	k2eAgFnSc1d1	tvořená
zachráněnými	zachráněný	k2eAgInPc7d1	zachráněný
rudolfinskými	rudolfinský	k2eAgInPc7d1	rudolfinský
obrazy	obraz	k1gInPc7	obraz
a	a	k8xC	a
nově	nově	k6eAd1	nově
přikoupenou	přikoupený	k2eAgFnSc7d1	přikoupená
bývalou	bývalý	k2eAgFnSc7d1	bývalá
Buckinghamovou	Buckinghamový	k2eAgFnSc7d1	Buckinghamový
sbírkou	sbírka	k1gFnSc7	sbírka
<g/>
.	.	kIx.	.
</s>
<s>
Výrazně	výrazně	k6eAd1	výrazně
se	se	k3xPyFc4	se
do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
zapsal	zapsat	k5eAaPmAgInS	zapsat
i	i	k9	i
císař	císař	k1gMnSc1	císař
a	a	k8xC	a
král	král	k1gMnSc1	král
Leopold	Leopold	k1gMnSc1	Leopold
I.	I.	kA	I.
Z	z	k7c2	z
několika	několik	k4yIc7	několik
jeho	jeho	k3xOp3gFnPc2	jeho
návštěv	návštěva	k1gFnPc2	návštěva
Prahy	Praha	k1gFnSc2	Praha
byl	být	k5eAaImAgMnS	být
nejvýznamnější	významný	k2eAgInSc4d3	nejvýznamnější
jeho	on	k3xPp3gInSc4	on
pobyt	pobyt	k1gInSc4	pobyt
od	od	k7c2	od
září	září	k1gNnSc2	září
1679	[number]	k4	1679
do	do	k7c2	do
května	květen	k1gInSc2	květen
1680	[number]	k4	1680
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
pobýval	pobývat	k5eAaImAgMnS	pobývat
dokonce	dokonce	k9	dokonce
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
rodinou	rodina	k1gFnSc7	rodina
i	i	k8xC	i
dvorem	dvůr	k1gInSc7	dvůr
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
řádil	řádit	k5eAaImAgInS	řádit
mor	mor	k1gInSc1	mor
<g/>
.	.	kIx.	.
</s>
<s>
Mor	mor	k1gInSc1	mor
(	(	kIx(	(
<g/>
tentokrát	tentokrát	k6eAd1	tentokrát
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
ho	on	k3xPp3gNnSc4	on
donutil	donutit	k5eAaPmAgMnS	donutit
Prahu	Praha	k1gFnSc4	Praha
i	i	k9	i
opustit	opustit	k5eAaPmF	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
vládu	vláda	k1gFnSc4	vláda
dodnes	dodnes	k6eAd1	dodnes
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
hradu	hrad	k1gInSc2	hrad
připomínají	připomínat	k5eAaImIp3nP	připomínat
barokní	barokní	k2eAgFnPc1d1	barokní
kašny	kašna	k1gFnPc1	kašna
s	s	k7c7	s
korunovaným	korunovaný	k2eAgNnSc7d1	korunované
písmenem	písmeno	k1gNnSc7	písmeno
"	"	kIx"	"
<g/>
L	L	kA	L
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Leopold	Leopold	k1gMnSc1	Leopold
se	se	k3xPyFc4	se
také	také	k9	také
pokusil	pokusit	k5eAaPmAgMnS	pokusit
o	o	k7c4	o
barokní	barokní	k2eAgFnSc4d1	barokní
dostavbu	dostavba	k1gFnSc4	dostavba
katedrály	katedrála	k1gFnSc2	katedrála
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
příliš	příliš	k6eAd1	příliš
finančně	finančně	k6eAd1	finančně
náročný	náročný	k2eAgInSc4d1	náročný
záměr	záměr	k1gInSc4	záměr
pak	pak	k6eAd1	pak
dlouho	dlouho	k6eAd1	dlouho
připomínaly	připomínat	k5eAaImAgInP	připomínat
rozestavěné	rozestavěný	k2eAgInPc1d1	rozestavěný
pilíře	pilíř	k1gInPc1	pilíř
<g/>
.	.	kIx.	.
</s>
<s>
Leopold	Leopold	k1gMnSc1	Leopold
nechal	nechat	k5eAaPmAgMnS	nechat
za	za	k7c7	za
Jelením	jelení	k2eAgInSc7d1	jelení
příkopem	příkop	k1gInSc7	příkop
roku	rok	k1gInSc2	rok
1680	[number]	k4	1680
postavit	postavit	k5eAaPmF	postavit
divadlo	divadlo	k1gNnSc1	divadlo
Comedihaus	Comedihaus	k1gInSc4	Comedihaus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1694-1695	[number]	k4	1694-1695
pak	pak	k6eAd1	pak
nechal	nechat	k5eAaPmAgMnS	nechat
vystavět	vystavět	k5eAaPmF	vystavět
novou	nový	k2eAgFnSc4d1	nová
monumentální	monumentální	k2eAgFnSc4d1	monumentální
jízdárnu	jízdárna	k1gFnSc4	jízdárna
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
architektem	architekt	k1gMnSc7	architekt
byl	být	k5eAaImAgInS	být
J.	J.	kA	J.
B.	B.	kA	B.
Mathey	Mathea	k1gMnSc2	Mathea
<g/>
.	.	kIx.	.
</s>
<s>
Pražský	pražský	k2eAgInSc4d1	pražský
hrad	hrad	k1gInSc4	hrad
zažil	zažít	k5eAaPmAgMnS	zažít
během	během	k7c2	během
dvacátých	dvacátý	k4xOgNnPc2	dvacátý
let	léto	k1gNnPc2	léto
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
tři	tři	k4xCgFnPc1	tři
velké	velká	k1gFnPc1	velká
vskutku	vskutku	k9	vskutku
barokní	barokní	k2eAgFnPc1d1	barokní
slavnosti	slavnost	k1gFnPc1	slavnost
-	-	kIx~	-
korunovaci	korunovace	k1gFnSc4	korunovace
císaře	císař	k1gMnSc2	císař
Karla	Karel	k1gMnSc2	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
(	(	kIx(	(
<g/>
1723	[number]	k4	1723
<g/>
)	)	kIx)	)
a	a	k8xC	a
slavnosti	slavnost	k1gFnPc1	slavnost
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
blahořečení	blahořečení	k1gNnSc2	blahořečení
(	(	kIx(	(
<g/>
1721	[number]	k4	1721
<g/>
)	)	kIx)	)
a	a	k8xC	a
svatořečení	svatořečení	k1gNnSc4	svatořečení
(	(	kIx(	(
<g/>
1729	[number]	k4	1729
<g/>
)	)	kIx)	)
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
blahořečením	blahořečení	k1gNnSc7	blahořečení
a	a	k8xC	a
svatořečením	svatořečení	k1gNnSc7	svatořečení
pak	pak	k6eAd1	pak
souvisela	souviset	k5eAaImAgFnS	souviset
stavba	stavba	k1gFnSc1	stavba
kaple	kaple	k1gFnSc2	kaple
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
při	při	k7c6	při
svatojiřské	svatojiřský	k2eAgFnSc6d1	Svatojiřská
bazilice	bazilika	k1gFnSc6	bazilika
a	a	k8xC	a
zřízení	zřízení	k1gNnSc6	zřízení
stříbrného	stříbrný	k2eAgInSc2d1	stříbrný
náhrobku	náhrobek	k1gInSc2	náhrobek
světce	světec	k1gMnSc2	světec
ve	v	k7c6	v
svatovítské	svatovítský	k2eAgFnSc6d1	Svatovítská
katedrále	katedrála	k1gFnSc6	katedrála
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
byla	být	k5eAaImAgFnS	být
poznamenána	poznamenán	k2eAgFnSc1d1	poznamenána
válkami	válka	k1gFnPc7	válka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
dotýkaly	dotýkat	k5eAaImAgFnP	dotýkat
i	i	k9	i
přímo	přímo	k6eAd1	přímo
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Pražský	pražský	k2eAgInSc1d1	pražský
hrad	hrad	k1gInSc1	hrad
byl	být	k5eAaImAgInS	být
obléhán	obléhán	k2eAgMnSc1d1	obléhán
Francouzi	Francouz	k1gMnPc1	Francouz
(	(	kIx(	(
<g/>
1741	[number]	k4	1741
<g/>
)	)	kIx)	)
i	i	k9	i
Prusy	Prus	k1gMnPc7	Prus
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
se	se	k3xPyFc4	se
dala	dát	k5eAaPmAgFnS	dát
roku	rok	k1gInSc2	rok
1743	[number]	k4	1743
korunovat	korunovat	k5eAaBmF	korunovat
českou	český	k2eAgFnSc7d1	Česká
královnou	královna	k1gFnSc7	královna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tato	tento	k3xDgFnSc1	tento
narychlo	narychlo	k6eAd1	narychlo
uspořádaná	uspořádaný	k2eAgFnSc1d1	uspořádaná
korunovace	korunovace	k1gFnSc1	korunovace
se	se	k3xPyFc4	se
nemohla	moct	k5eNaImAgFnS	moct
pompou	pompa	k1gFnSc7	pompa
rovnat	rovnat	k5eAaImF	rovnat
korunovaci	korunovace	k1gFnSc4	korunovace
Karla	Karel	k1gMnSc2	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1755-1775	[number]	k4	1755-1775
pak	pak	k9	pak
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
obrovská	obrovský	k2eAgFnSc1d1	obrovská
přestavba	přestavba	k1gFnSc1	přestavba
západní	západní	k2eAgFnSc2d1	západní
části	část	k1gFnSc2	část
hradního	hradní	k2eAgInSc2d1	hradní
areálu	areál	k1gInSc2	areál
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
dostal	dostat	k5eAaPmAgMnS	dostat
Nový	nový	k2eAgInSc4d1	nový
královský	královský	k2eAgInSc4d1	královský
palác	palác	k1gInSc4	palác
svou	svůj	k3xOyFgFnSc4	svůj
dnešní	dnešní	k2eAgFnSc4d1	dnešní
jednotnou	jednotný	k2eAgFnSc4d1	jednotná
fasádu	fasáda	k1gFnSc4	fasáda
a	a	k8xC	a
úplně	úplně	k6eAd1	úplně
nově	nově	k6eAd1	nově
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
čestné	čestný	k2eAgNnSc1d1	čestné
nádvoří	nádvoří	k1gNnSc1	nádvoří
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
court	court	k1gInSc1	court
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
honneur	honneur	k1gMnSc1	honneur
<g/>
)	)	kIx)	)
-	-	kIx~	-
I.	I.	kA	I.
nádvoří	nádvoří	k1gNnSc1	nádvoří
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
stavebně	stavebně	k6eAd1	stavebně
nesourodého	sourodý	k2eNgInSc2d1	nesourodý
hradu	hrad	k1gInSc2	hrad
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
měla	mít	k5eAaImAgFnS	mít
stát	stát	k5eAaPmF	stát
reprezentativní	reprezentativní	k2eAgFnSc1d1	reprezentativní
královská	královský	k2eAgFnSc1d1	královská
(	(	kIx(	(
<g/>
i	i	k9	i
císařská	císařský	k2eAgFnSc1d1	císařská
<g/>
)	)	kIx)	)
rezidence	rezidence	k1gFnSc1	rezidence
zámeckého	zámecký	k2eAgInSc2d1	zámecký
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Architektem	architekt	k1gMnSc7	architekt
této	tento	k3xDgFnSc2	tento
přestavby	přestavba	k1gFnSc2	přestavba
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
vídeňského	vídeňský	k2eAgInSc2d1	vídeňský
barokního	barokní	k2eAgInSc2d1	barokní
klasicismu	klasicismus	k1gInSc2	klasicismus
byl	být	k5eAaImAgInS	být
Nicolo	Nicola	k1gFnSc5	Nicola
Pacassi	Pacasse	k1gFnSc4	Pacasse
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
zrušil	zrušit	k5eAaPmAgInS	zrušit
klášter	klášter	k1gInSc1	klášter
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc1	Jiří
a	a	k8xC	a
zřídil	zřídit	k5eAaPmAgMnS	zřídit
zde	zde	k6eAd1	zde
kasárna	kasárna	k1gNnPc4	kasárna
<g/>
.	.	kIx.	.
</s>
<s>
Právo	právo	k1gNnSc1	právo
korunovat	korunovat	k5eAaBmF	korunovat
českou	český	k2eAgFnSc4d1	Česká
královnu	královna	k1gFnSc4	královna
tehdy	tehdy	k6eAd1	tehdy
přešlo	přejít	k5eAaPmAgNnS	přejít
z	z	k7c2	z
abatyše	abatyše	k1gFnSc2	abatyše
kláštera	klášter	k1gInSc2	klášter
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc1	Jiří
na	na	k7c6	na
abatyši	abatyše	k1gFnSc6	abatyše
blízkého	blízký	k2eAgInSc2d1	blízký
Ústavu	ústav	k1gInSc2	ústav
šlechtičen	šlechtična	k1gFnPc2	šlechtična
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
abdikaci	abdikace	k1gFnSc6	abdikace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1875	[number]	k4	1875
bydlel	bydlet	k5eAaImAgMnS	bydlet
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
královském	královský	k2eAgInSc6d1	královský
paláci	palác	k1gInSc6	palác
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
rakouský	rakouský	k2eAgMnSc1d1	rakouský
císař	císař	k1gMnSc1	císař
a	a	k8xC	a
poslední	poslední	k2eAgMnSc1d1	poslední
korunovaný	korunovaný	k2eAgMnSc1d1	korunovaný
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
Dobrotivý	dobrotivý	k2eAgInSc1d1	dobrotivý
<g/>
.	.	kIx.	.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
Marií	Maria	k1gFnSc7	Maria
Annou	Anna	k1gFnSc7	Anna
inicioval	iniciovat	k5eAaBmAgMnS	iniciovat
přestavbu	přestavba	k1gFnSc4	přestavba
kaple	kaple	k1gFnSc2	kaple
svatého	svatý	k2eAgInSc2d1	svatý
Kříže	kříž	k1gInSc2	kříž
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
nádvoří	nádvoří	k1gNnSc6	nádvoří
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
příprav	příprava	k1gFnPc2	příprava
korunovace	korunovace	k1gFnSc2	korunovace
císaře	císař	k1gMnSc4	císař
Františka	František	k1gMnSc4	František
Josefa	Josef	k1gMnSc4	Josef
I.	I.	kA	I.
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1865	[number]	k4	1865
<g/>
-	-	kIx~	-
<g/>
1868	[number]	k4	1868
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
podoby	podoba	k1gFnSc2	podoba
upraven	upraven	k2eAgInSc1d1	upraven
Španělský	španělský	k2eAgInSc1d1	španělský
sál	sál	k1gInSc1	sál
a	a	k8xC	a
Rudolfova	Rudolfův	k2eAgFnSc1d1	Rudolfova
galerie	galerie	k1gFnSc1	galerie
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
královském	královský	k2eAgInSc6d1	královský
paláci	palác	k1gInSc6	palác
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1873	[number]	k4	1873
byl	být	k5eAaImAgInS	být
kardinálem	kardinál	k1gMnSc7	kardinál
Bedřichem	Bedřich	k1gMnSc7	Bedřich
Schwarzenbergem	Schwarzenberg	k1gInSc7	Schwarzenberg
položen	položen	k2eAgInSc1d1	položen
základní	základní	k2eAgInSc1d1	základní
kámen	kámen	k1gInSc1	kámen
k	k	k7c3	k
dostavbě	dostavba	k1gFnSc3	dostavba
západní	západní	k2eAgFnSc2d1	západní
části	část	k1gFnSc2	část
katedrály	katedrála	k1gFnSc2	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Víta	Vít	k1gMnSc2	Vít
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
opravě	oprava	k1gFnSc6	oprava
staré	starý	k2eAgFnSc2d1	stará
části	část	k1gFnSc2	část
katedrály	katedrála	k1gFnSc2	katedrála
a	a	k8xC	a
na	na	k7c6	na
její	její	k3xOp3gFnSc6	její
dostavbě	dostavba	k1gFnSc6	dostavba
se	se	k3xPyFc4	se
podíleli	podílet	k5eAaImAgMnP	podílet
tři	tři	k4xCgMnPc1	tři
hlavní	hlavní	k2eAgMnPc1d1	hlavní
architekti	architekt	k1gMnPc1	architekt
<g/>
:	:	kIx,	:
Josef	Josef	k1gMnSc1	Josef
Kranner	Kranner	k1gMnSc1	Kranner
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Mocker	Mocker	k1gMnSc1	Mocker
a	a	k8xC	a
Kamil	Kamil	k1gMnSc1	Kamil
Hilbert	Hilbert	k1gMnSc1	Hilbert
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
prošla	projít	k5eAaPmAgFnS	projít
drastickou	drastický	k2eAgFnSc7d1	drastická
puristickou	puristický	k2eAgFnSc7d1	puristická
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
bazilika	bazilika	k1gFnSc1	bazilika
svatého	svatý	k2eAgMnSc2d1	svatý
Jiří	Jiří	k1gMnSc2	Jiří
a	a	k8xC	a
na	na	k7c6	na
Jiřském	jiřský	k2eAgNnSc6d1	Jiřské
náměstí	náměstí	k1gNnSc6	náměstí
bylo	být	k5eAaImAgNnS	být
Josefem	Josef	k1gMnSc7	Josef
Mockerem	Mocker	k1gInSc7	Mocker
vybudováno	vybudován	k2eAgNnSc1d1	vybudováno
Nové	Nové	k2eAgNnSc1d1	Nové
děkanství	děkanství	k1gNnSc1	děkanství
v	v	k7c6	v
neogotickém	ogotický	k2eNgInSc6d1	neogotický
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
vznikem	vznik	k1gInSc7	vznik
První	první	k4xOgFnSc2	první
československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
vyvstala	vyvstat	k5eAaPmAgFnS	vyvstat
i	i	k9	i
nutnost	nutnost	k1gFnSc1	nutnost
přestavby	přestavba	k1gFnSc2	přestavba
Hradu	hrad	k1gInSc2	hrad
k	k	k7c3	k
jeho	jeho	k3xOp3gFnPc3	jeho
novým	nový	k2eAgFnPc3d1	nová
funkcím	funkce	k1gFnPc3	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
dílem	dílo	k1gNnSc7	dílo
architekta	architekt	k1gMnSc2	architekt
slovinského	slovinský	k2eAgInSc2d1	slovinský
původu	původ	k1gInSc2	původ
Jože	Joža	k1gFnSc6	Joža
Plečnika	Plečnik	k1gMnSc4	Plečnik
a	a	k8xC	a
realizována	realizován	k2eAgFnSc1d1	realizována
byla	být	k5eAaImAgFnS	být
pod	pod	k7c7	pod
záštitou	záštita	k1gFnSc7	záštita
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
Tomáše	Tomáš	k1gMnSc2	Tomáš
Garriguea	Garrigueus	k1gMnSc2	Garrigueus
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
Plečnik	Plečnik	k1gMnSc1	Plečnik
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
též	též	k9	též
dláždění	dláždění	k1gNnSc4	dláždění
nádvoří	nádvoří	k1gNnSc2	nádvoří
<g/>
,	,	kIx,	,
prezidentskou	prezidentský	k2eAgFnSc4d1	prezidentská
rezidenci	rezidence	k1gFnSc4	rezidence
a	a	k8xC	a
přestavěl	přestavět	k5eAaPmAgMnS	přestavět
hradní	hradní	k2eAgFnSc2d1	hradní
zahrady	zahrada	k1gFnSc2	zahrada
obdobným	obdobný	k2eAgInSc7d1	obdobný
přístupem	přístup	k1gInSc7	přístup
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
užil	užít	k5eAaPmAgMnS	užít
Ieoh	Ieoh	k1gMnSc1	Ieoh
Ming	Ming	k1gMnSc1	Ming
Pei	Pei	k1gMnSc1	Pei
při	při	k7c6	při
úpravách	úprava	k1gFnPc6	úprava
Louvru	Louvre	k1gInSc2	Louvre
o	o	k7c4	o
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
vyznačoval	vyznačovat	k5eAaImAgMnS	vyznačovat
užitím	užití	k1gNnSc7	užití
rázné	rázný	k2eAgFnSc2d1	rázná
modernity	modernita	k1gFnSc2	modernita
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
však	však	k9	však
dokázala	dokázat	k5eAaPmAgFnS	dokázat
citlivě	citlivě	k6eAd1	citlivě
integrovat	integrovat	k5eAaBmF	integrovat
dědictví	dědictví	k1gNnSc4	dědictví
minulosti	minulost	k1gFnSc2	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Pei	Pei	k1gFnSc4	Pei
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
i	i	k8xC	i
Plečnik	Plečnik	k1gMnSc1	Plečnik
cizincem	cizinec	k1gMnSc7	cizinec
(	(	kIx(	(
<g/>
profesorem	profesor	k1gMnSc7	profesor
na	na	k7c4	na
pozvání	pozvání	k1gNnSc4	pozvání
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
uměleckoprůmyslové	uměleckoprůmyslový	k2eAgFnSc2d1	uměleckoprůmyslová
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
zakázku	zakázka	k1gFnSc4	zakázka
na	na	k7c6	na
základě	základ	k1gInSc6	základ
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
,	,	kIx,	,
a	a	k8xC	a
jehož	jehož	k3xOyRp3gNnSc1	jehož
konečné	konečný	k2eAgNnSc1d1	konečné
architektonické	architektonický	k2eAgNnSc1d1	architektonické
řešení	řešení	k1gNnSc1	řešení
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
ostře	ostro	k6eAd1	ostro
kritizováno	kritizován	k2eAgNnSc1d1	kritizováno
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
vchodu	vchod	k1gInSc2	vchod
do	do	k7c2	do
prvního	první	k4xOgNnSc2	první
nádvoří	nádvoří	k1gNnSc2	nádvoří
vedl	vést	k5eAaImAgInS	vést
Plečnik	Plečnik	k1gInSc1	Plečnik
sloupořadí	sloupořadí	k1gNnPc2	sloupořadí
v	v	k7c6	v
plné	plný	k2eAgFnSc6d1	plná
výši	výše	k1gFnSc6	výše
budovy	budova	k1gFnSc2	budova
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
jej	on	k3xPp3gMnSc4	on
spojilo	spojit	k5eAaPmAgNnS	spojit
se	s	k7c7	s
Španělským	španělský	k2eAgInSc7d1	španělský
sálem	sál	k1gInSc7	sál
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jednoduché	jednoduchý	k2eAgNnSc4d1	jednoduché
sloupořadí	sloupořadí	k1gNnSc4	sloupořadí
v	v	k7c6	v
ionském	ionský	k2eAgInSc6d1	ionský
stylu	styl	k1gInSc6	styl
zastřešené	zastřešený	k2eAgNnSc1d1	zastřešené
kazetovým	kazetový	k2eAgInSc7d1	kazetový
stropem	strop	k1gInSc7	strop
<g/>
.	.	kIx.	.
</s>
<s>
Čelní	čelní	k2eAgFnSc1d1	čelní
stěna	stěna	k1gFnSc1	stěna
se	s	k7c7	s
vstupem	vstup	k1gInSc7	vstup
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
Rothmayerův	Rothmayerův	k2eAgInSc4d1	Rothmayerův
sál	sál	k1gInSc4	sál
<g/>
)	)	kIx)	)
vedoucím	vedoucí	k1gMnSc7	vedoucí
do	do	k7c2	do
Španělského	španělský	k2eAgInSc2d1	španělský
sálu	sál	k1gInSc2	sál
byla	být	k5eAaImAgFnS	být
pojata	pojat	k2eAgFnSc1d1	pojata
jako	jako	k8xC	jako
triumfální	triumfální	k2eAgFnSc1d1	triumfální
brána	brána	k1gFnSc1	brána
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Plečnik	Plečnik	k1gMnSc1	Plečnik
se	se	k3xPyFc4	se
nedožil	dožít	k5eNaPmAgMnS	dožít
realizace	realizace	k1gFnSc2	realizace
všech	všecek	k3xTgInPc2	všecek
svých	svůj	k3xOyFgInPc2	svůj
projektů	projekt	k1gInPc2	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
výstavní	výstavní	k2eAgFnSc1d1	výstavní
síň	síň	k1gFnSc1	síň
v	v	k7c6	v
Jízdárně	jízdárna	k1gFnSc6	jízdárna
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
až	až	k9	až
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
jeho	jeho	k3xOp3gMnSc2	jeho
nástupce	nástupce	k1gMnSc2	nástupce
Pavla	Pavel	k1gMnSc2	Pavel
Janáka	Janák	k1gMnSc2	Janák
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
tvoří	tvořit	k5eAaImIp3nP	tvořit
původně	původně	k6eAd1	původně
císařské	císařský	k2eAgFnPc1d1	císařská
palácové	palácový	k2eAgFnPc1d1	palácová
budovy	budova	k1gFnPc1	budova
uspořádané	uspořádaný	k2eAgFnPc1d1	uspořádaná
kolem	kolem	k7c2	kolem
třech	tři	k4xCgInPc2	tři
nádvoří	nádvoří	k1gNnPc2	nádvoří
<g/>
,	,	kIx,	,
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
podobě	podoba	k1gFnSc6	podoba
od	od	k7c2	od
Nicolo	Nicola	k1gFnSc5	Nicola
Pacassiho	Pacassi	k1gMnSc2	Pacassi
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
nejvýznamnější	významný	k2eAgFnPc1d3	nejvýznamnější
stavby	stavba	k1gFnPc1	stavba
<g/>
,	,	kIx,	,
katedrála	katedrála	k1gFnSc1	katedrála
<g/>
,	,	kIx,	,
Starý	starý	k2eAgInSc1d1	starý
královský	královský	k2eAgInSc1d1	královský
palác	palác	k1gInSc1	palác
a	a	k8xC	a
bazilika	bazilika	k1gFnSc1	bazilika
s	s	k7c7	s
bývalým	bývalý	k2eAgInSc7d1	bývalý
klášterem	klášter	k1gInSc7	klášter
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
(	(	kIx(	(
<g/>
při	při	k7c6	při
Jiřském	jiřský	k2eAgNnSc6d1	Jiřské
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgInPc1	dva
původně	původně	k6eAd1	původně
šlechtické	šlechtický	k2eAgInPc1d1	šlechtický
paláce	palác	k1gInPc1	palác
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
<g/>
)	)	kIx)	)
obytné	obytný	k2eAgInPc1d1	obytný
domy	dům	k1gInPc1	dům
a	a	k8xC	a
zachované	zachovaný	k2eAgFnPc1d1	zachovaná
části	část	k1gFnPc1	část
středověkého	středověký	k2eAgNnSc2d1	středověké
opevnění	opevnění	k1gNnSc2	opevnění
včetně	včetně	k7c2	včetně
věží	věž	k1gFnPc2	věž
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
strana	strana	k1gFnSc1	strana
hradu	hrad	k1gInSc2	hrad
je	být	k5eAaImIp3nS	být
lemována	lemovat	k5eAaImNgFnS	lemovat
zahradami	zahrada	k1gFnPc7	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Pražskému	pražský	k2eAgInSc3d1	pražský
hradu	hrad	k1gInSc3	hrad
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
prostory	prostor	k1gInPc4	prostor
mimo	mimo	k7c4	mimo
ostrožnu	ostrožna	k1gFnSc4	ostrožna
<g/>
,	,	kIx,	,
za	za	k7c7	za
Jelením	jelení	k2eAgInSc7d1	jelení
příkopem	příkop	k1gInSc7	příkop
na	na	k7c6	na
severu	sever	k1gInSc6	sever
rovnoběžně	rovnoběžně	k6eAd1	rovnoběžně
ležící	ležící	k2eAgFnSc1d1	ležící
Královská	královský	k2eAgFnSc1d1	královská
zahrada	zahrada	k1gFnSc1	zahrada
s	s	k7c7	s
letohrádkem	letohrádek	k1gInSc7	letohrádek
a	a	k8xC	a
na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
situovaná	situovaný	k2eAgFnSc1d1	situovaná
Lumbeho	Lumbe	k1gMnSc2	Lumbe
zahrada	zahrada	k1gFnSc1	zahrada
s	s	k7c7	s
přilehlou	přilehlý	k2eAgFnSc7d1	přilehlá
jízdárnou	jízdárna	k1gFnSc7	jízdárna
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
památek	památka	k1gFnPc2	památka
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
katedrála	katedrála	k1gFnSc1	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Víta	Vít	k1gMnSc2	Vít
nebo	nebo	k8xC	nebo
Starý	Starý	k1gMnSc1	Starý
královský	královský	k2eAgInSc4d1	královský
palác	palác	k1gInSc4	palác
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
navštívit	navštívit	k5eAaPmF	navštívit
i	i	k9	i
několik	několik	k4yIc4	několik
trvalých	trvalý	k2eAgFnPc2d1	trvalá
expozic	expozice	k1gFnPc2	expozice
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
patří	patřit	k5eAaImIp3nS	patřit
Obrazárna	obrazárna	k1gFnSc1	obrazárna
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
v	v	k7c6	v
bývalých	bývalý	k2eAgFnPc6d1	bývalá
stájích	stáj	k1gFnPc6	stáj
Nového	Nového	k2eAgInSc2d1	Nového
královského	královský	k2eAgInSc2d1	královský
paláce	palác	k1gInSc2	palác
<g/>
,	,	kIx,	,
Příběh	příběh	k1gInSc1	příběh
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
ve	v	k7c6	v
Starém	starý	k2eAgInSc6d1	starý
královském	královský	k2eAgInSc6d1	královský
paláci	palác	k1gInSc6	palác
a	a	k8xC	a
Svatovítský	svatovítský	k2eAgInSc1d1	svatovítský
poklad	poklad	k1gInSc1	poklad
<g/>
,	,	kIx,	,
vystavený	vystavený	k2eAgMnSc1d1	vystavený
v	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
svatého	svatý	k2eAgMnSc2d1	svatý
Kříže	Kříž	k1gMnSc2	Kříž
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
Jízdárně	jízdárna	k1gFnSc6	jízdárna
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
a	a	k8xC	a
v	v	k7c6	v
Císařské	císařský	k2eAgFnSc6d1	císařská
konírně	konírna	k1gFnSc6	konírna
konají	konat	k5eAaImIp3nP	konat
nejrůznější	různý	k2eAgFnPc4d3	nejrůznější
krátkodobé	krátkodobý	k2eAgFnPc4d1	krátkodobá
výstavy	výstava	k1gFnPc4	výstava
<g/>
.	.	kIx.	.
</s>
<s>
Budovy	budova	k1gFnPc1	budova
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
prakticky	prakticky	k6eAd1	prakticky
všechny	všechen	k3xTgInPc4	všechen
architektonické	architektonický	k2eAgInPc4d1	architektonický
styly	styl	k1gInPc4	styl
2	[number]	k4	2
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Pražském	pražský	k2eAgInSc6d1	pražský
hradu	hrad	k1gInSc6	hrad
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
celkem	celkem	k6eAd1	celkem
4	[number]	k4	4
nádvoří	nádvoří	k1gNnPc2	nádvoří
<g/>
.	.	kIx.	.
</s>
<s>
Katedrála	katedrála	k1gFnSc1	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Víta	Vít	k1gMnSc2	Vít
<g/>
,	,	kIx,	,
Václava	Václav	k1gMnSc2	Václav
a	a	k8xC	a
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
(	(	kIx(	(
<g/>
uvnitř	uvnitř	k6eAd1	uvnitř
Hrobka	hrobka	k1gFnSc1	hrobka
českých	český	k2eAgMnPc2d1	český
králů	král	k1gMnPc2	král
a	a	k8xC	a
kaple	kaple	k1gFnSc1	kaple
Svatováclavská	svatováclavský	k2eAgFnSc1d1	Svatováclavská
<g/>
)	)	kIx)	)
Bazilika	bazilika	k1gFnSc1	bazilika
svatého	svatý	k2eAgMnSc2d1	svatý
Jiří	Jiří	k1gMnSc2	Jiří
Kaple	kaple	k1gFnSc2	kaple
svatého	svatý	k2eAgInSc2d1	svatý
Kříže	kříž	k1gInSc2	kříž
Kostel	kostel	k1gInSc1	kostel
Všech	všecek	k3xTgFnPc2	všecek
svatých	svatá	k1gFnPc2	svatá
Kaple	kaple	k1gFnSc2	kaple
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
Trojice	trojice	k1gFnSc2	trojice
v	v	k7c6	v
Ústavu	ústav	k1gInSc6	ústav
šlechtičen	šlechtična	k1gFnPc2	šlechtična
(	(	kIx(	(
<g/>
bývalý	bývalý	k2eAgInSc1d1	bývalý
Rožmberský	rožmberský	k2eAgInSc1d1	rožmberský
palác	palác	k1gInSc1	palác
<g/>
)	)	kIx)	)
bývalý	bývalý	k2eAgInSc1d1	bývalý
klášter	klášter	k1gInSc1	klášter
svatého	svatý	k2eAgMnSc2d1	svatý
Jiří	Jiří	k1gMnSc2	Jiří
Starý	starý	k2eAgInSc1d1	starý
královský	královský	k2eAgInSc1d1	královský
palác	palác	k1gInSc1	palác
Nový	nový	k2eAgInSc1d1	nový
královský	královský	k2eAgInSc1d1	královský
<g />
.	.	kIx.	.
</s>
<s>
palác	palác	k1gInSc1	palác
Letohrádek	letohrádek	k1gInSc1	letohrádek
královny	královna	k1gFnSc2	královna
Anny	Anna	k1gFnSc2	Anna
Lobkovický	lobkovický	k2eAgInSc4d1	lobkovický
palác	palác	k1gInSc4	palác
Ústav	ústav	k1gInSc1	ústav
šlechtičen	šlechtična	k1gFnPc2	šlechtična
(	(	kIx(	(
<g/>
bývalý	bývalý	k2eAgInSc1d1	bývalý
Rožmberský	rožmberský	k2eAgInSc1d1	rožmberský
palác	palác	k1gInSc1	palác
<g/>
)	)	kIx)	)
Vladislavský	vladislavský	k2eAgInSc1d1	vladislavský
sál	sál	k1gInSc1	sál
Španělský	španělský	k2eAgInSc1d1	španělský
sál	sál	k1gInSc1	sál
Rudolfova	Rudolfův	k2eAgFnSc1d1	Rudolfova
galerie	galerie	k1gFnSc1	galerie
Sloupová	sloupový	k2eAgFnSc1d1	sloupová
síň	síň	k1gFnSc1	síň
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
věž	věž	k1gFnSc1	věž
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
katedrály	katedrála	k1gFnSc2	katedrála
při	při	k7c6	při
její	její	k3xOp3gFnSc6	její
jižní	jižní	k2eAgFnSc6d1	jižní
straně	strana	k1gFnSc6	strana
Daliborka	Daliborka	k1gFnSc1	Daliborka
Prašná	prašný	k2eAgFnSc1d1	prašná
věž	věž	k1gFnSc1	věž
-	-	kIx~	-
Mihulka	mihulka	k1gFnSc1	mihulka
(	(	kIx(	(
<g/>
Nová	nový	k2eAgFnSc1d1	nová
<g/>
)	)	kIx)	)
Bílá	bílý	k2eAgFnSc1d1	bílá
věž	věž	k1gFnSc1	věž
Bílá	bílý	k2eAgFnSc1d1	bílá
věž	věž	k1gFnSc1	věž
(	(	kIx(	(
<g/>
Soběslavova	Soběslavův	k2eAgFnSc1d1	Soběslavova
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Černá	černý	k2eAgFnSc1d1	černá
věž	věž	k1gFnSc1	věž
Jižní	jižní	k2eAgFnSc1d1	jižní
věž	věž	k1gFnSc1	věž
(	(	kIx(	(
<g/>
brána	brána	k1gFnSc1	brána
<g/>
)	)	kIx)	)
Biskupská	biskupský	k2eAgFnSc1d1	biskupská
(	(	kIx(	(
<g/>
Matematická	matematický	k2eAgFnSc1d1	matematická
<g/>
)	)	kIx)	)
věž	věž	k1gFnSc1	věž
-	-	kIx~	-
zaniklá	zaniklý	k2eAgFnSc1d1	zaniklá
Prašný	prašný	k2eAgInSc4d1	prašný
most	most	k1gInSc4	most
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
ulička	ulička	k1gFnSc1	ulička
Staré	Staré	k2eAgNnSc2d1	Staré
proboštství	proboštství	k1gNnSc2	proboštství
Kapitulní	kapitulní	k2eAgNnSc1d1	kapitulní
děkanství	děkanství	k1gNnSc1	děkanství
Jízdárna	jízdárna	k1gFnSc1	jízdárna
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
Lví	lví	k2eAgInSc4d1	lví
dvůr	dvůr	k1gInSc4	dvůr
Míčovna	míčovna	k1gFnSc1	míčovna
Zvon	zvon	k1gInSc1	zvon
Zikmund	Zikmund	k1gMnSc1	Zikmund
obelisk	obelisk	k1gInSc1	obelisk
na	na	k7c6	na
III	III	kA	III
<g/>
.	.	kIx.	.
hradním	hradní	k2eAgNnSc6d1	hradní
nádvoří	nádvoří	k1gNnSc6	nádvoří
Sousoší	sousoší	k1gNnSc1	sousoší
Souboj	souboj	k1gInSc4	souboj
Titánů	Titán	k1gMnPc2	Titán
Socha	Socha	k1gMnSc1	Socha
svatého	svatý	k2eAgMnSc2d1	svatý
Jiří	Jiří	k1gMnSc2	Jiří
Socha	Socha	k1gMnSc1	Socha
svatého	svatý	k1gMnSc2	svatý
<g />
.	.	kIx.	.
</s>
<s>
Petra	Petra	k1gFnSc1	Petra
Socha	socha	k1gFnSc1	socha
svatého	svatý	k2eAgMnSc2d1	svatý
Pavla	Pavel	k1gMnSc2	Pavel
Socha	Socha	k1gMnSc1	Socha
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
Socha	Socha	k1gMnSc1	Socha
Ponocný	ponocný	k1gMnSc1	ponocný
(	(	kIx(	(
<g/>
Jelení	jelení	k2eAgInSc1d1	jelení
příkop	příkop	k1gInSc1	příkop
<g/>
)	)	kIx)	)
Socha	Socha	k1gMnSc1	Socha
mládí	mládí	k1gNnSc2	mládí
Socha	Socha	k1gMnSc1	Socha
Dobrý	dobrý	k2eAgMnSc1d1	dobrý
pastýř	pastýř	k1gMnSc1	pastýř
(	(	kIx(	(
<g/>
Rajská	rajský	k2eAgFnSc1d1	rajská
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
)	)	kIx)	)
Socha	Socha	k1gMnSc1	Socha
Vítěz	vítěz	k1gMnSc1	vítěz
(	(	kIx(	(
<g/>
u	u	k7c2	u
Letohrádku	letohrádek	k1gInSc2	letohrádek
královny	královna	k1gFnSc2	královna
Anny	Anna	k1gFnSc2	Anna
v	v	k7c6	v
Královské	královský	k2eAgFnSc6d1	královská
zahradě	zahrada	k1gFnSc6	zahrada
<g/>
)	)	kIx)	)
Socha	socha	k1gFnSc1	socha
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
Alegorie	alegorie	k1gFnSc2	alegorie
noci	noc	k1gFnSc2	noc
Sochy	socha	k1gFnSc2	socha
Antická	antický	k2eAgNnPc1d1	antické
božstva	božstvo	k1gNnPc1	božstvo
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Hartigovská	Hartigovský	k2eAgFnSc1d1	Hartigovský
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
)	)	kIx)	)
Lev	Lev	k1gMnSc1	Lev
s	s	k7c7	s
korunou	koruna	k1gFnSc7	koruna
a	a	k8xC	a
putti	putti	k1gNnSc7	putti
(	(	kIx(	(
<g/>
Hartigovská	Hartigovský	k2eAgFnSc1d1	Hartigovský
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
)	)	kIx)	)
Zpívající	zpívající	k2eAgFnSc1d1	zpívající
fontána	fontána	k1gFnSc1	fontána
(	(	kIx(	(
<g/>
u	u	k7c2	u
Letohrádku	letohrádek	k1gInSc2	letohrádek
královny	královna	k1gFnSc2	královna
Anny	Anna	k1gFnSc2	Anna
v	v	k7c6	v
Královské	královský	k2eAgFnSc6d1	královská
zahradě	zahrada	k1gFnSc6	zahrada
<g/>
)	)	kIx)	)
Kohlova	Kohlův	k2eAgFnSc1d1	Kohlova
kašna	kašna	k1gFnSc1	kašna
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
nádvoří	nádvoří	k1gNnSc6	nádvoří
<g/>
)	)	kIx)	)
Herkulova	Herkulův	k2eAgFnSc1d1	Herkulova
kašna	kašna	k1gFnSc1	kašna
(	(	kIx(	(
<g/>
zahrada	zahrada	k1gFnSc1	zahrada
Na	na	k7c6	na
valech	val	k1gInPc6	val
<g/>
)	)	kIx)	)
Studna	studna	k1gFnSc1	studna
s	s	k7c7	s
renesanční	renesanční	k2eAgFnSc7d1	renesanční
<g />
.	.	kIx.	.
</s>
<s>
mříží	mříží	k1gNnSc1	mříží
(	(	kIx(	(
<g/>
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
nádvoří	nádvoří	k1gNnSc6	nádvoří
<g/>
)	)	kIx)	)
Orlí	orlí	k2eAgFnSc1d1	orlí
kašna	kašna	k1gFnSc1	kašna
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
nádvoří	nádvoří	k1gNnSc6	nádvoří
<g/>
)	)	kIx)	)
Medvědí	medvědí	k2eAgFnSc1d1	medvědí
kašna	kašna	k1gFnSc1	kašna
Jelení	jelení	k2eAgFnSc1d1	jelení
příkop	příkop	k1gInSc1	příkop
-	-	kIx~	-
3,09	[number]	k4	3,09
ha	ha	kA	ha
(	(	kIx(	(
<g/>
horní	horní	k2eAgFnSc4d1	horní
část	část	k1gFnSc4	část
<g/>
)	)	kIx)	)
a	a	k8xC	a
5,06	[number]	k4	5,06
ha	ha	kA	ha
(	(	kIx(	(
<g/>
dolní	dolní	k2eAgFnSc1d1	dolní
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
zahrada	zahrada	k1gFnSc1	zahrada
Na	na	k7c6	na
Baště	bašta	k1gFnSc6	bašta
-	-	kIx~	-
27720	[number]	k4	27720
m	m	kA	m
<g/>
2	[number]	k4	2
Královská	královský	k2eAgFnSc1d1	královská
zahrada	zahrada	k1gFnSc1	zahrada
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
3,6	[number]	k4	3,6
ha	ha	kA	ha
Produkční	produkční	k2eAgFnSc2d1	produkční
zahrady	zahrada	k1gFnSc2	zahrada
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
(	(	kIx(	(
<g/>
Lumbeho	Lumbe	k1gMnSc2	Lumbe
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
)	)	kIx)	)
zahrada	zahrada	k1gFnSc1	zahrada
Na	na	k7c6	na
Valech	val	k1gInPc6	val
-	-	kIx~	-
1,43	[number]	k4	1,43
ha	ha	kA	ha
Hartigovská	Hartigovský	k2eAgFnSc1d1	Hartigovský
zahrada	zahrada	k1gFnSc1	zahrada
-	-	kIx~	-
757	[number]	k4	757
m	m	kA	m
<g/>
2	[number]	k4	2
Rajská	rajský	k2eAgFnSc1d1	rajská
zahrada	zahrada	k1gFnSc1	zahrada
-	-	kIx~	-
0,38	[number]	k4	0,38
ha	ha	kA	ha
Palácové	palácový	k2eAgFnSc2d1	palácová
zahrady	zahrada	k1gFnSc2	zahrada
pod	pod	k7c7	pod
Pražským	pražský	k2eAgInSc7d1	pražský
hradem	hrad	k1gInSc7	hrad
zahrada	zahrada	k1gFnSc1	zahrada
Na	na	k7c6	na
terase	teras	k1gInSc6	teras
Jízdárny	jízdárna	k1gFnSc2	jízdárna
-	-	kIx~	-
3452	[number]	k4	3452
m	m	kA	m
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
terasa	terasa	k1gFnSc1	terasa
<g/>
)	)	kIx)	)
a	a	k8xC	a
2241	[number]	k4	2241
m	m	kA	m
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
jízdárenský	jízdárenský	k2eAgInSc1d1	jízdárenský
dvůr	dvůr	k1gInSc1	dvůr
<g/>
)	)	kIx)	)
</s>
