<s>
Brekovská	Brekovský	k2eAgFnSc1d1
jeskyně	jeskyně	k1gFnSc1
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuPřírodní	infoboxuPřírodní	k2eAgFnSc3d1
památkaBrekovská	památkaBrekovský	k2eAgFnSc1d1
jaskyňaZákladní	jaskyňaZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Vyhlášení	vyhlášení	k1gNnSc1
</s>
<s>
2006	#num#	k4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
x	x	k?
ha	ha	kA
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
Slovensko	Slovensko	k1gNnSc1
Okres	okres	k1gInSc1
</s>
<s>
Humenné	Humenné	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
48	#num#	k4
<g/>
°	°	k?
<g/>
53	#num#	k4
<g/>
′	′	k?
<g/>
54	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
21	#num#	k4
<g/>
°	°	k?
<g/>
49	#num#	k4
<g/>
′	′	k?
<g/>
52	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Brekovská	Brekovský	k2eAgFnSc1d1
jeskyně	jeskyně	k1gFnSc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Kód	kód	k1gInSc1
</s>
<s>
1138	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Brekovská	Brekovský	k2eAgFnSc1d1
jeskyně	jeskyně	k1gFnSc1
je	být	k5eAaImIp3nS
nově	nově	k6eAd1
objevená	objevený	k2eAgFnSc1d1
jeskyně	jeskyně	k1gFnSc1
u	u	k7c2
Brekova	Brekov	k1gInSc2
na	na	k7c6
severovýchodním	severovýchodní	k2eAgNnSc6d1
Slovensku	Slovensko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
geologického	geologický	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
se	se	k3xPyFc4
v	v	k7c6
okolí	okolí	k1gNnSc6
obce	obec	k1gFnSc2
Brekov	Brekov	k1gInSc1
nacházejí	nacházet	k5eAaImIp3nP
několik	několik	k4yIc1
pozoruhodných	pozoruhodný	k2eAgInPc2d1
útvarů	útvar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povrch	povrch	k1gInSc1
tvoří	tvořit	k5eAaImIp3nP
čtvrtohorní	čtvrtohorní	k2eAgFnPc4d1
náplavy	náplava	k1gFnPc4
se	s	k7c7
sprašovými	sprašový	k2eAgFnPc7d1
a	a	k8xC
svahovými	svahový	k2eAgFnPc7d1
hlínami	hlína	k1gFnPc7
<g/>
,	,	kIx,
vápence	vápenec	k1gInPc1
<g/>
,	,	kIx,
dolomity	dolomit	k1gInPc1
a	a	k8xC
břidlice	břidlice	k1gFnPc1
s	s	k7c7
jeskyněmi	jeskyně	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
oblasti	oblast	k1gFnSc6
Reková	Reková	k1gFnSc1
<g/>
,	,	kIx,
Viničná	Viničný	k2eAgFnSc1d1
skala	skát	k5eAaImAgFnS
a	a	k8xC
Klokočiny	Klokočina	k1gFnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
chráněná	chráněný	k2eAgFnSc1d1
jeskynní	jeskynní	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
s	s	k7c7
Veľkou	Veľký	k2eAgFnSc7d1
artajamou	artajama	k1gFnSc7
a	a	k8xC
nově	nově	k6eAd1
objevená	objevený	k2eAgFnSc1d1
Brekovská	Brekovský	k2eAgFnSc1d1
jeskyně	jeskyně	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
starých	starý	k2eAgInPc6d1
záznamech	záznam	k1gInPc6
se	se	k3xPyFc4
říká	říkat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
velký	velký	k2eAgInSc4d1
jeskynní	jeskynní	k2eAgInSc4d1
komplex	komplex	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
táhne	táhnout	k5eAaImIp3nS
pravděpodobně	pravděpodobně	k6eAd1
i	i	k9
pod	pod	k7c7
korytem	koryto	k1gNnSc7
řeky	řeka	k1gFnSc2
Laborec	Laborec	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
zajímavé	zajímavý	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
pečeť	pečeť	k1gFnSc1
Brekovských	Brekovský	k2eAgMnPc2d1
starostů	starosta	k1gMnPc2
za	za	k7c2
starých	starý	k2eAgInPc2d1
časů	čas	k1gInPc2
obsahovala	obsahovat	k5eAaImAgFnS
obraz	obraz	k1gInSc4
sovy	sova	k1gFnSc2
a	a	k8xC
jeskyně	jeskyně	k1gFnSc1
se	se	k3xPyFc4
nazývala	nazývat	k5eAaImAgFnS
"	"	kIx"
<g/>
sovia	sovia	k1gFnSc1
jaskyňa	jaskyňa	k1gMnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeskyně	jeskyně	k1gFnSc1
není	být	k5eNaImIp3nS
v	v	k7c6
současnosti	současnost	k1gFnSc6
přístupná	přístupný	k2eAgFnSc1d1
pro	pro	k7c4
veřejnost	veřejnost	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
přijít	přijít	k5eAaPmF
až	až	k9
k	k	k7c3
velmi	velmi	k6eAd1
hluboké	hluboký	k2eAgFnSc6d1
jámě	jáma	k1gFnSc6
(	(	kIx(
<g/>
jeskyni	jeskyně	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Přístup	přístup	k1gInSc1
k	k	k7c3
jeskyni	jeskyně	k1gFnSc3
je	být	k5eAaImIp3nS
od	od	k7c2
kamenolomu	kamenolom	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc4
otvor	otvor	k1gInSc4
do	do	k7c2
jeskynních	jeskynní	k2eAgFnPc2d1
prostor	prostora	k1gFnPc2
vznikl	vzniknout	k5eAaPmAgInS
následkem	následkem	k7c2
proražení	proražení	k1gNnSc2
těžby	těžba	k1gFnSc2
z	z	k7c2
kamenolomu	kamenolom	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vchod	vchod	k1gInSc1
je	být	k5eAaImIp3nS
z	z	k7c2
této	tento	k3xDgFnSc2
strany	strana	k1gFnSc2
opatřen	opatřit	k5eAaPmNgInS
mřížemi	mříž	k1gFnPc7
kvůli	kvůli	k7c3
vandalům	vandal	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1
ničí	ničit	k5eAaImIp3nP
výzdobu	výzdoba	k1gFnSc4
jeskynních	jeskynní	k2eAgFnPc2d1
prostor	prostora	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asi	asi	k9
50	#num#	k4
m	m	kA
směrem	směr	k1gInSc7
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yRgInSc6,k3yQgInSc6
pokračuje	pokračovat	k5eAaImIp3nS
jeskyně	jeskyně	k1gFnSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
i	i	k9
Veľká	Veľký	k2eAgFnSc1d1
artajama	artajama	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díra-artajama	Díra-artajama	k1gFnSc1
je	být	k5eAaImIp3nS
hluboká	hluboký	k2eAgFnSc1d1
vertikálně	vertikálně	k6eAd1
přibližně	přibližně	k6eAd1
15	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
i	i	k9
vchod	vchod	k1gInSc1
do	do	k7c2
jeskyně	jeskyně	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
znám	znám	k2eAgInSc1d1
už	už	k6eAd1
staletí	staletí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokračuje	pokračovat	k5eAaImIp3nS
po	po	k7c6
malé	malý	k2eAgFnSc6d1
plošince	plošinka	k1gFnSc6
hlouběji	hluboko	k6eAd2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
rozvětvuje	rozvětvovat	k5eAaImIp3nS
přes	přes	k7c4
malé	malý	k2eAgInPc4d1
průchody	průchod	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeskynní	jeskynní	k2eAgFnSc1d1
výzdoba	výzdoba	k1gFnSc1
je	být	k5eAaImIp3nS
na	na	k7c6
všech	všecek	k3xTgNnPc6
dostupných	dostupný	k2eAgNnPc6d1
místech	místo	k1gNnPc6
poškozena	poškodit	k5eAaPmNgFnS
vandaly	vandal	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vchod	vchod	k1gInSc1
do	do	k7c2
nitra	nitro	k1gNnSc2
jeskyně	jeskyně	k1gFnSc2
je	být	k5eAaImIp3nS
nebezpečný	bezpečný	k2eNgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Brekovská	Brekovský	k2eAgFnSc1d1
jaskyňa	jaskyňa	k1gFnSc1
je	být	k5eAaImIp3nS
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
ve	v	k7c6
správě	správa	k1gFnSc6
příspěvkové	příspěvkový	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
Správa	správa	k1gFnSc1
slovenských	slovenský	k2eAgFnPc2d1
jeskyní	jeskyně	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
okrese	okres	k1gInSc6
Humenné	Humenné	k1gNnSc4
v	v	k7c6
Prešovském	prešovský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Území	území	k1gNnSc1
bylo	být	k5eAaImAgNnS
vyhlášeno	vyhlásit	k5eAaPmNgNnS
či	či	k8xC
novelizováno	novelizovat	k5eAaBmNgNnS
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
na	na	k7c6
rozloze	rozloha	k1gFnSc6
x	x	k?
ha	ha	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozloha	rozloha	k1gFnSc1
ochranného	ochranný	k2eAgNnSc2d1
pásma	pásmo	k1gNnSc2
byla	být	k5eAaImAgFnS
stanovena	stanovit	k5eAaPmNgFnS
na	na	k7c4
15,264	15,264	k4
<g/>
8	#num#	k4
ha	ha	kA
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Brekovská	Brekovský	k2eAgFnSc1d1
jaskyňa	jaskyňa	k1gFnSc1
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
Brekovská	Brekovský	k2eAgFnSc1d1
jaskyňa	jaskyňa	k1gFnSc1
<g/>
,	,	kIx,
Štátny	Štátno	k1gNnPc7
zoznam	zoznam	k6eAd1
osobitne	osobitnout	k5eAaPmIp3nS
chránených	chránená	k1gFnPc2
častí	častit	k5eAaImIp3nP
prírody	prírod	k1gInPc1
SR	SR	kA
</s>
<s>
Chránené	Chránená	k1gFnPc1
územia	územia	k1gFnSc1
<g/>
,	,	kIx,
Štátna	Štáten	k2eAgFnSc1d1
ochrana	ochrana	k1gFnSc1
prírody	príroda	k1gFnSc2
Slovenskej	Slovenskej	k?
republiky	republika	k1gFnSc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
