<s>
Brekovská	Brekovský	k2eAgFnSc1d1
jeskyně	jeskyně	k1gFnSc1
je	být	k5eAaImIp3nS
nově	nově	k6eAd1
objevená	objevený	k2eAgFnSc1d1
jeskyně	jeskyně	k1gFnSc1
u	u	k7c2
Brekova	Brekov	k1gInSc2
na	na	k7c6
severovýchodním	severovýchodní	k2eAgNnSc6d1
Slovensku	Slovensko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Přístup	přístup	k1gInSc1
k	k	k7c3
jeskyni	jeskyně	k1gFnSc3
je	být	k5eAaImIp3nS
od	od	k7c2
kamenolomu	kamenolom	k1gInSc2
<g/>
.	.	kIx.
</s>