<s>
Přibyslav	Přibyslav	k1gFnSc1	Přibyslav
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Primislau	Primislaus	k1gInSc3	Primislaus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
v	v	k7c6	v
Kraji	kraj	k1gInSc6	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Sázavě	Sázava	k1gFnSc6	Sázava
<g/>
,	,	kIx,	,
23	[number]	k4	23
km	km	kA	km
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
Jihlavy	Jihlava	k1gFnSc2	Jihlava
a	a	k8xC	a
12	[number]	k4	12
km	km	kA	km
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
Havlíčkova	Havlíčkův	k2eAgInSc2d1	Havlíčkův
Brodu	Brod	k1gInSc2	Brod
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
asi	asi	k9	asi
4	[number]	k4	4
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
několik	několik	k4yIc1	několik
částí	část	k1gFnPc2	část
města	město	k1gNnSc2	město
-	-	kIx~	-
Českou	český	k2eAgFnSc4d1	Česká
Jablonnou	Jablonná	k1gFnSc4	Jablonná
<g/>
,	,	kIx,	,
Dobrou	dobrý	k2eAgFnSc4d1	dobrá
<g/>
,	,	kIx,	,
Dolní	dolní	k2eAgFnSc4d1	dolní
Jablonnou	Jablonná	k1gFnSc4	Jablonná
<g/>
,	,	kIx,	,
Hřiště	hřiště	k1gNnSc1	hřiště
<g/>
,	,	kIx,	,
Poříčí	Poříčí	k1gNnSc1	Poříčí
<g/>
,	,	kIx,	,
Přibyslav	Přibyslav	k1gFnSc1	Přibyslav
<g/>
,	,	kIx,	,
Ronov	Ronov	k1gInSc1	Ronov
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
a	a	k8xC	a
Utín	Utína	k1gFnPc2	Utína
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
Svazku	svazek	k1gInSc2	svazek
obcí	obec	k1gFnPc2	obec
Přibyslavska	Přibyslavsko	k1gNnSc2	Přibyslavsko
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
od	od	k7c2	od
údajného	údajný	k2eAgMnSc2d1	údajný
zakladatele	zakladatel	k1gMnSc2	zakladatel
Přibyslava	Přibyslava	k1gFnSc1	Přibyslava
z	z	k7c2	z
Polné	Polná	k1gFnSc2	Polná
<g/>
.	.	kIx.	.
</s>
<s>
Přibyslavská	přibyslavský	k2eAgFnSc1d1	Přibyslavská
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
město	město	k1gNnSc4	město
leží	ležet	k5eAaImIp3nP	ležet
<g/>
,	,	kIx,	,
nebyla	být	k5eNaImAgNnP	být
nejprve	nejprve	k6eAd1	nejprve
příliš	příliš	k6eAd1	příliš
osídlena	osídlen	k2eAgFnSc1d1	osídlena
a	a	k8xC	a
písemné	písemný	k2eAgFnPc1d1	písemná
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
Přibyslavi	Přibyslav	k1gFnSc6	Přibyslav
pochází	pocházet	k5eAaImIp3nS	pocházet
přibližně	přibližně	k6eAd1	přibližně
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Přibyslav	Přibyslav	k1gFnSc1	Přibyslav
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
Jihlavě	Jihlava	k1gFnSc6	Jihlava
druhým	druhý	k4xOgNnSc7	druhý
nejdůležitějším	důležitý	k2eAgNnSc7d3	nejdůležitější
nalezištěm	naleziště	k1gNnSc7	naleziště
stříbra	stříbro	k1gNnSc2	stříbro
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
těžba	těžba	k1gFnSc1	těžba
však	však	k9	však
byla	být	k5eAaImAgFnS	být
poměrně	poměrně	k6eAd1	poměrně
brzy	brzy	k6eAd1	brzy
ukončena	ukončit	k5eAaPmNgFnS	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1424	[number]	k4	1424
obklíčili	obklíčit	k5eAaPmAgMnP	obklíčit
město	město	k1gNnSc4	město
husité	husita	k1gMnPc1	husita
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Žižkou	Žižka	k1gMnSc7	Žižka
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
však	však	k9	však
poblíž	poblíž	k6eAd1	poblíž
nedaleké	daleký	k2eNgFnSc2d1	nedaleká
vsi	ves	k1gFnSc2	ves
dnes	dnes	k6eAd1	dnes
zvané	zvaný	k2eAgNnSc4d1	zvané
Žižkovo	Žižkův	k2eAgNnSc4d1	Žižkovo
Pole	pole	k1gNnSc4	pole
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Husité	husita	k1gMnPc1	husita
přesto	přesto	k8xC	přesto
město	město	k1gNnSc4	město
dobyli	dobýt	k5eAaPmAgMnP	dobýt
a	a	k8xC	a
vypálili	vypálit	k5eAaPmAgMnP	vypálit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pobělohorské	pobělohorský	k2eAgFnSc6d1	pobělohorská
době	doba	k1gFnSc6	doba
město	město	k1gNnSc1	město
spravovala	spravovat	k5eAaImAgNnP	spravovat
knížata	kníže	k1gNnPc1	kníže
z	z	k7c2	z
Ditrichštejnu	Ditrichštejn	k1gInSc2	Ditrichštejn
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
postihla	postihnout	k5eAaPmAgFnS	postihnout
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
řada	řada	k1gFnSc1	řada
katastrof	katastrofa	k1gFnPc2	katastrofa
a	a	k8xC	a
několikrát	několikrát	k6eAd1	několikrát
vyhořelo	vyhořet	k5eAaPmAgNnS	vyhořet
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
klidné	klidný	k2eAgNnSc1d1	klidné
městečko	městečko	k1gNnSc1	městečko
s	s	k7c7	s
tradicí	tradice	k1gFnSc7	tradice
lehkého	lehký	k2eAgInSc2d1	lehký
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
především	především	k9	především
potravinářského	potravinářský	k2eAgInSc2d1	potravinářský
<g/>
,	,	kIx,	,
textilního	textilní	k2eAgInSc2d1	textilní
a	a	k8xC	a
dřevozpracujícího	dřevozpracující	k2eAgInSc2d1	dřevozpracující
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
podnikem	podnik	k1gInSc7	podnik
je	být	k5eAaImIp3nS	být
Pribina	Pribina	k1gFnSc1	Pribina
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
TPK	TPK	kA	TPK
Přibyslav	Přibyslav	k1gFnSc1	Přibyslav
<g/>
)	)	kIx)	)
vyrábějící	vyrábějící	k2eAgInPc1d1	vyrábějící
mléčné	mléčný	k2eAgInPc1d1	mléčný
produkty	produkt	k1gInPc1	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
je	být	k5eAaImIp3nS	být
Přibyslav	Přibyslav	k1gFnSc1	Přibyslav
přístupná	přístupný	k2eAgFnSc1d1	přístupná
po	po	k7c6	po
železnici	železnice	k1gFnSc6	železnice
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
i	i	k9	i
veřejné	veřejný	k2eAgNnSc4d1	veřejné
letiště	letiště	k1gNnSc4	letiště
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zajímavým	zajímavý	k2eAgFnPc3d1	zajímavá
pamětihodnostem	pamětihodnost	k1gFnPc3	pamětihodnost
patří	patřit	k5eAaImIp3nS	patřit
barokní	barokní	k2eAgFnSc1d1	barokní
kostel	kostel	k1gInSc1	kostel
Narození	narození	k1gNnSc4	narození
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
s	s	k7c7	s
věží	věž	k1gFnSc7	věž
<g/>
,	,	kIx,	,
Starý	starý	k2eAgInSc1d1	starý
špitál	špitál	k1gInSc1	špitál
či	či	k8xC	či
přibyslavský	přibyslavský	k2eAgInSc1d1	přibyslavský
zámek	zámek	k1gInSc1	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Přibyslav	Přibyslav	k1gFnSc1	Přibyslav
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
od	od	k7c2	od
historických	historický	k2eAgFnPc2d1	historická
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
žily	žít	k5eAaImAgFnP	žít
v	v	k7c6	v
době	doba	k1gFnSc6	doba
založení	založení	k1gNnSc2	založení
sídla	sídlo	k1gNnSc2	sídlo
či	či	k8xC	či
se	se	k3xPyFc4	se
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
založení	založení	k1gNnSc6	založení
přímo	přímo	k6eAd1	přímo
podílely	podílet	k5eAaImAgInP	podílet
<g/>
.	.	kIx.	.
</s>
<s>
Dlouho	dlouho	k6eAd1	dlouho
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
původce	původce	k1gMnSc4	původce
názvu	název	k1gInSc2	název
považován	považován	k2eAgMnSc1d1	považován
údajný	údajný	k2eAgMnSc1d1	údajný
zakladatel	zakladatel	k1gMnSc1	zakladatel
<g/>
,	,	kIx,	,
moravský	moravský	k2eAgMnSc1d1	moravský
velmož	velmož	k1gMnSc1	velmož
Přibyslav	Přibyslava	k1gFnPc2	Přibyslava
z	z	k7c2	z
Křižanova	Křižanův	k2eAgMnSc2d1	Křižanův
(	(	kIx(	(
<g/>
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
se	se	k3xPyFc4	se
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
například	například	k6eAd1	například
Antonín	Antonín	k1gMnSc1	Antonín
Profous	Profous	k1gMnSc1	Profous
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
Místní	místní	k2eAgNnPc1d1	místní
jména	jméno	k1gNnPc1	jméno
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Novější	nový	k2eAgInPc1d2	novější
prameny	pramen	k1gInPc1	pramen
odvozují	odvozovat	k5eAaImIp3nP	odvozovat
název	název	k1gInSc4	název
spíše	spíše	k9	spíše
od	od	k7c2	od
osoby	osoba	k1gFnSc2	osoba
Přibyslava	Přibyslava	k1gFnSc1	Přibyslava
z	z	k7c2	z
Polné	Polná	k1gFnSc2	Polná
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
svou	svůj	k3xOyFgFnSc7	svůj
kolonizační	kolonizační	k2eAgFnSc7d1	kolonizační
činností	činnost	k1gFnSc7	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenování	pojmenování	k1gNnSc1	pojmenování
města	město	k1gNnSc2	město
mělo	mít	k5eAaImAgNnS	mít
tyto	tento	k3xDgFnPc4	tento
formy	forma	k1gFnPc4	forma
<g/>
:	:	kIx,	:
Priemezlaves	Priemezlaves	k1gMnSc1	Priemezlaves
(	(	kIx(	(
<g/>
1257	[number]	k4	1257
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Primizlaus	Primizlaus	k1gMnSc1	Primizlaus
(	(	kIx(	(
<g/>
1265	[number]	k4	1265
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Primislaus	Primislaus	k1gMnSc1	Primislaus
(	(	kIx(	(
<g/>
1272	[number]	k4	1272
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Przemislabe	Przemislab	k1gMnSc5	Przemislab
(	(	kIx(	(
<g/>
1283	[number]	k4	1283
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Przibislaws	Przibislaws	k1gInSc1	Przibislaws
(	(	kIx(	(
<g/>
1307	[number]	k4	1307
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Primizlaus	Primizlaus	k1gMnSc1	Primizlaus
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1314	[number]	k4	1314
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Przymzlaus	Przymzlaus	k1gMnSc1	Przymzlaus
(	(	kIx(	(
<g/>
1316	[number]	k4	1316
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Primislauia	Primislauia	k1gFnSc1	Primislauia
(	(	kIx(	(
<g/>
1329	[number]	k4	1329
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Przibislaw	Przibislaw	k1gMnSc1	Przibislaw
(	(	kIx(	(
<g/>
1405	[number]	k4	1405
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Przibislauia	Przibislauia	k1gFnSc1	Przibislauia
(	(	kIx(	(
<g/>
1412	[number]	k4	1412
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Przibislaw	Przibislaw	k1gMnSc1	Przibislaw
(	(	kIx(	(
<g/>
1542	[number]	k4	1542
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pržibyslav	Pržibyslav	k1gMnSc1	Pržibyslav
(	(	kIx(	(
<g/>
1675	[number]	k4	1675
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pržibislaw	Pržibislaw	k1gMnSc1	Pržibislaw
(	(	kIx(	(
<g/>
1720	[number]	k4	1720
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pržibislau	Pržibislaa	k1gFnSc4	Pržibislaa
(	(	kIx(	(
<g/>
1838	[number]	k4	1838
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Přimislau	Přimislaa	k1gFnSc4	Přimislaa
(	(	kIx(	(
<g/>
Přibyslav	Přibyslav	k1gFnSc4	Přibyslav
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
)	)	kIx)	)
a	a	k8xC	a
současné	současný	k2eAgFnPc1d1	současná
Přibyslav	Přibyslav	k1gFnSc1	Přibyslav
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
jméno	jméno	k1gNnSc1	jméno
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
přivlastňovací	přivlastňovací	k2eAgFnSc7d1	přivlastňovací
příponou	přípona	k1gFnSc7	přípona
-jb	b	k?	-jb
z	z	k7c2	z
osobního	osobní	k2eAgNnSc2d1	osobní
jména	jméno	k1gNnSc2	jméno
Přibyslav	Přibyslava	k1gFnPc2	Přibyslava
a	a	k8xC	a
znamená	znamenat	k5eAaImIp3nS	znamenat
tak	tak	k6eAd1	tak
například	například	k6eAd1	například
Přibyslavův	Přibyslavův	k2eAgInSc1d1	Přibyslavův
dvůr	dvůr	k1gInSc1	dvůr
či	či	k8xC	či
Přibyslavův	Přibyslavův	k2eAgInSc1d1	Přibyslavův
hrad	hrad	k1gInSc1	hrad
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
Českomoravské	českomoravský	k2eAgFnSc2d1	Českomoravská
vrchoviny	vrchovina	k1gFnSc2	vrchovina
v	v	k7c6	v
klimaticky	klimaticky	k6eAd1	klimaticky
a	a	k8xC	a
geograficky	geograficky	k6eAd1	geograficky
poměrně	poměrně	k6eAd1	poměrně
nehostinné	hostinný	k2eNgFnSc6d1	nehostinná
krajině	krajina	k1gFnSc6	krajina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
proto	proto	k8xC	proto
v	v	k7c6	v
pravěkých	pravěký	k2eAgFnPc6d1	pravěká
dobách	doba	k1gFnPc6	doba
nebyla	být	k5eNaImAgFnS	být
příliš	příliš	k6eAd1	příliš
osídlena	osídlen	k2eAgFnSc1d1	osídlena
<g/>
.	.	kIx.	.
</s>
<s>
Archeologické	archeologický	k2eAgInPc1d1	archeologický
nálezy	nález	k1gInPc1	nález
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
jsou	být	k5eAaImIp3nP	být
proto	proto	k8xC	proto
na	na	k7c6	na
Přibyslavsku	Přibyslavsko	k1gNnSc6	Přibyslavsko
vzácné	vzácný	k2eAgInPc1d1	vzácný
a	a	k8xC	a
vypovídají	vypovídat	k5eAaPmIp3nP	vypovídat
o	o	k7c4	o
neexistenci	neexistence	k1gFnSc4	neexistence
sídliště	sídliště	k1gNnSc2	sídliště
trvalého	trvalý	k2eAgInSc2d1	trvalý
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
ve	v	k7c6	v
vrcholném	vrcholný	k2eAgInSc6d1	vrcholný
středověku	středověk	k1gInSc6	středověk
byla	být	k5eAaImAgFnS	být
většina	většina	k1gFnSc1	většina
území	území	k1gNnSc2	území
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
hlubokým	hluboký	k2eAgInSc7d1	hluboký
lesem	les	k1gInSc7	les
přerušeným	přerušený	k2eAgInSc7d1	přerušený
jen	jen	k8xS	jen
několika	několik	k4yIc7	několik
obchodními	obchodní	k2eAgFnPc7d1	obchodní
stezkami	stezka	k1gFnPc7	stezka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
písemné	písemný	k2eAgFnPc4d1	písemná
zmínky	zmínka	k1gFnPc4	zmínka
pochází	pocházet	k5eAaImIp3nS	pocházet
přibližně	přibližně	k6eAd1	přibližně
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
starší	starý	k2eAgInPc1d2	starší
spisy	spis	k1gInPc1	spis
sice	sice	k8xC	sice
místo	místo	k1gNnSc1	místo
podobného	podobný	k2eAgInSc2d1	podobný
názvu	název	k1gInSc2	název
zmiňují	zmiňovat	k5eAaImIp3nP	zmiňovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zřejmě	zřejmě	k6eAd1	zřejmě
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
spíše	spíše	k9	spíše
o	o	k7c4	o
moravskou	moravský	k2eAgFnSc4d1	Moravská
obec	obec	k1gFnSc4	obec
Přibyslavice	Přibyslavice	k1gFnSc2	Přibyslavice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1235	[number]	k4	1235
se	se	k3xPyFc4	se
v	v	k7c6	v
listině	listina	k1gFnSc6	listina
královny	královna	k1gFnSc2	královna
Konstancie	Konstancie	k1gFnSc2	Konstancie
objevuje	objevovat	k5eAaImIp3nS	objevovat
výraz	výraz	k1gInSc1	výraz
"	"	kIx"	"
<g/>
Pribyzlaun	Pribyzlaun	k1gInSc1	Pribyzlaun
<g/>
"	"	kIx"	"
a	a	k8xC	a
jako	jako	k8xC	jako
svědek	svědek	k1gMnSc1	svědek
je	být	k5eAaImIp3nS	být
podepsán	podepsat	k5eAaPmNgMnS	podepsat
přibyslavský	přibyslavský	k2eAgMnSc1d1	přibyslavský
rychtář	rychtář	k1gMnSc1	rychtář
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
soudí	soudit	k5eAaImIp3nS	soudit
o	o	k7c6	o
dobové	dobový	k2eAgFnSc6d1	dobová
existenci	existence	k1gFnSc6	existence
obecního	obecní	k2eAgNnSc2d1	obecní
uspořádání	uspořádání	k1gNnSc2	uspořádání
v	v	k7c6	v
Přibyslavi	Přibyslav	k1gFnSc6	Přibyslav
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
se	se	k3xPyFc4	se
však	však	k9	však
např.	např.	kA	např.
podle	podle	k7c2	podle
ČSÚ	ČSÚ	kA	ČSÚ
datuje	datovat	k5eAaImIp3nS	datovat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1257	[number]	k4	1257
a	a	k8xC	a
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
se	se	k3xPyFc4	se
k	k	k7c3	k
listině	listina	k1gFnSc3	listina
Smila	Smil	k1gMnSc2	Smil
z	z	k7c2	z
Lichtenburka	Lichtenburek	k1gMnSc2	Lichtenburek
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
daruje	darovat	k5eAaPmIp3nS	darovat
desátky	desátek	k1gInPc4	desátek
ze	z	k7c2	z
stříbrných	stříbrný	k2eAgInPc2d1	stříbrný
dolů	dol	k1gInPc2	dol
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Přibyslavi	Přibyslav	k1gFnSc2	Přibyslav
třem	tři	k4xCgInPc3	tři
cisterciáckým	cisterciácký	k2eAgInPc3d1	cisterciácký
klášterům	klášter	k1gInPc3	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
těžba	těžba	k1gFnSc1	těžba
souvisela	souviset	k5eAaImAgFnS	souviset
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
hornictví	hornictví	k1gNnSc2	hornictví
na	na	k7c6	na
Jihlavsku	Jihlavsko	k1gNnSc6	Jihlavsko
<g/>
.	.	kIx.	.
</s>
<s>
Falešné	falešný	k2eAgInPc1d1	falešný
rukopisy	rukopis	k1gInPc1	rukopis
se	se	k3xPyFc4	se
však	však	k9	však
snažily	snažit	k5eAaImAgFnP	snažit
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
klást	klást	k5eAaImF	klást
založení	založení	k1gNnSc3	založení
města	město	k1gNnSc2	město
do	do	k7c2	do
vzdálenější	vzdálený	k2eAgFnSc2d2	vzdálenější
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1889	[number]	k4	1889
byl	být	k5eAaImAgInS	být
zveřejněn	zveřejnit	k5eAaPmNgInS	zveřejnit
nález	nález	k1gInSc1	nález
tzv.	tzv.	kA	tzv.
Paměti	paměť	k1gFnSc2	paměť
přibyslavské	přibyslavský	k2eAgFnPc1d1	Přibyslavská
<g/>
,	,	kIx,	,
padělaného	padělaný	k2eAgInSc2d1	padělaný
dokumentu	dokument	k1gInSc2	dokument
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
založení	založení	k1gNnSc3	založení
města	město	k1gNnSc2	město
sestrou	sestra	k1gFnSc7	sestra
svatého	svatý	k2eAgMnSc4d1	svatý
Václava	Václav	k1gMnSc4	Václav
Přibyslavou	Přibyslava	k1gFnSc7	Přibyslava
klade	klást	k5eAaImIp3nS	klást
do	do	k7c2	do
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Nepravost	nepravost	k1gFnSc4	nepravost
rukopisu	rukopis	k1gInSc2	rukopis
prokázal	prokázat	k5eAaPmAgInS	prokázat
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
Gustav	Gustav	k1gMnSc1	Gustav
Friedrich	Friedrich	k1gMnSc1	Friedrich
<g/>
,	,	kIx,	,
dokument	dokument	k1gInSc1	dokument
byl	být	k5eAaImAgInS	být
však	však	k9	však
podrobován	podrobován	k2eAgInSc1d1	podrobován
rozsáhlému	rozsáhlý	k2eAgNnSc3d1	rozsáhlé
jazykovému	jazykový	k2eAgNnSc3d1	jazykové
i	i	k8xC	i
historickému	historický	k2eAgNnSc3d1	historické
zkoumání	zkoumání	k1gNnSc3	zkoumání
i	i	k8xC	i
v	v	k7c6	v
mnohem	mnohem	k6eAd1	mnohem
pozdější	pozdní	k2eAgFnSc6d2	pozdější
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Přibyslav	Přibyslav	k1gFnSc1	Přibyslav
a	a	k8xC	a
přilehlý	přilehlý	k2eAgInSc1d1	přilehlý
region	region	k1gInSc1	region
se	se	k3xPyFc4	se
již	již	k6eAd1	již
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
stala	stát	k5eAaPmAgFnS	stát
po	po	k7c6	po
Jihlavě	Jihlava	k1gFnSc6	Jihlava
druhým	druhý	k4xOgNnSc7	druhý
nejdůležitějším	důležitý	k2eAgNnSc7d3	nejdůležitější
nalezištěm	naleziště	k1gNnSc7	naleziště
stříbra	stříbro	k1gNnSc2	stříbro
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
však	však	k9	však
zřejmě	zřejmě	k6eAd1	zřejmě
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
osadu	osada	k1gFnSc4	osada
kolem	kolem	k7c2	kolem
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
přibyslavského	přibyslavský	k2eAgInSc2d1	přibyslavský
hradu	hrad	k1gInSc2	hrad
a	a	k8xC	a
sídlo	sídlo	k1gNnSc4	sídlo
městského	městský	k2eAgInSc2d1	městský
typu	typ	k1gInSc2	typ
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
zřejmě	zřejmě	k6eAd1	zřejmě
až	až	k9	až
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
třetině	třetina	k1gFnSc6	třetina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Smila	Smil	k1gMnSc2	Smil
z	z	k7c2	z
Lichtenburka	Lichtenburek	k1gMnSc2	Lichtenburek
získal	získat	k5eAaPmAgMnS	získat
Přibyslav	Přibyslav	k1gFnSc4	Přibyslav
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Častolov	Častolov	k1gInSc1	Častolov
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Jindřich	Jindřich	k1gMnSc1	Jindřich
z	z	k7c2	z
Přibyslavi	Přibyslav	k1gFnSc2	Přibyslav
a	a	k8xC	a
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
13	[number]	k4	13
<g/>
.	.	kIx.	.
a	a	k8xC	a
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Jindřichův	Jindřichův	k2eAgMnSc1d1	Jindřichův
syn	syn	k1gMnSc1	syn
Hynek	Hynek	k1gMnSc1	Hynek
z	z	k7c2	z
Přibyslavi	Přibyslav	k1gFnSc2	Přibyslav
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
hornické	hornický	k2eAgNnSc1d1	Hornické
město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
však	však	k9	však
mění	měnit	k5eAaImIp3nS	měnit
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
byla	být	k5eAaImAgNnP	být
naleziště	naleziště	k1gNnPc1	naleziště
postupně	postupně	k6eAd1	postupně
vytěžena	vytěžen	k2eAgNnPc1d1	vytěženo
a	a	k8xC	a
horníci	horník	k1gMnPc1	horník
odchází	odcházet	k5eAaImIp3nP	odcházet
na	na	k7c4	na
Kutnohorsko	Kutnohorsko	k1gNnSc4	Kutnohorsko
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
se	se	k3xPyFc4	se
přeorientovávají	přeorientovávat	k5eAaImIp3nP	přeorientovávat
na	na	k7c4	na
zemědělství	zemědělství	k1gNnSc4	zemědělství
a	a	k8xC	a
řemesla	řemeslo	k1gNnPc4	řemeslo
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
vlády	vláda	k1gFnSc2	vláda
na	na	k7c6	na
panství	panství	k1gNnSc6	panství
ujal	ujmout	k5eAaPmAgMnS	ujmout
Čeněk	Čeněk	k1gMnSc1	Čeněk
z	z	k7c2	z
Ronova	Ronův	k2eAgInSc2d1	Ronův
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
později	pozdě	k6eAd2	pozdě
proslul	proslout	k5eAaPmAgInS	proslout
loupežnými	loupežný	k2eAgInPc7d1	loupežný
přepadeními	přepadení	k1gNnPc7	přepadení
sousedních	sousední	k2eAgFnPc2d1	sousední
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
října	říjen	k1gInSc2	říjen
1424	[number]	k4	1424
přitáhli	přitáhnout	k5eAaPmAgMnP	přitáhnout
k	k	k7c3	k
městu	město	k1gNnSc3	město
husité	husita	k1gMnPc1	husita
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Žižkou	Žižka	k1gMnSc7	Žižka
<g/>
.	.	kIx.	.
</s>
<s>
Utábořil	utábořit	k5eAaPmAgMnS	utábořit
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
Šenfeldem	Šenfeld	k1gInSc7	Šenfeld
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgNnSc1d1	dnešní
Žižkovo	Žižkův	k2eAgNnSc1d1	Žižkovo
Pole	pole	k1gNnSc1	pole
<g/>
)	)	kIx)	)
a	a	k8xC	a
Hřišti	hřiště	k1gNnSc3	hřiště
a	a	k8xC	a
město	město	k1gNnSc4	město
obléhal	obléhat	k5eAaImAgMnS	obléhat
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Žižka	Žižka	k1gMnSc1	Žižka
však	však	k9	však
u	u	k7c2	u
vesnice	vesnice	k1gFnSc2	vesnice
Šenfeld	Šenfeld	k1gInSc1	Šenfeld
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
bojovníci	bojovník	k1gMnPc1	bojovník
pak	pak	k9	pak
město	město	k1gNnSc4	město
dobyli	dobýt	k5eAaPmAgMnP	dobýt
<g/>
,	,	kIx,	,
hrad	hrad	k1gInSc4	hrad
vypálili	vypálit	k5eAaPmAgMnP	vypálit
a	a	k8xC	a
několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
obránců	obránce	k1gMnPc2	obránce
popravili	popravit	k5eAaPmAgMnP	popravit
upálením	upálení	k1gNnSc7	upálení
<g/>
.	.	kIx.	.
</s>
<s>
Přibyslavský	přibyslavský	k2eAgInSc1d1	přibyslavský
hrad	hrad	k1gInSc1	hrad
pak	pak	k6eAd1	pak
husité	husita	k1gMnPc1	husita
(	(	kIx(	(
<g/>
kteří	který	k3yQgMnPc1	který
si	se	k3xPyFc3	se
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc4	svůj
vůdce	vůdce	k1gMnSc4	vůdce
říkali	říkat	k5eAaImAgMnP	říkat
Sirotci	Sirotek	k1gMnPc1	Sirotek
<g/>
)	)	kIx)	)
používali	používat	k5eAaImAgMnP	používat
jako	jako	k9	jako
opevněnou	opevněný	k2eAgFnSc4d1	opevněná
základnu	základna	k1gFnSc4	základna
pro	pro	k7c4	pro
výpady	výpad	k1gInPc4	výpad
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Moc	moc	k6eAd1	moc
nad	nad	k7c7	nad
městem	město	k1gNnSc7	město
ztratili	ztratit	k5eAaPmAgMnP	ztratit
až	až	k9	až
s	s	k7c7	s
koncem	konec	k1gInSc7	konec
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Lipan	lipan	k1gMnSc1	lipan
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
byl	být	k5eAaImAgInS	být
boji	boj	k1gInSc3	boj
tak	tak	k6eAd1	tak
poničen	poničit	k5eAaPmNgMnS	poničit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
desetiletích	desetiletí	k1gNnPc6	desetiletí
postupně	postupně	k6eAd1	postupně
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Panství	panství	k1gNnSc1	panství
bylo	být	k5eAaImAgNnS	být
Ronovci	Ronovec	k1gMnSc3	Ronovec
přepuštěno	přepuštěn	k2eAgNnSc1d1	přepuštěn
jejich	jejich	k3xOp3gNnPc6	jejich
příbuznému	příbuzný	k1gMnSc3	příbuzný
Hynku	Hynek	k1gMnSc3	Hynek
Ptáčkovi	Ptáček	k1gMnSc3	Ptáček
z	z	k7c2	z
Pirkštejna	Pirkštejn	k1gInSc2	Pirkštejn
z	z	k7c2	z
Polné	Polná	k1gFnSc2	Polná
<g/>
,	,	kIx,	,
slavnému	slavný	k2eAgMnSc3d1	slavný
učiteli	učitel	k1gMnSc3	učitel
budoucího	budoucí	k2eAgMnSc4d1	budoucí
krále	král	k1gMnSc4	král
Jiřího	Jiří	k1gMnSc4	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Hynka	Hynek	k1gMnSc2	Hynek
i	i	k8xC	i
jeho	jeho	k3xOp3gFnSc2	jeho
dcery	dcera	k1gFnSc2	dcera
spravoval	spravovat	k5eAaImAgMnS	spravovat
panství	panství	k1gNnSc4	panství
syn	syn	k1gMnSc1	syn
Jiříka	Jiřík	k1gMnSc2	Jiřík
<g/>
,	,	kIx,	,
Viktorín	Viktorín	k1gMnSc1	Viktorín
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
však	však	k9	však
kvůli	kvůli	k7c3	kvůli
dluhům	dluh	k1gInPc3	dluh
prodal	prodat	k5eAaPmAgMnS	prodat
město	město	k1gNnSc4	město
svému	svůj	k1gMnSc3	svůj
synovci	synovec	k1gMnSc3	synovec
Janu	Jan	k1gMnSc3	Jan
Bočkovi	Boček	k1gMnSc3	Boček
z	z	k7c2	z
Kunštátu	Kunštát	k1gInSc2	Kunštát
a	a	k8xC	a
Bočkové	Bočkové	k2eAgNnSc2d1	Bočkové
panství	panství	k1gNnSc2	panství
spravovali	spravovat	k5eAaImAgMnP	spravovat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1515	[number]	k4	1515
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jej	on	k3xPp3gMnSc4	on
na	na	k7c4	na
čas	čas	k1gInSc4	čas
převzal	převzít	k5eAaPmAgMnS	převzít
Mikuláš	mikuláš	k1gInSc4	mikuláš
mladší	mladý	k2eAgMnSc1d2	mladší
Trčka	Trčka	k1gMnSc1	Trčka
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Moháče	Moháč	k1gInSc2	Moháč
(	(	kIx(	(
<g/>
1526	[number]	k4	1526
<g/>
)	)	kIx)	)
koupil	koupit	k5eAaPmAgMnS	koupit
polensko-přibyslavské	polenskořibyslavský	k2eAgNnSc4d1	polensko-přibyslavský
panství	panství	k1gNnSc4	panství
Karel	Karla	k1gFnPc2	Karla
z	z	k7c2	z
Valdštejna	Valdštejn	k1gInSc2	Valdštejn
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
ho	on	k3xPp3gNnSc4	on
vyženil	vyženit	k5eAaPmAgMnS	vyženit
bohatý	bohatý	k2eAgMnSc1d1	bohatý
Zachariáš	Zachariáš	k1gMnSc1	Zachariáš
z	z	k7c2	z
Hradce	Hradec	k1gInSc2	Hradec
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
si	se	k3xPyFc3	se
v	v	k7c6	v
Přibyslavi	Přibyslav	k1gFnSc6	Přibyslav
nechal	nechat	k5eAaPmAgMnS	nechat
postavit	postavit	k5eAaPmF	postavit
zámek	zámek	k1gInSc4	zámek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1597	[number]	k4	1597
získali	získat	k5eAaPmAgMnP	získat
město	město	k1gNnSc4	město
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc7	jeho
okolí	okolí	k1gNnSc4	okolí
Žejdlicové	Žejdlicová	k1gFnSc2	Žejdlicová
ze	z	k7c2	z
Šenfeldu	Šenfeld	k1gInSc2	Šenfeld
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
války	válka	k1gFnSc2	válka
mezi	mezi	k7c7	mezi
císařem	císař	k1gMnSc7	císař
a	a	k8xC	a
českými	český	k2eAgInPc7d1	český
stavy	stav	k1gInPc7	stav
obsadila	obsadit	k5eAaPmAgFnS	obsadit
císařská	císařský	k2eAgFnSc1d1	císařská
armáda	armáda	k1gFnSc1	armáda
a	a	k8xC	a
v	v	k7c6	v
pobělohorském	pobělohorský	k2eAgNnSc6d1	pobělohorské
období	období	k1gNnSc6	období
byl	být	k5eAaImAgInS	být
před	před	k7c4	před
soud	soud	k1gInSc4	soud
postaven	postavit	k5eAaPmNgMnS	postavit
i	i	k9	i
majitel	majitel	k1gMnSc1	majitel
přibyslavského	přibyslavský	k2eAgNnSc2d1	Přibyslavské
panství	panství	k1gNnSc2	panství
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Žejdlic	Žejdlice	k1gFnPc2	Žejdlice
ze	z	k7c2	z
Šenfeldu	Šenfeld	k1gInSc2	Šenfeld
<g/>
.	.	kIx.	.
</s>
<s>
Majetky	majetek	k1gInPc4	majetek
mu	on	k3xPp3gMnSc3	on
byly	být	k5eAaImAgInP	být
zkonfiskovány	zkonfiskovat	k5eAaPmNgInP	zkonfiskovat
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
královské	královský	k2eAgFnSc2d1	královská
koruny	koruna	k1gFnSc2	koruna
a	a	k8xC	a
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Panství	panství	k1gNnSc1	panství
bylo	být	k5eAaImAgNnS	být
pod	pod	k7c7	pod
cenou	cena	k1gFnSc7	cena
prodáno	prodat	k5eAaPmNgNnS	prodat
kardinálovi	kardinál	k1gMnSc3	kardinál
Františkovi	František	k1gMnSc3	František
z	z	k7c2	z
Ditrichštejna	Ditrichštejno	k1gNnSc2	Ditrichštejno
a	a	k8xC	a
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
násilné	násilný	k2eAgFnSc3d1	násilná
rekatolizaci	rekatolizace	k1gFnSc3	rekatolizace
poddanstva	poddanstvo	k1gNnSc2	poddanstvo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1643	[number]	k4	1643
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
do	do	k7c2	do
života	život	k1gInSc2	život
města	město	k1gNnSc2	město
třicetiletá	třicetiletý	k2eAgFnSc1d1	třicetiletá
válka	válka	k1gFnSc1	válka
-	-	kIx~	-
město	město	k1gNnSc4	město
napadli	napadnout	k5eAaPmAgMnP	napadnout
Švédové	Švéd	k1gMnPc1	Švéd
a	a	k8xC	a
vyžádali	vyžádat	k5eAaPmAgMnP	vyžádat
si	se	k3xPyFc3	se
výpalné	výpalné	k1gNnSc4	výpalné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1645	[number]	k4	1645
přesto	přesto	k8xC	přesto
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
celé	celý	k2eAgNnSc1d1	celé
město	město	k1gNnSc1	město
vyhořelo	vyhořet	k5eAaPmAgNnS	vyhořet
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
zdevastované	zdevastovaný	k2eAgFnSc2d1	zdevastovaná
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc4	město
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
kníže	kníže	k1gMnSc1	kníže
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
z	z	k7c2	z
Ditrichštejna	Ditrichštejn	k1gInSc2	Ditrichštejn
<g/>
.	.	kIx.	.
</s>
<s>
Knížata	kníže	k1gMnPc1wR	kníže
z	z	k7c2	z
Ditrichštejnu	Ditrichštejn	k1gInSc2	Ditrichštejn
vládla	vládnout	k5eAaImAgFnS	vládnout
během	během	k7c2	během
valné	valný	k2eAgFnSc2d1	valná
části	část	k1gFnSc2	část
17	[number]	k4	17
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgMnSc1d1	poslední
Ditrichštejn	Ditrichštejn	k1gMnSc1	Ditrichštejn
zemřel	zemřít	k5eAaPmAgMnS	zemřít
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1864	[number]	k4	1864
a	a	k8xC	a
rod	rod	k1gInSc1	rod
vymřel	vymřít	k5eAaPmAgInS	vymřít
po	po	k7c6	po
meči	meč	k1gInSc6	meč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
postihlo	postihnout	k5eAaPmAgNnS	postihnout
Přibyslav	Přibyslav	k1gFnSc4	Přibyslav
několik	několik	k4yIc1	několik
pohrom	pohroma	k1gFnPc2	pohroma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1766	[number]	k4	1766
zmrzla	zmrznout	k5eAaPmAgFnS	zmrznout
úroda	úroda	k1gFnSc1	úroda
a	a	k8xC	a
obyvatelé	obyvatel	k1gMnPc1	obyvatel
si	se	k3xPyFc3	se
tak	tak	k6eAd1	tak
na	na	k7c4	na
příští	příští	k2eAgInSc4d1	příští
rok	rok	k1gInSc4	rok
museli	muset	k5eAaImAgMnP	muset
půjčit	půjčit	k5eAaPmF	půjčit
osivo	osivo	k1gNnSc4	osivo
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
1767	[number]	k4	1767
<g/>
)	)	kIx)	)
začala	začít	k5eAaPmAgFnS	začít
hořet	hořet	k5eAaImF	hořet
střecha	střecha	k1gFnSc1	střecha
domu	dům	k1gInSc2	dům
v	v	k7c6	v
Zámecké	zámecký	k2eAgFnSc6d1	zámecká
ulici	ulice	k1gFnSc6	ulice
a	a	k8xC	a
za	za	k7c4	za
15	[number]	k4	15
minut	minuta	k1gFnPc2	minuta
od	od	k7c2	od
vypuknutí	vypuknutí	k1gNnSc2	vypuknutí
požáru	požár	k1gInSc2	požár
shořelo	shořet	k5eAaPmAgNnS	shořet
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věži	věž	k1gFnSc6	věž
nad	nad	k7c7	nad
kostelem	kostel	k1gInSc7	kostel
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
roztavily	roztavit	k5eAaPmAgInP	roztavit
tři	tři	k4xCgInPc1	tři
malé	malý	k2eAgInPc1d1	malý
zvony	zvon	k1gInPc1	zvon
a	a	k8xC	a
shořela	shořet	k5eAaPmAgFnS	shořet
také	také	k9	také
nemocnice	nemocnice	k1gFnSc1	nemocnice
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
školou	škola	k1gFnSc7	škola
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
kostel	kostel	k1gInSc4	kostel
požár	požár	k1gInSc1	požár
vyjma	vyjma	k7c4	vyjma
střechy	střecha	k1gFnPc4	střecha
přečkal	přečkat	k5eAaPmAgMnS	přečkat
<g/>
.	.	kIx.	.
</s>
<s>
Většině	většina	k1gFnSc3	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
shořel	shořet	k5eAaPmAgMnS	shořet
při	při	k7c6	při
požáru	požár	k1gInSc6	požár
veškerý	veškerý	k3xTgInSc4	veškerý
majetek	majetek	k1gInSc4	majetek
a	a	k8xC	a
obec	obec	k1gFnSc1	obec
si	se	k3xPyFc3	se
tak	tak	k6eAd1	tak
musela	muset	k5eAaImAgFnS	muset
půjčovat	půjčovat	k5eAaImF	půjčovat
<g/>
.	.	kIx.	.
</s>
<s>
Majitel	majitel	k1gMnSc1	majitel
panství	panství	k1gNnSc2	panství
Karel	Karel	k1gMnSc1	Karel
z	z	k7c2	z
Ditrichštejna	Ditrichštejn	k1gInSc2	Ditrichštejn
daroval	darovat	k5eAaPmAgMnS	darovat
postiženým	postižený	k1gMnPc3	postižený
stavební	stavební	k2eAgInSc4d1	stavební
materiál	materiál	k1gInSc4	materiál
na	na	k7c4	na
obnovu	obnova	k1gFnSc4	obnova
poničených	poničený	k2eAgFnPc2d1	poničená
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
navíc	navíc	k6eAd1	navíc
postihl	postihnout	k5eAaPmAgInS	postihnout
zemi	zem	k1gFnSc4	zem
hladomor	hladomor	k1gInSc1	hladomor
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1775	[number]	k4	1775
shořelo	shořet	k5eAaPmAgNnS	shořet
při	při	k7c6	při
požáru	požár	k1gInSc6	požár
dalších	další	k2eAgInPc2d1	další
44	[number]	k4	44
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
padesát	padesát	k4xCc4	padesát
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1821	[number]	k4	1821
<g/>
,	,	kIx,	,
prošla	projít	k5eAaPmAgFnS	projít
Přibyslaví	Přibyslav	k1gFnSc7	Přibyslav
epidemie	epidemie	k1gFnSc2	epidemie
cholery	cholera	k1gFnSc2	cholera
a	a	k8xC	a
rok	rok	k1gInSc1	rok
1836	[number]	k4	1836
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
vážné	vážný	k2eAgFnSc2d1	vážná
nákazy	nákaza	k1gFnSc2	nákaza
chřipky	chřipka	k1gFnSc2	chřipka
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1842	[number]	k4	1842
panovalo	panovat	k5eAaImAgNnS	panovat
sucho	sucho	k1gNnSc1	sucho
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
dalšímu	další	k2eAgInSc3d1	další
požáru	požár	k1gInSc3	požár
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
město	město	k1gNnSc4	město
postihl	postihnout	k5eAaPmAgMnS	postihnout
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Ditrichštejnech	Ditrichštejn	k1gInPc6	Ditrichštejn
získala	získat	k5eAaPmAgFnS	získat
panství	panství	k1gNnSc2	panství
Klotilda	Klotildo	k1gNnSc2	Klotildo
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
hraběte	hrabě	k1gMnSc4	hrabě
Eduarda	Eduard	k1gMnSc4	Eduard
Clam-Gallase	Clam-Gallasa	k1gFnSc6	Clam-Gallasa
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
<g/>
.	.	kIx.	.
</s>
<s>
Dvacáté	dvacátý	k4xOgNnSc1	dvacátý
století	století	k1gNnSc1	století
se	se	k3xPyFc4	se
neslo	nést	k5eAaImAgNnS	nést
nejprve	nejprve	k6eAd1	nejprve
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
relativní	relativní	k2eAgFnSc2d1	relativní
prosperity	prosperita	k1gFnSc2	prosperita
a	a	k8xC	a
výstavby	výstavba	k1gFnSc2	výstavba
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgInP	dostat
první	první	k4xOgInPc1	první
automobily	automobil	k1gInPc1	automobil
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
pumpa	pumpa	k1gFnSc1	pumpa
<g/>
,	,	kIx,	,
stavěly	stavět	k5eAaImAgFnP	stavět
se	se	k3xPyFc4	se
podnikové	podnikový	k2eAgFnPc1d1	podniková
budovy	budova	k1gFnPc1	budova
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
v	v	k7c6	v
bojích	boj	k1gInPc6	boj
39	[number]	k4	39
mužů	muž	k1gMnPc2	muž
z	z	k7c2	z
Přibyslavi	Přibyslav	k1gFnSc2	Přibyslav
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
1924	[number]	k4	1924
přinesl	přinést	k5eAaPmAgInS	přinést
velké	velký	k2eAgFnPc4d1	velká
oslavy	oslava	k1gFnPc4	oslava
500	[number]	k4	500
let	léto	k1gNnPc2	léto
od	od	k7c2	od
smrti	smrt	k1gFnSc2	smrt
Jana	Jan	k1gMnSc2	Jan
Žižky	Žižka	k1gMnSc2	Žižka
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
založena	založen	k2eAgFnSc1d1	založena
kazatelská	kazatelský	k2eAgFnSc1d1	kazatelská
stanice	stanice	k1gFnSc1	stanice
sázavského	sázavský	k2eAgInSc2d1	sázavský
sboru	sbor	k1gInSc2	sbor
Českobratrské	českobratrský	k2eAgFnSc2d1	Českobratrská
církve	církev	k1gFnSc2	církev
evangelické	evangelický	k2eAgFnSc2d1	evangelická
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
věřící	věřící	k1gFnSc1	věřící
nyní	nyní	k6eAd1	nyní
jednou	jeden	k4xCgFnSc7	jeden
měsíčně	měsíčně	k6eAd1	měsíčně
scházejí	scházet	k5eAaImIp3nP	scházet
ke	k	k7c3	k
svým	svůj	k3xOyFgFnPc3	svůj
bohoslužbám	bohoslužba	k1gFnPc3	bohoslužba
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
městského	městský	k2eAgInSc2d1	městský
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
město	město	k1gNnSc1	město
na	na	k7c4	na
měsíc	měsíc	k1gInSc4	měsíc
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
600	[number]	k4	600
německých	německý	k2eAgMnPc2d1	německý
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
stalo	stát	k5eAaPmAgNnS	stát
součástí	součást	k1gFnPc2	součást
tzv.	tzv.	kA	tzv.
protektorátu	protektorát	k1gInSc6	protektorát
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
asi	asi	k9	asi
10	[number]	k4	10
přibyslavských	přibyslavský	k2eAgMnPc2d1	přibyslavský
Židů	Žid	k1gMnPc2	Žid
transportováno	transportován	k2eAgNnSc1d1	transportováno
do	do	k7c2	do
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
byli	být	k5eAaImAgMnP	být
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
části	část	k1gFnSc6	část
Hřiště	hřiště	k1gNnSc1	hřiště
českými	český	k2eAgMnPc7d1	český
četníky	četník	k1gMnPc7	četník
zastřeleni	zastřelen	k2eAgMnPc1d1	zastřelen
divizní	divizní	k2eAgMnSc1d1	divizní
generál	generál	k1gMnSc1	generál
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Boris	Boris	k1gMnSc1	Boris
Luža	Luža	k1gMnSc1	Luža
(	(	kIx(	(
<g/>
čelný	čelný	k2eAgMnSc1d1	čelný
představitel	představitel	k1gMnSc1	představitel
Obrany	obrana	k1gFnSc2	obrana
národa	národ	k1gInSc2	národ
<g/>
)	)	kIx)	)
a	a	k8xC	a
poručík	poručík	k1gMnSc1	poručík
Josef	Josef	k1gMnSc1	Josef
Koreš	Koreš	k1gMnSc1	Koreš
<g/>
.	.	kIx.	.
</s>
<s>
Smutnou	smutný	k2eAgFnSc4d1	smutná
událost	událost	k1gFnSc4	událost
připomíná	připomínat	k5eAaImIp3nS	připomínat
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následné	následný	k2eAgFnSc6d1	následná
krvavé	krvavý	k2eAgFnSc6d1	krvavá
odvetě	odveta	k1gFnSc6	odveta
vedené	vedený	k2eAgFnSc6d1	vedená
Lužovým	Lužův	k2eAgNnSc7d1	Lužův
synem	syn	k1gMnSc7	syn
bylo	být	k5eAaImAgNnS	být
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1944	[number]	k4	1944
na	na	k7c6	na
četnické	četnický	k2eAgFnSc6d1	četnická
stanici	stanice	k1gFnSc6	stanice
v	v	k7c6	v
Přibyslavi	Přibyslav	k1gFnSc6	Přibyslav
partyzány	partyzána	k1gFnSc2	partyzána
zastřeleno	zastřelit	k5eAaPmNgNnS	zastřelit
pět	pět	k4xCc1	pět
četníků	četník	k1gMnPc2	četník
<g/>
.	.	kIx.	.
</s>
<s>
Odbojáři	odbojář	k1gMnPc1	odbojář
posléze	posléze	k6eAd1	posléze
hrozili	hrozit	k5eAaImAgMnP	hrozit
smrtí	smrt	k1gFnSc7	smrt
všem	všecek	k3xTgMnPc3	všecek
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
chtěli	chtít	k5eAaImAgMnP	chtít
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
jejich	jejich	k3xOp3gInSc2	jejich
pohřbu	pohřeb	k1gInSc2	pohřeb
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
se	se	k3xPyFc4	se
na	na	k7c6	na
Přibyslavsku	Přibyslavsko	k1gNnSc6	Přibyslavsko
bojovalo	bojovat	k5eAaImAgNnS	bojovat
a	a	k8xC	a
toho	ten	k3xDgInSc2	ten
dne	den	k1gInSc2	den
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
město	město	k1gNnSc4	město
svrženo	svrhnout	k5eAaPmNgNnS	svrhnout
několik	několik	k4yIc1	několik
sovětských	sovětský	k2eAgFnPc2d1	sovětská
bomb	bomba	k1gFnPc2	bomba
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
zabila	zabít	k5eAaPmAgFnS	zabít
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
znárodnění	znárodnění	k1gNnSc1	znárodnění
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
podniků	podnik	k1gInPc2	podnik
<g/>
,	,	kIx,	,
v	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
probíhala	probíhat	k5eAaImAgFnS	probíhat
násilná	násilný	k2eAgFnSc1d1	násilná
kolektivizace	kolektivizace	k1gFnSc1	kolektivizace
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
1968	[number]	k4	1968
přinesl	přinést	k5eAaPmAgInS	přinést
další	další	k2eAgFnSc4d1	další
změnu	změna	k1gFnSc4	změna
<g/>
,	,	kIx,	,
vojska	vojsko	k1gNnPc4	vojsko
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
se	se	k3xPyFc4	se
však	však	k9	však
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
postupu	postup	k1gInSc6	postup
městu	město	k1gNnSc3	město
vyhnula	vyhnout	k5eAaPmAgFnS	vyhnout
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1989	[number]	k4	1989
začali	začít	k5eAaPmAgMnP	začít
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Přibyslavi	Přibyslav	k1gFnSc2	Přibyslav
projevovat	projevovat	k5eAaImF	projevovat
podporu	podpor	k1gInSc2	podpor
demonstrujícím	demonstrující	k2eAgMnPc3d1	demonstrující
studentům	student	k1gMnPc3	student
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
porevolučních	porevoluční	k2eAgNnPc6d1	porevoluční
letech	léto	k1gNnPc6	léto
byli	být	k5eAaImAgMnP	být
starosty	starosta	k1gMnPc7	starosta
postupně	postupně	k6eAd1	postupně
pan	pan	k1gMnSc1	pan
Mojmír	Mojmír	k1gMnSc1	Mojmír
Novotný	Novotný	k1gMnSc1	Novotný
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Jajtner	Jajtner	k1gMnSc1	Jajtner
<g/>
,	,	kIx,	,
sochař	sochař	k1gMnSc1	sochař
Roman	Roman	k1gMnSc1	Roman
Podrázský	Podrázský	k2eAgMnSc1d1	Podrázský
a	a	k8xC	a
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
předčasné	předčasný	k2eAgFnSc6d1	předčasná
smrti	smrt	k1gFnSc6	smrt
Jan	Jan	k1gMnSc1	Jan
Štefáček	Štefáček	k1gMnSc1	Štefáček
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
r.	r.	kA	r.
2014	[number]	k4	2014
zastává	zastávat	k5eAaImIp3nS	zastávat
funkci	funkce	k1gFnSc4	funkce
starosty	starosta	k1gMnSc2	starosta
Martin	Martin	k1gMnSc1	Martin
Kamarád	kamarád	k1gMnSc1	kamarád
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
využívá	využívat	k5eAaPmIp3nS	využívat
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
záchranná	záchranný	k2eAgFnSc1d1	záchranná
služba	služba	k1gFnSc1	služba
sídlo	sídlo	k1gNnSc4	sídlo
v	v	k7c6	v
Přibyslavi	Přibyslav	k1gFnSc6	Přibyslav
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
přestavbou	přestavba	k1gFnSc7	přestavba
rodinného	rodinný	k2eAgInSc2d1	rodinný
domu	dům	k1gInSc2	dům
na	na	k7c6	na
Bechyňově	Bechyňův	k2eAgNnSc6d1	Bechyňovo
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Geologicky	geologicky	k6eAd1	geologicky
leží	ležet	k5eAaImIp3nS	ležet
město	město	k1gNnSc1	město
Přibyslav	Přibyslava	k1gFnPc2	Přibyslava
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
pararulovém	pararulový	k2eAgInSc6d1	pararulový
podkladu	podklad	k1gInSc6	podklad
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
moldanubika	moldanubikum	k1gNnSc2	moldanubikum
<g/>
.	.	kIx.	.
</s>
<s>
Datuje	datovat	k5eAaImIp3nS	datovat
se	se	k3xPyFc4	se
do	do	k7c2	do
paleozoika	paleozoikum	k1gNnSc2	paleozoikum
až	až	k8xS	až
proterozoika	proterozoikum	k1gNnSc2	proterozoikum
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
přechází	přecházet	k5eAaImIp3nS	přecházet
hornina	hornina	k1gFnSc1	hornina
v	v	k7c6	v
migmatit	migmatit	k5eAaImF	migmatit
či	či	k8xC	či
amfibolit	amfibolit	k1gInSc4	amfibolit
stejného	stejný	k2eAgNnSc2d1	stejné
stáří	stáří	k1gNnSc2	stáří
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
podklad	podklad	k1gInSc1	podklad
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
metamorfovaného	metamorfovaný	k2eAgInSc2d1	metamorfovaný
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Skalnatý	skalnatý	k2eAgInSc1d1	skalnatý
svah	svah	k1gInSc1	svah
pod	pod	k7c7	pod
zámkem	zámek	k1gInSc7	zámek
Přibyslav	Přibyslav	k1gFnSc1	Přibyslav
je	být	k5eAaImIp3nS	být
názornou	názorný	k2eAgFnSc7d1	názorná
ukázkou	ukázka	k1gFnSc7	ukázka
horninového	horninový	k2eAgNnSc2d1	horninové
složení	složení	k1gNnSc2	složení
v	v	k7c6	v
pásmu	pásmo	k1gNnSc6	pásmo
tzv.	tzv.	kA	tzv.
přibyslavské	přibyslavský	k2eAgFnSc2d1	Přibyslavská
střižné	střižný	k2eAgFnSc2d1	střižná
mylonitové	mylonitový	k2eAgFnSc2d1	mylonitový
zóny	zóna	k1gFnSc2	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Vidět	vidět	k5eAaImF	vidět
jsou	být	k5eAaImIp3nP	být
mylonity	mylonita	k1gFnPc1	mylonita
a	a	k8xC	a
ultramylonity	ultramylonit	k1gInPc1	ultramylonit
včetně	včetně	k7c2	včetně
porfyroklastů	porfyroklast	k1gInPc2	porfyroklast
živců	živec	k1gInPc2	živec
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
České	český	k2eAgFnSc2d1	Česká
vysočiny	vysočina	k1gFnSc2	vysočina
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc2	její
Česko-moravské	českooravský	k2eAgFnSc2d1	česko-moravská
subprovincie	subprovincie	k1gFnSc2	subprovincie
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
oblasti	oblast	k1gFnSc2	oblast
Českomoravská	českomoravský	k2eAgFnSc1d1	Českomoravská
vrchovina	vrchovina	k1gFnSc1	vrchovina
<g/>
.	.	kIx.	.
řadí	řadit	k5eAaImIp3nS	řadit
se	se	k3xPyFc4	se
do	do	k7c2	do
jejího	její	k3xOp3gInSc2	její
geomorfologického	geomorfologický	k2eAgInSc2d1	geomorfologický
celku	celek	k1gInSc2	celek
Hornosázavská	Hornosázavský	k2eAgFnSc1d1	Hornosázavská
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
<g/>
,	,	kIx,	,
podcelku	podcelka	k1gFnSc4	podcelka
Havlíčkobrodská	havlíčkobrodský	k2eAgFnSc1d1	Havlíčkobrodská
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
<g/>
,	,	kIx,	,
okrsku	okrsek	k1gInSc6	okrsek
Přibyslavská	přibyslavský	k2eAgFnSc1d1	Přibyslavská
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrcholem	vrchol	k1gInSc7	vrchol
na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
Jarošův	Jarošův	k2eAgInSc1d1	Jarošův
kopec	kopec	k1gInSc1	kopec
(	(	kIx(	(
<g/>
554	[number]	k4	554
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
pojmenovaným	pojmenovaný	k2eAgInSc7d1	pojmenovaný
vrcholem	vrchol	k1gInSc7	vrchol
je	být	k5eAaImIp3nS	být
Borovina	borovina	k1gFnSc1	borovina
(	(	kIx(	(
<g/>
524	[number]	k4	524
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
nejnižším	nízký	k2eAgNnSc7d3	nejnižší
místem	místo	k1gNnSc7	místo
je	být	k5eAaImIp3nS	být
lokalita	lokalita	k1gFnSc1	lokalita
u	u	k7c2	u
Štukhejského	Štukhejský	k2eAgInSc2d1	Štukhejský
Mlýna	mlýn	k1gInSc2	mlýn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
řeka	řeka	k1gFnSc1	řeka
Sázava	Sázava	k1gFnSc1	Sázava
opouští	opouštět	k5eAaImIp3nS	opouštět
město	město	k1gNnSc4	město
Přibyslav	Přibyslava	k1gFnPc2	Přibyslava
(	(	kIx(	(
<g/>
245	[number]	k4	245
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Převažujícím	převažující	k2eAgInSc7d1	převažující
půdním	půdní	k2eAgInSc7d1	půdní
typem	typ	k1gInSc7	typ
je	být	k5eAaImIp3nS	být
mezobazická	mezobazická	k1gFnSc1	mezobazická
kambizem	kambiz	k1gInSc7	kambiz
<g/>
,	,	kIx,	,
místy	místy	k6eAd1	místy
oglejená	oglejený	k2eAgFnSc1d1	oglejená
<g/>
;	;	kIx,	;
kolem	kolem	k7c2	kolem
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
fluvizemě	fluvizemě	k6eAd1	fluvizemě
a	a	k8xC	a
modální	modální	k2eAgFnSc1d1	modální
gleje	gleje	k1gFnSc1	gleje
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
města	město	k1gNnSc2	město
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
úmoří	úmoří	k1gNnSc2	úmoří
Severního	severní	k2eAgNnSc2d1	severní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
směru	směr	k1gInSc6	směr
od	od	k7c2	od
východu	východ	k1gInSc2	východ
k	k	k7c3	k
severozápadu	severozápad	k1gInSc3	severozápad
protéká	protékat	k5eAaImIp3nS	protékat
městem	město	k1gNnSc7	město
řeka	řek	k1gMnSc2	řek
Sázava	Sázava	k1gFnSc1	Sázava
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vlévá	vlévat	k5eAaImIp3nS	vlévat
několik	několik	k4yIc1	několik
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
směru	směr	k1gInSc6	směr
toku	tok	k1gInSc2	tok
řeky	řeka	k1gFnSc2	řeka
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
na	na	k7c6	na
území	území	k1gNnSc4	území
Přibyslavi	Přibyslav	k1gFnSc2	Přibyslav
nejprve	nejprve	k6eAd1	nejprve
pravostranně	pravostranně	k6eAd1	pravostranně
vlévá	vlévat	k5eAaImIp3nS	vlévat
Losenický	Losenický	k2eAgInSc4d1	Losenický
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
levostranně	levostranně	k6eAd1	levostranně
postupně	postupně	k6eAd1	postupně
Olešenský	Olešenský	k2eAgInSc1d1	Olešenský
potok	potok	k1gInSc1	potok
a	a	k8xC	a
Bystřice	Bystřice	k1gFnSc1	Bystřice
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
opět	opět	k6eAd1	opět
z	z	k7c2	z
pravé	pravý	k2eAgFnSc2d1	pravá
strany	strana	k1gFnSc2	strana
Doberský	Doberský	k2eAgInSc1d1	Doberský
potok	potok	k1gInSc1	potok
a	a	k8xC	a
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
i	i	k9	i
Borovský	borovský	k2eAgInSc4d1	borovský
potok	potok	k1gInSc4	potok
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Sázavě	Sázava	k1gFnSc6	Sázava
se	se	k3xPyFc4	se
často	často	k6eAd1	často
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
povodně	povodeň	k1gFnPc1	povodeň
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
realizován	realizovat	k5eAaBmNgInS	realizovat
projekt	projekt	k1gInSc1	projekt
protipovodňové	protipovodňový	k2eAgFnSc2d1	protipovodňová
hráze	hráz	k1gFnSc2	hráz
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
Quittovy	Quittův	k2eAgFnSc2d1	Quittova
stupnice	stupnice	k1gFnSc2	stupnice
patří	patřit	k5eAaImIp3nS	patřit
Přibyslav	Přibyslav	k1gFnSc1	Přibyslav
do	do	k7c2	do
klimatické	klimatický	k2eAgFnSc2d1	klimatická
oblasti	oblast	k1gFnSc2	oblast
označované	označovaný	k2eAgFnSc2d1	označovaná
MT	MT	kA	MT
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
mírně	mírně	k6eAd1	mírně
teplé	teplý	k2eAgNnSc1d1	teplé
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
se	se	k3xPyFc4	se
ve	v	k7c6	v
120	[number]	k4	120
až	až	k9	až
140	[number]	k4	140
dnech	den	k1gInPc6	den
během	během	k7c2	během
roku	rok	k1gInSc2	rok
vyšplhá	vyšplhat	k5eAaPmIp3nS	vyšplhat
teplota	teplota	k1gFnSc1	teplota
nad	nad	k7c7	nad
10	[number]	k4	10
°	°	k?	°
<g/>
C.	C.	kA	C.
Během	během	k7c2	během
ledna	leden	k1gInSc2	leden
se	se	k3xPyFc4	se
průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
-	-	kIx~	-
až	až	k6eAd1	až
-	-	kIx~	-
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
mezi	mezi	k7c7	mezi
6	[number]	k4	6
a	a	k8xC	a
7	[number]	k4	7
°	°	k?	°
<g/>
C.	C.	kA	C.
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
průměrná	průměrný	k2eAgFnSc1d1	průměrná
červencová	červencový	k2eAgFnSc1d1	červencová
teplota	teplota	k1gFnSc1	teplota
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
16	[number]	k4	16
či	či	k8xC	či
17	[number]	k4	17
<g />
.	.	kIx.	.
</s>
<s>
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
se	se	k3xPyFc4	se
teplota	teplota	k1gFnSc1	teplota
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
nejčastěji	často	k6eAd3	často
od	od	k7c2	od
6	[number]	k4	6
do	do	k7c2	do
7	[number]	k4	7
°	°	k?	°
<g/>
C.	C.	kA	C.
Během	během	k7c2	během
110	[number]	k4	110
až	až	k9	až
120	[number]	k4	120
dnů	den	k1gInPc2	den
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
kalendářního	kalendářní	k2eAgInSc2d1	kalendářní
roku	rok	k1gInSc2	rok
spadnou	spadnout	k5eAaPmIp3nP	spadnout
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
dešťové	dešťový	k2eAgFnPc1d1	dešťová
srážky	srážka	k1gFnPc1	srážka
v	v	k7c6	v
úhrnu	úhrn	k1gInSc6	úhrn
alespoň	alespoň	k9	alespoň
1	[number]	k4	1
milimetr	milimetr	k1gInSc1	milimetr
na	na	k7c4	na
metr	metr	k1gInSc4	metr
čtvereční	čtvereční	k2eAgInSc4d1	čtvereční
a	a	k8xC	a
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
60	[number]	k4	60
až	až	k9	až
100	[number]	k4	100
dnů	den	k1gInPc2	den
je	být	k5eAaImIp3nS	být
země	země	k1gFnSc1	země
zakryta	zakryt	k2eAgFnSc1d1	zakryta
sněhovou	sněhový	k2eAgFnSc7d1	sněhová
pokrývkou	pokrývka	k1gFnSc7	pokrývka
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
teplota	teplota	k1gFnSc1	teplota
vzduchu	vzduch	k1gInSc2	vzduch
bylo	být	k5eAaImAgNnS	být
naměřena	naměřen	k2eAgFnSc1d1	naměřena
27	[number]	k4	27
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
36	[number]	k4	36
°	°	k?	°
<g/>
C.	C.	kA	C.
Naopak	naopak	k6eAd1	naopak
nejníže	nízce	k6eAd3	nízce
se	se	k3xPyFc4	se
teplota	teplota	k1gFnSc1	teplota
dostala	dostat	k5eAaPmAgFnS	dostat
10	[number]	k4	10
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
-	-	kIx~	-
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
polohách	poloha	k1gFnPc6	poloha
nacházejí	nacházet	k5eAaImIp3nP	nacházet
smrkové	smrkový	k2eAgInPc1d1	smrkový
a	a	k8xC	a
borové	borový	k2eAgInPc1d1	borový
lesy	les	k1gInPc1	les
či	či	k8xC	či
bukové	bukový	k2eAgInPc1d1	bukový
a	a	k8xC	a
jedlové	jedlový	k2eAgInPc1d1	jedlový
lesy	les	k1gInPc1	les
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
zde	zde	k6eAd1	zde
kyčelnice	kyčelnice	k1gFnSc1	kyčelnice
devítilistá	devítilistý	k2eAgFnSc1d1	devítilistá
(	(	kIx(	(
<g/>
Dentaria	Dentarium	k1gNnPc1	Dentarium
enneaphyllos	enneaphyllosa	k1gFnPc2	enneaphyllosa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
svízel	svízel	k1gInSc1	svízel
vonný	vonný	k2eAgInSc1d1	vonný
(	(	kIx(	(
<g/>
Galium	galium	k1gNnSc1	galium
odoratum	odoratum	k1gNnSc1	odoratum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bažanka	bažanka	k1gFnSc1	bažanka
vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
(	(	kIx(	(
<g/>
Mercurialis	Mercurialis	k1gFnSc1	Mercurialis
perennis	perennis	k1gFnSc2	perennis
<g/>
)	)	kIx)	)
či	či	k8xC	či
violka	violka	k1gFnSc1	violka
lesní	lesní	k2eAgFnSc1d1	lesní
(	(	kIx(	(
<g/>
Viola	Viola	k1gFnSc1	Viola
reichenbachiana	reichenbachiana	k1gFnSc1	reichenbachiana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nižších	nízký	k2eAgFnPc6d2	nižší
polohách	poloha	k1gFnPc6	poloha
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
doubravy	doubrava	k1gFnPc1	doubrava
a	a	k8xC	a
dubohabřiny	dubohabřina	k1gFnPc1	dubohabřina
tvořené	tvořený	k2eAgFnPc1d1	tvořená
duby	dub	k1gInPc7	dub
letními	letní	k2eAgInPc7d1	letní
(	(	kIx(	(
<g/>
Quercus	Quercus	k1gInSc1	Quercus
robur	robura	k1gFnPc2	robura
<g/>
)	)	kIx)	)
i	i	k8xC	i
zimními	zimní	k2eAgInPc7d1	zimní
(	(	kIx(	(
<g/>
Quercus	Quercus	k1gInSc1	Quercus
petraea	petrae	k1gInSc2	petrae
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
habrem	habr	k1gInSc7	habr
obecným	obecný	k2eAgInSc7d1	obecný
(	(	kIx(	(
<g/>
Carpinus	Carpinus	k1gInSc1	Carpinus
betulus	betulus	k1gInSc1	betulus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
podrostu	podrost	k1gInSc6	podrost
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
najít	najít	k5eAaPmF	najít
lipnici	lipnice	k1gFnSc4	lipnice
hajní	hajní	k2eAgFnSc4d1	hajní
(	(	kIx(	(
<g/>
Poa	Poa	k1gFnSc4	Poa
nemoralis	nemoralis	k1gFnSc2	nemoralis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kostřavu	kostřava	k1gFnSc4	kostřava
ovčí	ovčí	k2eAgMnSc1d1	ovčí
(	(	kIx(	(
<g/>
Festuca	Festuc	k2eAgFnSc1d1	Festuca
ovina	ovina	k1gFnSc1	ovina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
konvalinku	konvalinka	k1gFnSc4	konvalinka
vonnou	vonný	k2eAgFnSc4d1	vonná
(	(	kIx(	(
<g/>
Convallaria	Convallarium	k1gNnPc1	Convallarium
majalis	majalis	k1gFnPc2	majalis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jaterník	jaterník	k1gInSc1	jaterník
podléšku	podléška	k1gFnSc4	podléška
(	(	kIx(	(
<g/>
Hepatica	Hepatic	k2eAgFnSc1d1	Hepatica
nobilis	nobilis	k1gFnSc1	nobilis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zvonek	zvonek	k1gInSc4	zvonek
broskvolistý	broskvolistý	k2eAgInSc4d1	broskvolistý
(	(	kIx(	(
<g/>
Campanula	Campanula	k1gFnSc1	Campanula
persicifolia	persicifolia	k1gFnSc1	persicifolia
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
černýš	černýš	k1gInSc1	černýš
hajní	hajní	k2eAgInSc1d1	hajní
(	(	kIx(	(
<g/>
Melampyrum	Melampyrum	k1gInSc1	Melampyrum
nemorosum	nemorosum	k1gInSc1	nemorosum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
se	se	k3xPyFc4	se
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
olšiny	olšina	k1gFnPc1	olšina
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgNnPc6	jenž
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
bledule	bledule	k1gFnPc1	bledule
jarní	jarní	k2eAgFnPc1d1	jarní
(	(	kIx(	(
<g/>
Leucojum	Leucojum	k1gInSc1	Leucojum
vernum	vernum	k1gInSc1	vernum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ďáblík	ďáblík	k1gInSc1	ďáblík
bahenní	bahenní	k2eAgInSc1d1	bahenní
(	(	kIx(	(
<g/>
Calla	Calla	k1gFnSc1	Calla
palustris	palustris	k1gFnSc2	palustris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozpuk	rozpuk	k1gInSc1	rozpuk
jízlivý	jízlivý	k2eAgInSc1d1	jízlivý
(	(	kIx(	(
<g/>
Cicuta	Cicut	k2eAgFnSc1d1	Cicuta
virosa	virosa	k1gFnSc1	virosa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hladýš	hladýš	k1gInSc1	hladýš
pruský	pruský	k2eAgInSc1d1	pruský
(	(	kIx(	(
<g/>
Laserpitium	Laserpitium	k1gNnSc1	Laserpitium
prutenicum	prutenicum	k1gNnSc1	prutenicum
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
třtina	třtina	k1gFnSc1	třtina
nachová	nachový	k2eAgFnSc1d1	nachová
(	(	kIx(	(
<g/>
Calamagrostis	Calamagrostis	k1gFnSc1	Calamagrostis
phragmitoides	phragmitoides	k1gMnSc1	phragmitoides
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
violka	violka	k1gFnSc1	violka
bahenní	bahenní	k2eAgFnSc1d1	bahenní
(	(	kIx(	(
<g/>
Viola	Viola	k1gFnSc1	Viola
palustris	palustris	k1gFnSc2	palustris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bublinatka	bublinatka	k1gFnSc1	bublinatka
jižní	jižní	k2eAgFnSc1d1	jižní
(	(	kIx(	(
<g/>
Utricularia	Utricularium	k1gNnPc1	Utricularium
australis	australis	k1gFnPc2	australis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
tolije	tolije	k1gFnSc1	tolije
bahenní	bahenní	k2eAgFnSc1d1	bahenní
(	(	kIx(	(
<g/>
Parnassia	Parnassia	k1gFnSc1	Parnassia
palustris	palustris	k1gFnSc2	palustris
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
úpolín	úpolín	k1gInSc1	úpolín
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
(	(	kIx(	(
<g/>
Trollius	Trollius	k1gInSc1	Trollius
altissimus	altissimus	k1gInSc1	altissimus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sušších	suchý	k2eAgFnPc6d2	sušší
částech	část	k1gFnPc6	část
luk	louka	k1gFnPc2	louka
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
hořeček	hořeček	k1gInSc1	hořeček
mnohotvarý	mnohotvarý	k2eAgInSc1d1	mnohotvarý
český	český	k2eAgInSc1d1	český
(	(	kIx(	(
<g/>
Gentianella	Gentianella	k1gFnSc1	Gentianella
praecox	praecox	k1gInSc1	praecox
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
bohemica	bohemica	k1gMnSc1	bohemica
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vemeník	vemeník	k1gInSc4	vemeník
dvoulistý	dvoulistý	k2eAgInSc4d1	dvoulistý
(	(	kIx(	(
<g/>
Platanthera	Platanthera	k1gFnSc1	Platanthera
bifolia	bifolia	k1gFnSc1	bifolia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prha	prha	k1gFnSc1	prha
arnika	arnika	k1gFnSc1	arnika
(	(	kIx(	(
<g/>
Arnica	Arnic	k2eAgFnSc1d1	Arnica
montana	montana	k1gFnSc1	montana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
všivec	všivec	k1gInSc4	všivec
lesní	lesní	k2eAgInSc4d1	lesní
(	(	kIx(	(
<g/>
Pedicularis	Pedicularis	k1gInSc4	Pedicularis
sylvatica	sylvatic	k1gInSc2	sylvatic
<g/>
)	)	kIx)	)
či	či	k8xC	či
prstnatec	prstnatec	k1gMnSc1	prstnatec
májový	májový	k2eAgMnSc1d1	májový
(	(	kIx(	(
<g/>
Dactylorhiza	Dactylorhiza	k1gFnSc1	Dactylorhiza
majalis	majalis	k1gFnSc2	majalis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zvířat	zvíře	k1gNnPc2	zvíře
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
prase	prase	k1gNnSc1	prase
divoké	divoký	k2eAgNnSc1d1	divoké
(	(	kIx(	(
<g/>
Sus	Sus	k1gMnSc4	Sus
scrofa	scrof	k1gMnSc4	scrof
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
srnec	srnec	k1gMnSc1	srnec
obecný	obecný	k2eAgMnSc1d1	obecný
(	(	kIx(	(
<g/>
Capreolus	Capreolus	k1gMnSc1	Capreolus
capreolus	capreolus	k1gMnSc1	capreolus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jelen	jelen	k1gMnSc1	jelen
evropský	evropský	k2eAgMnSc1d1	evropský
(	(	kIx(	(
<g/>
Cervus	Cervus	k1gMnSc1	Cervus
elaphus	elaphus	k1gMnSc1	elaphus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
daněk	daněk	k1gMnSc1	daněk
evropský	evropský	k2eAgMnSc1d1	evropský
(	(	kIx(	(
<g/>
Dama	Dama	k1gMnSc1	Dama
dama	dama	k1gMnSc1	dama
<g/>
)	)	kIx)	)
či	či	k8xC	či
muflon	muflon	k1gMnSc1	muflon
(	(	kIx(	(
<g/>
Ovis	Ovis	k1gInSc1	Ovis
musimon	musimon	k1gInSc1	musimon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
šelem	šelma	k1gFnPc2	šelma
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
kuna	kuna	k1gFnSc1	kuna
lesní	lesní	k2eAgFnSc1d1	lesní
(	(	kIx(	(
<g/>
Martes	Martes	k1gMnSc1	Martes
martes	martes	k1gMnSc1	martes
<g/>
)	)	kIx)	)
či	či	k8xC	či
liška	liška	k1gFnSc1	liška
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Vulpes	Vulpes	k1gMnSc1	Vulpes
vulpes	vulpes	k1gMnSc1	vulpes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nalézt	nalézt	k5eAaBmF	nalézt
tu	tu	k6eAd1	tu
je	on	k3xPp3gNnSc4	on
možné	možný	k2eAgNnSc4d1	možné
také	také	k9	také
myšici	myšice	k1gFnSc4	myšice
lesní	lesní	k2eAgFnSc4d1	lesní
(	(	kIx(	(
<g/>
Apodemus	Apodemus	k1gMnSc1	Apodemus
flavicollis	flavicollis	k1gFnSc2	flavicollis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
norníka	norník	k1gMnSc2	norník
rudého	rudý	k1gMnSc2	rudý
(	(	kIx(	(
<g/>
Clethrionomys	Clethrionomys	k1gInSc1	Clethrionomys
glareolus	glareolus	k1gInSc1	glareolus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rejsky	rejsek	k1gInPc4	rejsek
obecného	obecný	k2eAgMnSc2d1	obecný
(	(	kIx(	(
<g/>
Sorex	Sorex	k1gInSc1	Sorex
araneus	araneus	k1gInSc1	araneus
<g/>
)	)	kIx)	)
a	a	k8xC	a
malého	malý	k2eAgMnSc2d1	malý
(	(	kIx(	(
<g/>
Sorex	Sorex	k1gInSc1	Sorex
minutus	minutus	k1gInSc1	minutus
<g/>
)	)	kIx)	)
či	či	k8xC	či
plšíka	plšík	k1gMnSc2	plšík
lískového	lískový	k2eAgMnSc2d1	lískový
(	(	kIx(	(
<g/>
Muscardinus	Muscardinus	k1gInSc1	Muscardinus
avellanarius	avellanarius	k1gInSc1	avellanarius
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vlhkých	vlhký	k2eAgFnPc6d1	vlhká
loukách	louka	k1gFnPc6	louka
našly	najít	k5eAaPmAgFnP	najít
svůj	svůj	k3xOyFgInSc4	svůj
domov	domov	k1gInSc4	domov
zmije	zmije	k1gFnSc1	zmije
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Vipera	Vipera	k1gFnSc1	Vipera
berus	berus	k1gMnSc1	berus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ještěrka	ještěrka	k1gFnSc1	ještěrka
živorodá	živorodý	k2eAgFnSc1d1	živorodá
(	(	kIx(	(
<g/>
Zootoca	Zootoc	k2eAgFnSc1d1	Zootoca
vivipara	vivipara	k1gFnSc1	vivipara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ropucha	ropucha	k1gFnSc1	ropucha
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Bufo	bufa	k1gFnSc5	bufa
bufo	bufa	k1gFnSc5	bufa
<g/>
)	)	kIx)	)
či	či	k8xC	či
kuňka	kuňka	k1gFnSc1	kuňka
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Bombina	Bombin	k2eAgFnSc1d1	Bombina
bombina	bombina	k1gFnSc1	bombina
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
ptáků	pták	k1gMnPc2	pták
bekasina	bekasina	k1gFnSc1	bekasina
otavní	otavní	k2eAgFnSc1d1	otavní
(	(	kIx(	(
<g/>
Gallinago	Gallinago	k1gMnSc1	Gallinago
gallinago	gallinago	k1gMnSc1	gallinago
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
linduška	linduška	k1gFnSc1	linduška
luční	luční	k2eAgFnSc1d1	luční
(	(	kIx(	(
<g/>
Anthus	Anthus	k1gMnSc1	Anthus
pratensis	pratensis	k1gFnSc2	pratensis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bramborníček	bramborníček	k1gMnSc1	bramborníček
hnědý	hnědý	k2eAgMnSc1d1	hnědý
(	(	kIx(	(
<g/>
Saxicola	Saxicola	k1gFnSc1	Saxicola
rubetra	rubetra	k1gFnSc1	rubetra
<g/>
)	)	kIx)	)
a	a	k8xC	a
ťuhýk	ťuhýk	k1gMnSc1	ťuhýk
šedý	šedý	k2eAgMnSc1d1	šedý
(	(	kIx(	(
<g/>
Lanius	Lanius	k1gMnSc1	Lanius
excubitor	excubitor	k1gMnSc1	excubitor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jedlobukových	jedlobukový	k2eAgInPc6d1	jedlobukový
lesích	les	k1gInPc6	les
žijí	žít	k5eAaImIp3nP	žít
datel	datel	k1gMnSc1	datel
černý	černý	k1gMnSc1	černý
(	(	kIx(	(
<g/>
Dryocopus	Dryocopus	k1gMnSc1	Dryocopus
martius	martius	k1gMnSc1	martius
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žluna	žluna	k1gFnSc1	žluna
šedá	šedá	k1gFnSc1	šedá
(	(	kIx(	(
<g/>
Picus	Picus	k1gMnSc1	Picus
canus	canus	k1gMnSc1	canus
<g/>
)	)	kIx)	)
či	či	k8xC	či
holub	holub	k1gMnSc1	holub
doupňák	doupňák	k1gMnSc1	doupňák
(	(	kIx(	(
<g/>
Columba	Columba	k1gMnSc1	Columba
oenas	oenas	k1gMnSc1	oenas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
jehličnatých	jehličnatý	k2eAgInPc6d1	jehličnatý
lesích	les	k1gInPc6	les
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
zahlédnout	zahlédnout	k5eAaPmF	zahlédnout
brhlíka	brhlík	k1gMnSc2	brhlík
lesního	lesní	k2eAgMnSc2d1	lesní
(	(	kIx(	(
<g/>
Sitta	Sitt	k1gMnSc2	Sitt
europaea	europaeus	k1gMnSc2	europaeus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
červenku	červenka	k1gFnSc4	červenka
obecnou	obecný	k2eAgFnSc4d1	obecná
(	(	kIx(	(
<g/>
Erithacus	Erithacus	k1gMnSc1	Erithacus
rubecula	rubecul	k1gMnSc2	rubecul
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
drozda	drozd	k1gMnSc2	drozd
zpěvného	zpěvný	k2eAgMnSc2d1	zpěvný
(	(	kIx(	(
<g/>
Turdus	Turdus	k1gInSc1	Turdus
philomelos	philomelos	k1gInSc1	philomelos
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodách	voda	k1gFnPc6	voda
se	se	k3xPyFc4	se
rozmnožují	rozmnožovat	k5eAaImIp3nP	rozmnožovat
skokan	skokan	k1gMnSc1	skokan
hnědý	hnědý	k2eAgMnSc1d1	hnědý
(	(	kIx(	(
<g/>
Rana	Rana	k1gFnSc1	Rana
temporaria	temporarium	k1gNnSc2	temporarium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mlok	mlok	k1gMnSc1	mlok
skvrnitý	skvrnitý	k2eAgMnSc1d1	skvrnitý
(	(	kIx(	(
<g/>
Salamandra	salamandr	k1gMnSc4	salamandr
salamandra	salamandr	k1gMnSc4	salamandr
<g/>
)	)	kIx)	)
a	a	k8xC	a
nad	nad	k7c7	nad
nimi	on	k3xPp3gMnPc7	on
poletují	poletovat	k5eAaImIp3nP	poletovat
ledňáčci	ledňáček	k1gMnPc1	ledňáček
říční	říční	k2eAgNnSc4d1	říční
(	(	kIx(	(
<g/>
Alcedo	Alcedo	k1gNnSc4	Alcedo
atthis	atthis	k1gFnPc1	atthis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
konipasové	konipas	k1gMnPc1	konipas
horští	horský	k2eAgMnPc1d1	horský
(	(	kIx(	(
<g/>
Motacilla	Motacilla	k1gMnSc1	Motacilla
cinerea	cinerea	k1gMnSc1	cinerea
<g/>
)	)	kIx)	)
a	a	k8xC	a
skorci	skorec	k1gMnPc1	skorec
vodní	vodní	k2eAgMnPc1d1	vodní
(	(	kIx(	(
<g/>
Cinclus	Cinclus	k1gMnSc1	Cinclus
cinclus	cinclus	k1gMnSc1	cinclus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
vody	voda	k1gFnSc2	voda
se	se	k3xPyFc4	se
výjimečně	výjimečně	k6eAd1	výjimečně
objeví	objevit	k5eAaPmIp3nS	objevit
i	i	k9	i
vydra	vydra	k1gFnSc1	vydra
říční	říční	k2eAgFnSc1d1	říční
(	(	kIx(	(
<g/>
Lutra	Lutra	k1gFnSc1	Lutra
lutra	lutra	k1gMnSc1	lutra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Východně	východně	k6eAd1	východně
od	od	k7c2	od
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
Chráněná	chráněný	k2eAgFnSc1d1	chráněná
krajinná	krajinný	k2eAgFnSc1d1	krajinná
oblast	oblast	k1gFnSc1	oblast
Žďárské	Žďárská	k1gFnSc2	Žďárská
vrchy	vrch	k1gInPc1	vrch
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
výnosu	výnos	k1gInSc2	výnos
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
kultury	kultura	k1gFnSc2	kultura
České	český	k2eAgFnSc2d1	Česká
socialistické	socialistický	k2eAgFnSc2d1	socialistická
republiky	republika	k1gFnSc2	republika
číslo	číslo	k1gNnSc1	číslo
8908	[number]	k4	8908
<g/>
/	/	kIx~	/
<g/>
70	[number]	k4	70
<g/>
-II	-II	k?	-II
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
ze	z	k7c2	z
dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
účelem	účel	k1gInSc7	účel
je	být	k5eAaImIp3nS	být
ochrana	ochrana	k1gFnSc1	ochrana
zdejší	zdejší	k2eAgFnSc2d1	zdejší
pestrosti	pestrost	k1gFnSc2	pestrost
a	a	k8xC	a
členitosti	členitost	k1gFnSc2	členitost
krajiny	krajina	k1gFnSc2	krajina
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
je	být	k5eAaImIp3nS	být
umístěno	umístěn	k2eAgNnSc4d1	umístěno
venkovské	venkovský	k2eAgNnSc4d1	venkovské
osídlení	osídlení	k1gNnSc4	osídlení
s	s	k7c7	s
roubenými	roubený	k2eAgFnPc7d1	roubená
chalupami	chalupa	k1gFnPc7	chalupa
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejcennější	cenný	k2eAgInPc4d3	nejcennější
prvky	prvek	k1gInPc4	prvek
patří	patřit	k5eAaImIp3nS	patřit
rašeliniště	rašeliniště	k1gNnSc1	rašeliniště
a	a	k8xC	a
vlhké	vlhký	k2eAgFnPc1d1	vlhká
rašelinné	rašelinný	k2eAgFnPc1d1	rašelinná
louky	louka	k1gFnPc1	louka
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
vzácné	vzácný	k2eAgInPc1d1	vzácný
druhy	druh	k1gInPc1	druh
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
celkem	celkem	k6eAd1	celkem
tři	tři	k4xCgFnPc4	tři
lokality	lokalita	k1gFnPc4	lokalita
výskytu	výskyt	k1gInSc2	výskyt
památných	památný	k2eAgInPc2d1	památný
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2003	[number]	k4	2003
chráněna	chráněn	k2eAgFnSc1d1	chráněna
dvojice	dvojice	k1gFnSc1	dvojice
vzrostlých	vzrostlý	k2eAgInPc2d1	vzrostlý
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
jasan	jasan	k1gInSc1	jasan
ztepilý	ztepilý	k2eAgInSc1d1	ztepilý
(	(	kIx(	(
<g/>
Fraxinus	Fraxinus	k1gInSc1	Fraxinus
excelsior	excelsior	k1gInSc1	excelsior
<g/>
)	)	kIx)	)
a	a	k8xC	a
druhým	druhý	k4xOgMnSc7	druhý
lípa	lípa	k1gFnSc1	lípa
malolistá	malolistý	k2eAgFnSc1d1	malolistá
(	(	kIx(	(
<g/>
Tilia	Tilia	k1gFnSc1	Tilia
cordata	cordata	k1gFnSc1	cordata
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zahradě	zahrada	k1gFnSc6	zahrada
přibyslavské	přibyslavský	k2eAgFnSc2d1	Přibyslavská
školy	škola	k1gFnSc2	škola
roste	růst	k5eAaImIp3nS	růst
22	[number]	k4	22
metrů	metr	k1gInPc2	metr
vysoký	vysoký	k2eAgInSc4d1	vysoký
buk	buk	k1gInSc4	buk
lesní	lesní	k2eAgInSc4d1	lesní
(	(	kIx(	(
<g/>
Fagus	Fagus	k1gInSc4	Fagus
sylvatica	sylvatic	k1gInSc2	sylvatic
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
obvod	obvod	k1gInSc1	obvod
kmene	kmen	k1gInSc2	kmen
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
360	[number]	k4	360
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
.	.	kIx.	.
</s>
<s>
Strom	strom	k1gInSc1	strom
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
června	červen	k1gInSc2	červen
1997	[number]	k4	1997
pod	pod	k7c7	pod
památkovou	památkový	k2eAgFnSc7d1	památková
ochranou	ochrana	k1gFnSc7	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
lokalitou	lokalita	k1gFnSc7	lokalita
je	být	k5eAaImIp3nS	být
silnice	silnice	k1gFnSc1	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
351	[number]	k4	351
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
mezi	mezi	k7c7	mezi
Dobrou	dobrá	k1gFnSc7	dobrá
a	a	k8xC	a
Žižkovým	Žižkův	k2eAgNnSc7d1	Žižkovo
Polem	pole	k1gNnSc7	pole
lemována	lemovat	k5eAaImNgFnS	lemovat
lipovou	lipový	k2eAgFnSc7d1	Lipová
alejí	alej	k1gFnSc7	alej
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
ji	on	k3xPp3gFnSc4	on
272	[number]	k4	272
lip	lípa	k1gFnPc2	lípa
velkolistých	velkolistý	k2eAgFnPc2d1	velkolistá
(	(	kIx(	(
<g/>
Tilia	Tilium	k1gNnPc1	Tilium
platyphyllos	platyphyllosa	k1gFnPc2	platyphyllosa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
jedna	jeden	k4xCgFnSc1	jeden
lípa	lípa	k1gFnSc1	lípa
malolistá	malolistý	k2eAgFnSc1d1	malolistá
(	(	kIx(	(
<g/>
Tilia	Tilia	k1gFnSc1	Tilia
cordata	cordata	k1gFnSc1	cordata
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
jedinec	jedinec	k1gMnSc1	jedinec
javoru	javor	k1gInSc2	javor
mléč	mléč	k1gInSc1	mléč
(	(	kIx(	(
<g/>
Acer	Acer	k1gMnSc1	Acer
platanoides	platanoides	k1gMnSc1	platanoides
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stromořadí	stromořadí	k1gNnSc1	stromořadí
je	být	k5eAaImIp3nS	být
chráněno	chránit	k5eAaImNgNnS	chránit
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
listopadu	listopad	k1gInSc2	listopad
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
k	k	k7c3	k
počátku	počátek	k1gInSc3	počátek
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
žilo	žít	k5eAaImAgNnS	žít
celkem	celek	k1gInSc7	celek
4	[number]	k4	4
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
1	[number]	k4	1
990	[number]	k4	990
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
žen	žena	k1gFnPc2	žena
bylo	být	k5eAaImAgNnS	být
přibližně	přibližně	k6eAd1	přibližně
stejně	stejně	k6eAd1	stejně
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
věk	věk	k1gInSc1	věk
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
činil	činit	k5eAaImAgInS	činit
40	[number]	k4	40
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
Sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
domů	dům	k1gInPc2	dům
a	a	k8xC	a
bytů	byt	k1gInPc2	byt
provedeném	provedený	k2eAgInSc6d1	provedený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ve	v	k7c6	v
městě	město	k1gNnSc6	město
žilo	žít	k5eAaImAgNnS	žít
3	[number]	k4	3
864	[number]	k4	864
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
nejvíce	hodně	k6eAd3	hodně
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
16	[number]	k4	16
%	%	kIx~	%
<g/>
)	)	kIx)	)
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
od	od	k7c2	od
30	[number]	k4	30
do	do	k7c2	do
39	[number]	k4	39
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
do	do	k7c2	do
14	[number]	k4	14
let	léto	k1gNnPc2	léto
věku	věk	k1gInSc2	věk
tvořily	tvořit	k5eAaImAgFnP	tvořit
15	[number]	k4	15
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
senioři	senior	k1gMnPc1	senior
nad	nad	k7c4	nad
70	[number]	k4	70
let	léto	k1gNnPc2	léto
úhrnem	úhrnem	k6eAd1	úhrnem
11	[number]	k4	11
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
celkem	celkem	k6eAd1	celkem
3	[number]	k4	3
227	[number]	k4	227
občanů	občan	k1gMnPc2	občan
města	město	k1gNnSc2	město
starších	starý	k2eAgNnPc2d2	starší
15	[number]	k4	15
let	léto	k1gNnPc2	léto
mělo	mít	k5eAaImAgNnS	mít
38	[number]	k4	38
%	%	kIx~	%
nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
ukončené	ukončený	k2eAgNnSc1d1	ukončené
vzdělání	vzdělání	k1gNnSc1	vzdělání
úplné	úplný	k2eAgFnSc2d1	úplná
střední	střední	k2eAgFnSc2d1	střední
bez	bez	k7c2	bez
maturity	maturita	k1gFnSc2	maturita
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
vysokoškoláků	vysokoškolák	k1gMnPc2	vysokoškolák
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
téměř	téměř	k6eAd1	téměř
10	[number]	k4	10
%	%	kIx~	%
a	a	k8xC	a
bez	bez	k7c2	bez
vzdělání	vzdělání	k1gNnSc2	vzdělání
byla	být	k5eAaImAgFnS	být
naopak	naopak	k6eAd1	naopak
0,5	[number]	k4	0,5
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
cenzu	cenzus	k1gInSc2	cenzus
dále	daleko	k6eAd2	daleko
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
městě	město	k1gNnSc6	město
žilo	žít	k5eAaImAgNnS	žít
1	[number]	k4	1
886	[number]	k4	886
ekonomicky	ekonomicky	k6eAd1	ekonomicky
aktivních	aktivní	k2eAgMnPc2d1	aktivní
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
92	[number]	k4	92
%	%	kIx~	%
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
řadilo	řadit	k5eAaImAgNnS	řadit
mezi	mezi	k7c4	mezi
zaměstnané	zaměstnaný	k1gMnPc4	zaměstnaný
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
81	[number]	k4	81
%	%	kIx~	%
patřilo	patřit	k5eAaImAgNnS	patřit
mezi	mezi	k7c4	mezi
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
<g/>
,	,	kIx,	,
2	[number]	k4	2
%	%	kIx~	%
k	k	k7c3	k
zaměstnavatelům	zaměstnavatel	k1gMnPc3	zaměstnavatel
a	a	k8xC	a
zbytek	zbytek	k1gInSc4	zbytek
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c4	na
vlastní	vlastní	k2eAgInSc4d1	vlastní
účet	účet	k1gInSc4	účet
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
celých	celý	k2eAgNnPc2d1	celé
50	[number]	k4	50
%	%	kIx~	%
občanů	občan	k1gMnPc2	občan
nebylo	být	k5eNaImAgNnS	být
ekonomicky	ekonomicky	k6eAd1	ekonomicky
aktivní	aktivní	k2eAgMnSc1d1	aktivní
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
nepracující	pracující	k2eNgMnPc1d1	nepracující
důchodci	důchodce	k1gMnPc1	důchodce
či	či	k8xC	či
žáci	žák	k1gMnPc1	žák
<g/>
,	,	kIx,	,
studenti	student	k1gMnPc1	student
nebo	nebo	k8xC	nebo
učni	učeň	k1gMnPc1	učeň
<g/>
)	)	kIx)	)
a	a	k8xC	a
zbytek	zbytek	k1gInSc1	zbytek
svou	svůj	k3xOyFgFnSc4	svůj
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
aktivitu	aktivita	k1gFnSc4	aktivita
uvést	uvést	k5eAaPmF	uvést
nechtěl	chtít	k5eNaImAgMnS	chtít
<g/>
.	.	kIx.	.
</s>
<s>
Úhrnem	úhrnem	k6eAd1	úhrnem
122	[number]	k4	122
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
pracovalo	pracovat	k5eAaImAgNnS	pracovat
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
,	,	kIx,	,
lesnictví	lesnictví	k1gNnSc6	lesnictví
či	či	k8xC	či
rybolovu	rybolov	k1gInSc6	rybolov
<g/>
,	,	kIx,	,
944	[number]	k4	944
obyvatel	obyvatel	k1gMnPc2	obyvatel
zaměstnával	zaměstnávat	k5eAaImAgMnS	zaměstnávat
průmysl	průmysl	k1gInSc4	průmysl
a	a	k8xC	a
stavebnictví	stavebnictví	k1gNnSc2	stavebnictví
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
672	[number]	k4	672
pracovalo	pracovat	k5eAaImAgNnS	pracovat
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
<g/>
.	.	kIx.	.
</s>
<s>
Úhrnem	úhrnem	k6eAd1	úhrnem
2	[number]	k4	2
820	[number]	k4	820
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
téměř	téměř	k6eAd1	téměř
tři	tři	k4xCgFnPc1	tři
čtvrtiny	čtvrtina	k1gFnPc1	čtvrtina
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
české	český	k2eAgFnSc3d1	Česká
národnosti	národnost	k1gFnSc3	národnost
<g/>
.	.	kIx.	.
</s>
<s>
Devatenáct	devatenáct	k4xCc1	devatenáct
bylo	být	k5eAaImAgNnS	být
Slováků	Slovák	k1gMnPc2	Slovák
<g/>
,	,	kIx,	,
šestnáct	šestnáct	k4xCc1	šestnáct
Moravanů	Moravan	k1gMnPc2	Moravan
<g/>
,	,	kIx,	,
devět	devět	k4xCc1	devět
Vietnamců	Vietnamec	k1gMnPc2	Vietnamec
<g/>
,	,	kIx,	,
dva	dva	k4xCgMnPc1	dva
Němci	Němec	k1gMnPc1	Němec
<g/>
,	,	kIx,	,
dva	dva	k4xCgMnPc1	dva
Ukrajinci	Ukrajinec	k1gMnPc1	Ukrajinec
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
Polák	Polák	k1gMnSc1	Polák
<g/>
.	.	kIx.	.
</s>
<s>
Celých	celý	k2eAgMnPc2d1	celý
966	[number]	k4	966
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
však	však	k8xC	však
svou	svůj	k3xOyFgFnSc4	svůj
národnost	národnost	k1gFnSc4	národnost
neuvedlo	uvést	k5eNaPmAgNnS	uvést
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Přibyslav	Přibyslav	k1gFnSc1	Přibyslav
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
farnosti	farnost	k1gFnSc2	farnost
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
součástí	součást	k1gFnSc7	součást
Havlíčkobrodského	havlíčkobrodský	k2eAgInSc2d1	havlíčkobrodský
vikariátu	vikariát	k1gInSc2	vikariát
královéhradecké	královéhradecký	k2eAgFnSc2d1	Královéhradecká
diecéze	diecéze	k1gFnSc2	diecéze
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
provincii	provincie	k1gFnSc6	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
zdejší	zdejší	k2eAgFnSc2d1	zdejší
farnosti	farnost	k1gFnSc2	farnost
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
nejen	nejen	k6eAd1	nejen
oblast	oblast	k1gFnSc4	oblast
samotného	samotný	k2eAgNnSc2d1	samotné
města	město	k1gNnSc2	město
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
obcí	obec	k1gFnPc2	obec
Modlíkov	Modlíkov	k1gInSc1	Modlíkov
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgInPc1d1	Nové
Dvory	Dvůr	k1gInPc1	Dvůr
<g/>
,	,	kIx,	,
Olešenka	Olešenka	k1gFnSc1	Olešenka
a	a	k8xC	a
Žižkovo	Žižkův	k2eAgNnSc1d1	Žižkovo
Pole	pole	k1gNnSc1	pole
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
duchovním	duchovní	k2eAgInSc7d1	duchovní
stánkem	stánek	k1gInSc7	stánek
farnosti	farnost	k1gFnSc2	farnost
je	být	k5eAaImIp3nS	být
Kostel	kostel	k1gInSc4	kostel
Narození	narození	k1gNnSc2	narození
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
v	v	k7c6	v
Přibyslavi	Přibyslav	k1gFnSc6	Přibyslav
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
kostela	kostel	k1gInSc2	kostel
ještě	ještě	k9	ještě
farnost	farnost	k1gFnSc1	farnost
spravuje	spravovat	k5eAaImIp3nS	spravovat
kostely	kostel	k1gInPc4	kostel
a	a	k8xC	a
kapličky	kaplička	k1gFnPc4	kaplička
<g/>
:	:	kIx,	:
Zmrtvýchvstání	zmrtvýchvstání	k1gNnSc1	zmrtvýchvstání
Páně	páně	k2eAgNnSc1d1	páně
(	(	kIx(	(
<g/>
Česká	český	k2eAgFnSc1d1	Česká
Jablonná	Jablonná	k1gFnSc1	Jablonná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nanebevzetí	nanebevzetí	k1gNnSc1	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
(	(	kIx(	(
<g/>
Nové	Nové	k2eAgInPc1d1	Nové
Dvory	Dvůr	k1gInPc1	Dvůr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
svaté	svatý	k2eAgFnSc2d1	svatá
Rozálie	Rozálie	k1gFnSc2	Rozálie
(	(	kIx(	(
<g/>
Olešenka	Olešenka	k1gFnSc1	Olešenka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
svaté	svatý	k2eAgFnSc2d1	svatá
Kateřiny	Kateřina	k1gFnSc2	Kateřina
Alexandrijské	alexandrijský	k2eAgFnSc2d1	Alexandrijská
(	(	kIx(	(
<g/>
Stříbrné	stříbrný	k2eAgFnSc2d1	stříbrná
Hory	hora	k1gFnSc2	hora
<g/>
)	)	kIx)	)
a	a	k8xC	a
svatého	svatý	k2eAgMnSc2d1	svatý
Michaela	Michael	k1gMnSc2	Michael
archanděla	archanděl	k1gMnSc2	archanděl
(	(	kIx(	(
<g/>
Žižkovo	Žižkův	k2eAgNnSc1d1	Žižkovo
Pole	pole	k1gNnSc1	pole
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Místním	místní	k2eAgMnSc7d1	místní
farářem	farář	k1gMnSc7	farář
a	a	k8xC	a
duchovním	duchovní	k2eAgMnSc7d1	duchovní
správcem	správce	k1gMnSc7	správce
farnosti	farnost	k1gFnSc2	farnost
je	být	k5eAaImIp3nS	být
Mgr.	Mgr.	kA	Mgr.
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Kubeš	Kubeš	k1gMnSc1	Kubeš
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
censu	census	k1gInSc6	census
prováděném	prováděný	k2eAgInSc6d1	prováděný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	s	k7c7	s
1	[number]	k4	1
293	[number]	k4	293
obyvatel	obyvatel	k1gMnSc1	obyvatel
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
33	[number]	k4	33
%	%	kIx~	%
<g/>
)	)	kIx)	)
označilo	označit	k5eAaPmAgNnS	označit
za	za	k7c4	za
věřící	věřící	k1gFnSc4	věřící
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
počtu	počet	k1gInSc2	počet
se	se	k3xPyFc4	se
1	[number]	k4	1
039	[number]	k4	039
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
církvi	církev	k1gFnSc3	církev
či	či	k8xC	či
náboženské	náboženský	k2eAgFnSc3d1	náboženská
obci	obec	k1gFnSc3	obec
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
římskokatolické	římskokatolický	k2eAgFnSc3d1	Římskokatolická
církvi	církev	k1gFnSc3	církev
se	se	k3xPyFc4	se
přihlásila	přihlásit	k5eAaPmAgFnS	přihlásit
největší	veliký	k2eAgFnSc1d3	veliký
část	část	k1gFnSc1	část
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
933	[number]	k4	933
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
24	[number]	k4	24
%	%	kIx~	%
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgFnPc2d1	další
10	[number]	k4	10
osob	osoba	k1gFnPc2	osoba
se	se	k3xPyFc4	se
hlásil	hlásit	k5eAaImAgMnS	hlásit
k	k	k7c3	k
Církvi	církev	k1gFnSc3	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
<g/>
,	,	kIx,	,
stejný	stejný	k2eAgInSc4d1	stejný
počet	počet	k1gInSc4	počet
k	k	k7c3	k
Českobratrské	českobratrský	k2eAgFnSc3d1	Českobratrská
církvi	církev	k1gFnSc3	církev
evangelické	evangelický	k2eAgFnPc1d1	evangelická
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgMnPc1	tři
občané	občan	k1gMnPc1	občan
se	se	k3xPyFc4	se
přihlásili	přihlásit	k5eAaPmAgMnP	přihlásit
k	k	k7c3	k
Pravoslavné	pravoslavný	k2eAgFnSc3d1	pravoslavná
církvi	církev	k1gFnSc3	církev
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
k	k	k7c3	k
Náboženské	náboženský	k2eAgFnSc3d1	náboženská
společnosti	společnost	k1gFnSc3	společnost
Svědkové	svědek	k1gMnPc1	svědek
Jehovovi	Jehovův	k2eAgMnPc1d1	Jehovův
<g/>
.	.	kIx.	.
</s>
<s>
Úhrnem	úhrnem	k6eAd1	úhrnem
876	[number]	k4	876
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
označilo	označit	k5eAaPmAgNnS	označit
bez	bez	k7c2	bez
náboženské	náboženský	k2eAgFnSc2d1	náboženská
víry	víra	k1gFnSc2	víra
a	a	k8xC	a
1	[number]	k4	1
695	[number]	k4	695
lidí	člověk	k1gMnPc2	člověk
odmítlo	odmítnout	k5eAaPmAgNnS	odmítnout
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
své	svůj	k3xOyFgFnSc2	svůj
náboženské	náboženský	k2eAgFnSc2d1	náboženská
víry	víra	k1gFnSc2	víra
odpovědět	odpovědět	k5eAaPmF	odpovědět
<g/>
.	.	kIx.	.
</s>
<s>
Přibyslav	Přibyslav	k1gFnSc1	Přibyslav
je	být	k5eAaImIp3nS	být
obec	obec	k1gFnSc1	obec
s	s	k7c7	s
pověřeným	pověřený	k2eAgInSc7d1	pověřený
obecním	obecní	k2eAgInSc7d1	obecní
úřadem	úřad	k1gInSc7	úřad
(	(	kIx(	(
<g/>
POÚ	POÚ	kA	POÚ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Správu	správa	k1gFnSc4	správa
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
pro	pro	k7c4	pro
obce	obec	k1gFnPc4	obec
Dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
Ves	ves	k1gFnSc1	ves
<g/>
,	,	kIx,	,
Havlíčkova	Havlíčkův	k2eAgFnSc1d1	Havlíčkova
Borová	borový	k2eAgFnSc1d1	Borová
<g/>
,	,	kIx,	,
Modlíkov	Modlíkov	k1gInSc1	Modlíkov
<g/>
,	,	kIx,	,
Olešenka	Olešenka	k1gFnSc1	Olešenka
<g/>
,	,	kIx,	,
Přibyslav	Přibyslav	k1gFnSc1	Přibyslav
<g/>
,	,	kIx,	,
Stříbrné	stříbrný	k2eAgFnPc1d1	stříbrná
Hory	hora	k1gFnPc1	hora
a	a	k8xC	a
Žižkovo	Žižkův	k2eAgNnSc1d1	Žižkovo
Pole	pole	k1gNnSc1	pole
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1850	[number]	k4	1850
a	a	k8xC	a
1949	[number]	k4	1949
byla	být	k5eAaImAgFnS	být
Přibyslav	Přibyslav	k1gFnSc4	Přibyslav
okresním	okresní	k2eAgNnSc7d1	okresní
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
okresu	okres	k1gInSc2	okres
Žďár	Žďár	k1gInSc4	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
osmi	osm	k4xCc2	osm
místními	místní	k2eAgFnPc7d1	místní
částmi	část	k1gFnPc7	část
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
pojmenovanými	pojmenovaný	k2eAgFnPc7d1	pojmenovaná
katastrálními	katastrální	k2eAgNnPc7d1	katastrální
územími	území	k1gNnPc7	území
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
Českou	český	k2eAgFnSc7d1	Česká
Jablonnou	Jablonná	k1gFnSc7	Jablonná
(	(	kIx(	(
<g/>
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
města	město	k1gNnSc2	město
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dobrou	dobrá	k1gFnSc4	dobrá
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
součástí	součást	k1gFnSc7	součást
města	město	k1gNnSc2	město
počínaje	počínaje	k7c7	počínaje
rokem	rok	k1gInSc7	rok
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dolní	dolní	k2eAgFnSc4d1	dolní
Jablonnou	Jablonná	k1gFnSc4	Jablonná
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Německá	německý	k2eAgFnSc1d1	německá
Jablonná	Jablonná	k1gFnSc1	Jablonná
<g/>
,	,	kIx,	,
součástí	součást	k1gFnSc7	součást
Přibyslavi	Přibyslav	k1gFnSc2	Přibyslav
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hřištěm	hřiště	k1gNnSc7	hřiště
(	(	kIx(	(
<g/>
částí	část	k1gFnPc2	část
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Poříčím	Poříčí	k1gNnSc7	Poříčí
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Přibyslavi	Přibyslav	k1gFnSc2	Přibyslav
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Přibyslaví	Přibyslav	k1gFnSc7	Přibyslav
<g/>
,	,	kIx,	,
Ronovem	Ronovo	k1gNnSc7	Ronovo
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
(	(	kIx(	(
<g/>
spadá	spadat	k5eAaImIp3nS	spadat
pod	pod	k7c4	pod
město	město	k1gNnSc4	město
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
a	a	k8xC	a
Utínem	Utín	k1gMnSc7	Utín
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Přibyslavi	Přibyslav	k1gFnSc2	Přibyslav
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1989	[number]	k4	1989
a	a	k8xC	a
1990	[number]	k4	1990
patřily	patřit	k5eAaImAgFnP	patřit
k	k	k7c3	k
Přibyslavi	Přibyslav	k1gFnSc3	Přibyslav
ještě	ještě	k9	ještě
také	také	k9	také
Stříbrné	stříbrný	k2eAgFnSc2d1	stříbrná
Hory	hora	k1gFnSc2	hora
ve	v	k7c6	v
stejném	stejný	k2eAgNnSc6d1	stejné
časovém	časový	k2eAgNnSc6d1	časové
období	období	k1gNnSc6	období
také	také	k9	také
Modlíkov	Modlíkov	k1gInSc4	Modlíkov
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
tak	tak	k6eAd1	tak
tvoří	tvořit	k5eAaImIp3nS	tvořit
11	[number]	k4	11
samostatných	samostatný	k2eAgFnPc2d1	samostatná
základních	základní	k2eAgFnPc2d1	základní
sídelních	sídelní	k2eAgFnPc2d1	sídelní
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgNnPc7	jenž
jsou	být	k5eAaImIp3nP	být
krom	krom	k7c2	krom
výše	výše	k1gFnSc2	výše
jmenovaných	jmenovaný	k2eAgFnPc2d1	jmenovaná
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
ještě	ještě	k9	ještě
Dvorek	dvorek	k1gInSc1	dvorek
<g/>
,	,	kIx,	,
Hesov	Hesov	k1gInSc1	Hesov
a	a	k8xC	a
Keřkov	Keřkov	k1gInSc1	Keřkov
<g/>
.	.	kIx.	.
</s>
<s>
Přibyslav	Přibyslav	k1gFnSc4	Přibyslav
má	mít	k5eAaImIp3nS	mít
patnáctičlenné	patnáctičlenný	k2eAgNnSc1d1	patnáctičlenné
zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
a	a	k8xC	a
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
čele	čelo	k1gNnSc6	čelo
je	být	k5eAaImIp3nS	být
starosta	starosta	k1gMnSc1	starosta
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
komunálních	komunální	k2eAgFnPc2d1	komunální
voleb	volba	k1gFnPc2	volba
konaných	konaný	k2eAgFnPc2d1	konaná
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
Jan	Jan	k1gMnSc1	Jan
Štefáček	Štefáček	k1gMnSc1	Štefáček
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
zástupcem	zástupce	k1gMnSc7	zástupce
je	být	k5eAaImIp3nS	být
Martin	Martin	k1gMnSc1	Martin
Kamarád	kamarád	k1gMnSc1	kamarád
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
volbách	volba	k1gFnPc6	volba
kandidovalo	kandidovat	k5eAaImAgNnS	kandidovat
do	do	k7c2	do
městského	městský	k2eAgNnSc2d1	Městské
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
celkem	celkem	k6eAd1	celkem
šest	šest	k4xCc4	šest
uskupení	uskupení	k1gNnPc2	uskupení
a	a	k8xC	a
každé	každý	k3xTgInPc1	každý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
získalo	získat	k5eAaPmAgNnS	získat
dle	dle	k7c2	dle
výsledků	výsledek	k1gInPc2	výsledek
hlasování	hlasování	k1gNnSc2	hlasování
alespoň	alespoň	k9	alespoň
jeden	jeden	k4xCgInSc4	jeden
mandát	mandát	k1gInSc4	mandát
<g/>
.	.	kIx.	.
</s>
<s>
Nejúspěšnější	úspěšný	k2eAgFnSc1d3	nejúspěšnější
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Občanská	občanský	k2eAgFnSc1d1	občanská
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
(	(	kIx(	(
<g/>
ODS	ODS	kA	ODS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
čtyři	čtyři	k4xCgInPc4	čtyři
mandáty	mandát	k1gInPc4	mandát
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
jednoho	jeden	k4xCgMnSc4	jeden
zastupitele	zastupitel	k1gMnSc4	zastupitel
méně	málo	k6eAd2	málo
získaly	získat	k5eAaPmAgFnP	získat
shodně	shodně	k6eAd1	shodně
Česká	český	k2eAgFnSc1d1	Česká
strana	strana	k1gFnSc1	strana
sociálně	sociálně	k6eAd1	sociálně
demokratická	demokratický	k2eAgFnSc1d1	demokratická
(	(	kIx(	(
<g/>
ČSSD	ČSSD	kA	ČSSD
<g/>
)	)	kIx)	)
a	a	k8xC	a
Nezávislí	závislý	k2eNgMnPc1d1	nezávislý
pro	pro	k7c4	pro
Přibyslav	Přibyslav	k1gFnSc4	Přibyslav
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgMnPc6	dva
zastupitelích	zastupitel	k1gMnPc6	zastupitel
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
z	z	k7c2	z
kandidátek	kandidátka	k1gFnPc2	kandidátka
Křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
a	a	k8xC	a
demokratické	demokratický	k2eAgFnSc2d1	demokratická
unie	unie	k1gFnSc2	unie
-	-	kIx~	-
Československé	československý	k2eAgFnSc2d1	Československá
strany	strana	k1gFnSc2	strana
lidové	lidový	k2eAgFnSc2d1	lidová
(	(	kIx(	(
<g/>
KDU-ČSL	KDU-ČSL	k1gFnSc2	KDU-ČSL
<g/>
)	)	kIx)	)
a	a	k8xC	a
sdružení	sdružení	k1gNnSc1	sdružení
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
kandidátů	kandidát	k1gMnPc2	kandidát
Přibyslav	Přibyslava	k1gFnPc2	Přibyslava
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc4d1	poslední
místo	místo	k1gNnSc4	místo
obsadil	obsadit	k5eAaPmAgMnS	obsadit
zástupce	zástupce	k1gMnSc1	zástupce
navržený	navržený	k2eAgMnSc1d1	navržený
(	(	kIx(	(
<g/>
KSČM	KSČM	kA	KSČM
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
komunálních	komunální	k2eAgFnPc2d1	komunální
voleb	volba	k1gFnPc2	volba
konaných	konaný	k2eAgFnPc2d1	konaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
preferovalo	preferovat	k5eAaImAgNnS	preferovat
z	z	k7c2	z
pěti	pět	k4xCc2	pět
uskupení	uskupení	k1gNnPc2	uskupení
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc1	jenž
tehdy	tehdy	k6eAd1	tehdy
kandidovala	kandidovat	k5eAaImAgFnS	kandidovat
<g/>
,	,	kIx,	,
nejvíce	nejvíce	k6eAd1	nejvíce
hlasujících	hlasující	k2eAgInPc2d1	hlasující
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
čtyři	čtyři	k4xCgMnPc4	čtyři
zastupitele	zastupitel	k1gMnPc4	zastupitel
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc4d1	další
tři	tři	k4xCgNnPc4	tři
místa	místo	k1gNnPc4	místo
v	v	k7c6	v
městském	městský	k2eAgNnSc6d1	Městské
zastupitelstvu	zastupitelstvo	k1gNnSc6	zastupitelstvo
připadly	připadnout	k5eAaPmAgFnP	připadnout
kandidujícím	kandidující	k2eAgInSc7d1	kandidující
za	za	k7c2	za
KDU-ČSL	KDU-ČSL	k1gFnPc2	KDU-ČSL
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Sdružení	sdružení	k1gNnSc1	sdružení
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
kandidátů	kandidát	k1gMnPc2	kandidát
a	a	k8xC	a
také	také	k6eAd1	také
společné	společný	k2eAgFnSc6d1	společná
kandidátce	kandidátka	k1gFnSc6	kandidátka
tří	tři	k4xCgNnPc2	tři
uskupení	uskupení	k1gNnPc2	uskupení
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
KSČM	KSČM	kA	KSČM
<g/>
,	,	kIx,	,
Strany	strana	k1gFnSc2	strana
demokratické	demokratický	k2eAgFnSc2d1	demokratická
levice	levice	k1gFnSc2	levice
(	(	kIx(	(
<g/>
SDL	SDL	kA	SDL
<g/>
)	)	kIx)	)
a	a	k8xC	a
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
kandidátů	kandidát	k1gMnPc2	kandidát
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInPc4d1	poslední
dva	dva	k4xCgInPc4	dva
mandáty	mandát	k1gInPc4	mandát
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
získala	získat	k5eAaPmAgFnS	získat
kandidátka	kandidátka	k1gFnSc1	kandidátka
společná	společný	k2eAgFnSc1d1	společná
čtyř	čtyři	k4xCgNnPc2	čtyři
subjektů	subjekt	k1gInPc2	subjekt
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
Liberálně	liberálně	k6eAd1	liberálně
sociální	sociální	k2eAgFnSc2d1	sociální
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
LSU	LSU	kA	LSU
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Strany	strana	k1gFnSc2	strana
zelených	zelená	k1gFnPc2	zelená
(	(	kIx(	(
<g/>
SZ	SZ	kA	SZ
<g/>
)	)	kIx)	)
a	a	k8xC	a
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
kandidátů	kandidát	k1gMnPc2	kandidát
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
konaných	konaný	k2eAgFnPc6d1	konaná
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
o	o	k7c4	o
hlasy	hlas	k1gInPc4	hlas
voličů	volič	k1gMnPc2	volič
ucházelo	ucházet	k5eAaImAgNnS	ucházet
sedm	sedm	k4xCc4	sedm
kandidátek	kandidátka	k1gFnPc2	kandidátka
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgMnPc1	tři
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
obsadily	obsadit	k5eAaPmAgFnP	obsadit
v	v	k7c6	v
zastupitelstvu	zastupitelstvo	k1gNnSc6	zastupitelstvo
po	po	k7c6	po
třech	tři	k4xCgMnPc6	tři
kandidátech	kandidát	k1gMnPc6	kandidát
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
to	ten	k3xDgNnSc1	ten
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
KDU-ČSL	KDU-ČSL	k1gFnSc1	KDU-ČSL
a	a	k8xC	a
ČSSD	ČSSD	kA	ČSSD
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc4d1	další
dvě	dva	k4xCgNnPc4	dva
místa	místo	k1gNnPc4	místo
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
získali	získat	k5eAaPmAgMnP	získat
kandidáté	kandidátý	k2eAgFnPc4d1	kandidátý
KSČM	KSČM	kA	KSČM
a	a	k8xC	a
zbývající	zbývající	k2eAgMnPc1d1	zbývající
čtyři	čtyři	k4xCgMnPc1	čtyři
místa	místo	k1gNnSc2	místo
si	se	k3xPyFc3	se
mezi	mezi	k7c4	mezi
sebe	sebe	k3xPyFc4	sebe
rozdělila	rozdělit	k5eAaPmAgNnP	rozdělit
tři	tři	k4xCgNnPc1	tři
sdružení	sdružení	k1gNnPc2	sdružení
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
kandidátů	kandidát	k1gMnPc2	kandidát
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
další	další	k2eAgInPc4d1	další
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
se	se	k3xPyFc4	se
voleb	volba	k1gFnPc2	volba
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
celkem	celkem	k6eAd1	celkem
osm	osm	k4xCc1	osm
kandidátek	kandidátka	k1gFnPc2	kandidátka
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
pět	pět	k4xCc4	pět
míst	místo	k1gNnPc2	místo
v	v	k7c6	v
zastupitelstvu	zastupitelstvo	k1gNnSc6	zastupitelstvo
obsadila	obsadit	k5eAaPmAgFnS	obsadit
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
další	další	k2eAgMnSc1d1	další
tři	tři	k4xCgFnPc1	tři
připadly	připadnout	k5eAaPmAgFnP	připadnout
KDU-ČSL	KDU-ČSL	k1gFnPc1	KDU-ČSL
<g/>
,	,	kIx,	,
po	po	k7c6	po
dvou	dva	k4xCgFnPc6	dva
kandidátkám	kandidátka	k1gFnPc3	kandidátka
ČSSD	ČSSD	kA	ČSSD
a	a	k8xC	a
KSČM	KSČM	kA	KSČM
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgMnPc1	tři
zbývající	zbývající	k2eAgMnPc1d1	zbývající
si	se	k3xPyFc3	se
po	po	k7c6	po
jednom	jeden	k4xCgMnSc6	jeden
zastupiteli	zastupitel	k1gMnSc6	zastupitel
mezi	mezi	k7c7	mezi
sebe	sebe	k3xPyFc4	sebe
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
tři	tři	k4xCgNnPc4	tři
sdružení	sdružení	k1gNnPc4	sdružení
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
kandidátů	kandidát	k1gMnPc2	kandidát
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgMnSc7	jeden
bylo	být	k5eAaImAgNnS	být
"	"	kIx"	"
<g/>
Sportovci	sportovec	k1gMnPc1	sportovec
pro	pro	k7c4	pro
Přibyslav	Přibyslav	k1gFnSc4	Přibyslav
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
dalším	další	k1gNnSc7	další
"	"	kIx"	"
<g/>
Náš	náš	k3xOp1gInSc1	náš
domov	domov	k1gInSc1	domov
<g/>
"	"	kIx"	"
a	a	k8xC	a
posledním	poslední	k2eAgMnSc6d1	poslední
"	"	kIx"	"
<g/>
Nezávislí	závislý	k2eNgMnPc1d1	nezávislý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
voleb	volba	k1gFnPc2	volba
do	do	k7c2	do
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
upřednostňovali	upřednostňovat	k5eAaImAgMnP	upřednostňovat
hlasující	hlasující	k1gFnSc4	hlasující
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
pěti	pět	k4xCc3	pět
kandidátek	kandidátka	k1gFnPc2	kandidátka
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
však	však	k9	však
podle	podle	k7c2	podle
konečných	konečný	k2eAgInPc2d1	konečný
výsledků	výsledek	k1gInPc2	výsledek
voleb	volba	k1gFnPc2	volba
své	svůj	k3xOyFgFnSc6	svůj
zástupce	zástupce	k1gMnSc1	zástupce
do	do	k7c2	do
městského	městský	k2eAgNnSc2d1	Městské
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
dostala	dostat	k5eAaPmAgFnS	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
-	-	kIx~	-
čtyři	čtyři	k4xCgMnPc4	čtyři
-	-	kIx~	-
jich	on	k3xPp3gMnPc2	on
měla	mít	k5eAaImAgFnS	mít
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
následovaná	následovaný	k2eAgFnSc1d1	následovaná
třemi	tři	k4xCgNnPc7	tři
uskupeními	uskupení	k1gNnPc7	uskupení
reprezentovanými	reprezentovaný	k2eAgInPc7d1	reprezentovaný
třemi	tři	k4xCgMnPc7	tři
zástupci	zástupce	k1gMnPc7	zástupce
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgFnPc7	jenž
byly	být	k5eAaImAgFnP	být
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
KDU-ČSL	KDU-ČSL	k1gFnSc1	KDU-ČSL
a	a	k8xC	a
sdružení	sdružení	k1gNnSc1	sdružení
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
kandidátů	kandidát	k1gMnPc2	kandidát
"	"	kIx"	"
<g/>
Přibyslav	Přibyslav	k1gFnSc1	Přibyslav
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnPc4d1	poslední
dvě	dva	k4xCgNnPc4	dva
místa	místo	k1gNnPc4	místo
obsadili	obsadit	k5eAaPmAgMnP	obsadit
kandidáti	kandidát	k1gMnPc1	kandidát
nominovaní	nominovaný	k2eAgMnPc1d1	nominovaný
KSČM	KSČM	kA	KSČM
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
neúplný	úplný	k2eNgInSc1d1	neúplný
seznam	seznam	k1gInSc1	seznam
starostů	starosta	k1gMnPc2	starosta
města	město	k1gNnSc2	město
Přibyslav	Přibyslava	k1gFnPc2	Přibyslava
<g/>
:	:	kIx,	:
Karel	Karel	k1gMnSc1	Karel
Hesse	Hess	k1gMnSc2	Hess
(	(	kIx(	(
<g/>
1846	[number]	k4	1846
<g/>
-	-	kIx~	-
<g/>
1855	[number]	k4	1855
<g/>
)	)	kIx)	)
Augustin	Augustin	k1gMnSc1	Augustin
Bedřich	Bedřich	k1gMnSc1	Bedřich
Čepl	Čepl	k1gMnSc1	Čepl
(	(	kIx(	(
<g/>
1861	[number]	k4	1861
<g/>
-	-	kIx~	-
<g/>
1867	[number]	k4	1867
<g/>
)	)	kIx)	)
Jan	Jan	k1gMnSc1	Jan
Jelínek	Jelínek	k1gMnSc1	Jelínek
(	(	kIx(	(
<g/>
1867	[number]	k4	1867
<g/>
)	)	kIx)	)
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Skřivan	Skřivan	k1gMnSc1	Skřivan
(	(	kIx(	(
<g/>
1868	[number]	k4	1868
<g/>
)	)	kIx)	)
Jindřich	Jindřich	k1gMnSc1	Jindřich
Smejkal	Smejkal	k1gMnSc1	Smejkal
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1868	[number]	k4	1868
<g/>
-	-	kIx~	-
<g/>
1874	[number]	k4	1874
<g/>
)	)	kIx)	)
Adolf	Adolf	k1gMnSc1	Adolf
Böhm	Böhm	k1gMnSc1	Böhm
(	(	kIx(	(
<g/>
1874	[number]	k4	1874
<g/>
-	-	kIx~	-
<g/>
1877	[number]	k4	1877
<g/>
)	)	kIx)	)
Augustin	Augustin	k1gMnSc1	Augustin
Bedřich	Bedřich	k1gMnSc1	Bedřich
Čepl	Čepl	k1gMnSc1	Čepl
(	(	kIx(	(
<g/>
1877	[number]	k4	1877
<g/>
-	-	kIx~	-
<g/>
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
František	František	k1gMnSc1	František
Niklfeld	Niklfeld	k1gMnSc1	Niklfeld
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
-	-	kIx~	-
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
Jan	Jan	k1gMnSc1	Jan
Bechyně	Bechyně	k1gMnSc1	Bechyně
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
-	-	kIx~	-
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Otta	Otta	k1gMnSc1	Otta
Klusáček	klusáček	k1gMnSc1	klusáček
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
-	-	kIx~	-
<g/>
)	)	kIx)	)
Mojmír	Mojmír	k1gMnSc1	Mojmír
Novotný	Novotný	k1gMnSc1	Novotný
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
-	-	kIx~	-
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Roman	Roman	k1gMnSc1	Roman
Podrázský	Podrázský	k2eAgMnSc1d1	Podrázský
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
-	-	kIx~	-
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Jan	Jan	k1gMnSc1	Jan
Štefáček	Štefáček	k1gMnSc1	Štefáček
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
-	-	kIx~	-
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Martin	Martin	k1gMnSc1	Martin
Kamarád	kamarád	k1gMnSc1	kamarád
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
-	-	kIx~	-
)	)	kIx)	)
Vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
městu	město	k1gNnSc3	město
Přibyslav	Přibyslav	k1gFnSc1	Přibyslav
předána	předán	k2eAgFnSc1d1	předána
Lubomírem	Lubomír	k1gMnSc7	Lubomír
Zaorálkem	Zaorálek	k1gMnSc7	Zaorálek
<g/>
,	,	kIx,	,
tehdejším	tehdejší	k2eAgMnSc7d1	tehdejší
předsedou	předseda	k1gMnSc7	předseda
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
číslo	číslo	k1gNnSc1	číslo
45	[number]	k4	45
ze	z	k7c2	z
dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
stran	strana	k1gFnPc2	strana
udělené	udělený	k2eAgFnSc2d1	udělená
vlajky	vlajka	k1gFnSc2	vlajka
je	být	k5eAaImIp3nS	být
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
list	list	k1gInSc1	list
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
žerďovým	žerďův	k2eAgInSc7d1	žerďův
pruhem	pruh	k1gInSc7	pruh
červené	červený	k2eAgFnSc2d1	červená
barvy	barva	k1gFnSc2	barva
o	o	k7c6	o
šířce	šířka	k1gFnSc6	šířka
tří	tři	k4xCgFnPc2	tři
sedmin	sedmina	k1gFnPc2	sedmina
délky	délka	k1gFnSc2	délka
listu	list	k1gInSc2	list
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
dvěma	dva	k4xCgInPc7	dva
vodorovnými	vodorovný	k2eAgInPc7d1	vodorovný
pruhy	pruh	k1gInPc7	pruh
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
modrým	modrý	k2eAgInSc7d1	modrý
a	a	k8xC	a
žlutým	žlutý	k2eAgInSc7d1	žlutý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červeném	červený	k2eAgInSc6d1	červený
pruhu	pruh	k1gInSc6	pruh
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
dva	dva	k4xCgInPc1	dva
zkřížené	zkřížený	k2eAgInPc1d1	zkřížený
bílé	bílý	k2eAgInPc1d1	bílý
vinařské	vinařský	k2eAgInPc1d1	vinařský
nože	nůž	k1gInPc1	nůž
se	s	k7c7	s
žlutými	žlutý	k2eAgFnPc7d1	žlutá
rukojeťmi	rukojeť	k1gFnPc7	rukojeť
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
nože	nůž	k1gInPc1	nůž
jsou	být	k5eAaImIp3nP	být
převýšené	převýšený	k2eAgInPc1d1	převýšený
červenou	červený	k2eAgFnSc7d1	červená
knížecí	knížecí	k2eAgFnSc7d1	knížecí
korunou	koruna	k1gFnSc7	koruna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poli	pole	k1gNnSc6	pole
modré	modrý	k2eAgFnSc2d1	modrá
barvy	barva	k1gFnSc2	barva
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
tři	tři	k4xCgInPc1	tři
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
sebou	se	k3xPyFc7	se
umístěné	umístěný	k2eAgNnSc4d1	umístěné
šedé	šedý	k2eAgFnPc4d1	šedá
ryby	ryba	k1gFnPc4	ryba
mající	mající	k2eAgFnSc2d1	mající
červené	červený	k2eAgFnSc2d1	červená
ploutve	ploutev	k1gFnSc2	ploutev
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
žlutém	žlutý	k2eAgInSc6d1	žlutý
pruhu	pruh	k1gInSc6	pruh
jsou	být	k5eAaImIp3nP	být
zkřížené	zkřížený	k2eAgFnPc4d1	zkřížená
černé	černý	k2eAgFnPc4d1	černá
ostrve	ostrve	k?	ostrve
<g/>
.	.	kIx.	.
</s>
<s>
Štít	štít	k1gInSc1	štít
je	být	k5eAaImIp3nS	být
dělený	dělený	k2eAgInSc1d1	dělený
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
horním	horní	k2eAgNnSc6d1	horní
červeném	červený	k2eAgNnSc6d1	červené
poli	pole	k1gNnSc6	pole
má	mít	k5eAaImIp3nS	mít
stejné	stejný	k2eAgInPc4d1	stejný
symboly	symbol	k1gInPc4	symbol
jako	jako	k8xC	jako
na	na	k7c6	na
znaku	znak	k1gInSc6	znak
<g/>
.	.	kIx.	.
</s>
<s>
Dolní	dolní	k2eAgFnSc1d1	dolní
polovina	polovina	k1gFnSc1	polovina
znaku	znak	k1gInSc2	znak
je	být	k5eAaImIp3nS	být
modro-zlatě	modrolatě	k6eAd1	modro-zlatě
polcena	poltit	k5eAaImNgFnS	poltit
<g/>
.	.	kIx.	.
</s>
<s>
Vpravo	vpravo	k6eAd1	vpravo
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
sebou	se	k3xPyFc7	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
tři	tři	k4xCgFnPc4	tři
ryby	ryba	k1gFnPc4	ryba
přirozené	přirozený	k2eAgFnSc2d1	přirozená
barvy	barva	k1gFnSc2	barva
a	a	k8xC	a
vlevo	vlevo	k6eAd1	vlevo
zkřížené	zkřížený	k2eAgFnPc4d1	zkřížená
ostrve	ostrve	k?	ostrve
černé	černý	k2eAgFnPc4d1	černá
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
konci	konec	k1gInSc6	konec
listopadu	listopad	k1gInSc2	listopad
2001	[number]	k4	2001
zakládajícím	zakládající	k2eAgInSc7d1	zakládající
členem	člen	k1gInSc7	člen
Svazku	svazek	k1gInSc2	svazek
obcí	obec	k1gFnPc2	obec
Přibyslavska	Přibyslavsko	k1gNnSc2	Přibyslavsko
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
má	mít	k5eAaImIp3nS	mít
sídlo	sídlo	k1gNnSc4	sídlo
právě	právě	k9	právě
v	v	k7c6	v
Přibyslavi	Přibyslav	k1gFnSc6	Přibyslav
a	a	k8xC	a
dbá	dbát	k5eAaImIp3nS	dbát
o	o	k7c4	o
rozvoj	rozvoj	k1gInSc4	rozvoj
zdejšího	zdejší	k2eAgInSc2d1	zdejší
regionu	region	k1gInSc2	region
ve	v	k7c6	v
vzájemné	vzájemný	k2eAgFnSc6d1	vzájemná
spolupráci	spolupráce	k1gFnSc6	spolupráce
členských	členský	k2eAgFnPc2d1	členská
obcí	obec	k1gFnPc2	obec
a	a	k8xC	a
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
též	též	k9	též
členem	člen	k1gMnSc7	člen
místní	místní	k2eAgFnSc2d1	místní
akční	akční	k2eAgFnSc2d1	akční
skupiny	skupina	k1gFnSc2	skupina
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
kraj	kraj	k1gInSc1	kraj
sídlící	sídlící	k2eAgInSc1d1	sídlící
ve	v	k7c6	v
Ždírci	Ždírec	k1gInSc6	Ždírec
nad	nad	k7c7	nad
Doubravou	Doubrava	k1gFnSc7	Doubrava
<g/>
.	.	kIx.	.
</s>
<s>
Přibyslav	Přibyslav	k1gFnSc1	Přibyslav
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
zemědělství	zemědělství	k1gNnSc1	zemědělství
<g/>
,	,	kIx,	,
v	v	k7c6	v
historických	historický	k2eAgFnPc6d1	historická
dobách	doba	k1gFnPc6	doba
orientovala	orientovat	k5eAaBmAgFnS	orientovat
spíše	spíše	k9	spíše
na	na	k7c4	na
živočišnou	živočišný	k2eAgFnSc4d1	živočišná
výrobu	výroba	k1gFnSc4	výroba
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
na	na	k7c4	na
rostlinnou	rostlinný	k2eAgFnSc4d1	rostlinná
výrobu	výroba	k1gFnSc4	výroba
neměli	mít	k5eNaImAgMnP	mít
sedláci	sedlák	k1gMnPc1	sedlák
dostatek	dostatek	k1gInSc4	dostatek
orné	orný	k2eAgFnSc2d1	orná
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
12	[number]	k4	12
dobytčích	dobytčí	k2eAgInPc2d1	dobytčí
trhů	trh	k1gInPc2	trh
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
dominoval	dominovat	k5eAaImAgInS	dominovat
hovězí	hovězí	k2eAgInSc4d1	hovězí
dobytek	dobytek	k1gInSc4	dobytek
<g/>
.	.	kIx.	.
</s>
<s>
Přibyslav	Přibyslav	k1gFnSc1	Přibyslav
si	se	k3xPyFc3	se
musela	muset	k5eAaImAgFnS	muset
půdu	půda	k1gFnSc4	půda
ve	v	k7c6	v
středověkých	středověký	k2eAgFnPc6d1	středověká
dobách	doba	k1gFnPc6	doba
dokupovat	dokupovat	k5eAaImF	dokupovat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
okolí	okolí	k1gNnSc6	okolí
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
škrobárenství	škrobárenství	k1gNnSc2	škrobárenství
k	k	k7c3	k
masivnímu	masivní	k2eAgNnSc3d1	masivní
pěstování	pěstování	k1gNnSc3	pěstování
brambor	brambora	k1gFnPc2	brambora
pod	pod	k7c7	pod
hlavičkou	hlavička	k1gFnSc7	hlavička
firmy	firma	k1gFnSc2	firma
Amylon	Amylon	k1gInSc1	Amylon
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
podnik	podnik	k1gInSc1	podnik
hospodařil	hospodařit	k5eAaImAgInS	hospodařit
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1937	[number]	k4	1937
na	na	k7c6	na
580	[number]	k4	580
hektarech	hektar	k1gInPc6	hektar
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
produkoval	produkovat	k5eAaImAgMnS	produkovat
také	také	k9	také
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
kravského	kravský	k2eAgNnSc2d1	kravské
mléka	mléko	k1gNnSc2	mléko
pro	pro	k7c4	pro
hesovské	hesovský	k2eAgFnPc4d1	hesovský
mlékárny	mlékárna	k1gFnPc4	mlékárna
<g/>
.	.	kIx.	.
</s>
<s>
Přibyslav	Přibyslav	k1gFnSc1	Přibyslav
byla	být	k5eAaImAgFnS	být
odjakživa	odjakživa	k6eAd1	odjakživa
spíše	spíše	k9	spíše
město	město	k1gNnSc1	město
orientované	orientovaný	k2eAgNnSc1d1	orientované
na	na	k7c4	na
lehký	lehký	k2eAgInSc4d1	lehký
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
devatenáctém	devatenáctý	k4xOgNnSc6	devatenáctý
století	století	k1gNnSc6	století
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
mnoho	mnoho	k4c4	mnoho
tkalců	tkadlec	k1gMnPc2	tkadlec
a	a	k8xC	a
soukeníků	soukeník	k1gMnPc2	soukeník
<g/>
,	,	kIx,	,
v	v	k7c6	v
Ronově	Ronův	k2eAgNnSc6d1	Ronovo
byla	být	k5eAaImAgFnS	být
papírna	papírna	k1gFnSc1	papírna
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
přestavěná	přestavěný	k2eAgFnSc1d1	přestavěná
na	na	k7c4	na
továrnu	továrna	k1gFnSc4	továrna
na	na	k7c4	na
škrob	škrob	k1gInSc4	škrob
a	a	k8xC	a
cukr	cukr	k1gInSc4	cukr
-	-	kIx~	-
předchůdce	předchůdce	k1gMnSc1	předchůdce
podniku	podnik	k1gInSc2	podnik
Amylon	Amylon	k1gInSc1	Amylon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Amylon	Amylon	k1gInSc1	Amylon
a.s.	a.s.	k?	a.s.
funguje	fungovat	k5eAaImIp3nS	fungovat
dodnes	dodnes	k6eAd1	dodnes
a	a	k8xC	a
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
např.	např.	kA	např.
pudinky	pudink	k1gInPc1	pudink
<g/>
,	,	kIx,	,
bramborové	bramborový	k2eAgInPc1d1	bramborový
prášky	prášek	k1gInPc1	prášek
<g/>
,	,	kIx,	,
škrobové	škrobový	k2eAgInPc1d1	škrobový
sirupy	sirup	k1gInPc1	sirup
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
produkty	produkt	k1gInPc1	produkt
škrobování	škrobování	k1gNnSc2	škrobování
brambor	brambora	k1gFnPc2	brambora
<g/>
.	.	kIx.	.
</s>
<s>
Vlajkovým	vlajkový	k2eAgInSc7d1	vlajkový
podnikem	podnik	k1gInSc7	podnik
Přibyslavi	Přibyslav	k1gFnSc2	Přibyslav
je	být	k5eAaImIp3nS	být
Pribina	Pribina	k1gFnSc1	Pribina
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
TPK	TPK	kA	TPK
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Mlékárenské	mlékárenský	k2eAgNnSc1d1	mlékárenské
a	a	k8xC	a
pastevní	pastevní	k2eAgNnSc1d1	pastevní
družstvo	družstvo	k1gNnSc1	družstvo
<g/>
"	"	kIx"	"
v	v	k7c6	v
Hesově	Hesov	k1gInSc6	Hesov
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
budov	budova	k1gFnPc2	budova
Amylonu	Amylon	k1gInSc2	Amylon
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
Specializoval	specializovat	k5eAaBmAgMnS	specializovat
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
na	na	k7c4	na
tvaroh	tvaroh	k1gInSc4	tvaroh
a	a	k8xC	a
sýry	sýr	k1gInPc4	sýr
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
"	"	kIx"	"
<g/>
přibyslavského	přibyslavský	k2eAgInSc2d1	přibyslavský
camembertu	camembert	k1gInSc2	camembert
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
za	za	k7c2	za
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
začaly	začít	k5eAaPmAgFnP	začít
vyrábět	vyrábět	k5eAaImF	vyrábět
i	i	k9	i
tavené	tavený	k2eAgInPc4d1	tavený
sýry	sýr	k1gInPc4	sýr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
byla	být	k5eAaImAgFnS	být
spuštěna	spuštěn	k2eAgFnSc1d1	spuštěna
produkce	produkce	k1gFnSc1	produkce
džemů	džem	k1gInPc2	džem
a	a	k8xC	a
džusu	džus	k1gInSc2	džus
Pribinka	pribinka	k1gFnSc1	pribinka
a	a	k8xC	a
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přejmenování	přejmenování	k1gNnSc3	přejmenování
společnosti	společnost	k1gFnSc2	společnost
na	na	k7c4	na
název	název	k1gInSc4	název
Pribina	Pribin	k2eAgFnSc1d1	Pribina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
Pribináček	pribináček	k1gInSc1	pribináček
<g/>
,	,	kIx,	,
vyráběný	vyráběný	k2eAgInSc1d1	vyráběný
tehdy	tehdy	k6eAd1	tehdy
v	v	k7c6	v
16	[number]	k4	16
příchutích	příchuť	k1gFnPc6	příchuť
a	a	k8xC	a
oblíbený	oblíbený	k2eAgMnSc1d1	oblíbený
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgInPc3d1	další
podnikům	podnik	k1gInPc3	podnik
patří	patřit	k5eAaImIp3nS	patřit
ACO	ACO	kA	ACO
<g/>
,	,	kIx,	,
kom	kdo	k3yRnSc6	kdo
<g/>
.	.	kIx.	.
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
systémové	systémový	k2eAgInPc1d1	systémový
prvky	prvek	k1gInPc1	prvek
(	(	kIx(	(
<g/>
bývalý	bývalý	k2eAgInSc1d1	bývalý
Agrostroj	agrostroj	k1gInSc1	agrostroj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dnes	dnes	k6eAd1	dnes
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
polymerický	polymerický	k2eAgInSc1d1	polymerický
beton	beton	k1gInSc1	beton
<g/>
,	,	kIx,	,
či	či	k8xC	či
OSIVA	osiva	k1gFnSc1	osiva
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g/>
;	;	kIx,	;
dřevozpracující	dřevozpracující	k2eAgInSc4d1	dřevozpracující
průmysl	průmysl	k1gInSc4	průmysl
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
Dřevovýroba	Dřevovýroba	k1gFnSc1	Dřevovýroba
Přibyslav	Přibyslav	k1gFnSc1	Přibyslav
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
a	a	k8xC	a
Lesy	les	k1gInPc1	les
Dřevo	dřevo	k1gNnSc1	dřevo
Obchod	obchod	k1gInSc1	obchod
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
;	;	kIx,	;
družstvo	družstvo	k1gNnSc1	družstvo
v	v	k7c6	v
Přibyslavi	Přibyslav	k1gFnSc6	Přibyslav
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgNnSc7d3	veliký
lesnickým	lesnický	k2eAgNnSc7d1	lesnické
družstvem	družstvo	k1gNnSc7	družstvo
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
Občanům	občan	k1gMnPc3	občan
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
pobočka	pobočka	k1gFnSc1	pobočka
České	český	k2eAgFnSc2d1	Česká
pošty	pošta	k1gFnSc2	pošta
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
sídlí	sídlet	k5eAaImIp3nS	sídlet
na	na	k7c6	na
Bechyňově	Bechyňův	k2eAgNnSc6d1	Bechyňovo
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
dohlíží	dohlížet	k5eAaImIp3nS	dohlížet
Policie	policie	k1gFnSc1	policie
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
svoje	svůj	k3xOyFgNnSc4	svůj
obvodní	obvodní	k2eAgNnSc4d1	obvodní
oddělení	oddělení	k1gNnSc4	oddělení
také	také	k9	také
na	na	k7c6	na
Bechyňově	Bechyňův	k2eAgNnSc6d1	Bechyňovo
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
domě	dům	k1gInSc6	dům
čp.	čp.	k?	čp.
255	[number]	k4	255
v	v	k7c6	v
Hasičské	hasičský	k2eAgFnSc6d1	hasičská
ulici	ulice	k1gFnSc6	ulice
má	mít	k5eAaImIp3nS	mít
svou	svůj	k3xOyFgFnSc4	svůj
zbrojnici	zbrojnice	k1gFnSc4	zbrojnice
místní	místní	k2eAgFnSc1d1	místní
jednotka	jednotka	k1gFnSc1	jednotka
dobrovolných	dobrovolný	k2eAgMnPc2d1	dobrovolný
hasičů	hasič	k1gMnPc2	hasič
<g/>
.	.	kIx.	.
</s>
<s>
Prodejny	prodejna	k1gFnPc1	prodejna
potravin	potravina	k1gFnPc2	potravina
mají	mít	k5eAaImIp3nP	mít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
obchodní	obchodní	k2eAgInPc1d1	obchodní
řetězce	řetězec	k1gInPc1	řetězec
Pramen	pramen	k1gInSc1	pramen
a	a	k8xC	a
COOP	COOP	kA	COOP
<g/>
-	-	kIx~	-
<g/>
TIP	tip	k1gInSc1	tip
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
bank	banka	k1gFnPc2	banka
působících	působící	k2eAgFnPc2d1	působící
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
Bechyňově	Bechyňův	k2eAgNnSc6d1	Bechyňovo
náměstí	náměstí	k1gNnSc6	náměstí
svoji	svůj	k3xOyFgFnSc4	svůj
expozituru	expozitura	k1gFnSc4	expozitura
Česká	český	k2eAgFnSc1d1	Česká
spořitelna	spořitelna	k1gFnSc1	spořitelna
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Havlíčkovým	Havlíčkův	k2eAgInSc7d1	Havlíčkův
Brodem	Brod	k1gInSc7	Brod
<g/>
,	,	kIx,	,
okresním	okresní	k2eAgNnSc7d1	okresní
městem	město	k1gNnSc7	město
ležícím	ležící	k2eAgInPc3d1	ležící
západně	západně	k6eAd1	západně
od	od	k7c2	od
Přibyslavi	Přibyslav	k1gFnSc2	Přibyslav
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
město	město	k1gNnSc4	město
silniční	silniční	k2eAgNnSc4d1	silniční
spojení	spojení	k1gNnSc4	spojení
silnicí	silnice	k1gFnSc7	silnice
I	i	k9	i
<g/>
/	/	kIx~	/
<g/>
19	[number]	k4	19
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
prochází	procházet	k5eAaImIp3nS	procházet
místními	místní	k2eAgFnPc7d1	místní
částmi	část	k1gFnPc7	část
Keřkov	Keřkov	k1gInSc1	Keřkov
<g/>
,	,	kIx,	,
Dobrá	dobrá	k1gFnSc1	dobrá
<g/>
,	,	kIx,	,
Přibyslav	Přibyslav	k1gFnSc1	Přibyslav
a	a	k8xC	a
Ronov	Ronov	k1gInSc1	Ronov
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
a	a	k8xC	a
pak	pak	k6eAd1	pak
dále	daleko	k6eAd2	daleko
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
východním	východní	k2eAgInSc7d1	východní
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
Žďáru	Žďár	k1gInSc2	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Přibyslavi	Přibyslav	k1gFnSc6	Přibyslav
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
komunikace	komunikace	k1gFnSc1	komunikace
vedena	vést	k5eAaImNgFnS	vést
ulicemi	ulice	k1gFnPc7	ulice
Tyršovou	Tyršová	k1gFnSc7	Tyršová
a	a	k8xC	a
Husovou	Husová	k1gFnSc7	Husová
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
silnici	silnice	k1gFnSc4	silnice
křižují	křižovat	k5eAaImIp3nP	křižovat
dvě	dva	k4xCgFnPc1	dva
komunikace	komunikace	k1gFnPc1	komunikace
druhé	druhý	k4xOgFnSc2	druhý
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jihu	jih	k1gInSc2	jih
na	na	k7c4	na
sever	sever	k1gInSc4	sever
(	(	kIx(	(
<g/>
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
staničení	staničení	k1gNnSc2	staničení
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
silnice	silnice	k1gFnSc1	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
350	[number]	k4	350
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
prochází	procházet	k5eAaImIp3nS	procházet
místní	místní	k2eAgFnSc7d1	místní
částí	část	k1gFnSc7	část
Dvorek	dvorek	k1gInSc1	dvorek
a	a	k8xC	a
přivádí	přivádět	k5eAaImIp3nS	přivádět
do	do	k7c2	do
města	město	k1gNnSc2	město
dopravu	doprava	k1gFnSc4	doprava
z	z	k7c2	z
jihozápadního	jihozápadní	k2eAgInSc2d1	jihozápadní
směru	směr	k1gInSc2	směr
od	od	k7c2	od
Šlapanova	Šlapanův	k2eAgInSc2d1	Šlapanův
a	a	k8xC	a
Štoků	štok	k1gInPc2	štok
<g/>
,	,	kIx,	,
na	na	k7c4	na
Bechyňové	Bechyňové	k2eAgNnSc4d1	Bechyňové
náměstí	náměstí	k1gNnSc4	náměstí
překříží	překřížit	k5eAaPmIp3nS	překřížit
krátkou	krátký	k2eAgFnSc7d1	krátká
peáží	peáž	k1gFnSc7	peáž
silnici	silnice	k1gFnSc4	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
19	[number]	k4	19
a	a	k8xC	a
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
dále	daleko	k6eAd2	daleko
severovýchodním	severovýchodní	k2eAgInSc7d1	severovýchodní
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Herálec	Herálec	k1gInSc4	Herálec
či	či	k8xC	či
Svratku	Svratka	k1gFnSc4	Svratka
Na	na	k7c6	na
území	území	k1gNnSc6	území
Přibyslavi	Přibyslav	k1gFnSc2	Přibyslav
prochází	procházet	k5eAaImIp3nS	procházet
silnice	silnice	k1gFnSc1	silnice
v	v	k7c6	v
popisovaném	popisovaný	k2eAgInSc6d1	popisovaný
směru	směr	k1gInSc6	směr
postupně	postupně	k6eAd1	postupně
ulicemi	ulice	k1gFnPc7	ulice
Rašínovou	Rašínový	k2eAgFnSc7d1	Rašínový
<g/>
,	,	kIx,	,
Žižkovou	Žižkův	k2eAgFnSc7d1	Žižkova
a	a	k8xC	a
za	za	k7c7	za
Bechyňovým	Bechyňův	k2eAgNnSc7d1	Bechyňovo
náměstím	náměstí	k1gNnSc7	náměstí
ulicí	ulice	k1gFnSc7	ulice
Havlíčkovou	Havlíčková	k1gFnSc7	Havlíčková
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
severního	severní	k2eAgInSc2d1	severní
směru	směr	k1gInSc2	směr
od	od	k7c2	od
Chotěboře	Chotěboř	k1gFnSc2	Chotěboř
a	a	k8xC	a
České	český	k2eAgFnSc2d1	Česká
Bělé	Bělá	k1gFnSc2	Bělá
přichází	přicházet	k5eAaImIp3nS	přicházet
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
části	část	k1gFnSc6	část
Dobrá	dobrý	k2eAgFnSc1d1	dobrá
na	na	k7c6	na
silnici	silnice	k1gFnSc6	silnice
I	i	k9	i
<g/>
/	/	kIx~	/
<g/>
19	[number]	k4	19
komunikace	komunikace	k1gFnSc2	komunikace
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
351	[number]	k4	351
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
dále	daleko	k6eAd2	daleko
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
části	část	k1gFnSc2	část
města	město	k1gNnSc2	město
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Polnou	Polná	k1gFnSc4	Polná
<g/>
.	.	kIx.	.
</s>
<s>
Motoristům	motorista	k1gMnPc3	motorista
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
čerpací	čerpací	k2eAgFnSc2d1	čerpací
stanice	stanice	k1gFnSc2	stanice
pohonných	pohonný	k2eAgFnPc2d1	pohonná
hmot	hmota	k1gFnPc2	hmota
společnosti	společnost	k1gFnSc2	společnost
Pasoil	Pasoila	k1gFnPc2	Pasoila
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
územím	území	k1gNnSc7	území
města	město	k1gNnSc2	město
prochází	procházet	k5eAaImIp3nS	procházet
několik	několik	k4yIc1	několik
silnic	silnice	k1gFnPc2	silnice
III	III	kA	III
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
1838	[number]	k4	1838
Přibyslav	Přibyslava	k1gFnPc2	Přibyslava
-	-	kIx~	-
Hřiště	hřiště	k1gNnSc1	hřiště
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
1840	[number]	k4	1840
Ronov	Ronovo	k1gNnPc2	Ronovo
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
-	-	kIx~	-
Hřiště	hřiště	k1gNnSc1	hřiště
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
3810	[number]	k4	3810
Dlouhá	Dlouhá	k1gFnSc1	Dlouhá
Ves	ves	k1gFnSc1	ves
-	-	kIx~	-
Utín	Utín	k1gInSc1	Utín
-	-	kIx~	-
Hesov	Hesov	k1gInSc1	Hesov
-	-	kIx~	-
Přibyslav	Přibyslav	k1gFnSc1	Přibyslav
III	III	kA	III
<g/>
/	/	kIx~	/
<g />
.	.	kIx.	.
</s>
<s>
<g/>
0	[number]	k4	0
<g/>
3818	[number]	k4	3818
Stříbrné	stříbrný	k2eAgFnSc2d1	stříbrná
Hory	hora	k1gFnSc2	hora
-	-	kIx~	-
Utín	Utín	k1gInSc1	Utín
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
3820	[number]	k4	3820
Keřkov	Keřkov	k1gInSc1	Keřkov
-	-	kIx~	-
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
3810	[number]	k4	3810
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
3505	[number]	k4	3505
ze	z	k7c2	z
silnice	silnice	k1gFnSc2	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
350	[number]	k4	350
z	z	k7c2	z
Dvorku	dvorek	k1gInSc2	dvorek
na	na	k7c4	na
Uhry	Uhry	k1gFnPc4	Uhry
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
3506	[number]	k4	3506
ze	z	k7c2	z
silnice	silnice	k1gFnSc2	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g />
.	.	kIx.	.
</s>
<s>
<g/>
350	[number]	k4	350
na	na	k7c4	na
Dolní	dolní	k2eAgFnSc4d1	dolní
Jablonnou	Jablonná	k1gFnSc4	Jablonná
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
3507	[number]	k4	3507
ze	z	k7c2	z
silnice	silnice	k1gFnSc2	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
350	[number]	k4	350
na	na	k7c4	na
Modlíkov	Modlíkov	k1gInSc4	Modlíkov
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
35012	[number]	k4	35012
Ronov	Ronov	k1gInSc1	Ronov
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
-	-	kIx~	-
Pořežín	Pořežín	k1gInSc1	Pořežín
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
3513	[number]	k4	3513
ze	z	k7c2	z
silnice	silnice	k1gFnSc2	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
351	[number]	k4	351
na	na	k7c4	na
Českou	český	k2eAgFnSc4d1	Česká
Jablonnou	Jablonná	k1gFnSc4	Jablonná
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
35211	[number]	k4	35211
ze	z	k7c2	z
<g />
.	.	kIx.	.
</s>
<s>
silnice	silnice	k1gFnSc1	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
19	[number]	k4	19
přes	přes	k7c4	přes
Poříčí	Poříčí	k1gNnSc4	Poříčí
na	na	k7c4	na
Olešenku	Olešenka	k1gFnSc4	Olešenka
Po	po	k7c6	po
silnici	silnice	k1gFnSc6	silnice
první	první	k4xOgFnSc2	první
třídy	třída	k1gFnSc2	třída
od	od	k7c2	od
Havlíčkova	Havlíčkův	k2eAgInSc2d1	Havlíčkův
Brodu	Brod	k1gInSc2	Brod
denně	denně	k6eAd1	denně
(	(	kIx(	(
<g/>
za	za	k7c4	za
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
<g/>
)	)	kIx)	)
na	na	k7c6	na
základě	základ	k1gInSc6	základ
výsledků	výsledek	k1gInPc2	výsledek
Celostátního	celostátní	k2eAgNnSc2d1	celostátní
sčítání	sčítání	k1gNnSc2	sčítání
dopravy	doprava	k1gFnSc2	doprava
provedeného	provedený	k2eAgInSc2d1	provedený
během	během	k7c2	během
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
a	a	k8xC	a
odjíždí	odjíždět	k5eAaImIp3nS	odjíždět
celkem	celkem	k6eAd1	celkem
2	[number]	k4	2
479	[number]	k4	479
vozidel	vozidlo	k1gNnPc2	vozidlo
ve	v	k7c6	v
skladbě	skladba	k1gFnSc6	skladba
1	[number]	k4	1
906	[number]	k4	906
osobních	osobní	k2eAgInPc2d1	osobní
a	a	k8xC	a
dodávkových	dodávkový	k2eAgInPc2d1	dodávkový
automobilů	automobil	k1gInPc2	automobil
<g/>
,	,	kIx,	,
522	[number]	k4	522
těžkých	těžký	k2eAgNnPc2d1	těžké
nákladních	nákladní	k2eAgNnPc2d1	nákladní
a	a	k8xC	a
51	[number]	k4	51
jednostopých	jednostopý	k2eAgNnPc2d1	jednostopé
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
mezi	mezi	k7c7	mezi
křižovatkami	křižovatka	k1gFnPc7	křižovatka
se	s	k7c7	s
silnicemi	silnice	k1gFnPc7	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
351	[number]	k4	351
a	a	k8xC	a
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
350	[number]	k4	350
na	na	k7c6	na
západě	západ	k1gInSc6	západ
města	město	k1gNnSc2	město
jezdí	jezdit	k5eAaImIp3nP	jezdit
denně	denně	k6eAd1	denně
úhrnem	úhrnem	k6eAd1	úhrnem
4	[number]	k4	4
137	[number]	k4	137
motorových	motorový	k2eAgNnPc2d1	motorové
vozidel	vozidlo	k1gNnPc2	vozidlo
ve	v	k7c6	v
skladbě	skladba	k1gFnSc6	skladba
3	[number]	k4	3
218	[number]	k4	218
vozidel	vozidlo	k1gNnPc2	vozidlo
osobních	osobní	k2eAgMnPc2d1	osobní
a	a	k8xC	a
dodávek	dodávka	k1gFnPc2	dodávka
<g/>
,	,	kIx,	,
858	[number]	k4	858
těžkých	těžký	k2eAgNnPc2d1	těžké
nákladních	nákladní	k2eAgNnPc2d1	nákladní
a	a	k8xC	a
61	[number]	k4	61
jednostopých	jednostopý	k2eAgFnPc2d1	jednostopá
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
křižovatky	křižovatka	k1gFnSc2	křižovatka
se	s	k7c7	s
silnicí	silnice	k1gFnSc7	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
350	[number]	k4	350
přes	přes	k7c4	přes
Bechyňovo	Bechyňův	k2eAgNnSc4d1	Bechyňovo
náměstí	náměstí	k1gNnSc4	náměstí
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
po	po	k7c6	po
Husově	Husův	k2eAgFnSc6d1	Husova
ulici	ulice	k1gFnSc6	ulice
kolem	kolem	k7c2	kolem
přibyslavského	přibyslavský	k2eAgInSc2d1	přibyslavský
zámku	zámek	k1gInSc2	zámek
až	až	k9	až
po	po	k7c4	po
křižovatku	křižovatka	k1gFnSc4	křižovatka
se	s	k7c7	s
silnicí	silnice	k1gFnSc7	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
351	[number]	k4	351
provede	provést	k5eAaPmIp3nS	provést
za	za	k7c4	za
celý	celý	k2eAgInSc4d1	celý
den	den	k1gInSc4	den
3	[number]	k4	3
983	[number]	k4	983
automobilů	automobil	k1gInPc2	automobil
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
je	být	k5eAaImIp3nS	být
3	[number]	k4	3
151	[number]	k4	151
osobních	osobní	k2eAgMnPc2d1	osobní
nebo	nebo	k8xC	nebo
dodávkových	dodávkový	k2eAgMnPc2d1	dodávkový
<g/>
,	,	kIx,	,
774	[number]	k4	774
těžkých	těžký	k2eAgNnPc2d1	těžké
nákladních	nákladní	k2eAgNnPc2d1	nákladní
a	a	k8xC	a
58	[number]	k4	58
jednostopých	jednostopý	k2eAgFnPc2d1	jednostopá
<g/>
.	.	kIx.	.
</s>
<s>
Směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Žďár	Žďár	k1gInSc4	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
pak	pak	k6eAd1	pak
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
2	[number]	k4	2
240	[number]	k4	240
automobilů	automobil	k1gInPc2	automobil
ve	v	k7c6	v
skladbě	skladba	k1gFnSc6	skladba
1	[number]	k4	1
804	[number]	k4	804
osobních	osobní	k2eAgNnPc2d1	osobní
nebo	nebo	k8xC	nebo
dodávkových	dodávkový	k2eAgNnPc2d1	dodávkové
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
,	,	kIx,	,
dalších	další	k2eAgNnPc2d1	další
422	[number]	k4	422
je	být	k5eAaImIp3nS	být
těžkých	těžký	k2eAgNnPc2d1	těžké
nákladních	nákladní	k2eAgNnPc2d1	nákladní
a	a	k8xC	a
zbylých	zbylý	k2eAgNnPc2d1	zbylé
14	[number]	k4	14
tvoří	tvořit	k5eAaImIp3nS	tvořit
jednostopá	jednostopý	k2eAgNnPc4d1	jednostopé
motorová	motorový	k2eAgNnPc4d1	motorové
vozidla	vozidlo	k1gNnPc4	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
komunikaci	komunikace	k1gFnSc6	komunikace
na	na	k7c4	na
Českou	český	k2eAgFnSc4d1	Česká
Bělou	Běla	k1gFnSc7	Běla
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
severním	severní	k2eAgInSc7d1	severní
směrem	směr	k1gInSc7	směr
po	po	k7c6	po
silnici	silnice	k1gFnSc6	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
351	[number]	k4	351
<g/>
,	,	kIx,	,
jezdí	jezdit	k5eAaImIp3nS	jezdit
dohromady	dohromady	k6eAd1	dohromady
505	[number]	k4	505
motorových	motorový	k2eAgNnPc2d1	motorové
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
největší	veliký	k2eAgFnSc7d3	veliký
část	část	k1gFnSc4	část
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
402	[number]	k4	402
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nP	tvořit
osobní	osobní	k2eAgNnPc1d1	osobní
a	a	k8xC	a
dodávková	dodávkový	k2eAgNnPc1d1	dodávkové
vozidla	vozidlo	k1gNnPc1	vozidlo
<g/>
,	,	kIx,	,
dalších	další	k2eAgNnPc2d1	další
83	[number]	k4	83
těžká	těžkat	k5eAaImIp3nS	těžkat
nákladní	nákladní	k2eAgFnSc1d1	nákladní
a	a	k8xC	a
20	[number]	k4	20
jednostopá	jednostopý	k2eAgNnPc4d1	jednostopé
motorová	motorový	k2eAgNnPc4d1	motorové
vozidla	vozidlo	k1gNnPc4	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Silnice	silnice	k1gFnSc1	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
350	[number]	k4	350
vedoucí	vedoucí	k1gMnSc1	vedoucí
jihozápadním	jihozápadní	k2eAgInSc7d1	jihozápadní
směrem	směr	k1gInSc7	směr
je	být	k5eAaImIp3nS	být
denně	denně	k6eAd1	denně
zatížena	zatížit	k5eAaPmNgFnS	zatížit
intenzitou	intenzita	k1gFnSc7	intenzita
619	[number]	k4	619
motorových	motorový	k2eAgNnPc2d1	motorové
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
tudy	tudy	k6eAd1	tudy
jezdí	jezdit	k5eAaImIp3nP	jezdit
ve	v	k7c6	v
skladbě	skladba	k1gFnSc6	skladba
484	[number]	k4	484
automobilů	automobil	k1gInPc2	automobil
osobních	osobní	k2eAgMnPc2d1	osobní
nebo	nebo	k8xC	nebo
dodávkových	dodávkový	k2eAgMnPc2d1	dodávkový
<g/>
,	,	kIx,	,
130	[number]	k4	130
těžkých	těžký	k2eAgNnPc2d1	těžké
nákladních	nákladní	k2eAgNnPc2d1	nákladní
a	a	k8xC	a
5	[number]	k4	5
jednostopých	jednostopý	k2eAgNnPc2d1	jednostopé
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Havlíčkovou	Havlíčkův	k2eAgFnSc7d1	Havlíčkova
ulicí	ulice	k1gFnSc7	ulice
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
po	po	k7c6	po
silnici	silnice	k1gFnSc6	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
350	[number]	k4	350
vedoucí	vedoucí	k1gFnPc4	vedoucí
k	k	k7c3	k
severovýchodu	severovýchod	k1gInSc3	severovýchod
denně	denně	k6eAd1	denně
projede	projet	k5eAaPmIp3nS	projet
448	[number]	k4	448
motorových	motorový	k2eAgNnPc2d1	motorové
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
309	[number]	k4	309
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jsou	být	k5eAaImIp3nP	být
osobní	osobní	k2eAgFnPc1d1	osobní
nebo	nebo	k8xC	nebo
dodávkové	dodávkový	k2eAgInPc1d1	dodávkový
automobily	automobil	k1gInPc1	automobil
<g/>
,	,	kIx,	,
127	[number]	k4	127
těžké	těžký	k2eAgInPc4d1	těžký
nákladní	nákladní	k2eAgInPc4d1	nákladní
a	a	k8xC	a
9	[number]	k4	9
je	být	k5eAaImIp3nS	být
motocyklů	motocykl	k1gInPc2	motocykl
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
silnici	silnice	k1gFnSc6	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
351	[number]	k4	351
odbočující	odbočující	k2eAgFnSc2d1	odbočující
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Přibyslavi	Přibyslav	k1gFnSc2	Přibyslav
ze	z	k7c2	z
silnice	silnice	k1gFnSc2	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
19	[number]	k4	19
projede	projet	k5eAaPmIp3nS	projet
za	za	k7c4	za
dvacet	dvacet	k4xCc4	dvacet
čtyři	čtyři	k4xCgInPc4	čtyři
hodin	hodina	k1gFnPc2	hodina
jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
celkově	celkově	k6eAd1	celkově
990	[number]	k4	990
motorových	motorový	k2eAgNnPc2d1	motorové
vozidel	vozidlo	k1gNnPc2	vozidlo
ve	v	k7c6	v
skladbě	skladba	k1gFnSc6	skladba
788	[number]	k4	788
osobních	osobní	k2eAgMnPc2d1	osobní
či	či	k8xC	či
dodávkových	dodávkový	k2eAgMnPc2d1	dodávkový
<g/>
,	,	kIx,	,
189	[number]	k4	189
těžkých	těžký	k2eAgNnPc2d1	těžké
nákladních	nákladní	k2eAgNnPc2d1	nákladní
a	a	k8xC	a
13	[number]	k4	13
jednostopých	jednostopý	k2eAgFnPc2d1	jednostopá
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
má	mít	k5eAaImIp3nS	mít
Přibyslav	Přibyslav	k1gFnSc4	Přibyslav
železniční	železniční	k2eAgNnSc1d1	železniční
spojení	spojení	k1gNnSc1	spojení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
provoz	provoz	k1gInSc1	provoz
po	po	k7c4	po
dnes	dnes	k6eAd1	dnes
opuštěné	opuštěný	k2eAgFnPc4d1	opuštěná
jednokolejné	jednokolejný	k2eAgFnPc4d1	jednokolejná
trati	trať	k1gFnPc4	trať
Německý	německý	k2eAgInSc1d1	německý
Brod	Brod	k1gInSc1	Brod
-	-	kIx~	-
Město	město	k1gNnSc1	město
Žďár	Žďár	k1gInSc1	Žďár
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
dvoukolejnou	dvoukolejný	k2eAgFnSc7d1	dvoukolejná
<g/>
,	,	kIx,	,
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
1966	[number]	k4	1966
elektrifikovanou	elektrifikovaný	k2eAgFnSc7d1	elektrifikovaná
tratí	trať	k1gFnSc7	trať
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
-	-	kIx~	-
Brno	Brno	k1gNnSc1	Brno
(	(	kIx(	(
<g/>
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
jízdním	jízdní	k2eAgInSc6d1	jízdní
řádu	řád	k1gInSc6	řád
pro	pro	k7c4	pro
cestující	cestující	k1gMnPc4	cestující
spojována	spojovat	k5eAaImNgFnS	spojovat
s	s	k7c7	s
úseky	úsek	k1gInPc7	úsek
až	až	k9	až
do	do	k7c2	do
slovenských	slovenský	k2eAgInPc2d1	slovenský
Kútů	Kút	k1gInPc2	Kút
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
město	město	k1gNnSc1	město
obchází	obcházet	k5eAaImIp3nS	obcházet
trať	trať	k1gFnSc4	trať
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
jižní	jižní	k2eAgFnSc6d1	jižní
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
straně	strana	k1gFnSc6	strana
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
u	u	k7c2	u
osady	osada	k1gFnSc2	osada
Hesov	Hesov	k1gInSc4	Hesov
zřízena	zřízen	k2eAgFnSc1d1	zřízena
Přibyslav	Přibyslav	k1gFnSc1	Přibyslav
zastávka	zastávka	k1gFnSc1	zastávka
a	a	k8xC	a
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
pak	pak	k6eAd1	pak
pětikolejná	pětikolejný	k2eAgFnSc1d1	pětikolejný
železniční	železniční	k2eAgFnSc1d1	železniční
stanice	stanice	k1gFnSc1	stanice
Přibyslav	Přibyslav	k1gFnSc1	Přibyslav
<g/>
.	.	kIx.	.
</s>
<s>
Autobusovou	autobusový	k2eAgFnSc4d1	autobusová
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
dopravu	doprava	k1gFnSc4	doprava
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2014	[number]	k4	2014
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
přímými	přímý	k2eAgFnPc7d1	přímá
linkami	linka	k1gFnPc7	linka
do	do	k7c2	do
Havlíčkova	Havlíčkův	k2eAgInSc2d1	Havlíčkův
Brodu	Brod	k1gInSc2	Brod
<g/>
,	,	kIx,	,
Chotěboře	Chotěboř	k1gFnSc2	Chotěboř
<g/>
,	,	kIx,	,
Šlapanova	Šlapanův	k2eAgFnSc1d1	Šlapanův
a	a	k8xC	a
Polné	Polné	k2eAgFnSc1d1	Polné
společnost	společnost	k1gFnSc1	společnost
Arriva	Arriv	k1gMnSc2	Arriv
Východní	východní	k2eAgFnPc1d1	východní
Čechy	Čechy	k1gFnPc1	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
Polnou	Polná	k1gFnSc4	Polná
až	až	k9	až
do	do	k7c2	do
Jihlavy	Jihlava	k1gFnSc2	Jihlava
jezdí	jezdit	k5eAaImIp3nP	jezdit
svou	svůj	k3xOyFgFnSc4	svůj
linku	linka	k1gFnSc4	linka
také	také	k9	také
společnost	společnost	k1gFnSc1	společnost
ICOM	ICOM	kA	ICOM
transport	transport	k1gInSc4	transport
a	a	k8xC	a
spojení	spojení	k1gNnSc4	spojení
se	s	k7c7	s
Žďárem	Žďár	k1gInSc7	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
firma	firma	k1gFnSc1	firma
ZDAR	zdar	k1gInSc1	zdar
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
celkem	celkem	k6eAd1	celkem
21	[number]	k4	21
autobusových	autobusový	k2eAgFnPc2d1	autobusová
zastávek	zastávka	k1gFnPc2	zastávka
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jízdního	jízdní	k2eAgInSc2d1	jízdní
řádu	řád	k1gInSc2	řád
2013	[number]	k4	2013
<g/>
/	/	kIx~	/
<g/>
2014	[number]	k4	2014
zastavují	zastavovat	k5eAaImIp3nP	zastavovat
v	v	k7c6	v
Přibyslavi	Přibyslav	k1gFnSc6	Přibyslav
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
v	v	k7c6	v
železniční	železniční	k2eAgFnSc6d1	železniční
stanici	stanice	k1gFnSc6	stanice
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
zastávce	zastávka	k1gFnSc6	zastávka
<g/>
,	,	kIx,	,
vlaky	vlak	k1gInPc1	vlak
jedoucí	jedoucí	k2eAgInPc1d1	jedoucí
do	do	k7c2	do
Žďáru	Žďár	k1gInSc2	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
nebo	nebo	k8xC	nebo
Havlíčkova	Havlíčkův	k2eAgInSc2d1	Havlíčkův
Brodu	Brod	k1gInSc2	Brod
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stanici	stanice	k1gFnSc6	stanice
navíc	navíc	k6eAd1	navíc
staví	stavit	k5eAaBmIp3nS	stavit
ještě	ještě	k9	ještě
také	také	k9	také
vlaky	vlak	k1gInPc1	vlak
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
cílovým	cílový	k2eAgNnSc7d1	cílové
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Brno	Brno	k1gNnSc1	Brno
či	či	k8xC	či
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Východně	východně	k6eAd1	východně
od	od	k7c2	od
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
531	[number]	k4	531
metrů	metr	k1gInPc2	metr
nachází	nacházet	k5eAaImIp3nS	nacházet
veřejné	veřejný	k2eAgNnSc4d1	veřejné
vnitrostátní	vnitrostátní	k2eAgNnSc4d1	vnitrostátní
letiště	letiště	k1gNnSc4	letiště
Přibyslav	Přibyslava	k1gFnPc2	Přibyslava
(	(	kIx(	(
<g/>
kód	kód	k1gInSc4	kód
ICAO	ICAO	kA	ICAO
<g/>
:	:	kIx,	:
LKPI	LKPI	kA	LKPI
<g/>
)	)	kIx)	)
s	s	k7c7	s
travnatou	travnatý	k2eAgFnSc7d1	travnatá
vzletovou	vzletový	k2eAgFnSc7d1	vzletová
a	a	k8xC	a
přistávací	přistávací	k2eAgFnSc7d1	přistávací
dráhou	dráha	k1gFnSc7	dráha
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
letiště	letiště	k1gNnSc4	letiště
pečuje	pečovat	k5eAaImIp3nS	pečovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
zdejší	zdejší	k2eAgInSc1d1	zdejší
Aeroklub	aeroklub	k1gInSc1	aeroklub
odbočka	odbočka	k1gFnSc1	odbočka
Českého	český	k2eAgInSc2d1	český
národního	národní	k2eAgInSc2d1	národní
aeroklubu	aeroklub	k1gInSc2	aeroklub
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
zdejšího	zdejší	k2eAgNnSc2d1	zdejší
letiště	letiště	k1gNnSc2	letiště
schválila	schválit	k5eAaPmAgFnS	schválit
městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
a	a	k8xC	a
dostavěno	dostavěn	k2eAgNnSc4d1	dostavěno
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zbudované	zbudovaný	k2eAgNnSc4d1	zbudované
jako	jako	k8xS	jako
pomocné	pomocný	k2eAgNnSc4d1	pomocné
letiště	letiště	k1gNnSc4	letiště
pro	pro	k7c4	pro
vojenské	vojenský	k2eAgInPc4d1	vojenský
dvouplošníky	dvouplošník	k1gInPc4	dvouplošník
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
Praha	Praha	k1gFnSc1	Praha
<g/>
-	-	kIx~	-
<g/>
Brno	Brno	k1gNnSc1	Brno
<g/>
-	-	kIx~	-
<g/>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
-	-	kIx~	-
<g/>
Záhřeb	Záhřeb	k1gInSc1	Záhřeb
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
dvě	dva	k4xCgFnPc1	dva
mateřské	mateřský	k2eAgFnPc1d1	mateřská
školy	škola	k1gFnPc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
původně	původně	k6eAd1	původně
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
odděleně	odděleně	k6eAd1	odděleně
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
jednu	jeden	k4xCgFnSc4	jeden
organizaci	organizace	k1gFnSc4	organizace
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
čele	čelo	k1gNnSc6	čelo
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2014	[number]	k4	2014
Mgr.	Mgr.	kA	Mgr.
Naděžda	Naděžda	k1gFnSc1	Naděžda
Štouračová	Štouračová	k1gFnSc1	Štouračová
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
objektu	objekt	k1gInSc2	objekt
v	v	k7c6	v
Bezručově	Bezručův	k2eAgFnSc6d1	Bezručova
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sídlí	sídlet	k5eAaImIp3nS	sídlet
ředitelství	ředitelství	k1gNnSc1	ředitelství
školky	školka	k1gFnSc2	školka
<g/>
,	,	kIx,	,
využívá	využívat	k5eAaPmIp3nS	využívat
mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
prostory	prostora	k1gFnSc2	prostora
v	v	k7c6	v
Tyršově	Tyršův	k2eAgFnSc6d1	Tyršova
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
školka	školka	k1gFnSc1	školka
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
Bezručově	Bezručův	k2eAgFnSc6d1	Bezručova
ulici	ulice	k1gFnSc6	ulice
až	až	k9	až
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
po	po	k7c4	po
sloučení	sloučení	k1gNnSc4	sloučení
obou	dva	k4xCgFnPc2	dva
školek	školka	k1gFnPc2	školka
(	(	kIx(	(
<g/>
během	během	k7c2	během
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
jejich	jejich	k3xOp3gMnSc1	jejich
zřizovatel	zřizovatel	k1gMnSc1	zřizovatel
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
Přibyslav	Přibyslava	k1gFnPc2	Přibyslava
<g/>
,	,	kIx,	,
o	o	k7c4	o
zrušení	zrušení	k1gNnSc4	zrušení
pracoviště	pracoviště	k1gNnSc2	pracoviště
v	v	k7c6	v
Tyršově	Tyršův	k2eAgFnSc6d1	Tyršova
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
však	však	k9	však
počet	počet	k1gInSc1	počet
malých	malý	k2eAgFnPc2d1	malá
dětí	dítě	k1gFnPc2	dítě
narůstal	narůstat	k5eAaImAgInS	narůstat
<g/>
,	,	kIx,	,
změnili	změnit	k5eAaPmAgMnP	změnit
zastupitelé	zastupitel	k1gMnPc1	zastupitel
města	město	k1gNnSc2	město
původní	původní	k2eAgNnSc4d1	původní
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
o	o	k7c4	o
uzavření	uzavření	k1gNnSc4	uzavření
a	a	k8xC	a
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
2012	[number]	k4	2012
jsou	být	k5eAaImIp3nP	být
opět	opět	k6eAd1	opět
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
obě	dva	k4xCgNnPc1	dva
pracoviště	pracoviště	k1gNnPc1	pracoviště
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
mateřské	mateřský	k2eAgFnSc6d1	mateřská
škole	škola	k1gFnSc6	škola
mohou	moct	k5eAaImIp3nP	moct
děti	dítě	k1gFnPc1	dítě
pokračovat	pokračovat	k5eAaImF	pokračovat
ve	v	k7c6	v
školní	školní	k2eAgFnSc6d1	školní
docházce	docházka	k1gFnSc6	docházka
v	v	k7c6	v
Přibyslavi	Přibyslav	k1gFnSc6	Přibyslav
i	i	k9	i
nadále	nadále	k6eAd1	nadále
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
na	na	k7c6	na
Bechyňově	Bechyňův	k2eAgNnSc6d1	Bechyňovo
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
devítiletá	devítiletý	k2eAgFnSc1d1	devítiletá
základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
ředitelem	ředitel	k1gMnSc7	ředitel
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Mgr.	Mgr.	kA	Mgr.
Petr	Petr	k1gMnSc1	Petr
Adam	Adam	k1gMnSc1	Adam
<g/>
.	.	kIx.	.
</s>
<s>
Hudebně	hudebně	k6eAd1	hudebně
nadané	nadaný	k2eAgFnPc1d1	nadaná
děti	dítě	k1gFnPc1	dítě
mají	mít	k5eAaImIp3nP	mít
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
svého	svůj	k3xOyFgInSc2	svůj
talentu	talent	k1gInSc2	talent
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
Přibyslavi	Přibyslav	k1gFnSc6	Přibyslav
pobočku	pobočka	k1gFnSc4	pobočka
hudebního	hudební	k2eAgInSc2d1	hudební
oboru	obor	k1gInSc2	obor
Základní	základní	k2eAgFnSc2d1	základní
umělecké	umělecký	k2eAgFnSc2d1	umělecká
školy	škola	k1gFnSc2	škola
J.	J.	kA	J.
V.	V.	kA	V.
Stamice	Stamika	k1gFnSc3	Stamika
z	z	k7c2	z
Havlíčkova	Havlíčkův	k2eAgInSc2d1	Havlíčkův
Brodu	Brod	k1gInSc2	Brod
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgFnSc1d1	místní
odbočka	odbočka	k1gFnSc1	odbočka
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
února	únor	k1gInSc2	únor
1984	[number]	k4	1984
a	a	k8xC	a
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
využívala	využívat	k5eAaPmAgFnS	využívat
pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
zkušebny	zkušebna	k1gFnPc4	zkušebna
prostory	prostora	k1gFnSc2	prostora
základní	základní	k2eAgFnSc2d1	základní
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
přesunula	přesunout	k5eAaPmAgFnS	přesunout
do	do	k7c2	do
objektu	objekt	k1gInSc2	objekt
čp.	čp.	k?	čp.
301	[number]	k4	301
v	v	k7c6	v
Husově	Husův	k2eAgFnSc6d1	Husova
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Vyučuje	vyučovat	k5eAaImIp3nS	vyučovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
hře	hra	k1gFnSc3	hra
na	na	k7c4	na
nástroje	nástroj	k1gInPc4	nástroj
dechové	dechový	k2eAgInPc4d1	dechový
i	i	k8xC	i
drnkací	drnkací	k2eAgInPc4d1	drnkací
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
škole	škola	k1gFnSc6	škola
existuje	existovat	k5eAaImIp3nS	existovat
houslový	houslový	k2eAgInSc4d1	houslový
soubor	soubor	k1gInSc4	soubor
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
při	při	k7c6	při
koncertech	koncert	k1gInPc6	koncert
například	například	k6eAd1	například
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Svěceného	svěcený	k2eAgMnSc2d1	svěcený
<g/>
,	,	kIx,	,
Pavla	Pavel	k1gMnSc2	Pavel
Šporcla	Šporcl	k1gMnSc2	Šporcl
<g/>
,	,	kIx,	,
Gabrielu	Gabriela	k1gFnSc4	Gabriela
Demeterovou	Demeterový	k2eAgFnSc4d1	Demeterová
či	či	k8xC	či
Václava	Václav	k1gMnSc2	Václav
Hudečka	Hudeček	k1gMnSc2	Hudeček
<g/>
.	.	kIx.	.
</s>
<s>
Kulturní	kulturní	k2eAgInPc1d1	kulturní
pořady	pořad	k1gInPc1	pořad
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
uskutečňují	uskutečňovat	k5eAaImIp3nP	uskutečňovat
v	v	k7c6	v
kulturním	kulturní	k2eAgInSc6d1	kulturní
domě	dům	k1gInSc6	dům
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
je	být	k5eAaImIp3nS	být
situován	situovat	k5eAaBmNgInS	situovat
v	v	k7c6	v
Husově	Husův	k2eAgFnSc6d1	Husova
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
budově	budova	k1gFnSc6	budova
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
sál	sál	k1gInSc1	sál
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
333	[number]	k4	333
míst	místo	k1gNnPc2	místo
k	k	k7c3	k
sezení	sezení	k1gNnSc3	sezení
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
probíhají	probíhat	k5eAaImIp3nP	probíhat
jak	jak	k6eAd1	jak
divadelní	divadelní	k2eAgNnPc1d1	divadelní
představení	představení	k1gNnPc1	představení
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
promítají	promítat	k5eAaImIp3nP	promítat
i	i	k9	i
filmy	film	k1gInPc1	film
<g/>
.	.	kIx.	.
</s>
<s>
Svá	svůj	k3xOyFgNnPc4	svůj
představení	představení	k1gNnPc4	představení
zde	zde	k6eAd1	zde
hraje	hrát	k5eAaImIp3nS	hrát
také	také	k9	také
amatérské	amatérský	k2eAgNnSc1d1	amatérské
Loutkové	loutkový	k2eAgNnSc1d1	loutkové
divadlo	divadlo	k1gNnSc1	divadlo
Přibyslav	Přibyslava	k1gFnPc2	Přibyslava
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
kinu	kino	k1gNnSc6	kino
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
během	během	k7c2	během
jara	jaro	k1gNnSc2	jaro
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
prošlo	projít	k5eAaPmAgNnS	projít
digitalizací	digitalizace	k1gFnSc7	digitalizace
<g/>
,	,	kIx,	,
funguje	fungovat	k5eAaImIp3nS	fungovat
filmový	filmový	k2eAgInSc1d1	filmový
klub	klub	k1gInSc1	klub
<g/>
.	.	kIx.	.
</s>
<s>
Čtenářům	čtenář	k1gMnPc3	čtenář
je	být	k5eAaImIp3nS	být
nepřetržitě	přetržitě	k6eNd1	přetržitě
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1896	[number]	k4	1896
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2011	[number]	k4	2011
disponovala	disponovat	k5eAaBmAgFnS	disponovat
celkem	celkem	k6eAd1	celkem
25	[number]	k4	25
900	[number]	k4	900
knihovními	knihovní	k2eAgFnPc7d1	knihovní
jednotkami	jednotka	k1gFnPc7	jednotka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
evidovány	evidovat	k5eAaImNgFnP	evidovat
v	v	k7c6	v
elektronickém	elektronický	k2eAgInSc6d1	elektronický
katalogu	katalog	k1gInSc6	katalog
LANius	LANius	k1gInSc1	LANius
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
je	být	k5eAaImIp3nS	být
návštěvníkům	návštěvník	k1gMnPc3	návštěvník
umožněn	umožněn	k2eAgInSc4d1	umožněn
přístup	přístup	k1gInSc4	přístup
na	na	k7c4	na
internet	internet	k1gInSc4	internet
<g/>
.	.	kIx.	.
</s>
<s>
Zájemci	zájemce	k1gMnPc1	zájemce
o	o	k7c6	o
historii	historie	k1gFnSc6	historie
města	město	k1gNnSc2	město
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
okolí	okolí	k1gNnSc4	okolí
mohou	moct	k5eAaImIp3nP	moct
navštívit	navštívit	k5eAaPmF	navštívit
místní	místní	k2eAgNnSc4d1	místní
Městské	městský	k2eAgNnSc4d1	Městské
muzeum	muzeum	k1gNnSc4	muzeum
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Kurfürstově	Kurfürstův	k2eAgInSc6d1	Kurfürstův
domě	dům	k1gInSc6	dům
na	na	k7c6	na
Bechyňově	Bechyňův	k2eAgNnSc6d1	Bechyňovo
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
objektu	objekt	k1gInSc6	objekt
sídlí	sídlet	k5eAaImIp3nS	sídlet
též	též	k9	též
informační	informační	k2eAgNnSc1d1	informační
středisko	středisko	k1gNnSc1	středisko
nabízející	nabízející	k2eAgFnSc2d1	nabízející
průvodcovské	průvodcovský	k2eAgFnSc2d1	průvodcovská
služby	služba	k1gFnSc2	služba
a	a	k8xC	a
prodávající	prodávající	k2eAgMnSc1d1	prodávající
též	též	k6eAd1	též
upomínkové	upomínkový	k2eAgInPc4d1	upomínkový
předměty	předmět	k1gInPc4	předmět
na	na	k7c4	na
návštěvu	návštěva	k1gFnSc4	návštěva
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Turisté	turist	k1gMnPc1	turist
zde	zde	k6eAd1	zde
mohou	moct	k5eAaImIp3nP	moct
zakoupit	zakoupit	k5eAaPmF	zakoupit
také	také	k9	také
publikace	publikace	k1gFnPc4	publikace
či	či	k8xC	či
turistické	turistický	k2eAgFnPc4d1	turistická
známky	známka	k1gFnPc4	známka
vztahující	vztahující	k2eAgFnPc4d1	vztahující
se	se	k3xPyFc4	se
k	k	k7c3	k
městu	město	k1gNnSc3	město
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Kulturním	kulturní	k2eAgNnSc7d1	kulturní
zařízením	zařízení	k1gNnSc7	zařízení
města	město	k1gNnSc2	město
Přibyslavi	Přibyslav	k1gFnSc2	Přibyslav
pořádá	pořádat	k5eAaImIp3nS	pořádat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
sportovní	sportovní	k2eAgFnSc6d1	sportovní
hale	hala	k1gFnSc6	hala
taneční	taneční	k2eAgFnSc4d1	taneční
soutěž	soutěž	k1gFnSc4	soutěž
"	"	kIx"	"
<g/>
Přibyslavský	přibyslavský	k2eAgInSc1d1	přibyslavský
pantoflíček	pantoflíček	k1gInSc1	pantoflíček
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Účastní	účastnit	k5eAaImIp3nP	účastnit
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc2	on
nejen	nejen	k6eAd1	nejen
oddíly	oddíl	k1gInPc4	oddíl
ze	z	k7c2	z
sousedních	sousední	k2eAgNnPc2d1	sousední
měst	město	k1gNnPc2	město
a	a	k8xC	a
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
třeba	třeba	k6eAd1	třeba
až	až	k9	až
z	z	k7c2	z
Ostravy	Ostrava	k1gFnSc2	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgFnSc1d1	místní
římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
farnost	farnost	k1gFnSc1	farnost
pořádá	pořádat	k5eAaImIp3nS	pořádat
hudební	hudební	k2eAgInSc4d1	hudební
festival	festival	k1gInSc4	festival
Přibyslavské	přibyslavský	k2eAgNnSc1d1	Přibyslavské
Nocturno	Nocturno	k1gNnSc1	Nocturno
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Přibyslav	Přibyslava	k1gFnPc2	Přibyslava
protínají	protínat	k5eAaImIp3nP	protínat
čtyři	čtyři	k4xCgFnPc1	čtyři
značené	značený	k2eAgFnPc1d1	značená
cyklotrasy	cyklotrasa	k1gFnPc1	cyklotrasa
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
dvě	dva	k4xCgFnPc1	dva
II	II	kA	II
<g/>
.	.	kIx.	.
třídy	třída	k1gFnPc4	třída
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Cyklotrasa	cyklotrasa	k1gFnSc1	cyklotrasa
číslo	číslo	k1gNnSc1	číslo
16	[number]	k4	16
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
Hlinsku	Hlinsko	k1gNnSc6	Hlinsko
a	a	k8xC	a
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
přes	přes	k7c4	přes
Jihlavu	Jihlava	k1gFnSc4	Jihlava
a	a	k8xC	a
Telč	Telč	k1gFnSc4	Telč
do	do	k7c2	do
Slavonic	Slavonice	k1gFnPc2	Slavonice
<g/>
.	.	kIx.	.
</s>
<s>
Cyklotrasa	cyklotrasa	k1gFnSc1	cyklotrasa
číslo	číslo	k1gNnSc1	číslo
19	[number]	k4	19
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
Lísku	líska	k1gFnSc4	líska
a	a	k8xC	a
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
dál	daleko	k6eAd2	daleko
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Nové	Nové	k2eAgNnSc4d1	Nové
Město	město	k1gNnSc4	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
přes	přes	k7c4	přes
Žďár	Žďár	k1gInSc4	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
a	a	k8xC	a
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
do	do	k7c2	do
Zruče	Zruč	k1gFnSc2	Zruč
<g/>
.	.	kIx.	.
</s>
<s>
Cyklotrasa	cyklotrasa	k1gFnSc1	cyklotrasa
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
třídy	třída	k1gFnPc1	třída
číslo	číslo	k1gNnSc1	číslo
4335	[number]	k4	4335
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
Dobroníně	Dobronína	k1gFnSc6	Dobronína
<g/>
,	,	kIx,	,
protíná	protínat	k5eAaImIp3nS	protínat
Šlapanov	Šlapanov	k1gInSc4	Šlapanov
a	a	k8xC	a
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
dál	daleko	k6eAd2	daleko
do	do	k7c2	do
Přibyslavi	Přibyslav	k1gFnSc2	Přibyslav
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
i	i	k9	i
končí	končit	k5eAaImIp3nS	končit
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
trasa	trasa	k1gFnSc1	trasa
číslo	číslo	k1gNnSc1	číslo
4122	[number]	k4	4122
začíná	začínat	k5eAaImIp3nS	začínat
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Přibyslav	Přibyslava	k1gFnPc2	Přibyslava
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
do	do	k7c2	do
Krucemburku	Krucemburk	k1gInSc2	Krucemburk
a	a	k8xC	a
dál	daleko	k6eAd2	daleko
do	do	k7c2	do
Dědové	děd	k1gMnPc1	děd
<g/>
,	,	kIx,	,
Vrbatova	Vrbatův	k2eAgInSc2d1	Vrbatův
Kostelece	Kosteleec	k1gInSc2	Kosteleec
<g/>
,	,	kIx,	,
Chrástu	chrást	k1gInSc2	chrást
<g/>
,	,	kIx,	,
Hrochova	Hrochův	k2eAgInSc2d1	Hrochův
Týnce	Týnec	k1gInSc2	Týnec
<g/>
,	,	kIx,	,
Dvakačovic	Dvakačovice	k1gFnPc2	Dvakačovice
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
v	v	k7c6	v
Dašicích	Dašice	k1gFnPc6	Dašice
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
města	město	k1gNnSc2	město
vede	vést	k5eAaImIp3nS	vést
též	též	k9	též
neočíslovaná	očíslovaný	k2eNgFnSc1d1	očíslovaný
značená	značený	k2eAgFnSc1d1	značená
Cyklostezka	cyklostezka	k1gFnSc1	cyklostezka
Přibyslav	Přibyslav	k1gFnSc1	Přibyslav
<g/>
-	-	kIx~	-
<g/>
Sázava	Sázava	k1gFnSc1	Sázava
po	po	k7c6	po
staré	starý	k2eAgFnSc6d1	stará
železniční	železniční	k2eAgFnSc6d1	železniční
trati	trať	k1gFnSc6	trať
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
stezka	stezka	k1gFnSc1	stezka
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
téměř	téměř	k6eAd1	téměř
9	[number]	k4	9
km	km	kA	km
a	a	k8xC	a
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
délce	délka	k1gFnSc6	délka
kopíruje	kopírovat	k5eAaImIp3nS	kopírovat
řeku	řeka	k1gFnSc4	řeka
Sázavu	Sázava	k1gFnSc4	Sázava
<g/>
.	.	kIx.	.
</s>
<s>
Stezka	stezka	k1gFnSc1	stezka
je	být	k5eAaImIp3nS	být
využívána	využívat	k5eAaPmNgFnS	využívat
mimo	mimo	k7c4	mimo
cyklistiku	cyklistika	k1gFnSc4	cyklistika
i	i	k9	i
na	na	k7c4	na
inline	inlinout	k5eAaPmIp3nS	inlinout
bruslení	bruslení	k1gNnSc2	bruslení
či	či	k8xC	či
pěší	pěší	k2eAgFnSc6d1	pěší
chůzi	chůze	k1gFnSc6	chůze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
období	období	k1gNnSc6	období
mívá	mívat	k5eAaImIp3nS	mívat
udržované	udržovaný	k2eAgFnPc4d1	udržovaná
stopy	stopa	k1gFnPc4	stopa
pro	pro	k7c4	pro
běžecké	běžecký	k2eAgNnSc4d1	běžecké
lyžování	lyžování	k1gNnSc4	lyžování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
otevřena	otevřít	k5eAaPmNgFnS	otevřít
pro	pro	k7c4	pro
pěší	pěší	k2eAgMnPc4d1	pěší
návštěvníky	návštěvník	k1gMnPc4	návštěvník
Naučná	naučný	k2eAgFnSc1d1	naučná
stezka	stezka	k1gFnSc1	stezka
Romana	Roman	k1gMnSc2	Roman
Podrázského	Podrázský	k2eAgMnSc2d1	Podrázský
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
akademickém	akademický	k2eAgMnSc6d1	akademický
sochaři	sochař	k1gMnSc6	sochař
Romanu	Roman	k1gMnSc6	Roman
Podrázském	Podrázský	k2eAgInSc6d1	Podrázský
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
je	být	k5eAaImIp3nS	být
cca	cca	kA	cca
2,5	[number]	k4	2,5
km	km	kA	km
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
12	[number]	k4	12
zastavení	zastavení	k1gNnPc2	zastavení
<g/>
.	.	kIx.	.
</s>
<s>
Trasa	trasa	k1gFnSc1	trasa
začíná	začínat	k5eAaImIp3nS	začínat
u	u	k7c2	u
vlakového	vlakový	k2eAgNnSc2d1	vlakové
nádraží	nádraží	k1gNnSc2	nádraží
v	v	k7c6	v
Přibyslavi	Přibyslav	k1gFnSc6	Přibyslav
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
Jablonského	jablonský	k2eAgInSc2d1	jablonský
žlabu	žlab	k1gInSc2	žlab
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Přibyslavi	Přibyslav	k1gFnSc6	Přibyslav
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
vyhlášky	vyhláška	k1gFnSc2	vyhláška
Krajského	krajský	k2eAgInSc2d1	krajský
národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
Východočeského	východočeský	k2eAgInSc2d1	východočeský
kraje	kraj	k1gInSc2	kraj
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1990	[number]	k4	1990
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
historické	historický	k2eAgNnSc1d1	historické
centrum	centrum	k1gNnSc1	centrum
Přibyslavi	Přibyslav	k1gFnSc2	Přibyslav
městskou	městský	k2eAgFnSc7d1	městská
památkovou	památkový	k2eAgFnSc7d1	památková
zónou	zóna	k1gFnSc7	zóna
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
místním	místní	k2eAgFnPc3d1	místní
pamětihodnostem	pamětihodnost	k1gFnPc3	pamětihodnost
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
Kostel	kostel	k1gInSc1	kostel
Narození	narození	k1gNnSc2	narození
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc4	Jan
Křtitele	křtitel	k1gMnSc4	křtitel
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
barokní	barokní	k2eAgInSc1d1	barokní
kostel	kostel	k1gInSc1	kostel
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1750	[number]	k4	1750
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
majitel	majitel	k1gMnSc1	majitel
panství	panství	k1gNnSc2	panství
Karel	Karel	k1gMnSc1	Karel
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
z	z	k7c2	z
Ditrichštejna	Ditrichštejn	k1gInSc2	Ditrichštejn
postavit	postavit	k5eAaPmF	postavit
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
původního	původní	k2eAgInSc2d1	původní
menšího	malý	k2eAgInSc2d2	menší
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
kámen	kámen	k1gInSc1	kámen
byl	být	k5eAaImAgInS	být
položen	položit	k5eAaPmNgInS	položit
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
a	a	k8xC	a
slavnostně	slavnostně	k6eAd1	slavnostně
vysvěcen	vysvěcen	k2eAgMnSc1d1	vysvěcen
byl	být	k5eAaImAgMnS	být
kostel	kostel	k1gInSc4	kostel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1753	[number]	k4	1753
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
ničivém	ničivý	k2eAgInSc6d1	ničivý
požáru	požár	k1gInSc6	požár
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1767	[number]	k4	1767
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
celá	celý	k2eAgFnSc1d1	celá
střecha	střecha	k1gFnSc1	střecha
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
byla	být	k5eAaImAgFnS	být
vztyčena	vztyčit	k5eAaPmNgFnS	vztyčit
ještě	ještě	k9	ještě
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nové	nový	k2eAgNnSc1d1	nové
vysvěcení	vysvěcení	k1gNnSc1	vysvěcení
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1853	[number]	k4	1853
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
kostela	kostel	k1gInSc2	kostel
býval	bývat	k5eAaImAgInS	bývat
hřbitov	hřbitov	k1gInSc1	hřbitov
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterého	který	k3yRgNnSc2	který
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgInP	dochovat
některé	některý	k3yIgInPc1	některý
pomníky	pomník	k1gInPc1	pomník
roztroušené	roztroušený	k2eAgInPc1d1	roztroušený
kolem	kolem	k7c2	kolem
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
levé	levý	k2eAgFnSc6d1	levá
straně	strana	k1gFnSc6	strana
od	od	k7c2	od
vstupu	vstup	k1gInSc2	vstup
do	do	k7c2	do
kostela	kostel	k1gInSc2	kostel
stojí	stát	k5eAaImIp3nS	stát
gotická	gotický	k2eAgFnSc1d1	gotická
věž	věž	k1gFnSc1	věž
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1497	[number]	k4	1497
<g/>
.	.	kIx.	.
</s>
<s>
Svého	svůj	k3xOyFgInSc2	svůj
času	čas	k1gInSc2	čas
věž	věž	k1gFnSc1	věž
patřila	patřit	k5eAaImAgFnS	patřit
k	k	k7c3	k
původnímu	původní	k2eAgNnSc3d1	původní
hradnímu	hradní	k2eAgNnSc3d1	hradní
opevnění	opevnění	k1gNnSc3	opevnění
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
byla	být	k5eAaImAgFnS	být
silně	silně	k6eAd1	silně
poškozena	poškodit	k5eAaPmNgFnS	poškodit
i	i	k9	i
s	s	k7c7	s
celým	celý	k2eAgNnSc7d1	celé
obranným	obranný	k2eAgNnSc7d1	obranné
opevněním	opevnění	k1gNnSc7	opevnění
<g/>
.	.	kIx.	.
</s>
<s>
Věž	věž	k1gFnSc1	věž
je	být	k5eAaImIp3nS	být
viditelnou	viditelný	k2eAgFnSc7d1	viditelná
dominantou	dominanta	k1gFnSc7	dominanta
města	město	k1gNnSc2	město
a	a	k8xC	a
bývá	bývat	k5eAaImIp3nS	bývat
příležitostně	příležitostně	k6eAd1	příležitostně
přístupná	přístupný	k2eAgFnSc1d1	přístupná
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc7d3	nejstarší
dochovanou	dochovaný	k2eAgFnSc7d1	dochovaná
památkou	památka	k1gFnSc7	památka
města	město	k1gNnSc2	město
Přibyslavi	Přibyslav	k1gFnPc4	Přibyslav
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
dobývání	dobývání	k1gNnSc2	dobývání
stříbrné	stříbrný	k2eAgFnSc2d1	stříbrná
rudy	ruda	k1gFnSc2	ruda
je	být	k5eAaImIp3nS	být
středověká	středověký	k2eAgFnSc1d1	středověká
štola	štola	k1gFnSc1	štola
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
i	i	k9	i
s	s	k7c7	s
odbočkami	odbočka	k1gFnPc7	odbočka
měří	měřit	k5eAaImIp3nS	měřit
118	[number]	k4	118
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Štola	štola	k1gFnSc1	štola
začíná	začínat	k5eAaImIp3nS	začínat
pod	pod	k7c7	pod
farou	fara	k1gFnSc7	fara
a	a	k8xC	a
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
80	[number]	k4	80
metrů	metr	k1gInPc2	metr
severním	severní	k2eAgInSc7d1	severní
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Neexistuje	existovat	k5eNaImIp3nS	existovat
žádný	žádný	k3yNgInSc4	žádný
dokument	dokument	k1gInSc4	dokument
o	o	k7c6	o
účelu	účel	k1gInSc6	účel
této	tento	k3xDgFnSc2	tento
štoly	štola	k1gFnSc2	štola
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pozůstatek	pozůstatek	k1gInSc1	pozůstatek
po	po	k7c6	po
těžbě	těžba	k1gFnSc6	těžba
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dnes	dnes	k6eAd1	dnes
spíše	spíše	k9	spíše
převládá	převládat	k5eAaImIp3nS	převládat
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
štola	štola	k1gFnSc1	štola
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
úniková	únikový	k2eAgFnSc1d1	úniková
cesta	cesta	k1gFnSc1	cesta
z	z	k7c2	z
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Starý	starý	k2eAgInSc1d1	starý
špitál	špitál	k1gInSc1	špitál
v	v	k7c6	v
Přibyslavi	Přibyslav	k1gFnSc6	Přibyslav
nechal	nechat	k5eAaPmAgMnS	nechat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1692	[number]	k4	1692
vystavět	vystavět	k5eAaPmF	vystavět
kníže	kníže	k1gMnSc1	kníže
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
z	z	k7c2	z
Ditrichštejna	Ditrichštejn	k1gInSc2	Ditrichštejn
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
přibyslavských	přibyslavský	k2eAgMnPc2d1	přibyslavský
konšelů	konšel	k1gMnPc2	konšel
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
po	po	k7c6	po
třicetileté	třicetiletý	k2eAgFnSc6d1	třicetiletá
válce	válka	k1gFnSc6	válka
jen	jen	k9	jen
s	s	k7c7	s
velkými	velký	k2eAgFnPc7d1	velká
obtížemi	obtíž	k1gFnPc7	obtíž
zvládalo	zvládat	k5eAaImAgNnS	zvládat
starat	starat	k5eAaImF	starat
o	o	k7c4	o
chudé	chudý	k1gMnPc4	chudý
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
válka	válka	k1gFnSc1	válka
připravila	připravit	k5eAaPmAgFnS	připravit
o	o	k7c4	o
domov	domov	k1gInSc4	domov
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc4	město
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
pro	pro	k7c4	pro
stavbu	stavba	k1gFnSc4	stavba
poskytlo	poskytnout	k5eAaPmAgNnS	poskytnout
pozemek	pozemek	k1gInSc4	pozemek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
štítě	štít	k1gInSc6	štít
špitálu	špitál	k1gInSc2	špitál
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
době	doba	k1gFnSc6	doba
chápán	chápat	k5eAaImNgMnS	chápat
spíše	spíše	k9	spíše
jako	jako	k9	jako
chudobinec	chudobinec	k1gInSc4	chudobinec
než	než	k8xS	než
jako	jako	k9	jako
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
dodnes	dodnes	k6eAd1	dodnes
zobrazen	zobrazit	k5eAaPmNgInS	zobrazit
znak	znak	k1gInSc1	znak
Ditrichštejnů	Ditrichštejn	k1gMnPc2	Ditrichštejn
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
funkci	funkce	k1gFnSc4	funkce
plnil	plnit	k5eAaImAgInS	plnit
zhruba	zhruba	k6eAd1	zhruba
250	[number]	k4	250
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nedávné	dávný	k2eNgFnSc6d1	nedávná
historii	historie	k1gFnSc6	historie
sídlo	sídlo	k1gNnSc1	sídlo
sloužilo	sloužit	k5eAaImAgNnS	sloužit
jako	jako	k9	jako
městské	městský	k2eAgNnSc1d1	Městské
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
z	z	k7c2	z
bývalého	bývalý	k2eAgInSc2d1	bývalý
špitálu	špitál	k1gInSc2	špitál
staly	stát	k5eAaPmAgInP	stát
prostory	prostor	k1gInPc1	prostor
sloužící	sloužící	k2eAgInPc1d1	sloužící
pro	pro	k7c4	pro
pořádání	pořádání	k1gNnSc4	pořádání
výstav	výstava	k1gFnPc2	výstava
a	a	k8xC	a
koncertů	koncert	k1gInPc2	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Přibyslavský	přibyslavský	k2eAgInSc1d1	přibyslavský
zámek	zámek	k1gInSc1	zámek
nechal	nechat	k5eAaPmAgInS	nechat
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1560	[number]	k4	1560
vystavět	vystavět	k5eAaPmF	vystavět
Zachariáš	Zachariáš	k1gFnPc4	Zachariáš
z	z	k7c2	z
Hradce	Hradec	k1gInSc2	Hradec
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
starého	starý	k2eAgInSc2d1	starý
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
dvora	dvůr	k1gInSc2	dvůr
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
náležel	náležet	k5eAaImAgMnS	náležet
k	k	k7c3	k
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
zaniklému	zaniklý	k2eAgInSc3d1	zaniklý
hradu	hrad	k1gInSc3	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
jako	jako	k8xS	jako
lovecký	lovecký	k2eAgInSc4d1	lovecký
zámeček	zámeček	k1gInSc4	zámeček
a	a	k8xC	a
letní	letní	k2eAgNnSc4d1	letní
sídlo	sídlo	k1gNnSc4	sídlo
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
italské	italský	k2eAgFnSc2d1	italská
renesance	renesance	k1gFnSc2	renesance
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
obdélníková	obdélníkový	k2eAgFnSc1d1	obdélníková
budova	budova	k1gFnSc1	budova
dnes	dnes	k6eAd1	dnes
tvoří	tvořit	k5eAaImIp3nS	tvořit
zadní	zadní	k2eAgInSc1d1	zadní
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgInSc1d1	jižní
trakt	trakt	k1gInSc4	trakt
čtyřkřídlého	čtyřkřídlý	k2eAgInSc2d1	čtyřkřídlý
zámeckého	zámecký	k2eAgInSc2d1	zámecký
areálu	areál	k1gInSc2	areál
<g/>
.	.	kIx.	.
</s>
<s>
Stavitelé	stavitel	k1gMnPc1	stavitel
nejspíš	nejspíš	k9	nejspíš
využili	využít	k5eAaPmAgMnP	využít
původní	původní	k2eAgNnSc4d1	původní
sklepení	sklepení	k1gNnSc4	sklepení
panského	panský	k2eAgInSc2d1	panský
dvora	dvůr	k1gInSc2	dvůr
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
jihozápadním	jihozápadní	k2eAgNnSc6d1	jihozápadní
nároží	nároží	k1gNnSc6	nároží
zůstala	zůstat	k5eAaPmAgFnS	zůstat
dochována	dochován	k2eAgFnSc1d1	dochována
gotická	gotický	k2eAgFnSc1d1	gotická
křížová	křížový	k2eAgFnSc1d1	křížová
klenba	klenba	k1gFnSc1	klenba
na	na	k7c6	na
dvou	dva	k4xCgInPc6	dva
kamenných	kamenný	k2eAgInPc6d1	kamenný
sloupech	sloup	k1gInPc6	sloup
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dveřním	dveřní	k2eAgNnSc6d1	dveřní
ostění	ostění	k1gNnSc6	ostění
místnosti	místnost	k1gFnSc2	místnost
je	být	k5eAaImIp3nS	být
také	také	k9	také
zachován	zachován	k2eAgInSc1d1	zachován
erb	erb	k1gInSc1	erb
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Hradce	Hradec	k1gInSc2	Hradec
s	s	k7c7	s
letopočtem	letopočet	k1gInSc7	letopočet
1565	[number]	k4	1565
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
zámkem	zámek	k1gInSc7	zámek
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
umístěna	umístěn	k2eAgFnSc1d1	umístěna
290	[number]	k4	290
cm	cm	kA	cm
vysoká	vysoký	k2eAgFnSc1d1	vysoká
jezdecká	jezdecký	k2eAgFnSc1d1	jezdecká
socha	socha	k1gFnSc1	socha
Jana	Jan	k1gMnSc2	Jan
Žižky	Žižka	k1gMnSc2	Žižka
<g/>
.	.	kIx.	.
3,5	[number]	k4	3,5
km	km	kA	km
východně	východně	k6eAd1	východně
Ronov	Ronov	k1gInSc1	Ronov
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
zřícenina	zřícenina	k1gFnSc1	zřícenina
hradu	hrad	k1gInSc2	hrad
Ronova	Ronův	k2eAgFnSc1d1	Ronova
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1329	[number]	k4	1329
(	(	kIx(	(
<g/>
sídlo	sídlo	k1gNnSc1	sídlo
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Ronova	Ronův	k2eAgInSc2d1	Ronův
<g/>
)	)	kIx)	)
a	a	k8xC	a
kamenný	kamenný	k2eAgInSc4d1	kamenný
most	most	k1gInSc4	most
ze	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
5	[number]	k4	5
km	km	kA	km
severně	severně	k6eAd1	severně
Žižkovo	Žižkův	k2eAgNnSc4d1	Žižkovo
Pole	pole	k1gNnSc4	pole
mohyla	mohyla	k1gFnSc1	mohyla
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1874	[number]	k4	1874
připomínající	připomínající	k2eAgNnSc4d1	připomínající
úmrtí	úmrtí	k1gNnSc4	úmrtí
Jana	Jan	k1gMnSc2	Jan
Žižky	Žižka	k1gMnSc2	Žižka
5,5	[number]	k4	5,5
km	km	kA	km
západně	západně	k6eAd1	západně
Stříbrné	stříbrná	k1gFnPc1	stříbrná
Hory	hora	k1gFnSc2	hora
s	s	k7c7	s
hornickým	hornický	k2eAgInSc7d1	hornický
kostelíkem	kostelík	k1gInSc7	kostelík
sv.	sv.	kA	sv.
Kateřiny	Kateřina	k1gFnSc2	Kateřina
8	[number]	k4	8
km	km	kA	km
západně	západně	k6eAd1	západně
Pohled	pohled	k1gInSc1	pohled
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	nedaleko	k7c2	nedaleko
obce	obec	k1gFnSc2	obec
známá	známý	k2eAgFnSc1d1	známá
poutní	poutní	k2eAgFnSc1d1	poutní
kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Anny	Anna	k1gFnPc4	Anna
s	s	k7c7	s
léčivým	léčivý	k2eAgInSc7d1	léčivý
pramenem	pramen	k1gInSc7	pramen
8,5	[number]	k4	8,5
km	km	kA	km
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
Nížkov	Nížkov	k1gInSc1	Nížkov
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
barokně	barokně	k6eAd1	barokně
přestavěný	přestavěný	k2eAgInSc1d1	přestavěný
s	s	k7c7	s
kostnicí	kostnice	k1gFnSc7	kostnice
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1709	[number]	k4	1709
přístupnou	přístupný	k2eAgFnSc4d1	přístupná
na	na	k7c4	na
požádání	požádání	k1gNnSc4	požádání
13,5	[number]	k4	13,5
km	km	kA	km
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
Rosička	rosička	k1gFnSc1	rosička
-	-	kIx~	-
kopec	kopec	k1gInSc1	kopec
nad	nad	k7c7	nad
stejnojmennou	stejnojmenný	k2eAgFnSc7d1	stejnojmenná
obcí	obec	k1gFnSc7	obec
osazený	osazený	k2eAgInSc4d1	osazený
vysílačem	vysílač	k1gMnSc7	vysílač
<g/>
.	.	kIx.	.
</s>
<s>
Věž	věž	k1gFnSc1	věž
je	být	k5eAaImIp3nS	být
celoročně	celoročně	k6eAd1	celoročně
volně	volně	k6eAd1	volně
přístupná	přístupný	k2eAgFnSc1d1	přístupná
jako	jako	k8xS	jako
rozhledna	rozhledna	k1gFnSc1	rozhledna
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
městem	město	k1gNnSc7	město
jsou	být	k5eAaImIp3nP	být
spjaty	spjat	k2eAgFnPc1d1	spjata
tyto	tento	k3xDgFnPc1	tento
osobnosti	osobnost	k1gFnPc1	osobnost
<g/>
:	:	kIx,	:
Hynce	Hynce	k1gMnSc1	Hynce
Ptáček	Ptáček	k1gMnSc1	Ptáček
z	z	k7c2	z
Pirkštejna	Pirkštejn	k1gInSc2	Pirkštejn
(	(	kIx(	(
<g/>
†	†	k?	†
1444	[number]	k4	1444
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
majitel	majitel	k1gMnSc1	majitel
polensko-přibyslavského	polenskořibyslavský	k2eAgNnSc2d1	polensko-přibyslavský
panství	panství	k1gNnSc2	panství
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
z	z	k7c2	z
Ditrichštejna	Ditrichštejn	k1gInSc2	Ditrichštejn
(	(	kIx(	(
<g/>
1636	[number]	k4	1636
<g/>
-	-	kIx~	-
<g/>
1698	[number]	k4	1698
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
majitel	majitel	k1gMnSc1	majitel
polensko-přibyslavského	polenskořibyslavský	k2eAgNnSc2d1	polensko-přibyslavský
panství	panství	k1gNnSc2	panství
Karel	Karel	k1gMnSc1	Karel
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
z	z	k7c2	z
Ditrichštejna	Ditrichštejn	k1gInSc2	Ditrichštejn
(	(	kIx(	(
<g/>
1702	[number]	k4	1702
<g/>
-	-	kIx~	-
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1784	[number]	k4	1784
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
majitel	majitel	k1gMnSc1	majitel
přibyslavského	přibyslavský	k2eAgNnSc2d1	Přibyslavské
panství	panství	k1gNnSc2	panství
<g/>
,	,	kIx,	,
stavitel	stavitel	k1gMnSc1	stavitel
kostela	kostel	k1gInSc2	kostel
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
z	z	k7c2	z
Ditrichštejna	Ditrichštejn	k1gInSc2	Ditrichštejn
(	(	kIx(	(
<g/>
1767	[number]	k4	1767
<g/>
-	-	kIx~	-
<g/>
1854	[number]	k4	1854
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
majitel	majitel	k1gMnSc1	majitel
přibyslavského	přibyslavský	k2eAgNnSc2d1	Přibyslavské
panství	panství	k1gNnSc2	panství
Augustin	Augustin	k1gMnSc1	Augustin
Bedřich	Bedřich	k1gMnSc1	Bedřich
Čepl	Čepl	k1gMnSc1	Čepl
(	(	kIx(	(
<g/>
1820	[number]	k4	1820
<g/>
-	-	kIx~	-
<g/>
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kupec	kupec	k1gMnSc1	kupec
<g/>
,	,	kIx,	,
přibyslavský	přibyslavský	k2eAgMnSc1d1	přibyslavský
mecenáš	mecenáš	k1gMnSc1	mecenáš
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
starosta	starosta	k1gMnSc1	starosta
města	město	k1gNnSc2	město
František	František	k1gMnSc1	František
Niklfeld	Niklfeld	k1gMnSc1	Niklfeld
(	(	kIx(	(
<g/>
1841	[number]	k4	1841
<g/>
-	-	kIx~	-
<g/>
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
starosta	starosta	k1gMnSc1	starosta
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
zemského	zemský	k2eAgInSc2d1	zemský
sněmu	sněm	k1gInSc2	sněm
Jan	Jan	k1gMnSc1	Jan
Otto	Otto	k1gMnSc1	Otto
(	(	kIx(	(
<g/>
1841	[number]	k4	1841
<g/>
-	-	kIx~	-
<g/>
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nakladatel	nakladatel	k1gMnSc1	nakladatel
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
čestný	čestný	k2eAgMnSc1d1	čestný
občan	občan	k1gMnSc1	občan
města	město	k1gNnSc2	město
Josef	Josef	k1gMnSc1	Josef
Reinsberg	Reinsberg	k1gMnSc1	Reinsberg
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1844	[number]	k4	1844
<g/>
-	-	kIx~	-
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
<g/>
,	,	kIx,	,
děkan	děkan	k1gMnSc1	děkan
a	a	k8xC	a
rektor	rektor	k1gMnSc1	rektor
František	František	k1gMnSc1	František
Malinský	Malinský	k2eAgMnSc1d1	Malinský
(	(	kIx(	(
<g/>
1850	[number]	k4	1850
<g/>
-	-	kIx~	-
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podnikatel	podnikatel	k1gMnSc1	podnikatel
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
společnosti	společnost	k1gFnSc2	společnost
Amylon	Amylon	k1gInSc1	Amylon
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
Jan	Jan	k1gMnSc1	Jan
Bechyně	Bechyně	k1gMnSc1	Bechyně
(	(	kIx(	(
<g/>
1866	[number]	k4	1866
<g />
.	.	kIx.	.
</s>
<s>
<g/>
-	-	kIx~	-
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
Muzejního	muzejní	k2eAgInSc2d1	muzejní
spolku	spolek	k1gInSc2	spolek
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
dokumentárních	dokumentární	k2eAgFnPc2d1	dokumentární
fotografií	fotografia	k1gFnPc2	fotografia
Přibyslavi	Přibyslav	k1gFnSc2	Přibyslav
František	František	k1gMnSc1	František
Půža	Půža	k1gMnSc1	Půža
(	(	kIx(	(
<g/>
1872	[number]	k4	1872
<g/>
-	-	kIx~	-
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
autor	autor	k1gMnSc1	autor
děl	dít	k5eAaBmAgMnS	dít
o	o	k7c6	o
dějinách	dějiny	k1gFnPc6	dějiny
Přibyslavska	Přibyslavsko	k1gNnSc2	Přibyslavsko
Rudolf	Rudolfa	k1gFnPc2	Rudolfa
Ludmila	Ludmila	k1gFnSc1	Ludmila
(	(	kIx(	(
<g/>
1872	[number]	k4	1872
<g/>
-	-	kIx~	-
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
kreslíř	kreslíř	k1gMnSc1	kreslíř
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
učitel	učitel	k1gMnSc1	učitel
Theodor	Theodor	k1gMnSc1	Theodor
Bohumír	Bohumír	k1gMnSc1	Bohumír
Pařík	Pařík	k1gMnSc1	Pařík
(	(	kIx(	(
<g/>
1881	[number]	k4	1881
<g/>
-	-	kIx~	-
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
houslista	houslista	k1gMnSc1	houslista
František	František	k1gMnSc1	František
Němec	Němec	k1gMnSc1	Němec
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
<g/>
-	-	kIx~	-
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sochař	sochař	k1gMnSc1	sochař
Františka	Františka	k1gFnSc1	Františka
Kolářová-Vlčková	Kolářová-Vlčková	k1gFnSc1	Kolářová-Vlčková
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
<g/>
-	-	kIx~	-
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
a	a	k8xC	a
profesorka	profesorka	k1gFnSc1	profesorka
Stanislav	Stanislav	k1gMnSc1	Stanislav
Bechyně	Bechyně	k1gMnSc1	Bechyně
(	(	kIx(	(
<g/>
1887	[number]	k4	1887
<g/>
-	-	kIx~	-
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
konstruktér	konstruktér	k1gMnSc1	konstruktér
a	a	k8xC	a
pedagog	pedagog	k1gMnSc1	pedagog
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Luža	Luža	k1gMnSc1	Luža
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
-	-	kIx~	-
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
armádní	armádní	k2eAgMnSc1d1	armádní
generál	generál	k1gMnSc1	generál
<g/>
,	,	kIx,	,
odbojář	odbojář	k1gMnSc1	odbojář
<g/>
,	,	kIx,	,
zabit	zabit	k2eAgMnSc1d1	zabit
při	při	k7c6	při
přestřelce	přestřelka	k1gFnSc6	přestřelka
v	v	k7c6	v
Přibyslavi	Přibyslav	k1gFnSc6	Přibyslav
Adolf	Adolf	k1gMnSc1	Adolf
<g />
.	.	kIx.	.
</s>
<s>
Švec	Švec	k1gMnSc1	Švec
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
-	-	kIx~	-
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
agronom	agronom	k1gMnSc1	agronom
<g/>
,	,	kIx,	,
starosta	starosta	k1gMnSc1	starosta
sokolské	sokolský	k2eAgFnSc2d1	Sokolská
jednoty	jednota	k1gFnSc2	jednota
v	v	k7c6	v
Přibyslavi	Přibyslav	k1gFnSc6	Přibyslav
Jan	Jan	k1gMnSc1	Jan
Filip	Filip	k1gMnSc1	Filip
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
-	-	kIx~	-
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teolog	teolog	k1gMnSc1	teolog
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
esperantista	esperantista	k1gMnSc1	esperantista
Jan	Jan	k1gMnSc1	Jan
Bechyně	Bechyně	k1gMnSc1	Bechyně
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
-	-	kIx~	-
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
entomolog	entomolog	k1gMnSc1	entomolog
Miroslav	Miroslav	k1gMnSc1	Miroslav
Řepiský	Řepiský	k1gMnSc1	Řepiský
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
-	-	kIx~	-
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
a	a	k8xC	a
organizátor	organizátor	k1gMnSc1	organizátor
hasičského	hasičský	k2eAgNnSc2d1	hasičské
hnutí	hnutí	k1gNnSc2	hnutí
Karel	Karel	k1gMnSc1	Karel
Pátek	Pátek	k1gMnSc1	Pátek
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
-	-	kIx~	-
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fyzik	fyzik	k1gMnSc1	fyzik
a	a	k8xC	a
spolutvůrce	spolutvůrce	k1gMnSc1	spolutvůrce
prvního	první	k4xOgInSc2	první
československého	československý	k2eAgInSc2d1	československý
laseru	laser	k1gInSc2	laser
Jaromír	Jaromír	k1gMnSc1	Jaromír
Málek	Málek	k1gMnSc1	Málek
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
-	-	kIx~	-
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
egyptolog	egyptolog	k1gMnSc1	egyptolog
Roman	Roman	k1gMnSc1	Roman
Podrázský	Podrázský	k2eAgMnSc1d1	Podrázský
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
-	-	kIx~	-
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
akademický	akademický	k2eAgMnSc1d1	akademický
sochař	sochař	k1gMnSc1	sochař
a	a	k8xC	a
starosta	starosta	k1gMnSc1	starosta
města	město	k1gNnSc2	město
Hana	Hana	k1gFnSc1	Hana
Orgoníková	Orgoníková	k1gFnSc1	Orgoníková
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
-	-	kIx~	-
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poslankyně	poslankyně	k1gFnSc1	poslankyně
Jan	Jan	k1gMnSc1	Jan
Štefáček	Štefáček	k1gMnSc1	Štefáček
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
-	-	kIx~	-
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
a	a	k8xC	a
starosta	starosta	k1gMnSc1	starosta
Přibyslavi	Přibyslav	k1gFnSc2	Přibyslav
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g />
.	.	kIx.	.
</s>
<s>
Kubeš	Kubeš	k1gMnSc1	Kubeš
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
-	-	kIx~	-
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teolog	teolog	k1gMnSc1	teolog
a	a	k8xC	a
kněz	kněz	k1gMnSc1	kněz
v	v	k7c6	v
Přibyslavi	Přibyslav	k1gFnSc6	Přibyslav
Mook	Mook	k1gMnSc1	Mook
en	en	k?	en
Middelaar	Middelaar	k1gInSc1	Middelaar
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
(	(	kIx(	(
<g/>
smlouva	smlouva	k1gFnSc1	smlouva
podepsána	podepsán	k2eAgFnSc1d1	podepsána
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Sliač	Sliač	k1gInSc1	Sliač
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
(	(	kIx(	(
<g/>
smlouva	smlouva	k1gFnSc1	smlouva
podepsána	podepsán	k2eAgFnSc1d1	podepsána
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
o	o	k7c4	o
navázání	navázání	k1gNnSc4	navázání
partnerské	partnerský	k2eAgFnSc2d1	partnerská
spolupráce	spolupráce	k1gFnSc2	spolupráce
i	i	k8xC	i
s	s	k7c7	s
německým	německý	k2eAgNnSc7d1	německé
městem	město	k1gNnSc7	město
Büdelsdorf	Büdelsdorf	k1gMnSc1	Büdelsdorf
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
však	však	k9	však
nakonec	nakonec	k6eAd1	nakonec
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
