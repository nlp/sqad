<s>
Přibyslav	Přibyslav	k1gFnSc1	Přibyslav
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Primislau	Primislaus	k1gInSc3	Primislaus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
v	v	k7c6	v
Kraji	kraj	k1gInSc6	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Sázavě	Sázava	k1gFnSc6	Sázava
<g/>
,	,	kIx,	,
23	[number]	k4	23
km	km	kA	km
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
Jihlavy	Jihlava	k1gFnSc2	Jihlava
a	a	k8xC	a
12	[number]	k4	12
km	km	kA	km
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
Havlíčkova	Havlíčkův	k2eAgInSc2d1	Havlíčkův
Brodu	Brod	k1gInSc2	Brod
<g/>
.	.	kIx.	.
</s>
