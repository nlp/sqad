<s>
Anatomie	anatomie	k1gFnSc1	anatomie
ptáků	pták	k1gMnPc2	pták
je	být	k5eAaImIp3nS	být
vědní	vědní	k2eAgInSc1d1	vědní
obor	obor	k1gInSc1	obor
zabývající	zabývající	k2eAgInSc1d1	zabývající
se	se	k3xPyFc4	se
stavbou	stavba	k1gFnSc7	stavba
ptačího	ptačí	k2eAgNnSc2d1	ptačí
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
jsou	být	k5eAaImIp3nP	být
specializovanou	specializovaný	k2eAgFnSc7d1	specializovaná
vývojovou	vývojový	k2eAgFnSc7d1	vývojová
větví	větev	k1gFnSc7	větev
plazů	plaz	k1gInPc2	plaz
skupiny	skupina	k1gFnSc2	skupina
Archosauria	Archosaurium	k1gNnSc2	Archosaurium
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterých	který	k3yQgInPc2	který
se	se	k3xPyFc4	se
odlišili	odlišit	k5eAaPmAgMnP	odlišit
schopností	schopnost	k1gFnSc7	schopnost
endotermní	endotermní	k2eAgFnSc2d1	endotermní
termoregulace	termoregulace	k1gFnSc2	termoregulace
(	(	kIx(	(
<g/>
homoitermie	homoitermie	k1gFnSc1	homoitermie
<g/>
)	)	kIx)	)
a	a	k8xC	a
vytvořením	vytvoření	k1gNnSc7	vytvoření
unikátního	unikátní	k2eAgInSc2d1	unikátní
způsobu	způsob	k1gInSc2	způsob
létání	létání	k1gNnSc2	létání
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
zcela	zcela	k6eAd1	zcela
nové	nový	k2eAgFnSc2d1	nová
struktury	struktura	k1gFnSc2	struktura
–	–	k?	–
peří	peří	k1gNnSc1	peří
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
jejich	jejich	k3xOp3gInPc7	jejich
progresivními	progresivní	k2eAgInPc7d1	progresivní
znaky	znak	k1gInPc7	znak
jsou	být	k5eAaImIp3nP	být
přestavba	přestavba	k1gFnSc1	přestavba
přední	přední	k2eAgFnSc2d1	přední
končetiny	končetina	k1gFnSc2	končetina
v	v	k7c4	v
křídlo	křídlo	k1gNnSc4	křídlo
<g/>
,	,	kIx,	,
změny	změna	k1gFnPc4	změna
v	v	k7c6	v
lopatkovém	lopatkový	k2eAgNnSc6d1	lopatkové
pásmu	pásmo	k1gNnSc6	pásmo
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
srůst	srůst	k5eAaPmF	srůst
klíční	klíční	k2eAgFnPc4d1	klíční
kosti	kost	k1gFnPc4	kost
v	v	k7c4	v
sáňky	sáňky	k1gFnPc4	sáňky
(	(	kIx(	(
<g/>
furcula	furcula	k1gFnSc1	furcula
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zadní	zadní	k2eAgFnSc1d1	zadní
končetina	končetina	k1gFnSc1	končetina
s	s	k7c7	s
prodlouženým	prodloužený	k2eAgInSc7d1	prodloužený
běhákem	běhák	k1gInSc7	běhák
(	(	kIx(	(
<g/>
tarsometatarsus	tarsometatarsus	k1gInSc1	tarsometatarsus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
úplné	úplný	k2eAgNnSc4d1	úplné
rozdělení	rozdělení	k1gNnSc4	rozdělení
srdce	srdce	k1gNnSc2	srdce
na	na	k7c4	na
pravou	pravý	k2eAgFnSc4d1	pravá
stranu	strana	k1gFnSc4	strana
s	s	k7c7	s
redukovanou	redukovaný	k2eAgFnSc7d1	redukovaná
a	a	k8xC	a
levou	levý	k2eAgFnSc4d1	levá
stranu	strana	k1gFnSc4	strana
s	s	k7c7	s
oxidovanou	oxidovaný	k2eAgFnSc7d1	oxidovaná
krví	krev	k1gFnSc7	krev
<g/>
,	,	kIx,	,
rozvoj	rozvoj	k1gInSc1	rozvoj
pravé	pravý	k2eAgFnSc2d1	pravá
aorty	aorta	k1gFnSc2	aorta
a	a	k8xC	a
vymizení	vymizení	k1gNnSc4	vymizení
levé	levý	k2eAgNnSc4d1	levé
<g/>
,	,	kIx,	,
a	a	k8xC	a
zvětšení	zvětšení	k1gNnSc1	zvětšení
koncového	koncový	k2eAgInSc2d1	koncový
mozku	mozek	k1gInSc2	mozek
rozvojem	rozvoj	k1gInSc7	rozvoj
neostriata	neostriat	k1gMnSc2	neostriat
<g/>
.	.	kIx.	.
</s>
<s>
Speciálními	speciální	k2eAgInPc7d1	speciální
znaky	znak	k1gInPc7	znak
jsou	být	k5eAaImIp3nP	být
dále	daleko	k6eAd2	daleko
pneumatizace	pneumatizace	k1gFnPc1	pneumatizace
kostí	kost	k1gFnPc2	kost
<g/>
,	,	kIx,	,
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
úprava	úprava	k1gFnSc1	úprava
plic	plíce	k1gFnPc2	plíce
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
vzdušných	vzdušný	k2eAgInPc2d1	vzdušný
vaků	vak	k1gInPc2	vak
<g/>
,	,	kIx,	,
zpevnění	zpevnění	k1gNnSc2	zpevnění
některých	některý	k3yIgFnPc2	některý
částí	část	k1gFnPc2	část
kostry	kostra	k1gFnSc2	kostra
(	(	kIx(	(
<g/>
lebky	lebka	k1gFnSc2	lebka
<g/>
,	,	kIx,	,
páteře	páteř	k1gFnSc2	páteř
<g/>
,	,	kIx,	,
pánve	pánev	k1gFnSc2	pánev
<g/>
)	)	kIx)	)
srůstem	srůst	k1gInSc7	srůst
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
kostí	kost	k1gFnPc2	kost
<g/>
,	,	kIx,	,
vznik	vznik	k1gInSc4	vznik
zobákovitých	zobákovitý	k2eAgFnPc2d1	zobákovitá
čelistí	čelist	k1gFnPc2	čelist
s	s	k7c7	s
rohovitým	rohovitý	k2eAgInSc7d1	rohovitý
pokryvem	pokryv	k1gInSc7	pokryv
<g/>
,	,	kIx,	,
zdokonalení	zdokonalení	k1gNnSc3	zdokonalení
ve	v	k7c6	v
stavbě	stavba	k1gFnSc6	stavba
oka	oko	k1gNnSc2	oko
a	a	k8xC	a
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
ucha	ucho	k1gNnSc2	ucho
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
plazích	plazí	k2eAgInPc6d1	plazí
předcích	předek	k1gInPc6	předek
zdědili	zdědit	k5eAaPmAgMnP	zdědit
suchou	suchý	k2eAgFnSc4d1	suchá
kůži	kůže	k1gFnSc4	kůže
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
původní	původní	k2eAgFnSc1d1	původní
epidermální	epidermální	k2eAgFnSc1d1	epidermální
šupina	šupina	k1gFnSc1	šupina
přetvořila	přetvořit	k5eAaPmAgFnS	přetvořit
v	v	k7c4	v
pero	pero	k1gNnSc4	pero
a	a	k8xC	a
rozvinula	rozvinout	k5eAaPmAgFnS	rozvinout
se	se	k3xPyFc4	se
jen	jen	k9	jen
jediná	jediný	k2eAgFnSc1d1	jediná
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
kostrční	kostrční	k2eAgFnSc1d1	kostrční
žláza	žláza	k1gFnSc1	žláza
<g/>
.	.	kIx.	.
</s>
<s>
Plazího	plazí	k2eAgInSc2d1	plazí
typu	typ	k1gInSc2	typ
je	být	k5eAaImIp3nS	být
i	i	k9	i
trojbazická	trojbazický	k2eAgFnSc1d1	trojbazický
a	a	k8xC	a
monokondylní	monokondylní	k2eAgFnSc1d1	monokondylní
lebka	lebka	k1gFnSc1	lebka
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
diapsidní	diapsidní	k2eAgNnPc1d1	diapsidní
<g/>
,	,	kIx,	,
modifikovaná	modifikovaný	k2eAgNnPc1d1	modifikované
ztrátou	ztráta	k1gFnSc7	ztráta
horního	horní	k2eAgInSc2d1	horní
jařmového	jařmový	k2eAgInSc2d1	jařmový
oblouku	oblouk	k1gInSc2	oblouk
a	a	k8xC	a
splynutím	splynutí	k1gNnSc7	splynutí
spánkové	spánkový	k2eAgFnSc2d1	spánková
jámy	jáma	k1gFnSc2	jáma
s	s	k7c7	s
orbitou	orbita	k1gFnSc7	orbita
<g/>
.	.	kIx.	.
</s>
<s>
Zachován	zachován	k2eAgMnSc1d1	zachován
zůstal	zůstat	k5eAaPmAgMnS	zůstat
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
i	i	k9	i
plazí	plazí	k2eAgInSc1d1	plazí
způsob	způsob	k1gInSc1	způsob
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
<g/>
;	;	kIx,	;
žloutkem	žloutek	k1gInSc7	žloutek
bohatá	bohatý	k2eAgNnPc4d1	bohaté
vejce	vejce	k1gNnPc4	vejce
však	však	k9	však
mají	mít	k5eAaImIp3nP	mít
vždy	vždy	k6eAd1	vždy
pevnou	pevný	k2eAgFnSc4d1	pevná
vápenitou	vápenitý	k2eAgFnSc4d1	vápenitá
skořápku	skořápka	k1gFnSc4	skořápka
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
také	také	k9	také
rozvinuta	rozvinut	k2eAgFnSc1d1	rozvinuta
komplikovaná	komplikovaný	k2eAgFnSc1d1	komplikovaná
péče	péče	k1gFnSc1	péče
o	o	k7c4	o
vejce	vejce	k1gNnPc4	vejce
a	a	k8xC	a
mláďata	mládě	k1gNnPc4	mládě
<g/>
,	,	kIx,	,
označovaná	označovaný	k2eAgFnSc1d1	označovaná
jako	jako	k8xC	jako
hnízdění	hnízdění	k1gNnSc1	hnízdění
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
znaků	znak	k1gInPc2	znak
ptáků	pták	k1gMnPc2	pták
tvoří	tvořit	k5eAaImIp3nS	tvořit
anatomické	anatomický	k2eAgFnPc4d1	anatomická
a	a	k8xC	a
funkční	funkční	k2eAgFnPc4d1	funkční
adaptace	adaptace	k1gFnPc4	adaptace
související	související	k2eAgFnPc4d1	související
se	se	k3xPyFc4	se
schopností	schopnost	k1gFnSc7	schopnost
letu	let	k1gInSc2	let
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
pokročilé	pokročilý	k2eAgFnPc1d1	pokročilá
adaptace	adaptace	k1gFnPc1	adaptace
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
stupněm	stupeň	k1gInSc7	stupeň
instinktivního	instinktivní	k2eAgNnSc2d1	instinktivní
jednání	jednání	k1gNnSc2	jednání
učinily	učinit	k5eAaPmAgInP	učinit
z	z	k7c2	z
ptáků	pták	k1gMnPc2	pták
vývojově	vývojově	k6eAd1	vývojově
velmi	velmi	k6eAd1	velmi
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
skupinu	skupina	k1gFnSc4	skupina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
osídlila	osídlit	k5eAaPmAgFnS	osídlit
celý	celý	k2eAgInSc4d1	celý
svět	svět	k1gInSc4	svět
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
mořských	mořský	k2eAgFnPc2d1	mořská
hlubin	hlubina	k1gFnPc2	hlubina
a	a	k8xC	a
rozrůznila	rozrůznit	k5eAaPmAgFnS	rozrůznit
se	se	k3xPyFc4	se
ve	v	k7c4	v
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
specializovaných	specializovaný	k2eAgFnPc2d1	specializovaná
skupin	skupina	k1gFnPc2	skupina
a	a	k8xC	a
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Embryonální	embryonální	k2eAgInSc4d1	embryonální
vývoj	vývoj	k1gInSc4	vývoj
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kůže	kůže	k1gFnSc2	kůže
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Kůže	kůže	k1gFnSc1	kůže
(	(	kIx(	(
<g/>
cutis	cutis	k1gFnSc1	cutis
<g/>
)	)	kIx)	)
společně	společně	k6eAd1	společně
se	s	k7c7	s
sliznicemi	sliznice	k1gFnPc7	sliznice
trávicího	trávicí	k2eAgInSc2d1	trávicí
<g/>
,	,	kIx,	,
dýchacího	dýchací	k2eAgInSc2d1	dýchací
<g/>
,	,	kIx,	,
močového	močový	k2eAgInSc2d1	močový
a	a	k8xC	a
pohlavního	pohlavní	k2eAgInSc2d1	pohlavní
traktu	trakt	k1gInSc2	trakt
představují	představovat	k5eAaImIp3nP	představovat
nejen	nejen	k6eAd1	nejen
primární	primární	k2eAgFnPc1d1	primární
mechanické	mechanický	k2eAgFnPc1d1	mechanická
bariéry	bariéra	k1gFnPc1	bariéra
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
se	se	k3xPyFc4	se
ptáci	pták	k1gMnPc1	pták
chrání	chránit	k5eAaImIp3nP	chránit
proti	proti	k7c3	proti
vstupu	vstup	k1gInSc3	vstup
patogenů	patogen	k1gInPc2	patogen
do	do	k7c2	do
organismu	organismus	k1gInSc2	organismus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
se	se	k3xPyFc4	se
podílejí	podílet	k5eAaImIp3nP	podílet
na	na	k7c6	na
imunitních	imunitní	k2eAgFnPc6d1	imunitní
reakcích	reakce	k1gFnPc6	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Kůže	kůže	k1gFnSc1	kůže
ptáků	pták	k1gMnPc2	pták
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
tenká	tenký	k2eAgFnSc1d1	tenká
a	a	k8xC	a
takřka	takřka	k6eAd1	takřka
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
kožní	kožní	k2eAgFnPc4d1	kožní
žlázy	žláza	k1gFnPc4	žláza
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
pokožky	pokožka	k1gFnSc2	pokožka
(	(	kIx(	(
<g/>
epidermis	epidermis	k1gFnSc1	epidermis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
škáry	škára	k1gFnPc4	škára
(	(	kIx(	(
<g/>
dermis	dermis	k1gFnPc4	dermis
<g/>
)	)	kIx)	)
a	a	k8xC	a
podkoží	podkoží	k1gNnSc1	podkoží
(	(	kIx(	(
<g/>
subcutis	subcutis	k1gInSc1	subcutis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
kůže	kůže	k1gFnSc2	kůže
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
odvozeny	odvozen	k2eAgInPc1d1	odvozen
kožní	kožní	k2eAgInPc1d1	kožní
deriváty	derivát	k1gInPc1	derivát
(	(	kIx(	(
<g/>
appendices	appendices	k1gInSc1	appendices
carnosae	carnosa	k1gInSc2	carnosa
<g/>
)	)	kIx)	)
–	–	k?	–
např.	např.	kA	např.
hřebínek	hřebínek	k1gInSc1	hřebínek
<g/>
,	,	kIx,	,
lalůčky	lalůček	k1gInPc1	lalůček
<g/>
,	,	kIx,	,
ušnice	ušnice	k1gFnPc1	ušnice
aj.	aj.	kA	aj.
Unikátním	unikátní	k2eAgInSc7d1	unikátní
útvarem	útvar	k1gInSc7	útvar
u	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
je	být	k5eAaImIp3nS	být
peří	peří	k1gNnSc1	peří
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kostra	kostra	k1gFnSc1	kostra
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
způsobu	způsob	k1gInSc3	způsob
života	život	k1gInSc2	život
a	a	k8xC	a
pohybu	pohyb	k1gInSc2	pohyb
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
kostra	kostra	k1gFnSc1	kostra
ptáků	pták	k1gMnPc2	pták
řadu	řada	k1gFnSc4	řada
anatomických	anatomický	k2eAgFnPc2d1	anatomická
odchylek	odchylka	k1gFnPc2	odchylka
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
se	s	k7c7	s
savci	savec	k1gMnPc7	savec
(	(	kIx(	(
<g/>
nižší	nízký	k2eAgFnSc1d2	nižší
relativní	relativní	k2eAgFnSc1d1	relativní
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
,	,	kIx,	,
pevnost	pevnost	k1gFnSc1	pevnost
<g/>
,	,	kIx,	,
pneumatizace	pneumatizace	k1gFnSc1	pneumatizace
většiny	většina	k1gFnSc2	většina
kostí	kost	k1gFnPc2	kost
a	a	k8xC	a
redukce	redukce	k1gFnSc1	redukce
jejich	jejich	k3xOp3gInSc2	jejich
počtu	počet	k1gInSc2	počet
srůstem	srůst	k1gInSc7	srůst
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
částech	část	k1gFnPc6	část
kostry	kostra	k1gFnSc2	kostra
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kosti	kost	k1gFnPc1	kost
jsou	být	k5eAaImIp3nP	být
díky	díky	k7c3	díky
vysokému	vysoký	k2eAgInSc3d1	vysoký
obsahu	obsah	k1gInSc3	obsah
minerálních	minerální	k2eAgFnPc2d1	minerální
látek	látka	k1gFnPc2	látka
velmi	velmi	k6eAd1	velmi
tvrdé	tvrdý	k2eAgNnSc1d1	tvrdé
a	a	k8xC	a
pevné	pevný	k2eAgNnSc1d1	pevné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
křehké	křehký	k2eAgFnPc1d1	křehká
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštnostmi	zvláštnost	k1gFnPc7	zvláštnost
ptačí	ptačí	k2eAgFnSc2d1	ptačí
kostry	kostra	k1gFnSc2	kostra
jsou	být	k5eAaImIp3nP	být
zejména	zejména	k9	zejména
bezzubý	bezzubý	k2eAgInSc4d1	bezzubý
zobák	zobák	k1gInSc4	zobák
<g/>
,	,	kIx,	,
vysoká	vysoký	k2eAgFnSc1d1	vysoká
pohyblivost	pohyblivost	k1gFnSc1	pohyblivost
horního	horní	k2eAgInSc2d1	horní
zobáku	zobák	k1gInSc2	zobák
<g/>
,	,	kIx,	,
plně	plně	k6eAd1	plně
vyvinutý	vyvinutý	k2eAgInSc1d1	vyvinutý
pletenec	pletenec	k1gInSc1	pletenec
hrudní	hrudní	k2eAgFnSc2d1	hrudní
končetiny	končetina	k1gFnSc2	končetina
přeměněné	přeměněný	k2eAgFnSc2d1	přeměněná
v	v	k7c4	v
křídlo	křídlo	k1gNnSc4	křídlo
<g/>
,	,	kIx,	,
osifikace	osifikace	k1gFnPc4	osifikace
hrudních	hrudní	k2eAgInPc2d1	hrudní
úseků	úsek	k1gInPc2	úsek
žeber	žebro	k1gNnPc2	žebro
a	a	k8xC	a
zpevnění	zpevnění	k1gNnSc2	zpevnění
hrudníku	hrudník	k1gInSc2	hrudník
pomocí	pomocí	k7c2	pomocí
háčkovitých	háčkovitý	k2eAgInPc2d1	háčkovitý
výběžků	výběžek	k1gInPc2	výběžek
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
jiných	jiný	k2eAgMnPc2d1	jiný
obratlovců	obratlovec	k1gMnPc2	obratlovec
se	se	k3xPyFc4	se
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
kostra	kostra	k1gFnSc1	kostra
osová	osový	k2eAgFnSc1d1	Osová
<g/>
,	,	kIx,	,
axiální	axiální	k2eAgFnSc1d1	axiální
(	(	kIx(	(
<g/>
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
kostru	kostra	k1gFnSc4	kostra
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
krku	krk	k1gInSc2	krk
a	a	k8xC	a
trupu	trup	k1gInSc2	trup
<g/>
)	)	kIx)	)
a	a	k8xC	a
kostra	kostra	k1gFnSc1	kostra
končetin	končetina	k1gFnPc2	končetina
(	(	kIx(	(
<g/>
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
kostry	kostra	k1gFnPc4	kostra
hrudní	hrudní	k2eAgFnPc4d1	hrudní
a	a	k8xC	a
pánevní	pánevní	k2eAgFnPc4d1	pánevní
končetiny	končetina	k1gFnPc4	končetina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
vzájemně	vzájemně	k6eAd1	vzájemně
liší	lišit	k5eAaImIp3nS	lišit
a	a	k8xC	a
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
stavba	stavba	k1gFnSc1	stavba
je	být	k5eAaImIp3nS	být
jiná	jiný	k2eAgFnSc1d1	jiná
než	než	k8xS	než
u	u	k7c2	u
ostatních	ostatní	k2eAgMnPc2d1	ostatní
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kosterní	kosterní	k2eAgFnSc6d1	kosterní
svalovině	svalovina	k1gFnSc6	svalovina
ptáků	pták	k1gMnPc2	pták
dominují	dominovat	k5eAaImIp3nP	dominovat
hlavně	hlavně	k9	hlavně
létací	létací	k2eAgInPc4d1	létací
svaly	sval	k1gInPc4	sval
a	a	k8xC	a
svaly	sval	k1gInPc4	sval
zadních	zadní	k2eAgFnPc2d1	zadní
končetin	končetina	k1gFnPc2	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgInPc7d3	veliký
svaly	sval	k1gInPc7	sval
na	na	k7c6	na
ptačím	ptačí	k2eAgNnSc6d1	ptačí
těle	tělo	k1gNnSc6	tělo
jsou	být	k5eAaImIp3nP	být
hrudní	hrudní	k2eAgInPc1d1	hrudní
svaly	sval	k1gInPc1	sval
(	(	kIx(	(
<g/>
mm	mm	kA	mm
<g/>
.	.	kIx.	.
pectorales	pectorales	k1gMnSc1	pectorales
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
upínající	upínající	k2eAgFnSc4d1	upínající
se	se	k3xPyFc4	se
na	na	k7c4	na
hřeben	hřeben	k1gInSc4	hřeben
kosti	kost	k1gFnSc2	kost
hrudní	hrudní	k2eAgFnSc2d1	hrudní
a	a	k8xC	a
pohybující	pohybující	k2eAgFnSc2d1	pohybující
křídlem	křídlo	k1gNnSc7	křídlo
dolů	dol	k1gInPc2	dol
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
antagonista	antagonista	k1gMnSc1	antagonista
m.	m.	k?	m.
supracoracoideus	supracoracoideus	k1gMnSc1	supracoracoideus
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc1d1	ležící
pod	pod	k7c7	pod
ním	on	k3xPp3gMnSc7	on
a	a	k8xC	a
vykonávající	vykonávající	k2eAgInSc1d1	vykonávající
pohyb	pohyb	k1gInSc1	pohyb
křídel	křídlo	k1gNnPc2	křídlo
směrem	směr	k1gInSc7	směr
nahoru	nahoru	k6eAd1	nahoru
<g/>
.	.	kIx.	.
</s>
<s>
Svalstvo	svalstvo	k1gNnSc1	svalstvo
končetiny	končetina	k1gFnSc2	končetina
se	se	k3xPyFc4	se
upíná	upínat	k5eAaImIp3nS	upínat
na	na	k7c4	na
synsakrum	synsakrum	k1gNnSc4	synsakrum
<g/>
,	,	kIx,	,
femur	femur	k1gMnSc1	femur
a	a	k8xC	a
tibiotarzum	tibiotarzum	k1gInSc1	tibiotarzum
<g/>
.	.	kIx.	.
</s>
<s>
Běhák	běhák	k1gInSc1	běhák
svalovinu	svalovina	k1gFnSc4	svalovina
nemá	mít	k5eNaImIp3nS	mít
<g/>
,	,	kIx,	,
podélně	podélně	k6eAd1	podélně
jím	on	k3xPp3gNnSc7	on
vedou	vést	k5eAaImIp3nP	vést
jen	jen	k9	jen
šlachy	šlacha	k1gFnPc1	šlacha
k	k	k7c3	k
prstům	prst	k1gInPc3	prst
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavá	zajímavý	k2eAgFnSc1d1	zajímavá
je	být	k5eAaImIp3nS	být
úprava	úprava	k1gFnSc1	úprava
některých	některý	k3yIgInPc2	některý
svalů	sval	k1gInPc2	sval
zadní	zadní	k2eAgFnSc2d1	zadní
končetiny	končetina	k1gFnSc2	končetina
(	(	kIx(	(
<g/>
m.	m.	k?	m.
ambiens	ambiens	k1gInSc1	ambiens
<g/>
,	,	kIx,	,
mm	mm	kA	mm
<g/>
.	.	kIx.	.
flexores	flexores	k1gMnSc1	flexores
perforantes	perforantesa	k1gFnPc2	perforantesa
et	et	k?	et
perforati	perforat	k1gMnPc1	perforat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
automatické	automatický	k2eAgNnSc4d1	automatické
sevření	sevření	k1gNnSc4	sevření
prstů	prst	k1gInPc2	prst
hřadujícího	hřadující	k2eAgMnSc2d1	hřadující
ptáka	pták	k1gMnSc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
napnutí	napnutí	k1gNnSc6	napnutí
šlach	šlacha	k1gFnPc2	šlacha
těchto	tento	k3xDgInPc2	tento
svalů	sval	k1gInPc2	sval
zapadnou	zapadnout	k5eAaPmIp3nP	zapadnout
jejich	jejich	k3xOp3gInPc1	jejich
výrůstky	výrůstek	k1gInPc1	výrůstek
do	do	k7c2	do
prohlubní	prohlubeň	k1gFnPc2	prohlubeň
šlachových	šlachový	k2eAgFnPc2d1	šlachová
pochev	pochva	k1gFnPc2	pochva
jako	jako	k8xC	jako
řetěz	řetěz	k1gInSc1	řetěz
do	do	k7c2	do
ozubeného	ozubený	k2eAgNnSc2d1	ozubené
kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
sedící	sedící	k2eAgMnSc1d1	sedící
pták	pták	k1gMnSc1	pták
už	už	k6eAd1	už
nevynakládá	vynakládat	k5eNaImIp3nS	vynakládat
žádnou	žádný	k3yNgFnSc4	žádný
svalovou	svalový	k2eAgFnSc4d1	svalová
námahu	námaha	k1gFnSc4	námaha
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Trávicí	trávicí	k2eAgFnSc1d1	trávicí
soustava	soustava	k1gFnSc1	soustava
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Trávicí	trávicí	k2eAgInSc1d1	trávicí
systém	systém	k1gInSc1	systém
(	(	kIx(	(
<g/>
systema	systema	k1gNnSc1	systema
digestorium	digestorium	k1gNnSc1	digestorium
<g/>
)	)	kIx)	)
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
u	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
základní	základní	k2eAgFnSc2d1	základní
stavbě	stavba	k1gFnSc3	stavba
u	u	k7c2	u
vyšších	vysoký	k2eAgMnPc2d2	vyšší
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c4	na
některé	některý	k3yIgFnPc4	některý
zvláštnosti	zvláštnost	k1gFnPc4	zvláštnost
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
savců	savec	k1gMnPc2	savec
lze	lze	k6eAd1	lze
jej	on	k3xPp3gMnSc4	on
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
část	část	k1gFnSc4	část
hlavovou	hlavový	k2eAgFnSc4d1	hlavová
a	a	k8xC	a
trávicí	trávicí	k2eAgFnSc4d1	trávicí
trubici	trubice	k1gFnSc4	trubice
<g/>
.	.	kIx.	.
</s>
<s>
Hlavovou	hlavový	k2eAgFnSc4d1	hlavová
část	část	k1gFnSc4	část
tvoří	tvořit	k5eAaImIp3nS	tvořit
tzv.	tzv.	kA	tzv.
orofarynx	orofarynx	k1gInSc1	orofarynx
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
dutinu	dutina	k1gFnSc4	dutina
ústní	ústní	k2eAgFnSc4d1	ústní
(	(	kIx(	(
<g/>
zobákovou	zobákový	k2eAgFnSc4d1	zobákový
<g/>
)	)	kIx)	)
a	a	k8xC	a
hltan	hltan	k1gInSc4	hltan
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgFnSc1d1	horní
a	a	k8xC	a
dolní	dolní	k2eAgFnSc1d1	dolní
čelist	čelist	k1gFnSc1	čelist
kryje	krýt	k5eAaImIp3nS	krýt
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
zrohovatělý	zrohovatělý	k2eAgInSc4d1	zrohovatělý
zobák	zobák	k1gInSc4	zobák
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
neohebný	ohebný	k2eNgMnSc1d1	neohebný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
podložen	podložit	k5eAaPmNgInS	podložit
kostí	kost	k1gFnSc7	kost
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
jej	on	k3xPp3gInSc4	on
ptáci	pták	k1gMnPc1	pták
mohou	moct	k5eAaImIp3nP	moct
vysunovat	vysunovat	k5eAaImF	vysunovat
poměrně	poměrně	k6eAd1	poměrně
daleko	daleko	k6eAd1	daleko
z	z	k7c2	z
ústní	ústní	k2eAgFnSc2d1	ústní
dutiny	dutina	k1gFnSc2	dutina
<g/>
.	.	kIx.	.
</s>
<s>
Chuťové	chuťový	k2eAgInPc1d1	chuťový
pohárky	pohárek	k1gInPc1	pohárek
jsou	být	k5eAaImIp3nP	být
přítomny	přítomen	k2eAgInPc1d1	přítomen
na	na	k7c6	na
jazyku	jazyk	k1gInSc6	jazyk
a	a	k8xC	a
na	na	k7c6	na
stěně	stěna	k1gFnSc6	stěna
dutiny	dutina	k1gFnSc2	dutina
zobáku	zobák	k1gInSc2	zobák
<g/>
.	.	kIx.	.
</s>
<s>
Choana	Choana	k1gFnSc1	Choana
je	být	k5eAaImIp3nS	být
jednotná	jednotný	k2eAgFnSc1d1	jednotná
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
i	i	k9	i
hltanové	hltanový	k2eAgNnSc1d1	hltanový
vyústění	vyústění	k1gNnSc1	vyústění
sluchových	sluchový	k2eAgFnPc2d1	sluchová
čili	čili	k8xC	čili
Eustachových	Eustachův	k2eAgFnPc2d1	Eustachova
trubic	trubice	k1gFnPc2	trubice
<g/>
.	.	kIx.	.
</s>
<s>
Trávicí	trávicí	k2eAgFnSc4d1	trávicí
trubici	trubice	k1gFnSc4	trubice
představují	představovat	k5eAaImIp3nP	představovat
jícen	jícen	k1gInSc4	jícen
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
probíhá	probíhat	k5eAaImIp3nS	probíhat
po	po	k7c6	po
pravé	pravý	k2eAgFnSc6d1	pravá
straně	strana	k1gFnSc6	strana
krku	krk	k1gInSc2	krk
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
ptáků	pták	k1gMnPc2	pták
v	v	k7c6	v
distální	distální	k2eAgFnSc6d1	distální
části	část	k1gFnSc6	část
rozšířen	rozšířit	k5eAaPmNgMnS	rozšířit
ve	v	k7c4	v
vole	vole	k1gNnSc4	vole
<g/>
.	.	kIx.	.
</s>
<s>
Trávicí	trávicí	k2eAgFnSc1d1	trávicí
trubice	trubice	k1gFnSc1	trubice
dále	daleko	k6eAd2	daleko
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
jako	jako	k9	jako
žláznatý	žláznatý	k2eAgInSc1d1	žláznatý
a	a	k8xC	a
svalnatý	svalnatý	k2eAgInSc1d1	svalnatý
žaludek	žaludek	k1gInSc1	žaludek
<g/>
,	,	kIx,	,
tenké	tenký	k2eAgNnSc1d1	tenké
střevo	střevo	k1gNnSc1	střevo
a	a	k8xC	a
poměrně	poměrně	k6eAd1	poměrně
krátké	krátký	k2eAgNnSc4d1	krátké
tlusté	tlustý	k2eAgNnSc4d1	tlusté
střevo	střevo	k1gNnSc4	střevo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozhraní	rozhraní	k1gNnSc4	rozhraní
tenkého	tenký	k2eAgNnSc2d1	tenké
a	a	k8xC	a
tlustého	tlustý	k2eAgNnSc2d1	tlusté
střeva	střevo	k1gNnSc2	střevo
zpravidla	zpravidla	k6eAd1	zpravidla
odstupují	odstupovat	k5eAaImIp3nP	odstupovat
dvě	dva	k4xCgFnPc1	dva
různě	různě	k6eAd1	různě
velká	velký	k2eAgNnPc4d1	velké
slepá	slepý	k2eAgNnPc4d1	slepé
střeva	střevo	k1gNnPc4	střevo
<g/>
.	.	kIx.	.
</s>
<s>
Trávicí	trávicí	k2eAgFnSc1d1	trávicí
trubice	trubice	k1gFnSc1	trubice
končí	končit	k5eAaImIp3nS	končit
společně	společně	k6eAd1	společně
s	s	k7c7	s
vývodnými	vývodný	k2eAgFnPc7d1	vývodná
cestami	cesta	k1gFnPc7	cesta
močovými	močový	k2eAgFnPc7d1	močová
a	a	k8xC	a
pohlavními	pohlavní	k2eAgFnPc7d1	pohlavní
v	v	k7c6	v
kloace	kloaka	k1gFnSc6	kloaka
<g/>
.	.	kIx.	.
</s>
<s>
Žluč	žluč	k1gFnSc1	žluč
do	do	k7c2	do
střeva	střevo	k1gNnSc2	střevo
přivádějí	přivádět	k5eAaImIp3nP	přivádět
dva	dva	k4xCgInPc4	dva
žlučovody	žlučovod	k1gInPc4	žlučovod
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
přítomnost	přítomnost	k1gFnSc4	přítomnost
nebo	nebo	k8xC	nebo
absenci	absence	k1gFnSc4	absence
žlučníku	žlučník	k1gInSc2	žlučník
<g/>
.	.	kIx.	.
</s>
<s>
Pankreatickou	pankreatický	k2eAgFnSc4d1	pankreatická
šťávu	šťáva	k1gFnSc4	šťáva
přivádějí	přivádět	k5eAaImIp3nP	přivádět
do	do	k7c2	do
střeva	střevo	k1gNnSc2	střevo
2-3	[number]	k4	2-3
vývody	vývoda	k1gMnPc7	vývoda
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Imunitní	imunitní	k2eAgInSc1d1	imunitní
systém	systém	k1gInSc1	systém
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Nejlépe	dobře	k6eAd3	dobře
prostudovaný	prostudovaný	k2eAgInSc1d1	prostudovaný
je	být	k5eAaImIp3nS	být
imunitní	imunitní	k2eAgInSc1d1	imunitní
systém	systém	k1gInSc1	systém
u	u	k7c2	u
kura	kur	k1gMnSc2	kur
domácího	domácí	k2eAgMnSc2d1	domácí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
funkci	funkce	k1gFnSc6	funkce
se	se	k3xPyFc4	se
podílejí	podílet	k5eAaImIp3nP	podílet
nespecifické	specifický	k2eNgInPc1d1	nespecifický
a	a	k8xC	a
specifické	specifický	k2eAgInPc1d1	specifický
obranné	obranný	k2eAgInPc1d1	obranný
mechanismy	mechanismus	k1gInPc1	mechanismus
organismu	organismus	k1gInSc2	organismus
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
vzájemně	vzájemně	k6eAd1	vzájemně
provázány	provázán	k2eAgFnPc1d1	provázána
a	a	k8xC	a
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
podmiňují	podmiňovat	k5eAaImIp3nP	podmiňovat
<g/>
.	.	kIx.	.
</s>
<s>
Morfologickým	morfologický	k2eAgInSc7d1	morfologický
podkladem	podklad	k1gInSc7	podklad
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
je	být	k5eAaImIp3nS	být
lymfatická	lymfatický	k2eAgFnSc1d1	lymfatická
tkáň	tkáň	k1gFnSc1	tkáň
<g/>
,	,	kIx,	,
uspořádaná	uspořádaný	k2eAgFnSc1d1	uspořádaná
do	do	k7c2	do
lymfatických	lymfatický	k2eAgInPc2d1	lymfatický
orgánů	orgán	k1gInPc2	orgán
souhrnně	souhrnně	k6eAd1	souhrnně
tvořících	tvořící	k2eAgInPc2d1	tvořící
lymfatický	lymfatický	k2eAgInSc4d1	lymfatický
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Obranné	obranný	k2eAgInPc1d1	obranný
mechanismy	mechanismus	k1gInPc1	mechanismus
ptáků	pták	k1gMnPc2	pták
jsou	být	k5eAaImIp3nP	být
analogické	analogický	k2eAgFnPc1d1	analogická
savčím	savčí	k2eAgFnPc3d1	savčí
<g/>
,	,	kIx,	,
výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
výrazná	výrazný	k2eAgFnSc1d1	výrazná
anatomická	anatomický	k2eAgFnSc1d1	anatomická
dichotomie	dichotomie	k1gFnSc1	dichotomie
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Močové	močový	k2eAgInPc1d1	močový
orgány	orgán	k1gInPc1	orgán
regulují	regulovat	k5eAaImIp3nP	regulovat
příjem	příjem	k1gInSc4	příjem
a	a	k8xC	a
výdej	výdej	k1gInSc4	výdej
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
udržují	udržovat	k5eAaImIp3nP	udržovat
homeostázu	homeostáza	k1gFnSc4	homeostáza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
se	se	k3xPyFc4	se
savci	savec	k1gMnPc1	savec
mají	mít	k5eAaImIp3nP	mít
ptáci	pták	k1gMnPc1	pták
odlišnou	odlišný	k2eAgFnSc4d1	odlišná
stavbu	stavba	k1gFnSc4	stavba
ledvin	ledvina	k1gFnPc2	ledvina
a	a	k8xC	a
chybí	chybit	k5eAaPmIp3nS	chybit
jim	on	k3xPp3gMnPc3	on
ledvinová	ledvinový	k2eAgFnSc1d1	ledvinová
pánvička	pánvička	k1gFnSc1	pánvička
<g/>
,	,	kIx,	,
močový	močový	k2eAgInSc1d1	močový
měchýř	měchýř	k1gInSc1	měchýř
(	(	kIx(	(
<g/>
výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
pštrosi	pštros	k1gMnPc1	pštros
<g/>
)	)	kIx)	)
i	i	k9	i
močová	močový	k2eAgFnSc1d1	močová
trubice	trubice	k1gFnSc1	trubice
<g/>
.	.	kIx.	.
</s>
<s>
Párové	párový	k2eAgFnSc2d1	párová
pravé	pravý	k2eAgFnSc2d1	pravá
ledviny	ledvina	k1gFnSc2	ledvina
(	(	kIx(	(
<g/>
metanefros	metanefrosa	k1gFnPc2	metanefrosa
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
protáhlého	protáhlý	k2eAgInSc2d1	protáhlý
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
třílaločnaté	třílaločnatý	k2eAgInPc1d1	třílaločnatý
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
uloženy	uložit	k5eAaPmNgInP	uložit
v	v	k7c6	v
zadní	zadní	k2eAgFnSc6d1	zadní
části	část	k1gFnSc6	část
tělní	tělní	k2eAgFnPc1d1	tělní
dutiny	dutina	k1gFnPc1	dutina
v	v	k7c6	v
prohlubních	prohlubeň	k1gFnPc6	prohlubeň
synsakra	synsakr	k1gInSc2	synsakr
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
je	být	k5eAaImIp3nS	být
vytvořena	vytvořen	k2eAgFnSc1d1	vytvořena
Henleova	Henleův	k2eAgFnSc1d1	Henleova
klička	klička	k1gFnSc1	klička
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
přispívá	přispívat	k5eAaImIp3nS	přispívat
ke	k	k7c3	k
zpětné	zpětný	k2eAgFnSc3d1	zpětná
resorpci	resorpce	k1gFnSc3	resorpce
vody	voda	k1gFnSc2	voda
z	z	k7c2	z
primární	primární	k2eAgFnSc2d1	primární
moče	moč	k1gFnSc2	moč
a	a	k8xC	a
tvorbě	tvorba	k1gFnSc6	tvorba
vlastní	vlastní	k2eAgFnSc2d1	vlastní
hypertonické	hypertonický	k2eAgFnSc2d1	hypertonická
moče	moč	k1gFnSc2	moč
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zvětšen	zvětšen	k2eAgInSc1d1	zvětšen
počet	počet	k1gInSc1	počet
glomerulů	glomerul	k1gMnPc2	glomerul
<g/>
;	;	kIx,	;
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
resorpci	resorpce	k1gFnSc3	resorpce
vody	voda	k1gFnSc2	voda
i	i	k8xC	i
v	v	k7c6	v
kloace	kloaka	k1gFnSc6	kloaka
<g/>
.	.	kIx.	.
</s>
<s>
Zbytky	zbytek	k1gInPc1	zbytek
vrátnicového	vrátnicový	k2eAgInSc2d1	vrátnicový
oběhu	oběh	k1gInSc2	oběh
v	v	k7c6	v
ledvinách	ledvina	k1gFnPc6	ledvina
jsou	být	k5eAaImIp3nP	být
zachovány	zachovat	k5eAaPmNgInP	zachovat
<g/>
.	.	kIx.	.
</s>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
močová	močový	k2eAgFnSc1d1	močová
se	se	k3xPyFc4	se
dopravuje	dopravovat	k5eAaImIp3nS	dopravovat
krví	krev	k1gFnSc7	krev
z	z	k7c2	z
jater	játra	k1gNnPc2	játra
do	do	k7c2	do
Malpighiho	Malpighiha	k1gFnSc5	Malpighiha
tělísek	tělísko	k1gNnPc2	tělísko
a	a	k8xC	a
zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
filtrována	filtrovat	k5eAaImNgFnS	filtrovat
z	z	k7c2	z
glomerulů	glomerul	k1gInPc2	glomerul
<g/>
.	.	kIx.	.
</s>
<s>
Párové	párový	k2eAgInPc1d1	párový
močovody	močovod	k1gInPc1	močovod
odvádějí	odvádět	k5eAaImIp3nP	odvádět
tekutou	tekutý	k2eAgFnSc4d1	tekutá
moč	moč	k1gFnSc4	moč
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
středního	střední	k2eAgInSc2d1	střední
oddílu	oddíl	k1gInSc2	oddíl
kloaky	kloaka	k1gFnSc2	kloaka
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
koprodea	koprode	k1gInSc2	koprode
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
po	po	k7c6	po
odnětí	odnětí	k1gNnSc6	odnětí
zbylé	zbylý	k2eAgFnSc2d1	zbylá
vody	voda	k1gFnSc2	voda
ukládá	ukládat	k5eAaImIp3nS	ukládat
bělavá	bělavý	k2eAgFnSc1d1	bělavá
kyselina	kyselina	k1gFnSc1	kyselina
močová	močový	k2eAgFnSc1d1	močová
a	a	k8xC	a
jiné	jiný	k2eAgInPc4d1	jiný
uráty	urát	k1gInPc4	urát
jako	jako	k8xC	jako
povrchový	povrchový	k2eAgInSc4d1	povrchový
povlak	povlak	k1gInSc4	povlak
na	na	k7c4	na
trus	trus	k1gInSc4	trus
<g/>
.	.	kIx.	.
</s>
<s>
Trus	trus	k1gInSc1	trus
s	s	k7c7	s
močí	moč	k1gFnSc7	moč
pak	pak	k6eAd1	pak
odchází	odcházet	k5eAaImIp3nS	odcházet
středním	střední	k2eAgInSc7d1	střední
a	a	k8xC	a
vnějším	vnější	k2eAgInSc7d1	vnější
oddílem	oddíl	k1gInSc7	oddíl
kloaky	kloaka	k1gFnSc2	kloaka
ven	ven	k6eAd1	ven
z	z	k7c2	z
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Rozmnožovací	rozmnožovací	k2eAgFnSc1d1	rozmnožovací
soustava	soustava	k1gFnSc1	soustava
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Způsob	způsob	k1gInSc1	způsob
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
dokonalejší	dokonalý	k2eAgInPc1d2	dokonalejší
než	než	k8xS	než
u	u	k7c2	u
plazů	plaz	k1gMnPc2	plaz
(	(	kIx(	(
<g/>
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
nich	on	k3xPp3gFnPc2	on
vyvinuty	vyvinout	k5eAaPmNgFnP	vyvinout
různé	různý	k2eAgFnPc1d1	různá
formy	forma	k1gFnPc1	forma
rodičovských	rodičovský	k2eAgInPc2d1	rodičovský
instinktů	instinkt	k1gInPc2	instinkt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavní	pohlavní	k2eAgInPc1d1	pohlavní
orgány	orgán	k1gInPc1	orgán
se	se	k3xPyFc4	se
však	však	k9	však
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
úsporností	úspornost	k1gFnSc7	úspornost
stavby	stavba	k1gFnSc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Samčími	samčí	k2eAgFnPc7d1	samčí
gonádami	gonáda	k1gFnPc7	gonáda
jsou	být	k5eAaImIp3nP	být
párová	párový	k2eAgNnPc1d1	párové
fazolovitá	fazolovitý	k2eAgNnPc1d1	fazolovitý
varlata	varle	k1gNnPc1	varle
(	(	kIx(	(
<g/>
pravé	pravá	k1gFnSc2	pravá
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgFnSc1d2	menší
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
většině	většina	k1gFnSc3	většina
ptáků	pták	k1gMnPc2	pták
chybí	chybit	k5eAaPmIp3nS	chybit
penis	penis	k1gInSc4	penis
a	a	k8xC	a
kopulují	kopulovat	k5eAaImIp3nP	kopulovat
přitištěním	přitištění	k1gNnSc7	přitištění
okrajů	okraj	k1gInPc2	okraj
kloak	kloaka	k1gFnPc2	kloaka
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
mají	mít	k5eAaImIp3nP	mít
zpravidla	zpravidla	k6eAd1	zpravidla
jen	jen	k9	jen
jeden	jeden	k4xCgInSc1	jeden
fungující	fungující	k2eAgInSc1d1	fungující
vaječník	vaječník	k1gInSc1	vaječník
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
levý	levý	k2eAgMnSc1d1	levý
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
levého	levý	k2eAgInSc2d1	levý
vejcovodu	vejcovod	k1gInSc2	vejcovod
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dýchací	dýchací	k2eAgFnSc1d1	dýchací
soustava	soustava	k1gFnSc1	soustava
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
mají	mít	k5eAaImIp3nP	mít
nejvýkonnější	výkonný	k2eAgInSc4d3	nejvýkonnější
dýchací	dýchací	k2eAgInSc4d1	dýchací
systém	systém	k1gInSc4	systém
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
aktivní	aktivní	k2eAgFnSc1d1	aktivní
plocha	plocha	k1gFnSc1	plocha
ptačích	ptačí	k2eAgFnPc2d1	ptačí
plic	plíce	k1gFnPc2	plíce
je	být	k5eAaImIp3nS	být
až	až	k9	až
o	o	k7c4	o
20	[number]	k4	20
<g/>
%	%	kIx~	%
větší	veliký	k2eAgNnPc4d2	veliký
než	než	k8xS	než
u	u	k7c2	u
savců	savec	k1gMnPc2	savec
<g/>
;	;	kIx,	;
také	také	k9	také
intenzita	intenzita	k1gFnSc1	intenzita
dýchání	dýchání	k1gNnSc2	dýchání
u	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
je	být	k5eAaImIp3nS	být
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
morfologickým	morfologický	k2eAgFnPc3d1	morfologická
i	i	k8xC	i
funkčním	funkční	k2eAgFnPc3d1	funkční
odlišnostem	odlišnost	k1gFnPc3	odlišnost
plic	plíce	k1gFnPc2	plíce
mnohem	mnohem	k6eAd1	mnohem
vyšší	vysoký	k2eAgFnPc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
<s>
Plíce	plíce	k1gFnPc1	plíce
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
poměrně	poměrně	k6eAd1	poměrně
malé	malý	k2eAgFnPc1d1	malá
<g/>
,	,	kIx,	,
kompaktní	kompaktní	k2eAgFnPc1d1	kompaktní
a	a	k8xC	a
při	při	k7c6	při
dýchání	dýchání	k1gNnSc6	dýchání
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gInSc1	jejich
objem	objem	k1gInSc1	objem
nemění	měnit	k5eNaImIp3nS	měnit
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
prakticky	prakticky	k6eAd1	prakticky
zhoršuje	zhoršovat	k5eAaImIp3nS	zhoršovat
jejich	jejich	k3xOp3gNnPc4	jejich
vyšetření	vyšetření	k1gNnPc4	vyšetření
auskultací	auskultace	k1gFnPc2	auskultace
<g/>
.	.	kIx.	.
</s>
<s>
Ventilace	ventilace	k1gFnSc1	ventilace
je	být	k5eAaImIp3nS	být
umožněna	umožnit	k5eAaPmNgFnS	umožnit
především	především	k9	především
systémem	systém	k1gInSc7	systém
vzdušných	vzdušný	k2eAgInPc2d1	vzdušný
vaků	vak	k1gInPc2	vak
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Oběhová	oběhový	k2eAgFnSc1d1	oběhová
soustava	soustava	k1gFnSc1	soustava
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Kardiovaskulární	kardiovaskulární	k2eAgInSc1d1	kardiovaskulární
systém	systém	k1gInSc1	systém
(	(	kIx(	(
<g/>
systema	systema	k1gFnSc1	systema
cardiovasculare	cardiovascular	k1gMnSc5	cardiovascular
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
u	u	k7c2	u
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
srdce	srdce	k1gNnSc1	srdce
<g/>
,	,	kIx,	,
krevní	krevní	k2eAgFnPc1d1	krevní
cévy	céva	k1gFnPc1	céva
a	a	k8xC	a
lymfatický	lymfatický	k2eAgInSc1d1	lymfatický
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
mají	mít	k5eAaImIp3nP	mít
relativně	relativně	k6eAd1	relativně
největší	veliký	k2eAgNnPc4d3	veliký
srdce	srdce	k1gNnPc4	srdce
z	z	k7c2	z
obratlovců	obratlovec	k1gMnPc2	obratlovec
a	a	k8xC	a
absolutně	absolutně	k6eAd1	absolutně
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
krevní	krevní	k2eAgInSc4d1	krevní
tlak	tlak	k1gInSc4	tlak
<g/>
,	,	kIx,	,
srdeční	srdeční	k2eAgFnSc4d1	srdeční
frekvenci	frekvence	k1gFnSc4	frekvence
a	a	k8xC	a
tělesnou	tělesný	k2eAgFnSc4d1	tělesná
teplotu	teplota	k1gFnSc4	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgInSc1d2	veliký
relativní	relativní	k2eAgInSc1d1	relativní
objem	objem	k1gInSc1	objem
srdce	srdce	k1gNnSc2	srdce
a	a	k8xC	a
vyšší	vysoký	k2eAgFnSc1d2	vyšší
tepová	tepový	k2eAgFnSc1d1	tepová
frekvence	frekvence	k1gFnSc1	frekvence
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
ptákům	pták	k1gMnPc3	pták
větší	veliký	k2eAgInSc4d2	veliký
minutový	minutový	k2eAgInSc4d1	minutový
objem	objem	k1gInSc4	objem
než	než	k8xS	než
u	u	k7c2	u
savců	savec	k1gMnPc2	savec
<g/>
.	.	kIx.	.
</s>
<s>
Plicní	plicní	k2eAgInSc4d1	plicní
a	a	k8xC	a
tělní	tělní	k2eAgInSc4d1	tělní
krevní	krevní	k2eAgInSc4d1	krevní
oběh	oběh	k1gInSc4	oběh
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
uspořádány	uspořádat	k5eAaPmNgInP	uspořádat
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
u	u	k7c2	u
savců	savec	k1gMnPc2	savec
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
mají	mít	k5eAaImIp3nP	mít
3	[number]	k4	3
portální	portální	k2eAgInSc4d1	portální
žilní	žilní	k2eAgInSc4d1	žilní
oběhy	oběh	k1gInPc4	oběh
(	(	kIx(	(
<g/>
hypofyzární	hypofyzární	k2eAgInSc4d1	hypofyzární
<g/>
,	,	kIx,	,
jaterní	jaterní	k2eAgInSc4d1	jaterní
<g/>
,	,	kIx,	,
ledvinový	ledvinový	k2eAgInSc4d1	ledvinový
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Smyslová	smyslový	k2eAgFnSc1d1	smyslová
soustava	soustava	k1gFnSc1	soustava
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
řídí	řídit	k5eAaImIp3nP	řídit
svá	svůj	k3xOyFgNnPc4	svůj
jednání	jednání	k1gNnPc4	jednání
zejména	zejména	k6eAd1	zejména
zrakem	zrak	k1gInSc7	zrak
<g/>
,	,	kIx,	,
sluchem	sluch	k1gInSc7	sluch
a	a	k8xC	a
hmatem	hmat	k1gInSc7	hmat
<g/>
.	.	kIx.	.
</s>
<s>
Smyslové	smyslový	k2eAgInPc1d1	smyslový
receptory	receptor	k1gInPc1	receptor
mohou	moct	k5eAaImIp3nP	moct
představovat	představovat	k5eAaImF	představovat
samostatné	samostatný	k2eAgInPc1d1	samostatný
orgány	orgán	k1gInPc1	orgán
(	(	kIx(	(
<g/>
ucho	ucho	k1gNnSc1	ucho
<g/>
,	,	kIx,	,
oko	oko	k1gNnSc1	oko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tvořit	tvořit	k5eAaImF	tvořit
shluky	shluk	k1gInPc4	shluk
ve	v	k7c6	v
sliznici	sliznice	k1gFnSc6	sliznice
nosní	nosní	k2eAgFnSc2d1	nosní
a	a	k8xC	a
ústní	ústní	k2eAgFnSc2d1	ústní
dutiny	dutina	k1gFnSc2	dutina
(	(	kIx(	(
<g/>
čich	čich	k1gInSc1	čich
<g/>
,	,	kIx,	,
chuť	chuť	k1gFnSc1	chuť
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
tvořit	tvořit	k5eAaImF	tvořit
orgány	orgán	k1gInPc4	orgán
s	s	k7c7	s
povrchovou	povrchový	k2eAgFnSc7d1	povrchová
a	a	k8xC	a
hlubokou	hluboký	k2eAgFnSc7d1	hluboká
citlivostí	citlivost	k1gFnSc7	citlivost
(	(	kIx(	(
<g/>
organa	organon	k1gNnSc2	organon
sensoria	sensorium	k1gNnSc2	sensorium
accessoria	accessorium	k1gNnSc2	accessorium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Endokrinní	endokrinní	k2eAgInSc1d1	endokrinní
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
analogický	analogický	k2eAgMnSc1d1	analogický
se	s	k7c7	s
savci	savec	k1gMnPc7	savec
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
existují	existovat	k5eAaImIp3nP	existovat
odlišnosti	odlišnost	k1gFnPc4	odlišnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
adenohypofýze	adenohypofýza	k1gFnSc6	adenohypofýza
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
postrádá	postrádat	k5eAaImIp3nS	postrádat
střední	střední	k2eAgInSc4d1	střední
lalok	lalok	k1gInSc4	lalok
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
zjištěny	zjistit	k5eAaPmNgInP	zjistit
folikuly	folikul	k1gInPc1	folikul
stimulující	stimulující	k2eAgInPc1d1	stimulující
hormon	hormon	k1gInSc1	hormon
<g/>
,	,	kIx,	,
luteinizační	luteinizační	k2eAgInSc1d1	luteinizační
hormon	hormon	k1gInSc1	hormon
(	(	kIx(	(
<g/>
svatební	svatební	k2eAgNnSc1d1	svatební
opeření	opeření	k1gNnSc1	opeření
samců	samec	k1gMnPc2	samec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
thyreotropní	thyreotropní	k2eAgInSc1d1	thyreotropní
hormon	hormon	k1gInSc1	hormon
<g/>
,	,	kIx,	,
adrenokortikotropní	adrenokortikotropní	k2eAgInSc1d1	adrenokortikotropní
hormon	hormon	k1gInSc1	hormon
<g/>
,	,	kIx,	,
luteotropní	luteotropní	k2eAgInSc1d1	luteotropní
hormon	hormon	k1gInSc1	hormon
(	(	kIx(	(
<g/>
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
hnízdní	hnízdní	k2eAgInSc1d1	hnízdní
pud	pud	k1gInSc1	pud
a	a	k8xC	a
u	u	k7c2	u
holubů	holub	k1gMnPc2	holub
produkci	produkce	k1gFnSc4	produkce
kašovité	kašovitý	k2eAgFnSc2d1	kašovitá
hmoty	hmota	k1gFnSc2	hmota
ke	k	k7c3	k
krmení	krmení	k1gNnSc3	krmení
mláďat	mládě	k1gNnPc2	mládě
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neurohypofýze	neurohypofýza	k1gFnSc6	neurohypofýza
byl	být	k5eAaImAgInS	být
zjištěn	zjištěn	k2eAgInSc1d1	zjištěn
oxytocin	oxytocin	k1gInSc1	oxytocin
(	(	kIx(	(
<g/>
snáška	snáška	k1gFnSc1	snáška
<g/>
)	)	kIx)	)
a	a	k8xC	a
antidiuretický	antidiuretický	k2eAgInSc1d1	antidiuretický
hormon	hormon	k1gInSc1	hormon
(	(	kIx(	(
<g/>
regulace	regulace	k1gFnSc1	regulace
hospodaření	hospodaření	k1gNnSc2	hospodaření
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Brzlík	brzlík	k1gInSc1	brzlík
funguje	fungovat	k5eAaImIp3nS	fungovat
jednak	jednak	k8xC	jednak
jako	jako	k8xS	jako
endokrinní	endokrinní	k2eAgFnSc1d1	endokrinní
žláza	žláza	k1gFnSc1	žláza
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
jako	jako	k9	jako
krvetvorný	krvetvorný	k2eAgInSc4d1	krvetvorný
orgán	orgán	k1gInSc4	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Hormon	hormon	k1gInSc1	hormon
štítné	štítný	k2eAgFnSc2d1	štítná
žlázy	žláza	k1gFnSc2	žláza
–	–	k?	–
tyroxin	tyroxin	k1gInSc1	tyroxin
<g/>
,	,	kIx,	,
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
metabolismus	metabolismus	k1gInSc1	metabolismus
<g/>
,	,	kIx,	,
růst	růst	k1gInSc1	růst
<g/>
,	,	kIx,	,
pelichání	pelichání	k1gNnSc1	pelichání
i	i	k8xC	i
chování	chování	k1gNnSc1	chování
ptáků	pták	k1gMnPc2	pták
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
tahem	tah	k1gInSc7	tah
<g/>
.	.	kIx.	.
</s>
<s>
Parathormon	Parathormon	k1gNnSc1	Parathormon
příštítných	příštítný	k2eAgNnPc2d1	příštítný
tělísek	tělísko	k1gNnPc2	tělísko
řídí	řídit	k5eAaImIp3nS	řídit
hospodaření	hospodaření	k1gNnSc1	hospodaření
vápníkem	vápník	k1gInSc7	vápník
a	a	k8xC	a
fosforem	fosfor	k1gInSc7	fosfor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nadledvinách	nadledvina	k1gFnPc6	nadledvina
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
savců	savec	k1gMnPc2	savec
nejsou	být	k5eNaImIp3nP	být
korová	korový	k2eAgFnSc1d1	korová
a	a	k8xC	a
dřeňová	dřeňový	k2eAgFnSc1d1	dřeňová
vrstva	vrstva	k1gFnSc1	vrstva
morfologicky	morfologicky	k6eAd1	morfologicky
zřetelně	zřetelně	k6eAd1	zřetelně
odděleny	oddělen	k2eAgInPc1d1	oddělen
<g/>
;	;	kIx,	;
kůra	kůra	k1gFnSc1	kůra
produkuje	produkovat	k5eAaImIp3nS	produkovat
kortikoidy	kortikoid	k1gInPc4	kortikoid
a	a	k8xC	a
dřeň	dřeň	k1gFnSc1	dřeň
adrenalin	adrenalin	k1gInSc1	adrenalin
<g/>
.	.	kIx.	.
</s>
<s>
Produkce	produkce	k1gFnSc1	produkce
insulinu	insulin	k1gInSc2	insulin
a	a	k8xC	a
glukagonu	glukagon	k1gInSc2	glukagon
v	v	k7c6	v
ostrůvkovité	ostrůvkovitý	k2eAgFnSc6d1	ostrůvkovitá
endokrinní	endokrinní	k2eAgFnSc6d1	endokrinní
části	část	k1gFnSc6	část
pankreatu	pankreas	k1gInSc2	pankreas
je	být	k5eAaImIp3nS	být
shodná	shodný	k2eAgFnSc1d1	shodná
s	s	k7c7	s
poměry	poměr	k1gInPc7	poměr
u	u	k7c2	u
savců	savec	k1gMnPc2	savec
i	i	k8xC	i
plazů	plaz	k1gMnPc2	plaz
<g/>
.	.	kIx.	.
</s>
<s>
Gonády	gonáda	k1gFnPc1	gonáda
obou	dva	k4xCgNnPc2	dva
pohlaví	pohlaví	k1gNnPc2	pohlaví
produkují	produkovat	k5eAaImIp3nP	produkovat
pohlavní	pohlavní	k2eAgInPc1d1	pohlavní
hormony	hormon	k1gInPc1	hormon
(	(	kIx(	(
<g/>
androgeny	androgen	k1gInPc1	androgen
i	i	k8xC	i
estrogeny	estrogen	k1gInPc1	estrogen
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
okolností	okolnost	k1gFnPc2	okolnost
však	však	k9	však
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
skupin	skupina	k1gFnPc2	skupina
převládá	převládat	k5eAaImIp3nS	převládat
a	a	k8xC	a
určuje	určovat	k5eAaImIp3nS	určovat
mj.	mj.	kA	mj.
i	i	k8xC	i
sekundární	sekundární	k2eAgInPc1d1	sekundární
pohlavní	pohlavní	k2eAgInPc1d1	pohlavní
znaky	znak	k1gInPc1	znak
<g/>
.	.	kIx.	.
</s>
<s>
Nervová	nervový	k2eAgFnSc1d1	nervová
soustava	soustava	k1gFnSc1	soustava
ptáků	pták	k1gMnPc2	pták
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
vyšším	vysoký	k2eAgInSc6d2	vyšší
stupni	stupeň	k1gInSc6	stupeň
vývoje	vývoj	k1gInSc2	vývoj
než	než	k8xS	než
u	u	k7c2	u
plazů	plaz	k1gMnPc2	plaz
a	a	k8xC	a
proto	proto	k8xC	proto
jejich	jejich	k3xOp3gInPc1	jejich
životní	životní	k2eAgInPc1d1	životní
projevy	projev	k1gInPc1	projev
jsou	být	k5eAaImIp3nP	být
dokonalejší	dokonalý	k2eAgInPc1d2	dokonalejší
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
schopnost	schopnost	k1gFnSc4	schopnost
přizpůsobovat	přizpůsobovat	k5eAaImF	přizpůsobovat
se	se	k3xPyFc4	se
podmínkám	podmínka	k1gFnPc3	podmínka
prostředí	prostředí	k1gNnSc2	prostředí
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
nervové	nervový	k2eAgFnSc6d1	nervová
soustavě	soustava	k1gFnSc6	soustava
(	(	kIx(	(
<g/>
CNS	CNS	kA	CNS
<g/>
)	)	kIx)	)
ptáků	pták	k1gMnPc2	pták
jsou	být	k5eAaImIp3nP	být
vyvinuty	vyvinout	k5eAaPmNgInP	vyvinout
zvláště	zvláště	k6eAd1	zvláště
ty	ten	k3xDgFnPc1	ten
části	část	k1gFnPc1	část
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
přímý	přímý	k2eAgInSc4d1	přímý
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
ústrojím	ústroj	k1gFnPc3	ústroj
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
zraku	zrak	k1gInSc2	zrak
a	a	k8xC	a
sluchu	sluch	k1gInSc2	sluch
<g/>
.	.	kIx.	.
</s>
<s>
Neobyčejně	obyčejně	k6eNd1	obyčejně
bohatý	bohatý	k2eAgInSc4d1	bohatý
instinktivní	instinktivní	k2eAgInSc4d1	instinktivní
život	život	k1gInSc4	život
ptáků	pták	k1gMnPc2	pták
podmínil	podmínit	k5eAaPmAgMnS	podmínit
rozvoj	rozvoj	k1gInSc4	rozvoj
koncového	koncový	k2eAgInSc2d1	koncový
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
již	již	k9	již
všechna	všechen	k3xTgNnPc4	všechen
vyšší	vysoký	k2eAgNnPc4d2	vyšší
ústředí	ústředí	k1gNnPc4	ústředí
kromě	kromě	k7c2	kromě
zrakového	zrakový	k2eAgNnSc2d1	zrakové
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
lokalizováno	lokalizovat	k5eAaBmNgNnS	lokalizovat
v	v	k7c6	v
dobře	dobře	k6eAd1	dobře
vyvinutém	vyvinutý	k2eAgInSc6d1	vyvinutý
středním	střední	k2eAgInSc6d1	střední
mozku	mozek	k1gInSc6	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Koncový	koncový	k2eAgInSc1d1	koncový
mozek	mozek	k1gInSc1	mozek
je	být	k5eAaImIp3nS	být
nejmohutnější	mohutný	k2eAgFnSc7d3	nejmohutnější
částí	část	k1gFnSc7	část
celého	celý	k2eAgInSc2d1	celý
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
povrch	povrch	k1gInSc4	povrch
hemisfér	hemisféra	k1gFnPc2	hemisféra
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
gyrifikován	gyrifikován	k2eAgMnSc1d1	gyrifikován
jako	jako	k8xC	jako
u	u	k7c2	u
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
získal	získat	k5eAaPmAgMnS	získat
na	na	k7c6	na
objemu	objem	k1gInSc6	objem
zvětšením	zvětšení	k1gNnSc7	zvětšení
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
hmoty	hmota	k1gFnSc2	hmota
bazálních	bazální	k2eAgFnPc2d1	bazální
ganglií	ganglie	k1gFnPc2	ganglie
a	a	k8xC	a
neopália	neopálium	k1gNnSc2	neopálium
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
mozeček	mozeček	k1gInSc1	mozeček
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
rozvinut	rozvinut	k2eAgMnSc1d1	rozvinut
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
zvětšené	zvětšený	k2eAgInPc1d1	zvětšený
laloky	lalok	k1gInPc1	lalok
jsou	být	k5eAaImIp3nP	být
zvrásněné	zvrásněný	k2eAgInPc1d1	zvrásněný
jako	jako	k8xC	jako
u	u	k7c2	u
savců	savec	k1gMnPc2	savec
<g/>
.	.	kIx.	.
</s>
<s>
Rozvinuty	rozvinut	k2eAgInPc1d1	rozvinut
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
zrakové	zrakový	k2eAgInPc1d1	zrakový
hrboly	hrbol	k1gInPc1	hrbol
ve	v	k7c6	v
stropu	strop	k1gInSc3	strop
středního	střední	k2eAgInSc2d1	střední
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
zrakové	zrakový	k2eAgNnSc1d1	zrakové
ústředí	ústředí	k1gNnSc1	ústředí
<g/>
.	.	kIx.	.
</s>
<s>
Čichové	čichový	k2eAgInPc1d1	čichový
laloky	lalok	k1gInPc1	lalok
jsou	být	k5eAaImIp3nP	být
naopak	naopak	k6eAd1	naopak
silně	silně	k6eAd1	silně
redukovány	redukován	k2eAgFnPc1d1	redukována
<g/>
.	.	kIx.	.
</s>
<s>
Mícha	mícha	k1gFnSc1	mícha
sahá	sahat	k5eAaImIp3nS	sahat
od	od	k7c2	od
mozku	mozek	k1gInSc2	mozek
až	až	k6eAd1	až
do	do	k7c2	do
posledního	poslední	k2eAgInSc2d1	poslední
ocasního	ocasní	k2eAgInSc2d1	ocasní
obratle	obratel	k1gInSc2	obratel
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
přechodu	přechod	k1gInSc6	přechod
z	z	k7c2	z
krční	krční	k2eAgFnSc2d1	krční
do	do	k7c2	do
hrudní	hrudní	k2eAgFnSc2d1	hrudní
oblasti	oblast	k1gFnSc2	oblast
a	a	k8xC	a
v	v	k7c6	v
pánevní	pánevní	k2eAgFnSc6d1	pánevní
krajině	krajina	k1gFnSc6	krajina
je	být	k5eAaImIp3nS	být
zduřelá	zduřelý	k2eAgFnSc1d1	zduřelá
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
odstupu	odstup	k1gInSc2	odstup
motorických	motorický	k2eAgInPc2d1	motorický
nervů	nerv	k1gInPc2	nerv
inervujících	inervující	k2eAgInPc2d1	inervující
létací	létací	k2eAgInPc4d1	létací
svaly	sval	k1gInPc4	sval
a	a	k8xC	a
svaly	sval	k1gInPc4	sval
běháku	běhák	k1gInSc2	běhák
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ptačí	ptačí	k2eAgFnSc4d1	ptačí
míchu	mícha	k1gFnSc4	mícha
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
toto	tento	k3xDgNnSc4	tento
zesílení	zesílení	k1gNnSc4	zesílení
(	(	kIx(	(
<g/>
hřbetní	hřbetní	k2eAgFnSc1d1	hřbetní
výduť	výduť	k1gFnSc1	výduť
<g/>
)	)	kIx)	)
v	v	k7c6	v
bederní	bederní	k2eAgFnSc6d1	bederní
a	a	k8xC	a
křížové	křížový	k2eAgFnSc6d1	křížová
krajině	krajina	k1gFnSc6	krajina
<g/>
,	,	kIx,	,
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
velké	velký	k2eAgFnSc2d1	velká
koncentrace	koncentrace	k1gFnSc2	koncentrace
pojivových	pojivový	k2eAgFnPc2d1	pojivová
(	(	kIx(	(
<g/>
neurogliových	urogliový	k2eNgFnPc2d1	urogliový
<g/>
)	)	kIx)	)
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Míšní	míšní	k2eAgInPc4d1	míšní
nervy	nerv	k1gInPc4	nerv
i	i	k8xC	i
vegetativní	vegetativní	k2eAgInSc4d1	vegetativní
nervový	nervový	k2eAgInSc4d1	nervový
systém	systém	k1gInSc4	systém
jsou	být	k5eAaImIp3nP	být
dobře	dobře	k6eAd1	dobře
vyvinuty	vyvinout	k5eAaPmNgFnP	vyvinout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vymezení	vymezení	k1gNnSc1	vymezení
sympatických	sympatický	k2eAgNnPc2d1	sympatické
a	a	k8xC	a
parasympatických	parasympatický	k2eAgNnPc2d1	parasympatické
vláken	vlákno	k1gNnPc2	vlákno
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
problematické	problematický	k2eAgNnSc1d1	problematické
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
útrobní	útrobní	k2eAgInPc1d1	útrobní
nervy	nerv	k1gInPc1	nerv
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
obojí	oboj	k1gFnSc7	oboj
vlákna	vlákno	k1gNnSc2	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
ČERNÝ	Černý	k1gMnSc1	Černý
<g/>
,	,	kIx,	,
Hugo	Hugo	k1gMnSc1	Hugo
<g/>
.	.	kIx.	.
</s>
<s>
Anatomie	anatomie	k1gFnSc1	anatomie
domácích	domácí	k2eAgMnPc2d1	domácí
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
Metoda	metoda	k1gFnSc1	metoda
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
447	[number]	k4	447
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
239	[number]	k4	239
<g/>
-	-	kIx~	-
<g/>
4966	[number]	k4	4966
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
ALTMAN	Altman	k1gMnSc1	Altman
<g/>
,	,	kIx,	,
R.	R.	kA	R.
<g/>
B.	B.	kA	B.
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
..	..	k?	..
Avian	Avian	k1gInSc1	Avian
Medicine	Medicin	k1gInSc5	Medicin
and	and	k?	and
Surgery	Surger	k1gMnPc7	Surger
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
:	:	kIx,	:
W.B.	W.B.	k1gFnSc1	W.B.
Saunders	Saunders	k1gInSc1	Saunders
Co	co	k3yInSc1	co
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
1070	[number]	k4	1070
s.	s.	k?	s.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
RITCHIE	RITCHIE	kA	RITCHIE
<g/>
,	,	kIx,	,
B.W.	B.W.	k1gFnSc1	B.W.
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
..	..	k?	..
Avian	Avian	k1gInSc1	Avian
Medicine	Medicin	k1gInSc5	Medicin
<g/>
:	:	kIx,	:
Principles	Principles	k1gInSc1	Principles
and	and	k?	and
Application	Application	k1gInSc1	Application
<g/>
.	.	kIx.	.
</s>
<s>
Florida	Florida	k1gFnSc1	Florida
<g/>
,	,	kIx,	,
USA	USA	kA	USA
:	:	kIx,	:
Wingers	Wingers	k1gInSc1	Wingers
Publ	Publ	k1gInSc1	Publ
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
1384	[number]	k4	1384
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
9636996	[number]	k4	9636996
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
