<s>
Řasenka	řasenka	k1gFnSc1
je	být	k5eAaImIp3nS
druh	druh	k1gInSc4
kosmetického	kosmetický	k2eAgInSc2d1
výrobku	výrobek	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
pro	pro	k7c4
nanášení	nanášení	k1gNnSc4
barvy	barva	k1gFnSc2
na	na	k7c4
oční	oční	k2eAgFnPc4d1
řasy	řasa	k1gFnPc4
za	za	k7c7
účelem	účel	k1gInSc7
jejich	jejich	k3xOp3gNnSc2
zvětšení	zvětšení	k1gNnSc2
a	a	k8xC
zviditelnění	zviditelnění	k1gNnSc2
<g/>
.	.	kIx.
</s>