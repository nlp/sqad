<s>
PlayStation	PlayStation	k1gInSc1
4	#num#	k4
</s>
<s>
PlayStation	PlayStation	k1gInSc1
4	#num#	k4
</s>
<s>
PlayStation	PlayStation	k1gInSc4
4	#num#	k4
Výrobce	výrobce	k1gMnSc1
</s>
<s>
Sony	Sony	kA
Interactive	Interactiv	k1gInSc5
Entertainment	Entertainment	k1gInSc1
Produktová	produktový	k2eAgFnSc1d1
řada	řada	k1gFnSc1
</s>
<s>
PlayStation	PlayStation	k1gInSc4
Datum	datum	k1gInSc1
uvedení	uvedení	k1gNnSc1
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2013	#num#	k4
29	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2013	#num#	k4
13	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
2013	#num#	k4
22	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2014	#num#	k4
Typ	typ	k1gInSc1
</s>
<s>
Herní	herní	k2eAgFnSc3d1
konzole	konzola	k1gFnSc3
Generace	generace	k1gFnSc2
</s>
<s>
Osmá	osmý	k4xOgFnSc1
generace	generace	k1gFnSc1
Prodáno	prodat	k5eAaPmNgNnS
kusů	kus	k1gInPc2
</s>
<s>
102,8	102,8	k4
milionů	milion	k4xCgInPc2
(	(	kIx(
<g/>
říjen	říjen	k1gInSc1
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
Orbis	orbis	k1gInSc1
OS	OS	kA
<g/>
,	,	kIx,
založen	založit	k5eAaPmNgInS
na	na	k7c6
FreeBSD	FreeBSD	k1gFnSc6
9	#num#	k4
Procesor	procesor	k1gInSc4
</s>
<s>
AMD	AMD	kA
APU	APU	kA
Jaguar	Jaguar	k1gInSc4
<g/>
8	#num#	k4
jader	jádro	k1gNnPc2
CPU18	CPU18	k1gMnPc2
nebo	nebo	k8xC
36	#num#	k4
jader	jádro	k1gNnPc2
GPU	GPU	kA
<g/>
.	.	kIx.
<g/>
Dále	daleko	k6eAd2
pro	pro	k7c4
zpracování	zpracování	k1gNnSc4
zvuku	zvuk	k1gInSc2
DSP	DSP	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Poslední	poslední	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
na	na	k7c6
mainboardu	mainboard	k1gInSc6
konzole	konzola	k1gFnSc6
samostatný	samostatný	k2eAgInSc1d1
úsporný	úsporný	k2eAgInSc1d1
procesor	procesor	k1gInSc1
ARM	ARM	kA
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
řídí	řídit	k5eAaImIp3nS
stanici	stanice	k1gFnSc4
<g/>
,	,	kIx,
když	když	k8xS
je	být	k5eAaImIp3nS
APU	APU	kA
vypnuté	vypnutý	k2eAgInPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Úložiště	úložiště	k1gNnSc2
</s>
<s>
500	#num#	k4
GB	GB	kA
-	-	kIx~
1	#num#	k4
TB	TB	kA
-	-	kIx~
2	#num#	k4
TB	TB	kA
(	(	kIx(
<g/>
limitovaná	limitovaný	k2eAgFnSc1d1
edice	edice	k1gFnSc1
<g/>
)	)	kIx)
Média	médium	k1gNnPc1
</s>
<s>
Blu-ray	Blu-ray	k1gInPc1
<g/>
,	,	kIx,
DVD	DVD	kA
Připojitelné	připojitelný	k2eAgInPc1d1
ovladače	ovladač	k1gInPc1
</s>
<s>
DualShock	DualShock	k1gInSc1
4	#num#	k4
<g/>
,	,	kIx,
PlayStation	PlayStation	k1gInSc1
Camera	Camera	k1gFnSc1
<g/>
,	,	kIx,
PlayStation	PlayStation	k1gInSc1
Move	Mov	k1gFnSc2
<g/>
,	,	kIx,
PlayStation	PlayStation	k1gInSc4
Vita	vit	k2eAgFnSc1d1
Připojitelnost	připojitelnost	k1gFnSc1
</s>
<s>
Wi-Fi	Wi-Fi	k6eAd1
b	b	k?
<g/>
/	/	kIx~
<g/>
g	g	kA
<g/>
/	/	kIx~
<g/>
n	n	k0
<g/>
,	,	kIx,
Bluetooth	Bluetooth	k1gMnSc1
2.1	2.1	k4
<g/>
,	,	kIx,
USB	USB	kA
3.0	3.0	k4
<g/>
,	,	kIx,
Ethernet	Ethernet	k1gInSc1
10	#num#	k4
<g/>
/	/	kIx~
<g/>
100	#num#	k4
<g/>
/	/	kIx~
<g/>
1000	#num#	k4
Online	Onlin	k1gInSc5
služba	služba	k1gFnSc1
</s>
<s>
PlayStation	PlayStation	k1gInSc4
Network	network	k1gInPc2
Zpětná	zpětný	k2eAgFnSc1d1
kompatibilita	kompatibilita	k1gFnSc1
</s>
<s>
Některé	některý	k3yIgFnPc1
hry	hra	k1gFnPc1
z	z	k7c2
PS2	PS2	k1gFnSc2
(	(	kIx(
<g/>
emulace	emulace	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
též	též	k9
plánováno	plánován	k2eAgNnSc1d1
stream	stream	k6eAd1
online	onlinout	k5eAaPmIp3nS
<g/>
,	,	kIx,
pomocí	pomocí	k7c2
cloud	clouda	k1gFnPc2
služby	služba	k1gFnSc2
Gaikai	Gaika	k1gFnSc2
Předchůdce	předchůdce	k1gMnSc2
</s>
<s>
PlayStation	PlayStation	k1gInSc4
3	#num#	k4
Nástupce	nástupce	k1gMnSc1
</s>
<s>
PlayStation	PlayStation	k1gInSc1
5	#num#	k4
</s>
<s>
PlayStation	PlayStation	k1gInSc1
4	#num#	k4
(	(	kIx(
<g/>
PS	PS	kA
<g/>
4	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
herní	herní	k2eAgFnSc3d1
konzole	konzola	k1gFnSc3
od	od	k7c2
japonské	japonský	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
Sony	Sony	kA
Interactive	Interactiv	k1gInSc5
Entertainment	Entertainment	k1gInSc4
<g/>
,	,	kIx,
výrobu	výroba	k1gFnSc4
zajišťuje	zajišťovat	k5eAaImIp3nS
společnost	společnost	k1gFnSc1
Foxconn	Foxconna	k1gFnPc2
Technology	technolog	k1gMnPc4
Group	Group	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konzole	konzola	k1gFnSc6
byla	být	k5eAaImAgFnS
oznámena	oznámit	k5eAaPmNgFnS
jako	jako	k9
nástupce	nástupce	k1gMnSc1
PlayStationu	PlayStation	k1gInSc2
3	#num#	k4
během	během	k7c2
konference	konference	k1gFnSc2
svolané	svolaný	k2eAgFnSc2d1
20	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prodej	prodej	k1gFnSc1
konzole	konzola	k1gFnSc3
oficiálně	oficiálně	k6eAd1
začal	začít	k5eAaPmAgInS
15	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2013	#num#	k4
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
Česku	Česko	k1gNnSc6
byl	být	k5eAaImAgInS
prodej	prodej	k1gInSc1
zahájen	zahájit	k5eAaPmNgInS
13	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PlayStation	PlayStation	k1gInSc1
4	#num#	k4
patří	patřit	k5eAaImIp3nS
již	již	k6eAd1
do	do	k7c2
8	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
herních	herní	k2eAgFnPc2d1
konzolí	konzole	k1gFnPc2
spolu	spolu	k6eAd1
s	s	k7c7
konkurenčními	konkurenční	k2eAgFnPc7d1
konzolemi	konzole	k1gFnPc7
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
Nintendo	Nintendo	k1gNnSc1
Wii	Wii	k1gFnPc2
U	U	kA
a	a	k8xC
Microsoft	Microsoft	kA
Xbox	Xbox	k1gInSc4
One	One	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
druhou	druhý	k4xOgFnSc4
nejprodávanější	prodávaný	k2eAgFnSc4d3
herní	herní	k2eAgFnSc4d1
konzoli	konzole	k1gFnSc4
v	v	k7c6
historii	historie	k1gFnSc6
po	po	k7c4
PS	PS	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Společnost	společnost	k1gFnSc1
Sony	Sony	kA
při	při	k7c6
návrhu	návrh	k1gInSc6
PS4	PS4	k1gFnSc2
upustila	upustit	k5eAaPmAgFnS
od	od	k7c2
komplikované	komplikovaný	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
PlayStationu	PlayStation	k1gInSc2
3	#num#	k4
<g/>
,	,	kIx,
založené	založený	k2eAgInPc1d1
na	na	k7c6
procesoru	procesor	k1gInSc6
Cell	cello	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
PS4	PS4	k1gFnSc2
tak	tak	k6eAd1
je	být	k5eAaImIp3nS
použit	použit	k2eAgInSc1d1
model	model	k1gInSc1
běžný	běžný	k2eAgInSc1d1
u	u	k7c2
PC	PC	kA
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
představuje	představovat	k5eAaImIp3nS
zejména	zejména	k9
osmijádrové	osmijádrový	k2eAgInPc4d1
APU	APU	kA
od	od	k7c2
společnosti	společnost	k1gFnSc2
AMD	AMD	kA
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
grafická	grafický	k2eAgFnSc1d1
část	část	k1gFnSc1
u	u	k7c2
základního	základní	k2eAgInSc2d1
modelu	model	k1gInSc2
dosahuje	dosahovat	k5eAaImIp3nS
výkonu	výkon	k1gInSc2
1,84	1,84	k4
TeraFLOPs	TeraFLOPsa	k1gFnPc2
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
u	u	k7c2
modelu	model	k1gInSc2
PlayStation	PlayStation	k1gInSc1
4	#num#	k4
Pro	pro	k7c4
dosahuje	dosahovat	k5eAaImIp3nS
grafická	grafický	k2eAgFnSc1d1
část	část	k1gFnSc1
výkon	výkon	k1gInSc1
4,2	4,2	k4
TeraFLOPs	TeraFLOPsa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sony	Sony	kA
u	u	k7c2
své	svůj	k3xOyFgFnSc6
v	v	k7c6
pořadí	pořadí	k1gNnSc6
čtvrté	čtvrtý	k4xOgFnSc6
konzole	konzola	k1gFnSc6
má	mít	k5eAaImIp3nS
v	v	k7c6
úmyslu	úmysl	k1gInSc6
zaměření	zaměření	k1gNnSc2
na	na	k7c4
sociální	sociální	k2eAgNnSc4d1
hraní	hraní	k1gNnSc4
díky	díky	k7c3
tlačítku	tlačítko	k1gNnSc3
„	„	k?
<g/>
share	shar	k1gInSc5
<g/>
“	“	k?
(	(	kIx(
<g/>
angl.	angl.	k?
sdílet	sdílet	k5eAaImF
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
ovladači	ovladač	k1gInSc6
DualShock	DualShocko	k1gNnPc2
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
jeho	jeho	k3xOp3gNnSc6
stisknutí	stisknutí	k1gNnSc6
začnete	začít	k5eAaPmIp2nP
streamovat	streamovat	k5eAaImF,k5eAaBmF,k5eAaPmF
Vašim	váš	k3xOp2gMnPc3
přátelům	přítel	k1gMnPc3
in-game	in-gam	k1gInSc5
obraz	obraz	k1gInSc4
hry	hra	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
aktuálně	aktuálně	k6eAd1
hrajete	hrát	k5eAaImIp2nP
nebo	nebo	k8xC
naopak	naopak	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konzole	konzola	k1gFnSc6
umožňuje	umožňovat	k5eAaImIp3nS
propojení	propojení	k1gNnSc4
se	s	k7c7
službami	služba	k1gFnPc7
či	či	k8xC
zařízeními	zařízení	k1gNnPc7
následujícími	následující	k2eAgFnPc7d1
metodami	metoda	k1gFnPc7
<g/>
:	:	kIx,
Gaikai	Gaika	k1gFnPc1
<g/>
,	,	kIx,
herní	herní	k2eAgFnSc1d1
služba	služba	k1gFnSc1
postavena	postaven	k2eAgFnSc1d1
na	na	k7c4
cloudu	clouda	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
umožňuje	umožňovat	k5eAaImIp3nS
streamovat	streamovat	k5eAaImF,k5eAaPmF,k5eAaBmF
videoherní	videoherní	k2eAgInSc1d1
obsah	obsah	k1gInSc1
<g/>
;	;	kIx,
PlayStation	PlayStation	k1gInSc1
App	App	k1gFnSc2
<g/>
,	,	kIx,
program	program	k1gInSc4
navržený	navržený	k2eAgInSc4d1
pro	pro	k7c4
PS	PS	kA
<g/>
4	#num#	k4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
pomocí	pomocí	k7c2
chytrých	chytrý	k2eAgInPc2d1
telefonů	telefon	k1gInPc2
nebo	nebo	k8xC
tabletů	tablet	k1gInPc2
umožní	umožnit	k5eAaPmIp3nS
druhý	druhý	k4xOgInSc1
obrazový	obrazový	k2eAgInSc1d1
výstup	výstup	k1gInSc1
a	a	k8xC
PlayStation	PlayStation	k1gInSc1
Vita	vit	k2eAgFnSc1d1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
může	moct	k5eAaImIp3nS
sloužit	sloužit	k5eAaImF
jako	jako	k8xC,k8xS
ovladač	ovladač	k1gInSc1
konzole	konzola	k1gFnSc3
díky	díky	k7c3
vlastnosti	vlastnost	k1gFnSc3
Remote	Remot	k1gInSc5
Play	play	k0
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	K	kA
Playstation	Playstation	k1gInSc4
4	#num#	k4
Pro	pro	k7c4
je	on	k3xPp3gMnPc4
navíc	navíc	k6eAd1
možno	možno	k6eAd1
zakoupit	zakoupit	k5eAaPmF
virtuální	virtuální	k2eAgFnSc4d1
realitu	realita	k1gFnSc4
<g/>
,	,	kIx,
také	také	k9
ve	v	k7c6
zkratce	zkratka	k1gFnSc6
VR	vr	k0
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
vás	vy	k3xPp2nPc4
dokáže	dokázat	k5eAaPmIp3nS
přenést	přenést	k5eAaPmF
do	do	k7c2
pomyslné	pomyslný	k2eAgFnSc2d1
reality	realita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Her	hra	k1gFnPc2
na	na	k7c4
virtuální	virtuální	k2eAgFnSc4d1
realitu	realita	k1gFnSc4
je	být	k5eAaImIp3nS
nespočet	nespočet	k1gInSc1
<g/>
,	,	kIx,
od	od	k7c2
obyčejné	obyčejný	k2eAgFnSc2d1
tzv.	tzv.	kA
skákačky	skákačka	k1gFnSc2
<g/>
,	,	kIx,
přes	přes	k7c4
různé	různý	k2eAgFnPc4d1
360	#num#	k4
<g/>
°	°	k?
pohledy	pohled	k1gInPc4
z	z	k7c2
celého	celý	k2eAgInSc2d1
světa	svět	k1gInSc2
až	až	k9
po	po	k7c4
propracované	propracovaný	k2eAgInPc4d1
virtuální	virtuální	k2eAgInPc4d1
války	válek	k1gInPc4
nebo	nebo	k8xC
závody	závod	k1gInPc4
ve	v	k7c6
sportovních	sportovní	k2eAgInPc6d1
autech	aut	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Podle	podle	k7c2
hlavního	hlavní	k2eAgNnSc2d1
architekta	architekt	k1gMnSc4
konzole	konzola	k1gFnSc6
<g/>
,	,	kIx,
Marka	Marek	k1gMnSc2
Cernyho	Cerny	k1gMnSc2
<g/>
,	,	kIx,
vývoj	vývoj	k1gInSc4
konzole	konzola	k1gFnSc6
osmé	osmý	k4xOgFnSc2
generace	generace	k1gFnSc2
od	od	k7c2
Sony	Sony	kA
začal	začít	k5eAaPmAgMnS
již	již	k6eAd1
počátkem	počátkem	k7c2
roku	rok	k1gInSc2
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c4
méně	málo	k6eAd2
než	než	k8xS
dvěma	dva	k4xCgInPc7
roky	rok	k1gInPc7
<g/>
,	,	kIx,
začala	začít	k5eAaPmAgFnS
Sony	Sony	kA
prodávat	prodávat	k5eAaImF
PlayStation	PlayStation	k1gInSc4
3	#num#	k4
pro	pro	k7c4
tehdejší	tehdejší	k2eAgFnSc4d1
aktuální	aktuální	k2eAgFnSc4d1
sedmou	sedmý	k4xOgFnSc4
generaci	generace	k1gFnSc4
videoherních	videoherní	k2eAgFnPc2d1
konzolí	konzole	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS3	PS3	k1gFnSc1
šla	jít	k5eAaImAgFnS
do	do	k7c2
prodeje	prodej	k1gInSc2
po	po	k7c6
několikaměsíčním	několikaměsíční	k2eAgNnSc6d1
odkládání	odkládání	k1gNnSc6
kvůli	kvůli	k7c3
problémům	problém	k1gInPc3
ve	v	k7c6
výrobě	výroba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odklady	odklad	k1gInPc7
způsobily	způsobit	k5eAaPmAgInP
<g/>
,	,	kIx,
že	že	k8xS
Sony	Sony	kA
přišla	přijít	k5eAaPmAgFnS
se	s	k7c7
svojí	svůj	k3xOyFgFnSc7
PS3	PS3	k1gMnSc1
až	až	k9
rok	rok	k1gInSc4
po	po	k7c6
Xboxu	Xbox	k1gInSc6
360	#num#	k4
ze	z	k7c2
stáje	stáj	k1gFnSc2
společnosti	společnost	k1gFnSc2
Microsoft	Microsoft	kA
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
měla	mít	k5eAaImAgFnS
na	na	k7c6
svém	svůj	k3xOyFgNnSc6
kontě	konto	k1gNnSc6
prodaných	prodaný	k2eAgNnPc2d1
více	hodně	k6eAd2
jak	jak	k8xS,k8xC
10	#num#	k4
milionů	milion	k4xCgInPc2
kusů	kus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jim	on	k3xPp3gMnPc3
Ryan	Ryan	k1gMnSc1
<g/>
,	,	kIx,
CEO	CEO	kA
divize	divize	k1gFnSc1
PlayStationu	PlayStation	k1gInSc2
pro	pro	k7c4
Evropu	Evropa	k1gFnSc4
řekl	říct	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
už	už	k6eAd1
Sony	Sony	kA
chce	chtít	k5eAaImIp3nS
vyvarovat	vyvarovat	k5eAaPmF
opakováním	opakování	k1gNnSc7
s	s	k7c7
problémy	problém	k1gInPc7
při	při	k7c6
PlayStationu	PlayStation	k1gInSc6
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
PlayStation	PlayStation	k1gInSc1
4	#num#	k4
na	na	k7c6
výstavě	výstava	k1gFnSc6
E3	E3	k1gFnSc2
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
<g/>
,	,	kIx,
Sony	Sony	kA
začala	začít	k5eAaPmAgFnS
prodávat	prodávat	k5eAaImF
vývojářské	vývojářský	k2eAgFnPc1d1
verze	verze	k1gFnPc1
své	svůj	k3xOyFgFnSc3
budoucí	budoucí	k2eAgFnSc3d1
konzole	konzola	k1gFnSc3
pro	pro	k7c4
herní	herní	k2eAgMnPc4d1
vývojáře	vývojář	k1gMnPc4
<g/>
,	,	kIx,
v	v	k7c6
podstatě	podstata	k1gFnSc6
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c6
upravené	upravený	k2eAgFnSc6d1
PC	PC	kA
uvnitř	uvnitř	k6eAd1
s	s	k7c7
procesorem	procesor	k1gInSc7
AMD	AMD	kA
(	(	kIx(
<g/>
APU	APU	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
vývojářské	vývojářský	k2eAgFnPc1d1
verze	verze	k1gFnPc1
se	se	k3xPyFc4
nazývaly	nazývat	k5eAaImAgFnP
Orbis	orbis	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
začátku	začátek	k1gInSc6
roku	rok	k1gInSc2
2013	#num#	k4
<g/>
,	,	kIx,
Sony	Sony	kA
oznámila	oznámit	k5eAaPmAgFnS
událost	událost	k1gFnSc4
s	s	k7c7
názvem	název	k1gInSc7
PlayStation	PlayStation	k1gInSc1
Meeting	meeting	k1gInSc1
2013	#num#	k4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
měla	mít	k5eAaImAgFnS
odehrát	odehrát	k5eAaPmF
20	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2013	#num#	k4
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Akce	akce	k1gFnSc1
byla	být	k5eAaImAgFnS
propagována	propagovat	k5eAaImNgFnS
se	s	k7c7
sloganem	slogan	k1gInSc7
„	„	k?
<g/>
future	futur	k1gMnSc5
of	of	k?
PlayStation	PlayStation	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
angl.	angl.	k?
budoucnost	budoucnost	k1gFnSc1
PlayStationu	PlayStation	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
této	tento	k3xDgFnSc6
akci	akce	k1gFnSc6
Sony	Sony	kA
oficiálně	oficiálně	k6eAd1
představila	představit	k5eAaPmAgFnS
PlayStation	PlayStation	k1gInSc4
4	#num#	k4
včetně	včetně	k7c2
informací	informace	k1gFnPc2
o	o	k7c6
použitém	použitý	k2eAgInSc6d1
hardwaru	hardware	k1gInSc6
<g/>
,	,	kIx,
též	též	k9
se	se	k3xPyFc4
zde	zde	k6eAd1
diskutovalo	diskutovat	k5eAaImAgNnS
i	i	k9
o	o	k7c6
nových	nový	k2eAgFnPc6d1
inovacích	inovace	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
PS4	PS4	k1gFnPc4
přinese	přinést	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sony	Sony	kA
také	také	k6eAd1
ukázala	ukázat	k5eAaPmAgFnS
záběry	záběr	k1gInPc4
z	z	k7c2
připravovaných	připravovaný	k2eAgFnPc2d1
her	hra	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
byly	být	k5eAaImAgFnP
ještě	ještě	k6eAd1
ve	v	k7c6
vývoji	vývoj	k1gInSc6
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
technologickou	technologický	k2eAgFnSc4d1
demonstraci	demonstrace	k1gFnSc4
a	a	k8xC
sílu	síla	k1gFnSc4
konzole	konzola	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Představení	představení	k1gNnSc1
samotné	samotný	k2eAgFnSc2d1
konzole	konzola	k1gFnSc3
a	a	k8xC
ještě	ještě	k6eAd1
více	hodně	k6eAd2
informací	informace	k1gFnPc2
o	o	k7c6
ní	on	k3xPp3gFnSc6
Sony	Sony	kA
ukázala	ukázat	k5eAaPmAgFnS
v	v	k7c6
červnu	červen	k1gInSc6
2013	#num#	k4
na	na	k7c6
výstavě	výstava	k1gFnSc6
E	E	kA
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
výstavě	výstava	k1gFnSc6
Gamescom	Gamescom	k1gInSc1
v	v	k7c6
německém	německý	k2eAgInSc6d1
Kolíně	Kolín	k1gInSc6
nad	nad	k7c7
Rýnem	Rýn	k1gInSc7
<g/>
,	,	kIx,
konané	konaný	k2eAgFnSc2d1
20	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2013	#num#	k4
<g/>
,	,	kIx,
společnost	společnost	k1gFnSc1
konečně	konečně	k6eAd1
oznámila	oznámit	k5eAaPmAgFnS
zahájení	zahájení	k1gNnSc3
prodeje	prodej	k1gInSc2
a	a	k8xC
to	ten	k3xDgNnSc4
15	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2013	#num#	k4
v	v	k7c6
USA	USA	kA
a	a	k8xC
Kanadě	Kanada	k1gFnSc6
a	a	k8xC
29	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2013	#num#	k4
v	v	k7c6
některých	některý	k3yIgFnPc6
zemích	zem	k1gFnPc6
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
Austrálii	Austrálie	k1gFnSc6
a	a	k8xC
ostatních	ostatní	k2eAgInPc6d1
státech	stát	k1gInPc6
amerického	americký	k2eAgInSc2d1
kontinentu	kontinent	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Česku	Česko	k1gNnSc6
se	se	k3xPyFc4
prodej	prodej	k1gInSc1
zahájí	zahájit	k5eAaPmIp3nS
13	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2013	#num#	k4
a	a	k8xC
např.	např.	kA
v	v	k7c6
Japonsku	Japonsko	k1gNnSc6
<g/>
,	,	kIx,
domovské	domovský	k2eAgFnSc3d1
zemi	zem	k1gFnSc3
konzole	konzola	k1gFnSc3
<g/>
,	,	kIx,
až	až	k9
22	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Hardware	hardware	k1gInSc1
</s>
<s>
Konzole	konzola	k1gFnSc3
obsahuje	obsahovat	k5eAaImIp3nS
hardware	hardware	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
podobný	podobný	k2eAgInSc1d1
s	s	k7c7
hardwarem	hardware	k1gInSc7
použitým	použitý	k2eAgInSc7d1
v	v	k7c6
osobních	osobní	k2eAgInPc6d1
počítačích	počítač	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obdobné	obdobný	k2eAgFnPc1d1
součástky	součástka	k1gFnPc1
zajistily	zajistit	k5eAaPmAgFnP
lehčí	lehký	k2eAgFnSc4d2
a	a	k8xC
cenově	cenově	k6eAd1
dostupnější	dostupný	k2eAgFnPc1d2
vývoj	vývoj	k1gInSc4
her	hra	k1gFnPc2
pro	pro	k7c4
herní	herní	k2eAgNnPc4d1
studia	studio	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konzole	konzola	k1gFnSc6
jako	jako	k8xC,k8xS
taková	takový	k3xDgFnSc1
byla	být	k5eAaImAgFnS
představena	představit	k5eAaPmNgFnS
na	na	k7c6
výstavišti	výstaviště	k1gNnSc6
E3	E3	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
společností	společnost	k1gFnPc2
Sony	Sony	kA
<g/>
.	.	kIx.
</s>
<s>
Hardware	hardware	k1gInSc1
PlayStation	PlayStation	k1gInSc1
4	#num#	k4
představuje	představovat	k5eAaImIp3nS
zejména	zejména	k9
APU	APU	kA
mikroprocesor	mikroprocesor	k1gInSc1
od	od	k7c2
společnosti	společnost	k1gFnSc2
AMD	AMD	kA
vyvíjený	vyvíjený	k2eAgInSc1d1
ve	v	k7c4
spolupráci	spolupráce	k1gFnSc4
se	s	k7c7
Sony	Sony	kA
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc4
čip	čip	k1gInSc4
v	v	k7c6
sobě	sebe	k3xPyFc6
spojuje	spojovat	k5eAaImIp3nS
relativně	relativně	k6eAd1
veliký	veliký	k2eAgInSc1d1
počet	počet	k1gInSc1
různých	různý	k2eAgFnPc2d1
součástí	součást	k1gFnPc2
<g/>
,	,	kIx,
kterým	který	k3yQgMnPc3,k3yIgMnPc3,k3yRgMnPc3
dominují	dominovat	k5eAaImIp3nP
tři	tři	k4xCgInPc1
druhy	druh	k1gInPc1
procesorů	procesor	k1gInPc2
<g/>
,	,	kIx,
dále	daleko	k6eAd2
paměťový	paměťový	k2eAgInSc4d1
řadič	řadič	k1gInSc4
a	a	k8xC
video	video	k1gNnSc4
dekodér	dekodér	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Procesory	procesor	k1gInPc1
(	(	kIx(
<g/>
CPU	CPU	kA
<g/>
)	)	kIx)
představují	představovat	k5eAaImIp3nP
základ	základ	k1gInSc4
konzole	konzola	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
první	první	k4xOgFnSc6
řadě	řada	k1gFnSc6
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
8	#num#	k4
klasických	klasický	k2eAgInPc2d1
64	#num#	k4
<g/>
bitových	bitový	k2eAgInPc2d1
procesorů	procesor	k1gInPc2
(	(	kIx(
<g/>
CPU	CPU	kA
jader	jádro	k1gNnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
řídí	řídit	k5eAaImIp3nP
celou	celý	k2eAgFnSc4d1
konzoli	konzole	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedno	jeden	k4xCgNnSc1
jádro	jádro	k1gNnSc1
má	mít	k5eAaImIp3nS
výkon	výkon	k1gInSc4
12,8	12,8	k4
GFLOPs	GFLOPsa	k1gFnPc2
<g/>
,	,	kIx,
všech	všecek	k3xTgInPc2
8	#num#	k4
společně	společně	k6eAd1
má	mít	k5eAaImIp3nS
výkon	výkon	k1gInSc1
přibližně	přibližně	k6eAd1
102,4	102,4	k4
GFLOPs	GFLOPsa	k1gFnPc2
(	(	kIx(
<g/>
ev.	ev.	k?
134,4	134,4	k4
GFLOPs	GFLOPsa	k1gFnPc2
pro	pro	k7c4
verzi	verze	k1gFnSc4
PlayStation	PlayStation	k1gInSc1
4	#num#	k4
Pro	pro	k7c4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Jejich	jejich	k3xOp3gFnSc1
síla	síla	k1gFnSc1
spočívá	spočívat	k5eAaImIp3nS
zejména	zejména	k9
v	v	k7c6
zavedené	zavedený	k2eAgFnSc6d1
kompatibilitě	kompatibilita	k1gFnSc6
a	a	k8xC
úzkém	úzký	k2eAgNnSc6d1
spojení	spojení	k1gNnSc6
s	s	k7c7
výkonnými	výkonný	k2eAgMnPc7d1
GPU	GPU	kA
jádry	jádro	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s>
Grafická	grafický	k2eAgFnSc1d1
část	část	k1gFnSc1
(	(	kIx(
<g/>
GPU	GPU	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
taktovaná	taktovaný	k2eAgFnSc1d1
na	na	k7c4
rychlost	rychlost	k1gFnSc4
800	#num#	k4
MHz	Mhz	kA
(	(	kIx(
<g/>
911	#num#	k4
MHz	Mhz	kA
u	u	k7c2
PS4	PS4	k1gFnSc2
Pro	pro	k7c4
<g/>
)	)	kIx)
a	a	k8xC
s	s	k7c7
jednotkami	jednotka	k1gFnPc7
CPU	cpát	k5eAaImIp1nS
úzce	úzko	k6eAd1
spolupracuje	spolupracovat	k5eAaImIp3nS
18	#num#	k4
nebo	nebo	k8xC
36	#num#	k4
GPU	GPU	kA
procesorů	procesor	k1gInPc2
(	(	kIx(
<g/>
8	#num#	k4
asynchronních	asynchronní	k2eAgNnPc2d1
jader	jádro	k1gNnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
kromě	kromě	k7c2
grafických	grafický	k2eAgInPc2d1
výpočtů	výpočet	k1gInPc2
počítají	počítat	k5eAaImIp3nP
i	i	k9
herní	herní	k2eAgFnSc4d1
fyziku	fyzika	k1gFnSc4
a	a	k8xC
efekty	efekt	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Grafická	grafický	k2eAgFnSc1d1
část	část	k1gFnSc1
má	mít	k5eAaImIp3nS
výkon	výkon	k1gInSc4
až	až	k9
1,84	1,84	k4
TFLOPs	TFLOPsa	k1gFnPc2
(	(	kIx(
<g/>
4,19	4,19	k4
TFLOPs	TFLOPsa	k1gFnPc2
PS4	PS4	k1gFnSc4
Pro	pro	k7c4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
GPU	GPU	kA
nemá	mít	k5eNaImIp3nS
žádnou	žádný	k3yNgFnSc4
paměť	paměť	k1gFnSc4
VRAM	VRAM	kA
<g/>
,	,	kIx,
protože	protože	k8xS
pro	pro	k7c4
grafické	grafický	k2eAgFnPc4d1
operace	operace	k1gFnPc4
používá	používat	k5eAaImIp3nS
systémovou	systémový	k2eAgFnSc4d1
paměť	paměť	k1gFnSc4
RAM	RAM	kA
<g/>
.	.	kIx.
</s>
<s>
Konzole	konzola	k1gFnSc3
má	mít	k5eAaImIp3nS
i	i	k9
audio	audio	k2eAgInSc1d1
procesor	procesor	k1gInSc1
(	(	kIx(
<g/>
DSP	DSP	kA
jádro	jádro	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
zpracovává	zpracovávat	k5eAaImIp3nS
zvuk	zvuk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
má	mít	k5eAaImIp3nS
konzole	konzola	k1gFnSc3
na	na	k7c4
mainboardu	mainboarda	k1gFnSc4
jeden	jeden	k4xCgInSc4
mikroprocesor	mikroprocesor	k1gInSc4
ARM	ARM	kA
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
řídí	řídit	k5eAaImIp3nS
konzoli	konzoli	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
je	být	k5eAaImIp3nS
vypnuté	vypnutý	k2eAgNnSc4d1
APU	APU	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
se	se	k3xPyFc4
stará	starat	k5eAaImIp3nS
o	o	k7c4
úlohy	úloha	k1gFnPc4
spojené	spojený	k2eAgFnPc4d1
se	s	k7c7
stahováním	stahování	k1gNnSc7
<g/>
,	,	kIx,
nahráváním	nahrávání	k1gNnSc7
a	a	k8xC
sociálním	sociální	k2eAgNnSc7d1
hraním	hraní	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
úlohy	úloha	k1gFnPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
nezávisle	závisle	k6eNd1
ukončeny	ukončit	k5eAaPmNgFnP
na	na	k7c4
pozadí	pozadí	k1gNnSc4
během	během	k7c2
hraní	hraní	k1gNnSc2
a	a	k8xC
i	i	k9
tehdy	tehdy	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
je	být	k5eAaImIp3nS
konzole	konzola	k1gFnSc3
v	v	k7c6
režimu	režim	k1gInSc6
spánku	spánek	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Operační	operační	k2eAgFnSc1d1
paměť	paměť	k1gFnSc1
typu	typ	k1gInSc2
GDDR5	GDDR5	k1gFnSc2
je	být	k5eAaImIp3nS
schopna	schopen	k2eAgFnSc1d1
běžet	běžet	k5eAaImF
na	na	k7c4
frekvenci	frekvence	k1gFnSc4
až	až	k9
2,75	2,75	k4
<g/>
GHz	GHz	k1gFnPc2
(	(	kIx(
<g/>
5,5	5,5	k4
<g/>
GT	GT	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
)	)	kIx)
a	a	k8xC
maximální	maximální	k2eAgFnSc1d1
paměťová	paměťový	k2eAgFnSc1d1
propustnost	propustnost	k1gFnSc1
je	být	k5eAaImIp3nS
až	až	k9
176	#num#	k4
<g/>
GB	GB	kA
<g/>
/	/	kIx~
<g/>
s.	s.	k?
Konzole	konzola	k1gFnSc6
obsahuje	obsahovat	k5eAaImIp3nS
8GB	8GB	k4
operační	operační	k2eAgFnSc2d1
paměti	paměť	k1gFnSc2
typu	typ	k1gInSc2
GDDR	GDDR	kA
<g/>
5	#num#	k4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
16	#num#	k4
<g/>
×	×	k?
větší	veliký	k2eAgNnPc4d2
množství	množství	k1gNnPc4
než	než	k8xS
v	v	k7c6
předchůdci	předchůdce	k1gMnSc6
a	a	k8xC
dává	dávat	k5eAaImIp3nS
tak	tak	k6eAd1
výraznou	výrazný	k2eAgFnSc4d1
rezervu	rezerva	k1gFnSc4
do	do	k7c2
budoucna	budoucno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Zabudovaná	zabudovaný	k2eAgFnSc1d1
optická	optický	k2eAgFnSc1d1
mechanika	mechanika	k1gFnSc1
dokáže	dokázat	k5eAaPmIp3nS
jen	jen	k9
číst	číst	k5eAaImF
Blu-ray	Blu-raa	k1gFnPc4
či	či	k8xC
DVD	DVD	kA
média	médium	k1gNnPc4
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
zapisovat	zapisovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rychlost	rychlost	k1gFnSc1
čtení	čtení	k1gNnSc2
Blu-ray	Blu-raa	k1gFnSc2
disků	disk	k1gInPc2
je	být	k5eAaImIp3nS
oproti	oproti	k7c3
PS3	PS3	k1gFnSc3
více	hodně	k6eAd2
jak	jak	k6eAd1
trojnásobná	trojnásobný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oproti	oproti	k7c3
předchůdci	předchůdce	k1gMnSc6
<g/>
,	,	kIx,
optická	optický	k2eAgFnSc1d1
mechanika	mechanika	k1gFnSc1
dokáže	dokázat	k5eAaPmIp3nS
přečíst	přečíst	k5eAaPmF
až	až	k9
16	#num#	k4
<g/>
vrstvý	vrstvý	k2eAgInSc4d1
Blu-ray	Blu-raa	k1gFnSc2
disk	disk	k1gInSc4
s	s	k7c7
celkovou	celkový	k2eAgFnSc7d1
kapacitou	kapacita	k1gFnSc7
400	#num#	k4
<g/>
GB	GB	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přehrávání	přehrávání	k1gNnPc4
CD	CD	kA
disků	disk	k1gInPc2
již	již	k6eAd1
není	být	k5eNaImIp3nS
podporováno	podporován	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
konzole	konzola	k1gFnSc3
podporuje	podporovat	k5eAaImIp3nS
fotografie	fotografia	k1gFnSc2
a	a	k8xC
videa	video	k1gNnSc2
v	v	k7c6
rozlišení	rozlišení	k1gNnSc6
4	#num#	k4
<g/>
K	K	kA
<g/>
,	,	kIx,
PS4	PS4	k1gFnSc1
není	být	k5eNaImIp3nS
schopna	schopen	k2eAgFnSc1d1
renderovat	renderovat	k5eAaImF,k5eAaPmF,k5eAaBmF
hry	hra	k1gFnPc4
v	v	k7c6
rozlišení	rozlišení	k1gNnSc6
větším	veliký	k2eAgNnSc6d2
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
1080	#num#	k4
<g/>
p	p	k?
(	(	kIx(
<g/>
Full	Full	k1gMnSc1
HD	HD	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konzole	konzola	k1gFnSc3
obsahuje	obsahovat	k5eAaImIp3nS
pevný	pevný	k2eAgInSc4d1
disk	disk	k1gInSc4
s	s	k7c7
velikostí	velikost	k1gFnSc7
500	#num#	k4
<g/>
GB	GB	kA
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
umožňuje	umožňovat	k5eAaImIp3nS
disk	disk	k1gInSc1
vyměnit	vyměnit	k5eAaPmF
za	za	k7c7
vlastní	vlastní	k2eAgFnSc7d1
pomocí	pomoc	k1gFnSc7
návodů	návod	k1gInPc2
dostupných	dostupný	k2eAgInPc2d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Konektivita	konektivita	k1gFnSc1
konzole	konzola	k1gFnSc3
je	být	k5eAaImIp3nS
zajištěna	zajistit	k5eAaPmNgFnS
pomocí	pomocí	k7c2
Ethernetu	Ethernet	k1gInSc2
<g/>
,	,	kIx,
Wi-Fi	Wi-Fi	k1gNnSc2
<g/>
,	,	kIx,
Bluetooth	Bluetootha	k1gFnPc2
modulu	modul	k1gInSc2
a	a	k8xC
dvou	dva	k4xCgFnPc2
USB	USB	kA
portů	port	k1gInPc2
ve	v	k7c4
verzi	verze	k1gFnSc4
3.0	3.0	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomocný	pomocný	k2eAgInSc1d1
Aux	Aux	k1gFnSc7
port	porta	k1gFnPc2
je	být	k5eAaImIp3nS
v	v	k7c6
konektorové	konektorový	k2eAgFnSc6d1
výbavě	výbava	k1gFnSc6
též	též	k6eAd1
zahrnut	zahrnout	k5eAaPmNgMnS
kvůli	kvůli	k7c3
propojení	propojení	k1gNnSc3
PlayStation	PlayStation	k1gInSc1
Camera	Camer	k1gMnSc4
<g/>
,	,	kIx,
digitální	digitální	k2eAgFnSc6d1
kameře	kamera	k1gFnSc6
snímající	snímající	k2eAgInSc4d1
pohyb	pohyb	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mono	mono	k2eAgNnSc2d1
sluchátka	sluchátko	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
můžou	můžou	k?
být	být	k5eAaImF
připojena	připojit	k5eAaPmNgFnS
přímo	přímo	k6eAd1
do	do	k7c2
gamepadu	gamepad	k1gInSc2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
již	již	k9
součástí	součást	k1gFnSc7
balení	balení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
se	se	k3xPyFc4
týče	týkat	k5eAaImIp3nS
digitálních	digitální	k2eAgInPc2d1
A	A	kA
<g/>
/	/	kIx~
<g/>
V	V	kA
výstupů	výstup	k1gInPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
tu	tu	k6eAd1
kromě	kromě	k7c2
HDMI	HDMI	kA
i	i	k8xC
optický	optický	k2eAgInSc4d1
SPDIF	SPDIF	kA
konektor	konektor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konzole	konzola	k1gFnSc3
již	již	k9
neobsahuje	obsahovat	k5eNaImIp3nS
žádný	žádný	k3yNgInSc4
analogový	analogový	k2eAgInSc4d1
A	a	k8xC
<g/>
/	/	kIx~
<g/>
V	v	k7c4
výstup	výstup	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konzole	konzola	k1gFnSc3
obsahuje	obsahovat	k5eAaImIp3nS
stav	stav	k1gInSc1
s	s	k7c7
nízkým	nízký	k2eAgInSc7d1
odběrem	odběr	k1gInSc7
proudu	proud	k1gInSc2
<g/>
,	,	kIx,
nazvaným	nazvaný	k2eAgInSc7d1
„	„	k?
<g/>
Suspend	Suspenda	k1gFnPc2
Mode	modus	k1gInSc5
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
stav	stav	k1gInSc4
uvede	uvést	k5eAaPmIp3nS
konzoli	konzoli	k6eAd1
do	do	k7c2
režimu	režim	k1gInSc2
s	s	k7c7
nízkým	nízký	k2eAgInSc7d1
odběrem	odběr	k1gInSc7
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
po	po	k7c6
probuzení	probuzení	k1gNnSc6
konzole	konzola	k1gFnSc3
má	mít	k5eAaImIp3nS
hráč	hráč	k1gMnSc1
okamžitý	okamžitý	k2eAgInSc4d1
přístup	přístup	k1gInSc4
k	k	k7c3
pozastavené	pozastavený	k2eAgFnSc3d1
hře	hra	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
PlayStation	PlayStation	k1gInSc1
4	#num#	k4
v	v	k7c6
tomto	tento	k3xDgInSc6
stavu	stav	k1gInSc6
umožňuje	umožňovat	k5eAaImIp3nS
stahovat	stahovat	k5eAaImF
různý	různý	k2eAgInSc4d1
obsah	obsah	k1gInSc4
a	a	k8xC
též	též	k9
stahovat	stahovat	k5eAaImF
i	i	k9
systémové	systémový	k2eAgFnPc4d1
aktualizace	aktualizace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Příslušenství	příslušenství	k1gNnSc1
</s>
<s>
Ovladače	ovladač	k1gInPc1
</s>
<s>
DualShock	DualShock	k6eAd1
4	#num#	k4
</s>
<s>
Sony	Sony	kA
DualShock	DualShock	k1gInSc4
4	#num#	k4
</s>
<s>
PlayStation	PlayStation	k1gInSc1
4	#num#	k4
se	se	k3xPyFc4
ovládá	ovládat	k5eAaImIp3nS
pomocí	pomocí	k7c2
ovladače	ovladač	k1gInSc2
DualShock	DualShocka	k1gFnPc2
4	#num#	k4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
podobný	podobný	k2eAgInSc1d1
svému	svůj	k3xOyFgMnSc3
předchůdci	předchůdce	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
ale	ale	k8xC
s	s	k7c7
PS3	PS3	k1gFnPc7
kompatibilní	kompatibilní	k2eAgMnSc1d1
není	být	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
DualShock	DualShock	k1gInSc1
<g/>
4	#num#	k4
se	se	k3xPyFc4
s	s	k7c7
konzolí	konzole	k1gFnSc7
propojuje	propojovat	k5eAaImIp3nS
přes	přes	k7c4
Bluetooth	Bluetooth	k1gInSc4
2.1	2.1	k4
+	+	kIx~
<g/>
EDR	EDR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovladač	ovladač	k1gInSc1
přinesl	přinést	k5eAaPmAgInS
jisté	jistý	k2eAgFnPc4d1
inovace	inovace	k1gFnPc4
a	a	k8xC
to	ten	k3xDgNnSc4
např.	např.	kA
kapacitní	kapacitní	k2eAgInSc4d1
touchpad	touchpad	k1gInSc4
v	v	k7c6
prostřední	prostřední	k2eAgFnSc6d1
části	část	k1gFnSc6
ovladače	ovladač	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Též	též	k6eAd1
podporuje	podporovat	k5eAaImIp3nS
pohybové	pohybový	k2eAgNnSc4d1
snímání	snímání	k1gNnSc4
díky	díky	k7c3
tříosovému	tříosový	k2eAgInSc3d1
setrvačníku	setrvačník	k1gInSc2
&	&	k?
acelerometru	acelerometr	k1gInSc2
a	a	k8xC
také	také	k9
vylepšené	vylepšený	k2eAgFnSc3d1
vibraci	vibrace	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
DualShock	DualShock	k1gInSc1
4	#num#	k4
je	být	k5eAaImIp3nS
první	první	k4xOgInSc1
ovladač	ovladač	k1gInSc1
od	od	k7c2
společnosti	společnost	k1gFnSc2
Sony	Sony	kA
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
oficiálně	oficiálně	k6eAd1
podporuje	podporovat	k5eAaImIp3nS
kromě	kromě	k7c2
PlayStationu	PlayStation	k1gInSc2
i	i	k8xC
PC	PC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvnitř	uvnitř	k7c2
ovladače	ovladač	k1gInSc2
je	být	k5eAaImIp3nS
zabudovaná	zabudovaný	k2eAgFnSc1d1
baterie	baterie	k1gFnSc1
typu	typ	k1gInSc2
Li-Ion	Li-Ion	k1gInSc4
s	s	k7c7
celkovou	celkový	k2eAgFnSc7d1
kapacitou	kapacita	k1gFnSc7
1000	#num#	k4
<g/>
mAh	mAh	k?
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
však	však	k9
nedá	dát	k5eNaPmIp3nS
žádným	žádný	k3yNgInSc7
způsobem	způsob	k1gInSc7
vyměnit	vyměnit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
DualShock	DualShock	k1gInSc1
4	#num#	k4
váží	vážit	k5eAaImIp3nS
210	#num#	k4
<g/>
g	g	kA
a	a	k8xC
jeho	jeho	k3xOp3gInPc1
rozměry	rozměr	k1gInPc1
jsou	být	k5eAaImIp3nP
162	#num#	k4
<g/>
x	x	k?
<g/>
92	#num#	k4
<g/>
x	x	k?
<g/>
98	#num#	k4
<g/>
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s>
Gamepad	Gamepad	k6eAd1
obsahuje	obsahovat	k5eAaImIp3nS
i	i	k9
konektory	konektor	k1gInPc1
jako	jako	k8xC,k8xS
stereo	stereo	k2eAgInPc1d1
3.5	3.5	k4
<g/>
mm	mm	kA
jack	jacka	k1gFnPc2
a	a	k8xC
microUSB	microUSB	k?
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
i	i	k9
mono	mono	k2eAgInSc1d1
reproduktor	reproduktor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přes	přes	k7c4
zmíněný	zmíněný	k2eAgInSc4d1
microUSB	microUSB	k?
port	port	k1gInSc1
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
pohodlně	pohodlně	k6eAd1
nabíjet	nabíjet	k5eAaImF
a	a	k8xC
po	po	k7c6
nabití	nabití	k1gNnSc6
baterie	baterie	k1gFnSc1
opět	opět	k6eAd1
může	moct	k5eAaImIp3nS
pracovat	pracovat	k5eAaImF
bez	bez	k7c2
kabelu	kabel	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovladač	ovladač	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
následující	následující	k2eAgNnPc4d1
tlačítka	tlačítko	k1gNnPc4
<g/>
:	:	kIx,
Hlavní	hlavní	k2eAgNnPc1d1
tlačítka	tlačítko	k1gNnPc1
označována	označován	k2eAgNnPc1d1
jako	jako	k8xS,k8xC
akční	akční	k2eAgFnSc1d1
<g/>
(	(	kIx(
<g/>
,	,	kIx,
,	,	kIx,
,	,	kIx,
)	)	kIx)
<g/>
,	,	kIx,
krajní	krajní	k2eAgNnPc1d1
tlačítka	tlačítko	k1gNnPc1
(	(	kIx(
<g/>
R	R	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
L	L	kA
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
spouště	spoušť	k1gFnPc4
(	(	kIx(
<g/>
R	R	kA
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
L	L	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
analogové	analogový	k2eAgFnSc2d1
páčky	páčka	k1gFnSc2
(	(	kIx(
<g/>
L	L	kA
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
R	R	kA
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
PlayStation	PlayStation	k1gInSc4
tlačítko	tlačítko	k1gNnSc4
<g/>
,	,	kIx,
tlačítko	tlačítko	k1gNnSc4
pro	pro	k7c4
sdílení	sdílení	k1gNnSc4
<g/>
,	,	kIx,
tlačítko	tlačítko	k1gNnSc4
pro	pro	k7c4
nastavení	nastavení	k1gNnSc4
<g/>
,	,	kIx,
čtyřsměrové	čtyřsměrový	k2eAgNnSc4d1
tlačítka	tlačítko	k1gNnPc1
a	a	k8xC
nakonec	nakonec	k6eAd1
tlačítko	tlačítko	k1gNnSc4
pro	pro	k7c4
obsluhu	obsluha	k1gFnSc4
touchpadu	touchpad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velmi	velmi	k6eAd1
známá	známý	k2eAgNnPc4d1
tlačítka	tlačítko	k1gNnPc4
Start	start	k1gInSc1
a	a	k8xC
Select	Select	k1gInSc1
se	se	k3xPyFc4
integrovaly	integrovat	k5eAaBmAgFnP
do	do	k7c2
již	již	k6eAd1
zmíněného	zmíněný	k2eAgInSc2d1
Options	Optionsa	k1gFnPc2
tlačítka	tlačítko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednoúčelové	jednoúčelový	k2eAgFnPc4d1
Share	Shar	k1gInSc5
tlačítko	tlačítko	k1gNnSc4
dovoluje	dovolovat	k5eAaImIp3nS
hráčům	hráč	k1gMnPc3
nahrát	nahrát	k5eAaPmF,k5eAaBmF
jejich	jejich	k3xOp3gInPc1
záběry	záběr	k1gInPc1
z	z	k7c2
her	hra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
DualShock	DualShock	k1gInSc1
4	#num#	k4
také	také	k9
obsahuje	obsahovat	k5eAaImIp3nS
světlý	světlý	k2eAgInSc1d1
sloupek	sloupek	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
může	moct	k5eAaImIp3nS
zobrazovat	zobrazovat	k5eAaImF
různé	různý	k2eAgFnPc4d1
barvy	barva	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barvy	barva	k1gFnSc2
můžou	můžou	k?
upozornit	upozornit	k5eAaPmF
hráče	hráč	k1gMnSc4
na	na	k7c4
různé	různý	k2eAgNnSc4d1
varování	varování	k1gNnSc4
<g/>
,	,	kIx,
např.	např.	kA
na	na	k7c4
skoro	skoro	k6eAd1
vybitou	vybitý	k2eAgFnSc4d1
baterii	baterie	k1gFnSc4
v	v	k7c6
ovladači	ovladač	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
slouží	sloužit	k5eAaImIp3nS
pro	pro	k7c4
kameru	kamera	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
lépe	dobře	k6eAd2
rozeznala	rozeznat	k5eAaPmAgFnS
pohyb	pohyb	k1gInSc4
a	a	k8xC
hloubku	hloubka	k1gFnSc4
ovladače	ovladač	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
je	být	k5eAaImIp3nS
postaveno	postavit	k5eAaPmNgNnS
na	na	k7c4
již	již	k6eAd1
existující	existující	k2eAgFnSc4d1
technologii	technologie	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
uplatnila	uplatnit	k5eAaPmAgFnS
u	u	k7c2
pohybového	pohybový	k2eAgInSc2d1
ovladače	ovladač	k1gInSc2
<g/>
,	,	kIx,
PlayStation	PlayStation	k1gInSc1
Move	Mov	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existující	existující	k2eAgInSc4d1
PS	PS	kA
Move	Move	k1gInSc4
ovladače	ovladač	k1gInSc2
jsou	být	k5eAaImIp3nP
s	s	k7c7
PS4	PS4	k1gMnSc7
kompatibilní	kompatibilní	k2eAgInPc4d1
<g/>
,	,	kIx,
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
DualShocku	DualShock	k1gInSc2
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2016	#num#	k4
vyšla	vyjít	k5eAaPmAgFnS
druhá	druhý	k4xOgFnSc1
generace	generace	k1gFnSc1
ovladače	ovladač	k1gInSc2
DualShock	DualShocko	k1gNnPc2
4	#num#	k4
V	V	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
vylepšení	vylepšení	k1gNnSc4
patří	patřit	k5eAaImIp3nS
například	například	k6eAd1
komunikace	komunikace	k1gFnSc1
přes	přes	k7c4
rozhraní	rozhraní	k1gNnSc4
USB	USB	kA
(	(	kIx(
<g/>
původní	původní	k2eAgInSc4d1
jen	jen	k9
přes	přes	k7c4
Bluetooth	Bluetooth	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
delší	dlouhý	k2eAgFnSc1d2
životnost	životnost	k1gFnSc1
baterie	baterie	k1gFnSc1
a	a	k8xC
možnost	možnost	k1gFnSc1
vidět	vidět	k5eAaImF
světelnou	světelný	k2eAgFnSc4d1
lištu	lišta	k1gFnSc4
z	z	k7c2
horní	horní	k2eAgFnSc2d1
části	část	k1gFnSc2
touchpadu	touchpad	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
PlayStation	PlayStation	k1gInSc1
Move	Mov	k1gFnSc2
</s>
<s>
PlayStation	PlayStation	k1gInSc1
Move	Mov	k1gFnSc2
</s>
<s>
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
pohybový	pohybový	k2eAgInSc1d1
ovladač	ovladač	k1gInSc1
s	s	k7c7
integrovaným	integrovaný	k2eAgInSc7d1
pohybovým	pohybový	k2eAgInSc7d1
snímačem	snímač	k1gInSc7
původně	původně	k6eAd1
vytvořený	vytvořený	k2eAgInSc4d1
pro	pro	k7c4
PlayStation	PlayStation	k1gInSc4
3	#num#	k4
<g/>
,	,	kIx,
avšak	avšak	k8xC
podporuje	podporovat	k5eAaImIp3nS
též	též	k9
i	i	k9
PlayStation	PlayStation	k1gInSc4
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS	PS	kA
Move	Move	k1gFnPc2
snímá	snímat	k5eAaImIp3nS
pohyb	pohyb	k1gInSc1
a	a	k8xC
pozice	pozice	k1gFnSc1
je	být	k5eAaImIp3nS
rozpoznávána	rozpoznávat	k5eAaImNgFnS
díky	díky	k7c3
PlayStation	PlayStation	k1gInSc1
kameře	kamera	k1gFnSc3
(	(	kIx(
<g/>
u	u	k7c2
PS3	PS3	k1gFnSc2
PlayStation	PlayStation	k1gInSc1
Eye	Eye	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS	PS	kA
Move	Mov	k1gFnSc2
je	být	k5eAaImIp3nS
konkurencí	konkurence	k1gFnSc7
Wii	Wii	k1gFnSc2
Remote	Remot	k1gInSc5
od	od	k7c2
společnosti	společnost	k1gFnSc2
Nintendo	Nintendo	k6eAd1
a	a	k8xC
Kinect	Kinect	k2eAgInSc1d1
snímači	snímač	k1gInPc7
od	od	k7c2
společnosti	společnost	k1gFnSc2
Microsoft	Microsoft	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
PlayStation	PlayStation	k1gInSc1
Move	Mov	k1gFnSc2
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
dvou	dva	k4xCgInPc2
ovladačů	ovladač	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc4
ovladač	ovladač	k1gInSc4
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
jednoduše	jednoduše	k6eAd1
„	„	k?
<g/>
pohybový	pohybový	k2eAgInSc4d1
ovladač	ovladač	k1gInSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
uživateli	uživatel	k1gMnSc3
umožňuje	umožňovat	k5eAaImIp3nS
snímat	snímat	k5eAaImF
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
pohyb	pohyb	k1gInSc4
<g/>
,	,	kIx,
pokud	pokud	k8xS
uživatel	uživatel	k1gMnSc1
stojí	stát	k5eAaImIp3nS
čelem	čelo	k1gNnSc7
před	před	k7c4
PlayStation	PlayStation	k1gInSc4
kamerou	kamera	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhý	druhý	k4xOgInSc4
ovladač	ovladač	k1gInSc4
je	být	k5eAaImIp3nS
„	„	k?
<g/>
navigační	navigační	k2eAgInSc4d1
ovladač	ovladač	k1gInSc4
<g/>
“	“	k?
a	a	k8xC
je	být	k5eAaImIp3nS
navržen	navrhnout	k5eAaPmNgInS
pro	pro	k7c4
spolupráci	spolupráce	k1gFnSc4
s	s	k7c7
pohybovým	pohybový	k2eAgInSc7d1
ovladačem	ovladač	k1gInSc7
<g/>
,	,	kIx,
navigační	navigační	k2eAgInPc4d1
ovladač	ovladač	k1gInSc4
slouží	sloužit	k5eAaImIp3nP
pro	pro	k7c4
různé	různý	k2eAgInPc4d1
typy	typ	k1gInPc4
her	hra	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
uplatní	uplatnit	k5eAaPmIp3nS
a	a	k8xC
díky	díky	k7c3
tlačítkům	tlačítko	k1gNnPc3
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
obsahuje	obsahovat	k5eAaImIp3nS
<g/>
,	,	kIx,
dokáže	dokázat	k5eAaPmIp3nS
zastoupit	zastoupit	k5eAaPmF
funkcí	funkce	k1gFnSc7
i	i	k9
gamepad	gamepad	k1gInSc1
DualShock	DualShocka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sony	Sony	kA
pro	pro	k7c4
PS	PS	kA
Move	Move	k1gNnSc1
prodává	prodávat	k5eAaImIp3nS
i	i	k9
nástavce	nástavec	k1gInSc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
z	z	k7c2
něj	on	k3xPp3gInSc2
mohou	moct	k5eAaImIp3nP
udělat	udělat	k5eAaPmF
samopal	samopal	k1gInSc4
či	či	k8xC
pistoli	pistole	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgInSc2
tu	tu	k6eAd1
je	být	k5eAaImIp3nS
i	i	k9
nástavec	nástavec	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
ho	on	k3xPp3gMnSc4
promění	proměnit	k5eAaPmIp3nS
ve	v	k7c4
volant	volant	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
dostání	dostání	k1gNnSc3
je	být	k5eAaImIp3nS
i	i	k9
nabíjecí	nabíjecí	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoliv	ačkoliv	k8xS
byl	být	k5eAaImAgInS
PS	PS	kA
Move	Mov	k1gFnSc2
uveden	uvést	k5eAaPmNgInS
již	již	k9
pro	pro	k7c4
existující	existující	k2eAgInSc4d1
PlayStation	PlayStation	k1gInSc4
3	#num#	k4
<g/>
,	,	kIx,
kampaň	kampaň	k1gFnSc1
pro	pro	k7c4
toto	tento	k3xDgNnSc4
zařízení	zařízení	k1gNnSc4
byla	být	k5eAaImAgFnS
velice	velice	k6eAd1
masivní	masivní	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS	PS	kA
Move	Move	k1gInSc1
vyšel	vyjít	k5eAaPmAgInS
15	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc4
2010	#num#	k4
v	v	k7c6
Evropě	Evropa	k1gFnSc6
a	a	k8xC
Asii	Asie	k1gFnSc6
<g/>
,	,	kIx,
16	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc4
2010	#num#	k4
v	v	k7c6
Austrálii	Austrálie	k1gFnSc6
<g/>
,	,	kIx,
17	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc4
2010	#num#	k4
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
Británii	Británie	k1gFnSc6
a	a	k8xC
USA	USA	kA
a	a	k8xC
23	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
v	v	k7c6
Japonsku	Japonsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
Sony	Sony	kA
potvrdila	potvrdit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
PlayStation	PlayStation	k1gInSc1
Move	Mov	k1gFnSc2
bude	být	k5eAaImBp3nS
plně	plně	k6eAd1
kompatibilní	kompatibilní	k2eAgMnSc1d1
s	s	k7c7
PlayStationem	PlayStation	k1gInSc7
4	#num#	k4
a	a	k8xC
bude	být	k5eAaImBp3nS
ještě	ještě	k9
přesnější	přesný	k2eAgMnSc1d2
ve	v	k7c6
snímání	snímání	k1gNnSc6
díky	díky	k7c3
pokrokovější	pokrokový	k2eAgFnSc3d2
kameře	kamera	k1gFnSc3
PlayStation	PlayStation	k1gInSc4
Camera	Camero	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
PlayStation	PlayStation	k1gInSc1
Camera	Camero	k1gNnSc2
</s>
<s>
PlayStation	PlayStation	k1gInSc1
Camera	Camero	k1gNnSc2
na	na	k7c4
PS4	PS4	k1gFnSc4
</s>
<s>
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
kameru	kamera	k1gFnSc4
výhradně	výhradně	k6eAd1
určenou	určený	k2eAgFnSc4d1
pro	pro	k7c4
PlayStation	PlayStation	k1gInSc4
4	#num#	k4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
snímá	snímat	k5eAaImIp3nS
pohyb	pohyb	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahuje	obsahovat	k5eAaImIp3nS
dva	dva	k4xCgInPc4
optické	optický	k2eAgInPc4d1
snímače	snímač	k1gInPc4
s	s	k7c7
rozlišením	rozlišení	k1gNnSc7
1280	#num#	k4
<g/>
x	x	k?
<g/>
800	#num#	k4
<g/>
px	px	k?
<g/>
,	,	kIx,
clonou	clona	k1gFnSc7
f	f	k?
<g/>
/	/	kIx~
<g/>
2.0	2.0	k4
s	s	k7c7
<g/>
,	,	kIx,
ohniskovou	ohniskový	k2eAgFnSc7d1
vzdáleností	vzdálenost	k1gFnSc7
30	#num#	k4
<g/>
cm	cm	kA
a	a	k8xC
zorným	zorný	k2eAgNnSc7d1
polem	pole	k1gNnSc7
85	#num#	k4
<g/>
°	°	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nastavení	nastavení	k1gNnSc1
kamery	kamera	k1gFnSc2
umožňuje	umožňovat	k5eAaImIp3nS
různé	různý	k2eAgInPc4d1
typy	typ	k1gInPc4
provozování	provozování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
snímače	snímač	k1gInPc1
v	v	k7c6
kameře	kamera	k1gFnSc6
spolu	spolu	k6eAd1
umožňují	umožňovat	k5eAaImIp3nP
vytvořit	vytvořit	k5eAaPmF
objekty	objekt	k1gInPc4
v	v	k7c6
jejich	jejich	k3xOp3gNnSc6
zorném	zorný	k2eAgNnSc6d1
poli	pole	k1gNnSc6
díky	díky	k7c3
fotogrammetrii	fotogrammetrie	k1gFnSc3
<g/>
,	,	kIx,
tedy	tedy	k8xC
podobně	podobně	k6eAd1
jako	jako	k9
Kinect	Kinect	k1gInSc1
u	u	k7c2
Xboxu	Xbox	k1gInSc2
360	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eventuálně	eventuálně	k6eAd1
jde	jít	k5eAaImIp3nS
jeden	jeden	k4xCgInSc4
snímač	snímač	k1gInSc4
využít	využít	k5eAaPmF
pro	pro	k7c4
tvorbu	tvorba	k1gFnSc4
videa	video	k1gNnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
druhý	druhý	k4xOgInSc4
snímač	snímač	k1gInSc4
by	by	kYmCp3nS
byl	být	k5eAaImAgInS
využit	využít	k5eAaPmNgInS
pro	pro	k7c4
zaznamenávání	zaznamenávání	k1gNnSc4
pohybů	pohyb	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
PlayStation	PlayStation	k1gInSc1
Camera	Camero	k1gNnSc2
také	také	k9
obsahuje	obsahovat	k5eAaImIp3nS
čtyřkanálový	čtyřkanálový	k2eAgInSc4d1
mikrofon	mikrofon	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
slouží	sloužit	k5eAaImIp3nS
pro	pro	k7c4
potlačení	potlačení	k1gNnSc4
nežádoucích	žádoucí	k2eNgInPc2d1
zvuků	zvuk	k1gInPc2
v	v	k7c6
pozadí	pozadí	k1gNnSc6
a	a	k8xC
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
případně	případně	k6eAd1
použit	použít	k5eAaPmNgInS
pro	pro	k7c4
vykonávání	vykonávání	k1gNnSc4
příkazů	příkaz	k1gInPc2
hlasem	hlasem	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozměry	rozměra	k1gFnPc4
kamery	kamera	k1gFnSc2
jsou	být	k5eAaImIp3nP
186	#num#	k4
<g/>
x	x	k?
<g/>
27	#num#	k4
<g/>
x	x	k?
<g/>
27	#num#	k4
<g/>
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kamera	kamera	k1gFnSc1
může	moct	k5eAaImIp3nS
natáčet	natáčet	k5eAaImF
ve	v	k7c6
formátu	formát	k1gInSc6
RAW	RAW	kA
nebo	nebo	k8xC
YUV	YUV	kA
a	a	k8xC
ke	k	k7c3
konzoli	konzole	k1gFnSc3
se	se	k3xPyFc4
připojuje	připojovat	k5eAaImIp3nS
přes	přes	k7c4
pomocný	pomocný	k2eAgInSc4d1
(	(	kIx(
<g/>
neboli	neboli	k8xC
Aux	Aux	k1gFnSc1
<g/>
)	)	kIx)
konektor	konektor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
PlayStation	PlayStation	k1gInSc1
Camera	Camero	k1gNnSc2
byla	být	k5eAaImAgFnS
vydána	vydán	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
volitelné	volitelný	k2eAgNnSc1d1
příslušenství	příslušenství	k1gNnSc1
a	a	k8xC
prodává	prodávat	k5eAaImIp3nS
se	se	k3xPyFc4
tedy	tedy	k9
samostatně	samostatně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
PS4	PS4	k4
Camera	Camera	k1gFnSc1
Version	Version	k1gInSc1
2	#num#	k4
(	(	kIx(
<g/>
CUH-ZEY	CUH-ZEY	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
má	mít	k5eAaImIp3nS
novou	nový	k2eAgFnSc4d1
kompaktní	kompaktní	k2eAgFnSc4d1
válcovou	válcový	k2eAgFnSc4d1
formu	forma	k1gFnSc4
a	a	k8xC
stojan	stojan	k1gInSc1
umožňující	umožňující	k2eAgInSc1d1
jednodušeji	jednoduše	k6eAd2
přizpůsobit	přizpůsobit	k5eAaPmF
úhel	úhel	k1gInSc4
kamery	kamera	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
PlayStation	PlayStation	k1gInSc1
VR	vr	k0
</s>
<s>
PlayStation	PlayStation	k1gInSc1
VR	vr	k0
</s>
<s>
PlayStation	PlayStation	k1gInSc1
VR	vr	k0
je	být	k5eAaImIp3nS
systém	systém	k1gInSc1
virtuální	virtuální	k2eAgFnSc2d1
reality	realita	k1gFnSc2
pro	pro	k7c4
systém	systém	k1gInSc4
PlayStation	PlayStation	k1gInSc4
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Náhlavní	náhlavní	k2eAgFnSc1d1
souprava	souprava	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
panel	panel	k1gInSc4
displeje	displej	k1gFnSc2
s	s	k7c7
rozlišením	rozlišení	k1gNnSc7
1080	#num#	k4
<g/>
p	p	k?
<g/>
,	,	kIx,
LED	LED	kA
světla	světlo	k1gNnSc2
pro	pro	k7c4
sledování	sledování	k1gNnSc4
jejího	její	k3xOp3gInSc2
pohybu	pohyb	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
ovládací	ovládací	k2eAgFnSc1d1
skříň	skříň	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
zpracovává	zpracovávat	k5eAaImIp3nS
3D	3D	k4
zvukové	zvukový	k2eAgInPc4d1
efekty	efekt	k1gInPc4
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
výstup	výstup	k1gInSc4
videa	video	k1gNnSc2
na	na	k7c4
externí	externí	k2eAgInSc4d1
displej	displej	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PlayStation	PlayStation	k1gInSc1
VR	vr	k0
lze	lze	k6eAd1
také	také	k9
použít	použít	k5eAaPmF
s	s	k7c7
pohybovým	pohybový	k2eAgInSc7d1
ovladačem	ovladač	k1gInSc7
PlayStation	PlayStation	k1gInSc1
Move	Move	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Ostatní	ostatní	k2eAgNnPc4d1
zařízení	zařízení	k1gNnPc4
</s>
<s>
Z	z	k7c2
oficiálních	oficiální	k2eAgInPc2d1
periferiích	periferie	k1gFnPc6
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
k	k	k7c3
PS4	PS4	k1gFnSc3
také	také	k9
připojit	připojit	k5eAaPmF
klávesnice	klávesnice	k1gFnSc1
a	a	k8xC
myš	myš	k1gFnSc1
Hori	Hor	k1gFnSc2
Tactical	Tactical	k1gFnSc1
Assault	Assault	k1gMnSc1
Commander	Commander	k1gInSc1
a	a	k8xC
závodní	závodní	k2eAgInPc1d1
volanty	volant	k1gInPc1
značek	značka	k1gFnPc2
Hori	Hori	k1gNnSc2
<g/>
,	,	kIx,
Thrustmaster	Thrustmastra	k1gFnPc2
a	a	k8xC
Logitech	Logit	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Chytré	chytrý	k2eAgInPc1d1
mobily	mobil	k1gInPc1
a	a	k8xC
tablety	tableta	k1gFnPc1
od	od	k7c2
společnosti	společnost	k1gFnSc2
Sony	Sony	kA
vč.	vč.	k?
herní	herní	k2eAgFnSc3d1
přenosné	přenosný	k2eAgFnSc3d1
konzole	konzola	k1gFnSc3
<g/>
,	,	kIx,
PlayStation	PlayStation	k1gInSc4
Vita	vít	k5eAaImNgFnS
<g/>
,	,	kIx,
dokáží	dokázat	k5eAaPmIp3nP
komunikovat	komunikovat	k5eAaImF
s	s	k7c7
PS4	PS4	k1gFnSc7
a	a	k8xC
na	na	k7c6
svých	svůj	k3xOyFgInPc6
displejích	displej	k1gInPc6
můžou	můžou	k?
zobrazit	zobrazit	k5eAaPmF
obraz	obraz	k1gInSc4
z	z	k7c2
konzole	konzola	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimoto	mimoto	k6eAd1
mohou	moct	k5eAaImIp3nP
konzoli	konzoli	k6eAd1
uvést	uvést	k5eAaPmF
do	do	k7c2
režimu	režim	k1gInSc2
spánku	spánek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Remote	Remot	k1gMnSc5
Play	play	k0
</s>
<s>
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
vychytávku	vychytávka	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
umožňuje	umožňovat	k5eAaImIp3nS
obraz	obraz	k1gInSc4
vč.	vč.	k?
zvuku	zvuk	k1gInSc2
přesměrovat	přesměrovat	k5eAaPmF
z	z	k7c2
PlayStationu	PlayStation	k1gInSc2
4	#num#	k4
do	do	k7c2
přenosné	přenosný	k2eAgFnSc3d1
herní	herní	k2eAgFnSc3d1
konzole	konzola	k1gFnSc3
PlayStation	PlayStation	k1gInSc4
Vita	vit	k2eAgFnSc1d1
či	či	k8xC
smartphonů	smartphon	k1gInPc2
a	a	k8xC
tabletů	tablet	k1gInPc2
XPERIA	XPERIA	kA
<g/>
™	™	k?
místo	místo	k7c2
televize	televize	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
podstatě	podstata	k1gFnSc6
tato	tento	k3xDgFnSc1
vlastnost	vlastnost	k1gFnSc1
umožňuje	umožňovat	k5eAaImIp3nS
hrát	hrát	k5eAaImF
hry	hra	k1gFnPc4
z	z	k7c2
konzolí	konzolí	k1gNnSc2
na	na	k7c6
přenosných	přenosný	k2eAgNnPc6d1
zařízeních	zařízení	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
obdobu	obdoba	k1gFnSc4
funkce	funkce	k1gFnSc2
Off-TV	Off-TV	k1gFnSc2
Play	play	k0
u	u	k7c2
konkurenční	konkurenční	k2eAgFnSc3d1
konzole	konzola	k1gFnSc3
Nintendo	Nintendo	k6eAd1
Wii	Wii	k1gMnPc1
U.	U.	kA
Co	co	k9
se	se	k3xPyFc4
týče	týkat	k5eAaImIp3nS
oficiální	oficiální	k2eAgFnSc2d1
kompatibility	kompatibilita	k1gFnSc2
<g/>
,	,	kIx,
Sony	Sony	kA
oznámila	oznámit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
veškeré	veškerý	k3xTgFnPc4
hry	hra	k1gFnPc4
určené	určený	k2eAgFnPc4d1
pro	pro	k7c4
PS4	PS4	k1gFnSc4
budou	být	k5eAaImBp3nP
podporovat	podporovat	k5eAaImF
funkci	funkce	k1gFnSc4
Remote	Remot	k1gInSc5
Play	play	k0
a	a	k8xC
tudíž	tudíž	k8xC
budou	být	k5eAaImBp3nP
moci	moct	k5eAaImF
být	být	k5eAaImF
hrány	hrát	k5eAaImNgInP
na	na	k7c6
PS	PS	kA
Vitě	Vitě	k1gMnSc4
a	a	k8xC
zařízeních	zařízení	k1gNnPc6
XPERIA	XPERIA	kA
<g/>
.	.	kIx.
</s>
<s>
PlayStation	PlayStation	k1gInSc1
App	App	k1gFnSc2
</s>
<s>
Je	být	k5eAaImIp3nS
aplikace	aplikace	k1gFnPc4
pro	pro	k7c4
mobilní	mobilní	k2eAgInPc4d1
telefony	telefon	k1gInPc4
s	s	k7c7
operačním	operační	k2eAgInSc7d1
systémem	systém	k1gInSc7
Apple	Apple	kA
iOS	iOS	k?
nebo	nebo	k8xC
Google	Google	k1gFnSc1
Android	android	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aplikace	aplikace	k1gFnSc1
umožňuje	umožňovat	k5eAaImIp3nS
komunikovat	komunikovat	k5eAaImF
s	s	k7c7
PS4	PS4	k1gFnSc7
přes	přes	k7c4
smartphone	smartphon	k1gMnSc5
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k8xC,k8xS
program	program	k1gInSc4
SmartGlass	SmartGlassa	k1gFnPc2
u	u	k7c2
konkurenčního	konkurenční	k2eAgInSc2d1
Xboxu	Xbox	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hráči	hráč	k1gMnPc1
mohou	moct	k5eAaImIp3nP
tedy	tedy	k9
tuto	tento	k3xDgFnSc4
aplikaci	aplikace	k1gFnSc4
použít	použít	k5eAaPmF
např.	např.	kA
pro	pro	k7c4
nakupování	nakupování	k1gNnSc4
her	hra	k1gFnPc2
<g/>
,	,	kIx,
když	když	k8xS
jsou	být	k5eAaImIp3nP
mimo	mimo	k7c4
domov	domov	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
přímém	přímý	k2eAgInSc6d1
přenosu	přenos	k1gInSc6
sledovat	sledovat	k5eAaImF
přátele	přítel	k1gMnPc4
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
hrají	hrát	k5eAaImIp3nP
některou	některý	k3yIgFnSc7
svojí	svojit	k5eAaImIp3nP
hru	hra	k1gFnSc4
<g/>
,	,	kIx,
mohou	moct	k5eAaImIp3nP
chatovat	chatovat	k5eAaPmF,k5eAaBmF,k5eAaImF
se	s	k7c7
svými	svůj	k3xOyFgInPc7
kontakty	kontakt	k1gInPc7
v	v	k7c4
PlayStation	PlayStation	k1gInSc4
Network	network	k1gInSc1
atd.	atd.	kA
</s>
<s>
Hardware	hardware	k1gInSc1
původní	původní	k2eAgFnSc2d1
verze	verze	k1gFnSc2
</s>
<s>
PlayStation	PlayStation	k1gInSc1
4	#num#	k4
</s>
<s>
Specifikace	specifikace	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
CPU	CPU	kA
<g/>
:	:	kIx,
osmijádrový	osmijádrový	k2eAgInSc1d1
28	#num#	k4
<g/>
nm	nm	k?
64	#num#	k4
<g/>
bitový	bitový	k2eAgInSc1d1
procesor	procesor	k1gInSc1
AMD	AMD	kA
Jaguar	Jaguar	k1gInSc4
1.6	1.6	k4
GHz	GHz	k1gFnPc2
</s>
<s>
GPU	GPU	kA
<g/>
:	:	kIx,
GPGPU	GPGPU	kA
AMD	AMD	kA
Radeon	Radeona	k1gFnPc2
GCN	GCN	kA
800	#num#	k4
MHz	Mhz	kA
<g/>
,	,	kIx,
až	až	k9
1.84	1.84	k4
TFLOPS	TFLOPS	kA
</s>
<s>
Operační	operační	k2eAgFnSc1d1
paměť	paměť	k1gFnSc1
<g/>
:	:	kIx,
8	#num#	k4
GB	GB	kA
RAM	RAM	kA
GDDR5	GDDR5	k1gFnSc1
2.75	2.75	k4
GHz	GHz	k1gFnPc2
</s>
<s>
Blu-ray	Blu-raa	k1gFnPc1
a	a	k8xC
DVD	DVD	kA
mechanika	mechanika	k1gFnSc1
</s>
<s>
HDD	HDD	kA
<g/>
:	:	kIx,
500GB	500GB	k4
SATA	SATA	kA
(	(	kIx(
<g/>
vyměnitelný	vyměnitelný	k2eAgInSc4d1
i	i	k9
za	za	k7c4
HDD	HDD	kA
s	s	k7c7
větší	veliký	k2eAgFnSc7d2
kapacitou	kapacita	k1gFnSc7
<g/>
,	,	kIx,
podporuje	podporovat	k5eAaImIp3nS
i	i	k9
SSD	SSD	kA
disky	disk	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
Bezdrátové	bezdrátový	k2eAgNnSc1d1
spojení	spojení	k1gNnSc1
<g/>
:	:	kIx,
Wi-Fi	Wi-Fi	k1gNnSc1
(	(	kIx(
<g/>
2.4	2.4	k4
<g/>
GHz	GHz	k1gFnSc1
802.11	802.11	k4
<g/>
b	b	k?
<g/>
/	/	kIx~
<g/>
g	g	kA
<g/>
/	/	kIx~
<g/>
n	n	k0
<g/>
)	)	kIx)
</s>
<s>
Bluetooth	Bluetooth	k1gInSc1
2.1	2.1	k4
</s>
<s>
Konektory	konektor	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
2	#num#	k4
<g/>
×	×	k?
USB	USB	kA
3.0	3.0	k4
</s>
<s>
Výstup	výstup	k1gInSc1
HDMI	HDMI	kA
1.4	1.4	k4
(	(	kIx(
<g/>
po	po	k7c6
aktualizaci	aktualizace	k1gFnSc6
softwaru	software	k1gInSc2
2.0	2.0	k4
<g/>
)	)	kIx)
</s>
<s>
TOSLINK	TOSLINK	kA
optický	optický	k2eAgInSc1d1
zvukový	zvukový	k2eAgInSc1d1
výstup	výstup	k1gInSc1
</s>
<s>
LAN	lano	k1gNnPc2
vstup	vstup	k1gInSc4
pro	pro	k7c4
Ethernet	Ethernet	k1gInSc4
</s>
<s>
AUX	AUX	kA
vstup	vstup	k1gInSc1
pro	pro	k7c4
kameru	kamera	k1gFnSc4
</s>
<s>
Dualshock	Dualshock	k1gInSc1
4	#num#	k4
<g/>
:	:	kIx,
</s>
<s>
Tlačítko	tlačítko	k1gNnSc1
Options	Optionsa	k1gFnPc2
<g/>
/	/	kIx~
<g/>
Share	Shar	k1gMnSc5
</s>
<s>
Dotyková	dotykový	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
touch	toucha	k1gFnPc2
pad	padnout	k5eAaPmDgInS
(	(	kIx(
<g/>
s	s	k7c7
proklikem	proklikum	k1gNnSc7
<g/>
)	)	kIx)
</s>
<s>
Mono	mono	k2eAgInSc1d1
reproduktor	reproduktor	k1gInSc1
</s>
<s>
Jack	Jack	k6eAd1
konektor	konektor	k1gInSc4
pro	pro	k7c4
sluchátka	sluchátko	k1gNnPc4
(	(	kIx(
<g/>
i	i	k9
s	s	k7c7
mikrofonem	mikrofon	k1gInSc7
<g/>
)	)	kIx)
</s>
<s>
Světelná	světelný	k2eAgFnSc1d1
lišta	lišta	k1gFnSc1
–	–	k?
Light	Light	k1gInSc1
Bar	bar	k1gInSc1
(	(	kIx(
<g/>
pro	pro	k7c4
snímání	snímání	k1gNnSc4
ovladače	ovladač	k1gInSc2
kamerou	kamera	k1gFnSc7
<g/>
)	)	kIx)
</s>
<s>
Software	software	k1gInSc1
</s>
<s>
PlayStation	PlayStation	k1gInSc1
Network	network	k1gInSc2
logo	logo	k1gNnSc4
</s>
<s>
PlayStation	PlayStation	k1gInSc1
4	#num#	k4
běží	běžet	k5eAaImIp3nS
na	na	k7c6
operačním	operační	k2eAgInSc6d1
systému	systém	k1gInSc6
Orbis	orbis	k1gInSc1
OS	OS	kA
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
založen	založit	k5eAaPmNgInS
na	na	k7c6
operačním	operační	k2eAgInSc6d1
systému	systém	k1gInSc6
FreeBSD	FreeBSD	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Konzole	konzola	k1gFnSc6
přímo	přímo	k6eAd1
nevyžaduje	vyžadovat	k5eNaImIp3nS
k	k	k7c3
fungování	fungování	k1gNnSc2
internetové	internetový	k2eAgNnSc4d1
připojení	připojení	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
připojena	připojit	k5eAaPmNgFnS
<g/>
,	,	kIx,
získá	získat	k5eAaPmIp3nS
tím	ten	k3xDgNnSc7
bohatší	bohatý	k2eAgFnSc4d2
funkcionalitu	funkcionalita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Síť	síť	k1gFnSc1
PlayStation	PlayStation	k1gInSc4
Network	network	k1gInSc1
umožní	umožnit	k5eAaPmIp3nS
uživatelům	uživatel	k1gMnPc3
se	se	k3xPyFc4
připojit	připojit	k5eAaPmF
k	k	k7c3
mnohým	mnohý	k2eAgFnPc3d1
online	onlinout	k5eAaPmIp3nS
službám	služba	k1gFnPc3
včetně	včetně	k7c2
online	onlinout	k5eAaPmIp3nS
obchodu	obchod	k1gInSc3
PlayStationu	PlayStation	k1gInSc2
Store	Stor	k1gInSc5
<g/>
,	,	kIx,
zpoplatněných	zpoplatněný	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
Video	video	k1gNnSc1
Unlimited	Unlimited	k1gMnSc1
<g/>
,	,	kIx,
Spotify	Spotif	k1gInPc1
(	(	kIx(
<g/>
placená	placený	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
pouze	pouze	k6eAd1
premium	premium	k1gNnSc1
verze	verze	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
další	další	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uživatelé	uživatel	k1gMnPc1
také	také	k9
mohou	moct	k5eAaImIp3nP
procházet	procházet	k5eAaImF
herní	herní	k2eAgInPc4d1
tituly	titul	k1gInPc4
a	a	k8xC
mohou	moct	k5eAaImIp3nP
je	on	k3xPp3gMnPc4
streamovat	streamovat	k5eAaBmF,k5eAaPmF,k5eAaImF
přes	přes	k7c4
Gaikai	Gaikae	k1gFnSc4
téměř	téměř	k6eAd1
okamžitě	okamžitě	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přístup	přístup	k1gInSc1
k	k	k7c3
multiplayeru	multiplayero	k1gNnSc3
v	v	k7c6
každé	každý	k3xTgFnSc6
hře	hra	k1gFnSc6
kromě	kromě	k7c2
free-to-play	free-to-plaa	k1gFnSc2
verzím	verze	k1gFnPc3
bude	být	k5eAaImBp3nS
vyžadovat	vyžadovat	k5eAaImF
členství	členství	k1gNnSc4
v	v	k7c4
PlayStation	PlayStation	k1gInSc4
Plus	plus	k1gInSc1
službě	služba	k1gFnSc3
<g/>
,	,	kIx,
které	který	k3yIgFnSc3,k3yQgFnSc3,k3yRgFnSc3
je	být	k5eAaImIp3nS
zpoplatněno	zpoplatněn	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
Sony	Sony	kA
nepovolila	povolit	k5eNaPmAgFnS
na	na	k7c6
své	svůj	k3xOyFgFnSc6
konzoli	konzole	k1gFnSc6
tzv.	tzv.	kA
online	onlinout	k5eAaPmIp3nS
passy	passa	k1gFnPc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
kód	kód	k1gInSc4
<g/>
,	,	kIx,
díky	díky	k7c3
kterému	který	k3yRgNnSc3,k3yQgNnSc3,k3yIgNnSc3
může	moct	k5eAaImIp3nS
osoba	osoba	k1gFnSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
zakoupila	zakoupit	k5eAaPmAgFnS
již	jenž	k3xRgFnSc4
použitou	použitý	k2eAgFnSc4d1
hru	hra	k1gFnSc4
<g/>
,	,	kIx,
hrát	hrát	k5eAaImF
multiplayer	multiplayer	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Online	Onlin	k1gInSc5
pass	passa	k1gFnPc2
byla	být	k5eAaImAgFnS
reakce	reakce	k1gFnSc1
herních	herní	k2eAgMnPc2d1
vydavatelů	vydavatel	k1gMnPc2
na	na	k7c4
stále	stále	k6eAd1
populárnější	populární	k2eAgInPc4d2
nákupy	nákup	k1gInPc4
her	hra	k1gFnPc2
v	v	k7c6
bazarech	bazar	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sony	Sony	kA
má	mít	k5eAaImIp3nS
v	v	k7c6
úmyslu	úmysl	k1gInSc6
ještě	ještě	k6eAd1
rozšířit	rozšířit	k5eAaPmF
a	a	k8xC
rozvinout	rozvinout	k5eAaPmF
stávající	stávající	k2eAgFnPc4d1
služby	služba	k1gFnPc4
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
prodloužit	prodloužit	k5eAaPmF
životnost	životnost	k1gFnSc4
konzole	konzola	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
PlayStation	PlayStation	k1gInSc1
4	#num#	k4
podporuje	podporovat	k5eAaImIp3nS
přehrávání	přehrávání	k1gNnSc4
Blu-ray	Blu-raa	k1gFnSc2
a	a	k8xC
DVD	DVD	kA
disků	disk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přehrávání	přehrávání	k1gNnPc4
CD	CD	kA
disků	disk	k1gInPc2
není	být	k5eNaImIp3nS
podporováno	podporován	k2eAgNnSc1d1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
jelikož	jelikož	k8xS
konzole	konzola	k1gFnSc3
již	již	k6eAd1
nedisponuje	disponovat	k5eNaBmIp3nS
infračerveným	infračervený	k2eAgNnSc7d1
780	#num#	k4
nm	nm	k?
laserem	laser	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlastní	vlastní	k2eAgFnSc1d1
audio	audio	k2eAgFnSc1d1
a	a	k8xC
video	video	k1gNnSc1
soubory	soubor	k1gInPc1
však	však	k9
lze	lze	k6eAd1
přehrávat	přehrávat	k5eAaImF
z	z	k7c2
jednotek	jednotka	k1gFnPc2
USB	USB	kA
a	a	k8xC
ze	z	k7c2
serverů	server	k1gInPc2
DLNA	DLNA	kA
pomocí	pomocí	k7c2
aplikace	aplikace	k1gFnSc2
Media	medium	k1gNnSc2
Player	Playra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Uživatelské	uživatelský	k2eAgNnSc1d1
rozhraní	rozhraní	k1gNnSc1
v	v	k7c6
PS4	PS4	k1gFnSc6
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
PlayStation	PlayStation	k1gInSc1
Dynamic	Dynamice	k1gFnPc2
Menu	menu	k1gNnSc1
a	a	k8xC
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
nástupce	nástupce	k1gMnSc1
XrossMediaBar	XrossMediaBar	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
Sony	Sony	kA
používala	používat	k5eAaImAgFnS
u	u	k7c2
PlayStationu	PlayStation	k1gInSc2
3	#num#	k4
<g/>
,	,	kIx,
PlayStationu	PlayStation	k1gInSc2
Portable	portable	k1gInSc1
a	a	k8xC
PSX	PSX	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nové	Nové	k2eAgNnSc7d1
menu	menu	k1gNnSc7
je	být	k5eAaImIp3nS
oproti	oproti	k7c3
XMB	XMB	kA
více	hodně	k6eAd2
přizpůsobitelné	přizpůsobitelný	k2eAgInPc1d1
podle	podle	k7c2
hráče	hráč	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uživatelský	uživatelský	k2eAgInSc1d1
profil	profil	k1gInSc1
hráčům	hráč	k1gMnPc3
zobrazuje	zobrazovat	k5eAaImIp3nS
nedávnou	dávný	k2eNgFnSc4d1
aktivitu	aktivita	k1gFnSc4
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gNnPc1
celá	celý	k2eAgNnPc1d1
jména	jméno	k1gNnPc1
a	a	k8xC
další	další	k2eAgFnPc4d1
podrobnosti	podrobnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Domovská	domovský	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
PS4	PS4	k1gFnPc2
též	též	k9
obsahuje	obsahovat	k5eAaImIp3nS
uzpůsobený	uzpůsobený	k2eAgInSc4d1
obsah	obsah	k1gInSc4
přátel	přítel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Služby	služba	k1gFnPc1
třetích	třetí	k4xOgFnPc2
stran	strana	k1gFnPc2
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
Netflix	Netflix	k1gInSc1
a	a	k8xC
Amazon	amazona	k1gFnPc2
Instant	Instant	k1gInSc4
Video	video	k1gNnSc1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
dostupné	dostupný	k2eAgFnPc1d1
skrze	skrze	k?
nové	nový	k2eAgNnSc1d1
uživatelské	uživatelský	k2eAgNnSc1d1
prostředí	prostředí	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
tak	tak	k9
možné	možný	k2eAgNnSc1d1
dělat	dělat	k5eAaImF
více	hodně	k6eAd2
úloh	úloha	k1gFnPc2
najednou	najednou	k6eAd1
<g/>
,	,	kIx,
např.	např.	kA
otevřít	otevřít	k5eAaPmF
internetový	internetový	k2eAgInSc4d1
prohlížeč	prohlížeč	k1gInSc4
při	při	k7c6
rozehrané	rozehraný	k2eAgFnSc6d1
hře	hra	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
se	se	k3xPyFc4
týče	týkat	k5eAaImIp3nS
ovládání	ovládání	k1gNnSc1
hlasem	hlas	k1gInSc7
<g/>
,	,	kIx,
mikrofon	mikrofon	k1gInSc1
nebo	nebo	k8xC
kamera	kamera	k1gFnSc1
umožňují	umožňovat	k5eAaImIp3nP
uživateli	uživatel	k1gMnSc3
ovládat	ovládat	k5eAaImF
konzoli	konzole	k1gFnSc4
skrze	skrze	k?
hlasové	hlasový	k2eAgInPc4d1
příkazy	příkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Služby	služba	k1gFnPc1
</s>
<s>
PlayStation	PlayStation	k1gInSc1
Network	network	k1gInSc1
</s>
<s>
URL	URL	kA
</s>
<s>
www.playstation.com	www.playstation.com	k1gInSc1
Charakter	charakter	k1gInSc1
stránky	stránka	k1gFnSc2
</s>
<s>
Online	Onlinout	k5eAaPmIp3nS
služba	služba	k1gFnSc1
Vlastník	vlastník	k1gMnSc1
</s>
<s>
Sony	Sony	kA
Corporation	Corporation	k1gInSc1
Autor	autor	k1gMnSc1
</s>
<s>
Sony	Sony	kA
Interactive	Interactiv	k1gInSc5
Entertainment	Entertainment	k1gInSc4
Spuštěno	spustit	k5eAaPmNgNnS
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2006	#num#	k4
Aktuální	aktuální	k2eAgInSc1d1
stav	stav	k1gInSc1
</s>
<s>
110	#num#	k4
milionů	milion	k4xCgInPc2
členů	člen	k1gInPc2
</s>
<s>
PlayStation	PlayStation	k1gInSc1
Network	network	k1gInSc1
</s>
<s>
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
komplexní	komplexní	k2eAgFnSc1d1
online	onlinout	k5eAaPmIp3nS
služba	služba	k1gFnSc1
poskytovaná	poskytovaný	k2eAgFnSc1d1
výhradně	výhradně	k6eAd1
herním	herní	k2eAgFnPc3d1
konzolím	konzole	k1gFnPc3
PlayStation	PlayStation	k1gInSc4
od	od	k7c2
společnosti	společnost	k1gFnSc2
Sony	Sony	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
podpory	podpora	k1gFnSc2
PlayStationu	PlayStation	k1gInSc2
4	#num#	k4
je	být	k5eAaImIp3nS
dostupná	dostupný	k2eAgFnSc1d1
i	i	k9
pro	pro	k7c4
PlayStation	PlayStation	k1gInSc4
3	#num#	k4
<g/>
,	,	kIx,
PlayStation	PlayStation	k1gInSc1
Portable	portable	k1gInSc1
a	a	k8xC
PlayStation	PlayStation	k1gInSc4
Vita	vit	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
PSN	PSN	kA
je	být	k5eAaImIp3nS
část	část	k1gFnSc4
služby	služba	k1gFnSc2
Sony	Sony	kA
Entertainment	Entertainment	k1gInSc1
Network	network	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
chce	chtít	k5eAaImIp3nS
být	být	k5eAaImF
uživatel	uživatel	k1gMnSc1
na	na	k7c6
síti	síť	k1gFnSc6
PSN	PSN	kA
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nP
mít	mít	k5eAaImF
svou	svůj	k3xOyFgFnSc4
přezdívku	přezdívka	k1gFnSc4
(	(	kIx(
<g/>
označováno	označován	k2eAgNnSc1d1
jako	jako	k9
online	onlinout	k5eAaPmIp3nS
ID	Ida	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
automaticky	automaticky	k6eAd1
generované	generovaný	k2eAgInPc4d1
přenosné	přenosný	k2eAgInPc4d1
ID	ido	k1gNnPc2
a	a	k8xC
svůj	svůj	k3xOyFgInSc4
založený	založený	k2eAgInSc4d1
profil	profil	k1gInSc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
kromě	kromě	k7c2
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
také	také	k6eAd1
profilový	profilový	k2eAgInSc4d1
obrázek	obrázek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PSN	PSN	kA
má	mít	k5eAaImIp3nS
také	také	k9
zabudovaný	zabudovaný	k2eAgInSc1d1
systém	systém	k1gInSc1
odměn	odměna	k1gFnPc2
zvaný	zvaný	k2eAgInSc4d1
Trophies	Trophies	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgFnSc7d3
konkurencí	konkurence	k1gFnSc7
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
oboru	obor	k1gInSc6
je	být	k5eAaImIp3nS
služba	služba	k1gFnSc1
Xbox	Xbox	k1gInSc4
Live	Liv	k1gFnSc2
společnosti	společnost	k1gFnSc2
Microsoft	Microsoft	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
je	být	k5eAaImIp3nS
PS	PS	kA
Network	network	k1gInSc1
dostupný	dostupný	k2eAgInSc1d1
v	v	k7c6
63	#num#	k4
zemích	zem	k1gFnPc6
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Pod	pod	k7c7
pojmem	pojem	k1gInSc7
PlayStation	PlayStation	k1gInSc1
Network	network	k1gInSc2
se	se	k3xPyFc4
de	de	k?
facto	facto	k1gNnSc4
skrývají	skrývat	k5eAaImIp3nP
jednotlivé	jednotlivý	k2eAgFnPc4d1
online	onlinout	k5eAaPmIp3nS
služby	služba	k1gFnPc4
–	–	k?
online	onlinout	k5eAaPmIp3nS
obchod	obchod	k1gInSc1
PlayStation	PlayStation	k1gInSc1
Store	Stor	k1gInSc5
<g/>
,	,	kIx,
služba	služba	k1gFnSc1
PlayStation	PlayStation	k1gInSc1
Plus	plus	k1gInSc1
<g/>
,	,	kIx,
video	video	k1gNnSc1
portál	portál	k1gInSc1
YouTube	YouTub	k1gInSc5
<g/>
,	,	kIx,
sociální	sociální	k2eAgFnSc4d1
síť	síť	k1gFnSc4
Facebook	Facebook	k1gInSc1
<g/>
,	,	kIx,
rodičovská	rodičovský	k2eAgFnSc1d1
ochrana	ochrana	k1gFnSc1
<g/>
,	,	kIx,
novinky	novinka	k1gFnPc1
ze	z	k7c2
světa	svět	k1gInSc2
PlayStation	PlayStation	k1gInSc1
„	„	k?
<g/>
What	What	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
New	New	k1gFnSc7
<g/>
“	“	k?
<g/>
,	,	kIx,
možnost	možnost	k1gFnSc4
chatovat	chatovat	k5eAaImF,k5eAaPmF,k5eAaBmF
/	/	kIx~
volat	volat	k5eAaImF
s	s	k7c7
přáteli	přítel	k1gMnPc7
<g/>
,	,	kIx,
možnost	možnost	k1gFnSc4
hrát	hrát	k5eAaImF
videohry	videohra	k1gFnPc4
po	po	k7c6
Internetu	Internet	k1gInSc6
<g/>
,	,	kIx,
aktualizační	aktualizační	k2eAgFnSc1d1
služba	služba	k1gFnSc1
firmwaru	firmware	k1gInSc2
apod.	apod.	kA
</s>
<s>
PlayStation	PlayStation	k1gInSc1
Store	Stor	k1gMnSc5
</s>
<s>
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
virtuální	virtuální	k2eAgFnPc4d1
online	onlinout	k5eAaPmIp3nS
obchod	obchod	k1gInSc1
<g/>
,	,	kIx,
spuštěný	spuštěný	k2eAgInSc1d1
v	v	k7c6
listopadu	listopad	k1gInSc6
2006	#num#	k4
společně	společně	k6eAd1
s	s	k7c7
příchodem	příchod	k1gInSc7
PlayStationu	PlayStation	k1gInSc2
3	#num#	k4
<g/>
,	,	kIx,
dostupný	dostupný	k2eAgInSc1d1
přes	přes	k7c4
síť	síť	k1gFnSc4
PlayStation	PlayStation	k1gInSc4
Network	network	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS	PS	kA
Store	Stor	k1gInSc5
je	být	k5eAaImIp3nS
dostupné	dostupný	k2eAgNnSc4d1
pro	pro	k7c4
PlayStation	PlayStation	k1gInSc4
4	#num#	k4
<g/>
,	,	kIx,
PlayStation	PlayStation	k1gInSc1
3	#num#	k4
<g/>
,	,	kIx,
PlayStation	PlayStation	k1gInSc4
Vita	vit	k2eAgFnSc1d1
a	a	k8xC
PlayStation	PlayStation	k1gInSc1
Portable	portable	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lednu	leden	k1gInSc6
2013	#num#	k4
se	se	k3xPyFc4
PlayStation	PlayStation	k1gInSc1
Store	Stor	k1gInSc5
stalo	stát	k5eAaPmAgNnS
dostupným	dostupný	k2eAgMnSc7d1
přes	přes	k7c4
internetový	internetový	k2eAgInSc4d1
prohlížeč	prohlížeč	k1gInSc4
z	z	k7c2
jakéhokoliv	jakýkoliv	k3yIgNnSc2
zařízení	zařízení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tomu	ten	k3xDgInSc3
mohou	moct	k5eAaImIp3nP
hráči	hráč	k1gMnPc1
být	být	k5eAaImF
na	na	k7c6
PS	PS	kA
Store	Stor	k1gInSc5
kdykoliv	kdykoliv	k6eAd1
<g/>
,	,	kIx,
pokud	pokud	k8xS
mají	mít	k5eAaImIp3nP
dostupný	dostupný	k2eAgInSc4d1
Internet	Internet	k1gInSc4
a	a	k8xC
zařízení	zařízení	k1gNnSc4
s	s	k7c7
prohlížečem	prohlížeč	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obchod	obchod	k1gInSc1
nabízí	nabízet	k5eAaImIp3nS
širokou	široký	k2eAgFnSc4d1
škálu	škála	k1gFnSc4
stahovatelného	stahovatelný	k2eAgInSc2d1
obsahu	obsah	k1gInSc2
jak	jak	k8xS,k8xC
zdarma	zdarma	k6eAd1
<g/>
,	,	kIx,
tak	tak	k6eAd1
placeně	placeně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupný	dostupný	k2eAgInSc1d1
obsah	obsah	k1gInSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
plné	plný	k2eAgFnPc4d1
hry	hra	k1gFnPc4
<g/>
,	,	kIx,
rozšiřitelný	rozšiřitelný	k2eAgInSc4d1
obsah	obsah	k1gInSc4
<g/>
,	,	kIx,
herní	herní	k2eAgNnPc1d1
dema	demum	k1gNnPc1
<g/>
,	,	kIx,
trailery	trailer	k1gInPc1
z	z	k7c2
her	hra	k1gFnPc2
a	a	k8xC
témata	téma	k1gNnPc4
pro	pro	k7c4
konzole	konzola	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupný	dostupný	k2eAgInSc1d1
obsah	obsah	k1gInSc1
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nS
podle	podle	k7c2
regionu	region	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Placený	placený	k2eAgInSc1d1
obsah	obsah	k1gInSc1
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
platit	platit	k5eAaImF
dvěma	dva	k4xCgFnPc7
způsoby	způsob	k1gInPc7
–	–	k?
buďto	buďto	k8xC
pomocí	pomocí	k7c2
kreditní	kreditní	k2eAgFnSc2d1
karty	karta	k1gFnSc2
nebo	nebo	k8xC
zakoupeného	zakoupený	k2eAgInSc2d1
kreditu	kredit	k1gInSc2
pomocí	pomocí	k7c2
dobíjecí	dobíjecí	k2eAgFnSc2d1
karty	karta	k1gFnSc2
dostupné	dostupný	k2eAgInPc4d1
v	v	k7c6
kamenných	kamenný	k2eAgInPc6d1
řetězcích	řetězec	k1gInPc6
i	i	k9
online	onlinout	k5eAaPmIp3nS
obchodech	obchod	k1gInPc6
<g/>
,	,	kIx,
platí	platit	k5eAaImIp3nS
se	se	k3xPyFc4
pomocí	pomocí	k7c2
české	český	k2eAgFnSc2d1
koruny	koruna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
obsah	obsah	k1gInSc4
v	v	k7c4
PAL	pal	k1gInSc4
regionu	region	k1gInSc2
<g/>
,	,	kIx,
do	do	k7c2
jehož	jenž	k3xRgInSc2,k3xOyRp3gInSc2
spadá	spadat	k5eAaImIp3nS,k5eAaPmIp3nS
i	i	k9
Česko	Česko	k1gNnSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
aktualizuje	aktualizovat	k5eAaBmIp3nS
pravidelně	pravidelně	k6eAd1
každou	každý	k3xTgFnSc4
středu	středa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
PlayStation	PlayStation	k1gInSc1
Plus	plus	k1gInSc1
</s>
<s>
Oproti	oproti	k7c3
PS	PS	kA
<g/>
3	#num#	k4
<g/>
,	,	kIx,
konzole	konzola	k1gFnSc6
PS4	PS4	k1gFnSc2
vyžaduje	vyžadovat	k5eAaImIp3nS
předplacené	předplacený	k2eAgNnSc4d1
PS	PS	kA
Plus	plus	k1gInSc1
pro	pro	k7c4
hraní	hraní	k1gNnSc4
v	v	k7c6
režimu	režim	k1gInSc6
více	hodně	k6eAd2
hráčů	hráč	k1gMnPc2
(	(	kIx(
<g/>
multiplayer	multiplayer	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
služba	služba	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
podstatě	podstata	k1gFnSc6
předplatné	předplatné	k1gNnSc1
<g/>
,	,	kIx,
díky	díky	k7c3
kterému	který	k3yQgNnSc3,k3yRgNnSc3,k3yIgNnSc3
uživatelé	uživatel	k1gMnPc1
každý	každý	k3xTgInSc4
měsíc	měsíc	k1gInSc4
získají	získat	k5eAaPmIp3nP
zdarma	zdarma	k6eAd1
dvě	dva	k4xCgFnPc1
vybrané	vybraný	k2eAgFnPc1d1
hry	hra	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
jim	on	k3xPp3gMnPc3
zůstávají	zůstávat	k5eAaImIp3nP
do	do	k7c2
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
než	než	k8xS
si	se	k3xPyFc3
PS	PS	kA
Plus	plus	k1gInSc1
znovu	znovu	k6eAd1
předplatí	předplatit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalšími	další	k2eAgMnPc7d1
výhodami	výhoda	k1gFnPc7
je	on	k3xPp3gNnPc4
online	onlinout	k5eAaPmIp3nS
úložiště	úložiště	k1gNnSc1
o	o	k7c6
velikosti	velikost	k1gFnSc6
1GB	1GB	k4
(	(	kIx(
<g/>
s	s	k7c7
novým	nový	k2eAgInSc7d1
updatem	update	k1gInSc7
konzole	konzola	k1gFnSc3
na	na	k7c4
podzim	podzim	k1gInSc4
2015	#num#	k4
dojde	dojít	k5eAaPmIp3nS
k	k	k7c3
navýšeni	navýšen	k2eAgMnPc1d1
na	na	k7c4
10	#num#	k4
GB	GB	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
slevy	sleva	k1gFnPc4
na	na	k7c4
placený	placený	k2eAgInSc4d1
obsah	obsah	k1gInSc4
v	v	k7c6
obchodě	obchod	k1gInSc6
PS	ps	k0
Store	Stor	k1gMnSc5
<g/>
,	,	kIx,
dřívější	dřívější	k2eAgInSc4d1
přístup	přístup	k1gInSc4
k	k	k7c3
beta	beta	k1gNnSc3
verzím	verze	k1gFnPc3
her	hra	k1gFnPc2
<g/>
,	,	kIx,
demoverzí	demoverze	k1gFnPc2
<g/>
,	,	kIx,
prémiového	prémiový	k2eAgInSc2d1
stahovatelného	stahovatelný	k2eAgInSc2d1
obsahu	obsah	k1gInSc2
a	a	k8xC
další	další	k2eAgFnPc4d1
výhody	výhoda	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PlayStation	PlayStation	k1gInSc1
Plus	plus	k6eAd1
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
zakoupit	zakoupit	k5eAaPmF
na	na	k7c4
tři	tři	k4xCgInPc4
měsíce	měsíc	k1gInPc4
nebo	nebo	k8xC
na	na	k7c4
rok	rok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Sociální	sociální	k2eAgInPc1d1
prvky	prvek	k1gInPc1
</s>
<s>
Sony	Sony	kA
je	být	k5eAaImIp3nS
zaměřena	zaměřit	k5eAaPmNgFnS
na	na	k7c4
sociální	sociální	k2eAgInPc4d1
aspekty	aspekt	k1gInPc4
jako	jako	k8xS,k8xC
na	na	k7c6
hlavní	hlavní	k2eAgFnSc6d1
funkci	funkce	k1gFnSc6
PlayStationu	PlayStation	k1gInSc2
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
konzole	konzola	k1gFnSc6
obsahuje	obsahovat	k5eAaImIp3nS
vylepšenou	vylepšený	k2eAgFnSc4d1
funkcionalitu	funkcionalita	k1gFnSc4
se	s	k7c7
sociálními	sociální	k2eAgFnPc7d1
sítěmi	síť	k1gFnPc7
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
volitelná	volitelný	k2eAgFnSc1d1
vlastnost	vlastnost	k1gFnSc1
a	a	k8xC
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
zcela	zcela	k6eAd1
vypnuta	vypnut	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hráči	hráč	k1gMnPc1
mají	mít	k5eAaImIp3nP
na	na	k7c4
výběr	výběr	k1gInSc4
<g/>
,	,	kIx,
zda	zda	k8xS
použijí	použít	k5eAaPmIp3nP
jako	jako	k9
své	svůj	k3xOyFgNnSc4
vystupování	vystupování	k1gNnSc4
<g/>
,	,	kIx,
skutečné	skutečný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
<g/>
,	,	kIx,
svou	svůj	k3xOyFgFnSc4
přezdívku	přezdívka	k1gFnSc4
a	a	k8xC
nebo	nebo	k8xC
i	i	k9
úplnou	úplný	k2eAgFnSc4d1
anonymitu	anonymita	k1gFnSc4
<g/>
,	,	kIx,
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
důležité	důležitý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovladač	ovladač	k1gInSc1
DualShock	DualShock	k1gInSc1
4	#num#	k4
obsahuje	obsahovat	k5eAaImIp3nS
„	„	k?
<g/>
share	shar	k1gInSc5
<g/>
“	“	k?
tlačítko	tlačítko	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
hráči	hráč	k1gMnSc3
umožňuje	umožňovat	k5eAaImIp3nS
prohlížet	prohlížet	k5eAaImF
posledních	poslední	k2eAgFnPc2d1
15	#num#	k4
minut	minuta	k1gFnPc2
z	z	k7c2
hraní	hraní	k1gNnSc2
a	a	k8xC
z	z	k7c2
toho	ten	k3xDgNnSc2
vytvářet	vytvářet	k5eAaImF
vytípané	vytípaný	k2eAgInPc4d1
obrázky	obrázek	k1gInPc4
nebo	nebo	k8xC
videa	video	k1gNnPc4
vhodné	vhodný	k2eAgFnSc2d1
ke	k	k7c3
sdílení	sdílení	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgInPc4
jsou	být	k5eAaImIp3nP
z	z	k7c2
konzole	konzola	k1gFnSc3
souvisle	souvisle	k6eAd1
nahrávány	nahráván	k2eAgFnPc1d1
buďto	buďto	k8xC
k	k	k7c3
přátelům	přítel	k1gMnPc3
používajícím	používající	k2eAgMnSc7d1
též	též	k9
PSN	PSN	kA
nebo	nebo	k8xC
na	na	k7c4
sociální	sociální	k2eAgFnPc4d1
sítě	síť	k1gFnPc4
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
Facebook	Facebook	k1gInSc4
nebo	nebo	k8xC
Twitter	Twitter	k1gInSc4
a	a	k8xC
další	další	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k1gNnSc1
vlastností	vlastnost	k1gFnPc2
konzole	konzola	k1gFnSc3
je	být	k5eAaImIp3nS
živé	živý	k2eAgNnSc4d1
vysílání	vysílání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hráči	hráč	k1gMnPc1
mohou	moct	k5eAaImIp3nP
procházet	procházet	k5eAaImF
živé	živý	k2eAgInPc4d1
přenosy	přenos	k1gInPc4
jejich	jejich	k3xOp3gFnSc2
hráčům	hráč	k1gMnPc3
skrze	skrze	k?
menu	menu	k1gNnSc4
PS4	PS4	k1gFnSc2
s	s	k7c7
možností	možnost	k1gFnSc7
komunikace	komunikace	k1gFnSc2
s	s	k7c7
hráčem	hráč	k1gMnSc7
skrze	skrze	k?
kameru	kamera	k1gFnSc4
nebo	nebo	k8xC
mikrofon	mikrofon	k1gInSc4
pro	pro	k7c4
sledování	sledování	k1gNnSc4
nebo	nebo	k8xC
i	i	k9
výpomoc	výpomoc	k1gFnSc4
pro	pro	k7c4
těžké	těžký	k2eAgFnPc4d1
překážky	překážka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hráči	hráč	k1gMnPc7
také	také	k6eAd1
mohou	moct	k5eAaImIp3nP
vysílat	vysílat	k5eAaImF
živé	živý	k2eAgNnSc4d1
video	video	k1gNnSc4
z	z	k7c2
jejich	jejich	k3xOp3gFnPc2
hraní	hraň	k1gFnPc2
přes	přes	k7c4
služby	služba	k1gFnPc4
Twitch	Twitcha	k1gFnPc2
a	a	k8xC
YouTube	YouTub	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přátelé	přítel	k1gMnPc1
mohou	moct	k5eAaImIp3nP
tato	tento	k3xDgNnPc4
videa	video	k1gNnPc4
komentovat	komentovat	k5eAaBmF
a	a	k8xC
hodnotit	hodnotit	k5eAaImF
z	z	k7c2
jiných	jiný	k2eAgInPc2d1
internetových	internetový	k2eAgInPc2d1
prohlížečů	prohlížeč	k1gInPc2
a	a	k8xC
zařízení	zařízení	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Hry	hra	k1gFnPc1
</s>
<s>
Hry	hra	k1gFnPc1
pro	pro	k7c4
PlayStation	PlayStation	k1gInSc4
4	#num#	k4
nejsou	být	k5eNaImIp3nP
regionálně	regionálně	k6eAd1
omezeny	omezit	k5eAaPmNgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
v	v	k7c6
praxi	praxe	k1gFnSc6
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
pokud	pokud	k8xS
se	se	k3xPyFc4
hra	hra	k1gFnSc1
koupí	koupit	k5eAaPmIp3nS
např.	např.	kA
v	v	k7c6
USA	USA	kA
<g/>
,	,	kIx,
hráč	hráč	k1gMnSc1
z	z	k7c2
Německa	Německo	k1gNnSc2
ji	on	k3xPp3gFnSc4
bude	být	k5eAaImBp3nS
moci	moct	k5eAaImF
bez	bez	k7c2
problémů	problém	k1gInPc2
provozovat	provozovat	k5eAaImF
na	na	k7c4
své	svůj	k3xOyFgNnSc4
konzoli	konzoli	k6eAd1
koupené	koupený	k2eAgNnSc1d1
v	v	k7c6
Německu	Německo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sony	Sony	kA
také	také	k9
věří	věřit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
PS4	PS4	k1gFnSc1
umožní	umožnit	k5eAaPmIp3nS
nezávislým	závislý	k2eNgInSc7d1
a	a	k8xC
také	také	k9
i	i	k9
profesionálním	profesionální	k2eAgMnPc3d1
herním	herní	k2eAgMnPc3d1
vývojářům	vývojář	k1gMnPc3
lehčí	lehký	k2eAgInSc4d2
vývoj	vývoj	k1gInSc4
pro	pro	k7c4
tuto	tento	k3xDgFnSc4
konzoli	konzole	k1gFnSc4
díky	díky	k7c3
použitým	použitý	k2eAgFnPc3d1
součástkám	součástka	k1gFnPc3
podobným	podobný	k2eAgFnPc3d1
počítačovým	počítačový	k2eAgMnPc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sony	Sony	kA
také	také	k9
dovoluje	dovolovat	k5eAaImIp3nS
nezávislým	závislý	k2eNgMnPc3d1
vývojářům	vývojář	k1gMnPc3
vydávat	vydávat	k5eAaPmF,k5eAaImF
hry	hra	k1gFnPc4
pod	pod	k7c7
vlastním	vlastní	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
na	na	k7c6
síti	síť	k1gFnSc6
PSN	PSN	kA
pro	pro	k7c4
PS	PS	kA
<g/>
4	#num#	k4
<g/>
,	,	kIx,
PS3	PS3	k1gFnSc1
a	a	k8xC
PS	PS	kA
Vitu	vit	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společnost	společnost	k1gFnSc1
oznámila	oznámit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
nejméně	málo	k6eAd3
deset	deset	k4xCc4
her	hra	k1gFnPc2
od	od	k7c2
nezávislých	závislý	k2eNgMnPc2d1
vývojářů	vývojář	k1gMnPc2
bude	být	k5eAaImBp3nS
mít	mít	k5eAaImF
svůj	svůj	k3xOyFgInSc4
debut	debut	k1gInSc4
na	na	k7c6
konci	konec	k1gInSc6
roku	rok	k1gInSc2
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgInSc1
herní	herní	k2eAgInSc1d1
titul	titul	k1gInSc1
pro	pro	k7c4
PS4	PS4	k1gFnSc4
si	se	k3xPyFc3
bude	být	k5eAaImBp3nS
moci	moct	k5eAaImF
uživatel	uživatel	k1gMnSc1
koupit	koupit	k5eAaPmF
buďto	buďto	k8xC
fyzicky	fyzicky	k6eAd1
nebo	nebo	k8xC
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnSc2
hry	hra	k1gFnSc2
budou	být	k5eAaImBp3nP
mít	mít	k5eAaImF
svou	svůj	k3xOyFgFnSc4
demoverzi	demoverze	k1gFnSc4
<g/>
,	,	kIx,
což	což	k3yRnSc4,k3yQnSc4
umožní	umožnit	k5eAaPmIp3nP
hráči	hráč	k1gMnPc1
si	se	k3xPyFc3
hru	hra	k1gFnSc4
před	před	k7c7
koupí	koupě	k1gFnSc7
nejdříve	dříve	k6eAd3
vyzkoušet	vyzkoušet	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sony	Sony	kA
navíc	navíc	k6eAd1
nevyžaduje	vyžadovat	k5eNaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
konzole	konzola	k1gFnSc3
připojovala	připojovat	k5eAaImAgFnS
online	onlinout	k5eAaPmIp3nS
k	k	k7c3
ověření	ověření	k1gNnSc3
její	její	k3xOp3gFnSc2
legitimity	legitimita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
PlayStation	PlayStation	k1gInSc1
4	#num#	k4
také	také	k9
obsahuje	obsahovat	k5eAaImIp3nS
vlastnost	vlastnost	k1gFnSc4
PlayGo	PlayGo	k6eAd1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
umožňuje	umožňovat	k5eAaImIp3nS
hrát	hrát	k5eAaImF
hru	hra	k1gFnSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
ještě	ještě	k9
není	být	k5eNaImIp3nS
dokončena	dokončit	k5eAaPmNgFnS
její	její	k3xOp3gFnSc1
instalace	instalace	k1gFnSc1
/	/	kIx~
stahování	stahování	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Některé	některý	k3yIgFnPc1
hry	hra	k1gFnPc1
jsou	být	k5eAaImIp3nP
exkluzivně	exkluzivně	k6eAd1
vyvinuty	vyvinout	k5eAaPmNgInP
jen	jen	k9
pro	pro	k7c4
PS	PS	kA
<g/>
4	#num#	k4
<g/>
,	,	kIx,
mezi	mezi	k7c4
nejprodávanější	prodávaný	k2eAgNnSc4d3
patří	patřit	k5eAaImIp3nS
<g/>
:	:	kIx,
Uncharted	Uncharted	k1gMnSc1
<g/>
,	,	kIx,
The	The	k1gMnSc1
Last	Last	k1gMnSc1
of	of	k?
Us	Us	k1gMnSc1
<g/>
,	,	kIx,
Spider-Man	Spider-Man	k1gMnSc1
<g/>
,	,	kIx,
Horizon	Horizon	k1gMnSc1
<g/>
:	:	kIx,
Zero	Zero	k1gMnSc1
Dawn	Dawn	k1gMnSc1
<g/>
,	,	kIx,
God	God	k1gMnSc1
of	of	k?
War	War	k1gMnSc1
<g/>
,	,	kIx,
Gran	Gran	k1gMnSc1
Turismo	Turisma	k1gFnSc5
<g/>
,	,	kIx,
Bloodborne	Bloodborn	k1gInSc5
<g/>
,	,	kIx,
Persona	persona	k1gFnSc1
5	#num#	k4
<g/>
,	,	kIx,
Detroit	Detroit	k1gInSc1
<g/>
:	:	kIx,
Become	Becom	k1gInSc5
Human	Human	k1gMnSc1
<g/>
,	,	kIx,
Last	Last	k1gMnSc1
Guardian	Guardian	k1gMnSc1
<g/>
,	,	kIx,
Yakuza	Yakuza	k1gFnSc1
6	#num#	k4
<g/>
,	,	kIx,
Shadow	Shadow	k1gFnSc1
of	of	k?
the	the	k?
Colossus	Colossus	k1gMnSc1
a	a	k8xC
WipEout	WipEout	k1gMnSc1
Omega	omega	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zpětná	zpětný	k2eAgFnSc1d1
kompatibilita	kompatibilita	k1gFnSc1
</s>
<s>
Konzole	konzola	k1gFnSc3
podporuje	podporovat	k5eAaImIp3nS
některé	některý	k3yIgFnPc4
hry	hra	k1gFnPc4
vydané	vydaný	k2eAgFnPc4d1
pro	pro	k7c4
svého	svůj	k3xOyFgMnSc4
předchůdce	předchůdce	k1gMnSc4
PlayStation	PlayStation	k1gInSc4
2	#num#	k4
prostřednictvím	prostřednictvím	k7c2
emulátoru	emulátor	k1gInSc2
(	(	kIx(
<g/>
tedy	tedy	k9
i	i	k9
offline	offlin	k1gInSc5
<g/>
,	,	kIx,
bez	bez	k7c2
Gaikai	Gaika	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Sony	Sony	kA
také	také	k9
plánuje	plánovat	k5eAaImIp3nS
streamování	streamování	k1gNnSc4
her	hra	k1gFnPc2
z	z	k7c2
jiných	jiný	k1gMnPc2
PlayStation	PlayStation	k1gInSc4
platforem	platforma	k1gFnPc2
prostřednictvím	prostřednictvím	k7c2
cloudu	cloud	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Emulace	emulace	k1gFnSc1
bude	být	k5eAaImBp3nS
fungovat	fungovat	k5eAaImF
prostřednictvím	prostřednictvím	k7c2
služby	služba	k1gFnSc2
Gaikai	Gaika	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
Sony	Sony	kA
koupila	koupit	k5eAaPmAgFnS
v	v	k7c6
červenci	červenec	k1gInSc6
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Služba	služba	k1gFnSc1
tedy	tedy	k9
bude	být	k5eAaImBp3nS
streamovat	streamovat	k5eAaImF,k5eAaBmF,k5eAaPmF
a	a	k8xC
renderovat	renderovat	k5eAaPmF,k5eAaBmF,k5eAaImF
předchozí	předchozí	k2eAgInSc4d1
PlayStation	PlayStation	k1gInSc4
hry	hra	k1gFnSc2
do	do	k7c2
PS4	PS4	k1gFnSc2
a	a	k8xC
PS	PS	kA
Vita	vít	k5eAaImNgFnS
přes	přes	k7c4
Internet	Internet	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrát	hrát	k5eAaImF
jdou	jít	k5eAaImIp3nP
jen	jen	k9
některé	některý	k3yIgFnPc4
hry	hra	k1gFnPc4
<g/>
,	,	kIx,
nyní	nyní	k6eAd1
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc4
54	#num#	k4
her	hra	k1gFnPc2
z	z	k7c2
PS	PS	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Prodeje	prodej	k1gInPc1
</s>
<s>
Společnost	společnost	k1gFnSc1
Sony	Sony	kA
v	v	k7c6
srpnu	srpen	k1gInSc6
2013	#num#	k4
oznámila	oznámit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
předobjednávky	předobjednávka	k1gFnPc1
PS4	PS4	k1gFnSc7
pokořily	pokořit	k5eAaPmAgFnP
hranici	hranice	k1gFnSc6
jednoho	jeden	k4xCgInSc2
milionu	milion	k4xCgInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
startovním	startovní	k2eAgInSc6d1
dni	den	k1gInSc6
prodeje	prodej	k1gInSc2
konzole	konzola	k1gFnSc3
se	se	k3xPyFc4
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
prodalo	prodat	k5eAaPmAgNnS
přes	přes	k7c4
jeden	jeden	k4xCgInSc4
milion	milion	k4xCgInSc4
kusů	kus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
startovního	startovní	k2eAgInSc2d1
dne	den	k1gInSc2
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
Británii	Británie	k1gFnSc6
se	se	k3xPyFc4
PlayStation	PlayStation	k1gInSc1
4	#num#	k4
stal	stát	k5eAaPmAgInS
nejlépe	dobře	k6eAd3
prodávanou	prodávaný	k2eAgFnSc7d1
konzolí	konzole	k1gFnSc7
za	za	k7c4
48	#num#	k4
hodin	hodina	k1gFnPc2
s	s	k7c7
celkovým	celkový	k2eAgInSc7d1
počtem	počet	k1gInSc7
250	#num#	k4
000	#num#	k4
kusů	kus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
28	#num#	k4
<g/>
.	.	kIx.
prosinci	prosinec	k1gInSc3
2013	#num#	k4
bylo	být	k5eAaImAgNnS
celkem	celkem	k6eAd1
prodáno	prodat	k5eAaPmNgNnS
4	#num#	k4
200	#num#	k4
000	#num#	k4
kusů	kus	k1gInPc2
konzole	konzola	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
2	#num#	k4
<g/>
.	.	kIx.
březnu	březen	k1gInSc6
bylo	být	k5eAaImAgNnS
prodáno	prodat	k5eAaPmNgNnS
celkem	celkem	k6eAd1
6	#num#	k4
000	#num#	k4
000	#num#	k4
kusů	kus	k1gInPc2
<g/>
,	,	kIx,
původně	původně	k6eAd1
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
prodáno	prodat	k5eAaPmNgNnS
pouze	pouze	k6eAd1
5	#num#	k4
000	#num#	k4
000	#num#	k4
kusů	kus	k1gInPc2
do	do	k7c2
konce	konec	k1gInSc2
března	březen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
roku	rok	k1gInSc2
2014	#num#	k4
bylo	být	k5eAaImAgNnS
oznámeno	oznámit	k5eAaPmNgNnS
tedy	tedy	k9
v	v	k7c6
listopadu	listopad	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
konzole	konzola	k1gFnSc3
PS4	PS4	k1gFnSc3
je	být	k5eAaImIp3nS
nejprodávanější	prodávaný	k2eAgNnSc1d3
za	za	k7c4
9	#num#	k4
měsíců	měsíc	k1gInPc2
v	v	k7c6
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
průběhu	průběh	k1gInSc6
výstavy	výstava	k1gFnSc2
CES	ces	k1gNnSc1
2016	#num#	k4
SONY	Sony	kA
oznámilo	oznámit	k5eAaPmAgNnS
prodej	prodej	k1gInSc4
35,9	35,9	k4
miliónů	milión	k4xCgInPc2
konzolí	konzole	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
společnosti	společnost	k1gFnSc2
Sony	Sony	kA
se	s	k7c7
22	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2016	#num#	k4
konzole	konzola	k1gFnSc3
Playstation	Playstation	k1gInSc1
4	#num#	k4
prodalo	prodat	k5eAaPmAgNnS
40	#num#	k4
miliónů	milión	k4xCgInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
16	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2018	#num#	k4
bylo	být	k5eAaImAgNnS
celkem	celkem	k6eAd1
prodáno	prodat	k5eAaPmNgNnS
přes	přes	k7c4
80	#num#	k4
milionů	milion	k4xCgInPc2
kusů	kus	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
K	k	k7c3
říjnu	říjen	k1gInSc3
2019	#num#	k4
se	se	k3xPyFc4
prodalo	prodat	k5eAaPmAgNnS
přes	přes	k7c4
102,8	102,8	k4
milionů	milion	k4xCgInPc2
PS	PS	kA
<g/>
4	#num#	k4
<g/>
,	,	kIx,
jde	jít	k5eAaImIp3nS
tak	tak	k9
o	o	k7c4
druhou	druhý	k4xOgFnSc4
nejprodávanější	prodávaný	k2eAgFnSc4d3
konzoli	konzole	k1gFnSc4
v	v	k7c6
historii	historie	k1gFnSc6
po	po	k7c4
PS	PS	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Novější	nový	k2eAgFnSc1d2
verze	verze	k1gFnSc1
</s>
<s>
PS4	PS4	k4
Pro	pro	k7c4
a	a	k8xC
PS4	PS4	k1gMnSc1
Slim	Slim	k1gMnSc1
</s>
<s>
PlayStation	PlayStation	k1gInSc1
4	#num#	k4
Slim	Slima	k1gFnPc2
</s>
<s>
Zmenšený	zmenšený	k2eAgInSc1d1
PlayStation	PlayStation	k1gInSc1
4	#num#	k4
Slim	Slima	k1gFnPc2
(	(	kIx(
<g/>
typ	typ	k1gInSc1
CUH-	CUH-	k1gFnSc2
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
vyšel	vyjít	k5eAaPmAgInS
15	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2016	#num#	k4
jako	jako	k8xC,k8xS
nástupnická	nástupnický	k2eAgFnSc1d1
verze	verze	k1gFnSc1
původního	původní	k2eAgMnSc2d1
PS	PS	kA
<g/>
4	#num#	k4
<g/>
.	.	kIx.
500	#num#	k4
GB	GB	kA
verze	verze	k1gFnSc1
byla	být	k5eAaImAgFnS
prodávána	prodávat	k5eAaImNgFnS
za	za	k7c4
stejnou	stejný	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
jako	jako	k8xS,k8xC
původní	původní	k2eAgFnSc4d1
PS4	PS4	k1gFnSc4
verze	verze	k1gFnSc2
<g/>
,	,	kIx,
k	k	k7c3
dispozici	dispozice	k1gFnSc3
byla	být	k5eAaImAgFnS
také	také	k6eAd1
verze	verze	k1gFnSc1
s	s	k7c7
1	#num#	k4
TB	TB	kA
paměti	paměť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
dubna	duben	k1gInSc2
2017	#num#	k4
se	se	k3xPyFc4
vyrábí	vyrábět	k5eAaImIp3nS
jen	jen	k9
verze	verze	k1gFnSc1
s	s	k7c7
1	#num#	k4
TB	TB	kA
úložného	úložný	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
40	#num#	k4
%	%	kIx~
menší	malý	k2eAgFnSc4d2
verzi	verze	k1gFnSc4
než	než	k8xS
původní	původní	k2eAgInSc4d1
model	model	k1gInSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
mírně	mírně	k6eAd1
zaoblenější	zaoblený	k2eAgMnSc1d2
a	a	k8xC
povrch	povrch	k1gInSc1
vršku	vršek	k1gInSc2
má	mít	k5eAaImIp3nS
místo	místo	k1gNnSc4
dvoutónového	dvoutónový	k2eAgNnSc2d1
provedení	provedení	k1gNnSc2
celý	celý	k2eAgInSc1d1
matný	matný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dva	dva	k4xCgInPc1
USB	USB	kA
porty	porta	k1gFnSc2
na	na	k7c6
přední	přední	k2eAgFnSc6d1
straně	strana	k1gFnSc6
byly	být	k5eAaImAgFnP
aktualizovány	aktualizovat	k5eAaBmNgFnP
na	na	k7c4
novější	nový	k2eAgInSc4d2
standard	standard	k1gInSc4
USB	USB	kA
3.1	3.1	k4
a	a	k8xC
mají	mít	k5eAaImIp3nP
mezi	mezi	k7c7
sebou	se	k3xPyFc7
větší	veliký	k2eAgFnSc7d2
rozestup	rozestup	k1gInSc4
<g/>
,	,	kIx,
optický	optický	k2eAgInSc4d1
audio	audio	k2eAgInSc4d1
port	port	k1gInSc4
byl	být	k5eAaImAgInS
ovšem	ovšem	k9
odstraněn	odstranit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS4	PS4	k1gMnSc1
Slim	Slim	k1gMnSc1
má	mít	k5eAaImIp3nS
rychlejší	rychlý	k2eAgFnSc4d2
podporu	podpora	k1gFnSc4
Wi-Fi	Wi-F	k1gFnSc2
5	#num#	k4
<g/>
GHz	GHz	k1gFnPc2
a	a	k8xC
bluetooth	bluetootha	k1gFnPc2
4.0	4.0	k4
<g/>
,	,	kIx,
oproti	oproti	k7c3
původní	původní	k2eAgNnPc4d1
PS	PS	kA
<g/>
4	#num#	k4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
podporuje	podporovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
2,4	2,4	k4
GHz	GHz	k1gFnPc2
Wi-Fi	Wi-Fe	k1gFnSc4
a	a	k8xC
zastaralé	zastaralý	k2eAgNnSc4d1
připojení	připojení	k1gNnSc4
bluetooth	bluetooth	k1gInSc1
2,1	2,1	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
PlayStation	PlayStation	k1gInSc1
4	#num#	k4
Pro	pro	k7c4
</s>
<s>
Modernizovaná	modernizovaný	k2eAgFnSc1d1
verze	verze	k1gFnSc1
PlayStation	PlayStation	k1gInSc1
4	#num#	k4
Pro	pro	k7c4
(	(	kIx(
<g/>
typ	typ	k1gInSc4
CUH-	CUH-	k1gFnSc4
<g/>
7000	#num#	k4
<g/>
)	)	kIx)
vyšla	vyjít	k5eAaPmAgFnS
10	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c6
verzi	verze	k1gFnSc6
se	s	k7c7
zdokonaleným	zdokonalený	k2eAgInSc7d1
hardwarem	hardware	k1gInSc7
umožňující	umožňující	k2eAgNnSc4d1
rozlišení	rozlišení	k1gNnSc4
4K	4K	k4
a	a	k8xC
s	s	k7c7
vylepšenou	vylepšený	k2eAgFnSc7d1
výkonností	výkonnost	k1gFnSc7
PS-VR	PS-VR	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Disponuje	disponovat	k5eAaBmIp3nS
výkonnějším	výkonný	k2eAgNnSc7d2
GPU	GPU	kA
zvládající	zvládající	k2eAgMnSc1d1
až	až	k8xS
4,2	4,2	k4
teraflops	teraflopsa	k1gFnPc2
a	a	k8xC
rychlejším	rychlý	k2eAgMnSc7d2
CPU	CPU	kA
v	v	k7c6
taktu	takt	k1gInSc6
2.13	2.13	k4
GHz	GHz	k1gFnPc2
<g/>
,	,	kIx,
oproti	oproti	k7c3
původnímu	původní	k2eAgMnSc3d1
PS4	PS4	k1gMnSc3
s	s	k7c7
grafikou	grafika	k1gFnSc7
1,8	1,8	k4
teraflops	teraflopsa	k1gFnPc2
a	a	k8xC
CPU	CPU	kA
1.6	1.6	k4
GHz	GHz	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
vydání	vydání	k1gNnSc2
šlo	jít	k5eAaImAgNnS
o	o	k7c4
nejvýkonnější	výkonný	k2eAgFnSc4d3
herní	herní	k2eAgFnSc4d1
konzoli	konzole	k1gFnSc4
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
7	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2017	#num#	k4
však	však	k9
byla	být	k5eAaImAgFnS
překonaná	překonaný	k2eAgFnSc1d1
novou	nova	k1gFnSc7
konzolí	konzolit	k5eAaPmIp3nS,k5eAaImIp3nS
Xbox	Xbox	k1gInSc1
One	One	k1gFnSc2
X.	X.	kA
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
Jelikož	jelikož	k8xS
si	se	k3xPyFc3
uživatelé	uživatel	k1gMnPc1
stěžovali	stěžovat	k5eAaImAgMnP
na	na	k7c4
příliš	příliš	k6eAd1
hlučný	hlučný	k2eAgInSc4d1
ventilátor	ventilátor	k1gInSc4
PS4	PS4	k1gFnPc2
Pro	pro	k7c4
<g/>
,	,	kIx,
v	v	k7c6
listopadu	listopad	k1gInSc6
2018	#num#	k4
vyšel	vyjít	k5eAaPmAgInS
novější	nový	k2eAgInSc1d2
typ	typ	k1gInSc1
CUH	CUH	kA
7216	#num#	k4
<g/>
B	B	kA
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
již	již	k6eAd1
disponuje	disponovat	k5eAaBmIp3nS
výrazně	výrazně	k6eAd1
tišším	tichý	k2eAgNnSc7d2
chlazením	chlazení	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Speciální	speciální	k2eAgFnSc1d1
edice	edice	k1gFnSc1
</s>
<s>
V	v	k7c6
srpnu	srpen	k1gInSc6
2018	#num#	k4
<g/>
,	,	kIx,
k	k	k7c3
oslavě	oslava	k1gFnSc3
500	#num#	k4
milionů	milion	k4xCgInPc2
prodaných	prodaný	k2eAgFnPc2d1
konzolí	konzole	k1gFnPc2
PlayStation	PlayStation	k1gInSc1
<g/>
,	,	kIx,
připravila	připravit	k5eAaPmAgFnS
Sony	Sony	kA
50	#num#	k4
000	#num#	k4
kusů	kus	k1gInPc2
konzolí	konzole	k1gFnPc2
PS4	PS4	k1gFnSc7
Pro	pro	k7c4
Limited	limited	k2eAgInSc4d1
Edition	Edition	k1gInSc4
„	„	k?
<g/>
500	#num#	k4
Million	Million	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc7d1
změnou	změna	k1gFnSc7
této	tento	k3xDgFnSc2
limitované	limitovaný	k2eAgFnSc2d1
edice	edice	k1gFnSc2
je	být	k5eAaImIp3nS
větší	veliký	k2eAgInSc4d2
úložný	úložný	k2eAgInSc4d1
prostor	prostor	k1gInSc4
2	#num#	k4
TB	TB	kA
<g/>
,	,	kIx,
k	k	k7c3
dalším	další	k2eAgMnPc3d1
patří	patřit	k5eAaImIp3nS
průsvitně	průsvitně	k6eAd1
tmavě	tmavě	k6eAd1
modrý	modrý	k2eAgInSc1d1
odstín	odstín	k1gInSc1
konzole	konzola	k1gFnSc3
i	i	k9
ovladačů	ovladač	k1gInPc2
a	a	k8xC
pamětní	pamětní	k2eAgFnSc1d1
měděná	měděný	k2eAgFnSc1d1
destička	destička	k1gFnSc1
na	na	k7c6
přední	přední	k2eAgFnSc6d1
straně	strana	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
balení	balení	k1gNnSc6
je	být	k5eAaImIp3nS
mimo	mimo	k7c4
ovladače	ovladač	k1gInPc4
a	a	k8xC
sluchátek	sluchátko	k1gNnPc2
rovněž	rovněž	k9
PlayStation	PlayStation	k1gInSc1
Camera	Camero	k1gNnSc2
a	a	k8xC
vertikální	vertikální	k2eAgInSc4d1
podstavec	podstavec	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Sony	Sony	kA
prodala	prodat	k5eAaPmAgFnS
102.8	102.8	k4
milionů	milion	k4xCgInPc2
konzolí	konzole	k1gFnPc2
PlayStation	PlayStation	k1gInSc1
4	#num#	k4
<g/>
,	,	kIx,
jde	jít	k5eAaImIp3nS
o	o	k7c4
druhou	druhý	k4xOgFnSc4
nejprodávanější	prodávaný	k2eAgFnSc4d3
domácí	domácí	k2eAgFnSc4d1
konzoli	konzole	k1gFnSc4
<g/>
.	.	kIx.
konzolista	konzolista	k1gMnSc1
<g/>
.	.	kIx.
<g/>
tiscali	tiscat	k5eAaPmAgMnP,k5eAaImAgMnP
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
AMD	AMD	kA
PlayStation	PlayStation	k1gInSc4
4	#num#	k4
GPU	GPU	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
TechPowerUp	TechPowerUp	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
AMD	AMD	kA
Playstation	Playstation	k1gInSc4
4	#num#	k4
Pro	pro	k7c4
GPU	GPU	kA
Specs	Specs	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
TechPowerUp	TechPowerUp	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
PlayStation	PlayStation	k1gInSc1
4	#num#	k4
Teardown	Teardowna	k1gFnPc2
-	-	kIx~
iFixit	iFixit	k1gInSc1
<g/>
.	.	kIx.
iFixit	iFixit	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Sony	Sony	kA
PlayStation	PlayStation	k1gInSc4
4	#num#	k4
Torn	Torn	k1gMnSc1
Down	Down	k1gMnSc1
<g/>
,	,	kIx,
Reveals	Reveals	k1gInSc1
Secondary	Secondara	k1gFnSc2
ARM	ARM	kA
Processor	Processor	k1gInSc4
And	Anda	k1gFnPc2
2	#num#	k4
Gb	Gb	k1gFnPc2
RAM	RAM	kA
Chip	Chip	k1gMnSc1
<g/>
.	.	kIx.
www.nextpowerup.com	www.nextpowerup.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
STUART	STUART	kA
<g/>
,	,	kIx,
Keith	Keith	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
PlayStation	PlayStation	k1gInSc1
2	#num#	k4
games	gamesa	k1gFnPc2
are	ar	k1gInSc5
coming	coming	k1gInSc1
to	ten	k3xDgNnSc1
PS	PS	kA
<g/>
4	#num#	k4
<g/>
,	,	kIx,
here	here	k6eAd1
are	ar	k1gInSc5
12	#num#	k4
titles	titles	k1gMnSc1
we	we	k?
want	want	k1gMnSc1
to	ten	k3xDgNnSc1
play	play	k0
again	againa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Http://www.theguardian.com	Http://www.theguardian.com	k1gInSc1
<g/>
.	.	kIx.
www.TheGuardian.com	www.TheGuardian.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-11-24	2015-11-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
MUNCY	MUNCY	kA
<g/>
,	,	kIx,
Jake	Jake	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
PlayStation	PlayStation	k1gInSc1
2	#num#	k4
Emulation	Emulation	k1gInSc1
Coming	Coming	k1gInSc4
to	ten	k3xDgNnSc1
PS	PS	kA
<g/>
4	#num#	k4
<g/>
,	,	kIx,
Sony	Sony	kA
Confirms	Confirms	k1gInSc1
<g/>
.	.	kIx.
http://www.wired.com/	http://www.wired.com/	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-11-19	2015-11-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
HOMOLA	Homola	k1gMnSc1
<g/>
,	,	kIx,
Adam	Adam	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sony	Sony	kA
potvrdila	potvrdit	k5eAaPmAgFnS
emulaci	emulace	k1gFnSc4
PlayStation	PlayStation	k1gInSc1
2	#num#	k4
her	hra	k1gFnPc2
na	na	k7c4
PS	PS	kA
<g/>
4	#num#	k4
<g/>
,	,	kIx,
první	první	k4xOgInPc4
tři	tři	k4xCgInPc4
už	už	k9
potichu	potichu	k6eAd1
fungují	fungovat	k5eAaImIp3nP
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
http://games.tiscali.cz/,	http://games.tiscali.cz/,	k?
2015-11-20	2015-11-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
IONESCU	Ionesco	k1gMnSc3
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
AMD	AMD	kA
plans	plans	k6eAd1
to	ten	k3xDgNnSc4
sell	sell	k1gMnSc1
an	an	k?
APU	APU	kA
based	based	k1gInSc4
on	on	k3xPp3gMnSc1
modified	modified	k1gMnSc1
PlayStation	PlayStation	k1gInSc4
4	#num#	k4
hardware	hardware	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Http://www.pcworld.com/.	Http://www.pcworld.com/.	k1gFnPc2
PC	PC	kA
World	World	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-04-01	2013-04-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglický	anglický	k2eAgInSc1d1
<g/>
)	)	kIx)
↑	↑	k?
TISCALI	TISCALI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Test	test	k1gInSc4
konzole	konzola	k1gFnSc3
PlayStation	PlayStation	k1gInSc1
4	#num#	k4
Pro	pro	k7c4
-	-	kIx~
Games	Games	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Games	Games	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
PS4	PS4	k1gFnPc2
'	'	kIx"
<g/>
Orbis	orbis	k1gInSc1
<g/>
'	'	kIx"
leak	leak	k1gInSc1
claims	claims	k6eAd1
8	#num#	k4
<g/>
-core	-cor	k1gInSc5
AMD	AMD	kA
CPU	CPU	kA
integrated	integrated	k1gMnSc1
with	with	k1gMnSc1
GPU	GPU	kA
<g/>
.	.	kIx.
http://www.examiner.com/	http://www.examiner.com/	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Playstation	Playstation	k1gInSc1
4	#num#	k4
Audio	audio	k2eAgInSc2d1
DSP	DSP	kA
Based	Based	k1gMnSc1
On	on	k3xPp3gMnSc1
AMD	AMD	kA
<g/>
’	’	k?
<g/>
s	s	k7c7
PC	PC	kA
TrueAudio	TrueAudio	k6eAd1
Technology	technolog	k1gMnPc4
<g/>
.	.	kIx.
http://www.redgamingtech.com/	http://www.redgamingtech.com/	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
New	New	k1gMnSc1
Official	Official	k1gMnSc1
PS4	PS4	k1gMnSc1
Peripherals	Peripheralsa	k1gFnPc2
Announced	Announced	k1gMnSc1
<g/>
,	,	kIx,
Includes	Includes	k1gMnSc1
New	New	k1gFnSc2
DualShock	DualShock	k1gInSc1
4	#num#	k4
<g/>
,	,	kIx,
PS	PS	kA
Camera	Camera	k1gFnSc1
and	and	k?
Headset	Headset	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
PlayStation	PlayStation	k1gInSc1
LifeStyle	LifeStyl	k1gInSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Specialty	Specialt	k1gInPc1
Controllers	Controllers	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
PlayStation	PlayStation	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ANTHONY	ANTHONY	kA
<g/>
,	,	kIx,
Sebastian	Sebastian	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS4	PS4	k1gFnPc2
runs	runs	k6eAd1
Orbis	orbis	k1gInSc1
OS	OS	kA
<g/>
,	,	kIx,
a	a	k8xC
modified	modified	k1gInSc1
version	version	k1gInSc1
of	of	k?
FreeBSD	FreeBSD	k1gFnSc2
that	that	k1gMnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
similar	similar	k1gMnSc1
to	ten	k3xDgNnSc4
Linux	linux	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Http://www.extremetech.com/.	Http://www.extremetech.com/.	k1gFnSc1
Extreme	Extrem	k1gInSc5
Tech	Tech	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-06-24	2013-06-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglický	anglický	k2eAgInSc1d1
<g/>
)	)	kIx)
↑	↑	k?
Playing	Playing	k1gInSc1
videos	videos	k1gMnSc1
on	on	k3xPp3gMnSc1
discs	discs	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
PlayStation	PlayStation	k1gInSc1
<g/>
®	®	k?
<g/>
4	#num#	k4
User	usrat	k5eAaPmRp2nS
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Guide	Guid	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
↑	↑	k?
PlayStation	PlayStation	k1gInSc1
4	#num#	k4
-	-	kIx~
VGChartz	VGChartza	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
VGChartz	VGChartz	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
PS2	PS2	k1gMnSc1
Games	Games	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
PlayStation	PlayStation	k1gInSc1
<g/>
™	™	k?
<g/>
Store	Stor	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SCHÖN	SCHÖN	kA
<g/>
,	,	kIx,
Otakar	Otakar	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samsung	Samsung	kA
na	na	k7c6
CES	ces	k1gNnSc6
2016	#num#	k4
spojil	spojit	k5eAaPmAgMnS
tablet	tablet	k1gInSc4
a	a	k8xC
ledničku	lednička	k1gFnSc4
s	s	k7c7
televizí	televize	k1gFnSc7
do	do	k7c2
Family	Famila	k1gFnSc2
Hubu	huba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářské	hospodářský	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-01-06	2016-01-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Prodeje	prodej	k1gInPc1
PlayStation	PlayStation	k1gInSc1
4	#num#	k4
překonaly	překonat	k5eAaPmAgInP
40	#num#	k4
milionů	milion	k4xCgInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Technet	Technet	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-05-27	2016-05-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
PS4	PS4	k1gMnSc1
Sales	Sales	k1gMnSc1
Top	topit	k5eAaImRp2nS
an	an	k?
Estimated	Estimated	k1gMnSc1
80	#num#	k4
Million	Million	k1gInSc1
Units	Units	k1gInSc4
Worldwide	Worldwid	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
VGChartz	VGChartz	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
What	What	k1gInSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
the	the	k?
Difference	Difference	k1gFnSc1
Between	Between	k1gInSc1
the	the	k?
PlayStation	PlayStation	k1gInSc1
4	#num#	k4
<g/>
,	,	kIx,
PlayStation	PlayStation	k1gInSc1
4	#num#	k4
Slim	Slima	k1gFnPc2
<g/>
,	,	kIx,
and	and	k?
PlayStation	PlayStation	k1gInSc1
4	#num#	k4
Pro	pro	k7c4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
How-To	How-To	k1gMnSc1
Geek	Geek	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Test	test	k1gInSc1
PlayStation	PlayStation	k1gInSc1
4	#num#	k4
Pro	pro	k7c4
<g/>
:	:	kIx,
Nejvýkonnější	výkonný	k2eAgFnSc4d3
konzoli	konzole	k1gFnSc4
současnosti	současnost	k1gFnSc2
ocení	ocenit	k5eAaPmIp3nP
nároční	náročný	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Blesk	blesk	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
TEST	test	k1gInSc1
<g/>
:	:	kIx,
Xbox	Xbox	k1gInSc1
One	One	k1gFnSc2
X	X	kA
je	být	k5eAaImIp3nS
nejvýkonnější	výkonný	k2eAgFnSc3d3
konzole	konzola	k1gFnSc3
na	na	k7c6
trhu	trh	k1gInSc6
a	a	k8xC
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc4
znát	znát	k5eAaImF
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Sony	Sony	kA
ztišilo	ztišit	k5eAaPmAgNnS
hlučné	hlučný	k2eAgNnSc1d1
chlazení	chlazení	k1gNnSc1
PlayStationu	PlayStation	k1gInSc2
4	#num#	k4
Pro	pro	k7c4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svethardware	Svethardwar	k1gMnSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
500	#num#	k4
Million	Million	k1gInSc1
Limited	limited	k2eAgInSc4d1
Edition	Edition	k1gInSc4
PS	PS	kA
<g/>
4	#num#	k4
<g/>
™	™	k?
Pro	pro	k7c4
Console	Console	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PlayStation	PlayStation	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
PlayStation	PlayStation	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Playstation	Playstation	k1gInSc1
4	#num#	k4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
HDR	HDR	kA
a	a	k8xC
Playstation	Playstation	k1gInSc1
4	#num#	k4
–	–	k?
dTest	dTest	k1gInSc1
(	(	kIx(
<g/>
recenze	recenze	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Sony	Sony	kA
PlayStation	PlayStation	k1gInSc4
4	#num#	k4
Pro	pro	k7c4
přesvědčí	přesvědčit	k5eAaPmIp3nS
i	i	k9
skeptiky	skeptik	k1gMnPc4
–	–	k?
iHned	ihned	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Nejvýkonnější	výkonný	k2eAgFnSc6d3
televizní	televizní	k2eAgFnSc6d1
konzole	konzola	k1gFnSc6
dorazila	dorazit	k5eAaPmAgFnS
do	do	k7c2
Česka	Česko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ceny	cena	k1gFnPc1
startují	startovat	k5eAaBmIp3nP
na	na	k7c4
10	#num#	k4
350	#num#	k4
Kč	Kč	kA
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Final	Final	k1gInSc1
Fantasy	fantas	k1gInPc7
na	na	k7c4
PlayStation	PlayStation	k1gInSc4
4	#num#	k4
Pro	pro	k7c4
půjde	jít	k5eAaImIp3nS
hrát	hrát	k5eAaImF
s	s	k7c7
lepší	dobrý	k2eAgFnSc7d2
a	a	k8xC
horší	zlý	k2eAgFnSc7d2
grafikou	grafika	k1gFnSc7
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Vylepšený	vylepšený	k2eAgInSc1d1
PlayStation	PlayStation	k1gInSc1
4	#num#	k4
nakopne	nakopnout	k5eAaPmIp3nS
grafiku	grafika	k1gFnSc4
a	a	k8xC
bude	být	k5eAaImBp3nS
stát	stát	k5eAaImF,k5eAaPmF
méně	málo	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
čekalo	čekat	k5eAaImAgNnS
–	–	k?
iDnes	iDnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Dvakrát	dvakrát	k6eAd1
levnější	levný	k2eAgFnSc1d2
než	než	k8xS
konkurence	konkurence	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ceny	cena	k1gFnPc1
virtuálních	virtuální	k2eAgFnPc2d1
brýlí	brýle	k1gFnPc2
PlayStation	PlayStation	k1gInSc1
VR	vr	k0
startují	startovat	k5eAaBmIp3nP
na	na	k7c4
10	#num#	k4
500	#num#	k4
Kč	Kč	kA
–	–	k?
Novinky	novinka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Inside	Insid	k1gInSc5
PlayStation	PlayStation	k1gInSc1
4	#num#	k4
Pro	pro	k7c4
<g/>
:	:	kIx,
How	How	k1gFnSc2
Sony	Sony	kA
made	mad	k1gMnSc2
the	the	k?
first	first	k1gMnSc1
4K	4K	k4
games	games	k1gInSc1
console	console	k1gFnSc2
–	–	k?
Jak	jak	k8xC,k8xS
Sony	Sony	kA
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
první	první	k4xOgInSc1
4K	4K	k4
konzoli	konzole	k1gFnSc4
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Playstation	Playstation	k1gInSc1
4	#num#	k4
Neo	Neo	k1gFnPc2
alias	alias	k9
„	„	k?
<g/>
4	#num#	k4
<g/>
k	k	k7c3
Playstation	Playstation	k1gInSc1
4	#num#	k4
<g/>
“	“	k?
nabídne	nabídnout	k5eAaPmIp3nS
výrazně	výrazně	k6eAd1
rychlejší	rychlý	k2eAgMnSc1d2
x	x	k?
<g/>
86	#num#	k4
APU	APU	kA
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Nintendo	Nintendo	k6eAd1
pracuje	pracovat	k5eAaImIp3nS
na	na	k7c6
nové	nový	k2eAgFnSc6d1
konzoli	konzole	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Starší	starý	k2eAgFnSc1d2
Wii	Wii	k1gFnSc1
U	U	kA
se	se	k3xPyFc4
ale	ale	k8xC
do	do	k7c2
důchodu	důchod	k1gInSc2
ještě	ještě	k6eAd1
nechystá	chystat	k5eNaImIp3nS
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Sony	Sony	kA
pracuje	pracovat	k5eAaImIp3nS
na	na	k7c4
„	„	k?
<g/>
Playstation	Playstation	k1gInSc4
4	#num#	k4
<g/>
K	k	k7c3
<g/>
“	“	k?
<g/>
,	,	kIx,
v	v	k7c6
prodeji	prodej	k1gInSc6
prý	prý	k9
bude	být	k5eAaImBp3nS
před	před	k7c7
Vánoci	Vánoce	k1gFnPc7
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
PlayStation	PlayStation	k1gInSc1
5	#num#	k4
určitě	určitě	k6eAd1
vznikne	vzniknout	k5eAaPmIp3nS
<g/>
,	,	kIx,
ujišťuje	ujišťovat	k5eAaImIp3nS
Sony	Sony	kA
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Report	report	k1gInSc1
<g/>
:	:	kIx,
Sony	Sony	kA
<g/>
'	'	kIx"
<g/>
s	s	k7c7
more	mor	k1gInSc5
powerful	powerful	k1gInSc1
PS4	PS4	k1gMnSc1
to	ten	k3xDgNnSc1
be	be	k?
announced	announced	k1gInSc1
before	befor	k1gInSc5
PlayStation	PlayStation	k1gInSc1
VR	vr	k0
launch	launch	k1gInSc4
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
www.playstation4.cz	www.playstation4.cz	k1gInSc1
–	–	k?
neoficiální	oficiální	k2eNgFnSc1d1,k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
pro	pro	k7c4
Česko	Česko	k1gNnSc4
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Jak	jak	k8xC,k8xS
vytvořit	vytvořit	k5eAaPmF
účet	účet	k1gInSc4
na	na	k7c4
Playstation	Playstation	k1gInSc4
Network	network	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
PlayStation	PlayStation	k1gInSc1
Domácí	domácí	k2eAgInSc1d1
konzole	konzola	k1gFnSc3
</s>
<s>
PlayStation	PlayStation	k1gInSc1
1	#num#	k4
•	•	k?
PlayStation	PlayStation	k1gInSc1
2	#num#	k4
•	•	k?
PlayStation	PlayStation	k1gInSc1
3	#num#	k4
•	•	k?
PlayStation	PlayStation	k1gInSc1
4	#num#	k4
•	•	k?
PlayStation	PlayStation	k1gInSc1
5	#num#	k4
Kapesní	kapesní	k2eAgFnSc6d1
konzole	konzola	k1gFnSc6
</s>
<s>
PlayStation	PlayStation	k1gInSc1
Portable	portable	k1gInSc1
•	•	k?
PlayStation	PlayStation	k1gInSc1
Vita	vit	k2eAgNnPc1d1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
1141271788	#num#	k4
</s>
