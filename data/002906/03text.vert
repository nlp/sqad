<s>
ZOO	zoo	k1gFnSc1	zoo
Dvůr	Dvůr	k1gInSc1	Dvůr
Králové	Králová	k1gFnSc2	Králová
a.	a.	k?	a.
s.	s.	k?	s.
je	být	k5eAaImIp3nS	být
druhá	druhý	k4xOgFnSc1	druhý
největší	veliký	k2eAgFnSc1d3	veliký
zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
Dvůr	Dvůr	k1gInSc4	Dvůr
Králové	Králová	k1gFnSc2	Králová
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
v	v	k7c6	v
Královéhradeckém	královéhradecký	k2eAgInSc6d1	královéhradecký
kraji	kraj	k1gInSc6	kraj
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Trutnov	Trutnov	k1gInSc1	Trutnov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zoo	zoo	k1gFnSc1	zoo
je	být	k5eAaImIp3nS	být
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
na	na	k7c4	na
africká	africký	k2eAgNnPc4d1	africké
zvířata	zvíře	k1gNnPc4	zvíře
-	-	kIx~	-
tuto	tento	k3xDgFnSc4	tento
specializaci	specializace	k1gFnSc4	specializace
prosadil	prosadit	k5eAaPmAgMnS	prosadit
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
ředitel	ředitel	k1gMnSc1	ředitel
Josef	Josef	k1gMnSc1	Josef
Vágner	Vágner	k1gMnSc1	Vágner
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
následně	následně	k6eAd1	následně
zorganizoval	zorganizovat	k5eAaPmAgMnS	zorganizovat
několik	několik	k4yIc4	několik
výprav	výprava	k1gFnPc2	výprava
safari	safari	k1gNnSc2	safari
do	do	k7c2	do
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
odchytil	odchytit	k5eAaPmAgInS	odchytit
množství	množství	k1gNnSc4	množství
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
kopytníků	kopytník	k1gMnPc2	kopytník
(	(	kIx(	(
<g/>
žiraf	žirafa	k1gFnPc2	žirafa
<g/>
,	,	kIx,	,
zeber	zebra	k1gFnPc2	zebra
<g/>
,	,	kIx,	,
antilop	antilopa	k1gFnPc2	antilopa
<g/>
,	,	kIx,	,
nosorožců	nosorožec	k1gMnPc2	nosorožec
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
a	a	k8xC	a
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
význam	význam	k1gInSc1	význam
zoo	zoo	k1gFnSc2	zoo
již	již	k6eAd1	již
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
přesáhl	přesáhnout	k5eAaPmAgMnS	přesáhnout
hranice	hranice	k1gFnPc4	hranice
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
jezdí	jezdit	k5eAaImIp3nP	jezdit
po	po	k7c6	po
safari	safari	k1gNnSc6	safari
upravené	upravený	k2eAgInPc4d1	upravený
turistické	turistický	k2eAgInPc4d1	turistický
autobusy	autobus	k1gInPc4	autobus
s	s	k7c7	s
návštěvníky	návštěvník	k1gMnPc7	návštěvník
přímo	přímo	k6eAd1	přímo
mezi	mezi	k7c7	mezi
zvířaty	zvíře	k1gNnPc7	zvíře
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
tuto	tento	k3xDgFnSc4	tento
trasu	trasa	k1gFnSc4	trasa
vykonat	vykonat	k5eAaPmF	vykonat
i	i	k9	i
ve	v	k7c6	v
vlastních	vlastní	k2eAgInPc6d1	vlastní
vozech	vůz	k1gInPc6	vůz
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
zoo	zoo	k1gFnSc2	zoo
je	být	k5eAaImIp3nS	být
také	také	k9	také
galerie	galerie	k1gFnSc1	galerie
obrazů	obraz	k1gInPc2	obraz
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Buriana	Burian	k1gMnSc2	Burian
<g/>
,	,	kIx,	,
slavného	slavný	k2eAgMnSc2d1	slavný
ilustrátora	ilustrátor	k1gMnSc2	ilustrátor
pravěku	pravěk	k1gInSc2	pravěk
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
čelí	čelit	k5eAaImIp3nS	čelit
zahrada	zahrada	k1gFnSc1	zahrada
kritice	kritika	k1gFnSc3	kritika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyústila	vyústit	k5eAaPmAgFnS	vyústit
ve	v	k7c6	v
vyloučení	vyloučení	k1gNnSc6	vyloučení
a	a	k8xC	a
odchod	odchod	k1gInSc1	odchod
zoo	zoo	k1gFnSc2	zoo
ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
mezinárodních	mezinárodní	k2eAgNnPc2d1	mezinárodní
sdružení	sdružení	k1gNnPc2	sdružení
zoologických	zoologický	k2eAgFnPc2d1	zoologická
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2016	[number]	k4	2016
je	být	k5eAaImIp3nS	být
zoo	zoo	k1gFnSc1	zoo
zpět	zpět	k6eAd1	zpět
ve	v	k7c6	v
sdružení	sdružení	k1gNnSc6	sdružení
evropských	evropský	k2eAgFnPc2d1	Evropská
zoologických	zoologický	k2eAgFnPc2d1	zoologická
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Ředitelem	ředitel	k1gMnSc7	ředitel
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2012	[number]	k4	2012
Přemysl	Přemysl	k1gMnSc1	Přemysl
Rabas	Rabas	k1gMnSc1	Rabas
<g/>
.	.	kIx.	.
</s>
<s>
Brány	brán	k2eAgFnPc1d1	brána
zoo	zoo	k1gFnPc1	zoo
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
poprvé	poprvé	k6eAd1	poprvé
otevřely	otevřít	k5eAaPmAgFnP	otevřít
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1946	[number]	k4	1946
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Josefa	Josef	k1gMnSc2	Josef
Fabiána	Fabián	k1gMnSc2	Fabián
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
milníkem	milník	k1gInSc7	milník
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
zoo	zoo	k1gFnSc4	zoo
stal	stát	k5eAaPmAgInS	stát
přelom	přelom	k1gInSc1	přelom
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Františka	František	k1gMnSc2	František
Císařovského	Císařovský	k1gMnSc2	Císařovský
tehdy	tehdy	k6eAd1	tehdy
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
modernizaci	modernizace	k1gFnSc3	modernizace
zoo	zoo	k1gNnSc2	zoo
a	a	k8xC	a
jejímu	její	k3xOp3gMnSc3	její
rozšíření	rozšíření	k1gNnSc1	rozšíření
z	z	k7c2	z
původní	původní	k2eAgFnSc2d1	původní
rozlohy	rozloha	k1gFnSc2	rozloha
6,5	[number]	k4	6,5
ha	ha	kA	ha
na	na	k7c4	na
28	[number]	k4	28
ha	ha	kA	ha
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
naprostá	naprostý	k2eAgFnSc1d1	naprostá
většina	většina	k1gFnSc1	většina
staveb	stavba	k1gFnPc2	stavba
a	a	k8xC	a
modernizací	modernizace	k1gFnPc2	modernizace
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Akce	akce	k1gFnSc2	akce
Z	Z	kA	Z
a	a	k8xC	a
bezplatně	bezplatně	k6eAd1	bezplatně
je	on	k3xPp3gMnPc4	on
provedli	provést	k5eAaPmAgMnP	provést
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
a	a	k8xC	a
občané	občan	k1gMnPc1	občan
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgFnPc1d1	současná
orientace	orientace	k1gFnPc1	orientace
na	na	k7c4	na
africká	africký	k2eAgNnPc4d1	africké
zvířata	zvíře	k1gNnPc4	zvíře
se	s	k7c7	s
zoo	zoo	k1gFnSc7	zoo
dostalo	dostat	k5eAaPmAgNnS	dostat
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
ředitele	ředitel	k1gMnSc2	ředitel
Josefa	Josef	k1gMnSc2	Josef
Vágnera	Vágner	k1gMnSc2	Vágner
uspořádala	uspořádat	k5eAaPmAgFnS	uspořádat
zoo	zoo	k1gFnSc1	zoo
celkem	celkem	k6eAd1	celkem
sedm	sedm	k4xCc1	sedm
expedic	expedice	k1gFnPc2	expedice
do	do	k7c2	do
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
cílem	cíl	k1gInSc7	cíl
byl	být	k5eAaImAgInS	být
dovoz	dovoz	k1gInSc4	dovoz
africké	africký	k2eAgFnSc2d1	africká
fauny	fauna	k1gFnSc2	fauna
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
těchto	tento	k3xDgFnPc2	tento
expedic	expedice	k1gFnPc2	expedice
v	v	k7c6	v
zoo	zoo	k1gFnSc6	zoo
přibylo	přibýt	k5eAaPmAgNnS	přibýt
na	na	k7c4	na
2000	[number]	k4	2000
nových	nový	k2eAgNnPc2d1	nové
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
především	především	k9	především
kopytníků	kopytník	k1gMnPc2	kopytník
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
významnou	významný	k2eAgFnSc7d1	významná
osobností	osobnost	k1gFnSc7	osobnost
zoo	zoo	k1gFnSc2	zoo
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
Jiří	Jiří	k1gMnSc1	Jiří
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
jeho	jeho	k3xOp3gNnSc7	jeho
vedením	vedení	k1gNnSc7	vedení
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
významné	významný	k2eAgFnSc3d1	významná
modernizaci	modernizace	k1gFnSc3	modernizace
zoo	zoo	k1gFnSc2	zoo
spojené	spojený	k2eAgFnSc2d1	spojená
se	se	k3xPyFc4	se
snahou	snaha	k1gFnSc7	snaha
imitovat	imitovat	k5eAaBmF	imitovat
přirozené	přirozený	k2eAgNnSc4d1	přirozené
prostředí	prostředí	k1gNnSc4	prostředí
zvířat	zvíře	k1gNnPc2	zvíře
ve	v	k7c6	v
výbězích	výběh	k1gInPc6	výběh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
došlo	dojít	k5eAaPmAgNnS	dojít
také	také	k9	také
k	k	k7c3	k
výstavbě	výstavba	k1gFnSc3	výstavba
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
umožnily	umožnit	k5eAaPmAgInP	umožnit
v	v	k7c6	v
pozdější	pozdní	k2eAgFnSc6d2	pozdější
době	doba	k1gFnSc6	doba
otevření	otevření	k1gNnSc2	otevření
safari	safari	k1gNnSc2	safari
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
práci	práce	k1gFnSc6	práce
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
Pavel	Pavel	k1gMnSc1	Pavel
Suk	Suk	k1gMnSc1	Suk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
upravil	upravit	k5eAaPmAgMnS	upravit
nedostavěné	dostavěný	k2eNgNnSc4d1	nedostavěné
safari	safari	k1gNnSc4	safari
(	(	kIx(	(
<g/>
i	i	k9	i
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
armády	armáda	k1gFnSc2	armáda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vystavěl	vystavět	k5eAaPmAgMnS	vystavět
přirozeně	přirozeně	k6eAd1	přirozeně
působící	působící	k2eAgFnPc4d1	působící
bariéry	bariéra	k1gFnPc4	bariéra
a	a	k8xC	a
unikátní	unikátní	k2eAgFnPc1d1	unikátní
samozavírací	samozavírací	k2eAgFnPc1d1	samozavírací
brány	brána	k1gFnPc1	brána
mezi	mezi	k7c4	mezi
oddíly	oddíl	k1gInPc4	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
Veřejnost	veřejnost	k1gFnSc1	veřejnost
se	se	k3xPyFc4	se
na	na	k7c4	na
safari	safari	k1gNnSc4	safari
poprvé	poprvé	k6eAd1	poprvé
podívala	podívat	k5eAaPmAgFnS	podívat
dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
pro	pro	k7c4	pro
autobusové	autobusový	k2eAgFnPc4d1	autobusová
výpravy	výprava	k1gFnPc4	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc4d1	další
výraznou	výrazný	k2eAgFnSc4d1	výrazná
modernizaci	modernizace	k1gFnSc4	modernizace
a	a	k8xC	a
přestavbu	přestavba	k1gFnSc4	přestavba
pavilonů	pavilon	k1gInPc2	pavilon
zahájil	zahájit	k5eAaPmAgMnS	zahájit
ředitel	ředitel	k1gMnSc1	ředitel
Ivan	Ivan	k1gMnSc1	Ivan
Pojar	Pojar	k1gMnSc1	Pojar
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
poklesu	pokles	k1gInSc6	pokles
provozní	provozní	k2eAgFnSc2d1	provozní
dotace	dotace	k1gFnSc2	dotace
od	od	k7c2	od
zřizovatele	zřizovatel	k1gMnSc2	zřizovatel
<g/>
,	,	kIx,	,
Okresního	okresní	k2eAgInSc2d1	okresní
úřadu	úřad	k1gInSc2	úřad
v	v	k7c6	v
Trutnově	Trutnov	k1gInSc6	Trutnov
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
sáhnout	sáhnout	k5eAaPmF	sáhnout
k	k	k7c3	k
výrazné	výrazný	k2eAgFnSc3d1	výrazná
provozní	provozní	k2eAgFnSc3d1	provozní
redukci	redukce	k1gFnSc3	redukce
(	(	kIx(	(
<g/>
pozastavení	pozastavení	k1gNnSc2	pozastavení
výstavby	výstavba	k1gFnSc2	výstavba
ptačího	ptačí	k2eAgInSc2d1	ptačí
pavilonu	pavilon	k1gInSc2	pavilon
<g/>
,	,	kIx,	,
zrušení	zrušení	k1gNnSc4	zrušení
Vědeckého	vědecký	k2eAgInSc2d1	vědecký
a	a	k8xC	a
diagnostického	diagnostický	k2eAgInSc2d1	diagnostický
ústavu	ústav	k1gInSc2	ústav
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
i	i	k9	i
v	v	k7c6	v
poklesu	pokles	k1gInSc6	pokles
počtu	počet	k1gInSc2	počet
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
z	z	k7c2	z
247	[number]	k4	247
téměř	téměř	k6eAd1	téměř
na	na	k7c4	na
polovinu	polovina	k1gFnSc4	polovina
<g/>
.	.	kIx.	.
</s>
<s>
Dlouholetá	dlouholetý	k2eAgFnSc1d1	dlouholetá
ředitelka	ředitelka	k1gFnSc1	ředitelka
Dana	Dana	k1gFnSc1	Dana
Holečková	Holečková	k1gFnSc1	Holečková
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
nástupu	nástup	k1gInSc6	nástup
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
v	v	k7c6	v
postupných	postupný	k2eAgFnPc6d1	postupná
modernizacích	modernizace	k1gFnPc6	modernizace
i	i	k8xC	i
novostavbách	novostavba	k1gFnPc6	novostavba
pavilonů	pavilon	k1gInPc2	pavilon
i	i	k9	i
zázemí	zázemí	k1gNnSc4	zázemí
pro	pro	k7c4	pro
návštěvníky	návštěvník	k1gMnPc4	návštěvník
<g/>
:	:	kIx,	:
nové	nový	k2eAgFnPc4d1	nová
restaurace	restaurace	k1gFnPc4	restaurace
<g/>
,	,	kIx,	,
hotelu	hotel	k1gInSc6	hotel
Safari	safari	k1gNnSc2	safari
<g/>
,	,	kIx,	,
postaveného	postavený	k2eAgNnSc2d1	postavené
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
i	i	k8xC	i
Safarikempu	Safarikemp	k1gInSc6	Safarikemp
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
také	také	k9	také
zřízena	zřízen	k2eAgFnSc1d1	zřízena
kontaktní	kontaktní	k2eAgFnSc1d1	kontaktní
dětská	dětský	k2eAgFnSc1d1	dětská
zoo	zoo	k1gFnSc1	zoo
a	a	k8xC	a
rekonstruována	rekonstruován	k2eAgFnSc1d1	rekonstruována
správní	správní	k2eAgFnSc1d1	správní
budova	budova	k1gFnSc1	budova
s	s	k7c7	s
galerií	galerie	k1gFnSc7	galerie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
realizoval	realizovat	k5eAaBmAgInS	realizovat
dávný	dávný	k2eAgInSc1d1	dávný
sen	sen	k1gInSc1	sen
Josefa	Josef	k1gMnSc2	Josef
Vágnera	Vágner	k1gMnSc2	Vágner
<g/>
:	:	kIx,	:
otevření	otevření	k1gNnSc1	otevření
safari	safari	k1gNnPc2	safari
pro	pro	k7c4	pro
motorizované	motorizovaný	k2eAgMnPc4d1	motorizovaný
návštěvníky	návštěvník	k1gMnPc4	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
práci	práce	k1gFnSc6	práce
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
od	od	k7c2	od
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
nový	nový	k2eAgMnSc1d1	nový
ředitel	ředitel	k1gMnSc1	ředitel
Přemysl	Přemysl	k1gMnSc1	Přemysl
Rabas	Rabas	k1gMnSc1	Rabas
<g/>
.	.	kIx.	.
</s>
<s>
Nechce	chtít	k5eNaImIp3nS	chtít
rušit	rušit	k5eAaImF	rušit
někdy	někdy	k6eAd1	někdy
kritizované	kritizovaný	k2eAgNnSc1d1	kritizované
safari	safari	k1gNnSc1	safari
vlastními	vlastní	k2eAgInPc7d1	vlastní
auty	aut	k1gInPc7	aut
<g/>
,	,	kIx,	,
rád	rád	k6eAd1	rád
by	by	kYmCp3nS	by
však	však	k9	však
zastavil	zastavit	k5eAaPmAgInS	zastavit
propad	propad	k1gInSc1	propad
návštěvnosti	návštěvnost	k1gFnSc2	návštěvnost
a	a	k8xC	a
rád	rád	k2eAgMnSc1d1	rád
by	by	kYmCp3nS	by
navrátil	navrátit	k5eAaPmAgMnS	navrátit
zoo	zoo	k1gFnSc4	zoo
do	do	k7c2	do
asociací	asociace	k1gFnPc2	asociace
EAZA	EAZA	kA	EAZA
a	a	k8xC	a
WAZA	WAZA	kA	WAZA
<g/>
.	.	kIx.	.
</s>
<s>
Zapojení	zapojení	k1gNnSc1	zapojení
do	do	k7c2	do
činnosti	činnost	k1gFnSc2	činnost
EAZA	EAZA	kA	EAZA
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
a	a	k8xC	a
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
došlo	dojít	k5eAaPmAgNnS	dojít
i	i	k9	i
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
počtu	počet	k1gInSc2	počet
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
otevření	otevření	k1gNnSc3	otevření
lvího	lví	k2eAgNnSc2d1	lví
safari	safari	k1gNnSc2	safari
s	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
přes	přes	k7c4	přes
1,5	[number]	k4	1,5
ha	ha	kA	ha
<g/>
,	,	kIx,	,
prvního	první	k4xOgNnSc2	první
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
byl	být	k5eAaImAgMnS	být
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
oslav	oslava	k1gFnPc2	oslava
70	[number]	k4	70
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
od	od	k7c2	od
založení	založení	k1gNnSc2	založení
zoo	zoo	k1gFnSc2	zoo
otevřen	otevřen	k2eAgInSc4d1	otevřen
nový	nový	k2eAgInSc4d1	nový
vjezd	vjezd	k1gInSc4	vjezd
do	do	k7c2	do
safari	safari	k1gNnSc2	safari
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
nyní	nyní	k6eAd1	nyní
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc4	název
Africké	africký	k2eAgNnSc1d1	africké
Safari	safari	k1gNnSc1	safari
Josefa	Josef	k1gMnSc2	Josef
Vágnera	Vágner	k1gMnSc2	Vágner
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
zoo	zoo	k1gNnSc2	zoo
byla	být	k5eAaImAgFnS	být
dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
prohlášena	prohlásit	k5eAaPmNgFnS	prohlásit
kulturní	kulturní	k2eAgFnSc7d1	kulturní
památkou	památka	k1gFnSc7	památka
<g/>
.	.	kIx.	.
</s>
<s>
Zoo	zoo	k1gFnSc1	zoo
Dvůr	Dvůr	k1gInSc1	Dvůr
Králové	Králová	k1gFnSc2	Králová
chová	chovat	k5eAaImIp3nS	chovat
2	[number]	k4	2
717	[number]	k4	717
ks	ks	kA	ks
evidovaných	evidovaný	k2eAgNnPc2d1	evidované
zvířat	zvíře	k1gNnPc2	zvíře
v	v	k7c6	v
292	[number]	k4	292
druzích	druh	k1gInPc6	druh
(	(	kIx(	(
<g/>
údaje	údaj	k1gInPc4	údaj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
je	být	k5eAaImIp3nS	být
67	[number]	k4	67
druhů	druh	k1gInPc2	druh
savců	savec	k1gMnPc2	savec
a	a	k8xC	a
101	[number]	k4	101
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
zvířata	zvíře	k1gNnPc4	zvíře
se	se	k3xPyFc4	se
stará	starat	k5eAaImIp3nS	starat
158	[number]	k4	158
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Provozní	provozní	k2eAgInPc1d1	provozní
náklady	náklad	k1gInPc1	náklad
zoo	zoo	k1gFnSc1	zoo
činily	činit	k5eAaImAgInP	činit
150,5	[number]	k4	150,5
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
zřizovatele	zřizovatel	k1gMnSc2	zřizovatel
-	-	kIx~	-
Královéhradeckého	královéhradecký	k2eAgInSc2d1	královéhradecký
kraje	kraj	k1gInSc2	kraj
-	-	kIx~	-
obdržela	obdržet	k5eAaPmAgFnS	obdržet
47,2	[number]	k4	47,2
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
od	od	k7c2	od
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
4,7	[number]	k4	4,7
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Provozní	provozní	k2eAgFnSc1d1	provozní
soběstačnost	soběstačnost	k1gFnSc1	soběstačnost
tak	tak	k6eAd1	tak
činila	činit	k5eAaImAgFnS	činit
68	[number]	k4	68
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
i	i	k8xC	i
podle	podle	k7c2	podle
celkových	celkový	k2eAgInPc2d1	celkový
příjmů	příjem	k1gInPc2	příjem
je	být	k5eAaImIp3nS	být
královédvorská	královédvorský	k2eAgFnSc1d1	Královédvorská
zoo	zoo	k1gFnSc1	zoo
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
(	(	kIx(	(
<g/>
po	po	k7c6	po
zoo	zoo	k1gFnSc6	zoo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
návštěvníků	návštěvník	k1gMnPc2	návštěvník
je	být	k5eAaImIp3nS	být
až	až	k9	až
na	na	k7c6	na
pátém	pátý	k4xOgNnSc6	pátý
místě	místo	k1gNnSc6	místo
-	-	kIx~	-
předstihuje	předstihovat	k5eAaImIp3nS	předstihovat
ji	on	k3xPp3gFnSc4	on
nejen	nejen	k6eAd1	nejen
Zoo	zoo	k1gFnSc4	zoo
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
Zoo	zoo	k1gFnSc1	zoo
Zlín	Zlín	k1gInSc1	Zlín
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
a	a	k8xC	a
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Unie	unie	k1gFnSc1	unie
českých	český	k2eAgFnPc2d1	Česká
a	a	k8xC	a
slovenských	slovenský	k2eAgFnPc2d1	slovenská
zoologických	zoologický	k2eAgFnPc2d1	zoologická
zahrad	zahrada	k1gFnPc2	zahrada
(	(	kIx(	(
<g/>
UCSZOO	UCSZOO	kA	UCSZOO
<g/>
)	)	kIx)	)
-	-	kIx~	-
člen	člen	k1gMnSc1	člen
od	od	k7c2	od
vzniku	vznik	k1gInSc2	vznik
Unie	unie	k1gFnSc2	unie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
International	International	k1gFnSc2	International
Zoo	zoo	k1gFnPc2	zoo
Educators	Educators	k1gInSc1	Educators
(	(	kIx(	(
<g/>
International	International	k1gFnSc1	International
Association	Association	k1gInSc1	Association
of	of	k?	of
Zoo	zoo	k1gFnSc1	zoo
Educators	Educators	k1gInSc1	Educators
<g/>
,	,	kIx,	,
IZE	IZE	kA	IZE
<g/>
)	)	kIx)	)
-	-	kIx~	-
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
předsedů	předseda	k1gMnPc2	předseda
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
Euro-asijská	Eurosijský	k2eAgFnSc1d1	Euro-asijská
regionální	regionální	k2eAgFnSc1d1	regionální
asociace	asociace	k1gFnSc1	asociace
zoo	zoo	k1gFnPc2	zoo
a	a	k8xC	a
akvárií	akvárium	k1gNnPc2	akvárium
(	(	kIx(	(
<g/>
Е	Е	k?	Е
<g />
.	.	kIx.	.
</s>
<s>
pе	pе	k?	pе
aс	aс	k?	aс
з	з	k?	з
и	и	k?	и
aк	aк	k?	aк
<g/>
,	,	kIx,	,
EARAZA	EARAZA	kA	EARAZA
<g/>
)	)	kIx)	)
-	-	kIx~	-
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
Evropská	evropský	k2eAgFnSc1d1	Evropská
asociace	asociace	k1gFnSc1	asociace
zoologických	zoologický	k2eAgFnPc2d1	zoologická
zahrad	zahrada	k1gFnPc2	zahrada
a	a	k8xC	a
akvárií	akvárium	k1gNnPc2	akvárium
(	(	kIx(	(
<g/>
European	European	k1gInSc1	European
Association	Association	k1gInSc1	Association
of	of	k?	of
Zoos	Zoos	k1gInSc1	Zoos
and	and	k?	and
Aquaria	Aquarium	k1gNnSc2	Aquarium
<g/>
,	,	kIx,	,
EAZA	EAZA	kA	EAZA
-	-	kIx~	-
opět	opět	k6eAd1	opět
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
Evropská	evropský	k2eAgFnSc1d1	Evropská
asociace	asociace	k1gFnSc1	asociace
zoologických	zoologický	k2eAgFnPc2d1	zoologická
zahrad	zahrada	k1gFnPc2	zahrada
a	a	k8xC	a
akvárií	akvárium	k1gNnPc2	akvárium
(	(	kIx(	(
<g/>
European	European	k1gInSc1	European
Association	Association	k1gInSc1	Association
of	of	k?	of
Zoos	Zoos	k1gInSc1	Zoos
and	and	k?	and
Aquaria	Aquarium	k1gNnSc2	Aquarium
<g/>
,	,	kIx,	,
EAZA	EAZA	kA	EAZA
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
člen	člen	k1gMnSc1	člen
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1995	[number]	k4	1995
<g/>
-	-	kIx~	-
<g/>
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2010	[number]	k4	2010
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
výkonný	výkonný	k2eAgInSc1d1	výkonný
výbor	výbor	k1gInSc1	výbor
EAZA	EAZA	kA	EAZA
podmínečně	podmínečně	k6eAd1	podmínečně
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
vyloučit	vyloučit	k5eAaPmF	vyloučit
královédvorskou	královédvorský	k2eAgFnSc4d1	Královédvorská
zoo	zoo	k1gFnSc4	zoo
pro	pro	k7c4	pro
opakované	opakovaný	k2eAgNnSc4d1	opakované
porušování	porušování	k1gNnSc4	porušování
pravidel	pravidlo	k1gNnPc2	pravidlo
v	v	k7c6	v
evropských	evropský	k2eAgInPc6d1	evropský
záchranných	záchranný	k2eAgInPc6d1	záchranný
programech	program	k1gInPc6	program
<g/>
,	,	kIx,	,
ředitelka	ředitelka	k1gFnSc1	ředitelka
zoo	zoo	k1gFnSc1	zoo
Dana	Dana	k1gFnSc1	Dana
Holečková	Holečková	k1gFnSc1	Holečková
označila	označit	k5eAaPmAgFnS	označit
tento	tento	k3xDgInSc4	tento
postup	postup	k1gInSc4	postup
za	za	k7c4	za
rozporný	rozporný	k2eAgInSc4d1	rozporný
se	s	k7c7	s
stanovami	stanova	k1gFnPc7	stanova
EAZA	EAZA	kA	EAZA
a	a	k8xC	a
zoo	zoo	k1gFnSc1	zoo
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
tomuto	tento	k3xDgNnSc3	tento
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
ukončila	ukončit	k5eAaPmAgFnS	ukončit
členství	členství	k1gNnSc4	členství
v	v	k7c4	v
EAZA	EAZA	kA	EAZA
sama	sám	k3xTgFnSc1	sám
k	k	k7c3	k
30	[number]	k4	30
<g/>
.	.	kIx.	.
červnu	červen	k1gInSc3	červen
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
tak	tak	k8xC	tak
první	první	k4xOgFnSc7	první
zoologickou	zoologický	k2eAgFnSc7d1	zoologická
zahradou	zahrada	k1gFnSc7	zahrada
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
EAZA	EAZA	kA	EAZA
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
z	z	k7c2	z
organizace	organizace	k1gFnSc2	organizace
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
krok	krok	k1gInSc1	krok
tehdy	tehdy	k6eAd1	tehdy
podpořil	podpořit	k5eAaPmAgInS	podpořit
i	i	k9	i
prezident	prezident	k1gMnSc1	prezident
Unie	unie	k1gFnSc2	unie
českých	český	k2eAgFnPc2d1	Česká
a	a	k8xC	a
slovenských	slovenský	k2eAgFnPc2d1	slovenská
zoologických	zoologický	k2eAgFnPc2d1	zoologická
zahrad	zahrada	k1gFnPc2	zahrada
Vladislav	Vladislav	k1gMnSc1	Vladislav
Jiroušek	Jiroušek	k1gMnSc1	Jiroušek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
proti	proti	k7c3	proti
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
výkonného	výkonný	k2eAgInSc2d1	výkonný
výboru	výbor	k1gInSc2	výbor
EAZA	EAZA	kA	EAZA
podal	podat	k5eAaPmAgMnS	podat
stížnost	stížnost	k1gFnSc4	stížnost
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
Královéhradecký	královéhradecký	k2eAgInSc1d1	královéhradecký
kraj	kraj	k1gInSc1	kraj
jako	jako	k8xS	jako
zřizovatel	zřizovatel	k1gMnSc1	zřizovatel
zoo	zoo	k1gFnSc2	zoo
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vystoupení	vystoupení	k1gNnSc1	vystoupení
z	z	k7c2	z
asociace	asociace	k1gFnSc2	asociace
by	by	kYmCp3nS	by
nemělo	mít	k5eNaImAgNnS	mít
provozu	provoz	k1gInSc3	provoz
či	či	k8xC	či
atraktivitě	atraktivita	k1gFnSc3	atraktivita
zahrady	zahrada	k1gFnSc2	zahrada
přinést	přinést	k5eAaPmF	přinést
žádné	žádný	k3yNgFnPc4	žádný
škody	škoda	k1gFnPc4	škoda
či	či	k8xC	či
újmy	újma	k1gFnPc4	újma
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
ji	on	k3xPp3gFnSc4	on
už	už	k6eAd1	už
nebudou	být	k5eNaImBp3nP	být
svazovat	svazovat	k5eAaImF	svazovat
neoprávněné	oprávněný	k2eNgInPc4d1	neoprávněný
požadavky	požadavek	k1gInPc4	požadavek
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
evropské	evropský	k2eAgFnSc2d1	Evropská
asociace	asociace	k1gFnSc2	asociace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
v	v	k7c6	v
českými	český	k2eAgInPc7d1	český
právními	právní	k2eAgInPc7d1	právní
předpisy	předpis	k1gInPc7	předpis
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgMnSc3	ten
bývalá	bývalý	k2eAgFnSc1d1	bývalá
pracovnice	pracovnice	k1gFnSc1	pracovnice
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
,	,	kIx,	,
zooložka	zooložka	k1gFnSc1	zooložka
Kristina	Kristina	k1gFnSc1	Kristina
Tomášová	Tomášová	k1gFnSc1	Tomášová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
10	[number]	k4	10
let	let	k1gInSc4	let
koordinátorkou	koordinátorka	k1gFnSc7	koordinátorka
evropského	evropský	k2eAgInSc2d1	evropský
záchranného	záchranný	k2eAgInSc2d1	záchranný
programu	program	k1gInSc2	program
bílých	bílý	k2eAgMnPc2d1	bílý
nosorožců	nosorožec	k1gMnPc2	nosorožec
<g/>
,	,	kIx,	,
označila	označit	k5eAaPmAgFnS	označit
zánik	zánik	k1gInSc4	zánik
členství	členství	k1gNnSc2	členství
za	za	k7c4	za
zánik	zánik	k1gInSc4	zánik
kreditu	kredit	k1gInSc2	kredit
a	a	k8xC	a
srovnala	srovnat	k5eAaPmAgFnS	srovnat
jej	on	k3xPp3gMnSc4	on
s	s	k7c7	s
přestupem	přestup	k1gInSc7	přestup
hokejového	hokejový	k2eAgInSc2d1	hokejový
týmu	tým	k1gInSc2	tým
z	z	k7c2	z
první	první	k4xOgFnSc2	první
ligy	liga	k1gFnSc2	liga
do	do	k7c2	do
okresního	okresní	k2eAgInSc2d1	okresní
přeboru	přebor	k1gInSc2	přebor
<g/>
.	.	kIx.	.
</s>
<s>
Zoo	zoo	k1gFnSc1	zoo
tak	tak	k9	tak
nyní	nyní	k6eAd1	nyní
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
členem	člen	k1gMnSc7	člen
EAZA	EAZA	kA	EAZA
jen	jen	k6eAd1	jen
nepřímo	přímo	k6eNd1	přímo
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
členství	členství	k1gNnSc4	členství
v	v	k7c6	v
UCSZOO	UCSZOO	kA	UCSZOO
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Avšak	avšak	k8xC	avšak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
se	se	k3xPyFc4	se
zoo	zoo	k1gFnSc1	zoo
opět	opět	k6eAd1	opět
připojila	připojit	k5eAaPmAgFnS	připojit
do	do	k7c2	do
EAZA	EAZA	kA	EAZA
.	.	kIx.	.
</s>
<s>
Světová	světový	k2eAgFnSc1d1	světová
asociace	asociace	k1gFnSc1	asociace
zoologických	zoologický	k2eAgFnPc2d1	zoologická
zahrad	zahrada	k1gFnPc2	zahrada
a	a	k8xC	a
akvárií	akvárium	k1gNnPc2	akvárium
(	(	kIx(	(
<g/>
World	World	k1gInSc1	World
Association	Association	k1gInSc1	Association
of	of	k?	of
Zoos	Zoos	k1gInSc1	Zoos
and	and	k?	and
Aquariums	Aquariums	k1gInSc1	Aquariums
<g/>
,	,	kIx,	,
WAZA	WAZA	kA	WAZA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
unie	unie	k1gFnSc1	unie
ředitelů	ředitel	k1gMnPc2	ředitel
zoologických	zoologický	k2eAgFnPc2d1	zoologická
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
;	;	kIx,	;
člen	člen	k1gMnSc1	člen
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1997	[number]	k4	1997
<g/>
-	-	kIx~	-
<g/>
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vystoupení	vystoupení	k1gNnSc6	vystoupení
zoo	zoo	k1gFnSc2	zoo
z	z	k7c2	z
EAZA	EAZA	kA	EAZA
následovalo	následovat	k5eAaImAgNnS	následovat
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2010	[number]	k4	2010
podmínečné	podmínečný	k2eAgFnSc2d1	podmínečná
vyloučení	vyloučení	k1gNnSc3	vyloučení
i	i	k8xC	i
z	z	k7c2	z
WAZA	WAZA	kA	WAZA
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
za	za	k7c4	za
nedodržování	nedodržování	k1gNnSc4	nedodržování
pravidel	pravidlo	k1gNnPc2	pravidlo
této	tento	k3xDgFnSc2	tento
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
organizace	organizace	k1gFnSc2	organizace
<g/>
,	,	kIx,	,
zoo	zoo	k1gFnSc1	zoo
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
tomuto	tento	k3xDgNnSc3	tento
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
odvolala	odvolat	k5eAaPmAgFnS	odvolat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2012	[number]	k4	2012
pak	pak	k8xC	pak
WAZA	WAZA	kA	WAZA
královédvorskou	královédvorský	k2eAgFnSc4d1	Královédvorská
zoo	zoo	k1gFnSc4	zoo
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
řad	řada	k1gFnPc2	řada
definitivně	definitivně	k6eAd1	definitivně
vyloučila	vyloučit	k5eAaPmAgFnS	vyloučit
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
neschopnost	neschopnost	k1gFnSc4	neschopnost
dodržet	dodržet	k5eAaPmF	dodržet
standardy	standard	k1gInPc4	standard
stanovené	stanovený	k2eAgInPc4d1	stanovený
etickým	etický	k2eAgInSc7d1	etický
kodexem	kodex	k1gInSc7	kodex
WAZA	WAZA	kA	WAZA
<g/>
"	"	kIx"	"
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
"	"	kIx"	"
<g/>
obvinění	obvinění	k1gNnSc3	obvinění
z	z	k7c2	z
vážného	vážný	k2eAgNnSc2d1	vážné
porušení	porušení	k1gNnSc2	porušení
etického	etický	k2eAgNnSc2d1	etické
chování	chování	k1gNnSc2	chování
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
četné	četný	k2eAgInPc4d1	četný
převody	převod	k1gInPc4	převod
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
veškeré	veškerý	k3xTgInPc4	veškerý
následky	následek	k1gInPc4	následek
těchto	tento	k3xDgNnPc2	tento
porušení	porušení	k1gNnPc2	porušení
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
podle	podle	k7c2	podle
zoo	zoo	k1gFnSc2	zoo
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgNnPc4	tento
obvinění	obvinění	k1gNnPc4	obvinění
konkretizována	konkretizován	k2eAgNnPc4d1	konkretizováno
<g/>
.	.	kIx.	.
</s>
<s>
Ředitelka	ředitelka	k1gFnSc1	ředitelka
zoo	zoo	k1gFnSc1	zoo
Dana	Dana	k1gFnSc1	Dana
Holečková	Holečková	k1gFnSc1	Holečková
tuto	tento	k3xDgFnSc4	tento
záležitost	záležitost	k1gFnSc1	záležitost
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
okrajovou	okrajový	k2eAgFnSc4d1	okrajová
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zoo	zoo	k1gFnSc1	zoo
již	jenž	k3xRgFnSc4	jenž
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
sama	sám	k3xTgFnSc1	sám
uvažovala	uvažovat	k5eAaImAgFnS	uvažovat
o	o	k7c4	o
vystoupení	vystoupení	k1gNnSc4	vystoupení
z	z	k7c2	z
WAZA	WAZA	kA	WAZA
<g/>
.	.	kIx.	.
</s>
<s>
Zoo	zoo	k1gFnSc1	zoo
také	také	k9	také
čelí	čelit	k5eAaImIp3nS	čelit
obvinění	obvinění	k1gNnSc4	obvinění
<g/>
,	,	kIx,	,
že	že	k8xS	že
obchodovala	obchodovat	k5eAaImAgFnS	obchodovat
se	s	k7c7	s
zvířaty	zvíře	k1gNnPc7	zvíře
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
exprezidenta	exprezident	k1gMnSc2	exprezident
Unie	unie	k1gFnSc2	unie
českých	český	k2eAgMnPc2d1	český
a	a	k8xC	a
slovenských	slovenský	k2eAgFnPc2d1	slovenská
zoologických	zoologický	k2eAgFnPc2d1	zoologická
zahrad	zahrada	k1gFnPc2	zahrada
V.	V.	kA	V.
Jirouška	Jiroušek	k1gMnSc2	Jiroušek
skutečný	skutečný	k2eAgInSc4d1	skutečný
důvod	důvod	k1gInSc4	důvod
vyloučení	vyloučení	k1gNnSc2	vyloučení
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Dvůr	Dvůr	k1gInSc1	Dvůr
se	se	k3xPyFc4	se
po	po	k7c4	po
vystoupení	vystoupení	k1gNnSc4	vystoupení
z	z	k7c2	z
EAZY	EAZY	kA	EAZY
necítil	cítit	k5eNaImAgInS	cítit
vázán	vázat	k5eAaImNgInS	vázat
žádnými	žádný	k3yNgNnPc7	žádný
pravidly	pravidlo	k1gNnPc7	pravidlo
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
zvířata	zvíře	k1gNnPc4	zvíře
prodával	prodávat	k5eAaImAgMnS	prodávat
jak	jak	k8xS	jak
chtěl	chtít	k5eAaImAgMnS	chtít
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
také	také	k9	také
kam	kam	k6eAd1	kam
chtěl	chtít	k5eAaImAgMnS	chtít
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Jeho	jeho	k3xOp3gNnSc2	jeho
slova	slovo	k1gNnSc2	slovo
podporuje	podporovat	k5eAaImIp3nS	podporovat
i	i	k9	i
zooložka	zooložka	k1gFnSc1	zooložka
Lenka	Lenka	k1gFnSc1	Lenka
Vágnerová	Vágnerová	k1gFnSc1	Vágnerová
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
Josefa	Josef	k1gMnSc2	Josef
Vágnera	Vágner	k1gMnSc2	Vágner
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jejíchž	jejíž	k3xOyRp3gFnPc2	jejíž
informací	informace	k1gFnPc2	informace
královédvorská	královédvorský	k2eAgFnSc1d1	Královédvorská
zoo	zoo	k1gFnSc1	zoo
odsunula	odsunout	k5eAaPmAgFnS	odsunout
během	během	k7c2	během
posledních	poslední	k2eAgNnPc2d1	poslední
let	léto	k1gNnPc2	léto
množství	množství	k1gNnSc1	množství
kopytníků	kopytník	k1gMnPc2	kopytník
do	do	k7c2	do
Emirates	Emiratesa	k1gFnPc2	Emiratesa
Zoo	zoo	k1gNnPc2	zoo
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
arabských	arabský	k2eAgInPc6d1	arabský
emirátech	emirát	k1gInPc6	emirát
(	(	kIx(	(
<g/>
tato	tento	k3xDgFnSc1	tento
zoo	zoo	k1gFnSc1	zoo
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
WAZA	WAZA	kA	WAZA
vyloučena	vyloučit	k5eAaPmNgFnS	vyloučit
současně	současně	k6eAd1	současně
s	s	k7c7	s
královédvorskou	královédvorský	k2eAgFnSc7d1	Královédvorská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
předseda	předseda	k1gMnSc1	předseda
představenstva	představenstvo	k1gNnSc2	představenstvo
zoo	zoo	k1gNnSc2	zoo
Marian	Marian	k1gMnSc1	Marian
Slodičák	Slodičák	k1gMnSc1	Slodičák
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
vyloučení	vyloučení	k1gNnSc2	vyloučení
z	z	k7c2	z
EAZA	EAZA	kA	EAZA
je	být	k5eAaImIp3nS	být
exemplárním	exemplární	k2eAgInSc7d1	exemplární
trestem	trest	k1gInSc7	trest
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
odstrašit	odstrašit	k5eAaPmF	odstrašit
případné	případný	k2eAgFnSc3d1	případná
další	další	k2eAgFnSc3d1	další
vzbouřence	vzbouřenka	k1gFnSc3	vzbouřenka
<g/>
.	.	kIx.	.
</s>
<s>
Spory	spor	k1gInPc1	spor
zoo	zoo	k1gFnSc2	zoo
s	s	k7c7	s
asociacemi	asociace	k1gFnPc7	asociace
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
začaly	začít	k5eAaPmAgInP	začít
reintrodukčním	reintrodukční	k2eAgInSc7d1	reintrodukční
transportem	transport	k1gInSc7	transport
nosorožců	nosorožec	k1gMnPc2	nosorožec
do	do	k7c2	do
Keni	Keňa	k1gFnSc2	Keňa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
se	se	k3xPyFc4	se
asociace	asociace	k1gFnPc1	asociace
snažily	snažit	k5eAaImAgFnP	snažit
zabránit	zabránit	k5eAaPmF	zabránit
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
K	k	k7c3	k
situaci	situace	k1gFnSc3	situace
okolo	okolo	k7c2	okolo
vyloučení	vyloučení	k1gNnSc2	vyloučení
vydala	vydat	k5eAaPmAgFnS	vydat
své	svůj	k3xOyFgNnSc4	svůj
stanovisko	stanovisko	k1gNnSc4	stanovisko
i	i	k8xC	i
Unie	unie	k1gFnSc1	unie
českých	český	k2eAgFnPc2d1	Česká
a	a	k8xC	a
slovenských	slovenský	k2eAgFnPc2d1	slovenská
zoologických	zoologický	k2eAgFnPc2d1	zoologická
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
stranu	strana	k1gFnSc4	strana
respektuje	respektovat	k5eAaImIp3nS	respektovat
právo	právo	k1gNnSc1	právo
vedení	vedení	k1gNnSc2	vedení
zoo	zoo	k1gFnSc2	zoo
"	"	kIx"	"
<g/>
začít	začít	k5eAaPmF	začít
naplňovat	naplňovat	k5eAaImF	naplňovat
poslání	poslání	k1gNnSc4	poslání
zoologických	zoologický	k2eAgFnPc2d1	zoologická
zahrad	zahrada	k1gFnPc2	zahrada
svou	svůj	k3xOyFgFnSc7	svůj
vlastní	vlastní	k2eAgFnSc7d1	vlastní
cestou	cesta	k1gFnSc7	cesta
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
považuje	považovat	k5eAaImIp3nS	považovat
aktivní	aktivní	k2eAgFnSc4d1	aktivní
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
EAZA	EAZA	kA	EAZA
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
WAZA	WAZA	kA	WAZA
za	za	k7c7	za
nejefektivnější	efektivní	k2eAgFnSc7d3	nejefektivnější
cestou	cesta	k1gFnSc7	cesta
k	k	k7c3	k
naplňování	naplňování	k1gNnSc3	naplňování
poslání	poslání	k1gNnSc2	poslání
zoologických	zoologický	k2eAgFnPc2d1	zoologická
zahrad	zahrada	k1gFnPc2	zahrada
a	a	k8xC	a
"	"	kIx"	"
<g/>
neztotožňuje	ztotožňovat	k5eNaImIp3nS	ztotožňovat
se	se	k3xPyFc4	se
s	s	k7c7	s
mediálními	mediální	k2eAgInPc7d1	mediální
výstupy	výstup	k1gInPc7	výstup
představitelů	představitel	k1gMnPc2	představitel
ZOO	zoo	k1gFnSc2	zoo
Dvůr	Dvůr	k1gInSc1	Dvůr
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
(	(	kIx(	(
<g/>
své	svůj	k3xOyFgInPc4	svůj
<g/>
)	)	kIx)	)
kroky	krok	k1gInPc4	krok
popisuje	popisovat	k5eAaImIp3nS	popisovat
jako	jako	k8xC	jako
logické	logický	k2eAgNnSc1d1	logické
vyústění	vyústění	k1gNnSc1	vyústění
špatné	špatný	k2eAgFnSc2d1	špatná
práce	práce	k1gFnSc2	práce
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
organizací	organizace	k1gFnPc2	organizace
EAZA	EAZA	kA	EAZA
a	a	k8xC	a
WAZA	WAZA	kA	WAZA
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc4	jejich
zbytečnosti	zbytečnost	k1gFnPc4	zbytečnost
<g/>
,	,	kIx,	,
potažmo	potažmo	k6eAd1	potažmo
škodlivosti	škodlivost	k1gFnPc1	škodlivost
pro	pro	k7c4	pro
chov	chov	k1gInSc4	chov
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
Daně	Dana	k1gFnSc3	Dana
Holečkové	Holečkové	k2eAgMnPc2d1	Holečkové
se	se	k3xPyFc4	se
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
zvedala	zvedat	k5eAaImAgFnS	zvedat
vlna	vlna	k1gFnSc1	vlna
odporu	odpor	k1gInSc2	odpor
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
čele	čelo	k1gNnSc6	čelo
stojí	stát	k5eAaImIp3nS	stát
několik	několik	k4yIc1	několik
bývalých	bývalý	k2eAgMnPc2d1	bývalý
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
zoo	zoo	k1gNnSc2	zoo
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
občanského	občanský	k2eAgNnSc2d1	občanské
sdružení	sdružení	k1gNnSc2	sdružení
Safari	safari	k1gNnSc2	safari
Archa	archa	k1gFnSc1	archa
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Vytýkají	vytýkat	k5eAaImIp3nP	vytýkat
jí	on	k3xPp3gFnSc7	on
například	například	k6eAd1	například
arogantní	arogantní	k2eAgInSc4d1	arogantní
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
podřízeným	podřízený	k2eAgMnPc3d1	podřízený
<g/>
,	,	kIx,	,
neodborné	odborný	k2eNgInPc1d1	neodborný
zásahy	zásah	k1gInPc1	zásah
do	do	k7c2	do
chovatelské	chovatelský	k2eAgFnSc2d1	chovatelská
práce	práce	k1gFnSc2	práce
(	(	kIx(	(
<g/>
které	který	k3yRgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
např.	např.	kA	např.
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
údajně	údajně	k6eAd1	údajně
za	za	k7c4	za
následek	následek	k1gInSc4	následek
úmrtí	úmrtí	k1gNnSc2	úmrtí
slonice	slonice	k1gFnSc2	slonice
a	a	k8xC	a
jejího	její	k3xOp3gMnSc2	její
ošetřovatele	ošetřovatel	k1gMnSc2	ošetřovatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nesystémovou	systémový	k2eNgFnSc4d1	nesystémová
cenovou	cenový	k2eAgFnSc4d1	cenová
politiku	politika	k1gFnSc4	politika
(	(	kIx(	(
<g/>
např.	např.	kA	např.
zavedení	zavedení	k1gNnSc1	zavedení
vstupného	vstupné	k1gNnSc2	vstupné
již	již	k9	již
pro	pro	k7c4	pro
dvouleté	dvouletý	k2eAgFnPc4d1	dvouletá
děti	dítě	k1gFnPc4	dítě
<g/>
)	)	kIx)	)
či	či	k8xC	či
podbízivý	podbízivý	k2eAgInSc4d1	podbízivý
lunaparkový	lunaparkový	k2eAgInSc4d1	lunaparkový
styl	styl	k1gInSc4	styl
<g/>
.	.	kIx.	.
</s>
<s>
Unie	unie	k1gFnSc1	unie
českých	český	k2eAgFnPc2d1	Česká
a	a	k8xC	a
slovenských	slovenský	k2eAgFnPc2d1	slovenská
zoologických	zoologický	k2eAgFnPc2d1	zoologická
zahrad	zahrada	k1gFnPc2	zahrada
tyto	tento	k3xDgInPc1	tento
"	"	kIx"	"
<g/>
tendenční	tendenční	k2eAgInPc1d1	tendenční
a	a	k8xC	a
účelové	účelový	k2eAgInPc1d1	účelový
mediální	mediální	k2eAgInPc1d1	mediální
výstupy	výstup	k1gInPc1	výstup
některých	některý	k3yIgMnPc2	některý
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
a	a	k8xC	a
občanských	občanský	k2eAgFnPc2d1	občanská
iniciativ	iniciativa	k1gFnPc2	iniciativa
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
vedeny	vést	k5eAaImNgFnP	vést
osobní	osobní	k2eAgFnSc7d1	osobní
záští	zášť	k1gFnSc7	zášť
a	a	k8xC	a
vlastními	vlastní	k2eAgInPc7d1	vlastní
zájmy	zájem	k1gInPc7	zájem
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
odsoudila	odsoudit	k5eAaPmAgFnS	odsoudit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
rezignaci	rezignace	k1gFnSc3	rezignace
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
ředitelku	ředitelka	k1gFnSc4	ředitelka
v	v	k7c6	v
září	září	k1gNnSc6	září
2012	[number]	k4	2012
i	i	k8xC	i
hejtman	hejtman	k1gMnSc1	hejtman
královéhradeckého	královéhradecký	k2eAgInSc2d1	královéhradecký
kraje	kraj	k1gInSc2	kraj
Lubomír	Lubomír	k1gMnSc1	Lubomír
Franc	Franc	k1gMnSc1	Franc
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
dosud	dosud	k6eAd1	dosud
zastával	zastávat	k5eAaImAgMnS	zastávat
<g/>
.	.	kIx.	.
</s>
<s>
Holečková	Holečková	k1gFnSc1	Holečková
jeho	jeho	k3xOp3gFnSc4	jeho
výzvu	výzva	k1gFnSc4	výzva
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
později	pozdě	k6eAd2	pozdě
rezignovala	rezignovat	k5eAaBmAgFnS	rezignovat
sama	sám	k3xTgMnSc4	sám
<g/>
.	.	kIx.	.
největší	veliký	k2eAgInSc4d3	veliký
chov	chov	k1gInSc4	chov
zaměřený	zaměřený	k2eAgInSc4d1	zaměřený
na	na	k7c4	na
africké	africký	k2eAgMnPc4d1	africký
savce	savec	k1gMnPc4	savec
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
jediné	jediný	k2eAgNnSc1d1	jediné
lví	lví	k2eAgNnSc1d1	lví
safari	safari	k1gNnSc1	safari
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
Jediné	jediný	k2eAgInPc1d1	jediný
exempláře	exemplář	k1gInPc1	exemplář
pralesní	pralesní	k2eAgFnSc2d1	pralesní
žirafy	žirafa	k1gFnSc2	žirafa
Okapi	okapi	k1gFnSc2	okapi
chovaný	chovaný	k2eAgInSc1d1	chovaný
v	v	k7c6	v
ČR	ČR	kA	ČR
-	-	kIx~	-
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
i	i	k9	i
2	[number]	k4	2
<g/>
×	×	k?	×
rozmnožit	rozmnožit	k5eAaPmF	rozmnožit
galerie	galerie	k1gFnPc4	galerie
originálů	originál	k1gMnPc2	originál
obrazů	obraz	k1gInPc2	obraz
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Buriana	Burian	k1gMnSc2	Burian
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
byli	být	k5eAaImAgMnP	být
4	[number]	k4	4
z	z	k7c2	z
posledních	poslední	k2eAgInPc2d1	poslední
7	[number]	k4	7
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
žijících	žijící	k2eAgMnPc2d1	žijící
nosorožců	nosorožec	k1gMnPc2	nosorožec
tuponosých	tuponosý	k2eAgFnPc2d1	tuponosá
severních	severní	k2eAgFnPc2d1	severní
převezeni	převézt	k5eAaPmNgMnP	převézt
kvůli	kvůli	k7c3	kvůli
naději	naděje	k1gFnSc3	naděje
na	na	k7c4	na
záchranu	záchrana	k1gFnSc4	záchrana
druhu	druh	k1gInSc2	druh
do	do	k7c2	do
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
Ol	Ola	k1gFnPc2	Ola
Pejeta	Pejeto	k1gNnSc2	Pejeto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1975	[number]	k4	1975
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
podezření	podezření	k1gNnSc4	podezření
z	z	k7c2	z
onemocnění	onemocnění	k1gNnSc2	onemocnění
slintavkou	slintavka	k1gFnSc7	slintavka
typu	typ	k1gInSc2	typ
SAT	SAT	kA	SAT
1	[number]	k4	1
zastřeleno	zastřelen	k2eAgNnSc4d1	zastřeleno
největší	veliký	k2eAgNnSc4d3	veliký
stádo	stádo	k1gNnSc4	stádo
žiraf	žirafa	k1gFnPc2	žirafa
(	(	kIx(	(
<g/>
29	[number]	k4	29
kusů	kus	k1gInPc2	kus
<g/>
)	)	kIx)	)
-	-	kIx~	-
toto	tento	k3xDgNnSc1	tento
podezření	podezření	k1gNnSc1	podezření
nebylo	být	k5eNaImAgNnS	být
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
spolehlivě	spolehlivě	k6eAd1	spolehlivě
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
ani	ani	k8xC	ani
vyvráceno	vyvrátit	k5eAaPmNgNnS	vyvrátit
kvůli	kvůli	k7c3	kvůli
minimální	minimální	k2eAgFnSc3d1	minimální
dokumentaci	dokumentace	k1gFnSc3	dokumentace
<g/>
.	.	kIx.	.
</s>
<s>
Akci	akce	k1gFnSc4	akce
tehdy	tehdy	k6eAd1	tehdy
kontrolovali	kontrolovat	k5eAaImAgMnP	kontrolovat
příslušníci	příslušník	k1gMnPc1	příslušník
Státní	státní	k2eAgFnSc2d1	státní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
a	a	k8xC	a
i	i	k9	i
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
ní	on	k3xPp3gFnSc2	on
objevilo	objevit	k5eAaPmAgNnS	objevit
několik	několik	k4yIc1	několik
konspiračních	konspirační	k2eAgFnPc2d1	konspirační
teorií	teorie	k1gFnPc2	teorie
<g/>
.	.	kIx.	.
</s>
