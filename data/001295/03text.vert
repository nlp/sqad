<s>
Polonium	polonium	k1gNnSc1	polonium
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Po	Po	kA	Po
<g/>
,	,	kIx,	,
lat.	lat.	k?	lat.
Polonium	polonium	k1gNnSc1	polonium
je	být	k5eAaImIp3nS	být
nestabilní	stabilní	k2eNgInSc4d1	nestabilní
radioaktivní	radioaktivní	k2eAgInSc4d1	radioaktivní
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
nejtěžší	těžký	k2eAgFnSc4d3	nejtěžší
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
chalkogenů	chalkogen	k1gInPc2	chalkogen
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
objeven	objeven	k2eAgInSc1d1	objeven
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
Marií	Maria	k1gFnSc7	Maria
Curie-Skłodowskou	Curie-Skłodowský	k2eAgFnSc7d1	Curie-Skłodowský
a	a	k8xC	a
pojmenován	pojmenovat	k5eAaPmNgMnS	pojmenovat
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
její	její	k3xOp3gFnSc2	její
vlasti	vlast	k1gFnSc2	vlast
-	-	kIx~	-
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Chemicky	chemicky	k6eAd1	chemicky
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c7	mezi
kovy	kov	k1gInPc7	kov
<g/>
.	.	kIx.	.
</s>
<s>
Polonium	polonium	k1gNnSc1	polonium
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
uran-radiové	uranadiový	k2eAgFnSc2d1	uran-radiový
<g/>
,	,	kIx,	,
neptuniové	ptuniový	k2eNgFnSc2d1	neptuniová
i	i	k8xC	i
thoriové	thoriový	k2eAgFnSc2d1	thoriová
rozpadové	rozpadový	k2eAgFnSc2d1	rozpadová
řady	řada	k1gFnSc2	řada
a	a	k8xC	a
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
uranových	uranový	k2eAgFnPc2d1	uranová
rud	ruda	k1gFnPc2	ruda
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
z	z	k7c2	z
uranové	uranový	k2eAgFnSc2d1	uranová
rudy	ruda	k1gFnSc2	ruda
-	-	kIx~	-
jáchymovského	jáchymovský	k2eAgInSc2d1	jáchymovský
smolince	smolinec	k1gInSc2	smolinec
bylo	být	k5eAaImAgNnS	být
polonium	polonium	k1gNnSc1	polonium
poprvé	poprvé	k6eAd1	poprvé
získáno	získat	k5eAaPmNgNnS	získat
jako	jako	k8xC	jako
elementární	elementární	k2eAgInSc1d1	elementární
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
koncentrace	koncentrace	k1gFnSc1	koncentrace
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
množství	množství	k1gNnSc6	množství
0,1	[number]	k4	0,1
mg	mg	kA	mg
na	na	k7c4	na
tunu	tuna	k1gFnSc4	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Uměle	uměle	k6eAd1	uměle
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
ozařováním	ozařování	k1gNnSc7	ozařování
bismutu	bismut	k1gInSc2	bismut
209	[number]	k4	209
<g/>
Bi	Bi	k1gFnSc1	Bi
neutrony	neutron	k1gInPc1	neutron
v	v	k7c6	v
jaderných	jaderný	k2eAgInPc6d1	jaderný
reaktorech	reaktor	k1gInPc6	reaktor
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
209	[number]	k4	209
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
i	i	k9	i
+	+	kIx~	+
n	n	k0	n
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
210	[number]	k4	210
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
i	i	k8xC	i
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
210	[number]	k4	210
:	:	kIx,	:
:	:	kIx,	:
P	P	kA	P
o	o	k7c4	o
+	+	kIx~	+
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
β	β	k?	β
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{{	{{	k?	{{
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
209	[number]	k4	209
<g/>
}	}	kIx)	}
<g/>
Bi	Bi	k1gFnSc2	Bi
<g/>
+	+	kIx~	+
<g/>
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
210	[number]	k4	210
<g/>
}	}	kIx)	}
<g/>
Bi	Bi	k1gFnSc1	Bi
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
210	[number]	k4	210
<g/>
}	}	kIx)	}
<g/>
Po	po	k7c6	po
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
}}	}}	k?	}}
}	}	kIx)	}
:	:	kIx,	:
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
je	být	k5eAaImIp3nS	být
nejvíce	hodně	k6eAd3	hodně
užívaným	užívaný	k2eAgInSc7d1	užívaný
izotopem	izotop	k1gInSc7	izotop
polonia	polonium	k1gNnSc2	polonium
210	[number]	k4	210
Po	Po	kA	Po
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
silným	silný	k2eAgNnSc7d1	silné
alfa	alfa	k1gNnSc7	alfa
zářičem	zářič	k1gInSc7	zářič
s	s	k7c7	s
poločasem	poločas	k1gInSc7	poločas
rozpadu	rozpad	k1gInSc2	rozpad
138,4	[number]	k4	138,4
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
různých	různý	k2eAgInPc2d1	různý
izotopů	izotop	k1gInPc2	izotop
polonia	polonium	k1gNnSc2	polonium
<g/>
,	,	kIx,	,
praktický	praktický	k2eAgInSc1d1	praktický
význam	význam	k1gInSc1	význam
však	však	k9	však
má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
již	již	k9	již
zmíněný	zmíněný	k2eAgInSc1d1	zmíněný
210	[number]	k4	210
<g/>
Po	Po	kA	Po
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
208	[number]	k4	208
<g/>
Po	Po	kA	Po
s	s	k7c7	s
poločasem	poločas	k1gInSc7	poločas
rozpadu	rozpad	k1gInSc2	rozpad
2,9	[number]	k4	2,9
let	léto	k1gNnPc2	léto
a	a	k8xC	a
209	[number]	k4	209
<g/>
Po	Po	kA	Po
s	s	k7c7	s
poločasem	poločas	k1gInSc7	poločas
125	[number]	k4	125
roků	rok	k1gInPc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Praktické	praktický	k2eAgNnSc4d1	praktické
využití	využití	k1gNnSc4	využití
nalézají	nalézat	k5eAaImIp3nP	nalézat
izotopy	izotop	k1gInPc1	izotop
polonia	polonium	k1gNnSc2	polonium
jako	jako	k8xC	jako
alfa	alfa	k1gNnSc1	alfa
zářiče	zářič	k1gInSc2	zářič
v	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
a	a	k8xC	a
při	při	k7c6	při
odstraňování	odstraňování	k1gNnSc6	odstraňování
statického	statický	k2eAgInSc2d1	statický
náboje	náboj	k1gInSc2	náboj
v	v	k7c6	v
textilním	textilní	k2eAgInSc6d1	textilní
průmyslu	průmysl	k1gInSc6	průmysl
a	a	k8xC	a
výrobě	výroba	k1gFnSc3	výroba
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zdravotního	zdravotní	k2eAgNnSc2d1	zdravotní
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
polonium	polonium	k1gNnSc4	polonium
vysoce	vysoce	k6eAd1	vysoce
rizikový	rizikový	k2eAgInSc1d1	rizikový
prvek	prvek	k1gInSc1	prvek
a	a	k8xC	a
při	při	k7c6	při
manipulaci	manipulace	k1gFnSc6	manipulace
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
dodržována	dodržován	k2eAgNnPc1d1	dodržováno
přísná	přísný	k2eAgNnPc1d1	přísné
bezpečnostní	bezpečnostní	k2eAgNnPc1d1	bezpečnostní
opatření	opatření	k1gNnPc1	opatření
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Otrava	otrava	k1gFnSc1	otrava
Alexandra	Alexandra	k1gFnSc1	Alexandra
Litviněnka	Litviněnka	k1gFnSc1	Litviněnka
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
oznámení	oznámení	k1gNnSc2	oznámení
londýnské	londýnský	k2eAgFnSc2d1	londýnská
policie	policie	k1gFnSc2	policie
byl	být	k5eAaImAgInS	být
poloniem	polonium	k1gNnSc7	polonium
210	[number]	k4	210
<g/>
Po	po	k7c6	po
otráven	otrávit	k5eAaPmNgMnS	otrávit
bývalý	bývalý	k2eAgMnSc1d1	bývalý
tajný	tajný	k2eAgMnSc1d1	tajný
agent	agent	k1gMnSc1	agent
KGB	KGB	kA	KGB
Alexandr	Alexandr	k1gMnSc1	Alexandr
Litviněnko	Litviněnka	k1gFnSc5	Litviněnka
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Polonium	polonium	k1gNnSc1	polonium
se	se	k3xPyFc4	se
také	také	k9	také
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
cigaretách	cigareta	k1gFnPc6	cigareta
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
poznatek	poznatek	k1gInSc1	poznatek
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgMnS	být
před	před	k7c7	před
veřejností	veřejnost	k1gFnSc7	veřejnost
zatajen	zatajit	k5eAaPmNgMnS	zatajit
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
škodlivá	škodlivý	k2eAgFnSc1d1	škodlivá
látka	látka	k1gFnSc1	látka
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
z	z	k7c2	z
cigaret	cigareta	k1gFnPc2	cigareta
odebrat	odebrat	k5eAaPmF	odebrat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
výrobci	výrobce	k1gMnPc1	výrobce
značek	značka	k1gFnPc2	značka
se	se	k3xPyFc4	se
bojí	bát	k5eAaImIp3nP	bát
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
cigarety	cigareta	k1gFnPc1	cigareta
méně	málo	k6eAd2	málo
nikotinu	nikotin	k1gInSc2	nikotin
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
by	by	kYmCp3nP	by
snížili	snížit	k5eAaPmAgMnP	snížit
návykovost	návykovost	k1gFnSc4	návykovost
těchto	tento	k3xDgFnPc2	tento
drog	droga	k1gFnPc2	droga
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
možnosti	možnost	k1gFnSc6	možnost
odebrání	odebrání	k1gNnSc1	odebrání
polonia	polonium	k1gNnSc2	polonium
210	[number]	k4	210
z	z	k7c2	z
cigaret	cigareta	k1gFnPc2	cigareta
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
známo	znám	k2eAgNnSc1d1	známo
již	již	k6eAd1	již
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Polonium	polonium	k1gNnSc1	polonium
zjištěné	zjištěný	k2eAgNnSc1d1	zjištěné
v	v	k7c6	v
tabáku	tabák	k1gInSc6	tabák
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
fosfátů	fosfát	k1gInPc2	fosfát
používaných	používaný	k2eAgInPc2d1	používaný
jako	jako	k8xS	jako
průmyslové	průmyslový	k2eAgNnSc1d1	průmyslové
hnojivo	hnojivo	k1gNnSc1	hnojivo
a	a	k8xC	a
stejné	stejný	k2eAgNnSc1d1	stejné
hnojiva	hnojivo	k1gNnPc1	hnojivo
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
na	na	k7c4	na
produkci	produkce	k1gFnSc4	produkce
mnoha	mnoho	k4c2	mnoho
druhů	druh	k1gInPc2	druh
zeleniny	zelenina	k1gFnSc2	zelenina
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
zemědělských	zemědělský	k2eAgInPc2d1	zemědělský
produktů	produkt	k1gInPc2	produkt
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
přímo	přímo	k6eAd1	přímo
konzumuje	konzumovat	k5eAaBmIp3nS	konzumovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
otázkou	otázka	k1gFnSc7	otázka
<g/>
,	,	kIx,	,
jaké	jaký	k3yQgNnSc1	jaký
množství	množství	k1gNnSc1	množství
polonia	polonium	k1gNnSc2	polonium
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
tabáku	tabák	k1gInSc6	tabák
skutečně	skutečně	k6eAd1	skutečně
nacházet	nacházet	k5eAaImF	nacházet
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
roční	roční	k2eAgFnSc1d1	roční
celosvětová	celosvětový	k2eAgFnSc1d1	celosvětová
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
produkce	produkce	k1gFnSc1	produkce
polonia	polonium	k1gNnSc2	polonium
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
množstvích	množství	k1gNnPc6	množství
řádově	řádově	k6eAd1	řádově
desítek	desítka	k1gFnPc2	desítka
gramů	gram	k1gInPc2	gram
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
1995	[number]	k4	1995
<g/>
-	-	kIx~	-
<g/>
2002	[number]	k4	2002
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
ne	ne	k9	ne
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
gramů	gram	k1gInPc2	gram
všech	všecek	k3xTgInPc2	všecek
izotopů	izotop	k1gInPc2	izotop
polonia	polonium	k1gNnSc2	polonium
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
