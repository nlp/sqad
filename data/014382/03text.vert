<s>
Celulární	celulární	k2eAgInSc1d1
automat	automat	k1gInSc1
</s>
<s>
Celulární	celulární	k2eAgInSc1d1
automatCelulární	automatCelulární	k2eAgInSc1d1
automat	automat	k1gInSc1
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
CA	ca	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
souhrnné	souhrnný	k2eAgNnSc1d1
označení	označení	k1gNnSc1
pro	pro	k7c4
určitý	určitý	k2eAgInSc4d1
typ	typ	k1gInSc4
fyzikálního	fyzikální	k2eAgInSc2d1
modelu	model	k1gInSc2
reálné	reálný	k2eAgFnSc2d1
situace	situace	k1gFnSc2
<g/>
,	,	kIx,
ať	ať	k8xS,k8xC
již	již	k6eAd1
v	v	k7c6
podobě	podoba	k1gFnSc6
reálného	reálný	k2eAgInSc2d1
přístroje	přístroj	k1gInSc2
či	či	k8xC
mnohem	mnohem	k6eAd1
častěji	často	k6eAd2
počítačového	počítačový	k2eAgInSc2d1
algoritmu	algoritmus	k1gInSc2
(	(	kIx(
<g/>
programu	program	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slouží	sloužit	k5eAaImIp3nS
k	k	k7c3
časové	časový	k2eAgFnSc3d1
i	i	k8xC
prostorové	prostorový	k2eAgFnSc3d1
diskrétní	diskrétní	k2eAgFnSc3d1
(	(	kIx(
<g/>
nespojité	spojitý	k2eNgFnSc3d1
<g/>
)	)	kIx)
idealizaci	idealizace	k1gFnSc3
(	(	kIx(
<g/>
ideální	ideální	k2eAgFnSc3d1
modelaci	modelace	k1gFnSc3
<g/>
)	)	kIx)
fyzikálních	fyzikální	k2eAgInPc2d1
systémů	systém	k1gInPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
hodnoty	hodnota	k1gFnPc1
veličin	veličina	k1gFnPc2
nabývají	nabývat	k5eAaImIp3nP
pouze	pouze	k6eAd1
diskrétních	diskrétní	k2eAgFnPc2d1
hodnot	hodnota	k1gFnPc2
v	v	k7c6
průběhu	průběh	k1gInSc6
času	čas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
se	se	k3xPyFc4
v	v	k7c6
teorii	teorie	k1gFnSc6
systémů	systém	k1gInPc2
<g/>
,	,	kIx,
matematice	matematika	k1gFnSc6
a	a	k8xC
teoretické	teoretický	k2eAgFnSc6d1
biologii	biologie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
První	první	k4xOgInPc4
nápady	nápad	k1gInPc4
na	na	k7c6
CA	ca	kA
se	se	k3xPyFc4
objevily	objevit	k5eAaPmAgInP
již	již	k6eAd1
ve	v	k7c6
40	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
dvacátého	dvacátý	k4xOgNnSc2
století	století	k1gNnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
John	John	k1gMnSc1
von	von	k1gInSc4
Neumann	Neumann	k1gMnSc1
snažil	snažit	k5eAaImAgMnS
navrhnout	navrhnout	k5eAaPmF
stroj	stroj	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
by	by	kYmCp3nS
kontroloval	kontrolovat	k5eAaImAgMnS
i	i	k8xC
opravoval	opravovat	k5eAaImAgMnS
sám	sám	k3xTgMnSc1
sebe	sebe	k3xPyFc4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gInSc7
cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
najít	najít	k5eAaPmF
logickou	logický	k2eAgFnSc4d1
strukturu	struktura	k1gFnSc4
sebereprodukujícího	sebereprodukující	k2eAgInSc2d1
se	se	k3xPyFc4
automatu	automat	k1gInSc2
bez	bez	k7c2
nutného	nutný	k2eAgInSc2d1
vztahu	vztah	k1gInSc2
k	k	k7c3
biologickým	biologický	k2eAgInPc3d1
procesům	proces	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
s	s	k7c7
S.	S.	kA
Ulamem	Ulam	k1gMnSc7
rozdělili	rozdělit	k5eAaPmAgMnP
celý	celý	k2eAgInSc4d1
prostor	prostor	k1gInSc4
na	na	k7c4
jednotlivé	jednotlivý	k2eAgFnPc4d1
buňky	buňka	k1gFnPc4
(	(	kIx(
<g/>
cells	cells	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
každá	každý	k3xTgFnSc1
buňka	buňka	k1gFnSc1
je	být	k5eAaImIp3nS
na	na	k7c6
začátku	začátek	k1gInSc6
charakterizována	charakterizovat	k5eAaBmNgFnS
počátečním	počáteční	k2eAgInSc7d1
stavem	stav	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
počáteční	počáteční	k2eAgInSc1d1
stav	stav	k1gInSc1
se	se	k3xPyFc4
podle	podle	k7c2
evolučního	evoluční	k2eAgNnSc2d1
pravidla	pravidlo	k1gNnSc2
mění	měnit	k5eAaImIp3nS
vždy	vždy	k6eAd1
po	po	k7c6
jednotlivých	jednotlivý	k2eAgInPc6d1
krocích	krok	k1gInPc6
<g/>
,	,	kIx,
tzv.	tzv.	kA
iteracích	iterace	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evoluční	evoluční	k2eAgInSc4d1
pravidlo	pravidlo	k1gNnSc1
nemusí	muset	k5eNaImIp3nS
být	být	k5eAaImF
stejné	stejný	k2eAgNnSc1d1
pro	pro	k7c4
všechny	všechen	k3xTgFnPc4
buňky	buňka	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
vždy	vždy	k6eAd1
funkcí	funkce	k1gFnSc7
stavů	stav	k1gInPc2
buněk	buňka	k1gFnPc2
v	v	k7c6
okolí	okolí	k1gNnSc6
buňky	buňka	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
právě	právě	k6eAd1
zkoumáme	zkoumat	k5eAaImIp1nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
padesátých	padesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
se	se	k3xPyFc4
začaly	začít	k5eAaPmAgFnP
CA	ca	kA
používat	používat	k5eAaImF
k	k	k7c3
automatickému	automatický	k2eAgNnSc3d1
zpracování	zpracování	k1gNnSc3
obrazů	obraz	k1gInPc2
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
vyvinuta	vyvinout	k5eAaPmNgFnS
speciální	speciální	k2eAgNnPc4d1
pravidla	pravidlo	k1gNnPc4
pro	pro	k7c4
úpravu	úprava	k1gFnSc4
šumu	šum	k1gInSc2
<g/>
,	,	kIx,
odhad	odhad	k1gInSc1
velikosti	velikost	k1gFnSc2
a	a	k8xC
počtu	počet	k1gInSc2
objektů	objekt	k1gInPc2
na	na	k7c6
obraze	obraz	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
mikroskopem	mikroskop	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1970	#num#	k4
našel	najít	k5eAaPmAgMnS
John	John	k1gMnSc1
Conway	Conwaa	k1gFnSc2
jednoduché	jednoduchý	k2eAgNnSc1d1
pravidlo	pravidlo	k1gNnSc1
vedoucí	vedoucí	k1gFnSc2
ke	k	k7c3
komplexnímu	komplexní	k2eAgNnSc3d1
chování	chování	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nazval	nazvat	k5eAaBmAgMnS,k5eAaPmAgMnS
je	být	k5eAaImIp3nS
Game	game	k1gInSc4
of	of	k?
Life	Life	k1gInSc1
(	(	kIx(
<g/>
více	hodně	k6eAd2
níže	nízce	k6eAd2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
sedmdesátých	sedmdesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
byl	být	k5eAaImAgInS
vytvořen	vytvořen	k2eAgInSc1d1
HPP	HPP	kA
model	model	k1gInSc1
mřížového	mřížový	k2eAgInSc2d1
plynu	plyn	k1gInSc2
trojicí	trojice	k1gFnSc7
Hardy	Harda	k1gFnSc2
<g/>
,	,	kIx,
Pomeau	Pomeaus	k1gInSc2
a	a	k8xC
de	de	k?
Pazzis	Pazzis	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
jeden	jeden	k4xCgInSc1
z	z	k7c2
prvních	první	k4xOgInPc2
CA	ca	kA
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
zachovává	zachovávat	k5eAaImIp3nS
kromě	kromě	k7c2
počtu	počet	k1gInSc2
částic	částice	k1gFnPc2
také	také	k9
hybnost	hybnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgInSc7
modelem	model	k1gInSc7
se	se	k3xPyFc4
otevřela	otevřít	k5eAaPmAgFnS
cesta	cesta	k1gFnSc1
k	k	k7c3
simulování	simulování	k1gNnSc3
pohybu	pohyb	k1gInSc2
tekutin	tekutina	k1gFnPc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
zrnitých	zrnitý	k2eAgFnPc2d1
látek	látka	k1gFnPc2
pomocí	pomocí	k7c2
CA	ca	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
osmdesátých	osmdesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
se	se	k3xPyFc4
Stephen	Stephen	k2eAgInSc1d1
Wolfram	wolfram	k1gInSc1
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
začal	začít	k5eAaPmAgInS
zabývat	zabývat	k5eAaImF
jednodimenzionálními	jednodimenzionální	k2eAgFnPc7d1
CA	ca	kA
<g/>
,	,	kIx,
u	u	k7c2
kterých	který	k3yIgInPc2,k3yRgInPc2,k3yQgInPc2
prokázal	prokázat	k5eAaPmAgInS
jejich	jejich	k3xOp3gInSc4
vztah	vztah	k1gInSc4
ke	k	k7c3
statistické	statistický	k2eAgFnSc3d1
fyzice	fyzika	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1986	#num#	k4
přišel	přijít	k5eAaPmAgInS
na	na	k7c4
svět	svět	k1gInSc4
FHP	FHP	kA
model	model	k1gInSc1
pánů	pan	k1gMnPc2
Frische	Frische	k1gNnSc2
<g/>
,	,	kIx,
Hasslachera	Hasslachero	k1gNnSc2
a	a	k8xC
Pomeaua	Pomeauum	k1gNnSc2
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
nezávisle	závisle	k6eNd1
na	na	k7c6
nich	on	k3xPp3gInPc2
objevil	objevit	k5eAaPmAgMnS
také	také	k9
Stephen	Stephen	k2eAgInSc4d1
Wolfram	wolfram	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Provedou	provést	k5eAaPmIp3nP
<g/>
-li	-li	k?
se	se	k3xPyFc4
u	u	k7c2
něj	on	k3xPp3gNnSc2
příslušné	příslušný	k2eAgInPc1d1
limity	limit	k1gInPc1
<g/>
,	,	kIx,
ukazuje	ukazovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
obdobu	obdoba	k1gFnSc4
Navierových-Stokesových	Navierových-Stokesový	k2eAgFnPc2d1
rovnic	rovnice	k1gFnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
nepopiratelný	popiratelný	k2eNgInSc1d1
důkaz	důkaz	k1gInSc1
schopnosti	schopnost	k1gFnSc2
CA	ca	kA
modelovat	modelovat	k5eAaImF
reálný	reálný	k2eAgInSc4d1
fyzikální	fyzikální	k2eAgInSc4d1
problém	problém	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
</s>
<s>
Celulární	celulární	k2eAgInSc1d1
automat	automat	k1gInSc1
je	být	k5eAaImIp3nS
dynamický	dynamický	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
,	,	kIx,
diskrétní	diskrétní	k2eAgInSc1d1
v	v	k7c6
hodnotách	hodnota	k1gFnPc6
<g/>
,	,	kIx,
prostoru	prostor	k1gInSc6
i	i	k8xC
čase	čas	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
tvořen	tvořit	k5eAaImNgInS
pravidelnou	pravidelný	k2eAgFnSc7d1
strukturou	struktura	k1gFnSc7
buněk	buňka	k1gFnPc2
v	v	k7c6
N-rozměrném	N-rozměrný	k2eAgInSc6d1
prostoru	prostor	k1gInSc6
(	(	kIx(
<g/>
nejčastěji	často	k6eAd3
je	být	k5eAaImIp3nS
N	N	kA
<g/>
=	=	kIx~
<g/>
2	#num#	k4
<g/>
,	,	kIx,
tzv.	tzv.	kA
2D	2D	k4
CA	ca	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
buňky	buňka	k1gFnPc1
tvoří	tvořit	k5eAaImIp3nP
čtvercovou	čtvercový	k2eAgFnSc4d1
mřížku	mřížka	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každá	každý	k3xTgFnSc1
buňka	buňka	k1gFnSc1
může	moct	k5eAaImIp3nS
nabývat	nabývat	k5eAaImF
jeden	jeden	k4xCgInSc4
z	z	k7c2
K	K	kA
možných	možný	k2eAgInPc2d1
stavů	stav	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
jde	jít	k5eAaImIp3nS
pouze	pouze	k6eAd1
o	o	k7c4
dva	dva	k4xCgInPc4
stavy	stav	k1gInPc4
<g/>
:	:	kIx,
0	#num#	k4
<g/>
-mrtvá	-mrtvý	k2eAgFnSc1d1
buňka	buňka	k1gFnSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
-živá	-živý	k2eAgFnSc1d1
buňka	buňka	k1gFnSc1
<g/>
;	;	kIx,
v	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
se	se	k3xPyFc4
občas	občas	k6eAd1
stav	stav	k1gInSc1
1	#num#	k4
označuje	označovat	k5eAaImIp3nS
jako	jako	k9
buňka	buňka	k1gFnSc1
a	a	k8xC
0	#num#	k4
jako	jako	k8xC,k8xS
prázdné	prázdný	k2eAgNnSc4d1
políčko	políčko	k1gNnSc4
(	(	kIx(
<g/>
mřížky	mřížka	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Hodnoty	hodnota	k1gFnPc1
stavů	stav	k1gInPc2
buněk	buňka	k1gFnPc2
v	v	k7c6
dalším	další	k2eAgInSc6d1
časovém	časový	k2eAgInSc6d1
kroku	krok	k1gInSc6
(	(	kIx(
<g/>
v	v	k7c6
následující	následující	k2eAgFnSc6d1
generaci	generace	k1gFnSc6
<g/>
)	)	kIx)
se	se	k3xPyFc4
vypočtou	vypočíst	k5eAaPmIp3nP
synchronně	synchronně	k6eAd1
na	na	k7c6
základě	základ	k1gInSc6
lokální	lokální	k2eAgFnSc2d1
přechodové	přechodový	k2eAgFnSc2d1
funkce	funkce	k1gFnSc2
(	(	kIx(
<g/>
stejné	stejná	k1gFnSc2
pro	pro	k7c4
všechny	všechen	k3xTgFnPc4
buňky	buňka	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Argumenty	argument	k1gInPc1
této	tento	k3xDgFnSc2
funkce	funkce	k1gFnSc2
jsou	být	k5eAaImIp3nP
aktuální	aktuální	k2eAgFnPc1d1
hodnoty	hodnota	k1gFnPc1
stavů	stav	k1gInPc2
vyšetřované	vyšetřovaný	k2eAgFnSc2d1
buňky	buňka	k1gFnSc2
a	a	k8xC
všech	všecek	k3xTgMnPc2
sousedů	soused	k1gMnPc2
(	(	kIx(
<g/>
buněk	buňka	k1gFnPc2
v	v	k7c6
jejím	její	k3xOp3gNnSc6
okolí	okolí	k1gNnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
případě	případ	k1gInSc6
1D	1D	k4
CA	ca	kA
je	být	k5eAaImIp3nS
okolí	okolí	k1gNnSc1
charakterizováno	charakterizovat	k5eAaBmNgNnS
tzv.	tzv.	kA
poloměrem	poloměr	k1gInSc7
<g/>
,	,	kIx,
tj.	tj.	kA
počtem	počet	k1gInSc7
sousedů	soused	k1gMnPc2
po	po	k7c6
obou	dva	k4xCgFnPc6
stranách	strana	k1gFnPc6
vyšetřované	vyšetřovaný	k2eAgFnPc1d1
buňky	buňka	k1gFnPc1
<g/>
;	;	kIx,
v	v	k7c6
případě	případ	k1gInSc6
2D	2D	k4
CA	ca	kA
tvoří	tvořit	k5eAaImIp3nP
okolí	okolí	k1gNnSc4
čtyři	čtyři	k4xCgFnPc1
přilehlé	přilehlý	k2eAgFnPc1d1
buňky	buňka	k1gFnPc1
(	(	kIx(
<g/>
tzv.	tzv.	kA
neumanovské	umanovský	k2eNgNnSc1d1
okolí	okolí	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
nebo	nebo	k8xC
se	se	k3xPyFc4
do	do	k7c2
okolí	okolí	k1gNnSc2
zařadí	zařadit	k5eAaPmIp3nS
i	i	k9
čtyři	čtyři	k4xCgMnPc1
další	další	k2eAgMnPc1d1
sousedi	soused	k1gMnPc1
<g/>
,	,	kIx,
dotýkajících	dotýkající	k2eAgFnPc2d1
se	se	k3xPyFc4
vyšetřované	vyšetřovaný	k2eAgFnSc2d1
buňky	buňka	k1gFnSc2
jen	jen	k9
v	v	k7c6
rozích	roh	k1gInPc6
(	(	kIx(
<g/>
tzv.	tzv.	kA
moorovské	moorovský	k2eAgNnSc1d1
okolí	okolí	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpravidla	zpravidla	k6eAd1
se	se	k3xPyFc4
očekává	očekávat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
struktura	struktura	k1gFnSc1
buněk	buňka	k1gFnPc2
je	být	k5eAaImIp3nS
nekonečná	konečný	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
praktických	praktický	k2eAgFnPc6d1
realizacích	realizace	k1gFnPc6
se	se	k3xPyFc4
buď	buď	k8xC
předpokládají	předpokládat	k5eAaImIp3nP
okrajové	okrajový	k2eAgFnPc1d1
buňky	buňka	k1gFnPc1
identicky	identicky	k6eAd1
nulové	nulový	k2eAgFnPc1d1
(	(	kIx(
<g/>
prázdné	prázdný	k2eAgFnPc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
jsou	být	k5eAaImIp3nP
okraje	okraj	k1gInPc1
„	„	k?
<g/>
propojeny	propojen	k2eAgInPc1d1
<g/>
“	“	k?
a	a	k8xC
tvoří	tvořit	k5eAaImIp3nS
v	v	k7c6
případě	případ	k1gInSc6
1D	1D	k4
smyčku	smyčka	k1gFnSc4
a	a	k8xC
v	v	k7c6
případě	případ	k1gInSc6
2D	2D	k4
anuloid	anuloid	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
z	z	k7c2
K	K	kA
možných	možný	k2eAgInPc2d1
stavů	stav	k1gInPc2
jsou	být	k5eAaImIp3nP
označovány	označovat	k5eAaImNgInP
za	za	k7c2
„	„	k?
<g/>
klidové	klidový	k2eAgFnSc2d1
<g/>
“	“	k?
<g/>
;	;	kIx,
když	když	k8xS
buňka	buňka	k1gFnSc1
v	v	k7c6
klidovém	klidový	k2eAgInSc6d1
stavu	stav	k1gInSc6
má	mít	k5eAaImIp3nS
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
okolí	okolí	k1gNnSc6
také	také	k9
jenom	jenom	k9
buňky	buňka	k1gFnPc1
v	v	k7c6
klidovém	klidový	k2eAgInSc6d1
stavu	stav	k1gInSc6
<g/>
,	,	kIx,
potom	potom	k6eAd1
se	se	k3xPyFc4
hodnota	hodnota	k1gFnSc1
jejího	její	k3xOp3gInSc2
stavu	stav	k1gInSc2
v	v	k7c6
další	další	k2eAgFnSc6d1
generaci	generace	k1gFnSc6
nemění	měnit	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Někdy	někdy	k6eAd1
je	být	k5eAaImIp3nS
účelná	účelný	k2eAgFnSc1d1
širší	široký	k2eAgFnSc1d2
koncepce	koncepce	k1gFnSc1
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
jsou	být	k5eAaImIp3nP
pro	pro	k7c4
CA	ca	kA
charakteristické	charakteristický	k2eAgFnPc4d1
tři	tři	k4xCgFnPc4
klíčové	klíčový	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
<g/>
:	:	kIx,
</s>
<s>
paralelismus	paralelismus	k1gInSc1
(	(	kIx(
<g/>
výpočet	výpočet	k1gInSc1
nových	nový	k2eAgFnPc2d1
hodnot	hodnota	k1gFnPc2
všech	všecek	k3xTgInPc2
stavů	stav	k1gInPc2
probíhá	probíhat	k5eAaImIp3nS
současně	současně	k6eAd1
<g/>
,	,	kIx,
na	na	k7c6
běžných	běžný	k2eAgInPc6d1
sériových	sériový	k2eAgInPc6d1
počítačích	počítač	k1gInPc6
se	se	k3xPyFc4
musí	muset	k5eAaImIp3nS
tento	tento	k3xDgInSc4
postup	postup	k1gInSc4
simulovat	simulovat	k5eAaImF
<g/>
)	)	kIx)
</s>
<s>
lokalita	lokalita	k1gFnSc1
(	(	kIx(
<g/>
nový	nový	k2eAgInSc1d1
stav	stav	k1gInSc1
prvku	prvek	k1gInSc2
závisí	záviset	k5eAaImIp3nS
jen	jen	k9
na	na	k7c6
jeho	jeho	k3xOp3gInSc6
původním	původní	k2eAgInSc6d1
stavu	stav	k1gInSc6
a	a	k8xC
na	na	k7c6
původních	původní	k2eAgInPc6d1
stavech	stav	k1gInPc6
prvků	prvek	k1gInPc2
z	z	k7c2
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
okolí	okolí	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
homogenita	homogenita	k1gFnSc1
(	(	kIx(
<g/>
pro	pro	k7c4
všechny	všechen	k3xTgInPc4
prvky	prvek	k1gInPc4
platí	platit	k5eAaImIp3nS
stejná	stejný	k2eAgFnSc1d1
lokální	lokální	k2eAgFnSc1d1
přechodová	přechodový	k2eAgFnSc1d1
funkce	funkce	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Nevyžaduje	vyžadovat	k5eNaImIp3nS
se	se	k3xPyFc4
při	při	k7c6
tom	ten	k3xDgNnSc6
nutně	nutně	k6eAd1
pravidelná	pravidelný	k2eAgFnSc1d1
prostorová	prostorový	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
(	(	kIx(
<g/>
proto	proto	k8xC
byl	být	k5eAaImAgMnS
v	v	k7c6
uvedené	uvedený	k2eAgFnSc6d1
charakteristice	charakteristika	k1gFnSc6
použit	použít	k5eAaPmNgInS
všeobecnější	všeobecný	k2eAgInSc1d2
pojem	pojem	k1gInSc1
„	„	k?
<g/>
prvek	prvek	k1gInSc1
<g/>
“	“	k?
místo	místo	k7c2
„	„	k?
<g/>
buňka	buňka	k1gFnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Význam	význam	k1gInSc1
ve	v	k7c6
fyzice	fyzika	k1gFnSc6
</s>
<s>
Pomocí	pomocí	k7c2
jednoduchých	jednoduchý	k2eAgNnPc2d1
pravidel	pravidlo	k1gNnPc2
lze	lze	k6eAd1
vytvořit	vytvořit	k5eAaPmF
CA	ca	kA
se	s	k7c7
složitým	složitý	k2eAgNnSc7d1
a	a	k8xC
dynamickým	dynamický	k2eAgNnSc7d1
chováním	chování	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
CA	ca	kA
mohou	moct	k5eAaImIp3nP
představovat	představovat	k5eAaImF
vymyšlený	vymyšlený	k2eAgInSc4d1
mikroskopický	mikroskopický	k2eAgInSc4d1
svět	svět	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
při	při	k7c6
pohledu	pohled	k1gInSc6
z	z	k7c2
větší	veliký	k2eAgFnSc2d2
výšky	výška	k1gFnSc2
odpovídá	odpovídat	k5eAaImIp3nS
fyzikální	fyzikální	k2eAgFnSc3d1
realitě	realita	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existují	existovat	k5eAaImIp3nP
dva	dva	k4xCgInPc4
základní	základní	k2eAgInPc4d1
přístupy	přístup	k1gInPc4
ke	k	k7c3
studiu	studio	k1gNnSc3
fyzikálních	fyzikální	k2eAgInPc2d1
dějů	děj	k1gInPc2
<g/>
,	,	kIx,
mikroskopický	mikroskopický	k2eAgInSc4d1
a	a	k8xC
makroskopický	makroskopický	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
mikroskopického	mikroskopický	k2eAgInSc2d1
pohledu	pohled	k1gInSc2
jsou	být	k5eAaImIp3nP
interakce	interakce	k1gFnPc1
velice	velice	k6eAd1
komplikované	komplikovaný	k2eAgFnPc1d1
a	a	k8xC
mnohdy	mnohdy	k6eAd1
je	být	k5eAaImIp3nS
k	k	k7c3
důkladnému	důkladný	k2eAgInSc3d1
popisu	popis	k1gInSc3
nezbytné	nezbytný	k2eAgNnSc1d1,k2eNgNnSc1d1
užití	užití	k1gNnSc1
kvantové	kvantový	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studujeme	studovat	k5eAaImIp1nP
<g/>
-li	-li	k?
děj	děj	k1gInSc4
makroskopicky	makroskopicky	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
vlastnosti	vlastnost	k1gFnPc4
systému	systém	k1gInSc2
zkoumány	zkoumat	k5eAaImNgInP
pomocí	pomocí	k7c2
středních	střední	k2eAgFnPc2d1
hodnot	hodnota	k1gFnPc2
příslušných	příslušný	k2eAgFnPc2d1
veličin	veličina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledné	výsledný	k2eAgNnSc1d1
chování	chování	k1gNnSc1
je	být	k5eAaImIp3nS
spjato	spjat	k2eAgNnSc1d1
s	s	k7c7
charakteristickými	charakteristický	k2eAgInPc7d1
rysy	rys	k1gInPc7
interakcí	interakce	k1gFnPc2
odehrávajících	odehrávající	k2eAgFnPc2d1
se	se	k3xPyFc4
na	na	k7c6
mikroskopické	mikroskopický	k2eAgFnSc6d1
škále	škála	k1gFnSc6
<g/>
,	,	kIx,
jenomže	jenomže	k8xC
tento	tento	k3xDgInSc1
vztah	vztah	k1gInSc1
se	se	k3xPyFc4
při	při	k7c6
provedení	provedení	k1gNnSc6
středování	středování	k1gNnSc2
ztrácí	ztrácet	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
CA	ca	kA
jsou	být	k5eAaImIp3nP
jedním	jeden	k4xCgInSc7
z	z	k7c2
pokusů	pokus	k1gInPc2
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
tuto	tento	k3xDgFnSc4
nepříjemnost	nepříjemnost	k1gFnSc4
odstranit	odstranit	k5eAaPmF
<g/>
,	,	kIx,
neboť	neboť	k8xC
zachování	zachování	k1gNnSc4
představy	představa	k1gFnSc2
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
se	se	k3xPyFc4
uvnitř	uvnitř	k7c2
daného	daný	k2eAgInSc2d1
materiálu	materiál	k1gInSc2
děje	dít	k5eAaImIp3nS
a	a	k8xC
jak	jak	k8xC,k8xS
se	se	k3xPyFc4
tyto	tento	k3xDgFnPc1
nepatrné	nepatrný	k2eAgFnPc1d1,k2eNgFnPc1d1
změny	změna	k1gFnPc1
projevují	projevovat	k5eAaImIp3nP
vůči	vůči	k7c3
okolí	okolí	k1gNnSc3
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
pro	pro	k7c4
fyziku	fyzika	k1gFnSc4
velmi	velmi	k6eAd1
důležité	důležitý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příkladem	příklad	k1gInSc7
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
studium	studium	k1gNnSc4
magnetických	magnetický	k2eAgFnPc2d1
vlastností	vlastnost	k1gFnPc2
feromagnetických	feromagnetický	k2eAgFnPc2d1
látek	látka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Makroskopickou	makroskopický	k2eAgFnSc7d1
charakteristikou	charakteristika	k1gFnSc7
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
kritická	kritický	k2eAgFnSc1d1
(	(	kIx(
<g/>
Curieova	Curieův	k2eAgFnSc1d1
<g/>
)	)	kIx)
teplota	teplota	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
má	mít	k5eAaImIp3nS
z	z	k7c2
mikroskopického	mikroskopický	k2eAgInSc2d1
pohledu	pohled	k1gInSc2
přímý	přímý	k2eAgInSc4d1
vztah	vztah	k1gInSc4
s	s	k7c7
rozdělením	rozdělení	k1gNnSc7
elektronů	elektron	k1gInPc2
v	v	k7c6
atomech	atom	k1gInPc6
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
lze	lze	k6eAd1
precizně	precizně	k6eAd1
studovat	studovat	k5eAaImF
právě	právě	k9
pomocí	pomocí	k7c2
kvantové	kvantový	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Celulární	celulární	k2eAgInSc1d1
automat	automat	k1gInSc1
jako	jako	k8xS,k8xC
jednoduchý	jednoduchý	k2eAgInSc1d1
dynamický	dynamický	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
Ve	v	k7c6
fyzice	fyzika	k1gFnSc6
jsme	být	k5eAaImIp1nP
zvyklí	zvyklý	k2eAgMnPc1d1
popisovat	popisovat	k5eAaImF
časový	časový	k2eAgInSc4d1
vývoj	vývoj	k1gInSc4
částic	částice	k1gFnPc2
pomocí	pomocí	k7c2
diferenciálních	diferenciální	k2eAgFnPc2d1
rovnic	rovnice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
vůbec	vůbec	k9
řešení	řešení	k1gNnSc1
existuje	existovat	k5eAaImIp3nS
<g/>
,	,	kIx,
bývá	bývat	k5eAaImIp3nS
často	často	k6eAd1
velmi	velmi	k6eAd1
citlivé	citlivý	k2eAgInPc1d1
na	na	k7c4
počáteční	počáteční	k2eAgFnPc4d1
podmínky	podmínka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studium	studium	k1gNnSc1
CA	ca	kA
nabízí	nabízet	k5eAaImIp3nS
jiný	jiný	k2eAgInSc4d1
přístup	přístup	k1gInSc4
ke	k	k7c3
studiu	studio	k1gNnSc3
dynamických	dynamický	k2eAgInPc2d1
procesů	proces	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc7
velká	velký	k2eAgFnSc1d1
výhoda	výhoda	k1gFnSc1
tkví	tkvět	k5eAaImIp3nS
v	v	k7c6
jednoduchosti	jednoduchost	k1gFnSc6
<g/>
,	,	kIx,
díky	díky	k7c3
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
jimi	on	k3xPp3gInPc7
lze	lze	k6eAd1
analyzovat	analyzovat	k5eAaImF
dynamické	dynamický	k2eAgInPc4d1
</s>
<s>
systémy	systém	k1gInPc1
a	a	k8xC
navíc	navíc	k6eAd1
se	se	k3xPyFc4
jejich	jejich	k3xOp3gNnSc1
numerické	numerický	k2eAgNnSc1d1
řešení	řešení	k1gNnSc1
obejde	obejít	k5eAaPmIp3nS
bez	bez	k7c2
zjednodušujících	zjednodušující	k2eAgFnPc2d1
aproximací	aproximace	k1gFnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
vede	vést	k5eAaImIp3nS
k	k	k7c3
přesnému	přesný	k2eAgInSc3d1
výsledku	výsledek	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
CA	ca	kA
se	se	k3xPyFc4
setkáváme	setkávat	k5eAaImIp1nP
se	s	k7c7
dvěma	dva	k4xCgInPc7
možnými	možný	k2eAgInPc7d1
přístupy	přístup	k1gInPc7
k	k	k7c3
řešení	řešení	k1gNnSc3
(	(	kIx(
<g/>
nejen	nejen	k6eAd1
<g/>
)	)	kIx)
fyzikálních	fyzikální	k2eAgInPc2d1
problémů	problém	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
prvé	prvý	k4xOgFnPc4
máme	mít	k5eAaImIp1nP
CA	ca	kA
pravidlo	pravidlo	k1gNnSc4
a	a	k8xC
chceme	chtít	k5eAaImIp1nP
předpovědět	předpovědět	k5eAaPmF
jeho	jeho	k3xOp3gNnSc4
chování	chování	k1gNnSc4
a	a	k8xC
tudíž	tudíž	k8xC
i	i	k9
potenciální	potenciální	k2eAgNnSc1d1
využití	využití	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
druhé	druhý	k4xOgFnSc6
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
obrácený	obrácený	k2eAgInSc4d1
problém	problém	k1gInSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
víme	vědět	k5eAaImIp1nP
<g/>
,	,	kIx,
jaké	jaký	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
požadavky	požadavek	k1gInPc4
na	na	k7c4
náš	náš	k3xOp1gInSc4
CA	ca	kA
klade	klást	k5eAaImIp3nS
realita	realita	k1gFnSc1
a	a	k8xC
hledáme	hledat	k5eAaImIp1nP
odpovídající	odpovídající	k2eAgNnSc4d1
pravidlo	pravidlo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
obou	dva	k4xCgInPc6
případech	případ	k1gInPc6
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
obtížně	obtížně	k6eAd1
řešitelný	řešitelný	k2eAgInSc4d1
problém	problém	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Druhy	druh	k1gInPc1
celulárních	celulární	k2eAgInPc2d1
automatů	automat	k1gInPc2
</s>
<s>
Jednodimenzionální	jednodimenzionální	k2eAgInSc1d1
automat	automat	k1gInSc1
</s>
<s>
Je	být	k5eAaImIp3nS
nejjednodušší	jednoduchý	k2eAgFnSc1d3
<g/>
,	,	kIx,
ale	ale	k8xC
nikoliv	nikoliv	k9
nezajímavý	zajímavý	k2eNgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
jednodimenzionální	jednodimenzionální	k2eAgInSc4d1
automat	automat	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
stavy	stav	k1gInPc1
buněk	buňka	k1gFnPc2
nabývají	nabývat	k5eAaImIp3nP
pouze	pouze	k6eAd1
dvou	dva	k4xCgFnPc2
hodnot	hodnota	k1gFnPc2
0	#num#	k4
nebo	nebo	k8xC
1	#num#	k4
a	a	k8xC
okolím	okolí	k1gNnSc7
chápeme	chápat	k5eAaImIp1nP
dvě	dva	k4xCgFnPc1
sousední	sousední	k2eAgFnPc1d1
buňky	buňka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdy	někdy	k6eAd1
se	se	k3xPyFc4
tomuto	tento	k3xDgInSc3
automatu	automat	k1gInSc3
výstižně	výstižně	k6eAd1
říká	říkat	k5eAaImIp3nS
toy	toy	k?
model	model	k1gInSc1
<g/>
,	,	kIx,
protože	protože	k8xS
</s>
<s>
je	být	k5eAaImIp3nS
velice	velice	k6eAd1
jednoduše	jednoduše	k6eAd1
programovatelný	programovatelný	k2eAgInSc1d1
a	a	k8xC
pro	pro	k7c4
mnoho	mnoho	k4c4
lidí	člověk	k1gMnPc2
je	být	k5eAaImIp3nS
první	první	k4xOgMnSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
si	se	k3xPyFc3
sami	sám	k3xTgMnPc1
vytvoří	vytvořit	k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s>
Von	von	k1gInSc1
Neumannův	Neumannův	k2eAgInSc1d1
CA	ca	kA
</s>
<s>
Von	von	k1gInSc1
Neumann	Neumann	k1gMnSc1
nebyl	být	k5eNaImAgMnS
spokojen	spokojen	k2eAgMnSc1d1
s	s	k7c7
kinematickým	kinematický	k2eAgInSc7d1
modelem	model	k1gInSc7
automatu	automat	k1gInSc2
kvůli	kvůli	k7c3
tzv.	tzv.	kA
„	„	k?
<g/>
black-box	black-box	k1gInSc1
<g/>
“	“	k?
problému	problém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
řešením	řešení	k1gNnSc7
mu	on	k3xPp3gMnSc3
pomohl	pomoct	k5eAaPmAgMnS
jeho	jeho	k3xOp3gMnSc1
spolupracovník	spolupracovník	k1gMnSc1
Stanisław	Stanisław	k1gMnSc1
Ulam	ulámat	k5eAaPmRp2nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navrhl	navrhnout	k5eAaPmAgMnS
prostředí	prostředí	k1gNnSc3
tvořené	tvořený	k2eAgNnSc1d1
pravidelnou	pravidelný	k2eAgFnSc7d1
mřížkou	mřížka	k1gFnSc7
-	-	kIx~
jakousi	jakýsi	k3yIgFnSc7
šachovnicí	šachovnice	k1gFnSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
každé	každý	k3xTgNnSc4
políčko	políčko	k1gNnSc4
představuje	představovat	k5eAaImIp3nS
jednu	jeden	k4xCgFnSc4
buňku	buňka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každá	každý	k3xTgFnSc1
buňka	buňka	k1gFnSc1
představuje	představovat	k5eAaImIp3nS
konečný	konečný	k2eAgInSc4d1
automat	automat	k1gInSc4
<g/>
,	,	kIx,
pracující	pracující	k2eAgInPc4d1
se	se	k3xPyFc4
shodnou	shodný	k2eAgFnSc7d1
množinou	množina	k1gFnSc7
pravidel	pravidlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Množinu	množina	k1gFnSc4
takových	takový	k3xDgFnPc2
buněk	buňka	k1gFnPc2
můžeme	moct	k5eAaImIp1nP
pak	pak	k6eAd1
považovat	považovat	k5eAaImF
za	za	k7c4
organismus	organismus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Neumannův	Neumannův	k2eAgInSc1d1
CA	ca	kA
byl	být	k5eAaImAgInS
sestaven	sestavit	k5eAaPmNgInS
asi	asi	k9
z	z	k7c2
200	#num#	k4
tisíc	tisíc	k4xCgInPc2
buněk	buňka	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
mohly	moct	k5eAaImAgFnP
mít	mít	k5eAaImF
29	#num#	k4
stavů	stav	k1gInPc2
<g/>
,	,	kIx,
CA	ca	kA
tvořilo	tvořit	k5eAaImAgNnS
tělo	tělo	k1gNnSc1
asi	asi	k9
80	#num#	k4
<g/>
x	x	k?
<g/>
400	#num#	k4
buněk	buňka	k1gFnPc2
(	(	kIx(
<g/>
bylo	být	k5eAaImAgNnS
rozčleněno	rozčlenit	k5eAaPmNgNnS
na	na	k7c4
tři	tři	k4xCgFnPc4
složky	složka	k1gFnPc4
A	A	kA
<g/>
,	,	kIx,
B	B	kA
<g/>
,	,	kIx,
C	C	kA
-	-	kIx~
továrnu	továrna	k1gFnSc4
<g/>
,	,	kIx,
duplikátor	duplikátor	k1gInSc4
a	a	k8xC
počítač	počítač	k1gInSc4
<g/>
)	)	kIx)
a	a	k8xC
dlouhý	dlouhý	k2eAgInSc1d1
výrůstek	výrůstek	k1gInSc1
z	z	k7c2
asi	asi	k9
150	#num#	k4
tisíc	tisíc	k4xCgInPc2
buněk	buňka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Game	game	k1gInSc1
of	of	k?
life	lifat	k5eAaPmIp3nS
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Hra	hra	k1gFnSc1
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Hra	hra	k1gFnSc1
života	život	k1gInSc2
je	být	k5eAaImIp3nS
dvourozměrný	dvourozměrný	k2eAgInSc1d1
<g/>
,	,	kIx,
dvoustavový	dvoustavový	k2eAgInSc1d1
celulární	celulární	k2eAgInSc1d1
automat	automat	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
svým	svůj	k3xOyFgNnSc7
fungováním	fungování	k1gNnSc7
připomíná	připomínat	k5eAaImIp3nS
vývoj	vývoj	k1gInSc4
společenství	společenství	k1gNnSc2
živých	živý	k2eAgInPc2d1
organismů	organismus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Coddův	Coddův	k2eAgInSc1d1
automat	automat	k1gInSc1
</s>
<s>
Pracoval	pracovat	k5eAaImAgMnS
s	s	k7c7
osmi	osm	k4xCc7
stavy	stav	k1gInPc7
a	a	k8xC
s	s	k7c7
neumanovským	umanovský	k2eNgNnSc7d1
okolím	okolí	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Čtyři	čtyři	k4xCgInPc1
stavy	stav	k1gInPc1
byly	být	k5eAaImAgInP
strukturální	strukturální	k2eAgInPc1d1
<g/>
:	:	kIx,
</s>
<s>
0	#num#	k4
-	-	kIx~
prázdná	prázdný	k2eAgFnSc1d1
buňka	buňka	k1gFnSc1
</s>
<s>
1	#num#	k4
-	-	kIx~
jádro	jádro	k1gNnSc1
signálové	signálový	k2eAgFnSc2d1
cesty	cesta	k1gFnSc2
</s>
<s>
2	#num#	k4
-	-	kIx~
obal	obal	k1gInSc4
signálové	signálový	k2eAgFnSc2d1
cesty	cesta	k1gFnSc2
</s>
<s>
3	#num#	k4
-	-	kIx~
speciální	speciální	k2eAgNnSc1d1
použití	použití	k1gNnSc1
<g/>
,	,	kIx,
např.	např.	kA
pro	pro	k7c4
hradlo	hradlo	k1gNnSc4
</s>
<s>
4,5	4,5	k4
<g/>
,6	,6	k4
<g/>
,7	,7	k4
-	-	kIx~
byly	být	k5eAaImAgFnP
signálové	signálový	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Základním	základní	k2eAgInSc7d1
prvkem	prvek	k1gInSc7
byla	být	k5eAaImAgFnS
dvojice	dvojice	k1gFnSc1
signálové	signálový	k2eAgFnSc2d1
buňky	buňka	k1gFnSc2
v	v	k7c6
kombinaci	kombinace	k1gFnSc6
s	s	k7c7
prázdnou	prázdný	k2eAgFnSc7d1
buňkou	buňka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
dvojice	dvojice	k1gFnSc1
se	se	k3xPyFc4
v	v	k7c6
každé	každý	k3xTgFnSc6
generaci	generace	k1gFnSc6
posune	posunout	k5eAaPmIp3nS
o	o	k7c4
jednu	jeden	k4xCgFnSc4
pozici	pozice	k1gFnSc4
po	po	k7c6
signálové	signálový	k2eAgFnSc6d1
cestě	cesta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Coddův	Coddův	k2eAgInSc1d1
automat	automat	k1gInSc1
byl	být	k5eAaImAgInS
teoreticky	teoreticky	k6eAd1
schopen	schopen	k2eAgMnSc1d1
emulovat	emulovat	k5eAaImF
Turingův	Turingův	k2eAgInSc4d1
stroj	stroj	k1gInSc4
a	a	k8xC
též	též	k9
vytvořit	vytvořit	k5eAaPmF
svou	svůj	k3xOyFgFnSc4
vlastní	vlastní	k2eAgFnSc4d1
kopii	kopie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Langtonovy	Langtonův	k2eAgFnPc1d1
Q-smyčky	Q-smyčka	k1gFnPc1
</s>
<s>
Langton	Langton	k1gInSc1
sestrojil	sestrojit	k5eAaPmAgInS
na	na	k7c6
bázi	báze	k1gFnSc6
Coddova	Coddův	k2eAgInSc2d1
modelu	model	k1gInSc2
nesrovnatelně	srovnatelně	k6eNd1
jednodušší	jednoduchý	k2eAgFnSc4d2
verzi	verze	k1gFnSc4
samoreprodukujícího	samoreprodukující	k2eAgInSc2d1
se	s	k7c7
2D	2D	k4
CA	ca	kA
(	(	kIx(
<g/>
chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
ovšem	ovšem	k9
emulace	emulace	k1gFnSc1
Turingova	Turingův	k2eAgInSc2d1
stroje	stroj	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tzv.	tzv.	kA
Q-smyčky	Q-smyček	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Langtonovy	Langtonův	k2eAgFnPc1d1
Q-smyčky	Q-smyčka	k1gFnPc1
jsou	být	k5eAaImIp3nP
názornou	názorný	k2eAgFnSc7d1
ukázkou	ukázka	k1gFnSc7
dvojí	dvojit	k5eAaImIp3nS
funkce	funkce	k1gFnSc1
informace	informace	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
neinterpretovaná	interpretovaný	k2eNgFnSc1d1
informace	informace	k1gFnSc1
(	(	kIx(
<g/>
genotyp	genotyp	k1gInSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
kopíruje	kopírovat	k5eAaImIp3nS
do	do	k7c2
potomka	potomek	k1gMnSc2
</s>
<s>
interpretovaná	interpretovaný	k2eAgFnSc1d1
informace	informace	k1gFnSc1
(	(	kIx(
<g/>
fenotyp	fenotyp	k1gInSc1
<g/>
)	)	kIx)
charakterizuje	charakterizovat	k5eAaBmIp3nS
tvar	tvar	k1gInSc4
a	a	k8xC
chování	chování	k1gNnSc4
jedince	jedinec	k1gMnSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
S.	S.	kA
Ulam	ulámat	k5eAaPmRp2nS
<g/>
:	:	kIx,
Random	Random	k1gInSc1
processes	processes	k1gInSc1
and	and	k?
transformations	transformations	k1gInSc1
<g/>
,	,	kIx,
Proc	proc	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Int	Int	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Congr	Congra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Math	Math	k1gInSc1
<g/>
.	.	kIx.
2	#num#	k4
</s>
<s>
(	(	kIx(
<g/>
1952	#num#	k4
<g/>
)	)	kIx)
<g/>
,264	,264	k4
<g/>
–	–	k?
<g/>
27	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
K.	K.	kA
Preston	Preston	k1gInSc1
a	a	k8xC
M.	M.	kA
Duﬀ	Duﬀ	k1gMnSc1
<g/>
:	:	kIx,
Modern	Modern	k1gMnSc1
Cellular	Cellular	k1gMnSc1
Automata	Automa	k1gNnPc1
<g/>
:	:	kIx,
Theory	Theor	k1gInPc1
and	and	k?
Applications	Applications	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Plenum	Plenum	k1gNnSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
↑	↑	k?
.	.	kIx.
</s>
<s desamb="1">
Hardy	Harda	k1gMnSc2
<g/>
,	,	kIx,
Y.	Y.	kA
Pomeau	Pomeaus	k1gInSc2
a	a	k8xC
O.	O.	kA
de	de	k?
Pazzis	Pazzis	k1gInSc1
<g/>
:	:	kIx,
Molecular	Molecular	k1gInSc1
Dynamics	Dynamics	k1gInSc1
of	of	k?
a	a	k8xC
classical	classicat	k5eAaPmAgInS
lattice	lattice	k1gFnPc4
gas	gas	k?
<g/>
:	:	kIx,
Transport	transport	k1gInSc1
properties	properties	k1gInSc1
and	and	k?
time	timat	k5eAaPmIp3nS
correlation	correlation	k1gInSc4
functions	functionsa	k1gFnPc2
<g/>
,	,	kIx,
Phys	Physa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rev	Rev	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
13	#num#	k4
(	(	kIx(
<g/>
1976	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1949	#num#	k4
<g/>
–	–	k?
<g/>
60	#num#	k4
<g/>
,	,	kIx,
<g/>
↑	↑	k?
M.	M.	kA
Garden	Gardna	k1gFnPc2
<g/>
:	:	kIx,
The	The	k1gFnSc1
fantastic	fantastice	k1gFnPc2
combinations	combinationsa	k1gFnPc2
of	of	k?
John	John	k1gMnSc1
Conway	Conwaa	k1gFnSc2
<g/>
‘	‘	k?
<g/>
s	s	k7c7
new	new	k?
solitaire	solitair	k1gInSc5
game	game	k1gInSc1
life	life	k1gFnSc1
<g/>
,	,	kIx,
Scientiﬁ	Scientiﬁ	k1gFnSc1
American	American	k1gInSc1
4	#num#	k4
(	(	kIx(
<g/>
1970	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
120	#num#	k4
<g/>
–	–	k?
<g/>
127	#num#	k4
<g/>
,	,	kIx,
S.	S.	kA
Wolfram	wolfram	k1gInSc1
<g/>
:	:	kIx,
Theory	Theora	k1gFnPc1
and	and	k?
Application	Application	k1gInSc1
of	of	k?
Cellular	Cellular	k1gInSc1
Automata	Automa	k1gNnPc1
<g/>
,	,	kIx,
World	World	k1gInSc1
Scientiﬁ	Scientiﬁ	k1gInSc1
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
↑	↑	k?
U.	U.	kA
Frisch	Frisch	k1gInSc1
<g/>
,	,	kIx,
B.	B.	kA
<g/>
Hasslacher	Hasslachra	k1gFnPc2
a	a	k8xC
Y.	Y.	kA
Pomeau	Pomeaus	k1gInSc2
<g/>
:	:	kIx,
Lattice	Lattice	k1gFnSc1
<g/>
–	–	k?
<g/>
gas	gas	k?
automata	automa	k1gNnPc1
for	forum	k1gNnPc2
the	the	k?
Navier	Navier	k1gMnSc1
<g/>
–	–	k?
</s>
<s>
Stokes	Stokes	k1gInSc1
equation	equation	k1gInSc1
<g/>
,	,	kIx,
Phys	Phys	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rev	Rev	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lett	Lett	k1gInSc1
<g/>
.	.	kIx.
56	#num#	k4
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1505	#num#	k4
<g/>
–	–	k?
<g/>
32	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
celulární	celulární	k2eAgInSc4d1
automat	automat	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Martina	Martina	k1gFnSc1
Husáková	Husáková	k1gFnSc1
<g/>
:	:	kIx,
Celulární	celulární	k2eAgFnSc1d1
automaty	automat	k1gInPc7
</s>
<s>
Implementace	implementace	k1gFnSc1
hry	hra	k1gFnSc2
Life	Lif	k1gFnSc2
v	v	k7c6
C	C	kA
<g/>
#	#	kIx~
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4190671-8	4190671-8	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
13873	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85021692	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85021692	#num#	k4
</s>
