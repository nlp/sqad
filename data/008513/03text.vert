<p>
<s>
Estonština	estonština	k1gFnSc1	estonština
(	(	kIx(	(
<g/>
estonsky	estonsky	k6eAd1	estonsky
eesti	eest	k5eAaBmF	eest
keel	keel	k1gInSc4	keel
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jazyk	jazyk	k1gInSc4	jazyk
baltofinské	baltofinský	k2eAgFnSc2d1	baltofinský
větve	větev	k1gFnSc2	větev
ugrofinských	ugrofinský	k2eAgInPc2d1	ugrofinský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Estonština	estonština	k1gFnSc1	estonština
je	být	k5eAaImIp3nS	být
příbuzná	příbuzný	k2eAgFnSc1d1	příbuzná
s	s	k7c7	s
finštinou	finština	k1gFnSc7	finština
a	a	k8xC	a
vzdáleně	vzdáleně	k6eAd1	vzdáleně
také	také	k9	také
s	s	k7c7	s
maďarštinou	maďarština	k1gFnSc7	maďarština
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
ugrofinských	ugrofinský	k2eAgInPc2d1	ugrofinský
jazyků	jazyk	k1gInPc2	jazyk
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
několik	několik	k4yIc4	několik
malých	malý	k2eAgInPc2d1	malý
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
livonština	livonština	k1gFnSc1	livonština
<g/>
,	,	kIx,	,
karelština	karelština	k1gFnSc1	karelština
<g/>
,	,	kIx,	,
laponština	laponština	k1gFnSc1	laponština
<g/>
,	,	kIx,	,
marijština	marijština	k1gFnSc1	marijština
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Estonština	estonština	k1gFnSc1	estonština
není	být	k5eNaImIp3nS	být
příbuzná	příbuzný	k2eAgFnSc1d1	příbuzná
s	s	k7c7	s
jazyky	jazyk	k1gInPc7	jazyk
sousedních	sousední	k2eAgInPc2d1	sousední
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
ruštinou	ruština	k1gFnSc7	ruština
<g/>
,	,	kIx,	,
lotyštinou	lotyština	k1gFnSc7	lotyština
<g/>
,	,	kIx,	,
švédštinou	švédština	k1gFnSc7	švédština
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
indoevropské	indoevropský	k2eAgFnSc2d1	indoevropská
jazykové	jazykový	k2eAgFnSc2d1	jazyková
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Estonsky	estonsky	k6eAd1	estonsky
mluví	mluvit	k5eAaImIp3nS	mluvit
přibližně	přibližně	k6eAd1	přibližně
1,15	[number]	k4	1,15
milionu	milion	k4xCgInSc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
většina	většina	k1gFnSc1	většina
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1	[number]	k4	1
100	[number]	k4	100
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Estonsku	Estonsko	k1gNnSc6	Estonsko
<g/>
.	.	kIx.	.
</s>
<s>
Estonština	estonština	k1gFnSc1	estonština
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgInSc7d1	jediný
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
v	v	k7c6	v
Estonsku	Estonsko	k1gNnSc6	Estonsko
a	a	k8xC	a
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
dvaceti	dvacet	k4xCc2	dvacet
tří	tři	k4xCgInPc2	tři
úředních	úřední	k2eAgMnPc2d1	úřední
jazyků	jazyk	k1gInPc2	jazyk
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
charakteristických	charakteristický	k2eAgInPc2d1	charakteristický
rysů	rys	k1gInPc2	rys
estonštiny	estonština	k1gFnSc2	estonština
je	být	k5eAaImIp3nS	být
trojí	trojit	k5eAaImIp3nS	trojit
délka	délka	k1gFnSc1	délka
hlásek	hlásek	k1gInSc1	hlásek
<g/>
:	:	kIx,	:
krátká	krátký	k2eAgNnPc1d1	krátké
<g/>
,	,	kIx,	,
dlouhá	dlouhý	k2eAgNnPc1d1	dlouhé
a	a	k8xC	a
předlouhá	předlouhý	k2eAgNnPc1d1	předlouhé
<g/>
.	.	kIx.	.
</s>
<s>
Těchto	tento	k3xDgFnPc2	tento
různých	různý	k2eAgFnPc2d1	různá
délek	délka	k1gFnPc2	délka
nabývají	nabývat	k5eAaImIp3nP	nabývat
nejen	nejen	k6eAd1	nejen
samohlásky	samohláska	k1gFnPc1	samohláska
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
souhlásky	souhláska	k1gFnSc2	souhláska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
estonštiny	estonština	k1gFnSc2	estonština
==	==	k?	==
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgInSc1d3	nejstarší
doložený	doložený	k2eAgInSc1d1	doložený
výskyt	výskyt	k1gInSc1	výskyt
estonského	estonský	k2eAgNnSc2d1	Estonské
slova	slovo	k1gNnSc2	slovo
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
do	do	k7c2	do
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
kněz	kněz	k1gMnSc1	kněz
Aethicus	Aethicus	k1gMnSc1	Aethicus
Ister	Ister	k1gInSc4	Ister
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
Cosmographia	Cosmographia	k1gFnSc1	Cosmographia
zmínil	zmínit	k5eAaPmAgInS	zmínit
ostrov	ostrov	k1gInSc1	ostrov
Taraconta	Taracont	k1gInSc2	Taracont
(	(	kIx(	(
<g/>
Tharaconta	Tharaconta	k1gFnSc1	Tharaconta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
buď	buď	k8xC	buď
o	o	k7c4	o
označení	označení	k1gNnSc4	označení
celého	celý	k2eAgNnSc2d1	celé
Estonska	Estonsko	k1gNnSc2	Estonsko
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gInSc2	jeho
největšího	veliký	k2eAgInSc2d3	veliký
ostrova	ostrov	k1gInSc2	ostrov
Saaremaa	Saarema	k1gInSc2	Saarema
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Taraconta	Taracont	k1gInSc2	Taracont
lze	lze	k6eAd1	lze
vyložit	vyložit	k5eAaPmF	vyložit
jako	jako	k8xS	jako
spojení	spojení	k1gNnSc1	spojení
slov	slovo	k1gNnPc2	slovo
Taara	Taar	k1gInSc2	Taar
a	a	k8xC	a
kond	konda	k1gFnPc2	konda
<g/>
.	.	kIx.	.
</s>
<s>
Taara	Taara	k6eAd1	Taara
byl	být	k5eAaImAgInS	být
zřejmě	zřejmě	k6eAd1	zřejmě
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgMnPc2d3	nejdůležitější
bohů	bůh	k1gMnPc2	bůh
starých	starý	k2eAgMnPc2d1	starý
Estonců	Estonec	k1gMnPc2	Estonec
a	a	k8xC	a
přípona	přípona	k1gFnSc1	přípona
-kond	onda	k1gFnPc2	-konda
(	(	kIx(	(
<g/>
staroestonsky	staroestonsky	k6eAd1	staroestonsky
-konda	ond	k1gMnSc2	-kond
<g/>
)	)	kIx)	)
v	v	k7c6	v
estonštině	estonština	k1gFnSc6	estonština
označuje	označovat	k5eAaImIp3nS	označovat
skupinu	skupina	k1gFnSc4	skupina
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
např.	např.	kA	např.
perekond	perekond	k1gInSc1	perekond
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
rodina	rodina	k1gFnSc1	rodina
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
územní	územní	k2eAgFnSc4d1	územní
jednotku	jednotka	k1gFnSc4	jednotka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
maakond	maakond	k1gInSc1	maakond
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
provincie	provincie	k1gFnSc1	provincie
<g/>
,	,	kIx,	,
kraj	kraj	k1gInSc1	kraj
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Taraconta	Taraconta	k1gFnSc1	Taraconta
<g/>
"	"	kIx"	"
tak	tak	k6eAd1	tak
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
označením	označení	k1gNnSc7	označení
Estonců	Estonec	k1gMnPc2	Estonec
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
uctívačů	uctívač	k1gMnPc2	uctívač
boha	bůh	k1gMnSc2	bůh
Taary	Taara	k1gMnSc2	Taara
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
estonského	estonský	k2eAgNnSc2d1	Estonské
území	území	k1gNnSc2	území
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
země	země	k1gFnSc1	země
boha	bůh	k1gMnSc2	bůh
Taary	Taara	k1gMnSc2	Taara
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Více	hodně	k6eAd2	hodně
písemných	písemný	k2eAgInPc2d1	písemný
záznamů	záznam	k1gInPc2	záznam
o	o	k7c6	o
vývoji	vývoj	k1gInSc6	vývoj
estonštiny	estonština	k1gFnSc2	estonština
se	se	k3xPyFc4	se
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
z	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
latinsky	latinsky	k6eAd1	latinsky
psaná	psaný	k2eAgFnSc1d1	psaná
kronika	kronika	k1gFnSc1	kronika
Heinrici	Heinrice	k1gFnSc4	Heinrice
Chronicon	Chronicon	k1gNnSc4	Chronicon
Livoniae	Livoniae	k1gInSc1	Livoniae
popisující	popisující	k2eAgFnSc2d1	popisující
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
německých	německý	k2eAgMnPc2d1	německý
a	a	k8xC	a
skandinávských	skandinávský	k2eAgMnPc2d1	skandinávský
křižáků	křižák	k1gMnPc2	křižák
proti	proti	k7c3	proti
Estoncům	Estonec	k1gMnPc3	Estonec
již	již	k6eAd1	již
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
slova	slovo	k1gNnPc4	slovo
a	a	k8xC	a
části	část	k1gFnPc4	část
vět	věta	k1gFnPc2	věta
v	v	k7c6	v
estonštině	estonština	k1gFnSc6	estonština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
křížových	křížový	k2eAgFnPc2d1	křížová
výprav	výprava	k1gFnPc2	výprava
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Estonska	Estonsko	k1gNnSc2	Estonsko
a	a	k8xC	a
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
usadila	usadit	k5eAaPmAgFnS	usadit
dolnoněmecká	dolnoněmecký	k2eAgFnSc1d1	dolnoněmecký
šlechta	šlechta	k1gFnSc1	šlechta
a	a	k8xC	a
měšťané	měšťan	k1gMnPc1	měšťan
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
následujících	následující	k2eAgNnPc2d1	následující
700	[number]	k4	700
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
území	území	k1gNnSc6	území
střídala	střídat	k5eAaImAgFnS	střídat
období	období	k1gNnSc3	období
různé	různý	k2eAgFnSc2d1	různá
cizí	cizí	k2eAgFnSc2d1	cizí
nadvlády	nadvláda	k1gFnSc2	nadvláda
(	(	kIx(	(
<g/>
dánské	dánský	k2eAgFnSc2d1	dánská
<g/>
,	,	kIx,	,
polské	polský	k2eAgFnSc2d1	polská
<g/>
,	,	kIx,	,
švédské	švédský	k2eAgFnSc2d1	švédská
<g/>
,	,	kIx,	,
ruské	ruský	k2eAgFnSc2d1	ruská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejsilněji	silně	k6eAd3	silně
estonštinu	estonština	k1gFnSc4	estonština
ovlivnily	ovlivnit	k5eAaPmAgInP	ovlivnit
dolní	dolní	k2eAgFnSc1d1	dolní
němčina	němčina	k1gFnSc1	němčina
a	a	k8xC	a
horní	horní	k2eAgFnSc1d1	horní
němčina	němčina	k1gFnSc1	němčina
a	a	k8xC	a
také	také	k9	také
místní	místní	k2eAgInPc1d1	místní
dialekty	dialekt	k1gInPc1	dialekt
baltské	baltský	k2eAgFnSc2d1	Baltská
němčiny	němčina	k1gFnSc2	němčina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prvním	první	k4xOgInSc7	první
souvislým	souvislý	k2eAgInSc7d1	souvislý
estonským	estonský	k2eAgInSc7d1	estonský
textem	text	k1gInSc7	text
je	být	k5eAaImIp3nS	být
rukopis	rukopis	k1gInSc1	rukopis
Kullamaa	Kullamaum	k1gNnSc2	Kullamaum
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1524	[number]	k4	1524
<g/>
–	–	k?	–
<g/>
1528	[number]	k4	1528
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jedenáct	jedenáct	k4xCc4	jedenáct
dochovaných	dochovaný	k2eAgFnPc2d1	dochovaná
stránek	stránka	k1gFnPc2	stránka
luteránského	luteránský	k2eAgInSc2d1	luteránský
katechismu	katechismus	k1gInSc2	katechismus
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
dvě	dva	k4xCgFnPc1	dva
modlitby	modlitba	k1gFnPc1	modlitba
–	–	k?	–
Otčenáš	otčenáš	k1gInSc1	otčenáš
a	a	k8xC	a
Zdrávas	zdrávas	k1gInSc1	zdrávas
Maria	Maria	k1gFnSc1	Maria
–	–	k?	–
a	a	k8xC	a
vyznání	vyznání	k1gNnSc2	vyznání
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnPc1	první
estonské	estonský	k2eAgFnPc1d1	Estonská
gramatiky	gramatika	k1gFnPc1	gramatika
a	a	k8xC	a
slovníky	slovník	k1gInPc1	slovník
byly	být	k5eAaImAgInP	být
sestaveny	sestavit	k5eAaPmNgInP	sestavit
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
a	a	k8xC	a
latině	latina	k1gFnSc6	latina
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
první	první	k4xOgFnSc2	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1637	[number]	k4	1637
německý	německý	k2eAgMnSc1d1	německý
farář	farář	k1gMnSc1	farář
Heinrich	Heinrich	k1gMnSc1	Heinrich
Stahl	Stahl	k1gMnSc1	Stahl
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
se	se	k3xPyFc4	se
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
také	také	k9	také
literatura	literatura	k1gFnSc1	literatura
v	v	k7c6	v
estonštině	estonština	k1gFnSc6	estonština
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
až	až	k9	až
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
o	o	k7c4	o
status	status	k1gInSc4	status
spisovné	spisovný	k2eAgFnSc2d1	spisovná
estonštiny	estonština	k1gFnSc2	estonština
bojovaly	bojovat	k5eAaImAgInP	bojovat
dvě	dva	k4xCgFnPc4	dva
jazykové	jazykový	k2eAgFnPc4d1	jazyková
varianty	varianta	k1gFnPc4	varianta
<g/>
,	,	kIx,	,
severní	severní	k2eAgNnSc4d1	severní
(	(	kIx(	(
<g/>
okolí	okolí	k1gNnSc4	okolí
Tallinnu	Tallinn	k1gInSc2	Tallinn
<g/>
)	)	kIx)	)
a	a	k8xC	a
jižní	jižní	k2eAgNnSc4d1	jižní
(	(	kIx(	(
<g/>
okolí	okolí	k1gNnSc4	okolí
Tartu	Tart	k1gInSc2	Tart
<g/>
)	)	kIx)	)
estonština	estonština	k1gFnSc1	estonština
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1739	[number]	k4	1739
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
první	první	k4xOgInSc1	první
úplný	úplný	k2eAgInSc1d1	úplný
překlad	překlad	k1gInSc1	překlad
Bible	bible	k1gFnSc2	bible
do	do	k7c2	do
estonštiny	estonština	k1gFnSc2	estonština
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
severní	severní	k2eAgFnSc4d1	severní
estonštinu	estonština	k1gFnSc4	estonština
<g/>
,	,	kIx,	,
získala	získat	k5eAaPmAgFnS	získat
tato	tento	k3xDgFnSc1	tento
jazyková	jazykový	k2eAgFnSc1d1	jazyková
varianta	varianta	k1gFnSc1	varianta
dominantní	dominantní	k2eAgNnSc4d1	dominantní
postavení	postavení	k1gNnSc4	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
důvodem	důvod	k1gInSc7	důvod
úpadku	úpadek	k1gInSc2	úpadek
jižní	jižní	k2eAgFnSc2d1	jižní
estonštiny	estonština	k1gFnSc2	estonština
bylo	být	k5eAaImAgNnS	být
vypálení	vypálení	k1gNnSc1	vypálení
Tartu	Tart	k1gInSc2	Tart
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1708	[number]	k4	1708
během	během	k7c2	během
války	válka	k1gFnSc2	válka
mezi	mezi	k7c4	mezi
Švédy	Švéd	k1gMnPc4	Švéd
a	a	k8xC	a
Rusy	Rus	k1gMnPc4	Rus
a	a	k8xC	a
následná	následný	k2eAgFnSc1d1	následná
deportace	deportace	k1gFnSc1	deportace
jeho	jeho	k3xOp3gMnPc2	jeho
obyvatel	obyvatel	k1gMnPc2	obyvatel
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
prudkému	prudký	k2eAgInSc3d1	prudký
rozvoji	rozvoj	k1gInSc3	rozvoj
estonštiny	estonština	k1gFnSc2	estonština
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
z	z	k7c2	z
jazyka	jazyk	k1gInSc2	jazyk
rolníků	rolník	k1gMnPc2	rolník
stala	stát	k5eAaPmAgFnS	stát
jazykem	jazyk	k1gMnSc7	jazyk
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Estonština	estonština	k1gFnSc1	estonština
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
používat	používat	k5eAaImF	používat
také	také	k9	také
ve	v	k7c6	v
vědě	věda	k1gFnSc6	věda
<g/>
.	.	kIx.	.
</s>
<s>
Karl	Karl	k1gMnSc1	Karl
August	August	k1gMnSc1	August
Hermann	Hermann	k1gMnSc1	Hermann
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1884	[number]	k4	1884
vydal	vydat	k5eAaPmAgMnS	vydat
první	první	k4xOgFnSc4	první
gramatiku	gramatika	k1gFnSc4	gramatika
estonštiny	estonština	k1gFnSc2	estonština
psanou	psaný	k2eAgFnSc7d1	psaná
estonsky	estonsky	k6eAd1	estonsky
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
hrálo	hrát	k5eAaImAgNnS	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
při	při	k7c6	při
její	její	k3xOp3gFnSc6	její
standardizaci	standardizace	k1gFnSc6	standardizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
prvních	první	k4xOgNnPc2	první
desetiletí	desetiletí	k1gNnPc2	desetiletí
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
si	se	k3xPyFc3	se
estonské	estonský	k2eAgNnSc4d1	Estonské
vzdělané	vzdělaný	k2eAgFnPc4d1	vzdělaná
vrstvy	vrstva	k1gFnPc4	vrstva
určily	určit	k5eAaPmAgInP	určit
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
rozvoj	rozvoj	k1gInSc4	rozvoj
estonštiny	estonština	k1gFnSc2	estonština
jako	jako	k8xC	jako
evropského	evropský	k2eAgInSc2d1	evropský
kulturního	kulturní	k2eAgInSc2d1	kulturní
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
úsilí	úsilí	k1gNnSc6	úsilí
měl	mít	k5eAaImAgMnS	mít
Johannes	Johannes	k1gMnSc1	Johannes
Aavik	Aavik	k1gMnSc1	Aavik
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
literární	literární	k2eAgInSc1d1	literární
jazyk	jazyk	k1gInSc1	jazyk
byl	být	k5eAaImAgInS	být
bohatší	bohatý	k2eAgFnSc4d2	bohatší
a	a	k8xC	a
krásnější	krásný	k2eAgFnSc4d2	krásnější
<g/>
.	.	kIx.	.
</s>
<s>
Vycházel	vycházet	k5eAaImAgInS	vycházet
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
z	z	k7c2	z
finštiny	finština	k1gFnSc2	finština
a	a	k8xC	a
estonských	estonský	k2eAgNnPc2d1	Estonské
nářečí	nářečí	k1gNnPc2	nářečí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vytvářel	vytvářet	k5eAaImAgMnS	vytvářet
také	také	k6eAd1	také
umělá	umělý	k2eAgNnPc4d1	umělé
slova	slovo	k1gNnPc4	slovo
a	a	k8xC	a
tvary	tvar	k1gInPc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
významným	významný	k2eAgMnSc7d1	významný
průkopníkem	průkopník	k1gMnSc7	průkopník
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
jazyka	jazyk	k1gInSc2	jazyk
byl	být	k5eAaImAgInS	být
Johannes	Johannes	k1gMnSc1	Johannes
Voldemar	Voldemar	k1gMnSc1	Voldemar
Veski	Vesk	k1gMnSc3	Vesk
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
zaměřil	zaměřit	k5eAaPmAgMnS	zaměřit
na	na	k7c4	na
standardizaci	standardizace	k1gFnSc4	standardizace
terminologie	terminologie	k1gFnSc2	terminologie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
největším	veliký	k2eAgInSc7d3	veliký
přínosem	přínos	k1gInSc7	přínos
je	být	k5eAaImIp3nS	být
standardizace	standardizace	k1gFnSc1	standardizace
přípon	přípona	k1gFnPc2	přípona
<g/>
,	,	kIx,	,
kterými	který	k3yIgFnPc7	který
se	se	k3xPyFc4	se
odvozují	odvozovat	k5eAaImIp3nP	odvozovat
estonská	estonský	k2eAgNnPc1d1	Estonské
slova	slovo	k1gNnPc1	slovo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
při	při	k7c6	při
utváření	utváření	k1gNnSc6	utváření
spisovného	spisovný	k2eAgInSc2d1	spisovný
jazyka	jazyk	k1gInSc2	jazyk
měly	mít	k5eAaImAgInP	mít
také	také	k9	také
normativní	normativní	k2eAgInPc1d1	normativní
slovníky	slovník	k1gInPc1	slovník
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
takový	takový	k3xDgInSc1	takový
slovník	slovník	k1gInSc1	slovník
estonštiny	estonština	k1gFnSc2	estonština
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
sovětské	sovětský	k2eAgFnSc2d1	sovětská
okupace	okupace	k1gFnSc2	okupace
v	v	k7c6	v
letech	let	k1gInPc6	let
1940	[number]	k4	1940
<g/>
–	–	k?	–
<g/>
1991	[number]	k4	1991
se	se	k3xPyFc4	se
standardizace	standardizace	k1gFnSc1	standardizace
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
přísné	přísný	k2eAgNnSc4d1	přísné
dodržování	dodržování	k1gNnSc4	dodržování
pravidel	pravidlo	k1gNnPc2	pravidlo
staly	stát	k5eAaPmAgFnP	stát
způsobem	způsob	k1gInSc7	způsob
národního	národní	k2eAgInSc2d1	národní
odporu	odpor	k1gInSc2	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Používání	používání	k1gNnSc1	používání
estonštiny	estonština	k1gFnSc2	estonština
bylo	být	k5eAaImAgNnS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejsilnějších	silný	k2eAgInPc2d3	nejsilnější
projevů	projev	k1gInPc2	projev
národní	národní	k2eAgFnSc2d1	národní
identity	identita	k1gFnSc2	identita
<g/>
.	.	kIx.	.
</s>
<s>
Estonština	estonština	k1gFnSc1	estonština
přečkala	přečkat	k5eAaPmAgFnS	přečkat
rusifikaci	rusifikace	k1gFnSc3	rusifikace
také	také	k9	také
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
sovětské	sovětský	k2eAgInPc1d1	sovětský
úřady	úřad	k1gInPc1	úřad
nezakázaly	zakázat	k5eNaPmAgFnP	zakázat
její	její	k3xOp3gNnSc4	její
používání	používání	k1gNnSc4	používání
ve	v	k7c6	v
vědě	věda	k1gFnSc6	věda
<g/>
,	,	kIx,	,
veřejném	veřejný	k2eAgInSc6d1	veřejný
životě	život	k1gInSc6	život
ani	ani	k8xC	ani
ve	v	k7c6	v
školství	školství	k1gNnSc6	školství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
uvolnění	uvolnění	k1gNnSc3	uvolnění
jazykových	jazykový	k2eAgFnPc2d1	jazyková
norem	norma	k1gFnPc2	norma
a	a	k8xC	a
začínají	začínat	k5eAaImIp3nP	začínat
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
objevovat	objevovat	k5eAaImF	objevovat
různé	různý	k2eAgInPc4d1	různý
sociolekty	sociolekt	k1gInPc4	sociolekt
a	a	k8xC	a
jazykové	jazykový	k2eAgNnSc4d1	jazykové
varianty	varianta	k1gFnPc4	varianta
estonštiny	estonština	k1gFnSc2	estonština
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
spisovná	spisovný	k2eAgFnSc1d1	spisovná
estonština	estonština	k1gFnSc1	estonština
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
nářečích	nářečí	k1gNnPc6	nářečí
severní	severní	k2eAgFnSc2d1	severní
estonštiny	estonština	k1gFnSc2	estonština
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
se	se	k3xPyFc4	se
na	na	k7c4	na
území	území	k1gNnSc4	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Estonska	Estonsko	k1gNnSc2	Estonsko
mluvilo	mluvit	k5eAaImAgNnS	mluvit
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
jižní	jižní	k2eAgFnSc1d1	jižní
estonština	estonština	k1gFnSc1	estonština
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
estonštiny	estonština	k1gFnSc2	estonština
se	se	k3xPyFc4	se
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
několik	několik	k4yIc1	několik
nářečí	nářečí	k1gNnPc2	nářečí
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
foneticky	foneticky	k6eAd1	foneticky
a	a	k8xC	a
gramaticky	gramaticky	k6eAd1	gramaticky
odlišná	odlišný	k2eAgFnSc1d1	odlišná
od	od	k7c2	od
spisovné	spisovný	k2eAgFnSc2d1	spisovná
estonštiny	estonština	k1gFnSc2	estonština
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
však	však	k9	však
začalo	začít	k5eAaPmAgNnS	začít
obrození	obrození	k1gNnSc1	obrození
jižní	jižní	k2eAgFnSc2d1	jižní
estonštiny	estonština	k1gFnSc2	estonština
na	na	k7c6	na
základě	základ	k1gInSc6	základ
nářečí	nářečí	k1gNnSc2	nářečí
Võ	Võ	k1gFnSc2	Võ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2004	[number]	k4	2004
se	se	k3xPyFc4	se
estonština	estonština	k1gFnSc1	estonština
stala	stát	k5eAaPmAgFnS	stát
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
tehdy	tehdy	k6eAd1	tehdy
dvaceti	dvacet	k4xCc2	dvacet
úředních	úřední	k2eAgInPc2d1	úřední
jazyků	jazyk	k1gInPc2	jazyk
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Abeceda	abeceda	k1gFnSc1	abeceda
==	==	k?	==
</s>
</p>
<p>
<s>
Estonština	estonština	k1gFnSc1	estonština
používá	používat	k5eAaImIp3nS	používat
upravenou	upravený	k2eAgFnSc4d1	upravená
latinku	latinka	k1gFnSc4	latinka
<g/>
.	.	kIx.	.
</s>
<s>
Abeceda	abeceda	k1gFnSc1	abeceda
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
32	[number]	k4	32
písmen	písmeno	k1gNnPc2	písmeno
<g/>
,	,	kIx,	,
přesně	přesně	k6eAd1	přesně
v	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Písmena	písmeno	k1gNnPc4	písmeno
C	C	kA	C
<g/>
,	,	kIx,	,
F	F	kA	F
<g/>
,	,	kIx,	,
X	X	kA	X
<g/>
,	,	kIx,	,
Y	Y	kA	Y
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
cizích	cizí	k2eAgNnPc6d1	cizí
slovech	slovo	k1gNnPc6	slovo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výslovnost	výslovnost	k1gFnSc1	výslovnost
==	==	k?	==
</s>
</p>
<p>
<s>
Estonština	estonština	k1gFnSc1	estonština
používá	používat	k5eAaImIp3nS	používat
fonematický	fonematický	k2eAgInSc4d1	fonematický
a	a	k8xC	a
morfematický	morfematický	k2eAgInSc4d1	morfematický
pravopis	pravopis	k1gInSc4	pravopis
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
čeština	čeština	k1gFnSc1	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gFnSc1	její
výslovnost	výslovnost	k1gFnSc1	výslovnost
relativně	relativně	k6eAd1	relativně
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
"	"	kIx"	"
<g/>
co	co	k3yRnSc1	co
vidíme	vidět	k5eAaImIp1nP	vidět
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc4	ten
vyslovujeme	vyslovovat	k5eAaImIp1nP	vyslovovat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Psaná	psaný	k2eAgFnSc1d1	psaná
podoba	podoba	k1gFnSc1	podoba
nicméně	nicméně	k8xC	nicméně
nerozlišuje	rozlišovat	k5eNaImIp3nS	rozlišovat
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
a	a	k8xC	a
předlouhou	předlouhý	k2eAgFnSc4d1	předlouhá
výslovnost	výslovnost	k1gFnSc4	výslovnost
hlásek	hláska	k1gFnPc2	hláska
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
také	také	k9	také
označena	označen	k2eAgFnSc1d1	označena
palatalizace	palatalizace	k1gFnSc1	palatalizace
(	(	kIx(	(
<g/>
měkčení	měkčení	k1gNnSc1	měkčení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
psané	psaný	k2eAgInPc1d1	psaný
tvary	tvar	k1gInPc1	tvar
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
píší	psát	k5eAaImIp3nP	psát
historickým	historický	k2eAgInSc7d1	historický
pravopisem	pravopis	k1gInSc7	pravopis
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
výslovnost	výslovnost	k1gFnSc1	výslovnost
je	být	k5eAaImIp3nS	být
jiná	jiný	k2eAgFnSc1d1	jiná
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
Estonců	Estonec	k1gMnPc2	Estonec
například	například	k6eAd1	například
nevyslovuje	vyslovovat	k5eNaImIp3nS	vyslovovat
počáteční	počáteční	k2eAgFnSc4d1	počáteční
hlásku	hláska	k1gFnSc4	hláska
"	"	kIx"	"
<g/>
h	h	k?	h
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přízvuk	přízvuk	k1gInSc1	přízvuk
v	v	k7c6	v
estonštině	estonština	k1gFnSc6	estonština
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
několika	několik	k4yIc7	několik
málo	málo	k6eAd1	málo
výjimkami	výjimka	k1gFnPc7	výjimka
na	na	k7c6	na
první	první	k4xOgFnSc6	první
slabice	slabika	k1gFnSc6	slabika
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
častý	častý	k2eAgInSc1d1	častý
je	být	k5eAaImIp3nS	být
vedlejší	vedlejší	k2eAgInSc1d1	vedlejší
přízvuk	přízvuk	k1gInSc1	přízvuk
na	na	k7c6	na
lichých	lichý	k2eAgFnPc6d1	lichá
nekoncových	koncový	k2eNgFnPc6d1	koncový
slabikách	slabika	k1gFnPc6	slabika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Samohlásky	samohláska	k1gFnSc2	samohláska
===	===	k?	===
</s>
</p>
<p>
<s>
Samohlásky	samohláska	k1gFnPc1	samohláska
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
estonštině	estonština	k1gFnSc6	estonština
velmi	velmi	k6eAd1	velmi
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gMnPc2	on
devět	devět	k4xCc1	devět
(	(	kIx(	(
<g/>
u	u	k7c2	u
<g/>
,	,	kIx,	,
o	o	k0	o
<g/>
,	,	kIx,	,
a	a	k8xC	a
<g/>
,	,	kIx,	,
õ	õ	k?	õ
<g/>
,	,	kIx,	,
ü	ü	k?	ü
<g/>
,	,	kIx,	,
ö	ö	k?	ö
<g/>
,	,	kIx,	,
ä	ä	k?	ä
<g/>
,	,	kIx,	,
e	e	k0	e
<g/>
,	,	kIx,	,
i	i	k9	i
<g/>
)	)	kIx)	)
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
celkem	celkem	k6eAd1	celkem
36	[number]	k4	36
dvojhlásek	dvojhláska	k1gFnPc2	dvojhláska
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
slova	slovo	k1gNnPc1	slovo
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
pouze	pouze	k6eAd1	pouze
samohlásky	samohláska	k1gFnPc1	samohláska
<g/>
,	,	kIx,	,
např.	např.	kA	např.
öö	öö	k?	öö
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
noc	noc	k1gFnSc4	noc
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ei	ei	k?	ei
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
ne	ne	k9	ne
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
ugrofinských	ugrofinský	k2eAgInPc2d1	ugrofinský
jazyků	jazyk	k1gInPc2	jazyk
spisovná	spisovný	k2eAgFnSc1d1	spisovná
estonština	estonština	k1gFnSc1	estonština
ani	ani	k8xC	ani
většina	většina	k1gFnSc1	většina
estonských	estonský	k2eAgNnPc2d1	Estonské
nářečí	nářečí	k1gNnPc2	nářečí
nezná	znát	k5eNaImIp3nS	znát
pravidlo	pravidlo	k1gNnSc1	pravidlo
vokálové	vokálový	k2eAgFnSc2d1	vokálová
harmonie	harmonie	k1gFnSc2	harmonie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tohoto	tento	k3xDgNnSc2	tento
pravidla	pravidlo	k1gNnSc2	pravidlo
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
ve	v	k7c6	v
slově	slovo	k1gNnSc6	slovo
měly	mít	k5eAaImAgFnP	mít
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
buď	buď	k8xC	buď
jen	jen	k9	jen
předopatrové	předopatrový	k2eAgFnPc1d1	předopatrový
samohlásky	samohláska	k1gFnPc1	samohláska
(	(	kIx(	(
<g/>
ä	ä	k?	ä
<g/>
,	,	kIx,	,
ö	ö	k?	ö
<g/>
,	,	kIx,	,
ü	ü	k?	ü
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
zadopatrové	zadopatrový	k2eAgFnPc4d1	zadopatrový
samohlásky	samohláska	k1gFnPc4	samohláska
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
o	o	k0	o
<g/>
,	,	kIx,	,
u	u	k7c2	u
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pravidlo	pravidlo	k1gNnSc1	pravidlo
vokálové	vokálový	k2eAgFnSc2d1	vokálová
harmonie	harmonie	k1gFnSc2	harmonie
nicméně	nicméně	k8xC	nicméně
platí	platit	k5eAaImIp3nS	platit
v	v	k7c6	v
nářečí	nářečí	k1gNnSc6	nářečí
võ	võ	k?	võ
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
küla	küla	k6eAd1	küla
ve	v	k7c6	v
spisovné	spisovný	k2eAgFnSc6d1	spisovná
estonštině	estonština	k1gFnSc6	estonština
a	a	k8xC	a
külä	külä	k?	külä
ve	v	k7c6	v
võ	võ	k?	võ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Souhlásky	souhláska	k1gFnSc2	souhláska
===	===	k?	===
</s>
</p>
<p>
<s>
Souhláskové	souhláskový	k2eAgInPc1d1	souhláskový
shluky	shluk	k1gInPc1	shluk
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
estonštině	estonština	k1gFnSc6	estonština
neoblíbené	oblíbený	k2eNgFnSc6d1	neoblíbená
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
souhláskových	souhláskový	k2eAgInPc2d1	souhláskový
shluků	shluk	k1gInPc2	shluk
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
přejatých	přejatý	k2eAgNnPc6d1	přejaté
slovech	slovo	k1gNnPc6	slovo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Gramatika	gramatika	k1gFnSc1	gramatika
==	==	k?	==
</s>
</p>
<p>
<s>
Typologicky	typologicky	k6eAd1	typologicky
estonština	estonština	k1gFnSc1	estonština
představuje	představovat	k5eAaImIp3nS	představovat
přechodnou	přechodný	k2eAgFnSc4d1	přechodná
formu	forma	k1gFnSc4	forma
mezi	mezi	k7c7	mezi
aglutinačním	aglutinační	k2eAgInSc7d1	aglutinační
a	a	k8xC	a
flektivním	flektivní	k2eAgInSc7d1	flektivní
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
syntax	syntax	k1gFnSc1	syntax
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
slovní	slovní	k2eAgFnSc1d1	slovní
zásoba	zásoba	k1gFnSc1	zásoba
byly	být	k5eAaImAgFnP	být
značně	značně	k6eAd1	značně
ovlivněny	ovlivnit	k5eAaPmNgInP	ovlivnit
němčinou	němčina	k1gFnSc7	němčina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
estonštině	estonština	k1gFnSc6	estonština
neexistuje	existovat	k5eNaImIp3nS	existovat
člen	člen	k1gInSc1	člen
určitý	určitý	k2eAgInSc1d1	určitý
ani	ani	k8xC	ani
neurčitý	určitý	k2eNgInSc1d1	neurčitý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hláskové	hláskový	k2eAgFnPc1d1	hlásková
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
estonštině	estonština	k1gFnSc6	estonština
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
estonštině	estonština	k1gFnSc6	estonština
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
celé	celý	k2eAgFnSc3d1	celá
řadě	řada	k1gFnSc3	řada
hláskových	hláskový	k2eAgFnPc2d1	hlásková
změn	změna	k1gFnPc2	změna
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
buď	buď	k8xC	buď
kvantitativních	kvantitativní	k2eAgInPc2d1	kvantitativní
(	(	kIx(	(
<g/>
rozdíl	rozdíl	k1gInSc4	rozdíl
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
hlásky	hláska	k1gFnSc2	hláska
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
kvalitativních	kvalitativní	k2eAgInPc2d1	kvalitativní
(	(	kIx(	(
<g/>
záměna	záměna	k1gFnSc1	záměna
jedné	jeden	k4xCgFnSc2	jeden
souhlásky	souhláska	k1gFnSc2	souhláska
za	za	k7c4	za
druhou	druhý	k4xOgFnSc4	druhý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kvantitativní	kvantitativní	k2eAgFnPc1d1	kvantitativní
změny	změna	k1gFnPc1	změna
se	se	k3xPyFc4	se
u	u	k7c2	u
samohlásek	samohláska	k1gFnPc2	samohláska
týkají	týkat	k5eAaImIp3nP	týkat
většinou	většinou	k6eAd1	většinou
záměny	záměna	k1gFnPc1	záměna
mezi	mezi	k7c7	mezi
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
a	a	k8xC	a
předlouhými	předlouhý	k2eAgInPc7d1	předlouhý
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
souhlásek	souhláska	k1gFnPc2	souhláska
jde	jít	k5eAaImIp3nS	jít
obvykle	obvykle	k6eAd1	obvykle
o	o	k7c4	o
různé	různý	k2eAgFnPc4d1	různá
záměny	záměna	k1gFnPc4	záměna
všech	všecek	k3xTgFnPc2	všecek
tří	tři	k4xCgFnPc2	tři
délek	délka	k1gFnPc2	délka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přehled	přehled	k1gInSc1	přehled
možných	možný	k2eAgFnPc2d1	možná
kvalitativních	kvalitativní	k2eAgFnPc2d1	kvalitativní
změn	změna	k1gFnPc2	změna
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Plný	plný	k2eAgInSc1d1	plný
a	a	k8xC	a
zkrácený	zkrácený	k2eAgInSc1d1	zkrácený
kmen	kmen	k1gInSc1	kmen
==	==	k?	==
</s>
</p>
<p>
<s>
Každé	každý	k3xTgNnSc1	každý
slovo	slovo	k1gNnSc1	slovo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
skloňovat	skloňovat	k5eAaImF	skloňovat
či	či	k8xC	či
časovat	časovat	k5eAaImF	časovat
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
estonštině	estonština	k1gFnSc6	estonština
kmen	kmen	k1gInSc1	kmen
zakončený	zakončený	k2eAgInSc1d1	zakončený
samohláskou	samohláska	k1gFnSc7	samohláska
(	(	kIx(	(
<g/>
např.	např.	kA	např.
jää-	jää-	k?	jää-
-	-	kIx~	-
led	led	k1gInSc1	led
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
mají	mít	k5eAaImIp3nP	mít
navíc	navíc	k6eAd1	navíc
tzv.	tzv.	kA	tzv.
zkrácený	zkrácený	k2eAgInSc1d1	zkrácený
kmen	kmen	k1gInSc1	kmen
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
samohlásky	samohláska	k1gFnSc2	samohláska
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Silný	silný	k2eAgInSc4d1	silný
a	a	k8xC	a
slabý	slabý	k2eAgInSc4d1	slabý
stupeň	stupeň	k1gInSc4	stupeň
kmene	kmen	k1gInSc2	kmen
==	==	k?	==
</s>
</p>
<p>
<s>
Ohebná	ohebný	k2eAgNnPc1d1	ohebné
slova	slovo	k1gNnPc1	slovo
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
kmen	kmen	k1gInSc4	kmen
buď	buď	k8xC	buď
ve	v	k7c6	v
slabém	slabý	k2eAgInSc6d1	slabý
nebo	nebo	k8xC	nebo
silném	silný	k2eAgInSc6d1	silný
stupni	stupeň	k1gInSc6	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Síla	síla	k1gFnSc1	síla
kmene	kmen	k1gInSc2	kmen
se	se	k3xPyFc4	se
určuje	určovat	k5eAaImIp3nS	určovat
podle	podle	k7c2	podle
druhu	druh	k1gInSc2	druh
změny	změna	k1gFnSc2	změna
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
prodělává	prodělávat	k5eAaImIp3nS	prodělávat
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
<g/>
-li	i	k?	-li
o	o	k7c4	o
kvalitativní	kvalitativní	k2eAgFnSc4d1	kvalitativní
změnu	změna	k1gFnSc4	změna
<g/>
,	,	kIx,	,
silný	silný	k2eAgInSc1d1	silný
je	být	k5eAaImIp3nS	být
kmen	kmen	k1gInSc1	kmen
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
delší	dlouhý	k2eAgFnSc4d2	delší
<g/>
,	,	kIx,	,
hláskově	hláskově	k6eAd1	hláskově
různorodější	různorodý	k2eAgFnSc1d2	různorodější
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
změna	změna	k1gFnSc1	změna
kvantitativní	kvantitativní	k2eAgFnSc1d1	kvantitativní
<g/>
,	,	kIx,	,
za	za	k7c4	za
silný	silný	k2eAgInSc4d1	silný
kmen	kmen	k1gInSc4	kmen
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
tvar	tvar	k1gInSc1	tvar
s	s	k7c7	s
delší	dlouhý	k2eAgFnSc7d2	delší
formou	forma	k1gFnSc7	forma
hlásky	hláska	k1gFnSc2	hláska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
silném	silný	k2eAgInSc6d1	silný
stupni	stupeň	k1gInSc6	stupeň
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
objevit	objevit	k5eAaPmF	objevit
plný	plný	k2eAgInSc4d1	plný
i	i	k8xC	i
redukovaný	redukovaný	k2eAgInSc4d1	redukovaný
stupeň	stupeň	k1gInSc4	stupeň
<g/>
,	,	kIx,	,
ve	v	k7c6	v
slabém	slabý	k2eAgMnSc6d1	slabý
pouze	pouze	k6eAd1	pouze
redukovaný	redukovaný	k2eAgMnSc1d1	redukovaný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Skloňování	skloňování	k1gNnSc2	skloňování
==	==	k?	==
</s>
</p>
<p>
<s>
Podstatná	podstatný	k2eAgNnPc1d1	podstatné
a	a	k8xC	a
přídavná	přídavný	k2eAgNnPc1d1	přídavné
jména	jméno	k1gNnPc1	jméno
ani	ani	k8xC	ani
zájmena	zájmeno	k1gNnPc1	zájmeno
nemají	mít	k5eNaImIp3nP	mít
gramatický	gramatický	k2eAgInSc4d1	gramatický
rod	rod	k1gInSc4	rod
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
jména	jméno	k1gNnPc1	jméno
se	se	k3xPyFc4	se
skloňují	skloňovat	k5eAaImIp3nP	skloňovat
podle	podle	k7c2	podle
14	[number]	k4	14
pádů	pád	k1gInPc2	pád
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
nominativ	nominativ	k1gInSc1	nominativ
<g/>
,	,	kIx,	,
genitiv	genitiv	k1gInSc1	genitiv
<g/>
,	,	kIx,	,
partitiv	partitiv	k1gInSc1	partitiv
<g/>
,	,	kIx,	,
illativ	illativ	k1gInSc1	illativ
<g/>
,	,	kIx,	,
inessiv	inessit	k5eAaPmDgInS	inessit
<g/>
,	,	kIx,	,
elativ	elativ	k1gInSc1	elativ
<g/>
,	,	kIx,	,
allativ	allativ	k1gInSc1	allativ
<g/>
,	,	kIx,	,
adessiv	adessit	k5eAaPmDgInS	adessit
<g/>
,	,	kIx,	,
ablativ	ablativ	k1gInSc4	ablativ
<g/>
,	,	kIx,	,
translativ	translatit	k5eAaPmDgInS	translatit
<g/>
,	,	kIx,	,
terminativ	terminativum	k1gNnPc2	terminativum
<g/>
,	,	kIx,	,
essiv	essiva	k1gFnPc2	essiva
<g/>
,	,	kIx,	,
abessiv	abessit	k5eAaPmDgInS	abessit
a	a	k8xC	a
komitativ	komitativum	k1gNnPc2	komitativum
<g/>
.	.	kIx.	.
</s>
<s>
Pádové	pádový	k2eAgFnPc1d1	pádová
koncovky	koncovka	k1gFnPc1	koncovka
se	se	k3xPyFc4	se
často	často	k6eAd1	často
užívají	užívat	k5eAaImIp3nP	užívat
k	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
gramatických	gramatický	k2eAgInPc2d1	gramatický
vztahů	vztah	k1gInPc2	vztah
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
se	se	k3xPyFc4	se
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
předložkami	předložka	k1gFnPc7	předložka
(	(	kIx(	(
<g/>
v	v	k7c6	v
<g/>
,	,	kIx,	,
do	do	k7c2	do
<g/>
,	,	kIx,	,
z	z	k7c2	z
<g/>
,	,	kIx,	,
na	na	k7c6	na
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skloňování	skloňování	k1gNnSc1	skloňování
se	se	k3xPyFc4	se
v	v	k7c6	v
jednotném	jednotný	k2eAgNnSc6d1	jednotné
a	a	k8xC	a
množném	množný	k2eAgNnSc6d1	množné
čísle	číslo	k1gNnSc6	číslo
neliší	lišit	k5eNaImIp3nS	lišit
tolik	tolik	k4yIc1	tolik
jako	jako	k8xS	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
několika	několik	k4yIc2	několik
pádů	pád	k1gInPc2	pád
(	(	kIx(	(
<g/>
nominativ	nominativ	k1gInSc1	nominativ
<g/>
,	,	kIx,	,
genitiv	genitiv	k1gInSc1	genitiv
<g/>
,	,	kIx,	,
partitiv	partitiv	k1gInSc1	partitiv
<g/>
,	,	kIx,	,
illativ	illativ	k1gInSc1	illativ
<g/>
)	)	kIx)	)
užívá	užívat	k5eAaImIp3nS	užívat
plurál	plurál	k1gInSc4	plurál
stejné	stejný	k2eAgFnSc2d1	stejná
koncovky	koncovka	k1gFnSc2	koncovka
jako	jako	k8xS	jako
singulár	singulár	k1gInSc1	singulár
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
před	před	k7c4	před
ně	on	k3xPp3gNnSc4	on
přidává	přidávat	k5eAaImIp3nS	přidávat
-de-	e-	k?	-de-
<g/>
,	,	kIx,	,
-te-	e-	k?	-te-
<g/>
,	,	kIx,	,
-e-	-	k?	-e-
<g/>
,	,	kIx,	,
-i-	-	k?	-i-
nebo	nebo	k8xC	nebo
-u-	-	k?	-u-
,	,	kIx,	,
[	[	kIx(	[
<g/>
*	*	kIx~	*
<g/>
]	]	kIx)	]
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
skloňovacím	skloňovací	k2eAgInSc6d1	skloňovací
vzoru	vzor	k1gInSc6	vzor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stupňování	stupňování	k1gNnSc1	stupňování
přídavných	přídavný	k2eAgNnPc2d1	přídavné
jmen	jméno	k1gNnPc2	jméno
==	==	k?	==
</s>
</p>
<p>
<s>
Komparativ	komparativ	k1gInSc1	komparativ
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
stupeň	stupeň	k1gInSc1	stupeň
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
přidáním	přidání	k1gNnSc7	přidání
koncovky	koncovka	k1gFnSc2	koncovka
-em	m	k?	-em
<g/>
,	,	kIx,	,
superlativ	superlativ	k1gInSc1	superlativ
(	(	kIx(	(
<g/>
třetí	třetí	k4xOgInSc1	třetí
stupeň	stupeň	k1gInSc1	stupeň
<g/>
)	)	kIx)	)
přidáním	přidání	k1gNnSc7	přidání
koncovky	koncovka	k1gFnSc2	koncovka
-im	m	k?	-im
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzorový	vzorový	k2eAgInSc1d1	vzorový
text	text	k1gInSc1	text
==	==	k?	==
</s>
</p>
<p>
<s>
Všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
deklarace	deklarace	k1gFnSc1	deklarace
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
estonsky	estonsky	k6eAd1	estonsky
</s>
</p>
<p>
<s>
Kõ	Kõ	k?	Kõ
inimesed	inimesed	k1gMnSc1	inimesed
sünnivad	sünnivad	k6eAd1	sünnivad
vabadena	vabaden	k2eAgFnSc1d1	vabaden
ja	ja	k?	ja
võ	võ	k?	võ
oma	oma	k?	oma
väärikuselt	väärikuselt	k1gInSc1	väärikuselt
ja	ja	k?	ja
õ	õ	k?	õ
<g/>
.	.	kIx.	.
</s>
<s>
Neile	Neile	k6eAd1	Neile
on	on	k3xPp3gMnSc1	on
antud	antud	k1gMnSc1	antud
mõ	mõ	k?	mõ
ja	ja	k?	ja
südametunnistus	südametunnistus	k1gInSc1	südametunnistus
ja	ja	k?	ja
nende	nde	k6eNd1	nde
suhtumist	suhtumist	k1gMnSc1	suhtumist
üksteisesse	üksteisesse	k1gFnSc2	üksteisesse
peab	peab	k1gMnSc1	peab
kandma	kandma	k1gFnSc1	kandma
vendluse	vendluse	k6eAd1	vendluse
vaim	vaim	k6eAd1	vaim
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
česky	česky	k6eAd1	česky
</s>
</p>
<p>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nP	rodit
svobodní	svobodný	k2eAgMnPc1d1	svobodný
a	a	k8xC	a
sobě	se	k3xPyFc3	se
rovní	rovný	k2eAgMnPc1d1	rovný
co	co	k9	co
do	do	k7c2	do
důstojnosti	důstojnost	k1gFnSc2	důstojnost
a	a	k8xC	a
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
nadáni	nadat	k5eAaPmNgMnP	nadat
rozumem	rozum	k1gInSc7	rozum
a	a	k8xC	a
svědomím	svědomí	k1gNnSc7	svědomí
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
spolu	spolu	k6eAd1	spolu
jednat	jednat	k5eAaImF	jednat
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
bratrství	bratrství	k1gNnSc2	bratrství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Estonská	estonský	k2eAgFnSc1d1	Estonská
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
</s>
</p>
<p>
<s>
Estonsko	Estonsko	k1gNnSc1	Estonsko
</s>
</p>
<p>
<s>
Estonská	estonský	k2eAgNnPc1d1	Estonské
slovesa	sloveso	k1gNnPc1	sloveso
</s>
</p>
<p>
<s>
Estonské	estonský	k2eAgFnPc1d1	Estonská
číslovky	číslovka	k1gFnPc1	číslovka
</s>
</p>
<p>
<s>
Estonské	estonský	k2eAgFnPc1d1	Estonská
postpozice	postpozice	k1gFnPc1	postpozice
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
estonština	estonština	k1gFnSc1	estonština
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
estonština	estonština	k1gFnSc1	estonština
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Estonština	estonština	k1gFnSc1	estonština
pro	pro	k7c4	pro
každého	každý	k3xTgMnSc4	každý
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejucelenější	ucelený	k2eAgInSc1d3	nejucelenější
česky	česky	k6eAd1	česky
psaný	psaný	k2eAgInSc1d1	psaný
<g/>
,	,	kIx,	,
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
dostupný	dostupný	k2eAgInSc1d1	dostupný
souhrn	souhrn	k1gInSc1	souhrn
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
estonštině	estonština	k1gFnSc6	estonština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Estonsko-český	estonsko-český	k2eAgInSc1d1	estonsko-český
slovník	slovník	k1gInSc1	slovník
online	onlinout	k5eAaPmIp3nS	onlinout
</s>
</p>
