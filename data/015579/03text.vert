<s>
Šizuoka	Šizuoka	k6eAd1
</s>
<s>
Šizuoka	Šizuoka	k1gFnSc1
静	静	k?
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
34	#num#	k4
<g/>
°	°	k?
<g/>
58	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
138	#num#	k4
<g/>
°	°	k?
<g/>
22	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
12	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Japonsko	Japonsko	k1gNnSc1
Japonsko	Japonsko	k1gNnSc1
Region	region	k1gInSc1
</s>
<s>
Čúbu	Čúbu	k6eAd1
Prefektura	prefektura	k1gFnSc1
</s>
<s>
Prefektura	prefektura	k1gFnSc1
Šizuoka	Šizuoka	k1gFnSc1
Administrativní	administrativní	k2eAgFnSc1d1
dělení	dělení	k1gNnSc4
</s>
<s>
3	#num#	k4
čtvrtě	čtvrt	k1gFnPc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
1	#num#	k4
374	#num#	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
697	#num#	k4
578	#num#	k4
(	(	kIx(
<g/>
březen	březen	k1gInSc1
2018	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
507,7	507,7	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Starosta	Starosta	k1gMnSc1
</s>
<s>
Nobuhiro	Nobuhira	k1gMnSc5
Tanabe	Tanab	k1gMnSc5
Vznik	vznik	k1gInSc1
</s>
<s>
1889	#num#	k4
a	a	k8xC
2003	#num#	k4
Oficiální	oficiální	k2eAgInSc4d1
web	web	k1gInSc4
</s>
<s>
www.city.shizuoka.jp	www.city.shizuoka.jp	k1gInSc1
Adresa	adresa	k1gFnSc1
obecního	obecní	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
</s>
<s>
5-1	5-1	k4
Ótemači	Ótemač	k1gInPc7
<g/>
,	,	kIx,
Aoi-ku	Aoi-ek	k1gMnSc3
<g/>
,	,	kIx,
Šizuoka-ši	Šizuoka-š	k1gMnSc3
<g/>
,	,	kIx,
Šizuoka-ken	Šizuoka-ken	k1gInSc1
420-8602	420-8602	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Šizuoka	Šizuoka	k1gFnSc1
(	(	kIx(
<g/>
japonsky	japonsky	k6eAd1
<g/>
:	:	kIx,
静	静	k?
<g/>
;	;	kIx,
Šizuoka-ši	Šizuoka-š	k1gMnPc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
prefektury	prefektura	k1gFnSc2
Šizuoka	Šizuoek	k1gInSc2
na	na	k7c6
ostrově	ostrov	k1gInSc6
Honšú	Honšú	k1gFnSc2
v	v	k7c6
Japonsku	Japonsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
městě	město	k1gNnSc6
je	být	k5eAaImIp3nS
také	také	k9
přístav	přístav	k1gInSc1
(	(	kIx(
<g/>
známá	známý	k2eAgFnSc1d1
lodní	lodní	k2eAgFnSc1d1
trasa	trasa	k1gFnSc1
Šizuoka-Su-čou	Šizuoka-Su-ča	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Městské	městský	k2eAgFnSc3d1
čtvrti	čtvrt	k1gFnSc3
</s>
<s>
Aoi-ku	Aoi-ku	k6eAd1
</s>
<s>
Suruga-ku	Suruga-ku	k6eAd1
</s>
<s>
Šimizu-ku	Šimizu-ku	k6eAd1
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Během	během	k7c2
období	období	k1gNnSc2
Edo	Eda	k1gMnSc5
byla	být	k5eAaImAgFnS
Šizuoka	Šizuoka	k1gFnSc1
nejprve	nejprve	k6eAd1
obsazena	obsadit	k5eAaPmNgFnS
Tokugawou	Tokugawa	k1gMnSc7
Iejasuem	Iejasu	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
se	se	k3xPyFc4
jmenovala	jmenovat	k5eAaImAgFnS,k5eAaBmAgFnS
Sunpu	Sunpa	k1gFnSc4
(	(	kIx(
<g/>
駿	駿	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
byla	být	k5eAaImAgFnS
zkratka	zkratka	k1gFnSc1
z	z	k7c2
Suruga	Surug	k1gMnSc2
no	no	k9
Kokufu	Kokuf	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
lénem	léno	k1gNnSc7
Tokugawy	Tokugawa	k1gFnSc2
Tadanagy	Tadanaga	k1gFnSc2
(	(	kIx(
<g/>
Iejasuova	Iejasuův	k2eAgMnSc2d1
syna	syn	k1gMnSc2
<g/>
)	)	kIx)
a	a	k8xC
nakonec	nakonec	k6eAd1
byla	být	k5eAaImAgFnS
spravována	spravovat	k5eAaImNgFnS
přímo	přímo	k6eAd1
šógunátem	šógunát	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Moderní	moderní	k2eAgNnSc1d1
město	město	k1gNnSc1
bylo	být	k5eAaImAgNnS
založeno	založit	k5eAaPmNgNnS
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1889	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Sport	sport	k1gInSc1
</s>
<s>
Město	město	k1gNnSc1
má	mít	k5eAaImIp3nS
silnou	silný	k2eAgFnSc4d1
fotbalovou	fotbalový	k2eAgFnSc4d1
tradici	tradice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonské	japonský	k2eAgFnSc2d1
fotbalové	fotbalový	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
(	(	kIx(
<g/>
J.	J.	kA
League	League	k1gFnSc6
<g/>
)	)	kIx)
se	se	k3xPyFc4
účastní	účastnit	k5eAaImIp3nS
klub	klub	k1gInSc1
Šimizu	Šimiz	k1gInSc2
S-Pulse	S-Pulse	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Rodáci	rodák	k1gMnPc1
</s>
<s>
Midori	Midori	k6eAd1
Hondaová	Hondaový	k2eAgFnSc1d1
(	(	kIx(
<g/>
*	*	kIx~
1964	#num#	k4
<g/>
)	)	kIx)
–	–	k?
fotbalistka	fotbalistka	k1gFnSc1
</s>
<s>
Ecuko	Ecuko	k6eAd1
Handaová	Handaový	k2eAgFnSc1d1
(	(	kIx(
<g/>
*	*	kIx~
1965	#num#	k4
<g/>
)	)	kIx)
–	–	k?
fotbalistka	fotbalistka	k1gFnSc1
</s>
<s>
Čiaki	Čiaki	k6eAd1
Jamadaová	Jamadaový	k2eAgFnSc1d1
(	(	kIx(
<g/>
*	*	kIx~
1966	#num#	k4
<g/>
)	)	kIx)
–	–	k?
fotbalistka	fotbalistka	k1gFnSc1
</s>
<s>
Sesterská	sesterský	k2eAgNnPc1d1
města	město	k1gNnPc1
</s>
<s>
Omaha	Omaha	k1gFnSc1
<g/>
,	,	kIx,
Nebraska	Nebraska	k1gFnSc1
<g/>
,	,	kIx,
USA	USA	kA
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
1965	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Cannes	Cannes	k1gNnSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
1991	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hue	Hue	k?
<g/>
,	,	kIx,
Vietnam	Vietnam	k1gInSc1
(	(	kIx(
<g/>
12	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Shelbyville	Shelbyville	k1gFnPc1
<g/>
,	,	kIx,
Shelbyville	Shelbyville	k1gFnPc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgFnSc2d1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
1989	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Stockton	Stockton	k1gInSc1
<g/>
,	,	kIx,
Kalifornie	Kalifornie	k1gFnSc1
<g/>
,	,	kIx,
USA	USA	kA
(	(	kIx(
<g/>
16	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
1959	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Šizuoka	Šizuoek	k1gInSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
města	město	k1gNnSc2
Šizuoka	Šizuoko	k1gNnSc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Japonsko	Japonsko	k1gNnSc1
–	–	k?
日	日	k?
(	(	kIx(
<g/>
Nihon	Nihon	k1gMnSc1
<g/>
)	)	kIx)
–	–	k?
(	(	kIx(
<g/>
J	J	kA
<g/>
)	)	kIx)
Prefektury	prefektura	k1gFnSc2
(	(	kIx(
<g/>
都	都	k?
<g/>
)	)	kIx)
a	a	k8xC
jejich	jejich	k3xOp3gNnPc4
hlavní	hlavní	k2eAgNnPc4d1
města	město	k1gNnPc4
</s>
<s>
Prefektura	prefektura	k1gFnSc1
Aiči	Aič	k1gFnSc2
(	(	kIx(
<g/>
Nagoja	Nagoja	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Akita	Akita	k1gFnSc1
(	(	kIx(
<g/>
Akita	Akita	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Aomori	Aomor	k1gFnSc2
(	(	kIx(
<g/>
Aomori	Aomor	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Čiba	Čiba	k1gFnSc1
(	(	kIx(
<g/>
Čiba	Čiba	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Ehime	Ehim	k1gInSc5
(	(	kIx(
<g/>
Macujama	Macujamum	k1gNnSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Fukui	Fuku	k1gFnSc2
(	(	kIx(
<g/>
Fukui	Fuku	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Fukuoka	Fukuoka	k1gFnSc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
Fukuoka	Fukuoka	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Fukušima	Fukušima	k1gFnSc1
(	(	kIx(
<g/>
Fukušima	Fukušima	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Gifu	Gifus	k1gInSc2
(	(	kIx(
<g/>
Gifu	Gifus	k1gInSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Gunma	Gunma	k1gFnSc1
(	(	kIx(
<g/>
Maebaši	Maebaše	k1gFnSc4
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Hirošima	Hirošima	k1gFnSc1
(	(	kIx(
<g/>
Hirošima	Hirošima	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Hokkaidó	Hokkaidó	k1gFnSc1
(	(	kIx(
<g/>
Sapporo	Sappora	k1gFnSc5
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Hjógo	Hjógo	k1gMnSc1
(	(	kIx(
<g/>
Kóbe	Kóbe	k1gNnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Ibaraki	Ibarak	k1gFnSc2
(	(	kIx(
<g/>
Mito	Mito	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Išikawa	Išikawa	k1gFnSc1
(	(	kIx(
<g/>
Kanazawa	Kanazawa	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Iwate	Iwat	k1gInSc5
(	(	kIx(
<g/>
Morioka	Morioko	k1gNnSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Jamagata	Jamagata	k1gFnSc1
(	(	kIx(
<g/>
Jamagata	Jamagata	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Jamaguči	Jamaguč	k1gInPc7
(	(	kIx(
<g/>
Jamaguči	Jamaguč	k1gInPc7
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Jamanaši	Jamanaše	k1gFnSc4
(	(	kIx(
<g/>
Kófu	Kófa	k1gFnSc4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Prefektura	prefektura	k1gFnSc1
Kagawa	Kagawa	k1gFnSc1
(	(	kIx(
<g/>
Takamacu	Takamacus	k1gInSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Kagošima	Kagošima	k1gFnSc1
(	(	kIx(
<g/>
Kagošima	Kagošima	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Kanagawa	Kanagawa	k1gFnSc1
(	(	kIx(
<g/>
Jokohama	Jokohama	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Kjóto	Kjóto	k1gNnSc4
(	(	kIx(
<g/>
Kjóto	Kjóto	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Kóči	Kóč	k1gFnSc2
(	(	kIx(
<g/>
Kóči	Kóč	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Kumamoto	Kumamota	k1gFnSc5
(	(	kIx(
<g/>
Kumamoto	Kumamota	k1gFnSc5
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Mie	Mie	k1gFnSc1
(	(	kIx(
<g/>
Cu	Cu	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Mijagi	Mijag	k1gFnSc2
(	(	kIx(
<g/>
Sendai	Senda	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Mijazaki	Mijazak	k1gFnSc2
(	(	kIx(
<g/>
Mijazaki	Mijazak	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Nagano	Nagano	k1gNnSc4
(	(	kIx(
<g/>
Nagano	Nagano	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Nagasaki	Nagasaki	k1gNnSc2
(	(	kIx(
<g/>
Nagasaki	Nagasaki	k1gNnSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Nara	Nara	k1gFnSc1
(	(	kIx(
<g/>
Nara	Nara	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Niigata	Niigata	k1gFnSc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
Niigata	Niigata	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Óita	Óita	k1gFnSc1
(	(	kIx(
<g/>
Óita	Óita	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Okajama	Okajama	k1gFnSc1
(	(	kIx(
<g/>
Okajama	Okajama	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Okinawa	Okinawa	k1gFnSc1
(	(	kIx(
<g/>
Naha	naho	k1gNnSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Ósaka	Ósaka	k1gFnSc1
(	(	kIx(
<g/>
Ósaka	Ósaka	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Saga	Saga	k1gFnSc1
(	(	kIx(
<g/>
Saga	Saga	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Saitama	Saitama	k1gFnSc1
(	(	kIx(
<g/>
Saitama	Saitama	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Šiga	Šiga	k1gFnSc1
(	(	kIx(
<g/>
Ócu	Ócu	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Šimane	Šiman	k1gMnSc5
(	(	kIx(
<g/>
Macue	Macue	k1gNnSc7
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Šizuoka	Šizuoka	k1gFnSc1
(	(	kIx(
<g/>
Šizuoka	Šizuoka	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Točigi	Točig	k1gFnSc2
(	(	kIx(
<g/>
Ucunomija	Ucunomija	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Tokušima	Tokušima	k1gFnSc1
(	(	kIx(
<g/>
Tokušima	Tokušima	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Tokio	Tokio	k1gNnSc4
(	(	kIx(
<g/>
Šindžuku	Šindžuk	k1gInSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Tottori	Tottor	k1gFnSc2
(	(	kIx(
<g/>
Tottori	Tottor	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Tojama	Tojama	k1gFnSc1
(	(	kIx(
<g/>
Tojama	Tojama	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Wakajama	Wakajama	k1gFnSc1
(	(	kIx(
<g/>
Wakajama	Wakajama	k1gFnSc1
<g/>
)	)	kIx)
Japonské	japonský	k2eAgInPc1d1
regiony	region	k1gInPc1
(	(	kIx(
<g/>
地	地	k?
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Hokkaidó	Hokkaidó	k1gFnSc1
•	•	k?
Tóhoku	Tóhok	k1gInSc2
•	•	k?
Kantó	Kantó	k1gMnSc1
•	•	k?
Čúbu	Čúba	k1gFnSc4
•	•	k?
Kansai	Kansa	k1gFnSc3
•	•	k?
Čúgoku	Čúgok	k1gInSc6
•	•	k?
Šikoku	Šikok	k1gInSc2
•	•	k?
Kjúšú	Kjúšú	k1gMnSc1
</s>
<s>
Prefektura	prefektura	k1gFnSc1
Šizuoka	Šizuoek	k1gInSc2
–	–	k?
静	静	k?
(	(	kIx(
<g/>
Šizuoka-ken	Šizuoka-ken	k1gInSc1
<g/>
)	)	kIx)
Města	město	k1gNnSc2
(	(	kIx(
<g/>
市	市	k?
<g/>
,	,	kIx,
ši	ši	k?
<g/>
)	)	kIx)
</s>
<s>
Atami	Ata	k1gFnPc7
(	(	kIx(
<g/>
熱	熱	k?
<g/>
)	)	kIx)
•	•	k?
Fudži	Fudž	k1gFnSc6
(	(	kIx(
<g/>
富	富	k?
<g/>
)	)	kIx)
•	•	k?
Fudžieda	Fudžieda	k1gMnSc1
(	(	kIx(
<g/>
藤	藤	k?
<g/>
)	)	kIx)
•	•	k?
Fudžinomija	Fudžinomija	k1gFnSc1
(	(	kIx(
<g/>
富	富	k?
<g/>
)	)	kIx)
•	•	k?
Fukuroi	Fukuro	k1gFnSc2
(	(	kIx(
<g/>
袋	袋	k?
<g/>
)	)	kIx)
•	•	k?
Gotemba	Gotemba	k1gFnSc1
(	(	kIx(
<g/>
御	御	k?
<g/>
)	)	kIx)
•	•	k?
Hamamacu	Hamamacus	k1gInSc2
(	(	kIx(
<g/>
浜	浜	k?
<g/>
)	)	kIx)
•	•	k?
Itó	Itó	k1gFnSc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
伊	伊	k?
<g/>
)	)	kIx)
•	•	k?
Iwata	Iwata	k1gFnSc1
(	(	kIx(
<g/>
磐	磐	k?
<g/>
)	)	kIx)
•	•	k?
Izu	Izu	k1gFnSc1
(	(	kIx(
<g/>
伊	伊	k?
<g/>
)	)	kIx)
•	•	k?
Izunokuni	Izunokueň	k1gFnSc6
(	(	kIx(
<g/>
伊	伊	k?
<g/>
)	)	kIx)
•	•	k?
Jaizu	Jaiz	k1gInSc2
(	(	kIx(
<g/>
焼	焼	k?
<g/>
)	)	kIx)
•	•	k?
Kakegawa	Kakegawa	k1gFnSc1
(	(	kIx(
<g/>
掛	掛	k?
<g/>
)	)	kIx)
•	•	k?
Kikugawa	Kikugawa	k1gFnSc1
(	(	kIx(
<g/>
菊	菊	k?
<g/>
)	)	kIx)
•	•	k?
Kosai	Kosa	k1gFnSc2
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
湖	湖	k?
<g/>
)	)	kIx)
•	•	k?
Makinohara	Makinohara	k1gFnSc1
(	(	kIx(
<g/>
牧	牧	k?
<g/>
)	)	kIx)
•	•	k?
Mišima	Mišima	k1gFnSc1
(	(	kIx(
<g/>
三	三	k?
<g/>
)	)	kIx)
•	•	k?
Numazu	Numaz	k1gInSc2
(	(	kIx(
<g/>
沼	沼	k?
<g/>
)	)	kIx)
•	•	k?
Omaezaki	Omaezak	k1gFnSc2
(	(	kIx(
<g/>
御	御	k?
<g/>
)	)	kIx)
•	•	k?
Susono	Susona	k1gFnSc5
(	(	kIx(
<g/>
裾	裾	k?
<g/>
)	)	kIx)
•	•	k?
Šimada	Šimada	k1gFnSc1
(	(	kIx(
<g/>
島	島	k?
<g/>
)	)	kIx)
•	•	k?
Šimoda	Šimoda	k1gFnSc1
(	(	kIx(
<g/>
下	下	k?
<g/>
)	)	kIx)
•	•	k?
Šizuoka	Šizuoka	k1gFnSc1
(	(	kIx(
<g/>
静	静	k?
hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
<g/>
)	)	kIx)
Okresy	okres	k1gInPc1
(	(	kIx(
<g/>
郡	郡	k?
<g/>
,	,	kIx,
gun	gun	k?
<g/>
)	)	kIx)
</s>
<s>
městečka	městečko	k1gNnPc4
(	(	kIx(
<g/>
町	町	k?
<g/>
,	,	kIx,
mači	mač	k1gInSc3
<g/>
/	/	kIx~
<g/>
čó	čó	k?
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Kamo	kamo	k6eAd1
(	(	kIx(
<g/>
賀	賀	k?
<g/>
)	)	kIx)
</s>
<s>
Higaši	Higaš	k1gMnPc1
Izu-čó	Izu-čó	k1gFnSc2
(	(	kIx(
<g/>
東	東	k?
<g/>
)	)	kIx)
•	•	k?
Kawazu-čó	Kawazu-čó	k1gFnSc1
(	(	kIx(
<g/>
河	河	k?
<g/>
)	)	kIx)
•	•	k?
Macuzaki-čó	Macuzaki-čó	k1gFnSc1
(	(	kIx(
<g/>
松	松	k?
<g/>
)	)	kIx)
•	•	k?
Minami	mina	k1gFnPc7
Izu-čó	Izu-čó	k1gFnSc2
(	(	kIx(
<g/>
南	南	k?
<g/>
)	)	kIx)
•	•	k?
Niši	Niši	k1gNnSc1
Izu-čó	Izu-čó	k1gFnSc1
(	(	kIx(
<g/>
西	西	k?
<g/>
)	)	kIx)
Tagata	Tagata	k1gFnSc1
(	(	kIx(
<g/>
田	田	k?
<g/>
)	)	kIx)
</s>
<s>
Kannami-čó	Kannami-čó	k?
(	(	kIx(
<g/>
函	函	k?
<g/>
)	)	kIx)
Suntó	Suntó	k1gMnSc1
(	(	kIx(
<g/>
駿	駿	k?
<g/>
)	)	kIx)
</s>
<s>
Nagaizumi-čó	Nagaizumi-čó	k?
(	(	kIx(
<g/>
長	長	k?
<g/>
)	)	kIx)
•	•	k?
Ojama-čó	Ojama-čó	k1gFnSc1
(	(	kIx(
<g/>
小	小	k?
<g/>
)	)	kIx)
•	•	k?
Šimizu-čó	Šimizu-čó	k1gFnSc1
(	(	kIx(
<g/>
清	清	k?
<g/>
)	)	kIx)
Haibara	Haibara	k1gFnSc1
(	(	kIx(
<g/>
榛	榛	k?
<g/>
)	)	kIx)
</s>
<s>
Jošida-čó	Jošida-čó	k?
(	(	kIx(
<g/>
吉	吉	k?
<g/>
)	)	kIx)
•	•	k?
Kawanehon-čó	Kawanehon-čó	k1gFnSc1
(	(	kIx(
<g/>
川	川	k?
<g/>
)	)	kIx)
Šúči	Šúč	k1gFnSc2
(	(	kIx(
<g/>
周	周	k?
<g/>
)	)	kIx)
</s>
<s>
Mori-mači	Mori-mač	k1gMnPc1
(	(	kIx(
<g/>
森	森	k?
<g/>
)	)	kIx)
Jiné	jiný	k2eAgFnPc1d1
</s>
<s>
Hora	hora	k1gFnSc1
Akaišidake	Akaišidak	k1gFnSc2
•	•	k?
Amagi-san	Amagi-san	k1gMnSc1
•	•	k?
Fudži	Fudž	k1gFnSc6
•	•	k?
Hakone	Hakon	k1gInSc5
(	(	kIx(
<g/>
sopka	sopka	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Hrad	hrad	k1gInSc1
Hamamacu	Hamamacus	k1gInSc2
Sousedící	sousedící	k2eAgFnSc2d1
prefektury	prefektura	k1gFnSc2
(	(	kIx(
<g/>
県	県	k?
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Prefektura	prefektura	k1gFnSc1
Aiči	Aič	k1gFnSc2
(	(	kIx(
<g/>
愛	愛	k?
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Nagano	Nagano	k1gNnSc4
(	(	kIx(
<g/>
愛	愛	k?
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Jamanaši	Jamanaše	k1gFnSc4
(	(	kIx(
<g/>
山	山	k?
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Kanagawa	Kanagawa	k1gFnSc1
(	(	kIx(
<g/>
神	神	k?
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Japonsko	Japonsko	k1gNnSc1
|	|	kIx~
Geografie	geografie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
388964	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4289915-1	4289915-1	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0004	#num#	k4
0389	#num#	k4
1822	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
80026055	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
152390639	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
80026055	#num#	k4
</s>
