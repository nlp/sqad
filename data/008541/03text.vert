<p>
<s>
Birkfeld	Birkfeld	k6eAd1	Birkfeld
je	být	k5eAaImIp3nS	být
městys	městys	k1gInSc1	městys
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Weiz	Weiza	k1gFnPc2	Weiza
<g/>
,	,	kIx,	,
ve	v	k7c6	v
spolkové	spolkový	k2eAgFnSc6d1	spolková
zemi	zem	k1gFnSc6	zem
Štýrsko	Štýrsko	k1gNnSc1	Štýrsko
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
,	,	kIx,	,
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
Štýrského	štýrský	k2eAgInSc2d1	štýrský
Hradce	Hradec	k1gInSc2	Hradec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2017	[number]	k4	2017
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
5050	[number]	k4	5050
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Území	území	k1gNnSc1	území
městyse	městys	k1gInSc2	městys
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
89,78	[number]	k4	89,78
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc7	svůj
rozlohou	rozloha	k1gFnSc7	rozloha
je	být	k5eAaImIp3nS	být
městys	městys	k1gInSc1	městys
největší	veliký	k2eAgInSc1d3	veliký
obcí	obec	k1gFnSc7	obec
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
údolí	údolí	k1gNnSc2	údolí
Feistritztal	Feistritztal	k1gMnSc1	Feistritztal
na	na	k7c6	na
obou	dva	k4xCgInPc6	dva
březích	břeh	k1gInPc6	břeh
říčky	říčka	k1gFnSc2	říčka
Feistritz	Feistritza	k1gFnPc2	Feistritza
<g/>
.	.	kIx.	.
</s>
<s>
Nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
území	území	k1gNnSc2	území
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
řece	řeka	k1gFnSc6	řeka
od	od	k7c2	od
500	[number]	k4	500
m	m	kA	m
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
až	až	k9	až
po	po	k7c4	po
650	[number]	k4	650
m	m	kA	m
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
údolí	údolí	k1gNnSc2	údolí
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
západu	západ	k1gInSc3	západ
<g/>
,	,	kIx,	,
výška	výška	k1gFnSc1	výška
stoupá	stoupat	k5eAaImIp3nS	stoupat
k	k	k7c3	k
vrcholům	vrchol	k1gInPc3	vrchol
Hirschberg	Hirschberg	k1gMnSc1	Hirschberg
1263	[number]	k4	1263
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
Koenigskogel	Koenigskogel	k1gInSc1	Koenigskogel
1243	[number]	k4	1243
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
a	a	k8xC	a
Offnerberg	Offnerberg	k1gInSc1	Offnerberg
1293	[number]	k4	1293
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Zalesněné	zalesněný	k2eAgNnSc1d1	zalesněné
horstvo	horstvo	k1gNnSc1	horstvo
je	být	k5eAaImIp3nS	být
odvodňováno	odvodňován	k2eAgNnSc1d1	odvodňován
několika	několik	k4yIc7	několik
potoky	potok	k1gInPc7	potok
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
vlévají	vlévat	k5eAaImIp3nP	vlévat
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Členění	členění	k1gNnSc1	členění
obce	obec	k1gFnSc2	obec
==	==	k?	==
</s>
</p>
<p>
<s>
Obec	obec	k1gFnSc1	obec
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
devět	devět	k4xCc4	devět
osad	osada	k1gFnPc2	osada
(	(	kIx(	(
<g/>
místních	místní	k2eAgFnPc2d1	místní
částí	část	k1gFnPc2	část
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závorkách	závorka	k1gFnPc6	závorka
jsou	být	k5eAaImIp3nP	být
uvedeny	uveden	k2eAgInPc1d1	uveden
počty	počet	k1gInPc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Birkfeld	Birkfeld	k1gInSc1	Birkfeld
(	(	kIx(	(
<g/>
1617	[number]	k4	1617
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Gschaid	Gschaid	k1gInSc1	Gschaid
bei	bei	k?	bei
Birkfeld	Birkfeld	k1gInSc1	Birkfeld
(	(	kIx(	(
<g/>
932	[number]	k4	932
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Haslau	Haslau	k6eAd1	Haslau
bei	bei	k?	bei
Birkfeld	Birkfeld	k1gInSc1	Birkfeld
(	(	kIx(	(
<g/>
440	[number]	k4	440
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Aschau	Aschau	k6eAd1	Aschau
(	(	kIx(	(
<g/>
411	[number]	k4	411
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rabendorf	Rabendorf	k1gInSc1	Rabendorf
(	(	kIx(	(
<g/>
268	[number]	k4	268
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rossegg	Rossegg	k1gInSc1	Rossegg
(	(	kIx(	(
<g/>
285	[number]	k4	285
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sallegg	Sallegg	k1gInSc1	Sallegg
(	(	kIx(	(
<g/>
106	[number]	k4	106
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Piregg	Piregg	k1gInSc1	Piregg
(	(	kIx(	(
<g/>
304	[number]	k4	304
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Waisenegg	Waisenegg	k1gInSc1	Waisenegg
(	(	kIx(	(
<g/>
756	[number]	k4	756
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Sousedé	soused	k1gMnPc5	soused
==	==	k?	==
</s>
</p>
<p>
<s>
Městys	městys	k1gInSc1	městys
Birkfeld	Birkfeld	k1gInSc1	Birkfeld
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
těmito	tento	k3xDgFnPc7	tento
obcemi	obec	k1gFnPc7	obec
<g/>
:	:	kIx,	:
Fischbach	Fischbach	k1gInSc1	Fischbach
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
Strallegg	Strallegg	k1gInSc1	Strallegg
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
<g/>
,	,	kIx,	,
Miesenbach	Miesenbach	k1gMnSc1	Miesenbach
bei	bei	k?	bei
Birkfeld	Birkfeld	k1gMnSc1	Birkfeld
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
Pöllau	Pölla	k2eAgFnSc4d1	Pölla
v	v	k7c6	v
okresu	okres	k1gInSc6	okres
Hartberg-Fürstenfeld	Hartberg-Fürstenfelda	k1gFnPc2	Hartberg-Fürstenfelda
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
<g/>
,	,	kIx,	,
Anger	Anger	k1gInSc1	Anger
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
,	,	kIx,	,
Sankt	Sankt	k1gInSc4	Sankt
Kathrein	Kathrein	k2eAgInSc4d1	Kathrein
am	am	k?	am
Offenegg	Offenegg	k1gInSc4	Offenegg
na	na	k7c6	na
západě	západ	k1gInSc6	západ
a	a	k8xC	a
Gasen	Gasen	k1gInSc1	Gasen
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Územím	území	k1gNnSc7	území
od	od	k7c2	od
severu	sever	k1gInSc2	sever
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
prochází	procházet	k5eAaImIp3nP	procházet
podél	podél	k7c2	podél
řeky	řeka	k1gFnSc2	řeka
Zemská	zemský	k2eAgFnSc1d1	zemská
silnice	silnice	k1gFnSc1	silnice
B72	B72	k1gFnSc1	B72
(	(	kIx(	(
<g/>
Weizer	Weizer	k1gInSc1	Weizer
Straße	Straß	k1gInSc2	Straß
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
několik	několik	k4yIc4	několik
zemských	zemský	k2eAgFnPc2d1	zemská
silnic	silnice	k1gFnPc2	silnice
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
třídy	třída	k1gFnPc1	třída
a	a	k8xC	a
silnice	silnice	k1gFnPc1	silnice
místního	místní	k2eAgInSc2d1	místní
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
osady	osada	k1gFnSc2	osada
Birkfeld	Birkfelda	k1gFnPc2	Birkfelda
podél	podél	k7c2	podél
řeky	řeka	k1gFnSc2	řeka
i	i	k8xC	i
silnice	silnice	k1gFnSc2	silnice
B	B	kA	B
72	[number]	k4	72
vede	vést	k5eAaImIp3nS	vést
jižním	jižní	k2eAgInSc7d1	jižní
směrem	směr	k1gInSc7	směr
úzkorozchodná	úzkorozchodný	k2eAgFnSc1d1	úzkorozchodná
železniční	železniční	k2eAgFnSc1d1	železniční
dráha	dráha	k1gFnSc1	dráha
Feistritztalbahn	Feistritztalbahn	k1gMnSc1	Feistritztalbahn
(	(	kIx(	(
<g/>
rozchod	rozchod	k1gInSc1	rozchod
760	[number]	k4	760
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Trať	trať	k1gFnSc1	trať
končí	končit	k5eAaImIp3nS	končit
v	v	k7c6	v
okresním	okresní	k2eAgNnSc6d1	okresní
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Zámek	zámek	k1gInSc1	zámek
Birkenstein	Birkensteina	k1gFnPc2	Birkensteina
</s>
</p>
<p>
<s>
Cyklostezka	cyklostezka	k1gFnSc1	cyklostezka
Feistritztal-Radweg	Feistritztal-Radweg	k1gMnSc1	Feistritztal-Radweg
R8	R8	k1gMnSc1	R8
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Birkfeld	Birkfelda	k1gFnPc2	Birkfelda
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Birkfeld	Birkfelda	k1gFnPc2	Birkfelda
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
obce	obec	k1gFnSc2	obec
Birkfeld	Birkfelda	k1gFnPc2	Birkfelda
</s>
</p>
<p>
<s>
mapy	mapa	k1gFnPc1	mapa
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
