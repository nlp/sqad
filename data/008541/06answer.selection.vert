<s>
Birkfeld	Birkfeld	k6eAd1	Birkfeld
je	být	k5eAaImIp3nS	být
městys	městys	k1gInSc1	městys
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Weiz	Weiza	k1gFnPc2	Weiza
<g/>
,	,	kIx,	,
ve	v	k7c6	v
spolkové	spolkový	k2eAgFnSc6d1	spolková
zemi	zem	k1gFnSc6	zem
Štýrsko	Štýrsko	k1gNnSc1	Štýrsko
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
,	,	kIx,	,
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
Štýrského	štýrský	k2eAgInSc2d1	štýrský
Hradce	Hradec	k1gInSc2	Hradec
<g/>
.	.	kIx.	.
</s>
