<s>
Ürzig	Ürzig	k1gMnSc1
</s>
<s>
Ürzig	Ürzig	k1gMnSc1
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
58	#num#	k4
<g/>
′	′	k?
<g/>
56	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
7	#num#	k4
<g/>
°	°	k?
<g/>
0	#num#	k4
<g/>
′	′	k?
<g/>
19	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
104	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Německo	Německo	k1gNnSc1
Německo	Německo	k1gNnSc1
</s>
<s>
Ürzig	Ürzig	k1gMnSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
6,0	6,0	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
866	#num#	k4
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
143,4	143,4	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Oficiální	oficiální	k2eAgFnSc1d1
web	web	k1gInSc4
</s>
<s>
www.uerzig.de	www.uerzig.de	k6eAd1
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
06532	#num#	k4
PSČ	PSČ	kA
</s>
<s>
54539	#num#	k4
Označení	označení	k1gNnSc1
vozidel	vozidlo	k1gNnPc2
</s>
<s>
WIL	WIL	kA
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ürzig	Ürzig	k1gInSc1
je	být	k5eAaImIp3nS
německá	německý	k2eAgFnSc1d1
obec	obec	k1gFnSc1
na	na	k7c6
středním	střední	k2eAgInSc6d1
toku	tok	k1gInSc6
řeky	řeka	k1gFnSc2
Mosely	Mosela	k1gFnSc2
v	v	k7c6
zemském	zemský	k2eAgInSc6d1
okrese	okres	k1gInSc6
Bernkastel-Wittlich	Bernkastel-Wittlicha	k1gFnPc2
ve	v	k7c6
spolkové	spolkový	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
Porýní-Falc	Porýní-Falc	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
blízko	blízko	k7c2
města	město	k1gNnSc2
Bernkastel-Kues	Bernkastel-Kuesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Geografie	geografie	k1gFnSc1
</s>
<s>
,	,	kIx,
<g/>
Sluneční	sluneční	k2eAgFnPc4d1
hodiny	hodina	k1gFnPc4
ve	v	k7c6
skalní	skalní	k2eAgFnSc6d1
stěně	stěna	k1gFnSc6
nad	nad	k7c7
spolkovou	spolkový	k2eAgFnSc7d1
silnicí	silnice	k1gFnSc7
53	#num#	k4
na	na	k7c6
břehu	břeh	k1gInSc6
řeky	řeka	k1gFnSc2
Mosely	Mosela	k1gFnSc2
v	v	k7c6
Ürzigu	Ürzig	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Geografická	geografický	k2eAgFnSc1d1
poloha	poloha	k1gFnSc1
</s>
<s>
Obec	obec	k1gFnSc1
je	být	k5eAaImIp3nS
obklopena	obklopit	k5eAaPmNgFnS
vinicemi	vinice	k1gFnPc7
ve	v	k7c6
velké	velká	k1gFnSc6
zákrutu	zákrut	k1gInSc2
řeky	řeka	k1gFnSc2
Moselly	Mosella	k1gFnSc2
mezi	mezi	k7c7
městy	město	k1gNnPc7
Bernkastel-Kues	Bernkastel-Kues	k1gMnSc1
a	a	k8xC
Traben-Trarbach	Traben-Trarbach	k1gMnSc1
nedaleko	nedaleko	k7c2
města	město	k1gNnSc2
Trier	Trira	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ürzig	Ürzig	k1gInSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
levém	levý	k2eAgInSc6d1
břehu	břeh	k1gInSc6
řeky	řeka	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
k	k	k7c3
Eifelu	Eifel	k1gInSc3
stoupají	stoupat	k5eAaImIp3nP
strmé	strmý	k2eAgInPc1d1
svahy	svah	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
opačné	opačný	k2eAgFnSc6d1
straně	strana	k1gFnSc6
Mosely	Mosela	k1gFnSc2
se	se	k3xPyFc4
údolí	údolí	k1gNnSc1
rozšiřuje	rozšiřovat	k5eAaImIp3nS
do	do	k7c2
ploché	plochý	k2eAgFnSc2d1
krajiny	krajina	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
hraničí	hraničit	k5eAaImIp3nS
s	s	k7c7
Hunsrückem	Hunsrück	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Sousední	sousední	k2eAgFnPc1d1
obce	obec	k1gFnPc1
</s>
<s>
Sousedními	sousední	k2eAgFnPc7d1
obcemi	obec	k1gFnPc7
jsou	být	k5eAaImIp3nP
Bausendorf	Bausendorf	k1gInSc4
na	na	k7c6
severu	sever	k1gInSc6
<g/>
,	,	kIx,
Kinheim	Kinheim	k1gInSc1
a	a	k8xC
Erden	Erden	k1gInSc1
na	na	k7c6
východě	východ	k1gInSc6
a	a	k8xC
Zeltingen-Rachtig	Zeltingen-Rachtig	k1gMnSc1
na	na	k7c6
jihu	jih	k1gInSc6
a	a	k8xC
západě	západ	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejbližším	blízký	k2eAgNnSc7d3
větším	veliký	k2eAgNnSc7d2
městem	město	k1gNnSc7
je	být	k5eAaImIp3nS
Bernkastel-Kues	Bernkastel-Kues	k1gMnSc1
vzdálené	vzdálený	k2eAgNnSc4d1
přibližně	přibližně	k6eAd1
devět	devět	k4xCc4
kilometrů	kilometr	k1gInPc2
a	a	k8xC
okresní	okresní	k2eAgNnSc1d1
město	město	k1gNnSc1
Wittlich	Wittlich	k1gMnSc1
vzdálené	vzdálený	k2eAgNnSc4d1
přibližně	přibližně	k6eAd1
osm	osm	k4xCc4
kilometrů	kilometr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trier	Trier	k1gInSc4
jako	jako	k8xS,k8xC
největší	veliký	k2eAgNnSc4d3
město	město	k1gNnSc4
v	v	k7c6
regionu	region	k1gInSc6
je	být	k5eAaImIp3nS
vzdáleno	vzdálit	k5eAaPmNgNnS
asi	asi	k9
37	#num#	k4
kilometrů	kilometr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
Ke	k	k7c3
30	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2020	#num#	k4
žilo	žít	k5eAaImAgNnS
v	v	k7c6
obci	obec	k1gFnSc6
866	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hustota	hustota	k1gFnSc1
obyvatelstva	obyvatelstvo	k1gNnSc2
je	být	k5eAaImIp3nS
143	#num#	k4
obyvatel	obyvatel	k1gMnPc2
na	na	k7c4
kilometr	kilometr	k1gInSc4
čtvereční	čtvereční	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
regionu	region	k1gInSc6
se	se	k3xPyFc4
stále	stále	k6eAd1
mluví	mluvit	k5eAaImIp3nS
moselskou	moselský	k2eAgFnSc7d1
frančtinou	frančtina	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1
je	být	k5eAaImIp3nS
dialektem	dialekt	k1gInSc7
skupiny	skupina	k1gFnSc2
středoněmeckých	středoněmecký	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Klima	klima	k1gNnSc1
</s>
<s>
Ürzig	Ürzig	k1gInSc1
leží	ležet	k5eAaImIp3nS
v	v	k7c6
přechodové	přechodový	k2eAgFnSc6d1
zóně	zóna	k1gFnSc6
mezi	mezi	k7c7
mírným	mírný	k2eAgNnSc7d1
mořským	mořský	k2eAgNnSc7d1
podnebím	podnebí	k1gNnSc7
a	a	k8xC
kontinentálním	kontinentální	k2eAgNnSc7d1
podnebím	podnebí	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
jinými	jiný	k2eAgInPc7d1
regiony	region	k1gInPc7
v	v	k7c6
Německu	Německo	k1gNnSc6
je	být	k5eAaImIp3nS
podnebí	podnebí	k1gNnSc1
velmi	velmi	k6eAd1
teplé	teplý	k2eAgNnSc1d1
a	a	k8xC
slunečné	slunečný	k2eAgNnSc1d1
-	-	kIx~
v	v	k7c6
nedalekém	daleký	k2eNgInSc6d1
Braunebergu	Brauneberg	k1gInSc6
byla	být	k5eAaImAgFnS
naměřena	naměřen	k2eAgFnSc1d1
dne	den	k1gInSc2
11	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1998	#num#	k4
rekordní	rekordní	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
41,2	41,2	k4
<g/>
°	°	k?
C	C	kA
ve	v	k7c6
stínu	stín	k1gInSc6
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgFnSc1d3
teplota	teplota	k1gFnSc1
vzduchu	vzduch	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
v	v	k7c6
Německu	Německo	k1gNnSc6
zaznamenána	zaznamenat	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s>
Ürzig	Ürzig	k1gInSc1
leží	ležet	k5eAaImIp3nS
v	v	k7c6
dešťovém	dešťový	k2eAgInSc6d1
stínu	stín	k1gInSc6
vrchoviny	vrchovina	k1gFnSc2
Eifel	Eifela	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
kraj	kraj	k1gInSc4
chrání	chránit	k5eAaImIp3nS
před	před	k7c7
západními	západní	k2eAgInPc7d1
větry	vítr	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
je	být	k5eAaImIp3nS
oteplování	oteplování	k1gNnSc1
vzduchu	vzduch	k1gInSc2
podporováno	podporován	k2eAgNnSc1d1
nízkou	nízký	k2eAgFnSc7d1
výměnou	výměna	k1gFnSc7
vzduchu	vzduch	k1gInSc2
s	s	k7c7
okolím	okolí	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
tím	ten	k3xDgNnSc7
je	být	k5eAaImIp3nS
v	v	k7c6
důsledku	důsledek	k1gInSc6
neustálého	neustálý	k2eAgNnSc2d1
odpařování	odpařování	k1gNnSc2
vody	voda	k1gFnSc2
Mosely	Mosela	k1gFnSc2
spojena	spojit	k5eAaPmNgFnS
pravidelně	pravidelně	k6eAd1
vysoká	vysoký	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
vlhkosti	vlhkost	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
<g/>
,	,	kIx,
zejména	zejména	k9
v	v	k7c6
létě	léto	k1gNnSc6
<g/>
,	,	kIx,
vede	vést	k5eAaImIp3nS
k	k	k7c3
vlhkému	vlhký	k2eAgNnSc3d1
počasí	počasí	k1gNnSc3
a	a	k8xC
četným	četný	k2eAgFnPc3d1
bouřkám	bouřka	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
Dějiny	dějiny	k1gFnPc1
</s>
<s>
Urzig	Urzig	k1gMnSc1
<g/>
,	,	kIx,
1954	#num#	k4
</s>
<s>
V	v	k7c6
okolí	okolí	k1gNnSc6
obce	obec	k1gFnSc2
byly	být	k5eAaImAgInP
nalezeny	nalezen	k2eAgInPc1d1
zbytky	zbytek	k1gInPc1
osídlení	osídlení	k1gNnPc2
z	z	k7c2
doby	doba	k1gFnSc2
bronzové	bronzový	k2eAgFnPc4d1
<g/>
,	,	kIx,
asi	asi	k9
500	#num#	k4
let	léto	k1gNnPc2
před	před	k7c7
naším	náš	k3xOp1gInSc7
letopočtem	letopočet	k1gInSc7
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
zde	zde	k6eAd1
žil	žít	k5eAaImAgInS
keltsko-germánský	keltsko-germánský	k2eAgInSc1d1
kmen	kmen	k1gInSc1
Treverů	Trever	k1gMnPc2
<g/>
,	,	kIx,
od	od	k7c2
jejichž	jejichž	k3xOyRp3gNnSc2
jména	jméno	k1gNnSc2
je	být	k5eAaImIp3nS
odvozen	odvodit	k5eAaPmNgInS
i	i	k9
latinský	latinský	k2eAgInSc1d1
název	název	k1gInSc1
města	město	k1gNnSc2
Trevír	Trevír	k1gInSc1
<g/>
:	:	kIx,
Augusta	August	k1gMnSc2
Treverorum	Treverorum	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přibližně	přibližně	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
50	#num#	k4
před	před	k7c7
naším	náš	k3xOp1gInSc7
letopočtem	letopočet	k1gInSc7
do	do	k7c2
roku	rok	k1gInSc2
500	#num#	k4
našeho	náš	k3xOp1gInSc2
letopočtu	letopočet	k1gInSc2
kraj	kraj	k1gInSc4
ovládali	ovládat	k5eAaImAgMnP
Římané	Říman	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1946	#num#	k4
je	být	k5eAaImIp3nS
obec	obec	k1gFnSc1
součástí	součást	k1gFnPc2
tehdy	tehdy	k6eAd1
nově	nově	k6eAd1
založené	založený	k2eAgFnPc4d1
spolkové	spolkový	k2eAgFnPc4d1
země	zem	k1gFnPc4
Porýní-Falc	Porýní-Falc	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Politika	politika	k1gFnSc1
</s>
<s>
Obecní	obecní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
</s>
<s>
Místní	místní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
v	v	k7c6
Ürzigu	Ürzig	k1gInSc6
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
dvanácti	dvanáct	k4xCc2
členů	člen	k1gInPc2
rady	rada	k1gFnSc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
byli	být	k5eAaImAgMnP
zvoleni	zvolit	k5eAaPmNgMnP
v	v	k7c6
komunálních	komunální	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
26	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Partnerství	partnerství	k1gNnSc1
farností	farnost	k1gFnPc2
</s>
<s>
Místní	místní	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Ürzig	Ürziga	k1gFnPc2
udržuje	udržovat	k5eAaImIp3nS
partnerství	partnerství	k1gNnSc1
s	s	k7c7
farností	farnost	k1gFnSc7
Aloxe-Corton	Aloxe-Corton	k1gInSc1
ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Kultura	kultura	k1gFnSc1
a	a	k8xC
památky	památka	k1gFnPc1
</s>
<s>
Obytný	obytný	k2eAgInSc1d1
dům	dům	k1gInSc1
Mönchshof	Mönchshof	k1gInSc1
</s>
<s>
Památky	památka	k1gFnPc1
</s>
<s>
Historické	historický	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
města	město	k1gNnSc2
Ürzig	Ürzig	k1gInSc1
se	se	k3xPyFc4
vyznačuje	vyznačovat	k5eAaImIp3nS
malými	malý	k2eAgFnPc7d1
uličkami	ulička	k1gFnPc7
a	a	k8xC
zákoutími	zákoutí	k1gNnPc7
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
starými	starý	k2eAgInPc7d1
patricijskými	patricijský	k2eAgInPc7d1
a	a	k8xC
hrázděnými	hrázděný	k2eAgInPc7d1
domy	dům	k1gInPc7
ze	z	k7c2
16	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celé	celý	k2eAgNnSc4d1
historické	historický	k2eAgNnSc4d1
centrum	centrum	k1gNnSc4
s	s	k7c7
katolickým	katolický	k2eAgInSc7d1
farním	farní	k2eAgInSc7d1
kostelem	kostel	k1gInSc7
svatého	svatý	k1gMnSc2
Materna	Materna	k1gMnSc1
a	a	k8xC
hrázděnými	hrázděný	k2eAgInPc7d1
domy	dům	k1gInPc7
na	na	k7c6
náměstí	náměstí	k1gNnSc6
radnice	radnice	k1gFnSc2
bylo	být	k5eAaImAgNnS
proto	proto	k8xC
zařazeno	zařadit	k5eAaPmNgNnS
pod	pod	k7c4
památkovou	památkový	k2eAgFnSc4d1
ochranu	ochrana	k1gFnSc4
jako	jako	k8xS,k8xC
památková	památkový	k2eAgFnSc1d1
zóna	zóna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Rytířské	rytířský	k2eAgFnPc1d1
rodiny	rodina	k1gFnPc1
kdysi	kdysi	k6eAd1
postavily	postavit	k5eAaPmAgFnP
v	v	k7c6
okolí	okolí	k1gNnSc6
obce	obec	k1gFnSc2
tři	tři	k4xCgInPc4
hrady	hrad	k1gInPc4
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
jen	jen	k9
jeden	jeden	k4xCgMnSc1
se	se	k3xPyFc4
zachoval	zachovat	k5eAaPmAgInS
do	do	k7c2
současnosti	současnost	k1gFnSc2
jako	jako	k8xS,k8xC
zřícenina	zřícenina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
zbytcích	zbytek	k1gInPc6
bývalé	bývalý	k2eAgFnSc2d1
strážní	strážní	k2eAgFnSc2d1
věže	věž	k1gFnSc2
ve	v	k7c6
vinicích	vinice	k1gFnPc6
jsou	být	k5eAaImIp3nP
umístěny	umístit	k5eAaPmNgFnP
sluneční	sluneční	k2eAgFnPc1d1
hodiny	hodina	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Ürziger	Ürziger	k1gInSc1
Gewürzgarten	Gewürzgarten	k2eAgInSc1d1
</s>
<s>
Pohled	pohled	k1gInSc1
na	na	k7c4
vinice	vinice	k1gFnPc4
Ürziger	Ürzigero	k1gNnPc2
Würzgarten	Würzgarten	k2eAgInSc1d1
</s>
<s>
V	v	k7c6
červnu	červen	k1gInSc6
2005	#num#	k4
byla	být	k5eAaImAgFnS
jako	jako	k9
regionální	regionální	k2eAgFnSc1d1
atrakce	atrakce	k1gFnSc1
slavnostně	slavnostně	k6eAd1
otevřena	otevřen	k2eAgFnSc1d1
zahrada	zahrada	k1gFnSc1
Ürziger	Ürziger	k1gMnSc1
Gewürzgarten	Gewürzgarten	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Projekt	projekt	k1gInSc4
zadaný	zadaný	k2eAgInSc4d1
kulturním	kulturní	k2eAgInSc7d1
úřadem	úřad	k1gInSc7
Bernkastel-Kues	Bernkastel-Kuesa	k1gFnPc2
je	být	k5eAaImIp3nS
středomořská	středomořský	k2eAgFnSc1d1
bylinková	bylinkový	k2eAgFnSc1d1
zahrada	zahrada	k1gFnSc1
na	na	k7c6
exponovaném	exponovaný	k2eAgNnSc6d1
strmém	strmý	k2eAgNnSc6d1
místě	místo	k1gNnSc6
v	v	k7c4
Ürziger	Ürziger	k1gInSc4
Würzgarten	Würzgarten	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
tam	tam	k6eAd1
roste	růst	k5eAaImIp3nS
a	a	k8xC
kvete	kvést	k5eAaImIp3nS
10	#num#	k4
000	#num#	k4
rostlin	rostlina	k1gFnPc2
více	hodně	k6eAd2
než	než	k8xS
160	#num#	k4
druhů	druh	k1gInPc2
<g/>
,	,	kIx,
tisíce	tisíc	k4xCgInPc1
cibulovin	cibulovina	k1gFnPc2
<g/>
,	,	kIx,
původních	původní	k2eAgInPc2d1
keřů	keř	k1gInPc2
<g/>
,	,	kIx,
divokých	divoký	k2eAgFnPc2d1
a	a	k8xC
historických	historický	k2eAgFnPc2d1
růží	růž	k1gFnPc2
i	i	k8xC
tradičních	tradiční	k2eAgFnPc2d1
léčivých	léčivý	k2eAgFnPc2d1
a	a	k8xC
aromatických	aromatický	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1
a	a	k8xC
infrastruktura	infrastruktura	k1gFnSc1
</s>
<s>
Nejdůležitějšími	důležitý	k2eAgInPc7d3
hospodářskými	hospodářský	k2eAgInPc7d1
odvětvími	odvětví	k1gNnPc7
jsou	být	k5eAaImIp3nP
vinařství	vinařství	k1gNnSc1
a	a	k8xC
cestovní	cestovní	k2eAgInSc1d1
ruch	ruch	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Vinařství	vinařství	k1gNnSc1
</s>
<s>
V	v	k7c6
Ürzigu	Ürzig	k1gInSc6
existuje	existovat	k5eAaImIp3nS
několik	několik	k4yIc1
vinařství	vinařství	k1gNnPc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
na	na	k7c6
strmém	strmý	k2eAgInSc6d1
svahu	svah	k1gInSc6
pěstují	pěstovat	k5eAaImIp3nP
hlavně	hlavně	k9
Ryzlink	ryzlink	k1gInSc4
rýnský	rýnský	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejznámější	známý	k2eAgFnSc7d3
vinařskou	vinařský	k2eAgFnSc7d1
lokalitou	lokalita	k1gFnSc7
je	být	k5eAaImIp3nS
vinice	vinice	k1gFnSc1
Ürziger	Ürziger	k1gMnSc1
Würzgarten	Würzgarten	k2eAgMnSc1d1
<g/>
;	;	kIx,
další	další	k2eAgFnPc1d1
vinice	vinice	k1gFnPc1
jsou	být	k5eAaImIp3nP
například	například	k6eAd1
Ürziger	Ürziger	k1gMnSc1
Goldwingert	Goldwingert	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patří	patřit	k5eAaImIp3nS
do	do	k7c2
vinařské	vinařský	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
Mosel-Saar-Ruwer	Mosel-Saar-Ruwra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Cestovní	cestovní	k2eAgInSc1d1
ruch	ruch	k1gInSc1
</s>
<s>
Obec	obec	k1gFnSc1
je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
ze	z	k7c2
zastávek	zastávka	k1gFnPc2
turistické	turistický	k2eAgFnSc2d1
stezky	stezka	k1gFnSc2
Moselsteig	Moselsteiga	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
dlouhá	dlouhý	k2eAgFnSc1d1
242	#num#	k4
kilometrů	kilometr	k1gInPc2
a	a	k8xC
vede	vést	k5eAaImIp3nS
z	z	k7c2
Perlu	perl	k1gInSc2
na	na	k7c6
německo-francouzské	německo-francouzský	k2eAgFnSc6d1
hranici	hranice	k1gFnSc6
do	do	k7c2
Koblenzu	Koblenz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ürzig	Ürzig	k1gInSc1
je	být	k5eAaImIp3nS
etapovou	etapový	k2eAgFnSc7d1
obcí	obec	k1gFnSc7
mezi	mezi	k7c7
11	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
12	#num#	k4
<g/>
.	.	kIx.
etapou	etapa	k1gFnSc7
této	tento	k3xDgFnSc2
trasy	trasa	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Doprava	doprava	k1gFnSc1
</s>
<s>
Domy	dům	k1gInPc1
Rathausplatz	Rathausplatz	k1gInSc1
7	#num#	k4
a	a	k8xC
Rathausplatz	Rathausplatz	k1gInSc1
10	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
nalézá	nalézat	k5eAaImIp3nS
středisko	středisko	k1gNnSc1
turistických	turistický	k2eAgFnPc2d1
informací	informace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Silniční	silniční	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
</s>
<s>
Ürzig	Ürzig	k1gInSc1
leží	ležet	k5eAaImIp3nS
na	na	k7c6
spolkové	spolkový	k2eAgFnSc6d1
silnici	silnice	k1gFnSc6
číslo	číslo	k1gNnSc1
53	#num#	k4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
vede	vést	k5eAaImIp3nS
podél	podél	k7c2
Mosely	Mosela	k1gFnSc2
z	z	k7c2
Trevíru	Trevír	k1gInSc2
do	do	k7c2
Alf	alfa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bezprostředně	bezprostředně	k6eAd1
poblíž	poblíž	k7c2
Ürzigu	Ürzig	k1gInSc2
byl	být	k5eAaImAgInS
realizován	realizovat	k5eAaBmNgInS
největší	veliký	k2eAgInSc1d3
mostní	mostní	k2eAgInSc1d1
projekt	projekt	k1gInSc1
v	v	k7c6
Německu	Německo	k1gNnSc6
na	na	k7c6
spolkové	spolkový	k2eAgFnSc6d1
silnici	silnice	k1gFnSc6
B	B	kA
<g/>
50	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Most	most	k1gInSc1
(	(	kIx(
<g/>
Hochmoselbrücke	Hochmoselbrücke	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
1,7	1,7	k4
kilometrů	kilometr	k1gInPc2
dlouhý	dlouhý	k2eAgMnSc1d1
a	a	k8xC
158	#num#	k4
metrů	metr	k1gInPc2
vysoký	vysoký	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komunikace	komunikace	k1gFnSc1
je	být	k5eAaImIp3nS
čtyřproudá	čtyřproudý	k2eAgFnSc1d1
se	s	k7c7
dvěma	dva	k4xCgInPc7
odstavnými	odstavný	k2eAgInPc7d1
pruhy	pruh	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavba	stavba	k1gFnSc1
byla	být	k5eAaImAgFnS
zahájena	zahájit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
<g/>
,	,	kIx,
otevření	otevření	k1gNnSc1
mostu	most	k1gInSc2
bylo	být	k5eAaImAgNnS
plánováno	plánovat	k5eAaImNgNnS
na	na	k7c4
rok	rok	k1gInSc4
2016	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Most	most	k1gInSc1
byl	být	k5eAaImAgInS
dokončen	dokončit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Lodní	lodní	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
</s>
<s>
Obec	obec	k1gFnSc1
má	mít	k5eAaImIp3nS
říční	říční	k2eAgFnSc1d1
přistav	přistavit	k5eAaPmRp2nS
pro	pro	k7c4
přepravu	přeprava	k1gFnSc4
po	po	k7c6
Moselle	Mosella	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
května	květen	k1gInSc2
do	do	k7c2
října	říjen	k1gInSc2
zajišťuje	zajišťovat	k5eAaImIp3nS
pravidelnou	pravidelný	k2eAgFnSc4d1
říční	říční	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
společnost	společnost	k1gFnSc1
Mosel-Schiffs-Touristik	Mosel-Schiffs-Touristika	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
</s>
<s>
Vlakové	vlakový	k2eAgNnSc1d1
nádraží	nádraží	k1gNnSc1
v	v	k7c6
Ürzigu	Ürzig	k1gInSc6
je	být	k5eAaImIp3nS
asi	asi	k9
tři	tři	k4xCgInPc4
kilometry	kilometr	k1gInPc4
od	od	k7c2
obce	obec	k1gFnSc2
na	na	k7c6
moselské	moselský	k2eAgFnSc6d1
železniční	železniční	k2eAgFnSc6d1
trati	trať	k1gFnSc6
(	(	kIx(
<g/>
Moselstrecke	Moselstrecke	k1gInSc1
<g/>
)	)	kIx)
mezi	mezi	k7c7
Trevírem	Trevír	k1gInSc7
a	a	k8xC
Koblenzí	Koblenze	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Přeprava	přeprava	k1gFnSc1
</s>
<s>
Obec	obec	k1gFnSc1
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
Trier	Trier	k1gInSc4
Region	region	k1gInSc1
Transport	transport	k1gInSc1
Association	Association	k1gInSc1
(	(	kIx(
<g/>
VRT	vrt	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Ürzig	Ürziga	k1gFnPc2
na	na	k7c6
německé	německý	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Gemeindeverzeichnis	Gemeindeverzeichnis	k1gInSc1
|	|	kIx~
Statistikportal	Statistikportal	k1gFnSc1
<g/>
.	.	kIx.
<g/>
de	de	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Statistische	Statistisch	k1gInSc2
Ämter	Ämter	k1gMnSc1
des	des	k1gNnSc2
Bundes	Bundes	k1gMnSc1
und	und	k?
der	drát	k5eAaImRp2nS
Länder	Länder	k1gMnSc1
|	|	kIx~
Gemeinsames	Gemeinsames	k1gMnSc1
Statistikportal	Statistikportal	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Der	drát	k5eAaImRp2nS
Landeswahlleiter	Landeswahlleiter	k1gMnSc1
Rheinland-Pfalz	Rheinland-Pfalz	k1gMnSc1
<g/>
:	:	kIx,
Kommunalwahl	Kommunalwahl	k1gFnSc1
2019	#num#	k4
<g/>
,	,	kIx,
Stadt-	Stadt-	k1gMnSc1
und	und	k?
Gemeinderatswahlen	Gemeinderatswahlen	k2eAgMnSc1d1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Der	drát	k5eAaImRp2nS
Landeswahlleiter	Landeswahlleiter	k1gMnSc1
Rheinland-Pfalz	Rheinland-Pfalz	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Ürziger	Ürziger	k1gInSc1
Gewürzgarten	Gewürzgarten	k2eAgInSc1d1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gemeinede	Gemeined	k1gMnSc5
Ürzig	Ürzig	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Etappen	Etappen	k2eAgInSc4d1
des	des	k1gNnPc6
Moselsteigs	Moselsteigs	k1gInSc4
<g/>
.	.	kIx.
www.moselsteig.de	www.moselsteig.de	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Infrastruktur	infrastruktura	k1gFnPc2
<g/>
:	:	kIx,
Das	Das	k1gFnSc1
sind	sind	k1gInSc1
die	die	k?
größten	größten	k2eAgInSc1d1
Brücken-Bauprojekte	Brücken-Bauprojekt	k1gInSc5
-	-	kIx~
DER	drát	k5eAaImRp2nS
SPIEGEL	SPIEGEL	kA
-	-	kIx~
Mobilität	Mobilität	k1gMnSc1
<g/>
.	.	kIx.
www.spiegel.de	www.spiegel.de	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
DER	drát	k5eAaImRp2nS
SPIEGEL	SPIEGEL	kA
GmbH	GmbH	k1gMnSc2
&	&	k?
Co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
KG	kg	kA
<g/>
,	,	kIx,
2018-04-16	2018-04-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Die	Die	k1gFnSc1
Hochmoselbrücke	Hochmoselbrücke	k1gFnSc1
-	-	kIx~
das	das	k?
Herzstück	Herzstück	k1gInSc1
<g/>
.	.	kIx.
www.hochmoseluebergang.rlp.de	www.hochmoseluebergang.rlp.de	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Landesbetrieb	Landesbetriba	k1gFnPc2
Mobilität	Mobilität	k1gMnSc1
Rheinland-Pfalz	Rheinland-Pfalz	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Ürzig	Ürziga	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
obce	obec	k1gFnSc2
Ürzig	Ürziga	k1gFnPc2
</s>
<s>
Ürzigerské	Ürzigerský	k2eAgFnPc4d1
vinice	vinice	k1gFnPc4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4875640-4	4875640-4	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
248753629	#num#	k4
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Německo	Německo	k1gNnSc1
</s>
