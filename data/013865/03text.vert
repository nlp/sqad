<s>
Porýní-Falc	Porýní-Falc	k6eAd1
</s>
<s>
Porýní-Falc	Porýní-Falc	k1gFnSc1
Rheinland-Pfalz	Rheinland-Pfalza	k1gFnPc2
Mohučská	mohučský	k2eAgFnSc1d1
katedrála	katedrála	k1gFnSc1
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
Geografie	geografie	k1gFnSc1
</s>
<s>
Porýní-Falc	Porýní-Falc	k6eAd1
na	na	k7c6
mapě	mapa	k1gFnSc6
Německa	Německo	k1gNnSc2
Hlavní	hlavní	k2eAgFnSc6d1
město	město	k1gNnSc1
</s>
<s>
Mohuč	Mohuč	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
54	#num#	k4
<g/>
′	′	k?
<g/>
47	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
7	#num#	k4
<g/>
°	°	k?
<g/>
26	#num#	k4
<g/>
′	′	k?
<g/>
59	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Rozloha	rozloha	k1gFnSc1
</s>
<s>
19	#num#	k4
854,21	854,21	k4
km²	km²	k?
Časové	časový	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
</s>
<s>
+1	+1	k4
Geodata	Geodata	k1gFnSc1
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
Obyvatelstvo	obyvatelstvo	k1gNnSc1
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
4	#num#	k4
003	#num#	k4
000	#num#	k4
(	(	kIx(
<g/>
31	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
201,6	201,6	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
regionu	region	k1gInSc2
Nadřazený	nadřazený	k2eAgInSc1d1
celek	celek	k1gInSc1
</s>
<s>
Německo	Německo	k1gNnSc1
Druh	druh	k1gInSc1
celku	celek	k1gInSc3
</s>
<s>
Spolkový	spolkový	k2eAgInSc4d1
stát	stát	k1gInSc4
Předseda	předseda	k1gMnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
Malu	Malu	k6eAd1
Dreyerová	Dreyerová	k1gFnSc1
(	(	kIx(
<g/>
SPD	SPD	kA
<g/>
)	)	kIx)
Vláda	vláda	k1gFnSc1
</s>
<s>
SPD	SPD	kA
<g/>
,	,	kIx,
Bündnis	Bündnis	k1gFnSc1
90	#num#	k4
<g/>
/	/	kIx~
<g/>
Die	Die	k1gFnSc1
Grünen	Grünna	k1gFnPc2
Mezinárodní	mezinárodní	k2eAgFnSc2d1
identifikace	identifikace	k1gFnSc2
ISO	ISO	kA
3166-2	3166-2	k4
</s>
<s>
DE-RP	DE-RP	k?
Oficiální	oficiální	k2eAgInSc4d1
web	web	k1gInSc4
</s>
<s>
www.rlp.de	www.rlp.de	k6eAd1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Porýní-Falc	Porýní-Falc	k1gFnSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Rheinland-Pfalz	Rheinland-Pfalz	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
spolková	spolkový	k2eAgFnSc1d1
země	země	k1gFnSc1
rozkládající	rozkládající	k2eAgFnSc1d1
se	se	k3xPyFc4
na	na	k7c6
západě	západ	k1gInSc6
Spolkové	spolkový	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
Německo	Německo	k1gNnSc1
po	po	k7c6
obou	dva	k4xCgInPc6
březích	břeh	k1gInPc6
Rýna	Rýn	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hraničí	hraničit	k5eAaImIp3nS
s	s	k7c7
Belgií	Belgie	k1gFnSc7
<g/>
,	,	kIx,
Lucemburskem	Lucembursko	k1gNnSc7
a	a	k8xC
Francií	Francie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgNnSc7d1
a	a	k8xC
největším	veliký	k2eAgNnSc7d3
městem	město	k1gNnSc7
je	být	k5eAaImIp3nS
Mohuč	Mohuč	k1gFnSc1
(	(	kIx(
<g/>
Mainz	Mainz	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Historický	historický	k2eAgInSc1d1
přehled	přehled	k1gInSc1
</s>
<s>
Spolková	spolkový	k2eAgFnSc1d1
země	země	k1gFnSc1
Porýní-Falc	Porýní-Falc	k1gFnSc4
byla	být	k5eAaImAgFnS
ustavena	ustaven	k2eAgFnSc1d1
30	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1946	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
vytvořena	vytvořit	k5eAaPmNgFnS
na	na	k7c6
severu	sever	k1gInSc6
francouzské	francouzský	k2eAgFnSc2d1
okupační	okupační	k2eAgFnSc2d1
zóny	zóna	k1gFnSc2
z	z	k7c2
větší	veliký	k2eAgFnSc2d2
části	část	k1gFnSc2
dosud	dosud	k6eAd1
bavorské	bavorský	k2eAgFnSc2d1
Rýnské	rýnský	k2eAgFnSc2d1
Falce	Falc	k1gFnSc2
<g/>
,	,	kIx,
jižních	jižní	k2eAgFnPc2d1
částí	část	k1gFnPc2
pruské	pruský	k2eAgFnSc2d1
provincie	provincie	k1gFnSc2
Porýní	Porýní	k1gNnSc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
území	území	k1gNnSc2
zemského	zemský	k2eAgInSc2d1
okresu	okres	k1gInSc2
Birkenfeld	Birkenfelda	k1gFnPc2
<g/>
,	,	kIx,
náležejícího	náležející	k2eAgInSc2d1
původně	původně	k6eAd1
k	k	k7c3
Oldenbursku	Oldenbursek	k1gInSc3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
západní	západní	k2eAgFnSc2d1
části	část	k1gFnSc2
pruské	pruský	k2eAgFnSc2d1
provincie	provincie	k1gFnSc2
Nasavska	Nasavsko	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
části	část	k1gFnPc1
Hesenska-Darmstadtska	Hesenska-Darmstadtsk	k1gInSc2
(	(	kIx(
<g/>
části	část	k1gFnSc2
provincie	provincie	k1gFnSc2
Rýnské	rýnský	k2eAgNnSc1d1
Hesensko	Hesensko	k1gNnSc1
ležící	ležící	k2eAgNnSc1d1
na	na	k7c6
západním	západní	k2eAgInSc6d1
břehu	břeh	k1gInSc6
Rýna	Rýn	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vytvoření	vytvoření	k1gNnSc1
nové	nový	k2eAgFnSc2d1
země	zem	k1gFnSc2
bylo	být	k5eAaImAgNnS
poté	poté	k6eAd1
schváleno	schválit	k5eAaPmNgNnS
referendem	referendum	k1gNnSc7
dne	den	k1gInSc2
18	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1947	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Administrativní	administrativní	k2eAgNnSc1d1
členění	členění	k1gNnSc1
</s>
<s>
Zemské	zemský	k2eAgInPc1d1
okresy	okres	k1gInPc1
</s>
<s>
Ahrweiler	Ahrweiler	k1gMnSc1
</s>
<s>
Altenkirchen	Altenkirchen	k1gInSc1
(	(	kIx(
<g/>
Westerwald	Westerwald	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Alzey-Worms	Alzey-Worms	k6eAd1
</s>
<s>
Bad	Bad	k?
Dürkheim	Dürkheim	k1gInSc1
</s>
<s>
Bad	Bad	k?
Kreuznach	Kreuznach	k1gInSc1
</s>
<s>
Bernkastel-Wittlich	Bernkastel-Wittlich	k1gMnSc1
</s>
<s>
Birkenfeld	Birkenfeld	k6eAd1
</s>
<s>
Cochem-Zell	Cochem-Zell	k1gMnSc1
</s>
<s>
Donnersberg	Donnersberg	k1gMnSc1
</s>
<s>
Eifel	Eifel	k1gMnSc1
Bitburg-Prüm	Bitburg-Prüm	k1gMnSc1
</s>
<s>
Germersheim	Germersheim	k6eAd1
</s>
<s>
Kaiserslautern	Kaiserslautern	k1gMnSc1
</s>
<s>
Kusel	Kusel	k1gMnSc1
</s>
<s>
Mainz-Bingen	Mainz-Bingen	k1gInSc1
</s>
<s>
Mayen-Koblenz	Mayen-Koblenz	k1gMnSc1
</s>
<s>
Neuwied	Neuwied	k1gMnSc1
</s>
<s>
Rhein-Hunsrück	Rhein-Hunsrück	k6eAd1
</s>
<s>
Rhein-Lahn	Rhein-Lahn	k1gMnSc1
</s>
<s>
Rhein-Pfalz	Rhein-Pfalz	k1gMnSc1
</s>
<s>
Südliche	Südliche	k1gFnSc1
Weinstraße	Weinstraß	k1gFnSc2
</s>
<s>
Südwestpfalz	Südwestpfalz	k1gMnSc1
</s>
<s>
Trier-Saarburg	Trier-Saarburg	k1gMnSc1
</s>
<s>
Vulkaneifel	Vulkaneifel	k1gMnSc1
</s>
<s>
Westerwald	Westerwald	k6eAd1
</s>
<s>
Městské	městský	k2eAgInPc1d1
okresy	okres	k1gInPc1
</s>
<s>
Frankenthal	Frankenthal	k1gMnSc1
</s>
<s>
Kaiserslautern	Kaiserslautern	k1gMnSc1
</s>
<s>
Koblenz	Koblenz	k1gMnSc1
</s>
<s>
Landau	Landau	k6eAd1
in	in	k?
der	drát	k5eAaImRp2nS
Pfalz	Pfalz	k1gMnSc1
</s>
<s>
Ludwigshafen	Ludwigshafen	k2eAgInSc1d1
am	am	k?
Rhein	Rhein	k1gInSc1
</s>
<s>
Mohuč	Mohuč	k1gFnSc1
</s>
<s>
Neustadt	Neustadt	k1gInSc1
an	an	k?
der	drát	k5eAaImRp2nS
Weinstraße	Weinstraß	k1gFnPc4
</s>
<s>
Pirmasens	Pirmasens	k6eAd1
</s>
<s>
Špýr	Špýr	k1gMnSc1
</s>
<s>
Trevír	Trevír	k1gInSc1
</s>
<s>
Worms	Worms	k6eAd1
</s>
<s>
Zweibrücken	Zweibrücken	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Porýní-Falc	Porýní-Falc	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Porýní-Falc	Porýní-Falc	k1gFnSc1
na	na	k7c4
OpenStreetMap	OpenStreetMap	k1gInSc4
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Německo	Německo	k1gNnSc1
–	–	k?
Deutschland	Deutschlando	k1gNnPc2
–	–	k?
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
Spolkové	spolkový	k2eAgFnSc2d1
země	zem	k1gFnSc2
Německa	Německo	k1gNnSc2
a	a	k8xC
jejich	jejich	k3xOp3gNnPc4
hlavní	hlavní	k2eAgNnPc4d1
města	město	k1gNnPc4
</s>
<s>
Bádensko-Württembersko	Bádensko-Württembersko	k1gNnSc1
(	(	kIx(
<g/>
Stuttgart	Stuttgart	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Svobodný	svobodný	k2eAgInSc1d1
stát	stát	k1gInSc1
Bavorsko	Bavorsko	k1gNnSc1
(	(	kIx(
<g/>
Mnichov	Mnichov	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Berlín	Berlín	k1gInSc1
•	•	k?
Braniborsko	Braniborsko	k1gNnSc1
(	(	kIx(
<g/>
Postupim	Postupim	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Svobodné	svobodný	k2eAgFnSc3d1
hanzovní	hanzovní	k2eAgFnSc3d1
město	město	k1gNnSc4
Brémy	Brémy	k1gFnPc1
(	(	kIx(
<g/>
Brémy	Brémy	k1gFnPc1
<g/>
)	)	kIx)
•	•	k?
Dolní	dolní	k2eAgNnSc1d1
Sasko	Sasko	k1gNnSc1
(	(	kIx(
<g/>
Hannover	Hannover	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Svobodný	svobodný	k2eAgInSc1d1
stát	stát	k1gInSc1
Durynsko	Durynsko	k1gNnSc1
(	(	kIx(
<g/>
Erfurt	Erfurt	k1gInSc1
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Svobodné	svobodný	k2eAgNnSc1d1
a	a	k8xC
hanzovní	hanzovní	k2eAgNnSc1d1
město	město	k1gNnSc1
Hamburk	Hamburk	k1gInSc1
(	(	kIx(
<g/>
Hamburk	Hamburk	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Hesensko	Hesensko	k1gNnSc1
(	(	kIx(
<g/>
Wiesbaden	Wiesbaden	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Meklenbursko-Přední	meklenbursko-přední	k2eAgNnSc4d1
Pomořansko	Pomořansko	k1gNnSc4
(	(	kIx(
<g/>
Schwerin	Schwerin	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Porýní-Falc	Porýní-Falc	k1gInSc1
(	(	kIx(
<g/>
Mohuč	Mohuč	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Sársko	Sársko	k1gNnSc1
(	(	kIx(
<g/>
Saarbrücken	Saarbrücken	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Svobodný	svobodný	k2eAgInSc1d1
stát	stát	k1gInSc1
Sasko	Sasko	k1gNnSc1
(	(	kIx(
<g/>
Drážďany	Drážďany	k1gInPc1
<g/>
)	)	kIx)
•	•	k?
Sasko-Anhaltsko	Sasko-Anhaltsko	k1gNnSc1
(	(	kIx(
<g/>
Magdeburg	Magdeburg	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Severní	severní	k2eAgNnSc4d1
Porýní-Vestfálsko	Porýní-Vestfálsko	k1gNnSc4
(	(	kIx(
<g/>
Düsseldorf	Düsseldorf	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Šlesvicko-Holštýnsko	Šlesvicko-Holštýnsko	k1gNnSc1
(	(	kIx(
<g/>
Kiel	Kiel	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
134436	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4049795-1	4049795-1	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
81035110	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
123162663	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
81035110	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Německo	Německo	k1gNnSc1
</s>
