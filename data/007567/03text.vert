<s>
Karibská	karibský	k2eAgFnSc1d1	karibská
krize	krize	k1gFnSc1	krize
(	(	kIx(	(
<g/>
též	též	k9	též
Kubánská	kubánský	k2eAgFnSc1d1	kubánská
krize	krize	k1gFnSc1	krize
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
politická	politický	k2eAgFnSc1d1	politická
krize	krize	k1gFnSc1	krize
<g/>
.	.	kIx.	.
</s>
<s>
Hrozilo	hrozit	k5eAaImAgNnS	hrozit
<g/>
,	,	kIx,	,
že	že	k8xS	že
přeroste	přerůst	k5eAaPmIp3nS	přerůst
v	v	k7c4	v
jaderný	jaderný	k2eAgInSc4d1	jaderný
konflikt	konflikt	k1gInSc4	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
Vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
rozmístění	rozmístění	k1gNnSc2	rozmístění
sovětských	sovětský	k2eAgFnPc2d1	sovětská
raket	raketa	k1gFnPc2	raketa
středního	střední	k2eAgInSc2d1	střední
doletu	dolet	k1gInSc2	dolet
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
SSSR	SSSR	kA	SSSR
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
na	na	k7c4	na
umístění	umístění	k1gNnSc4	umístění
amerických	americký	k2eAgFnPc2d1	americká
raket	raketa	k1gFnPc2	raketa
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
vyhlásily	vyhlásit	k5eAaPmAgInP	vyhlásit
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
blokádu	blokáda	k1gFnSc4	blokáda
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
zabránit	zabránit	k5eAaPmF	zabránit
dopravení	dopravení	k1gNnSc4	dopravení
dalších	další	k2eAgFnPc2d1	další
raket	raketa	k1gFnPc2	raketa
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
sovětská	sovětský	k2eAgNnPc1d1	sovětské
plavidla	plavidlo	k1gNnPc1	plavidlo
se	se	k3xPyFc4	se
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
kontrolované	kontrolovaný	k2eAgFnSc2d1	kontrolovaná
zóny	zóna	k1gFnSc2	zóna
skutečně	skutečně	k6eAd1	skutečně
zastavila	zastavit	k5eAaPmAgFnS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dalších	další	k2eAgNnPc6d1	další
jednáních	jednání	k1gNnPc6	jednání
byly	být	k5eAaImAgFnP	být
odstraněny	odstranit	k5eAaPmNgInP	odstranit
i	i	k9	i
již	již	k6eAd1	již
instalované	instalovaný	k2eAgFnPc4d1	instalovaná
rakety	raketa	k1gFnPc4	raketa
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
americké	americký	k2eAgInPc4d1	americký
se	se	k3xPyFc4	se
zavázaly	zavázat	k5eAaPmAgInP	zavázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nenapadnou	napadnout	k5eNaPmIp3nP	napadnout
Kubu	Kuba	k1gFnSc4	Kuba
a	a	k8xC	a
že	že	k8xS	že
stáhnou	stáhnout	k5eAaPmIp3nP	stáhnout
svoje	svůj	k3xOyFgFnPc4	svůj
rakety	raketa	k1gFnPc4	raketa
středního	střední	k2eAgInSc2d1	střední
doletu	dolet	k1gInSc2	dolet
z	z	k7c2	z
Turecka	Turecko	k1gNnSc2	Turecko
(	(	kIx(	(
<g/>
taktické	taktický	k2eAgFnSc2d1	taktická
jaderné	jaderný	k2eAgFnSc2d1	jaderná
pumy	puma	k1gFnSc2	puma
B61	B61	k1gFnSc2	B61
tam	tam	k6eAd1	tam
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
dodnes	dodnes	k6eAd1	dodnes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
významným	významný	k2eAgInSc7d1	významný
důsledkem	důsledek	k1gInSc7	důsledek
této	tento	k3xDgFnSc2	tento
krize	krize	k1gFnSc2	krize
bylo	být	k5eAaImAgNnS	být
zavedení	zavedení	k1gNnSc1	zavedení
horké	horký	k2eAgFnSc2d1	horká
linky	linka	k1gFnSc2	linka
mezi	mezi	k7c7	mezi
Bílým	bílý	k2eAgInSc7d1	bílý
domem	dům	k1gInSc7	dům
a	a	k8xC	a
Kremlem	Kreml	k1gInSc7	Kreml
<g/>
.	.	kIx.	.
</s>
<s>
Události	událost	k1gFnPc1	událost
vedoucí	vedoucí	k1gFnSc2	vedoucí
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
krizi	krize	k1gFnSc3	krize
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
odehrávat	odehrávat	k5eAaImF	odehrávat
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
moci	moct	k5eAaImF	moct
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
za	za	k7c2	za
podpory	podpora	k1gFnSc2	podpora
americké	americký	k2eAgFnSc2d1	americká
zpravodajské	zpravodajský	k2eAgFnSc2d1	zpravodajská
služby	služba	k1gFnSc2	služba
CIA	CIA	kA	CIA
zmocnil	zmocnit	k5eAaPmAgMnS	zmocnit
diktátor	diktátor	k1gMnSc1	diktátor
Fulgencio	Fulgencio	k1gMnSc1	Fulgencio
Batista	Batista	k1gMnSc1	Batista
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
okamžitě	okamžitě	k6eAd1	okamžitě
se	se	k3xPyFc4	se
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
opozice	opozice	k1gFnSc1	opozice
proti	proti	k7c3	proti
jeho	jeho	k3xOp3gFnSc3	jeho
vládě	vláda	k1gFnSc3	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
zformovala	zformovat	k5eAaPmAgFnS	zformovat
do	do	k7c2	do
Hnutí	hnutí	k1gNnSc2	hnutí
mládeže	mládež	k1gFnSc2	mládež
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
Hnutí	hnutí	k1gNnSc2	hnutí
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
jehož	jenž	k3xRgNnSc2	jenž
čela	čelo	k1gNnSc2	čelo
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
mladý	mladý	k2eAgMnSc1d1	mladý
levicově	levicově	k6eAd1	levicově
orientovaný	orientovaný	k2eAgMnSc1d1	orientovaný
právník	právník	k1gMnSc1	právník
Fidel	Fidel	k1gMnSc1	Fidel
Castro	Castro	k1gNnSc4	Castro
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
za	za	k7c4	za
svoji	svůj	k3xOyFgFnSc4	svůj
opoziční	opoziční	k2eAgFnSc4d1	opoziční
činnost	činnost	k1gFnSc4	činnost
pobýval	pobývat	k5eAaImAgMnS	pobývat
i	i	k9	i
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Castro	Castro	k6eAd1	Castro
později	pozdě	k6eAd2	pozdě
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
mexického	mexický	k2eAgInSc2d1	mexický
exilu	exil	k1gInSc2	exil
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
začal	začít	k5eAaPmAgMnS	začít
organizovat	organizovat	k5eAaBmF	organizovat
partyzánskou	partyzánský	k2eAgFnSc4d1	Partyzánská
válku	válka	k1gFnSc4	válka
proti	proti	k7c3	proti
ostrovnímu	ostrovní	k2eAgInSc3d1	ostrovní
režimu	režim	k1gInSc3	režim
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
povstalci	povstalec	k1gMnPc1	povstalec
zvítězili	zvítězit	k5eAaPmAgMnP	zvítězit
a	a	k8xC	a
prozatímním	prozatímní	k2eAgMnSc7d1	prozatímní
prezidentem	prezident	k1gMnSc7	prezident
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Manuel	Manuel	k1gMnSc1	Manuel
Urrutia	Urrutius	k1gMnSc2	Urrutius
<g/>
.	.	kIx.	.
</s>
<s>
Fidel	Fidel	k1gMnSc1	Fidel
Castro	Castro	k1gNnSc4	Castro
zaujal	zaujmout	k5eAaPmAgInS	zaujmout
funkci	funkce	k1gFnSc4	funkce
velitele	velitel	k1gMnSc2	velitel
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
ale	ale	k8xC	ale
nevydržel	vydržet	k5eNaPmAgMnS	vydržet
dlouho	dlouho	k6eAd1	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
17	[number]	k4	17
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1959	[number]	k4	1959
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
předsedou	předseda	k1gMnSc7	předseda
nové	nový	k2eAgFnSc2d1	nová
reorganizované	reorganizovaný	k2eAgFnSc2d1	reorganizovaná
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
nová	nový	k2eAgFnSc1d1	nová
vláda	vláda	k1gFnSc1	vláda
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
bezvýhradně	bezvýhradně	k6eAd1	bezvýhradně
podporovala	podporovat	k5eAaImAgFnS	podporovat
nejchudší	chudý	k2eAgMnPc4d3	nejchudší
obyvatele	obyvatel	k1gMnPc4	obyvatel
a	a	k8xC	a
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgNnPc2	první
rozhodnutí	rozhodnutí	k1gNnPc2	rozhodnutí
bylo	být	k5eAaImAgNnS	být
zvýšení	zvýšení	k1gNnSc1	zvýšení
nejnižších	nízký	k2eAgFnPc2d3	nejnižší
mezd	mzda	k1gFnPc2	mzda
a	a	k8xC	a
snížení	snížení	k1gNnSc2	snížení
cen	cena	k1gFnPc2	cena
léků	lék	k1gInPc2	lék
<g/>
,	,	kIx,	,
elektřiny	elektřina	k1gFnSc2	elektřina
apod.	apod.	kA	apod.
Od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
vládě	vláda	k1gFnSc6	vláda
také	také	k9	také
vzrůstal	vzrůstat	k5eAaImAgInS	vzrůstat
vliv	vliv	k1gInSc1	vliv
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1959	[number]	k4	1959
dokonce	dokonce	k9	dokonce
Castro	Castro	k1gNnSc4	Castro
ostře	ostro	k6eAd1	ostro
zasáhl	zasáhnout	k5eAaPmAgMnS	zasáhnout
proti	proti	k7c3	proti
pravicovým	pravicový	k2eAgMnPc3d1	pravicový
odpůrcům	odpůrce	k1gMnPc3	odpůrce
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
např.	např.	kA	např.
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
důstojníků	důstojník	k1gMnPc2	důstojník
<g/>
,	,	kIx,	,
Hubert	Hubert	k1gMnSc1	Hubert
Matos	Matos	k1gMnSc1	Matos
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
k	k	k7c3	k
dvaceti	dvacet	k4xCc3	dvacet
letům	léto	k1gNnPc3	léto
vězení	vězení	k1gNnSc2	vězení
za	za	k7c4	za
svoji	svůj	k3xOyFgFnSc4	svůj
kritiku	kritika	k1gFnSc4	kritika
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
důvodem	důvod	k1gInSc7	důvod
Castrových	Castrův	k2eAgInPc2d1	Castrův
zásahů	zásah	k1gInPc2	zásah
proti	proti	k7c3	proti
nesouhlasným	souhlasný	k2eNgInPc3d1	nesouhlasný
hlasům	hlas	k1gInPc3	hlas
však	však	k9	však
nebyla	být	k5eNaImAgFnS	být
přímá	přímý	k2eAgFnSc1d1	přímá
sympatie	sympatie	k1gFnSc1	sympatie
s	s	k7c7	s
komunisty	komunista	k1gMnPc7	komunista
<g/>
.	.	kIx.	.
</s>
<s>
Castro	Castro	k6eAd1	Castro
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
bál	bát	k5eAaImAgMnS	bát
spojení	spojení	k1gNnSc4	spojení
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
opozice	opozice	k1gFnSc2	opozice
s	s	k7c7	s
exilovými	exilový	k2eAgFnPc7d1	exilová
organizacemi	organizace	k1gFnPc7	organizace
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
aktivita	aktivita	k1gFnSc1	aktivita
výrazně	výrazně	k6eAd1	výrazně
narůstala	narůstat	k5eAaImAgFnS	narůstat
<g/>
.	.	kIx.	.
</s>
<s>
Castrova	Castrův	k2eAgFnSc1d1	Castrova
vláda	vláda	k1gFnSc1	vláda
mezitím	mezitím	k6eAd1	mezitím
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
zasahovala	zasahovat	k5eAaImAgNnP	zasahovat
do	do	k7c2	do
soukromého	soukromý	k2eAgInSc2d1	soukromý
sektoru	sektor	k1gInSc2	sektor
hospodářství	hospodářství	k1gNnSc2	hospodářství
(	(	kIx(	(
<g/>
např.	např.	kA	např.
uvalila	uvalit	k5eAaPmAgFnS	uvalit
vysokou	vysoký	k2eAgFnSc4d1	vysoká
daň	daň	k1gFnSc4	daň
na	na	k7c4	na
těžbu	těžba	k1gFnSc4	těžba
ropy	ropa	k1gFnSc2	ropa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
opatření	opatření	k1gNnPc1	opatření
se	se	k3xPyFc4	se
kubánských	kubánský	k2eAgFnPc2d1	kubánská
firem	firma	k1gFnPc2	firma
výrazně	výrazně	k6eAd1	výrazně
nedotkla	dotknout	k5eNaPmAgFnS	dotknout
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
dotkla	dotknout	k5eAaPmAgFnS	dotknout
se	se	k3xPyFc4	se
amerických	americký	k2eAgFnPc2d1	americká
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
část	část	k1gFnSc1	část
svého	svůj	k3xOyFgNnSc2	svůj
podnikání	podnikání	k1gNnSc2	podnikání
provozovaly	provozovat	k5eAaImAgFnP	provozovat
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
se	se	k3xPyFc4	se
ihned	ihned	k6eAd1	ihned
rozhodly	rozhodnout	k5eAaPmAgInP	rozhodnout
pro	pro	k7c4	pro
odvetná	odvetný	k2eAgNnPc4d1	odvetné
opatření	opatření	k1gNnPc4	opatření
a	a	k8xC	a
začaly	začít	k5eAaPmAgFnP	začít
blokovat	blokovat	k5eAaImF	blokovat
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
Kubou	Kuba	k1gFnSc7	Kuba
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
ale	ale	k9	ale
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
odstartovaly	odstartovat	k5eAaPmAgInP	odstartovat
kubánský	kubánský	k2eAgInSc4d1	kubánský
příklon	příklon	k1gInSc4	příklon
k	k	k7c3	k
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
a	a	k8xC	a
přiřadily	přiřadit	k5eAaPmAgFnP	přiřadit
jim	on	k3xPp3gMnPc3	on
tak	tak	k9	tak
roli	role	k1gFnSc4	role
v	v	k7c6	v
bipolárním	bipolární	k2eAgNnSc6d1	bipolární
rozdělení	rozdělení	k1gNnSc6	rozdělení
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
momentě	moment	k1gInSc6	moment
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
během	během	k7c2	během
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
také	také	k9	také
začala	začít	k5eAaPmAgFnS	začít
sovětská	sovětský	k2eAgFnSc1d1	sovětská
podpora	podpora	k1gFnSc1	podpora
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1960	[number]	k4	1960
ukončily	ukončit	k5eAaPmAgInP	ukončit
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
veškerou	veškerý	k3xTgFnSc4	veškerý
svou	svůj	k3xOyFgFnSc7	svůj
pomoc	pomoc	k1gFnSc1	pomoc
Kubě	Kuba	k1gFnSc3	Kuba
a	a	k8xC	a
o	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
později	pozdě	k6eAd2	pozdě
navázala	navázat	k5eAaPmAgFnS	navázat
Havana	havana	k1gNnSc6	havana
oficiální	oficiální	k2eAgInPc4d1	oficiální
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
a	a	k8xC	a
obchodní	obchodní	k2eAgInPc4d1	obchodní
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
Moskvou	Moskva	k1gFnSc7	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Kuba	Kuba	k1gFnSc1	Kuba
zároveň	zároveň	k6eAd1	zároveň
ukončila	ukončit	k5eAaPmAgFnS	ukončit
veškeré	veškerý	k3xTgFnPc4	veškerý
dodávky	dodávka	k1gFnPc4	dodávka
cukru	cukr	k1gInSc2	cukr
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
a	a	k8xC	a
ukončila	ukončit	k5eAaPmAgFnS	ukončit
tak	tak	k9	tak
svoji	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
"	"	kIx"	"
<g/>
cukřenky	cukřenka	k1gFnSc2	cukřenka
<g/>
"	"	kIx"	"
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
poté	poté	k6eAd1	poté
začal	začít	k5eAaPmAgInS	začít
svoji	svůj	k3xOyFgFnSc4	svůj
masivní	masivní	k2eAgFnSc4d1	masivní
podporu	podpora	k1gFnSc4	podpora
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zahrnula	zahrnout	k5eAaPmAgFnS	zahrnout
i	i	k9	i
úvěr	úvěr	k1gInSc4	úvěr
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
sto	sto	k4xCgNnSc4	sto
milionů	milion	k4xCgInPc2	milion
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
určený	určený	k2eAgInSc1d1	určený
především	především	k9	především
na	na	k7c4	na
nákup	nákup	k1gInSc4	nákup
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
velmoci	velmoc	k1gFnPc1	velmoc
potvrdily	potvrdit	k5eAaPmAgFnP	potvrdit
začátek	začátek	k1gInSc4	začátek
nových	nový	k2eAgInPc2d1	nový
sporů	spor	k1gInPc2	spor
posílením	posílení	k1gNnSc7	posílení
svých	svůj	k3xOyFgFnPc2	svůj
vojenských	vojenský	k2eAgFnPc2d1	vojenská
pozic	pozice	k1gFnPc2	pozice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1960	[number]	k4	1960
například	například	k6eAd1	například
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
posílily	posílit	k5eAaPmAgInP	posílit
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
přítomnost	přítomnost	k1gFnSc4	přítomnost
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Antil	Antily	k1gFnPc2	Antily
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
další	další	k2eAgInSc4d1	další
krok	krok	k1gInSc4	krok
obrany	obrana	k1gFnSc2	obrana
proti	proti	k7c3	proti
sebemenšímu	sebemenší	k2eAgInSc3d1	sebemenší
americkému	americký	k2eAgInSc3d1	americký
vlivu	vliv	k1gInSc3	vliv
zvolil	zvolit	k5eAaPmAgMnS	zvolit
Fidel	Fidel	k1gMnSc1	Fidel
Castro	Castro	k1gNnSc4	Castro
znárodňování	znárodňování	k1gNnSc2	znárodňování
kubánských	kubánský	k2eAgInPc2d1	kubánský
podniků	podnik	k1gInPc2	podnik
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zřejmě	zřejmě	k6eAd1	zřejmě
utvrdilo	utvrdit	k5eAaPmAgNnS	utvrdit
odhodlání	odhodlání	k1gNnSc4	odhodlání
Američanů	Američan	k1gMnPc2	Američan
zničit	zničit	k5eAaPmF	zničit
Castrův	Castrův	k2eAgInSc4d1	Castrův
režim	režim	k1gInSc4	režim
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
definitivně	definitivně	k6eAd1	definitivně
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
19	[number]	k4	19
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
z	z	k7c2	z
Havany	Havana	k1gFnSc2	Havana
odvolaly	odvolat	k5eAaPmAgInP	odvolat
svého	svůj	k3xOyFgMnSc4	svůj
velvyslance	velvyslanec	k1gMnSc4	velvyslanec
a	a	k8xC	a
úplně	úplně	k6eAd1	úplně
zablokovaly	zablokovat	k5eAaPmAgFnP	zablokovat
veškerý	veškerý	k3xTgInSc4	veškerý
vývoz	vývoz	k1gInSc4	vývoz
amerického	americký	k2eAgNnSc2d1	americké
zboží	zboží	k1gNnSc2	zboží
na	na	k7c4	na
Kubu	Kuba	k1gFnSc4	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Všemi	všecek	k3xTgInPc7	všecek
těmito	tento	k3xDgNnPc7	tento
opatřeními	opatření	k1gNnPc7	opatření
však	však	k8xC	však
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
jen	jen	k6eAd1	jen
prohlubovaly	prohlubovat	k5eAaImAgInP	prohlubovat
vztahy	vztah	k1gInPc4	vztah
Moskva	Moskva	k1gFnSc1	Moskva
–	–	k?	–
Havana	Havana	k1gFnSc1	Havana
<g/>
,	,	kIx,	,
a	a	k8xC	a
potvrzovaly	potvrzovat	k5eAaImAgInP	potvrzovat
tak	tak	k9	tak
postavení	postavení	k1gNnSc4	postavení
Kuby	Kuba	k1gFnSc2	Kuba
jako	jako	k8xC	jako
předsunuté	předsunutý	k2eAgFnSc2d1	předsunutá
hlídky	hlídka	k1gFnSc2	hlídka
SSSR	SSSR	kA	SSSR
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kubánská	kubánský	k2eAgFnSc1d1	kubánská
role	role	k1gFnSc1	role
byla	být	k5eAaImAgFnS	být
potvrzena	potvrdit	k5eAaPmNgFnS	potvrdit
<g/>
,	,	kIx,	,
když	když	k8xS	když
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1961	[number]	k4	1961
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přerušení	přerušení	k1gNnSc3	přerušení
diplomatických	diplomatický	k2eAgInPc2d1	diplomatický
vztahů	vztah	k1gInPc2	vztah
mezi	mezi	k7c7	mezi
Havanou	Havana	k1gFnSc7	Havana
a	a	k8xC	a
Washingtonem	Washington	k1gInSc7	Washington
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
tak	tak	k6eAd1	tak
svou	svůj	k3xOyFgFnSc7	svůj
reakcí	reakce	k1gFnSc7	reakce
na	na	k7c4	na
kubánskou	kubánský	k2eAgFnSc4d1	kubánská
agrární	agrární	k2eAgFnSc4d1	agrární
reformu	reforma	k1gFnSc4	reforma
prakticky	prakticky	k6eAd1	prakticky
vehnaly	vehnat	k5eAaPmAgFnP	vehnat
Castra	Castrum	k1gNnPc1	Castrum
do	do	k7c2	do
náruče	náruč	k1gFnSc2	náruč
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
jediná	jediný	k2eAgFnSc1d1	jediná
obrana	obrana	k1gFnSc1	obrana
proti	proti	k7c3	proti
politice	politika	k1gFnSc3	politika
USA	USA	kA	USA
vede	vést	k5eAaImIp3nS	vést
přes	přes	k7c4	přes
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
vedl	vést	k5eAaImAgMnS	vést
Castra	Castr	k1gMnSc4	Castr
k	k	k7c3	k
akceptování	akceptování	k1gNnSc3	akceptování
komunismu	komunismus	k1gInSc2	komunismus
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
sám	sám	k3xTgMnSc1	sám
vyznavačem	vyznavač	k1gMnSc7	vyznavač
marxismu-leninismu	marxismueninismus	k1gInSc2	marxismu-leninismus
nebyl	být	k5eNaImAgInS	být
a	a	k8xC	a
s	s	k7c7	s
kubánskými	kubánský	k2eAgMnPc7d1	kubánský
komunisty	komunista	k1gMnPc7	komunista
měl	mít	k5eAaImAgInS	mít
nejeden	jeden	k2eNgInSc1d1	nejeden
konflikt	konflikt	k1gInSc1	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vylodění	vylodění	k1gNnSc2	vylodění
v	v	k7c6	v
zátoce	zátoka	k1gFnSc6	zátoka
Sviní	svině	k1gFnPc2	svině
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgNnSc6	tento
Castrově	Castrův	k2eAgNnSc6d1	Castrovo
rozhodnutí	rozhodnutí	k1gNnSc6	rozhodnutí
o	o	k7c6	o
znárodnění	znárodnění	k1gNnSc6	znárodnění
začaly	začít	k5eAaPmAgInP	začít
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
organizovat	organizovat	k5eAaBmF	organizovat
tajné	tajný	k2eAgFnPc4d1	tajná
operace	operace	k1gFnPc4	operace
a	a	k8xC	a
opatření	opatření	k1gNnSc4	opatření
proti	proti	k7c3	proti
havanské	havanský	k2eAgFnSc3d1	Havanská
vládě	vláda	k1gFnSc3	vláda
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
podporu	podpora	k1gFnSc4	podpora
kubánskému	kubánský	k2eAgNnSc3d1	kubánské
exilovému	exilový	k2eAgNnSc3d1	exilové
hnutí	hnutí	k1gNnSc3	hnutí
<g/>
,	,	kIx,	,
kterému	který	k3yRgNnSc3	který
byly	být	k5eAaImAgFnP	být
poskytnuty	poskytnut	k2eAgFnPc1d1	poskytnuta
zbraně	zbraň	k1gFnPc1	zbraň
a	a	k8xC	a
výcvik	výcvik	k1gInSc1	výcvik
od	od	k7c2	od
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1961	[number]	k4	1961
se	se	k3xPyFc4	se
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
kubánského	kubánský	k2eAgInSc2d1	kubánský
exilu	exil	k1gInSc2	exil
vylodily	vylodit	k5eAaPmAgFnP	vylodit
v	v	k7c6	v
zátoce	zátoka	k1gFnSc6	zátoka
Sviní	svině	k1gFnPc2	svině
<g/>
.	.	kIx.	.
</s>
<s>
Útok	útok	k1gInSc1	útok
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
(	(	kIx(	(
<g/>
minimálně	minimálně	k6eAd1	minimálně
podle	podle	k7c2	podle
představ	představa	k1gFnPc2	představa
velitelů	velitel	k1gMnPc2	velitel
těchto	tento	k3xDgFnPc2	tento
invazních	invazní	k2eAgFnPc2d1	invazní
sil	síla	k1gFnPc2	síla
<g/>
)	)	kIx)	)
podpořen	podpořen	k2eAgInSc1d1	podpořen
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
fázích	fáze	k1gFnPc6	fáze
letectvem	letectvo	k1gNnSc7	letectvo
a	a	k8xC	a
ozbrojenými	ozbrojený	k2eAgFnPc7d1	ozbrojená
silami	síla	k1gFnPc7	síla
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
tento	tento	k3xDgInSc4	tento
příslib	příslib	k1gInSc4	příslib
měli	mít	k5eAaImAgMnP	mít
od	od	k7c2	od
CIA	CIA	kA	CIA
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ovšem	ovšem	k9	ovšem
nikdy	nikdy	k6eAd1	nikdy
nedocílila	docílit	k5eNaPmAgFnS	docílit
otevřeného	otevřený	k2eAgInSc2d1	otevřený
souhlasu	souhlas	k1gInSc2	souhlas
vládní	vládní	k2eAgFnSc2d1	vládní
administrativy	administrativa	k1gFnSc2	administrativa
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
bodem	bod	k1gInSc7	bod
plánu	plán	k1gInSc2	plán
<g/>
.	.	kIx.	.
</s>
<s>
Invaze	invaze	k1gFnSc1	invaze
byla	být	k5eAaImAgFnS	být
velice	velice	k6eAd1	velice
špatně	špatně	k6eAd1	špatně
naplánována	naplánován	k2eAgFnSc1d1	naplánována
<g/>
,	,	kIx,	,
k	k	k7c3	k
očekávanému	očekávaný	k2eAgNnSc3d1	očekávané
povstání	povstání	k1gNnSc3	povstání
nedošlo	dojít	k5eNaPmAgNnS	dojít
a	a	k8xC	a
invazní	invazní	k2eAgFnPc1d1	invazní
jednotky	jednotka	k1gFnPc1	jednotka
samy	sám	k3xTgFnPc1	sám
byly	být	k5eAaImAgFnP	být
směšně	směšně	k6eAd1	směšně
slabé	slabý	k2eAgFnPc1d1	slabá
a	a	k8xC	a
mizerně	mizerně	k6eAd1	mizerně
vyzbrojené	vyzbrojený	k2eAgFnPc1d1	vyzbrojená
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
rychlému	rychlý	k2eAgNnSc3d1	rychlé
rozdrcení	rozdrcení	k1gNnSc3	rozdrcení
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
když	když	k8xS	když
rozhořčené	rozhořčený	k2eAgFnPc1d1	rozhořčená
reakce	reakce	k1gFnPc1	reakce
světové	světový	k2eAgFnSc2d1	světová
veřejnosti	veřejnost	k1gFnSc2	veřejnost
přesvědčily	přesvědčit	k5eAaPmAgFnP	přesvědčit
prezidenta	prezident	k1gMnSc2	prezident
Kennedyho	Kennedy	k1gMnSc2	Kennedy
<g/>
,	,	kIx,	,
že	že	k8xS	že
podpořit	podpořit	k5eAaPmF	podpořit
zřejmě	zřejmě	k6eAd1	zřejmě
předem	předem	k6eAd1	předem
ztracenou	ztracený	k2eAgFnSc4d1	ztracená
věc	věc	k1gFnSc4	věc
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
nevhodné	vhodný	k2eNgNnSc1d1	nevhodné
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
od	od	k7c2	od
celé	celý	k2eAgFnSc2d1	celá
věci	věc	k1gFnSc2	věc
distancoval	distancovat	k5eAaBmAgMnS	distancovat
<g/>
.	.	kIx.	.
</s>
<s>
Castro	Castro	k6eAd1	Castro
díky	díky	k7c3	díky
tomuto	tento	k3xDgInSc3	tento
útoku	útok	k1gInSc3	útok
a	a	k8xC	a
následnému	následný	k2eAgNnSc3d1	následné
vítězství	vítězství	k1gNnSc3	vítězství
dostal	dostat	k5eAaPmAgMnS	dostat
příležitost	příležitost	k1gFnSc4	příležitost
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
dokončit	dokončit	k5eAaPmF	dokončit
budování	budování	k1gNnSc4	budování
totality	totalita	k1gFnSc2	totalita
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
také	také	k9	také
učinil	učinit	k5eAaImAgInS	učinit
založením	založení	k1gNnSc7	založení
Sjednocení	sjednocení	k1gNnPc2	sjednocení
revolučních	revoluční	k2eAgFnPc2d1	revoluční
organizací	organizace	k1gFnPc2	organizace
(	(	kIx(	(
<g/>
ORI	ORI	kA	ORI
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
spojovalo	spojovat	k5eAaImAgNnS	spojovat
doposud	doposud	k6eAd1	doposud
samostatné	samostatný	k2eAgInPc4d1	samostatný
politické	politický	k2eAgInPc4d1	politický
subjekty	subjekt	k1gInPc4	subjekt
(	(	kIx(	(
<g/>
Hnutí	hnutí	k1gNnSc2	hnutí
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
,	,	kIx,	,
Lidovou	lidový	k2eAgFnSc4d1	lidová
socialistickou	socialistický	k2eAgFnSc4d1	socialistická
stranu	strana	k1gFnSc4	strana
a	a	k8xC	a
Direktorium	direktorium	k1gNnSc4	direktorium
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
před	před	k7c7	před
tímto	tento	k3xDgInSc7	tento
útokem	útok	k1gInSc7	útok
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
americká	americký	k2eAgFnSc1d1	americká
vláda	vláda	k1gFnSc1	vláda
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
otevřenou	otevřený	k2eAgFnSc4d1	otevřená
distanci	distance	k1gFnSc4	distance
od	od	k7c2	od
latinskoamerických	latinskoamerický	k2eAgFnPc2d1	latinskoamerická
diktatur	diktatura	k1gFnPc2	diktatura
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
veškeré	veškerý	k3xTgFnPc4	veškerý
neshody	neshoda	k1gFnPc4	neshoda
ale	ale	k8xC	ale
kubánští	kubánský	k2eAgMnPc1d1	kubánský
činitelé	činitel	k1gMnPc1	činitel
<g/>
,	,	kIx,	,
především	především	k9	především
Che	che	k0	che
Guevara	Guevara	k1gFnSc1	Guevara
a	a	k8xC	a
Fidel	Fidel	k1gMnSc1	Fidel
Castro	Castro	k1gNnSc1	Castro
<g/>
,	,	kIx,	,
několikrát	několikrát	k6eAd1	několikrát
naznačili	naznačit	k5eAaPmAgMnP	naznačit
možnost	možnost	k1gFnSc4	možnost
uvolnění	uvolnění	k1gNnSc4	uvolnění
vazeb	vazba	k1gFnPc2	vazba
se	s	k7c7	s
sovětským	sovětský	k2eAgInSc7d1	sovětský
blokem	blok	k1gInSc7	blok
a	a	k8xC	a
ochotu	ochota	k1gFnSc4	ochota
"	"	kIx"	"
<g/>
rozmrazit	rozmrazit	k5eAaPmF	rozmrazit
<g/>
"	"	kIx"	"
vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
Havanou	Havana	k1gFnSc7	Havana
a	a	k8xC	a
Washingtonem	Washington	k1gInSc7	Washington
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k8xC	ale
Kennedyho	Kennedy	k1gMnSc2	Kennedy
vláda	vláda	k1gFnSc1	vláda
na	na	k7c4	na
tyto	tento	k3xDgInPc4	tento
náznaky	náznak	k1gInPc4	náznak
nereagovala	reagovat	k5eNaBmAgFnS	reagovat
a	a	k8xC	a
posunula	posunout	k5eAaPmAgFnS	posunout
tak	tak	k9	tak
Kubu	Kuba	k1gFnSc4	Kuba
opět	opět	k6eAd1	opět
blíže	blízce	k6eAd2	blízce
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
se	se	k3xPyFc4	se
Castro	Castro	k1gNnSc1	Castro
dokonce	dokonce	k9	dokonce
demonstrativně	demonstrativně	k6eAd1	demonstrativně
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
k	k	k7c3	k
ideologii	ideologie	k1gFnSc3	ideologie
marxismu-leninismu	marxismueninismus	k1gInSc2	marxismu-leninismus
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
ovšem	ovšem	k9	ovšem
ztratil	ztratit	k5eAaPmAgMnS	ztratit
podporu	podpora	k1gFnSc4	podpora
některých	některý	k3yIgMnPc2	některý
jihoamerických	jihoamerický	k2eAgMnPc2d1	jihoamerický
států	stát	k1gInPc2	stát
a	a	k8xC	a
na	na	k7c6	na
lednovém	lednový	k2eAgInSc6d1	lednový
sjezdu	sjezd	k1gInSc6	sjezd
Organizace	organizace	k1gFnSc2	organizace
amerických	americký	k2eAgInPc2d1	americký
států	stát	k1gInPc2	stát
byla	být	k5eAaImAgFnS	být
Kuba	Kuba	k1gFnSc1	Kuba
ostrakizována	ostrakizován	k2eAgFnSc1d1	ostrakizována
coby	coby	k?	coby
nástroj	nástroj	k1gInSc4	nástroj
komunistického	komunistický	k2eAgInSc2d1	komunistický
bloku	blok	k1gInSc2	blok
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
,	,	kIx,	,
uvalily	uvalit	k5eAaPmAgInP	uvalit
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
embargo	embargo	k1gNnSc4	embargo
na	na	k7c4	na
veškerý	veškerý	k3xTgInSc4	veškerý
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
Kubou	Kuba	k1gFnSc7	Kuba
<g/>
,	,	kIx,	,
zanedlouho	zanedlouho	k6eAd1	zanedlouho
poté	poté	k6eAd1	poté
byla	být	k5eAaImAgFnS	být
Kuba	Kuba	k1gFnSc1	Kuba
dokonce	dokonce	k9	dokonce
vyloučena	vyloučit	k5eAaPmNgFnS	vyloučit
z	z	k7c2	z
Hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
a	a	k8xC	a
sociální	sociální	k2eAgFnSc2d1	sociální
rady	rada	k1gFnSc2	rada
Organizace	organizace	k1gFnSc2	organizace
amerických	americký	k2eAgInPc2d1	americký
států	stát	k1gInPc2	stát
a	a	k8xC	a
jiných	jiný	k2eAgNnPc2d1	jiné
sdružení	sdružení	k1gNnPc2	sdružení
a	a	k8xC	a
rad	rada	k1gFnPc2	rada
Organizace	organizace	k1gFnSc2	organizace
amerických	americký	k2eAgInPc2d1	americký
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
navíc	navíc	k6eAd1	navíc
byla	být	k5eAaImAgFnS	být
ustanovena	ustanoven	k2eAgFnSc1d1	ustanovena
komise	komise	k1gFnSc1	komise
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
plnit	plnit	k5eAaImF	plnit
dozor	dozor	k1gInSc4	dozor
nad	nad	k7c7	nad
dodržováním	dodržování	k1gNnSc7	dodržování
všech	všecek	k3xTgFnPc2	všecek
sankcí	sankce	k1gFnPc2	sankce
vůči	vůči	k7c3	vůči
Kubě	Kuba	k1gFnSc3	Kuba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
zrodila	zrodit	k5eAaPmAgFnS	zrodit
operace	operace	k1gFnSc1	operace
Anadyr	Anadyr	k1gMnSc1	Anadyr
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gMnSc1	jejíž
náplní	náplň	k1gFnSc7	náplň
byl	být	k5eAaImAgInS	být
projekt	projekt	k1gInSc1	projekt
vybudování	vybudování	k1gNnSc2	vybudování
sovětských	sovětský	k2eAgFnPc2d1	sovětská
vojenských	vojenský	k2eAgFnPc2d1	vojenská
základen	základna	k1gFnPc2	základna
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
plán	plán	k1gInSc1	plán
byl	být	k5eAaImAgInS	být
vymyšlen	vymyslet	k5eAaPmNgInS	vymyslet
jako	jako	k8xC	jako
odpověď	odpověď	k1gFnSc1	odpověď
na	na	k7c6	na
rozmístění	rozmístění	k1gNnSc6	rozmístění
amerických	americký	k2eAgFnPc2d1	americká
střel	střela	k1gFnPc2	střela
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
a	a	k8xC	a
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jara	jaro	k1gNnSc2	jaro
1962	[number]	k4	1962
bylo	být	k5eAaImAgNnS	být
o	o	k7c6	o
operaci	operace	k1gFnSc6	operace
definitivně	definitivně	k6eAd1	definitivně
rozhodnuto	rozhodnut	k2eAgNnSc1d1	rozhodnuto
a	a	k8xC	a
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
na	na	k7c4	na
Kubu	Kuba	k1gFnSc4	Kuba
vyrazila	vyrazit	k5eAaPmAgFnS	vyrazit
sovětská	sovětský	k2eAgFnSc1d1	sovětská
delegace	delegace	k1gFnSc1	delegace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
vyjednat	vyjednat	k5eAaPmF	vyjednat
souhlas	souhlas	k1gInSc4	souhlas
kubánské	kubánský	k2eAgFnSc2d1	kubánská
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
také	také	k9	také
dostalo	dostat	k5eAaPmAgNnS	dostat
(	(	kIx(	(
<g/>
souhlas	souhlas	k1gInSc1	souhlas
byl	být	k5eAaImAgInS	být
bez	bez	k7c2	bez
váhání	váhání	k1gNnSc2	váhání
a	a	k8xC	a
jednohlasný	jednohlasný	k2eAgMnSc1d1	jednohlasný
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
už	už	k6eAd1	už
Chruščov	Chruščov	k1gInSc1	Chruščov
potvrzoval	potvrzovat	k5eAaImAgInS	potvrzovat
sestavu	sestava	k1gFnSc4	sestava
velitelského	velitelský	k2eAgInSc2d1	velitelský
sboru	sbor	k1gInSc2	sbor
pro	pro	k7c4	pro
operaci	operace	k1gFnSc4	operace
a	a	k8xC	a
na	na	k7c4	na
Kubu	Kuba	k1gFnSc4	Kuba
byla	být	k5eAaImAgFnS	být
zároveň	zároveň	k6eAd1	zároveň
vyslána	vyslat	k5eAaPmNgFnS	vyslat
další	další	k2eAgFnSc1d1	další
sovětská	sovětský	k2eAgFnSc1d1	sovětská
delegace	delegace	k1gFnSc1	delegace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
s	s	k7c7	s
Havanou	Havana	k1gFnSc7	Havana
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
sovětsko-kubánskou	sovětskoubánský	k2eAgFnSc4d1	sovětsko-kubánský
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
začal	začít	k5eAaPmAgMnS	začít
probíhat	probíhat	k5eAaImF	probíhat
přesun	přesun	k1gInSc4	přesun
vojenského	vojenský	k2eAgInSc2d1	vojenský
materiálu	materiál	k1gInSc2	materiál
a	a	k8xC	a
personálu	personál	k1gInSc2	personál
na	na	k7c4	na
Kubu	Kuba	k1gFnSc4	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
selhala	selhat	k5eAaPmAgFnS	selhat
americká	americký	k2eAgFnSc1d1	americká
rozvědka	rozvědka	k1gFnSc1	rozvědka
<g/>
.	.	kIx.	.
</s>
<s>
Washington	Washington	k1gInSc1	Washington
totiž	totiž	k9	totiž
o	o	k7c4	o
dění	dění	k1gNnSc4	dění
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
dostal	dostat	k5eAaPmAgMnS	dostat
informace	informace	k1gFnPc4	informace
až	až	k9	až
koncem	koncem	k7c2	koncem
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
varování	varování	k1gNnSc1	varování
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
reakci	reakce	k1gFnSc4	reakce
na	na	k7c4	na
tyto	tento	k3xDgFnPc4	tento
informace	informace	k1gFnPc4	informace
<g/>
,	,	kIx,	,
dostala	dostat	k5eAaPmAgFnS	dostat
Moskva	Moskva	k1gFnSc1	Moskva
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
povolaly	povolat	k5eAaPmAgInP	povolat
do	do	k7c2	do
akce	akce	k1gFnSc2	akce
150	[number]	k4	150
tisíc	tisíc	k4xCgInPc2	tisíc
záložních	záložní	k2eAgMnPc2d1	záložní
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
varování	varování	k1gNnSc4	varování
Chruščov	Chruščov	k1gInSc4	Chruščov
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
prohlášením	prohlášení	k1gNnSc7	prohlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
žádné	žádný	k3yNgFnPc1	žádný
sovětské	sovětský	k2eAgFnPc1d1	sovětská
rakety	raketa	k1gFnPc1	raketa
neumístí	umístit	k5eNaPmIp3nP	umístit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
už	už	k6eAd1	už
rakety	raketa	k1gFnSc2	raketa
ale	ale	k8xC	ale
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
byly	být	k5eAaImAgFnP	být
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
necelý	celý	k2eNgInSc4d1	necelý
měsíc	měsíc	k1gInSc4	měsíc
později	pozdě	k6eAd2	pozdě
založil	založit	k5eAaPmAgMnS	založit
prezident	prezident	k1gMnSc1	prezident
John	John	k1gMnSc1	John
Fitzgerald	Fitzgerald	k1gMnSc1	Fitzgerald
Kennedy	Kenneda	k1gMnSc2	Kenneda
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
pozorování	pozorování	k1gNnPc2	pozorování
průzkumných	průzkumný	k2eAgInPc2d1	průzkumný
letounů	letoun	k1gInPc2	letoun
<g/>
,	,	kIx,	,
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
skupinu	skupina	k1gFnSc4	skupina
CIA	CIA	kA	CIA
pro	pro	k7c4	pro
Kubu	Kuba	k1gFnSc4	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
probíhalo	probíhat	k5eAaImAgNnS	probíhat
zasedání	zasedání	k1gNnSc1	zasedání
Valného	valný	k2eAgNnSc2d1	Valné
shromáždění	shromáždění	k1gNnSc2	shromáždění
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
kubánský	kubánský	k2eAgMnSc1d1	kubánský
prezident	prezident	k1gMnSc1	prezident
Osvaldo	Osvaldo	k1gNnSc1	Osvaldo
Dorticos	Dorticos	k1gMnSc1	Dorticos
Torrado	Torrada	k1gFnSc5	Torrada
reagoval	reagovat	k5eAaBmAgMnS	reagovat
na	na	k7c4	na
kritiku	kritika	k1gFnSc4	kritika
militarizace	militarizace	k1gFnSc2	militarizace
prohlášením	prohlášení	k1gNnSc7	prohlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
země	země	k1gFnSc1	země
pouze	pouze	k6eAd1	pouze
reaguje	reagovat	k5eAaBmIp3nS	reagovat
na	na	k7c4	na
hrozbu	hrozba	k1gFnSc4	hrozba
vnější	vnější	k2eAgFnSc2d1	vnější
agrese	agrese	k1gFnSc2	agrese
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
další	další	k2eAgInSc4d1	další
týden	týden	k1gInSc4	týden
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
prezidentu	prezident	k1gMnSc6	prezident
Kennedymu	Kennedym	k1gInSc2	Kennedym
dostaly	dostat	k5eAaPmAgFnP	dostat
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
snímky	snímka	k1gFnSc2	snímka
sovětských	sovětský	k2eAgFnPc2d1	sovětská
základen	základna	k1gFnPc2	základna
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Okamžitě	okamžitě	k6eAd1	okamžitě
svolal	svolat	k5eAaPmAgMnS	svolat
schůzi	schůze	k1gFnSc4	schůze
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
poradci	poradce	k1gMnPc7	poradce
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgInPc4	který
byla	být	k5eAaImAgFnS	být
řešena	řešit	k5eAaImNgFnS	řešit
otázka	otázka	k1gFnSc1	otázka
možného	možný	k2eAgInSc2d1	možný
zásahu	zásah	k1gInSc2	zásah
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jeho	on	k3xPp3gInSc2	on
charakteru	charakter	k1gInSc2	charakter
a	a	k8xC	a
možných	možný	k2eAgNnPc2d1	možné
rizik	riziko	k1gNnPc2	riziko
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
měl	mít	k5eAaImAgMnS	mít
Kennedy	Kenned	k1gMnPc4	Kenned
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
další	další	k2eAgInPc4d1	další
snímky	snímek	k1gInPc4	snímek
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
zřetelně	zřetelně	k6eAd1	zřetelně
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
vyčetl	vyčíst	k5eAaPmAgMnS	vyčíst
rychlý	rychlý	k2eAgInSc4d1	rychlý
postup	postup	k1gInSc4	postup
sovětských	sovětský	k2eAgFnPc2d1	sovětská
prací	práce	k1gFnPc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vědecké	vědecký	k2eAgFnSc2d1	vědecká
analýzy	analýza	k1gFnSc2	analýza
nové	nový	k2eAgNnSc4d1	nové
ozbrojení	ozbrojení	k1gNnSc4	ozbrojení
"	"	kIx"	"
<g/>
ostrova	ostrov	k1gInSc2	ostrov
svobody	svoboda	k1gFnSc2	svoboda
<g/>
"	"	kIx"	"
ohrožovalo	ohrožovat	k5eAaImAgNnS	ohrožovat
životy	život	k1gInPc4	život
80	[number]	k4	80
milionů	milion	k4xCgInPc2	milion
Američanů	Američan	k1gMnPc2	Američan
raketami	raketa	k1gFnPc7	raketa
Sandal	Sandal	k1gMnSc2	Sandal
SS-4	SS-4	k1gMnSc2	SS-4
s	s	k7c7	s
doletem	dolet	k1gInSc7	dolet
2000	[number]	k4	2000
km	km	kA	km
<g/>
,	,	kIx,	,
rakety	raketa	k1gFnPc1	raketa
Skean	Skean	k1gInSc4	Skean
měly	mít	k5eAaImAgFnP	mít
dosah	dosah	k1gInSc4	dosah
5000	[number]	k4	5000
km	km	kA	km
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
přímé	přímý	k2eAgNnSc4d1	přímé
ohrožení	ohrožení	k1gNnSc4	ohrožení
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
naléhavěji	naléhavě	k6eAd2	naléhavě
<g/>
,	,	kIx,	,
diskutoval	diskutovat	k5eAaImAgMnS	diskutovat
americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
poradci	poradce	k1gMnPc7	poradce
o	o	k7c6	o
možnostech	možnost	k1gFnPc6	možnost
obrany	obrana	k1gFnSc2	obrana
proti	proti	k7c3	proti
takovému	takový	k3xDgNnSc3	takový
ohrožení	ohrožení	k1gNnSc3	ohrožení
a	a	k8xC	a
sovětských	sovětský	k2eAgFnPc6d1	sovětská
reakcích	reakce	k1gFnPc6	reakce
na	na	k7c4	na
taková	takový	k3xDgNnPc4	takový
opatření	opatření	k1gNnPc4	opatření
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
v	v	k7c4	v
17	[number]	k4	17
hodin	hodina	k1gFnPc2	hodina
se	se	k3xPyFc4	se
Kennedy	Kenned	k1gMnPc7	Kenned
setkal	setkat	k5eAaPmAgMnS	setkat
se	s	k7c7	s
sovětským	sovětský	k2eAgMnSc7d1	sovětský
ministrem	ministr	k1gMnSc7	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Andrejem	Andrej	k1gMnSc7	Andrej
Gromykem	Gromyk	k1gInSc7	Gromyk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
schůzce	schůzka	k1gFnSc6	schůzka
Gromyko	Gromyko	k1gNnSc4	Gromyko
vznesl	vznést	k5eAaPmAgMnS	vznést
požadavek	požadavek	k1gInSc4	požadavek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
přestaly	přestat	k5eAaPmAgInP	přestat
ohrožovat	ohrožovat	k5eAaImF	ohrožovat
Kubu	Kuba	k1gFnSc4	Kuba
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
konstatoval	konstatovat	k5eAaBmAgMnS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
Kubě	Kuba	k1gFnSc6	Kuba
pouze	pouze	k6eAd1	pouze
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
s	s	k7c7	s
modernizací	modernizace	k1gFnSc7	modernizace
zemědělství	zemědělství	k1gNnSc2	zemědělství
a	a	k8xC	a
jen	jen	k9	jen
Kubě	Kuba	k1gFnSc3	Kuba
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
obranných	obranný	k2eAgFnPc2d1	obranná
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
raketách	raketa	k1gFnPc6	raketa
na	na	k7c6	na
schůzce	schůzka	k1gFnSc6	schůzka
nepadlo	padnout	k5eNaPmAgNnS	padnout
ani	ani	k8xC	ani
slovo	slovo	k1gNnSc1	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Gromyko	Gromyko	k6eAd1	Gromyko
o	o	k7c6	o
nich	on	k3xPp3gFnPc6	on
pochopitelně	pochopitelně	k6eAd1	pochopitelně
nezačal	začít	k5eNaPmAgMnS	začít
a	a	k8xC	a
udivený	udivený	k2eAgMnSc1d1	udivený
Kennedy	Kenned	k1gMnPc7	Kenned
nakonec	nakonec	k6eAd1	nakonec
také	také	k9	také
ne	ne	k9	ne
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
veškeré	veškerý	k3xTgInPc4	veškerý
dohady	dohad	k1gInPc4	dohad
<g/>
,	,	kIx,	,
rozhodly	rozhodnout	k5eAaPmAgFnP	rozhodnout
reagovat	reagovat	k5eAaBmF	reagovat
na	na	k7c4	na
ohrožení	ohrožení	k1gNnSc4	ohrožení
námořní	námořní	k2eAgFnSc7d1	námořní
blokádou	blokáda	k1gFnSc7	blokáda
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
půjde	jít	k5eAaImIp3nS	jít
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
čistě	čistě	k6eAd1	čistě
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
blokádu	blokáda	k1gFnSc4	blokáda
a	a	k8xC	a
útočit	útočit	k5eAaImF	útočit
budou	být	k5eAaImBp3nP	být
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
případě	případ	k1gInSc6	případ
selhání	selhání	k1gNnSc1	selhání
tohoto	tento	k3xDgNnSc2	tento
opatření	opatření	k1gNnSc2	opatření
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnPc4	první
vojenská	vojenský	k2eAgNnPc4d1	vojenské
opatření	opatření	k1gNnPc4	opatření
začala	začít	k5eAaPmAgFnS	začít
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1962	[number]	k4	1962
v	v	k7c4	v
pravé	pravý	k2eAgNnSc4d1	pravé
poledne	poledne	k1gNnSc4	poledne
<g/>
.	.	kIx.	.
</s>
<s>
Tentýž	týž	k3xTgInSc4	týž
den	den	k1gInSc4	den
v	v	k7c6	v
19	[number]	k4	19
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
prezident	prezident	k1gMnSc1	prezident
Kennedy	Kenneda	k1gMnSc2	Kenneda
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
a	a	k8xC	a
v	v	k7c6	v
rozhlase	rozhlas	k1gInSc6	rozhlas
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
projevu	projev	k1gInSc6	projev
vysvětloval	vysvětlovat	k5eAaImAgMnS	vysvětlovat
svým	svůj	k3xOyFgMnPc3	svůj
občanům	občan	k1gMnPc3	občan
následující	následující	k2eAgFnSc4d1	následující
akci	akce	k1gFnSc4	akce
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
začala	začít	k5eAaPmAgFnS	začít
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
v	v	k7c6	v
10	[number]	k4	10
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc4	den
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
Rada	rada	k1gFnSc1	rada
Organizace	organizace	k1gFnSc2	organizace
amerických	americký	k2eAgInPc2d1	americký
států	stát	k1gInPc2	stát
usnesla	usnést	k5eAaPmAgFnS	usnést
na	na	k7c6	na
ukončení	ukončení	k1gNnSc6	ukončení
dodávky	dodávka	k1gFnSc2	dodávka
zbraní	zbraň	k1gFnPc2	zbraň
na	na	k7c4	na
Kubu	Kuba	k1gFnSc4	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
počaly	počnout	k5eAaPmAgFnP	počnout
kubánské	kubánský	k2eAgFnPc1d1	kubánská
jednotky	jednotka	k1gFnPc1	jednotka
obléhat	obléhat	k5eAaImF	obléhat
Guantánamo	Guantánama	k1gFnSc5	Guantánama
a	a	k8xC	a
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
začala	začít	k5eAaPmAgFnS	začít
mobilizace	mobilizace	k1gFnSc1	mobilizace
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
polednem	poledne	k1gNnSc7	poledne
23	[number]	k4	23
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
dorazila	dorazit	k5eAaPmAgFnS	dorazit
do	do	k7c2	do
Washingtonu	Washington	k1gInSc2	Washington
odpověď	odpověď	k1gFnSc4	odpověď
na	na	k7c4	na
chystanou	chystaný	k2eAgFnSc4d1	chystaná
blokádu	blokáda	k1gFnSc4	blokáda
<g/>
.	.	kIx.	.
</s>
<s>
Moskva	Moskva	k1gFnSc1	Moskva
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
(	(	kIx(	(
<g/>
zcela	zcela	k6eAd1	zcela
pochopitelně	pochopitelně	k6eAd1	pochopitelně
<g/>
)	)	kIx)	)
postavila	postavit	k5eAaPmAgFnS	postavit
naprosto	naprosto	k6eAd1	naprosto
proti	proti	k7c3	proti
akci	akce	k1gFnSc3	akce
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
a	a	k8xC	a
označila	označit	k5eAaPmAgFnS	označit
ji	on	k3xPp3gFnSc4	on
za	za	k7c4	za
ohrožení	ohrožení	k1gNnSc4	ohrožení
světového	světový	k2eAgInSc2d1	světový
míru	mír	k1gInSc2	mír
<g/>
,	,	kIx,	,
vměšování	vměšování	k1gNnSc1	vměšování
se	se	k3xPyFc4	se
do	do	k7c2	do
cizích	cizí	k2eAgFnPc2d1	cizí
záležitostí	záležitost	k1gFnPc2	záležitost
apod.	apod.	kA	apod.
Po	po	k7c6	po
započetí	započetí	k1gNnSc6	započetí
blokády	blokáda	k1gFnSc2	blokáda
se	se	k3xPyFc4	se
sovětské	sovětský	k2eAgFnPc1d1	sovětská
lodě	loď	k1gFnPc1	loď
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nebyly	být	k5eNaImAgInP	být
podpořeny	podpořit	k5eAaPmNgInP	podpořit
ponorkami	ponorka	k1gFnPc7	ponorka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
pluly	plout	k5eAaImAgFnP	plout
daleko	daleko	k6eAd1	daleko
za	za	k7c7	za
obchodními	obchodní	k2eAgInPc7d1	obchodní
<g />
.	.	kIx.	.
</s>
<s>
loděmi	loď	k1gFnPc7	loď
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
to	ten	k3xDgNnSc1	ten
snad	snad	k9	snad
nejhorší	zlý	k2eAgFnPc1d3	nejhorší
dieselelektrické	dieselelektrický	k2eAgFnPc1d1	dieselelektrická
ponorky	ponorka	k1gFnPc1	ponorka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
mohli	moct	k5eAaImAgMnP	moct
Sověti	Sovět	k1gMnPc1	Sovět
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
akce	akce	k1gFnSc2	akce
zapojit	zapojit	k5eAaPmF	zapojit
<g/>
,	,	kIx,	,
zastavily	zastavit	k5eAaPmAgFnP	zastavit
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
blokády	blokáda	k1gFnSc2	blokáda
na	na	k7c6	na
základě	základ	k1gInSc6	základ
signálů	signál	k1gInPc2	signál
vysílaných	vysílaný	k2eAgInPc2d1	vysílaný
z	z	k7c2	z
USA	USA	kA	USA
válečných	válečný	k2eAgFnPc2d1	válečná
lodí	loď	k1gFnPc2	loď
(	(	kIx(	(
<g/>
některé	některý	k3yIgFnPc1	některý
se	se	k3xPyFc4	se
obrátily	obrátit	k5eAaPmAgFnP	obrátit
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tankery	tanker	k1gInPc1	tanker
podle	podle	k7c2	podle
dohody	dohoda	k1gFnSc2	dohoda
mohly	moct	k5eAaImAgInP	moct
projet	projet	k5eAaPmF	projet
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Kuby	Kuba	k1gFnSc2	Kuba
však	však	k9	však
nadále	nadále	k6eAd1	nadále
probíhala	probíhat	k5eAaImAgFnS	probíhat
kompletace	kompletace	k1gFnSc1	kompletace
již	již	k6eAd1	již
dovezených	dovezený	k2eAgInPc2d1	dovezený
raketových	raketový	k2eAgInPc2d1	raketový
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
začala	začít	k5eAaPmAgNnP	začít
probíhat	probíhat	k5eAaImF	probíhat
preventivní	preventivní	k2eAgNnPc1d1	preventivní
opatření	opatření	k1gNnPc1	opatření
(	(	kIx(	(
<g/>
otevírání	otevírání	k1gNnSc1	otevírání
krytů	kryt	k1gInPc2	kryt
<g/>
,	,	kIx,	,
poplašné	poplašný	k2eAgNnSc1d1	poplašné
cvičení	cvičení	k1gNnSc1	cvičení
aj.	aj.	kA	aj.
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
začala	začít	k5eAaPmAgFnS	začít
velká	velký	k2eAgFnSc1d1	velká
nákupní	nákupní	k2eAgFnSc1d1	nákupní
horečka	horečka	k1gFnSc1	horečka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vláda	vláda	k1gFnSc1	vláda
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
začala	začít	k5eAaPmAgFnS	začít
zvažovat	zvažovat	k5eAaImF	zvažovat
i	i	k9	i
embargo	embargo	k1gNnSc4	embargo
na	na	k7c4	na
dovoz	dovoz	k1gInSc4	dovoz
ropy	ropa	k1gFnSc2	ropa
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
a	a	k8xC	a
také	také	k9	také
začala	začít	k5eAaPmAgFnS	začít
nacvičovat	nacvičovat	k5eAaImF	nacvičovat
v	v	k7c6	v
Západním	západní	k2eAgInSc6d1	západní
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Obávala	obávat	k5eAaImAgFnS	obávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
konflikt	konflikt	k1gInSc1	konflikt
mohl	moct	k5eAaImAgInS	moct
přenést	přenést	k5eAaPmF	přenést
i	i	k9	i
tam	tam	k6eAd1	tam
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
Chruščov	Chruščov	k1gInSc1	Chruščov
poprvé	poprvé	k6eAd1	poprvé
uznal	uznat	k5eAaPmAgInS	uznat
existenci	existence	k1gFnSc4	existence
raket	raketa	k1gFnPc2	raketa
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
a	a	k8xC	a
také	také	k9	také
prvně	prvně	k?	prvně
naznačil	naznačit	k5eAaPmAgInS	naznačit
možnost	možnost	k1gFnSc4	možnost
kompromisu	kompromis	k1gInSc2	kompromis
<g/>
,	,	kIx,	,
o	o	k7c4	o
další	další	k2eAgInSc4d1	další
den	den	k1gInSc4	den
později	pozdě	k6eAd2	pozdě
vznesl	vznést	k5eAaPmAgMnS	vznést
jako	jako	k9	jako
základní	základní	k2eAgInSc4d1	základní
požadavek	požadavek	k1gInSc4	požadavek
stažení	stažení	k1gNnSc2	stažení
amerických	americký	k2eAgFnPc2d1	americká
raket	raketa	k1gFnPc2	raketa
z	z	k7c2	z
Turecka	Turecko	k1gNnSc2	Turecko
a	a	k8xC	a
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
27	[number]	k4	27
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
dali	dát	k5eAaPmAgMnP	dát
Sověti	Sovět	k1gMnPc1	Sovět
jasně	jasně	k6eAd1	jasně
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
nenechají	nechat	k5eNaPmIp3nP	nechat
tuto	tento	k3xDgFnSc4	tento
situaci	situace	k1gFnSc4	situace
jen	jen	k9	jen
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
popud	popud	k1gInSc4	popud
sovětského	sovětský	k2eAgMnSc2d1	sovětský
důstojníka	důstojník	k1gMnSc2	důstojník
nad	nad	k7c7	nad
Kubou	Kuba	k1gFnSc7	Kuba
sestřelen	sestřelen	k2eAgInSc1d1	sestřelen
americký	americký	k2eAgInSc1d1	americký
špionážní	špionážní	k2eAgInSc1d1	špionážní
letoun	letoun	k1gInSc1	letoun
Lockheed	Lockheed	k1gInSc1	Lockheed
U-	U-	k1gFnSc1	U-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
jeho	on	k3xPp3gInSc4	on
pilot	pilot	k1gMnSc1	pilot
major	major	k1gMnSc1	major
Rodolf	Rodolf	k1gMnSc1	Rodolf
Anderson	Anderson	k1gMnSc1	Anderson
<g/>
,	,	kIx,	,
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
zahynul	zahynout	k5eAaPmAgMnS	zahynout
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
zamyšlení	zamyšlení	k1gNnSc2	zamyšlení
Kennedyho	Kennedy	k1gMnSc2	Kennedy
nad	nad	k7c7	nad
vážností	vážnost	k1gFnSc7	vážnost
situace	situace	k1gFnSc2	situace
a	a	k8xC	a
tak	tak	k6eAd1	tak
napsal	napsat	k5eAaPmAgMnS	napsat
další	další	k2eAgInSc4d1	další
rozhořčený	rozhořčený	k2eAgInSc4d1	rozhořčený
dopis	dopis	k1gInSc4	dopis
pro	pro	k7c4	pro
Nikitu	Nikita	k1gFnSc4	Nikita
Chruščova	Chruščův	k2eAgFnSc1d1	Chruščova
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
prezident	prezident	k1gMnSc1	prezident
Kennedy	Kenned	k1gMnPc7	Kenned
rozčiloval	rozčilovat	k5eAaImAgMnS	rozčilovat
nad	nad	k7c7	nad
sestřelením	sestřelení	k1gNnSc7	sestřelení
letounu	letoun	k1gInSc2	letoun
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
podle	podle	k7c2	podle
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
práva	právo	k1gNnSc2	právo
porušil	porušit	k5eAaPmAgInS	porušit
územní	územní	k2eAgFnSc4d1	územní
celistvost	celistvost	k1gFnSc4	celistvost
Kuby	Kuba	k1gFnSc2	Kuba
a	a	k8xC	a
nad	nad	k7c7	nad
pokračující	pokračující	k2eAgFnSc7d1	pokračující
výstavbou	výstavba	k1gFnSc7	výstavba
sovětských	sovětský	k2eAgFnPc2d1	sovětská
základen	základna	k1gFnPc2	základna
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
Kennedy	Kenneda	k1gMnSc2	Kenneda
definitivně	definitivně	k6eAd1	definitivně
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
mírové	mírový	k2eAgNnSc4d1	Mírové
řešení	řešení	k1gNnSc4	řešení
končí	končit	k5eAaImIp3nS	končit
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
jasné	jasný	k2eAgNnSc1d1	jasné
ultimátum	ultimátum	k1gNnSc1	ultimátum
požadující	požadující	k2eAgNnSc1d1	požadující
stažení	stažení	k1gNnSc1	stažení
sovětských	sovětský	k2eAgFnPc2d1	sovětská
raket	raketa	k1gFnPc2	raketa
do	do	k7c2	do
48	[number]	k4	48
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stane	stanout	k5eAaPmIp3nS	stanout
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
upustí	upustit	k5eAaPmIp3nP	upustit
od	od	k7c2	od
blokády	blokáda	k1gFnSc2	blokáda
Kuby	Kuba	k1gFnSc2	Kuba
a	a	k8xC	a
budou	být	k5eAaImBp3nP	být
garantovat	garantovat	k5eAaBmF	garantovat
i	i	k9	i
její	její	k3xOp3gFnSc4	její
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Ultimáta	ultimátum	k1gNnPc1	ultimátum
se	se	k3xPyFc4	se
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
nezalekl	zaleknout	k5eNaPmAgInS	zaleknout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
dál	daleko	k6eAd2	daleko
stupňoval	stupňovat	k5eAaImAgInS	stupňovat
své	svůj	k3xOyFgInPc4	svůj
požadavky	požadavek	k1gInPc4	požadavek
<g/>
.	.	kIx.	.
</s>
<s>
Chruščov	Chruščov	k1gInSc1	Chruščov
odpověděl	odpovědět	k5eAaPmAgInS	odpovědět
již	již	k6eAd1	již
druhý	druhý	k4xOgInSc1	druhý
den	den	k1gInSc1	den
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
dopoledne	dopoledne	k1gNnSc2	dopoledne
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
odpověď	odpověď	k1gFnSc1	odpověď
byla	být	k5eAaImAgFnS	být
naprostým	naprostý	k2eAgInSc7d1	naprostý
šokem	šok	k1gInSc7	šok
pro	pro	k7c4	pro
Kubu	Kuba	k1gFnSc4	Kuba
a	a	k8xC	a
úlevou	úleva	k1gFnSc7	úleva
pro	pro	k7c4	pro
zbytek	zbytek	k1gInSc4	zbytek
světa	svět	k1gInSc2	svět
<g/>
:	:	kIx,	:
Chruščov	Chruščov	k1gInSc1	Chruščov
souhlasil	souhlasit	k5eAaImAgInS	souhlasit
se	s	k7c7	s
stažením	stažení	k1gNnSc7	stažení
zbraní	zbraň	k1gFnPc2	zbraň
z	z	k7c2	z
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vymínil	vymínit	k5eAaPmAgInS	vymínit
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
staženy	stažen	k2eAgFnPc1d1	stažena
rakety	raketa	k1gFnPc1	raketa
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Kennedy	Kenned	k1gMnPc4	Kenned
vyšel	vyjít	k5eAaPmAgMnS	vyjít
vstříc	vstříc	k6eAd1	vstříc
Sovětům	Sověty	k1gInPc3	Sověty
a	a	k8xC	a
slíbil	slíbit	k5eAaPmAgMnS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
rakety	raketa	k1gFnPc1	raketa
z	z	k7c2	z
Turecka	Turecko	k1gNnSc2	Turecko
a	a	k8xC	a
Itálie	Itálie	k1gFnSc2	Itálie
odstraní	odstranit	k5eAaPmIp3nS	odstranit
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
další	další	k2eAgInSc4d1	další
den	den	k1gInSc4	den
byly	být	k5eAaImAgFnP	být
nařízeny	nařízen	k2eAgFnPc1d1	nařízena
demontážní	demontážní	k2eAgFnPc1d1	demontážní
práce	práce	k1gFnPc1	práce
ze	z	k7c2	z
sovětské	sovětský	k2eAgFnSc2d1	sovětská
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
souhlasem	souhlas	k1gInSc7	souhlas
se	se	k3xPyFc4	se
hrozící	hrozící	k2eAgFnSc1d1	hrozící
horká	horký	k2eAgFnSc1d1	horká
válka	válka	k1gFnSc1	válka
stala	stát	k5eAaPmAgFnS	stát
opět	opět	k6eAd1	opět
studenou	studený	k2eAgFnSc7d1	studená
válkou	válka	k1gFnSc7	válka
a	a	k8xC	a
hrozba	hrozba	k1gFnSc1	hrozba
jaderné	jaderný	k2eAgFnSc2d1	jaderná
katastrofy	katastrofa	k1gFnSc2	katastrofa
byla	být	k5eAaImAgFnS	být
odložena	odložit	k5eAaPmNgFnS	odložit
na	na	k7c4	na
neurčito	neurčito	k1gNnSc4	neurčito
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
zdlouhavé	zdlouhavý	k2eAgNnSc4d1	zdlouhavé
jednání	jednání	k1gNnSc4	jednání
o	o	k7c6	o
přesných	přesný	k2eAgFnPc6d1	přesná
podmínkách	podmínka	k1gFnPc6	podmínka
stažení	stažení	k1gNnSc2	stažení
zbraní	zbraň	k1gFnPc2	zbraň
spory	spora	k1gFnSc2	spora
nastaly	nastat	k5eAaPmAgInP	nastat
kolem	kolem	k7c2	kolem
určování	určování	k1gNnSc2	určování
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
je	být	k5eAaImIp3nS	být
ještě	ještě	k6eAd1	ještě
obranná	obranný	k2eAgFnSc1d1	obranná
a	a	k8xC	a
co	co	k3yQnSc1	co
už	už	k6eAd1	už
útočná	útočný	k2eAgFnSc1d1	útočná
zbraň	zbraň	k1gFnSc1	zbraň
(	(	kIx(	(
<g/>
tento	tento	k3xDgInSc1	tento
spor	spor	k1gInSc1	spor
se	se	k3xPyFc4	se
týkal	týkat	k5eAaImAgInS	týkat
především	především	k6eAd1	především
letounů	letoun	k1gInPc2	letoun
Iljušin	iljušin	k1gInSc4	iljušin
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
největší	veliký	k2eAgInSc1d3	veliký
problém	problém	k1gInSc1	problém
byl	být	k5eAaImAgInS	být
ale	ale	k9	ale
s	s	k7c7	s
Castrovým	Castrův	k2eAgNnSc7d1	Castrovo
odmítnutím	odmítnutí	k1gNnSc7	odmítnutí
kontroly	kontrola	k1gFnSc2	kontrola
odsunu	odsun	k1gInSc2	odsun
sovětských	sovětský	k2eAgFnPc2d1	sovětská
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Fidel	Fidel	k1gMnSc1	Fidel
Castro	Castro	k1gNnSc4	Castro
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
(	(	kIx(	(
<g/>
celkem	celkem	k6eAd1	celkem
právem	právem	k6eAd1	právem
<g/>
)	)	kIx)	)
cítil	cítit	k5eAaImAgMnS	cítit
naprosto	naprosto	k6eAd1	naprosto
ošizen	ošidit	k5eAaPmNgMnS	ošidit
a	a	k8xC	a
vyřazen	vyřadit	k5eAaPmNgMnS	vyřadit
z	z	k7c2	z
veškerého	veškerý	k3xTgNnSc2	veškerý
jednání	jednání	k1gNnSc2	jednání
a	a	k8xC	a
neuklidnil	uklidnit	k5eNaPmAgMnS	uklidnit
ho	on	k3xPp3gNnSc4	on
ani	ani	k8xC	ani
příjezd	příjezd	k1gInSc1	příjezd
Anastáze	Anastáze	k1gFnSc2	Anastáze
Mikojana	Mikojan	k1gMnSc2	Mikojan
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
mu	on	k3xPp3gMnSc3	on
všechny	všechen	k3xTgInPc4	všechen
důvody	důvod	k1gInPc4	důvod
vynechání	vynechání	k1gNnSc2	vynechání
kubánské	kubánský	k2eAgFnSc2d1	kubánská
vlády	vláda	k1gFnSc2	vláda
z	z	k7c2	z
jednání	jednání	k1gNnSc2	jednání
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
hlavní	hlavní	k2eAgInPc4d1	hlavní
důvody	důvod	k1gInPc4	důvod
uvedl	uvést	k5eAaPmAgInS	uvést
nedostatek	nedostatek	k1gInSc1	nedostatek
času	čas	k1gInSc2	čas
a	a	k8xC	a
také	také	k9	také
argument	argument	k1gInSc1	argument
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
případě	případ	k1gInSc6	případ
útoku	útok	k1gInSc2	útok
jadernou	jaderný	k2eAgFnSc7d1	jaderná
zbraní	zbraň	k1gFnSc7	zbraň
by	by	kYmCp3nS	by
Kuba	Kuba	k1gFnSc1	Kuba
nebyla	být	k5eNaImAgFnS	být
jen	jen	k6eAd1	jen
poškozena	poškodit	k5eAaPmNgFnS	poškodit
jako	jako	k8xC	jako
světové	světový	k2eAgFnPc1d1	světová
velmoci	velmoc	k1gFnPc1	velmoc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rovnou	rovnou	k6eAd1	rovnou
by	by	kYmCp3nS	by
zmizela	zmizet	k5eAaPmAgFnS	zmizet
z	z	k7c2	z
mapy	mapa	k1gFnSc2	mapa
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
téže	tenže	k3xDgFnSc6	tenže
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Mikojan	Mikojan	k1gMnSc1	Mikojan
uklidňoval	uklidňovat	k5eAaImAgMnS	uklidňovat
rozčileného	rozčilený	k2eAgMnSc4d1	rozčilený
Castra	Castr	k1gMnSc4	Castr
<g/>
,	,	kIx,	,
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
náměstek	náměstek	k1gMnSc1	náměstek
sovětského	sovětský	k2eAgMnSc2d1	sovětský
ministra	ministr	k1gMnSc2	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Vasil	Vasil	k1gMnSc1	Vasil
Kuzněcov	Kuzněcov	k1gInSc1	Kuzněcov
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zde	zde	k6eAd1	zde
vyjednával	vyjednávat	k5eAaImAgInS	vyjednávat
přesnou	přesný	k2eAgFnSc4d1	přesná
podobu	podoba	k1gFnSc4	podoba
sovětsko-americké	sovětskomerický	k2eAgFnSc2d1	sovětsko-americká
dohody	dohoda	k1gFnSc2	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Blokáda	blokáda	k1gFnSc1	blokáda
Kuby	Kuba	k1gFnSc2	Kuba
byla	být	k5eAaImAgFnS	být
definitivně	definitivně	k6eAd1	definitivně
odvolána	odvolán	k2eAgFnSc1d1	odvolána
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1962	[number]	k4	1962
po	po	k7c6	po
splnění	splnění	k1gNnSc6	splnění
Kennedyho	Kennedy	k1gMnSc2	Kennedy
požadavků	požadavek	k1gInPc2	požadavek
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
souhlasil	souhlasit	k5eAaImAgInS	souhlasit
i	i	k9	i
s	s	k7c7	s
kontrolou	kontrola	k1gFnSc7	kontrola
demilitarizace	demilitarizace	k1gFnSc2	demilitarizace
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
jednání	jednání	k1gNnSc1	jednání
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
kroku	krok	k1gInSc6	krok
elegantně	elegantně	k6eAd1	elegantně
přehodil	přehodit	k5eAaPmAgMnS	přehodit
na	na	k7c4	na
Kubu	Kuba	k1gFnSc4	Kuba
<g/>
,	,	kIx,	,
coby	coby	k?	coby
suverénní	suverénní	k2eAgInSc1d1	suverénní
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Jednání	jednání	k1gNnSc1	jednání
sice	sice	k8xC	sice
ještě	ještě	k9	ještě
pokračovala	pokračovat	k5eAaImAgNnP	pokračovat
<g/>
,	,	kIx,	,
Chruščov	Chruščov	k1gInSc1	Chruščov
s	s	k7c7	s
Kennedym	Kennedymum	k1gNnPc2	Kennedymum
si	se	k3xPyFc3	se
vyměnili	vyměnit	k5eAaPmAgMnP	vyměnit
několik	několik	k4yIc4	několik
dalších	další	k2eAgInPc2d1	další
dopisů	dopis	k1gInPc2	dopis
(	(	kIx(	(
<g/>
ty	ty	k3xPp2nSc1	ty
se	se	k3xPyFc4	se
však	však	k9	však
týkaly	týkat	k5eAaImAgInP	týkat
spíše	spíše	k9	spíše
budoucnosti	budoucnost	k1gFnSc2	budoucnost
vztahů	vztah	k1gInPc2	vztah
s	s	k7c7	s
Kubou	Kuba	k1gFnSc7	Kuba
než	než	k8xS	než
minulé	minulý	k2eAgFnSc2d1	minulá
krize	krize	k1gFnSc2	krize
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
karibská	karibský	k2eAgFnSc1d1	karibská
krize	krize	k1gFnSc1	krize
pomalu	pomalu	k6eAd1	pomalu
utichala	utichat	k5eAaImAgFnS	utichat
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
americké	americký	k2eAgInPc1d1	americký
závazky	závazek	k1gInPc1	závazek
ke	k	k7c3	k
Kubě	Kuba	k1gFnSc3	Kuba
písemně	písemně	k6eAd1	písemně
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
až	až	k9	až
Henry	Henry	k1gMnSc1	Henry
Kissinger	Kissinger	k1gMnSc1	Kissinger
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
