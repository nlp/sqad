<s>
Vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
rozmístění	rozmístění	k1gNnSc2	rozmístění
sovětských	sovětský	k2eAgFnPc2d1	sovětská
raket	raketa	k1gFnPc2	raketa
středního	střední	k2eAgInSc2d1	střední
doletu	dolet	k1gInSc2	dolet
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
SSSR	SSSR	kA	SSSR
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
na	na	k7c4	na
umístění	umístění	k1gNnSc4	umístění
amerických	americký	k2eAgFnPc2d1	americká
raket	raketa	k1gFnPc2	raketa
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
.	.	kIx.	.
</s>
