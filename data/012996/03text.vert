<p>
<s>
Hoko	Hoko	k6eAd1	Hoko
přílbový	přílbový	k2eAgMnSc1d1	přílbový
(	(	kIx(	(
<g/>
Pauxi	Pauxe	k1gFnSc4	Pauxe
pauxi	pauxe	k1gFnSc4	pauxe
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
hrabavé	hrabavý	k2eAgMnPc4d1	hrabavý
ptáky	pták	k1gMnPc4	pták
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
mohutným	mohutný	k2eAgFnPc3d1	mohutná
příbuzným	příbuzná	k1gFnPc3	příbuzná
bažantů	bažant	k1gMnPc2	bažant
<g/>
,	,	kIx,	,
pávů	páv	k1gMnPc2	páv
či	či	k8xC	či
křepelek	křepelka	k1gFnPc2	křepelka
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
jihoamerických	jihoamerický	k2eAgInPc6d1	jihoamerický
pralesích	prales	k1gInPc6	prales
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
dostal	dostat	k5eAaPmAgInS	dostat
od	od	k7c2	od
zvláštní	zvláštní	k2eAgFnSc2d1	zvláštní
modravě	modravě	k6eAd1	modravě
šedé	šedý	k2eAgFnSc2d1	šedá
přilby	přilba	k1gFnSc2	přilba
před	před	k7c7	před
očima	oko	k1gNnPc7	oko
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
výskytu	výskyt	k1gInSc2	výskyt
loveckou	lovecký	k2eAgFnSc7d1	lovecká
trofejí	trofej	k1gFnSc7	trofej
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
pohlaví	pohlaví	k1gNnPc1	pohlaví
mají	mít	k5eAaImIp3nP	mít
obvykle	obvykle	k6eAd1	obvykle
černé	černý	k2eAgNnSc4d1	černé
tělo	tělo	k1gNnSc4	tělo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzácně	vzácně	k6eAd1	vzácně
lze	lze	k6eAd1	lze
narazit	narazit	k5eAaPmF	narazit
i	i	k9	i
na	na	k7c4	na
samičky	samička	k1gFnPc4	samička
rezavé	rezavý	k2eAgNnSc1d1	rezavé
s	s	k7c7	s
černým	černý	k2eAgNnSc7d1	černé
pruhováním	pruhování	k1gNnSc7	pruhování
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
hoka	hok	k1gInSc2	hok
přílbového	přílbový	k2eAgInSc2d1	přílbový
snáší	snášet	k5eAaImIp3nS	snášet
obvykle	obvykle	k6eAd1	obvykle
dvě	dva	k4xCgNnPc4	dva
vejce	vejce	k1gNnPc4	vejce
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
inkubace	inkubace	k1gFnSc1	inkubace
trvá	trvat	k5eAaImIp3nS	trvat
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
34	[number]	k4	34
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekologie	ekologie	k1gFnSc1	ekologie
==	==	k?	==
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgFnSc4d3	veliký
aktivitu	aktivita	k1gFnSc4	aktivita
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
za	za	k7c2	za
rozbřesku	rozbřesk	k1gInSc2	rozbřesk
nebo	nebo	k8xC	nebo
soumraku	soumrak	k1gInSc2	soumrak
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
párech	pár	k1gInPc6	pár
či	či	k8xC	či
rodinných	rodinný	k2eAgFnPc6d1	rodinná
skupinách	skupina	k1gFnPc6	skupina
hledá	hledat	k5eAaImIp3nS	hledat
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nP	živit
se	se	k3xPyFc4	se
plody	plod	k1gInPc7	plod
<g/>
,	,	kIx,	,
semeny	semeno	k1gNnPc7	semeno
<g/>
,	,	kIx,	,
pupeny	pupen	k1gInPc7	pupen
a	a	k8xC	a
mladými	mladý	k2eAgInPc7d1	mladý
listy	list	k1gInPc7	list
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
hrabavých	hrabavý	k2eAgFnPc2d1	hrabavá
si	se	k3xPyFc3	se
mláďata	mládě	k1gNnPc4	mládě
neshání	shánět	k5eNaImIp3nS	shánět
potravu	potrava	k1gFnSc4	potrava
samostatně	samostatně	k6eAd1	samostatně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
oba	dva	k4xCgMnPc4	dva
rodiče	rodič	k1gMnPc4	rodič
či	či	k8xC	či
alespoň	alespoň	k9	alespoň
matka	matka	k1gFnSc1	matka
jim	on	k3xPp3gMnPc3	on
ji	on	k3xPp3gFnSc4	on
nosí	nosit	k5eAaImIp3nP	nosit
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
skrytě	skrytě	k6eAd1	skrytě
v	v	k7c6	v
podrostu	podrost	k1gInSc6	podrost
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
ohrožení	ohrožení	k1gNnSc6	ohrožení
se	se	k3xPyFc4	se
stahuje	stahovat	k5eAaImIp3nS	stahovat
na	na	k7c4	na
strom	strom	k1gInSc4	strom
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
na	na	k7c4	na
noc	noc	k1gFnSc4	noc
se	se	k3xPyFc4	se
uchyluje	uchylovat	k5eAaImIp3nS	uchylovat
hřadovat	hřadovat	k5eAaImF	hřadovat
do	do	k7c2	do
větví	větev	k1gFnPc2	větev
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Vábení	vábení	k1gNnSc1	vábení
samic	samice	k1gFnPc2	samice
při	při	k7c6	při
toku	tok	k1gInSc6	tok
probíhá	probíhat	k5eAaImIp3nS	probíhat
formou	forma	k1gFnSc7	forma
nabízení	nabízení	k1gNnSc2	nabízení
potravy	potrava	k1gFnSc2	potrava
v	v	k7c6	v
zobáku	zobák	k1gInSc6	zobák
samce	samec	k1gInSc2	samec
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
mláďata	mládě	k1gNnPc4	mládě
se	se	k3xPyFc4	se
starají	starat	k5eAaImIp3nP	starat
oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Partneři	partner	k1gMnPc1	partner
si	se	k3xPyFc3	se
bývají	bývat	k5eAaImIp3nP	bývat
věrní	věrný	k2eAgMnPc1d1	věrný
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
ukázal	ukázat	k5eAaPmAgInS	ukázat
jako	jako	k9	jako
problematický	problematický	k2eAgInSc1d1	problematický
při	při	k7c6	při
snahách	snaha	k1gFnPc6	snaha
chovat	chovat	k5eAaImF	chovat
hoka	hoka	k6eAd1	hoka
na	na	k7c4	na
maso	maso	k1gNnSc4	maso
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
měly	mít	k5eAaImAgFnP	mít
i	i	k9	i
další	další	k2eAgFnPc1d1	další
typické	typický	k2eAgFnPc1d1	typická
charakteristiky	charakteristika	k1gFnPc1	charakteristika
<g/>
:	:	kIx,	:
nevelká	velký	k2eNgFnSc1d1	nevelká
snůška	snůška	k1gFnSc1	snůška
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
choulostivost	choulostivost	k1gFnSc1	choulostivost
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
<g/>
,	,	kIx,	,
že	že	k8xS	že
snaha	snaha	k1gFnSc1	snaha
chovat	chovat	k5eAaImF	chovat
tyto	tento	k3xDgMnPc4	tento
ptáky	pták	k1gMnPc4	pták
pro	pro	k7c4	pro
lidské	lidský	k2eAgFnPc4d1	lidská
potřeby	potřeba	k1gFnPc4	potřeba
na	na	k7c4	na
maso	maso	k1gNnSc4	maso
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
jejich	jejich	k3xOp3gNnSc6	jejich
objevení	objevení	k1gNnSc6	objevení
Evropany	Evropan	k1gMnPc4	Evropan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Hoko	Hoko	k1gMnSc1	Hoko
přílbový	přílbový	k2eAgMnSc1d1	přílbový
se	se	k3xPyFc4	se
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
hustých	hustý	k2eAgInPc6d1	hustý
mlžných	mlžný	k2eAgInPc6d1	mlžný
lesech	les	k1gInPc6	les
na	na	k7c6	na
území	území	k1gNnSc6	území
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
a	a	k8xC	a
severozápadě	severozápad	k1gInSc6	severozápad
Venezuely	Venezuela	k1gFnSc2	Venezuela
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ohrožení	ohrožení	k1gNnSc1	ohrožení
==	==	k?	==
</s>
</p>
<p>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
dostal	dostat	k5eAaPmAgInS	dostat
od	od	k7c2	od
zvláštní	zvláštní	k2eAgFnSc2d1	zvláštní
modravě	modravě	k6eAd1	modravě
šedé	šedý	k2eAgFnSc2d1	šedá
přilby	přilba	k1gFnSc2	přilba
před	před	k7c7	před
očima	oko	k1gNnPc7	oko
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
výskytu	výskyt	k1gInSc2	výskyt
loveckou	lovecký	k2eAgFnSc7d1	lovecká
trofejí	trofej	k1gFnSc7	trofej
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
šperky	šperk	k1gInPc1	šperk
<g/>
.	.	kIx.	.
</s>
<s>
Početní	početní	k2eAgInPc1d1	početní
stavy	stav	k1gInPc1	stav
hoků	hok	k1gInPc2	hok
však	však	k9	však
neklesají	klesat	k5eNaImIp3nP	klesat
jen	jen	k9	jen
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
.	.	kIx.	.
</s>
<s>
Loven	loven	k2eAgInSc1d1	loven
je	být	k5eAaImIp3nS	být
také	také	k9	také
pro	pro	k7c4	pro
chutné	chutný	k2eAgNnSc4d1	chutné
maso	maso	k1gNnSc4	maso
a	a	k8xC	a
ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
ho	on	k3xPp3gInSc4	on
i	i	k9	i
nelegální	legální	k2eNgInSc4d1	nelegální
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
drogami	droga	k1gFnPc7	droga
–	–	k?	–
horské	horský	k2eAgInPc4d1	horský
lesy	les	k1gInPc4	les
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žije	žít	k5eAaImIp3nS	žít
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
totiž	totiž	k9	totiž
káceny	kácen	k2eAgFnPc1d1	kácen
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
na	na	k7c6	na
jejich	jejich	k3xOp3gNnSc6	jejich
místě	místo	k1gNnSc6	místo
mohly	moct	k5eAaImAgFnP	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
plantáže	plantáž	k1gFnPc4	plantáž
koky	koka	k1gFnSc2	koka
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nS	docházet
tak	tak	k6eAd1	tak
ke	k	k7c3	k
ztrátě	ztráta	k1gFnSc3	ztráta
jejich	jejich	k3xOp3gNnSc2	jejich
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hoko	Hoko	k6eAd1	Hoko
přílbový	přílbový	k2eAgInSc1d1	přílbový
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
na	na	k7c6	na
Červeného	Červeného	k2eAgInSc6d1	Červeného
seznamu	seznam	k1gInSc6	seznam
IUCN	IUCN	kA	IUCN
zařazen	zařadit	k5eAaPmNgInS	zařadit
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chov	chov	k1gInSc1	chov
v	v	k7c6	v
zoo	zoo	k1gFnSc6	zoo
==	==	k?	==
</s>
</p>
<p>
<s>
Hoko	Hoko	k1gMnSc1	Hoko
přílbový	přílbový	k2eAgMnSc1d1	přílbový
nejenže	nejenže	k6eAd1	nejenže
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ohrožené	ohrožený	k2eAgInPc4d1	ohrožený
druhy	druh	k1gInPc4	druh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
taky	taky	k6eAd1	taky
mezi	mezi	k7c7	mezi
druhy	druh	k1gInPc7	druh
v	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
péči	péče	k1gFnSc6	péče
relativně	relativně	k6eAd1	relativně
málo	málo	k6eAd1	málo
chované	chovaný	k2eAgNnSc1d1	chované
a	a	k8xC	a
zejména	zejména	k9	zejména
odchovávané	odchovávaný	k2eAgNnSc1d1	odchovávané
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
chován	chovat	k5eAaImNgInS	chovat
jen	jen	k9	jen
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
zoologických	zoologický	k2eAgFnPc6d1	zoologická
zahradách	zahrada	k1gFnPc6	zahrada
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
Zoo	zoo	k1gFnSc6	zoo
Hluboká	hluboký	k2eAgFnSc1d1	hluboká
a	a	k8xC	a
v	v	k7c6	v
Zoo	zoo	k1gFnSc6	zoo
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
pak	pak	k6eAd1	pak
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
v	v	k7c6	v
Zoo	zoo	k1gFnSc6	zoo
Bojnice	Bojnice	k1gFnPc1	Bojnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
patřily	patřit	k5eAaImAgFnP	patřit
k	k	k7c3	k
chovatelům	chovatel	k1gMnPc3	chovatel
tohoto	tento	k3xDgInSc2	tento
opeřence	opeřenec	k1gMnSc4	opeřenec
krátkodobě	krátkodobě	k6eAd1	krátkodobě
také	také	k9	také
zoo	zoo	k1gFnSc1	zoo
ve	v	k7c6	v
Dvoře	Dvůr	k1gInSc6	Dvůr
Králové	Králová	k1gFnSc2	Králová
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
a	a	k8xC	a
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
jej	on	k3xPp3gNnSc2	on
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
pěti	pět	k4xCc6	pět
desítkách	desítka	k1gFnPc6	desítka
zoologických	zoologický	k2eAgNnPc2d1	zoologické
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Chov	chov	k1gInSc1	chov
v	v	k7c6	v
Zoo	zoo	k1gFnSc6	zoo
Praha	Praha	k1gFnSc1	Praha
===	===	k?	===
</s>
</p>
<p>
<s>
Chov	chov	k1gInSc1	chov
v	v	k7c6	v
Zoo	zoo	k1gNnSc6	zoo
Praha	Praha	k1gFnSc1	Praha
započal	započnout	k5eAaPmAgInS	započnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
přišel	přijít	k5eAaPmAgMnS	přijít
jeden	jeden	k4xCgMnSc1	jeden
samec	samec	k1gMnSc1	samec
a	a	k8xC	a
dvě	dva	k4xCgFnPc1	dva
samice	samice	k1gFnPc1	samice
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
zvířata	zvíře	k1gNnPc4	zvíře
původem	původ	k1gInSc7	původ
z	z	k7c2	z
německého	německý	k2eAgInSc2d1	německý
Erfurtu	Erfurt	k1gInSc2	Erfurt
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
vylíhli	vylíhnout	k5eAaPmAgMnP	vylíhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Získáni	získán	k2eAgMnPc1d1	získán
však	však	k9	však
byli	být	k5eAaImAgMnP	být
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
ze	z	k7c2	z
Zoo	zoo	k1gFnSc2	zoo
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
odchov	odchov	k1gInSc1	odchov
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
dokonce	dokonce	k9	dokonce
vůbec	vůbec	k9	vůbec
prvním	první	k4xOgInSc7	první
odchovem	odchov	k1gInSc7	odchov
na	na	k7c6	na
území	území	k1gNnSc6	území
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
a	a	k8xC	a
potažmo	potažmo	k6eAd1	potažmo
celého	celý	k2eAgNnSc2d1	celé
někdejšího	někdejší	k2eAgNnSc2d1	někdejší
Československa	Československo	k1gNnSc2	Československo
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
teprve	teprve	k6eAd1	teprve
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgNnPc4	dva
mláďata	mládě	k1gNnPc4	mládě
se	se	k3xPyFc4	se
vylíhla	vylíhnout	k5eAaPmAgFnS	vylíhnout
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
předchozích	předchozí	k2eAgFnPc6d1	předchozí
snůškách	snůška	k1gFnPc6	snůška
samice	samice	k1gFnSc2	samice
vejce	vejce	k1gNnSc2	vejce
ignorovala	ignorovat	k5eAaImAgFnS	ignorovat
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
je	být	k5eAaImIp3nS	být
i	i	k9	i
rozbila	rozbít	k5eAaPmAgFnS	rozbít
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
celkových	celkový	k2eAgNnPc2d1	celkové
13	[number]	k4	13
vajec	vejce	k1gNnPc2	vejce
pět	pět	k4xCc4	pět
rozbito	rozbít	k5eAaPmNgNnS	rozbít
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dvě	dva	k4xCgFnPc4	dva
živě	živě	k6eAd1	živě
narozená	narozený	k2eAgNnPc4d1	narozené
mláďata	mládě	k1gNnPc4	mládě
se	se	k3xPyFc4	se
však	však	k9	však
jejich	jejich	k3xOp3gFnSc1	jejich
matka	matka	k1gFnSc1	matka
začala	začít	k5eAaPmAgFnS	začít
vzorně	vzorně	k6eAd1	vzorně
starat	starat	k5eAaImF	starat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
odchovat	odchovat	k5eAaPmF	odchovat
jednoho	jeden	k4xCgMnSc4	jeden
samce	samec	k1gMnSc4	samec
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
samici	samice	k1gFnSc4	samice
<g/>
,	,	kIx,	,
a	a	k8xC	a
výrazně	výrazně	k6eAd1	výrazně
tak	tak	k6eAd1	tak
obohatit	obohatit	k5eAaPmF	obohatit
evropský	evropský	k2eAgInSc4d1	evropský
chov	chov	k1gInSc4	chov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2018	[number]	k4	2018
byl	být	k5eAaImAgInS	být
chován	chovat	k5eAaImNgInS	chovat
pár	pár	k1gInSc1	pár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hoko	Hoko	k6eAd1	Hoko
přílbový	přílbový	k2eAgMnSc1d1	přílbový
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
zoo	zoo	k1gFnSc2	zoo
<g/>
,	,	kIx,	,
v	v	k7c6	v
komplexu	komplex	k1gInSc6	komplex
voliér	voliéra	k1gFnPc2	voliéra
u	u	k7c2	u
pavilonu	pavilon	k1gInSc2	pavilon
tučňáků	tučňák	k1gMnPc2	tučňák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Ročenka	ročenka	k1gFnSc1	ročenka
Unie	unie	k1gFnSc2	unie
českých	český	k2eAgFnPc2d1	Česká
a	a	k8xC	a
slovenských	slovenský	k2eAgFnPc2d1	slovenská
zoologických	zoologický	k2eAgFnPc2d1	zoologická
zahrad	zahrada	k1gFnPc2	zahrada
2016	[number]	k4	2016
</s>
</p>
<p>
<s>
Vaidl	Vaidnout	k5eAaPmAgMnS	Vaidnout
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
.	.	kIx.	.
</s>
<s>
Hoko	Hoko	k6eAd1	Hoko
poprvé	poprvé	k6eAd1	poprvé
<g/>
.	.	kIx.	.
</s>
<s>
Trojský	trojský	k2eAgMnSc1d1	trojský
koník	koník	k1gMnSc1	koník
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
,	,	kIx,	,
s.	s.	k?	s.
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zoo	zoo	k1gFnSc1	zoo
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Přehled	přehled	k1gInSc4	přehled
"	"	kIx"	"
<g/>
Vzácně	vzácně	k6eAd1	vzácně
chovaní	chovaný	k2eAgMnPc1d1	chovaný
v	v	k7c6	v
zoo	zoo	k1gFnSc6	zoo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
hoko	hoko	k1gMnSc1	hoko
přílbový	přílbový	k2eAgMnSc1d1	přílbový
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Pauxi	Pauxe	k1gFnSc6	Pauxe
praxi	praxe	k1gFnSc6	praxe
ve	v	k7c6	v
Wikidruzích	Wikidruh	k1gInPc6	Wikidruh
</s>
</p>
