<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gMnSc4	jeho
nejznámější	známý	k2eAgNnPc1d3	nejznámější
díla	dílo	k1gNnPc1	dílo
patří	patřit	k5eAaImIp3nP	patřit
například	například	k6eAd1	například
Zahrada	zahrada	k1gFnSc1	zahrada
pozemských	pozemský	k2eAgFnPc2d1	pozemská
rozkoší	rozkoš	k1gFnPc2	rozkoš
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
malba	malba	k1gFnSc1	malba
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
ráj	ráj	k1gInSc4	ráj
s	s	k7c7	s
Adamem	Adam	k1gMnSc7	Adam
a	a	k8xC	a
Evou	Eva	k1gFnSc7	Eva
a	a	k8xC	a
mnoho	mnoho	k6eAd1	mnoho
podivuhodnými	podivuhodný	k2eAgNnPc7d1	podivuhodné
zvířaty	zvíře	k1gNnPc7	zvíře
v	v	k7c6	v
levé	levý	k2eAgFnSc6d1	levá
části	část	k1gFnSc6	část
<g/>
,	,	kIx,	,
pozemské	pozemský	k2eAgFnPc1d1	pozemská
rozkoše	rozkoš	k1gFnPc1	rozkoš
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
nahých	nahý	k2eAgFnPc2d1	nahá
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
s	s	k7c7	s
ovocem	ovoce	k1gNnSc7	ovoce
a	a	k8xC	a
děsivými	děsivý	k2eAgMnPc7d1	děsivý
ptáky	pták	k1gMnPc7	pták
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
a	a	k8xC	a
fantazijním	fantazijní	k2eAgNnSc7d1	fantazijní
zobrazením	zobrazení	k1gNnSc7	zobrazení
hudebního	hudební	k2eAgNnSc2d1	hudební
pekla	peklo	k1gNnSc2	peklo
trestajícího	trestající	k2eAgInSc2d1	trestající
hříchy	hřích	k1gInPc4	hřích
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
panelu	panel	k1gInSc6	panel
triptychu	triptych	k1gInSc2	triptych
<g/>
.	.	kIx.	.
</s>
