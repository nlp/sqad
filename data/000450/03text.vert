<s>
Anoda	anoda	k1gFnSc1	anoda
je	být	k5eAaImIp3nS	být
elektroda	elektroda	k1gFnSc1	elektroda
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
proudí	proudit	k5eAaPmIp3nP	proudit
elektrony	elektron	k1gInPc1	elektron
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
katodě	katoda	k1gFnSc3	katoda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
elektrochemii	elektrochemie	k1gFnSc6	elektrochemie
představuje	představovat	k5eAaImIp3nS	představovat
anoda	anoda	k1gFnSc1	anoda
elektrodu	elektroda	k1gFnSc4	elektroda
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgFnPc4	který
probíhá	probíhat	k5eAaImIp3nS	probíhat
oxidace	oxidace	k1gFnSc1	oxidace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
vložení	vložení	k1gNnSc2	vložení
vnějšího	vnější	k2eAgNnSc2d1	vnější
napětí	napětí	k1gNnSc2	napětí
na	na	k7c4	na
elektrody	elektroda	k1gFnPc4	elektroda
(	(	kIx(	(
<g/>
při	při	k7c6	při
elektrolýze	elektrolýza	k1gFnSc6	elektrolýza
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
anoda	anoda	k1gFnSc1	anoda
kladným	kladný	k2eAgNnSc7d1	kladné
pólem	pólo	k1gNnSc7	pólo
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
elektrického	elektrický	k2eAgInSc2d1	elektrický
článku	článek	k1gInSc2	článek
záporným	záporný	k2eAgInSc7d1	záporný
pólem	pól	k1gInSc7	pól
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
elektronice	elektronika	k1gFnSc6	elektronika
se	se	k3xPyFc4	se
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
názvem	název	k1gInSc7	název
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
u	u	k7c2	u
součástek	součástka	k1gFnPc2	součástka
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
dioda	dioda	k1gFnSc1	dioda
nebo	nebo	k8xC	nebo
elektronka	elektronka	k1gFnSc1	elektronka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
anoda	anoda	k1gFnSc1	anoda
obvykle	obvykle	k6eAd1	obvykle
představuje	představovat	k5eAaImIp3nS	představovat
elektrodu	elektroda	k1gFnSc4	elektroda
s	s	k7c7	s
kladným	kladný	k2eAgNnSc7d1	kladné
napětím	napětí	k1gNnSc7	napětí
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
polovodičových	polovodičový	k2eAgFnPc2d1	polovodičová
diod	dioda	k1gFnPc2	dioda
je	být	k5eAaImIp3nS	být
anoda	anoda	k1gFnSc1	anoda
polovodič	polovodič	k1gInSc1	polovodič
typu	typ	k1gInSc2	typ
P	P	kA	P
a	a	k8xC	a
v	v	k7c6	v
propustném	propustný	k2eAgInSc6d1	propustný
směru	směr	k1gInSc6	směr
je	být	k5eAaImIp3nS	být
připojena	připojen	k2eAgFnSc1d1	připojena
ke	k	k7c3	k
kladnému	kladný	k2eAgNnSc3d1	kladné
napětí	napětí	k1gNnSc3	napětí
<g/>
.	.	kIx.	.
</s>
<s>
Opakem	opak	k1gInSc7	opak
anody	anoda	k1gFnSc2	anoda
je	být	k5eAaImIp3nS	být
katoda	katoda	k1gFnSc1	katoda
<g/>
.	.	kIx.	.
</s>
<s>
Mnemotechnická	mnemotechnický	k2eAgFnSc1d1	mnemotechnická
pomůcka	pomůcka	k1gFnSc1	pomůcka
-	-	kIx~	-
jak	jak	k6eAd1	jak
si	se	k3xPyFc3	se
zapamatovat	zapamatovat	k5eAaPmF	zapamatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
anoda	anoda	k1gFnSc1	anoda
je	být	k5eAaImIp3nS	být
kladná	kladný	k2eAgFnSc1d1	kladná
elektroda	elektroda	k1gFnSc1	elektroda
</s>
