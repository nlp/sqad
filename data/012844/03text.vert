<p>
<s>
Zápřednice	Zápřednice	k1gFnSc1	Zápřednice
jedovatá	jedovatý	k2eAgFnSc1d1	jedovatá
(	(	kIx(	(
<g/>
Cheiracanthium	Cheiracanthium	k1gNnSc1	Cheiracanthium
punctorium	punctorium	k1gNnSc1	punctorium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
pavouka	pavouk	k1gMnSc2	pavouk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
také	také	k9	také
na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
např.	např.	kA	např.
na	na	k7c6	na
Litoměřicku	Litoměřicko	k1gNnSc6	Litoměřicko
<g/>
.	.	kIx.	.
<g/>
Samice	samice	k1gFnSc1	samice
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
může	moct	k5eAaImIp3nS	moct
dorůstat	dorůstat	k5eAaImF	dorůstat
až	až	k9	až
1,5	[number]	k4	1,5
cm	cm	kA	cm
<g/>
,	,	kIx,	,
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
samec	samec	k1gMnSc1	samec
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
menší	malý	k2eAgNnSc1d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
maximálně	maximálně	k6eAd1	maximálně
1,2	[number]	k4	1,2
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
původních	původní	k2eAgInPc2d1	původní
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
nápadný	nápadný	k2eAgInSc1d1	nápadný
druh	druh	k1gInSc1	druh
si	se	k3xPyFc3	se
během	během	k7c2	během
léta	léto	k1gNnSc2	léto
dělá	dělat	k5eAaImIp3nS	dělat
zámotky	zámotek	k1gInPc4	zámotek
na	na	k7c6	na
vrcholcích	vrcholek	k1gInPc6	vrcholek
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Zámotek	zámotek	k1gInSc1	zámotek
si	se	k3xPyFc3	se
zápřednice	zápřednice	k1gFnSc2	zápřednice
jedovaté	jedovatý	k2eAgFnPc4d1	jedovatá
nejčastěji	často	k6eAd3	často
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
v	v	k7c6	v
květenství	květenství	k1gNnSc6	květenství
třtiny	třtina	k1gFnSc2	třtina
křovištní	křovištní	k2eAgFnSc2d1	křovištní
(	(	kIx(	(
<g/>
Calamagrostis	Calamagrostis	k1gFnSc2	Calamagrostis
epigejos	epigejos	k1gInSc1	epigejos
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mnohdy	mnohdy	k6eAd1	mnohdy
ale	ale	k9	ale
dochází	docházet	k5eAaImIp3nS	docházet
také	také	k9	také
k	k	k7c3	k
osídlování	osídlování	k1gNnSc3	osídlování
dalších	další	k2eAgFnPc2d1	další
vhodných	vhodný	k2eAgFnPc2d1	vhodná
rostlin	rostlina	k1gFnPc2	rostlina
(	(	kIx(	(
<g/>
lipnice	lipnice	k1gFnSc1	lipnice
–	–	k?	–
Poa	Poa	k1gFnPc2	Poa
<g/>
,	,	kIx,	,
ovsíky	ovsík	k1gInPc1	ovsík
–	–	k?	–
Arrhenatherum	Arrhenatherum	k1gInSc4	Arrhenatherum
<g/>
,	,	kIx,	,
miříkovité	miříkovitý	k2eAgFnPc4d1	miříkovitá
–	–	k?	–
Apiaceae	Apiacea	k1gFnPc4	Apiacea
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejjedovatějších	jedovatý	k2eAgMnPc2d3	nejjedovatější
pavouků	pavouk	k1gMnPc2	pavouk
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc1	jeho
kousnutí	kousnutí	k1gNnPc1	kousnutí
působí	působit	k5eAaImIp3nP	působit
velkou	velký	k2eAgFnSc4d1	velká
bolest	bolest	k1gFnSc4	bolest
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
spojené	spojený	k2eAgNnSc1d1	spojené
s	s	k7c7	s
horečkou	horečka	k1gFnSc7	horečka
<g/>
,	,	kIx,	,
pocity	pocit	k1gInPc4	pocit
úzkosti	úzkost	k1gFnSc2	úzkost
a	a	k8xC	a
chvilkovým	chvilkový	k2eAgNnSc7d1	chvilkové
ochrnutím	ochrnutí	k1gNnSc7	ochrnutí
okolo	okolo	k7c2	okolo
místa	místo	k1gNnSc2	místo
pokousání	pokousání	k1gNnSc2	pokousání
<g/>
.	.	kIx.	.
</s>
<s>
Následky	následek	k1gInPc1	následek
kousnutí	kousnutí	k1gNnPc2	kousnutí
však	však	k9	však
většinou	většina	k1gFnSc7	většina
do	do	k7c2	do
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
samy	sám	k3xTgFnPc1	sám
odezní	odeznět	k5eAaPmIp3nS	odeznět
<g/>
.	.	kIx.	.
</s>
<s>
Reakce	reakce	k1gFnPc1	reakce
organismu	organismus	k1gInSc2	organismus
bývají	bývat	k5eAaImIp3nP	bývat
individuální	individuální	k2eAgInPc1d1	individuální
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
však	však	k9	však
třeba	třeba	k6eAd1	třeba
se	se	k3xPyFc4	se
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
nějak	nějak	k6eAd1	nějak
zvlášť	zvlášť	k6eAd1	zvlášť
obávat	obávat	k5eAaImF	obávat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
druh	druh	k1gInSc1	druh
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
velmi	velmi	k6eAd1	velmi
vzácný	vzácný	k2eAgInSc4d1	vzácný
<g/>
,	,	kIx,	,
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
však	však	k9	však
zaznamenáváme	zaznamenávat	k5eAaImIp1nP	zaznamenávat
bohaté	bohatý	k2eAgInPc1d1	bohatý
nálezy	nález	k1gInPc1	nález
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
zápřednice	zápřednice	k1gFnSc1	zápřednice
jedovatá	jedovatý	k2eAgFnSc1d1	jedovatá
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
zápřednice	zápřednice	k1gFnSc2	zápřednice
jedovatá	jedovatý	k2eAgFnSc1d1	jedovatá
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Cheiracanthium	Cheiracanthium	k1gNnSc1	Cheiracanthium
punctorium	punctorium	k1gNnSc4	punctorium
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
