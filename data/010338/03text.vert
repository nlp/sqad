<p>
<s>
Kaple	kaple	k1gFnSc1	kaple
svaté	svatý	k2eAgFnSc2d1	svatá
Anny	Anna	k1gFnSc2	Anna
v	v	k7c6	v
Jáchymově	Jáchymov	k1gInSc6	Jáchymov
je	být	k5eAaImIp3nS	být
původně	původně	k6eAd1	původně
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
stavba	stavba	k1gFnSc1	stavba
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1517	[number]	k4	1517
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
chráněna	chráněn	k2eAgFnSc1d1	chráněna
jako	jako	k8xC	jako
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
kaple	kaple	k1gFnSc1	kaple
několikrát	několikrát	k6eAd1	několikrát
vyhořela	vyhořet	k5eAaPmAgFnS	vyhořet
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1642	[number]	k4	1642
<g/>
.	.	kIx.	.
</s>
<s>
Obnovena	obnoven	k2eAgNnPc1d1	obnoveno
již	již	k9	již
jako	jako	k8xC	jako
kamenná	kamenný	k2eAgFnSc1d1	kamenná
byla	být	k5eAaImAgFnS	být
až	až	k9	až
po	po	k7c6	po
uplynutí	uplynutí	k1gNnSc6	uplynutí
132	[number]	k4	132
let	léto	k1gNnPc2	léto
–	–	k?	–
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1778	[number]	k4	1778
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krátké	krátký	k2eAgFnSc6d1	krátká
době	doba	k1gFnSc6	doba
–	–	k?	–
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1783	[number]	k4	1783
-	-	kIx~	-
se	se	k3xPyFc4	se
přestala	přestat	k5eAaPmAgFnS	přestat
využívat	využívat	k5eAaPmF	využívat
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
byla	být	k5eAaImAgFnS	být
vysvěcena	vysvětit	k5eAaPmNgFnS	vysvětit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1803	[number]	k4	1803
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
velikém	veliký	k2eAgInSc6d1	veliký
požáru	požár	k1gInSc6	požár
města	město	k1gNnSc2	město
31	[number]	k4	31
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1873	[number]	k4	1873
vyhořela	vyhořet	k5eAaPmAgFnS	vyhořet
na	na	k7c6	na
holé	holý	k2eAgFnSc6d1	holá
zdi	zeď	k1gFnSc6	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
byla	být	k5eAaImAgFnS	být
opět	opět	k6eAd1	opět
obnovena	obnoven	k2eAgFnSc1d1	obnovena
a	a	k8xC	a
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
také	také	k9	také
pochází	pocházet	k5eAaImIp3nS	pocházet
její	její	k3xOp3gFnSc1	její
současná	současný	k2eAgFnSc1d1	současná
podoba	podoba	k1gFnSc1	podoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
původní	původní	k2eAgFnSc6d1	původní
podobě	podoba	k1gFnSc6	podoba
nebylo	být	k5eNaImAgNnS	být
obnovena	obnovit	k5eAaPmNgNnP	obnovit
pouze	pouze	k6eAd1	pouze
střecha	střecha	k1gFnSc1	střecha
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
totiž	totiž	k9	totiž
byla	být	k5eAaImAgFnS	být
kaple	kaple	k1gFnSc1	kaple
zastřešena	zastřešit	k5eAaPmNgFnS	zastřešit
vysokou	vysoký	k2eAgFnSc7d1	vysoká
kupolí	kupole	k1gFnSc7	kupole
zakončenou	zakončený	k2eAgFnSc7d1	zakončená
lucernou	lucerna	k1gFnSc7	lucerna
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
proběhly	proběhnout	k5eAaPmAgInP	proběhnout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1976	[number]	k4	1976
a	a	k8xC	a
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
byla	být	k5eAaImAgFnS	být
kaple	kaple	k1gFnSc1	kaple
zbavena	zbavit	k5eAaPmNgFnS	zbavit
dřevomorky	dřevomorka	k1gFnPc4	dřevomorka
a	a	k8xC	a
dostala	dostat	k5eAaPmAgFnS	dostat
novou	nový	k2eAgFnSc4d1	nová
střechu	střecha	k1gFnSc4	střecha
s	s	k7c7	s
fasádou	fasáda	k1gFnSc7	fasáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Obdélníkový	obdélníkový	k2eAgInSc4d1	obdélníkový
půdorys	půdorys	k1gInSc4	půdorys
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
stran	stran	k7c2	stran
12	[number]	k4	12
x	x	k?	x
9	[number]	k4	9
metrů	metr	k1gInPc2	metr
se	s	k7c7	s
zkosenými	zkosený	k2eAgNnPc7d1	zkosené
nárožími	nároží	k1gNnPc7	nároží
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
jsou	být	k5eAaImIp3nP	být
členěny	členěn	k2eAgInPc1d1	členěn
pilastry	pilastr	k1gInPc1	pilastr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stavbě	stavba	k1gFnSc6	stavba
je	být	k5eAaImIp3nS	být
hladký	hladký	k2eAgInSc4d1	hladký
portál	portál	k1gInSc4	portál
s	s	k7c7	s
uchy	ucho	k1gNnPc7	ucho
a	a	k8xC	a
s	s	k7c7	s
letopočtem	letopočet	k1gInSc7	letopočet
1778	[number]	k4	1778
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
vchodem	vchod	k1gInSc7	vchod
s	s	k7c7	s
nízkým	nízký	k2eAgInSc7d1	nízký
štítem	štít	k1gInSc7	štít
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
zvonička	zvonička	k1gFnSc1	zvonička
s	s	k7c7	s
kopulovitou	kopulovitý	k2eAgFnSc7d1	kopulovitá
střechou	střecha	k1gFnSc7	střecha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Kaple	kaple	k1gFnSc1	kaple
svaté	svatý	k2eAgFnSc2d1	svatá
Anny	Anna	k1gFnSc2	Anna
</s>
</p>
<p>
<s>
Jáchymov	Jáchymov	k1gInSc1	Jáchymov
-	-	kIx~	-
kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Anny	Anna	k1gFnSc2	Anna
</s>
</p>
