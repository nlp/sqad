<s>
Karolina	Karolinum	k1gNnPc1	Karolinum
Světlá	světlý	k2eAgNnPc1d1	světlé
(	(	kIx(	(
<g/>
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Johana	Johan	k1gMnSc2	Johan
Nepomucena	Nepomucen	k2eAgFnSc1d1	Nepomucena
Rottová	Rottová	k1gFnSc1	Rottová
<g/>
,	,	kIx,	,
provdaná	provdaný	k2eAgFnSc1d1	provdaná
Mužáková	Mužáková	k1gFnSc1	Mužáková
<g/>
;	;	kIx,	;
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1830	[number]	k4	1830
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
7	[number]	k4	7
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1899	[number]	k4	1899
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
česká	český	k2eAgFnSc1d1	Česká
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
<g/>
,	,	kIx,	,
představitelka	představitelka	k1gFnSc1	představitelka
generace	generace	k1gFnSc1	generace
májovců	májovec	k1gMnPc2	májovec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
zakladatelku	zakladatelka	k1gFnSc4	zakladatelka
českého	český	k2eAgInSc2d1	český
vesnického	vesnický	k2eAgInSc2d1	vesnický
románu	román	k1gInSc2	román
<g/>
.	.	kIx.	.
</s>
<s>
Karolina	Karolinum	k1gNnSc2	Karolinum
Světlá	světlat	k5eAaImIp3nS	světlat
pocházela	pocházet	k5eAaImAgFnS	pocházet
ze	z	k7c2	z
zámožné	zámožný	k2eAgFnSc2d1	zámožná
rodiny	rodina	k1gFnSc2	rodina
Rottovy	Rottův	k2eAgInPc1d1	Rottův
(	(	kIx(	(
<g/>
firma	firma	k1gFnSc1	firma
V.	V.	kA	V.
J.	J.	kA	J.
Rott	Rott	k1gMnSc1	Rott
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
..	..	k?	..
Jejím	její	k3xOp3gMnSc7	její
otcem	otec	k1gMnSc7	otec
byl	být	k5eAaImAgMnS	být
obchodník	obchodník	k1gMnSc1	obchodník
Eustach	Eustach	k1gMnSc1	Eustach
Rott	Rott	k1gMnSc1	Rott
(	(	kIx(	(
<g/>
1795	[number]	k4	1795
<g/>
-	-	kIx~	-
<g/>
1869	[number]	k4	1869
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
Anna	Anna	k1gFnSc1	Anna
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Vogelová	Vogelová	k1gFnSc1	Vogelová
(	(	kIx(	(
<g/>
1811	[number]	k4	1811
<g/>
-	-	kIx~	-
<g/>
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
sestru	sestra	k1gFnSc4	sestra
Sofii	Sofia	k1gFnSc4	Sofia
(	(	kIx(	(
<g/>
provdaná	provdaný	k2eAgFnSc1d1	provdaná
Sofie	Sofie	k1gFnSc1	Sofie
Podlipská	Podlipský	k2eAgFnSc1d1	Podlipská
<g/>
,	,	kIx,	,
1833	[number]	k4	1833
<g/>
-	-	kIx~	-
<g/>
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
a	a	k8xC	a
bratra	bratr	k1gMnSc4	bratr
Jindřicha	Jindřich	k1gMnSc4	Jindřich
(	(	kIx(	(
<g/>
*	*	kIx~	*
1837	[number]	k4	1837
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
dostalo	dostat	k5eAaPmAgNnS	dostat
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
;	;	kIx,	;
kromě	kromě	k7c2	kromě
němčiny	němčina	k1gFnSc2	němčina
a	a	k8xC	a
češtiny	čeština	k1gFnSc2	čeština
ovládala	ovládat	k5eAaImAgFnS	ovládat
také	také	k9	také
francouzštinu	francouzština	k1gFnSc4	francouzština
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc4	její
dílo	dílo	k1gNnSc4	dílo
a	a	k8xC	a
život	život	k1gInSc4	život
velmi	velmi	k6eAd1	velmi
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
přátelství	přátelství	k1gNnSc1	přátelství
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Nerudou	Neruda	k1gMnSc7	Neruda
(	(	kIx(	(
<g/>
se	se	k3xPyFc4	se
kterým	který	k3yRgMnPc3	který
měla	mít	k5eAaImAgFnS	mít
platonický	platonický	k2eAgInSc4d1	platonický
vztah	vztah	k1gInSc4	vztah
<g/>
)	)	kIx)	)
a	a	k8xC	a
s	s	k7c7	s
Boženou	Božena	k1gFnSc7	Božena
Němcovou	Němcová	k1gFnSc7	Němcová
<g/>
,	,	kIx,	,
ze	z	k7c2	z
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
literátů	literát	k1gMnPc2	literát
její	její	k3xOp3gFnSc4	její
tvorbu	tvorba	k1gFnSc4	tvorba
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
francouzská	francouzský	k2eAgFnSc1d1	francouzská
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
George	Georg	k1gFnSc2	Georg
Sand	Sanda	k1gFnPc2	Sanda
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
však	však	k9	však
její	její	k3xOp3gNnSc4	její
dílo	dílo	k1gNnSc4	dílo
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
i	i	k9	i
smrt	smrt	k1gFnSc1	smrt
její	její	k3xOp3gFnSc2	její
jediné	jediný	k2eAgFnSc2d1	jediná
dcery	dcera	k1gFnSc2	dcera
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
u	u	k7c2	u
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
zemřel	zemřít	k5eAaPmAgMnS	zemřít
syn	syn	k1gMnSc1	syn
Hynek	Hynek	k1gMnSc1	Hynek
roku	rok	k1gInSc2	rok
1853	[number]	k4	1853
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1852	[number]	k4	1852
se	se	k3xPyFc4	se
vdala	vdát	k5eAaPmAgFnS	vdát
za	za	k7c4	za
svého	svůj	k3xOyFgMnSc4	svůj
učitele	učitel	k1gMnSc4	učitel
hry	hra	k1gFnSc2	hra
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
Petra	Petr	k1gMnSc2	Petr
Mužáka	Mužák	k1gMnSc2	Mužák
(	(	kIx(	(
<g/>
1821	[number]	k4	1821
<g/>
-	-	kIx~	-
<g/>
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ji	on	k3xPp3gFnSc4	on
také	také	k6eAd1	také
uvedl	uvést	k5eAaPmAgMnS	uvést
do	do	k7c2	do
kruhů	kruh	k1gInPc2	kruh
české	český	k2eAgFnSc2d1	Česká
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
sblížila	sblížit	k5eAaPmAgFnS	sblížit
s	s	k7c7	s
Boženou	Božena	k1gFnSc7	Božena
Němcovou	Němcův	k2eAgFnSc7d1	Němcova
<g/>
.	.	kIx.	.
</s>
<s>
Literárně	literárně	k6eAd1	literárně
začala	začít	k5eAaPmAgFnS	začít
tvořit	tvořit	k5eAaImF	tvořit
koncem	koncem	k7c2	koncem
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
překonávala	překonávat	k5eAaImAgFnS	překonávat
krizi	krize	k1gFnSc3	krize
způsobenou	způsobený	k2eAgFnSc7d1	způsobená
smrtí	smrt	k1gFnSc7	smrt
svého	své	k1gNnSc2	své
jediného	jediný	k2eAgNnSc2d1	jediné
dítěte	dítě	k1gNnSc2	dítě
<g/>
,	,	kIx,	,
dcery	dcera	k1gFnSc2	dcera
Boženky	Boženka	k1gFnSc2	Boženka
(	(	kIx(	(
<g/>
1853	[number]	k4	1853
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Manželovo	manželův	k2eAgNnSc1d1	manželovo
rodiště	rodiště	k1gNnSc1	rodiště
Světlá	světlat	k5eAaImIp3nS	světlat
pod	pod	k7c7	pod
Ještědem	Ještěd	k1gInSc7	Ještěd
bylo	být	k5eAaImAgNnS	být
inspirací	inspirace	k1gFnSc7	inspirace
pro	pro	k7c4	pro
její	její	k3xOp3gInSc4	její
pseudonym	pseudonym	k1gInSc4	pseudonym
a	a	k8xC	a
život	život	k1gInSc4	život
v	v	k7c6	v
Podještědí	Podještědí	k1gNnSc6	Podještědí
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
jezdila	jezdit	k5eAaImAgFnS	jezdit
na	na	k7c4	na
léto	léto	k1gNnSc4	léto
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
její	její	k3xOp3gFnSc4	její
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1878	[number]	k4	1878
trpěla	trpět	k5eAaImAgFnS	trpět
oční	oční	k2eAgFnSc7d1	oční
chorobou	choroba	k1gFnSc7	choroba
a	a	k8xC	a
musela	muset	k5eAaImAgFnS	muset
svá	svůj	k3xOyFgNnPc4	svůj
díla	dílo	k1gNnPc4	dílo
diktovat	diktovat	k5eAaImF	diktovat
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
sekretářkou	sekretářka	k1gFnSc7	sekretářka
a	a	k8xC	a
společnicí	společnice	k1gFnSc7	společnice
byla	být	k5eAaImAgFnS	být
její	její	k3xOp3gFnSc4	její
neteř	neteř	k1gFnSc1	neteř
Anežka	Anežka	k1gFnSc1	Anežka
Čermáková-Sluková	Čermáková-Slukový	k2eAgFnSc1d1	Čermáková-Sluková
<g/>
.	.	kIx.	.
</s>
<s>
Karolina	Karolinum	k1gNnSc2	Karolinum
Světlá	světlat	k5eAaImIp3nS	světlat
byla	být	k5eAaImAgFnS	být
členkou	členka	k1gFnSc7	členka
několika	několik	k4yIc2	několik
emancipačních	emancipační	k2eAgInPc2d1	emancipační
spolků	spolek	k1gInPc2	spolek
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
založila	založit	k5eAaPmAgFnS	založit
tzv.	tzv.	kA	tzv.
Ženský	ženský	k2eAgInSc1d1	ženský
výrobní	výrobní	k2eAgInSc1d1	výrobní
spolek	spolek	k1gInSc1	spolek
český	český	k2eAgInSc1d1	český
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
pak	pak	k6eAd1	pak
i	i	k9	i
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
řídila	řídit	k5eAaImAgFnS	řídit
<g/>
;	;	kIx,	;
cílem	cíl	k1gInSc7	cíl
spolku	spolek	k1gInSc2	spolek
byla	být	k5eAaImAgFnS	být
pomoc	pomoc	k1gFnSc4	pomoc
dívkám	dívka	k1gFnPc3	dívka
z	z	k7c2	z
chudých	chudý	k2eAgFnPc2d1	chudá
rodin	rodina	k1gFnPc2	rodina
<g/>
,	,	kIx,	,
vzděláním	vzdělání	k1gNnSc7	vzdělání
a	a	k8xC	a
prací	práce	k1gFnSc7	práce
<g/>
.	.	kIx.	.
</s>
<s>
Spoluzaložila	spoluzaložit	k5eAaPmAgFnS	spoluzaložit
Americký	americký	k2eAgInSc4d1	americký
klub	klub	k1gInSc4	klub
dam	dáma	k1gFnPc2	dáma
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
Vojtěchem	Vojtěch	k1gMnSc7	Vojtěch
Náprstkem	náprstek	k1gInSc7	náprstek
<g/>
.	.	kIx.	.
</s>
<s>
Působila	působit	k5eAaImAgFnS	působit
i	i	k9	i
jako	jako	k9	jako
novinářka	novinářka	k1gFnSc1	novinářka
<g/>
,	,	kIx,	,
jejím	její	k3xOp3gNnSc7	její
hlavním	hlavní	k2eAgNnSc7d1	hlavní
tématem	téma	k1gNnSc7	téma
bylo	být	k5eAaImAgNnS	být
postavení	postavení	k1gNnSc1	postavení
ženy	žena	k1gFnSc2	žena
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
manželem	manžel	k1gMnSc7	manžel
<g/>
,	,	kIx,	,
profesorem	profesor	k1gMnSc7	profesor
Petrem	Petr	k1gMnSc7	Petr
Mužákem	Mužák	k1gMnSc7	Mužák
bydlela	bydlet	k5eAaImAgFnS	bydlet
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Ve	v	k7c6	v
Smečkách	smečka	k1gFnPc6	smečka
a	a	k8xC	a
v	v	k7c6	v
domě	dům	k1gInSc6	dům
U	u	k7c2	u
Kamenného	kamenný	k2eAgInSc2d1	kamenný
stolu	stol	k1gInSc2	stol
na	na	k7c6	na
Karlově	Karlův	k2eAgNnSc6d1	Karlovo
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Světlá	světlý	k2eAgFnSc1d1	světlá
chodila	chodit	k5eAaImAgFnS	chodit
s	s	k7c7	s
hlavou	hlava	k1gFnSc7	hlava
plnou	plný	k2eAgFnSc7d1	plná
filozofických	filozofický	k2eAgMnPc2d1	filozofický
<g/>
,	,	kIx,	,
historických	historický	k2eAgInPc2d1	historický
<g/>
,	,	kIx,	,
psychologických	psychologický	k2eAgInPc2d1	psychologický
a	a	k8xC	a
přírodovědných	přírodovědný	k2eAgInPc2d1	přírodovědný
spisů	spis	k1gInPc2	spis
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
přesvědčena	přesvědčit	k5eAaPmNgFnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ženy	žena	k1gFnPc1	žena
svými	svůj	k3xOyFgFnPc7	svůj
vlohami	vloha	k1gFnPc7	vloha
a	a	k8xC	a
inteligencí	inteligence	k1gFnSc7	inteligence
jednou	jednou	k6eAd1	jednou
dokáží	dokázat	k5eAaPmIp3nP	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgFnPc1d1	schopná
studovat	studovat	k5eAaImF	studovat
a	a	k8xC	a
samostatně	samostatně	k6eAd1	samostatně
se	se	k3xPyFc4	se
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
rozhledem	rozhled	k1gInSc7	rozhled
působila	působit	k5eAaImAgFnS	působit
na	na	k7c4	na
nejbližší	blízký	k2eAgNnSc4d3	nejbližší
okolí	okolí	k1gNnSc4	okolí
jako	jako	k8xS	jako
geniální	geniální	k2eAgFnSc1d1	geniální
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
jaká	jaký	k3yRgFnSc1	jaký
se	se	k3xPyFc4	se
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
společnosti	společnost	k1gFnSc6	společnost
běžně	běžně	k6eAd1	běžně
nevyskytovala	vyskytovat	k5eNaImAgFnS	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
Boženou	Božena	k1gFnSc7	Božena
Němcovou	Němcův	k2eAgFnSc7d1	Němcova
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
dílech	díl	k1gInPc6	díl
narážela	narážet	k5eAaPmAgFnS	narážet
na	na	k7c4	na
sociální	sociální	k2eAgFnSc4d1	sociální
tematiku	tematika	k1gFnSc4	tematika
(	(	kIx(	(
<g/>
činila	činit	k5eAaImAgFnS	činit
tak	tak	k6eAd1	tak
poměrně	poměrně	k6eAd1	poměrně
často	často	k6eAd1	často
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
uvažovala	uvažovat	k5eAaImAgFnS	uvažovat
o	o	k7c6	o
služkách	služka	k1gFnPc6	služka
jako	jako	k8xC	jako
o	o	k7c6	o
členech	člen	k1gMnPc6	člen
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
psala	psát	k5eAaImAgFnS	psát
o	o	k7c6	o
pražském	pražský	k2eAgNnSc6d1	Pražské
měšťanském	měšťanský	k2eAgNnSc6d1	měšťanské
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgNnSc2	jenž
pocházela	pocházet	k5eAaImAgFnS	pocházet
<g/>
.	.	kIx.	.
</s>
<s>
K.	K.	kA	K.
Světlá	světlat	k5eAaImIp3nS	světlat
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
několik	několik	k4yIc4	několik
tzv.	tzv.	kA	tzv.
pražských	pražský	k2eAgFnPc2d1	Pražská
próz	próza	k1gFnPc2	próza
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebyly	být	k5eNaImAgFnP	být
zdaleka	zdaleka	k6eAd1	zdaleka
tak	tak	k6eAd1	tak
úspěšné	úspěšný	k2eAgFnPc1d1	úspěšná
jako	jako	k8xC	jako
její	její	k3xOp3gFnPc1	její
prózy	próza	k1gFnPc1	próza
venkovské	venkovský	k2eAgFnPc1d1	venkovská
<g/>
.	.	kIx.	.
</s>
<s>
Černý	Černý	k1gMnSc1	Černý
Petříček	Petříček	k1gMnSc1	Petříček
(	(	kIx(	(
<g/>
1871	[number]	k4	1871
<g/>
)	)	kIx)	)
-	-	kIx~	-
líčí	líčit	k5eAaImIp3nS	líčit
zde	zde	k6eAd1	zde
staropražský	staropražský	k2eAgInSc1d1	staropražský
život	život	k1gInSc1	život
obchodníka	obchodník	k1gMnSc2	obchodník
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
Upomínky	upomínka	k1gFnSc2	upomínka
(	(	kIx(	(
<g/>
1874	[number]	k4	1874
<g/>
)	)	kIx)	)
-	-	kIx~	-
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
životě	život	k1gInSc6	život
a	a	k8xC	a
o	o	k7c6	o
životě	život	k1gInSc6	život
typické	typický	k2eAgFnSc2d1	typická
pražské	pražský	k2eAgFnSc2d1	Pražská
rodiny	rodina	k1gFnSc2	rodina
30	[number]	k4	30
<g/>
.	.	kIx.	.
a	a	k8xC	a
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Zvonečková	zvonečkový	k2eAgFnSc1d1	Zvonečková
královna	královna	k1gFnSc1	královna
(	(	kIx(	(
<g/>
1872	[number]	k4	1872
<g/>
)	)	kIx)	)
-	-	kIx~	-
výrazně	výrazně	k6eAd1	výrazně
protikatolicky	protikatolicky	k6eAd1	protikatolicky
zaměřený	zaměřený	k2eAgInSc1d1	zaměřený
román	román	k1gInSc1	román
(	(	kIx(	(
<g/>
dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
)	)	kIx)	)
První	první	k4xOgFnSc1	první
Češka	Češka	k1gFnSc1	Češka
(	(	kIx(	(
<g/>
1861	[number]	k4	1861
<g/>
)	)	kIx)	)
-	-	kIx~	-
román	román	k1gInSc1	román
o	o	k7c6	o
těžkém	těžký	k2eAgNnSc6d1	těžké
prosazování	prosazování	k1gNnSc6	prosazování
českého	český	k2eAgNnSc2d1	české
vlastenectví	vlastenectví	k1gNnSc2	vlastenectví
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
poněmčelé	poněmčelý	k2eAgFnSc6d1	poněmčelý
společnosti	společnost	k1gFnSc6	společnost
(	(	kIx(	(
<g/>
částečně	částečně	k6eAd1	částečně
autobiografické	autobiografický	k2eAgNnSc1d1	autobiografické
dílo	dílo	k1gNnSc1	dílo
<g/>
)	)	kIx)	)
Její	její	k3xOp3gFnPc4	její
nejslavnější	slavný	k2eAgFnPc4d3	nejslavnější
prózy	próza	k1gFnPc4	próza
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
Podještědí	Podještědí	k1gNnSc2	Podještědí
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
30	[number]	k4	30
let	léto	k1gNnPc2	léto
jezdila	jezdit	k5eAaImAgFnS	jezdit
každé	každý	k3xTgNnSc4	každý
léto	léto	k1gNnSc4	léto
-	-	kIx~	-
tzv.	tzv.	kA	tzv.
ještědské	ještědský	k2eAgFnPc4d1	Ještědská
prózy	próza	k1gFnPc4	próza
<g/>
.	.	kIx.	.
</s>
<s>
Snažila	snažit	k5eAaImAgFnS	snažit
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
podat	podat	k5eAaPmF	podat
charakteristiku	charakteristika	k1gFnSc4	charakteristika
venkovského	venkovský	k2eAgInSc2d1	venkovský
lidu	lid	k1gInSc2	lid
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
řešit	řešit	k5eAaImF	řešit
určité	určitý	k2eAgFnPc4d1	určitá
morální	morální	k2eAgFnPc4d1	morální
otázky	otázka	k1gFnPc4	otázka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
považovala	považovat	k5eAaImAgFnS	považovat
za	za	k7c2	za
důležité	důležitý	k2eAgFnSc2d1	důležitá
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
pozornost	pozornost	k1gFnSc1	pozornost
věnovala	věnovat	k5eAaPmAgFnS	věnovat
vztahu	vztah	k1gInSc3	vztah
muže	muž	k1gMnSc2	muž
a	a	k8xC	a
ženy	žena	k1gFnPc1	žena
-	-	kIx~	-
jejími	její	k3xOp3gMnPc7	její
literárními	literární	k2eAgMnPc7d1	literární
hrdiny	hrdina	k1gMnPc7	hrdina
jsou	být	k5eAaImIp3nP	být
vždy	vždy	k6eAd1	vždy
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
mravně	mravně	k6eAd1	mravně
a	a	k8xC	a
morálně	morálně	k6eAd1	morálně
silné	silný	k2eAgFnPc1d1	silná
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
ženy	žena	k1gFnPc1	žena
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
obětovat	obětovat	k5eAaBmF	obětovat
svou	svůj	k3xOyFgFnSc4	svůj
lásku	láska	k1gFnSc4	láska
vyšším	vysoký	k2eAgInPc3d2	vyšší
ideálům	ideál	k1gInPc3	ideál
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc1	její
hrdinky	hrdinka	k1gFnPc1	hrdinka
většinou	většinou	k6eAd1	většinou
nejsou	být	k5eNaImIp3nP	být
spokojeny	spokojen	k2eAgFnPc1d1	spokojena
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
životem	život	k1gInSc7	život
a	a	k8xC	a
nenacházejí	nacházet	k5eNaImIp3nP	nacházet
štěstí	štěstí	k1gNnSc4	štěstí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejich	jejich	k3xOp3gFnPc6	jejich
postavách	postava	k1gFnPc6	postava
Světlá	světlý	k2eAgFnSc1d1	světlá
demonstruje	demonstrovat	k5eAaBmIp3nS	demonstrovat
svou	svůj	k3xOyFgFnSc4	svůj
základní	základní	k2eAgFnSc4d1	základní
ideu	idea	k1gFnSc4	idea
<g/>
:	:	kIx,	:
skutečné	skutečný	k2eAgNnSc4d1	skutečné
štěstí	štěstí	k1gNnSc4	štěstí
nelze	lze	k6eNd1	lze
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
porušením	porušení	k1gNnSc7	porušení
mravních	mravní	k2eAgInPc2d1	mravní
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Následujících	následující	k2eAgInPc2d1	následující
5	[number]	k4	5
románů	román	k1gInPc2	román
bývá	bývat	k5eAaImIp3nS	bývat
nazýváno	nazýván	k2eAgNnSc1d1	nazýváno
Ještědské	ještědský	k2eAgInPc1d1	ještědský
romány	román	k1gInPc1	román
<g/>
.	.	kIx.	.
</s>
<s>
Světlá	světlat	k5eAaImIp3nS	světlat
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
románech	román	k1gInPc6	román
staví	stavit	k5eAaImIp3nS	stavit
venkovskou	venkovský	k2eAgFnSc4d1	venkovská
společnost	společnost	k1gFnSc4	společnost
na	na	k7c4	na
vyšší	vysoký	k2eAgFnSc4d2	vyšší
mravní	mravní	k2eAgFnSc4d1	mravní
úroveň	úroveň	k1gFnSc4	úroveň
než	než	k8xS	než
pražské	pražský	k2eAgMnPc4d1	pražský
měšťáky	měšťák	k1gMnPc4	měšťák
<g/>
.	.	kIx.	.
</s>
<s>
Vesnický	vesnický	k2eAgInSc1d1	vesnický
román	román	k1gInSc1	román
(	(	kIx(	(
<g/>
1867	[number]	k4	1867
<g/>
)	)	kIx)	)
-	-	kIx~	-
tragédie	tragédie	k1gFnSc1	tragédie
manželství	manželství	k1gNnSc2	manželství
bez	bez	k7c2	bez
lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
</s>
<s>
Ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
tu	tu	k6eAd1	tu
příklad	příklad	k1gInSc4	příklad
nerovného	rovný	k2eNgNnSc2d1	nerovné
manželství	manželství	k1gNnSc2	manželství
<g/>
.	.	kIx.	.
</s>
<s>
Antoš	Antoš	k1gMnSc1	Antoš
Jirovec	Jirovec	k1gMnSc1	Jirovec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
statku	statek	k1gInSc6	statek
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
po	po	k7c6	po
rychtářově	rychtářův	k2eAgFnSc6d1	Rychtářova
smrti	smrt	k1gFnSc6	smrt
vezme	vzít	k5eAaPmIp3nS	vzít
rychtářku	rychtářka	k1gFnSc4	rychtářka
<g/>
.	.	kIx.	.
</s>
<s>
Rychtářka	rychtářka	k1gFnSc1	rychtářka
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
Antošovi	Antoš	k1gMnSc3	Antoš
líbit	líbit	k5eAaImF	líbit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ten	ten	k3xDgInSc1	ten
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
kvůli	kvůli	k7c3	kvůli
práci	práce	k1gFnSc3	práce
na	na	k7c6	na
statku	statek	k1gInSc6	statek
nemá	mít	k5eNaImIp3nS	mít
čas	čas	k1gInSc1	čas
<g/>
.	.	kIx.	.
</s>
<s>
Rychtářka	rychtářka	k1gFnSc1	rychtářka
pojme	pojmout	k5eAaPmIp3nS	pojmout
podezření	podezření	k1gNnSc4	podezření
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
jinou	jiný	k2eAgFnSc4d1	jiná
ženu	žena	k1gFnSc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Najme	najmout	k5eAaPmIp3nS	najmout
si	se	k3xPyFc3	se
služku	služka	k1gFnSc4	služka
Sylvu	Sylva	k1gFnSc4	Sylva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
s	s	k7c7	s
Antošem	Antoš	k1gMnSc7	Antoš
velmi	velmi	k6eAd1	velmi
sblíží	sblížit	k5eAaPmIp3nS	sblížit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
rychtářka	rychtářka	k1gFnSc1	rychtářka
odejde	odejít	k5eAaPmIp3nS	odejít
ze	z	k7c2	z
statku	statek	k1gInSc2	statek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
od	od	k7c2	od
svých	svůj	k3xOyFgFnPc2	svůj
dětí	dítě	k1gFnPc2	dítě
nenakazila	nakazit	k5eNaPmAgFnS	nakazit
neštovicemi	neštovice	k1gFnPc7	neštovice
<g/>
,	,	kIx,	,
Sylva	Sylva	k1gFnSc1	Sylva
se	se	k3xPyFc4	se
o	o	k7c4	o
ně	on	k3xPp3gFnPc4	on
i	i	k9	i
o	o	k7c4	o
Antoše	Antoš	k1gMnPc4	Antoš
stará	starý	k2eAgFnSc1d1	stará
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
rychtářka	rychtářka	k1gFnSc1	rychtářka
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
,	,	kIx,	,
Sylva	Sylva	k1gFnSc1	Sylva
si	se	k3xPyFc3	se
Antoše	Antoš	k1gMnPc4	Antoš
vzít	vzít	k5eAaPmF	vzít
nemůže	moct	k5eNaImIp3nS	moct
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
rychtářce	rychtářka	k1gFnSc3	rychtářka
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
slíbila	slíbit	k5eAaPmAgFnS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Antoš	Antoš	k1gMnSc1	Antoš
zůstane	zůstat	k5eAaPmIp3nS	zůstat
jejím	její	k3xOp3gMnPc3	její
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
Sylva	Sylva	k1gFnSc1	Sylva
ze	z	k7c2	z
statku	statek	k1gInSc2	statek
odchází	odcházet	k5eAaImIp3nS	odcházet
<g/>
.	.	kIx.	.
</s>
<s>
Kříž	Kříž	k1gMnSc1	Kříž
u	u	k7c2	u
potoka	potok	k1gInSc2	potok
(	(	kIx(	(
<g/>
1868	[number]	k4	1868
<g/>
)	)	kIx)	)
-	-	kIx~	-
hlavní	hlavní	k2eAgFnSc1d1	hlavní
hrdinka	hrdinka	k1gFnSc1	hrdinka
Eva	Eva	k1gFnSc1	Eva
bojuje	bojovat	k5eAaImIp3nS	bojovat
za	za	k7c4	za
rovnoprávnost	rovnoprávnost	k1gFnSc4	rovnoprávnost
<g/>
,	,	kIx,	,
končí	končit	k5eAaImIp3nS	končit
tragicky	tragicky	k6eAd1	tragicky
<g/>
.	.	kIx.	.
</s>
<s>
Obětuje	obětovat	k5eAaBmIp3nS	obětovat
svou	svůj	k3xOyFgFnSc4	svůj
lásku	láska	k1gFnSc4	láska
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zachránila	zachránit	k5eAaPmAgFnS	zachránit
manžela	manžel	k1gMnSc4	manžel
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
)	)	kIx)	)
Nemodlenec	nemodlenec	k1gMnSc1	nemodlenec
(	(	kIx(	(
<g/>
1873	[number]	k4	1873
<g/>
)	)	kIx)	)
-	-	kIx~	-
proti	proti	k7c3	proti
katolickému	katolický	k2eAgInSc3d1	katolický
náboženskému	náboženský	k2eAgInSc3d1	náboženský
fanatismu	fanatismus	k1gInSc3	fanatismus
<g/>
,	,	kIx,	,
relativita	relativita	k1gFnSc1	relativita
hodnot	hodnota	k1gFnPc2	hodnota
(	(	kIx(	(
<g/>
sama	sám	k3xTgFnSc1	sám
často	často	k6eAd1	často
hájila	hájit	k5eAaImAgFnS	hájit
a	a	k8xC	a
oslavovala	oslavovat	k5eAaImAgFnS	oslavovat
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
dílech	díl	k1gInPc6	díl
příslušníky	příslušník	k1gMnPc7	příslušník
jednoty	jednota	k1gFnSc2	jednota
bratrské	bratrský	k2eAgFnSc2d1	bratrská
<g/>
)	)	kIx)	)
Frantina	Frantina	k1gFnSc1	Frantina
(	(	kIx(	(
<g/>
1870	[number]	k4	1870
<g/>
)	)	kIx)	)
-	-	kIx~	-
Frantina	Frantina	k1gFnSc1	Frantina
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
rychtářkou	rychtářka	k1gFnSc7	rychtářka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vůdci	vůdce	k1gMnSc3	vůdce
loupežníků	loupežník	k1gMnPc2	loupežník
pozná	poznat	k5eAaPmIp3nS	poznat
svého	svůj	k3xOyFgMnSc2	svůj
vyvoleného	vyvolený	k1gMnSc2	vyvolený
a	a	k8xC	a
zabije	zabít	k5eAaPmIp3nS	zabít
ho	on	k3xPp3gNnSc4	on
sama	sám	k3xTgFnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
milého	milý	k2eAgMnSc4d1	milý
zavraždí	zavraždit	k5eAaPmIp3nP	zavraždit
<g/>
,	,	kIx,	,
vede	vést	k5eAaImIp3nS	vést
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
spor	spor	k1gInSc1	spor
(	(	kIx(	(
<g/>
rychtářka	rychtářka	k1gFnSc1	rychtářka
×	×	k?	×
její	její	k3xOp3gNnSc1	její
osobní	osobní	k2eAgNnSc1d1	osobní
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
<g/>
)	)	kIx)	)
nakonec	nakonec	k6eAd1	nakonec
přemůže	přemoct	k5eAaPmIp3nS	přemoct
svoje	svůj	k3xOyFgInPc4	svůj
city	cit	k1gInPc4	cit
a	a	k8xC	a
vůdce	vůdce	k1gMnSc1	vůdce
loupežníků	loupežník	k1gMnPc2	loupežník
zabije	zabít	k5eAaPmIp3nS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Kantůrčice	Kantůrčice	k?	Kantůrčice
(	(	kIx(	(
<g/>
1869	[number]	k4	1869
<g/>
)	)	kIx)	)
-	-	kIx~	-
problém	problém	k1gInSc1	problém
postavení	postavení	k1gNnSc2	postavení
ženy	žena	k1gFnSc2	žena
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
(	(	kIx(	(
<g/>
dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
)	)	kIx)	)
Hubička	hubička	k1gFnSc1	hubička
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
ještědské	ještědský	k2eAgFnPc1d1	Ještědská
povídky	povídka	k1gFnPc1	povídka
-	-	kIx~	-
povídku	povídka	k1gFnSc4	povídka
Hubička	hubička	k1gFnSc1	hubička
zpracovala	zpracovat	k5eAaPmAgFnS	zpracovat
Eliška	Eliška	k1gFnSc1	Eliška
Krásnohorská	krásnohorský	k2eAgFnSc1d1	Krásnohorská
jako	jako	k8xC	jako
libreto	libreto	k1gNnSc1	libreto
ke	k	k7c3	k
stejnojmenné	stejnojmenný	k2eAgFnSc3d1	stejnojmenná
opeře	opera	k1gFnSc3	opera
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Smetany	smetana	k1gFnSc2	smetana
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
chlapec	chlapec	k1gMnSc1	chlapec
Lukáš	Lukáš	k1gMnSc1	Lukáš
se	se	k3xPyFc4	se
ožení	oženit	k5eAaPmIp3nS	oženit
bez	bez	k7c2	bez
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
myslí	myslet	k5eAaImIp3nS	myslet
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
lásku	láska	k1gFnSc4	láska
Vendulku	Vendulka	k1gFnSc4	Vendulka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
své	svůj	k3xOyFgFnSc2	svůj
manželky	manželka	k1gFnSc2	manželka
se	se	k3xPyFc4	se
chce	chtít	k5eAaImIp3nS	chtít
oženit	oženit	k5eAaPmF	oženit
s	s	k7c7	s
Vendulkou	Vendulka	k1gFnSc7	Vendulka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ta	ten	k3xDgFnSc1	ten
odmítá	odmítat	k5eAaImIp3nS	odmítat
předsvatební	předsvatební	k2eAgInSc4d1	předsvatební
polibek	polibek	k1gInSc4	polibek
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
jeho	jeho	k3xOp3gFnSc4	jeho
ženu	žena	k1gFnSc4	žena
mrzelo	mrzet	k5eAaImAgNnS	mrzet
<g/>
.	.	kIx.	.
</s>
<s>
Lukáš	Lukáš	k1gMnSc1	Lukáš
jí	on	k3xPp3gFnSc7	on
začne	začít	k5eAaPmIp3nS	začít
dělat	dělat	k5eAaImF	dělat
naschvály	naschvál	k1gInPc4	naschvál
a	a	k8xC	a
ona	onen	k3xDgFnSc1	onen
odchází	odcházet	k5eAaImIp3nS	odcházet
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
tetě	teta	k1gFnSc3	teta
<g/>
,	,	kIx,	,
překupnici	překupnice	k1gFnSc6	překupnice
pašovaného	pašovaný	k2eAgNnSc2d1	pašované
zboží	zboží	k1gNnSc2	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Lukáš	Lukáš	k1gMnSc1	Lukáš
ji	on	k3xPp3gFnSc4	on
hledá	hledat	k5eAaImIp3nS	hledat
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
delším	dlouhý	k2eAgInSc6d2	delší
čase	čas	k1gInSc6	čas
ji	on	k3xPp3gFnSc4	on
najde	najít	k5eAaPmIp3nS	najít
<g/>
.	.	kIx.	.
</s>
<s>
Vendulka	Vendulka	k1gFnSc1	Vendulka
ho	on	k3xPp3gMnSc4	on
pak	pak	k6eAd1	pak
políbí	políbit	k5eAaPmIp3nP	políbit
a	a	k8xC	a
vezmou	vzít	k5eAaPmIp3nP	vzít
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
další	další	k2eAgFnPc1d1	další
povídky	povídka	k1gFnPc1	povídka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nedosáhly	dosáhnout	k5eNaPmAgFnP	dosáhnout
významu	význam	k1gInSc3	význam
Hubičky	hubička	k1gFnSc2	hubička
(	(	kIx(	(
<g/>
Přišla	přijít	k5eAaPmAgFnS	přijít
do	do	k7c2	do
rozumu	rozum	k1gInSc2	rozum
<g/>
,	,	kIx,	,
Selka	selka	k1gFnSc1	selka
<g/>
,	,	kIx,	,
Večer	večer	k6eAd1	večer
u	u	k7c2	u
koryta	koryto	k1gNnSc2	koryto
<g/>
,	,	kIx,	,
Námluvy	námluva	k1gFnSc2	námluva
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
Kromě	kromě	k7c2	kromě
těchto	tento	k3xDgFnPc2	tento
knih	kniha	k1gFnPc2	kniha
napsala	napsat	k5eAaBmAgFnS	napsat
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
povídek	povídka	k1gFnPc2	povídka
(	(	kIx(	(
<g/>
Společnice	společnice	k1gFnSc1	společnice
<g/>
,	,	kIx,	,
Skalák	Skalák	k1gMnSc1	Skalák
<g/>
,	,	kIx,	,
Cikánka	cikánka	k1gFnSc1	cikánka
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Lesní	lesní	k2eAgFnSc1d1	lesní
panna	panna	k1gFnSc1	panna
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
uveřejněných	uveřejněný	k2eAgInPc2d1	uveřejněný
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
časopisech	časopis	k1gInPc6	časopis
(	(	kIx(	(
<g/>
Světozor	světozor	k1gInSc1	světozor
<g/>
,	,	kIx,	,
Lumír	Lumír	k1gMnSc1	Lumír
<g/>
,	,	kIx,	,
Máj	máj	k1gFnSc1	máj
<g/>
,	,	kIx,	,
Kresby	kresba	k1gFnPc1	kresba
<g/>
,	,	kIx,	,
Květy	květ	k1gInPc1	květ
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
Zachovala	zachovat	k5eAaPmAgFnS	zachovat
se	se	k3xPyFc4	se
její	její	k3xOp3gFnSc7	její
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
korespondence	korespondence	k1gFnSc1	korespondence
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
udržovala	udržovat	k5eAaImAgFnS	udržovat
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
sestrou	sestra	k1gFnSc7	sestra
Sofií	Sofia	k1gFnSc7	Sofia
Podlipskou	Podlipský	k2eAgFnSc7d1	Podlipská
<g/>
,	,	kIx,	,
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
mladou	mladý	k2eAgFnSc7d1	mladá
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
Eliškou	Eliška	k1gFnSc7	Eliška
Krásnohorskou	krásnohorský	k2eAgFnSc7d1	Krásnohorská
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Nerudou	Neruda	k1gMnSc7	Neruda
<g/>
.	.	kIx.	.
</s>
<s>
Karolína	Karolína	k1gFnSc1	Karolína
Světlá	světlat	k5eAaImIp3nS	světlat
se	se	k3xPyFc4	se
přiměřeně	přiměřeně	k6eAd1	přiměřeně
svému	svůj	k3xOyFgNnSc3	svůj
společenskému	společenský	k2eAgNnSc3d1	společenské
postavení	postavení	k1gNnSc3	postavení
dávala	dávat	k5eAaImAgFnS	dávat
ráda	rád	k2eAgFnSc1d1	ráda
portrétovat	portrétovat	k5eAaImF	portrétovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
kresebně	kresebně	k6eAd1	kresebně
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
malbě	malba	k1gFnSc6	malba
a	a	k8xC	a
v	v	k7c6	v
pozdějším	pozdní	k2eAgInSc6d2	pozdější
věku	věk	k1gInSc6	věk
zejména	zejména	k9	zejména
fotograficky	fotograficky	k6eAd1	fotograficky
(	(	kIx(	(
<g/>
ve	v	k7c6	v
sbírkách	sbírka	k1gFnPc6	sbírka
Muzea	muzeum	k1gNnSc2	muzeum
hl.	hl.	k?	hl.
<g/>
m.	m.	k?	m.
<g/>
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
Národní	národní	k2eAgFnSc2d1	národní
galerie	galerie	k1gFnSc2	galerie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
známé	známý	k2eAgFnPc1d1	známá
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
dva	dva	k4xCgInPc4	dva
pomníky	pomník	k1gInPc4	pomník
<g/>
,	,	kIx,	,
vzniklé	vzniklý	k2eAgInPc4d1	vzniklý
teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
portrétní	portrétní	k2eAgFnSc1d1	portrétní
busta	busta	k1gFnSc1	busta
spoluzakladatelky	spoluzakladatelka	k1gFnSc2	spoluzakladatelka
v	v	k7c6	v
průčelí	průčelí	k1gNnSc6	průčelí
budovy	budova	k1gFnSc2	budova
Ženského	ženský	k2eAgInSc2d1	ženský
výrobního	výrobní	k2eAgInSc2d1	výrobní
spolku	spolek	k1gInSc2	spolek
v	v	k7c6	v
Resslově	Resslově	k1gFnSc6	Resslově
ulici	ulice	k1gFnSc6	ulice
č.	č.	k?	č.
5	[number]	k4	5
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
pomník	pomník	k1gInSc1	pomník
s	s	k7c7	s
bronzovou	bronzový	k2eAgFnSc7d1	bronzová
polofigurou	polofigura	k1gFnSc7	polofigura
Karolíny	Karolína	k1gFnSc2	Karolína
Světlé	světlý	k2eAgFnSc2d1	světlá
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
na	na	k7c6	na
Karlově	Karlův	k2eAgNnSc6d1	Karlovo
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
Gustav	Gustav	k1gMnSc1	Gustav
Zoula	Zoul	k1gMnSc4	Zoul
pomník	pomník	k1gInSc4	pomník
-	-	kIx~	-
socha	socha	k1gFnSc1	socha
sedící	sedící	k2eAgFnSc2d1	sedící
spisovatelky	spisovatelka	k1gFnSc2	spisovatelka
ve	v	k7c6	v
Světlé	světlý	k2eAgFnSc6d1	světlá
pod	pod	k7c7	pod
Ještědem	Ještěd	k1gInSc7	Ještěd
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
Josef	Josef	k1gMnSc1	Josef
Bílek	Bílek	k1gMnSc1	Bílek
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
</s>
