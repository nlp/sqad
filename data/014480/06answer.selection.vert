<s>
Karákóramská	Karákóramský	k2eAgFnSc1d1
dálnice	dálnice	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Karakoram	Karakoram	k1gInSc1
Highway	Highwaa	k1gFnSc2
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
KKH	KKH	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
dálková	dálkový	k2eAgFnSc1d1
vysokohorská	vysokohorský	k2eAgFnSc1d1
silnice	silnice	k1gFnSc1
vedoucí	vedoucí	k1gFnSc1
z	z	k7c2
Kašgaru	Kašgar	k1gInSc2
v	v	k7c6
Číně	Čína	k1gFnSc6
do	do	k7c2
Abbottábádu	Abbottábád	k1gInSc2
v	v	k7c6
Pákistánu	Pákistán	k1gInSc6
<g/>
.	.	kIx.
</s>