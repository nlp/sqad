<s>
Karákóramská	Karákóramský	k2eAgFnSc1d1
dálnice	dálnice	k1gFnSc1
</s>
<s>
Mapa	mapa	k1gFnSc1
</s>
<s>
Dálnice	dálnice	k1gFnSc1
nedaleko	nedaleko	k7c2
kulminačního	kulminační	k2eAgInSc2d1
bodu	bod	k1gInSc2
</s>
<s>
Karákóramská	Karákóramský	k2eAgFnSc1d1
dálnice	dálnice	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Karakoram	Karakoram	k1gInSc1
Highway	Highwaa	k1gFnSc2
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
KKH	KKH	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
dálková	dálkový	k2eAgFnSc1d1
vysokohorská	vysokohorský	k2eAgFnSc1d1
silnice	silnice	k1gFnSc1
vedoucí	vedoucí	k1gFnSc1
z	z	k7c2
Kašgaru	Kašgar	k1gInSc2
v	v	k7c6
Číně	Čína	k1gFnSc6
do	do	k7c2
Abbottábádu	Abbottábád	k1gInSc2
v	v	k7c6
Pákistánu	Pákistán	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
dlouhá	dlouhý	k2eAgFnSc1d1
1300	#num#	k4
km	km	kA
(	(	kIx(
<g/>
někdy	někdy	k6eAd1
se	se	k3xPyFc4
k	k	k7c3
ní	on	k3xPp3gFnSc3
počítá	počítat	k5eAaImIp3nS
také	také	k9
70	#num#	k4
km	km	kA
dlouhý	dlouhý	k2eAgInSc4d1
úsek	úsek	k1gInSc4
z	z	k7c2
Abbottábádu	Abbottábád	k1gInSc2
na	na	k7c4
jih	jih	k1gInSc4
do	do	k7c2
Hasan	Hasana	k1gFnPc2
Abdalu	Abdal	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
napojuje	napojovat	k5eAaImIp3nS
na	na	k7c4
páteřní	páteřní	k2eAgFnSc4d1
celoindickou	celoindický	k2eAgFnSc4d1
Grand	grand	k1gMnSc1
Trunk	Trunk	k?
Road	Road	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průsmyku	průsmyk	k1gInSc6
Chundžeráb	Chundžeráb	k1gInSc1
silnice	silnice	k1gFnSc2
dosahuje	dosahovat	k5eAaImIp3nS
nadmořské	nadmořský	k2eAgFnSc2d1
výšky	výška	k1gFnSc2
4693	#num#	k4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
z	z	k7c2
ní	on	k3xPp3gFnSc2
činí	činit	k5eAaImIp3nP
nejvýše	nejvýše	k6eAd1,k6eAd3
položenou	položený	k2eAgFnSc4d1
mezinárodní	mezinárodní	k2eAgFnSc4d1
silnici	silnice	k1gFnSc4
s	s	k7c7
pevným	pevný	k2eAgInSc7d1
povrchem	povrch	k1gInSc7
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Oficiálně	oficiálně	k6eAd1
je	být	k5eAaImIp3nS
označována	označovat	k5eAaImNgFnS
v	v	k7c6
Číně	Čína	k1gFnSc6
jako	jako	k8xC,k8xS
G314	G314	k1gFnSc6
a	a	k8xC
v	v	k7c6
Pákistánu	Pákistán	k1gInSc6
jako	jako	k8xC,k8xS
N-	N-	k1gFnSc6
<g/>
35	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Stavba	stavba	k1gFnSc1
silnice	silnice	k1gFnSc2
ve	v	k7c6
strategické	strategický	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
Kašmíru	Kašmír	k1gInSc2
byla	být	k5eAaImAgFnS
zahájena	zahájit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1959	#num#	k4
jako	jako	k8xS,k8xC
výraz	výraz	k1gInSc1
čínsko-pákistánského	čínsko-pákistánský	k2eAgNnSc2d1
spojenectví	spojenectví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stovky	stovka	k1gFnPc1
čínských	čínský	k2eAgMnPc2d1
dělníků	dělník	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
při	při	k7c6
stavbě	stavba	k1gFnSc6
zahynuli	zahynout	k5eAaPmAgMnP
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
pohřbeny	pohřbít	k5eAaPmNgFnP
na	na	k7c6
hřbitově	hřbitov	k1gInSc6
China	China	k1gFnSc1
Yagdar	Yagdar	k1gInSc1
ve	v	k7c6
městě	město	k1gNnSc6
Gilgit	Gilgita	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Silnice	silnice	k1gFnSc1
byla	být	k5eAaImAgFnS
dokončena	dokončit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1979	#num#	k4
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
1986	#num#	k4
je	být	k5eAaImIp3nS
otevřena	otevřít	k5eAaPmNgFnS
pro	pro	k7c4
civilní	civilní	k2eAgFnPc4d1
vozidla	vozidlo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
června	červen	k1gInSc2
2006	#num#	k4
funguje	fungovat	k5eAaImIp3nS
mezi	mezi	k7c7
Abbottábádem	Abbottábád	k1gInSc7
a	a	k8xC
Kašgarem	Kašgar	k1gInSc7
pravidelná	pravidelný	k2eAgFnSc1d1
autobusová	autobusový	k2eAgFnSc1d1
linka	linka	k1gFnSc1
<g/>
,	,	kIx,
provozovaná	provozovaný	k2eAgFnSc1d1
firmou	firma	k1gFnSc7
Northern	Northern	k1gMnSc1
Areas	Areas	k1gMnSc1
Transport	transporta	k1gFnPc2
Corporation	Corporation	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
začaly	začít	k5eAaPmAgFnP
práce	práce	k1gFnPc1
na	na	k7c4
rozšíření	rozšíření	k1gNnSc4
a	a	k8xC
zpevnění	zpevnění	k1gNnSc4
silnice	silnice	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
umožní	umožnit	k5eAaPmIp3nS
čínským	čínský	k2eAgInPc3d1
kamionům	kamion	k1gInPc3
se	s	k7c7
zbožím	zboží	k1gNnSc7
přístup	přístup	k1gInSc1
k	k	k7c3
Arabskému	arabský	k2eAgNnSc3d1
moři	moře	k1gNnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Zatím	zatím	k6eAd1
jsou	být	k5eAaImIp3nP
v	v	k7c6
zimním	zimní	k2eAgNnSc6d1
období	období	k1gNnSc6
vysokohorské	vysokohorský	k2eAgInPc4d1
úseky	úsek	k1gInPc4
kvůli	kvůli	k7c3
závějím	závěj	k1gFnPc3
a	a	k8xC
lavinám	lavina	k1gFnPc3
nesjízdné	sjízdný	k2eNgInPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dálnice	dálnice	k1gFnSc1
vede	vést	k5eAaImIp3nS
přes	přes	k7c4
čtyři	čtyři	k4xCgNnPc4
nejvyšší	vysoký	k2eAgNnPc4d3
světová	světový	k2eAgNnPc4d1
pohoří	pohoří	k1gNnPc4
(	(	kIx(
<g/>
Karákóram	Karákóram	k1gInSc1
<g/>
,	,	kIx,
Hindúkuš	Hindúkuš	k1gInSc1
<g/>
,	,	kIx,
Himálaj	Himálaj	k1gFnSc1
a	a	k8xC
Pamír	Pamír	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
překonává	překonávat	k5eAaImIp3nS
údolí	údolí	k1gNnSc2
řek	řeka	k1gFnPc2
Indus	Indus	k1gInSc1
a	a	k8xC
Hunza	Hunza	k1gFnSc1
River	Rivra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
www.inautonews.com	www.inautonews.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www.pakpost.gov.pk	www.pakpost.gov.pk	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://zpravy.e15.cz/zahranicni/udalosti/cina-stavi-pro-pakistan-nejvyse-polozenou-dalnici-na-svete-834581	http://zpravy.e15.cz/zahranicni/udalosti/cina-stavi-pro-pakistan-nejvyse-polozenou-dalnici-na-svete-834581	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Karákóramská	Karákóramský	k2eAgFnSc1d1
dálnice	dálnice	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
https://web.archive.org/web/20130613165212/http://www.historyofkkh.info/	https://web.archive.org/web/20130613165212/http://www.historyofkkh.info/	k4
</s>
<s>
http://pakistanpaedia.com/landmarks/kkh/kkh.htm	http://pakistanpaedia.com/landmarks/kkh/kkh.htm	k6eAd1
</s>
<s>
http://www.czech-press.cz/index.php?option=com_content&	http://www.czech-press.cz/index.php?option=com_content&	k?
</s>
<s>
http://www.ceskatelevize.cz/porady/1096911352-objektiv/208411030400713/	http://www.ceskatelevize.cz/porady/1096911352-objektiv/208411030400713/	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Doprava	doprava	k1gFnSc1
|	|	kIx~
Čína	Čína	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
87007210	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
315144452	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
87007210	#num#	k4
</s>
