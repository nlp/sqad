<s>
Led	led	k1gInSc4	led
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
byla	být	k5eAaImAgFnS	být
britská	britský	k2eAgFnSc1d1	britská
rocková	rockový	k2eAgFnSc1d1	rocková
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
<g/>
,	,	kIx,	,
nejúspěšnějších	úspěšný	k2eAgFnPc2d3	nejúspěšnější
a	a	k8xC	a
nejvlivnějších	vlivný	k2eAgFnPc2d3	nejvlivnější
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
rockové	rockový	k2eAgFnSc2d1	rocková
hudby	hudba	k1gFnSc2	hudba
(	(	kIx(	(
<g/>
dala	dát	k5eAaPmAgFnS	dát
prakticky	prakticky	k6eAd1	prakticky
vzniknout	vzniknout	k5eAaPmF	vzniknout
heavy	heav	k1gInPc4	heav
metalu	metal	k1gInSc2	metal
a	a	k8xC	a
dalším	další	k2eAgNnSc7d1	další
odnožím	odnoží	k1gNnSc7	odnoží
rockové	rockový	k2eAgFnSc2d1	rocková
hudby	hudba	k1gFnSc2	hudba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
bubeníka	bubeník	k1gMnSc2	bubeník
Johna	John	k1gMnSc2	John
Bonhama	Bonham	k1gMnSc2	Bonham
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
ostatních	ostatní	k2eAgMnPc2d1	ostatní
členů	člen	k1gMnPc2	člen
nenahraditelný	nahraditelný	k2eNgInSc1d1	nenahraditelný
<g/>
,	,	kIx,	,
ukončila	ukončit	k5eAaPmAgFnS	ukončit
skupina	skupina	k1gFnSc1	skupina
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
svoji	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
zbývající	zbývající	k2eAgMnPc1d1	zbývající
hudebníci	hudebník	k1gMnPc1	hudebník
uspořádali	uspořádat	k5eAaPmAgMnP	uspořádat
jediný	jediný	k2eAgInSc4d1	jediný
koncert	koncert	k1gInSc4	koncert
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
jejich	jejich	k3xOp3gMnSc7	jejich
posledním	poslední	k2eAgMnSc7d1	poslední
<g/>
.	.	kIx.	.
</s>
<s>
Jimmy	Jimm	k1gInPc1	Jimm
Page	Pag	k1gFnSc2	Pag
z	z	k7c2	z
Felthamu	Feltham	k1gInSc2	Feltham
v	v	k7c6	v
Middlesexu	Middlesex	k1gInSc6	Middlesex
začal	začít	k5eAaPmAgInS	začít
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
blues	blues	k1gNnSc2	blues
<g/>
,	,	kIx,	,
Chucka	Chucka	k1gFnSc1	Chucka
Berryho	Berry	k1gMnSc2	Berry
a	a	k8xC	a
Elvise	Elvis	k1gMnSc2	Elvis
Presleyho	Presley	k1gMnSc2	Presley
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
působení	působení	k1gNnSc6	působení
v	v	k7c6	v
amatérských	amatérský	k2eAgFnPc6d1	amatérská
skupinách	skupina	k1gFnPc6	skupina
přijímal	přijímat	k5eAaImAgInS	přijímat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
studií	studio	k1gNnPc2	studio
na	na	k7c6	na
umělecké	umělecký	k2eAgFnSc6d1	umělecká
škole	škola	k1gFnSc6	škola
studiovou	studiový	k2eAgFnSc4d1	studiová
práci	práce	k1gFnSc4	práce
na	na	k7c6	na
nahrávkách	nahrávka	k1gFnPc6	nahrávka
The	The	k1gFnSc2	The
Kinks	Kinks	k1gInSc1	Kinks
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Who	Who	k1gFnSc1	Who
<g/>
,	,	kIx,	,
Van	van	k1gInSc1	van
Morrisona	Morrisona	k1gFnSc1	Morrisona
<g/>
,	,	kIx,	,
Joe	Joe	k1gFnSc1	Joe
Cockera	Cockero	k1gNnSc2	Cockero
aj.	aj.	kA	aj.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
jako	jako	k9	jako
baskytarista	baskytarista	k1gMnSc1	baskytarista
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gFnSc2	The
Yardbirds	Yardbirdsa	k1gFnPc2	Yardbirdsa
<g/>
,	,	kIx,	,
po	po	k7c6	po
jejíž	jejíž	k3xOyRp3gFnSc6	jejíž
reorganizaci	reorganizace	k1gFnSc6	reorganizace
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
společně	společně	k6eAd1	společně
s	s	k7c7	s
Jeffem	Jeff	k1gMnSc7	Jeff
Beckem	Becek	k1gMnSc7	Becek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Beckově	Beckův	k2eAgInSc6d1	Beckův
odchodu	odchod	k1gInSc6	odchod
měl	mít	k5eAaImAgMnS	mít
možnost	možnost	k1gFnSc4	možnost
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
manažerem	manažer	k1gMnSc7	manažer
Peterem	Peter	k1gMnSc7	Peter
Grantem	grant	k1gInSc7	grant
vést	vést	k5eAaImF	vést
"	"	kIx"	"
<g/>
Nové	Nové	k2eAgInSc4d1	Nové
Yardbirds	Yardbirds	k1gInSc4	Yardbirds
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
na	na	k7c4	na
radu	rada	k1gFnSc4	rada
Keitha	Keith	k1gMnSc2	Keith
Moona	Moon	k1gMnSc2	Moon
se	se	k3xPyFc4	se
však	však	k9	však
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
pro	pro	k7c4	pro
název	název	k1gInSc4	název
Led	led	k1gInSc1	led
Zeppelin	Zeppelin	k2eAgInSc1d1	Zeppelin
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
londýnským	londýnský	k2eAgMnSc7d1	londýnský
multi-instrumentalistou	multinstrumentalista	k1gMnSc7	multi-instrumentalista
John	John	k1gMnSc1	John
Paul	Paul	k1gMnSc1	Paul
Jonesem	Jones	k1gMnSc7	Jones
(	(	kIx(	(
<g/>
v	v	k7c6	v
L.	L.	kA	L.
Z.	Z.	kA	Z.
hrál	hrát	k5eAaImAgInS	hrát
na	na	k7c4	na
mandolínu	mandolína	k1gFnSc4	mandolína
<g/>
,	,	kIx,	,
basovou	basový	k2eAgFnSc4d1	basová
kytaru	kytara	k1gFnSc4	kytara
a	a	k8xC	a
na	na	k7c4	na
klávesy	klávesa	k1gFnPc4	klávesa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
doposud	doposud	k6eAd1	doposud
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
Donovanem	Donovan	k1gMnSc7	Donovan
a	a	k8xC	a
aranžoval	aranžovat	k5eAaImAgMnS	aranžovat
jeho	jeho	k3xOp3gInPc4	jeho
snímky	snímek	k1gInPc4	snímek
Sunshine	Sunshin	k1gInSc5	Sunshin
Superman	superman	k1gMnSc1	superman
A	A	kA	A
Mellow	Mellow	k1gMnSc1	Mellow
Yellow	Yellow	k1gMnSc1	Yellow
(	(	kIx(	(
<g/>
na	na	k7c4	na
nástroj	nástroj	k1gInSc4	nástroj
hraje	hrát	k5eAaImIp3nS	hrát
od	od	k7c2	od
r.	r.	kA	r.
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
k	k	k7c3	k
hudbě	hudba	k1gFnSc3	hudba
ho	on	k3xPp3gInSc4	on
přivedli	přivést	k5eAaPmAgMnP	přivést
rodiče	rodič	k1gMnPc1	rodič
<g/>
,	,	kIx,	,
varietní	varietní	k2eAgMnPc1d1	varietní
umělci	umělec	k1gMnPc1	umělec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
usilovali	usilovat	k5eAaImAgMnP	usilovat
zprvu	zprvu	k6eAd1	zprvu
o	o	k7c4	o
zpěváka	zpěvák	k1gMnSc4	zpěvák
Terryho	Terry	k1gMnSc4	Terry
Reida	Reid	k1gMnSc4	Reid
<g/>
,	,	kIx,	,
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
odmítnutí	odmítnutý	k2eAgMnPc1d1	odmítnutý
vyzvali	vyzvat	k5eAaPmAgMnP	vyzvat
Roberta	Robert	k1gMnSc4	Robert
Planta	planta	k1gFnSc1	planta
z	z	k7c2	z
Birminghamu	Birmingham	k1gInSc2	Birmingham
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
Robert	Robert	k1gMnSc1	Robert
Plant	planta	k1gFnPc2	planta
se	se	k3xPyFc4	se
od	od	k7c2	od
svých	svůj	k3xOyFgNnPc2	svůj
15	[number]	k4	15
let	léto	k1gNnPc2	léto
zajímal	zajímat	k5eAaImAgMnS	zajímat
o	o	k7c4	o
bluesové	bluesový	k2eAgMnPc4d1	bluesový
umělce	umělec	k1gMnPc4	umělec
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
ho	on	k3xPp3gNnSc4	on
zejména	zejména	k9	zejména
Robert	Robert	k1gMnSc1	Robert
Johnson	Johnson	k1gMnSc1	Johnson
silně	silně	k6eAd1	silně
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
vokálním	vokální	k2eAgInSc7d1	vokální
stylem	styl	k1gInSc7	styl
napodobujícím	napodobující	k2eAgInSc7d1	napodobující
zvuk	zvuk	k1gInSc4	zvuk
elektrické	elektrický	k2eAgFnSc2d1	elektrická
kytary	kytara	k1gFnSc2	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Tohoto	tento	k3xDgInSc2	tento
způsobu	způsob	k1gInSc2	způsob
použil	použít	k5eAaPmAgMnS	použít
Plant	planta	k1gFnPc2	planta
hned	hned	k6eAd1	hned
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
LP	LP	kA	LP
(	(	kIx(	(
<g/>
Led	led	k1gInSc1	led
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
<g/>
)	)	kIx)	)
ve	v	k7c6	v
snímku	snímek	k1gInSc6	snímek
"	"	kIx"	"
<g/>
You	You	k1gFnSc1	You
Shook	Shook	k1gInSc1	Shook
Me	Me	k1gFnSc1	Me
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
snímky	snímek	k1gInPc4	snímek
nazpíval	nazpívat	k5eAaPmAgMnS	nazpívat
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Listen	listen	k1gInSc1	listen
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
sešel	sejít	k5eAaPmAgMnS	sejít
s	s	k7c7	s
Johnem	John	k1gMnSc7	John
Bonhamem	Bonham	k1gInSc7	Bonham
ke	k	k7c3	k
zpěvákovi	zpěvák	k1gMnSc3	zpěvák
T.	T.	kA	T.
Rosemu	Rose	k1gMnSc3	Rose
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
si	se	k3xPyFc3	se
navzájem	navzájem	k6eAd1	navzájem
doporučili	doporučit	k5eAaPmAgMnP	doporučit
pro	pro	k7c4	pro
Led	led	k1gInSc4	led
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
Jimmyho	Jimmy	k1gMnSc4	Jimmy
Page	Pag	k1gMnSc4	Pag
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
měl	mít	k5eAaImAgInS	mít
premiéru	premiéra	k1gFnSc4	premiéra
v	v	k7c6	v
londýnském	londýnský	k2eAgInSc6d1	londýnský
klubu	klub	k1gInSc6	klub
Middle	Middle	k1gMnSc1	Middle
Earth	Earth	k1gMnSc1	Earth
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
vydal	vydat	k5eAaPmAgMnS	vydat
spontánně	spontánně	k6eAd1	spontánně
<g/>
,	,	kIx,	,
za	za	k7c4	za
pouhých	pouhý	k2eAgFnPc2d1	pouhá
30	[number]	k4	30
hodin	hodina	k1gFnPc2	hodina
natočené	natočený	k2eAgNnSc4d1	natočené
album	album	k1gNnSc4	album
(	(	kIx(	(
<g/>
Led	led	k1gInSc1	led
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
dodatečně	dodatečně	k6eAd1	dodatečně
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
i	i	k9	i
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Led	led	k1gInSc1	led
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
sešli	sejít	k5eAaPmAgMnP	sejít
na	na	k7c6	na
první	první	k4xOgFnSc6	první
zkoušce	zkouška	k1gFnSc6	zkouška
<g/>
,	,	kIx,	,
nemohli	moct	k5eNaImAgMnP	moct
ani	ani	k9	ani
uvěřit	uvěřit	k5eAaPmF	uvěřit
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
spolu	spolu	k6eAd1	spolu
hraje	hrát	k5eAaImIp3nS	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
šlapala	šlapat	k5eAaImAgFnS	šlapat
jako	jako	k9	jako
švýcarské	švýcarský	k2eAgFnPc4d1	švýcarská
hodinky	hodinka	k1gFnPc4	hodinka
<g/>
.	.	kIx.	.
</s>
<s>
Náhodou	náhodou	k6eAd1	náhodou
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
dokonalá	dokonalý	k2eAgFnSc1d1	dokonalá
alchymie	alchymie	k1gFnSc1	alchymie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
sešly	sejít	k5eAaPmAgInP	sejít
čtyři	čtyři	k4xCgInPc1	čtyři
mimořádné	mimořádný	k2eAgInPc1d1	mimořádný
talenty	talent	k1gInPc1	talent
<g/>
.	.	kIx.	.
</s>
<s>
Hlas	hlas	k1gInSc1	hlas
Roberta	Robert	k1gMnSc2	Robert
Planta	planta	k1gFnSc1	planta
s	s	k7c7	s
krásným	krásný	k2eAgInSc7d1	krásný
projevem	projev	k1gInSc7	projev
a	a	k8xC	a
obrovským	obrovský	k2eAgInSc7d1	obrovský
rozsahem	rozsah	k1gInSc7	rozsah
<g/>
,	,	kIx,	,
Pageovy	Pageův	k2eAgFnPc4d1	Pageova
schopnosti	schopnost	k1gFnPc4	schopnost
skladatelské	skladatelský	k2eAgFnPc4d1	skladatelská
<g/>
,	,	kIx,	,
producentské	producentský	k2eAgFnPc4d1	producentská
a	a	k8xC	a
hráčské	hráčský	k2eAgFnPc4d1	hráčská
<g/>
,	,	kIx,	,
Bonhamovy	Bonhamův	k2eAgFnPc4d1	Bonhamův
paže	paže	k1gFnPc4	paže
rychlé	rychlý	k2eAgNnSc4d1	rychlé
jako	jako	k8xS	jako
světlo	světlo	k1gNnSc4	světlo
a	a	k8xC	a
velice	velice	k6eAd1	velice
progresivní	progresivní	k2eAgInSc1d1	progresivní
styl	styl	k1gInSc1	styl
bubnování	bubnování	k1gNnSc2	bubnování
(	(	kIx(	(
<g/>
byl	být	k5eAaImAgMnS	být
to	ten	k3xDgNnSc4	ten
např.	např.	kA	např.
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
poprvé	poprvé	k6eAd1	poprvé
použil	použít	k5eAaPmAgMnS	použít
"	"	kIx"	"
<g/>
metalové	metalový	k2eAgNnSc1d1	metalové
<g/>
"	"	kIx"	"
víření	víření	k1gNnSc1	víření
basovým	basový	k2eAgInSc7d1	basový
bubnem	buben	k1gInSc7	buben
a	a	k8xC	a
ukázal	ukázat	k5eAaPmAgMnS	ukázat
tím	ten	k3xDgNnSc7	ten
cestu	cesta	k1gFnSc4	cesta
mnohým	mnohý	k2eAgNnSc7d1	mnohé
dalším	další	k1gNnSc7	další
<g/>
)	)	kIx)	)
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
John	John	k1gMnSc1	John
Paul	Paul	k1gMnSc1	Paul
Jones	Jones	k1gMnSc1	Jones
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
tvořil	tvořit	k5eAaImAgInS	tvořit
solidní	solidní	k2eAgFnSc4d1	solidní
páteř	páteř	k1gFnSc4	páteř
celé	celý	k2eAgFnSc2d1	celá
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
s	s	k7c7	s
postupujícím	postupující	k2eAgInSc7d1	postupující
časem	čas	k1gInSc7	čas
také	také	k9	také
víc	hodně	k6eAd2	hodně
a	a	k8xC	a
víc	hodně	k6eAd2	hodně
skládal	skládat	k5eAaImAgMnS	skládat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
připojili	připojit	k5eAaPmAgMnP	připojit
Led	led	k1gInSc4	led
Zeppelin	Zeppelin	k2eAgInSc4d1	Zeppelin
druhou	druhý	k4xOgFnSc4	druhý
<g/>
,	,	kIx,	,
po	po	k7c6	po
nocích	noc	k1gFnPc6	noc
na	na	k7c6	na
cestách	cesta	k1gFnPc6	cesta
natočenou	natočený	k2eAgFnSc4d1	natočená
LP	LP	kA	LP
Led	led	k1gInSc4	led
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
II	II	kA	II
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
mnohými	mnohý	k2eAgInPc7d1	mnohý
kritiky	kritika	k1gFnSc2	kritika
označována	označovat	k5eAaImNgFnS	označovat
za	za	k7c4	za
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
souboru	soubor	k1gInSc2	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodně	rozhodně	k6eAd1	rozhodně
je	být	k5eAaImIp3nS	být
dokonalou	dokonalý	k2eAgFnSc7d1	dokonalá
ukázkou	ukázka	k1gFnSc7	ukázka
hardrockového	hardrockový	k2eAgInSc2d1	hardrockový
ideálu	ideál	k1gInSc2	ideál
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nespoutaný	spoutaný	k2eNgInSc4d1	nespoutaný
a	a	k8xC	a
úderný	úderný	k2eAgInSc4d1	úderný
rock	rock	k1gInSc4	rock
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
vzrušenými	vzrušený	k2eAgMnPc7d1	vzrušený
shouty	shout	k1gMnPc7	shout
Roberta	Roberta	k1gFnSc1	Roberta
Planta	planta	k1gFnSc1	planta
povýšen	povýšit	k5eAaPmNgMnS	povýšit
virtuozitou	virtuozita	k1gFnSc7	virtuozita
všech	všecek	k3xTgMnPc2	všecek
hráčů	hráč	k1gMnPc2	hráč
a	a	k8xC	a
jednoznačným	jednoznačný	k2eAgInSc7d1	jednoznačný
názorem	názor	k1gInSc7	názor
jejich	jejich	k3xOp3gFnPc2	jejich
kompozic	kompozice	k1gFnPc2	kompozice
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
vyniká	vynikat	k5eAaImIp3nS	vynikat
především	především	k6eAd1	především
"	"	kIx"	"
<g/>
Whole	Whole	k1gFnSc1	Whole
Lotta	Lotta	k1gFnSc1	Lotta
Love	lov	k1gInSc5	lov
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
tato	tento	k3xDgFnSc1	tento
píseň	píseň	k1gFnSc1	píseň
byla	být	k5eAaImAgFnS	být
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1969	[number]	k4	1969
vydána	vydán	k2eAgFnSc1d1	vydána
na	na	k7c4	na
jediné	jediné	k1gNnSc4	jediné
SP	SP	kA	SP
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
po	po	k7c6	po
týdnu	týden	k1gInSc6	týden
však	však	k9	však
byla	být	k5eAaImAgFnS	být
překvapivě	překvapivě	k6eAd1	překvapivě
stažena	stáhnout	k5eAaPmNgFnS	stáhnout
a	a	k8xC	a
Led	led	k1gInSc1	led
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
se	se	k3xPyFc4	se
výjimečně	výjimečně	k6eAd1	výjimečně
stali	stát	k5eAaPmAgMnP	stát
skupinou	skupina	k1gFnSc7	skupina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
celou	celý	k2eAgFnSc4d1	celá
svou	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
obešla	obejít	k5eAaPmAgFnS	obejít
bez	bez	k7c2	bez
singlů	singl	k1gInPc2	singl
i	i	k9	i
bez	bez	k7c2	bez
popularizace	popularizace	k1gFnSc2	popularizace
na	na	k7c6	na
hitparádách	hitparáda	k1gFnPc6	hitparáda
a	a	k8xC	a
přesto	přesto	k8xC	přesto
prodala	prodat	k5eAaPmAgFnS	prodat
přes	přes	k7c4	přes
50	[number]	k4	50
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
desek	deska	k1gFnPc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
9	[number]	k4	9
SP	SP	kA	SP
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
největší	veliký	k2eAgFnSc7d3	veliký
ohlas	ohlas	k1gInSc4	ohlas
měly	mít	k5eAaImAgInP	mít
tituly	titul	k1gInPc1	titul
"	"	kIx"	"
<g/>
Communication	Communication	k1gInSc1	Communication
Breakdown	Breakdown	k1gNnSc1	Breakdown
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Whole	Whole	k1gFnSc1	Whole
Lotta	Lotta	k1gFnSc1	Lotta
Love	lov	k1gInSc5	lov
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Immigrant	Immigrant	k1gMnSc1	Immigrant
Song	song	k1gInSc1	song
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Black	Black	k1gInSc1	Black
Dog	doga	k1gFnPc2	doga
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Rock	rock	k1gInSc1	rock
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
Roll	Roll	k1gInSc4	Roll
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
D	D	kA	D
<g/>
'	'	kIx"	'
<g/>
yer	yer	k?	yer
Mak	mako	k1gNnPc2	mako
<g/>
'	'	kIx"	'
<g/>
er	er	k?	er
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejúspěšnější	úspěšný	k2eAgFnSc7d3	nejúspěšnější
skladbou	skladba	k1gFnSc7	skladba
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
skupiny	skupina	k1gFnSc2	skupina
však	však	k9	však
byla	být	k5eAaImAgFnS	být
píseň	píseň	k1gFnSc1	píseň
Stairway	Stairwaa	k1gFnSc2	Stairwaa
to	ten	k3xDgNnSc4	ten
Heaven	Heavno	k1gNnPc2	Heavno
ze	z	k7c2	z
čtvrtého	čtvrtý	k4xOgNnSc2	čtvrtý
bezejmenného	bezejmenný	k2eAgNnSc2d1	bezejmenné
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k8xS	jako
Led	led	k1gInSc1	led
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
IV	IV	kA	IV
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
anketu	anketa	k1gFnSc4	anketa
sdružených	sdružený	k2eAgFnPc2d1	sdružená
amerických	americký	k2eAgFnPc2d1	americká
rozhlasových	rozhlasový	k2eAgFnPc2d1	rozhlasová
stanic	stanice	k1gFnPc2	stanice
o	o	k7c4	o
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
píseň	píseň	k1gFnSc4	píseň
posledních	poslední	k2eAgInPc2d1	poslední
50	[number]	k4	50
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
album	album	k1gNnSc1	album
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
předchozí	předchozí	k2eAgMnSc1d1	předchozí
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
Led	led	k1gInSc1	led
Zeppelin	Zeppelin	k2eAgInSc1d1	Zeppelin
III	III	kA	III
se	s	k7c7	s
stěžejními	stěžejní	k2eAgInPc7d1	stěžejní
snímky	snímek	k1gInPc7	snímek
"	"	kIx"	"
<g/>
Immigrant	Immigrant	k1gMnSc1	Immigrant
Song	song	k1gInSc1	song
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Friends	Friends	k1gInSc1	Friends
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
naznačilo	naznačit	k5eAaPmAgNnS	naznačit
příklon	příklon	k1gInSc4	příklon
skupiny	skupina	k1gFnSc2	skupina
k	k	k7c3	k
akustickému	akustický	k2eAgNnSc3d1	akustické
<g/>
,	,	kIx,	,
specificky	specificky	k6eAd1	specificky
folkově	folkově	k6eAd1	folkově
avantgardnímu	avantgardní	k2eAgInSc3d1	avantgardní
zvuku	zvuk	k1gInSc3	zvuk
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
<g />
.	.	kIx.	.
</s>
<s>
pozdějších	pozdní	k2eAgNnPc2d2	pozdější
slov	slovo	k1gNnPc2	slovo
Robeta	Robeta	k1gFnSc1	Robeta
Planta	planta	k1gFnSc1	planta
(	(	kIx(	(
<g/>
Melody	Melod	k1gInPc4	Melod
Marker	marker	k1gInSc1	marker
19	[number]	k4	19
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
chtěla	chtít	k5eAaImAgFnS	chtít
odlišit	odlišit	k5eAaPmF	odlišit
od	od	k7c2	od
hudby	hudba	k1gFnSc2	hudba
typu	typ	k1gInSc2	typ
Deep	Deep	k1gInSc1	Deep
Purple	Purple	k1gFnSc1	Purple
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Počínaje	počínaje	k7c7	počínaje
třetím	třetí	k4xOgNnSc7	třetí
albem	album	k1gNnSc7	album
jsme	být	k5eAaImIp1nP	být
chtěli	chtít	k5eAaImAgMnP	chtít
lidem	člověk	k1gMnPc3	člověk
ukázat	ukázat	k5eAaPmF	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
umíme	umět	k5eAaImIp1nP	umět
zahrát	zahrát	k5eAaPmF	zahrát
i	i	k9	i
něco	něco	k3yInSc4	něco
jiného	jiný	k2eAgMnSc4d1	jiný
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Skupina	skupina	k1gFnSc1	skupina
vždy	vždy	k6eAd1	vždy
měla	mít	k5eAaImAgFnS	mít
hlavní	hlavní	k2eAgFnSc4d1	hlavní
základnu	základna	k1gFnSc4	základna
svých	svůj	k3xOyFgMnPc2	svůj
posluchačů	posluchač	k1gMnPc2	posluchač
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nepřetržitě	přetržitě	k6eNd1	přetržitě
lámala	lámat	k5eAaImAgFnS	lámat
návštěvnické	návštěvnický	k2eAgInPc4d1	návštěvnický
rekordy	rekord	k1gInPc4	rekord
<g/>
:	:	kIx,	:
1973	[number]	k4	1973
v	v	k7c6	v
Tampě	Tampa	k1gFnSc6	Tampa
57	[number]	k4	57
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
v	v	k7c6	v
Silverstone	Silverston	k1gInSc5	Silverston
76	[number]	k4	76
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
vystoupení	vystoupení	k1gNnSc1	vystoupení
1973	[number]	k4	1973
v	v	k7c4	v
Madison	Madison	k1gInSc4	Madison
Square	square	k1gInSc1	square
Garden	Gardna	k1gFnPc2	Gardna
(	(	kIx(	(
<g/>
NYC	NYC	kA	NYC
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
zfilmováno	zfilmován	k2eAgNnSc1d1	zfilmováno
a	a	k8xC	a
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
premiérou	premiéra	k1gFnSc7	premiéra
filmu	film	k1gInSc2	film
vyšel	vyjít	k5eAaPmAgMnS	vyjít
1976	[number]	k4	1976
i	i	k8xC	i
dvojalbový	dvojalbový	k2eAgInSc4d1	dvojalbový
soundtrack	soundtrack	k1gInSc4	soundtrack
The	The	k1gFnSc1	The
Song	song	k1gInSc1	song
Remains	Remains	k1gInSc1	Remains
the	the	k?	the
Same	Sam	k1gMnSc5	Sam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
uvedla	uvést	k5eAaPmAgFnS	uvést
skupina	skupina	k1gFnSc1	skupina
v	v	k7c4	v
život	život	k1gInSc4	život
svou	svůj	k3xOyFgFnSc4	svůj
gramofonovou	gramofonový	k2eAgFnSc4d1	gramofonová
společnost	společnost	k1gFnSc4	společnost
Swan	Swana	k1gFnPc2	Swana
Song	song	k1gInSc4	song
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dala	dát	k5eAaPmAgFnS	dát
příležitost	příležitost	k1gFnSc1	příležitost
mj.	mj.	kA	mj.
skupinám	skupina	k1gFnPc3	skupina
Bad	Bad	k1gFnSc2	Bad
Company	Compana	k1gFnSc2	Compana
<g/>
,	,	kIx,	,
Detective	Detectiv	k1gInSc5	Detectiv
a	a	k8xC	a
Pretty	Prett	k1gInPc7	Prett
Things	Thingsa	k1gFnPc2	Thingsa
a	a	k8xC	a
sólistům	sólista	k1gMnPc3	sólista
M.	M.	kA	M.
Belové	Belové	k2eAgInPc7d1	Belové
<g/>
,	,	kIx,	,
Dave	Dav	k1gInSc5	Dav
Edmundsovi	Edmunds	k1gMnSc3	Edmunds
a	a	k8xC	a
svému	svůj	k1gMnSc3	svůj
favoritovi	favorit	k1gMnSc3	favorit
Roy	Roy	k1gMnSc1	Roy
Harperovi	Harper	k1gMnSc3	Harper
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
věnovala	věnovat	k5eAaImAgFnS	věnovat
i	i	k9	i
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Hats	Hats	k1gInSc1	Hats
Off	Off	k1gMnSc1	Off
to	ten	k3xDgNnSc4	ten
(	(	kIx(	(
<g/>
Roy	Roy	k1gMnSc1	Roy
<g/>
)	)	kIx)	)
Harper	Harper	k1gMnSc1	Harper
<g/>
"	"	kIx"	"
na	na	k7c4	na
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
LP	LP	kA	LP
(	(	kIx(	(
<g/>
Led	led	k1gInSc1	led
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
III	III	kA	III
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
akademicky	akademicky	k6eAd1	akademicky
chladném	chladný	k2eAgNnSc6d1	chladné
<g/>
,	,	kIx,	,
komorním	komorní	k2eAgNnSc6d1	komorní
albu	album	k1gNnSc6	album
Houses	Houses	k1gMnSc1	Houses
of	of	k?	of
the	the	k?	the
Holy	hola	k1gFnSc2	hola
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
debutovali	debutovat	k5eAaBmAgMnP	debutovat
Led	led	k1gInSc4	led
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
značce	značka	k1gFnSc6	značka
dvojalbem	dvojalbum	k1gNnSc7	dvojalbum
Physical	Physical	k1gFnSc2	Physical
Graffiti	graffiti	k1gNnSc2	graffiti
s	s	k7c7	s
nejlepšími	dobrý	k2eAgInPc7d3	nejlepší
snímky	snímek	k1gInPc7	snímek
"	"	kIx"	"
<g/>
Custard	Custard	k1gInSc1	Custard
Pie	Pius	k1gMnSc5	Pius
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Trampled	Trampled	k1gMnSc1	Trampled
under	under	k1gMnSc1	under
Foot	Foot	k1gMnSc1	Foot
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Kashmir	Kashmir	k1gInSc1	Kashmir
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kompozičně	kompozičně	k6eAd1	kompozičně
i	i	k9	i
zvukově	zvukově	k6eAd1	zvukově
pojatými	pojatý	k2eAgInPc7d1	pojatý
stejně	stejně	k9	stejně
grandiózně	grandiózně	k6eAd1	grandiózně
jako	jako	k8xS	jako
skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
Achilles	Achilles	k1gMnSc1	Achilles
Last	Last	k1gMnSc1	Last
Stand	Standa	k1gFnPc2	Standa
<g/>
"	"	kIx"	"
z	z	k7c2	z
následujícího	následující	k2eAgInSc2d1	následující
LP	LP	kA	LP
Presence	presence	k1gFnSc2	presence
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
souboru	soubor	k1gInSc2	soubor
předána	předat	k5eAaPmNgFnS	předat
mimořádná	mimořádný	k2eAgFnSc1d1	mimořádná
cena	cena	k1gFnSc1	cena
Ivora	Ivor	k1gMnSc2	Ivor
Novella	Novell	k1gMnSc2	Novell
za	za	k7c4	za
vynikající	vynikající	k2eAgInSc4d1	vynikající
přínos	přínos	k1gInSc4	přínos
britské	britský	k2eAgFnSc3d1	britská
hudbě	hudba	k1gFnSc3	hudba
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
naplnilo	naplnit	k5eAaPmAgNnS	naplnit
obtížné	obtížný	k2eAgNnSc1d1	obtížné
období	období	k1gNnSc1	období
Roberta	Robert	k1gMnSc2	Robert
Planta	planta	k1gFnSc1	planta
(	(	kIx(	(
<g/>
předtím	předtím	k6eAd1	předtím
-	-	kIx~	-
1975	[number]	k4	1975
-	-	kIx~	-
se	se	k3xPyFc4	se
vážně	vážně	k6eAd1	vážně
zranil	zranit	k5eAaPmAgMnS	zranit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Maureen	Maurena	k1gFnPc2	Maurena
při	při	k7c6	při
autonehodě	autonehoda	k1gFnSc6	autonehoda
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
27	[number]	k4	27
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1977	[number]	k4	1977
neočekávaně	očekávaně	k6eNd1	očekávaně
zemřel	zemřít	k5eAaPmAgMnS	zemřít
jeho	jeho	k3xOp3gMnSc1	jeho
pětiletý	pětiletý	k2eAgMnSc1d1	pětiletý
syn	syn	k1gMnSc1	syn
Karac	Karac	k1gFnSc1	Karac
(	(	kIx(	(
<g/>
Plant	planta	k1gFnPc2	planta
má	mít	k5eAaImIp3nS	mít
kromě	kromě	k7c2	kromě
něho	on	k3xPp3gNnSc2	on
ještě	ještě	k6eAd1	ještě
o	o	k7c4	o
4	[number]	k4	4
roky	rok	k1gInPc4	rok
starší	starý	k2eAgFnSc4d2	starší
dceru	dcera	k1gFnSc4	dcera
Carmen	Carmen	k2eAgMnSc1d1	Carmen
<g/>
)	)	kIx)	)
-	-	kIx~	-
pro	pro	k7c4	pro
něho	on	k3xPp3gMnSc4	on
byla	být	k5eAaImAgFnS	být
napsána	napsat	k5eAaBmNgFnS	napsat
skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
All	All	k1gFnSc1	All
My	my	k3xPp1nPc1	my
Love	lov	k1gInSc5	lov
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
proto	proto	k8xC	proto
přerušila	přerušit	k5eAaPmAgFnS	přerušit
své	svůj	k3xOyFgNnSc4	svůj
koncertní	koncertní	k2eAgNnSc4d1	koncertní
turné	turné	k1gNnSc4	turné
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
na	na	k7c4	na
pódium	pódium	k1gNnSc4	pódium
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
Plant	planta	k1gFnPc2	planta
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
pohostinsky	pohostinsky	k6eAd1	pohostinsky
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Little	Little	k1gFnSc1	Little
Acre	Acre	k1gNnPc4	Acre
v	v	k7c6	v
Birminghamu	Birmingham	k1gInSc6	Birmingham
<g/>
,	,	kIx,	,
s	s	k7c7	s
D.	D.	kA	D.
Edmundsem	Edmunds	k1gMnSc7	Edmunds
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
a	a	k8xC	a
skupinou	skupina	k1gFnSc7	skupina
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Feelgood	Feelgood	k1gInSc1	Feelgood
ve	v	k7c6	v
španělské	španělský	k2eAgFnSc6d1	španělská
Ibize	Ibiza	k1gFnSc6	Ibiza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
skupina	skupina	k1gFnSc1	skupina
rozvinula	rozvinout	k5eAaPmAgFnS	rozvinout
blues-hard	bluesard	k1gInSc4	blues-hard
rockový	rockový	k2eAgInSc4d1	rockový
koncept	koncept	k1gInSc4	koncept
své	svůj	k3xOyFgFnSc2	svůj
hudby	hudba	k1gFnSc2	hudba
na	na	k7c6	na
albu	album	k1gNnSc6	album
In	In	k1gFnSc2	In
Through	Through	k1gMnSc1	Through
the	the	k?	the
Out	Out	k1gMnSc1	Out
Door	Door	k1gMnSc1	Door
s	s	k7c7	s
dominujícím	dominující	k2eAgInSc7d1	dominující
zvukem	zvuk	k1gInSc7	zvuk
syntezátorů	syntezátor	k1gInPc2	syntezátor
J.	J.	kA	J.
P.	P.	kA	P.
Jonese	Jonese	k1gFnSc2	Jonese
a	a	k8xC	a
kytarového	kytarový	k2eAgInSc2d1	kytarový
syntezátoru	syntezátor	k1gInSc2	syntezátor
Roland	Roland	k1gInSc1	Roland
Jimmyho	Jimmy	k1gMnSc2	Jimmy
Page	Pag	k1gMnSc2	Pag
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
prahu	práh	k1gInSc6	práh
další	další	k2eAgFnSc2d1	další
dekády	dekáda	k1gFnSc2	dekáda
<g/>
,	,	kIx,	,
do	do	k7c2	do
jejíhož	jejíž	k3xOyRp3gInSc2	jejíž
hudebního	hudební	k2eAgInSc2d1	hudební
vývoje	vývoj	k1gInSc2	vývoj
by	by	kYmCp3nS	by
Led	led	k1gInSc4	led
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
nepochybně	pochybně	k6eNd1	pochybně
rovněž	rovněž	k9	rovněž
promluvili	promluvit	k5eAaPmAgMnP	promluvit
<g/>
,	,	kIx,	,
umírá	umírat	k5eAaImIp3nS	umírat
John	John	k1gMnSc1	John
Bonham	Bonham	k1gInSc1	Bonham
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1980	[number]	k4	1980
-	-	kIx~	-
opilý	opilý	k2eAgMnSc1d1	opilý
se	se	k3xPyFc4	se
udusil	udusit	k5eAaPmAgMnS	udusit
zvratky	zvratek	k1gInPc4	zvratek
<g/>
)	)	kIx)	)
a	a	k8xC	a
skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
ukončit	ukončit	k5eAaPmF	ukončit
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgNnSc1d1	následující
sólové	sólový	k2eAgNnSc1d1	sólové
album	album	k1gNnSc1	album
Roberta	Robert	k1gMnSc2	Robert
Planta	planta	k1gFnSc1	planta
(	(	kIx(	(
<g/>
s	s	k7c7	s
Robbiem	Robbius	k1gMnSc7	Robbius
Bluntem	Blunt	k1gMnSc7	Blunt
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
C.	C.	kA	C.
Powellem	Powell	k1gMnSc7	Powell
a	a	k8xC	a
P.	P.	kA	P.
Collinsem	Collins	k1gInSc7	Collins
-	-	kIx~	-
bicí	bicí	k2eAgInPc1d1	bicí
nástroje	nástroj	k1gInPc1	nástroj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pageova	Pageův	k2eAgFnSc1d1	Pageova
hudba	hudba	k1gFnSc1	hudba
k	k	k7c3	k
filmu	film	k1gInSc3	film
Death	Death	k1gMnSc1	Death
Wish	Wish	k1gMnSc1	Wish
II	II	kA	II
<g/>
,	,	kIx,	,
příležitostné	příležitostný	k2eAgNnSc1d1	příležitostné
seskupení	seskupení	k1gNnSc1	seskupení
Roberta	Robert	k1gMnSc2	Robert
Planta	planta	k1gFnSc1	planta
The	The	k1gFnSc1	The
Honey	Honea	k1gFnSc2	Honea
Dripper	Drippra	k1gFnPc2	Drippra
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
inspirované	inspirovaný	k2eAgFnSc2d1	inspirovaná
Ahmetem	Ahmet	k1gMnSc7	Ahmet
Ertegünem	Ertegün	k1gMnSc7	Ertegün
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
Jimmyho	Jimmyze	k6eAd1	Jimmyze
Page	Pag	k1gInPc1	Pag
s	s	k7c7	s
J.	J.	kA	J.
Beckem	Becek	k1gInSc7	Becek
(	(	kIx(	(
<g/>
obě	dva	k4xCgFnPc1	dva
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
vydání	vydání	k1gNnSc2	vydání
alba	album	k1gNnSc2	album
Coda	coda	k1gFnSc1	coda
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
s	s	k7c7	s
dosud	dosud	k6eAd1	dosud
nepublikovanými	publikovaný	k2eNgFnPc7d1	nepublikovaná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
méně	málo	k6eAd2	málo
významnými	významný	k2eAgInPc7d1	významný
snímky	snímek	k1gInPc7	snímek
z	z	k7c2	z
pozůstalosti	pozůstalost	k1gFnSc2	pozůstalost
Led	led	k1gInSc1	led
Zeppelin	Zeppelin	k2eAgInSc1d1	Zeppelin
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
chápány	chápat	k5eAaImNgFnP	chápat
jako	jako	k8xS	jako
předstupeň	předstupeň	k1gInSc1	předstupeň
budoucích	budoucí	k2eAgInPc2d1	budoucí
projektů	projekt	k1gInPc2	projekt
<g/>
,	,	kIx,	,
snad	snad	k9	snad
opět	opět	k6eAd1	opět
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
stálé	stálý	k2eAgNnSc1d1	stálé
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
obsazené	obsazený	k2eAgFnPc1d1	obsazená
skupiny	skupina	k1gFnPc1	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
stojí	stát	k5eAaImIp3nS	stát
též	též	k9	též
výrazný	výrazný	k2eAgInSc1d1	výrazný
přínos	přínos	k1gInSc1	přínos
Petera	Peter	k1gMnSc2	Peter
Granta	Grant	k1gMnSc2	Grant
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1935	[number]	k4	1935
-	-	kIx~	-
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
producenta	producent	k1gMnSc2	producent
a	a	k8xC	a
manažera	manažer	k1gMnSc2	manažer
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
dokázal	dokázat	k5eAaPmAgMnS	dokázat
vytvořit	vytvořit	k5eAaPmF	vytvořit
kolem	kolem	k7c2	kolem
skupiny	skupina	k1gFnSc2	skupina
opar	opar	k1gInSc1	opar
tajemna	tajemno	k1gNnSc2	tajemno
a	a	k8xC	a
výjimečnosti	výjimečnost	k1gFnSc2	výjimečnost
<g/>
,	,	kIx,	,
zásadně	zásadně	k6eAd1	zásadně
byl	být	k5eAaImAgInS	být
proti	proti	k7c3	proti
vydávání	vydávání	k1gNnSc3	vydávání
singlů	singl	k1gInPc2	singl
skupiny	skupina	k1gFnSc2	skupina
a	a	k8xC	a
dokázal	dokázat	k5eAaPmAgMnS	dokázat
<g />
.	.	kIx.	.
</s>
<s>
prosadit	prosadit	k5eAaPmF	prosadit
kapelu	kapela	k1gFnSc4	kapela
i	i	k9	i
bez	bez	k7c2	bez
hitparádových	hitparádový	k2eAgFnPc2d1	hitparádová
berliček	berlička	k1gFnPc2	berlička
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2008	[number]	k4	2008
přinesla	přinést	k5eAaPmAgFnS	přinést
BBC	BBC	kA	BBC
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jimmy	Jimma	k1gFnPc1	Jimma
Page	Pag	k1gFnSc2	Pag
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Paul	Paul	k1gMnSc1	Paul
Jones	Jones	k1gMnSc1	Jones
a	a	k8xC	a
Jason	Jason	k1gMnSc1	Jason
Bonham	Bonham	k1gInSc4	Bonham
nahráli	nahrát	k5eAaPmAgMnP	nahrát
materiál	materiál	k1gInSc4	materiál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
využit	využít	k5eAaPmNgInS	využít
jako	jako	k8xC	jako
nové	nový	k2eAgNnSc1d1	nové
studiové	studiový	k2eAgNnSc1d1	studiové
album	album	k1gNnSc1	album
skupiny	skupina	k1gFnSc2	skupina
<g/>
;	;	kIx,	;
Robert	Robert	k1gMnSc1	Robert
Plant	planta	k1gFnPc2	planta
se	se	k3xPyFc4	se
této	tento	k3xDgFnSc2	tento
schůze	schůze	k1gFnSc2	schůze
prý	prý	k9	prý
nezúčastnil	zúčastnit	k5eNaPmAgMnS	zúčastnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
už	už	k6eAd1	už
se	se	k3xPyFc4	se
nečekalo	čekat	k5eNaImAgNnS	čekat
od	od	k7c2	od
"	"	kIx"	"
<g/>
nových	nový	k2eAgFnPc2d1	nová
<g/>
"	"	kIx"	"
Led	led	k1gInSc1	led
Zeppelinů	Zeppelin	k1gInPc2	Zeppelin
nic	nic	k3yNnSc1	nic
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
,	,	kIx,	,
než	než	k8xS	než
sólové	sólový	k2eAgFnPc1d1	sólová
dráhy	dráha	k1gFnPc1	dráha
členů	člen	k1gMnPc2	člen
kapely	kapela	k1gFnSc2	kapela
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Led	led	k1gInSc1	led
Zeppelini	Zeppelin	k2eAgMnPc1d1	Zeppelin
chystají	chystat	k5eAaImIp3nP	chystat
opět	opět	k6eAd1	opět
zahrát	zahrát	k5eAaPmF	zahrát
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Ano	ano	k9	ano
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
tak	tak	k6eAd1	tak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
O2	O2	k1gFnSc6	O2
Aréně	aréna	k1gFnSc6	aréna
se	se	k3xPyFc4	se
rozlehla	rozlehnout	k5eAaPmAgNnP	rozlehnout
kytarová	kytarový	k2eAgNnPc1d1	kytarové
sóla	sólo	k1gNnPc1	sólo
Jimmyho	Jimmy	k1gMnSc2	Jimmy
Page	Pag	k1gMnSc2	Pag
<g/>
,	,	kIx,	,
složité	složitý	k2eAgNnSc1d1	složité
bubnování	bubnování	k1gNnSc1	bubnování
Jasona	Jason	k1gMnSc2	Jason
Bonhama	Bonham	k1gMnSc2	Bonham
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
převzal	převzít	k5eAaPmAgMnS	převzít
otěže	otěž	k1gFnPc4	otěž
po	po	k7c6	po
svém	svůj	k1gMnSc6	svůj
otci	otec	k1gMnSc6	otec
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
melodický	melodický	k2eAgInSc1d1	melodický
hlas	hlas	k1gInSc1	hlas
Roberta	Robert	k1gMnSc2	Robert
Planta	planta	k1gFnSc1	planta
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
kritiků	kritik	k1gMnPc2	kritik
v	v	k7c6	v
nejlepší	dobrý	k2eAgFnSc6d3	nejlepší
formě	forma	k1gFnSc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
Suma	suma	k1gFnSc1	suma
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
vytěžená	vytěžený	k2eAgFnSc1d1	vytěžená
ze	z	k7c2	z
vstupného	vstupné	k1gNnSc2	vstupné
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
věnována	věnovat	k5eAaImNgFnS	věnovat
na	na	k7c4	na
charitu	charita	k1gFnSc4	charita
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
po	po	k7c6	po
zahájení	zahájení	k1gNnSc6	zahájení
prodeje	prodej	k1gInSc2	prodej
lístků	lístek	k1gInPc2	lístek
byly	být	k5eAaImAgInP	být
lístky	lístek	k1gInPc1	lístek
vyprodané	vyprodaný	k2eAgInPc1d1	vyprodaný
za	za	k7c4	za
necelý	celý	k2eNgInSc4d1	necelý
týden	týden	k1gInSc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
koncert	koncert	k1gInSc4	koncert
se	se	k3xPyFc4	se
sjeli	sjet	k5eAaPmAgMnP	sjet
nadšenci	nadšenec	k1gMnPc1	nadšenec
bezmála	bezmála	k6eAd1	bezmála
ze	z	k7c2	z
40	[number]	k4	40
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Kritici	kritik	k1gMnPc1	kritik
tento	tento	k3xDgInSc4	tento
koncert	koncert	k1gInSc4	koncert
odsuzovali	odsuzovat	k5eAaImAgMnP	odsuzovat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
když	když	k8xS	když
zazněla	zaznít	k5eAaPmAgFnS	zaznít
úvodní	úvodní	k2eAgFnSc4d1	úvodní
píseň	píseň	k1gFnSc4	píseň
Good	Good	k1gMnSc1	Good
Times	Times	k1gMnSc1	Times
Bad	Bad	k1gMnSc1	Bad
Times	Times	k1gMnSc1	Times
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc1	všechen
pochyby	pochyba	k1gFnPc1	pochyba
opadly	opadnout	k5eAaPmAgFnP	opadnout
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
koncertu	koncert	k1gInSc6	koncert
se	se	k3xPyFc4	se
dlouho	dlouho	k6eAd1	dlouho
spekuluje	spekulovat	k5eAaImIp3nS	spekulovat
o	o	k7c6	o
návratu	návrat	k1gInSc6	návrat
Led	led	k1gInSc1	led
Zeppelinů	Zeppelin	k1gInPc2	Zeppelin
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2007	[number]	k4	2007
sešli	sejít	k5eAaPmAgMnP	sejít
po	po	k7c6	po
dlouhých	dlouhý	k2eAgInPc6d1	dlouhý
letech	let	k1gInPc6	let
pospolu	pospolu	k6eAd1	pospolu
<g/>
.	.	kIx.	.
</s>
<s>
Nečekané	čekaný	k2eNgNnSc1d1	nečekané
překvapení	překvapení	k1gNnSc1	překvapení
přišlo	přijít	k5eAaPmAgNnS	přijít
také	také	k9	také
na	na	k7c6	na
olympiádě	olympiáda	k1gFnSc6	olympiáda
Beijing	Beijing	k1gInSc1	Beijing
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
na	na	k7c4	na
zakončení	zakončení	k1gNnSc4	zakončení
olympiády	olympiáda	k1gFnSc2	olympiáda
Jimmy	Jimma	k1gFnSc2	Jimma
Page	Pag	k1gFnSc2	Pag
zahrál	zahrát	k5eAaPmAgInS	zahrát
rockovou	rockový	k2eAgFnSc4d1	rocková
klasiku	klasika	k1gFnSc4	klasika
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
"	"	kIx"	"
<g/>
Whole	Whole	k1gInSc1	Whole
Lotta	Lott	k1gInSc2	Lott
Love	lov	k1gInSc5	lov
<g/>
"	"	kIx"	"
s	s	k7c7	s
popovou	popový	k2eAgFnSc7d1	popová
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
Leonou	Leona	k1gFnSc7	Leona
Lewis	Lewis	k1gFnSc7	Lewis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
byl	být	k5eAaImAgInS	být
vydán	vydán	k2eAgInSc1d1	vydán
záznam	záznam	k1gInSc1	záznam
z	z	k7c2	z
koncertu	koncert	k1gInSc2	koncert
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Celebration	Celebration	k1gInSc1	Celebration
Day	Day	k1gFnPc2	Day
<g/>
.	.	kIx.	.
1969	[number]	k4	1969
Led	led	k1gInSc1	led
Zeppelin	Zeppelin	k2eAgInSc1d1	Zeppelin
1969	[number]	k4	1969
Led	led	k1gInSc1	led
Zeppelin	Zeppelin	k2eAgInSc1d1	Zeppelin
II	II	kA	II
1970	[number]	k4	1970
Led	led	k1gInSc1	led
Zeppelin	Zeppelin	k2eAgInSc1d1	Zeppelin
III	III	kA	III
1971	[number]	k4	1971
bezejmenné	bezejmenný	k2eAgNnSc1d1	bezejmenné
album	album	k1gNnSc4	album
známé	známý	k2eAgNnSc4d1	známé
jako	jako	k8xC	jako
Led	led	k1gInSc4	led
Zeppelin	Zeppelin	k2eAgInSc4d1	Zeppelin
IV	IV	kA	IV
1973	[number]	k4	1973
Houses	Houses	k1gInSc4	Houses
of	of	k?	of
the	the	k?	the
Holy	hola	k1gFnSc2	hola
1975	[number]	k4	1975
Physical	Physical	k1gFnSc1	Physical
Graffiti	graffiti	k1gNnSc2	graffiti
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
CD	CD	kA	CD
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
1976	[number]	k4	1976
Presence	presence	k1gFnSc2	presence
1979	[number]	k4	1979
In	In	k1gMnSc1	In
Through	Through	k1gMnSc1	Through
the	the	k?	the
Out	Out	k1gMnSc1	Out
Door	Door	k1gInSc4	Door
1982	[number]	k4	1982
Coda	coda	k1gFnSc1	coda
1976	[number]	k4	1976
The	The	k1gMnPc2	The
Song	song	k1gInSc4	song
Remains	Remainsa	k1gFnPc2	Remainsa
the	the	k?	the
Same	Sam	k1gMnSc5	Sam
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
CD	CD	kA	CD
<g/>
)	)	kIx)	)
1997	[number]	k4	1997
BBC	BBC	kA	BBC
Sessions	Sessions	k1gInSc4	Sessions
2003	[number]	k4	2003
How	How	k1gMnSc1	How
the	the	k?	the
West	West	k1gMnSc1	West
Was	Was	k1gMnSc1	Was
Won	won	k1gInSc4	won
2012	[number]	k4	2012
Celebration	Celebration	k1gInSc1	Celebration
Day	Day	k1gFnSc2	Day
1990	[number]	k4	1990
Profiled	Profiled	k1gMnSc1	Profiled
1990	[number]	k4	1990
Box	box	k1gInSc1	box
Set	set	k1gInSc4	set
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
CD	CD	kA	CD
<g/>
)	)	kIx)	)
1992	[number]	k4	1992
Remasters	Remasters	k1gInSc1	Remasters
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
CD	CD	kA	CD
<g/>
)	)	kIx)	)
1993	[number]	k4	1993
Box	box	k1gInSc1	box
Set	set	k1gInSc1	set
2	[number]	k4	2
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
CD	CD	kA	CD
<g/>
)	)	kIx)	)
1993	[number]	k4	1993
The	The	k1gFnSc2	The
Complete	Comple	k1gNnSc2	Comple
Studio	studio	k1gNnSc1	studio
Recordings	Recordings	k1gInSc1	Recordings
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
CD	CD	kA	CD
<g/>
)	)	kIx)	)
1999	[number]	k4	1999
Early	earl	k1gMnPc4	earl
Days	Daysa	k1gFnPc2	Daysa
<g/>
:	:	kIx,	:
Best	Best	k1gInSc1	Best
of	of	k?	of
Led	led	k1gInSc1	led
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
Volume	volum	k1gInSc5	volum
One	One	k1gMnPc2	One
2000	[number]	k4	2000
Latter	Lattrum	k1gNnPc2	Lattrum
Days	Daysa	k1gFnPc2	Daysa
<g/>
:	:	kIx,	:
Best	Best	k1gInSc1	Best
of	of	k?	of
Led	led	k1gInSc1	led
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
Volume	volum	k1gInSc5	volum
Two	Two	k1gFnSc3	Two
2007	[number]	k4	2007
Mothership	Mothership	k1gInSc1	Mothership
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
CD	CD	kA	CD
<g/>
)	)	kIx)	)
2008	[number]	k4	2008
Definitive	Definitiv	k1gInSc5	Definitiv
Collection	Collection	k1gInSc1	Collection
Mini	mini	k2eAgNnSc2d1	mini
LP	LP	kA	LP
Replica	Replicum	k1gNnSc2	Replicum
CD	CD	kA	CD
Boxset	Boxset	k1gMnSc1	Boxset
</s>
