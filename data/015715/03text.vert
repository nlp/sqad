<s>
Anarchokolektivismus	Anarchokolektivismus	k1gInSc1
</s>
<s>
Anarchismus	anarchismus	k1gInSc1
</s>
<s>
Směry	směr	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
Agorismus	Agorismus	k1gInSc1
</s>
<s>
Anarchokapitalismus	Anarchokapitalismus	k1gInSc1
</s>
<s>
Anarchokomunismus	anarchokomunismus	k1gInSc1
</s>
<s>
Anarchoprimitivismus	Anarchoprimitivismus	k1gInSc1
</s>
<s>
Anarchosyndikalismus	anarchosyndikalismus	k1gInSc1
</s>
<s>
Bez	bez	k7c2
přívlastků	přívlastek	k1gInPc2
</s>
<s>
Buddhistický	buddhistický	k2eAgInSc1d1
</s>
<s>
Feministický	feministický	k2eAgInSc1d1
</s>
<s>
Filosofický	filosofický	k2eAgInSc1d1
</s>
<s>
Kolektivistický	kolektivistický	k2eAgInSc1d1
</s>
<s>
Individualistický	individualistický	k2eAgInSc1d1
</s>
<s>
Infoanarchismus	Infoanarchismus	k1gInSc1
</s>
<s>
Křesťanský	křesťanský	k2eAgInSc1d1
</s>
<s>
Mutualismus	mutualismus	k1gInSc1
</s>
<s>
Pacifistický	pacifistický	k2eAgInSc1d1
</s>
<s>
Povstalecký	povstalecký	k2eAgInSc1d1
</s>
<s>
Sociální	sociální	k2eAgFnSc1d1
</s>
<s>
Tržní	tržní	k2eAgInSc1d1
</s>
<s>
Zelený	zelený	k2eAgInSc1d1
</s>
<s>
z	z	k7c2
•	•	k?
d	d	k?
•	•	k?
e	e	k0
</s>
<s>
Anarchokolektivismus	Anarchokolektivismus	k1gInSc1
je	být	k5eAaImIp3nS
směr	směr	k1gInSc4
anarchismu	anarchismus	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
zastává	zastávat	k5eAaImIp3nS
zrušení	zrušení	k1gNnSc4
státu	stát	k1gInSc2
a	a	k8xC
soukromého	soukromý	k2eAgNnSc2d1
vlastnictví	vlastnictví	k1gNnSc2
výrobních	výrobní	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Namísto	namísto	k7c2
toho	ten	k3xDgInSc2
předpokládá	předpokládat	k5eAaImIp3nS
kolektivní	kolektivní	k2eAgNnSc4d1
vlastnictví	vlastnictví	k1gNnSc4
výrobních	výrobní	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
a	a	k8xC
jejich	jejich	k3xOp3gFnSc4
kontrolu	kontrola	k1gFnSc4
samotnými	samotný	k2eAgMnPc7d1
pracujícími	pracující	k2eAgMnPc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolektivistický	kolektivistický	k2eAgInSc1d1
anarchismus	anarchismus	k1gInSc1
je	být	k5eAaImIp3nS
nejčastěji	často	k6eAd3
spojován	spojován	k2eAgMnSc1d1
s	s	k7c7
Michailem	Michail	k1gMnSc7
Alexandrovičem	Alexandrovič	k1gMnSc7
Bakuninem	Bakunin	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Anarchokolektivismus	Anarchokolektivismus	k1gInSc1
kontrastuje	kontrastovat	k5eAaImIp3nS
s	s	k7c7
anarchokomunismem	anarchokomunismus	k1gInSc7
<g/>
,	,	kIx,
podle	podle	k7c2
kterého	který	k3yRgInSc2,k3yIgInSc2,k3yQgInSc2
by	by	kYmCp3nS
měly	mít	k5eAaImAgInP
být	být	k5eAaImF
zrušeny	zrušit	k5eAaPmNgInP
peníze	peníz	k1gInPc1
a	a	k8xC
tedy	tedy	k9
i	i	k9
mzdy	mzda	k1gFnPc1
a	a	k8xC
distribuce	distribuce	k1gFnSc1
statků	statek	k1gInPc2
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
probíhat	probíhat	k5eAaImF
každému	každý	k3xTgMnSc3
podle	podle	k7c2
jeho	jeho	k3xOp3gFnPc2
potřeb	potřeba	k1gFnPc2
<g/>
,	,	kIx,
kdežto	kdežto	k8xS
anarchokolektivismus	anarchokolektivismus	k1gInSc1
ponechává	ponechávat	k5eAaImIp3nS
způsob	způsob	k1gInSc4
distribuce	distribuce	k1gFnSc2
na	na	k7c6
volbě	volba	k1gFnSc6
každé	každý	k3xTgFnSc2
komunity	komunita	k1gFnSc2
zvlášť	zvlášť	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konečným	konečný	k2eAgInSc7d1
ideálem	ideál	k1gInSc7
obou	dva	k4xCgInPc2
směrů	směr	k1gInPc2
je	být	k5eAaImIp3nS
„	„	k?
<g/>
každý	každý	k3xTgInSc4
podle	podle	k7c2
svých	svůj	k3xOyFgFnPc2
schopností	schopnost	k1gFnPc2
<g/>
,	,	kIx,
každému	každý	k3xTgMnSc3
podle	podle	k7c2
jeho	jeho	k3xOp3gNnPc2
potřeb	potřeba	k1gFnPc2
<g/>
“	“	k?
<g/>
,	,	kIx,
ale	ale	k8xC
neshodují	shodovat	k5eNaImIp3nP
se	se	k3xPyFc4
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
rychle	rychle	k6eAd1
lze	lze	k6eAd1
tento	tento	k3xDgInSc4
ideál	ideál	k1gInSc4
uskutečnit	uskutečnit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
The	The	k1gMnSc1
Anarchist	Anarchist	k1gMnSc1
FAQ	FAQ	kA
Editorial	Editorial	k1gMnSc1
Collective	Collectiv	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
An	An	k1gMnSc1
Anarchist	Anarchist	k1gMnSc1
FAQ	FAQ	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2009-06-18	2009-06-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
A	A	kA
<g/>
.3	.3	k4
<g/>
.2	.2	k4
Are	ar	k1gInSc5
there	ther	k1gInSc5
different	different	k1gInSc1
types	types	k1gMnSc1
of	of	k?
social	social	k1gMnSc1
anarchism	anarchism	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
