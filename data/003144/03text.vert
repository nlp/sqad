<s>
Maršál	maršál	k1gMnSc1	maršál
(	(	kIx(	(
<g/>
v	v	k7c6	v
odvozené	odvozený	k2eAgFnSc6d1	odvozená
podobě	podoba	k1gFnSc6	podoba
i	i	k9	i
maršálek	maršálek	k1gMnSc1	maršálek
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
slovo	slovo	k1gNnSc1	slovo
používané	používaný	k2eAgNnSc1d1	používané
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
oficiálních	oficiální	k2eAgInPc2d1	oficiální
titulů	titul	k1gInPc2	titul
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
řadě	řada	k1gFnSc6	řada
organizací	organizace	k1gFnPc2	organizace
a	a	k8xC	a
společenských	společenský	k2eAgNnPc2d1	společenské
odvětví	odvětví	k1gNnPc2	odvětví
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
jeho	jeho	k3xOp3gNnSc4	jeho
použití	použití	k1gNnSc4	použití
v	v	k7c6	v
ozbrojených	ozbrojený	k2eAgFnPc6d1	ozbrojená
silách	síla	k1gFnPc6	síla
<g/>
;	;	kIx,	;
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
státech	stát	k1gInPc6	stát
představuje	představovat	k5eAaImIp3nS	představovat
(	(	kIx(	(
<g/>
polní	polní	k2eAgMnSc1d1	polní
<g/>
)	)	kIx)	)
maršál	maršál	k1gMnSc1	maršál
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
hodnost	hodnost	k1gFnSc4	hodnost
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
někdy	někdy	k6eAd1	někdy
pouze	pouze	k6eAd1	pouze
čestnou	čestný	k2eAgFnSc4d1	čestná
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
starohornoněmeckých	starohornoněmecký	k2eAgNnPc2d1	starohornoněmecké
slov	slovo	k1gNnPc2	slovo
marah	marah	k1gInSc1	marah
(	(	kIx(	(
<g/>
kůň	kůň	k1gMnSc1	kůň
<g/>
)	)	kIx)	)
a	a	k8xC	a
scalch	scalch	k1gMnSc1	scalch
(	(	kIx(	(
<g/>
služebník	služebník	k1gMnSc1	služebník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
původně	původně	k6eAd1	původně
znamenalo	znamenat	k5eAaImAgNnS	znamenat
vrchního	vrchní	k1gMnSc4	vrchní
štolbu	štolba	k1gMnSc4	štolba
(	(	kIx(	(
<g/>
správce	správce	k1gMnSc1	správce
stájí	stáj	k1gFnPc2	stáj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vrchními	vrchní	k2eAgMnPc7d1	vrchní
štolby	štolba	k1gMnPc7	štolba
byli	být	k5eAaImAgMnP	být
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
jmenováni	jmenován	k2eAgMnPc1d1	jmenován
důvěryhodní	důvěryhodný	k2eAgMnPc1d1	důvěryhodný
členové	člen	k1gMnPc1	člen
dvora	dvůr	k1gInSc2	dvůr
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
reputace	reputace	k1gFnSc1	reputace
jejich	jejich	k3xOp3gInSc2	jejich
titulu	titul	k1gInSc2	titul
a	a	k8xC	a
úřadu	úřad	k1gInSc2	úřad
stoupala	stoupat	k5eAaImAgFnS	stoupat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
posledních	poslední	k2eAgNnPc2d1	poslední
několika	několik	k4yIc2	několik
století	století	k1gNnPc2	století
se	se	k3xPyFc4	se
titul	titul	k1gInSc1	titul
maršál	maršál	k1gMnSc1	maršál
začal	začít	k5eAaPmAgMnS	začít
používat	používat	k5eAaImF	používat
na	na	k7c6	na
řadě	řada	k1gFnSc6	řada
postů	post	k1gInPc2	post
a	a	k8xC	a
hodností	hodnost	k1gFnPc2	hodnost
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
najít	najít	k5eAaPmF	najít
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
v	v	k7c6	v
například	například	k6eAd1	například
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
(	(	kIx(	(
<g/>
např.	např.	kA	např.
maršálek	maršálek	k1gMnSc1	maršálek
Sejmu	sejmout	k5eAaPmIp1nS	sejmout
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
u	u	k7c2	u
státní	státní	k2eAgFnSc2d1	státní
policie	policie	k1gFnSc2	policie
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
armády	armáda	k1gFnSc2	armáda
ne	ne	k9	ne
<g/>
.	.	kIx.	.
</s>
<s>
Hodnost	hodnost	k1gFnSc1	hodnost
polního	polní	k2eAgMnSc2d1	polní
maršála	maršál	k1gMnSc2	maršál
nebyla	být	k5eNaImAgFnS	být
v	v	k7c6	v
USA	USA	kA	USA
nikdy	nikdy	k6eAd1	nikdy
zavedena	zaveden	k2eAgFnSc1d1	zavedena
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
žádný	žádný	k3yNgInSc4	žádný
vojenský	vojenský	k2eAgMnSc1d1	vojenský
velitel	velitel	k1gMnSc1	velitel
nedosáhl	dosáhnout	k5eNaPmAgMnS	dosáhnout
vyšší	vysoký	k2eAgFnPc4d2	vyšší
hodnosti	hodnost	k1gFnPc4	hodnost
než	než	k8xS	než
George	Georg	k1gInPc4	Georg
Washington	Washington	k1gInSc1	Washington
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Na	na	k7c6	na
jejich	jejich	k3xOp3gNnSc6	jejich
místě	místo	k1gNnSc6	místo
působili	působit	k5eAaImAgMnP	působit
tzv.	tzv.	kA	tzv.
pětihvězdičkoví	pětihvězdičkový	k2eAgMnPc1d1	pětihvězdičkový
generálové	generál	k1gMnPc1	generál
a	a	k8xC	a
i	i	k8xC	i
těch	ten	k3xDgMnPc2	ten
bylo	být	k5eAaImAgNnS	být
jen	jen	k9	jen
několik	několik	k4yIc1	několik
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc4d1	jediná
výjimku	výjimka	k1gFnSc4	výjimka
tak	tak	k6eAd1	tak
představuje	představovat	k5eAaImIp3nS	představovat
Douglas	Douglas	k1gMnSc1	Douglas
MacArthur	MacArthur	k1gMnSc1	MacArthur
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
polním	polní	k2eAgMnSc7d1	polní
maršálem	maršál	k1gMnSc7	maršál
díky	díky	k7c3	díky
svému	svůj	k3xOyFgNnSc3	svůj
působení	působení	k1gNnSc3	působení
ve	v	k7c6	v
filipínské	filipínský	k2eAgFnSc6d1	filipínská
armádě	armáda	k1gFnSc6	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
česká	český	k2eAgFnSc1d1	Česká
armáda	armáda	k1gFnSc1	armáda
(	(	kIx(	(
<g/>
AČR	AČR	kA	AČR
<g/>
)	)	kIx)	)
žádnou	žádný	k3yNgFnSc4	žádný
maršálskou	maršálský	k2eAgFnSc4d1	maršálská
hodnost	hodnost	k1gFnSc4	hodnost
neužívá	užívat	k5eNaImIp3nS	užívat
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
v	v	k7c6	v
zaniklém	zaniklý	k2eAgNnSc6d1	zaniklé
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
vešlo	vejít	k5eAaPmAgNnS	vejít
několik	několik	k4yIc1	několik
Čechů	Čech	k1gMnPc2	Čech
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
tento	tento	k3xDgInSc4	tento
titul	titul	k1gInSc4	titul
získali	získat	k5eAaPmAgMnP	získat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
době	doba	k1gFnSc6	doba
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
vyznamenali	vyznamenat	k5eAaPmAgMnP	vyznamenat
celkem	celkem	k6eAd1	celkem
tři	tři	k4xCgMnPc1	tři
čeští	český	k2eAgMnPc1d1	český
polní	polní	k2eAgMnPc1d1	polní
maršálové	maršál	k1gMnPc1	maršál
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
vzpoury	vzpoura	k1gFnSc2	vzpoura
českých	český	k2eAgInPc2d1	český
stavů	stav	k1gInPc2	stav
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
celou	celý	k2eAgFnSc4d1	celá
třicetiletou	třicetiletý	k2eAgFnSc4d1	třicetiletá
válku	válka	k1gFnSc4	válka
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
jako	jako	k8xC	jako
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
velitel	velitel	k1gMnSc1	velitel
a	a	k8xC	a
polní	polní	k2eAgMnSc1d1	polní
maršál	maršál	k1gMnSc1	maršál
stavovského	stavovský	k2eAgNnSc2d1	Stavovské
vojska	vojsko	k1gNnSc2	vojsko
Linhart	Linhart	k1gMnSc1	Linhart
Colona	colon	k1gNnSc2	colon
z	z	k7c2	z
Felsu	Fels	k1gInSc2	Fels
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1620	[number]	k4	1620
po	po	k7c6	po
prohrané	prohraný	k2eAgFnSc6d1	prohraná
bitvě	bitva	k1gFnSc6	bitva
s	s	k7c7	s
císařskou	císařský	k2eAgFnSc7d1	císařská
armádou	armáda	k1gFnSc7	armáda
Karla	Karel	k1gMnSc2	Karel
Bonaventury	Bonaventura	k1gFnSc2	Bonaventura
Buquoye	Buquoy	k1gMnSc2	Buquoy
u	u	k7c2	u
Sinzendorfu	Sinzendorf	k1gInSc2	Sinzendorf
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Habsburků	Habsburk	k1gMnPc2	Habsburk
stál	stát	k5eAaImAgMnS	stát
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
Albrecht	Albrecht	k1gMnSc1	Albrecht
Václav	Václav	k1gMnSc1	Václav
Eusebius	Eusebius	k1gMnSc1	Eusebius
z	z	k7c2	z
Valdštejna	Valdštejn	k1gInSc2	Valdštejn
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
Jana	Jan	k1gMnSc2	Jan
Tserclaese	Tserclaese	k1gFnSc2	Tserclaese
Tillyho	Tilly	k1gMnSc2	Tilly
byl	být	k5eAaImAgMnS	být
polní	polní	k2eAgMnSc1d1	polní
maršál	maršál	k1gMnSc1	maršál
a	a	k8xC	a
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
velitel	velitel	k1gMnSc1	velitel
(	(	kIx(	(
<g/>
generalissimus	generalissimus	k1gMnSc1	generalissimus
<g/>
)	)	kIx)	)
císařských	císařský	k2eAgNnPc2d1	císařské
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
ovšem	ovšem	k9	ovšem
upadl	upadnout	k5eAaPmAgMnS	upadnout
u	u	k7c2	u
panovníka	panovník	k1gMnSc2	panovník
v	v	k7c4	v
nemilost	nemilost	k1gFnSc4	nemilost
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
úkladně	úkladně	k6eAd1	úkladně
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
v	v	k7c6	v
Chebu	Cheb	k1gInSc6	Cheb
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
měrou	míra	k1gFnSc7wR	míra
k	k	k7c3	k
záchraně	záchrana	k1gFnSc3	záchrana
Evropy	Evropa	k1gFnSc2	Evropa
před	před	k7c7	před
osmanským	osmanský	k2eAgNnSc7d1	osmanské
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
přispěl	přispět	k5eAaPmAgMnS	přispět
český	český	k2eAgMnSc1d1	český
polní	polní	k2eAgMnSc1d1	polní
maršálek	maršálek	k1gMnSc1	maršálek
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Kašpar	Kašpar	k1gMnSc1	Kašpar
Kaplíř	Kaplíř	k1gMnSc1	Kaplíř
ze	z	k7c2	z
Sulevic	Sulevice	k1gFnPc2	Sulevice
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
děd	děd	k1gMnSc1	děd
byl	být	k5eAaImAgMnS	být
Kašpar	Kašpar	k1gMnSc1	Kašpar
Kaplíř	Kaplíř	k1gMnSc1	Kaplíř
ze	z	k7c2	z
Sulevic	Sulevice	k1gFnPc2	Sulevice
-	-	kIx~	-
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
27	[number]	k4	27
českých	český	k2eAgMnPc2d1	český
pánů	pan	k1gMnPc2	pan
popravených	popravený	k2eAgInPc2d1	popravený
na	na	k7c6	na
Staroměstském	staroměstský	k2eAgNnSc6d1	Staroměstské
náměstí	náměstí	k1gNnSc6	náměstí
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1621	[number]	k4	1621
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
on	on	k3xPp3gMnSc1	on
byl	být	k5eAaImAgMnS	být
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Kašpar	Kašpar	k1gMnSc1	Kašpar
vyznáním	vyznání	k1gNnSc7	vyznání
původně	původně	k6eAd1	původně
evangelík	evangelík	k1gMnSc1	evangelík
a	a	k8xC	a
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
sloužil	sloužit	k5eAaImAgMnS	sloužit
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
ve	v	k7c6	v
vojskách	vojsko	k1gNnPc6	vojsko
Sasů	Sas	k1gMnPc2	Sas
a	a	k8xC	a
Švédů	Švéd	k1gMnPc2	Švéd
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
konvertoval	konvertovat	k5eAaBmAgMnS	konvertovat
ke	k	k7c3	k
katolictví	katolictví	k1gNnSc3	katolictví
a	a	k8xC	a
přeběhl	přeběhnout	k5eAaPmAgMnS	přeběhnout
k	k	k7c3	k
Habsburkům	Habsburk	k1gMnPc3	Habsburk
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
proslulost	proslulost	k1gFnSc1	proslulost
mu	on	k3xPp3gMnSc3	on
přinesla	přinést	k5eAaPmAgFnS	přinést
brilantní	brilantní	k2eAgFnSc1d1	brilantní
obrana	obrana	k1gFnSc1	obrana
Vídně	Vídeň	k1gFnSc2	Vídeň
roku	rok	k1gInSc2	rok
1683	[number]	k4	1683
před	před	k7c7	před
vojsky	vojsko	k1gNnPc7	vojsko
sultána	sultán	k1gMnSc2	sultán
Mehmeda	Mehmed	k1gMnSc2	Mehmed
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
vojevůdců	vojevůdce	k1gMnPc2	vojevůdce
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
i	i	k9	i
Josef	Josef	k1gMnSc1	Josef
Václav	Václav	k1gMnSc1	Václav
Antonín	Antonín	k1gMnSc1	Antonín
František	František	k1gMnSc1	František
Karel	Karel	k1gMnSc1	Karel
Radecký	Radecký	k1gMnSc1	Radecký
z	z	k7c2	z
Radče	Radč	k1gFnSc2	Radč
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
český	český	k2eAgInSc1d1	český
národní	národní	k2eAgInSc1d1	národní
folklor	folklor	k1gInSc1	folklor
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
tradice	tradice	k1gFnSc2	tradice
téměř	téměř	k6eAd1	téměř
vyloučil	vyloučit	k5eAaPmAgInS	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Češi	Čech	k1gMnPc1	Čech
snažili	snažit	k5eAaImAgMnP	snažit
vymanit	vymanit	k5eAaPmF	vymanit
se	se	k3xPyFc4	se
zpod	zpod	k7c2	zpod
rakouské	rakouský	k2eAgFnSc2d1	rakouská
nadvlády	nadvláda	k1gFnSc2	nadvláda
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
získával	získávat	k5eAaImAgMnS	získávat
Radecký	Radecký	k1gMnSc1	Radecký
pro	pro	k7c4	pro
Habsburky	Habsburk	k1gMnPc4	Habsburk
ty	ten	k3xDgInPc4	ten
největší	veliký	k2eAgInPc4d3	veliký
válečné	válečný	k2eAgInPc4d1	válečný
úspěchy	úspěch	k1gInPc4	úspěch
na	na	k7c6	na
italských	italský	k2eAgNnPc6d1	italské
bojištích	bojiště	k1gNnPc6	bojiště
během	během	k7c2	během
První	první	k4xOgFnSc2	první
italské	italský	k2eAgFnSc2d1	italská
války	válka	k1gFnSc2	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
vešel	vejít	k5eAaPmAgInS	vejít
i	i	k9	i
jeho	jeho	k3xOp3gInSc4	jeho
citát	citát	k1gInSc4	citát
na	na	k7c4	na
adresu	adresa	k1gFnSc4	adresa
obrozenců	obrozenec	k1gMnPc2	obrozenec
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Já	já	k3xPp1nSc1	já
jsem	být	k5eAaImIp1nS	být
také	také	k9	také
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikdo	nikdo	k3yNnSc1	nikdo
to	ten	k3xDgNnSc1	ten
nemusí	muset	k5eNaImIp3nS	muset
vědět	vědět	k5eAaImF	vědět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Československý	československý	k2eAgMnSc1d1	československý
maršál	maršál	k1gMnSc1	maršál
Karel	Karel	k1gMnSc1	Karel
Janoušek	Janoušek	k1gMnSc1	Janoušek
si	se	k3xPyFc3	se
hodnost	hodnost	k1gFnSc1	hodnost
Air	Air	k1gMnSc4	Air
Marshala	Marshal	k1gMnSc4	Marshal
vysloužil	vysloužit	k5eAaPmAgMnS	vysloužit
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
mu	on	k3xPp3gNnSc3	on
udělena	udělen	k2eAgNnPc4d1	uděleno
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1945	[number]	k4	1945
britským	britský	k2eAgMnSc7d1	britský
králem	král	k1gMnSc7	král
Jiřím	Jiří	k1gMnSc7	Jiří
VI	VI	kA	VI
<g/>
.	.	kIx.	.
za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
při	při	k7c6	při
formování	formování	k1gNnSc6	formování
československých	československý	k2eAgFnPc2d1	Československá
jednotek	jednotka	k1gFnPc2	jednotka
v	v	k7c6	v
RAF	raf	k0	raf
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
vyznamenal	vyznamenat	k5eAaPmAgInS	vyznamenat
i	i	k9	i
jako	jako	k9	jako
legionář	legionář	k1gMnSc1	legionář
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
frontě	fronta	k1gFnSc6	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
přese	přese	k7c4	přese
všechno	všechen	k3xTgNnSc4	všechen
ho	on	k3xPp3gNnSc4	on
nechal	nechat	k5eAaPmAgInS	nechat
komunistický	komunistický	k2eAgInSc1d1	komunistický
režim	režim	k1gInSc1	režim
dlouho	dlouho	k6eAd1	dlouho
věznit	věznit	k5eAaImF	věznit
<g/>
.	.	kIx.	.
</s>
<s>
Plného	plný	k2eAgNnSc2d1	plné
zadostiučinění	zadostiučinění	k1gNnSc2	zadostiučinění
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
dostalo	dostat	k5eAaPmAgNnS	dostat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
vrácena	vrácen	k2eAgFnSc1d1	vrácena
hodnost	hodnost	k1gFnSc1	hodnost
i	i	k8xC	i
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
posmrtně	posmrtně	k6eAd1	posmrtně
udělena	udělit	k5eAaPmNgFnS	udělit
i	i	k9	i
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
československá	československý	k2eAgFnSc1d1	Československá
vojenská	vojenský	k2eAgFnSc1d1	vojenská
hodnost	hodnost	k1gFnSc1	hodnost
armádního	armádní	k2eAgMnSc2d1	armádní
generála	generál	k1gMnSc2	generál
<g/>
.	.	kIx.	.
</s>
<s>
Polní	polní	k2eAgMnSc1d1	polní
maršál	maršál	k1gMnSc1	maršál
(	(	kIx(	(
<g/>
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
Generalfeldmarschall	Generalfeldmarschalla	k1gFnPc2	Generalfeldmarschalla
nebo	nebo	k8xC	nebo
Feldmarschall	Feldmarschalla	k1gFnPc2	Feldmarschalla
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
vojenská	vojenský	k2eAgFnSc1d1	vojenská
hodnost	hodnost	k1gFnSc1	hodnost
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
až	až	k9	až
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jejích	její	k3xOp3gMnPc2	její
následníků	následník	k1gMnPc2	následník
Rakouského	rakouský	k2eAgNnSc2d1	rakouské
císařství	císařství	k1gNnSc2	císařství
<g/>
,	,	kIx,	,
Pruského	pruský	k2eAgNnSc2d1	pruské
království	království	k1gNnSc2	království
a	a	k8xC	a
Německého	německý	k2eAgNnSc2d1	německé
císařství	císařství	k1gNnSc2	císařství
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
v	v	k7c6	v
Prusku	Prusko	k1gNnSc6	Prusko
se	se	k3xPyFc4	se
udělování	udělování	k1gNnSc2	udělování
této	tento	k3xDgFnSc2	tento
hodnosti	hodnost	k1gFnSc2	hodnost
řídilo	řídit	k5eAaImAgNnS	řídit
přísnými	přísný	k2eAgNnPc7d1	přísné
pravidly	pravidlo	k1gNnPc7	pravidlo
-	-	kIx~	-
mohl	moct	k5eAaImAgMnS	moct
ji	on	k3xPp3gFnSc4	on
získat	získat	k5eAaPmF	získat
pouze	pouze	k6eAd1	pouze
polní	polní	k2eAgMnSc1d1	polní
velitel	velitel	k1gMnSc1	velitel
v	v	k7c6	v
době	doba	k1gFnSc6	doba
války	válka	k1gFnSc2	válka
za	za	k7c4	za
vítězství	vítězství	k1gNnSc4	vítězství
v	v	k7c6	v
rozhodující	rozhodující	k2eAgFnSc6d1	rozhodující
bitvě	bitva	k1gFnSc6	bitva
nebo	nebo	k8xC	nebo
za	za	k7c4	za
dobytí	dobytí	k1gNnSc4	dobytí
významné	významný	k2eAgFnSc2d1	významná
pevnosti	pevnost	k1gFnSc2	pevnost
či	či	k8xC	či
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Hodnost	hodnost	k1gFnSc1	hodnost
zároveň	zároveň	k6eAd1	zároveň
svému	svůj	k3xOyFgMnSc3	svůj
nositeli	nositel	k1gMnSc3	nositel
garantovala	garantovat	k5eAaBmAgFnS	garantovat
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
výjimečných	výjimečný	k2eAgNnPc2d1	výjimečné
privilegií	privilegium	k1gNnPc2	privilegium
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
třeba	třeba	k6eAd1	třeba
automatické	automatický	k2eAgNnSc4d1	automatické
povýšení	povýšení	k1gNnSc4	povýšení
do	do	k7c2	do
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
členství	členství	k1gNnSc4	členství
v	v	k7c6	v
kabinetu	kabinet	k1gInSc6	kabinet
(	(	kIx(	(
<g/>
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
vládního	vládní	k2eAgMnSc4d1	vládní
ministra	ministr	k1gMnSc4	ministr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
právo	právo	k1gNnSc4	právo
přímého	přímý	k2eAgInSc2d1	přímý
přístupu	přístup	k1gInSc2	přístup
ke	k	k7c3	k
králi	král	k1gMnSc3	král
a	a	k8xC	a
císaři	císař	k1gMnSc3	císař
a	a	k8xC	a
nárok	nárok	k1gInSc4	nárok
na	na	k7c4	na
čestnou	čestný	k2eAgFnSc4d1	čestná
stráž	stráž	k1gFnSc4	stráž
a	a	k8xC	a
eskortu	eskorta	k1gFnSc4	eskorta
<g/>
.	.	kIx.	.
</s>
<s>
Polní	polní	k2eAgMnPc1d1	polní
maršálové	maršál	k1gMnPc1	maršál
Třetí	třetí	k4xOgFnPc4	třetí
říše	říš	k1gFnPc4	říš
Maršálové	maršál	k1gMnPc1	maršál
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
Maršál	maršál	k1gMnSc1	maršál
Polska	Polsko	k1gNnSc2	Polsko
Maršál	maršál	k1gMnSc1	maršál
Francie	Francie	k1gFnSc2	Francie
Josip	Josip	k1gMnSc1	Josip
Broz	Broza	k1gFnPc2	Broza
Tito	tento	k3xDgMnPc1	tento
Kim	Kim	k1gMnPc1	Kim
Čong-un	Čongn	k1gMnSc1	Čong-un
Konstábl	konstábl	k1gMnSc1	konstábl
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Maršál	maršál	k1gMnSc1	maršál
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
polní	polní	k2eAgMnSc1d1	polní
maršál	maršál	k1gMnSc1	maršál
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
