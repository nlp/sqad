<s>
Barva	barva	k1gFnSc1	barva
kouzel	kouzlo	k1gNnPc2	kouzlo
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
The	The	k1gMnSc1	The
Colour	Colour	k1gMnSc1	Colour
of	of	k?	of
Magic	Magic	k1gMnSc1	Magic
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
humoristická	humoristický	k2eAgFnSc1d1	humoristická
fantasy	fantas	k1gInPc1	fantas
kniha	kniha	k1gFnSc1	kniha
Terryho	Terry	k1gMnSc4	Terry
Pratchetta	Pratchett	k1gMnSc4	Pratchett
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
ze	z	k7c2	z
série	série	k1gFnSc2	série
Zeměplocha	Zeměploch	k1gMnSc2	Zeměploch
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
napsána	napsat	k5eAaBmNgFnS	napsat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jan	Jan	k1gMnSc1	Jan
Kantůrek	kantůrek	k1gMnSc1	kantůrek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
byla	být	k5eAaImAgFnS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
knihou	kniha	k1gFnSc7	kniha
z	z	k7c2	z
hlavní	hlavní	k2eAgFnSc2d1	hlavní
série	série	k1gFnSc2	série
(	(	kIx(	(
<g/>
další	další	k2eAgFnSc1d1	další
byla	být	k5eAaImAgFnS	být
až	až	k9	až
Zaslaná	zaslaný	k2eAgFnSc1d1	zaslaná
pošta	pošta	k1gFnSc1	pošta
<g/>
)	)	kIx)	)
rozdělenou	rozdělená	k1gFnSc4	rozdělená
do	do	k7c2	do
kapitol	kapitola	k1gFnPc2	kapitola
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
kapitola	kapitola	k1gFnSc1	kapitola
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
povídka	povídka	k1gFnSc1	povídka
<g/>
,	,	kIx,	,
pokaždé	pokaždé	k6eAd1	pokaždé
se	s	k7c7	s
stejnými	stejný	k2eAgMnPc7d1	stejný
hrdiny	hrdina	k1gMnPc7	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenka	myšlenka	k1gFnSc1	myšlenka
Barvy	barva	k1gFnSc2	barva
kouzel	kouzlo	k1gNnPc2	kouzlo
-	-	kIx~	-
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vše	všechen	k3xTgNnSc1	všechen
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
jen	jen	k9	jen
hra	hra	k1gFnSc1	hra
bohů	bůh	k1gMnPc2	bůh
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
podobná	podobný	k2eAgFnSc1d1	podobná
tradičním	tradiční	k2eAgMnSc6d1	tradiční
stolním	stolní	k2eAgInSc6d1	stolní
RPG	RPG	kA	RPG
hrám	hra	k1gFnPc3	hra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hod	hod	k1gInSc1	hod
kostkou	kostka	k1gFnSc7	kostka
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
<g/>
,	,	kIx,	,
co	co	k9	co
se	se	k3xPyFc4	se
hráčům	hráč	k1gMnPc3	hráč
stane	stanout	k5eAaPmIp3nS	stanout
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
hrdinou	hrdina	k1gMnSc7	hrdina
je	být	k5eAaImIp3nS	být
neschopný	schopný	k2eNgMnSc1d1	neschopný
a	a	k8xC	a
cynický	cynický	k2eAgMnSc1d1	cynický
mág	mág	k1gMnSc1	mág
Mrakoplaš	Mrakoplaš	k1gMnSc1	Mrakoplaš
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
nedobrovolně	dobrovolně	k6eNd1	dobrovolně
stane	stanout	k5eAaPmIp3nS	stanout
průvodcem	průvodce	k1gMnSc7	průvodce
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
turisty	turist	k1gMnPc4	turist
jménem	jméno	k1gNnSc7	jméno
Dvoukvítek	Dvoukvítka	k1gFnPc2	Dvoukvítka
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
připluje	připlout	k5eAaPmIp3nS	připlout
do	do	k7c2	do
Ankh-Morporku	Ankh-Morporek	k1gInSc2	Ankh-Morporek
z	z	k7c2	z
Agateánské	Agateánský	k2eAgFnSc2d1	Agateánský
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
způsobí	způsobit	k5eAaPmIp3nS	způsobit
zde	zde	k6eAd1	zde
velký	velký	k2eAgInSc1d1	velký
rozruch	rozruch	k1gInSc1	rozruch
svým	svůj	k3xOyFgNnSc7	svůj
obrovským	obrovský	k2eAgNnSc7d1	obrovské
bohatstvím	bohatství	k1gNnSc7	bohatství
<g/>
,	,	kIx,	,
kterého	který	k3yQgNnSc2	který
si	se	k3xPyFc3	se
není	být	k5eNaImIp3nS	být
vědom	vědom	k2eAgMnSc1d1	vědom
(	(	kIx(	(
<g/>
cena	cena	k1gFnSc1	cena
zlata	zlato	k1gNnSc2	zlato
v	v	k7c6	v
Ankh-Morporku	Ankh-Morporek	k1gInSc6	Ankh-Morporek
a	a	k8xC	a
v	v	k7c6	v
Agateánské	Agateánský	k2eAgFnSc6d1	Agateánský
říši	říš	k1gFnSc6	říš
se	se	k3xPyFc4	se
řádově	řádově	k6eAd1	řádově
liší	lišit	k5eAaImIp3nP	lišit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
rozsáhlého	rozsáhlý	k2eAgInSc2d1	rozsáhlý
požáru	požár	k1gInSc2	požár
<g/>
,	,	kIx,	,
založeného	založený	k2eAgInSc2d1	založený
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k8xS	co
Dvoukvítek	Dvoukvítka	k1gFnPc2	Dvoukvítka
objasní	objasnit	k5eAaPmIp3nS	objasnit
obyvatelům	obyvatel	k1gMnPc3	obyvatel
Morporku	Morporka	k1gFnSc4	Morporka
princip	princip	k1gInSc1	princip
pojištění	pojištění	k1gNnSc2	pojištění
<g/>
,	,	kIx,	,
opouštějí	opouštět	k5eAaImIp3nP	opouštět
oba	dva	k4xCgMnPc1	dva
hlavní	hlavní	k2eAgMnPc1d1	hlavní
hrdinové	hrdina	k1gMnPc1	hrdina
společně	společně	k6eAd1	společně
Ankh-Morpork	Ankh-Morpork	k1gInSc4	Ankh-Morpork
a	a	k8xC	a
vydávají	vydávat	k5eAaPmIp3nP	vydávat
se	se	k3xPyFc4	se
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
po	po	k7c4	po
Zeměploše	Zeměploš	k1gMnPc4	Zeměploš
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	on	k3xPp3gInPc4	on
zavede	zavést	k5eAaPmIp3nS	zavést
postupně	postupně	k6eAd1	postupně
do	do	k7c2	do
chrámu	chrám	k1gInSc2	chrám
pekelného	pekelný	k2eAgMnSc4d1	pekelný
boha	bůh	k1gMnSc4	bůh
Bel-Shammharotha	Bel-Shammharoth	k1gMnSc4	Bel-Shammharoth
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
jezdce	jezdec	k1gInPc4	jezdec
na	na	k7c6	na
dracích	drak	k1gInPc6	drak
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
do	do	k7c2	do
říše	říš	k1gFnSc2	říš
Krull	Krull	k1gInSc1	Krull
ležící	ležící	k2eAgInSc1d1	ležící
na	na	k7c6	na
samém	samý	k3xTgInSc6	samý
okraji	okraj	k1gInSc6	okraj
Zeměplochy	Zeměploch	k1gInPc1	Zeměploch
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
knihy	kniha	k1gFnSc2	kniha
končí	končit	k5eAaImIp3nS	končit
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
oba	dva	k4xCgInPc4	dva
dva	dva	k4xCgInPc4	dva
vrženi	vržen	k2eAgMnPc1d1	vržen
přes	přes	k7c4	přes
okraj	okraj	k1gInSc4	okraj
Zeměplochy	Zeměploch	k1gInPc4	Zeměploch
do	do	k7c2	do
nekonečného	konečný	k2eNgInSc2d1	nekonečný
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Příběhy	příběh	k1gInPc1	příběh
Mrakoplaše	Mrakoplaše	k1gFnSc2	Mrakoplaše
a	a	k8xC	a
Dvoukvítka	Dvoukvítko	k1gNnSc2	Dvoukvítko
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
v	v	k7c6	v
další	další	k2eAgFnSc6d1	další
knize	kniha	k1gFnSc6	kniha
ze	z	k7c2	z
série	série	k1gFnSc2	série
o	o	k7c4	o
Zeměploše	Zeměploš	k1gMnPc4	Zeměploš
-	-	kIx~	-
v	v	k7c6	v
Lehkém	lehký	k2eAgNnSc6d1	lehké
fantastičnu	fantastično	k1gNnSc6	fantastično
<g/>
.	.	kIx.	.
</s>
<s>
Knihou	kniha	k1gFnSc7	kniha
se	se	k3xPyFc4	se
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
počítačová	počítačový	k2eAgFnSc1d1	počítačová
hra	hra	k1gFnSc1	hra
The	The	k1gMnSc1	The
Colour	Colour	k1gMnSc1	Colour
of	of	k?	of
Magic	Magic	k1gMnSc1	Magic
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
byla	být	k5eAaImAgFnS	být
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
následujícím	následující	k2eAgInSc7d1	následující
dílem	díl	k1gInSc7	díl
<g/>
,	,	kIx,	,
zfilmována	zfilmován	k2eAgFnSc1d1	zfilmována
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
Barva	barva	k1gFnSc1	barva
kouzel	kouzlo	k1gNnPc2	kouzlo
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
Zeměplocha	Zeměploch	k1gMnSc4	Zeměploch
<g/>
.	.	kIx.	.
<g/>
info	info	k1gMnSc1	info
DiscWorld	DiscWorld	k1gMnSc1	DiscWorld
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Ankh	Ankh	k1gInSc1	Ankh
<g/>
.	.	kIx.	.
<g/>
ic	ic	k?	ic
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Oktarína	Oktarína	k1gFnSc1	Oktarína
anglicky	anglicky	k6eAd1	anglicky
na	na	k7c4	na
L-Space	L-Space	k1gFnPc4	L-Space
Web	web	k1gInSc1	web
<g/>
:	:	kIx,	:
Podrobný	podrobný	k2eAgInSc1d1	podrobný
obsah	obsah	k1gInSc1	obsah
Anotace	anotace	k1gFnSc1	anotace
(	(	kIx(	(
<g/>
vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
různých	různý	k2eAgFnPc2d1	různá
narážek	narážka	k1gFnPc2	narážka
v	v	k7c6	v
textu	text	k1gInSc6	text
<g/>
)	)	kIx)	)
</s>
