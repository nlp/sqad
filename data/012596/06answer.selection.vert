<s>
Adiktologie	Adiktologie	k1gFnSc1	Adiktologie
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
addictology	addictolog	k1gMnPc4	addictolog
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
věda	věda	k1gFnSc1	věda
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	s	k7c7	s
závislostmi	závislost	k1gFnPc7	závislost
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc7	jejich
prevencí	prevence	k1gFnSc7	prevence
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
<g/>
,	,	kIx,	,
léčbou	léčba	k1gFnSc7	léčba
<g/>
,	,	kIx,	,
výzkumem	výzkum	k1gInSc7	výzkum
<g/>
,	,	kIx,	,
poradenstvím	poradenství	k1gNnSc7	poradenství
a	a	k8xC	a
jinými	jiný	k2eAgFnPc7d1	jiná
souvislostmi	souvislost	k1gFnPc7	souvislost
<g/>
.	.	kIx.	.
</s>
