<p>
<s>
Adiktologie	Adiktologie	k1gFnSc1	Adiktologie
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
addictology	addictolog	k1gMnPc4	addictolog
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
věda	věda	k1gFnSc1	věda
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	s	k7c7	s
závislostmi	závislost	k1gFnPc7	závislost
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc7	jejich
prevencí	prevence	k1gFnSc7	prevence
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
<g/>
,	,	kIx,	,
léčbou	léčba	k1gFnSc7	léčba
<g/>
,	,	kIx,	,
výzkumem	výzkum	k1gInSc7	výzkum
<g/>
,	,	kIx,	,
poradenstvím	poradenství	k1gNnSc7	poradenství
a	a	k8xC	a
jinými	jiný	k2eAgFnPc7d1	jiná
souvislostmi	souvislost	k1gFnPc7	souvislost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
užším	úzký	k2eAgNnSc6d2	užší
pojetí	pojetí	k1gNnSc6	pojetí
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
závislostmi	závislost	k1gFnPc7	závislost
na	na	k7c6	na
návykových	návykový	k2eAgFnPc6d1	návyková
látkách	látka	k1gFnPc6	látka
<g/>
.	.	kIx.	.
</s>
<s>
Klinická	klinický	k2eAgFnSc1d1	klinická
adiktologie	adiktologie	k1gFnSc1	adiktologie
se	se	k3xPyFc4	se
zužuje	zužovat	k5eAaImIp3nS	zužovat
na	na	k7c4	na
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
klientem	klient	k1gMnSc7	klient
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
a	a	k8xC	a
vznik	vznik	k1gInSc1	vznik
slova	slovo	k1gNnSc2	slovo
==	==	k?	==
</s>
</p>
<p>
<s>
Latina	latina	k1gFnSc1	latina
<g/>
:	:	kIx,	:
Ad-dico	Adico	k6eAd1	Ad-dico
(	(	kIx(	(
<g/>
přiřknout	přiřknout	k5eAaPmF	přiřknout
<g/>
,	,	kIx,	,
vydávat	vydávat	k5eAaImF	vydávat
<g/>
)	)	kIx)	)
-	-	kIx~	-
<g/>
>	>	kIx)	>
addictus	addictus	k1gInSc1	addictus
(	(	kIx(	(
<g/>
závislá	závislý	k2eAgFnSc1d1	závislá
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
otrok	otrok	k1gMnSc1	otrok
<g/>
)	)	kIx)	)
<g/>
Angličtina	angličtina	k1gFnSc1	angličtina
<g/>
:	:	kIx,	:
addicted	addicted	k1gInSc1	addicted
(	(	kIx(	(
<g/>
závislý	závislý	k2eAgMnSc1d1	závislý
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
addiction	addiction	k1gInSc1	addiction
(	(	kIx(	(
<g/>
závislost	závislost	k1gFnSc1	závislost
<g/>
,	,	kIx,	,
sklon	sklon	k1gInSc1	sklon
<g/>
)	)	kIx)	)
<g/>
Adiktologie	Adiktologie	k1gFnSc1	Adiktologie
=	=	kIx~	=
ad	ad	k7c4	ad
<g/>
(	(	kIx(	(
<g/>
d	d	k?	d
<g/>
)	)	kIx)	)
<g/>
ico	ico	k?	ico
+	+	kIx~	+
logia	logia	k1gFnSc1	logia
=	=	kIx~	=
věda	věda	k1gFnSc1	věda
o	o	k7c6	o
závislostech	závislost	k1gFnPc6	závislost
</s>
</p>
<p>
<s>
==	==	k?	==
Definice	definice	k1gFnSc2	definice
závislosti	závislost	k1gFnSc2	závislost
==	==	k?	==
</s>
</p>
<p>
<s>
Závislostí	závislost	k1gFnSc7	závislost
je	být	k5eAaImIp3nS	být
míněno	mínit	k5eAaImNgNnS	mínit
cokoliv	cokoliv	k3yInSc4	cokoliv
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
je	být	k5eAaImIp3nS	být
závislostním	závislostní	k2eAgInSc7d1	závislostní
vztahem	vztah	k1gInSc7	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
to	ten	k3xDgNnSc1	ten
tedy	tedy	k9	tedy
široké	široký	k2eAgNnSc4d1	široké
spektrum	spektrum	k1gNnSc4	spektrum
<g/>
,	,	kIx,	,
a	a	k8xC	a
adiktologie	adiktologie	k1gFnSc1	adiktologie
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
zabývá	zabývat	k5eAaImIp3nS	zabývat
závislostmi	závislost	k1gFnPc7	závislost
na	na	k7c6	na
alkoholu	alkohol	k1gInSc6	alkohol
<g/>
,	,	kIx,	,
drogách	droga	k1gFnPc6	droga
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
hracích	hrací	k2eAgInPc6d1	hrací
automatech	automat	k1gInPc6	automat
či	či	k8xC	či
zavislostními	zavislostní	k2eAgInPc7d1	zavislostní
partnerskými	partnerský	k2eAgInPc7d1	partnerský
vztahy	vztah	k1gInPc7	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Spojuje	spojovat	k5eAaImIp3nS	spojovat
lidi	člověk	k1gMnPc4	člověk
s	s	k7c7	s
různorodou	různorodý	k2eAgFnSc7d1	různorodá
primární	primární	k2eAgFnSc7d1	primární
odborností	odbornost	k1gFnSc7	odbornost
a	a	k8xC	a
její	její	k3xOp3gFnSc7	její
základní	základní	k2eAgFnSc7d1	základní
otázkou	otázka	k1gFnSc7	otázka
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
lze	lze	k6eAd1	lze
odhalit	odhalit	k5eAaPmF	odhalit
<g/>
,	,	kIx,	,
porozumět	porozumět	k5eAaPmF	porozumět
a	a	k8xC	a
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
příčiny	příčina	k1gFnPc4	příčina
a	a	k8xC	a
následky	následek	k1gInPc4	následek
vzniku	vznik	k1gInSc2	vznik
a	a	k8xC	a
rozvoje	rozvoj	k1gInSc2	rozvoj
návykového	návykový	k2eAgNnSc2d1	návykové
chování	chování	k1gNnSc2	chování
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
může	moct	k5eAaImIp3nS	moct
ústit	ústit	k5eAaImF	ústit
až	až	k9	až
do	do	k7c2	do
závislosti	závislost	k1gFnSc2	závislost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Definice	definice	k1gFnSc2	definice
závislosti	závislost	k1gFnSc2	závislost
MKN-10	MKN-10	k1gFnSc2	MKN-10
===	===	k?	===
</s>
</p>
<p>
<s>
Syndrom	syndrom	k1gInSc1	syndrom
závislosti	závislost	k1gFnSc2	závislost
<g/>
:	:	kIx,	:
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
skupina	skupina	k1gFnSc1	skupina
fyziologických	fyziologický	k2eAgMnPc2d1	fyziologický
<g/>
,	,	kIx,	,
behaviorálních	behaviorální	k2eAgInPc2d1	behaviorální
a	a	k8xC	a
kognitivních	kognitivní	k2eAgInPc2d1	kognitivní
fenoménů	fenomén	k1gInPc2	fenomén
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
užívání	užívání	k1gNnSc1	užívání
nějaké	nějaký	k3yIgFnSc2	nějaký
látky	látka	k1gFnSc2	látka
nebo	nebo	k8xC	nebo
třídy	třída	k1gFnSc2	třída
látek	látka	k1gFnPc2	látka
má	mít	k5eAaImIp3nS	mít
u	u	k7c2	u
daného	daný	k2eAgMnSc2d1	daný
jedince	jedinec	k1gMnSc2	jedinec
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc1d2	veliký
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
než	než	k8xS	než
jiné	jiný	k2eAgNnSc1d1	jiné
jednání	jednání	k1gNnSc1	jednání
<g/>
,	,	kIx,	,
kterého	který	k3yQgNnSc2	který
si	se	k3xPyFc3	se
kdysi	kdysi	k6eAd1	kdysi
cenil	cenit	k5eAaImAgMnS	cenit
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Centrální	centrální	k2eAgFnSc7d1	centrální
popisnou	popisný	k2eAgFnSc7d1	popisná
charakteristikou	charakteristika	k1gFnSc7	charakteristika
syndromu	syndrom	k1gInSc2	syndrom
závislosti	závislost	k1gFnSc2	závislost
je	být	k5eAaImIp3nS	být
touha	touha	k1gFnSc1	touha
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
silná	silný	k2eAgFnSc1d1	silná
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
přemáhající	přemáhající	k2eAgInPc1d1	přemáhající
<g/>
)	)	kIx)	)
brát	brát	k5eAaImF	brát
psychoaktivní	psychoaktivní	k2eAgFnPc4d1	psychoaktivní
látky	látka	k1gFnPc4	látka
(	(	kIx(	(
<g/>
které	který	k3yQgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
lékařsky	lékařsky	k6eAd1	lékařsky
předepsány	předepsán	k2eAgInPc4d1	předepsán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
alkohol	alkohol	k1gInSc4	alkohol
nebo	nebo	k8xC	nebo
tabák	tabák	k1gInSc4	tabák
<g/>
.	.	kIx.	.
</s>
<s>
Návrat	návrat	k1gInSc1	návrat
k	k	k7c3	k
užívání	užívání	k1gNnSc3	užívání
látky	látka	k1gFnSc2	látka
po	po	k7c6	po
období	období	k1gNnSc6	období
abstinence	abstinence	k1gFnSc2	abstinence
často	často	k6eAd1	často
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
rychlejšímu	rychlý	k2eAgNnSc3d2	rychlejší
znovuobjevení	znovuobjevení	k1gNnSc3	znovuobjevení
jiných	jiný	k2eAgInPc2d1	jiný
rysů	rys	k1gInPc2	rys
syndromu	syndrom	k1gInSc2	syndrom
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
jedinců	jedinec	k1gMnPc2	jedinec
u	u	k7c2	u
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
závislost	závislost	k1gFnSc1	závislost
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Definitivní	definitivní	k2eAgFnSc1d1	definitivní
diagnóza	diagnóza	k1gFnSc1	diagnóza
závislostí	závislost	k1gFnPc2	závislost
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
měla	mít	k5eAaImAgFnS	mít
stanovit	stanovit	k5eAaPmF	stanovit
pouze	pouze	k6eAd1	pouze
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
během	během	k7c2	během
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
třem	tři	k4xCgFnPc3	tři
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
z	z	k7c2	z
následujících	následující	k2eAgInPc2d1	následující
jevů	jev	k1gInPc2	jev
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
silná	silný	k2eAgFnSc1d1	silná
touha	touha	k1gFnSc1	touha
nebo	nebo	k8xC	nebo
pocit	pocit	k1gInSc1	pocit
puzení	puzení	k1gNnSc4	puzení
užívat	užívat	k5eAaImF	užívat
látku	látka	k1gFnSc4	látka
</s>
</p>
<p>
<s>
potíže	potíž	k1gFnPc1	potíž
v	v	k7c6	v
sebeovládání	sebeovládání	k1gNnSc6	sebeovládání
</s>
</p>
<p>
<s>
tělesný	tělesný	k2eAgInSc4d1	tělesný
odvykací	odvykací	k2eAgInSc4d1	odvykací
stav	stav	k1gInSc4	stav
</s>
</p>
<p>
<s>
průkaz	průkaz	k1gInSc4	průkaz
tolerance	tolerance	k1gFnSc2	tolerance
k	k	k7c3	k
účinku	účinek	k1gInSc3	účinek
látky	látka	k1gFnSc2	látka
</s>
</p>
<p>
<s>
postupné	postupný	k2eAgNnSc4d1	postupné
zanedbávání	zanedbávání	k1gNnSc4	zanedbávání
jiných	jiný	k2eAgNnPc2d1	jiné
potěšení	potěšení	k1gNnPc2	potěšení
nebo	nebo	k8xC	nebo
zájmů	zájem	k1gInPc2	zájem
</s>
</p>
<p>
<s>
pokračování	pokračování	k1gNnSc1	pokračování
užívání	užívání	k1gNnSc2	užívání
přes	přes	k7c4	přes
jasný	jasný	k2eAgInSc4d1	jasný
důkaz	důkaz	k1gInSc4	důkaz
zjevně	zjevně	k6eAd1	zjevně
škodlivých	škodlivý	k2eAgInPc2d1	škodlivý
následků	následek	k1gInPc2	následek
<g/>
"	"	kIx"	"
<g/>
Jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
druhy	druh	k1gInPc4	druh
závislostí	závislost	k1gFnPc2	závislost
s	s	k7c7	s
jejich	jejich	k3xOp3gInSc7	jejich
kódem	kód	k1gInSc7	kód
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
F	F	kA	F
<g/>
10.2	[number]	k4	10.2
Závislost	závislost	k1gFnSc1	závislost
na	na	k7c6	na
alkoholu	alkohol	k1gInSc6	alkohol
</s>
</p>
<p>
<s>
F	F	kA	F
<g/>
11.2	[number]	k4	11.2
Závislost	závislost	k1gFnSc1	závislost
na	na	k7c6	na
opioidech	opioid	k1gInPc6	opioid
(	(	kIx(	(
<g/>
např.	např.	kA	např.
heroin	heroin	k1gInSc1	heroin
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
F	F	kA	F
<g/>
12.2	[number]	k4	12.2
Závislost	závislost	k1gFnSc1	závislost
na	na	k7c6	na
kanabinoidech	kanabinoid	k1gInPc6	kanabinoid
</s>
</p>
<p>
<s>
F	F	kA	F
<g/>
13.2	[number]	k4	13.2
Závislost	závislost	k1gFnSc1	závislost
na	na	k7c6	na
sedativech	sedativum	k1gNnPc6	sedativum
nebo	nebo	k8xC	nebo
hypnoticích	hypnotice	k1gFnPc6	hypnotice
</s>
</p>
<p>
<s>
F	F	kA	F
<g/>
14.2	[number]	k4	14.2
Závislost	závislost	k1gFnSc1	závislost
na	na	k7c6	na
kokainu	kokain	k1gInSc6	kokain
</s>
</p>
<p>
<s>
F	F	kA	F
<g/>
15.2	[number]	k4	15.2
Závislost	závislost	k1gFnSc1	závislost
na	na	k7c6	na
jiných	jiný	k2eAgFnPc6d1	jiná
stimulanciích	stimulancie	k1gFnPc6	stimulancie
včetně	včetně	k7c2	včetně
kofeinu	kofein	k1gInSc2	kofein
a	a	k8xC	a
pervitinu	pervitin	k1gInSc2	pervitin
</s>
</p>
<p>
<s>
F	F	kA	F
<g/>
16.2	[number]	k4	16.2
Závislost	závislost	k1gFnSc1	závislost
na	na	k7c6	na
halucinogenech	halucinogen	k1gInPc6	halucinogen
(	(	kIx(	(
<g/>
např.	např.	kA	např.
MDMA	MDMA	kA	MDMA
(	(	kIx(	(
<g/>
extáze	extáze	k1gFnSc1	extáze
<g/>
))	))	k?	))
</s>
</p>
<p>
<s>
F	F	kA	F
<g/>
17.2	[number]	k4	17.2
Závislost	závislost	k1gFnSc1	závislost
na	na	k7c6	na
tabáku	tabák	k1gInSc6	tabák
</s>
</p>
<p>
<s>
F	F	kA	F
<g/>
18.2	[number]	k4	18.2
Závislost	závislost	k1gFnSc1	závislost
na	na	k7c6	na
organických	organický	k2eAgNnPc6d1	organické
rozpouštědlech	rozpouštědlo	k1gNnPc6	rozpouštědlo
</s>
</p>
<p>
<s>
F	F	kA	F
<g/>
19.2	[number]	k4	19.2
Závislost	závislost	k1gFnSc1	závislost
na	na	k7c6	na
několika	několik	k4yIc6	několik
látkách	látka	k1gFnPc6	látka
nebo	nebo	k8xC	nebo
jiných	jiný	k2eAgFnPc6d1	jiná
psychoaktivních	psychoaktivní	k2eAgFnPc6d1	psychoaktivní
látkách	látka	k1gFnPc6	látka
</s>
</p>
<p>
<s>
Avšak	avšak	k8xC	avšak
nelze	lze	k6eNd1	lze
vyloučit	vyloučit	k5eAaPmF	vyloučit
i	i	k9	i
závislost	závislost	k1gFnSc4	závislost
na	na	k7c4	na
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
internetu	internet	k1gInSc2	internet
<g/>
,	,	kIx,	,
patologické	patologický	k2eAgNnSc1d1	patologické
hráčství	hráčství	k1gNnSc1	hráčství
<g/>
,	,	kIx,	,
či	či	k8xC	či
PPP	PPP	kA	PPP
ze	z	k7c2	z
závislostí	závislost	k1gFnPc2	závislost
úplně	úplně	k6eAd1	úplně
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
mají	mít	k5eAaImIp3nP	mít
některé	některý	k3yIgInPc4	některý
společné	společný	k2eAgInPc4d1	společný
rysy	rys	k1gInPc4	rys
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
úplnost	úplnost	k1gFnSc4	úplnost
tedy	tedy	k8xC	tedy
doplníme	doplnit	k5eAaPmIp1nP	doplnit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
F50	F50	k4	F50
Poruchy	porucha	k1gFnPc4	porucha
příjmu	příjem	k1gInSc2	příjem
potravy	potrava	k1gFnSc2	potrava
</s>
</p>
<p>
<s>
F	F	kA	F
<g/>
52.7	[number]	k4	52.7
Nadměrné	nadměrný	k2eAgFnSc2d1	nadměrná
sexuální	sexuální	k2eAgFnSc2d1	sexuální
nutkání	nutkání	k1gNnSc4	nutkání
</s>
</p>
<p>
<s>
F	F	kA	F
<g/>
60.7	[number]	k4	60.7
Závislá	závislý	k2eAgFnSc1d1	závislá
porucha	porucha	k1gFnSc1	porucha
osobnosti	osobnost	k1gFnSc2	osobnost
</s>
</p>
<p>
<s>
F	F	kA	F
<g/>
63.0	[number]	k4	63.0
Patologické	patologický	k2eAgFnSc2d1	patologická
hráčství	hráčství	k1gNnSc6	hráčství
</s>
</p>
<p>
<s>
F	F	kA	F
<g/>
63.1	[number]	k4	63.1
Pyromanie	pyromanie	k1gFnSc2	pyromanie
</s>
</p>
<p>
<s>
F	F	kA	F
<g/>
63.2	[number]	k4	63.2
Kleptomanie	kleptomanie	k1gFnSc2	kleptomanie
</s>
</p>
<p>
<s>
F	F	kA	F
<g/>
63.3	[number]	k4	63.3
Trichotillomanie	Trichotillomanie	k1gFnSc2	Trichotillomanie
</s>
</p>
<p>
<s>
F	F	kA	F
<g/>
63.4	[number]	k4	63.4
Jiné	jiná	k1gFnSc2	jiná
nutkavé	nutkavý	k2eAgFnSc2d1	nutkavá
a	a	k8xC	a
impulzivní	impulzivní	k2eAgFnSc2d1	impulzivní
poruchy	porucha	k1gFnSc2	porucha
(	(	kIx(	(
<g/>
-	-	kIx~	-
do	do	k7c2	do
kterých	který	k3yRgFnPc2	který
lze	lze	k6eAd1	lze
zařadit	zařadit	k5eAaPmF	zařadit
např.	např.	kA	např.
workoholismus	workoholismus	k1gInSc4	workoholismus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Definice	definice	k1gFnSc2	definice
závislosti	závislost	k1gFnSc2	závislost
DSM-IV	DSM-IV	k1gFnSc2	DSM-IV
===	===	k?	===
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
diagnozu	diagnoz	k1gInSc3	diagnoz
závislosti	závislost	k1gFnSc2	závislost
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
pacient	pacient	k1gMnSc1	pacient
vykazovat	vykazovat	k5eAaImF	vykazovat
alespoň	alespoň	k9	alespoň
3	[number]	k4	3
ze	z	k7c2	z
7	[number]	k4	7
příznaků	příznak	k1gInPc2	příznak
v	v	k7c6	v
období	období	k1gNnSc6	období
12	[number]	k4	12
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
růst	růst	k1gInSc1	růst
tolerance	tolerance	k1gFnSc2	tolerance
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
odvykací	odvykací	k2eAgInPc1d1	odvykací
příznaky	příznak	k1gInPc1	příznak
po	po	k7c6	po
vysazení	vysazení	k1gNnSc6	vysazení
látky	látka	k1gFnSc2	látka
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
přijímání	přijímání	k1gNnSc2	přijímání
látky	látka	k1gFnSc2	látka
ve	v	k7c6	v
větším	veliký	k2eAgNnSc6d2	veliký
množství	množství	k1gNnSc6	množství
nebo	nebo	k8xC	nebo
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
měl	mít	k5eAaImAgMnS	mít
člověk	člověk	k1gMnSc1	člověk
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
snaha	snaha	k1gFnSc1	snaha
nebo	nebo	k8xC	nebo
jeden	jeden	k4xCgInSc4	jeden
či	či	k8xC	či
více	hodně	k6eAd2	hodně
pokusů	pokus	k1gInPc2	pokus
omezit	omezit	k5eAaPmF	omezit
a	a	k8xC	a
ovládat	ovládat	k5eAaImF	ovládat
přijímání	přijímání	k1gNnSc4	přijímání
látky	látka	k1gFnSc2	látka
</s>
</p>
<p>
<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
trávení	trávení	k1gNnSc2	trávení
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
času	čas	k1gInSc2	čas
užíváním	užívání	k1gNnSc7	užívání
a	a	k8xC	a
obstaráváním	obstarávání	k1gNnSc7	obstarávání
látky	látka	k1gFnSc2	látka
nebo	nebo	k8xC	nebo
zotavováním	zotavování	k1gNnSc7	zotavování
se	se	k3xPyFc4	se
z	z	k7c2	z
účinků	účinek	k1gInPc2	účinek
látky	látka	k1gFnSc2	látka
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
zanechávání	zanechávání	k1gNnSc1	zanechávání
sociálních	sociální	k2eAgFnPc2d1	sociální
<g/>
,	,	kIx,	,
pracovních	pracovní	k2eAgFnPc2d1	pracovní
a	a	k8xC	a
rekreačních	rekreační	k2eAgFnPc2d1	rekreační
aktivit	aktivita	k1gFnPc2	aktivita
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
užívání	užívání	k1gNnSc2	užívání
látky	látka	k1gFnSc2	látka
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gNnPc1	jejich
omezení	omezení	k1gNnPc1	omezení
</s>
</p>
<p>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
pokračující	pokračující	k2eAgNnSc1d1	pokračující
užívání	užívání	k1gNnSc1	užívání
látky	látka	k1gFnSc2	látka
navzdory	navzdory	k7c3	navzdory
dlouhodobým	dlouhodobý	k2eAgMnPc3d1	dlouhodobý
nebo	nebo	k8xC	nebo
opakujícím	opakující	k2eAgMnPc3d1	opakující
se	se	k3xPyFc4	se
sociálním	sociální	k2eAgMnSc7d1	sociální
<g/>
,	,	kIx,	,
psychologickým	psychologický	k2eAgNnSc7d1	psychologické
nebo	nebo	k8xC	nebo
tělesným	tělesný	k2eAgInPc3d1	tělesný
problémům	problém	k1gInPc3	problém
<g/>
,	,	kIx,	,
o	o	k7c6	o
nichž	jenž	k3xRgNnPc6	jenž
člověk	člověk	k1gMnSc1	člověk
ví	vědět	k5eAaImIp3nS	vědět
a	a	k8xC	a
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
působeny	působit	k5eAaImNgInP	působit
nebo	nebo	k8xC	nebo
zhoršovány	zhoršovat	k5eAaImNgInP	zhoršovat
užíváním	užívání	k1gNnSc7	užívání
látky	látka	k1gFnSc2	látka
</s>
</p>
<p>
<s>
==	==	k?	==
Terminologie	terminologie	k1gFnSc2	terminologie
==	==	k?	==
</s>
</p>
<p>
<s>
Úzus	úzus	k1gInSc1	úzus
-	-	kIx~	-
mírné	mírný	k2eAgNnSc1d1	mírné
požívání	požívání	k1gNnSc1	požívání
</s>
</p>
<p>
<s>
Minúzus	Minúzus	k1gInSc1	Minúzus
-	-	kIx~	-
Zneužívání	zneužívání	k1gNnSc1	zneužívání
</s>
</p>
<p>
<s>
Abúzus	abúzus	k1gInSc1	abúzus
-	-	kIx~	-
Nadužívání	nadužívání	k1gNnSc1	nadužívání
(	(	kIx(	(
<g/>
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
se	se	k3xPyFc4	se
od	od	k7c2	od
úzu	úzus	k1gInSc2	úzus
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
konzumující	konzumující	k2eAgFnSc1d1	konzumující
směřuje	směřovat	k5eAaImIp3nS	směřovat
či	či	k8xC	či
vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
účinek	účinek	k1gInSc4	účinek
drogy	droga	k1gFnSc2	droga
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rituální	rituální	k2eAgFnSc1d1	rituální
</s>
</p>
<p>
<s>
Periodický	periodický	k2eAgInSc1d1	periodický
</s>
</p>
<p>
<s>
Systematický	systematický	k2eAgInSc1d1	systematický
</s>
</p>
<p>
<s>
Craving	Craving	k1gInSc1	Craving
neboli	neboli	k8xC	neboli
bažení	bažení	k1gNnSc1	bažení
-	-	kIx~	-
silná	silný	k2eAgFnSc1d1	silná
touha	touha	k1gFnSc1	touha
<g/>
,	,	kIx,	,
puzení	puzený	k2eAgMnPc1d1	puzený
k	k	k7c3	k
požití	požití	k1gNnSc3	požití
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
projevuje	projevovat	k5eAaImIp3nS	projevovat
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
aktivací	aktivace	k1gFnPc2	aktivace
určitých	určitý	k2eAgFnPc2d1	určitá
mozkových	mozkový	k2eAgFnPc2d1	mozková
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
zvýšení	zvýšení	k1gNnSc4	zvýšení
tepu	tep	k1gInSc2	tep
<g/>
,	,	kIx,	,
zvýšení	zvýšení	k1gNnSc4	zvýšení
pocení	pocení	k1gNnSc2	pocení
</s>
</p>
<p>
<s>
Tolerance	tolerance	k1gFnSc1	tolerance
-	-	kIx~	-
míra	míra	k1gFnSc1	míra
snášenlivosti	snášenlivost	k1gFnSc2	snášenlivost
</s>
</p>
<p>
<s>
Obranné	obranný	k2eAgInPc1d1	obranný
mechanismy	mechanismus	k1gInPc1	mechanismus
</s>
</p>
<p>
<s>
Bagatelizace	bagatelizace	k1gFnSc1	bagatelizace
-	-	kIx~	-
zlehčování	zlehčování	k1gNnSc1	zlehčování
</s>
</p>
<p>
<s>
Racionalizace	racionalizace	k1gFnSc1	racionalizace
-	-	kIx~	-
přesunutí	přesunutí	k1gNnSc1	přesunutí
odpovědnosti	odpovědnost	k1gFnSc2	odpovědnost
mimo	mimo	k7c4	mimo
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
použití	použití	k1gNnPc4	použití
zdánlivě	zdánlivě	k6eAd1	zdánlivě
logické	logický	k2eAgFnPc1d1	logická
argumentace	argumentace	k1gFnPc1	argumentace
</s>
</p>
<p>
<s>
Abstinenční	abstinenční	k2eAgInPc1d1	abstinenční
příznaky	příznak	k1gInPc1	příznak
nebo	nebo	k8xC	nebo
Somatický	somatický	k2eAgInSc1d1	somatický
odvykací	odvykací	k2eAgInSc1d1	odvykací
stav	stav	k1gInSc1	stav
-	-	kIx~	-
Soubor	soubor	k1gInSc1	soubor
tělesných	tělesný	k2eAgFnPc2d1	tělesná
a	a	k8xC	a
psychických	psychický	k2eAgFnPc2d1	psychická
subjektivně	subjektivně	k6eAd1	subjektivně
velmi	velmi	k6eAd1	velmi
nepříjemných	příjemný	k2eNgInPc2d1	nepříjemný
projevů	projev	k1gInPc2	projev
</s>
</p>
<p>
<s>
Recidiva	recidiva	k1gFnSc1	recidiva
-	-	kIx~	-
opětovné	opětovný	k2eAgNnSc1d1	opětovné
užití	užití	k1gNnSc1	užití
a	a	k8xC	a
znovuspuštění	znovuspuštění	k1gNnSc1	znovuspuštění
závislosti	závislost	k1gFnSc2	závislost
po	po	k7c6	po
již	již	k6eAd1	již
zdárné	zdárný	k2eAgFnSc6d1	zdárná
abstinenci	abstinence	k1gFnSc6	abstinence
</s>
</p>
<p>
<s>
==	==	k?	==
Primární	primární	k2eAgFnSc1d1	primární
prevence	prevence	k1gFnSc1	prevence
závislostí	závislost	k1gFnSc7	závislost
==	==	k?	==
</s>
</p>
<p>
<s>
Primární	primární	k2eAgFnSc7d1	primární
prevencí	prevence	k1gFnSc7	prevence
rozumíme	rozumět	k5eAaImIp1nP	rozumět
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nemoci	nemoc	k1gFnPc1	nemoc
snažíme	snažit	k5eAaImIp1nP	snažit
předcházet	předcházet	k5eAaImF	předcházet
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
vždy	vždy	k6eAd1	vždy
třeba	třeba	k6eAd1	třeba
vykonávat	vykonávat	k5eAaImF	vykonávat
ji	on	k3xPp3gFnSc4	on
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
její	její	k3xOp3gFnSc1	její
prezentace	prezentace	k1gFnSc1	prezentace
odpovídala	odpovídat	k5eAaImAgFnS	odpovídat
věku	věk	k1gInSc3	věk
a	a	k8xC	a
možnostem	možnost	k1gFnPc3	možnost
porozumění	porozumění	k1gNnSc2	porozumění
dané	daný	k2eAgFnSc2d1	daná
skupiny	skupina	k1gFnSc2	skupina
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vyhnutí	vyhnutí	k1gNnSc1	vyhnutí
se	se	k3xPyFc4	se
užívání	užívání	k1gNnSc3	užívání
odborné	odborný	k2eAgFnSc2d1	odborná
terminologie	terminologie	k1gFnSc2	terminologie
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dobré	dobrý	k2eAgNnSc1d1	dobré
využívat	využívat	k5eAaImF	využívat
příklady	příklad	k1gInPc4	příklad
a	a	k8xC	a
mluvit	mluvit	k5eAaImF	mluvit
nejen	nejen	k6eAd1	nejen
o	o	k7c6	o
nelegálních	legální	k2eNgFnPc6d1	nelegální
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
legálních	legální	k2eAgFnPc6d1	legální
návykových	návykový	k2eAgFnPc6d1	návyková
látkách	látka	k1gFnPc6	látka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
lepší	dobrý	k2eAgNnSc1d2	lepší
vyhnout	vyhnout	k5eAaPmF	vyhnout
se	se	k3xPyFc4	se
moralizování	moralizování	k1gNnSc3	moralizování
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
obvykle	obvykle	k6eAd1	obvykle
vede	vést	k5eAaImIp3nS	vést
jen	jen	k9	jen
k	k	k7c3	k
zatrpknutí	zatrpknutí	k1gNnSc3	zatrpknutí
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
je	být	k5eAaImIp3nS	být
také	také	k9	také
třeba	třeba	k6eAd1	třeba
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
soustavná	soustavný	k2eAgFnSc1d1	soustavná
a	a	k8xC	a
dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
<g/>
,	,	kIx,	,
ideálně	ideálně	k6eAd1	ideálně
pracující	pracující	k2eAgMnSc1d1	pracující
i	i	k9	i
s	s	k7c7	s
rodiči	rodič	k1gMnPc7	rodič
-	-	kIx~	-
pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Léčba	léčba	k1gFnSc1	léčba
závislostí	závislost	k1gFnSc7	závislost
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Ambulantní	ambulantní	k2eAgFnSc1d1	ambulantní
léčba	léčba	k1gFnSc1	léčba
závislostí	závislost	k1gFnSc7	závislost
===	===	k?	===
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
velké	velký	k2eAgNnSc4d1	velké
spektrum	spektrum	k1gNnSc4	spektrum
nabídky	nabídka	k1gFnSc2	nabídka
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poraden	poradna	k1gFnPc2	poradna
po	po	k7c4	po
psychiatrické	psychiatrický	k2eAgFnPc4d1	psychiatrická
organizace	organizace	k1gFnPc4	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
znak	znak	k1gInSc1	znak
je	být	k5eAaImIp3nS	být
terénní	terénní	k2eAgFnSc1d1	terénní
nabídka	nabídka	k1gFnSc1	nabídka
služeb	služba	k1gFnPc2	služba
umožňující	umožňující	k2eAgFnSc1d1	umožňující
zájemcům	zájemce	k1gMnPc3	zájemce
do	do	k7c2	do
péče	péče	k1gFnSc2	péče
docházet	docházet	k5eAaImF	docházet
a	a	k8xC	a
po	po	k7c6	po
konzultaci	konzultace	k1gFnSc6	konzultace
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
vracet	vracet	k5eAaImF	vracet
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
místa	místo	k1gNnPc4	místo
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
ná	ná	k?	ná
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jeden	jeden	k4xCgInSc4	jeden
druh	druh	k1gInSc4	druh
závislosti	závislost	k1gFnSc2	závislost
<g/>
.	.	kIx.	.
</s>
<s>
Ambulantní	ambulantní	k2eAgFnSc1d1	ambulantní
léčba	léčba	k1gFnSc1	léčba
je	být	k5eAaImIp3nS	být
také	také	k9	také
dobrovolná	dobrovolný	k2eAgFnSc1d1	dobrovolná
a	a	k8xC	a
tak	tak	k6eAd1	tak
lze	lze	k6eAd1	lze
u	u	k7c2	u
ní	on	k3xPp3gFnSc2	on
předpokládat	předpokládat	k5eAaImF	předpokládat
motivovanost	motivovanost	k1gFnSc4	motivovanost
docházejících	docházející	k2eAgMnPc2d1	docházející
klientů	klient	k1gMnPc2	klient
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
léčby	léčba	k1gFnSc2	léčba
může	moct	k5eAaImIp3nS	moct
probíhat	probíhat	k5eAaImF	probíhat
podpůrná	podpůrný	k2eAgFnSc1d1	podpůrná
farmakoterapie	farmakoterapie	k1gFnSc1	farmakoterapie
či	či	k8xC	či
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
pracovat	pracovat	k5eAaImF	pracovat
i	i	k9	i
s	s	k7c7	s
rodinnými	rodinný	k2eAgMnPc7d1	rodinný
příslušníky	příslušník	k1gMnPc7	příslušník
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgNnSc1d1	důležité
je	být	k5eAaImIp3nS	být
uvědomění	uvědomění	k1gNnSc1	uvědomění
<g/>
,	,	kIx,	,
že	že	k8xS	že
abstinence	abstinence	k1gFnSc1	abstinence
není	být	k5eNaImIp3nS	být
a	a	k8xC	a
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
cílem	cíl	k1gInSc7	cíl
terapie	terapie	k1gFnSc2	terapie
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
jen	jen	k6eAd1	jen
cestou	cesta	k1gFnSc7	cesta
otevírající	otevírající	k2eAgFnSc4d1	otevírající
bránu	brána	k1gFnSc4	brána
k	k	k7c3	k
plnohodnotnému	plnohodnotný	k2eAgInSc3d1	plnohodnotný
životu	život	k1gInSc3	život
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
hledat	hledat	k5eAaImF	hledat
motivy	motiv	k1gInPc4	motiv
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
pro	pro	k7c4	pro
klienta	klient	k1gMnSc4	klient
budou	být	k5eAaImBp3nP	být
dostatečně	dostatečně	k6eAd1	dostatečně
silné	silný	k2eAgFnPc1d1	silná
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mu	on	k3xPp3gMnSc3	on
pomohly	pomoct	k5eAaPmAgFnP	pomoct
překonat	překonat	k5eAaPmF	překonat
období	období	k1gNnSc4	období
bažení	bažení	k1gNnSc2	bažení
či	či	k8xC	či
těžké	těžký	k2eAgFnSc2d1	těžká
chvíle	chvíle	k1gFnSc2	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dříve	dříve	k6eAd2	dříve
jako	jako	k8xC	jako
řešení	řešení	k1gNnPc4	řešení
využíval	využívat	k5eAaImAgMnS	využívat
právě	právě	k6eAd1	právě
drogu	droga	k1gFnSc4	droga
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
se	se	k3xPyFc4	se
také	také	k9	také
zaměřit	zaměřit	k5eAaPmF	zaměřit
na	na	k7c4	na
hledání	hledání	k1gNnSc4	hledání
konstruktivních	konstruktivní	k2eAgFnPc2d1	konstruktivní
a	a	k8xC	a
fungujících	fungující	k2eAgFnPc2d1	fungující
řešení	řešení	k1gNnSc4	řešení
problémů	problém	k1gInPc2	problém
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
===	===	k?	===
Lůžková	lůžkový	k2eAgFnSc1d1	lůžková
léčba	léčba	k1gFnSc1	léčba
závislostí	závislost	k1gFnSc7	závislost
===	===	k?	===
</s>
</p>
<p>
<s>
Oproti	oproti	k7c3	oproti
ambulantní	ambulantní	k2eAgFnSc3d1	ambulantní
léčbě	léčba	k1gFnSc3	léčba
je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
klientovi	klient	k1gMnSc3	klient
věnováno	věnovat	k5eAaPmNgNnS	věnovat
více	hodně	k6eAd2	hodně
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
klient	klient	k1gMnSc1	klient
již	již	k6eAd1	již
po	po	k7c6	po
konzultaci	konzultace	k1gFnSc6	konzultace
nedochází	docházet	k5eNaImIp3nS	docházet
domů	domů	k6eAd1	domů
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
den	den	k1gInSc4	den
sledován	sledovat	k5eAaImNgMnS	sledovat
psychology	psycholog	k1gMnPc7	psycholog
<g/>
,	,	kIx,	,
psychiatry	psychiatr	k1gMnPc7	psychiatr
či	či	k8xC	či
jinými	jiný	k2eAgMnPc7d1	jiný
doktory	doktor	k1gMnPc7	doktor
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
tam	tam	k6eAd1	tam
klienti	klient	k1gMnPc1	klient
tráví	trávit	k5eAaImIp3nP	trávit
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
a	a	k8xC	a
účastní	účastnit	k5eAaImIp3nP	účastnit
se	se	k3xPyFc4	se
programu	program	k1gInSc2	program
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
direktivní	direktivní	k2eAgFnSc1d1	direktivní
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
sem	sem	k6eAd1	sem
chodí	chodit	k5eAaImIp3nP	chodit
klienti	klient	k1gMnPc1	klient
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
neuspěli	uspět	k5eNaPmAgMnP	uspět
v	v	k7c6	v
ambulantní	ambulantní	k2eAgFnSc6d1	ambulantní
léčbě	léčba	k1gFnSc6	léčba
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
léčba	léčba	k1gFnSc1	léčba
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
také	také	k9	také
nařízená	nařízený	k2eAgFnSc1d1	nařízená
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
tak	tak	k9	tak
otázkou	otázka	k1gFnSc7	otázka
motivace	motivace	k1gFnSc2	motivace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Terapeutické	terapeutický	k2eAgFnSc2d1	terapeutická
skupiny	skupina	k1gFnSc2	skupina
====	====	k?	====
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
léčbu	léčba	k1gFnSc4	léčba
závislostí	závislost	k1gFnPc2	závislost
se	se	k3xPyFc4	se
také	také	k9	také
velmi	velmi	k6eAd1	velmi
osvědčily	osvědčit	k5eAaPmAgInP	osvědčit
tzv.	tzv.	kA	tzv.
terapeutické	terapeutický	k2eAgFnSc2d1	terapeutická
komunity	komunita	k1gFnSc2	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Klienti	klient	k1gMnPc1	klient
tak	tak	k9	tak
utvářejí	utvářet	k5eAaImIp3nP	utvářet
uzavřenou	uzavřený	k2eAgFnSc4d1	uzavřená
komunitu	komunita	k1gFnSc4	komunita
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
navracet	navracet	k5eAaBmF	navracet
do	do	k7c2	do
procesu	proces	k1gInSc2	proces
života	život	k1gInSc2	život
pomocí	pomocí	k7c2	pomocí
znovuosvojování	znovuosvojování	k1gNnSc2	znovuosvojování
si	se	k3xPyFc3	se
základních	základní	k2eAgInPc2d1	základní
návyků	návyk	k1gInPc2	návyk
<g/>
.	.	kIx.	.
</s>
<s>
Mnohdy	mnohdy	k6eAd1	mnohdy
je	být	k5eAaImIp3nS	být
základním	základní	k2eAgInSc7d1	základní
předpokladem	předpoklad	k1gInSc7	předpoklad
pro	pro	k7c4	pro
přijetí	přijetí	k1gNnSc4	přijetí
již	již	k6eAd1	již
projití	projití	k1gNnSc4	projití
odvykací	odvykací	k2eAgFnSc7d1	odvykací
terapií	terapie	k1gFnSc7	terapie
v	v	k7c6	v
jiném	jiný	k2eAgNnSc6d1	jiné
zařízení	zařízení	k1gNnSc6	zařízení
tzn.	tzn.	kA	tzn.
že	že	k8xS	že
při	při	k7c6	při
nástupu	nástup	k1gInSc6	nástup
již	již	k6eAd1	již
musejí	muset	k5eAaImIp3nP	muset
mít	mít	k5eAaImF	mít
čisté	čistý	k2eAgInPc1d1	čistý
testy	test	k1gInPc1	test
krve	krev	k1gFnSc2	krev
a	a	k8xC	a
moči	moč	k1gFnSc2	moč
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
rozřazeny	rozřadit	k5eAaPmNgInP	rozřadit
do	do	k7c2	do
několika	několik	k4yIc2	několik
fází	fáze	k1gFnPc2	fáze
v	v	k7c6	v
komunitě	komunita	k1gFnSc6	komunita
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
každá	každý	k3xTgFnSc1	každý
fáze	fáze	k1gFnSc1	fáze
sebou	se	k3xPyFc7	se
nese	nést	k5eAaImIp3nS	nést
určité	určitý	k2eAgFnPc4d1	určitá
kompetence	kompetence	k1gFnPc4	kompetence
<g/>
.	.	kIx.	.
</s>
<s>
Mnohdy	mnohdy	k6eAd1	mnohdy
se	se	k3xPyFc4	se
také	také	k9	také
spojuje	spojovat	k5eAaImIp3nS	spojovat
s	s	k7c7	s
pracovní	pracovní	k2eAgFnSc7d1	pracovní
terapií	terapie	k1gFnSc7	terapie
a	a	k8xC	a
pozdějším	pozdní	k2eAgInSc7d2	pozdější
odchodem	odchod	k1gInSc7	odchod
do	do	k7c2	do
chráněného	chráněný	k2eAgNnSc2d1	chráněné
bydlení	bydlení	k1gNnSc2	bydlení
<g/>
.	.	kIx.	.
</s>
<s>
Instituce	instituce	k1gFnSc1	instituce
se	se	k3xPyFc4	se
také	také	k9	také
snaží	snažit	k5eAaImIp3nP	snažit
v	v	k7c6	v
prvních	první	k4xOgFnPc6	první
fázích	fáze	k1gFnPc6	fáze
pomoci	pomoct	k5eAaPmF	pomoct
klientovi	klient	k1gMnSc3	klient
urovnat	urovnat	k5eAaPmF	urovnat
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
a	a	k8xC	a
vyřešit	vyřešit	k5eAaPmF	vyřešit
právní	právní	k2eAgFnPc4d1	právní
záležitosti	záležitost	k1gFnPc4	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
zřídka	zřídka	k6eAd1	zřídka
klienti	klient	k1gMnPc1	klient
nemají	mít	k5eNaImIp3nP	mít
dluhy	dluh	k1gInPc4	dluh
či	či	k8xC	či
oplétačky	oplétačka	k1gFnPc4	oplétačka
se	s	k7c7	s
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Svépomocné	svépomocný	k2eAgFnSc2d1	svépomocná
skupiny	skupina	k1gFnSc2	skupina
===	===	k?	===
</s>
</p>
<p>
<s>
Svépomocné	svépomocný	k2eAgFnPc1d1	svépomocná
skupiny	skupina	k1gFnPc1	skupina
jsou	být	k5eAaImIp3nP	být
spontánně	spontánně	k6eAd1	spontánně
vytvářející	vytvářející	k2eAgNnSc1d1	vytvářející
se	se	k3xPyFc4	se
sdružení	sdružení	k1gNnSc1	sdružení
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
mají	mít	k5eAaImIp3nP	mít
stejný	stejný	k2eAgInSc4d1	stejný
cíl	cíl	k1gInSc4	cíl
-	-	kIx~	-
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
vymanit	vymanit	k5eAaPmF	vymanit
se	se	k3xPyFc4	se
ze	z	k7c2	z
spárů	spár	k1gInPc2	spár
vlastní	vlastní	k2eAgFnSc2d1	vlastní
závislosti	závislost	k1gFnSc2	závislost
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc1d3	nejznámější
a	a	k8xC	a
světově	světově	k6eAd1	světově
nejrozšířenější	rozšířený	k2eAgNnSc4d3	nejrozšířenější
hnutí	hnutí	k1gNnSc4	hnutí
jsou	být	k5eAaImIp3nP	být
anonymní	anonymní	k2eAgMnPc1d1	anonymní
alkoholici	alkoholik	k1gMnPc1	alkoholik
</s>
</p>
<p>
<s>
AA	AA	kA	AA
avšak	avšak	k8xC	avšak
vznikají	vznikat	k5eAaImIp3nP	vznikat
tyto	tento	k3xDgFnPc4	tento
skupiny	skupina	k1gFnPc4	skupina
i	i	k9	i
pro	pro	k7c4	pro
jiné	jiný	k2eAgInPc4d1	jiný
typy	typ	k1gInPc4	typ
závislostí	závislost	k1gFnPc2	závislost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Časopisy	časopis	k1gInPc4	časopis
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
začal	začít	k5eAaPmAgMnS	začít
vycházet	vycházet	k5eAaImF	vycházet
první	první	k4xOgInSc4	první
Československý	československý	k2eAgInSc4d1	československý
odborný	odborný	k2eAgInSc4d1	odborný
časopis	časopis	k1gInSc4	časopis
Protialkoholický	protialkoholický	k2eAgInSc4d1	protialkoholický
obzor	obzor	k1gInSc4	obzor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
<g/>
)	)	kIx)	)
přejmenoval	přejmenovat	k5eAaPmAgInS	přejmenovat
na	na	k7c4	na
Alkoholismus	alkoholismus	k1gInSc4	alkoholismus
a	a	k8xC	a
drogové	drogový	k2eAgFnSc2d1	drogová
závislosti	závislost	k1gFnSc2	závislost
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
vychází	vycházet	k5eAaImIp3nS	vycházet
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
odborný	odborný	k2eAgInSc1d1	odborný
časopis	časopis	k1gInSc1	časopis
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
téma	téma	k1gNnSc4	téma
s	s	k7c7	s
názvem	název	k1gInSc7	název
Adiktologie	Adiktologie	k1gFnSc2	Adiktologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Studium	studium	k1gNnSc1	studium
==	==	k?	==
</s>
</p>
<p>
<s>
Adiktologii	Adiktologie	k1gFnSc4	Adiktologie
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
možné	možný	k2eAgInPc1d1	možný
jako	jako	k8xC	jako
bakalářský	bakalářský	k2eAgInSc1d1	bakalářský
i	i	k8xC	i
magisterský	magisterský	k2eAgInSc1d1	magisterský
obor	obor	k1gInSc1	obor
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
lékařské	lékařský	k2eAgFnSc6d1	lékařská
fakultě	fakulta	k1gFnSc6	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
KALINA	Kalina	k1gMnSc1	Kalina
<g/>
,	,	kIx,	,
Kamil	Kamil	k1gMnSc1	Kamil
<g/>
.	.	kIx.	.
</s>
<s>
Základy	základ	k1gInPc1	základ
klinické	klinický	k2eAgFnSc2d1	klinická
adiktologie	adiktologie	k1gFnSc2	adiktologie
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
Publishing	Publishing	k1gInSc1	Publishing
a.s.	a.s.	k?	a.s.
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
247	[number]	k4	247
<g/>
-	-	kIx~	-
<g/>
1411	[number]	k4	1411
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NEŠPOR	Nešpor	k1gMnSc1	Nešpor
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Návykové	návykový	k2eAgNnSc4d1	návykové
chování	chování	k1gNnSc4	chování
a	a	k8xC	a
závislost	závislost	k1gFnSc4	závislost
<g/>
:	:	kIx,	:
současné	současný	k2eAgInPc4d1	současný
poznatky	poznatek	k1gInPc4	poznatek
a	a	k8xC	a
perspektivy	perspektiva	k1gFnPc4	perspektiva
léčby	léčba	k1gFnSc2	léčba
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Portál	portál	k1gInSc1	portál
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
9788073679088	[number]	k4	9788073679088
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŘEHAN	ŘEHAN	kA	ŘEHAN
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
.	.	kIx.	.
</s>
<s>
Adiktologie	Adiktologie	k1gFnSc1	Adiktologie
1	[number]	k4	1
<g/>
..	..	k?	..
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
:	:	kIx,	:
Univerzita	univerzita	k1gFnSc1	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
9788024417455	[number]	k4	9788024417455
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Adiktologie	Adiktologie	k1gFnSc1	Adiktologie
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
Klinika	klinika	k1gFnSc1	klinika
adiktologie	adiktologie	k1gFnSc2	adiktologie
</s>
</p>
