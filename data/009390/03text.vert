<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Hilský	Hilský	k1gMnSc1	Hilský
je	být	k5eAaImIp3nS	být
první	první	k4xOgMnSc1	první
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
přeložil	přeložit	k5eAaPmAgMnS	přeložit
kompletní	kompletní	k2eAgNnSc4d1	kompletní
dílo	dílo	k1gNnSc4	dílo
Williama	William	k1gMnSc2	William
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
přehled	přehled	k1gInSc4	přehled
knižních	knižní	k2eAgNnPc2d1	knižní
vydání	vydání	k1gNnPc2	vydání
a	a	k8xC	a
divadelních	divadelní	k2eAgNnPc2d1	divadelní
nastudování	nastudování	k1gNnPc2	nastudování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Knižní	knižní	k2eAgNnSc1d1	knižní
vydání	vydání	k1gNnSc1	vydání
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Dvojjazyčná	dvojjazyčný	k2eAgFnSc1d1	dvojjazyčná
edice	edice	k1gFnSc1	edice
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
Torst	Torst	k1gFnSc1	Torst
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
Atlantis	Atlantis	k1gFnSc1	Atlantis
===	===	k?	===
</s>
</p>
<p>
<s>
Sen	sen	k1gInSc1	sen
noci	noc	k1gFnSc2	noc
svatojánské	svatojánský	k2eAgFnSc2d1	Svatojánská
/	/	kIx~	/
A	a	k8xC	a
Midsummer	Midsummer	k1gMnSc1	Midsummer
Night	Night	k1gMnSc1	Night
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Dream	Dream	k1gInSc1	Dream
<g/>
,	,	kIx,	,
Torst	Torst	k1gInSc1	Torst
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
ISBN	ISBN	kA	ISBN
80-7215-002-2	[number]	k4	80-7215-002-2
</s>
</p>
<p>
<s>
Atlantis	Atlantis	k1gFnSc1	Atlantis
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
ISBN	ISBN	kA	ISBN
978-80-7108-318-4	[number]	k4	978-80-7108-318-4
</s>
</p>
<p>
<s>
Sonety	sonet	k1gInPc1	sonet
/	/	kIx~	/
The	The	k1gFnSc1	The
Sonnets	Sonnets	k1gInSc1	Sonnets
<g/>
,	,	kIx,	,
Torst	Torst	k1gInSc1	Torst
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
ISBN	ISBN	kA	ISBN
80-7215-041-3	[number]	k4	80-7215-041-3
</s>
</p>
<p>
<s>
Atlantis	Atlantis	k1gFnSc1	Atlantis
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
ISBN	ISBN	kA	ISBN
978-80-7108-336-8	[number]	k4	978-80-7108-336-8
</s>
</p>
<p>
<s>
Kupec	kupec	k1gMnSc1	kupec
benátský	benátský	k2eAgMnSc1d1	benátský
/	/	kIx~	/
The	The	k1gMnSc1	The
Merchant	Merchant	k1gMnSc1	Merchant
of	of	k?	of
Venice	Venice	k1gFnSc1	Venice
<g/>
,	,	kIx,	,
Torst	Torst	k1gFnSc1	Torst
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
ISBN	ISBN	kA	ISBN
80-7215-096-0	[number]	k4	80-7215-096-0
</s>
</p>
<p>
<s>
Atlantis	Atlantis	k1gFnSc1	Atlantis
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
ISBN	ISBN	kA	ISBN
978-80-7108-056-5	[number]	k4	978-80-7108-056-5
</s>
</p>
<p>
<s>
Hamlet	Hamlet	k1gMnSc1	Hamlet
<g/>
,	,	kIx,	,
dánský	dánský	k2eAgMnSc1d1	dánský
princ	princ	k1gMnSc1	princ
/	/	kIx~	/
Hamlet	Hamlet	k1gMnSc1	Hamlet
<g/>
,	,	kIx,	,
the	the	k?	the
Prince	princ	k1gMnSc2	princ
of	of	k?	of
Denmark	Denmark	k1gInSc1	Denmark
<g/>
,	,	kIx,	,
Torst	Torst	k1gInSc1	Torst
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
ISBN	ISBN	kA	ISBN
80-7215-153-3	[number]	k4	80-7215-153-3
</s>
</p>
<p>
<s>
Atlantis	Atlantis	k1gFnSc1	Atlantis
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
ISBN	ISBN	kA	ISBN
978-80-7108-321-4	[number]	k4	978-80-7108-321-4
</s>
</p>
<p>
<s>
Othello	Othello	k1gMnSc1	Othello
<g/>
,	,	kIx,	,
benátský	benátský	k2eAgMnSc1d1	benátský
mouřenín	mouřenín	k1gMnSc1	mouřenín
/	/	kIx~	/
Othello	Othello	k1gMnSc1	Othello
<g/>
,	,	kIx,	,
the	the	k?	the
Moor	Moor	k1gInSc1	Moor
of	of	k?	of
Venice	Venice	k1gFnSc1	Venice
<g/>
,	,	kIx,	,
Atlantis	Atlantis	k1gFnSc1	Atlantis
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
ISBN	ISBN	kA	ISBN
80-7108-280-5	[number]	k4	80-7108-280-5
</s>
</p>
<p>
<s>
Král	Král	k1gMnSc1	Král
Lear	Lear	k1gMnSc1	Lear
/	/	kIx~	/
King	King	k1gMnSc1	King
Lear	Lear	k1gMnSc1	Lear
<g/>
,	,	kIx,	,
Atlantis	Atlantis	k1gFnSc1	Atlantis
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
ISBN	ISBN	kA	ISBN
978-80-7108-057-2	[number]	k4	978-80-7108-057-2
</s>
</p>
<p>
<s>
Macbeth	Macbeth	k1gMnSc1	Macbeth
/	/	kIx~	/
Macbeth	Macbeth	k1gMnSc1	Macbeth
<g/>
,	,	kIx,	,
Atlantis	Atlantis	k1gFnSc1	Atlantis
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
ISBN	ISBN	kA	ISBN
978-80-7108-354-2	[number]	k4	978-80-7108-354-2
</s>
</p>
<p>
<s>
===	===	k?	===
Jednojazyčná	jednojazyčný	k2eAgFnSc1d1	jednojazyčná
edice	edice	k1gFnSc1	edice
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
Atlantis	Atlantis	k1gFnSc1	Atlantis
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Edice	edice	k1gFnSc2	edice
Filmová	filmový	k2eAgFnSc1d1	filmová
řada	řada	k1gFnSc1	řada
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
===	===	k?	===
</s>
</p>
<p>
<s>
Kupec	kupec	k1gMnSc1	kupec
benátský	benátský	k2eAgMnSc1d1	benátský
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
ISBN	ISBN	kA	ISBN
978-80-200-1541-9	[number]	k4	978-80-200-1541-9
</s>
</p>
<p>
<s>
Hamlet	Hamlet	k1gMnSc1	Hamlet
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
ISBN	ISBN	kA	ISBN
978-80-200-1819-9	[number]	k4	978-80-200-1819-9
</s>
</p>
<p>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
V.	V.	kA	V.
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
ISBN	ISBN	kA	ISBN
978-80-200-1807-6	[number]	k4	978-80-200-1807-6
</s>
</p>
<p>
<s>
===	===	k?	===
Edice	edice	k1gFnPc4	edice
Knižního	knižní	k2eAgInSc2d1	knižní
klubu	klub	k1gInSc2	klub
===	===	k?	===
</s>
</p>
<p>
<s>
Soubor	soubor	k1gInSc1	soubor
1	[number]	k4	1
<g/>
:	:	kIx,	:
Jak	jak	k8xC	jak
se	se	k3xPyFc4	se
vám	vy	k3xPp2nPc3	vy
líbí	líbit	k5eAaImIp3nS	líbit
<g/>
,	,	kIx,	,
Večer	večer	k6eAd1	večer
tříkrálový	tříkrálový	k2eAgInSc1d1	tříkrálový
<g/>
,	,	kIx,	,
Marná	marný	k2eAgFnSc1d1	marná
lásky	láska	k1gFnSc2	láska
snaha	snaha	k1gFnSc1	snaha
<g/>
,	,	kIx,	,
Sen	sen	k1gInSc1	sen
noci	noc	k1gFnSc2	noc
svatojánské	svatojánský	k2eAgFnSc2d1	Svatojánská
<g/>
,	,	kIx,	,
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
</s>
</p>
<p>
<s>
===	===	k?	===
Edice	edice	k1gFnSc2	edice
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
===	===	k?	===
</s>
</p>
<p>
<s>
Zkrocení	zkrocení	k1gNnSc1	zkrocení
zlé	zlý	k2eAgFnSc2d1	zlá
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
ISBN	ISBN	kA	ISBN
978-80-7258-363-8	[number]	k4	978-80-7258-363-8
</s>
</p>
<p>
<s>
Troilus	Troilus	k1gInSc1	Troilus
a	a	k8xC	a
Kressida	Kressida	k1gFnSc1	Kressida
<g/>
,	,	kIx,	,
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
ISBN	ISBN	kA	ISBN
978-80-7258-411-6	[number]	k4	978-80-7258-411-6
</s>
</p>
<p>
<s>
===	===	k?	===
Mimo	mimo	k7c4	mimo
edice	edice	k1gFnPc4	edice
<g/>
,	,	kIx,	,
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
===	===	k?	===
</s>
</p>
<p>
<s>
Sonety	sonet	k1gInPc1	sonet
<g/>
,	,	kIx,	,
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
</s>
</p>
<p>
<s>
O	o	k7c6	o
lásce	láska	k1gFnSc6	láska
<g/>
,	,	kIx,	,
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
ISBN	ISBN	kA	ISBN
80-7021-626-3	[number]	k4	80-7021-626-3
–	–	k?	–
kniha	kniha	k1gFnSc1	kniha
citátů	citát	k1gInPc2	citát
</s>
</p>
<p>
<s>
===	===	k?	===
Kompletní	kompletní	k2eAgNnSc1d1	kompletní
dílo	dílo	k1gNnSc1	dílo
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
svazku	svazek	k1gInSc6	svazek
===	===	k?	===
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
ISBN	ISBN	kA	ISBN
978-80-200-1903-5	[number]	k4	978-80-200-1903-5
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgFnSc1d1	další
literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Slovník	slovník	k1gInSc1	slovník
citátů	citát	k1gInPc2	citát
z	z	k7c2	z
Díla	dílo	k1gNnSc2	dílo
Williama	William	k1gMnSc2	William
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
ISBN	ISBN	kA	ISBN
978-80-200-2193-9	[number]	k4	978-80-200-2193-9
</s>
</p>
<p>
<s>
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
a	a	k8xC	a
jeviště	jeviště	k1gNnSc1	jeviště
svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
ISBN	ISBN	kA	ISBN
978-80-200-1857-1	[number]	k4	978-80-200-1857-1
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
<g/>
:	:	kIx,	:
2015	[number]	k4	2015
ISBN	ISBN	kA	ISBN
978-80-200-2282-0	[number]	k4	978-80-200-2282-0
</s>
</p>
<p>
<s>
==	==	k?	==
Divadelní	divadelní	k2eAgNnSc4d1	divadelní
nastudování	nastudování	k1gNnSc4	nastudování
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Rozhlasové	rozhlasový	k2eAgFnPc1d1	rozhlasová
verze	verze	k1gFnPc1	verze
==	==	k?	==
</s>
</p>
<p>
<s>
Sonety	sonet	k1gInPc1	sonet
<g/>
,	,	kIx,	,
interpreti	interpret	k1gMnPc1	interpret
Pavel	Pavel	k1gMnSc1	Pavel
Soukup	Soukup	k1gMnSc1	Soukup
a	a	k8xC	a
Scott	Scott	k1gMnSc1	Scott
Bellefeuille	Bellefeuille	k1gFnSc2	Bellefeuille
<g/>
,	,	kIx,	,
Radioservis	Radioservis	k1gFnSc2	Radioservis
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
</s>
</p>
