<s>
Skořice	skořice	k1gFnSc1	skořice
je	být	k5eAaImIp3nS	být
aromatická	aromatický	k2eAgFnSc1d1	aromatická
kůra	kůra	k1gFnSc1	kůra
skořicovníků	skořicovník	k1gInPc2	skořicovník
<g/>
,	,	kIx,	,
stálezelených	stálezelený	k2eAgInPc2d1	stálezelený
tropických	tropický	k2eAgInPc2d1	tropický
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
i	i	k9	i
léčivá	léčivý	k2eAgFnSc1d1	léčivá
<g/>
,	,	kIx,	,
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
silice	silice	k1gFnSc2	silice
<g/>
,	,	kIx,	,
tinktury	tinktura	k1gFnSc2	tinktura
<g/>
,	,	kIx,	,
olej	olej	k1gInSc4	olej
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
kořenně	kořenně	k6eAd1	kořenně
nasládlou	nasládlý	k2eAgFnSc4d1	nasládlá
chuť	chuť	k1gFnSc4	chuť
a	a	k8xC	a
příjemnou	příjemný	k2eAgFnSc4d1	příjemná
vůni	vůně	k1gFnSc4	vůně
<g/>
,	,	kIx,	,
které	který	k3yQgFnSc3	který
jí	on	k3xPp3gFnSc3	on
dává	dávat	k5eAaImIp3nS	dávat
zejména	zejména	k9	zejména
obsažená	obsažený	k2eAgFnSc1d1	obsažená
sloučenina	sloučenina	k1gFnSc1	sloučenina
cinnamaldehyd	cinnamaldehyda	k1gFnPc2	cinnamaldehyda
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
chuť	chuť	k1gFnSc4	chuť
k	k	k7c3	k
jídlu	jídlo	k1gNnSc3	jídlo
<g/>
,	,	kIx,	,
celkově	celkově	k6eAd1	celkově
osvěžuje	osvěžovat	k5eAaImIp3nS	osvěžovat
a	a	k8xC	a
podporuje	podporovat	k5eAaImIp3nS	podporovat
trávení	trávení	k1gNnSc1	trávení
<g/>
.	.	kIx.	.
</s>
<s>
Skořicovníky	skořicovník	k1gInPc1	skořicovník
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
čeledi	čeleď	k1gFnSc2	čeleď
vavřínovité	vavřínovitý	k2eAgFnSc2d1	vavřínovitý
(	(	kIx(	(
<g/>
Lauraceae	Lauracea	k1gFnSc2	Lauracea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgMnPc1d3	nejdůležitější
z	z	k7c2	z
produkčního	produkční	k2eAgNnSc2d1	produkční
hlediska	hledisko	k1gNnSc2	hledisko
jsou	být	k5eAaImIp3nP	být
skořicovník	skořicovník	k1gInSc1	skořicovník
ceylonský	ceylonský	k2eAgInSc1d1	ceylonský
a	a	k8xC	a
čínský	čínský	k2eAgMnSc1d1	čínský
<g/>
.	.	kIx.	.
</s>
<s>
Skořicovník	skořicovník	k1gInSc1	skořicovník
ceylonský	ceylonský	k2eAgInSc1d1	ceylonský
(	(	kIx(	(
<g/>
Cinnamomum	Cinnamomum	k1gInSc1	Cinnamomum
zeylanicum	zeylanicum	k1gInSc1	zeylanicum
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
Srí	Srí	k1gMnPc1	Srí
Lanky	lanko	k1gNnPc7	lanko
(	(	kIx(	(
<g/>
ostrov	ostrov	k1gInSc1	ostrov
Ceylon	Ceylon	k1gInSc1	Ceylon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
však	však	k9	však
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
tropických	tropický	k2eAgFnPc6d1	tropická
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
úroda	úroda	k1gFnSc1	úroda
se	se	k3xPyFc4	se
sklízí	sklízet	k5eAaImIp3nS	sklízet
dva	dva	k4xCgInPc4	dva
až	až	k6eAd1	až
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
po	po	k7c6	po
vysazení	vysazení	k1gNnSc6	vysazení
<g/>
.	.	kIx.	.
</s>
<s>
Cejlonská	cejlonský	k2eAgFnSc1d1	cejlonská
skořice	skořice	k1gFnSc1	skořice
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
tu	tu	k6eAd1	tu
nejkvalitnější	kvalitní	k2eAgInPc4d3	nejkvalitnější
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
kůry	kůra	k1gFnSc2	kůra
osmiletých	osmiletý	k2eAgInPc2d1	osmiletý
keřů	keř	k1gInPc2	keř
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
aromatická	aromatický	k2eAgFnSc1d1	aromatická
<g/>
.	.	kIx.	.
</s>
<s>
Kůra	kůra	k1gFnSc1	kůra
po	po	k7c6	po
usušení	usušení	k1gNnSc6	usušení
tvoří	tvořit	k5eAaImIp3nP	tvořit
charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
tenké	tenký	k2eAgInPc1d1	tenký
svitky	svitek	k1gInPc1	svitek
<g/>
,	,	kIx,	,
okraje	okraj	k1gInPc1	okraj
se	se	k3xPyFc4	se
svinují	svinovat	k5eAaImIp3nP	svinovat
dovnitř	dovnitř	k6eAd1	dovnitř
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
vznikají	vznikat	k5eAaImIp3nP	vznikat
jakési	jakýsi	k3yIgFnPc4	jakýsi
dvojruličky	dvojrulička	k1gFnPc4	dvojrulička
<g/>
.	.	kIx.	.
</s>
<s>
Skořicovník	skořicovník	k1gInSc1	skořicovník
čínský	čínský	k2eAgInSc1d1	čínský
(	(	kIx(	(
<g/>
Cinnamomum	Cinnamomum	k1gInSc1	Cinnamomum
cassia	cassium	k1gNnSc2	cassium
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Barmy	Barma	k1gFnSc2	Barma
<g/>
,	,	kIx,	,
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
Thajsku	Thajsko	k1gNnSc6	Thajsko
a	a	k8xC	a
Indonésii	Indonésie	k1gFnSc6	Indonésie
<g/>
.	.	kIx.	.
</s>
<s>
Skořice	skořice	k1gFnSc1	skořice
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
stromu	strom	k1gInSc2	strom
se	se	k3xPyFc4	se
využívala	využívat	k5eAaImAgFnS	využívat
již	již	k6eAd1	již
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
(	(	kIx(	(
<g/>
ve	v	k7c6	v
starověkém	starověký	k2eAgNnSc6d1	starověké
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rostlina	rostlina	k1gFnSc1	rostlina
může	moct	k5eAaImIp3nS	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
výšky	výška	k1gFnSc2	výška
přes	přes	k7c4	přes
10	[number]	k4	10
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
plantážích	plantáž	k1gFnPc6	plantáž
se	se	k3xPyFc4	se
řeže	řezat	k5eAaImIp3nS	řezat
a	a	k8xC	a
udržuje	udržovat	k5eAaImIp3nS	udržovat
se	se	k3xPyFc4	se
na	na	k7c4	na
zhruba	zhruba	k6eAd1	zhruba
dva	dva	k4xCgInPc4	dva
metry	metr	k1gInPc4	metr
vysoký	vysoký	k2eAgInSc1d1	vysoký
keř	keř	k1gInSc1	keř
<g/>
.	.	kIx.	.
</s>
<s>
Kůra	kůra	k1gFnSc1	kůra
čínského	čínský	k2eAgInSc2d1	čínský
skořicovníku	skořicovník	k1gInSc2	skořicovník
je	být	k5eAaImIp3nS	být
hrubší	hrubý	k2eAgNnSc1d2	hrubší
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
červenější	červený	k2eAgFnSc4d2	červenější
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
po	po	k7c6	po
usušení	usušení	k1gNnSc6	usušení
tvoří	tvořit	k5eAaImIp3nS	tvořit
pouze	pouze	k6eAd1	pouze
žlábkovité	žlábkovitý	k2eAgFnPc4d1	žlábkovitá
trubky	trubka	k1gFnPc4	trubka
nebo	nebo	k8xC	nebo
pruhy	pruh	k1gInPc4	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Skořicovník	skořicovník	k1gInSc1	skořicovník
čínský	čínský	k2eAgInSc1d1	čínský
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
též	též	k9	též
sušené	sušený	k2eAgInPc4d1	sušený
plody	plod	k1gInPc4	plod
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgInPc1d1	známý
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
květy	květ	k1gInPc1	květ
kassiové	kassiový	k2eAgFnSc2d1	kassiový
<g/>
"	"	kIx"	"
a	a	k8xC	a
využívají	využívat	k5eAaImIp3nP	využívat
se	se	k3xPyFc4	se
jako	jako	k9	jako
koření	kořenit	k5eAaImIp3nS	kořenit
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
skořicovníku	skořicovník	k1gInSc2	skořicovník
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
sušená	sušený	k2eAgFnSc1d1	sušená
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
část	část	k1gFnSc1	část
kůry	kůra	k1gFnSc2	kůra
bohatá	bohatý	k2eAgFnSc1d1	bohatá
na	na	k7c6	na
silice	silika	k1gFnSc6	silika
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
listů	list	k1gInPc2	list
<g/>
,	,	kIx,	,
větví	větev	k1gFnPc2	větev
i	i	k8xC	i
květů	květ	k1gInPc2	květ
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
skořicový	skořicový	k2eAgInSc1d1	skořicový
olej	olej	k1gInSc1	olej
<g/>
.	.	kIx.	.
</s>
<s>
Skořicovník	skořicovník	k1gInSc1	skořicovník
nachází	nacházet	k5eAaImIp3nS	nacházet
uplatnění	uplatnění	k1gNnSc4	uplatnění
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
průmyslových	průmyslový	k2eAgNnPc6d1	průmyslové
odvětvích	odvětví	k1gNnPc6	odvětví
<g/>
:	:	kIx,	:
v	v	k7c6	v
potravinářství	potravinářství	k1gNnSc6	potravinářství
-	-	kIx~	-
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
zemí	zem	k1gFnPc2	zem
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
prášková	práškový	k2eAgFnSc1d1	prášková
skořice	skořice	k1gFnSc1	skořice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
přidává	přidávat	k5eAaImIp3nS	přidávat
zejména	zejména	k9	zejména
do	do	k7c2	do
sladkých	sladký	k2eAgInPc2d1	sladký
pokrmů	pokrm	k1gInPc2	pokrm
<g/>
,	,	kIx,	,
do	do	k7c2	do
pečiva	pečivo	k1gNnSc2	pečivo
a	a	k8xC	a
cukrářských	cukrářský	k2eAgInPc2d1	cukrářský
výrobků	výrobek	k1gInPc2	výrobek
<g/>
,	,	kIx,	,
při	při	k7c6	při
zavařování	zavařování	k1gNnSc6	zavařování
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
však	však	k9	však
používat	používat	k5eAaImF	používat
i	i	k9	i
skořicová	skořicový	k2eAgFnSc1d1	skořicová
kůra	kůra	k1gFnSc1	kůra
vcelku	vcelku	k6eAd1	vcelku
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
se	se	k3xPyFc4	se
smaží	smažit	k5eAaImIp3nP	smažit
v	v	k7c6	v
horkém	horký	k2eAgInSc6d1	horký
oleji	olej	k1gInSc6	olej
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
masitých	masitý	k2eAgInPc2d1	masitý
pokrmů	pokrm	k1gInPc2	pokrm
se	se	k3xPyFc4	se
přidává	přidávat	k5eAaImIp3nS	přidávat
skořice	skořice	k1gFnSc1	skořice
v	v	k7c6	v
asijské	asijský	k2eAgFnSc6d1	asijská
<g/>
,	,	kIx,	,
latinskoamerické	latinskoamerický	k2eAgFnSc6d1	latinskoamerická
<g/>
,	,	kIx,	,
arabské	arabský	k2eAgFnSc6d1	arabská
i	i	k8xC	i
španělské	španělský	k2eAgFnSc6d1	španělská
kuchyni	kuchyně	k1gFnSc6	kuchyně
<g/>
.	.	kIx.	.
ve	v	k7c6	v
farmaceutickém	farmaceutický	k2eAgInSc6d1	farmaceutický
průmyslu	průmysl	k1gInSc6	průmysl
-	-	kIx~	-
upravuje	upravovat	k5eAaImIp3nS	upravovat
nepříjemnou	příjemný	k2eNgFnSc4d1	nepříjemná
chuť	chuť	k1gFnSc4	chuť
léků	lék	k1gInPc2	lék
(	(	kIx(	(
<g/>
chuťové	chuťový	k2eAgNnSc4d1	chuťové
korigens	korigens	k1gNnSc4	korigens
<g/>
)	)	kIx)	)
v	v	k7c6	v
likérnictví	likérnictví	k1gNnSc6	likérnictví
-	-	kIx~	-
výroba	výroba	k1gFnSc1	výroba
žaludečních	žaludeční	k2eAgInPc2d1	žaludeční
likérů	likér	k1gInPc2	likér
<g/>
,	,	kIx,	,
kořeněných	kořeněný	k2eAgInPc2d1	kořeněný
a	a	k8xC	a
bylinných	bylinný	k2eAgInPc2d1	bylinný
likérů	likér	k1gInPc2	likér
a	a	k8xC	a
vína	víno	k1gNnSc2	víno
ve	v	k7c6	v
voňavkářství	voňavkářství	k1gNnSc6	voňavkářství
-	-	kIx~	-
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
charakteristickou	charakteristický	k2eAgFnSc4d1	charakteristická
vůni	vůně	k1gFnSc4	vůně
v	v	k7c6	v
kosmetice	kosmetika	k1gFnSc6	kosmetika
-	-	kIx~	-
výroba	výroba	k1gFnSc1	výroba
mýdla	mýdlo	k1gNnPc1	mýdlo
<g/>
,	,	kIx,	,
přípravky	přípravek	k1gInPc1	přípravek
pro	pro	k7c4	pro
ústní	ústní	k2eAgFnSc4d1	ústní
hygienu	hygiena	k1gFnSc4	hygiena
v	v	k7c6	v
léčitelství	léčitelství	k1gNnSc6	léčitelství
-	-	kIx~	-
díky	díky	k7c3	díky
vysokému	vysoký	k2eAgInSc3d1	vysoký
obsahu	obsah	k1gInSc2	obsah
silic	silice	k1gFnPc2	silice
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
zvl.	zvl.	kA	zvl.
skořicový	skořicový	k2eAgInSc1d1	skořicový
olej	olej	k1gInSc1	olej
<g/>
,	,	kIx,	,
tinktura	tinktura	k1gFnSc1	tinktura
a	a	k8xC	a
sirup	sirup	k1gInSc1	sirup
<g/>
.	.	kIx.	.
</s>
<s>
Benzoát	Benzoát	k5eAaPmF	Benzoát
sodný	sodný	k2eAgInSc4d1	sodný
Cinnamaldehyd	Cinnamaldehyd	k1gInSc4	Cinnamaldehyd
–	–	k?	–
základní	základní	k2eAgFnSc1d1	základní
složka	složka	k1gFnSc1	složka
aromatu	aroma	k1gNnSc2	aroma
typického	typický	k2eAgNnSc2d1	typické
pro	pro	k7c4	pro
skořici	skořice	k1gFnSc3	skořice
Ethylcinnamát	Ethylcinnamát	k1gInSc1	Ethylcinnamát
–	–	k?	–
podílí	podílet	k5eAaImIp3nS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
chuti	chuť	k1gFnSc6	chuť
a	a	k8xC	a
aromatu	aroma	k1gNnSc6	aroma
skořice	skořice	k1gFnSc2	skořice
(	(	kIx(	(
<g/>
ovocná	ovocný	k2eAgFnSc1d1	ovocná
a	a	k8xC	a
balzámová	balzámový	k2eAgFnSc1d1	balzámová
vůně	vůně	k1gFnSc1	vůně
připomínající	připomínající	k2eAgFnSc4d1	připomínající
skořici	skořice	k1gFnSc4	skořice
s	s	k7c7	s
trochou	trocha	k1gFnSc7	trocha
jantaru	jantar	k1gInSc2	jantar
<g/>
)	)	kIx)	)
Eugenol	Eugenol	k1gInSc1	Eugenol
–	–	k?	–
podílí	podílet	k5eAaImIp3nS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
aromatu	aroma	k1gNnSc6	aroma
skořice	skořice	k1gFnSc2	skořice
(	(	kIx(	(
<g/>
hřebíčková	hřebíčkový	k2eAgFnSc1d1	hřebíčková
vůně	vůně	k1gFnSc1	vůně
<g/>
)	)	kIx)	)
Kumarin	kumarin	k1gInSc1	kumarin
–	–	k?	–
zvýrazňuje	zvýrazňovat	k5eAaImIp3nS	zvýrazňovat
a	a	k8xC	a
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
<g />
.	.	kIx.	.
</s>
<s>
aroma	aroma	k1gNnSc1	aroma
(	(	kIx(	(
<g/>
sladká	sladký	k2eAgFnSc1d1	sladká
vůně	vůně	k1gFnSc1	vůně
připomínající	připomínající	k2eAgFnSc4d1	připomínající
posečenou	posečený	k2eAgFnSc4d1	posečená
trávu	tráva	k1gFnSc4	tráva
<g/>
)	)	kIx)	)
Kyselina	kyselina	k1gFnSc1	kyselina
skořicová	skořicový	k2eAgFnSc1d1	skořicová
–	–	k?	–
podílí	podílet	k5eAaImIp3nS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
chuti	chuť	k1gFnSc6	chuť
a	a	k8xC	a
aromatu	aroma	k1gNnSc6	aroma
skořice	skořice	k1gFnSc2	skořice
(	(	kIx(	(
<g/>
medová	medový	k2eAgFnSc1d1	medová
<g/>
,	,	kIx,	,
květinová	květinový	k2eAgFnSc1d1	květinová
vůně	vůně	k1gFnSc1	vůně
<g/>
)	)	kIx)	)
Linalool	Linalool	k1gInSc1	Linalool
–	–	k?	–
podílí	podílet	k5eAaImIp3nS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
aromatu	aroma	k1gNnSc6	aroma
skořice	skořice	k1gFnSc2	skořice
(	(	kIx(	(
<g/>
květinová	květinový	k2eAgFnSc1d1	květinová
<g/>
,	,	kIx,	,
s	s	k7c7	s
trochou	trocha	k1gFnSc7	trocha
kořenné	kořenný	k2eAgFnSc2d1	kořenná
vůně	vůně	k1gFnSc2	vůně
<g/>
)	)	kIx)	)
Safrol	Safrol	k1gInSc1	Safrol
–	–	k?	–
podílí	podílet	k5eAaImIp3nS	podílet
<g />
.	.	kIx.	.
</s>
<s>
se	se	k3xPyFc4	se
na	na	k7c6	na
aromatu	aroma	k1gNnSc6	aroma
skořice	skořice	k1gFnSc2	skořice
(	(	kIx(	(
<g/>
vůně	vůně	k1gFnSc2	vůně
cukrárny	cukrárna	k1gFnSc2	cukrárna
<g/>
)	)	kIx)	)
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
krevnímu	krevní	k2eAgInSc3d1	krevní
oběhu	oběh	k1gInSc3	oběh
<g/>
,	,	kIx,	,
snižuje	snižovat	k5eAaImIp3nS	snižovat
krevní	krevní	k2eAgInSc4d1	krevní
tlak	tlak	k1gInSc4	tlak
<g/>
,	,	kIx,	,
zmírňuje	zmírňovat	k5eAaImIp3nS	zmírňovat
krvácení	krvácení	k1gNnSc4	krvácení
z	z	k7c2	z
nosu	nos	k1gInSc2	nos
a	a	k8xC	a
silné	silný	k2eAgFnSc2d1	silná
menstruace	menstruace	k1gFnSc2	menstruace
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
trávení	trávení	k1gNnSc1	trávení
-	-	kIx~	-
štěpí	štěpit	k5eAaImIp3nP	štěpit
tuky	tuk	k1gInPc4	tuk
<g/>
,	,	kIx,	,
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
metabolismus	metabolismus	k1gInSc1	metabolismus
tuků	tuk	k1gInPc2	tuk
a	a	k8xC	a
cukru	cukr	k1gInSc2	cukr
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
cukrovkou	cukrovka	k1gFnSc7	cukrovka
<g/>
,	,	kIx,	,
zahřívá	zahřívat	k5eAaImIp3nS	zahřívat
a	a	k8xC	a
stimuluje	stimulovat	k5eAaImIp3nS	stimulovat
zažívací	zažívací	k2eAgInSc4d1	zažívací
trakt	trakt	k1gInSc4	trakt
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
uklidňuje	uklidňovat	k5eAaImIp3nS	uklidňovat
koliky	kolika	k1gFnPc4	kolika
<g/>
,	,	kIx,	,
průjmy	průjem	k1gInPc4	průjem
<g/>
,	,	kIx,	,
nevolnosti	nevolnost	k1gFnPc4	nevolnost
<g/>
,	,	kIx,	,
nadýmání	nadýmání	k1gNnSc4	nadýmání
podporuje	podporovat	k5eAaImIp3nS	podporovat
chuť	chuť	k1gFnSc4	chuť
k	k	k7c3	k
jídlu	jídlo	k1gNnSc3	jídlo
zevně	zevně	k6eAd1	zevně
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
proti	proti	k7c3	proti
revmatismu	revmatismus	k1gInSc3	revmatismus
má	mít	k5eAaImIp3nS	mít
dezinfekční	dezinfekční	k2eAgMnSc1d1	dezinfekční
a	a	k8xC	a
antibiotické	antibiotický	k2eAgInPc1d1	antibiotický
účinky	účinek	k1gInPc1	účinek
<g/>
,	,	kIx,	,
ničí	ničí	k3xOyNgFnSc1	ničí
bakterie	bakterie	k1gFnSc1	bakterie
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
jako	jako	k9	jako
prostředek	prostředek	k1gInSc1	prostředek
proti	proti	k7c3	proti
nachlazení	nachlazení	k1gNnSc3	nachlazení
<g/>
,	,	kIx,	,
kašli	kašel	k1gInSc6	kašel
<g/>
,	,	kIx,	,
chřipce	chřipka	k1gFnSc3	chřipka
pravidelné	pravidelný	k2eAgNnSc1d1	pravidelné
užívání	užívání	k1gNnSc1	užívání
skořice	skořice	k1gFnSc2	skořice
<g/>
(	(	kIx(	(
<g/>
dávky	dávka	k1gFnSc2	dávka
1	[number]	k4	1
-	-	kIx~	-
5	[number]	k4	5
g	g	kA	g
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
pokles	pokles	k1gInSc4	pokles
hladin	hladina	k1gFnPc2	hladina
plazmatické	plazmatický	k2eAgFnSc2d1	plazmatická
glukózy	glukóza	k1gFnSc2	glukóza
nalačno	nalačno	k6eAd1	nalačno
<g/>
,	,	kIx,	,
celkového	celkový	k2eAgInSc2d1	celkový
cholesterolu	cholesterol	k1gInSc2	cholesterol
<g/>
,	,	kIx,	,
LDL-Ctariglyceridů	LDL-Ctariglycerid	k1gInPc2	LDL-Ctariglycerid
a	a	k8xC	a
zvýšení	zvýšení	k1gNnSc4	zvýšení
HDL-C	HDL-C	k1gFnSc2	HDL-C
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgInS	být
však	však	k9	však
prokázán	prokázat	k5eAaPmNgInS	prokázat
žádný	žádný	k3yNgInSc4	žádný
významný	významný	k2eAgInSc4d1	významný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
hladinu	hladina	k1gFnSc4	hladina
glykovaného	glykovaný	k2eAgInSc2d1	glykovaný
hemoglobinu	hemoglobin	k1gInSc2	hemoglobin
Může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
alergickou	alergický	k2eAgFnSc4d1	alergická
reakci	reakce	k1gFnSc4	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
má	mít	k5eAaImIp3nS	mít
skořicovník	skořicovník	k1gInSc4	skořicovník
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
jako	jako	k8xS	jako
koření	kořenit	k5eAaImIp3nS	kořenit
i	i	k9	i
lék	lék	k1gInSc1	lék
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
jej	on	k3xPp3gMnSc4	on
najdeme	najít	k5eAaPmIp1nP	najít
nejen	nejen	k6eAd1	nejen
na	na	k7c6	na
plantážích	plantáž	k1gFnPc6	plantáž
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
tropickém	tropický	k2eAgNnSc6d1	tropické
a	a	k8xC	a
subtropickém	subtropický	k2eAgNnSc6d1	subtropické
pásmu	pásmo	k1gNnSc6	pásmo
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
Indonésii	Indonésie	k1gFnSc6	Indonésie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c6	na
Seychelských	seychelský	k2eAgInPc6d1	seychelský
ostrovech	ostrov	k1gInPc6	ostrov
a	a	k8xC	a
Madagaskaru	Madagaskar	k1gInSc6	Madagaskar
nebo	nebo	k8xC	nebo
na	na	k7c6	na
americkém	americký	k2eAgInSc6d1	americký
kontinentu	kontinent	k1gInSc6	kontinent
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Plantáže	plantáž	k1gFnPc1	plantáž
skořicovníků	skořicovník	k1gInPc2	skořicovník
bývají	bývat	k5eAaImIp3nP	bývat
poblíž	poblíž	k7c2	poblíž
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tyto	tento	k3xDgFnPc1	tento
rostliny	rostlina	k1gFnPc1	rostlina
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
hodně	hodně	k6eAd1	hodně
spodní	spodní	k2eAgFnPc1d1	spodní
vody	voda	k1gFnPc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
skořicovníku	skořicovník	k1gInSc6	skořicovník
pochází	pocházet	k5eAaImIp3nS	pocházet
již	již	k6eAd1	již
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
4000	[number]	k4	4000
let	léto	k1gNnPc2	léto
před	před	k7c7	před
n.	n.	k?	n.
l.	l.	k?	l.
Jako	jako	k8xS	jako
léčivo	léčivo	k1gNnSc4	léčivo
i	i	k8xC	i
aromatickou	aromatický	k2eAgFnSc4d1	aromatická
přísadu	přísada	k1gFnSc4	přísada
ji	on	k3xPp3gFnSc4	on
používali	používat	k5eAaImAgMnP	používat
staří	starý	k2eAgMnPc1d1	starý
Egypťané	Egypťan	k1gMnPc1	Egypťan
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
ve	v	k7c6	v
starověkém	starověký	k2eAgNnSc6d1	starověké
Řecku	Řecko	k1gNnSc6	Řecko
byla	být	k5eAaImAgFnS	být
skořice	skořice	k1gFnSc1	skořice
známá	známá	k1gFnSc1	známá
a	a	k8xC	a
Řekové	Řek	k1gMnPc1	Řek
přikládali	přikládat	k5eAaImAgMnP	přikládat
jejím	její	k3xOp3gInPc3	její
léčebným	léčebný	k2eAgInPc3d1	léčebný
účinkům	účinek	k1gInPc3	účinek
velký	velký	k2eAgInSc1d1	velký
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nevěděli	vědět	k5eNaImAgMnP	vědět
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
pochází	pocházet	k5eAaImIp3nS	pocházet
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
sám	sám	k3xTgMnSc1	sám
Aristoteles	Aristoteles	k1gMnSc1	Aristoteles
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
z	z	k7c2	z
neznámých	známý	k2eNgFnPc2d1	neznámá
zemí	zem	k1gFnPc2	zem
přinášejí	přinášet	k5eAaImIp3nP	přinášet
obrovští	obrovský	k2eAgMnPc1d1	obrovský
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgMnPc3	jenž
ji	on	k3xPp3gFnSc4	on
z	z	k7c2	z
hnízd	hnízdo	k1gNnPc2	hnízdo
s	s	k7c7	s
nasazením	nasazení	k1gNnSc7	nasazení
života	život	k1gInSc2	život
loupí	loupit	k5eAaImIp3nP	loupit
odvážní	odvážný	k2eAgMnPc1d1	odvážný
Arabové	Arab	k1gMnPc1	Arab
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
jeho	jeho	k3xOp3gMnSc1	jeho
žák	žák	k1gMnSc1	žák
Alexandr	Alexandr	k1gMnSc1	Alexandr
Makedonský	makedonský	k2eAgInSc1d1	makedonský
poznal	poznat	k5eAaPmAgInS	poznat
skořicovník	skořicovník	k1gInSc1	skořicovník
při	při	k7c6	při
tažení	tažení	k1gNnSc6	tažení
do	do	k7c2	do
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Skořice	skořice	k1gFnSc1	skořice
byla	být	k5eAaImAgFnS	být
vzácná	vzácný	k2eAgFnSc1d1	vzácná
a	a	k8xC	a
drahá	drahý	k2eAgFnSc1d1	drahá
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
o	o	k7c4	o
ni	on	k3xPp3gFnSc4	on
vedlo	vést	k5eAaImAgNnS	vést
mnoho	mnoho	k4c4	mnoho
bojů	boj	k1gInPc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
byla	být	k5eAaImAgFnS	být
dříve	dříve	k6eAd2	dříve
známá	známý	k2eAgFnSc1d1	známá
pouze	pouze	k6eAd1	pouze
skořice	skořice	k1gFnSc1	skořice
čínská	čínský	k2eAgFnSc1d1	čínská
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
objevitelským	objevitelský	k2eAgFnPc3d1	objevitelská
cestám	cesta	k1gFnPc3	cesta
začíná	začínat	k5eAaImIp3nS	začínat
dovážet	dovážet	k5eAaImF	dovážet
i	i	k9	i
ceylonská	ceylonský	k2eAgFnSc1d1	ceylonská
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
r.	r.	kA	r.
1580	[number]	k4	1580
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
Cejlon	Cejlon	k1gInSc4	Cejlon
Portugalci	Portugalec	k1gMnPc1	Portugalec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
tvrdě	tvrdě	k6eAd1	tvrdě
utlačovali	utlačovat	k5eAaImAgMnP	utlačovat
místní	místní	k2eAgMnPc4d1	místní
obyvatele	obyvatel	k1gMnPc4	obyvatel
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zajistili	zajistit	k5eAaPmAgMnP	zajistit
Evropě	Evropa	k1gFnSc3	Evropa
dostatek	dostatek	k1gInSc4	dostatek
skořice	skořice	k1gFnSc2	skořice
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
r.	r.	kA	r.
1632	[number]	k4	1632
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
na	na	k7c6	na
Cejlonu	Cejlon	k1gInSc6	Cejlon
začínají	začínat	k5eAaImIp3nP	začínat
prosazovat	prosazovat	k5eAaImF	prosazovat
Nizozemci	Nizozemec	k1gMnPc1	Nizozemec
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnPc3	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
zavést	zavést	k5eAaPmF	zavést
monopol	monopol	k1gInSc4	monopol
na	na	k7c4	na
cejlonskou	cejlonský	k2eAgFnSc4d1	cejlonská
skořici	skořice	k1gFnSc4	skořice
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
přenesli	přenést	k5eAaPmAgMnP	přenést
její	její	k3xOp3gNnSc4	její
pěstění	pěstění	k1gNnSc4	pěstění
také	také	k9	také
do	do	k7c2	do
Indonésie	Indonésie	k1gFnSc2	Indonésie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
jejích	její	k3xOp3gMnPc2	její
největších	veliký	k2eAgMnPc2d3	veliký
producentů	producent	k1gMnPc2	producent
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
byla	být	k5eAaImAgFnS	být
skořice	skořice	k1gFnSc1	skořice
známa	znám	k2eAgFnSc1d1	známa
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
