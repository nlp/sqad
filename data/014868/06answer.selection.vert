<s>
Existují	existovat	k5eAaImIp3nP
čtyři	čtyři	k4xCgInPc1
odlišné	odlišný	k2eAgInPc1d1
principy	princip	k1gInPc1
chlazení	chlazení	k1gNnSc1
ledniček	lednička	k1gFnPc2
<g/>
:	:	kIx,
kompresorové	kompresorový	k2eAgFnSc2d1
<g/>
,	,	kIx,
absorpční	absorpční	k2eAgFnSc2d1
<g/>
,	,	kIx,
adsorpční	adsorpční	k2eAgFnSc2d1
a	a	k8xC
na	na	k7c6
bázi	báze	k1gFnSc6
Peltierova	Peltierův	k2eAgInSc2d1
článku	článek	k1gInSc2
<g/>
.	.	kIx.
</s>