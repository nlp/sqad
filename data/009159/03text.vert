<p>
<s>
Létavkovití	Létavkovitý	k2eAgMnPc1d1	Létavkovitý
(	(	kIx(	(
<g/>
Rhacophoridae	Rhacophoridae	k1gNnSc7	Rhacophoridae
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
čeleď	čeleď	k1gFnSc1	čeleď
žab	žába	k1gFnPc2	žába
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
systematiky	systematika	k1gFnSc2	systematika
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
podčeledi	podčeleď	k1gFnPc4	podčeleď
<g/>
:	:	kIx,	:
japonečky	japonečka	k1gFnPc4	japonečka
(	(	kIx(	(
<g/>
Buergeriinae	Buergeriina	k1gFnPc4	Buergeriina
<g/>
)	)	kIx)	)
a	a	k8xC	a
létavky	létavka	k1gFnSc2	létavka
(	(	kIx(	(
<g/>
Rhacophorinae	Rhacophorinae	k1gInSc1	Rhacophorinae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2016	[number]	k4	2016
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
celkem	celkem	k6eAd1	celkem
devatenáct	devatenáct	k4xCc4	devatenáct
rodů	rod	k1gInPc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
přes	přes	k7c4	přes
400	[number]	k4	400
druhů	druh	k1gInPc2	druh
a	a	k8xC	a
počet	počet	k1gInSc1	počet
popsaných	popsaný	k2eAgMnPc2d1	popsaný
létavkovitých	létavkovitý	k2eAgMnPc2d1	létavkovitý
stále	stále	k6eAd1	stále
roste	růst	k5eAaImIp3nS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
čeleď	čeleď	k1gFnSc1	čeleď
je	být	k5eAaImIp3nS	být
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc6d1	jižní
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Žáby	žába	k1gFnPc1	žába
obývají	obývat	k5eAaImIp3nP	obývat
především	především	k9	především
lesy	les	k1gInPc1	les
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přizpůsobily	přizpůsobit	k5eAaPmAgFnP	přizpůsobit
se	se	k3xPyFc4	se
i	i	k9	i
jiným	jiný	k2eAgInPc3d1	jiný
biotopům	biotop	k1gInPc3	biotop
včetně	včetně	k7c2	včetně
krajiny	krajina	k1gFnSc2	krajina
změněné	změněný	k2eAgFnPc1d1	změněná
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Zaznamenány	zaznamenán	k2eAgFnPc1d1	zaznamenána
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
zemědělských	zemědělský	k2eAgFnPc6d1	zemědělská
oblastech	oblast	k1gFnPc6	oblast
včetně	včetně	k7c2	včetně
plantáží	plantáž	k1gFnPc2	plantáž
palem	palma	k1gFnPc2	palma
olejných	olejný	k2eAgFnPc2d1	olejná
<g/>
,	,	kIx,	,
adaptovaly	adaptovat	k5eAaBmAgInP	adaptovat
se	se	k3xPyFc4	se
i	i	k9	i
životu	život	k1gInSc3	život
ve	v	k7c6	v
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Létavkovití	Létavkovitý	k2eAgMnPc1d1	Létavkovitý
jsou	být	k5eAaImIp3nP	být
malé	malý	k2eAgFnPc1d1	malá
i	i	k8xC	i
velké	velký	k2eAgFnPc1d1	velká
žáby	žába	k1gFnPc1	žába
<g/>
,	,	kIx,	,
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
velikosti	velikost	k1gFnPc4	velikost
mezi	mezi	k7c7	mezi
15	[number]	k4	15
až	až	k9	až
120	[number]	k4	120
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
zploštělé	zploštělý	k2eAgNnSc1d1	zploštělé
tělo	tělo	k1gNnSc1	tělo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
různorodě	různorodě	k6eAd1	různorodě
zbarvené	zbarvený	k2eAgFnPc4d1	zbarvená
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
prsty	prst	k1gInPc7	prst
mají	mít	k5eAaImIp3nP	mít
blánu	blána	k1gFnSc4	blána
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
které	který	k3yRgFnSc2	který
mohou	moct	k5eAaImIp3nP	moct
někteří	některý	k3yIgMnPc1	některý
zástupci	zástupce	k1gMnPc1	zástupce
při	při	k7c6	při
skocích	skok	k1gInPc6	skok
plachtit	plachtit	k5eAaImF	plachtit
ze	z	k7c2	z
stromu	strom	k1gInSc2	strom
na	na	k7c4	na
strom	strom	k1gInSc4	strom
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
čemuž	což	k3yRnSc3	což
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
známými	známá	k1gFnPc7	známá
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Létavkovití	Létavkovitý	k2eAgMnPc1d1	Létavkovitý
jsou	být	k5eAaImIp3nP	být
stromové	stromový	k2eAgFnPc1d1	stromová
žáby	žába	k1gFnPc1	žába
<g/>
,	,	kIx,	,
žít	žít	k5eAaImF	žít
mohou	moct	k5eAaImIp3nP	moct
nicméně	nicméně	k8xC	nicméně
i	i	k9	i
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Aktivní	aktivní	k2eAgMnPc1d1	aktivní
jsou	být	k5eAaImIp3nP	být
během	během	k7c2	během
noci	noc	k1gFnSc2	noc
a	a	k8xC	a
živí	živit	k5eAaImIp3nS	živit
se	s	k7c7	s
převážně	převážně	k6eAd1	převážně
bezobratlými	bezobratlý	k2eAgFnPc7d1	bezobratlý
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
hmyz	hmyz	k1gInSc4	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Způsob	způsob	k1gInSc1	způsob
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
rodech	rod	k1gInPc6	rod
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
některé	některý	k3yIgFnPc1	některý
žáby	žába	k1gFnPc1	žába
kladou	klást	k5eAaImIp3nP	klást
vajíčka	vajíčko	k1gNnPc1	vajíčko
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
a	a	k8xC	a
prodělávají	prodělávat	k5eAaImIp3nP	prodělávat
přímý	přímý	k2eAgInSc4d1	přímý
vývoj	vývoj	k1gInSc4	vývoj
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc1d1	jiná
do	do	k7c2	do
dutin	dutina	k1gFnPc2	dutina
stromů	strom	k1gInPc2	strom
naplněných	naplněný	k2eAgInPc2d1	naplněný
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
pěnovitá	pěnovitý	k2eAgNnPc4d1	pěnovitý
hnízda	hnízdo	k1gNnPc4	hnízdo
nad	nad	k7c7	nad
vodní	vodní	k2eAgFnSc7d1	vodní
hladinou	hladina	k1gFnSc7	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Vylíhlí	vylíhlý	k2eAgMnPc1d1	vylíhlý
pulci	pulec	k1gMnPc1	pulec
poté	poté	k6eAd1	poté
spadnou	spadnout	k5eAaPmIp3nP	spadnout
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
popularizaci	popularizace	k1gFnSc6	popularizace
těchto	tento	k3xDgFnPc2	tento
žab	žába	k1gFnPc2	žába
se	se	k3xPyFc4	se
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
vědec	vědec	k1gMnSc1	vědec
Alfred	Alfred	k1gMnSc1	Alfred
Russel	Russel	k1gMnSc1	Russel
Wallace	Wallace	k1gFnSc1	Wallace
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
chovat	chovat	k5eAaImF	chovat
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gFnSc4	jejich
užitečnost	užitečnost	k1gFnSc4	užitečnost
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
malá	malý	k2eAgFnSc1d1	malá
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
jsou	být	k5eAaImIp3nP	být
loveny	loven	k2eAgInPc1d1	loven
pro	pro	k7c4	pro
maso	maso	k1gNnSc4	maso
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
svazu	svaz	k1gInSc2	svaz
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
bylo	být	k5eAaImAgNnS	být
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2018	[number]	k4	2018
hodnoceno	hodnocen	k2eAgNnSc1d1	hodnoceno
18	[number]	k4	18
druhů	druh	k1gInPc2	druh
jako	jako	k8xS	jako
vyhynulých	vyhynulý	k2eAgMnPc2d1	vyhynulý
a	a	k8xC	a
25	[number]	k4	25
kriticky	kriticky	k6eAd1	kriticky
ohrožených	ohrožený	k2eAgMnPc2d1	ohrožený
<g/>
.	.	kIx.	.
</s>
<s>
Nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
představuje	představovat	k5eAaImIp3nS	představovat
hlavně	hlavně	k9	hlavně
ztráta	ztráta	k1gFnSc1	ztráta
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
==	==	k?	==
</s>
</p>
<p>
<s>
Čeleď	čeleď	k1gFnSc4	čeleď
létavkovití	létavkovitý	k2eAgMnPc1d1	létavkovitý
z	z	k7c2	z
řádu	řád	k1gInSc2	řád
žáby	žába	k1gFnSc2	žába
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
popsána	popsán	k2eAgFnSc1d1	popsána
roku	rok	k1gInSc2	rok
1858	[number]	k4	1858
jako	jako	k8xS	jako
čeleď	čeleď	k1gFnSc1	čeleď
Polypedatidae	Polypedatidae	k1gFnSc1	Polypedatidae
<g/>
.	.	kIx.	.
</s>
<s>
Abraham	Abraham	k1gMnSc1	Abraham
Carel	Carel	k1gMnSc1	Carel
Hoffman	Hoffman	k1gMnSc1	Hoffman
ji	on	k3xPp3gFnSc4	on
roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
přejmenoval	přejmenovat	k5eAaPmAgMnS	přejmenovat
na	na	k7c4	na
současný	současný	k2eAgInSc4d1	současný
název	název	k1gInSc4	název
Rhacophoridae	Rhacophorida	k1gFnSc2	Rhacophorida
<g/>
.	.	kIx.	.
</s>
<s>
Českým	český	k2eAgNnSc7d1	české
synonymem	synonymum	k1gNnSc7	synonymum
této	tento	k3xDgFnSc2	tento
čeledi	čeleď	k1gFnSc2	čeleď
je	on	k3xPp3gMnPc4	on
dlouhoprstovití	dlouhoprstovitý	k2eAgMnPc1d1	dlouhoprstovitý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
ní	on	k3xPp3gFnSc2	on
existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgFnPc4	dva
podčeledi	podčeleď	k1gFnPc4	podčeleď
<g/>
:	:	kIx,	:
japonečky	japonečka	k1gFnPc4	japonečka
(	(	kIx(	(
<g/>
Buergeriinae	Buergeriina	k1gFnPc4	Buergeriina
<g/>
)	)	kIx)	)
a	a	k8xC	a
létavky	létavka	k1gFnSc2	létavka
(	(	kIx(	(
<g/>
Rhacophorinae	Rhacophorinae	k1gInSc1	Rhacophorinae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fosilní	fosilní	k2eAgInPc1d1	fosilní
záznamy	záznam	k1gInPc1	záznam
neexistují	existovat	k5eNaImIp3nP	existovat
<g/>
.	.	kIx.	.
<g/>
Systematika	systematika	k1gFnSc1	systematika
této	tento	k3xDgFnSc2	tento
čeledi	čeleď	k1gFnSc2	čeleď
byla	být	k5eAaImAgFnS	být
vícekráte	vícekráte	k6eAd1	vícekráte
měněna	měněn	k2eAgFnSc1d1	měněna
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nejasná	jasný	k2eNgFnSc1d1	nejasná
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
čeledi	čeleď	k1gFnSc2	čeleď
létavkovití	létavkovitý	k2eAgMnPc1d1	létavkovitý
byly	být	k5eAaImAgFnP	být
zařazovány	zařazovat	k5eAaImNgFnP	zařazovat
i	i	k9	i
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
osamostatněné	osamostatněný	k2eAgInPc1d1	osamostatněný
taxony	taxon	k1gInPc1	taxon
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Polypedatidae	Polypedatidae	k1gNnSc2	Polypedatidae
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
na	na	k7c6	na
základě	základ	k1gInSc6	základ
podobností	podobnost	k1gFnPc2	podobnost
ve	v	k7c6	v
vzhledu	vzhled	k1gInSc6	vzhled
i	i	k8xC	i
chování	chování	k1gNnSc6	chování
řadily	řadit	k5eAaImAgFnP	řadit
létavkovité	létavkovitý	k2eAgFnPc1d1	létavkovitý
a	a	k8xC	a
rákosničkovité	rákosničkovitý	k2eAgFnPc1d1	rákosničkovitý
(	(	kIx(	(
<g/>
Hyperoliidae	Hyperoliida	k1gFnPc1	Hyperoliida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
další	další	k2eAgInPc4d1	další
výzkumy	výzkum	k1gInPc4	výzkum
je	být	k5eAaImIp3nS	být
nicméně	nicméně	k8xC	nicméně
rozdělily	rozdělit	k5eAaPmAgFnP	rozdělit
do	do	k7c2	do
samostatných	samostatný	k2eAgFnPc2d1	samostatná
čeledí	čeleď	k1gFnPc2	čeleď
<g/>
.	.	kIx.	.
</s>
<s>
Rákosničkovití	Rákosničkovitý	k2eAgMnPc1d1	Rákosničkovitý
a	a	k8xC	a
létavkovití	létavkovitý	k2eAgMnPc1d1	létavkovitý
nyní	nyní	k6eAd1	nyní
nejsou	být	k5eNaImIp3nP	být
považováni	považován	k2eAgMnPc1d1	považován
ani	ani	k8xC	ani
za	za	k7c2	za
blízce	blízce	k6eAd1	blízce
příbuzné	příbuzný	k2eAgFnSc2d1	příbuzná
čeledi	čeleď	k1gFnSc2	čeleď
a	a	k8xC	a
obě	dva	k4xCgFnPc1	dva
tyto	tento	k3xDgFnPc1	tento
linie	linie	k1gFnPc1	linie
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
odštěpily	odštěpit	k5eAaPmAgFnP	odštěpit
od	od	k7c2	od
skokanovitých	skokanovitý	k2eAgInPc2d1	skokanovitý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
prvního	první	k4xOgNnSc2	první
století	stoletý	k2eAgMnPc1d1	stoletý
dva	dva	k4xCgMnPc1	dva
němečtí	německý	k2eAgMnPc1d1	německý
herpetologové	herpetolog	k1gMnPc1	herpetolog
−	−	k?	−
Miguel	Miguel	k1gMnSc1	Miguel
Vences	Vences	k1gMnSc1	Vences
a	a	k8xC	a
Frank	Frank	k1gMnSc1	Frank
Glaw	Glaw	k1gMnSc1	Glaw
<g/>
,	,	kIx,	,
vyňali	vynít	k5eAaPmAgMnP	vynít
z	z	k7c2	z
létavkovitých	létavkovitý	k2eAgInPc2d1	létavkovitý
několik	několik	k4yIc4	několik
rodů	rod	k1gInPc2	rod
(	(	kIx(	(
<g/>
Boophis	Boophis	k1gFnSc1	Boophis
<g/>
,	,	kIx,	,
Laliostoma	Laliostoma	k1gFnSc1	Laliostoma
<g/>
,	,	kIx,	,
Aglyptodactylus	Aglyptodactylus	k1gMnSc1	Aglyptodactylus
<g/>
,	,	kIx,	,
Mantella	Mantella	k1gMnSc1	Mantella
a	a	k8xC	a
Mantidactylus	Mantidactylus	k1gMnSc1	Mantidactylus
<g/>
)	)	kIx)	)
a	a	k8xC	a
správně	správně	k6eAd1	správně
je	on	k3xPp3gMnPc4	on
přeřadili	přeřadit	k5eAaPmAgMnP	přeřadit
mezi	mezi	k7c4	mezi
mantelovité	mantelovitý	k2eAgInPc4d1	mantelovitý
(	(	kIx(	(
<g/>
Mantellidae	Mantellida	k1gInPc4	Mantellida
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
I	i	k9	i
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
postavení	postavení	k1gNnSc2	postavení
samotné	samotný	k2eAgFnSc2d1	samotná
čeledě	čeleď	k1gFnSc2	čeleď
panovaly	panovat	k5eAaImAgInP	panovat
zmatky	zmatek	k1gInPc1	zmatek
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
autoři	autor	k1gMnPc1	autor
řadili	řadit	k5eAaImAgMnP	řadit
létavkovité	létavkovitý	k2eAgNnSc4d1	létavkovitý
jako	jako	k9	jako
podčeleď	podčeleď	k1gFnSc4	podčeleď
čeledi	čeleď	k1gFnSc2	čeleď
skokanovití	skokanovitý	k2eAgMnPc1d1	skokanovitý
(	(	kIx(	(
<g/>
Ranidae	Ranidae	k1gNnSc7	Ranidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zástupce	zástupce	k1gMnPc4	zástupce
skokanovitých	skokanovitý	k2eAgMnPc2d1	skokanovitý
považoval	považovat	k5eAaImAgMnS	považovat
létavkovité	létavkovitý	k2eAgInPc4d1	létavkovitý
výzkumník	výzkumník	k1gMnSc1	výzkumník
J.	J.	kA	J.
D.	D.	kA	D.
Lynch	Lynch	k1gMnSc1	Lynch
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
studii	studie	k1gFnSc6	studie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
studie	studie	k1gFnSc1	studie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
(	(	kIx(	(
<g/>
Dubois	Dubois	k1gFnSc1	Dubois
<g/>
)	)	kIx)	)
řadila	řadit	k5eAaImAgFnS	řadit
tuto	tento	k3xDgFnSc4	tento
skupinu	skupina	k1gFnSc4	skupina
žab	žába	k1gFnPc2	žába
pod	pod	k7c4	pod
skokanovité	skokanovitý	k2eAgInPc4d1	skokanovitý
a	a	k8xC	a
rozčlenila	rozčlenit	k5eAaPmAgFnS	rozčlenit
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
triby	trib	k1gInPc4	trib
Buergeriini	Buergeriin	k2eAgMnPc1d1	Buergeriin
<g/>
,	,	kIx,	,
Philautini	Philautin	k2eAgMnPc1d1	Philautin
a	a	k8xC	a
Rhacophorini	Rhacophorin	k2eAgMnPc1d1	Rhacophorin
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
zároveň	zároveň	k6eAd1	zároveň
uváděla	uvádět	k5eAaImAgFnS	uvádět
jako	jako	k9	jako
podčeleď	podčeleď	k1gFnSc1	podčeleď
skokanovitých	skokanovitý	k2eAgInPc2d1	skokanovitý
i	i	k8xC	i
mantelovité	mantelovitý	k2eAgInPc1d1	mantelovitý
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
Mantellinae	Mantellinae	k1gInSc1	Mantellinae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
z	z	k7c2	z
následujího	následují	k2eAgInSc2d1	následují
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
Frost	Frost	k1gFnSc1	Frost
&	&	k?	&
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
nicméně	nicméně	k8xC	nicméně
tuto	tento	k3xDgFnSc4	tento
systematiku	systematika	k1gFnSc4	systematika
zavrhla	zavrhnout	k5eAaPmAgFnS	zavrhnout
a	a	k8xC	a
považovala	považovat	k5eAaImAgFnS	považovat
létavkovité	létavkovitý	k2eAgInPc4d1	létavkovitý
i	i	k8xC	i
mantelovité	mantelovitý	k2eAgNnSc4d1	mantelovitý
za	za	k7c4	za
samostatné	samostatný	k2eAgFnPc4d1	samostatná
čeledě	čeleď	k1gFnPc4	čeleď
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
společně	společně	k6eAd1	společně
tvoří	tvořit	k5eAaImIp3nP	tvořit
sesterskou	sesterský	k2eAgFnSc4d1	sesterská
skupinu	skupina	k1gFnSc4	skupina
ke	k	k7c3	k
skokanovitým	skokanovitý	k2eAgMnPc3d1	skokanovitý
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
prvního	první	k4xOgNnSc2	první
století	století	k1gNnSc2	století
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
provedeno	provést	k5eAaPmNgNnS	provést
několik	několik	k4yIc1	několik
molekulárních	molekulární	k2eAgFnPc2d1	molekulární
analýz	analýza	k1gFnPc2	analýza
<g/>
.	.	kIx.	.
<g/>
Čeleď	čeleď	k1gFnSc1	čeleď
létavkovití	létavkovitý	k2eAgMnPc1d1	létavkovitý
je	on	k3xPp3gNnSc4	on
velmi	velmi	k6eAd1	velmi
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
<g/>
.	.	kIx.	.
</s>
<s>
AmphibiaWeb	AmphibiaWba	k1gFnPc2	AmphibiaWba
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
listopadu	listopad	k1gInSc3	listopad
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
existovalo	existovat	k5eAaImAgNnS	existovat
celkem	celek	k1gInSc7	celek
19	[number]	k4	19
rodů	rod	k1gInPc2	rod
členících	členící	k2eAgInPc2d1	členící
se	se	k3xPyFc4	se
na	na	k7c4	na
423	[number]	k4	423
druhů	druh	k1gInPc2	druh
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Stále	stále	k6eAd1	stále
však	však	k9	však
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
popisování	popisování	k1gNnSc3	popisování
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
nalézání	nalézání	k1gNnSc3	nalézání
nových	nový	k2eAgInPc2d1	nový
rodů	rod	k1gInPc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Značnou	značný	k2eAgFnSc4d1	značná
rozmanitost	rozmanitost	k1gFnSc4	rozmanitost
lze	lze	k6eAd1	lze
pozorovat	pozorovat	k5eAaImF	pozorovat
kupříkladu	kupříkladu	k6eAd1	kupříkladu
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Západní	západní	k2eAgInSc1d1	západní
Ghát	Ghát	k1gInSc1	Ghát
a	a	k8xC	a
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Cejlon	Cejlon	k1gInSc1	Cejlon
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zdejší	zdejší	k2eAgFnSc2d1	zdejší
oblasti	oblast	k1gFnSc2	oblast
bylo	být	k5eAaImAgNnS	být
při	při	k7c6	při
revizi	revize	k1gFnSc6	revize
rodu	rod	k1gInSc2	rod
Philautus	Philautus	k1gInSc4	Philautus
popsáno	popsat	k5eAaPmNgNnS	popsat
dvanáct	dvanáct	k4xCc4	dvanáct
nových	nový	k2eAgInPc2d1	nový
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
zde	zde	k6eAd1	zde
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
objevu	objev	k1gInSc3	objev
nového	nový	k2eAgInSc2d1	nový
druhu	druh	k1gInSc2	druh
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Polypedates	Polypedatesa	k1gFnPc2	Polypedatesa
a	a	k8xC	a
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
dvou	dva	k4xCgInPc2	dva
nových	nový	k2eAgInPc2d1	nový
monotypických	monotypický	k2eAgInPc2d1	monotypický
rodů	rod	k1gInPc2	rod
Mercurana	Mercurana	k1gFnSc1	Mercurana
a	a	k8xC	a
Beddomixalus	Beddomixalus	k1gInSc1	Beddomixalus
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
pohoří	pohořet	k5eAaPmIp3nS	pohořet
Západní	západní	k2eAgInSc1d1	západní
Ghát	Ghát	k1gInSc1	Ghát
nalezen	naleznout	k5eAaPmNgInS	naleznout
nový	nový	k2eAgInSc1d1	nový
druh	druh	k1gInSc1	druh
Ghatixalus	Ghatixalus	k1gInSc1	Ghatixalus
magnus	magnus	k1gInSc4	magnus
<g/>
.	.	kIx.	.
<g/>
Genetickou	genetický	k2eAgFnSc4d1	genetická
výbavu	výbava	k1gFnSc4	výbava
tvoří	tvořit	k5eAaImIp3nS	tvořit
diploidní	diploidní	k2eAgInSc1d1	diploidní
karyotyp	karyotyp	k1gInSc1	karyotyp
2	[number]	k4	2
<g/>
n	n	k0	n
=	=	kIx~	=
24	[number]	k4	24
nebo	nebo	k8xC	nebo
26	[number]	k4	26
chromozomů	chromozom	k1gInPc2	chromozom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Létavkovití	Létavkovitý	k2eAgMnPc1d1	Létavkovitý
jsou	být	k5eAaImIp3nP	být
rozšířeni	rozšířen	k2eAgMnPc1d1	rozšířen
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
jejich	jejich	k3xOp3gInSc4	jejich
rozsah	rozsah	k1gInSc4	rozsah
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
ostrovy	ostrov	k1gInPc4	ostrov
Indonésie	Indonésie	k1gFnSc2	Indonésie
až	až	k9	až
po	po	k7c6	po
Sulawesi	Sulawesi	k1gNnSc6	Sulawesi
(	(	kIx(	(
<g/>
Celebes	Celebes	k1gInSc1	Celebes
<g/>
)	)	kIx)	)
či	či	k8xC	či
Filipíny	Filipíny	k1gFnPc4	Filipíny
a	a	k8xC	a
areál	areál	k1gInSc4	areál
výskytu	výskyt	k1gInSc2	výskyt
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
táhne	táhnout	k5eAaImIp3nS	táhnout
do	do	k7c2	do
států	stát	k1gInPc2	stát
jižní	jižní	k2eAgFnSc2d1	jižní
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Laos	Laos	k1gInSc1	Laos
<g/>
,	,	kIx,	,
Thajsko	Thajsko	k1gNnSc1	Thajsko
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
či	či	k8xC	či
východní	východní	k2eAgFnSc1d1	východní
Indie	Indie	k1gFnSc1	Indie
a	a	k8xC	a
ostrovní	ostrovní	k2eAgInSc1d1	ostrovní
stát	stát	k1gInSc1	stát
Srí	Srí	k1gFnSc2	Srí
Lanka	lanko	k1gNnSc2	lanko
<g/>
.	.	kIx.	.
</s>
<s>
Létavkovití	Létavkovitý	k2eAgMnPc1d1	Létavkovitý
potom	potom	k6eAd1	potom
osídlili	osídlit	k5eAaPmAgMnP	osídlit
i	i	k9	i
část	část	k1gFnSc4	část
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Africké	africký	k2eAgFnSc2d1	africká
populace	populace	k1gFnSc2	populace
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
pouze	pouze	k6eAd1	pouze
rod	rod	k1gInSc4	rod
Chiromantis	Chiromantis	k1gFnSc2	Chiromantis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
dělení	dělení	k1gNnSc2	dělení
na	na	k7c6	na
podčeledi	podčeleď	k1gFnSc6	podčeleď
je	být	k5eAaImIp3nS	být
podčeleď	podčeleď	k1gFnSc1	podčeleď
Buergeriinae	Buergeriinae	k1gFnSc1	Buergeriinae
(	(	kIx(	(
<g/>
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
rodem	rod	k1gInSc7	rod
Buergeria	Buergerium	k1gNnSc2	Buergerium
<g/>
)	)	kIx)	)
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
od	od	k7c2	od
Tchaj-wanu	Tchajan	k1gInSc2	Tchaj-wan
po	po	k7c4	po
Japonsko	Japonsko	k1gNnSc4	Japonsko
a	a	k8xC	a
podčeleď	podčeleď	k1gFnSc4	podčeleď
Rhacophorinae	Rhacophorina	k1gInSc2	Rhacophorina
obývá	obývat	k5eAaImIp3nS	obývat
Afriku	Afrika	k1gFnSc4	Afrika
a	a	k8xC	a
Asii	Asie	k1gFnSc4	Asie
až	až	k9	až
po	po	k7c4	po
mírné	mírný	k2eAgInPc4d1	mírný
areály	areál	k1gInPc4	areál
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
<g/>
Zástupci	zástupce	k1gMnPc1	zástupce
čeledi	čeleď	k1gFnSc2	čeleď
mohou	moct	k5eAaImIp3nP	moct
obývat	obývat	k5eAaImF	obývat
různé	různý	k2eAgInPc4d1	různý
biotopy	biotop	k1gInPc4	biotop
<g/>
.	.	kIx.	.
</s>
<s>
Obvyklým	obvyklý	k2eAgInSc7d1	obvyklý
areálem	areál	k1gInSc7	areál
výskytu	výskyt	k1gInSc2	výskyt
jsou	být	k5eAaImIp3nP	být
lesy	les	k1gInPc1	les
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
zástupce	zástupce	k1gMnSc1	zástupce
žijící	žijící	k2eAgMnSc1d1	žijící
v	v	k7c6	v
lesích	les	k1gInPc6	les
lze	lze	k6eAd1	lze
zmínit	zmínit	k5eAaPmF	zmínit
druhy	druh	k1gInPc4	druh
pouchalka	pouchalka	k1gFnSc1	pouchalka
Eiffingerova	Eiffingerův	k2eAgFnSc1d1	Eiffingerův
(	(	kIx(	(
<g/>
Kurixalus	Kurixalus	k1gMnSc1	Kurixalus
eiffingeri	eiffinger	k1gFnSc2	eiffinger
<g/>
)	)	kIx)	)
či	či	k8xC	či
pouchalka	pouchalka	k1gFnSc1	pouchalka
luzonská	luzonský	k2eAgFnSc1d1	luzonský
(	(	kIx(	(
<g/>
Philautus	Philautus	k1gMnSc1	Philautus
surdus	surdus	k1gMnSc1	surdus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Blízkost	blízkost	k1gFnSc1	blízkost
vody	voda	k1gFnSc2	voda
není	být	k5eNaImIp3nS	být
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
potřebná	potřebný	k2eAgFnSc1d1	potřebná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
jako	jako	k8xC	jako
japonečka	japonečka	k1gFnSc1	japonečka
zavalitá	zavalitý	k2eAgFnSc1d1	zavalitá
(	(	kIx(	(
<g/>
Buergeria	Buergerium	k1gNnSc2	Buergerium
robusta	robusta	k1gFnSc1	robusta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
žijí	žít	k5eAaImIp3nP	žít
<g/>
.	.	kIx.	.
</s>
<s>
Africké	africký	k2eAgFnPc1d1	africká
populace	populace	k1gFnPc1	populace
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
speciálním	speciální	k2eAgFnPc3d1	speciální
adaptacím	adaptace	k1gFnPc3	adaptace
přizpůsobily	přizpůsobit	k5eAaPmAgFnP	přizpůsobit
životu	život	k1gInSc3	život
v	v	k7c6	v
suchých	suchý	k2eAgFnPc6d1	suchá
oblastech	oblast	k1gFnPc6	oblast
a	a	k8xC	a
dovedou	dovést	k5eAaPmIp3nP	dovést
snižovat	snižovat	k5eAaImF	snižovat
ztrátu	ztráta	k1gFnSc4	ztráta
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Žáby	žába	k1gFnPc1	žába
se	se	k3xPyFc4	se
adaptovaly	adaptovat	k5eAaBmAgFnP	adaptovat
i	i	k9	i
na	na	k7c6	na
prostředí	prostředí	k1gNnSc6	prostředí
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
člověkem	člověk	k1gMnSc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
zástupcům	zástupce	k1gMnPc3	zástupce
rodů	rod	k1gInPc2	rod
Polypedates	Polypedates	k1gInSc1	Polypedates
či	či	k8xC	či
Rhacophorus	Rhacophorus	k1gInSc1	Rhacophorus
nevadí	vadit	k5eNaImIp3nS	vadit
život	život	k1gInSc4	život
na	na	k7c6	na
rýžových	rýžový	k2eAgNnPc6d1	rýžové
polích	pole	k1gNnPc6	pole
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
v	v	k7c6	v
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
lidského	lidský	k2eAgNnSc2d1	lidské
osídlení	osídlení	k1gNnSc2	osídlení
<g/>
.	.	kIx.	.
</s>
<s>
Létavka	létavka	k1gFnSc1	létavka
Chiromantis	Chiromantis	k1gFnSc1	Chiromantis
baladika	baladika	k1gFnSc1	baladika
se	se	k3xPyFc4	se
adaptovala	adaptovat	k5eAaBmAgFnS	adaptovat
na	na	k7c4	na
život	život	k1gInSc4	život
v	v	k7c6	v
plantážích	plantáž	k1gFnPc6	plantáž
palem	palma	k1gFnPc2	palma
olejných	olejný	k2eAgFnPc2d1	olejná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Čeleď	čeleď	k1gFnSc1	čeleď
létavkovití	létavkovitý	k2eAgMnPc1d1	létavkovitý
sdílí	sdílet	k5eAaImIp3nS	sdílet
následující	následující	k2eAgInPc4d1	následující
znaky	znak	k1gInPc4	znak
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
pouze	pouze	k6eAd1	pouze
jediná	jediný	k2eAgFnSc1d1	jediná
část	část	k1gFnSc1	část
dlouhého	dlouhý	k2eAgInSc2d1	dlouhý
natahovače	natahovač	k1gInSc2	natahovač
prstů	prst	k1gInPc2	prst
se	se	k3xPyFc4	se
upíná	upínat	k5eAaImIp3nS	upínat
na	na	k7c6	na
distální	distální	k2eAgFnSc6d1	distální
plošce	ploška	k1gFnSc6	ploška
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
nártní	nártní	k2eAgFnSc2d1	nártní
kosti	kost	k1gFnSc2	kost
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
nejzevnější	zevný	k2eAgFnSc1d3	nejzevnější
část	část	k1gFnSc1	část
dlouhého	dlouhý	k2eAgInSc2d1	dlouhý
svalu	sval	k1gInSc2	sval
dlaňového	dlaňový	k2eAgInSc2d1	dlaňový
se	se	k3xPyFc4	se
upíná	upínat	k5eAaImIp3nS	upínat
na	na	k7c4	na
proximolaterální	proximolaterální	k2eAgInSc4d1	proximolaterální
okraj	okraj	k1gInSc4	okraj
aponeurózy	aponeuróza	k1gFnSc2	aponeuróza
dlaně	dlaň	k1gFnSc2	dlaň
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
lichoběžníkový	lichoběžníkový	k2eAgInSc1d1	lichoběžníkový
tvar	tvar	k1gInSc1	tvar
frontoparietální	frontoparietální	k2eAgFnSc2d1	frontoparietální
části	část	k1gFnSc2	část
lebky	lebka	k1gFnSc2	lebka
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
rozvětvení	rozvětvení	k1gNnSc1	rozvětvení
posledního	poslední	k2eAgInSc2d1	poslední
článku	článek	k1gInSc2	článek
prstu	prst	k1gInSc2	prst
<g/>
.	.	kIx.	.
<g/>
Létavkovití	Létavkovitý	k2eAgMnPc1d1	Létavkovitý
mají	mít	k5eAaImIp3nP	mít
zploštělé	zploštělý	k2eAgNnSc4d1	zploštělé
tělo	tělo	k1gNnSc4	tělo
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
mezi	mezi	k7c7	mezi
čenichem	čenich	k1gInSc7	čenich
a	a	k8xC	a
kloakou	kloaka	k1gFnSc7	kloaka
činí	činit	k5eAaImIp3nS	činit
15	[number]	k4	15
až	až	k9	až
120	[number]	k4	120
mm	mm	kA	mm
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
malé	malý	k2eAgFnPc4d1	malá
až	až	k8xS	až
velké	velký	k2eAgFnPc4d1	velká
žáby	žába	k1gFnPc4	žába
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
podčeledi	podčeleď	k1gFnSc2	podčeleď
létavek	létavka	k1gFnPc2	létavka
někteří	některý	k3yIgMnPc1	některý
členové	člen	k1gMnPc1	člen
rodu	rod	k1gInSc2	rod
Theloderma	Thelodermum	k1gNnSc2	Thelodermum
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
velikosti	velikost	k1gFnSc3	velikost
pouze	pouze	k6eAd1	pouze
17	[number]	k4	17
až	až	k9	až
20,6	[number]	k4	20,6
mm	mm	kA	mm
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
druhy	druh	k1gInPc1	druh
jako	jako	k8xC	jako
létavka	létavka	k1gFnSc1	létavka
černoblanná	černoblanný	k2eAgFnSc1d1	černoblanný
(	(	kIx(	(
<g/>
Rhacophorus	Rhacophorus	k1gMnSc1	Rhacophorus
nigropalmatus	nigropalmatus	k1gMnSc1	nigropalmatus
<g/>
)	)	kIx)	)
−	−	k?	−
největší	veliký	k2eAgFnSc1d3	veliký
stromová	stromový	k2eAgFnSc1d1	stromová
žába	žába	k1gFnSc1	žába
Bornea	Borneo	k1gNnSc2	Borneo
−	−	k?	−
nebo	nebo	k8xC	nebo
létavka	létavka	k1gFnSc1	létavka
listová	listový	k2eAgFnSc1d1	listová
(	(	kIx(	(
<g/>
Rhacophorus	Rhacophorus	k1gMnSc1	Rhacophorus
dennysi	dennyse	k1gFnSc4	dennyse
<g/>
)	)	kIx)	)
z	z	k7c2	z
podčeledi	podčeleď	k1gFnSc2	podčeleď
létavek	létavka	k1gFnPc2	létavka
měří	měřit	k5eAaImIp3nS	měřit
i	i	k9	i
okolo	okolo	k7c2	okolo
10	[number]	k4	10
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
.	.	kIx.	.
</s>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
druhé	druhý	k4xOgFnPc4	druhý
podčeledi	podčeleď	k1gFnPc4	podčeleď
<g/>
,	,	kIx,	,
japoneček	japoneček	k1gInSc4	japoneček
<g/>
,	,	kIx,	,
měří	měřit	k5eAaImIp3nS	měřit
mezi	mezi	k7c7	mezi
25	[number]	k4	25
a	a	k8xC	a
70	[number]	k4	70
mm	mm	kA	mm
<g/>
.	.	kIx.	.
<g/>
Lebka	lebka	k1gFnSc1	lebka
létavkovitých	létavkovitý	k2eAgInPc2d1	létavkovitý
je	být	k5eAaImIp3nS	být
široká	široký	k2eAgFnSc1d1	široká
a	a	k8xC	a
plochá	plochý	k2eAgFnSc1d1	plochá
<g/>
,	,	kIx,	,
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
čelisti	čelist	k1gFnSc6	čelist
rostou	růst	k5eAaImIp3nP	růst
zuby	zub	k1gInPc1	zub
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
oči	oko	k1gNnPc1	oko
jsou	být	k5eAaImIp3nP	být
zdobeny	zdoben	k2eAgInPc1d1	zdoben
vodorovnými	vodorovný	k2eAgFnPc7d1	vodorovná
zornicemi	zornice	k1gFnPc7	zornice
<g/>
.	.	kIx.	.
</s>
<s>
Páteř	páteř	k1gFnSc1	páteř
má	mít	k5eAaImIp3nS	mít
osm	osm	k4xCc4	osm
obratlů	obratel	k1gInPc2	obratel
a	a	k8xC	a
výrazná	výrazný	k2eAgFnSc1d1	výrazná
je	být	k5eAaImIp3nS	být
hrudní	hrudní	k2eAgFnSc1d1	hrudní
kost	kost	k1gFnSc1	kost
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
distálnimi	distálni	k1gFnPc7	distálni
články	článek	k1gInPc1	článek
prstů	prst	k1gInPc2	prst
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
vmezeřené	vmezeřený	k2eAgFnPc1d1	vmezeřená
struktury	struktura	k1gFnPc1	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
znak	znak	k1gInSc1	znak
je	být	k5eAaImIp3nS	být
typický	typický	k2eAgInSc1d1	typický
i	i	k9	i
pro	pro	k7c4	pro
zástupce	zástupce	k1gMnPc4	zástupce
mantelovitých	mantelovitý	k2eAgFnPc2d1	mantelovitý
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
konce	konec	k1gInSc2	konec
prstů	prst	k1gInPc2	prst
poté	poté	k6eAd1	poté
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
přísavky	přísavka	k1gFnPc1	přísavka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
žábám	žába	k1gFnPc3	žába
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
lépe	dobře	k6eAd2	dobře
se	se	k3xPyFc4	se
pohybovat	pohybovat	k5eAaImF	pohybovat
ve	v	k7c6	v
stromovém	stromový	k2eAgNnSc6d1	stromové
patře	patro	k1gNnSc6	patro
a	a	k8xC	a
vertikálních	vertikální	k2eAgFnPc6d1	vertikální
plochách	plocha	k1gFnPc6	plocha
<g/>
,	,	kIx,	,
tuto	tento	k3xDgFnSc4	tento
vlastnost	vlastnost	k1gFnSc4	vlastnost
sdílejí	sdílet	k5eAaImIp3nP	sdílet
pro	pro	k7c4	pro
změnu	změna	k1gFnSc4	změna
s	s	k7c7	s
některými	některý	k3yIgMnPc7	některý
skokanovitými	skokanovitý	k2eAgMnPc7d1	skokanovitý
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
létavkovitých	létavkovitý	k2eAgFnPc2d1	létavkovitý
se	se	k3xPyFc4	se
mezi	mezi	k7c4	mezi
prsty	prst	k1gInPc4	prst
či	či	k8xC	či
i	i	k9	i
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
těla	tělo	k1gNnSc2	tělo
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
blána	blána	k1gFnSc1	blána
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
těmto	tento	k3xDgFnPc3	tento
žábám	žába	k1gFnPc3	žába
plachtit	plachtit	k5eAaImF	plachtit
až	až	k9	až
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
několika	několik	k4yIc2	několik
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Schopnost	schopnost	k1gFnSc1	schopnost
si	se	k3xPyFc3	se
však	však	k9	však
osvojilo	osvojit	k5eAaPmAgNnS	osvojit
navzdory	navzdory	k7c3	navzdory
popularitě	popularita	k1gFnSc3	popularita
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc1	několik
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
nemá	mít	k5eNaImIp3nS	mít
blánu	blána	k1gFnSc4	blána
dostatečně	dostatečně	k6eAd1	dostatečně
vyvinutou	vyvinutý	k2eAgFnSc7d1	vyvinutá
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jim	on	k3xPp3gMnPc3	on
umožnila	umožnit	k5eAaPmAgFnS	umožnit
plachtivý	plachtivý	k2eAgInSc4d1	plachtivý
pohyb	pohyb	k1gInSc4	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
největší	veliký	k2eAgFnPc1d3	veliký
blány	blána	k1gFnPc1	blána
mají	mít	k5eAaImIp3nP	mít
plochu	plocha	k1gFnSc4	plocha
až	až	k9	až
20	[number]	k4	20
cm	cm	kA	cm
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Plachtivého	plachtivý	k2eAgInSc2d1	plachtivý
pohybu	pohyb	k1gInSc2	pohyb
jsou	být	k5eAaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
především	především	k9	především
zástupci	zástupce	k1gMnPc1	zástupce
rodu	rod	k1gInSc2	rod
Rhacophorus	Rhacophorus	k1gInSc1	Rhacophorus
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
létavka	létavka	k1gFnSc1	létavka
černoblanná	černoblanný	k2eAgFnSc1d1	černoblanný
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
uplachtí	uplachtit	k5eAaPmIp3nS	uplachtit
7,3	[number]	k4	7,3
m	m	kA	m
při	při	k7c6	při
skoku	skok	k1gInSc6	skok
z	z	k7c2	z
výšky	výška	k1gFnSc2	výška
5,4	[number]	k4	5,4
m	m	kA	m
<g/>
,	,	kIx,	,
či	či	k8xC	či
létavka	létavka	k1gFnSc1	létavka
pardálí	pardálí	k2eAgFnSc1d1	pardálí
(	(	kIx(	(
<g/>
Rhacophorus	Rhacophorus	k1gMnSc1	Rhacophorus
pardalis	pardalis	k1gFnSc2	pardalis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
létavkovití	létavkovitý	k2eAgMnPc1d1	létavkovitý
mohou	moct	k5eAaImIp3nP	moct
plachtění	plachtěný	k2eAgMnPc1d1	plachtěný
využívat	využívat	k5eAaPmF	využívat
i	i	k9	i
někteří	některý	k3yIgMnPc1	některý
zástupci	zástupce	k1gMnPc1	zástupce
čeledi	čeleď	k1gFnSc2	čeleď
rosničkovitých	rosničkovitý	k2eAgNnPc2d1	rosničkovitý
(	(	kIx(	(
<g/>
Hylidae	Hylidae	k1gNnPc2	Hylidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zbarvení	zbarvení	k1gNnSc1	zbarvení
zástupců	zástupce	k1gMnPc2	zástupce
čeledi	čeleď	k1gFnSc2	čeleď
je	být	k5eAaImIp3nS	být
různorodé	různorodý	k2eAgNnSc1d1	různorodé
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hřbetě	hřbet	k1gInSc6	hřbet
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
bílé	bílý	k2eAgFnSc2d1	bílá
přes	přes	k7c4	přes
zelenou	zelená	k1gFnSc4	zelená
(	(	kIx(	(
<g/>
létavka	létavka	k1gFnSc1	létavka
černoblanná	černoblanný	k2eAgFnSc1d1	černoblanný
či	či	k8xC	či
létavka	létavka	k1gFnSc1	létavka
Helenina	Helenin	k2eAgFnSc1d1	Helenina
<g/>
)	)	kIx)	)
až	až	k9	až
po	po	k7c4	po
tmavé	tmavý	k2eAgInPc4d1	tmavý
odstíny	odstín	k1gInPc4	odstín
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
hnědá	hnědý	k2eAgFnSc1d1	hnědá
(	(	kIx(	(
<g/>
červenohnědá	červenohnědý	k2eAgFnSc1d1	červenohnědá
létavka	létavka	k1gFnSc1	létavka
pardálí	pardálí	k2eAgFnSc1d1	pardálí
<g/>
)	)	kIx)	)
či	či	k8xC	či
černá	černé	k1gNnPc4	černé
<g/>
,	,	kIx,	,
vyvinout	vyvinout	k5eAaPmF	vyvinout
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
i	i	k9	i
vzorkování	vzorkování	k1gNnSc1	vzorkování
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stehnech	stehno	k1gNnPc6	stehno
mají	mít	k5eAaImIp3nP	mít
tyto	tento	k3xDgFnPc1	tento
žáby	žába	k1gFnPc1	žába
výstražné	výstražný	k2eAgFnPc1d1	výstražná
vzory	vzor	k1gInPc4	vzor
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
nichž	jenž	k3xRgInPc2	jenž
mohou	moct	k5eAaImIp3nP	moct
odradit	odradit	k5eAaPmF	odradit
případného	případný	k2eAgMnSc4d1	případný
predátora	predátor	k1gMnSc4	predátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chování	chování	k1gNnSc1	chování
==	==	k?	==
</s>
</p>
<p>
<s>
Létavkovití	Létavkovitý	k2eAgMnPc1d1	Létavkovitý
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
stromové	stromový	k2eAgFnPc1d1	stromová
žáby	žába	k1gFnPc1	žába
<g/>
,	,	kIx,	,
konvergentní	konvergentní	k2eAgFnSc1d1	konvergentní
starosvětská	starosvětský	k2eAgFnSc1d1	starosvětská
obdoba	obdoba	k1gFnSc1	obdoba
některých	některý	k3yIgMnPc2	některý
amerických	americký	k2eAgMnPc2d1	americký
stromových	stromový	k2eAgMnPc2d1	stromový
rosničkovitých	rosničkovitý	k2eAgMnPc2d1	rosničkovitý
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
létavky	létavka	k1gFnPc1	létavka
nicméně	nicméně	k8xC	nicméně
žijí	žít	k5eAaImIp3nP	žít
i	i	k9	i
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
nočních	noční	k2eAgFnPc2d1	noční
<g/>
.	.	kIx.	.
</s>
<s>
Zbarvení	zbarvení	k1gNnSc1	zbarvení
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
drsnokožky	drsnokožka	k1gFnPc1	drsnokožka
kornaté	kornatý	k2eAgFnPc1d1	kornatý
(	(	kIx(	(
<g/>
Theloderma	Theloderma	k1gNnSc4	Theloderma
corticale	corticale	k6eAd1	corticale
<g/>
)	)	kIx)	)
či	či	k8xC	či
Philautus	Philautus	k1gInSc4	Philautus
macroscelis	macroscelis	k1gFnSc2	macroscelis
<g/>
,	,	kIx,	,
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
mimikry	mimikry	k1gNnSc1	mimikry
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterým	který	k3yIgInPc3	který
nejsou	být	k5eNaImIp3nP	být
žáby	žába	k1gFnPc4	žába
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
když	když	k8xS	když
odpočívají	odpočívat	k5eAaImIp3nP	odpočívat
(	(	kIx(	(
<g/>
drsnokožka	drsnokožka	k1gFnSc1	drsnokožka
kornatá	kornatý	k2eAgFnSc1d1	kornatý
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
také	také	k9	také
schopna	schopen	k2eAgFnSc1d1	schopna
předvádět	předvádět	k5eAaImF	předvádět
smrt	smrt	k1gFnSc4	smrt
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
odradit	odradit	k5eAaPmF	odradit
predátory	predátor	k1gMnPc4	predátor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
létavky	létavka	k1gFnSc2	létavka
Rhacophorus	Rhacophorus	k1gInSc1	Rhacophorus
penanorum	penanorum	k1gNnSc4	penanorum
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
dokonce	dokonce	k9	dokonce
zaznamenána	zaznamenat	k5eAaPmNgFnS	zaznamenat
i	i	k9	i
změna	změna	k1gFnSc1	změna
zbarvení	zbarvení	k1gNnSc2	zbarvení
během	během	k7c2	během
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jinak	jinak	k6eAd1	jinak
zelené	zelený	k2eAgFnPc4d1	zelená
žáby	žába	k1gFnPc4	žába
přes	přes	k7c4	přes
den	den	k1gInSc4	den
ztmavnou	ztmavnout	k5eAaPmIp3nP	ztmavnout
<g/>
.	.	kIx.	.
</s>
<s>
Létavkovití	Létavkovitý	k2eAgMnPc1d1	Létavkovitý
žerou	žrát	k5eAaImIp3nP	žrát
především	především	k9	především
členovce	členovec	k1gMnPc4	členovec
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
hmyz	hmyz	k1gInSc4	hmyz
nebo	nebo	k8xC	nebo
pavouky	pavouk	k1gMnPc4	pavouk
<g/>
,	,	kIx,	,
některé	některý	k3yIgMnPc4	některý
mohou	moct	k5eAaImIp3nP	moct
pojídat	pojídat	k5eAaImF	pojídat
i	i	k9	i
vajíčka	vajíčko	k1gNnPc4	vajíčko
jiných	jiný	k2eAgInPc2d1	jiný
druhů	druh	k1gInPc2	druh
žab	žába	k1gFnPc2	žába
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgMnPc1d2	veliký
zástupci	zástupce	k1gMnPc1	zástupce
čeledi	čeleď	k1gFnSc2	čeleď
se	se	k3xPyFc4	se
uchylují	uchylovat	k5eAaImIp3nP	uchylovat
i	i	k9	i
k	k	k7c3	k
lovení	lovení	k1gNnSc3	lovení
jiných	jiný	k2eAgMnPc2d1	jiný
obratlovců	obratlovec	k1gMnPc2	obratlovec
a	a	k8xC	a
výjimkou	výjimka	k1gFnSc7	výjimka
není	být	k5eNaImIp3nS	být
ani	ani	k8xC	ani
kanibalismus	kanibalismus	k1gInSc4	kanibalismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
===	===	k?	===
</s>
</p>
<p>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
informací	informace	k1gFnPc2	informace
o	o	k7c4	o
létavkovitých	létavkovitý	k2eAgMnPc2d1	létavkovitý
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
během	během	k7c2	během
doby	doba	k1gFnSc2	doba
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
nejaktivnějšími	aktivní	k2eAgNnPc7d3	nejaktivnější
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
samci	samec	k1gMnPc1	samec
od	od	k7c2	od
různých	různý	k2eAgFnPc2d1	různá
tůní	tůně	k1gFnPc2	tůně
lákají	lákat	k5eAaImIp3nP	lákat
samice	samice	k1gFnPc1	samice
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
mohli	moct	k5eAaImAgMnP	moct
spářit	spářit	k5eAaPmF	spářit
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
létavkovitých	létavkovitý	k2eAgFnPc2d1	létavkovitý
se	se	k3xPyFc4	se
při	při	k7c6	při
reprodukci	reprodukce	k1gFnSc6	reprodukce
drží	držet	k5eAaImIp3nS	držet
samic	samice	k1gFnPc2	samice
za	za	k7c2	za
podpaží	podpaží	k1gNnSc2	podpaží
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgInSc3	tento
způsobu	způsob	k1gInSc3	způsob
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
axilární	axilární	k2eAgInSc1d1	axilární
amplexus	amplexus	k1gInSc1	amplexus
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
může	moct	k5eAaImIp3nS	moct
trvat	trvat	k5eAaImF	trvat
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
až	až	k6eAd1	až
pět	pět	k4xCc4	pět
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
na	na	k7c4	na
něj	on	k3xPp3gNnSc4	on
samice	samice	k1gFnSc1	samice
není	být	k5eNaImIp3nS	být
připravena	připravit	k5eAaPmNgFnS	připravit
<g/>
,	,	kIx,	,
dá	dát	k5eAaPmIp3nS	dát
to	ten	k3xDgNnSc1	ten
najevo	najevo	k6eAd1	najevo
zvláštními	zvláštní	k2eAgInPc7d1	zvláštní
pohyby	pohyb	k1gInPc7	pohyb
a	a	k8xC	a
ostrými	ostrý	k2eAgInPc7d1	ostrý
výkřiky	výkřik	k1gInPc7	výkřik
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
páří	pářit	k5eAaImIp3nS	pářit
více	hodně	k6eAd2	hodně
samců	samec	k1gInPc2	samec
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
samičkou	samička	k1gFnSc7	samička
<g/>
.	.	kIx.	.
<g/>
Někteří	některý	k3yIgMnPc1	některý
zástupci	zástupce	k1gMnPc1	zástupce
rodu	rod	k1gInSc2	rod
Philautus	Philautus	k1gInSc1	Philautus
prodělávají	prodělávat	k5eAaImIp3nP	prodělávat
vývoj	vývoj	k1gInSc4	vývoj
přímý	přímý	k2eAgInSc4d1	přímý
a	a	k8xC	a
vajíčka	vajíčko	k1gNnPc1	vajíčko
kladou	klást	k5eAaImIp3nP	klást
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
druhy	druh	k1gInPc1	druh
však	však	k9	však
mají	mít	k5eAaImIp3nP	mít
odlišné	odlišný	k2eAgFnPc4d1	odlišná
rozmnožovací	rozmnožovací	k2eAgFnPc4d1	rozmnožovací
strategie	strategie	k1gFnPc4	strategie
<g/>
.	.	kIx.	.
</s>
<s>
Rody	rod	k1gInPc1	rod
Nyctixalus	Nyctixalus	k1gInSc1	Nyctixalus
a	a	k8xC	a
Theloderma	Theloderma	k1gFnSc1	Theloderma
umisťují	umisťovat	k5eAaImIp3nP	umisťovat
vajíčka	vajíčko	k1gNnPc1	vajíčko
do	do	k7c2	do
vodou	voda	k1gFnSc7	voda
naplněných	naplněný	k2eAgFnPc2d1	naplněná
dutin	dutina	k1gFnPc2	dutina
<g/>
.	.	kIx.	.
</s>
<s>
Létavkovití	Létavkovitý	k2eAgMnPc1d1	Létavkovitý
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Rhacophorus	Rhacophorus	k1gInSc1	Rhacophorus
<g/>
,	,	kIx,	,
Polypedates	Polypedates	k1gInSc1	Polypedates
<g/>
,	,	kIx,	,
Chiromantis	Chiromantis	k1gInSc1	Chiromantis
a	a	k8xC	a
Chirixalus	Chirixalus	k1gInSc1	Chirixalus
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
na	na	k7c4	na
vegetaci	vegetace	k1gFnSc4	vegetace
umístěné	umístěný	k2eAgFnPc4d1	umístěná
nad	nad	k7c7	nad
vodní	vodní	k2eAgFnSc7d1	vodní
hladinou	hladina	k1gFnSc7	hladina
pěnovité	pěnovitý	k2eAgNnSc4d1	pěnovitý
hnízdo	hnízdo	k1gNnSc4	hnízdo
<g/>
,	,	kIx,	,
do	do	k7c2	do
něhož	jenž	k3xRgNnSc2	jenž
vajíčka	vajíčko	k1gNnSc2	vajíčko
nakladou	naklást	k5eAaPmIp3nP	naklást
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdo	hnízdo	k1gNnSc1	hnízdo
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
sekretem	sekret	k1gInSc7	sekret
samic	samice	k1gFnPc2	samice
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
i	i	k9	i
sperma	sperma	k1gNnSc1	sperma
samečků	sameček	k1gMnPc2	sameček
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
napěnění	napěnění	k1gNnSc4	napěnění
podílejí	podílet	k5eAaImIp3nP	podílet
kopáním	kopání	k1gNnSc7	kopání
nohama	noha	k1gFnPc7	noha
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
možného	možný	k2eAgNnSc2d1	možné
páření	páření	k1gNnSc2	páření
většího	veliký	k2eAgInSc2d2	veliký
počtu	počet	k1gInSc2	počet
samců	samec	k1gMnPc2	samec
najednou	najednou	k6eAd1	najednou
<g/>
,	,	kIx,	,
a	a	k8xC	a
existujícího	existující	k2eAgInSc2d1	existující
velkého	velký	k2eAgInSc2d1	velký
konkurenčního	konkurenční	k2eAgInSc2d1	konkurenční
boje	boj	k1gInSc2	boj
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
samcům	samec	k1gMnPc3	samec
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
neúměrně	úměrně	k6eNd1	úměrně
velká	velký	k2eAgNnPc4d1	velké
varlata	varle	k1gNnPc4	varle
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
japonských	japonský	k2eAgMnPc2d1	japonský
vědců	vědec	k1gMnPc2	vědec
Kusana	Kusan	k1gMnSc2	Kusan
<g/>
,	,	kIx,	,
Tody	Tody	k1gInPc7	Tody
a	a	k8xC	a
Fukujamy	Fukujam	k1gInPc7	Fukujam
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Behavioral	Behavioral	k1gFnSc2	Behavioral
Ecology	Ecologa	k1gFnSc2	Ecologa
and	and	k?	and
Sociobiology	Sociobiolog	k1gMnPc4	Sociobiolog
zkoumala	zkoumat	k5eAaImAgFnS	zkoumat
devatenáct	devatenáct	k4xCc4	devatenáct
druhů	druh	k1gInPc2	druh
japonských	japonský	k2eAgFnPc2d1	japonská
žab	žába	k1gFnPc2	žába
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
varlata	varle	k1gNnPc1	varle
létavkovitých	létavkovitý	k2eAgFnPc2d1	létavkovitý
byla	být	k5eAaImAgFnS	být
nejmohutnější	mohutný	k2eAgFnSc1d3	nejmohutnější
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
ostatních	ostatní	k2eAgFnPc2d1	ostatní
žab	žába	k1gFnPc2	žába
se	se	k3xPyFc4	se
podílela	podílet	k5eAaImAgFnS	podílet
jen	jen	k9	jen
na	na	k7c6	na
asi	asi	k9	asi
0,2	[number]	k4	0,2
až	až	k9	až
0,4	[number]	k4	0,4
%	%	kIx~	%
tělesné	tělesný	k2eAgFnSc2d1	tělesná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
,	,	kIx,	,
u	u	k7c2	u
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
létavek	létavka	k1gFnPc2	létavka
tvořila	tvořit	k5eAaImAgFnS	tvořit
až	až	k6eAd1	až
5	[number]	k4	5
%	%	kIx~	%
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
,	,	kIx,	,
minimálně	minimálně	k6eAd1	minimálně
vždy	vždy	k6eAd1	vždy
nad	nad	k7c7	nad
1	[number]	k4	1
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Pěnové	pěnový	k2eAgNnSc1d1	pěnové
hnízdo	hnízdo	k1gNnSc1	hnízdo
plní	plnit	k5eAaImIp3nS	plnit
funkci	funkce	k1gFnSc4	funkce
termostatu	termostat	k1gInSc2	termostat
a	a	k8xC	a
udržuje	udržovat	k5eAaImIp3nS	udržovat
vajíčka	vajíčko	k1gNnPc4	vajíčko
v	v	k7c6	v
konstantní	konstantní	k2eAgFnSc6d1	konstantní
teplotě	teplota	k1gFnSc6	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
hnízda	hnízdo	k1gNnSc2	hnízdo
se	se	k3xPyFc4	se
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
vodní	vodní	k2eAgNnSc1d1	vodní
prostředí	prostředí	k1gNnSc1	prostředí
a	a	k8xC	a
asi	asi	k9	asi
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
se	se	k3xPyFc4	se
začne	začít	k5eAaPmIp3nS	začít
rozpadat	rozpadat	k5eAaImF	rozpadat
<g/>
.	.	kIx.	.
</s>
<s>
Pulci	pulec	k1gMnPc1	pulec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
z	z	k7c2	z
pěnovitého	pěnovitý	k2eAgNnSc2d1	pěnovitý
hnízda	hnízdo	k1gNnSc2	hnízdo
dostanou	dostat	k5eAaPmIp3nP	dostat
<g/>
,	,	kIx,	,
spadnou	spadnout	k5eAaPmIp3nP	spadnout
do	do	k7c2	do
vodní	vodní	k2eAgFnSc2d1	vodní
hladiny	hladina	k1gFnSc2	hladina
pod	pod	k7c7	pod
ním	on	k3xPp3gNnSc7	on
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dále	daleko	k6eAd2	daleko
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
jejich	jejich	k3xOp3gInSc4	jejich
vývoj	vývoj	k1gInSc4	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
hnízdo	hnízdo	k1gNnSc1	hnízdo
rozpadne	rozpadnout	k5eAaPmIp3nS	rozpadnout
za	za	k7c4	za
příliš	příliš	k6eAd1	příliš
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
larvy	larva	k1gFnPc1	larva
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
pomalý	pomalý	k2eAgInSc4d1	pomalý
růst	růst	k1gInSc4	růst
<g/>
.	.	kIx.	.
<g/>
Pulci	pulec	k1gMnPc1	pulec
mají	mít	k5eAaImIp3nP	mít
ústa	ústa	k1gNnPc4	ústa
tvořená	tvořený	k2eAgNnPc4d1	tvořené
keratinem	keratin	k1gInSc7	keratin
a	a	k8xC	a
žábrové	žábrový	k2eAgFnPc1d1	žábrový
komory	komora	k1gFnPc1	komora
za	za	k7c7	za
srdcem	srdce	k1gNnSc7	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
všežraví	všežravý	k2eAgMnPc1d1	všežravý
a	a	k8xC	a
nevyhýbají	vyhýbat	k5eNaImIp3nP	vyhýbat
se	se	k3xPyFc4	se
ani	ani	k9	ani
kanibalismu	kanibalismus	k1gInSc3	kanibalismus
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
požírají	požírat	k5eAaImIp3nP	požírat
ostatní	ostatní	k2eAgMnPc4d1	ostatní
pulce	pulec	k1gMnPc4	pulec
či	či	k8xC	či
vajíčka	vajíčko	k1gNnPc4	vajíčko
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
létavkovitých	létavkovitý	k2eAgInPc2d1	létavkovitý
(	(	kIx(	(
<g/>
pouchalka	pouchalka	k1gFnSc1	pouchalka
Eiffingerova	Eiffingerův	k2eAgFnSc1d1	Eiffingerův
−	−	k?	−
Kurixalus	Kurixalus	k1gInSc4	Kurixalus
eiffingeri	eiffinger	k1gFnSc2	eiffinger
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
zaznamenána	zaznamenat	k5eAaPmNgFnS	zaznamenat
i	i	k9	i
rodičovská	rodičovský	k2eAgFnSc1d1	rodičovská
péče	péče	k1gFnSc1	péče
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
samice	samice	k1gFnSc1	samice
svým	svůj	k3xOyFgInSc7	svůj
mláďatům	mládě	k1gNnPc3	mládě
přinášely	přinášet	k5eAaImAgFnP	přinášet
potravu	potrava	k1gFnSc4	potrava
tvořenou	tvořený	k2eAgFnSc7d1	tvořená
vajíčky	vajíčko	k1gNnPc7	vajíčko
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
za	za	k7c7	za
tím	ten	k3xDgInSc7	ten
účelem	účel	k1gInSc7	účel
nakladly	naklást	k5eAaPmAgFnP	naklást
předem	předem	k6eAd1	předem
<g/>
.	.	kIx.	.
</s>
<s>
Pulci	pulec	k1gMnPc1	pulec
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
potravě	potrava	k1gFnSc6	potrava
od	od	k7c2	od
matky	matka	k1gFnSc2	matka
natolik	natolik	k6eAd1	natolik
závislí	závislý	k2eAgMnPc1d1	závislý
<g/>
,	,	kIx,	,
že	že	k8xS	že
bez	bez	k7c2	bez
ní	on	k3xPp3gFnSc2	on
uhynou	uhynout	k5eAaPmIp3nP	uhynout
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
dosáhnou	dosáhnout	k5eAaPmIp3nP	dosáhnout
pulci	pulec	k1gMnPc1	pulec
velikosti	velikost	k1gFnSc2	velikost
asi	asi	k9	asi
4	[number]	k4	4
cm	cm	kA	cm
<g/>
,	,	kIx,	,
promění	proměnit	k5eAaPmIp3nS	proměnit
se	se	k3xPyFc4	se
v	v	k7c4	v
dospělce	dospělec	k1gMnSc4	dospělec
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vysokých	vysoký	k2eAgFnPc6d1	vysoká
teplotách	teplota	k1gFnPc6	teplota
se	se	k3xPyFc4	se
v	v	k7c4	v
dospělce	dospělec	k1gMnPc4	dospělec
mohou	moct	k5eAaImIp3nP	moct
vyvinout	vyvinout	k5eAaPmF	vyvinout
i	i	k9	i
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
nicméně	nicméně	k8xC	nicméně
negativně	negativně	k6eAd1	negativně
odrazí	odrazit	k5eAaPmIp3nS	odrazit
v	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
dalším	další	k2eAgInSc6d1	další
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vztah	vztah	k1gInSc1	vztah
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
==	==	k?	==
</s>
</p>
<p>
<s>
O	o	k7c6	o
popularizaci	popularizace	k1gFnSc6	popularizace
těchto	tento	k3xDgFnPc2	tento
žab	žába	k1gFnPc2	žába
se	se	k3xPyFc4	se
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
britský	britský	k2eAgMnSc1d1	britský
přírodopisec	přírodopisec	k1gMnSc1	přírodopisec
<g/>
,	,	kIx,	,
badatel	badatel	k1gMnSc1	badatel
<g/>
,	,	kIx,	,
geograf	geograf	k1gMnSc1	geograf
<g/>
,	,	kIx,	,
antropolog	antropolog	k1gMnSc1	antropolog
a	a	k8xC	a
biolog	biolog	k1gMnSc1	biolog
Alfred	Alfred	k1gMnSc1	Alfred
Russel	Russel	k1gMnSc1	Russel
Wallace	Wallace	k1gFnSc1	Wallace
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zmínil	zmínit	k5eAaPmAgInS	zmínit
létavku	létavka	k1gFnSc4	létavka
černoblannou	černoblanný	k2eAgFnSc7d1	černoblanný
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
publikaci	publikace	k1gFnSc6	publikace
The	The	k1gMnSc1	The
Malay	Malaa	k1gFnSc2	Malaa
Archipelago	Archipelago	k1gMnSc1	Archipelago
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
ji	on	k3xPp3gFnSc4	on
spatřil	spatřit	k5eAaPmAgMnS	spatřit
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Borneo	Borneo	k1gNnSc4	Borneo
roku	rok	k1gInSc2	rok
1855	[number]	k4	1855
a	a	k8xC	a
také	také	k9	také
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
příkladu	příklad	k1gInSc6	příklad
osvětloval	osvětlovat	k5eAaImAgInS	osvětlovat
evoluční	evoluční	k2eAgFnSc3d1	evoluční
teorii	teorie	k1gFnSc3	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc1	svůj
text	text	k1gInSc1	text
opatřil	opatřit	k5eAaPmAgInS	opatřit
dřevorytem	dřevoryt	k1gInSc7	dřevoryt
žáby	žába	k1gFnSc2	žába
vytvořeným	vytvořený	k2eAgNnSc7d1	vytvořené
podle	podle	k7c2	podle
dříve	dříve	k6eAd2	dříve
nakresleného	nakreslený	k2eAgInSc2d1	nakreslený
akvarelu	akvarel	k1gInSc2	akvarel
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Létavky	létavka	k1gFnPc1	létavka
<g/>
,	,	kIx,	,
především	především	k9	především
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Polypedatus	Polypedatus	k1gMnSc1	Polypedatus
či	či	k8xC	či
Rhacophorus	Rhacophorus	k1gMnSc1	Rhacophorus
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
chovají	chovat	k5eAaImIp3nP	chovat
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gFnPc4	on
zřizována	zřizován	k2eAgFnSc1d1	zřizována
ubikace	ubikace	k1gFnSc1	ubikace
pokrytá	pokrytý	k2eAgFnSc1d1	pokrytá
rašelinou	rašelina	k1gFnSc7	rašelina
s	s	k7c7	s
dostatkem	dostatek	k1gInSc7	dostatek
úkrytů	úkryt	k1gInPc2	úkryt
a	a	k8xC	a
větví	větev	k1gFnPc2	větev
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
pohyblivosti	pohyblivost	k1gFnSc3	pohyblivost
žab	žába	k1gFnPc2	žába
vysoká	vysoký	k2eAgFnSc1d1	vysoká
alespoň	alespoň	k9	alespoň
jeden	jeden	k4xCgInSc4	jeden
metr	metr	k1gInSc4	metr
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
pohybovat	pohybovat	k5eAaImF	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
24	[number]	k4	24
až	až	k9	až
29	[number]	k4	29
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
akvaterárium	akvaterárium	k1gNnSc1	akvaterárium
obsahovat	obsahovat	k5eAaImF	obsahovat
zdroj	zdroj	k1gInSc4	zdroj
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Krmí	krmit	k5eAaImIp3nS	krmit
se	se	k3xPyFc4	se
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
lze	lze	k6eAd1	lze
však	však	k9	však
chovat	chovat	k5eAaImF	chovat
i	i	k9	i
jiné	jiný	k2eAgMnPc4d1	jiný
zástupce	zástupce	k1gMnPc4	zástupce
čeledi	čeleď	k1gFnSc2	čeleď
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
M.	M.	kA	M.
F.	F.	kA	F.
Nagaturov	Nagaturovo	k1gNnPc2	Nagaturovo
studii	studie	k1gFnSc4	studie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zabývala	zabývat	k5eAaImAgFnS	zabývat
chovem	chov	k1gInSc7	chov
létavek	létavka	k1gFnPc2	létavka
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Theloderma	Thelodermum	k1gNnSc2	Thelodermum
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
nejlepšího	dobrý	k2eAgMnSc2d3	nejlepší
chovance	chovanec	k1gMnSc2	chovanec
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
drsnokožka	drsnokožka	k1gFnSc1	drsnokožka
kornatá	kornatý	k2eAgFnSc1d1	kornatý
(	(	kIx(	(
<g/>
Theloderma	Theloderma	k1gFnSc1	Theloderma
corticale	corticale	k6eAd1	corticale
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
trávila	trávit	k5eAaImAgFnS	trávit
většinu	většina	k1gFnSc4	většina
dne	den	k1gInSc2	den
mimo	mimo	k7c4	mimo
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
dobře	dobře	k6eAd1	dobře
viditelnou	viditelný	k2eAgFnSc7d1	viditelná
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zástupci	zástupce	k1gMnPc1	zástupce
tohoto	tento	k3xDgInSc2	tento
rodu	rod	k1gInSc2	rod
žijí	žít	k5eAaImIp3nP	žít
převážně	převážně	k6eAd1	převážně
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
rod	rod	k1gInSc1	rod
Theloderma	Thelodermum	k1gNnSc2	Thelodermum
je	být	k5eAaImIp3nS	být
doporučeno	doporučit	k5eAaPmNgNnS	doporučit
chovat	chovat	k5eAaImF	chovat
ve	v	k7c6	v
vodních	vodní	k2eAgFnPc6d1	vodní
nádržích	nádrž	k1gFnPc6	nádrž
o	o	k7c6	o
teplotě	teplota	k1gFnSc6	teplota
22	[number]	k4	22
až	až	k9	až
24	[number]	k4	24
°	°	k?	°
<g/>
C	C	kA	C
doplněných	doplněná	k1gFnPc2	doplněná
o	o	k7c4	o
větve	větev	k1gFnPc4	větev
či	či	k8xC	či
další	další	k2eAgInSc4d1	další
rostlinný	rostlinný	k2eAgInSc4d1	rostlinný
materiál	materiál	k1gInSc4	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Vodu	voda	k1gFnSc4	voda
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
měnit	měnit	k5eAaImF	měnit
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
8	[number]	k4	8
až	až	k9	až
9	[number]	k4	9
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
častá	častý	k2eAgFnSc1d1	častá
výměna	výměna	k1gFnSc1	výměna
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
ztrátu	ztráta	k1gFnSc4	ztráta
živin	živina	k1gFnPc2	živina
<g/>
.	.	kIx.	.
</s>
<s>
Důležitý	důležitý	k2eAgInSc1d1	důležitý
je	být	k5eAaImIp3nS	být
dostatek	dostatek	k1gInSc1	dostatek
tříslovin	tříslovina	k1gFnPc2	tříslovina
<g/>
,	,	kIx,	,
zajištěných	zajištěný	k2eAgFnPc2d1	zajištěná
například	například	k6eAd1	například
listy	lista	k1gFnPc4	lista
dubu	dub	k1gInSc2	dub
v	v	k7c6	v
nádržích	nádrž	k1gFnPc6	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
větších	veliký	k2eAgFnPc6d2	veliký
nádržích	nádrž	k1gFnPc6	nádrž
lze	lze	k6eAd1	lze
dokonce	dokonce	k9	dokonce
chovat	chovat	k5eAaImF	chovat
dospělce	dospělec	k1gMnSc4	dospělec
společně	společně	k6eAd1	společně
s	s	k7c7	s
pulci	pulec	k1gMnPc7	pulec
i	i	k8xC	i
vajíčky	vajíčko	k1gNnPc7	vajíčko
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
jinak	jinak	k6eAd1	jinak
nemají	mít	k5eNaImIp3nP	mít
létavkovití	létavkovitý	k2eAgMnPc1d1	létavkovitý
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
lovit	lovit	k5eAaImF	lovit
lidé	člověk	k1gMnPc1	člověk
pro	pro	k7c4	pro
maso	maso	k1gNnSc4	maso
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
létavku	létavka	k1gFnSc4	létavka
indočínskou	indočínský	k2eAgFnSc4d1	indočínská
(	(	kIx(	(
<g/>
Rhacophorus	Rhacophorus	k1gMnSc1	Rhacophorus
feae	fea	k1gFnSc2	fea
<g/>
)	)	kIx)	)
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
Laosu	Laos	k1gInSc2	Laos
<g/>
,	,	kIx,	,
Myanmaru	Myanmar	k1gInSc2	Myanmar
<g/>
,	,	kIx,	,
Thajska	Thajsko	k1gNnSc2	Thajsko
a	a	k8xC	a
Vietnamu	Vietnam	k1gInSc2	Vietnam
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
pronásledována	pronásledován	k2eAgNnPc1d1	pronásledováno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ohrožení	ohrožení	k1gNnSc1	ohrožení
===	===	k?	===
</s>
</p>
<p>
<s>
Nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
pro	pro	k7c4	pro
létavkovité	létavkovitý	k2eAgFnPc4d1	létavkovitý
představuje	představovat	k5eAaImIp3nS	představovat
ztráta	ztráta	k1gFnSc1	ztráta
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
níž	jenž	k3xRgFnSc3	jenž
se	se	k3xPyFc4	se
několik	několik	k4yIc1	několik
druhů	druh	k1gInPc2	druh
stalo	stát	k5eAaPmAgNnS	stát
ohroženými	ohrožený	k2eAgInPc7d1	ohrožený
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
ztráty	ztráta	k1gFnSc2	ztráta
lesů	les	k1gInPc2	les
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
téměř	téměř	k6eAd1	téměř
nebo	nebo	k8xC	nebo
možná	možná	k9	možná
vyhynulou	vyhynulý	k2eAgFnSc4d1	vyhynulá
javánská	javánský	k2eAgFnSc1d1	Javánská
pouchalka	pouchalka	k1gFnSc1	pouchalka
Jacobsnova	Jacobsnov	k1gInSc2	Jacobsnov
(	(	kIx(	(
<g/>
Philautus	Philautus	k1gInSc1	Philautus
jacobsoni	jacobsoň	k1gFnSc3	jacobsoň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
poslední	poslední	k2eAgFnSc7d1	poslední
državou	država	k1gFnSc7	država
je	být	k5eAaImIp3nS	být
či	či	k8xC	či
bylo	být	k5eAaImAgNnS	být
malé	malý	k2eAgNnSc1d1	malé
území	území	k1gNnSc1	území
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
asi	asi	k9	asi
10	[number]	k4	10
km	km	kA	km
<g/>
2	[number]	k4	2
na	na	k7c6	na
hoře	hora	k1gFnSc6	hora
Ungaran	Ungarana	k1gFnPc2	Ungarana
<g/>
.	.	kIx.	.
</s>
<s>
Ztráta	ztráta	k1gFnSc1	ztráta
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
prostředí	prostředí	k1gNnSc2	prostředí
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
plantáží	plantáž	k1gFnPc2	plantáž
přivedla	přivést	k5eAaPmAgFnS	přivést
k	k	k7c3	k
vyhynutí	vyhynutí	k1gNnSc3	vyhynutí
i	i	k8xC	i
létavku	létavka	k1gFnSc4	létavka
Raorchestes	Raorchestesa	k1gFnPc2	Raorchestesa
sushili	sushit	k5eAaImAgMnP	sushit
<g/>
.	.	kIx.	.
</s>
<s>
Druh	druh	k1gMnSc1	druh
Pseudophilautus	Pseudophilautus	k1gMnSc1	Pseudophilautus
papillosus	papillosus	k1gMnSc1	papillosus
je	být	k5eAaImIp3nS	být
kromě	kromě	k7c2	kromě
výsadby	výsadba	k1gFnSc2	výsadba
plantáží	plantáž	k1gFnPc2	plantáž
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
degradace	degradace	k1gFnPc4	degradace
stanovišť	stanoviště	k1gNnPc2	stanoviště
ohrožován	ohrožován	k2eAgInSc1d1	ohrožován
i	i	k8xC	i
ztrátou	ztráta	k1gFnSc7	ztráta
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
prostředí	prostředí	k1gNnSc2	prostředí
následkem	následkem	k7c2	následkem
těžby	těžba	k1gFnSc2	těžba
drahých	drahý	k2eAgInPc2d1	drahý
kamenů	kámen	k1gInPc2	kámen
(	(	kIx(	(
<g/>
všechny	všechen	k3xTgInPc1	všechen
tři	tři	k4xCgInPc1	tři
druhy	druh	k1gInPc1	druh
jsou	být	k5eAaImIp3nP	být
hodnoceny	hodnotit	k5eAaImNgInP	hodnotit
jako	jako	k9	jako
kriticky	kriticky	k6eAd1	kriticky
ohrožené	ohrožený	k2eAgNnSc1d1	ohrožené
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druh	druh	k1gInSc1	druh
Chiromantis	Chiromantis	k1gFnSc2	Chiromantis
baladika	baladik	k1gMnSc2	baladik
ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
znečišťování	znečišťování	k1gNnSc4	znečišťování
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
chemikáliemi	chemikálie	k1gFnPc7	chemikálie
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
létavku	létavka	k1gFnSc4	létavka
indočínskou	indočínský	k2eAgFnSc4d1	indočínská
(	(	kIx(	(
<g/>
Rhacophorus	Rhacophorus	k1gMnSc1	Rhacophorus
feae	fea	k1gFnSc2	fea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
ohrožovat	ohrožovat	k5eAaImF	ohrožovat
i	i	k9	i
odchyt	odchyt	k1gInSc4	odchyt
pro	pro	k7c4	pro
obchod	obchod	k1gInSc4	obchod
se	s	k7c7	s
zvířaty	zvíře	k1gNnPc7	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
létavkovitých	létavkovitý	k2eAgFnPc2d1	létavkovitý
též	též	k9	též
byla	být	k5eAaImAgFnS	být
zaznamenána	zaznamenán	k2eAgFnSc1d1	zaznamenána
plíseň	plíseň	k1gFnSc1	plíseň
Batrachochytrium	Batrachochytrium	k1gNnSc1	Batrachochytrium
dendrobatidis	dendrobatidis	k1gFnSc2	dendrobatidis
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
napadá	napadat	k5eAaImIp3nS	napadat
kůži	kůže	k1gFnSc4	kůže
žab	žába	k1gFnPc2	žába
<g/>
,	,	kIx,	,
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
jim	on	k3xPp3gMnPc3	on
tak	tak	k9	tak
v	v	k7c6	v
dýchání	dýchání	k1gNnSc6	dýchání
a	a	k8xC	a
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
smrtelné	smrtelný	k2eAgNnSc4d1	smrtelné
onemocnění	onemocnění	k1gNnSc4	onemocnění
chytridiomykózu	chytridiomykóza	k1gFnSc4	chytridiomykóza
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
většiny	většina	k1gFnSc2	většina
jiných	jiný	k2eAgFnPc2d1	jiná
žab	žába	k1gFnPc2	žába
lze	lze	k6eAd1	lze
využít	využít	k5eAaPmF	využít
k	k	k7c3	k
léčení	léčení	k1gNnSc3	léčení
itrakonazol	itrakonazola	k1gFnPc2	itrakonazola
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
senzitivnosti	senzitivnost	k1gFnSc3	senzitivnost
létavkovitých	létavkovitý	k2eAgFnPc2d1	létavkovitý
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
lék	lék	k1gInSc4	lék
však	však	k9	však
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
nelze	lze	k6eNd1	lze
tuto	tento	k3xDgFnSc4	tento
léčbu	léčba	k1gFnSc4	léčba
nasadit	nasadit	k5eAaPmF	nasadit
<g/>
.	.	kIx.	.
<g/>
Některé	některý	k3yIgFnPc1	některý
létavky	létavka	k1gFnPc1	létavka
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vyhynulé	vyhynulý	k2eAgNnSc1d1	vyhynulé
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
cejlonská	cejlonský	k2eAgFnSc1d1	cejlonská
létavka	létavka	k1gFnSc1	létavka
Pseudophilautus	Pseudophilautus	k1gInSc1	Pseudophilautus
extirpo	extirpa	k1gFnSc5	extirpa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
a	a	k8xC	a
naposledy	naposledy	k6eAd1	naposledy
spatřena	spatřen	k2eAgFnSc1d1	spatřena
roku	rok	k1gInSc2	rok
1882	[number]	k4	1882
a	a	k8xC	a
za	za	k7c4	za
jejíž	jejíž	k3xOyRp3gNnSc4	jejíž
vymření	vymření	k1gNnSc4	vymření
může	moct	k5eAaImIp3nS	moct
zřejmě	zřejmě	k6eAd1	zřejmě
ztráta	ztráta	k1gFnSc1	ztráta
biotopu	biotop	k1gInSc2	biotop
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
čeleď	čeleď	k1gFnSc1	čeleď
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
272	[number]	k4	272
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
celkem	celkem	k6eAd1	celkem
36	[number]	k4	36
kriticky	kriticky	k6eAd1	kriticky
ohrožených	ohrožený	k2eAgMnPc2d1	ohrožený
či	či	k8xC	či
vyhynulých	vyhynulý	k2eAgMnPc2d1	vyhynulý
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
o	o	k7c6	o
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
starších	starý	k2eAgInPc2d2	starší
propočtů	propočet	k1gInPc2	propočet
okolo	okolo	k7c2	okolo
13,2	[number]	k4	13,2
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
nebo	nebo	k8xC	nebo
vyhynulých	vyhynulý	k2eAgInPc2d1	vyhynulý
druhů	druh	k1gInPc2	druh
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
tehdy	tehdy	k6eAd1	tehdy
hodnoceném	hodnocený	k2eAgInSc6d1	hodnocený
rodu	rod	k1gInSc6	rod
Philautus	Philautus	k1gInSc1	Philautus
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
34	[number]	k4	34
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
představoval	představovat	k5eAaImAgInS	představovat
rod	rod	k1gInSc1	rod
žáby	žába	k1gFnSc2	žába
s	s	k7c7	s
třetím	třetí	k4xOgInSc7	třetí
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
počtem	počet	k1gInSc7	počet
takto	takto	k6eAd1	takto
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
po	po	k7c6	po
bezblance	bezblanka	k1gFnSc6	bezblanka
rodu	rod	k1gInSc2	rod
Eleutherodactylus	Eleutherodactylus	k1gInSc1	Eleutherodactylus
a	a	k8xC	a
rodu	rod	k1gInSc2	rod
atelopus	atelopus	k1gMnSc1	atelopus
(	(	kIx(	(
<g/>
Atelopus	Atelopus	k1gMnSc1	Atelopus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2018	[number]	k4	2018
bylo	být	k5eAaImAgNnS	být
Mezinárodním	mezinárodní	k2eAgInSc7d1	mezinárodní
svazem	svaz	k1gInSc7	svaz
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
jako	jako	k8xC	jako
vyhynulých	vyhynulý	k2eAgFnPc2d1	vyhynulá
hodnoceno	hodnocen	k2eAgNnSc1d1	hodnoceno
18	[number]	k4	18
druhů	druh	k1gInPc2	druh
létavkovitých	létavkovitý	k2eAgInPc2d1	létavkovitý
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
kriticky	kriticky	k6eAd1	kriticky
ohrožených	ohrožený	k2eAgNnPc2d1	ohrožené
již	již	k9	již
25	[number]	k4	25
druhů	druh	k1gInPc2	druh
a	a	k8xC	a
celkem	celkem	k6eAd1	celkem
143	[number]	k4	143
dalších	další	k2eAgMnPc2d1	další
bylo	být	k5eAaImAgNnS	být
buď	buď	k8xC	buď
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
<g/>
,	,	kIx,	,
zranitelných	zranitelný	k2eAgInPc2d1	zranitelný
či	či	k8xC	či
téměř	téměř	k6eAd1	téměř
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
<g/>
;	;	kIx,	;
k	k	k7c3	k
řadě	řada	k1gFnSc3	řada
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
pak	pak	k6eAd1	pak
chyběly	chybět	k5eAaImAgInP	chybět
údaje	údaj	k1gInPc1	údaj
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
status	status	k1gInSc1	status
byl	být	k5eAaImAgInS	být
stále	stále	k6eAd1	stále
nejasný	jasný	k2eNgInSc1d1	nejasný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
В	В	k?	В
л	л	k?	л
na	na	k7c6	na
ruské	ruský	k2eAgFnSc6d1	ruská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
létavkovití	létavkovitý	k2eAgMnPc1d1	létavkovitý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Rhacophoridae	Rhacophoridae	k1gInSc1	Rhacophoridae
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
