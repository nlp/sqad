<p>
<s>
Temple	templ	k1gInSc5	templ
je	on	k3xPp3gNnPc4	on
stanice	stanice	k1gFnSc1	stanice
londýnského	londýnský	k2eAgNnSc2d1	Londýnské
metra	metro	k1gNnSc2	metro
<g/>
,	,	kIx,	,
otevřená	otevřený	k2eAgFnSc1d1	otevřená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
:	:	kIx,	:
</s>
</p>
<p>
<s>
District	District	k1gInSc1	District
Line	linout	k5eAaImIp3nS	linout
a	a	k8xC	a
Circle	Circle	k1gFnSc1	Circle
Line	linout	k5eAaImIp3nS	linout
mezi	mezi	k7c7	mezi
stanicemi	stanice	k1gFnPc7	stanice
Embankment	Embankment	k1gInSc4	Embankment
a	a	k8xC	a
Blackfriars	Blackfriars	k1gInSc4	Blackfriars
<g/>
.	.	kIx.	.
<g/>
Ročně	ročně	k6eAd1	ročně
odbaví	odbavit	k5eAaPmIp3nS	odbavit
cca	cca	kA	cca
8	[number]	k4	8
milionů	milion	k4xCgInPc2	milion
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
</p>
