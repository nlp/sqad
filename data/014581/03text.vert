<s>
Sukulent	sukulent	k1gInSc1
</s>
<s>
List	list	k1gInSc1
Aloe	aloe	k1gFnSc2
vera	vera	k6eAd1
</s>
<s>
Sukulenty	sukulent	k1gInPc1
(	(	kIx(
<g/>
ze	z	k7c2
šp	šp	k?
<g/>
.	.	kIx.
suculento	suculento	k1gNnSc1
–	–	k?
šťavnatý	šťavnatý	k2eAgMnSc1d1
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
rostliny	rostlina	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
umí	umět	k5eAaImIp3nP
shromažďovat	shromažďovat	k5eAaImF
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
těle	tělo	k1gNnSc6
(	(	kIx(
<g/>
stonku	stonek	k1gInSc6
nebo	nebo	k8xC
listech	list	k1gInPc6
<g/>
)	)	kIx)
vodu	voda	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
jim	on	k3xPp3gMnPc3
umožňuje	umožňovat	k5eAaImIp3nS
přežít	přežít	k5eAaPmF
i	i	k9
velmi	velmi	k6eAd1
dlouhá	dlouhý	k2eAgNnPc4d1
období	období	k1gNnPc4
sucha	sucho	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
tak	tak	k6eAd1
uzpůsobené	uzpůsobený	k2eAgFnPc1d1
k	k	k7c3
přežití	přežití	k1gNnSc3
v	v	k7c6
pouštních	pouštní	k2eAgFnPc6d1
a	a	k8xC
polopouštních	polopouštní	k2eAgFnPc6d1
podmínkách	podmínka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Typické	typický	k2eAgInPc1d1
znaky	znak	k1gInPc1
</s>
<s>
vyžadují	vyžadovat	k5eAaImIp3nP
hodně	hodně	k6eAd1
světla	světlo	k1gNnSc2
a	a	k8xC
občasnou	občasný	k2eAgFnSc4d1
zálivku	zálivka	k1gFnSc4
(	(	kIx(
<g/>
sprchnutí	sprchnutí	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
CAM	CAM	kA
metabolismus	metabolismus	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
minimalizuje	minimalizovat	k5eAaBmIp3nS
ztrátu	ztráta	k1gFnSc4
vody	voda	k1gFnSc2
</s>
<s>
méně	málo	k6eAd2
průduchů	průduch	k1gInPc2
</s>
<s>
spíše	spíše	k9
mohutnější	mohutný	k2eAgInPc1d2
kmeny	kmen	k1gInPc1
než	než	k8xS
drobné	drobný	k2eAgFnPc1d1
větve	větev	k1gFnPc1
s	s	k7c7
lístky	lístek	k1gInPc7
<g/>
;	;	kIx,
kmeny	kmen	k1gInPc7
nebývají	bývat	k5eNaImIp3nP
zdřevnatělé	zdřevnatělý	k2eAgInPc1d1
a	a	k8xC
jsou	být	k5eAaImIp3nP
schopny	schopen	k2eAgFnPc1d1
fotosyntézy	fotosyntéza	k1gFnPc1
</s>
<s>
tělo	tělo	k1gNnSc1
těchto	tento	k3xDgFnPc2
rostlin	rostlina	k1gFnPc2
bývá	bývat	k5eAaImIp3nS
odlišné	odlišný	k2eAgNnSc1d1
od	od	k7c2
běžných	běžný	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
–	–	k?
sukulenty	sukulent	k1gInPc1
mají	mít	k5eAaImIp3nP
redukovanou	redukovaný	k2eAgFnSc4d1
formu	forma	k1gFnSc4
růstu	růst	k1gInSc2
<g/>
,	,	kIx,
„	„	k?
<g/>
kompaktnější	kompaktní	k2eAgNnSc1d2
<g/>
“	“	k?
tělo	tělo	k1gNnSc1
(	(	kIx(
<g/>
ne	ne	k9
tolik	tolik	k6eAd1
větvené	větvený	k2eAgFnPc1d1
jako	jako	k8xC,k8xS
u	u	k7c2
běžných	běžný	k2eAgInPc2d1
keřů	keř	k1gInPc2
nebo	nebo	k8xC
stromů	strom	k1gInPc2
<g/>
)	)	kIx)
<g/>
;	;	kIx,
nezdřevnatělé	zdřevnatělý	k2eNgNnSc1d1
<g/>
,	,	kIx,
většinou	většina	k1gFnSc7
dužinaté	dužinatý	k2eAgNnSc1d1
objemné	objemný	k2eAgNnSc1d1
tělo	tělo	k1gNnSc1
(	(	kIx(
<g/>
nebo	nebo	k8xC
jeho	jeho	k3xOp3gFnSc1
část	část	k1gFnSc1
–	–	k?
listy	list	k1gInPc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
kmen	kmen	k1gInSc1
<g/>
,	,	kIx,
kořeny	kořen	k1gInPc1
<g/>
,	,	kIx,
hlízy	hlíza	k1gFnPc1
<g/>
,	,	kIx,
…	…	k?
<g/>
)	)	kIx)
schopné	schopný	k2eAgFnPc1d1
změny	změna	k1gFnPc1
objemu	objem	k1gInSc2
(	(	kIx(
<g/>
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
příjmu	příjem	k1gInSc6
a	a	k8xC
akumulaci	akumulace	k1gFnSc6
vody	voda	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
povrch	povrch	k1gInSc1
je	být	k5eAaImIp3nS
nějakým	nějaký	k3yIgInSc7
způsobem	způsob	k1gInSc7
(	(	kIx(
<g/>
voskem	vosk	k1gInSc7
<g/>
,	,	kIx,
„	„	k?
<g/>
chloupky	chloupek	k1gInPc1
<g/>
“	“	k?
<g/>
)	)	kIx)
opatřen	opatřen	k2eAgInSc1d1
proti	proti	k7c3
rapidnímu	rapidní	k2eAgNnSc3d1
vysoušení	vysoušení	k1gNnSc3
</s>
<s>
Výskyt	výskyt	k1gInSc1
sukulentů	sukulent	k1gInPc2
</s>
<s>
Sukulenty	sukulent	k1gInPc1
se	se	k3xPyFc4
typicky	typicky	k6eAd1
vyskytují	vyskytovat	k5eAaImIp3nP
v	v	k7c6
suchých	suchý	k2eAgFnPc6d1
tropických	tropický	k2eAgFnPc6d1
a	a	k8xC
subtropických	subtropický	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
stepi	step	k1gFnPc4
<g/>
,	,	kIx,
polopouště	polopoušť	k1gFnPc4
a	a	k8xC
pouště	poušť	k1gFnPc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
jsou	být	k5eAaImIp3nP
vlivem	vliv	k1gInSc7
vysokých	vysoký	k2eAgFnPc2d1
teplot	teplota	k1gFnPc2
a	a	k8xC
nízkých	nízký	k2eAgFnPc2d1
srážek	srážka	k1gFnPc2
ke	k	k7c3
svému	svůj	k3xOyFgNnSc3
přežití	přežití	k1gNnSc3
nuceny	nucen	k2eAgFnPc1d1
akumulovat	akumulovat	k5eAaBmF
vodu	voda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyskytují	vyskytovat	k5eAaImIp3nP
se	se	k3xPyFc4
také	také	k9
v	v	k7c6
oblastech	oblast	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
sice	sice	k8xC
celoroční	celoroční	k2eAgInSc1d1
úhrn	úhrn	k1gInSc1
srážek	srážka	k1gFnPc2
vysoký	vysoký	k2eAgMnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
střídají	střídat	k5eAaImIp3nP
se	se	k3xPyFc4
zde	zde	k6eAd1
výrazná	výrazný	k2eAgNnPc4d1
období	období	k1gNnPc4
vlhka	vlhko	k1gNnSc2
a	a	k8xC
sucha	sucho	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Příklady	příklad	k1gInPc1
sukulentů	sukulent	k1gInPc2
</s>
<s>
Nejznámějšími	známý	k2eAgInPc7d3
sukulenty	sukulent	k1gInPc7
jsou	být	k5eAaImIp3nP
kaktusy	kaktus	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Téměř	téměř	k6eAd1
každý	každý	k3xTgInSc1
kaktus	kaktus	k1gInSc1
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c7
sukulenty	sukulent	k1gInPc7
<g/>
,	,	kIx,
ale	ale	k8xC
existují	existovat	k5eAaImIp3nP
i	i	k9
jiné	jiný	k2eAgInPc1d1
sukulenty	sukulent	k1gInPc1
než	než	k8xS
kaktusy	kaktus	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mohou	moct	k5eAaImIp3nP
existovat	existovat	k5eAaImF
i	i	k9
jako	jako	k9
epifyty	epifyt	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgInSc7d1
příkladem	příklad	k1gInSc7
je	být	k5eAaImIp3nS
Aloe	aloe	k1gFnSc1
<g/>
,	,	kIx,
z	z	k7c2
čeledi	čeleď	k1gFnSc2
Asphodelaceae	Asphodelacea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sukulenty	sukulent	k1gInPc7
většinou	většinou	k6eAd1
nejsou	být	k5eNaImIp3nP
vybíravé	vybíravý	k2eAgInPc1d1
na	na	k7c4
kvalitní	kvalitní	k2eAgFnSc4d1
půdu	půda	k1gFnSc4
–	–	k?
mohou	moct	k5eAaImIp3nP
vegetovat	vegetovat	k5eAaImF
na	na	k7c6
pouštích	poušť	k1gFnPc6
<g/>
,	,	kIx,
mořských	mořský	k2eAgNnPc6d1
pobřežích	pobřeží	k1gNnPc6
i	i	k9
v	v	k7c6
oblastech	oblast	k1gFnPc6
solných	solný	k2eAgFnPc2d1
pánví	pánev	k1gFnPc2
bohatých	bohatý	k2eAgFnPc2d1
na	na	k7c4
minerály	minerál	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
ČeleďPočet	ČeleďPočet	k1gInSc4
sukulentůPřizpůsobené	sukulentůPřizpůsobený	k2eAgNnSc4d1
částiRozšíření	částiRozšíření	k1gNnSc4
</s>
<s>
Agavaceae	Agavaceae	k1gNnSc1
-	-	kIx~
Agávovité	Agávovitý	k2eAgNnSc1d1
<g/>
300	#num#	k4
<g/>
listySeverní	listySeverní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
<g/>
,	,	kIx,
Střední	střední	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
</s>
<s>
Cactaceae	Cactaceae	k1gNnSc1
-	-	kIx~
Kaktusovité	kaktusovitý	k2eAgNnSc1d1
<g/>
1600	#num#	k4
<g/>
kmen	kmen	k1gInSc1
(	(	kIx(
<g/>
kořen	kořen	k1gInSc1
<g/>
,	,	kIx,
listy	list	k1gInPc1
<g/>
)	)	kIx)
<g/>
Severní	severní	k2eAgFnSc1d1
a	a	k8xC
Jižní	jižní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
</s>
<s>
Crassulaceae	Crassulaceae	k6eAd1
-	-	kIx~
Tlusticovité	Tlusticovitý	k2eAgInPc4d1
<g/>
1300	#num#	k4
<g/>
listy	list	k1gInPc4
(	(	kIx(
<g/>
kořen	kořen	k1gInSc4
<g/>
)	)	kIx)
<g/>
celosvětově	celosvětově	k6eAd1
</s>
<s>
Aizoaceae	Aizoaceae	k1gNnSc1
-	-	kIx~
Kosmatcovité	Kosmatcovitý	k2eAgNnSc1d1
<g/>
2000	#num#	k4
<g/>
listyJižní	listyJižní	k2eAgFnSc1d1
Afrika	Afrika	k1gFnSc1
</s>
<s>
Apocynaceae	Apocynaceae	k1gNnSc1
-	-	kIx~
Toješťovité	Toješťovitý	k2eAgNnSc1d1
<g/>
500	#num#	k4
<g/>
kmenAfrika	kmenAfrik	k1gMnSc2
<g/>
,	,	kIx,
Arabský	arabský	k2eAgInSc1d1
poloostrov	poloostrov	k1gInSc1
<g/>
,	,	kIx,
Indický	indický	k2eAgInSc1d1
subkontinent	subkontinent	k1gInSc1
</s>
<s>
Didiereaceae	Didiereaceae	k1gNnSc1
-	-	kIx~
Didierovité	Didierovitý	k2eAgNnSc1d1
<g/>
11	#num#	k4
<g/>
kmenMadagaskar	kmenMadagaskar	k1gInSc4
</s>
<s>
Euphorbiaceae	Euphorbiaceae	k1gNnSc1
-	-	kIx~
Pryšcovité	Pryšcovitý	k2eAgNnSc1d1
<g/>
>	>	kIx)
1000	#num#	k4
<g/>
kmen	kmen	k1gInSc1
<g/>
/	/	kIx~
<g/>
kořen	kořen	k1gInSc1
<g/>
/	/	kIx~
<g/>
listyAustrálie	listyAustrálie	k1gFnSc1
<g/>
,	,	kIx,
Afrika	Afrika	k1gFnSc1
<g/>
,	,	kIx,
Madagaskar	Madagaskar	k1gInSc1
<g/>
,	,	kIx,
subtropická	subtropický	k2eAgFnSc1d1
Asie	Asie	k1gFnSc1
<g/>
,	,	kIx,
Severní	severní	k2eAgFnSc1d1
i	i	k8xC
Jižní	jižní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
<g/>
,	,	kIx,
subtropická	subtropický	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
</s>
<s>
Asphodelaceae	Asphodelaceae	k1gNnSc1
-	-	kIx~
Asfodelovité	Asfodelovitý	k2eAgNnSc1d1
<g/>
500	#num#	k4
<g/>
listyAfrika	listyAfrika	k1gFnSc1
<g/>
,	,	kIx,
Madagaskar	Madagaskar	k1gInSc1
</s>
<s>
Portulacaceae	Portulacaceae	k1gFnSc1
-	-	kIx~
Šrucha	šrucha	k1gFnSc1
?	?	kIx.
</s>
<s desamb="1">
<g/>
listy	list	k1gInPc1
a	a	k8xC
kmenJižní	kmenJižní	k2eAgFnSc1d1
i	i	k8xC
Severní	severní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Rebutia	Rebutia	k1gFnSc1
muscula	muscout	k5eAaPmAgFnS
</s>
<s>
Beaucarnea	Beaucarne	k2eAgFnSc1d1
recurvata	recurvata	k1gFnSc1
</s>
<s>
Dracaena	Dracaena	k1gFnSc1
draco	draco	k6eAd1
</s>
<s>
Crassulaceae	Crassulaceae	k1gFnSc1
<g/>
:	:	kIx,
<g/>
Crassula	Crassula	k1gFnSc1
ovata	ovata	k1gMnSc1
-	-	kIx~
Tlustice	Tlustika	k1gFnSc6
</s>
<s>
Euphorbiaceae	Euphorbiaceae	k1gFnSc1
<g/>
:	:	kIx,
<g/>
Euphorbia	Euphorbia	k1gFnSc1
obesa	obesa	k1gFnSc1
ssp	ssp	k?
<g/>
.	.	kIx.
symmetrica	symmetrica	k1gMnSc1
</s>
<s>
Asphodelaceae	Asphodelaceae	k1gNnSc1
<g/>
:	:	kIx,
<g/>
Aloe	aloe	k1gNnSc1
vera	ver	k1gInSc2
</s>
<s>
Moringaceae	Moringaceae	k1gFnSc1
<g/>
:	:	kIx,
<g/>
Moringa	Moringa	k1gFnSc1
ovalifolia	ovalifolia	k1gFnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Succulent	Succulent	k1gMnSc1
plant	planta	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
sukulent	sukulent	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
https://web.archive.org/web/20140808185230/http://www.sukulenty.eu/co-je-to-sukulent-sukulentni-rostlina/	https://web.archive.org/web/20140808185230/http://www.sukulenty.eu/co-je-to-sukulent-sukulentni-rostlina/	k4
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
http://www.floridanaturepictures.com/cacti/cacti.html	http://www.floridanaturepictures.com/cacti/cacti.htmnout	k5eAaPmAgMnS
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
http://ralph.cs.cf.ac.uk/Cacti/fieldno.html	http://ralph.cs.cf.ac.uk/Cacti/fieldno.htmnout	k5eAaPmAgMnS
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Rostliny	rostlina	k1gFnPc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4058540-2	4058540-2	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85129595	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85129595	#num#	k4
</s>
