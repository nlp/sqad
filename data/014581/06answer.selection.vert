<s>
Sukulenty	sukulent	k1gInPc1
(	(	kIx(
<g/>
ze	z	k7c2
šp	šp	k?
<g/>
.	.	kIx.
suculento	suculento	k1gNnSc1
–	–	k?
šťavnatý	šťavnatý	k2eAgMnSc1d1
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
rostliny	rostlina	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
umí	umět	k5eAaImIp3nP
shromažďovat	shromažďovat	k5eAaImF
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
těle	tělo	k1gNnSc6
(	(	kIx(
<g/>
stonku	stonek	k1gInSc6
nebo	nebo	k8xC
listech	list	k1gInPc6
<g/>
)	)	kIx)
vodu	voda	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
jim	on	k3xPp3gMnPc3
umožňuje	umožňovat	k5eAaImIp3nS
přežít	přežít	k5eAaPmF
i	i	k9
velmi	velmi	k6eAd1
dlouhá	dlouhý	k2eAgNnPc4d1
období	období	k1gNnPc4
sucha	sucho	k1gNnSc2
<g/>
.	.	kIx.
</s>