<s desamb="1">
V	v	k7c4
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byla	být	k5eAaImAgFnS
15	#num#	k4
km	km	kA
severozápadně	severozápadně	k6eAd1
od	od	k7c2
města	město	k1gNnSc2
postavena	postaven	k2eAgFnSc1d1
Černobylská	černobylský	k2eAgFnSc1d1
jaderná	jaderný	k2eAgFnSc1d1
elektrárna	elektrárna	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
došlo	dojít	k5eAaPmAgNnS
roku	rok	k1gInSc2
1986	#num#	k4
k	k	k7c3
závažné	závažný	k2eAgFnSc3d1
havárii	havárie	k1gFnSc3
<g/>
,	,	kIx,
rozsahem	rozsah	k1gInSc7
následků	následek	k1gInPc2
nejhorší	zlý	k2eAgInSc4d3
v	v	k7c6
dějinách	dějiny	k1gFnPc6
jaderné	jaderný	k2eAgFnSc2d1
energetiky	energetika	k1gFnSc2
<g/>
.	.	kIx.
</s>