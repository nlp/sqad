<s>
Jak	jak	k6eAd1
se	se	k3xPyFc4
jmenuje	jmenovat	k5eAaImIp3nS
město	město	k1gNnSc1
<g/>
,	,	kIx,
u	u	k7c2
kterého	který	k3yRgNnSc2
byla	být	k5eAaImAgFnS
postavena	postaven	k2eAgFnSc1d1
elektrárna	elektrárna	k1gFnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
havárie	havárie	k1gFnSc1
roku	rok	k1gInSc2
1986	#num#	k4
<g/>
?	?	kIx.
</s>