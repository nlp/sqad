<s>
Iwodžima	Iwodžima	k1gFnSc1	Iwodžima
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
硫	硫	k?	硫
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
Iótó	Iótó	k1gFnSc1	Iótó
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
ale	ale	k9	ale
i	i	k9	i
Iódžima	Iódžima	k1gFnSc1	Iódžima
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
Ostrov	ostrov	k1gInSc1	ostrov
síry	síra	k1gFnSc2	síra
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vulkanický	vulkanický	k2eAgInSc1d1	vulkanický
ostrov	ostrov	k1gInSc1	ostrov
patřící	patřící	k2eAgInSc1d1	patřící
Japonsku	Japonsko	k1gNnSc3	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
přibližně	přibližně	k6eAd1	přibližně
1200	[number]	k4	1200
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Tokia	Tokio	k1gNnSc2	Tokio
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
°	°	k?	°
<g/>
47	[number]	k4	47
<g/>
'	'	kIx"	'
s.	s.	k?	s.
š.	š.	k?	š.
<g/>
,	,	kIx,	,
141	[number]	k4	141
<g/>
°	°	k?	°
<g/>
19	[number]	k4	19
<g/>
'	'	kIx"	'
v.	v.	k?	v.
d.	d.	k?	d.
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Vulkánových	vulkánový	k2eAgInPc2d1	vulkánový
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
jižní	jižní	k2eAgFnSc4d1	jižní
část	část	k1gFnSc4	část
ostrovů	ostrov	k1gInPc2	ostrov
Ogasawara	Ogasawar	k1gMnSc2	Ogasawar
<g/>
.	.	kIx.	.
</s>
<s>
Slavným	slavný	k2eAgMnPc3d1	slavný
se	se	k3xPyFc4	se
ostrov	ostrov	k1gInSc4	ostrov
stal	stát	k5eAaPmAgInS	stát
díky	díky	k7c3	díky
bitvě	bitva	k1gFnSc3	bitva
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
tu	tu	k6eAd1	tu
vybojovaly	vybojovat	k5eAaPmAgInP	vybojovat
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
a	a	k8xC	a
Japonsko	Japonsko	k1gNnSc1	Japonsko
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
a	a	k8xC	a
březnu	březen	k1gInSc6	březen
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
námořní	námořní	k2eAgFnSc1d1	námořní
pěchota	pěchota	k1gFnSc1	pěchota
ostrov	ostrov	k1gInSc4	ostrov
obsadila	obsadit	k5eAaPmAgFnS	obsadit
až	až	k9	až
po	po	k7c6	po
extrémně	extrémně	k6eAd1	extrémně
krvavých	krvavý	k2eAgInPc6d1	krvavý
bojích	boj	k1gInPc6	boj
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
přibližně	přibližně	k6eAd1	přibližně
21	[number]	k4	21
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
jediným	jediný	k2eAgInSc7d1	jediný
nápadnějším	nápadní	k2eAgInSc7d2	nápadní
bodem	bod	k1gInSc7	bod
je	být	k5eAaImIp3nS	být
hora	hora	k1gFnSc1	hora
Suribači	Suribač	k1gInSc3	Suribač
(	(	kIx(	(
<g/>
také	také	k9	také
Suribačijama	Suribačijamum	k1gNnSc2	Suribačijamum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyhaslý	vyhaslý	k2eAgMnSc1d1	vyhaslý
<g/>
,	,	kIx,	,
166	[number]	k4	166
m	m	kA	m
vysoký	vysoký	k2eAgInSc4d1	vysoký
vulkán	vulkán	k1gInSc4	vulkán
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
je	být	k5eAaImIp3nS	být
Iwodžima	Iwodžima	k1gFnSc1	Iwodžima
zcela	zcela	k6eAd1	zcela
plochá	plochý	k2eAgFnSc1d1	plochá
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
sopečnou	sopečný	k2eAgFnSc7d1	sopečná
činností	činnost	k1gFnSc7	činnost
neobvyklé	obvyklý	k2eNgFnPc1d1	neobvyklá
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2007	[number]	k4	2007
se	se	k3xPyFc4	se
oficiální	oficiální	k2eAgInSc1d1	oficiální
název	název	k1gInSc1	název
ostrova	ostrov	k1gInSc2	ostrov
změnil	změnit	k5eAaPmAgInS	změnit
zpět	zpět	k6eAd1	zpět
na	na	k7c6	na
Iótó	Iótó	k1gMnSc6	Iótó
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
ostrov	ostrov	k1gInSc4	ostrov
nosil	nosit	k5eAaImAgInS	nosit
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Tó	Tó	k?	Tó
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
jiným	jiný	k2eAgNnSc7d1	jiné
čtením	čtení	k1gNnSc7	čtení
znaku	znak	k1gInSc2	znak
šima	šim	k1gInSc2	šim
島	島	k?	島
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
zápis	zápis	k1gInSc1	zápis
názvu	název	k1gInSc2	název
v	v	k7c6	v
japonských	japonský	k2eAgInPc6d1	japonský
znacích	znak	k1gInPc6	znak
ani	ani	k8xC	ani
význam	význam	k1gInSc1	význam
názvu	název	k1gInSc2	název
nemění	měnit	k5eNaImIp3nS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
byl	být	k5eAaImAgInS	být
neobydlený	obydlený	k2eNgMnSc1d1	neobydlený
až	až	k9	až
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
jej	on	k3xPp3gMnSc4	on
kolonizovalo	kolonizovat	k5eAaBmAgNnS	kolonizovat
Japonsko	Japonsko	k1gNnSc1	Japonsko
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
žilo	žít	k5eAaImAgNnS	žít
1	[number]	k4	1
100	[number]	k4	100
japonských	japonský	k2eAgMnPc2d1	japonský
civilistů	civilista	k1gMnPc2	civilista
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
byla	být	k5eAaImAgFnS	být
zaměstnána	zaměstnán	k2eAgFnSc1d1	zaměstnána
<g/>
,	,	kIx,	,
buď	buď	k8xC	buď
v	v	k7c6	v
cukrovaru	cukrovar	k1gInSc6	cukrovar
postaveném	postavený	k2eAgInSc6d1	postavený
v	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
části	část	k1gFnSc6	část
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
sirném	sirný	k2eAgInSc6d1	sirný
dole	dol	k1gInSc6	dol
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
obyvatelé	obyvatel	k1gMnPc1	obyvatel
ostrova	ostrov	k1gInSc2	ostrov
žili	žít	k5eAaImAgMnP	žít
v	v	k7c6	v
pěti	pět	k4xCc6	pět
osadách	osada	k1gFnPc6	osada
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polovině	polovina	k1gFnSc6	polovina
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Nejsevernější	severní	k2eAgMnPc1d3	nejsevernější
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byla	být	k5eAaImAgFnS	být
osada	osada	k1gFnSc1	osada
Kita	Kita	k1gFnSc1	Kita
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
Sever	sever	k1gInSc1	sever
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
osada	osada	k1gFnSc1	osada
Niši	Niš	k1gFnSc2	Niš
(	(	kIx(	(
<g/>
Západ	západ	k1gInSc1	západ
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnPc1d3	veliký
osada	osada	k1gFnSc1	osada
Motojama	Motojama	k1gFnSc1	Motojama
ležela	ležet	k5eAaImAgFnS	ležet
u	u	k7c2	u
dolu	dol	k1gInSc2	dol
na	na	k7c4	na
síru	síra	k1gFnSc4	síra
a	a	k8xC	a
Higaši	Higaše	k1gFnSc4	Higaše
(	(	kIx(	(
<g/>
Východ	východ	k1gInSc4	východ
<g/>
)	)	kIx)	)
a	a	k8xC	a
Minami	mina	k1gFnPc7	mina
(	(	kIx(	(
<g/>
Jih	jih	k1gInSc1	jih
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
situovány	situovat	k5eAaBmNgInP	situovat
v	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
části	část	k1gFnSc6	část
ostrova	ostrov	k1gInSc2	ostrov
Iwodžima	Iwodžima	k1gFnSc1	Iwodžima
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
dějištěm	dějiště	k1gNnSc7	dějiště
důležité	důležitý	k2eAgFnSc2d1	důležitá
bitvy	bitva	k1gFnSc2	bitva
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Bitva	bitva	k1gFnSc1	bitva
o	o	k7c4	o
Iwodžimu	Iwodžima	k1gFnSc4	Iwodžima
probíhala	probíhat	k5eAaImAgFnS	probíhat
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1945	[number]	k4	1945
do	do	k7c2	do
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1945	[number]	k4	1945
a	a	k8xC	a
jejím	její	k3xOp3gInSc7	její
důsledkem	důsledek	k1gInSc7	důsledek
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
ostrov	ostrov	k1gInSc1	ostrov
zůstal	zůstat	k5eAaPmAgInS	zůstat
okupován	okupovat	k5eAaBmNgInS	okupovat
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
na	na	k7c6	na
hoře	hora	k1gFnSc6	hora
Suribači	Suribač	k1gMnPc1	Suribač
pořídil	pořídit	k5eAaPmAgInS	pořídit
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1945	[number]	k4	1945
Joe	Joe	k1gMnSc1	Joe
Rosenthal	Rosenthal	k1gMnSc1	Rosenthal
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejslavnějších	slavný	k2eAgFnPc2d3	nejslavnější
fotografií	fotografia	k1gFnPc2	fotografia
celé	celý	k2eAgFnSc2d1	celá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
na	na	k7c6	na
Iwodžimě	Iwodžima	k1gFnSc6	Iwodžima
nežijí	žít	k5eNaImIp3nP	žít
žádní	žádný	k3yNgMnPc1	žádný
stálí	stálý	k2eAgMnPc1d1	stálý
obyvatelé	obyvatel	k1gMnPc1	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
pouze	pouze	k6eAd1	pouze
umístěna	umístěn	k2eAgFnSc1d1	umístěna
základna	základna	k1gFnSc1	základna
japonského	japonský	k2eAgNnSc2d1	Japonské
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vstup	vstup	k1gInSc4	vstup
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
zvláštního	zvláštní	k2eAgNnSc2d1	zvláštní
povolení	povolení	k1gNnSc2	povolení
<g/>
.	.	kIx.	.
</s>
