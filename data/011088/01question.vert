<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
vrch	vrch	k1gInSc1	vrch
o	o	k7c6	o
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
462	[number]	k4	462
metrů	metr	k1gInPc2	metr
v	v	k7c6	v
jižním	jižní	k2eAgInSc6d1	jižní
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
,	,	kIx,	,
v	v	k7c4	v
pohoří	pohoří	k1gNnSc4	pohoří
Harej	Harej	k1gInSc4	Harej
Ejlat	Ejlat	k2eAgInSc4d1	Ejlat
<g/>
?	?	kIx.	?
</s>
