<p>
<s>
Har	Har	k?	Har
Ora	Ora	k1gFnSc1	Ora
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
:	:	kIx,	:
ה	ה	k?	ה
א	א	k?	א
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vrch	vrch	k1gInSc4	vrch
o	o	k7c6	o
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
462	[number]	k4	462
metrů	metr	k1gInPc2	metr
v	v	k7c6	v
jižním	jižní	k2eAgInSc6d1	jižní
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
,	,	kIx,	,
v	v	k7c4	v
pohoří	pohoří	k1gNnSc4	pohoří
Harej	Harej	k1gInSc4	Harej
Ejlat	Ejlat	k2eAgInSc4d1	Ejlat
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
cca	cca	kA	cca
15	[number]	k4	15
kilometrů	kilometr	k1gInPc2	kilometr
severně	severně	k6eAd1	severně
od	od	k7c2	od
města	město	k1gNnSc2	město
Ejlat	Ejlat	k1gInSc1	Ejlat
<g/>
,	,	kIx,	,
3	[number]	k4	3
kilometry	kilometr	k1gInPc7	kilometr
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
vesnice	vesnice	k1gFnSc2	vesnice
Be	Be	k1gFnSc2	Be
<g/>
'	'	kIx"	'
<g/>
er	er	k?	er
Ora	Ora	k1gFnPc2	Ora
a	a	k8xC	a
5	[number]	k4	5
kilometrů	kilometr	k1gInPc2	kilometr
západně	západně	k6eAd1	západně
od	od	k7c2	od
mezistátní	mezistátní	k2eAgFnSc2d1	mezistátní
hranice	hranice	k1gFnSc2	hranice
mezi	mezi	k7c7	mezi
Izraelem	Izrael	k1gInSc7	Izrael
a	a	k8xC	a
Jordánskem	Jordánsko	k1gNnSc7	Jordánsko
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
výrazného	výrazný	k2eAgInSc2d1	výrazný
odlesněného	odlesněný	k2eAgInSc2d1	odlesněný
skalního	skalní	k2eAgInSc2d1	skalní
masivu	masiv	k1gInSc2	masiv
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
svahy	svah	k1gInPc1	svah
spadají	spadat	k5eAaImIp3nP	spadat
do	do	k7c2	do
hlubokých	hluboký	k2eAgFnPc2d1	hluboká
údolí	údolí	k1gNnPc2	údolí
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgInPc7	jenž
protékají	protékat	k5eAaImIp3nP	protékat
vádí	vádí	k1gNnSc1	vádí
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
jde	jít	k5eAaImIp3nS	jít
o	o	k7c6	o
údolí	údolí	k1gNnSc6	údolí
vádí	vádí	k1gNnSc6	vádí
Nachal	Nachal	k1gMnSc1	Nachal
Racham	Racham	k1gInSc1	Racham
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
Nachal	Nachal	k1gMnSc1	Nachal
Ora	Ora	k1gMnSc1	Ora
na	na	k7c6	na
východě	východ	k1gInSc6	východ
a	a	k8xC	a
Nachal	Nachal	k1gMnSc1	Nachal
Nicoc	Nicoc	k1gFnSc4	Nicoc
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
<g/>
.	.	kIx.	.
</s>
<s>
Krajina	Krajina	k1gFnSc1	Krajina
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
hory	hora	k1gFnSc2	hora
je	být	k5eAaImIp3nS	být
členěna	členit	k5eAaImNgFnS	členit
četnými	četný	k2eAgInPc7d1	četný
skalnatými	skalnatý	k2eAgInPc7d1	skalnatý
vrchy	vrch	k1gInPc7	vrch
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
vrcholové	vrcholový	k2eAgFnSc2d1	vrcholová
partie	partie	k1gFnSc2	partie
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
skalnatý	skalnatý	k2eAgInSc4d1	skalnatý
hřbet	hřbet	k1gInSc4	hřbet
Cukej	cukat	k5eAaImRp2nS	cukat
Avrona	Avrona	k1gFnSc1	Avrona
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
straně	strana	k1gFnSc6	strana
se	se	k3xPyFc4	se
terén	terén	k1gInSc1	terén
svažuje	svažovat	k5eAaImIp3nS	svažovat
do	do	k7c2	do
příkopové	příkopový	k2eAgFnSc2d1	příkopová
propadliny	propadlina	k1gFnSc2	propadlina
vádí	vádí	k1gNnPc2	vádí
al-Araba	al-Araba	k1gMnSc1	al-Araba
<g/>
.	.	kIx.	.
</s>
<s>
Okolí	okolí	k1gNnSc1	okolí
hory	hora	k1gFnSc2	hora
je	být	k5eAaImIp3nS	být
turisticky	turisticky	k6eAd1	turisticky
využíváno	využívat	k5eAaImNgNnS	využívat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jejích	její	k3xOp3gInPc6	její
západních	západní	k2eAgInPc6d1	západní
svazích	svah	k1gInPc6	svah
vede	vést	k5eAaImIp3nS	vést
Izraelská	izraelský	k2eAgFnSc1d1	izraelská
stezka	stezka	k1gFnSc1	stezka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Ejlat	Ejlat	k1gInSc1	Ejlat
</s>
</p>
