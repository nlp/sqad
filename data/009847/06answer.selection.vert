<s>
Většina	většina	k1gFnSc1	většina
SUV	SUV	kA	SUV
má	mít	k5eAaImIp3nS	mít
pohon	pohon	k1gInSc4	pohon
všech	všecek	k3xTgNnPc2	všecek
kol	kolo	k1gNnPc2	kolo
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k9	jako
4WD	[number]	k4	4WD
nebo	nebo	k8xC	nebo
4	[number]	k4	4
<g/>
×	×	k?	×
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
těšit	těšit	k5eAaImF	těšit
z	z	k7c2	z
výhod	výhoda	k1gFnPc2	výhoda
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
toto	tento	k3xDgNnSc4	tento
technické	technický	k2eAgNnSc4d1	technické
řešení	řešení	k1gNnSc4	řešení
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
<g/>
.	.	kIx.	.
</s>
