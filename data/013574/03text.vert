<s>
Smrk	smrk	k1gInSc1
sivý	sivý	k2eAgInSc1d1
</s>
<s>
Smrk	smrk	k1gInSc1
sivý	sivý	k2eAgInSc4d1
Smrk	smrk	k1gInSc4
sivý	sivý	k2eAgInSc1d1
Stupeň	stupeň	k1gInSc1
ohrožení	ohrožení	k1gNnSc2
podle	podle	k7c2
IUCN	IUCN	kA
</s>
<s>
málo	málo	k6eAd1
dotčený	dotčený	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Plantae	Plantae	k1gFnSc1
<g/>
)	)	kIx)
Podříše	podříše	k1gFnSc1
</s>
<s>
cévnaté	cévnatý	k2eAgFnPc1d1
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Tracheobionta	Tracheobionta	k1gFnSc1
<g/>
)	)	kIx)
Oddělení	oddělení	k1gNnSc1
</s>
<s>
nahosemenné	nahosemenný	k2eAgNnSc1d1
(	(	kIx(
<g/>
Pinophyta	Pinophyta	k1gFnSc1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
jehličnany	jehličnan	k1gInPc1
(	(	kIx(
<g/>
Pinopsida	Pinopsida	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
borovicotvaré	borovicotvarý	k2eAgFnPc1d1
(	(	kIx(
<g/>
Pinales	Pinales	k1gInSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
borovicovité	borovicovitý	k2eAgInPc4d1
(	(	kIx(
<g/>
Pinaceae	Pinacea	k1gInPc4
<g/>
)	)	kIx)
Rod	rod	k1gInSc4
</s>
<s>
smrk	smrk	k1gInSc1
(	(	kIx(
<g/>
Picea	Picea	k1gFnSc1
<g/>
)	)	kIx)
Binomické	binomický	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Picea	Pice	k2eAgFnSc1d1
glauca	glauca	k1gFnSc1
<g/>
(	(	kIx(
<g/>
Moench	Moench	k1gInSc1
<g/>
)	)	kIx)
Voss	Voss	k1gInSc1
<g/>
,	,	kIx,
1907	#num#	k4
</s>
<s>
Areál	areál	k1gInSc1
rozšíření	rozšíření	k1gNnSc2
</s>
<s>
Areál	areál	k1gInSc1
rozšíření	rozšíření	k1gNnSc2
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Smrk	smrk	k1gInSc1
sivý	sivý	k2eAgInSc1d1
(	(	kIx(
<g/>
Picea	Picea	k2eAgFnSc1d1
glauca	glauca	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
druh	druh	k1gInSc4
smrku	smrk	k1gInSc2
<g/>
,	,	kIx,
původem	původ	k1gInSc7
ze	z	k7c2
Severní	severní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
vždyzelený	vždyzelený	k2eAgInSc4d1
středně	středně	k6eAd1
velký	velký	k2eAgInSc4d1
strom	strom	k1gInSc4
vysoký	vysoký	k2eAgInSc4d1
od	od	k7c2
15	#num#	k4
do	do	k7c2
30	#num#	k4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
výjimečně	výjimečně	k6eAd1
až	až	k9
40	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kmen	kmen	k1gInSc1
má	mít	k5eAaImIp3nS
průměr	průměr	k1gInSc4
až	až	k9
1	#num#	k4
metr	metr	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kůra	kůra	k1gFnSc1
je	být	k5eAaImIp3nS
tenká	tenký	k2eAgFnSc1d1
a	a	k8xC
šupinatá	šupinatý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koruna	koruna	k1gFnSc1
ve	v	k7c6
tvaru	tvar	k1gInSc6
úzkého	úzký	k2eAgInSc2d1
kuželu	kužel	k1gInSc2
u	u	k7c2
mladých	mladý	k2eAgInPc2d1
stromů	strom	k1gInPc2
se	se	k3xPyFc4
stává	stávat	k5eAaImIp3nS
cylindrickou	cylindrický	k2eAgFnSc4d1
u	u	k7c2
starších	starý	k2eAgMnPc2d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
středoevropských	středoevropský	k2eAgFnPc6d1
podmínkách	podmínka	k1gFnPc6
dorůstá	dorůstat	k5eAaImIp3nS
podstatně	podstatně	k6eAd1
menších	malý	k2eAgInPc2d2
rozměrů	rozměr	k1gInPc2
<g/>
,	,	kIx,
rovněž	rovněž	k9
na	na	k7c6
severní	severní	k2eAgFnSc6d1
hranici	hranice	k1gFnSc6
areálu	areál	k1gInSc2
dosahuje	dosahovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
keřovitých	keřovitý	k2eAgFnPc2d1
forem	forma	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jehlice	jehlice	k1gFnPc1
mají	mít	k5eAaImIp3nP
délku	délka	k1gFnSc4
mezi	mezi	k7c7
12	#num#	k4
-	-	kIx~
20	#num#	k4
mm	mm	kA
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
mírně	mírně	k6eAd1
zahnuté	zahnutý	k2eAgInPc1d1
<g/>
,	,	kIx,
stříbřitě	stříbřitě	k6eAd1
ojíněné	ojíněný	k2eAgFnPc4d1
a	a	k8xC
tupé	tupý	k2eAgFnPc4d1
<g/>
,	,	kIx,
nepichlavé	pichlavý	k2eNgFnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Válcovité	válcovitý	k2eAgFnPc4d1
<g/>
,	,	kIx,
štíhlé	štíhlý	k2eAgFnPc4d1
šišky	šiška	k1gFnPc4
jsou	být	k5eAaImIp3nP
světle	světle	k6eAd1
hnědé	hnědý	k2eAgInPc1d1
<g/>
,	,	kIx,
tuhé	tuhý	k2eAgInPc1d1
<g/>
,	,	kIx,
3	#num#	k4
-	-	kIx~
7	#num#	k4
cm	cm	kA
dlouhé	dlouhý	k2eAgFnPc1d1
<g/>
,	,	kIx,
1,5	1,5	k4
cm	cm	kA
široké	široký	k2eAgNnSc1d1
a	a	k8xC
dozrávají	dozrávat	k5eAaImIp3nP
koncem	koncem	k7c2
srpna	srpen	k1gInSc2
a	a	k8xC
v	v	k7c6
září	září	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sbírány	sbírán	k2eAgFnPc4d1
jsou	být	k5eAaImIp3nP
veverkami	veverka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šišky	Šiška	k1gMnPc7
nasazuje	nasazovat	k5eAaImIp3nS
velmi	velmi	k6eAd1
brzo	brzo	k6eAd1
<g/>
,	,	kIx,
už	už	k6eAd1
před	před	k7c7
10	#num#	k4
<g/>
.	.	kIx.
rokem	rok	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Kořenový	kořenový	k2eAgInSc1d1
systém	systém	k1gInSc1
je	být	k5eAaImIp3nS
převážně	převážně	k6eAd1
povrchový	povrchový	k2eAgInSc1d1
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
jejich	jejich	k3xOp3gFnSc1
menší	malý	k2eAgFnSc1d2
část	část	k1gFnSc1
zasahuje	zasahovat	k5eAaImIp3nS
do	do	k7c2
větší	veliký	k2eAgFnSc2d2
hloubky	hloubka	k1gFnSc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
</s>
<s>
Je	být	k5eAaImIp3nS
nejseverněji	severně	k6eAd3
zasahujícím	zasahující	k2eAgInSc7d1
smrkem	smrk	k1gInSc7
<g/>
,	,	kIx,
zabírajícím	zabírající	k2eAgInSc7d1
rozsáhlý	rozsáhlý	k2eAgInSc4d1
areál	areál	k1gInSc4
především	především	k9
v	v	k7c6
boreální	boreální	k2eAgFnSc6d1
části	část	k1gFnSc6
Kanady	Kanada	k1gFnSc2
od	od	k7c2
Newfoundlandu	Newfoundland	k1gInSc2
na	na	k7c6
východě	východ	k1gInSc6
až	až	k9
po	po	k7c4
pacifické	pacifický	k2eAgNnSc4d1
pobřeží	pobřeží	k1gNnSc4
Aljašky	Aljaška	k1gFnSc2
na	na	k7c6
západě	západ	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
severu	sever	k1gInSc6
zasahuje	zasahovat	k5eAaImIp3nS
až	až	k9
k	k	k7c3
arktické	arktický	k2eAgFnSc3d1
hranici	hranice	k1gFnSc3
lesa	les	k1gInSc2
<g/>
,	,	kIx,
téměř	téměř	k6eAd1
až	až	k9
k	k	k7c3
Severnímu	severní	k2eAgInSc3d1
ledovému	ledový	k2eAgInSc3d1
oceánu	oceán	k1gInSc3
<g/>
;	;	kIx,
na	na	k7c6
jihu	jih	k1gInSc6
potom	potom	k8xC
k	k	k7c3
hranici	hranice	k1gFnSc3
s	s	k7c7
USA	USA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
V	v	k7c6
dřevařském	dřevařský	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
se	se	k3xPyFc4
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
na	na	k7c4
vlákninu	vláknina	k1gFnSc4
a	a	k8xC
stavební	stavební	k2eAgNnSc4d1
dříví	dříví	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hojně	hojně	k6eAd1
byl	být	k5eAaImAgInS
využíván	využívat	k5eAaImNgInS,k5eAaPmNgInS
původními	původní	k2eAgInPc7d1
indiánskými	indiánský	k2eAgInPc7d1
kmeny	kmen	k1gInPc7
<g/>
,	,	kIx,
především	především	k6eAd1
jeho	jeho	k3xOp3gInPc4
pružné	pružný	k2eAgInPc4d1
kořeny	kořen	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pěstování	pěstování	k1gNnSc1
</s>
<s>
Pěstování	pěstování	k1gNnSc1
nečiní	činit	k5eNaImIp3nS
potíže	potíž	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyžaduje	vyžadovat	k5eAaImIp3nS
spíše	spíše	k9
vlhčí	vlhký	k2eAgNnPc4d2
stanoviště	stanoviště	k1gNnPc4
a	a	k8xC
provzdušněné	provzdušněný	k2eAgFnPc4d1
<g/>
,	,	kIx,
úrodné	úrodný	k2eAgFnPc4d1
půdy	půda	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snadno	snadno	k6eAd1
se	se	k3xPyFc4
množí	množit	k5eAaImIp3nP
ze	z	k7c2
semen	semeno	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yQgNnPc1,k3yRgNnPc1
jsou	být	k5eAaImIp3nP
dobře	dobře	k6eAd1
klíčivá	klíčivý	k2eAgNnPc1d1
i	i	k8xC
z	z	k7c2
mladých	mladý	k2eAgInPc2d1
stromů	strom	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bývá	bývat	k5eAaImIp3nS
često	često	k1gNnSc4
napadán	napadán	k2eAgInSc1d1
patogenní	patogenní	k2eAgFnSc7d1
houbou	houba	k1gFnSc7
sypavkou	sypavka	k1gFnSc7
smrku	smrk	k1gInSc3
poškozující	poškozující	k2eAgNnSc1d1
jehličí	jehličí	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trpívá	trpívat	k5eAaImIp3nS
šokem	šok	k1gInSc7
z	z	k7c2
přesazení	přesazení	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nejčastěji	často	k6eAd3
se	se	k3xPyFc4
pěstuje	pěstovat	k5eAaImIp3nS
zakrslý	zakrslý	k2eAgInSc1d1
kuželový	kuželový	k2eAgInSc1d1
kultivar	kultivar	k1gInSc1
Picea	Piceus	k1gMnSc2
glauca	glaucus	k1gMnSc2
'	'	kIx"
<g/>
Conica	Conica	k1gMnSc1
<g/>
'	'	kIx"
(	(	kIx(
<g/>
tzv.	tzv.	kA
"	"	kIx"
<g/>
homole	homole	k1gFnSc1
cukru	cukr	k1gInSc2
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vypěstovaný	vypěstovaný	k2eAgInSc1d1
z	z	k7c2
vrcholového	vrcholový	k2eAgInSc2d1
čarověníku	čarověník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
kultivar	kultivar	k1gInSc1
se	se	k3xPyFc4
velice	velice	k6eAd1
snadno	snadno	k6eAd1
množí	množit	k5eAaImIp3nP
řízkováním	řízkování	k1gNnSc7
a	a	k8xC
v	v	k7c6
amatérských	amatérský	k2eAgFnPc6d1
podmínkách	podmínka	k1gFnPc6
hřížením	hřížení	k1gNnPc3
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
The	The	k1gFnSc1
IUCN	IUCN	kA
Red	Red	k1gFnSc1
List	list	k1gInSc1
of	of	k?
Threatened	Threatened	k1gInSc1
Species	species	k1gFnSc1
2021.1	2021.1	k4
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Musil	Musil	k1gMnSc1
<g/>
,	,	kIx,
Ivanː	Ivanː	k1gFnSc1
Lesnická	lesnický	k2eAgFnSc1d1
dendrologie	dendrologie	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jehličnaté	jehličnatý	k2eAgFnPc4d1
dřeviny	dřevina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prahaː	Prahaː	k1gFnSc1
ČZU	ČZU	kA
2003	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
smrk	smrk	k1gInSc1
sivý	sivý	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Taxon	taxon	k1gInSc1
Picea	Piceus	k1gMnSc2
glauca	glaucus	k1gMnSc2
ve	v	k7c6
Wikidruzích	Wikidruze	k1gFnPc6
</s>
<s>
Smrk	smrk	k1gInSc1
sivý	sivý	k2eAgInSc1d1
na	na	k7c6
biolibu	biolib	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Rod	rod	k1gInSc1
smrk	smrk	k1gInSc1
(	(	kIx(
<g/>
Picea	Picea	k1gFnSc1
<g/>
)	)	kIx)
Evropa	Evropa	k1gFnSc1
</s>
<s>
smrk	smrk	k1gInSc1
ztepilý	ztepilý	k2eAgInSc1d1
•	•	k?
smrk	smrk	k1gInSc1
omorika	omorika	k1gFnSc1
Asie	Asie	k1gFnSc1
</s>
<s>
smrk	smrk	k1gInSc4
ajanský	ajanský	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
Glehnův	Glehnův	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
dvoubarvý	dvoubarvý	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
chupejský	chupejský	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
Koyamův	Koyamův	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
korejský	korejský	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
liťiangský	liťiangský	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
lesklý	lesklý	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
Maximovičův	Maximovičův	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
Meyerův	Meyerův	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
nachový	nachový	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
Schrenkův	Schrenkův	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
sibiřský	sibiřský	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
štětinatý	štětinatý	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
východní	východní	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc1
Wilsonův	Wilsonův	k2eAgInSc1d1
•	•	k?
Picea	Picea	k1gFnSc1
brachytyla	brachytýt	k5eAaPmAgFnS
•	•	k?
Picea	Pice	k2eAgFnSc1d1
crassifolia	crassifolia	k1gFnSc1
•	•	k?
Picea	Picea	k1gFnSc1
farreri	farrer	k1gFnSc2
•	•	k?
Picea	Pice	k2eAgFnSc1d1
morrisonicola	morrisonicola	k1gFnSc1
•	•	k?
Picea	Pice	k2eAgFnSc1d1
spinulosa	spinulosa	k1gFnSc1
•	•	k?
Picea	Picea	k1gFnSc1
smithiana	smithiana	k1gFnSc1
Amerika	Amerika	k1gFnSc1
</s>
<s>
smrk	smrk	k1gInSc4
Brewerův	Brewerův	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
Engelmannův	Engelmannův	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
černý	černý	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
červený	červený	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
pichlavý	pichlavý	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
sivý	sivý	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
sitka	sitek	k1gMnSc2
•	•	k?
Picea	Piceus	k1gMnSc2
chihuahuana	chihuahuan	k1gMnSc2
•	•	k?
Picea	Picea	k1gMnSc1
martinezii	martinezie	k1gFnSc3
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Rostliny	rostlina	k1gFnPc1
|	|	kIx~
Stromy	strom	k1gInPc1
</s>
