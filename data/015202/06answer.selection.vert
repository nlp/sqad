<s>
Krevní	krevní	k2eAgFnSc1d1
msta	msta	k1gFnSc1
(	(	kIx(
<g/>
něm.	něm.	k?
Blutrache	Blutrache	k1gInSc1
<g/>
,	,	kIx,
fr.	fr.	k?
vengeance	vengeance	k1gFnSc1
sanglante	sanglant	k1gMnSc5
<g/>
,	,	kIx,
lat.	lat.	k?
inimicitia	inimicitia	k1gFnSc1
capitalis	capitalis	k1gFnSc1
nebo	nebo	k8xC
mortalis	mortalis	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
právní	právní	k2eAgInSc4d1
institut	institut	k1gInSc4
ve	v	k7c6
všech	všecek	k3xTgFnPc6
rodových	rodový	k2eAgFnPc6d1
(	(	kIx(
<g/>
tj.	tj.	kA
předstátních	předstátní	k2eAgFnPc6d1
<g/>
)	)	kIx)
společnostech	společnost	k1gFnPc6
a	a	k8xC
jako	jako	k9
přežitek	přežitek	k1gInSc4
i	i	k8xC
v	v	k7c6
některých	některý	k3yIgInPc6
státech	stát	k1gInPc6
spočívající	spočívající	k2eAgFnSc1d1
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
příbuzný	příbuzný	k1gMnSc1
(	(	kIx(
<g/>
příslušník	příslušník	k1gMnSc1
rodu	rod	k1gInSc2
<g/>
/	/	kIx~
<g/>
klanu	klan	k1gInSc2
<g/>
)	)	kIx)
zabitého	zabitý	k1gMnSc4
má	mít	k5eAaImIp3nS
právo	právo	k1gNnSc4
a	a	k8xC
zároveň	zároveň	k6eAd1
i	i	k9
povinnost	povinnost	k1gFnSc1
pomstít	pomstít	k5eAaPmF
vraždu	vražda	k1gFnSc4
vlastní	vlastní	k2eAgFnSc7d1
rukou	ruka	k1gFnSc7
<g/>
.	.	kIx.
</s>