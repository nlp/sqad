<s>
Krevní	krevní	k2eAgFnSc1d1
msta	msta	k1gFnSc1
</s>
<s>
O	o	k7c6
dílu	díl	k1gInSc6
seriálu	seriál	k1gInSc2
Simpsonovi	Simpson	k1gMnSc6
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Krevní	krevní	k2eAgFnSc1d1
msta	msta	k1gFnSc1
(	(	kIx(
<g/>
Simpsonovi	Simpsonův	k2eAgMnPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Krevní	krevní	k2eAgFnSc1d1
msta	msta	k1gFnSc1
(	(	kIx(
<g/>
něm.	něm.	k?
Blutrache	Blutrache	k1gInSc1
<g/>
,	,	kIx,
fr.	fr.	k?
vengeance	vengeance	k1gFnSc1
sanglante	sanglant	k1gMnSc5
<g/>
,	,	kIx,
lat.	lat.	k?
inimicitia	inimicitia	k1gFnSc1
capitalis	capitalis	k1gFnSc1
nebo	nebo	k8xC
mortalis	mortalis	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
právní	právní	k2eAgInSc4d1
institut	institut	k1gInSc4
ve	v	k7c6
všech	všecek	k3xTgFnPc6
rodových	rodový	k2eAgFnPc6d1
(	(	kIx(
<g/>
tj.	tj.	kA
předstátních	předstátní	k2eAgFnPc6d1
<g/>
)	)	kIx)
společnostech	společnost	k1gFnPc6
a	a	k8xC
jako	jako	k9
přežitek	přežitek	k1gInSc4
i	i	k8xC
v	v	k7c6
některých	některý	k3yIgInPc6
státech	stát	k1gInPc6
spočívající	spočívající	k2eAgFnSc1d1
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
příbuzný	příbuzný	k1gMnSc1
(	(	kIx(
<g/>
příslušník	příslušník	k1gMnSc1
rodu	rod	k1gInSc2
<g/>
/	/	kIx~
<g/>
klanu	klan	k1gInSc2
<g/>
)	)	kIx)
zabitého	zabitý	k1gMnSc4
má	mít	k5eAaImIp3nS
právo	právo	k1gNnSc4
a	a	k8xC
zároveň	zároveň	k6eAd1
i	i	k9
povinnost	povinnost	k1gFnSc1
pomstít	pomstít	k5eAaPmF
vraždu	vražda	k1gFnSc4
vlastní	vlastní	k2eAgFnSc7d1
rukou	ruka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krevní	krevní	k2eAgFnSc1d1
msta	msta	k1gFnSc1
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
již	již	k6eAd1
s	s	k7c7
příchodem	příchod	k1gInSc7
zemědělství	zemědělství	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Mezi	mezi	k7c4
nejstarší	starý	k2eAgFnPc4d3
zásady	zásada	k1gFnPc4
krevní	krevní	k2eAgFnSc2d1
msty	msta	k1gFnSc2
patří	patřit	k5eAaImIp3nS
„	„	k?
<g/>
krev	krev	k1gFnSc4
za	za	k7c4
krev	krev	k1gFnSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
takže	takže	k8xS
vražda	vražda	k1gFnSc1
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
pomstěna	pomstěn	k2eAgFnSc1d1
vraždou	vražda	k1gFnSc7
a	a	k8xC
„	„	k?
<g/>
všichni	všechen	k3xTgMnPc1
za	za	k7c4
jednoho	jeden	k4xCgMnSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
tedy	tedy	k8xC
vražda	vražda	k1gFnSc1
je	být	k5eAaImIp3nS
kolektivní	kolektivní	k2eAgFnSc7d1
vinou	vina	k1gFnSc7
celého	celý	k2eAgInSc2d1
rodu	rod	k1gInSc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgInSc2
důvodu	důvod	k1gInSc2
nabývala	nabývat	k5eAaImAgFnS
krevní	krevní	k2eAgFnSc1d1
msta	msta	k1gFnSc1
rázu	ráz	k1gInSc2
války	válka	k1gFnSc2
mezi	mezi	k7c7
dvěma	dva	k4xCgInPc7
rody	rod	k1gInPc7
a	a	k8xC
vražda	vražda	k1gFnSc1
na	na	k7c6
jedné	jeden	k4xCgFnSc6
straně	strana	k1gFnSc6
je	být	k5eAaImIp3nS
vykoupena	vykoupen	k2eAgFnSc1d1
více	hodně	k6eAd2
vraždami	vražda	k1gFnPc7
na	na	k7c6
druhé	druhý	k4xOgFnSc6
straně	strana	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krevní	krevní	k2eAgFnSc1d1
msta	msta	k1gFnSc1
je	být	k5eAaImIp3nS
později	pozdě	k6eAd2
zjemňována	zjemňován	k2eAgFnSc1d1
zavedením	zavedení	k1gNnSc7
fikce	fikce	k1gFnSc2
msty	msta	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
vrahovi	vrah	k1gMnSc3
rituálně	rituálně	k6eAd1
odpuštěno	odpustit	k5eAaPmNgNnS
<g/>
,	,	kIx,
a	a	k8xC
zavedením	zavedení	k1gNnSc7
pokuty	pokuta	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc7,k3yQgFnSc7,k3yRgFnSc7
se	se	k3xPyFc4
vrah	vrah	k1gMnSc1
může	moct	k5eAaImIp3nS
vykoupit	vykoupit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Evropě	Evropa	k1gFnSc6
byl	být	k5eAaImAgInS
tento	tento	k3xDgInSc1
společenský	společenský	k2eAgInSc1d1
institut	institut	k1gInSc1
vytlačen	vytlačen	k2eAgInSc1d1
v	v	k7c6
raném	raný	k2eAgNnSc6d1
křesťanském	křesťanský	k2eAgNnSc6d1
období	období	k1gNnSc6
<g/>
,	,	kIx,
na	na	k7c6
mnoha	mnoho	k4c6
místech	místo	k1gNnPc6
se	se	k3xPyFc4
ale	ale	k9
dochoval	dochovat	k5eAaPmAgInS
až	až	k6eAd1
do	do	k7c2
počátku	počátek	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
(	(	kIx(
<g/>
Albánie	Albánie	k1gFnSc1
<g/>
,	,	kIx,
Korsika	Korsika	k1gFnSc1
<g/>
,	,	kIx,
Srbsko	Srbsko	k1gNnSc1
<g/>
,	,	kIx,
jižní	jižní	k2eAgFnSc1d1
Itálie	Itálie	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
mafiánských	mafiánský	k2eAgInPc6d1
kruzích	kruh	k1gInPc6
až	až	k9
dodnes	dodnes	k6eAd1
(	(	kIx(
<g/>
vendeta	vendeta	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
mimoevropských	mimoevropský	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
byl	být	k5eAaImAgInS
dlouho	dlouho	k6eAd1
znám	znám	k2eAgInSc1d1
například	například	k6eAd1
v	v	k7c6
Japonsku	Japonsko	k1gNnSc6
i	i	k9
jinde	jinde	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dlouhodobé	dlouhodobý	k2eAgInPc1d1
násilné	násilný	k2eAgInPc1d1
konflikty	konflikt	k1gInPc1
se	se	k3xPyFc4
ke	k	k7c3
konci	konec	k1gInSc3
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
vyskytovaly	vyskytovat	k5eAaImAgFnP
i	i	k9
v	v	k7c4
americké	americký	k2eAgFnPc4d1
Kentucky	Kentucka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Opevněný	opevněný	k2eAgInSc1d1
dům	dům	k1gInSc1
(	(	kIx(
<g/>
kullë	kullë	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Theth	Theth	k1gInSc1
<g/>
,	,	kIx,
severní	severní	k2eAgFnSc1d1
Albánie	Albánie	k1gFnSc1
</s>
<s>
Krevní	krevní	k2eAgFnSc1d1
msta	msta	k1gFnSc1
v	v	k7c6
Itálii	Itálie	k1gFnSc6
</s>
<s>
Krevní	krevní	k2eAgFnSc1d1
msta	msta	k1gFnSc1
se	se	k3xPyFc4
v	v	k7c6
Itálii	Itálie	k1gFnSc6
nazývá	nazývat	k5eAaImIp3nS
vendeta	vendeta	k1gFnSc1
(	(	kIx(
<g/>
italsky	italsky	k6eAd1
vendetta	vendetta	k1gFnSc1
<g/>
,	,	kIx,
z	z	k7c2
latinského	latinský	k2eAgInSc2d1
vindicta	vindict	k1gInSc2
<g/>
,	,	kIx,
msta	msta	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
typická	typický	k2eAgFnSc1d1
pro	pro	k7c4
jižní	jižní	k2eAgFnSc4d1
Itálii	Itálie	k1gFnSc4
<g/>
,	,	kIx,
Sicílii	Sicílie	k1gFnSc4
a	a	k8xC
francouzskou	francouzský	k2eAgFnSc4d1
Korsiku	Korsika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Známá	známá	k1gFnSc1
je	být	k5eAaImIp3nS
především	především	k9
z	z	k7c2
mafiánského	mafiánský	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
a	a	k8xC
jeho	jeho	k3xOp3gNnPc2
filmových	filmový	k2eAgNnPc2d1
ztvárnění	ztvárnění	k1gNnPc2
<g/>
,	,	kIx,
proto	proto	k8xC
v	v	k7c6
moderní	moderní	k2eAgFnSc6d1
době	doba	k1gFnSc6
bývá	bývat	k5eAaImIp3nS
výraz	výraz	k1gInSc1
používán	používat	k5eAaImNgInS
i	i	k9
pro	pro	k7c4
pomstu	pomsta	k1gFnSc4
všeobecně	všeobecně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Krevní	krevní	k2eAgFnSc1d1
msta	msta	k1gFnSc1
v	v	k7c6
Albánii	Albánie	k1gFnSc6
</s>
<s>
Zde	zde	k6eAd1
se	se	k3xPyFc4
krevní	krevní	k2eAgFnSc1d1
msta	msta	k1gFnSc1
nazývá	nazývat	k5eAaImIp3nS
gjakmarrja	gjakmarrj	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
některých	některý	k3yIgFnPc6
oblastech	oblast	k1gFnPc6
Albánie	Albánie	k1gFnSc1
a	a	k8xC
Kosova	Kosův	k2eAgFnSc1d1
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Odhaduje	odhadovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
se	se	k3xPyFc4
gjakmarrja	gjakmarrja	k1gFnSc1
týkala	týkat	k5eAaImAgFnS
přibližně	přibližně	k6eAd1
3000	#num#	k4
albánských	albánský	k2eAgFnPc2d1
rodin	rodina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
Albánii	Albánie	k1gFnSc6
se	se	k3xPyFc4
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
v	v	k7c6
jiných	jiný	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
praktikujících	praktikující	k2eAgFnPc6d1
krevní	krevní	k2eAgFnSc7d1
mstu	msta	k1gFnSc4
<g/>
,	,	kIx,
stavěly	stavět	k5eAaImAgInP
opevněné	opevněný	k2eAgInPc1d1
věžovité	věžovitý	k2eAgInPc1d1
domy	dům	k1gInPc1
<g/>
,	,	kIx,
do	do	k7c2
kterých	který	k3yQgInPc2,k3yRgInPc2,k3yIgInPc2
se	se	k3xPyFc4
uchylovali	uchylovat	k5eAaImAgMnP
muži	muž	k1gMnPc1
ohrožení	ohrožení	k1gNnSc2
krevní	krevní	k2eAgFnSc7d1
mstou	msta	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Opovědnictví	opovědnictví	k1gNnSc1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
</s>
<s>
Až	až	k6eAd1
do	do	k7c2
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
existovalo	existovat	k5eAaImAgNnS
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
tzv.	tzv.	kA
opovědnictví	opovědnictví	k1gNnPc4
<g/>
,	,	kIx,
čili	čili	k8xC
soukromé	soukromý	k2eAgNnSc1d1
vypovězení	vypovězení	k1gNnSc1
nepřátelství	nepřátelství	k1gNnSc1
(	(	kIx(
<g/>
původně	původně	k6eAd1
vycházející	vycházející	k2eAgInPc4d1
z	z	k7c2
principů	princip	k1gInPc2
krevní	krevní	k2eAgFnSc2d1
msty	msta	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejznámějším	známý	k2eAgMnSc7d3
opovědníkem	opovědník	k1gMnSc7
byl	být	k5eAaImAgMnS
šlechtic	šlechtic	k1gMnSc1
Jiří	Jiří	k1gMnSc1
Kopidlanský	Kopidlanský	k2eAgMnSc1d1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
vedl	vést	k5eAaImAgMnS
od	od	k7c2
roku	rok	k1gInSc2
1507	#num#	k4
několik	několik	k4yIc4
let	léto	k1gNnPc2
soukromou	soukromý	k2eAgFnSc4d1
válku	válka	k1gFnSc4
proti	proti	k7c3
Praze	Praha	k1gFnSc3
a	a	k8xC
dalším	další	k2eAgNnPc3d1
městům	město	k1gNnPc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
pomstil	pomstit	k5eAaImAgMnS,k5eAaPmAgMnS
smrt	smrt	k1gFnSc4
svého	svůj	k3xOyFgMnSc2
bratra	bratr	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://phys.org/news/2016-10-ancient-burials-blood-feuds.html	http://phys.org/news/2016-10-ancient-burials-blood-feuds.html	k1gMnSc1
-	-	kIx~
Ancient	Ancient	k1gMnSc1
burials	burials	k6eAd1
suggestive	suggestiv	k1gInSc5
of	of	k?
blood	blood	k1gInSc1
feuds	feuds	k1gInSc1
<g/>
↑	↑	k?
Kolotoč	kolotoč	k1gInSc1
s	s	k7c7
příchutí	příchuť	k1gFnSc7
krve	krev	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidé	člověk	k1gMnPc1
a	a	k8xC
Země	zem	k1gFnPc1
<g/>
.	.	kIx.
27	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2010	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Blood	Blood	k1gInSc1
feuds	feuds	k1gInSc1
in	in	k?
Albania	Albanium	k1gNnSc2
<g/>
’	’	k?
<g/>
s	s	k7c7
Accursed	Accursed	k1gMnSc1
Mountains	Mountains	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Zemští	zemský	k2eAgMnPc1d1
škůdci	škůdce	k1gMnPc1
na	na	k7c6
počátku	počátek	k1gInSc6
raného	raný	k2eAgInSc2d1
novověku	novověk	k1gInSc2
<g/>
..	..	k?
Jihočeská	jihočeský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Českých	český	k2eAgInPc6d1
Budějovicích	Budějovice	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc4d1
heslo	heslo	k1gNnSc4
Msta	msta	k1gFnSc1
krevní	krevní	k2eAgFnSc1d1
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4146085-6	4146085-6	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85142614	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85142614	#num#	k4
</s>
