<s>
Medvědí	medvědí	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
</s>
<s>
Medvědí	medvědí	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
je	být	k5eAaImIp3nS
skupina	skupina	k1gFnSc1
šesti	šest	k4xCc2
ostrovů	ostrov	k1gInPc2
v	v	k7c6
Jakutsku	Jakutsk	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
v	v	k7c6
Severním	severní	k2eAgInSc6d1
ledovém	ledový	k2eAgInSc6d1
oceánu	oceán	k1gInSc6
<g/>
,	,	kIx,
ve	v	k7c6
Východosibiřském	východosibiřský	k2eAgNnSc6d1
moři	moře	k1gNnSc6
severně	severně	k6eAd1
od	od	k7c2
ústí	ústí	k1gNnSc2
řeky	řeka	k1gFnSc2
Kolymy	Kolyma	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zabírají	zabírat	k5eAaImIp3nP
plochu	plocha	k1gFnSc4
60	#num#	k4
km²	km²	k?
<g/>
,	,	kIx,
největším	veliký	k2eAgInSc7d3
ostrovem	ostrov	k1gInSc7
je	být	k5eAaImIp3nS
Krestovskij	Krestovskij	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšší	vysoký	k2eAgInSc1d3
místo	místo	k7c2
ostrovů	ostrov	k1gInPc2
je	být	k5eAaImIp3nS
273	#num#	k4
metrů	metr	k1gInPc2
nad	nad	k7c7
mořem	moře	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Souostroví	souostroví	k1gNnSc1
je	být	k5eAaImIp3nS
tvořeno	tvořit	k5eAaImNgNnS
šesti	šest	k4xCc7
ostrovy	ostrov	k1gInPc7
<g/>
:	:	kIx,
Krestovskij	Krestovskij	k1gFnSc1
<g/>
,	,	kIx,
Leonťjeva	Leonťjeva	k1gFnSc1
<g/>
,	,	kIx,
Četyrjochstolbovoj	Četyrjochstolbovoj	k1gInSc1
<g/>
,	,	kIx,
Puškarjova	Puškarjův	k2eAgMnSc4d1
<g/>
,	,	kIx,
Lysova	Lysův	k2eAgMnSc4d1
a	a	k8xC
Andrejeva	Andrejev	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
nejrozlehlejším	rozlehlý	k2eAgInSc6d3
Krestovském	Krestovský	k2eAgInSc6d1
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
dva	dva	k4xCgInPc1
kopce	kopec	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Severní	severní	k2eAgInPc1d1
a	a	k8xC
východní	východní	k2eAgInPc4d1
břehy	břeh	k1gInPc4
má	mít	k5eAaImIp3nS
strmé	strmý	k2eAgNnSc1d1
<g/>
,	,	kIx,
západní	západní	k2eAgInSc4d1
břeh	břeh	k1gInSc4
je	být	k5eAaImIp3nS
pozvolný	pozvolný	k2eAgInSc1d1
<g/>
,	,	kIx,
pokryt	pokrýt	k5eAaPmNgInS
kameny	kámen	k1gInPc7
a	a	k8xC
štěrkem	štěrk	k1gInSc7
<g/>
,	,	kIx,
na	na	k7c6
jižním	jižní	k2eAgInSc6d1
břehu	břeh	k1gInSc6
teče	téct	k5eAaImIp3nS
jediný	jediný	k2eAgInSc1d1
potok	potok	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rostou	růst	k5eAaImIp3nP
zde	zde	k6eAd1
mechy	mech	k1gInPc1
a	a	k8xC
lišejníky	lišejník	k1gInPc1
a	a	k8xC
tuhá	tuhý	k2eAgFnSc1d1
nízká	nízký	k2eAgFnSc1d1
tráva	tráva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
М	М	k?
о	о	k?
na	na	k7c6
ruské	ruský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Malá	malý	k2eAgFnSc1d1
Československá	československý	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
ČSAV	ČSAV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
IV	IV	kA
<g/>
.	.	kIx.
svazek	svazek	k1gInSc1
M-	M-	k1gMnSc2
<g/>
Pol.	Pol.	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
ACADEMIA	academia	k1gFnSc1
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heslo	heslo	k1gNnSc1
Medvědí	medvědí	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
<g/>
,	,	kIx,
s.	s.	k?
159	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Medvědí	medvědí	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
|	|	kIx~
Rusko	Rusko	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
7535115-8	7535115-8	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
234364387	#num#	k4
</s>
