<p>
<s>
Laos	Laos	k1gInSc1	Laos
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
Laoská	laoský	k2eAgFnSc1d1	laoská
lidově	lidově	k6eAd1	lidově
demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vnitrozemský	vnitrozemský	k2eAgInSc1d1	vnitrozemský
stát	stát	k1gInSc1	stát
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
Zadní	zadní	k2eAgFnSc2d1	zadní
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
sousedy	soused	k1gMnPc7	soused
jsou	být	k5eAaImIp3nP	být
Vietnam	Vietnam	k1gInSc1	Vietnam
<g/>
,	,	kIx,	,
Kambodža	Kambodža	k1gFnSc1	Kambodža
<g/>
,	,	kIx,	,
Thajsko	Thajsko	k1gNnSc1	Thajsko
<g/>
,	,	kIx,	,
Myanmar	Myanmar	k1gInSc1	Myanmar
a	a	k8xC	a
Čína	Čína	k1gFnSc1	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
Laosu	Laos	k1gInSc2	Laos
náleží	náležet	k5eAaImIp3nS	náležet
k	k	k7c3	k
etnické	etnický	k2eAgFnSc3d1	etnická
skupině	skupina	k1gFnSc3	skupina
Lao	Lao	k1gFnSc2	Lao
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
ní	on	k3xPp3gFnSc2	on
zde	zde	k6eAd1	zde
ale	ale	k8xC	ale
žije	žít	k5eAaImIp3nS	žít
snad	snad	k9	snad
až	až	k9	až
120	[number]	k4	120
národnostních	národnostní	k2eAgFnPc2d1	národnostní
menšin	menšina	k1gFnPc2	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Laos	Laos	k1gInSc1	Laos
je	být	k5eAaImIp3nS	být
socialistický	socialistický	k2eAgInSc1d1	socialistický
stát	stát	k1gInSc1	stát
s	s	k7c7	s
vedoucí	vedoucí	k2eAgFnSc7d1	vedoucí
úlohou	úloha	k1gFnSc7	úloha
jedné	jeden	k4xCgFnSc2	jeden
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Etymologie	etymologie	k1gFnSc2	etymologie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Laoštině	Laoština	k1gFnSc6	Laoština
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaImF	stát
nazýván	nazývat	k5eAaImNgInS	nazývat
Muang	Muang	k1gInSc1	Muang
Lao	Lao	k1gFnSc2	Lao
(	(	kIx(	(
<g/>
ເ	ເ	k?	ເ
<g/>
ື	ື	k?	ື
<g/>
ອ	ອ	k?	ອ
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Pathet	Pathet	k1gMnSc1	Pathet
Lao	Lao	k1gMnSc1	Lao
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
v	v	k7c6	v
doslovném	doslovný	k2eAgNnSc6d1	doslovné
překladu	překlad	k1gInSc6	překlad
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
Laoská	laoský	k2eAgFnSc1d1	laoská
země	země	k1gFnSc1	země
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Francouzi	Francouz	k1gMnPc1	Francouz
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
spojili	spojit	k5eAaPmAgMnP	spojit
tři	tři	k4xCgNnPc4	tři
separovaná	separovaný	k2eAgNnPc4d1	separované
Laoská	laoský	k2eAgNnPc4d1	laoské
království	království	k1gNnPc4	království
do	do	k7c2	do
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
Indočíny	Indočína	k1gFnSc2	Indočína
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1893	[number]	k4	1893
<g/>
,	,	kIx,	,
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
zemi	zem	k1gFnSc4	zem
podle	podle	k7c2	podle
místních	místní	k2eAgMnPc2d1	místní
obyvatel	obyvatel	k1gMnPc2	obyvatel
"	"	kIx"	"
<g/>
Laos	Laos	k1gInSc1	Laos
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
se	se	k3xPyFc4	se
koncové	koncový	k2eAgNnSc1d1	koncové
"	"	kIx"	"
<g/>
s	s	k7c7	s
<g/>
"	"	kIx"	"
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
velmi	velmi	k6eAd1	velmi
tiše	tiš	k1gFnPc4	tiš
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vůbec	vůbec	k9	vůbec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
757	[number]	k4	757
zde	zde	k6eAd1	zde
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
první	první	k4xOgInSc1	první
laoský	laoský	k2eAgInSc1d1	laoský
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
do	do	k7c2	do
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
Laos	Laos	k1gInSc4	Laos
silným	silný	k2eAgInSc7d1	silný
státem	stát	k1gInSc7	stát
známým	známý	k2eAgInSc7d1	známý
jako	jako	k8xC	jako
Lan	lano	k1gNnPc2	lano
Xang	Xanga	k1gFnPc2	Xanga
<g/>
,	,	kIx,	,
království	království	k1gNnSc1	království
miliónu	milión	k4xCgInSc2	milión
slonů	slon	k1gMnPc2	slon
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
rozpadlo	rozpadnout	k5eAaPmAgNnS	rozpadnout
na	na	k7c4	na
několik	několik	k4yIc4	několik
drobných	drobný	k2eAgNnPc2d1	drobné
království	království	k1gNnPc2	království
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1893	[number]	k4	1893
se	se	k3xPyFc4	se
území	území	k1gNnSc2	území
dostalo	dostat	k5eAaPmAgNnS	dostat
pod	pod	k7c4	pod
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
nadvládu	nadvláda	k1gFnSc4	nadvláda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
spojením	spojení	k1gNnSc7	spojení
malých	malý	k2eAgNnPc2d1	malé
království	království	k1gNnPc2	království
se	s	k7c7	s
sousedními	sousední	k2eAgFnPc7d1	sousední
zeměmi	zem	k1gFnPc7	zem
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
Francouzskou	francouzský	k2eAgFnSc4d1	francouzská
Indočínu	Indočína	k1gFnSc4	Indočína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
poloviny	polovina	k1gFnSc2	polovina
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
francouzská	francouzský	k2eAgFnSc1d1	francouzská
vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
selhala	selhat	k5eAaPmAgFnS	selhat
a	a	k8xC	a
do	do	k7c2	do
severního	severní	k2eAgInSc2d1	severní
Laosu	Laos	k1gInSc2	Laos
vtrhli	vtrhnout	k5eAaPmAgMnP	vtrhnout
komunističtí	komunistický	k2eAgMnPc1d1	komunistický
rebelové	rebel	k1gMnPc1	rebel
z	z	k7c2	z
hnutí	hnutí	k1gNnSc2	hnutí
Pathet	Pathet	k1gMnSc1	Pathet
Lao	Lao	k1gMnSc1	Lao
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
ustanovili	ustanovit	k5eAaPmAgMnP	ustanovit
dočasnou	dočasný	k2eAgFnSc4d1	dočasná
socialistickou	socialistický	k2eAgFnSc4d1	socialistická
republiku	republika	k1gFnSc4	republika
Laos	Laos	k1gInSc1	Laos
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
a	a	k8xC	a
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
během	během	k7c2	během
války	válka	k1gFnSc2	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
se	se	k3xPyFc4	se
boje	boj	k1gInPc1	boj
rozšířily	rozšířit	k5eAaPmAgInP	rozšířit
také	také	k9	také
do	do	k7c2	do
Laosu	Laos	k1gInSc2	Laos
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
americkém	americký	k2eAgNnSc6d1	americké
bombardování	bombardování	k1gNnSc6	bombardování
Laosu	Laos	k1gInSc2	Laos
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
"	"	kIx"	"
<g/>
tajné	tajný	k2eAgFnSc2d1	tajná
války	válka	k1gFnSc2	válka
<g/>
"	"	kIx"	"
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1964	[number]	k4	1964
a	a	k8xC	a
1973	[number]	k4	1973
bylo	být	k5eAaImAgNnS	být
svrženo	svrhnout	k5eAaPmNgNnS	svrhnout
přes	přes	k7c4	přes
dva	dva	k4xCgInPc4	dva
miliony	milion	k4xCgInPc4	milion
tun	tuna	k1gFnPc2	tuna
bomb	bomba	k1gFnPc2	bomba
a	a	k8xC	a
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
350	[number]	k4	350
000	[number]	k4	000
civilistů	civilista	k1gMnPc2	civilista
<g/>
.	.	kIx.	.
</s>
<s>
Problémem	problém	k1gInSc7	problém
do	do	k7c2	do
dnešního	dnešní	k2eAgInSc2d1	dnešní
dne	den	k1gInSc2	den
je	být	k5eAaImIp3nS	být
i	i	k9	i
nevybuchlá	vybuchlý	k2eNgFnSc1d1	nevybuchlá
munice	munice	k1gFnSc1	munice
z	z	k7c2	z
kobercových	kobercový	k2eAgInPc2d1	kobercový
náletů	nálet	k1gInPc2	nálet
–	–	k?	–
podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
tato	tento	k3xDgFnSc1	tento
munice	munice	k1gFnSc1	munice
zabila	zabít	k5eAaPmAgFnS	zabít
20	[number]	k4	20
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
lidí	člověk	k1gMnPc2	člověk
ročně	ročně	k6eAd1	ročně
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
obětí	oběť	k1gFnSc7	oběť
<g/>
,	,	kIx,	,
60	[number]	k4	60
%	%	kIx~	%
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
,	,	kIx,	,
40	[number]	k4	40
%	%	kIx~	%
jsou	být	k5eAaImIp3nP	být
děti	dítě	k1gFnPc1	dítě
<g/>
.	.	kIx.	.
<g/>
Když	když	k8xS	když
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
skončila	skončit	k5eAaPmAgFnS	skončit
vietnamská	vietnamský	k2eAgFnSc1d1	vietnamská
válka	válka	k1gFnSc1	válka
i	i	k8xC	i
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Laosu	Laos	k1gInSc6	Laos
<g/>
,	,	kIx,	,
Pathet	Pathet	k1gInSc4	Pathet
Lao	Lao	k1gMnPc2	Lao
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
v	v	k7c6	v
Laosu	Laos	k1gInSc6	Laos
novou	nový	k2eAgFnSc4d1	nová
komunistickou	komunistický	k2eAgFnSc4d1	komunistická
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
vláda	vláda	k1gFnSc1	vláda
země	země	k1gFnSc1	země
silně	silně	k6eAd1	silně
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
politikou	politika	k1gFnSc7	politika
vietnamských	vietnamský	k2eAgMnPc2d1	vietnamský
komunistů	komunista	k1gMnPc2	komunista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
náhorní	náhorní	k2eAgFnSc1d1	náhorní
plošina	plošina	k1gFnSc1	plošina
Xiangkhoang	Xiangkhoanga	k1gFnPc2	Xiangkhoanga
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
2800	[number]	k4	2800
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
s	s	k7c7	s
hlubokými	hluboký	k2eAgFnPc7d1	hluboká
pánvemi	pánev	k1gFnPc7	pánev
(	(	kIx(	(
<g/>
planina	planina	k1gFnSc1	planina
Džbánů	džbán	k1gInPc2	džbán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
s	s	k7c7	s
Vietnamem	Vietnam	k1gInSc7	Vietnam
tvoří	tvořit	k5eAaImIp3nS	tvořit
Annamské	Annamský	k2eAgNnSc4d1	Annamský
pohoří	pohoří	k1gNnSc4	pohoří
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnSc4d1	západní
<g/>
,	,	kIx,	,
jihozápadní	jihozápadní	k2eAgFnSc4d1	jihozápadní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc4d1	jižní
část	část	k1gFnSc4	část
vyplňují	vyplňovat	k5eAaImIp3nP	vyplňovat
úrodné	úrodný	k2eAgFnPc1d1	úrodná
nížiny	nížina	k1gFnPc1	nížina
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
také	také	k9	také
plošina	plošina	k1gFnSc1	plošina
Bolovens	Bolovensa	k1gFnPc2	Bolovensa
podél	podél	k7c2	podél
největší	veliký	k2eAgFnSc2d3	veliký
řeky	řeka	k1gFnSc2	řeka
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
–	–	k?	–
Mekongu	Mekong	k1gInSc2	Mekong
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
je	být	k5eAaImIp3nS	být
tropické	tropický	k2eAgInPc4d1	tropický
monzunové	monzunový	k2eAgInPc4d1	monzunový
s	s	k7c7	s
vydatnými	vydatný	k2eAgFnPc7d1	vydatná
dešťovými	dešťový	k2eAgFnPc7d1	dešťová
srážkami	srážka	k1gFnPc7	srážka
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
letních	letní	k2eAgInPc6d1	letní
měsících	měsíc	k1gInPc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgFnPc1d1	průměrná
lednové	lednový	k2eAgFnPc1d1	lednová
teploty	teplota	k1gFnPc1	teplota
ve	v	k7c6	v
Vientiane	Vientian	k1gMnSc5	Vientian
jsou	být	k5eAaImIp3nP	být
21	[number]	k4	21
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
červencové	červencový	k2eAgFnSc2d1	červencová
27	[number]	k4	27
°	°	k?	°
<g/>
C.	C.	kA	C.
Průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc1d1	roční
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
činí	činit	k5eAaImIp3nS	činit
tamtéž	tamtéž	k6eAd1	tamtéž
1700	[number]	k4	1700
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politika	politikum	k1gNnSc2	politikum
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Konflikt	konflikt	k1gInSc1	konflikt
s	s	k7c7	s
Hmongy	Hmong	k1gInPc7	Hmong
===	===	k?	===
</s>
</p>
<p>
<s>
Vláda	vláda	k1gFnSc1	vláda
Laosu	Laos	k1gInSc2	Laos
byla	být	k5eAaImAgFnS	být
obviněna	obvinit	k5eAaPmNgFnS	obvinit
z	z	k7c2	z
páchání	páchání	k1gNnSc2	páchání
genocidy	genocida	k1gFnSc2	genocida
a	a	k8xC	a
porušování	porušování	k1gNnSc2	porušování
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
náboženské	náboženský	k2eAgFnSc2d1	náboženská
svobody	svoboda	k1gFnSc2	svoboda
proti	proti	k7c3	proti
etnické	etnický	k2eAgFnSc3d1	etnická
menšině	menšina	k1gFnSc3	menšina
Hmongů	Hmong	k1gInPc2	Hmong
uvnitř	uvnitř	k7c2	uvnitř
vlastních	vlastní	k2eAgFnPc2d1	vlastní
hranic	hranice	k1gFnPc2	hranice
Laosu	Laos	k1gInSc2	Laos
<g/>
.	.	kIx.	.
<g/>
Některé	některý	k3yIgFnSc2	některý
skupiny	skupina	k1gFnSc2	skupina
Hmongů	Hmong	k1gInPc2	Hmong
bojovaly	bojovat	k5eAaImAgFnP	bojovat
jako	jako	k9	jako
jednotky	jednotka	k1gFnPc1	jednotka
podporované	podporovaný	k2eAgFnSc2d1	podporovaná
CIA	CIA	kA	CIA
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
monarchistů	monarchista	k1gMnPc2	monarchista
během	během	k7c2	během
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Laosu	Laos	k1gInSc6	Laos
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
hnutí	hnutí	k1gNnSc1	hnutí
Pathet	Patheta	k1gFnPc2	Patheta
Lao	Lao	k1gMnPc2	Lao
převzalo	převzít	k5eAaPmAgNnS	převzít
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
moc	moc	k1gFnSc1	moc
<g/>
,	,	kIx,	,
konflikt	konflikt	k1gInSc1	konflikt
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
izolovaných	izolovaný	k2eAgNnPc6d1	izolované
ohniskách	ohnisko	k1gNnPc6	ohnisko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
komunistický	komunistický	k2eAgInSc4d1	komunistický
tisk	tisk	k1gInSc4	tisk
slíbil	slíbit	k5eAaPmAgMnS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
strana	strana	k1gFnSc1	strana
dopadne	dopadnout	k5eAaPmIp3nS	dopadnout
"	"	kIx"	"
<g/>
americké	americký	k2eAgMnPc4d1	americký
kolaboranty	kolaborant	k1gMnPc4	kolaborant
<g/>
"	"	kIx"	"
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc4	jejich
rodiny	rodina	k1gFnPc4	rodina
"	"	kIx"	"
<g/>
do	do	k7c2	do
posledního	poslední	k2eAgInSc2d1	poslední
kořene	kořen	k1gInSc2	kořen
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Okolo	okolo	k7c2	okolo
200	[number]	k4	200
000	[number]	k4	000
Hmongů	Hmong	k1gInPc2	Hmong
odešlo	odejít	k5eAaPmAgNnS	odejít
do	do	k7c2	do
exilu	exil	k1gInSc2	exil
v	v	k7c6	v
Thajsku	Thajsko	k1gNnSc6	Thajsko
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
skončilo	skončit	k5eAaPmAgNnS	skončit
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
hmongských	hmongský	k2eAgMnPc2d1	hmongský
bojovníků	bojovník	k1gMnPc2	bojovník
se	se	k3xPyFc4	se
skrývala	skrývat	k5eAaImAgFnS	skrývat
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Xienghoang	Xienghoang	k1gMnSc1	Xienghoang
po	po	k7c6	po
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
zbytky	zbytek	k1gInPc1	zbytek
se	se	k3xPyFc4	se
vynořily	vynořit	k5eAaPmAgInP	vynořit
z	z	k7c2	z
džungle	džungle	k1gFnSc2	džungle
ještě	ještě	k9	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Administrativní	administrativní	k2eAgNnSc4d1	administrativní
členění	členění	k1gNnSc4	členění
===	===	k?	===
</s>
</p>
<p>
<s>
Laos	Laos	k1gInSc1	Laos
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
16	[number]	k4	16
provincií	provincie	k1gFnPc2	provincie
(	(	kIx(	(
<g/>
qwang	qwang	k1gInSc1	qwang
<g/>
)	)	kIx)	)
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
prefekturu	prefektura	k1gFnSc4	prefektura
(	(	kIx(	(
<g/>
Nakhonluang	Nakhonluang	k1gMnSc1	Nakhonluang
ViengChan	ViengChan	k1gMnSc1	ViengChan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
území	území	k1gNnSc4	území
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
Na	na	k7c6	na
Kone	kon	k1gInSc5	kon
Luang	Luang	k1gInSc1	Luang
Vientiane	Vientian	k1gMnSc5	Vientian
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Provincie	provincie	k1gFnSc1	provincie
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
distrikty	distrikt	k1gInPc4	distrikt
(	(	kIx(	(
<g/>
muang	muang	k1gInSc4	muang
<g/>
)	)	kIx)	)
a	a	k8xC	a
vesnice	vesnice	k1gFnSc1	vesnice
(	(	kIx(	(
<g/>
baan	baan	k1gInSc1	baan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
<s>
Laos	Laos	k1gInSc1	Laos
je	být	k5eAaImIp3nS	být
málo	málo	k6eAd1	málo
rozvinutý	rozvinutý	k2eAgInSc1d1	rozvinutý
zemědělský	zemědělský	k2eAgInSc1d1	zemědělský
stát	stát	k1gInSc1	stát
s	s	k7c7	s
důležitým	důležitý	k2eAgNnSc7d1	důležité
lesním	lesní	k2eAgNnSc7d1	lesní
hospodářstvím	hospodářství	k1gNnSc7	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
Význačná	význačný	k2eAgFnSc1d1	význačná
je	být	k5eAaImIp3nS	být
těžba	těžba	k1gFnSc1	těžba
vzácného	vzácný	k2eAgNnSc2d1	vzácné
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Přední	přední	k2eAgFnSc7d1	přední
nerostnou	nerostný	k2eAgFnSc7d1	nerostná
surovinou	surovina	k1gFnSc7	surovina
je	být	k5eAaImIp3nS	být
cín	cín	k1gInSc1	cín
<g/>
.	.	kIx.	.
</s>
<s>
Rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
je	být	k5eAaImIp3nS	být
řemeslná	řemeslný	k2eAgFnSc1d1	řemeslná
výroba	výroba	k1gFnSc1	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Elektrická	elektrický	k2eAgFnSc1d1	elektrická
energie	energie	k1gFnSc1	energie
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
ve	v	k7c6	v
vodních	vodní	k2eAgFnPc6d1	vodní
elektrárnách	elektrárna	k1gFnPc6	elektrárna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
rýže	rýže	k1gFnSc1	rýže
<g/>
,	,	kIx,	,
kukuřice	kukuřice	k1gFnSc1	kukuřice
<g/>
,	,	kIx,	,
batáty	batáty	k1gInPc1	batáty
<g/>
,	,	kIx,	,
maniok	maniok	k1gInSc1	maniok
<g/>
,	,	kIx,	,
brambory	brambor	k1gInPc1	brambor
<g/>
,	,	kIx,	,
citrusy	citrus	k1gInPc1	citrus
<g/>
,	,	kIx,	,
kávovník	kávovník	k1gInSc1	kávovník
a	a	k8xC	a
kaučukovník	kaučukovník	k1gInSc1	kaučukovník
<g/>
.	.	kIx.	.
</s>
<s>
Živočišná	živočišný	k2eAgFnSc1d1	živočišná
produkce	produkce	k1gFnSc1	produkce
<g/>
:	:	kIx,	:
skot	skot	k1gInSc1	skot
<g/>
,	,	kIx,	,
buvoli	buvol	k1gMnPc1	buvol
<g/>
,	,	kIx,	,
prasata	prase	k1gNnPc4	prase
<g/>
,	,	kIx,	,
drůbež	drůbež	k1gFnSc4	drůbež
a	a	k8xC	a
rybolov	rybolov	k1gInSc4	rybolov
<g/>
.	.	kIx.	.
</s>
<s>
Důležitá	důležitý	k2eAgFnSc1d1	důležitá
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
také	také	k9	také
říční	říční	k2eAgFnSc1d1	říční
doprava	doprava	k1gFnSc1	doprava
na	na	k7c6	na
Mekongu	Mekong	k1gInSc6	Mekong
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
třikrát	třikrát	k6eAd1	třikrát
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
Česko	Česko	k1gNnSc1	Česko
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
jen	jen	k9	jen
několik	několik	k4yIc1	několik
málo	málo	k4c1	málo
kilometrů	kilometr	k1gInPc2	kilometr
železnice	železnice	k1gFnSc2	železnice
z	z	k7c2	z
thajského	thajský	k2eAgNnSc2d1	Thajské
pohraničního	pohraniční	k2eAgNnSc2d1	pohraniční
města	město	k1gNnSc2	město
Nong	Nong	k1gInSc1	Nong
Khai	Khae	k1gFnSc4	Khae
do	do	k7c2	do
stanice	stanice	k1gFnSc2	stanice
Thanalang	Thanalanga	k1gFnPc2	Thanalanga
na	na	k7c6	na
předměstí	předměstí	k1gNnSc6	předměstí
Vientiane	Vientian	k1gInSc5	Vientian
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Laosu	Laos	k1gInSc2	Laos
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
těm	ten	k3xDgNnPc3	ten
slabším	slabý	k2eAgNnPc3d2	slabší
v	v	k7c6	v
regionu	region	k1gInSc6	region
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
tržním	tržní	k2eAgFnPc3d1	tržní
reformám	reforma	k1gFnPc3	reforma
Laoské	laoský	k2eAgFnSc2d1	laoská
lidové	lidový	k2eAgFnSc2d1	lidová
revoluční	revoluční	k2eAgFnSc2d1	revoluční
strany	strana	k1gFnSc2	strana
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
slibně	slibně	k6eAd1	slibně
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
laoská	laoský	k2eAgFnSc1d1	laoská
burza	burza	k1gFnSc1	burza
<g/>
.	.	kIx.	.
</s>
<s>
HDP	HDP	kA	HDP
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
se	se	k3xPyFc4	se
pohyboval	pohybovat	k5eAaImAgInS	pohybovat
kolem	kolem	k7c2	kolem
1400	[number]	k4	1400
USD	USD	kA	USD
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
a	a	k8xC	a
růst	růst	k1gInSc1	růst
činil	činit	k5eAaImAgInS	činit
8,3	[number]	k4	8,3
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Turistický	turistický	k2eAgInSc4d1	turistický
ruch	ruch	k1gInSc4	ruch
===	===	k?	===
</s>
</p>
<p>
<s>
Laos	Laos	k1gInSc1	Laos
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
při	při	k7c6	při
dodržení	dodržení	k1gNnSc6	dodržení
základních	základní	k2eAgNnPc2d1	základní
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
,	,	kIx,	,
bezpečná	bezpečný	k2eAgFnSc1d1	bezpečná
země	země	k1gFnSc1	země
pro	pro	k7c4	pro
cestování	cestování	k1gNnSc4	cestování
<g/>
.	.	kIx.	.
</s>
<s>
Turistická	turistický	k2eAgNnPc4d1	turistické
víza	vízo	k1gNnPc4	vízo
dostanou	dostat	k5eAaPmIp3nP	dostat
občané	občan	k1gMnPc1	občan
ČR	ČR	kA	ČR
bez	bez	k7c2	bez
problémů	problém	k1gInPc2	problém
při	při	k7c6	při
vstupu	vstup	k1gInSc6	vstup
do	do	k7c2	do
země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
30	[number]	k4	30
USD	USD	kA	USD
s	s	k7c7	s
platností	platnost	k1gFnSc7	platnost
30	[number]	k4	30
dnů	den	k1gInPc2	den
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zajímavé	zajímavý	k2eAgInPc1d1	zajímavý
turistické	turistický	k2eAgInPc1d1	turistický
cíle	cíl	k1gInPc1	cíl
===	===	k?	===
</s>
</p>
<p>
<s>
Luang	Luang	k1gMnSc1	Luang
Nam	Nam	k1gMnSc1	Nam
Tha	Tha	k1gMnSc1	Tha
</s>
</p>
<p>
<s>
Vang	Vang	k1gMnSc1	Vang
Vieng	Vieng	k1gMnSc1	Vieng
</s>
</p>
<p>
<s>
Luang	Luang	k1gMnSc1	Luang
Prabang	Prabang	k1gMnSc1	Prabang
</s>
</p>
<p>
<s>
Vientiane	Vientianout	k5eAaPmIp3nS	Vientianout
</s>
</p>
<p>
<s>
Planina	planina	k1gFnSc1	planina
hrnců	hrnec	k1gInPc2	hrnec
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Laos	Laos	k1gInSc1	Laos
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
NOŽINA	NOŽINA	kA	NOŽINA
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Laosu	Laos	k1gInSc2	Laos
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7422	[number]	k4	7422
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
63	[number]	k4	63
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SLAVICKÝ	SLAVICKÝ	kA	SLAVICKÝ
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
.	.	kIx.	.
</s>
<s>
Scénické	scénický	k2eAgFnPc1d1	scénická
tradice	tradice	k1gFnPc1	tradice
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
KANT	Kant	k1gMnSc1	Kant
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7437	[number]	k4	7437
<g/>
-	-	kIx~	-
<g/>
175	[number]	k4	175
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Laos	Laos	k1gInSc1	Laos
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Laos	Laos	k1gInSc1	Laos
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
portál	portál	k1gInSc1	portál
Laosu	Laos	k1gInSc2	Laos
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Laos	Laos	k1gInSc1	Laos
na	na	k7c4	na
ODP	ODP	kA	ODP
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Laos	Laos	k1gInSc1	Laos
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
Wikivoyage	Wikivoyag	k1gFnSc2	Wikivoyag
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dalky	Dalka	k1gFnPc1	Dalka
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
Laoská	laoský	k2eAgFnSc1d1	laoská
lidově	lidově	k6eAd1	lidově
demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
</s>
</p>
<p>
<s>
Laos	Laos	k1gInSc1	Laos
-	-	kIx~	-
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc2	International
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Laos	Laos	k1gInSc1	Laos
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc1	house
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
.	.	kIx.	.
</s>
<s>
BTI	BTI	kA	BTI
2010	[number]	k4	2010
–	–	k?	–
Laos	Laos	k1gInSc1	Laos
Country	country	k2eAgInSc1d1	country
Report	report	k1gInSc1	report
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Gütersloh	Gütersloh	k1gMnSc1	Gütersloh
<g/>
:	:	kIx,	:
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Note	k1gInSc1	Note
<g/>
:	:	kIx,	:
Laos	Laos	k1gInSc1	Laos
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011-06-06	[number]	k4	2011-06-06
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2009	[number]	k4	2009
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Cyprus	Cyprus	k1gInSc1	Cyprus
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2011-08-03	[number]	k4	2011-08-03
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Bangkoku	Bangkok	k1gInSc6	Bangkok
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Laos	Laos	k1gInSc1	Laos
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2010-12-17	[number]	k4	2010-12-17
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
DOMMEN	DOMMEN	kA	DOMMEN
<g/>
,	,	kIx,	,
Arthur	Arthur	k1gMnSc1	Arthur
J	J	kA	J
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Laos	Laos	k1gInSc1	Laos
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
