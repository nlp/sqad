<p>
<s>
Žítkovské	Žítkovský	k2eAgFnPc4d1	Žítkovský
bohyně	bohyně	k1gFnPc4	bohyně
je	být	k5eAaImIp3nS	být
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
napsala	napsat	k5eAaPmAgFnS	napsat
česká	český	k2eAgFnSc1d1	Česká
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
a	a	k8xC	a
kurátorka	kurátorka	k1gFnSc1	kurátorka
Kateřina	Kateřina	k1gFnSc1	Kateřina
Tučková	Tučková	k1gFnSc1	Tučková
<g/>
.	.	kIx.	.
</s>
<s>
Knižně	knižně	k6eAd1	knižně
vyšel	vyjít	k5eAaPmAgInS	vyjít
román	román	k1gInSc1	román
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
podruhé	podruhé	k6eAd1	podruhé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Host	host	k1gMnSc1	host
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgNnSc1	první
vydání	vydání	k1gNnSc1	vydání
románu	román	k1gInSc2	román
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
šedesátitisícovém	šedesátitisícový	k2eAgInSc6d1	šedesátitisícový
nákladu	náklad	k1gInSc6	náklad
<g/>
,	,	kIx,	,
druhého	druhý	k4xOgMnSc2	druhý
<g/>
,	,	kIx,	,
brožovaného	brožovaný	k2eAgNnSc2d1	brožované
vydání	vydání	k1gNnSc2	vydání
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
22	[number]	k4	22
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
přes	přes	k7c4	přes
100	[number]	k4	100
000	[number]	k4	000
výtisků	výtisk	k1gInPc2	výtisk
<g/>
;	;	kIx,	;
kniha	kniha	k1gFnSc1	kniha
vyšla	vyjít	k5eAaPmAgFnS	vyjít
ve	v	k7c6	v
dvanácti	dvanáct	k4xCc6	dvanáct
jazycích	jazyk	k1gInPc6	jazyk
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
také	také	k9	také
v	v	k7c6	v
arabštině	arabština	k1gFnSc6	arabština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
kniha	kniha	k1gFnSc1	kniha
získala	získat	k5eAaPmAgFnS	získat
literární	literární	k2eAgFnSc4d1	literární
Cenu	cena	k1gFnSc4	cena
Josefa	Josef	k1gMnSc2	Josef
Škvoreckého	Škvorecký	k2eAgMnSc2d1	Škvorecký
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
Český	český	k2eAgInSc1d1	český
bestseller	bestseller	k1gInSc1	bestseller
2012	[number]	k4	2012
<g/>
;	;	kIx,	;
Magnesia	magnesium	k1gNnSc2	magnesium
Litera	litera	k1gFnSc1	litera
2013	[number]	k4	2013
-	-	kIx~	-
Kosmas	Kosmas	k1gMnSc1	Kosmas
cena	cena	k1gFnSc1	cena
čtenářů	čtenář	k1gMnPc2	čtenář
<g/>
.	.	kIx.	.
<g/>
Román	Román	k1gMnSc1	Román
byl	být	k5eAaImAgMnS	být
nejpůjčovanější	půjčovaný	k2eAgFnSc7d3	nejpůjčovanější
knihou	kniha	k1gFnSc7	kniha
od	od	k7c2	od
českého	český	k2eAgMnSc2d1	český
autora	autor	k1gMnSc2	autor
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
knihovnách	knihovna	k1gFnPc6	knihovna
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsah	obsah	k1gInSc1	obsah
knihy	kniha	k1gFnSc2	kniha
==	==	k?	==
</s>
</p>
<p>
<s>
Dora	Dora	k1gFnSc1	Dora
Idesová	Idesový	k2eAgFnSc1d1	Idesový
je	být	k5eAaImIp3nS	být
neteř	neteř	k1gFnSc1	neteř
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
Žítkovských	Žítkovský	k2eAgFnPc2d1	Žítkovský
bohyň	bohyně	k1gFnPc2	bohyně
zvané	zvaný	k2eAgFnPc4d1	zvaná
Surmena	Surmen	k2eAgNnPc4d1	Surmen
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
na	na	k7c6	na
Kopanicích	Kopanice	k1gFnPc6	Kopanice
nad	nad	k7c7	nad
Starým	starý	k2eAgInSc7d1	starý
Hrozenkovem	Hrozenkov	k1gInSc7	Hrozenkov
je	být	k5eAaImIp3nS	být
však	však	k9	však
tvrdý	tvrdý	k2eAgMnSc1d1	tvrdý
jako	jako	k8xC	jako
zdejší	zdejší	k2eAgMnSc1d1	zdejší
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
i	i	k8xC	i
ženy	žena	k1gFnPc1	žena
pijí	pít	k5eAaImIp3nP	pít
pálenku	pálenka	k1gFnSc4	pálenka
<g/>
,	,	kIx,	,
polovina	polovina	k1gFnSc1	polovina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
negramotná	gramotný	k2eNgFnSc1d1	negramotná
<g/>
,	,	kIx,	,
žijí	žít	k5eAaImIp3nP	žít
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
okolní	okolní	k2eAgFnSc4d1	okolní
civilizaci	civilizace	k1gFnSc4	civilizace
i	i	k9	i
lidově	lidově	k6eAd1	lidově
demokratické	demokratický	k2eAgNnSc4d1	demokratické
zřízení	zřízení	k1gNnSc4	zřízení
<g/>
.	.	kIx.	.
</s>
<s>
Poradili	poradit	k5eAaPmAgMnP	poradit
si	se	k3xPyFc3	se
za	za	k7c2	za
honů	hon	k1gInPc2	hon
na	na	k7c4	na
čarodějnice	čarodějnice	k1gFnPc4	čarodějnice
<g/>
,	,	kIx,	,
za	za	k7c2	za
nacistů	nacista	k1gMnPc2	nacista
i	i	k9	i
teď	teď	k6eAd1	teď
<g/>
.	.	kIx.	.
</s>
<s>
Bohyně	bohyně	k1gFnSc2	bohyně
to	ten	k3xDgNnSc1	ten
neměly	mít	k5eNaImAgFnP	mít
jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
nikdy	nikdy	k6eAd1	nikdy
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
režimů	režim	k1gInPc2	režim
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
je	být	k5eAaImIp3nS	být
bez	bez	k7c2	bez
výjimky	výjimka	k1gFnSc2	výjimka
pronásledovaly	pronásledovat	k5eAaImAgInP	pronásledovat
<g/>
,	,	kIx,	,
musely	muset	k5eAaImAgInP	muset
reagovat	reagovat	k5eAaBmF	reagovat
i	i	k9	i
na	na	k7c4	na
nenávist	nenávist	k1gFnSc4	nenávist
a	a	k8xC	a
zášť	zášť	k1gFnSc4	zášť
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
jejich	jejich	k3xOp3gNnSc1	jejich
zaříkávání	zaříkávání	k1gNnSc1	zaříkávání
nepomohlo	pomoct	k5eNaPmAgNnS	pomoct
nebo	nebo	k8xC	nebo
kteří	který	k3yIgMnPc1	který
jim	on	k3xPp3gMnPc3	on
záviděli	závidět	k5eAaImAgMnP	závidět
tu	tu	k6eAd1	tu
trochu	trochu	k6eAd1	trochu
věhlasu	věhlas	k1gInSc2	věhlas
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
měly	mít	k5eAaImAgFnP	mít
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
někoho	někdo	k3yInSc4	někdo
čarodějnice	čarodějnice	k1gFnSc1	čarodějnice
a	a	k8xC	a
podvodnice	podvodnice	k1gFnSc1	podvodnice
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
jiného	jiný	k1gMnSc4	jiný
moudré	moudrý	k2eAgFnSc2d1	moudrá
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
si	se	k3xPyFc3	se
své	svůj	k3xOyFgFnSc2	svůj
dovednosti	dovednost	k1gFnSc2	dovednost
a	a	k8xC	a
tajemství	tajemství	k1gNnSc2	tajemství
předávají	předávat	k5eAaImIp3nP	předávat
z	z	k7c2	z
generace	generace	k1gFnSc2	generace
na	na	k7c4	na
generaci	generace	k1gFnSc4	generace
po	po	k7c4	po
mnoho	mnoho	k4c4	mnoho
desítek	desítka	k1gFnPc2	desítka
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Žítkovské	Žítkovský	k2eAgFnPc1d1	Žítkovský
bohyně	bohyně	k1gFnPc1	bohyně
svou	svůj	k3xOyFgFnSc4	svůj
magickou	magický	k2eAgFnSc4d1	magická
sílu	síla	k1gFnSc4	síla
čerpají	čerpat	k5eAaImIp3nP	čerpat
z	z	k7c2	z
neméně	málo	k6eNd2	málo
magické	magický	k2eAgFnSc2d1	magická
krajiny	krajina	k1gFnSc2	krajina
kolem	kolem	k7c2	kolem
obce	obec	k1gFnSc2	obec
Žítkové	Žítkový	k2eAgFnSc2d1	Žítkový
na	na	k7c6	na
Moravských	moravský	k2eAgFnPc6d1	Moravská
Kopanicích	Kopanice	k1gFnPc6	Kopanice
v	v	k7c6	v
Bílých	bílý	k2eAgInPc6d1	bílý
Karpatech	Karpaty	k1gInPc6	Karpaty
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
svérázné	svérázný	k2eAgFnSc2d1	svérázná
kopcovité	kopcovitý	k2eAgFnSc2d1	kopcovitá
krajiny	krajina	k1gFnSc2	krajina
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
lidé	člověk	k1gMnPc1	člověk
žili	žít	k5eAaImAgMnP	žít
roztroušeně	roztroušeně	k6eAd1	roztroušeně
po	po	k7c6	po
samotách	samota	k1gFnPc6	samota
a	a	k8xC	a
vystačili	vystačit	k5eAaBmAgMnP	vystačit
si	se	k3xPyFc3	se
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
málem	málo	k1gNnSc7	málo
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
měli	mít	k5eAaImAgMnP	mít
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
výdobytky	výdobytek	k1gInPc7	výdobytek
okolního	okolní	k2eAgInSc2d1	okolní
moderního	moderní	k2eAgInSc2d1	moderní
světa	svět	k1gInSc2	svět
dostávaly	dostávat	k5eAaImAgInP	dostávat
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
pozvolna	pozvolna	k6eAd1	pozvolna
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
zde	zde	k6eAd1	zde
mohly	moct	k5eAaImAgFnP	moct
vládnout	vládnout	k5eAaImF	vládnout
bohyně	bohyně	k1gFnPc1	bohyně
ještě	ještě	k6eAd1	ještě
takřka	takřka	k6eAd1	takřka
do	do	k7c2	do
nedávné	dávný	k2eNgFnSc2d1	nedávná
minulosti	minulost	k1gFnSc2	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Dora	Dora	k1gFnSc1	Dora
Idesová	Idesová	k1gFnSc1	Idesová
jakožto	jakožto	k8xS	jakožto
poslední	poslední	k2eAgInSc1d1	poslední
článek	článek	k1gInSc1	článek
na	na	k7c6	na
konci	konec	k1gInSc6	konec
řetězce	řetězec	k1gInSc2	řetězec
rodu	rod	k1gInSc2	rod
bohyní	bohyně	k1gFnPc2	bohyně
ze	z	k7c2	z
Žítkové	Žítková	k1gFnSc2	Žítková
se	se	k3xPyFc4	se
ocitá	ocitat	k5eAaImIp3nS	ocitat
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
<g/>
.	.	kIx.	.
</s>
<s>
Umění	umění	k1gNnSc1	umění
bohovat	bohovat	k5eAaBmF	bohovat
ji	on	k3xPp3gFnSc4	on
její	její	k3xOp3gFnSc1	její
teta	teta	k1gFnSc1	teta
<g/>
,	,	kIx,	,
věhlasná	věhlasný	k2eAgFnSc1d1	věhlasná
bohyně	bohyně	k1gFnSc1	bohyně
Surmena	Surmen	k2eAgFnSc1d1	Surmena
<g/>
,	,	kIx,	,
nenaučila	naučit	k5eNaPmAgFnS	naučit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Dora	Dora	k1gFnSc1	Dora
této	tento	k3xDgFnSc2	tento
skutečnosti	skutečnost	k1gFnSc2	skutečnost
neželí	želet	k5eNaImIp3nS	želet
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
vystudovaná	vystudovaný	k2eAgFnSc1d1	vystudovaná
vědkyně	vědkyně	k1gFnSc1	vědkyně
má	mít	k5eAaImIp3nS	mít
k	k	k7c3	k
těmto	tento	k3xDgInPc3	tento
pověrčivým	pověrčivý	k2eAgInPc3d1	pověrčivý
rituálům	rituál	k1gInPc3	rituál
jistou	jistý	k2eAgFnSc4d1	jistá
nedůvěru	nedůvěra	k1gFnSc4	nedůvěra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
však	však	k9	však
cítí	cítit	k5eAaImIp3nS	cítit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
něco	něco	k3yInSc1	něco
tajemného	tajemný	k2eAgNnSc2d1	tajemné
po	po	k7c6	po
jejích	její	k3xOp3gFnPc6	její
předchůdkyních	předchůdkyně	k1gFnPc6	předchůdkyně
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dora	Dora	k1gFnSc1	Dora
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
vydat	vydat	k5eAaPmF	vydat
cestou	cesta	k1gFnSc7	cesta
k	k	k7c3	k
odhalení	odhalení	k1gNnSc3	odhalení
tajemství	tajemství	k1gNnSc2	tajemství
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
ji	on	k3xPp3gFnSc4	on
celým	celý	k2eAgInSc7d1	celý
příběhem	příběh	k1gInSc7	příběh
provází	provázet	k5eAaImIp3nS	provázet
jako	jako	k9	jako
temný	temný	k2eAgInSc1d1	temný
stín	stín	k1gInSc1	stín
<g/>
.	.	kIx.	.
</s>
<s>
Pátrá	pátrat	k5eAaImIp3nS	pátrat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
paměti	paměť	k1gFnSc6	paměť
<g/>
,	,	kIx,	,
po	po	k7c6	po
archivech	archiv	k1gInPc6	archiv
<g/>
,	,	kIx,	,
i	i	k8xC	i
u	u	k7c2	u
sousedů	soused	k1gMnPc2	soused
z	z	k7c2	z
Žítkové	Žítková	k1gFnSc2	Žítková
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
je	být	k5eAaImIp3nS	být
prokládán	prokládat	k5eAaImNgInS	prokládat
archivními	archivní	k2eAgInPc7d1	archivní
záznamy	záznam	k1gInPc7	záznam
<g/>
,	,	kIx,	,
lékařskými	lékařský	k2eAgFnPc7d1	lékařská
zprávami	zpráva	k1gFnPc7	zpráva
<g/>
,	,	kIx,	,
odbornými	odborný	k2eAgInPc7d1	odborný
posudky	posudek	k1gInPc7	posudek
i	i	k8xC	i
částmi	část	k1gFnPc7	část
Dořiny	Dořin	k2eAgFnSc2d1	Dořina
diplomové	diplomový	k2eAgFnSc2d1	Diplomová
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vynikla	vyniknout	k5eAaPmAgFnS	vyniknout
věrohodnost	věrohodnost	k1gFnSc1	věrohodnost
předkládaného	předkládaný	k2eAgInSc2d1	předkládaný
příběhu	příběh	k1gInSc2	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
osobitého	osobitý	k2eAgInSc2d1	osobitý
stylu	styl	k1gInSc2	styl
vyprávění	vyprávění	k1gNnSc2	vyprávění
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bez	bez	k7c2	bez
patosu	patos	k1gInSc2	patos
líčí	líčit	k5eAaImIp3nS	líčit
umění	umění	k1gNnSc1	umění
bohyní	bohyně	k1gFnPc2	bohyně
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
strohost	strohost	k1gFnSc1	strohost
a	a	k8xC	a
zdrcující	zdrcující	k2eAgFnSc1d1	zdrcující
věcnost	věcnost	k1gFnSc1	věcnost
úředních	úřední	k2eAgInPc2d1	úřední
dokumentů	dokument	k1gInPc2	dokument
jako	jako	k8xS	jako
byrokratický	byrokratický	k2eAgInSc1d1	byrokratický
vykřičník	vykřičník	k1gInSc1	vykřičník
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zkresluje	zkreslovat	k5eAaImIp3nS	zkreslovat
složitost	složitost	k1gFnSc4	složitost
a	a	k8xC	a
mnohostrannost	mnohostrannost	k1gFnSc4	mnohostrannost
lidského	lidský	k2eAgInSc2d1	lidský
života	život	k1gInSc2	život
do	do	k7c2	do
jednoduché	jednoduchý	k2eAgFnSc2d1	jednoduchá
rovnice	rovnice	k1gFnSc2	rovnice
<g/>
.	.	kIx.	.
</s>
<s>
Dora	Dora	k1gFnSc1	Dora
sama	sám	k3xTgFnSc1	sám
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
poněkud	poněkud	k6eAd1	poněkud
tajemně	tajemně	k6eAd1	tajemně
<g/>
,	,	kIx,	,
neosobně	osobně	k6eNd1	osobně
a	a	k8xC	a
nepřístupně	přístupně	k6eNd1	přístupně
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
hledá	hledat	k5eAaImIp3nS	hledat
své	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
životě	život	k1gInSc6	život
a	a	k8xC	a
vcelku	vcelku	k6eAd1	vcelku
působí	působit	k5eAaImIp3nS	působit
spíš	spíš	k9	spíš
jako	jako	k9	jako
zprostředkovatelka	zprostředkovatelka	k1gFnSc1	zprostředkovatelka
vyprávění	vyprávění	k1gNnSc2	vyprávění
než	než	k8xS	než
jako	jako	k9	jako
nositelka	nositelka	k1gFnSc1	nositelka
děje	děj	k1gInSc2	děj
<g/>
.	.	kIx.	.
</s>
<s>
Logické	logický	k2eAgNnSc1d1	logické
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
vyprávění	vyprávění	k1gNnSc4	vyprávění
ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
osobě	osoba	k1gFnSc6	osoba
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
rozprostřeno	rozprostřít	k5eAaPmNgNnS	rozprostřít
na	na	k7c6	na
širokém	široký	k2eAgInSc6d1	široký
časovém	časový	k2eAgInSc6d1	časový
horizontu	horizont	k1gInSc6	horizont
a	a	k8xC	a
větví	větvit	k5eAaImIp3nS	větvit
se	se	k3xPyFc4	se
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
dějových	dějový	k2eAgFnPc2d1	dějová
linek	linka	k1gFnPc2	linka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
postavy	postava	k1gFnPc1	postava
==	==	k?	==
</s>
</p>
<p>
<s>
Dora	Dora	k1gFnSc1	Dora
Idesová	Idesová	k1gFnSc1	Idesová
-	-	kIx~	-
hlavní	hlavní	k2eAgFnSc1d1	hlavní
postava	postava	k1gFnSc1	postava
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
pracuje	pracovat	k5eAaImIp3nS	pracovat
jako	jako	k9	jako
etnografka	etnografka	k1gFnSc1	etnografka
a	a	k8xC	a
rozplétá	rozplétat	k5eAaImIp3nS	rozplétat
minulost	minulost	k1gFnSc4	minulost
žítkovských	žítkovský	k2eAgFnPc2d1	žítkovský
bohyní	bohyně	k1gFnPc2	bohyně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Terézie	Terézie	k1gFnSc1	Terézie
Surmenová	Surmenový	k2eAgFnSc1d1	Surmenový
-	-	kIx~	-
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
bohyň	bohyně	k1gFnPc2	bohyně
<g/>
,	,	kIx,	,
teta	teta	k1gFnSc1	teta
Dory	Dora	k1gFnSc2	Dora
a	a	k8xC	a
Jakuba	Jakub	k1gMnSc2	Jakub
Idesových	Idesová	k1gFnPc2	Idesová
<g/>
,	,	kIx,	,
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
jejich	jejich	k3xOp3gFnSc2	jejich
matky	matka	k1gFnSc2	matka
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
ujala	ujmout	k5eAaPmAgFnS	ujmout
a	a	k8xC	a
vychovávala	vychovávat	k5eAaImAgFnS	vychovávat
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
na	na	k7c4	na
nucenou	nucený	k2eAgFnSc4d1	nucená
léčbu	léčba	k1gFnSc4	léčba
v	v	k7c6	v
Psychiatrické	psychiatrický	k2eAgFnSc6d1	psychiatrická
léčebně	léčebna	k1gFnSc6	léčebna
v	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ji	on	k3xPp3gFnSc4	on
Dora	Dora	k1gFnSc1	Dora
navštěvovala	navštěvovat	k5eAaImAgFnS	navštěvovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jakub	Jakub	k1gMnSc1	Jakub
Ides	Ides	k1gInSc1	Ides
-	-	kIx~	-
bratr	bratr	k1gMnSc1	bratr
Dory	Dora	k1gFnSc2	Dora
Idesové	Idesová	k1gFnSc2	Idesová
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
Apertův	Apertův	k2eAgInSc4d1	Apertův
syndrom	syndrom	k1gInSc4	syndrom
(	(	kIx(	(
<g/>
fyzické	fyzický	k2eAgNnSc4d1	fyzické
i	i	k8xC	i
mentální	mentální	k2eAgNnSc4d1	mentální
postižení	postižení	k1gNnSc4	postižení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Adaptace	adaptace	k1gFnSc1	adaptace
==	==	k?	==
</s>
</p>
<p>
<s>
Dramatizaci	dramatizace	k1gFnSc4	dramatizace
Žítkovských	Žítkovský	k2eAgFnPc2d1	Žítkovský
bohyní	bohyně	k1gFnPc2	bohyně
hrají	hrát	k5eAaImIp3nP	hrát
v	v	k7c6	v
Městském	městský	k2eAgNnSc6d1	Městské
divadle	divadlo	k1gNnSc6	divadlo
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
Dodo	dodo	k1gMnSc1	dodo
Gombára	Gombár	k1gMnSc2	Gombár
a	a	k8xC	a
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
pod	pod	k7c7	pod
Palmovkou	Palmovka	k1gFnSc7	Palmovka
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
Michala	Michal	k1gMnSc4	Michal
Langa	Lang	k1gMnSc4	Lang
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
románu	román	k1gInSc2	román
se	se	k3xPyFc4	se
chystá	chystat	k5eAaImIp3nS	chystat
také	také	k9	také
filmové	filmový	k2eAgNnSc1d1	filmové
zpracování	zpracování	k1gNnSc1	zpracování
Žítkovské	Žítkovský	k2eAgFnSc2d1	Žítkovský
bohyně	bohyně	k1gFnSc2	bohyně
<g/>
.	.	kIx.	.
<g/>
Stejnojmennou	stejnojmenný	k2eAgFnSc4d1	stejnojmenná
audioknihu	audioknih	k1gInSc2	audioknih
načetla	načíst	k5eAaBmAgFnS	načíst
Tereza	Tereza	k1gFnSc1	Tereza
Bebarová	Bebarová	k1gFnSc1	Bebarová
<g/>
,	,	kIx,	,
historickými	historický	k2eAgInPc7d1	historický
dokumenty	dokument	k1gInPc7	dokument
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
ústřední	ústřední	k2eAgFnSc1d1	ústřední
hrdinka	hrdinka	k1gFnSc1	hrdinka
studuje	studovat	k5eAaImIp3nS	studovat
<g/>
,	,	kIx,	,
provází	provázet	k5eAaImIp3nS	provázet
Miroslav	Miroslav	k1gMnSc1	Miroslav
Táborský	Táborský	k1gMnSc1	Táborský
(	(	kIx(	(
<g/>
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
OneHotBook	OneHotBook	k1gInSc1	OneHotBook
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Psychiatrická	psychiatrický	k2eAgFnSc1d1	psychiatrická
nemocnice	nemocnice	k1gFnSc1	nemocnice
v	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Jilík	Jilík	k1gMnSc1	Jilík
–	–	k?	–
Žítkovské	Žítkovský	k2eAgNnSc1d1	Žítkovský
čarování	čarování	k1gNnSc1	čarování
<g/>
:	:	kIx,	:
pravdivý	pravdivý	k2eAgInSc1d1	pravdivý
příběh	příběh	k1gInSc1	příběh
žítkovských	žítkovský	k2eAgFnPc2d1	žítkovský
bohyní	bohyně	k1gFnPc2	bohyně
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
