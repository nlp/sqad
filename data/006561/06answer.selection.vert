<s>
Při	při	k7c6	při
transkripci	transkripce	k1gFnSc6	transkripce
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
přibližně	přibližně	k6eAd1	přibližně
jedna	jeden	k4xCgFnSc1	jeden
chyba	chyba	k1gFnSc1	chyba
za	za	k7c4	za
10	[number]	k4	10
000	[number]	k4	000
bází	báze	k1gFnPc2	báze
(	(	kIx(	(
<g/>
chybovost	chybovost	k1gFnSc1	chybovost
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
však	však	k9	však
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
na	na	k7c4	na
závadu	závada	k1gFnSc4	závada
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
životnost	životnost	k1gFnSc1	životnost
RNA	RNA	kA	RNA
molekul	molekula	k1gFnPc2	molekula
bývá	bývat	k5eAaImIp3nS	bývat
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
poměrně	poměrně	k6eAd1	poměrně
krátká	krátký	k2eAgFnSc1d1	krátká
<g/>
.	.	kIx.	.
</s>
