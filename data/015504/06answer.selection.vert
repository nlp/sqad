<s>
Doktor	doktor	k1gMnSc1	doktor
práv	práv	k2eAgMnSc1d1	práv
(	(	kIx(	(
<g/>
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
JUDr.	JUDr.	kA	JUDr.
psané	psaný	k2eAgInPc4d1	psaný
před	před	k7c7	před
jménem	jméno	k1gNnSc7	jméno
<g/>
;	;	kIx,	;
dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
též	též	k9	též
používala	používat	k5eAaImAgFnS	používat
zkratka	zkratka	k1gFnSc1	zkratka
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
jur	jura	k1gFnPc2	jura
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
akademický	akademický	k2eAgInSc4d1	akademický
titul	titul	k1gInSc4	titul
udělovaný	udělovaný	k2eAgInSc4d1	udělovaný
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
