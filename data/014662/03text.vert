<s>
Džavád	Džaváda	k1gFnPc2
Mahdžúb	Mahdžúba	k1gFnPc2
</s>
<s>
Javad	Javad	k6eAd1
Mahjoub	Mahjoub	k1gInSc1
Narození	narození	k1gNnSc2
</s>
<s>
1991	#num#	k4
(	(	kIx(
<g/>
29	#num#	k4
<g/>
–	–	k?
<g/>
30	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Mašhad	Mašhad	k1gInSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
judista	judista	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Javad	Javad	k1gInSc1
Mahjoub	Mahjouba	k1gFnPc2
(	(	kIx(
<g/>
Džavád	Džaváda	k1gFnPc2
Mahdžúb	Mahdžúb	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
(	(	kIx(
<g/>
*	*	kIx~
26	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1991	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
íránský	íránský	k2eAgMnSc1d1
zápasník	zápasník	k1gMnSc1
<g/>
–	–	k?
<g/>
judista	judista	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Sportovní	sportovní	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
</s>
<s>
Na	na	k7c6
mezinárodní	mezinárodní	k2eAgFnSc6d1
scéně	scéna	k1gFnSc6
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
dosáhl	dosáhnout	k5eAaPmAgInS
na	na	k7c4
asijkou	asijka	k1gFnSc7
kontinentální	kontinentální	k2eAgFnSc4d1
kvotu	kvota	k1gFnSc4
pro	pro	k7c4
účast	účast	k1gFnSc4
na	na	k7c6
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
v	v	k7c6
Londýně	Londýn	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
pár	pár	k4xCyI
dní	den	k1gInPc2
před	před	k7c7
startem	start	k1gInSc7
soutěží	soutěž	k1gFnPc2
byl	být	k5eAaImAgInS
z	z	k7c2
turnaje	turnaj	k1gInSc2
ztažen	ztažna	k1gFnPc2
oficiálně	oficiálně	k6eAd1
z	z	k7c2
důvodu	důvod	k1gInSc2
nemoci	nemoc	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
svojí	svůj	k3xOyFgFnSc7
olympijskou	olympijský	k2eAgFnSc7d1
premiéru	premiéra	k1gFnSc4
si	se	k3xPyFc3
tak	tak	k9
musel	muset	k5eAaImAgMnS
počkat	počkat	k5eAaPmF
do	do	k7c2
roku	rok	k1gInSc2
2016	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
mu	on	k3xPp3gMnSc3
žádná	žádný	k3yNgFnSc1
z	z	k7c2
okolností	okolnost	k1gFnPc2
nebránila	bránit	k5eNaImAgFnS
účastnit	účastnit	k5eAaImF
se	se	k3xPyFc4
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
v	v	k7c6
Riu	Riu	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prvním	první	k4xOgInSc6
kole	kolo	k1gNnSc6
nastoupil	nastoupit	k5eAaPmAgMnS
proti	proti	k7c3
Tomu	ten	k3xDgMnSc3
Nikiforovi	Nikifor	k1gMnSc3
z	z	k7c2
Belgie	Belgie	k1gFnSc2
a	a	k8xC
po	po	k7c6
vyrovnaném	vyrovnaný	k2eAgInSc6d1
průběhu	průběh	k1gInSc6
zápasu	zápas	k1gInSc2
prohrál	prohrát	k5eAaPmAgMnS
na	na	k7c4
jedno	jeden	k4xCgNnSc4
napomenutí	napomenutí	k1gNnSc4
(	(	kIx(
<g/>
šido	šido	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vítězství	vítězství	k1gNnSc1
</s>
<s>
2013	#num#	k4
-	-	kIx~
1	#num#	k4
<g/>
x	x	k?
světový	světový	k2eAgInSc4d1
pohár	pohár	k1gInSc4
(	(	kIx(
<g/>
Moskva	Moskva	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
2016	#num#	k4
-	-	kIx~
1	#num#	k4
<g/>
x	x	k?
světový	světový	k2eAgInSc4d1
pohár	pohár	k1gInSc4
(	(	kIx(
<g/>
Oberwart	Oberwart	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Výsledky	výsledek	k1gInPc1
</s>
<s>
Turnaj	turnaj	k1gInSc1
</s>
<s>
2010	#num#	k4
</s>
<s>
2011	#num#	k4
</s>
<s>
2012	#num#	k4
</s>
<s>
2013	#num#	k4
</s>
<s>
2014	#num#	k4
</s>
<s>
2015	#num#	k4
</s>
<s>
2016	#num#	k4
</s>
<s>
19	#num#	k4
</s>
<s>
20	#num#	k4
</s>
<s>
21	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
23	#num#	k4
</s>
<s>
24	#num#	k4
</s>
<s>
25	#num#	k4
</s>
<s>
polotěžká	polotěžký	k2eAgFnSc1d1
váha	váha	k1gFnSc1
</s>
<s>
Olympijské	olympijský	k2eAgFnPc1d1
hryDNSúč	hryDNSúč	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
<g/>
—	—	k?
<g/>
úč	úč	k?
<g/>
.	.	kIx.
<g/>
úč	úč	k?
<g/>
.	.	kIx.
<g/>
—	—	k?
</s>
<s>
Asijské	asijský	k2eAgFnPc1d1
hry	hra	k1gFnPc1
<g/>
7.5	7.5	k4
<g/>
.	.	kIx.
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Asie	Asie	k1gFnSc2
<g/>
5.2.1.7	5.2.1.7	k4
<g/>
.5	.5	k4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
iranian	iranian	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Výsledky	výsledek	k1gInPc1
a	a	k8xC
novinky	novinka	k1gFnPc1
Javada	Javada	k1gFnSc1
Mahjouba	Mahjouba	k1gFnSc1
na	na	k7c6
Judoinside	Judoinsid	k1gInSc5
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
<s>
Výsledky	výsledek	k1gInPc1
a	a	k8xC
novinky	novinka	k1gFnPc1
Javada	Javada	k1gFnSc1
Mahjouba	Mahjouba	k1gFnSc1
na	na	k7c6
Judobase	Judobasa	k1gFnSc6
<g/>
.	.	kIx.
<g/>
org	org	k?
</s>
