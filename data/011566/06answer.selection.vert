<s>
V	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
obchodní	obchodní	k2eAgFnSc6d1	obchodní
síti	síť	k1gFnSc6	síť
se	se	k3xPyFc4	se
pšeničná	pšeničný	k2eAgFnSc1d1	pšeničná
mouka	mouka	k1gFnSc1	mouka
(	(	kIx(	(
<g/>
nejběžnější	běžný	k2eAgInSc1d3	nejběžnější
typ	typ	k1gInSc1	typ
<g/>
)	)	kIx)	)
běžně	běžně	k6eAd1	běžně
prodává	prodávat	k5eAaImIp3nS	prodávat
ve	v	k7c6	v
čtyřech	čtyři	k4xCgInPc6	čtyři
druzích	druh	k1gInPc6	druh
rozlišených	rozlišený	k2eAgInPc6d1	rozlišený
podle	podle	k7c2	podle
tloušťky	tloušťka	k1gFnSc2	tloušťka
zrn	zrno	k1gNnPc2	zrno
<g/>
:	:	kIx,	:
hladká	hladký	k2eAgFnSc1d1	hladká
(	(	kIx(	(
<g/>
nejjemnější	jemný	k2eAgFnSc1d3	nejjemnější
zrnitost	zrnitost	k1gFnSc1	zrnitost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
polohrubá	polohrubý	k2eAgFnSc1d1	polohrubá
<g/>
,	,	kIx,	,
hrubá	hrubý	k2eAgFnSc1d1	hrubá
<g/>
,	,	kIx,	,
krupice	krupice	k1gFnSc1	krupice
(	(	kIx(	(
<g/>
nejhrubší	hrubý	k2eAgFnSc1d3	nejhrubší
zrnitost	zrnitost	k1gFnSc1	zrnitost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
