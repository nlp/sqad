<s desamb="1">
Množina	množina	k1gFnSc1
všech	všecek	k3xTgNnPc2
reálných	reálný	k2eAgNnPc2d1
čísel	číslo	k1gNnPc2
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
R	R	kA
nebo	nebo	k8xC
ℝ	ℝ	k?
Zápis	zápis	k1gInSc1
ℝ	ℝ	k?
označuje	označovat	k5eAaImIp3nS
n-rozměrný	n-rozměrný	k2eAgInSc1d1
vektorový	vektorový	k2eAgInSc1d1
prostor	prostor	k1gInSc1
reálných	reálný	k2eAgNnPc2d1
čísel	číslo	k1gNnPc2
<g/>
.	.	kIx.
</s>