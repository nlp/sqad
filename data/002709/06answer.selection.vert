<s>
Ještěd	Ještěd	k1gInSc1	Ještěd
(	(	kIx(	(
<g/>
1012	[number]	k4	1012
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Jeschken	Jeschken	k2eAgInSc1d1	Jeschken
nebo	nebo	k8xC	nebo
Jeschkenkoppe	Jeschkenkopp	k1gMnSc5	Jeschkenkopp
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hora	hora	k1gFnSc1	hora
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Liberce	Liberec	k1gInSc2	Liberec
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
vrchol	vrchol	k1gInSc4	vrchol
Ještědsko-kozákovského	Ještědskoozákovský	k2eAgInSc2d1	Ještědsko-kozákovský
hřbetu	hřbet	k1gInSc2	hřbet
<g/>
.	.	kIx.	.
</s>
