<p>
<s>
Libyjská	libyjský	k2eAgFnSc1d1	Libyjská
poušť	poušť	k1gFnSc1	poušť
je	být	k5eAaImIp3nS	být
africká	africký	k2eAgFnSc1d1	africká
poušť	poušť	k1gFnSc1	poušť
nacházející	nacházející	k2eAgFnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
Sahary	Sahara	k1gFnSc2	Sahara
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
západního	západní	k2eAgInSc2d1	západní
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
východní	východní	k2eAgFnSc1d1	východní
Libye	Libye	k1gFnSc1	Libye
a	a	k8xC	a
severozápadní	severozápadní	k2eAgFnPc1d1	severozápadní
části	část	k1gFnPc1	část
Súdánu	Súdán	k1gInSc2	Súdán
<g/>
.	.	kIx.	.
</s>
<s>
Zabírá	zabírat	k5eAaImIp3nS	zabírat
území	území	k1gNnSc4	území
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
100	[number]	k4	100
000	[number]	k4	000
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
čtvercového	čtvercový	k2eAgInSc2d1	čtvercový
tvaru	tvar	k1gInSc2	tvar
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
asi	asi	k9	asi
1	[number]	k4	1
100	[number]	k4	100
km	km	kA	km
od	od	k7c2	od
východu	východ	k1gInSc2	východ
na	na	k7c4	na
západ	západ	k1gInSc4	západ
a	a	k8xC	a
1	[number]	k4	1
000	[number]	k4	000
km	km	kA	km
od	od	k7c2	od
severu	sever	k1gInSc2	sever
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Povrch	povrch	k1gInSc1	povrch
pouště	poušť	k1gFnSc2	poušť
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
kamenitý	kamenitý	k2eAgMnSc1d1	kamenitý
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
hamada	hamada	k1gFnSc1	hamada
<g/>
)	)	kIx)	)
s	s	k7c7	s
ostrými	ostrý	k2eAgInPc7d1	ostrý
hřebeny	hřeben	k1gInPc7	hřeben
a	a	k8xC	a
hlubokými	hluboký	k2eAgFnPc7d1	hluboká
proláklinami	proláklina	k1gFnPc7	proláklina
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
části	část	k1gFnPc1	část
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
písečná	písečný	k2eAgNnPc4d1	písečné
moře	moře	k1gNnPc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgNnSc7d3	nejvyšší
místem	místo	k1gNnSc7	místo
pouště	poušť	k1gFnSc2	poušť
je	být	k5eAaImIp3nS	být
náhorní	náhorní	k2eAgFnSc1d1	náhorní
plošina	plošina	k1gFnSc1	plošina
Džilf	Džilf	k1gMnSc1	Džilf
al-Kabír	al-Kabír	k1gMnSc1	al-Kabír
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
jihozápadním	jihozápadní	k2eAgInSc6d1	jihozápadní
Egyptě	Egypt	k1gInSc6	Egypt
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
průměrnou	průměrný	k2eAgFnSc4d1	průměrná
výšku	výška	k1gFnSc4	výška
asi	asi	k9	asi
900	[number]	k4	900
a	a	k8xC	a
1100	[number]	k4	1100
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
osm	osm	k4xCc1	osm
významných	významný	k2eAgFnPc2d1	významná
proláklin	proláklina	k1gFnPc2	proláklina
<g/>
.	.	kIx.	.
</s>
<s>
Převážně	převážně	k6eAd1	převážně
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
oázy	oáza	k1gFnPc4	oáza
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
napájeny	napájet	k5eAaImNgFnP	napájet
podzemní	podzemní	k2eAgFnSc7d1	podzemní
vodou	voda	k1gFnSc7	voda
z	z	k7c2	z
Nilu	Nil	k1gInSc2	Nil
a	a	k8xC	a
představují	představovat	k5eAaImIp3nP	představovat
jediná	jediný	k2eAgNnPc1d1	jediné
místa	místo	k1gNnPc1	místo
v	v	k7c6	v
poušti	poušť	k1gFnSc6	poušť
s	s	k7c7	s
trvalým	trvalý	k2eAgNnSc7d1	trvalé
lidským	lidský	k2eAgNnSc7d1	lidské
osídlením	osídlení	k1gNnSc7	osídlení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kattarská	Kattarský	k2eAgFnSc1d1	Kattarský
proláklina	proláklina	k1gFnSc1	proláklina
===	===	k?	===
</s>
</p>
<p>
<s>
Kattarská	Kattarský	k2eAgFnSc1d1	Kattarský
proláklina	proláklina	k1gFnSc1	proláklina
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
pouště	poušť	k1gFnSc2	poušť
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
asi	asi	k9	asi
15	[number]	k4	15
000	[number]	k4	000
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
této	tento	k3xDgFnSc2	tento
plochy	plocha	k1gFnSc2	plocha
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
pod	pod	k7c7	pod
hladinou	hladina	k1gFnSc7	hladina
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nejnižší	nízký	k2eAgInSc1d3	nejnižší
bod	bod	k1gInSc1	bod
oblasti	oblast	k1gFnSc2	oblast
je	být	k5eAaImIp3nS	být
133	[number]	k4	133
m	m	kA	m
pod	pod	k7c7	pod
mořskou	mořský	k2eAgFnSc7d1	mořská
hladinou	hladina	k1gFnSc7	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Řídce	řídce	k6eAd1	řídce
obydlená	obydlený	k2eAgFnSc1d1	obydlená
proláklina	proláklina	k1gFnSc1	proláklina
je	být	k5eAaImIp3nS	být
pokrytá	pokrytý	k2eAgFnSc1d1	pokrytá
krajinou	krajina	k1gFnSc7	krajina
zvanou	zvaný	k2eAgFnSc7d1	zvaná
badlands	badlandsa	k1gFnPc2	badlandsa
<g/>
,	,	kIx,	,
slanými	slaný	k2eAgFnPc7d1	slaná
bažinami	bažina	k1gFnPc7	bažina
a	a	k8xC	a
slanými	slaný	k2eAgNnPc7d1	slané
jezery	jezero	k1gNnPc7	jezero
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Písečná	písečný	k2eAgNnPc1d1	písečné
moře	moře	k1gNnPc1	moře
===	===	k?	===
</s>
</p>
<p>
<s>
Oblasti	oblast	k1gFnPc1	oblast
písečných	písečný	k2eAgNnPc2d1	písečné
moří	moře	k1gNnPc2	moře
zabírají	zabírat	k5eAaImIp3nP	zabírat
menší	malý	k2eAgFnSc4d2	menší
část	část	k1gFnSc4	část
rozlohy	rozloha	k1gFnSc2	rozloha
celé	celý	k2eAgFnSc2d1	celá
pouště	poušť	k1gFnSc2	poušť
<g/>
,	,	kIx,	,
písečné	písečný	k2eAgFnPc1d1	písečná
duny	duna	k1gFnPc1	duna
zde	zde	k6eAd1	zde
často	často	k6eAd1	často
přesahují	přesahovat	k5eAaImIp3nP	přesahovat
výšku	výška	k1gFnSc4	výška
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
oblasti	oblast	k1gFnPc4	oblast
patří	patřit	k5eAaImIp3nP	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Velké	velký	k2eAgNnSc1d1	velké
písečné	písečný	k2eAgNnSc1d1	písečné
moře	moře	k1gNnSc1	moře
</s>
</p>
<p>
<s>
písečné	písečný	k2eAgNnSc1d1	písečné
moře	moře	k1gNnSc1	moře
Calanscio	Calanscio	k6eAd1	Calanscio
</s>
</p>
<p>
<s>
písečné	písečný	k2eAgNnSc1d1	písečné
moře	moře	k1gNnSc1	moře
Ribiana	Ribian	k1gMnSc2	Ribian
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Prvním	první	k4xOgMnSc7	první
Evropanem	Evropan	k1gMnSc7	Evropan
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
o	o	k7c6	o
prozkoumání	prozkoumání	k1gNnSc6	prozkoumání
Libyjské	libyjský	k2eAgFnSc2d1	Libyjská
pouště	poušť	k1gFnSc2	poušť
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1865	[number]	k4	1865
německý	německý	k2eAgMnSc1d1	německý
cestovatel	cestovatel	k1gMnSc1	cestovatel
Gerhard	Gerhard	k1gMnSc1	Gerhard
Rohlfs	Rohlfs	k1gInSc4	Rohlfs
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
poznatků	poznatek	k1gInPc2	poznatek
z	z	k7c2	z
cesty	cesta	k1gFnSc2	cesta
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
první	první	k4xOgInSc4	první
nepříliš	příliš	k6eNd1	příliš
přesnou	přesný	k2eAgFnSc4d1	přesná
mapu	mapa	k1gFnSc4	mapa
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
vedl	vést	k5eAaImAgMnS	vést
výpravu	výprava	k1gFnSc4	výprava
do	do	k7c2	do
Libyjské	libyjský	k2eAgFnSc2d1	Libyjská
pouště	poušť	k1gFnSc2	poušť
Egypťan	Egypťan	k1gMnSc1	Egypťan
Ahmed	Ahmed	k1gMnSc1	Ahmed
Hassanein	Hassanein	k1gMnSc1	Hassanein
<g/>
.	.	kIx.	.
</s>
<s>
Podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
prozkoumat	prozkoumat	k5eAaPmF	prozkoumat
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
pouště	poušť	k1gFnSc2	poušť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Uwainat	Uwainat	k1gFnSc6	Uwainat
objevil	objevit	k5eAaPmAgInS	objevit
unikátní	unikátní	k2eAgMnPc4d1	unikátní
petroglyfy	petroglyf	k1gMnPc4	petroglyf
<g/>
,	,	kIx,	,
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
po	po	k7c6	po
pasteveckých	pastevecký	k2eAgFnPc6d1	pastevecká
civilizacích	civilizace	k1gFnPc6	civilizace
obývajících	obývající	k2eAgInPc2d1	obývající
tuto	tento	k3xDgFnSc4	tento
část	část	k1gFnSc4	část
Sahary	Sahara	k1gFnSc2	Sahara
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fauna	fauna	k1gFnSc1	fauna
a	a	k8xC	a
flóra	flóra	k1gFnSc1	flóra
==	==	k?	==
</s>
</p>
<p>
<s>
Území	území	k1gNnSc1	území
je	být	k5eAaImIp3nS	být
domovem	domov	k1gInSc7	domov
mnoha	mnoho	k4c2	mnoho
druhů	druh	k1gInPc2	druh
pouštních	pouštní	k2eAgMnPc2d1	pouštní
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
zmije	zmije	k1gFnSc1	zmije
písečná	písečný	k2eAgFnSc1d1	písečná
<g/>
,	,	kIx,	,
štíři	štír	k1gMnPc1	štír
<g/>
,	,	kIx,	,
sup	sup	k1gMnSc1	sup
mrchožravý	mrchožravý	k2eAgMnSc1d1	mrchožravý
<g/>
,	,	kIx,	,
fenek	fenek	k1gMnSc1	fenek
berberský	berberský	k2eAgMnSc1d1	berberský
<g/>
,	,	kIx,	,
šakal	šakal	k1gMnSc1	šakal
obecný	obecný	k2eAgMnSc1d1	obecný
<g/>
,	,	kIx,	,
daman	daman	k1gMnSc1	daman
skalní	skalní	k2eAgMnPc1d1	skalní
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Sahara	Sahara	k1gFnSc1	Sahara
</s>
</p>
<p>
<s>
Alžírská	alžírský	k2eAgFnSc1d1	alžírská
poušť	poušť	k1gFnSc1	poušť
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Libyjská	libyjský	k2eAgFnSc1d1	Libyjská
poušť	poušť	k1gFnSc1	poušť
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
