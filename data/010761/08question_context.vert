<s>
Libyjská	libyjský	k2eAgFnSc1d1	Libyjská
poušť	poušť	k1gFnSc1	poušť
je	být	k5eAaImIp3nS	být
africká	africký	k2eAgFnSc1d1	africká
poušť	poušť	k1gFnSc1	poušť
nacházející	nacházející	k2eAgFnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
Sahary	Sahara	k1gFnSc2	Sahara
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
západního	západní	k2eAgInSc2d1	západní
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
východní	východní	k2eAgFnSc1d1	východní
Libye	Libye	k1gFnSc1	Libye
a	a	k8xC	a
severozápadní	severozápadní	k2eAgFnPc1d1	severozápadní
části	část	k1gFnPc1	část
Súdánu	Súdán	k1gInSc2	Súdán
<g/>
.	.	kIx.	.
</s>
<s>
Zabírá	zabírat	k5eAaImIp3nS	zabírat
území	území	k1gNnSc4	území
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
100	[number]	k4	100
000	[number]	k4	000
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
čtvercového	čtvercový	k2eAgInSc2d1	čtvercový
tvaru	tvar	k1gInSc2	tvar
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
asi	asi	k9	asi
1	[number]	k4	1
100	[number]	k4	100
km	km	kA	km
od	od	k7c2	od
východu	východ	k1gInSc2	východ
na	na	k7c4	na
západ	západ	k1gInSc4	západ
a	a	k8xC	a
1	[number]	k4	1
000	[number]	k4	000
km	km	kA	km
od	od	k7c2	od
severu	sever	k1gInSc2	sever
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Povrch	povrch	k1gInSc1	povrch
pouště	poušť	k1gFnSc2	poušť
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
kamenitý	kamenitý	k2eAgMnSc1d1	kamenitý
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
hamada	hamada	k1gFnSc1	hamada
<g/>
)	)	kIx)	)
s	s	k7c7	s
ostrými	ostrý	k2eAgInPc7d1	ostrý
hřebeny	hřeben	k1gInPc7	hřeben
a	a	k8xC	a
hlubokými	hluboký	k2eAgFnPc7d1	hluboká
proláklinami	proláklina	k1gFnPc7	proláklina
<g/>
.	.	kIx.	.
</s>

