<p>
<s>
Chemické	chemický	k2eAgInPc1d1	chemický
listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
recenzovaným	recenzovaný	k2eAgInSc7d1	recenzovaný
časopisem	časopis	k1gInSc7	časopis
s	s	k7c7	s
měsíční	měsíční	k2eAgFnSc7d1	měsíční
periodicitou	periodicita	k1gFnSc7	periodicita
<g/>
,	,	kIx,	,
vydávaným	vydávaný	k2eAgInSc7d1	vydávaný
Českou	český	k2eAgFnSc7d1	Česká
společností	společnost	k1gFnSc7	společnost
chemickou	chemický	k2eAgFnSc7d1	chemická
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Chemické	chemický	k2eAgInPc1d1	chemický
listy	list	k1gInPc1	list
byly	být	k5eAaImAgInP	být
založené	založený	k2eAgInPc1d1	založený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1876	[number]	k4	1876
Karlem	Karel	k1gMnSc7	Karel
Preisem	Preis	k1gInSc7	Preis
<g/>
,	,	kIx,	,
profesorem	profesor	k1gMnSc7	profesor
pražské	pražský	k2eAgFnSc2d1	Pražská
polytechniky	polytechnika	k1gFnSc2	polytechnika
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
zaměřen	zaměřit	k5eAaPmNgInS	zaměřit
na	na	k7c4	na
publikování	publikování	k1gNnSc4	publikování
prací	práce	k1gFnPc2	práce
z	z	k7c2	z
praktické	praktický	k2eAgFnSc2d1	praktická
a	a	k8xC	a
technické	technický	k2eAgFnSc2d1	technická
chemie	chemie	k1gFnSc2	chemie
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1882	[number]	k4	1882
však	však	k9	však
začaly	začít	k5eAaPmAgInP	začít
převažovat	převažovat	k5eAaImF	převažovat
příspěvky	příspěvek	k1gInPc1	příspěvek
vědeckého	vědecký	k2eAgInSc2d1	vědecký
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
v	v	k7c6	v
Chemických	chemický	k2eAgInPc6d1	chemický
listech	list	k1gInPc6	list
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Heyrovský	Heyrovský	k2eAgMnSc1d1	Heyrovský
(	(	kIx(	(
<g/>
pozdější	pozdní	k2eAgMnSc1d2	pozdější
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
<g/>
,	,	kIx,	,
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
poprvé	poprvé	k6eAd1	poprvé
publikoval	publikovat	k5eAaBmAgMnS	publikovat
základní	základní	k2eAgInSc4d1	základní
princip	princip	k1gInSc4	princip
polarografie	polarografie	k1gFnSc2	polarografie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Šéfredaktoři	šéfredaktor	k1gMnPc5	šéfredaktor
==	==	k?	==
</s>
</p>
<p>
<s>
1876	[number]	k4	1876
<g/>
–	–	k?	–
<g/>
1907	[number]	k4	1907
Karel	Karel	k1gMnSc1	Karel
Preis	Preis	k1gFnSc4	Preis
</s>
</p>
<p>
<s>
1908	[number]	k4	1908
<g/>
–	–	k?	–
<g/>
1926	[number]	k4	1926
Josef	Josef	k1gMnSc1	Josef
Hanuš	Hanuš	k1gMnSc1	Hanuš
</s>
</p>
<p>
<s>
1927	[number]	k4	1927
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
Otakar	Otakar	k1gMnSc1	Otakar
Webr	Webr	k1gMnSc1	Webr
</s>
</p>
<p>
<s>
1946	[number]	k4	1946
<g/>
–	–	k?	–
<g/>
1951	[number]	k4	1951
Josef	Josef	k1gMnSc1	Josef
Koštíř	Koštíř	k1gMnSc1	Koštíř
</s>
</p>
<p>
<s>
1952	[number]	k4	1952
<g/>
–	–	k?	–
<g/>
1954	[number]	k4	1954
Josef	Josef	k1gMnSc1	Josef
Rudinger	Rudinger	k1gMnSc1	Rudinger
</s>
</p>
<p>
<s>
1955	[number]	k4	1955
<g/>
–	–	k?	–
<g/>
1957	[number]	k4	1957
Blahoslav	blahoslavit	k5eAaImRp2nS	blahoslavit
Sedláček	Sedláček	k1gMnSc1	Sedláček
</s>
</p>
<p>
<s>
1958	[number]	k4	1958
Miloš	Miloš	k1gMnSc1	Miloš
Kraus	Kraus	k1gMnSc1	Kraus
</s>
</p>
<p>
<s>
1959	[number]	k4	1959
<g/>
–	–	k?	–
<g/>
1996	[number]	k4	1996
Jiří	Jiří	k1gMnSc2	Jiří
Gut	Gut	k1gMnSc2	Gut
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
Bohumil	Bohumil	k1gMnSc1	Bohumil
Kratochvíl	Kratochvíl	k1gMnSc1	Kratochvíl
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
<g/>
–	–	k?	–
<g/>
2015	[number]	k4	2015
Pavel	Pavla	k1gFnPc2	Pavla
Chuchvalec	chuchvalec	k1gInSc4	chuchvalec
</s>
</p>
<p>
<s>
2016	[number]	k4	2016
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc1	současnost
Bohumil	Bohumil	k1gMnSc1	Bohumil
Kratochvíl	Kratochvíl	k1gMnSc1	Kratochvíl
(	(	kIx(	(
<g/>
podruhé	podruhé	k6eAd1	podruhé
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Zaměření	zaměření	k1gNnSc1	zaměření
==	==	k?	==
</s>
</p>
<p>
<s>
Chemické	chemický	k2eAgInPc1d1	chemický
listy	list	k1gInPc1	list
publikují	publikovat	k5eAaBmIp3nP	publikovat
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
,	,	kIx,	,
slovenštině	slovenština	k1gFnSc6	slovenština
a	a	k8xC	a
angličtině	angličtina	k1gFnSc6	angličtina
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
zvané	zvaný	k2eAgInPc4d1	zvaný
příspěvky	příspěvek	k1gInPc4	příspěvek
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Úvodníky	úvodník	k1gInPc1	úvodník
</s>
</p>
<p>
<s>
Referátové	referátový	k2eAgInPc1d1	referátový
články	článek	k1gInPc1	článek
o	o	k7c6	o
chemii	chemie	k1gFnSc6	chemie
a	a	k8xC	a
příbuzných	příbuzný	k2eAgInPc6d1	příbuzný
oborech	obor	k1gInPc6	obor
</s>
</p>
<p>
<s>
Původní	původní	k2eAgInPc1d1	původní
články	článek	k1gInPc1	článek
o	o	k7c6	o
laboratorní	laboratorní	k2eAgFnSc6d1	laboratorní
technice	technika	k1gFnSc6	technika
a	a	k8xC	a
postupech	postup	k1gInPc6	postup
</s>
</p>
<p>
<s>
Recenze	recenze	k1gFnSc1	recenze
nových	nový	k2eAgFnPc2d1	nová
knih	kniha	k1gFnPc2	kniha
</s>
</p>
<p>
<s>
Diskusní	diskusní	k2eAgInPc4d1	diskusní
příspěvky	příspěvek	k1gInPc4	příspěvek
týkající	týkající	k2eAgInPc4d1	týkající
se	se	k3xPyFc4	se
chemické	chemický	k2eAgFnSc2d1	chemická
komunity	komunita	k1gFnSc2	komunita
</s>
</p>
<p>
<s>
Osobní	osobní	k2eAgFnPc1d1	osobní
zprávy	zpráva	k1gFnPc1	zpráva
<g/>
,	,	kIx,	,
konference	konference	k1gFnPc1	konference
<g/>
,	,	kIx,	,
redakční	redakční	k2eAgFnPc1d1	redakční
poznámky	poznámka	k1gFnPc1	poznámka
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
==	==	k?	==
Abstrakty	abstrakt	k1gInPc1	abstrakt
a	a	k8xC	a
rejstříky	rejstřík	k1gInPc1	rejstřík
==	==	k?	==
</s>
</p>
<p>
<s>
Chemické	chemický	k2eAgInPc1d1	chemický
listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
abstrahovány	abstrahován	k2eAgFnPc1d1	abstrahována
službami	služba	k1gFnPc7	služba
Chemical	Chemical	k1gFnSc1	Chemical
Abstracts	Abstracts	k1gInSc1	Abstracts
<g/>
,	,	kIx,	,
Current	Current	k1gInSc1	Current
Contents	Contents	k1gInSc1	Contents
<g/>
/	/	kIx~	/
<g/>
Physical	Physical	k1gMnSc1	Physical
<g/>
,	,	kIx,	,
Chemical	Chemical	k1gMnSc1	Chemical
&	&	k?	&
Earth	Earth	k1gMnSc1	Earth
Sciences	Sciences	k1gMnSc1	Sciences
<g/>
,	,	kIx,	,
Science	Science	k1gFnSc1	Science
Citation	Citation	k1gInSc1	Citation
Index	index	k1gInSc1	index
<g/>
,	,	kIx,	,
Scopus	Scopus	k1gInSc1	Scopus
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Journal	Journal	k1gFnSc2	Journal
Citation	Citation	k1gInSc1	Citation
reports	reports	k6eAd1	reports
má	mít	k5eAaImIp3nS	mít
časopis	časopis	k1gInSc1	časopis
impakt	impakt	k1gInSc1	impakt
faktor	faktor	k1gInSc1	faktor
0.279	[number]	k4	0.279
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klon	klon	k1gInSc4	klon
==	==	k?	==
</s>
</p>
<p>
<s>
Klonem	klon	k1gInSc7	klon
Chemických	chemický	k2eAgInPc2d1	chemický
listů	list	k1gInPc2	list
je	být	k5eAaImIp3nS	být
sborník	sborník	k1gInSc1	sborník
"	"	kIx"	"
<g/>
Czech	Czech	k1gInSc1	Czech
Chemical	Chemical	k1gFnSc2	Chemical
Society	societa	k1gFnSc2	societa
Symposium	symposium	k1gNnSc1	symposium
Series	Series	k1gInSc1	Series
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
publikuje	publikovat	k5eAaBmIp3nS	publikovat
abstrakta	abstraktum	k1gNnPc4	abstraktum
a	a	k8xC	a
plné	plný	k2eAgInPc4d1	plný
texty	text	k1gInPc4	text
příspěvků	příspěvek	k1gInPc2	příspěvek
z	z	k7c2	z
konferencí	konference	k1gFnPc2	konference
a	a	k8xC	a
sympózií	sympózium	k1gNnPc2	sympózium
pořádaných	pořádaný	k2eAgFnPc2d1	pořádaná
Českou	český	k2eAgFnSc7d1	Česká
společností	společnost	k1gFnSc7	společnost
chemickou	chemický	k2eAgFnSc7d1	chemická
a	a	k8xC	a
jinými	jiný	k2eAgFnPc7d1	jiná
odbornými	odborný	k2eAgFnPc7d1	odborná
společnostmi	společnost	k1gFnPc7	společnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
