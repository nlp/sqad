<s>
Jak	jak	k6eAd1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
prvek	prvek	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
slitiny	slitina	k1gFnSc2
bronzu	bronz	k1gInSc2
a	a	k8xC
má	mít	k5eAaImIp3nS
značku	značka	k1gFnSc4
Sn	Sn	kA
<g/>
?	?	kIx.
</s>