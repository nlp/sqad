<p>
<s>
Chester	Chester	k1gMnSc1	Chester
Alan	Alan	k1gMnSc1	Alan
Arthur	Arthur	k1gMnSc1	Arthur
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1829	[number]	k4	1829
–	–	k?	–
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
21	[number]	k4	21
<g/>
.	.	kIx.	.
prezident	prezident	k1gMnSc1	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
v	v	k7c6	v
letech	let	k1gInPc6	let
1881	[number]	k4	1881
<g/>
–	–	k?	–
<g/>
1885	[number]	k4	1885
<g/>
.	.	kIx.	.
</s>
<s>
Ujal	ujmout	k5eAaPmAgInS	ujmout
se	se	k3xPyFc4	se
úřadu	úřad	k1gInSc2	úřad
po	po	k7c6	po
atentátu	atentát	k1gInSc6	atentát
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
Jamese	Jamese	k1gFnSc2	Jamese
A.	A.	kA	A.
Garfielda	Garfield	k1gMnSc2	Garfield
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vláda	vláda	k1gFnSc1	vláda
Chestera	Chester	k1gMnSc2	Chester
Arthura	Arthur	k1gMnSc2	Arthur
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Chester	Chestra	k1gFnPc2	Chestra
A.	A.	kA	A.
Arthur	Arthur	k1gMnSc1	Arthur
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc4	Commons
</s>
</p>
