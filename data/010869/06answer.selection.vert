<s>
Sesterskou	sesterský	k2eAgFnSc7d1	sesterská
lodí	loď	k1gFnSc7	loď
Bismarcka	Bismarcko	k1gNnSc2	Bismarcko
byla	být	k5eAaImAgFnS	být
bitevní	bitevní	k2eAgFnSc1d1	bitevní
loď	loď	k1gFnSc1	loď
Tirpitz	Tirpitza	k1gFnPc2	Tirpitza
o	o	k7c6	o
výtlaku	výtlak	k1gInSc6	výtlak
asi	asi	k9	asi
50	[number]	k4	50
000	[number]	k4	000
tun	tuna	k1gFnPc2	tuna
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc1d3	veliký
německá	německý	k2eAgFnSc1d1	německá
bitevní	bitevní	k2eAgFnSc1d1	bitevní
loď	loď	k1gFnSc1	loď
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
