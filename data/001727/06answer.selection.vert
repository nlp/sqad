<s>
Progresivní	progresivní	k2eAgInSc1d1	progresivní
rock	rock	k1gInSc1	rock
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
zaměňuje	zaměňovat	k5eAaImIp3nS	zaměňovat
s	s	k7c7	s
art	art	k?	art
rockem	rock	k1gInSc7	rock
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
pokusem	pokus	k1gInSc7	pokus
odpoutat	odpoutat	k5eAaPmF	odpoutat
se	se	k3xPyFc4	se
od	od	k7c2	od
zavedených	zavedený	k2eAgInPc2d1	zavedený
hudebních	hudební	k2eAgInPc2d1	hudební
zvyků	zvyk	k1gInPc2	zvyk
experimentováním	experimentování	k1gNnSc7	experimentování
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
nástroji	nástroj	k1gInPc7	nástroj
<g/>
,	,	kIx,	,
typy	typ	k1gInPc1	typ
písní	píseň	k1gFnPc2	píseň
a	a	k8xC	a
podobami	podoba	k1gFnPc7	podoba
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
