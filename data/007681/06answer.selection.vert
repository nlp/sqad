<s>
Avatar	Avatar	k1gInSc1	Avatar
v	v	k7c6	v
počítačové	počítačový	k2eAgFnSc6d1	počítačová
hře	hra	k1gFnSc6	hra
je	být	k5eAaImIp3nS	být
virtuální	virtuální	k2eAgFnSc1d1	virtuální
postava	postava	k1gFnSc1	postava
ovládaná	ovládaný	k2eAgFnSc1d1	ovládaná
hráčem	hráč	k1gMnSc7	hráč
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
s	s	k7c7	s
avatarem	avatar	k1gInSc7	avatar
v	v	k7c6	v
jistém	jistý	k2eAgInSc6d1	jistý
smyslu	smysl	k1gInSc6	smysl
slova	slovo	k1gNnSc2	slovo
ztotožňuje	ztotožňovat	k5eAaImIp3nS	ztotožňovat
a	a	k8xC	a
skrze	skrze	k?	skrze
něj	on	k3xPp3gMnSc4	on
jedná	jednat	k5eAaImIp3nS	jednat
ve	v	k7c6	v
virtuálním	virtuální	k2eAgInSc6d1	virtuální
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
