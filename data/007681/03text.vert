<s>
Avatar	Avatar	k1gInSc1	Avatar
(	(	kIx(	(
<g/>
z	z	k7c2	z
anglického	anglický	k2eAgInSc2d1	anglický
přepisu	přepis	k1gInSc2	přepis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
především	především	k9	především
vizuální	vizuální	k2eAgFnSc1d1	vizuální
<g/>
)	)	kIx)	)
reprezentace	reprezentace	k1gFnSc1	reprezentace
uživatele	uživatel	k1gMnSc2	uživatel
ve	v	k7c6	v
virtuální	virtuální	k2eAgFnSc6d1	virtuální
realitě	realita	k1gFnSc6	realita
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
reprezentací	reprezentace	k1gFnSc7	reprezentace
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
trojrozměrný	trojrozměrný	k2eAgInSc1d1	trojrozměrný
model	model	k1gInSc1	model
<g/>
,	,	kIx,	,
dvourozměrný	dvourozměrný	k2eAgInSc1d1	dvourozměrný
obraz	obraz	k1gInSc1	obraz
nebo	nebo	k8xC	nebo
třeba	třeba	k6eAd1	třeba
jediný	jediný	k2eAgInSc1d1	jediný
znak	znak	k1gInSc1	znak
–	–	k?	–
v	v	k7c6	v
textových	textový	k2eAgInPc6d1	textový
MUDech	MUDy	k1gInPc6	MUDy
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
často	často	k6eAd1	často
zavináč	zavináč	k1gInSc1	zavináč
<g/>
.	.	kIx.	.
</s>
<s>
Avatar	Avatar	k1gInSc1	Avatar
v	v	k7c6	v
počítačové	počítačový	k2eAgFnSc6d1	počítačová
hře	hra	k1gFnSc6	hra
je	být	k5eAaImIp3nS	být
virtuální	virtuální	k2eAgFnSc1d1	virtuální
postava	postava	k1gFnSc1	postava
ovládaná	ovládaný	k2eAgFnSc1d1	ovládaná
hráčem	hráč	k1gMnSc7	hráč
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
s	s	k7c7	s
avatarem	avatar	k1gInSc7	avatar
v	v	k7c6	v
jistém	jistý	k2eAgInSc6d1	jistý
smyslu	smysl	k1gInSc6	smysl
slova	slovo	k1gNnSc2	slovo
ztotožňuje	ztotožňovat	k5eAaImIp3nS	ztotožňovat
a	a	k8xC	a
skrze	skrze	k?	skrze
něj	on	k3xPp3gMnSc4	on
jedná	jednat	k5eAaImIp3nS	jednat
ve	v	k7c6	v
virtuálním	virtuální	k2eAgInSc6d1	virtuální
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pojem	pojem	k1gInSc1	pojem
je	být	k5eAaImIp3nS	být
užíván	užívat	k5eAaImNgInS	užívat
zejména	zejména	k9	zejména
v	v	k7c4	v
online	onlinout	k5eAaPmIp3nS	onlinout
hrách	hrách	k1gInSc1	hrách
na	na	k7c4	na
hrdiny	hrdina	k1gMnPc4	hrdina
(	(	kIx(	(
<g/>
MUD	MUD	kA	MUD
<g/>
,	,	kIx,	,
MMORPG	MMORPG	kA	MMORPG
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Avatary	Avatar	k1gInPc1	Avatar
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
v	v	k7c6	v
neherních	herní	k2eNgInPc6d1	neherní
světech	svět	k1gInPc6	svět
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
pro	pro	k7c4	pro
rozšíření	rozšíření	k1gNnSc4	rozšíření
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
diskusních	diskusní	k2eAgInPc6d1	diskusní
fórech	fór	k1gInPc6	fór
(	(	kIx(	(
<g/>
např.	např.	kA	např.
na	na	k7c6	na
serverech	server	k1gInPc6	server
založených	založený	k2eAgFnPc2d1	založená
na	na	k7c6	na
programu	program	k1gInSc6	program
phpBB	phpBB	k?	phpBB
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pojem	pojem	k1gInSc1	pojem
avatar	avatara	k1gFnPc2	avatara
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
ikonku	ikonka	k1gFnSc4	ikonka
uživatele	uživatel	k1gMnSc2	uživatel
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
u	u	k7c2	u
jeho	jeho	k3xOp3gInPc2	jeho
příspěvků	příspěvek	k1gInPc2	příspěvek
<g/>
.	.	kIx.	.
</s>
<s>
Programy	program	k1gInPc1	program
virtuální	virtuální	k2eAgFnSc2d1	virtuální
reality	realita	k1gFnSc2	realita
často	často	k6eAd1	často
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
trojrozměrný	trojrozměrný	k2eAgInSc4d1	trojrozměrný
model	model	k1gInSc4	model
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
avatarem	avatar	k1gInSc7	avatar
uživatele	uživatel	k1gMnSc2	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
hinduistického	hinduistický	k2eAgInSc2d1	hinduistický
pojmu	pojem	k1gInSc2	pojem
avatár	avatár	k1gInSc4	avatár
<g/>
,	,	kIx,	,
znamenajícího	znamenající	k2eAgNnSc2d1	znamenající
pozemské	pozemský	k2eAgNnSc1d1	pozemské
vtělení	vtělení	k1gNnSc1	vtělení
duchovní	duchovní	k2eAgFnSc2d1	duchovní
bytosti	bytost	k1gFnSc2	bytost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počítačové	počítačový	k2eAgFnSc6d1	počítačová
sféře	sféra	k1gFnSc6	sféra
se	se	k3xPyFc4	se
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgNnPc2	první
použití	použití	k1gNnPc2	použití
pojmu	pojem	k1gInSc2	pojem
objevuje	objevovat	k5eAaImIp3nS	objevovat
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
v	v	k7c6	v
počítačové	počítačový	k2eAgFnSc6d1	počítačová
hře	hra	k1gFnSc6	hra
Ultima	ultimo	k1gNnSc2	ultimo
–	–	k?	–
stát	stát	k1gInSc1	stát
se	se	k3xPyFc4	se
avatárem	avatár	k1gInSc7	avatár
bylo	být	k5eAaImAgNnS	být
cílem	cíl	k1gInSc7	cíl
Ultimy	ultimo	k1gNnPc7	ultimo
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějších	pozdní	k2eAgFnPc6d2	pozdější
hrách	hra	k1gFnPc6	hra
se	se	k3xPyFc4	se
již	již	k6eAd1	již
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
avatarem	avatar	k1gInSc7	avatar
jste	být	k5eAaImIp2nP	být
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
termín	termín	k1gInSc1	termín
avatar	avatar	k1gInSc1	avatar
použit	použit	k2eAgInSc1d1	použit
ve	v	k7c6	v
stolní	stolní	k2eAgFnSc6d1	stolní
hře	hra	k1gFnSc6	hra
na	na	k7c4	na
hrdiny	hrdina	k1gMnPc4	hrdina
Shadowrun	Shadowruna	k1gFnPc2	Shadowruna
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
v	v	k7c6	v
online	onlinout	k5eAaPmIp3nS	onlinout
hře	hra	k1gFnSc3	hra
Habitat	Habitat	k1gFnSc2	Habitat
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
rozšíření	rozšíření	k1gNnSc4	rozšíření
termínu	termín	k1gInSc2	termín
bylo	být	k5eAaImAgNnS	být
významné	významný	k2eAgNnSc1d1	významné
jeho	jeho	k3xOp3gNnSc4	jeho
použití	použití	k1gNnSc4	použití
v	v	k7c6	v
kyberpunkovém	kyberpunkový	k2eAgInSc6d1	kyberpunkový
románu	román	k1gInSc6	román
Neala	Neala	k1gFnSc1	Neala
Stephensona	Stephensona	k1gFnSc1	Stephensona
Sníh	sníh	k1gInSc1	sníh
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Snow	Snow	k1gMnSc1	Snow
Crash	Crash	k1gMnSc1	Crash
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
virtuální	virtuální	k2eAgFnSc2d1	virtuální
simulace	simulace	k1gFnSc2	simulace
lidského	lidský	k2eAgNnSc2d1	lidské
těla	tělo	k1gNnSc2	tělo
ve	v	k7c6	v
virtuální	virtuální	k2eAgFnSc6d1	virtuální
realitě	realita	k1gFnSc6	realita
Metaverse	Metaverse	k1gFnSc2	Metaverse
<g/>
.	.	kIx.	.
</s>
<s>
Sociální	sociální	k2eAgNnSc1d1	sociální
postavení	postavení	k1gNnSc1	postavení
v	v	k7c6	v
Metaversu	Metavers	k1gInSc6	Metavers
bylo	být	k5eAaImAgNnS	být
často	často	k6eAd1	často
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
kvalitách	kvalita	k1gFnPc6	kvalita
avatara	avatar	k1gMnSc2	avatar
<g/>
:	:	kIx,	:
detailní	detailní	k2eAgInSc1d1	detailní
avatar	avatar	k1gInSc1	avatar
znamenal	znamenat	k5eAaImAgInS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
uživatel	uživatel	k1gMnSc1	uživatel
je	být	k5eAaImIp3nS	být
hacker	hacker	k1gMnSc1	hacker
a	a	k8xC	a
programátor	programátor	k1gMnSc1	programátor
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
méně	málo	k6eAd2	málo
talentovaní	talentovaný	k2eAgMnPc1d1	talentovaný
si	se	k3xPyFc3	se
museli	muset	k5eAaImAgMnP	muset
kupovat	kupovat	k5eAaImF	kupovat
konfekční	konfekční	k2eAgInPc4d1	konfekční
modely	model	k1gInPc4	model
<g/>
.	.	kIx.	.
</s>
<s>
Stephenson	Stephenson	k1gMnSc1	Stephenson
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
termín	termín	k1gInSc1	termín
avatár	avatár	k1gMnSc1	avatár
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
nezávisle	závisle	k6eNd1	závisle
a	a	k8xC	a
až	až	k6eAd1	až
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jak	jak	k8xC	jak
tento	tento	k3xDgInSc4	tento
termín	termín	k1gInSc4	termín
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
některé	některý	k3yIgInPc1	některý
další	další	k2eAgInPc1d1	další
prvky	prvek	k1gInPc1	prvek
jeho	on	k3xPp3gInSc2	on
románu	román	k1gInSc2	román
se	se	k3xPyFc4	se
už	už	k6eAd1	už
objevily	objevit	k5eAaPmAgFnP	objevit
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
Habitat	Habitat	k1gFnSc2	Habitat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
další	další	k2eAgFnSc6d1	další
kyberpunkové	kyberpunkový	k2eAgFnSc6d1	kyberpunková
literatuře	literatura	k1gFnSc6	literatura
se	se	k3xPyFc4	se
termín	termín	k1gInSc1	termín
avatar	avatara	k1gFnPc2	avatara
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
elektronicky	elektronicky	k6eAd1	elektronicky
zaznamenaného	zaznamenaný	k2eAgNnSc2d1	zaznamenané
a	a	k8xC	a
interpretovaného	interpretovaný	k2eAgNnSc2d1	interpretované
vědomí	vědomí	k1gNnSc2	vědomí
konkrétního	konkrétní	k2eAgMnSc2d1	konkrétní
člověka	člověk	k1gMnSc2	člověk
bez	bez	k7c2	bez
jeho	jeho	k3xOp3gFnSc2	jeho
fyzické	fyzický	k2eAgFnSc2d1	fyzická
podoby	podoba	k1gFnSc2	podoba
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
zatímco	zatímco	k8xS	zatímco
jeho	jeho	k3xOp3gNnSc1	jeho
tělo	tělo	k1gNnSc1	tělo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
avatár	avatár	k1gInSc1	avatár
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
schopen	schopen	k2eAgInSc1d1	schopen
komunikovat	komunikovat	k5eAaImF	komunikovat
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Avatar	Avatar	k1gInSc1	Avatar
je	být	k5eAaImIp3nS	být
také	také	k9	také
název	název	k1gInSc1	název
akčního	akční	k2eAgNnSc2d1	akční
sci-fi	scii	k1gNnSc2	sci-fi
filmu	film	k1gInSc2	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
režiséra	režisér	k1gMnSc2	režisér
Jamese	Jamese	k1gFnSc1	Jamese
Camerona	Camerona	k1gFnSc1	Camerona
<g/>
.	.	kIx.	.
</s>
<s>
Userbar	Userbar	k1gInSc1	Userbar
Virtuální	virtuální	k2eAgFnSc1d1	virtuální
realita	realita	k1gFnSc1	realita
</s>
