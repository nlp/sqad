<p>
<s>
Tajuplný	tajuplný	k2eAgInSc1d1	tajuplný
ostrov	ostrov	k1gInSc1	ostrov
(	(	kIx(	(
<g/>
1875	[number]	k4	1875
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
île	île	k?	île
mystérieuse	mystérieuse	k1gFnSc1	mystérieuse
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgMnPc2d3	nejznámější
románů	román	k1gInPc2	román
francouzského	francouzský	k2eAgMnSc2d1	francouzský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Julesa	Julesa	k1gFnSc1	Julesa
Verna	Verna	k1gFnSc1	Verna
z	z	k7c2	z
jeho	on	k3xPp3gInSc2	on
cyklu	cyklus	k1gInSc2	cyklus
Podivuhodné	podivuhodný	k2eAgFnSc2d1	podivuhodná
cesty	cesta	k1gFnSc2	cesta
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
Voyages	Voyages	k1gMnSc1	Voyages
extraordinaires	extraordinaires	k1gMnSc1	extraordinaires
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
závěrečný	závěrečný	k2eAgInSc4d1	závěrečný
díl	díl	k1gInSc4	díl
volné	volný	k2eAgFnSc2d1	volná
trilogie	trilogie	k1gFnSc2	trilogie
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yIgFnSc2	který
ještě	ještě	k6eAd1	ještě
patří	patřit	k5eAaImIp3nS	patřit
Děti	dítě	k1gFnPc4	dítě
kapitána	kapitán	k1gMnSc2	kapitán
Granta	Grant	k1gMnSc2	Grant
(	(	kIx(	(
<g/>
1867	[number]	k4	1867
<g/>
-	-	kIx~	-
<g/>
1868	[number]	k4	1868
<g/>
,	,	kIx,	,
Les	les	k1gInSc1	les
enfants	enfants	k1gInSc1	enfants
du	du	k?	du
capitaine	capitainout	k5eAaPmIp3nS	capitainout
Grant	grant	k1gInSc1	grant
<g/>
)	)	kIx)	)
a	a	k8xC	a
Dvacet	dvacet	k4xCc4	dvacet
tisíc	tisíc	k4xCgInPc2	tisíc
mil	míle	k1gFnPc2	míle
pod	pod	k7c7	pod
mořem	moře	k1gNnSc7	moře
(	(	kIx(	(
<g/>
1869	[number]	k4	1869
<g/>
-	-	kIx~	-
<g/>
1870	[number]	k4	1870
<g/>
,	,	kIx,	,
Vingt	Vingt	k2eAgInSc1d1	Vingt
mille	mille	k1gInSc1	mille
lieues	lieuesa	k1gFnPc2	lieuesa
sous	sous	k6eAd1	sous
les	les	k1gInSc1	les
mers	mersa	k1gFnPc2	mersa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
předcházejícím	předcházející	k2eAgInSc7d1	předcházející
díly	dílo	k1gNnPc7	dílo
je	být	k5eAaImIp3nS	být
spjat	spjat	k2eAgInSc1d1	spjat
některými	některý	k3yIgFnPc7	některý
postavami	postava	k1gFnPc7	postava
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
i	i	k9	i
místem	místo	k1gNnSc7	místo
děje	děj	k1gInSc2	děj
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
díly	díl	k1gInPc7	díl
však	však	k9	však
panuje	panovat	k5eAaImIp3nS	panovat
časový	časový	k2eAgInSc4d1	časový
nesoulad	nesoulad	k1gInSc4	nesoulad
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
románu	román	k1gInSc2	román
==	==	k?	==
</s>
</p>
<p>
<s>
Za	za	k7c2	za
americké	americký	k2eAgFnSc2d1	americká
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
"	"	kIx"	"
<g/>
Severu	sever	k1gInSc2	sever
proti	proti	k7c3	proti
Jihu	jih	k1gInSc3	jih
<g/>
"	"	kIx"	"
uprchne	uprchnout	k5eAaPmIp3nS	uprchnout
balónem	balón	k1gInSc7	balón
z	z	k7c2	z
jižanského	jižanský	k2eAgNnSc2d1	jižanské
zajetí	zajetí	k1gNnSc2	zajetí
skupina	skupina	k1gFnSc1	skupina
pěti	pět	k4xCc2	pět
seveřanů	seveřan	k1gMnPc2	seveřan
vedená	vedený	k2eAgFnSc1d1	vedená
Cyrusem	Cyrus	k1gMnSc7	Cyrus
Smithem	Smith	k1gInSc7	Smith
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
balón	balón	k1gInSc1	balón
je	být	k5eAaImIp3nS	být
větrnou	větrný	k2eAgFnSc7d1	větrná
smrští	smršť	k1gFnSc7	smršť
zahnán	zahnat	k5eAaPmNgMnS	zahnat
nad	nad	k7c4	nad
moře	moře	k1gNnSc4	moře
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
zničen	zničen	k2eAgMnSc1d1	zničen
<g/>
,	,	kIx,	,
a	a	k8xC	a
uprchlíci	uprchlík	k1gMnPc1	uprchlík
se	se	k3xPyFc4	se
zachrání	zachránit	k5eAaPmIp3nP	zachránit
na	na	k7c6	na
pustém	pustý	k2eAgInSc6d1	pustý
ostrově	ostrov	k1gInSc6	ostrov
v	v	k7c6	v
jižním	jižní	k2eAgInSc6d1	jižní
Tichém	tichý	k2eAgInSc6d1	tichý
oceánu	oceán	k1gInSc6	oceán
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
km	km	kA	km
od	od	k7c2	od
nejbližšího	blízký	k2eAgNnSc2d3	nejbližší
obydleného	obydlený	k2eAgNnSc2d1	obydlené
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
ostrovů	ostrov	k1gInPc2	ostrov
Polynésie	Polynésie	k1gFnSc2	Polynésie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
počest	počest	k1gFnSc4	počest
amerického	americký	k2eAgMnSc2d1	americký
prezidenta	prezident	k1gMnSc2	prezident
a	a	k8xC	a
bojovníka	bojovník	k1gMnSc2	bojovník
proti	proti	k7c3	proti
otrokářství	otrokářství	k1gNnSc3	otrokářství
Abrahama	Abraham	k1gMnSc2	Abraham
Lincolna	Lincoln	k1gMnSc2	Lincoln
jej	on	k3xPp3gInSc2	on
nazvou	nazvat	k5eAaPmIp3nP	nazvat
Lincolnův	Lincolnův	k2eAgInSc4d1	Lincolnův
ostrov	ostrov	k1gInSc4	ostrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
zajištění	zajištění	k1gNnSc6	zajištění
holého	holý	k2eAgInSc2d1	holý
života	život	k1gInSc2	život
využijí	využít	k5eAaPmIp3nP	využít
trosečníci	trosečník	k1gMnPc1	trosečník
všech	všecek	k3xTgFnPc2	všecek
svých	svůj	k3xOyFgFnPc2	svůj
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
a	a	k8xC	a
technických	technický	k2eAgFnPc2d1	technická
znalostí	znalost	k1gFnPc2	znalost
a	a	k8xC	a
přírodních	přírodní	k2eAgFnPc2d1	přírodní
podmínek	podmínka	k1gFnPc2	podmínka
ostrova	ostrov	k1gInSc2	ostrov
k	k	k7c3	k
vybudování	vybudování	k1gNnSc3	vybudování
pohodlného	pohodlný	k2eAgNnSc2d1	pohodlné
sídla	sídlo	k1gNnSc2	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
trosečníků	trosečník	k1gMnPc2	trosečník
jsou	být	k5eAaImIp3nP	být
zastoupena	zastoupen	k2eAgNnPc4d1	zastoupeno
různá	různý	k2eAgNnPc4d1	různé
povolání	povolání	k1gNnPc4	povolání
(	(	kIx(	(
<g/>
inženýr	inženýr	k1gMnSc1	inženýr
<g/>
,	,	kIx,	,
lodní	lodní	k2eAgMnSc1d1	lodní
tesař	tesař	k1gMnSc1	tesař
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
<g/>
,	,	kIx,	,
přírodopisec	přírodopisec	k1gMnSc1	přírodopisec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
vedle	vedle	k7c2	vedle
nedospělého	nedospělý	k1gMnSc2	nedospělý
chlapce	chlapec	k1gMnSc2	chlapec
také	také	k9	také
černoch	černoch	k1gMnSc1	černoch
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
dokonce	dokonce	k9	dokonce
i	i	k9	i
bývalý	bývalý	k2eAgMnSc1d1	bývalý
trestanec	trestanec	k1gMnSc1	trestanec
Ayrton	Ayrton	k1gInSc1	Ayrton
(	(	kIx(	(
<g/>
postava	postava	k1gFnSc1	postava
z	z	k7c2	z
Vernova	Vernův	k2eAgInSc2d1	Vernův
románu	román	k1gInSc2	román
Děti	dítě	k1gFnPc4	dítě
kapitána	kapitán	k1gMnSc2	kapitán
Granta	Grant	k1gMnSc2	Grant
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
trosečníci	trosečník	k1gMnPc1	trosečník
našli	najít	k5eAaPmAgMnP	najít
na	na	k7c6	na
blízkém	blízký	k2eAgInSc6d1	blízký
ostrově	ostrov	k1gInSc6	ostrov
Tabor	Tabor	k1gInSc1	Tabor
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
všichni	všechen	k3xTgMnPc1	všechen
spojí	spojit	k5eAaPmIp3nP	spojit
síly	síla	k1gFnPc4	síla
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
přemohli	přemoct	k5eAaPmAgMnP	přemoct
svízele	svízel	k1gInPc4	svízel
svého	svůj	k3xOyFgNnSc2	svůj
postavení	postavení	k1gNnSc2	postavení
a	a	k8xC	a
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
přírodu	příroda	k1gFnSc4	příroda
ve	v	k7c4	v
svůj	svůj	k3xOyFgInSc4	svůj
prospěch	prospěch	k1gInSc4	prospěch
<g/>
.	.	kIx.	.
</s>
<s>
Verne	Vernout	k5eAaPmIp3nS	Vernout
tím	ten	k3xDgNnSc7	ten
oslavuje	oslavovat	k5eAaImIp3nS	oslavovat
lidskou	lidský	k2eAgFnSc4d1	lidská
solidaritu	solidarita	k1gFnSc4	solidarita
<g/>
,	,	kIx,	,
sílu	síla	k1gFnSc4	síla
kolektivu	kolektiv	k1gInSc2	kolektiv
a	a	k8xC	a
přátelství	přátelství	k1gNnSc2	přátelství
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
románem	román	k1gInSc7	román
Dvacet	dvacet	k4xCc4	dvacet
tisíc	tisíc	k4xCgInPc2	tisíc
mil	míle	k1gFnPc2	míle
pod	pod	k7c7	pod
mořem	moře	k1gNnSc7	moře
spojuje	spojovat	k5eAaImIp3nS	spojovat
knihu	kniha	k1gFnSc4	kniha
Tajuplný	tajuplný	k2eAgInSc4d1	tajuplný
ostrov	ostrov	k1gInSc4	ostrov
postava	postava	k1gFnSc1	postava
umírajícího	umírající	k1gMnSc2	umírající
kapitána	kapitán	k1gMnSc2	kapitán
Nema	Nemus	k1gMnSc2	Nemus
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
ponorka	ponorka	k1gFnSc1	ponorka
Nautilus	Nautilus	k1gMnSc1	Nautilus
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
základnu	základna	k1gFnSc4	základna
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
z	z	k7c2	z
klasické	klasický	k2eAgFnSc2d1	klasická
"	"	kIx"	"
<g/>
robinzonády	robinzonáda	k1gFnSc2	robinzonáda
<g/>
"	"	kIx"	"
stal	stát	k5eAaPmAgInS	stát
vědeckofantastický	vědeckofantastický	k2eAgInSc4d1	vědeckofantastický
román	román	k1gInSc4	román
<g/>
.	.	kIx.	.
</s>
<s>
Kapitán	kapitán	k1gMnSc1	kapitán
Nemo	Nemo	k1gMnSc1	Nemo
nejprve	nejprve	k6eAd1	nejprve
tajně	tajně	k6eAd1	tajně
trosečníkům	trosečník	k1gMnPc3	trosečník
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
a	a	k8xC	a
torpédem	torpédo	k1gNnSc7	torpédo
zničí	zničit	k5eAaPmIp3nS	zničit
loď	loď	k1gFnSc1	loď
pirátů	pirát	k1gMnPc2	pirát
<g/>
,	,	kIx,	,
nemůže	moct	k5eNaImIp3nS	moct
však	však	k9	však
odvrátit	odvrátit	k5eAaPmF	odvrátit
sopečnou	sopečný	k2eAgFnSc4d1	sopečná
katastrofu	katastrofa	k1gFnSc4	katastrofa
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
ostrov	ostrov	k1gInSc4	ostrov
zničí	zničit	k5eAaPmIp3nS	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
objevení	objevení	k1gNnSc6	objevení
Nemo	Nemo	k6eAd1	Nemo
trosečníky	trosečník	k1gMnPc4	trosečník
seznámí	seznámit	k5eAaPmIp3nS	seznámit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
minulostí	minulost	k1gFnSc7	minulost
<g/>
:	:	kIx,	:
Je	být	k5eAaImIp3nS	být
indický	indický	k2eAgMnSc1d1	indický
kníže	kníže	k1gMnSc1	kníže
Dakkar	Dakkar	k1gMnSc1	Dakkar
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
duší	duše	k1gFnSc7	duše
povstání	povstání	k1gNnSc2	povstání
sipáhijů	sipáhij	k1gInPc2	sipáhij
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1857	[number]	k4	1857
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozdrcení	rozdrcení	k1gNnSc6	rozdrcení
povstání	povstání	k1gNnSc2	povstání
s	s	k7c7	s
dvaceti	dvacet	k4xCc2	dvacet
nejvěrnějšími	věrný	k2eAgFnPc7d3	nejvěrnější
vybudoval	vybudovat	k5eAaPmAgInS	vybudovat
Nautilus	Nautilus	k1gMnSc1	Nautilus
a	a	k8xC	a
přijal	přijmout	k5eAaPmAgMnS	přijmout
bojové	bojový	k2eAgNnSc4d1	bojové
jméno	jméno	k1gNnSc4	jméno
Nemo	Nemo	k6eAd1	Nemo
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
nikdo	nikdo	k3yNnSc1	nikdo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
postupně	postupně	k6eAd1	postupně
umírali	umírat	k5eAaImAgMnP	umírat
<g/>
.	.	kIx.	.
</s>
<s>
Osamělý	osamělý	k2eAgMnSc1d1	osamělý
Nemo	Nemo	k1gMnSc1	Nemo
se	se	k3xPyFc4	se
uchýlil	uchýlit	k5eAaPmAgMnS	uchýlit
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
přístavů	přístav	k1gInPc2	přístav
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
tajně	tajně	k6eAd1	tajně
vybudoval	vybudovat	k5eAaPmAgInS	vybudovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
šest	šest	k4xCc1	šest
let	léto	k1gNnPc2	léto
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
pobýval	pobývat	k5eAaImAgMnS	pobývat
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
nesetkal	setkat	k5eNaPmAgMnS	setkat
s	s	k7c7	s
Cyrusem	Cyrus	k1gInSc7	Cyrus
Smithem	Smith	k1gInSc7	Smith
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc7	jeho
přáteli	přítel	k1gMnPc7	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
požádá	požádat	k5eAaPmIp3nS	požádat
trosečníky	trosečník	k1gMnPc4	trosečník
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
Nautilus	Nautilus	k1gMnSc1	Nautilus
potopili	potopit	k5eAaPmAgMnP	potopit
otevřením	otevření	k1gNnSc7	otevření
kohoutů	kohout	k1gInPc2	kohout
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
naplnit	naplnit	k5eAaPmF	naplnit
ho	on	k3xPp3gNnSc4	on
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
potopení	potopení	k1gNnSc6	potopení
Nautilu	nautilus	k1gInSc2	nautilus
je	být	k5eAaImIp3nS	být
ostrov	ostrov	k1gInSc1	ostrov
zničen	zničen	k2eAgInSc1d1	zničen
výbuchem	výbuch	k1gInSc7	výbuch
sopky	sopka	k1gFnSc2	sopka
<g/>
.	.	kIx.	.
</s>
<s>
Trosečníci	trosečník	k1gMnPc1	trosečník
se	se	k3xPyFc4	se
zachrání	zachránit	k5eAaPmIp3nP	zachránit
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
posledním	poslední	k2eAgNnSc6d1	poslední
skalisku	skalisko	k1gNnSc6	skalisko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
ještě	ještě	k9	ještě
vyčnívá	vyčnívat	k5eAaImIp3nS	vyčnívat
nad	nad	k7c7	nad
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
a	a	k8xC	a
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
hladem	hlad	k1gInSc7	hlad
a	a	k8xC	a
žízní	žízeň	k1gFnSc7	žízeň
je	být	k5eAaImIp3nS	být
zachrání	zachránit	k5eAaPmIp3nS	zachránit
jachta	jachta	k1gFnSc1	jachta
Duncan	Duncany	k1gInPc2	Duncany
<g/>
,	,	kIx,	,
připlouvající	připlouvající	k2eAgFnSc7d1	připlouvající
pro	pro	k7c4	pro
Ayrtona	Ayrton	k1gMnSc4	Ayrton
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filmové	filmový	k2eAgFnSc2d1	filmová
adaptace	adaptace	k1gFnSc2	adaptace
==	==	k?	==
</s>
</p>
<p>
<s>
Román	román	k1gInSc1	román
byl	být	k5eAaImAgInS	být
několikrát	několikrát	k6eAd1	několikrát
zfilmován	zfilmovat	k5eAaPmNgInS	zfilmovat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Tajuplný	tajuplný	k2eAgInSc1d1	tajuplný
ostrov	ostrov	k1gInSc1	ostrov
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Mysterious	Mysterious	k1gMnSc1	Mysterious
Island	Island	k1gInSc1	Island
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Lucien	Lucina	k1gFnPc2	Lucina
Hubbard	Hubbard	k1gMnSc1	Hubbard
<g/>
,	,	kIx,	,
Benjamin	Benjamin	k1gMnSc1	Benjamin
Christensen	Christensen	k1gInSc1	Christensen
a	a	k8xC	a
Maurice	Maurika	k1gFnSc3	Maurika
Tourneur	Tourneura	k1gFnPc2	Tourneura
<g/>
,	,	kIx,	,
němý	němý	k2eAgInSc1d1	němý
film	film	k1gInSc1	film
</s>
</p>
<p>
<s>
Tajuplný	tajuplný	k2eAgInSc1d1	tajuplný
ostrov	ostrov	k1gInSc1	ostrov
(	(	kIx(	(
<g/>
Т	Т	k?	Т
о	о	k?	о
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Eduard	Eduard	k1gMnSc1	Eduard
Pentslin	Pentslin	k1gInSc1	Pentslin
</s>
</p>
<p>
<s>
Tajuplný	tajuplný	k2eAgInSc1d1	tajuplný
ostrov	ostrov	k1gInSc1	ostrov
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Mysterious	Mysterious	k1gMnSc1	Mysterious
Island	Island	k1gInSc1	Island
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Cyril	Cyril	k1gMnSc1	Cyril
Endfield	Endfield	k1gMnSc1	Endfield
<g/>
,	,	kIx,	,
v	v	k7c6	v
roli	role	k1gFnSc6	role
kapitána	kapitán	k1gMnSc2	kapitán
Nema	Nemus	k1gMnSc2	Nemus
Herbert	Herbert	k1gMnSc1	Herbert
Lom	lom	k1gInSc1	lom
</s>
</p>
<p>
<s>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
île	île	k?	île
mystérieuse	mystérieuse	k1gFnSc1	mystérieuse
/	/	kIx~	/
La	la	k1gNnSc1	la
Isla	Isl	k1gInSc2	Isl
misteriosa	misteriosa	k1gFnSc1	misteriosa
y	y	k?	y
el	ela	k1gFnPc2	ela
capitán	capitán	k1gMnSc1	capitán
Nemo	Nemo	k1gMnSc1	Nemo
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
a	a	k8xC	a
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Henri	Henr	k1gFnSc2	Henr
Colpi	Colp	k1gFnSc2	Colp
a	a	k8xC	a
Juan	Juan	k1gMnSc1	Juan
Antonio	Antonio	k1gMnSc1	Antonio
Bardem	bard	k1gMnSc7	bard
<g/>
,	,	kIx,	,
v	v	k7c6	v
roli	role	k1gFnSc6	role
kapitána	kapitán	k1gMnSc2	kapitán
Nema	Nemum	k1gNnSc2	Nemum
Omar	Omar	k1gMnSc1	Omar
Sharif	Sharif	k1gMnSc1	Sharif
</s>
</p>
<p>
<s>
Tajuplný	tajuplný	k2eAgInSc1d1	tajuplný
ostrov	ostrov	k1gInSc1	ostrov
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Mysterious	Mysterious	k1gMnSc1	Mysterious
Island	Island	k1gInSc1	Island
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
a	a	k8xC	a
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
</s>
</p>
<p>
<s>
Tajuplný	tajuplný	k2eAgInSc1d1	tajuplný
ostrov	ostrov	k1gInSc1	ostrov
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
île	île	k?	île
mystérieuse	mystérieuse	k1gFnSc1	mystérieuse
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Claude	Claud	k1gInSc5	Claud
Allix	Allix	k1gInSc1	Allix
<g/>
,	,	kIx,	,
animovaný	animovaný	k2eAgInSc1d1	animovaný
film	film	k1gInSc1	film
</s>
</p>
<p>
<s>
Tajuplný	tajuplný	k2eAgInSc1d1	tajuplný
ostrov	ostrov	k1gInSc1	ostrov
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Mysterious	Mysterious	k1gMnSc1	Mysterious
Island	Island	k1gInSc1	Island
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc1	Austrálie
a	a	k8xC	a
USA	USA	kA	USA
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Russell	Russella	k1gFnPc2	Russella
Mulcahy	Mulcaha	k1gFnSc2	Mulcaha
<g/>
,	,	kIx,	,
v	v	k7c6	v
roli	role	k1gFnSc6	role
kapitána	kapitán	k1gMnSc2	kapitán
Nema	Nemum	k1gNnSc2	Nemum
Patrick	Patrick	k1gMnSc1	Patrick
Stewart	Stewart	k1gMnSc1	Stewart
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
</s>
</p>
<p>
<s>
Tajuplný	tajuplný	k2eAgInSc1d1	tajuplný
ostrov	ostrov	k1gInSc1	ostrov
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Mysterious	Mysterious	k1gMnSc1	Mysterious
Island	Island	k1gInSc1	Island
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Mark	Mark	k1gMnSc1	Mark
Sheppard	Sheppard	k1gMnSc1	Sheppard
</s>
</p>
<p>
<s>
Cesta	cesta	k1gFnSc1	cesta
2	[number]	k4	2
<g/>
:	:	kIx,	:
Tajuplný	tajuplný	k2eAgInSc1d1	tajuplný
ostrov	ostrov	k1gInSc1	ostrov
(	(	kIx(	(
<g/>
Journey	Journea	k1gFnSc2	Journea
2	[number]	k4	2
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Mysterious	Mysterious	k1gMnSc1	Mysterious
Island	Island	k1gInSc1	Island
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc2	režie
Brad	brada	k1gFnPc2	brada
Peyton	Peyton	k1gInSc1	Peyton
<g/>
,	,	kIx,	,
pokračování	pokračování	k1gNnSc1	pokračování
filmu	film	k1gInSc2	film
Cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
středu	střed	k1gInSc2	střed
Země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
Journey	Journea	k1gMnSc2	Journea
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Center	centrum	k1gNnPc2	centrum
of	of	k?	of
the	the	k?	the
Earth	Earth	k1gMnSc1	Earth
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
3D	[number]	k4	3D
film	film	k1gInSc1	film
</s>
</p>
<p>
<s>
==	==	k?	==
Divadelní	divadelní	k2eAgFnSc1d1	divadelní
adaptace	adaptace	k1gFnSc1	adaptace
==	==	k?	==
</s>
</p>
<p>
<s>
Románem	román	k1gInSc7	román
Tajuplný	tajuplný	k2eAgInSc4d1	tajuplný
ostrov	ostrov	k1gInSc4	ostrov
se	se	k3xPyFc4	se
inspirovalo	inspirovat	k5eAaBmAgNnS	inspirovat
představení	představení	k1gNnSc1	představení
pražské	pražský	k2eAgFnSc2d1	Pražská
Laterny	laterna	k1gFnSc2	laterna
magiky	magika	k1gFnSc2	magika
Podivuhodné	podivuhodný	k2eAgFnSc2d1	podivuhodná
cesty	cesta	k1gFnSc2	cesta
Julese	Julese	k1gFnSc1	Julese
Verna	Verna	k1gFnSc1	Verna
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Ilustrace	ilustrace	k1gFnSc2	ilustrace
==	==	k?	==
</s>
</p>
<p>
<s>
Knihu	kniha	k1gFnSc4	kniha
Tajuplný	tajuplný	k2eAgInSc4d1	tajuplný
ostrov	ostrov	k1gInSc4	ostrov
poprvé	poprvé	k6eAd1	poprvé
ilustroval	ilustrovat	k5eAaBmAgInS	ilustrovat
Jules	Jules	k1gInSc1	Jules
Férat	Férat	k2eAgInSc1d1	Férat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Česká	český	k2eAgNnPc1d1	české
vydání	vydání	k1gNnPc1	vydání
v	v	k7c4	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
až	až	k9	až
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc1	století
proslavilo	proslavit	k5eAaPmAgNnS	proslavit
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
SNDK	SNDK	kA	SNDK
(	(	kIx(	(
<g/>
Albatros	albatros	k1gMnSc1	albatros
<g/>
)	)	kIx)	)
v	v	k7c6	v
grafické	grafický	k2eAgFnSc6d1	grafická
úpravě	úprava	k1gFnSc6	úprava
Jiřího	Jiří	k1gMnSc2	Jiří
Blažka	Blažek	k1gMnSc2	Blažek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Časový	časový	k2eAgInSc1d1	časový
nesoulad	nesoulad	k1gInSc1	nesoulad
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
díly	díl	k1gInPc7	díl
trilogie	trilogie	k1gFnSc2	trilogie
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
díly	díl	k1gInPc7	díl
trilogie	trilogie	k1gFnSc2	trilogie
panuje	panovat	k5eAaImIp3nS	panovat
časový	časový	k2eAgInSc1d1	časový
nesoulad	nesoulad	k1gInSc1	nesoulad
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
kapitána	kapitán	k1gMnSc2	kapitán
Granta	Grant	k1gMnSc2	Grant
se	se	k3xPyFc4	se
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1864	[number]	k4	1864
až	až	k9	až
1865	[number]	k4	1865
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
příběhu	příběh	k1gInSc2	příběh
<g/>
,	,	kIx,	,
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
roku	rok	k1gInSc2	rok
1865	[number]	k4	1865
je	být	k5eAaImIp3nS	být
Ayrton	Ayrton	k1gInSc1	Ayrton
vysazen	vysazen	k2eAgInSc1d1	vysazen
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Tabor	Tabor	k1gInSc1	Tabor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Tajuplném	tajuplný	k2eAgInSc6d1	tajuplný
ostrově	ostrov	k1gInSc6	ostrov
se	se	k3xPyFc4	se
děj	děj	k1gInSc1	děj
příběhu	příběh	k1gInSc2	příběh
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1865	[number]	k4	1865
až	až	k9	až
1869	[number]	k4	1869
<g/>
.	.	kIx.	.
</s>
<s>
Cyrus	Cyrus	k1gMnSc1	Cyrus
Smith	Smith	k1gMnSc1	Smith
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
přáteli	přítel	k1gMnPc7	přítel
nalézá	nalézat	k5eAaImIp3nS	nalézat
Ayrtona	Ayrtona	k1gFnSc1	Ayrtona
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
1866	[number]	k4	1866
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Ayrton	Ayrton	k1gInSc1	Ayrton
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
vysazen	vysazen	k2eAgInSc4d1	vysazen
před	před	k7c7	před
dvanácti	dvanáct	k4xCc2	dvanáct
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1854	[number]	k4	1854
<g/>
,	,	kIx,	,
deset	deset	k4xCc1	deset
let	léto	k1gNnPc2	léto
před	před	k7c7	před
datem	datum	k1gNnSc7	datum
zmíněným	zmíněný	k2eAgNnSc7d1	zmíněné
v	v	k7c6	v
Dětech	dítě	k1gFnPc6	dítě
kapitána	kapitán	k1gMnSc2	kapitán
Granta	Grant	k1gMnSc2	Grant
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Tajuplném	tajuplný	k2eAgInSc6d1	tajuplný
ostrově	ostrov	k1gInSc6	ostrov
sděluje	sdělovat	k5eAaImIp3nS	sdělovat
Cyrus	Cyrus	k1gMnSc1	Cyrus
Smith	Smith	k1gMnSc1	Smith
roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
Nemovi	Nemův	k2eAgMnPc1d1	Nemův
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
zná	znát	k5eAaImIp3nS	znát
z	z	k7c2	z
Aronnaxovy	Aronnaxův	k2eAgFnSc2d1	Aronnaxův
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
napsal	napsat	k5eAaBmAgInS	napsat
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
podmořské	podmořský	k2eAgFnSc6d1	podmořská
cestě	cesta	k1gFnSc6	cesta
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
Nautila	Nautilus	k1gMnSc2	Nautilus
<g/>
,	,	kIx,	,
a	a	k8xC	a
Nemo	Nemo	k6eAd1	Nemo
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
již	již	k6eAd1	již
šestnáct	šestnáct	k4xCc4	šestnáct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
co	co	k9	co
Aronnax	Aronnax	k1gInSc1	Aronnax
z	z	k7c2	z
ponorky	ponorka	k1gFnSc2	ponorka
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
Dvacet	dvacet	k4xCc4	dvacet
tisíc	tisíc	k4xCgInPc2	tisíc
mil	míle	k1gFnPc2	míle
pod	pod	k7c7	pod
mořem	moře	k1gNnSc7	moře
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1866	[number]	k4	1866
a	a	k8xC	a
ne	ne	k9	ne
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1853	[number]	k4	1853
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Tajuplném	tajuplný	k2eAgInSc6d1	tajuplný
ostrově	ostrov	k1gInSc6	ostrov
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dozvídáme	dozvídat	k5eAaImIp1nP	dozvídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kníže	kníže	k1gMnSc1	kníže
Dakkar	Dakkar	k1gMnSc1	Dakkar
přijal	přijmout	k5eAaPmAgMnS	přijmout
jméno	jméno	k1gNnSc4	jméno
Nemo	Nemo	k6eAd1	Nemo
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
povstání	povstání	k1gNnSc2	povstání
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
odehrálo	odehrát	k5eAaPmAgNnS	odehrát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1857	[number]	k4	1857
<g/>
,	,	kIx,	,
a	a	k8xC	a
zemřel	zemřít	k5eAaPmAgMnS	zemřít
údajně	údajně	k6eAd1	údajně
po	po	k7c6	po
třiceti	třicet	k4xCc6	třicet
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
znamenalo	znamenat	k5eAaImAgNnS	znamenat
rok	rok	k1gInSc4	rok
1887	[number]	k4	1887
a	a	k8xC	a
ne	ne	k9	ne
1869	[number]	k4	1869
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Už	už	k6eAd1	už
Jules	Jules	k1gInSc1	Jules
Verne	Vern	k1gInSc5	Vern
i	i	k9	i
jeho	jeho	k3xOp3gMnSc1	jeho
vydavatel	vydavatel	k1gMnSc1	vydavatel
Pierre-Jules	Pierre-Jules	k1gMnSc1	Pierre-Jules
Hetzel	Hetzel	k1gMnSc1	Hetzel
si	se	k3xPyFc3	se
těchto	tento	k3xDgInPc2	tento
časových	časový	k2eAgInPc2d1	časový
paradoxů	paradox	k1gInPc2	paradox
byli	být	k5eAaImAgMnP	být
vědomi	vědom	k2eAgMnPc1d1	vědom
<g/>
.	.	kIx.	.
</s>
<s>
Verne	Vernout	k5eAaImIp3nS	Vernout
je	on	k3xPp3gMnPc4	on
nakonec	nakonec	k6eAd1	nakonec
vyřešil	vyřešit	k5eAaPmAgInS	vyřešit
poznámkou	poznámka	k1gFnSc7	poznámka
v	v	k7c6	v
Tajuplném	tajuplný	k2eAgInSc6d1	tajuplný
ostrově	ostrov	k1gInSc6	ostrov
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
nejrůznější	různý	k2eAgFnPc4d3	nejrůznější
okolnosti	okolnost	k1gFnPc4	okolnost
nemohl	moct	k5eNaImAgMnS	moct
uvést	uvést	k5eAaPmF	uvést
skutečná	skutečný	k2eAgNnPc1d1	skutečné
data	datum	k1gNnPc1	datum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Česká	český	k2eAgNnPc1d1	české
vydání	vydání	k1gNnPc1	vydání
==	==	k?	==
</s>
</p>
<p>
<s>
Tajemný	tajemný	k2eAgInSc1d1	tajemný
ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1878	[number]	k4	1878
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
František	František	k1gMnSc1	František
Brábek	Brábek	k1gMnSc1	Brábek
<g/>
,	,	kIx,	,
tři	tři	k4xCgInPc4	tři
svazky	svazek	k1gInPc4	svazek
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1896	[number]	k4	1896
a	a	k8xC	a
1905	[number]	k4	1905
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tajůplný	tajůplný	k2eAgInSc1d1	tajůplný
ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
J.	J.	kA	J.
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1897	[number]	k4	1897
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
A.	A.	kA	A.
Koudelka	Koudelka	k1gMnSc1	Koudelka
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1912	[number]	k4	1912
</s>
</p>
<p>
<s>
Tajemný	tajemný	k2eAgInSc1d1	tajemný
ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
Bedřich	Bedřich	k1gMnSc1	Bedřich
Kočí	Kočí	k1gMnSc1	Kočí
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Gustav	Gustav	k1gMnSc1	Gustav
Žďárský	Žďárský	k1gMnSc1	Žďárský
<g/>
,	,	kIx,	,
tři	tři	k4xCgInPc4	tři
svazky	svazek	k1gInPc4	svazek
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
Trosečníci	trosečník	k1gMnPc1	trosečník
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Opuštěný	opuštěný	k2eAgMnSc1d1	opuštěný
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Tajemství	tajemství	k1gNnSc1	tajemství
ostrova	ostrov	k1gInSc2	ostrov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1911	[number]	k4	1911
a	a	k8xC	a
1922	[number]	k4	1922
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tajuplný	tajuplný	k2eAgInSc1d1	tajuplný
ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
J.	J.	kA	J.
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
A.	A.	kA	A.
Koudelka	Koudelka	k1gMnSc1	Koudelka
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1929	[number]	k4	1929
a	a	k8xC	a
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tajuplný	tajuplný	k2eAgInSc1d1	tajuplný
ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
J.	J.	kA	J.
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Hobzík	Hobzík	k1gMnSc1	Hobzík
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1948	[number]	k4	1948
a	a	k8xC	a
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tajuplný	tajuplný	k2eAgInSc1d1	tajuplný
ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
Práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
vycházel	vycházet	k5eAaImAgInS	vycházet
na	na	k7c4	na
pokračování	pokračování	k1gNnSc4	pokračování
jako	jako	k8xS	jako
příloha	příloha	k1gFnSc1	příloha
svazků	svazek	k1gInPc2	svazek
edice	edice	k1gFnSc2	edice
Knižní	knižní	k2eAgFnSc1d1	knižní
novinky	novinka	k1gFnPc1	novinka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tajuplný	tajuplný	k2eAgInSc1d1	tajuplný
ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
SNDK	SNDK	kA	SNDK
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Václav	Václav	k1gMnSc1	Václav
Netušil	Netušil	k1gMnSc1	Netušil
<g/>
,	,	kIx,	,
grafická	grafický	k2eAgFnSc1d1	grafická
úprava	úprava	k1gFnSc1	úprava
Jiří	Jiří	k1gMnSc1	Jiří
Blažek	Blažek	k1gMnSc1	Blažek
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
1960	[number]	k4	1960
a	a	k8xC	a
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tajuplný	tajuplný	k2eAgInSc1d1	tajuplný
ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Benjamin	Benjamin	k1gMnSc1	Benjamin
Jedlička	Jedlička	k1gMnSc1	Jedlička
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Tajuplný	tajuplný	k2eAgInSc1d1	tajuplný
ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
SPN	SPN	kA	SPN
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Václav	Václav	k1gMnSc1	Václav
Netušil	Netušil	k1gMnSc1	Netušil
<g/>
,	,	kIx,	,
grafická	grafický	k2eAgFnSc1d1	grafická
úprava	úprava	k1gFnSc1	úprava
Jiří	Jiří	k1gMnSc1	Jiří
Blažek	Blažek	k1gMnSc1	Blažek
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tajuplný	tajuplný	k2eAgInSc1d1	tajuplný
ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Václav	Václav	k1gMnSc1	Václav
Netušil	Netušil	k1gMnSc1	Netušil
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
a	a	k8xC	a
1988	[number]	k4	1988
a	a	k8xC	a
Sfinga	sfinga	k1gFnSc1	sfinga
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tajuplný	tajuplný	k2eAgInSc1d1	tajuplný
ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
Návrat	návrat	k1gInSc1	návrat
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
A.	A.	kA	A.
Koudelka	Koudelka	k1gMnSc1	Koudelka
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tajuplný	tajuplný	k2eAgInSc1d1	tajuplný
ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
převyprávěl	převyprávět	k5eAaPmAgMnS	převyprávět
Ondřej	Ondřej	k1gMnSc1	Ondřej
Neff	Neff	k1gMnSc1	Neff
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tajuplný	tajuplný	k2eAgInSc1d1	tajuplný
ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
Omega	omega	k1gFnSc1	omega
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
A.	A.	kA	A.
Koudelka	Koudelka	k1gMnSc1	Koudelka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Tajuplný	tajuplný	k2eAgInSc4d1	tajuplný
ostrov	ostrov	k1gInSc4	ostrov
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Francouzský	francouzský	k2eAgInSc1d1	francouzský
text	text	k1gInSc1	text
románu	román	k1gInSc2	román
</s>
</p>
<p>
<s>
Tajuplný	tajuplný	k2eAgInSc1d1	tajuplný
ostrov	ostrov	k1gInSc1	ostrov
v	v	k7c6	v
Československé	československý	k2eAgFnSc6d1	Československá
bibliografické	bibliografický	k2eAgFnSc6d1	bibliografická
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
