<s>
Tajuplný	tajuplný	k2eAgInSc1d1	tajuplný
ostrov	ostrov	k1gInSc1	ostrov
(	(	kIx(	(
<g/>
1875	[number]	k4	1875
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
île	île	k?	île
mystérieuse	mystérieuse	k1gFnSc1	mystérieuse
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgMnPc2d3	nejznámější
románů	román	k1gInPc2	román
francouzského	francouzský	k2eAgMnSc2d1	francouzský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Julesa	Julesa	k1gFnSc1	Julesa
Verna	Verna	k1gFnSc1	Verna
z	z	k7c2	z
jeho	on	k3xPp3gInSc2	on
cyklu	cyklus	k1gInSc2	cyklus
Podivuhodné	podivuhodný	k2eAgFnSc2d1	podivuhodná
cesty	cesta	k1gFnSc2	cesta
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
Voyages	Voyages	k1gMnSc1	Voyages
extraordinaires	extraordinaires	k1gMnSc1	extraordinaires
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
