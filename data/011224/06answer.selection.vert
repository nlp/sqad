<s>
Válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Zálivu	záliv	k1gInSc6	záliv
či	či	k8xC	či
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Perském	perský	k2eAgInSc6d1	perský
zálivu	záliv	k1gInSc6	záliv
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1990	[number]	k4	1990
–	–	k?	–
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
operace	operace	k1gFnPc4	operace
vedoucí	vedoucí	k1gFnSc2	vedoucí
k	k	k7c3	k
nahromadění	nahromadění	k1gNnSc3	nahromadění
vojsk	vojsko	k1gNnPc2	vojsko
na	na	k7c6	na
území	území	k1gNnSc6	území
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
i	i	k9	i
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
obraně	obrana	k1gFnSc3	obrana
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xC	jako
operace	operace	k1gFnPc1	operace
Pouštní	pouštní	k2eAgFnPc1d1	pouštní
štít	štít	k1gInSc4	štít
a	a	k8xC	a
operace	operace	k1gFnPc4	operace
Pouštní	pouštní	k2eAgFnPc4d1	pouštní
bouře	bouř	k1gFnPc4	bouř
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1991	[number]	k4	1991
–	–	k?	–
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
ozbrojeným	ozbrojený	k2eAgInSc7d1	ozbrojený
konfliktem	konflikt	k1gInSc7	konflikt
mezi	mezi	k7c7	mezi
Irákem	Irák	k1gInSc7	Irák
a	a	k8xC	a
koalicí	koalice	k1gFnSc7	koalice
28	[number]	k4	28
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
Československa	Československo	k1gNnSc2	Československo
<g/>
)	)	kIx)	)
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kterým	který	k3yRgInPc3	který
OSN	OSN	kA	OSN
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
mandát	mandát	k1gInSc4	mandát
k	k	k7c3	k
provedení	provedení	k1gNnSc3	provedení
vojenské	vojenský	k2eAgFnSc2d1	vojenská
operace	operace	k1gFnSc2	operace
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
osvobození	osvobození	k1gNnSc2	osvobození
Kuvajtu	Kuvajt	k1gInSc2	Kuvajt
<g/>
.	.	kIx.	.
</s>
