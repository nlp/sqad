<p>
<s>
Válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Zálivu	záliv	k1gInSc6	záliv
či	či	k8xC	či
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Perském	perský	k2eAgInSc6d1	perský
zálivu	záliv	k1gInSc6	záliv
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1990	[number]	k4	1990
–	–	k?	–
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
operace	operace	k1gFnPc4	operace
vedoucí	vedoucí	k1gFnSc2	vedoucí
k	k	k7c3	k
nahromadění	nahromadění	k1gNnSc3	nahromadění
vojsk	vojsko	k1gNnPc2	vojsko
na	na	k7c6	na
území	území	k1gNnSc6	území
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
i	i	k9	i
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
obraně	obrana	k1gFnSc3	obrana
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xC	jako
operace	operace	k1gFnPc1	operace
Pouštní	pouštní	k2eAgFnPc1d1	pouštní
štít	štít	k1gInSc4	štít
a	a	k8xC	a
operace	operace	k1gFnPc4	operace
Pouštní	pouštní	k2eAgFnPc4d1	pouštní
bouře	bouř	k1gFnPc4	bouř
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1991	[number]	k4	1991
–	–	k?	–
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
ozbrojeným	ozbrojený	k2eAgInSc7d1	ozbrojený
konfliktem	konflikt	k1gInSc7	konflikt
mezi	mezi	k7c7	mezi
Irákem	Irák	k1gInSc7	Irák
a	a	k8xC	a
koalicí	koalice	k1gFnSc7	koalice
28	[number]	k4	28
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
Československa	Československo	k1gNnSc2	Československo
<g/>
)	)	kIx)	)
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kterým	který	k3yQgInPc3	který
OSN	OSN	kA	OSN
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
mandát	mandát	k1gInSc4	mandát
k	k	k7c3	k
provedení	provedení	k1gNnSc3	provedení
vojenské	vojenský	k2eAgFnSc2d1	vojenská
operace	operace	k1gFnSc2	operace
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
osvobození	osvobození	k1gNnSc2	osvobození
Kuvajtu	Kuvajt	k1gInSc2	Kuvajt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Irácká	irácký	k2eAgFnSc1d1	irácká
invaze	invaze	k1gFnSc1	invaze
do	do	k7c2	do
Kuvajtu	Kuvajt	k1gInSc2	Kuvajt
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
setkala	setkat	k5eAaPmAgFnS	setkat
s	s	k7c7	s
mezinárodním	mezinárodní	k2eAgNnSc7d1	mezinárodní
odsouzením	odsouzení	k1gNnSc7	odsouzení
<g/>
,	,	kIx,	,
Rada	rada	k1gFnSc1	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
schválila	schválit	k5eAaPmAgFnS	schválit
ekonomické	ekonomický	k2eAgFnPc4d1	ekonomická
sankce	sankce	k1gFnPc4	sankce
proti	proti	k7c3	proti
Iráku	Irák	k1gInSc3	Irák
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
George	Georg	k1gMnSc2	Georg
H.	H.	kA	H.
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
vyslal	vyslat	k5eAaPmAgInS	vyslat
americká	americký	k2eAgNnPc4d1	americké
vojska	vojsko	k1gNnPc4	vojsko
k	k	k7c3	k
obraně	obrana	k1gFnSc3	obrana
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
<g/>
,	,	kIx,	,
záhy	záhy	k6eAd1	záhy
se	se	k3xPyFc4	se
přidaly	přidat	k5eAaPmAgInP	přidat
další	další	k2eAgInPc1d1	další
státy	stát	k1gInPc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
vojenských	vojenský	k2eAgFnPc2d1	vojenská
sil	síla	k1gFnPc2	síla
koalice	koalice	k1gFnSc2	koalice
tvořili	tvořit	k5eAaImAgMnP	tvořit
Američané	Američan	k1gMnPc1	Američan
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Saúdskými	saúdský	k2eAgMnPc7d1	saúdský
Araby	Arab	k1gMnPc7	Arab
následovaní	následovaný	k2eAgMnPc1d1	následovaný
Brity	Brit	k1gMnPc7	Brit
<g/>
,	,	kIx,	,
Egypťany	Egypťan	k1gMnPc7	Egypťan
a	a	k8xC	a
Francouzi	Francouz	k1gMnPc1	Francouz
<g/>
.	.	kIx.	.
</s>
<s>
Polovinu	polovina	k1gFnSc4	polovina
nákladů	náklad	k1gInPc2	náklad
operace	operace	k1gFnSc2	operace
uhradila	uhradit	k5eAaPmAgFnS	uhradit
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příčiny	příčina	k1gFnSc2	příčina
války	válka	k1gFnSc2	válka
==	==	k?	==
</s>
</p>
<p>
<s>
Irácko-íránská	irácko-íránský	k2eAgFnSc1d1	irácko-íránská
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
skončila	skončit	k5eAaPmAgFnS	skončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
Saddáma	Saddám	k1gMnSc4	Saddám
Husajna	Husajn	k1gMnSc4	Husajn
a	a	k8xC	a
jeho	on	k3xPp3gInSc4	on
režim	režim	k1gInSc4	režim
finančně	finančně	k6eAd1	finančně
zruinovala	zruinovat	k5eAaPmAgFnS	zruinovat
<g/>
.	.	kIx.	.
</s>
<s>
Dlužil	dlužit	k5eAaImAgInS	dlužit
15	[number]	k4	15
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
Kuvajtu	Kuvajt	k1gInSc2	Kuvajt
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
stejnou	stejný	k2eAgFnSc4d1	stejná
částku	částka	k1gFnSc4	částka
Saúdské	saúdský	k2eAgFnSc6d1	Saúdská
Arábii	Arábie	k1gFnSc6	Arábie
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
10	[number]	k4	10
miliard	miliarda	k4xCgFnPc2	miliarda
ostatním	ostatní	k2eAgMnPc3d1	ostatní
věřitelům	věřitel	k1gMnPc3	věřitel
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
půjčky	půjčka	k1gFnPc1	půjčka
mu	on	k3xPp3gNnSc3	on
byly	být	k5eAaImAgFnP	být
zamítnuty	zamítnut	k2eAgFnPc1d1	zamítnuta
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
záchranou	záchrana	k1gFnSc7	záchrana
ze	z	k7c2	z
spirály	spirála	k1gFnSc2	spirála
dluhů	dluh	k1gInPc2	dluh
a	a	k8xC	a
úroků	úrok	k1gInPc2	úrok
byla	být	k5eAaImAgFnS	být
domácí	domácí	k2eAgFnSc1d1	domácí
ropná	ropný	k2eAgFnSc1d1	ropná
produkce	produkce	k1gFnSc1	produkce
<g/>
.	.	kIx.	.
</s>
<s>
Jenže	jenže	k8xC	jenže
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
konce	konec	k1gInSc2	konec
Irácko-íránské	irácko-íránský	k2eAgFnSc2d1	irácko-íránská
války	válka	k1gFnSc2	válka
společenství	společenství	k1gNnSc2	společenství
OPEC	opéct	k5eAaPmRp2nSwK	opéct
schválilo	schválit	k5eAaPmAgNnS	schválit
limity	limita	k1gFnPc4	limita
produkce	produkce	k1gFnSc2	produkce
svých	svůj	k3xOyFgInPc2	svůj
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
stavělo	stavět	k5eAaImAgNnS	stavět
Saddáma	Saddám	k1gMnSc4	Saddám
Hussajna	Hussajen	k2eAgMnSc4d1	Hussajen
do	do	k7c2	do
ještě	ještě	k6eAd1	ještě
svízelnější	svízelný	k2eAgFnSc2d2	svízelnější
situace	situace	k1gFnSc2	situace
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
<g/>
,	,	kIx,	,
Kuvajt	Kuvajt	k1gInSc1	Kuvajt
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
přesáhl	přesáhnout	k5eAaPmAgInS	přesáhnout
v	v	k7c6	v
těžbě	těžba	k1gFnSc6	těžba
ropy	ropa	k1gFnSc2	ropa
svůj	svůj	k3xOyFgInSc4	svůj
limit	limit	k1gInSc4	limit
o	o	k7c4	o
20	[number]	k4	20
%	%	kIx~	%
a	a	k8xC	a
nepřímo	přímo	k6eNd1	přímo
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
cena	cena	k1gFnSc1	cena
této	tento	k3xDgFnSc2	tento
suroviny	surovina	k1gFnSc2	surovina
vlivem	vlivem	k7c2	vlivem
převisu	převis	k1gInSc2	převis
nabídky	nabídka	k1gFnSc2	nabídka
poklesla	poklesnout	k5eAaPmAgFnS	poklesnout
<g/>
,	,	kIx,	,
což	což	k9	což
výnosy	výnos	k1gInPc4	výnos
z	z	k7c2	z
irácké	irácký	k2eAgFnSc2d1	irácká
ropné	ropný	k2eAgFnSc2d1	ropná
produkce	produkce	k1gFnSc2	produkce
(	(	kIx(	(
<g/>
která	který	k3yQgFnSc1	který
tvořila	tvořit	k5eAaImAgFnS	tvořit
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
ekonomiky	ekonomika	k1gFnSc2	ekonomika
celé	celá	k1gFnSc2	celá
této	tento	k3xDgFnSc2	tento
země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
znehodnotilo	znehodnotit	k5eAaPmAgNnS	znehodnotit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Husajn	Husajn	k1gMnSc1	Husajn
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
vydal	vydat	k5eAaPmAgInS	vydat
cestou	cestou	k7c2	cestou
nátlaku	nátlak	k1gInSc2	nátlak
na	na	k7c4	na
Kuvajt	Kuvajt	k1gInSc4	Kuvajt
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
otevřeně	otevřeně	k6eAd1	otevřeně
provokovat	provokovat	k5eAaImF	provokovat
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Iráku	Irák	k1gInSc2	Irák
náleží	náležet	k5eAaImIp3nS	náležet
ušlý	ušlý	k2eAgInSc4d1	ušlý
zisk	zisk	k1gInSc4	zisk
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
22,4	[number]	k4	22,4
mld	mld	k?	mld
dolarů	dolar	k1gInPc2	dolar
z	z	k7c2	z
ukradené	ukradený	k2eAgFnSc2d1	ukradená
ropy	ropa	k1gFnSc2	ropa
těžené	těžený	k2eAgFnSc2d1	těžená
Kuvajtem	Kuvajt	k1gInSc7	Kuvajt
metodou	metoda	k1gFnSc7	metoda
šikmých	šikmý	k2eAgInPc2d1	šikmý
vrtů	vrt	k1gInPc2	vrt
u	u	k7c2	u
ropného	ropný	k2eAgNnSc2d1	ropné
pole	pole	k1gNnSc2	pole
Rumaila	Rumaila	k1gFnSc1	Rumaila
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
leží	ležet	k5eAaImIp3nP	ležet
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
obou	dva	k4xCgInPc2	dva
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kuvajt	Kuvajt	k1gInSc1	Kuvajt
je	být	k5eAaImIp3nS	být
historicky	historicky	k6eAd1	historicky
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
i	i	k9	i
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
<g />
.	.	kIx.	.
</s>
<s>
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
oddělen	oddělit	k5eAaPmNgInS	oddělit
<g/>
)	)	kIx)	)
19	[number]	k4	19
<g/>
.	.	kIx.	.
provincií	provincie	k1gFnSc7	provincie
Iráku	Irák	k1gInSc2	Irák
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
1990	[number]	k4	1990
nebyly	být	k5eNaImAgInP	být
irácko-kuvajtské	iráckouvajtský	k2eAgInPc1d1	irácko-kuvajtský
vztahy	vztah	k1gInPc1	vztah
pro	pro	k7c4	pro
USA	USA	kA	USA
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
zájmu	zájem	k1gInSc2	zájem
–	–	k?	–
přesněji	přesně	k6eAd2	přesně
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
velvyslanec	velvyslanec	k1gMnSc1	velvyslanec
Husajnovi	Husajn	k1gMnSc3	Husajn
doslova	doslova	k6eAd1	doslova
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
spor	spor	k1gInSc1	spor
s	s	k7c7	s
Kuvajtem	Kuvajt	k1gInSc7	Kuvajt
je	být	k5eAaImIp3nS	být
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
arabská	arabský	k2eAgFnSc1d1	arabská
záležitost	záležitost	k1gFnSc1	záležitost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
nijak	nijak	k6eAd1	nijak
netýká	týkat	k5eNaImIp3nS	týkat
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Americká	americký	k2eAgFnSc1d1	americká
zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
politika	politika	k1gFnSc1	politika
se	se	k3xPyFc4	se
již	již	k9	již
řadu	řada	k1gFnSc4	řada
let	léto	k1gNnPc2	léto
zabývala	zabývat	k5eAaImAgFnS	zabývat
islámskou	islámský	k2eAgFnSc7d1	islámská
revolucí	revoluce	k1gFnSc7	revoluce
v	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
začala	začít	k5eAaPmAgFnS	začít
svržením	svržení	k1gNnSc7	svržení
šáha	šáh	k1gMnSc2	šáh
<g/>
.	.	kIx.	.
</s>
<s>
Irák	Irák	k1gInSc1	Irák
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
jevil	jevit	k5eAaImAgInS	jevit
jako	jako	k8xS	jako
vhodná	vhodný	k2eAgFnSc1d1	vhodná
protiváha	protiváha	k1gFnSc1	protiváha
fundamentalistů	fundamentalista	k1gMnPc2	fundamentalista
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
začala	začít	k5eAaPmAgFnS	začít
irácko-íránská	irácko-íránský	k2eAgFnSc1d1	irácko-íránská
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
USA	USA	kA	USA
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
i	i	k9	i
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgInPc2d1	další
států	stát	k1gInPc2	stát
včetně	včetně	k7c2	včetně
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
SSSR	SSSR	kA	SSSR
<g/>
)	)	kIx)	)
Saddáma	Saddám	k1gMnSc2	Saddám
fakticky	fakticky	k6eAd1	fakticky
podporovalo	podporovat	k5eAaImAgNnS	podporovat
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
informací	informace	k1gFnPc2	informace
CIA	CIA	kA	CIA
o	o	k7c6	o
pohybech	pohyb	k1gInPc6	pohyb
íránské	íránský	k2eAgFnSc2d1	íránská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jasném	jasný	k2eAgNnSc6d1	jasné
vyjádření	vyjádření	k1gNnSc6	vyjádření
velvyslance	velvyslanec	k1gMnSc2	velvyslanec
USA	USA	kA	USA
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
případě	případ	k1gInSc6	případ
vojenského	vojenský	k2eAgInSc2d1	vojenský
zákroku	zákrok	k1gInSc2	zákrok
nebude	být	k5eNaImBp3nS	být
reagovat	reagovat	k5eAaBmF	reagovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Saddám	Saddám	k1gMnSc1	Saddám
Husajn	Husajn	k1gMnSc1	Husajn
odhodlal	odhodlat	k5eAaPmAgMnS	odhodlat
k	k	k7c3	k
vojenské	vojenský	k2eAgFnSc3d1	vojenská
akci	akce	k1gFnSc3	akce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
ihned	ihned	k6eAd1	ihned
poté	poté	k6eAd1	poté
zahájily	zahájit	k5eAaPmAgFnP	zahájit
kampaň	kampaň	k1gFnSc4	kampaň
za	za	k7c4	za
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
intervenci	intervence	k1gFnSc4	intervence
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
USA	USA	kA	USA
si	se	k3xPyFc3	se
pomohl	pomoct	k5eAaPmAgInS	pomoct
kupříkladu	kupříkladu	k6eAd1	kupříkladu
svědectvím	svědectví	k1gNnSc7	svědectví
kuvajtské	kuvajtský	k2eAgFnSc2d1	kuvajtská
dívky	dívka	k1gFnSc2	dívka
Nayirah	Nayirah	k1gMnSc1	Nayirah
al-Ṣ	al-Ṣ	k?	al-Ṣ
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
před	před	k7c7	před
členy	člen	k1gMnPc7	člen
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
z	z	k7c2	z
amerického	americký	k2eAgInSc2d1	americký
kongresu	kongres	k1gInSc2	kongres
vypovídala	vypovídat	k5eAaPmAgFnS	vypovídat
o	o	k7c6	o
údajných	údajný	k2eAgNnPc6d1	údajné
zvěrstvech	zvěrstvo	k1gNnPc6	zvěrstvo
páchaných	páchaný	k2eAgFnPc2d1	páchaná
iráckou	irácký	k2eAgFnSc7d1	irácká
armádou	armáda	k1gFnSc7	armáda
(	(	kIx(	(
<g/>
vyndavání	vyndavání	k1gNnSc1	vyndavání
dětí	dítě	k1gFnPc2	dítě
z	z	k7c2	z
inkubátorů	inkubátor	k1gInPc2	inkubátor
a	a	k8xC	a
jejich	jejich	k3xOp3gNnPc2	jejich
ponechání	ponechání	k1gNnPc2	ponechání
na	na	k7c6	na
chladné	chladný	k2eAgFnSc6d1	chladná
podlaze	podlaha	k1gFnSc6	podlaha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měly	mít	k5eAaImAgFnP	mít
zemřít	zemřít	k5eAaPmF	zemřít
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
dceru	dcera	k1gFnSc4	dcera
kuvajtského	kuvajtský	k2eAgMnSc2d1	kuvajtský
velvyslance	velvyslanec	k1gMnSc2	velvyslanec
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
odhalil	odhalit	k5eAaPmAgMnS	odhalit
až	až	k6eAd1	až
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
novinář	novinář	k1gMnSc1	novinář
John	John	k1gMnSc1	John
MacArthur	MacArthur	k1gMnSc1	MacArthur
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
Human	Humana	k1gFnPc2	Humana
Rights	Rightsa	k1gFnPc2	Rightsa
Watch	Watch	k1gInSc1	Watch
a	a	k8xC	a
z	z	k7c2	z
výpovědí	výpověď	k1gFnPc2	výpověď
doktorů	doktor	k1gMnPc2	doktor
zmíněné	zmíněný	k2eAgFnSc2d1	zmíněná
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mělo	mít	k5eAaImAgNnS	mít
k	k	k7c3	k
incidentu	incident	k1gInSc3	incident
dojít	dojít	k5eAaPmF	dojít
a	a	k8xC	a
kde	kde	k6eAd1	kde
Nayirah	Nayirah	k1gInSc4	Nayirah
pracovala	pracovat	k5eAaImAgFnS	pracovat
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
výmysl	výmysl	k1gInSc4	výmysl
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Tento	tento	k3xDgInSc1	tento
výmysl	výmysl	k1gInSc1	výmysl
byl	být	k5eAaImAgInS	být
ale	ale	k8xC	ale
během	během	k7c2	během
předválečné	předválečný	k2eAgFnSc2d1	předválečná
kampaně	kampaň	k1gFnSc2	kampaň
velmi	velmi	k6eAd1	velmi
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
a	a	k8xC	a
pomohl	pomoct	k5eAaPmAgInS	pomoct
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
názory	názor	k1gInPc4	názor
na	na	k7c6	na
rozpoutání	rozpoutání	k1gNnSc6	rozpoutání
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Průběh	průběh	k1gInSc4	průběh
války	válka	k1gFnSc2	válka
==	==	k?	==
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1990	[number]	k4	1990
–	–	k?	–
irácké	irácký	k2eAgFnSc2d1	irácká
elitní	elitní	k2eAgFnSc2d1	elitní
jednotky	jednotka	k1gFnSc2	jednotka
vtrhly	vtrhnout	k5eAaPmAgFnP	vtrhnout
s	s	k7c7	s
obrovskou	obrovský	k2eAgFnSc7d1	obrovská
přesilou	přesila	k1gFnSc7	přesila
do	do	k7c2	do
Kuvajtu	Kuvajt	k1gInSc2	Kuvajt
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krátkém	krátký	k2eAgInSc6d1	krátký
boji	boj	k1gInSc6	boj
jej	on	k3xPp3gNnSc4	on
obsadily	obsadit	k5eAaPmAgFnP	obsadit
<g/>
.	.	kIx.	.
</s>
<s>
OSN	OSN	kA	OSN
přijalo	přijmout	k5eAaPmAgNnS	přijmout
rezoluci	rezoluce	k1gFnSc4	rezoluce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyzývala	vyzývat	k5eAaImAgFnS	vyzývat
Irák	Irák	k1gInSc4	Irák
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
okamžitě	okamžitě	k6eAd1	okamžitě
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
–	–	k?	–
Jordánský	jordánský	k2eAgMnSc1d1	jordánský
král	král	k1gMnSc1	král
Husajn	Husajn	k1gMnSc1	Husajn
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
konferenci	konference	k1gFnSc4	konference
čtyř	čtyři	k4xCgInPc2	čtyři
arabských	arabský	k2eAgInPc2d1	arabský
států	stát	k1gInPc2	stát
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
egyptského	egyptský	k2eAgMnSc4d1	egyptský
prezidenta	prezident	k1gMnSc4	prezident
Mubaraka	Mubarak	k1gMnSc4	Mubarak
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnPc4	jeho
osoby	osoba	k1gFnPc4	osoba
<g/>
,	,	kIx,	,
Saddáma	Saddám	k1gMnSc4	Saddám
Husajna	Husajn	k1gMnSc4	Husajn
a	a	k8xC	a
za	za	k7c2	za
předsednictví	předsednictví	k1gNnSc2	předsednictví
krále	král	k1gMnSc2	král
Fahda	Fahd	k1gMnSc2	Fahd
ze	z	k7c2	z
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
<g/>
.	.	kIx.	.
</s>
<s>
Věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
takováto	takovýto	k3xDgFnSc1	takovýto
konference	konference	k1gFnSc1	konference
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
iráckého	irácký	k2eAgMnSc4d1	irácký
diktátora	diktátor	k1gMnSc4	diktátor
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
boje	boj	k1gInSc2	boj
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
z	z	k7c2	z
Kuvajtu	Kuvajt	k1gInSc2	Kuvajt
<g/>
.	.	kIx.	.
</s>
<s>
Rychlé	Rychlé	k2eAgNnSc1d1	Rychlé
Saddámovo	Saddámův	k2eAgNnSc1d1	Saddámovo
odmítnutí	odmítnutí	k1gNnSc1	odmítnutí
jeho	jeho	k3xOp3gFnSc2	jeho
arabské	arabský	k2eAgFnSc2d1	arabská
přátele	přítel	k1gMnPc4	přítel
urazilo	urazit	k5eAaPmAgNnS	urazit
<g/>
.	.	kIx.	.
</s>
<s>
Otevřela	otevřít	k5eAaPmAgFnS	otevřít
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
cesta	cesta	k1gFnSc1	cesta
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
souhlasila	souhlasit	k5eAaImAgFnS	souhlasit
s	s	k7c7	s
umístěním	umístění	k1gNnSc7	umístění
cizích	cizí	k2eAgFnPc2d1	cizí
vojenských	vojenský	k2eAgFnPc2d1	vojenská
jednotek	jednotka	k1gFnPc2	jednotka
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
–	–	k?	–
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
oficiálně	oficiálně	k6eAd1	oficiálně
požádala	požádat	k5eAaPmAgFnS	požádat
síly	síla	k1gFnPc4	síla
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vstoupily	vstoupit	k5eAaPmAgInP	vstoupit
do	do	k7c2	do
království	království	k1gNnSc2	království
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
jeho	jeho	k3xOp3gFnSc2	jeho
obrany	obrana	k1gFnSc2	obrana
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
letky	letka	k1gFnPc1	letka
stíhačů	stíhač	k1gMnPc2	stíhač
odletěly	odletět	k5eAaPmAgFnP	odletět
na	na	k7c4	na
Blízký	blízký	k2eAgInSc4d1	blízký
východ	východ	k1gInSc4	východ
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Začala	začít	k5eAaPmAgFnS	začít
operace	operace	k1gFnSc1	operace
Pouštní	pouštní	k2eAgFnSc1d1	pouštní
štít	štít	k1gInSc1	štít
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
zabránit	zabránit	k5eAaPmF	zabránit
Saddámovi	Saddám	k1gMnSc3	Saddám
v	v	k7c6	v
další	další	k2eAgFnSc3d1	další
expanzi	expanze	k1gFnSc3	expanze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
zdaleka	zdaleka	k6eAd1	zdaleka
nebylo	být	k5eNaImAgNnS	být
jasno	jasno	k6eAd1	jasno
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
zvítězí	zvítězit	k5eAaPmIp3nS	zvítězit
diplomatické	diplomatický	k2eAgNnSc4d1	diplomatické
či	či	k8xC	či
vojenské	vojenský	k2eAgNnSc4d1	vojenské
řešení	řešení	k1gNnSc4	řešení
krize	krize	k1gFnSc2	krize
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
–	–	k?	–
Prezident	prezident	k1gMnSc1	prezident
Bush	Bush	k1gMnSc1	Bush
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
projevů	projev	k1gInPc2	projev
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
během	během	k7c2	během
(	(	kIx(	(
<g/>
řádově	řádově	k6eAd1	řádově
<g/>
)	)	kIx)	)
dnů	den	k1gInPc2	den
zaplavilo	zaplavit	k5eAaPmAgNnS	zaplavit
Kuvajt	Kuvajt	k1gInSc4	Kuvajt
120	[number]	k4	120
tisíc	tisíc	k4xCgInPc2	tisíc
mužů	muž	k1gMnPc2	muž
irácké	irácký	k2eAgFnSc2d1	irácká
pěchoty	pěchota	k1gFnSc2	pěchota
a	a	k8xC	a
850	[number]	k4	850
tanků	tank	k1gInPc2	tank
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
a	a	k8xC	a
začínají	začínat	k5eAaImIp3nP	začínat
ohrožovat	ohrožovat	k5eAaImF	ohrožovat
Saúdskou	saúdský	k2eAgFnSc4d1	Saúdská
Arábii	Arábie	k1gFnSc4	Arábie
<g/>
,	,	kIx,	,
k	k	k7c3	k
jejíž	jejíž	k3xOyRp3gFnSc3	jejíž
hranici	hranice	k1gFnSc3	hranice
se	se	k3xPyFc4	se
přiblížili	přiblížit	k5eAaPmAgMnP	přiblížit
<g/>
.	.	kIx.	.
</s>
<s>
Pentagon	Pentagon	k1gInSc1	Pentagon
prohlásil	prohlásit	k5eAaPmAgInS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
satelitní	satelitní	k2eAgInPc4d1	satelitní
snímky	snímek	k1gInPc4	snímek
iráckých	irácký	k2eAgFnPc2d1	irácká
jednotek	jednotka	k1gFnPc2	jednotka
poblíž	poblíž	k7c2	poblíž
hranic	hranice	k1gFnPc2	hranice
mezi	mezi	k7c7	mezi
Kuvajtem	Kuvajt	k1gInSc7	Kuvajt
a	a	k8xC	a
Saúdskou	saúdský	k2eAgFnSc7d1	Saúdská
Arábií	Arábie	k1gFnSc7	Arábie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
Prezident	prezident	k1gMnSc1	prezident
Bush	Bush	k1gMnSc1	Bush
se	se	k3xPyFc4	se
zasazuje	zasazovat	k5eAaImIp3nS	zasazovat
o	o	k7c4	o
vojenský	vojenský	k2eAgInSc4d1	vojenský
zásah	zásah	k1gInSc4	zásah
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podle	podle	k7c2	podle
průzkumů	průzkum	k1gInPc2	průzkum
veřejného	veřejný	k2eAgNnSc2d1	veřejné
mínění	mínění	k1gNnSc2	mínění
není	být	k5eNaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
veřejnost	veřejnost	k1gFnSc1	veřejnost
jednoznačně	jednoznačně	k6eAd1	jednoznačně
rozhodnuta	rozhodnout	k5eAaPmNgFnS	rozhodnout
pro	pro	k7c4	pro
nasazení	nasazení	k1gNnSc4	nasazení
svých	svůj	k3xOyFgFnPc2	svůj
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
ve	v	k7c6	v
slyšení	slyšení	k1gNnSc6	slyšení
před	před	k7c7	před
Kongresem	kongres	k1gInSc7	kongres
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
patnáctiletá	patnáctiletý	k2eAgFnSc1d1	patnáctiletá
dívka	dívka	k1gFnSc1	dívka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
popisovala	popisovat	k5eAaImAgFnS	popisovat
událost	událost	k1gFnSc4	událost
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc6	který
měli	mít	k5eAaImAgMnP	mít
iráčtí	irácký	k2eAgMnPc1d1	irácký
vojáci	voják	k1gMnPc1	voják
vtrhnout	vtrhnout	k5eAaPmF	vtrhnout
do	do	k7c2	do
kuvajtské	kuvajtský	k2eAgFnSc2d1	kuvajtská
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tato	tento	k3xDgFnSc1	tento
dívka	dívka	k1gFnSc1	dívka
měla	mít	k5eAaImAgFnS	mít
pracovat	pracovat	k5eAaImF	pracovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
zabíjet	zabíjet	k5eAaImF	zabíjet
novorozence	novorozenec	k1gMnPc4	novorozenec
v	v	k7c6	v
inkubátorech	inkubátor	k1gInPc6	inkubátor
<g/>
.	.	kIx.	.
</s>
<s>
Svědectví	svědectví	k1gNnSc1	svědectví
mělo	mít	k5eAaImAgNnS	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
veřejné	veřejný	k2eAgNnSc4d1	veřejné
mínění	mínění	k1gNnSc4	mínění
<g/>
,	,	kIx,	,
představitele	představitel	k1gMnSc2	představitel
Senátu	senát	k1gInSc2	senát
i	i	k8xC	i
následné	následný	k2eAgNnSc4d1	následné
schválení	schválení	k1gNnSc4	schválení
vojenského	vojenský	k2eAgInSc2d1	vojenský
zásahu	zásah	k1gInSc2	zásah
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
září	září	k1gNnSc6	září
1992	[number]	k4	1992
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
nepravdivé	pravdivý	k2eNgFnPc4d1	nepravdivá
informace	informace	k1gFnPc4	informace
<g/>
.	.	kIx.	.
</s>
<s>
Dívka	dívka	k1gFnSc1	dívka
byla	být	k5eAaImAgFnS	být
dcerou	dcera	k1gFnSc7	dcera
kuvajtského	kuvajtský	k2eAgMnSc2d1	kuvajtský
ambasadora	ambasador	k1gMnSc2	ambasador
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
v	v	k7c6	v
Kuvajtu	Kuvajt	k1gInSc6	Kuvajt
nebyla	být	k5eNaImAgFnS	být
a	a	k8xC	a
své	svůj	k3xOyFgNnSc4	svůj
vystoupení	vystoupení	k1gNnSc4	vystoupení
před	před	k7c7	před
Kongresem	kongres	k1gInSc7	kongres
konzultovala	konzultovat	k5eAaImAgFnS	konzultovat
s	s	k7c7	s
experty	expert	k1gMnPc7	expert
na	na	k7c4	na
public	publicum	k1gNnPc2	publicum
relations	relationsa	k1gFnPc2	relationsa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
Saddám	Saddám	k1gMnSc1	Saddám
Husajn	Husajn	k1gMnSc1	Husajn
přednesl	přednést	k5eAaPmAgMnS	přednést
prohlášení	prohlášení	k1gNnSc4	prohlášení
Revoluční	revoluční	k2eAgFnSc2d1	revoluční
velitelské	velitelský	k2eAgFnSc2d1	velitelská
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
ani	ani	k8xC	ani
nejmenší	malý	k2eAgFnSc4d3	nejmenší
možnost	možnost	k1gFnSc4	možnost
iráckého	irácký	k2eAgInSc2d1	irácký
odsunu	odsun	k1gInSc2	odsun
z	z	k7c2	z
Kuvajtu	Kuvajt	k1gInSc2	Kuvajt
a	a	k8xC	a
všechny	všechen	k3xTgInPc1	všechen
pokusy	pokus	k1gInPc1	pokus
vyhnat	vyhnat	k5eAaPmF	vyhnat
Irák	Irák	k1gInSc4	Irák
by	by	kYmCp3nP	by
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
"	"	kIx"	"
<g/>
matce	matka	k1gFnSc3	matka
bitev	bitva	k1gFnPc2	bitva
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
schválila	schválit	k5eAaPmAgFnS	schválit
celkem	celkem	k6eAd1	celkem
12	[number]	k4	12
rezolucí	rezoluce	k1gFnPc2	rezoluce
odsuzujících	odsuzující	k2eAgFnPc2d1	odsuzující
agresi	agrese	k1gFnSc4	agrese
Iráku	Irák	k1gInSc2	Irák
proti	proti	k7c3	proti
Kuvajtu	Kuvajt	k1gInSc3	Kuvajt
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
spolu	spolu	k6eAd1	spolu
poprvé	poprvé	k6eAd1	poprvé
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
všech	všecek	k3xTgMnPc2	všecek
pět	pět	k4xCc4	pět
stálých	stálý	k2eAgMnPc2d1	stálý
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
koalice	koalice	k1gFnSc1	koalice
28	[number]	k4	28
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
jednotky	jednotka	k1gFnPc1	jednotka
se	se	k3xPyFc4	se
vydaly	vydat	k5eAaPmAgFnP	vydat
do	do	k7c2	do
ohrožené	ohrožený	k2eAgFnSc2d1	ohrožená
oblasti	oblast	k1gFnSc2	oblast
v	v	k7c6	v
Perském	perský	k2eAgInSc6d1	perský
zálivu	záliv	k1gInSc6	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
300	[number]	k4	300
<g/>
tisícové	tisícový	k2eAgFnSc2d1	tisícová
koaliční	koaliční	k2eAgFnSc2d1	koaliční
armády	armáda	k1gFnSc2	armáda
byla	být	k5eAaImAgFnS	být
i	i	k9	i
čs	čs	kA	čs
<g/>
.	.	kIx.	.
protichemická	protichemický	k2eAgFnSc1d1	protichemická
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Faktickým	faktický	k2eAgNnSc7d1	faktické
ultimátem	ultimátum	k1gNnSc7	ultimátum
pro	pro	k7c4	pro
Saddáma	Saddám	k1gMnSc4	Saddám
byla	být	k5eAaImAgFnS	být
rezoluce	rezoluce	k1gFnSc1	rezoluce
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
č.	č.	k?	č.
678	[number]	k4	678
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1991	[number]	k4	1991
začala	začít	k5eAaPmAgFnS	začít
operace	operace	k1gFnSc1	operace
Pouštní	pouštní	k2eAgFnSc2d1	pouštní
bouře	bouř	k1gFnSc2	bouř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ocelový	ocelový	k2eAgInSc1d1	ocelový
déšť	déšť	k1gInSc1	déšť
===	===	k?	===
</s>
</p>
<p>
<s>
Vrchní	vrchní	k2eAgMnPc1d1	vrchní
velitel	velitel	k1gMnSc1	velitel
koaličních	koaliční	k2eAgFnPc2d1	koaliční
sil	síla	k1gFnPc2	síla
generál	generál	k1gMnSc1	generál
Norman	Norman	k1gMnSc1	Norman
Schwarzkopf	Schwarzkopf	k1gMnSc1	Schwarzkopf
měl	mít	k5eAaImAgMnS	mít
jasný	jasný	k2eAgInSc4d1	jasný
plán	plán	k1gInSc4	plán
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
cílem	cíl	k1gInSc7	cíl
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
nepřátelské	přátelský	k2eNgInPc4d1	nepřátelský
radarové	radarový	k2eAgInPc4d1	radarový
a	a	k8xC	a
protiletecké	protiletecký	k2eAgInPc4d1	protiletecký
systémy	systém	k1gInPc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Nepřítel	nepřítel	k1gMnSc1	nepřítel
nesmí	smět	k5eNaImIp3nS	smět
"	"	kIx"	"
<g/>
vidět	vidět	k5eAaImF	vidět
<g/>
"	"	kIx"	"
útočící	útočící	k2eAgNnPc1d1	útočící
letadla	letadlo	k1gNnPc1	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
hrozí	hrozit	k5eAaImIp3nS	hrozit
velké	velký	k2eAgFnPc4d1	velká
ztráty	ztráta	k1gFnPc4	ztráta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
17.1	[number]	k4	17.1
<g/>
.1991	.1991	k4	.1991
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
38	[number]	k4	38
-	-	kIx~	-
z	z	k7c2	z
vrtulníků	vrtulník	k1gInPc2	vrtulník
AH-64A	AH-64A	k1gMnPc2	AH-64A
Apache	Apache	k1gFnPc2	Apache
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
operují	operovat	k5eAaImIp3nP	operovat
nízko	nízko	k6eAd1	nízko
nad	nad	k7c7	nad
terénem	terén	k1gInSc7	terén
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
těžko	těžko	k6eAd1	těžko
detekovatelné	detekovatelný	k2eAgNnSc1d1	detekovatelné
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
vypálena	vypálen	k2eAgFnSc1d1	vypálena
baráž	baráž	k1gFnSc1	baráž
raket	raketa	k1gFnPc2	raketa
typu	typ	k1gInSc2	typ
AGM-114C	AGM-114C	k1gMnSc5	AGM-114C
Hellfire	Hellfir	k1gMnSc5	Hellfir
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
úkolem	úkol	k1gInSc7	úkol
bylo	být	k5eAaImAgNnS	být
zničit	zničit	k5eAaPmF	zničit
radary	radar	k1gInPc4	radar
včasného	včasný	k2eAgNnSc2d1	včasné
varování	varování	k1gNnSc2	varování
v	v	k7c6	v
jižním	jižní	k2eAgInSc6d1	jižní
Iráku	Irák	k1gInSc6	Irák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
-	-	kIx~	-
přesně	přesně	k6eAd1	přesně
v	v	k7c4	v
hodinu	hodina	k1gFnSc4	hodina
H	H	kA	H
"	"	kIx"	"
<g/>
neviditelné	viditelný	k2eNgInPc1d1	neviditelný
<g/>
"	"	kIx"	"
bombardéry	bombardér	k1gInPc1	bombardér
F-117A	F-117A	k1gFnSc1	F-117A
Nighthawk	Nighthawk	k1gInSc1	Nighthawk
vypouští	vypouštět	k5eAaImIp3nS	vypouštět
první	první	k4xOgInPc4	první
laserem	laser	k1gInSc7	laser
naváděné	naváděný	k2eAgFnPc4d1	naváděná
bomby	bomba	k1gFnPc4	bomba
na	na	k7c4	na
telekomunikační	telekomunikační	k2eAgNnSc4d1	telekomunikační
centrum	centrum	k1gNnSc4	centrum
v	v	k7c6	v
Bagdádu	Bagdád	k1gInSc6	Bagdád
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
-	-	kIx~	-
z	z	k7c2	z
lodí	loď	k1gFnPc2	loď
v	v	k7c6	v
Rudém	rudý	k2eAgNnSc6d1	Rudé
moři	moře	k1gNnSc6	moře
a	a	k8xC	a
Perském	perský	k2eAgInSc6d1	perský
zálivu	záliv	k1gInSc6	záliv
startují	startovat	k5eAaBmIp3nP	startovat
střely	střel	k1gInPc1	střel
Tomahawk	tomahawk	k1gInSc1	tomahawk
s	s	k7c7	s
plochou	plochý	k2eAgFnSc7d1	plochá
dráhou	dráha	k1gFnSc7	dráha
letu	let	k1gInSc2	let
</s>
</p>
<p>
<s>
byly	být	k5eAaImAgInP	být
také	také	k6eAd1	také
použity	použít	k5eAaPmNgInP	použít
klamné	klamný	k2eAgInPc1d1	klamný
cíle	cíl	k1gInPc1	cíl
BQM-74C	BQM-74C	k1gMnPc2	BQM-74C
Chukar	Chukara	k1gFnPc2	Chukara
pro	pro	k7c4	pro
krytí	krytí	k1gNnSc4	krytí
skutečných	skutečný	k2eAgFnPc2d1	skutečná
střel	střela	k1gFnPc2	střela
BGM-109	BGM-109	k1gFnSc1	BGM-109
Tomahawk	tomahawk	k1gInSc1	tomahawk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
několika	několik	k4yIc6	několik
případech	případ	k1gInPc6	případ
byly	být	k5eAaImAgInP	být
klamné	klamný	k2eAgInPc1d1	klamný
cíle	cíl	k1gInPc1	cíl
Chukar	Chukara	k1gFnPc2	Chukara
použity	použít	k5eAaPmNgInP	použít
pro	pro	k7c4	pro
odhalení	odhalení	k1gNnSc4	odhalení
radarů	radar	k1gInPc2	radar
protiletadlové	protiletadlový	k2eAgFnSc2d1	protiletadlová
obrany	obrana	k1gFnSc2	obrana
a	a	k8xC	a
následně	následně	k6eAd1	následně
zničeny	zničit	k5eAaPmNgInP	zničit
protiradarovými	protiradarový	k2eAgFnPc7d1	protiradarová
střelami	střela	k1gFnPc7	střela
AGM-88	AGM-88	k1gMnSc2	AGM-88
HARM	HARM	kA	HARM
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
pátého	pátý	k4xOgInSc2	pátý
dne	den	k1gInSc2	den
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
irácká	irácký	k2eAgFnSc1d1	irácká
protiletadlová	protiletadlový	k2eAgFnSc1d1	protiletadlová
obrana	obrana	k1gFnSc1	obrana
vyřazena	vyřadit	k5eAaPmNgFnS	vyřadit
z	z	k7c2	z
provozu	provoz	k1gInSc2	provoz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
začalo	začít	k5eAaPmAgNnS	začít
vytrvalé	vytrvalý	k2eAgNnSc1d1	vytrvalé
bombardování	bombardování	k1gNnSc1	bombardování
ve	v	k7c6	v
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
</s>
</p>
<p>
<s>
ocelový	ocelový	k2eAgInSc1d1	ocelový
déšť	déšť	k1gInSc1	déšť
padal	padat	k5eAaImAgInS	padat
na	na	k7c4	na
Irák	Irák	k1gInSc4	Irák
43	[number]	k4	43
dnů	den	k1gInPc2	den
</s>
</p>
<p>
<s>
===	===	k?	===
Pouštní	pouštní	k2eAgFnPc4d1	pouštní
bouře	bouř	k1gFnPc4	bouř
===	===	k?	===
</s>
</p>
<p>
<s>
Když	když	k8xS	když
USA	USA	kA	USA
získaly	získat	k5eAaPmAgFnP	získat
naprostou	naprostý	k2eAgFnSc4d1	naprostá
leteckou	letecký	k2eAgFnSc4d1	letecká
převahu	převaha	k1gFnSc4	převaha
nad	nad	k7c7	nad
Irákem	Irák	k1gInSc7	Irák
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
operace	operace	k1gFnSc1	operace
Pouštní	pouštní	k2eAgFnSc2d1	pouštní
bouře	bouř	k1gFnSc2	bouř
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
vyhnat	vyhnat	k5eAaPmF	vyhnat
irácké	irácký	k2eAgMnPc4d1	irácký
okupanty	okupant	k1gMnPc4	okupant
z	z	k7c2	z
Kuvajtu	Kuvajt	k1gInSc2	Kuvajt
<g/>
.	.	kIx.	.
</s>
<s>
Schwarzkopf	Schwarzkopf	k1gMnSc1	Schwarzkopf
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
toto	tento	k3xDgNnSc4	tento
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
<g/>
,	,	kIx,	,
naplánoval	naplánovat	k5eAaBmAgInS	naplánovat
masivní	masivní	k2eAgInSc1d1	masivní
obkličovací	obkličovací	k2eAgInSc1d1	obkličovací
manévr	manévr	k1gInSc1	manévr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Kuvajt	Kuvajt	k1gInSc1	Kuvajt
byly	být	k5eAaImAgFnP	být
provedeny	provést	k5eAaPmNgInP	provést
diverzní	diverzní	k2eAgInPc1d1	diverzní
útoky	útok	k1gInPc1	útok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavní	hlavní	k2eAgInSc1d1	hlavní
útok	útok	k1gInSc1	útok
směřoval	směřovat	k5eAaImAgInS	směřovat
přes	přes	k7c4	přes
saúdskoarabskou	saúdskoarabský	k2eAgFnSc4d1	saúdskoarabská
hranici	hranice	k1gFnSc4	hranice
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
sever	sever	k1gInSc4	sever
a	a	k8xC	a
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
stočil	stočit	k5eAaPmAgMnS	stočit
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
začal	začít	k5eAaPmAgInS	začít
pozemní	pozemní	k2eAgInSc1d1	pozemní
útok	útok	k1gInSc1	útok
mohutným	mohutný	k2eAgNnSc7d1	mohutné
leteckým	letecký	k2eAgNnSc7d1	letecké
a	a	k8xC	a
dělostřeleckým	dělostřelecký	k2eAgNnSc7d1	dělostřelecké
bombardováním	bombardování	k1gNnSc7	bombardování
Iráckých	irácký	k2eAgFnPc2d1	irácká
frontových	frontový	k2eAgFnPc2d1	frontová
linií	linie	k1gFnPc2	linie
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
americký	americký	k2eAgMnSc1d1	americký
17	[number]	k4	17
<g/>
.	.	kIx.	.
sbor	sbor	k1gInSc1	sbor
<g/>
,	,	kIx,	,
101	[number]	k4	101
<g/>
.	.	kIx.	.
výsadková	výsadkový	k2eAgFnSc1d1	výsadková
divize	divize	k1gFnSc1	divize
a	a	k8xC	a
francouzská	francouzský	k2eAgFnSc1d1	francouzská
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Daguetova	Daguetův	k2eAgFnSc1d1	Daguetův
divize	divize	k1gFnSc1	divize
pronikly	proniknout	k5eAaPmAgFnP	proniknout
hluboko	hluboko	k6eAd1	hluboko
na	na	k7c4	na
irácké	irácký	k2eAgNnSc4d1	irácké
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
za	za	k7c7	za
nimi	on	k3xPp3gMnPc7	on
postupoval	postupovat	k5eAaImAgMnS	postupovat
americký	americký	k2eAgMnSc1d1	americký
7	[number]	k4	7
<g/>
.	.	kIx.	.
sbor	sbor	k1gInSc1	sbor
včetně	včetně	k7c2	včetně
britské	britský	k2eAgFnSc2d1	britská
1	[number]	k4	1
<g/>
.	.	kIx.	.
obrněné	obrněný	k2eAgFnSc2d1	obrněná
divize	divize	k1gFnSc2	divize
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
stočila	stočit	k5eAaPmAgFnS	stočit
východním	východní	k2eAgInSc7d1	východní
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Kuvajt	Kuvajt	k1gInSc4	Kuvajt
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
americká	americký	k2eAgFnSc1d1	americká
námořní	námořní	k2eAgFnSc1d1	námořní
pěchota	pěchota	k1gFnSc1	pěchota
a	a	k8xC	a
pozemní	pozemní	k2eAgFnPc1d1	pozemní
jednotky	jednotka	k1gFnPc1	jednotka
arabských	arabský	k2eAgInPc2d1	arabský
států	stát	k1gInPc2	stát
postoupily	postoupit	k5eAaPmAgFnP	postoupit
přímo	přímo	k6eAd1	přímo
přes	přes	k7c4	přes
kuvajtskou	kuvajtský	k2eAgFnSc4d1	kuvajtská
hranici	hranice	k1gFnSc4	hranice
k	k	k7c3	k
hlavnímu	hlavní	k2eAgNnSc3d1	hlavní
městu	město	k1gNnSc3	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Úvodní	úvodní	k2eAgNnSc1d1	úvodní
spojenecké	spojenecký	k2eAgNnSc1d1	spojenecké
bombardování	bombardování	k1gNnSc1	bombardování
bylo	být	k5eAaImAgNnS	být
tak	tak	k6eAd1	tak
intenzivní	intenzivní	k2eAgNnSc1d1	intenzivní
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnozí	mnohý	k2eAgMnPc1d1	mnohý
iráčtí	irácký	k2eAgMnPc1d1	irácký
vojáci	voják	k1gMnPc1	voják
byli	být	k5eAaImAgMnP	být
zaživa	zaživa	k6eAd1	zaživa
pohřbeni	pohřben	k2eAgMnPc1d1	pohřben
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
bunkrech	bunkr	k1gInPc6	bunkr
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ostatní	ostatní	k2eAgMnPc1d1	ostatní
byli	být	k5eAaImAgMnP	být
tak	tak	k9	tak
otřeseni	otřesen	k2eAgMnPc1d1	otřesen
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
okamžitě	okamžitě	k6eAd1	okamžitě
vzdávali	vzdávat	k5eAaImAgMnP	vzdávat
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
dokonce	dokonce	k9	dokonce
bezpilotnímu	bezpilotní	k2eAgNnSc3d1	bezpilotní
průzkumnému	průzkumný	k2eAgNnSc3d1	průzkumné
letadlu	letadlo	k1gNnSc3	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Přístroje	přístroj	k1gInPc1	přístroj
pro	pro	k7c4	pro
noční	noční	k2eAgNnSc4d1	noční
vidění	vidění	k1gNnSc4	vidění
a	a	k8xC	a
granáty	granát	k1gInPc4	granát
s	s	k7c7	s
ochuzeným	ochuzený	k2eAgInSc7d1	ochuzený
uranem	uran	k1gInSc7	uran
dávaly	dávat	k5eAaImAgFnP	dávat
západním	západní	k2eAgFnPc3d1	západní
ozbrojeným	ozbrojený	k2eAgFnPc3d1	ozbrojená
silám	síla	k1gFnPc3	síla
naprostou	naprostý	k2eAgFnSc4d1	naprostá
převahu	převaha	k1gFnSc4	převaha
nad	nad	k7c7	nad
sovětskými	sovětský	k2eAgInPc7d1	sovětský
tanky	tank	k1gInPc7	tank
<g/>
,	,	kIx,	,
kterými	který	k3yIgFnPc7	který
disponovala	disponovat	k5eAaBmAgFnS	disponovat
irácká	irácký	k2eAgFnSc1d1	irácká
armáda	armáda	k1gFnSc1	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
někde	někde	k6eAd1	někde
spojenecké	spojenecký	k2eAgFnPc1d1	spojenecká
jednotky	jednotka	k1gFnPc1	jednotka
setkaly	setkat	k5eAaPmAgFnP	setkat
s	s	k7c7	s
odporem	odpor	k1gInSc7	odpor
<g/>
,	,	kIx,	,
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
ho	on	k3xPp3gNnSc4	on
podařilo	podařit	k5eAaPmAgNnS	podařit
zlomit	zlomit	k5eAaPmF	zlomit
<g/>
.	.	kIx.	.
</s>
<s>
Obávaná	obávaný	k2eAgFnSc1d1	obávaná
republikánská	republikánský	k2eAgFnSc1d1	republikánská
garda	garda	k1gFnSc1	garda
–	–	k?	–
elitní	elitní	k2eAgFnSc2d1	elitní
jednotky	jednotka	k1gFnSc2	jednotka
armády	armáda	k1gFnSc2	armáda
Saddáma	Saddám	k1gMnSc2	Saddám
Husajna	Husajn	k1gMnSc2	Husajn
byla	být	k5eAaImAgFnS	být
rychle	rychle	k6eAd1	rychle
obklíčena	obklíčit	k5eAaPmNgFnS	obklíčit
a	a	k8xC	a
bombardována	bombardovat	k5eAaImNgFnS	bombardovat
tak	tak	k6eAd1	tak
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
se	se	k3xPyFc4	se
vzdala	vzdát	k5eAaPmAgFnS	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Operace	operace	k1gFnSc1	operace
pouštní	pouštní	k2eAgFnSc2d1	pouštní
bouře	bouř	k1gFnSc2	bouř
skončila	skončit	k5eAaPmAgFnS	skončit
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
a	a	k8xC	a
Kuvajt	Kuvajt	k1gInSc1	Kuvajt
byl	být	k5eAaImAgInS	být
osvobozen	osvobodit	k5eAaPmNgInS	osvobodit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Role	role	k1gFnPc4	role
médií	médium	k1gNnPc2	médium
==	==	k?	==
</s>
</p>
<p>
<s>
Válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Zálivu	záliv	k1gInSc6	záliv
je	být	k5eAaImIp3nS	být
specifická	specifický	k2eAgFnSc1d1	specifická
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jakou	jaký	k3yQgFnSc4	jaký
úlohu	úloha	k1gFnSc4	úloha
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
sehrála	sehrát	k5eAaPmAgNnP	sehrát
média	médium	k1gNnPc1	médium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
televizi	televize	k1gFnSc6	televize
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
objevovaly	objevovat	k5eAaImAgInP	objevovat
záběry	záběr	k1gInPc1	záběr
z	z	k7c2	z
válčení	válčení	k1gNnSc2	válčení
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
však	však	k9	však
především	především	k9	především
o	o	k7c4	o
odosobněné	odosobněný	k2eAgInPc4d1	odosobněný
záznamy	záznam	k1gInPc4	záznam
naváděných	naváděný	k2eAgFnPc2d1	naváděná
raket	raketa	k1gFnPc2	raketa
nebo	nebo	k8xC	nebo
nočních	noční	k2eAgInPc2d1	noční
bojů	boj	k1gInPc2	boj
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgFnPc6	jenž
diváci	divák	k1gMnPc1	divák
nemohli	moct	k5eNaImAgMnP	moct
spatřit	spatřit	k5eAaPmF	spatřit
umírající	umírající	k2eAgMnPc4d1	umírající
vojáky	voják	k1gMnPc4	voják
nebo	nebo	k8xC	nebo
civilisty	civilista	k1gMnPc4	civilista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pentagon	Pentagon	k1gInSc1	Pentagon
po	po	k7c4	po
zkušenosti	zkušenost	k1gFnPc4	zkušenost
s	s	k7c7	s
vlivem	vliv	k1gInSc7	vliv
médií	médium	k1gNnPc2	médium
na	na	k7c4	na
průběh	průběh	k1gInSc4	průběh
války	válka	k1gFnSc2	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
přehodnotil	přehodnotit	k5eAaPmAgMnS	přehodnotit
situaci	situace	k1gFnSc4	situace
a	a	k8xC	a
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
převzal	převzít	k5eAaPmAgMnS	převzít
iniciativu	iniciativa	k1gFnSc4	iniciativa
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
přicházel	přicházet	k5eAaImAgMnS	přicházet
médiím	médium	k1gNnPc3	médium
a	a	k8xC	a
novinářům	novinář	k1gMnPc3	novinář
vstříc	vstříc	k6eAd1	vstříc
–	–	k?	–
například	například	k6eAd1	například
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
tzv.	tzv.	kA	tzv.
embedded	embedded	k1gInSc1	embedded
journalismu	journalismus	k1gInSc2	journalismus
nebo	nebo	k8xC	nebo
konáním	konání	k1gNnSc7	konání
brífingů	brífing	k1gInPc2	brífing
a	a	k8xC	a
tiskových	tiskový	k2eAgFnPc2d1	tisková
konferencí	konference	k1gFnPc2	konference
–	–	k?	–
mnoho	mnoho	k4c1	mnoho
zpráv	zpráva	k1gFnPc2	zpráva
tak	tak	k6eAd1	tak
pocházelo	pocházet	k5eAaImAgNnS	pocházet
právě	právě	k9	právě
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
,	,	kIx,	,
pořádaných	pořádaný	k2eAgFnPc2d1	pořádaná
přímo	přímo	k6eAd1	přímo
armádou	armáda	k1gFnSc7	armáda
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
z	z	k7c2	z
vlastní	vlastní	k2eAgFnSc2d1	vlastní
zkušenosti	zkušenost	k1gFnSc2	zkušenost
novinářů	novinář	k1gMnPc2	novinář
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
podrobnějších	podrobný	k2eAgFnPc2d2	podrobnější
zpravodajských	zpravodajský	k2eAgFnPc2d1	zpravodajská
a	a	k8xC	a
komentovaných	komentovaný	k2eAgFnPc6d1	komentovaná
relacích	relace	k1gFnPc6	relace
o	o	k7c6	o
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
Zálivu	záliv	k1gInSc6	záliv
se	se	k3xPyFc4	se
objevovala	objevovat	k5eAaImAgFnS	objevovat
řada	řada	k1gFnSc1	řada
poradců	poradce	k1gMnPc2	poradce
Pentagonu	Pentagon	k1gInSc2	Pentagon
coby	coby	k?	coby
respondenti	respondent	k1gMnPc1	respondent
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
zodpovídali	zodpovídat	k5eAaPmAgMnP	zodpovídat
otázky	otázka	k1gFnPc4	otázka
moderátorů	moderátor	k1gMnPc2	moderátor
<g/>
,	,	kIx,	,
a	a	k8xC	a
diváky	divák	k1gMnPc4	divák
byli	být	k5eAaImAgMnP	být
vnímáni	vnímán	k2eAgMnPc1d1	vnímán
jako	jako	k8xC	jako
autorita	autorita	k1gFnSc1	autorita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Izrael	Izrael	k1gInSc1	Izrael
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
napadení	napadení	k1gNnSc6	napadení
Irák	Irák	k1gInSc1	Irák
zareagoval	zareagovat	k5eAaPmAgInS	zareagovat
odpálením	odpálení	k1gNnSc7	odpálení
raket	raketa	k1gFnPc2	raketa
Scud	Scuda	k1gFnPc2	Scuda
částečně	částečně	k6eAd1	částečně
na	na	k7c4	na
spojenecké	spojenecký	k2eAgFnPc4d1	spojenecká
základny	základna	k1gFnPc4	základna
v	v	k7c6	v
Saúdské	saúdský	k2eAgFnSc6d1	Saúdská
Arábii	Arábie	k1gFnSc6	Arábie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
území	území	k1gNnSc6	území
Izraele	Izrael	k1gInSc2	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Izrael	Izrael	k1gInSc1	Izrael
vojensky	vojensky	k6eAd1	vojensky
zareaguje	zareagovat	k5eAaPmIp3nS	zareagovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
by	by	kYmCp3nP	by
mohlo	moct	k5eAaImAgNnS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
rozbití	rozbití	k1gNnSc3	rozbití
koalice	koalice	k1gFnSc2	koalice
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Arabové	Arab	k1gMnPc1	Arab
by	by	kYmCp3nP	by
mohli	moct	k5eAaImAgMnP	moct
mít	mít	k5eAaImF	mít
problém	problém	k1gInSc4	problém
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
boji	boj	k1gInSc6	boj
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Židů	Žid	k1gMnPc2	Žid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
svých	svůj	k3xOyFgMnPc2	svůj
spojenců	spojenec	k1gMnPc2	spojenec
dostal	dostat	k5eAaPmAgInS	dostat
de	de	k?	de
facto	fact	k2eAgNnSc4d1	facto
ultimátum	ultimátum	k1gNnSc4	ultimátum
nechat	nechat	k5eAaPmF	nechat
na	na	k7c4	na
jeho	jeho	k3xOp3gNnPc4	jeho
města	město	k1gNnPc4	město
dopadat	dopadat	k5eAaImF	dopadat
rakety	raketa	k1gFnPc4	raketa
Scud	Scuda	k1gFnPc2	Scuda
<g/>
,	,	kIx,	,
a	a	k8xC	a
nepodnikat	podnikat	k5eNaImF	podnikat
nic	nic	k3yNnSc1	nic
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
příčilo	příčit	k5eAaImAgNnS	příčit
celé	celý	k2eAgFnSc3d1	celá
dosavadní	dosavadní	k2eAgFnSc3d1	dosavadní
izraelské	izraelský	k2eAgFnSc3d1	izraelská
strategii	strategie	k1gFnSc3	strategie
<g/>
.	.	kIx.	.
</s>
<s>
Diplomatická	diplomatický	k2eAgNnPc1d1	diplomatické
jednání	jednání	k1gNnPc1	jednání
byla	být	k5eAaImAgNnP	být
velmi	velmi	k6eAd1	velmi
složitá	složitý	k2eAgNnPc1d1	složité
<g/>
.	.	kIx.	.
</s>
<s>
Misku	miska	k1gFnSc4	miska
vah	váha	k1gFnPc2	váha
převážily	převážit	k5eAaPmAgFnP	převážit
až	až	k9	až
americké	americký	k2eAgFnPc1d1	americká
baterie	baterie	k1gFnPc1	baterie
obranných	obranný	k2eAgFnPc2d1	obranná
protiraketových	protiraketový	k2eAgFnPc2d1	protiraketová
střel	střela	k1gFnPc2	střela
MIM-104C	MIM-104C	k1gMnSc1	MIM-104C
Patriot	patriot	k1gMnSc1	patriot
PAC-	PAC-	k1gMnSc1	PAC-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
Saddámův	Saddámův	k2eAgInSc1d1	Saddámův
plán	plán	k1gInSc1	plán
nevyšel	vyjít	k5eNaPmAgInS	vyjít
a	a	k8xC	a
Izrael	Izrael	k1gInSc1	Izrael
si	se	k3xPyFc3	se
zachoval	zachovat	k5eAaPmAgInS	zachovat
neutralitu	neutralita	k1gFnSc4	neutralita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dohra	dohra	k1gFnSc1	dohra
==	==	k?	==
</s>
</p>
<p>
<s>
Úspěch	úspěch	k1gInSc1	úspěch
nepřinesly	přinést	k5eNaPmAgFnP	přinést
ani	ani	k8xC	ani
obranné	obranný	k2eAgFnPc1d1	obranná
či	či	k8xC	či
lokální	lokální	k2eAgFnPc1d1	lokální
útočné	útočný	k2eAgFnPc1d1	útočná
operace	operace	k1gFnPc1	operace
irácké	irácký	k2eAgFnSc2d1	irácká
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
ploché	plochý	k2eAgFnSc6d1	plochá
poušti	poušť	k1gFnSc6	poušť
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
rozdrcena	rozdrtit	k5eAaPmNgFnS	rozdrtit
zejména	zejména	k9	zejména
leteckými	letecký	k2eAgInPc7d1	letecký
údery	úder	k1gInPc7	úder
<g/>
.	.	kIx.	.
</s>
<s>
Husajn	Husajn	k1gMnSc1	Husajn
následně	následně	k6eAd1	následně
nařídil	nařídit	k5eAaPmAgMnS	nařídit
zapálit	zapálit	k5eAaPmF	zapálit
ropné	ropný	k2eAgInPc4d1	ropný
vrty	vrt	k1gInPc4	vrt
v	v	k7c6	v
Kuvajtu	Kuvajt	k1gInSc6	Kuvajt
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
vypustit	vypustit	k5eAaPmF	vypustit
ropu	ropa	k1gFnSc4	ropa
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
(	(	kIx(	(
<g/>
tím	ten	k3xDgNnSc7	ten
způsobil	způsobit	k5eAaPmAgMnS	způsobit
velké	velký	k2eAgFnSc2d1	velká
ekologické	ekologický	k2eAgFnSc2d1	ekologická
a	a	k8xC	a
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
škody	škoda	k1gFnSc2	škoda
<g/>
)	)	kIx)	)
a	a	k8xC	a
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
své	svůj	k3xOyFgFnPc4	svůj
nejlepší	dobrý	k2eAgFnPc4d3	nejlepší
jednotky	jednotka	k1gFnPc4	jednotka
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
Republikánskou	republikánský	k2eAgFnSc4d1	republikánská
gardu	garda	k1gFnSc4	garda
<g/>
)	)	kIx)	)
pryč	pryč	k6eAd1	pryč
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vyhnul	vyhnout	k5eAaPmAgMnS	vyhnout
jejímu	její	k3xOp3gMnSc3	její
zničení	zničení	k1gNnSc4	zničení
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
její	její	k3xOp3gFnSc7	její
pomocí	pomoc	k1gFnSc7	pomoc
pak	pak	k6eAd1	pak
potlačil	potlačit	k5eAaPmAgMnS	potlačit
povstání	povstání	k1gNnPc4	povstání
šíitů	šíita	k1gMnPc2	šíita
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
Američané	Američan	k1gMnPc1	Američan
vyprovokovali	vyprovokovat	k5eAaPmAgMnP	vyprovokovat
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
mu	on	k3xPp3gNnSc3	on
pak	pak	k6eAd1	pak
poskytli	poskytnout	k5eAaPmAgMnP	poskytnout
slibovanou	slibovaný	k2eAgFnSc4d1	slibovaná
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Válka	válka	k1gFnSc1	válka
skončila	skončit	k5eAaPmAgFnS	skončit
osvobozením	osvobození	k1gNnSc7	osvobození
Kuvajtu	Kuvajt	k1gInSc2	Kuvajt
<g/>
.	.	kIx.	.
</s>
<s>
Američané	Američan	k1gMnPc1	Američan
ani	ani	k8xC	ani
jejich	jejich	k3xOp3gMnPc1	jejich
spojenci	spojenec	k1gMnPc1	spojenec
se	se	k3xPyFc4	se
neodhodlali	odhodlat	k5eNaPmAgMnP	odhodlat
ke	k	k7c3	k
svržení	svržení	k1gNnSc3	svržení
Husajna	Husajn	k1gMnSc2	Husajn
(	(	kIx(	(
<g/>
ke	k	k7c3	k
kterému	který	k3yQgInSc3	který
ovšem	ovšem	k9	ovšem
neměli	mít	k5eNaImAgMnP	mít
mandát	mandát	k1gInSc4	mandát
od	od	k7c2	od
OSN	OSN	kA	OSN
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
svržení	svržení	k1gNnSc4	svržení
(	(	kIx(	(
<g/>
též	též	k9	též
bez	bez	k7c2	bez
mandátu	mandát	k1gInSc2	mandát
OSN	OSN	kA	OSN
<g/>
)	)	kIx)	)
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
pozemní	pozemní	k2eAgInPc1d1	pozemní
boje	boj	k1gInPc1	boj
byly	být	k5eAaImAgInP	být
sváděny	svádět	k5eAaImNgInP	svádět
především	především	k6eAd1	především
v	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
pouštní	pouštní	k2eAgFnSc6d1	pouštní
krajině	krajina	k1gFnSc6	krajina
<g/>
,	,	kIx,	,
ztráty	ztráta	k1gFnPc1	ztráta
na	na	k7c6	na
životech	život	k1gInPc6	život
nebyly	být	k5eNaImAgFnP	být
zdaleka	zdaleka	k6eAd1	zdaleka
takové	takový	k3xDgFnPc1	takový
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
spojenečtí	spojenecký	k2eAgMnPc1d1	spojenecký
plánovači	plánovač	k1gMnPc1	plánovač
obávali	obávat	k5eAaImAgMnP	obávat
<g/>
.	.	kIx.	.
</s>
<s>
Generál	generál	k1gMnSc1	generál
Schwarzkopf	Schwarzkopf	k1gMnSc1	Schwarzkopf
už	už	k6eAd1	už
při	při	k7c6	při
svém	svůj	k3xOyFgNnSc6	svůj
jmenování	jmenování	k1gNnSc6	jmenování
striktně	striktně	k6eAd1	striktně
trval	trvat	k5eAaImAgInS	trvat
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
bojové	bojový	k2eAgFnPc4d1	bojová
operace	operace	k1gFnPc4	operace
bude	být	k5eAaImBp3nS	být
řídit	řídit	k5eAaImF	řídit
on	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
štábem	štáb	k1gInSc7	štáb
<g/>
,	,	kIx,	,
a	a	k8xC	a
ne	ne	k9	ne
Bílý	bílý	k2eAgInSc1d1	bílý
dům	dům	k1gInSc1	dům
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ztráty	ztráta	k1gFnSc2	ztráta
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
straně	strana	k1gFnSc6	strana
spojenců	spojenec	k1gMnPc2	spojenec
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
kolem	kolem	k7c2	kolem
400	[number]	k4	400
mrtvých	mrtvý	k1gMnPc2	mrtvý
a	a	k8xC	a
necelých	celý	k2eNgInPc2d1	necelý
1000	[number]	k4	1000
zraněných	zraněný	k1gMnPc2	zraněný
<g/>
,	,	kIx,	,
odhady	odhad	k1gInPc4	odhad
iráckých	irácký	k2eAgFnPc2d1	irácká
ztrát	ztráta	k1gFnPc2	ztráta
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
rozcházejí	rozcházet	k5eAaImIp3nP	rozcházet
<g/>
.	.	kIx.	.
</s>
<s>
Nejmenší	malý	k2eAgInPc1d3	nejmenší
odhady	odhad	k1gInPc1	odhad
docházejí	docházet	k5eAaImIp3nP	docházet
k	k	k7c3	k
1500	[number]	k4	1500
obětí	oběť	k1gFnPc2	oběť
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnPc1d3	veliký
uvádějí	uvádět	k5eAaImIp3nP	uvádět
až	až	k9	až
200	[number]	k4	200
000	[number]	k4	000
obětí	oběť	k1gFnPc2	oběť
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
25	[number]	k4	25
000	[number]	k4	000
-	-	kIx~	-
75	[number]	k4	75
000	[number]	k4	000
obětí	oběť	k1gFnPc2	oběť
mezi	mezi	k7c7	mezi
vojáky	voják	k1gMnPc7	voják
a	a	k8xC	a
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInSc4	tisíc
mezi	mezi	k7c7	mezi
civilním	civilní	k2eAgNnSc7d1	civilní
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
odhady	odhad	k1gInPc1	odhad
uvádějí	uvádět	k5eAaImIp3nP	uvádět
až	až	k9	až
35	[number]	k4	35
000	[number]	k4	000
civilních	civilní	k2eAgFnPc2d1	civilní
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
spojeneckého	spojenecký	k2eAgNnSc2d1	spojenecké
zajetí	zajetí	k1gNnSc2	zajetí
padlo	padnout	k5eAaPmAgNnS	padnout
na	na	k7c4	na
71	[number]	k4	71
000	[number]	k4	000
příslušníků	příslušník	k1gMnPc2	příslušník
irácké	irácký	k2eAgFnSc2d1	irácká
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ztráty	ztráta	k1gFnPc1	ztráta
vzdušných	vzdušný	k2eAgFnPc2d1	vzdušná
sil	síla	k1gFnPc2	síla
===	===	k?	===
</s>
</p>
<p>
<s>
Celkem	celkem	k6eAd1	celkem
ztratila	ztratit	k5eAaPmAgFnS	ztratit
koalice	koalice	k1gFnSc1	koalice
přes	přes	k7c4	přes
70	[number]	k4	70
letadel	letadlo	k1gNnPc2	letadlo
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
vlivem	vliv	k1gInSc7	vliv
irácké	irácký	k2eAgFnSc2d1	irácká
pozemní	pozemní	k2eAgFnSc2d1	pozemní
PVO	PVO	kA	PVO
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Američané	Američan	k1gMnPc1	Američan
ztratili	ztratit	k5eAaPmAgMnP	ztratit
během	během	k7c2	během
kampaně	kampaň	k1gFnSc2	kampaň
<g/>
:	:	kIx,	:
3	[number]	k4	3
<g/>
x	x	k?	x
F-	F-	k1gFnPc2	F-
<g/>
16	[number]	k4	16
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
x	x	k?	x
F-	F-	k1gFnPc2	F-
<g/>
14	[number]	k4	14
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
x	x	k?	x
F-	F-	k1gFnPc2	F-
<g/>
15	[number]	k4	15
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
x	x	k?	x
F-	F-	k1gFnPc2	F-
<g/>
18	[number]	k4	18
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
x	x	k?	x
A-10	A-10	k1gFnPc2	A-10
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
typů	typ	k1gInPc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
ztráty	ztráta	k1gFnPc1	ztráta
jsou	být	k5eAaImIp3nP	být
obecně	obecně	k6eAd1	obecně
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
velice	velice	k6eAd1	velice
nízké	nízký	k2eAgNnSc1d1	nízké
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
koalice	koalice	k1gFnSc1	koalice
ztratí	ztratit	k5eAaPmIp3nS	ztratit
jenom	jenom	k9	jenom
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
dnech	den	k1gInPc6	den
války	válka	k1gFnSc2	válka
přes	přes	k7c4	přes
100	[number]	k4	100
letadel	letadlo	k1gNnPc2	letadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Irácké	irácký	k2eAgNnSc1d1	irácké
letectvo	letectvo	k1gNnSc1	letectvo
mělo	mít	k5eAaImAgNnS	mít
ztráty	ztráta	k1gFnPc4	ztráta
mnohem	mnohem	k6eAd1	mnohem
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
,	,	kIx,	,
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
bojů	boj	k1gInPc2	boj
přestalo	přestat	k5eAaPmAgNnS	přestat
prakticky	prakticky	k6eAd1	prakticky
existovat	existovat	k5eAaImF	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
vzdušných	vzdušný	k2eAgInPc2d1	vzdušný
soubojů	souboj	k1gInPc2	souboj
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
13	[number]	k4	13
letadel	letadlo	k1gNnPc2	letadlo
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
stovky	stovka	k1gFnPc4	stovka
strojů	stroj	k1gInPc2	stroj
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
příští	příští	k2eAgFnSc2d1	příští
války	válka	k1gFnSc2	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
již	již	k6eAd1	již
nezasáhlo	zasáhnout	k5eNaPmAgNnS	zasáhnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Irácké	irácký	k2eAgNnSc1d1	irácké
letectvo	letectvo	k1gNnSc1	letectvo
si	se	k3xPyFc3	se
může	moct	k5eAaImIp3nS	moct
nárokovat	nárokovat	k5eAaImF	nárokovat
pouze	pouze	k6eAd1	pouze
jediné	jediný	k2eAgNnSc4d1	jediné
vzdušné	vzdušný	k2eAgNnSc4d1	vzdušné
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c4	v
první	první	k4xOgInSc4	první
den	den	k1gInSc4	den
války	válka	k1gFnSc2	válka
irácký	irácký	k2eAgMnSc1d1	irácký
Mig-	Mig-	k1gMnSc1	Mig-
<g/>
25	[number]	k4	25
sestřelil	sestřelit	k5eAaPmAgMnS	sestřelit
americkou	americký	k2eAgFnSc4d1	americká
stíhačku	stíhačka	k1gFnSc4	stíhačka
F-	F-	k1gFnSc2	F-
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Důsledky	důsledek	k1gInPc1	důsledek
války	válka	k1gFnSc2	válka
==	==	k?	==
</s>
</p>
<p>
<s>
Cena	cena	k1gFnSc1	cena
ropy	ropa	k1gFnSc2	ropa
během	během	k7c2	během
války	válka	k1gFnSc2	válka
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
z	z	k7c2	z
13	[number]	k4	13
na	na	k7c4	na
40	[number]	k4	40
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
barel	barel	k1gInSc4	barel
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
zlata	zlato	k1gNnSc2	zlato
na	na	k7c6	na
burzách	burza	k1gFnPc6	burza
se	se	k3xPyFc4	se
dočasně	dočasně	k6eAd1	dočasně
zdvojnásobila	zdvojnásobit	k5eAaPmAgFnS	zdvojnásobit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spojenecká	spojenecký	k2eAgNnPc1d1	spojenecké
vojska	vojsko	k1gNnPc1	vojsko
vystřílela	vystřílet	k5eAaPmAgNnP	vystřílet
nebo	nebo	k8xC	nebo
svrhla	svrhnout	k5eAaPmAgFnS	svrhnout
okolo	okolo	k7c2	okolo
325	[number]	k4	325
tun	tuna	k1gFnPc2	tuna
munice	munice	k1gFnSc2	munice
a	a	k8xC	a
bomb	bomba	k1gFnPc2	bomba
z	z	k7c2	z
ochuzeného	ochuzený	k2eAgInSc2d1	ochuzený
uranu	uran	k1gInSc2	uran
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
OSN	OSN	kA	OSN
na	na	k7c4	na
Irák	Irák	k1gInSc4	Irák
uplatnila	uplatnit	k5eAaPmAgFnS	uplatnit
ekonomické	ekonomický	k2eAgFnPc4d1	ekonomická
sankce	sankce	k1gFnPc4	sankce
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
trvaly	trvat	k5eAaImAgFnP	trvat
od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1990	[number]	k4	1990
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
druhé	druhý	k4xOgFnSc2	druhý
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Zálivu	záliv	k1gInSc6	záliv
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2003	[number]	k4	2003
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
země	zem	k1gFnSc2	zem
byly	být	k5eAaImAgFnP	být
zřízeny	zřídit	k5eAaPmNgFnP	zřídit
bezletové	bezletový	k2eAgFnPc1d1	bezletová
zóny	zóna	k1gFnPc1	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Saddámova	Saddámův	k2eAgFnSc1d1	Saddámova
armádní	armádní	k2eAgFnSc1d1	armádní
technika	technika	k1gFnSc1	technika
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
prakticky	prakticky	k6eAd1	prakticky
zničena	zničit	k5eAaPmNgFnS	zničit
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nedosáhla	dosáhnout	k5eNaPmAgFnS	dosáhnout
stavů	stav	k1gInPc2	stav
a	a	k8xC	a
síly	síla	k1gFnPc4	síla
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
(	(	kIx(	(
<g/>
UNEP	UNEP	kA	UNEP
<g/>
)	)	kIx)	)
došlo	dojít	k5eAaPmAgNnS	dojít
během	během	k7c2	během
války	válka	k1gFnSc2	válka
při	při	k7c6	při
uvolnění	uvolnění	k1gNnSc6	uvolnění
15	[number]	k4	15
000	[number]	k4	000
tun	tuna	k1gFnPc2	tuna
zplodin	zplodina	k1gFnPc2	zplodina
z	z	k7c2	z
ropných	ropný	k2eAgInPc2d1	ropný
vrtů	vrt	k1gInPc2	vrt
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
k	k	k7c3	k
"	"	kIx"	"
<g/>
rozsáhlé	rozsáhlý	k2eAgFnSc3d1	rozsáhlá
ekologické	ekologický	k2eAgFnSc3d1	ekologická
katastrofě	katastrofa	k1gFnSc3	katastrofa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Armáda	armáda	k1gFnSc1	armáda
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
vybudovala	vybudovat	k5eAaPmAgFnS	vybudovat
vojenské	vojenský	k2eAgFnPc4d1	vojenská
základny	základna	k1gFnPc4	základna
v	v	k7c6	v
Saúdské	saúdský	k2eAgFnSc6d1	Saúdská
Arábii	Arábie	k1gFnSc6	Arábie
a	a	k8xC	a
Kuvajtu	Kuvajt	k1gInSc6	Kuvajt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Část	část	k1gFnSc1	část
veteránů	veterán	k1gMnPc2	veterán
z	z	k7c2	z
Války	válka	k1gFnSc2	válka
v	v	k7c6	v
Perském	perský	k2eAgInSc6d1	perský
zálivu	záliv	k1gInSc6	záliv
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pěchota	pěchota	k1gFnSc1	pěchota
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
pohybovala	pohybovat	k5eAaImAgFnS	pohybovat
mezi	mezi	k7c7	mezi
zapálenými	zapálený	k2eAgFnPc7d1	zapálená
kuvajtskými	kuvajtský	k2eAgFnPc7d1	kuvajtská
ropnými	ropný	k2eAgNnPc7d1	ropné
poli	pole	k1gNnPc7	pole
<g/>
)	)	kIx)	)
trpí	trpět	k5eAaImIp3nS	trpět
psychosomatickými	psychosomatický	k2eAgInPc7d1	psychosomatický
<g/>
,	,	kIx,	,
respiračními	respirační	k2eAgInPc7d1	respirační
<g/>
,	,	kIx,	,
imunitními	imunitní	k2eAgInPc7d1	imunitní
<g/>
,	,	kIx,	,
psychickými	psychický	k2eAgFnPc7d1	psychická
či	či	k8xC	či
jinými	jiný	k2eAgFnPc7d1	jiná
poruchami	porucha	k1gFnPc7	porucha
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
syndrom	syndrom	k1gInSc1	syndrom
Války	válka	k1gFnSc2	válka
v	v	k7c6	v
zálivu	záliv	k1gInSc6	záliv
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
MAROLDA	MAROLDA	kA	MAROLDA
<g/>
,	,	kIx,	,
Edward	Edward	k1gMnSc1	Edward
J.	J.	kA	J.
<g/>
;	;	kIx,	;
SCHNELLER	SCHNELLER	kA	SCHNELLER
JR	JR	kA	JR
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
J.	J.	kA	J.
Shield	Shield	k1gMnSc1	Shield
and	and	k?	and
sword	sword	k1gMnSc1	sword
:	:	kIx,	:
the	the	k?	the
United	United	k1gMnSc1	United
States	States	k1gMnSc1	States
Navy	Navy	k?	Navy
and	and	k?	and
the	the	k?	the
Persian	Persian	k1gMnSc1	Persian
Gulf	Gulf	k1gMnSc1	Gulf
War	War	k1gMnSc1	War
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Naval	navalit	k5eAaPmRp2nS	navalit
Institute	institut	k1gInSc5	institut
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
544	[number]	k4	544
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
9781557504852	[number]	k4	9781557504852
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Irácko-íránská	irácko-íránský	k2eAgFnSc1d1	irácko-íránská
válka	válka	k1gFnSc1	válka
</s>
</p>
<p>
<s>
Dálnice	dálnice	k1gFnSc1	dálnice
smrti	smrt	k1gFnSc2	smrt
</s>
</p>
<p>
<s>
Válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
</s>
</p>
<p>
<s>
Saddám	Saddám	k1gMnSc1	Saddám
Husajn	Husajn	k1gMnSc1	Husajn
</s>
</p>
<p>
<s>
Norman	Norman	k1gMnSc1	Norman
Schwarzkopf	Schwarzkopf	k1gMnSc1	Schwarzkopf
</s>
</p>
<p>
<s>
Válka	válka	k1gFnSc1	válka
proti	proti	k7c3	proti
terorismu	terorismus	k1gInSc3	terorismus
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Zálivu	záliv	k1gInSc6	záliv
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Zálivu	záliv	k1gInSc6	záliv
</s>
</p>
<p>
<s>
POUŠTNÍ	pouštní	k2eAgFnSc1d1	pouštní
BOUŘE	bouře	k1gFnSc1	bouře
1991	[number]	k4	1991
</s>
</p>
<p>
<s>
Zpravodaj	zpravodaj	k1gMnSc1	zpravodaj
SISYFOS	Sisyfos	k1gMnSc1	Sisyfos
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
2005	[number]	k4	2005
-	-	kIx~	-
viz	vidět	k5eAaImRp2nS	vidět
Jiří	Jiří	k1gMnSc1	Jiří
Heřt	Heřt	k1gMnSc1	Heřt
<g/>
:	:	kIx,	:
SYNDROM	syndrom	k1gInSc1	syndrom
"	"	kIx"	"
<g/>
VÁLKY	válka	k1gFnPc1	válka
V	v	k7c6	v
ZÁLIVU	záliv	k1gInSc6	záliv
<g/>
"	"	kIx"	"
</s>
</p>
