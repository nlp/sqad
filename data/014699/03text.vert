<s>
Český	český	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
(	(	kIx(
<g/>
nádraží	nádraží	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Český	český	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
Nádraží	nádraží	k1gNnSc2
v	v	k7c6
Českém	český	k2eAgInSc6d1
KrumlověStát	KrumlověStát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Kraj	kraj	k7c2
</s>
<s>
Jihočeský	jihočeský	k2eAgInSc1d1
Město	město	k1gNnSc1
</s>
<s>
Český	český	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
48	#num#	k4
<g/>
°	°	k?
<g/>
49	#num#	k4
<g/>
′	′	k?
<g/>
19,92	19,92	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
19	#num#	k4
<g/>
′	′	k?
<g/>
1,92	1,92	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Český	český	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
(	(	kIx(
<g/>
nádraží	nádraží	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Provozovatel	provozovatel	k1gMnSc1
dráhy	dráha	k1gFnSc2
</s>
<s>
Správa	správa	k1gFnSc1
železnic	železnice	k1gFnPc2
Kód	kód	k1gInSc4
stanice	stanice	k1gFnPc4
</s>
<s>
760025	#num#	k4
Tratě	trať	k1gFnPc4
</s>
<s>
194	#num#	k4
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
540	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
V	v	k7c6
provozu	provoz	k1gInSc6
od	od	k7c2
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1891	#num#	k4
Zabezpečovací	zabezpečovací	k2eAgFnSc4d1
zařízení	zařízení	k1gNnPc1
</s>
<s>
elektronické	elektronický	k2eAgNnSc4d1
stavědlo	stavědlo	k1gNnSc4
ESA	eso	k1gNnSc2
44	#num#	k4
(	(	kIx(
<g/>
DOZ	DOZ	kA
z	z	k7c2
Kájova	Kájovo	k1gNnSc2
<g/>
)	)	kIx)
Dopravních	dopravní	k2eAgFnPc2d1
kolejí	kolej	k1gFnPc2
</s>
<s>
2	#num#	k4
Nástupišť	nástupiště	k1gNnPc2
(	(	kIx(
<g/>
hran	hrana	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
Prodej	prodej	k1gInSc1
jízdenek	jízdenka	k1gFnPc2
</s>
<s>
Návazná	návazný	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
</s>
<s>
Městská	městský	k2eAgFnSc1d1
hromadná	hromadný	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
(	(	kIx(
<g/>
autobusy	autobus	k1gInPc1
<g/>
)	)	kIx)
Služby	služba	k1gFnPc1
ve	v	k7c6
stanici	stanice	k1gFnSc6
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Český	český	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
je	být	k5eAaImIp3nS
železniční	železniční	k2eAgFnPc4d1
stanice	stanice	k1gFnPc4
v	v	k7c6
severní	severní	k2eAgFnSc6d1
části	část	k1gFnSc6
okresního	okresní	k2eAgNnSc2d1
města	město	k1gNnSc2
Český	český	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
v	v	k7c6
Jihočeském	jihočeský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
nedaleko	nedaleko	k7c2
řeky	řeka	k1gFnSc2
Vltavy	Vltava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
na	na	k7c6
jednokolejné	jednokolejný	k2eAgFnSc6d1
neelektrizované	elektrizovaný	k2eNgFnSc6d1
trati	trať	k1gFnSc6
194	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1891	#num#	k4
otevřela	otevřít	k5eAaPmAgFnS
Rakouská	rakouský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
místních	místní	k2eAgFnPc2d1
drah	draha	k1gFnPc2
ÖLEG	ÖLEG	kA
odbočnou	odbočný	k2eAgFnSc4d1
trať	trať	k1gFnSc4
z	z	k7c2
Rožnova	Rožnov	k1gInSc2
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1871	#num#	k4
vedla	vést	k5eAaImAgFnS
trať	trať	k1gFnSc1
spojující	spojující	k2eAgInPc1d1
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
a	a	k8xC
Linec	Linec	k1gInSc1
<g/>
,	,	kIx,
do	do	k7c2
Kájova	Kájov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stanice	stanice	k1gFnSc1
v	v	k7c6
Českém	český	k2eAgInSc6d1
Krumlově	Krumlov	k1gInSc6
na	na	k7c6
této	tento	k3xDgFnSc6
trase	trasa	k1gFnSc6
vznikla	vzniknout	k5eAaPmAgNnP
dle	dle	k7c2
typizovaného	typizovaný	k2eAgInSc2d1
stavebního	stavební	k2eAgInSc2d1
vzoru	vzor	k1gInSc2
nádražních	nádražní	k2eAgFnPc2d1
budov	budova	k1gFnPc2
ÖLEG	ÖLEG	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Železniční	železniční	k2eAgInSc4d1
spojení	spojení	k1gNnSc1
bylo	být	k5eAaImAgNnS
4	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1892	#num#	k4
prodlouženo	prodloužit	k5eAaPmNgNnS
do	do	k7c2
Želnavy	Želnava	k1gFnSc2
(	(	kIx(
<g/>
nyní	nyní	k6eAd1
Nová	nový	k2eAgFnSc1d1
Pec	Pec	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
roku	rok	k1gInSc2
1910	#num#	k4
do	do	k7c2
stanice	stanice	k1gFnSc2
Černý	Černý	k1gMnSc1
Kříž	Kříž	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Provoz	provoz	k1gInSc1
zajišťovaly	zajišťovat	k5eAaImAgInP
od	od	k7c2
začátku	začátek	k1gInSc2
Císařsko-královské	císařsko-královský	k2eAgFnSc2d1
státní	státní	k2eAgFnSc2d1
dráhy	dráha	k1gFnSc2
(	(	kIx(
<g/>
kkStB	kkStB	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1894	#num#	k4
byla	být	k5eAaImAgFnS
trať	trať	k1gFnSc1
zestátněna	zestátněn	k2eAgFnSc1d1
<g/>
,	,	kIx,
po	po	k7c6
roce	rok	k1gInSc6
1918	#num#	k4
pak	pak	k6eAd1
správu	správa	k1gFnSc4
přebraly	přebrat	k5eAaPmAgFnP
Československé	československý	k2eAgFnPc1d1
státní	státní	k2eAgFnPc1d1
dráhy	dráha	k1gFnPc1
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1993	#num#	k4
byly	být	k5eAaImAgFnP
provozovatelem	provozovatel	k1gMnSc7
České	český	k2eAgFnSc2d1
dráhy	dráha	k1gFnSc2
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
2008	#num#	k4
tuto	tento	k3xDgFnSc4
povinnost	povinnost	k1gFnSc4
převzala	převzít	k5eAaPmAgFnS
Správa	správa	k1gFnSc1
železniční	železniční	k2eAgFnSc2d1
dopravní	dopravní	k2eAgFnSc2d1
cesty	cesta	k1gFnSc2
(	(	kIx(
<g/>
nynější	nynější	k2eAgFnSc1d1
Správa	správa	k1gFnSc1
železnic	železnice	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
prošla	projít	k5eAaPmAgFnS
celá	celý	k2eAgFnSc1d1
trať	trať	k1gFnSc1
rekonstrukcí	rekonstrukce	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
rámci	rámec	k1gInSc6
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
byla	být	k5eAaImAgFnS
ve	v	k7c6
stanici	stanice	k1gFnSc6
původní	původní	k2eAgNnPc4d1
hranová	hranový	k2eAgNnPc4d1
nástupiště	nástupiště	k1gNnPc4
nahrazena	nahrazen	k2eAgNnPc4d1
ostrovním	ostrovní	k2eAgInSc7d1
<g/>
,	,	kIx,
zároveň	zároveň	k6eAd1
byla	být	k5eAaImAgFnS
ve	v	k7c6
stanici	stanice	k1gFnSc6
zrušena	zrušen	k2eAgFnSc1d1
dopravní	dopravní	k2eAgFnSc1d1
služba	služba	k1gFnSc1
a	a	k8xC
stanice	stanice	k1gFnSc1
je	být	k5eAaImIp3nS
nyní	nyní	k6eAd1
ovládána	ovládat	k5eAaImNgFnS
z	z	k7c2
Kájova	Kájovo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
stanici	stanice	k1gFnSc6
zastavují	zastavovat	k5eAaImIp3nP
pravidelné	pravidelný	k2eAgInPc4d1
osobní	osobní	k2eAgInPc4d1
vlaky	vlak	k1gInPc4
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
dopravcem	dopravce	k1gMnSc7
je	být	k5eAaImIp3nS
od	od	k7c2
prosince	prosinec	k1gInSc2
2017	#num#	k4
GW	GW	kA
Train	Train	k1gMnSc1
Regio	Regio	k1gMnSc1
<g/>
,	,	kIx,
od	od	k7c2
prosince	prosinec	k1gInSc2
2016	#num#	k4
jezdí	jezdit	k5eAaImIp3nP
do	do	k7c2
České	český	k2eAgFnSc2d1
Krumlova	Krumlov	k1gInSc2
též	též	k9
jeden	jeden	k4xCgInSc4
pár	pár	k1gInSc4
pravidelných	pravidelný	k2eAgInPc2d1
expresů	expres	k1gInPc2
z	z	k7c2
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnSc7
dopravcem	dopravce	k1gMnSc7
jsou	být	k5eAaImIp3nP
České	český	k2eAgFnSc2d1
dráhy	dráha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
zde	zde	k6eAd1
ostrovní	ostrovní	k2eAgNnSc1d1
nekryté	krytý	k2eNgNnSc1d1
nástupiště	nástupiště	k1gNnSc1
<g/>
,	,	kIx,
k	k	k7c3
příchodu	příchod	k1gInSc3
na	na	k7c4
nástupiště	nástupiště	k1gNnSc4
slouží	sloužit	k5eAaImIp3nS
přechod	přechod	k1gInSc1
přes	přes	k7c4
kolej	kolej	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Český	český	k2eAgInSc4d1
Krumlov	Krumlov	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Článek	článek	k1gInSc1
o	o	k7c6
plánovaném	plánovaný	k2eAgNnSc6d1
odstavení	odstavení	k1gNnSc6
nádražní	nádražní	k2eAgFnSc2d1
budovy	budova	k1gFnSc2
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
Rakouské	rakouský	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
místních	místní	k2eAgFnPc2d1
drah	draha	k1gFnPc2
</s>
<s>
Profil	profil	k1gInSc1
stanice	stanice	k1gFnSc2
na	na	k7c6
stránkách	stránka	k1gFnPc6
Českých	český	k2eAgFnPc2d1
drah	draha	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gNnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc4
<g/>
}	}	kIx)
Železniční	železniční	k2eAgFnSc4d1
trať	trať	k1gFnSc4
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
–	–	k?
Černý	černý	k2eAgMnSc1d1
Kříž	Kříž	k1gMnSc1
</s>
<s>
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
–	–	k?
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
jižní	jižní	k2eAgFnSc1d1
zastávka	zastávka	k1gFnSc1
–	–	k?
Boršov	Boršov	k1gInSc1
nad	nad	k7c7
Vltavou	Vltava	k1gFnSc7
–	–	k?
Černý	Černý	k1gMnSc1
Dub	Dub	k1gMnSc1
–	–	k?
Hradce	Hradec	k1gInPc1
–	–	k?
Vrábče	Vrábč	k1gInSc2
–	–	k?
Křemže	Křemž	k1gFnSc2
–	–	k?
Holubov	Holubov	k1gInSc1
–	–	k?
Třísov	Třísov	k1gInSc1
–	–	k?
Plešovice	Plešovice	k1gFnSc2
–	–	k?
Zlatá	zlatý	k2eAgFnSc1d1
Koruna	koruna	k1gFnSc1
–	–	k?
Přísečná	Přísečný	k2eAgFnSc1d1
–	–	k?
Domoradice	Domoradice	k1gFnSc1
–	–	k?
Český	český	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
–	–	k?
Kájov	Kájov	k1gInSc1
–	–	k?
Mezipotočí	Mezipotočí	k1gFnSc2
–	–	k?
Hořice	Hořice	k1gFnPc1
na	na	k7c6
Šumavě	Šumava	k1gFnSc6
–	–	k?
Polná	Polná	k1gFnSc1
na	na	k7c6
Šumavě	Šumava	k1gFnSc6
–	–	k?
Polečnice	Polečnice	k1gFnSc2
–	–	k?
Hodňov	Hodňov	k1gInSc1
–	–	k?
Žlábek	žlábek	k1gInSc1
–	–	k?
Černá	černat	k5eAaImIp3nS
v	v	k7c6
Pošumaví	Pošumaví	k1gNnSc6
–	–	k?
Horní	horní	k2eAgFnSc1d1
Planá	planý	k2eAgFnSc1d1
zastávka	zastávka	k1gFnSc1
–	–	k?
Horní	horní	k2eAgFnSc1d1
Planá	Planá	k1gFnSc1
–	–	k?
Pernek	Pernky	k1gFnPc2
na	na	k7c6
Šumavě	Šumava	k1gFnSc6
–	–	k?
Nová	nový	k2eAgFnSc1d1
Pec	Pec	k1gFnSc1
–	–	k?
Ovesná	ovesný	k2eAgFnSc1d1
–	–	k?
Pěkná	pěkná	k1gFnSc1
–	–	k?
Černý	Černý	k1gMnSc1
Kříž	Kříž	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Železnice	železnice	k1gFnSc1
|	|	kIx~
Česko	Česko	k1gNnSc1
|	|	kIx~
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
</s>
