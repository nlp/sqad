<s>
Provoz	provoz	k1gInSc1
zajišťovaly	zajišťovat	k5eAaImAgInP
od	od	k7c2
začátku	začátek	k1gInSc2
Císařsko-královské	císařsko-královský	k2eAgFnSc2d1
státní	státní	k2eAgFnSc2d1
dráhy	dráha	k1gFnSc2
(	(	kIx(
<g/>
kkStB	kkStB	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1894	#num#	k4
byla	být	k5eAaImAgFnS
trať	trať	k1gFnSc1
zestátněna	zestátněn	k2eAgFnSc1d1
<g/>
,	,	kIx,
po	po	k7c6
roce	rok	k1gInSc6
1918	#num#	k4
pak	pak	k6eAd1
správu	správa	k1gFnSc4
přebraly	přebrat	k5eAaPmAgFnP
Československé	československý	k2eAgFnPc1d1
státní	státní	k2eAgFnPc1d1
dráhy	dráha	k1gFnPc1
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1993	#num#	k4
byly	být	k5eAaImAgFnP
provozovatelem	provozovatel	k1gMnSc7
České	český	k2eAgFnSc2d1
dráhy	dráha	k1gFnSc2
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
2008	#num#	k4
tuto	tento	k3xDgFnSc4
povinnost	povinnost	k1gFnSc4
převzala	převzít	k5eAaPmAgFnS
Správa	správa	k1gFnSc1
železniční	železniční	k2eAgFnSc2d1
dopravní	dopravní	k2eAgFnSc2d1
cesty	cesta	k1gFnSc2
(	(	kIx(
<g/>
nynější	nynější	k2eAgFnSc1d1
Správa	správa	k1gFnSc1
železnic	železnice	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>