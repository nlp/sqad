<p>
<s>
Sycopsis	Sycopsis	k1gFnSc1	Sycopsis
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc4	rod
rostlin	rostlina	k1gFnPc2	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
vilínovité	vilínovitý	k2eAgFnSc2d1	vilínovitý
(	(	kIx(	(
<g/>
Hamamelidaceae	Hamamelidacea	k1gFnSc2	Hamamelidacea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
2	[number]	k4	2
druhy	druh	k1gInPc1	druh
stálezelených	stálezelený	k2eAgFnPc2d1	stálezelená
dřevin	dřevina	k1gFnPc2	dřevina
se	s	k7c7	s
střídavými	střídavý	k2eAgInPc7d1	střídavý
jednoduchými	jednoduchý	k2eAgInPc7d1	jednoduchý
listy	list	k1gInPc7	list
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
bezkorunné	bezkorunný	k2eAgInPc1d1	bezkorunný
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
s	s	k7c7	s
nápadnými	nápadný	k2eAgFnPc7d1	nápadná
červenými	červený	k2eAgFnPc7d1	červená
tyčinkami	tyčinka	k1gFnPc7	tyčinka
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
výhradně	výhradně	k6eAd1	výhradně
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
rodu	rod	k1gInSc2	rod
Sycopsis	Sycopsis	k1gFnSc2	Sycopsis
jsou	být	k5eAaImIp3nP	být
stálezelené	stálezelený	k2eAgInPc1d1	stálezelený
keře	keř	k1gInPc1	keř
a	a	k8xC	a
malé	malý	k2eAgInPc1d1	malý
stromy	strom	k1gInPc1	strom
s	s	k7c7	s
jednoduchými	jednoduchý	k2eAgInPc7d1	jednoduchý
střídavými	střídavý	k2eAgInPc7d1	střídavý
kožovitými	kožovitý	k2eAgInPc7d1	kožovitý
listy	list	k1gInPc7	list
<g/>
.	.	kIx.	.
</s>
<s>
Větévky	větévka	k1gFnPc1	větévka
jsou	být	k5eAaImIp3nP	být
lysé	lysý	k2eAgFnPc1d1	Lysá
nebo	nebo	k8xC	nebo
hvězdovitě	hvězdovitě	k6eAd1	hvězdovitě
chlupaté	chlupatý	k2eAgFnPc1d1	chlupatá
<g/>
,	,	kIx,	,
s	s	k7c7	s
nahými	nahý	k2eAgInPc7d1	nahý
pupeny	pupen	k1gInPc7	pupen
<g/>
.	.	kIx.	.
</s>
<s>
Palisty	palist	k1gInPc1	palist
jsou	být	k5eAaImIp3nP	být
drobné	drobný	k2eAgMnPc4d1	drobný
<g/>
,	,	kIx,	,
opadavé	opadavý	k2eAgFnPc1d1	opadavá
<g/>
,	,	kIx,	,
zanechávající	zanechávající	k2eAgFnPc1d1	zanechávající
na	na	k7c6	na
větévce	větévka	k1gFnSc6	větévka
drobnou	drobný	k2eAgFnSc4d1	drobná
jizvu	jizva	k1gFnSc4	jizva
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
tuhé	tuhý	k2eAgInPc1d1	tuhý
<g/>
,	,	kIx,	,
kožovité	kožovitý	k2eAgInPc1d1	kožovitý
<g/>
,	,	kIx,	,
řapíkaté	řapíkatý	k2eAgInPc1d1	řapíkatý
<g/>
,	,	kIx,	,
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
zaokrouhlené	zaokrouhlený	k2eAgInPc1d1	zaokrouhlený
až	až	k6eAd1	až
klínovité	klínovitý	k2eAgInPc1d1	klínovitý
<g/>
,	,	kIx,	,
celokrajné	celokrajný	k2eAgInPc1d1	celokrajný
nebo	nebo	k8xC	nebo
při	při	k7c6	při
vrcholu	vrchol	k1gInSc6	vrchol
listu	list	k1gInSc2	list
drobně	drobně	k6eAd1	drobně
zubaté	zubatý	k2eAgFnPc1d1	zubatá
<g/>
.	.	kIx.	.
</s>
<s>
Žilnatina	žilnatina	k1gFnSc1	žilnatina
je	být	k5eAaImIp3nS	být
zpeřená	zpeřený	k2eAgFnSc1d1	zpeřená
<g/>
,	,	kIx,	,
brochidodromní	brochidodromní	k2eAgFnSc1d1	brochidodromní
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
od	od	k7c2	od
báze	báze	k1gFnSc2	báze
trojžilná	trojžilný	k2eAgFnSc1d1	trojžilná
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
bezkorunné	bezkorunný	k2eAgFnPc4d1	bezkorunný
<g/>
,	,	kIx,	,
oboupohlavné	oboupohlavný	k2eAgFnPc4d1	oboupohlavná
nebo	nebo	k8xC	nebo
samčí	samčí	k2eAgFnPc4d1	samčí
<g/>
,	,	kIx,	,
uspořádané	uspořádaný	k2eAgFnPc4d1	uspořádaná
v	v	k7c6	v
krátkém	krátký	k2eAgInSc6d1	krátký
hustém	hustý	k2eAgInSc6d1	hustý
klasu	klas	k1gInSc6	klas
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
květ	květ	k1gInSc1	květ
je	být	k5eAaImIp3nS	být
podepřen	podepřen	k2eAgInSc1d1	podepřen
jedním	jeden	k4xCgInSc7	jeden
listenem	listen	k1gInSc7	listen
<g/>
.	.	kIx.	.
</s>
<s>
Kalich	kalich	k1gInSc1	kalich
je	být	k5eAaImIp3nS	být
5	[number]	k4	5
nebo	nebo	k8xC	nebo
6	[number]	k4	6
<g/>
-četný	-četný	k2eAgInSc4d1	-četný
<g/>
,	,	kIx,	,
nepravidelný	pravidelný	k2eNgInSc4d1	nepravidelný
<g/>
.	.	kIx.	.
</s>
<s>
Koruna	koruna	k1gFnSc1	koruna
chybí	chybět	k5eAaImIp3nS	chybět
<g/>
.	.	kIx.	.
</s>
<s>
Tyčinek	tyčinka	k1gFnPc2	tyčinka
je	být	k5eAaImIp3nS	být
5	[number]	k4	5
až	až	k9	až
10	[number]	k4	10
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
přirostlé	přirostlý	k2eAgFnPc1d1	přirostlá
k	k	k7c3	k
okraji	okraj	k1gInSc3	okraj
kalicha	kalich	k1gInSc2	kalich
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
červené	červený	k2eAgInPc4d1	červený
prašníky	prašník	k1gInPc4	prašník
<g/>
.	.	kIx.	.
</s>
<s>
Semeník	semeník	k1gInSc1	semeník
v	v	k7c6	v
oboupohlavných	oboupohlavný	k2eAgInPc6d1	oboupohlavný
květech	květ	k1gInPc6	květ
je	být	k5eAaImIp3nS	být
svrchní	svrchní	k2eAgNnSc1d1	svrchní
avšak	avšak	k8xC	avšak
obklopený	obklopený	k2eAgInSc1d1	obklopený
lůžkem	lůžko	k1gNnSc7	lůžko
květu	květ	k1gInSc2	květ
<g/>
,	,	kIx,	,
srostlý	srostlý	k2eAgInSc4d1	srostlý
ze	z	k7c2	z
2	[number]	k4	2
plodolistů	plodolist	k1gInPc2	plodolist
obsahujících	obsahující	k2eAgInPc2d1	obsahující
po	po	k7c6	po
1	[number]	k4	1
vajíčku	vajíčko	k1gNnSc6	vajíčko
<g/>
,	,	kIx,	,
se	s	k7c7	s
2	[number]	k4	2
tenkými	tenký	k2eAgFnPc7d1	tenká
čnělkami	čnělka	k1gFnPc7	čnělka
<g/>
.	.	kIx.	.
</s>
<s>
Tobolky	tobolka	k1gFnPc1	tobolka
jsou	být	k5eAaImIp3nP	být
dřevnaté	dřevnatý	k2eAgFnPc1d1	dřevnatá
<g/>
,	,	kIx,	,
plstnaté	plstnatý	k2eAgFnPc1d1	plstnatá
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
vejcovitě	vejcovitě	k6eAd1	vejcovitě
kulovité	kulovitý	k2eAgFnPc1d1	kulovitá
<g/>
,	,	kIx,	,
pukající	pukající	k2eAgFnPc1d1	pukající
2	[number]	k4	2
dvoulaločnými	dvoulaločný	k2eAgFnPc7d1	dvoulaločná
chlopněmi	chlopeň	k1gFnPc7	chlopeň
a	a	k8xC	a
obsahující	obsahující	k2eAgInSc1d1	obsahující
2	[number]	k4	2
úzce	úzko	k6eAd1	úzko
vejcovitá	vejcovitý	k2eAgNnPc4d1	vejcovité
semena	semeno	k1gNnPc4	semeno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Rod	rod	k1gInSc1	rod
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
2	[number]	k4	2
druhy	druh	k1gInPc4	druh
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
výhradně	výhradně	k6eAd1	výhradně
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Sycopsis	Sycopsis	k1gFnSc1	Sycopsis
sinensis	sinensis	k1gFnPc2	sinensis
roste	růst	k5eAaImIp3nS	růst
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
území	území	k1gNnSc6	území
Číny	Čína	k1gFnSc2	Čína
ve	v	k7c6	v
stálezelených	stálezelený	k2eAgInPc6d1	stálezelený
horských	horský	k2eAgInPc6d1	horský
lesích	les	k1gInPc6	les
v	v	k7c6	v
nadmořských	nadmořský	k2eAgFnPc6d1	nadmořská
výškách	výška	k1gFnPc6	výška
1300	[number]	k4	1300
až	až	k6eAd1	až
1500	[number]	k4	1500
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
Sycopsis	Sycopsis	k1gFnSc1	Sycopsis
triplinervia	triplinervium	k1gNnSc2	triplinervium
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Číně	Čína	k1gFnSc6	Čína
v	v	k7c6	v
provinciích	provincie	k1gFnPc6	provincie
S	s	k7c7	s
<g/>
'	'	kIx"	'
<g/>
-čchuan	-čchuan	k1gMnSc1	-čchuan
a	a	k8xC	a
Jün-nan	Jünan	k1gMnSc1	Jün-nan
v	v	k7c6	v
nadmořských	nadmořský	k2eAgFnPc6d1	nadmořská
výškách	výška	k1gFnPc6	výška
800	[number]	k4	800
až	až	k6eAd1	až
1000	[number]	k4	1000
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
==	==	k?	==
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
zdroje	zdroj	k1gInPc1	zdroj
uvádějí	uvádět	k5eAaImIp3nP	uvádět
3	[number]	k4	3
druhy	druh	k1gInPc4	druh
rodu	rod	k1gInSc2	rod
Sycopsis	Sycopsis	k1gFnSc2	Sycopsis
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
2	[number]	k4	2
výše	vysoce	k6eAd2	vysoce
zmíněných	zmíněný	k2eAgInPc2d1	zmíněný
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
ještě	ještě	k9	ještě
druh	druh	k1gInSc1	druh
Sycopsis	Sycopsis	k1gFnSc2	Sycopsis
dunnii	dunnie	k1gFnSc4	dunnie
<g/>
,	,	kIx,	,
rozšířený	rozšířený	k2eAgInSc4d1	rozšířený
od	od	k7c2	od
Číny	Čína	k1gFnSc2	Čína
po	po	k7c6	po
Malajsii	Malajsie	k1gFnSc6	Malajsie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
taxonomii	taxonomie	k1gFnSc6	taxonomie
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
řazen	řadit	k5eAaImNgInS	řadit
do	do	k7c2	do
samostatného	samostatný	k2eAgInSc2d1	samostatný
rodu	rod	k1gInSc2	rod
Distyliopsis	Distyliopsis	k1gFnSc1	Distyliopsis
jako	jako	k8xS	jako
druh	druh	k1gInSc1	druh
Distyliopsis	Distyliopsis	k1gFnSc2	Distyliopsis
dunnii	dunnie	k1gFnSc3	dunnie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přehled	přehled	k1gInSc4	přehled
druhů	druh	k1gInPc2	druh
==	==	k?	==
</s>
</p>
<p>
<s>
Sycopsis	Sycopsis	k1gFnSc1	Sycopsis
sinensis	sinensis	k1gFnSc2	sinensis
</s>
</p>
<p>
<s>
Sycopsis	Sycopsis	k1gFnSc1	Sycopsis
triplinervia	triplinervia	k1gFnSc1	triplinervia
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Druh	druh	k1gInSc1	druh
Sycopsis	Sycopsis	k1gFnPc2	Sycopsis
sinensis	sinensis	k1gFnPc1	sinensis
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
vzácně	vzácně	k6eAd1	vzácně
pěstován	pěstovat	k5eAaImNgInS	pěstovat
v	v	k7c6	v
botanických	botanický	k2eAgFnPc6d1	botanická
zahradách	zahrada	k1gFnPc6	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
např.	např.	kA	např.
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
mediterránní	mediterránní	k2eAgFnSc2d1	mediterránní
vegetace	vegetace	k1gFnSc2	vegetace
v	v	k7c6	v
Arboretu	arboretum	k1gNnSc6	arboretum
MU	MU	kA	MU
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Sycopsis	Sycopsis	k1gFnSc2	Sycopsis
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Sycopsis	Sycopsis	k1gInSc1	Sycopsis
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
