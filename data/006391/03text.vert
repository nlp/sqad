<s>
Vladimir	Vladimir	k1gMnSc1	Vladimir
Semjonovič	Semjonovič	k1gMnSc1	Semjonovič
Vysockij	Vysockij	k1gMnSc1	Vysockij
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
В	В	k?	В
С	С	k?	С
В	В	k?	В
<g/>
,	,	kIx,	,
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1938	[number]	k4	1938
Moskva	Moskva	k1gFnSc1	Moskva
–	–	k?	–
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1980	[number]	k4	1980
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
ruský	ruský	k2eAgMnSc1d1	ruský
písničkář	písničkář	k1gMnSc1	písničkář
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1947	[number]	k4	1947
-	-	kIx~	-
1949	[number]	k4	1949
žil	žít	k5eAaImAgMnS	žít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Eberswalde	Eberswald	k1gInSc5	Eberswald
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
u	u	k7c2	u
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
důstojníka	důstojník	k1gMnSc2	důstojník
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
r.	r.	kA	r.
1955	[number]	k4	1955
studovat	studovat	k5eAaImF	studovat
strojírenskou	strojírenský	k2eAgFnSc4d1	strojírenská
fakultu	fakulta	k1gFnSc4	fakulta
Moskevského	moskevský	k2eAgInSc2d1	moskevský
institutu	institut	k1gInSc2	institut
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
strávil	strávit	k5eAaPmAgMnS	strávit
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c4	na
divadelní	divadelní	k2eAgFnSc4d1	divadelní
fakultu	fakulta	k1gFnSc4	fakulta
MCHAT	MCHAT	kA	MCHAT
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
úspěšně	úspěšně	k6eAd1	úspěšně
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1960	[number]	k4	1960
<g/>
–	–	k?	–
<g/>
1964	[number]	k4	1964
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
Divadla	divadlo	k1gNnSc2	divadlo
A.	A.	kA	A.
S.	S.	kA	S.
Puškina	Puškina	k1gMnSc1	Puškina
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
avantgardního	avantgardní	k2eAgNnSc2d1	avantgardní
divadla	divadlo	k1gNnSc2	divadlo
Moskevského	moskevský	k2eAgNnSc2d1	moskevské
dramatu	drama	k1gNnSc2	drama
a	a	k8xC	a
komedie	komedie	k1gFnSc2	komedie
Na	na	k7c6	na
Tagance	Taganka	k1gFnSc6	Taganka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
divadle	divadlo	k1gNnSc6	divadlo
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgMnS	prosadit
v	v	k7c6	v
několika	několik	k4yIc6	několik
těžkých	těžký	k2eAgFnPc6d1	těžká
rolích	role	k1gFnPc6	role
<g/>
;	;	kIx,	;
hrál	hrát	k5eAaImAgMnS	hrát
např.	např.	kA	např.
Hamleta	Hamlet	k1gMnSc2	Hamlet
(	(	kIx(	(
<g/>
William	William	k1gInSc1	William
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Galilea	Galilea	k1gFnSc1	Galilea
(	(	kIx(	(
<g/>
Bertolt	Bertolt	k1gMnSc1	Bertolt
Brecht	Brecht	k1gMnSc1	Brecht
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
též	též	k9	též
filmových	filmový	k2eAgNnPc2d1	filmové
hercem	herec	k1gMnSc7	herec
a	a	k8xC	a
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
filmech	film	k1gInPc6	film
a	a	k8xC	a
několika	několik	k4yIc6	několik
seriálech	seriál	k1gInPc6	seriál
-	-	kIx~	-
viz	vidět	k5eAaImRp2nS	vidět
jeho	jeho	k3xOp3gInPc4	jeho
profil	profil	k1gInSc4	profil
na	na	k7c6	na
ČSFD	ČSFD	kA	ČSFD
<g/>
.	.	kIx.	.
</s>
<s>
Oženil	oženit	k5eAaPmAgMnS	oženit
se	se	k3xPyFc4	se
s	s	k7c7	s
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
herečkou	herečka	k1gFnSc7	herečka
ruského	ruský	k2eAgInSc2d1	ruský
původu	původ	k1gInSc2	původ
Marinou	Marina	k1gFnSc7	Marina
Vladyovou	Vladyová	k1gFnSc7	Vladyová
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
kolovalo	kolovat	k5eAaImAgNnS	kolovat
asi	asi	k9	asi
2000	[number]	k4	2000
jeho	jeho	k3xOp3gFnPc2	jeho
písní	píseň	k1gFnPc2	píseň
plus	plus	k6eAd1	plus
mnoho	mnoho	k4c1	mnoho
falzifikátů	falzifikát	k1gInPc2	falzifikát
<g/>
.	.	kIx.	.
</s>
<s>
Zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
se	se	k3xPyFc4	se
na	na	k7c6	na
přípravě	příprava	k1gFnSc6	příprava
11	[number]	k4	11
rozhlasových	rozhlasový	k2eAgFnPc2d1	rozhlasová
relací	relace	k1gFnPc2	relace
a	a	k8xC	a
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1000	[number]	k4	1000
koncertů	koncert	k1gInPc2	koncert
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
autorem	autor	k1gMnSc7	autor
více	hodně	k6eAd2	hodně
než	než	k8xS	než
700	[number]	k4	700
písní	píseň	k1gFnPc2	píseň
a	a	k8xC	a
básní	báseň	k1gFnPc2	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
básnická	básnický	k2eAgFnSc1d1	básnická
tvorba	tvorba	k1gFnSc1	tvorba
představuje	představovat	k5eAaImIp3nS	představovat
hlavně	hlavně	k9	hlavně
lyrickou	lyrický	k2eAgFnSc4d1	lyrická
poezii	poezie	k1gFnSc4	poezie
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
hojně	hojně	k6eAd1	hojně
používal	používat	k5eAaImAgMnS	používat
ironii	ironie	k1gFnSc4	ironie
a	a	k8xC	a
sarkasmus	sarkasmus	k1gInSc4	sarkasmus
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc4d3	veliký
popularitu	popularita	k1gFnSc4	popularita
získal	získat	k5eAaPmAgMnS	získat
zhudebněnými	zhudebněný	k2eAgFnPc7d1	zhudebněná
básněmi	báseň	k1gFnPc7	báseň
a	a	k8xC	a
písněmi	píseň	k1gFnPc7	píseň
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
interpretoval	interpretovat	k5eAaBmAgMnS	interpretovat
především	především	k6eAd1	především
s	s	k7c7	s
doprovodem	doprovod	k1gInSc7	doprovod
akustické	akustický	k2eAgFnSc2d1	akustická
kytary	kytara	k1gFnSc2	kytara
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
které	který	k3yQgFnSc2	který
udržoval	udržovat	k5eAaImAgInS	udržovat
rytmus	rytmus	k1gInSc1	rytmus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
textech	text	k1gInPc6	text
vycházel	vycházet	k5eAaImAgMnS	vycházet
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
městského	městský	k2eAgInSc2d1	městský
folklóru	folklór	k1gInSc2	folklór
(	(	kIx(	(
<g/>
romance	romance	k1gFnSc1	romance
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slovní	slovní	k2eAgFnSc2d1	slovní
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
parodie	parodie	k1gFnSc2	parodie
<g/>
,	,	kIx,	,
parafráze	parafráze	k1gFnSc2	parafráze
a	a	k8xC	a
filozofických	filozofický	k2eAgFnPc2d1	filozofická
úvah	úvaha	k1gFnPc2	úvaha
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
oběhu	oběh	k1gInSc2	oběh
se	se	k3xPyFc4	se
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
dostalo	dostat	k5eAaPmAgNnS	dostat
i	i	k9	i
mnoho	mnoho	k4c1	mnoho
falzifikátů	falzifikát	k1gInPc2	falzifikát
vydávaných	vydávaný	k2eAgInPc2d1	vydávaný
za	za	k7c4	za
jeho	jeho	k3xOp3gFnPc4	jeho
písně	píseň	k1gFnPc4	píseň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
zpívali	zpívat	k5eAaImAgMnP	zpívat
některé	některý	k3yIgFnPc4	některý
jeho	jeho	k3xOp3gFnPc4	jeho
přeložené	přeložený	k2eAgFnPc4d1	přeložená
písně	píseň	k1gFnPc4	píseň
Jaromír	Jaromír	k1gMnSc1	Jaromír
Nohavica	Nohavica	k1gMnSc1	Nohavica
či	či	k8xC	či
Radůza	Radůza	k1gFnSc1	Radůza
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
překladatelem	překladatel	k1gMnSc7	překladatel
a	a	k8xC	a
také	také	k6eAd1	také
interpretem	interpret	k1gMnSc7	interpret
Vysockého	vysocký	k2eAgNnSc2d1	Vysocké
díla	dílo	k1gNnSc2	dílo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
rusista	rusista	k1gMnSc1	rusista
Milan	Milan	k1gMnSc1	Milan
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
převedl	převést	k5eAaPmAgMnS	převést
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
písní	píseň	k1gFnPc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
překládala	překládat	k5eAaImAgFnS	překládat
jeho	jeho	k3xOp3gFnPc4	jeho
básně	báseň	k1gFnPc4	báseň
např.	např.	kA	např.
Lýdia	Lýdia	k1gFnSc1	Lýdia
Vadkerti-Gavorníková	Vadkerti-Gavorníková	k1gFnSc1	Vadkerti-Gavorníková
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgFnPc1d1	mnohá
písně	píseň	k1gFnPc1	píseň
vystihovaly	vystihovat	k5eAaImAgFnP	vystihovat
názory	názor	k1gInPc4	názor
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
ruské	ruský	k2eAgFnSc2d1	ruská
mládeže	mládež	k1gFnSc2	mládež
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgFnPc1d1	populární
byly	být	k5eAaImAgFnP	být
písně	píseň	k1gFnPc1	píseň
s	s	k7c7	s
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
tematikou	tematika	k1gFnSc7	tematika
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
posluchačů	posluchač	k1gMnPc2	posluchač
jeho	jeho	k3xOp3gFnSc2	jeho
písní	píseň	k1gFnPc2	píseň
nemohlo	moct	k5eNaImAgNnS	moct
dlouho	dlouho	k6eAd1	dlouho
uvěřit	uvěřit	k5eAaPmF	uvěřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Vysockij	Vysockij	k1gMnSc1	Vysockij
nebyl	být	k5eNaImAgMnS	být
přímým	přímý	k2eAgMnSc7d1	přímý
účastníkem	účastník	k1gMnSc7	účastník
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
L.	L.	kA	L.
I.	I.	kA	I.
Brežněva	Brežněva	k1gFnSc1	Brežněva
do	do	k7c2	do
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
funkcí	funkce	k1gFnPc2	funkce
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
písních	píseň	k1gFnPc6	píseň
patrná	patrný	k2eAgFnSc1d1	patrná
skrytá	skrytý	k2eAgFnSc1d1	skrytá
forma	forma	k1gFnSc1	forma
politické	politický	k2eAgFnSc2d1	politická
satiry	satira	k1gFnSc2	satira
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
přísné	přísný	k2eAgFnSc2d1	přísná
cenzury	cenzura	k1gFnSc2	cenzura
se	se	k3xPyFc4	se
dotýkal	dotýkat	k5eAaImAgMnS	dotýkat
mnoha	mnoho	k4c2	mnoho
zakázaných	zakázaný	k2eAgNnPc2d1	zakázané
témat	téma	k1gNnPc2	téma
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgInSc3	ten
byly	být	k5eAaImAgFnP	být
některé	některý	k3yIgFnPc4	některý
jeho	jeho	k3xOp3gFnPc4	jeho
písně	píseň	k1gFnPc4	píseň
zakázané	zakázaný	k2eAgFnPc4d1	zakázaná
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
jeho	jeho	k3xOp3gFnSc2	jeho
tvorby	tvorba	k1gFnSc2	tvorba
šířila	šířit	k5eAaImAgFnS	šířit
na	na	k7c6	na
podomácky	podomácky	k6eAd1	podomácky
nahrávaných	nahrávaný	k2eAgInPc6d1	nahrávaný
magnetofonových	magnetofonový	k2eAgInPc6d1	magnetofonový
páscích	pásec	k1gInPc6	pásec
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
neshodám	neshoda	k1gFnPc3	neshoda
s	s	k7c7	s
nejvyššími	vysoký	k2eAgMnPc7d3	nejvyšší
představiteli	představitel	k1gMnPc7	představitel
mu	on	k3xPp3gMnSc3	on
nebylo	být	k5eNaImAgNnS	být
umožněno	umožnit	k5eAaPmNgNnS	umožnit
hrát	hrát	k5eAaImF	hrát
ve	v	k7c4	v
vícero	vícero	k1gNnSc4	vícero
filmech	film	k1gInPc6	film
<g/>
,	,	kIx,	,
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
cestoval	cestovat	k5eAaImAgMnS	cestovat
převážně	převážně	k6eAd1	převážně
jen	jen	k9	jen
díky	díky	k7c3	díky
možnosti	možnost	k1gFnSc3	možnost
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
svoji	svůj	k3xOyFgFnSc4	svůj
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
manželku	manželka	k1gFnSc4	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaBmAgInS	napsat
anebo	anebo	k8xC	anebo
byl	být	k5eAaImAgInS	být
spoluautorem	spoluautor	k1gMnSc7	spoluautor
i	i	k8xC	i
několika	několik	k4yIc2	několik
prozaických	prozaický	k2eAgNnPc2d1	prozaické
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
filmový	filmový	k2eAgMnSc1d1	filmový
herec	herec	k1gMnSc1	herec
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
v	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
ve	v	k7c6	v
filmech	film	k1gInPc6	film
"	"	kIx"	"
<g/>
Miesto	Miesta	k1gMnSc5	Miesta
stretnutia	stretnutius	k1gMnSc4	stretnutius
nemožno	možno	k6eNd1	možno
zmeniť	zmeniť	k1gFnSc1	zmeniť
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Vertikála	vertikála	k1gFnSc1	vertikála
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
24	[number]	k4	24
filmech	film	k1gInPc6	film
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrál	hrát	k5eAaImAgInS	hrát
menší	malý	k2eAgInSc1d2	menší
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
až	až	k6eAd1	až
epizodní	epizodní	k2eAgFnPc4d1	epizodní
postavy	postava	k1gFnPc4	postava
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Vysockij	Vysockij	k1gMnSc1	Vysockij
patří	patřit	k5eAaImIp3nS	patřit
dodnes	dodnes	k6eAd1	dodnes
k	k	k7c3	k
nejpopulárnějším	populární	k2eAgMnPc3d3	nejpopulárnější
a	a	k8xC	a
nejtalentovanějším	talentovaný	k2eAgMnPc3d3	nejtalentovanější
umělcům	umělec	k1gMnPc3	umělec
v	v	k7c6	v
novodobých	novodobý	k2eAgFnPc6d1	novodobá
dějinách	dějiny	k1gFnPc6	dějiny
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
a	a	k8xC	a
bývalého	bývalý	k2eAgInSc2d1	bývalý
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Melodie	melodie	k1gFnSc1	melodie
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
Nerv	nerv	k1gInSc1	nerv
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
–	–	k?	–
výběr	výběr	k1gInSc1	výběr
z	z	k7c2	z
poezie	poezie	k1gFnSc2	poezie
Klíč	klíč	k1gInSc1	klíč
Básně	báseň	k1gFnSc2	báseň
a	a	k8xC	a
písně	píseň	k1gFnSc2	píseň
Tři	tři	k4xCgFnPc4	tři
čtvrtiny	čtvrtina	k1gFnPc4	čtvrtina
cesty	cesta	k1gFnSc2	cesta
Otázky	otázka	k1gFnSc2	otázka
filozofii	filozofie	k1gFnSc4	filozofie
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
Úsvit	úsvit	k1gInSc1	úsvit
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
Náš	náš	k3xOp1gMnSc1	náš
současník	současník	k1gMnSc1	současník
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
Sputnik	sputnik	k1gInSc1	sputnik
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
Koně	kůň	k1gMnSc4	kůň
k	k	k7c3	k
nezkrocení	nezkrocení	k1gNnSc3	nezkrocení
–	–	k?	–
Bulat	bulat	k5eAaImF	bulat
Okudžava	Okudžava	k1gFnSc1	Okudžava
a	a	k8xC	a
Vladimír	Vladimír	k1gMnSc1	Vladimír
Vysockij	Vysockij	k1gMnSc1	Vysockij
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Václav	Václav	k1gMnSc1	Václav
Daněk	Daněk	k1gMnSc1	Daněk
<g/>
;	;	kIx,	;
překlad	překlad	k1gInSc1	překlad
Václav	Václav	k1gMnSc1	Václav
Daněk	Daněk	k1gMnSc1	Daněk
(	(	kIx(	(
<g/>
Bulat	bulat	k5eAaImF	bulat
Okudžava	Okudžava	k1gFnSc1	Okudžava
<g/>
)	)	kIx)	)
a	a	k8xC	a
Milan	Milan	k1gMnSc1	Milan
Dvořák	Dvořák	k1gMnSc1	Dvořák
(	(	kIx(	(
<g/>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Vysockij	Vysockij	k1gMnSc1	Vysockij
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Svaz	svaz	k1gInSc1	svaz
Hudebníků	hudebník	k1gMnPc2	hudebník
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Edice	edice	k1gFnSc1	edice
Kruhu	kruh	k1gInSc2	kruh
přátel	přítel	k1gMnPc2	přítel
mladé	mladý	k2eAgFnSc2d1	mladá
hudby	hudba	k1gFnSc2	hudba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
VYSOCKIJ	VYSOCKIJ	kA	VYSOCKIJ
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
.	.	kIx.	.
</s>
<s>
Zaklínač	zaklínač	k1gMnSc1	zaklínač
hadů	had	k1gMnPc2	had
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Jana	Jan	k1gMnSc2	Jan
Moravcová	Moravcová	k1gFnSc1	Moravcová
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Lidové	lidový	k2eAgNnSc1d1	lidové
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Edice	edice	k1gFnSc1	edice
Kamarád	kamarád	k1gMnSc1	kamarád
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
ještě	ještě	k9	ještě
za	za	k7c2	za
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
několik	několik	k4yIc1	několik
desek	deska	k1gFnPc2	deska
<g/>
.	.	kIx.	.
</s>
