<s>
Katakana	Katakana	k1gFnSc1	Katakana
(	(	kIx(	(
<g/>
psáno	psát	k5eAaImNgNnS	psát
カ	カ	k?	カ
v	v	k7c6	v
katakaně	katakaně	k6eAd1	katakaně
a	a	k8xC	a
片	片	k?	片
v	v	k7c6	v
kandži	kandž	k1gFnSc6	kandž
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
(	(	kIx(	(
<g/>
druhým	druhý	k4xOgNnSc7	druhý
je	být	k5eAaImIp3nS	být
hiragana	hiragana	k1gFnSc1	hiragana
<g/>
)	)	kIx)	)
japonských	japonský	k2eAgNnPc2d1	Japonské
slabičných	slabičný	k2eAgNnPc2d1	slabičné
písem	písmo	k1gNnPc2	písmo
(	(	kIx(	(
<g/>
ve	v	k7c6	v
slabičném	slabičný	k2eAgNnSc6d1	slabičné
písmu	písmo	k1gNnSc6	písmo
jeden	jeden	k4xCgInSc1	jeden
znak	znak	k1gInSc1	znak
představuje	představovat	k5eAaImIp3nS	představovat
celou	celý	k2eAgFnSc4d1	celá
slabiku	slabika	k1gFnSc4	slabika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sestávající	sestávající	k2eAgMnSc1d1	sestávající
ze	z	k7c2	z
48	[number]	k4	48
symbolů	symbol	k1gInPc2	symbol
(	(	kIx(	(
<g/>
47	[number]	k4	47
slabičných	slabičný	k2eAgInPc2d1	slabičný
symbolů	symbol	k1gInPc2	symbol
a	a	k8xC	a
písmeno	písmeno	k1gNnSc4	písmeno
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Připojením	připojení	k1gNnSc7	připojení
dvou	dva	k4xCgFnPc2	dva
čárek	čárka	k1gFnPc2	čárka
(	(	kIx(	(
<g/>
nigori	nigori	k6eAd1	nigori
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
kroužku	kroužek	k1gInSc2	kroužek
(	(	kIx(	(
<g/>
marunigori	marunigori	k6eAd1	marunigori
<g/>
)	)	kIx)	)
nad	nad	k7c4	nad
určité	určitý	k2eAgInPc4d1	určitý
symboly	symbol	k1gInPc4	symbol
či	či	k8xC	či
jejich	jejich	k3xOp3gNnSc7	jejich
skládáním	skládání	k1gNnSc7	skládání
vznikají	vznikat	k5eAaImIp3nP	vznikat
ještě	ještě	k9	ještě
další	další	k2eAgInPc4d1	další
zvuky	zvuk	k1gInPc4	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
za	za	k7c4	za
přispění	přispění	k1gNnSc4	přispění
Kibi	Kib	k1gFnSc2	Kib
no	no	k9	no
Makibiho	Makibi	k1gMnSc4	Makibi
(	(	kIx(	(
<g/>
693	[number]	k4	693
<g/>
-	-	kIx~	-
<g/>
755	[number]	k4	755
<g/>
)	)	kIx)	)
zjednodušením	zjednodušení	k1gNnSc7	zjednodušení
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
částí	část	k1gFnPc2	část
(	(	kIx(	(
<g/>
kata	kata	k9	kata
znamená	znamenat	k5eAaImIp3nS	znamenat
japonsky	japonsky	k6eAd1	japonsky
"	"	kIx"	"
<g/>
část	část	k1gFnSc1	část
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
znaků	znak	k1gInPc2	znak
kandži	kandzat	k5eAaPmIp1nS	kandzat
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
hiragany	hiragana	k1gFnSc2	hiragana
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
zjednodušením	zjednodušení	k1gNnSc7	zjednodušení
celých	celý	k2eAgInPc2d1	celý
znaků	znak	k1gInPc2	znak
kandži	kandzat	k5eAaPmIp1nS	kandzat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yIgFnPc2	který
již	již	k6eAd1	již
předtím	předtím	k6eAd1	předtím
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
desémantizaci	desémantizace	k1gFnSc3	desémantizace
a	a	k8xC	a
fonetizaci	fonetizace	k1gFnSc3	fonetizace
(	(	kIx(	(
<g/>
ztratily	ztratit	k5eAaPmAgInP	ztratit
význam	význam	k1gInSc4	význam
a	a	k8xC	a
staly	stát	k5eAaPmAgFnP	stát
se	se	k3xPyFc4	se
pouhými	pouhý	k2eAgInPc7d1	pouhý
zvuky	zvuk	k1gInPc7	zvuk
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohly	moct	k5eAaImAgFnP	moct
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
japonské	japonský	k2eAgFnPc1d1	japonská
koncovky	koncovka	k1gFnPc1	koncovka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Znaky	znak	k1gInPc1	znak
katakany	katakana	k1gFnSc2	katakana
jsou	být	k5eAaImIp3nP	být
hranatější	hranatý	k2eAgInPc1d2	hranatější
a	a	k8xC	a
jednodušší	jednoduchý	k2eAgInPc1d2	jednodušší
než	než	k8xS	než
znaky	znak	k1gInPc1	znak
hiragany	hiragana	k1gFnSc2	hiragana
a	a	k8xC	a
cizinci	cizinec	k1gMnPc1	cizinec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
učí	učit	k5eAaImIp3nP	učit
japonsky	japonsky	k6eAd1	japonsky
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
většinou	většinou	k6eAd1	většinou
učí	učit	k5eAaImIp3nS	učit
jako	jako	k9	jako
první	první	k4xOgInSc4	první
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc4	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Katakanu	Katakana	k1gFnSc4	Katakana
původně	původně	k6eAd1	původně
používali	používat	k5eAaImAgMnP	používat
japonští	japonský	k2eAgMnPc1d1	japonský
buddhisté	buddhista	k1gMnPc1	buddhista
jako	jako	k9	jako
nápovědu	nápověda	k1gFnSc4	nápověda
pro	pro	k7c4	pro
výslovnost	výslovnost	k1gFnSc4	výslovnost
v	v	k7c6	v
buddhistických	buddhistický	k2eAgInPc6d1	buddhistický
spisech	spis	k1gInPc6	spis
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
používá	používat	k5eAaImIp3nS	používat
hiragana	hiragana	k1gFnSc1	hiragana
v	v	k7c6	v
knížkách	knížka	k1gFnPc6	knížka
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
jako	jako	k9	jako
způsob	způsob	k1gInSc4	způsob
rychlého	rychlý	k2eAgNnSc2d1	rychlé
psaní	psaní	k1gNnSc2	psaní
<g/>
.	.	kIx.	.
slova	slovo	k1gNnSc2	slovo
převzatá	převzatý	k2eAgFnSc1d1	převzatá
(	(	kIx(	(
<g/>
z	z	k7c2	z
čínštiny	čínština	k1gFnSc2	čínština
<g/>
,	,	kIx,	,
portugalštiny	portugalština	k1gFnSc2	portugalština
<g/>
,	,	kIx,	,
holandštiny	holandština	k1gFnSc2	holandština
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
dnes	dnes	k6eAd1	dnes
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
)	)	kIx)	)
onomatopoia	onomatopoion	k1gNnSc2	onomatopoion
(	(	kIx(	(
<g/>
v	v	k7c6	v
japonštině	japonština	k1gFnSc6	japonština
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
zvukomalebných	zvukomalebný	k2eAgNnPc2d1	zvukomalebné
slov	slovo	k1gNnPc2	slovo
než	než	k8xS	než
např.	např.	kA	např.
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
)	)	kIx)	)
telegramy	telegram	k1gInPc4	telegram
<g/>
,	,	kIx,	,
zdůraznění	zdůraznění	k1gNnSc1	zdůraznění
(	(	kIx(	(
<g/>
katakana	katakana	k1gFnSc1	katakana
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
typografii	typografia	k1gFnSc6	typografia
použije	použít	k5eAaPmIp3nS	použít
tučné	tučný	k2eAgNnSc4d1	tučné
písmo	písmo	k1gNnSc4	písmo
<g/>
,	,	kIx,	,
podtržení	podtržení	k1gNnSc4	podtržení
nebo	nebo	k8xC	nebo
kurzíva	kurzíva	k1gFnSc1	kurzíva
<g/>
)	)	kIx)	)
cizí	cizí	k2eAgInPc4d1	cizí
místní	místní	k2eAgInPc4d1	místní
názvy	název	k1gInPc4	název
vědecké	vědecký	k2eAgNnSc4d1	vědecké
názvosloví	názvosloví	k1gNnSc4	názvosloví
(	(	kIx(	(
<g/>
zvířata	zvíře	k1gNnPc4	zvíře
a	a	k8xC	a
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
)	)	kIx)	)
Různé	různý	k2eAgFnSc2d1	různá
TV	TV	kA	TV
relace	relace	k1gFnSc2	relace
Videohry	videohra	k1gFnSc2	videohra
Poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
Tabulka	tabulka	k1gFnSc1	tabulka
pro	pro	k7c4	pro
úplnost	úplnost	k1gFnSc4	úplnost
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
znaky	znak	k1gInPc1	znak
wi	wi	k?	wi
a	a	k8xC	a
we	we	k?	we
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
byly	být	k5eAaImAgFnP	být
r.	r.	kA	r.
1946	[number]	k4	1946
označeny	označit	k5eAaPmNgInP	označit
za	za	k7c4	za
zastaralé	zastaralý	k2eAgNnSc4d1	zastaralé
<g/>
.	.	kIx.	.
</s>
<s>
Tabulka	tabulka	k1gFnSc1	tabulka
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	s	k7c7	s
znaky	znak	k1gInPc7	znak
katakany	katakana	k1gFnSc2	katakana
píší	psát	k5eAaImIp3nP	psát
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
uspořádaná	uspořádaný	k2eAgFnSc1d1	uspořádaná
podle	podle	k7c2	podle
tradičního	tradiční	k2eAgInSc2d1	tradiční
japonského	japonský	k2eAgInSc2d1	japonský
způsobu	způsob	k1gInSc2	způsob
čtení	čtení	k1gNnSc2	čtení
<g/>
.	.	kIx.	.
</s>
<s>
Začátek	začátek	k1gInSc1	začátek
je	být	k5eAaImIp3nS	být
vpravo	vpravo	k6eAd1	vpravo
nahoře	nahoře	k6eAd1	nahoře
a	a	k8xC	a
postupuje	postupovat	k5eAaImIp3nS	postupovat
se	se	k3xPyFc4	se
shora	shora	k6eAd1	shora
dolů	dolů	k6eAd1	dolů
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgNnPc4d1	malé
čísla	číslo	k1gNnPc4	číslo
určují	určovat	k5eAaImIp3nP	určovat
pořadí	pořadí	k1gNnPc1	pořadí
tahů	tah	k1gInPc2	tah
a	a	k8xC	a
šipky	šipka	k1gFnPc1	šipka
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
jejich	jejich	k3xOp3gInSc4	jejich
směr	směr	k1gInSc4	směr
<g/>
.	.	kIx.	.
kana	kanout	k5eAaImSgInS	kanout
hiragana	hiragana	k1gFnSc1	hiragana
kandži	kandzat	k5eAaPmIp1nS	kandzat
Další	další	k2eAgInPc4d1	další
související	související	k2eAgInPc4d1	související
články	článek	k1gInPc4	článek
hentaigana	hentaigan	k1gMnSc2	hentaigan
man	mana	k1gFnPc2	mana
<g/>
'	'	kIx"	'
<g/>
jógana	jógana	k1gFnSc1	jógana
Japonsko	Japonsko	k1gNnSc1	Japonsko
japonština	japonština	k1gFnSc1	japonština
seznam	seznam	k1gInSc4	seznam
různých	různý	k2eAgNnPc2d1	různé
písem	písmo	k1gNnPc2	písmo
furigana	furigana	k1gFnSc1	furigana
Janoš	Janoš	k1gMnSc1	Janoš
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
99	[number]	k4	99
zajímavostí	zajímavost	k1gFnPc2	zajímavost
z	z	k7c2	z
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Katakana	Katakan	k1gMnSc2	Katakan
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
katakana	katakana	k1gFnSc1	katakana
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
www.kanachart.com	www.kanachart.com	k1gInSc4	www.kanachart.com
Jednoduché	jednoduchý	k2eAgNnSc4d1	jednoduché
zkoušení	zkoušení	k1gNnSc4	zkoušení
z	z	k7c2	z
katakany	katakana	k1gFnSc2	katakana
(	(	kIx(	(
<g/>
s	s	k7c7	s
volitelnými	volitelný	k2eAgFnPc7d1	volitelná
skupinami	skupina	k1gFnPc7	skupina
<g/>
)	)	kIx)	)
</s>
