<s>
Katakana	Katakana	k1gFnSc1	Katakana
(	(	kIx(	(
<g/>
psáno	psát	k5eAaImNgNnS	psát
カ	カ	k?	カ
v	v	k7c6	v
katakaně	katakaně	k6eAd1	katakaně
a	a	k8xC	a
片	片	k?	片
v	v	k7c6	v
kandži	kandž	k1gFnSc6	kandž
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
(	(	kIx(	(
<g/>
druhým	druhý	k4xOgNnSc7	druhý
je	být	k5eAaImIp3nS	být
hiragana	hiragana	k1gFnSc1	hiragana
<g/>
)	)	kIx)	)
japonských	japonský	k2eAgNnPc2d1	Japonské
slabičných	slabičný	k2eAgNnPc2d1	slabičné
písem	písmo	k1gNnPc2	písmo
(	(	kIx(	(
<g/>
ve	v	k7c6	v
slabičném	slabičný	k2eAgNnSc6d1	slabičné
písmu	písmo	k1gNnSc6	písmo
jeden	jeden	k4xCgInSc1	jeden
znak	znak	k1gInSc1	znak
představuje	představovat	k5eAaImIp3nS	představovat
celou	celý	k2eAgFnSc4d1	celá
slabiku	slabika	k1gFnSc4	slabika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sestávající	sestávající	k2eAgMnSc1d1	sestávající
ze	z	k7c2	z
48	[number]	k4	48
symbolů	symbol	k1gInPc2	symbol
(	(	kIx(	(
<g/>
47	[number]	k4	47
slabičných	slabičný	k2eAgInPc2d1	slabičný
symbolů	symbol	k1gInPc2	symbol
a	a	k8xC	a
písmeno	písmeno	k1gNnSc4	písmeno
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
