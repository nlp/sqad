<s>
Makrobiotika	makrobiotika	k1gFnSc1	makrobiotika
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
makrós	makrós	k1gInSc1	makrós
–	–	k?	–
velký	velký	k2eAgInSc1d1	velký
<g/>
,	,	kIx,	,
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
a	a	k8xC	a
bios	bios	k1gInSc1	bios
–	–	k?	–
život	život	k1gInSc1	život
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
životní	životní	k2eAgInSc4d1	životní
styl	styl	k1gInSc4	styl
a	a	k8xC	a
dietní	dietní	k2eAgInSc4d1	dietní
režim	režim	k1gInSc4	režim
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
taoistickém	taoistický	k2eAgNnSc6d1	taoistické
učení	učení	k1gNnSc6	učení
jin	jin	k?	jin
a	a	k8xC	a
jang	jang	k1gInSc1	jang
<g/>
.	.	kIx.	.
</s>
<s>
Makrobiotika	makrobiotika	k1gFnSc1	makrobiotika
jako	jako	k8xC	jako
základní	základní	k2eAgFnSc4d1	základní
potravinu	potravina	k1gFnSc4	potravina
předepisuje	předepisovat	k5eAaImIp3nS	předepisovat
obilniny	obilnina	k1gFnSc2	obilnina
a	a	k8xC	a
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
další	další	k2eAgFnPc4d1	další
potraviny	potravina	k1gFnPc4	potravina
(	(	kIx(	(
<g/>
místní	místní	k2eAgFnSc1d1	místní
zelenina	zelenina	k1gFnSc1	zelenina
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
lokální	lokální	k2eAgInPc1d1	lokální
produkty	produkt	k1gInPc1	produkt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
požívání	požívání	k1gNnSc1	požívání
průmyslově	průmyslově	k6eAd1	průmyslově
zpracovaných	zpracovaný	k2eAgFnPc2d1	zpracovaná
potravin	potravina	k1gFnPc2	potravina
a	a	k8xC	a
většinu	většina	k1gFnSc4	většina
živočišných	živočišný	k2eAgInPc2d1	živočišný
produktů	produkt	k1gInPc2	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Makrobiotické	makrobiotický	k2eAgNnSc1d1	makrobiotické
učení	učení	k1gNnSc1	učení
také	také	k9	také
omezuje	omezovat	k5eAaImIp3nS	omezovat
množství	množství	k1gNnSc1	množství
tekutin	tekutina	k1gFnPc2	tekutina
<g/>
,	,	kIx,	,
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
přejídání	přejídání	k1gNnPc4	přejídání
a	a	k8xC	a
požaduje	požadovat	k5eAaImIp3nS	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
potrava	potrava	k1gFnSc1	potrava
byla	být	k5eAaImAgFnS	být
před	před	k7c7	před
polknutím	polknutí	k1gNnSc7	polknutí
důkladně	důkladně	k6eAd1	důkladně
rozžvýkána	rozžvýkán	k2eAgFnSc1d1	rozžvýkán
<g/>
.	.	kIx.	.
</s>
<s>
Standardní	standardní	k2eAgInSc1d1	standardní
makrobiotický	makrobiotický	k2eAgInSc1d1	makrobiotický
talíř	talíř	k1gInSc1	talíř
tvoří	tvořit	k5eAaImIp3nS	tvořit
z	z	k7c2	z
50	[number]	k4	50
%	%	kIx~	%
celozrnné	celozrnný	k2eAgFnSc2d1	celozrnná
obilniny	obilnina	k1gFnSc2	obilnina
<g/>
,	,	kIx,	,
z	z	k7c2	z
30	[number]	k4	30
%	%	kIx~	%
zelenina	zelenina	k1gFnSc1	zelenina
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
tepelně	tepelně	k6eAd1	tepelně
upravená	upravený	k2eAgFnSc1d1	upravená
a	a	k8xC	a
kvašená	kvašený	k2eAgFnSc1d1	kvašená
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
15	[number]	k4	15
%	%	kIx~	%
luštěniny	luštěnina	k1gFnPc1	luštěnina
a	a	k8xC	a
mořské	mořský	k2eAgFnPc1d1	mořská
řasy	řasa	k1gFnPc1	řasa
<g/>
,	,	kIx,	,
z	z	k7c2	z
5	[number]	k4	5
%	%	kIx~	%
polévky	polévka	k1gFnPc4	polévka
<g/>
.	.	kIx.	.
</s>
<s>
Doplňkově	doplňkově	k6eAd1	doplňkově
lze	lze	k6eAd1	lze
zařadit	zařadit	k5eAaPmF	zařadit
ryby	ryba	k1gFnPc4	ryba
<g/>
,	,	kIx,	,
ovoce	ovoce	k1gNnSc4	ovoce
mírného	mírný	k2eAgNnSc2d1	mírné
pásma	pásmo	k1gNnSc2	pásmo
<g/>
,	,	kIx,	,
semena	semeno	k1gNnPc1	semeno
<g/>
,	,	kIx,	,
ořechy	ořech	k1gInPc1	ořech
<g/>
,	,	kIx,	,
oleje	olej	k1gInPc1	olej
apod.	apod.	kA	apod.
</s>
<s>
Nejstarší	starý	k2eAgNnSc1d3	nejstarší
zaznamenané	zaznamenaný	k2eAgNnSc1d1	zaznamenané
použití	použití	k1gNnSc1	použití
termínu	termín	k1gInSc2	termín
makrobiotika	makrobiotika	k1gFnSc1	makrobiotika
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Hippokratově	Hippokratův	k2eAgInSc6d1	Hippokratův
spisu	spis	k1gInSc6	spis
O	o	k7c6	o
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
,	,	kIx,	,
vodách	voda	k1gFnPc6	voda
a	a	k8xC	a
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
vlivu	vliv	k1gInSc6	vliv
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
výživy	výživa	k1gFnSc2	výživa
na	na	k7c4	na
lidské	lidský	k2eAgNnSc4d1	lidské
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
makrobiotika	makrobiotika	k1gFnSc1	makrobiotika
<g/>
"	"	kIx"	"
použil	použít	k5eAaPmAgInS	použít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1796	[number]	k4	1796
německý	německý	k2eAgMnSc1d1	německý
lékař	lékař	k1gMnSc1	lékař
Christoph	Christoph	k1gMnSc1	Christoph
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Hufeland	Hufelanda	k1gFnPc2	Hufelanda
(	(	kIx(	(
<g/>
1762	[number]	k4	1762
<g/>
–	–	k?	–
<g/>
1836	[number]	k4	1836
<g/>
)	)	kIx)	)
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Makrobiotika	makrobiotika	k1gFnSc1	makrobiotika
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
prodloužit	prodloužit	k5eAaPmF	prodloužit
lidský	lidský	k2eAgInSc4d1	lidský
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Hufeland	Hufeland	k1gInSc4	Hufeland
byl	být	k5eAaImAgMnS	být
profesorem	profesor	k1gMnSc7	profesor
lékařství	lékařství	k1gNnSc2	lékařství
v	v	k7c6	v
Jeně	Jena	k1gFnSc6	Jena
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
prvním	první	k4xOgMnSc7	první
děkanem	děkan	k1gMnSc7	děkan
medicíny	medicína	k1gFnSc2	medicína
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Hufeland	Hufeland	k1gInSc1	Hufeland
postuluje	postulovat	k5eAaImIp3nS	postulovat
"	"	kIx"	"
<g/>
životní	životní	k2eAgFnSc4d1	životní
sílu	síla	k1gFnSc4	síla
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
nachází	nacházet	k5eAaImIp3nS	nacházet
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
živých	živý	k2eAgInPc6d1	živý
organismech	organismus	k1gInPc6	organismus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
i	i	k8xC	i
v	v	k7c6	v
ostatních	ostatní	k2eAgFnPc6d1	ostatní
látkách	látka	k1gFnPc6	látka
<g/>
;	;	kIx,	;
tuto	tento	k3xDgFnSc4	tento
sílu	síla	k1gFnSc4	síla
mohou	moct	k5eAaImIp3nP	moct
posilovat	posilovat	k5eAaImF	posilovat
nebo	nebo	k8xC	nebo
oslabovat	oslabovat	k5eAaImF	oslabovat
vlivy	vliv	k1gInPc4	vliv
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Hufelandova	Hufelandův	k2eAgInSc2d1	Hufelandův
názoru	názor	k1gInSc2	názor
lze	lze	k6eAd1	lze
nemocem	nemoc	k1gFnPc3	nemoc
zabránit	zabránit	k5eAaPmF	zabránit
především	především	k9	především
správnou	správný	k2eAgFnSc7d1	správná
výživou	výživa	k1gFnSc7	výživa
a	a	k8xC	a
životosprávou	životospráva	k1gFnSc7	životospráva
<g/>
.	.	kIx.	.
</s>
<s>
Japonský	japonský	k2eAgMnSc1d1	japonský
vojenský	vojenský	k2eAgMnSc1d1	vojenský
lékař	lékař	k1gMnSc1	lékař
Sagen	Sagna	k1gFnPc2	Sagna
Išizuka	Išizuk	k1gMnSc2	Išizuk
(	(	kIx(	(
<g/>
1850	[number]	k4	1850
<g/>
–	–	k?	–
<g/>
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
profesní	profesní	k2eAgFnSc2d1	profesní
kariéry	kariéra	k1gFnSc2	kariéra
došel	dojít	k5eAaPmAgInS	dojít
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
tradiční	tradiční	k2eAgNnSc4d1	tradiční
japonské	japonský	k2eAgNnSc4d1	Japonské
lékařství	lékařství	k1gNnSc4	lékařství
je	být	k5eAaImIp3nS	být
účinnější	účinný	k2eAgFnSc1d2	účinnější
než	než	k8xS	než
západní	západní	k2eAgFnSc1d1	západní
medicína	medicína	k1gFnSc1	medicína
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc4	jeho
dietní	dietní	k2eAgNnPc4d1	dietní
doporučení	doporučení	k1gNnPc4	doporučení
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
tradiční	tradiční	k2eAgFnSc3d1	tradiční
japonské	japonský	k2eAgFnSc3d1	japonská
stravě	strava	k1gFnSc3	strava
(	(	kIx(	(
<g/>
rozumí	rozumět	k5eAaImIp3nS	rozumět
se	se	k3xPyFc4	se
před	před	k7c7	před
"	"	kIx"	"
<g/>
pozápadněním	pozápadnění	k1gNnSc7	pozápadnění
<g/>
"	"	kIx"	"
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgNnSc3	jenž
došlo	dojít	k5eAaPmAgNnS	dojít
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Išizuky	Išizuk	k1gInPc1	Išizuk
závisejí	záviset	k5eAaImIp3nP	záviset
zdraví	zdraví	k1gNnSc4	zdraví
a	a	k8xC	a
dlouhověkost	dlouhověkost	k1gFnSc4	dlouhověkost
na	na	k7c6	na
rovnováze	rovnováha	k1gFnSc6	rovnováha
mezi	mezi	k7c7	mezi
sodíkem	sodík	k1gInSc7	sodík
a	a	k8xC	a
draslíkem	draslík	k1gInSc7	draslík
a	a	k8xC	a
kyselinami	kyselina	k1gFnPc7	kyselina
a	a	k8xC	a
zásadami	zásada	k1gFnPc7	zásada
<g/>
.	.	kIx.	.
</s>
<s>
Jídlo	jídlo	k1gNnSc1	jídlo
je	být	k5eAaImIp3nS	být
určujícím	určující	k2eAgInSc7d1	určující
faktorem	faktor	k1gInSc7	faktor
této	tento	k3xDgFnSc2	tento
rovnováhy	rovnováha	k1gFnSc2	rovnováha
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc4	vliv
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
další	další	k2eAgInPc1d1	další
faktory	faktor	k1gInPc1	faktor
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
přírodní	přírodní	k2eAgFnPc1d1	přírodní
podmínky	podmínka	k1gFnPc1	podmínka
a	a	k8xC	a
podnebí	podnebí	k1gNnSc1	podnebí
<g/>
,	,	kIx,	,
fyzická	fyzický	k2eAgFnSc1d1	fyzická
aktivita	aktivita	k1gFnSc1	aktivita
nebo	nebo	k8xC	nebo
psychický	psychický	k2eAgInSc1d1	psychický
stres	stres	k1gInSc1	stres
<g/>
.	.	kIx.	.
</s>
<s>
Išizukův	Išizukův	k2eAgMnSc1d1	Išizukův
následovník	následovník	k1gMnSc1	následovník
<g/>
,	,	kIx,	,
filosof	filosof	k1gMnSc1	filosof
George	Georg	k1gMnSc2	Georg
Ohsawa	Ohsawa	k1gMnSc1	Ohsawa
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
–	–	k?	–
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
v	v	k7c6	v
chudé	chudý	k2eAgFnSc6d1	chudá
rodině	rodina	k1gFnSc6	rodina
samuraje	samuraj	k1gMnSc2	samuraj
a	a	k8xC	a
neměl	mít	k5eNaImAgInS	mít
prostředky	prostředek	k1gInPc4	prostředek
na	na	k7c4	na
vyšší	vysoký	k2eAgNnSc4d2	vyšší
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Manabu	Manab	k1gInSc2	Manab
Nišibatou	Nišibata	k1gFnSc7	Nišibata
(	(	kIx(	(
<g/>
přímým	přímý	k2eAgMnSc7d1	přímý
žákem	žák	k1gMnSc7	žák
Išizuky	Išizuk	k1gMnPc7	Išizuk
<g/>
)	)	kIx)	)
a	a	k8xC	a
studoval	studovat	k5eAaImAgMnS	studovat
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
v	v	k7c6	v
Tokiu	Tokio	k1gNnSc6	Tokio
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
cestoval	cestovat	k5eAaImAgMnS	cestovat
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začal	začít	k5eAaPmAgInS	začít
šířit	šířit	k5eAaImF	šířit
svou	svůj	k3xOyFgFnSc4	svůj
filosofii	filosofie	k1gFnSc4	filosofie
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
prý	prý	k9	prý
přijal	přijmout	k5eAaPmAgInS	přijmout
pseudonym	pseudonym	k1gInSc1	pseudonym
"	"	kIx"	"
<g/>
Ohsawa	Ohsawa	k1gFnSc1	Ohsawa
<g/>
"	"	kIx"	"
podle	podle	k7c2	podle
francouzského	francouzský	k2eAgMnSc2d1	francouzský
"	"	kIx"	"
<g/>
oh	oh	k0	oh
<g/>
,	,	kIx,	,
ça	ça	k?	ça
va	va	k0wR	va
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
obvyklá	obvyklý	k2eAgFnSc1d1	obvyklá
odpověď	odpověď	k1gFnSc1	odpověď
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
"	"	kIx"	"
<g/>
Jak	jak	k8xC	jak
se	se	k3xPyFc4	se
máš	mít	k5eAaImIp2nS	mít
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
své	svůj	k3xOyFgInPc4	svůj
pacifistické	pacifistický	k2eAgInPc4d1	pacifistický
ideály	ideál	k1gInPc4	ideál
vězněn	věznit	k5eAaImNgMnS	věznit
a	a	k8xC	a
jen	jen	k9	jen
těsně	těsně	k6eAd1	těsně
unikl	uniknout	k5eAaPmAgInS	uniknout
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
šíření	šíření	k1gNnSc6	šíření
makrobiotiky	makrobiotika	k1gFnSc2	makrobiotika
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
se	se	k3xPyFc4	se
podíleli	podílet	k5eAaImAgMnP	podílet
Ohsawovi	Ohsawův	k2eAgMnPc1d1	Ohsawův
žáci	žák	k1gMnPc1	žák
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgInPc3	jenž
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Mičio	Mičio	k6eAd1	Mičio
Kuši	kuše	k1gFnSc4	kuše
a	a	k8xC	a
Tomio	Tomio	k6eAd1	Tomio
Kikuči	Kikuč	k1gFnSc3	Kikuč
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
klubu	klub	k1gInSc2	klub
Sisyfos	Sisyfos	k1gMnSc1	Sisyfos
je	být	k5eAaImIp3nS	být
makrobiotická	makrobiotický	k2eAgFnSc1d1	makrobiotická
dieta	dieta	k1gFnSc1	dieta
"	"	kIx"	"
<g/>
jednostranná	jednostranný	k2eAgFnSc1d1	jednostranná
<g/>
,	,	kIx,	,
nevyvážená	vyvážený	k2eNgFnSc1d1	nevyvážená
a	a	k8xC	a
proto	proto	k8xC	proto
nevhodná	vhodný	k2eNgFnSc1d1	nevhodná
a	a	k8xC	a
riziková	rizikový	k2eAgFnSc1d1	riziková
<g/>
.	.	kIx.	.
</s>
<s>
Makrobiotická	makrobiotický	k2eAgFnSc1d1	makrobiotická
výživa	výživa	k1gFnSc1	výživa
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
dostatek	dostatek	k1gInSc4	dostatek
výživných	výživný	k2eAgFnPc2d1	výživná
látek	látka	k1gFnPc2	látka
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
těžké	těžký	k2eAgFnSc3d1	těžká
podvýživě	podvýživa	k1gFnSc3	podvýživa
<g/>
"	"	kIx"	"
<g/>
..	..	k?	..
Zajištění	zajištění	k1gNnSc1	zajištění
dostatečného	dostatečný	k2eAgNnSc2d1	dostatečné
množství	množství	k1gNnSc2	množství
vápníku	vápník	k1gInSc2	vápník
(	(	kIx(	(
<g/>
1	[number]	k4	1
až	až	k9	až
1,5	[number]	k4	1,5
g	g	kA	g
<g/>
)	)	kIx)	)
z	z	k7c2	z
rostlinných	rostlinný	k2eAgInPc2d1	rostlinný
zdrojů	zdroj	k1gInPc2	zdroj
je	být	k5eAaImIp3nS	být
problematické	problematický	k2eAgNnSc1d1	problematické
<g/>
.	.	kIx.	.
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
Rovněž	rovněž	k9	rovněž
vstřebávání	vstřebávání	k1gNnSc1	vstřebávání
železa	železo	k1gNnSc2	železo
je	být	k5eAaImIp3nS	být
rostlinnou	rostlinný	k2eAgFnSc7d1	rostlinná
stravou	strava	k1gFnSc7	strava
tlumeno	tlumit	k5eAaImNgNnS	tlumit
(	(	kIx(	(
<g/>
vznik	vznik	k1gInSc1	vznik
nerozpustných	rozpustný	k2eNgFnPc2d1	nerozpustná
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
)	)	kIx)	)
–	–	k?	–
přitom	přitom	k6eAd1	přitom
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
populaci	populace	k1gFnSc6	populace
ČR	ČR	kA	ČR
poměrně	poměrně	k6eAd1	poměrně
častý	častý	k2eAgInSc4d1	častý
nedostatek	nedostatek	k1gInSc4	nedostatek
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
