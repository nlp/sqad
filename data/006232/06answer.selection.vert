<s>
Makrobiotika	makrobiotika	k1gFnSc1	makrobiotika
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
makrós	makrós	k1gInSc1	makrós
–	–	k?	–
velký	velký	k2eAgInSc1d1	velký
<g/>
,	,	kIx,	,
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
a	a	k8xC	a
bios	bios	k1gInSc1	bios
–	–	k?	–
život	život	k1gInSc1	život
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
životní	životní	k2eAgInSc4d1	životní
styl	styl	k1gInSc4	styl
a	a	k8xC	a
dietní	dietní	k2eAgInSc4d1	dietní
režim	režim	k1gInSc4	režim
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
taoistickém	taoistický	k2eAgNnSc6d1	taoistické
učení	učení	k1gNnSc6	učení
jin	jin	k?	jin
a	a	k8xC	a
jang	jang	k1gInSc1	jang
<g/>
.	.	kIx.	.
</s>
