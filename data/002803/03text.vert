<s>
Franta	Franta	k1gMnSc1	Franta
Kocourek	Kocourek	k1gMnSc1	Kocourek
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
Brno-Řečkovice	Brno-Řečkovice	k1gFnSc1	Brno-Řečkovice
-	-	kIx~	-
7	[number]	k4	7
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
Vír	Vír	k1gInSc1	Vír
<g/>
,	,	kIx,	,
Hrdá	hrdý	k2eAgFnSc1d1	hrdá
Ves	ves	k1gFnSc1	ves
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
bavič	bavič	k1gMnSc1	bavič
<g/>
,	,	kIx,	,
silák	silák	k1gMnSc1	silák
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
znalec	znalec	k1gMnSc1	znalec
brněnského	brněnský	k2eAgInSc2d1	brněnský
hantecu	hantecus	k1gInSc2	hantecus
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
řadou	řada	k1gFnSc7	řada
happeningů	happening	k1gInPc2	happening
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yRgInPc2	který
dodnes	dodnes	k6eAd1	dodnes
vychází	vycházet	k5eAaImIp3nS	vycházet
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
vystoupeních	vystoupení	k1gNnPc6	vystoupení
Miroslav	Miroslav	k1gMnSc1	Miroslav
Donutil	donutit	k5eAaPmAgInS	donutit
<g/>
.	.	kIx.	.
</s>
<s>
Vyučil	vyučit	k5eAaPmAgInS	vyučit
se	se	k3xPyFc4	se
písmomalířem	písmomalíř	k1gMnSc7	písmomalíř
na	na	k7c6	na
brněnském	brněnský	k2eAgNnSc6d1	brněnské
výstavišti	výstaviště	k1gNnSc6	výstaviště
<g/>
,	,	kIx,	,
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
uhelných	uhelný	k2eAgInPc6d1	uhelný
skladech	sklad	k1gInPc6	sklad
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
mládí	mládí	k1gNnSc2	mládí
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
zápasení	zápasení	k1gNnSc2	zápasení
v	v	k7c6	v
družstvu	družstvo	k1gNnSc6	družstvo
Zbrojovky	zbrojovka	k1gFnSc2	zbrojovka
Brno	Brno	k1gNnSc4	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Postava	postava	k1gFnSc1	postava
Franty	Franta	k1gMnSc2	Franta
Kocourka	Kocourek	k1gMnSc2	Kocourek
je	být	k5eAaImIp3nS	být
neoddělitelně	oddělitelně	k6eNd1	oddělitelně
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
městem	město	k1gNnSc7	město
Brnem	Brno	k1gNnSc7	Brno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žil	žít	k5eAaImAgMnS	žít
a	a	k8xC	a
působil	působit	k5eAaImAgMnS	působit
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
ho	on	k3xPp3gInSc4	on
zastihla	zastihnout	k5eAaPmAgFnS	zastihnout
předčasně	předčasně	k6eAd1	předčasně
a	a	k8xC	a
nečekaně	nečekaně	k6eAd1	nečekaně
ve	v	k7c6	v
Víru	Vír	k1gInSc6	Vír
<g/>
.	.	kIx.	.
</s>
<s>
Selhalo	selhat	k5eAaPmAgNnS	selhat
mu	on	k3xPp3gNnSc3	on
srdce	srdce	k1gNnSc1	srdce
<g/>
,	,	kIx,	,
když	když	k8xS	když
seděl	sedět	k5eAaImAgMnS	sedět
s	s	k7c7	s
přáteli	přítel	k1gMnPc7	přítel
v	v	k7c6	v
restauraci	restaurace	k1gFnSc6	restaurace
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
si	se	k3xPyFc3	se
tehdy	tehdy	k6eAd1	tehdy
myslelo	myslet	k5eAaImAgNnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
další	další	k2eAgFnSc1d1	další
recese	recese	k1gFnSc1	recese
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
realita	realita	k1gFnSc1	realita
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
posledního	poslední	k2eAgInSc2d1	poslední
odpočinku	odpočinek	k1gInSc2	odpočinek
našel	najít	k5eAaPmAgMnS	najít
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
v	v	k7c6	v
Brně-Řečkovicích	Brně-Řečkovice	k1gFnPc6	Brně-Řečkovice
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
A	A	kA	A
<g/>
/	/	kIx~	/
<g/>
103	[number]	k4	103
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
hůř	zle	k6eAd2	zle
<g/>
,	,	kIx,	,
když	když	k8xS	když
padnou	padnout	k5eAaPmIp3nP	padnout
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
Pavlínka	Pavlínka	k1gFnSc1	Pavlínka
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
Hudba	hudba	k1gFnSc1	hudba
kolonád	kolonáda	k1gFnPc2	kolonáda
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
Balada	balada	k1gFnSc1	balada
pro	pro	k7c4	pro
banditu	bandita	k1gMnSc4	bandita
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
Šahrazád	Šahrazád	k1gFnSc2	Šahrazád
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
Kára	kára	k1gFnSc1	kára
plná	plný	k2eAgFnSc1d1	plná
bolesti	bolest	k1gFnSc2	bolest
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
Antonyho	Antony	k1gMnSc2	Antony
šance	šance	k1gFnSc1	šance
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
Páni	pan	k1gMnPc1	pan
Edisoni	Edisoň	k1gFnSc6	Edisoň
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
Smrt	smrt	k1gFnSc1	smrt
v	v	k7c6	v
kruhu	kruh	k1gInSc6	kruh
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
Černá	černý	k2eAgFnSc1d1	černá
Fortuna	Fortuna	k1gFnSc1	Fortuna
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
</s>
