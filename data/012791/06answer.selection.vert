<s>
Generál	generál	k1gMnSc1	generál
Dwight	Dwight	k1gMnSc1	Dwight
David	David	k1gMnSc1	David
Eisenhower	Eisenhower	k1gMnSc1	Eisenhower
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
též	též	k9	též
jako	jako	k9	jako
Ike	Ike	k1gFnSc1	Ike
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1890	[number]	k4	1890
–	–	k?	–
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
pětihvězdičkový	pětihvězdičkový	k2eAgMnSc1d1	pětihvězdičkový
generál	generál	k1gMnSc1	generál
a	a	k8xC	a
34	[number]	k4	34
<g/>
.	.	kIx.	.
prezident	prezident	k1gMnSc1	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
