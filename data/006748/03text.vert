<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Severní	severní	k2eAgFnSc2d1	severní
Koreje	Korea	k1gFnSc2	Korea
formálně	formálně	k6eAd1	formálně
začínají	začínat	k5eAaImIp3nP	začínat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
konci	konec	k1gInSc6	konec
japonské	japonský	k2eAgFnSc2d1	japonská
okupace	okupace	k1gFnSc2	okupace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
skončila	skončit	k5eAaPmAgFnS	skončit
souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
druhou	druhý	k4xOgFnSc7	druhý
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Korea	Korea	k1gFnSc1	Korea
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
zhruba	zhruba	k6eAd1	zhruba
podle	podle	k7c2	podle
38	[number]	k4	38
<g/>
.	.	kIx.	.
rovnoběžky	rovnoběžka	k1gFnSc2	rovnoběžka
na	na	k7c6	na
Severní	severní	k2eAgFnSc6d1	severní
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
SSSR	SSSR	kA	SSSR
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc2d1	jižní
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
celosvětovou	celosvětový	k2eAgFnSc4d1	celosvětová
snahu	snaha	k1gFnSc4	snaha
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
velmoci	velmoc	k1gFnPc1	velmoc
nedohodly	dohodnout	k5eNaPmAgFnP	dohodnout
na	na	k7c6	na
společném	společný	k2eAgNnSc6d1	společné
vedení	vedení	k1gNnSc6	vedení
<g/>
,	,	kIx,	,
a	a	k8xC	a
vytvořily	vytvořit	k5eAaPmAgInP	vytvořit
tak	tak	k9	tak
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
či	či	k8xC	či
Německu	Německo	k1gNnSc6	Německo
<g/>
)	)	kIx)	)
dvě	dva	k4xCgFnPc1	dva
oddělené	oddělený	k2eAgFnPc1d1	oddělená
vlády	vláda	k1gFnPc1	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
vlád	vláda	k1gFnPc2	vláda
si	se	k3xPyFc3	se
nárokovala	nárokovat	k5eAaImAgFnS	nárokovat
legitimní	legitimní	k2eAgFnSc4d1	legitimní
vládu	vláda	k1gFnSc4	vláda
nad	nad	k7c7	nad
celým	celý	k2eAgInSc7d1	celý
korejským	korejský	k2eAgInSc7d1	korejský
poloostrovem	poloostrov	k1gInSc7	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
(	(	kIx(	(
<g/>
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
Korejská	korejský	k2eAgFnSc1d1	Korejská
lidově	lidově	k6eAd1	lidově
demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1948	[number]	k4	1948
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
okupujících	okupující	k2eAgFnPc2d1	okupující
sovětských	sovětský	k2eAgFnPc2d1	sovětská
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
zde	zde	k6eAd1	zde
usilovalo	usilovat	k5eAaImAgNnS	usilovat
o	o	k7c4	o
moc	moc	k1gFnSc4	moc
spoustu	spoustu	k6eAd1	spoustu
různých	různý	k2eAgFnPc2d1	různá
<g/>
,	,	kIx,	,
levicových	levicový	k2eAgFnPc2d1	levicová
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
však	však	k9	však
byla	být	k5eAaImAgFnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
původní	původní	k2eAgFnSc1d1	původní
Korejská	korejský	k2eAgFnSc1d1	Korejská
komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
čele	čelo	k1gNnSc6	čelo
stáli	stát	k5eAaImAgMnP	stát
Korejci	Korejec	k1gMnPc1	Korejec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
žili	žít	k5eAaImAgMnP	žít
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
či	či	k8xC	či
Čínské	čínský	k2eAgFnSc6d1	čínská
lidové	lidový	k2eAgFnSc6d1	lidová
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
a	a	k8xC	a
měli	mít	k5eAaImAgMnP	mít
tak	tak	k6eAd1	tak
již	již	k6eAd1	již
spoustu	spousta	k1gFnSc4	spousta
zkušeností	zkušenost	k1gFnPc2	zkušenost
s	s	k7c7	s
praktikami	praktika	k1gFnPc7	praktika
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1948	[number]	k4	1948
se	se	k3xPyFc4	se
vůdčí	vůdčí	k2eAgMnSc1d1	vůdčí
komunista	komunista	k1gMnSc1	komunista
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
<g/>
,	,	kIx,	,
Kim	Kim	k1gFnSc6	Kim
Ir-sen	Irno	k1gNnPc2	Ir-sno
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgInS	stát
hlavou	hlava	k1gFnSc7	hlava
severokorejské	severokorejský	k2eAgFnSc2d1	severokorejská
lidové	lidový	k2eAgFnSc2d1	lidová
prozatímní	prozatímní	k2eAgFnSc2d1	prozatímní
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
formálně	formálně	k6eAd1	formálně
stabilizovat	stabilizovat	k5eAaBmF	stabilizovat
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Kimův	Kimův	k2eAgInSc1d1	Kimův
vzestup	vzestup	k1gInSc1	vzestup
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
lze	lze	k6eAd1	lze
přičíst	přičíst	k5eAaPmF	přičíst
jeho	jeho	k3xOp3gFnPc4	jeho
schopnosti	schopnost	k1gFnPc4	schopnost
zaujmout	zaujmout	k5eAaPmF	zaujmout
běžné	běžný	k2eAgNnSc4d1	běžné
venkovské	venkovský	k2eAgNnSc4d1	venkovské
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dalších	další	k2eAgInPc6d1	další
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
už	už	k6eAd1	už
oficiálních	oficiální	k2eAgFnPc6d1	oficiální
volbách	volba	k1gFnPc6	volba
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
premiérem	premiér	k1gMnSc7	premiér
a	a	k8xC	a
zemi	zem	k1gFnSc6	zem
vládl	vládnout	k5eAaImAgInS	vládnout
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
post	post	k1gInSc4	post
zastával	zastávat	k5eAaImAgMnS	zastávat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Vládnoucí	vládnoucí	k2eAgFnSc7d1	vládnoucí
stranou	strana	k1gFnSc7	strana
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
stala	stát	k5eAaPmAgFnS	stát
Korejská	korejský	k2eAgFnSc1d1	Korejská
dělnická	dělnický	k2eAgFnSc1d1	Dělnická
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
předsedou	předseda	k1gMnSc7	předseda
byl	být	k5eAaImAgMnS	být
právě	právě	k9	právě
Kim	Kim	k1gMnSc1	Kim
Ir-sen	Iren	k2eAgMnSc1d1	Ir-sen
<g/>
.	.	kIx.	.
</s>
<s>
Kimova	Kimův	k2eAgFnSc1d1	Kimova
vláda	vláda	k1gFnSc1	vláda
začala	začít	k5eAaPmAgFnS	začít
rychle	rychle	k6eAd1	rychle
zavádět	zavádět	k5eAaImF	zavádět
sovětský	sovětský	k2eAgInSc4d1	sovětský
model	model	k1gInSc4	model
politického	politický	k2eAgInSc2d1	politický
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
KDS	KDS	kA	KDS
(	(	kIx(	(
<g/>
Korejská	korejský	k2eAgFnSc1d1	Korejská
dělnická	dělnický	k2eAgFnSc1d1	Dělnická
strana	strana	k1gFnSc1	strana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
ekonomiky	ekonomika	k1gFnSc2	ekonomika
řízené	řízený	k2eAgFnSc2d1	řízená
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
produktivity	produktivita	k1gFnSc2	produktivita
země	zem	k1gFnSc2	zem
tvořily	tvořit	k5eAaImAgFnP	tvořit
japonské	japonský	k2eAgFnPc1d1	japonská
a	a	k8xC	a
jihokorejské	jihokorejský	k2eAgFnPc1d1	jihokorejská
firmy	firma	k1gFnPc1	firma
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
označeny	označit	k5eAaPmNgInP	označit
za	za	k7c4	za
kolaborantské	kolaborantský	k2eAgNnSc4d1	kolaborantské
<g/>
.	.	kIx.	.
</s>
<s>
Znárodnění	znárodnění	k1gNnSc1	znárodnění
probíhalo	probíhat	k5eAaImAgNnS	probíhat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pod	pod	k7c4	pod
stát	stát	k1gInSc4	stát
spadalo	spadat	k5eAaPmAgNnS	spadat
již	již	k9	již
70	[number]	k4	70
%	%	kIx~	%
veškerého	veškerý	k3xTgInSc2	veškerý
kapitálu	kapitál	k1gInSc2	kapitál
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
90	[number]	k4	90
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byly	být	k5eAaImAgFnP	být
prakticky	prakticky	k6eAd1	prakticky
všechny	všechen	k3xTgFnPc1	všechen
továrny	továrna	k1gFnPc1	továrna
a	a	k8xC	a
kapitál	kapitál	k1gInSc1	kapitál
ovládány	ovládat	k5eAaImNgInP	ovládat
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
tolik	tolik	k6eAd1	tolik
nepokročilo	pokročit	k5eNaPmAgNnS	pokročit
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
reforma	reforma	k1gFnSc1	reforma
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
přerozdělila	přerozdělit	k5eAaPmAgFnS	přerozdělit
veliké	veliký	k2eAgNnSc4d1	veliké
množství	množství	k1gNnSc4	množství
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
půdy	půda	k1gFnSc2	půda
bezzemným	bezzemný	k2eAgMnSc7d1	bezzemný
Rolníkům	rolník	k1gMnPc3	rolník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
však	však	k9	však
přišla	přijít	k5eAaPmAgFnS	přijít
částečná	částečný	k2eAgFnSc1d1	částečná
kolektivizace	kolektivizace	k1gFnSc1	kolektivizace
i	i	k9	i
do	do	k7c2	do
tohoto	tento	k3xDgNnSc2	tento
odvětví	odvětví	k1gNnSc2	odvětví
a	a	k8xC	a
40	[number]	k4	40
%	%	kIx~	%
z	z	k7c2	z
výnosu	výnos	k1gInSc2	výnos
muselo	muset	k5eAaImAgNnS	muset
být	být	k5eAaImF	být
odevzdáváno	odevzdávat	k5eAaImNgNnS	odevzdávat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
už	už	k6eAd1	už
všichni	všechen	k3xTgMnPc1	všechen
rolníci	rolník	k1gMnPc1	rolník
přešli	přejít	k5eAaPmAgMnP	přejít
pod	pod	k7c4	pod
centrálně	centrálně	k6eAd1	centrálně
spravované	spravovaný	k2eAgInPc4d1	spravovaný
celky	celek	k1gInPc4	celek
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
všechny	všechen	k3xTgInPc1	všechen
poválečné	poválečný	k2eAgInPc1d1	poválečný
komunistické	komunistický	k2eAgInPc1d1	komunistický
státy	stát	k1gInPc1	stát
se	se	k3xPyFc4	se
i	i	k9	i
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
pustila	pustit	k5eAaPmAgFnS	pustit
do	do	k7c2	do
masivního	masivní	k2eAgInSc2d1	masivní
těžkého	těžký	k2eAgInSc2d1	těžký
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
státní	státní	k2eAgFnSc2d1	státní
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
a	a	k8xC	a
vojenského	vojenský	k2eAgInSc2d1	vojenský
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
zanedbávajíc	zanedbávat	k5eAaImSgNnS	zanedbávat
produkci	produkce	k1gFnSc4	produkce
spotřebního	spotřební	k2eAgNnSc2d1	spotřební
zboží	zboží	k1gNnSc2	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělcům	zemědělec	k1gMnPc3	zemědělec
byly	být	k5eAaImAgInP	být
placené	placený	k2eAgFnSc2d1	placená
pouze	pouze	k6eAd1	pouze
nízké	nízký	k2eAgFnSc2d1	nízká
ceny	cena	k1gFnSc2	cena
stanovené	stanovený	k2eAgFnSc2d1	stanovená
státem	stát	k1gInSc7	stát
a	a	k8xC	a
všechny	všechen	k3xTgInPc1	všechen
přebytky	přebytek	k1gInPc1	přebytek
šly	jít	k5eAaImAgInP	jít
do	do	k7c2	do
těžkého	těžký	k2eAgInSc2d1	těžký
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
začal	začít	k5eAaPmAgInS	začít
plánovat	plánovat	k5eAaImF	plánovat
pomocí	pomocí	k7c2	pomocí
systému	systém	k1gInSc2	systém
tříletek	tříletka	k1gFnPc2	tříletka
a	a	k8xC	a
47	[number]	k4	47
<g/>
%	%	kIx~	%
průmyslový	průmyslový	k2eAgInSc1d1	průmyslový
podíl	podíl	k1gInSc1	podíl
na	na	k7c6	na
ekonomice	ekonomika	k1gFnSc6	ekonomika
státu	stát	k1gInSc2	stát
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
na	na	k7c4	na
70	[number]	k4	70
<g/>
%	%	kIx~	%
navzdory	navzdory	k7c3	navzdory
Korejské	korejský	k2eAgFnSc3d1	Korejská
válce	válka	k1gFnSc3	válka
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
měla	mít	k5eAaImAgFnS	mít
obrovský	obrovský	k2eAgInSc4d1	obrovský
nárůst	nárůst	k1gInSc4	nárůst
v	v	k7c6	v
produkci	produkce	k1gFnSc6	produkce
elektřiny	elektřina	k1gFnSc2	elektřina
<g/>
,	,	kIx,	,
oceli	ocel	k1gFnSc2	ocel
a	a	k8xC	a
strojírenského	strojírenský	k2eAgInSc2d1	strojírenský
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
němuž	jenž	k3xRgNnSc3	jenž
stoupala	stoupat	k5eAaImAgFnS	stoupat
efektivita	efektivita	k1gFnSc1	efektivita
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
byla	být	k5eAaImAgFnS	být
ekonomika	ekonomika	k1gFnSc1	ekonomika
země	země	k1gFnSc1	země
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
zcela	zcela	k6eAd1	zcela
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
na	na	k7c6	na
cizí	cizí	k2eAgFnSc6d1	cizí
pomoci	pomoc	k1gFnSc6	pomoc
a	a	k8xC	a
překonala	překonat	k5eAaPmAgFnS	překonat
dokonce	dokonce	k9	dokonce
své	svůj	k3xOyFgMnPc4	svůj
rivaly	rival	k1gMnPc4	rival
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Koreji	Korea	k1gFnSc6	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
relativně	relativně	k6eAd1	relativně
stoupal	stoupat	k5eAaImAgMnS	stoupat
<g/>
,	,	kIx,	,
přinejmenším	přinejmenším	k6eAd1	přinejmenším
ve	v	k7c6	v
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
životní	životní	k2eAgInPc4d1	životní
standard	standard	k1gInSc4	standard
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
lékařská	lékařský	k2eAgFnSc1d1	lékařská
pomoc	pomoc	k1gFnSc1	pomoc
byla	být	k5eAaImAgFnS	být
už	už	k6eAd1	už
na	na	k7c6	na
lepší	dobrý	k2eAgFnSc6d2	lepší
úrovni	úroveň	k1gFnSc6	úroveň
než	než	k8xS	než
před	před	k7c7	před
2	[number]	k4	2
<g/>
.	.	kIx.	.
sv.	sv.	kA	sv.
válkou	válka	k1gFnSc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
byl	být	k5eAaImAgInS	být
všude	všude	k6eAd1	všude
kritický	kritický	k2eAgInSc1d1	kritický
nedostatek	nedostatek	k1gInSc1	nedostatek
spotřebního	spotřební	k2eAgNnSc2d1	spotřební
zboží	zboží	k1gNnSc2	zboží
a	a	k8xC	a
populace	populace	k1gFnSc1	populace
žila	žít	k5eAaImAgFnS	žít
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
stálého	stálý	k2eAgNnSc2d1	stálé
zvyšování	zvyšování	k1gNnSc2	zvyšování
produktivity	produktivita	k1gFnSc2	produktivita
<g/>
.	.	kIx.	.
</s>
<s>
Upevnění	upevnění	k1gNnSc1	upevnění
moci	moc	k1gFnSc2	moc
I	i	k8xC	i
Sung-mana	Sungan	k1gMnSc2	Sung-man
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
americké	americký	k2eAgFnSc2d1	americká
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
potlačení	potlačení	k1gNnSc2	potlačení
říjnového	říjnový	k2eAgNnSc2d1	říjnové
povstání	povstání	k1gNnSc2	povstání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
ukončilo	ukončit	k5eAaPmAgNnS	ukončit
naděje	naděje	k1gFnPc4	naděje
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
země	země	k1gFnSc1	země
mohla	moct	k5eAaImAgFnS	moct
ubírat	ubírat	k5eAaImF	ubírat
stejnou	stejný	k2eAgFnSc7d1	stejná
revoluční	revoluční	k2eAgFnSc7d1	revoluční
cestou	cesta	k1gFnSc7	cesta
jako	jako	k8xS	jako
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
.	.	kIx.	.
</s>
<s>
Kim	Kim	k?	Kim
proto	proto	k8xC	proto
hledal	hledat	k5eAaImAgMnS	hledat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
podporu	podpor	k1gInSc2	podpor
u	u	k7c2	u
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
SSSR	SSSR	kA	SSSR
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
sjednotit	sjednotit	k5eAaPmF	sjednotit
zemi	zem	k1gFnSc4	zem
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Stažením	stažení	k1gNnSc7	stažení
většiny	většina	k1gFnSc2	většina
amerických	americký	k2eAgFnPc2d1	americká
jednotek	jednotka	k1gFnPc2	jednotka
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1949	[number]	k4	1949
zůstala	zůstat	k5eAaPmAgFnS	zůstat
země	země	k1gFnSc1	země
prakticky	prakticky	k6eAd1	prakticky
nebráněna	bráněn	k2eNgFnSc1d1	bráněn
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
složkou	složka	k1gFnSc7	složka
armády	armáda	k1gFnSc2	armáda
byli	být	k5eAaImAgMnP	být
nyní	nyní	k6eAd1	nyní
pouze	pouze	k6eAd1	pouze
vojáci	voják	k1gMnPc1	voják
se	s	k7c7	s
základní	základní	k2eAgFnSc7d1	základní
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
službou	služba	k1gFnSc7	služba
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgMnSc3	ten
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
měla	mít	k5eAaImAgFnS	mít
veterány	veterán	k1gMnPc4	veterán
z	z	k7c2	z
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
partyzány	partyzána	k1gFnSc2	partyzána
z	z	k7c2	z
anti-japonského	antiaponský	k2eAgInSc2d1	anti-japonský
odboje	odboj	k1gInSc2	odboj
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
podporovaná	podporovaný	k2eAgFnSc1d1	podporovaná
sovětskou	sovětský	k2eAgFnSc7d1	sovětská
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
Stalin	Stalin	k1gMnSc1	Stalin
odmítal	odmítat	k5eAaImAgMnS	odmítat
Kimovy	Kimův	k2eAgInPc4d1	Kimův
požadavky	požadavek	k1gInPc4	požadavek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
zvítězili	zvítězit	k5eAaPmAgMnP	zvítězit
komunisté	komunista	k1gMnPc1	komunista
a	a	k8xC	a
SSSR	SSSR	kA	SSSR
vyrobil	vyrobit	k5eAaPmAgInS	vyrobit
první	první	k4xOgFnSc4	první
atomovou	atomový	k2eAgFnSc4d1	atomová
bombu	bomba	k1gFnSc4	bomba
<g/>
,	,	kIx,	,
přehodnotil	přehodnotit	k5eAaPmAgMnS	přehodnotit
Kimovu	Kimův	k2eAgFnSc4d1	Kimova
nabídku	nabídka	k1gFnSc4	nabídka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1950	[number]	k4	1950
schválil	schválit	k5eAaPmAgMnS	schválit
Stalin	Stalin	k1gMnSc1	Stalin
myšlenku	myšlenka	k1gFnSc4	myšlenka
ovládnutí	ovládnutí	k1gNnSc2	ovládnutí
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Sověti	Sovět	k1gMnPc1	Sovět
poskytli	poskytnout	k5eAaPmAgMnP	poskytnout
omezenou	omezený	k2eAgFnSc4d1	omezená
pomoc	pomoc	k1gFnSc4	pomoc
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
rádců	rádce	k1gMnPc2	rádce
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
pomohli	pomoct	k5eAaPmAgMnP	pomoct
celou	celý	k2eAgFnSc4d1	celá
operaci	operace	k1gFnSc4	operace
naplánovat	naplánovat	k5eAaBmF	naplánovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
asi	asi	k9	asi
400	[number]	k4	400
instruktorů	instruktor	k1gMnPc2	instruktor
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
trénovali	trénovat	k5eAaImAgMnP	trénovat
některé	některý	k3yIgFnPc4	některý
elitní	elitní	k2eAgFnPc4d1	elitní
Korejské	korejský	k2eAgFnPc4d1	Korejská
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Stalin	Stalin	k1gMnSc1	Stalin
se	se	k3xPyFc4	se
však	však	k9	však
nechtěl	chtít	k5eNaImAgMnS	chtít
pouštět	pouštět	k5eAaImF	pouštět
do	do	k7c2	do
přímé	přímý	k2eAgFnSc2d1	přímá
konfrontace	konfrontace	k1gFnSc2	konfrontace
s	s	k7c7	s
USA	USA	kA	USA
přes	přes	k7c4	přes
Koreu	Korea	k1gFnSc4	Korea
a	a	k8xC	a
neposkytl	poskytnout	k5eNaPmAgMnS	poskytnout
tak	tak	k6eAd1	tak
žádné	žádný	k3yNgFnPc4	žádný
jednotky	jednotka	k1gFnPc4	jednotka
ani	ani	k8xC	ani
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nutnosti	nutnost	k1gFnSc2	nutnost
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
zinscenované	zinscenovaný	k2eAgNnSc1d1	zinscenované
jakožto	jakožto	k8xS	jakožto
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
konkurenčními	konkurenční	k2eAgInPc7d1	konkurenční
režimy	režim	k1gInPc7	režim
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
před	před	k7c7	před
útokem	útok	k1gInSc7	útok
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
malým	malý	k2eAgFnPc3d1	malá
potyčkám	potyčka	k1gFnPc3	potyčka
na	na	k7c6	na
území	území	k1gNnSc6	území
podél	podél	k7c2	podél
38	[number]	k4	38
<g/>
.	.	kIx.	.
rovnoběžky	rovnoběžka	k1gFnSc2	rovnoběžka
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
obě	dva	k4xCgFnPc1	dva
země	zem	k1gFnPc1	zem
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1950	[number]	k4	1950
severní	severní	k2eAgFnSc2d1	severní
jednotky	jednotka	k1gFnSc2	jednotka
vystupňovaly	vystupňovat	k5eAaPmAgInP	vystupňovat
tlak	tlak	k1gInSc4	tlak
a	a	k8xC	a
začaly	začít	k5eAaPmAgFnP	začít
přecházet	přecházet	k5eAaImF	přecházet
toto	tento	k3xDgNnSc4	tento
pomyslné	pomyslný	k2eAgNnSc4d1	pomyslné
území	území	k1gNnSc4	území
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
počtu	počet	k1gInSc6	počet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
překvapením	překvapení	k1gNnSc7	překvapení
<g/>
,	,	kIx,	,
lepší	dobrý	k2eAgFnSc7d2	lepší
armádou	armáda	k1gFnSc7	armáda
a	a	k8xC	a
naprostou	naprostý	k2eAgFnSc7d1	naprostá
neschopností	neschopnost	k1gFnSc7	neschopnost
jihokorejské	jihokorejský	k2eAgFnSc2d1	jihokorejská
armády	armáda	k1gFnSc2	armáda
byl	být	k5eAaImAgInS	být
rychle	rychle	k6eAd1	rychle
dobyt	dobyt	k2eAgInSc1d1	dobyt
Soul	Soul	k1gInSc1	Soul
<g/>
,	,	kIx,	,
I	i	k9	i
Sung-man	Sungan	k1gMnSc1	Sung-man
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
vláda	vláda	k1gFnSc1	vláda
byli	být	k5eAaImAgMnP	být
nuceni	nutit	k5eAaImNgMnP	nutit
prchnout	prchnout	k5eAaPmF	prchnout
daleko	daleko	k6eAd1	daleko
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
nebyla	být	k5eNaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
ovládnout	ovládnout	k5eAaPmF	ovládnout
celý	celý	k2eAgInSc4d1	celý
poloostrov	poloostrov	k1gInSc4	poloostrov
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
do	do	k7c2	do
války	válka	k1gFnSc2	válka
vstoupily	vstoupit	k5eAaPmAgFnP	vstoupit
jednotky	jednotka	k1gFnPc1	jednotka
OSN	OSN	kA	OSN
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
už	už	k6eAd1	už
byly	být	k5eAaImAgFnP	být
všechny	všechen	k3xTgFnPc1	všechen
severní	severní	k2eAgFnPc1d1	severní
jednotky	jednotka	k1gFnPc1	jednotka
vytlačeny	vytlačen	k2eAgFnPc1d1	vytlačena
ze	z	k7c2	z
Soulu	Soul	k1gInSc2	Soul
a	a	k8xC	a
spojenci	spojenec	k1gMnPc1	spojenec
dokonce	dokonce	k9	dokonce
obsadili	obsadit	k5eAaPmAgMnP	obsadit
Pchjongjang	Pchjongjang	k1gInSc4	Pchjongjang
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
však	však	k9	však
přišel	přijít	k5eAaPmAgMnS	přijít
další	další	k2eAgInSc4d1	další
zvrat	zvrat	k1gInSc4	zvrat
–	–	k?	–
účast	účast	k1gFnSc4	účast
vojsk	vojsko	k1gNnPc2	vojsko
Čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
prosince	prosinec	k1gInSc2	prosinec
bylo	být	k5eAaImAgNnS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
severu	sever	k1gInSc2	sever
opět	opět	k6eAd1	opět
v	v	k7c6	v
komunistických	komunistický	k2eAgFnPc6d1	komunistická
rukou	ruka	k1gFnPc6	ruka
a	a	k8xC	a
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1951	[number]	k4	1951
komunisté	komunista	k1gMnPc1	komunista
na	na	k7c4	na
pár	pár	k4xCyI	pár
dnů	den	k1gInPc2	den
dokonce	dokonce	k9	dokonce
obsadili	obsadit	k5eAaPmAgMnP	obsadit
Soul	Soul	k1gInSc4	Soul
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
se	se	k3xPyFc4	se
tak	tak	k9	tak
na	na	k7c4	na
2	[number]	k4	2
roky	rok	k1gInPc7	rok
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
slepé	slepý	k2eAgFnSc2d1	slepá
uličky	ulička	k1gFnSc2	ulička
a	a	k8xC	a
armády	armáda	k1gFnSc2	armáda
se	se	k3xPyFc4	se
přetahovaly	přetahovat	k5eAaImAgInP	přetahovat
o	o	k7c4	o
každý	každý	k3xTgInSc4	každý
kilometr	kilometr	k1gInSc4	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Fronta	fronta	k1gFnSc1	fronta
se	se	k3xPyFc4	se
stabilizovala	stabilizovat	k5eAaBmAgFnS	stabilizovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
na	na	k7c6	na
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stalo	stát	k5eAaPmAgNnS	stát
mírovou	mírový	k2eAgFnSc7d1	mírová
zónou	zóna	k1gFnSc7	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dlouhých	dlouhý	k2eAgNnPc6d1	dlouhé
jednáních	jednání	k1gNnPc6	jednání
se	se	k3xPyFc4	se
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
dohodly	dohodnout	k5eAaPmAgFnP	dohodnout
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
demilitarizovaném	demilitarizovaný	k2eAgNnSc6d1	demilitarizované
území	území	k1gNnSc6	území
(	(	kIx(	(
<g/>
zónou	zóna	k1gFnSc7	zóna
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
KDZ	KDZ	kA	KDZ
<g/>
)	)	kIx)	)
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
klid	klid	k1gInSc1	klid
zbraní	zbraň	k1gFnPc2	zbraň
(	(	kIx(	(
<g/>
příměří	příměří	k1gNnSc2	příměří
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
oficiální	oficiální	k2eAgFnSc1d1	oficiální
mírová	mírový	k2eAgFnSc1d1	mírová
smlouva	smlouva	k1gFnSc1	smlouva
nebyla	být	k5eNaImAgFnS	být
nikdy	nikdy	k6eAd1	nikdy
podepsána	podepsán	k2eAgFnSc1d1	podepsána
a	a	k8xC	a
Severní	severní	k2eAgFnSc1d1	severní
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
se	se	k3xPyFc4	se
technicky	technicky	k6eAd1	technicky
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
válečném	válečný	k2eAgInSc6d1	válečný
stavu	stav	k1gInSc6	stav
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
až	až	k9	až
do	do	k7c2	do
současné	současný	k2eAgFnSc2d1	současná
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
upevnil	upevnit	k5eAaPmAgInS	upevnit
Kim	Kim	k1gFnSc4	Kim
pomocí	pomocí	k7c2	pomocí
oddané	oddaný	k2eAgFnSc2d1	oddaná
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ho	on	k3xPp3gMnSc4	on
respektovala	respektovat	k5eAaImAgFnS	respektovat
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc4	svůj
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
hlavní	hlavní	k2eAgMnSc1d1	hlavní
protivník	protivník	k1gMnSc1	protivník
Pak	pak	k8xC	pak
Hon-jong	Honong	k1gMnSc1	Hon-jong
byl	být	k5eAaImAgMnS	být
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
pomoci	pomoc	k1gFnSc2	pomoc
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreji	Korea	k1gFnSc3	Korea
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
války	válka	k1gFnSc2	válka
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
zřejmě	zřejmě	k6eAd1	zřejmě
popraven	popravit	k5eAaPmNgInS	popravit
<g/>
.	.	kIx.	.
</s>
<s>
Spousta	spousta	k1gFnSc1	spousta
levicových	levicový	k2eAgMnPc2d1	levicový
přeběhlíků	přeběhlík	k1gMnPc2	přeběhlík
z	z	k7c2	z
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
obviněna	obvinit	k5eAaPmNgFnS	obvinit
ze	z	k7c2	z
špionáže	špionáž	k1gFnSc2	špionáž
či	či	k8xC	či
jiných	jiný	k2eAgInPc2d1	jiný
zločinů	zločin	k1gInPc2	zločin
<g/>
,	,	kIx,	,
a	a	k8xC	a
uvězněna	uvězněn	k2eAgFnSc1d1	uvězněna
<g/>
,	,	kIx,	,
deportována	deportován	k2eAgFnSc1d1	deportována
do	do	k7c2	do
pracovních	pracovní	k2eAgFnPc2d1	pracovní
vesnic	vesnice	k1gFnPc2	vesnice
nebo	nebo	k8xC	nebo
zabita	zabit	k2eAgFnSc1d1	zabita
<g/>
.	.	kIx.	.
</s>
<s>
Potenciální	potenciální	k2eAgMnPc1d1	potenciální
rivalové	rival	k1gMnPc1	rival
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
Kim	Kim	k1gMnPc1	Kim
T-bong	Tonga	k1gFnPc2	T-bonga
byli	být	k5eAaImAgMnP	být
také	také	k6eAd1	také
popraveni	popravit	k5eAaPmNgMnP	popravit
<g/>
.	.	kIx.	.
</s>
<s>
Tříletka	tříletka	k1gFnSc1	tříletka
1954-1956	[number]	k4	1954-1956
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
napravit	napravit	k5eAaPmF	napravit
masivní	masivní	k2eAgNnSc4d1	masivní
poničení	poničení	k1gNnSc4	poničení
země	zem	k1gFnSc2	zem
a	a	k8xC	a
navrátit	navrátit	k5eAaPmF	navrátit
průmyslovou	průmyslový	k2eAgFnSc4d1	průmyslová
produkci	produkce	k1gFnSc4	produkce
na	na	k7c4	na
předválečnou	předválečný	k2eAgFnSc4d1	předválečná
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
následovala	následovat	k5eAaImAgFnS	následovat
pětiletka	pětiletka	k1gFnSc1	pětiletka
1957-1961	[number]	k4	1957-1961
a	a	k8xC	a
sedmiletka	sedmiletka	k1gFnSc1	sedmiletka
1961	[number]	k4	1961
<g/>
-	-	kIx~	-
<g/>
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
dva	dva	k4xCgInPc1	dva
plány	plán	k1gInPc1	plán
měly	mít	k5eAaImAgInP	mít
dále	daleko	k6eAd2	daleko
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
produkci	produkce	k1gFnSc4	produkce
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
zprůmyslnění	zprůmyslnění	k1gNnSc6	zprůmyslnění
ve	v	k7c6	v
Východní	východní	k2eAgFnSc6d1	východní
Asii	Asie	k1gFnSc6	Asie
za	za	k7c7	za
Japonskem	Japonsko	k1gNnSc7	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
se	se	k3xPyFc4	se
pomalu	pomalu	k6eAd1	pomalu
začaly	začít	k5eAaPmAgFnP	začít
objevovat	objevovat	k5eAaImF	objevovat
nevýhody	nevýhoda	k1gFnPc4	nevýhoda
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
například	například	k6eAd1	například
totální	totální	k2eAgInSc1d1	totální
nedostatek	nedostatek	k1gInSc1	nedostatek
spotřebního	spotřební	k2eAgNnSc2d1	spotřební
zboží	zboží	k1gNnSc2	zboží
<g/>
,	,	kIx,	,
spousta	spousta	k1gFnSc1	spousta
zemí	zem	k1gFnPc2	zem
třetího	třetí	k4xOgInSc2	třetí
světa	svět	k1gInSc2	svět
považovala	považovat	k5eAaImAgFnS	považovat
tento	tento	k3xDgInSc4	tento
systém	systém	k1gInSc4	systém
jako	jako	k8xC	jako
alternativu	alternativa	k1gFnSc4	alternativa
kapitalistického	kapitalistický	k2eAgInSc2d1	kapitalistický
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
všechno	všechen	k3xTgNnSc4	všechen
však	však	k9	však
severokorejská	severokorejský	k2eAgFnSc1d1	severokorejská
ekonomika	ekonomika	k1gFnSc1	ekonomika
zvládala	zvládat	k5eAaImAgFnS	zvládat
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
poskytovat	poskytovat	k5eAaImF	poskytovat
lidem	lid	k1gInSc7	lid
bezplatnou	bezplatný	k2eAgFnSc4d1	bezplatná
lékařskou	lékařský	k2eAgFnSc4d1	lékařská
péči	péče	k1gFnSc4	péče
<g/>
,	,	kIx,	,
vysokoškolské	vysokoškolský	k2eAgNnSc4d1	vysokoškolské
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
,	,	kIx,	,
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
dostatku	dostatek	k1gInSc3	dostatek
jídla	jídlo	k1gNnSc2	jídlo
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
se	s	k7c7	s
sousedními	sousední	k2eAgFnPc7d1	sousední
zeměmi	zem	k1gFnPc7	zem
<g/>
,	,	kIx,	,
jakými	jaký	k3yIgFnPc7	jaký
byla	být	k5eAaImAgFnS	být
ČLR	ČLR	kA	ČLR
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
i	i	k9	i
relativně	relativně	k6eAd1	relativně
slušné	slušný	k2eAgNnSc4d1	slušné
bydlení	bydlení	k1gNnSc4	bydlení
<g/>
.	.	kIx.	.
</s>
<s>
Kim	Kim	k?	Kim
Ir-sen	Iren	k1gInSc1	Ir-sen
se	se	k3xPyFc4	se
prohlásil	prohlásit	k5eAaPmAgInS	prohlásit
za	za	k7c7	za
"	"	kIx"	"
<g/>
Ču-Čcheho	Ču-Čche	k1gMnSc2	Ču-Čche
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
neomezený	omezený	k2eNgMnSc1d1	neomezený
vládce	vládce	k1gMnSc1	vládce
–	–	k?	–
hlavní	hlavní	k2eAgFnSc1d1	hlavní
postava	postava	k1gFnSc1	postava
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
Kimovy	Kimův	k2eAgFnSc2d1	Kimova
doktríny	doktrína	k1gFnSc2	doktrína
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
propagovat	propagovat	k5eAaImF	propagovat
heslo	heslo	k1gNnSc4	heslo
samostatnosti	samostatnost	k1gFnSc2	samostatnost
a	a	k8xC	a
izolovanosti	izolovanost	k1gFnSc2	izolovanost
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
ideologie	ideologie	k1gFnSc1	ideologie
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
oficiální	oficiální	k2eAgFnSc1d1	oficiální
a	a	k8xC	a
nahradila	nahradit	k5eAaPmAgFnS	nahradit
marxismus-leninismus	marxismuseninismus	k1gInSc4	marxismus-leninismus
<g/>
.	.	kIx.	.
</s>
<s>
Úkolem	úkol	k1gInSc7	úkol
Ču-čche	Ču-čche	k1gNnSc2	Ču-čche
bylo	být	k5eAaImAgNnS	být
vytvořit	vytvořit	k5eAaPmF	vytvořit
Severní	severní	k2eAgFnSc4d1	severní
Koreu	Korea	k1gFnSc4	Korea
dostatečně	dostatečně	k6eAd1	dostatečně
silnou	silný	k2eAgFnSc4d1	silná
k	k	k7c3	k
obraně	obrana	k1gFnSc3	obrana
proti	proti	k7c3	proti
cizím	cizí	k2eAgFnPc3d1	cizí
invazím	invaze	k1gFnPc3	invaze
a	a	k8xC	a
zásahům	zásah	k1gInPc3	zásah
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
<g/>
.	.	kIx.	.
</s>
<s>
Nemělo	mít	k5eNaImAgNnS	mít
však	však	k9	však
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
vytvořit	vytvořit	k5eAaPmF	vytvořit
separovanou	separovaný	k2eAgFnSc4d1	separovaná
Koreu	Korea	k1gFnSc4	Korea
od	od	k7c2	od
okolního	okolní	k2eAgInSc2d1	okolní
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Kim	Kim	k?	Kim
Ir-sen	Iren	k1gInSc4	Ir-sen
si	se	k3xPyFc3	se
přál	přát	k5eAaImAgMnS	přát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jeho	jeho	k3xOp3gMnPc1	jeho
lidé	člověk	k1gMnPc1	člověk
tvořili	tvořit	k5eAaImAgMnP	tvořit
všechny	všechen	k3xTgFnPc4	všechen
složky	složka	k1gFnPc4	složka
národního	národní	k2eAgNnSc2d1	národní
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
tak	tak	k9	tak
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
ohledu	ohled	k1gInSc6	ohled
nebylo	být	k5eNaImAgNnS	být
závislé	závislý	k2eAgNnSc1d1	závislé
na	na	k7c6	na
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
však	však	k9	však
začala	začít	k5eAaPmAgFnS	začít
komplikovat	komplikovat	k5eAaBmF	komplikovat
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
zejména	zejména	k9	zejména
díky	díky	k7c3	díky
rusko-čínskému	rusko-čínský	k2eAgInSc3d1	rusko-čínský
konfliktu	konflikt	k1gInSc3	konflikt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
Vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
Severní	severní	k2eAgFnSc7d1	severní
Koreou	Korea	k1gFnSc7	Korea
a	a	k8xC	a
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
se	se	k3xPyFc4	se
zhoršily	zhoršit	k5eAaPmAgFnP	zhoršit
<g/>
,	,	kIx,	,
když	když	k8xS	když
sovětská	sovětský	k2eAgFnSc1d1	sovětská
strana	strana	k1gFnSc1	strana
nařkla	nařknout	k5eAaPmAgFnS	nařknout
Kim	Kim	k1gFnSc1	Kim
Ir-sena	Iren	k2eAgFnSc1d1	Ir-sena
z	z	k7c2	z
podpory	podpora	k1gFnSc2	podpora
čínské	čínský	k2eAgFnSc2d1	čínská
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
bylo	být	k5eAaImAgNnS	být
snížení	snížení	k1gNnSc1	snížení
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
pomoci	pomoc	k1gFnSc2	pomoc
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
právě	právě	k6eAd1	právě
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
sérii	série	k1gFnSc3	série
politických	politický	k2eAgNnPc2d1	politické
rozhodnutí	rozhodnutí	k1gNnPc2	rozhodnutí
<g/>
,	,	kIx,	,
vztahujících	vztahující	k2eAgNnPc2d1	vztahující
se	se	k3xPyFc4	se
k	k	k7c3	k
výdajům	výdaj	k1gInPc3	výdaj
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
těžebního	těžební	k2eAgInSc2d1	těžební
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
vysokým	vysoký	k2eAgFnPc3d1	vysoká
cenám	cena	k1gFnPc3	cena
ropy	ropa	k1gFnSc2	ropa
ze	z	k7c2	z
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
začala	začít	k5eAaPmAgFnS	začít
ekonomika	ekonomika	k1gFnSc1	ekonomika
Severní	severní	k2eAgFnSc2d1	severní
Koreje	Korea	k1gFnSc2	Korea
upadat	upadat	k5eAaPmF	upadat
<g/>
.	.	kIx.	.
</s>
<s>
Začaly	začít	k5eAaPmAgFnP	začít
se	se	k3xPyFc4	se
také	také	k9	také
množit	množit	k5eAaImF	množit
zahraniční	zahraniční	k2eAgInPc4d1	zahraniční
dluhy	dluh	k1gInPc4	dluh
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
všemu	všecek	k3xTgNnSc3	všecek
politika	politika	k1gFnSc1	politika
"	"	kIx"	"
<g/>
Jistota	jistota	k1gFnSc1	jistota
jen	jen	k9	jen
v	v	k7c4	v
nás	my	k3xPp1nPc4	my
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ču-čche	ču-čchat	k5eAaPmIp3nS	ču-čchat
<g/>
)	)	kIx)	)
a	a	k8xC	a
odpor	odpor	k1gInSc4	odpor
k	k	k7c3	k
USA	USA	kA	USA
i	i	k8xC	i
západu	západ	k1gInSc2	západ
jen	jen	k9	jen
utužovala	utužovat	k5eAaImAgFnS	utužovat
uzavřenost	uzavřenost	k1gFnSc1	uzavřenost
severokorejského	severokorejský	k2eAgInSc2d1	severokorejský
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Ropný	ropný	k2eAgInSc1d1	ropný
šok	šok	k1gInSc1	šok
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
těžce	těžce	k6eAd1	těžce
zamával	zamávat	k5eAaPmAgInS	zamávat
všemi	všecek	k3xTgFnPc7	všecek
zeměmi	zem	k1gFnPc7	zem
světa	svět	k1gInSc2	svět
a	a	k8xC	a
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
jakýchkoli	jakýkoli	k3yIgInPc2	jakýkoli
zdrojů	zdroj	k1gInPc2	zdroj
této	tento	k3xDgFnSc2	tento
kapaliny	kapalina	k1gFnSc2	kapalina
jen	jen	k9	jen
těžko	těžko	k6eAd1	těžko
stavěla	stavět	k5eAaImAgNnP	stavět
na	na	k7c4	na
nohy	noha	k1gFnPc4	noha
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
navíc	navíc	k6eAd1	navíc
neměla	mít	k5eNaImAgFnS	mít
žádné	žádný	k3yNgNnSc4	žádný
vývozní	vývozní	k2eAgNnSc4d1	vývozní
zboží	zboží	k1gNnSc4	zboží
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
tak	tak	k6eAd1	tak
odkázána	odkázat	k5eAaPmNgFnS	odkázat
jen	jen	k9	jen
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Kim	Kim	k?	Kim
Ir-sen	Iren	k1gInSc1	Ir-sen
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
ču-čche	ču-čche	k1gFnSc1	ču-čche
<g/>
,	,	kIx,	,
zapříčinilo	zapříčinit	k5eAaPmAgNnS	zapříčinit
obrovské	obrovský	k2eAgNnSc1d1	obrovské
zadlužení	zadlužení	k1gNnSc1	zadlužení
země	zem	k1gFnSc2	zem
<g/>
;	;	kIx,	;
země	země	k1gFnSc1	země
si	se	k3xPyFc3	se
celosvětově	celosvětově	k6eAd1	celosvětově
půjčovala	půjčovat	k5eAaImAgFnS	půjčovat
ohromné	ohromný	k2eAgFnPc4d1	ohromná
částky	částka	k1gFnPc4	částka
<g/>
,	,	kIx,	,
určené	určený	k2eAgInPc4d1	určený
na	na	k7c4	na
investice	investice	k1gFnPc4	investice
do	do	k7c2	do
těžebního	těžební	k2eAgInSc2d1	těžební
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
těžkého	těžký	k2eAgInSc2d1	těžký
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
militarizace	militarizace	k1gFnSc1	militarizace
měla	mít	k5eAaImAgFnS	mít
především	především	k6eAd1	především
vést	vést	k5eAaImF	vést
k	k	k7c3	k
větší	veliký	k2eAgFnSc3d2	veliký
nezávislosti	nezávislost	k1gFnSc3	nezávislost
na	na	k7c6	na
Číně	Čína	k1gFnSc6	Čína
a	a	k8xC	a
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Severokorejští	severokorejský	k2eAgMnPc1d1	severokorejský
představitelé	představitel	k1gMnPc1	představitel
věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
půjčené	půjčený	k2eAgInPc1d1	půjčený
peníze	peníz	k1gInPc1	peníz
pokryjí	pokrýt	k5eAaPmIp3nP	pokrýt
těžební	těžební	k2eAgInSc4d1	těžební
průmysl	průmysl	k1gInSc4	průmysl
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
investovali	investovat	k5eAaBmAgMnP	investovat
polovinu	polovina	k1gFnSc4	polovina
půjčených	půjčený	k2eAgInPc2d1	půjčený
peněz	peníze	k1gInPc2	peníze
<g/>
.	.	kIx.	.
</s>
<s>
Bohužel	bohužel	k9	bohužel
zrovna	zrovna	k6eAd1	zrovna
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vynaložili	vynaložit	k5eAaPmAgMnP	vynaložit
ohromné	ohromný	k2eAgFnPc4d1	ohromná
částky	částka	k1gFnPc4	částka
na	na	k7c4	na
těžící	těžící	k2eAgInPc4d1	těžící
stroje	stroj	k1gInPc4	stroj
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ceny	cena	k1gFnPc4	cena
minerálů	minerál	k1gInPc2	minerál
hluboko	hluboko	k6eAd1	hluboko
propadly	propadnout	k5eAaPmAgFnP	propadnout
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
velkých	velký	k2eAgInPc2d1	velký
dluhů	dluh	k1gInPc2	dluh
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
schopná	schopný	k2eAgFnSc1d1	schopná
je	být	k5eAaImIp3nS	být
splácet	splácet	k5eAaImF	splácet
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
zajišťovat	zajišťovat	k5eAaImF	zajišťovat
poměrně	poměrně	k6eAd1	poměrně
vysoký	vysoký	k2eAgInSc4d1	vysoký
životní	životní	k2eAgInSc4d1	životní
standard	standard	k1gInSc4	standard
svým	svůj	k3xOyFgMnPc3	svůj
obyvatelům	obyvatel	k1gMnPc3	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
všemu	všecek	k3xTgNnSc3	všecek
<g/>
,	,	kIx,	,
centrálně	centrálně	k6eAd1	centrálně
plánovaná	plánovaný	k2eAgFnSc1d1	plánovaná
ekonomika	ekonomika	k1gFnSc1	ekonomika
<g/>
,	,	kIx,	,
zaměřující	zaměřující	k2eAgFnSc1d1	zaměřující
se	se	k3xPyFc4	se
na	na	k7c4	na
těžký	těžký	k2eAgInSc4d1	těžký
průmysl	průmysl	k1gInSc4	průmysl
<g/>
,	,	kIx,	,
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
svého	svůj	k3xOyFgInSc2	svůj
potenciálního	potenciální	k2eAgInSc2d1	potenciální
limitu	limit	k1gInSc2	limit
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
malým	malý	k2eAgFnPc3d1	malá
finanční	finanční	k2eAgFnPc4d1	finanční
příspěvkům	příspěvek	k1gInPc3	příspěvek
technologie	technologie	k1gFnSc1	technologie
zastaraly	zastarat	k5eAaPmAgInP	zastarat
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářsky	hospodářsky	k6eAd1	hospodářsky
se	se	k3xPyFc4	se
severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
poloostrova	poloostrov	k1gInSc2	poloostrov
propadla	propadnout	k5eAaPmAgFnS	propadnout
pod	pod	k7c4	pod
celosvětový	celosvětový	k2eAgInSc4d1	celosvětový
průměr	průměr	k1gInSc4	průměr
<g/>
.	.	kIx.	.
</s>
<s>
Kim	Kim	k?	Kim
Ir-sen	Iren	k1gInSc1	Ir-sen
<g/>
,	,	kIx,	,
uvězněn	uvězněn	k2eAgInSc1d1	uvězněn
v	v	k7c6	v
ideologii	ideologie	k1gFnSc6	ideologie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
kdysi	kdysi	k6eAd1	kdysi
účinná	účinný	k2eAgFnSc1d1	účinná
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgMnS	být
schopen	schopen	k2eAgMnSc1d1	schopen
reagovat	reagovat	k5eAaBmF	reagovat
na	na	k7c4	na
vzestup	vzestup	k1gInSc4	vzestup
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
podkopalo	podkopat	k5eAaPmAgNnS	podkopat
legitimitu	legitimita	k1gFnSc4	legitimita
jeho	jeho	k3xOp3gInSc2	jeho
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Nedokázal	dokázat	k5eNaPmAgMnS	dokázat
utvořit	utvořit	k5eAaPmF	utvořit
tržní	tržní	k2eAgFnSc4d1	tržní
ekonomiku	ekonomika	k1gFnSc4	ekonomika
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
se	se	k3xPyFc4	se
povedlo	povést	k5eAaPmAgNnS	povést
například	například	k6eAd1	například
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
Teng	Teng	k1gMnSc1	Teng
Siao-pchingovi	Siaoching	k1gMnSc3	Siao-pching
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
<g/>
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
KLDR	KLDR	kA	KLDR
postavena	postaven	k2eAgFnSc1d1	postavena
před	před	k7c4	před
volbu	volba	k1gFnSc4	volba
–	–	k?	–
buď	buď	k8xC	buď
splatí	splatit	k5eAaPmIp3nP	splatit
půjčky	půjčka	k1gFnPc4	půjčka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
bude	být	k5eAaImBp3nS	být
nadále	nadále	k6eAd1	nadále
garantovat	garantovat	k5eAaBmF	garantovat
lidem	člověk	k1gMnPc3	člověk
už	už	k6eAd1	už
tak	tak	k6eAd1	tak
slabou	slabý	k2eAgFnSc4d1	slabá
sociální	sociální	k2eAgFnSc4d1	sociální
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Kim	Kim	k?	Kim
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
dostalo	dostat	k5eAaPmAgNnS	dostat
zemi	zem	k1gFnSc4	zem
do	do	k7c2	do
totálního	totální	k2eAgNnSc2d1	totální
embarga	embargo	k1gNnSc2	embargo
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
sociálním	sociální	k2eAgFnPc3d1	sociální
dávkám	dávka	k1gFnPc3	dávka
bylo	být	k5eAaImAgNnS	být
navíc	navíc	k6eAd1	navíc
omezeno	omezit	k5eAaPmNgNnS	omezit
i	i	k9	i
to	ten	k3xDgNnSc1	ten
minimální	minimální	k2eAgNnSc1d1	minimální
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
země	země	k1gFnSc1	země
dokázala	dokázat	k5eAaPmAgFnS	dokázat
vyrobit	vyrobit	k5eAaPmF	vyrobit
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Hladomor	hladomor	k1gInSc1	hladomor
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Koreji	Korea	k1gFnSc6	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Kim	Kim	k?	Kim
Ir-sen	Iren	k1gInSc1	Ir-sen
zemřel	zemřít	k5eAaPmAgInS	zemřít
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1994	[number]	k4	1994
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Kim	Kim	k1gMnSc1	Kim
Čong-il	Čongl	k1gMnSc1	Čong-il
ho	on	k3xPp3gInSc4	on
následoval	následovat	k5eAaImAgMnS	následovat
ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
Korejské	korejský	k2eAgFnSc2d1	Korejská
dělnické	dělnický	k2eAgFnSc2d1	Dělnická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentské	prezidentský	k2eAgNnSc1d1	prezidentské
křeslo	křeslo	k1gNnSc1	křeslo
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
prázdné	prázdný	k2eAgNnSc1d1	prázdné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Čong-il	Čongl	k1gMnSc1	Čong-il
jakožto	jakožto	k8xS	jakožto
velitel	velitel	k1gMnSc1	velitel
národní	národní	k2eAgFnSc2d1	národní
obrany	obrana	k1gFnSc2	obrana
zemi	zem	k1gFnSc3	zem
de	de	k?	de
facto	facto	k1gNnSc1	facto
vládl	vládnout	k5eAaImAgMnS	vládnout
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
následnictví	následnictví	k1gNnSc6	následnictví
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
již	již	k9	již
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
první	první	k4xOgFnSc2	první
dekády	dekáda	k1gFnSc2	dekáda
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
Kim	Kim	k1gFnSc1	Kim
Čong-ila	Čongla	k1gFnSc1	Čong-ila
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
stagnace	stagnace	k1gFnSc1	stagnace
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1996-1999	[number]	k4	1996-1999
zemi	zem	k1gFnSc6	zem
postihl	postihnout	k5eAaPmAgMnS	postihnout
hladomor	hladomor	k1gInSc4	hladomor
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgNnSc6	který
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
600	[number]	k4	600
-	-	kIx~	-
900	[number]	k4	900
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Neefektivita	Neefektivita	k1gFnSc1	Neefektivita
stalinistického	stalinistický	k2eAgInSc2d1	stalinistický
stylu	styl	k1gInSc2	styl
kolektivizace	kolektivizace	k1gFnSc2	kolektivizace
byla	být	k5eAaImAgFnS	být
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
totálním	totální	k2eAgInSc7d1	totální
nedostatkem	nedostatek	k1gInSc7	nedostatek
jakýchkoliv	jakýkoliv	k3yIgInPc2	jakýkoliv
zemědělských	zemědělský	k2eAgInPc2d1	zemědělský
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc7	jeho
hlavní	hlavní	k2eAgFnSc7d1	hlavní
příčinou	příčina	k1gFnSc7	příčina
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
přispívá	přispívat	k5eAaImIp3nS	přispívat
enormním	enormní	k2eAgInSc7d1	enormní
podílem	podíl	k1gInSc7	podíl
svého	svůj	k3xOyFgNnSc2	svůj
již	již	k9	již
tak	tak	k6eAd1	tak
nízkého	nízký	k2eAgMnSc2d1	nízký
HDP	HDP	kA	HDP
na	na	k7c4	na
armádu	armáda	k1gFnSc4	armáda
a	a	k8xC	a
výrobu	výroba	k1gFnSc4	výroba
atomové	atomový	k2eAgFnSc2d1	atomová
zbraně	zbraň	k1gFnSc2	zbraň
<g/>
;	;	kIx,	;
drží	držet	k5eAaImIp3nS	držet
všechny	všechen	k3xTgMnPc4	všechen
schopné	schopný	k2eAgMnPc4d1	schopný
muže	muž	k1gMnPc4	muž
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
18-30	[number]	k4	18-30
let	léto	k1gNnPc2	léto
ve	v	k7c6	v
vojenské	vojenský	k2eAgFnSc6d1	vojenská
službě	služba	k1gFnSc6	služba
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
státu	stát	k1gInSc2	stát
se	se	k3xPyFc4	se
však	však	k9	však
bez	bez	k7c2	bez
potřebných	potřebný	k2eAgFnPc2d1	potřebná
investic	investice	k1gFnPc2	investice
rozpadá	rozpadat	k5eAaImIp3nS	rozpadat
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
dnes	dnes	k6eAd1	dnes
bez	bez	k7c2	bez
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
pomoci	pomoc	k1gFnSc2	pomoc
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
pokrýt	pokrýt	k5eAaPmF	pokrýt
ani	ani	k8xC	ani
polovinu	polovina	k1gFnSc4	polovina
potřeb	potřeba	k1gFnPc2	potřeba
jídla	jídlo	k1gNnSc2	jídlo
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Amnesty	Amnest	k1gInPc4	Amnest
International	International	k1gFnPc7	International
více	hodně	k6eAd2	hodně
než	než	k8xS	než
13	[number]	k4	13
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
KLDR	KLDR	kA	KLDR
trpělo	trpět	k5eAaImAgNnS	trpět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
podvýživou	podvýživa	k1gFnSc7	podvýživa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
dostala	dostat	k5eAaPmAgFnS	dostat
KLDR	KLDR	kA	KLDR
humanitární	humanitární	k2eAgFnSc4d1	humanitární
pomoc	pomoc	k1gFnSc4	pomoc
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
300	[number]	k4	300
milionů	milion	k4xCgInPc2	milion
USD	USD	kA	USD
z	z	k7c2	z
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
a	a	k8xC	a
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Nespecifikované	specifikovaný	k2eNgNnSc1d1	nespecifikované
množství	množství	k1gNnSc1	množství
pomoci	pomoc	k1gFnSc2	pomoc
přichází	přicházet	k5eAaImIp3nS	přicházet
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
jídla	jídlo	k1gNnSc2	jídlo
a	a	k8xC	a
uhlí	uhlí	k1gNnSc2	uhlí
také	také	k9	také
každým	každý	k3xTgInSc7	každý
rokem	rok	k1gInSc7	rok
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
vláda	vláda	k1gFnSc1	vláda
Severní	severní	k2eAgFnSc2d1	severní
Koreje	Korea	k1gFnSc2	Korea
pořád	pořád	k6eAd1	pořád
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
odporem	odpor	k1gInSc7	odpor
vůči	vůči	k7c3	vůči
Spojeným	spojený	k2eAgInPc3d1	spojený
státům	stát	k1gInPc3	stát
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc3d1	jižní
Koreji	Korea	k1gFnSc3	Korea
a	a	k8xC	a
Japonsku	Japonsko	k1gNnSc3	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Špatná	špatný	k2eAgFnSc1d1	špatná
úroda	úroda	k1gFnSc1	úroda
se	se	k3xPyFc4	se
každým	každý	k3xTgInSc7	každý
rokem	rok	k1gInSc7	rok
stává	stávat	k5eAaImIp3nS	stávat
už	už	k6eAd1	už
pravidlem	pravidlem	k6eAd1	pravidlem
<g/>
,	,	kIx,	,
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
trpí	trpět	k5eAaImIp3nS	trpět
pořád	pořád	k6eAd1	pořád
hladem	hlad	k1gInSc7	hlad
<g/>
;	;	kIx,	;
kvete	kvést	k5eAaImIp3nS	kvést
černý	černý	k2eAgInSc4d1	černý
trh	trh	k1gInSc4	trh
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
z	z	k7c2	z
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
důvodů	důvod	k1gInPc2	důvod
zásobují	zásobovat	k5eAaImIp3nP	zásobovat
potravinami	potravina	k1gFnPc7	potravina
dlouho	dlouho	k6eAd1	dlouho
dopředu	dopředu	k6eAd1	dopředu
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
pokouší	pokoušet	k5eAaImIp3nP	pokoušet
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
skupinách	skupina	k1gFnPc6	skupina
ilegálně	ilegálně	k6eAd1	ilegálně
utíkat	utíkat	k5eAaImF	utíkat
do	do	k7c2	do
Číny	Čína	k1gFnSc2	Čína
nebo	nebo	k8xC	nebo
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2002	[number]	k4	2002
byly	být	k5eAaImAgFnP	být
ohlášeny	ohlášen	k2eAgFnPc1d1	ohlášena
další	další	k2eAgFnPc1d1	další
reformy	reforma	k1gFnPc1	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Měna	měna	k1gFnSc1	měna
byla	být	k5eAaImAgFnS	být
devalvována	devalvován	k2eAgFnSc1d1	devalvována
a	a	k8xC	a
některé	některý	k3yIgFnPc4	některý
ceny	cena	k1gFnPc4	cena
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
ponechány	ponechat	k5eAaPmNgFnP	ponechat
prodejcům	prodejce	k1gMnPc3	prodejce
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
povzbuzení	povzbuzení	k1gNnSc3	povzbuzení
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
produkce	produkce	k1gFnSc2	produkce
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
ohlášeno	ohlášen	k2eAgNnSc1d1	ohlášeno
<g/>
,	,	kIx,	,
že	že	k8xS	že
příděly	příděl	k1gInPc1	příděl
jídla	jídlo	k1gNnSc2	jídlo
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
bydlení	bydlení	k1gNnSc4	bydlení
<g/>
,	,	kIx,	,
budou	být	k5eAaImBp3nP	být
postupně	postupně	k6eAd1	postupně
rušeny	rušen	k2eAgFnPc1d1	rušena
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
je	být	k5eAaImIp3nS	být
možnost	možnost	k1gFnSc1	možnost
získat	získat	k5eAaPmF	získat
půdu	půda	k1gFnSc4	půda
pro	pro	k7c4	pro
potřebu	potřeba	k1gFnSc4	potřeba
své	svůj	k3xOyFgFnSc2	svůj
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
také	také	k9	také
zavedla	zavést	k5eAaPmAgFnS	zavést
"	"	kIx"	"
<g/>
speciální	speciální	k2eAgFnSc4d1	speciální
administrativní	administrativní	k2eAgFnSc4d1	administrativní
zónu	zóna	k1gFnSc4	zóna
<g/>
"	"	kIx"	"
v	v	k7c6	v
městě	město	k1gNnSc6	město
Sinuidžu	Sinuidžu	k1gFnSc2	Sinuidžu
<g/>
,	,	kIx,	,
blízko	blízko	k7c2	blízko
hranic	hranice	k1gFnPc2	hranice
s	s	k7c7	s
Čínou	Čína	k1gFnSc7	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgNnSc3	tento
městu	město	k1gNnSc3	město
byla	být	k5eAaImAgFnS	být
dána	dát	k5eAaPmNgFnS	dát
poměrná	poměrný	k2eAgFnSc1d1	poměrná
autonomie	autonomie	k1gFnSc1	autonomie
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ve	v	k7c6	v
finanční	finanční	k2eAgFnSc6d1	finanční
sféře	sféra	k1gFnSc6	sféra
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
pokus	pokus	k1gInSc1	pokus
jak	jak	k6eAd1	jak
napodobit	napodobit	k5eAaPmF	napodobit
úspěšné	úspěšný	k2eAgFnSc2d1	úspěšná
volné	volný	k2eAgFnSc2d1	volná
zóny	zóna	k1gFnSc2	zóna
známé	známý	k2eAgFnSc2d1	známá
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Zájem	zájem	k1gInSc1	zájem
zvenčí	zvenčí	k6eAd1	zvenčí
byl	být	k5eAaImAgInS	být
však	však	k9	však
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
nulové	nulový	k2eAgFnSc3d1	nulová
poptávce	poptávka	k1gFnSc3	poptávka
<g/>
,	,	kIx,	,
minimální	minimální	k2eAgMnSc1d1	minimální
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
Kim	Kim	k1gMnSc1	Kim
Te-džung	Težung	k1gMnSc1	Te-džung
se	se	k3xPyFc4	se
aktivně	aktivně	k6eAd1	aktivně
snažil	snažit	k5eAaImAgMnS	snažit
zmenšit	zmenšit	k5eAaPmF	zmenšit
napětí	napětí	k1gNnSc4	napětí
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
však	však	k9	však
mělo	mít	k5eAaImAgNnS	mít
málo	málo	k1gNnSc1	málo
okamžitých	okamžitý	k2eAgInPc2d1	okamžitý
výsledků	výsledek	k1gInPc2	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dob	doba	k1gFnPc2	doba
zvolení	zvolení	k1gNnSc4	zvolení
George	George	k1gNnSc2	George
W.	W.	kA	W.
Bushe	Bush	k1gMnSc4	Bush
jako	jako	k8xS	jako
prezidenta	prezident	k1gMnSc4	prezident
USA	USA	kA	USA
<g/>
,	,	kIx,	,
čelila	čelit	k5eAaImAgFnS	čelit
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
obnovenému	obnovený	k2eAgInSc3d1	obnovený
tlaku	tlak	k1gInSc3	tlak
ohledně	ohledně	k7c2	ohledně
jejich	jejich	k3xOp3gInSc2	jejich
atomového	atomový	k2eAgInSc2d1	atomový
programu	program	k1gInSc2	program
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
ještě	ještě	k6eAd1	ještě
méně	málo	k6eAd2	málo
pomoci	pomoct	k5eAaPmF	pomoct
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
byl	být	k5eAaImAgInS	být
trošičku	trošičku	k6eAd1	trošičku
uvolněn	uvolněn	k2eAgInSc1d1	uvolněn
trh	trh	k1gInSc1	trh
–	–	k?	–
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
zóna	zóna	k1gFnSc1	zóna
Kesong	Kesonga	k1gFnPc2	Kesonga
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
Jižní	jižní	k2eAgFnSc7d1	jižní
Koreou	Korea	k1gFnSc7	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
byly	být	k5eAaImAgInP	být
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
Čínou	Čína	k1gFnSc7	Čína
vypracovány	vypracován	k2eAgInPc1d1	vypracován
plány	plán	k1gInPc1	plán
pro	pro	k7c4	pro
otevření	otevření	k1gNnSc4	otevření
trhu	trh	k1gInSc2	trh
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
za	za	k7c4	za
udržení	udržení	k1gNnSc4	udržení
zdejšího	zdejší	k2eAgInSc2d1	zdejší
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
10	[number]	k4	10
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlastní	vlastní	k2eAgFnPc4d1	vlastní
atomové	atomový	k2eAgFnPc4d1	atomová
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
dlouho	dlouho	k6eAd1	dlouho
probíráno	probírán	k2eAgNnSc1d1	probíráno
zda	zda	k8xS	zda
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
toto	tento	k3xDgNnSc1	tento
tvrzení	tvrzení	k1gNnSc1	tvrzení
pravdivé	pravdivý	k2eAgNnSc1d1	pravdivé
<g/>
.	.	kIx.	.
</s>
<s>
Rusové	Rus	k1gMnPc1	Rus
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
tvrzení	tvrzení	k1gNnSc4	tvrzení
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
aby	aby	kYmCp3nS	aby
KLDR	KLDR	kA	KLDR
měla	mít	k5eAaImAgFnS	mít
technologii	technologie	k1gFnSc4	technologie
potřebnou	potřebný	k2eAgFnSc4d1	potřebná
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
atomové	atomový	k2eAgFnSc2d1	atomová
zbraně	zbraň	k1gFnSc2	zbraň
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2006	[number]	k4	2006
zástupci	zástupce	k1gMnPc1	zástupce
KLDR	KLDR	kA	KLDR
prohlásili	prohlásit	k5eAaPmAgMnP	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
podařil	podařit	k5eAaPmAgInS	podařit
podzemní	podzemní	k2eAgInSc1d1	podzemní
výbuch	výbuch	k1gInSc1	výbuch
jaderné	jaderný	k2eAgFnSc2d1	jaderná
hlavice	hlavice	k1gFnSc2	hlavice
<g/>
.	.	kIx.	.
</s>
<s>
Doposud	doposud	k6eAd1	doposud
klesající	klesající	k2eAgNnSc1d1	klesající
napětí	napětí	k1gNnSc1	napětí
opět	opět	k6eAd1	opět
narostlo	narůst	k5eAaPmAgNnS	narůst
<g/>
.	.	kIx.	.
</s>
<s>
Čin	čin	k1gInSc1	čin
byl	být	k5eAaImAgInS	být
celosvětově	celosvětově	k6eAd1	celosvětově
odsouzen	odsouzet	k5eAaImNgInS	odsouzet
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
v	v	k7c6	v
OSN	OSN	kA	OSN
hlasování	hlasování	k1gNnSc2	hlasování
o	o	k7c4	o
znovu	znovu	k6eAd1	znovu
uvalení	uvalení	k1gNnSc4	uvalení
sankcí	sankce	k1gFnPc2	sankce
na	na	k7c6	na
KLDR	KLDR	kA	KLDR
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2011	[number]	k4	2011
zemřel	zemřít	k5eAaPmAgMnS	zemřít
Kim	Kim	k1gMnSc1	Kim
Čong-il	Čongl	k1gMnSc1	Čong-il
na	na	k7c4	na
mrtvici	mrtvice	k1gFnSc4	mrtvice
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
severokorejskými	severokorejský	k2eAgMnPc7d1	severokorejský
emigranty	emigrant	k1gMnPc7	emigrant
obviňovaný	obviňovaný	k2eAgInSc1d1	obviňovaný
ze	z	k7c2	z
soustavného	soustavný	k2eAgNnSc2d1	soustavné
porušování	porušování	k1gNnSc2	porušování
lidských	lidský	k2eAgNnPc2d1	lidské
a	a	k8xC	a
občanských	občanský	k2eAgNnPc2d1	občanské
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
totalitní	totalitní	k2eAgFnSc6d1	totalitní
zemi	zem	k1gFnSc6	zem
podle	podle	k7c2	podle
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc2	International
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2011	[number]	k4	2011
existovaly	existovat	k5eAaImAgFnP	existovat
rozsáhlé	rozsáhlý	k2eAgInPc4d1	rozsáhlý
pracovní	pracovní	k2eAgInPc4d1	pracovní
tábory	tábor	k1gInPc4	tábor
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
vězněno	věznit	k5eAaImNgNnS	věznit
200	[number]	k4	200
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
bídě	bída	k1gFnSc3	bída
země	zem	k1gFnSc2	zem
pracovala	pracovat	k5eAaImAgFnS	pracovat
na	na	k7c6	na
velmi	velmi	k6eAd1	velmi
nákladném	nákladný	k2eAgInSc6d1	nákladný
zbrojním	zbrojní	k2eAgInSc6d1	zbrojní
programu	program	k1gInSc6	program
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
zdokonalení	zdokonalení	k1gNnSc1	zdokonalení
zbraní	zbraň	k1gFnPc2	zbraň
hromadného	hromadný	k2eAgNnSc2d1	hromadné
ničení	ničení	k1gNnSc2	ničení
<g/>
.	.	kIx.	.
</s>
<s>
Média	médium	k1gNnPc1	médium
přinesla	přinést	k5eAaPmAgFnS	přinést
zprávy	zpráva	k1gFnPc4	zpráva
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vůdce	vůdce	k1gMnSc1	vůdce
obklopoval	obklopovat	k5eAaImAgMnS	obklopovat
luxusem	luxus	k1gInSc7	luxus
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
měl	mít	k5eAaImAgInS	mít
ročně	ročně	k6eAd1	ročně
utrácet	utrácet	k5eAaImF	utrácet
800	[number]	k4	800
000	[number]	k4	000
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
oblíbený	oblíbený	k2eAgInSc4d1	oblíbený
koňak	koňak	k1gInSc4	koňak
<g/>
,	,	kIx,	,
vlastnit	vlastnit	k5eAaImF	vlastnit
17	[number]	k4	17
honosných	honosný	k2eAgInPc2d1	honosný
domů	dům	k1gInPc2	dům
či	či	k8xC	či
luxusní	luxusní	k2eAgFnSc4d1	luxusní
filmovou	filmový	k2eAgFnSc4d1	filmová
knihovnu	knihovna	k1gFnSc4	knihovna
s	s	k7c7	s
20	[number]	k4	20
000	[number]	k4	000
tituly	titul	k1gInPc7	titul
(	(	kIx(	(
<g/>
běžným	běžný	k2eAgInSc7d1	běžný
občanům	občan	k1gMnPc3	občan
KLDR	KLDR	kA	KLDR
je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
sledování	sledování	k1gNnSc1	sledování
"	"	kIx"	"
<g/>
západních	západní	k2eAgInPc2d1	západní
filmů	film	k1gInPc2	film
<g/>
"	"	kIx"	"
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
až	až	k9	až
pod	pod	k7c7	pod
trestem	trest	k1gInSc7	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
země	zem	k1gFnSc2	zem
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
za	za	k7c2	za
podpory	podpora	k1gFnSc2	podpora
svého	svůj	k3xOyFgMnSc2	svůj
strýce	strýc	k1gMnSc2	strýc
Čang	Čang	k1gMnSc1	Čang
Song-tcheka	Songcheka	k1gMnSc1	Song-tcheka
převzal	převzít	k5eAaPmAgMnS	převzít
Kim	Kim	k1gMnPc3	Kim
Čong-ilův	Čonglův	k2eAgMnSc1d1	Čong-ilův
nejmladší	mladý	k2eAgMnSc1d3	nejmladší
syn	syn	k1gMnSc1	syn
Kim	Kim	k1gMnSc1	Kim
Čong-un	Čongn	k1gMnSc1	Čong-un
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
stalinistickým	stalinistický	k2eAgInSc7d1	stalinistický
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nedostatek	nedostatek	k1gInSc1	nedostatek
přístupu	přístup	k1gInSc2	přístup
pro	pro	k7c4	pro
zahraniční	zahraniční	k2eAgNnPc4d1	zahraniční
média	médium	k1gNnPc4	médium
a	a	k8xC	a
tradiční	tradiční	k2eAgNnPc4d1	tradiční
tajnůstkářství	tajnůstkářství	k1gNnPc4	tajnůstkářství
znamená	znamenat	k5eAaImIp3nS	znamenat
nedostatek	nedostatek	k1gInSc1	nedostatek
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
politickém	politický	k2eAgInSc6d1	politický
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podle	podle	k7c2	podle
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc7	International
byly	být	k5eAaImAgInP	být
zaznamenány	zaznamenat	k5eAaPmNgInP	zaznamenat
případy	případ	k1gInPc1	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
lidé	člověk	k1gMnPc1	člověk
byli	být	k5eAaImAgMnP	být
zavíráni	zavírán	k2eAgMnPc1d1	zavírán
<g/>
,	,	kIx,	,
mučeni	mučen	k2eAgMnPc1d1	mučen
či	či	k8xC	či
popravováni	popravován	k2eAgMnPc1d1	popravován
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
své	svůj	k3xOyFgFnSc2	svůj
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
Mučení	mučení	k1gNnSc1	mučení
a	a	k8xC	a
špatné	špatný	k2eAgNnSc1d1	špatné
zacházení	zacházení	k1gNnSc1	zacházení
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
věznicích	věznice	k1gFnPc6	věznice
či	či	k8xC	či
pracovních	pracovní	k2eAgInPc6d1	pracovní
táborech	tábor	k1gInPc6	tábor
na	na	k7c6	na
denním	denní	k2eAgInSc6d1	denní
pořádku	pořádek	k1gInSc6	pořádek
<g/>
.	.	kIx.	.
</s>
<s>
Vyšetřovací	vyšetřovací	k2eAgFnSc1d1	vyšetřovací
komise	komise	k1gFnSc1	komise
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
Severní	severní	k2eAgFnSc4d1	severní
Koreu	Korea	k1gFnSc4	Korea
porušování	porušování	k1gNnSc2	porušování
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
v	v	k7c6	v
KLDR	KLDR	kA	KLDR
přirovnala	přirovnat	k5eAaPmAgFnS	přirovnat
ke	k	k7c3	k
zvěrstvům	zvěrstvo	k1gNnPc3	zvěrstvo
z	z	k7c2	z
éry	éra	k1gFnSc2	éra
nacismu	nacismus	k1gInSc2	nacismus
a	a	k8xC	a
doporučila	doporučit	k5eAaPmAgFnS	doporučit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
před	před	k7c7	před
soudem	soud	k1gInSc7	soud
stanul	stanout	k5eAaPmAgInS	stanout
i	i	k9	i
Kim	Kim	k1gMnSc1	Kim
Čong-un	Čongn	k1gMnSc1	Čong-un
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2013	[number]	k4	2013
nechal	nechat	k5eAaPmAgMnS	nechat
Kim	Kim	k1gMnSc1	Kim
Čang	Čang	k1gMnSc1	Čang
Song-tcheka	Songcheka	k1gMnSc1	Song-tcheka
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
čistky	čistka	k1gFnSc2	čistka
popravit	popravit	k5eAaPmF	popravit
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
kvůli	kvůli	k7c3	kvůli
upevnění	upevnění	k1gNnSc3	upevnění
vlastní	vlastní	k2eAgFnSc2d1	vlastní
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
Čangově	Čangův	k2eAgFnSc6d1	Čangova
popravě	poprava	k1gFnSc6	poprava
byla	být	k5eAaImAgFnS	být
popravena	popravit	k5eAaPmNgFnS	popravit
podle	podle	k7c2	podle
jihokorejské	jihokorejský	k2eAgFnSc2d1	jihokorejská
agentury	agentura	k1gFnSc2	agentura
Jonhap	Jonhap	k1gInSc4	Jonhap
popravena	popraven	k2eAgFnSc1d1	popravena
také	také	k9	také
celá	celý	k2eAgFnSc1d1	celá
jeho	jeho	k3xOp3gFnSc1	jeho
širší	široký	k2eAgFnSc1d2	širší
rodina	rodina	k1gFnSc1	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Popraveni	popraven	k2eAgMnPc1d1	popraven
byli	být	k5eAaImAgMnP	být
jeho	jeho	k3xOp3gMnPc7	jeho
sestra	sestra	k1gFnSc1	sestra
a	a	k8xC	a
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
zastávající	zastávající	k2eAgInSc1d1	zastávající
post	post	k1gInSc1	post
velvyslance	velvyslanec	k1gMnSc2	velvyslanec
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gMnSc1	jejich
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
velvyslanec	velvyslanec	k1gMnSc1	velvyslanec
v	v	k7c6	v
Malajsii	Malajsie	k1gFnSc6	Malajsie
a	a	k8xC	a
všechny	všechen	k3xTgFnPc1	všechen
děti	dítě	k1gFnPc1	dítě
a	a	k8xC	a
vnoučata	vnouče	k1gNnPc1	vnouče
Čangových	Čangův	k2eAgMnPc2d1	Čangův
bratrů	bratr	k1gMnPc2	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Čistky	čistka	k1gFnPc1	čistka
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
i	i	k9	i
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
např.	např.	kA	např.
jeho	jeho	k3xOp3gMnSc1	jeho
bývalý	bývalý	k2eAgMnSc1d1	bývalý
náměstek	náměstek	k1gMnSc1	náměstek
byl	být	k5eAaImAgMnS	být
podle	podle	k7c2	podle
jihokorejských	jihokorejský	k2eAgInPc2d1	jihokorejský
zdrojů	zdroj	k1gInPc2	zdroj
upálen	upálit	k5eAaPmNgInS	upálit
zaživa	zaživa	k6eAd1	zaživa
plamenometem	plamenomet	k1gInSc7	plamenomet
<g/>
.	.	kIx.	.
</s>
