<s>
Po	po	k7c6	po
konci	konec	k1gInSc6	konec
japonské	japonský	k2eAgFnSc2d1	japonská
okupace	okupace	k1gFnSc2	okupace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
skončila	skončit	k5eAaPmAgFnS	skončit
souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
druhou	druhý	k4xOgFnSc7	druhý
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Korea	Korea	k1gFnSc1	Korea
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
zhruba	zhruba	k6eAd1	zhruba
podle	podle	k7c2	podle
38	[number]	k4	38
<g/>
.	.	kIx.	.
rovnoběžky	rovnoběžka	k1gFnSc2	rovnoběžka
na	na	k7c6	na
Severní	severní	k2eAgFnSc6d1	severní
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
SSSR	SSSR	kA	SSSR
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc2d1	jižní
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
