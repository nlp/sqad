<s>
Narodnaja	Narodnaja	k6eAd1	Narodnaja
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
Г	Г	k?	Г
Н	Н	k?	Н
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
1895	[number]	k4	1895
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
pohoří	pohořet	k5eAaPmIp3nS	pohořet
Ural	Ural	k1gInSc1	Ural
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
vrchol	vrchol	k1gInSc1	vrchol
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Připolárního	Připolární	k2eAgInSc2d1	Připolární
Uralu	Ural	k1gInSc2	Ural
na	na	k7c6	na
území	území	k1gNnSc6	území
Chantymansijského	Chantymansijský	k2eAgInSc2d1	Chantymansijský
autonomního	autonomní	k2eAgInSc2d1	autonomní
okruhu	okruh	k1gInSc2	okruh
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Ťumeňské	Ťumeňský	k2eAgFnSc2d1	Ťumeňská
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
Kavkaz	Kavkaz	k1gInSc1	Kavkaz
nepočítá	počítat	k5eNaImIp3nS	počítat
jako	jako	k9	jako
evropské	evropský	k2eAgNnSc4d1	Evropské
pohoří	pohoří	k1gNnSc4	pohoří
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Hranice	hranice	k1gFnSc2	hranice
mezi	mezi	k7c7	mezi
Evropou	Evropa	k1gFnSc7	Evropa
a	a	k8xC	a
Asií	Asie	k1gFnSc7	Asie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Narodnaja	Narodnajum	k1gNnSc2	Narodnajum
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
celé	celý	k2eAgFnSc2d1	celá
evropské	evropský	k2eAgFnSc2d1	Evropská
části	část	k1gFnSc2	část
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Pohoří	pohoří	k1gNnSc1	pohoří
Ural	Ural	k1gInSc1	Ural
</s>
