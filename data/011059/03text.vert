<p>
<s>
Potměšilý	potměšilý	k2eAgMnSc1d1	potměšilý
host	host	k1gMnSc1	host
je	být	k5eAaImIp3nS	být
osmé	osmý	k4xOgNnSc1	osmý
studiové	studiový	k2eAgNnSc1d1	studiové
album	album	k1gNnSc1	album
Hany	Hana	k1gFnSc2	Hana
Hegerové	Hegerová	k1gFnSc2	Hegerová
nahrané	nahraný	k2eAgFnPc4d1	nahraná
v	v	k7c4	v
Mozarteum	Mozarteum	k1gInSc4	Mozarteum
+	+	kIx~	+
Hrnčíře	Hrnčíř	k1gMnSc2	Hrnčíř
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
složené	složený	k2eAgNnSc1d1	složené
z	z	k7c2	z
12	[number]	k4	12
písní	píseň	k1gFnPc2	píseň
s	s	k7c7	s
hudbou	hudba	k1gFnSc7	hudba
Petra	Petra	k1gFnSc1	Petra
Hapky	hapek	k1gInPc4	hapek
s	s	k7c7	s
texty	text	k1gInPc7	text
Michala	Michal	k1gMnSc2	Michal
Horáčka	Horáček	k1gMnSc2	Horáček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc1	seznam
skladeb	skladba	k1gFnPc2	skladba
==	==	k?	==
</s>
</p>
<p>
<s>
Stín	stín	k1gInSc1	stín
stíhá	stíhat	k5eAaImIp3nS	stíhat
stín	stín	k1gInSc4	stín
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
10	[number]	k4	10
</s>
</p>
<p>
<s>
Jinde	jinde	k6eAd1	jinde
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
</s>
</p>
<p>
<s>
Levandulová	levandulový	k2eAgFnSc1d1	levandulová
(	(	kIx(	(
<g/>
duet	duet	k1gInSc1	duet
s	s	k7c7	s
Petrem	Petr	k1gMnSc7	Petr
Hapkou	Hapka	k1gMnSc7	Hapka
<g/>
)	)	kIx)	)
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
51	[number]	k4	51
</s>
</p>
<p>
<s>
Gabriel	Gabriel	k1gMnSc1	Gabriel
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
</s>
</p>
<p>
<s>
Všechno	všechen	k3xTgNnSc4	všechen
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
19	[number]	k4	19
</s>
</p>
<p>
<s>
Vůně	vůně	k1gFnSc1	vůně
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
42	[number]	k4	42
</s>
</p>
<p>
<s>
Denim	Denim	k?	Denim
blue	bluat	k5eAaPmIp3nS	bluat
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
</s>
</p>
<p>
<s>
Žít	žít	k5eAaImF	žít
a	a	k8xC	a
nechat	nechat	k5eAaPmF	nechat
žít	žít	k5eAaImF	žít
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
26	[number]	k4	26
</s>
</p>
<p>
<s>
Vana	vana	k1gFnSc1	vana
plná	plný	k2eAgFnSc1d1	plná
fialek	fialka	k1gFnPc2	fialka
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
</s>
</p>
<p>
<s>
Kolotoč	kolotoč	k1gInSc1	kolotoč
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
24	[number]	k4	24
</s>
</p>
<p>
<s>
Táta	táta	k1gMnSc1	táta
měl	mít	k5eAaImAgMnS	mít
rád	rád	k6eAd1	rád
Máju	Mája	k1gFnSc4	Mája
Westovou	Westový	k2eAgFnSc4d1	Westová
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
25	[number]	k4	25
</s>
</p>
<p>
<s>
Potměšilý	potměšilý	k2eAgMnSc1d1	potměšilý
host	host	k1gMnSc1	host
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
59	[number]	k4	59
</s>
</p>
<p>
<s>
==	==	k?	==
Reedice	reedice	k1gFnSc2	reedice
==	==	k?	==
</s>
</p>
<p>
<s>
Album	album	k1gNnSc1	album
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
reedici	reedice	k1gFnSc6	reedice
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1995	[number]	k4	1995
a	a	k8xC	a
2006	[number]	k4	2006
u	u	k7c2	u
Supraphonu	supraphon	k1gInSc2	supraphon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Supraphon	supraphon	k1gInSc1	supraphon
</s>
</p>
