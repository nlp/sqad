<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
druhý	druhý	k4xOgInSc4	druhý
nejmenší	malý	k2eAgInSc4d3	nejmenší
světadíl	světadíl	k1gInSc4	světadíl
mající	mající	k2eAgFnSc4d1	mající
rozlohu	rozloha	k1gFnSc4	rozloha
asi	asi	k9	asi
10	[number]	k4	10
058	[number]	k4	058
912	[number]	k4	912
km2	km2	k4	km2
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
asi	asi	k9	asi
7	[number]	k4	7
%	%	kIx~	%
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
zároveň	zároveň	k6eAd1	zároveň
druhý	druhý	k4xOgMnSc1	druhý
nejhustěji	husto	k6eAd3	husto
zalidněný	zalidněný	k2eAgMnSc1d1	zalidněný
(	(	kIx(	(
<g/>
asi	asi	k9	asi
72	[number]	k4	72
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
asi	asi	k9	asi
742	[number]	k4	742
500	[number]	k4	500
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
představuje	představovat	k5eAaImIp3nS	představovat
přibližně	přibližně	k6eAd1	přibližně
9,6	[number]	k4	9,6
<g/>
%	%	kIx~	%
podíl	podíl	k1gInSc1	podíl
na	na	k7c6	na
světové	světový	k2eAgFnSc6d1	světová
populaci	populace	k1gFnSc6	populace
(	(	kIx(	(
<g/>
údaje	údaj	k1gInPc1	údaj
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
