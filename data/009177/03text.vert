<p>
<s>
Kantáta	kantáta	k1gFnSc1	kantáta
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
<g/>
,	,	kIx,	,
it.	it.	k?	it.
cantare	cantar	k1gMnSc5	cantar
=	=	kIx~	=
zpívat	zpívat	k5eAaImF	zpívat
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rozsáhlejší	rozsáhlý	k2eAgNnSc1d2	rozsáhlejší
vokálně	vokálně	k6eAd1	vokálně
instrumentální	instrumentální	k2eAgNnSc1d1	instrumentální
dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
pro	pro	k7c4	pro
sóla	sólo	k1gNnPc4	sólo
<g/>
,	,	kIx,	,
sbor	sbor	k1gInSc1	sbor
a	a	k8xC	a
orchestr	orchestr	k1gInSc1	orchestr
<g/>
.	.	kIx.	.
</s>
<s>
Obsazením	obsazení	k1gNnSc7	obsazení
<g/>
,	,	kIx,	,
strukturou	struktura	k1gFnSc7	struktura
a	a	k8xC	a
absencí	absence	k1gFnSc7	absence
scénické	scénický	k2eAgFnSc2d1	scénická
akce	akce	k1gFnSc2	akce
se	se	k3xPyFc4	se
kantáta	kantáta	k1gFnSc1	kantáta
blíží	blížit	k5eAaImIp3nS	blížit
oratoriu	oratorium	k1gNnSc3	oratorium
<g/>
,	,	kIx,	,
od	od	k7c2	od
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
menším	malý	k2eAgInSc7d2	menší
rozměrem	rozměr	k1gInSc7	rozměr
a	a	k8xC	a
menší	malý	k2eAgFnSc7d2	menší
dramatičností	dramatičnost	k1gFnSc7	dramatičnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
kantátě	kantáta	k1gFnSc6	kantáta
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
střídat	střídat	k5eAaImF	střídat
recitativy	recitativ	k1gInPc1	recitativ
<g/>
,	,	kIx,	,
árie	árie	k1gFnPc1	árie
<g/>
,	,	kIx,	,
ariosa	arioso	k1gNnPc1	arioso
<g/>
,	,	kIx,	,
sborové	sborový	k2eAgFnPc1d1	sborová
věty	věta	k1gFnPc1	věta
<g/>
,	,	kIx,	,
chorály	chorál	k1gInPc1	chorál
a	a	k8xC	a
hudební	hudební	k2eAgFnPc1d1	hudební
předehry	předehra	k1gFnPc1	předehra
a	a	k8xC	a
mezihry	mezihra	k1gFnPc1	mezihra
v	v	k7c6	v
libovolném	libovolný	k2eAgInSc6d1	libovolný
počtu	počet	k1gInSc6	počet
i	i	k8xC	i
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
.	.	kIx.	.
</s>
<s>
Svého	svůj	k3xOyFgMnSc4	svůj
největšího	veliký	k2eAgMnSc4d3	veliký
rozšíření	rozšíření	k1gNnSc1	rozšíření
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
tato	tento	k3xDgFnSc1	tento
hudební	hudební	k2eAgFnSc1d1	hudební
forma	forma	k1gFnSc1	forma
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
jak	jak	k6eAd1	jak
světské	světský	k2eAgInPc1d1	světský
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
duchovní	duchovní	k2eAgFnPc1d1	duchovní
kantáty	kantáta	k1gFnPc1	kantáta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
znamenala	znamenat	k5eAaImAgFnS	znamenat
kantáta	kantáta	k1gFnSc1	kantáta
každou	každý	k3xTgFnSc4	každý
zpívanou	zpívaný	k2eAgFnSc4d1	zpívaná
skladbu	skladba	k1gFnSc4	skladba
(	(	kIx(	(
<g/>
cantare	cantar	k1gMnSc5	cantar
=	=	kIx~	=
latinsky	latinsky	k6eAd1	latinsky
zpívat	zpívat	k5eAaImF	zpívat
<g/>
)	)	kIx)	)
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
instrumentální	instrumentální	k2eAgFnSc2d1	instrumentální
sonáty	sonáta	k1gFnSc2	sonáta
(	(	kIx(	(
<g/>
sonare	sonar	k1gInSc5	sonar
=	=	kIx~	=
latinsky	latinsky	k6eAd1	latinsky
hrát	hrát	k5eAaImF	hrát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgFnSc1d1	vlastní
kantáta	kantáta	k1gFnSc1	kantáta
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
zprvu	zprvu	k6eAd1	zprvu
v	v	k7c6	v
sólové	sólový	k2eAgFnSc6d1	sólová
vokální	vokální	k2eAgFnSc6d1	vokální
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
kantáty	kantáta	k1gFnSc2	kantáta
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
skládaly	skládat	k5eAaImAgInP	skládat
ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
nebo	nebo	k8xC	nebo
několika	několik	k4yIc2	několik
árií	árie	k1gFnPc2	árie
da	da	k?	da
capo	capa	k1gFnSc5	capa
se	s	k7c7	s
vstupními	vstupní	k2eAgInPc7d1	vstupní
recitativy	recitativ	k1gInPc7	recitativ
<g/>
,	,	kIx,	,
doprovod	doprovod	k1gInSc1	doprovod
byl	být	k5eAaImAgInS	být
psán	psán	k2eAgInSc1d1	psán
formou	forma	k1gFnSc7	forma
bassa	bassa	k1gFnSc1	bassa
continua	continua	k1gFnSc1	continua
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
kantáty	kantáta	k1gFnPc1	kantáta
se	se	k3xPyFc4	se
přiblížily	přiblížit	k5eAaPmAgFnP	přiblížit
operní	operní	k2eAgFnPc1d1	operní
scéně	scéna	k1gFnSc6	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Italské	italský	k2eAgFnPc1d1	italská
kantáty	kantáta	k1gFnPc1	kantáta
byly	být	k5eAaImAgFnP	být
převážně	převážně	k6eAd1	převážně
světské	světský	k2eAgInPc1d1	světský
(	(	kIx(	(
<g/>
cantata	cantata	k1gFnSc1	cantata
da	da	k?	da
camera	camera	k1gFnSc1	camera
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Duchovní	duchovní	k2eAgFnSc1d1	duchovní
kantáta	kantáta	k1gFnSc1	kantáta
(	(	kIx(	(
<g/>
cantata	cantat	k2eAgFnSc1d1	cantat
da	da	k?	da
chiesa	chiesa	k1gFnSc1	chiesa
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zformovala	zformovat	k5eAaPmAgFnS	zformovat
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
zprvu	zprvu	k6eAd1	zprvu
byla	být	k5eAaImAgFnS	být
identická	identický	k2eAgFnSc1d1	identická
s	s	k7c7	s
duchovním	duchovní	k2eAgInSc7d1	duchovní
koncertem	koncert	k1gInSc7	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc4d1	důležitá
úlohu	úloha	k1gFnSc4	úloha
při	při	k7c6	při
utváření	utváření	k1gNnSc6	utváření
duchovní	duchovní	k2eAgFnSc2d1	duchovní
kantáty	kantáta	k1gFnSc2	kantáta
měly	mít	k5eAaImAgInP	mít
sborníky	sborník	k1gInPc1	sborník
kantátových	kantátový	k2eAgMnPc2d1	kantátový
textů	text	k1gInPc2	text
<g/>
,	,	kIx,	,
sestavené	sestavený	k2eAgFnSc6d1	sestavená
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
německým	německý	k2eAgMnSc7d1	německý
básníkem	básník	k1gMnSc7	básník
a	a	k8xC	a
teologem	teolog	k1gMnSc7	teolog
E.	E.	kA	E.
Neumeisterem	Neumeister	k1gMnSc7	Neumeister
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
typu	typ	k1gInSc3	typ
kantáty	kantáta	k1gFnSc2	kantáta
se	se	k3xPyFc4	se
přiklonil	přiklonit	k5eAaPmAgInS	přiklonit
zvláště	zvláště	k9	zvláště
J.	J.	kA	J.
S.	S.	kA	S.
Bach	Bach	k1gMnSc1	Bach
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
zásluhou	zásluha	k1gFnSc7	zásluha
zaujala	zaujmout	k5eAaPmAgFnS	zaujmout
duchovní	duchovní	k2eAgFnSc1d1	duchovní
kantáta	kantáta	k1gFnSc1	kantáta
významné	významný	k2eAgNnSc4d1	významné
místo	místo	k1gNnSc4	místo
mezi	mezi	k7c7	mezi
tehdejšími	tehdejší	k2eAgInPc7d1	tehdejší
hudebními	hudební	k2eAgInPc7d1	hudební
žánry	žánr	k1gInPc7	žánr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
německá	německý	k2eAgFnSc1d1	německá
světské	světský	k2eAgFnPc1d1	světská
kantáta	kantáta	k1gFnSc1	kantáta
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
sólová	sólový	k2eAgNnPc1d1	sólové
<g/>
.	.	kIx.	.
</s>
<s>
Kantátové	kantátový	k2eAgInPc1d1	kantátový
sborové	sborový	k2eAgInPc1d1	sborový
žánry	žánr	k1gInPc1	žánr
se	se	k3xPyFc4	se
významně	významně	k6eAd1	významně
uplatnily	uplatnit	k5eAaPmAgFnP	uplatnit
za	za	k7c2	za
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
konce	konec	k1gInSc2	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
období	období	k1gNnSc2	období
hudebního	hudební	k2eAgInSc2d1	hudební
romantismu	romantismus	k1gInSc2	romantismus
a	a	k8xC	a
zejména	zejména	k9	zejména
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnPc1	století
vznikají	vznikat	k5eAaImIp3nP	vznikat
jednotlivá	jednotlivý	k2eAgNnPc1d1	jednotlivé
individualizovaná	individualizovaný	k2eAgNnPc1d1	individualizované
kantátová	kantátový	k2eAgNnPc1d1	kantátové
díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
má	mít	k5eAaImIp3nS	mít
zpravidla	zpravidla	k6eAd1	zpravidla
závažnou	závažný	k2eAgFnSc4d1	závažná
úlohu	úloha	k1gFnSc4	úloha
sbor	sbor	k1gInSc1	sbor
(	(	kIx(	(
<g/>
díla	dílo	k1gNnSc2	dílo
S.	S.	kA	S.
S.	S.	kA	S.
Prokofjeva	Prokofjev	k1gMnSc2	Prokofjev
<g/>
,	,	kIx,	,
I.	I.	kA	I.
<g/>
F.	F.	kA	F.
<g/>
Stravinského	Stravinský	k2eAgInSc2d1	Stravinský
<g/>
,	,	kIx,	,
C.	C.	kA	C.
Orffa	Orff	k1gMnSc2	Orff
<g/>
,	,	kIx,	,
Z.	Z.	kA	Z.
Kodálye	Kodálye	k1gNnSc1	Kodálye
<g/>
,	,	kIx,	,
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
hudbě	hudba	k1gFnSc6	hudba
B.	B.	kA	B.
Smetany	Smetana	k1gMnSc2	Smetana
<g/>
,	,	kIx,	,
A.	A.	kA	A.
Dvořáka	Dvořák	k1gMnSc2	Dvořák
<g/>
,	,	kIx,	,
Z.	Z.	kA	Z.
Fibicha	Fibich	k1gMnSc2	Fibich
<g/>
,	,	kIx,	,
V.	V.	kA	V.
Nováka	Novák	k1gMnSc2	Novák
<g/>
,	,	kIx,	,
L.	L.	kA	L.
Vycpálka	Vycpálka	k1gFnSc1	Vycpálka
<g/>
,	,	kIx,	,
V.	V.	kA	V.
Dobiáše	Dobiáš	k1gMnSc2	Dobiáš
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Seidla	Seidel	k1gMnSc2	Seidel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
jako	jako	k9	jako
kantáta	kantáta	k1gFnSc1	kantáta
označovala	označovat	k5eAaImAgFnS	označovat
také	také	k9	také
literární	literární	k2eAgFnSc1d1	literární
forma	forma	k1gFnSc1	forma
<g/>
,	,	kIx,	,
báseň	báseň	k1gFnSc1	báseň
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
určena	určit	k5eAaPmNgFnS	určit
ke	k	k7c3	k
zhudebnění	zhudebnění	k1gNnSc3	zhudebnění
touto	tento	k3xDgFnSc7	tento
formou	forma	k1gFnSc7	forma
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
patetickou	patetický	k2eAgFnSc4d1	patetická
<g/>
,	,	kIx,	,
oslavnou	oslavný	k2eAgFnSc4d1	oslavná
báseň	báseň	k1gFnSc4	báseň
<g/>
,	,	kIx,	,
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
o	o	k7c4	o
variantu	varianta	k1gFnSc4	varianta
ódy	óda	k1gFnSc2	óda
<g/>
,	,	kIx,	,
zpěvného	zpěvný	k2eAgMnSc2d1	zpěvný
a	a	k8xC	a
snadno	snadno	k6eAd1	snadno
zhudebnitelného	zhudebnitelný	k2eAgInSc2d1	zhudebnitelný
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Mohlo	moct	k5eAaImAgNnS	moct
jít	jít	k5eAaImF	jít
o	o	k7c4	o
poměrně	poměrně	k6eAd1	poměrně
konvenční	konvenční	k2eAgInSc4d1	konvenční
příležitostný	příležitostný	k2eAgInSc4d1	příležitostný
text	text	k1gInSc4	text
oslavující	oslavující	k2eAgFnSc4d1	oslavující
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
událost	událost	k1gFnSc4	událost
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Vrchlického	Vrchlického	k2eAgFnPc4d1	Vrchlického
kantáty	kantáta	k1gFnPc4	kantáta
k	k	k7c3	k
výročím	výročí	k1gNnSc7	výročí
narození	narození	k1gNnSc2	narození
různých	různý	k2eAgFnPc2d1	různá
význačných	význačný	k2eAgFnPc2d1	význačná
osobností	osobnost	k1gFnPc2	osobnost
<g/>
,	,	kIx,	,
např.	např.	kA	např.
P.	P.	kA	P.
J.	J.	kA	J.
Šafaříka	Šafařík	k1gMnSc2	Šafařík
<g/>
,	,	kIx,	,
ze	z	k7c2	z
sbírky	sbírka	k1gFnSc2	sbírka
Hlasy	hlas	k1gInPc1	hlas
v	v	k7c6	v
poušti	poušť	k1gFnSc6	poušť
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
o	o	k7c4	o
složitější	složitý	k2eAgFnSc4d2	složitější
<g/>
,	,	kIx,	,
filosoficky	filosoficky	k6eAd1	filosoficky
hlubší	hluboký	k2eAgInSc4d2	hlubší
text	text	k1gInSc4	text
<g/>
,	,	kIx,	,
určený	určený	k2eAgInSc4d1	určený
oslavě	oslava	k1gFnSc3	oslava
nějaké	nějaký	k3yIgFnSc2	nějaký
abstraktní	abstraktní	k2eAgFnSc2d1	abstraktní
ideje	idea	k1gFnSc2	idea
(	(	kIx(	(
<g/>
básně	báseň	k1gFnPc1	báseň
Miloty	milota	k1gFnSc2	milota
Zdirada	Zdirada	k1gFnSc1	Zdirada
Poláka	Polák	k1gMnSc2	Polák
Vznešenost	vznešenost	k1gFnSc4	vznešenost
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
Vznešenost	vznešenost	k1gFnSc1	vznešenost
hudby	hudba	k1gFnSc2	hudba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
rysem	rys	k1gInSc7	rys
takovéto	takovýto	k3xDgFnSc2	takovýto
kantáty	kantáta	k1gFnSc2	kantáta
je	být	k5eAaImIp3nS	být
dialogická	dialogický	k2eAgFnSc1d1	dialogická
forma	forma	k1gFnSc1	forma
(	(	kIx(	(
<g/>
střídání	střídání	k1gNnSc1	střídání
promluv	promluva	k1gFnPc2	promluva
sboru	sbor	k1gInSc2	sbor
a	a	k8xC	a
sólistů	sólista	k1gMnPc2	sólista
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
ne	ne	k9	ne
nezbytně	zbytně	k6eNd1	zbytně
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
formu	forma	k1gFnSc4	forma
apostrofy	apostrofa	k1gFnSc2	apostrofa
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
oslovení	oslovení	k1gNnSc4	oslovení
oslavovaného	oslavovaný	k2eAgNnSc2d1	oslavované
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdější	pozdní	k2eAgFnSc6d2	pozdější
době	doba	k1gFnSc6	doba
tento	tento	k3xDgInSc4	tento
žánr	žánr	k1gInSc4	žánr
vymizel	vymizet	k5eAaPmAgMnS	vymizet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Moteto	moteto	k1gNnSc1	moteto
</s>
</p>
<p>
<s>
Oratorium	oratorium	k1gNnSc1	oratorium
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kantáta	kantáta	k1gFnSc1	kantáta
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
kantáta	kantáta	k1gFnSc1	kantáta
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
