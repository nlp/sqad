<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
spánku	spánek	k1gInSc2	spánek
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
časové	časový	k2eAgInPc1d1	časový
úseky	úsek	k1gInPc1	úsek
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mozek	mozek	k1gInSc4	mozek
<g/>
,	,	kIx,	,
navzdory	navzdory	k7c3	navzdory
převážně	převážně	k6eAd1	převážně
útlumovému	útlumový	k2eAgInSc3d1	útlumový
charakteru	charakter	k1gInSc3	charakter
spánku	spánek	k1gInSc2	spánek
<g/>
,	,	kIx,	,
intenzivně	intenzivně	k6eAd1	intenzivně
pracuje	pracovat	k5eAaImIp3nS	pracovat
<g/>
.	.	kIx.	.
</s>
