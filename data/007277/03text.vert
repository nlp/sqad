<s>
Spánek	spánek	k1gInSc1	spánek
je	být	k5eAaImIp3nS	být
útlumově-relaxační	útlumověelaxační	k2eAgFnPc4d1	útlumově-relaxační
fáze	fáze	k1gFnPc4	fáze
organismu	organismus	k1gInSc2	organismus
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
činnosti	činnost	k1gFnSc3	činnost
mozku	mozek	k1gInSc2	mozek
doprovázené	doprovázený	k2eAgFnSc2d1	doprovázená
ztrátou	ztráta	k1gFnSc7	ztráta
vědomí	vědomí	k1gNnSc2	vědomí
a	a	k8xC	a
podstatně	podstatně	k6eAd1	podstatně
sníženou	snížený	k2eAgFnSc7d1	snížená
citlivostí	citlivost	k1gFnSc7	citlivost
na	na	k7c4	na
vnější	vnější	k2eAgInPc4d1	vnější
podněty	podnět	k1gInPc4	podnět
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
uvolnění	uvolnění	k1gNnSc3	uvolnění
svalstva	svalstvo	k1gNnSc2	svalstvo
<g/>
,	,	kIx,	,
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
doby	doba	k1gFnSc2	doba
jeho	jeho	k3xOp3gNnPc2	jeho
trvání	trvání	k1gNnPc2	trvání
je	být	k5eAaImIp3nS	být
snížena	snížit	k5eAaPmNgFnS	snížit
tělesná	tělesný	k2eAgFnSc1d1	tělesná
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
dýchání	dýchání	k1gNnSc1	dýchání
se	se	k3xPyFc4	se
zpomaluje	zpomalovat	k5eAaImIp3nS	zpomalovat
a	a	k8xC	a
krevní	krevní	k2eAgInSc1d1	krevní
tlak	tlak	k1gInSc1	tlak
se	se	k3xPyFc4	se
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
spánku	spánek	k1gInSc2	spánek
se	se	k3xPyFc4	se
lidem	člověk	k1gMnPc3	člověk
zdají	zdát	k5eAaImIp3nP	zdát
sny	sen	k1gInPc7	sen
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
cíleným	cílený	k2eAgNnPc3d1	cílené
buzením	buzení	k1gNnPc3	buzení
při	při	k7c6	při
výzkumu	výzkum	k1gInSc6	výzkum
<g/>
,	,	kIx,	,
i	i	k9	i
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
po	po	k7c6	po
probuzení	probuzení	k1gNnSc6	probuzení
žádný	žádný	k3yNgInSc1	žádný
sen	sen	k1gInSc1	sen
nevybavují	vybavovat	k5eNaImIp3nP	vybavovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
spánku	spánek	k1gInSc2	spánek
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
časové	časový	k2eAgInPc1d1	časový
úseky	úsek	k1gInPc1	úsek
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mozek	mozek	k1gInSc4	mozek
<g/>
,	,	kIx,	,
navzdory	navzdory	k7c3	navzdory
převážně	převážně	k6eAd1	převážně
útlumovému	útlumový	k2eAgInSc3d1	útlumový
charakteru	charakter	k1gInSc3	charakter
spánku	spánek	k1gInSc2	spánek
<g/>
,	,	kIx,	,
intenzivně	intenzivně	k6eAd1	intenzivně
pracuje	pracovat	k5eAaImIp3nS	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Spánek	spánek	k1gInSc1	spánek
nám	my	k3xPp1nPc3	my
zabere	zabrat	k5eAaPmIp3nS	zabrat
přibližně	přibližně	k6eAd1	přibližně
třetinu	třetina	k1gFnSc4	třetina
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Rušení	rušení	k1gNnSc1	rušení
nebo	nebo	k8xC	nebo
neumožnění	neumožnění	k1gNnSc1	neumožnění
spánku	spánek	k1gInSc2	spánek
vede	vést	k5eAaImIp3nS	vést
potřebě	potřeba	k1gFnSc3	potřeba
spánku	spánek	k1gInSc2	spánek
mimo	mimo	k7c4	mimo
obvyklou	obvyklý	k2eAgFnSc4d1	obvyklá
dobu	doba	k1gFnSc4	doba
a	a	k8xC	a
k	k	k7c3	k
psychickým	psychický	k2eAgFnPc3d1	psychická
potížím	potíž	k1gFnPc3	potíž
<g/>
,	,	kIx,	,
od	od	k7c2	od
mírných	mírný	k2eAgNnPc2d1	mírné
až	až	k9	až
po	po	k7c4	po
velmi	velmi	k6eAd1	velmi
závažné	závažný	k2eAgInPc4d1	závažný
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
pokusných	pokusný	k2eAgNnPc2d1	pokusné
zvířat	zvíře	k1gNnPc2	zvíře
se	se	k3xPyFc4	se
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
dlouhodobější	dlouhodobý	k2eAgMnSc1d2	dlouhodobější
(	(	kIx(	(
<g/>
dny	den	k1gInPc1	den
až	až	k8xS	až
týdny	týden	k1gInPc1	týden
<g/>
)	)	kIx)	)
zamezení	zamezení	k1gNnSc1	zamezení
spánku	spánek	k1gInSc2	spánek
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
spánku	spánek	k1gInSc2	spánek
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
je	být	k5eAaImIp3nS	být
individuální	individuální	k2eAgNnSc1d1	individuální
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
dospělé	dospělý	k2eAgFnPc4d1	dospělá
osoby	osoba	k1gFnPc4	osoba
uvádí	uvádět	k5eAaImIp3nS	uvádět
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
8	[number]	k4	8
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
někteří	některý	k3yIgMnPc1	některý
jedinci	jedinec	k1gMnPc1	jedinec
mají	mít	k5eAaImIp3nP	mít
potřebu	potřeba	k1gFnSc4	potřeba
spánku	spánek	k1gInSc2	spánek
odlišnou	odlišný	k2eAgFnSc7d1	odlišná
(	(	kIx(	(
<g/>
kolísá	kolísat	k5eAaImIp3nS	kolísat
od	od	k7c2	od
4	[number]	k4	4
až	až	k9	až
po	po	k7c4	po
12	[number]	k4	12
hodin	hodina	k1gFnPc2	hodina
spánku	spánek	k1gInSc2	spánek
za	za	k7c4	za
den	den	k1gInSc4	den
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
žijící	žijící	k2eAgMnPc1d1	žijící
tradičně	tradičně	k6eAd1	tradičně
a	a	k8xC	a
s	s	k7c7	s
minimálním	minimální	k2eAgInSc7d1	minimální
vlivem	vliv	k1gInSc7	vliv
civilizace	civilizace	k1gFnSc2	civilizace
spí	spát	k5eAaImIp3nS	spát
průměrně	průměrně	k6eAd1	průměrně
6,5	[number]	k4	6,5
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
spí	spát	k5eAaImIp3nS	spát
efektivněji	efektivně	k6eAd2	efektivně
a	a	k8xC	a
tak	tak	k6eAd1	tak
méně	málo	k6eAd2	málo
než	než	k8xS	než
příbuzní	příbuzný	k2eAgMnPc1d1	příbuzný
živočichové	živočich	k1gMnPc1	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vnějšího	vnější	k2eAgInSc2d1	vnější
pohledu	pohled	k1gInSc2	pohled
je	být	k5eAaImIp3nS	být
spánek	spánek	k1gInSc1	spánek
stav	stav	k1gInSc1	stav
charakterizovaný	charakterizovaný	k2eAgInSc1d1	charakterizovaný
<g/>
:	:	kIx,	:
stereotypní	stereotypní	k2eAgFnSc7d1	stereotypní
polohou	poloha	k1gFnSc7	poloha
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
minimálním	minimální	k2eAgInSc7d1	minimální
pohybem	pohyb	k1gInSc7	pohyb
<g/>
,	,	kIx,	,
zvýšením	zvýšení	k1gNnSc7	zvýšení
prahu	práh	k1gInSc2	práh
pro	pro	k7c4	pro
reakci	reakce	k1gFnSc4	reakce
na	na	k7c4	na
smyslové	smyslový	k2eAgInPc4d1	smyslový
podněty	podnět	k1gInPc4	podnět
sníženou	snížený	k2eAgFnSc7d1	snížená
tělesnou	tělesný	k2eAgFnSc7d1	tělesná
teplotou	teplota	k1gFnSc7	teplota
a	a	k8xC	a
produkcí	produkce	k1gFnSc7	produkce
tepla	teplo	k1gNnSc2	teplo
(	(	kIx(	(
<g/>
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
před	před	k7c7	před
spaním	spaní	k1gNnSc7	spaní
přikrývají	přikrývat	k5eAaImIp3nP	přikrývat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
relativně	relativně	k6eAd1	relativně
snadnou	snadný	k2eAgFnSc7d1	snadná
reverzibilitou	reverzibilita	k1gFnSc7	reverzibilita
–	–	k?	–
možností	možnost	k1gFnPc2	možnost
probuzení	probuzení	k1gNnSc2	probuzení
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
kómatu	kóma	k1gNnSc2	kóma
nebo	nebo	k8xC	nebo
hibernace	hibernace	k1gFnSc2	hibernace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Typická	typický	k2eAgFnSc1d1	typická
celková	celkový	k2eAgFnSc1d1	celková
doba	doba	k1gFnSc1	doba
spánku	spánek	k1gInSc2	spánek
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
živočichů	živočich	k1gMnPc2	živočich
velmi	velmi	k6eAd1	velmi
odlišná	odlišný	k2eAgFnSc1d1	odlišná
<g/>
,	,	kIx,	,
od	od	k7c2	od
přibližně	přibližně	k6eAd1	přibližně
2	[number]	k4	2
hodin	hodina	k1gFnPc2	hodina
až	až	k9	až
po	po	k7c4	po
20	[number]	k4	20
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
okolo	okolo	k7c2	okolo
8	[number]	k4	8
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Spánek	spánek	k1gInSc1	spánek
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgMnPc2d1	další
savců	savec	k1gMnPc2	savec
je	být	k5eAaImIp3nS	být
načasován	načasovat	k5eAaPmNgInS	načasovat
především	především	k9	především
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
tma	tma	k6eAd1	tma
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
savců	savec	k1gMnPc2	savec
spí	spát	k5eAaImIp3nP	spát
ve	v	k7c6	v
dne	den	k1gInSc2	den
za	za	k7c2	za
světla	světlo	k1gNnSc2	světlo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
hlodavci	hlodavec	k1gMnPc1	hlodavec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
doba	doba	k1gFnSc1	doba
spánku	spánek	k1gInSc2	spánek
se	se	k3xPyFc4	se
od	od	k7c2	od
narození	narození	k1gNnSc2	narození
rychle	rychle	k6eAd1	rychle
zkracuje	zkracovat	k5eAaImIp3nS	zkracovat
od	od	k7c2	od
17	[number]	k4	17
<g/>
–	–	k?	–
<g/>
18	[number]	k4	18
hodin	hodina	k1gFnPc2	hodina
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
k	k	k7c3	k
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
12	[number]	k4	12
hodinám	hodina	k1gFnPc3	hodina
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
4	[number]	k4	4
let	léto	k1gNnPc2	léto
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
pozvolněji	pozvolně	k6eAd2	pozvolně
k	k	k7c3	k
celkem	celkem	k6eAd1	celkem
stabilní	stabilní	k2eAgFnSc3d1	stabilní
době	doba	k1gFnSc3	doba
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
8,5	[number]	k4	8,5
hodiny	hodina	k1gFnSc2	hodina
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Krátké	krátké	k1gNnSc1	krátké
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
hodinové	hodinový	k2eAgInPc1d1	hodinový
intervaly	interval	k1gInPc1	interval
spánku	spánek	k1gInSc2	spánek
novorozence	novorozenec	k1gMnSc2	novorozenec
se	se	k3xPyFc4	se
střídají	střídat	k5eAaImIp3nP	střídat
s	s	k7c7	s
krátkými	krátký	k2eAgInPc7d1	krátký
intervaly	interval	k1gInPc7	interval
krmení	krmení	k1gNnSc2	krmení
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
postupně	postupně	k6eAd1	postupně
nahrazovány	nahrazovat	k5eAaImNgInP	nahrazovat
plynulejším	plynulý	k2eAgInSc7d2	plynulejší
spánkem	spánek	k1gInSc7	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Spánkový	spánkový	k2eAgInSc1d1	spánkový
režim	režim	k1gInSc1	režim
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
trénovat	trénovat	k5eAaImF	trénovat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věku	věk	k1gInSc6	věk
4	[number]	k4	4
let	léto	k1gNnPc2	léto
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
spánek	spánek	k1gInSc1	spánek
spojen	spojen	k2eAgInSc1d1	spojen
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
noční	noční	k2eAgFnSc6d1	noční
periodě	perioda	k1gFnSc6	perioda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
s	s	k7c7	s
několika	několik	k4yIc7	několik
kratšími	krátký	k2eAgNnPc7d2	kratší
usnutími	usnutí	k1gNnPc7	usnutí
během	během	k7c2	během
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Dospělí	dospělý	k2eAgMnPc1d1	dospělý
lidé	člověk	k1gMnPc1	člověk
zpravidla	zpravidla	k6eAd1	zpravidla
spí	spát	k5eAaImIp3nP	spát
jednou	jeden	k4xCgFnSc7	jeden
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
někteří	některý	k3yIgMnPc1	některý
vrcholoví	vrcholový	k2eAgMnPc1d1	vrcholový
sportovci	sportovec	k1gMnPc1	sportovec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
spí	spát	k5eAaImIp3nP	spát
dvakrát	dvakrát	k6eAd1	dvakrát
denně	denně	k6eAd1	denně
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
tak	tak	k6eAd1	tak
podpořili	podpořit	k5eAaPmAgMnP	podpořit
celkovou	celkový	k2eAgFnSc4d1	celková
regeneraci	regenerace	k1gFnSc4	regenerace
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
do	do	k7c2	do
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
spaní	spaní	k1gNnSc4	spaní
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
nadvakrát	nadvakrát	k6eAd1	nadvakrát
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
jako	jako	k9	jako
u	u	k7c2	u
kojenců	kojenec	k1gMnPc2	kojenec
<g/>
)	)	kIx)	)
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnPc1	studie
dokonce	dokonce	k9	dokonce
potvrdily	potvrdit	k5eAaPmAgFnP	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
delší	dlouhý	k2eAgInSc1d2	delší
spánek	spánek	k1gInSc1	spánek
u	u	k7c2	u
basketbalistů	basketbalista	k1gMnPc2	basketbalista
má	mít	k5eAaImIp3nS	mít
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
přesnost	přesnost	k1gFnSc4	přesnost
<g/>
,	,	kIx,	,
reakční	reakční	k2eAgInSc4d1	reakční
čas	čas	k1gInSc4	čas
a	a	k8xC	a
rychlost	rychlost	k1gFnSc4	rychlost
běhu	běh	k1gInSc2	běh
<g/>
.	.	kIx.	.
</s>
<s>
Tohoto	tento	k3xDgInSc2	tento
výzkumu	výzkum	k1gInSc2	výzkum
se	se	k3xPyFc4	se
účastnili	účastnit	k5eAaImAgMnP	účastnit
univerzitní	univerzitní	k2eAgMnPc1d1	univerzitní
basketbalisté	basketbalista	k1gMnPc1	basketbalista
a	a	k8xC	a
do	do	k7c2	do
sedmi	sedm	k4xCc2	sedm
týdnů	týden	k1gInPc2	týden
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gInPc1	jejich
výkony	výkon	k1gInPc1	výkon
zlepšily	zlepšit	k5eAaPmAgInP	zlepšit
díky	díky	k7c3	díky
8,5	[number]	k4	8,5
<g/>
hodinovému	hodinový	k2eAgInSc3d1	hodinový
spánku	spánek	k1gInSc3	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
spí	spát	k5eAaImIp3nP	spát
v	v	k7c6	v
poloze	poloha	k1gFnSc6	poloha
vleže	vleže	k6eAd1	vleže
se	s	k7c7	s
zavřenýma	zavřený	k2eAgNnPc7d1	zavřené
očima	oko	k1gNnPc7	oko
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
spící	spící	k2eAgMnSc1d1	spící
vsedě	vsedě	k6eAd1	vsedě
nikdy	nikdy	k6eAd1	nikdy
neupadne	upadnout	k5eNaPmIp3nS	upadnout
do	do	k7c2	do
nejhlubšího	hluboký	k2eAgInSc2d3	nejhlubší
spánku	spánek	k1gInSc2	spánek
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
si	se	k3xPyFc3	se
dobře	dobře	k6eAd1	dobře
neodpočine	odpočinout	k5eNaPmIp3nS	odpočinout
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
savci	savec	k1gMnPc1	savec
ovšem	ovšem	k9	ovšem
spí	spát	k5eAaImIp3nP	spát
i	i	k9	i
s	s	k7c7	s
otevřenýma	otevřený	k2eAgNnPc7d1	otevřené
očima	oko	k1gNnPc7	oko
(	(	kIx(	(
<g/>
např.	např.	kA	např.
dobytek	dobytek	k1gInSc4	dobytek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
vestoje	vestoje	k6eAd1	vestoje
(	(	kIx(	(
<g/>
např.	např.	kA	např.
koně	kůň	k1gMnPc1	kůň
nebo	nebo	k8xC	nebo
sloni	slon	k1gMnPc1	slon
<g/>
)	)	kIx)	)
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
visí	viset	k5eAaImIp3nP	viset
za	za	k7c4	za
nohy	noha	k1gFnPc4	noha
dolů	dol	k1gInPc2	dol
(	(	kIx(	(
<g/>
např.	např.	kA	např.
netopýr	netopýr	k1gMnSc1	netopýr
nebo	nebo	k8xC	nebo
někteří	některý	k3yIgMnPc1	některý
papoušci	papoušek	k1gMnPc1	papoušek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
nepohyblivost	nepohyblivost	k1gFnSc1	nepohyblivost
je	být	k5eAaImIp3nS	být
relativní	relativní	k2eAgFnSc1d1	relativní
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
ryby	ryba	k1gFnPc1	ryba
plavou	plavat	k5eAaImIp3nP	plavat
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
savci	savec	k1gMnPc1	savec
se	se	k3xPyFc4	se
během	během	k7c2	během
spánku	spánek	k1gInSc2	spánek
periodicky	periodicky	k6eAd1	periodicky
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
některých	některý	k3yIgFnPc2	některý
poruch	porucha	k1gFnPc2	porucha
spánku	spánek	k1gInSc2	spánek
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
je	být	k5eAaImIp3nS	být
usnutí	usnutí	k1gNnSc1	usnutí
možné	možný	k2eAgNnSc1d1	možné
při	při	k7c6	při
chůzi	chůze	k1gFnSc6	chůze
nebo	nebo	k8xC	nebo
při	při	k7c6	při
hovoru	hovor	k1gInSc6	hovor
<g/>
.	.	kIx.	.
</s>
<s>
Spánek	spánek	k1gInSc1	spánek
savců	savec	k1gMnPc2	savec
a	a	k8xC	a
ptáků	pták	k1gMnPc2	pták
probíhá	probíhat	k5eAaImIp3nS	probíhat
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
principálně	principálně	k6eAd1	principálně
odlišných	odlišný	k2eAgInPc6d1	odlišný
režimech	režim	k1gInPc6	režim
<g/>
,	,	kIx,	,
ve	v	k7c6	v
fázi	fáze	k1gFnSc6	fáze
REM	REM	kA	REM
(	(	kIx(	(
<g/>
z	z	k7c2	z
ang	ang	k?	ang
<g/>
.	.	kIx.	.
rapid	rapid	k1gInSc1	rapid
eye	eye	k?	eye
movement	movement	k1gInSc4	movement
–	–	k?	–
rychlé	rychlý	k2eAgInPc4d1	rychlý
oční	oční	k2eAgInPc4d1	oční
pohyby	pohyb	k1gInPc4	pohyb
<g/>
)	)	kIx)	)
a	a	k8xC	a
NREM	NREM	kA	NREM
(	(	kIx(	(
<g/>
non-REM	non-REM	k?	non-REM
–	–	k?	–
opak	opak	k1gInSc1	opak
REM	REM	kA	REM
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
spánku	spánek	k1gInSc2	spánek
pravidelně	pravidelně	k6eAd1	pravidelně
střídají	střídat	k5eAaImIp3nP	střídat
<g/>
.	.	kIx.	.
</s>
<s>
Fáze	fáze	k1gFnSc1	fáze
NREM	NREM	kA	NREM
je	být	k5eAaImIp3nS	být
charakterizována	charakterizovat	k5eAaBmNgFnS	charakterizovat
útlumem	útlum	k1gInSc7	útlum
mozkové	mozkový	k2eAgFnSc2d1	mozková
činnosti	činnost	k1gFnSc2	činnost
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
tělesným	tělesný	k2eAgInSc7d1	tělesný
klidem	klid	k1gInSc7	klid
a	a	k8xC	a
uvolněním	uvolnění	k1gNnSc7	uvolnění
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
fáze	fáze	k1gFnSc1	fáze
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
čtyři	čtyři	k4xCgNnPc4	čtyři
stádia	stádium	k1gNnPc4	stádium
podle	podle	k7c2	podle
hloubky	hloubka	k1gFnSc2	hloubka
spánku	spánek	k1gInSc2	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
fázi	fáze	k1gFnSc4	fáze
REM	REM	kA	REM
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
intenzita	intenzita	k1gFnSc1	intenzita
mozkové	mozkový	k2eAgFnSc2d1	mozková
činnosti	činnost	k1gFnSc2	činnost
přibližně	přibližně	k6eAd1	přibližně
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
bdělého	bdělý	k2eAgInSc2d1	bdělý
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
rychlé	rychlý	k2eAgInPc1d1	rychlý
oční	oční	k2eAgInPc1d1	oční
pohyby	pohyb	k1gInPc1	pohyb
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
zavřenými	zavřený	k2eAgNnPc7d1	zavřené
víčky	víčko	k1gNnPc7	víčko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
živé	živý	k2eAgInPc1d1	živý
sny	sen	k1gInPc1	sen
a	a	k8xC	a
ztráta	ztráta	k1gFnSc1	ztráta
svalového	svalový	k2eAgNnSc2d1	svalové
napětí	napětí	k1gNnSc2	napětí
téměř	téměř	k6eAd1	téměř
všech	všecek	k3xTgFnPc2	všecek
vůlí	vůle	k1gFnPc2	vůle
ovládaných	ovládaný	k2eAgInPc2d1	ovládaný
svalů	sval	k1gInPc2	sval
<g/>
.	.	kIx.	.
</s>
<s>
Spánek	spánek	k1gInSc1	spánek
dospělého	dospělý	k2eAgMnSc2d1	dospělý
člověka	člověk	k1gMnSc2	člověk
obvykle	obvykle	k6eAd1	obvykle
začíná	začínat	k5eAaImIp3nS	začínat
postupným	postupný	k2eAgNnSc7d1	postupné
střídáním	střídání	k1gNnSc7	střídání
stadií	stadion	k1gNnPc2	stadion
NREM	NREM	kA	NREM
od	od	k7c2	od
prvního	první	k4xOgInSc2	první
do	do	k7c2	do
čtvrtého	čtvrtý	k4xOgNnSc2	čtvrtý
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
postup	postup	k1gInSc1	postup
je	být	k5eAaImIp3nS	být
občas	občas	k6eAd1	občas
přerušen	přerušit	k5eAaPmNgInS	přerušit
tělesnými	tělesný	k2eAgInPc7d1	tělesný
pohyby	pohyb	k1gInPc7	pohyb
a	a	k8xC	a
částečným	částečný	k2eAgNnSc7d1	částečné
probuzením	probuzení	k1gNnSc7	probuzení
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
po	po	k7c6	po
70	[number]	k4	70
<g/>
–	–	k?	–
<g/>
80	[number]	k4	80
minutách	minuta	k1gFnPc6	minuta
se	s	k7c7	s
spící	spící	k2eAgFnSc7d1	spící
obvykle	obvykle	k6eAd1	obvykle
krátce	krátce	k6eAd1	krátce
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
třetího	třetí	k4xOgMnSc2	třetí
nebo	nebo	k8xC	nebo
druhého	druhý	k4xOgNnSc2	druhý
stadia	stadion	k1gNnSc2	stadion
a	a	k8xC	a
následuje	následovat	k5eAaImIp3nS	následovat
první	první	k4xOgFnSc1	první
fáze	fáze	k1gFnSc1	fáze
REM	REM	kA	REM
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
trvá	trvat	k5eAaImIp3nS	trvat
asi	asi	k9	asi
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
celého	celý	k2eAgInSc2d1	celý
cyklu	cyklus	k1gInSc2	cyklus
<g/>
,	,	kIx,	,
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
prvního	první	k4xOgNnSc2	první
stadia	stadion	k1gNnPc1	stadion
NREM	NREM	kA	NREM
až	až	k6eAd1	až
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
první	první	k4xOgFnSc2	první
fáze	fáze	k1gFnSc2	fáze
REM	REM	kA	REM
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
90	[number]	k4	90
<g/>
–	–	k?	–
<g/>
110	[number]	k4	110
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
typických	typický	k2eAgInPc6d1	typický
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
cyklus	cyklus	k1gInSc1	cyklus
opakuje	opakovat	k5eAaImIp3nS	opakovat
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
až	až	k9	až
šestkrát	šestkrát	k6eAd1	šestkrát
za	za	k7c4	za
noc	noc	k1gFnSc4	noc
a	a	k8xC	a
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
následujícím	následující	k2eAgInSc6d1	následující
cyklu	cyklus	k1gInSc6	cyklus
se	se	k3xPyFc4	se
třetí	třetí	k4xOgNnSc4	třetí
a	a	k8xC	a
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
stadium	stadium	k1gNnSc4	stadium
NREM	NREM	kA	NREM
zkracuje	zkracovat	k5eAaImIp3nS	zkracovat
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
narůstá	narůstat	k5eAaImIp3nS	narůstat
délka	délka	k1gFnSc1	délka
fáze	fáze	k1gFnSc2	fáze
REM	REM	kA	REM
<g/>
.	.	kIx.	.
</s>
<s>
Přesný	přesný	k2eAgInSc1d1	přesný
průběh	průběh	k1gInSc1	průběh
spánkového	spánkový	k2eAgInSc2d1	spánkový
cyklu	cyklus	k1gInSc2	cyklus
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
každého	každý	k3xTgMnSc4	každý
jedince	jedinec	k1gMnSc4	jedinec
odlišný	odlišný	k2eAgInSc1d1	odlišný
a	a	k8xC	a
mění	měnit	k5eAaImIp3nS	měnit
se	se	k3xPyFc4	se
i	i	k9	i
s	s	k7c7	s
věkem	věk	k1gInSc7	věk
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
novorozenců	novorozenec	k1gMnPc2	novorozenec
tvoří	tvořit	k5eAaImIp3nS	tvořit
fáze	fáze	k1gFnSc1	fáze
REM	REM	kA	REM
50	[number]	k4	50
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
doby	doba	k1gFnSc2	doba
spánku	spánek	k1gInSc2	spánek
a	a	k8xC	a
v	v	k7c6	v
několika	několik	k4yIc6	několik
ohledech	ohled	k1gInPc6	ohled
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
spánku	spánek	k1gInSc2	spánek
dospělého	dospělý	k2eAgMnSc2d1	dospělý
jedince	jedinec	k1gMnSc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
spánku	spánek	k1gInSc2	spánek
REM	REM	kA	REM
na	na	k7c6	na
celkové	celkový	k2eAgFnSc6d1	celková
době	doba	k1gFnSc6	doba
spánku	spánek	k1gInSc2	spánek
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
snižuje	snižovat	k5eAaImIp3nS	snižovat
přibližně	přibližně	k6eAd1	přibližně
do	do	k7c2	do
věku	věk	k1gInSc2	věk
4	[number]	k4	4
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
tvoří	tvořit	k5eAaImIp3nS	tvořit
okolo	okolo	k7c2	okolo
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
25	[number]	k4	25
%	%	kIx~	%
a	a	k8xC	a
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
množství	množství	k1gNnSc6	množství
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
mladší	mladý	k2eAgInSc4d2	mladší
věk	věk	k1gInSc4	věk
dospělosti	dospělost	k1gFnSc2	dospělost
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vyšším	vysoký	k2eAgInSc6d2	vyšší
věku	věk	k1gInSc6	věk
REM	REM	kA	REM
postupně	postupně	k6eAd1	postupně
klesá	klesat	k5eAaImIp3nS	klesat
k	k	k7c3	k
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
mladých	mladý	k2eAgFnPc2d1	mladá
dospělých	dospělý	k2eAgFnPc2d1	dospělá
osob	osoba	k1gFnPc2	osoba
tvoří	tvořit	k5eAaImIp3nS	tvořit
fáze	fáze	k1gFnSc1	fáze
REM	REM	kA	REM
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
25	[number]	k4	25
%	%	kIx~	%
celkového	celkový	k2eAgNnSc2d1	celkové
množství	množství	k1gNnSc2	množství
spánku	spánek	k1gInSc2	spánek
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgNnSc4	třetí
a	a	k8xC	a
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
stadium	stadium	k1gNnSc4	stadium
NREM	NREM	kA	NREM
asi	asi	k9	asi
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
%	%	kIx~	%
a	a	k8xC	a
první	první	k4xOgNnSc4	první
stadium	stadium	k1gNnSc4	stadium
NREM	NREM	kA	NREM
asi	asi	k9	asi
5	[number]	k4	5
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
část	část	k1gFnSc1	část
spánku	spánek	k1gInSc2	spánek
probíhá	probíhat	k5eAaImIp3nS	probíhat
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
stadiu	stadion	k1gNnSc6	stadion
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
okolo	okolo	k7c2	okolo
50	[number]	k4	50
<g/>
–	–	k?	–
<g/>
60	[number]	k4	60
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Usínání	usínání	k1gNnSc1	usínání
(	(	kIx(	(
<g/>
hypnagogium	hypnagogium	k1gNnSc1	hypnagogium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
přechodný	přechodný	k2eAgInSc4d1	přechodný
stav	stav	k1gInSc4	stav
mezi	mezi	k7c7	mezi
bdělostí	bdělost	k1gFnSc7	bdělost
a	a	k8xC	a
spánkem	spánek	k1gInSc7	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Přechod	přechod	k1gInSc1	přechod
z	z	k7c2	z
bdělého	bdělý	k2eAgMnSc2d1	bdělý
do	do	k7c2	do
spánkového	spánkový	k2eAgInSc2d1	spánkový
stavu	stav	k1gInSc2	stav
probíhá	probíhat	k5eAaImIp3nS	probíhat
vždy	vždy	k6eAd1	vždy
podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
rozdílným	rozdílný	k2eAgNnSc7d1	rozdílné
tempem	tempo	k1gNnSc7	tempo
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
výrazné	výrazný	k2eAgInPc1d1	výrazný
tělesné	tělesný	k2eAgInPc1d1	tělesný
pohyby	pohyb	k1gInPc1	pohyb
a	a	k8xC	a
změny	změna	k1gFnPc1	změna
polohy	poloha	k1gFnSc2	poloha
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
následuje	následovat	k5eAaImIp3nS	následovat
prohloubené	prohloubený	k2eAgNnSc4d1	prohloubené
dýchání	dýchání	k1gNnSc4	dýchání
a	a	k8xC	a
pomalé	pomalý	k2eAgNnSc4d1	pomalé
zavírání	zavírání	k1gNnSc4	zavírání
očí	oko	k1gNnPc2	oko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
stadiu	stadion	k1gNnSc6	stadion
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
objevují	objevovat	k5eAaImIp3nP	objevovat
výrazné	výrazný	k2eAgFnPc1d1	výrazná
svalové	svalový	k2eAgFnPc1d1	svalová
křeče	křeč	k1gFnPc1	křeč
provázené	provázený	k2eAgFnPc1d1	provázená
škubnutím	škubnutí	k1gNnSc7	škubnutí
celého	celý	k2eAgNnSc2d1	celé
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
vést	vést	k5eAaImF	vést
ke	k	k7c3	k
krátkému	krátký	k2eAgNnSc3d1	krátké
přechodnému	přechodný	k2eAgNnSc3d1	přechodné
probuzení	probuzení	k1gNnSc3	probuzení
<g/>
.	.	kIx.	.
</s>
<s>
Křeče	křeč	k1gFnPc1	křeč
jsou	být	k5eAaImIp3nP	být
nejspíše	nejspíše	k9	nejspíše
vyvolávány	vyvoláván	k2eAgInPc1d1	vyvoláván
motorickými	motorický	k2eAgInPc7d1	motorický
impulsy	impuls	k1gInPc7	impuls
z	z	k7c2	z
nižších	nízký	k2eAgNnPc2d2	nižší
mozkových	mozkový	k2eAgNnPc2d1	mozkové
center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
projevem	projev	k1gInSc7	projev
nervových	nervový	k2eAgInPc2d1	nervový
procesů	proces	k1gInPc2	proces
reagujících	reagující	k2eAgInPc2d1	reagující
na	na	k7c4	na
přechody	přechod	k1gInPc4	přechod
k	k	k7c3	k
další	další	k2eAgFnSc3d1	další
etapě	etapa	k1gFnSc3	etapa
spánku	spánek	k1gInSc2	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
usínání	usínání	k1gNnSc6	usínání
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
snižuje	snižovat	k5eAaImIp3nS	snižovat
svalové	svalový	k2eAgNnSc4d1	svalové
napětí	napětí	k1gNnSc4	napětí
<g/>
,	,	kIx,	,
krevní	krevní	k2eAgInSc4d1	krevní
tlak	tlak	k1gInSc4	tlak
i	i	k8xC	i
tepová	tepový	k2eAgFnSc1d1	tepová
frekvence	frekvence	k1gFnSc1	frekvence
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
usínání	usínání	k1gNnSc2	usínání
má	mít	k5eAaImIp3nS	mít
člověk	člověk	k1gMnSc1	člověk
sklon	sklon	k1gInSc4	sklon
k	k	k7c3	k
těkavým	těkavý	k2eAgFnPc3d1	těkavá
myšlenkám	myšlenka	k1gFnPc3	myšlenka
a	a	k8xC	a
polosnům	polosen	k1gInPc3	polosen
<g/>
,	,	kIx,	,
snadno	snadno	k6eAd1	snadno
podléhá	podléhat	k5eAaImIp3nS	podléhat
smyslovým	smyslový	k2eAgInPc3d1	smyslový
klamům	klam	k1gInPc3	klam
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
pád	pád	k1gInSc1	pád
z	z	k7c2	z
kola	kolo	k1gNnSc2	kolo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgFnPc4	který
může	moct	k5eAaImIp3nS	moct
zareagovat	zareagovat	k5eAaPmF	zareagovat
škubnutím	škubnutí	k1gNnSc7	škubnutí
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
následným	následný	k2eAgNnSc7d1	následné
probuzením	probuzení	k1gNnSc7	probuzení
<g/>
.	.	kIx.	.
</s>
<s>
Objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
také	také	k9	také
specifické	specifický	k2eAgFnPc1d1	specifická
pseudohalucinace	pseudohalucinace	k1gFnPc1	pseudohalucinace
–	–	k?	–
pokud	pokud	k8xS	pokud
člověk	člověk	k1gMnSc1	člověk
vykonával	vykonávat	k5eAaImAgMnS	vykonávat
nějakou	nějaký	k3yIgFnSc4	nějaký
zdlouhavou	zdlouhavý	k2eAgFnSc4d1	zdlouhavá
jednotvárnou	jednotvárný	k2eAgFnSc4d1	jednotvárná
činnost	činnost	k1gFnSc4	činnost
(	(	kIx(	(
<g/>
psaní	psaní	k1gNnSc4	psaní
na	na	k7c6	na
stroji	stroj	k1gInSc6	stroj
<g/>
,	,	kIx,	,
zakládání	zakládání	k1gNnSc6	zakládání
listů	list	k1gInPc2	list
<g/>
,	,	kIx,	,
počítačová	počítačový	k2eAgFnSc1d1	počítačová
hra	hra	k1gFnSc1	hra
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
útržky	útržka	k1gFnSc2	útržka
této	tento	k3xDgFnSc2	tento
činnosti	činnost	k1gFnSc2	činnost
se	se	k3xPyFc4	se
mu	on	k3xPp3gInSc3	on
stále	stále	k6eAd1	stále
míhají	míhat	k5eAaImIp3nP	míhat
před	před	k7c7	před
očima	oko	k1gNnPc7	oko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
NREM	NREM	kA	NREM
spánku	spánek	k1gInSc2	spánek
je	být	k5eAaImIp3nS	být
aktivita	aktivita	k1gFnSc1	aktivita
neuronů	neuron	k1gInPc2	neuron
celkově	celkově	k6eAd1	celkově
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
odráží	odrážet	k5eAaImIp3nS	odrážet
v	v	k7c6	v
nízké	nízký	k2eAgFnSc6d1	nízká
úrovni	úroveň	k1gFnSc6	úroveň
metabolizmu	metabolizmus	k1gInSc2	metabolizmus
a	a	k8xC	a
nízké	nízký	k2eAgFnSc6d1	nízká
teplotě	teplota	k1gFnSc6	teplota
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
fázi	fáze	k1gFnSc6	fáze
dochází	docházet	k5eAaImIp3nS	docházet
také	také	k9	také
tělesné	tělesný	k2eAgFnSc3d1	tělesná
regeneraci	regenerace	k1gFnSc3	regenerace
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zjednává	zjednávat	k5eAaImIp3nS	zjednávat
optimální	optimální	k2eAgFnPc4d1	optimální
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
syntézu	syntéza	k1gFnSc4	syntéza
základních	základní	k2eAgInPc2d1	základní
proteinů	protein	k1gInPc2	protein
<g/>
.	.	kIx.	.
</s>
<s>
Přechod	přechod	k1gInSc1	přechod
z	z	k7c2	z
bdění	bdění	k1gNnSc2	bdění
do	do	k7c2	do
spánku	spánek	k1gInSc2	spánek
NREM	NREM	kA	NREM
je	být	k5eAaImIp3nS	být
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
postupně	postupně	k6eAd1	postupně
se	s	k7c7	s
zpomalující	zpomalující	k2eAgFnSc7d1	zpomalující
frekvencí	frekvence	k1gFnSc7	frekvence
a	a	k8xC	a
prokazatelným	prokazatelný	k2eAgInSc7d1	prokazatelný
záznamem	záznam	k1gInSc7	záznam
elektroencefalografu	elektroencefalograf	k1gInSc2	elektroencefalograf
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnSc1d2	podrobnější
analýza	analýza	k1gFnSc1	analýza
průběhu	průběh	k1gInSc2	průběh
EEG	EEG	kA	EEG
vln	vlna	k1gFnPc2	vlna
napovídá	napovídat	k5eAaBmIp3nS	napovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
NREM	NREM	kA	NREM
fáze	fáze	k1gFnSc1	fáze
sestává	sestávat	k5eAaImIp3nS	sestávat
ze	z	k7c2	z
čtyř	čtyři	k4xCgNnPc2	čtyři
stadií	stadion	k1gNnPc2	stadion
o	o	k7c6	o
různé	různý	k2eAgFnSc6d1	různá
hloubce	hloubka	k1gFnSc6	hloubka
spánku	spánek	k1gInSc2	spánek
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
stadium	stadium	k1gNnSc4	stadium
<g/>
:	:	kIx,	:
trvá	trvat	k5eAaImIp3nS	trvat
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc4	několik
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
přechodem	přechod	k1gInSc7	přechod
od	od	k7c2	od
bdění	bdění	k1gNnSc2	bdění
k	k	k7c3	k
začátku	začátek	k1gInSc3	začátek
spánku	spánek	k1gInSc2	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Vlny	vlna	k1gFnPc1	vlna
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
nepravidelnými	pravidelný	k2eNgFnPc7d1	nepravidelná
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
amplituda	amplituda	k1gFnSc1	amplituda
se	se	k3xPyFc4	se
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
tohoto	tento	k3xDgNnSc2	tento
stádia	stádium	k1gNnSc2	stádium
lze	lze	k6eAd1	lze
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
pomalé	pomalý	k2eAgInPc4d1	pomalý
valivé	valivý	k2eAgInPc4d1	valivý
pohyby	pohyb	k1gInPc4	pohyb
očí	oko	k1gNnPc2	oko
a	a	k8xC	a
EEG	EEG	kA	EEG
vlny	vlna	k1gFnPc4	vlna
s	s	k7c7	s
nízkou	nízký	k2eAgFnSc7d1	nízká
voltáží	voltáž	k1gFnSc7	voltáž
a	a	k8xC	a
smíšenou	smíšený	k2eAgFnSc7d1	smíšená
frekvencí	frekvence	k1gFnSc7	frekvence
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
stadiu	stadion	k1gNnSc6	stadion
a	a	k8xC	a
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
celé	celý	k2eAgFnSc2d1	celá
fáze	fáze	k1gFnSc2	fáze
NREM	NREM	kA	NREM
je	být	k5eAaImIp3nS	být
patrná	patrný	k2eAgFnSc1d1	patrná
určitá	určitý	k2eAgFnSc1d1	určitá
aktivita	aktivita	k1gFnSc1	aktivita
kosterního	kosterní	k2eAgNnSc2d1	kosterní
svalstva	svalstvo	k1gNnSc2	svalstvo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikoliv	nikoliv	k9	nikoliv
rychlé	rychlý	k2eAgInPc4d1	rychlý
pohyby	pohyb	k1gInPc4	pohyb
očí	oko	k1gNnPc2	oko
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
pro	pro	k7c4	pro
REM	REM	kA	REM
fázi	fáze	k1gFnSc6	fáze
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
stadium	stadium	k1gNnSc4	stadium
<g/>
:	:	kIx,	:
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
spánková	spánkový	k2eAgNnPc1d1	spánkové
vřeténka	vřeténko	k1gNnPc1	vřeténko
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
krátké	krátký	k2eAgInPc4d1	krátký
úseky	úsek	k1gInPc4	úsek
rytmických	rytmický	k2eAgFnPc2d1	rytmická
vln	vlna	k1gFnPc2	vlna
s	s	k7c7	s
frekvencí	frekvence	k1gFnSc7	frekvence
12	[number]	k4	12
<g/>
–	–	k?	–
<g/>
16	[number]	k4	16
Hz	Hz	kA	Hz
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
amplituda	amplituda	k1gFnSc1	amplituda
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
celého	celý	k2eAgInSc2d1	celý
EEG	EEG	kA	EEG
náhle	náhle	k6eAd1	náhle
klesá	klesat	k5eAaImIp3nS	klesat
a	a	k8xC	a
narůstá	narůstat	k5eAaImIp3nS	narůstat
(	(	kIx(	(
<g/>
K-komplex	Komplex	k1gInSc4	K-komplex
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
stadium	stadium	k1gNnSc4	stadium
<g/>
:	:	kIx,	:
objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
pomalé	pomalý	k2eAgFnSc2d1	pomalá
vlny	vlna	k1gFnSc2	vlna
o	o	k7c6	o
frekvenci	frekvence	k1gFnSc6	frekvence
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
Hz	Hz	kA	Hz
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
vlny	vlna	k1gFnPc1	vlna
delta	delta	k1gNnSc2	delta
<g/>
,	,	kIx,	,
a	a	k8xC	a
představují	představovat	k5eAaImIp3nP	představovat
asi	asi	k9	asi
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
%	%	kIx~	%
všech	všecek	k3xTgFnPc2	všecek
vln	vlna	k1gFnPc2	vlna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
fázi	fáze	k1gFnSc6	fáze
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
člověka	člověk	k1gMnSc4	člověk
obtížné	obtížný	k2eAgNnSc1d1	obtížné
probudit	probudit	k5eAaPmF	probudit
např.	např.	kA	např.
hlukem	hluk	k1gInSc7	hluk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
jej	on	k3xPp3gMnSc4	on
vzbudit	vzbudit	k5eAaPmF	vzbudit
např.	např.	kA	např.
voláním	volání	k1gNnSc7	volání
jeho	on	k3xPp3gNnSc2	on
jména	jméno	k1gNnSc2	jméno
nebo	nebo	k8xC	nebo
dětským	dětský	k2eAgInSc7d1	dětský
pláčem	pláč	k1gInSc7	pláč
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
stadium	stadium	k1gNnSc1	stadium
<g/>
:	:	kIx,	:
delta	delta	k1gFnSc1	delta
vlny	vlna	k1gFnSc2	vlna
tvoří	tvořit	k5eAaImIp3nS	tvořit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
%	%	kIx~	%
všech	všecek	k3xTgFnPc2	všecek
vln	vlna	k1gFnPc2	vlna
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
hluboký	hluboký	k2eAgInSc4d1	hluboký
spánek	spánek	k1gInSc4	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Stadia	stadion	k1gNnPc4	stadion
3	[number]	k4	3
a	a	k8xC	a
4	[number]	k4	4
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
nazývají	nazývat	k5eAaImIp3nP	nazývat
u	u	k7c2	u
člověka	člověk	k1gMnSc4	člověk
hluboký	hluboký	k2eAgInSc1d1	hluboký
NREM	NREM	kA	NREM
spánek	spánek	k1gInSc1	spánek
nebo	nebo	k8xC	nebo
delta	delta	k1gFnSc1	delta
spánek	spánek	k1gInSc1	spánek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
REM	REM	kA	REM
stadia	stadion	k1gNnSc2	stadion
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
náhlému	náhlý	k2eAgInSc3d1	náhlý
výskytu	výskyt	k1gInSc3	výskyt
očních	oční	k2eAgInPc2d1	oční
pohybů	pohyb	k1gInPc2	pohyb
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
trvají	trvat	k5eAaImIp3nP	trvat
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
REM	REM	kA	REM
spánku	spánek	k1gInSc2	spánek
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
téměř	téměř	k6eAd1	téměř
strnulý	strnulý	k2eAgMnSc1d1	strnulý
<g/>
,	,	kIx,	,
ušetřen	ušetřen	k2eAgMnSc1d1	ušetřen
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
srdeční	srdeční	k2eAgInSc1d1	srdeční
sval	sval	k1gInSc1	sval
<g/>
,	,	kIx,	,
bránice	bránice	k1gFnPc1	bránice
<g/>
,	,	kIx,	,
okohybné	okohybný	k2eAgInPc1d1	okohybný
svaly	sval	k1gInPc1	sval
a	a	k8xC	a
hladké	hladký	k2eAgNnSc1d1	hladké
svalstvo	svalstvo	k1gNnSc1	svalstvo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
REM	REM	kA	REM
fázi	fáze	k1gFnSc4	fáze
začne	začít	k5eAaPmIp3nS	začít
člověk	člověk	k1gMnSc1	člověk
těžce	těžce	k6eAd1	těžce
a	a	k8xC	a
nepravidelně	pravidelně	k6eNd1	pravidelně
dýchat	dýchat	k5eAaImF	dýchat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
srdeční	srdeční	k2eAgFnSc1d1	srdeční
frekvence	frekvence	k1gFnSc1	frekvence
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
na	na	k7c4	na
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
bdělému	bdělý	k2eAgInSc3d1	bdělý
stavu	stav	k1gInSc3	stav
<g/>
.	.	kIx.	.
</s>
<s>
Probudit	probudit	k5eAaPmF	probudit
člověka	člověk	k1gMnSc4	člověk
v	v	k7c6	v
REM	REM	kA	REM
fázi	fáze	k1gFnSc4	fáze
je	být	k5eAaImIp3nS	být
obtížnější	obtížný	k2eAgMnSc1d2	obtížnější
než	než	k8xS	než
v	v	k7c6	v
ostatních	ostatní	k2eAgNnPc6d1	ostatní
spánkových	spánkový	k2eAgNnPc6d1	spánkové
stadiích	stadion	k1gNnPc6	stadion
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
úbytku	úbytek	k1gInSc3	úbytek
svalového	svalový	k2eAgNnSc2d1	svalové
napětí	napětí	k1gNnSc2	napětí
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
patrné	patrný	k2eAgNnSc1d1	patrné
v	v	k7c6	v
uvolnění	uvolnění	k1gNnSc6	uvolnění
obličeje	obličej	k1gInSc2	obličej
spícího	spící	k2eAgInSc2d1	spící
<g/>
.	.	kIx.	.
</s>
<s>
Změny	změna	k1gFnPc1	změna
průtoku	průtok	k1gInSc2	průtok
krve	krev	k1gFnSc2	krev
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
erekci	erekce	k1gFnSc6	erekce
a	a	k8xC	a
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
prokrvení	prokrvení	k1gNnSc2	prokrvení
vaginální	vaginální	k2eAgFnSc2d1	vaginální
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Autonomní	autonomní	k2eAgInSc1d1	autonomní
nervový	nervový	k2eAgInSc1d1	nervový
systém	systém	k1gInSc1	systém
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
značnými	značný	k2eAgFnPc7d1	značná
nepravidelnostmi	nepravidelnost	k1gFnPc7	nepravidelnost
v	v	k7c6	v
pulsu	puls	k1gInSc6	puls
<g/>
,	,	kIx,	,
dýchání	dýchání	k1gNnSc6	dýchání
a	a	k8xC	a
hodnotách	hodnota	k1gFnPc6	hodnota
krevního	krevní	k2eAgInSc2d1	krevní
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Mozek	mozek	k1gInSc1	mozek
má	mít	k5eAaImIp3nS	mít
zvýšenou	zvýšený	k2eAgFnSc4d1	zvýšená
spotřebu	spotřeba	k1gFnSc4	spotřeba
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
na	na	k7c4	na
regenerativní	regenerativní	k2eAgFnSc4d1	regenerativní
funkci	funkce	k1gFnSc4	funkce
spánku	spánek	k1gInSc2	spánek
pro	pro	k7c4	pro
centrální	centrální	k2eAgFnSc4d1	centrální
nervovou	nervový	k2eAgFnSc4d1	nervová
soustavu	soustava	k1gFnSc4	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Zjistilo	zjistit	k5eAaPmAgNnS	zjistit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
náhlé	náhlý	k2eAgInPc1d1	náhlý
pohyby	pohyb	k1gInPc1	pohyb
<g/>
,	,	kIx,	,
kterými	který	k3yRgInPc7	který
spící	spící	k2eAgMnSc1d1	spící
jedinec	jedinec	k1gMnSc1	jedinec
mění	měnit	k5eAaImIp3nS	měnit
svou	svůj	k3xOyFgFnSc4	svůj
polohu	poloha	k1gFnSc4	poloha
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
zpravidla	zpravidla	k6eAd1	zpravidla
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
nebo	nebo	k8xC	nebo
těsně	těsně	k6eAd1	těsně
po	po	k7c6	po
REM	REM	kA	REM
fázi	fáze	k1gFnSc6	fáze
spánku	spánek	k1gInSc6	spánek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
REM	REM	kA	REM
fáze	fáze	k1gFnSc1	fáze
probíhá	probíhat	k5eAaImIp3nS	probíhat
velká	velký	k2eAgFnSc1d1	velká
většina	většina	k1gFnSc1	většina
snů	sen	k1gInPc2	sen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
fázi	fáze	k1gFnSc6	fáze
spánku	spánek	k1gInSc2	spánek
jsou	být	k5eAaImIp3nP	být
sny	sen	k1gInPc1	sen
výrazné	výrazný	k2eAgFnSc2d1	výrazná
až	až	k8xS	až
mimořádně	mimořádně	k6eAd1	mimořádně
živé	živý	k2eAgInPc1d1	živý
a	a	k8xC	a
mívají	mívat	k5eAaImIp3nP	mívat
bizarní	bizarní	k2eAgInSc4d1	bizarní
a	a	k8xC	a
nelogický	logický	k2eNgInSc4d1	nelogický
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
REM	REM	kA	REM
fázi	fáze	k1gFnSc6	fáze
spánku	spánek	k1gInSc2	spánek
je	být	k5eAaImIp3nS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
fyziologicky	fyziologicky	k6eAd1	fyziologicky
podstatou	podstata	k1gFnSc7	podstata
snové	snový	k2eAgFnSc2d1	snová
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
bývá	bývat	k5eAaImIp3nS	bývat
nazývána	nazývat	k5eAaImNgFnS	nazývat
také	také	k9	také
D-stav	Dtav	k1gMnPc3	D-stav
(	(	kIx(	(
<g/>
od	od	k7c2	od
anglického	anglický	k2eAgNnSc2d1	anglické
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
dream	dream	k6eAd1	dream
<g/>
"	"	kIx"	"
=	=	kIx~	=
sen	sen	k1gInSc1	sen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Srovnáním	srovnání	k1gNnSc7	srovnání
spánku	spánek	k1gInSc2	spánek
REM	REM	kA	REM
a	a	k8xC	a
NREM	NREM	kA	NREM
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
někteří	některý	k3yIgMnPc1	některý
vědci	vědec	k1gMnPc1	vědec
přestali	přestat	k5eAaPmAgMnP	přestat
REM	REM	kA	REM
stadium	stadium	k1gNnSc4	stadium
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
spánek	spánek	k1gInSc4	spánek
a	a	k8xC	a
označují	označovat	k5eAaImIp3nP	označovat
ho	on	k3xPp3gInSc4	on
spíše	spíše	k9	spíše
za	za	k7c4	za
třetí	třetí	k4xOgInSc4	třetí
stav	stav	k1gInSc4	stav
existence	existence	k1gFnSc2	existence
mimo	mimo	k7c4	mimo
stav	stav	k1gInSc4	stav
bdění	bdění	k1gNnSc2	bdění
a	a	k8xC	a
NREM	NREM	kA	NREM
spánku	spánek	k1gInSc2	spánek
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k8xS	jako
paradoxní	paradoxní	k2eAgInSc1d1	paradoxní
spánek	spánek	k1gInSc1	spánek
<g/>
.	.	kIx.	.
</s>
<s>
REM	REM	kA	REM
fáze	fáze	k1gFnSc1	fáze
(	(	kIx(	(
<g/>
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
sny	sen	k1gInPc1	sen
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
prokázána	prokázat	k5eAaPmNgFnS	prokázat
u	u	k7c2	u
všech	všecek	k3xTgInPc2	všecek
druhů	druh	k1gInPc2	druh
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
u	u	k7c2	u
každého	každý	k3xTgMnSc2	každý
jinak	jinak	k6eAd1	jinak
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
–	–	k?	–
nejdelší	dlouhý	k2eAgFnSc4d3	nejdelší
REM	REM	kA	REM
fázi	fáze	k1gFnSc4	fáze
má	mít	k5eAaImIp3nS	mít
ptakopysk	ptakopysk	k1gMnSc1	ptakopysk
(	(	kIx(	(
<g/>
57	[number]	k4	57
%	%	kIx~	%
spánku	spánek	k1gInSc2	spánek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejkratší	krátký	k2eAgMnSc1d3	nejkratší
delfín	delfín	k1gMnSc1	delfín
(	(	kIx(	(
<g/>
2	[number]	k4	2
%	%	kIx~	%
spánku	spánek	k1gInSc2	spánek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
REM	REM	kA	REM
fázi	fáze	k1gFnSc6	fáze
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
byla	být	k5eAaImAgFnS	být
popsána	popsat	k5eAaPmNgFnS	popsat
u	u	k7c2	u
plaza	plaz	k1gMnSc2	plaz
agamy	agama	k1gFnSc2	agama
vousaté	vousatý	k2eAgInPc1d1	vousatý
(	(	kIx(	(
<g/>
Pogona	Pogona	k1gFnSc1	Pogona
vitticeps	vitticepsa	k1gFnPc2	vitticepsa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
existence	existence	k1gFnSc1	existence
fází	fáze	k1gFnPc2	fáze
REM	REM	kA	REM
a	a	k8xC	a
NREM	NREM	kA	NREM
je	být	k5eAaImIp3nS	být
evolučně	evolučně	k6eAd1	evolučně
poměrně	poměrně	k6eAd1	poměrně
stará	starat	k5eAaImIp3nS	starat
a	a	k8xC	a
sahá	sahat	k5eAaImIp3nS	sahat
až	až	k9	až
ke	k	k7c3	k
společnému	společný	k2eAgInSc3d1	společný
předku	předek	k1gInSc3	předek
celé	celý	k2eAgFnSc2d1	celá
skupiny	skupina	k1gFnSc2	skupina
Amniota	Amniota	k1gFnSc1	Amniota
před	před	k7c7	před
312	[number]	k4	312
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Probouzení	probouzení	k1gNnSc1	probouzení
(	(	kIx(	(
<g/>
hypnexagogium	hypnexagogium	k1gNnSc1	hypnexagogium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
návrat	návrat	k1gInSc4	návrat
ze	z	k7c2	z
spánku	spánek	k1gInSc2	spánek
do	do	k7c2	do
stavu	stav	k1gInSc2	stav
bdělosti	bdělost	k1gFnSc2	bdělost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnohém	mnohé	k1gNnSc6	mnohé
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
fázi	fáze	k1gFnSc3	fáze
usínání	usínání	k1gNnSc2	usínání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zpravidla	zpravidla	k6eAd1	zpravidla
probíhá	probíhat	k5eAaImIp3nS	probíhat
mnohem	mnohem	k6eAd1	mnohem
rychleji	rychle	k6eAd2	rychle
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
někoho	někdo	k3yInSc2	někdo
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
přetrvávat	přetrvávat	k5eAaImF	přetrvávat
stav	stav	k1gInSc4	stav
podobný	podobný	k2eAgInSc4d1	podobný
náměsíčnosti	náměsíčnost	k1gFnSc5	náměsíčnost
<g/>
,	,	kIx,	,
člověk	člověk	k1gMnSc1	člověk
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
později	pozdě	k6eAd2	pozdě
pamatoval	pamatovat	k5eAaImAgInS	pamatovat
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
dokážou	dokázat	k5eAaPmIp3nP	dokázat
probudit	probudit	k5eAaPmF	probudit
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
přesně	přesně	k6eAd1	přesně
v	v	k7c4	v
určenou	určený	k2eAgFnSc4d1	určená
hodinu	hodina	k1gFnSc4	hodina
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nP	by
používali	používat	k5eAaImAgMnP	používat
budík	budík	k1gInSc4	budík
<g/>
.	.	kIx.	.
</s>
<s>
Přesnost	přesnost	k1gFnSc1	přesnost
probuzení	probuzení	k1gNnSc2	probuzení
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
obdivuhodná	obdivuhodný	k2eAgFnSc1d1	obdivuhodná
<g/>
.	.	kIx.	.
</s>
<s>
Sen	sen	k1gInSc1	sen
je	být	k5eAaImIp3nS	být
prožitek	prožitek	k1gInSc4	prožitek
zdánlivých	zdánlivý	k2eAgInPc2d1	zdánlivý
smyslových	smyslový	k2eAgInPc2d1	smyslový
vjemů	vjem	k1gInPc2	vjem
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
zrakových	zrakový	k2eAgMnPc2d1	zrakový
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
emocí	emoce	k1gFnPc2	emoce
a	a	k8xC	a
myšlenek	myšlenka	k1gFnPc2	myšlenka
vnímaných	vnímaný	k2eAgFnPc2d1	vnímaná
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
spánku	spánek	k1gInSc2	spánek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nejsou	být	k5eNaImIp3nP	být
ovládány	ovládat	k5eAaImNgInP	ovládat
vůlí	vůle	k1gFnSc7	vůle
snícího	snící	k2eAgMnSc4d1	snící
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Převážná	převážný	k2eAgFnSc1d1	převážná
většina	většina	k1gFnSc1	většina
snů	sen	k1gInPc2	sen
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
REM	REM	kA	REM
fáze	fáze	k1gFnSc1	fáze
spánku	spánek	k1gInSc2	spánek
<g/>
;	;	kIx,	;
když	když	k8xS	když
byli	být	k5eAaImAgMnP	být
při	při	k7c6	při
pokusu	pokus	k1gInSc6	pokus
dobrovolníci	dobrovolník	k1gMnPc1	dobrovolník
probuzeni	probuzen	k2eAgMnPc1d1	probuzen
v	v	k7c6	v
NREM	NREM	kA	NREM
fázi	fáze	k1gFnSc6	fáze
spánku	spánek	k1gInSc6	spánek
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
uvedli	uvést	k5eAaPmAgMnP	uvést
sny	sen	k1gInPc4	sen
jen	jen	k9	jen
ve	v	k7c4	v
20	[number]	k4	20
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
sny	sen	k1gInPc1	sen
v	v	k7c6	v
NREM	NREM	kA	NREM
fázi	fáze	k1gFnSc6	fáze
jsou	být	k5eAaImIp3nP	být
podstatně	podstatně	k6eAd1	podstatně
kratší	krátký	k2eAgInPc1d2	kratší
<g/>
,	,	kIx,	,
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
méně	málo	k6eAd2	málo
vizuálních	vizuální	k2eAgInPc2d1	vizuální
prvků	prvek	k1gInPc2	prvek
a	a	k8xC	a
zdaleka	zdaleka	k6eAd1	zdaleka
nejsou	být	k5eNaImIp3nP	být
tak	tak	k6eAd1	tak
živé	živý	k2eAgInPc1d1	živý
jako	jako	k8xC	jako
sny	sen	k1gInPc1	sen
v	v	k7c6	v
REM	REM	kA	REM
fázi	fáze	k1gFnSc6	fáze
<g/>
.	.	kIx.	.
</s>
<s>
Dobrovolníci	dobrovolník	k1gMnPc1	dobrovolník
uváděli	uvádět	k5eAaImAgMnP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
NREM	NREM	kA	NREM
fázi	fáze	k1gFnSc6	fáze
měli	mít	k5eAaImAgMnP	mít
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
spíše	spíše	k9	spíše
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
než	než	k8xS	než
sní	sníst	k5eAaPmIp3nS	sníst
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
zásadní	zásadní	k2eAgInSc1d1	zásadní
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
sny	sen	k1gInPc7	sen
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
fází	fáze	k1gFnPc2	fáze
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
člověk	člověk	k1gMnSc1	člověk
probuzen	probuzen	k2eAgMnSc1d1	probuzen
v	v	k7c6	v
REM	REM	kA	REM
fázi	fáze	k1gFnSc6	fáze
<g/>
,	,	kIx,	,
průměrně	průměrně	k6eAd1	průměrně
v	v	k7c6	v
88	[number]	k4	88
%	%	kIx~	%
potvrdí	potvrdit	k5eAaPmIp3nS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
právě	právě	k9	právě
zdál	zdát	k5eAaImAgInS	zdát
sen	sen	k1gInSc1	sen
<g/>
,	,	kIx,	,
a	a	k8xC	a
celkem	celkem	k6eAd1	celkem
bez	bez	k7c2	bez
problémů	problém	k1gInPc2	problém
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
sen	sen	k1gInSc1	sen
schopný	schopný	k2eAgInSc1d1	schopný
vyprávět	vyprávět	k5eAaImF	vyprávět
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
vysoké	vysoký	k2eAgNnSc1d1	vysoké
procento	procento	k1gNnSc1	procento
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
REM	REM	kA	REM
fáze	fáze	k1gFnSc2	fáze
se	se	k3xPyFc4	se
skutečně	skutečně	k6eAd1	skutečně
zdají	zdát	k5eAaImIp3nP	zdát
sny	sen	k1gInPc1	sen
<g/>
.	.	kIx.	.
</s>
<s>
Perioda	perioda	k1gFnSc1	perioda
REM	REM	kA	REM
spánku	spánek	k1gInSc2	spánek
se	se	k3xPyFc4	se
u	u	k7c2	u
zdravého	zdravý	k2eAgMnSc2d1	zdravý
člověka	člověk	k1gMnSc2	člověk
objevuje	objevovat	k5eAaImIp3nS	objevovat
nejméně	málo	k6eAd3	málo
třikrát	třikrát	k6eAd1	třikrát
za	za	k7c4	za
noc	noc	k1gFnSc4	noc
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
bychom	by	kYmCp1nP	by
měli	mít	k5eAaImAgMnP	mít
mít	mít	k5eAaImF	mít
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
nejméně	málo	k6eAd3	málo
jeden	jeden	k4xCgInSc1	jeden
tisíc	tisíc	k4xCgInSc1	tisíc
snů	sen	k1gInPc2	sen
<g/>
.	.	kIx.	.
</s>
<s>
Valnou	valný	k2eAgFnSc4d1	valná
část	část	k1gFnSc4	část
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
zapomeneme	zapomenout	k5eAaPmIp1nP	zapomenout
těsně	těsně	k6eAd1	těsně
po	po	k7c6	po
probuzení	probuzení	k1gNnSc6	probuzení
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ještě	ještě	k9	ještě
větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
si	se	k3xPyFc3	se
vůbec	vůbec	k9	vůbec
neuvědomíme	uvědomit	k5eNaPmIp1nP	uvědomit
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
zdají	zdát	k5eAaPmIp3nP	zdát
během	během	k7c2	během
souvislého	souvislý	k2eAgNnSc2d1	souvislé
spaní	spaní	k1gNnSc2	spaní
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
.	.	kIx.	.
</s>
<s>
Abychom	aby	kYmCp1nP	aby
měli	mít	k5eAaImAgMnP	mít
alespoň	alespoň	k9	alespoň
nějakou	nějaký	k3yIgFnSc4	nějaký
naději	naděje	k1gFnSc4	naděje
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
sen	sen	k1gInSc4	sen
zapamatujeme	zapamatovat	k5eAaPmIp1nP	zapamatovat
<g/>
,	,	kIx,	,
musíme	muset	k5eAaImIp1nP	muset
se	se	k3xPyFc4	se
ze	z	k7c2	z
spánku	spánek	k1gInSc2	spánek
probudit	probudit	k5eAaPmF	probudit
a	a	k8xC	a
chvíli	chvíle	k1gFnSc4	chvíle
zůstat	zůstat	k5eAaPmF	zůstat
vzhůru	vzhůru	k6eAd1	vzhůru
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
teprve	teprve	k6eAd1	teprve
bdělé	bdělý	k2eAgNnSc1d1	bdělé
vědomí	vědomí	k1gNnSc1	vědomí
může	moct	k5eAaImIp3nS	moct
uložit	uložit	k5eAaPmF	uložit
vzpomínku	vzpomínka	k1gFnSc4	vzpomínka
na	na	k7c4	na
sen	sen	k1gInSc4	sen
a	a	k8xC	a
uchovat	uchovat	k5eAaPmF	uchovat
paměťovou	paměťový	k2eAgFnSc4d1	paměťová
stopu	stopa	k1gFnSc4	stopa
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
právě	právě	k9	právě
proto	proto	k8xC	proto
si	se	k3xPyFc3	se
pamatujeme	pamatovat	k5eAaImIp1nP	pamatovat
nejčastěji	často	k6eAd3	často
ranní	ranní	k2eAgInPc4d1	ranní
sny	sen	k1gInPc4	sen
<g/>
.	.	kIx.	.
</s>
<s>
Vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
na	na	k7c4	na
sen	sen	k1gInSc4	sen
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
prchavá	prchavý	k2eAgFnSc1d1	prchavá
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
možná	možná	k9	možná
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
jejími	její	k3xOp3gFnPc7	její
nelogickými	logický	k2eNgFnPc7d1	nelogická
kvalitami	kvalita	k1gFnPc7	kvalita
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
znesnadňují	znesnadňovat	k5eAaImIp3nP	znesnadňovat
pamatování	pamatování	k1gNnSc4	pamatování
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
však	však	k9	však
sny	sen	k1gInPc1	sen
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
si	se	k3xPyFc3	se
pamatujeme	pamatovat	k5eAaImIp1nP	pamatovat
léta	léto	k1gNnPc4	léto
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
<g/>
,	,	kIx,	,
přinejmenším	přinejmenším	k6eAd1	přinejmenším
u	u	k7c2	u
vývojově	vývojově	k6eAd1	vývojově
pokročilých	pokročilý	k2eAgMnPc2d1	pokročilý
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
spánek	spánek	k1gInSc1	spánek
nezbytný	zbytný	k2eNgInSc1d1	zbytný
<g/>
,	,	kIx,	,
dosud	dosud	k6eAd1	dosud
nebylo	být	k5eNaImAgNnS	být
nalezeno	nalézt	k5eAaBmNgNnS	nalézt
jednoznačné	jednoznačný	k2eAgNnSc1d1	jednoznačné
vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
jeho	jeho	k3xOp3gFnSc2	jeho
nepostradatelnosti	nepostradatelnost	k1gFnSc2	nepostradatelnost
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
této	tento	k3xDgFnSc2	tento
neznámé	známý	k2eNgFnSc2d1	neznámá
klíčové	klíčový	k2eAgFnSc2d1	klíčová
funkce	funkce	k1gFnSc2	funkce
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
funkcí	funkce	k1gFnPc2	funkce
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
spánek	spánek	k1gInSc1	spánek
další	další	k2eAgInSc1d1	další
organizmu	organizmus	k1gInSc3	organizmus
prospěšné	prospěšný	k2eAgFnSc2d1	prospěšná
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
existuje	existovat	k5eAaImIp3nS	existovat
řada	řada	k1gFnSc1	řada
zjištění	zjištění	k1gNnPc2	zjištění
a	a	k8xC	a
domněnek	domněnka	k1gFnPc2	domněnka
vysvětlujících	vysvětlující	k2eAgFnPc2d1	vysvětlující
význam	význam	k1gInSc4	význam
spánku	spánek	k1gInSc2	spánek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neví	vědět	k5eNaImIp3nS	vědět
se	se	k3xPyFc4	se
jestli	jestli	k9	jestli
některá	některý	k3yIgFnSc1	některý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
možností	možnost	k1gFnPc2	možnost
je	být	k5eAaImIp3nS	být
onou	onen	k3xDgFnSc7	onen
nezbytnou	zbytný	k2eNgFnSc7d1	zbytný
funkcí	funkce	k1gFnSc7	funkce
a	a	k8xC	a
pokud	pokud	k8xS	pokud
ano	ano	k9	ano
která	který	k3yRgFnSc1	který
<g/>
.	.	kIx.	.
</s>
<s>
Spánek	spánek	k1gInSc1	spánek
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
nejen	nejen	k6eAd1	nejen
u	u	k7c2	u
savců	savec	k1gMnPc2	savec
a	a	k8xC	a
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
(	(	kIx(	(
<g/>
v	v	k7c6	v
jednodušší	jednoduchý	k2eAgFnSc6d2	jednodušší
podobě	podoba	k1gFnSc6	podoba
<g/>
)	)	kIx)	)
u	u	k7c2	u
nižších	nízký	k2eAgFnPc2d2	nižší
forem	forma	k1gFnPc2	forma
živočichů	živočich	k1gMnPc2	živočich
např.	např.	kA	např.
plazů	plaz	k1gMnPc2	plaz
<g/>
,	,	kIx,	,
ryb	ryba	k1gFnPc2	ryba
nebo	nebo	k8xC	nebo
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejdiskutovanější	diskutovaný	k2eAgFnPc4d3	Nejdiskutovanější
hypotézy	hypotéza	k1gFnPc4	hypotéza
patří	patřit	k5eAaImIp3nP	patřit
<g/>
:	:	kIx,	:
Odvádění	odvádění	k1gNnSc1	odvádění
metabolitů	metabolit	k1gInPc2	metabolit
nervových	nervový	k2eAgFnPc2d1	nervová
buněk	buňka	k1gFnPc2	buňka
Hromadění	hromadění	k1gNnSc2	hromadění
odpadních	odpadní	k2eAgInPc2d1	odpadní
produktů	produkt	k1gInPc2	produkt
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
jeho	jeho	k3xOp3gFnSc2	jeho
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Spánek	spánek	k1gInSc1	spánek
pak	pak	k6eAd1	pak
v	v	k7c6	v
klidovém	klidový	k2eAgInSc6d1	klidový
stavu	stav	k1gInSc6	stav
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
jeho	on	k3xPp3gNnSc2	on
čištění	čištění	k1gNnSc2	čištění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mechanismu	mechanismus	k1gInSc6	mechanismus
může	moct	k5eAaImIp3nS	moct
hrát	hrát	k5eAaImF	hrát
roli	role	k1gFnSc4	role
hematoencefalická	hematoencefalický	k2eAgFnSc1d1	hematoencefalická
bariéra	bariéra	k1gFnSc1	bariéra
a	a	k8xC	a
glymfatický	glymfatický	k2eAgInSc1d1	glymfatický
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
spánku	spánek	k1gInSc2	spánek
pro	pro	k7c4	pro
kognitivní	kognitivní	k2eAgFnPc4d1	kognitivní
funkce	funkce	k1gFnPc4	funkce
Fyziologické	fyziologický	k2eAgFnSc2d1	fyziologická
funkce	funkce	k1gFnSc2	funkce
člověka	člověk	k1gMnSc2	člověk
nejsou	být	k5eNaImIp3nP	být
narušeny	narušen	k2eAgFnPc1d1	narušena
ani	ani	k8xC	ani
po	po	k7c6	po
několika	několik	k4yIc6	několik
dnech	den	k1gInPc6	den
spánkové	spánkový	k2eAgFnPc1d1	spánková
deprivace	deprivace	k1gFnPc1	deprivace
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
evidentní	evidentní	k2eAgNnSc1d1	evidentní
je	být	k5eAaImIp3nS	být
snížení	snížení	k1gNnSc1	snížení
intelektového	intelektový	k2eAgInSc2d1	intelektový
výkonu	výkon	k1gInSc2	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Spánek	spánek	k1gInSc1	spánek
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
potřebný	potřebný	k2eAgInSc4d1	potřebný
pro	pro	k7c4	pro
fungování	fungování	k1gNnSc4	fungování
psychických	psychický	k2eAgFnPc2d1	psychická
funkcí	funkce	k1gFnPc2	funkce
(	(	kIx(	(
<g/>
myšlení	myšlení	k1gNnSc1	myšlení
<g/>
,	,	kIx,	,
rozhodování	rozhodování	k1gNnSc1	rozhodování
<g/>
,	,	kIx,	,
paměť	paměť	k1gFnSc1	paměť
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
např.	např.	kA	např.
i	i	k9	i
zrak	zrak	k1gInSc4	zrak
<g/>
)	)	kIx)	)
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
odstraňuje	odstraňovat	k5eAaImIp3nS	odstraňovat
jejich	jejich	k3xOp3gFnSc4	jejich
únavu	únava	k1gFnSc4	únava
vyvolanou	vyvolaný	k2eAgFnSc4d1	vyvolaná
bdělým	bdělý	k2eAgInSc7d1	bdělý
stavem	stav	k1gInSc7	stav
<g/>
.	.	kIx.	.
</s>
<s>
Konsolidace	konsolidace	k1gFnSc1	konsolidace
paměti	paměť	k1gFnSc2	paměť
Je	být	k5eAaImIp3nS	být
prokázán	prokázat	k5eAaPmNgInS	prokázat
zásadní	zásadní	k2eAgInSc1d1	zásadní
význam	význam	k1gInSc1	význam
spánku	spánek	k1gInSc2	spánek
pro	pro	k7c4	pro
učení	učení	k1gNnSc4	učení
a	a	k8xC	a
paměť	paměť	k1gFnSc4	paměť
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
ale	ale	k8xC	ale
spánek	spánek	k1gInSc1	spánek
není	být	k5eNaImIp3nS	být
pro	pro	k7c4	pro
učení	učení	k1gNnSc4	učení
naprosto	naprosto	k6eAd1	naprosto
nezbytný	zbytný	k2eNgInSc1d1	zbytný
<g/>
.	.	kIx.	.
</s>
<s>
Úloha	úloha	k1gFnSc1	úloha
spánku	spánek	k1gInSc2	spánek
při	při	k7c6	při
zrání	zrání	k1gNnSc6	zrání
nervového	nervový	k2eAgInSc2d1	nervový
systému	systém	k1gInSc2	systém
Je	být	k5eAaImIp3nS	být
i	i	k9	i
častá	častý	k2eAgFnSc1d1	častá
představa	představa	k1gFnSc1	představa
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
spánek	spánek	k1gInSc1	spánek
REM	REM	kA	REM
hraje	hrát	k5eAaImIp3nS	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
při	při	k7c6	při
zrání	zrání	k1gNnSc6	zrání
nervového	nervový	k2eAgInSc2d1	nervový
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
pohledu	pohled	k1gInSc2	pohled
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
spánek	spánek	k1gInSc1	spánek
REM	REM	kA	REM
uchovává	uchovávat	k5eAaImIp3nS	uchovávat
i	i	k9	i
u	u	k7c2	u
dospělých	dospělý	k2eAgMnPc2d1	dospělý
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
intenzita	intenzita	k1gFnSc1	intenzita
regenerace	regenerace	k1gFnSc2	regenerace
organizmu	organizmus	k1gInSc2	organizmus
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
spánku	spánek	k1gInSc2	spánek
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
úroveň	úroveň	k1gFnSc1	úroveň
anabolismu	anabolismus	k1gInSc2	anabolismus
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
posílení	posílení	k1gNnSc3	posílení
růstových	růstový	k2eAgInPc2d1	růstový
a	a	k8xC	a
ozdravných	ozdravný	k2eAgInPc2d1	ozdravný
procesů	proces	k1gInPc2	proces
tělesných	tělesný	k2eAgInPc2d1	tělesný
systémů	systém	k1gInPc2	systém
(	(	kIx(	(
<g/>
např.	např.	kA	např.
imunitního	imunitní	k2eAgInSc2d1	imunitní
nebo	nebo	k8xC	nebo
svalového	svalový	k2eAgInSc2d1	svalový
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
je	být	k5eAaImIp3nS	být
podpořeno	podpořen	k2eAgNnSc4d1	podpořeno
hojení	hojení	k1gNnSc4	hojení
ran	rána	k1gFnPc2	rána
<g/>
.	.	kIx.	.
</s>
<s>
Šetření	šetření	k1gNnSc1	šetření
energií	energie	k1gFnSc7	energie
Představa	představa	k1gFnSc1	představa
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
spánek	spánek	k1gInSc1	spánek
konzervuje	konzervovat	k5eAaBmIp3nS	konzervovat
energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
podpořena	podpořit	k5eAaPmNgFnS	podpořit
několika	několik	k4yIc7	několik
studiemi	studie	k1gFnPc7	studie
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
prokázaly	prokázat	k5eAaPmAgFnP	prokázat
zvýšený	zvýšený	k2eAgInSc4d1	zvýšený
příjem	příjem	k1gInSc4	příjem
jídla	jídlo	k1gNnSc2	jídlo
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
spánkové	spánkový	k2eAgFnSc2d1	spánková
deprivace	deprivace	k1gFnSc2	deprivace
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
metabolizmus	metabolizmus	k1gInSc1	metabolizmus
ve	v	k7c6	v
spánku	spánek	k1gInSc6	spánek
je	být	k5eAaImIp3nS	být
snížen	snížit	k5eAaPmNgInS	snížit
oproti	oproti	k7c3	oproti
bdění	bdění	k1gNnSc3	bdění
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
15	[number]	k4	15
%	%	kIx~	%
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
ztráta	ztráta	k1gFnSc1	ztráta
energie	energie	k1gFnSc2	energie
po	po	k7c6	po
probdělé	probdělý	k2eAgFnSc6d1	probdělá
noci	noc	k1gFnSc6	noc
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
jen	jen	k9	jen
malou	malý	k2eAgFnSc7d1	malá
trochou	trocha	k1gFnSc7	trocha
jídla	jídlo	k1gNnSc2	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Domněnce	domněnka	k1gFnSc3	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
spánek	spánek	k1gInSc1	spánek
je	být	k5eAaImIp3nS	být
cenný	cenný	k2eAgInSc1d1	cenný
pro	pro	k7c4	pro
úsporu	úspora	k1gFnSc4	úspora
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
odpovídal	odpovídat	k5eAaImAgMnS	odpovídat
sklon	sklon	k1gInSc4	sklon
ke	k	k7c3	k
dlouhému	dlouhý	k2eAgInSc3d1	dlouhý
spánku	spánek	k1gInSc3	spánek
malých	malý	k2eAgMnPc2d1	malý
savců	savec	k1gMnPc2	savec
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
spotřebou	spotřeba	k1gFnSc7	spotřeba
energie	energie	k1gFnSc2	energie
(	(	kIx(	(
<g/>
nezbytné	nezbytný	k2eAgNnSc1d1	nezbytný
pro	pro	k7c4	pro
termoregulaci	termoregulace	k1gFnSc4	termoregulace
<g/>
)	)	kIx)	)
ale	ale	k8xC	ale
s	s	k7c7	s
omezenými	omezený	k2eAgInPc7d1	omezený
zdroji	zdroj	k1gInPc7	zdroj
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
ní	on	k3xPp3gFnSc3	on
svědčí	svědčit	k5eAaImIp3nS	svědčit
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
hibernující	hibernující	k2eAgInSc1d1	hibernující
(	(	kIx(	(
<g/>
zimní	zimní	k2eAgInSc1d1	zimní
spánek	spánek	k1gInSc1	spánek
<g/>
)	)	kIx)	)
savci	savec	k1gMnPc1	savec
musí	muset	k5eAaImIp3nP	muset
několikrát	několikrát	k6eAd1	několikrát
hibernaci	hibernace	k1gFnSc4	hibernace
přerušit	přerušit	k5eAaPmF	přerušit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
vyspat	vyspat	k5eAaPmF	vyspat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
obnáší	obnášet	k5eAaImIp3nS	obnášet
zahřátí	zahřátí	k1gNnSc1	zahřátí
na	na	k7c4	na
normální	normální	k2eAgFnSc4d1	normální
tělesnou	tělesný	k2eAgFnSc4d1	tělesná
teplotu	teplota	k1gFnSc4	teplota
a	a	k8xC	a
tedy	tedy	k9	tedy
ztrátu	ztráta	k1gFnSc4	ztráta
energie	energie	k1gFnSc2	energie
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
nepřerušovanou	přerušovaný	k2eNgFnSc7d1	nepřerušovaná
hibernací	hibernace	k1gFnSc7	hibernace
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
různých	různý	k2eAgFnPc2d1	různá
teorií	teorie	k1gFnPc2	teorie
napovídá	napovídat	k5eAaBmIp3nS	napovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
spánek	spánek	k1gInSc1	spánek
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
mít	mít	k5eAaImF	mít
buď	buď	k8xC	buď
mnoho	mnoho	k4c1	mnoho
funkcí	funkce	k1gFnPc2	funkce
(	(	kIx(	(
<g/>
formulace	formulace	k1gFnPc1	formulace
některých	některý	k3yIgFnPc2	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
byly	být	k5eAaImAgInP	být
výše	vysoce	k6eAd2	vysoce
uvedeny	uveden	k2eAgInPc1d1	uveden
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
že	že	k8xS	že
slouží	sloužit	k5eAaImIp3nS	sloužit
jediné	jediný	k2eAgFnSc3d1	jediná
společné	společný	k2eAgFnSc3d1	společná
funkci	funkce	k1gFnSc3	funkce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dosud	dosud	k6eAd1	dosud
nebyla	být	k5eNaImAgFnS	být
identifikována	identifikován	k2eAgFnSc1d1	identifikována
nebo	nebo	k8xC	nebo
všeobecně	všeobecně	k6eAd1	všeobecně
akceptována	akceptován	k2eAgFnSc1d1	akceptována
<g/>
.	.	kIx.	.
</s>
<s>
Možná	možná	k9	možná
dosud	dosud	k6eAd1	dosud
neznámé	známý	k2eNgFnSc3d1	neznámá
buněčné	buněčný	k2eAgFnSc3d1	buněčná
funkci	funkce	k1gFnSc3	funkce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
u	u	k7c2	u
mladých	mladý	k2eAgMnPc2d1	mladý
jedinců	jedinec	k1gMnPc2	jedinec
podporuje	podporovat	k5eAaImIp3nS	podporovat
proces	proces	k1gInSc1	proces
zrání	zrání	k1gNnSc2	zrání
<g/>
,	,	kIx,	,
u	u	k7c2	u
malých	malý	k2eAgMnPc2d1	malý
živočichů	živočich	k1gMnPc2	živočich
regulaci	regulace	k1gFnSc4	regulace
teploty	teplota	k1gFnSc2	teplota
nebo	nebo	k8xC	nebo
u	u	k7c2	u
dospělého	dospělý	k2eAgMnSc2d1	dospělý
člověka	člověk	k1gMnSc2	člověk
vyšší	vysoký	k2eAgFnSc4d2	vyšší
duševní	duševní	k2eAgFnSc4d1	duševní
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Spánková	spánkový	k2eAgFnSc1d1	spánková
deprivace	deprivace	k1gFnSc1	deprivace
<g/>
.	.	kIx.	.
</s>
<s>
Spánková	spánkový	k2eAgFnSc1d1	spánková
deprivace	deprivace	k1gFnSc1	deprivace
je	být	k5eAaImIp3nS	být
stav	stav	k1gInSc1	stav
způsobený	způsobený	k2eAgInSc1d1	způsobený
nedostatkem	nedostatek	k1gInSc7	nedostatek
spánku	spánek	k1gInSc2	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jiným	jiné	k1gNnSc7	jiné
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
ospalost	ospalost	k1gFnSc1	ospalost
<g/>
,	,	kIx,	,
zhoršení	zhoršení	k1gNnSc1	zhoršení
soustředění	soustředění	k1gNnSc2	soustředění
a	a	k8xC	a
úsudku	úsudek	k1gInSc2	úsudek
<g/>
,	,	kIx,	,
v	v	k7c6	v
těžkých	těžký	k2eAgInPc6d1	těžký
případech	případ	k1gInPc6	případ
také	také	k9	také
např.	např.	kA	např.
halucinace	halucinace	k1gFnSc1	halucinace
nebo	nebo	k8xC	nebo
nesouvislé	souvislý	k2eNgNnSc1d1	nesouvislé
myšlení	myšlení	k1gNnSc1	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Syndrom	syndrom	k1gInSc1	syndrom
odkládané	odkládaný	k2eAgFnSc2d1	odkládaná
spánkové	spánkový	k2eAgFnSc2d1	spánková
fáze	fáze	k1gFnSc2	fáze
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
existují	existovat	k5eAaImIp3nP	existovat
rozdíly	rozdíl	k1gInPc1	rozdíl
v	v	k7c6	v
úrovni	úroveň	k1gFnSc6	úroveň
ranní	ranní	k2eAgFnSc2d1	ranní
aktivity	aktivita	k1gFnSc2	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
aktivní	aktivní	k2eAgMnSc1d1	aktivní
po	po	k7c6	po
ránu	ráno	k1gNnSc6	ráno
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
ranní	ranní	k2eAgNnSc1d1	ranní
ptáče	ptáče	k1gNnSc1	ptáče
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
aktivita	aktivita	k1gFnSc1	aktivita
vzrůstá	vzrůstat	k5eAaImIp3nS	vzrůstat
odpoledne	odpoledne	k1gNnSc4	odpoledne
a	a	k8xC	a
večer	večer	k1gInSc4	večer
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
noční	noční	k2eAgMnSc1d1	noční
pták	pták	k1gMnSc1	pták
(	(	kIx(	(
<g/>
též	též	k9	též
sova	sova	k1gFnSc1	sova
<g/>
,	,	kIx,	,
z	z	k7c2	z
angl.	angl.	k?	angl.
owl	owl	k?	owl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
rozdíly	rozdíl	k1gInPc1	rozdíl
nejsou	být	k5eNaImIp3nP	být
pouze	pouze	k6eAd1	pouze
subjektivní	subjektivní	k2eAgInPc1d1	subjektivní
<g/>
.	.	kIx.	.
</s>
<s>
Srovnávací	srovnávací	k2eAgFnPc1d1	srovnávací
studie	studie	k1gFnPc1	studie
těchto	tento	k3xDgInPc2	tento
dvou	dva	k4xCgInPc2	dva
typů	typ	k1gInPc2	typ
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
v	v	k7c6	v
několika	několik	k4yIc6	několik
důležitých	důležitý	k2eAgInPc6d1	důležitý
momentech	moment	k1gInPc6	moment
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
ranní	ranní	k2eAgNnPc1d1	ranní
ptáčata	ptáče	k1gNnPc1	ptáče
mají	mít	k5eAaImIp3nP	mít
větší	veliký	k2eAgFnSc4d2	veliký
produkci	produkce	k1gFnSc4	produkce
adrenalinu	adrenalin	k1gInSc2	adrenalin
než	než	k8xS	než
noční	noční	k2eAgMnPc1d1	noční
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
projevuje	projevovat	k5eAaImIp3nS	projevovat
celkovou	celkový	k2eAgFnSc7d1	celková
aktivační	aktivační	k2eAgFnSc7d1	aktivační
úrovní	úroveň	k1gFnSc7	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholu	vrchol	k1gInSc2	vrchol
své	svůj	k3xOyFgFnSc2	svůj
tělesné	tělesný	k2eAgFnSc2d1	tělesná
teploty	teplota	k1gFnSc2	teplota
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
během	během	k7c2	během
dne	den	k1gInSc2	den
o	o	k7c4	o
něco	něco	k3yInSc4	něco
dřív	dříve	k6eAd2	dříve
než	než	k8xS	než
noční	noční	k2eAgMnPc1d1	noční
ptáci	pták	k1gMnPc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
rozdíly	rozdíl	k1gInPc1	rozdíl
v	v	k7c6	v
aktivitě	aktivita	k1gFnSc6	aktivita
a	a	k8xC	a
tělesných	tělesný	k2eAgInPc6d1	tělesný
stavech	stav	k1gInPc6	stav
mohou	moct	k5eAaImIp3nP	moct
výrazně	výrazně	k6eAd1	výrazně
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
chování	chování	k1gNnSc4	chování
i	i	k8xC	i
výkonnost	výkonnost	k1gFnSc4	výkonnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ranních	ranní	k2eAgFnPc6d1	ranní
hodinách	hodina	k1gFnPc6	hodina
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
tělesná	tělesný	k2eAgFnSc1d1	tělesná
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
produkce	produkce	k1gFnSc1	produkce
hormonů	hormon	k1gInPc2	hormon
a	a	k8xC	a
celková	celkový	k2eAgFnSc1d1	celková
aktivita	aktivita	k1gFnSc1	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
dopolední	dopolední	k2eAgInSc1d1	dopolední
spánek	spánek	k1gInSc1	spánek
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
útlumu	útlum	k1gInSc3	útlum
metabolických	metabolický	k2eAgInPc2d1	metabolický
procesů	proces	k1gInPc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
probuzení	probuzení	k1gNnSc6	probuzení
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
celý	celý	k2eAgInSc1d1	celý
zbytek	zbytek	k1gInSc1	zbytek
dne	den	k1gInSc2	den
utlumený	utlumený	k2eAgMnSc1d1	utlumený
a	a	k8xC	a
omámený	omámený	k2eAgMnSc1d1	omámený
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
spánku	spánek	k1gInSc2	spánek
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Loomis	Loomis	k1gFnPc1	Loomis
<g/>
,	,	kIx,	,
Harvey	Harvea	k1gFnPc1	Harvea
<g/>
,	,	kIx,	,
Hobart	Hobart	k1gInSc1	Hobart
v	v	k7c6	v
r.	r.	kA	r.
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
nalezli	nalézt	k5eAaBmAgMnP	nalézt
citlivé	citlivý	k2eAgFnPc4d1	citlivá
techniky	technika	k1gFnPc4	technika
pro	pro	k7c4	pro
měření	měření	k1gNnSc4	měření
hloubky	hloubka	k1gFnSc2	hloubka
spánku	spánek	k1gInSc2	spánek
a	a	k8xC	a
pro	pro	k7c4	pro
stanovení	stanovení	k1gNnSc4	stanovení
stadií	stadion	k1gNnPc2	stadion
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
sny	sen	k1gInPc1	sen
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
spánku	spánek	k1gInSc2	spánek
pracuje	pracovat	k5eAaImIp3nS	pracovat
s	s	k7c7	s
přístroji	přístroj	k1gInPc7	přístroj
zaznamenávajícími	zaznamenávající	k2eAgInPc7d1	zaznamenávající
elektrické	elektrický	k2eAgFnSc2d1	elektrická
změny	změna	k1gFnPc1	změna
na	na	k7c6	na
pokožce	pokožka	k1gFnSc6	pokožka
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
souvisejí	souviset	k5eAaImIp3nP	souviset
se	s	k7c7	s
spontánní	spontánní	k2eAgFnSc7d1	spontánní
mozkovou	mozkový	k2eAgFnSc7d1	mozková
aktivitou	aktivita	k1gFnSc7	aktivita
během	během	k7c2	během
spánku	spánek	k1gInSc2	spánek
a	a	k8xC	a
s	s	k7c7	s
očními	oční	k2eAgInPc7d1	oční
pohyby	pohyb	k1gInPc7	pohyb
při	při	k7c6	při
snění	snění	k1gNnSc6	snění
<g/>
.	.	kIx.	.
</s>
<s>
Grafický	grafický	k2eAgInSc1d1	grafický
záznam	záznam	k1gInSc1	záznam
těchto	tento	k3xDgFnPc2	tento
elektrických	elektrický	k2eAgFnPc2d1	elektrická
změn	změna	k1gFnPc2	změna
čili	čili	k8xC	čili
mozkových	mozkový	k2eAgFnPc2d1	mozková
vln	vlna	k1gFnPc2	vlna
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
elektroencefalogram	elektroencefalogram	k1gInSc1	elektroencefalogram
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
EEG	EEG	kA	EEG
<g/>
.	.	kIx.	.
</s>
<s>
Měří	měřit	k5eAaImIp3nS	měřit
se	se	k3xPyFc4	se
měnící	měnící	k2eAgMnSc1d1	měnící
se	se	k3xPyFc4	se
průměrný	průměrný	k2eAgInSc1d1	průměrný
elektrický	elektrický	k2eAgInSc1d1	elektrický
potenciál	potenciál	k1gInSc1	potenciál
tisíců	tisíc	k4xCgInPc2	tisíc
neuronů	neuron	k1gInPc2	neuron
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
mozkové	mozkový	k2eAgFnSc2d1	mozková
kůry	kůra	k1gFnSc2	kůra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdější	pozdní	k2eAgFnSc6d2	pozdější
době	doba	k1gFnSc6	doba
dospěli	dochvít	k5eAaPmAgMnP	dochvít
Dale	Dale	k1gNnSc4	Dale
Edgar	Edgar	k1gMnSc1	Edgar
a	a	k8xC	a
Wiliam	Wiliam	k1gInSc1	Wiliam
Dement	Dement	k1gInSc1	Dement
<g/>
,	,	kIx,	,
přední	přední	k2eAgMnPc1d1	přední
odborníci	odborník	k1gMnPc1	odborník
na	na	k7c4	na
výzkum	výzkum	k1gInSc4	výzkum
spánku	spánek	k1gInSc2	spánek
<g/>
,	,	kIx,	,
k	k	k7c3	k
modelu	model	k1gInSc3	model
protikladného	protikladný	k2eAgInSc2d1	protikladný
procesu	proces	k1gInSc2	proces
spánku	spánek	k1gInSc2	spánek
a	a	k8xC	a
bdění	bdění	k1gNnSc2	bdění
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
se	se	k3xPyFc4	se
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
dva	dva	k4xCgInPc1	dva
protikladné	protikladný	k2eAgInPc1d1	protikladný
procesy	proces	k1gInPc1	proces
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
pud	pud	k1gInSc1	pud
homeostatického	homeostatický	k2eAgInSc2d1	homeostatický
spánku	spánek	k1gInSc2	spánek
(	(	kIx(	(
<g/>
fyziologický	fyziologický	k2eAgInSc1d1	fyziologický
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
účelem	účel	k1gInSc7	účel
je	být	k5eAaImIp3nS	být
zajistit	zajistit	k5eAaPmF	zajistit
takové	takový	k3xDgNnSc4	takový
množství	množství	k1gNnSc4	množství
spánku	spánek	k1gInSc2	spánek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
bdělý	bdělý	k2eAgInSc1d1	bdělý
stav	stav	k1gInSc1	stav
přes	přes	k7c4	přes
den	den	k1gInSc4	den
stálý	stálý	k2eAgInSc4d1	stálý
<g/>
)	)	kIx)	)
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
proces	proces	k1gInSc1	proces
bdění	bdění	k1gNnSc2	bdění
řízený	řízený	k2eAgInSc1d1	řízený
časem	časem	k6eAd1	časem
(	(	kIx(	(
<g/>
podléhá	podléhat	k5eAaImIp3nS	podléhat
kontrole	kontrola	k1gFnSc3	kontrola
biologických	biologický	k2eAgFnPc2d1	biologická
hodin	hodina	k1gFnPc2	hodina
sestávajících	sestávající	k2eAgFnPc2d1	sestávající
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
drobných	drobný	k2eAgFnPc2d1	drobná
neurálních	neurální	k2eAgFnPc2d1	neurální
struktur	struktura	k1gFnPc2	struktura
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
mozku	mozek	k1gInSc2	mozek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Biologické	biologický	k2eAgFnPc1d1	biologická
hodiny	hodina	k1gFnPc1	hodina
řídí	řídit	k5eAaImIp3nP	řídit
řadu	řada	k1gFnSc4	řada
duševních	duševní	k2eAgFnPc2d1	duševní
a	a	k8xC	a
fyziologických	fyziologický	k2eAgFnPc2d1	fyziologická
změn	změna	k1gFnPc2	změna
včetně	včetně	k7c2	včetně
rytmu	rytmus	k1gInSc2	rytmus
bdění	bdění	k1gNnSc2	bdění
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
cirkadiánního	cirkadiánní	k2eAgInSc2d1	cirkadiánní
rytmu	rytmus	k1gInSc2	rytmus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
opakuje	opakovat	k5eAaImIp3nS	opakovat
přibližně	přibližně	k6eAd1	přibližně
každých	každý	k3xTgFnPc2	každý
čtyřiadvacet	čtyřiadvacet	k4xCc4	čtyřiadvacet
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Biologické	biologický	k2eAgFnPc1d1	biologická
hodiny	hodina	k1gFnPc1	hodina
podléhají	podléhat	k5eAaImIp3nP	podléhat
vlivu	vliv	k1gInSc2	vliv
působení	působení	k1gNnSc1	působení
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Denní	denní	k2eAgNnSc1d1	denní
světlo	světlo	k1gNnSc1	světlo
totiž	totiž	k9	totiž
zastavuje	zastavovat	k5eAaImIp3nS	zastavovat
sekreci	sekrece	k1gFnSc4	sekrece
hormonu	hormon	k1gInSc2	hormon
melatoninu	melatonin	k1gInSc2	melatonin
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
hormon	hormon	k1gInSc1	hormon
navozující	navozující	k2eAgInSc1d1	navozující
spánek	spánek	k1gInSc1	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Poruchami	porucha	k1gFnPc7	porucha
spánku	spánek	k1gInSc2	spánek
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
hypnopatologie	hypnopatologie	k1gFnSc1	hypnopatologie
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Pásmová	pásmový	k2eAgFnSc1d1	pásmová
nemoc	nemoc	k1gFnSc1	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Posun	posun	k1gInSc4	posun
normálních	normální	k2eAgInPc2d1	normální
tělesných	tělesný	k2eAgInPc2d1	tělesný
rytmů	rytmus	k1gInPc2	rytmus
způsobený	způsobený	k2eAgInSc1d1	způsobený
lety	léto	k1gNnPc7	léto
na	na	k7c6	na
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yIgFnPc6	který
jsou	být	k5eAaImIp3nP	být
překonávána	překonáván	k2eAgNnPc4d1	překonáváno
časová	časový	k2eAgNnPc4d1	časové
pásma	pásmo	k1gNnPc4	pásmo
<g/>
.	.	kIx.	.
</s>
<s>
Normální	normální	k2eAgInSc1d1	normální
spánkový	spánkový	k2eAgInSc1d1	spánkový
režim	režim	k1gInSc1	režim
je	být	k5eAaImIp3nS	být
narušen	narušen	k2eAgMnSc1d1	narušen
<g/>
,	,	kIx,	,
postižený	postižený	k2eAgMnSc1d1	postižený
spí	spát	k5eAaImIp3nS	spát
přes	přes	k7c4	přes
den	den	k1gInSc4	den
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
čilý	čilý	k2eAgInSc1d1	čilý
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
příznaky	příznak	k1gInPc4	příznak
patří	patřit	k5eAaImIp3nS	patřit
únava	únava	k1gFnSc1	únava
a	a	k8xC	a
snížení	snížení	k1gNnSc1	snížení
duševních	duševní	k2eAgFnPc2d1	duševní
schopností	schopnost	k1gFnPc2	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Přizpůsobení	přizpůsobení	k1gNnSc2	přizpůsobení
novému	nový	k2eAgInSc3d1	nový
rytmu	rytmus	k1gInSc3	rytmus
trvá	trvat	k5eAaImIp3nS	trvat
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnné	souhrnný	k2eAgNnSc1d1	souhrnné
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
poruchy	porucha	k1gFnPc4	porucha
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
týkají	týkat	k5eAaImIp3nP	týkat
množství	množství	k1gNnPc4	množství
<g/>
,	,	kIx,	,
kvality	kvalita	k1gFnPc4	kvalita
nebo	nebo	k8xC	nebo
časování	časování	k1gNnSc4	časování
spánku	spánek	k1gInSc2	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Insomnie	insomnie	k1gFnSc2	insomnie
<g/>
.	.	kIx.	.
</s>
<s>
Nespavost	nespavost	k1gFnSc1	nespavost
(	(	kIx(	(
<g/>
insomnie	insomnie	k1gFnSc1	insomnie
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
velmi	velmi	k6eAd1	velmi
častým	častý	k2eAgInPc3d1	častý
zdravotním	zdravotní	k2eAgInPc3d1	zdravotní
problémům	problém	k1gInPc3	problém
současné	současný	k2eAgFnSc2d1	současná
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedinec	jedinec	k1gMnSc1	jedinec
nemůže	moct	k5eNaImIp3nS	moct
usnout	usnout	k5eAaPmF	usnout
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
ve	v	k7c6	v
spánku	spánek	k1gInSc6	spánek
často	často	k6eAd1	často
probouzí	probouzet	k5eAaImIp3nS	probouzet
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
příčinou	příčina	k1gFnSc7	příčina
je	být	k5eAaImIp3nS	být
stres	stres	k1gInSc1	stres
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
nedostatek	nedostatek	k1gInSc1	nedostatek
pohybu	pohyb	k1gInSc2	pohyb
a	a	k8xC	a
nesprávné	správný	k2eNgNnSc1d1	nesprávné
užívání	užívání	k1gNnSc1	užívání
léků	lék	k1gInPc2	lék
<g/>
.	.	kIx.	.
</s>
<s>
Nespavost	nespavost	k1gFnSc1	nespavost
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
doprovodným	doprovodný	k2eAgInSc7d1	doprovodný
symptomem	symptom	k1gInSc7	symptom
tělesných	tělesný	k2eAgInPc2d1	tělesný
nebo	nebo	k8xC	nebo
duševních	duševní	k2eAgFnPc2d1	duševní
poruch	porucha	k1gFnPc2	porucha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
můžeme	moct	k5eAaImIp1nP	moct
hovořit	hovořit	k5eAaImF	hovořit
o	o	k7c6	o
nespavosti	nespavost	k1gFnSc6	nespavost
jako	jako	k8xC	jako
o	o	k7c6	o
primárním	primární	k2eAgNnSc6d1	primární
onemocnění	onemocnění	k1gNnSc6	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Hypersomnie	hypersomnie	k1gFnSc2	hypersomnie
<g/>
.	.	kIx.	.
</s>
<s>
Hypersomnie	hypersomnie	k1gFnSc1	hypersomnie
(	(	kIx(	(
<g/>
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
spavost	spavost	k1gFnSc1	spavost
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
patologicky	patologicky	k6eAd1	patologicky
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
spavost	spavost	k1gFnSc1	spavost
<g/>
.	.	kIx.	.
</s>
<s>
Jedinec	jedinec	k1gMnSc1	jedinec
spí	spát	k5eAaImIp3nS	spát
denně	denně	k6eAd1	denně
12	[number]	k4	12
hodin	hodina	k1gFnPc2	hodina
i	i	k8xC	i
více	hodně	k6eAd2	hodně
a	a	k8xC	a
často	často	k6eAd1	často
upadá	upadat	k5eAaImIp3nS	upadat
do	do	k7c2	do
spánku	spánek	k1gInSc2	spánek
i	i	k8xC	i
během	během	k7c2	během
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
typů	typ	k1gInPc2	typ
hypersomnie	hypersomnie	k1gFnSc2	hypersomnie
je	být	k5eAaImIp3nS	být
narkolepsie	narkolepsie	k1gFnSc1	narkolepsie
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
postižený	postižený	k2eAgMnSc1d1	postižený
usíná	usínat	k5eAaImIp3nS	usínat
i	i	k9	i
uprostřed	uprostřed	k7c2	uprostřed
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Parasomnie	Parasomnie	k1gFnSc2	Parasomnie
<g/>
.	.	kIx.	.
</s>
<s>
Parasomnie	Parasomnie	k1gFnSc1	Parasomnie
je	být	k5eAaImIp3nS	být
souhrnné	souhrnný	k2eAgNnSc4d1	souhrnné
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
nepřirozené	přirozený	k2eNgInPc4d1	nepřirozený
stavy	stav	k1gInPc4	stav
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
doprovázejí	doprovázet	k5eAaImIp3nP	doprovázet
spánek	spánek	k1gInSc4	spánek
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
vázány	vázán	k2eAgInPc1d1	vázán
–	–	k?	–
objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
průběhu	průběh	k1gInSc6	průběh
nebo	nebo	k8xC	nebo
při	při	k7c6	při
probuzení	probuzení	k1gNnSc6	probuzení
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Noční	noční	k2eAgFnSc1d1	noční
můra	můra	k1gFnSc1	můra
<g/>
.	.	kIx.	.
</s>
<s>
Noční	noční	k2eAgFnPc1d1	noční
můry	můra	k1gFnPc1	můra
jsou	být	k5eAaImIp3nP	být
děsivé	děsivý	k2eAgInPc4d1	děsivý
<g/>
,	,	kIx,	,
živé	živý	k2eAgInPc4d1	živý
sny	sen	k1gInPc4	sen
během	během	k7c2	během
REM	REM	kA	REM
spánku	spánek	k1gInSc2	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
probuzení	probuzení	k1gNnSc6	probuzení
si	se	k3xPyFc3	se
postižený	postižený	k1gMnSc1	postižený
noční	noční	k2eAgFnSc4d1	noční
můru	můra	k1gFnSc4	můra
jasně	jasně	k6eAd1	jasně
pamatuje	pamatovat	k5eAaImIp3nS	pamatovat
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
prožili	prožít	k5eAaPmAgMnP	prožít
nějaké	nějaký	k3yIgNnSc4	nějaký
trauma	trauma	k1gNnSc4	trauma
<g/>
.	.	kIx.	.
</s>
<s>
Závažným	závažný	k2eAgInSc7d1	závažný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poměrně	poměrně	k6eAd1	poměrně
vzácným	vzácný	k2eAgInSc7d1	vzácný
typem	typ	k1gInSc7	typ
noční	noční	k2eAgFnSc2d1	noční
můry	můra	k1gFnSc2	můra
je	být	k5eAaImIp3nS	být
REM	REM	kA	REM
spánková	spánkový	k2eAgFnSc1d1	spánková
porucha	porucha	k1gFnSc1	porucha
chování	chování	k1gNnSc2	chování
(	(	kIx(	(
<g/>
RBD	RBD	kA	RBD
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
spící	spící	k2eAgFnSc6d1	spící
nemá	mít	k5eNaImIp3nS	mít
během	během	k7c2	během
REM	REM	kA	REM
fáze	fáze	k1gFnSc2	fáze
ochablé	ochablý	k2eAgNnSc1d1	ochablé
kosterní	kosterní	k2eAgNnSc1d1	kosterní
svalstvo	svalstvo	k1gNnSc1	svalstvo
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
během	během	k7c2	během
spánku	spánek	k1gInSc2	spánek
"	"	kIx"	"
<g/>
zápasí	zápasit	k5eAaImIp3nS	zápasit
<g/>
"	"	kIx"	"
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
noční	noční	k2eAgFnSc7d1	noční
můrou	můra	k1gFnSc7	můra
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
tak	tak	k6eAd1	tak
zranit	zranit	k5eAaPmF	zranit
sebe	sebe	k3xPyFc4	sebe
nebo	nebo	k8xC	nebo
spolunocležníka	spolunocležník	k1gMnSc4	spolunocležník
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Noční	noční	k2eAgInSc4d1	noční
děs	děs	k1gInSc4	děs
<g/>
.	.	kIx.	.
</s>
<s>
Noční	noční	k2eAgInSc1d1	noční
děs	děs	k1gInSc1	děs
(	(	kIx(	(
<g/>
pavor	pavor	k1gMnSc1	pavor
nocturnus	nocturnus	k1gMnSc1	nocturnus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
krátký	krátký	k2eAgInSc4d1	krátký
děsivý	děsivý	k2eAgInSc4d1	děsivý
sen	sen	k1gInSc4	sen
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
postihuje	postihovat	k5eAaImIp3nS	postihovat
spícího	spící	k2eAgInSc2d1	spící
během	během	k7c2	během
NREM	NREM	kA	NREM
spánku	spánek	k1gInSc2	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
jej	on	k3xPp3gInSc4	on
zřídkakdy	zřídkakdy	k6eAd1	zřídkakdy
probudí	probudit	k5eAaPmIp3nP	probudit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
probuzení	probuzení	k1gNnSc6	probuzení
upadá	upadat	k5eAaImIp3nS	upadat
člověk	člověk	k1gMnSc1	člověk
ihned	ihned	k6eAd1	ihned
znovu	znovu	k6eAd1	znovu
do	do	k7c2	do
spánku	spánek	k1gInSc2	spánek
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
si	se	k3xPyFc3	se
děsivý	děsivý	k2eAgInSc1d1	děsivý
sen	sen	k1gInSc1	sen
ráno	ráno	k6eAd1	ráno
nepamatuje	pamatovat	k5eNaImIp3nS	pamatovat
<g/>
.	.	kIx.	.
</s>
<s>
Somnambulismus	somnambulismus	k1gInSc1	somnambulismus
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
náměsíčnost	náměsíčnost	k1gFnSc1	náměsíčnost
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
spánková	spánkový	k2eAgFnSc1d1	spánková
porucha	porucha	k1gFnSc1	porucha
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
postižený	postižený	k1gMnSc1	postižený
během	během	k7c2	během
spánku	spánek	k1gInSc2	spánek
vstane	vstát	k5eAaPmIp3nS	vstát
z	z	k7c2	z
postele	postel	k1gFnSc2	postel
a	a	k8xC	a
prochází	procházet	k5eAaImIp3nS	procházet
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
může	moct	k5eAaImIp3nS	moct
vykonávat	vykonávat	k5eAaImF	vykonávat
i	i	k9	i
poměrně	poměrně	k6eAd1	poměrně
složité	složitý	k2eAgFnPc4d1	složitá
činnosti	činnost	k1gFnPc4	činnost
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
odeslat	odeslat	k5eAaPmF	odeslat
SMS	SMS	kA	SMS
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Náměsíční	náměsíční	k2eAgMnPc1d1	náměsíční
se	se	k3xPyFc4	se
nemají	mít	k5eNaImIp3nP	mít
budit	budit	k5eAaImF	budit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
odvedeni	odveden	k2eAgMnPc1d1	odveden
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
postele	postel	k1gFnSc2	postel
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
náměsíčnosti	náměsíčnost	k1gFnSc3	náměsíčnost
zpravidla	zpravidla	k6eAd1	zpravidla
dochází	docházet	k5eAaImIp3nS	docházet
během	během	k7c2	během
NREM	NREM	kA	NREM
spánku	spánek	k1gInSc2	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Mluvení	mluvení	k1gNnSc1	mluvení
ze	z	k7c2	z
spaní	spaní	k1gNnSc2	spaní
(	(	kIx(	(
<g/>
somnilokvie	somnilokvie	k1gFnSc1	somnilokvie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
u	u	k7c2	u
náměsíčnosti	náměsíčnost	k1gFnSc2	náměsíčnost
aktivita	aktivita	k1gFnSc1	aktivita
během	během	k7c2	během
spánku	spánek	k1gInSc2	spánek
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
mluvením	mluvení	k1gNnSc7	mluvení
<g/>
,	,	kIx,	,
výkřiky	výkřik	k1gInPc7	výkřik
<g/>
,	,	kIx,	,
zpíváním	zpívání	k1gNnSc7	zpívání
atd.	atd.	kA	atd.
Rovněž	rovněž	k9	rovněž
je	být	k5eAaImIp3nS	být
běžnější	běžný	k2eAgNnSc1d2	běžnější
v	v	k7c6	v
NREM	NREM	kA	NREM
spánku	spánek	k1gInSc6	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Řeckým	řecký	k2eAgMnSc7d1	řecký
bohem	bůh	k1gMnSc7	bůh
spánku	spánek	k1gInSc2	spánek
byl	být	k5eAaImAgMnS	být
Hypnos	hypnosa	k1gFnPc2	hypnosa
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
bratrem	bratr	k1gMnSc7	bratr
Thanata	Thanat	k1gMnSc2	Thanat
<g/>
,	,	kIx,	,
boha	bůh	k1gMnSc2	bůh
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
sestrami	sestra	k1gFnPc7	sestra
byly	být	k5eAaImAgInP	být
Oneiry	Oneir	k1gInPc1	Oneir
<g/>
,	,	kIx,	,
sny	sen	k1gInPc1	sen
<g/>
.	.	kIx.	.
</s>
