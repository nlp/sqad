<s>
Gotický	gotický	k2eAgInSc1d1	gotický
metal	metal	k1gInSc1	metal
<g/>
,	,	kIx,	,
gothic	gothic	k1gMnSc1	gothic
metal	metat	k5eAaImAgMnS	metat
či	či	k8xC	či
goth	goth	k1gMnSc1	goth
metal	metat	k5eAaImAgMnS	metat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
hudební	hudební	k2eAgInSc4d1	hudební
styl	styl	k1gInSc4	styl
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
zkombinováním	zkombinování	k1gNnSc7	zkombinování
death	deatha	k1gFnPc2	deatha
<g/>
/	/	kIx~	/
<g/>
doomu	doomat	k5eAaPmIp1nS	doomat
a	a	k8xC	a
gothic	gothic	k1gMnSc1	gothic
rocku	rock	k1gInSc2	rock
<g/>
.	.	kIx.	.
</s>
