<s>
Vltavská	vltavský	k2eAgFnSc1d1	Vltavská
kaskáda	kaskáda	k1gFnSc1	kaskáda
je	být	k5eAaImIp3nS	být
soustava	soustava	k1gFnSc1	soustava
vodních	vodní	k2eAgNnPc2d1	vodní
děl	dělo	k1gNnPc2	dělo
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Vltavě	Vltava	k1gFnSc6	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
celkem	celkem	k6eAd1	celkem
9	[number]	k4	9
přehrad	přehrada	k1gFnPc2	přehrada
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
první	první	k4xOgFnPc1	první
byly	být	k5eAaImAgFnP	být
budovány	budovat	k5eAaImNgFnP	budovat
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Vltavské	vltavský	k2eAgFnSc2d1	Vltavská
kaskády	kaskáda	k1gFnSc2	kaskáda
patří	patřit	k5eAaImIp3nS	patřit
přehrada	přehrada	k1gFnSc1	přehrada
zadržující	zadržující	k2eAgFnSc2d1	zadržující
největší	veliký	k2eAgInSc4d3	veliký
objem	objem	k1gInSc4	objem
vody	voda	k1gFnSc2	voda
z	z	k7c2	z
českých	český	k2eAgFnPc2d1	Česká
nádrží	nádrž	k1gFnPc2	nádrž
(	(	kIx(	(
<g/>
Orlík	Orlík	k1gInSc1	Orlík
<g/>
)	)	kIx)	)
i	i	k9	i
přehrada	přehrada	k1gFnSc1	přehrada
největší	veliký	k2eAgFnPc1d3	veliký
co	co	k9	co
do	do	k7c2	do
plochy	plocha	k1gFnSc2	plocha
hladiny	hladina	k1gFnSc2	hladina
(	(	kIx(	(
<g/>
Lipno	Lipno	k1gNnSc1	Lipno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgFnPc1d1	vodní
elektrárny	elektrárna	k1gFnPc1	elektrárna
v	v	k7c6	v
přehradách	přehrada	k1gFnPc6	přehrada
kaskády	kaskáda	k1gFnSc2	kaskáda
produkují	produkovat	k5eAaImIp3nP	produkovat
elektrický	elektrický	k2eAgInSc4d1	elektrický
výkon	výkon	k1gInSc4	výkon
až	až	k9	až
750	[number]	k4	750
MW	MW	kA	MW
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
jezy	jez	k1gInPc1	jez
na	na	k7c6	na
Vltavě	Vltava	k1gFnSc6	Vltava
byly	být	k5eAaImAgFnP	být
stavěny	stavit	k5eAaImNgFnP	stavit
již	již	k6eAd1	již
za	za	k7c2	za
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
I.	I.	kA	I.
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
úpravami	úprava	k1gFnPc7	úprava
zlepšujícími	zlepšující	k2eAgFnPc7d1	zlepšující
splavnost	splavnost	k1gFnSc4	splavnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1640	[number]	k4	1640
<g/>
–	–	k?	–
<g/>
1643	[number]	k4	1643
strahovské	strahovský	k2eAgFnSc2d1	Strahovská
opatství	opatství	k1gNnSc4	opatství
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
III	III	kA	III
odstranilo	odstranit	k5eAaPmAgNnS	odstranit
skalisko	skalisko	k1gNnSc4	skalisko
Horní	horní	k2eAgInSc1d1	horní
slap	slap	k1gInSc1	slap
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
nebezpečně	bezpečně	k6eNd1	bezpečně
zasahovalo	zasahovat	k5eAaImAgNnS	zasahovat
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
řečiště	řečiště	k1gNnSc2	řečiště
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zbytku	zbytek	k1gInSc6	zbytek
skály	skála	k1gFnSc2	skála
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
vztyčen	vztyčen	k2eAgInSc4d1	vztyčen
Ferdinandův	Ferdinandův	k2eAgInSc4d1	Ferdinandův
sloup	sloup	k1gInSc4	sloup
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc1d1	zvaný
též	též	k9	též
Solný	solný	k2eAgInSc1d1	solný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1722	[number]	k4	1722
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
přibyla	přibýt	k5eAaPmAgFnS	přibýt
socha	socha	k1gFnSc1	socha
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
<g/>
,	,	kIx,	,
po	po	k7c6	po
níž	jenž	k3xRgFnSc6	jenž
Svatojánské	svatojánský	k2eAgInPc1d1	svatojánský
proudy	proud	k1gInPc1	proud
získaly	získat	k5eAaPmAgInP	získat
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
památky	památka	k1gFnPc1	památka
byly	být	k5eAaImAgFnP	být
přemístěny	přemístit	k5eAaPmNgFnP	přemístit
pod	pod	k7c4	pod
hráz	hráz	k1gFnSc4	hráz
Slapské	slapský	k2eAgFnSc2d1	Slapská
nádrže	nádrž	k1gFnSc2	nádrž
poblíž	poblíž	k7c2	poblíž
budovy	budova	k1gFnSc2	budova
elektrárny	elektrárna	k1gFnSc2	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
ucelený	ucelený	k2eAgInSc4d1	ucelený
projekt	projekt	k1gInSc4	projekt
usplavnění	usplavnění	k1gNnSc2	usplavnění
Vltavy	Vltava	k1gFnSc2	Vltava
mezi	mezi	k7c7	mezi
Mělníkem	Mělník	k1gInSc7	Mělník
a	a	k8xC	a
Českými	český	k2eAgInPc7d1	český
Budějovicemi	Budějovice	k1gInPc7	Budějovice
zpracovala	zpracovat	k5eAaPmAgFnS	zpracovat
roku	rok	k1gInSc2	rok
1894	[number]	k4	1894
firma	firma	k1gFnSc1	firma
Lanna	Lanna	k1gFnSc1	Lanna
a	a	k8xC	a
Vering	Vering	k1gInSc1	Vering
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
Vltava	Vltava	k1gFnSc1	Vltava
hojně	hojně	k6eAd1	hojně
využívaná	využívaný	k2eAgFnSc1d1	využívaná
pro	pro	k7c4	pro
voroplavbu	voroplavba	k1gFnSc4	voroplavba
a	a	k8xC	a
klasickou	klasický	k2eAgFnSc4d1	klasická
plavbu	plavba	k1gFnSc4	plavba
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
přepravu	přeprava	k1gFnSc4	přeprava
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
kamene	kámen	k1gInSc2	kámen
a	a	k8xC	a
soli	sůl	k1gFnSc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
námět	námět	k1gInSc1	námět
na	na	k7c4	na
postavení	postavení	k1gNnSc4	postavení
dvou	dva	k4xCgFnPc2	dva
vysokých	vysoký	k2eAgFnPc2d1	vysoká
přehrad	přehrada	k1gFnPc2	přehrada
<g/>
,	,	kIx,	,
u	u	k7c2	u
Slap	slap	k1gInSc4	slap
a	a	k8xC	a
u	u	k7c2	u
Orlíku	Orlík	k1gInSc2	Orlík
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgMnSc1d1	nový
<g/>
,	,	kIx,	,
energetický	energetický	k2eAgInSc1d1	energetický
zájem	zájem	k1gInSc1	zájem
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
střetu	střet	k1gInSc2	střet
s	s	k7c7	s
dosavadními	dosavadní	k2eAgInPc7d1	dosavadní
hospodářskými	hospodářský	k2eAgInPc7d1	hospodářský
zájmy	zájem	k1gInPc7	zájem
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnPc4	první
dvě	dva	k4xCgNnPc4	dva
vodní	vodní	k2eAgNnPc4d1	vodní
díla	dílo	k1gNnPc4	dílo
kaskády	kaskáda	k1gFnSc2	kaskáda
<g/>
,	,	kIx,	,
Vrané	vraný	k2eAgFnPc4d1	Vraná
a	a	k8xC	a
Štěchovice	Štěchovice	k1gFnPc4	Štěchovice
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
budována	budovat	k5eAaImNgFnS	budovat
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
plavební	plavební	k2eAgInPc4d1	plavební
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
účelem	účel	k1gInSc7	účel
výstavby	výstavba	k1gFnSc2	výstavba
prvních	první	k4xOgInPc2	první
děl	dělo	k1gNnPc2	dělo
Vltavské	vltavský	k2eAgFnSc2d1	Vltavská
kaskády	kaskáda	k1gFnSc2	kaskáda
byla	být	k5eAaImAgFnS	být
výroba	výroba	k1gFnSc1	výroba
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
byla	být	k5eAaImAgFnS	být
upřednostněna	upřednostněn	k2eAgFnSc1d1	upřednostněna
akumulační	akumulační	k2eAgFnSc1d1	akumulační
funkce	funkce	k1gFnSc1	funkce
a	a	k8xC	a
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
vyplývající	vyplývající	k2eAgNnPc1d1	vyplývající
nadlepšení	nadlepšení	k1gNnSc4	nadlepšení
průtoků	průtok	k1gInPc2	průtok
na	na	k7c6	na
Vltavě	Vltava	k1gFnSc6	Vltava
a	a	k8xC	a
na	na	k7c6	na
dolním	dolní	k2eAgNnSc6d1	dolní
Labi	Labe	k1gNnSc6	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Vltavské	vltavský	k2eAgFnPc1d1	Vltavská
nádrže	nádrž	k1gFnPc1	nádrž
mají	mít	k5eAaImIp3nP	mít
význam	význam	k1gInSc4	význam
především	především	k6eAd1	především
energetický	energetický	k2eAgInSc4d1	energetický
(	(	kIx(	(
<g/>
výroba	výroba	k1gFnSc1	výroba
el.	el.	k?	el.
<g/>
energie	energie	k1gFnSc2	energie
v	v	k7c6	v
hydroelektrárnách	hydroelektrárna	k1gFnPc6	hydroelektrárna
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
při	při	k7c6	při
špičkovém	špičkový	k2eAgInSc6d1	špičkový
odběru	odběr	k1gInSc6	odběr
<g/>
)	)	kIx)	)
a	a	k8xC	a
ochranný	ochranný	k2eAgInSc4d1	ochranný
před	před	k7c7	před
povodněmi	povodeň	k1gFnPc7	povodeň
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
plavební	plavební	k2eAgInSc1d1	plavební
<g/>
,	,	kIx,	,
rekreační	rekreační	k2eAgInSc1d1	rekreační
a	a	k8xC	a
vodárenský	vodárenský	k2eAgInSc1d1	vodárenský
<g/>
.	.	kIx.	.
</s>
<s>
Vedlejšími	vedlejší	k2eAgInPc7d1	vedlejší
přínosy	přínos	k1gInPc7	přínos
jsou	být	k5eAaImIp3nP	být
ochrana	ochrana	k1gFnSc1	ochrana
před	před	k7c7	před
povodněmi	povodeň	k1gFnPc7	povodeň
<g/>
,	,	kIx,	,
usplavnění	usplavnění	k1gNnSc4	usplavnění
některých	některý	k3yIgFnPc2	některý
částí	část	k1gFnPc2	část
Vltavy	Vltava	k1gFnSc2	Vltava
<g/>
,	,	kIx,	,
stabilizace	stabilizace	k1gFnSc2	stabilizace
hladiny	hladina	k1gFnSc2	hladina
pro	pro	k7c4	pro
odběr	odběr	k1gInSc4	odběr
vody	voda	k1gFnSc2	voda
k	k	k7c3	k
průmyslovým	průmyslový	k2eAgInPc3d1	průmyslový
účelům	účel	k1gInPc3	účel
i	i	k9	i
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
vytvoření	vytvoření	k1gNnSc1	vytvoření
nových	nový	k2eAgNnPc2d1	nové
rekreačních	rekreační	k2eAgNnPc2d1	rekreační
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgNnSc1d1	vodní
hospodaření	hospodaření	k1gNnSc1	hospodaření
na	na	k7c6	na
Vltavě	Vltava	k1gFnSc6	Vltava
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
i	i	k9	i
splavnost	splavnost	k1gFnSc1	splavnost
Labe	Labe	k1gNnSc2	Labe
pod	pod	k7c7	pod
Mělníkem	Mělník	k1gInSc7	Mělník
<g/>
.	.	kIx.	.
</s>
<s>
Kaskáda	kaskáda	k1gFnSc1	kaskáda
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
platného	platný	k2eAgInSc2d1	platný
manipulačního	manipulační	k2eAgInSc2d1	manipulační
řádu	řád	k1gInSc2	řád
schopna	schopen	k2eAgFnSc1d1	schopna
zcela	zcela	k6eAd1	zcela
zastavit	zastavit	k5eAaPmF	zastavit
povodeň	povodeň	k1gFnSc4	povodeň
do	do	k7c2	do
velikosti	velikost	k1gFnSc2	velikost
dvacetileté	dvacetiletý	k2eAgFnSc2d1	dvacetiletá
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
povodně	povodně	k6eAd1	povodně
větší	veliký	k2eAgNnSc1d2	veliký
zmírnit	zmírnit	k5eAaPmF	zmírnit
(	(	kIx(	(
<g/>
transformovat	transformovat	k5eAaBmF	transformovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
během	během	k7c2	během
povodně	povodeň	k1gFnSc2	povodeň
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
max	max	kA	max
<g/>
.	.	kIx.	.
přítok	přítok	k1gInSc1	přítok
do	do	k7c2	do
VD	VD	kA	VD
Orlík	Orlík	k1gInSc1	Orlík
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
3900	[number]	k4	3900
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
·	·	k?	·
<g/>
s	s	k7c7	s
<g/>
−	−	k?	−
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
překračující	překračující	k2eAgFnSc1d1	překračující
úroveň	úroveň	k1gFnSc1	úroveň
tisícileté	tisíciletý	k2eAgFnSc2d1	tisíciletá
povodně	povodeň	k1gFnSc2	povodeň
<g/>
,	,	kIx,	,
snížen	snížen	k2eAgInSc1d1	snížen
transformací	transformace	k1gFnSc7	transformace
na	na	k7c6	na
VD	VD	kA	VD
Orlík	Orlík	k1gInSc1	Orlík
o	o	k7c4	o
800	[number]	k4	800
až	až	k9	až
900	[number]	k4	900
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
·	·	k?	·
<g/>
s	s	k7c7	s
<g/>
−	−	k?	−
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
technické	technický	k2eAgNnSc1d1	technické
řešení	řešení	k1gNnSc1	řešení
a	a	k8xC	a
architektura	architektura	k1gFnSc1	architektura
přehrad	přehrada	k1gFnPc2	přehrada
je	být	k5eAaImIp3nS	být
předmětem	předmět	k1gInSc7	předmět
obdivu	obdiv	k1gInSc2	obdiv
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
přehrad	přehrada	k1gFnPc2	přehrada
však	však	k9	však
znamenala	znamenat	k5eAaImAgFnS	znamenat
také	také	k9	také
zničení	zničení	k1gNnSc3	zničení
jedinečných	jedinečný	k2eAgFnPc2d1	jedinečná
přírodně	přírodně	k6eAd1	přírodně
i	i	k9	i
historicky	historicky	k6eAd1	historicky
cenných	cenný	k2eAgNnPc2d1	cenné
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Svatojánských	svatojánský	k2eAgInPc2d1	svatojánský
proudů	proud	k1gInPc2	proud
a	a	k8xC	a
mnoha	mnoho	k4c2	mnoho
vesnic	vesnice	k1gFnPc2	vesnice
i	i	k8xC	i
osad	osada	k1gFnPc2	osada
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
ukončila	ukončit	k5eAaPmAgFnS	ukončit
tradiční	tradiční	k2eAgFnSc4d1	tradiční
vltavskou	vltavský	k2eAgFnSc4d1	Vltavská
voroplavbu	voroplavba	k1gFnSc4	voroplavba
<g/>
.	.	kIx.	.
</s>
<s>
Výstavbou	výstavba	k1gFnSc7	výstavba
se	se	k3xPyFc4	se
stabilizovala	stabilizovat	k5eAaBmAgFnS	stabilizovat
teplota	teplota	k1gFnSc1	teplota
řeky	řeka	k1gFnSc2	řeka
pod	pod	k7c7	pod
přehradami	přehrada	k1gFnPc7	přehrada
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
již	již	k6eAd1	již
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
obvykle	obvykle	k6eAd1	obvykle
mimo	mimo	k7c4	mimo
slepá	slepý	k2eAgNnPc4d1	slepé
ramena	rameno	k1gNnPc4	rameno
nezamrzá	zamrzat	k5eNaImIp3nS	zamrzat
a	a	k8xC	a
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
příliš	příliš	k6eAd1	příliš
chladná	chladný	k2eAgFnSc1d1	chladná
na	na	k7c4	na
koupání	koupání	k1gNnSc4	koupání
<g/>
.	.	kIx.	.
</s>
<s>
Upraven	upraven	k2eAgMnSc1d1	upraven
musel	muset	k5eAaImAgMnS	muset
být	být	k5eAaImF	být
zámek	zámek	k1gInSc4	zámek
Orlík	orlík	k1gMnSc1	orlík
a	a	k8xC	a
rozebrán	rozebrán	k2eAgMnSc1d1	rozebrán
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
sestaven	sestavit	k5eAaPmNgInS	sestavit
kostelík	kostelík	k1gInSc1	kostelík
v	v	k7c6	v
Červené	Červená	k1gFnSc6	Červená
z	z	k7c2	z
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Žďákova	Žďákov	k1gInSc2	Žďákov
překlenul	překlenout	k5eAaPmAgMnS	překlenout
Vltavu	Vltava	k1gFnSc4	Vltava
jednoobloukový	jednoobloukový	k2eAgInSc4d1	jednoobloukový
Žďákovský	Žďákovský	k2eAgInSc4d1	Žďákovský
most	most	k1gInSc4	most
s	s	k7c7	s
největším	veliký	k2eAgNnSc7d3	veliký
rozpětím	rozpětí	k1gNnSc7	rozpětí
jednoho	jeden	k4xCgInSc2	jeden
oblouku	oblouk	k1gInSc2	oblouk
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Elektrotechnika	elektrotechnika	k1gFnSc1	elektrotechnika
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Elektrotechnický	elektrotechnický	k2eAgInSc1d1	elektrotechnický
svaz	svaz	k1gInSc1	svaz
československý	československý	k2eAgInSc1d1	československý
<g/>
,	,	kIx,	,
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
-	-	kIx~	-
kapitola	kapitola	k1gFnSc1	kapitola
Středovltavské	Středovltavský	k2eAgFnSc2d1	Středovltavský
vodní	vodní	k2eAgFnSc2d1	vodní
elektrárny	elektrárna	k1gFnSc2	elektrárna
<g/>
,	,	kIx,	,
s.	s.	k?	s.
26	[number]	k4	26
<g/>
-	-	kIx~	-
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
</s>
<s>
Protipovodňová	protipovodňový	k2eAgFnSc1d1	protipovodňová
ochrana	ochrana	k1gFnSc1	ochrana
Prahy	Praha	k1gFnSc2	Praha
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vltavská	vltavský	k2eAgFnSc1d1	Vltavská
kaskáda	kaskáda	k1gFnSc1	kaskáda
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Vltavská	vltavský	k2eAgFnSc1d1	Vltavská
kaskáda	kaskáda	k1gFnSc1	kaskáda
(	(	kIx(	(
<g/>
stručný	stručný	k2eAgInSc1d1	stručný
přehled	přehled	k1gInSc1	přehled
na	na	k7c6	na
webu	web	k1gInSc6	web
Povodí	povodí	k1gNnSc2	povodí
Vltavy	Vltava	k1gFnSc2	Vltava
<g/>
)	)	kIx)	)
Seznam	seznam	k1gInSc1	seznam
vybraných	vybraný	k2eAgFnPc2d1	vybraná
nádrží	nádrž	k1gFnPc2	nádrž
ve	v	k7c6	v
správě	správa	k1gFnSc6	správa
Povodí	povodí	k1gNnSc2	povodí
Vltavy	Vltava	k1gFnSc2	Vltava
Potenciál	potenciál	k1gInSc1	potenciál
splavnění	splavnění	k1gNnSc2	splavnění
vltavské	vltavský	k2eAgFnSc2d1	Vltavská
vodní	vodní	k2eAgFnSc2d1	vodní
cesty	cesta	k1gFnSc2	cesta
(	(	kIx(	(
<g/>
CityPlan	CityPlan	k1gInSc1	CityPlan
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
s	s	k7c7	s
r.	r.	kA	r.
o.	o.	k?	o.
2004	[number]	k4	2004
<g/>
–	–	k?	–
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
</s>
