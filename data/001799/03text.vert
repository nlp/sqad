<s>
ARM	ARM	kA	ARM
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
označení	označení	k1gNnSc1	označení
architektury	architektura	k1gFnSc2	architektura
procesorů	procesor	k1gInPc2	procesor
používaných	používaný	k2eAgInPc2d1	používaný
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
nízké	nízký	k2eAgFnSc3d1	nízká
spotřebě	spotřeba	k1gFnSc3	spotřeba
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
zejména	zejména	k9	zejména
v	v	k7c6	v
mobilních	mobilní	k2eAgNnPc6d1	mobilní
zařízeních	zařízení	k1gNnPc6	zařízení
(	(	kIx(	(
<g/>
mobilní	mobilní	k2eAgInPc1d1	mobilní
telefony	telefon	k1gInPc1	telefon
<g/>
,	,	kIx,	,
tablety	tableta	k1gFnPc1	tableta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Globálně	globálně	k6eAd1	globálně
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
ARM	ARM	kA	ARM
nejpočetněji	početně	k6eAd3	početně
zastoupenou	zastoupený	k2eAgFnSc7d1	zastoupená
architekturou	architektura	k1gFnSc7	architektura
mikroprocesorů	mikroprocesor	k1gInPc2	mikroprocesor
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
60	[number]	k4	60
%	%	kIx~	%
mobilních	mobilní	k2eAgNnPc2d1	mobilní
zařízení	zařízení	k1gNnPc2	zařízení
na	na	k7c6	na
světě	svět	k1gInSc6	svět
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
ARM	ARM	kA	ARM
čip	čip	k1gInSc1	čip
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
bylo	být	k5eAaImAgNnS	být
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
10	[number]	k4	10
miliard	miliarda	k4xCgFnPc2	miliarda
ARM	ARM	kA	ARM
procesorů	procesor	k1gInPc2	procesor
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
už	už	k9	už
50	[number]	k4	50
miliard	miliarda	k4xCgFnPc2	miliarda
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
ARM	ARM	kA	ARM
architektury	architektura	k1gFnSc2	architektura
započal	započnout	k5eAaPmAgInS	započnout
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
ve	v	k7c6	v
firmě	firma	k1gFnSc6	firma
ARM	ARM	kA	ARM
Holdings	Holdings	k1gInSc1	Holdings
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
ARM	ARM	kA	ARM
Holdings	Holdingsa	k1gFnPc2	Holdingsa
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
ARM	ARM	kA	ARM
Limited	limited	k2eAgInPc1d1	limited
<g/>
)	)	kIx)	)
používala	používat	k5eAaImAgFnS	používat
dříve	dříve	k6eAd2	dříve
pro	pro	k7c4	pro
ARM	ARM	kA	ARM
architekturu	architektura	k1gFnSc4	architektura
obchodní	obchodní	k2eAgInSc4d1	obchodní
název	název	k1gInSc4	název
Advanced	Advanced	k1gInSc1	Advanced
RISC	RISC	kA	RISC
Machine	Machin	k1gMnSc5	Machin
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
pak	pak	k6eAd1	pak
Acorn	Acorn	k1gMnSc1	Acorn
RISC	RISC	kA	RISC
Machine	Machin	k1gMnSc5	Machin
<g/>
.	.	kIx.	.
</s>
<s>
ARM	ARM	kA	ARM
architektura	architektura	k1gFnSc1	architektura
způsobila	způsobit	k5eAaPmAgFnS	způsobit
v	v	k7c6	v
několika	několik	k4yIc6	několik
směrech	směr	k1gInPc6	směr
revoluci	revoluce	k1gFnSc3	revoluce
v	v	k7c6	v
informačních	informační	k2eAgFnPc6d1	informační
technologiích	technologie	k1gFnPc6	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
návrh	návrh	k1gInSc1	návrh
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
filosofií	filosofie	k1gFnSc7	filosofie
RISC	RISC	kA	RISC
<g/>
,	,	kIx,	,
neméně	málo	k6eNd2	málo
pozoruhodné	pozoruhodný	k2eAgNnSc1d1	pozoruhodné
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
první	první	k4xOgInPc1	první
procesory	procesor	k1gInPc1	procesor
ARM	ARM	kA	ARM
byly	být	k5eAaImAgInP	být
založeny	založit	k5eAaPmNgInP	založit
na	na	k7c4	na
GaAs	GaAs	k1gInSc4	GaAs
polovodičích	polovodič	k1gInPc6	polovodič
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
dovolily	dovolit	k5eAaPmAgInP	dovolit
na	na	k7c4	na
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
dobu	doba	k1gFnSc4	doba
velmi	velmi	k6eAd1	velmi
vysoké	vysoký	k2eAgFnSc2d1	vysoká
taktovací	taktovací	k2eAgFnSc2d1	taktovací
frekvence	frekvence	k1gFnSc2	frekvence
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
použitá	použitý	k2eAgFnSc1d1	použitá
32	[number]	k4	32
<g/>
bitová	bitový	k2eAgFnSc1d1	bitová
šířka	šířka	k1gFnSc1	šířka
slova	slovo	k1gNnSc2	slovo
nebyla	být	k5eNaImAgFnS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vzniku	vznik	k1gInSc2	vznik
ARMu	ARMus	k1gInSc2	ARMus
samozřejmostí	samozřejmost	k1gFnPc2	samozřejmost
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
mikroprocesor	mikroprocesor	k1gInSc1	mikroprocesor
s	s	k7c7	s
architekturou	architektura	k1gFnSc7	architektura
ARM	ARM	kA	ARM
byl	být	k5eAaImAgInS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
firmou	firma	k1gFnSc7	firma
ARM	ARM	kA	ARM
Limited	limited	k2eAgFnSc1d1	limited
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
ARM	ARM	kA	ARM
Holdings	Holdings	k1gInSc4	Holdings
časem	časem	k6eAd1	časem
ustoupila	ustoupit	k5eAaPmAgFnS	ustoupit
od	od	k7c2	od
výroby	výroba	k1gFnSc2	výroba
procesorů	procesor	k1gInPc2	procesor
a	a	k8xC	a
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
soustředila	soustředit	k5eAaPmAgFnS	soustředit
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
jejich	jejich	k3xOp3gInSc4	jejich
vývoj	vývoj	k1gInSc4	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Schéma	schéma	k1gNnSc1	schéma
procesorů	procesor	k1gInPc2	procesor
ARM	ARM	kA	ARM
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
"	"	kIx"	"
<g/>
intelektuálním	intelektuální	k2eAgNnSc7d1	intelektuální
vlastnictvím	vlastnictví	k1gNnSc7	vlastnictví
<g/>
"	"	kIx"	"
firmy	firma	k1gFnSc2	firma
ARM	ARM	kA	ARM
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
od	od	k7c2	od
výrobců	výrobce	k1gMnPc2	výrobce
hardware	hardware	k1gInSc1	hardware
vybírá	vybírat	k5eAaImIp3nS	vybírat
licence	licence	k1gFnSc1	licence
za	za	k7c4	za
jeho	jeho	k3xOp3gNnSc4	jeho
použití	použití	k1gNnSc4	použití
<g/>
.	.	kIx.	.
</s>
<s>
Procesory	procesor	k1gInPc1	procesor
ARM	ARM	kA	ARM
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
možné	možný	k2eAgNnSc1d1	možné
najít	najít	k5eAaPmF	najít
ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
odvětvích	odvětví	k1gNnPc6	odvětví
spotřební	spotřební	k2eAgFnSc2d1	spotřební
elektroniky	elektronika	k1gFnSc2	elektronika
od	od	k7c2	od
PDA	PDA	kA	PDA
<g/>
,	,	kIx,	,
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonů	telefon	k1gInPc2	telefon
<g/>
,	,	kIx,	,
multimediálních	multimediální	k2eAgInPc2d1	multimediální
přehrávačů	přehrávač	k1gInPc2	přehrávač
<g/>
,	,	kIx,	,
přenosných	přenosný	k2eAgFnPc2d1	přenosná
herních	herní	k2eAgFnPc2d1	herní
konzolí	konzole	k1gFnPc2	konzole
<g/>
,	,	kIx,	,
kalkulaček	kalkulačka	k1gFnPc2	kalkulačka
až	až	k8xS	až
po	po	k7c4	po
počítačové	počítačový	k2eAgFnPc4d1	počítačová
periferie	periferie	k1gFnPc4	periferie
(	(	kIx(	(
<g/>
pevné	pevný	k2eAgInPc4d1	pevný
disky	disk	k1gInPc4	disk
<g/>
,	,	kIx,	,
routery	router	k1gInPc4	router
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Procesory	procesor	k1gInPc1	procesor
ARM	ARM	kA	ARM
mají	mít	k5eAaImIp3nP	mít
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
výrobním	výrobní	k2eAgInSc6d1	výrobní
programu	program	k1gInSc6	program
desítky	desítka	k1gFnSc2	desítka
výrobců	výrobce	k1gMnPc2	výrobce
<g/>
,	,	kIx,	,
ve	v	k7c6	v
spotřební	spotřební	k2eAgFnSc6d1	spotřební
elektronice	elektronika	k1gFnSc6	elektronika
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používají	používat	k5eAaImIp3nP	používat
např.	např.	kA	např.
procesory	procesor	k1gInPc7	procesor
XScale	XScala	k1gFnSc6	XScala
od	od	k7c2	od
firmy	firma	k1gFnSc2	firma
Marvell	Marvell	k1gInSc1	Marvell
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
OMAP	OMAP	kA	OMAP
od	od	k7c2	od
firmy	firma	k1gFnSc2	firma
Texas	Texas	k1gInSc1	Texas
Instruments	Instruments	kA	Instruments
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
byla	být	k5eAaImAgFnS	být
architektura	architektura	k1gFnSc1	architektura
ARM	ARM	kA	ARM
zastoupena	zastoupit	k5eAaPmNgFnS	zastoupit
v	v	k7c4	v
98	[number]	k4	98
%	%	kIx~	%
z	z	k7c2	z
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jedné	jeden	k4xCgFnSc2	jeden
miliardy	miliarda	k4xCgFnSc2	miliarda
každoročně	každoročně	k6eAd1	každoročně
prodaných	prodaný	k2eAgInPc2d1	prodaný
mobilů	mobil	k1gInPc2	mobil
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Rostoucí	rostoucí	k2eAgInSc1d1	rostoucí
význam	význam	k1gInSc1	význam
architektury	architektura	k1gFnSc2	architektura
ARM	ARM	kA	ARM
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gMnPc1	jejíž
nejvýkonnější	výkonný	k2eAgMnPc1d3	nejvýkonnější
zástupci	zástupce	k1gMnPc1	zástupce
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
mají	mít	k5eAaImIp3nP	mít
dostatečný	dostatečný	k2eAgInSc4d1	dostatečný
výkon	výkon	k1gInSc4	výkon
i	i	k9	i
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
v	v	k7c6	v
jednodušších	jednoduchý	k2eAgInPc6d2	jednodušší
osobních	osobní	k2eAgInPc6d1	osobní
počítačích	počítač	k1gInPc6	počítač
byl	být	k5eAaImAgInS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
impulsů	impuls	k1gInPc2	impuls
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
přiměly	přimět	k5eAaPmAgFnP	přimět
firmu	firma	k1gFnSc4	firma
Intel	Intel	kA	Intel
k	k	k7c3	k
vývoji	vývoj	k1gInSc3	vývoj
"	"	kIx"	"
<g/>
nízkopříkonových	nízkopříkonův	k2eAgInPc2d1	nízkopříkonův
<g/>
"	"	kIx"	"
procesorů	procesor	k1gInPc2	procesor
Intel	Intel	kA	Intel
Atom	atom	k1gInSc1	atom
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
byl	být	k5eAaImAgInS	být
Intel	Intel	kA	Intel
Atom	atom	k1gInSc4	atom
odpovědí	odpověď	k1gFnPc2	odpověď
na	na	k7c4	na
úsporné	úsporný	k2eAgInPc4d1	úsporný
mikroprocesory	mikroprocesor	k1gInPc4	mikroprocesor
AMD	AMD	kA	AMD
Geode	Geod	k1gMnSc5	Geod
<g/>
.	.	kIx.	.
</s>
<s>
Architektura	architektura	k1gFnSc1	architektura
ARM	ARM	kA	ARM
se	se	k3xPyFc4	se
nejvýrazněji	výrazně	k6eAd3	výrazně
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
v	v	k7c6	v
mobilních	mobilní	k2eAgNnPc6d1	mobilní
zařízeních	zařízení	k1gNnPc6	zařízení
(	(	kIx(	(
<g/>
mobilní	mobilní	k2eAgInPc1d1	mobilní
telefony	telefon	k1gInPc1	telefon
<g/>
,	,	kIx,	,
tablety	tableta	k1gFnPc1	tableta
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c6	v
vestavěných	vestavěný	k2eAgInPc6d1	vestavěný
systémech	systém	k1gInPc6	systém
(	(	kIx(	(
<g/>
pevný	pevný	k2eAgInSc1d1	pevný
disk	disk	k1gInSc1	disk
<g/>
,	,	kIx,	,
USB	USB	kA	USB
flash	flash	k1gInSc1	flash
disk	disk	k1gInSc1	disk
<g/>
,	,	kIx,	,
Wi-Fi	Wi-Fi	k1gNnPc7	Wi-Fi
čipy	čip	k1gInPc1	čip
<g/>
,	,	kIx,	,
routery	router	k1gInPc1	router
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nízká	nízký	k2eAgFnSc1d1	nízká
spotřeba	spotřeba	k1gFnSc1	spotřeba
energie	energie	k1gFnSc2	energie
při	při	k7c6	při
vysokém	vysoký	k2eAgInSc6d1	vysoký
výpočetním	výpočetní	k2eAgInSc6d1	výpočetní
výkonu	výkon	k1gInSc6	výkon
má	mít	k5eAaImIp3nS	mít
zásadní	zásadní	k2eAgInSc4d1	zásadní
význam	význam	k1gInSc4	význam
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
zařízeních	zařízení	k1gNnPc6	zařízení
napájených	napájený	k2eAgFnPc2d1	napájená
bateriemi	baterie	k1gFnPc7	baterie
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
je	být	k5eAaImIp3nS	být
velkou	velký	k2eAgFnSc7d1	velká
výhodou	výhoda	k1gFnSc7	výhoda
také	také	k9	také
u	u	k7c2	u
zařízení	zařízení	k1gNnSc2	zařízení
pracujících	pracující	k1gMnPc2	pracující
v	v	k7c6	v
náročných	náročný	k2eAgFnPc6d1	náročná
tepelných	tepelný	k2eAgFnPc6d1	tepelná
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Nízkopříkonové	Nízkopříkonový	k2eAgInPc1d1	Nízkopříkonový
procesory	procesor	k1gInPc1	procesor
totiž	totiž	k9	totiž
nepotřebují	potřebovat	k5eNaImIp3nP	potřebovat
složité	složitý	k2eAgInPc1d1	složitý
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
relativně	relativně	k6eAd1	relativně
nespolehlivé	spolehlivý	k2eNgNnSc1d1	nespolehlivé
chlazení	chlazení	k1gNnSc1	chlazení
<g/>
.	.	kIx.	.
přístup	přístup	k1gInSc1	přístup
do	do	k7c2	do
paměti	paměť	k1gFnSc2	paměť
pouze	pouze	k6eAd1	pouze
instrukcemi	instrukce	k1gFnPc7	instrukce
Load	Load	k1gInSc4	Load
<g/>
/	/	kIx~	/
<g/>
Store	Stor	k1gMnSc5	Stor
částečné	částečný	k2eAgNnSc4d1	částečné
překrývání	překrývání	k1gNnSc4	překrývání
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
registrů	registr	k1gInPc2	registr
možnost	možnost	k1gFnSc4	možnost
podmíněného	podmíněný	k2eAgNnSc2d1	podmíněné
vykonání	vykonání	k1gNnSc2	vykonání
instrukcí	instrukce	k1gFnPc2	instrukce
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
a	a	k8xC	a
výkonný	výkonný	k2eAgInSc1d1	výkonný
instrukční	instrukční	k2eAgInSc1d1	instrukční
soubor	soubor	k1gInSc1	soubor
<g/>
,	,	kIx,	,
jednoduše	jednoduše	k6eAd1	jednoduše
využitelné	využitelný	k2eAgInPc1d1	využitelný
kompilátory	kompilátor	k1gInPc1	kompilátor
vyšších	vysoký	k2eAgInPc2d2	vyšší
programovacích	programovací	k2eAgInPc2d1	programovací
jazyků	jazyk	k1gInPc2	jazyk
Procesory	procesor	k1gInPc1	procesor
ARM	ARM	kA	ARM
podporují	podporovat	k5eAaImIp3nP	podporovat
dva	dva	k4xCgInPc4	dva
adresové	adresový	k2eAgInPc4d1	adresový
módy	mód	k1gInPc4	mód
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
adresovat	adresovat	k5eAaBmF	adresovat
buď	buď	k8xC	buď
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
čítače	čítač	k1gInSc2	čítač
instrukcí	instrukce	k1gFnPc2	instrukce
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pomocí	pomocí	k7c2	pomocí
bázové	bázový	k2eAgFnSc2d1	bázová
adresy	adresa	k1gFnSc2	adresa
uložené	uložený	k2eAgFnSc2d1	uložená
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
registrů	registr	k1gInPc2	registr
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
paměti	paměť	k1gFnSc2	paměť
lze	lze	k6eAd1	lze
přistupovat	přistupovat	k5eAaImF	přistupovat
pouze	pouze	k6eAd1	pouze
instrukcemi	instrukce	k1gFnPc7	instrukce
Load	Load	k1gInSc4	Load
<g/>
/	/	kIx~	/
<g/>
Store	Stor	k1gMnSc5	Stor
(	(	kIx(	(
<g/>
Load-Store	Load-Stor	k1gMnSc5	Load-Stor
Architecture	Architectur	k1gMnSc5	Architectur
<g/>
)	)	kIx)	)
výrazně	výrazně	k6eAd1	výrazně
zjednodušuje	zjednodušovat	k5eAaImIp3nS	zjednodušovat
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
jednotku	jednotka	k1gFnSc4	jednotka
(	(	kIx(	(
<g/>
Execution	Execution	k1gInSc1	Execution
Unit	Unita	k1gFnPc2	Unita
<g/>
)	)	kIx)	)
procesoru	procesor	k1gInSc2	procesor
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc4	několik
instrukcí	instrukce	k1gFnPc2	instrukce
pracuje	pracovat	k5eAaImIp3nS	pracovat
přímo	přímo	k6eAd1	přímo
s	s	k7c7	s
pamětí	paměť	k1gFnSc7	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
instrukcí	instrukce	k1gFnPc2	instrukce
pracuje	pracovat	k5eAaImIp3nS	pracovat
s	s	k7c7	s
vnitřními	vnitřní	k2eAgInPc7d1	vnitřní
registry	registr	k1gInPc7	registr
<g/>
.	.	kIx.	.
</s>
<s>
ARM	ARM	kA	ARM
procesory	procesor	k1gInPc4	procesor
podporují	podporovat	k5eAaImIp3nP	podporovat
dvě	dva	k4xCgFnPc1	dva
úrovně	úroveň	k1gFnPc1	úroveň
priority	priorita	k1gFnSc2	priorita
přerušení	přerušení	k1gNnSc2	přerušení
s	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
zaměnitelnými	zaměnitelný	k2eAgFnPc7d1	zaměnitelná
bankami	banka	k1gFnPc7	banka
registrů	registr	k1gInPc2	registr
<g/>
.	.	kIx.	.
</s>
<s>
Nejkratší	krátký	k2eAgFnSc1d3	nejkratší
doba	doba	k1gFnSc1	doba
provedení	provedení	k1gNnSc2	provedení
požadavku	požadavek	k1gInSc2	požadavek
na	na	k7c6	na
přerušení	přerušení	k1gNnSc6	přerušení
je	být	k5eAaImIp3nS	být
poskytována	poskytovat	k5eAaImNgFnS	poskytovat
režimem	režim	k1gInSc7	režim
rychlého	rychlý	k2eAgNnSc2d1	rychlé
přerušení	přerušení	k1gNnSc2	přerušení
FIQ	FIQ	kA	FIQ
(	(	kIx(	(
<g/>
Fast	Fast	k1gMnSc1	Fast
Interrupt	Interrupt	k1gMnSc1	Interrupt
Request	Request	k1gMnSc1	Request
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
typ	typ	k1gInSc1	typ
přerušení	přerušení	k1gNnSc2	přerušení
je	být	k5eAaImIp3nS	být
IRQ	IRQ	kA	IRQ
(	(	kIx(	(
<g/>
Interrupt	Interrupt	k1gMnSc1	Interrupt
Request	Request	k1gMnSc1	Request
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
obsluhu	obsluha	k1gFnSc4	obsluha
přerušení	přerušení	k1gNnPc2	přerušení
nevyžadujících	vyžadující	k2eNgNnPc2d1	nevyžadující
extrémně	extrémně	k6eAd1	extrémně
krátké	krátký	k2eAgFnSc2d1	krátká
doby	doba	k1gFnSc2	doba
odezvy	odezva	k1gFnSc2	odezva
nebo	nebo	k8xC	nebo
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlastní	vlastní	k2eAgFnSc1d1	vlastní
obsluha	obsluha	k1gFnSc1	obsluha
přerušení	přerušení	k1gNnSc2	přerušení
je	být	k5eAaImIp3nS	být
oproti	oproti	k7c3	oproti
době	doba	k1gFnSc3	doba
reakce	reakce	k1gFnSc2	reakce
procesoru	procesor	k1gInSc2	procesor
mnohonásobně	mnohonásobně	k6eAd1	mnohonásobně
delší	dlouhý	k2eAgFnSc1d2	delší
<g/>
.	.	kIx.	.
</s>
<s>
Procesor	procesor	k1gInSc1	procesor
ARM	ARM	kA	ARM
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
44	[number]	k4	44
základních	základní	k2eAgFnPc2d1	základní
instrukcí	instrukce	k1gFnPc2	instrukce
s	s	k7c7	s
jednotnou	jednotný	k2eAgFnSc7d1	jednotná
šířkou	šířka	k1gFnSc7	šířka
32	[number]	k4	32
bitů	bit	k1gInPc2	bit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
taktu	takt	k1gInSc6	takt
se	se	k3xPyFc4	se
vykonávají	vykonávat	k5eAaImIp3nP	vykonávat
pouze	pouze	k6eAd1	pouze
instrukce	instrukce	k1gFnPc4	instrukce
pracující	pracující	k2eAgFnPc4d1	pracující
s	s	k7c7	s
aritmeticko-logickou	aritmetickoogický	k2eAgFnSc7d1	aritmeticko-logická
jednotkou	jednotka	k1gFnSc7	jednotka
(	(	kIx(	(
<g/>
ALU	ala	k1gFnSc4	ala
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
registry	registrum	k1gNnPc7	registrum
nebo	nebo	k8xC	nebo
s	s	k7c7	s
přímými	přímý	k2eAgInPc7d1	přímý
operandy	operand	k1gInPc7	operand
<g/>
.	.	kIx.	.
</s>
<s>
Procesor	procesor	k1gInSc1	procesor
pracuje	pracovat	k5eAaImIp3nS	pracovat
ve	v	k7c6	v
čtyřech	čtyři	k4xCgInPc6	čtyři
základních	základní	k2eAgInPc6d1	základní
režimech	režim	k1gInPc6	režim
<g/>
:	:	kIx,	:
uživatelský	uživatelský	k2eAgInSc1d1	uživatelský
režim	režim	k1gInSc1	režim
USR	USR	kA	USR
privilegovaný	privilegovaný	k2eAgInSc1d1	privilegovaný
režim	režim	k1gInSc1	režim
supervizora	supervizor	k1gMnSc2	supervizor
SUP	sup	k1gMnSc1	sup
privilegovaný	privilegovaný	k2eAgInSc1d1	privilegovaný
režim	režim	k1gInSc1	režim
přerušení	přerušení	k1gNnSc2	přerušení
IRQ	IRQ	kA	IRQ
privilegovaný	privilegovaný	k2eAgInSc4d1	privilegovaný
režim	režim	k1gInSc4	režim
rychlého	rychlý	k2eAgNnSc2d1	rychlé
přerušení	přerušení	k1gNnSc2	přerušení
FIQ	FIQ	kA	FIQ
V	v	k7c6	v
procesoru	procesor	k1gInSc6	procesor
je	být	k5eAaImIp3nS	být
obsaženo	obsáhnout	k5eAaPmNgNnS	obsáhnout
25	[number]	k4	25
částečně	částečně	k6eAd1	částečně
se	se	k3xPyFc4	se
překrývajících	překrývající	k2eAgInPc2d1	překrývající
32	[number]	k4	32
<g/>
bitových	bitový	k2eAgInPc2d1	bitový
registrů	registr	k1gInPc2	registr
(	(	kIx(	(
<g/>
15	[number]	k4	15
registrů	registr	k1gInPc2	registr
je	být	k5eAaImIp3nS	být
univerzálních	univerzální	k2eAgNnPc2d1	univerzální
a	a	k8xC	a
zbývajících	zbývající	k2eAgNnPc2d1	zbývající
10	[number]	k4	10
má	mít	k5eAaImIp3nS	mít
speciální	speciální	k2eAgFnSc1d1	speciální
funkce	funkce	k1gFnSc1	funkce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
16	[number]	k4	16
registrů	registr	k1gInPc2	registr
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
režimu	režim	k1gInSc6	režim
činnosti	činnost	k1gFnSc2	činnost
programově	programově	k6eAd1	programově
přístupných	přístupný	k2eAgFnPc2d1	přístupná
<g/>
.	.	kIx.	.
</s>
<s>
Registry	registr	k1gInPc1	registr
R0	R0	k1gFnSc2	R0
až	až	k9	až
R13	R13	k1gFnPc1	R13
jsou	být	k5eAaImIp3nP	být
přístupné	přístupný	k2eAgFnPc1d1	přístupná
v	v	k7c6	v
uživatelském	uživatelský	k2eAgInSc6d1	uživatelský
režimu	režim	k1gInSc6	režim
pro	pro	k7c4	pro
libovolný	libovolný	k2eAgInSc4d1	libovolný
účel	účel	k1gInSc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Registr	registr	k1gInSc1	registr
R14	R14	k1gFnSc2	R14
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
výhradně	výhradně	k6eAd1	výhradně
pro	pro	k7c4	pro
uživatelský	uživatelský	k2eAgInSc4d1	uživatelský
režim	režim	k1gInSc4	režim
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
registrů	registr	k1gInPc2	registr
je	být	k5eAaImIp3nS	být
speciálně	speciálně	k6eAd1	speciálně
určeno	určit	k5eAaPmNgNnS	určit
pro	pro	k7c4	pro
režim	režim	k1gInSc4	režim
rychlého	rychlý	k2eAgNnSc2d1	rychlé
přerušení	přerušení	k1gNnSc2	přerušení
(	(	kIx(	(
<g/>
FIQ	FIQ	kA	FIQ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
procesor	procesor	k1gInSc1	procesor
v	v	k7c6	v
režimu	režim	k1gInSc6	režim
FIQ	FIQ	kA	FIQ
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
těchto	tento	k3xDgInPc2	tento
pět	pět	k4xCc4	pět
registrů	registr	k1gInPc2	registr
mapováno	mapován	k2eAgNnSc4d1	mapováno
do	do	k7c2	do
registrů	registr	k1gInPc2	registr
R10	R10	k1gFnSc2	R10
až	až	k9	až
R	R	kA	R
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
registru	registr	k1gInSc6	registr
R14	R14	k1gFnSc2	R14
je	být	k5eAaImIp3nS	být
uložena	uložen	k2eAgFnSc1d1	uložena
návratová	návratový	k2eAgFnSc1d1	návratová
adresa	adresa	k1gFnSc1	adresa
do	do	k7c2	do
přerušeného	přerušený	k2eAgInSc2d1	přerušený
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
dva	dva	k4xCgInPc1	dva
registry	registr	k1gInPc1	registr
jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgInP	určit
pro	pro	k7c4	pro
režim	režim	k1gInSc4	režim
přerušení	přerušení	k1gNnSc2	přerušení
(	(	kIx(	(
<g/>
IRQ	IRQ	kA	IRQ
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
režimu	režim	k1gInSc6	režim
překrývají	překrývat	k5eAaImIp3nP	překrývat
registry	registr	k1gInPc1	registr
R13	R13	k1gMnPc2	R13
a	a	k8xC	a
R14	R14	k1gMnPc2	R14
uživatelského	uživatelský	k2eAgInSc2d1	uživatelský
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
dva	dva	k4xCgInPc1	dva
registry	registr	k1gInPc1	registr
jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgInP	určit
pro	pro	k7c4	pro
privilegovaný	privilegovaný	k2eAgInSc4d1	privilegovaný
režim	režim	k1gInSc4	režim
supervizora	supervizor	k1gMnSc2	supervizor
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
registr	registr	k1gInSc1	registr
R15	R15	k1gFnSc2	R15
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
stavové	stavový	k2eAgNnSc4d1	stavové
slovo	slovo	k1gNnSc4	slovo
procesoru	procesor	k1gInSc2	procesor
a	a	k8xC	a
čítač	čítač	k1gInSc4	čítač
instrukcí	instrukce	k1gFnPc2	instrukce
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
sdílen	sdílet	k5eAaImNgInS	sdílet
všemi	všecek	k3xTgFnPc7	všecek
režimy	režim	k1gInPc4	režim
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Významově	významově	k6eAd1	významově
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
6	[number]	k4	6
bitů	bit	k1gInPc2	bit
PSW	PSW	kA	PSW
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
stav	stav	k1gInSc4	stav
procesoru	procesor	k1gInSc2	procesor
<g/>
,	,	kIx,	,
dalších	další	k2eAgInPc2d1	další
24	[number]	k4	24
bitů	bit	k1gInPc2	bit
představuje	představovat	k5eAaImIp3nS	představovat
čítač	čítač	k1gInSc1	čítač
instrukcí	instrukce	k1gFnPc2	instrukce
a	a	k8xC	a
významově	významově	k6eAd1	významově
nejnižší	nízký	k2eAgInPc1d3	nejnižší
dva	dva	k4xCgInPc1	dva
bity	bit	k1gInPc1	bit
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
aktuální	aktuální	k2eAgInSc4d1	aktuální
režim	režim	k1gInSc4	režim
činnosti	činnost	k1gFnSc2	činnost
procesoru	procesor	k1gInSc2	procesor
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
bylo	být	k5eAaImAgNnS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
<g/>
,	,	kIx,	,
procesor	procesor	k1gInSc1	procesor
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
množinu	množina	k1gFnSc4	množina
částečně	částečně	k6eAd1	částečně
se	se	k3xPyFc4	se
překrývajících	překrývající	k2eAgInPc2d1	překrývající
registrů	registr	k1gInPc2	registr
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
v	v	k7c6	v
případě	případ	k1gInSc6	případ
přerušení	přerušení	k1gNnSc2	přerušení
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
proveden	provést	k5eAaPmNgInS	provést
kompletní	kompletní	k2eAgInSc1d1	kompletní
úklid	úklid	k1gInSc1	úklid
registrů	registr	k1gInPc2	registr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
režimu	režim	k1gInSc2	režim
rychlého	rychlý	k2eAgNnSc2d1	rychlé
přerušení	přerušení	k1gNnSc2	přerušení
FIQ	FIQ	kA	FIQ
je	být	k5eAaImIp3nS	být
zkrácení	zkrácení	k1gNnSc4	zkrácení
doby	doba	k1gFnSc2	doba
odezvy	odezva	k1gFnSc2	odezva
procesoru	procesor	k1gInSc2	procesor
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
použitím	použití	k1gNnSc7	použití
čtyř	čtyři	k4xCgInPc2	čtyři
lokálních	lokální	k2eAgInPc2d1	lokální
univerzálních	univerzální	k2eAgInPc2d1	univerzální
registrů	registr	k1gInPc2	registr
a	a	k8xC	a
jednoho	jeden	k4xCgInSc2	jeden
registru	registr	k1gInSc2	registr
s	s	k7c7	s
návratovou	návratový	k2eAgFnSc7d1	návratová
adresou	adresa	k1gFnSc7	adresa
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
registry	registr	k1gInPc1	registr
mohou	moct	k5eAaImIp3nP	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
všechny	všechen	k3xTgInPc4	všechen
ukazatele	ukazatel	k1gInPc4	ukazatel
a	a	k8xC	a
různé	různý	k2eAgInPc4d1	různý
čítače	čítač	k1gInPc4	čítač
používané	používaný	k2eAgInPc4d1	používaný
v	v	k7c6	v
jednoduchých	jednoduchý	k2eAgFnPc6d1	jednoduchá
procedurách	procedura	k1gFnPc6	procedura
obsluhy	obsluha	k1gFnSc2	obsluha
vstupů	vstup	k1gInPc2	vstup
a	a	k8xC	a
výstupů	výstup	k1gInPc2	výstup
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
lze	lze	k6eAd1	lze
dobře	dobře	k6eAd1	dobře
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
velmi	velmi	k6eAd1	velmi
rychlého	rychlý	k2eAgNnSc2d1	rychlé
opakovaného	opakovaný	k2eAgNnSc2d1	opakované
přepínání	přepínání	k1gNnSc2	přepínání
procesoru	procesor	k1gInSc2	procesor
mezi	mezi	k7c7	mezi
uživatelským	uživatelský	k2eAgInSc7d1	uživatelský
režimem	režim	k1gInSc7	režim
a	a	k8xC	a
režimem	režim	k1gInSc7	režim
"	"	kIx"	"
<g/>
rychlého	rychlý	k2eAgNnSc2d1	rychlé
přerušení	přerušení	k1gNnSc2	přerušení
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Procesor	procesor	k1gInSc1	procesor
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
schopen	schopen	k2eAgMnSc1d1	schopen
obsloužit	obsloužit	k5eAaPmF	obsloužit
následující	následující	k2eAgNnSc4d1	následující
přerušení	přerušení	k1gNnSc4	přerušení
<g/>
:	:	kIx,	:
chyba	chyba	k1gFnSc1	chyba
v	v	k7c6	v
adresování	adresování	k1gNnSc6	adresování
(	(	kIx(	(
<g/>
Address	Address	k1gInSc1	Address
Exception	Exception	k1gInSc1	Exception
Trap	trap	k1gInSc1	trap
<g/>
)	)	kIx)	)
chyba	chyba	k1gFnSc1	chyba
při	při	k7c6	při
čtení	čtení	k1gNnSc6	čtení
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zápisu	zápis	k1gInSc3	zápis
dat	datum	k1gNnPc2	datum
do	do	k7c2	do
vnější	vnější	k2eAgFnSc2d1	vnější
paměti	paměť	k1gFnSc2	paměť
(	(	kIx(	(
<g/>
Data	datum	k1gNnPc1	datum
Fetch	Fetcha	k1gFnPc2	Fetcha
Cyrcle	Cyrcle	k1gFnSc1	Cyrcle
Aborts	Aborts	k1gInSc1	Aborts
<g/>
)	)	kIx)	)
chyba	chyba	k1gFnSc1	chyba
při	při	k7c6	při
čtení	čtení	k1gNnSc6	čtení
instrukce	instrukce	k1gFnSc2	instrukce
z	z	k7c2	z
vnější	vnější	k2eAgFnSc2d1	vnější
paměti	paměť	k1gFnSc2	paměť
(	(	kIx(	(
<g/>
Instruction	Instruction	k1gInSc1	Instruction
Fetch	Fetcha	k1gFnPc2	Fetcha
Cyrcle	Cyrcle	k1gFnSc2	Cyrcle
Aborts	Aborts	k1gInSc1	Aborts
<g/>
)	)	kIx)	)
přerušení	přerušení	k1gNnSc1	přerušení
programovými	programový	k2eAgInPc7d1	programový
prostředky	prostředek	k1gInPc7	prostředek
(	(	kIx(	(
<g/>
instrukce	instrukce	k1gFnSc1	instrukce
SWI	SWI	kA	SWI
<g/>
)	)	kIx)	)
nedefinovaný	definovaný	k2eNgInSc4d1	nedefinovaný
kód	kód	k1gInSc4	kód
instrukcí	instrukce	k1gFnPc2	instrukce
(	(	kIx(	(
<g/>
Undefined	Undefined	k1gInSc1	Undefined
Instruction	Instruction	k1gInSc1	Instruction
Traps	Traps	k1gInSc4	Traps
<g/>
)	)	kIx)	)
reset	reset	k1gInSc4	reset
procesoru	procesor	k1gInSc2	procesor
Procesor	procesor	k1gInSc1	procesor
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
26	[number]	k4	26
<g/>
bitovou	bitový	k2eAgFnSc4d1	bitová
adresu	adresa	k1gFnSc4	adresa
lineární	lineární	k2eAgFnSc2d1	lineární
operační	operační	k2eAgFnSc2d1	operační
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
adresovat	adresovat	k5eAaBmF	adresovat
64	[number]	k4	64
MiB	MiB	k1gFnPc4	MiB
fyzické	fyzický	k2eAgFnSc2d1	fyzická
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Odkaz	odkaz	k1gInSc1	odkaz
na	na	k7c4	na
data	datum	k1gNnPc4	datum
mimo	mimo	k7c4	mimo
rozsah	rozsah	k1gInSc4	rozsah
způsobí	způsobit	k5eAaPmIp3nS	způsobit
přerušení	přerušení	k1gNnSc1	přerušení
chyby	chyba	k1gFnSc2	chyba
adresování	adresování	k1gNnSc2	adresování
(	(	kIx(	(
<g/>
Address	Address	k1gInSc1	Address
Exception	Exception	k1gInSc1	Exception
Trap	trap	k1gInSc1	trap
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Instrukční	instrukční	k2eAgInSc4d1	instrukční
soubor	soubor	k1gInSc4	soubor
můžeme	moct	k5eAaImIp1nP	moct
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
skupiny	skupina	k1gFnPc4	skupina
<g/>
:	:	kIx,	:
instrukce	instrukce	k1gFnSc1	instrukce
zpracování	zpracování	k1gNnSc2	zpracování
údajů	údaj	k1gInPc2	údaj
-	-	kIx~	-
zpracování	zpracování	k1gNnSc2	zpracování
registrových	registrový	k2eAgInPc2d1	registrový
operandů	operand	k1gInPc2	operand
<g/>
,	,	kIx,	,
zpracování	zpracování	k1gNnSc3	zpracování
přímých	přímý	k2eAgInPc2d1	přímý
operandů	operand	k1gInPc2	operand
<g/>
,	,	kIx,	,
nastavení	nastavení	k1gNnSc1	nastavení
podmínkového	podmínkový	k2eAgInSc2d1	podmínkový
kódu	kód	k1gInSc2	kód
a	a	k8xC	a
instrukce	instrukce	k1gFnPc4	instrukce
aritmeticko-logické	aritmetickoogický	k2eAgFnPc4d1	aritmeticko-logická
instrukce	instrukce	k1gFnPc4	instrukce
jednoduchého	jednoduchý	k2eAgInSc2d1	jednoduchý
přenosu	přenos	k1gInSc2	přenos
údajů	údaj	k1gInPc2	údaj
instrukce	instrukce	k1gFnSc2	instrukce
blokového	blokový	k2eAgInSc2d1	blokový
přenosu	přenos	k1gInSc2	přenos
údajů	údaj	k1gInPc2	údaj
-	-	kIx~	-
instrukce	instrukce	k1gFnPc1	instrukce
vykonávají	vykonávat	k5eAaImIp3nP	vykonávat
přenos	přenos	k1gInSc4	přenos
mezi	mezi	k7c7	mezi
skupinou	skupina	k1gFnSc7	skupina
registrů	registr	k1gInPc2	registr
a	a	k8xC	a
spojitou	spojitý	k2eAgFnSc7d1	spojitá
oblastí	oblast	k1gFnSc7	oblast
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
<g />
.	.	kIx.	.
</s>
<s>
jeden	jeden	k4xCgInSc1	jeden
registr	registr	k1gInSc1	registr
je	být	k5eAaImIp3nS	být
použit	použít	k5eAaPmNgInS	použít
jako	jako	k9	jako
směrník	směrník	k1gInSc4	směrník
instrukce	instrukce	k1gFnSc1	instrukce
větvení	větvení	k1gNnPc2	větvení
a	a	k8xC	a
větvení	větvení	k1gNnPc2	větvení
s	s	k7c7	s
uchováním	uchování	k1gNnSc7	uchování
návratové	návratový	k2eAgFnSc2d1	návratová
adresy	adresa	k1gFnSc2	adresa
(	(	kIx(	(
<g/>
tyto	tento	k3xDgFnPc1	tento
instrukce	instrukce	k1gFnPc1	instrukce
odkládají	odkládat	k5eAaImIp3nP	odkládat
PSW	PSW	kA	PSW
do	do	k7c2	do
R	R	kA	R
<g/>
14	[number]	k4	14
<g/>
)	)	kIx)	)
instrukce	instrukce	k1gFnSc1	instrukce
přechodu	přechod	k1gInSc2	přechod
do	do	k7c2	do
privilegovaného	privilegovaný	k2eAgInSc2d1	privilegovaný
režimu	režim	k1gInSc2	režim
supervizora	supervizor	k1gMnSc2	supervizor
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
i	i	k9	i
programové	programový	k2eAgNnSc1d1	programové
přerušení	přerušení	k1gNnSc1	přerušení
Všechny	všechen	k3xTgFnPc4	všechen
instrukce	instrukce	k1gFnPc1	instrukce
mají	mít	k5eAaImIp3nP	mít
čtyřbitový	čtyřbitový	k2eAgInSc4d1	čtyřbitový
prefix	prefix	k1gInSc4	prefix
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
podmíněné	podmíněný	k2eAgNnSc4d1	podmíněné
vykonání	vykonání	k1gNnSc4	vykonání
samotné	samotný	k2eAgFnSc2d1	samotná
instrukce	instrukce	k1gFnSc2	instrukce
<g/>
.	.	kIx.	.
</s>
<s>
Instrukce	instrukce	k1gFnSc1	instrukce
zpracování	zpracování	k1gNnSc2	zpracování
údajů	údaj	k1gInPc2	údaj
pracují	pracovat	k5eAaImIp3nP	pracovat
pouze	pouze	k6eAd1	pouze
nad	nad	k7c7	nad
souborem	soubor	k1gInSc7	soubor
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
registrů	registr	k1gInPc2	registr
a	a	k8xC	a
každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
instrukcí	instrukce	k1gFnPc2	instrukce
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
reference	reference	k1gFnPc4	reference
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
operandy	operand	k1gInPc4	operand
<g/>
:	:	kIx,	:
jeden	jeden	k4xCgInSc4	jeden
cílový	cílový	k2eAgInSc4d1	cílový
a	a	k8xC	a
dva	dva	k4xCgInPc1	dva
zdrojové	zdrojový	k2eAgInPc1d1	zdrojový
<g/>
.	.	kIx.	.
</s>
<s>
Instrukce	instrukce	k1gFnSc1	instrukce
přenosu	přenos	k1gInSc2	přenos
údajů	údaj	k1gInPc2	údaj
jsou	být	k5eAaImIp3nP	být
použity	použít	k5eAaPmNgInP	použít
k	k	k7c3	k
přenosu	přenos	k1gInSc3	přenos
dat	datum	k1gNnPc2	datum
mezi	mezi	k7c7	mezi
pamětí	paměť	k1gFnSc7	paměť
a	a	k8xC	a
souborem	soubor	k1gInSc7	soubor
registrů	registr	k1gInPc2	registr
(	(	kIx(	(
<g/>
Load	Load	k1gInSc1	Load
<g/>
)	)	kIx)	)
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
(	(	kIx(	(
<g/>
Store	Stor	k1gInSc5	Stor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Efektivní	efektivní	k2eAgFnSc1d1	efektivní
adresa	adresa	k1gFnSc1	adresa
je	být	k5eAaImIp3nS	být
vypočtena	vypočíst	k5eAaPmNgFnS	vypočíst
součtem	součet	k1gInSc7	součet
obsahu	obsah	k1gInSc2	obsah
zdrojového	zdrojový	k2eAgInSc2d1	zdrojový
registru	registr	k1gInSc2	registr
a	a	k8xC	a
posuvu	posuv	k1gInSc2	posuv
(	(	kIx(	(
<g/>
offset	offset	k1gInSc1	offset
<g/>
)	)	kIx)	)
daného	daný	k2eAgInSc2d1	daný
12	[number]	k4	12
<g/>
bitovou	bitový	k2eAgFnSc7d1	bitová
konstantou	konstanta	k1gFnSc7	konstanta
nebo	nebo	k8xC	nebo
obsahem	obsah	k1gInSc7	obsah
dalšího	další	k2eAgInSc2d1	další
registru	registr	k1gInSc2	registr
<g/>
.	.	kIx.	.
</s>
<s>
Posuv	posuv	k1gInSc1	posuv
(	(	kIx(	(
<g/>
offset	offset	k1gInSc1	offset
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
přičítán	přičítat	k5eAaImNgMnS	přičítat
k	k	k7c3	k
indexovému	indexový	k2eAgInSc3d1	indexový
registru	registr	k1gInSc3	registr
nebo	nebo	k8xC	nebo
od	od	k7c2	od
něj	on	k3xPp3gInSc2	on
odčítán	odčítán	k2eAgInSc1d1	odčítán
<g/>
.	.	kIx.	.
</s>
<s>
Instrukce	instrukce	k1gFnSc1	instrukce
přenosu	přenos	k1gInSc2	přenos
údajů	údaj	k1gInPc2	údaj
mohou	moct	k5eAaImIp3nP	moct
pracovat	pracovat	k5eAaImF	pracovat
jak	jak	k6eAd1	jak
s	s	k7c7	s
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
byty	byt	k1gInPc7	byt
<g/>
,	,	kIx,	,
tak	tak	k9	tak
s	s	k7c7	s
dvojitým	dvojitý	k2eAgNnSc7d1	dvojité
slovem	slovo	k1gNnSc7	slovo
(	(	kIx(	(
<g/>
Double	double	k2eAgFnSc7d1	double
Word	Word	kA	Word
<g/>
,	,	kIx,	,
32	[number]	k4	32
<g/>
bitů	bit	k1gInPc2	bit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byte	byte	k1gInSc1	byte
přečtený	přečtený	k2eAgInSc1d1	přečtený
z	z	k7c2	z
paměti	paměť	k1gFnSc2	paměť
je	být	k5eAaImIp3nS	být
uložen	uložen	k2eAgInSc1d1	uložen
do	do	k7c2	do
významově	významově	k6eAd1	významově
nejnižších	nízký	k2eAgInPc2d3	nejnižší
8	[number]	k4	8
<g/>
bitů	bit	k1gInPc2	bit
cílového	cílový	k2eAgInSc2d1	cílový
registru	registr	k1gInSc2	registr
a	a	k8xC	a
zbytek	zbytek	k1gInSc1	zbytek
obsahu	obsah	k1gInSc2	obsah
je	být	k5eAaImIp3nS	být
zaplněn	zaplnit	k5eAaPmNgMnS	zaplnit
nulami	nula	k1gFnPc7	nula
<g/>
.	.	kIx.	.
</s>
<s>
Instrukce	instrukce	k1gFnSc1	instrukce
blokového	blokový	k2eAgInSc2d1	blokový
přenosu	přenos	k1gInSc2	přenos
zabezpečují	zabezpečovat	k5eAaImIp3nP	zabezpečovat
přenos	přenos	k1gInSc4	přenos
několika	několik	k4yIc2	několik
registrů	registr	k1gInPc2	registr
jednou	jednou	k6eAd1	jednou
instrukcí	instrukce	k1gFnSc7	instrukce
<g/>
.	.	kIx.	.
</s>
<s>
Instrukce	instrukce	k1gFnSc1	instrukce
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
pole	pole	k1gNnSc4	pole
bitů	bit	k1gInPc2	bit
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
každý	každý	k3xTgMnSc1	každý
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
jednomu	jeden	k4xCgNnSc3	jeden
registru	registr	k1gInSc3	registr
viditelnému	viditelný	k2eAgInSc3d1	viditelný
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
režimu	režim	k1gInSc6	režim
<g/>
.	.	kIx.	.
</s>
<s>
Bit	bit	k1gInSc1	bit
0	[number]	k4	0
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
R	R	kA	R
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
bit	bit	k1gInSc1	bit
1	[number]	k4	1
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
R	R	kA	R
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
Instrukce	instrukce	k1gFnSc1	instrukce
programového	programový	k2eAgNnSc2d1	programové
přerušení	přerušení	k1gNnSc2	přerušení
jsou	být	k5eAaImIp3nP	být
použity	použít	k5eAaPmNgFnP	použít
především	především	k9	především
pro	pro	k7c4	pro
přechod	přechod	k1gInSc4	přechod
do	do	k7c2	do
privilegovaného	privilegovaný	k2eAgInSc2d1	privilegovaný
režimu	režim	k1gInSc2	režim
supervizora	supervizor	k1gMnSc4	supervizor
<g/>
.	.	kIx.	.
</s>
<s>
PSW	PSW	kA	PSW
je	být	k5eAaImIp3nS	být
ukládáno	ukládat	k5eAaImNgNnS	ukládat
do	do	k7c2	do
registru	registr	k1gInSc2	registr
R14	R14	k1gMnSc2	R14
režimu	režim	k1gInSc2	režim
supervizora	supervizor	k1gMnSc2	supervizor
a	a	k8xC	a
hodnota	hodnota	k1gFnSc1	hodnota
čítače	čítač	k1gInSc2	čítač
instrukcí	instrukce	k1gFnPc2	instrukce
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
adresou	adresa	k1gFnSc7	adresa
softwarového	softwarový	k2eAgNnSc2d1	softwarové
přerušení	přerušení	k1gNnSc2	přerušení
<g/>
.	.	kIx.	.
</s>
<s>
LIČEV	LIČEV	kA	LIČEV
<g/>
,	,	kIx,	,
Lačezar	Lačezar	k1gMnSc1	Lačezar
<g/>
;	;	kIx,	;
MORKES	MORKES	kA	MORKES
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
<g/>
.	.	kIx.	.
</s>
<s>
Procesory	procesor	k1gInPc1	procesor
-	-	kIx~	-
architektura	architektura	k1gFnSc1	architektura
<g/>
,	,	kIx,	,
funkce	funkce	k1gFnSc1	funkce
<g/>
,	,	kIx,	,
použití	použití	k1gNnSc1	použití
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Computer	computer	k1gInSc1	computer
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7226	[number]	k4	7226
<g/>
-	-	kIx~	-
<g/>
172	[number]	k4	172
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
VÁŇA	Váňa	k1gMnSc1	Váňa
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
.	.	kIx.	.
</s>
<s>
ARM	ARM	kA	ARM
pro	pro	k7c4	pro
začátečníky	začátečník	k1gMnPc4	začátečník
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
BEN	Ben	k1gInSc1	Ben
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7300	[number]	k4	7300
<g/>
-	-	kIx~	-
<g/>
246	[number]	k4	246
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
ARM	ARM	kA	ARM
big	big	k?	big
<g/>
.	.	kIx.	.
<g/>
LITTLE	LITTLE	kA	LITTLE
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
ARM	ARM	kA	ARM
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
http://pandatron.cz/?542&	[url]	k?	http://pandatron.cz/?542&
http://pandatron.cz/?606&	[url]	k?	http://pandatron.cz/?606&
</s>
