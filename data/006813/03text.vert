<s>
Medvěd	medvěd	k1gMnSc1	medvěd
lední	lední	k2eAgMnSc1d1	lední
(	(	kIx(	(
<g/>
Ursus	Ursus	k1gMnSc1	Ursus
maritimus	maritimus	k1gMnSc1	maritimus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
označovaný	označovaný	k2eAgInSc1d1	označovaný
též	též	k9	též
jako	jako	k9	jako
polární	polární	k2eAgMnSc1d1	polární
medvěd	medvěd	k1gMnSc1	medvěd
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc1d1	velký
druh	druh	k1gInSc1	druh
medvěda	medvěd	k1gMnSc2	medvěd
typický	typický	k2eAgInSc1d1	typický
pro	pro	k7c4	pro
severní	severní	k2eAgFnPc4d1	severní
polární	polární	k2eAgFnPc4d1	polární
oblasti	oblast	k1gFnPc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kohoutku	kohoutek	k1gInSc6	kohoutek
měří	měřit	k5eAaImIp3nS	měřit
1,5	[number]	k4	1,5
m	m	kA	m
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
činí	činit	k5eAaImIp3nS	činit
přibližně	přibližně	k6eAd1	přibližně
2,5	[number]	k4	2,5
m	m	kA	m
a	a	k8xC	a
výška	výška	k1gFnSc1	výška
vzpřímeného	vzpřímený	k2eAgNnSc2d1	vzpřímené
těla	tělo	k1gNnSc2	tělo
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
2,4	[number]	k4	2,4
<g/>
–	–	k?	–
<g/>
3,3	[number]	k4	3,3
m.	m.	k?	m.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
váží	vážit	k5eAaImIp3nP	vážit
300	[number]	k4	300
<g/>
–	–	k?	–
<g/>
800	[number]	k4	800
kg	kg	kA	kg
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
150	[number]	k4	150
<g/>
–	–	k?	–
<g/>
300	[number]	k4	300
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Dožívají	dožívat	k5eAaImIp3nP	dožívat
se	s	k7c7	s
25	[number]	k4	25
až	až	k9	až
30	[number]	k4	30
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
rekord	rekord	k1gInSc1	rekord
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
je	být	k5eAaImIp3nS	být
43	[number]	k4	43
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
lovem	lov	k1gInSc7	lov
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jeho	jeho	k3xOp3gFnSc7	jeho
hlavní	hlavní	k2eAgFnSc7d1	hlavní
kořistí	kořist	k1gFnSc7	kořist
jsou	být	k5eAaImIp3nP	být
tuleni	tuleň	k1gMnPc1	tuleň
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
Grónsku	Grónsko	k1gNnSc6	Grónsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
severním	severní	k2eAgNnSc6d1	severní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
všech	všecek	k3xTgInPc6	všecek
těchto	tento	k3xDgNnPc6	tento
územích	území	k1gNnPc6	území
je	být	k5eAaImIp3nS	být
chráněn	chráněn	k2eAgInSc4d1	chráněn
a	a	k8xC	a
jeho	on	k3xPp3gInSc4	on
lov	lov	k1gInSc4	lov
se	se	k3xPyFc4	se
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
žije	žít	k5eAaImIp3nS	žít
asi	asi	k9	asi
20	[number]	k4	20
000	[number]	k4	000
-	-	kIx~	-
25	[number]	k4	25
000	[number]	k4	000
těchto	tento	k3xDgMnPc2	tento
medvědů	medvěd	k1gMnPc2	medvěd
<g/>
.	.	kIx.	.
</s>
<s>
Lední	lední	k2eAgMnPc1d1	lední
medvědi	medvěd	k1gMnPc1	medvěd
žijí	žít	k5eAaImIp3nP	žít
většinou	většinou	k6eAd1	většinou
samotářsky	samotářsky	k6eAd1	samotářsky
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
dne	den	k1gInSc2	den
hledají	hledat	k5eAaImIp3nP	hledat
potravu	potrava	k1gFnSc4	potrava
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yIgFnSc4	který
jsou	být	k5eAaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
putovat	putovat	k5eAaImF	putovat
i	i	k9	i
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
kilometrů	kilometr	k1gInPc2	kilometr
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
obratní	obratní	k2eAgMnPc1d1	obratní
<g/>
,	,	kIx,	,
silní	silný	k2eAgMnPc1d1	silný
a	a	k8xC	a
mrštní	mrštný	k2eAgMnPc1d1	mrštný
<g/>
.	.	kIx.	.
</s>
<s>
Dokážou	dokázat	k5eAaPmIp3nP	dokázat
vyšplhat	vyšplhat	k5eAaPmF	vyšplhat
na	na	k7c4	na
téměř	téměř	k6eAd1	téměř
kolmé	kolmý	k2eAgFnPc4d1	kolmá
kry	kra	k1gFnPc4	kra
a	a	k8xC	a
dovedou	dovést	k5eAaPmIp3nP	dovést
přeskočit	přeskočit	k5eAaPmF	přeskočit
až	až	k9	až
čtyřmetrové	čtyřmetrový	k2eAgFnSc2d1	čtyřmetrová
díry	díra	k1gFnSc2	díra
v	v	k7c6	v
ledu	led	k1gInSc6	led
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
velmi	velmi	k6eAd1	velmi
zdatní	zdatný	k2eAgMnPc1d1	zdatný
plavci	plavec	k1gMnPc1	plavec
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgMnPc1d3	nejlepší
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
medvědů	medvěd	k1gMnPc2	medvěd
<g/>
.	.	kIx.	.
</s>
<s>
Dokáží	dokázat	k5eAaPmIp3nP	dokázat
v	v	k7c6	v
kuse	kus	k1gInSc6	kus
plavat	plavat	k5eAaImF	plavat
až	až	k9	až
10	[number]	k4	10
dní	den	k1gInPc2	den
a	a	k8xC	a
urazit	urazit	k5eAaPmF	urazit
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
stovky	stovka	k1gFnPc1	stovka
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Umějí	umět	k5eAaImIp3nP	umět
se	se	k3xPyFc4	se
také	také	k9	také
potápět	potápět	k5eAaImF	potápět
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
vydrží	vydržet	k5eAaPmIp3nS	vydržet
až	až	k9	až
tři	tři	k4xCgFnPc4	tři
minuty	minuta	k1gFnPc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nP	živit
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
lovem	lov	k1gInSc7	lov
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc7	jejich
nejčastější	častý	k2eAgFnSc7d3	nejčastější
kořistí	kořist	k1gFnSc7	kořist
bývají	bývat	k5eAaImIp3nP	bývat
tuleni	tuleň	k1gMnPc1	tuleň
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ledního	lední	k2eAgMnSc4d1	lední
medvěda	medvěd	k1gMnSc4	medvěd
je	být	k5eAaImIp3nS	být
snadné	snadný	k2eAgNnSc1d1	snadné
vypátrat	vypátrat	k5eAaPmF	vypátrat
doupata	doupě	k1gNnPc4	doupě
tuleňů	tuleň	k1gMnPc2	tuleň
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
vyvinutý	vyvinutý	k2eAgInSc4d1	vyvinutý
čich	čich	k1gInSc4	čich
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
ucítit	ucítit	k5eAaPmF	ucítit
pach	pach	k1gInSc1	pach
mrtvé	mrtvý	k2eAgFnSc2d1	mrtvá
velryby	velryba	k1gFnSc2	velryba
i	i	k9	i
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
několika	několik	k4yIc2	několik
kilometrů	kilometr	k1gInPc2	kilometr
a	a	k8xC	a
doupata	doupě	k1gNnPc1	doupě
tuleňů	tuleň	k1gMnPc2	tuleň
najdou	najít	k5eAaPmIp3nP	najít
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jeden	jeden	k4xCgInSc4	jeden
metr	metr	k1gInSc4	metr
pod	pod	k7c7	pod
sněhem	sníh	k1gInSc7	sníh
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgFnPc1	svůj
tulení	tulení	k2eAgFnPc1d1	tulení
oběti	oběť	k1gFnPc1	oběť
zabíjí	zabíjet	k5eAaImIp3nP	zabíjet
jediným	jediný	k2eAgInSc7d1	jediný
mohutným	mohutný	k2eAgInSc7d1	mohutný
úderem	úder	k1gInSc7	úder
do	do	k7c2	do
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
kořisti	kořist	k1gFnSc2	kořist
zkonzumuje	zkonzumovat	k5eAaPmIp3nS	zkonzumovat
kůži	kůže	k1gFnSc4	kůže
<g/>
,	,	kIx,	,
tuk	tuk	k1gInSc4	tuk
a	a	k8xC	a
vnitřnosti	vnitřnost	k1gFnPc4	vnitřnost
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
letních	letní	k2eAgInPc2d1	letní
měsíců	měsíc	k1gInPc2	měsíc
žere	žrát	k5eAaImIp3nS	žrát
dokonce	dokonce	k9	dokonce
i	i	k9	i
různé	různý	k2eAgFnPc4d1	různá
bobule	bobule	k1gFnPc4	bobule
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
uloví	ulovit	k5eAaPmIp3nS	ulovit
i	i	k9	i
nějaké	nějaký	k3yIgMnPc4	nějaký
suchozemské	suchozemský	k2eAgMnPc4d1	suchozemský
savce	savec	k1gMnPc4	savec
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
šelem	šelma	k1gFnPc2	šelma
nemá	mít	k5eNaImIp3nS	mít
lední	lední	k2eAgMnSc1d1	lední
medvěd	medvěd	k1gMnSc1	medvěd
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
svého	svůj	k3xOyFgMnSc2	svůj
přirozeného	přirozený	k2eAgMnSc2d1	přirozený
nepřítele	nepřítel	k1gMnSc2	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Jedinými	jediný	k2eAgMnPc7d1	jediný
nepřáteli	nepřítel	k1gMnPc7	nepřítel
jsou	být	k5eAaImIp3nP	být
lidé	člověk	k1gMnPc1	člověk
nebo	nebo	k8xC	nebo
jiní	jiný	k2eAgMnPc1d1	jiný
lední	lední	k2eAgMnPc1d1	lední
medvědi	medvěd	k1gMnPc1	medvěd
(	(	kIx(	(
<g/>
samci	samec	k1gMnPc1	samec
zabíjejí	zabíjet	k5eAaImIp3nP	zabíjet
mláďata	mládě	k1gNnPc4	mládě
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
druhů	druh	k1gInPc2	druh
medvědů	medvěd	k1gMnPc2	medvěd
<g/>
,	,	kIx,	,
zvětří	zvětřet	k5eAaPmIp3nS	zvětřet
<g/>
-li	i	k?	-li
člověka	člověk	k1gMnSc2	člověk
medvěd	medvěd	k1gMnSc1	medvěd
lední	lední	k2eAgMnSc1d1	lední
<g/>
,	,	kIx,	,
považuje	považovat	k5eAaImIp3nS	považovat
jej	on	k3xPp3gMnSc4	on
za	za	k7c4	za
potenciální	potenciální	k2eAgFnSc4d1	potenciální
kořist	kořist	k1gFnSc4	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
okrajích	okraj	k1gInPc6	okraj
jeho	on	k3xPp3gNnSc2	on
teritoria	teritorium	k1gNnSc2	teritorium
mu	on	k3xPp3gMnSc3	on
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
konkurentem	konkurent	k1gMnSc7	konkurent
medvěd	medvěd	k1gMnSc1	medvěd
hnědý	hnědý	k2eAgMnSc1d1	hnědý
(	(	kIx(	(
<g/>
grizzly	grizzly	k1gMnSc1	grizzly
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
případných	případný	k2eAgInPc6d1	případný
střetech	střet	k1gInPc6	střet
o	o	k7c4	o
potravu	potrava	k1gFnSc4	potrava
obvykle	obvykle	k6eAd1	obvykle
navrch	navrch	k6eAd1	navrch
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
ve	v	k7c6	v
zcela	zcela	k6eAd1	zcela
výjimečných	výjimečný	k2eAgInPc6d1	výjimečný
případech	případ	k1gInPc6	případ
představují	představovat	k5eAaImIp3nP	představovat
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
mroži	mrož	k1gMnPc1	mrož
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
někdy	někdy	k6eAd1	někdy
útočí	útočit	k5eAaImIp3nS	útočit
zespodu	zespodu	k6eAd1	zespodu
a	a	k8xC	a
kly	kel	k1gInPc1	kel
mu	on	k3xPp3gNnSc3	on
rozpářou	rozpárat	k5eAaPmIp3nP	rozpárat
břicho	břicho	k1gNnSc4	břicho
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Občas	občas	k6eAd1	občas
ho	on	k3xPp3gMnSc4	on
dokáže	dokázat	k5eAaPmIp3nS	dokázat
zabít	zabít	k5eAaPmF	zabít
i	i	k9	i
kosatka	kosatka	k1gFnSc1	kosatka
dravá	dravý	k2eAgFnSc1d1	dravá
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Lední	lední	k2eAgMnPc1d1	lední
medvědi	medvěd	k1gMnPc1	medvěd
začínají	začínat	k5eAaImIp3nP	začínat
s	s	k7c7	s
námluvami	námluva	k1gFnPc7	námluva
v	v	k7c6	v
době	doba	k1gFnSc6	doba
od	od	k7c2	od
konce	konec	k1gInSc2	konec
března	březen	k1gInSc2	březen
až	až	k8xS	až
začátku	začátek	k1gInSc2	začátek
června	červen	k1gInSc2	červen
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
samci	samec	k1gMnPc1	samec
začínají	začínat	k5eAaImIp3nP	začínat
hledat	hledat	k5eAaImF	hledat
samice	samice	k1gFnPc1	samice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
už	už	k6eAd1	už
nestarají	starat	k5eNaImIp3nP	starat
o	o	k7c4	o
mláďata	mládě	k1gNnPc4	mládě
z	z	k7c2	z
minulých	minulý	k2eAgNnPc2d1	Minulé
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
a	a	k8xC	a
prosinci	prosinec	k1gInSc6	prosinec
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
lední	lední	k2eAgMnPc1d1	lední
medvědi	medvěd	k1gMnPc1	medvěd
vyhrabávají	vyhrabávat	k5eAaImIp3nP	vyhrabávat
ve	v	k7c6	v
sněhu	sníh	k1gInSc6	sníh
doupě	doupě	k1gNnSc4	doupě
na	na	k7c6	na
zimu	zima	k1gFnSc4	zima
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
rodí	rodit	k5eAaImIp3nS	rodit
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
2	[number]	k4	2
mláďata	mládě	k1gNnPc1	mládě
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
vrhu	vrh	k1gInSc6	vrh
<g/>
.	.	kIx.	.
</s>
<s>
Stěny	stěna	k1gFnPc1	stěna
v	v	k7c6	v
brlohu	brloh	k1gInSc6	brloh
pak	pak	k6eAd1	pak
zledovatí	zledovatět	k5eAaPmIp3nS	zledovatět
díky	díky	k7c3	díky
teplému	teplý	k2eAgInSc3d1	teplý
medvědímu	medvědí	k2eAgInSc3d1	medvědí
dechu	dech	k1gInSc3	dech
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
vevnitř	vevnitř	k6eAd1	vevnitř
se	se	k3xPyFc4	se
udržuje	udržovat	k5eAaImIp3nS	udržovat
teplota	teplota	k1gFnSc1	teplota
vhodná	vhodný	k2eAgFnSc1d1	vhodná
pro	pro	k7c4	pro
mláďata	mládě	k1gNnPc4	mládě
(	(	kIx(	(
<g/>
cca	cca	kA	cca
18	[number]	k4	18
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Medvědice	medvědice	k1gFnSc1	medvědice
si	se	k3xPyFc3	se
je	on	k3xPp3gFnPc4	on
navíc	navíc	k6eAd1	navíc
přidržuje	přidržovat	k5eAaImIp3nS	přidržovat
tlapami	tlapa	k1gFnPc7	tlapa
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
kožichu	kožich	k1gInSc6	kožich
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
medvíďata	medvídě	k1gNnPc1	medvídě
rodí	rodit	k5eAaImIp3nP	rodit
holá	holý	k2eAgNnPc1d1	holé
<g/>
,	,	kIx,	,
slepá	slepý	k2eAgNnPc1d1	slepé
a	a	k8xC	a
hluchá	hluchý	k2eAgNnPc1d1	hluché
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
jsou	být	k5eAaImIp3nP	být
navíc	navíc	k6eAd1	navíc
poměrně	poměrně	k6eAd1	poměrně
malá	malý	k2eAgFnSc1d1	malá
<g/>
.	.	kIx.	.
</s>
<s>
Svými	svůj	k3xOyFgInPc7	svůj
450	[number]	k4	450
až	až	k8xS	až
900	[number]	k4	900
gramy	gram	k1gInPc7	gram
se	se	k3xPyFc4	se
velikostí	velikost	k1gFnSc7	velikost
podobají	podobat	k5eAaImIp3nP	podobat
kryse	krysa	k1gFnSc3	krysa
<g/>
.	.	kIx.	.
</s>
<s>
Medvědice	medvědice	k1gFnSc1	medvědice
se	se	k3xPyFc4	se
o	o	k7c4	o
mláďata	mládě	k1gNnPc4	mládě
stará	starat	k5eAaImIp3nS	starat
zhruba	zhruba	k6eAd1	zhruba
3	[number]	k4	3
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
je	on	k3xPp3gInPc4	on
odvrhne	odvrhnout	k5eAaPmIp3nS	odvrhnout
a	a	k8xC	a
v	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
páří	pářit	k5eAaImIp3nS	pářit
se	s	k7c7	s
samcem	samec	k1gMnSc7	samec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
najde	najít	k5eAaPmIp3nS	najít
<g/>
.	.	kIx.	.
</s>
<s>
Lední	lední	k2eAgMnPc1d1	lední
medvědi	medvěd	k1gMnPc1	medvěd
pohlavně	pohlavně	k6eAd1	pohlavně
dospějí	dochvít	k5eAaPmIp3nP	dochvít
ve	v	k7c6	v
třetím	třetí	k4xOgMnSc6	třetí
až	až	k8xS	až
pátém	pátý	k4xOgInSc6	pátý
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Lední	lední	k2eAgMnPc1d1	lední
medvědi	medvěd	k1gMnPc1	medvěd
se	se	k3xPyFc4	se
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
stahují	stahovat	k5eAaImIp3nP	stahovat
do	do	k7c2	do
blízkosti	blízkost	k1gFnSc2	blízkost
kanadského	kanadský	k2eAgNnSc2d1	kanadské
města	město	k1gNnSc2	město
Churchill	Churchilla	k1gFnPc2	Churchilla
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Manitoba	Manitoba	k1gFnSc1	Manitoba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
čekají	čekat	k5eAaImIp3nP	čekat
<g/>
,	,	kIx,	,
než	než	k8xS	než
zamrzne	zamrznout	k5eAaPmIp3nS	zamrznout
Hudsonův	Hudsonův	k2eAgInSc4d1	Hudsonův
záliv	záliv	k1gInSc4	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Churchillu	Churchill	k1gInSc2	Churchill
se	se	k3xPyFc4	se
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
podívanou	podívaná	k1gFnSc4	podívaná
každým	každý	k3xTgInSc7	každý
rokem	rok	k1gInSc7	rok
sjíždí	sjíždět	k5eAaImIp3nS	sjíždět
stovky	stovka	k1gFnPc4	stovka
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnSc1	populace
ledního	lední	k2eAgMnSc2d1	lední
medvěda	medvěd	k1gMnSc2	medvěd
rapidně	rapidně	k6eAd1	rapidně
poklesla	poklesnout	k5eAaPmAgFnS	poklesnout
např.	např.	kA	např.
v	v	k7c4	v
oblasti	oblast	k1gFnPc4	oblast
Beaufortova	Beaufortův	k2eAgNnSc2d1	Beaufortovo
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
globální	globální	k2eAgNnSc4d1	globální
oteplování	oteplování	k1gNnSc4	oteplování
a	a	k8xC	a
úbytek	úbytek	k1gInSc4	úbytek
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k2eAgMnPc1d1	vedoucí
k	k	k7c3	k
vyhladovění	vyhladovění	k1gNnSc3	vyhladovění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
chová	chovat	k5eAaImIp3nS	chovat
medvědy	medvěd	k1gMnPc4	medvěd
lední	lední	k2eAgFnSc2d1	lední
ZOO	zoo	k1gFnSc2	zoo
Brno	Brno	k1gNnSc1	Brno
a	a	k8xC	a
ZOO	zoo	k1gFnSc1	zoo
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
tyto	tento	k3xDgFnPc1	tento
ZOO	zoo	k1gFnPc1	zoo
se	se	k3xPyFc4	se
proslavily	proslavit	k5eAaPmAgFnP	proslavit
úspěšnými	úspěšný	k2eAgInPc7d1	úspěšný
odchovy	odchov	k1gInPc7	odchov
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Inuity	Inuita	k1gFnPc4	Inuita
je	být	k5eAaImIp3nS	být
lední	lední	k2eAgMnSc1d1	lední
medvěd	medvěd	k1gMnSc1	medvěd
posvátným	posvátný	k2eAgNnSc7d1	posvátné
zvířetem	zvíře	k1gNnSc7	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
jazyce	jazyk	k1gInSc6	jazyk
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
nanuk	nanuk	k1gInSc1	nanuk
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
slovo	slovo	k1gNnSc1	slovo
bylo	být	k5eAaImAgNnS	být
převzato	převzít	k5eAaPmNgNnS	převzít
jako	jako	k8xC	jako
název	název	k1gInSc1	název
značky	značka	k1gFnSc2	značka
zmrzliny	zmrzlina	k1gFnSc2	zmrzlina
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ledním	lední	k2eAgMnSc7d1	lední
medvědem	medvěd	k1gMnSc7	medvěd
souvisí	souviset	k5eAaImIp3nS	souviset
i	i	k9	i
pojmenování	pojmenování	k1gNnSc4	pojmenování
zmrzliny	zmrzlina	k1gFnSc2	zmrzlina
Polárka	Polárka	k1gFnSc1	Polárka
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
totiž	totiž	k9	totiž
nazvána	nazvat	k5eAaPmNgFnS	nazvat
podle	podle	k7c2	podle
prvního	první	k4xOgNnSc2	první
úspěšně	úspěšně	k6eAd1	úspěšně
odchovaného	odchovaný	k2eAgNnSc2d1	odchované
medvíděte	medvídě	k1gNnSc2	medvídě
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
ZOO	zoo	k1gFnSc6	zoo
<g/>
.	.	kIx.	.
</s>
<s>
Játra	játra	k1gNnPc1	játra
ledního	lední	k2eAgMnSc2d1	lední
medvěda	medvěd	k1gMnSc2	medvěd
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
nepoživatelná	poživatelný	k2eNgFnSc1d1	nepoživatelná
<g/>
.	.	kIx.	.
</s>
<s>
Obsahují	obsahovat	k5eAaImIp3nP	obsahovat
totiž	totiž	k9	totiž
příliš	příliš	k6eAd1	příliš
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
vitamínu	vitamín	k1gInSc2	vitamín
A.	A.	kA	A.
Jejich	jejich	k3xOp3gNnSc4	jejich
požití	požití	k1gNnSc4	požití
tak	tak	k6eAd1	tak
zpravidla	zpravidla	k6eAd1	zpravidla
vyvolá	vyvolat	k5eAaPmIp3nS	vyvolat
nebezpečnou	bezpečný	k2eNgFnSc4d1	nebezpečná
hypervitaminózu	hypervitaminóza	k1gFnSc4	hypervitaminóza
<g/>
.	.	kIx.	.
</s>
<s>
Černý	černý	k2eAgInSc1d1	černý
čenich	čenich	k1gInSc1	čenich
medvěda	medvěd	k1gMnSc2	medvěd
ledního	lední	k2eAgMnSc4d1	lední
je	být	k5eAaImIp3nS	být
za	za	k7c2	za
jasného	jasný	k2eAgInSc2d1	jasný
dne	den	k1gInSc2	den
viditelný	viditelný	k2eAgInSc1d1	viditelný
dalekohledem	dalekohled	k1gInSc7	dalekohled
až	až	k9	až
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
několika	několik	k4yIc2	několik
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
medvědi	medvěd	k1gMnPc1	medvěd
<g/>
,	,	kIx,	,
číhající	číhající	k2eAgMnSc1d1	číhající
na	na	k7c4	na
tuleně	tuleň	k1gMnSc4	tuleň
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
čenich	čenich	k1gInSc4	čenich
zakrývají	zakrývat	k5eAaImIp3nP	zakrývat
přední	přední	k2eAgFnSc7d1	přední
tlapou	tlapa	k1gFnSc7	tlapa
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
je	on	k3xPp3gInPc4	on
neprozradil	prozradit	k5eNaPmAgMnS	prozradit
<g/>
.	.	kIx.	.
</s>
<s>
Patrně	patrně	k6eAd1	patrně
se	se	k3xPyFc4	se
však	však	k9	však
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
obecně	obecně	k6eAd1	obecně
rozšířený	rozšířený	k2eAgInSc4d1	rozšířený
mýtus	mýtus	k1gInSc4	mýtus
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
medvěd	medvěd	k1gMnSc1	medvěd
lední	lední	k2eAgMnSc1d1	lední
donucen	donutit	k5eAaPmNgMnS	donutit
okolnostmi	okolnost	k1gFnPc7	okolnost
<g/>
,	,	kIx,	,
dokáže	dokázat	k5eAaPmIp3nS	dokázat
vyvinout	vyvinout	k5eAaPmF	vyvinout
rychlost	rychlost	k1gFnSc4	rychlost
až	až	k9	až
40	[number]	k4	40
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
hod	hod	k1gInSc1	hod
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rychle	rychle	k6eAd1	rychle
ho	on	k3xPp3gMnSc4	on
taková	takový	k3xDgFnSc1	takový
zátěž	zátěž	k1gFnSc1	zátěž
unaví	unavit	k5eAaPmIp3nS	unavit
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
rychlostí	rychlost	k1gFnSc7	rychlost
jen	jen	k9	jen
3	[number]	k4	3
až	až	k9	až
6,5	[number]	k4	6,5
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Medvěd	medvěd	k1gMnSc1	medvěd
lední	lední	k2eAgMnSc1d1	lední
má	mít	k5eAaImIp3nS	mít
černou	černý	k2eAgFnSc4d1	černá
kůži	kůže	k1gFnSc4	kůže
(	(	kIx(	(
<g/>
mláďata	mládě	k1gNnPc1	mládě
růžovou	růžový	k2eAgFnSc4d1	růžová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
lépe	dobře	k6eAd2	dobře
vstřebával	vstřebávat	k5eAaImAgMnS	vstřebávat
teplo	teplo	k1gNnSc4	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Chlupy	Chlup	k1gMnPc4	Chlup
jsou	být	k5eAaImIp3nP	být
duté	dutý	k2eAgFnPc1d1	dutá
a	a	k8xC	a
průhledné	průhledný	k2eAgFnPc1d1	průhledná
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
kožešina	kožešina	k1gFnSc1	kožešina
je	být	k5eAaImIp3nS	být
výborným	výborný	k2eAgInSc7d1	výborný
izolačním	izolační	k2eAgInSc7d1	izolační
materiálem	materiál	k1gInSc7	materiál
<g/>
,	,	kIx,	,
ledního	lední	k2eAgMnSc2d1	lední
medvěda	medvěd	k1gMnSc2	medvěd
jde	jít	k5eAaImIp3nS	jít
proto	proto	k8xC	proto
jen	jen	k9	jen
těžko	těžko	k6eAd1	těžko
zachytit	zachytit	k5eAaPmF	zachytit
termovizí	termovizí	k1gNnSc4	termovizí
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
odchov	odchov	k1gInSc1	odchov
mláděte	mládě	k1gNnSc2	mládě
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
se	se	k3xPyFc4	se
podařil	podařit	k5eAaPmAgInS	podařit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
v	v	k7c6	v
ZOO	zoo	k1gFnSc6	zoo
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Odchovy	odchov	k1gInPc1	odchov
ledních	lední	k2eAgMnPc2d1	lední
medvědů	medvěd	k1gMnPc2	medvěd
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
mají	mít	k5eAaImIp3nP	mít
nízkou	nízký	k2eAgFnSc4d1	nízká
úspěšnost	úspěšnost	k1gFnSc4	úspěšnost
<g/>
,	,	kIx,	,
uvádí	uvádět	k5eAaImIp3nS	uvádět
se	s	k7c7	s
10-20	[number]	k4	10-20
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Lední	lední	k2eAgMnSc1d1	lední
medvěd	medvěd	k1gMnSc1	medvěd
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
křížit	křížit	k5eAaImF	křížit
s	s	k7c7	s
medvědem	medvěd	k1gMnSc7	medvěd
grizzly	grizzly	k1gMnSc1	grizzly
<g/>
,	,	kIx,	,
hybrid	hybrid	k1gInSc1	hybrid
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
pizzly	pizznout	k5eAaPmAgInP	pizznout
<g/>
.	.	kIx.	.
</s>
<s>
Lední	lední	k2eAgMnPc1d1	lední
medvědi	medvěd	k1gMnPc1	medvěd
se	se	k3xPyFc4	se
zřejmě	zřejmě	k6eAd1	zřejmě
vyvinuli	vyvinout	k5eAaPmAgMnP	vyvinout
už	už	k6eAd1	už
před	před	k7c7	před
600	[number]	k4	600
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
