<p>
<s>
Libnov	Libnov	k1gInSc1	Libnov
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Liebenau	Liebenaus	k1gInSc3	Liebenaus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
vesnice	vesnice	k1gFnSc1	vesnice
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
obce	obec	k1gFnSc2	obec
Krajková	krajkový	k2eAgFnSc1d1	krajková
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Sokolov	Sokolov	k1gInSc1	Sokolov
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
asi	asi	k9	asi
1,5	[number]	k4	1,5
km	km	kA	km
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
Krajkové	krajkový	k2eAgFnSc2d1	krajková
<g/>
,	,	kIx,	,
při	při	k7c6	při
okraji	okraj	k1gInSc6	okraj
přírodního	přírodní	k2eAgInSc2d1	přírodní
parku	park	k1gInSc2	park
Leopoldovy	Leopoldův	k2eAgInPc1d1	Leopoldův
Hamry	Hamry	k1gInPc1	Hamry
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
evidováno	evidovat	k5eAaImNgNnS	evidovat
29	[number]	k4	29
adres	adresa	k1gFnPc2	adresa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
zde	zde	k6eAd1	zde
trvale	trvale	k6eAd1	trvale
žilo	žít	k5eAaImAgNnS	žít
25	[number]	k4	25
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Libnov	Libnov	k1gInSc1	Libnov
je	být	k5eAaImIp3nS	být
také	také	k9	také
název	název	k1gInSc1	název
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
3,81	[number]	k4	3,81
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
vesnici	vesnice	k1gFnSc6	vesnice
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1420	[number]	k4	1420
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Část	část	k1gFnSc1	část
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
živila	živit	k5eAaImAgFnS	živit
v	v	k7c6	v
rudním	rudní	k2eAgNnSc6d1	rudní
hornictví	hornictví	k1gNnSc6	hornictví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
katastru	katastr	k1gInSc6	katastr
obce	obec	k1gFnSc2	obec
se	se	k3xPyFc4	se
na	na	k7c6	na
jižních	jižní	k2eAgInPc6d1	jižní
svazích	svah	k1gInPc6	svah
Šibeničního	šibeniční	k2eAgInSc2d1	šibeniční
vrchu	vrch	k1gInSc2	vrch
(	(	kIx(	(
<g/>
666	[number]	k4	666
m	m	kA	m
<g/>
)	)	kIx)	)
těžila	těžit	k5eAaImAgFnS	těžit
olověná	olověný	k2eAgFnSc1d1	olověná
ruda	ruda	k1gFnSc1	ruda
<g/>
.	.	kIx.	.
</s>
<s>
Bohaté	bohatý	k2eAgInPc1d1	bohatý
výtěžky	výtěžek	k1gInPc1	výtěžek
poskytovaly	poskytovat	k5eAaImAgInP	poskytovat
doly	dol	k1gInPc1	dol
zejména	zejména	k9	zejména
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odsunu	odsun	k1gInSc6	odsun
německého	německý	k2eAgNnSc2d1	německé
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
výraznému	výrazný	k2eAgInSc3d1	výrazný
poklesu	pokles	k1gInSc3	pokles
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
obec	obec	k1gFnSc1	obec
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
dosídlit	dosídlit	k5eAaPmF	dosídlit
jen	jen	k9	jen
částečně	částečně	k6eAd1	částečně
<g/>
.	.	kIx.	.
</s>
<s>
Libnov	Libnov	k1gInSc1	Libnov
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
místem	místo	k1gNnSc7	místo
chalupářů	chalupář	k1gMnPc2	chalupář
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
zde	zde	k6eAd1	zde
větší	veliký	k2eAgFnSc1d2	veliký
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
farma	farma	k1gFnSc1	farma
s	s	k7c7	s
chovem	chov	k1gInSc7	chov
skotu	skot	k1gInSc2	skot
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pamětihodnosti	pamětihodnost	k1gFnSc6	pamětihodnost
==	==	k?	==
</s>
</p>
<p>
<s>
Kaplička	kaplička	k1gFnSc1	kaplička
Marie	Maria	k1gFnSc2	Maria
Pomocné	pomocný	k2eAgFnSc2d1	pomocná
–	–	k?	–
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
neznámého	známý	k2eNgNnSc2d1	neznámé
stáří	stáří	k1gNnSc2	stáří
<g/>
,	,	kIx,	,
obnovená	obnovený	k2eAgFnSc1d1	obnovená
roku	rok	k1gInSc2	rok
1738	[number]	k4	1738
<g/>
,	,	kIx,	,
zrekonstruovaná	zrekonstruovaný	k2eAgFnSc1d1	zrekonstruovaná
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
historických	historický	k2eAgInPc2d1	historický
pramenů	pramen	k1gInPc2	pramen
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejstarší	starý	k2eAgFnSc1d3	nejstarší
kaplička	kaplička	k1gFnSc1	kaplička
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Libnov	Libnovo	k1gNnPc2	Libnovo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Katastrální	katastrální	k2eAgFnSc1d1	katastrální
mapa	mapa	k1gFnSc1	mapa
katastru	katastr	k1gInSc2	katastr
Libnov	Libnovo	k1gNnPc2	Libnovo
na	na	k7c6	na
webu	web	k1gInSc6	web
ČÚZK	ČÚZK	kA	ČÚZK
</s>
</p>
