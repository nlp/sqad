<p>
<s>
Sokol	Sokol	k1gMnSc1	Sokol
stěhovavý	stěhovavý	k2eAgMnSc1d1	stěhovavý
(	(	kIx(	(
<g/>
Falco	Falco	k1gMnSc1	Falco
peregrinus	peregrinus	k1gMnSc1	peregrinus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kosmopolitně	kosmopolitně	k6eAd1	kosmopolitně
rozšířený	rozšířený	k2eAgMnSc1d1	rozšířený
sokolovitý	sokolovitý	k2eAgMnSc1d1	sokolovitý
pták	pták	k1gMnSc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
větší	veliký	k2eAgMnSc1d2	veliký
než	než	k8xS	než
samec	samec	k1gMnSc1	samec
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
až	až	k9	až
velikosti	velikost	k1gFnPc4	velikost
havrana	havran	k1gMnSc2	havran
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
špičatá	špičatý	k2eAgNnPc4d1	špičaté
křídla	křídlo	k1gNnPc4	křídlo
a	a	k8xC	a
krátký	krátký	k2eAgInSc4d1	krátký
ocas	ocas	k1gInSc4	ocas
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
zužuje	zužovat	k5eAaImIp3nS	zužovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
zbarvení	zbarvení	k1gNnSc4	zbarvení
je	být	k5eAaImIp3nS	být
nejnápadnější	nápadní	k2eAgInSc1d3	nápadní
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
tmavý	tmavý	k2eAgInSc1d1	tmavý
"	"	kIx"	"
<g/>
vous	vous	k1gInSc1	vous
<g/>
"	"	kIx"	"
na	na	k7c6	na
bílé	bílý	k2eAgFnSc6d1	bílá
tváři	tvář	k1gFnSc6	tvář
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
kořistí	kořist	k1gFnSc7	kořist
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
ptáci	pták	k1gMnPc1	pták
do	do	k7c2	do
velikosti	velikost	k1gFnSc2	velikost
kachny	kachna	k1gFnSc2	kachna
<g/>
.	.	kIx.	.
</s>
<s>
Druh	druh	k1gInSc1	druh
má	mít	k5eAaImIp3nS	mít
množství	množství	k1gNnSc4	množství
popsaných	popsaný	k2eAgInPc2d1	popsaný
poddruhů	poddruh	k1gInPc2	poddruh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
všechny	všechen	k3xTgFnPc1	všechen
jsou	být	k5eAaImIp3nP	být
uznávány	uznáván	k2eAgFnPc1d1	uznávána
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgInPc1d1	různý
prameny	pramen	k1gInPc1	pramen
uvádějí	uvádět	k5eAaImIp3nP	uvádět
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
17	[number]	k4	17
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
dokonce	dokonce	k9	dokonce
i	i	k9	i
19	[number]	k4	19
poddruhů	poddruh	k1gInPc2	poddruh
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
maximální	maximální	k2eAgFnSc7d1	maximální
rychlostí	rychlost	k1gFnSc7	rychlost
při	při	k7c6	při
střemhlavém	střemhlavý	k2eAgInSc6d1	střemhlavý
letu	let	k1gInSc6	let
až	až	k9	až
389	[number]	k4	389
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
představuje	představovat	k5eAaImIp3nS	představovat
sokol	sokol	k1gMnSc1	sokol
stěhovavý	stěhovavý	k2eAgMnSc1d1	stěhovavý
zřejmě	zřejmě	k6eAd1	zřejmě
nejrychlejšího	rychlý	k2eAgMnSc4d3	nejrychlejší
živočicha	živočich	k1gMnSc4	živočich
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Sokol	Sokol	k1gMnSc1	Sokol
stěhovavý	stěhovavý	k2eAgMnSc1d1	stěhovavý
má	mít	k5eAaImIp3nS	mít
kosmopolitní	kosmopolitní	k2eAgNnSc1d1	kosmopolitní
rozšíření	rozšíření	k1gNnSc1	rozšíření
<g/>
.	.	kIx.	.
</s>
<s>
Přirozeně	přirozeně	k6eAd1	přirozeně
se	se	k3xPyFc4	se
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Antarktidě	Antarktida	k1gFnSc6	Antarktida
<g/>
,	,	kIx,	,
v	v	k7c6	v
částech	část	k1gFnPc6	část
jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
a	a	k8xC	a
Islandu	Island	k1gInSc6	Island
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc3	jeho
rozšíření	rozšíření	k1gNnSc3	rozšíření
menší	malý	k2eAgInPc1d2	menší
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
rozvinutých	rozvinutý	k2eAgFnPc6d1	rozvinutá
industrializovaných	industrializovaný	k2eAgFnPc6d1	industrializovaná
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
sokol	sokol	k1gMnSc1	sokol
vyhynul	vyhynout	k5eAaPmAgMnS	vyhynout
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
mapka	mapka	k1gFnSc1	mapka
rozšíření	rozšíření	k1gNnPc1	rozšíření
poddruhů	poddruh	k1gInPc2	poddruh
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc1	tento
oblasti	oblast	k1gFnPc1	oblast
vyznačeny	vyznačen	k2eAgFnPc1d1	vyznačena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zásadní	zásadní	k2eAgInSc4d1	zásadní
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
úbytku	úbytek	k1gInSc6	úbytek
sokola	sokol	k1gMnSc2	sokol
stěhovavého	stěhovavý	k2eAgMnSc2d1	stěhovavý
mělo	mít	k5eAaImAgNnS	mít
používání	používání	k1gNnSc4	používání
pesticidů	pesticid	k1gInPc2	pesticid
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
potravní	potravní	k2eAgInSc4d1	potravní
řetězec	řetězec	k1gInSc4	řetězec
dostávaly	dostávat	k5eAaImAgInP	dostávat
do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
dravců	dravec	k1gMnPc2	dravec
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
těchto	tento	k3xDgInPc2	tento
přípravků	přípravek	k1gInPc2	přípravek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
DDT	DDT	kA	DDT
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
zakázána	zakázat	k5eAaPmNgFnS	zakázat
a	a	k8xC	a
do	do	k7c2	do
některých	některý	k3yIgFnPc2	některý
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyhynul	vyhynout	k5eAaPmAgMnS	vyhynout
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
sokol	sokol	k1gMnSc1	sokol
pomalu	pomalu	k6eAd1	pomalu
vrací	vracet	k5eAaImIp3nS	vracet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1950	[number]	k4	1950
byla	být	k5eAaImAgFnS	být
populace	populace	k1gFnSc1	populace
sokolů	sokol	k1gMnPc2	sokol
v	v	k7c6	v
tehdejším	tehdejší	k2eAgNnSc6d1	tehdejší
Československu	Československo	k1gNnSc6	Československo
poměrně	poměrně	k6eAd1	poměrně
silná	silný	k2eAgFnSc1d1	silná
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
rozšíření	rozšíření	k1gNnSc1	rozšíření
bylo	být	k5eAaImAgNnS	být
limitováno	limitovat	k5eAaBmNgNnS	limitovat
pouze	pouze	k6eAd1	pouze
výskytem	výskyt	k1gInSc7	výskyt
vhodných	vhodný	k2eAgNnPc2d1	vhodné
hnízdišť	hnízdiště	k1gNnPc2	hnízdiště
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
prudkému	prudký	k2eAgInSc3d1	prudký
úbytku	úbytek	k1gInSc3	úbytek
došlo	dojít	k5eAaPmAgNnS	dojít
kvůli	kvůli	k7c3	kvůli
zmíněnému	zmíněný	k2eAgNnSc3d1	zmíněné
nadměrnému	nadměrný	k2eAgNnSc3d1	nadměrné
používání	používání	k1gNnSc3	používání
pesticidů	pesticid	k1gInPc2	pesticid
a	a	k8xC	a
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
nebylo	být	k5eNaImAgNnS	být
při	při	k7c6	při
ornitologickém	ornitologický	k2eAgNnSc6d1	ornitologické
mapování	mapování	k1gNnSc6	mapování
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
žádné	žádný	k3yNgNnSc1	žádný
hnízdění	hnízdění	k1gNnSc1	hnízdění
<g/>
.	.	kIx.	.
</s>
<s>
Stav	stav	k1gInSc1	stav
přetrvával	přetrvávat	k5eAaImAgInS	přetrvávat
do	do	k7c2	do
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
objevovat	objevovat	k5eAaImF	objevovat
pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
zahnízdění	zahnízdění	k1gNnSc4	zahnízdění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
byl	být	k5eAaImAgInS	být
počet	počet	k1gInSc1	počet
hnízdících	hnízdící	k2eAgMnPc2d1	hnízdící
sokolů	sokol	k1gMnPc2	sokol
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
odhadnut	odhadnout	k5eAaPmNgInS	odhadnout
na	na	k7c6	na
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
25	[number]	k4	25
párů	pár	k1gInPc2	pár
(	(	kIx(	(
<g/>
v	v	k7c6	v
ČR	ČR	kA	ČR
druh	druh	k1gInSc4	druh
kriticky	kriticky	k6eAd1	kriticky
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jedince	jedinec	k1gMnSc4	jedinec
pocházející	pocházející	k2eAgMnSc1d1	pocházející
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
či	či	k8xC	či
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Rozpětí	rozpětí	k1gNnSc1	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
sokola	sokol	k1gMnSc2	sokol
stěhovavého	stěhovavý	k2eAgInSc2d1	stěhovavý
je	on	k3xPp3gNnSc4	on
85	[number]	k4	85
<g/>
–	–	k?	–
<g/>
115	[number]	k4	115
cm	cm	kA	cm
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
35	[number]	k4	35
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
váží	vážit	k5eAaImIp3nP	vážit
od	od	k7c2	od
500	[number]	k4	500
g	g	kA	g
do	do	k7c2	do
750	[number]	k4	750
g	g	kA	g
<g/>
,	,	kIx,	,
samice	samice	k1gFnPc1	samice
jsou	být	k5eAaImIp3nP	být
výrazně	výrazně	k6eAd1	výrazně
větší	veliký	k2eAgFnPc1d2	veliký
a	a	k8xC	a
váží	vážit	k5eAaImIp3nP	vážit
910	[number]	k4	910
až	až	k9	až
1500	[number]	k4	1500
g.	g.	k?	g.
Velikost	velikost	k1gFnSc4	velikost
je	být	k5eAaImIp3nS	být
variabilní	variabilní	k2eAgInSc1d1	variabilní
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
zbarvení	zbarvení	k1gNnSc4	zbarvení
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
o	o	k7c4	o
jaký	jaký	k3yRgInSc4	jaký
poddruh	poddruh	k1gInSc4	poddruh
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
<g/>
.	.	kIx.	.
</s>
<s>
Severnější	severní	k2eAgInPc1d2	severnější
druhy	druh	k1gInPc1	druh
jsou	být	k5eAaImIp3nP	být
větší	veliký	k2eAgInPc1d2	veliký
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgInPc1d1	jižní
druhy	druh	k1gInPc1	druh
jsou	být	k5eAaImIp3nP	být
drobnější	drobný	k2eAgNnSc4d2	drobnější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zbarvení	zbarvení	k1gNnSc1	zbarvení
sokolů	sokol	k1gMnPc2	sokol
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
variabilní	variabilní	k2eAgInSc1d1	variabilní
<g/>
.	.	kIx.	.
</s>
<s>
Čím	čí	k3xOyRgNnSc7	čí
severnější	severní	k2eAgFnSc1d2	severnější
populace	populace	k1gFnSc1	populace
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
celkové	celkový	k2eAgNnSc1d1	celkové
zbarvení	zbarvení	k1gNnSc1	zbarvení
světlejší	světlý	k2eAgNnSc1d2	světlejší
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
tropech	trop	k1gInPc6	trop
může	moct	k5eAaImIp3nS	moct
zbarvení	zbarvení	k1gNnSc1	zbarvení
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
zad	zad	k1gInSc4	zad
být	být	k5eAaImF	být
až	až	k9	až
černé	černý	k2eAgFnPc4d1	černá
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
popis	popis	k1gInSc1	popis
tedy	tedy	k9	tedy
platí	platit	k5eAaImIp3nS	platit
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
poddruh	poddruh	k1gInSc4	poddruh
sokol	sokol	k1gMnSc1	sokol
stěhovavý	stěhovavý	k2eAgMnSc1d1	stěhovavý
eurosibiřský	eurosibiřský	k2eAgMnSc1d1	eurosibiřský
(	(	kIx(	(
<g/>
F.	F.	kA	F.
p.	p.	k?	p.
peregrinus	peregrinus	k1gInSc1	peregrinus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Dospělý	dospělý	k2eAgMnSc1d1	dospělý
pták	pták	k1gMnSc1	pták
má	mít	k5eAaImIp3nS	mít
tmavě	tmavě	k6eAd1	tmavě
šedý	šedý	k2eAgInSc4d1	šedý
až	až	k8xS	až
šedočerný	šedočerný	k2eAgInSc4d1	šedočerný
hřbet	hřbet	k1gInSc4	hřbet
a	a	k8xC	a
bílou	bílý	k2eAgFnSc4d1	bílá
spodinu	spodina	k1gFnSc4	spodina
s	s	k7c7	s
kapkovitými	kapkovitý	k2eAgFnPc7d1	kapkovitá
skvrnami	skvrna	k1gFnPc7	skvrna
na	na	k7c6	na
hrdle	hrdla	k1gFnSc6	hrdla
přecházejícími	přecházející	k2eAgInPc7d1	přecházející
v	v	k7c4	v
příčné	příčný	k2eAgFnPc4d1	příčná
vlnky	vlnka	k1gFnPc4	vlnka
na	na	k7c6	na
břiše	břicho	k1gNnSc6	břicho
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
spodinu	spodina	k1gFnSc4	spodina
těla	tělo	k1gNnSc2	tělo
nahnědlou	nahnědlý	k2eAgFnSc7d1	nahnědlá
<g/>
.	.	kIx.	.
</s>
<s>
Ruční	ruční	k2eAgFnPc1d1	ruční
letky	letka	k1gFnPc1	letka
jsou	být	k5eAaImIp3nP	být
černohnědé	černohnědý	k2eAgFnPc1d1	černohnědá
<g/>
,	,	kIx,	,
špičky	špička	k1gFnPc1	špička
křídel	křídlo	k1gNnPc2	křídlo
černé	černý	k2eAgInPc1d1	černý
<g/>
,	,	kIx,	,
ocas	ocas	k1gInSc1	ocas
šedohnědý	šedohnědý	k2eAgInSc1d1	šedohnědý
<g/>
,	,	kIx,	,
s	s	k7c7	s
příčnými	příčný	k2eAgFnPc7d1	příčná
skvrnami	skvrna	k1gFnPc7	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
ocasu	ocas	k1gInSc2	ocas
je	být	k5eAaImIp3nS	být
bíle	bíle	k6eAd1	bíle
olemován	olemován	k2eAgInSc1d1	olemován
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
bělavou	bělavý	k2eAgFnSc4d1	bělavá
tvář	tvář	k1gFnSc4	tvář
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
tmavá	tmavý	k2eAgFnSc1d1	tmavá
šedočerná	šedočerný	k2eAgFnSc1d1	šedočerná
barva	barva	k1gFnSc1	barva
z	z	k7c2	z
temene	temeno	k1gNnSc2	temeno
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
nápadný	nápadný	k2eAgMnSc1d1	nápadný
<g/>
,	,	kIx,	,
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
vous	vous	k1gInSc1	vous
<g/>
.	.	kIx.	.
</s>
<s>
Světlejší	světlý	k2eAgFnPc1d2	světlejší
severní	severní	k2eAgFnPc1d1	severní
populace	populace	k1gFnPc1	populace
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
tento	tento	k3xDgInSc1	tento
vous	vous	k1gInSc1	vous
užší	úzký	k2eAgInSc1d2	užší
a	a	k8xC	a
méně	málo	k6eAd2	málo
výrazný	výrazný	k2eAgInSc1d1	výrazný
<g/>
.	.	kIx.	.
</s>
<s>
Mladí	mladý	k2eAgMnPc1d1	mladý
ptáci	pták	k1gMnPc1	pták
mají	mít	k5eAaImIp3nP	mít
hřbet	hřbet	k1gInSc4	hřbet
spíš	spíš	k9	spíš
šedohnědý	šedohnědý	k2eAgInSc4d1	šedohnědý
s	s	k7c7	s
bělavými	bělavý	k2eAgInPc7d1	bělavý
nebo	nebo	k8xC	nebo
nažloutlými	nažloutlý	k2eAgInPc7d1	nažloutlý
lemy	lem	k1gInPc7	lem
per	pero	k1gNnPc2	pero
<g/>
.	.	kIx.	.
</s>
<s>
Ocas	ocas	k1gInSc4	ocas
mají	mít	k5eAaImIp3nP	mít
tmavohnědý	tmavohnědý	k2eAgMnSc1d1	tmavohnědý
se	s	k7c7	s
světlými	světlý	k2eAgInPc7d1	světlý
příčnými	příčný	k2eAgInPc7d1	příčný
pruhy	pruh	k1gInPc7	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Spodinu	spodina	k1gFnSc4	spodina
těla	tělo	k1gNnSc2	tělo
mají	mít	k5eAaImIp3nP	mít
výrazně	výrazně	k6eAd1	výrazně
podélně	podélně	k6eAd1	podélně
skvrnitou	skvrnitý	k2eAgFnSc4d1	skvrnitá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sokoli	sokol	k1gMnPc1	sokol
létají	létat	k5eAaImIp3nP	létat
rychlým	rychlý	k2eAgNnSc7d1	rychlé
máváním	mávání	k1gNnSc7	mávání
křídel	křídlo	k1gNnPc2	křídlo
<g/>
,	,	kIx,	,
plachtí	plachtit	k5eAaImIp3nS	plachtit
jen	jen	k9	jen
na	na	k7c6	na
krátkých	krátký	k2eAgInPc6d1	krátký
úsecích	úsek	k1gInPc6	úsek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekologie	ekologie	k1gFnSc1	ekologie
==	==	k?	==
</s>
</p>
<p>
<s>
Severské	severský	k2eAgFnPc1d1	severská
populace	populace	k1gFnPc1	populace
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
tažné	tažný	k2eAgFnPc1d1	tažná
<g/>
,	,	kIx,	,
z	z	k7c2	z
ostatních	ostatní	k2eAgFnPc2d1	ostatní
populací	populace	k1gFnPc2	populace
jsou	být	k5eAaImIp3nP	být
někteří	některý	k3yIgMnPc1	některý
jedinci	jedinec	k1gMnPc1	jedinec
tažní	tažní	k2eAgMnPc1d1	tažní
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
přelétaví	přelétavý	k2eAgMnPc1d1	přelétavý
a	a	k8xC	a
stálí	stálý	k2eAgMnPc1d1	stálý
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
skalních	skalní	k2eAgFnPc6d1	skalní
stěnách	stěna	k1gFnPc6	stěna
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nemají	mít	k5eNaImIp3nP	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
skály	skála	k1gFnSc2	skála
<g/>
,	,	kIx,	,
zahnízdí	zahnízdit	k5eAaPmIp3nS	zahnízdit
i	i	k9	i
ve	v	k7c6	v
starém	starý	k2eAgNnSc6d1	staré
hnízdě	hnízdo	k1gNnSc6	hnízdo
jiného	jiný	k2eAgInSc2d1	jiný
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
hnízdo	hnízdo	k1gNnSc4	hnízdo
nestaví	stavit	k5eNaImIp3nP	stavit
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
neupravuje	upravovat	k5eNaImIp3nS	upravovat
<g/>
,	,	kIx,	,
na	na	k7c6	na
skále	skála	k1gFnSc6	skála
mnohdy	mnohdy	k6eAd1	mnohdy
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
holém	holý	k2eAgInSc6d1	holý
podkladu	podklad	k1gInSc6	podklad
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
zaznamenána	zaznamenat	k5eAaPmNgFnS	zaznamenat
i	i	k8xC	i
hnízda	hnízdo	k1gNnPc4	hnízdo
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbil	oblíbit	k5eAaPmAgMnS	oblíbit
si	se	k3xPyFc3	se
lidské	lidský	k2eAgFnPc4d1	lidská
stavby	stavba	k1gFnPc4	stavba
a	a	k8xC	a
často	často	k6eAd1	často
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
ve	v	k7c6	v
zříceninách	zřícenina	k1gFnPc6	zřícenina
hradů	hrad	k1gInPc2	hrad
nebo	nebo	k8xC	nebo
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
věžích	věž	k1gFnPc6	věž
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
poštolky	poštolka	k1gFnSc2	poštolka
nehnízdí	hnízdit	k5eNaImIp3nS	hnízdit
příliš	příliš	k6eAd1	příliš
často	často	k6eAd1	často
ve	v	k7c6	v
velkých	velký	k2eAgNnPc6d1	velké
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
hnízdo	hnízdo	k1gNnSc4	hnízdo
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
vrací	vracet	k5eAaImIp3nS	vracet
nebo	nebo	k8xC	nebo
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
aspoň	aspoň	k9	aspoň
poblíž	poblíž	k7c2	poblíž
původního	původní	k2eAgNnSc2d1	původní
hnízda	hnízdo	k1gNnSc2	hnízdo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Před	před	k7c7	před
hnízděním	hnízdění	k1gNnSc7	hnízdění
předvádí	předvádět	k5eAaImIp3nS	předvádět
akrobatické	akrobatický	k2eAgInPc4d1	akrobatický
zásnubní	zásnubní	k2eAgInPc4d1	zásnubní
lety	let	k1gInPc4	let
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
snáší	snášet	k5eAaImIp3nS	snášet
samice	samice	k1gFnSc1	samice
nejčastěji	často	k6eAd3	často
na	na	k7c6	na
konci	konec	k1gInSc6	konec
března	březen	k1gInSc2	březen
či	či	k8xC	či
začátku	začátek	k1gInSc2	začátek
dubna	duben	k1gInSc2	duben
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
vejce	vejce	k1gNnSc2	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Sedí	sedit	k5eAaImIp3nP	sedit
na	na	k7c6	na
nich	on	k3xPp3gInPc6	on
cca	cca	kA	cca
29	[number]	k4	29
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
samec	samec	k1gInSc1	samec
ji	on	k3xPp3gFnSc4	on
občas	občas	k6eAd1	občas
střídá	střídat	k5eAaImIp3nS	střídat
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
si	se	k3xPyFc3	se
buď	buď	k8xC	buď
potravu	potrava	k1gFnSc4	potrava
sama	sám	k3xTgMnSc4	sám
uloví	ulovit	k5eAaPmIp3nS	ulovit
<g/>
,	,	kIx,	,
když	když	k8xS	když
ji	on	k3xPp3gFnSc4	on
samec	samec	k1gMnSc1	samec
vystřídá	vystřídat	k5eAaPmIp3nS	vystřídat
v	v	k7c6	v
sezení	sezení	k1gNnSc6	sezení
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jí	on	k3xPp3gFnSc3	on
samec	samec	k1gMnSc1	samec
nosí	nosit	k5eAaImIp3nS	nosit
kořist	kořist	k1gFnSc4	kořist
na	na	k7c4	na
hnízdo	hnízdo	k1gNnSc4	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vylíhnutí	vylíhnutí	k1gNnSc6	vylíhnutí
se	se	k3xPyFc4	se
o	o	k7c4	o
mláďata	mládě	k1gNnPc4	mládě
na	na	k7c6	na
hnízdě	hnízdo	k1gNnSc6	hnízdo
stará	starat	k5eAaImIp3nS	starat
asi	asi	k9	asi
14	[number]	k4	14
dní	den	k1gInPc2	den
jen	jen	k9	jen
samice	samice	k1gFnSc1	samice
<g/>
,	,	kIx,	,
samec	samec	k1gMnSc1	samec
přináší	přinášet	k5eAaImIp3nS	přinášet
kořist	kořist	k1gFnSc4	kořist
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
střídají	střídat	k5eAaImIp3nP	střídat
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
opouštějí	opouštět	k5eAaImIp3nP	opouštět
hnízdo	hnízdo	k1gNnSc1	hnízdo
po	po	k7c6	po
35	[number]	k4	35
<g/>
–	–	k?	–
<g/>
42	[number]	k4	42
dnech	den	k1gInPc6	den
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rodiče	rodič	k1gMnPc1	rodič
je	on	k3xPp3gNnSc4	on
nadále	nadále	k6eAd1	nadále
krmí	krmit	k5eAaImIp3nS	krmit
ještě	ještě	k9	ještě
aspoň	aspoň	k9	aspoň
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Sokoli	sokol	k1gMnPc1	sokol
pohlavně	pohlavně	k6eAd1	pohlavně
dospívají	dospívat	k5eAaImIp3nP	dospívat
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
až	až	k6eAd1	až
třech	tři	k4xCgInPc6	tři
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Loví	lovit	k5eAaImIp3nS	lovit
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
ptáky	pták	k1gMnPc4	pták
za	za	k7c2	za
letu	let	k1gInSc2	let
<g/>
.	.	kIx.	.
</s>
<s>
Styl	styl	k1gInSc1	styl
lovu	lov	k1gInSc2	lov
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
vhodný	vhodný	k2eAgInSc1d1	vhodný
pro	pro	k7c4	pro
pozemní	pozemní	k2eAgFnSc4d1	pozemní
kořist	kořist	k1gFnSc4	kořist
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
k	k	k7c3	k
lovu	lov	k1gInSc3	lov
drobných	drobný	k2eAgMnPc2d1	drobný
savců	savec	k1gMnPc2	savec
uchyluje	uchylovat	k5eAaImIp3nS	uchylovat
výjimečně	výjimečně	k6eAd1	výjimečně
a	a	k8xC	a
ptáky	pták	k1gMnPc4	pták
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
donutit	donutit	k5eAaPmF	donutit
vzlétnout	vzlétnout	k5eAaPmF	vzlétnout
<g/>
.	.	kIx.	.
</s>
<s>
Vrhá	vrhat	k5eAaImIp3nS	vrhat
se	se	k3xPyFc4	se
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
výšky	výška	k1gFnSc2	výška
na	na	k7c4	na
kořist	kořist	k1gFnSc4	kořist
pod	pod	k7c7	pod
sebou	se	k3xPyFc7	se
a	a	k8xC	a
útočí	útočit	k5eAaImIp3nS	útočit
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
pařáty	pařát	k1gInPc1	pařát
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
střemhlavém	střemhlavý	k2eAgInSc6d1	střemhlavý
útoku	útok	k1gInSc6	útok
s	s	k7c7	s
přitaženými	přitažený	k2eAgNnPc7d1	přitažené
křídly	křídlo	k1gNnPc7	křídlo
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
rychlosti	rychlost	k1gFnSc2	rychlost
přes	přes	k7c4	přes
300	[number]	k4	300
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Náraz	náraz	k1gInSc1	náraz
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
rychlosti	rychlost	k1gFnSc6	rychlost
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
nebezpečný	bezpečný	k2eNgInSc1d1	nebezpečný
i	i	k9	i
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
trefit	trefit	k5eAaPmF	trefit
spíš	spíš	k9	spíš
křídlo	křídlo	k1gNnSc4	křídlo
než	než	k8xS	než
přímo	přímo	k6eAd1	přímo
tělo	tělo	k1gNnSc1	tělo
kořisti	kořist	k1gFnSc2	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
stejného	stejný	k2eAgInSc2d1	stejný
důvodu	důvod	k1gInSc2	důvod
nemůže	moct	k5eNaImIp3nS	moct
zaútočit	zaútočit	k5eAaPmF	zaútočit
na	na	k7c4	na
hejno	hejno	k1gNnSc4	hejno
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
drobných	drobný	k2eAgMnPc2d1	drobný
ptáků	pták	k1gMnPc2	pták
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
před	před	k7c7	před
sokolem	sokol	k1gMnSc7	sokol
shlukují	shlukovat	k5eAaImIp3nP	shlukovat
do	do	k7c2	do
hejna	hejno	k1gNnSc2	hejno
<g/>
.	.	kIx.	.
</s>
<s>
Kořist	kořist	k1gFnSc1	kořist
většinou	většinou	k6eAd1	většinou
po	po	k7c6	po
útoku	útok	k1gInSc6	útok
klesne	klesnout	k5eAaPmIp3nS	klesnout
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ji	on	k3xPp3gFnSc4	on
sokol	sokol	k1gMnSc1	sokol
usmrtí	usmrtit	k5eAaPmIp3nS	usmrtit
klovnutím	klovnutí	k1gNnSc7	klovnutí
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
pád	pád	k1gInSc4	pád
přežila	přežít	k5eAaPmAgFnS	přežít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejčastější	častý	k2eAgFnSc7d3	nejčastější
kořistí	kořist	k1gFnSc7	kořist
sokola	sokol	k1gMnSc2	sokol
stěhovavého	stěhovavý	k2eAgMnSc2d1	stěhovavý
bývají	bývat	k5eAaImIp3nP	bývat
<g/>
:	:	kIx,	:
holubi	holub	k1gMnPc1	holub
<g/>
,	,	kIx,	,
hrdličky	hrdlička	k1gFnPc1	hrdlička
<g/>
,	,	kIx,	,
špačci	špaček	k1gMnPc1	špaček
<g/>
,	,	kIx,	,
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
drozdů	drozd	k1gMnPc2	drozd
<g/>
,	,	kIx,	,
koroptve	koroptev	k1gFnPc1	koroptev
<g/>
,	,	kIx,	,
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
kachen	kachna	k1gFnPc2	kachna
<g/>
,	,	kIx,	,
racci	racek	k1gMnPc1	racek
i	i	k8xC	i
drobnější	drobný	k2eAgMnPc1d2	drobnější
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
rorýsi	rorýs	k1gMnPc1	rorýs
<g/>
,	,	kIx,	,
vlaštovky	vlaštovka	k1gFnPc1	vlaštovka
či	či	k8xC	či
pěnkavy	pěnkava	k1gFnPc1	pěnkava
<g/>
.	.	kIx.	.
</s>
<s>
Troufne	troufnout	k5eAaPmIp3nS	troufnout
si	se	k3xPyFc3	se
i	i	k9	i
na	na	k7c4	na
havrany	havran	k1gMnPc4	havran
a	a	k8xC	a
vrány	vrána	k1gFnPc4	vrána
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
savců	savec	k1gMnPc2	savec
jsou	být	k5eAaImIp3nP	být
častou	častý	k2eAgFnSc7d1	častá
kořistí	kořist	k1gFnSc7	kořist
netopýři	netopýr	k1gMnPc1	netopýr
<g/>
,	,	kIx,	,
králíci	králík	k1gMnPc1	králík
<g/>
,	,	kIx,	,
sysli	sysel	k1gMnPc1	sysel
a	a	k8xC	a
veverky	veverka	k1gFnPc1	veverka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Použitá	použitý	k2eAgFnSc1d1	použitá
literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BALÁT	BALÁT	kA	BALÁT
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
;	;	kIx,	;
HUDEC	Hudec	k1gMnSc1	Hudec
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
a	a	k8xC	a
ŠŤASTNÝ	Šťastný	k1gMnSc1	Šťastný
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
–	–	k?	–
Aves	Avesa	k1gFnPc2	Avesa
<g/>
.	.	kIx.	.
</s>
<s>
Díl	díl	k1gInSc1	díl
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
přeprac	přeprac	k1gFnSc1	přeprac
<g/>
.	.	kIx.	.
a	a	k8xC	a
dopl	dopl	k1gInSc1	dopl
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
572	[number]	k4	572
s.	s.	k?	s.
Fauna	fauna	k1gFnSc1	fauna
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
29	[number]	k4	29
<g/>
/	/	kIx~	/
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
1113	[number]	k4	1113
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SAUER	SAUER	kA	SAUER
<g/>
,	,	kIx,	,
F.	F.	kA	F.
Průvodce	průvodce	k1gMnSc1	průvodce
přírodou	příroda	k1gFnSc7	příroda
<g/>
:	:	kIx,	:
ptáci	pták	k1gMnPc1	pták
lesů	les	k1gInPc2	les
<g/>
,	,	kIx,	,
luk	louka	k1gFnPc2	louka
a	a	k8xC	a
polí	pole	k1gFnPc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ikar	Ikar	k1gMnSc1	Ikar
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
286	[number]	k4	286
s.	s.	k?	s.
</s>
</p>
<p>
<s>
ŠŤASTNÝ	Šťastný	k1gMnSc1	Šťastný
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
;	;	kIx,	;
BEJČEK	Bejček	k1gMnSc1	Bejček
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
;	;	kIx,	;
HUDEC	Hudec	k1gMnSc1	Hudec
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Atlas	Atlas	k1gInSc1	Atlas
hnízdního	hnízdní	k2eAgNnSc2d1	hnízdní
rozšíření	rozšíření	k1gNnSc2	rozšíření
ptáků	pták	k1gMnPc2	pták
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
:	:	kIx,	:
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Aventinum	Aventinum	k1gNnSc1	Aventinum
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
463	[number]	k4	463
s.	s.	k?	s.
<g/>
,	,	kIx,	,
volná	volný	k2eAgFnSc1d1	volná
příl	příl	k1gInSc1	příl
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86858	[number]	k4	86858
<g/>
-	-	kIx~	-
<g/>
88	[number]	k4	88
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
sokolnictví	sokolnictví	k1gNnSc1	sokolnictví
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
sokol	sokol	k1gMnSc1	sokol
stěhovavý	stěhovavý	k2eAgMnSc1d1	stěhovavý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
sokol	sokol	k1gMnSc1	sokol
stěhovavý	stěhovavý	k2eAgMnSc1d1	stěhovavý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
hlasová	hlasový	k2eAgFnSc1d1	hlasová
ukázka	ukázka	k1gFnSc1	ukázka
z	z	k7c2	z
projektu	projekt	k1gInSc2	projekt
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Hlas	hlas	k1gInSc1	hlas
na	na	k7c4	na
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
</s>
</p>
<p>
<s>
wildafrica	wildafrica	k1gFnSc1	wildafrica
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
http://www.priroda.cz/lexikon.php?detail=376	[url]	k4	http://www.priroda.cz/lexikon.php?detail=376
</s>
</p>
<p>
<s>
BioLib	BioLib	k1gInSc1	BioLib
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
Falco	Falco	k1gMnSc1	Falco
peregrinus	peregrinus	k1gMnSc1	peregrinus
(	(	kIx(	(
<g/>
sokol	sokol	k1gMnSc1	sokol
stěhovavý	stěhovavý	k2eAgMnSc1d1	stěhovavý
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
BioLib	BioLib	k1gInSc1	BioLib
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
European	European	k1gMnSc1	European
Peregrine	Peregrin	k1gInSc5	Peregrin
Falcon	Falcon	k1gNnSc1	Falcon
Working	Working	k1gInSc1	Working
Group	Group	k1gMnSc1	Group
</s>
</p>
<p>
<s>
Webkamera	Webkamera	k1gFnSc1	Webkamera
v	v	k7c6	v
hnízdní	hnízdní	k2eAgFnSc6d1	hnízdní
budce	budka	k1gFnSc6	budka
</s>
</p>
<p>
<s>
Webkamera	Webkamera	k1gFnSc1	Webkamera
z	z	k7c2	z
hnízda	hnízdo	k1gNnSc2	hnízdo
sokola	sokol	k1gMnSc2	sokol
<g/>
:	:	kIx,	:
http://www.zoocam.info/sokoli-hnizdo-webkamera-decin/	[url]	k?	http://www.zoocam.info/sokoli-hnizdo-webkamera-decin/
</s>
</p>
<p>
<s>
Webkamera	Webkamera	k1gFnSc1	Webkamera
u	u	k7c2	u
hnízda	hnízdo	k1gNnSc2	hnízdo
na	na	k7c6	na
komíně	komín	k1gInSc6	komín
v	v	k7c6	v
Mladé	mladý	k2eAgFnSc6d1	mladá
Boleslavi	Boleslaev	k1gFnSc6	Boleslaev
<g/>
:	:	kIx,	:
http://www.sko-energo.cz/cs/sokoli/	[url]	k?	http://www.sko-energo.cz/cs/sokoli/
</s>
</p>
