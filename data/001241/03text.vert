<p>
<s>
Generál	generál	k1gMnSc1	generál
Dwight	Dwight	k1gMnSc1	Dwight
David	David	k1gMnSc1	David
Eisenhower	Eisenhower	k1gMnSc1	Eisenhower
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
též	též	k9	též
jako	jako	k9	jako
Ike	Ike	k1gFnSc1	Ike
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1890	[number]	k4	1890
–	–	k?	–
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
pětihvězdičkový	pětihvězdičkový	k2eAgMnSc1d1	pětihvězdičkový
generál	generál	k1gMnSc1	generál
a	a	k8xC	a
34	[number]	k4	34
<g/>
.	.	kIx.	.
prezident	prezident	k1gMnSc1	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgMnS	být
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
(	(	kIx(	(
<g/>
západních	západní	k2eAgFnPc2d1	západní
<g/>
)	)	kIx)	)
spojeneckých	spojenecký	k2eAgFnPc2d1	spojenecká
expedičních	expediční	k2eAgFnPc2d1	expediční
sil	síla	k1gFnPc2	síla
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
1944	[number]	k4	1944
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
zastával	zastávat	k5eAaImAgMnS	zastávat
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
vojenské	vojenský	k2eAgFnSc2d1	vojenská
funkce	funkce	k1gFnSc2	funkce
v	v	k7c6	v
ozbrojených	ozbrojený	k2eAgFnPc6d1	ozbrojená
silách	síla	k1gFnPc6	síla
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
odešel	odejít	k5eAaPmAgMnS	odejít
jako	jako	k9	jako
voják	voják	k1gMnSc1	voják
do	do	k7c2	do
důchodu	důchod	k1gInSc2	důchod
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
americké	americký	k2eAgFnSc2d1	americká
vysoké	vysoký	k2eAgFnSc2d1	vysoká
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgInS	získat
nominaci	nominace	k1gFnSc4	nominace
Republikánské	republikánský	k2eAgFnSc2d1	republikánská
strany	strana	k1gFnSc2	strana
pro	pro	k7c4	pro
kandidaturu	kandidatura	k1gFnSc4	kandidatura
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
USA	USA	kA	USA
a	a	k8xC	a
v	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
<g/>
.	.	kIx.	.
</s>
<s>
Úřad	úřad	k1gInSc1	úřad
prezidenta	prezident	k1gMnSc2	prezident
USA	USA	kA	USA
poté	poté	k6eAd1	poté
zastával	zastávat	k5eAaImAgMnS	zastávat
po	po	k7c4	po
dvě	dva	k4xCgNnPc4	dva
funkční	funkční	k2eAgNnPc4d1	funkční
období	období	k1gNnPc4	období
v	v	k7c6	v
letech	let	k1gInPc6	let
1953	[number]	k4	1953
<g/>
–	–	k?	–
<g/>
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc4	původ
a	a	k8xC	a
mládí	mládí	k1gNnSc4	mládí
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
do	do	k7c2	do
německo-americké	německomerický	k2eAgFnSc2d1	německo-americká
rodiny	rodina	k1gFnSc2	rodina
v	v	k7c6	v
Denisonu	Denison	k1gInSc6	Denison
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Texas	Texas	k1gInSc1	Texas
<g/>
,	,	kIx,	,
vyrůstal	vyrůstat	k5eAaImAgInS	vyrůstat
však	však	k9	však
v	v	k7c6	v
Kansasu	Kansas	k1gInSc6	Kansas
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
třetí	třetí	k4xOgMnSc1	třetí
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
synů	syn	k1gMnPc2	syn
Davida	David	k1gMnSc2	David
Jacoba	Jacoba	k1gFnSc1	Jacoba
Eisenhowera	Eisenhowera	k1gFnSc1	Eisenhowera
a	a	k8xC	a
Idy	Ida	k1gFnPc1	Ida
Elizabeth	Elizabeth	k1gFnSc2	Elizabeth
Stover	Stovra	k1gFnPc2	Stovra
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jediným	jediný	k2eAgInSc7d1	jediný
jejich	jejich	k3xOp3gNnSc4	jejich
dítětem	dítě	k1gNnSc7	dítě
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
narodilo	narodit	k5eAaPmAgNnS	narodit
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Eisenhowerovi	Eisenhowerův	k2eAgMnPc1d1	Eisenhowerův
předkové	předek	k1gMnPc1	předek
v	v	k7c6	v
mužské	mužský	k2eAgFnSc6d1	mužská
linii	linie	k1gFnSc6	linie
připluli	připlout	k5eAaPmAgMnP	připlout
do	do	k7c2	do
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1741	[number]	k4	1741
<g/>
.	.	kIx.	.
</s>
<s>
Pocházeli	pocházet	k5eAaImAgMnP	pocházet
z	z	k7c2	z
vesnice	vesnice	k1gFnSc2	vesnice
Karlsbrunn	Karlsbrunna	k1gFnPc2	Karlsbrunna
v	v	k7c6	v
tehdejším	tehdejší	k2eAgInSc6d1	tehdejší
–	–	k?	–
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
–	–	k?	–
samostatném	samostatný	k2eAgNnSc6d1	samostatné
hrabství	hrabství	k1gNnSc6	hrabství
Nassau-Saarbrücken	Nassau-Saarbrückna	k1gFnPc2	Nassau-Saarbrückna
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
původní	původní	k2eAgNnSc1d1	původní
jméno	jméno	k1gNnSc1	jméno
znělo	znět	k5eAaImAgNnS	znět
Eisenhauer	Eisenhauer	k1gInSc4	Eisenhauer
<g/>
.	.	kIx.	.
</s>
<s>
Usadili	usadit	k5eAaPmAgMnP	usadit
se	se	k3xPyFc4	se
napřed	napřed	k6eAd1	napřed
v	v	k7c6	v
Yorku	York	k1gInSc6	York
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
britské	britský	k2eAgFnSc6d1	britská
korunní	korunní	k2eAgFnSc6d1	korunní
kolonii	kolonie	k1gFnSc6	kolonie
Pennsylvánie	Pennsylvánie	k1gFnSc2	Pennsylvánie
<g/>
.	.	kIx.	.
</s>
<s>
Hans	Hans	k1gMnSc1	Hans
Nikolaus	Nikolaus	k1gMnSc1	Nikolaus
Eisenhauer	Eisenhauer	k1gMnSc1	Eisenhauer
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
do	do	k7c2	do
Lancasteru	Lancaster	k1gInSc2	Lancaster
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
Pennsylvánii	Pennsylvánie	k1gFnSc6	Pennsylvánie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
americké	americký	k2eAgFnSc2d1	americká
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
rodina	rodina	k1gFnSc1	rodina
Eisenhowerů	Eisenhower	k1gInPc2	Eisenhower
prakticky	prakticky	k6eAd1	prakticky
vůbec	vůbec	k9	vůbec
nezásahla	zásahnout	k5eNaPmAgFnS	zásahnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1892	[number]	k4	1892
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
přesunula	přesunout	k5eAaPmAgFnS	přesunout
do	do	k7c2	do
Abilene	Abilen	k1gInSc5	Abilen
<g/>
.	.	kIx.	.
</s>
<s>
Dwightův	Dwightův	k2eAgMnSc1d1	Dwightův
otec	otec	k1gMnSc1	otec
pracoval	pracovat	k5eAaImAgMnS	pracovat
ve	v	k7c6	v
školství	školství	k1gNnSc6	školství
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
pro	pro	k7c4	pro
změnu	změna	k1gFnSc4	změna
náboženského	náboženský	k2eAgNnSc2d1	náboženské
vyznání	vyznání	k1gNnSc2	vyznání
a	a	k8xC	a
připojila	připojit	k5eAaPmAgFnS	připojit
se	se	k3xPyFc4	se
k	k	k7c3	k
Mezinárodním	mezinárodní	k2eAgMnPc3d1	mezinárodní
badatelům	badatel	k1gMnPc3	badatel
Bible	bible	k1gFnSc2	bible
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
známým	známá	k1gFnPc3	známá
jako	jako	k8xC	jako
Svědkové	svědek	k1gMnPc1	svědek
Jehovovi	Jehovův	k2eAgMnPc1d1	Jehovův
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1896	[number]	k4	1896
do	do	k7c2	do
1915	[number]	k4	1915
jejich	jejich	k3xOp3gFnSc4	jejich
dům	dům	k1gInSc1	dům
sloužil	sloužit	k5eAaImAgInS	sloužit
jako	jako	k9	jako
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
místní	místní	k2eAgFnSc1d1	místní
skupina	skupina	k1gFnSc1	skupina
badatelů	badatel	k1gMnPc2	badatel
Bible	bible	k1gFnSc2	bible
scházela	scházet	k5eAaImAgFnS	scházet
<g/>
.	.	kIx.	.
</s>
<s>
Svědkové	svědek	k1gMnPc1	svědek
Jehovovi	Jehovův	k2eAgMnPc1d1	Jehovův
jsou	být	k5eAaImIp3nP	být
silně	silně	k6eAd1	silně
zaměřeni	zaměřit	k5eAaPmNgMnP	zaměřit
proti	proti	k7c3	proti
vojenství	vojenství	k1gNnSc3	vojenství
a	a	k8xC	a
válce	válka	k1gFnSc3	válka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
výrazně	výrazně	k6eAd1	výrazně
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
život	život	k1gInSc4	život
Dwighta	Dwight	k1gMnSc2	Dwight
Eisenhowera	Eisenhower	k1gMnSc2	Eisenhower
<g/>
.	.	kIx.	.
</s>
<s>
Projevilo	projevit	k5eAaPmAgNnS	projevit
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
nastoupit	nastoupit	k5eAaPmF	nastoupit
na	na	k7c4	na
United	United	k1gInSc4	United
States	States	k1gMnSc1	States
Military	Militara	k1gFnSc2	Militara
Academy	Academa	k1gFnSc2	Academa
(	(	kIx(	(
<g/>
Vojenskou	vojenský	k2eAgFnSc4d1	vojenská
akademii	akademie	k1gFnSc4	akademie
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
)	)	kIx)	)
v	v	k7c6	v
městě	město	k1gNnSc6	město
West	Westum	k1gNnPc2	Westum
Point	pointa	k1gFnPc2	pointa
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
také	také	k9	také
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
dům	dům	k1gInSc1	dům
jeho	jeho	k3xOp3gFnSc2	jeho
matky	matka	k1gFnSc2	matka
už	už	k6eAd1	už
nebyl	být	k5eNaImAgMnS	být
místem	místo	k1gNnSc7	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
badatelé	badatel	k1gMnPc1	badatel
Bible	bible	k1gFnSc2	bible
mohli	moct	k5eAaImAgMnP	moct
scházet	scházet	k5eAaImF	scházet
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
tak	tak	k6eAd1	tak
dlouhodobý	dlouhodobý	k2eAgInSc1d1	dlouhodobý
ideový	ideový	k2eAgInSc1d1	ideový
spor	spor	k1gInSc1	spor
mezi	mezi	k7c7	mezi
matkou	matka	k1gFnSc7	matka
a	a	k8xC	a
synem	syn	k1gMnSc7	syn
(	(	kIx(	(
<g/>
jakož	jakož	k8xC	jakož
i	i	k9	i
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgFnPc2d1	další
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
výrazně	výrazně	k6eAd1	výrazně
nesouhlasila	souhlasit	k5eNaImAgFnS	souhlasit
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
kariérou	kariéra	k1gFnSc7	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
spolu	spolu	k6eAd1	spolu
nadále	nadále	k6eAd1	nadále
udržovali	udržovat	k5eAaImAgMnP	udržovat
velmi	velmi	k6eAd1	velmi
blízký	blízký	k2eAgInSc4d1	blízký
vztah	vztah	k1gInSc4	vztah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzdělání	vzdělání	k1gNnSc1	vzdělání
==	==	k?	==
</s>
</p>
<p>
<s>
Společně	společně	k6eAd1	společně
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
šesti	šest	k4xCc7	šest
bratry	bratr	k1gMnPc7	bratr
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
Abilenejskou	Abilenejský	k2eAgFnSc4d1	Abilenejský
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
a	a	k8xC	a
přijal	přijmout	k5eAaPmAgInS	přijmout
práci	práce	k1gFnSc4	práce
nočního	noční	k2eAgMnSc2d1	noční
předáka	předák	k1gMnSc2	předák
v	v	k7c6	v
mlékárně	mlékárna	k1gFnSc6	mlékárna
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pomohl	pomoct	k5eAaPmAgInS	pomoct
svému	svůj	k3xOyFgMnSc3	svůj
bratru	bratr	k1gMnSc3	bratr
Edgarovi	Edgar	k1gMnSc3	Edgar
zaplatit	zaplatit	k5eAaPmF	zaplatit
vysokoškolské	vysokoškolský	k2eAgNnSc4d1	vysokoškolské
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
pracovní	pracovní	k2eAgFnSc6d1	pracovní
pauze	pauza	k1gFnSc6	pauza
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
testy	test	k1gInPc7	test
na	na	k7c6	na
Námořní	námořní	k2eAgFnSc6d1	námořní
akademii	akademie	k1gFnSc6	akademie
<g/>
,	,	kIx,	,
kterými	který	k3yIgMnPc7	který
prošel	projít	k5eAaPmAgMnS	projít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
protože	protože	k8xS	protože
byl	být	k5eAaImAgInS	být
starý	starý	k2eAgInSc1d1	starý
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jej	on	k3xPp3gMnSc4	on
přijali	přijmout	k5eAaPmAgMnP	přijmout
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
být	být	k5eAaImF	být
doporučen	doporučit	k5eAaPmNgMnS	doporučit
kansaským	kansaský	k2eAgMnSc7d1	kansaský
senátorem	senátor	k1gMnSc7	senátor
Josephem	Joseph	k1gInSc7	Joseph
L.	L.	kA	L.
Bristowem	Bristowem	k1gInSc1	Bristowem
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
byli	být	k5eAaImAgMnP	být
proti	proti	k7c3	proti
vojenství	vojenství	k1gNnSc3	vojenství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzdělání	vzdělání	k1gNnSc2	vzdělání
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
přednější	přední	k2eAgNnSc4d2	přednější
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svých	svůj	k3xOyFgNnPc2	svůj
studií	studio	k1gNnPc2	studio
na	na	k7c6	na
akademii	akademie	k1gFnSc6	akademie
dosahoval	dosahovat	k5eAaImAgMnS	dosahovat
skvělých	skvělý	k2eAgInPc2d1	skvělý
sportovních	sportovní	k2eAgInPc2d1	sportovní
výsledků	výsledek	k1gInPc2	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
člen	člen	k1gMnSc1	člen
školního	školní	k2eAgInSc2d1	školní
fotbalového	fotbalový	k2eAgInSc2d1	fotbalový
týmu	tým	k1gInSc2	tým
získal	získat	k5eAaPmAgInS	získat
i	i	k9	i
několik	několik	k4yIc4	několik
cen	cena	k1gFnPc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
jeho	jeho	k3xOp3gFnSc1	jeho
slibná	slibný	k2eAgFnSc1d1	slibná
sportovní	sportovní	k2eAgFnSc1d1	sportovní
kariéra	kariéra	k1gFnSc1	kariéra
fotbalisty	fotbalista	k1gMnSc2	fotbalista
skončila	skončit	k5eAaPmAgFnS	skončit
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
kvůli	kvůli	k7c3	kvůli
zranění	zranění	k1gNnSc3	zranění
<g/>
.	.	kIx.	.
</s>
<s>
Akademii	akademie	k1gFnSc4	akademie
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
svého	svůj	k3xOyFgInSc2	svůj
ročníku	ročník	k1gInSc2	ročník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
První	první	k4xOgFnPc1	první
vojenské	vojenský	k2eAgFnPc1d1	vojenská
zkušenosti	zkušenost	k1gFnPc1	zkušenost
==	==	k?	==
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
sloužil	sloužit	k5eAaImAgMnS	sloužit
u	u	k7c2	u
pěchoty	pěchota	k1gFnSc2	pěchota
na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
a	a	k8xC	a
v	v	k7c6	v
Georgii	Georgie	k1gFnSc6	Georgie
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
velitelem	velitel	k1gMnSc7	velitel
tankové	tankový	k2eAgFnSc2d1	tanková
jednotky	jednotka	k1gFnSc2	jednotka
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
dočasně	dočasně	k6eAd1	dočasně
povýšen	povýšit	k5eAaPmNgMnS	povýšit
do	do	k7c2	do
hodnosti	hodnost	k1gFnSc2	hodnost
podplukovníka	podplukovník	k1gMnSc2	podplukovník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc4	první
světovou	světový	k2eAgFnSc4d1	světová
válku	válka	k1gFnSc4	válka
strávil	strávit	k5eAaPmAgMnS	strávit
jako	jako	k9	jako
instruktor	instruktor	k1gMnSc1	instruktor
a	a	k8xC	a
cvičil	cvičit	k5eAaImAgMnS	cvičit
posádky	posádka	k1gFnPc4	posádka
tanků	tank	k1gInPc2	tank
v	v	k7c6	v
Pensylvánii	Pensylvánie	k1gFnSc6	Pensylvánie
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nikdy	nikdy	k6eAd1	nikdy
nezažil	zažít	k5eNaPmAgMnS	zažít
ani	ani	k8xC	ani
neviděl	vidět	k5eNaImAgMnS	vidět
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
navrácena	navrácen	k2eAgFnSc1d1	navrácena
jeho	jeho	k3xOp3gFnSc1	jeho
původní	původní	k2eAgFnSc1d1	původní
mírová	mírový	k2eAgFnSc1d1	mírová
hodnost	hodnost	k1gFnSc1	hodnost
kapitána	kapitán	k1gMnSc2	kapitán
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
následně	následně	k6eAd1	následně
byl	být	k5eAaImAgMnS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
majora	major	k1gMnSc4	major
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
zůstal	zůstat	k5eAaPmAgMnS	zůstat
u	u	k7c2	u
tankové	tankový	k2eAgFnSc2d1	tanková
divize	divize	k1gFnSc2	divize
jako	jako	k8xC	jako
instruktor	instruktor	k1gMnSc1	instruktor
a	a	k8xC	a
během	během	k7c2	během
tohoto	tento	k3xDgInSc2	tento
pobytu	pobyt	k1gInSc2	pobyt
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
styku	styk	k1gInSc6	styk
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
americkými	americký	k2eAgMnPc7d1	americký
tankovými	tankový	k2eAgMnPc7d1	tankový
veliteli	velitel	k1gMnPc7	velitel
včetně	včetně	k7c2	včetně
George	Georg	k1gInSc2	Georg
S.	S.	kA	S.
Pattona	Patton	k1gMnSc2	Patton
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
výkonným	výkonný	k2eAgMnSc7d1	výkonný
důstojníkem	důstojník	k1gMnSc7	důstojník
u	u	k7c2	u
generála	generál	k1gMnSc4	generál
Foxe	fox	k1gInSc5	fox
Connera	Conner	k1gMnSc4	Conner
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
panamského	panamský	k2eAgInSc2d1	panamský
kanálu	kanál	k1gInSc2	kanál
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sloužil	sloužit	k5eAaImAgMnS	sloužit
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
Connerovým	Connerův	k2eAgNnSc7d1	Connerův
vedením	vedení	k1gNnSc7	vedení
věnoval	věnovat	k5eAaImAgMnS	věnovat
svůj	svůj	k3xOyFgInSc4	svůj
volný	volný	k2eAgInSc4d1	volný
čas	čas	k1gInSc4	čas
studiu	studio	k1gNnSc3	studio
historické	historický	k2eAgFnSc2d1	historická
a	a	k8xC	a
teoretické	teoretický	k2eAgFnSc2d1	teoretická
části	část	k1gFnSc2	část
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
mělo	mít	k5eAaImAgNnS	mít
možná	možná	k9	možná
největší	veliký	k2eAgInSc4d3	veliký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
kariéru	kariéra	k1gFnSc4	kariéra
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
na	na	k7c6	na
celé	celá	k1gFnSc6	celá
jeho	jeho	k3xOp3gNnSc4	jeho
vojenské	vojenský	k2eAgNnSc4d1	vojenské
myšlení	myšlení	k1gNnSc4	myšlení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1925	[number]	k4	1925
<g/>
–	–	k?	–
<g/>
1926	[number]	k4	1926
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
velitelskou	velitelský	k2eAgFnSc4d1	velitelská
školu	škola	k1gFnSc4	škola
ve	v	k7c4	v
Forth	Forth	k1gInSc4	Forth
Leavenworth	Leavenwortha	k1gFnPc2	Leavenwortha
v	v	k7c6	v
Kansasu	Kansas	k1gInSc6	Kansas
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
jako	jako	k8xC	jako
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
ročníku	ročník	k1gInSc6	ročník
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k9	jako
velitel	velitel	k1gMnSc1	velitel
praporu	prapor	k1gInSc2	prapor
v	v	k7c6	v
pevnosti	pevnost	k1gFnSc6	pevnost
Benning	Benning	k1gInSc4	Benning
v	v	k7c6	v
Georgii	Georgie	k1gFnSc6	Georgie
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
dvacátých	dvacátý	k4xOgNnPc2	dvacátý
a	a	k8xC	a
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
jeho	jeho	k3xOp3gFnSc1	jeho
kariéra	kariéra	k1gFnSc1	kariéra
stagnovala	stagnovat	k5eAaImAgFnS	stagnovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
byl	být	k5eAaImAgInS	být
mír	mír	k1gInSc1	mír
<g/>
.	.	kIx.	.
</s>
<s>
Nebylo	být	k5eNaImAgNnS	být
tedy	tedy	k9	tedy
možné	možný	k2eAgNnSc1d1	možné
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
projevit	projevit	k5eAaPmF	projevit
jeho	jeho	k3xOp3gFnSc1	jeho
vojenská	vojenský	k2eAgFnSc1d1	vojenská
genialita	genialita	k1gFnSc1	genialita
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
zcela	zcela	k6eAd1	zcela
nepochybně	pochybně	k6eNd1	pochybně
oplýval	oplývat	k5eAaImAgMnS	oplývat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
mnoho	mnoho	k6eAd1	mnoho
jeho	jeho	k3xOp3gMnPc2	jeho
přátel	přítel	k1gMnPc2	přítel
odešlo	odejít	k5eAaPmAgNnS	odejít
z	z	k7c2	z
americké	americký	k2eAgFnSc2d1	americká
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
hledalo	hledat	k5eAaImAgNnS	hledat
své	svůj	k3xOyFgInPc4	svůj
uplatnění	uplatnění	k1gNnSc4	uplatnění
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
nevojenských	vojenský	k2eNgFnPc6d1	nevojenská
oblastech	oblast	k1gFnPc6	oblast
a	a	k8xC	a
lukrativnějších	lukrativní	k2eAgNnPc6d2	lukrativnější
odvětvích	odvětví	k1gNnPc6	odvětví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
"	"	kIx"	"
<g/>
nudy	nuda	k1gFnSc2	nuda
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
American	Americana	k1gFnPc2	Americana
Battle	Battle	k1gFnSc2	Battle
Monuments	Monumentsa	k1gFnPc2	Monumentsa
Commission	Commission	k1gInSc1	Commission
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
vedena	vést	k5eAaImNgFnS	vést
generálem	generál	k1gMnSc7	generál
Johnem	John	k1gMnSc7	John
J.	J.	kA	J.
Pershingem	Pershing	k1gInSc7	Pershing
<g/>
,	,	kIx,	,
záhy	záhy	k6eAd1	záhy
také	také	k9	také
do	do	k7c2	do
Army	Arma	k1gMnSc2	Arma
War	War	k1gMnSc2	War
College	Colleg	k1gMnSc2	Colleg
a	a	k8xC	a
poté	poté	k6eAd1	poté
sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k8xS	jako
výkonný	výkonný	k2eAgMnSc1d1	výkonný
důstojník	důstojník	k1gMnSc1	důstojník
Generála	generál	k1gMnSc4	generál
George	Georg	k1gMnSc4	Georg
V.	V.	kA	V.
Moselyho	Mosely	k1gMnSc4	Mosely
jako	jako	k8xC	jako
Assistant	Assistant	k1gMnSc1	Assistant
Secretary	Secretara	k1gFnSc2	Secretara
of	of	k?	of
War	War	k1gFnSc2	War
od	od	k7c2	od
1923	[number]	k4	1923
do	do	k7c2	do
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
přišlo	přijít	k5eAaPmAgNnS	přijít
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgNnPc2d3	nejdůležitější
období	období	k1gNnPc2	období
jeho	jeho	k3xOp3gInSc2	jeho
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
sloužil	sloužit	k5eAaImAgInS	sloužit
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Chief	Chief	k1gInSc1	Chief
military	militara	k1gFnSc2	militara
aide	aid	k1gFnSc2	aid
<g/>
"	"	kIx"	"
u	u	k7c2	u
generála	generál	k1gMnSc2	generál
Douglase	Douglasa	k1gFnSc3	Douglasa
MacArthura	MacArthura	k1gFnSc1	MacArthura
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
k	k	k7c3	k
"	"	kIx"	"
<g/>
Army	Arma	k1gMnSc2	Arma
Chief	Chief	k1gInSc1	Chief
of	of	k?	of
Staff	Staff	k1gInSc1	Staff
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zůstal	zůstat	k5eAaPmAgMnS	zůstat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
společně	společně	k6eAd1	společně
s	s	k7c7	s
generálem	generál	k1gMnSc7	generál
MacArthurem	MacArthur	k1gMnSc7	MacArthur
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
na	na	k7c4	na
Filipíny	Filipíny	k1gFnPc4	Filipíny
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k8xS	jako
vojenský	vojenský	k2eAgMnSc1d1	vojenský
poradce	poradce	k1gMnSc1	poradce
filipínské	filipínský	k2eAgFnSc3d1	filipínská
vládě	vláda	k1gFnSc3	vláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
ta	ten	k3xDgFnSc1	ten
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jej	on	k3xPp3gMnSc4	on
mohla	moct	k5eAaImAgFnS	moct
připravit	připravit	k5eAaPmF	připravit
na	na	k7c4	na
"	"	kIx"	"
<g/>
ego	ego	k1gNnSc4	ego
<g/>
"	"	kIx"	"
britského	britský	k2eAgMnSc2d1	britský
premiéra	premiér	k1gMnSc2	premiér
Winstona	Winston	k1gMnSc4	Winston
Churchilla	Churchill	k1gMnSc4	Churchill
<g/>
,	,	kIx,	,
generála	generál	k1gMnSc4	generál
George	Georg	k1gMnSc4	Georg
S.	S.	kA	S.
Pattona	Patton	k1gMnSc4	Patton
a	a	k8xC	a
Bernarda	Bernard	k1gMnSc4	Bernard
Law	Law	k1gMnSc4	Law
Montgomeryho	Montgomery	k1gMnSc4	Montgomery
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yIgInPc7	který
musel	muset	k5eAaImAgInS	muset
během	běh	k1gInSc7	běh
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
často	často	k6eAd1	často
jednat	jednat	k5eAaImF	jednat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
16	[number]	k4	16
letech	léto	k1gNnPc6	léto
povýšen	povýšit	k5eAaPmNgMnS	povýšit
z	z	k7c2	z
majora	major	k1gMnSc2	major
do	do	k7c2	do
hodnosti	hodnost	k1gFnSc2	hodnost
podplukovníka	podplukovník	k1gMnSc2	podplukovník
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
naučit	naučit	k5eAaPmF	naučit
létat	létat	k5eAaImF	létat
<g/>
,	,	kIx,	,
vyvrcholením	vyvrcholení	k1gNnSc7	vyvrcholení
jeho	jeho	k3xOp3gFnSc2	jeho
letecké	letecký	k2eAgFnSc2d1	letecká
pilotní	pilotní	k2eAgFnSc2d1	pilotní
kariéry	kariéra	k1gFnSc2	kariéra
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
stal	stát	k5eAaPmAgInS	stát
jeho	jeho	k3xOp3gInSc1	jeho
přelet	přelet	k1gInSc1	přelet
na	na	k7c4	na
Filipíny	Filipíny	k1gFnPc4	Filipíny
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
všemu	všecek	k3xTgNnSc3	všecek
nikdy	nikdy	k6eAd1	nikdy
nebyl	být	k5eNaImAgMnS	být
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
armádě	armáda	k1gFnSc6	armáda
veden	vést	k5eAaImNgMnS	vést
jako	jako	k8xC	jako
vojenský	vojenský	k2eAgMnSc1d1	vojenský
pilot	pilot	k1gMnSc1	pilot
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
vlasti	vlast	k1gFnSc2	vlast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
pracoval	pracovat	k5eAaImAgMnS	pracovat
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
<g/>
,	,	kIx,	,
D.C.	D.C.	k1gFnSc6	D.C.
<g/>
,	,	kIx,	,
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
a	a	k8xC	a
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
funkcích	funkce	k1gFnPc6	funkce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1941	[number]	k4	1941
se	se	k3xPyFc4	se
sešel	sejít	k5eAaPmAgMnS	sejít
s	s	k7c7	s
"	"	kIx"	"
<g/>
Chief	Chief	k1gMnSc1	Chief
of	of	k?	of
Staff	Staff	k1gMnSc1	Staff
<g/>
"	"	kIx"	"
generálem	generál	k1gMnSc7	generál
Walterem	Walter	k1gMnSc7	Walter
Kruegerem	Krueger	k1gMnSc7	Krueger
<g/>
,	,	kIx,	,
velitelem	velitel	k1gMnSc7	velitel
třetí	třetí	k4xOgFnSc2	třetí
americké	americký	k2eAgFnSc2d1	americká
armády	armáda	k1gFnSc2	armáda
ve	v	k7c6	v
Fort	Fort	k?	Fort
Sam	Sam	k1gMnSc1	Sam
Houston	Houston	k1gInSc1	Houston
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1941	[number]	k4	1941
byl	být	k5eAaImAgMnS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
do	do	k7c2	do
hodnosti	hodnost	k1gFnSc2	hodnost
"	"	kIx"	"
<g/>
Brigadier	Brigadier	k1gMnSc1	Brigadier
General	General	k1gMnSc1	General
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
brigádní	brigádní	k2eAgMnSc1d1	brigádní
generál	generál	k1gMnSc1	generál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
jeho	jeho	k3xOp3gFnPc1	jeho
velitelské	velitelský	k2eAgFnPc1d1	velitelská
schopnosti	schopnost	k1gFnPc1	schopnost
byly	být	k5eAaImAgFnP	být
jeho	jeho	k3xOp3gMnSc7	jeho
nadřízeným	nadřízený	k1gMnSc7	nadřízený
známy	znám	k2eAgInPc4d1	znám
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
nepočítalo	počítat	k5eNaImAgNnS	počítat
jako	jako	k8xS	jako
s	s	k7c7	s
někým	někdo	k3yInSc7	někdo
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
vést	vést	k5eAaImF	vést
důležité	důležitý	k2eAgFnPc4d1	důležitá
operace	operace	k1gFnPc4	operace
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
japonském	japonský	k2eAgInSc6d1	japonský
útoku	útok	k1gInSc6	útok
na	na	k7c4	na
Pearl	Pearl	k1gInSc4	Pearl
Harbor	Harbor	k1gInSc1	Harbor
byl	být	k5eAaImAgInS	být
převelen	převelet	k5eAaPmNgInS	převelet
do	do	k7c2	do
Generálního	generální	k2eAgInSc2d1	generální
štábu	štáb	k1gInSc2	štáb
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sloužil	sloužit	k5eAaImAgMnS	sloužit
do	do	k7c2	do
června	červen	k1gInSc2	červen
1942	[number]	k4	1942
<g/>
.	.	kIx.	.
</s>
<s>
Zodpovídal	zodpovídat	k5eAaImAgMnS	zodpovídat
za	za	k7c4	za
vytváření	vytváření	k1gNnSc4	vytváření
vojenských	vojenský	k2eAgInPc2d1	vojenský
plánů	plán	k1gInPc2	plán
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
měly	mít	k5eAaImAgFnP	mít
zajistit	zajistit	k5eAaPmF	zajistit
porážku	porážka	k1gFnSc4	porážka
Japonského	japonský	k2eAgNnSc2d1	Japonské
císařství	císařství	k1gNnSc2	císařství
a	a	k8xC	a
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1942	[number]	k4	1942
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
do	do	k7c2	do
"	"	kIx"	"
<g/>
Deputy	Deput	k1gInPc1	Deput
Chief	Chief	k1gInSc1	Chief
in	in	k?	in
charge	charg	k1gFnSc2	charg
of	of	k?	of
Pacific	Pacific	k1gMnSc1	Pacific
Defenses	Defenses	k1gMnSc1	Defenses
under	under	k1gMnSc1	under
the	the	k?	the
Chief	Chief	k1gMnSc1	Chief
of	of	k?	of
the	the	k?	the
War	War	k1gFnSc2	War
Plans	Plansa	k1gFnPc2	Plansa
Division	Division	k1gInSc1	Division
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Všude	všude	k6eAd1	všude
sklízel	sklízet	k5eAaImAgMnS	sklízet
uznání	uznání	k1gNnSc4	uznání
a	a	k8xC	a
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgMnS	být
nakonec	nakonec	k6eAd1	nakonec
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
"	"	kIx"	"
<g/>
Assistant	Assistant	k1gMnSc1	Assistant
Chief	Chief	k1gMnSc1	Chief
of	of	k?	of
Staff	Staff	k1gMnSc1	Staff
in	in	k?	in
charge	charge	k1gInSc1	charge
of	of	k?	of
Operation	Operation	k1gInSc1	Operation
Division	Division	k1gInSc1	Division
under	under	k1gInSc1	under
Chief	Chief	k1gInSc1	Chief
of	of	k?	of
Staff	Staff	k1gInSc1	Staff
General	General	k1gMnSc2	General
George	Georg	k1gMnSc2	Georg
C.	C.	kA	C.
Marshall	Marshall	k1gMnSc1	Marshall
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
George	Georg	k1gMnSc2	Georg
Catlett	Catlett	k1gMnSc1	Catlett
Marshall	Marshall	k1gMnSc1	Marshall
si	se	k3xPyFc3	se
plně	plně	k6eAd1	plně
uvědomoval	uvědomovat	k5eAaImAgMnS	uvědomovat
a	a	k8xC	a
znal	znát	k5eAaImAgMnS	znát
jeho	jeho	k3xOp3gInPc4	jeho
skvělé	skvělý	k2eAgInPc4d1	skvělý
organizační	organizační	k2eAgInPc4d1	organizační
a	a	k8xC	a
administrativní	administrativní	k2eAgFnPc4d1	administrativní
schopnosti	schopnost	k1gFnPc4	schopnost
a	a	k8xC	a
pomohl	pomoct	k5eAaPmAgInS	pomoct
mu	on	k3xPp3gMnSc3	on
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
mezi	mezi	k7c7	mezi
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Senior	senior	k1gMnSc1	senior
Command	Commanda	k1gFnPc2	Commanda
Positions	Positionsa	k1gFnPc2	Positionsa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
byl	být	k5eAaImAgInS	být
jmenován	jmenovat	k5eAaImNgInS	jmenovat
"	"	kIx"	"
<g/>
Commanding	Commanding	k1gInSc1	Commanding
General	General	k1gFnSc2	General
<g/>
,	,	kIx,	,
European	European	k1gMnSc1	European
Theater	Theater	k1gMnSc1	Theater
of	of	k?	of
Operation	Operation	k1gInSc1	Operation
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ETOUSA	ETOUSA	kA	ETOUSA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
sídlila	sídlit	k5eAaImAgFnS	sídlit
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
byl	být	k5eAaImAgInS	být
také	také	k9	také
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
"	"	kIx"	"
<g/>
Supreme	Suprem	k1gInSc5	Suprem
Commander	Commandero	k1gNnPc2	Commandero
Allied	Allied	k1gMnSc1	Allied
Expeditionary	Expeditionara	k1gFnSc2	Expeditionara
Force	force	k1gFnSc2	force
of	of	k?	of
the	the	k?	the
North	North	k1gMnSc1	North
African	African	k1gMnSc1	African
Theater	Theater	k1gMnSc1	Theater
of	of	k?	of
Operations	Operations	k1gInSc1	Operations
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
NATOUSA	NATOUSA	kA	NATOUSA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1943	[number]	k4	1943
začali	začít	k5eAaPmAgMnP	začít
také	také	k9	také
jeho	jeho	k3xOp3gFnSc7	jeho
zásluhou	zásluha	k1gFnSc7	zásluha
spojenci	spojenec	k1gMnPc7	spojenec
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
postupovat	postupovat	k5eAaImF	postupovat
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgInS	získat
si	se	k3xPyFc3	se
tím	ten	k3xDgNnSc7	ten
respekt	respekt	k1gInSc4	respekt
všech	všecek	k3xTgMnPc2	všecek
generálů	generál	k1gMnPc2	generál
včetně	včetně	k7c2	včetně
arogantního	arogantní	k2eAgMnSc2d1	arogantní
britského	britský	k2eAgMnSc2d1	britský
generála	generál	k1gMnSc2	generál
Bernarda	Bernard	k1gMnSc2	Bernard
Law	Law	k1gMnSc2	Law
Montgomeryho	Montgomery	k1gMnSc2	Montgomery
-	-	kIx~	-
velitele	velitel	k1gMnSc2	velitel
osmé	osmý	k4xOgFnSc2	osmý
britské	britský	k2eAgFnSc2d1	britská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Mohlo	moct	k5eAaImAgNnS	moct
konečně	konečně	k6eAd1	konečně
začít	začít	k5eAaPmF	začít
také	také	k9	také
tuniské	tuniský	k2eAgNnSc1d1	tuniské
tažení	tažení	k1gNnSc1	tažení
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
kterému	který	k3yRgInSc3	který
se	se	k3xPyFc4	se
vzdal	vzdát	k5eAaPmAgMnS	vzdát
velení	velení	k1gNnSc2	velení
ETOUSA	ETOUSA	kA	ETOUSA
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
plně	plně	k6eAd1	plně
věnovat	věnovat	k5eAaPmF	věnovat
vedení	vedení	k1gNnSc4	vedení
NATOUSA	NATOUSA	kA	NATOUSA
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
fašistických	fašistický	k2eAgFnPc2d1	fašistická
sil	síla	k1gFnPc2	síla
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
byl	být	k5eAaImAgInS	být
převelen	převelet	k5eAaPmNgInS	převelet
k	k	k7c3	k
"	"	kIx"	"
<g/>
Mediterranean	Mediterranean	k1gMnSc1	Mediterranean
Theater	Theater	k1gMnSc1	Theater
of	of	k?	of
Operations	Operations	k1gInSc1	Operations
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
MTO	MTO	kA	MTO
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
pro	pro	k7c4	pro
něj	on	k3xPp3gInSc4	on
další	další	k2eAgFnSc1d1	další
výzva	výzva	k1gFnSc1	výzva
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
měl	mít	k5eAaImAgInS	mít
vymyslet	vymyslet	k5eAaPmF	vymyslet
plán	plán	k1gInSc1	plán
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
zajistil	zajistit	k5eAaPmAgInS	zajistit
úspěšné	úspěšný	k2eAgNnSc4d1	úspěšné
vylodění	vylodění	k1gNnSc4	vylodění
spojenců	spojenec	k1gMnPc2	spojenec
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
přišel	přijít	k5eAaPmAgMnS	přijít
s	s	k7c7	s
plánem	plán	k1gInSc7	plán
invaze	invaze	k1gFnSc2	invaze
na	na	k7c4	na
Sicílii	Sicílie	k1gFnSc4	Sicílie
a	a	k8xC	a
na	na	k7c4	na
Apeninský	apeninský	k2eAgInSc4d1	apeninský
poloostrov	poloostrov	k1gInSc4	poloostrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1943	[number]	k4	1943
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
přislíbena	přislíben	k2eAgFnSc1d1	přislíbena
hodnost	hodnost	k1gFnSc1	hodnost
"	"	kIx"	"
<g/>
Supreme	Suprem	k1gInSc5	Suprem
Allied	Allied	k1gMnSc1	Allied
Commander	Commandra	k1gFnPc2	Commandra
in	in	k?	in
Europe	Europ	k1gInSc5	Europ
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
SHAEF	SHAEF	kA	SHAEF
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1944	[number]	k4	1944
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
jako	jako	k9	jako
velitel	velitel	k1gMnSc1	velitel
EUTOUSA	EUTOUSA	kA	EUTOUSA
a	a	k8xC	a
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
měsících	měsíc	k1gInPc6	měsíc
byl	být	k5eAaImAgMnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
velitelem	velitel	k1gMnSc7	velitel
"	"	kIx"	"
<g/>
SHAEF	SHAEF	kA	SHAEF
<g/>
"	"	kIx"	"
kde	kde	k6eAd1	kde
plnil	plnit	k5eAaImAgInS	plnit
nezbytnou	nezbytný	k2eAgFnSc4d1	nezbytná
roli	role	k1gFnSc4	role
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
procesu	proces	k1gInSc6	proces
osvobozování	osvobozování	k1gNnSc2	osvobozování
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
hodnostech	hodnost	k1gFnPc6	hodnost
byl	být	k5eAaImAgInS	být
pověřen	pověřit	k5eAaPmNgInS	pověřit
dalším	další	k2eAgInSc7d1	další
úkolem	úkol	k1gInSc7	úkol
-	-	kIx~	-
vyloděním	vylodění	k1gNnPc3	vylodění
spojenců	spojenec	k1gMnPc2	spojenec
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
s	s	k7c7	s
krycím	krycí	k2eAgInSc7d1	krycí
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Operation	Operation	k1gInSc1	Operation
Overlord	Overlord	k1gInSc1	Overlord
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
osvobodit	osvobodit	k5eAaPmF	osvobodit
Evropu	Evropa	k1gFnSc4	Evropa
a	a	k8xC	a
porazit	porazit	k5eAaPmF	porazit
nacistické	nacistický	k2eAgNnSc4d1	nacistické
Německo	Německo	k1gNnSc4	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Operace	operace	k1gFnSc1	operace
Overlord	Overlorda	k1gFnPc2	Overlorda
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
Den	den	k1gInSc1	den
D	D	kA	D
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
dále	daleko	k6eAd2	daleko
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
velitel	velitel	k1gMnSc1	velitel
AFHQ	AFHQ	kA	AFHQ
<g/>
,	,	kIx,	,
naplánovat	naplánovat	k5eAaBmF	naplánovat
vylodění	vylodění	k1gNnSc4	vylodění
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Francii	Francie	k1gFnSc6	Francie
(	(	kIx(	(
<g/>
Operace	operace	k1gFnSc1	operace
Dragoon	Dragoon	k1gMnSc1	Dragoon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
války	válka	k1gFnSc2	válka
zodpovídal	zodpovídat	k5eAaPmAgMnS	zodpovídat
za	za	k7c4	za
všechna	všechen	k3xTgNnPc4	všechen
americká	americký	k2eAgNnPc4d1	americké
vojska	vojsko	k1gNnPc4	vojsko
na	na	k7c6	na
Západní	západní	k2eAgFnSc6d1	západní
frontě	fronta	k1gFnSc6	fronta
severně	severně	k6eAd1	severně
od	od	k7c2	od
Alp	Alpy	k1gFnPc2	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
uznáni	uznán	k2eAgMnPc1d1	uznán
mezi	mezi	k7c7	mezi
"	"	kIx"	"
<g/>
senior	senior	k1gMnSc1	senior
commander	commander	k1gMnSc1	commander
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgInS	být
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1944	[number]	k4	1944
povýšen	povýšit	k5eAaPmNgInS	povýšit
do	do	k7c2	do
hodnosti	hodnost	k1gFnSc2	hodnost
"	"	kIx"	"
<g/>
General	General	k1gFnSc1	General
of	of	k?	of
the	the	k?	the
Army	Arma	k1gFnSc2	Arma
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
americkou	americký	k2eAgFnSc7d1	americká
obdobou	obdoba	k1gFnSc7	obdoba
polního	polní	k2eAgMnSc4d1	polní
maršála	maršál	k1gMnSc4	maršál
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
evropských	evropský	k2eAgFnPc2d1	Evropská
armád	armáda	k1gFnPc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
a	a	k8xC	a
předešlých	předešlý	k2eAgFnPc6d1	předešlá
hodnostech	hodnost	k1gFnPc6	hodnost
ukázal	ukázat	k5eAaPmAgInS	ukázat
svůj	svůj	k3xOyFgInSc4	svůj
velký	velký	k2eAgInSc4d1	velký
talent	talent	k1gInSc4	talent
ve	v	k7c4	v
vedení	vedení	k1gNnSc4	vedení
a	a	k8xC	a
smysl	smysl	k1gInSc4	smysl
pro	pro	k7c4	pro
diplomacii	diplomacie	k1gFnSc4	diplomacie
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
nikdy	nikdy	k6eAd1	nikdy
přímo	přímo	k6eAd1	přímo
nezažil	zažít	k5eNaPmAgMnS	zažít
žádnou	žádný	k3yNgFnSc4	žádný
válku	válka	k1gFnSc4	válka
a	a	k8xC	a
bitvu	bitva	k1gFnSc4	bitva
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
kůži	kůže	k1gFnSc4	kůže
<g/>
,	,	kIx,	,
vydobyl	vydobýt	k5eAaPmAgMnS	vydobýt
si	se	k3xPyFc3	se
respekt	respekt	k1gInSc4	respekt
předních	přední	k2eAgMnPc2d1	přední
generálů	generál	k1gMnPc2	generál
a	a	k8xC	a
politických	politický	k2eAgMnPc2d1	politický
vůdců	vůdce	k1gMnPc2	vůdce
<g/>
.	.	kIx.	.
</s>
<s>
Vypořádával	vypořádávat	k5eAaImAgMnS	vypořádávat
se	se	k3xPyFc4	se
s	s	k7c7	s
vojenským	vojenský	k2eAgNnSc7d1	vojenské
uměním	umění	k1gNnSc7	umění
i	i	k8xC	i
osobními	osobní	k2eAgFnPc7d1	osobní
ambicemi	ambice	k1gFnPc7	ambice
a	a	k8xC	a
egoismem	egoismus	k1gInSc7	egoismus
svých	svůj	k3xOyFgFnPc2	svůj
problematických	problematický	k2eAgFnPc2d1	problematická
podřízených	podřízená	k1gFnPc2	podřízená
jako	jako	k8xC	jako
byl	být	k5eAaImAgMnS	být
např.	např.	kA	např.
Omar	Omar	k1gMnSc1	Omar
Bradley	Bradlea	k1gMnSc2	Bradlea
<g/>
,	,	kIx,	,
George	Georg	k1gMnSc2	Georg
Patton	Patton	k1gInSc1	Patton
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
jemu	on	k3xPp3gMnSc3	on
podřízení	podřízený	k2eAgMnPc1d1	podřízený
velitelé	velitel	k1gMnPc1	velitel
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
často	často	k6eAd1	často
zastávali	zastávat	k5eAaImAgMnP	zastávat
odlišnou	odlišný	k2eAgFnSc4d1	odlišná
taktiku	taktika	k1gFnSc4	taktika
pro	pro	k7c4	pro
osvobozování	osvobozování	k1gNnSc4	osvobozování
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
také	také	k9	také
o	o	k7c4	o
takové	takový	k3xDgFnPc4	takový
osobnosti	osobnost	k1gFnPc4	osobnost
jako	jako	k8xS	jako
byli	být	k5eAaImAgMnP	být
Winston	Winston	k1gInSc4	Winston
Churchill	Churchilla	k1gFnPc2	Churchilla
<g/>
,	,	kIx,	,
britský	britský	k2eAgMnSc1d1	britský
polní	polní	k2eAgMnSc1d1	polní
maršál	maršál	k1gMnSc1	maršál
Bernard	Bernard	k1gMnSc1	Bernard
Montgomery	Montgomera	k1gFnSc2	Montgomera
a	a	k8xC	a
francouzský	francouzský	k2eAgMnSc1d1	francouzský
generál	generál	k1gMnSc1	generál
a	a	k8xC	a
budoucí	budoucí	k2eAgMnSc1d1	budoucí
prezident	prezident	k1gMnSc1	prezident
Francie	Francie	k1gFnSc2	Francie
Charles	Charles	k1gMnSc1	Charles
de	de	k?	de
Gaulle	Gaulle	k1gInSc1	Gaulle
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
spory	spor	k1gInPc1	spor
o	o	k7c4	o
způsob	způsob	k1gInSc4	způsob
osvobozování	osvobozování	k1gNnSc2	osvobozování
Evropy	Evropa	k1gFnSc2	Evropa
byly	být	k5eAaImAgInP	být
nezanedbatelné	zanedbatelný	k2eNgInPc1d1	nezanedbatelný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikdy	nikdy	k6eAd1	nikdy
neohrožovaly	ohrožovat	k5eNaImAgInP	ohrožovat
jejich	jejich	k3xOp3gInPc1	jejich
vzájemné	vzájemný	k2eAgInPc1d1	vzájemný
vztahy	vztah	k1gInPc1	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
Američanů	Američan	k1gMnPc2	Američan
byl	být	k5eAaImAgInS	být
schopen	schopen	k2eAgInSc1d1	schopen
najít	najít	k5eAaPmF	najít
cestu	cesta	k1gFnSc4	cesta
i	i	k9	i
k	k	k7c3	k
Sovětům	Sověty	k1gInPc3	Sověty
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
maršálem	maršál	k1gMnSc7	maršál
Georgijem	Georgij	k1gMnSc7	Georgij
Žukovem	Žukovo	k1gNnSc7	Žukovo
a	a	k8xC	a
Josefem	Josef	k1gMnSc7	Josef
Visarionovičem	Visarionovič	k1gMnSc7	Visarionovič
Stalinem	Stalin	k1gMnSc7	Stalin
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yIgInPc7	který
často	často	k6eAd1	často
osobně	osobně	k6eAd1	osobně
jednal	jednat	k5eAaImAgMnS	jednat
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
si	se	k3xPyFc3	se
velice	velice	k6eAd1	velice
získal	získat	k5eAaPmAgMnS	získat
důvěru	důvěra	k1gFnSc4	důvěra
u	u	k7c2	u
svého	svůj	k3xOyFgMnSc2	svůj
nejvyššího	vysoký	k2eAgMnSc2d3	nejvyšší
nadřízeného	nadřízený	k1gMnSc2	nadřízený
<g/>
,	,	kIx,	,
amerického	americký	k2eAgMnSc2d1	americký
prezidenta	prezident	k1gMnSc2	prezident
Franklina	Franklina	k1gFnSc1	Franklina
D.	D.	kA	D.
Roosevelta	Roosevelt	k1gMnSc2	Roosevelt
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jednání	jednání	k1gNnSc2	jednání
se	s	k7c7	s
Sověty	Sovět	k1gMnPc7	Sovět
vyjednal	vyjednat	k5eAaPmAgMnS	vyjednat
například	například	k6eAd1	například
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
budou	být	k5eAaImBp3nP	být
právě	právě	k6eAd1	právě
Sověti	Sovět	k1gMnPc1	Sovět
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
osvobodí	osvobodit	k5eAaPmIp3nP	osvobodit
Berlín	Berlín	k1gInSc4	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tím	ten	k3xDgNnSc7	ten
zachránil	zachránit	k5eAaPmAgInS	zachránit
podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
až	až	k9	až
100	[number]	k4	100
000	[number]	k4	000
amerických	americký	k2eAgMnPc2d1	americký
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
toto	tento	k3xDgNnSc1	tento
jeho	jeho	k3xOp3gNnSc1	jeho
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
mnohými	mnohý	k2eAgMnPc7d1	mnohý
historiky	historik	k1gMnPc7	historik
kritizováno	kritizovat	k5eAaImNgNnS	kritizovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
tím	ten	k3xDgNnSc7	ten
posílil	posílit	k5eAaPmAgInS	posílit
sovětský	sovětský	k2eAgInSc4d1	sovětský
vliv	vliv	k1gInSc4	vliv
a	a	k8xC	a
mělo	mít	k5eAaImAgNnS	mít
to	ten	k3xDgNnSc1	ten
za	za	k7c4	za
následek	následek	k1gInSc4	následek
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
Rusů	Rus	k1gMnPc2	Rus
padlých	padlý	k1gMnPc2	padlý
v	v	k7c6	v
berlínských	berlínský	k2eAgFnPc6d1	Berlínská
ulicích	ulice	k1gFnPc6	ulice
během	během	k7c2	během
jeho	jeho	k3xOp3gNnPc2	jeho
osvobozování	osvobozování	k1gNnPc2	osvobozování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nikdy	nikdy	k6eAd1	nikdy
nebylo	být	k5eNaImAgNnS	být
zcela	zcela	k6eAd1	zcela
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
zdali	zdali	k8xS	zdali
bude	být	k5eAaImBp3nS	být
Operace	operace	k1gFnSc1	operace
Overlord	Overlorda	k1gFnPc2	Overlorda
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
.	.	kIx.	.
</s>
<s>
Takováto	takovýto	k3xDgFnSc1	takovýto
operace	operace	k1gFnSc1	operace
sebou	se	k3xPyFc7	se
nesla	nést	k5eAaImAgFnS	nést
mnoho	mnoho	k4c4	mnoho
rizik	riziko	k1gNnPc2	riziko
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
důležité	důležitý	k2eAgNnSc1d1	důležité
navrhnout	navrhnout	k5eAaPmF	navrhnout
nejen	nejen	k6eAd1	nejen
správnou	správný	k2eAgFnSc4d1	správná
oblast	oblast	k1gFnSc4	oblast
pro	pro	k7c4	pro
vylodění	vylodění	k1gNnSc4	vylodění
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
správně	správně	k6eAd1	správně
stanovit	stanovit	k5eAaPmF	stanovit
její	její	k3xOp3gNnSc4	její
načasování	načasování	k1gNnSc4	načasování
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
všechny	všechen	k3xTgInPc1	všechen
alternativní	alternativní	k2eAgInPc1d1	alternativní
plány	plán	k1gInPc1	plán
měly	mít	k5eAaImAgInP	mít
ovšem	ovšem	k9	ovšem
svá	svůj	k3xOyFgNnPc4	svůj
rizika	riziko	k1gNnPc4	riziko
<g/>
.	.	kIx.	.
</s>
<s>
Naštěstí	naštěstí	k6eAd1	naštěstí
se	se	k3xPyFc4	se
spojenci	spojenec	k1gMnPc1	spojenec
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
pro	pro	k7c4	pro
vylodění	vylodění	k1gNnSc4	vylodění
v	v	k7c6	v
Normandii	Normandie	k1gFnSc6	Normandie
<g/>
.	.	kIx.	.
</s>
<s>
Pochybnosti	pochybnost	k1gFnPc4	pochybnost
o	o	k7c6	o
tomto	tento	k3xDgNnSc6	tento
vylodění	vylodění	k1gNnSc6	vylodění
měl	mít	k5eAaImAgMnS	mít
i	i	k9	i
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
náhodně	náhodně	k6eAd1	náhodně
nalezený	nalezený	k2eAgInSc1d1	nalezený
a	a	k8xC	a
předem	předem	k6eAd1	předem
připravený	připravený	k2eAgInSc4d1	připravený
projev	projev	k1gInSc4	projev
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
nachystán	nachystat	k5eAaPmNgInS	nachystat
pro	pro	k7c4	pro
případ	případ	k1gInSc4	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
vylodění	vylodění	k1gNnSc1	vylodění
nepovedlo	povést	k5eNaPmAgNnS	povést
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
bral	brát	k5eAaImAgMnS	brát
všechnu	všechen	k3xTgFnSc4	všechen
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Originální	originální	k2eAgInSc1d1	originální
proslov	proslov	k1gInSc1	proslov
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
nalezen	nalézt	k5eAaBmNgInS	nalézt
v	v	k7c6	v
kapse	kapsa	k1gFnSc6	kapsa
košile	košile	k1gFnSc2	košile
vojenským	vojenský	k2eAgMnSc7d1	vojenský
pobočníkem	pobočník	k1gMnSc7	pobočník
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Our	Our	k1gMnSc2	Our
landings	landings	k1gInSc1	landings
in	in	k?	in
the	the	k?	the
Cherbourg-Havre	Cherbourg-Havr	k1gInSc5	Cherbourg-Havr
area	area	k1gFnSc1	area
have	have	k5eAaImRp2nP	have
failed	failed	k1gInSc4	failed
to	ten	k3xDgNnSc1	ten
gain	gain	k1gNnSc1	gain
a	a	k8xC	a
satisfactory	satisfactor	k1gInPc1	satisfactor
foothold	footholda	k1gFnPc2	footholda
and	and	k?	and
I	i	k9	i
have	havat	k5eAaPmIp3nS	havat
withdrawn	withdrawn	k1gInSc1	withdrawn
the	the	k?	the
troops	troops	k1gInSc1	troops
<g/>
.	.	kIx.	.
</s>
<s>
My	my	k3xPp1nPc1	my
decision	decision	k1gInSc1	decision
to	ten	k3xDgNnSc4	ten
attack	attack	k6eAd1	attack
at	at	k?	at
this	this	k1gInSc1	this
time	time	k1gFnSc1	time
and	and	k?	and
place	plac	k1gInSc6	plac
was	was	k?	was
based	based	k1gInSc1	based
on	on	k3xPp3gMnSc1	on
the	the	k?	the
best	best	k2eAgInSc4d1	best
information	information	k1gInSc4	information
available	available	k6eAd1	available
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
troops	troops	k1gInSc1	troops
<g/>
,	,	kIx,	,
the	the	k?	the
air	air	k?	air
and	and	k?	and
the	the	k?	the
Navy	Navy	k?	Navy
did	did	k?	did
all	all	k?	all
that	that	k1gMnSc1	that
bravery	bravera	k1gFnSc2	bravera
and	and	k?	and
devotion	devotion	k1gInSc1	devotion
to	ten	k3xDgNnSc1	ten
duty	duty	k?	duty
could	could	k1gInSc1	could
do	do	k7c2	do
<g/>
.	.	kIx.	.
</s>
<s>
If	If	k?	If
any	any	k?	any
blame	blamat	k5eAaPmIp3nS	blamat
or	or	k?	or
fault	fault	k1gMnSc1	fault
attaches	attaches	k1gMnSc1	attaches
to	ten	k3xDgNnSc1	ten
the	the	k?	the
attempt	attempt	k1gInSc1	attempt
<g/>
,	,	kIx,	,
it	it	k?	it
is	is	k?	is
mine	minout	k5eAaImIp3nS	minout
alone	alonout	k5eAaPmIp3nS	alonout
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Přeloženo	přeložen	k2eAgNnSc1d1	přeloženo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Naše	náš	k3xOp1gNnSc1	náš
vylodění	vylodění	k1gNnSc1	vylodění
v	v	k7c6	v
Cherbourg-Havre	Cherbourg-Havr	k1gInSc5	Cherbourg-Havr
selhalo	selhat	k5eAaPmAgNnS	selhat
<g/>
.	.	kIx.	.
</s>
<s>
Náš	náš	k3xOp1gInSc1	náš
pokus	pokus	k1gInSc1	pokus
získat	získat	k5eAaPmF	získat
uspokojivou	uspokojivý	k2eAgFnSc4d1	uspokojivá
výhodu	výhoda	k1gFnSc4	výhoda
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
selhal	selhat	k5eAaPmAgInS	selhat
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jsem	být	k5eAaImIp1nS	být
to	ten	k3xDgNnSc1	ten
já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
stáhnout	stáhnout	k5eAaPmF	stáhnout
naše	náš	k3xOp1gNnPc4	náš
vojska	vojsko	k1gNnPc4	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Mé	můj	k3xOp1gNnSc4	můj
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
zaútočit	zaútočit	k5eAaPmF	zaútočit
v	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
chvíli	chvíle	k1gFnSc4	chvíle
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
nejlepších	dobrý	k2eAgFnPc6d3	nejlepší
dostupných	dostupný	k2eAgFnPc6d1	dostupná
informacích	informace	k1gFnPc6	informace
<g/>
.	.	kIx.	.
</s>
<s>
Pěchota	pěchota	k1gFnSc1	pěchota
<g/>
,	,	kIx,	,
letectvo	letectvo	k1gNnSc1	letectvo
a	a	k8xC	a
námořnictvo	námořnictvo	k1gNnSc1	námořnictvo
provedlo	provést	k5eAaPmAgNnS	provést
vše	všechen	k3xTgNnSc4	všechen
nejodvážněji	odvážně	k6eAd3	odvážně
a	a	k8xC	a
nejobětavěji	obětavě	k6eAd3	obětavě
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jen	jen	k9	jen
mohli	moct	k5eAaImAgMnP	moct
<g/>
.	.	kIx.	.
</s>
<s>
Jestli	jestli	k8xS	jestli
někdo	někdo	k3yInSc1	někdo
chce	chtít	k5eAaImIp3nS	chtít
někomu	někdo	k3yInSc3	někdo
nadávat	nadávat	k5eAaImF	nadávat
a	a	k8xC	a
obviňovat	obviňovat	k5eAaImF	obviňovat
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
obviňujte	obviňovat	k5eAaImRp2nP	obviňovat
jenom	jenom	k9	jenom
mě	já	k3xPp1nSc2	já
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jenom	jenom	k9	jenom
má	mít	k5eAaImIp3nS	mít
chyba	chyba	k1gFnSc1	chyba
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Počátky	počátek	k1gInPc4	počátek
politické	politický	k2eAgFnSc2d1	politická
kariéry	kariéra	k1gFnSc2	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
německé	německý	k2eAgFnSc6d1	německá
bezpodmínečné	bezpodmínečný	k2eAgFnSc6d1	bezpodmínečná
kapitulaci	kapitulace	k1gFnSc6	kapitulace
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
byl	být	k5eAaImAgMnS	být
jmenován	jmenován	k2eAgMnSc1d1	jmenován
"	"	kIx"	"
<g/>
Military	Militar	k1gInPc1	Militar
Governor	Governora	k1gFnPc2	Governora
of	of	k?	of
the	the	k?	the
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Occupation	Occupation	k1gInSc1	Occupation
Zone	Zone	k1gInSc1	Zone
<g/>
"	"	kIx"	"
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
spravovat	spravovat	k5eAaImF	spravovat
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
okupačních	okupační	k2eAgFnPc2d1	okupační
zón	zóna	k1gFnPc2	zóna
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgFnPc4	který
bylo	být	k5eAaImAgNnS	být
Německo	Německo	k1gNnSc1	Německo
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
rozděleno	rozdělen	k2eAgNnSc1d1	rozděleno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
plném	plný	k2eAgNnSc6d1	plné
prozkoumání	prozkoumání	k1gNnSc6	prozkoumání
koncentračních	koncentrační	k2eAgInPc2d1	koncentrační
táborů	tábor	k1gInPc2	tábor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
měly	mít	k5eAaImAgInP	mít
konečně	konečně	k6eAd1	konečně
vyřešit	vyřešit	k5eAaPmF	vyřešit
židovskou	židovský	k2eAgFnSc4d1	židovská
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
přikázal	přikázat	k5eAaPmAgMnS	přikázat
svým	svůj	k3xOyFgMnPc3	svůj
válečným	válečný	k2eAgMnPc3d1	válečný
zpravodajům	zpravodaj	k1gMnPc3	zpravodaj
natočit	natočit	k5eAaBmF	natočit
co	co	k9	co
nejvěrnější	věrný	k2eAgInSc4d3	nejvěrnější
dokument	dokument	k1gInSc4	dokument
o	o	k7c6	o
koncentračních	koncentrační	k2eAgInPc6d1	koncentrační
táborech	tábor	k1gInPc6	tábor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
pomoci	pomoct	k5eAaPmF	pomoct
varovat	varovat	k5eAaImF	varovat
a	a	k8xC	a
předejít	předejít	k5eAaPmF	předejít
opakování	opakování	k1gNnSc6	opakování
něčeho	něco	k3yInSc2	něco
podobného	podobný	k2eAgNnSc2d1	podobné
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
také	také	k9	také
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
velitelé	velitel	k1gMnPc1	velitel
těchto	tento	k3xDgInPc2	tento
táborů	tábor	k1gInPc2	tábor
nemohli	moct	k5eNaImAgMnP	moct
před	před	k7c7	před
soudem	soud	k1gInSc7	soud
později	pozdě	k6eAd2	pozdě
tvrdit	tvrdit	k5eAaImF	tvrdit
něco	něco	k3yInSc4	něco
jiného	jiný	k2eAgMnSc4d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Učinil	učinit	k5eAaPmAgInS	učinit
také	také	k9	také
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
překlasifikovalo	překlasifikovat	k5eAaBmAgNnS	překlasifikovat
německé	německý	k2eAgMnPc4d1	německý
válečné	válečný	k2eAgMnPc4d1	válečný
zajatce	zajatec	k1gMnPc4	zajatec
na	na	k7c4	na
neozbrojené	ozbrojený	k2eNgFnPc4d1	neozbrojená
vojenské	vojenský	k2eAgFnPc4d1	vojenská
síly	síla	k1gFnPc4	síla
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
tyto	tento	k3xDgFnPc1	tento
jednotky	jednotka	k1gFnPc1	jednotka
mohly	moct	k5eAaImAgFnP	moct
pomoci	pomoct	k5eAaPmF	pomoct
s	s	k7c7	s
budováním	budování	k1gNnSc7	budování
poválečného	poválečný	k2eAgNnSc2d1	poválečné
Německa	Německo	k1gNnSc2	Německo
s	s	k7c7	s
minimálním	minimální	k2eAgNnSc7d1	minimální
právem	právo	k1gNnSc7	právo
na	na	k7c4	na
odměny	odměna	k1gFnPc4	odměna
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
také	také	k9	také
podporoval	podporovat	k5eAaImAgInS	podporovat
Morgenthauův	Morgenthauův	k2eAgInSc1d1	Morgenthauův
plán	plán	k1gInSc1	plán
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
odstranit	odstranit	k5eAaPmF	odstranit
průmyslové	průmyslový	k2eAgFnSc3d1	průmyslová
oblasti	oblast	k1gFnSc3	oblast
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zamezit	zamezit	k5eAaPmF	zamezit
možné	možný	k2eAgFnSc3d1	možná
budoucí	budoucí	k2eAgFnSc3d1	budoucí
válce	válka	k1gFnSc3	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1945	[number]	k4	1945
pomohl	pomoct	k5eAaPmAgInS	pomoct
s	s	k7c7	s
distribucí	distribuce	k1gFnSc7	distribuce
Morgenthauovy	Morgenthauův	k2eAgFnSc2d1	Morgenthauův
knihy	kniha	k1gFnSc2	kniha
"	"	kIx"	"
<g/>
Germany	German	k1gMnPc4	German
is	is	k?	is
Our	Our	k1gFnSc1	Our
Problem	Probl	k1gInSc7	Probl
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
představovala	představovat	k5eAaImAgFnS	představovat
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
jakou	jaký	k3yRgFnSc4	jaký
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
poválečné	poválečný	k2eAgNnSc1d1	poválečné
Německo	Německo	k1gNnSc1	Německo
mělo	mít	k5eAaImAgNnS	mít
vydat	vydat	k5eAaPmF	vydat
a	a	k8xC	a
jak	jak	k8xS	jak
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
spravováno	spravovat	k5eAaImNgNnS	spravovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
sloužil	sloužit	k5eAaImAgMnS	sloužit
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1948	[number]	k4	1948
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Chief	Chief	k1gInSc1	Chief
of	of	k?	of
Staff	Staff	k1gInSc1	Staff
of	of	k?	of
the	the	k?	the
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Army	Arma	k1gMnSc2	Arma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1950	[number]	k4	1950
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
<s>
Velel	velet	k5eAaImAgMnS	velet
operačnímu	operační	k2eAgNnSc3d1	operační
velitelství	velitelství	k1gNnSc3	velitelství
sil	síla	k1gFnPc2	síla
NATO	NATO	kA	NATO
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
aktivní	aktivní	k2eAgFnSc2d1	aktivní
služby	služba	k1gFnSc2	služba
odstoupil	odstoupit	k5eAaPmAgInS	odstoupit
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1952	[number]	k4	1952
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
vstupem	vstup	k1gInSc7	vstup
do	do	k7c2	do
politiky	politika	k1gFnSc2	politika
napsal	napsat	k5eAaPmAgMnS	napsat
knihu	kniha	k1gFnSc4	kniha
"	"	kIx"	"
<g/>
Crusade	Crusad	k1gInSc5	Crusad
in	in	k?	in
Europe	Europ	k1gMnSc5	Europ
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
valečných	valečný	k2eAgNnPc2d1	valečný
memoárových	memoárový	k2eAgNnPc2d1	memoárové
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
amerických	americký	k2eAgFnPc6d1	americká
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
děkan	děkan	k1gMnSc1	děkan
Kolumbijské	kolumbijský	k2eAgFnSc2d1	kolumbijská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Díky	díky	k7c3	díky
jeho	jeho	k3xOp3gInPc3	jeho
četným	četný	k2eAgInPc3d1	četný
válečným	válečný	k2eAgInPc3d1	válečný
úspěchům	úspěch	k1gInPc3	úspěch
byl	být	k5eAaImAgInS	být
při	při	k7c6	při
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
oslavován	oslavován	k2eAgMnSc1d1	oslavován
jako	jako	k8xC	jako
národní	národní	k2eAgMnSc1d1	národní
hrdina	hrdina	k1gMnSc1	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
netradičního	tradiční	k2eNgMnSc4d1	netradiční
válečného	válečný	k2eAgMnSc4d1	válečný
hrdinu	hrdina	k1gMnSc4	hrdina
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
nikdy	nikdy	k6eAd1	nikdy
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
životě	život	k1gInSc6	život
neviděl	vidět	k5eNaImAgMnS	vidět
žádnou	žádný	k3yNgFnSc4	žádný
válečnou	válečný	k2eAgFnSc4d1	válečná
frontu	fronta	k1gFnSc4	fronta
na	na	k7c4	na
vlastní	vlastní	k2eAgNnPc4d1	vlastní
oči	oko	k1gNnPc4	oko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
osobní	osobní	k2eAgFnSc1d1	osobní
zkušenost	zkušenost	k1gFnSc1	zkušenost
s	s	k7c7	s
nepřátelskou	přátelský	k2eNgFnSc7d1	nepřátelská
palbou	palba	k1gFnSc7	palba
nastala	nastat	k5eAaPmAgFnS	nastat
pouze	pouze	k6eAd1	pouze
jednou	jeden	k4xCgFnSc7	jeden
jedinkrát	jedinkrát	k6eAd1	jedinkrát
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
při	při	k7c6	při
inspekci	inspekce	k1gFnSc6	inspekce
pozemních	pozemní	k2eAgNnPc2d1	pozemní
vojsk	vojsko	k1gNnPc2	vojsko
v	v	k7c6	v
Normandii	Normandie	k1gFnSc6	Normandie
nad	nad	k7c7	nad
jeho	jeho	k3xOp3gFnSc7	jeho
hlavou	hlava	k1gFnSc7	hlava
proletělo	proletět	k5eAaPmAgNnS	proletět
německé	německý	k2eAgNnSc1d1	německé
letadlo	letadlo	k1gNnSc1	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgInSc2	tento
přeletu	přelet	k1gInSc2	přelet
se	se	k3xPyFc4	se
všichni	všechen	k3xTgMnPc1	všechen
vojáci	voják	k1gMnPc1	voják
včetně	včetně	k7c2	včetně
nejvyššího	vysoký	k2eAgMnSc2d3	nejvyšší
velitele	velitel	k1gMnSc2	velitel
kryli	krýt	k5eAaImAgMnP	krýt
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
letadlo	letadlo	k1gNnSc1	letadlo
zneškodněno	zneškodnit	k5eAaPmNgNnS	zneškodnit
<g/>
,	,	kIx,	,
vojáci	voják	k1gMnPc1	voják
mu	on	k3xPp3gMnSc3	on
pomohli	pomoct	k5eAaPmAgMnP	pomoct
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
nohy	noha	k1gFnPc4	noha
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jim	on	k3xPp3gFnPc3	on
poděkoval	poděkovat	k5eAaPmAgMnS	poděkovat
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
mu	on	k3xPp3gMnSc3	on
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gInSc1	jejich
jediný	jediný	k2eAgInSc1d1	jediný
úkol	úkol	k1gInSc1	úkol
–	–	k?	–
aby	aby	kYmCp3nS	aby
zůstal	zůstat	k5eAaPmAgMnS	zůstat
v	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
sektoru	sektor	k1gInSc6	sektor
nezraněn	zranit	k5eNaPmNgMnS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
historku	historka	k1gFnSc4	historka
pak	pak	k6eAd1	pak
vyprávěl	vyprávět	k5eAaImAgMnS	vyprávět
svému	svůj	k3xOyFgMnSc3	svůj
okolí	okolí	k1gNnSc4	okolí
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
návratu	návrat	k1gInSc6	návrat
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgInS	připojit
k	k	k7c3	k
Republikanské	Republikanský	k2eAgFnSc3d1	Republikanský
straně	strana	k1gFnSc3	strana
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jej	on	k3xPp3gNnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
nominovala	nominovat	k5eAaBmAgFnS	nominovat
jako	jako	k9	jako
kandidáta	kandidát	k1gMnSc4	kandidát
do	do	k7c2	do
prezidentských	prezidentský	k2eAgFnPc2d1	prezidentská
voleb	volba	k1gFnPc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
soupeř	soupeř	k1gMnSc1	soupeř
na	na	k7c6	na
kandidátce	kandidátka	k1gFnSc6	kandidátka
byl	být	k5eAaImAgMnS	být
senátor	senátor	k1gMnSc1	senátor
Robert	Robert	k1gMnSc1	Robert
Taft	taft	k1gInSc4	taft
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
porazil	porazit	k5eAaPmAgMnS	porazit
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
však	však	k9	však
nakonec	nakonec	k6eAd1	nakonec
došli	dojít	k5eAaPmAgMnP	dojít
ke	k	k7c3	k
společnému	společný	k2eAgInSc3d1	společný
kompromisu	kompromis	k1gInSc3	kompromis
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jejich	jejich	k3xOp3gInSc1	jejich
úmysl	úmysl	k1gInSc1	úmysl
stát	stát	k5eAaPmF	stát
se	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
USA	USA	kA	USA
byl	být	k5eAaImAgInS	být
prosazován	prosazovat	k5eAaImNgInS	prosazovat
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
"	"	kIx"	"
<g/>
křižácké	křižácký	k2eAgFnSc2d1	křižácká
výpravy	výprava	k1gFnSc2	výprava
<g/>
"	"	kIx"	"
proti	proti	k7c3	proti
Trumanově	Trumanův	k2eAgFnSc3d1	Trumanova
politice	politika	k1gFnSc3	politika
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgInSc1	sám
před	před	k7c7	před
volbami	volba	k1gFnPc7	volba
přislíbil	přislíbit	k5eAaPmAgMnS	přislíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
osobně	osobně	k6eAd1	osobně
pojede	pojet	k5eAaPmIp3nS	pojet
do	do	k7c2	do
Koreje	Korea	k1gFnSc2	Korea
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pomůže	pomoct	k5eAaPmIp3nS	pomoct
ukončit	ukončit	k5eAaPmF	ukončit
Korejskou	korejský	k2eAgFnSc4d1	Korejská
válku	válka	k1gFnSc4	válka
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
dále	daleko	k6eAd2	daleko
bojovat	bojovat	k5eAaImF	bojovat
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
NATO	nato	k6eAd1	nato
proti	proti	k7c3	proti
komunismu	komunismus	k1gInSc3	komunismus
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
měl	mít	k5eAaImAgInS	mít
podtrhnout	podtrhnout	k5eAaPmF	podtrhnout
jeho	jeho	k3xOp3gInSc1	jeho
boj	boj	k1gInSc1	boj
proti	proti	k7c3	proti
korupci	korupce	k1gFnSc3	korupce
v	v	k7c6	v
administrativní	administrativní	k2eAgFnSc6d1	administrativní
sféře	sféra	k1gFnSc6	sféra
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Prezidentství	prezidentství	k1gNnSc2	prezidentství
1953	[number]	k4	1953
<g/>
–	–	k?	–
<g/>
1961	[number]	k4	1961
==	==	k?	==
</s>
</p>
<p>
<s>
Měl	mít	k5eAaImAgMnS	mít
za	za	k7c7	za
sebou	se	k3xPyFc7	se
silného	silný	k2eAgMnSc4d1	silný
společníka	společník	k1gMnSc4	společník
Richarda	Richard	k1gMnSc4	Richard
Nixona	Nixon	k1gMnSc4	Nixon
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
dcera	dcera	k1gFnSc1	dcera
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c2	za
jeho	on	k3xPp3gMnSc2	on
synovce	synovec	k1gMnSc2	synovec
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
Richardem	Richard	k1gMnSc7	Richard
Nixonem	Nixon	k1gMnSc7	Nixon
snadno	snadno	k6eAd1	snadno
porazili	porazit	k5eAaPmAgMnP	porazit
demokraty	demokrat	k1gMnPc7	demokrat
Adlai	Adla	k1gFnSc2	Adla
Stevensona	Stevenson	k1gMnSc2	Stevenson
a	a	k8xC	a
Johna	John	k1gMnSc2	John
Sparkmana	Sparkman	k1gMnSc2	Sparkman
a	a	k8xC	a
po	po	k7c6	po
dvaceti	dvacet	k4xCc6	dvacet
letech	let	k1gInPc6	let
byli	být	k5eAaImAgMnP	být
dalšími	další	k2eAgMnPc7d1	další
republikány	republikán	k1gMnPc7	republikán
v	v	k7c6	v
Bílém	bílý	k2eAgInSc6d1	bílý
domě	dům	k1gInSc6	dům
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jediným	jediný	k2eAgMnSc7d1	jediný
americkým	americký	k2eAgMnSc7d1	americký
generálem	generál	k1gMnSc7	generál
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
během	během	k7c2	během
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
stal	stát	k5eAaPmAgMnS	stát
prezidentem	prezident	k1gMnSc7	prezident
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
největších	veliký	k2eAgInPc2d3	veliký
úspěchů	úspěch	k1gInPc2	úspěch
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
prezidenta	prezident	k1gMnSc2	prezident
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
Američany	Američan	k1gMnPc4	Američan
považován	považován	k2eAgInSc1d1	považován
jeho	jeho	k3xOp3gInSc1	jeho
Mezistátní	mezistátní	k2eAgInSc1d1	mezistátní
dálniční	dálniční	k2eAgInSc1d1	dálniční
systém	systém	k1gInSc1	systém
USA	USA	kA	USA
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
tehdy	tehdy	k6eAd1	tehdy
pokládán	pokládat	k5eAaImNgInS	pokládat
za	za	k7c4	za
nezbytný	nezbytný	k2eAgInSc4d1	nezbytný
kvůli	kvůli	k7c3	kvůli
Studené	Studená	k1gFnSc3	Studená
válce	válka	k1gFnSc3	válka
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
tehdy	tehdy	k6eAd1	tehdy
věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
případné	případný	k2eAgFnSc6d1	případná
válce	válka	k1gFnSc6	válka
to	ten	k3xDgNnSc1	ten
budou	být	k5eAaImBp3nP	být
právě	právě	k6eAd1	právě
velká	velký	k2eAgNnPc1d1	velké
města	město	k1gNnPc1	město
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
se	se	k3xPyFc4	se
stanou	stanout	k5eAaPmIp3nP	stanout
hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
nepřítele	nepřítel	k1gMnSc2	nepřítel
a	a	k8xC	a
systém	systém	k1gInSc1	systém
dálnic	dálnice	k1gFnPc2	dálnice
měl	mít	k5eAaImAgInS	mít
zajistit	zajistit	k5eAaPmF	zajistit
rychlou	rychlý	k2eAgFnSc4d1	rychlá
evakuaci	evakuace	k1gFnSc4	evakuace
velkých	velký	k2eAgNnPc2d1	velké
měst	město	k1gNnPc2	město
a	a	k8xC	a
rychlý	rychlý	k2eAgInSc1d1	rychlý
přesun	přesun	k1gInSc1	přesun
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
jeho	jeho	k3xOp3gFnPc6	jeho
vlastních	vlastní	k2eAgFnPc6d1	vlastní
zkušenostech	zkušenost	k1gFnPc6	zkušenost
z	z	k7c2	z
</s>
</p>
<p>
<s>
války	válka	k1gFnPc1	válka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pochopil	pochopit	k5eAaPmAgMnS	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
flexibilita	flexibilita	k1gFnSc1	flexibilita
vojska	vojsko	k1gNnSc2	vojsko
a	a	k8xC	a
civilního	civilní	k2eAgNnSc2d1	civilní
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
důležitá	důležitý	k2eAgFnSc1d1	důležitá
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jeho	on	k3xPp3gInSc2	on
výkonu	výkon	k1gInSc2	výkon
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
funkce	funkce	k1gFnSc2	funkce
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
doktrínu	doktrína	k1gFnSc4	doktrína
Dynamického	dynamický	k2eAgInSc2d1	dynamický
konzervatismu	konzervatismus	k1gInSc2	konzervatismus
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
zastával	zastávat	k5eAaImAgMnS	zastávat
konzervativní	konzervativní	k2eAgFnSc4d1	konzervativní
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
podstatných	podstatný	k2eAgInPc6d1	podstatný
bodech	bod	k1gInPc6	bod
v	v	k7c6	v
New	New	k1gFnSc6	New
Dealu	Deal	k1gInSc2	Deal
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
sociálního	sociální	k2eAgNnSc2d1	sociální
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
<g/>
.	.	kIx.	.
</s>
<s>
Rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
tento	tento	k3xDgInSc4	tento
program	program	k1gInSc4	program
i	i	k9	i
na	na	k7c4	na
ministerstva	ministerstvo	k1gNnPc4	ministerstvo
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
<g/>
,	,	kIx,	,
školství	školství	k1gNnSc2	školství
a	a	k8xC	a
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
osobní	osobní	k2eAgFnPc1d1	osobní
kvality	kvalita	k1gFnPc1	kvalita
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
ve	v	k7c4	v
funkci	funkce	k1gFnSc4	funkce
prezidenta	prezident	k1gMnSc2	prezident
USA	USA	kA	USA
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
oceněny	ocenit	k5eAaPmNgInP	ocenit
v	v	k7c6	v
druhých	druhý	k4xOgFnPc6	druhý
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
získal	získat	k5eAaPmAgInS	získat
57.6	[number]	k4	57.6
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
Suezské	suezský	k2eAgFnSc2d1	Suezská
krize	krize	k1gFnSc2	krize
se	se	k3xPyFc4	se
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
staly	stát	k5eAaPmAgInP	stát
ochráncem	ochránce	k1gMnSc7	ochránce
většiny	většina	k1gFnSc2	většina
západních	západní	k2eAgInPc2d1	západní
států	stát	k1gInPc2	stát
a	a	k8xC	a
některých	některý	k3yIgFnPc2	některý
na	na	k7c6	na
Středním	střední	k2eAgInSc6d1	střední
Východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
Eisenhowerovu	Eisenhowerův	k2eAgFnSc4d1	Eisenhowerova
doktrínu	doktrína	k1gFnSc4	doktrína
z	z	k7c2	z
ledna	leden	k1gInSc2	leden
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zaručovala	zaručovat	k5eAaImAgFnS	zaručovat
použití	použití	k1gNnSc3	použití
vojenské	vojenský	k2eAgFnSc2d1	vojenská
síly	síla	k1gFnSc2	síla
jako	jako	k8xC	jako
reakci	reakce	k1gFnSc4	reakce
na	na	k7c4	na
případnou	případný	k2eAgFnSc4d1	případná
agresi	agrese	k1gFnSc4	agrese
jakékoliv	jakýkoliv	k3yIgFnSc2	jakýkoliv
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
baštou	bašta	k1gFnSc7	bašta
komunismu	komunismus	k1gInSc2	komunismus
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1958	[number]	k4	1958
poslal	poslat	k5eAaPmAgMnS	poslat
15	[number]	k4	15
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
do	do	k7c2	do
Libanonu	Libanon	k1gInSc2	Libanon
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
operace	operace	k1gFnSc1	operace
"	"	kIx"	"
<g/>
Blue	Blue	k1gFnSc1	Blue
Bat	Bat	k1gFnSc1	Bat
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Modrý	modrý	k2eAgMnSc1d1	modrý
netopýr	netopýr	k1gMnSc1	netopýr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
operace	operace	k1gFnSc1	operace
byla	být	k5eAaImAgFnS	být
nevojenského	vojenský	k2eNgInSc2d1	nevojenský
charakteru	charakter	k1gInSc2	charakter
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
pomoci	pomoct	k5eAaPmF	pomoct
stabilizovat	stabilizovat	k5eAaBmF	stabilizovat
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
prozápadní	prozápadní	k2eAgFnSc4d1	prozápadní
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Operace	operace	k1gFnSc1	operace
"	"	kIx"	"
<g/>
Blue	Blue	k1gFnSc1	Blue
Bat	Bat	k1gFnSc1	Bat
<g/>
"	"	kIx"	"
skončila	skončit	k5eAaPmAgFnS	skončit
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgInS	být
zastáncem	zastánce	k1gMnSc7	zastánce
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Podpořil	podpořit	k5eAaPmAgInS	podpořit
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
v	v	k7c6	v
rozhodnutí	rozhodnutí	k1gNnSc6	rozhodnutí
o	o	k7c6	o
desegregaci	desegregace	k1gFnSc6	desegregace
amerických	americký	k2eAgFnPc2d1	americká
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
byl	být	k5eAaImAgInS	být
poté	poté	k6eAd1	poté
přijat	přijmout	k5eAaPmNgInS	přijmout
jako	jako	k8xS	jako
dodatek	dodatek	k1gInSc1	dodatek
ústavy	ústava	k1gFnSc2	ústava
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
po	po	k7c6	po
vynesení	vynesení	k1gNnSc6	vynesení
rozsudku	rozsudek	k1gInSc2	rozsudek
se	se	k3xPyFc4	se
zasadil	zasadit	k5eAaPmAgInS	zasadit
o	o	k7c6	o
vytvoření	vytvoření	k1gNnSc6	vytvoření
modelové	modelový	k2eAgFnSc2d1	modelová
situace	situace	k1gFnSc2	situace
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
<g/>
.	.	kIx.	.
</s>
<s>
Liberálové	liberál	k1gMnPc1	liberál
sice	sice	k8xC	sice
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nikdy	nikdy	k6eAd1	nikdy
nebyl	být	k5eNaImAgMnS	být
v	v	k7c6	v
tomhle	tenhle	k3xDgInSc6	tenhle
ohledu	ohled	k1gInSc6	ohled
skutečně	skutečně	k6eAd1	skutečně
zapálený	zapálený	k2eAgInSc1d1	zapálený
<g/>
,	,	kIx,	,
nic	nic	k3yNnSc1	nic
to	ten	k3xDgNnSc4	ten
ale	ale	k9	ale
nemění	měnit	k5eNaImIp3nS	měnit
na	na	k7c6	na
faktu	fakt	k1gInSc6	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInPc1	jeho
činy	čin	k1gInPc1	čin
přispěly	přispět	k5eAaPmAgInP	přispět
k	k	k7c3	k
desegregaci	desegregace	k1gFnSc3	desegregace
americké	americký	k2eAgFnSc2d1	americká
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
posledním	poslední	k2eAgInSc7d1	poslední
činem	čin	k1gInSc7	čin
byl	být	k5eAaImAgInS	být
program	program	k1gInSc4	program
"	"	kIx"	"
<g/>
People	People	k1gFnSc4	People
to	ten	k3xDgNnSc1	ten
People	People	k1gFnPc1	People
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zajišťoval	zajišťovat	k5eAaImAgInS	zajišťovat
stipendia	stipendium	k1gNnPc4	stipendium
ve	v	k7c6	v
školství	školství	k1gNnSc6	školství
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
americká	americký	k2eAgFnSc1d1	americká
federace	federace	k1gFnSc1	federace
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
na	na	k7c4	na
50	[number]	k4	50
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1959	[number]	k4	1959
byla	být	k5eAaImAgFnS	být
připojena	připojen	k2eAgFnSc1d1	připojena
Aljaška	Aljaška	k1gFnSc1	Aljaška
a	a	k8xC	a
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1959	[number]	k4	1959
souostroví	souostroví	k1gNnSc2	souostroví
Havaj	Havaj	k1gFnSc4	Havaj
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jeho	jeho	k3xOp3gMnSc1	jeho
předchůdce	předchůdce	k1gMnSc1	předchůdce
<g/>
,	,	kIx,	,
zbavil	zbavit	k5eAaPmAgMnS	zbavit
za	za	k7c4	za
svoje	svůj	k3xOyFgNnSc4	svůj
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
značné	značný	k2eAgFnSc2d1	značná
části	část	k1gFnSc2	část
národního	národní	k2eAgInSc2d1	národní
dluhu	dluh	k1gInSc2	dluh
z	z	k7c2	z
konce	konec	k1gInSc2	konec
Druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Důchod	důchod	k1gInSc4	důchod
a	a	k8xC	a
smrt	smrt	k1gFnSc4	smrt
==	==	k?	==
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
poslední	poslední	k2eAgInSc1d1	poslední
proslov	proslov	k1gInSc1	proslov
z	z	k7c2	z
Oválné	oválný	k2eAgFnSc2d1	oválná
pracovny	pracovna	k1gFnSc2	pracovna
zazněl	zaznět	k5eAaImAgInS	zaznět
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
<s>
Varoval	varovat	k5eAaImAgMnS	varovat
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
před	před	k7c7	před
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
a	a	k8xC	a
zdůraznil	zdůraznit	k5eAaPmAgMnS	zdůraznit
roli	role	k1gFnSc4	role
Americké	americký	k2eAgFnSc2d1	americká
armády	armáda	k1gFnSc2	armáda
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
projevu	projev	k1gInSc6	projev
poprvé	poprvé	k6eAd1	poprvé
použil	použít	k5eAaPmAgInS	použít
pojem	pojem	k1gInSc1	pojem
vojensko-průmyslový	vojenskorůmyslový	k2eAgInSc4d1	vojensko-průmyslový
komplex	komplex	k1gInSc4	komplex
<g/>
,	,	kIx,	,
před	před	k7c7	před
jehož	jehož	k3xOyRp3gInSc7	jehož
rozmachem	rozmach	k1gInSc7	rozmach
veřejnost	veřejnost	k1gFnSc4	veřejnost
taktéž	taktéž	k?	taktéž
varoval	varovat	k5eAaImAgMnS	varovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
jeho	on	k3xPp3gInSc2	on
projevu	projev	k1gInSc2	projev
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
stala	stát	k5eAaPmAgFnS	stát
námětem	námět	k1gInSc7	námět
celovečerního	celovečerní	k2eAgInSc2d1	celovečerní
dokumentu	dokument	k1gInSc2	dokument
Why	Why	k1gMnSc1	Why
We	We	k1gMnSc1	We
Fight	Fight	k1gMnSc1	Fight
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
Bílého	bílý	k2eAgInSc2d1	bílý
domu	dům	k1gInSc2	dům
jeho	jeho	k3xOp3gFnSc4	jeho
popularita	popularita	k1gFnSc1	popularita
poklesla	poklesnout	k5eAaPmAgFnS	poklesnout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byl	být	k5eAaImAgMnS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
prezidenta	prezident	k1gMnSc4	prezident
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
prakticky	prakticky	k6eAd1	prakticky
nic	nic	k3yNnSc1	nic
neudělal	udělat	k5eNaPmAgMnS	udělat
<g/>
.	.	kIx.	.
</s>
<s>
Možná	možná	k9	možná
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
proto	proto	k6eAd1	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnSc7	jeho
nástupcem	nástupce	k1gMnSc7	nástupce
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
aktivně	aktivně	k6eAd1	aktivně
pracující	pracující	k2eAgMnSc1d1	pracující
John	John	k1gMnSc1	John
Fitzgerald	Fitzgerald	k1gMnSc1	Fitzgerald
Kennedy	Kenneda	k1gMnSc2	Kenneda
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
jeho	jeho	k3xOp3gFnPc1	jeho
zásluhy	zásluha	k1gFnPc1	zásluha
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
občanských	občanský	k2eAgNnPc2d1	občanské
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
spojeným	spojený	k2eAgInSc7d1	spojený
přerušeným	přerušený	k2eAgInSc7d1	přerušený
McCarthyismem	McCarthyismus	k1gInSc7	McCarthyismus
jsou	být	k5eAaImIp3nP	být
nesporné	sporný	k2eNgFnPc1d1	nesporná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
mnohými	mnohý	k2eAgMnPc7d1	mnohý
historiky	historik	k1gMnPc7	historik
označován	označovat	k5eAaImNgMnS	označovat
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
deseti	deset	k4xCc2	deset
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
amerických	americký	k2eAgMnPc2d1	americký
prezidentů	prezident	k1gMnPc2	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Důchod	důchod	k1gInSc1	důchod
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
ženou	žena	k1gFnSc7	žena
strávil	strávit	k5eAaPmAgMnS	strávit
poklidně	poklidně	k6eAd1	poklidně
na	na	k7c6	na
farmě	farma	k1gFnSc6	farma
v	v	k7c6	v
Gettysburgu	Gettysburg	k1gInSc6	Gettysburg
v	v	k7c6	v
Pensylvánii	Pensylvánie	k1gFnSc6	Pensylvánie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
politiky	politika	k1gFnSc2	politika
se	se	k3xPyFc4	se
nedistancoval	distancovat	k5eNaBmAgMnS	distancovat
úplně	úplně	k6eAd1	úplně
<g/>
.	.	kIx.	.
</s>
<s>
Pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
při	při	k7c6	při
kampaních	kampaň	k1gFnPc6	kampaň
stranických	stranický	k2eAgMnPc2d1	stranický
kandidátů	kandidát	k1gMnPc2	kandidát
v	v	k7c6	v
Gettysburgu	Gettysburg	k1gInSc6	Gettysburg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Také	také	k9	také
pro	pro	k7c4	pro
zajímavost	zajímavost	k1gFnSc4	zajímavost
–	–	k?	–
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prezidentem	prezident	k1gMnSc7	prezident
USA	USA	kA	USA
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
rezignovat	rezignovat	k5eAaBmF	rezignovat
na	na	k7c4	na
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
hodnosti	hodnost	k1gFnPc4	hodnost
"	"	kIx"	"
<g/>
General	General	k1gFnSc1	General
of	of	k?	of
the	the	k?	the
Army	Arma	k1gFnSc2	Arma
<g/>
"	"	kIx"	"
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
všemi	všecek	k3xTgFnPc7	všecek
vojenskými	vojenský	k2eAgFnPc7d1	vojenská
funkcemi	funkce	k1gFnPc7	funkce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
12	[number]	k4	12
<g/>
:	:	kIx,	:
<g/>
25	[number]	k4	25
hodin	hodina	k1gFnPc2	hodina
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1969	[number]	k4	1969
ve	v	k7c4	v
"	"	kIx"	"
<g/>
Walter	Walter	k1gMnSc1	Walter
Reed	Reed	k1gMnSc1	Reed
Army	Arma	k1gFnSc2	Arma
Hospital	Hospital	k1gMnSc1	Hospital
<g/>
"	"	kIx"	"
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
<g/>
,	,	kIx,	,
D.C.	D.C.	k1gFnSc1	D.C.
na	na	k7c4	na
srdeční	srdeční	k2eAgNnSc4d1	srdeční
selhání	selhání	k1gNnSc4	selhání
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
78	[number]	k4	78
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pochován	pochován	k2eAgMnSc1d1	pochován
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
a	a	k8xC	a
svým	svůj	k3xOyFgInSc7	svůj
prvním	první	k4xOgMnSc7	první
synem	syn	k1gMnSc7	syn
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
kapli	kaple	k1gFnSc6	kaple
"	"	kIx"	"
<g/>
Place	plac	k1gInSc6	plac
of	of	k?	of
Meditation	Meditation	k1gInSc1	Meditation
<g/>
"	"	kIx"	"
v	v	k7c4	v
"	"	kIx"	"
<g/>
Eisenhower	Eisenhower	k1gMnSc1	Eisenhower
Presidential	Presidential	k1gMnSc1	Presidential
Library	Librara	k1gFnSc2	Librara
<g/>
"	"	kIx"	"
v	v	k7c6	v
Abilene	Abilen	k1gInSc5	Abilen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pocty	pocta	k1gFnSc2	pocta
==	==	k?	==
</s>
</p>
<p>
<s>
Eisenhowerův	Eisenhowerův	k2eAgInSc1d1	Eisenhowerův
obraz	obraz	k1gInSc1	obraz
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
dolarové	dolarový	k2eAgFnSc6d1	dolarová
minci	mince	k1gFnSc6	mince
v	v	k7c6	v
letech	let	k1gInPc6	let
1971	[number]	k4	1971
<g/>
–	–	k?	–
<g/>
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Britská	britský	k2eAgFnSc1d1	britská
lokomotiva	lokomotiva	k1gFnSc1	lokomotiva
třídy	třída	k1gFnSc2	třída
A4	A4	k1gFnPc2	A4
číslo	číslo	k1gNnSc1	číslo
4496	[number]	k4	4496
byla	být	k5eAaImAgFnS	být
přejmenována	přejmenovat	k5eAaPmNgFnS	přejmenovat
z	z	k7c2	z
"	"	kIx"	"
<g/>
Golden	Goldna	k1gFnPc2	Goldna
Shuttle	Shuttle	k1gFnSc2	Shuttle
<g/>
"	"	kIx"	"
na	na	k7c4	na
"	"	kIx"	"
<g/>
Dwight	Dwight	k1gInSc4	Dwight
D.	D.	kA	D.
Eisenhower	Eisenhower	k1gInSc1	Eisenhower
<g/>
"	"	kIx"	"
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
lokomotiva	lokomotiva	k1gFnSc1	lokomotiva
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
vlakovém	vlakový	k2eAgNnSc6d1	vlakové
muzeu	muzeum	k1gNnSc6	muzeum
v	v	k7c4	v
Green	Green	k1gInSc4	Green
Bay	Bay	k1gFnSc2	Bay
ve	v	k7c4	v
Wisconsinu	Wisconsina	k1gFnSc4	Wisconsina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Eisenhowerovo	Eisenhowerův	k2eAgNnSc1d1	Eisenhowerovo
Zdravotnické	zdravotnický	k2eAgNnSc1d1	zdravotnické
středisko	středisko	k1gNnSc1	středisko
v	v	k7c6	v
Rancho	Rancha	k1gMnSc5	Rancha
Mirage	Miragus	k1gMnSc5	Miragus
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
byla	být	k5eAaImAgFnS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
po	po	k7c6	po
prezidentovi	prezident	k1gMnSc6	prezident
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Eisenhower	Eisenhower	k1gInSc1	Eisenhower
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Tunnel	Tunnel	k1gInSc1	Tunnel
byl	být	k5eAaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
1979	[number]	k4	1979
v	v	k7c6	v
Denveru	Denver	k1gInSc6	Denver
v	v	k7c6	v
Coloradu	Colorado	k1gNnSc6	Colorado
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Eisenhower	Eisenhower	k1gInSc1	Eisenhower
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Institute	institut	k1gInSc5	institut
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
D.C.	D.C.	k1gFnSc2	D.C.
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Policy	Polica	k1gFnSc2	Polica
institute	institut	k1gInSc5	institut
to	ten	k3xDgNnSc4	ten
advance	advance	k1gFnPc1	advance
Eisenhower	Eisenhowra	k1gFnPc2	Eisenhowra
<g/>
́	́	k?	́
<g/>
s	s	k7c7	s
intellectual	intellectual	k1gMnSc1	intellectual
and	and	k?	and
leadership	leadership	k1gMnSc1	leadership
legacies	legacies	k1gMnSc1	legacies
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Citáty	citát	k1gInPc1	citát
==	==	k?	==
</s>
</p>
<p>
<s>
A	a	k9	a
people	people	k6eAd1	people
that	that	k2eAgMnSc1d1	that
values	values	k1gMnSc1	values
its	its	k?	its
privileges	privileges	k1gMnSc1	privileges
above	aboev	k1gFnSc2	aboev
its	its	k?	its
principles	principles	k1gMnSc1	principles
soon	soon	k1gMnSc1	soon
loses	loses	k1gMnSc1	loses
both	both	k1gMnSc1	both
<g/>
.	.	kIx.	.
<g/>
(	(	kIx(	(
<g/>
překlad	překlad	k1gInSc1	překlad
<g/>
:	:	kIx,	:
Národ	národ	k1gInSc1	národ
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
si	se	k3xPyFc3	se
cení	cenit	k5eAaImIp3nP	cenit
svých	svůj	k3xOyFgFnPc2	svůj
výsad	výsada	k1gFnPc2	výsada
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
svých	svůj	k3xOyFgFnPc2	svůj
zásad	zásada	k1gFnPc2	zásada
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
ztratí	ztratit	k5eAaPmIp3nS	ztratit
obojí	oboj	k1gFnSc7	oboj
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odrazy	odraz	k1gInPc1	odraz
v	v	k7c6	v
populární	populární	k2eAgFnSc6d1	populární
kultuře	kultura	k1gFnSc6	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
den	den	k1gInSc1	den
<g/>
,	,	kIx,	,
hraný	hraný	k2eAgInSc1d1	hraný
film	film	k1gInSc1	film
o	o	k7c6	o
invazi	invaze	k1gFnSc6	invaze
do	do	k7c2	do
Normandie	Normandie	k1gFnSc2	Normandie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Generál	generál	k1gMnSc1	generál
Eisenhower	Eisenhower	k1gMnSc1	Eisenhower
<g/>
:	:	kIx,	:
Velitel	velitel	k1gMnSc1	velitel
invaze	invaze	k1gFnSc1	invaze
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Eisenhowerova	Eisenhowerův	k2eAgFnSc1d1	Eisenhowerova
doktrína	doktrína	k1gFnSc1	doktrína
</s>
</p>
<p>
<s>
Bangor	Bangor	k1gInSc1	Bangor
(	(	kIx(	(
<g/>
Severní	severní	k2eAgNnSc1d1	severní
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Dwight	Dwighta	k1gFnPc2	Dwighta
D.	D.	kA	D.
Eisenhower	Eisenhower	k1gInSc1	Eisenhower
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Dwight	Dwight	k1gMnSc1	Dwight
D.	D.	kA	D.
Eisenhower	Eisenhower	k1gMnSc1	Eisenhower
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Eisenhower	Eisenhower	k1gInSc1	Eisenhower
-	-	kIx~	-
Farewell	Farewell	k1gInSc1	Farewell
address	address	k1gInSc1	address
(	(	kIx(	(
<g/>
video	video	k1gNnSc1	video
youtube	youtubat	k5eAaPmIp3nS	youtubat
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Ešner	Ešner	k1gMnSc1	Ešner
<g/>
:	:	kIx,	:
Dwight	Dwight	k1gMnSc1	Dwight
D.	D.	kA	D.
Eisenhower	Eisenhower	k1gMnSc1	Eisenhower
</s>
</p>
