<p>
<s>
Antifašismus	antifašismus	k1gInSc1	antifašismus
je	být	k5eAaImIp3nS	být
široký	široký	k2eAgInSc4d1	široký
pojem	pojem	k1gInSc4	pojem
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgFnSc4d1	zahrnující
celou	celý	k2eAgFnSc4d1	celá
škálu	škála	k1gFnSc4	škála
názorových	názorový	k2eAgInPc2d1	názorový
proudů	proud	k1gInPc2	proud
a	a	k8xC	a
hnutí	hnutí	k1gNnPc2	hnutí
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
nějakém	nějaký	k3yIgInSc6	nějaký
smyslu	smysl	k1gInSc6	smysl
nepřátelské	přátelský	k2eNgNnSc1d1	nepřátelské
vůči	vůči	k7c3	vůči
fašismu	fašismus	k1gInSc3	fašismus
a	a	k8xC	a
nacismu	nacismus	k1gInSc3	nacismus
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
mylně	mylně	k6eAd1	mylně
redukován	redukovat	k5eAaBmNgInS	redukovat
na	na	k7c4	na
politické	politický	k2eAgInPc4d1	politický
směry	směr	k1gInPc4	směr
a	a	k8xC	a
hnutí	hnutí	k1gNnPc4	hnutí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
==	==	k?	==
</s>
</p>
<p>
<s>
Antifašismus	antifašismus	k1gInSc1	antifašismus
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
fašismem	fašismus	k1gInSc7	fašismus
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
antifašisticky	antifašisticky	k6eAd1	antifašisticky
projevovali	projevovat	k5eAaImAgMnP	projevovat
jak	jak	k8xS	jak
aktivní	aktivní	k2eAgMnPc1d1	aktivní
odpůrci	odpůrce	k1gMnPc1	odpůrce
politiky	politika	k1gFnSc2	politika
fašismu	fašismus	k1gInSc2	fašismus
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
"	"	kIx"	"
<g/>
pasivní	pasivní	k2eAgMnPc1d1	pasivní
<g/>
"	"	kIx"	"
odpůrci	odpůrce	k1gMnPc1	odpůrce
(	(	kIx(	(
<g/>
názoroví	názorový	k2eAgMnPc1d1	názorový
oponenti	oponent	k1gMnPc1	oponent
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
stávali	stávat	k5eAaImAgMnP	stávat
obětí	oběť	k1gFnSc7	oběť
fašistického	fašistický	k2eAgInSc2d1	fašistický
systému	systém	k1gInSc2	systém
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
příkladem	příklad	k1gInSc7	příklad
antifašismu	antifašismus	k1gInSc2	antifašismus
je	být	k5eAaImIp3nS	být
odboj	odboj	k1gInSc4	odboj
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
chápán	chápat	k5eAaImNgInS	chápat
jako	jako	k8xC	jako
aktivní	aktivní	k2eAgInSc1d1	aktivní
projev	projev	k1gInSc1	projev
politického	politický	k2eAgInSc2d1	politický
názoru	názor	k1gInSc2	názor
a	a	k8xC	a
přímý	přímý	k2eAgInSc1d1	přímý
i	i	k8xC	i
nepřímý	přímý	k2eNgInSc1d1	nepřímý
(	(	kIx(	(
<g/>
třebaže	třebaže	k8xS	třebaže
i	i	k9	i
skrytý	skrytý	k2eAgInSc4d1	skrytý
<g/>
)	)	kIx)	)
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
nacistické	nacistický	k2eAgFnSc3d1	nacistická
okupaci	okupace	k1gFnSc3	okupace
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
poválečném	poválečný	k2eAgNnSc6d1	poválečné
Československu	Československo	k1gNnSc6	Československo
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
Československý	československý	k2eAgInSc1d1	československý
svaz	svaz	k1gInSc1	svaz
protifašistických	protifašistický	k2eAgMnPc2d1	protifašistický
bojovníků	bojovník	k1gMnPc2	bojovník
<g/>
,	,	kIx,	,
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnPc7	jeho
členy	člen	k1gMnPc7	člen
(	(	kIx(	(
<g/>
opět	opět	k6eAd1	opět
vcelku	vcelku	k6eAd1	vcelku
logicky	logicky	k6eAd1	logicky
<g/>
)	)	kIx)	)
i	i	k8xC	i
řada	řada	k1gFnSc1	řada
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
během	během	k7c2	během
války	válka	k1gFnSc2	válka
nikoli	nikoli	k9	nikoli
aktivními	aktivní	k2eAgMnPc7d1	aktivní
bojovníky	bojovník	k1gMnPc7	bojovník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
oběťmi	oběť	k1gFnPc7	oběť
nacismu	nacismus	k1gInSc2	nacismus
(	(	kIx(	(
<g/>
přeživšími	přeživší	k2eAgNnPc7d1	přeživší
z	z	k7c2	z
nacisty	nacista	k1gMnSc2	nacista
zničené	zničený	k2eAgFnPc1d1	zničená
obce	obec	k1gFnPc1	obec
Lidice	Lidice	k1gInPc4	Lidice
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Současnost	současnost	k1gFnSc4	současnost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
antifašismus	antifašismus	k1gInSc1	antifašismus
také	také	k9	také
dělí	dělit	k5eAaImIp3nS	dělit
podle	podle	k7c2	podle
otázky	otázka	k1gFnSc2	otázka
použití	použití	k1gNnSc2	použití
násilí	násilí	k1gNnSc2	násilí
na	na	k7c4	na
liberální	liberální	k2eAgInSc4d1	liberální
antifašismus	antifašismus	k1gInSc4	antifašismus
a	a	k8xC	a
na	na	k7c4	na
militantní	militantní	k2eAgInSc4d1	militantní
antifašismus	antifašismus	k1gInSc4	antifašismus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
obhajuje	obhajovat	k5eAaImIp3nS	obhajovat
použití	použití	k1gNnSc4	použití
násilí	násilí	k1gNnSc2	násilí
proti	proti	k7c3	proti
fašistům	fašista	k1gMnPc3	fašista
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
organizací	organizace	k1gFnPc2	organizace
obhajující	obhajující	k2eAgInSc1d1	obhajující
militantní	militantní	k2eAgInSc1d1	militantní
antifašismus	antifašismus	k1gInSc1	antifašismus
je	být	k5eAaImIp3nS	být
Antifa	Antif	k1gMnSc4	Antif
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Československý	československý	k2eAgInSc1d1	československý
svaz	svaz	k1gInSc1	svaz
protifašistických	protifašistický	k2eAgMnPc2d1	protifašistický
bojovníků	bojovník	k1gMnPc2	bojovník
</s>
</p>
<p>
<s>
Svaz	svaz	k1gInSc1	svaz
protifašistických	protifašistický	k2eAgMnPc2d1	protifašistický
bojovníků	bojovník	k1gMnPc2	bojovník
</s>
</p>
<p>
<s>
Odboj	odboj	k1gInSc1	odboj
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
</s>
</p>
<p>
<s>
Československý	československý	k2eAgInSc1d1	československý
odboj	odboj	k1gInSc1	odboj
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Antifašismus	antifašismus	k1gInSc1	antifašismus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
antifašismus	antifašismus	k1gInSc1	antifašismus
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Antifašismus	antifašismus	k1gInSc1	antifašismus
(	(	kIx(	(
<g/>
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
–	–	k?	–
Martin	Martin	k1gMnSc1	Martin
Bastl	Bastl	k1gMnSc1	Bastl
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Intellectuals	Intellectuals	k1gInSc1	Intellectuals
and	and	k?	and
Anti-Fascism	Anti-Fascism	k1gInSc1	Anti-Fascism
<g/>
:	:	kIx,	:
For	forum	k1gNnPc2	forum
a	a	k8xC	a
Critical	Critical	k1gFnPc3	Critical
Historization	Historization	k1gInSc1	Historization
–	–	k?	–
Enzo	Enzo	k6eAd1	Enzo
Traverso	Traversa	k1gFnSc5	Traversa
<g/>
,	,	kIx,	,
New	New	k1gMnSc1	New
Politics	Politicsa	k1gFnPc2	Politicsa
<g/>
,	,	kIx,	,
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
,	,	kIx,	,
no	no	k9	no
<g/>
.	.	kIx.	.
4	[number]	k4	4
(	(	kIx(	(
<g/>
new	new	k?	new
series	series	k1gInSc1	series
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
whole	whole	k6eAd1	whole
no	no	k9	no
<g/>
.	.	kIx.	.
36	[number]	k4	36
<g/>
,	,	kIx,	,
Winter	Winter	k1gMnSc1	Winter
2004	[number]	k4	2004
</s>
</p>
