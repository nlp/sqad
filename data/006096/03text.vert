<s>
Egypt	Egypt	k1gInSc1	Egypt
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
Egyptská	egyptský	k2eAgFnSc1d1	egyptská
arabská	arabský	k2eAgFnSc1d1	arabská
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
EAR	EAR	kA	EAR
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ve	v	k7c6	v
staré	starý	k2eAgFnSc6d1	stará
češtině	čeština	k1gFnSc6	čeština
Ejipt	Ejipt	k1gInSc1	Ejipt
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
arabská	arabský	k2eAgFnSc1d1	arabská
republika	republika	k1gFnSc1	republika
v	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
Africe	Afrika	k1gFnSc6	Afrika
(	(	kIx(	(
<g/>
malou	malý	k2eAgFnSc7d1	malá
částí	část	k1gFnSc7	část
též	též	k9	též
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc1d1	ležící
na	na	k7c6	na
Nilu	Nil	k1gInSc6	Nil
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
hraničí	hraničit	k5eAaImIp3nP	hraničit
s	s	k7c7	s
Libyí	Libye	k1gFnSc7	Libye
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
se	s	k7c7	s
Súdánem	Súdán	k1gInSc7	Súdán
<g/>
,	,	kIx,	,
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
s	s	k7c7	s
Izraelem	Izrael	k1gInSc7	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
severu	sever	k1gInSc2	sever
ho	on	k3xPp3gMnSc2	on
omývají	omývat	k5eAaImIp3nP	omývat
vody	voda	k1gFnPc1	voda
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
z	z	k7c2	z
východu	východ	k1gInSc2	východ
pak	pak	k6eAd1	pak
Rudé	rudý	k2eAgNnSc1d1	Rudé
moře	moře	k1gNnSc1	moře
<g/>
.	.	kIx.	.
</s>
<s>
Egypt	Egypt	k1gInSc1	Egypt
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejlidnatějších	lidnatý	k2eAgFnPc2d3	nejlidnatější
zemí	zem	k1gFnPc2	zem
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
přibližně	přibližně	k6eAd1	přibližně
94	[number]	k4	94
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
žije	žít	k5eAaImIp3nS	žít
poblíž	poblíž	k6eAd1	poblíž
břehů	břeh	k1gInPc2	břeh
řeky	řeka	k1gFnSc2	řeka
Nil	Nil	k1gInSc1	Nil
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
40	[number]	k4	40
000	[number]	k4	000
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jediná	jediný	k2eAgFnSc1d1	jediná
orná	orný	k2eAgFnSc1d1	orná
půda	půda	k1gFnSc1	půda
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
nehostinné	hostinný	k2eNgFnSc2d1	nehostinná
pouště	poušť	k1gFnSc2	poušť
Sahary	Sahara	k1gFnSc2	Sahara
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
velmi	velmi	k6eAd1	velmi
řídce	řídce	k6eAd1	řídce
obydlena	obydlen	k2eAgFnSc1d1	obydlena
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
polovina	polovina	k1gFnSc1	polovina
obyvatel	obyvatel	k1gMnPc2	obyvatel
Egypta	Egypt	k1gInSc2	Egypt
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
v	v	k7c6	v
hustě	hustě	k6eAd1	hustě
obydlených	obydlený	k2eAgNnPc6d1	obydlené
centrech	centrum	k1gNnPc6	centrum
Káhiry	Káhira	k1gFnSc2	Káhira
<g/>
,	,	kIx,	,
Alexandrie	Alexandrie	k1gFnSc2	Alexandrie
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
velkých	velký	k2eAgNnPc2d1	velké
měst	město	k1gNnPc2	město
Nilské	nilský	k2eAgFnSc2d1	nilská
delty	delta	k1gFnSc2	delta
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
především	především	k9	především
díky	díky	k7c3	díky
starověké	starověký	k2eAgFnSc3d1	starověká
civilizaci	civilizace	k1gFnSc3	civilizace
a	a	k8xC	a
několika	několik	k4yIc7	několik
světově	světově	k6eAd1	světově
proslulým	proslulý	k2eAgInPc3d1	proslulý
monumentům	monument	k1gInPc3	monument
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
pyramidy	pyramida	k1gFnPc1	pyramida
v	v	k7c6	v
Gíze	Gíze	k1gFnSc6	Gíze
a	a	k8xC	a
Velká	velký	k2eAgFnSc1d1	velká
sfinga	sfinga	k1gFnSc1	sfinga
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Luxor	Luxora	k1gFnPc2	Luxora
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
skrývá	skrývat	k5eAaImIp3nS	skrývat
početné	početný	k2eAgInPc4d1	početný
starověké	starověký	k2eAgInPc4d1	starověký
artefakty	artefakt	k1gInPc4	artefakt
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
chrámový	chrámový	k2eAgInSc1d1	chrámový
komplex	komplex	k1gInSc1	komplex
Karnak	Karnak	k1gInSc1	Karnak
a	a	k8xC	a
Údolí	údolí	k1gNnSc1	údolí
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Egypťané	Egypťan	k1gMnPc1	Egypťan
jsou	být	k5eAaImIp3nP	být
obecně	obecně	k6eAd1	obecně
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c2	za
kulturně	kulturně	k6eAd1	kulturně
a	a	k8xC	a
politicky	politicky	k6eAd1	politicky
důležitý	důležitý	k2eAgInSc1d1	důležitý
národ	národ	k1gInSc1	národ
Středního	střední	k2eAgInSc2d1	střední
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
mívaly	mívat	k5eAaImAgInP	mívat
státy	stát	k1gInPc1	stát
nebo	nebo	k8xC	nebo
území	území	k1gNnSc1	území
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
dnešní	dnešní	k2eAgInPc1d1	dnešní
státy	stát	k1gInPc1	stát
nacházejí	nacházet	k5eAaImIp3nP	nacházet
<g/>
,	,	kIx,	,
poněkud	poněkud	k6eAd1	poněkud
odlišná	odlišný	k2eAgNnPc4d1	odlišné
jména	jméno	k1gNnPc4	jméno
<g/>
,	,	kIx,	,
než	než	k8xS	než
jaká	jaký	k3yIgNnPc1	jaký
mají	mít	k5eAaImIp3nP	mít
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
čím	co	k3yInSc7	co
vzdálenější	vzdálený	k2eAgFnSc1d2	vzdálenější
minulost	minulost	k1gFnSc1	minulost
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
propastnější	propastný	k2eAgFnSc1d2	propastnější
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
být	být	k5eAaImF	být
rozdíl	rozdíl	k1gInSc4	rozdíl
mezi	mezi	k7c7	mezi
současným	současný	k2eAgInSc7d1	současný
a	a	k8xC	a
tehdejším	tehdejší	k2eAgInSc7d1	tehdejší
názvem	název	k1gInSc7	název
daného	daný	k2eAgNnSc2d1	dané
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
Egypt	Egypt	k1gInSc1	Egypt
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
není	být	k5eNaImIp3nS	být
výjimkou	výjimka	k1gFnSc7	výjimka
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
současné	současný	k2eAgNnSc1d1	současné
označení	označení	k1gNnSc1	označení
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgInPc4	svůj
kořeny	kořen	k1gInPc4	kořen
v	v	k7c6	v
egyptštině	egyptština	k1gFnSc6	egyptština
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
původně	původně	k6eAd1	původně
to	ten	k3xDgNnSc4	ten
s	s	k7c7	s
názvem	název	k1gInSc7	název
této	tento	k3xDgFnSc2	tento
země	zem	k1gFnSc2	zem
nemělo	mít	k5eNaImAgNnS	mít
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgNnSc2d1	společné
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
šlo	jít	k5eAaImAgNnS	jít
totiž	totiž	k9	totiž
o	o	k7c4	o
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
Ptahův	Ptahův	k2eAgInSc4d1	Ptahův
chrámový	chrámový	k2eAgInSc4d1	chrámový
komplex	komplex	k1gInSc4	komplex
v	v	k7c6	v
městě	město	k1gNnSc6	město
Mennofer	Mennofra	k1gFnPc2	Mennofra
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Memfis	Memfis	k1gInSc1	Memfis
<g/>
)	)	kIx)	)
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
nazýván	nazývat	k5eAaImNgInS	nazývat
"	"	kIx"	"
<g/>
Palác	palác	k1gInSc1	palác
Ptahovy	Ptahův	k2eAgFnSc2d1	Ptahova
Ka	Ka	k1gFnSc2	Ka
<g/>
"	"	kIx"	"
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
v	v	k7c6	v
egyptštině	egyptština	k1gFnSc6	egyptština
řekne	říct	k5eAaPmIp3nS	říct
"	"	kIx"	"
<g/>
hat	hat	k0	hat
ka	ka	k?	ka
ptah	ptah	k1gInSc1	ptah
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
názvu	název	k1gInSc2	název
se	se	k3xPyFc4	se
však	však	k9	však
během	během	k7c2	během
staletí	staletí	k1gNnSc2	staletí
stal	stát	k5eAaPmAgInS	stát
nový	nový	k2eAgInSc1d1	nový
výraz	výraz	k1gInSc1	výraz
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
se	se	k3xPyFc4	se
z	z	k7c2	z
půl	půl	k1xP	půl
dne	den	k1gInSc2	den
stalo	stát	k5eAaPmAgNnS	stát
poledne	poledne	k1gNnSc1	poledne
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
vyslovovat	vyslovovat	k5eAaImF	vyslovovat
přibližně	přibližně	k6eAd1	přibližně
"	"	kIx"	"
<g/>
hykupta	hykupta	k1gFnSc1	hykupta
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
podobě	podoba	k1gFnSc6	podoba
se	se	k3xPyFc4	se
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
označením	označení	k1gNnSc7	označení
setkali	setkat	k5eAaPmAgMnP	setkat
Řekové	Řek	k1gMnPc1	Řek
<g/>
.	.	kIx.	.
</s>
<s>
Přizpůsobili	přizpůsobit	k5eAaPmAgMnP	přizpůsobit
si	se	k3xPyFc3	se
jej	on	k3xPp3gMnSc4	on
svému	svůj	k3xOyFgInSc3	svůj
jazyku	jazyk	k1gInSc3	jazyk
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
jej	on	k3xPp3gMnSc4	on
používat	používat	k5eAaImF	používat
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
celé	celý	k2eAgFnSc2d1	celá
země	zem	k1gFnSc2	zem
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
i	i	k9	i
měli	mít	k5eAaImAgMnP	mít
mýtus	mýtus	k1gInSc4	mýtus
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vlastně	vlastně	k9	vlastně
jméno	jméno	k1gNnSc1	jméno
egyptského	egyptský	k2eAgMnSc2d1	egyptský
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
po	po	k7c6	po
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
i	i	k9	i
celá	celý	k2eAgFnSc1d1	celá
zem	zem	k1gFnSc1	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
Řeky	Řek	k1gMnPc4	Řek
zkomolený	zkomolený	k2eAgInSc1d1	zkomolený
název	název	k1gInSc1	název
zněl	znět	k5eAaImAgInS	znět
AIΓ	AIΓ	k1gFnSc3	AIΓ
(	(	kIx(	(
<g/>
Aigyptos	Aigyptos	k1gInSc1	Aigyptos
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
označením	označení	k1gNnSc7	označení
pro	pro	k7c4	pro
Egypt	Egypt	k1gInSc4	Egypt
setkali	setkat	k5eAaPmAgMnP	setkat
Římané	Říman	k1gMnPc1	Říman
<g/>
,	,	kIx,	,
převzali	převzít	k5eAaPmAgMnP	převzít
jej	on	k3xPp3gNnSc4	on
a	a	k8xC	a
přizpůsobili	přizpůsobit	k5eAaPmAgMnP	přizpůsobit
si	se	k3xPyFc3	se
jej	on	k3xPp3gMnSc4	on
zase	zase	k9	zase
svému	svůj	k3xOyFgInSc3	svůj
jazyku	jazyk	k1gInSc3	jazyk
a	a	k8xC	a
tak	tak	k6eAd1	tak
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
Æ	Æ	k?	Æ
(	(	kIx(	(
<g/>
Égyptus	Égyptus	k1gMnSc1	Égyptus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
když	když	k8xS	když
se	se	k3xPyFc4	se
Egypt	Egypt	k1gInSc1	Egypt
stal	stát	k5eAaPmAgInS	stát
součástí	součást	k1gFnSc7	součást
Impéria	impérium	k1gNnSc2	impérium
<g/>
)	)	kIx)	)
i	i	k9	i
oficiální	oficiální	k2eAgInSc4d1	oficiální
název	název	k1gInSc4	název
provincie	provincie	k1gFnSc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
odvozen	odvodit	k5eAaPmNgInS	odvodit
náš	náš	k3xOp1gInSc1	náš
současný	současný	k2eAgInSc1d1	současný
název	název	k1gInSc1	název
Egypt	Egypt	k1gInSc1	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Sami	sám	k3xTgMnPc1	sám
Egypťané	Egypťan	k1gMnPc1	Egypťan
však	však	k9	však
svou	svůj	k3xOyFgFnSc4	svůj
zemi	zem	k1gFnSc4	zem
nazývali	nazývat	k5eAaImAgMnP	nazývat
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
neměli	mít	k5eNaImAgMnP	mít
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
jen	jen	k9	jen
jeden	jeden	k4xCgInSc4	jeden
název	název	k1gInSc4	název
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hned	hned	k6eAd1	hned
několik	několik	k4yIc4	několik
a	a	k8xC	a
každý	každý	k3xTgInSc4	každý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
Egypťané	Egypťan	k1gMnPc1	Egypťan
svou	svůj	k3xOyFgFnSc4	svůj
zem	zem	k1gFnSc4	zem
vnímali	vnímat	k5eAaImAgMnP	vnímat
a	a	k8xC	a
co	co	k3yRnSc4	co
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
znamenala	znamenat	k5eAaImAgFnS	znamenat
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Staroegyptské	staroegyptský	k2eAgInPc4d1	staroegyptský
názvy	název	k1gInPc4	název
Egypta	Egypt	k1gInSc2	Egypt
<g/>
)	)	kIx)	)
Podle	podle	k7c2	podle
archeologických	archeologický	k2eAgInPc2d1	archeologický
nálezů	nález	k1gInPc2	nález
a	a	k8xC	a
egyptských	egyptský	k2eAgInPc2d1	egyptský
hieroglyfů	hieroglyf	k1gInPc2	hieroglyf
nazývali	nazývat	k5eAaImAgMnP	nazývat
nejčastěji	často	k6eAd3	často
starověcí	starověký	k2eAgMnPc1d1	starověký
Egypťané	Egypťan	k1gMnPc1	Egypťan
svou	svůj	k3xOyFgFnSc4	svůj
zemi	zem	k1gFnSc4	zem
"	"	kIx"	"
<g/>
Kemet	Kemet	k1gInSc4	Kemet
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Černá	černý	k2eAgFnSc1d1	černá
země	země	k1gFnSc1	země
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
i	i	k9	i
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
obyvatelé	obyvatel	k1gMnPc1	obyvatel
svou	svůj	k3xOyFgFnSc4	svůj
zemi	zem	k1gFnSc4	zem
nenazývají	nazývat	k5eNaImIp3nP	nazývat
Egypt	Egypt	k1gInSc4	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
název	název	k1gInSc1	název
země	zem	k1gFnSc2	zem
zní	znět	k5eAaImIp3nS	znět
Džumhúrja	Džumhúrj	k2eAgFnSc1d1	Džumhúrj
al-Misríja	al-Misríja	k1gFnSc1	al-Misríja
al-	al-	k?	al-
<g/>
'	'	kIx"	'
<g/>
Arabíja	Arabíja	k1gMnSc1	Arabíja
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
v	v	k7c6	v
arabském	arabský	k2eAgInSc6d1	arabský
světě	svět	k1gInSc6	svět
a	a	k8xC	a
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
samotném	samotný	k2eAgInSc6d1	samotný
v	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
hovoru	hovor	k1gInSc6	hovor
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
zkráceným	zkrácený	k2eAgMnSc7d1	zkrácený
Maṣ	Maṣ	k1gMnSc7	Maṣ
či	či	k8xC	či
Miṣ	Miṣ	k1gMnSc7	Miṣ
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
Egypta	Egypt	k1gInSc2	Egypt
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejdelší	dlouhý	k2eAgFnPc4d3	nejdelší
souvislé	souvislý	k2eAgFnPc4d1	souvislá
historie	historie	k1gFnPc4	historie
sjednoceného	sjednocený	k2eAgInSc2d1	sjednocený
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nilské	nilský	k2eAgNnSc1d1	nilské
údolí	údolí	k1gNnSc1	údolí
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
zeměpisnou	zeměpisný	k2eAgFnSc4d1	zeměpisná
oblast	oblast	k1gFnSc4	oblast
ohraničenou	ohraničený	k2eAgFnSc4d1	ohraničená
na	na	k7c6	na
východě	východ	k1gInSc6	východ
a	a	k8xC	a
západě	západ	k1gInSc6	západ
pouštěmi	poušť	k1gFnPc7	poušť
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
mořem	moře	k1gNnSc7	moře
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc2	jih
nilskými	nilský	k2eAgInPc7d1	nilský
katarakty	katarakt	k1gInPc7	katarakt
<g/>
.	.	kIx.	.
</s>
<s>
Snaha	snaha	k1gFnSc1	snaha
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
nilské	nilský	k2eAgFnPc4d1	nilská
záplavy	záplava	k1gFnPc4	záplava
vedla	vést	k5eAaImAgFnS	vést
v	v	k7c6	v
době	doba	k1gFnSc6	doba
3	[number]	k4	3
000	[number]	k4	000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
států	stát	k1gInPc2	stát
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Pravěký	pravěký	k2eAgInSc1d1	pravěký
Egypt	Egypt	k1gInSc1	Egypt
a	a	k8xC	a
Starověký	starověký	k2eAgInSc1d1	starověký
Egypt	Egypt	k1gInSc1	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
paleolitu	paleolit	k1gInSc2	paleolit
(	(	kIx(	(
<g/>
700000	[number]	k4	700000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
–	–	k?	–
10000	[number]	k4	10000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Období	období	k1gNnSc1	období
mezolitu	mezolit	k1gInSc2	mezolit
(	(	kIx(	(
<g/>
10000	[number]	k4	10000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
–	–	k?	–
5000	[number]	k4	5000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Období	období	k1gNnSc1	období
neolitu	neolit	k1gInSc2	neolit
a	a	k8xC	a
předdynastické	předdynastický	k2eAgNnSc4d1	předdynastický
období	období	k1gNnSc4	období
(	(	kIx(	(
<g/>
5000	[number]	k4	5000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
<g />
.	.	kIx.	.
</s>
<s>
l.	l.	k?	l.
–	–	k?	–
3000	[number]	k4	3000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Raně	raně	k6eAd1	raně
dynastické	dynastický	k2eAgNnSc4d1	dynastické
období	období	k1gNnSc4	období
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
až	až	k9	až
2	[number]	k4	2
<g/>
.	.	kIx.	.
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
;	;	kIx,	;
do	do	k7c2	do
cca	cca	kA	cca
27	[number]	k4	27
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Skupinky	skupinka	k1gFnSc2	skupinka
člověka	člověk	k1gMnSc2	člověk
rodu	rod	k1gInSc2	rod
Homo	Homo	k1gMnSc1	Homo
erectus	erectus	k1gMnSc1	erectus
se	se	k3xPyFc4	se
při	při	k7c6	při
rozšiřování	rozšiřování	k1gNnSc6	rozšiřování
z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
do	do	k7c2	do
západní	západní	k2eAgFnSc2d1	západní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc2d1	jižní
Asie	Asie	k1gFnSc2	Asie
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
též	též	k9	též
usadily	usadit	k5eAaPmAgFnP	usadit
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Nilu	Nil	k1gInSc2	Nil
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
středního	střední	k2eAgInSc2d1	střední
paleolitu	paleolit	k1gInSc2	paleolit
pochází	pocházet	k5eAaImIp3nS	pocházet
mnoho	mnoho	k4c1	mnoho
lokalit	lokalita	k1gFnPc2	lokalita
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
dokládají	dokládat	k5eAaImIp3nP	dokládat
rohovce	rohovec	k1gInPc4	rohovec
vhodného	vhodný	k2eAgInSc2d1	vhodný
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
pazourků	pazourek	k1gInPc2	pazourek
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
skupiny	skupina	k1gFnPc1	skupina
lovců	lovec	k1gMnPc2	lovec
a	a	k8xC	a
sběračů	sběrač	k1gMnPc2	sběrač
začaly	začít	k5eAaPmAgFnP	začít
usazovat	usazovat	k5eAaImF	usazovat
a	a	k8xC	a
v	v	k7c6	v
Nilském	nilský	k2eAgNnSc6d1	nilské
údolí	údolí	k1gNnSc6	údolí
začaly	začít	k5eAaPmAgFnP	začít
vznikat	vznikat	k5eAaImF	vznikat
trvalejší	trvalý	k2eAgNnPc4d2	trvalejší
zemědělská	zemědělský	k2eAgNnPc4d1	zemědělské
sídliště	sídliště	k1gNnPc4	sídliště
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
využívali	využívat	k5eAaPmAgMnP	využívat
bohatství	bohatství	k1gNnSc4	bohatství
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
jim	on	k3xPp3gMnPc3	on
nabízelo	nabízet	k5eAaImAgNnS	nabízet
údolí	údolí	k1gNnSc1	údolí
Nilu	Nil	k1gInSc2	Nil
<g/>
.	.	kIx.	.
</s>
<s>
Nilské	nilský	k2eAgNnSc1d1	nilské
bahno	bahno	k1gNnSc1	bahno
poskytovalo	poskytovat	k5eAaImAgNnS	poskytovat
materiál	materiál	k1gInSc4	materiál
na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
nízkých	nízký	k2eAgFnPc2d1	nízká
stěn	stěna	k1gFnPc2	stěna
chatrčí	chatrč	k1gFnPc2	chatrč
<g/>
,	,	kIx,	,
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
keramiky	keramika	k1gFnSc2	keramika
a	a	k8xC	a
také	také	k9	také
každoroční	každoroční	k2eAgFnSc2d1	každoroční
nilské	nilský	k2eAgFnSc2d1	nilská
záplavy	záplava	k1gFnSc2	záplava
přinášely	přinášet	k5eAaImAgFnP	přinášet
živiny	živina	k1gFnPc1	živina
pro	pro	k7c4	pro
rostlinstvo	rostlinstvo	k1gNnSc4	rostlinstvo
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vývoje	vývoj	k1gInSc2	vývoj
keramiky	keramika	k1gFnSc2	keramika
můžeme	moct	k5eAaImIp1nP	moct
usuzovat	usuzovat	k5eAaImF	usuzovat
na	na	k7c4	na
vznik	vznik	k1gInSc4	vznik
několika	několik	k4yIc2	několik
kultur	kultura	k1gFnPc2	kultura
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
je	být	k5eAaImIp3nS	být
badárská	badárský	k2eAgFnSc1d1	badárský
kultura	kultura	k1gFnSc1	kultura
a	a	k8xC	a
nakádská	nakádský	k2eAgFnSc1d1	nakádský
kultura	kultura	k1gFnSc1	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
se	se	k3xPyFc4	se
kmenové	kmenový	k2eAgInPc1d1	kmenový
svazy	svaz	k1gInPc1	svaz
<g/>
,	,	kIx,	,
vyvolané	vyvolaný	k2eAgInPc1d1	vyvolaný
potřebou	potřeba	k1gFnSc7	potřeba
organizovat	organizovat	k5eAaBmF	organizovat
odvodňovací	odvodňovací	k2eAgFnSc2d1	odvodňovací
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Horním	horní	k2eAgInSc6d1	horní
Egyptě	Egypt	k1gInSc6	Egypt
nabyl	nabýt	k5eAaPmAgInS	nabýt
převahy	převaha	k1gFnSc2	převaha
kmen	kmen	k1gInSc1	kmen
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
sídlil	sídlit	k5eAaImAgInS	sídlit
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
osady	osada	k1gFnSc2	osada
Nechen	Nechna	k1gFnPc2	Nechna
(	(	kIx(	(
<g/>
pozdější	pozdní	k2eAgFnSc2d2	pozdější
Hierakonpolis	Hierakonpolis	k1gFnSc2	Hierakonpolis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Dolním	dolní	k2eAgInSc6d1	dolní
Egyptě	Egypt	k1gInSc6	Egypt
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
kmen	kmen	k1gInSc1	kmen
sídlící	sídlící	k2eAgInSc1d1	sídlící
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
deltě	delta	k1gFnSc6	delta
Nilu	Nil	k1gInSc2	Nil
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
osady	osada	k1gFnSc2	osada
Bútó	Bútó	k1gFnSc2	Bútó
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
3150	[number]	k4	3150
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
sjednotil	sjednotit	k5eAaPmAgInS	sjednotit
faraon	faraon	k1gInSc1	faraon
Menes	Menes	k1gInSc1	Menes
Horní	horní	k2eAgInSc1d1	horní
a	a	k8xC	a
Dolní	dolní	k2eAgInSc1d1	dolní
Egypt	Egypt	k1gInSc1	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Stará	starý	k2eAgFnSc1d1	stará
říše	říše	k1gFnSc1	říše
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gMnPc1	jejíž
faraoni	faraon	k1gMnPc1	faraon
si	se	k3xPyFc3	se
nechávají	nechávat	k5eAaImIp3nP	nechávat
budovat	budovat	k5eAaImF	budovat
pyramidy	pyramida	k1gFnPc1	pyramida
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
badatelé	badatel	k1gMnPc1	badatel
ztotožňují	ztotožňovat	k5eAaImIp3nP	ztotožňovat
tohoto	tento	k3xDgMnSc4	tento
panovníka	panovník	k1gMnSc4	panovník
s	s	k7c7	s
králem	král	k1gMnSc7	král
Narmerem	Narmer	k1gMnSc7	Narmer
nebo	nebo	k8xC	nebo
s	s	k7c7	s
panovníkem	panovník	k1gMnSc7	panovník
Hor-Aha	Hor-Ah	k1gMnSc2	Hor-Ah
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
má	mít	k5eAaImIp3nS	mít
pravdu	pravda	k1gFnSc4	pravda
je	být	k5eAaImIp3nS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
používání	používání	k1gNnSc2	používání
dvou	dva	k4xCgNnPc2	dva
jmen	jméno	k1gNnPc2	jméno
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
osobního	osobní	k2eAgMnSc2d1	osobní
a	a	k8xC	a
druhého	druhý	k4xOgMnSc2	druhý
trůnního	trůnní	k2eAgMnSc2d1	trůnní
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
pozdějším	pozdní	k2eAgInSc7d2	pozdější
přepisem	přepis	k1gInSc7	přepis
jmen	jméno	k1gNnPc2	jméno
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc7	jejich
pořečtěním	pořečtění	k1gNnSc7	pořečtění
mohlo	moct	k5eAaImAgNnS	moct
znění	znění	k1gNnSc1	znění
změnit	změnit	k5eAaPmF	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výzkumu	výzkum	k1gInSc6	výzkum
staroegyptské	staroegyptský	k2eAgFnSc2d1	staroegyptská
historie	historie	k1gFnSc2	historie
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
relativního	relativní	k2eAgNnSc2d1	relativní
datování	datování	k1gNnSc2	datování
podle	podle	k7c2	podle
vlády	vláda	k1gFnSc2	vláda
panovníků	panovník	k1gMnPc2	panovník
a	a	k8xC	a
egyptských	egyptský	k2eAgFnPc2d1	egyptská
dynastií	dynastie	k1gFnPc2	dynastie
<g/>
,	,	kIx,	,
vycházejícího	vycházející	k2eAgMnSc2d1	vycházející
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
díla	dílo	k1gNnSc2	dílo
řeckého	řecký	k2eAgMnSc2d1	řecký
historika	historik	k1gMnSc2	historik
Manehta	Maneht	k1gMnSc2	Maneht
<g/>
.	.	kIx.	.
</s>
<s>
Stará	starý	k2eAgFnSc1d1	stará
říše	říše	k1gFnSc1	říše
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
až	až	k9	až
8	[number]	k4	8
<g/>
.	.	kIx.	.
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
;	;	kIx,	;
2700	[number]	k4	2700
–	–	k?	–
2180	[number]	k4	2180
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Se	s	k7c7	s
začátkem	začátek	k1gInSc7	začátek
panování	panování	k1gNnSc2	panování
třetí	třetí	k4xOgFnSc2	třetí
dynastie	dynastie	k1gFnSc2	dynastie
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
tzv.	tzv.	kA	tzv.
Stará	Stará	k1gFnSc1	Stará
říše	říše	k1gFnSc1	říše
<g/>
.	.	kIx.	.
</s>
<s>
Panovník	panovník	k1gMnSc1	panovník
byl	být	k5eAaImAgMnS	být
ztělesněním	ztělesnění	k1gNnSc7	ztělesnění
boha	bůh	k1gMnSc2	bůh
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
neomezenou	omezený	k2eNgFnSc4d1	neomezená
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
faraona	faraon	k1gMnSc2	faraon
Džosera	Džoser	k1gMnSc2	Džoser
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
2610	[number]	k4	2610
<g/>
–	–	k?	–
<g/>
2590	[number]	k4	2590
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
a	a	k8xC	a
panovníků	panovník	k1gMnPc2	panovník
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
a	a	k8xC	a
páté	pátá	k1gFnSc2	pátá
dynastie	dynastie	k1gFnSc2	dynastie
se	se	k3xPyFc4	se
rozloha	rozloha	k1gFnSc1	rozloha
Egypta	Egypt	k1gInSc2	Egypt
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
až	až	k9	až
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
k	k	k7c3	k
Asuánu	Asuán	k1gInSc3	Asuán
<g/>
.	.	kIx.	.
</s>
<s>
Faraonové	faraon	k1gMnPc1	faraon
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	době	k6eAd1	době
uctíváni	uctíván	k2eAgMnPc1d1	uctíván
jako	jako	k8xS	jako
synové	syn	k1gMnPc1	syn
slunečního	sluneční	k2eAgMnSc2d1	sluneční
boha	bůh	k1gMnSc2	bůh
Re	re	k9	re
<g/>
.	.	kIx.	.
</s>
<s>
Džosér	Džosér	k1gInSc1	Džosér
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
nechal	nechat	k5eAaPmAgMnS	nechat
postavit	postavit	k5eAaPmF	postavit
pyramidu	pyramida	k1gFnSc4	pyramida
pojmenovanou	pojmenovaný	k2eAgFnSc4d1	pojmenovaná
po	po	k7c6	po
něm.	něm.	k?	něm.
První	první	k4xOgNnSc1	první
přechodné	přechodný	k2eAgNnSc1d1	přechodné
období	období	k1gNnSc1	období
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
až	až	k9	až
11	[number]	k4	11
<g/>
.	.	kIx.	.
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2180	[number]	k4	2180
–	–	k?	–
2065	[number]	k4	2065
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Střední	střední	k2eAgFnSc2d1	střední
říše	říš	k1gFnSc2	říš
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
až	až	k9	až
14	[number]	k4	14
<g/>
.	.	kIx.	.
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
;	;	kIx,	;
<g />
.	.	kIx.	.
</s>
<s>
2055	[number]	k4	2055
-	-	kIx~	-
1650	[number]	k4	1650
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Druhé	druhý	k4xOgNnSc1	druhý
přechodné	přechodný	k2eAgNnSc1d1	přechodné
období	období	k1gNnSc1	období
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
až	až	k9	až
17	[number]	k4	17
<g/>
.	.	kIx.	.
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
,	,	kIx,	,
1650	[number]	k4	1650
–	–	k?	–
1550	[number]	k4	1550
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Staré	Staré	k2eAgFnSc2d1	Staré
říše	říš	k1gFnSc2	říš
se	se	k3xPyFc4	se
po	po	k7c6	po
období	období	k1gNnSc6	období
rozpadu	rozpad	k1gInSc2	rozpad
dosavadního	dosavadní	k2eAgNnSc2d1	dosavadní
zřízení	zřízení	k1gNnSc2	zřízení
podařilo	podařit	k5eAaPmAgNnS	podařit
vládcům	vládce	k1gMnPc3	vládce
města	město	k1gNnSc2	město
Vesetu	Veset	k2eAgFnSc4d1	Veset
(	(	kIx(	(
<g/>
Théby	Théby	k1gFnPc4	Théby
<g/>
)	)	kIx)	)
v	v	k7c6	v
Horním	horní	k2eAgInSc6d1	horní
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
Mentuhotepovi	Mentuhotepův	k2eAgMnPc1d1	Mentuhotepův
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
říši	říše	k1gFnSc4	říše
znovu	znovu	k6eAd1	znovu
sjednotit	sjednotit	k5eAaPmF	sjednotit
a	a	k8xC	a
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
XI	XI	kA	XI
<g/>
.	.	kIx.	.
dynastie	dynastie	k1gFnSc1	dynastie
začíná	začínat	k5eAaImIp3nS	začínat
Střední	střední	k2eAgFnSc1d1	střední
říše	říše	k1gFnSc1	říše
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1650	[number]	k4	1650
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
vpadli	vpadnout	k5eAaPmAgMnP	vpadnout
do	do	k7c2	do
Egypta	Egypt	k1gInSc2	Egypt
z	z	k7c2	z
Asie	Asie	k1gFnSc2	Asie
semitské	semitský	k2eAgInPc1d1	semitský
kmeny	kmen	k1gInPc1	kmen
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
později	pozdě	k6eAd2	pozdě
nazvány	nazván	k2eAgFnPc1d1	nazvána
Hyksósové	Hyksósová	k1gFnPc1	Hyksósová
<g/>
.	.	kIx.	.
</s>
<s>
Přinesly	přinést	k5eAaPmAgInP	přinést
s	s	k7c7	s
sebou	se	k3xPyFc7	se
nový	nový	k2eAgInSc4d1	nový
typ	typ	k1gInSc4	typ
luku	luk	k1gInSc2	luk
<g/>
,	,	kIx,	,
nové	nový	k2eAgInPc1d1	nový
typy	typ	k1gInPc1	typ
bronzových	bronzový	k2eAgFnPc2d1	bronzová
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
chov	chov	k1gInSc1	chov
koní	kůň	k1gMnPc2	kůň
a	a	k8xC	a
užití	užití	k1gNnSc2	užití
válečných	válečný	k2eAgInPc2d1	válečný
vozů	vůz	k1gInPc2	vůz
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
změnil	změnit	k5eAaPmAgMnS	změnit
způsob	způsob	k1gInSc4	způsob
vedení	vedení	k1gNnSc2	vedení
válečných	válečný	k2eAgInPc2d1	válečný
bojů	boj	k1gInPc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
říše	říše	k1gFnSc1	říše
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
až	až	k9	až
20	[number]	k4	20
<g/>
.	.	kIx.	.
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
;	;	kIx,	;
1550	[number]	k4	1550
–	–	k?	–
1069	[number]	k4	1069
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Třetí	třetí	k4xOgNnSc1	třetí
přechodné	přechodný	k2eAgNnSc1d1	přechodné
období	období	k1gNnSc1	období
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
až	až	k9	až
25	[number]	k4	25
<g/>
.	.	kIx.	.
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
;	;	kIx,	;
1069	[number]	k4	1069
–	–	k?	–
664	[number]	k4	664
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Pozdní	pozdní	k2eAgNnPc1d1	pozdní
období	období	k1gNnPc1	období
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
až	až	k9	až
31	[number]	k4	31
<g/>
.	.	kIx.	.
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
;	;	kIx,	;
664	[number]	k4	664
–	–	k?	–
332	[number]	k4	332
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Faraonu	faraon	k1gInSc2	faraon
Ahmose	Ahmosa	k1gFnSc3	Ahmosa
I.	I.	kA	I.
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
Egypt	Egypt	k1gInSc4	Egypt
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1550	[number]	k4	1550
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
znovu	znovu	k6eAd1	znovu
sjednotit	sjednotit	k5eAaPmF	sjednotit
a	a	k8xC	a
založil	založit	k5eAaPmAgMnS	založit
tak	tak	k9	tak
XVIII	XVIII	kA	XVIII
<g/>
.	.	kIx.	.
dynastii	dynastie	k1gFnSc4	dynastie
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
panovníků	panovník	k1gMnPc2	panovník
této	tento	k3xDgFnSc2	tento
dynastie	dynastie	k1gFnSc2	dynastie
se	se	k3xPyFc4	se
Egyptská	egyptský	k2eAgFnSc1d1	egyptská
říše	říše	k1gFnSc1	říše
rozkládala	rozkládat	k5eAaImAgFnS	rozkládat
až	až	k9	až
k	k	k7c3	k
Nubii	Nubie	k1gFnSc3	Nubie
a	a	k8xC	a
k	k	k7c3	k
údolí	údolí	k1gNnSc3	údolí
Eufratu	Eufrat	k1gInSc2	Eufrat
<g/>
.	.	kIx.	.
</s>
<s>
Panovník	panovník	k1gMnSc1	panovník
Amenhotep	Amenhotep	k1gMnSc1	Amenhotep
IV	IV	kA	IV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1364	[number]	k4	1364
–	–	k?	–
<g/>
1347	[number]	k4	1347
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
manželkou	manželka	k1gFnSc7	manželka
byla	být	k5eAaImAgFnS	být
Nefertiti	Nefertiti	k1gFnSc1	Nefertiti
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
změnil	změnit	k5eAaPmAgMnS	změnit
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
na	na	k7c4	na
Achnaton	Achnaton	k1gInSc4	Achnaton
a	a	k8xC	a
provedl	provést	k5eAaPmAgInS	provést
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
reformu	reforma	k1gFnSc4	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
byl	být	k5eAaImAgMnS	být
jediným	jediný	k2eAgMnSc7d1	jediný
uctívaným	uctívaný	k2eAgMnSc7d1	uctívaný
bohem	bůh	k1gMnSc7	bůh
Aton	Atona	k1gFnPc2	Atona
<g/>
,	,	kIx,	,
bůh	bůh	k1gMnSc1	bůh
slunce	slunce	k1gNnSc2	slunce
(	(	kIx(	(
<g/>
před	před	k7c7	před
reformou	reforma	k1gFnSc7	reforma
Amon	Amona	k1gFnPc2	Amona
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
nástupce	nástupce	k1gMnSc1	nástupce
Tutanchamon	Tutanchamon	k1gMnSc1	Tutanchamon
tento	tento	k3xDgInSc4	tento
kult	kult	k1gInSc4	kult
zrušil	zrušit	k5eAaPmAgMnS	zrušit
a	a	k8xC	a
jméno	jméno	k1gNnSc4	jméno
Achnatona	Achnaton	k1gMnSc2	Achnaton
bylo	být	k5eAaImAgNnS	být
ze	z	k7c2	z
seznamu	seznam	k1gInSc2	seznam
králů	král	k1gMnPc2	král
vymazáno	vymazán	k2eAgNnSc1d1	vymazáno
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Ramesse	Ramesse	k1gFnSc2	Ramesse
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1290	[number]	k4	1290
–	–	k?	–
<g/>
1224	[number]	k4	1224
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
zažila	zažít	k5eAaPmAgFnS	zažít
Nová	nový	k2eAgFnSc1d1	nová
říše	říše	k1gFnSc1	říše
ještě	ještě	k9	ještě
jednou	jednou	k6eAd1	jednou
dobu	doba	k1gFnSc4	doba
rozkvětu	rozkvět	k1gInSc2	rozkvět
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Ramesse	Ramesse	k1gFnSc2	Ramesse
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1184	[number]	k4	1184
–	–	k?	–
1153	[number]	k4	1153
<g/>
)	)	kIx)	)
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rychlému	rychlý	k2eAgInSc3d1	rychlý
úpadu	úpad	k1gInSc3	úpad
Egypta	Egypt	k1gInSc2	Egypt
a	a	k8xC	a
nepokojům	nepokoj	k1gInPc3	nepokoj
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
toho	ten	k3xDgNnSc2	ten
byl	být	k5eAaImAgInS	být
vpád	vpád	k1gInSc1	vpád
Libyjců	Libyjec	k1gMnPc2	Libyjec
a	a	k8xC	a
mořských	mořský	k2eAgInPc2d1	mořský
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Zakladatelem	zakladatel	k1gMnSc7	zakladatel
XXII	XXII	kA	XXII
<g/>
.	.	kIx.	.
dynastie	dynastie	k1gFnSc2	dynastie
byl	být	k5eAaImAgMnS	být
Libyjec	Libyjec	k1gMnSc1	Libyjec
jménem	jméno	k1gNnSc7	jméno
Šešonk	Šešonka	k1gFnPc2	Šešonka
I.	I.	kA	I.
<g/>
,	,	kIx,	,
XXV	XXV	kA	XXV
<g/>
.	.	kIx.	.
dynastii	dynastie	k1gFnSc4	dynastie
založili	založit	k5eAaPmAgMnP	založit
králové	král	k1gMnPc1	král
Núbie	Núbie	k1gFnSc2	Núbie
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Ptolemaiovský	Ptolemaiovský	k2eAgInSc4d1	Ptolemaiovský
Egypt	Egypt	k1gInSc4	Egypt
a	a	k8xC	a
Aegyptus	Aegyptus	k1gInSc4	Aegyptus
<g/>
.	.	kIx.	.
</s>
<s>
Makedonské	makedonský	k2eAgNnSc1d1	makedonské
období	období	k1gNnSc1	období
(	(	kIx(	(
<g/>
332	[number]	k4	332
–	–	k?	–
310	[number]	k4	310
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Ptolemaiovský	Ptolemaiovský	k2eAgInSc1d1	Ptolemaiovský
Egypt	Egypt	k1gInSc1	Egypt
(	(	kIx(	(
<g/>
305	[number]	k4	305
–	–	k?	–
30	[number]	k4	30
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
'	'	kIx"	'
V	v	k7c6	v
roce	rok	k1gInSc6	rok
525	[number]	k4	525
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
Egypta	Egypt	k1gInSc2	Egypt
poprvé	poprvé	k6eAd1	poprvé
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
Peršané	Peršan	k1gMnPc1	Peršan
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
naposled	naposled	k6eAd1	naposled
osamostatnil	osamostatnit	k5eAaPmAgMnS	osamostatnit
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
domácích	domácí	k2eAgFnPc2d1	domácí
dynastií	dynastie	k1gFnPc2	dynastie
(	(	kIx(	(
<g/>
XXVIII	XXVIII	kA	XXVIII
<g/>
.	.	kIx.	.
–	–	k?	–
XXX	XXX	kA	XXX
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
Egypta	Egypt	k1gInSc2	Egypt
znovu	znovu	k6eAd1	znovu
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
Peršané	Peršan	k1gMnPc1	Peršan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
333	[number]	k4	333
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
porazil	porazit	k5eAaPmAgMnS	porazit
Alexandr	Alexandr	k1gMnSc1	Alexandr
Veliký	veliký	k2eAgMnSc1d1	veliký
perského	perský	k2eAgMnSc4d1	perský
krále	král	k1gMnSc4	král
Dáreia	Dáreius	k1gMnSc2	Dáreius
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
tak	tak	k9	tak
egyptské	egyptský	k2eAgNnSc4d1	egyptské
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
(	(	kIx(	(
<g/>
323	[number]	k4	323
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
vojevůdců	vojevůdce	k1gMnPc2	vojevůdce
Ptolemaios	Ptolemaios	k1gMnSc1	Ptolemaios
vládl	vládnout	k5eAaImAgMnS	vládnout
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
provincii	provincie	k1gFnSc6	provincie
a	a	k8xC	a
v	v	k7c6	v
r.	r.	kA	r.
305	[number]	k4	305
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
přijal	přijmout	k5eAaPmAgMnS	přijmout
titul	titul	k1gInSc4	titul
krále	král	k1gMnSc2	král
jako	jako	k8xS	jako
Ptolemaios	Ptolemaios	k1gMnSc1	Ptolemaios
I.	I.	kA	I.
Sótér	Sótér	k1gMnSc1	Sótér
<g/>
.	.	kIx.	.
</s>
<s>
Ptolemaiovská	Ptolemaiovský	k2eAgFnSc1d1	Ptolemaiovská
dynastie	dynastie	k1gFnSc1	dynastie
pak	pak	k6eAd1	pak
vládla	vládnout	k5eAaImAgFnS	vládnout
Egyptu	Egypt	k1gInSc3	Egypt
až	až	k8xS	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
byla	být	k5eAaImAgFnS	být
Alexandrie	Alexandrie	k1gFnSc1	Alexandrie
a	a	k8xC	a
velký	velký	k2eAgInSc1d1	velký
kulturní	kulturní	k2eAgInSc1d1	kulturní
vliv	vliv	k1gInSc1	vliv
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
měl	mít	k5eAaImAgInS	mít
helénismus	helénismus	k1gInSc1	helénismus
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
(	(	kIx(	(
<g/>
30	[number]	k4	30
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
až	až	k9	až
395	[number]	k4	395
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Byzantské	byzantský	k2eAgNnSc1d1	byzantské
období	období	k1gNnSc1	období
(	(	kIx(	(
<g/>
395	[number]	k4	395
–	–	k?	–
640	[number]	k4	640
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Kleopatry	Kleopatra	k1gFnSc2	Kleopatra
VII	VII	kA	VII
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
30	[number]	k4	30
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgFnSc1d1	poslední
panovnice	panovnice	k1gFnSc1	panovnice
dynastie	dynastie	k1gFnSc2	dynastie
Ptolemaiovců	Ptolemaiovec	k1gMnPc2	Ptolemaiovec
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Egypt	Egypt	k1gInSc4	Egypt
římskou	římský	k2eAgFnSc7d1	římská
provincií	provincie	k1gFnSc7	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dělení	dělení	k1gNnSc6	dělení
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
roku	rok	k1gInSc2	rok
395	[number]	k4	395
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
pod	pod	k7c4	pod
byzantskou	byzantský	k2eAgFnSc4d1	byzantská
vládu	vláda	k1gFnSc4	vláda
a	a	k8xC	a
ztratil	ztratit	k5eAaPmAgMnS	ztratit
dosavadní	dosavadní	k2eAgInSc4d1	dosavadní
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
<g/>
.	.	kIx.	.
</s>
<s>
Islámské	islámský	k2eAgNnSc1d1	islámské
období	období	k1gNnSc1	období
(	(	kIx(	(
<g/>
640	[number]	k4	640
–	–	k?	–
1517	[number]	k4	1517
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Osmanské	osmanský	k2eAgNnSc1d1	osmanské
období	období	k1gNnSc1	období
(	(	kIx(	(
<g/>
1517	[number]	k4	1517
–	–	k?	–
1805	[number]	k4	1805
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Kolem	kolem	k7c2	kolem
640	[number]	k4	640
dobyli	dobýt	k5eAaPmAgMnP	dobýt
islámští	islámský	k2eAgMnPc1d1	islámský
Arabové	Arab	k1gMnPc1	Arab
Nilské	nilský	k2eAgFnSc2d1	nilská
údolí	údolí	k1gNnPc4	údolí
a	a	k8xC	a
vyvraždili	vyvraždit	k5eAaPmAgMnP	vyvraždit
veškeré	veškerý	k3xTgFnPc4	veškerý
na	na	k7c4	na
odpor	odpor	k1gInSc4	odpor
se	se	k3xPyFc4	se
stavějící	stavějící	k2eAgFnSc1d1	stavějící
původní	původní	k2eAgNnSc4d1	původní
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc4	jehož
zbytky	zbytek	k1gInPc4	zbytek
jsou	být	k5eAaImIp3nP	být
dnešní	dnešní	k2eAgMnPc1d1	dnešní
Koptové	Kopt	k1gMnPc1	Kopt
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Mu	on	k3xPp3gInSc3	on
<g/>
'	'	kIx"	'
<g/>
ávijada	ávijada	k1gFnSc1	ávijada
I.	I.	kA	I.
(	(	kIx(	(
<g/>
661	[number]	k4	661
<g/>
–	–	k?	–
<g/>
750	[number]	k4	750
<g/>
)	)	kIx)	)
osídlily	osídlit	k5eAaPmAgInP	osídlit
arabské	arabský	k2eAgInPc1d1	arabský
kmeny	kmen	k1gInPc1	kmen
plodné	plodný	k2eAgFnSc2d1	plodná
planiny	planina	k1gFnSc2	planina
a	a	k8xC	a
s	s	k7c7	s
nastoupením	nastoupení	k1gNnSc7	nastoupení
Saladina	Saladina	k1gFnSc1	Saladina
<g/>
,	,	kIx,	,
zakladatele	zakladatel	k1gMnSc2	zakladatel
dynastie	dynastie	k1gFnSc2	dynastie
Ajjúbovců	Ajjúbovec	k1gMnPc2	Ajjúbovec
(	(	kIx(	(
<g/>
1171	[number]	k4	1171
<g/>
–	–	k?	–
<g/>
1249	[number]	k4	1249
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Káhira	Káhira	k1gFnSc1	Káhira
centrem	centrum	k1gNnSc7	centrum
muslimského	muslimský	k2eAgInSc2d1	muslimský
odporu	odpor	k1gInSc2	odpor
proti	proti	k7c3	proti
křížovým	křížový	k2eAgNnPc3d1	křížové
tažením	tažení	k1gNnPc3	tažení
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1250	[number]	k4	1250
povstala	povstat	k5eAaPmAgFnS	povstat
palácová	palácový	k2eAgFnSc1d1	palácová
garda	garda	k1gFnSc1	garda
a	a	k8xC	a
mamlúci	mamlúce	k1gMnPc1	mamlúce
(	(	kIx(	(
<g/>
vojenští	vojenský	k2eAgMnPc1d1	vojenský
otroci	otrok	k1gMnPc1	otrok
<g/>
)	)	kIx)	)
získali	získat	k5eAaPmAgMnP	získat
moc	moc	k6eAd1	moc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1517	[number]	k4	1517
dobyl	dobýt	k5eAaPmAgInS	dobýt
Egypt	Egypt	k1gInSc4	Egypt
turecký	turecký	k2eAgMnSc1d1	turecký
sultán	sultán	k1gMnSc1	sultán
Selim	Selim	k?	Selim
I.	I.	kA	I.
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
tak	tak	k9	tak
stal	stát	k5eAaPmAgMnS	stát
tureckou	turecký	k2eAgFnSc7d1	turecká
provincií	provincie	k1gFnSc7	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Správa	správa	k1gFnSc1	správa
Egypta	Egypt	k1gInSc2	Egypt
byla	být	k5eAaImAgFnS	být
nadále	nadále	k6eAd1	nadále
svěřena	svěřit	k5eAaPmNgFnS	svěřit
mamlúkům	mamlúek	k1gMnPc3	mamlúek
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dynastie	dynastie	k1gFnSc2	dynastie
Muhammada	Muhammada	k1gFnSc1	Muhammada
Alího	Alí	k1gMnSc2	Alí
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
Napoleonovo	Napoleonův	k2eAgNnSc1d1	Napoleonovo
egyptské	egyptský	k2eAgNnSc1d1	egyptské
tažení	tažení	k1gNnSc1	tažení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1798	[number]	k4	1798
ukončilo	ukončit	k5eAaPmAgNnS	ukončit
osmanskou	osmanský	k2eAgFnSc4d1	Osmanská
nadvládu	nadvláda	k1gFnSc4	nadvláda
nad	nad	k7c7	nad
Egyptem	Egypt	k1gInSc7	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	Napoleon	k1gMnSc1	Napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
byl	být	k5eAaImAgInS	být
sice	sice	k8xC	sice
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
donucen	donucen	k2eAgMnSc1d1	donucen
opustit	opustit	k5eAaPmF	opustit
zemi	zem	k1gFnSc4	zem
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
vítězství	vítězství	k1gNnSc2	vítězství
admirála	admirál	k1gMnSc2	admirál
Nelsona	Nelson	k1gMnSc2	Nelson
v	v	k7c6	v
námořní	námořní	k2eAgFnSc6d1	námořní
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Abukiru	Abukir	k1gInSc2	Abukir
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poté	poté	k6eAd1	poté
ovládl	ovládnout	k5eAaPmAgInS	ovládnout
Egypt	Egypt	k1gInSc4	Egypt
albánský	albánský	k2eAgMnSc1d1	albánský
důstojník	důstojník	k1gMnSc1	důstojník
tureckého	turecký	k2eAgNnSc2d1	turecké
vojska	vojsko	k1gNnSc2	vojsko
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
Muhammad	Muhammad	k1gInSc1	Muhammad
Alí	Alí	k1gFnSc2	Alí
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1801	[number]	k4	1801
dostal	dostat	k5eAaPmAgInS	dostat
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
armády	armáda	k1gFnSc2	armáda
pod	pod	k7c4	pod
svou	svůj	k3xOyFgFnSc4	svůj
kontrolu	kontrola	k1gFnSc4	kontrola
postupně	postupně	k6eAd1	postupně
celou	celý	k2eAgFnSc4d1	celá
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Zamezil	zamezit	k5eAaPmAgMnS	zamezit
britským	britský	k2eAgInPc3d1	britský
pokusům	pokus	k1gInPc3	pokus
o	o	k7c4	o
vměšování	vměšování	k1gNnSc4	vměšování
do	do	k7c2	do
egyptské	egyptský	k2eAgFnSc2d1	egyptská
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
politiky	politika	k1gFnSc2	politika
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1811	[number]	k4	1811
plně	plně	k6eAd1	plně
stabilizoval	stabilizovat	k5eAaBmAgInS	stabilizovat
situaci	situace	k1gFnSc4	situace
vyvražděním	vyvraždění	k1gNnSc7	vyvraždění
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
vojenské	vojenský	k2eAgFnSc2d1	vojenská
a	a	k8xC	a
politické	politický	k2eAgFnSc2d1	politická
elity	elita	k1gFnSc2	elita
–	–	k?	–
mamlúků	mamlúk	k1gInPc2	mamlúk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
však	však	k9	však
již	již	k6eAd1	již
stal	stát	k5eAaPmAgInS	stát
faktickým	faktický	k2eAgMnSc7d1	faktický
vládcem	vládce	k1gMnSc7	vládce
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
neměla	mít	k5eNaImAgFnS	mít
prostředky	prostředek	k1gInPc4	prostředek
na	na	k7c4	na
znovudobytí	znovudobytí	k1gNnSc4	znovudobytí
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
se	se	k3xPyFc4	se
Muhammad	Muhammad	k1gInSc1	Muhammad
Alí	Alí	k1gMnPc2	Alí
vzdal	vzdát	k5eAaPmAgInS	vzdát
trůnu	trůn	k1gInSc3	trůn
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
Ibráhíma	Ibráhí	k2eAgFnPc7d1	Ibráhí
<g/>
.	.	kIx.	.
</s>
<s>
Dynastie	dynastie	k1gFnSc1	dynastie
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
založil	založit	k5eAaPmAgMnS	založit
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
vládla	vládnout	k5eAaImAgFnS	vládnout
Egyptu	Egypt	k1gInSc3	Egypt
-	-	kIx~	-
zprvu	zprvu	k6eAd1	zprvu
pod	pod	k7c7	pod
formální	formální	k2eAgFnSc7d1	formální
osmanskou	osmanský	k2eAgFnSc7d1	Osmanská
správou	správa	k1gFnSc7	správa
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
pak	pak	k6eAd1	pak
de	de	k?	de
facto	facto	k1gNnSc1	facto
pod	pod	k7c7	pod
britskou	britský	k2eAgFnSc7d1	britská
nadvládou	nadvláda	k1gFnSc7	nadvláda
-	-	kIx~	-
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
Suezského	suezský	k2eAgInSc2d1	suezský
průplavu	průplav	k1gInSc2	průplav
pod	pod	k7c7	pod
francouzským	francouzský	k2eAgInSc7d1	francouzský
patronátem	patronát	k1gInSc7	patronát
však	však	k9	však
zemi	zem	k1gFnSc3	zem
zadlužila	zadlužit	k5eAaPmAgFnS	zadlužit
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
otevření	otevření	k1gNnSc6	otevření
průplavu	průplav	k1gInSc2	průplav
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1875	[number]	k4	1875
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
největším	veliký	k2eAgMnSc7d3	veliký
akcionářem	akcionář	k1gMnSc7	akcionář
společnosti	společnost	k1gFnSc2	společnost
provozující	provozující	k2eAgFnSc4d1	provozující
tuto	tento	k3xDgFnSc4	tento
důležitou	důležitý	k2eAgFnSc4d1	důležitá
vodní	vodní	k2eAgFnSc4d1	vodní
cestu	cesta	k1gFnSc4	cesta
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1888	[number]	k4	1888
byla	být	k5eAaImAgFnS	být
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
tzv.	tzv.	kA	tzv.
Konstantinopolská	konstantinopolský	k2eAgFnSc1d1	konstantinopolská
konvence	konvence	k1gFnSc1	konvence
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
byl	být	k5eAaImAgInS	být
Suezský	suezský	k2eAgInSc1d1	suezský
průplav	průplav	k1gInSc1	průplav
prohlášen	prohlášen	k2eAgInSc1d1	prohlášen
za	za	k7c4	za
neutrální	neutrální	k2eAgNnSc4d1	neutrální
území	území	k1gNnSc4	území
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1914	[number]	k4	1914
učinila	učinit	k5eAaPmAgFnS	učinit
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
Egypt	Egypt	k1gInSc1	Egypt
svým	svůj	k3xOyFgInSc7	svůj
protektorátem	protektorát	k1gInSc7	protektorát
<g/>
.	.	kIx.	.
</s>
<s>
Dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
chediv	chediv	k1gMnSc1	chediv
Abbás	Abbása	k1gFnPc2	Abbása
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Hilmí	Hilmí	k1gNnSc4	Hilmí
byl	být	k5eAaImAgMnS	být
sesazen	sesadit	k5eAaPmNgMnS	sesadit
a	a	k8xC	a
nahrazen	nahradit	k5eAaPmNgInS	nahradit
sultánem	sultán	k1gMnSc7	sultán
Husajnem	Husajn	k1gMnSc7	Husajn
Kámilem	Kámil	k1gMnSc7	Kámil
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Husajna	Husajn	k1gMnSc4	Husajn
Kámila	Kámil	k1gMnSc4	Kámil
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
se	s	k7c7	s
sultánem	sultán	k1gMnSc7	sultán
stal	stát	k5eAaPmAgMnS	stát
Ismáíla	Ismáíla	k1gMnSc1	Ismáíla
Ahmad	Ahmad	k1gInSc4	Ahmad
Fu	fu	k0	fu
<g/>
́	́	k?	́
<g/>
ád	ád	k?	ád
<g/>
.	.	kIx.	.
</s>
<s>
Britové	Brit	k1gMnPc1	Brit
poté	poté	k6eAd1	poté
zatížili	zatížit	k5eAaPmAgMnP	zatížit
egyptské	egyptský	k2eAgFnSc2d1	egyptská
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
nucenými	nucený	k2eAgFnPc7d1	nucená
pracemi	práce	k1gFnPc7	práce
pro	pro	k7c4	pro
budování	budování	k1gNnSc4	budování
komunikací	komunikace	k1gFnPc2	komunikace
a	a	k8xC	a
vodovodů	vodovod	k1gInPc2	vodovod
s	s	k7c7	s
velkými	velký	k2eAgFnPc7d1	velká
ztrátami	ztráta	k1gFnPc7	ztráta
na	na	k7c6	na
lidských	lidský	k2eAgInPc6d1	lidský
životech	život	k1gInPc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Omezení	omezení	k1gNnSc1	omezení
dovozu	dovoz	k1gInSc2	dovoz
zboží	zboží	k1gNnSc2	zboží
však	však	k9	však
podnítilo	podnítit	k5eAaPmAgNnS	podnítit
rozvoj	rozvoj	k1gInSc4	rozvoj
egyptského	egyptský	k2eAgInSc2d1	egyptský
lehkého	lehký	k2eAgInSc2d1	lehký
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1922	[number]	k4	1922
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
Egypt	Egypt	k1gInSc1	Egypt
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
Fuada	Fuada	k1gMnSc1	Fuada
I.	I.	kA	I.
samostatnost	samostatnost	k1gFnSc1	samostatnost
jako	jako	k8xS	jako
Egyptské	egyptský	k2eAgNnSc1d1	egyptské
království	království	k1gNnSc1	království
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
Egypt	Egypt	k1gInSc1	Egypt
stal	stát	k5eAaPmAgInS	stát
dějištěm	dějiště	k1gNnSc7	dějiště
urputných	urputný	k2eAgFnPc2d1	urputná
bitev	bitva	k1gFnPc2	bitva
britských	britský	k2eAgNnPc2d1	Britské
vojsk	vojsko	k1gNnPc2	vojsko
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
maršála	maršál	k1gMnSc2	maršál
Bernarda	Bernard	k1gMnSc2	Bernard
Montgomeryho	Montgomery	k1gMnSc2	Montgomery
s	s	k7c7	s
německou	německý	k2eAgFnSc7d1	německá
a	a	k8xC	a
italskou	italský	k2eAgFnSc7d1	italská
armádou	armáda	k1gFnSc7	armáda
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
německého	německý	k2eAgMnSc2d1	německý
generála	generál	k1gMnSc2	generál
Erwina	Erwin	k1gMnSc2	Erwin
Rommela	Rommel	k1gMnSc2	Rommel
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
zůstala	zůstat	k5eAaPmAgNnP	zůstat
britská	britský	k2eAgNnPc1d1	Britské
vojska	vojsko	k1gNnPc1	vojsko
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
státu	stát	k1gInSc2	stát
Izrael	Izrael	k1gInSc1	Izrael
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
egyptská	egyptský	k2eAgFnSc1d1	egyptská
armáda	armáda	k1gFnSc1	armáda
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
arabskými	arabský	k2eAgInPc7d1	arabský
státy	stát	k1gInPc7	stát
napadla	napadnout	k5eAaPmAgFnS	napadnout
Izrael	Izrael	k1gInSc4	Izrael
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zničit	zničit	k5eAaPmF	zničit
židovský	židovský	k2eAgInSc4d1	židovský
stát	stát	k1gInSc4	stát
a	a	k8xC	a
"	"	kIx"	"
<g/>
vyhnat	vyhnat	k5eAaPmF	vyhnat
Židy	Žid	k1gMnPc4	Žid
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
"	"	kIx"	"
při	při	k7c6	při
první	první	k4xOgFnSc6	první
arabsko-izraelské	arabskozraelský	k2eAgFnSc6d1	arabsko-izraelská
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
byla	být	k5eAaImAgFnS	být
porážka	porážka	k1gFnSc1	porážka
arabské	arabský	k2eAgFnSc2d1	arabská
koalice	koalice	k1gFnSc2	koalice
<g/>
,	,	kIx,	,
vítězství	vítězství	k1gNnSc1	vítězství
Izraele	Izrael	k1gInSc2	Izrael
a	a	k8xC	a
zabránění	zabránění	k1gNnSc4	zabránění
vzniku	vznik	k1gInSc2	vznik
palestinského	palestinský	k2eAgInSc2d1	palestinský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Egypt	Egypt	k1gInSc1	Egypt
totiž	totiž	k9	totiž
vzápětí	vzápětí	k6eAd1	vzápětí
okupoval	okupovat	k5eAaBmAgMnS	okupovat
Gazu	Gaza	k1gFnSc4	Gaza
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Zajordánsko	Zajordánsko	k1gNnSc1	Zajordánsko
Západní	západní	k2eAgFnSc2d1	západní
břeh	břeh	k1gInSc4	břeh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
Egypt	Egypt	k1gInSc1	Egypt
vypověděl	vypovědět	k5eAaPmAgInS	vypovědět
Konstantinopolskou	konstantinopolský	k2eAgFnSc4d1	konstantinopolská
konvenci	konvence	k1gFnSc4	konvence
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
souhlasila	souhlasit	k5eAaImAgFnS	souhlasit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Suezského	suezský	k2eAgInSc2d1	suezský
průplavu	průplav	k1gInSc2	průplav
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
stáhne	stáhnout	k5eAaPmIp3nS	stáhnout
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1952	[number]	k4	1952
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
převratem	převrat	k1gInSc7	převrat
svržen	svržen	k2eAgMnSc1d1	svržen
král	král	k1gMnSc1	král
Farúk	Farúka	k1gFnPc2	Farúka
I.	I.	kA	I.
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
Egyptská	egyptský	k2eAgFnSc1d1	egyptská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
byl	být	k5eAaImAgMnS	být
nejdříve	dříve	k6eAd3	dříve
generál	generál	k1gMnSc1	generál
Muhammad	Muhammad	k1gInSc1	Muhammad
Nadžíb	Nadžíb	k1gInSc4	Nadžíb
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
vůdce	vůdce	k1gMnSc1	vůdce
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
plukovník	plukovník	k1gMnSc1	plukovník
Gamál	Gamál	k1gMnSc1	Gamál
Abd	Abd	k1gMnSc1	Abd
an-Násir	an-Násir	k1gMnSc1	an-Násir
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
–	–	k?	–
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
zavedl	zavést	k5eAaPmAgInS	zavést
socialistický	socialistický	k2eAgInSc1d1	socialistický
způsob	způsob	k1gInSc1	způsob
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Zestátnění	zestátnění	k1gNnSc1	zestátnění
Suezského	suezský	k2eAgInSc2d1	suezský
kanálu	kanál	k1gInSc2	kanál
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
vojenskému	vojenský	k2eAgInSc3d1	vojenský
napadení	napadení	k1gNnPc4	napadení
Izraelem	Izrael	k1gInSc7	Izrael
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
součástí	součást	k1gFnSc7	součást
tajné	tajný	k2eAgFnSc2d1	tajná
dohody	dohoda	k1gFnSc2	dohoda
mezi	mezi	k7c7	mezi
Jeruzalémem	Jeruzalém	k1gInSc7	Jeruzalém
<g/>
,	,	kIx,	,
Londýnem	Londýn	k1gInSc7	Londýn
a	a	k8xC	a
Paříží	Paříž	k1gFnSc7	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Vzápětí	vzápětí	k6eAd1	vzápětí
vyslala	vyslat	k5eAaPmAgFnS	vyslat
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
a	a	k8xC	a
Francie	Francie	k1gFnSc1	Francie
své	svůj	k3xOyFgFnSc2	svůj
jednotky	jednotka	k1gFnSc2	jednotka
údajně	údajně	k6eAd1	údajně
pro	pro	k7c4	pro
udržení	udržení	k1gNnSc4	udržení
svobody	svoboda	k1gFnSc2	svoboda
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
plavby	plavba	k1gFnSc2	plavba
Suezským	suezský	k2eAgInSc7d1	suezský
průlivem	průliv	k1gInSc7	průliv
<g/>
.	.	kIx.	.
</s>
<s>
Vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
tzv.	tzv.	kA	tzv.
Suezská	suezský	k2eAgFnSc1d1	Suezská
krize	krize	k1gFnSc1	krize
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k8xS	co
USA	USA	kA	USA
daly	dát	k5eAaPmAgFnP	dát
ruce	ruka	k1gFnPc1	ruka
pryč	pryč	k6eAd1	pryč
od	od	k7c2	od
celé	celý	k2eAgFnSc2d1	celá
akce	akce	k1gFnSc2	akce
<g/>
,	,	kIx,	,
o	o	k7c4	o
které	který	k3yRgInPc4	který
nebyl	být	k5eNaImAgInS	být
Washington	Washington	k1gInSc1	Washington
informován	informovat	k5eAaBmNgInS	informovat
dopředu	dopředu	k6eAd1	dopředu
<g/>
,	,	kIx,	,
a	a	k8xC	a
Moskva	Moskva	k1gFnSc1	Moskva
pohrozila	pohrozit	k5eAaPmAgFnS	pohrozit
bombardováním	bombardování	k1gNnSc7	bombardování
Paříže	Paříž	k1gFnSc2	Paříž
a	a	k8xC	a
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc1	všechen
tři	tři	k4xCgFnPc1	tři
útočící	útočící	k2eAgFnPc1d1	útočící
strany	strana	k1gFnPc1	strana
se	se	k3xPyFc4	se
stáhly	stáhnout	k5eAaPmAgFnP	stáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
bylo	být	k5eAaImAgNnS	být
posílení	posílení	k1gNnSc1	posílení
egyptské	egyptský	k2eAgFnSc2d1	egyptská
pozice	pozice	k1gFnSc2	pozice
a	a	k8xC	a
zvýšení	zvýšení	k1gNnSc3	zvýšení
vlivu	vliv	k1gInSc2	vliv
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
Východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
února	únor	k1gInSc2	únor
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
diplomatickým	diplomatický	k2eAgInPc3d1	diplomatický
sporům	spor	k1gInPc3	spor
se	se	k3xPyFc4	se
Súdánem	Súdán	k1gInSc7	Súdán
o	o	k7c4	o
Halaibský	Halaibský	k2eAgInSc4d1	Halaibský
trojúhelník	trojúhelník	k1gInSc4	trojúhelník
a	a	k8xC	a
Bir	Bir	k1gFnSc4	Bir
Tawil	Tawila	k1gFnPc2	Tawila
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
Egypt	Egypt	k1gInSc1	Egypt
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Sýrií	Sýrie	k1gFnSc7	Sýrie
Sjednocenou	sjednocený	k2eAgFnSc4d1	sjednocená
arabskou	arabský	k2eAgFnSc4d1	arabská
republiku	republika	k1gFnSc4	republika
vedenou	vedený	k2eAgFnSc4d1	vedená
Násirem	Násir	k1gInSc7	Násir
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
dominoval	dominovat	k5eAaImAgInS	dominovat
Egypt	Egypt	k1gInSc1	Egypt
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
Sýrie	Sýrie	k1gFnSc1	Sýrie
svazek	svazek	k1gInSc1	svazek
opustila	opustit	k5eAaPmAgFnS	opustit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
Egypt	Egypt	k1gInSc1	Egypt
používal	používat	k5eAaImAgInS	používat
tento	tento	k3xDgInSc4	tento
název	název	k1gInSc4	název
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
vyslal	vyslat	k5eAaPmAgMnS	vyslat
Násir	Násir	k1gInSc4	Násir
armádu	armáda	k1gFnSc4	armáda
na	na	k7c4	na
Sinajský	sinajský	k2eAgInSc4d1	sinajský
poloostrov	poloostrov	k1gInSc4	poloostrov
a	a	k8xC	a
donutil	donutit	k5eAaPmAgInS	donutit
pozorovatelskou	pozorovatelský	k2eAgFnSc4d1	pozorovatelská
misi	mise	k1gFnSc4	mise
OSN	OSN	kA	OSN
se	se	k3xPyFc4	se
stáhnout	stáhnout	k5eAaPmF	stáhnout
<g/>
,	,	kIx,	,
vzápětí	vzápětí	k6eAd1	vzápětí
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
Tiranskou	tiranský	k2eAgFnSc4d1	Tiranská
úžinu	úžina	k1gFnSc4	úžina
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
Izrael	Izrael	k1gInSc4	Izrael
předtím	předtím	k6eAd1	předtím
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
za	za	k7c4	za
Casus	Casus	k1gInSc4	Casus
belli	belle	k1gFnSc4	belle
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
<g/>
června	červen	k1gInSc2	červen
zahájil	zahájit	k5eAaPmAgMnS	zahájit
Izrael	Izrael	k1gInSc4	Izrael
preventivní	preventivní	k2eAgInSc4d1	preventivní
letecký	letecký	k2eAgInSc4d1	letecký
úder	úder	k1gInSc4	úder
<g/>
,	,	kIx,	,
zničil	zničit	k5eAaPmAgInS	zničit
egyptské	egyptský	k2eAgNnSc4d1	egyptské
letectvo	letectvo	k1gNnSc4	letectvo
a	a	k8xC	a
během	během	k7c2	během
Šestidenní	šestidenní	k2eAgFnSc2d1	šestidenní
války	válka	k1gFnSc2	válka
obsadil	obsadit	k5eAaPmAgMnS	obsadit
nejen	nejen	k6eAd1	nejen
Gazu	Gaza	k1gFnSc4	Gaza
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
celý	celý	k2eAgInSc4d1	celý
Sinajský	sinajský	k2eAgInSc4d1	sinajský
poloostrov	poloostrov	k1gInSc4	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Násira	Násir	k1gInSc2	Násir
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prezidentem	prezident	k1gMnSc7	prezident
jeho	jeho	k3xOp3gMnSc7	jeho
viceprezident	viceprezident	k1gMnSc1	viceprezident
Anvar	Anvar	k1gMnSc1	Anvar
as-Sádát	as-Sádát	k1gMnSc1	as-Sádát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
Egypt	Egypt	k1gInSc4	Egypt
a	a	k8xC	a
Sýrie	Sýrie	k1gFnPc4	Sýrie
znovuvyzbrojené	znovuvyzbrojený	k2eAgFnPc4d1	znovuvyzbrojený
moderními	moderní	k2eAgInPc7d1	moderní
sovětskými	sovětský	k2eAgInPc7d1	sovětský
zbraněmi	zbraň	k1gFnPc7	zbraň
zaútočily	zaútočit	k5eAaPmAgFnP	zaútočit
za	za	k7c2	za
Jomkipurské	Jomkipurský	k2eAgFnSc2d1	Jomkipurská
války	válka	k1gFnSc2	válka
na	na	k7c4	na
Izrael	Izrael	k1gInSc4	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
prvních	první	k4xOgInPc6	první
dvou	dva	k4xCgInPc6	dva
dnech	den	k1gInPc6	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Egyptu	Egypt	k1gInSc3	Egypt
podařilo	podařit	k5eAaPmAgNnS	podařit
překročit	překročit	k5eAaPmF	překročit
Suezský	suezský	k2eAgInSc4d1	suezský
kanál	kanál	k1gInSc4	kanál
a	a	k8xC	a
postupovat	postupovat	k5eAaImF	postupovat
do	do	k7c2	do
hloubi	hloub	k1gFnSc2	hloub
Sinajského	sinajský	k2eAgInSc2d1	sinajský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
syrské	syrský	k2eAgFnPc1d1	Syrská
obrněné	obrněný	k2eAgFnPc1d1	obrněná
jednotky	jednotka	k1gFnPc1	jednotka
dobyly	dobýt	k5eAaPmAgFnP	dobýt
zpět	zpět	k6eAd1	zpět
severní	severní	k2eAgFnSc4d1	severní
polovinu	polovina	k1gFnSc4	polovina
Golanských	Golanský	k2eAgFnPc2d1	Golanský
výšin	výšina	k1gFnPc2	výšina
ztracených	ztracený	k2eAgFnPc2d1	ztracená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
zahájil	zahájit	k5eAaPmAgInS	zahájit
Izrael	Izrael	k1gInSc1	Izrael
protiofenzivu	protiofenziva	k1gFnSc4	protiofenziva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
porazil	porazit	k5eAaPmAgMnS	porazit
útočící	útočící	k2eAgFnSc2d1	útočící
arabské	arabský	k2eAgFnSc2d1	arabská
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
jednotky	jednotka	k1gFnSc2	jednotka
Ariela	Ariela	k1gFnSc1	Ariela
Šarona	Šarona	k1gFnSc1	Šarona
dokonce	dokonce	k9	dokonce
obsadila	obsadit	k5eAaPmAgFnS	obsadit
Port	port	k1gInSc4	port
Said	Saida	k1gFnPc2	Saida
na	na	k7c6	na
africkém	africký	k2eAgInSc6d1	africký
břehu	břeh	k1gInSc6	břeh
Suezského	suezský	k2eAgInSc2d1	suezský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
odvážně	odvážně	k6eAd1	odvážně
zahájil	zahájit	k5eAaPmAgMnS	zahájit
Anvar	Anvar	k1gMnSc1	Anvar
Sadát	Sadát	k1gMnSc1	Sadát
mírové	mírový	k2eAgNnSc4d1	Mírové
jednání	jednání	k1gNnSc4	jednání
s	s	k7c7	s
Izraelem	Izrael	k1gInSc7	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
podpisu	podpis	k1gInSc3	podpis
mírové	mírový	k2eAgFnSc2d1	mírová
smlouvy	smlouva	k1gFnSc2	smlouva
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
a	a	k8xC	a
k	k	k7c3	k
odchodu	odchod	k1gInSc3	odchod
izraelského	izraelský	k2eAgNnSc2d1	izraelské
vojska	vojsko	k1gNnSc2	vojsko
ze	z	k7c2	z
Sinajského	sinajský	k2eAgInSc2d1	sinajský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
však	však	k9	však
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
odpor	odpor	k1gInSc4	odpor
islamistů	islamista	k1gMnPc2	islamista
z	z	k7c2	z
Muslimského	muslimský	k2eAgNnSc2d1	muslimské
bratrstva	bratrstvo	k1gNnSc2	bratrstvo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
byl	být	k5eAaImAgMnS	být
Sadát	Sadát	k1gMnSc1	Sadát
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
nástupcem	nástupce	k1gMnSc7	nástupce
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Muhammad	Muhammad	k1gInSc1	Muhammad
Husní	Husní	k1gMnSc1	Husní
Mubarak	Mubarak	k1gMnSc1	Mubarak
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1997	[number]	k4	1997
bylo	být	k5eAaImAgNnS	být
62	[number]	k4	62
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
turistů	turist	k1gMnPc2	turist
<g/>
,	,	kIx,	,
zmasakrováno	zmasakrován	k2eAgNnSc1d1	zmasakrován
poblíž	poblíž	k7c2	poblíž
Luxoru	Luxor	k1gInSc2	Luxor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
se	se	k3xPyFc4	se
Egypt	Egypt	k1gInSc1	Egypt
de	de	k?	de
facto	facto	k1gNnSc1	facto
ujal	ujmout	k5eAaPmAgInS	ujmout
správy	správa	k1gFnPc4	správa
nad	nad	k7c7	nad
Halaibským	Halaibský	k2eAgInSc7d1	Halaibský
trojúhelníkem	trojúhelník	k1gInSc7	trojúhelník
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Egyptská	egyptský	k2eAgFnSc1d1	egyptská
revoluce	revoluce	k1gFnSc1	revoluce
2011	[number]	k4	2011
a	a	k8xC	a
Vojenský	vojenský	k2eAgInSc1d1	vojenský
převrat	převrat	k1gInSc1	převrat
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
až	až	k9	až
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
probíhaly	probíhat	k5eAaImAgFnP	probíhat
nebo	nebo	k8xC	nebo
probíhají	probíhat	k5eAaImIp3nP	probíhat
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
arabských	arabský	k2eAgInPc2d1	arabský
států	stát	k1gInPc2	stát
protesty	protest	k1gInPc1	protest
<g/>
,	,	kIx,	,
nepokoje	nepokoj	k1gInPc1	nepokoj
<g/>
,	,	kIx,	,
povstání	povstání	k1gNnPc1	povstání
a	a	k8xC	a
revoluce	revoluce	k1gFnPc1	revoluce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
nazývány	nazývat	k5eAaImNgFnP	nazývat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
arabské	arabský	k2eAgNnSc1d1	arabské
jaro	jaro	k1gNnSc1	jaro
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
ledna	leden	k1gInSc2	leden
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
egyptský	egyptský	k2eAgMnSc1d1	egyptský
prezident	prezident	k1gMnSc1	prezident
Husní	Husní	k1gMnSc1	Husní
Mubarak	Mubarak	k1gMnSc1	Mubarak
poprvé	poprvé	k6eAd1	poprvé
po	po	k7c6	po
čtyřech	čtyři	k4xCgInPc6	čtyři
dnech	den	k1gInPc6	den
nepokojů	nepokoj	k1gInPc2	nepokoj
a	a	k8xC	a
násilností	násilnost	k1gFnPc2	násilnost
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
promluvil	promluvit	k5eAaPmAgMnS	promluvit
o	o	k7c6	o
událostech	událost	k1gFnPc6	událost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
si	se	k3xPyFc3	se
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
vyžádaly	vyžádat	k5eAaPmAgInP	vyžádat
nejméně	málo	k6eAd3	málo
18	[number]	k4	18
mrtvých	mrtvý	k1gMnPc2	mrtvý
a	a	k8xC	a
přes	přes	k7c4	přes
1000	[number]	k4	1000
zraněných	zraněný	k1gMnPc2	zraněný
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
projevu	projev	k1gInSc6	projev
oznámil	oznámit	k5eAaPmAgMnS	oznámit
občanům	občan	k1gMnPc3	občan
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
vládu	vláda	k1gFnSc4	vláda
k	k	k7c3	k
rezignaci	rezignace	k1gFnSc3	rezignace
<g/>
,	,	kIx,	,
a	a	k8xC	a
slíbil	slíbit	k5eAaPmAgMnS	slíbit
demokratické	demokratický	k2eAgFnPc4d1	demokratická
reformy	reforma	k1gFnPc4	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
agentury	agentura	k1gFnSc2	agentura
AP	ap	kA	ap
Mubarak	Mubarak	k1gMnSc1	Mubarak
ujišťoval	ujišťovat	k5eAaImAgMnS	ujišťovat
občany	občan	k1gMnPc4	občan
<g/>
,	,	kIx,	,
že	že	k8xS	že
prosadí	prosadit	k5eAaPmIp3nS	prosadit
sociální	sociální	k2eAgInSc1d1	sociální
<g/>
,	,	kIx,	,
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
a	a	k8xC	a
politické	politický	k2eAgFnSc2d1	politická
reformy	reforma	k1gFnSc2	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Protivládní	protivládní	k2eAgFnPc1d1	protivládní
demonstrace	demonstrace	k1gFnPc1	demonstrace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
ochromily	ochromit	k5eAaPmAgFnP	ochromit
celou	celý	k2eAgFnSc4d1	celá
zem	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
součást	součást	k1gFnSc4	součást
spiknutí	spiknutí	k1gNnSc2	spiknutí
k	k	k7c3	k
destabilizaci	destabilizace	k1gFnSc3	destabilizace
Egypta	Egypt	k1gInSc2	Egypt
a	a	k8xC	a
zničení	zničení	k1gNnSc2	zničení
legitimity	legitimita	k1gFnSc2	legitimita
jeho	jeho	k3xOp3gInSc3	jeho
režimu	režim	k1gInSc3	režim
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
země	země	k1gFnSc1	země
byla	být	k5eAaImAgFnS	být
také	také	k9	také
odpojena	odpojit	k5eAaPmNgFnS	odpojit
od	od	k7c2	od
internetu	internet	k1gInSc2	internet
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
Káhiře	Káhira	k1gFnSc6	Káhira
a	a	k8xC	a
ostatních	ostatní	k2eAgNnPc6d1	ostatní
velkoměstech	velkoměsto	k1gNnPc6	velkoměsto
ještě	ještě	k6eAd1	ještě
více	hodně	k6eAd2	hodně
vyhrotila	vyhrotit	k5eAaPmAgFnS	vyhrotit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
komisařky	komisařka	k1gFnSc2	komisařka
OSN	OSN	kA	OSN
si	se	k3xPyFc3	se
nepokoje	nepokoj	k1gInSc2	nepokoj
vyžádaly	vyžádat	k5eAaPmAgFnP	vyžádat
až	až	k9	až
300	[number]	k4	300
životů	život	k1gInPc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Odpůrci	odpůrce	k1gMnPc1	odpůrce
prezidenta	prezident	k1gMnSc2	prezident
Husního	Husní	k1gMnSc2	Husní
Mubaraka	Mubarak	k1gMnSc2	Mubarak
se	se	k3xPyFc4	se
31.1	[number]	k4	31.1
<g/>
.2011	.2011	k4	.2011
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
vyhlásit	vyhlásit	k5eAaPmF	vyhlásit
časově	časově	k6eAd1	časově
neomezenou	omezený	k2eNgFnSc4d1	neomezená
generální	generální	k2eAgFnSc4d1	generální
stávku	stávka	k1gFnSc4	stávka
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
den	den	k1gInSc1	den
ji	on	k3xPp3gFnSc4	on
měl	mít	k5eAaImAgInS	mít
doprovodit	doprovodit	k5eAaPmF	doprovodit
"	"	kIx"	"
<g/>
miliónový	miliónový	k2eAgInSc4d1	miliónový
pochod	pochod	k1gInSc4	pochod
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2011	[number]	k4	2011
prezident	prezident	k1gMnSc1	prezident
Mubarak	Mubarak	k1gMnSc1	Mubarak
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
svých	svůj	k3xOyFgNnPc2	svůj
slov	slovo	k1gNnPc2	slovo
chce	chtít	k5eAaImIp3nS	chtít
dožít	dožít	k5eAaPmF	dožít
v	v	k7c6	v
letovisku	letovisko	k1gNnSc6	letovisko
Šarm	šarm	k1gInSc4	šarm
aš-Šajch	aš-Šajcha	k1gFnPc2	aš-Šajcha
<g/>
.	.	kIx.	.
</s>
<s>
Moc	moc	k6eAd1	moc
převzala	převzít	k5eAaPmAgFnS	převzít
armáda	armáda	k1gFnSc1	armáda
(	(	kIx(	(
<g/>
vojenská	vojenský	k2eAgFnSc1d1	vojenská
junta	junta	k1gFnSc1	junta
<g/>
)	)	kIx)	)
a	a	k8xC	a
faktickou	faktický	k2eAgFnSc7d1	faktická
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
předseda	předseda	k1gMnSc1	předseda
Nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
vojenské	vojenský	k2eAgFnSc2d1	vojenská
rady	rada	k1gFnSc2	rada
Muhammad	Muhammad	k1gInSc1	Muhammad
Hosejn	Hosejn	k1gNnSc4	Hosejn
Tantaví	Tantavý	k2eAgMnPc1d1	Tantavý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2012	[number]	k4	2012
se	se	k3xPyFc4	se
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
konaly	konat	k5eAaImAgFnP	konat
prezidentské	prezidentský	k2eAgFnPc1d1	prezidentská
volby	volba	k1gFnPc1	volba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
islamista	islamista	k1gMnSc1	islamista
Muhammad	Muhammad	k1gInSc4	Muhammad
Mursí	Mursí	k1gNnSc2	Mursí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2012	[number]	k4	2012
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
referendum	referendum	k1gNnSc1	referendum
o	o	k7c6	o
nové	nový	k2eAgFnSc6d1	nová
ústavě	ústava	k1gFnSc6	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
kritizována	kritizovat	k5eAaImNgFnS	kritizovat
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
kritiků	kritik	k1gMnPc2	kritik
mohla	moct	k5eAaImAgFnS	moct
umožnit	umožnit	k5eAaPmF	umožnit
vznik	vznik	k1gInSc4	vznik
islamistického	islamistický	k2eAgInSc2d1	islamistický
státu	stát	k1gInSc2	stát
a	a	k8xC	a
nezaručovala	zaručovat	k5eNaImAgFnS	zaručovat
stejná	stejná	k1gFnSc1	stejná
práva	právo	k1gNnSc2	právo
všem	všecek	k3xTgMnPc3	všecek
obyvatelům	obyvatel	k1gMnPc3	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Referendum	referendum	k1gNnSc1	referendum
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
termínech	termín	k1gInPc6	termín
<g/>
,	,	kIx,	,
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
volila	volit	k5eAaImAgFnS	volit
polovina	polovina	k1gFnSc1	polovina
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
oficiálních	oficiální	k2eAgInPc2d1	oficiální
výsledků	výsledek	k1gInPc2	výsledek
byla	být	k5eAaImAgFnS	být
ústava	ústava	k1gFnSc1	ústava
přijata	přijat	k2eAgFnSc1d1	přijata
-	-	kIx~	-
pro	pro	k7c4	pro
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
63,8	[number]	k4	63,8
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Volební	volební	k2eAgFnSc4d1	volební
účast	účast	k1gFnSc4	účast
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
velice	velice	k6eAd1	velice
nízká	nízký	k2eAgFnSc1d1	nízká
-	-	kIx~	-
32,9	[number]	k4	32,9
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Mursí	Mursí	k1gNnSc1	Mursí
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
roce	rok	k1gInSc6	rok
vlády	vláda	k1gFnSc2	vláda
sesazen	sesazen	k2eAgInSc4d1	sesazen
armádním	armádní	k2eAgInSc7d1	armádní
pučem	puč	k1gInSc7	puč
<g/>
.	.	kIx.	.
</s>
<s>
Egyptská	egyptský	k2eAgFnSc1d1	egyptská
armáda	armáda	k1gFnSc1	armáda
při	při	k7c6	při
potlačování	potlačování	k1gNnSc6	potlačování
protestů	protest	k1gInPc2	protest
vůči	vůči	k7c3	vůči
puči	puč	k1gInSc3	puč
zabila	zabít	k5eAaPmAgFnS	zabít
nejméně	málo	k6eAd3	málo
817	[number]	k4	817
stoupenců	stoupenec	k1gMnPc2	stoupenec
vlády	vláda	k1gFnSc2	vláda
prezidenta	prezident	k1gMnSc2	prezident
Mursího	Mursí	k1gMnSc2	Mursí
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
údajů	údaj	k1gInPc2	údaj
i	i	k9	i
přes	přes	k7c4	přes
1	[number]	k4	1
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Muslimské	muslimský	k2eAgNnSc1d1	muslimské
bratrstvo	bratrstvo	k1gNnSc1	bratrstvo
bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
mimo	mimo	k7c4	mimo
zákon	zákon	k1gInSc4	zákon
a	a	k8xC	a
v	v	k7c6	v
následných	následný	k2eAgFnPc6d1	následná
volbách	volba	k1gFnPc6	volba
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
bývalý	bývalý	k2eAgMnSc1d1	bývalý
náčelník	náčelník	k1gMnSc1	náčelník
generálního	generální	k2eAgInSc2d1	generální
štábu	štáb	k1gInSc2	štáb
egyptské	egyptský	k2eAgFnSc2d1	egyptská
armády	armáda	k1gFnSc2	armáda
Abd	Abd	k1gMnSc1	Abd
al-Fattáh	al-Fattáh	k1gMnSc1	al-Fattáh
as-Sísí	as-Sísit	k5eAaPmIp3nS	as-Sísit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
as-Sísího	as-Sísí	k2eAgInSc2d1	as-Sísí
nástupu	nástup	k1gInSc2	nástup
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
čelí	čelit	k5eAaImIp3nP	čelit
společnost	společnost	k1gFnSc4	společnost
ostrým	ostrý	k2eAgFnPc3d1	ostrá
represím	represe	k1gFnPc3	represe
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
prohlubování	prohlubování	k1gNnSc3	prohlubování
krize	krize	k1gFnSc2	krize
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Systematicky	systematicky	k6eAd1	systematicky
jsou	být	k5eAaImIp3nP	být
omezovány	omezován	k2eAgFnPc4d1	omezována
svobody	svoboda	k1gFnPc4	svoboda
shromažďování	shromažďování	k1gNnSc2	shromažďování
a	a	k8xC	a
svobodného	svobodný	k2eAgNnSc2d1	svobodné
vyjádření	vyjádření	k1gNnSc2	vyjádření
protestu	protest	k1gInSc2	protest
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
vojenského	vojenský	k2eAgInSc2d1	vojenský
převratu	převrat	k1gInSc2	převrat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
bylo	být	k5eAaImAgNnS	být
zatčeno	zatknout	k5eAaPmNgNnS	zatknout
a	a	k8xC	a
uvězněno	uvěznit	k5eAaPmNgNnS	uvěznit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
16	[number]	k4	16
000	[number]	k4	000
odpůrců	odpůrce	k1gMnPc2	odpůrce
režimu	režim	k1gInSc2	režim
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
některých	některý	k3yIgInPc2	některý
odhadů	odhad	k1gInPc2	odhad
až	až	k9	až
40	[number]	k4	40
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stovky	stovka	k1gFnPc1	stovka
členů	člen	k1gMnPc2	člen
Muslimského	muslimský	k2eAgNnSc2d1	muslimské
bratrstva	bratrstvo	k1gNnSc2	bratrstvo
byly	být	k5eAaImAgFnP	být
odsouzeny	odsoudit	k5eAaPmNgFnP	odsoudit
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2015	[number]	k4	2015
byl	být	k5eAaImAgMnS	být
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
odsouzen	odsouzet	k5eAaImNgInS	odsouzet
i	i	k9	i
Muhammad	Muhammad	k1gInSc1	Muhammad
Mursí	Murse	k1gFnPc2	Murse
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Egypt	Egypt	k1gInSc1	Egypt
je	být	k5eAaImIp3nS	být
stát	stát	k1gInSc4	stát
ležící	ležící	k2eAgInSc4d1	ležící
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
rozloha	rozloha	k1gFnSc1	rozloha
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
002	[number]	k4	002
450	[number]	k4	450
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Fyzicky	fyzicky	k6eAd1	fyzicky
tvoří	tvořit	k5eAaImIp3nS	tvořit
Egypt	Egypt	k1gInSc1	Egypt
východní	východní	k2eAgFnSc4d1	východní
část	část	k1gFnSc4	část
Sahary	Sahara	k1gFnSc2	Sahara
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tvořen	tvořen	k2eAgInSc1d1	tvořen
vysoko	vysoko	k6eAd1	vysoko
položenou	položený	k2eAgFnSc7d1	položená
tabulí	tabule	k1gFnSc7	tabule
<g/>
,	,	kIx,	,
rozdělenou	rozdělená	k1gFnSc4	rozdělená
údolím	údolí	k1gNnSc7	údolí
Nilu	Nil	k1gInSc2	Nil
na	na	k7c4	na
západní	západní	k2eAgFnSc4d1	západní
část	část	k1gFnSc4	část
<g/>
,	,	kIx,	,
tvořenou	tvořený	k2eAgFnSc7d1	tvořená
Libyjskou	libyjský	k2eAgFnSc7d1	Libyjská
pouští	poušť	k1gFnSc7	poušť
a	a	k8xC	a
východní	východní	k2eAgFnSc1d1	východní
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
tvořenou	tvořený	k2eAgFnSc7d1	tvořená
Arabskou	arabský	k2eAgFnSc7d1	arabská
pouští	poušť	k1gFnSc7	poušť
<g/>
.	.	kIx.	.
</s>
<s>
Podložím	podloží	k1gNnSc7	podloží
tabule	tabule	k1gFnSc2	tabule
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
rula	rula	k1gFnSc1	rula
a	a	k8xC	a
žula	žula	k1gFnSc1	žula
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
Libyjské	libyjský	k2eAgFnSc2d1	Libyjská
pouště	poušť	k1gFnSc2	poušť
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
náhorní	náhorní	k2eAgFnSc1d1	náhorní
plošina	plošina	k1gFnSc1	plošina
Džilf	Džilf	k1gMnSc1	Džilf
al-Kabír	al-Kabír	k1gMnSc1	al-Kabír
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
výškou	výška	k1gFnSc7	výška
1000	[number]	k4	1000
m	m	kA	m
nejvyšším	vysoký	k2eAgNnSc7d3	nejvyšší
místem	místo	k1gNnSc7	místo
této	tento	k3xDgFnSc2	tento
pouště	poušť	k1gFnSc2	poušť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Libyjské	libyjský	k2eAgFnSc6d1	Libyjská
poušti	poušť	k1gFnSc6	poušť
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
nachází	nacházet	k5eAaImIp3nS	nacházet
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
proláklin	proláklina	k1gFnPc2	proláklina
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c4	v
největší	veliký	k2eAgFnSc4d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
,	,	kIx,	,
Katarské	katarský	k2eAgFnSc3d1	katarská
proláklině	proláklina	k1gFnSc3	proláklina
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
137	[number]	k4	137
metrů	metr	k1gInPc2	metr
pod	pod	k7c7	pod
hladinou	hladina	k1gFnSc7	hladina
moře	moře	k1gNnSc2	moře
nachází	nacházet	k5eAaImIp3nS	nacházet
nejnižší	nízký	k2eAgInSc4d3	nejnižší
bod	bod	k1gInSc4	bod
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgFnSc1d1	východní
část	část	k1gFnSc1	část
Egypta	Egypt	k1gInSc2	Egypt
tvoří	tvořit	k5eAaImIp3nS	tvořit
Arabská	arabský	k2eAgFnSc1d1	arabská
poušť	poušť	k1gFnSc1	poušť
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
od	od	k7c2	od
nilského	nilský	k2eAgNnSc2d1	nilské
údolí	údolí	k1gNnSc2	údolí
až	až	k9	až
k	k	k7c3	k
Rudému	rudý	k2eAgNnSc3d1	Rudé
moři	moře	k1gNnSc3	moře
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Libyjské	libyjský	k2eAgFnSc2d1	Libyjská
pouště	poušť	k1gFnSc2	poušť
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
hornatá	hornatý	k2eAgFnSc1d1	hornatá
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
oblast	oblast	k1gFnSc4	oblast
jsou	být	k5eAaImIp3nP	být
charakteristická	charakteristický	k2eAgNnPc1d1	charakteristické
suchá	suchý	k2eAgNnPc1d1	suché
údolí	údolí	k1gNnPc1	údolí
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
vádí	vádí	k1gNnPc1	vádí
<g/>
.	.	kIx.	.
</s>
<s>
Pomyslný	pomyslný	k2eAgInSc1d1	pomyslný
střed	střed	k1gInSc1	střed
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
tvoří	tvořit	k5eAaImIp3nS	tvořit
řeka	řeka	k1gFnSc1	řeka
Nil	Nil	k1gInSc1	Nil
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
protéká	protékat	k5eAaImIp3nS	protékat
Egyptem	Egypt	k1gInSc7	Egypt
od	od	k7c2	od
jižních	jižní	k2eAgFnPc2d1	jižní
hranic	hranice	k1gFnPc2	hranice
se	s	k7c7	s
Súdánem	Súdán	k1gInSc7	Súdán
až	až	k9	až
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
tato	tento	k3xDgFnSc1	tento
řeka	řeka	k1gFnSc1	řeka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
na	na	k7c6	na
území	území	k1gNnSc6	území
Egypta	Egypt	k1gInSc2	Egypt
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
přibližné	přibližný	k2eAgFnPc4d1	přibližná
délky	délka	k1gFnPc4	délka
1	[number]	k4	1
600	[number]	k4	600
km	km	kA	km
<g/>
,	,	kIx,	,
dala	dát	k5eAaPmAgFnS	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
egyptské	egyptský	k2eAgFnSc3d1	egyptská
civilizaci	civilizace	k1gFnSc3	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
údolí	údolí	k1gNnSc6	údolí
a	a	k8xC	a
deltě	delta	k1gFnSc6	delta
99	[number]	k4	99
<g/>
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
tak	tak	k9	tak
využívá	využívat	k5eAaImIp3nS	využívat
pouze	pouze	k6eAd1	pouze
5,5	[number]	k4	5,5
<g/>
%	%	kIx~	%
rozlohy	rozloha	k1gFnPc1	rozloha
své	svůj	k3xOyFgFnSc2	svůj
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Egyptu	Egypt	k1gInSc3	Egypt
rovněž	rovněž	k9	rovněž
patří	patřit	k5eAaImIp3nS	patřit
Sinajský	sinajský	k2eAgInSc1d1	sinajský
poloostrov	poloostrov	k1gInSc1	poloostrov
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
ohraničen	ohraničen	k2eAgInSc4d1	ohraničen
Suezským	suezský	k2eAgInSc7d1	suezský
a	a	k8xC	a
Akabským	Akabský	k2eAgInSc7d1	Akabský
zálivem	záliv	k1gInSc7	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Sinajský	sinajský	k2eAgInSc1d1	sinajský
poloostrov	poloostrov	k1gInSc1	poloostrov
patří	patřit	k5eAaImIp3nS	patřit
geograficky	geograficky	k6eAd1	geograficky
již	již	k6eAd1	již
k	k	k7c3	k
Asii	Asie	k1gFnSc3	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
hornatá	hornatý	k2eAgFnSc1d1	hornatá
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
Hora	hora	k1gFnSc1	hora
sv.	sv.	kA	sv.
Kateřiny	Kateřina	k1gFnSc2	Kateřina
(	(	kIx(	(
<g/>
Gebel	Gebel	k1gInSc1	Gebel
Katherína	Katherín	k1gInSc2	Katherín
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
výšky	výška	k1gFnSc2	výška
2	[number]	k4	2
642	[number]	k4	642
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Egypt	Egypt	k1gInSc1	Egypt
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejsušších	suchý	k2eAgFnPc2d3	nejsušší
a	a	k8xC	a
nejslunnějších	slunný	k2eAgFnPc2d3	nejslunnější
zemí	zem	k1gFnPc2	zem
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Klima	klima	k1gNnSc1	klima
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
od	od	k7c2	od
severu	sever	k1gInSc2	sever
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
od	od	k7c2	od
východu	východ	k1gInSc2	východ
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgNnSc4d1	severní
pobřeží	pobřeží	k1gNnSc4	pobřeží
u	u	k7c2	u
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
má	mít	k5eAaImIp3nS	mít
horká	horký	k2eAgNnPc4d1	horké
suchá	suchý	k2eAgNnPc4d1	suché
léta	léto	k1gNnPc4	léto
a	a	k8xC	a
mírné	mírný	k2eAgFnPc4d1	mírná
zimy	zima	k1gFnPc4	zima
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
přiměřené	přiměřený	k2eAgInPc1d1	přiměřený
deště	dešť	k1gInPc1	dešť
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
nejteplejšího	teplý	k2eAgInSc2d3	nejteplejší
července	červenec	k1gInSc2	červenec
v	v	k7c6	v
Alexandrii	Alexandrie	k1gFnSc6	Alexandrie
je	být	k5eAaImIp3nS	být
26	[number]	k4	26
<g/>
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
13	[number]	k4	13
<g/>
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
<g/>
.	.	kIx.	.
</s>
<s>
Narozdíl	narozdíl	k6eAd1	narozdíl
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
území	území	k1gNnSc2	území
zde	zde	k6eAd1	zde
nejsou	být	k5eNaImIp3nP	být
srážky	srážka	k1gFnPc1	srážka
neobvyklé	obvyklý	k2eNgFnPc1d1	neobvyklá
<g/>
.	.	kIx.	.
</s>
<s>
Směrem	směr	k1gInSc7	směr
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
se	se	k3xPyFc4	se
podnebí	podnebí	k1gNnPc2	podnebí
ze	z	k7c2	z
subtropického	subtropický	k2eAgNnSc2d1	subtropické
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
aridní	aridní	k2eAgNnSc4d1	aridní
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
vyznačováno	vyznačovat	k5eAaImNgNnS	vyznačovat
obecně	obecně	k6eAd1	obecně
vyššími	vysoký	k2eAgInPc7d2	vyšší
teplotními	teplotní	k2eAgInPc7d1	teplotní
rozdíly	rozdíl	k1gInPc7	rozdíl
<g/>
,	,	kIx,	,
vyššími	vysoký	k2eAgFnPc7d2	vyšší
teplotami	teplota	k1gFnPc7	teplota
a	a	k8xC	a
menším	malý	k2eAgInSc7d2	menší
či	či	k8xC	či
zanedbatelným	zanedbatelný	k2eAgInSc7d1	zanedbatelný
úhrnem	úhrn	k1gInSc7	úhrn
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Luxor	Luxor	k1gInSc1	Luxor
uprostřed	uprostřed	k7c2	uprostřed
země	zem	k1gFnSc2	zem
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
horká	horký	k2eAgNnPc4d1	horké
léta	léto	k1gNnPc4	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
teploty	teplota	k1gFnPc1	teplota
běžně	běžně	k6eAd1	běžně
překračují	překračovat	k5eAaImIp3nP	překračovat
40	[number]	k4	40
<g/>
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
krátké	krátký	k2eAgFnSc2d1	krátká
slunné	slunný	k2eAgFnSc2d1	slunná
zimy	zima	k1gFnSc2	zima
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
nejchladnější	chladný	k2eAgFnSc6d3	nejchladnější
části	část	k1gFnSc6	část
mohou	moct	k5eAaImIp3nP	moct
teploty	teplota	k1gFnPc4	teplota
klesnout	klesnout	k5eAaPmF	klesnout
k	k	k7c3	k
ránu	ráno	k1gNnSc3	ráno
pod	pod	k7c7	pod
10	[number]	k4	10
<g/>
°	°	k?	°
<g/>
C.	C.	kA	C.
Asuán	Asuán	k1gInSc1	Asuán
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
leží	ležet	k5eAaImIp3nS	ležet
takřka	takřka	k6eAd1	takřka
na	na	k7c6	na
obratníku	obratník	k1gInSc6	obratník
raka	rak	k1gMnSc2	rak
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
klima	klima	k1gNnSc4	klima
nejteplejší	teplý	k2eAgNnSc4d3	nejteplejší
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
letních	letní	k2eAgInPc2d1	letní
měsíců	měsíc	k1gInPc2	měsíc
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
34	[number]	k4	34
<g/>
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
během	během	k7c2	během
dne	den	k1gInSc2	den
mohou	moct	k5eAaImIp3nP	moct
teploty	teplota	k1gFnPc1	teplota
vystoupit	vystoupit	k5eAaPmF	vystoupit
až	až	k6eAd1	až
na	na	k7c4	na
47	[number]	k4	47
<g/>
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
neklesají	klesat	k5eNaImIp3nP	klesat
pod	pod	k7c7	pod
25	[number]	k4	25
<g/>
°	°	k?	°
<g/>
C.	C.	kA	C.
Zimy	zima	k1gFnPc1	zima
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
krátké	krátký	k2eAgFnPc1d1	krátká
<g/>
,	,	kIx,	,
slunné	slunný	k2eAgFnPc1d1	slunná
a	a	k8xC	a
teplé	teplý	k2eAgFnPc1d1	teplá
<g/>
,	,	kIx,	,
s	s	k7c7	s
průměrnými	průměrný	k2eAgFnPc7d1	průměrná
teplotami	teplota	k1gFnPc7	teplota
25	[number]	k4	25
<g/>
°	°	k?	°
<g/>
C	C	kA	C
během	během	k7c2	během
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
.	.	kIx.	.
</s>
<s>
Asuán	Asuán	k1gInSc1	Asuán
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejsušších	suchý	k2eAgNnPc2d3	nejsušší
trvale	trvale	k6eAd1	trvale
obydlených	obydlený	k2eAgNnPc2d1	obydlené
míst	místo	k1gNnPc2	místo
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
déšť	déšť	k1gInSc1	déšť
nemusí	muset	k5eNaImIp3nS	muset
objevit	objevit	k5eAaPmF	objevit
i	i	k9	i
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
vlhkost	vlhkost	k1gFnSc1	vlhkost
vzduchu	vzduch	k1gInSc2	vzduch
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
,	,	kIx,	,
v	v	k7c6	v
extrému	extrém	k1gInSc2	extrém
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
pouhých	pouhý	k2eAgNnPc2d1	pouhé
10	[number]	k4	10
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
všeho	všecek	k3xTgNnSc2	všecek
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejslunnější	slunný	k2eAgNnPc4d3	nejslunnější
místa	místo	k1gNnPc4	místo
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
<g/>
,	,	kIx,	,
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
3800	[number]	k4	3800
hodinami	hodina	k1gFnPc7	hodina
slunečního	sluneční	k2eAgInSc2d1	sluneční
svitu	svit	k1gInSc2	svit
za	za	k7c4	za
rok	rok	k1gInSc4	rok
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
na	na	k7c6	na
teoretickém	teoretický	k2eAgNnSc6d1	teoretické
maximu	maximum	k1gNnSc6	maximum
a	a	k8xC	a
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
místo	místo	k1gNnSc4	místo
hned	hned	k6eAd1	hned
za	za	k7c4	za
severoamerickou	severoamerický	k2eAgFnSc4d1	severoamerická
Yumou	Yumá	k1gFnSc4	Yumá
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
díky	díky	k7c3	díky
vyšší	vysoký	k2eAgFnSc3d2	vyšší
zeměpisné	zeměpisný	k2eAgFnSc3d1	zeměpisná
šířce	šířka	k1gFnSc3	šířka
a	a	k8xC	a
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
delším	dlouhý	k2eAgInPc3d2	delší
dnům	den	k1gInPc3	den
počet	počet	k1gInSc1	počet
slunečních	sluneční	k2eAgFnPc2d1	sluneční
hodin	hodina	k1gFnPc2	hodina
4000	[number]	k4	4000
<g/>
.	.	kIx.	.
</s>
<s>
Pobřeží	pobřeží	k1gNnSc1	pobřeží
u	u	k7c2	u
Rudého	rudý	k2eAgNnSc2d1	Rudé
moře	moře	k1gNnSc2	moře
má	mít	k5eAaImIp3nS	mít
klima	klima	k1gNnSc4	klima
obecně	obecně	k6eAd1	obecně
mírnější	mírný	k2eAgFnPc1d2	mírnější
<g/>
,	,	kIx,	,
maximální	maximální	k2eAgFnPc1d1	maximální
teploty	teplota	k1gFnPc1	teplota
obvykle	obvykle	k6eAd1	obvykle
nestoupají	stoupat	k5eNaImIp3nP	stoupat
tak	tak	k6eAd1	tak
vysoko	vysoko	k6eAd1	vysoko
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
noci	noc	k1gFnPc1	noc
jsou	být	k5eAaImIp3nP	být
tu	tu	k6eAd1	tu
teplejší	teplý	k2eAgFnPc1d2	teplejší
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
zimních	zimní	k2eAgInPc6d1	zimní
měsících	měsíc	k1gInPc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
nejteplejšího	teplý	k2eAgInSc2d3	nejteplejší
července	červenec	k1gInSc2	červenec
v	v	k7c6	v
letoviscích	letovisko	k1gNnPc6	letovisko
Šarm	šarm	k1gInSc1	šarm
aš-Šajch	aš-Šajch	k1gInSc1	aš-Šajch
<g/>
,	,	kIx,	,
Hurghada	Hurghada	k1gFnSc1	Hurghada
<g/>
,	,	kIx,	,
Marsa	Marsa	k1gFnSc1	Marsa
Alam	Alama	k1gFnPc2	Alama
je	být	k5eAaImIp3nS	být
34	[number]	k4	34
<g/>
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
34	[number]	k4	34
<g/>
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
33	[number]	k4	33
<g/>
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
nejchladnějšího	chladný	k2eAgInSc2d3	nejchladnější
ledna	leden	k1gInSc2	leden
18	[number]	k4	18
<g/>
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
19	[number]	k4	19
<g/>
°	°	k?	°
<g/>
C.	C.	kA	C.
Marsa	Marsa	k1gFnSc1	Marsa
Alam	Alam	k1gInSc1	Alam
a	a	k8xC	a
Šarm	šarm	k1gInSc1	šarm
aš-Šajch	aš-Šajch	k1gMnSc1	aš-Šajch
mají	mít	k5eAaImIp3nP	mít
vůbec	vůbec	k9	vůbec
nejteplejší	teplý	k2eAgFnPc1d3	nejteplejší
noci	noc	k1gFnPc1	noc
ze	z	k7c2	z
všech	všecek	k3xTgNnPc2	všecek
egyptských	egyptský	k2eAgNnPc2d1	egyptské
měst	město	k1gNnPc2	město
a	a	k8xC	a
obecně	obecně	k6eAd1	obecně
nejkonstantnější	konstantní	k2eAgNnSc1d3	konstantní
klima	klima	k1gNnSc1	klima
<g/>
,	,	kIx,	,
s	s	k7c7	s
malými	malý	k2eAgInPc7d1	malý
teplotními	teplotní	k2eAgInPc7d1	teplotní
výkyvy	výkyv	k1gInPc7	výkyv
<g/>
.	.	kIx.	.
</s>
<s>
Nejpříhodnější	příhodný	k2eAgNnSc1d3	nejpříhodnější
období	období	k1gNnSc1	období
pro	pro	k7c4	pro
návštěvu	návštěva	k1gFnSc4	návštěva
jsou	být	k5eAaImIp3nP	být
měsíce	měsíc	k1gInPc1	měsíc
březen	březen	k1gInSc1	březen
a	a	k8xC	a
duben	duben	k1gInSc1	duben
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
říjen	říjen	k1gInSc1	říjen
a	a	k8xC	a
listopad	listopad	k1gInSc1	listopad
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
průměrnými	průměrný	k2eAgFnPc7d1	průměrná
teplotami	teplota	k1gFnPc7	teplota
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
26	[number]	k4	26
<g/>
°	°	k?	°
<g/>
C-	C-	k1gFnSc1	C-
<g/>
29	[number]	k4	29
<g/>
°	°	k?	°
<g/>
C	C	kA	C
připomínají	připomínat	k5eAaImIp3nP	připomínat
léta	léto	k1gNnPc4	léto
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
byl	být	k5eAaImAgInS	být
Egypt	Egypt	k1gInSc4	Egypt
prezidentskou	prezidentský	k2eAgFnSc7d1	prezidentská
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
Vládu	vláda	k1gFnSc4	vláda
si	se	k3xPyFc3	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
držela	držet	k5eAaImAgFnS	držet
Národní	národní	k2eAgFnSc1d1	národní
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
454	[number]	k4	454
členného	členný	k2eAgInSc2d1	členný
parlamentu	parlament	k1gInSc2	parlament
se	se	k3xPyFc4	se
volilo	volit	k5eAaImAgNnS	volit
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgNnSc7d1	významné
opozičním	opoziční	k2eAgNnSc7d1	opoziční
hnutím	hnutí	k1gNnSc7	hnutí
bylo	být	k5eAaImAgNnS	být
Muslimské	muslimský	k2eAgNnSc1d1	muslimské
bratrstvo	bratrstvo	k1gNnSc1	bratrstvo
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgNnSc3	jenž
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
náboženských	náboženský	k2eAgInPc2d1	náboženský
důvodů	důvod	k1gInPc2	důvod
zakázáno	zakázán	k2eAgNnSc1d1	zakázáno
vytvořit	vytvořit	k5eAaPmF	vytvořit
politickou	politický	k2eAgFnSc4d1	politická
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
prezident	prezident	k1gMnSc1	prezident
Muhammad	Muhammad	k1gInSc4	Muhammad
Husní	Husní	k2eAgInSc4d1	Husní
Mubarak	Mubarak	k1gInSc4	Mubarak
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
Egypta	Egypt	k1gInSc2	Egypt
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
donucen	donutit	k5eAaPmNgMnS	donutit
odstoupit	odstoupit	k5eAaPmF	odstoupit
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentovu	prezidentův	k2eAgFnSc4d1	prezidentova
rezignaci	rezignace	k1gFnSc4	rezignace
oznámil	oznámit	k5eAaPmAgMnS	oznámit
viceprezident	viceprezident	k1gMnSc1	viceprezident
Umar	Umar	k1gMnSc1	Umar
Sulajmán	Sulajmán	k2eAgMnSc1d1	Sulajmán
v	v	k7c6	v
krátkém	krátký	k2eAgNnSc6d1	krátké
televizním	televizní	k2eAgNnSc6d1	televizní
prohlášení	prohlášení	k1gNnSc6	prohlášení
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
prezident	prezident	k1gMnSc1	prezident
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
pro	pro	k7c4	pro
dobro	dobro	k1gNnSc4	dobro
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Moc	moc	k6eAd1	moc
poté	poté	k6eAd1	poté
převzala	převzít	k5eAaPmAgFnS	převzít
armáda	armáda	k1gFnSc1	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
demokratická	demokratický	k2eAgFnSc1d1	demokratická
ústava	ústava	k1gFnSc1	ústava
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
v	v	k7c6	v
referendu	referendum	k1gNnSc6	referendum
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
ustavila	ustavit	k5eAaPmAgFnS	ustavit
poloprezidentskou	poloprezidentský	k2eAgFnSc4d1	poloprezidentská
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následných	následný	k2eAgFnPc6d1	následná
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
i	i	k8xC	i
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
zvítězili	zvítězit	k5eAaPmAgMnP	zvítězit
islamisté	islamista	k1gMnPc1	islamista
<g/>
.	.	kIx.	.
</s>
<s>
Islamistická	islamistický	k2eAgFnSc1d1	islamistická
Strana	strana	k1gFnSc1	strana
svobody	svoboda	k1gFnSc2	svoboda
a	a	k8xC	a
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
,	,	kIx,	,
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
Muslimským	muslimský	k2eAgNnSc7d1	muslimské
bratrstvem	bratrstvo	k1gNnSc7	bratrstvo
<g/>
,	,	kIx,	,
získala	získat	k5eAaPmAgFnS	získat
37,5	[number]	k4	37,5
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
dolní	dolní	k2eAgFnSc2d1	dolní
komory	komora	k1gFnSc2	komora
a	a	k8xC	a
45	[number]	k4	45
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
horní	horní	k2eAgFnSc2d1	horní
komory	komora	k1gFnSc2	komora
Parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
skončila	skončit	k5eAaPmAgFnS	skončit
také	také	k9	také
islamistická	islamistický	k2eAgFnSc1d1	islamistická
Strana	strana	k1gFnSc1	strana
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
k	k	k7c3	k
salafismu	salafismus	k1gInSc3	salafismus
(	(	kIx(	(
<g/>
či	či	k8xC	či
wahhábismu	wahhábismus	k1gInSc2	wahhábismus
<g/>
)	)	kIx)	)
se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
okolo	okolo	k7c2	okolo
28	[number]	k4	28
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Liberální	liberální	k2eAgFnPc1d1	liberální
strany	strana	k1gFnPc1	strana
vstoupily	vstoupit	k5eAaPmAgFnP	vstoupit
do	do	k7c2	do
opozice	opozice	k1gFnSc2	opozice
<g/>
,	,	kIx,	,
když	když	k8xS	když
celkem	celkem	k6eAd1	celkem
získaly	získat	k5eAaPmAgFnP	získat
okolo	okolo	k7c2	okolo
20	[number]	k4	20
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentem	prezident	k1gMnSc7	prezident
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
přímé	přímý	k2eAgFnSc6d1	přímá
volbě	volba	k1gFnSc6	volba
zvolen	zvolit	k5eAaPmNgInS	zvolit
Muhammad	Muhammad	k1gInSc1	Muhammad
Mursí	Mursí	k1gNnSc2	Mursí
(	(	kIx(	(
<g/>
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Svobody	svoboda	k1gFnSc2	svoboda
a	a	k8xC	a
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
dvoukolové	dvoukolový	k2eAgFnSc6d1	dvoukolová
volbě	volba	k1gFnSc6	volba
získal	získat	k5eAaPmAgInS	získat
24,78	[number]	k4	24,78
%	%	kIx~	%
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
51,73	[number]	k4	51,73
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
další	další	k2eAgFnSc6d1	další
vlně	vlna	k1gFnSc6	vlna
lidových	lidový	k2eAgFnPc2d1	lidová
demonstrací	demonstrace	k1gFnPc2	demonstrace
<g/>
,	,	kIx,	,
protestujících	protestující	k2eAgInPc2d1	protestující
proti	proti	k7c3	proti
Mursího	Mursí	k1gMnSc4	Mursí
vládě	vláda	k1gFnSc6	vláda
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
armáda	armáda	k1gFnSc1	armáda
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
července	červenec	k1gInSc2	červenec
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
nespokojeného	spokojený	k2eNgInSc2d1	nespokojený
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
chopila	chopit	k5eAaPmAgFnS	chopit
moci	moct	k5eAaImF	moct
a	a	k8xC	a
svrhla	svrhnout	k5eAaPmAgFnS	svrhnout
Mursího	Mursí	k1gMnSc4	Mursí
i	i	k8xC	i
jeho	jeho	k3xOp3gFnSc4	jeho
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Prozatímním	prozatímní	k2eAgMnSc7d1	prozatímní
prezidentem	prezident	k1gMnSc7	prezident
byl	být	k5eAaImAgMnS	být
jmenován	jmenován	k2eAgMnSc1d1	jmenován
předseda	předseda	k1gMnSc1	předseda
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
Adlí	Adlí	k1gMnSc1	Adlí
Mansúr	Mansúr	k1gMnSc1	Mansúr
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
poté	poté	k6eAd1	poté
rozpustil	rozpustit	k5eAaPmAgInS	rozpustit
Parlament	parlament	k1gInSc1	parlament
s	s	k7c7	s
plánem	plán	k1gInSc7	plán
vytvořit	vytvořit	k5eAaPmF	vytvořit
úřednickou	úřednický	k2eAgFnSc4d1	úřednická
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
navrhne	navrhnout	k5eAaPmIp3nS	navrhnout
novou	nový	k2eAgFnSc4d1	nová
ústavu	ústava	k1gFnSc4	ústava
a	a	k8xC	a
dovede	dovést	k5eAaPmIp3nS	dovést
zemi	zem	k1gFnSc4	zem
k	k	k7c3	k
novým	nový	k2eAgFnPc3d1	nová
volbám	volba	k1gFnPc3	volba
<g/>
.	.	kIx.	.
</s>
<s>
Egypt	Egypt	k1gInSc1	Egypt
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
největší	veliký	k2eAgFnSc4d3	veliký
a	a	k8xC	a
nejsilnější	silný	k2eAgFnSc4d3	nejsilnější
armádu	armáda	k1gFnSc4	armáda
na	na	k7c6	na
africkém	africký	k2eAgInSc6d1	africký
kontinentu	kontinent	k1gInSc6	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
současně	současně	k6eAd1	současně
o	o	k7c4	o
největší	veliký	k2eAgFnSc4d3	veliký
armádu	armáda	k1gFnSc4	armáda
v	v	k7c6	v
arabském	arabský	k2eAgInSc6d1	arabský
světě	svět	k1gInSc6	svět
<g/>
;	;	kIx,	;
celosvětově	celosvětově	k6eAd1	celosvětově
na	na	k7c4	na
11	[number]	k4	11
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
aktivní	aktivní	k2eAgFnSc6d1	aktivní
službě	služba	k1gFnSc6	služba
má	mít	k5eAaImIp3nS	mít
Egypt	Egypt	k1gInSc4	Egypt
asi	asi	k9	asi
468	[number]	k4	468
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
vojáků	voják	k1gMnPc2	voják
záložních	záložní	k2eAgNnPc2d1	záložní
vojsk	vojsko	k1gNnPc2	vojsko
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
l	l	kA	l
<g/>
800	[number]	k4	800
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výzbroji	výzbroj	k1gInSc6	výzbroj
egyptské	egyptský	k2eAgFnSc2d1	egyptská
armády	armáda	k1gFnSc2	armáda
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
počtu	počet	k1gInSc6	počet
zastoupeny	zastoupit	k5eAaPmNgFnP	zastoupit
nejmodernější	moderní	k2eAgFnPc1d3	nejmodernější
zbraně	zbraň	k1gFnPc1	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Platí	platit	k5eAaImIp3nS	platit
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
o	o	k7c6	o
letectvu	letectvo	k1gNnSc6	letectvo
a	a	k8xC	a
pozemním	pozemní	k2eAgNnSc6d1	pozemní
vojsku	vojsko	k1gNnSc6	vojsko
(	(	kIx(	(
<g/>
armáda	armáda	k1gFnSc1	armáda
má	mít	k5eAaImIp3nS	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
odhadem	odhad	k1gInSc7	odhad
3750	[number]	k4	3750
tanků	tank	k1gInPc2	tank
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
nejméně	málo	k6eAd3	málo
1000	[number]	k4	1000
špičkových	špičkový	k2eAgInPc2d1	špičkový
těžkých	těžký	k2eAgInPc2d1	těžký
tanků	tank	k1gInPc2	tank
M1	M1	k1gFnSc2	M1
Abrams	Abramsa	k1gFnPc2	Abramsa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Egyptská	egyptský	k2eAgFnSc1d1	egyptská
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
vedla	vést	k5eAaImAgFnS	vést
během	během	k7c2	během
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
čtyři	čtyři	k4xCgFnPc1	čtyři
války	válka	k1gFnPc1	válka
se	s	k7c7	s
státem	stát	k1gInSc7	stát
Izrael	Izrael	k1gInSc1	Izrael
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
a	a	k8xC	a
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
se	se	k3xPyFc4	se
egyptské	egyptský	k2eAgNnSc1d1	egyptské
vojsko	vojsko	k1gNnSc1	vojsko
účastnilo	účastnit	k5eAaImAgNnS	účastnit
operace	operace	k1gFnPc4	operace
Pouštní	pouštní	k2eAgFnPc4d1	pouštní
bouře	bouř	k1gFnPc4	bouř
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
vojsk	vojsko	k1gNnPc2	vojsko
koalice	koalice	k1gFnSc2	koalice
<g/>
.	.	kIx.	.
</s>
<s>
Egypt	Egypt	k1gInSc1	Egypt
je	být	k5eAaImIp3nS	být
silným	silný	k2eAgMnSc7d1	silný
strategickým	strategický	k2eAgMnSc7d1	strategický
partnerem	partner	k1gMnSc7	partner
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
Egyptu	Egypt	k1gInSc3	Egypt
pravidelnou	pravidelný	k2eAgFnSc4d1	pravidelná
roční	roční	k2eAgFnSc4d1	roční
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
pomoc	pomoc	k1gFnSc4	pomoc
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
1,3	[number]	k4	1,3
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
2012	[number]	k4	2012
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Egypta	Egypt	k1gInSc2	Egypt
závisí	záviset	k5eAaImIp3nS	záviset
především	především	k9	především
na	na	k7c6	na
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
,	,	kIx,	,
mediální	mediální	k2eAgFnSc3d1	mediální
sféře	sféra	k1gFnSc3	sféra
<g/>
,	,	kIx,	,
vývozu	vývoz	k1gInSc3	vývoz
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
turistice	turistika	k1gFnSc6	turistika
<g/>
;	;	kIx,	;
více	hodně	k6eAd2	hodně
než	než	k8xS	než
tři	tři	k4xCgInPc4	tři
miliony	milion	k4xCgInPc4	milion
Egypťanů	Egypťan	k1gMnPc2	Egypťan
však	však	k8xC	však
přitom	přitom	k6eAd1	přitom
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
Saúdské	saúdský	k2eAgFnSc6d1	Saúdská
Arábii	Arábie	k1gFnSc6	Arábie
<g/>
,	,	kIx,	,
zemích	zem	k1gFnPc6	zem
Perského	perský	k2eAgInSc2d1	perský
zálivu	záliv	k1gInSc2	záliv
a	a	k8xC	a
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Dokončení	dokončení	k1gNnSc1	dokončení
přehrady	přehrada	k1gFnSc2	přehrada
v	v	k7c6	v
Asuánu	Asuán	k1gInSc6	Asuán
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
a	a	k8xC	a
výsledné	výsledný	k2eAgNnSc1d1	výsledné
Násirovo	Násirův	k2eAgNnSc1d1	Násirovo
jezero	jezero	k1gNnSc1	jezero
změnilo	změnit	k5eAaPmAgNnS	změnit
časem	časem	k6eAd1	časem
prověřenou	prověřený	k2eAgFnSc4d1	prověřená
úlohu	úloha	k1gFnSc4	úloha
řeky	řeka	k1gFnSc2	řeka
Nilu	Nil	k1gInSc2	Nil
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
a	a	k8xC	a
ekologii	ekologie	k1gFnSc3	ekologie
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Rychle	rychle	k6eAd1	rychle
rostoucí	rostoucí	k2eAgFnPc1d1	rostoucí
populace	populace	k1gFnPc1	populace
<g/>
,	,	kIx,	,
omezené	omezený	k2eAgNnSc1d1	omezené
množství	množství	k1gNnSc1	množství
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
závislost	závislost	k1gFnSc4	závislost
na	na	k7c6	na
Nilu	Nil	k1gInSc6	Nil
však	však	k9	však
stále	stále	k6eAd1	stále
zdražují	zdražovat	k5eAaImIp3nP	zdražovat
zdroje	zdroj	k1gInPc4	zdroj
a	a	k8xC	a
stresují	stresovat	k5eAaImIp3nP	stresovat
ekonomiku	ekonomika	k1gFnSc4	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
připravit	připravit	k5eAaPmF	připravit
ekonomiku	ekonomika	k1gFnSc4	ekonomika
na	na	k7c4	na
nové	nový	k2eAgNnSc4d1	nové
milénium	milénium	k1gNnSc4	milénium
pomocí	pomocí	k7c2	pomocí
reforem	reforma	k1gFnPc2	reforma
a	a	k8xC	a
rozsáhlých	rozsáhlý	k2eAgFnPc2d1	rozsáhlá
investic	investice	k1gFnPc2	investice
do	do	k7c2	do
dopravy	doprava	k1gFnSc2	doprava
a	a	k8xC	a
fyzické	fyzický	k2eAgFnSc2d1	fyzická
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
<g/>
.	.	kIx.	.
</s>
<s>
Egypt	Egypt	k1gInSc1	Egypt
přijímal	přijímat	k5eAaImAgInS	přijímat
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
pomoc	pomoc	k1gFnSc4	pomoc
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
(	(	kIx(	(
<g/>
průměrně	průměrně	k6eAd1	průměrně
2,2	[number]	k4	2,2
miliardy	miliarda	k4xCgFnSc2	miliarda
ročně	ročně	k6eAd1	ročně
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
třetím	třetí	k4xOgInSc7	třetí
největším	veliký	k2eAgInSc7d3	veliký
cílem	cíl	k1gInSc7	cíl
zahraničních	zahraniční	k2eAgInPc2d1	zahraniční
fondů	fond	k1gInPc2	fond
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
hlavní	hlavní	k2eAgInPc1d1	hlavní
příjmy	příjem	k1gInPc1	příjem
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
z	z	k7c2	z
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
a	a	k8xC	a
také	také	k9	také
z	z	k7c2	z
lodní	lodní	k2eAgFnSc2d1	lodní
přepravy	přeprava	k1gFnSc2	přeprava
skrze	skrze	k?	skrze
Suezský	suezský	k2eAgInSc4d1	suezský
průplav	průplav	k1gInSc4	průplav
<g/>
.	.	kIx.	.
</s>
<s>
Egypt	Egypt	k1gInSc1	Egypt
má	mít	k5eAaImIp3nS	mít
vyvinutý	vyvinutý	k2eAgInSc4d1	vyvinutý
energetický	energetický	k2eAgInSc4d1	energetický
trh	trh	k1gInSc4	trh
založený	založený	k2eAgInSc4d1	založený
na	na	k7c6	na
uhlí	uhlí	k1gNnSc6	uhlí
<g/>
,	,	kIx,	,
ropě	ropa	k1gFnSc6	ropa
<g/>
,	,	kIx,	,
zemním	zemní	k2eAgInSc6d1	zemní
plynu	plyn	k1gInSc6	plyn
a	a	k8xC	a
vodní	vodní	k2eAgFnSc6d1	vodní
energetice	energetika	k1gFnSc6	energetika
<g/>
.	.	kIx.	.
</s>
<s>
Značné	značný	k2eAgFnPc1d1	značná
zásoby	zásoba	k1gFnPc1	zásoba
uhlí	uhlí	k1gNnSc2	uhlí
se	se	k3xPyFc4	se
nalézají	nalézat	k5eAaImIp3nP	nalézat
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
Sinaje	Sinaj	k1gInSc2	Sinaj
a	a	k8xC	a
těží	těžet	k5eAaImIp3nS	těžet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
přibližně	přibližně	k6eAd1	přibližně
600	[number]	k4	600
000	[number]	k4	000
tun	tuna	k1gFnPc2	tuna
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Ropa	ropa	k1gFnSc1	ropa
a	a	k8xC	a
plyn	plyn	k1gInSc1	plyn
se	se	k3xPyFc4	se
těží	těžet	k5eAaImIp3nS	těžet
v	v	k7c6	v
pouštních	pouštní	k2eAgFnPc6d1	pouštní
oblastech	oblast	k1gFnPc6	oblast
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
,	,	kIx,	,
Suezském	suezský	k2eAgInSc6d1	suezský
zálivu	záliv	k1gInSc6	záliv
a	a	k8xC	a
v	v	k7c6	v
Nilské	nilský	k2eAgFnSc6d1	nilská
deltě	delta	k1gFnSc6	delta
<g/>
.	.	kIx.	.
</s>
<s>
Egypt	Egypt	k1gInSc1	Egypt
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
značné	značný	k2eAgFnPc4d1	značná
rezervy	rezerva	k1gFnPc4	rezerva
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
odhadovalo	odhadovat	k5eAaImAgNnS	odhadovat
kolem	kolem	k7c2	kolem
1	[number]	k4	1
100	[number]	k4	100
000	[number]	k4	000
m3	m3	k4	m3
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyváží	vyvážit	k5eAaPmIp3nS	vyvážit
jej	on	k3xPp3gMnSc4	on
také	také	k9	také
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Egypt	Egypt	k1gInSc1	Egypt
je	být	k5eAaImIp3nS	být
také	také	k9	také
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
afrických	africký	k2eAgFnPc2d1	africká
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
usilují	usilovat	k5eAaImIp3nP	usilovat
o	o	k7c4	o
stavbu	stavba	k1gFnSc4	stavba
jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
v	v	k7c6	v
horizontu	horizont	k1gInSc6	horizont
příštích	příští	k2eAgNnPc2d1	příští
deseti	deset	k4xCc2	deset
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
tendr	tendr	k1gInSc1	tendr
na	na	k7c4	na
hlavního	hlavní	k2eAgMnSc4d1	hlavní
dodavatel	dodavatel	k1gMnSc1	dodavatel
a	a	k8xC	a
elektrárna	elektrárna	k1gFnSc1	elektrárna
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
podle	podle	k7c2	podle
původního	původní	k2eAgInSc2d1	původní
plánu	plán	k1gInSc2	plán
spuštěna	spustit	k5eAaPmNgFnS	spustit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2020	[number]	k4	2020
<g/>
.	.	kIx.	.
</s>
<s>
Elektrárna	elektrárna	k1gFnSc1	elektrárna
bude	být	k5eAaImBp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
stát	stát	k5eAaPmF	stát
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
obce	obec	k1gFnSc2	obec
Ad	ad	k7c4	ad
Dabaá	Dabaé	k1gNnPc4	Dabaé
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
nestabilní	stabilní	k2eNgFnSc3d1	nestabilní
situaci	situace	k1gFnSc3	situace
po	po	k7c6	po
protestech	protest	k1gInPc6	protest
<g/>
,	,	kIx,	,
pádu	pád	k1gInSc6	pád
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
prezidenta	prezident	k1gMnSc2	prezident
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
bylo	být	k5eAaImAgNnS	být
ale	ale	k8xC	ale
zahájení	zahájení	k1gNnSc1	zahájení
tendru	tendr	k1gInSc2	tendr
prozatímní	prozatímní	k2eAgFnSc7d1	prozatímní
vládou	vláda	k1gFnSc7	vláda
odloženo	odložen	k2eAgNnSc4d1	odloženo
na	na	k7c4	na
neurčito	neurčito	k1gNnSc4	neurčito
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
vláda	vláda	k1gFnSc1	vláda
zahájila	zahájit	k5eAaPmAgFnS	zahájit
přípravné	přípravný	k2eAgFnPc4d1	přípravná
práce	práce	k1gFnPc4	práce
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
stavbu	stavba	k1gFnSc4	stavba
zablokovali	zablokovat	k5eAaPmAgMnP	zablokovat
místní	místní	k2eAgMnPc1d1	místní
beduíni	beduín	k1gMnPc1	beduín
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
elektrárna	elektrárna	k1gFnSc1	elektrárna
připravila	připravit	k5eAaPmAgFnS	připravit
o	o	k7c4	o
půdu	půda	k1gFnSc4	půda
<g/>
,	,	kIx,	,
konflikt	konflikt	k1gInSc4	konflikt
zatím	zatím	k6eAd1	zatím
nebyl	být	k5eNaImAgInS	být
vyřešen	vyřešit	k5eAaPmNgInS	vyřešit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
10	[number]	k4	10
<g/>
%	%	kIx~	%
míra	míra	k1gFnSc1	míra
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
a	a	k8xC	a
11	[number]	k4	11
<g/>
%	%	kIx~	%
míra	míra	k1gFnSc1	míra
inflace	inflace	k1gFnSc2	inflace
<g/>
.	.	kIx.	.
</s>
<s>
HDP	HDP	kA	HDP
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
zde	zde	k6eAd1	zde
dosahovalo	dosahovat	k5eAaImAgNnS	dosahovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
2355	[number]	k4	2355
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Měna	měna	k1gFnSc1	měna
je	být	k5eAaImIp3nS	být
Egyptská	egyptský	k2eAgFnSc1d1	egyptská
libra	libra	k1gFnSc1	libra
(	(	kIx(	(
<g/>
LE	LE	kA	LE
<g/>
)	)	kIx)	)
=	=	kIx~	=
100	[number]	k4	100
piastrů	piastr	k1gInPc2	piastr
(	(	kIx(	(
<g/>
1	[number]	k4	1
USD	USD	kA	USD
=	=	kIx~	=
5,70	[number]	k4	5,70
LE	LE	kA	LE
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Turismus	turismus	k1gInSc1	turismus
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Cestovní	cestovní	k2eAgInSc1d1	cestovní
ruch	ruch	k1gInSc1	ruch
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgInPc2d3	nejdůležitější
sektorů	sektor	k1gInPc2	sektor
egyptské	egyptský	k2eAgFnSc2d1	egyptská
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgMnPc7d1	hlavní
turistickými	turistický	k2eAgMnPc7d1	turistický
lákadly	lákadlo	k1gNnPc7	lákadlo
jsou	být	k5eAaImIp3nP	být
tisíce	tisíc	k4xCgInPc1	tisíc
let	léto	k1gNnPc2	léto
staré	starý	k2eAgFnSc2d1	stará
památky	památka	k1gFnSc2	památka
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
nimž	jenž	k3xRgFnPc3	jenž
je	být	k5eAaImIp3nS	být
Egypt	Egypt	k1gInSc1	Egypt
světově	světově	k6eAd1	světově
proslulou	proslulý	k2eAgFnSc7d1	proslulá
destinací	destinace	k1gFnSc7	destinace
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgInPc7d1	hlavní
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
jsou	být	k5eAaImIp3nP	být
pyramidy	pyramida	k1gFnPc1	pyramida
a	a	k8xC	a
Velká	velký	k2eAgFnSc1d1	velká
sfinga	sfinga	k1gFnSc1	sfinga
v	v	k7c6	v
Gíze	Gíze	k1gFnSc6	Gíze
<g/>
,	,	kIx,	,
chrám	chrám	k1gInSc1	chrám
Abú	abú	k1gMnSc1	abú
Simbel	Simbel	k1gMnSc1	Simbel
ležící	ležící	k2eAgNnSc4d1	ležící
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Asuánu	Asuán	k1gInSc2	Asuán
<g/>
,	,	kIx,	,
Karnacký	Karnacký	k2eAgInSc1d1	Karnacký
chrámový	chrámový	k2eAgInSc1d1	chrámový
komplex	komplex	k1gInSc1	komplex
a	a	k8xC	a
Údolí	údolí	k1gNnSc1	údolí
králů	král	k1gMnPc2	král
nedaleko	nedaleko	k7c2	nedaleko
Luxoru	Luxor	k1gInSc2	Luxor
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Káhira	Káhira	k1gFnSc1	Káhira
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
pochlubit	pochlubit	k5eAaPmF	pochlubit
Egyptským	egyptský	k2eAgNnSc7d1	egyptské
muzeem	muzeum	k1gNnSc7	muzeum
či	či	k8xC	či
mešitou	mešita	k1gFnSc7	mešita
Muhammada	Muhammada	k1gFnSc1	Muhammada
Alího	Alí	k1gMnSc2	Alí
<g/>
.	.	kIx.	.
</s>
<s>
Turisty	turist	k1gMnPc4	turist
rovněž	rovněž	k9	rovněž
láká	lákat	k5eAaImIp3nS	lákat
pestrobarevný	pestrobarevný	k2eAgInSc1d1	pestrobarevný
podvodní	podvodní	k2eAgInSc1d1	podvodní
svět	svět	k1gInSc1	svět
Rudého	rudý	k2eAgNnSc2d1	Rudé
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Podmínky	podmínka	k1gFnPc1	podmínka
pro	pro	k7c4	pro
potápění	potápění	k1gNnSc4	potápění
a	a	k8xC	a
šnorchlování	šnorchlování	k1gNnSc4	šnorchlování
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
jedny	jeden	k4xCgFnPc1	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Přímořská	přímořský	k2eAgNnPc1d1	přímořské
letoviska	letovisko	k1gNnPc1	letovisko
jako	jako	k8xC	jako
například	například	k6eAd1	například
Hurghada	Hurghada	k1gFnSc1	Hurghada
<g/>
,	,	kIx,	,
Šarm	šarm	k1gInSc1	šarm
aš-Šajch	aš-Šajch	k1gInSc1	aš-Šajch
či	či	k8xC	či
Taba	Taba	k1gFnSc1	Taba
navštíví	navštívit	k5eAaPmIp3nS	navštívit
ročně	ročně	k6eAd1	ročně
několik	několik	k4yIc1	několik
milionů	milion	k4xCgInPc2	milion
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Demografie	demografie	k1gFnSc2	demografie
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Egypt	Egypt	k1gInSc1	Egypt
je	být	k5eAaImIp3nS	být
nejlidnatější	lidnatý	k2eAgFnSc7d3	nejlidnatější
zemí	zem	k1gFnSc7	zem
Blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
a	a	k8xC	a
třetí	třetí	k4xOgFnSc7	třetí
nejlidnatější	lidnatý	k2eAgFnSc7d3	nejlidnatější
zemí	zem	k1gFnSc7	zem
na	na	k7c6	na
africkém	africký	k2eAgInSc6d1	africký
kontinentu	kontinent	k1gInSc6	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Rychlý	rychlý	k2eAgInSc1d1	rychlý
růst	růst	k1gInSc1	růst
populace	populace	k1gFnSc2	populace
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1970	[number]	k4	1970
až	až	k9	až
2010	[number]	k4	2010
byl	být	k5eAaImAgInS	být
způsoben	způsobit	k5eAaPmNgInS	způsobit
zlepšením	zlepšení	k1gNnSc7	zlepšení
lékařské	lékařský	k2eAgFnSc2d1	lékařská
péče	péče	k1gFnSc2	péče
a	a	k8xC	a
nárůstem	nárůst	k1gInSc7	nárůst
produktivity	produktivita	k1gFnSc2	produktivita
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Napoleonovy	Napoleonův	k2eAgFnSc2d1	Napoleonova
invaze	invaze	k1gFnSc2	invaze
od	od	k7c2	od
Egypta	Egypt	k1gInSc2	Egypt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1798	[number]	k4	1798
byla	být	k5eAaImAgFnS	být
místní	místní	k2eAgFnSc1d1	místní
populace	populace	k1gFnSc1	populace
odhadnuta	odhadnut	k2eAgFnSc1d1	odhadnuta
na	na	k7c4	na
3	[number]	k4	3
000	[number]	k4	000
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
měl	mít	k5eAaImAgInS	mít
Egypt	Egypt	k1gInSc1	Egypt
již	již	k6eAd1	již
16	[number]	k4	16
500	[number]	k4	500
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnSc1	populace
je	být	k5eAaImIp3nS	být
koncentrovaná	koncentrovaný	k2eAgFnSc1d1	koncentrovaná
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
řeky	řeka	k1gFnSc2	řeka
Nil	Nil	k1gInSc4	Nil
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
v	v	k7c6	v
nilské	nilský	k2eAgFnSc6d1	nilská
deltě	delta	k1gFnSc6	delta
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Suezského	suezský	k2eAgInSc2d1	suezský
průplavu	průplav	k1gInSc2	průplav
<g/>
.	.	kIx.	.
90	[number]	k4	90
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
jsou	být	k5eAaImIp3nP	být
muslimové	muslim	k1gMnPc1	muslim
a	a	k8xC	a
zbytek	zbytek	k1gInSc1	zbytek
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
převážně	převážně	k6eAd1	převážně
koptskými	koptský	k2eAgMnPc7d1	koptský
křesťany	křesťan	k1gMnPc7	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
příslušnosti	příslušnost	k1gFnSc2	příslušnost
k	k	k7c3	k
náboženství	náboženství	k1gNnSc3	náboženství
se	se	k3xPyFc4	se
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Egypta	Egypt	k1gInSc2	Egypt
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
obyvatele	obyvatel	k1gMnSc4	obyvatel
měst	město	k1gNnPc2	město
a	a	k8xC	a
feláhy	feláh	k1gMnPc4	feláh
<g/>
,	,	kIx,	,
venkovské	venkovský	k2eAgMnPc4d1	venkovský
zemědělce	zemědělec	k1gMnPc4	zemědělec
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
etnickou	etnický	k2eAgFnSc7d1	etnická
skupinou	skupina	k1gFnSc7	skupina
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
jsou	být	k5eAaImIp3nP	být
Egypťané	Egypťan	k1gMnPc1	Egypťan
<g/>
.	.	kIx.	.
</s>
<s>
Etnickými	etnický	k2eAgFnPc7d1	etnická
minoritami	minorita	k1gFnPc7	minorita
jsou	být	k5eAaImIp3nP	být
Turci	Turek	k1gMnPc1	Turek
<g/>
,	,	kIx,	,
Řekové	Řek	k1gMnPc1	Řek
<g/>
,	,	kIx,	,
kmeny	kmen	k1gInPc1	kmen
arabských	arabský	k2eAgMnPc2d1	arabský
Beduínů	Beduín	k1gMnPc2	Beduín
žijících	žijící	k2eAgMnPc2d1	žijící
v	v	k7c6	v
Arabské	arabský	k2eAgFnSc6d1	arabská
poušti	poušť	k1gFnSc6	poušť
a	a	k8xC	a
na	na	k7c6	na
Sinajském	sinajský	k2eAgInSc6d1	sinajský
poloostrově	poloostrov	k1gInSc6	poloostrov
a	a	k8xC	a
také	také	k9	také
Berbeři	Berber	k1gMnPc1	Berber
žijící	žijící	k2eAgMnPc1d1	žijící
na	na	k7c6	na
západě	západ	k1gInSc6	západ
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
2,7	[number]	k4	2,7
milionu	milion	k4xCgInSc2	milion
Egypťanů	Egypťan	k1gMnPc2	Egypťan
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Jazyky	jazyk	k1gInPc1	jazyk
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálním	oficiální	k2eAgInSc7d1	oficiální
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
moderní	moderní	k2eAgFnSc1d1	moderní
standardní	standardní	k2eAgFnSc1d1	standardní
arabština	arabština	k1gFnSc1	arabština
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hovorové	hovorový	k2eAgFnSc6d1	hovorová
formě	forma	k1gFnSc6	forma
se	se	k3xPyFc4	se
nejvíce	nejvíce	k6eAd1	nejvíce
využívají	využívat	k5eAaImIp3nP	využívat
následující	následující	k2eAgInPc4d1	následující
jazyky	jazyk	k1gInPc4	jazyk
<g/>
:	:	kIx,	:
egyptská	egyptský	k2eAgFnSc1d1	egyptská
arabština	arabština	k1gFnSc1	arabština
(	(	kIx(	(
<g/>
68	[number]	k4	68
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
saidi	said	k1gMnPc1	said
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bedáwí	bedáwí	k1gMnSc1	bedáwí
(	(	kIx(	(
<g/>
1,6	[number]	k4	1,6
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
súdánská	súdánský	k2eAgFnSc1d1	súdánská
arabština	arabština	k1gFnSc1	arabština
(	(	kIx(	(
<g/>
0,6	[number]	k4	0,6
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
domari	domari	k1gNnSc1	domari
(	(	kIx(	(
<g/>
0,3	[number]	k4	0,3
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
núbijský	núbijský	k2eAgInSc1d1	núbijský
jazyk	jazyk	k1gInSc1	jazyk
(	(	kIx(	(
<g/>
0,3	[number]	k4	0,3
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
bedža	bedža	k6eAd1	bedža
(	(	kIx(	(
<g/>
0,1	[number]	k4	0,1
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
řečtina	řečtina	k1gFnSc1	řečtina
<g/>
,	,	kIx,	,
arménština	arménština	k1gFnSc1	arménština
a	a	k8xC	a
italština	italština	k1gFnSc1	italština
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
hlavní	hlavní	k2eAgInPc1d1	hlavní
jazyky	jazyk	k1gInPc1	jazyk
početné	početný	k2eAgFnSc2d1	početná
skupiny	skupina	k1gFnSc2	skupina
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
cizí	cizí	k2eAgInPc4d1	cizí
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
vyučují	vyučovat	k5eAaImIp3nP	vyučovat
na	na	k7c6	na
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
,	,	kIx,	,
francouzština	francouzština	k1gFnSc1	francouzština
<g/>
,	,	kIx,	,
němčina	němčina	k1gFnSc1	němčina
a	a	k8xC	a
v	v	k7c6	v
menším	malý	k2eAgInSc6d2	menší
rozsahu	rozsah	k1gInSc6	rozsah
i	i	k9	i
italština	italština	k1gFnSc1	italština
<g/>
.	.	kIx.	.
</s>
<s>
Historická	historický	k2eAgFnSc1d1	historická
egyptština	egyptština	k1gFnSc1	egyptština
<g/>
,	,	kIx,	,
skládající	skládající	k2eAgInPc4d1	skládající
se	se	k3xPyFc4	se
ze	z	k7c2	z
starověké	starověký	k2eAgFnSc2d1	starověká
egyptštiny	egyptština	k1gFnSc2	egyptština
a	a	k8xC	a
koptštiny	koptština	k1gFnSc2	koptština
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
větev	větev	k1gFnSc4	větev
mezi	mezi	k7c7	mezi
rodinami	rodina	k1gFnPc7	rodina
afroasijských	afroasijský	k2eAgMnPc2d1	afroasijský
jazyků	jazyk	k1gMnPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Náboženství	náboženství	k1gNnSc2	náboženství
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
náboženstvím	náboženství	k1gNnSc7	náboženství
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
je	být	k5eAaImIp3nS	být
Islám	islám	k1gInSc4	islám
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yIgInSc3	který
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
přibližně	přibližně	k6eAd1	přibližně
90	[number]	k4	90
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Drtivou	drtivý	k2eAgFnSc4d1	drtivá
většinu	většina	k1gFnSc4	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
tvoří	tvořit	k5eAaImIp3nP	tvořit
sunnité	sunnita	k1gMnPc1	sunnita
<g/>
.	.	kIx.	.
</s>
<s>
Islám	islám	k1gInSc1	islám
se	se	k3xPyFc4	se
do	do	k7c2	do
země	zem	k1gFnSc2	zem
dostal	dostat	k5eAaPmAgMnS	dostat
s	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
arabů	arab	k1gMnPc2	arab
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
a	a	k8xC	a
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
Egypt	Egypt	k1gInSc1	Egypt
stal	stát	k5eAaPmAgInS	stát
politickým	politický	k2eAgMnSc7d1	politický
a	a	k8xC	a
kulturním	kulturní	k2eAgNnSc7d1	kulturní
centrem	centrum	k1gNnSc7	centrum
muslimského	muslimský	k2eAgInSc2d1	muslimský
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
prezidenta	prezident	k1gMnSc2	prezident
Anvara	Anvar	k1gMnSc2	Anvar
as-Sádáta	as-Sádát	k1gMnSc2	as-Sádát
se	se	k3xPyFc4	se
islám	islám	k1gInSc1	islám
stal	stát	k5eAaPmAgInS	stát
oficiálním	oficiální	k2eAgNnSc7d1	oficiální
státním	státní	k2eAgNnSc7d1	státní
náboženstvím	náboženství	k1gNnSc7	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Zbylých	zbylý	k2eAgNnPc2d1	zbylé
10	[number]	k4	10
<g/>
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
tvoří	tvořit	k5eAaImIp3nP	tvořit
křesťané	křesťan	k1gMnPc1	křesťan
<g/>
.	.	kIx.	.
90	[number]	k4	90
<g/>
%	%	kIx~	%
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nP	hlásit
ke	k	k7c3	k
koptské	koptský	k2eAgFnSc3d1	koptská
pravoslavné	pravoslavný	k2eAgFnSc3d1	pravoslavná
církvi	církev	k1gFnSc3	církev
a	a	k8xC	a
zbytek	zbytek	k1gInSc4	zbytek
ke	k	k7c3	k
koptské	koptský	k2eAgFnSc3d1	koptská
katolické	katolický	k2eAgFnSc3d1	katolická
církvi	církev	k1gFnSc3	církev
a	a	k8xC	a
egyptské	egyptský	k2eAgFnSc3d1	egyptská
evangelické	evangelický	k2eAgFnSc3d1	evangelická
církvi	církev	k1gFnSc3	církev
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgMnSc7d1	poslední
ze	z	k7c2	z
tří	tři	k4xCgNnPc2	tři
náboženství	náboženství	k1gNnPc2	náboženství
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
uznávané	uznávaný	k2eAgFnPc1d1	uznávaná
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Judaismus	judaismus	k1gInSc4	judaismus
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yRgInSc3	který
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc1	několik
stovek	stovka	k1gFnPc2	stovka
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgNnSc4d1	další
minoritní	minoritní	k2eAgNnSc4d1	minoritní
náboženství	náboženství	k1gNnSc4	náboženství
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
společně	společně	k6eAd1	společně
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
státem	stát	k1gInSc7	stát
uznané	uznaný	k2eAgNnSc1d1	uznané
<g/>
.	.	kIx.	.
</s>
<s>
Občanům	občan	k1gMnPc3	občan
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
chtěli	chtít	k5eAaImAgMnP	chtít
mít	mít	k5eAaImF	mít
takové	takový	k3xDgNnSc1	takový
náboženství	náboženství	k1gNnSc1	náboženství
napsané	napsaný	k2eAgNnSc1d1	napsané
v	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
průkazu	průkaz	k1gInSc6	průkaz
totožnosti	totožnost	k1gFnSc2	totožnost
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
vydání	vydání	k1gNnSc1	vydání
průkazu	průkaz	k1gInSc2	průkaz
odepřeno	odepřít	k5eAaPmNgNnS	odepřít
<g/>
.	.	kIx.	.
</s>
<s>
Egypťané	Egypťan	k1gMnPc1	Egypťan
tak	tak	k6eAd1	tak
byli	být	k5eAaImAgMnP	být
postaveni	postavit	k5eAaPmNgMnP	postavit
do	do	k7c2	do
situace	situace	k1gFnSc2	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
museli	muset	k5eAaImAgMnP	muset
lhát	lhát	k5eAaImF	lhát
o	o	k7c6	o
svém	svůj	k3xOyFgNnSc6	svůj
náboženství	náboženství	k1gNnSc6	náboženství
nebo	nebo	k8xC	nebo
nezískali	získat	k5eNaPmAgMnP	získat
průkaz	průkaz	k1gInSc4	průkaz
totožnosti	totožnost	k1gFnSc2	totožnost
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
zákona	zákon	k1gInSc2	zákon
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
umožnila	umožnit	k5eAaPmAgFnS	umožnit
těmto	tento	k3xDgMnPc3	tento
lidem	člověk	k1gMnPc3	člověk
získat	získat	k5eAaPmF	získat
průkaz	průkaz	k1gInSc4	průkaz
totožnosti	totožnost	k1gFnSc2	totožnost
a	a	k8xC	a
zanechat	zanechat	k5eAaPmF	zanechat
kolonku	kolonka	k1gFnSc4	kolonka
"	"	kIx"	"
<g/>
Náboženství	náboženství	k1gNnSc4	náboženství
<g/>
"	"	kIx"	"
prázdnou	prázdná	k1gFnSc4	prázdná
<g/>
.	.	kIx.	.
</s>
<s>
Egyptská	egyptský	k2eAgFnSc1d1	egyptská
kultura	kultura	k1gFnSc1	kultura
sahá	sahat	k5eAaImIp3nS	sahat
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
starší	starý	k2eAgMnSc1d2	starší
než	než	k8xS	než
šest	šest	k4xCc1	šest
tisíc	tisíc	k4xCgInPc2	tisíc
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Starověký	starověký	k2eAgInSc1d1	starověký
Egypt	Egypt	k1gInSc1	Egypt
patřil	patřit	k5eAaImAgInS	patřit
mezi	mezi	k7c4	mezi
nejstarší	starý	k2eAgFnPc4d3	nejstarší
civilizace	civilizace	k1gFnPc4	civilizace
a	a	k8xC	a
po	po	k7c4	po
celá	celý	k2eAgNnPc4d1	celé
tisíciletí	tisíciletí	k1gNnPc4	tisíciletí
si	se	k3xPyFc3	se
svoji	svůj	k3xOyFgFnSc4	svůj
kulturu	kultura	k1gFnSc4	kultura
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
mnohé	mnohé	k1gNnSc4	mnohé
kultury	kultura	k1gFnSc2	kultura
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
středního	střední	k2eAgInSc2d1	střední
východu	východ	k1gInSc2	východ
<g/>
,	,	kIx,	,
udržel	udržet	k5eAaPmAgMnS	udržet
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
éře	éra	k1gFnSc6	éra
faraonů	faraon	k1gInPc2	faraon
se	se	k3xPyFc4	se
Egypt	Egypt	k1gInSc1	Egypt
dostal	dostat	k5eAaPmAgInS	dostat
pod	pod	k7c4	pod
vliv	vliv	k1gInSc4	vliv
helénistické	helénistický	k2eAgFnSc2d1	helénistická
<g/>
,	,	kIx,	,
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
a	a	k8xC	a
islámské	islámský	k2eAgFnSc2d1	islámská
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c4	mnoho
aspektů	aspekt	k1gInPc2	aspekt
staroegyptské	staroegyptský	k2eAgFnSc2d1	staroegyptská
kultury	kultura	k1gFnSc2	kultura
propojeno	propojen	k2eAgNnSc1d1	propojeno
s	s	k7c7	s
novějšími	nový	k2eAgInPc7d2	novější
prvky	prvek	k1gInPc7	prvek
obsahujícími	obsahující	k2eAgInPc7d1	obsahující
vliv	vliv	k1gInSc4	vliv
západní	západní	k2eAgFnSc2d1	západní
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
již	již	k9	již
sama	sám	k3xTgFnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
staroegyptskou	staroegyptský	k2eAgFnSc7d1	staroegyptská
kulturou	kultura	k1gFnSc7	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
Káhira	Káhira	k1gFnSc1	Káhira
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
po	po	k7c4	po
celá	celý	k2eAgNnPc4d1	celé
staletí	staletí	k1gNnPc4	staletí
bylo	být	k5eAaImAgNnS	být
centrem	centrum	k1gNnSc7	centrum
vzdělání	vzdělání	k1gNnSc2	vzdělání
<g/>
,	,	kIx,	,
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Egypt	Egypt	k1gInSc1	Egypt
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
pyšní	pyšnit	k5eAaImIp3nS	pyšnit
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
počtem	počet	k1gInSc7	počet
laureátů	laureát	k1gMnPc2	laureát
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
arabského	arabský	k2eAgInSc2d1	arabský
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
egyptští	egyptský	k2eAgMnPc1d1	egyptský
politici	politik	k1gMnPc1	politik
byli	být	k5eAaImAgMnP	být
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
významných	významný	k2eAgFnPc2d1	významná
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
organizací	organizace	k1gFnPc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
například	například	k6eAd1	například
patří	patřit	k5eAaImIp3nS	patřit
Butrus	Butrus	k1gMnSc1	Butrus
Butrus-Ghálí	Butrus-Ghálý	k2eAgMnPc1d1	Butrus-Ghálý
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
generální	generální	k2eAgMnSc1d1	generální
tajemník	tajemník	k1gMnSc1	tajemník
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
či	či	k8xC	či
Muhammad	Muhammad	k1gInSc1	Muhammad
Baradej	Baradej	k1gFnSc2	Baradej
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
agentury	agentura	k1gFnSc2	agentura
pro	pro	k7c4	pro
atomovou	atomový	k2eAgFnSc4d1	atomová
energii	energie	k1gFnSc4	energie
a	a	k8xC	a
držitel	držitel	k1gMnSc1	držitel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
<s>
Egypt	Egypt	k1gInSc1	Egypt
má	mít	k5eAaImIp3nS	mít
rovněž	rovněž	k9	rovněž
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
v	v	k7c6	v
arabsky	arabsky	k6eAd1	arabsky
mluvících	mluvící	k2eAgFnPc6d1	mluvící
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnSc1d1	moderní
arabská	arabský	k2eAgFnSc1d1	arabská
kultura	kultura	k1gFnSc1	kultura
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
egyptskou	egyptský	k2eAgFnSc7d1	egyptská
literaturou	literatura	k1gFnSc7	literatura
<g/>
,	,	kIx,	,
hudbou	hudba	k1gFnSc7	hudba
a	a	k8xC	a
kinematografií	kinematografie	k1gFnSc7	kinematografie
<g/>
.	.	kIx.	.
</s>
<s>
Egypťané	Egypťan	k1gMnPc1	Egypťan
byli	být	k5eAaImAgMnP	být
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
civilizací	civilizace	k1gFnPc2	civilizace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
začlenila	začlenit	k5eAaPmAgFnS	začlenit
designové	designový	k2eAgInPc4d1	designový
prvky	prvek	k1gInPc4	prvek
do	do	k7c2	do
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Egyptská	egyptský	k2eAgFnSc1d1	egyptská
civilizace	civilizace	k1gFnSc1	civilizace
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
kolosální	kolosální	k2eAgInPc4d1	kolosální
pyramidy	pyramid	k1gInPc4	pyramid
<g/>
,	,	kIx,	,
chrámy	chrám	k1gInPc4	chrám
a	a	k8xC	a
monumentální	monumentální	k2eAgFnPc4d1	monumentální
hrobky	hrobka	k1gFnPc4	hrobka
<g/>
.	.	kIx.	.
</s>
<s>
Známými	známý	k2eAgInPc7d1	známý
příklady	příklad	k1gInPc7	příklad
jsou	být	k5eAaImIp3nP	být
Džoserova	Džoserův	k2eAgFnSc1d1	Džoserova
stupňovitá	stupňovitý	k2eAgFnSc1d1	stupňovitá
pyramida	pyramida	k1gFnSc1	pyramida
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
navržena	navrhnout	k5eAaPmNgFnS	navrhnout
starověkým	starověký	k2eAgInSc7d1	starověký
architektem	architekt	k1gMnSc7	architekt
Imhotepem	Imhotep	k1gInSc7	Imhotep
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
sfinga	sfinga	k1gFnSc1	sfinga
v	v	k7c6	v
Gíze	Gíze	k1gNnSc6	Gíze
a	a	k8xC	a
chrám	chrám	k1gInSc1	chrám
v	v	k7c6	v
Abú	abú	k1gMnSc6	abú
Simbelu	Simbel	k1gMnSc6	Simbel
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgNnSc1d1	moderní
egyptské	egyptský	k2eAgNnSc1d1	egyptské
umění	umění	k1gNnSc1	umění
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
různorodé	různorodý	k2eAgNnSc1d1	různorodé
<g/>
.	.	kIx.	.
</s>
<s>
Významnými	významný	k2eAgMnPc7d1	významný
umělci	umělec	k1gMnPc7	umělec
byli	být	k5eAaImAgMnP	být
například	například	k6eAd1	například
architekti	architekt	k1gMnPc1	architekt
Hassan	Hassana	k1gFnPc2	Hassana
Fathy	Fatha	k1gFnSc2	Fatha
a	a	k8xC	a
Ramses	Ramses	k1gInSc1	Ramses
Wissa	Wissa	k1gFnSc1	Wissa
Wassef	Wassef	k1gMnSc1	Wassef
<g/>
,	,	kIx,	,
sochař	sochař	k1gMnSc1	sochař
Mahmúd	Mahmúd	k1gMnSc1	Mahmúd
Mochtar	Mochtar	k1gMnSc1	Mochtar
či	či	k8xC	či
Izák	Izák	k1gMnSc1	Izák
Fanús	Fanús	k1gInSc1	Fanús
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
koptským	koptský	k2eAgNnSc7d1	Koptské
uměním	umění	k1gNnSc7	umění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
egyptském	egyptský	k2eAgNnSc6d1	egyptské
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
nachází	nacházet	k5eAaImIp3nS	nacházet
Káhirská	káhirský	k2eAgFnSc1d1	Káhirská
opera	opera	k1gFnSc1	opera
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
centrem	centr	k1gMnSc7	centr
egyptského	egyptský	k2eAgNnSc2d1	egyptské
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Egyptské	egyptský	k2eAgNnSc1d1	egyptské
umění	umění	k1gNnSc1	umění
a	a	k8xC	a
filmový	filmový	k2eAgInSc1d1	filmový
průmysl	průmysl	k1gInSc1	průmysl
vzkvétá	vzkvétat	k5eAaImIp3nS	vzkvétat
od	od	k7c2	od
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
přes	přes	k7c4	přes
třicet	třicet	k4xCc4	třicet
satelitních	satelitní	k2eAgInPc2d1	satelitní
kanálů	kanál	k1gInPc2	kanál
a	a	k8xC	a
přes	přes	k7c4	přes
sto	sto	k4xCgNnSc4	sto
natočených	natočený	k2eAgMnPc2d1	natočený
filmů	film	k1gInPc2	film
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Káhira	Káhira	k1gFnSc1	Káhira
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
dlouho	dlouho	k6eAd1	dlouho
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Holywood	Holywood	k1gInSc1	Holywood
středního	střední	k2eAgInSc2d1	střední
východu	východ	k1gInSc2	východ
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Hostí	hostit	k5eAaImIp3nS	hostit
každoroční	každoroční	k2eAgInSc1d1	každoroční
Káhirský	káhirský	k2eAgInSc1d1	káhirský
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
filmový	filmový	k2eAgInSc1d1	filmový
festival	festival	k1gInSc1	festival
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
hodnocen	hodnotit	k5eAaImNgMnS	hodnotit
jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
jedenácti	jedenáct	k4xCc2	jedenáct
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
filmových	filmový	k2eAgInPc2d1	filmový
festivalů	festival	k1gInPc2	festival
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgMnPc4d3	nejznámější
egyptské	egyptský	k2eAgMnPc4d1	egyptský
herce	herec	k1gMnPc4	herec
patří	patřit	k5eAaImIp3nS	patřit
Omar	Omar	k1gMnSc1	Omar
Sharif	Sharif	k1gMnSc1	Sharif
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Egyptská	egyptský	k2eAgFnSc1d1	egyptská
literatura	literatura	k1gFnSc1	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Literatura	literatura	k1gFnSc1	literatura
tvoří	tvořit	k5eAaImIp3nS	tvořit
důležitý	důležitý	k2eAgInSc4d1	důležitý
kulturní	kulturní	k2eAgInSc4d1	kulturní
prvek	prvek	k1gInSc4	prvek
v	v	k7c6	v
životě	život	k1gInSc6	život
mnoha	mnoho	k4c2	mnoho
egypťanů	egypťan	k1gMnPc2	egypťan
<g/>
.	.	kIx.	.
</s>
<s>
Egyptští	egyptský	k2eAgMnPc1d1	egyptský
spisovatelé	spisovatel	k1gMnPc1	spisovatel
a	a	k8xC	a
básníci	básník	k1gMnPc1	básník
byli	být	k5eAaImAgMnP	být
mezi	mezi	k7c7	mezi
prvními	první	k4xOgFnPc7	první
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
experimentovali	experimentovat	k5eAaImAgMnP	experimentovat
s	s	k7c7	s
moderním	moderní	k2eAgInSc7d1	moderní
stylem	styl	k1gInSc7	styl
arabské	arabský	k2eAgFnSc2d1	arabská
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
styl	styl	k1gInSc1	styl
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
středním	střední	k2eAgInSc6d1	střední
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Egyptský	egyptský	k2eAgMnSc1d1	egyptský
spisovatel	spisovatel	k1gMnSc1	spisovatel
Nadžíb	Nadžíb	k1gMnSc1	Nadžíb
Mahfúz	Mahfúz	k1gMnSc1	Mahfúz
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
arabským	arabský	k2eAgMnSc7d1	arabský
spisovatelem	spisovatel	k1gMnSc7	spisovatel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
egypťany	egypťan	k1gMnPc4	egypťan
je	být	k5eAaImIp3nS	být
nejoblíbenějším	oblíbený	k2eAgInSc7d3	nejoblíbenější
literárním	literární	k2eAgInSc7d1	literární
žánrem	žánr	k1gInSc7	žánr
lidová	lidový	k2eAgFnSc1d1	lidová
poezie	poezie	k1gFnSc1	poezie
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Egyptská	egyptský	k2eAgFnSc1d1	egyptská
hudba	hudba	k1gFnSc1	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Egyptská	egyptský	k2eAgFnSc1d1	egyptská
hudba	hudba	k1gFnSc1	hudba
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
místními	místní	k2eAgInPc7d1	místní
<g/>
,	,	kIx,	,
středomořskými	středomořský	k2eAgInPc7d1	středomořský
<g/>
,	,	kIx,	,
africkými	africký	k2eAgInPc7d1	africký
a	a	k8xC	a
západními	západní	k2eAgInPc7d1	západní
prvky	prvek	k1gInPc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
egypťané	egypťan	k1gMnPc1	egypťan
používali	používat	k5eAaImAgMnP	používat
mnohé	mnohý	k2eAgInPc4d1	mnohý
hudební	hudební	k2eAgInPc4d1	hudební
nástroje	nástroj	k1gInPc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
však	však	k9	však
harfu	harfa	k1gFnSc4	harfa
<g/>
,	,	kIx,	,
flétnu	flétna	k1gFnSc4	flétna
a	a	k8xC	a
místních	místní	k2eAgMnPc2d1	místní
nástroje	nástroj	k1gInPc4	nástroj
ney	ney	k?	ney
a	a	k8xC	a
úd	úd	k1gInSc1	úd
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	s	k7c7	s
součástí	součást	k1gFnSc7	součást
hudebního	hudební	k2eAgInSc2d1	hudební
projevu	projev	k1gInSc2	projev
stal	stát	k5eAaPmAgInS	stát
zpěv	zpěv	k1gInSc1	zpěv
a	a	k8xC	a
bicí	bicí	k2eAgInPc1d1	bicí
nástroje	nástroj	k1gInPc1	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
egyptská	egyptský	k2eAgFnSc1d1	egyptská
hudba	hudba	k1gFnSc1	hudba
je	být	k5eAaImIp3nS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
lidovou	lidový	k2eAgFnSc7d1	lidová
tvorbou	tvorba	k1gFnSc7	tvorba
a	a	k8xC	a
umělci	umělec	k1gMnPc1	umělec
jako	jako	k9	jako
byli	být	k5eAaImAgMnP	být
například	například	k6eAd1	například
Sajid	Sajid	k1gInSc4	Sajid
Darwíš	Darwíš	k1gFnSc2	Darwíš
<g/>
,	,	kIx,	,
Umm	Umm	k1gFnSc1	Umm
Kulthum	Kulthum	k1gInSc1	Kulthum
<g/>
,	,	kIx,	,
Mohamed	Mohamed	k1gMnSc1	Mohamed
Abdel	Abdel	k1gMnSc1	Abdel
Wahab	Wahab	k1gMnSc1	Wahab
či	či	k8xC	či
Abd	Abd	k1gMnSc1	Abd
el-Halim	el-Halim	k1gMnSc1	el-Halim
Hafez	Hafez	k1gMnSc1	Hafez
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
egyptské	egyptský	k2eAgFnSc2d1	egyptská
hudební	hudební	k2eAgFnSc2d1	hudební
scény	scéna	k1gFnSc2	scéna
stává	stávat	k5eAaImIp3nS	stávat
pop	pop	k1gMnSc1	pop
music	music	k1gMnSc1	music
<g/>
.	.	kIx.	.
</s>
<s>
Egyptská	egyptský	k2eAgFnSc1d1	egyptská
lidová	lidový	k2eAgFnSc1d1	lidová
hudba	hudba	k1gFnSc1	hudba
se	se	k3xPyFc4	se
i	i	k9	i
nadále	nadále	k6eAd1	nadále
hrává	hrávat	k5eAaImIp3nS	hrávat
na	na	k7c6	na
svatbách	svatba	k1gFnPc6	svatba
či	či	k8xC	či
jiných	jiný	k2eAgFnPc6d1	jiná
tradičních	tradiční	k2eAgFnPc6d1	tradiční
slavnostech	slavnost	k1gFnPc6	slavnost
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejpopulárnější	populární	k2eAgMnPc4d3	nejpopulárnější
zpěváky	zpěvák	k1gMnPc4	zpěvák
současnosti	současnost	k1gFnSc2	současnost
patří	patřit	k5eAaImIp3nS	patřit
Muhammad	Muhammad	k1gInSc4	Muhammad
Munir	Munir	k1gMnSc1	Munir
a	a	k8xC	a
Amr	Amr	k1gMnSc1	Amr
Dijáb	Dijáb	k1gMnSc1	Dijáb
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Fotbal	fotbal	k1gInSc1	fotbal
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Nejpopulárnějším	populární	k2eAgInSc7d3	nejpopulárnější
sportem	sport	k1gInSc7	sport
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
je	být	k5eAaImIp3nS	být
fotbal	fotbal	k1gInSc1	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejpopulárnější	populární	k2eAgInPc4d3	nejpopulárnější
týmy	tým	k1gInPc4	tým
patří	patřit	k5eAaImIp3nS	patřit
kluby	klub	k1gInPc7	klub
Al	ala	k1gFnPc2	ala
Ahly	Ahl	k2eAgFnPc4d1	Ahl
<g/>
,	,	kIx,	,
Zamalek	Zamalek	k1gInSc1	Zamalek
<g/>
,	,	kIx,	,
Ismaily	Ismail	k1gInPc1	Ismail
<g/>
,	,	kIx,	,
Al-Ittihad	Al-Ittihad	k1gInSc1	Al-Ittihad
Alexandrie	Alexandrie	k1gFnSc2	Alexandrie
a	a	k8xC	a
Al	ala	k1gFnPc2	ala
Masry	Masra	k1gFnSc2	Masra
<g/>
.	.	kIx.	.
</s>
<s>
Káhirské	káhirský	k2eAgNnSc1d1	Káhirské
derby	derby	k1gNnSc1	derby
mezi	mezi	k7c7	mezi
týmem	tým	k1gInSc7	tým
Al	ala	k1gFnPc2	ala
Ahly	Ahla	k1gFnPc4	Ahla
a	a	k8xC	a
Zamalkem	Zamalek	k1gInSc7	Zamalek
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
nejbouřlivější	bouřlivý	k2eAgNnPc4d3	nejbouřlivější
africká	africký	k2eAgNnPc4d1	africké
a	a	k8xC	a
světová	světový	k2eAgNnPc4d1	světové
derby	derby	k1gNnPc4	derby
<g/>
.	.	kIx.	.
</s>
<s>
Egyptská	egyptský	k2eAgFnSc1d1	egyptská
fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
reprezentace	reprezentace	k1gFnSc1	reprezentace
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejúspěšnější	úspěšný	k2eAgInPc4d3	nejúspěšnější
africké	africký	k2eAgInPc4d1	africký
celky	celek	k1gInPc4	celek
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgFnPc4	svůj
kvality	kvalita	k1gFnPc4	kvalita
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
především	především	k9	především
na	na	k7c6	na
africkém	africký	k2eAgInSc6d1	africký
kontinentu	kontinent	k1gInSc6	kontinent
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
již	již	k6eAd1	již
sedmkrát	sedmkrát	k6eAd1	sedmkrát
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
Africký	africký	k2eAgInSc4d1	africký
pohár	pohár	k1gInSc4	pohár
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1957	[number]	k4	1957
a	a	k8xC	a
1959	[number]	k4	1959
ho	on	k3xPp3gMnSc2	on
získala	získat	k5eAaPmAgFnS	získat
dvakrát	dvakrát	k6eAd1	dvakrát
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
a	a	k8xC	a
2010	[number]	k4	2010
dokonce	dokonce	k9	dokonce
třikrát	třikrát	k6eAd1	třikrát
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
nový	nový	k2eAgInSc4d1	nový
světový	světový	k2eAgInSc4d1	světový
rekord	rekord	k1gInSc4	rekord
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k1gNnPc7	další
velmi	velmi	k6eAd1	velmi
populárními	populární	k2eAgInPc7d1	populární
sporty	sport	k1gInPc7	sport
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
jsou	být	k5eAaImIp3nP	být
squash	squash	k1gInSc4	squash
a	a	k8xC	a
tenis	tenis	k1gInSc4	tenis
<g/>
.	.	kIx.	.
</s>
<s>
Egyptský	egyptský	k2eAgInSc1d1	egyptský
squashový	squashový	k2eAgInSc1d1	squashový
tým	tým	k1gInSc1	tým
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
hráčem	hráč	k1gMnSc7	hráč
je	být	k5eAaImIp3nS	být
Amr	Amr	k1gMnSc1	Amr
Šabana	Šaban	k1gMnSc2	Šaban
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
2003	[number]	k4	2003
až	až	k9	až
2009	[number]	k4	2009
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
světový	světový	k2eAgInSc1d1	světový
šampionát	šampionát	k1gInSc1	šampionát
ve	v	k7c6	v
squashi	squash	k1gInSc6	squash
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
africkými	africký	k2eAgInPc7d1	africký
basketbalovými	basketbalový	k2eAgInPc7d1	basketbalový
týmy	tým	k1gInPc7	tým
je	být	k5eAaImIp3nS	být
egyptský	egyptský	k2eAgInSc1d1	egyptský
národní	národní	k2eAgInSc1d1	národní
tým	tým	k1gInSc1	tým
historicky	historicky	k6eAd1	historicky
nejúspěšnější	úspěšný	k2eAgInSc1d3	nejúspěšnější
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
výkonů	výkon	k1gInPc2	výkon
na	na	k7c4	na
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
a	a	k8xC	a
Olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
basketbalovém	basketbalový	k2eAgNnSc6d1	basketbalové
mistrovství	mistrovství	k1gNnSc6	mistrovství
Afriky	Afrika	k1gFnSc2	Afrika
již	již	k6eAd1	již
získali	získat	k5eAaPmAgMnP	získat
rekordních	rekordní	k2eAgInPc2d1	rekordní
šestnáct	šestnáct	k4xCc4	šestnáct
medailí	medaile	k1gFnPc2	medaile
včetně	včetně	k7c2	včetně
pěti	pět	k4xCc2	pět
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
je	být	k5eAaImIp3nS	být
i	i	k9	i
házenkářská	házenkářský	k2eAgFnSc1d1	házenkářská
reprezentace	reprezentace	k1gFnSc1	reprezentace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
z	z	k7c2	z
osmnácti	osmnáct	k4xCc2	osmnáct
afrických	africký	k2eAgInPc2d1	africký
šampionátů	šampionát	k1gInPc2	šampionát
získala	získat	k5eAaPmAgFnS	získat
šestnáct	šestnáct	k4xCc4	šestnáct
medailí	medaile	k1gFnPc2	medaile
včetně	včetně	k7c2	včetně
pěti	pět	k4xCc2	pět
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
se	se	k3xPyFc4	se
Omar	Omar	k1gMnSc1	Omar
Samra	Samra	k1gMnSc1	Samra
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
Egypťanem	Egypťan	k1gMnSc7	Egypťan
a	a	k8xC	a
nejmladším	mladý	k2eAgMnSc7d3	nejmladší
Arabem	Arab	k1gMnSc7	Arab
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
na	na	k7c4	na
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
horu	hora	k1gFnSc4	hora
světa	svět	k1gInSc2	svět
Mount	Mount	k1gInSc1	Mount
Everest	Everest	k1gInSc1	Everest
<g/>
.	.	kIx.	.
</s>
<s>
Expedice	expedice	k1gFnSc1	expedice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
vedena	vést	k5eAaImNgFnS	vést
jižní	jižní	k2eAgFnSc7d1	jižní
cestou	cesta	k1gFnSc7	cesta
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2007	[number]	k4	2007
a	a	k8xC	a
skončila	skončit	k5eAaPmAgFnS	skončit
o	o	k7c4	o
devět	devět	k4xCc4	devět
týdnů	týden	k1gInPc2	týden
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
