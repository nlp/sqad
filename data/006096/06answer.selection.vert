<s>
Egypt	Egypt	k1gInSc1	Egypt
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
Egyptská	egyptský	k2eAgFnSc1d1	egyptská
arabská	arabský	k2eAgFnSc1d1	arabská
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
EAR	EAR	kA	EAR
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ve	v	k7c6	v
staré	starý	k2eAgFnSc6d1	stará
češtině	čeština	k1gFnSc6	čeština
Ejipt	Ejipt	k1gInSc1	Ejipt
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
arabská	arabský	k2eAgFnSc1d1	arabská
republika	republika	k1gFnSc1	republika
v	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
Africe	Afrika	k1gFnSc6	Afrika
(	(	kIx(	(
<g/>
malou	malý	k2eAgFnSc7d1	malá
částí	část	k1gFnSc7	část
též	též	k9	též
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc1d1	ležící
na	na	k7c6	na
Nilu	Nil	k1gInSc6	Nil
<g/>
.	.	kIx.	.
</s>
