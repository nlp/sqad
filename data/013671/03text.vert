<s>
Rotný	rotný	k1gMnSc1
</s>
<s>
Hodnostní	hodnostní	k2eAgNnSc1d1
označení	označení	k1gNnSc1
rotného	rotný	k1gMnSc2
(	(	kIx(
<g/>
OR-	OR-	k1gFnSc1
<g/>
4	#num#	k4
<g/>
)	)	kIx)
Armády	armáda	k1gFnSc2
ČR	ČR	kA
</s>
<s>
Rotný	rotný	k1gMnSc1
(	(	kIx(
<g/>
rtn.	rtn.	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgFnSc1d3
poddůstojnická	poddůstojnický	k2eAgFnSc1d1
hodnost	hodnost	k1gFnSc1
AČR	AČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
hodnostním	hodnostní	k2eAgNnSc6d1
kódování	kódování	k1gNnSc6
NATO	NATO	kA
odpovídá	odpovídat	k5eAaImIp3nS
OR-	OR-	k1gFnSc4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejbližší	blízký	k2eAgFnSc7d3
nižší	nízký	k2eAgFnSc7d2
hodností	hodnost	k1gFnSc7
je	být	k5eAaImIp3nS
četař	četař	k1gMnSc1
a	a	k8xC
nejbližší	blízký	k2eAgFnSc7d3
vyšší	vysoký	k2eAgFnSc7d2
hodností	hodnost	k1gFnSc7
je	být	k5eAaImIp3nS
rotmistr	rotmistr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
americké	americký	k2eAgFnSc6d1
armádě	armáda	k1gFnSc6
odpovídá	odpovídat	k5eAaImIp3nS
hodnosti	hodnost	k1gFnPc4
„	„	k?
<g/>
Staff	Staff	k1gMnSc1
Sergeant	Sergeant	k1gMnSc1
<g/>
”	”	k?
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
rotný	rotný	k1gMnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
