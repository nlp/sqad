<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
současným	současný	k2eAgInSc7d1	současný
mrakodrapem	mrakodrap	k1gInSc7	mrakodrap
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
stavbou	stavba	k1gFnSc7	stavba
světa	svět	k1gInSc2	svět
je	být	k5eAaImIp3nS	být
Burdž	Burdž	k1gFnSc1	Burdž
Chalífa	chalífa	k1gMnSc1	chalífa
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
arabských	arabský	k2eAgInPc6d1	arabský
emirátech	emirát	k1gInPc6	emirát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
vysoký	vysoký	k2eAgInSc1d1	vysoký
828	[number]	k4	828
m	m	kA	m
a	a	k8xC	a
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
otevřen	otevřen	k2eAgInSc1d1	otevřen
dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
