<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
atletických	atletický	k2eAgFnPc2d1
federací	federace	k1gFnPc2
(	(	kIx(
<g/>
IAAF	IAAF	kA
anglicky	anglicky	k6eAd1
<g/>
:	:	kIx,
International	International	k1gFnSc1
Association	Association	k1gInSc1
of	of	k?
Athletics	Athletics	k1gInSc1
Federations	Federations	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgFnSc7d3
světovou	světový	k2eAgFnSc7d1
organizací	organizace	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
řídí	řídit	k5eAaImIp3nS
atletiku	atletika	k1gFnSc4
<g/>
.	.	kIx.
</s>