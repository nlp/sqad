<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
atletických	atletický	k2eAgFnPc2d1
federací	federace	k1gFnPc2
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
atletických	atletický	k2eAgFnPc2d1
federací	federace	k1gFnPc2
Zkratka	zkratka	k1gFnSc1
</s>
<s>
IAAF	IAAF	kA
Vznik	vznik	k1gInSc1
</s>
<s>
1912	#num#	k4
Typ	typ	k1gInSc1
</s>
<s>
mezinárodní	mezinárodní	k2eAgFnSc1d1
sportovní	sportovní	k2eAgFnSc1d1
federace	federace	k1gFnSc1
Účel	účel	k1gInSc1
</s>
<s>
atletika	atletika	k1gFnSc1
Sídlo	sídlo	k1gNnSc1
</s>
<s>
Monako	Monako	k1gNnSc1
Monako	Monako	k1gNnSc1
Místo	místo	k7c2
</s>
<s>
Monaco-Ville	Monaco-Ville	k1gFnSc1
Působnost	působnost	k1gFnSc1
</s>
<s>
celosvětová	celosvětový	k2eAgFnSc1d1
Členové	člen	k1gMnPc1
</s>
<s>
214	#num#	k4
Prezident	prezident	k1gMnSc1
</s>
<s>
Sebastian	Sebastian	k1gMnSc1
Coe	Coe	k1gMnSc1
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Přidružení	přidružení	k1gNnSc2
</s>
<s>
MOV	MOV	kA
<g/>
,	,	kIx,
ASOIF	ASOIF	kA
<g/>
,	,	kIx,
SportAccord	SportAccord	k1gInSc1
<g/>
,	,	kIx,
Český	český	k2eAgInSc1d1
atletický	atletický	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Oficiální	oficiální	k2eAgInSc4d1
web	web	k1gInSc4
</s>
<s>
www.iaaf.org	www.iaaf.org	k1gInSc1
Dřívější	dřívější	k2eAgInSc4d1
název	název	k1gInSc4
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
amatérská	amatérský	k2eAgFnSc1d1
atletická	atletický	k2eAgFnSc1d1
federace	federace	k1gFnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
atletických	atletický	k2eAgFnPc2d1
federací	federace	k1gFnPc2
(	(	kIx(
<g/>
IAAF	IAAF	kA
anglicky	anglicky	k6eAd1
<g/>
:	:	kIx,
International	International	k1gFnSc1
Association	Association	k1gInSc1
of	of	k?
Athletics	Athletics	k1gInSc1
Federations	Federations	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgFnSc7d3
světovou	světový	k2eAgFnSc7d1
organizací	organizace	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
řídí	řídit	k5eAaImIp3nS
atletiku	atletika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1912	#num#	k4
sedmnácti	sedmnáct	k4xCc2
národními	národní	k2eAgFnPc7d1
federacemi	federace	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
bylo	být	k5eAaImAgNnS
členy	člen	k1gMnPc4
IAAF	IAAF	kA
214	#num#	k4
národních	národní	k2eAgFnPc2d1
federací	federace	k1gFnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
Českého	český	k2eAgInSc2d1
atletického	atletický	k2eAgInSc2d1
svazu	svaz	k1gInSc2
(	(	kIx(
<g/>
ČAS	čas	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgInSc1d1
název	název	k1gInSc1
byl	být	k5eAaImAgInS
International	International	k1gFnSc4
Amateur	Amateura	k1gFnPc2
Athletics	Athleticsa	k1gFnPc2
Federation	Federation	k1gInSc1
—	—	k?
Mezinárodní	mezinárodní	k2eAgFnSc1d1
amatérská	amatérský	k2eAgFnSc1d1
atletická	atletický	k2eAgFnSc1d1
federace	federace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
slovo	slovo	k1gNnSc1
"	"	kIx"
<g/>
amatérský	amatérský	k2eAgInSc1d1
<g/>
"	"	kIx"
na	na	k7c6
konci	konec	k1gInSc6
dvacátého	dvacátý	k4xOgNnSc2
století	století	k1gNnSc2
pozbylo	pozbýt	k5eAaPmAgNnS
svého	svůj	k3xOyFgInSc2
původního	původní	k2eAgInSc2d1
významu	význam	k1gInSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
hledán	hledán	k2eAgInSc1d1
nový	nový	k2eAgInSc1d1
název	název	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
by	by	kYmCp3nS
ovšem	ovšem	k9
zachoval	zachovat	k5eAaPmAgInS
původní	původní	k2eAgFnSc4d1
zkratku	zkratka	k1gFnSc4
—	—	k?
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
byl	být	k5eAaImAgInS
přijat	přijmout	k5eAaPmNgInS
dnešní	dnešní	k2eAgInSc1d1
název	název	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Členem	člen	k1gMnSc7
zastřešující	zastřešující	k2eAgFnSc2d1
sportovní	sportovní	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
SportAccord	SportAccorda	k1gFnPc2
je	být	k5eAaImIp3nS
od	od	k7c2
20	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2019	#num#	k4
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
World	World	k1gInSc1
Athletics	Athletics	k1gInSc1
(	(	kIx(
<g/>
Světová	světový	k2eAgFnSc1d1
atletika	atletika	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Organizace	organizace	k1gFnSc1
</s>
<s>
Prezidenti	prezident	k1gMnPc1
IAAF	IAAF	kA
</s>
<s>
Jméno	jméno	k1gNnSc1
</s>
<s>
Země	země	k1gFnSc1
</s>
<s>
Doba	doba	k1gFnSc1
v	v	k7c6
úřadu	úřad	k1gInSc6
</s>
<s>
Sigfrid	Sigfrid	k1gInSc1
EdströmŠvédsko	EdströmŠvédsko	k1gNnSc4
Švédsko	Švédsko	k1gNnSc1
<g/>
1912	#num#	k4
<g/>
–	–	k?
<g/>
1946	#num#	k4
</s>
<s>
Lord	lord	k1gMnSc1
Burghley	Burghlea	k1gFnSc2
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
1946	#num#	k4
<g/>
–	–	k?
<g/>
1976	#num#	k4
</s>
<s>
Adriaan	Adriaan	k1gMnSc1
PaulenNizozemsko	PaulenNizozemsko	k1gNnSc4
Nizozemsko	Nizozemsko	k1gNnSc1
<g/>
1976	#num#	k4
<g/>
–	–	k?
<g/>
1981	#num#	k4
</s>
<s>
Primo	primo	k1gNnSc1
NebioloItálie	NebioloItálie	k1gFnSc2
Itálie	Itálie	k1gFnSc2
<g/>
1981	#num#	k4
<g/>
–	–	k?
<g/>
1999	#num#	k4
</s>
<s>
Lamine	Laminout	k5eAaPmIp3nS
DiackSenegal	DiackSenegal	k1gInSc1
Senegal	Senegal	k1gInSc1
<g/>
1999	#num#	k4
<g/>
–	–	k?
<g/>
2015	#num#	k4
</s>
<s>
Sebastian	Sebastian	k1gMnSc1
Coe	Coe	k1gMnSc1
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Spojené	spojený	k2eAgInPc1d1
královstvíod	královstvíod	k1gInSc4
2015	#num#	k4
</s>
<s>
Sídlo	sídlo	k1gNnSc1
IAAF	IAAF	kA
</s>
<s>
Do	do	k7c2
roku	rok	k1gInSc2
1994	#num#	k4
byl	být	k5eAaImAgMnS
sídlem	sídlo	k1gNnSc7
IAAF	IAAF	kA
Londýn	Londýn	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1994	#num#	k4
organizace	organizace	k1gFnSc1
přesídlila	přesídlit	k5eAaPmAgFnS
do	do	k7c2
Monaka	Monako	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
její	její	k3xOp3gInPc1
orgány	orgán	k1gInPc1
10.6	10.6	k4
<g/>
.1994	.1994	k4
nastěhovaly	nastěhovat	k5eAaPmAgFnP
do	do	k7c2
tamní	tamní	k2eAgFnSc2d1
vily	vila	k1gFnSc2
Miraflores	Mirafloresa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Členové	člen	k1gMnPc1
</s>
<s>
Český	český	k2eAgInSc1d1
atletický	atletický	k2eAgInSc1d1
svaz	svaz	k1gInSc1
</s>
<s>
Kontinentální	kontinentální	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
</s>
<s>
Mapa	mapa	k1gFnSc1
se	s	k7c7
šesti	šest	k4xCc7
kontinentálními	kontinentální	k2eAgFnPc7d1
členskými	členský	k2eAgFnPc7d1
federacemi	federace	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
AAA	AAA	kA
–	–	k?
Asijská	asijský	k2eAgFnSc1d1
atletická	atletický	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
v	v	k7c6
Asii	Asie	k1gFnSc6
</s>
<s>
CAA	CAA	kA
–	–	k?
Africká	africký	k2eAgFnSc1d1
atletická	atletický	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
v	v	k7c6
Africe	Afrika	k1gFnSc6
</s>
<s>
CONSUDATLE	CONSUDATLE	kA
–	–	k?
Konfederace	konfederace	k1gFnSc2
jihoamerické	jihoamerický	k2eAgFnSc2d1
atletiky	atletika	k1gFnSc2
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
</s>
<s>
EAA	EAA	kA
–	–	k?
Evropská	evropský	k2eAgFnSc1d1
atletická	atletický	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
</s>
<s>
NACACAA	NACACAA	kA
–	–	k?
Severoamerická	severoamerický	k2eAgFnSc1d1
a	a	k8xC
středoamerická	středoamerický	k2eAgFnSc1d1
atletická	atletický	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
a	a	k8xC
Střední	střední	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
</s>
<s>
OAA	OAA	kA
–	–	k?
Atletická	atletický	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
Oceánie	Oceánie	k1gFnSc2
v	v	k7c6
Oceánii	Oceánie	k1gFnSc6
</s>
<s>
Soutěže	soutěž	k1gFnPc1
</s>
<s>
IAAF	IAAF	kA
je	být	k5eAaImIp3nS
zastřešujícím	zastřešující	k2eAgMnSc7d1
pořadatelem	pořadatel	k1gMnSc7
několika	několik	k4yIc2
největších	veliký	k2eAgFnPc2d3
světových	světový	k2eAgFnPc2d1
atletických	atletický	k2eAgFnPc2d1
soutěží	soutěž	k1gFnPc2
<g/>
,	,	kIx,
mezi	mezi	k7c4
něž	jenž	k3xRgFnPc4
patří	patřit	k5eAaImIp3nS
především	především	k6eAd1
<g/>
:	:	kIx,
</s>
<s>
Soutěž	soutěž	k1gFnSc1
</s>
<s>
Frekvence	frekvence	k1gFnSc1
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
atletice	atletika	k1gFnSc6
</s>
<s>
Každé	každý	k3xTgInPc4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
</s>
<s>
Halové	halový	k2eAgNnSc1d1
mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
atletice	atletika	k1gFnSc6
</s>
<s>
Každé	každý	k3xTgInPc4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
přespolním	přespolní	k2eAgInSc6d1
běhu	běh	k1gInSc6
</s>
<s>
Každý	každý	k3xTgInSc4
rok	rok	k1gInSc4
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
půlmaratonu	půlmaraton	k1gInSc6
</s>
<s>
Každé	každý	k3xTgInPc4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
atletice	atletika	k1gFnSc6
do	do	k7c2
17	#num#	k4
let	léto	k1gNnPc2
</s>
<s>
Každé	každý	k3xTgInPc4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
juniorů	junior	k1gMnPc2
v	v	k7c6
atletice	atletika	k1gFnSc6
</s>
<s>
Každé	každý	k3xTgInPc4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
</s>
<s>
Světový	světový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
v	v	k7c6
chůzi	chůze	k1gFnSc6
</s>
<s>
Každé	každý	k3xTgInPc4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
</s>
<s>
Světový	světový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
v	v	k7c6
atletice	atletika	k1gFnSc6
</s>
<s>
Každé	každý	k3xTgInPc4
čtyři	čtyři	k4xCgInPc4
roky	rok	k1gInPc4
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
Každý	každý	k3xTgInSc4
rok	rok	k1gInSc4
</s>
<s>
Světové	světový	k2eAgFnSc3d1
atletické	atletický	k2eAgFnSc3d1
finále	finála	k1gFnSc3
</s>
<s>
Každý	každý	k3xTgInSc4
rok	rok	k1gInSc4
</s>
<s>
Atlet	atlet	k1gMnSc1
světa	svět	k1gInSc2
IAAF	IAAF	kA
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1988	#num#	k4
vyhlašuje	vyhlašovat	k5eAaImIp3nS
IAAF	IAAF	kA
každoročně	každoročně	k6eAd1
v	v	k7c6
mužské	mužský	k2eAgFnSc6d1
i	i	k8xC
ženské	ženský	k2eAgFnSc6d1
kategorii	kategorie	k1gFnSc6
nejlepšího	dobrý	k2eAgMnSc4d3
atleta	atlet	k1gMnSc4
a	a	k8xC
atletku	atletka	k1gFnSc4
sezóny	sezóna	k1gFnSc2
(	(	kIx(
<g/>
IAAF	IAAF	kA
World	World	k1gInSc4
Athlete	Athle	k1gNnSc2
of	of	k?
the	the	k?
Year	Year	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dosud	dosud	k6eAd1
jediným	jediný	k2eAgMnSc7d1
českým	český	k2eAgMnSc7d1
vítězem	vítěz	k1gMnSc7
ankety	anketa	k1gFnSc2
byl	být	k5eAaImAgMnS
v	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
oštěpař	oštěpař	k1gMnSc1
Jan	Jan	k1gMnSc1
Železný	Železný	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://sport.idnes.cz/atletika-sef-coe-0j7-/atletika.aspx?c=A150819_072123_atletika_rou	http://sport.idnes.cz/atletika-sef-coe-0j7-/atletika.aspx?c=A150819_072123_atletika_rý	k2eAgFnSc4d1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Síň	síň	k1gFnSc1
slávy	sláva	k1gFnSc2
IAAF	IAAF	kA
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
IAAF	IAAF	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Atletické	atletický	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
a	a	k8xC
přehledy	přehled	k1gInPc1
Muži	muž	k1gMnPc1
</s>
<s>
Světové	světový	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
·	·	k?
Halové	halový	k2eAgInPc4d1
světové	světový	k2eAgInPc4d1
rekordy	rekord	k1gInPc4
·	·	k?
Juniorské	juniorský	k2eAgInPc4d1
světové	světový	k2eAgInPc4d1
rekordy	rekord	k1gInPc4
·	·	k?
Halové	halový	k2eAgInPc4d1
juniorské	juniorský	k2eAgInPc4d1
světové	světový	k2eAgInPc4d1
rekordy	rekord	k1gInPc4
·	·	k?
Evropské	evropský	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
·	·	k?
Halové	halový	k2eAgInPc1d1
evropské	evropský	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
·	·	k?
České	český	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
·	·	k?
Halové	halový	k2eAgInPc4d1
české	český	k2eAgInPc4d1
rekordy	rekord	k1gInPc4
Ženy	žena	k1gFnPc5
</s>
<s>
Světové	světový	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
·	·	k?
Halové	halový	k2eAgInPc4d1
světové	světový	k2eAgInPc4d1
rekordy	rekord	k1gInPc4
·	·	k?
Juniorské	juniorský	k2eAgInPc4d1
světové	světový	k2eAgInPc4d1
rekordy	rekord	k1gInPc4
·	·	k?
Halové	halový	k2eAgInPc4d1
juniorské	juniorský	k2eAgInPc4d1
světové	světový	k2eAgInPc4d1
rekordy	rekord	k1gInPc4
·	·	k?
Evropské	evropský	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
·	·	k?
Halové	halový	k2eAgInPc1d1
evropské	evropský	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
·	·	k?
České	český	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
·	·	k?
Halové	halový	k2eAgInPc4d1
české	český	k2eAgInPc4d1
rekordy	rekord	k1gInPc4
Olympijské	olympijský	k2eAgFnSc2d1
hry	hra	k1gFnSc2
</s>
<s>
Olympijské	olympijský	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
Mistrovství	mistrovství	k1gNnSc2
světa	svět	k1gInSc2
a	a	k8xC
Evropy	Evropa	k1gFnSc2
</s>
<s>
Rekordy	rekord	k1gInPc1
mistrovství	mistrovství	k1gNnSc2
světa	svět	k1gInSc2
(	(	kIx(
<g/>
dráha	dráha	k1gFnSc1
<g/>
)	)	kIx)
·	·	k?
Rekordy	rekord	k1gInPc1
mistrovství	mistrovství	k1gNnPc2
světa	svět	k1gInSc2
(	(	kIx(
<g/>
hala	hala	k1gFnSc1
<g/>
)	)	kIx)
·	·	k?
Rekordy	rekord	k1gInPc4
mistrovství	mistrovství	k1gNnSc2
Evropy	Evropa	k1gFnSc2
(	(	kIx(
<g/>
dráha	dráha	k1gFnSc1
<g/>
)	)	kIx)
·	·	k?
Rekordy	rekord	k1gInPc4
mistrovství	mistrovství	k1gNnSc2
Evropy	Evropa	k1gFnSc2
(	(	kIx(
<g/>
hala	hala	k1gFnSc1
<g/>
)	)	kIx)
Československo	Československo	k1gNnSc1
</s>
<s>
Přehled	přehled	k1gInSc1
na	na	k7c6
OH	OH	kA
·	·	k?
Přehled	přehled	k1gInSc1
na	na	k7c4
MS	MS	kA
·	·	k?
Přehled	přehled	k1gInSc1
na	na	k7c4
ME	ME	kA
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Přehled	přehled	k1gInSc1
na	na	k7c6
OH	OH	kA
·	·	k?
Přehled	přehled	k1gInSc1
na	na	k7c4
MS	MS	kA
·	·	k?
Přehled	přehled	k1gInSc1
na	na	k7c4
ME	ME	kA
·	·	k?
Mistrovství	mistrovství	k1gNnSc1
ČR	ČR	kA
v	v	k7c6
atletice	atletika	k1gFnSc6
Jiné	jiný	k2eAgFnPc1d1
</s>
<s>
Seznam	seznam	k1gInSc1
československých	československý	k2eAgMnPc2d1
a	a	k8xC
českých	český	k2eAgMnPc2d1
medailistů	medailista	k1gMnPc2
v	v	k7c6
atletice	atletika	k1gFnSc6
Mítinky	mítink	k1gInPc4
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1
liga	liga	k1gFnSc1
do	do	k7c2
roku	rok	k1gInSc2
2009	#num#	k4
·	·	k?
Diamantová	diamantový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
Ankety	anketa	k1gFnSc2
</s>
<s>
Atlet	atlet	k1gMnSc1
roku	rok	k1gInSc2
(	(	kIx(
<g/>
Česko	Česko	k1gNnSc1
<g/>
)	)	kIx)
·	·	k?
Atlet	atlet	k1gMnSc1
Evropy	Evropa	k1gFnSc2
·	·	k?
Atlet	atlet	k1gMnSc1
světa	svět	k1gInSc2
·	·	k?
Síň	síň	k1gFnSc1
slávy	sláva	k1gFnSc2
IAAF	IAAF	kA
IAAF	IAAF	kA
–	–	k?
(	(	kIx(
<g/>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
<g/>
)	)	kIx)
·	·	k?
AAA	AAA	kA
–	–	k?
(	(	kIx(
<g/>
Asie	Asie	k1gFnSc2
<g/>
)	)	kIx)
·	·	k?
CAA	CAA	kA
–	–	k?
(	(	kIx(
<g/>
Afrika	Afrika	k1gFnSc1
<g/>
)	)	kIx)
·	·	k?
CONSUDATLE	CONSUDATLE	kA
–	–	k?
(	(	kIx(
<g/>
Jižní	jižní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
<g/>
)	)	kIx)
·	·	k?
EAA	EAA	kA
–	–	k?
(	(	kIx(
<g/>
Evropa	Evropa	k1gFnSc1
<g/>
)	)	kIx)
·	·	k?
NACACAA	NACACAA	kA
–	–	k?
(	(	kIx(
<g/>
Severní	severní	k2eAgFnSc1d1
a	a	k8xC
Střední	střední	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
<g/>
)	)	kIx)
·	·	k?
OAA	OAA	kA
–	–	k?
(	(	kIx(
<g/>
Oceánie	Oceánie	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
sportovní	sportovní	k2eAgFnSc1d1
federace	federace	k1gFnSc1
ASOIF	ASOIF	kA
(	(	kIx(
<g/>
28	#num#	k4
<g/>
)	)	kIx)
<g/>
Federace	federace	k1gFnSc1
letních	letní	k2eAgFnPc2d1
olympiád	olympiáda	k1gFnPc2
</s>
<s>
IAAF	IAAF	kA
(	(	kIx(
<g/>
atletika	atletika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
BWF	BWF	kA
(	(	kIx(
<g/>
badminton	badminton	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIBA	FIBA	kA
(	(	kIx(
<g/>
basketbal	basketbal	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
AIBA	AIBA	kA
(	(	kIx(
<g/>
box	box	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
UCI	UCI	kA
(	(	kIx(
<g/>
cyklistika	cyklistika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
FIFA	FIFA	kA
(	(	kIx(
<g/>
fotbal	fotbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IGF	IGF	kA
(	(	kIx(
<g/>
golf	golf	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
FIG	FIG	kA
(	(	kIx(
<g/>
gymnastika	gymnastik	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
IHF	IHF	kA
(	(	kIx(
<g/>
házená	házená	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ISAF	ISAF	kA
(	(	kIx(
<g/>
jachting	jachting	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FEI	FEI	kA
(	(	kIx(
<g/>
jezdectví	jezdectví	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
IJF	IJF	kA
(	(	kIx(
<g/>
judo	judo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ICF	ICF	kA
(	(	kIx(
<g/>
kanoistika	kanoistika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
WA	WA	kA
(	(	kIx(
<g/>
lukostřelba	lukostřelba	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
UIPM	UIPM	kA
(	(	kIx(
<g/>
moderní	moderní	k2eAgInSc4d1
pětiboj	pětiboj	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
FIH	FIH	kA
(	(	kIx(
<g/>
pozemní	pozemní	k2eAgInSc4d1
hokej	hokej	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
WR	WR	kA
(	(	kIx(
<g/>
ragby	ragby	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ISSF	ISSF	kA
(	(	kIx(
<g/>
sportovní	sportovní	k2eAgFnSc1d1
střelba	střelba	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ITTF	ITTF	kA
(	(	kIx(
<g/>
stolní	stolní	k2eAgInSc1d1
tenis	tenis	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIE	FIE	kA
(	(	kIx(
<g/>
šerm	šerm	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
WTF	WTF	kA
(	(	kIx(
<g/>
taekwondo	taekwondo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ITF	ITF	kA
(	(	kIx(
<g/>
tenis	tenis	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ITU	ITU	kA
(	(	kIx(
<g/>
triatlon	triatlon	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FISA	FISA	kA
(	(	kIx(
<g/>
veslování	veslování	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
FINA	Fina	k1gFnSc1
(	(	kIx(
<g/>
vodní	vodní	k2eAgInPc1d1
sporty	sport	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
FIVB	FIVB	kA
(	(	kIx(
<g/>
volejbal	volejbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IWF	IWF	kA
(	(	kIx(
<g/>
vzpírání	vzpírání	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
UWW	UWW	kA
(	(	kIx(
<g/>
zápas	zápas	k1gInSc1
<g/>
)	)	kIx)
AIOWF	AIOWF	kA
(	(	kIx(
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
Federace	federace	k1gFnSc1
zimních	zimní	k2eAgFnPc2d1
olympiád	olympiáda	k1gFnPc2
</s>
<s>
IBU	IBU	kA
(	(	kIx(
<g/>
biatlon	biatlon	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IBSF	IBSF	kA
(	(	kIx(
<g/>
boby	bob	k1gInPc1
a	a	k8xC
skeleton	skeleton	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ISU	ISU	kA
(	(	kIx(
<g/>
bruslení	bruslení	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
WCF	WCF	kA
(	(	kIx(
<g/>
curling	curling	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IIHF	IIHF	kA
(	(	kIx(
<g/>
lední	lední	k2eAgInSc4d1
hokej	hokej	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
FIS	FIS	kA
(	(	kIx(
<g/>
lyžařské	lyžařský	k2eAgInPc4d1
sporty	sport	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
FIL	FIL	kA
(	(	kIx(
<g/>
sáňkařský	sáňkařský	k2eAgInSc1d1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
ARISF	ARISF	kA
(	(	kIx(
<g/>
37	#num#	k4
<g/>
)	)	kIx)
<g/>
Další	další	k2eAgFnSc1d1
federace	federace	k1gFnSc1
uznané	uznaný	k2eAgFnSc2d1
MOV	MOV	kA
</s>
<s>
IFAF	IFAF	kA
(	(	kIx(
<g/>
americký	americký	k2eAgInSc1d1
fotbal	fotbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIA	FIA	kA
(	(	kIx(
<g/>
automobilový	automobilový	k2eAgInSc1d1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIB	FIB	kA
(	(	kIx(
<g/>
bandy	bandy	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
WBSC	WBSC	kA
(	(	kIx(
<g/>
baseball	baseball	k1gInSc1
a	a	k8xC
softball	softball	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
WB	WB	kA
(	(	kIx(
<g/>
bowling	bowling	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
WBF	WBF	kA
(	(	kIx(
<g/>
bridž	bridž	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IFF	IFF	kA
(	(	kIx(
<g/>
florbal	florbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
UIAA	UIAA	kA
(	(	kIx(
<g/>
horolezectví	horolezectví	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ICU	ICU	kA
(	(	kIx(
<g/>
cheerleading	cheerleading	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
WKF	WKF	kA
(	(	kIx(
<g/>
karate	karate	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ICC	ICC	kA
(	(	kIx(
<g/>
kriket	kriket	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIRS	FIRS	kA
(	(	kIx(
<g/>
kolečkové	kolečkový	k2eAgInPc4d1
sporty	sport	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
IKF	IKF	kA
(	(	kIx(
<g/>
korfbal	korfbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
CMSB	CMSB	kA
(	(	kIx(
<g/>
koulové	koulový	k2eAgInPc4d1
sporty	sport	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
WCBS	WCBS	kA
(	(	kIx(
<g/>
kulečník	kulečník	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FAI	FAI	kA
(	(	kIx(
<g/>
letecký	letecký	k2eAgInSc1d1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIM	FIM	kA
(	(	kIx(
<g/>
motocyklový	motocyklový	k2eAgInSc1d1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IFMA	IFMA	kA
(	(	kIx(
<g/>
muay	mua	k2eAgFnPc1d1
thai	tha	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
IFNA	IFNA	kA
(	(	kIx(
<g/>
netball	netball	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
IOF	IOF	kA
(	(	kIx(
<g/>
orientační	orientační	k2eAgInSc1d1
běh	běh	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIPV	FIPV	kA
(	(	kIx(
<g/>
pelota	pelota	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
CMAS	CMAS	kA
(	(	kIx(
<g/>
podvodní	podvodní	k2eAgInPc4d1
sporty	sport	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
FIP	FIP	kA
(	(	kIx(
<g/>
pólo	pólo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
TWIF	TWIF	kA
(	(	kIx(
<g/>
přetahování	přetahování	k1gNnPc2
lanem	lano	k1gNnSc7
<g/>
)	)	kIx)
</s>
<s>
IRF	IRF	kA
(	(	kIx(
<g/>
raketbal	raketbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ISMF	ISMF	kA
(	(	kIx(
<g/>
skialpinismus	skialpinismus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IFSC	IFSC	kA
(	(	kIx(
<g/>
sportovní	sportovní	k2eAgNnSc1d1
lezení	lezení	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
WSF	WSF	kA
(	(	kIx(
<g/>
squash	squash	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IFS	IFS	kA
(	(	kIx(
<g/>
sumo	suma	k1gFnSc5
<g/>
)	)	kIx)
</s>
<s>
ISA	ISA	kA
(	(	kIx(
<g/>
surfing	surfing	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIDE	FIDE	kA
(	(	kIx(
<g/>
šachy	šach	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
WDSF	WDSF	kA
(	(	kIx(
<g/>
taneční	taneční	k2eAgInSc1d1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
WFDF	WFDF	kA
(	(	kIx(
<g/>
ultimate	ultimat	k1gInSc5
frisbee	frisbee	k1gNnPc5
<g/>
)	)	kIx)
</s>
<s>
IWWF	IWWF	kA
(	(	kIx(
<g/>
vodní	vodní	k2eAgNnSc1d1
lyžování	lyžování	k1gNnSc1
a	a	k8xC
wakeboarding	wakeboarding	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
UIM	UIM	kA
(	(	kIx(
<g/>
vodní	vodní	k2eAgInSc1d1
motorismus	motorismus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IWUF	IWUF	kA
(	(	kIx(
<g/>
wu-šu	wu-sat	k5eAaPmIp1nS
<g/>
)	)	kIx)
</s>
<s>
ILSF	ILSF	kA
(	(	kIx(
<g/>
záchranářský	záchranářský	k2eAgInSc1d1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
Ostatní	ostatní	k2eAgFnSc1d1
federace	federace	k1gFnSc1
ve	v	k7c6
SportAccordu	SportAccordo	k1gNnSc6
(	(	kIx(
<g/>
21	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
IAF	IAF	kA
(	(	kIx(
<g/>
aikido	aikida	k1gFnSc5
<g/>
)	)	kIx)
</s>
<s>
FMJD	FMJD	kA
(	(	kIx(
<g/>
dáma	dáma	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
IDBF	IDBF	kA
(	(	kIx(
<g/>
dračí	dračí	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
JJIF	JJIF	kA
(	(	kIx(
<g/>
džú-džucu	džú-džucu	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
IFA	IFA	kA
(	(	kIx(
<g/>
faustball	faustball	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
IGF	IGF	kA
(	(	kIx(
<g/>
go	go	k?
<g/>
)	)	kIx)
</s>
<s>
IFI	IFI	kA
(	(	kIx(
<g/>
ice	ice	k?
stock	stock	k1gInSc1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIK	fik	k0
(	(	kIx(
<g/>
kendó	kendó	k?
<g/>
)	)	kIx)
</s>
<s>
WAKO	WAKO	kA
(	(	kIx(
<g/>
kickboxing	kickboxing	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IFBB	IFBB	kA
(	(	kIx(
<g/>
kulturistika	kulturistika	k1gFnSc1
a	a	k8xC
fitness	fitness	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIL	FIL	kA
(	(	kIx(
<g/>
lakros	lakros	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
WMF	WMF	kA
(	(	kIx(
<g/>
minigolf	minigolf	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ISFF	ISFF	kA
(	(	kIx(
<g/>
psí	psí	k2eAgNnSc1d1
spřežení	spřežení	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ICSF	ICSF	kA
(	(	kIx(
<g/>
rybolovná	rybolovný	k2eAgFnSc1d1
technika	technika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
FIAS	FIAS	kA
(	(	kIx(
<g/>
sambo	samba	k1gFnSc5
<g/>
)	)	kIx)
</s>
<s>
FISav	FISav	k1gInSc4
(	(	kIx(
<g/>
savate	savat	k1gMnSc5
<g/>
)	)	kIx)
</s>
<s>
ISTAF	ISTAF	kA
(	(	kIx(
<g/>
sepak	sepak	k1gMnSc1
takraw	takraw	k?
<g/>
)	)	kIx)
</s>
<s>
IPF	IPF	kA
(	(	kIx(
<g/>
silový	silový	k2eAgInSc1d1
trojboj	trojboj	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ISTF	ISTF	kA
(	(	kIx(
<g/>
soft	soft	k?
tenis	tenis	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
CIPS	CIPS	kA
(	(	kIx(
<g/>
sportovní	sportovní	k2eAgInSc1d1
rybolov	rybolov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
WDF	WDF	kA
(	(	kIx(
<g/>
šipky	šipka	k1gFnPc1
<g/>
)	)	kIx)
Jiné	jiný	k2eAgFnPc1d1
federace	federace	k1gFnPc1
(	(	kIx(
<g/>
19	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
WAF	WAF	kA
(	(	kIx(
<g/>
armwrestling	armwrestling	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ARI	ARI	kA
(	(	kIx(
<g/>
Australský	australský	k2eAgInSc1d1
fotbal	fotbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IBA	iba	k6eAd1
(	(	kIx(
<g/>
bodyboarding	bodyboarding	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
PBA	PBA	kA
(	(	kIx(
<g/>
bowls	bowls	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IFBA	IFBA	kA
(	(	kIx(
<g/>
broomball	broomball	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
IKAEF	IKAEF	kA
(	(	kIx(
<g/>
eskrima	eskrim	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
WFA	WFA	kA
(	(	kIx(
<g/>
footbag	footbag	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
ISBHF	ISBHF	kA
(	(	kIx(
<g/>
hokejbal	hokejbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
WCF	WCF	kA
(	(	kIx(
<g/>
kroket	kroket	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IKF	IKF	kA
(	(	kIx(
<g/>
kabaddi	kabaddit	k5eAaPmRp2nS
<g/>
)	)	kIx)
</s>
<s>
IMMAF	IMMAF	kA
(	(	kIx(
<g/>
MMA	MMA	kA
<g/>
)	)	kIx)
</s>
<s>
IFP	IFP	kA
(	(	kIx(
<g/>
poker	poker	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IPSC	IPSC	kA
(	(	kIx(
<g/>
practical	practicat	k5eAaPmAgInS
shooting	shooting	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IQA	IQA	kA
(	(	kIx(
<g/>
mudlovský	mudlovský	k2eAgInSc1d1
famfrpál	famfrpál	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IFMAR	IFMAR	kA
(	(	kIx(
<g/>
závody	závod	k1gInPc1
automobilových	automobilový	k2eAgInPc2d1
modelů	model	k1gInPc2
<g/>
)	)	kIx)
</s>
<s>
FIFTA	FIFTA	kA
(	(	kIx(
<g/>
nohejbal	nohejbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IRF	IRF	kA
(	(	kIx(
<g/>
rogaining	rogaining	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
RLIF	RLIF	kA
(	(	kIx(
<g/>
třináctkové	třináctkový	k2eAgNnSc1d1
ragby	ragby	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
WSSA	WSSA	kA
(	(	kIx(
<g/>
sport	sport	k1gInSc1
stacking	stacking	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ITPF	ITPF	kA
(	(	kIx(
<g/>
Tent	tent	k1gInSc1
pegging	pegging	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIT	fit	k2eAgInSc1d1
(	(	kIx(
<g/>
touch	touch	k1gInSc1
rugby	rugby	k1gNnSc1
<g/>
)	)	kIx)
Mezinárodní	mezinárodní	k2eAgInSc1d1
olympijský	olympijský	k2eAgInSc1d1
výbor	výbor	k1gInSc1
•	•	k?
Mezinárodní	mezinárodní	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
Světových	světový	k2eAgFnPc2d1
her	hra	k1gFnPc2
•	•	k?
SportAccord	SportAccord	k1gMnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
mzk	mzk	k?
<g/>
2006318406	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
10050595-8	10050595-8	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2226	#num#	k4
9551	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2005093264	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
148709621	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2005093264	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Sport	sport	k1gInSc1
</s>
