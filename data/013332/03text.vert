<p>
<s>
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1678	[number]	k4	1678
Vídeň	Vídeň	k1gFnSc1	Vídeň
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1711	[number]	k4	1711
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
a	a	k8xC	a
uherský	uherský	k2eAgMnSc1d1	uherský
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
moravský	moravský	k2eAgMnSc1d1	moravský
markrabě	markrabě	k1gMnSc1	markrabě
atd.	atd.	kA	atd.
<g/>
,	,	kIx,	,
předposlední	předposlední	k2eAgMnSc1d1	předposlední
člen	člen	k1gMnSc1	člen
dynastie	dynastie	k1gFnSc2	dynastie
Habsburků	Habsburk	k1gMnPc2	Habsburk
v	v	k7c6	v
mužské	mužský	k2eAgFnSc6d1	mužská
linii	linie	k1gFnSc6	linie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobnost	osobnost	k1gFnSc4	osobnost
Josefa	Josef	k1gMnSc2	Josef
I.	I.	kA	I.
==	==	k?	==
</s>
</p>
<p>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
syna	syn	k1gMnSc4	syn
císaře	císař	k1gMnSc4	císař
Leopolda	Leopold	k1gMnSc4	Leopold
I.	I.	kA	I.
ze	z	k7c2	z
třetího	třetí	k4xOgNnSc2	třetí
manželství	manželství	k1gNnSc2	manželství
s	s	k7c7	s
Eleonorou	Eleonora	k1gFnSc7	Eleonora
Neuburskou	Neuburský	k2eAgFnSc7d1	Neuburský
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
sice	sice	k8xC	sice
mnoho	mnoho	k4c1	mnoho
vlastních	vlastní	k2eAgMnPc2d1	vlastní
i	i	k8xC	i
nevlastních	vlastní	k2eNgMnPc2d1	nevlastní
sourozenců	sourozenec	k1gMnPc2	sourozenec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
brzkého	brzký	k2eAgNnSc2d1	brzké
úmrtí	úmrtí	k1gNnSc2	úmrtí
většiny	většina	k1gFnSc2	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
záhy	záhy	k6eAd1	záhy
nejstarším	starý	k2eAgMnSc7d3	nejstarší
mužským	mužský	k2eAgMnSc7d1	mužský
potomkem	potomek	k1gMnSc7	potomek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
byl	být	k5eAaImAgMnS	být
inteligentní	inteligentní	k2eAgMnSc1d1	inteligentní
a	a	k8xC	a
schopný	schopný	k2eAgMnSc1d1	schopný
mladík	mladík	k1gMnSc1	mladík
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ovládal	ovládat	k5eAaImAgInS	ovládat
sedm	sedm	k4xCc4	sedm
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
disponoval	disponovat	k5eAaBmAgMnS	disponovat
i	i	k8xC	i
jistým	jistý	k2eAgNnSc7d1	jisté
hudebním	hudební	k2eAgNnSc7d1	hudební
nadáním	nadání	k1gNnSc7	nadání
<g/>
,	,	kIx,	,
když	když	k8xS	když
sám	sám	k3xTgMnSc1	sám
komponoval	komponovat	k5eAaImAgMnS	komponovat
skladby	skladba	k1gFnPc4	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc4	jeho
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
se	se	k3xPyFc4	se
ujali	ujmout	k5eAaPmAgMnP	ujmout
Franz	Franz	k1gMnSc1	Franz
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
von	von	k1gInSc4	von
Rummel	Rummel	k1gMnSc1	Rummel
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgMnSc1d2	pozdější
biskup	biskup	k1gMnSc1	biskup
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
a	a	k8xC	a
kníže	kníže	k1gMnSc1	kníže
Karel	Karel	k1gMnSc1	Karel
Theodor	Theodor	k1gMnSc1	Theodor
ze	z	k7c2	z
Salmu	Salm	k1gInSc2	Salm
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
manželkou	manželka	k1gFnSc7	manželka
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1699	[number]	k4	1699
(	(	kIx(	(
<g/>
svatba	svatba	k1gFnSc1	svatba
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
)	)	kIx)	)
o	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
starší	starý	k2eAgFnSc1d2	starší
Amálie	Amálie	k1gFnSc1	Amálie
Vilemína	Vilemína	k1gFnSc1	Vilemína
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
ovšem	ovšem	k9	ovšem
měl	mít	k5eAaImAgMnS	mít
milostné	milostný	k2eAgFnPc4d1	milostná
aféry	aféra	k1gFnPc4	aféra
před	před	k7c7	před
sňatkem	sňatek	k1gInSc7	sňatek
i	i	k9	i
po	po	k7c6	po
sňatku	sňatek	k1gInSc6	sňatek
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
svou	svůj	k3xOyFgFnSc4	svůj
ženu	žena	k1gFnSc4	žena
nakazil	nakazit	k5eAaPmAgMnS	nakazit
pohlavní	pohlavní	k2eAgFnSc7d1	pohlavní
chorobou	choroba	k1gFnSc7	choroba
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
po	po	k7c6	po
několika	několik	k4yIc6	několik
letech	let	k1gInPc6	let
zůstala	zůstat	k5eAaPmAgFnS	zůstat
neplodná	plodný	k2eNgFnSc1d1	neplodná
a	a	k8xC	a
počet	počet	k1gInSc1	počet
jejich	jejich	k3xOp3gMnPc2	jejich
potomků	potomek	k1gMnPc2	potomek
skončil	skončit	k5eAaPmAgMnS	skončit
na	na	k7c6	na
třech	tři	k4xCgInPc6	tři
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Válečné	válečný	k2eAgInPc1d1	válečný
konflikty	konflikt	k1gInPc1	konflikt
==	==	k?	==
</s>
</p>
<p>
<s>
Otec	otec	k1gMnSc1	otec
ho	on	k3xPp3gMnSc4	on
již	již	k6eAd1	již
v	v	k7c6	v
dětských	dětský	k2eAgNnPc6d1	dětské
letech	léto	k1gNnPc6	léto
nechal	nechat	k5eAaPmAgInS	nechat
korunovat	korunovat	k5eAaBmF	korunovat
uherským	uherský	k2eAgMnSc7d1	uherský
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1687	[number]	k4	1687
<g/>
)	)	kIx)	)
a	a	k8xC	a
římskoněmeckým	římskoněmecký	k2eAgMnSc7d1	římskoněmecký
králem	král	k1gMnSc7	král
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
1690	[number]	k4	1690
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
naopak	naopak	k6eAd1	naopak
korunován	korunován	k2eAgMnSc1d1	korunován
nebyl	být	k5eNaImAgInS	být
<g/>
.	.	kIx.	.
</s>
<s>
Jazykově	jazykově	k6eAd1	jazykově
všestranně	všestranně	k6eAd1	všestranně
vybavený	vybavený	k2eAgMnSc1d1	vybavený
muž	muž	k1gMnSc1	muž
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
uměl	umět	k5eAaImAgMnS	umět
česky	česky	k6eAd1	česky
jistě	jistě	k9	jistě
lépe	dobře	k6eAd2	dobře
než	než	k8xS	než
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1702	[number]	k4	1702
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Prahu	Praha	k1gFnSc4	Praha
a	a	k8xC	a
Starou	Stará	k1gFnSc4	Stará
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
poklonil	poklonit	k5eAaPmAgMnS	poklonit
Palladiu	palladion	k1gNnSc3	palladion
země	zem	k1gFnSc2	zem
české	český	k2eAgFnSc2d1	Česká
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
plném	plný	k2eAgInSc6d1	plný
proudu	proud	k1gInSc6	proud
války	válka	k1gFnSc2	válka
o	o	k7c4	o
španělské	španělský	k2eAgNnSc4d1	španělské
dědictví	dědictví	k1gNnSc4	dědictví
<g/>
,	,	kIx,	,
během	během	k7c2	během
nichž	jenž	k3xRgInPc2	jenž
podporoval	podporovat	k5eAaImAgInS	podporovat
nároky	nárok	k1gInPc4	nárok
svého	svůj	k3xOyFgMnSc2	svůj
mladšího	mladý	k2eAgMnSc2d2	mladší
bratra	bratr	k1gMnSc2	bratr
Karla	Karel	k1gMnSc2	Karel
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
osobně	osobně	k6eAd1	osobně
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
obléhání	obléhání	k1gNnSc4	obléhání
města	město	k1gNnSc2	město
Landau	Landaus	k1gInSc2	Landaus
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1702	[number]	k4	1702
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
bojištích	bojiště	k1gNnPc6	bojiště
zde	zde	k6eAd1	zde
získával	získávat	k5eAaImAgInS	získávat
další	další	k2eAgInPc4d1	další
úspěchy	úspěch	k1gInPc4	úspěch
oblíbenec	oblíbenec	k1gMnSc1	oblíbenec
Josefa	Josef	k1gMnSc2	Josef
Evžen	Evžen	k1gMnSc1	Evžen
Savojský	savojský	k2eAgMnSc1d1	savojský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vláda	vláda	k1gFnSc1	vláda
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1705	[number]	k4	1705
zdědil	zdědit	k5eAaPmAgInS	zdědit
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
otci	otec	k1gMnSc3	otec
mocnářství	mocnářství	k1gNnSc4	mocnářství
v	v	k7c6	v
nelehké	lehký	k2eNgFnSc6d1	nelehká
situaci	situace	k1gFnSc6	situace
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
podpory	podpora	k1gFnSc2	podpora
bratrovi	bratr	k1gMnSc3	bratr
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgMnS	muset
věnovat	věnovat	k5eAaPmF	věnovat
také	také	k6eAd1	také
vnitrostátnímu	vnitrostátní	k2eAgInSc3d1	vnitrostátní
problému	problém	k1gInSc3	problém
na	na	k7c4	na
území	území	k1gNnSc4	území
Uher	Uhry	k1gFnPc2	Uhry
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
povstání	povstání	k1gNnSc3	povstání
tam	tam	k6eAd1	tam
totiž	totiž	k9	totiž
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
přes	přes	k7c4	přes
rok	rok	k1gInSc4	rok
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
značné	značný	k2eAgFnSc6d1	značná
části	část	k1gFnSc6	část
jako	jako	k8xC	jako
sedmihradský	sedmihradský	k2eAgMnSc1d1	sedmihradský
kníže	kníže	k1gMnSc1	kníže
František	František	k1gMnSc1	František
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Rákóczi	Rákóch	k1gMnPc1	Rákóch
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
situace	situace	k1gFnSc1	situace
zemí	zem	k1gFnPc2	zem
monarchie	monarchie	k1gFnSc2	monarchie
nebyla	být	k5eNaImAgFnS	být
příznivá	příznivý	k2eAgFnSc1d1	příznivá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
dlouhodobých	dlouhodobý	k2eAgFnPc2d1	dlouhodobá
válek	válka	k1gFnPc2	válka
vzrůstalo	vzrůstat	k5eAaImAgNnS	vzrůstat
daňové	daňový	k2eAgNnSc1d1	daňové
zatížení	zatížení	k1gNnSc1	zatížení
poddaných	poddaný	k1gMnPc2	poddaný
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
způsobovalo	způsobovat	k5eAaImAgNnS	způsobovat
nespokojenost	nespokojenost	k1gFnSc4	nespokojenost
širokých	široký	k2eAgFnPc2d1	široká
mas	masa	k1gFnPc2	masa
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
panovnickém	panovnický	k2eAgInSc6d1	panovnický
dvoře	dvůr	k1gInSc6	dvůr
vládla	vládnout	k5eAaImAgFnS	vládnout
byrokracie	byrokracie	k1gFnSc1	byrokracie
<g/>
,	,	kIx,	,
úplatkářství	úplatkářství	k1gNnSc1	úplatkářství
a	a	k8xC	a
protekce	protekce	k1gFnSc1	protekce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sedmadvacetiletý	sedmadvacetiletý	k2eAgMnSc1d1	sedmadvacetiletý
Josef	Josef	k1gMnSc1	Josef
byl	být	k5eAaImAgMnS	být
však	však	k9	však
už	už	k6eAd1	už
schopným	schopný	k2eAgMnSc7d1	schopný
vládcem	vládce	k1gMnSc7	vládce
odhodlaným	odhodlaný	k2eAgMnSc7d1	odhodlaný
k	k	k7c3	k
radikálním	radikální	k2eAgNnPc3d1	radikální
opatřením	opatření	k1gNnPc3	opatření
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
po	po	k7c6	po
všech	všecek	k3xTgFnPc6	všecek
stránkách	stránka	k1gFnPc6	stránka
dobře	dobře	k6eAd1	dobře
vzdělaný	vzdělaný	k2eAgMnSc1d1	vzdělaný
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
rozhodný	rozhodný	k2eAgMnSc1d1	rozhodný
člověk	člověk	k1gMnSc1	člověk
zároveň	zároveň	k6eAd1	zároveň
hodlal	hodlat	k5eAaImAgMnS	hodlat
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
mnoho	mnoho	k4c4	mnoho
reforem	reforma	k1gFnPc2	reforma
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
finanční	finanční	k2eAgFnSc2d1	finanční
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgFnSc2d1	soudní
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
u	u	k7c2	u
vlastního	vlastní	k2eAgInSc2d1	vlastní
dvora	dvůr	k1gInSc2	dvůr
<g/>
,	,	kIx,	,
když	když	k8xS	když
výrazně	výrazně	k6eAd1	výrazně
zúžil	zúžit	k5eAaPmAgInS	zúžit
a	a	k8xC	a
omladil	omladit	k5eAaPmAgInS	omladit
okruh	okruh	k1gInSc1	okruh
svých	svůj	k3xOyFgMnPc2	svůj
spolupracovníků	spolupracovník	k1gMnPc2	spolupracovník
<g/>
.	.	kIx.	.
</s>
<s>
Posiloval	posilovat	k5eAaImAgMnS	posilovat
centrální	centrální	k2eAgNnSc4d1	centrální
řízení	řízení	k1gNnSc4	řízení
státní	státní	k2eAgFnSc1d1	státní
byrokracie	byrokracie	k1gFnSc1	byrokracie
a	a	k8xC	a
reformy	reforma	k1gFnPc1	reforma
začaly	začít	k5eAaPmAgFnP	začít
i	i	k9	i
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
daňového	daňový	k2eAgInSc2d1	daňový
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
mnoho	mnoho	k4c4	mnoho
jím	jíst	k5eAaImIp1nS	jíst
zamýšlených	zamýšlený	k2eAgMnPc2d1	zamýšlený
plánů	plán	k1gInPc2	plán
však	však	k9	však
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1707	[number]	k4	1707
vydal	vydat	k5eAaPmAgInS	vydat
reskript	reskript	k1gInSc1	reskript
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
českým	český	k2eAgInPc3d1	český
stavům	stav	k1gInPc3	stav
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
projednali	projednat	k5eAaPmAgMnP	projednat
návrh	návrh	k1gInSc4	návrh
Christiana	Christian	k1gMnSc4	Christian
Josefa	Josef	k1gMnSc4	Josef
Willenberga	Willenberg	k1gMnSc4	Willenberg
<g/>
,	,	kIx,	,
a	a	k8xC	a
nařizuje	nařizovat	k5eAaImIp3nS	nařizovat
jim	on	k3xPp3gMnPc3	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
pro	pro	k7c4	pro
něho	on	k3xPp3gMnSc4	on
zřídili	zřídit	k5eAaPmAgMnP	zřídit
stálé	stálý	k2eAgNnSc4d1	stálé
profesorské	profesorský	k2eAgNnSc4d1	profesorské
místo	místo	k1gNnSc4	místo
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
výuku	výuka	k1gFnSc4	výuka
umění	umění	k1gNnSc2	umění
inženýrského	inženýrský	k2eAgNnSc2d1	inženýrské
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
reskript	reskript	k1gInSc1	reskript
dnes	dnes	k6eAd1	dnes
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejcennějším	cenný	k2eAgInPc3d3	nejcennější
historickým	historický	k2eAgInPc3d1	historický
dokumentům	dokument	k1gInPc3	dokument
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Českého	český	k2eAgNnSc2d1	české
vysokého	vysoký	k2eAgNnSc2d1	vysoké
učení	učení	k1gNnSc2	učení
technického	technický	k2eAgNnSc2d1	technické
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1707	[number]	k4	1707
jej	on	k3xPp3gMnSc4	on
uherský	uherský	k2eAgInSc1d1	uherský
onódský	onódský	k2eAgInSc1d1	onódský
sněm	sněm	k1gInSc1	sněm
sesadil	sesadit	k5eAaPmAgInS	sesadit
z	z	k7c2	z
trůnu	trůn	k1gInSc2	trůn
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
ovšem	ovšem	k9	ovšem
zahájil	zahájit	k5eAaPmAgInS	zahájit
mírová	mírový	k2eAgNnPc4d1	Mírové
jednání	jednání	k1gNnPc4	jednání
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgFnPc7	jenž
si	se	k3xPyFc3	se
nakonec	nakonec	k6eAd1	nakonec
znovu	znovu	k6eAd1	znovu
získal	získat	k5eAaPmAgMnS	získat
oporu	opora	k1gFnSc4	opora
proti	proti	k7c3	proti
stavovskému	stavovský	k2eAgNnSc3d1	Stavovské
hnutí	hnutí	k1gNnSc3	hnutí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1711	[number]	k4	1711
se	se	k3xPyFc4	se
již	již	k6eAd1	již
situace	situace	k1gFnSc1	situace
zdála	zdát	k5eAaImAgFnS	zdát
stabilizovaná	stabilizovaný	k2eAgFnSc1d1	stabilizovaná
<g/>
:	:	kIx,	:
španělská	španělský	k2eAgFnSc1d1	španělská
válka	válka	k1gFnSc1	válka
se	se	k3xPyFc4	se
zdála	zdát	k5eAaImAgFnS	zdát
být	být	k5eAaImF	být
vyhranou	vyhraná	k1gFnSc4	vyhraná
a	a	k8xC	a
problém	problém	k1gInSc4	problém
Uher	uher	k1gInSc1	uher
vyřešen	vyřešen	k2eAgInSc1d1	vyřešen
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
ovšem	ovšem	k9	ovšem
zhatila	zhatit	k5eAaPmAgFnS	zhatit
nenadálá	nenadálý	k2eAgFnSc1d1	nenadálá
panovníkova	panovníkův	k2eAgFnSc1d1	panovníkova
smrt	smrt	k1gFnSc1	smrt
způsobená	způsobený	k2eAgFnSc1d1	způsobená
neštovicemi	neštovice	k1gFnPc7	neštovice
<g/>
.	.	kIx.	.
</s>
<s>
Přerušil	přerušit	k5eAaPmAgInS	přerušit
se	se	k3xPyFc4	se
tak	tak	k9	tak
reformní	reformní	k2eAgInSc1d1	reformní
proces	proces	k1gInSc1	proces
v	v	k7c6	v
monarchii	monarchie	k1gFnSc6	monarchie
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yQgInSc4	který
později	pozdě	k6eAd2	pozdě
navázal	navázat	k5eAaPmAgMnS	navázat
především	především	k9	především
Josefův	Josefův	k2eAgMnSc1d1	Josefův
jmenovec	jmenovec	k1gMnSc1	jmenovec
<g/>
,	,	kIx,	,
toho	ten	k3xDgNnSc2	ten
jména	jméno	k1gNnSc2	jméno
Druhý	druhý	k4xOgInSc1	druhý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Pochován	pochován	k2eAgMnSc1d1	pochován
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Císařské	císařský	k2eAgFnSc6d1	císařská
hrobce	hrobka	k1gFnSc6	hrobka
(	(	kIx(	(
<g/>
sarkofág	sarkofág	k1gInSc1	sarkofág
č.	č.	k?	č.
35	[number]	k4	35
v	v	k7c6	v
Karlově	Karlův	k2eAgFnSc6d1	Karlova
hrobce	hrobka	k1gFnSc6	hrobka
<g/>
)	)	kIx)	)
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
rakvi	rakev	k1gFnSc6	rakev
je	být	k5eAaImIp3nS	být
latinský	latinský	k2eAgInSc1d1	latinský
nápis	nápis	k1gInSc1	nápis
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
český	český	k2eAgInSc1d1	český
překlad	překlad	k1gInSc1	překlad
zní	znět	k5eAaImIp3nS	znět
<g/>
:	:	kIx,	:
Světu	svět	k1gInSc3	svět
propůjčen	propůjčen	k2eAgMnSc1d1	propůjčen
1678	[number]	k4	1678
<g/>
,	,	kIx,	,
království	království	k1gNnSc1	království
uherskému	uherský	k2eAgInSc3d1	uherský
1687	[number]	k4	1687
<g/>
,	,	kIx,	,
římské	římský	k2eAgFnSc6d1	římská
říši	říš	k1gFnSc6	říš
1690	[number]	k4	1690
<g/>
,	,	kIx,	,
Amalii	Amalie	k1gFnSc6	Amalie
Wilhelmině	Wilhelmin	k2eAgFnSc6d1	Wilhelmina
1699	[number]	k4	1699
<g/>
,	,	kIx,	,
římskému	římský	k2eAgNnSc3d1	římské
císařství	císařství	k1gNnSc3	císařství
<g/>
,	,	kIx,	,
Čechám	Čechy	k1gFnPc3	Čechy
<g/>
,	,	kIx,	,
provinciím	provincie	k1gFnPc3	provincie
atd.	atd.	kA	atd.
1705	[number]	k4	1705
<g/>
,	,	kIx,	,
nebesům	nebesa	k1gNnPc3	nebesa
navrácen	navrácen	k2eAgMnSc1d1	navrácen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1711	[number]	k4	1711
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
a	a	k8xC	a
všude	všude	k6eAd1	všude
vítězný	vítězný	k2eAgMnSc1d1	vítězný
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
srdce	srdce	k1gNnSc1	srdce
bylo	být	k5eAaImAgNnS	být
uloženo	uložit	k5eAaPmNgNnS	uložit
v	v	k7c6	v
urně	urna	k1gFnSc6	urna
č.	č.	k?	č.
12	[number]	k4	12
v	v	k7c6	v
Hrobce	hrobka	k1gFnSc6	hrobka
srdcí	srdce	k1gNnPc2	srdce
(	(	kIx(	(
<g/>
Herzgruft	Herzgruft	k1gInSc1	Herzgruft
<g/>
)	)	kIx)	)
v	v	k7c6	v
augustiniánském	augustiniánský	k2eAgInSc6d1	augustiniánský
kostele	kostel	k1gInSc6	kostel
a	a	k8xC	a
vnitřnosti	vnitřnost	k1gFnSc6	vnitřnost
v	v	k7c6	v
urně	urna	k1gFnSc6	urna
č.	č.	k?	č.
42	[number]	k4	42
ve	v	k7c6	v
Vévodské	vévodský	k2eAgFnSc6d1	vévodská
hrobce	hrobka	k1gFnSc6	hrobka
svatoštěpánské	svatoštěpánský	k2eAgFnSc2d1	Svatoštěpánská
katedrály	katedrála	k1gFnSc2	katedrála
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potomci	potomek	k1gMnPc5	potomek
==	==	k?	==
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Josefa	Josef	k1gMnSc2	Josef
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1699	[number]	k4	1699
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1757	[number]	k4	1757
<g/>
)	)	kIx)	)
∞	∞	k?	∞
20.8	[number]	k4	20.8
<g/>
.1719	.1719	k4	.1719
August	August	k1gMnSc1	August
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
saský	saský	k2eAgMnSc1d1	saský
kurfiřt	kurfiřt	k1gMnSc1	kurfiřt
(	(	kIx(	(
<g/>
1733	[number]	k4	1733
<g/>
/	/	kIx~	/
<g/>
1736	[number]	k4	1736
<g/>
-	-	kIx~	-
<g/>
1763	[number]	k4	1763
<g/>
)	)	kIx)	)
a	a	k8xC	a
polský	polský	k2eAgMnSc1d1	polský
král	král	k1gMnSc1	král
(	(	kIx(	(
<g/>
1733	[number]	k4	1733
<g/>
-	-	kIx~	-
<g/>
1763	[number]	k4	1763
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Leopold	Leopold	k1gMnSc1	Leopold
Josef	Josef	k1gMnSc1	Josef
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1700	[number]	k4	1700
Vídeň	Vídeň	k1gFnSc1	Vídeň
-	-	kIx~	-
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1701	[number]	k4	1701
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
hydrocephalus	hydrocephalus	k1gInSc4	hydrocephalus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Amálie	Amálie	k1gFnSc1	Amálie
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1701	[number]	k4	1701
–	–	k?	–
11	[number]	k4	11
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1756	[number]	k4	1756
<g/>
)	)	kIx)	)
∞	∞	k?	∞
1722	[number]	k4	1722
Karel	Karla	k1gFnPc2	Karla
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Albrecht	Albrecht	k1gMnSc1	Albrecht
<g/>
,	,	kIx,	,
kurfiřt	kurfiřt	k1gMnSc1	kurfiřt
bavorský	bavorský	k2eAgMnSc1d1	bavorský
(	(	kIx(	(
<g/>
1726	[number]	k4	1726
<g/>
-	-	kIx~	-
<g/>
1745	[number]	k4	1745
<g/>
)	)	kIx)	)
a	a	k8xC	a
římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
(	(	kIx(	(
<g/>
1742	[number]	k4	1742
<g/>
-	-	kIx~	-
<g/>
1745	[number]	k4	1745
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Vývod	vývod	k1gInSc1	vývod
z	z	k7c2	z
předků	předek	k1gInPc2	předek
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ČORNEJOVÁ	ČORNEJOVÁ	kA	ČORNEJOVÁ
<g/>
,	,	kIx,	,
Ivana	Ivana	k1gFnSc1	Ivana
<g/>
;	;	kIx,	;
RAK	rak	k1gMnSc1	rak
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
VLNAS	VLNAS	kA	VLNAS
<g/>
,	,	kIx,	,
Vít	Vít	k1gMnSc1	Vít
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
tvých	tvůj	k3xOp2gNnPc2	tvůj
křídel	křídlo	k1gNnPc2	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Habsburkové	Habsburk	k1gMnPc1	Habsburk
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Grafoprint-Neubert	Grafoprint-Neubert	k1gMnSc1	Grafoprint-Neubert
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
289	[number]	k4	289
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85785	[number]	k4	85785
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
HAMANNOVÁ	HAMANNOVÁ	kA	HAMANNOVÁ
<g/>
,	,	kIx,	,
Brigitte	Brigitte	k1gFnSc1	Brigitte
<g/>
.	.	kIx.	.
</s>
<s>
Habsburkové	Habsburk	k1gMnPc1	Habsburk
:	:	kIx,	:
životopisná	životopisný	k2eAgFnSc1d1	životopisná
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Brána	brána	k1gFnSc1	brána
;	;	kIx,	;
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
408	[number]	k4	408
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85946	[number]	k4	85946
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
S.	S.	kA	S.
180	[number]	k4	180
<g/>
–	–	k?	–
<g/>
182	[number]	k4	182
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
INGRAO	INGRAO	kA	INGRAO
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
W.	W.	kA	W.
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
:	:	kIx,	:
der	drát	k5eAaImRp2nS	drát
"	"	kIx"	"
<g/>
vergessene	vergessen	k1gInSc5	vergessen
<g/>
"	"	kIx"	"
Kaiser	Kaiser	k1gMnSc1	Kaiser
<g/>
.	.	kIx.	.
</s>
<s>
Graz	Graz	k1gInSc1	Graz
<g/>
:	:	kIx,	:
Styria	Styrium	k1gNnSc2	Styrium
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
293	[number]	k4	293
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
222	[number]	k4	222
<g/>
-	-	kIx~	-
<g/>
11399	[number]	k4	11399
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MIKULEC	Mikulec	k1gMnSc1	Mikulec
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
BĚLINA	Bělina	k1gMnSc1	Bělina
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
;	;	kIx,	;
VLNAS	VLNAS	kA	VLNAS
<g/>
,	,	kIx,	,
Vít	Vít	k1gMnSc1	Vít
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
dějiny	dějiny	k1gFnPc1	dějiny
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
IX	IX	kA	IX
<g/>
.	.	kIx.	.
1683	[number]	k4	1683
<g/>
-	-	kIx~	-
<g/>
1740	[number]	k4	1740
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Paseka	paseka	k1gFnSc1	paseka
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
860	[number]	k4	860
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7432	[number]	k4	7432
<g/>
-	-	kIx~	-
<g/>
105	[number]	k4	105
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
URFUS	URFUS	kA	URFUS
<g/>
,	,	kIx,	,
Valentin	Valentin	k1gMnSc1	Valentin
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
:	:	kIx,	:
nekorunovaný	korunovaný	k2eNgInSc1d1	nekorunovaný
Habsburk	Habsburk	k1gInSc1	Habsburk
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
trůně	trůn	k1gInSc6	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
172	[number]	k4	172
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
211	[number]	k4	211
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VLNAS	VLNAS	kA	VLNAS
<g/>
,	,	kIx,	,
Vít	Vít	k1gMnSc1	Vít
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
In	In	k1gMnSc1	In
<g/>
:	:	kIx,	:
RYANTOVÁ	RYANTOVÁ	kA	RYANTOVÁ
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
<g/>
;	;	kIx,	;
VOREL	Vorel	k1gMnSc1	Vorel
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
králové	král	k1gMnPc1	král
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
940	[number]	k4	940
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
421	[number]	k4	421
<g/>
-	-	kIx~	-
<g/>
431	[number]	k4	431
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VONDRA	Vondra	k1gMnSc1	Vondra
<g/>
,	,	kIx,	,
Roman	Roman	k1gMnSc1	Roman
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
země	zem	k1gFnPc1	zem
v	v	k7c6	v
letech	let	k1gInPc6	let
1705	[number]	k4	1705
<g/>
–	–	k?	–
<g/>
1792	[number]	k4	1792
:	:	kIx,	:
doba	doba	k1gFnSc1	doba
absolutismu	absolutismus	k1gInSc2	absolutismus
<g/>
,	,	kIx,	,
osvícenství	osvícenství	k1gNnSc2	osvícenství
<g/>
,	,	kIx,	,
paruk	paruka	k1gFnPc2	paruka
a	a	k8xC	a
třírohých	třírohý	k2eAgInPc2d1	třírohý
klobouků	klobouk	k1gInPc2	klobouk
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
384	[number]	k4	384
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
448	[number]	k4	448
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Josef	Josefa	k1gFnPc2	Josefa
I.	I.	kA	I.
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Josef	Josef	k1gMnSc1	Josef
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
Autor	autor	k1gMnSc1	autor
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Reskript	reskript	k1gInSc1	reskript
císaře	císař	k1gMnSc2	císař
Josefa	Josef	k1gMnSc2	Josef
I.	I.	kA	I.
českým	český	k2eAgMnPc3d1	český
stavům	stav	k1gInPc3	stav
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
