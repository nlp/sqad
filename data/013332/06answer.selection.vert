<s>
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1678	[number]	k4	1678
Vídeň	Vídeň	k1gFnSc1	Vídeň
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1711	[number]	k4	1711
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
a	a	k8xC	a
uherský	uherský	k2eAgMnSc1d1	uherský
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
moravský	moravský	k2eAgMnSc1d1	moravský
markrabě	markrabě	k1gMnSc1	markrabě
atd.	atd.	kA	atd.
<g/>
,	,	kIx,	,
předposlední	předposlední	k2eAgMnSc1d1	předposlední
člen	člen	k1gMnSc1	člen
dynastie	dynastie	k1gFnSc2	dynastie
Habsburků	Habsburk	k1gMnPc2	Habsburk
v	v	k7c6	v
mužské	mužský	k2eAgFnSc6d1	mužská
linii	linie	k1gFnSc6	linie
<g/>
.	.	kIx.	.
</s>
