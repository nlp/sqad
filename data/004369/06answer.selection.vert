<s>
Nepočítáme	počítat	k5eNaImIp1nP	počítat
<g/>
-li	i	k?	-li
nedokončenou	dokončený	k2eNgFnSc4d1	nedokončená
operu	opera	k1gFnSc4	opera
Die	Die	k1gMnSc1	Die
Hochzeit	Hochzeit	k1gMnSc1	Hochzeit
(	(	kIx(	(
<g/>
Svatba	svatba	k1gFnSc1	svatba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
uvedeny	uvést	k5eAaPmNgFnP	uvést
všechny	všechen	k3xTgFnPc1	všechen
Wagnerovy	Wagnerův	k2eAgFnPc1d1	Wagnerova
opery	opera	k1gFnPc1	opera
kromě	kromě	k7c2	kromě
Das	Das	k1gMnPc2	Das
Liebesverbot	Liebesverbot	k1gInSc4	Liebesverbot
oder	odra	k1gFnPc2	odra
Der	drát	k5eAaImRp2nS	drát
Novize	Novize	k1gFnPc4	Novize
von	von	k1gInSc4	von
Palermo	Palermo	k1gNnSc1	Palermo
(	(	kIx(	(
<g/>
Zápověď	zápověď	k1gFnSc1	zápověď
lásky	láska	k1gFnSc2	láska
aneb	aneb	k?	aneb
Novicka	novicka	k1gFnSc1	novicka
z	z	k7c2	z
Palerma	Palermo	k1gNnSc2	Palermo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
