<p>
<s>
Lahar	lahar	k1gInSc1	lahar
(	(	kIx(	(
<g/>
termín	termín	k1gInSc1	termín
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Indonésie	Indonésie	k1gFnSc2	Indonésie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
sopečný	sopečný	k2eAgInSc1d1	sopečný
bahnotok	bahnotok	k1gInSc1	bahnotok
je	být	k5eAaImIp3nS	být
doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
jev	jev	k1gInSc1	jev
při	při	k7c6	při
některých	některý	k3yIgFnPc6	některý
sopečných	sopečný	k2eAgFnPc6d1	sopečná
erupcích	erupce	k1gFnPc6	erupce
<g/>
.	.	kIx.	.
</s>
<s>
Lahar	lahar	k1gInSc1	lahar
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
tekoucí	tekoucí	k2eAgFnSc7d1	tekoucí
směsí	směs	k1gFnSc7	směs
sopečného	sopečný	k2eAgInSc2d1	sopečný
popela	popel	k1gInSc2	popel
<g/>
,	,	kIx,	,
úlomků	úlomek	k1gInPc2	úlomek
ztuhlé	ztuhlý	k2eAgFnSc2d1	ztuhlá
lávy	láva	k1gFnSc2	láva
a	a	k8xC	a
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lahary	lahar	k1gInPc1	lahar
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
nebezpečným	bezpečný	k2eNgInSc7d1	nebezpečný
projevem	projev	k1gInSc7	projev
sopečné	sopečný	k2eAgFnSc2d1	sopečná
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
možná	možná	k9	možná
nebezpečnějším	bezpečný	k2eNgInSc6d2	nebezpečnější
než	než	k8xS	než
tekoucí	tekoucí	k2eAgFnSc1d1	tekoucí
láva	láva	k1gFnSc1	láva
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gFnSc1	jejich
velká	velký	k2eAgFnSc1d1	velká
mobilita	mobilita	k1gFnSc1	mobilita
(	(	kIx(	(
<g/>
až	až	k9	až
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
metrů	metr	k1gInPc2	metr
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
)	)	kIx)	)
a	a	k8xC	a
rychlost	rychlost	k1gFnSc4	rychlost
jejich	jejich	k3xOp3gNnSc2	jejich
šíření	šíření	k1gNnSc2	šíření
je	být	k5eAaImIp3nS	být
srovnatelná	srovnatelný	k2eAgFnSc1d1	srovnatelná
s	s	k7c7	s
povodňovou	povodňový	k2eAgFnSc7d1	povodňová
vlnou	vlna	k1gFnSc7	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgFnPc2d3	nejstarší
katastrof	katastrofa	k1gFnPc2	katastrofa
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
79	[number]	k4	79
při	při	k7c6	při
erupci	erupce	k1gFnSc6	erupce
sopky	sopka	k1gFnSc2	sopka
Vesuv	Vesuv	k1gInSc1	Vesuv
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
laharem	lahar	k1gInSc7	lahar
zasáhnuto	zasáhnut	k2eAgNnSc1d1	zasáhnuto
město	město	k1gNnSc1	město
Herculaneum	Herculaneum	k1gInSc1	Herculaneum
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
velká	velký	k2eAgFnSc1d1	velká
katastrofa	katastrofa	k1gFnSc1	katastrofa
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
13	[number]	k4	13
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1985	[number]	k4	1985
v	v	k7c6	v
Kolumbii	Kolumbie	k1gFnSc6	Kolumbie
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
Armero	Armero	k1gNnSc1	Armero
pochováno	pochován	k2eAgNnSc1d1	pochováno
pod	pod	k7c7	pod
osmimetrovou	osmimetrový	k2eAgFnSc7d1	osmimetrová
vrstvou	vrstva	k1gFnSc7	vrstva
laharu	lahar	k1gInSc2	lahar
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
na	na	k7c6	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
40	[number]	k4	40
km	km	kA	km
vzdálené	vzdálený	k2eAgFnSc6d1	vzdálená
eruptující	eruptující	k2eAgFnSc6d1	eruptující
sopce	sopka	k1gFnSc6	sopka
Nevado	Nevada	k1gFnSc5	Nevada
del	del	k?	del
Ruiz	Ruiza	k1gFnPc2	Ruiza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některá	některý	k3yIgNnPc1	některý
sídla	sídlo	k1gNnPc1	sídlo
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgInPc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
sopek	sopka	k1gFnPc2	sopka
s	s	k7c7	s
pravděpodobným	pravděpodobný	k2eAgInSc7d1	pravděpodobný
výskytem	výskyt	k1gInSc7	výskyt
laharů	lahar	k1gInPc2	lahar
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Rainier	Rainier	k1gInSc1	Rainier
v	v	k7c6	v
USA	USA	kA	USA
nebo	nebo	k8xC	nebo
Ruapehu	Ruapeha	k1gFnSc4	Ruapeha
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
vypracovaný	vypracovaný	k2eAgInSc4d1	vypracovaný
systém	systém	k1gInSc4	systém
včasné	včasný	k2eAgFnSc2d1	včasná
výstrahy	výstraha	k1gFnSc2	výstraha
před	před	k7c7	před
blížícím	blížící	k2eAgMnSc7d1	blížící
se	se	k3xPyFc4	se
laharem	lahar	k1gInSc7	lahar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
==	==	k?	==
</s>
</p>
<p>
<s>
Roztavení	roztavení	k1gNnSc1	roztavení
sněhové	sněhový	k2eAgFnSc2d1	sněhová
nebo	nebo	k8xC	nebo
ledové	ledový	k2eAgFnSc2d1	ledová
pokrývky	pokrývka	k1gFnSc2	pokrývka
sopky	sopka	k1gFnSc2	sopka
při	při	k7c6	při
erupci	erupce	k1gFnSc6	erupce
</s>
</p>
<p>
<s>
Zvýšení	zvýšení	k1gNnSc1	zvýšení
srážek	srážka	k1gFnPc2	srážka
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
protržení	protržení	k1gNnSc1	protržení
kráterového	kráterový	k2eAgNnSc2d1	kráterové
jezera	jezero	k1gNnSc2	jezero
a	a	k8xC	a
smíchání	smíchání	k1gNnSc2	smíchání
se	se	k3xPyFc4	se
starším	starý	k2eAgInSc7d2	starší
sopečným	sopečný	k2eAgInSc7d1	sopečný
materiálem	materiál	k1gInSc7	materiál
na	na	k7c6	na
svazích	svah	k1gInPc6	svah
</s>
</p>
<p>
<s>
Zintenzivnění	zintenzivnění	k1gNnSc1	zintenzivnění
srážek	srážka	k1gFnPc2	srážka
do	do	k7c2	do
vrstev	vrstva	k1gFnPc2	vrstva
nezpevněných	zpevněný	k2eNgFnPc2d1	nezpevněná
sopečných	sopečný	k2eAgFnPc2d1	sopečná
usazenin	usazenina	k1gFnPc2	usazenina
a	a	k8xC	a
popela	popel	k1gInSc2	popel
</s>
</p>
<p>
<s>
Vznik	vznik	k1gInSc4	vznik
laharu	lahar	k1gInSc2	lahar
mohou	moct	k5eAaImIp3nP	moct
podpořit	podpořit	k5eAaPmF	podpořit
i	i	k9	i
menší	malý	k2eAgNnPc1d2	menší
drobná	drobný	k2eAgNnPc1d1	drobné
zemětřesení	zemětřesení	k1gNnPc1	zemětřesení
</s>
</p>
<p>
<s>
==	==	k?	==
Katastrofy	katastrofa	k1gFnPc1	katastrofa
zapříčiněné	zapříčiněný	k2eAgInPc4d1	zapříčiněný
lahary	lahar	k1gInPc4	lahar
==	==	k?	==
</s>
</p>
<p>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
bezprostředním	bezprostřední	k2eAgNnSc6d1	bezprostřední
okolí	okolí	k1gNnSc6	okolí
sopek	sopka	k1gFnPc2	sopka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
je	být	k5eAaImIp3nS	být
nejvíce	hodně	k6eAd3	hodně
ohroženo	ohrozit	k5eAaPmNgNnS	ohrozit
erupcemi	erupce	k1gFnPc7	erupce
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
bez	bez	k7c2	bez
trvalého	trvalý	k2eAgNnSc2d1	trvalé
osídlení	osídlení	k1gNnSc2	osídlení
<g/>
,	,	kIx,	,
širší	široký	k2eAgNnSc4d2	širší
okolí	okolí	k1gNnSc4	okolí
sopky	sopka	k1gFnSc2	sopka
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
kvůli	kvůli	k7c3	kvůli
úrodné	úrodný	k2eAgFnSc3d1	úrodná
půdě	půda	k1gFnSc3	půda
hustěji	husto	k6eAd2	husto
osídleno	osídlit	k5eAaPmNgNnS	osídlit
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
domnívat	domnívat	k5eAaImF	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hrozba	hrozba	k1gFnSc1	hrozba
sopky	sopka	k1gFnSc2	sopka
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
oblastech	oblast	k1gFnPc6	oblast
nižší	nízký	k2eAgInSc1d2	nižší
<g/>
.	.	kIx.	.
</s>
<s>
Lahary	lahar	k1gInPc1	lahar
proto	proto	k8xC	proto
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
oblastech	oblast	k1gFnPc6	oblast
mají	mít	k5eAaImIp3nP	mít
značný	značný	k2eAgInSc4d1	značný
ničivý	ničivý	k2eAgInSc4d1	ničivý
potenciál	potenciál	k1gInSc4	potenciál
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
díky	díky	k7c3	díky
rychlému	rychlý	k2eAgMnSc3d1	rychlý
a	a	k8xC	a
širokému	široký	k2eAgInSc3d1	široký
průtoku	průtok	k1gInSc3	průtok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
24	[number]	k4	24
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1953	[number]	k4	1953
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
největší	veliký	k2eAgFnSc3d3	veliký
železniční	železniční	k2eAgFnSc3d1	železniční
nehodě	nehoda	k1gFnSc3	nehoda
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgFnPc1d1	zvaná
katastrofa	katastrofa	k1gFnSc1	katastrofa
u	u	k7c2	u
Tangiwai	Tangiwa	k1gFnSc2	Tangiwa
<g/>
.	.	kIx.	.
</s>
<s>
Lahar	lahar	k1gInSc1	lahar
ze	z	k7c2	z
svahu	svah	k1gInSc2	svah
vulkánu	vulkán	k1gInSc2	vulkán
Ruapehu	Ruapeh	k1gInSc2	Ruapeh
silně	silně	k6eAd1	silně
poškodil	poškodit	k5eAaPmAgInS	poškodit
železniční	železniční	k2eAgInSc1d1	železniční
most	most	k1gInSc1	most
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
Whangaehu	Whangaeh	k1gInSc2	Whangaeh
u	u	k7c2	u
Tangiwai	Tangiwa	k1gFnSc2	Tangiwa
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
po	po	k7c6	po
mostě	most	k1gInSc6	most
projížděl	projíždět	k5eAaImAgMnS	projíždět
noční	noční	k2eAgInSc4d1	noční
vlak	vlak	k1gInSc4	vlak
Wellington-Auckland	Wellington-Aucklanda	k1gFnPc2	Wellington-Aucklanda
a	a	k8xC	a
zřítil	zřítit	k5eAaPmAgInS	zřítit
se	se	k3xPyFc4	se
do	do	k7c2	do
proudu	proud	k1gInSc2	proud
bahna	bahno	k1gNnSc2	bahno
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
život	život	k1gInSc4	život
přišlo	přijít	k5eAaPmAgNnS	přijít
151	[number]	k4	151
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
obětí	oběť	k1gFnPc2	oběť
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nepodařilo	podařit	k5eNaPmAgNnS	podařit
nalézt	nalézt	k5eAaBmF	nalézt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lahar	lahar	k1gInSc1	lahar
při	při	k7c6	při
erupci	erupce	k1gFnSc6	erupce
sopky	sopka	k1gFnSc2	sopka
Mount	Mount	k1gInSc1	Mount
St.	st.	kA	st.
Helens	Helens	k1gInSc4	Helens
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1980	[number]	k4	1980
velmi	velmi	k6eAd1	velmi
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
rozsahu	rozsah	k1gInSc3	rozsah
ničivé	ničivý	k2eAgFnSc2d1	ničivá
katastrofy	katastrofa	k1gFnSc2	katastrofa
<g/>
.	.	kIx.	.
</s>
<s>
Lavina	lavina	k1gFnSc1	lavina
bahna	bahno	k1gNnSc2	bahno
a	a	k8xC	a
kamení	kamení	k1gNnSc2	kamení
a	a	k8xC	a
popela	popel	k1gInSc2	popel
se	se	k3xPyFc4	se
z	z	k7c2	z
úbočí	úbočí	k1gNnSc2	úbočí
řítila	řítit	k5eAaImAgFnS	řítit
rychlostí	rychlost	k1gFnSc7	rychlost
až	až	k6eAd1	až
75	[number]	k4	75
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
a	a	k8xC	a
překonala	překonat	k5eAaPmAgFnS	překonat
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
více	hodně	k6eAd2	hodně
než	než	k8xS	než
25	[number]	k4	25
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
13	[number]	k4	13
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1985	[number]	k4	1985
způsobil	způsobit	k5eAaPmAgInS	způsobit
lahar	lahar	k1gInSc1	lahar
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
při	při	k7c6	při
erupci	erupce	k1gFnSc6	erupce
vulkánu	vulkán	k1gInSc2	vulkán
Nevado	Nevada	k1gFnSc5	Nevada
del	del	k?	del
Ruiz	Ruiz	k1gMnSc1	Ruiz
v	v	k7c6	v
Kolumbii	Kolumbie	k1gFnSc6	Kolumbie
co	co	k3yRnSc1	co
do	do	k7c2	do
počtu	počet	k1gInSc2	počet
druhé	druhý	k4xOgNnSc4	druhý
největší	veliký	k2eAgNnSc4d3	veliký
neštěstí	neštěstí	k1gNnSc4	neštěstí
při	při	k7c6	při
erupci	erupce	k1gFnSc6	erupce
sopky	sopka	k1gFnSc2	sopka
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
5	[number]	k4	5
metrů	metr	k1gInPc2	metr
vysoká	vysoký	k2eAgFnSc1d1	vysoká
přívalová	přívalový	k2eAgFnSc1d1	přívalová
vlna	vlna	k1gFnSc1	vlna
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
47	[number]	k4	47
kilometrů	kilometr	k1gInPc2	kilometr
vzdálené	vzdálený	k2eAgNnSc1d1	vzdálené
město	město	k1gNnSc1	město
Armero	Armero	k1gNnSc4	Armero
za	za	k7c4	za
dvě	dva	k4xCgFnPc4	dva
a	a	k8xC	a
půl	půl	k1xP	půl
hodiny	hodina	k1gFnSc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Zahynulo	zahynout	k5eAaPmAgNnS	zahynout
28	[number]	k4	28
700	[number]	k4	700
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
29	[number]	k4	29
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1971	[number]	k4	1971
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
na	na	k7c6	na
svahu	svah	k1gInSc6	svah
sopky	sopka	k1gFnSc2	sopka
Villarrica	Villarric	k1gInSc2	Villarric
(	(	kIx(	(
<g/>
2847	[number]	k4	2847
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
v	v	k7c6	v
Chile	Chile	k1gNnSc6	Chile
lahar	lahar	k1gInSc4	lahar
s	s	k7c7	s
celkovou	celkový	k2eAgFnSc7d1	celková
délkou	délka	k1gFnSc7	délka
14	[number]	k4	14
km	km	kA	km
<g/>
,	,	kIx,	,
128	[number]	k4	128
metrů	metr	k1gInPc2	metr
široký	široký	k2eAgMnSc1d1	široký
a	a	k8xC	a
až	až	k9	až
8	[number]	k4	8
metrů	metr	k1gInPc2	metr
vysoký	vysoký	k2eAgInSc1d1	vysoký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
se	se	k3xPyFc4	se
rozlil	rozlít	k5eAaPmAgInS	rozlít
lahar	lahar	k1gInSc1	lahar
ze	z	k7c2	z
sopky	sopka	k1gFnSc2	sopka
Ruapehu	Ruapeh	k1gInSc2	Ruapeh
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
ohrožení	ohrožení	k1gNnSc3	ohrožení
životů	život	k1gInPc2	život
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
úřady	úřad	k1gInPc1	úřad
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
podél	podél	k7c2	podél
toku	tok	k1gInSc2	tok
řeky	řeka	k1gFnSc2	řeka
Whangaehu	Whangaeh	k1gInSc2	Whangaeh
přijaly	přijmout	k5eAaPmAgInP	přijmout
včas	včas	k6eAd1	včas
účinná	účinný	k2eAgNnPc1d1	účinné
opatření	opatření	k1gNnPc1	opatření
a	a	k8xC	a
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
bylo	být	k5eAaImAgNnS	být
varováno	varovat	k5eAaImNgNnS	varovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Lahár	Lahár	k1gInSc4	Lahár
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
Lahar	lahar	k1gInSc4	lahar
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
SIGURDSSON	SIGURDSSON	kA	SIGURDSSON
<g/>
,	,	kIx,	,
Haraldur	Haraldur	k1gMnSc1	Haraldur
<g/>
;	;	kIx,	;
VALLANCE	VALLANCE	kA	VALLANCE
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
W.	W.	kA	W.
Encyclopedia	Encyclopedium	k1gNnSc2	Encyclopedium
of	of	k?	of
Volcanoes	Volcanoes	k1gMnSc1	Volcanoes
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Academic	Academic	k1gMnSc1	Academic
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
643140	[number]	k4	643140
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Lahars	Laharsa	k1gFnPc2	Laharsa
<g/>
,	,	kIx,	,
s.	s.	k?	s.
601	[number]	k4	601
<g/>
-	-	kIx~	-
<g/>
616	[number]	k4	616
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Lahar	lahar	k1gInSc1	lahar
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Popis	popis	k1gInSc1	popis
laharů	lahar	k1gInPc2	lahar
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
sopečných	sopečný	k2eAgInPc2d1	sopečný
jevů	jev	k1gInPc2	jev
s	s	k7c7	s
fotografiemi	fotografia	k1gFnPc7	fotografia
</s>
</p>
