<s>
Československo	Československo	k1gNnSc1	Československo
byl	být	k5eAaImAgInS	být
stát	stát	k5eAaImF	stát
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
existoval	existovat	k5eAaImAgMnS	existovat
(	(	kIx(	(
<g/>
s	s	k7c7	s
krátkou	krátký	k2eAgFnSc7d1	krátká
přestávkou	přestávka	k1gFnSc7	přestávka
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
)	)	kIx)	)
od	od	k7c2	od
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1918	[number]	k4	1918
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Československo	Československo	k1gNnSc1	Československo
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
jako	jako	k8xS	jako
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nástupnických	nástupnický	k2eAgInPc2d1	nástupnický
států	stát	k1gInPc2	stát
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2	Rakouska-Uhersk
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
území	území	k1gNnSc1	území
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
Českého	český	k2eAgNnSc2d1	české
Slezska	Slezsko	k1gNnSc2	Slezsko
(	(	kIx(	(
<g/>
jihovýchodní	jihovýchodní	k2eAgFnSc1d1	jihovýchodní
část	část	k1gFnSc1	část
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Slovenska	Slovensko	k1gNnSc2	Slovensko
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
i	i	k9	i
Podkarpatské	podkarpatský	k2eAgFnSc6d1	Podkarpatská
Rusi	Rus	k1gFnSc6	Rus
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Horní	horní	k2eAgFnSc1d1	horní
Uhry	Uhry	k1gFnPc1	Uhry
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Československo	Československo	k1gNnSc1	Československo
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
k	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosinci	prosinec	k1gInSc6	prosinec
1992	[number]	k4	1992
rozdělením	rozdělení	k1gNnSc7	rozdělení
na	na	k7c4	na
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
a	a	k8xC	a
Slovenskou	slovenský	k2eAgFnSc4d1	slovenská
republiku	republika	k1gFnSc4	republika
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Název	název	k1gInSc1	název
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
název	název	k1gInSc1	název
státu	stát	k1gInSc2	stát
procházel	procházet	k5eAaImAgInS	procházet
vývojem	vývoj	k1gInSc7	vývoj
dle	dle	k7c2	dle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
měnilo	měnit	k5eAaImAgNnS	měnit
jeho	jeho	k3xOp3gNnSc1	jeho
politické	politický	k2eAgNnSc1d1	politické
zřízení	zřízení	k1gNnSc1	zřízení
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálním	oficiální	k2eAgInSc7d1	oficiální
název	název	k1gInSc1	název
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1918	[number]	k4	1918
až	až	k9	až
1920	[number]	k4	1920
byl	být	k5eAaImAgMnS	být
Republika	republika	k1gFnSc1	republika
Československá	československý	k2eAgFnSc1d1	Československá
nebo	nebo	k8xC	nebo
Česko-Slovenský	českolovenský	k2eAgInSc1d1	česko-slovenský
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1920	[number]	k4	1920
až	až	k8xS	až
1938	[number]	k4	1938
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
název	název	k1gInSc1	název
Československá	československý	k2eAgFnSc1d1	Československá
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
-	-	kIx~	-
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
název	název	k1gInSc1	název
Česko-Slovenská	českolovenský	k2eAgFnSc1d1	česko-slovenská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
bylo	být	k5eAaImAgNnS	být
opět	opět	k6eAd1	opět
užíváno	užívat	k5eAaImNgNnS	užívat
názvu	název	k1gInSc3	název
Československá	československý	k2eAgFnSc1d1	Československá
republika	republika	k1gFnSc1	republika
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
ideologii	ideologie	k1gFnSc3	ideologie
vládnoucí	vládnoucí	k2eAgFnSc2d1	vládnoucí
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Československa	Československo	k1gNnSc2	Československo
název	název	k1gInSc1	název
státu	stát	k1gInSc2	stát
změněn	změnit	k5eAaPmNgInS	změnit
na	na	k7c4	na
Československá	československý	k2eAgFnSc1d1	Československá
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
preambuli	preambule	k1gFnSc6	preambule
ústavy	ústava	k1gFnSc2	ústava
se	se	k3xPyFc4	se
nový	nový	k2eAgInSc4d1	nový
název	název	k1gInSc4	název
zdůvodňoval	zdůvodňovat	k5eAaImAgMnS	zdůvodňovat
vítězstvím	vítězství	k1gNnSc7	vítězství
socialismu	socialismus	k1gInSc2	socialismus
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
po	po	k7c6	po
němž	jenž	k3xRgInSc6	jenž
měl	mít	k5eAaImAgInS	mít
následovat	následovat	k5eAaImF	následovat
přechod	přechod	k1gInSc1	přechod
ke	k	k7c3	k
konečnému	konečný	k2eAgNnSc3d1	konečné
stádiu	stádium	k1gNnSc3	stádium
dějin	dějiny	k1gFnPc2	dějiny
-	-	kIx~	-
komunismu	komunismus	k1gInSc2	komunismus
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
názvem	název	k1gInSc7	název
byl	být	k5eAaImAgInS	být
změněn	změnit	k5eAaPmNgInS	změnit
také	také	k9	také
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
režimu	režim	k1gInSc2	režim
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
byl	být	k5eAaImAgInS	být
název	název	k1gInSc1	název
státu	stát	k1gInSc2	stát
změněn	změnit	k5eAaPmNgInS	změnit
na	na	k7c4	na
Československá	československý	k2eAgFnSc1d1	Československá
federativní	federativní	k2eAgFnSc1d1	federativní
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vleklých	vleklý	k2eAgInPc6d1	vleklý
sporech	spor	k1gInPc6	spor
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
schválen	schválit	k5eAaPmNgInS	schválit
název	název	k1gInSc1	název
(	(	kIx(	(
<g/>
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
pravopisných	pravopisný	k2eAgNnPc2d1	pravopisné
pravidel	pravidlo	k1gNnPc2	pravidlo
nesprávný	správný	k2eNgInSc1d1	nesprávný
<g/>
)	)	kIx)	)
Česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
Federativní	federativní	k2eAgFnSc1d1	federativní
Republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
až	až	k9	až
do	do	k7c2	do
zániku	zánik	k1gInSc2	zánik
státu	stát	k1gInSc2	stát
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
současných	současný	k2eAgNnPc2d1	současné
Pravidel	pravidlo	k1gNnPc2	pravidlo
slovenského	slovenský	k2eAgInSc2d1	slovenský
pravopisu	pravopis	k1gInSc2	pravopis
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
krátký	krátký	k2eAgInSc1d1	krátký
název	název	k1gInSc1	název
psán	psán	k2eAgInSc1d1	psán
se	s	k7c7	s
spojovníkem	spojovník	k1gInSc7	spojovník
jako	jako	k8xC	jako
Česko-Slovensko	Česko-Slovensko	k1gNnSc1	Česko-Slovensko
(	(	kIx(	(
<g/>
adjektivum	adjektivum	k1gNnSc1	adjektivum
česko-slovenský	českolovenský	k2eAgMnSc1d1	česko-slovenský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
s	s	k7c7	s
dobovými	dobový	k2eAgInPc7d1	dobový
dokumenty	dokument	k1gInPc7	dokument
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
<g/>
.	.	kIx.	.
</s>
<s>
Hlavně	hlavně	k9	hlavně
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
to	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
výkladu	výklad	k1gInSc3	výklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
tvar	tvar	k1gInSc1	tvar
Československo	Československo	k1gNnSc1	Československo
(	(	kIx(	(
<g/>
adjektivum	adjektivum	k1gNnSc1	adjektivum
československý	československý	k2eAgMnSc1d1	československý
<g/>
)	)	kIx)	)
není	být	k5eNaImIp3nS	být
přípustný	přípustný	k2eAgInSc1d1	přípustný
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
příliš	příliš	k6eAd1	příliš
rigidním	rigidní	k2eAgInSc7d1	rigidní
výkladem	výklad	k1gInSc7	výklad
<g/>
.	.	kIx.	.
</s>
<s>
Slovník	slovník	k1gInSc1	slovník
současného	současný	k2eAgInSc2d1	současný
slovenského	slovenský	k2eAgInSc2d1	slovenský
jazyka	jazyk	k1gInSc2	jazyk
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
udává	udávat	k5eAaImIp3nS	udávat
oba	dva	k4xCgInPc4	dva
tvary	tvar	k1gInPc4	tvar
právě	právě	k9	právě
v	v	k7c6	v
historickém	historický	k2eAgInSc6d1	historický
kontextu	kontext	k1gInSc6	kontext
<g/>
,	,	kIx,	,
pojem	pojem	k1gInSc1	pojem
Československo	Československo	k1gNnSc1	Československo
připouští	připouštět	k5eAaImIp3nS	připouštět
i	i	k9	i
internetová	internetový	k2eAgFnSc1d1	internetová
jazyková	jazykový	k2eAgFnSc1d1	jazyková
poradna	poradna	k1gFnSc1	poradna
Jazykovedného	Jazykovedný	k2eAgInSc2d1	Jazykovedný
ústavu	ústav	k1gInSc2	ústav
Ľudovíta	Ľudovíta	k1gMnSc1	Ľudovíta
Štúra	Štúra	k1gMnSc1	Štúra
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
První	první	k4xOgFnSc1	první
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc4d1	původní
koncepci	koncepce	k1gFnSc4	koncepce
nově	nova	k1gFnSc3	nova
vzniklého	vzniklý	k2eAgNnSc2d1	vzniklé
Československa	Československo	k1gNnSc2	Československo
nastínil	nastínit	k5eAaPmAgMnS	nastínit
na	na	k7c6	na
mírové	mírový	k2eAgFnSc6d1	mírová
konferenci	konference	k1gFnSc6	konference
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
předložil	předložit	k5eAaPmAgMnS	předložit
státníkům	státník	k1gMnPc3	státník
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
memorand	memorandum	k1gNnPc2	memorandum
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
takto	takto	k6eAd1	takto
předložených	předložený	k2eAgInPc2d1	předložený
dokumentů	dokument	k1gInPc2	dokument
byla	být	k5eAaImAgFnS	být
Nóta	nóta	k1gFnSc1	nóta
o	o	k7c6	o
národnostním	národnostní	k2eAgInSc6d1	národnostní
režimu	režim	k1gInSc6	režim
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
Beneš	Beneš	k1gMnSc1	Beneš
přislíbil	přislíbit	k5eAaPmAgMnS	přislíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nově	nově	k6eAd1	nově
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
Československá	československý	k2eAgFnSc1d1	Československá
republika	republika	k1gFnSc1	republika
"	"	kIx"	"
<g/>
zamýšlí	zamýšlet	k5eAaImIp3nS	zamýšlet
vybudovat	vybudovat	k5eAaPmF	vybudovat
organizaci	organizace	k1gFnSc4	organizace
státu	stát	k1gInSc2	stát
na	na	k7c6	na
přijetí	přijetí	k1gNnSc6	přijetí
národních	národní	k2eAgNnPc2d1	národní
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
zásad	zásada	k1gFnPc2	zásada
uplatňovaných	uplatňovaný	k2eAgFnPc2d1	uplatňovaná
v	v	k7c6	v
ústavě	ústava	k1gFnSc6	ústava
Švýcarské	švýcarský	k2eAgFnSc2d1	švýcarská
republiky	republika	k1gFnSc2	republika
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
nótě	nóta	k1gFnSc6	nóta
uvedeno	uvést	k5eAaPmNgNnS	uvést
například	například	k6eAd1	například
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
oficiálním	oficiální	k2eAgInSc7d1	oficiální
jazykem	jazyk	k1gInSc7	jazyk
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
bude	být	k5eAaImBp3nS	být
čeština	čeština	k1gFnSc1	čeština
a	a	k8xC	a
rovnoprávným	rovnoprávný	k2eAgInSc7d1	rovnoprávný
jazykem	jazyk	k1gInSc7	jazyk
s	s	k7c7	s
češtinou	čeština	k1gFnSc7	čeština
bude	být	k5eAaImBp3nS	být
němčina	němčina	k1gFnSc1	němčina
a	a	k8xC	a
že	že	k8xS	že
veřejné	veřejný	k2eAgInPc1d1	veřejný
úřady	úřad	k1gInPc1	úřad
budou	být	k5eAaImBp3nP	být
otevřeny	otevřít	k5eAaPmNgInP	otevřít
všem	všecek	k3xTgMnPc3	všecek
národnostem	národnost	k1gFnPc3	národnost
obývajícím	obývající	k2eAgNnSc6d1	obývající
území	území	k1gNnSc6	území
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
Československa	Československo	k1gNnSc2	Československo
však	však	k9	však
převážila	převážit	k5eAaPmAgFnS	převážit
myšlenka	myšlenka	k1gFnSc1	myšlenka
vytvořit	vytvořit	k5eAaPmF	vytvořit
jednotný	jednotný	k2eAgInSc4d1	jednotný
<g/>
,	,	kIx,	,
demokratický	demokratický	k2eAgInSc4d1	demokratický
<g/>
,	,	kIx,	,
centralistický	centralistický	k2eAgInSc4d1	centralistický
"	"	kIx"	"
<g/>
národní	národní	k2eAgInSc4d1	národní
stát	stát	k1gInSc4	stát
<g/>
"	"	kIx"	"
založený	založený	k2eAgInSc4d1	založený
na	na	k7c6	na
<g/>
,	,	kIx,	,
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
umělé	umělý	k2eAgFnSc2d1	umělá
<g/>
,	,	kIx,	,
ideji	idea	k1gFnSc3	idea
čechoslovakismu	čechoslovakismus	k1gInSc2	čechoslovakismus
<g/>
,	,	kIx,	,
příkladem	příklad	k1gInSc7	příklad
a	a	k8xC	a
garantem	garant	k1gMnSc7	garant
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Francie	Francie	k1gFnSc1	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
území	území	k1gNnSc6	území
se	se	k3xPyFc4	se
hovořilo	hovořit	k5eAaImAgNnS	hovořit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
půl	půl	k6eAd1	půl
tuctem	tucet	k1gInSc7	tucet
jazyků	jazyk	k1gInPc2	jazyk
-	-	kIx~	-
česky	česky	k6eAd1	česky
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
<g/>
,	,	kIx,	,
slovensky	slovensky	k6eAd1	slovensky
<g/>
,	,	kIx,	,
maďarsky	maďarsky	k6eAd1	maďarsky
<g/>
,	,	kIx,	,
polsky	polsky	k6eAd1	polsky
<g/>
,	,	kIx,	,
rusínsky	rusínsky	k6eAd1	rusínsky
a	a	k8xC	a
dalšími	další	k2eAgNnPc7d1	další
nářečími	nářečí	k1gNnPc7	nářečí
ukrajinštiny	ukrajinština	k1gFnSc2	ukrajinština
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
jazyky	jazyk	k1gInPc4	jazyk
patřily	patřit	k5eAaImAgInP	patřit
jidiš	jidiš	k6eAd1	jidiš
<g/>
,	,	kIx,	,
rómština	rómština	k1gFnSc1	rómština
<g/>
,	,	kIx,	,
rumunština	rumunština	k1gFnSc1	rumunština
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
nářečí	nářečí	k1gNnSc1	nářečí
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
v	v	k7c6	v
příhraničí	příhraničí	k1gNnSc6	příhraničí
s	s	k7c7	s
Rumunskem	Rumunsko	k1gNnSc7	Rumunsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sudetskými	sudetský	k2eAgMnPc7d1	sudetský
Němci	Němec	k1gMnPc7	Němec
již	již	k6eAd1	již
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
požadované	požadovaný	k2eAgNnSc4d1	požadované
vytvoření	vytvoření	k1gNnSc4	vytvoření
oddělených	oddělený	k2eAgNnPc2d1	oddělené
jazykových	jazykový	k2eAgNnPc2d1	jazykové
území	území	k1gNnPc2	území
s	s	k7c7	s
vlastní	vlastní	k2eAgFnSc7d1	vlastní
oddělenou	oddělený	k2eAgFnSc7d1	oddělená
správou	správa	k1gFnSc7	správa
bylo	být	k5eAaImAgNnS	být
zakladateli	zakladatel	k1gMnSc3	zakladatel
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c6	na
secesi	secese	k1gFnSc6	secese
německojazyčných	německojazyčný	k2eAgNnPc2d1	německojazyčné
území	území	k1gNnPc2	území
a	a	k8xC	a
německé	německý	k2eAgNnSc1d1	německé
odmítnutí	odmítnutí	k1gNnSc1	odmítnutí
spolupráce	spolupráce	k1gFnSc2	spolupráce
při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
nového	nový	k2eAgInSc2d1	nový
státu	stát	k1gInSc2	stát
odmítnuto	odmítnut	k2eAgNnSc1d1	odmítnuto
<g/>
.	.	kIx.	.
</s>
<s>
Státotvorným	státotvorný	k2eAgInSc7d1	státotvorný
národem	národ	k1gInSc7	národ
byl	být	k5eAaImAgInS	být
prohlášen	prohlášen	k2eAgInSc1d1	prohlášen
národ	národ	k1gInSc1	národ
československý	československý	k2eAgInSc1d1	československý
a	a	k8xC	a
státním	státní	k2eAgInSc7d1	státní
jazykem	jazyk	k1gInSc7	jazyk
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
jazyk	jazyk	k1gInSc1	jazyk
československý	československý	k2eAgInSc1d1	československý
(	(	kIx(	(
<g/>
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
češtiny	čeština	k1gFnSc2	čeština
a	a	k8xC	a
slovenštiny	slovenština	k1gFnSc2	slovenština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Češi	Čech	k1gMnPc1	Čech
a	a	k8xC	a
Slováci	Slovák	k1gMnPc1	Slovák
byli	být	k5eAaImAgMnP	být
vnímáni	vnímat	k5eAaImNgMnP	vnímat
jako	jako	k8xS	jako
dvě	dva	k4xCgFnPc4	dva
větve	větev	k1gFnPc4	větev
jednoho	jeden	k4xCgInSc2	jeden
politického	politický	k2eAgInSc2d1	politický
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
populačně	populačně	k6eAd1	populačně
dominoval	dominovat	k5eAaImAgMnS	dominovat
meziválečnému	meziválečný	k2eAgNnSc3d1	meziválečné
Československu	Československo	k1gNnSc3	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Slováci	Slovák	k1gMnPc1	Slovák
jako	jako	k8xC	jako
samostatný	samostatný	k2eAgInSc4d1	samostatný
národ	národ	k1gInSc4	národ
by	by	kYmCp3nP	by
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
tehdejším	tehdejší	k2eAgNnSc6d1	tehdejší
Československu	Československo	k1gNnSc6	Československo
až	až	k8xS	až
třetí	třetí	k4xOgFnSc7	třetí
nejpočetnější	početní	k2eAgFnSc7d3	nejpočetnější
národností	národnost	k1gFnSc7	národnost
<g/>
,	,	kIx,	,
po	po	k7c6	po
Češích	Čech	k1gMnPc6	Čech
a	a	k8xC	a
Němcích	Němec	k1gMnPc6	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Ústava	ústava	k1gFnSc1	ústava
byla	být	k5eAaImAgFnS	být
dopracována	dopracovat	k5eAaPmNgFnS	dopracovat
ve	v	k7c6	v
2	[number]	k4	2
hod	hod	k1gInSc1	hod
<g/>
.	.	kIx.	.
30	[number]	k4	30
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1920	[number]	k4	1920
a	a	k8xC	a
schválena	schválen	k2eAgFnSc1d1	schválena
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
první	první	k4xOgFnSc1	první
ústavní	ústavní	k2eAgFnSc1d1	ústavní
listina	listina	k1gFnSc1	listina
nových	nový	k2eAgInPc2d1	nový
evropských	evropský	k2eAgInPc2d1	evropský
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
československým	československý	k2eAgMnSc7d1	československý
prezidentem	prezident	k1gMnSc7	prezident
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1918	[number]	k4	1918
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigu	k1gInSc2	Garrigu
Masaryk	Masaryk	k1gMnSc1	Masaryk
(	(	kIx(	(
<g/>
opětovně	opětovně	k6eAd1	opětovně
zvolen	zvolit	k5eAaPmNgMnS	zvolit
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
abdikaci	abdikace	k1gFnSc6	abdikace
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
československých	československý	k2eAgInPc2d1	československý
poměrů	poměr	k1gInPc2	poměr
státu	stát	k1gInSc2	stát
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
životní	životní	k2eAgFnSc7d1	životní
úrovní	úroveň	k1gFnSc7	úroveň
<g/>
,	,	kIx,	,
projevující	projevující	k2eAgFnSc4d1	projevující
se	se	k3xPyFc4	se
také	také	k9	také
výstavbou	výstavba	k1gFnSc7	výstavba
vilových	vilový	k2eAgFnPc2d1	vilová
čtvrtí	čtvrt	k1gFnPc2	čtvrt
většiny	většina	k1gFnSc2	většina
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
vážně	vážně	k6eAd1	vážně
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
celosvětová	celosvětový	k2eAgFnSc1d1	celosvětová
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
krize	krize	k1gFnSc1	krize
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
téměř	téměř	k6eAd1	téměř
1	[number]	k4	1
000	[number]	k4	000
000	[number]	k4	000
nezaměstnaných	nezaměstnaný	k1gMnPc2	nezaměstnaný
<g/>
.	.	kIx.	.
</s>
<s>
Krizí	krize	k1gFnSc7	krize
bylo	být	k5eAaImAgNnS	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
nejvíce	hodně	k6eAd3	hodně
postiženo	postižen	k2eAgNnSc4d1	postiženo
české	český	k2eAgNnSc4d1	české
pohraničí	pohraničí	k1gNnSc4	pohraničí
obývané	obývaný	k2eAgFnSc2d1	obývaná
převážně	převážně	k6eAd1	převážně
českými	český	k2eAgMnPc7d1	český
Němci	Němec	k1gMnPc7	Němec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
především	především	k9	především
nacházel	nacházet	k5eAaImAgMnS	nacházet
nejpostiženější	postižený	k2eAgInSc4d3	Nejpostiženější
spotřební	spotřební	k2eAgInSc4d1	spotřební
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
několikanásobně	několikanásobně	k6eAd1	několikanásobně
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
zemědělském	zemědělský	k2eAgNnSc6d1	zemědělské
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
byl	být	k5eAaImAgInS	být
dobudován	dobudován	k2eAgInSc1d1	dobudován
zimní	zimní	k2eAgInSc1d1	zimní
stadion	stadion	k1gInSc1	stadion
na	na	k7c6	na
Štvanici	Štvanice	k1gFnSc6	Štvanice
s	s	k7c7	s
uspořádáním	uspořádání	k1gNnSc7	uspořádání
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
jehož	jenž	k3xRgMnSc4	jenž
vítěze	vítěz	k1gMnSc4	vítěz
-	-	kIx~	-
Kanadu	Kanada	k1gFnSc4	Kanada
(	(	kIx(	(
<g/>
Československo	Československo	k1gNnSc1	Československo
bylo	být	k5eAaImAgNnS	být
třetí	třetí	k4xOgFnSc7	třetí
<g/>
)	)	kIx)	)
věnoval	věnovat	k5eAaPmAgMnS	věnovat
prezident	prezident	k1gMnSc1	prezident
stříbrný	stříbrný	k1gInSc4	stříbrný
velký	velký	k2eAgInSc4d1	velký
Masarykův	Masarykův	k2eAgInSc4d1	Masarykův
pohár	pohár	k1gInSc4	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
vážným	vážný	k2eAgInSc7d1	vážný
zásahem	zásah	k1gInSc7	zásah
do	do	k7c2	do
vývoje	vývoj	k1gInSc2	vývoj
situace	situace	k1gFnSc2	situace
v	v	k7c6	v
ČSR	ČSR	kA	ČSR
byl	být	k5eAaImAgInS	být
nástup	nástup	k1gInSc1	nástup
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
v	v	k7c6	v
sousedním	sousední	k2eAgNnSc6d1	sousední
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
podnětem	podnět	k1gInSc7	podnět
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
Sudetoněmecké	sudetoněmecký	k2eAgFnSc2d1	Sudetoněmecká
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
vyvolávala	vyvolávat	k5eAaImAgFnS	vyvolávat
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
pohraničí	pohraničí	k1gNnSc6	pohraničí
protičeské	protičeský	k2eAgFnSc2d1	protičeská
nálady	nálada	k1gFnSc2	nálada
<g/>
,	,	kIx,	,
v	v	k7c6	v
čemž	což	k3yQnSc6	což
jí	on	k3xPp3gFnSc7	on
pomáhala	pomáhat	k5eAaImAgFnS	pomáhat
i	i	k9	i
přílišná	přílišný	k2eAgFnSc1d1	přílišná
pasivita	pasivita	k1gFnSc1	pasivita
československé	československý	k2eAgFnSc2d1	Československá
vlády	vláda	k1gFnSc2	vláda
v	v	k7c4	v
řešení	řešení	k1gNnSc4	řešení
problémů	problém	k1gInPc2	problém
důsledků	důsledek	k1gInPc2	důsledek
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
<g/>
,	,	kIx,	,
a	a	k8xC	a
kladla	klást	k5eAaImAgFnS	klást
československým	československý	k2eAgMnPc3d1	československý
oficiálním	oficiální	k2eAgMnPc3d1	oficiální
orgánům	orgán	k1gMnPc3	orgán
požadavky	požadavek	k1gInPc4	požadavek
dle	dle	k7c2	dle
zadání	zadání	k1gNnSc2	zadání
A.	A.	kA	A.
Hitlera	Hitler	k1gMnSc2	Hitler
<g/>
.	.	kIx.	.
</s>
<s>
Československo	Československo	k1gNnSc1	Československo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
těchto	tento	k3xDgFnPc2	tento
událostí	událost	k1gFnPc2	událost
někdy	někdy	k6eAd1	někdy
přezdíváno	přezdíván	k2eAgNnSc1d1	přezdíváno
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
ostrov	ostrov	k1gInSc1	ostrov
demokracie	demokracie	k1gFnSc2	demokracie
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
(	(	kIx(	(
<g/>
po	po	k7c6	po
skonu	skon	k1gInSc6	skon
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
začala	začít	k5eAaPmAgFnS	začít
dramaticky	dramaticky	k6eAd1	dramaticky
radikalizovat	radikalizovat	k5eAaBmF	radikalizovat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS	vyvrcholit
protičeským	protičeský	k2eAgInSc7d1	protičeský
pučem	puč	k1gInSc7	puč
v	v	k7c6	v
září	září	k1gNnSc6	září
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
rostoucími	rostoucí	k2eAgInPc7d1	rostoucí
vlivy	vliv	k1gInPc7	vliv
nacionalistických	nacionalistický	k2eAgNnPc2d1	nacionalistické
hnutí	hnutí	k1gNnPc2	hnutí
a	a	k8xC	a
tendencí	tendence	k1gFnPc2	tendence
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
také	také	k9	také
Slováci	Slovák	k1gMnPc1	Slovák
začali	začít	k5eAaPmAgMnP	začít
usilovat	usilovat	k5eAaImF	usilovat
o	o	k7c4	o
svoji	svůj	k3xOyFgFnSc4	svůj
národní	národní	k2eAgFnSc4d1	národní
identitu	identita	k1gFnSc4	identita
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Hlinkova	Hlinkův	k2eAgFnSc1d1	Hlinkova
slovenská	slovenský	k2eAgFnSc1d1	slovenská
ľudová	ľudový	k2eAgFnSc1d1	ľudová
strana	strana	k1gFnSc1	strana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Mnichovská	mnichovský	k2eAgFnSc1d1	Mnichovská
dohoda	dohoda	k1gFnSc1	dohoda
<g/>
,	,	kIx,	,
Druhá	druhý	k4xOgFnSc1	druhý
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
První	první	k4xOgFnSc1	první
vídeňská	vídeňský	k2eAgFnSc1d1	Vídeňská
arbitráž	arbitráž	k1gFnSc1	arbitráž
a	a	k8xC	a
Československý	československý	k2eAgInSc1d1	československý
odboj	odboj	k1gInSc1	odboj
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
československá	československý	k2eAgFnSc1d1	Československá
politická	politický	k2eAgFnSc1d1	politická
reprezentace	reprezentace	k1gFnSc1	reprezentace
přinucena	přinucen	k2eAgFnSc1d1	přinucena
přijmout	přijmout	k5eAaPmF	přijmout
Mnichovskou	mnichovský	k2eAgFnSc4d1	Mnichovská
dohodu	dohoda	k1gFnSc4	dohoda
podepsanou	podepsaný	k2eAgFnSc4d1	podepsaná
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
konec	konec	k1gInSc1	konec
předválečné	předválečný	k2eAgFnSc2d1	předválečná
RČS	RČS	kA	RČS
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
První	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Němci	Němec	k1gMnPc1	Němec
osídlené	osídlený	k2eAgFnSc2d1	osídlená
Sudety	Sudety	k1gFnPc4	Sudety
byly	být	k5eAaImAgInP	být
o	o	k7c4	o
den	den	k1gInSc4	den
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
postoupeny	postoupen	k2eAgFnPc4d1	postoupena
Třetí	třetí	k4xOgFnSc4	třetí
říši	říše	k1gFnSc4	říše
a	a	k8xC	a
východní	východní	k2eAgFnSc4d1	východní
část	část	k1gFnSc4	část
československého	československý	k2eAgNnSc2d1	Československé
Těšínska	Těšínsko	k1gNnSc2	Těšínsko
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
se	s	k7c7	s
35	[number]	k4	35
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
polské	polský	k2eAgFnSc3d1	polská
a	a	k8xC	a
56	[number]	k4	56
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
k	k	k7c3	k
české	český	k2eAgFnSc3d1	Česká
národnosti	národnost	k1gFnSc3	národnost
<g/>
)	)	kIx)	)
Polsku	Polsko	k1gNnSc3	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
První	první	k4xOgFnSc6	první
vídeňské	vídeňský	k2eAgFnSc6d1	Vídeňská
arbitráži	arbitráž	k1gFnSc6	arbitráž
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
odstoupena	odstoupen	k2eAgNnPc1d1	odstoupen
národnostně	národnostně	k6eAd1	národnostně
smíšená	smíšený	k2eAgNnPc1d1	smíšené
území	území	k1gNnPc1	území
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Slovenska	Slovensko	k1gNnSc2	Slovensko
a	a	k8xC	a
Podkarpatské	podkarpatský	k2eAgFnSc6d1	Podkarpatská
Rusi	Rus	k1gFnSc6	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Československa	Československo	k1gNnSc2	Československo
zatím	zatím	k6eAd1	zatím
odcházela	odcházet	k5eAaImAgFnS	odcházet
první	první	k4xOgFnSc1	první
vlna	vlna	k1gFnSc1	vlna
emigrace	emigrace	k1gFnSc2	emigrace
-	-	kIx~	-
nejen	nejen	k6eAd1	nejen
lidé	člověk	k1gMnPc1	člověk
politicky	politicky	k6eAd1	politicky
angažovaní	angažovaný	k2eAgMnPc1d1	angažovaný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zejména	zejména	k9	zejména
Židé	Žid	k1gMnPc1	Žid
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnPc3	který
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
otevřeně	otevřeně	k6eAd1	otevřeně
vyhrožoval	vyhrožovat	k5eAaImAgMnS	vyhrožovat
likvidací	likvidace	k1gFnSc7	likvidace
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
rasově	rasově	k6eAd1	rasově
nečistí	čistit	k5eNaImIp3nS	čistit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
zůstali	zůstat	k5eAaPmAgMnP	zůstat
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Židé	Žid	k1gMnPc1	Žid
<g/>
,	,	kIx,	,
Romové	Rom	k1gMnPc1	Rom
<g/>
,	,	kIx,	,
tělesně	tělesně	k6eAd1	tělesně
nebo	nebo	k8xC	nebo
mentálně	mentálně	k6eAd1	mentálně
hendikepovaní	hendikepovaný	k2eAgMnPc1d1	hendikepovaný
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
političtí	politický	k2eAgMnPc1d1	politický
oponenti	oponent	k1gMnPc1	oponent
nacistického	nacistický	k2eAgInSc2d1	nacistický
režimu	režim	k1gInSc2	režim
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
zahynuli	zahynout	k5eAaPmAgMnP	zahynout
v	v	k7c6	v
koncentračních	koncentrační	k2eAgInPc6d1	koncentrační
táborech	tábor	k1gInPc6	tábor
nebo	nebo	k8xC	nebo
byli	být	k5eAaImAgMnP	být
nacisty	nacista	k1gMnPc7	nacista
zadrženi	zadržet	k5eAaPmNgMnP	zadržet
a	a	k8xC	a
následně	následně	k6eAd1	následně
zavražděni	zavražděn	k2eAgMnPc1d1	zavražděn
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
náhradu	náhrada	k1gFnSc4	náhrada
za	za	k7c4	za
ztráty	ztráta	k1gFnPc4	ztráta
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
Československo	Československo	k1gNnSc1	Československo
utrpělo	utrpět	k5eAaPmAgNnS	utrpět
vlivem	vlivem	k7c2	vlivem
uzavření	uzavření	k1gNnSc2	uzavření
Mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
dohody	dohoda	k1gFnSc2	dohoda
a	a	k8xC	a
k	k	k7c3	k
překonání	překonání	k1gNnSc3	překonání
hospodářských	hospodářský	k2eAgFnPc2d1	hospodářská
těžkostí	těžkost	k1gFnPc2	těžkost
spojených	spojený	k2eAgFnPc2d1	spojená
s	s	k7c7	s
odstoupením	odstoupení	k1gNnSc7	odstoupení
Sudet	Sudety	k1gFnPc2	Sudety
nacistickému	nacistický	k2eAgNnSc3d1	nacistické
Německu	Německo	k1gNnSc3	Německo
<g/>
,	,	kIx,	,
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
Československu	Československo	k1gNnSc6	Československo
tzv.	tzv.	kA	tzv.
mnichovský	mnichovský	k2eAgInSc4d1	mnichovský
úvěr	úvěr	k1gInSc4	úvěr
<g/>
.	.	kIx.	.
</s>
<s>
Okleštěné	okleštěný	k2eAgNnSc1d1	okleštěné
Československo	Československo	k1gNnSc1	Československo
však	však	k9	však
existovalo	existovat	k5eAaImAgNnS	existovat
jen	jen	k9	jen
krátce	krátce	k6eAd1	krátce
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1939	[number]	k4	1939
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
Hitlera	Hitler	k1gMnSc2	Hitler
Slovensko	Slovensko	k1gNnSc1	Slovensko
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
samostatnost	samostatnost	k1gFnSc4	samostatnost
a	a	k8xC	a
jako	jako	k9	jako
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
republika	republika	k1gFnSc1	republika
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
satelitem	satelit	k1gInSc7	satelit
hitlerovského	hitlerovský	k2eAgNnSc2d1	hitlerovské
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Zbylé	zbylý	k2eAgNnSc1d1	zbylé
území	území	k1gNnSc1	území
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
bylo	být	k5eAaImAgNnS	být
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1939	[number]	k4	1939
obsazeno	obsadit	k5eAaPmNgNnS	obsadit
nacistickými	nacistický	k2eAgNnPc7d1	nacistické
vojsky	vojsko	k1gNnPc7	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
den	den	k1gInSc4	den
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
Protektorát	protektorát	k1gInSc1	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
Edvardem	Edvard	k1gMnSc7	Edvard
Benešem	Beneš	k1gMnSc7	Beneš
ustanovena	ustanoven	k2eAgFnSc1d1	ustanovena
exilová	exilový	k2eAgFnSc1d1	exilová
vláda	vláda	k1gFnSc1	vláda
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Prozatímní	prozatímní	k2eAgNnSc1d1	prozatímní
státní	státní	k2eAgNnSc1d1	státní
zřízení	zřízení	k1gNnSc1	zřízení
<g/>
"	"	kIx"	"
a	a	k8xC	a
britské	britský	k2eAgNnSc4d1	Britské
území	území	k1gNnSc4	území
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
azylem	azyl	k1gInSc7	azyl
pro	pro	k7c4	pro
stovky	stovka	k1gFnPc4	stovka
československých	československý	k2eAgMnPc2d1	československý
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
bylo	být	k5eAaImAgNnS	být
povoleno	povolit	k5eAaPmNgNnS	povolit
bojovat	bojovat	k5eAaImF	bojovat
v	v	k7c6	v
britském	britský	k2eAgNnSc6d1	Britské
královském	královský	k2eAgNnSc6d1	královské
letectvu	letectvo	k1gNnSc6	letectvo
proti	proti	k7c3	proti
Německu	Německo	k1gNnSc3	Německo
<g/>
;	;	kIx,	;
exilová	exilový	k2eAgFnSc1d1	exilová
vláda	vláda	k1gFnSc1	vláda
však	však	k9	však
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
spíš	spíš	k9	spíš
trpěným	trpěný	k2eAgInSc7d1	trpěný
rozmarem	rozmar	k1gInSc7	rozmar
bývalých	bývalý	k2eAgMnPc2d1	bývalý
československých	československý	k2eAgMnPc2d1	československý
politiků	politik	k1gMnPc2	politik
a	a	k8xC	a
za	za	k7c2	za
legální	legální	k2eAgFnSc1d1	legální
byla	být	k5eAaImAgFnS	být
uznána	uznat	k5eAaPmNgFnS	uznat
až	až	k9	až
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
centrum	centrum	k1gNnSc1	centrum
protiněmeckého	protiněmecký	k2eAgInSc2d1	protiněmecký
odboje	odboj	k1gInSc2	odboj
sídlilo	sídlit	k5eAaImAgNnS	sídlit
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
a	a	k8xC	a
vedli	vést	k5eAaImAgMnP	vést
ho	on	k3xPp3gNnSc4	on
komunisté	komunista	k1gMnPc1	komunista
Klement	Klement	k1gMnSc1	Klement
Gottwald	Gottwald	k1gMnSc1	Gottwald
a	a	k8xC	a
Rudolf	Rudolf	k1gMnSc1	Rudolf
Slánský	Slánský	k1gMnSc1	Slánský
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dohodě	dohoda	k1gFnSc6	dohoda
mezi	mezi	k7c7	mezi
exilovou	exilový	k2eAgFnSc7d1	exilová
vládou	vláda	k1gFnSc7	vláda
a	a	k8xC	a
vedením	vedení	k1gNnSc7	vedení
SSSR	SSSR	kA	SSSR
se	se	k3xPyFc4	se
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
území	území	k1gNnSc6	území
začaly	začít	k5eAaPmAgInP	začít
od	od	k7c2	od
podzimu	podzim	k1gInSc2	podzim
1941	[number]	k4	1941
organizovat	organizovat	k5eAaBmF	organizovat
řádné	řádný	k2eAgFnSc2d1	řádná
československé	československý	k2eAgFnSc2d1	Československá
vojenské	vojenský	k2eAgFnSc2d1	vojenská
jednotky	jednotka	k1gFnSc2	jednotka
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Svobody	Svoboda	k1gMnSc2	Svoboda
-	-	kIx~	-
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
podařilo	podařit	k5eAaPmAgNnS	podařit
zorganizovat	zorganizovat	k5eAaPmF	zorganizovat
největší	veliký	k2eAgFnSc4d3	veliký
československou	československý	k2eAgFnSc4d1	Československá
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
jednotku	jednotka	k1gFnSc4	jednotka
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
působila	působit	k5eAaImAgFnS	působit
na	na	k7c6	na
frontách	fronta	k1gFnPc6	fronta
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
nejdéle	dlouho	k6eAd3	dlouho
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
jednotek	jednotka	k1gFnPc2	jednotka
našeho	náš	k3xOp1gNnSc2	náš
zahraničního	zahraniční	k2eAgNnSc2d1	zahraniční
vojska	vojsko	k1gNnSc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
domácí	domácí	k2eAgFnSc6d1	domácí
půdě	půda	k1gFnSc6	půda
se	se	k3xPyFc4	se
do	do	k7c2	do
boje	boj	k1gInSc2	boj
proti	proti	k7c3	proti
nacistické	nacistický	k2eAgFnSc3d1	nacistická
okupaci	okupace	k1gFnSc3	okupace
zapojily	zapojit	k5eAaPmAgFnP	zapojit
tisíce	tisíc	k4xCgInPc1	tisíc
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Nekomunistickou	komunistický	k2eNgFnSc4d1	nekomunistická
odbojovou	odbojový	k2eAgFnSc4d1	odbojová
činnost	činnost	k1gFnSc4	činnost
koordinovaly	koordinovat	k5eAaBmAgFnP	koordinovat
například	například	k6eAd1	například
organizace	organizace	k1gFnSc1	organizace
Obrana	obrana	k1gFnSc1	obrana
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
Petiční	petiční	k2eAgInSc1d1	petiční
výbor	výbor	k1gInSc1	výbor
Věrni	věren	k2eAgMnPc1d1	věren
zůstaneme	zůstat	k5eAaPmIp1nP	zůstat
a	a	k8xC	a
Politické	politický	k2eAgNnSc1d1	politické
ústředí	ústředí	k1gNnSc1	ústředí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
založily	založit	k5eAaPmAgFnP	založit
Ústřední	ústřední	k2eAgInSc4d1	ústřední
vedení	vedení	k1gNnSc4	vedení
odboje	odboj	k1gInSc2	odboj
domácího	domácí	k1gMnSc2	domácí
(	(	kIx(	(
<g/>
ÚVOD	úvod	k1gInSc1	úvod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
Ústřední	ústřední	k2eAgInSc1d1	ústřední
národní	národní	k2eAgInSc1d1	národní
revoluční	revoluční	k2eAgInSc1d1	revoluční
výbor	výbor	k1gInSc1	výbor
coby	coby	k?	coby
orgán	orgán	k1gInSc4	orgán
společného	společný	k2eAgInSc2d1	společný
komunistického	komunistický	k2eAgInSc2d1	komunistický
a	a	k8xC	a
nekomunistického	komunistický	k2eNgInSc2d1	nekomunistický
odboje	odboj	k1gInSc2	odboj
<g/>
.	.	kIx.	.
</s>
<s>
Nástupem	nástup	k1gInSc7	nástup
zastupujícího	zastupující	k2eAgMnSc2d1	zastupující
říšského	říšský	k2eAgMnSc2d1	říšský
protektora	protektor	k1gMnSc2	protektor
Reinharda	Reinhard	k1gMnSc2	Reinhard
Heydricha	Heydrich	k1gMnSc2	Heydrich
však	však	k9	však
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
výraznému	výrazný	k2eAgNnSc3d1	výrazné
zostření	zostření	k1gNnSc3	zostření
situace	situace	k1gFnSc2	situace
v	v	k7c6	v
Protektorátu	protektorát	k1gInSc6	protektorát
<g/>
,	,	kIx,	,
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
odboje	odboj	k1gInSc2	odboj
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
nacistům	nacista	k1gMnPc3	nacista
zlikvidovat	zlikvidovat	k5eAaPmF	zlikvidovat
a	a	k8xC	a
začalo	začít	k5eAaPmAgNnS	začít
být	být	k5eAaImF	být
krutě	krutě	k6eAd1	krutě
postihováno	postihován	k2eAgNnSc4d1	postihováno
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
činnost	činnost	k1gFnSc4	činnost
"	"	kIx"	"
<g/>
proti	proti	k7c3	proti
Říši	říš	k1gFnSc3	říš
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
tisíce	tisíc	k4xCgInPc1	tisíc
lidí	člověk	k1gMnPc2	člověk
byly	být	k5eAaImAgInP	být
odesílány	odesílán	k2eAgInPc1d1	odesílán
do	do	k7c2	do
koncentračních	koncentrační	k2eAgInPc2d1	koncentrační
táborů	tábor	k1gInPc2	tábor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Operace	operace	k1gFnSc1	operace
Anthropoid	Anthropoid	k1gInSc1	Anthropoid
zorganizovat	zorganizovat	k5eAaPmF	zorganizovat
výsadek	výsadek	k1gInSc4	výsadek
československých	československý	k2eAgMnPc2d1	československý
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1942	[number]	k4	1942
provedli	provést	k5eAaPmAgMnP	provést
úspěšný	úspěšný	k2eAgInSc4d1	úspěšný
atentát	atentát	k1gInSc4	atentát
na	na	k7c4	na
protektora	protektor	k1gMnSc4	protektor
Heydricha	Heydrich	k1gMnSc4	Heydrich
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
zemřel	zemřít	k5eAaPmAgMnS	zemřít
o	o	k7c4	o
osm	osm	k4xCc4	osm
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
vůbec	vůbec	k9	vůbec
nejvýše	vysoce	k6eAd3	vysoce
postaveným	postavený	k2eAgMnSc7d1	postavený
nacistickým	nacistický	k2eAgMnSc7d1	nacistický
politikem	politik	k1gMnSc7	politik
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
během	během	k7c2	během
války	válka	k1gFnSc2	válka
zabit	zabít	k5eAaPmNgInS	zabít
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
se	s	k7c7	s
silami	síla	k1gFnPc7	síla
protinacistického	protinacistický	k2eAgInSc2d1	protinacistický
odboje	odboj	k1gInSc2	odboj
<g/>
.	.	kIx.	.
</s>
<s>
Nacisté	nacista	k1gMnPc1	nacista
následně	následně	k6eAd1	následně
rozpoutali	rozpoutat	k5eAaPmAgMnP	rozpoutat
vlnu	vlna	k1gFnSc4	vlna
represí	represe	k1gFnPc2	represe
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejvýraznějšími	výrazný	k2eAgFnPc7d3	nejvýraznější
bylo	být	k5eAaImAgNnS	být
vypálení	vypálení	k1gNnSc4	vypálení
vesnice	vesnice	k1gFnSc2	vesnice
Lidice	Lidice	k1gInPc1	Lidice
a	a	k8xC	a
osady	osada	k1gFnPc1	osada
Ležáky	Ležáky	k1gInPc4	Ležáky
včetně	včetně	k7c2	včetně
likvidace	likvidace	k1gFnSc2	likvidace
většiny	většina	k1gFnSc2	většina
jejich	jejich	k3xOp3gMnPc2	jejich
obyvatel	obyvatel	k1gMnPc2	obyvatel
-	-	kIx~	-
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgMnSc2	ten
však	však	k9	však
vlády	vláda	k1gFnPc1	vláda
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Francie	Francie	k1gFnSc2	Francie
uznaly	uznat	k5eAaPmAgFnP	uznat
mnichovskou	mnichovský	k2eAgFnSc4d1	Mnichovská
dohodu	dohoda	k1gFnSc4	dohoda
za	za	k7c4	za
neplatnou	platný	k2eNgFnSc4d1	neplatná
od	od	k7c2	od
samého	samý	k3xTgInSc2	samý
počátku	počátek	k1gInSc2	počátek
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
plně	plně	k6eAd1	plně
legalizovaly	legalizovat	k5eAaBmAgInP	legalizovat
jak	jak	k6eAd1	jak
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
československou	československý	k2eAgFnSc4d1	Československá
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
odboj	odboj	k1gInSc4	odboj
<g/>
.	.	kIx.	.
</s>
<s>
Osvobozování	osvobozování	k1gNnSc1	osvobozování
československého	československý	k2eAgNnSc2d1	Československé
území	území	k1gNnSc2	území
od	od	k7c2	od
nacismu	nacismus	k1gInSc2	nacismus
začalo	začít	k5eAaPmAgNnS	začít
koncem	koncem	k7c2	koncem
srpna	srpen	k1gInSc2	srpen
1944	[number]	k4	1944
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
bojů	boj	k1gInPc2	boj
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
frontě	fronta	k1gFnSc6	fronta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
rychlého	rychlý	k2eAgInSc2d1	rychlý
postupu	postup	k1gInSc2	postup
sovětských	sovětský	k2eAgNnPc2d1	sovětské
vojsk	vojsko	k1gNnPc2	vojsko
se	s	k7c7	s
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1944	[number]	k4	1944
představitelé	představitel	k1gMnPc1	představitel
Wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
obsadit	obsadit	k5eAaPmF	obsadit
území	území	k1gNnSc4	území
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
krok	krok	k1gInSc1	krok
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
na	na	k7c6	na
středním	střední	k2eAgNnSc6d1	střední
Slovensku	Slovensko	k1gNnSc6	Slovensko
protifašistické	protifašistický	k2eAgNnSc4d1	protifašistické
povstání	povstání	k1gNnSc4	povstání
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
zamezit	zamezit	k5eAaPmF	zamezit
obsazení	obsazení	k1gNnSc4	obsazení
slovenského	slovenský	k2eAgNnSc2d1	slovenské
území	území	k1gNnSc2	území
a	a	k8xC	a
pomoci	pomoc	k1gFnSc2	pomoc
tak	tak	k8xS	tak
postupu	postup	k1gInSc2	postup
sovětské	sovětský	k2eAgFnSc2d1	sovětská
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
zdržení	zdržení	k1gNnSc2	zdržení
sovětské	sovětský	k2eAgFnSc2d1	sovětská
ofenzivy	ofenziva	k1gFnSc2	ofenziva
bylo	být	k5eAaImAgNnS	být
povstání	povstání	k1gNnSc1	povstání
tvrdě	tvrdě	k6eAd1	tvrdě
potlačeno	potlačen	k2eAgNnSc1d1	potlačeno
a	a	k8xC	a
celé	celý	k2eAgNnSc1d1	celé
slovenské	slovenský	k2eAgNnSc1d1	slovenské
území	území	k1gNnSc1	území
bylo	být	k5eAaImAgNnS	být
obsazeno	obsadit	k5eAaPmNgNnS	obsadit
německou	německý	k2eAgFnSc7d1	německá
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Povstalci	povstalec	k1gMnPc1	povstalec
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
museli	muset	k5eAaImAgMnP	muset
uchýlit	uchýlit	k5eAaPmF	uchýlit
k	k	k7c3	k
metodám	metoda	k1gFnPc3	metoda
partyzánského	partyzánský	k2eAgInSc2d1	partyzánský
boje	boj	k1gInSc2	boj
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
povstání	povstání	k1gNnSc1	povstání
v	v	k7c6	v
Přerově	Přerov	k1gInSc6	Přerov
a	a	k8xC	a
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
dnech	den	k1gInPc6	den
se	se	k3xPyFc4	se
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
formách	forma	k1gFnPc6	forma
na	na	k7c4	na
desítky	desítka	k1gFnPc4	desítka
dalších	další	k2eAgNnPc2d1	další
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
propuklo	propuknout	k5eAaPmAgNnS	propuknout
povstání	povstání	k1gNnSc1	povstání
také	také	k9	také
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
zachvátilo	zachvátit	k5eAaPmAgNnS	zachvátit
většinu	většina	k1gFnSc4	většina
území	území	k1gNnSc2	území
Protektorátu	protektorát	k1gInSc2	protektorát
(	(	kIx(	(
<g/>
Květnové	květnový	k2eAgNnSc1d1	květnové
povstání	povstání	k1gNnSc1	povstání
českého	český	k2eAgInSc2d1	český
lidu	lid	k1gInSc2	lid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
co	co	k9	co
nejrychlejší	rychlý	k2eAgNnSc4d3	nejrychlejší
osvobození	osvobození	k1gNnSc4	osvobození
českého	český	k2eAgNnSc2d1	české
území	území	k1gNnSc2	území
z	z	k7c2	z
německé	německý	k2eAgFnSc2d1	německá
nadvlády	nadvláda	k1gFnSc2	nadvláda
a	a	k8xC	a
minimalizace	minimalizace	k1gFnSc2	minimalizace
dalších	další	k2eAgFnPc2d1	další
válečných	válečný	k2eAgFnPc2d1	válečná
škod	škoda	k1gFnPc2	škoda
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Praze	Praha	k1gFnSc3	Praha
pomohla	pomoct	k5eAaPmAgFnS	pomoct
tzv.	tzv.	kA	tzv.
ROA	ROA	kA	ROA
generála	generál	k1gMnSc2	generál
Vlasova	Vlasův	k2eAgMnSc2d1	Vlasův
<g/>
,	,	kIx,	,
když	když	k8xS	když
Americká	americký	k2eAgFnSc1d1	americká
armáda	armáda	k1gFnSc1	armáda
přítomná	přítomný	k2eAgFnSc1d1	přítomná
od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
musela	muset	k5eAaImAgFnS	muset
zastavit	zastavit	k5eAaPmF	zastavit
po	po	k7c6	po
překročení	překročení	k1gNnSc6	překročení
tzv.	tzv.	kA	tzv.
demarkační	demarkační	k2eAgInPc1d1	demarkační
čáry	čár	k1gInPc1	čár
v	v	k7c6	v
Rokycanech	Rokycany	k1gInPc6	Rokycany
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
v	v	k7c4	v
tyto	tento	k3xDgInPc4	tento
dny	den	k1gInPc4	den
dorazil	dorazit	k5eAaPmAgMnS	dorazit
pouze	pouze	k6eAd1	pouze
americký	americký	k2eAgInSc4d1	americký
průzkumný	průzkumný	k2eAgInSc4d1	průzkumný
oddíl	oddíl	k1gInSc4	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krutých	krutý	k2eAgInPc6d1	krutý
bojích	boj	k1gInPc6	boj
v	v	k7c6	v
Nuslích	Nusle	k1gFnPc6	Nusle
a	a	k8xC	a
na	na	k7c6	na
Pankráci	Pankrác	k1gMnSc6	Pankrác
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
již	již	k9	již
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
klid	klid	k1gInSc4	klid
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
den	den	k1gInSc4	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
byla	být	k5eAaImAgFnS	být
Praha	Praha	k1gFnSc1	Praha
osvobozena	osvobodit	k5eAaPmNgFnS	osvobodit
vojsky	vojsky	k6eAd1	vojsky
sovětské	sovětský	k2eAgFnSc2d1	sovětská
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
de	de	k?	de
facto	facto	k1gNnSc1	facto
skončila	skončit	k5eAaPmAgFnS	skončit
druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Třetí	třetí	k4xOgFnSc1	třetí
Československá	československý	k2eAgFnSc1d1	Československá
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1945	[number]	k4	1945
bylo	být	k5eAaImAgNnS	být
Československo	Československo	k1gNnSc1	Československo
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bez	bez	k7c2	bez
území	území	k1gNnSc2	území
Podkarpatské	podkarpatský	k2eAgFnSc6d1	Podkarpatská
Rusi	Rus	k1gFnSc6	Rus
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
připojeno	připojit	k5eAaPmNgNnS	připojit
k	k	k7c3	k
sovětské	sovětský	k2eAgFnSc3d1	sovětská
Ukrajině	Ukrajina	k1gFnSc3	Ukrajina
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
expanze	expanze	k1gFnSc2	expanze
SSSR	SSSR	kA	SSSR
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Varianta	varianta	k1gFnSc1	varianta
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
Československo	Československo	k1gNnSc1	Československo
bylo	být	k5eAaImAgNnS	být
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
federace	federace	k1gFnSc2	federace
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
Rakouska	Rakousko	k1gNnSc2	Rakousko
a	a	k8xC	a
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Edvardem	Edvard	k1gMnSc7	Edvard
Benešem	Beneš	k1gMnSc7	Beneš
odmítnuta	odmítnout	k5eAaPmNgFnS	odmítnout
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
dohod	dohoda	k1gFnPc2	dohoda
z	z	k7c2	z
postupimské	postupimský	k2eAgFnSc2d1	Postupimská
konference	konference	k1gFnSc2	konference
bylo	být	k5eAaImAgNnS	být
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1946	[number]	k4	1946
odsunuto	odsunut	k2eAgNnSc4d1	odsunuto
nebo	nebo	k8xC	nebo
vysídleno	vysídlen	k2eAgNnSc4d1	vysídleno
2	[number]	k4	2
232	[number]	k4	232
544	[number]	k4	544
obyvatel	obyvatel	k1gMnPc2	obyvatel
německé	německý	k2eAgFnSc2d1	německá
národnosti	národnost	k1gFnSc2	národnost
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
odsunu	odsun	k1gInSc3	odsun
dalších	další	k2eAgInPc2d1	další
80	[number]	k4	80
000	[number]	k4	000
českých	český	k2eAgMnPc2d1	český
Němců	Němec	k1gMnPc2	Němec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Protože	protože	k8xS	protože
postupimská	postupimský	k2eAgFnSc1d1	Postupimská
konference	konference	k1gFnSc1	konference
zároveň	zároveň	k6eAd1	zároveň
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
schválit	schválit	k5eAaPmF	schválit
podobný	podobný	k2eAgInSc4d1	podobný
postup	postup	k1gInSc4	postup
i	i	k9	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
občanů	občan	k1gMnPc2	občan
maďarské	maďarský	k2eAgFnSc2d1	maďarská
národnosti	národnost	k1gFnSc2	národnost
<g/>
,	,	kIx,	,
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
československá	československý	k2eAgFnSc1d1	Československá
vláda	vláda	k1gFnSc1	vláda
s	s	k7c7	s
maďarskou	maďarský	k2eAgFnSc7d1	maďarská
vládou	vláda	k1gFnSc7	vláda
dohodu	dohoda	k1gFnSc4	dohoda
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
výměně	výměna	k1gFnSc6	výměna
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
jakýkoliv	jakýkoliv	k3yIgMnSc1	jakýkoliv
příslušník	příslušník	k1gMnSc1	příslušník
slovenské	slovenský	k2eAgFnSc2d1	slovenská
menšiny	menšina	k1gFnSc2	menšina
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
mohl	moct	k5eAaImAgMnS	moct
přestěhovat	přestěhovat	k5eAaPmF	přestěhovat
do	do	k7c2	do
ČSR	ČSR	kA	ČSR
a	a	k8xC	a
za	za	k7c2	za
něj	on	k3xPp3gNnSc2	on
byl	být	k5eAaImAgMnS	být
vystěhován	vystěhovat	k5eAaPmNgMnS	vystěhovat
do	do	k7c2	do
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
jeden	jeden	k4xCgMnSc1	jeden
příslušník	příslušník	k1gMnSc1	příslušník
maďarské	maďarský	k2eAgFnSc2d1	maďarská
menšiny	menšina	k1gFnSc2	menšina
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
této	tento	k3xDgFnSc2	tento
dohody	dohoda	k1gFnSc2	dohoda
bylo	být	k5eAaImAgNnS	být
přesídleno	přesídlet	k5eAaPmNgNnS	přesídlet
do	do	k7c2	do
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
90	[number]	k4	90
000	[number]	k4	000
slovenských	slovenský	k2eAgMnPc2d1	slovenský
Maďarů	Maďar	k1gMnPc2	Maďar
<g/>
.	.	kIx.	.
</s>
<s>
Samostatnost	samostatnost	k1gFnSc1	samostatnost
poválečné	poválečný	k2eAgFnSc2d1	poválečná
ČSR	ČSR	kA	ČSR
byla	být	k5eAaImAgFnS	být
oslabena	oslabit	k5eAaPmNgFnS	oslabit
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
v	v	k7c6	v
československé	československý	k2eAgFnSc6d1	Československá
politice	politika	k1gFnSc6	politika
začala	začít	k5eAaPmAgFnS	začít
prosazovat	prosazovat	k5eAaImF	prosazovat
závislost	závislost	k1gFnSc4	závislost
na	na	k7c6	na
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
<g/>
,	,	kIx,	,
připravovaná	připravovaný	k2eAgFnSc1d1	připravovaná
Komunistickou	komunistický	k2eAgFnSc7d1	komunistická
stranou	strana	k1gFnSc7	strana
Československa	Československo	k1gNnSc2	Československo
(	(	kIx(	(
<g/>
KSČ	KSČ	kA	KSČ
<g/>
)	)	kIx)	)
za	za	k7c2	za
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zestátnění	zestátnění	k1gNnSc3	zestátnění
většiny	většina	k1gFnSc2	většina
československého	československý	k2eAgInSc2d1	československý
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1946	[number]	k4	1946
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
první	první	k4xOgFnPc1	první
poválečné	poválečný	k2eAgFnPc1d1	poválečná
parlamentní	parlamentní	k2eAgFnPc1d1	parlamentní
volby	volba	k1gFnPc1	volba
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
ziskem	zisk	k1gInSc7	zisk
40	[number]	k4	40
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Vytěžila	vytěžit	k5eAaPmAgFnS	vytěžit
nejen	nejen	k6eAd1	nejen
maximum	maximum	k1gNnSc4	maximum
z	z	k7c2	z
poválečného	poválečný	k2eAgInSc2d1	poválečný
růstu	růst	k1gInSc2	růst
popularity	popularita	k1gFnSc2	popularita
levice	levice	k1gFnSc2	levice
a	a	k8xC	a
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
jako	jako	k8xC	jako
vítěze	vítěz	k1gMnPc4	vítěz
nad	nad	k7c7	nad
nacismem	nacismus	k1gInSc7	nacismus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
díky	díky	k7c3	díky
slibům	slib	k1gInPc3	slib
další	další	k2eAgFnSc2d1	další
pozemkové	pozemkový	k2eAgFnSc2d1	pozemková
reformy	reforma	k1gFnSc2	reforma
získala	získat	k5eAaPmAgFnS	získat
většinu	většina	k1gFnSc4	většina
hlasů	hlas	k1gInPc2	hlas
venkovského	venkovský	k2eAgNnSc2d1	venkovské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Zato	zato	k6eAd1	zato
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
jasně	jasně	k6eAd1	jasně
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
s	s	k7c7	s
62	[number]	k4	62
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
komunisté	komunista	k1gMnPc1	komunista
jich	on	k3xPp3gFnPc2	on
dostali	dostat	k5eAaPmAgMnP	dostat
jen	jen	k6eAd1	jen
30	[number]	k4	30
%	%	kIx~	%
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
přes	přes	k7c4	přes
tento	tento	k3xDgInSc4	tento
neúspěch	neúspěch	k1gInSc4	neúspěch
získali	získat	k5eAaPmAgMnP	získat
komunisté	komunista	k1gMnPc1	komunista
společně	společně	k6eAd1	společně
38	[number]	k4	38
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
stali	stát	k5eAaPmAgMnP	stát
se	se	k3xPyFc4	se
nejsilnější	silný	k2eAgFnSc7d3	nejsilnější
stranou	strana	k1gFnSc7	strana
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
voleb	volba	k1gFnPc2	volba
se	se	k3xPyFc4	se
odrazil	odrazit	k5eAaPmAgInS	odrazit
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Komunisté	komunista	k1gMnPc1	komunista
si	se	k3xPyFc3	se
podrželi	podržet	k5eAaPmAgMnP	podržet
nejen	nejen	k6eAd1	nejen
vlivná	vlivný	k2eAgNnPc1d1	vlivné
místa	místo	k1gNnPc1	místo
na	na	k7c6	na
ministerstvech	ministerstvo	k1gNnPc6	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
,	,	kIx,	,
informací	informace	k1gFnPc2	informace
a	a	k8xC	a
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
získali	získat	k5eAaPmAgMnP	získat
i	i	k8xC	i
křeslo	křeslo	k1gNnSc4	křeslo
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
do	do	k7c2	do
něhož	jenž	k3xRgInSc2	jenž
usedl	usednout	k5eAaPmAgMnS	usednout
předseda	předseda	k1gMnSc1	předseda
strany	strana	k1gFnSc2	strana
Klement	Klement	k1gMnSc1	Klement
Gottwald	Gottwald	k1gMnSc1	Gottwald
<g/>
.	.	kIx.	.
</s>
<s>
Ministrem	ministr	k1gMnSc7	ministr
obrany	obrana	k1gFnSc2	obrana
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
generál	generál	k1gMnSc1	generál
Ludvík	Ludvík	k1gMnSc1	Ludvík
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
některých	některý	k3yIgMnPc2	některý
historiků	historik	k1gMnPc2	historik
tajný	tajný	k2eAgInSc4d1	tajný
člen	člen	k1gInSc4	člen
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
navenek	navenek	k6eAd1	navenek
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
jako	jako	k9	jako
nestraník	nestraník	k1gMnSc1	nestraník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1948	[number]	k4	1948
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
vládní	vládní	k2eAgFnSc1d1	vládní
krize	krize	k1gFnSc1	krize
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
Hradeckým	hradecký	k2eAgInSc7d1	hradecký
programem	program	k1gInSc7	program
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc1	program
požadoval	požadovat	k5eAaImAgInS	požadovat
další	další	k2eAgNnSc4d1	další
znárodňování	znárodňování	k1gNnSc4	znárodňování
a	a	k8xC	a
rozparcelování	rozparcelování	k1gNnSc4	rozparcelování
statků	statek	k1gInPc2	statek
nad	nad	k7c7	nad
50	[number]	k4	50
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Komunisté	komunista	k1gMnPc1	komunista
také	také	k9	také
dále	daleko	k6eAd2	daleko
pronikali	pronikat	k5eAaImAgMnP	pronikat
na	na	k7c4	na
významná	významný	k2eAgNnPc4d1	významné
místa	místo	k1gNnPc4	místo
v	v	k7c6	v
silových	silový	k2eAgFnPc6d1	silová
složkách	složka	k1gFnPc6	složka
(	(	kIx(	(
<g/>
8	[number]	k4	8
členů	člen	k1gInPc2	člen
vedení	vedení	k1gNnSc2	vedení
SNB	SNB	kA	SNB
bylo	být	k5eAaImAgNnS	být
nahrazeno	nahradit	k5eAaPmNgNnS	nahradit
komunisty	komunista	k1gMnPc7	komunista
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
podala	podat	k5eAaPmAgFnS	podat
většina	většina	k1gFnSc1	většina
nekomunistických	komunistický	k2eNgMnPc2d1	nekomunistický
ministrů	ministr	k1gMnPc2	ministr
20	[number]	k4	20
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
demisi	demise	k1gFnSc4	demise
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Jan	Jan	k1gMnSc1	Jan
Masaryk	Masaryk	k1gMnSc1	Masaryk
a	a	k8xC	a
Ludvík	Ludvík	k1gMnSc1	Ludvík
Svoboda	Svoboda	k1gMnSc1	Svoboda
zůstali	zůstat	k5eAaPmAgMnP	zůstat
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Odstoupivší	odstoupivší	k2eAgMnPc1d1	odstoupivší
předpokládali	předpokládat	k5eAaImAgMnP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
prezident	prezident	k1gMnSc1	prezident
Beneš	Beneš	k1gMnSc1	Beneš
demisi	demise	k1gFnSc4	demise
nepřijme	přijmout	k5eNaPmIp3nS	přijmout
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
novou	nový	k2eAgFnSc4d1	nová
úřednickou	úřednický	k2eAgFnSc4d1	úřednická
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
ale	ale	k9	ale
nestalo	stát	k5eNaPmAgNnS	stát
a	a	k8xC	a
prezident	prezident	k1gMnSc1	prezident
25	[number]	k4	25
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
demisi	demise	k1gFnSc4	demise
přijal	přijmout	k5eAaPmAgMnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
pěti	pět	k4xCc6	pět
dnech	den	k1gInPc6	den
komunisté	komunista	k1gMnPc1	komunista
vyvíjeli	vyvíjet	k5eAaImAgMnP	vyvíjet
na	na	k7c4	na
prezidenta	prezident	k1gMnSc2	prezident
silný	silný	k2eAgInSc4d1	silný
nátlak	nátlak	k1gInSc4	nátlak
organizováním	organizování	k1gNnSc7	organizování
demonstrací	demonstrace	k1gFnPc2	demonstrace
<g/>
,	,	kIx,	,
stávek	stávka	k1gFnPc2	stávka
a	a	k8xC	a
vyzbrojováním	vyzbrojování	k1gNnSc7	vyzbrojování
Lidových	lidový	k2eAgFnPc2d1	lidová
milicí	milice	k1gFnPc2	milice
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
sestavena	sestavit	k5eAaPmNgFnS	sestavit
nová	nový	k2eAgFnSc1d1	nová
vláda	vláda	k1gFnSc1	vláda
Národní	národní	k2eAgFnSc2d1	národní
fronty	fronta	k1gFnSc2	fronta
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Klementem	Klement	k1gMnSc7	Klement
Gottwaldem	Gottwald	k1gMnSc7	Gottwald
<g/>
.	.	kIx.	.
</s>
<s>
Jediný	jediný	k2eAgMnSc1d1	jediný
skutečně	skutečně	k6eAd1	skutečně
nekomunistický	komunistický	k2eNgMnSc1d1	nekomunistický
ministr	ministr	k1gMnSc1	ministr
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
vládě	vláda	k1gFnSc6	vláda
Jan	Jan	k1gMnSc1	Jan
Masaryk	Masaryk	k1gMnSc1	Masaryk
zemřel	zemřít	k5eAaPmAgMnS	zemřít
za	za	k7c2	za
nevyjasněných	vyjasněný	k2eNgFnPc2d1	nevyjasněná
okolností	okolnost	k1gFnPc2	okolnost
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Komunistický	komunistický	k2eAgInSc4d1	komunistický
režim	režim	k1gInSc4	režim
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Komunistický	komunistický	k2eAgInSc1d1	komunistický
převrat	převrat	k1gInSc1	převrat
<g/>
,	,	kIx,	,
dovršený	dovršený	k2eAgMnSc1d1	dovršený
25	[number]	k4	25
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
převzetí	převzetí	k1gNnSc3	převzetí
veškeré	veškerý	k3xTgFnSc2	veškerý
moci	moc	k1gFnSc2	moc
KSČ	KSČ	kA	KSČ
a	a	k8xC	a
k	k	k7c3	k
jednomu	jeden	k4xCgMnSc3	jeden
z	z	k7c2	z
totalitních	totalitní	k2eAgMnPc2d1	totalitní
režimů	režim	k1gInPc2	režim
bývalé	bývalý	k2eAgFnSc2d1	bývalá
zájmové	zájmový	k2eAgFnSc2d1	zájmová
sféry	sféra	k1gFnSc2	sféra
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
den	den	k1gInSc1	den
byl	být	k5eAaImAgInS	být
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Vítězný	vítězný	k2eAgInSc4d1	vítězný
Únor	únor	k1gInSc4	únor
<g/>
"	"	kIx"	"
následně	následně	k6eAd1	následně
jako	jako	k8xC	jako
významný	významný	k2eAgInSc4d1	významný
den	den	k1gInSc4	den
režimem	režim	k1gInSc7	režim
oslavován	oslavovat	k5eAaImNgInS	oslavovat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1948	[number]	k4	1948
komunisté	komunista	k1gMnPc1	komunista
znárodnili	znárodnit	k5eAaPmAgMnP	znárodnit
podniky	podnik	k1gInPc4	podnik
nad	nad	k7c4	nad
50	[number]	k4	50
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
znárodnění	znárodnění	k1gNnSc1	znárodnění
průmyslu	průmysl	k1gInSc2	průmysl
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
95	[number]	k4	95
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
rozdělili	rozdělit	k5eAaPmAgMnP	rozdělit
statky	statek	k1gInPc4	statek
nad	nad	k7c7	nad
50	[number]	k4	50
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Vydali	vydat	k5eAaPmAgMnP	vydat
zákon	zákon	k1gInSc4	zákon
o	o	k7c6	o
všeobecném	všeobecný	k2eAgNnSc6d1	všeobecné
zdravotním	zdravotní	k2eAgNnSc6d1	zdravotní
pojištění	pojištění	k1gNnSc6	pojištění
a	a	k8xC	a
provedli	provést	k5eAaPmAgMnP	provést
reformu	reforma	k1gFnSc4	reforma
školství	školství	k1gNnSc2	školství
<g/>
.	.	kIx.	.
</s>
<s>
Všem	všecek	k3xTgMnPc3	všecek
sociálním	sociální	k2eAgMnPc3d1	sociální
demokratům	demokrat	k1gMnPc3	demokrat
poslali	poslat	k5eAaPmAgMnP	poslat
bez	bez	k7c2	bez
jejich	jejich	k3xOp3gNnSc2	jejich
vědomí	vědomí	k1gNnSc2	vědomí
poštou	pošta	k1gFnSc7	pošta
komunistické	komunistický	k2eAgFnSc2d1	komunistická
legitimace	legitimace	k1gFnSc2	legitimace
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
sociální	sociální	k2eAgFnSc3d1	sociální
demokracii	demokracie	k1gFnSc3	demokracie
tak	tak	k6eAd1	tak
zlikvidovali	zlikvidovat	k5eAaPmAgMnP	zlikvidovat
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kdož	kdož	k3yRnSc1	kdož
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
doživotně	doživotně	k6eAd1	doživotně
perzekvováni	perzekvovat	k5eAaImNgMnP	perzekvovat
včetně	včetně	k7c2	včetně
rodinných	rodinný	k2eAgMnPc2d1	rodinný
příslušníků	příslušník	k1gMnPc2	příslušník
(	(	kIx(	(
<g/>
zabránění	zabránění	k1gNnSc1	zabránění
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
<g/>
,	,	kIx,	,
nepřijetí	nepřijetí	k1gNnSc6	nepřijetí
nebo	nebo	k8xC	nebo
ztráta	ztráta	k1gFnSc1	ztráta
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgNnSc1d2	pozdější
odnímání	odnímání	k1gNnSc1	odnímání
majetku	majetek	k1gInSc2	majetek
zejména	zejména	k9	zejména
soukromníkům-živnostníkům	soukromníkům-živnostník	k1gMnPc3	soukromníkům-živnostník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
táhlo	táhlo	k1gNnSc1	táhlo
i	i	k8xC	i
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
(	(	kIx(	(
<g/>
nálepky	nálepka	k1gFnPc1	nálepka
reakcionářů	reakcionář	k1gMnPc2	reakcionář
<g/>
,	,	kIx,	,
domnělý	domnělý	k2eAgInSc1d1	domnělý
antikomunismus	antikomunismus	k1gInSc1	antikomunismus
<g/>
,	,	kIx,	,
nemožnost	nemožnost	k1gFnSc1	nemožnost
rehabilitace	rehabilitace	k1gFnSc1	rehabilitace
<g/>
,	,	kIx,	,
předpojatost	předpojatost	k1gFnSc1	předpojatost
v	v	k7c6	v
restitučních	restituční	k2eAgInPc6d1	restituční
sporech	spor	k1gInPc6	spor
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1948	[number]	k4	1948
bylo	být	k5eAaImAgNnS	být
Československo	Československo	k1gNnSc1	Československo
prohlášeno	prohlásit	k5eAaPmNgNnS	prohlásit
lidově	lidově	k6eAd1	lidově
demokratickou	demokratický	k2eAgFnSc7d1	demokratická
republikou	republika	k1gFnSc7	republika
<g/>
,	,	kIx,	,
na	na	k7c4	na
což	což	k3yQnSc4	což
již	již	k6eAd1	již
nemocný	mocný	k2eNgMnSc1d1	nemocný
a	a	k8xC	a
psychicky	psychicky	k6eAd1	psychicky
deptaný	deptaný	k2eAgMnSc1d1	deptaný
prezident	prezident	k1gMnSc1	prezident
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
reagoval	reagovat	k5eAaBmAgMnS	reagovat
svou	svůj	k3xOyFgFnSc7	svůj
abdikací	abdikace	k1gFnSc7	abdikace
<g/>
,	,	kIx,	,
když	když	k8xS	když
předtím	předtím	k6eAd1	předtím
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
podepsat	podepsat	k5eAaPmF	podepsat
takzvanou	takzvaný	k2eAgFnSc4d1	takzvaná
"	"	kIx"	"
<g/>
Ústavu	ústava	k1gFnSc4	ústava
devátého	devátý	k4xOgInSc2	devátý
května	květen	k1gInSc2	květen
<g/>
"	"	kIx"	"
.	.	kIx.	.
</s>
<s>
Prezidentem	prezident	k1gMnSc7	prezident
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgMnS	stát
komunista	komunista	k1gMnSc1	komunista
Klement	Klement	k1gMnSc1	Klement
Gottwald	Gottwald	k1gMnSc1	Gottwald
<g/>
.	.	kIx.	.
</s>
<s>
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
skonal	skonat	k5eAaPmAgMnS	skonat
v	v	k7c6	v
Sezimově	Sezimův	k2eAgNnSc6d1	Sezimovo
Ústí	ústí	k1gNnSc6	ústí
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1948	[number]	k4	1948
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
64	[number]	k4	64
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1948	[number]	k4	1948
byl	být	k5eAaImAgInS	být
schválen	schválit	k5eAaPmNgInS	schválit
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
táborech	tábor	k1gInPc6	tábor
nucené	nucený	k2eAgFnSc2d1	nucená
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
byly	být	k5eAaImAgInP	být
zřízeny	zřízen	k2eAgInPc1d1	zřízen
tábory	tábor	k1gInPc1	tábor
nucené	nucený	k2eAgFnSc2d1	nucená
práce	práce	k1gFnSc2	práce
při	při	k7c6	při
uranových	uranový	k2eAgInPc6d1	uranový
dolech	dol	k1gInPc6	dol
(	(	kIx(	(
<g/>
Vojna	vojna	k1gFnSc1	vojna
u	u	k7c2	u
Příbrami	Příbram	k1gFnSc2	Příbram
<g/>
,	,	kIx,	,
Rovnost	rovnost	k1gFnSc1	rovnost
<g/>
,	,	kIx,	,
Svornost	svornost	k1gFnSc1	svornost
<g/>
,	,	kIx,	,
Bratrství	bratrství	k1gNnSc1	bratrství
u	u	k7c2	u
Jáchymova	Jáchymov	k1gInSc2	Jáchymov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
Zákon	zákon	k1gInSc1	zákon
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
lidově	lidově	k6eAd1	lidově
demokratické	demokratický	k2eAgFnSc2d1	demokratická
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
též	též	k9	též
zřízeny	zřízen	k2eAgInPc1d1	zřízen
armádní	armádní	k2eAgInPc1d1	armádní
pomocné	pomocný	k2eAgInPc1d1	pomocný
technické	technický	k2eAgInPc1d1	technický
prapory	prapor	k1gInPc1	prapor
pro	pro	k7c4	pro
politicky	politicky	k6eAd1	politicky
nespolehlivé	spolehlivý	k2eNgMnPc4d1	nespolehlivý
jedince	jedinec	k1gMnPc4	jedinec
jako	jako	k8xS	jako
kulaky	kulak	k1gMnPc4	kulak
<g/>
,	,	kIx,	,
kněží	kněz	k1gMnPc2	kněz
<g/>
,	,	kIx,	,
západní	západní	k2eAgMnPc4d1	západní
letce	letec	k1gMnPc4	letec
atd.	atd.	kA	atd.
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nesměli	smět	k5eNaImAgMnP	smět
sloužit	sloužit	k5eAaImF	sloužit
se	s	k7c7	s
zbraní	zbraň	k1gFnSc7	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jimi	on	k3xPp3gInPc7	on
prošlo	projít	k5eAaPmAgNnS	projít
asi	asi	k9	asi
22	[number]	k4	22
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Režim	režim	k1gInSc4	režim
plánovitě	plánovitě	k6eAd1	plánovitě
likvidoval	likvidovat	k5eAaBmAgMnS	likvidovat
své	svůj	k3xOyFgMnPc4	svůj
politické	politický	k2eAgMnPc4d1	politický
odpůrce	odpůrce	k1gMnPc4	odpůrce
<g/>
,	,	kIx,	,
náboženské	náboženský	k2eAgMnPc4d1	náboženský
představitele	představitel	k1gMnPc4	představitel
a	a	k8xC	a
nekomunistickou	komunistický	k2eNgFnSc4d1	nekomunistická
inteligenci	inteligence	k1gFnSc4	inteligence
<g/>
,	,	kIx,	,
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
omezit	omezit	k5eAaPmF	omezit
vyšší	vysoký	k2eAgNnSc4d2	vyšší
vzdělání	vzdělání	k1gNnSc4	vzdělání
na	na	k7c4	na
politicky	politicky	k6eAd1	politicky
spolehlivé	spolehlivý	k2eAgMnPc4d1	spolehlivý
jedince	jedinec	k1gMnPc4	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
velkým	velký	k2eAgFnPc3d1	velká
změnám	změna	k1gFnPc3	změna
došlo	dojít	k5eAaPmAgNnS	dojít
také	také	k9	také
v	v	k7c6	v
hospodářství	hospodářství	k1gNnSc6	hospodářství
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
budováno	budovat	k5eAaImNgNnS	budovat
centrálně	centrálně	k6eAd1	centrálně
podle	podle	k7c2	podle
sovětského	sovětský	k2eAgInSc2d1	sovětský
vzoru	vzor	k1gInSc2	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Důraz	důraz	k1gInSc1	důraz
byl	být	k5eAaImAgInS	být
kladen	klást	k5eAaImNgInS	klást
na	na	k7c4	na
těžký	těžký	k2eAgInSc4d1	těžký
průmysl	průmysl	k1gInSc4	průmysl
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
zbrojní	zbrojní	k2eAgFnSc1d1	zbrojní
<g/>
.	.	kIx.	.
</s>
<s>
Začala	začít	k5eAaPmAgFnS	začít
industrializace	industrializace	k1gFnSc1	industrializace
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
1	[number]	k4	1
<g/>
.	.	kIx.	.
pětiletka	pětiletka	k1gFnSc1	pětiletka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
znamenala	znamenat	k5eAaImAgFnS	znamenat
rozvoj	rozvoj	k1gInSc4	rozvoj
těžkého	těžký	k2eAgInSc2d1	těžký
průmyslu	průmysl	k1gInSc2	průmysl
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
spotřebního	spotřební	k2eAgMnSc2d1	spotřební
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
také	také	k9	také
vydán	vydán	k2eAgInSc1d1	vydán
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
JZD	JZD	kA	JZD
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
násilná	násilný	k2eAgFnSc1d1	násilná
kolektivizace	kolektivizace	k1gFnSc1	kolektivizace
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
odešla	odejít	k5eAaPmAgFnS	odejít
druhá	druhý	k4xOgFnSc1	druhý
vlna	vlna	k1gFnSc1	vlna
emigrace	emigrace	k1gFnSc2	emigrace
<g/>
.	.	kIx.	.
</s>
<s>
Režim	režim	k1gInSc1	režim
konsolidoval	konsolidovat	k5eAaBmAgInS	konsolidovat
svou	svůj	k3xOyFgFnSc4	svůj
moc	moc	k1gFnSc4	moc
vykonstruovanými	vykonstruovaný	k2eAgInPc7d1	vykonstruovaný
procesy	proces	k1gInPc7	proces
<g/>
,	,	kIx,	,
popravami	poprava	k1gFnPc7	poprava
(	(	kIx(	(
<g/>
vraždami	vražda	k1gFnPc7	vražda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vězněním	věznění	k1gNnSc7	věznění
a	a	k8xC	a
vládou	vláda	k1gFnSc7	vláda
strachu	strach	k1gInSc2	strach
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
vlna	vlna	k1gFnSc1	vlna
znárodnění	znárodnění	k1gNnSc2	znárodnění
a	a	k8xC	a
kolektivizace	kolektivizace	k1gFnSc1	kolektivizace
převedla	převést	k5eAaPmAgFnS	převést
další	další	k2eAgInPc4d1	další
majetky	majetek	k1gInPc4	majetek
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
vznikající	vznikající	k2eAgFnSc2d1	vznikající
"	"	kIx"	"
<g/>
nové	nový	k2eAgFnSc2d1	nová
třídy	třída	k1gFnSc2	třída
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
všechny	všechen	k3xTgFnPc1	všechen
odbojové	odbojový	k2eAgFnPc1d1	odbojová
skupiny	skupina	k1gFnPc1	skupina
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
bratří	bratr	k1gMnPc2	bratr
Mašínů	Mašín	k1gInPc2	Mašín
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
i	i	k9	i
v	v	k7c6	v
náznaku	náznak	k1gInSc6	náznak
likvidovány	likvidovat	k5eAaBmNgFnP	likvidovat
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1949	[number]	k4	1949
byly	být	k5eAaImAgFnP	být
vykonány	vykonat	k5eAaPmNgInP	vykonat
rovněž	rovněž	k9	rovněž
tresty	trest	k1gInPc4	trest
smrti	smrt	k1gFnSc2	smrt
nad	nad	k7c7	nad
představiteli	představitel	k1gMnPc7	představitel
skupin	skupina	k1gFnPc2	skupina
nestraníků	nestraník	k1gMnPc2	nestraník
Vratislavem	Vratislav	k1gMnSc7	Vratislav
Polesným	polesný	k1gMnSc7	polesný
<g/>
,	,	kIx,	,
Vratislavem	Vratislav	k1gMnSc7	Vratislav
Jandou	Janda	k1gMnSc7	Janda
<g/>
,	,	kIx,	,
Josefem	Josef	k1gMnSc7	Josef
Charvátem	Charvát	k1gMnSc7	Charvát
<g/>
,	,	kIx,	,
Emanuelem	Emanuel	k1gMnSc7	Emanuel
Čančíkem	Čančík	k1gMnSc7	Čančík
<g/>
,	,	kIx,	,
Květoslavem	Květoslav	k1gMnSc7	Květoslav
Prokešem	Prokeš	k1gMnSc7	Prokeš
a	a	k8xC	a
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Borkovcem	Borkovec	k1gMnSc7	Borkovec
(	(	kIx(	(
<g/>
bratr	bratr	k1gMnSc1	bratr
prvního	první	k4xOgMnSc2	první
vyšetřovatele	vyšetřovatel	k1gMnSc2	vyšetřovatel
kauzy	kauza	k1gFnSc2	kauza
Jana	Jan	k1gMnSc2	Jan
Masaryka	Masaryk	k1gMnSc2	Masaryk
JUDr.	JUDr.	kA	JUDr.
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Borkovce	Borkovec	k1gMnSc2	Borkovec
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
byl	být	k5eAaImAgInS	být
případ	případ	k1gInSc1	případ
odejmut	odejmout	k5eAaPmNgInS	odejmout
a	a	k8xC	a
předán	předat	k5eAaPmNgInS	předat
StB	StB	k1gFnSc7	StB
<g/>
)	)	kIx)	)
odsouzenými	odsouzená	k1gFnPc7	odsouzená
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
přípravou	příprava	k1gFnSc7	příprava
údajného	údajný	k2eAgNnSc2d1	údajné
květnového	květnový	k2eAgNnSc2d1	květnové
protikomunistického	protikomunistický	k2eAgNnSc2d1	protikomunistické
povstání	povstání	k1gNnSc2	povstání
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
března	březen	k1gInSc2	březen
1953	[number]	k4	1953
zemřel	zemřít	k5eAaPmAgMnS	zemřít
sovětský	sovětský	k2eAgMnSc1d1	sovětský
vůdce	vůdce	k1gMnSc1	vůdce
Josif	Josif	k1gMnSc1	Josif
Vissarionovič	Vissarionovič	k1gMnSc1	Vissarionovič
Stalin	Stalin	k1gMnSc1	Stalin
a	a	k8xC	a
nové	nový	k2eAgNnSc4d1	nové
vedení	vedení	k1gNnSc4	vedení
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Nikitou	Nikita	k1gMnSc7	Nikita
Sergejevičem	Sergejevič	k1gMnSc7	Sergejevič
Chruščovem	Chruščov	k1gInSc7	Chruščov
zanedlouho	zanedlouho	k6eAd1	zanedlouho
odsoudilo	odsoudit	k5eAaPmAgNnS	odsoudit
kult	kult	k1gInSc4	kult
Stalinovy	Stalinův	k2eAgFnSc2d1	Stalinova
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
zemřel	zemřít	k5eAaPmAgMnS	zemřít
(	(	kIx(	(
<g/>
bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
ze	z	k7c2	z
Stalinova	Stalinův	k2eAgInSc2d1	Stalinův
pohřbu	pohřeb	k1gInSc2	pohřeb
<g/>
)	)	kIx)	)
i	i	k8xC	i
Klement	Klement	k1gMnSc1	Klement
Gottwald	Gottwald	k1gMnSc1	Gottwald
a	a	k8xC	a
novým	nový	k2eAgMnSc7d1	nový
prezidentem	prezident	k1gMnSc7	prezident
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
Antonín	Antonín	k1gMnSc1	Antonín
Zápotocký	Zápotocký	k1gMnSc1	Zápotocký
<g/>
.	.	kIx.	.
</s>
<s>
Uvedl	uvést	k5eAaPmAgMnS	uvést
se	se	k3xPyFc4	se
neblahou	blahý	k2eNgFnSc7d1	neblahá
měnovou	měnový	k2eAgFnSc7d1	měnová
reformou	reforma	k1gFnSc7	reforma
s	s	k7c7	s
následnými	následný	k2eAgInPc7d1	následný
krvavě	krvavě	k6eAd1	krvavě
potlačovanými	potlačovaný	k2eAgInPc7d1	potlačovaný
nepokoji	nepokoj	k1gInPc7	nepokoj
a	a	k8xC	a
protesty	protest	k1gInPc7	protest
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
jako	jako	k9	jako
odveta	odveta	k1gFnSc1	odveta
za	za	k7c4	za
tyto	tento	k3xDgInPc4	tento
protesty	protest	k1gInPc4	protest
odstraněna	odstraněn	k2eAgFnSc1d1	odstraněna
i	i	k8xC	i
socha	socha	k1gFnSc1	socha
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Zápotocký	Zápotocký	k1gMnSc1	Zápotocký
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
něj	on	k3xPp3gMnSc2	on
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
první	první	k4xOgMnSc1	první
tajemník	tajemník	k1gMnSc1	tajemník
strany	strana	k1gFnSc2	strana
Antonín	Antonín	k1gMnSc1	Antonín
Novotný	Novotný	k1gMnSc1	Novotný
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
něho	on	k3xPp3gMnSc2	on
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pomalu	pomalu	k6eAd1	pomalu
již	již	k6eAd1	již
ustávaly	ustávat	k5eAaImAgFnP	ustávat
popravy	poprava	k1gFnPc4	poprava
z	z	k7c2	z
politických	politický	k2eAgInPc2d1	politický
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgMnSc7d1	poslední
takto	takto	k6eAd1	takto
popraveným	popravený	k2eAgInSc7d1	popravený
byl	být	k5eAaImAgInS	být
dle	dle	k7c2	dle
údajů	údaj	k1gInPc2	údaj
ÚDV	ÚDV	kA	ÚDV
Vladivoj	Vladivoj	k1gInSc4	Vladivoj
Tomek	tomka	k1gFnPc2	tomka
dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
nicméně	nicméně	k8xC	nicméně
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
za	za	k7c2	za
více	hodně	k6eAd2	hodně
trestných	trestný	k2eAgInPc2d1	trestný
činnů	činn	k1gInPc2	činn
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
vraždu	vražda	k1gFnSc4	vražda
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
předtím	předtím	k6eAd1	předtím
byl	být	k5eAaImAgInS	být
formálně	formálně	k6eAd1	formálně
dovršen	dovršen	k2eAgInSc1d1	dovršen
socialismus	socialismus	k1gInSc1	socialismus
a	a	k8xC	a
změněn	změněn	k2eAgInSc1d1	změněn
název	název	k1gInSc1	název
republiky	republika	k1gFnSc2	republika
na	na	k7c6	na
Československá	československý	k2eAgFnSc1d1	Československá
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
ČSSR	ČSSR	kA	ČSSR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
krize	krize	k1gFnSc1	krize
systému	systém	k1gInSc2	systém
se	se	k3xPyFc4	se
projevila	projevit	k5eAaPmAgFnS	projevit
nutností	nutnost	k1gFnSc7	nutnost
zrušit	zrušit	k5eAaPmF	zrušit
3	[number]	k4	3
<g/>
.	.	kIx.	.
pětiletku	pětiletka	k1gFnSc4	pětiletka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
Krátké	Krátké	k2eAgNnSc1d1	Krátké
období	období	k1gNnSc1	období
tání	tání	k1gNnSc2	tání
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
vedlo	vést	k5eAaImAgNnS	vést
i	i	k8xC	i
k	k	k7c3	k
uvolnění	uvolnění	k1gNnSc3	uvolnění
v	v	k7c4	v
tehdejší	tehdejší	k2eAgFnPc4d1	tehdejší
ČSSR	ČSSR	kA	ČSSR
a	a	k8xC	a
k	k	k7c3	k
Pražskému	pražský	k2eAgInSc3d1	pražský
jaru	jar	k1gInSc3	jar
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Antonín	Antonín	k1gMnSc1	Antonín
Novotný	Novotný	k1gMnSc1	Novotný
vzdal	vzdát	k5eAaPmAgMnS	vzdát
prezidentské	prezidentský	k2eAgFnPc4d1	prezidentská
i	i	k8xC	i
stranické	stranický	k2eAgFnPc4d1	stranická
funkce	funkce	k1gFnPc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentem	prezident	k1gMnSc7	prezident
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
po	po	k7c6	po
krátké	krátký	k2eAgFnSc6d1	krátká
době	doba	k1gFnSc6	doba
Ludvík	Ludvík	k1gMnSc1	Ludvík
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Naděje	naděje	k1gFnSc1	naděje
na	na	k7c4	na
"	"	kIx"	"
<g/>
socialismus	socialismus	k1gInSc4	socialismus
s	s	k7c7	s
lidskou	lidský	k2eAgFnSc7d1	lidská
tváří	tvář	k1gFnSc7	tvář
<g/>
"	"	kIx"	"
byly	být	k5eAaImAgInP	být
na	na	k7c4	na
rozkaz	rozkaz	k1gInSc4	rozkaz
Moskvy	Moskva	k1gFnSc2	Moskva
rázně	rázně	k6eAd1	rázně
ukončeny	ukončit	k5eAaPmNgFnP	ukončit
invazí	invaze	k1gFnSc7	invaze
států	stát	k1gInPc2	stát
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
"	"	kIx"	"
<g/>
bratrská	bratrský	k2eAgFnSc1d1	bratrská
internacionální	internacionální	k2eAgFnSc1d1	internacionální
pomoc	pomoc	k1gFnSc1	pomoc
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
spontánní	spontánní	k2eAgInSc4d1	spontánní
odpor	odpor	k1gInSc4	odpor
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
zejména	zejména	k9	zejména
k	k	k7c3	k
sovětským	sovětský	k2eAgMnPc3d1	sovětský
okupantům	okupant	k1gMnPc3	okupant
tehdejší	tehdejší	k2eAgMnPc1d1	tehdejší
českoslovenští	československý	k2eAgMnPc1d1	československý
politici	politik	k1gMnPc1	politik
(	(	kIx(	(
<g/>
Alexander	Alexandra	k1gFnPc2	Alexandra
Dubček	Dubček	k1gInSc1	Dubček
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
dalších	další	k2eAgMnPc2d1	další
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
odvlečení	odvlečení	k1gNnSc6	odvlečení
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
,	,	kIx,	,
ustoupili	ustoupit	k5eAaPmAgMnP	ustoupit
nátlaku	nátlak	k1gInSc3	nátlak
a	a	k8xC	a
okupaci	okupace	k1gFnSc3	okupace
schválili	schválit	k5eAaPmAgMnP	schválit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
1969	[number]	k4	1969
bylo	být	k5eAaImAgNnS	být
Československo	Československo	k1gNnSc1	Československo
přeměněno	přeměnit	k5eAaPmNgNnS	přeměnit
na	na	k7c6	na
federaci	federace	k1gFnSc6	federace
dvou	dva	k4xCgInPc2	dva
formálně	formálně	k6eAd1	formálně
suverénních	suverénní	k2eAgInPc2d1	suverénní
národních	národní	k2eAgInPc2d1	národní
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
České	český	k2eAgFnSc2d1	Česká
socialistické	socialistický	k2eAgFnSc2d1	socialistická
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
socialistické	socialistický	k2eAgFnSc2d1	socialistická
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
se	s	k7c7	s
zachováním	zachování	k1gNnSc7	zachování
souhrnného	souhrnný	k2eAgInSc2d1	souhrnný
názvu	název	k1gInSc2	název
Československá	československý	k2eAgFnSc1d1	Československá
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
odešla	odejít	k5eAaPmAgFnS	odejít
třetí	třetí	k4xOgFnSc1	třetí
vlna	vlna	k1gFnSc1	vlna
emigrace	emigrace	k1gFnSc2	emigrace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
pasivitě	pasivita	k1gFnSc3	pasivita
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
při	při	k7c6	při
počínající	počínající	k2eAgFnSc6d1	počínající
normalizaci	normalizace	k1gFnSc6	normalizace
obětovali	obětovat	k5eAaBmAgMnP	obětovat
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
16	[number]	k4	16
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1969	[number]	k4	1969
Jan	Jan	k1gMnSc1	Jan
Palach	palach	k1gInSc1	palach
a	a	k8xC	a
25	[number]	k4	25
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1969	[number]	k4	1969
Jan	Jan	k1gMnSc1	Jan
Zajíc	Zajíc	k1gMnSc1	Zajíc
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
Gustáva	Gustáv	k1gMnSc2	Gustáv
Husáka	Husák	k1gMnSc2	Husák
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
na	na	k7c4	na
pokyn	pokyn	k1gInSc4	pokyn
z	z	k7c2	z
Moskvy	Moskva	k1gFnSc2	Moskva
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
Alexandra	Alexandr	k1gMnSc4	Alexandr
Dubčeka	Dubčeek	k1gMnSc4	Dubčeek
<g/>
,	,	kIx,	,
nastalo	nastat	k5eAaPmAgNnS	nastat
definitivně	definitivně	k6eAd1	definitivně
období	období	k1gNnSc1	období
utužení	utužení	k1gNnSc2	utužení
režimu	režim	k1gInSc2	režim
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
normalizace	normalizace	k1gFnSc2	normalizace
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgNnPc4d1	následující
dvě	dva	k4xCgNnPc4	dva
desetiletí	desetiletí	k1gNnPc4	desetiletí
jsou	být	k5eAaImIp3nP	být
označována	označovat	k5eAaImNgFnS	označovat
za	za	k7c4	za
vládu	vláda	k1gFnSc4	vláda
"	"	kIx"	"
<g/>
šedé	šedý	k2eAgFnPc1d1	šedá
zóny	zóna	k1gFnPc1	zóna
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vládu	vláda	k1gFnSc4	vláda
konformismu	konformismus	k1gInSc2	konformismus
a	a	k8xC	a
"	"	kIx"	"
<g/>
reálný	reálný	k2eAgInSc1d1	reálný
socialismus	socialismus	k1gInSc1	socialismus
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
lidově	lidově	k6eAd1	lidově
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
socialismus	socialismus	k1gInSc1	socialismus
s	s	k7c7	s
husí	husí	k2eAgFnSc7d1	husí
kůží	kůže	k1gFnSc7	kůže
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ideologicky	ideologicky	k6eAd1	ideologicky
nepohodlní	pohodlný	k2eNgMnPc1d1	nepohodlný
lidé	člověk	k1gMnPc1	člověk
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
opět	opět	k6eAd1	opět
odstraňováni	odstraňován	k2eAgMnPc1d1	odstraňován
z	z	k7c2	z
významných	významný	k2eAgNnPc2d1	významné
míst	místo	k1gNnPc2	místo
a	a	k8xC	a
stát	stát	k1gInSc1	stát
dál	daleko	k6eAd2	daleko
ztrácel	ztrácet	k5eAaImAgInS	ztrácet
schopné	schopný	k2eAgMnPc4d1	schopný
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Petiční	petiční	k2eAgFnSc1d1	petiční
akce	akce	k1gFnSc1	akce
Charta	charta	k1gFnSc1	charta
77	[number]	k4	77
poukazovala	poukazovat	k5eAaImAgFnS	poukazovat
na	na	k7c6	na
porušování	porušování	k1gNnSc6	porušování
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
v	v	k7c6	v
ČSSR	ČSSR	kA	ČSSR
a	a	k8xC	a
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
ostrou	ostrý	k2eAgFnSc4d1	ostrá
reakci	reakce	k1gFnSc4	reakce
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
obětí	oběť	k1gFnSc7	oběť
režimního	režimní	k2eAgNnSc2d1	režimní
potlačování	potlačování	k1gNnSc2	potlačování
této	tento	k3xDgFnSc2	tento
(	(	kIx(	(
<g/>
plně	plně	k6eAd1	plně
v	v	k7c6	v
mezích	mez	k1gFnPc6	mez
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
zákonů	zákon	k1gInPc2	zákon
vedené	vedený	k2eAgFnSc2d1	vedená
<g/>
)	)	kIx)	)
akce	akce	k1gFnSc2	akce
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
již	již	k6eAd1	již
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1977	[number]	k4	1977
český	český	k2eAgMnSc1d1	český
filozof	filozof	k1gMnSc1	filozof
Jan	Jan	k1gMnSc1	Jan
Patočka	Patočka	k1gMnSc1	Patočka
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgMnSc1d1	poslední
Pavel	Pavel	k1gMnSc1	Pavel
Wonka	Wonka	k1gMnSc1	Wonka
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1988	[number]	k4	1988
za	za	k7c2	za
nejasných	jasný	k2eNgFnPc2d1	nejasná
okolností	okolnost	k1gFnPc2	okolnost
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Policejní	policejní	k2eAgFnSc1d1	policejní
represe	represe	k1gFnSc1	represe
zahrnující	zahrnující	k2eAgFnSc2d1	zahrnující
nevybíravé	vybíravý	k2eNgFnSc2d1	nevybíravá
metody	metoda	k1gFnSc2	metoda
získávání	získávání	k1gNnSc2	získávání
spolupracovníků	spolupracovník	k1gMnPc2	spolupracovník
tajné	tajný	k2eAgFnSc2d1	tajná
Státní	státní	k2eAgFnSc2d1	státní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
a	a	k8xC	a
zřejmě	zřejmě	k6eAd1	zřejmě
i	i	k9	i
policejní	policejní	k2eAgFnSc2d1	policejní
vraždy	vražda	k1gFnSc2	vražda
inscenované	inscenovaný	k2eAgFnSc2d1	inscenovaná
jako	jako	k8xC	jako
sebevraždy	sebevražda	k1gFnSc2	sebevražda
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Přemysl	Přemysl	k1gMnSc1	Přemysl
Coufal	Coufal	k1gMnSc1	Coufal
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Švanda	Švanda	k1gMnSc1	Švanda
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
nehody	nehoda	k1gFnSc2	nehoda
(	(	kIx(	(
<g/>
Luboš	Luboš	k1gMnSc1	Luboš
Holeček	Holeček	k1gMnSc1	Holeček
<g/>
,	,	kIx,	,
spolužák	spolužák	k1gMnSc1	spolužák
Jana	Jan	k1gMnSc2	Jan
Palacha	Palacha	k1gMnSc1	Palacha
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
mírnější	mírný	k2eAgFnPc1d2	mírnější
a	a	k8xC	a
méně	málo	k6eAd2	málo
časté	častý	k2eAgNnSc1d1	časté
než	než	k8xS	než
v	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
zároveň	zároveň	k6eAd1	zároveň
dobře	dobře	k6eAd1	dobře
utajeny	utajit	k5eAaPmNgFnP	utajit
a	a	k8xC	a
nedávány	dávat	k5eNaImNgFnP	dávat
na	na	k7c4	na
odiv	odiv	k1gInSc4	odiv
jako	jako	k8xS	jako
výstraha	výstraha	k1gFnSc1	výstraha
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
padesátých	padesátý	k4xOgNnPc6	padesátý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
rostl	růst	k5eAaImAgInS	růst
hrubý	hrubý	k2eAgInSc1d1	hrubý
domácí	domácí	k2eAgInSc4d1	domácí
produkt	produkt	k1gInSc4	produkt
tempem	tempo	k1gNnSc7	tempo
o	o	k7c4	o
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
%	%	kIx~	%
každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
a	a	k8xC	a
republika	republika	k1gFnSc1	republika
zažila	zažít	k5eAaPmAgFnS	zažít
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
boom	boom	k1gInSc4	boom
<g/>
:	:	kIx,	:
dílčí	dílčí	k2eAgFnSc1d1	dílčí
restrukturalizace	restrukturalizace	k1gFnSc1	restrukturalizace
konce	konec	k1gInSc2	konec
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
levné	levný	k2eAgFnPc1d1	levná
suroviny	surovina	k1gFnPc1	surovina
ze	z	k7c2	z
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
vývoz	vývoz	k1gInSc4	vývoz
do	do	k7c2	do
zemí	zem	k1gFnPc2	zem
RVHP	RVHP	kA	RVHP
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
prodloužily	prodloužit	k5eAaPmAgInP	prodloužit
trvání	trvání	k1gNnSc4	trvání
neefektivního	efektivní	k2eNgInSc2d1	neefektivní
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Technické	technický	k2eAgNnSc1d1	technické
zaostávání	zaostávání	k1gNnSc1	zaostávání
většiny	většina	k1gFnSc2	většina
podniků	podnik	k1gInPc2	podnik
se	se	k3xPyFc4	se
především	především	k9	především
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
soustavně	soustavně	k6eAd1	soustavně
prohlubovalo	prohlubovat	k5eAaImAgNnS	prohlubovat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
plně	plně	k6eAd1	plně
projevilo	projevit	k5eAaPmAgNnS	projevit
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Nadále	nadále	k6eAd1	nadále
ovšem	ovšem	k9	ovšem
probíhala	probíhat	k5eAaImAgFnS	probíhat
industrializace	industrializace	k1gFnSc1	industrializace
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
dál	daleko	k6eAd2	daleko
byl	být	k5eAaImAgInS	být
budován	budovat	k5eAaImNgInS	budovat
převážně	převážně	k6eAd1	převážně
těžký	těžký	k2eAgInSc1d1	těžký
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
dálniční	dálniční	k2eAgFnSc1d1	dálniční
síť	síť	k1gFnSc1	síť
<g/>
,	,	kIx,	,
Pražské	pražský	k2eAgNnSc1d1	Pražské
metro	metro	k1gNnSc1	metro
a	a	k8xC	a
panelová	panelový	k2eAgNnPc1d1	panelové
sídliště	sídliště	k1gNnPc1	sídliště
<g/>
.	.	kIx.	.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
skončil	skončit	k5eAaPmAgInS	skončit
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
v	v	k7c6	v
ČSSR	ČSSR	kA	ČSSR
se	s	k7c7	s
zpožděním	zpoždění	k1gNnSc7	zpoždění
projevil	projevit	k5eAaPmAgInS	projevit
tzv.	tzv.	kA	tzv.
ropný	ropný	k2eAgInSc1d1	ropný
šok	šok	k1gInSc1	šok
-	-	kIx~	-
ČSSR	ČSSR	kA	ČSSR
totiž	totiž	k9	totiž
platila	platit	k5eAaImAgFnS	platit
za	za	k7c4	za
dodávky	dodávka	k1gFnPc4	dodávka
ropy	ropa	k1gFnSc2	ropa
pětileté	pětiletý	k2eAgFnPc4d1	pětiletá
průměrné	průměrný	k2eAgFnPc4d1	průměrná
tržní	tržní	k2eAgFnPc4d1	tržní
světové	světový	k2eAgFnPc4d1	světová
ceny	cena	k1gFnPc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
částečná	částečný	k2eAgFnSc1d1	částečná
krize	krize	k1gFnSc1	krize
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
,	,	kIx,	,
vyvolaná	vyvolaný	k2eAgFnSc1d1	vyvolaná
centrálním	centrální	k2eAgNnSc7d1	centrální
direktivním	direktivní	k2eAgNnSc7d1	direktivní
řízením	řízení	k1gNnSc7	řízení
ekonomiky	ekonomika	k1gFnSc2	ekonomika
s	s	k7c7	s
mnoha	mnoho	k4c2	mnoho
redundantními	redundantní	k2eAgFnPc7d1	redundantní
rozhodovacími	rozhodovací	k2eAgFnPc7d1	rozhodovací
úrovněmi	úroveň	k1gFnPc7	úroveň
(	(	kIx(	(
<g/>
závod	závod	k1gInSc1	závod
-	-	kIx~	-
podnik	podnik	k1gInSc1	podnik
-	-	kIx~	-
koncern	koncern	k1gInSc1	koncern
-	-	kIx~	-
VHJ	VHJ	kA	VHJ
-	-	kIx~	-
resortní	resortní	k2eAgNnSc1d1	resortní
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
paralelní	paralelní	k2eAgFnSc1d1	paralelní
struktura	struktura	k1gFnSc1	struktura
v	v	k7c6	v
KSČ	KSČ	kA	KSČ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mnohým	mnohý	k2eAgMnPc3d1	mnohý
podnikům	podnik	k1gInPc3	podnik
chyběly	chybět	k5eAaImAgFnP	chybět
peníze	peníz	k1gInPc4	peníz
(	(	kIx(	(
<g/>
zvláště	zvláště	k6eAd1	zvláště
přídělové	přídělový	k2eAgFnPc1d1	přídělová
devizy	deviza	k1gFnPc1	deviza
<g/>
)	)	kIx)	)
na	na	k7c4	na
stavební	stavební	k2eAgFnPc4d1	stavební
a	a	k8xC	a
strojní	strojní	k2eAgFnPc4d1	strojní
investice	investice	k1gFnPc4	investice
-	-	kIx~	-
oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
však	však	k9	však
byl	být	k5eAaImAgInS	být
režimem	režim	k1gInSc7	režim
podporován	podporován	k2eAgInSc4d1	podporován
rozvoj	rozvoj	k1gInSc4	rozvoj
výpočetní	výpočetní	k2eAgFnSc2d1	výpočetní
techniky	technika	k1gFnSc2	technika
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
podniků	podnik	k1gInPc2	podnik
i	i	k8xC	i
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Průmysl	průmysl	k1gInSc1	průmysl
totiž	totiž	k9	totiž
zůstával	zůstávat	k5eAaImAgInS	zůstávat
málo	málo	k6eAd1	málo
efektivní	efektivní	k2eAgFnSc4d1	efektivní
a	a	k8xC	a
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
těžký	těžký	k2eAgInSc4d1	těžký
průmysl	průmysl	k1gInSc4	průmysl
<g/>
)	)	kIx)	)
výrazně	výrazně	k6eAd1	výrazně
poškozoval	poškozovat	k5eAaImAgMnS	poškozovat
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
v	v	k7c6	v
severních	severní	k2eAgFnPc6d1	severní
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1	relativní
životní	životní	k2eAgFnSc1d1	životní
úroveň	úroveň	k1gFnSc1	úroveň
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
se	se	k3xPyFc4	se
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
se	s	k7c7	s
západní	západní	k2eAgFnSc7d1	západní
Evropou	Evropa	k1gFnSc7	Evropa
dál	daleko	k6eAd2	daleko
postupně	postupně	k6eAd1	postupně
snižovala	snižovat	k5eAaImAgFnS	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
(	(	kIx(	(
<g/>
jen	jen	k9	jen
částečně	částečně	k6eAd1	částečně
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
)	)	kIx)	)
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
restrukturalizaci	restrukturalizace	k1gFnSc4	restrukturalizace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgInP	být
centralizované	centralizovaný	k2eAgInPc1d1	centralizovaný
národní	národní	k2eAgInPc1d1	národní
podniky	podnik	k1gInPc1	podnik
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
vlnách	vlna	k1gFnPc6	vlna
převedeny	převeden	k2eAgFnPc1d1	převedena
na	na	k7c4	na
formu	forma	k1gFnSc4	forma
relativně	relativně	k6eAd1	relativně
samostatných	samostatný	k2eAgInPc2d1	samostatný
státních	státní	k2eAgInPc2d1	státní
podniků	podnik	k1gInPc2	podnik
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
podléhaly	podléhat	k5eAaImAgInP	podléhat
pouze	pouze	k6eAd1	pouze
resortním	resortní	k2eAgNnPc3d1	resortní
ministerstvům	ministerstvo	k1gNnPc3	ministerstvo
a	a	k8xC	a
jejichž	jejichž	k3xOyRp3gMnPc4	jejichž
ředitele	ředitel	k1gMnPc4	ředitel
volili	volit	k5eAaImAgMnP	volit
sami	sám	k3xTgMnPc1	sám
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
uvolnění	uvolnění	k1gNnSc6	uvolnění
poměrů	poměr	k1gInPc2	poměr
uvnitř	uvnitř	k7c2	uvnitř
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
a	a	k8xC	a
prosazení	prosazení	k1gNnSc2	prosazení
perestrojky	perestrojka	k1gFnSc2	perestrojka
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
ČSSR	ČSSR	kA	ČSSR
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
posledních	poslední	k2eAgInPc2d1	poslední
států	stát	k1gInPc2	stát
pod	pod	k7c7	pod
totalitním	totalitní	k2eAgNnSc7d1	totalitní
vedením	vedení	k1gNnSc7	vedení
komunistů	komunista	k1gMnPc2	komunista
<g/>
.	.	kIx.	.
</s>
<s>
Změnu	změna	k1gFnSc4	změna
přinesly	přinést	k5eAaPmAgFnP	přinést
až	až	k9	až
události	událost	k1gFnPc1	událost
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
impulsem	impuls	k1gInSc7	impuls
k	k	k7c3	k
protivládním	protivládní	k2eAgFnPc3d1	protivládní
aktivitám	aktivita	k1gFnPc3	aktivita
stala	stát	k5eAaPmAgFnS	stát
vymyšlená	vymyšlený	k2eAgFnSc1d1	vymyšlená
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
smrti	smrt	k1gFnSc6	smrt
studenta	student	k1gMnSc2	student
Martina	Martin	k1gMnSc2	Martin
Šmída	Šmíd	k1gMnSc2	Šmíd
po	po	k7c6	po
policejním	policejní	k2eAgInSc6d1	policejní
zásahu	zásah	k1gInSc6	zásah
proti	proti	k7c3	proti
studentskému	studentský	k2eAgInSc3d1	studentský
pochodu	pochod	k1gInSc3	pochod
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
několika	několik	k4yIc2	několik
hodin	hodina	k1gFnPc2	hodina
bylo	být	k5eAaImAgNnS	být
zorganizováno	zorganizovat	k5eAaPmNgNnS	zorganizovat
hnutí	hnutí	k1gNnSc2	hnutí
(	(	kIx(	(
<g/>
zprvu	zprvu	k6eAd1	zprvu
jen	jen	k9	jen
pražských	pražský	k2eAgMnPc2d1	pražský
<g/>
)	)	kIx)	)
herců	herec	k1gMnPc2	herec
a	a	k8xC	a
vysokoškolských	vysokoškolský	k2eAgMnPc2d1	vysokoškolský
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
následovalo	následovat	k5eAaImAgNnS	následovat
založení	založení	k1gNnSc1	založení
Občanského	občanský	k2eAgNnSc2d1	občanské
fóra	fórum	k1gNnSc2	fórum
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
stávek	stávka	k1gFnPc2	stávka
či	či	k8xC	či
demonstrací	demonstrace	k1gFnPc2	demonstrace
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
republice	republika	k1gFnSc6	republika
-	-	kIx~	-
ať	ať	k8xC	ať
už	už	k6eAd1	už
živelných	živelný	k2eAgInPc2d1	živelný
nebo	nebo	k8xC	nebo
organizovaných	organizovaný	k2eAgInPc2d1	organizovaný
<g/>
.	.	kIx.	.
</s>
<s>
Sympatie	sympatie	k1gFnSc2	sympatie
veřejnosti	veřejnost	k1gFnPc1	veřejnost
si	se	k3xPyFc3	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
získal	získat	k5eAaPmAgMnS	získat
pragmatický	pragmatický	k2eAgMnSc1d1	pragmatický
předseda	předseda	k1gMnSc1	předseda
komunistické	komunistický	k2eAgFnSc2d1	komunistická
federální	federální	k2eAgFnSc2d1	federální
vlády	vláda	k1gFnSc2	vláda
Ladislav	Ladislav	k1gMnSc1	Ladislav
Adamec	Adamec	k1gMnSc1	Adamec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
začal	začít	k5eAaPmAgMnS	začít
jednat	jednat	k5eAaImF	jednat
s	s	k7c7	s
představiteli	představitel	k1gMnPc7	představitel
OF	OF	kA	OF
o	o	k7c6	o
jejich	jejich	k3xOp3gInPc6	jejich
požadavcích	požadavek	k1gInPc6	požadavek
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
na	na	k7c6	na
veřejném	veřejný	k2eAgNnSc6d1	veřejné
shromáždění	shromáždění	k1gNnSc6	shromáždění
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
Letné	Letná	k1gFnSc6	Letná
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
odsoudil	odsoudit	k5eAaPmAgMnS	odsoudit
připravovanou	připravovaný	k2eAgFnSc4d1	připravovaná
generální	generální	k2eAgFnSc4d1	generální
stávku	stávka	k1gFnSc4	stávka
<g/>
,	,	kIx,	,
však	však	k9	však
byl	být	k5eAaImAgInS	být
vypískán	vypískat	k5eAaPmNgMnS	vypískat
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
na	na	k7c6	na
funkci	funkce	k1gFnSc6	funkce
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
byla	být	k5eAaImAgFnS	být
jmenována	jmenován	k2eAgFnSc1d1	jmenována
Vláda	vláda	k1gFnSc1	vláda
národního	národní	k2eAgNnSc2d1	národní
porozumění	porozumění	k1gNnSc2	porozumění
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Mariána	Marián	k1gMnSc2	Marián
Čalfy	Čalf	k1gInPc4	Čalf
<g/>
,	,	kIx,	,
následovala	následovat	k5eAaImAgFnS	následovat
abdikace	abdikace	k1gFnSc1	abdikace
prezidenta	prezident	k1gMnSc2	prezident
Gustáva	Gustáv	k1gMnSc2	Gustáv
Husáka	Husák	k1gMnSc2	Husák
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
odvolání	odvolání	k1gNnSc4	odvolání
řady	řada	k1gFnPc1	řada
poslanců	poslanec	k1gMnPc2	poslanec
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
,	,	kIx,	,
kooptace	kooptace	k1gFnSc1	kooptace
nových	nový	k2eAgMnPc2d1	nový
poslanců	poslanec	k1gMnPc2	poslanec
od	od	k7c2	od
28	[number]	k4	28
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
a	a	k8xC	a
zvolení	zvolení	k1gNnSc4	zvolení
Alexandra	Alexandr	k1gMnSc2	Alexandr
Dubčeka	Dubčeek	k1gMnSc2	Dubčeek
předsedou	předseda	k1gMnSc7	předseda
společného	společný	k2eAgInSc2d1	společný
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Nejviditelnějším	viditelný	k2eAgInSc7d3	nejviditelnější
symbolem	symbol	k1gInSc7	symbol
změn	změna	k1gFnPc2	změna
byla	být	k5eAaImAgFnS	být
volba	volba	k1gFnSc1	volba
nového	nový	k2eAgMnSc2d1	nový
prezidenta	prezident	k1gMnSc2	prezident
ČSSR	ČSSR	kA	ČSSR
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnSc7	který
byl	být	k5eAaImAgMnS	být
dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1989	[number]	k4	1989
zvolen	zvolit	k5eAaPmNgMnS	zvolit
jednomyslně	jednomyslně	k6eAd1	jednomyslně
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1990	[number]	k4	1990
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přejmenování	přejmenování	k1gNnSc3	přejmenování
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
socialistické	socialistický	k2eAgFnSc2d1	socialistická
republiky	republika	k1gFnSc2	republika
na	na	k7c4	na
Slovenskou	slovenský	k2eAgFnSc4d1	slovenská
republiku	republika	k1gFnSc4	republika
(	(	kIx(	(
<g/>
SR	SR	kA	SR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1990	[number]	k4	1990
<g />
.	.	kIx.	.
</s>
<s>
pak	pak	k6eAd1	pak
došlo	dojít	k5eAaPmAgNnS	dojít
i	i	k9	i
k	k	k7c3	k
přejmenování	přejmenování	k1gNnSc3	přejmenování
České	český	k2eAgFnSc2d1	Česká
socialistické	socialistický	k2eAgFnSc2d1	socialistická
republiky	republika	k1gFnSc2	republika
na	na	k7c4	na
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
(	(	kIx(	(
<g/>
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1990	[number]	k4	1990
následovalo	následovat	k5eAaImAgNnS	následovat
přejmenování	přejmenování	k1gNnSc1	přejmenování
Československé	československý	k2eAgFnSc2d1	Československá
socialistické	socialistický	k2eAgFnSc2d1	socialistická
republiky	republika	k1gFnSc2	republika
na	na	k7c4	na
Československou	československý	k2eAgFnSc4d1	Československá
federativní	federativní	k2eAgFnSc4d1	federativní
republiku	republika	k1gFnSc4	republika
<g/>
,	,	kIx,	,
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1990	[number]	k4	1990
však	však	k8xC	však
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
definitivnímu	definitivní	k2eAgNnSc3d1	definitivní
přejmenování	přejmenování	k1gNnSc3	přejmenování
na	na	k7c4	na
Česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
Federativní	federativní	k2eAgFnSc1d1	federativní
Republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Československo	Československo	k1gNnSc1	Československo
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
k	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosinci	prosinec	k1gInSc3	prosinec
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1993	[number]	k4	1993
existují	existovat	k5eAaImIp3nP	existovat
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
a	a	k8xC	a
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
republika	republika	k1gFnSc1	republika
jako	jako	k8xC	jako
dva	dva	k4xCgInPc1	dva
samostatné	samostatný	k2eAgInPc1d1	samostatný
a	a	k8xC	a
na	na	k7c6	na
sobě	se	k3xPyFc3	se
zcela	zcela	k6eAd1	zcela
nezávislé	závislý	k2eNgInPc1d1	nezávislý
státy	stát	k1gInPc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Sudetští	sudetský	k2eAgMnPc1d1	sudetský
Němci	Němec	k1gMnPc1	Němec
a	a	k8xC	a
Slováci	Slovák	k1gMnPc1	Slovák
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
-	-	kIx~	-
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnSc1	populace
Československa	Československo	k1gNnSc2	Československo
za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
jeho	jeho	k3xOp3gFnSc2	jeho
existence	existence	k1gFnSc2	existence
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
z	z	k7c2	z
14	[number]	k4	14
milionů	milion	k4xCgInPc2	milion
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
na	na	k7c4	na
15,6	[number]	k4	15,6
milionů	milion	k4xCgInPc2	milion
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
etnického	etnický	k2eAgNnSc2d1	etnické
hlediska	hledisko	k1gNnSc2	hledisko
tvořili	tvořit	k5eAaImAgMnP	tvořit
populaci	populace	k1gFnSc4	populace
z	z	k7c2	z
62,8	[number]	k4	62,8
<g/>
%	%	kIx~	%
Češi	Čech	k1gMnPc1	Čech
následovaní	následovaný	k2eAgMnPc1d1	následovaný
Slováky	Slováky	k1gInPc4	Slováky
se	s	k7c7	s
31	[number]	k4	31
%	%	kIx~	%
dále	daleko	k6eAd2	daleko
Maďary	maďar	k1gInPc4	maďar
3,8	[number]	k4	3,8
%	%	kIx~	%
Rómy	Róm	k1gMnPc4	Róm
0,7	[number]	k4	0,7
%	%	kIx~	%
<g/>
,	,	kIx,	,
Slezany	Slezan	k1gMnPc4	Slezan
0,3	[number]	k4	0,3
%	%	kIx~	%
<g/>
,	,	kIx,	,
Rusíny	Rusín	k1gMnPc7	Rusín
<g/>
,	,	kIx,	,
Ukrajinci	Ukrajinec	k1gMnPc7	Ukrajinec
<g/>
,	,	kIx,	,
Němci	Němec	k1gMnPc7	Němec
<g/>
,	,	kIx,	,
Poláky	Polák	k1gMnPc7	Polák
a	a	k8xC	a
Židy	Žid	k1gMnPc7	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Přirozený	přirozený	k2eAgInSc1d1	přirozený
přírůstek	přírůstek	k1gInSc1	přírůstek
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
2,7	[number]	k4	2,7
%	%	kIx~	%
a	a	k8xC	a
1,7	[number]	k4	1,7
%	%	kIx~	%
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
tedy	tedy	k9	tedy
klesal	klesat	k5eAaImAgInS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
převládalo	převládat	k5eAaImAgNnS	převládat
římskokatolické	římskokatolický	k2eAgNnSc1d1	římskokatolické
náboženství	náboženství	k1gNnSc1	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
komunismu	komunismus	k1gInSc2	komunismus
bylo	být	k5eAaImAgNnS	být
náboženství	náboženství	k1gNnSc1	náboženství
potlačováno	potlačovat	k5eAaImNgNnS	potlačovat
a	a	k8xC	a
prosazován	prosazován	k2eAgInSc1d1	prosazován
byl	být	k5eAaImAgInS	být
ateismus	ateismus	k1gInSc1	ateismus
<g/>
.	.	kIx.	.
</s>
<s>
JANČÍK	Jančík	k1gMnSc1	Jančík
<g/>
,	,	kIx,	,
Drahomír	Drahomír	k1gMnSc1	Drahomír
<g/>
.	.	kIx.	.
</s>
<s>
Bestrebungen	Bestrebungen	k1gInSc1	Bestrebungen
der	drát	k5eAaImRp2nS	drát
Tschechoslowakei	Tschechoslowakei	k1gNnSc4	Tschechoslowakei
um	um	k1gInSc1	um
eine	eine	k1gNnSc3	eine
wirtschaftliche	wirtschaftlichat	k5eAaPmIp3nS	wirtschaftlichat
Organisation	Organisation	k1gInSc1	Organisation
Mittel-	Mittel-	k1gFnSc2	Mittel-
und	und	k?	und
Südosteuropas	Südosteuropas	k1gInSc1	Südosteuropas
1918	[number]	k4	1918
<g/>
-	-	kIx~	-
<g/>
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
<s>
Prague	Prague	k1gFnSc1	Prague
Papers	Papersa	k1gFnPc2	Papersa
on	on	k3xPp3gMnSc1	on
the	the	k?	the
History	Histor	k1gInPc4	Histor
of	of	k?	of
International	International	k1gFnSc2	International
Relations	Relationsa	k1gFnPc2	Relationsa
<g/>
.	.	kIx.	.
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
,	,	kIx,	,
s.	s.	k?	s.
313	[number]	k4	313
<g/>
-	-	kIx~	-
<g/>
337	[number]	k4	337
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85899	[number]	k4	85899
<g/>
-	-	kIx~	-
<g/>
49	[number]	k4	49
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
KVAČEK	KVAČEK	k?	KVAČEK
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
Evropou	Evropa	k1gFnSc7	Evropa
zataženo	zatáhnout	k5eAaPmNgNnS	zatáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Československo	Československo	k1gNnSc1	Československo
a	a	k8xC	a
Evropa	Evropa	k1gFnSc1	Evropa
1933	[number]	k4	1933
<g/>
-	-	kIx~	-
<g/>
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
450	[number]	k4	450
s.	s.	k?	s.
PECKA	Pecka	k1gMnSc1	Pecka
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
armáda	armáda	k1gFnSc1	armáda
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
1968	[number]	k4	1968
<g/>
-	-	kIx~	-
<g/>
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Chronologický	chronologický	k2eAgInSc4d1	chronologický
přehled	přehled	k1gInSc4	přehled
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
soudobé	soudobý	k2eAgFnPc4d1	soudobá
dějiny	dějiny	k1gFnPc4	dějiny
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
231	[number]	k4	231
s.	s.	k?	s.
PECKA	Pecka	k1gMnSc1	Pecka
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
.	.	kIx.	.
</s>
<s>
Odsun	odsun	k1gInSc1	odsun
sovětských	sovětský	k2eAgNnPc2d1	sovětské
vojsk	vojsko	k1gNnPc2	vojsko
z	z	k7c2	z
Československa	Československo	k1gNnSc2	Československo
1989	[number]	k4	1989
<g/>
-	-	kIx~	-
<g/>
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Dokumenty	dokument	k1gInPc1	dokument
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
soudobé	soudobý	k2eAgFnPc4d1	soudobá
dějiny	dějiny	k1gFnPc4	dějiny
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
351	[number]	k4	351
s.	s.	k?	s.
RYCHLÍK	Rychlík	k1gMnSc1	Rychlík
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Češi	Čech	k1gMnPc1	Čech
a	a	k8xC	a
Slováci	Slovák	k1gMnPc1	Slovák
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
:	:	kIx,	:
spolupráce	spolupráce	k1gFnSc1	spolupráce
a	a	k8xC	a
konflikty	konflikt	k1gInPc1	konflikt
1914	[number]	k4	1914
<g/>
-	-	kIx~	-
<g/>
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
688	[number]	k4	688
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7429	[number]	k4	7429
<g/>
-	-	kIx~	-
<g/>
133	[number]	k4	133
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Kipke	Kipke	k6eAd1	Kipke
Rüdiger	Rüdiger	k1gMnSc1	Rüdiger
<g/>
,	,	kIx,	,
Vodička	Vodička	k1gMnSc1	Vodička
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
:	:	kIx,	:
Rozloučení	rozloučení	k1gNnSc1	rozloučení
s	s	k7c7	s
Československem	Československo	k1gNnSc7	Československo
<g/>
,	,	kIx,	,
Český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
202	[number]	k4	202
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
447	[number]	k4	447
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Kipke	Kipke	k6eAd1	Kipke
Rüdiger	Rüdiger	k1gMnSc1	Rüdiger
<g/>
,	,	kIx,	,
Vodička	Vodička	k1gMnSc1	Vodička
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
:	:	kIx,	:
Abschied	Abschied	k1gInSc1	Abschied
von	von	k1gInSc1	von
der	drát	k5eAaImRp2nS	drát
Tschechoslowakei	Tschechoslowake	k1gFnSc2	Tschechoslowake
<g/>
,	,	kIx,	,
Wissenschaft	Wissenschaft	k1gMnSc1	Wissenschaft
und	und	k?	und
Politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
Köln	Köln	k1gMnSc1	Köln
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
8046	[number]	k4	8046
<g/>
-	-	kIx~	-
<g/>
8803	[number]	k4	8803
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Vodička	Vodička	k1gMnSc1	Vodička
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
:	:	kIx,	:
Dělení	dělení	k1gNnSc1	dělení
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
poté	poté	k6eAd1	poté
<g/>
...	...	k?	...
<g/>
,	,	kIx,	,
Volvox	Volvox	k1gInSc1	Volvox
Globator	Globator	k1gInSc1	Globator
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7207	[number]	k4	7207
<g/>
-	-	kIx~	-
<g/>
479	[number]	k4	479
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Bojující	bojující	k2eAgNnSc1d1	bojující
Československo	Československo	k1gNnSc1	Československo
<g/>
:	:	kIx,	:
1938	[number]	k4	1938
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Vladimír	Vladimír	k1gMnSc1	Vladimír
Žikeš	Žikeš	k1gMnSc1	Žikeš
<g/>
,	,	kIx,	,
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
230	[number]	k4	230
s.	s.	k?	s.
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Československo	Československo	k1gNnSc4	Československo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Československo	Československo	k1gNnSc4	Československo
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
A	a	k9	a
Country	country	k2eAgInPc4d1	country
Study	stud	k1gInPc4	stud
<g/>
:	:	kIx,	:
Czechoslovakia	Czechoslovakia	k1gFnSc1	Czechoslovakia
(	(	kIx(	(
<g/>
Former	Former	k1gInSc1	Former
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Library	Librar	k1gInPc1	Librar
of	of	k?	of
Congress	Congress	k1gInSc1	Congress
<g/>
,	,	kIx,	,
Country	country	k2eAgInSc1d1	country
Studies	Studies	k1gInSc1	Studies
Návrhy	návrh	k1gInPc4	návrh
uspořádání	uspořádání	k1gNnSc2	uspořádání
ČSR	ČSR	kA	ČSR
<g/>
,	,	kIx,	,
alternativní	alternativní	k2eAgFnSc2d1	alternativní
historické	historický	k2eAgFnSc2d1	historická
koncepce	koncepce	k1gFnSc2	koncepce
státoprávního	státoprávní	k2eAgNnSc2d1	státoprávní
uspořádání	uspořádání	k1gNnSc2	uspořádání
Československa	Československo	k1gNnSc2	Československo
</s>
