<s>
Otto	Otto	k1gMnSc1	Otto
Wichterle	Wichterle	k1gFnSc2	Wichterle
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1913	[number]	k4	1913
Prostějov	Prostějov	k1gInSc1	Prostějov
–	–	k?	–
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1998	[number]	k4	1998
Stražisko	Stražisko	k1gNnSc1	Stražisko
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
světově	světově	k6eAd1	světově
proslulý	proslulý	k2eAgMnSc1d1	proslulý
český	český	k2eAgMnSc1d1	český
vědec	vědec	k1gMnSc1	vědec
a	a	k8xC	a
vynálezce	vynálezce	k1gMnSc1	vynálezce
<g/>
,	,	kIx,	,
pracující	pracující	k2eAgMnSc1d1	pracující
zejména	zejména	k9	zejména
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
makromolekulární	makromolekulární	k2eAgFnSc2d1	makromolekulární
organické	organický	k2eAgFnSc2d1	organická
chemie	chemie	k1gFnSc2	chemie
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
jejíž	jejíž	k3xOyRp3gMnPc4	jejíž
zakladatele	zakladatel	k1gMnPc4	zakladatel
patřil	patřit	k5eAaImAgInS	patřit
<g/>
.	.	kIx.	.
</s>
