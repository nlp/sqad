<p>
<s>
Make	Make	k1gFnSc1	Make
It	It	k1gFnSc2	It
or	or	k?	or
Break	break	k1gInSc1	break
It	It	k1gFnSc1	It
(	(	kIx(	(
<g/>
také	také	k9	také
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k8xS	jako
MIOBI	MIOBI	kA	MIOBI
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc1d1	americký
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
soustředěný	soustředěný	k2eAgInSc1d1	soustředěný
na	na	k7c4	na
život	život	k1gInSc4	život
gymnastek	gymnastka	k1gFnPc2	gymnastka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
dostat	dostat	k5eAaPmF	dostat
na	na	k7c4	na
Olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
byl	být	k5eAaImAgInS	být
inspirován	inspirovat	k5eAaBmNgInS	inspirovat
filmem	film	k1gInSc7	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
Rebelka	rebelka	k1gFnSc1	rebelka
<g/>
.	.	kIx.	.
</s>
<s>
Premiéra	premiéra	k1gFnSc1	premiéra
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
na	na	k7c4	na
stanici	stanice	k1gFnSc4	stanice
ABC	ABC	kA	ABC
Family	Famila	k1gFnSc2	Famila
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2009	[number]	k4	2009
a	a	k8xC	a
sledovalo	sledovat	k5eAaImAgNnS	sledovat
ji	on	k3xPp3gFnSc4	on
2,5	[number]	k4	2,5
milionů	milion	k4xCgInPc2	milion
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2009	[number]	k4	2009
bylo	být	k5eAaImAgNnS	být
objednáno	objednán	k2eAgNnSc1d1	objednáno
dalších	další	k2eAgFnPc2d1	další
10	[number]	k4	10
epizod	epizoda	k1gFnPc2	epizoda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
vysílat	vysílat	k5eAaImF	vysílat
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2010	[number]	k4	2010
získal	získat	k5eAaPmAgInS	získat
seriál	seriál	k1gInSc4	seriál
druhou	druhý	k4xOgFnSc4	druhý
sérii	série	k1gFnSc4	série
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
a	a	k8xC	a
poslední	poslední	k2eAgFnSc1d1	poslední
řada	řada	k1gFnSc1	řada
seriálu	seriál	k1gInSc2	seriál
byla	být	k5eAaImAgFnS	být
objednána	objednán	k2eAgFnSc1d1	objednána
16	[number]	k4	16
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
premiéru	premiér	k1gMnSc3	premiér
měla	mít	k5eAaImAgFnS	mít
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Finálová	finálový	k2eAgFnSc1d1	finálová
epizoda	epizoda	k1gFnSc1	epizoda
byla	být	k5eAaImAgFnS	být
vysílána	vysílat	k5eAaImNgFnS	vysílat
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
<g/>
Tvůrcem	tvůrce	k1gMnSc7	tvůrce
seriálu	seriál	k1gInSc2	seriál
je	být	k5eAaImIp3nS	být
Holly	Holla	k1gFnPc4	Holla
Sorensen	Sorensno	k1gNnPc2	Sorensno
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
s	s	k7c7	s
Paulem	Paul	k1gMnSc7	Paul
Stupinem	Stupin	k1gMnSc7	Stupin
a	a	k8xC	a
Johnem	John	k1gMnSc7	John
Ziffrenem	Ziffren	k1gMnSc7	Ziffren
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
exkluzivní	exkluzivní	k2eAgFnSc1d1	exkluzivní
producentka	producentka	k1gFnSc1	producentka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Seriál	seriál	k1gInSc1	seriál
se	se	k3xPyFc4	se
vysílal	vysílat	k5eAaImAgInS	vysílat
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
,	,	kIx,	,
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
Malajsii	Malajsie	k1gFnSc6	Malajsie
a	a	k8xC	a
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Seriál	seriál	k1gInSc1	seriál
sleduje	sledovat	k5eAaImIp3nS	sledovat
Payson	Payson	k1gMnSc1	Payson
Keller	Keller	k1gMnSc1	Keller
(	(	kIx(	(
<g/>
Ayla	Ayla	k1gMnSc1	Ayla
Kell	Kell	k1gMnSc1	Kell
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kaylie	Kaylie	k1gFnSc1	Kaylie
Cruz	Cruz	k1gMnSc1	Cruz
(	(	kIx(	(
<g/>
Josie	Josie	k1gFnSc1	Josie
Loren	Lorna	k1gFnPc2	Lorna
<g/>
)	)	kIx)	)
a	a	k8xC	a
Lauren	Laurna	k1gFnPc2	Laurna
Tanner	Tannra	k1gFnPc2	Tannra
(	(	kIx(	(
<g/>
Cassie	Cassie	k1gFnSc1	Cassie
Scerbo	Scerba	k1gMnSc5	Scerba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
profesionální	profesionální	k2eAgFnPc1d1	profesionální
gymnastky	gymnastka	k1gFnPc1	gymnastka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
trénují	trénovat	k5eAaImIp3nP	trénovat
v	v	k7c6	v
nejlepší	dobrý	k2eAgFnSc6d3	nejlepší
tělocvičně	tělocvična	k1gFnSc6	tělocvična
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
"	"	kIx"	"
<g/>
Rocky	rock	k1gInPc4	rock
Mountain	Mountaina	k1gFnPc2	Mountaina
Gymnastics	Gymnasticsa	k1gFnPc2	Gymnasticsa
Trainging	Trainging	k1gInSc1	Trainging
Center	centrum	k1gNnPc2	centrum
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Rock	rock	k1gInSc1	rock
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
v	v	k7c4	v
Boulder	Boulder	k1gInSc4	Boulder
v	v	k7c6	v
Coloradu	Colorado	k1gNnSc6	Colorado
a	a	k8xC	a
doufají	doufat	k5eAaImIp3nP	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyhrají	vyhrát	k5eAaPmIp3nP	vyhrát
olympijské	olympijský	k2eAgNnSc4d1	Olympijské
zlato	zlato	k1gNnSc4	zlato
na	na	k7c6	na
Olympiádě	olympiáda	k1gFnSc6	olympiáda
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Připojuje	připojovat	k5eAaImIp3nS	připojovat
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
nováček	nováček	k1gInSc1	nováček
Emily	Emil	k1gMnPc4	Emil
Kmetko	Kmetka	k1gFnSc5	Kmetka
(	(	kIx(	(
<g/>
Chelsea	Chelseum	k1gNnPc1	Chelseum
Hobbs	Hobbsa	k1gFnPc2	Hobbsa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
netrénovaná	trénovaný	k2eNgFnSc1d1	netrénovaná
gymnastka	gymnastka	k1gFnSc1	gymnastka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
prozrazeno	prozrazen	k2eAgNnSc1d1	prozrazeno
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gFnSc1	její
rodina	rodina	k1gFnSc1	rodina
má	mít	k5eAaImIp3nS	mít
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
penězi	peníze	k1gInPc7	peníze
a	a	k8xC	a
proto	proto	k8xC	proto
musí	muset	k5eAaImIp3nS	muset
pracovat	pracovat	k5eAaImF	pracovat
v	v	k7c6	v
restauraci	restaurace	k1gFnSc6	restaurace
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
získala	získat	k5eAaPmAgFnS	získat
peníze	peníz	k1gInPc4	peníz
a	a	k8xC	a
mohla	moct	k5eAaImAgFnS	moct
tak	tak	k6eAd1	tak
obdržet	obdržet	k5eAaPmF	obdržet
stipendium	stipendium	k1gNnSc4	stipendium
a	a	k8xC	a
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
gymnastice	gymnastika	k1gFnSc6	gymnastika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
role	role	k1gFnSc1	role
===	===	k?	===
</s>
</p>
<p>
<s>
Josie	Josie	k1gFnSc1	Josie
Loren	Lorna	k1gFnPc2	Lorna
jako	jako	k8xC	jako
Kaylie	Kaylie	k1gFnSc2	Kaylie
Cruz	Cruza	k1gFnPc2	Cruza
</s>
</p>
<p>
<s>
Ayla	Ayla	k1gMnSc1	Ayla
Kell	Kell	k1gMnSc1	Kell
jako	jako	k8xC	jako
Payson	Payson	k1gMnSc1	Payson
Keeler	Keeler	k1gMnSc1	Keeler
</s>
</p>
<p>
<s>
Cassie	Cassie	k1gFnSc5	Cassie
Scerbo	Scerba	k1gFnSc5	Scerba
jako	jako	k9	jako
Lauren	Laurno	k1gNnPc2	Laurno
Tanner	Tannra	k1gFnPc2	Tannra
</s>
</p>
<p>
<s>
Chelsea	Chelsea	k1gFnSc1	Chelsea
Hobbs	Hobbsa	k1gFnPc2	Hobbsa
jako	jako	k8xC	jako
Emily	Emil	k1gMnPc4	Emil
Kmetko	Kmetka	k1gMnSc5	Kmetka
</s>
</p>
<p>
<s>
Candace	Candace	k1gFnSc1	Candace
Cameron	Cameron	k1gMnSc1	Cameron
Bure	Bure	k1gInSc1	Bure
jako	jako	k8xS	jako
Summer	Summer	k1gInSc1	Summer
van	vana	k1gFnPc2	vana
Horne	Horn	k1gMnSc5	Horn
</s>
</p>
<p>
<s>
Neil	Neil	k1gMnSc1	Neil
Jackson	Jackson	k1gMnSc1	Jackson
jako	jako	k8xS	jako
Sasha	Sasha	k1gMnSc1	Sasha
Belov	Belov	k1gInSc4	Belov
</s>
</p>
<p>
<s>
Anthony	Anthona	k1gFnPc1	Anthona
Starke	Starke	k1gFnSc1	Starke
jako	jako	k8xC	jako
Steve	Steve	k1gMnSc1	Steve
Tanner	Tanner	k1gMnSc1	Tanner
</s>
</p>
<p>
<s>
Peri	peri	k1gFnSc3	peri
Gilpin	Gilpin	k1gMnSc1	Gilpin
jako	jako	k8xC	jako
Kim	Kim	k1gMnSc1	Kim
Keeler	Keeler	k1gMnSc1	Keeler
</s>
</p>
<p>
<s>
Susan	Susan	k1gMnSc1	Susan
Ward	Ward	k1gMnSc1	Ward
jako	jako	k8xS	jako
Chloe	Chloe	k1gFnSc1	Chloe
Kmetko	Kmetka	k1gFnSc5	Kmetka
</s>
</p>
<p>
<s>
Johnny	Johnna	k1gMnSc2	Johnna
Pacar	Pacar	k1gMnSc1	Pacar
jako	jako	k8xC	jako
Damon	Damon	k1gMnSc1	Damon
Young	Young	k1gMnSc1	Young
</s>
</p>
<p>
<s>
Dondre	Dondr	k1gInSc5	Dondr
Whitfeld	Whitfeld	k1gMnSc1	Whitfeld
jako	jako	k8xS	jako
Trenér	trenér	k1gMnSc1	trenér
McIntire	McIntir	k1gInSc5	McIntir
(	(	kIx(	(
<g/>
Mac	Mac	kA	Mac
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Vedlejší	vedlejší	k2eAgFnSc2d1	vedlejší
role	role	k1gFnSc2	role
===	===	k?	===
</s>
</p>
<p>
<s>
Rosa	Rosa	k1gFnSc1	Rosa
Blasi	Blase	k1gFnSc4	Blase
jako	jako	k8xS	jako
Ronnie	Ronnie	k1gFnPc4	Ronnie
Cruz	Cruza	k1gFnPc2	Cruza
</s>
</p>
<p>
<s>
Jason	Jason	k1gMnSc1	Jason
Manuel	Manuel	k1gMnSc1	Manuel
Olazabal	Olazabal	k1gInSc1	Olazabal
jako	jako	k8xS	jako
Alex	Alex	k1gMnSc1	Alex
Cruz	Cruz	k1gMnSc1	Cruz
</s>
</p>
<p>
<s>
Brett	Brett	k2eAgInSc1d1	Brett
Cullen	Cullen	k1gInSc1	Cullen
jako	jako	k8xC	jako
Mark	Mark	k1gMnSc1	Mark
Keeler	Keeler	k1gMnSc1	Keeler
</s>
</p>
<p>
<s>
Nicole	Nicola	k1gFnSc3	Nicola
Gale	Gale	k1gMnSc1	Gale
Anderson	Anderson	k1gMnSc1	Anderson
jako	jako	k8xS	jako
Kelly	Kella	k1gFnPc1	Kella
Parker	Parkra	k1gFnPc2	Parkra
</s>
</p>
<p>
<s>
Zachary	Zachara	k1gFnPc1	Zachara
Burr	Burr	k1gMnSc1	Burr
Abel	Abel	k1gMnSc1	Abel
jako	jako	k8xS	jako
Carter	Carter	k1gMnSc1	Carter
Anderson	Anderson	k1gMnSc1	Anderson
</s>
</p>
<p>
<s>
Nico	Nico	k1gMnSc1	Nico
Tortorella	Tortorella	k1gMnSc1	Tortorella
jako	jako	k8xS	jako
Razor	Razor	k1gMnSc1	Razor
</s>
</p>
<p>
<s>
Michelle	Michelle	k1gFnSc1	Michelle
Clunie	Clunie	k1gFnSc1	Clunie
jako	jako	k8xS	jako
Ellen	Ellen	k2eAgInSc1d1	Ellen
Beals	Beals	k1gInSc1	Beals
</s>
</p>
<p>
<s>
Mia	Mia	k?	Mia
Rose	Rose	k1gMnSc1	Rose
Frampton	Frampton	k1gInSc1	Frampton
jako	jako	k8xS	jako
Rebecca	Rebecca	k1gFnSc1	Rebecca
"	"	kIx"	"
<g/>
Becca	Becca	k1gMnSc1	Becca
<g/>
"	"	kIx"	"
Keeler	Keeler	k1gMnSc1	Keeler
</s>
</p>
<p>
<s>
Marcus	Marcus	k1gMnSc1	Marcus
Coloma	Colom	k1gMnSc4	Colom
jako	jako	k9	jako
Leo	Leo	k1gMnSc1	Leo
Cruz	Cruz	k1gMnSc1	Cruz
</s>
</p>
<p>
<s>
Cody	coda	k1gFnPc1	coda
Longo	Longo	k6eAd1	Longo
jako	jako	k9	jako
Nicky	nicka	k1gFnSc2	nicka
"	"	kIx"	"
<g/>
Nick	Nick	k1gInSc1	Nick
<g/>
"	"	kIx"	"
Russo	Russa	k1gFnSc5	Russa
</s>
</p>
<p>
<s>
Erik	Erika	k1gFnPc2	Erika
Palladino	Palladin	k2eAgNnSc4d1	Palladino
jako	jako	k9	jako
Martin	Martin	k1gInSc4	Martin
"	"	kIx"	"
<g/>
Marty	Marta	k1gFnSc2	Marta
<g/>
"	"	kIx"	"
Walsh	Walsh	k1gMnSc1	Walsh
</s>
</p>
<p>
<s>
Marsha	Marsha	k1gMnSc1	Marsha
Thomason	Thomason	k1gMnSc1	Thomason
jako	jako	k8xS	jako
Mary	Mary	k1gFnSc1	Mary
Jan	Jan	k1gMnSc1	Jan
"	"	kIx"	"
<g/>
MJ	mj	kA	mj
<g/>
"	"	kIx"	"
Martin	Martin	k1gMnSc1	Martin
</s>
</p>
<p>
<s>
Wyatt	Wyatt	k1gMnSc1	Wyatt
Smith	Smith	k1gMnSc1	Smith
jako	jako	k8xC	jako
Brian	Brian	k1gMnSc1	Brian
Kmetko	Kmetka	k1gFnSc5	Kmetka
</s>
</p>
<p>
<s>
Meagan	Meagan	k1gMnSc1	Meagan
Holder	Holder	k1gMnSc1	Holder
jako	jako	k8xC	jako
Darby	Darba	k1gFnPc1	Darba
Conrad	Conrada	k1gFnPc2	Conrada
</s>
</p>
<p>
<s>
Joshua	Joshua	k1gMnSc1	Joshua
Bowman	Bowman	k1gMnSc1	Bowman
jako	jako	k8xC	jako
Max	Max	k1gMnSc1	Max
Spencer	Spencer	k1gMnSc1	Spencer
</s>
</p>
<p>
<s>
Kathy	Kath	k1gInPc1	Kath
Najimy	Najima	k1gFnSc2	Najima
jako	jako	k8xC	jako
Sheila	Sheila	k1gFnSc1	Sheila
Baboyon	Baboyona	k1gFnPc2	Baboyona
</s>
</p>
<p>
<s>
Zane	Zane	k5eAaImRp2nP	Zane
Holtz	Holtz	k1gInSc4	Holtz
jako	jako	k8xS	jako
Austin	Austin	k2eAgInSc4d1	Austin
Tucker	Tucker	k1gInSc4	Tucker
</s>
</p>
<p>
<s>
Chelsea	Chelsea	k1gMnSc1	Chelsea
Taveres	Taveres	k1gMnSc1	Taveres
jako	jako	k8xC	jako
Jordan	Jordan	k1gMnSc1	Jordan
Randall	Randall	k1gMnSc1	Randall
</s>
</p>
<p>
<s>
Amanda	Amanda	k1gFnSc1	Amanda
Leighton	Leighton	k1gInSc1	Leighton
jako	jako	k8xC	jako
Wendy	Wenda	k1gFnPc1	Wenda
Capshaw	Capshaw	k1gFnSc2	Capshaw
</s>
</p>
<p>
<s>
Tom	Tom	k1gMnSc1	Tom
Maden	Maden	k2eAgMnSc1d1	Maden
jako	jako	k8xC	jako
Rigo	Riga	k1gFnSc5	Riga
</s>
</p>
<p>
<s>
Russel	Russel	k1gInSc1	Russel
Pitts	Pittsa	k1gFnPc2	Pittsa
jako	jako	k8xC	jako
Jake	Jake	k1gFnPc2	Jake
</s>
</p>
<p>
<s>
==	==	k?	==
Epizody	epizoda	k1gFnSc2	epizoda
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Na	na	k7c4	na
DVD	DVD	kA	DVD
===	===	k?	===
</s>
</p>
<p>
<s>
Walt	Walt	k1gMnSc1	Walt
Disney	Disnea	k1gFnSc2	Disnea
Studios	Studios	k?	Studios
Home	Hom	k1gFnSc2	Hom
Entertainment	Entertainment	k1gMnSc1	Entertainment
vydalo	vydat	k5eAaPmAgNnS	vydat
prvních	první	k4xOgNnPc6	první
deset	deset	k4xCc1	deset
epizod	epizoda	k1gFnPc2	epizoda
v	v	k7c6	v
setu	set	k1gInSc6	set
nazvaném	nazvaný	k2eAgInSc6d1	nazvaný
Make	Make	k1gNnSc4	Make
It	It	k1gFnSc3	It
or	or	k?	or
Break	break	k1gInSc1	break
It	It	k1gFnSc1	It
-	-	kIx~	-
Volume	volum	k1gInSc5	volum
1	[number]	k4	1
<g/>
:	:	kIx,	:
Extandet	Extandet	k1gInSc1	Extandet
Edidtion	Edidtion	k1gInSc1	Edidtion
<g/>
.	.	kIx.	.
</s>
<s>
DVD	DVD	kA	DVD
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
bonusové	bonusový	k2eAgInPc4d1	bonusový
materiály	materiál	k1gInPc4	materiál
<g/>
:	:	kIx,	:
vymazané	vymazaný	k2eAgFnPc4d1	vymazaná
scény	scéna	k1gFnPc4	scéna
<g/>
,	,	kIx,	,
nikdy-neviděné	nikdyeviděný	k2eAgNnSc4d1	nikdy-neviděný
finále	finále	k1gNnSc4	finále
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2011	[number]	k4	2011
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
Volume	volum	k1gInSc5	volum
2	[number]	k4	2
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2011	[number]	k4	2011
Season	Season	k1gInSc1	Season
2	[number]	k4	2
<g/>
,	,	kIx,	,
Volume	volum	k1gInSc5	volum
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vysílání	vysílání	k1gNnSc2	vysílání
==	==	k?	==
</s>
</p>
<p>
<s>
Seriál	seriál	k1gInSc1	seriál
se	se	k3xPyFc4	se
vysílal	vysílat	k5eAaImAgInS	vysílat
na	na	k7c4	na
stanici	stanice	k1gFnSc4	stanice
E4	E4	k1gFnSc2	E4
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
,	,	kIx,	,
ABC	ABC	kA	ABC
Spark	Spark	k1gInSc1	Spark
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
Zee	Zee	k1gFnSc6	Zee
Café	café	k1gNnSc2	café
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
Fox	fox	k1gInSc1	fox
<g/>
8	[number]	k4	8
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
<g/>
RTM	RTM	kA	RTM
<g/>
2	[number]	k4	2
v	v	k7c6	v
Malajsii	Malajsie	k1gFnSc6	Malajsie
a	a	k8xC	a
na	na	k7c6	na
TV2	TV2	k1gFnSc6	TV2
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
a	a	k8xC	a
nominace	nominace	k1gFnSc1	nominace
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Make	Mak	k1gMnSc2	Mak
It	It	k1gMnSc2	It
or	or	k?	or
Break	break	k1gInSc1	break
It	It	k1gFnPc2	It
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
