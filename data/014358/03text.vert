<s>
Montevideo	Montevideo	k1gNnSc1
</s>
<s>
Montevideo	Montevideo	k1gNnSc1
San	San	k1gFnSc2
Felipe	Felip	k1gInSc5
y	y	k?
Santiago	Santiago	k1gNnSc1
de	de	k?
Montevideo	Montevideo	k1gNnSc1
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
34	#num#	k4
<g/>
°	°	k?
<g/>
52	#num#	k4
<g/>
′	′	k?
j.	j.	k?
š.	š.	k?
<g/>
,	,	kIx,
56	#num#	k4
<g/>
°	°	k?
<g/>
10	#num#	k4
<g/>
′	′	k?
z.	z.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
43	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Uruguay	Uruguay	k1gFnSc1
Uruguay	Uruguay	k1gFnSc1
</s>
<s>
Montevideo	Montevideo	k1gNnSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
730	#num#	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
1	#num#	k4
319	#num#	k4
108	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
1	#num#	k4
807	#num#	k4
obyv	obyva	k1gFnPc2
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Starosta	Starosta	k1gMnSc1
</s>
<s>
Christian	Christian	k1gMnSc1
Di	Di	k1gMnSc2
Candia	Candium	k1gNnSc2
(	(	kIx(
<g/>
od	od	k7c2
2019	#num#	k4
<g/>
)	)	kIx)
Vznik	vznik	k1gInSc1
</s>
<s>
1726	#num#	k4
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.montevideo.gub.uy	www.montevideo.gub.u	k2eAgInPc1d1
Telefonní	telefonní	k2eAgInPc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
2	#num#	k4
PSČ	PSČ	kA
</s>
<s>
11000	#num#	k4
<g/>
–	–	k?
<g/>
12000	#num#	k4
Označení	označení	k1gNnSc4
vozidel	vozidlo	k1gNnPc2
</s>
<s>
S	s	k7c7
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Družicový	družicový	k2eAgInSc1d1
pohled	pohled	k1gInSc1
na	na	k7c4
Río	Río	k1gFnPc4
de	de	k?
la	la	k1gNnSc1
Plata	plato	k1gNnSc2
směrem	směr	k1gInSc7
k	k	k7c3
východu	východ	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlevo	vlevo	k6eAd1
nahoře	nahoře	k6eAd1
<g/>
,	,	kIx,
na	na	k7c6
rozhraní	rozhraní	k1gNnSc6
modré	modrý	k2eAgFnSc2d1
oceánské	oceánský	k2eAgFnSc2d1
a	a	k8xC
kalné	kalný	k2eAgFnSc2d1
říční	říční	k2eAgFnSc2d1
vody	voda	k1gFnSc2
se	se	k3xPyFc4
při	při	k7c6
ústí	ústí	k1gNnSc6
řeky	řeka	k1gFnSc2
Santa	Santa	k1gMnSc1
Lucía	Lucía	k1gMnSc1
rozkládá	rozkládat	k5eAaImIp3nS
Montevideo	Montevideo	k1gNnSc4
<g/>
,	,	kIx,
vpravo	vpravo	k6eAd1
dole	dole	k6eAd1
pak	pak	k6eAd1
Buenos	Buenos	k1gMnSc1
Aires	Aires	k1gMnSc1
</s>
<s>
Pohled	pohled	k1gInSc1
na	na	k7c4
centrum	centrum	k1gNnSc4
města	město	k1gNnSc2
</s>
<s>
Montevideo	Montevideo	k1gNnSc1
je	být	k5eAaImIp3nS
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Uruguaye	Uruguay	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
na	na	k7c6
východním	východní	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
Atlantského	atlantský	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
při	při	k7c6
severovýchodním	severovýchodní	k2eAgInSc6d1
okraji	okraj	k1gInSc6
Río	Río	k1gFnSc2
de	de	k?
la	la	k1gNnSc1
Plata	plato	k1gNnSc2
<g/>
,	,	kIx,
společného	společný	k2eAgNnSc2d1
nálevkovitého	nálevkovitý	k2eAgNnSc2d1
ústí	ústí	k1gNnSc2
řek	řeka	k1gFnPc2
Uruguay	Uruguay	k1gFnSc1
a	a	k8xC
Paraná	Paraná	k1gFnSc1
<g/>
,	,	kIx,
asi	asi	k9
220	#num#	k4
km	km	kA
východně	východně	k6eAd1
od	od	k7c2
argentinské	argentinský	k2eAgFnSc2d1
metropole	metropol	k1gFnSc2
Buenos	Buenos	k1gMnSc1
Aires	Aires	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
městě	město	k1gNnSc6
žije	žít	k5eAaImIp3nS
zhruba	zhruba	k6eAd1
1,4	1,4	k4
milionu	milion	k4xCgInSc2
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
tedy	tedy	k9
40	#num#	k4
%	%	kIx~
obyvatelstva	obyvatelstvo	k1gNnSc2
Uruguaye	Uruguay	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
je	být	k5eAaImIp3nS
nejvýznamnějším	významný	k2eAgInSc7d3
námořním	námořní	k2eAgInSc7d1
i	i	k8xC
turistickým	turistický	k2eAgInSc7d1
přístavem	přístav	k1gInSc7
<g/>
,	,	kIx,
administrativním	administrativní	k2eAgMnSc7d1
<g/>
,	,	kIx,
hospodářským	hospodářský	k2eAgNnSc7d1
a	a	k8xC
kulturním	kulturní	k2eAgNnSc7d1
centrem	centrum	k1gNnSc7
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bývá	bývat	k5eAaImIp3nS
označováno	označovat	k5eAaImNgNnS
za	za	k7c4
nejbezpečnější	bezpečný	k2eAgNnSc4d3
město	město	k1gNnSc4
celé	celý	k2eAgFnSc2d1
Latinské	latinský	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
městě	město	k1gNnSc6
je	být	k5eAaImIp3nS
také	také	k9
Mezinárodní	mezinárodní	k2eAgNnSc1d1
letiště	letiště	k1gNnSc1
Carrasco	Carrasco	k6eAd1
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
nejdůležitější	důležitý	k2eAgNnSc4d3
letiště	letiště	k1gNnSc4
celého	celý	k2eAgInSc2d1
Uruguaye	Uruguay	k1gFnPc5
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Etymologie	etymologie	k1gFnSc1
názvu	název	k1gInSc2
pochází	pocházet	k5eAaImIp3nS
podle	podle	k7c2
tradice	tradice	k1gFnSc2
ze	z	k7c2
zvolání	zvolání	k1gNnSc2
námořníka	námořník	k1gMnSc2
z	z	k7c2
výpravy	výprava	k1gFnSc2
portugalského	portugalský	k2eAgMnSc2d1
objevitele	objevitel	k1gMnSc2
země	zem	k1gFnSc2
<g/>
,	,	kIx,
mořeplavce	mořeplavec	k1gMnSc2
Fernanda	Fernanda	k1gFnSc1
Magalhã	Magalhã	k1gFnSc1
"	"	kIx"
<g/>
Viděl	vidět	k5eAaImAgMnS
jsem	být	k5eAaImIp1nS
pahorek	pahorek	k1gInSc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
"	"	kIx"
(	(	kIx(
<g/>
„	„	k?
<g/>
Monte	Mont	k1gInSc5
vi	vi	k?
eu	eu	k?
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
nejstarších	starý	k2eAgFnPc6d3
námořních	námořní	k2eAgFnPc6d1
mapách	mapa	k1gFnPc6
je	být	k5eAaImIp3nS
označení	označení	k1gNnSc1
podle	podle	k7c2
zasvěcení	zasvěcení	k1gNnSc2
„	„	k?
<g/>
Monte	Mont	k1gInSc5
de	de	k?
San	San	k1gMnSc1
Ovidio	Ovidio	k1gMnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
Hora	Hora	k1gMnSc1
sv.	sv.	kA
Ovidia	Ovidius	k1gMnSc4
podle	podle	k7c2
sicilského	sicilský	k2eAgMnSc2d1
misionáře	misionář	k1gMnSc2
a	a	k8xC
mučedníka	mučedník	k1gMnSc2
<g/>
,	,	kIx,
umučeného	umučený	k2eAgInSc2d1
roku	rok	k1gInSc2
135	#num#	k4
v	v	k7c6
Portugalsku	Portugalsko	k1gNnSc6
<g/>
)	)	kIx)
nebo	nebo	k8xC
popis	popis	k1gInSc4
"	"	kIx"
<g/>
Šestá	šestý	k4xOgFnSc1
hora	hora	k1gFnSc1
od	od	k7c2
východu	východ	k1gInSc2
na	na	k7c4
západ	západ	k1gInSc4
„	„	k?
<g/>
Monte	Mont	k1gInSc5
VI	VI	kA
D	D	kA
<g/>
(	(	kIx(
<g/>
e	e	k0
<g/>
)	)	kIx)
E	E	kA
<g/>
(	(	kIx(
<g/>
ste	ste	k?
a	a	k8xC
<g/>
)	)	kIx)
O	o	k7c4
<g/>
(	(	kIx(
<g/>
este	este	k1gFnSc4
<g/>
)	)	kIx)
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
autentický	autentický	k2eAgInSc4d1
132	#num#	k4
metrů	metr	k1gInPc2
vysoký	vysoký	k2eAgInSc4d1
pahorekCerro	pahorekCerro	k6eAd1
z	z	k7c2
pohoří	pohoří	k1gNnSc2
Guaraní	Guaraní	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
dosud	dosud	k6eAd1
pohledově	pohledově	k6eAd1
dominantní	dominantní	k2eAgFnSc1d1
pro	pro	k7c4
turisty	turist	k1gMnPc4
připlouvající	připlouvající	k2eAgInSc1d1
lodí	loď	k1gFnSc7
po	po	k7c6
moři	moře	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Rekonstrukce	rekonstrukce	k1gFnSc1
města	město	k1gNnSc2
z	z	k7c2
konce	konec	k1gInSc2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Vstupní	vstupní	k2eAgFnSc1d1
brána	brána	k1gFnSc1
barokní	barokní	k2eAgFnSc2d1
citadely	citadela	k1gFnSc2
<g/>
,	,	kIx,
jediná	jediný	k2eAgFnSc1d1
dochovaná	dochovaný	k2eAgFnSc1d1
část	část	k1gFnSc1
z	z	k7c2
r.	r.	kA
1741	#num#	k4
</s>
<s>
Katedrála	katedrála	k1gFnSc1
a	a	k8xC
kašna	kašna	k1gFnSc1
na	na	k7c6
Náměstí	náměstí	k1gNnSc6
nezávislosti	nezávislost	k1gFnSc2
</s>
<s>
Palác	palác	k1gInSc1
Salvo	salva	k1gFnSc5
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Osada	osada	k1gFnSc1
zaujímala	zaujímat	k5eAaImAgFnS
od	od	k7c2
počátku	počátek	k1gInSc2
důležité	důležitý	k2eAgNnSc4d1
strategické	strategický	k2eAgNnSc4d1
místo	místo	k1gNnSc4
při	při	k7c6
ústí	ústí	k1gNnSc6
Rio	Rio	k1gFnSc2
de	de	k?
la	la	k1gNnSc1
Plata	plato	k1gNnSc2
<g/>
,	,	kIx,
k	k	k7c3
ochraně	ochrana	k1gFnSc3
říčního	říční	k2eAgMnSc2d1
i	i	k8xC
námořního	námořní	k2eAgInSc2d1
obchodu	obchod	k1gInSc2
a	a	k8xC
jako	jako	k9
katolická	katolický	k2eAgFnSc1d1
misijní	misijní	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
jezuitů	jezuita	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gNnSc1
osídlení	osídlení	k1gNnSc1
kromě	kromě	k7c2
Indiánů	Indián	k1gMnPc2
kmene	kmen	k1gInSc2
Charrua	Charrua	k1gMnSc1
tvořili	tvořit	k5eAaImAgMnP
původem	původ	k1gInSc7
imigranti	imigrant	k1gMnPc1
z	z	k7c2
Kanárských	kanárský	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
především	především	k9
míšenci	míšenec	k1gMnPc7
ras	rasa	k1gFnPc2
<g/>
,	,	kIx,
souhrnně	souhrnně	k6eAd1
zvaní	zvaný	k2eAgMnPc1d1
gaučové	gaučo	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Opevněné	opevněný	k2eAgNnSc1d1
město	město	k1gNnSc1
bylo	být	k5eAaImAgNnS
založeno	založit	k5eAaPmNgNnS
Španěly	Španěl	k1gMnPc7
roku	rok	k1gInSc2
1724	#num#	k4
<g/>
,	,	kIx,
oficiálně	oficiálně	k6eAd1
je	být	k5eAaImIp3nS
zakládací	zakládací	k2eAgFnSc7d1
listinou	listina	k1gFnSc7
stvrdil	stvrdit	k5eAaPmAgInS
španělský	španělský	k2eAgMnSc1d1
voják	voják	k1gMnSc1
Don	Don	k1gMnSc1
Bruno	Bruno	k1gMnSc1
Mauricio	Mauricio	k1gMnSc1
de	de	k?
Zabala	Zabala	k1gFnSc1
<g/>
,	,	kIx,
guvernér	guvernér	k1gMnSc1
z	z	k7c2
Buenos	Buenos	k1gMnSc1
Aires	Aires	k1gInSc4
24	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1726	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
a	a	k8xC
je	být	k5eAaImIp3nS
důležitým	důležitý	k2eAgInSc7d1
přístavem	přístav	k1gInSc7
<g/>
,	,	kIx,
kulturním	kulturní	k2eAgNnSc7d1
a	a	k8xC
náboženským	náboženský	k2eAgNnSc7d1
centrem	centrum	k1gNnSc7
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
hlavní	hlavní	k2eAgInSc1d1
biskupský	biskupský	k2eAgInSc1d1
chrám	chrám	k1gInSc1
byla	být	k5eAaImAgFnS
roku	rok	k1gInSc2
1790	#num#	k4
založena	založen	k2eAgFnSc1d1
katedrála	katedrála	k1gFnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Španělská	španělský	k2eAgFnSc1d1
koloniální	koloniální	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
trvala	trvat	k5eAaImAgFnS
do	do	k7c2
roku	rok	k1gInSc2
1814	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
občanské	občanský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
mezi	mezi	k7c7
přistěhovalci	přistěhovalec	k1gMnPc7
ze	z	k7c2
Španělska	Španělsko	k1gNnSc2
<g/>
,	,	kIx,
Anglie	Anglie	k1gFnSc2
a	a	k8xC
Itálie	Itálie	k1gFnSc2
roku	rok	k1gInSc2
1814	#num#	k4
zvítězila	zvítězit	k5eAaPmAgFnS
vojenská	vojenský	k2eAgFnSc1d1
junta	junta	k1gFnSc1
vedená	vedený	k2eAgFnSc1d1
generálem	generál	k1gMnSc7
José	Josá	k1gFnSc2
Artigasem	Artigas	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
roku	rok	k1gInSc2
1817	#num#	k4
vyhlásil	vyhlásit	k5eAaPmAgInS
nezávislý	závislý	k2eNgInSc1d1
svazek	svazek	k1gInSc1
latinskoamerických	latinskoamerický	k2eAgInPc2d1
států	stát	k1gInPc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
ligu	liga	k1gFnSc4
<g/>
)	)	kIx)
a	a	k8xC
Montevideo	Montevideo	k1gNnSc1
jeho	jeho	k3xOp3gNnSc7
hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
neúspěchu	neúspěch	k1gInSc6
byl	být	k5eAaImAgInS
nucen	nucen	k2eAgInSc1d1
uprchnout	uprchnout	k5eAaPmF
a	a	k8xC
nezávislá	závislý	k2eNgFnSc1d1
Uruguay	Uruguay	k1gFnSc1
byla	být	k5eAaImAgFnS
vyhlášena	vyhlásit	k5eAaPmNgFnS
až	až	k9
roku	rok	k1gInSc2
1828	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Válečným	válečný	k2eAgInSc7d1
konfliktem	konflikt	k1gInSc7
zvaným	zvaný	k2eAgInSc7d1
Velká	velký	k2eAgFnSc1d1
válka	válka	k1gFnSc1
(	(	kIx(
<g/>
Guerra	Guerra	k1gFnSc1
grande	grand	k1gMnSc5
<g/>
)	)	kIx)
trpělo	trpět	k5eAaImAgNnS
město	město	k1gNnSc1
v	v	k7c6
letech	léto	k1gNnPc6
1843	#num#	k4
-	-	kIx~
1851	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
počátku	počátek	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
zde	zde	k6eAd1
usídlilo	usídlit	k5eAaPmAgNnS
mnoho	mnoho	k4c1
dalších	další	k2eAgMnPc2d1
přistěhovalců	přistěhovalec	k1gMnPc2
ze	z	k7c2
Španělska	Španělsko	k1gNnSc2
a	a	k8xC
Itálie	Itálie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prosinci	prosinec	k1gInSc6
roku	rok	k1gInSc2
1933	#num#	k4
se	se	k3xPyFc4
zde	zde	k6eAd1
konala	konat	k5eAaImAgFnS
mezinárodní	mezinárodní	k2eAgFnSc1d1
konference	konference	k1gFnSc1
<g/>
,	,	kIx,
výsledkem	výsledek	k1gInSc7
jednání	jednání	k1gNnSc2
byla	být	k5eAaImAgFnS
Montevidejská	montevidejský	k2eAgFnSc1d1
konvence	konvence	k1gFnSc1
o	o	k7c6
právech	právo	k1gNnPc6
a	a	k8xC
povinnostech	povinnost	k1gFnPc6
států	stát	k1gInPc2
<g/>
,	,	kIx,
úmluva	úmluva	k1gFnSc1
podepsaná	podepsaný	k2eAgFnSc1d1
26	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1933	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1939	#num#	k4
zde	zde	k6eAd1
byla	být	k5eAaImAgFnS
zneškodněna	zneškodněn	k2eAgFnSc1d1
německá	německý	k2eAgFnSc1d1
válečná	válečná	k1gFnSc1
loď	loď	k1gFnSc1
Admiral	Admiral	k1gFnSc1
Graf	graf	k1gInSc1
Spee	Spee	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
potopila	potopit	k5eAaPmAgFnS
devět	devět	k4xCc4
lodí	loď	k1gFnPc2
britských	britský	k2eAgMnPc2d1
<g/>
,	,	kIx,
tři	tři	k4xCgInPc1
britské	britský	k2eAgInPc1d1
křižníky	křižník	k1gInPc1
pod	pod	k7c7
vedením	vedení	k1gNnSc7
komodora	komodor	k1gMnSc2
Henryho	Henry	k1gMnSc2
Harwooda	Harwood	k1gMnSc2
rozpoutaly	rozpoutat	k5eAaPmAgFnP
následně	následně	k6eAd1
bitvu	bitva	k1gFnSc4
o	o	k7c6
ústí	ústí	k1gNnSc6
Rio	Rio	k1gFnSc2
de	de	k?
la	la	k1gNnSc1
Plata	plato	k1gNnSc2
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Pamětihodnosti	pamětihodnost	k1gFnPc1
</s>
<s>
Montevideo	Montevideo	k1gNnSc1
nabízí	nabízet	k5eAaImIp3nS
v	v	k7c6
historickém	historický	k2eAgNnSc6d1
centru	centrum	k1gNnSc6
množství	množství	k1gNnSc4
památekː	památekː	k?
</s>
<s>
Náměstí	náměstí	k1gNnSc1
ústavy	ústava	k1gFnSc2
-	-	kIx~
Plaza	plaz	k1gMnSc2
Constitución	Constitución	k1gMnSc1
</s>
<s>
katedrála	katedrála	k1gFnSc1
Panny	Panna	k1gFnSc2
Marie	Marie	k1gFnSc1
-	-	kIx~
Iglesia	Iglesia	k1gFnSc1
Matriz	Matriza	k1gFnPc2
neoklasicistní	neoklasicistní	k2eAgFnSc1d1
budova	budova	k1gFnSc1
se	s	k7c7
dvěma	dva	k4xCgFnPc7
věžemi	věž	k1gFnPc7
v	v	k7c6
průčelí	průčelí	k1gNnSc6
a	a	k8xC
kupolí	kupole	k1gFnPc2
v	v	k7c6
křížení	křížení	k1gNnSc6
hlavní	hlavní	k2eAgFnSc2d1
lodi	loď	k1gFnSc2
s	s	k7c7
transeptem	transept	k1gInSc7
<g/>
,	,	kIx,
založená	založený	k2eAgFnSc1d1
roku	rok	k1gInSc2
1790	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
kamenná	kamenný	k2eAgFnSc1d1
trojpatrová	trojpatrový	k2eAgFnSc1d1
kašna	kašna	k1gFnSc1
</s>
<s>
Náměstí	náměstí	k1gNnSc1
nezávislosti	nezávislost	k1gFnSc2
-	-	kIx~
Plaza	plaz	k1gMnSc2
Independencia	Independencius	k1gMnSc2
</s>
<s>
jezdecký	jezdecký	k2eAgInSc1d1
pomník	pomník	k1gInSc1
generála	generál	k1gMnSc2
José	José	k1gNnSc2
Artigase	Artigas	k1gInSc6
(	(	kIx(
<g/>
Mausoleo	Mausoleo	k1gMnSc1
del	del	k?
General	General	k1gMnSc1
Artigas	Artigas	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
palác	palác	k1gInSc1
Palacio	Palacio	k1gMnSc1
Estévez	Estévez	k1gMnSc1
z	z	k7c2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
</s>
<s>
palác	palác	k1gInSc1
Palacio	Palacio	k6eAd1
Salvo	salva	k1gFnSc5
<g/>
,	,	kIx,
první	první	k4xOgInSc4
mrakodrap	mrakodrap	k1gInSc4
z	z	k7c2
roku	rok	k1gInSc2
1925	#num#	k4
<g/>
,	,	kIx,
původně	původně	k6eAd1
hotel	hotel	k1gInSc1
<g/>
,	,	kIx,
nyní	nyní	k6eAd1
úřední	úřední	k2eAgFnSc1d1
a	a	k8xC
rezidenční	rezidenční	k2eAgFnSc1d1
budova	budova	k1gFnSc1
<g/>
,	,	kIx,
100	#num#	k4
metrů	metr	k1gInPc2
vysoká	vysoký	k2eAgFnSc1d1
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgFnSc1d3
budova	budova	k1gFnSc1
historického	historický	k2eAgNnSc2d1
centra	centrum	k1gNnSc2
</s>
<s>
Nejstarší	starý	k2eAgNnSc1d3
městské	městský	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
Teatro	Teatro	k1gNnSc1
Solís	Solísa	k1gFnPc2
(	(	kIx(
<g/>
z	z	k7c2
let	léto	k1gNnPc2
1840	#num#	k4
-	-	kIx~
1856	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
interiéry	interiér	k1gInPc1
jsou	být	k5eAaImIp3nP
přístupné	přístupný	k2eAgFnPc1d1
jako	jako	k8xC,k8xS
muzeum	muzeum	k1gNnSc1
</s>
<s>
Náměstí	náměstí	k1gNnSc1
generála	generál	k1gMnSc2
Zabaly	Zabala	k1gFnSc2
-	-	kIx~
Plaza	plaz	k1gMnSc2
Zabala	Zabal	k1gMnSc2
s	s	k7c7
jezdeckou	jezdecký	k2eAgFnSc7d1
sochou	socha	k1gFnSc7
generála	generál	k1gMnSc2
Zabaly	Zabala	k1gFnSc2
</s>
<s>
Historické	historický	k2eAgFnPc4d1
kavárny	kavárna	k1gFnPc4
</s>
<s>
Café	café	k1gNnSc1
Brasilero	Brasilero	k1gNnSc1
(	(	kIx(
<g/>
1877	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ul	ul	kA
<g/>
.	.	kIx.
Ituzaingó	Ituzaingó	k1gMnSc2
<g/>
,	,	kIx,
secesní	secesní	k2eAgInSc1d1
interiér	interiér	k1gInSc1
</s>
<s>
Café	café	k1gNnSc1
Roldos	Roldosa	k1gFnPc2
(	(	kIx(
<g/>
1886	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
uvnitř	uvnitř	k7c2
městské	městský	k2eAgFnSc2d1
tržnice	tržnice	k1gFnSc2
v	v	k7c6
přístavu	přístav	k1gInSc6
</s>
<s>
Café	café	k1gNnSc1
Bacacay	Bacacaa	k1gFnSc2
ve	v	k7c6
stejnojmenné	stejnojmenný	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
<g/>
,	,	kIx,
s	s	k7c7
barem	bar	k1gInSc7
El	Ela	k1gFnPc2
Vasquito	Vasquit	k2eAgNnSc4d1
</s>
<s>
Městská	městský	k2eAgFnSc1d1
tržnice	tržnice	k1gFnSc1
-	-	kIx~
Mercado	Mercada	k1gFnSc5
del	del	k?
puerto	puerta	k1gFnSc5
<g/>
,	,	kIx,
litinová	litinový	k2eAgFnSc1d1
konstrukce	konstrukce	k1gFnSc1
členité	členitý	k2eAgFnSc2d1
haly	hala	k1gFnSc2
se	s	k7c7
skleněnou	skleněný	k2eAgFnSc7d1
střechou	střecha	k1gFnSc7
a	a	k8xC
cihelnými	cihelný	k2eAgFnPc7d1
příčkami	příčka	k1gFnPc7
<g/>
,	,	kIx,
postavena	postaven	k2eAgFnSc1d1
v	v	k7c6
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
;	;	kIx,
na	na	k7c6
ose	osa	k1gFnSc6
hlavní	hlavní	k2eAgFnSc2d1
ulice	ulice	k1gFnSc2
z	z	k7c2
přístavu	přístav	k1gInSc2
</s>
<s>
Muzea	muzeum	k1gNnSc2
</s>
<s>
Museo	Museo	k6eAd1
de	de	k?
arte	artat	k5eAaPmIp3nS
precolombiano	precolombiana	k1gFnSc5
et	et	k?
indigena	indigena	k1gFnSc1
(	(	kIx(
<g/>
archeologické	archeologický	k2eAgInPc1d1
nálezy	nález	k1gInPc1
jižní	jižní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
z	z	k7c2
předkolumbovského	předkolumbovský	k2eAgNnSc2d1
období	období	k1gNnSc2
<g/>
,	,	kIx,
současné	současný	k2eAgFnSc2d1
národopisné	národopisný	k2eAgFnSc2d1
památky	památka	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Museo	Museo	k1gMnSc1
historico	historico	k6eAd1
nacional	nacionat	k5eAaPmAgMnS,k5eAaImAgMnS
de	de	k?
25	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mayo	Mayo	k1gNnSc1
-	-	kIx~
Historické	historický	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
národně	národně	k6eAd1
osvobozeneckého	osvobozenecký	k2eAgInSc2d1
boje	boj	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Museo	Museo	k6eAd1
Torrés	Torrés	k1gInSc1
García	Garcíum	k1gNnSc2
-	-	kIx~
soukromá	soukromý	k2eAgFnSc1d1
sbírka	sbírka	k1gFnSc1
<g/>
,	,	kIx,
především	především	k6eAd1
obrazů	obraz	k1gInPc2
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
ikon	ikon	k1gInSc1
<g/>
,	,	kIx,
tak	tak	k6eAd1
moderní	moderní	k2eAgFnSc2d1
malby	malba	k1gFnSc2
Pablo	Pablo	k1gNnSc4
Picassovy	Picassův	k2eAgFnSc2d1
a	a	k8xC
Braqueovy	Braqueův	k2eAgFnSc2d1
</s>
<s>
Museo	Museo	k1gMnSc1
naval	navalit	k5eAaPmRp2nS
-	-	kIx~
Námořní	námořní	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
</s>
<s>
Museo	Museo	k1gMnSc1
del	del	k?
Gaucho	Gaucha	k1gFnSc5
-	-	kIx~
muzeum	muzeum	k1gNnSc1
obyvatelstva	obyvatelstvo	k1gNnSc2
komunity	komunita	k1gFnSc2
gaučů	gaučo	k1gMnPc2
</s>
<s>
Museo	Museo	k6eAd1
del	del	k?
Carnaval	Carnaval	k1gFnSc1
-	-	kIx~
muzeum	muzeum	k1gNnSc1
karnevalu	karneval	k1gInSc2
</s>
<s>
Sport	sport	k1gInSc1
</s>
<s>
Fotbal	fotbal	k1gInSc1
</s>
<s>
V	v	k7c6
Montevideu	Montevideo	k1gNnSc6
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1930	#num#	k4
odehrálo	odehrát	k5eAaPmAgNnS
celé	celý	k2eAgNnSc4d1
první	první	k4xOgNnSc4
mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
tento	tento	k3xDgInSc4
účel	účel	k1gInSc4
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgInS
národní	národní	k2eAgInSc1d1
stadion	stadion	k1gInSc1
Estadio	Estadio	k6eAd1
Centenario	Centenario	k6eAd1
pojmenovaný	pojmenovaný	k2eAgMnSc1d1
podle	podle	k7c2
100	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc4
první	první	k4xOgFnSc2
ústavy	ústava	k1gFnSc2
Uruguaye	Uruguay	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Montevideu	Montevideo	k1gNnSc6
sídlí	sídlet	k5eAaImIp3nS
2	#num#	k4
nejslavnější	slavný	k2eAgInPc4d3
uruguayské	uruguayský	k2eAgInPc4d1
fotbalové	fotbalový	k2eAgInPc4d1
kluby	klub	k1gInPc4
<g/>
,	,	kIx,
Peñ	Peñ	k1gInSc4
a	a	k8xC
Nacional	Nacional	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgInSc4
z	z	k7c2
nich	on	k3xPp3gInPc2
vyhrál	vyhrát	k5eAaPmAgMnS
3	#num#	k4
<g/>
×	×	k?
Interkontinentální	interkontinentální	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Jachting	jachting	k1gInSc1
</s>
<s>
Patří	patřit	k5eAaImIp3nP
k	k	k7c3
nejoblíběnějším	oblíběný	k2eAgInPc3d3
sportům	sport	k1gInPc3
v	v	k7c6
Uruguayi	Uruguay	k1gFnSc6
a	a	k8xC
v	v	k7c6
Montevideou	Montevidea	k1gFnSc7
si	se	k3xPyFc3
lze	lze	k6eAd1
najmout	najmout	k5eAaPmF
samostatnou	samostatný	k2eAgFnSc4d1
jachtu	jachta	k1gFnSc4
na	na	k7c4
krátkou	krátký	k2eAgFnSc4d1
vyjížďku	vyjížďka	k1gFnSc4
či	či	k8xC
na	na	k7c4
celou	celý	k2eAgFnSc4d1
dovolenou	dovolená	k1gFnSc4
<g/>
,	,	kIx,
s	s	k7c7
kapitánem	kapitán	k1gMnSc7
či	či	k8xC
bez	bez	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Koňské	koňský	k2eAgNnSc1d1
polo	polo	k6eAd1
</s>
<s>
Oblíbený	oblíbený	k2eAgInSc1d1
sport	sport	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
zavedli	zavést	k5eAaPmAgMnP
počátkem	počátkem	k7c2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
britští	britský	k2eAgMnPc1d1
přistěhovalci	přistěhovalec	k1gMnPc1
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
vlastní	vlastní	k2eAgInSc1d1
stadion	stadion	k1gInSc1
a	a	k8xC
zdejší	zdejší	k2eAgNnPc1d1
mužstva	mužstvo	k1gNnPc1
hrají	hrát	k5eAaImIp3nP
uruguayskou	uruguayský	k2eAgFnSc4d1
ligu	liga	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Partnerská	partnerský	k2eAgNnPc1d1
města	město	k1gNnPc1
</s>
<s>
Barcelona	Barcelona	k1gFnSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Berlín	Berlín	k1gInSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
</s>
<s>
Bogotá	Bogotat	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
,	,	kIx,
Kolumbie	Kolumbie	k1gFnSc1
</s>
<s>
Buenos	Buenos	k1gMnSc1
Aires	Aires	k1gMnSc1
<g/>
,	,	kIx,
Argentina	Argentina	k1gFnSc1
</s>
<s>
Cádiz	Cádiz	k1gInSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Córdoba	Córdoba	k1gFnSc1
<g/>
,	,	kIx,
Argentina	Argentina	k1gFnSc1
</s>
<s>
Curitiba	Curitiba	k1gFnSc1
<g/>
,	,	kIx,
Brazílie	Brazílie	k1gFnSc1
</s>
<s>
Porto	porto	k1gNnSc1
Alegre	Alegr	k1gInSc5
<g/>
,	,	kIx,
Brazílie	Brazílie	k1gFnSc2
</s>
<s>
La	la	k1gNnSc1
Plata	plato	k1gNnSc2
<g/>
,	,	kIx,
Argentina	Argentina	k1gFnSc1
</s>
<s>
Madrid	Madrid	k1gInSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Melilla	Melilla	k1gFnSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Montevideo	Montevideo	k1gNnSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
<s>
Čching-tao	Čching-tao	k1gNnSc1
<g/>
,	,	kIx,
Čína	Čína	k1gFnSc1
</s>
<s>
Québec	Québec	k1gInSc1
<g/>
,	,	kIx,
Kanada	Kanada	k1gFnSc1
</s>
<s>
Rosario	Rosario	k1gNnSc1
<g/>
,	,	kIx,
Argentina	Argentina	k1gFnSc1
</s>
<s>
Petrohrad	Petrohrad	k1gInSc1
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
</s>
<s>
Tchien-ťin	Tchien-ťin	k1gInSc1
<g/>
,	,	kIx,
Čína	Čína	k1gFnSc1
</s>
<s>
Wellington	Wellington	k1gInSc1
<g/>
,	,	kIx,
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Instituto	Institut	k2eAgNnSc1d1
Nacional	Nacional	k1gMnPc7
de	de	k?
Estadística	Estadístic	k1gInSc2
<g/>
.	.	kIx.
<g/>
↑	↑	k?
http://www.maritimequest.com/warship_directory/germany/pages/cruisers/admiral_graf_spee.htm	http://www.maritimequest.com/warship_directory/germany/pages/cruisers/admiral_graf_spee.htm	k1gInSc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Irving	Irving	k1gInSc1
<g/>
,	,	kIx,
Washington	Washington	k1gInSc1
-	-	kIx~
Malý	Malý	k1gMnSc1
<g/>
,	,	kIx,
Jakub	Jakub	k1gMnSc1
-	-	kIx~
Prescott	Prescott	k1gInSc1
<g/>
,	,	kIx,
William	William	k1gInSc1
<g/>
,	,	kIx,
Hickling	Hickling	k1gInSc1
ː	ː	k?
Amerika	Amerika	k1gFnSc1
od	od	k7c2
času	čas	k1gInSc2
svého	svůj	k3xOyFgNnSc2
odkrytí	odkrytí	k1gNnSc2
až	až	k9
na	na	k7c4
nejnovější	nový	k2eAgFnSc4d3
dobu	doba	k1gFnSc4
<g/>
,	,	kIx,
díl	díl	k1gInSc4
VI	VI	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Dějepis	dějepis	k1gInSc1
emancipace	emancipace	k1gFnSc2
Ameriky	Amerika	k1gFnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1857	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
334	#num#	k4
<g/>
-	-	kIx~
<g/>
336	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Josef	Josef	k1gMnSc1
Janáčekː	Janáčekː	k1gMnSc2
Století	století	k1gNnSc2
zámořských	zámořský	k2eAgInPc2d1
objevů	objev	k1gInPc2
(	(	kIx(
<g/>
1415	#num#	k4
-1522	-1522	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
1959	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Aleš	Aleš	k1gMnSc1
SKŘIVAN	Skřivan	k1gMnSc1
-	-	kIx~
Petr	Petr	k1gMnSc1
KŘIVSKÝː	KŘIVSKÝː	k1gMnSc2
Moře	moře	k1gNnSc2
<g/>
,	,	kIx,
objevy	objev	k1gInPc1
<g/>
,	,	kIx,
staletí	staletí	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
1980	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Montevideo	Montevideo	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnPc1
Montevideo	Montevideo	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
https://www.lonelyplanet.com/uruguay/montevideo	https://www.lonelyplanet.com/uruguay/montevideo	k6eAd1
</s>
<s>
http://www.montevideo.gub.uy	http://www.montevideo.gub.u	k2eAgFnPc4d1
-	-	kIx~
oficiální	oficiální	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
města	město	k1gNnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Hlavní	hlavní	k2eAgNnPc1d1
města	město	k1gNnPc1
Jižní	jižní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
Nezávislé	závislý	k2eNgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
Asunción	Asunción	k1gInSc1
(	(	kIx(
<g/>
Paraguay	Paraguay	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Bogotá	Bogotá	k1gFnSc1
(	(	kIx(
<g/>
Kolumbie	Kolumbie	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Brasília	Brasília	k1gFnSc1
(	(	kIx(
<g/>
Brazílie	Brazílie	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Buenos	Buenos	k1gMnSc1
Aires	Aires	k1gMnSc1
(	(	kIx(
<g/>
Argentina	Argentina	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Caracas	Caracas	k1gInSc1
(	(	kIx(
<g/>
Venezuela	Venezuela	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Georgetown	Georgetown	k1gInSc1
(	(	kIx(
<g/>
Guyana	Guyana	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Lima	Lima	k1gFnSc1
(	(	kIx(
<g/>
Peru	Peru	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Montevideo	Montevideo	k1gNnSc1
(	(	kIx(
<g/>
Uruguay	Uruguay	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Paramaribo	Paramariba	k1gFnSc5
(	(	kIx(
<g/>
Surinam	Surinam	k1gInSc4
<g/>
)	)	kIx)
•	•	k?
Quito	Quito	k1gNnSc1
(	(	kIx(
<g/>
Ekvádor	Ekvádor	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Santiago	Santiago	k1gNnSc4
de	de	k?
Chile	Chile	k1gNnSc2
(	(	kIx(
<g/>
Chile	Chile	k1gNnSc2
<g/>
)	)	kIx)
•	•	k?
Sucre	Sucr	k1gInSc5
(	(	kIx(
<g/>
Bolívie	Bolívie	k1gFnSc2
<g/>
)	)	kIx)
Teritoria	teritorium	k1gNnSc2
<g/>
,	,	kIx,
kolonie	kolonie	k1gFnSc2
azámořská	azámořský	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Cayenne	Cayennout	k5eAaImIp3nS,k5eAaPmIp3nS
(	(	kIx(
<g/>
Francouzská	francouzský	k2eAgFnSc1d1
Guyana	Guyana	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Port	port	k1gInSc1
Stanley	Stanlea	k1gFnSc2
(	(	kIx(
<g/>
Falklandy	Falklanda	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
|	|	kIx~
Latinská	latinský	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
34856	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4115214-1	4115214-1	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79089276	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
144235117	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79089276	#num#	k4
</s>
