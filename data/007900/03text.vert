<s>
Ivana	Ivana	k1gFnSc1	Ivana
je	být	k5eAaImIp3nS	být
ženské	ženský	k2eAgNnSc4d1	ženské
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
českého	český	k2eAgInSc2d1	český
kalendáře	kalendář	k1gInSc2	kalendář
má	mít	k5eAaImIp3nS	mít
svátek	svátek	k1gInSc1	svátek
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
<s>
Ivana	Ivana	k1gFnSc1	Ivana
je	být	k5eAaImIp3nS	být
ženským	ženský	k2eAgInSc7d1	ženský
protějškem	protějšek	k1gInSc7	protějšek
jména	jméno	k1gNnSc2	jméno
Ivan	Ivana	k1gFnPc2	Ivana
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
staroslovanskou	staroslovanský	k2eAgFnSc7d1	staroslovanská
variantou	varianta	k1gFnSc7	varianta
jména	jméno	k1gNnSc2	jméno
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Ivana	Ivana	k1gFnSc1	Ivana
proto	proto	k8xC	proto
tvoří	tvořit	k5eAaImIp3nS	tvořit
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
obdob	obdoba	k1gFnPc2	obdoba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
staletí	staletí	k1gNnSc6	staletí
vytvářely	vytvářet	k5eAaImAgInP	vytvářet
na	na	k7c6	na
základě	základ	k1gInSc6	základ
prastarého	prastarý	k2eAgNnSc2d1	prastaré
jména	jméno	k1gNnSc2	jméno
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
jde	jít	k5eAaImIp3nS	jít
ovšem	ovšem	k9	ovšem
o	o	k7c4	o
obdobu	obdoba	k1gFnSc4	obdoba
zcela	zcela	k6eAd1	zcela
moderní	moderní	k2eAgFnSc4d1	moderní
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Ivana	Ivana	k1gFnSc1	Ivana
přišla	přijít	k5eAaPmAgFnS	přijít
do	do	k7c2	do
módy	móda	k1gFnSc2	móda
zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
zkrácené	zkrácený	k2eAgFnSc6d1	zkrácená
formě	forma	k1gFnSc6	forma
Iva	Ivo	k1gMnSc2	Ivo
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
se	se	k3xPyFc4	se
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
Ivankami	Ivanka	k1gFnPc7	Ivanka
<g/>
,	,	kIx,	,
ženským	ženský	k2eAgNnSc7d1	ženské
jménem	jméno	k1gNnSc7	jméno
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
Česka	Česko	k1gNnSc2	Česko
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
začalo	začít	k5eAaPmAgNnS	začít
šířit	šířit	k5eAaImF	šířit
ze	z	k7c2	z
zemí	zem	k1gFnPc2	zem
jižních	jižní	k2eAgInPc2d1	jižní
Slovanů	Slovan	k1gInPc2	Slovan
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
uvádí	uvádět	k5eAaImIp3nS	uvádět
četnost	četnost	k1gFnSc4	četnost
jména	jméno	k1gNnSc2	jméno
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
pořadí	pořadí	k1gNnSc2	pořadí
mezi	mezi	k7c7	mezi
ženskými	ženský	k2eAgNnPc7d1	ženské
jmény	jméno	k1gNnPc7	jméno
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
dvou	dva	k4xCgInPc2	dva
roků	rok	k1gInPc2	rok
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yRgInPc4	který
jsou	být	k5eAaImIp3nP	být
dostupné	dostupný	k2eAgInPc1d1	dostupný
údaje	údaj	k1gInPc1	údaj
MV	MV	kA	MV
ČR	ČR	kA	ČR
–	–	k?	–
lze	lze	k6eAd1	lze
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
tedy	tedy	k9	tedy
vysledovat	vysledovat	k5eAaImF	vysledovat
trend	trend	k1gInSc4	trend
v	v	k7c6	v
užívání	užívání	k1gNnSc6	užívání
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
:	:	kIx,	:
Změna	změna	k1gFnSc1	změna
procentního	procentní	k2eAgNnSc2d1	procentní
zastoupení	zastoupení	k1gNnSc2	zastoupení
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
mezi	mezi	k7c7	mezi
žijícími	žijící	k2eAgFnPc7d1	žijící
ženami	žena	k1gFnPc7	žena
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
procentní	procentní	k2eAgFnSc1d1	procentní
změna	změna	k1gFnSc1	změna
se	se	k3xPyFc4	se
započítáním	započítání	k1gNnSc7	započítání
celkového	celkový	k2eAgInSc2d1	celkový
úbytku	úbytek	k1gInSc2	úbytek
žen	žena	k1gFnPc2	žena
v	v	k7c6	v
ČR	ČR	kA	ČR
za	za	k7c4	za
sledované	sledovaný	k2eAgInPc4d1	sledovaný
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
+1,1	+1,1	k4	+1,1
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Ivana	Ivana	k1gFnSc1	Ivana
Andrlová	Andrlová	k1gFnSc1	Andrlová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
Ivana	Ivana	k1gFnSc1	Ivana
Čornejová	Čornejový	k2eAgFnSc1d1	Čornejová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
historička	historička	k1gFnSc1	historička
Ivanka	Ivanka	k1gFnSc1	Ivanka
Devátá	devátý	k4xOgFnSc1	devátý
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
Ivana	Ivana	k1gFnSc1	Ivana
Gottová	Gottová	k1gFnSc1	Gottová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
moderátorka	moderátorka	k1gFnSc1	moderátorka
Ivana	Ivana	k1gFnSc1	Ivana
Hušková	Hušková	k1gFnSc1	Hušková
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
Ivana	Ivana	k1gFnSc1	Ivana
Christová	Christová	k1gFnSc1	Christová
–	–	k?	–
slovenská	slovenský	k2eAgFnSc1d1	slovenská
modelka	modelka	k1gFnSc1	modelka
a	a	k8xC	a
Miss	miss	k1gFnSc1	miss
ČSSR	ČSSR	kA	ČSSR
Ivana	Ivana	k1gFnSc1	Ivana
Chýlková	Chýlková	k1gFnSc1	Chýlková
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
Ivana	Ivana	k1gFnSc1	Ivana
Jirešová	Jirešová	k1gFnSc1	Jirešová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
Ivana	Ivan	k1gMnSc2	Ivan
Kobilca	Kobilc	k1gInSc2	Kobilc
–	–	k?	–
slovinská	slovinský	k2eAgFnSc1d1	slovinská
malířka	malířka	k1gFnSc1	malířka
Ivana	Ivan	k1gMnSc2	Ivan
Milicevic	Milicevice	k1gFnPc2	Milicevice
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
chorvatská	chorvatský	k2eAgFnSc1d1	chorvatská
herečka	herečka	k1gFnSc1	herečka
Ivana	Ivana	k1gFnSc1	Ivana
Řápková	Řápková	k1gFnSc1	Řápková
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
politička	politička	k1gFnSc1	politička
Ivana	Ivana	k1gFnSc1	Ivana
Trumpová	Trumpová	k1gFnSc1	Trumpová
–	–	k?	–
americká	americký	k2eAgFnSc1d1	americká
podnikatelka	podnikatelka	k1gFnSc1	podnikatelka
a	a	k8xC	a
modelka	modelka	k1gFnSc1	modelka
Ivana	Ivan	k1gMnSc2	Ivan
Maria	Maria	k1gFnSc1	Maria
Vidović	Vidović	k1gFnSc1	Vidović
–	–	k?	–
chorvatská	chorvatský	k2eAgFnSc1d1	chorvatská
pianistka	pianistka	k1gFnSc1	pianistka
Ivana	Ivana	k1gFnSc1	Ivana
Korolová	Korolový	k2eAgFnSc1d1	Korolová
–	–	k?	–
herečka	herečka	k1gFnSc1	herečka
<g/>
,	,	kIx,	,
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Ivana	Ivana	k1gFnSc1	Ivana
Zemanová	Zemanová	k1gFnSc1	Zemanová
–	–	k?	–
manželka	manželka	k1gFnSc1	manželka
prezidenta	prezident	k1gMnSc2	prezident
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
Milan	Milan	k1gMnSc1	Milan
Ivana	Ivan	k1gMnSc2	Ivan
–	–	k?	–
slovenský	slovenský	k2eAgMnSc1d1	slovenský
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
útočník	útočník	k1gMnSc1	útočník
Seznam	seznam	k1gInSc4	seznam
článků	článek	k1gInPc2	článek
začínajících	začínající	k2eAgInPc2d1	začínající
na	na	k7c4	na
"	"	kIx"	"
<g/>
Ivana	Ivan	k1gMnSc4	Ivan
<g/>
"	"	kIx"	"
Seznam	seznam	k1gInSc1	seznam
článků	článek	k1gInPc2	článek
začínajících	začínající	k2eAgInPc2d1	začínající
na	na	k7c4	na
"	"	kIx"	"
<g/>
Ivanka	Ivanka	k1gFnSc1	Ivanka
<g/>
"	"	kIx"	"
</s>
