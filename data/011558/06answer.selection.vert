<s>
Leishmania	Leishmanium	k1gNnPc1	Leishmanium
major	major	k1gMnSc1	major
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
prvoka	prvok	k1gMnSc2	prvok
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
leismania	leismanium	k1gNnSc2	leismanium
<g/>
,	,	kIx,	,
způsobující	způsobující	k2eAgFnSc4d1	způsobující
leismaniózu	leismanióza	k1gFnSc4	leismanióza
(	(	kIx(	(
<g/>
také	také	k9	také
známou	známá	k1gFnSc4	známá
jako	jako	k8xS	jako
aleppské	aleppský	k2eAgInPc1d1	aleppský
vředy	vřed	k1gInPc1	vřed
<g/>
,	,	kIx,	,
bagdádské	bagdádský	k2eAgInPc1d1	bagdádský
vředy	vřed	k1gInPc1	vřed
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
lokální	lokální	k2eAgInPc1d1	lokální
názvy	název	k1gInPc1	název
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
