<p>
<s>
Leishmania	Leishmanium	k1gNnPc1	Leishmanium
major	major	k1gMnSc1	major
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
prvoka	prvok	k1gMnSc2	prvok
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
leismania	leismanium	k1gNnSc2	leismanium
<g/>
,	,	kIx,	,
způsobující	způsobující	k2eAgFnSc4d1	způsobující
leismaniózu	leismanióza	k1gFnSc4	leismanióza
(	(	kIx(	(
<g/>
také	také	k9	také
známou	známá	k1gFnSc4	známá
jako	jako	k8xS	jako
aleppské	aleppský	k2eAgInPc1d1	aleppský
vředy	vřed	k1gInPc1	vřed
<g/>
,	,	kIx,	,
bagdádské	bagdádský	k2eAgInPc1d1	bagdádský
vředy	vřed	k1gInPc1	vřed
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
lokální	lokální	k2eAgInPc1d1	lokální
názvy	název	k1gInPc1	název
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
L.	L.	kA	L.
major	major	k1gMnSc1	major
vnitrobuněčný	vnitrobuněčný	k2eAgInSc4d1	vnitrobuněčný
patogen	patogen	k1gInSc4	patogen
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
infikuje	infikovat	k5eAaBmIp3nS	infikovat
fagocyty	fagocyt	k1gInPc4	fagocyt
a	a	k8xC	a
dendritické	dendritický	k2eAgFnPc4d1	dendritická
buňky	buňka	k1gFnPc4	buňka
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
výhradně	výhradně	k6eAd1	výhradně
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
,	,	kIx,	,
nejvíce	hodně	k6eAd3	hodně
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
na	na	k7c6	na
středním	střední	k2eAgInSc6d1	střední
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
a	a	k8xC	a
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životní	životní	k2eAgInSc1d1	životní
cyklus	cyklus	k1gInSc1	cyklus
==	==	k?	==
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
většina	většina	k1gFnSc1	většina
trypanozóm	trypanozóma	k1gFnPc2	trypanozóma
<g/>
,	,	kIx,	,
L.	L.	kA	L.
major	major	k1gMnSc1	major
začíná	začínat	k5eAaImIp3nS	začínat
životní	životní	k2eAgInSc4d1	životní
cyklus	cyklus	k1gInSc4	cyklus
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
amastigota	amastigota	k1gFnSc1	amastigota
(	(	kIx(	(
<g/>
kulatý	kulatý	k2eAgInSc1d1	kulatý
s	s	k7c7	s
krátkým	krátký	k2eAgInSc7d1	krátký
bičíkem	bičík	k1gInSc7	bičík
<g/>
)	)	kIx)	)
uvnitř	uvnitř	k7c2	uvnitř
zažívacího	zažívací	k2eAgInSc2d1	zažívací
traktu	trakt	k1gInSc2	trakt
hlavního	hlavní	k2eAgInSc2d1	hlavní
přenašeče	přenašeč	k1gInSc2	přenašeč
<g/>
,	,	kIx,	,
samičce	samičko	k6eAd1	samičko
mouchy	moucha	k1gFnPc4	moucha
rodu	rod	k1gInSc2	rod
Phlebotomus	Phlebotomus	k1gInSc1	Phlebotomus
<g/>
.	.	kIx.	.
ve	v	k7c6	v
střevě	střevo	k1gNnSc6	střevo
přenašeče	přenašeč	k1gInSc2	přenašeč
se	se	k3xPyFc4	se
po	po	k7c6	po
1-2	[number]	k4	1-2
týdnech	týden	k1gInPc6	týden
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c4	v
promastigotní	promastigotní	k2eAgNnSc4d1	promastigotní
stádium	stádium	k1gNnSc4	stádium
(	(	kIx(	(
<g/>
protáhlý	protáhlý	k2eAgInSc1d1	protáhlý
s	s	k7c7	s
bičíkem	bičík	k1gInSc7	bičík
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
přesouvá	přesouvat	k5eAaImIp3nS	přesouvat
do	do	k7c2	do
sosáku	sosák	k1gInSc2	sosák
</s>
</p>
<p>
<s>
Během	během	k7c2	během
sání	sání	k1gNnSc2	sání
se	se	k3xPyFc4	se
promastigot	promastigot	k1gInSc1	promastigot
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
krevního	krevní	k2eAgInSc2d1	krevní
oběhu	oběh	k1gInSc2	oběh
savčího	savčí	k2eAgMnSc2d1	savčí
hostitele	hostitel	k1gMnSc2	hostitel
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
pohlcen	pohlcen	k2eAgInSc4d1	pohlcen
makrofágy	makrofág	k1gInPc4	makrofág
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pohlcení	pohlcení	k1gNnSc6	pohlcení
se	se	k3xPyFc4	se
promastigotní	promastigotní	k2eAgNnSc1d1	promastigotní
stádium	stádium	k1gNnSc1	stádium
vyvine	vyvinout	k5eAaPmIp3nS	vyvinout
ve	v	k7c4	v
stádium	stádium	k1gNnSc4	stádium
amastigotní	amastigotní	k2eAgNnSc4d1	amastigotní
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
oválné	oválný	k2eAgNnSc1d1	oválné
<g/>
,	,	kIx,	,
či	či	k8xC	či
kulovité	kulovitý	k2eAgNnSc4d1	kulovité
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
okolo	okolo	k7c2	okolo
2-3	[number]	k4	2-3
μ	μ	k?	μ
<g/>
,	,	kIx,	,
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
velké	velký	k2eAgNnSc4d1	velké
jádro	jádro	k1gNnSc4	jádro
a	a	k8xC	a
kinetoplast	kinetoplast	k1gInSc4	kinetoplast
<g/>
.	.	kIx.	.
</s>
<s>
Vybaveny	vybaven	k2eAgFnPc1d1	vybavena
k	k	k7c3	k
přežití	přežití	k1gNnSc3	přežití
acidické	acidický	k2eAgNnSc1d1	acidický
prostředí	prostředí	k1gNnSc1	prostředí
uvnitř	uvnitř	k7c2	uvnitř
potravních	potravní	k2eAgFnPc2d1	potravní
vakuol	vakuola	k1gFnPc2	vakuola
makrofága	makrofága	k1gFnSc1	makrofága
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
amastigotní	amastigotní	k2eAgNnPc1d1	amastigotní
stádia	stádium	k1gNnPc1	stádium
množí	množit	k5eAaImIp3nS	množit
binárním	binární	k2eAgNnSc7d1	binární
dělením	dělení	k1gNnSc7	dělení
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
amastigot	amastigot	k1gMnSc1	amastigot
rozmnožil	rozmnožit	k5eAaPmAgMnS	rozmnožit
a	a	k8xC	a
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
po	po	k7c6	po
těle	tělo	k1gNnSc6	tělo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
připraven	připravit	k5eAaPmNgMnS	připravit
k	k	k7c3	k
nasání	nasání	k1gNnSc3	nasání
mouchou	moucha	k1gFnSc7	moucha
společně	společně	k6eAd1	společně
s	s	k7c7	s
krví	krev	k1gFnSc7	krev
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
dokončí	dokončit	k5eAaPmIp3nS	dokončit
svůj	svůj	k3xOyFgInSc4	svůj
cyklus	cyklus	k1gInSc4	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
L.	L.	kA	L.
major	major	k1gMnSc1	major
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
sexuální	sexuální	k2eAgInSc1d1	sexuální
cyklus	cyklus	k1gInSc1	cyklus
s	s	k7c7	s
meiózou	meióza	k1gFnSc7	meióza
<g/>
,	,	kIx,	,
probíhající	probíhající	k2eAgFnSc7d1	probíhající
jen	jen	k6eAd1	jen
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
přenašeče	přenašeč	k1gInSc2	přenašeč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Infekce	infekce	k1gFnSc1	infekce
==	==	k?	==
</s>
</p>
<p>
<s>
L.	L.	kA	L.
major	major	k1gMnSc1	major
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
do	do	k7c2	do
krevního	krevní	k2eAgInSc2d1	krevní
oběhu	oběh	k1gInSc2	oběh
hostitele	hostitel	k1gMnSc2	hostitel
při	při	k7c6	při
sání	sání	k1gNnSc6	sání
hematofágní	hematofágní	k2eAgFnSc2d1	hematofágní
mouchy	moucha	k1gFnSc2	moucha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nalézá	nalézat	k5eAaImIp3nS	nalézat
cíl	cíl	k1gInSc4	cíl
nákazy	nákaza	k1gFnSc2	nákaza
<g/>
:	:	kIx,	:
makrofágy	makrofág	k1gInPc1	makrofág
<g/>
.	.	kIx.	.
</s>
<s>
Promastigotní	Promastigotní	k2eAgNnPc1d1	Promastigotní
stádia	stádium	k1gNnPc1	stádium
jsou	být	k5eAaImIp3nP	být
vybavena	vybavit	k5eAaPmNgNnP	vybavit
množstvím	množství	k1gNnSc7	množství
receptorů	receptor	k1gInPc2	receptor
k	k	k7c3	k
rozpoznání	rozpoznání	k1gNnSc3	rozpoznání
makrofága	makrofág	k1gMnSc2	makrofág
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
CR1	CR1	k1gFnSc2	CR1
a	a	k8xC	a
CR3	CR3	k1gFnSc2	CR3
receptorů	receptor	k1gInPc2	receptor
a	a	k8xC	a
RAGE	RAGE	kA	RAGE
receptoru	receptor	k1gInSc2	receptor
<g/>
.	.	kIx.	.
</s>
<s>
Aktivace	aktivace	k1gFnSc1	aktivace
k	k	k7c3	k
napadení	napadení	k1gNnSc3	napadení
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
cytoplazmatické	cytoplazmatický	k2eAgFnSc2d1	cytoplazmatická
membrány	membrána	k1gFnSc2	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
parazit	parazit	k1gMnSc1	parazit
přežije	přežít	k5eAaPmIp3nS	přežít
nepřátelské	přátelský	k2eNgNnSc4d1	nepřátelské
prostředí	prostředí	k1gNnSc4	prostředí
v	v	k7c6	v
hostitelském	hostitelský	k2eAgMnSc6d1	hostitelský
makrofágovi	makrofág	k1gMnSc6	makrofág
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Epidemiologie	epidemiologie	k1gFnSc2	epidemiologie
==	==	k?	==
</s>
</p>
<p>
<s>
Každoročně	každoročně	k6eAd1	každoročně
onemocní	onemocnět	k5eAaPmIp3nP	onemocnět
kožní	kožní	k2eAgFnSc7d1	kožní
leismaniózou	leismanióza	k1gFnSc7	leismanióza
kolem	kolem	k7c2	kolem
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
1,5	[number]	k4	1,5
miliónu	milión	k4xCgInSc2	milión
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
nákazy	nákaza	k1gFnSc2	nákaza
jsou	být	k5eAaImIp3nP	být
bodavé	bodavý	k2eAgFnPc4d1	bodavá
mouchy	moucha	k1gFnPc4	moucha
rodu	rod	k1gInSc2	rod
Phlebotomus	Phlebotomus	k1gInSc1	Phlebotomus
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
ovšem	ovšem	k9	ovšem
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
nedokáží	dokázat	k5eNaPmIp3nP	dokázat
uletět	uletět	k5eAaPmF	uletět
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
delší	dlouhý	k2eAgFnSc4d2	delší
než	než	k8xS	než
jeden	jeden	k4xCgInSc4	jeden
kilometr	kilometr	k1gInSc4	kilometr
<g/>
.	.	kIx.	.
<g/>
Hlavními	hlavní	k2eAgInPc7d1	hlavní
rezervoáry	rezervoár	k1gInPc7	rezervoár
nákazy	nákaza	k1gFnSc2	nákaza
jsou	být	k5eAaImIp3nP	být
menší	malý	k2eAgMnPc1d2	menší
hlodavci	hlodavec	k1gMnPc1	hlodavec
<g/>
,	,	kIx,	,
malí	malý	k2eAgMnPc1d1	malý
savci	savec	k1gMnPc1	savec
a	a	k8xC	a
ptáci	pták	k1gMnPc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
zdokumentované	zdokumentovaný	k2eAgInPc4d1	zdokumentovaný
případy	případ	k1gInPc4	případ
nákazy	nákaza	k1gFnSc2	nákaza
psů	pes	k1gMnPc2	pes
z	z	k7c2	z
Egypta	Egypt	k1gInSc2	Egypt
a	a	k8xC	a
Saudské	saudský	k2eAgFnSc2d1	Saudská
Arábie	Arábie	k1gFnSc2	Arábie
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
spíše	spíše	k9	spíše
vzácností	vzácnost	k1gFnSc7	vzácnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
L.	L.	kA	L.
major	major	k1gMnSc1	major
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
příbuzná	příbuzná	k1gFnSc1	příbuzná
<g/>
,	,	kIx,	,
L.	L.	kA	L.
tropica	tropic	k1gInSc2	tropic
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
příčinou	příčina	k1gFnSc7	příčina
leismaniózy	leismanióza	k1gFnSc2	leismanióza
na	na	k7c6	na
středním	střední	k2eAgInSc6d1	střední
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
africe	afrika	k1gFnSc6	afrika
<g/>
,	,	kIx,	,
a	a	k8xC	a
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
2002	[number]	k4	2002
a	a	k8xC	a
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
přes	přes	k7c4	přes
700	[number]	k4	700
případů	případ	k1gInPc2	případ
onemocnění	onemocnění	k1gNnSc2	onemocnění
personálu	personál	k1gInSc2	personál
vojenských	vojenský	k2eAgFnPc2d1	vojenská
složek	složka	k1gFnPc2	složka
USA	USA	kA	USA
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klinické	klinický	k2eAgInPc1d1	klinický
projevy	projev	k1gInPc1	projev
==	==	k?	==
</s>
</p>
<p>
<s>
Nástup	nástup	k1gInSc1	nástup
infekce	infekce	k1gFnSc2	infekce
většinou	většinou	k6eAd1	většinou
provází	provázet	k5eAaImIp3nS	provázet
výskyt	výskyt	k1gInSc1	výskyt
lézí	léze	k1gFnPc2	léze
v	v	k7c6	v
obslatech	obsle	k1gNnPc6	obsle
pobodání	pobodání	k1gNnSc6	pobodání
přenašečem	přenašeč	k1gMnSc7	přenašeč
<g/>
.	.	kIx.	.
</s>
<s>
Infekce	infekce	k1gFnSc1	infekce
je	být	k5eAaImIp3nS	být
akutní	akutní	k2eAgFnSc1d1	akutní
s	s	k7c7	s
průběhem	průběh	k1gInSc7	průběh
okolo	okolo	k7c2	okolo
3-6	[number]	k4	3-6
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yQnSc7	co
více	hodně	k6eAd2	hodně
fagocyty	fagocyt	k1gInPc1	fagocyt
pohlcují	pohlcovat	k5eAaImIp3nP	pohlcovat
promastigoty	promastigot	k1gInPc1	promastigot
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
více	hodně	k6eAd2	hodně
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
produkci	produkce	k1gFnSc3	produkce
amastigotních	amastigotní	k2eAgMnPc2d1	amastigotní
jedinců	jedinec	k1gMnPc2	jedinec
v	v	k7c6	v
hnisajících	hnisající	k2eAgFnPc6d1	hnisající
uzlinách	uzlina	k1gFnPc6	uzlina
na	na	k7c6	na
kůži	kůže	k1gFnSc6	kůže
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
fázi	fáze	k1gFnSc6	fáze
nelze	lze	k6eNd1	lze
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
o	o	k7c4	o
jaký	jaký	k3yQgInSc4	jaký
patogen	patogen	k1gInSc4	patogen
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
<g/>
.	.	kIx.	.
</s>
<s>
Nejběžněji	běžně	k6eAd3	běžně
mají	mít	k5eAaImIp3nP	mít
léze	léze	k1gFnPc4	léze
zvýšený	zvýšený	k2eAgInSc4d1	zvýšený
kontinuální	kontinuální	k2eAgInSc4d1	kontinuální
okraj	okraj	k1gInSc4	okraj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dal	dát	k5eAaPmAgInS	dát
přirovnat	přirovnat	k5eAaPmF	přirovnat
ke	k	k7c3	k
kůrce	kůrka	k1gFnSc3	kůrka
od	od	k7c2	od
pizzy	pizza	k1gFnSc2	pizza
<g/>
.	.	kIx.	.
<g/>
Biopsie	biopsie	k1gFnPc1	biopsie
těchto	tento	k3xDgFnPc2	tento
lézí	léze	k1gFnPc2	léze
obvykle	obvykle	k6eAd1	obvykle
ukáže	ukázat	k5eAaPmIp3nS	ukázat
množství	množství	k1gNnSc1	množství
makrofágů	makrofág	k1gInPc2	makrofág
obsahujících	obsahující	k2eAgFnPc2d1	obsahující
amastigóty	amastigóta	k1gFnSc2	amastigóta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diagnóza	diagnóza	k1gFnSc1	diagnóza
==	==	k?	==
</s>
</p>
<p>
<s>
L.	L.	kA	L.
major	major	k1gMnSc1	major
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
rozpoznána	rozpoznat	k5eAaPmNgFnS	rozpoznat
pomocí	pomocí	k7c2	pomocí
diferenciální	diferenciální	k2eAgFnSc2d1	diferenciální
diagnostiky	diagnostika	k1gFnSc2	diagnostika
-	-	kIx~	-
tedy	tedy	k9	tedy
na	na	k7c6	na
základě	základ	k1gInSc6	základ
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
určené	určený	k2eAgFnSc2d1	určená
díky	díky	k7c3	díky
známému	známý	k1gMnSc3	známý
areálu	areál	k1gInSc2	areál
rozšíření	rozšíření	k1gNnSc2	rozšíření
druhů	druh	k1gInPc2	druh
leismaniózy	leismanióza	k1gFnSc2	leismanióza
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
stejné	stejný	k2eAgFnPc1d1	stejná
léze	léze	k1gFnPc1	léze
jsou	být	k5eAaImIp3nP	být
příznakem	příznak	k1gInSc7	příznak
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgNnPc2d1	další
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
,	,	kIx,	,
za	za	k7c4	za
která	který	k3yRgNnPc4	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
leismanióza	leismanióza	k1gFnSc1	leismanióza
zaměněna	zaměněn	k2eAgFnSc1d1	zaměněna
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
retikuloendotelióza	retikuloendotelióza	k1gFnSc1	retikuloendotelióza
<g/>
,	,	kIx,	,
sporotrichóza	sporotrichóza	k1gFnSc1	sporotrichóza
<g/>
,	,	kIx,	,
lupus	lupus	k1gInSc1	lupus
vulgaris	vulgaris	k1gFnSc1	vulgaris
<g/>
,	,	kIx,	,
syfilis	syfilis	k1gFnSc1	syfilis
<g/>
,	,	kIx,	,
sarkoidóza	sarkoidóza	k1gFnSc1	sarkoidóza
a	a	k8xC	a
lepra	lepra	k1gFnSc1	lepra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejběžnější	běžný	k2eAgFnSc7d3	nejběžnější
metodou	metoda	k1gFnSc7	metoda
diagnózy	diagnóza	k1gFnSc2	diagnóza
leismaniózy	leismanióza	k1gFnSc2	leismanióza
je	být	k5eAaImIp3nS	být
identifikace	identifikace	k1gFnSc1	identifikace
amastigota	amastigota	k1gFnSc1	amastigota
pomocí	pomocí	k7c2	pomocí
Wrightova	Wrightův	k2eAgNnSc2d1	Wrightovo
barvení	barvení	k1gNnSc2	barvení
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
izolací	izolace	k1gFnSc7	izolace
parazita	parazit	k1gMnSc2	parazit
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Léčba	léčba	k1gFnSc1	léčba
==	==	k?	==
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
hostitelův	hostitelův	k2eAgInSc4d1	hostitelův
imunitní	imunitní	k2eAgInSc4d1	imunitní
systém	systém	k1gInSc4	systém
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
schopen	schopen	k2eAgInSc1d1	schopen
infekci	infekce	k1gFnSc4	infekce
po	po	k7c6	po
3-6	[number]	k4	3-6
měsících	měsíc	k1gInPc6	měsíc
potlačit	potlačit	k5eAaPmF	potlačit
<g/>
,	,	kIx,	,
spočívá	spočívat	k5eAaImIp3nS	spočívat
léčba	léčba	k1gFnSc1	léčba
v	v	k7c6	v
boji	boj	k1gInSc6	boj
s	s	k7c7	s
příznaky	příznak	k1gInPc7	příznak
<g/>
,	,	kIx,	,
snahou	snaha	k1gFnSc7	snaha
zmenšit	zmenšit	k5eAaPmF	zmenšit
škody	škoda	k1gFnPc4	škoda
a	a	k8xC	a
zabránit	zabránit	k5eAaPmF	zabránit
nekrózám	nekróza	k1gFnPc3	nekróza
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
různých	různý	k2eAgInPc2d1	různý
způsobů	způsob	k1gInPc2	způsob
léčby	léčba	k1gFnSc2	léčba
je	být	k5eAaImIp3nS	být
variabilní	variabilní	k2eAgInSc1d1	variabilní
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
efektivity	efektivita	k1gFnSc2	efektivita
týče	týkat	k5eAaImIp3nS	týkat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklady	příklad	k1gInPc1	příklad
léčby	léčba	k1gFnSc2	léčba
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Fluconazol	Fluconazol	k1gInSc1	Fluconazol
podaný	podaný	k2eAgInSc1d1	podaný
v	v	k7c6	v
200	[number]	k4	200
mg	mg	kA	mg
dávce	dávka	k1gFnSc3	dávka
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
6	[number]	k4	6
týdnů	týden	k1gInPc2	týden
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
90	[number]	k4	90
%	%	kIx~	%
vyléčených	vyléčený	k2eAgNnPc2d1	vyléčené
oproti	oproti	k7c3	oproti
60	[number]	k4	60
%	%	kIx~	%
vyléčeným	vyléčený	k2eAgInSc7d1	vyléčený
<g/>
,	,	kIx,	,
kterým	který	k3yQgFnPc3	který
bylo	být	k5eAaImAgNnS	být
podáno	podat	k5eAaPmNgNnS	podat
placebo	placebo	k1gNnSc1	placebo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Topicalní	Topicalný	k2eAgMnPc1d1	Topicalný
aplikace	aplikace	k1gFnSc2	aplikace
15	[number]	k4	15
%	%	kIx~	%
paromomycinu	paromomycin	k1gInSc2	paromomycin
a	a	k8xC	a
12	[number]	k4	12
%	%	kIx~	%
methylbenzethonimu	methylbenzethonimat	k5eAaPmIp1nS	methylbenzethonimat
se	se	k3xPyFc4	se
již	již	k6eAd1	již
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
úspěšně	úspěšně	k6eAd1	úspěšně
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
léčbě	léčba	k1gFnSc3	léčba
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Intralézní	Intralézní	k2eAgFnPc1d1	Intralézní
injekce	injekce	k1gFnPc1	injekce
antimonu	antimon	k1gInSc2	antimon
se	se	k3xPyFc4	se
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
být	být	k5eAaImF	být
efektivní	efektivní	k2eAgFnSc1d1	efektivní
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
poddána	poddán	k2eAgFnSc1d1	poddána
okolo	okolo	k7c2	okolo
okraje	okraj	k1gInSc2	okraj
lézí	léze	k1gFnPc2	léze
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
podání	podání	k1gNnSc6	podání
10	[number]	k4	10
injekcí	injekce	k1gFnPc2	injekce
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
zaznamenáno	zaznamenán	k2eAgNnSc1d1	zaznamenáno
85	[number]	k4	85
%	%	kIx~	%
vyléčených	vyléčený	k2eAgMnPc2d1	vyléčený
během	během	k7c2	během
3	[number]	k4	3
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Prevence	prevence	k1gFnSc1	prevence
==	==	k?	==
</s>
</p>
<p>
<s>
Nejlepší	dobrý	k2eAgFnSc7d3	nejlepší
cestou	cesta	k1gFnSc7	cesta
jak	jak	k8xS	jak
bojovat	bojovat	k5eAaImF	bojovat
s	s	k7c7	s
šířením	šíření	k1gNnSc7	šíření
leismaniózy	leismanióza	k1gFnSc2	leismanióza
je	být	k5eAaImIp3nS	být
narušení	narušení	k1gNnSc1	narušení
životního	životní	k2eAgInSc2d1	životní
cyklu	cyklus	k1gInSc2	cyklus
vektorů	vektor	k1gInPc2	vektor
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
přenašečů	přenašeč	k1gMnPc2	přenašeč
nákazy	nákaza	k1gFnSc2	nákaza
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
odstraněním	odstranění	k1gNnSc7	odstranění
rezervoárů	rezervoár	k1gInPc2	rezervoár
nákazy	nákaza	k1gFnSc2	nákaza
<g/>
.	.	kIx.	.
</s>
<s>
Vyhýbání	vyhýbání	k1gNnSc2	vyhýbání
se	se	k3xPyFc4	se
přenašečům	přenašeč	k1gMnPc3	přenašeč
pomocí	pomocí	k7c2	pomocí
repelentů	repelent	k1gInPc2	repelent
<g/>
,	,	kIx,	,
moskytiér	moskytiéra	k1gFnPc2	moskytiéra
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
či	či	k8xC	či
vyhýbání	vyhýbání	k1gNnSc2	vyhýbání
se	se	k3xPyFc4	se
době	doba	k1gFnSc3	doba
aktivity	aktivita	k1gFnSc2	aktivita
přenášejících	přenášející	k2eAgFnPc2d1	přenášející
much	moucha	k1gFnPc2	moucha
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
od	od	k7c2	od
soumraku	soumrak	k1gInSc2	soumrak
do	do	k7c2	do
svítání	svítání	k1gNnSc2	svítání
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
efektivní	efektivní	k2eAgFnSc1d1	efektivní
v	v	k7c6	v
případě	případ	k1gInSc6	případ
krátkodobých	krátkodobý	k2eAgInPc2d1	krátkodobý
pobytů	pobyt	k1gInPc2	pobyt
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
výskytu	výskyt	k1gInSc2	výskyt
L.	L.	kA	L.
major	major	k1gMnSc1	major
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Leishmania	Leishmanium	k1gNnSc2	Leishmanium
major	major	k1gMnSc1	major
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
