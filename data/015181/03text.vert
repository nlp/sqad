<s>
Vilém	Vilém	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britský	britský	k2eAgInSc1d1
</s>
<s>
Vilém	Vilém	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s>
král	král	k1gMnSc1
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Irska	Irsko	k1gNnSc2
<g/>
,	,	kIx,
Hannoverska	Hannoversko	k1gNnSc2
</s>
<s>
Vilém	Vilém	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s>
Doba	doba	k1gFnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
1830	#num#	k4
–	–	k?
20	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
1837	#num#	k4
</s>
<s>
Korunovace	korunovace	k1gFnSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1831	#num#	k4
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
srpen	srpen	k1gInSc1
1765	#num#	k4
</s>
<s>
Buckinghamský	buckinghamský	k2eAgInSc1d1
palác	palác	k1gInSc1
<g/>
,	,	kIx,
Londýn	Londýn	k1gInSc1
</s>
<s>
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
1837	#num#	k4
</s>
<s>
Windsorský	windsorský	k2eAgInSc1d1
hrad	hrad	k1gInSc1
</s>
<s>
Pohřben	pohřben	k2eAgInSc1d1
</s>
<s>
kaple	kaple	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Jiří	Jiří	k1gMnSc2
na	na	k7c6
Windsorském	windsorský	k2eAgInSc6d1
hradu	hrad	k1gInSc6
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s>
Nástupce	nástupce	k1gMnSc1
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
:	:	kIx,
<g/>
ViktorieHannoversko	ViktorieHannoversko	k1gNnSc1
<g/>
:	:	kIx,
<g/>
Arnošt	Arnošt	k1gMnSc1
August	August	k1gMnSc1
I.	I.	kA
</s>
<s>
Královna	královna	k1gFnSc1
</s>
<s>
Adelheid	Adelheid	k1gInSc1
von	von	k1gInSc1
Sachsen-Meiningen	Sachsen-Meiningen	k1gInSc4
</s>
<s>
Potomci	potomek	k1gMnPc1
</s>
<s>
KarlaAlžběta	KarlaAlžběta	k1gFnSc1
</s>
<s>
Rod	rod	k1gInSc1
</s>
<s>
Welfové	Welf	k1gMnPc1
</s>
<s>
Dynastie	dynastie	k1gFnSc1
</s>
<s>
Hannoverská	hannoverský	k2eAgFnSc1d1
dynastie	dynastie	k1gFnSc1
</s>
<s>
Otec	otec	k1gMnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Matka	matka	k1gFnSc1
</s>
<s>
Šarlota	Šarlota	k1gFnSc1
von	von	k1gInSc1
Mecklenburg-Strelitz	Mecklenburg-Strelitz	k1gMnSc1
</s>
<s>
Podpis	podpis	k1gInSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vilém	Vilém	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britský	britský	k2eAgInSc1d1
</s>
<s>
Vilém	Vilém	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
21	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1765	#num#	k4
–	–	k?
20	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1837	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
panovník	panovník	k1gMnSc1
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Irska	Irsko	k1gNnSc2
a	a	k8xC
Hannoveru	Hannover	k1gInSc2
od	od	k7c2
29	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1830	#num#	k4
do	do	k7c2
své	svůj	k3xOyFgFnSc2
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
třetím	třetí	k4xOgMnSc7
synem	syn	k1gMnSc7
Jiřího	Jiří	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
a	a	k8xC
mladším	mladý	k2eAgMnSc7d2
bratrem	bratr	k1gMnSc7
a	a	k8xC
následníkem	následník	k1gMnSc7
Jiřího	Jiří	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
posledním	poslední	k2eAgMnSc7d1
mužským	mužský	k2eAgMnSc7d1
panovníkem	panovník	k1gMnSc7
z	z	k7c2
Hannoverské	hannoverský	k2eAgFnSc2d1
dynastie	dynastie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
mládí	mládí	k1gNnSc6
sloužil	sloužit	k5eAaImAgMnS
u	u	k7c2
královského	královský	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
a	a	k8xC
byl	být	k5eAaImAgMnS
přezdíván	přezdívat	k5eAaImNgMnS
námořním	námořní	k2eAgMnSc7d1
králem	král	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnPc1
dva	dva	k4xCgMnPc1
starší	starý	k2eAgMnPc1d2
bratři	bratr	k1gMnPc1
zemřeli	zemřít	k5eAaPmAgMnP
bez	bez	k7c2
legitimních	legitimní	k2eAgMnPc2d1
potomků	potomek	k1gMnPc2
a	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
Vilém	Vilém	k1gMnSc1
stal	stát	k5eAaPmAgMnS
ve	v	k7c6
věku	věk	k1gInSc6
64	#num#	k4
let	léto	k1gNnPc2
nástupcem	nástupce	k1gMnSc7
na	na	k7c6
britském	britský	k2eAgInSc6d1
trůnu	trůn	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
jeho	jeho	k3xOp3gFnSc2
vlády	vláda	k1gFnSc2
byly	být	k5eAaImAgFnP
uskutečněny	uskutečnit	k5eAaPmNgFnP
některé	některý	k3yIgFnPc1
reformy	reforma	k1gFnPc1
–	–	k?
úprava	úprava	k1gFnSc1
sociálních	sociální	k2eAgInPc2d1
zákonů	zákon	k1gInPc2
<g/>
,	,	kIx,
zákaz	zákaz	k1gInSc1
práce	práce	k1gFnSc2
dětí	dítě	k1gFnPc2
<g/>
,	,	kIx,
zákaz	zákaz	k1gInSc1
otrokářství	otrokářství	k1gNnSc2
platící	platící	k2eAgInSc1d1
v	v	k7c4
téměř	téměř	k6eAd1
celém	celý	k2eAgNnSc6d1
britském	britský	k2eAgNnSc6d1
impériu	impérium	k1gNnSc6
a	a	k8xC
úprava	úprava	k1gFnSc1
volebního	volební	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
se	se	k3xPyFc4
nezapojoval	zapojovat	k5eNaImAgMnS
do	do	k7c2
politiky	politika	k1gFnSc2
tak	tak	k6eAd1
výrazně	výrazně	k6eAd1
jako	jako	k8xC,k8xS
jeho	jeho	k3xOp3gMnSc1
bratr	bratr	k1gMnSc1
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
posledním	poslední	k2eAgMnSc7d1
britským	britský	k2eAgMnSc7d1
monarchou	monarcha	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
jmenoval	jmenovat	k5eAaBmAgMnS,k5eAaImAgMnS
předsedu	předseda	k1gMnSc4
vlády	vláda	k1gFnSc2
v	v	k7c6
rozporu	rozpor	k1gInSc6
s	s	k7c7
přáním	přání	k1gNnSc7
parlamentu	parlament	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Protože	protože	k8xS
v	v	k7c6
době	doba	k1gFnSc6
své	svůj	k3xOyFgFnSc2
smrti	smrt	k1gFnSc2
nezůstavil	zůstavit	k5eNaPmAgMnS
žádné	žádný	k3yNgMnPc4
žijící	žijící	k2eAgMnPc4d1
legitimní	legitimní	k2eAgMnPc4d1
potomky	potomek	k1gMnPc4
<g/>
,	,	kIx,
stala	stát	k5eAaPmAgFnS
se	se	k3xPyFc4
jeho	jeho	k3xOp3gFnSc7
následnicí	následnice	k1gFnSc7
na	na	k7c6
britském	britský	k2eAgInSc6d1
trůnu	trůn	k1gInSc6
jeho	jeho	k3xOp3gFnSc4
neteř	neteř	k1gFnSc4
princezna	princezna	k1gFnSc1
Viktorie	Viktorie	k1gFnSc1
z	z	k7c2
Kentu	Kent	k1gInSc2
<g/>
,	,	kIx,
jediná	jediný	k2eAgFnSc1d1
dcera	dcera	k1gFnSc1
Vilémova	Vilémův	k2eAgMnSc2d1
bratra	bratr	k1gMnSc2
prince	princ	k1gMnSc2
Eduarda	Eduard	k1gMnSc2
Augusta	August	k1gMnSc2
<g/>
,	,	kIx,
vévody	vévoda	k1gMnSc2
z	z	k7c2
Kentu	Kent	k1gInSc2
a	a	k8xC
Strathearnu	Strathearn	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Hannoverském	hannoverský	k2eAgNnSc6d1
království	království	k1gNnSc6
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
akceptovalo	akceptovat	k5eAaBmAgNnS
pouze	pouze	k6eAd1
mužské	mužský	k2eAgMnPc4d1
následníky	následník	k1gMnPc4
<g/>
,	,	kIx,
pak	pak	k6eAd1
na	na	k7c4
trůn	trůn	k1gInSc4
nastoupil	nastoupit	k5eAaPmAgMnS
Arnošt	Arnošt	k1gMnSc1
August	August	k1gMnSc1
I.	I.	kA
</s>
<s>
Mládí	mládí	k1gNnSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	s	k7c7
21	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1765	#num#	k4
v	v	k7c6
Buckinghamském	buckinghamský	k2eAgInSc6d1
paláci	palác	k1gInSc6
jako	jako	k9
třetí	třetí	k4xOgMnSc1
syn	syn	k1gMnSc1
Jiřího	Jiří	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
a	a	k8xC
jeho	jeho	k3xOp3gFnPc1
manželky	manželka	k1gFnPc1
královny	královna	k1gFnSc2
Šarloty	Šarlota	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měl	mít	k5eAaImAgMnS
dva	dva	k4xCgMnPc4
starší	starší	k1gMnPc4
bratry	bratr	k1gMnPc7
Jiřího	Jiří	k1gMnSc2
a	a	k8xC
Frederika	Frederik	k1gMnSc2
a	a	k8xC
nepředpokládalo	předpokládat	k5eNaImAgNnS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
kdy	kdy	k6eAd1
usedl	usednout	k5eAaPmAgMnS
na	na	k7c4
trůn	trůn	k1gInSc4
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Jako	jako	k9
třináctiletý	třináctiletý	k2eAgInSc4d1
nastoupil	nastoupit	k5eAaPmAgInS
ke	k	k7c3
Královskému	královský	k2eAgNnSc3d1
námořnictvu	námořnictvo	k1gNnSc3
jako	jako	k8xS,k8xC
lodní	lodní	k2eAgMnSc1d1
poddůstojník	poddůstojník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
americké	americký	k2eAgFnSc2d1
války	válka	k1gFnSc2
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
sloužil	sloužit	k5eAaImAgMnS
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
v	v	k7c6
Americe	Amerika	k1gFnSc6
<g/>
,	,	kIx,
schválil	schválit	k5eAaPmAgInS
George	George	k1gInSc1
Washington	Washington	k1gInSc1
plán	plán	k1gInSc4
na	na	k7c4
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
únos	únos	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k8xS
se	se	k3xPyFc4
tato	tento	k3xDgFnSc1
informace	informace	k1gFnSc1
donesla	donést	k5eAaPmAgFnS
Britům	Brit	k1gMnPc3
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
Vilém	Vilém	k1gMnSc1
při	při	k7c6
cestách	cesta	k1gFnPc6
po	po	k7c6
městě	město	k1gNnSc6
doprovázen	doprovázet	k5eAaImNgInS
stráží	strážit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1785	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
poručíkem	poručík	k1gMnSc7
a	a	k8xC
kapitánem	kapitán	k1gMnSc7
lodi	loď	k1gFnSc2
HMS	HMS	kA
Pegasus	Pegasus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
roku	rok	k1gInSc2
1786	#num#	k4
byl	být	k5eAaImAgInS
přeložen	přeložit	k5eAaPmNgInS
do	do	k7c2
Indie	Indie	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
sloužil	sloužit	k5eAaImAgMnS
pod	pod	k7c7
Horatiem	Horatius	k1gMnSc7
Nelsonem	Nelson	k1gMnSc7
<g/>
,	,	kIx,
se	s	k7c7
kterým	který	k3yQgMnSc7,k3yIgMnSc7,k3yRgMnSc7
se	se	k3xPyFc4
stali	stát	k5eAaPmAgMnP
blízkými	blízký	k2eAgMnPc7d1
přáteli	přítel	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
Snažil	snažit	k5eAaImAgInS
se	se	k3xPyFc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
podobně	podobně	k6eAd1
jako	jako	k8xC,k8xS
jeho	jeho	k3xOp3gMnPc1
starší	starý	k2eAgMnPc1d2
bratři	bratr	k1gMnPc1
získal	získat	k5eAaPmAgInS
titul	titul	k1gInSc1
vévody	vévoda	k1gMnSc2
a	a	k8xC
tím	ten	k3xDgNnSc7
i	i	k9
stálý	stálý	k2eAgInSc4d1
příjem	příjem	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
jeho	jeho	k3xOp3gMnSc1
otec	otec	k1gMnSc1
o	o	k7c6
tom	ten	k3xDgInSc6
nechtěl	chtít	k5eNaImAgMnS
slyšet	slyšet	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nS
přinutil	přinutit	k5eAaPmAgInS
otce	otec	k1gMnSc4
jednat	jednat	k5eAaImF
<g/>
,	,	kIx,
začal	začít	k5eAaPmAgMnS
se	se	k3xPyFc4
Vilém	Vilém	k1gMnSc1
ucházet	ucházet	k5eAaImF
o	o	k7c4
místo	místo	k1gNnSc4
poslance	poslanec	k1gMnSc2
v	v	k7c6
Dolní	dolní	k2eAgFnSc6d1
sněmovně	sněmovna	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInPc4
otec	otec	k1gMnSc1
nechtěl	chtít	k5eNaImAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
tato	tento	k3xDgFnSc1
záležitost	záležitost	k1gFnSc1
přetřásala	přetřásat	k5eAaImAgFnS
mezi	mezi	k7c7
voliči	volič	k1gMnPc7
a	a	k8xC
tak	tak	k6eAd1
ho	on	k3xPp3gInSc4
v	v	k7c6
květnu	květen	k1gInSc6
roku	rok	k1gInSc2
1789	#num#	k4
jmenoval	jmenovat	k5eAaBmAgInS,k5eAaImAgInS
vévodou	vévoda	k1gMnSc7
z	z	k7c2
Clarence	Clarenec	k1gInSc2
a	a	k8xC
St	St	kA
Andrews	Andrews	k1gInSc4
a	a	k8xC
hrabětem	hrabě	k1gMnSc7
z	z	k7c2
Munsteru	Munster	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Manželství	manželství	k1gNnSc1
a	a	k8xC
potomci	potomek	k1gMnPc1
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1791	#num#	k4
žil	žít	k5eAaImAgInS
s	s	k7c7
irskou	irský	k2eAgFnSc7d1
herečkou	herečka	k1gFnSc7
Dorotheou	Dorothea	k1gFnSc7
Blandovou	Blandový	k2eAgFnSc4d1
(	(	kIx(
<g/>
později	pozdě	k6eAd2
přijala	přijmout	k5eAaPmAgFnS
jméno	jméno	k1gNnSc4
Dorothea	Dorotheus	k1gMnSc2
Jordan	Jordan	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
tohoto	tento	k3xDgInSc2
vztahu	vztah	k1gInSc2
se	se	k3xPyFc4
narodilo	narodit	k5eAaPmAgNnS
deset	deset	k4xCc1
nelegitimních	legitimní	k2eNgFnPc2d1
dětí	dítě	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
George	Georg	k1gMnSc2
FitzClarence	FitzClarenec	k1gMnSc2
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Earl	earl	k1gMnSc1
of	of	k?
Munster	Munster	k1gMnSc1
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1831	#num#	k4
Earl	earl	k1gMnSc1
of	of	k?
Munster	Munster	k1gMnSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
29	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1794	#num#	k4
–	–	k?
20	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1842	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Henry	Henry	k1gMnSc1
FitzClarence	FitzClarence	k1gFnSc2
</s>
<s>
Sophia	Sophia	k1gFnSc1
Sidney	Sidnea	k1gFnSc2
<g/>
,	,	kIx,
baronka	baronka	k1gFnSc1
De	De	k?
L	L	kA
<g/>
'	'	kIx"
<g/>
Isle	Isl	k1gMnSc2
and	and	k?
Dudley	Dudlea	k1gMnSc2
</s>
<s>
Lady	lad	k1gInPc1
Mary	Mary	k1gFnSc1
Fox	fox	k1gInSc1
(	(	kIx(
<g/>
19	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1798	#num#	k4
–	–	k?
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1864	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Lord	lord	k1gMnSc1
Frederick	Frederick	k1gMnSc1
FitzClarence	FitzClarence	k1gFnSc1
(	(	kIx(
<g/>
9	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1799	#num#	k4
–	–	k?
říjen	říjen	k1gInSc1
1854	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Elizabeth	Elizabeth	k1gFnSc1
Hay	Hay	k1gFnSc1
<g/>
,	,	kIx,
hraběnka	hraběnka	k1gFnSc1
of	of	k?
Erroll	Erroll	k1gInSc1
(	(	kIx(
<g/>
17	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1801	#num#	k4
–	–	k?
16	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1856	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Lord	lord	k1gMnSc1
Adolphus	Adolphus	k1gMnSc1
FitzClarence	FitzClarence	k1gFnSc1
(	(	kIx(
<g/>
18	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1802	#num#	k4
–	–	k?
17	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1856	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Lady	lady	k1gFnSc1
Augusta	August	k1gMnSc2
Kennedy-Erskine	Kennedy-Erskin	k1gMnSc5
</s>
<s>
Lord	lord	k1gMnSc1
Augustus	Augustus	k1gMnSc1
FitzClarence	FitzClarence	k1gFnSc1
</s>
<s>
Amelia	Amelium	k1gNnSc2
Cary	car	k1gMnPc4
<g/>
,	,	kIx,
hraběnka	hraběnka	k1gFnSc1
Falkland	Falklanda	k1gFnPc2
(	(	kIx(
<g/>
21	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1807	#num#	k4
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1858	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Tyto	tento	k3xDgFnPc1
děti	dítě	k1gFnPc1
měly	mít	k5eAaImAgFnP
řadu	řada	k1gFnSc4
prominentních	prominentní	k2eAgMnPc2d1
potomků	potomek	k1gMnPc2
<g/>
;	;	kIx,
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
nich	on	k3xPp3gMnPc2
např.	např.	kA
bývalý	bývalý	k2eAgMnSc1d1
britský	britský	k2eAgMnSc1d1
premiér	premiér	k1gMnSc1
David	David	k1gMnSc1
Cameron	Cameron	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
roku	rok	k1gInSc2
1818	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
Kew	Kew	k1gFnSc6
Palace	Palace	k1gFnSc2
v	v	k7c4
Surrey	Surrey	k1gInPc4
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
princeznou	princezna	k1gFnSc7
Adelheid	Adelheid	k1gInSc4
von	von	k1gInSc1
Sachsen-Meiningen	Sachsen-Meiningen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šlo	jít	k5eAaImAgNnS
o	o	k7c4
dvojitou	dvojitý	k2eAgFnSc4d1
svatbu	svatba	k1gFnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
tentýž	týž	k3xTgInSc4
den	den	k1gInSc4
se	se	k3xPyFc4
ženil	ženit	k5eAaImAgMnS
i	i	k9
Vilémův	Vilémův	k2eAgMnSc1d1
bratr	bratr	k1gMnSc1
Eduard	Eduard	k1gMnSc1
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Kentu	Kent	k1gInSc2
s	s	k7c7
jinou	jiný	k2eAgFnSc7d1
německou	německý	k2eAgFnSc7d1
princeznou	princezna	k1gFnSc7
<g/>
,	,	kIx,
Viktorií	Viktoria	k1gFnSc7
z	z	k7c2
dynastie	dynastie	k1gFnSc2
Sachsen-Coburg-Saalfeldské	Sachsen-Coburg-Saalfeldský	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Adelheid	Adelheid	k1gInSc1
měla	mít	k5eAaImAgFnS
v	v	k7c6
době	doba	k1gFnSc6
sňatku	sňatek	k1gInSc2
25	#num#	k4
let	léto	k1gNnPc2
a	a	k8xC
byla	být	k5eAaImAgFnS
o	o	k7c4
28	#num#	k4
let	léto	k1gNnPc2
mladší	mladý	k2eAgMnSc1d2
než	než	k8xS
Vilém	Vilém	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manželství	manželství	k1gNnSc1
nebylo	být	k5eNaImAgNnS
bezdětné	bezdětný	k2eAgNnSc1d1
–	–	k?
Adelheid	Adelheid	k1gInSc1
porodila	porodit	k5eAaPmAgFnS
čtyřikrát	čtyřikrát	k6eAd1
<g/>
,	,	kIx,
nejstarší	starý	k2eAgFnSc1d3
dcerka	dcerka	k1gFnSc1
však	však	k9
zemřela	zemřít	k5eAaPmAgFnS
těsně	těsně	k6eAd1
po	po	k7c6
porodu	porod	k1gInSc6
<g/>
,	,	kIx,
ve	v	k7c6
dvou	dva	k4xCgInPc6
případech	případ	k1gInPc6
se	se	k3xPyFc4
děti	dítě	k1gFnPc1
narodily	narodit	k5eAaPmAgFnP
mrtvé	mrtvý	k2eAgFnPc1d1
a	a	k8xC
jediná	jediný	k2eAgFnSc1d1
přeživší	přeživší	k2eAgFnSc1d1
dcerka	dcerka	k1gFnSc1
zemřela	zemřít	k5eAaPmAgFnS
v	v	k7c6
necelých	celý	k2eNgInPc6d1
čtyřech	čtyři	k4xCgInPc6
měsících	měsíc	k1gInPc6
věku	věk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Charlotte	Charlotit	k5eAaImRp2nP,k5eAaBmRp2nP,k5eAaPmRp2nP
Augusta	August	k1gMnSc4
Louisa	Louisa	k?
(	(	kIx(
<g/>
*	*	kIx~
<g/>
/	/	kIx~
<g/>
†	†	k?
21	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1819	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
potrat	potrat	k1gInSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1819	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Elizabeth	Elizabeth	k1gFnSc1
Georgiana	Georgiana	k1gFnSc1
Adelaide	Adelaid	k1gInSc5
(	(	kIx(
<g/>
10	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1820	#num#	k4
–	–	k?
4	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1821	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
dvojčata-chlapci	dvojčata-chlapec	k1gMnPc1
(	(	kIx(
<g/>
narozeni	narozen	k2eAgMnPc1d1
mrtví	mrtvý	k1gMnPc1
23	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1824	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Portrét	portrét	k1gInSc1
Viléma	Vilém	k1gMnSc2
ve	v	k7c6
vojenské	vojenský	k2eAgFnSc6d1
uniformě	uniforma	k1gFnSc6
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS
tedy	tedy	k9
<g/>
,	,	kIx,
aniž	aniž	k8xC,k8xS
by	by	kYmCp3nS
po	po	k7c6
něm	on	k3xPp3gNnSc6
zůstali	zůstat	k5eAaPmAgMnP
legitimní	legitimní	k2eAgMnPc1d1
potomci	potomek	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Karolina	Karolinum	k1gNnPc4
von	von	k1gInSc1
Linsingen	Linsingen	k1gInSc4
</s>
<s>
Téměř	téměř	k6eAd1
neznámá	neznámá	k1gFnSc1
však	však	k9
je	být	k5eAaImIp3nS
skutečnost	skutečnost	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
ještě	ještě	k9
jako	jako	k9
mladík	mladík	k1gMnSc1
(	(	kIx(
<g/>
tehdy	tehdy	k6eAd1
až	až	k8xS
třetí	třetí	k4xOgNnSc4
v	v	k7c6
pořadí	pořadí	k1gNnSc6
na	na	k7c6
následnictví	následnictví	k1gNnSc6
trůnu	trůn	k1gInSc2
<g/>
)	)	kIx)
se	se	k3xPyFc4
tajně	tajně	k6eAd1
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
hannoverskou	hannoverský	k2eAgFnSc7d1
šlechtičnou	šlechtična	k1gFnSc7
<g/>
,	,	kIx,
hraběnkou	hraběnka	k1gFnSc7
Karolinou	Karolina	k1gFnSc7
Charlottou	Charlotta	k1gFnSc7
Dorotheou	Dorothea	k1gFnSc7
von	von	k1gInSc4
Linsingen	Linsingen	k1gInSc1
(	(	kIx(
<g/>
1768	#num#	k4
<g/>
–	–	k?
<g/>
1815	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznámili	seznámit	k5eAaPmAgMnP
se	se	k3xPyFc4
v	v	k7c6
Hannoveru	Hannover	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
1790	#num#	k4
a	a	k8xC
v	v	k7c6
srpnu	srpen	k1gInSc6
roku	rok	k1gInSc2
1791	#num#	k4
se	se	k3xPyFc4
tajně	tajně	k6eAd1
vzali	vzít	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
po	po	k7c6
čase	čas	k1gInSc6
vyšla	vyjít	k5eAaPmAgFnS
pravda	pravda	k1gFnSc1
o	o	k7c6
tajném	tajný	k2eAgInSc6d1
a	a	k8xC
nerovném	rovný	k2eNgInSc6d1
sňatku	sňatek	k1gInSc6
najevo	najevo	k6eAd1
<g/>
,	,	kIx,
vypukl	vypuknout	k5eAaPmAgInS
skandál	skandál	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Královská	královský	k2eAgFnSc1d1
rodina	rodina	k1gFnSc1
<g/>
,	,	kIx,
především	především	k6eAd1
královna	královna	k1gFnSc1
Šarlota	Šarlota	k1gFnSc1
<g/>
,	,	kIx,
odmítla	odmítnout	k5eAaPmAgFnS
Karolinu	Karolinum	k1gNnSc3
přijmout	přijmout	k5eAaPmF
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1792	#num#	k4
k	k	k7c3
rozvodu	rozvod	k1gInSc3
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
Karolina	Karolinum	k1gNnPc4
již	již	k9
byla	být	k5eAaImAgFnS
těhotná	těhotný	k2eAgFnSc1d1
(	(	kIx(
<g/>
v	v	k7c6
listopadu	listopad	k1gInSc6
1792	#num#	k4
údajně	údajně	k6eAd1
potratila	potratit	k5eAaPmAgFnS
<g/>
,	,	kIx,
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
jí	on	k3xPp3gFnSc7
však	však	k9
bylo	být	k5eAaImAgNnS
zamlčeno	zamlčen	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
dítě	dítě	k1gNnSc1
–	–	k?
chlapec	chlapec	k1gMnSc1
–	–	k?
předčasný	předčasný	k2eAgInSc4d1
porod	porod	k1gInSc4
přežilo	přežít	k5eAaPmAgNnS
a	a	k8xC
bylo	být	k5eAaImAgNnS
dáno	dát	k5eAaPmNgNnS
pod	pod	k7c7
jménem	jméno	k1gNnSc7
Hans	Hans	k1gMnSc1
Georg	Georg	k1gMnSc1
Meyer	Meyer	k1gMnSc1
na	na	k7c4
vychování	vychování	k1gNnSc4
do	do	k7c2
bohaté	bohatý	k2eAgFnSc2d1
židovské	židovský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karolina	Karolinum	k1gNnPc1
(	(	kIx(
<g/>
posléze	posléze	k6eAd1
provdaná	provdaný	k2eAgFnSc1d1
za	za	k7c4
lékaře	lékař	k1gMnPc4
dr	dr	kA
<g/>
.	.	kIx.
Adolfa	Adolf	k1gMnSc2
Meineke	Meinek	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zemřela	zemřít	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1815	#num#	k4
<g/>
;	;	kIx,
snad	snad	k9
lze	lze	k6eAd1
i	i	k9
v	v	k7c6
této	tento	k3xDgFnSc6
skutečnosti	skutečnost	k1gFnSc6
spatřovat	spatřovat	k5eAaImF
důvod	důvod	k1gInSc4
<g/>
,	,	kIx,
proč	proč	k6eAd1
se	se	k3xPyFc4
Vilém	Vilém	k1gMnSc1
oženil	oženit	k5eAaPmAgMnS
až	až	k9
v	v	k7c6
poměrně	poměrně	k6eAd1
pokročilém	pokročilý	k2eAgInSc6d1
věku	věk	k1gInSc6
padesáti	padesát	k4xCc2
dvou	dva	k4xCgNnPc2
let	léto	k1gNnPc2
v	v	k7c6
roce	rok	k1gInSc6
1817	#num#	k4
<g/>
,	,	kIx,
tedy	tedy	k9
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
po	po	k7c6
její	její	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
druhé	druhý	k4xOgFnSc6
straně	strana	k1gFnSc6
i	i	k9
dva	dva	k4xCgInPc4
z	z	k7c2
pěti	pět	k4xCc2
jeho	jeho	k3xOp3gInSc6
bratří	bratřit	k5eAaImIp3nP
se	se	k3xPyFc4
oženili	oženit	k5eAaPmAgMnP
v	v	k7c6
takto	takto	k6eAd1
pozdním	pozdní	k2eAgInSc6d1
věku	věk	k1gInSc6
<g/>
,	,	kIx,
poté	poté	k6eAd1
co	co	k9
v	v	k7c6
roce	rok	k1gInSc6
1817	#num#	k4
zemřela	zemřít	k5eAaPmAgFnS
po	po	k7c6
porodu	porod	k1gInSc6
svého	svůj	k3xOyFgNnSc2
jediného	jediné	k1gNnSc2
mrtvě	mrtvě	k6eAd1
narozeného	narozený	k2eAgNnSc2d1
dítěte	dítě	k1gNnSc2
Šarlota	Šarlota	k1gFnSc1
Augusta	August	k1gMnSc2
<g/>
,	,	kIx,
jediná	jediný	k2eAgFnSc1d1
dcera	dcera	k1gFnSc1
nástupce	nástupce	k1gMnSc2
trůnu	trůn	k1gInSc2
<g/>
,	,	kIx,
waleského	waleský	k2eAgMnSc2d1
prince	princ	k1gMnSc2
Jiřího	Jiří	k1gMnSc2
<g/>
,	,	kIx,
a	a	k8xC
britský	britský	k2eAgInSc1d1
trůn	trůn	k1gInSc1
neměl	mít	k5eNaImAgInS
v	v	k7c4
další	další	k2eAgFnSc4d1
generaci	generace	k1gFnSc4
následníka	následník	k1gMnSc2
<g/>
.	.	kIx.
<g/>
Portrét	portrét	k1gInSc1
Vilémovy	Vilémův	k2eAgFnSc2d1
manželky	manželka	k1gFnSc2
Adelheid	Adelheida	k1gFnPc2
</s>
<s>
Admirál	admirál	k1gMnSc1
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1
nejstarší	starý	k2eAgMnSc1d3
bratr	bratr	k1gMnSc1
Jiří	Jiří	k1gMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
<g/>
,	,	kIx,
poté	poté	k6eAd1
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
byl	být	k5eAaImAgMnS
jeho	jeho	k3xOp3gMnSc1
otec	otec	k1gMnSc1
roku	rok	k1gInSc2
1811	#num#	k4
postižen	postihnout	k5eAaPmNgMnS
záchvatem	záchvat	k1gInSc7
duševní	duševní	k2eAgFnPc4d1
nemoci	nemoc	k1gFnPc4
<g/>
,	,	kIx,
princem	princ	k1gMnSc7
regentem	regens	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1820	#num#	k4
Jiří	Jiří	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
zemřel	zemřít	k5eAaPmAgMnS
a	a	k8xC
jeho	jeho	k3xOp3gMnSc1
nejstarší	starý	k2eAgMnSc1d3
syn	syn	k1gMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
britským	britský	k2eAgMnSc7d1
panovníkem	panovník	k1gMnSc7
jako	jako	k8xC,k8xS
Jiří	Jiří	k1gMnPc1
IV	IV	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
následníkem	následník	k1gMnSc7
trůnu	trůn	k1gInSc2
pak	pak	k8xC
druhý	druhý	k4xOgMnSc1
syn	syn	k1gMnSc1
Jiřího	Jiří	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Frederik	Frederik	k1gMnSc1
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Yorku	York	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
roku	rok	k1gInSc2
1827	#num#	k4
zemřel	zemřít	k5eAaPmAgMnS
i	i	k9
on	on	k3xPp3gMnSc1
<g/>
,	,	kIx,
stal	stát	k5eAaPmAgInS
se	se	k3xPyFc4
v	v	k7c6
šedesáti	šedesát	k4xCc6
letech	léto	k1gNnPc6
následníkem	následník	k1gMnSc7
trůnu	trůn	k1gInSc2
Vilém	Vilém	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
konci	konec	k1gInSc3
roku	rok	k1gInSc2
ho	on	k3xPp3gNnSc4
premiér	premiér	k1gMnSc1
George	Georg	k1gFnSc2
Canning	Canning	k1gInSc1
jmenoval	jmenovat	k5eAaImAgInS,k5eAaBmAgInS
do	do	k7c2
úřadu	úřad	k1gInSc2
admirála	admirál	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
zřízen	zřízen	k2eAgInSc1d1
roku	rok	k1gInSc2
1709	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c4
dobu	doba	k1gFnSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
admirálem	admirál	k1gMnSc7
<g/>
,	,	kIx,
měl	mít	k5eAaImAgInS
stálé	stálý	k2eAgInPc4d1
spory	spor	k1gInPc4
s	s	k7c7
admirálskou	admirálský	k2eAgFnSc7d1
radou	rada	k1gFnSc7
složenou	složený	k2eAgFnSc7d1
z	z	k7c2
důstojníků	důstojník	k1gMnPc2
admirality	admiralita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Navzdory	navzdory	k7c3
rozporům	rozpor	k1gInPc3
dosáhl	dosáhnout	k5eAaPmAgMnS
mnoha	mnoho	k4c3
změn	změna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zakázal	zakázat	k5eAaPmAgMnS
použití	použití	k1gNnSc4
devítiocasého	devítiocasý	k2eAgInSc2d1
biče	bič	k1gInSc2
pro	pro	k7c4
tresty	trest	k1gInPc4
kromě	kromě	k7c2
vzpoury	vzpoura	k1gFnSc2
<g/>
,	,	kIx,
dosáhl	dosáhnout	k5eAaPmAgInS
zlepšení	zlepšení	k1gNnSc4
úrovně	úroveň	k1gFnSc2
dělostřelectva	dělostřelectvo	k1gNnSc2
a	a	k8xC
vyžadoval	vyžadovat	k5eAaImAgMnS
pravidelné	pravidelný	k2eAgFnPc4d1
zprávy	zpráva	k1gFnPc4
o	o	k7c6
stavu	stav	k1gInSc6
a	a	k8xC
připravenosti	připravenost	k1gFnSc6
každé	každý	k3xTgFnSc2
lodi	loď	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nechal	nechat	k5eAaPmAgMnS
postavit	postavit	k5eAaPmF
první	první	k4xOgInSc4
válečný	válečný	k2eAgInSc4d1
parník	parník	k1gInSc4
a	a	k8xC
doporučoval	doporučovat	k5eAaImAgMnS
výstavbu	výstavba	k1gFnSc4
dalších	další	k2eAgMnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbytek	zbytek	k1gInSc1
období	období	k1gNnSc2
vlády	vláda	k1gFnSc2
svého	svůj	k3xOyFgMnSc4
bratra	bratr	k1gMnSc4
strávil	strávit	k5eAaPmAgMnS
jako	jako	k9
člen	člen	k1gMnSc1
Sněmovny	sněmovna	k1gFnSc2
lordů	lord	k1gMnPc2
a	a	k8xC
podporoval	podporovat	k5eAaImAgInS
emancipaci	emancipace	k1gFnSc3
katolíků	katolík	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Socha	Socha	k1gMnSc1
Viléma	Vilém	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britského	britský	k2eAgMnSc2d1
v	v	k7c6
německém	německý	k2eAgInSc6d1
Göttingenu	Göttingen	k1gInSc6
(	(	kIx(
<g/>
Wilhelmsplatz	Wilhelmsplatz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Vláda	vláda	k1gFnSc1
</s>
<s>
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
jeho	jeho	k3xOp3gMnSc1
bratr	bratr	k1gMnSc1
26	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1830	#num#	k4
zemřel	zemřít	k5eAaPmAgMnS
bez	bez	k7c2
legitimních	legitimní	k2eAgMnPc2d1
dědiců	dědic	k1gMnPc2
<g/>
,	,	kIx,
nastoupil	nastoupit	k5eAaPmAgMnS
na	na	k7c4
britský	britský	k2eAgInSc4d1
trůn	trůn	k1gInSc4
v	v	k7c6
64	#num#	k4
letech	let	k1gInPc6
jako	jako	k8xC,k8xS
nejstarší	starý	k2eAgFnSc1d3
osoba	osoba	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
kdy	kdy	k6eAd1
britský	britský	k2eAgInSc4d1
trůn	trůn	k1gInSc4
získala	získat	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
svého	svůj	k3xOyFgMnSc2
bratra	bratr	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
trávil	trávit	k5eAaImAgMnS
mnoho	mnoho	k6eAd1
času	čas	k1gInSc2
na	na	k7c6
Windsorském	windsorský	k2eAgInSc6d1
hradu	hrad	k1gInSc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
Vilém	Vilém	k1gMnSc1
zdržoval	zdržovat	k5eAaImAgMnS
v	v	k7c6
Londýně	Londýn	k1gInSc6
nebo	nebo	k8xC
Brightonu	Brighton	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většinu	většina	k1gFnSc4
sbírek	sbírka	k1gFnPc2
obrazů	obraz	k1gInPc2
svého	svůj	k3xOyFgMnSc2
bratra	bratr	k1gMnSc2
věnoval	věnovat	k5eAaPmAgMnS,k5eAaImAgMnS
galeriím	galerie	k1gFnPc3
a	a	k8xC
zahájil	zahájit	k5eAaPmAgMnS
rozsáhlou	rozsáhlý	k2eAgFnSc4d1
rekonstrukci	rekonstrukce	k1gFnSc4
Buckinghamského	buckinghamský	k2eAgInSc2d1
paláce	palác	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
smrti	smrt	k1gFnSc6
předchozího	předchozí	k2eAgMnSc2d1
krále	král	k1gMnSc2
byly	být	k5eAaImAgFnP
vyhlášeny	vyhlášen	k2eAgFnPc1d1
všeobecné	všeobecný	k2eAgFnPc1d1
volby	volba	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toryové	tory	k1gMnPc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
vzešli	vzejít	k5eAaPmAgMnP
oslabeni	oslaben	k2eAgMnPc1d1
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
stále	stále	k6eAd1
disponovali	disponovat	k5eAaBmAgMnP
většinou	většinou	k6eAd1
v	v	k7c6
dolní	dolní	k2eAgFnSc6d1
komoře	komora	k1gFnSc6
parlamentu	parlament	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
byli	být	k5eAaImAgMnP
nejednotní	jednotný	k2eNgMnPc1d1
<g/>
,	,	kIx,
vytvořil	vytvořit	k5eAaPmAgMnS
novou	nový	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
Whig	whig	k1gMnSc1
Charles	Charles	k1gMnSc1
Grey	Grea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
slíbil	slíbit	k5eAaPmAgInS
reformovat	reformovat	k5eAaBmF
volební	volební	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
od	od	k7c2
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
doznal	doznat	k5eAaPmAgInS
pouze	pouze	k6eAd1
malých	malý	k2eAgFnPc2d1
změn	změna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nesrovnalosti	nesrovnalost	k1gFnPc1
mezi	mezi	k7c7
jednotlivými	jednotlivý	k2eAgInPc7d1
obvody	obvod	k1gInPc7
byly	být	k5eAaImAgFnP
obrovské	obrovský	k2eAgInPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
velká	velký	k2eAgNnPc1d1
města	město	k1gNnPc1
jako	jako	k8xS,k8xC
Manchester	Manchester	k1gInSc1
nebo	nebo	k8xC
Birmingham	Birmingham	k1gInSc4
neměla	mít	k5eNaImAgFnS
v	v	k7c6
parlamentu	parlament	k1gInSc6
zastoupení	zastoupení	k1gNnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
byla	být	k5eAaImAgFnS
částí	část	k1gFnSc7
hrabství	hrabství	k1gNnSc2
<g/>
,	,	kIx,
malá	malá	k1gFnSc1
městečka	městečko	k1gNnSc2
(	(	kIx(
<g/>
označována	označovat	k5eAaImNgFnS
jako	jako	k8xC,k8xS
rotten	rotten	k2eAgInSc4d1
boroughs	boroughs	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
například	například	k6eAd1
Old	Olda	k1gFnPc2
Sarum	Sarum	k1gInSc4
se	s	k7c7
sedmi	sedm	k4xCc7
voliči	volič	k1gInPc7
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
zastoupena	zastoupit	k5eAaPmNgFnS
dvěma	dva	k4xCgInPc7
poslanci	poslanec	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc1
obvody	obvod	k1gInPc1
byly	být	k5eAaImAgInP
ovládány	ovládán	k2eAgMnPc4d1
aristokraty	aristokrat	k1gMnPc4
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnPc1
kandidáti	kandidát	k1gMnPc1
byli	být	k5eAaImAgMnP
často	často	k6eAd1
zvoleni	zvolen	k2eAgMnPc1d1
<g/>
,	,	kIx,
případně	případně	k6eAd1
mohli	moct	k5eAaImAgMnP
tato	tento	k3xDgNnPc4
zajištěná	zajištěný	k2eAgNnPc4d1
místa	místo	k1gNnPc4
v	v	k7c6
parlamentu	parlament	k1gInSc6
i	i	k8xC
prodávat	prodávat	k5eAaImF
zájemcům	zájemce	k1gMnPc3
o	o	k7c4
post	post	k1gInSc4
poslance	poslanec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Když	když	k8xS
roku	rok	k1gInSc2
1831	#num#	k4
neprošel	projít	k5eNaPmAgInS
dolní	dolní	k2eAgFnSc7d1
komorou	komora	k1gFnSc7
parlamentu	parlament	k1gInSc2
první	první	k4xOgInSc4
reformní	reformní	k2eAgInSc4d1
zákon	zákon	k1gInSc4
<g/>
,	,	kIx,
Greyovi	Greyův	k2eAgMnPc1d1
ministři	ministr	k1gMnPc1
jej	on	k3xPp3gMnSc4
pobízeli	pobízet	k5eAaImAgMnP
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
vyhlásil	vyhlásit	k5eAaPmAgMnS
nové	nový	k2eAgFnPc4d1
všeobecné	všeobecný	k2eAgFnPc4d1
volby	volba	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpočátku	zpočátku	k6eAd1
nesouhlasil	souhlasit	k5eNaImAgMnS
<g/>
,	,	kIx,
protože	protože	k8xS
volby	volba	k1gFnPc1
se	se	k3xPyFc4
konaly	konat	k5eAaImAgFnP
předchozí	předchozí	k2eAgInSc4d1
rok	rok	k1gInSc4
a	a	k8xC
veřejnost	veřejnost	k1gFnSc4
byla	být	k5eAaImAgFnS
podrážděna	podrážděn	k2eAgFnSc1d1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
mohlo	moct	k5eAaImAgNnS
vyústit	vyústit	k5eAaPmF
v	v	k7c4
násilné	násilný	k2eAgInPc4d1
protesty	protest	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
byl	být	k5eAaImAgInS
roztrpčen	roztrpčit	k5eAaPmNgInS
úmyslem	úmysl	k1gInSc7
opozice	opozice	k1gFnSc2
vydat	vydat	k5eAaPmF
rezoluci	rezoluce	k1gFnSc4
proti	proti	k7c3
rozpuštění	rozpuštění	k1gNnSc3
parlamentu	parlament	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
úmyslu	úmysl	k1gInSc6
viděl	vidět	k5eAaImAgMnS
útok	útok	k1gInSc4
proti	proti	k7c3
svému	svůj	k3xOyFgNnSc3
právu	právo	k1gNnSc3
odročit	odročit	k5eAaPmF
jednání	jednání	k1gNnSc4
parlamentu	parlament	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhodl	rozhodnout	k5eAaPmAgMnS
se	se	k3xPyFc4
zúčastnit	zúčastnit	k5eAaPmF
jednání	jednání	k1gNnSc3
Sněmovny	sněmovna	k1gFnSc2
lordů	lord	k1gMnPc2
a	a	k8xC
jednání	jednání	k1gNnSc2
parlamentu	parlament	k1gInSc2
odročit	odročit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přístup	přístup	k1gInSc4
poslanců	poslanec	k1gMnPc2
ho	on	k3xPp3gMnSc4
ale	ale	k8xC
rozčílil	rozčílit	k5eAaPmAgMnS
natolik	natolik	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
parlament	parlament	k1gInSc1
rovnou	rovnou	k6eAd1
rozpustil	rozpustit	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nových	nový	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
do	do	k7c2
dolní	dolní	k2eAgFnSc2d1
komory	komora	k1gFnSc2
zvítězili	zvítězit	k5eAaPmAgMnP
stoupenci	stoupenec	k1gMnPc1
reforem	reforma	k1gFnPc2
<g/>
,	,	kIx,
ovšem	ovšem	k9
v	v	k7c6
horní	horní	k2eAgFnSc6d1
komoře	komora	k1gFnSc6
převládali	převládat	k5eAaImAgMnP
jejich	jejich	k3xOp3gMnPc1
odpůrci	odpůrce	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Krátkým	krátký	k2eAgNnSc7d1
přerušením	přerušení	k1gNnSc7
této	tento	k3xDgFnSc2
krize	krize	k1gFnSc2
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
jeho	jeho	k3xOp3gFnSc1
korunovace	korunovace	k1gFnSc1
8	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
1831	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nebyl	být	k5eNaImAgMnS
si	se	k3xPyFc3
jist	jist	k2eAgMnSc1d1
<g/>
,	,	kIx,
zda	zda	k8xS
na	na	k7c4
ni	on	k3xPp3gFnSc4
má	mít	k5eAaImIp3nS
přistoupit	přistoupit	k5eAaPmF
<g/>
,	,	kIx,
ale	ale	k8xC
nechal	nechat	k5eAaPmAgMnS
se	s	k7c7
tradicionalisty	tradicionalista	k1gMnPc7
přesvědčit	přesvědčit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
odmítl	odmítnout	k5eAaPmAgMnS
oslavy	oslava	k1gFnPc4
takového	takový	k3xDgInSc2
rozsahu	rozsah	k1gInSc2
<g/>
,	,	kIx,
jaké	jaký	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
doprovázely	doprovázet	k5eAaImAgFnP
korunovaci	korunovace	k1gFnSc4
jeho	jeho	k3xOp3gMnSc2,k3xPp3gMnSc2
předchůdce	předchůdce	k1gMnSc2
a	a	k8xC
vyžádaly	vyžádat	k5eAaPmAgFnP
si	se	k3xPyFc3
náklady	náklad	k1gInPc4
ve	v	k7c4
výši	výše	k1gFnSc4
240	#num#	k4
000	#num#	k4
liber	libra	k1gFnPc2
a	a	k8xC
určil	určit	k5eAaPmAgInS
maximální	maximální	k2eAgFnSc4d1
výši	výše	k1gFnSc4
prostředků	prostředek	k1gInPc2
na	na	k7c4
30	#num#	k4
000	#num#	k4
liber	libra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
odmítnutí	odmítnutí	k1gNnSc6
druhého	druhý	k4xOgMnSc2
návrhu	návrh	k1gInSc3
reforem	reforma	k1gFnPc2
Sněmovnou	sněmovna	k1gFnSc7
lordů	lord	k1gMnPc2
roku	rok	k1gInSc2
1831	#num#	k4
se	se	k3xPyFc4
rozpoutala	rozpoutat	k5eAaPmAgFnS
kampaň	kampaň	k1gFnSc1
za	za	k7c4
jejich	jejich	k3xOp3gNnSc4
prosazení	prosazení	k1gNnSc4
po	po	k7c6
celé	celý	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
základě	základ	k1gInSc6
podpory	podpora	k1gFnSc2
veřejnosti	veřejnost	k1gFnSc2
se	se	k3xPyFc4
Greyova	Greyův	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
rozhodla	rozhodnout	k5eAaPmAgFnS
neuznat	uznat	k5eNaPmF
porážku	porážka	k1gFnSc4
a	a	k8xC
znovu	znovu	k6eAd1
navrhla	navrhnout	k5eAaPmAgFnS
přijetí	přijetí	k1gNnSc4
tohoto	tento	k3xDgInSc2
zákona	zákon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Grey	Grea	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
byl	být	k5eAaImAgInS
zklamán	zklamat	k5eAaPmNgMnS
potížemi	potíž	k1gFnPc7
s	s	k7c7
projednáváním	projednávání	k1gNnSc7
zákona	zákon	k1gInSc2
v	v	k7c6
horní	horní	k2eAgFnSc6d1
komoře	komora	k1gFnSc6
<g/>
,	,	kIx,
jej	on	k3xPp3gInSc4
žádal	žádat	k5eAaImAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
jmenoval	jmenovat	k5eAaImAgInS,k5eAaBmAgInS
nové	nový	k2eAgMnPc4d1
peery	peer	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
by	by	kYmCp3nP
pomohli	pomoct	k5eAaPmAgMnP
tento	tento	k3xDgInSc4
zákon	zákon	k1gInSc4
prosadit	prosadit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
On	on	k3xPp3gMnSc1
sám	sám	k3xTgMnSc1
byl	být	k5eAaImAgInS
ochoten	ochoten	k2eAgMnSc1d1
jmenovat	jmenovat	k5eAaImF,k5eAaBmF
nové	nový	k2eAgMnPc4d1
peery	peer	k1gMnPc4
potřebné	potřebný	k2eAgMnPc4d1
pro	pro	k7c4
přijetí	přijetí	k1gNnSc4
reforem	reforma	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
zároveň	zároveň	k6eAd1
viděl	vidět	k5eAaImAgMnS
nevýhody	nevýhoda	k1gFnPc4
ve	v	k7c6
vytváření	vytváření	k1gNnSc6
stále	stále	k6eAd1
početnější	početní	k2eAgFnSc2d2
horní	horní	k2eAgFnSc2d1
komory	komora	k1gFnSc2
a	a	k8xC
trval	trvat	k5eAaImAgInS
na	na	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
nově	nově	k6eAd1
vytvořená	vytvořený	k2eAgNnPc1d1
místa	místo	k1gNnPc1
byla	být	k5eAaImAgNnP
pokud	pokud	k8xS
možno	možno	k6eAd1
vyhrazena	vyhradit	k5eAaPmNgFnS
pro	pro	k7c4
nejstarší	starý	k2eAgMnPc4d3
syny	syn	k1gMnPc4
nebo	nebo	k8xC
dědice	dědic	k1gMnPc4
současných	současný	k2eAgInPc2d1
členů	člen	k1gInPc2
horní	horní	k2eAgFnSc2d1
komory	komora	k1gFnSc2
a	a	k8xC
postupně	postupně	k6eAd1
tak	tak	k6eAd1
vedla	vést	k5eAaImAgFnS
k	k	k7c3
opětovnému	opětovný	k2eAgNnSc3d1
snížení	snížení	k1gNnSc3
počtu	počet	k1gInSc2
poslanců	poslanec	k1gMnPc2
Sněmovny	sněmovna	k1gFnSc2
lordů	lord	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Doplněná	doplněný	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
neodmítla	odmítnout	k5eNaPmAgFnS
zákon	zákon	k1gInSc4
přímo	přímo	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
snažila	snažit	k5eAaImAgFnS
se	se	k3xPyFc4
změnit	změnit	k5eAaPmF
jeho	jeho	k3xOp3gInPc4
charakter	charakter	k1gInSc4
pomocí	pomocí	k7c2
dodatků	dodatek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Grey	Grea	k1gFnPc1
a	a	k8xC
jeho	jeho	k3xOp3gMnPc1
ministři	ministr	k1gMnPc1
pohrozili	pohrozit	k5eAaPmAgMnP
rezignací	rezignace	k1gFnSc7
<g/>
,	,	kIx,
pokud	pokud	k8xS
panovník	panovník	k1gMnSc1
nebude	být	k5eNaImBp3nS
souhlasit	souhlasit	k5eAaImF
se	s	k7c7
jmenováním	jmenování	k1gNnSc7
dostatečného	dostatečný	k2eAgNnSc2d1
počtů	počet	k1gInPc2
nových	nový	k2eAgMnPc2d1
peerů	peer	k1gMnPc2
pro	pro	k7c4
přijetí	přijetí	k1gNnSc4
reforem	reforma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
On	on	k3xPp3gMnSc1
ale	ale	k8xC
jejich	jejich	k3xOp3gMnSc1
rezignaci	rezignace	k1gFnSc4
přijal	přijmout	k5eAaPmAgMnS
a	a	k8xC
pověřil	pověřit	k5eAaPmAgMnS
sestavením	sestavení	k1gNnSc7
nové	nový	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
Wellingtona	Wellingtona	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
ten	ten	k3xDgMnSc1
neměl	mít	k5eNaImAgMnS
dostatečnou	dostatečný	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
pro	pro	k7c4
vytvoření	vytvoření	k1gNnSc4
stabilní	stabilní	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vilém	Vilém	k1gMnSc1
pak	pak	k9
souhlasil	souhlasit	k5eAaImAgMnS
s	s	k7c7
opětným	opětný	k2eAgNnSc7d1
jmenováním	jmenování	k1gNnSc7
Greyovy	Greyův	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
a	a	k8xC
vytvořením	vytvoření	k1gNnSc7
nových	nový	k2eAgMnPc2d1
peerů	peer	k1gMnPc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
bude	být	k5eAaImBp3nS
Sněmovna	sněmovna	k1gFnSc1
lordů	lord	k1gMnPc2
pokračovat	pokračovat	k5eAaImF
v	v	k7c6
obstrukcích	obstrukce	k1gFnPc6
s	s	k7c7
přijetím	přijetí	k1gNnSc7
zákona	zákon	k1gInSc2
o	o	k7c6
reformách	reforma	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1832	#num#	k4
byl	být	k5eAaImAgInS
zákon	zákon	k1gInSc4
reformující	reformující	k2eAgInPc4d1
volební	volební	k2eAgInPc4d1
obvody	obvod	k1gInPc4
přijat	přijat	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
zahraniční	zahraniční	k2eAgFnSc6d1
politice	politika	k1gFnSc6
podporoval	podporovat	k5eAaImAgInS
záměr	záměr	k1gInSc1
výstavby	výstavba	k1gFnSc2
Suezského	suezský	k2eAgInSc2d1
průplavu	průplav	k1gInSc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
příspěvek	příspěvek	k1gInSc1
ke	k	k7c3
zlepšení	zlepšení	k1gNnSc3
vztahů	vztah	k1gInPc2
s	s	k7c7
Egyptem	Egypt	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
se	se	k3xPyFc4
snažil	snažit	k5eAaImAgMnS
urovnat	urovnat	k5eAaPmF
vztahy	vztah	k1gInPc4
s	s	k7c7
Amerikou	Amerika	k1gFnSc7
<g/>
,	,	kIx,
silně	silně	k6eAd1
narušené	narušený	k2eAgInPc1d1
za	za	k7c2
vlády	vláda	k1gFnSc2
jeho	on	k3xPp3gMnSc2,k3xOp3gMnSc2
otce	otec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Pozdní	pozdní	k2eAgNnSc1d1
období	období	k1gNnSc1
</s>
<s>
V	v	k7c6
pozdním	pozdní	k2eAgNnSc6d1
období	období	k1gNnSc6
své	svůj	k3xOyFgFnSc2
vlády	vláda	k1gFnSc2
zasáhl	zasáhnout	k5eAaPmAgInS
do	do	k7c2
politiky	politika	k1gFnSc2
pouze	pouze	k6eAd1
jednou	jeden	k4xCgFnSc7
<g/>
,	,	kIx,
když	když	k8xS
roku	rok	k1gInSc2
1834	#num#	k4
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
poslední	poslední	k2eAgMnSc1d1
britský	britský	k2eAgMnSc1d1
panovník	panovník	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
tak	tak	k6eAd1
učinil	učinit	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
,	,	kIx,
jmenoval	jmenovat	k5eAaBmAgMnS,k5eAaImAgMnS
předsedu	předseda	k1gMnSc4
vlády	vláda	k1gFnSc2
v	v	k7c6
rozporu	rozpor	k1gInSc6
s	s	k7c7
vůlí	vůle	k1gFnSc7
parlamentu	parlament	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vláda	vláda	k1gFnSc1
čelila	čelit	k5eAaImAgFnS
velké	velká	k1gFnSc3
nepopularitě	nepopularita	k1gFnSc3
<g/>
,	,	kIx,
Grey	Gre	k1gMnPc4
odstoupil	odstoupit	k5eAaPmAgMnS
a	a	k8xC
nahradil	nahradit	k5eAaPmAgMnS
ho	on	k3xPp3gInSc4
William	William	k1gInSc4
Lamb	Lamba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
ponechal	ponechat	k5eAaPmAgMnS
ve	v	k7c6
funkci	funkce	k1gFnSc6
většinu	většina	k1gFnSc4
ministrů	ministr	k1gMnPc2
a	a	k8xC
získal	získat	k5eAaPmAgMnS
v	v	k7c6
dolní	dolní	k2eAgFnSc6d1
komoře	komora	k1gFnSc6
dostatečnou	dostatečný	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vilém	Vilém	k1gMnSc1
ale	ale	k9
některými	některý	k3yIgInPc7
členy	člen	k1gInPc7
kabinetu	kabinet	k1gInSc2
opovrhoval	opovrhovat	k5eAaImAgMnS
a	a	k8xC
považoval	považovat	k5eAaImAgMnS
je	být	k5eAaImIp3nS
za	za	k7c4
radikály	radikál	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
listopadu	listopad	k1gInSc6
zdědil	zdědit	k5eAaPmAgMnS
John	John	k1gMnSc1
Charles	Charles	k1gMnSc1
Spencer	Spencer	k1gMnSc1
<g/>
,	,	kIx,
mluvčí	mluvčí	k1gMnSc1
Dolní	dolní	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
a	a	k8xC
ministr	ministr	k1gMnSc1
financí	finance	k1gFnPc2
<g/>
,	,	kIx,
hodnost	hodnost	k1gFnSc4
šlechtice	šlechtic	k1gMnSc2
a	a	k8xC
přesunul	přesunout	k5eAaPmAgMnS
se	se	k3xPyFc4
z	z	k7c2
dolní	dolní	k2eAgFnSc2d1
komory	komora	k1gFnSc2
do	do	k7c2
Sněmovny	sněmovna	k1gFnSc2
lordů	lord	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lamb	Lamb	k1gMnSc1
musel	muset	k5eAaImAgMnS
navrhnout	navrhnout	k5eAaPmF
nového	nový	k2eAgMnSc4d1
mluvčího	mluvčí	k1gMnSc4
a	a	k8xC
ministra	ministr	k1gMnSc2
financí	finance	k1gFnPc2
<g/>
,	,	kIx,
protože	protože	k8xS
podle	podle	k7c2
dlouhodobé	dlouhodobý	k2eAgFnSc2d1
tradice	tradice	k1gFnSc2
byli	být	k5eAaImAgMnP
tito	tento	k3xDgMnPc1
funkcionáři	funkcionář	k1gMnPc1
jmenováni	jmenovat	k5eAaBmNgMnP,k5eAaImNgMnP
z	z	k7c2
členů	člen	k1gInPc2
dolní	dolní	k2eAgFnSc2d1
komory	komora	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jediným	jediný	k2eAgMnSc7d1
vhodným	vhodný	k2eAgMnSc7d1
kandidátem	kandidát	k1gMnSc7
byl	být	k5eAaImAgMnS
podle	podle	k7c2
Lamba	Lamb	k1gMnSc2
John	John	k1gMnSc1
Russel	Russel	k1gMnSc1
<g/>
,	,	kIx,
kterého	který	k3yRgMnSc4,k3yIgMnSc4,k3yQgMnSc4
on	on	k3xPp3gMnSc1
sám	sám	k3xTgMnSc1
(	(	kIx(
<g/>
a	a	k8xC
mnozí	mnohý	k2eAgMnPc1d1
jiní	jiný	k1gMnPc1
<g/>
)	)	kIx)
považovali	považovat	k5eAaImAgMnP
pro	pro	k7c4
jeho	jeho	k3xOp3gInPc4
radikální	radikální	k2eAgInPc4d1
postoje	postoj	k1gInPc4
za	za	k7c4
nepřijatelného	přijatelný	k2eNgMnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odvolal	odvolat	k5eAaPmAgMnS
proto	proto	k8xC
vládu	vláda	k1gFnSc4
a	a	k8xC
pověřil	pověřit	k5eAaPmAgMnS
jejím	její	k3xOp3gNnSc7
sestavením	sestavení	k1gNnSc7
Torye	tory	k1gMnPc4
Roberta	Robert	k1gMnSc4
Peela	Peel	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
ale	ale	k8xC
nemohl	moct	k5eNaImAgInS
sestavit	sestavit	k5eAaPmF
funkční	funkční	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
neměl	mít	k5eNaImAgInS
dostatečnou	dostatečný	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
v	v	k7c6
dolní	dolní	k2eAgFnSc6d1
komoře	komora	k1gFnSc6
a	a	k8xC
tak	tak	k6eAd1
byly	být	k5eAaImAgFnP
vyhlášeny	vyhlásit	k5eAaPmNgFnP
nové	nový	k2eAgFnPc1d1
volby	volba	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toryové	tory	k1gMnPc1
v	v	k7c6
nových	nový	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
získali	získat	k5eAaPmAgMnP
více	hodně	k6eAd2
křesel	křeslo	k1gNnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
stále	stále	k6eAd1
měli	mít	k5eAaImAgMnP
ve	v	k7c6
sněmovně	sněmovna	k1gFnSc6
menšinu	menšina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Peel	Peel	k1gInSc1
v	v	k7c6
úřadu	úřad	k1gInSc6
setrval	setrvat	k5eAaPmAgMnS
ještě	ještě	k9
několik	několik	k4yIc4
následujících	následující	k2eAgInPc2d1
měsíců	měsíc	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
po	po	k7c6
několika	několik	k4yIc6
porážkách	porážka	k1gFnPc6
rezignoval	rezignovat	k5eAaBmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novým	Nový	k1gMnSc7
premiérem	premiér	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Lamb	Lamb	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
v	v	k7c6
úřadu	úřad	k1gInSc6
setrval	setrvat	k5eAaPmAgMnS
až	až	k9
do	do	k7c2
Vilémovy	Vilémův	k2eAgFnSc2d1
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Následnictví	následnictví	k1gNnSc1
</s>
<s>
Vilém	Vilém	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
zemřel	zemřít	k5eAaPmAgMnS
na	na	k7c6
zástavu	zástav	k1gInSc6
srdce	srdce	k1gNnSc1
20	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1837	#num#	k4
na	na	k7c6
Windsorském	windsorský	k2eAgInSc6d1
hradu	hrad	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgMnS
také	také	k6eAd1
pohřben	pohřbít	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
nezanechal	zanechat	k5eNaPmAgMnS
žádné	žádný	k3yNgMnPc4
žijící	žijící	k2eAgMnPc4d1
legitimní	legitimní	k2eAgMnPc4d1
potomky	potomek	k1gMnPc4
<g/>
,	,	kIx,
stala	stát	k5eAaPmAgFnS
se	se	k3xPyFc4
následnicí	následnice	k1gFnSc7
na	na	k7c6
britském	britský	k2eAgInSc6d1
trůnu	trůn	k1gInSc6
princezna	princezna	k1gFnSc1
Viktorie	Viktorie	k1gFnSc1
z	z	k7c2
Kentu	Kent	k1gInSc2
<g/>
,	,	kIx,
jediná	jediný	k2eAgFnSc1d1
dcera	dcera	k1gFnSc1
jeho	jeho	k3xOp3gMnSc2,k3xPp3gMnSc2
bratra	bratr	k1gMnSc2
Eduarda	Eduard	k1gMnSc2
<g/>
,	,	kIx,
vévody	vévoda	k1gMnSc2
z	z	k7c2
Kentu	Kent	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hannoverské	hannoverský	k2eAgNnSc1d1
království	království	k1gNnSc1
ovšem	ovšem	k9
vyžadovalo	vyžadovat	k5eAaImAgNnS
následnictví	následnictví	k1gNnSc1
mužské	mužský	k2eAgFnSc2d1
linie	linie	k1gFnSc2
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnSc7
následníkem	následník	k1gMnSc7
v	v	k7c6
Hannoveru	Hannover	k1gInSc6
se	se	k3xPyFc4
tak	tak	k6eAd1
stal	stát	k5eAaPmAgMnS
jeho	jeho	k3xOp3gMnSc1
bratr	bratr	k1gMnSc1
Ernest	Ernest	k1gMnSc1
Augustus	Augustus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Vývod	vývod	k1gInSc1
z	z	k7c2
předků	předek	k1gInPc2
</s>
<s>
Jiří	Jiří	k1gMnSc1
I.	I.	kA
</s>
<s>
Jiří	Jiří	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Žofie	Žofie	k1gFnSc1
Dorotea	Dorote	k1gInSc2
z	z	k7c2
Celle	Celle	k1gFnSc2
</s>
<s>
Frederik	Frederik	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Hannoverský	hannoverský	k2eAgMnSc1d1
</s>
<s>
Jan	Jan	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
Braniborsko-Ansbašský	Braniborsko-Ansbašský	k2eAgMnSc1d1
</s>
<s>
Karolina	Karolinum	k1gNnPc1
z	z	k7c2
Ansbachu	Ansbach	k1gInSc2
</s>
<s>
Eleonora	Eleonora	k1gFnSc1
Sasko-Eisenašská	Sasko-Eisenašský	k2eAgFnSc1d1
</s>
<s>
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
I.	I.	kA
Sasko-Gothajsko-Altenburský	Sasko-Gothajsko-Altenburský	k2eAgInSc5d1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sasko-Gothajsko-Altenburský	Sasko-Gothajsko-Altenburský	k2eAgInSc1d1
</s>
<s>
Magdaléna	Magdaléna	k1gFnSc1
Sibyla	Sibyla	k1gFnSc1
Sasko-Weissenfelská	Sasko-Weissenfelský	k2eAgFnSc1d1
</s>
<s>
Augusta	Augusta	k1gMnSc1
Sasko-Gothajská	sasko-gothajský	k2eAgNnPc1d1
</s>
<s>
Karel	Karel	k1gMnSc1
Anhaltsko-Zerbstský	Anhaltsko-Zerbstský	k2eAgMnSc1d1
</s>
<s>
Magdalena	Magdalena	k1gFnSc1
Augusta	August	k1gMnSc2
Anhaltsko-Zerbstská	Anhaltsko-Zerbstský	k2eAgFnSc5d1
</s>
<s>
Žofie	Žofie	k1gFnSc1
Sasko-Weissenfelská	Sasko-Weissenfelský	k2eAgFnSc1d1
</s>
<s>
Vilém	Vilém	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britský	britský	k2eAgInSc1d1
</s>
<s>
Adolf	Adolf	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
I.	I.	kA
Meklenburský	meklenburský	k2eAgInSc4d1
</s>
<s>
Adolf	Adolf	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Meklenbursko-Střelický	Meklenbursko-Střelický	k2eAgInSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Kateřina	Kateřina	k1gFnSc1
Brunšvicko-Dannenberská	Brunšvicko-Dannenberský	k2eAgFnSc1d1
</s>
<s>
Karel	Karel	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
Meklenbursko-Střelický	Meklenbursko-Střelický	k2eAgMnSc1d1
</s>
<s>
Kristián	Kristián	k1gMnSc1
Vilém	Vilém	k1gMnSc1
I.	I.	kA
Schwarzbursko-Sondershausenský	Schwarzbursko-Sondershausenský	k2eAgInSc1d1
</s>
<s>
Christiana	Christian	k1gMnSc4
Emilie	Emilie	k1gFnSc2
Schwarzbursko-Sondershausenská	Schwarzbursko-Sondershausenský	k2eAgFnSc1d1
</s>
<s>
Antonie	Antonie	k1gFnSc1
Sibyla	Sibyla	k1gFnSc1
Barby-Mühlingenská	Barby-Mühlingenský	k2eAgFnSc1d1
</s>
<s>
Šarlota	Šarlota	k1gFnSc1
Meklenbursko-Střelická	Meklenbursko-Střelický	k2eAgFnSc1d1
</s>
<s>
Arnošt	Arnošt	k1gMnSc1
Sasko-Hildburghausenský	Sasko-Hildburghausenský	k2eAgMnSc1d1
</s>
<s>
Arnošt	Arnošt	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
I.	I.	kA
Sasko-Hildburghausenský	Sasko-Hildburghausenský	k2eAgMnSc1d1
</s>
<s>
Žofie	Žofie	k1gFnSc1
Henrieta	Henriet	k2eAgFnSc1d1
Waldecká	Waldecký	k2eAgFnSc1d1
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Sasko-Hildburghausenská	Sasko-Hildburghausenský	k2eAgFnSc1d1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
I.	I.	kA
Erbašský	Erbašský	k2eAgInSc4d1
</s>
<s>
Žofie	Žofie	k1gFnSc1
Albertina	Albertin	k2eAgFnSc1d1
z	z	k7c2
Erbachu	Erbach	k1gInSc2
</s>
<s>
Amálie	Amálie	k1gFnSc1
Kateřina	Kateřina	k1gFnSc1
Waldecko-Eisenberská	Waldecko-Eisenberský	k2eAgFnSc1d1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
William	William	k1gInSc1
IV	IV	kA
of	of	k?
the	the	k?
United	United	k1gInSc1
Kingdom	Kingdom	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Vilém	Viléma	k1gFnPc2
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britský	britský	k2eAgInSc4d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Osoba	osoba	k1gFnSc1
Vilém	Vilém	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anglický	anglický	k2eAgInSc4d1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Stručné	stručný	k2eAgInPc1d1
biografické	biografický	k2eAgInPc1d1
údaje	údaj	k1gInPc1
na	na	k7c4
www.thepeerage.com	www.thepeerage.com	k1gInSc4
</s>
<s>
Galerie	galerie	k1gFnSc1
Vilém	Vilém	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britský	britský	k2eAgInSc4d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Britští	britský	k2eAgMnPc1d1
panovníci	panovník	k1gMnPc1
Království	království	k1gNnSc1
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
(	(	kIx(
<g/>
1707	#num#	k4
<g/>
–	–	k?
<g/>
1801	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Anna	Anna	k1gFnSc1
Stuartovna	Stuartovna	k1gFnSc1
(	(	kIx(
<g/>
1702	#num#	k4
<g/>
–	–	k?
<g/>
1714	#num#	k4
<g/>
)	)	kIx)
<g/>
1	#num#	k4
</s>
<s>
Jiří	Jiří	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1714	#num#	k4
<g/>
–	–	k?
<g/>
1727	#num#	k4
<g/>
)	)	kIx)
<g/>
1	#num#	k4
</s>
<s>
Jiří	Jiří	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1727	#num#	k4
<g/>
–	–	k?
<g/>
1760	#num#	k4
<g/>
)	)	kIx)
<g/>
1	#num#	k4
</s>
<s>
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1760	#num#	k4
<g/>
–	–	k?
<g/>
1801	#num#	k4
<g/>
)	)	kIx)
<g/>
1	#num#	k4
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Irska	Irsko	k1gNnSc2
(	(	kIx(
<g/>
1801	#num#	k4
<g/>
–	–	k?
<g/>
1927	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1801	#num#	k4
<g/>
–	–	k?
<g/>
1820	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1820	#num#	k4
<g/>
–	–	k?
<g/>
1830	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vilém	Vilém	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britský	britský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1830	#num#	k4
<g/>
–	–	k?
<g/>
1837	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Viktorie	Viktorie	k1gFnSc1
(	(	kIx(
<g/>
1837	#num#	k4
<g/>
–	–	k?
<g/>
1901	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Eduard	Eduard	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1901	#num#	k4
<g/>
–	–	k?
<g/>
1910	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
V.	V.	kA
(	(	kIx(
<g/>
1910	#num#	k4
<g/>
–	–	k?
<g/>
1927	#num#	k4
<g/>
)	)	kIx)
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Severního	severní	k2eAgNnSc2d1
Irska	Irsko	k1gNnSc2
(	(	kIx(
<g/>
od	od	k7c2
r.	r.	kA
1927	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
V.	V.	kA
(	(	kIx(
<g/>
1927	#num#	k4
<g/>
–	–	k?
<g/>
1936	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Eduard	Eduard	k1gMnSc1
VIII	VIII	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
–	–	k?
<g/>
1952	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
od	od	k7c2
1952	#num#	k4
<g/>
)	)	kIx)
1	#num#	k4
<g/>
Rovněž	rovněž	k9
vládce	vládce	k1gMnSc2
Irska	Irsko	k1gNnSc2
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Jiří	Jiří	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s>
Hannoverský	hannoverský	k2eAgMnSc1d1
král	král	k1gMnSc1
1830	#num#	k4
–	–	k?
1837	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Arnošt	Arnošt	k1gMnSc1
August	August	k1gMnSc1
I.	I.	kA
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
118632906	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
6658	#num#	k4
031X	031X	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
50059902	#num#	k4
|	|	kIx~
ULAN	ULAN	kA
<g/>
:	:	kIx,
500373195	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
265993441	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
50059902	#num#	k4
</s>
