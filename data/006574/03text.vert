<s>
Anabolismus	anabolismus	k1gInSc1	anabolismus
je	být	k5eAaImIp3nS	být
soubor	soubor	k1gInSc4	soubor
syntetických	syntetický	k2eAgFnPc2d1	syntetická
reakcí	reakce	k1gFnPc2	reakce
(	(	kIx(	(
<g/>
asimilačních	asimilační	k2eAgFnPc2d1	asimilační
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yQgFnPc6	který
z	z	k7c2	z
látek	látka	k1gFnPc2	látka
jednodušších	jednoduchý	k2eAgFnPc2d2	jednodušší
vznikají	vznikat	k5eAaImIp3nP	vznikat
látky	látka	k1gFnPc1	látka
složitější	složitý	k2eAgFnPc1d2	složitější
(	(	kIx(	(
<g/>
stavební	stavební	k2eAgFnPc1d1	stavební
a	a	k8xC	a
zásobní	zásobní	k2eAgFnPc1d1	zásobní
látky	látka	k1gFnPc1	látka
–	–	k?	–
bílkoviny	bílkovina	k1gFnSc2	bílkovina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Energie	energie	k1gFnSc1	energie
se	se	k3xPyFc4	se
při	při	k7c6	při
těchto	tento	k3xDgFnPc6	tento
reakcích	reakce	k1gFnPc6	reakce
spotřebovává	spotřebovávat	k5eAaImIp3nS	spotřebovávat
(	(	kIx(	(
<g/>
endergonické	endergonický	k2eAgFnPc1d1	endergonický
reakce	reakce	k1gFnPc1	reakce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Anabolismus	anabolismus	k1gInSc1	anabolismus
převažuje	převažovat	k5eAaImIp3nS	převažovat
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
roste	růst	k5eAaImIp3nS	růst
a	a	k8xC	a
rozmnožuje	rozmnožovat	k5eAaImIp3nS	rozmnožovat
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Anabolismus	anabolismus	k1gInSc1	anabolismus
společně	společně	k6eAd1	společně
s	s	k7c7	s
katabolismem	katabolismus	k1gInSc7	katabolismus
tvoří	tvořit	k5eAaImIp3nP	tvořit
metabolismus	metabolismus	k1gInSc4	metabolismus
<g/>
.	.	kIx.	.
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
anabolismus	anabolismus	k1gInSc1	anabolismus
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
