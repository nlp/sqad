<s>
Anabolismus	anabolismus	k1gInSc1	anabolismus
je	být	k5eAaImIp3nS	být
soubor	soubor	k1gInSc4	soubor
syntetických	syntetický	k2eAgFnPc2d1	syntetická
reakcí	reakce	k1gFnPc2	reakce
(	(	kIx(	(
<g/>
asimilačních	asimilační	k2eAgFnPc2d1	asimilační
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yQgFnPc6	který
z	z	k7c2	z
látek	látka	k1gFnPc2	látka
jednodušších	jednoduchý	k2eAgFnPc2d2	jednodušší
vznikají	vznikat	k5eAaImIp3nP	vznikat
látky	látka	k1gFnPc1	látka
složitější	složitý	k2eAgFnPc1d2	složitější
(	(	kIx(	(
<g/>
stavební	stavební	k2eAgFnPc1d1	stavební
a	a	k8xC	a
zásobní	zásobní	k2eAgFnPc1d1	zásobní
látky	látka	k1gFnPc1	látka
–	–	k?	–
bílkoviny	bílkovina	k1gFnSc2	bílkovina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
