<s>
Prostějov	Prostějov	k1gInSc1	Prostějov
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Proßnitz	Proßnitz	k1gInSc1	Proßnitz
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
statutární	statutární	k2eAgMnSc1d1	statutární
město	město	k1gNnSc4	město
v	v	k7c6	v
Olomouckém	olomoucký	k2eAgInSc6d1	olomoucký
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
17	[number]	k4	17
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Olomouce	Olomouc	k1gFnSc2	Olomouc
<g/>
,	,	kIx,	,
na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
okraji	okraj	k1gInSc6	okraj
Hané	Haná	k1gFnSc2	Haná
<g/>
,	,	kIx,	,
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
Hornomoravského	hornomoravský	k2eAgInSc2d1	hornomoravský
úvalu	úval	k1gInSc2	úval
<g/>
,	,	kIx,	,
východně	východně	k6eAd1	východně
od	od	k7c2	od
Drahanské	Drahanský	k2eAgFnSc2d1	Drahanská
vrchoviny	vrchovina	k1gFnSc2	vrchovina
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
225	[number]	k4	225
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
,	,	kIx,	,
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
okraji	okraj	k1gInSc6	okraj
protékají	protékat	k5eAaImIp3nP	protékat
říčky	říčka	k1gFnSc2	říčka
Hloučela	Hloučela	k1gFnSc2	Hloučela
a	a	k8xC	a
Romže	Romž	k1gFnSc2	Romž
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přes	přes	k7c4	přes
44	[number]	k4	44
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgNnSc1d1	historické
jádro	jádro	k1gNnSc1	jádro
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
městskou	městský	k2eAgFnSc7d1	městská
památkovou	památkový	k2eAgFnSc7d1	památková
zónou	zóna	k1gFnSc7	zóna
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
historická	historický	k2eAgFnSc1d1	historická
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
vsi	ves	k1gFnSc6	ves
Prostějovice	Prostějovice	k1gFnSc2	Prostějovice
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1141	[number]	k4	1141
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
ve	v	k7c4	v
významnou	významný	k2eAgFnSc4d1	významná
trhovou	trhový	k2eAgFnSc4d1	trhová
ves	ves	k1gFnSc4	ves
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
sem	sem	k6eAd1	sem
byli	být	k5eAaImAgMnP	být
pozváni	pozván	k2eAgMnPc1d1	pozván
němečtí	německý	k2eAgMnPc1d1	německý
osadníci	osadník	k1gMnPc1	osadník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
dnešního	dnešní	k2eAgMnSc2d1	dnešní
nám.	nám.	k?	nám.
T.G.	T.G.	k1gMnSc2	T.G.
<g/>
Masaryka	Masaryk	k1gMnSc2	Masaryk
založili	založit	k5eAaPmAgMnP	založit
novou	nový	k2eAgFnSc4d1	nová
osadu	osada	k1gFnSc4	osada
<g/>
,	,	kIx,	,
na	na	k7c4	na
niž	jenž	k3xRgFnSc4	jenž
přešla	přejít	k5eAaPmAgNnP	přejít
práva	právo	k1gNnPc1	právo
osady	osada	k1gFnSc2	osada
původní	původní	k2eAgFnSc2d1	původní
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1390	[number]	k4	1390
bylo	být	k5eAaImAgNnS	být
Prostějovu	Prostějov	k1gInSc3	Prostějov
uděleno	udělit	k5eAaPmNgNnS	udělit
díky	díky	k7c3	díky
pánům	pan	k1gMnPc3	pan
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
právo	právo	k1gNnSc4	právo
výročního	výroční	k2eAgInSc2d1	výroční
trhu	trh	k1gInSc2	trh
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
fakticky	fakticky	k6eAd1	fakticky
stal	stát	k5eAaPmAgMnS	stát
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
husitském	husitský	k2eAgNnSc6d1	husitské
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
slibný	slibný	k2eAgInSc1d1	slibný
vývoj	vývoj	k1gInSc1	vývoj
zpomalil	zpomalit	k5eAaPmAgInS	zpomalit
<g/>
,	,	kIx,	,
když	když	k8xS	když
město	město	k1gNnSc1	město
utrpělo	utrpět	k5eAaPmAgNnS	utrpět
průtahy	průtah	k1gInPc1	průtah
obou	dva	k4xCgFnPc2	dva
nepřátelských	přátelský	k2eNgFnPc2d1	nepřátelská
stran	strana	k1gFnPc2	strana
<g/>
;	;	kIx,	;
nedostatečně	dostatečně	k6eNd1	dostatečně
opevněný	opevněný	k2eAgInSc1d1	opevněný
Prostějov	Prostějov	k1gInSc1	Prostějov
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
snadnou	snadný	k2eAgFnSc7d1	snadná
kořistí	kořist	k1gFnSc7	kořist
vojsk	vojsko	k1gNnPc2	vojsko
markraběte	markrabě	k1gMnSc2	markrabě
Albrechta	Albrecht	k1gMnSc2	Albrecht
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1431	[number]	k4	1431
byl	být	k5eAaImAgInS	být
vypálen	vypálit	k5eAaPmNgInS	vypálit
<g/>
.	.	kIx.	.
</s>
<s>
Prosperitu	prosperita	k1gFnSc4	prosperita
městu	město	k1gNnSc3	město
přinesl	přinést	k5eAaPmAgInS	přinést
vznik	vznik	k1gInSc1	vznik
židovského	židovský	k2eAgNnSc2d1	Židovské
města	město	k1gNnSc2	město
a	a	k8xC	a
především	především	k9	především
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1490	[number]	k4	1490
více	hodně	k6eAd2	hodně
než	než	k8xS	než
stoletá	stoletý	k2eAgFnSc1d1	stoletá
vláda	vláda	k1gFnSc1	vláda
rodů	rod	k1gInPc2	rod
Pernštejnů	Pernštejn	k1gInPc2	Pernštejn
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
majetkem	majetek	k1gInSc7	majetek
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
stalo	stát	k5eAaPmAgNnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1495	[number]	k4	1495
zahájilo	zahájit	k5eAaPmAgNnS	zahájit
město	město	k1gNnSc1	město
výstavbu	výstavba	k1gFnSc4	výstavba
kamenných	kamenný	k2eAgFnPc2d1	kamenná
hradeb	hradba	k1gFnPc2	hradba
se	s	k7c7	s
čtyřmi	čtyři	k4xCgFnPc7	čtyři
branami	brána	k1gFnPc7	brána
s	s	k7c7	s
baštami	bašta	k1gFnPc7	bašta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1521	[number]	k4	1521
až	až	k9	až
1538	[number]	k4	1538
si	se	k3xPyFc3	se
měšťané	měšťan	k1gMnPc1	měšťan
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
renesanční	renesanční	k2eAgFnSc4d1	renesanční
radnici	radnice	k1gFnSc4	radnice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
stává	stávat	k5eAaImIp3nS	stávat
majetkem	majetek	k1gInSc7	majetek
Lichtenštejnů	Lichtenštejn	k1gInPc2	Lichtenštejn
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
stagnaci	stagnace	k1gFnSc4	stagnace
rozvoje	rozvoj	k1gInSc2	rozvoj
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Prostějově	Prostějov	k1gInSc6	Prostějov
je	být	k5eAaImIp3nS	být
roku	rok	k1gInSc2	rok
1527	[number]	k4	1527
tiskárnou	tiskárna	k1gFnSc7	tiskárna
Kašpara	Kašpar	k1gMnSc2	Kašpar
Aorga	Aorg	k1gMnSc2	Aorg
vytištěna	vytištěn	k2eAgFnSc1d1	vytištěna
první	první	k4xOgFnSc1	první
kniha	kniha	k1gFnSc1	kniha
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zpustošení	zpustošení	k1gNnSc3	zpustošení
města	město	k1gNnSc2	město
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1697	[number]	k4	1697
vypukl	vypuknout	k5eAaPmAgInS	vypuknout
požár	požár	k1gInSc1	požár
<g/>
,	,	kIx,	,
kterému	který	k3yQgNnSc3	který
padly	padnout	k5eAaImAgFnP	padnout
za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
radnice	radnice	k1gFnSc2	radnice
<g/>
,	,	kIx,	,
škola	škola	k1gFnSc1	škola
i	i	k8xC	i
kostel	kostel	k1gInSc1	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
začíná	začínat	k5eAaImIp3nS	začínat
město	město	k1gNnSc4	město
dostávat	dostávat	k5eAaImF	dostávat
barokní	barokní	k2eAgInSc4d1	barokní
ráz	ráz	k1gInSc4	ráz
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
poloviny	polovina	k1gFnSc2	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
dochází	docházet	k5eAaImIp3nS	docházet
<g/>
,	,	kIx,	,
především	především	k9	především
zásluhou	zásluha	k1gFnSc7	zásluha
místních	místní	k2eAgMnPc2d1	místní
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
k	k	k7c3	k
prudkému	prudký	k2eAgInSc3d1	prudký
rozvoji	rozvoj	k1gInSc3	rozvoj
potravinářského	potravinářský	k2eAgInSc2d1	potravinářský
<g/>
,	,	kIx,	,
textilního	textilní	k2eAgInSc2d1	textilní
a	a	k8xC	a
oděvního	oděvní	k2eAgInSc2d1	oděvní
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1858	[number]	k4	1858
je	být	k5eAaImIp3nS	být
Prostějově	Prostějov	k1gInSc6	Prostějov
založena	založit	k5eAaPmNgFnS	založit
první	první	k4xOgFnSc1	první
česká	český	k2eAgFnSc1d1	Česká
konfekční	konfekční	k2eAgFnSc1d1	konfekční
továrna	továrna	k1gFnSc1	továrna
bratří	bratr	k1gMnPc2	bratr
Mandlů	mandl	k1gInPc2	mandl
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
přilákalo	přilákat	k5eAaPmAgNnS	přilákat
nové	nový	k2eAgMnPc4d1	nový
obyvatele	obyvatel	k1gMnPc4	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
Prostějov	Prostějov	k1gInSc1	Prostějov
spojen	spojit	k5eAaPmNgInS	spojit
železnicí	železnice	k1gFnSc7	železnice
s	s	k7c7	s
Brnem	Brno	k1gNnSc7	Brno
a	a	k8xC	a
Olomoucí	Olomouc	k1gFnSc7	Olomouc
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
změnilo	změnit	k5eAaPmAgNnS	změnit
tvář	tvář	k1gFnSc4	tvář
města	město	k1gNnSc2	město
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
historismu	historismus	k1gInSc2	historismus
a	a	k8xC	a
secese	secese	k1gFnSc2	secese
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
především	především	k9	především
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
dominující	dominující	k2eAgFnSc7d1	dominující
výstavbou	výstavba	k1gFnSc7	výstavba
stává	stávat	k5eAaImIp3nS	stávat
v	v	k7c6	v
Prostějově	Prostějov	k1gInSc6	Prostějov
funkcionalismus	funkcionalismus	k1gInSc1	funkcionalismus
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
60	[number]	k4	60
<g/>
.	.	kIx.	.
do	do	k7c2	do
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
stol	stol	k1gInSc4	stol
spolu	spolu	k6eAd1	spolu
soupeřili	soupeřit	k5eAaImAgMnP	soupeřit
čeští	český	k2eAgMnPc1d1	český
a	a	k8xC	a
němečtí	německý	k2eAgMnPc1d1	německý
představitelé	představitel	k1gMnPc1	představitel
o	o	k7c4	o
vedení	vedení	k1gNnSc4	vedení
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
bylo	být	k5eAaImAgNnS	být
německy	německy	k6eAd1	německy
mluvící	mluvící	k2eAgNnSc1d1	mluvící
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
v	v	k7c6	v
menšině	menšina	k1gFnSc6	menšina
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kuriovému	kuriový	k2eAgInSc3d1	kuriový
systému	systém	k1gInSc3	systém
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
preferoval	preferovat	k5eAaImAgInS	preferovat
bohatší	bohatý	k2eAgFnPc4d2	bohatší
vrstvy	vrstva	k1gFnPc4	vrstva
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
vedení	vedení	k1gNnSc1	vedení
města	město	k1gNnSc2	město
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1892	[number]	k4	1892
německé	německý	k2eAgInPc1d1	německý
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
starostou	starosta	k1gMnSc7	starosta
české	český	k2eAgFnSc2d1	Česká
národnosti	národnost	k1gFnSc2	národnost
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Karel	Karel	k1gMnSc1	Karel
Vojáček	Vojáček	k1gMnSc1	Vojáček
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
k	k	k7c3	k
hladovým	hladový	k2eAgFnPc3d1	hladová
bouřím	bouř	k1gFnPc3	bouř
před	před	k7c7	před
budovou	budova	k1gFnSc7	budova
Okresního	okresní	k2eAgNnSc2d1	okresní
hejtmanství	hejtmanství	k1gNnSc2	hejtmanství
<g/>
.	.	kIx.	.
</s>
<s>
Nezkušené	zkušený	k2eNgFnPc1d1	nezkušená
vojenské	vojenský	k2eAgFnPc1d1	vojenská
jednotky	jednotka	k1gFnPc1	jednotka
povolané	povolaný	k2eAgFnPc1d1	povolaná
z	z	k7c2	z
Olomouce	Olomouc	k1gFnSc2	Olomouc
nezvládly	zvládnout	k5eNaPmAgInP	zvládnout
situaci	situace	k1gFnSc4	situace
a	a	k8xC	a
začaly	začít	k5eAaPmAgFnP	začít
střílet	střílet	k5eAaImF	střílet
do	do	k7c2	do
davu	dav	k1gInSc2	dav
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
střelby	střelba	k1gFnSc2	střelba
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
23	[number]	k4	23
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
80	[number]	k4	80
bylo	být	k5eAaImAgNnS	být
zraněno	zranit	k5eAaPmNgNnS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
památku	památka	k1gFnSc4	památka
události	událost	k1gFnSc2	událost
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
postaven	postavit	k5eAaPmNgInS	postavit
Pomník	pomník	k1gInSc1	pomník
padlých	padlý	k1gMnPc2	padlý
hrdinů	hrdina	k1gMnPc2	hrdina
a	a	k8xC	a
celé	celý	k2eAgNnSc1d1	celé
náměstí	náměstí	k1gNnSc1	náměstí
bylo	být	k5eAaImAgNnS	být
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c6	na
Náměstí	náměstí	k1gNnSc6	náměstí
Padlých	padlý	k2eAgMnPc2d1	padlý
hrdinů	hrdina	k1gMnPc2	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
židovská	židovský	k2eAgFnSc1d1	židovská
komunita	komunita	k1gFnSc1	komunita
deportována	deportován	k2eAgFnSc1d1	deportována
do	do	k7c2	do
vyhlazovacích	vyhlazovací	k2eAgInPc2d1	vyhlazovací
táborů	tábor	k1gInPc2	tábor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
většina	většina	k1gFnSc1	většina
(	(	kIx(	(
<g/>
celkem	celkem	k6eAd1	celkem
1227	[number]	k4	1227
osob	osoba	k1gFnPc2	osoba
<g/>
)	)	kIx)	)
zahynula	zahynout	k5eAaPmAgFnS	zahynout
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1953	[number]	k4	1953
po	po	k7c6	po
půlnoci	půlnoc	k1gFnSc6	půlnoc
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
hlavního	hlavní	k2eAgNnSc2d1	hlavní
náměstí	náměstí	k1gNnSc2	náměstí
odstraněna	odstraněn	k2eAgFnSc1d1	odstraněna
socha	socha	k1gFnSc1	socha
Tomáše	Tomáš	k1gMnSc2	Tomáš
Garrigue	Garrigu	k1gMnSc2	Garrigu
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
čin	čin	k1gInSc1	čin
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
vlnu	vlna	k1gFnSc4	vlna
nesouhlasu	nesouhlas	k1gInSc2	nesouhlas
a	a	k8xC	a
odpoledne	odpoledne	k1gNnSc2	odpoledne
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
demonstrace	demonstrace	k1gFnSc1	demonstrace
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
města	město	k1gNnSc2	město
byly	být	k5eAaImAgFnP	být
povolány	povolán	k2eAgFnPc1d1	povolána
policejní	policejní	k2eAgFnPc1d1	policejní
jednotky	jednotka	k1gFnPc1	jednotka
a	a	k8xC	a
Lidové	lidový	k2eAgFnPc1d1	lidová
milice	milice	k1gFnPc1	milice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
demonstrací	demonstrace	k1gFnSc7	demonstrace
bylo	být	k5eAaImAgNnS	být
vyslýcháno	vyslýchat	k5eAaImNgNnS	vyslýchat
90	[number]	k4	90
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
43	[number]	k4	43
posláno	poslat	k5eAaPmNgNnS	poslat
před	před	k7c4	před
soud	soud	k1gInSc4	soud
a	a	k8xC	a
20	[number]	k4	20
osob	osoba	k1gFnPc2	osoba
bylo	být	k5eAaImAgNnS	být
odsouzeno	odsouzet	k5eAaImNgNnS	odsouzet
k	k	k7c3	k
odnětí	odnětí	k1gNnSc3	odnětí
svobody	svoboda	k1gFnSc2	svoboda
až	až	k9	až
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
1,5	[number]	k4	1,5
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
pozdějšího	pozdní	k2eAgNnSc2d2	pozdější
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
se	se	k3xPyFc4	se
na	na	k7c6	na
odstranění	odstranění	k1gNnSc6	odstranění
sochy	socha	k1gFnSc2	socha
podílel	podílet	k5eAaImAgMnS	podílet
předseda	předseda	k1gMnSc1	předseda
ONV	ONV	kA	ONV
František	František	k1gMnSc1	František
Ján	Ján	k1gMnSc1	Ján
<g/>
,	,	kIx,	,
důstojníci	důstojník	k1gMnPc1	důstojník
leteckého	letecký	k2eAgNnSc2d1	letecké
učiliště	učiliště	k1gNnSc2	učiliště
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Františkem	František	k1gMnSc7	František
Bernátem	Bernát	k1gMnSc7	Bernát
a	a	k8xC	a
několik	několik	k4yIc1	několik
příslušníků	příslušník	k1gMnPc2	příslušník
StB	StB	k1gFnPc2	StB
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
dní	den	k1gInPc2	den
po	po	k7c6	po
začátku	začátek	k1gInSc6	začátek
okupace	okupace	k1gFnSc2	okupace
vojsky	vojsky	k6eAd1	vojsky
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
došlo	dojít	k5eAaPmAgNnS	dojít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
ke	k	k7c3	k
střelbě	střelba	k1gFnSc3	střelba
<g/>
.	.	kIx.	.
</s>
<s>
Sovětští	sovětský	k2eAgMnPc1d1	sovětský
vojáci	voják	k1gMnPc1	voják
začali	začít	k5eAaPmAgMnP	začít
střílet	střílet	k5eAaImF	střílet
a	a	k8xC	a
zastřelili	zastřelit	k5eAaPmAgMnP	zastřelit
tři	tři	k4xCgMnPc4	tři
lidi	člověk	k1gMnPc4	člověk
a	a	k8xC	a
množství	množství	k1gNnSc4	množství
dalších	další	k2eAgMnPc2d1	další
lidí	člověk	k1gMnPc2	člověk
bylo	být	k5eAaImAgNnS	být
zraněno	zranit	k5eAaPmNgNnS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
vojáků	voják	k1gMnPc2	voják
nikdy	nikdy	k6eAd1	nikdy
neuvedli	uvést	k5eNaPmAgMnP	uvést
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
incident	incident	k1gInSc4	incident
zodpovědný	zodpovědný	k2eAgMnSc1d1	zodpovědný
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dochovaným	dochovaný	k2eAgFnPc3d1	dochovaná
památkám	památka	k1gFnPc3	památka
patří	patřit	k5eAaImIp3nS	patřit
bašta	bašta	k6eAd1	bašta
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
renesanční	renesanční	k2eAgFnSc2d1	renesanční
radnice	radnice	k1gFnSc2	radnice
ze	z	k7c2	z
století	století	k1gNnSc2	století
šestnáctého	šestnáctý	k4xOgMnSc2	šestnáctý
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
sloužící	sloužící	k2eAgMnSc1d1	sloužící
jako	jako	k8xC	jako
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Dominantou	dominanta	k1gFnSc7	dominanta
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
nová	nový	k2eAgFnSc1d1	nová
radnice	radnice	k1gFnSc1	radnice
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
s	s	k7c7	s
věží	věž	k1gFnSc7	věž
vysokou	vysoká	k1gFnSc7	vysoká
66	[number]	k4	66
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
orlojem	orloj	k1gInSc7	orloj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Prostějově	Prostějov	k1gInSc6	Prostějov
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
významný	významný	k2eAgMnSc1d1	významný
český	český	k2eAgMnSc1d1	český
vědec	vědec	k1gMnSc1	vědec
Otto	Otto	k1gMnSc1	Otto
Wichterle	Wichterle	k1gFnSc1	Wichterle
<g/>
,	,	kIx,	,
gotický	gotický	k2eAgMnSc1d1	gotický
stavitel	stavitel	k1gMnSc1	stavitel
Matěj	Matěj	k1gMnSc1	Matěj
Rejsek	rejsek	k1gMnSc1	rejsek
<g/>
,	,	kIx,	,
filozof	filozof	k1gMnSc1	filozof
Edmund	Edmund	k1gMnSc1	Edmund
Husserl	Husserl	k1gMnSc1	Husserl
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
Alois	Alois	k1gMnSc1	Alois
Fišárek	Fišárek	k1gMnSc1	Fišárek
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
Jiří	Jiří	k1gMnSc1	Jiří
Wolker	Wolker	k1gMnSc1	Wolker
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Prostějově	Prostějov	k1gInSc6	Prostějov
působil	působit	k5eAaImAgMnS	působit
farář	farář	k1gMnSc1	farář
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
katolické	katolický	k2eAgFnSc2d1	katolická
moderny	moderna	k1gFnSc2	moderna
Karel	Karel	k1gMnSc1	Karel
Dostál-Lutinov	Dostál-Lutinov	k1gInSc1	Dostál-Lutinov
<g/>
.	.	kIx.	.
</s>
<s>
Starostou	Starosta	k1gMnSc7	Starosta
města	město	k1gNnSc2	město
a	a	k8xC	a
gymnaziálním	gymnaziální	k2eAgMnSc7d1	gymnaziální
učitelem	učitel	k1gMnSc7	učitel
tu	tu	k6eAd1	tu
byl	být	k5eAaImAgMnS	být
Jan	Jan	k1gMnSc1	Jan
Sedláček	Sedláček	k1gMnSc1	Sedláček
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
československého	československý	k2eAgInSc2d1	československý
parlamentu	parlament	k1gInSc2	parlament
za	za	k7c4	za
Národní	národní	k2eAgNnSc4d1	národní
sjednocení	sjednocení	k1gNnSc4	sjednocení
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
byl	být	k5eAaImAgInS	být
Prostějov	Prostějov	k1gInSc4	Prostějov
sídlem	sídlo	k1gNnSc7	sídlo
okresního	okresní	k2eAgInSc2d1	okresní
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
je	být	k5eAaImIp3nS	být
statutárním	statutární	k2eAgNnSc7d1	statutární
městem	město	k1gNnSc7	město
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
primátorem	primátor	k1gMnSc7	primátor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Prostějově	Prostějov	k1gInSc6	Prostějov
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
významný	významný	k2eAgInSc4d1	významný
strojírenský	strojírenský	k2eAgInSc4d1	strojírenský
a	a	k8xC	a
potravinářský	potravinářský	k2eAgInSc4d1	potravinářský
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Prostějově	Prostějov	k1gInSc6	Prostějov
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
významná	významný	k2eAgFnSc1d1	významná
židovská	židovský	k2eAgFnSc1d1	židovská
komunita	komunita	k1gFnSc1	komunita
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
druhou	druhý	k4xOgFnSc7	druhý
nejpočetnější	početní	k2eAgFnSc7d3	nejpočetnější
židovskou	židovský	k2eAgFnSc7d1	židovská
komunitou	komunita	k1gFnSc7	komunita
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
Prostějově	Prostějov	k1gInSc6	Prostějov
1680	[number]	k4	1680
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
byl	být	k5eAaImAgInS	být
24000	[number]	k4	24000
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Prostějově	Prostějov	k1gInSc6	Prostějov
působili	působit	k5eAaImAgMnP	působit
tito	tento	k3xDgMnPc1	tento
rabíni	rabín	k1gMnPc1	rabín
<g/>
:	:	kIx,	:
Geršon	Geršon	k1gMnSc1	Geršon
Aškenazi	Aškenaze	k1gFnSc4	Aškenaze
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1650	[number]	k4	1650
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Meir	Meir	k1gMnSc1	Meir
Eisenstadt	Eisenstadt	k1gMnSc1	Eisenstadt
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1700	[number]	k4	1700
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Nehemias	Nehemias	k1gMnSc1	Nehemias
Trebitsch	Trebitsch	k1gMnSc1	Trebitsch
(	(	kIx(	(
<g/>
1825	[number]	k4	1825
<g/>
-	-	kIx~	-
<g/>
1830	[number]	k4	1830
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Löw	Löw	k1gMnSc1	Löw
Schwab	Schwab	k1gMnSc1	Schwab
(	(	kIx(	(
<g/>
1830	[number]	k4	1830
<g/>
-	-	kIx~	-
<g/>
1836	[number]	k4	1836
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hirsch	Hirsch	k1gMnSc1	Hirsch
Fassel	Fassel	k1gMnSc1	Fassel
(	(	kIx(	(
<g/>
1836	[number]	k4	1836
<g/>
-	-	kIx~	-
<g/>
1853	[number]	k4	1853
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Adolf	Adolf	k1gMnSc1	Adolf
Schmiedl	Schmiedl	k1gMnSc1	Schmiedl
(	(	kIx(	(
<g/>
1853	[number]	k4	1853
<g/>
-	-	kIx~	-
<g/>
1869	[number]	k4	1869
<g/>
)	)	kIx)	)
a	a	k8xC	a
Emil	Emil	k1gMnSc1	Emil
Hoff	Hoff	k1gMnSc1	Hoff
(	(	kIx(	(
<g/>
1870	[number]	k4	1870
<g/>
-	-	kIx~	-
<g/>
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Demolici	demolice	k1gFnSc4	demolice
historické	historický	k2eAgFnSc2d1	historická
části	část	k1gFnSc2	část
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
kterému	který	k3yQgInSc3	který
se	se	k3xPyFc4	se
přezdívalo	přezdívat	k5eAaImAgNnS	přezdívat
Hanácký	hanácký	k2eAgInSc4d1	hanácký
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
potažmo	potažmo	k6eAd1	potažmo
přezdívka	přezdívka	k1gFnSc1	přezdívka
celému	celý	k2eAgInSc3d1	celý
Prostějovu	Prostějov	k1gInSc3	Prostějov
<g/>
)	)	kIx)	)
popisuje	popisovat	k5eAaImIp3nS	popisovat
i	i	k9	i
film	film	k1gInSc4	film
Zámek	zámek	k1gInSc1	zámek
Nekonečno	nekonečno	k1gNnSc1	nekonečno
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
s	s	k7c7	s
Pavlem	Pavel	k1gMnSc7	Pavel
Křížem	Kříž	k1gMnSc7	Kříž
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
(	(	kIx(	(
<g/>
dostupný	dostupný	k2eAgInSc1d1	dostupný
na	na	k7c6	na
youtube	youtubat	k5eAaPmIp3nS	youtubat
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
uváděn	uvádět	k5eAaImNgInS	uvádět
podle	podle	k7c2	podle
výsledků	výsledek	k1gInPc2	výsledek
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
za	za	k7c4	za
Prostějov	Prostějov	k1gInSc4	Prostějov
včetně	včetně	k7c2	včetně
těch	ten	k3xDgFnPc2	ten
místních	místní	k2eAgFnPc2d1	místní
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
v	v	k7c6	v
konkrétní	konkrétní	k2eAgFnSc6d1	konkrétní
době	doba	k1gFnSc6	doba
patřily	patřit	k5eAaImAgInP	patřit
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
představitelů	představitel	k1gMnPc2	představitel
Prostějova	Prostějov	k1gInSc2	Prostějov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
primátor	primátor	k1gMnSc1	primátor
<g/>
.	.	kIx.	.
</s>
<s>
Zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
má	mít	k5eAaImIp3nS	mít
35	[number]	k4	35
členů	člen	k1gInPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
má	mít	k5eAaImIp3nS	mít
podle	podle	k7c2	podle
dohody	dohoda	k1gFnSc2	dohoda
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
jedenáct	jedenáct	k4xCc4	jedenáct
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
byl	být	k5eAaImAgInS	být
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
zvolen	zvolit	k5eAaPmNgInS	zvolit
primátorem	primátor	k1gMnSc7	primátor
města	město	k1gNnSc2	město
Miroslav	Miroslav	k1gMnSc1	Miroslav
Pišťák	Pišťák	k1gMnSc1	Pišťák
z	z	k7c2	z
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
poprvé	poprvé	k6eAd1	poprvé
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
samosprávy	samospráva	k1gFnSc2	samospráva
již	již	k9	již
po	po	k7c6	po
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k9	ještě
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
starosty	starosta	k1gMnSc2	starosta
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
jako	jako	k8xC	jako
primátor	primátor	k1gMnSc1	primátor
statutárního	statutární	k2eAgNnSc2d1	statutární
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
rezignoval	rezignovat	k5eAaBmAgInS	rezignovat
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
spolustraničkou	spolustranička	k1gFnSc7	spolustranička
Alenou	Alena	k1gFnSc7	Alena
Raškovou	Rašková	k1gFnSc7	Rašková
<g/>
.	.	kIx.	.
</s>
<s>
Radu	rada	k1gFnSc4	rada
města	město	k1gNnSc2	město
tvoří	tvořit	k5eAaImIp3nP	tvořit
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
zástupci	zástupce	k1gMnPc1	zástupce
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
KSČM	KSČM	kA	KSČM
a	a	k8xC	a
hnutí	hnutí	k1gNnSc2	hnutí
PéVéčko	PéVéčko	k6eAd1	PéVéčko
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
vyhláška	vyhláška	k1gFnSc1	vyhláška
č.	č.	k?	č.
18	[number]	k4	18
<g/>
/	/	kIx~	/
<g/>
1994	[number]	k4	1994
(	(	kIx(	(
<g/>
zrušená	zrušený	k2eAgFnSc1d1	zrušená
vyhláškou	vyhláška	k1gFnSc7	vyhláška
č.	č.	k?	č.
5	[number]	k4	5
<g/>
/	/	kIx~	/
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
popisuje	popisovat	k5eAaImIp3nS	popisovat
městský	městský	k2eAgInSc1d1	městský
prapor	prapor	k1gInSc1	prapor
a	a	k8xC	a
znak	znak	k1gInSc1	znak
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
Městský	městský	k2eAgInSc1d1	městský
prapor	prapor	k1gInSc1	prapor
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
obdélníku	obdélník	k1gInSc2	obdélník
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
stran	strana	k1gFnPc2	strana
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
svisle	svisle	k6eAd1	svisle
dělený	dělený	k2eAgMnSc1d1	dělený
na	na	k7c4	na
pravou	pravý	k2eAgFnSc4d1	pravá
modrou	modrý	k2eAgFnSc4d1	modrá
a	a	k8xC	a
levou	levý	k2eAgFnSc4d1	levá
žlutou	žlutý	k2eAgFnSc4d1	žlutá
část	část	k1gFnSc4	část
<g/>
,	,	kIx,	,
uprostřed	uprostřed	k7c2	uprostřed
na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
barevných	barevný	k2eAgNnPc2d1	barevné
polí	pole	k1gNnPc2	pole
je	být	k5eAaImIp3nS	být
znak	znak	k1gInSc1	znak
města	město	k1gNnSc2	město
ve	v	k7c6	v
španělském	španělský	k2eAgInSc6d1	španělský
štítu	štít	k1gInSc6	štít
<g/>
.	.	kIx.	.
</s>
<s>
Městský	městský	k2eAgInSc1d1	městský
prapor	prapor	k1gInSc1	prapor
se	se	k3xPyFc4	se
umísťuje	umísťovat	k5eAaImIp3nS	umísťovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
modré	modrý	k2eAgNnSc1d1	modré
pole	pole	k1gNnSc1	pole
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
vpravo	vpravo	k6eAd1	vpravo
nebo	nebo	k8xC	nebo
nahoře	nahoře	k6eAd1	nahoře
<g/>
.	.	kIx.	.
</s>
<s>
Městský	městský	k2eAgInSc1d1	městský
znak	znak	k1gInSc1	znak
představuje	představovat	k5eAaImIp3nS	představovat
štít	štít	k1gInSc1	štít
svisle	svisle	k6eAd1	svisle
půlený	půlený	k2eAgInSc1d1	půlený
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pravé	pravý	k2eAgFnSc6d1	pravá
zlaté	zlatý	k2eAgFnSc6d1	zlatá
polovině	polovina	k1gFnSc6	polovina
je	být	k5eAaImIp3nS	být
červená	červený	k2eAgFnSc1d1	červená
mříž	mříž	k1gFnSc1	mříž
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
kosmých	kosmý	k2eAgFnPc2d1	kosmý
a	a	k8xC	a
pěti	pět	k4xCc2	pět
šikmých	šikmý	k2eAgInPc2d1	šikmý
pásů	pás	k1gInPc2	pás
se	s	k7c7	s
zlatými	zlatý	k2eAgInPc7d1	zlatý
hřeby	hřeb	k1gInPc7	hřeb
na	na	k7c6	na
křížení	křížení	k1gNnSc6	křížení
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
šesticípých	šesticípý	k2eAgFnPc2d1	šesticípá
hvězdiček	hvězdička	k1gFnPc2	hvězdička
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
levé	levý	k2eAgFnSc6d1	levá
modré	modrý	k2eAgFnSc6d1	modrá
polovině	polovina	k1gFnSc6	polovina
je	být	k5eAaImIp3nS	být
půl	půl	k6eAd1	půl
černé	černý	k2eAgFnPc4d1	černá
nekorunované	korunovaný	k2eNgFnPc4d1	nekorunovaná
orlice	orlice	k1gFnPc4	orlice
se	s	k7c7	s
zlatými	zlatý	k2eAgInPc7d1	zlatý
drápy	dráp	k1gInPc7	dráp
<g/>
,	,	kIx,	,
zobákem	zobák	k1gInSc7	zobák
a	a	k8xC	a
červeným	červený	k2eAgInSc7d1	červený
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
hledící	hledící	k2eAgMnSc1d1	hledící
doleva	doleva	k6eAd1	doleva
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Prostějově	Prostějov	k1gInSc6	Prostějov
<g/>
.	.	kIx.	.
</s>
<s>
Centrem	centr	k1gInSc7	centr
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
dnes	dnes	k6eAd1	dnes
nese	nést	k5eAaImIp3nS	nést
jméno	jméno	k1gNnSc4	jméno
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
,	,	kIx,	,
s	s	k7c7	s
dominantou	dominanta	k1gFnSc7	dominanta
Nové	Nové	k2eAgFnSc2d1	Nové
radnice	radnice	k1gFnSc2	radnice
s	s	k7c7	s
66	[number]	k4	66
m	m	kA	m
vysokou	vysoký	k2eAgFnSc7d1	vysoká
věží	věž	k1gFnSc7	věž
a	a	k8xC	a
orlojem	orloj	k1gInSc7	orloj
<g/>
,	,	kIx,	,
další	další	k2eAgFnSc7d1	další
dominantou	dominanta	k1gFnSc7	dominanta
je	být	k5eAaImIp3nS	být
barokní	barokní	k2eAgInSc1d1	barokní
morový	morový	k2eAgInSc1d1	morový
sloup	sloup	k1gInSc1	sloup
se	s	k7c7	s
sochou	socha	k1gFnSc7	socha
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1714	[number]	k4	1714
<g/>
.	.	kIx.	.
</s>
<s>
Domy	dům	k1gInPc1	dům
lemující	lemující	k2eAgInPc1d1	lemující
náměstí	náměstí	k1gNnSc4	náměstí
měly	mít	k5eAaImAgFnP	mít
původně	původně	k6eAd1	původně
svá	svůj	k3xOyFgNnPc4	svůj
domovní	domovní	k2eAgNnPc4d1	domovní
znamení	znamení	k1gNnPc4	znamení
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některá	některý	k3yIgFnSc1	některý
se	se	k3xPyFc4	se
dochovala	dochovat	k5eAaPmAgFnS	dochovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgInSc1d1	národní
dům	dům	k1gInSc1	dům
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
české	český	k2eAgFnSc2d1	Česká
secese	secese	k1gFnSc2	secese
od	od	k7c2	od
architekta	architekt	k1gMnSc2	architekt
Jana	Jan	k1gMnSc2	Jan
Kotěry	Kotěra	k1gFnSc2	Kotěra
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1905	[number]	k4	1905
<g/>
-	-	kIx~	-
<g/>
1907	[number]	k4	1907
<g/>
.	.	kIx.	.
</s>
<s>
Stavitelem	stavitel	k1gMnSc7	stavitel
byl	být	k5eAaImAgMnS	být
Otakar	Otakar	k1gMnSc1	Otakar
Pokorný	Pokorný	k1gMnSc1	Pokorný
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
zároveň	zároveň	k6eAd1	zároveň
o	o	k7c4	o
spolkový	spolkový	k2eAgInSc4d1	spolkový
dům	dům	k1gInSc4	dům
<g/>
,	,	kIx,	,
městské	městský	k2eAgNnSc4d1	Městské
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
,	,	kIx,	,
restauraci	restaurace	k1gFnSc4	restaurace
a	a	k8xC	a
kavárnu	kavárna	k1gFnSc4	kavárna
<g/>
.	.	kIx.	.
</s>
<s>
Architekt	architekt	k1gMnSc1	architekt
sám	sám	k3xTgMnSc1	sám
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
nábytek	nábytek	k1gInSc4	nábytek
<g/>
,	,	kIx,	,
obložení	obložení	k1gNnPc1	obložení
<g/>
,	,	kIx,	,
osvětlovací	osvětlovací	k2eAgNnPc1d1	osvětlovací
tělesa	těleso	k1gNnPc1	těleso
a	a	k8xC	a
vitráže	vitráž	k1gFnPc1	vitráž
<g/>
.	.	kIx.	.
</s>
<s>
Výzdobu	výzdoba	k1gFnSc4	výzdoba
interiéru	interiér	k1gInSc2	interiér
provedli	provést	k5eAaPmAgMnP	provést
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
také	také	k9	také
malíři	malíř	k1gMnPc1	malíř
František	František	k1gMnSc1	František
Kysela	Kysela	k1gMnSc1	Kysela
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
Petr	Petr	k1gMnSc1	Petr
a	a	k8xC	a
sochař	sochař	k1gMnSc1	sochař
Stanislav	Stanislav	k1gMnSc1	Stanislav
Sucharda	Sucharda	k1gMnSc1	Sucharda
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
německé	německý	k2eAgFnSc2d1	německá
okupace	okupace	k1gFnSc2	okupace
byl	být	k5eAaImAgInS	být
interiér	interiér	k1gInSc1	interiér
částečně	částečně	k6eAd1	částečně
poškozen	poškodit	k5eAaPmNgInS	poškodit
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
radnice	radnice	k1gFnSc1	radnice
-	-	kIx~	-
sídlo	sídlo	k1gNnSc1	sídlo
Městského	městský	k2eAgInSc2d1	městský
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
navržena	navržen	k2eAgFnSc1d1	navržena
a	a	k8xC	a
postavena	postaven	k2eAgFnSc1d1	postavena
v	v	k7c6	v
secesním	secesní	k2eAgInSc6d1	secesní
stylu	styl	k1gInSc6	styl
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1909	[number]	k4	1909
<g/>
-	-	kIx~	-
<g/>
1914	[number]	k4	1914
podle	podle	k7c2	podle
projektu	projekt	k1gInSc2	projekt
profesora	profesor	k1gMnSc2	profesor
Karla	Karel	k1gMnSc2	Karel
Huga	Hugo	k1gMnSc2	Hugo
Kepky	Kepka	k1gMnSc2	Kepka
<g/>
,	,	kIx,	,
s	s	k7c7	s
66	[number]	k4	66
m	m	kA	m
vysokou	vysoký	k2eAgFnSc7d1	vysoká
věží	věž	k1gFnSc7	věž
a	a	k8xC	a
bohatě	bohatě	k6eAd1	bohatě
zdobenými	zdobený	k2eAgInPc7d1	zdobený
interiéry	interiér	k1gInPc7	interiér
<g/>
,	,	kIx,	,
projektované	projektovaný	k2eAgNnSc1d1	projektované
pravé	pravý	k2eAgNnSc1d1	pravé
křídlo	křídlo	k1gNnSc1	křídlo
nebylo	být	k5eNaImAgNnS	být
realizováno	realizovat	k5eAaBmNgNnS	realizovat
<g/>
.	.	kIx.	.
</s>
<s>
Stará	starý	k2eAgFnSc1d1	stará
radnice	radnice	k1gFnSc1	radnice
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
portál	portál	k1gInSc1	portál
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1539	[number]	k4	1539
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1697	[number]	k4	1697
vyhořela	vyhořet	k5eAaPmAgFnS	vyhořet
<g/>
.	.	kIx.	.
</s>
<s>
Barokní	barokní	k2eAgFnSc4d1	barokní
lodžii	lodžie	k1gFnSc4	lodžie
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
Giovanni	Giovann	k1gMnPc1	Giovann
Pietro	Pietro	k1gNnSc4	Pietro
Tencalla	Tencallo	k1gNnSc2	Tencallo
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
radnice	radnice	k1gFnSc1	radnice
sloužila	sloužit	k5eAaImAgFnS	sloužit
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
jako	jako	k8xC	jako
sídlo	sídlo	k1gNnSc4	sídlo
Muzea	muzeum	k1gNnSc2	muzeum
Prostějovska	Prostějovsko	k1gNnSc2	Prostějovsko
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
přistavěno	přistavět	k5eAaPmNgNnS	přistavět
patro	patro	k1gNnSc4	patro
výstavních	výstavní	k2eAgFnPc2d1	výstavní
prostor	prostora	k1gFnPc2	prostora
za	za	k7c7	za
slepou	slepý	k2eAgFnSc7d1	slepá
atikou	atika	k1gFnSc7	atika
<g/>
.	.	kIx.	.
</s>
<s>
Atika	Atika	k1gFnSc1	Atika
byla	být	k5eAaImAgFnS	být
obnovena	obnoven	k2eAgFnSc1d1	obnovena
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
Zámek	zámek	k1gInSc1	zámek
Prostějov	Prostějov	k1gInSc1	Prostějov
-	-	kIx~	-
pseudorenesanční	pseudorenesanční	k2eAgNnPc1d1	pseudorenesanční
sgrafita	sgrafito	k1gNnPc1	sgrafito
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
1900	[number]	k4	1900
<g/>
-	-	kIx~	-
<g/>
1901	[number]	k4	1901
Jano	Jano	k1gMnSc1	Jano
Köhler	Köhler	k1gMnSc1	Köhler
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Švandera	Švandero	k1gNnSc2	Švandero
<g/>
,	,	kIx,	,
Arnošt	Arnošt	k1gMnSc1	Arnošt
Podloudek	Podloudek	k1gMnSc1	Podloudek
a	a	k8xC	a
Bohumil	Bohumil	k1gMnSc1	Bohumil
Kazda	Kazda	k1gMnSc1	Kazda
Městské	městský	k2eAgFnSc2d1	městská
hradby	hradba	k1gFnSc2	hradba
s	s	k7c7	s
baštou	bašta	k1gFnSc7	bašta
Špalíček	špalíček	k1gInSc4	špalíček
-	-	kIx~	-
bývalé	bývalý	k2eAgNnSc4d1	bývalé
židovské	židovský	k2eAgNnSc4d1	Židovské
ghetto	ghetto	k1gNnSc4	ghetto
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
škola	škola	k1gFnSc1	škola
Vila	vít	k5eAaImAgFnS	vít
Františka	František	k1gMnSc4	František
Kováříka	kovářík	k1gMnSc4	kovářík
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Padlých	padlý	k2eAgMnPc2d1	padlý
Hrdinů	Hrdina	k1gMnPc2	Hrdina
5	[number]	k4	5
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1910	[number]	k4	1910
<g/>
-	-	kIx~	-
<g/>
1911	[number]	k4	1911
<g />
.	.	kIx.	.
</s>
<s>
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
wagneriánské	wagneriánský	k2eAgFnSc2d1	wagneriánský
moderny	moderna	k1gFnSc2	moderna
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
<g/>
:	:	kIx,	:
Emil	Emil	k1gMnSc1	Emil
Králík	Králík	k1gMnSc1	Králík
<g/>
,	,	kIx,	,
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
<g/>
,	,	kIx,	,
veřejnosti	veřejnost	k1gFnPc1	veřejnost
nepřístupné	přístupný	k2eNgFnPc1d1	nepřístupná
(	(	kIx(	(
<g/>
obrázek	obrázek	k1gInSc1	obrázek
<g/>
)	)	kIx)	)
Vila	vila	k1gFnSc1	vila
Josefa	Josef	k1gMnSc2	Josef
Kováříka	kovářík	k1gMnSc2	kovářík
na	na	k7c6	na
Vojáčkově	Vojáčkův	k2eAgNnSc6d1	Vojáčkovo
náměstí	náměstí	k1gNnSc6	náměstí
č.	č.	k?	č.
5	[number]	k4	5
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1911	[number]	k4	1911
<g/>
-	-	kIx~	-
<g/>
1912	[number]	k4	1912
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
moderny	moderna	k1gFnSc2	moderna
a	a	k8xC	a
secese	secese	k1gFnSc2	secese
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
<g/>
:	:	kIx,	:
Emil	Emil	k1gMnSc1	Emil
<g />
.	.	kIx.	.
</s>
<s>
Králík	Králík	k1gMnSc1	Králík
<g/>
,	,	kIx,	,
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
Vila	vít	k5eAaImAgFnS	vít
Eveliny	Evelin	k2eAgFnPc4d1	Evelina
Fleischerové	Fleischerová	k1gFnPc4	Fleischerová
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Svatoplukova	Svatoplukův	k2eAgFnSc1d1	Svatoplukova
64	[number]	k4	64
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1932	[number]	k4	1932
<g/>
-	-	kIx~	-
<g/>
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
styl	styl	k1gInSc1	styl
<g/>
:	:	kIx,	:
funkcionalismus	funkcionalismus	k1gInSc1	funkcionalismus
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
<g/>
:	:	kIx,	:
Eduard	Eduard	k1gMnSc1	Eduard
Žáček	Žáček	k1gMnSc1	Žáček
Rodinný	rodinný	k2eAgInSc1d1	rodinný
dům	dům	k1gInSc1	dům
Karla	Karel	k1gMnSc2	Karel
Melhuby	Melhuby	k?	Melhuby
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Sádky	sádka	k1gFnPc4	sádka
5	[number]	k4	5
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1935	[number]	k4	1935
<g/>
-	-	kIx~	-
<g/>
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
styl	styl	k1gInSc1	styl
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
funkcionalismus	funkcionalismus	k1gInSc1	funkcionalismus
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
<g/>
:	:	kIx,	:
Eduard	Eduard	k1gMnSc1	Eduard
Žáček	Žáček	k1gMnSc1	Žáček
Vila	vila	k1gFnSc1	vila
Jana	Jan	k1gMnSc2	Jan
a	a	k8xC	a
Anastázie	Anastázie	k1gFnSc2	Anastázie
Neherových	Neherových	k2eAgFnSc2d1	Neherových
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Pod	pod	k7c7	pod
Kosířem	kosíř	k1gInSc7	kosíř
73	[number]	k4	73
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1938	[number]	k4	1938
<g/>
-	-	kIx~	-
<g/>
1939	[number]	k4	1939
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
funkcionalismu	funkcionalismus	k1gInSc2	funkcionalismus
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
<g/>
:	:	kIx,	:
Antonín	Antonín	k1gMnSc1	Antonín
Navrátil	Navrátil	k1gMnSc1	Navrátil
Multifunční	Multifunční	k2eAgFnSc1d1	Multifunční
nájemní	nájemní	k2eAgFnSc1d1	nájemní
vila	vila	k1gFnSc1	vila
Radomíra	Radomír	k1gMnSc2	Radomír
a	a	k8xC	a
Vlasty	Vlasta	k1gMnSc2	Vlasta
Růžičkových	Růžičková	k1gFnPc2	Růžičková
na	na	k7c6	na
Arbesově	Arbesův	k2eAgNnSc6d1	Arbesovo
náměstí	náměstí	k1gNnSc6	náměstí
4	[number]	k4	4
z	z	k7c2	z
let	léto	k1gNnPc2	léto
<g />
.	.	kIx.	.
</s>
<s>
1938	[number]	k4	1938
<g/>
-	-	kIx~	-
<g/>
1939	[number]	k4	1939
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
funkcionalismu	funkcionalismus	k1gInSc2	funkcionalismus
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
<g/>
:	:	kIx,	:
František	František	k1gMnSc1	František
Kalivoda	Kalivoda	k1gMnSc1	Kalivoda
(	(	kIx(	(
<g/>
architekt	architekt	k1gMnSc1	architekt
<g/>
)	)	kIx)	)
Neofunkcionalistická	Neofunkcionalistický	k2eAgFnSc1d1	Neofunkcionalistická
vila	vila	k1gFnSc1	vila
Jindřišky	Jindřiška	k1gFnSc2	Jindřiška
a	a	k8xC	a
Miroslava	Miroslav	k1gMnSc2	Miroslav
Vysloužilových	Vysloužilových	k2eAgMnSc2d1	Vysloužilových
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Sadová	sadový	k2eAgFnSc1d1	Sadová
9	[number]	k4	9
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1970	[number]	k4	1970
<g/>
-	-	kIx~	-
<g/>
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
architekti	architekt	k1gMnPc1	architekt
<g/>
:	:	kIx,	:
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Plesník	Plesník	k1gMnSc1	Plesník
a	a	k8xC	a
Josef	Josef	k1gMnSc1	Josef
Taťák	Taťák	k1gMnSc1	Taťák
Onšův	Onšův	k2eAgInSc4d1	Onšův
dům	dům	k1gInSc4	dům
-	-	kIx~	-
dům	dům	k1gInSc1	dům
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
Onše	Onš	k1gMnSc2	Onš
z	z	k7c2	z
Břesovic	Břesovice	k1gFnPc2	Břesovice
se	s	k7c7	s
zdobeným	zdobený	k2eAgInSc7d1	zdobený
portálem	portál	k1gInSc7	portál
Dům	dům	k1gInSc1	dům
U	u	k7c2	u
zlaté	zlatý	k2eAgFnSc2d1	zlatá
studny	studna	k1gFnSc2	studna
Dům	dům	k1gInSc1	dům
čp.	čp.	k?	čp.
72	[number]	k4	72
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bývalém	bývalý	k2eAgNnSc6d1	bývalé
židovském	židovský	k2eAgNnSc6d1	Židovské
ghettu	ghetto	k1gNnSc6	ghetto
význačný	význačný	k2eAgInSc1d1	význačný
dům	dům	k1gInSc1	dům
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1819	[number]	k4	1819
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
nechal	nechat	k5eAaPmAgInS	nechat
postavil	postavit	k5eAaPmAgInS	postavit
Veight	Veight	k1gInSc1	Veight
Ehrenstamm	Ehrenstamma	k1gFnPc2	Ehrenstamma
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
velkoprůmyslník	velkoprůmyslník	k1gMnSc1	velkoprůmyslník
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
v	v	k7c6	v
Prostějově	Prostějov	k1gInSc6	Prostějov
<g/>
,	,	kIx,	,
kterému	který	k3yQgInSc3	který
bylo	být	k5eAaImAgNnS	být
povoleno	povolen	k2eAgNnSc1d1	povoleno
se	se	k3xPyFc4	se
usídlit	usídlit	k5eAaPmF	usídlit
uvnitř	uvnitř	k7c2	uvnitř
městských	městský	k2eAgFnPc2d1	městská
hradeb	hradba	k1gFnPc2	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Ábelův	Ábelův	k2eAgInSc1d1	Ábelův
dům	dům	k1gInSc1	dům
nám.	nám.	k?	nám.
T.G.	T.G.	k1gMnSc2	T.G.
<g/>
Masaryka	Masaryk	k1gMnSc2	Masaryk
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
spojením	spojení	k1gNnSc7	spojení
dvou	dva	k4xCgInPc2	dva
prastarých	prastarý	k2eAgInPc2d1	prastarý
domů	dům	k1gInPc2	dům
Dům	dům	k1gInSc1	dům
u	u	k7c2	u
sv.	sv.	kA	sv.
Antonína	Antonín	k1gMnSc2	Antonín
nám.	nám.	k?	nám.
T.G.	T.G.	k1gMnSc2	T.G.
<g/>
Masaryka	Masaryk	k1gMnSc2	Masaryk
22	[number]	k4	22
<g/>
.	.	kIx.	.
</s>
<s>
Rodný	rodný	k2eAgInSc1d1	rodný
dům	dům	k1gInSc1	dům
básníka	básník	k1gMnSc2	básník
Jiřího	Jiří	k1gMnSc2	Jiří
Wolkera	Wolker	k1gMnSc2	Wolker
Kostel	kostel	k1gInSc4	kostel
Povýšení	povýšení	k1gNnSc2	povýšení
svatého	svatý	k2eAgMnSc2d1	svatý
Kříže	Kříž	k1gMnSc2	Kříž
s	s	k7c7	s
kaplí	kaple	k1gFnSc7	kaple
začali	začít	k5eAaPmAgMnP	začít
stavět	stavět	k5eAaImF	stavět
augustiniáni	augustinián	k1gMnPc1	augustinián
roku	rok	k1gInSc2	rok
1391	[number]	k4	1391
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
vypálen	vypálit	k5eAaPmNgInS	vypálit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
je	být	k5eAaImIp3nS	být
kostel	kostel	k1gInSc1	kostel
v	v	k7c6	v
majetku	majetek	k1gInSc6	majetek
města	město	k1gNnSc2	město
Prostějova	Prostějov	k1gInSc2	Prostějov
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
jej	on	k3xPp3gMnSc4	on
přestavovalo	přestavovat	k5eAaImAgNnS	přestavovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1522	[number]	k4	1522
<g/>
-	-	kIx~	-
<g/>
1535	[number]	k4	1535
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1697	[number]	k4	1697
vyhořel	vyhořet	k5eAaPmAgInS	vyhořet
a	a	k8xC	a
následně	následně	k6eAd1	následně
byla	být	k5eAaImAgFnS	být
znovu	znovu	k6eAd1	znovu
zaklenuta	zaklenut	k2eAgFnSc1d1	zaklenuta
hlavní	hlavní	k2eAgFnSc1d1	hlavní
loď	loď	k1gFnSc1	loď
<g/>
,	,	kIx,	,
vstup	vstup	k1gInSc1	vstup
přesunut	přesunout	k5eAaPmNgInS	přesunout
na	na	k7c4	na
sever	sever	k1gInSc4	sever
a	a	k8xC	a
kostel	kostel	k1gInSc4	kostel
byl	být	k5eAaImAgInS	být
přesvěcen	přesvěcen	k2eAgMnSc1d1	přesvěcen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1725	[number]	k4	1725
<g/>
-	-	kIx~	-
<g/>
1726	[number]	k4	1726
byla	být	k5eAaImAgFnS	být
barokně	barokně	k6eAd1	barokně
přestavěna	přestavěn	k2eAgFnSc1d1	přestavěna
věž	věž	k1gFnSc1	věž
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc4	kostel
byl	být	k5eAaImAgMnS	být
znovu	znovu	k6eAd1	znovu
přestavěn	přestavět	k5eAaPmNgMnS	přestavět
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1856	[number]	k4	1856
<g/>
-	-	kIx~	-
<g/>
1888	[number]	k4	1888
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
roku	rok	k1gInSc2	rok
1893	[number]	k4	1893
byla	být	k5eAaImAgFnS	být
novobarokně	novobarokně	k6eAd1	novobarokně
přestavěna	přestavěn	k2eAgFnSc1d1	přestavěna
věž	věž	k1gFnSc1	věž
<g/>
.	.	kIx.	.
</s>
<s>
Klášter	klášter	k1gInSc1	klášter
milosrdných	milosrdný	k2eAgMnPc2d1	milosrdný
bratří	bratr	k1gMnPc2	bratr
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1733	[number]	k4	1733
<g/>
-	-	kIx~	-
<g/>
1756	[number]	k4	1756
<g/>
.	.	kIx.	.
</s>
<s>
Interiéry	interiér	k1gInPc1	interiér
jsou	být	k5eAaImIp3nP	být
vyzdobeny	vyzdobit	k5eAaPmNgInP	vyzdobit
cennými	cenný	k2eAgFnPc7d1	cenná
freskami	freska	k1gFnPc7	freska
malíře	malíř	k1gMnSc2	malíř
F.	F.	kA	F.
A.	A.	kA	A.
Sebastiniho	Sebastini	k1gMnSc2	Sebastini
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
komplexu	komplex	k1gInSc2	komplex
byla	být	k5eAaImAgFnS	být
i	i	k9	i
nemocnice	nemocnice	k1gFnSc1	nemocnice
a	a	k8xC	a
lékárna	lékárna	k1gFnSc1	lékárna
sloužící	sloužící	k1gFnSc1	sloužící
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1736	[number]	k4	1736
dodnes	dodnes	k6eAd1	dodnes
a	a	k8xC	a
kostel	kostel	k1gInSc4	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
obrázek	obrázek	k1gInSc1	obrázek
<g/>
)	)	kIx)	)
Husův	Husův	k2eAgInSc1d1	Husův
sbor	sbor	k1gInSc1	sbor
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Petra	Petra	k1gFnSc1	Petra
a	a	k8xC	a
Pavla	Pavla	k1gFnSc1	Pavla
s	s	k7c7	s
barokní	barokní	k2eAgFnSc7d1	barokní
kaplí	kaple	k1gFnSc7	kaple
sv.	sv.	kA	sv.
Lazara	Lazar	k1gMnSc2	Lazar
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Cyrila	Cyril	k1gMnSc2	Cyril
a	a	k8xC	a
Metoděje	Metoděj	k1gMnSc2	Metoděj
Karel	Karel	k1gMnSc1	Karel
Havlíček	Havlíček	k1gMnSc1	Havlíček
Borovský	Borovský	k1gMnSc1	Borovský
-	-	kIx~	-
socha	socha	k1gFnSc1	socha
(	(	kIx(	(
<g/>
obrázek	obrázek	k1gInSc1	obrázek
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Edmund	Edmund	k1gMnSc1	Edmund
Husserl	Husserl	k1gMnSc1	Husserl
-	-	kIx~	-
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
(	(	kIx(	(
<g/>
obrázek	obrázek	k1gInSc1	obrázek
<g/>
)	)	kIx)	)
František	František	k1gMnSc1	František
Xaver	Xaver	k1gMnSc1	Xaver
Krumlovský	krumlovský	k2eAgMnSc1d1	krumlovský
-	-	kIx~	-
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigu	k1gInSc2	Garrigu
Masaryk	Masaryk	k1gMnSc1	Masaryk
-	-	kIx~	-
socha	socha	k1gFnSc1	socha
František	František	k1gMnSc1	František
Palacký	Palacký	k1gMnSc1	Palacký
-	-	kIx~	-
busta	busta	k1gFnSc1	busta
Matěj	Matěj	k1gMnSc1	Matěj
Rejsek	rejsek	k1gMnSc1	rejsek
-	-	kIx~	-
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
(	(	kIx(	(
<g/>
obrázek	obrázek	k1gInSc1	obrázek
<g/>
,	,	kIx,	,
detail	detail	k1gInSc1	detail
desky	deska	k1gFnSc2	deska
<g/>
)	)	kIx)	)
Bedřich	Bedřich	k1gMnSc1	Bedřich
Smetana	Smetana	k1gMnSc1	Smetana
-	-	kIx~	-
socha	socha	k1gFnSc1	socha
(	(	kIx(	(
<g/>
obrázek	obrázek	k1gInSc1	obrázek
<g/>
)	)	kIx)	)
Karel	Karel	k1gMnSc1	Karel
<g />
.	.	kIx.	.
</s>
<s>
Vojáček	Vojáček	k1gMnSc1	Vojáček
-	-	kIx~	-
busta	busta	k1gFnSc1	busta
(	(	kIx(	(
<g/>
obrázek	obrázek	k1gInSc1	obrázek
<g/>
)	)	kIx)	)
Jiří	Jiří	k1gMnSc1	Jiří
Wolker	Wolker	k1gMnSc1	Wolker
-	-	kIx~	-
socha	socha	k1gFnSc1	socha
(	(	kIx(	(
<g/>
obrázek	obrázek	k1gInSc1	obrázek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
busta	busta	k1gFnSc1	busta
na	na	k7c6	na
rodném	rodný	k2eAgInSc6d1	rodný
domě	dům	k1gInSc6	dům
(	(	kIx(	(
<g/>
obrázek	obrázek	k1gInSc1	obrázek
<g/>
)	)	kIx)	)
Pomník	pomník	k1gInSc1	pomník
manželů	manžel	k1gMnPc2	manžel
Karla	Karel	k1gMnSc2	Karel
a	a	k8xC	a
Karly	Karla	k1gFnPc1	Karla
Vojáčkových	Vojáčkových	k2eAgFnPc1d1	Vojáčkových
Pomník	pomník	k1gInSc4	pomník
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nás	my	k3xPp1nPc2	my
Pomník	pomník	k1gInSc4	pomník
Padlých	padlý	k2eAgMnPc2d1	padlý
hrdinů	hrdina	k1gMnPc2	hrdina
-	-	kIx~	-
(	(	kIx(	(
<g/>
obrázek	obrázek	k1gInSc1	obrázek
<g/>
)	)	kIx)	)
Spaniův	Spaniův	k2eAgInSc1d1	Spaniův
obelisk	obelisk	k1gInSc1	obelisk
Parní	parní	k2eAgInSc1d1	parní
<g />
.	.	kIx.	.
</s>
<s>
lokomotiva	lokomotiva	k1gFnSc1	lokomotiva
310.127	[number]	k4	310.127
na	na	k7c6	na
podstavci	podstavec	k1gInSc6	podstavec
u	u	k7c2	u
hlavního	hlavní	k2eAgNnSc2d1	hlavní
vlakového	vlakový	k2eAgNnSc2d1	vlakové
nádraží	nádraží	k1gNnSc2	nádraží
Mariánský	mariánský	k2eAgInSc1d1	mariánský
sloup	sloup	k1gInSc1	sloup
se	s	k7c7	s
sochou	socha	k1gFnSc7	socha
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Socha	socha	k1gFnSc1	socha
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Karlovské	Karlovský	k2eAgFnPc1d1	Karlovská
Biokoridor	biokoridor	k1gInSc4	biokoridor
Hloučela	Hloučela	k1gFnSc2	Hloučela
-	-	kIx~	-
pěší	pěší	k2eAgFnSc1d1	pěší
trasa	trasa	k1gFnSc1	trasa
po	po	k7c6	po
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
říčky	říčka	k1gFnSc2	říčka
Hloučely	Hloučely	k1gFnSc2	Hloučely
Botanická	botanický	k2eAgFnSc1d1	botanická
zahrada	zahrada	k1gFnSc1	zahrada
Petra	Petr	k1gMnSc2	Petr
Albrechta	Albrecht	k1gMnSc2	Albrecht
Smetanovy	Smetanův	k2eAgFnSc2d1	Smetanova
sady	sada	k1gFnSc2	sada
-	-	kIx~	-
parčík	parčík	k1gInSc1	parčík
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vzniká	vznikat	k5eAaImIp3nS	vznikat
galerie	galerie	k1gFnSc1	galerie
ze	z	k7c2	z
soch	socha	k1gFnPc2	socha
z	z	k7c2	z
každoročně	každoročně	k6eAd1	každoročně
<g />
.	.	kIx.	.
</s>
<s>
konaného	konaný	k2eAgNnSc2d1	konané
sochařského	sochařský	k2eAgNnSc2d1	sochařské
sympozia	sympozion	k1gNnSc2	sympozion
Dolní	dolní	k2eAgInPc1d1	dolní
vinohrádky	vinohrádek	k1gInPc1	vinohrádek
Arboretum	arboretum	k1gNnSc4	arboretum
Vrahovice	Vrahovice	k1gFnSc2	Vrahovice
Střížova	Střížův	k2eAgFnSc1d1	Střížova
lípa	lípa	k1gFnSc1	lípa
Lípa	lípa	k1gFnSc1	lípa
u	u	k7c2	u
Sarkandera	Sarkandero	k1gNnSc2	Sarkandero
Jan	Jan	k1gMnSc1	Jan
Filipec	Filipec	k1gMnSc1	Filipec
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1431	[number]	k4	1431
<g/>
-	-	kIx~	-
<g/>
1509	[number]	k4	1509
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
varadínský	varadínský	k2eAgMnSc1d1	varadínský
biskup	biskup	k1gMnSc1	biskup
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Matěj	Matěj	k1gMnSc1	Matěj
Rejsek	rejsek	k1gMnSc1	rejsek
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1450	[number]	k4	1450
<g/>
-	-	kIx~	-
<g/>
1506	[number]	k4	1506
<g/>
)	)	kIx)	)
-	-	kIx~	-
architekt	architekt	k1gMnSc1	architekt
(	(	kIx(	(
<g/>
gotika	gotika	k1gFnSc1	gotika
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Jindřich	Jindřich	k1gMnSc1	Jindřich
Václav	Václav	k1gMnSc1	Václav
Richter	Richter	k1gMnSc1	Richter
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1653	[number]	k4	1653
<g/>
-	-	kIx~	-
<g/>
1696	[number]	k4	1696
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jezuita	jezuita	k1gMnSc1	jezuita
Veith	Veith	k1gMnSc1	Veith
Ehrenstamm	Ehrenstamm	k1gMnSc1	Ehrenstamm
(	(	kIx(	(
<g/>
1763	[number]	k4	1763
<g/>
-	-	kIx~	-
<g/>
1827	[number]	k4	1827
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
průmyslník	průmyslník	k1gMnSc1	průmyslník
Gideon	Gideona	k1gFnPc2	Gideona
Brecher	Brechra	k1gFnPc2	Brechra
(	(	kIx(	(
<g/>
1797	[number]	k4	1797
<g/>
-	-	kIx~	-
<g/>
1873	[number]	k4	1873
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
Franz	Franz	k1gMnSc1	Franz
Mandelblüh	Mandelblüh	k1gMnSc1	Mandelblüh
(	(	kIx(	(
<g/>
1807	[number]	k4	1807
<g/>
-	-	kIx~	-
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
Moravského	moravský	k2eAgInSc2d1	moravský
zemského	zemský	k2eAgInSc2d1	zemský
sněmu	sněm	k1gInSc2	sněm
<g/>
,	,	kIx,	,
říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
a	a	k8xC	a
starosta	starosta	k1gMnSc1	starosta
Olomouce	Olomouc	k1gFnSc2	Olomouc
Wenzel	Wenzel	k1gMnSc1	Wenzel
Messenhauser	Messenhauser	k1gMnSc1	Messenhauser
(	(	kIx(	(
<g/>
1813	[number]	k4	1813
<g/>
-	-	kIx~	-
<g/>
1848	[number]	k4	1848
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
důstojník	důstojník	k1gMnSc1	důstojník
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
Moritz	Moritz	k1gMnSc1	Moritz
Steinschneider	Steinschneider	k1gMnSc1	Steinschneider
(	(	kIx(	(
<g/>
1816	[number]	k4	1816
<g/>
-	-	kIx~	-
<g/>
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
orientalista	orientalista	k1gMnSc1	orientalista
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
moderní	moderní	k2eAgFnSc2d1	moderní
hebrejské	hebrejský	k2eAgFnSc2d1	hebrejská
bibliografie	bibliografie	k1gFnSc2	bibliografie
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
pojmu	pojmout	k5eAaPmIp1nS	pojmout
antisemitismus	antisemitismus	k1gInSc1	antisemitismus
Johann	Johann	k1gNnSc1	Johann
von	von	k1gInSc1	von
Berger	Berger	k1gMnSc1	Berger
(	(	kIx(	(
<g/>
1816	[number]	k4	1816
<g/>
-	-	kIx~	-
<g/>
1870	[number]	k4	1870
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
právník	právník	k1gMnSc1	právník
Franz	Franz	k1gMnSc1	Franz
Kuhn	Kuhna	k1gFnPc2	Kuhna
von	von	k1gInSc1	von
Kuhnenfeld	Kuhnenfeld	k1gMnSc1	Kuhnenfeld
(	(	kIx(	(
<g/>
1817	[number]	k4	1817
<g/>
-	-	kIx~	-
<g/>
1896	[number]	k4	1896
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rakousko-uherský	rakouskoherský	k2eAgMnSc1d1	rakousko-uherský
ministr	ministr	k1gMnSc1	ministr
války	válka	k1gFnSc2	válka
Jan	Jan	k1gMnSc1	Jan
<g />
.	.	kIx.	.
</s>
<s>
Chmelař	Chmelař	k1gMnSc1	Chmelař
(	(	kIx(	(
<g/>
1817	[number]	k4	1817
<g/>
-	-	kIx~	-
<g/>
1896	[number]	k4	1896
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
Mayer	Mayer	k1gMnSc1	Mayer
Mandl	mandl	k1gInSc1	mandl
(	(	kIx(	(
<g/>
1820	[number]	k4	1820
<g/>
/	/	kIx~	/
<g/>
1821	[number]	k4	1821
<g/>
-	-	kIx~	-
<g/>
1888	[number]	k4	1888
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podnikatel	podnikatel	k1gMnSc1	podnikatel
v	v	k7c6	v
oděvník	oděvník	k1gMnSc1	oděvník
průmyslu	průmysl	k1gInSc2	průmysl
Ignaz	Ignaz	k1gInSc1	Ignaz
Mandl	mandl	k1gInSc1	mandl
(	(	kIx(	(
<g/>
1823	[number]	k4	1823
<g/>
-	-	kIx~	-
<g/>
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podnikatel	podnikatel	k1gMnSc1	podnikatel
v	v	k7c4	v
oděvník	oděvník	k1gMnSc1	oděvník
<g />
.	.	kIx.	.
</s>
<s>
průmyslu	průmysl	k1gInSc2	průmysl
Adolf	Adolf	k1gMnSc1	Adolf
Beer	Beer	k1gMnSc1	Beer
(	(	kIx(	(
<g/>
1831	[number]	k4	1831
<g/>
-	-	kIx~	-
<g/>
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
Jan	Jan	k1gMnSc1	Jan
Rudolf	Rudolf	k1gMnSc1	Rudolf
Demel	Demel	k1gMnSc1	Demel
(	(	kIx(	(
<g/>
1833	[number]	k4	1833
<g/>
-	-	kIx~	-
<g/>
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
Eugen	Eugna	k1gFnPc2	Eugna
Felix	Felix	k1gMnSc1	Felix
(	(	kIx(	(
<g/>
1836	[number]	k4	1836
<g/>
-	-	kIx~	-
<g/>
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
Max	Max	k1gMnSc1	Max
Fleischer	Fleischra	k1gFnPc2	Fleischra
(	(	kIx(	(
<g/>
1841	[number]	k4	1841
<g/>
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
<g/>
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vídeňský	vídeňský	k2eAgMnSc1d1	vídeňský
architekt	architekt	k1gMnSc1	architekt
Ignaz	Ignaz	k1gInSc4	Ignaz
Brüll	Brüll	k1gInSc1	Brüll
(	(	kIx(	(
<g/>
1846	[number]	k4	1846
<g/>
-	-	kIx~	-
<g/>
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
klavírista	klavírista	k1gMnSc1	klavírista
Alois	Alois	k1gMnSc1	Alois
Czerny	Czerna	k1gFnSc2	Czerna
(	(	kIx(	(
<g/>
1847	[number]	k4	1847
<g/>
-	-	kIx~	-
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
etnograf	etnograf	k1gMnSc1	etnograf
Nathan	Nathan	k1gMnSc1	Nathan
Porges	Porges	k1gMnSc1	Porges
(	(	kIx(	(
<g/>
1848	[number]	k4	1848
<g/>
-	-	kIx~	-
<g/>
1924	[number]	k4	1924
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rabín	rabín	k1gMnSc1	rabín
Georg	Georg	k1gMnSc1	Georg
Schmiedl	Schmiedl	k1gMnSc1	Schmiedl
(	(	kIx(	(
<g/>
1855	[number]	k4	1855
<g/>
-	-	kIx~	-
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
učitel	učitel	k1gMnSc1	učitel
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
hnutí	hnutí	k1gNnSc1	hnutí
přátel	přítel	k1gMnPc2	přítel
přírody	příroda	k1gFnSc2	příroda
Konrad	Konrad	k1gInSc1	Konrad
Loewe	Loewe	k1gInSc1	Loewe
(	(	kIx(	(
<g/>
1856	[number]	k4	1856
<g/>
-	-	kIx~	-
<g/>
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
Richard	Richard	k1gMnSc1	Richard
Mandl	mandl	k1gInSc1	mandl
(	(	kIx(	(
<g/>
1859	[number]	k4	1859
<g/>
-	-	kIx~	-
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
skladatel	skladatel	k1gMnSc1	skladatel
Edmund	Edmund	k1gMnSc1	Edmund
Husserl	Husserl	k1gMnSc1	Husserl
(	(	kIx(	(
<g/>
1859	[number]	k4	1859
<g/>
-	-	kIx~	-
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
filosof	filosof	k1gMnSc1	filosof
českého	český	k2eAgInSc2d1	český
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
fenomenologie	fenomenologie	k1gFnSc2	fenomenologie
Ing.	ing.	kA	ing.
František	František	k1gMnSc1	František
Kovářík	kovářík	k1gMnSc1	kovářík
(	(	kIx(	(
<g/>
1865	[number]	k4	1865
<g/>
-	-	kIx~	-
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
veřejných	veřejný	k2eAgFnPc2d1	veřejná
prací	práce	k1gFnPc2	práce
1920	[number]	k4	1920
<g/>
-	-	kIx~	-
<g/>
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
spolumajitel	spolumajitel	k1gMnSc1	spolumajitel
<g />
.	.	kIx.	.
</s>
<s>
firmy	firma	k1gFnPc1	firma
Wichterle	Wichterle	k1gFnSc1	Wichterle
a	a	k8xC	a
Kovářík	kovářík	k1gMnSc1	kovářík
(	(	kIx(	(
<g/>
Wikov	Wikov	k1gInSc1	Wikov
<g/>
)	)	kIx)	)
Antonín	Antonín	k1gMnSc1	Antonín
Gottwald	Gottwald	k1gMnSc1	Gottwald
(	(	kIx(	(
<g/>
1869	[number]	k4	1869
<g/>
-	-	kIx~	-
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
archeolog	archeolog	k1gMnSc1	archeolog
Richard	Richard	k1gMnSc1	Richard
Stein	Stein	k1gMnSc1	Stein
(	(	kIx(	(
<g/>
1871	[number]	k4	1871
<g/>
-	-	kIx~	-
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nakladatel	nakladatel	k1gMnSc1	nakladatel
Karel	Karel	k1gMnSc1	Karel
Dostál-Lutinov	Dostál-Lutinov	k1gInSc1	Dostál-Lutinov
(	(	kIx(	(
<g/>
1871	[number]	k4	1871
<g/>
-	-	kIx~	-
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
představitel	představitel	k1gMnSc1	představitel
Katolické	katolický	k2eAgFnSc2d1	katolická
moderny	moderna	k1gFnSc2	moderna
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Kallab	Kallab	k1gMnSc1	Kallab
(	(	kIx(	(
<g/>
1875	[number]	k4	1875
<g/>
-	-	kIx~	-
<g/>
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
historik	historik	k1gMnSc1	historik
umění	umění	k1gNnSc2	umění
Pankraz	Pankraz	k1gInSc1	Pankraz
Schuk	Schuko	k1gNnPc2	Schuko
(	(	kIx(	(
<g/>
1877	[number]	k4	1877
<g/>
-	-	kIx~	-
<g/>
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
spisovatel	spisovatel	k1gMnSc1	spisovatel
Jan	Jan	k1gMnSc1	Jan
Bažant	Bažant	k1gMnSc1	Bažant
(	(	kIx(	(
<g/>
1881	[number]	k4	1881
<g/>
-	-	kIx~	-
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
významný	významný	k2eAgMnSc1d1	významný
stavitel	stavitel	k1gMnSc1	stavitel
a	a	k8xC	a
architekt	architekt	k1gMnSc1	architekt
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Stuka	Stuka	k1gMnSc1	Stuka
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
<g/>
-	-	kIx~	-
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
dirigent	dirigent	k1gMnSc1	dirigent
<g/>
,	,	kIx,	,
sbormistr	sbormistr	k1gMnSc1	sbormistr
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
pedagog	pedagog	k1gMnSc1	pedagog
Franz	Franz	k1gMnSc1	Franz
Fiedler	Fiedler	k1gMnSc1	Fiedler
(	(	kIx(	(
<g/>
1885	[number]	k4	1885
<g/>
-	-	kIx~	-
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotograf	fotograf	k1gMnSc1	fotograf
Alois	Alois	k1gMnSc1	Alois
Bábek	Bábek	k1gMnSc1	Bábek
(	(	kIx(	(
<g/>
1885	[number]	k4	1885
<g/>
-	-	kIx~	-
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
Vladimír	Vladimír	k1gMnSc1	Vladimír
Ambros	Ambrosa	k1gFnPc2	Ambrosa
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
-	-	kIx~	-
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
dirigent	dirigent	k1gMnSc1	dirigent
a	a	k8xC	a
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
předních	přední	k2eAgMnPc2d1	přední
představitelů	představitel	k1gMnPc2	představitel
brněnské	brněnský	k2eAgFnSc2d1	brněnská
skladatelské	skladatelský	k2eAgFnSc2d1	skladatelská
školy	škola	k1gFnSc2	škola
Leoše	Leoš	k1gMnSc2	Leoš
Janáčka	Janáček	k1gMnSc2	Janáček
Carmen	Carmen	k1gInSc4	Carmen
Cartellieri	Cartellieri	k1gNnSc2	Cartellieri
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
-	-	kIx~	-
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
Jan	Jan	k1gMnSc1	Jan
<g />
.	.	kIx.	.
</s>
<s>
Uher	uher	k1gInSc1	uher
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
-	-	kIx~	-
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
psycholog	psycholog	k1gMnSc1	psycholog
a	a	k8xC	a
sociolog	sociolog	k1gMnSc1	sociolog
<g/>
;	;	kIx,	;
profesor	profesor	k1gMnSc1	profesor
na	na	k7c6	na
Masarykově	Masarykův	k2eAgFnSc6d1	Masarykova
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
popraven	popraven	k2eAgInSc1d1	popraven
nacisty	nacista	k1gMnPc7	nacista
Max	Max	k1gMnSc1	Max
Zweig	Zweig	k1gMnSc1	Zweig
(	(	kIx(	(
<g/>
1892	[number]	k4	1892
<g/>
-	-	kIx~	-
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Janoušek	Janoušek	k1gMnSc1	Janoušek
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
-	-	kIx~	-
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vlastivědný	vlastivědný	k2eAgMnSc1d1	vlastivědný
pracovník	pracovník	k1gMnSc1	pracovník
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
knihy	kniha	k1gFnSc2	kniha
Prostějovský	prostějovský	k2eAgInSc1d1	prostějovský
okres	okres	k1gInSc1	okres
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Wolker	Wolker	k1gMnSc1	Wolker
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
-	-	kIx~	-
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
básník	básník	k1gMnSc1	básník
Edvard	Edvard	k1gMnSc1	Edvard
Valenta	Valenta	k1gMnSc1	Valenta
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
-	-	kIx~	-
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prozaik	prozaik	k1gMnSc1	prozaik
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
publicista	publicista	k1gMnSc1	publicista
Bernhard	Bernhard	k1gMnSc1	Bernhard
Heilig	Heilig	k1gMnSc1	Heilig
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
-	-	kIx~	-
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ekonom	ekonom	k1gMnSc1	ekonom
a	a	k8xC	a
historik	historik	k1gMnSc1	historik
<g />
.	.	kIx.	.
</s>
<s>
Alois	Alois	k1gMnSc1	Alois
Fišárek	Fišárek	k1gMnSc1	Fišárek
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
-	-	kIx~	-
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
Vilém	Vilém	k1gMnSc1	Vilém
Sacher	Sachra	k1gFnPc2	Sachra
(	(	kIx(	(
<g/>
1907	[number]	k4	1907
<g/>
-	-	kIx~	-
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
generálporučík	generálporučík	k1gMnSc1	generálporučík
<g/>
,	,	kIx,	,
válečný	válečný	k2eAgMnSc1d1	válečný
hrdina	hrdina	k1gMnSc1	hrdina
<g/>
,	,	kIx,	,
signatář	signatář	k1gMnSc1	signatář
Charty	charta	k1gFnSc2	charta
77	[number]	k4	77
Josef	Josef	k1gMnSc1	Josef
Šafařík	Šafařík	k1gMnSc1	Šafařík
(	(	kIx(	(
<g/>
1907	[number]	k4	1907
<g/>
-	-	kIx~	-
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
filozof	filozof	k1gMnSc1	filozof
<g/>
,	,	kIx,	,
esejista	esejista	k1gMnSc1	esejista
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
Pravdomil	Pravdomil	k1gMnSc1	Pravdomil
Svoboda	Svoboda	k1gMnSc1	Svoboda
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
-	-	kIx~	-
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
dendrolog	dendrolog	k1gMnSc1	dendrolog
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
Arboreta	arboretum	k1gNnSc2	arboretum
v	v	k7c6	v
Kostelci	Kostelec	k1gInSc6	Kostelec
nad	nad	k7c7	nad
Černými	černý	k2eAgInPc7d1	černý
lesy	les	k1gInPc7	les
Josef	Josef	k1gMnSc1	Josef
Mach	Mach	k1gMnSc1	Mach
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
-	-	kIx~	-
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
František	František	k1gMnSc1	František
Panuška	panuška	k1gFnSc1	panuška
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
římskokatolický	římskokatolický	k2eAgMnSc1d1	římskokatolický
teolog	teolog	k1gMnSc1	teolog
<g/>
,	,	kIx,	,
duchovní	duchovní	k1gMnSc1	duchovní
<g/>
,	,	kIx,	,
vysokoškolský	vysokoškolský	k2eAgMnSc1d1	vysokoškolský
pedagog	pedagog	k1gMnSc1	pedagog
Otto	Otto	k1gMnSc1	Otto
Wichterle	Wichterle	k1gFnSc1	Wichterle
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
-	-	kIx~	-
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
československý	československý	k2eAgMnSc1d1	československý
chemik	chemik	k1gMnSc1	chemik
<g/>
,	,	kIx,	,
vynálezce	vynálezce	k1gMnSc1	vynálezce
kontaktních	kontaktní	k2eAgFnPc2d1	kontaktní
čoček	čočka	k1gFnPc2	čočka
Ivo	Ivo	k1gMnSc1	Ivo
Ducháček	Ducháček	k1gMnSc1	Ducháček
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
-	-	kIx~	-
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
Susi	Sus	k1gFnSc2	Sus
Weigel	Weigel	k1gMnSc1	Weigel
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rakouská	rakouský	k2eAgFnSc1d1	rakouská
ilustrátorka	ilustrátorka	k1gFnSc1	ilustrátorka
dětských	dětský	k2eAgFnPc2d1	dětská
knih	kniha	k1gFnPc2	kniha
Josef	Josef	k1gMnSc1	Josef
Polišenský	Polišenský	k2eAgMnSc1d1	Polišenský
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
<g/>
-	-	kIx~	-
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
specializující	specializující	k2eAgMnSc1d1	specializující
se	se	k3xPyFc4	se
na	na	k7c4	na
novověké	novověký	k2eAgFnPc4d1	novověká
dějiny	dějiny	k1gFnPc4	dějiny
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
Karel	Karel	k1gMnSc1	Karel
Vaca	Vaca	k1gMnSc1	Vaca
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1919	[number]	k4	1919
<g/>
-	-	kIx~	-
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výtvarník	výtvarník	k1gMnSc1	výtvarník
Bedřich	Bedřich	k1gMnSc1	Bedřich
Seliger	Seliger	k1gMnSc1	Seliger
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
-	-	kIx~	-
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
voják	voják	k1gMnSc1	voják
Alexej	Alexej	k1gMnSc1	Alexej
Pludek	Pludek	k1gMnSc1	Pludek
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
-	-	kIx~	-
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
František	František	k1gMnSc1	František
Spurný	Spurný	k1gMnSc1	Spurný
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
-	-	kIx~	-
<g/>
2004	[number]	k4	2004
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
archivář	archivář	k1gMnSc1	archivář
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
publicista	publicista	k1gMnSc1	publicista
Pavel	Pavel	k1gMnSc1	Pavel
Machonin	Machonin	k2eAgMnSc1d1	Machonin
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
-	-	kIx~	-
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sociolog	sociolog	k1gMnSc1	sociolog
Dušan	Dušan	k1gMnSc1	Dušan
Janoušek	Janoušek	k1gMnSc1	Janoušek
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
-	-	kIx~	-
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
Jiřina	Jiřina	k1gFnSc1	Jiřina
Prekopová	Prekopový	k2eAgFnSc1d1	Prekopová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
psycholožka	psycholožka	k1gFnSc1	psycholožka
Otokar	Otokar	k1gMnSc1	Otokar
Hořínek	Hořínek	k1gMnSc1	Hořínek
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
-	-	kIx~	-
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sportovní	sportovní	k2eAgMnSc1d1	sportovní
střelec	střelec	k1gMnSc1	střelec
<g/>
,	,	kIx,	,
olympionik	olympionik	k1gMnSc1	olympionik
Miloš	Miloš	k1gMnSc1	Miloš
Zapletal	Zapletal	k1gMnSc1	Zapletal
(	(	kIx(	(
<g/>
*	*	kIx~	*
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
Bohumír	Bohumír	k1gMnSc1	Bohumír
Kolář	Kolář	k1gMnSc1	Kolář
(	(	kIx(	(
<g/>
*	*	kIx~	*
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
Ivo	Ivo	k1gMnSc1	Ivo
Možný	možný	k2eAgMnSc1d1	možný
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sociolog	sociolog	k1gMnSc1	sociolog
Milan	Milan	k1gMnSc1	Milan
Dvořák	Dvořák	k1gMnSc1	Dvořák
(	(	kIx(	(
<g/>
*	*	kIx~	*
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
Jiří	Jiří	k1gMnSc1	Jiří
Kuběna	Kuběna	k1gMnSc1	Kuběna
(	(	kIx(	(
<g/>
*	*	kIx~	*
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
Miloš	Miloš	k1gMnSc1	Miloš
Hubáček	Hubáček	k1gMnSc1	Hubáček
(	(	kIx(	(
<g/>
*	*	kIx~	*
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
literatury	literatura	k1gFnSc2	literatura
faktu	fakt	k1gInSc2	fakt
Milena	Milena	k1gFnSc1	Milena
Dvorská	Dvorská	k1gFnSc1	Dvorská
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
Vladimír	Vladimír	k1gMnSc1	Vladimír
Körner	Körner	k1gMnSc1	Körner
(	(	kIx(	(
<g/>
*	*	kIx~	*
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prozaik	prozaik	k1gMnSc1	prozaik
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
a	a	k8xC	a
dramaturg	dramaturg	k1gMnSc1	dramaturg
Gabriela	Gabriela	k1gFnSc1	Gabriela
Wilhelmová	Wilhelmová	k1gFnSc1	Wilhelmová
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
-	-	kIx~	-
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
Ladislav	Ladislav	k1gMnSc1	Ladislav
Županič	Županič	k1gMnSc1	Županič
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Petr	Petr	k1gMnSc1	Petr
<g />
.	.	kIx.	.
</s>
<s>
Chudožilov	Chudožilov	k1gInSc1	Chudožilov
(	(	kIx(	(
<g/>
*	*	kIx~	*
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
Pavel	Pavel	k1gMnSc1	Pavel
Dyba	Dyba	k1gMnSc1	Dyba
(	(	kIx(	(
<g/>
*	*	kIx~	*
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
Oldřich	Oldřich	k1gMnSc1	Oldřich
Machač	Machač	k1gMnSc1	Machač
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
-	-	kIx~	-
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
československý	československý	k2eAgMnSc1d1	československý
hokejový	hokejový	k2eAgMnSc1d1	hokejový
obránce	obránce	k1gMnSc1	obránce
Nina	Nina	k1gFnSc1	Nina
Škottová	Škottová	k1gFnSc1	Škottová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politička	politička	k1gFnSc1	politička
Vladimír	Vladimír	k1gMnSc1	Vladimír
Vačkář	Vačkář	k1gMnSc1	Vačkář
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
*	*	kIx~	*
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dráhový	dráhový	k2eAgMnSc1d1	dráhový
cyklista	cyklista	k1gMnSc1	cyklista
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Petržela	Petržela	k1gMnSc1	Petržela
(	(	kIx(	(
<g/>
*	*	kIx~	*
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
a	a	k8xC	a
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
trenér	trenér	k1gMnSc1	trenér
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Čížek	Čížek	k1gMnSc1	Čížek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
Ivo	Ivo	k1gMnSc1	Ivo
Šmoldas	Šmoldas	k1gMnSc1	Šmoldas
(	(	kIx(	(
<g/>
*	*	kIx~	*
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
redaktor	redaktor	k1gMnSc1	redaktor
a	a	k8xC	a
nakladatel	nakladatel	k1gMnSc1	nakladatel
Ladislav	Ladislav	k1gMnSc1	Ladislav
Svozil	Svozil	k1gMnSc1	Svozil
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
*	*	kIx~	*
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
Vladimír	Vladimír	k1gMnSc1	Vladimír
Balaš	Balaš	k1gMnSc1	Balaš
(	(	kIx(	(
<g/>
*	*	kIx~	*
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
Jiří	Jiří	k1gMnSc1	Jiří
Víšek	Víšek	k1gMnSc1	Víšek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotograf	fotograf	k1gMnSc1	fotograf
Luděk	Luděk	k1gMnSc1	Luděk
Mikloško	Mikloška	k1gFnSc5	Mikloška
(	(	kIx(	(
<g/>
*	*	kIx~	*
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
Petr	Petr	k1gMnSc1	Petr
Lessy	Lessa	k1gFnSc2	Lessa
(	(	kIx(	(
<g/>
*	*	kIx~	*
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
policejní	policejní	k2eAgMnSc1d1	policejní
prezident	prezident	k1gMnSc1	prezident
Libor	Libor	k1gMnSc1	Libor
Štefánik	Štefánik	k1gMnSc1	Štefánik
(	(	kIx(	(
<g/>
*	*	kIx~	*
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
generál	generál	k1gMnSc1	generál
letectva	letectvo	k1gNnSc2	letectvo
Gorazd	Gorazd	k1gMnSc1	Gorazd
Pavel	Pavel	k1gMnSc1	Pavel
Cetkovský	Cetkovský	k2eAgMnSc1d1	Cetkovský
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kněz	kněz	k1gMnSc1	kněz
Karel	Karel	k1gMnSc1	Karel
Nováček	Nováček	k1gMnSc1	Nováček
(	(	kIx(	(
<g/>
*	*	kIx~	*
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
československý	československý	k2eAgMnSc1d1	československý
a	a	k8xC	a
český	český	k2eAgMnSc1d1	český
tenista	tenista	k1gMnSc1	tenista
Pavlína	Pavlína	k1gFnSc1	Pavlína
Pořízková	Pořízková	k1gFnSc1	Pořízková
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g />
.	.	kIx.	.
</s>
<s>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americká	americký	k2eAgFnSc1d1	americká
modelka	modelka	k1gFnSc1	modelka
a	a	k8xC	a
herečka	herečka	k1gFnSc1	herečka
českého	český	k2eAgInSc2d1	český
původu	původ	k1gInSc2	původ
Jiří	Jiří	k1gMnSc1	Jiří
Peňás	Peňása	k1gFnPc2	Peňása
(	(	kIx(	(
<g/>
*	*	kIx~	*
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
Rena	Rena	k1gMnSc1	Rena
Dumont	Dumont	k1gMnSc1	Dumont
(	(	kIx(	(
<g/>
*	*	kIx~	*
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německá	německý	k2eAgFnSc1d1	německá
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
českého	český	k2eAgInSc2d1	český
původu	původ	k1gInSc2	původ
Robert	Robert	k1gMnSc1	Robert
Změlík	Změlík	k1gMnSc1	Změlík
(	(	kIx(	(
<g/>
*	*	kIx~	*
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
olympijský	olympijský	k2eAgMnSc1d1	olympijský
vítěz	vítěz	k1gMnSc1	vítěz
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
desetiboji	desetiboj	k1gInSc6	desetiboj
Kateřina	Kateřina	k1gFnSc1	Kateřina
Piňosová	Piňosová	k1gFnSc1	Piňosová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výtvarnice	výtvarnice	k1gFnSc1	výtvarnice
<g/>
,	,	kIx,	,
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
a	a	k8xC	a
překladatelka	překladatelka	k1gFnSc1	překladatelka
Petr	Petr	k1gMnSc1	Petr
Marek	Marek	k1gMnSc1	Marek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudebník	hudebník	k1gMnSc1	hudebník
a	a	k8xC	a
frontman	frontman	k1gMnSc1	frontman
skupiny	skupina	k1gFnSc2	skupina
MIDI	MIDI	kA	MIDI
LIDI	člověk	k1gMnPc4	člověk
a	a	k8xC	a
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
Helena	Helena	k1gFnSc1	Helena
Šulcová	Šulcová	k1gFnSc1	Šulcová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
a	a	k8xC	a
televizní	televizní	k2eAgFnSc1d1	televizní
<g />
.	.	kIx.	.
</s>
<s>
moderátorka	moderátorka	k1gFnSc1	moderátorka
<g/>
,	,	kIx,	,
moderovala	moderovat	k5eAaBmAgFnS	moderovat
v	v	k7c6	v
ČT	ČT	kA	ČT
<g/>
,	,	kIx,	,
na	na	k7c6	na
Primě	prima	k1gFnSc6	prima
<g/>
,	,	kIx,	,
Radiožurnálu	radiožurnál	k1gInSc6	radiožurnál
i	i	k8xC	i
Frekvenci	frekvence	k1gFnSc6	frekvence
1	[number]	k4	1
Martin	Martin	k1gMnSc1	Martin
Richter	Richter	k1gMnSc1	Richter
(	(	kIx(	(
<g/>
*	*	kIx~	*
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
Ondřej	Ondřej	k1gMnSc1	Ondřej
Kolář	Kolář	k1gMnSc1	Kolář
(	(	kIx(	(
<g/>
*	*	kIx~	*
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teolog	teolog	k1gMnSc1	teolog
a	a	k8xC	a
překladatel	překladatel	k1gMnSc1	překladatel
Jindřich	Jindřich	k1gMnSc1	Jindřich
Skácel	Skácel	k1gMnSc1	Skácel
(	(	kIx(	(
<g/>
*	*	kIx~	*
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
fotbalista	fotbalista	k1gMnSc1	fotbalista
Petr	Petr	k1gMnSc1	Petr
Kumstát	Kumstát	k1gInSc1	Kumstát
(	(	kIx(	(
<g/>
*	*	kIx~	*
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
Lukáš	Lukáš	k1gMnSc1	Lukáš
Krajíček	Krajíček	k1gMnSc1	Krajíček
(	(	kIx(	(
<g/>
*	*	kIx~	*
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
Jana	Jana	k1gFnSc1	Jana
Horáková	Horáková	k1gFnSc1	Horáková
(	(	kIx(	(
<g/>
*	*	kIx~	*
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cyklistka	cyklistka	k1gFnSc1	cyklistka
Pavlína	Pavlína	k1gFnSc1	Pavlína
Tichá	Tichá	k1gFnSc1	Tichá
(	(	kIx(	(
<g/>
*	*	kIx~	*
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
juniorská	juniorský	k2eAgFnSc1d1	juniorská
mistryně	mistryně	k1gFnSc1	mistryně
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
tenise	tenis	k1gInSc6	tenis
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
trenérka	trenérka	k1gFnSc1	trenérka
Petra	Petra	k1gFnSc1	Petra
Cetkovská	Cetkovský	k2eAgFnSc1d1	Cetkovská
(	(	kIx(	(
<g/>
*	*	kIx~	*
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tenistka	tenistka	k1gFnSc1	tenistka
Petr	Petr	k1gMnSc1	Petr
Pohl	Pohl	k1gMnSc1	Pohl
(	(	kIx(	(
<g/>
*	*	kIx~	*
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
působící	působící	k2eAgMnSc1d1	působící
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
Nikkarin	Nikkarin	k1gInSc4	Nikkarin
(	(	kIx(	(
<g/>
*	*	kIx~	*
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výtvarník	výtvarník	k1gMnSc1	výtvarník
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
komiksů	komiks	k1gInPc2	komiks
Pavel	Pavel	k1gMnSc1	Pavel
Dreksa	Dreks	k1gMnSc2	Dreks
(	(	kIx(	(
<g/>
*	*	kIx~	*
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
Antonín	Antonín	k1gMnSc1	Antonín
Honejsek	Honejsek	k1gMnSc1	Honejsek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
Jakub	Jakub	k1gMnSc1	Jakub
Čech	Čech	k1gMnSc1	Čech
(	(	kIx(	(
<g/>
*	*	kIx~	*
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
občanský	občanský	k2eAgMnSc1d1	občanský
aktivista	aktivista	k1gMnSc1	aktivista
Jan	Jan	k1gMnSc1	Jan
Černý	Černý	k1gMnSc1	Černý
(	(	kIx(	(
<g/>
1456	[number]	k4	1456
<g/>
-	-	kIx~	-
<g/>
1530	[number]	k4	1530
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
kněz	kněz	k1gMnSc1	kněz
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
(	(	kIx(	(
<g/>
1487	[number]	k4	1487
<g/>
-	-	kIx~	-
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1548	[number]	k4	1548
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
moravský	moravský	k2eAgMnSc1d1	moravský
zemský	zemský	k2eAgMnSc1d1	zemský
hejtman	hejtman	k1gMnSc1	hejtman
Meir	Meir	k1gMnSc1	Meir
Eisenstadt	Eisenstadt	k1gMnSc1	Eisenstadt
(	(	kIx(	(
<g/>
1670	[number]	k4	1670
<g/>
-	-	kIx~	-
<g/>
1744	[number]	k4	1744
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rabín	rabín	k1gMnSc1	rabín
Jonathan	Jonathan	k1gMnSc1	Jonathan
Eybeschütz	Eybeschütz	k1gMnSc1	Eybeschütz
(	(	kIx(	(
<g/>
1690	[number]	k4	1690
<g/>
-	-	kIx~	-
<g/>
1764	[number]	k4	1764
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rabín	rabín	k1gMnSc1	rabín
Jan	Jan	k1gMnSc1	Jan
Alois	Alois	k1gMnSc1	Alois
Hanke	Hanke	k1gFnSc1	Hanke
(	(	kIx(	(
<g/>
1751	[number]	k4	1751
<g/>
-	-	kIx~	-
<g/>
1806	[number]	k4	1806
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
moravský	moravský	k2eAgMnSc1d1	moravský
osvícenský	osvícenský	k2eAgMnSc1d1	osvícenský
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
humanista	humanista	k1gMnSc1	humanista
Cvi	Cvi	k1gMnSc1	Cvi
Jehošua	Jehošua	k1gMnSc1	Jehošua
ha-Levi	ha-Leev	k1gFnSc3	ha-Leev
Horowitz	Horowitz	k1gInSc1	Horowitz
(	(	kIx(	(
<g/>
1754	[number]	k4	1754
<g/>
-	-	kIx~	-
<g/>
1816	[number]	k4	1816
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rabín	rabín	k1gMnSc1	rabín
Eduard	Eduard	k1gMnSc1	Eduard
Skála	Skála	k1gMnSc1	Skála
(	(	kIx(	(
<g/>
1842	[number]	k4	1842
<g/>
-	-	kIx~	-
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cukrovarník	cukrovarník	k1gMnSc1	cukrovarník
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
Arnold	Arnold	k1gMnSc1	Arnold
Korff	Korff	k1gMnSc1	Korff
(	(	kIx(	(
<g/>
1870	[number]	k4	1870
<g />
.	.	kIx.	.
</s>
<s>
<g/>
-	-	kIx~	-
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
herec	herec	k1gMnSc1	herec
Carl	Carl	k1gMnSc1	Carl
Techet	Techet	k1gMnSc1	Techet
(	(	kIx(	(
<g/>
1877	[number]	k4	1877
<g/>
-	-	kIx~	-
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
satirický	satirický	k2eAgMnSc1d1	satirický
spisovatel	spisovatel	k1gMnSc1	spisovatel
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kučera	Kučera	k1gMnSc1	Kučera
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
politik	politik	k1gMnSc1	politik
Jan	Jan	k1gMnSc1	Jan
Sedláček	Sedláček	k1gMnSc1	Sedláček
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
-	-	kIx~	-
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
starosta	starosta	k1gMnSc1	starosta
Prostějova	Prostějov	k1gInSc2	Prostějov
Jan	Jan	k1gMnSc1	Jan
Poláček	Poláček	k1gMnSc1	Poláček
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
-	-	kIx~	-
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sběratel	sběratel	k1gMnSc1	sběratel
lidových	lidový	k2eAgFnPc2d1	lidová
písní	píseň	k1gFnPc2	píseň
Leo	Leo	k1gMnSc1	Leo
Lehner	Lehner	k1gMnSc1	Lehner
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
-	-	kIx~	-
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
skladatel	skladatel	k1gMnSc1	skladatel
Jan	Jan	k1gMnSc1	Jan
Tříska	Tříska	k1gMnSc1	Tříska
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
-	-	kIx~	-
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sochař	sochař	k1gMnSc1	sochař
a	a	k8xC	a
medailér	medailér	k1gMnSc1	medailér
Josef	Josef	k1gMnSc1	Josef
Duda	Duda	k1gMnSc1	Duda
(	(	kIx(	(
<g/>
19051	[number]	k4	19051
<g/>
-	-	kIx~	-
<g/>
977	[number]	k4	977
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vojenský	vojenský	k2eAgMnSc1d1	vojenský
letec	letec	k1gMnSc1	letec
<g/>
,	,	kIx,	,
účastník	účastník	k1gMnSc1	účastník
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
Prostějov	Prostějov	k1gInSc1	Prostějov
<g/>
,	,	kIx,	,
ul	ul	kA	ul
<g/>
.	.	kIx.	.
Vl	Vl	k1gMnSc1	Vl
<g/>
.	.	kIx.	.
</s>
<s>
Majakovského	Majakovský	k2eAgInSc2d1	Majakovský
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
Jana	Jan	k1gMnSc2	Jan
Železného	Železný	k1gMnSc2	Železný
Prostějov	Prostějov	k1gInSc4	Prostějov
<g/>
,	,	kIx,	,
Sídliště	sídliště	k1gNnSc1	sídliště
svobody	svoboda	k1gFnSc2	svoboda
24	[number]	k4	24
<g/>
/	/	kIx~	/
<g/>
79	[number]	k4	79
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
Prostějov	Prostějov	k1gInSc1	Prostějov
<g/>
,	,	kIx,	,
Melantrichova	Melantrichův	k2eAgFnSc1d1	Melantrichova
ul	ul	kA	ul
<g/>
.	.	kIx.	.
60	[number]	k4	60
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
Prostějov	Prostějov	k1gInSc1	Prostějov
<g/>
,	,	kIx,	,
Palackého	Palackého	k2eAgFnSc1d1	Palackého
tř	tř	kA	tř
<g/>
.	.	kIx.	.
14	[number]	k4	14
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
Prostějov	Prostějov	k1gInSc1	Prostějov
<g/>
,	,	kIx,	,
ul	ul	kA	ul
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Horáka	Horák	k1gMnSc4	Horák
24	[number]	k4	24
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
Prostějov	Prostějov	k1gInSc1	Prostějov
<g/>
,	,	kIx,	,
Kollárova	Kollárův	k2eAgFnSc1d1	Kollárova
4	[number]	k4	4
Cyrilometodějské	cyrilometodějský	k2eAgInPc1d1	cyrilometodějský
gymnázium	gymnázium	k1gNnSc4	gymnázium
v	v	k7c6	v
Prostějově	Prostějov	k1gInSc6	Prostějov
Gymnázium	gymnázium	k1gNnSc1	gymnázium
Jiřího	Jiří	k1gMnSc2	Jiří
Wolkera	Wolker	k1gMnSc2	Wolker
Reálné	reálný	k2eAgNnSc1d1	reálné
gymnázium	gymnázium	k1gNnSc1	gymnázium
a	a	k8xC	a
základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
města	město	k1gNnSc2	město
Prostějova	Prostějov	k1gInSc2	Prostějov
Střední	střední	k2eAgFnSc1d1	střední
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
a	a	k8xC	a
Střední	střední	k2eAgNnSc1d1	střední
odborné	odborný	k2eAgNnSc1d1	odborné
učiliště	učiliště	k1gNnSc1	učiliště
strojírenské	strojírenský	k2eAgNnSc1d1	strojírenské
<g/>
,	,	kIx,	,
Prostějov	Prostějov	k1gInSc1	Prostějov
<g/>
,	,	kIx,	,
Lidická	lidický	k2eAgFnSc1d1	Lidická
4	[number]	k4	4
Střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
veřejnoprávní	veřejnoprávní	k2eAgFnSc2d1	veřejnoprávní
<g />
.	.	kIx.	.
</s>
<s>
Prostějov	Prostějov	k1gInSc1	Prostějov
Střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
designu	design	k1gInSc2	design
a	a	k8xC	a
módy	móda	k1gFnSc2	móda
Střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
automobilní	automobilní	k2eAgFnSc1d1	automobilní
Prostějov	Prostějov	k1gInSc1	Prostějov
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
Švehlova	Švehlův	k2eAgFnSc1d1	Švehlova
střední	střední	k2eAgFnSc1d1	střední
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
Obchodní	obchodní	k2eAgFnSc2d1	obchodní
akademie	akademie	k1gFnSc2	akademie
Prostějov	Prostějov	k1gInSc4	Prostějov
Střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
JISTOTA	jistota	k1gFnSc1	jistota
<g/>
,	,	kIx,	,
o.p.s.	o.p.s.	k?	o.p.s.
Střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
Mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
Prostějov	Prostějov	k1gInSc1	Prostějov
<g/>
,	,	kIx,	,
Komenského	Komenského	k2eAgFnSc1d1	Komenského
10	[number]	k4	10
Střední	střední	k2eAgFnSc1d1	střední
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
podnikání	podnikání	k1gNnSc2	podnikání
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
Prostějov	Prostějov	k1gInSc1	Prostějov
je	být	k5eAaImIp3nS	být
i	i	k9	i
městem	město	k1gNnSc7	město
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelně	pravidelně	k6eAd1	pravidelně
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
konají	konat	k5eAaImIp3nP	konat
festivaly	festival	k1gInPc4	festival
<g/>
,	,	kIx,	,
promítání	promítání	k1gNnSc4	promítání
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
výstavy	výstava	k1gFnSc2	výstava
a	a	k8xC	a
nejen	nejen	k6eAd1	nejen
to	ten	k3xDgNnSc4	ten
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgFnPc4d3	nejznámější
akce	akce	k1gFnPc4	akce
patří	patřit	k5eAaImIp3nP	patřit
<g/>
:	:	kIx,	:
Wolkrův	Wolkrův	k2eAgInSc1d1	Wolkrův
Prostějov	Prostějov	k1gInSc1	Prostějov
-	-	kIx~	-
celostátní	celostátní	k2eAgFnSc1d1	celostátní
přehlídka	přehlídka	k1gFnSc1	přehlídka
poezie	poezie	k1gFnSc1	poezie
<g/>
,	,	kIx,	,
konaná	konaný	k2eAgFnSc1d1	konaná
vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
Prostějovské	prostějovský	k2eAgNnSc1d1	prostějovské
léto	léto	k1gNnSc1	léto
-	-	kIx~	-
projekt	projekt	k1gInSc1	projekt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
každý	každý	k3xTgInSc4	každý
letní	letní	k2eAgInSc4d1	letní
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
nabízí	nabízet	k5eAaImIp3nS	nabízet
veřejnosti	veřejnost	k1gFnSc2	veřejnost
rozmanité	rozmanitý	k2eAgFnSc2d1	rozmanitá
aktivity	aktivita	k1gFnSc2	aktivita
Prostějovská	prostějovský	k2eAgFnSc1d1	prostějovská
zima	zima	k1gFnSc1	zima
-	-	kIx~	-
série	série	k1gFnSc1	série
zimních	zimní	k2eAgFnPc2d1	zimní
akcí	akce	k1gFnPc2	akce
většinou	většinou	k6eAd1	většinou
s	s	k7c7	s
vánoční	vánoční	k2eAgFnSc7d1	vánoční
tematikou	tematika	k1gFnSc7	tematika
Mladá	mladý	k2eAgFnSc1d1	mladá
scéna	scéna	k1gFnSc1	scéna
-	-	kIx~	-
krajské	krajský	k2eAgNnSc1d1	krajské
kolo	kolo	k1gNnSc1	kolo
přehlídky	přehlídka	k1gFnSc2	přehlídka
studentských	studentský	k2eAgNnPc2d1	studentské
divadel	divadlo	k1gNnPc2	divadlo
<g />
.	.	kIx.	.
</s>
<s>
Hanácké	hanácký	k2eAgFnPc1d1	Hanácká
slavnosti	slavnost	k1gFnPc1	slavnost
-	-	kIx~	-
tradiční	tradiční	k2eAgInPc1d1	tradiční
hody	hod	k1gInPc1	hod
s	s	k7c7	s
lidovým	lidový	k2eAgInSc7d1	lidový
jarmarkem	jarmarkem	k?	jarmarkem
<g/>
,	,	kIx,	,
poutí	pouť	k1gFnSc7	pouť
a	a	k8xC	a
kulturním	kulturní	k2eAgInSc7d1	kulturní
programem	program	k1gInSc7	program
Velikonoční	velikonoční	k2eAgInSc1d1	velikonoční
Prostějov	Prostějov	k1gInSc1	Prostějov
-	-	kIx~	-
tradiční	tradiční	k2eAgFnSc1d1	tradiční
modelářská	modelářský	k2eAgFnSc1d1	modelářská
soutěž	soutěž	k1gFnSc1	soutěž
s	s	k7c7	s
bohatým	bohatý	k2eAgInSc7d1	bohatý
doprovodným	doprovodný	k2eAgInSc7d1	doprovodný
programem	program	k1gInSc7	program
<g/>
;	;	kIx,	;
součástí	součást	k1gFnPc2	součást
je	být	k5eAaImIp3nS	být
kategorie	kategorie	k1gFnSc1	kategorie
"	"	kIx"	"
<g/>
Prostějovská	prostějovský	k2eAgFnSc1d1	prostějovská
-	-	kIx~	-
72	[number]	k4	72
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
již	již	k9	již
po	po	k7c4	po
40	[number]	k4	40
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Akce	akce	k1gFnSc1	akce
<g />
.	.	kIx.	.
</s>
<s>
zde	zde	k6eAd1	zde
zajišťuji	zajišťovat	k5eAaImIp1nS	zajišťovat
tyto	tento	k3xDgFnPc4	tento
organizace	organizace	k1gFnPc4	organizace
<g/>
:	:	kIx,	:
Magistrát	magistrát	k1gInSc4	magistrát
města	město	k1gNnSc2	město
Prostějova	Prostějov	k1gInSc2	Prostějov
<g/>
,	,	kIx,	,
Odbor	odbor	k1gInSc4	odbor
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
sportu	sport	k1gInSc2	sport
<g/>
,	,	kIx,	,
oddělení	oddělení	k1gNnSc4	oddělení
kulturního	kulturní	k2eAgInSc2d1	kulturní
klubu	klub	k1gInSc2	klub
Duha	duha	k1gFnSc1	duha
Městské	městský	k2eAgNnSc4d1	Městské
divadlo	divadlo	k1gNnSc4	divadlo
-	-	kIx~	-
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
koncerty	koncert	k1gInPc4	koncert
Kino	kino	k1gNnSc4	kino
Metro	metro	k1gNnSc4	metro
70	[number]	k4	70
-	-	kIx~	-
filmová	filmový	k2eAgNnPc4d1	filmové
představení	představení	k1gNnPc4	představení
Galerie	galerie	k1gFnSc2	galerie
u	u	k7c2	u
Hanáka	Hanák	k1gMnSc2	Hanák
-	-	kIx~	-
výstavy	výstav	k1gInPc4	výstav
fotografií	fotografia	k1gFnPc2	fotografia
a	a	k8xC	a
obrazů	obraz	k1gInPc2	obraz
Ekocentrum	Ekocentrum	k1gNnSc4	Ekocentrum
Iris	iris	k1gFnSc2	iris
-	-	kIx~	-
ekologické	ekologický	k2eAgFnSc2d1	ekologická
akce	akce	k1gFnSc2	akce
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
vycházky	vycházka	k1gFnPc1	vycházka
do	do	k7c2	do
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
výstavy	výstava	k1gFnPc1	výstava
<g/>
,	,	kIx,	,
besedy	beseda	k1gFnPc1	beseda
Sportcentrum	Sportcentrum	k1gNnSc1	Sportcentrum
DDM	DDM	kA	DDM
Divadlo	divadlo	k1gNnSc1	divadlo
Point	pointa	k1gFnPc2	pointa
-	-	kIx~	-
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
promítání	promítání	k1gNnSc4	promítání
amatérských	amatérský	k2eAgInPc2d1	amatérský
filmů	film	k1gInPc2	film
Muzeum	muzeum	k1gNnSc4	muzeum
Prostějovska	Prostějovsko	k1gNnSc2	Prostějovsko
-	-	kIx~	-
výstavy	výstava	k1gFnPc1	výstava
<g/>
,	,	kIx,	,
přednášky	přednáška	k1gFnPc1	přednáška
Národní	národní	k2eAgFnPc1d1	národní
dům	dům	k1gInSc4	dům
Galerie	galerie	k1gFnSc2	galerie
Metro	metro	k1gNnSc4	metro
-	-	kIx~	-
výstavy	výstava	k1gFnSc2	výstava
fotografií	fotografia	k1gFnSc7	fotografia
Art	Art	k1gMnSc2	Art
Moravia	Moravius	k1gMnSc2	Moravius
-	-	kIx~	-
Kavárna	kavárna	k1gFnSc1	kavárna
GALERIE	galerie	k1gFnSc2	galerie
Národního	národní	k2eAgInSc2d1	národní
domu	dům	k1gInSc2	dům
-	-	kIx~	-
výstavy	výstava	k1gFnPc4	výstava
obrazů	obraz	k1gInPc2	obraz
a	a	k8xC	a
fotografií	fotografia	k1gFnPc2	fotografia
Regioální	Regioální	k2eAgNnSc1d1	Regioální
infocentrum	infocentrum	k1gNnSc1	infocentrum
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
výstavní	výstavní	k2eAgInSc4d1	výstavní
sál	sál	k1gInSc4	sál
-	-	kIx~	-
výstavy	výstava	k1gFnPc4	výstava
obrazů	obraz	k1gInPc2	obraz
<g/>
,	,	kIx,	,
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
,	,	kIx,	,
výrobků	výrobek	k1gInPc2	výrobek
<g/>
,	,	kIx,	,
přístup	přístup	k1gInSc4	přístup
na	na	k7c4	na
internet	internet	k1gInSc4	internet
Městská	městský	k2eAgFnSc1d1	městská
knihovna	knihovna	k1gFnSc1	knihovna
Prostějov	Prostějov	k1gInSc1	Prostějov
-	-	kIx~	-
možnost	možnost	k1gFnSc1	možnost
půjčování	půjčování	k1gNnSc2	půjčování
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
přístup	přístup	k1gInSc4	přístup
na	na	k7c4	na
internet	internet	k1gInSc4	internet
<g/>
,	,	kIx,	,
přednášky	přednáška	k1gFnPc4	přednáška
Lidová	lidový	k2eAgFnSc1d1	lidová
hvězdárna	hvězdárna	k1gFnSc1	hvězdárna
-	-	kIx~	-
přednášky	přednáška	k1gFnPc1	přednáška
<g/>
,	,	kIx,	,
pořady	pořad	k1gInPc1	pořad
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
i	i	k8xC	i
dospělé	dospělí	k1gMnPc4	dospělí
<g/>
,	,	kIx,	,
výstavy	výstava	k1gFnPc4	výstava
Prostějov	Prostějov	k1gInSc1	Prostějov
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
i	i	k9	i
dnes	dnes	k6eAd1	dnes
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xS	jako
město	město	k1gNnSc1	město
sportu	sport	k1gInSc2	sport
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
především	především	k9	především
o	o	k7c4	o
fotbal	fotbal	k1gInSc4	fotbal
a	a	k8xC	a
hokej	hokej	k1gInSc4	hokej
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
sportovní	sportovní	k2eAgInPc1d1	sportovní
oddíly	oddíl	k1gInPc1	oddíl
vznikaly	vznikat	k5eAaImAgInP	vznikat
ve	v	k7c6	v
městě	město	k1gNnSc6	město
již	již	k6eAd1	již
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
z	z	k7c2	z
nich	on	k3xPp3gFnPc6	on
byl	být	k5eAaImAgInS	být
Sportovní	sportovní	k2eAgInSc1d1	sportovní
Klub	klub	k1gInSc1	klub
Prostějov	Prostějov	k1gInSc1	Prostějov
založený	založený	k2eAgInSc1d1	založený
roku	rok	k1gInSc2	rok
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
pod	pod	k7c7	pod
zkratkou	zkratka	k1gFnSc7	zkratka
SK	Sk	kA	Sk
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
zastřešoval	zastřešovat	k5eAaImAgMnS	zastřešovat
několik	několik	k4yIc4	několik
sportovních	sportovní	k2eAgNnPc2d1	sportovní
odvětví	odvětví	k1gNnPc2	odvětví
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nejpopulárnější	populární	k2eAgInSc4d3	nejpopulárnější
sport	sport	k1gInSc4	sport
ve	v	k7c6	v
městě	město	k1gNnSc6	město
-	-	kIx~	-
fotbal	fotbal	k1gInSc1	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
SK	Sk	kA	Sk
Prostějov	Prostějov	k1gInSc1	Prostějov
(	(	kIx(	(
<g/>
působící	působící	k2eAgMnSc1d1	působící
dnes	dnes	k6eAd1	dnes
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
SK	Sk	kA	Sk
Prostějov	Prostějov	k1gInSc1	Prostějov
<g/>
)	)	kIx)	)
působil	působit	k5eAaImAgInS	působit
ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
a	a	k8xC	a
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1945	[number]	k4	1945
<g/>
/	/	kIx~	/
<g/>
46	[number]	k4	46
v	v	k7c6	v
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
profesionální	profesionální	k2eAgFnSc6d1	profesionální
fotbalové	fotbalový	k2eAgFnSc6d1	fotbalová
lize	liga	k1gFnSc6	liga
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
v	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
1939	[number]	k4	1939
až	až	k6eAd1	až
1943	[number]	k4	1943
pak	pak	k6eAd1	pak
v	v	k7c6	v
Protektorátní	protektorátní	k2eAgFnSc6d1	protektorátní
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Česko-moravské	českooravský	k2eAgFnSc3d1	česko-moravská
lize	liga	k1gFnSc3	liga
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
SK	Sk	kA	Sk
Prostějov	Prostějov	k1gInSc1	Prostějov
také	také	k9	také
hájil	hájit	k5eAaImAgMnS	hájit
barvy	barva	k1gFnPc4	barva
města	město	k1gNnSc2	město
v	v	k7c6	v
tehdy	tehdy	k6eAd1	tehdy
velmi	velmi	k6eAd1	velmi
prestižním	prestižní	k2eAgInSc6d1	prestižní
Středoevropském	středoevropský	k2eAgInSc6d1	středoevropský
poháru	pohár	k1gInSc6	pohár
(	(	kIx(	(
<g/>
Mitropa	Mitropa	k1gFnSc1	Mitropa
-	-	kIx~	-
Cup	cup	k1gInSc1	cup
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
jediným	jediný	k2eAgMnSc7d1	jediný
evropským	evropský	k2eAgInSc7d1	evropský
pohárem	pohár	k1gInSc7	pohár
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
fotbalových	fotbalový	k2eAgInPc2d1	fotbalový
klubů	klub	k1gInPc2	klub
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Prostějovský	prostějovský	k2eAgInSc1d1	prostějovský
SK	Sk	kA	Sk
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
několika	několik	k4yIc2	několik
senzačních	senzační	k2eAgInPc2d1	senzační
úspěchů	úspěch	k1gInPc2	úspěch
(	(	kIx(	(
<g/>
vítězství	vítězství	k1gNnSc1	vítězství
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
špičkového	špičkový	k2eAgInSc2d1	špičkový
rakouského	rakouský	k2eAgInSc2d1	rakouský
týmu	tým	k1gInSc2	tým
Admiry	Admira	k1gFnSc2	Admira
Wien	Wien	k1gInSc1	Wien
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
<g/>
nebo	nebo	k8xC	nebo
vyrovnané	vyrovnaný	k2eAgInPc1d1	vyrovnaný
zápasy	zápas	k1gInPc1	zápas
s	s	k7c7	s
curyšským	curyšský	k2eAgInSc7d1	curyšský
Grasshoppers	Grasshoppersa	k1gFnPc2	Grasshoppersa
<g/>
)	)	kIx)	)
a	a	k8xC	a
lákal	lákat	k5eAaImAgMnS	lákat
do	do	k7c2	do
hlediště	hlediště	k1gNnSc2	hlediště
stadionu	stadion	k1gInSc2	stadion
ve	v	k7c6	v
Sportovní	sportovní	k2eAgFnSc6d1	sportovní
ulici	ulice	k1gFnSc6	ulice
desetitisícové	desetitisícový	k2eAgFnSc2d1	desetitisícová
návštěvy	návštěva	k1gFnSc2	návštěva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
mezi	mezi	k7c7	mezi
válkami	válka	k1gFnPc7	válka
působila	působit	k5eAaImAgFnS	působit
v	v	k7c6	v
Prostějově	Prostějov	k1gInSc6	Prostějov
téměř	téměř	k6eAd1	téměř
desítka	desítka	k1gFnSc1	desítka
klubů	klub	k1gInPc2	klub
provozujících	provozující	k2eAgFnPc2d1	provozující
kopanou	kopaná	k1gFnSc7	kopaná
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
Makkabi	Makkabi	k1gNnSc1	Makkabi
<g/>
,	,	kIx,	,
SK	Sk	kA	Sk
Rolný	Rolný	k1gMnSc1	Rolný
<g/>
,	,	kIx,	,
SK	Sk	kA	Sk
Slavoj	Slavoj	k1gMnSc1	Slavoj
<g/>
,	,	kIx,	,
Moravan	Moravan	k1gMnSc1	Moravan
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Rudá	rudý	k2eAgFnSc1d1	rudá
Hvězda	hvězda	k1gFnSc1	hvězda
<g/>
)	)	kIx)	)
atd.	atd.	kA	atd.
I	i	k9	i
přes	přes	k7c4	přes
tato	tento	k3xDgNnPc4	tento
historická	historický	k2eAgNnPc4d1	historické
fakta	faktum	k1gNnPc4	faktum
dnes	dnes	k6eAd1	dnes
prostějovské	prostějovský	k2eAgInPc1d1	prostějovský
fotbalové	fotbalový	k2eAgInPc1d1	fotbalový
kluby	klub	k1gInPc1	klub
působí	působit	k5eAaImIp3nP	působit
v	v	k7c6	v
nižších	nízký	k2eAgNnPc6d2	nižší
patrech	patro	k1gNnPc6	patro
fotbalových	fotbalový	k2eAgFnPc2d1	fotbalová
soutěží	soutěž	k1gFnPc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Prostějově	Prostějov	k1gInSc6	Prostějov
též	též	k9	též
sídlí	sídlet	k5eAaImIp3nS	sídlet
tenisový	tenisový	k2eAgInSc1d1	tenisový
klub	klub	k1gInSc1	klub
TK	TK	kA	TK
Agrofert	Agrofert	k1gInSc1	Agrofert
Prostějov	Prostějov	k1gInSc1	Prostějov
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
111	[number]	k4	111
<g/>
ti	ten	k3xDgMnPc1	ten
letou	letý	k2eAgFnSc7d1	letá
tradicí	tradice	k1gFnSc7	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgInS	proslavit
hráči	hráč	k1gMnPc7	hráč
na	na	k7c6	na
světové	světový	k2eAgFnSc6d1	světová
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Petra	Petra	k1gFnSc1	Petra
Kvitová	Kvitová	k1gFnSc1	Kvitová
a	a	k8xC	a
Tomáš	Tomáš	k1gMnSc1	Tomáš
Berdych	Berdych	k1gMnSc1	Berdych
<g/>
.	.	kIx.	.
</s>
<s>
Prostějov	Prostějov	k1gInSc1	Prostějov
má	mít	k5eAaImIp3nS	mít
hokejový	hokejový	k2eAgInSc1d1	hokejový
klub	klub	k1gInSc1	klub
LHK	LHK	kA	LHK
Jestřábi	jestřáb	k1gMnPc1	jestřáb
Prostějov	Prostějov	k1gInSc4	Prostějov
Momentálně	momentálně	k6eAd1	momentálně
hrají	hrát	k5eAaImIp3nP	hrát
2	[number]	k4	2
<g/>
.	.	kIx.	.
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
soutěž	soutěž	k1gFnSc4	soutěž
tzv.	tzv.	kA	tzv.
WSM	WSM	kA	WSM
ligu	liga	k1gFnSc4	liga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2013	[number]	k4	2013
<g/>
/	/	kIx~	/
<g/>
2014	[number]	k4	2014
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
2	[number]	k4	2
<g/>
.	.	kIx.	.
ligu	liga	k1gFnSc4	liga
skupinu	skupina	k1gFnSc4	skupina
východ	východ	k1gInSc4	východ
díky	díky	k7c3	díky
výhrám	výhra	k1gFnPc3	výhra
proti	proti	k7c3	proti
HC	HC	kA	HC
Zubr	zubr	k1gMnSc1	zubr
Přerov	Přerov	k1gInSc1	Přerov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2015	[number]	k4	2015
<g/>
/	/	kIx~	/
<g/>
2016	[number]	k4	2016
se	se	k3xPyFc4	se
Prostějovští	prostějovský	k2eAgMnPc1d1	prostějovský
jestřábí	jestřábí	k2eAgMnPc1d1	jestřábí
probojovali	probojovat	k5eAaPmAgMnP	probojovat
do	do	k7c2	do
play-off	playff	k1gInSc4	play-off
ale	ale	k8xC	ale
přes	přes	k7c4	přes
HC	HC	kA	HC
Slavii	slavie	k1gFnSc4	slavie
Praha	Praha	k1gFnSc1	Praha
se	se	k3xPyFc4	se
nedostali	dostat	k5eNaPmAgMnP	dostat
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
SK	Sk	kA	Sk
Prostějov	Prostějov	k1gInSc1	Prostějov
(	(	kIx(	(
<g/>
fotbal	fotbal	k1gInSc1	fotbal
<g/>
)	)	kIx)	)
AFK	AFK	kA	AFK
Prostějov	Prostějov	k1gInSc1	Prostějov
City	city	k1gNnSc1	city
2016	[number]	k4	2016
(	(	kIx(	(
<g/>
fotbal	fotbal	k1gInSc4	fotbal
<g/>
)	)	kIx)	)
Ariete	Arie	k1gNnSc2	Arie
Prostějov	Prostějov	k1gInSc1	Prostějov
(	(	kIx(	(
<g/>
basketbal	basketbal	k1gInSc1	basketbal
<g/>
)	)	kIx)	)
DTJ	DTJ	kA	DTJ
Prostějov	Prostějov	k1gInSc1	Prostějov
(	(	kIx(	(
<g/>
box	box	k1gInSc1	box
<g/>
)	)	kIx)	)
LHK	LHK	kA	LHK
Jestřábi	jestřáb	k1gMnPc1	jestřáb
Prostějov	Prostějov	k1gInSc1	Prostějov
(	(	kIx(	(
<g/>
hokej	hokej	k1gInSc1	hokej
<g/>
)	)	kIx)	)
Sokol	Sokol	k1gMnSc1	Sokol
Vrahovice	Vrahovice	k1gFnSc2	Vrahovice
(	(	kIx(	(
<g/>
fotbal	fotbal	k1gInSc1	fotbal
<g/>
)	)	kIx)	)
TK	TK	kA	TK
Agrofert	Agrofert	k1gInSc1	Agrofert
Prostějov	Prostějov	k1gInSc1	Prostějov
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
tenis	tenis	k1gInSc1	tenis
<g/>
)	)	kIx)	)
TJ	tj	kA	tj
Haná	Haná	k1gFnSc1	Haná
Prostějov	Prostějov	k1gInSc1	Prostějov
(	(	kIx(	(
<g/>
fotbal	fotbal	k1gInSc1	fotbal
<g/>
)	)	kIx)	)
TJ	tj	kA	tj
Sokol	Sokol	k1gMnSc1	Sokol
II	II	kA	II
Prostějov	Prostějov	k1gInSc1	Prostějov
(	(	kIx(	(
<g/>
házená	házená	k1gFnSc1	házená
<g/>
)	)	kIx)	)
TJ	tj	kA	tj
Sokol	Sokol	k1gMnSc1	Sokol
Čechovice	Čechovice	k1gFnSc2	Čechovice
(	(	kIx(	(
<g/>
fotbal	fotbal	k1gInSc1	fotbal
<g/>
)	)	kIx)	)
VK	VK	kA	VK
AGEL	AGEL	kA	AGEL
Prostějov	Prostějov	k1gInSc1	Prostějov
(	(	kIx(	(
<g/>
volejbal	volejbal	k1gInSc1	volejbal
<g/>
)	)	kIx)	)
SK	Sk	kA	Sk
RG	RG	kA	RG
Prostějov	Prostějov	k1gInSc1	Prostějov
(	(	kIx(	(
<g/>
korfbal	korfbal	k1gInSc1	korfbal
<g/>
)	)	kIx)	)
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1798	[number]	k4	1798
je	být	k5eAaImIp3nS	být
Prostějov	Prostějov	k1gInSc1	Prostějov
posádkovým	posádkový	k2eAgNnSc7d1	posádkové
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
zde	zde	k6eAd1	zde
sídlí	sídlet	k5eAaImIp3nS	sídlet
tyto	tento	k3xDgFnPc4	tento
vojenské	vojenský	k2eAgFnPc4d1	vojenská
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
:	:	kIx,	:
601	[number]	k4	601
<g/>
.	.	kIx.	.
skupina	skupina	k1gFnSc1	skupina
speciálních	speciální	k2eAgFnPc2d1	speciální
sil	síla	k1gFnPc2	síla
generála	generál	k1gMnSc2	generál
Moravce	Moravec	k1gMnSc2	Moravec
102	[number]	k4	102
<g/>
.	.	kIx.	.
průzkumný	průzkumný	k2eAgInSc1d1	průzkumný
prapor	prapor	k1gInSc1	prapor
generála	generál	k1gMnSc2	generál
Karla	Karel	k1gMnSc2	Karel
Palečka	Paleček	k1gMnSc2	Paleček
V	v	k7c6	v
době	doba	k1gFnSc6	doba
nacistické	nacistický	k2eAgFnSc2d1	nacistická
okupace	okupace	k1gFnSc2	okupace
Prostějov	Prostějov	k1gInSc1	Prostějov
tlačil	tlačit	k5eAaImAgInS	tlačit
na	na	k7c4	na
připojení	připojení	k1gNnSc4	připojení
Vrahovic	Vrahovice	k1gFnPc2	Vrahovice
<g/>
,	,	kIx,	,
Držovic	Držovice	k1gFnPc2	Držovice
<g/>
,	,	kIx,	,
Čechovic	Čechovice	k1gFnPc2	Čechovice
a	a	k8xC	a
Krasic	Krasice	k1gFnPc2	Krasice
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
obce	obec	k1gFnPc1	obec
si	se	k3xPyFc3	se
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k6eAd1	ještě
uhájily	uhájit	k5eAaPmAgFnP	uhájit
samostatnost	samostatnost	k1gFnSc4	samostatnost
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
návrhy	návrh	k1gInPc1	návrh
na	na	k7c4	na
připojení	připojení	k1gNnSc4	připojení
těchto	tento	k3xDgFnPc2	tento
obcí	obec	k1gFnPc2	obec
byly	být	k5eAaImAgInP	být
činěny	činit	k5eAaImNgInP	činit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1946	[number]	k4	1946
<g/>
-	-	kIx~	-
<g/>
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
uvedené	uvedený	k2eAgFnPc1d1	uvedená
obce	obec	k1gFnPc1	obec
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
opět	opět	k6eAd1	opět
nesouhlasily	souhlasit	k5eNaImAgInP	souhlasit
a	a	k8xC	a
nepovažovaly	považovat	k5eNaImAgInP	považovat
to	ten	k3xDgNnSc4	ten
za	za	k7c2	za
výhodné	výhodný	k2eAgFnSc2d1	výhodná
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
připojení	připojení	k1gNnSc3	připojení
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
po	po	k7c6	po
komunistickém	komunistický	k2eAgInSc6d1	komunistický
převratu	převrat	k1gInSc6	převrat
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
shora	shora	k6eAd1	shora
bez	bez	k7c2	bez
souhlasu	souhlas	k1gInSc2	souhlas
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
Okresní	okresní	k2eAgInSc1d1	okresní
národní	národní	k2eAgInSc1d1	národní
výbor	výbor	k1gInSc1	výbor
Prostějov	Prostějov	k1gInSc1	Prostějov
o	o	k7c4	o
připojení	připojení	k1gNnSc4	připojení
těchto	tento	k3xDgFnPc2	tento
čtyř	čtyři	k4xCgFnPc2	čtyři
obcí	obec	k1gFnPc2	obec
k	k	k7c3	k
Prostějovu	Prostějov	k1gInSc3	Prostějov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
byly	být	k5eAaImAgFnP	být
Vrahovice	Vrahovice	k1gFnPc1	Vrahovice
<g/>
,	,	kIx,	,
Držovice	Držovice	k1gFnSc1	Držovice
a	a	k8xC	a
Krasice	Krasice	k1gFnSc2	Krasice
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
Krajského	krajský	k2eAgInSc2d1	krajský
národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
opět	opět	k6eAd1	opět
osamostatněny	osamostatněn	k2eAgFnPc1d1	osamostatněna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
byly	být	k5eAaImAgInP	být
k	k	k7c3	k
Prostějovu	Prostějov	k1gInSc3	Prostějov
opět	opět	k6eAd1	opět
připojeny	připojit	k5eAaPmNgFnP	připojit
Krasice	Krasice	k1gFnPc1	Krasice
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
Vrahovice	Vrahovice	k1gFnSc2	Vrahovice
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
Čechovice	Čechovice	k1gFnSc2	Čechovice
a	a	k8xC	a
Domamyslice	Domamyslice	k1gFnSc2	Domamyslice
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
Mostkovice	Mostkovice	k1gFnPc1	Mostkovice
a	a	k8xC	a
1981	[number]	k4	1981
Žešov	Žešovo	k1gNnPc2	Žešovo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
se	se	k3xPyFc4	se
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Mostkovic	Mostkovice	k1gFnPc2	Mostkovice
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
pro	pro	k7c4	pro
opětovné	opětovný	k2eAgNnSc4d1	opětovné
osamostatnění	osamostatnění	k1gNnSc4	osamostatnění
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
konalo	konat	k5eAaImAgNnS	konat
referendum	referendum	k1gNnSc1	referendum
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgNnSc6	který
se	s	k7c7	s
96	[number]	k4	96
%	%	kIx~	%
občanů	občan	k1gMnPc2	občan
(	(	kIx(	(
<g/>
při	při	k7c6	při
80	[number]	k4	80
<g/>
%	%	kIx~	%
účasti	účast	k1gFnSc2	účast
<g/>
)	)	kIx)	)
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
pro	pro	k7c4	pro
oddělení	oddělení	k1gNnSc4	oddělení
Mostkovic	Mostkovice	k1gFnPc2	Mostkovice
od	od	k7c2	od
Prostějova	Prostějov	k1gInSc2	Prostějov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
po	po	k7c6	po
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
sílila	sílit	k5eAaImAgFnS	sílit
nespokojenost	nespokojenost	k1gFnSc1	nespokojenost
obyvatel	obyvatel	k1gMnPc2	obyvatel
Vrahovic	Vrahovice	k1gFnPc2	Vrahovice
a	a	k8xC	a
Držovic	Držovice	k1gFnPc2	Držovice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
městských	městský	k2eAgFnPc6d1	městská
částech	část	k1gFnPc6	část
se	se	k3xPyFc4	se
projevovala	projevovat	k5eAaImAgFnS	projevovat
touha	touha	k1gFnSc1	touha
po	po	k7c6	po
znovuobnovení	znovuobnovení	k1gNnSc6	znovuobnovení
samostatnosti	samostatnost	k1gFnSc2	samostatnost
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
kritizovány	kritizovat	k5eAaImNgFnP	kritizovat
nedostatečné	dostatečný	k2eNgFnPc1d1	nedostatečná
investice	investice	k1gFnPc1	investice
<g/>
,	,	kIx,	,
přehlížení	přehlížený	k2eAgMnPc1d1	přehlížený
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
představitelů	představitel	k1gMnPc2	představitel
města	město	k1gNnSc2	město
a	a	k8xC	a
malá	malý	k2eAgFnSc1d1	malá
možnost	možnost	k1gFnSc1	možnost
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
dění	dění	k1gNnSc4	dění
<g/>
.	.	kIx.	.
</s>
<s>
Představitelé	představitel	k1gMnPc1	představitel
města	město	k1gNnSc2	město
naopak	naopak	k6eAd1	naopak
argumentovali	argumentovat	k5eAaImAgMnP	argumentovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
těchto	tento	k3xDgFnPc2	tento
částí	část	k1gFnPc2	část
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
investovat	investovat	k5eAaBmF	investovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
Držovice	Držovice	k1gFnSc1	Držovice
po	po	k7c6	po
referendu	referendum	k1gNnSc6	referendum
osamostatnily	osamostatnit	k5eAaPmAgInP	osamostatnit
<g/>
,	,	kIx,	,
Vrahovice	Vrahovice	k1gFnPc1	Vrahovice
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
částí	část	k1gFnSc7	část
Prostějova	Prostějov	k1gInSc2	Prostějov
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
Prostějov	Prostějov	k1gInSc1	Prostějov
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
Prostějov	Prostějov	k1gInSc1	Prostějov
Čechovice	Čechovice	k1gFnSc2	Čechovice
Čechůvky	Čechůvka	k1gFnSc2	Čechůvka
Domamyslice	Domamyslice	k1gFnSc2	Domamyslice
Krasice	Krasice	k1gFnSc2	Krasice
Vrahovice	Vrahovice	k1gFnPc1	Vrahovice
Žešov	Žešov	k1gInSc4	Žešov
Borlänge	Borläng	k1gFnSc2	Borläng
<g/>
,	,	kIx,	,
Švédsko	Švédsko	k1gNnSc1	Švédsko
Środa	Środa	k1gFnSc1	Środa
Wielkopolska	Wielkopolska	k1gFnSc1	Wielkopolska
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
Hoyerswerda	Hoyerswerda	k1gFnSc1	Hoyerswerda
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
Tatry	Tatra	k1gFnSc2	Tatra
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
St.	st.	kA	st.
Pölten	Pölten	k2eAgMnSc1d1	Pölten
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
</s>
