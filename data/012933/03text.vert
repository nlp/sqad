<p>
<s>
Plynový	plynový	k2eAgInSc1d1	plynový
kotel	kotel	k1gInSc1	kotel
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
topného	topný	k2eAgInSc2d1	topný
kotle	kotel	k1gInSc2	kotel
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
spalováním	spalování	k1gNnSc7	spalování
plynných	plynný	k2eAgNnPc2d1	plynné
paliv	palivo	k1gNnPc2	palivo
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
teplo	teplo	k1gNnSc1	teplo
<g/>
,	,	kIx,	,
kterým	který	k3yRgNnSc7	který
se	se	k3xPyFc4	se
ohřívá	ohřívat	k5eAaImIp3nS	ohřívat
teplonosná	teplonosný	k2eAgFnSc1d1	teplonosná
látka	látka	k1gFnSc1	látka
(	(	kIx(	(
<g/>
voda	voda	k1gFnSc1	voda
nebo	nebo	k8xC	nebo
nemrznoucí	mrznoucí	k2eNgFnSc1d1	nemrznoucí
kapalina	kapalina	k1gFnSc1	kapalina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Plynové	plynový	k2eAgInPc1d1	plynový
kotle	kotel	k1gInPc1	kotel
jsou	být	k5eAaImIp3nP	být
vhodné	vhodný	k2eAgFnPc1d1	vhodná
pro	pro	k7c4	pro
domácnosti	domácnost	k1gFnPc4	domácnost
i	i	k9	i
pro	pro	k7c4	pro
jiné	jiný	k2eAgFnPc4d1	jiná
rozsáhlejší	rozsáhlý	k2eAgFnPc4d2	rozsáhlejší
budovy	budova	k1gFnPc4	budova
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
výhodou	výhoda	k1gFnSc7	výhoda
je	být	k5eAaImIp3nS	být
schopnost	schopnost	k1gFnSc4	schopnost
úsporně	úsporně	k6eAd1	úsporně
vytápět	vytápět	k5eAaImF	vytápět
zvolený	zvolený	k2eAgInSc4d1	zvolený
prostor	prostor	k1gInSc4	prostor
a	a	k8xC	a
také	také	k9	také
úsporně	úsporně	k6eAd1	úsporně
ohřívat	ohřívat	k5eAaImF	ohřívat
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Vytápění	vytápění	k1gNnSc1	vytápění
plynovým	plynový	k2eAgInSc7d1	plynový
kotlem	kotel	k1gInSc7	kotel
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
ekologickému	ekologický	k2eAgMnSc3d1	ekologický
a	a	k8xC	a
efektivnímu	efektivní	k2eAgInSc3d1	efektivní
způsobu	způsob	k1gInSc3	způsob
vytápění	vytápění	k1gNnSc2	vytápění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kotlech	kotel	k1gInPc6	kotel
se	se	k3xPyFc4	se
topí	topit	k5eAaImIp3nS	topit
zemním	zemní	k2eAgInSc7d1	zemní
plynem	plyn	k1gInSc7	plyn
<g/>
,	,	kIx,	,
propanem	propan	k1gInSc7	propan
nebo	nebo	k8xC	nebo
propan-butanem	propanutan	k1gInSc7	propan-butan
<g/>
.	.	kIx.	.
</s>
<s>
Regulaci	regulace	k1gFnSc4	regulace
a	a	k8xC	a
provoz	provoz	k1gInSc4	provoz
kotle	kotel	k1gInSc2	kotel
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
moderní	moderní	k2eAgFnSc1d1	moderní
elektronická	elektronický	k2eAgFnSc1d1	elektronická
řídící	řídící	k2eAgFnSc1d1	řídící
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k6eAd1	právě
plyn	plyn	k1gInSc1	plyn
a	a	k8xC	a
propan	propan	k1gInSc1	propan
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
naftou	nafta	k1gFnSc7	nafta
a	a	k8xC	a
benzínem	benzín	k1gInSc7	benzín
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
těm	ten	k3xDgNnPc3	ten
nejvýhřevnějším	výhřevný	k2eAgNnPc3d3	výhřevný
palivům	palivo	k1gNnPc3	palivo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Princip	princip	k1gInSc1	princip
plynového	plynový	k2eAgInSc2d1	plynový
kotle	kotel	k1gInSc2	kotel
==	==	k?	==
</s>
</p>
<p>
<s>
Kotel	kotel	k1gInSc1	kotel
při	při	k7c6	při
spalování	spalování	k1gNnSc6	spalování
plynu	plyn	k1gInSc2	plyn
ohřívá	ohřívat	k5eAaImIp3nS	ohřívat
ve	v	k7c6	v
výměníku	výměník	k1gInSc6	výměník
teplonosnou	teplonosný	k2eAgFnSc4d1	teplonosná
látku	látka	k1gFnSc4	látka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
rozvedena	rozvést	k5eAaPmNgFnS	rozvést
do	do	k7c2	do
radiátorů	radiátor	k1gInPc2	radiátor
nebo	nebo	k8xC	nebo
jiných	jiný	k2eAgNnPc2d1	jiné
topidel	topidlo	k1gNnPc2	topidlo
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
klesne	klesnout	k5eAaPmIp3nS	klesnout
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
místnosti	místnost	k1gFnSc6	místnost
pod	pod	k7c4	pod
určenou	určený	k2eAgFnSc4d1	určená
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
sepne	sepnout	k5eAaPmIp3nS	sepnout
termostat	termostat	k1gInSc1	termostat
plynový	plynový	k2eAgInSc1d1	plynový
kotel	kotel	k1gInSc4	kotel
a	a	k8xC	a
ten	ten	k3xDgInSc1	ten
ohřeje	ohřát	k5eAaPmIp3nS	ohřát
další	další	k2eAgFnSc4d1	další
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
může	moct	k5eAaImIp3nS	moct
pracovat	pracovat	k5eAaImF	pracovat
v	v	k7c6	v
plně	plně	k6eAd1	plně
automatickém	automatický	k2eAgInSc6d1	automatický
režimu	režim	k1gInSc6	režim
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgNnSc1d1	základní
dělení	dělení	k1gNnSc1	dělení
kotlů	kotel	k1gInPc2	kotel
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Stacionární	stacionární	k2eAgInPc4d1	stacionární
a	a	k8xC	a
závěsné	závěsný	k2eAgInPc4d1	závěsný
kotle	kotel	k1gInPc4	kotel
===	===	k?	===
</s>
</p>
<p>
<s>
Plynové	plynový	k2eAgInPc1d1	plynový
kotle	kotel	k1gInPc1	kotel
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
stacionární	stacionární	k2eAgInPc4d1	stacionární
a	a	k8xC	a
závěsné	závěsný	k2eAgInPc4d1	závěsný
<g/>
.	.	kIx.	.
</s>
<s>
Stacionární	stacionární	k2eAgInPc1d1	stacionární
kotle	kotel	k1gInPc1	kotel
nacházejí	nacházet	k5eAaImIp3nP	nacházet
své	svůj	k3xOyFgNnSc4	svůj
uplatnění	uplatnění	k1gNnSc4	uplatnění
např.	např.	kA	např.
v	v	k7c6	v
kotelnách	kotelna	k1gFnPc6	kotelna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nahrazují	nahrazovat	k5eAaImIp3nP	nahrazovat
původní	původní	k2eAgInPc4d1	původní
kotle	kotel	k1gInPc4	kotel
na	na	k7c4	na
pevná	pevný	k2eAgNnPc4d1	pevné
paliva	palivo	k1gNnPc4	palivo
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
kladen	kladen	k2eAgMnSc1d1	kladen
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
vzhled	vzhled	k1gInSc4	vzhled
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
velikost	velikost	k1gFnSc1	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Závěsné	závěsný	k2eAgInPc1d1	závěsný
kotle	kotel	k1gInPc1	kotel
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
oproti	oproti	k7c3	oproti
stacionárním	stacionární	k2eAgFnPc3d1	stacionární
menší	malý	k2eAgFnPc4d2	menší
a	a	k8xC	a
často	často	k6eAd1	často
s	s	k7c7	s
lepším	dobrý	k2eAgInSc7d2	lepší
designem	design	k1gInSc7	design
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
i	i	k9	i
v	v	k7c6	v
běžných	běžný	k2eAgFnPc6d1	běžná
domácnostech	domácnost	k1gFnPc6	domácnost
<g/>
.	.	kIx.	.
</s>
<s>
Neruší	rušit	k5eNaImIp3nS	rušit
komfort	komfort	k1gInSc4	komfort
domácností	domácnost	k1gFnPc2	domácnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
naprogramovat	naprogramovat	k5eAaPmF	naprogramovat
jejich	jejich	k3xOp3gNnSc4	jejich
nastavení	nastavení	k1gNnSc4	nastavení
nebo	nebo	k8xC	nebo
funkcí	funkce	k1gFnSc7	funkce
rychlého	rychlý	k2eAgInSc2d1	rychlý
ohřevu	ohřev	k1gInSc2	ohřev
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
ho	on	k3xPp3gMnSc4	on
vhodně	vhodně	k6eAd1	vhodně
dotváří	dotvářet	k5eAaImIp3nS	dotvářet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Klasické	klasický	k2eAgInPc1d1	klasický
a	a	k8xC	a
kondenzační	kondenzační	k2eAgInPc1d1	kondenzační
plynové	plynový	k2eAgInPc1d1	plynový
kotle	kotel	k1gInPc1	kotel
===	===	k?	===
</s>
</p>
<p>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
kotle	kotel	k1gInPc1	kotel
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
klasické	klasický	k2eAgInPc4d1	klasický
plynové	plynový	k2eAgInPc4d1	plynový
kotle	kotel	k1gInPc4	kotel
a	a	k8xC	a
kondenzační	kondenzační	k2eAgInPc4d1	kondenzační
plynové	plynový	k2eAgInPc4d1	plynový
kotle	kotel	k1gInPc4	kotel
<g/>
.	.	kIx.	.
</s>
<s>
Kondenzační	kondenzační	k2eAgInSc1d1	kondenzační
kotel	kotel	k1gInSc1	kotel
se	se	k3xPyFc4	se
od	od	k7c2	od
normálního	normální	k2eAgInSc2d1	normální
kotle	kotel	k1gInSc2	kotel
liší	lišit	k5eAaImIp3nP	lišit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
dokáže	dokázat	k5eAaPmIp3nS	dokázat
využít	využít	k5eAaPmF	využít
i	i	k9	i
část	část	k1gFnSc4	část
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
by	by	kYmCp3nS	by
jinak	jinak	k6eAd1	jinak
kotel	kotel	k1gInSc4	kotel
ztratil	ztratit	k5eAaPmAgMnS	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
spalování	spalování	k1gNnSc6	spalování
plynu	plyn	k1gInSc2	plyn
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
kotli	kotel	k1gInSc6	kotel
chemickou	chemický	k2eAgFnSc4d1	chemická
reakcí	reakce	k1gFnSc7	reakce
určité	určitý	k2eAgNnSc4d1	určité
množství	množství	k1gNnSc4	množství
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
oxidem	oxid	k1gInSc7	oxid
uhličitým	uhličitý	k2eAgInSc7d1	uhličitý
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
produkty	produkt	k1gInPc7	produkt
hoření	hoření	k1gNnSc2	hoření
tvoří	tvořit	k5eAaImIp3nP	tvořit
spaliny	spaliny	k1gFnPc1	spaliny
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
odváděna	odvádět	k5eAaImNgFnS	odvádět
z	z	k7c2	z
kotle	kotel	k1gInSc2	kotel
komínem	komín	k1gInSc7	komín
do	do	k7c2	do
ovzduší	ovzduší	k1gNnSc2	ovzduší
<g/>
.	.	kIx.	.
</s>
<s>
Jenže	jenže	k8xC	jenže
kondenzační	kondenzační	k2eAgInPc1d1	kondenzační
kotle	kotel	k1gInPc1	kotel
dokáží	dokázat	k5eAaPmIp3nP	dokázat
využít	využít	k5eAaPmF	využít
i	i	k9	i
kondenzační	kondenzační	k2eAgNnSc4d1	kondenzační
teplo	teplo	k1gNnSc4	teplo
této	tento	k3xDgFnSc2	tento
páry	pára	k1gFnSc2	pára
<g/>
.	.	kIx.	.
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
to	ten	k3xDgNnSc1	ten
jejich	jejich	k3xOp3gFnSc1	jejich
pracovní	pracovní	k2eAgFnSc1d1	pracovní
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
teplota	teplota	k1gFnSc1	teplota
varu	var	k1gInSc2	var
vody	voda	k1gFnSc2	voda
za	za	k7c2	za
běžného	běžný	k2eAgInSc2d1	běžný
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
se	se	k3xPyFc4	se
spaliny	spaliny	k1gFnPc1	spaliny
ochladí	ochladit	k5eAaPmIp3nP	ochladit
pod	pod	k7c4	pod
teplotu	teplota	k1gFnSc4	teplota
jejich	jejich	k3xOp3gInSc2	jejich
rosného	rosný	k2eAgInSc2d1	rosný
bodu	bod	k1gInSc2	bod
<g/>
,	,	kIx,	,
dojde	dojít	k5eAaPmIp3nS	dojít
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
skupenství	skupenství	k1gNnSc2	skupenství
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kondenzaci	kondenzace	k1gFnSc3	kondenzace
obsažené	obsažený	k2eAgFnSc2d1	obsažená
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
ve	v	k7c6	v
výměníku	výměník	k1gInSc6	výměník
a	a	k8xC	a
k	k	k7c3	k
dodatečnému	dodatečný	k2eAgInSc3d1	dodatečný
ohřátí	ohřátý	k2eAgMnPc1d1	ohřátý
topné	topný	k2eAgFnPc4d1	topná
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomuto	tento	k3xDgInSc3	tento
procesu	proces	k1gInSc3	proces
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
teoreticky	teoreticky	k6eAd1	teoreticky
normativního	normativní	k2eAgNnSc2d1	normativní
využití	využití	k1gNnSc2	využití
až	až	k9	až
108	[number]	k4	108
%	%	kIx~	%
a	a	k8xC	a
spotřeba	spotřeba	k1gFnSc1	spotřeba
plynu	plyn	k1gInSc2	plyn
může	moct	k5eAaImIp3nS	moct
klesnout	klesnout	k5eAaPmF	klesnout
až	až	k9	až
o	o	k7c4	o
20	[number]	k4	20
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
kondenzačního	kondenzační	k2eAgInSc2d1	kondenzační
kotle	kotel	k1gInSc2	kotel
je	být	k5eAaImIp3nS	být
však	však	k9	však
mnohdy	mnohdy	k6eAd1	mnohdy
až	až	k9	až
dvakrát	dvakrát	k6eAd1	dvakrát
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
cena	cena	k1gFnSc1	cena
klasického	klasický	k2eAgInSc2d1	klasický
kotle	kotel	k1gInSc2	kotel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výhody	výhoda	k1gFnPc1	výhoda
a	a	k8xC	a
nevýhody	nevýhoda	k1gFnPc1	nevýhoda
plynových	plynový	k2eAgInPc2d1	plynový
kotlů	kotel	k1gInPc2	kotel
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Výhody	výhoda	k1gFnPc1	výhoda
===	===	k?	===
</s>
</p>
<p>
<s>
Při	při	k7c6	při
spalování	spalování	k1gNnSc6	spalování
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
nevznikají	vznikat	k5eNaImIp3nP	vznikat
žádné	žádný	k3yNgFnPc1	žádný
nespálené	spálený	k2eNgFnPc1d1	nespálená
částice	částice	k1gFnPc1	částice
(	(	kIx(	(
<g/>
prach	prach	k1gInSc1	prach
<g/>
,	,	kIx,	,
saze	saze	k1gFnSc1	saze
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
největším	veliký	k2eAgInSc7d3	veliký
problémem	problém	k1gInSc7	problém
čistoty	čistota	k1gFnSc2	čistota
ovzduší	ovzduší	k1gNnSc2	ovzduší
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
spalování	spalování	k1gNnSc6	spalování
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
nevznikají	vznikat	k5eNaImIp3nP	vznikat
vysoce	vysoce	k6eAd1	vysoce
nebezpečné	bezpečný	k2eNgInPc4d1	nebezpečný
dioxiny	dioxin	k1gInPc4	dioxin
a	a	k8xC	a
furany	furan	k1gInPc4	furan
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgMnSc3	ten
u	u	k7c2	u
spalování	spalování	k1gNnSc2	spalování
uhlí	uhlí	k1gNnSc2	uhlí
a	a	k8xC	a
biomasy	biomasa	k1gFnSc2	biomasa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Emise	emise	k1gFnPc1	emise
oxidu	oxid	k1gInSc2	oxid
uhelnatého	uhelnatý	k2eAgInSc2d1	uhelnatý
a	a	k8xC	a
oxidů	oxid	k1gInPc2	oxid
síry	síra	k1gFnSc2	síra
jsou	být	k5eAaImIp3nP	být
zanedbatelné	zanedbatelný	k2eAgFnPc1d1	zanedbatelná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Emise	emise	k1gFnPc1	emise
oxidů	oxid	k1gInPc2	oxid
dusíku	dusík	k1gInSc2	dusík
a	a	k8xC	a
hlavního	hlavní	k2eAgInSc2d1	hlavní
skleníkového	skleníkový	k2eAgInSc2d1	skleníkový
plynu	plyn	k1gInSc2	plyn
CO2	CO2	k1gFnSc2	CO2
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
25	[number]	k4	25
-	-	kIx~	-
50	[number]	k4	50
<g/>
%	%	kIx~	%
emisí	emise	k1gFnPc2	emise
vznikajících	vznikající	k2eAgFnPc2d1	vznikající
při	při	k7c6	při
spalování	spalování	k1gNnSc6	spalování
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
,	,	kIx,	,
topného	topný	k2eAgInSc2d1	topný
oleje	olej	k1gInSc2	olej
nebo	nebo	k8xC	nebo
biomasy	biomasa	k1gFnSc2	biomasa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
dostatek	dostatek	k1gInSc4	dostatek
a	a	k8xC	a
budováním	budování	k1gNnSc7	budování
a	a	k8xC	a
propojováním	propojování	k1gNnSc7	propojování
sítě	síť	k1gFnSc2	síť
páteřních	páteřní	k2eAgInPc2d1	páteřní
plynovodů	plynovod	k1gInPc2	plynovod
a	a	k8xC	a
podzemních	podzemní	k2eAgInPc2d1	podzemní
zásobníků	zásobník	k1gInPc2	zásobník
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
jeho	jeho	k3xOp3gFnSc4	jeho
dostupnost	dostupnost	k1gFnSc4	dostupnost
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
dodávek	dodávka	k1gFnPc2	dodávka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Účinnost	účinnost	k1gFnSc1	účinnost
využití	využití	k1gNnSc2	využití
energetického	energetický	k2eAgInSc2d1	energetický
obsahu	obsah	k1gInSc2	obsah
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
pokrokové	pokrokový	k2eAgFnSc3d1	pokroková
technologii	technologie	k1gFnSc3	technologie
a	a	k8xC	a
dokonalé	dokonalý	k2eAgFnSc3d1	dokonalá
regulaci	regulace	k1gFnSc3	regulace
u	u	k7c2	u
moderních	moderní	k2eAgInPc2d1	moderní
spotřebičů	spotřebič	k1gInPc2	spotřebič
výrazně	výrazně	k6eAd1	výrazně
vyšší	vysoký	k2eAgFnPc1d2	vyšší
než	než	k8xS	než
u	u	k7c2	u
ostatních	ostatní	k2eAgInPc2d1	ostatní
neobnovitelných	obnovitelný	k2eNgInPc2d1	neobnovitelný
i	i	k8xC	i
obnovitelných	obnovitelný	k2eAgInPc2d1	obnovitelný
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výhody	výhoda	k1gFnPc1	výhoda
uvedené	uvedený	k2eAgFnPc1d1	uvedená
u	u	k7c2	u
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
zachovány	zachovat	k5eAaPmNgInP	zachovat
i	i	k9	i
u	u	k7c2	u
zkapalněných	zkapalněný	k2eAgInPc2d1	zkapalněný
plynů	plyn	k1gInPc2	plyn
(	(	kIx(	(
<g/>
propan	propan	k1gInSc1	propan
<g/>
,	,	kIx,	,
propan-butan	propanutan	k1gInSc1	propan-butan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
možné	možný	k2eAgNnSc1d1	možné
použít	použít	k5eAaPmF	použít
i	i	k9	i
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
není	být	k5eNaImIp3nS	být
dostupný	dostupný	k2eAgInSc1d1	dostupný
zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nenáročnost	nenáročnost	k1gFnSc1	nenáročnost
obsluhy	obsluha	k1gFnSc2	obsluha
<g/>
,	,	kIx,	,
majiteli	majitel	k1gMnSc3	majitel
odpadne	odpadnout	k5eAaPmIp3nS	odpadnout
skládání	skládání	k1gNnSc4	skládání
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
,	,	kIx,	,
pravidelné	pravidelný	k2eAgNnSc1d1	pravidelné
přikládání	přikládání	k1gNnSc1	přikládání
nebo	nebo	k8xC	nebo
vynášení	vynášení	k1gNnSc1	vynášení
žhavého	žhavý	k2eAgInSc2d1	žhavý
popela	popel	k1gInSc2	popel
<g/>
.	.	kIx.	.
</s>
<s>
Zbaví	zbavit	k5eAaPmIp3nS	zbavit
se	se	k3xPyFc4	se
i	i	k9	i
starostí	starost	k1gFnSc7	starost
s	s	k7c7	s
každoročním	každoroční	k2eAgNnSc7d1	každoroční
nakupováním	nakupování	k1gNnSc7	nakupování
paliva	palivo	k1gNnSc2	palivo
či	či	k8xC	či
se	s	k7c7	s
skladovacími	skladovací	k2eAgInPc7d1	skladovací
prostory	prostor	k1gInPc7	prostor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nízká	nízký	k2eAgFnSc1d1	nízká
hlučnost	hlučnost	k1gFnSc1	hlučnost
kotlů	kotel	k1gInPc2	kotel
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
systém	systém	k1gInSc1	systém
nasávání	nasávání	k1gNnSc2	nasávání
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
spalování	spalování	k1gNnSc2	spalování
a	a	k8xC	a
odvodu	odvod	k1gInSc2	odvod
spalin	spaliny	k1gFnPc2	spaliny
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
dělá	dělat	k5eAaImIp3nS	dělat
ideální	ideální	k2eAgNnSc1d1	ideální
vytápění	vytápění	k1gNnSc1	vytápění
do	do	k7c2	do
rodinných	rodinný	k2eAgInPc2d1	rodinný
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Plynové	plynový	k2eAgInPc1d1	plynový
kotle	kotel	k1gInPc1	kotel
se	se	k3xPyFc4	se
obecně	obecně	k6eAd1	obecně
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
nejvýhodnější	výhodný	k2eAgInSc4d3	nejvýhodnější
způsob	způsob	k1gInSc4	způsob
vytápění	vytápění	k1gNnSc2	vytápění
všude	všude	k6eAd1	všude
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
plynová	plynový	k2eAgFnSc1d1	plynová
přípojka	přípojka	k1gFnSc1	přípojka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nevýhody	nevýhoda	k1gFnSc2	nevýhoda
===	===	k?	===
</s>
</p>
<p>
<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
pořizovací	pořizovací	k2eAgFnSc1d1	pořizovací
cena	cena	k1gFnSc1	cena
zejména	zejména	k9	zejména
u	u	k7c2	u
kondenzačních	kondenzační	k2eAgInPc2d1	kondenzační
kotlů	kotel	k1gInPc2	kotel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
použití	použití	k1gNnSc6	použití
propanu	propan	k1gInSc2	propan
nebo	nebo	k8xC	nebo
propan-butanu	propanutan	k1gInSc2	propan-butan
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
mít	mít	k5eAaImF	mít
jejich	jejich	k3xOp3gInSc4	jejich
samostatný	samostatný	k2eAgInSc4d1	samostatný
zásobník	zásobník	k1gInSc4	zásobník
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
ochranou	ochrana	k1gFnSc7	ochrana
zónu	zóna	k1gFnSc4	zóna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
omezuje	omezovat	k5eAaImIp3nS	omezovat
pohyb	pohyb	k1gInSc4	pohyb
na	na	k7c6	na
pozemku	pozemek	k1gInSc6	pozemek
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
umístění	umístění	k1gNnSc1	umístění
a	a	k8xC	a
zavedení	zavedení	k1gNnSc1	zavedení
tedy	tedy	k9	tedy
představuje	představovat	k5eAaImIp3nS	představovat
další	další	k2eAgFnSc4d1	další
investici	investice	k1gFnSc4	investice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
cenou	cena	k1gFnSc7	cena
použitých	použitý	k2eAgInPc2d1	použitý
plynů	plyn	k1gInPc2	plyn
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
pořizovací	pořizovací	k2eAgFnSc4d1	pořizovací
a	a	k8xC	a
provozní	provozní	k2eAgFnSc4d1	provozní
cenu	cena	k1gFnSc4	cena
tohoto	tento	k3xDgNnSc2	tento
řešení	řešení	k1gNnSc2	řešení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
pořizovací	pořizovací	k2eAgFnSc3d1	pořizovací
ceně	cena	k1gFnSc3	cena
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
připočítat	připočítat	k5eAaPmF	připočítat
také	také	k9	také
cenu	cena	k1gFnSc4	cena
stavby	stavba	k1gFnSc2	stavba
nebo	nebo	k8xC	nebo
vložkování	vložkování	k1gNnSc2	vložkování
komína	komín	k1gInSc2	komín
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
odolný	odolný	k2eAgMnSc1d1	odolný
vůči	vůči	k7c3	vůči
kondenzátu	kondenzát	k1gInSc3	kondenzát
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
komín	komín	k1gInSc1	komín
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
kotel	kotel	k1gInSc4	kotel
odkouřit	odkouřit	k5eAaPmF	odkouřit
přes	přes	k7c4	přes
fasádu	fasáda	k1gFnSc4	fasáda
<g/>
.	.	kIx.	.
</s>
<s>
Nutné	nutný	k2eAgNnSc1d1	nutné
je	být	k5eAaImIp3nS	být
i	i	k9	i
napojení	napojení	k1gNnSc1	napojení
na	na	k7c4	na
kanalizaci	kanalizace	k1gFnSc4	kanalizace
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
odtékají	odtékat	k5eAaImIp3nP	odtékat
litry	litr	k1gInPc4	litr
kondenzátu	kondenzát	k1gInSc2	kondenzát
za	za	k7c4	za
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Opravy	oprava	k1gFnPc1	oprava
kondenzačních	kondenzační	k2eAgInPc2d1	kondenzační
kotlů	kotel	k1gInPc2	kotel
jsou	být	k5eAaImIp3nP	být
nákladnější	nákladný	k2eAgFnPc1d2	nákladnější
než	než	k8xS	než
u	u	k7c2	u
ostatních	ostatní	k2eAgInPc2d1	ostatní
druhů	druh	k1gInPc2	druh
kotlů	kotel	k1gInPc2	kotel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nutnost	nutnost	k1gFnSc1	nutnost
před	před	k7c7	před
každou	každý	k3xTgFnSc7	každý
sezónou	sezóna	k1gFnSc7	sezóna
nechat	nechat	k5eAaPmF	nechat
plynový	plynový	k2eAgInSc4d1	plynový
kotel	kotel	k1gInSc4	kotel
vyčistit	vyčistit	k5eAaPmF	vyčistit
<g/>
,	,	kIx,	,
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
výměníku	výměník	k1gInSc6	výměník
se	se	k3xPyFc4	se
usazuje	usazovat	k5eAaImIp3nS	usazovat
prach	prach	k1gInSc1	prach
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
procházet	procházet	k5eAaImF	procházet
pravidelnými	pravidelný	k2eAgFnPc7d1	pravidelná
jednoročními	jednoroční	k2eAgFnPc7d1	jednoroční
revizemi	revize	k1gFnPc7	revize
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
špatném	špatný	k2eAgNnSc6d1	špatné
spalování	spalování	k1gNnSc6	spalování
plynu	plyn	k1gInSc2	plyn
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
uvolňovat	uvolňovat	k5eAaImF	uvolňovat
nebezpečný	bezpečný	k2eNgInSc1d1	nebezpečný
oxid	oxid	k1gInSc1	oxid
uhelnatý	uhelnatý	k2eAgInSc1d1	uhelnatý
<g/>
.	.	kIx.	.
</s>
<s>
Plynový	plynový	k2eAgInSc1d1	plynový
kotel	kotel	k1gInSc1	kotel
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
také	také	k9	také
zajištěný	zajištěný	k2eAgInSc4d1	zajištěný
dostatečný	dostatečný	k2eAgInSc4d1	dostatečný
přísun	přísun	k1gInSc4	přísun
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
odkouření	odkouření	k1gNnSc2	odkouření
do	do	k7c2	do
komína	komín	k1gInSc2	komín
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
také	také	k9	také
podléhá	podléhat	k5eAaImIp3nS	podléhat
pravidelným	pravidelný	k2eAgFnPc3d1	pravidelná
revizím	revize	k1gFnPc3	revize
<g/>
.	.	kIx.	.
</s>
<s>
Vhodné	vhodný	k2eAgNnSc1d1	vhodné
je	být	k5eAaImIp3nS	být
si	se	k3xPyFc3	se
pořídit	pořídit	k5eAaPmF	pořídit
čidlo	čidlo	k1gNnSc4	čidlo
úniku	únik	k1gInSc2	únik
CO	co	k8xS	co
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
dokáže	dokázat	k5eAaPmIp3nS	dokázat
včas	včas	k6eAd1	včas
varovat	varovat	k5eAaImF	varovat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
poruchy	porucha	k1gFnSc2	porucha
kotle	kotel	k1gInSc2	kotel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zákaz	zákaz	k1gInSc1	zákaz
prodeje	prodej	k1gInSc2	prodej
klasických	klasický	k2eAgInPc2d1	klasický
plynových	plynový	k2eAgInPc2d1	plynový
kotlů	kotel	k1gInPc2	kotel
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
opatření	opatření	k1gNnSc2	opatření
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
nařízení	nařízení	k1gNnSc2	nařízení
č.	č.	k?	č.
813	[number]	k4	813
<g/>
/	/	kIx~	/
<g/>
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
po	po	k7c6	po
26	[number]	k4	26
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2015	[number]	k4	2015
uváděny	uvádět	k5eAaImNgInP	uvádět
na	na	k7c4	na
trh	trh	k1gInSc4	trh
kotle	kotel	k1gInSc2	kotel
na	na	k7c4	na
zemní	zemní	k2eAgInSc4d1	zemní
plyn	plyn	k1gInSc4	plyn
se	s	k7c7	s
jmenovitým	jmenovitý	k2eAgInSc7d1	jmenovitý
výkonem	výkon	k1gInSc7	výkon
do	do	k7c2	do
70	[number]	k4	70
kW	kW	kA	kW
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
sezónní	sezónní	k2eAgFnSc1d1	sezónní
energická	energický	k2eAgFnSc1d1	energická
účinnost	účinnost	k1gFnSc1	účinnost
vytápění	vytápění	k1gNnSc2	vytápění
je	být	k5eAaImIp3nS	být
nižší	nízký	k2eAgMnSc1d2	nižší
než	než	k8xS	než
86	[number]	k4	86
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
pak	pak	k6eAd1	pak
vyřazuje	vyřazovat	k5eAaImIp3nS	vyřazovat
všechny	všechen	k3xTgInPc4	všechen
klasické	klasický	k2eAgInPc4d1	klasický
kotle	kotel	k1gInPc4	kotel
a	a	k8xC	a
na	na	k7c4	na
trh	trh	k1gInSc4	trh
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
uváděny	uvádět	k5eAaImNgInP	uvádět
pouze	pouze	k6eAd1	pouze
kondenzační	kondenzační	k2eAgInPc1d1	kondenzační
plynové	plynový	k2eAgInPc1d1	plynový
kotle	kotel	k1gInPc1	kotel
<g/>
.	.	kIx.	.
</s>
<s>
Klasické	klasický	k2eAgInPc1d1	klasický
kotle	kotel	k1gInPc1	kotel
lze	lze	k6eAd1	lze
samozřejmě	samozřejmě	k6eAd1	samozřejmě
provozovat	provozovat	k5eAaImF	provozovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
firmy	firma	k1gFnPc1	firma
mohou	moct	k5eAaImIp3nP	moct
už	už	k6eAd1	už
jen	jen	k9	jen
doprodat	doprodat	k5eAaPmF	doprodat
své	svůj	k3xOyFgFnPc4	svůj
skladové	skladový	k2eAgFnPc4d1	skladová
zásoby	zásoba	k1gFnPc4	zásoba
nekondenzačních	kondenzační	k2eNgInPc2d1	kondenzační
plynových	plynový	k2eAgInPc2d1	plynový
kotlů	kotel	k1gInPc2	kotel
napojených	napojený	k2eAgFnPc2d1	napojená
do	do	k7c2	do
komína	komín	k1gInSc2	komín
a	a	k8xC	a
nekondenzačních	kondenzační	k2eNgInPc2d1	kondenzační
plynových	plynový	k2eAgInPc2d1	plynový
kotlů	kotel	k1gInPc2	kotel
v	v	k7c6	v
provedení	provedení	k1gNnSc6	provedení
turbo	turba	k1gFnSc5	turba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
představují	představovat	k5eAaImIp3nP	představovat
kotle	kotel	k1gInPc1	kotel
se	s	k7c7	s
jmenovitým	jmenovitý	k2eAgInSc7d1	jmenovitý
výkonem	výkon	k1gInSc7	výkon
do	do	k7c2	do
10	[number]	k4	10
kW	kW	kA	kW
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
kombikotle	kombikotle	k6eAd1	kombikotle
do	do	k7c2	do
30	[number]	k4	30
kW	kW	kA	kW
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
požadavek	požadavek	k1gInSc1	požadavek
na	na	k7c4	na
minimální	minimální	k2eAgFnSc4d1	minimální
sezónní	sezónní	k2eAgFnSc4d1	sezónní
účinnost	účinnost	k1gFnSc4	účinnost
snížen	snížit	k5eAaPmNgInS	snížit
na	na	k7c6	na
75	[number]	k4	75
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
kotle	kotel	k1gInPc1	kotel
lze	lze	k6eAd1	lze
montovat	montovat	k5eAaImF	montovat
jen	jen	k9	jen
jako	jako	k9	jako
náhradu	náhrada	k1gFnSc4	náhrada
stávajícího	stávající	k2eAgInSc2d1	stávající
kotle	kotel	k1gInSc2	kotel
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
instalován	instalovat	k5eAaBmNgInS	instalovat
do	do	k7c2	do
společného	společný	k2eAgInSc2d1	společný
otevřeného	otevřený	k2eAgInSc2d1	otevřený
kouřovodu	kouřovod	k1gInSc2	kouřovod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Této	tento	k3xDgFnSc3	tento
zákaz	zákaz	k1gInSc1	zákaz
je	být	k5eAaImIp3nS	být
motivován	motivovat	k5eAaBmNgInS	motivovat
úsporami	úspora	k1gFnPc7	úspora
<g/>
.	.	kIx.	.
</s>
<s>
Kondenzační	kondenzační	k2eAgInPc1d1	kondenzační
kotle	kotel	k1gInPc1	kotel
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
efektivnější	efektivní	k2eAgInSc4d2	efektivnější
zdroj	zdroj	k1gInSc4	zdroj
vytápění	vytápění	k1gNnSc2	vytápění
než	než	k8xS	než
běžné	běžný	k2eAgInPc1d1	běžný
plynové	plynový	k2eAgInPc1d1	plynový
kotle	kotel	k1gInPc1	kotel
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgInPc2	jenž
jsou	být	k5eAaImIp3nP	být
spaliny	spaliny	k1gFnPc1	spaliny
odváděny	odvádět	k5eAaImNgFnP	odvádět
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
komína	komín	k1gInSc2	komín
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
v	v	k7c6	v
normovaném	normovaný	k2eAgInSc6d1	normovaný
stupni	stupeň	k1gInSc6	stupeň
využití	využití	k1gNnSc2	využití
je	být	k5eAaImIp3nS	být
108	[number]	k4	108
%	%	kIx~	%
u	u	k7c2	u
kondenzačního	kondenzační	k2eAgInSc2d1	kondenzační
kotle	kotel	k1gInSc2	kotel
proti	proti	k7c3	proti
92	[number]	k4	92
%	%	kIx~	%
u	u	k7c2	u
běžného	běžný	k2eAgInSc2d1	běžný
kotle	kotel	k1gInSc2	kotel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
úsporu	úspora	k1gFnSc4	úspora
30	[number]	k4	30
až	až	k9	až
50	[number]	k4	50
%	%	kIx~	%
oproti	oproti	k7c3	oproti
starému	starý	k2eAgInSc3d1	starý
kotli	kotel	k1gInSc3	kotel
<g/>
.	.	kIx.	.
<g/>
Evropská	evropský	k2eAgFnSc1d1	Evropská
i	i	k8xC	i
česká	český	k2eAgFnSc1d1	Česká
legislativa	legislativa	k1gFnSc1	legislativa
dále	daleko	k6eAd2	daleko
počítá	počítat	k5eAaImIp3nS	počítat
i	i	k9	i
s	s	k7c7	s
dalším	další	k2eAgNnSc7d1	další
zpřísněním	zpřísnění	k1gNnSc7	zpřísnění
emisních	emisní	k2eAgFnPc2d1	emisní
norem	norma	k1gFnPc2	norma
<g/>
.	.	kIx.	.
</s>
<s>
Emise	emise	k1gFnPc1	emise
plynových	plynový	k2eAgInPc2d1	plynový
kotlů	kotel	k1gInPc2	kotel
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
obsahovat	obsahovat	k5eAaImF	obsahovat
maximálně	maximálně	k6eAd1	maximálně
65	[number]	k4	65
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
m	m	kA	m
<g/>
3	[number]	k4	3
oxidů	oxid	k1gInPc2	oxid
dusíku	dusík	k1gInSc2	dusík
(	(	kIx(	(
<g/>
NOx	noxa	k1gFnPc2	noxa
<g/>
)	)	kIx)	)
a	a	k8xC	a
80	[number]	k4	80
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
m	m	kA	m
<g/>
3	[number]	k4	3
oxidu	oxid	k1gInSc2	oxid
uhelnatého	uhelnatý	k2eAgInSc2d1	uhelnatý
(	(	kIx(	(
<g/>
CO	co	k3yRnSc1	co
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
stále	stále	k6eAd1	stále
instalovat	instalovat	k5eAaBmF	instalovat
starší	starý	k2eAgInPc1d2	starší
kotle	kotel	k1gInPc1	kotel
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
splňují	splňovat	k5eAaImIp3nP	splňovat
podmínku	podmínka	k1gFnSc4	podmínka
vyhovujícího	vyhovující	k2eAgInSc2d1	vyhovující
stavu	stav	k1gInSc2	stav
spalinových	spalinový	k2eAgFnPc2d1	spalinová
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dotace	dotace	k1gFnPc1	dotace
==	==	k?	==
</s>
</p>
<p>
<s>
Nákup	nákup	k1gInSc4	nákup
mnohem	mnohem	k6eAd1	mnohem
dražšího	drahý	k2eAgInSc2d2	dražší
kondenzačního	kondenzační	k2eAgInSc2d1	kondenzační
plynového	plynový	k2eAgInSc2d1	plynový
kotle	kotel	k1gInSc2	kotel
podporuje	podporovat	k5eAaImIp3nS	podporovat
dotační	dotační	k2eAgInSc4d1	dotační
program	program	k1gInSc4	program
Nová	nový	k2eAgFnSc1d1	nová
zelená	zelený	k2eAgFnSc1d1	zelená
úsporám	úspora	k1gFnPc3	úspora
nebo	nebo	k8xC	nebo
krajské	krajský	k2eAgFnPc4d1	krajská
kotlíkové	kotlíkový	k2eAgFnPc4d1	Kotlíková
dotace	dotace	k1gFnPc4	dotace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jak	jak	k8xS	jak
vybrat	vybrat	k5eAaPmF	vybrat
plynový	plynový	k2eAgInSc4d1	plynový
kotel	kotel	k1gInSc4	kotel
==	==	k?	==
</s>
</p>
<p>
<s>
Výkon	výkon	k1gInSc1	výkon
kotle	kotel	k1gInSc2	kotel
musí	muset	k5eAaImIp3nS	muset
pokrýt	pokrýt	k5eAaPmF	pokrýt
tepelnou	tepelný	k2eAgFnSc4d1	tepelná
ztrátu	ztráta	k1gFnSc4	ztráta
objektu	objekt	k1gInSc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
zateplením	zateplení	k1gNnSc7	zateplení
<g/>
,	,	kIx,	,
kvalitou	kvalita	k1gFnSc7	kvalita
střechy	střecha	k1gFnSc2	střecha
<g/>
,	,	kIx,	,
těsností	těsnost	k1gFnSc7	těsnost
oken	okno	k1gNnPc2	okno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
polohou	poloha	k1gFnSc7	poloha
samotného	samotný	k2eAgInSc2d1	samotný
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
toho	ten	k3xDgMnSc4	ten
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
přesně	přesně	k6eAd1	přesně
navrhnout	navrhnout	k5eAaPmF	navrhnout
řešení	řešení	k1gNnSc4	řešení
<g/>
,	,	kIx,	,
s	s	k7c7	s
tím	ten	k3xDgInSc7	ten
mohou	moct	k5eAaImIp3nP	moct
pomoci	pomoct	k5eAaPmF	pomoct
nejrůznější	různý	k2eAgInPc1d3	nejrůznější
kalkulátory	kalkulátor	k1gInPc1	kalkulátor
tepelných	tepelný	k2eAgFnPc2d1	tepelná
ztrát	ztráta	k1gFnPc2	ztráta
nebo	nebo	k8xC	nebo
odborník	odborník	k1gMnSc1	odborník
<g/>
.	.	kIx.	.
</s>
<s>
Výkony	výkon	k1gInPc4	výkon
kotlů	kotel	k1gInPc2	kotel
pro	pro	k7c4	pro
domácnosti	domácnost	k1gFnPc4	domácnost
a	a	k8xC	a
menší	malý	k2eAgInPc4d2	menší
podnikatelské	podnikatelský	k2eAgInPc4d1	podnikatelský
prostory	prostor	k1gInPc4	prostor
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
5	[number]	k4	5
do	do	k7c2	do
49,9	[number]	k4	49,9
kW	kW	kA	kW
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
plynové	plynový	k2eAgInPc1d1	plynový
kotle	kotel	k1gInPc1	kotel
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
také	také	k9	také
ohřev	ohřev	k1gInSc4	ohřev
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dobré	dobrý	k2eAgNnSc1d1	dobré
se	se	k3xPyFc4	se
rozmyslet	rozmyslet	k5eAaPmF	rozmyslet
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
vody	voda	k1gFnSc2	voda
bude	být	k5eAaImBp3nS	být
potřeba	potřeba	k6eAd1	potřeba
ohřívat	ohřívat	k5eAaImF	ohřívat
<g/>
,	,	kIx,	,
a	a	k8xC	a
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
toho	ten	k3xDgNnSc2	ten
vybrat	vybrat	k5eAaPmF	vybrat
<g/>
.	.	kIx.	.
</s>
<s>
Kotel	kotel	k1gInSc1	kotel
s	s	k7c7	s
průtokovým	průtokový	k2eAgInSc7d1	průtokový
ohřívačem	ohřívač	k1gInSc7	ohřívač
obslouží	obsloužit	k5eAaPmIp3nS	obsloužit
dřez	dřez	k1gInSc1	dřez
<g/>
,	,	kIx,	,
umyvadlo	umyvadlo	k1gNnSc1	umyvadlo
a	a	k8xC	a
vanu	van	k1gInSc2	van
<g/>
.	.	kIx.	.
</s>
<s>
Kotel	kotel	k1gInSc1	kotel
s	s	k7c7	s
vnitřním	vnitřní	k2eAgInSc7d1	vnitřní
bojlerem	bojler	k1gInSc7	bojler
nebo	nebo	k8xC	nebo
externím	externí	k2eAgInSc7d1	externí
zásobníkem	zásobník	k1gInSc7	zásobník
zvládne	zvládnout	k5eAaPmIp3nS	zvládnout
i	i	k9	i
větší	veliký	k2eAgInSc1d2	veliký
odběr	odběr	k1gInSc1	odběr
teplé	teplý	k2eAgFnSc2d1	teplá
užitkové	užitkový	k2eAgFnSc2d1	užitková
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
běžnou	běžný	k2eAgFnSc4d1	běžná
čtyřčlennou	čtyřčlenný	k2eAgFnSc4d1	čtyřčlenná
rodinu	rodina	k1gFnSc4	rodina
stačí	stačit	k5eAaBmIp3nS	stačit
zásobník	zásobník	k1gInSc1	zásobník
na	na	k7c4	na
120	[number]	k4	120
–	–	k?	–
160	[number]	k4	160
litrů	litr	k1gInPc2	litr
a	a	k8xC	a
pro	pro	k7c4	pro
běžný	běžný	k2eAgInSc4d1	běžný
rodinný	rodinný	k2eAgInSc4d1	rodinný
dům	dům	k1gInSc4	dům
kotel	kotel	k1gInSc4	kotel
s	s	k7c7	s
rozsahem	rozsah	k1gInSc7	rozsah
výkonu	výkon	k1gInSc2	výkon
4	[number]	k4	4
–	–	k?	–
20	[number]	k4	20
kW	kW	kA	kW
<g/>
.	.	kIx.	.
<g/>
Výměník	výměník	k1gMnSc1	výměník
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
z	z	k7c2	z
nerezavějících	rezavějící	k2eNgInPc2d1	nerezavějící
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
z	z	k7c2	z
materiálů	materiál	k1gInPc2	materiál
odolných	odolný	k2eAgFnPc2d1	odolná
vůči	vůči	k7c3	vůči
kyselosti	kyselost	k1gFnSc3	kyselost
<g/>
.	.	kIx.	.
</s>
<s>
Kondenzát	kondenzát	k1gInSc1	kondenzát
má	mít	k5eAaImIp3nS	mít
totiž	totiž	k9	totiž
kyselou	kyselý	k2eAgFnSc4d1	kyselá
povahu	povaha	k1gFnSc4	povaha
pH	ph	kA	ph
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Důležitý	důležitý	k2eAgInSc1d1	důležitý
je	být	k5eAaImIp3nS	být
i	i	k9	i
minimální	minimální	k2eAgInSc4d1	minimální
výkon	výkon	k1gInSc4	výkon
kotle	kotel	k1gInSc2	kotel
<g/>
,	,	kIx,	,
čím	co	k3yInSc7	co
nižší	nízký	k2eAgFnSc1d2	nižší
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
hodnota	hodnota	k1gFnSc1	hodnota
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
lépe	dobře	k6eAd2	dobře
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
tím	ten	k3xDgNnSc7	ten
omezeny	omezen	k2eAgInPc1d1	omezen
časté	častý	k2eAgInPc1d1	častý
starty	start	k1gInPc1	start
kotle	kotel	k1gInSc2	kotel
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
prodlužuje	prodlužovat	k5eAaImIp3nS	prodlužovat
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
životnost	životnost	k1gFnSc1	životnost
a	a	k8xC	a
snižuje	snižovat	k5eAaImIp3nS	snižovat
jeho	jeho	k3xOp3gFnSc1	jeho
spotřeba	spotřeba	k1gFnSc1	spotřeba
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
kotel	kotel	k1gInSc1	kotel
instalován	instalovat	k5eAaBmNgInS	instalovat
v	v	k7c6	v
obytných	obytný	k2eAgFnPc6d1	obytná
místnostech	místnost	k1gFnPc6	místnost
či	či	k8xC	či
blízko	blízko	k7c2	blízko
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
by	by	kYmCp3nS	by
být	být	k5eAaImF	být
ověřena	ověřit	k5eAaPmNgFnS	ověřit
i	i	k8xC	i
jeho	jeho	k3xOp3gFnSc4	jeho
hlučnost	hlučnost	k1gFnSc4	hlučnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Vytápění	vytápění	k1gNnSc1	vytápění
</s>
</p>
