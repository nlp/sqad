<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1890	[number]	k4	1890
Malé	Malé	k2eAgFnPc4d1	Malé
Svatoňovice	Svatoňovice	k1gFnPc4	Svatoňovice
–	–	k?	–
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1938	[number]	k4	1938
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
intelektuál	intelektuál	k1gMnSc1	intelektuál
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
překladatel	překladatel	k1gMnSc1	překladatel
a	a	k8xC	a
amatérský	amatérský	k2eAgMnSc1d1	amatérský
fotograf	fotograf	k1gMnSc1	fotograf
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
mladším	mladý	k2eAgMnSc7d2	mladší
bratrem	bratr	k1gMnSc7	bratr
malíře	malíř	k1gMnSc2	malíř
a	a	k8xC	a
spisovatele	spisovatel	k1gMnSc2	spisovatel
Josefa	Josef	k1gMnSc2	Josef
Čapka	Čapek	k1gMnSc2	Čapek
(	(	kIx(	(
<g/>
1887	[number]	k4	1887
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Původ	původ	k1gInSc4	původ
a	a	k8xC	a
studium	studium	k1gNnSc4	studium
===	===	k?	===
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Malých	Malých	k2eAgFnPc6d1	Malých
Svatoňovicích	Svatoňovice	k1gFnPc6	Svatoňovice
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
venkovského	venkovský	k2eAgMnSc2d1	venkovský
lékaře	lékař	k1gMnSc2	lékař
MUDr.	MUDr.	kA	MUDr.
Antonína	Antonín	k1gMnSc2	Antonín
Čapka	Čapek	k1gMnSc2	Čapek
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
sbírala	sbírat	k5eAaImAgFnS	sbírat
slovesný	slovesný	k2eAgInSc4d1	slovesný
folklor	folklor	k1gInSc4	folklor
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rodiči	rodič	k1gMnPc7	rodič
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
do	do	k7c2	do
Úpice	Úpice	k1gFnSc2	Úpice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
místním	místní	k2eAgInSc6d1	místní
kostele	kostel	k1gInSc6	kostel
13	[number]	k4	13
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1890	[number]	k4	1890
pokřtěn	pokřtěn	k2eAgInSc1d1	pokřtěn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Úpici	Úpice	k1gFnSc6	Úpice
také	také	k9	také
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
základní	základní	k2eAgFnSc4d1	základní
školu	škola	k1gFnSc4	škola
Na	na	k7c6	na
Blahovce	Blahovka	k1gFnSc6	Blahovka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
po	po	k7c6	po
Karlově	Karlův	k2eAgFnSc6d1	Karlova
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
přejmenována	přejmenován	k2eAgFnSc1d1	přejmenována
na	na	k7c4	na
Základní	základní	k2eAgFnSc4d1	základní
školu	škola	k1gFnSc4	škola
bratří	bratr	k1gMnPc2	bratr
Čapků	Čapek	k1gMnPc2	Čapek
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
musel	muset	k5eAaImAgMnS	muset
po	po	k7c6	po
odhalení	odhalení	k1gNnSc6	odhalení
jím	jíst	k5eAaImIp1nS	jíst
organizovaného	organizovaný	k2eAgInSc2d1	organizovaný
protirakouského	protirakouský	k2eAgInSc2d1	protirakouský
spolku	spolek	k1gInSc2	spolek
přestoupit	přestoupit	k5eAaPmF	přestoupit
na	na	k7c4	na
gymnázium	gymnázium	k1gNnSc4	gymnázium
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
ukončil	ukončit	k5eAaPmAgInS	ukončit
studium	studium	k1gNnSc4	studium
na	na	k7c6	na
Filosofické	filosofický	k2eAgFnSc6d1	filosofická
fakultě	fakulta	k1gFnSc6	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
doktorát	doktorát	k1gInSc4	doktorát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1910	[number]	k4	1910
<g/>
–	–	k?	–
<g/>
1911	[number]	k4	1911
byl	být	k5eAaImAgMnS	být
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
na	na	k7c6	na
studijním	studijní	k2eAgInSc6d1	studijní
pobytu	pobyt	k1gInSc6	pobyt
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Počátky	počátek	k1gInPc4	počátek
literární	literární	k2eAgFnSc2d1	literární
činnosti	činnost	k1gFnSc2	činnost
===	===	k?	===
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
trpěl	trpět	k5eAaImAgMnS	trpět
od	od	k7c2	od
svých	svůj	k3xOyFgNnPc2	svůj
21	[number]	k4	21
let	léto	k1gNnPc2	léto
Bechtěrevovou	Bechtěrevový	k2eAgFnSc4d1	Bechtěrevový
nemocí	nemoc	k1gFnSc7	nemoc
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
chronické	chronický	k2eAgNnSc1d1	chronické
zánětlivé	zánětlivý	k2eAgNnSc1d1	zánětlivé
onemocnění	onemocnění	k1gNnSc1	onemocnění
především	především	k6eAd1	především
páteřních	páteřní	k2eAgInPc2d1	páteřní
obratlů	obratel	k1gInPc2	obratel
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
nemoc	nemoc	k1gFnSc4	nemoc
nebyl	být	k5eNaImAgInS	být
odveden	odvést	k5eAaPmNgMnS	odvést
do	do	k7c2	do
rakouské	rakouský	k2eAgFnSc2d1	rakouská
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
nemusel	muset	k5eNaImAgMnS	muset
proto	proto	k8xC	proto
bojovat	bojovat	k5eAaImF	bojovat
v	v	k7c6	v
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
byl	být	k5eAaImAgInS	být
touto	tento	k3xDgFnSc7	tento
válkou	válka	k1gFnSc7	válka
a	a	k8xC	a
jejími	její	k3xOp3gInPc7	její
následky	následek	k1gInPc7	následek
velmi	velmi	k6eAd1	velmi
ovlivněn	ovlivněn	k2eAgMnSc1d1	ovlivněn
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
studia	studio	k1gNnSc2	studio
krátce	krátce	k6eAd1	krátce
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
vychovatel	vychovatel	k1gMnSc1	vychovatel
v	v	k7c6	v
šlechtické	šlechtický	k2eAgFnSc6d1	šlechtická
rodině	rodina	k1gFnSc6	rodina
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
byl	být	k5eAaImAgInS	být
domácím	domácí	k2eAgMnSc7d1	domácí
učitelem	učitel	k1gMnSc7	učitel
Prokopa	Prokop	k1gMnSc2	Prokop
Lažanského	Lažanský	k2eAgMnSc2d1	Lažanský
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Chyše	Chyše	k?	Chyše
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
vychovatel	vychovatel	k1gMnSc1	vychovatel
však	však	k9	však
údajně	údajně	k6eAd1	údajně
působil	působit	k5eAaImAgMnS	působit
pouze	pouze	k6eAd1	pouze
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
<g/>
,	,	kIx,	,
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
přešel	přejít	k5eAaPmAgInS	přejít
k	k	k7c3	k
novinařině	novinařina	k1gFnSc3	novinařina
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
redaktorem	redaktor	k1gMnSc7	redaktor
v	v	k7c6	v
několika	několik	k4yIc6	několik
denících	deník	k1gInPc6	deník
a	a	k8xC	a
časopisech	časopis	k1gInPc6	časopis
<g/>
:	:	kIx,	:
v	v	k7c6	v
Národních	národní	k2eAgInPc6d1	národní
listech	list	k1gInPc6	list
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
–	–	k?	–
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
týdeníku	týdeník	k1gInSc6	týdeník
Nebojsa	nebojsa	k1gMnSc1	nebojsa
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Lidových	lidový	k2eAgFnPc6d1	lidová
novinách	novina	k1gFnPc6	novina
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Národních	národní	k2eAgInPc2d1	národní
listů	list	k1gInPc2	list
odešel	odejít	k5eAaPmAgMnS	odejít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
vyloučení	vyloučení	k1gNnSc3	vyloučení
svého	svůj	k1gMnSc2	svůj
bratra	bratr	k1gMnSc2	bratr
z	z	k7c2	z
redakce	redakce	k1gFnSc2	redakce
a	a	k8xC	a
proti	proti	k7c3	proti
politickému	politický	k2eAgNnSc3d1	politické
směřování	směřování	k1gNnSc3	směřování
listu	list	k1gInSc2	list
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
vnímal	vnímat	k5eAaImAgInS	vnímat
jako	jako	k9	jako
zaměřené	zaměřený	k2eAgNnSc1d1	zaměřené
proti	proti	k7c3	proti
prvnímu	první	k4xOgNnSc3	první
československému	československý	k2eAgMnSc3d1	československý
prezidentovi	prezident	k1gMnSc3	prezident
Tomáši	Tomáš	k1gMnSc3	Tomáš
Garrigue	Garrigue	k1gNnSc2	Garrigue
Masarykovi	Masaryk	k1gMnSc3	Masaryk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vrcholné	vrcholný	k2eAgNnSc4d1	vrcholné
období	období	k1gNnSc4	období
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1921	[number]	k4	1921
<g/>
–	–	k?	–
<g/>
1923	[number]	k4	1923
byl	být	k5eAaImAgInS	být
dramaturgem	dramaturg	k1gMnSc7	dramaturg
i	i	k8xC	i
režisérem	režisér	k1gMnSc7	režisér
Vinohradského	vinohradský	k2eAgNnSc2d1	Vinohradské
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1925	[number]	k4	1925
<g/>
–	–	k?	–
<g/>
1933	[number]	k4	1933
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
předsedou	předseda	k1gMnSc7	předseda
československého	československý	k2eAgInSc2d1	československý
odboru	odbor	k1gInSc2	odbor
PEN	PEN	kA	PEN
klubu	klub	k1gInSc2	klub
<g/>
.	.	kIx.	.
<g/>
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Josef	Josef	k1gMnSc1	Josef
byli	být	k5eAaImAgMnP	být
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
aktéry	aktér	k1gMnPc7	aktér
pravidelného	pravidelný	k2eAgNnSc2d1	pravidelné
pátečního	páteční	k2eAgNnSc2d1	páteční
setkávání	setkávání	k1gNnSc2	setkávání
osobností	osobnost	k1gFnPc2	osobnost
politického	politický	k2eAgInSc2d1	politický
a	a	k8xC	a
kulturního	kulturní	k2eAgInSc2d1	kulturní
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
schůzky	schůzka	k1gFnPc1	schůzka
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
zavedenou	zavedený	k2eAgFnSc7d1	zavedená
"	"	kIx"	"
<g/>
institucí	instituce	k1gFnPc2	instituce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gMnPc4	jejich
účastníky	účastník	k1gMnPc4	účastník
všeobecně	všeobecně	k6eAd1	všeobecně
vžil	vžít	k5eAaPmAgInS	vžít
název	název	k1gInSc1	název
pátečníci	pátečník	k1gMnPc1	pátečník
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
pátečníky	pátečník	k1gMnPc7	pátečník
patřili	patřit	k5eAaImAgMnP	patřit
kromě	kromě	k7c2	kromě
bratří	bratr	k1gMnPc2	bratr
Čapků	Čapek	k1gMnPc2	Čapek
mj.	mj.	kA	mj.
prezident	prezident	k1gMnSc1	prezident
T.	T.	kA	T.
G.	G.	kA	G.
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
Josef	Josef	k1gMnSc1	Josef
Šusta	Šusta	k1gMnSc1	Šusta
a	a	k8xC	a
novinář	novinář	k1gMnSc1	novinář
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Peroutka	Peroutka	k1gMnSc1	Peroutka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čapkovy	Čapkův	k2eAgInPc1d1	Čapkův
Hovory	hovor	k1gInPc1	hovor
s	s	k7c7	s
T.	T.	kA	T.
G.	G.	kA	G.
Masarykem	Masaryk	k1gMnSc7	Masaryk
(	(	kIx(	(
<g/>
sepsány	sepsat	k5eAaPmNgFnP	sepsat
1928	[number]	k4	1928
<g/>
–	–	k?	–
<g/>
1935	[number]	k4	1935
<g/>
,	,	kIx,	,
ve	v	k7c6	v
3	[number]	k4	3
svazcích	svazek	k1gInPc6	svazek
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
shrnuty	shrnut	k2eAgInPc4d1	shrnut
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
svazku	svazek	k1gInSc2	svazek
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
jedinečným	jedinečný	k2eAgNnSc7d1	jedinečné
svědectvím	svědectví	k1gNnSc7	svědectví
o	o	k7c4	o
T.	T.	kA	T.
G.	G.	kA	G.
Masarykovi	Masaryk	k1gMnSc3	Masaryk
<g/>
,	,	kIx,	,
o	o	k7c6	o
jeho	jeho	k3xOp3gInPc6	jeho
názorech	názor	k1gInPc6	názor
nejen	nejen	k6eAd1	nejen
politických	politický	k2eAgInPc2d1	politický
<g/>
,	,	kIx,	,
filozofických	filozofický	k2eAgInPc2d1	filozofický
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
lidských	lidský	k2eAgNnPc2d1	lidské
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc2	jeho
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1935	[number]	k4	1935
se	se	k3xPyFc4	se
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
na	na	k7c6	na
vinohradské	vinohradský	k2eAgFnSc6d1	Vinohradská
radnici	radnice	k1gFnSc6	radnice
oženil	oženit	k5eAaPmAgMnS	oženit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
dlouholetou	dlouholetý	k2eAgFnSc7d1	dlouholetá
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
<g/>
,	,	kIx,	,
herečkou	herečka	k1gFnSc7	herečka
Olgou	Olga	k1gFnSc7	Olga
Scheinpflugovou	Scheinpflugův	k2eAgFnSc7d1	Scheinpflugův
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zhroucení	zhroucení	k1gNnSc1	zhroucení
Čapkova	Čapkův	k2eAgInSc2d1	Čapkův
světa	svět	k1gInSc2	svět
===	===	k?	===
</s>
</p>
<p>
<s>
Mnichovská	mnichovský	k2eAgFnSc1d1	Mnichovská
dohoda	dohoda	k1gFnSc1	dohoda
a	a	k8xC	a
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
následující	následující	k2eAgFnSc6d1	následující
kapitulace	kapitulace	k1gFnSc2	kapitulace
znamenaly	znamenat	k5eAaImAgFnP	znamenat
pro	pro	k7c4	pro
Karla	Karel	k1gMnSc4	Karel
Čapka	Čapek	k1gMnSc2	Čapek
zhroucení	zhroucení	k1gNnSc2	zhroucení
jeho	jeho	k3xOp3gInSc2	jeho
dosavadního	dosavadní	k2eAgInSc2d1	dosavadní
světa	svět	k1gInSc2	svět
a	a	k8xC	a
osobní	osobní	k2eAgFnSc4d1	osobní
tragédii	tragédie	k1gFnSc4	tragédie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
vzpamatoval	vzpamatovat	k5eAaPmAgMnS	vzpamatovat
z	z	k7c2	z
prvotního	prvotní	k2eAgInSc2d1	prvotní
šoku	šok	k1gInSc2	šok
<g/>
,	,	kIx,	,
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
o	o	k7c4	o
ospravedlnění	ospravedlnění	k1gNnPc4	ospravedlnění
vládních	vládní	k2eAgInPc2d1	vládní
a	a	k8xC	a
prezidentových	prezidentův	k2eAgInPc2d1	prezidentův
kroků	krok	k1gInPc2	krok
v	v	k7c6	v
situaci	situace	k1gFnSc6	situace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dle	dle	k7c2	dle
Čapka	Čapek	k1gMnSc2	Čapek
nenabízela	nabízet	k5eNaImAgFnS	nabízet
jiná	jiný	k2eAgNnPc4d1	jiné
ospravedlnitelná	ospravedlnitelný	k2eAgNnPc4d1	ospravedlnitelné
řešení	řešení	k1gNnPc4	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
nemístné	místný	k2eNgNnSc1d1	nemístné
viděl	vidět	k5eAaImAgMnS	vidět
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
situaci	situace	k1gFnSc6	situace
hledání	hledání	k1gNnSc1	hledání
viníků	viník	k1gMnPc2	viník
<g/>
.	.	kIx.	.
</s>
<s>
Snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
zabránit	zabránit	k5eAaPmF	zabránit
rozdělení	rozdělení	k1gNnSc3	rozdělení
národa	národ	k1gInSc2	národ
a	a	k8xC	a
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c4	o
jeho	jeho	k3xOp3gFnSc4	jeho
jednotu	jednota	k1gFnSc4	jednota
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
abdikaci	abdikace	k1gFnSc6	abdikace
prezidenta	prezident	k1gMnSc2	prezident
Beneše	Beneš	k1gMnSc2	Beneš
se	se	k3xPyFc4	se
však	však	k9	však
stal	stát	k5eAaPmAgInS	stát
jediným	jediný	k2eAgInSc7d1	jediný
viditelným	viditelný	k2eAgInSc7d1	viditelný
symbolem	symbol	k1gInSc7	symbol
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
často	často	k6eAd1	často
plnil	plnit	k5eAaImAgInS	plnit
roli	role	k1gFnSc4	role
"	"	kIx"	"
<g/>
obětního	obětní	k2eAgMnSc2d1	obětní
beránka	beránek	k1gMnSc2	beránek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
této	tento	k3xDgFnSc2	tento
kampaně	kampaň	k1gFnSc2	kampaň
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stávaly	stávat	k5eAaImAgFnP	stávat
nejen	nejen	k6eAd1	nejen
četné	četný	k2eAgInPc1d1	četný
urážlivé	urážlivý	k2eAgInPc1d1	urážlivý
anonymní	anonymní	k2eAgInPc1d1	anonymní
dopisy	dopis	k1gInPc1	dopis
a	a	k8xC	a
telefonáty	telefonát	k1gInPc1	telefonát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
vytloukání	vytloukání	k1gNnSc2	vytloukání
oken	okno	k1gNnPc2	okno
Čapkova	Čapkův	k2eAgInSc2d1	Čapkův
domu	dům	k1gInSc2	dům
apod.	apod.	kA	apod.
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
útoky	útok	k1gInPc4	útok
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
osobu	osoba	k1gFnSc4	osoba
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
úvahu	úvaha	k1gFnSc4	úvaha
Jak	jak	k8xS	jak
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
otištěnou	otištěný	k2eAgFnSc4d1	otištěná
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1938	[number]	k4	1938
v	v	k7c6	v
Lidových	lidový	k2eAgFnPc6d1	lidová
novinách	novina	k1gFnPc6	novina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
své	svůj	k3xOyFgFnPc4	svůj
aktivity	aktivita	k1gFnPc4	aktivita
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bydliště	bydliště	k1gNnSc2	bydliště
==	==	k?	==
</s>
</p>
<p>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
žil	žít	k5eAaImAgMnS	žít
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
Josefem	Josef	k1gMnSc7	Josef
Čapkem	Čapek	k1gMnSc7	Čapek
v	v	k7c6	v
bytě	byt	k1gInSc6	byt
na	na	k7c6	na
Malé	Malé	k2eAgFnSc6d1	Malé
Straně	strana	k1gFnSc6	strana
v	v	k7c6	v
Říční	říční	k2eAgFnSc6d1	říční
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
přestěhovali	přestěhovat	k5eAaPmAgMnP	přestěhovat
na	na	k7c4	na
východní	východní	k2eAgFnSc4d1	východní
hranu	hrana	k1gFnSc4	hrana
vinohradské	vinohradský	k2eAgFnSc2d1	Vinohradská
kolonie	kolonie	k1gFnSc2	kolonie
Spolku	spolek	k1gInSc2	spolek
žurnalistů	žurnalist	k1gMnPc2	žurnalist
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
postavil	postavit	k5eAaPmAgMnS	postavit
architekt	architekt	k1gMnSc1	architekt
Ladislav	Ladislav	k1gMnSc1	Ladislav
Machoň	Machoň	k1gMnSc1	Machoň
v	v	k7c6	v
letech	let	k1gInPc6	let
1923	[number]	k4	1923
<g/>
–	–	k?	–
<g/>
1924	[number]	k4	1924
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
dvojvilu	dvojvila	k1gFnSc4	dvojvila
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
národním	národní	k2eAgInSc6d1	národní
slohu	sloh	k1gInSc6	sloh
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
žil	žít	k5eAaImAgMnS	žít
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
polovině	polovina	k1gFnSc6	polovina
dvojdomu	dvojdom	k1gInSc2	dvojdom
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
Praze	Praha	k1gFnSc6	Praha
10	[number]	k4	10
sám	sám	k3xTgInSc1	sám
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
Olgou	Olga	k1gFnSc7	Olga
Scheinpflugovou	Scheinpflugův	k2eAgFnSc7d1	Scheinpflugův
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc4	druhý
polovinu	polovina	k1gFnSc4	polovina
domu	dům	k1gInSc2	dům
obýval	obývat	k5eAaImAgInS	obývat
bratr	bratr	k1gMnSc1	bratr
Josef	Josef	k1gMnSc1	Josef
Čapek	Čapek	k1gMnSc1	Čapek
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
rodinou	rodina	k1gFnSc7	rodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ke	k	k7c3	k
svatbě	svatba	k1gFnSc3	svatba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
obdrželi	obdržet	k5eAaPmAgMnP	obdržet
novomanželé	novomanžel	k1gMnPc1	novomanžel
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
a	a	k8xC	a
Olga	Olga	k1gFnSc1	Olga
Scheinpflugová	Scheinpflugový	k2eAgFnSc1d1	Scheinpflugová
doživotní	doživotní	k2eAgNnSc4d1	doživotní
právo	právo	k1gNnSc4	právo
bydlet	bydlet	k5eAaImF	bydlet
na	na	k7c6	na
letním	letní	k2eAgNnSc6d1	letní
sídle	sídlo	k1gNnSc6	sídlo
nad	nad	k7c7	nad
rybníkem	rybník	k1gInSc7	rybník
Strž	strž	k1gFnSc1	strž
u	u	k7c2	u
Staré	Staré	k2eAgFnSc2d1	Staré
Huti	huť	k1gFnSc2	huť
(	(	kIx(	(
<g/>
nedaleko	nedaleko	k7c2	nedaleko
Dobříše	Dobříš	k1gFnSc2	Dobříš
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
zásluhou	zásluhou	k7c2	zásluhou
generálního	generální	k2eAgMnSc2d1	generální
ředitele	ředitel	k1gMnSc2	ředitel
hutí	huť	k1gFnPc2	huť
v	v	k7c6	v
Dobříši	Dobříš	k1gFnSc6	Dobříš
Václava	Václav	k1gMnSc2	Václav
Palivce	Palivec	k1gMnSc2	Palivec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
domě	dům	k1gInSc6	dům
památník	památník	k1gInSc1	památník
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapek	k1gMnSc2	Čapek
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
Čapek	čapka	k1gFnPc2	čapka
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
posledních	poslední	k2eAgInPc6d1	poslední
třech	tři	k4xCgInPc6	tři
rocích	rok	k1gInPc6	rok
života	život	k1gInSc2	život
převážně	převážně	k6eAd1	převážně
pobýval	pobývat	k5eAaImAgMnS	pobývat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úmrtí	úmrtí	k1gNnPc1	úmrtí
==	==	k?	==
</s>
</p>
<p>
<s>
Koncem	koncem	k7c2	koncem
srpna	srpen	k1gInSc2	srpen
1938	[number]	k4	1938
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
dům	dům	k1gInSc4	dům
a	a	k8xC	a
zahradu	zahrada	k1gFnSc4	zahrada
ve	v	k7c6	v
Staré	Staré	k2eAgFnSc6d1	Staré
Huti	huť	k1gFnSc6	huť
povodeň	povodeň	k1gFnSc4	povodeň
<g/>
.	.	kIx.	.
</s>
<s>
Čapek	Čapek	k1gMnSc1	Čapek
se	se	k3xPyFc4	se
ihned	ihned	k6eAd1	ihned
pustil	pustit	k5eAaPmAgMnS	pustit
do	do	k7c2	do
náročných	náročný	k2eAgFnPc2d1	náročná
oprav	oprava	k1gFnPc2	oprava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1938	[number]	k4	1938
si	se	k3xPyFc3	se
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
přivodil	přivodit	k5eAaBmAgMnS	přivodit
lehkou	lehký	k2eAgFnSc4d1	lehká
chřipku	chřipka	k1gFnSc4	chřipka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
stav	stav	k1gInSc1	stav
přechodně	přechodně	k6eAd1	přechodně
zlepšil	zlepšit	k5eAaPmAgInS	zlepšit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
brzy	brzy	k6eAd1	brzy
musel	muset	k5eAaImAgInS	muset
znovu	znovu	k6eAd1	znovu
ulehnout	ulehnout	k5eAaPmF	ulehnout
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
chřipce	chřipka	k1gFnSc3	chřipka
se	se	k3xPyFc4	se
přidal	přidat	k5eAaPmAgMnS	přidat
zánět	zánět	k1gInSc4	zánět
ledvin	ledvina	k1gFnPc2	ledvina
a	a	k8xC	a
zápal	zápal	k1gInSc1	zápal
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
plicní	plicní	k2eAgInSc4d1	plicní
edém	edém	k1gInSc4	edém
dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
nĕ	nĕ	k?	nĕ
měsíců	měsíc	k1gInPc2	měsíc
před	před	k7c7	před
okupací	okupace	k1gFnSc7	okupace
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
nacistickým	nacistický	k2eAgNnSc7d1	nacistické
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
gestapo	gestapo	k1gNnSc1	gestapo
plánovalo	plánovat	k5eAaImAgNnS	plánovat
jeho	jeho	k3xOp3gFnSc4	jeho
zatčení	zatčení	k1gNnSc1	zatčení
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
na	na	k7c6	na
Vyšehradském	vyšehradský	k2eAgInSc6d1	vyšehradský
hřbitově	hřbitov	k1gInSc6	hřbitov
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
in	in	k?	in
memoriam	memoriam	k6eAd1	memoriam
propůjčen	propůjčen	k2eAgInSc1d1	propůjčen
Řád	řád	k1gInSc1	řád
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
.	.	kIx.	.
<g/>
Čapek	Čapek	k1gMnSc1	Čapek
byl	být	k5eAaImAgMnS	být
mimořádně	mimořádně	k6eAd1	mimořádně
dobrým	dobrý	k2eAgMnSc7d1	dobrý
amatérským	amatérský	k2eAgMnSc7d1	amatérský
fotografem	fotograf	k1gMnSc7	fotograf
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yQnSc6	což
vedle	vedle	k7c2	vedle
známých	známý	k2eAgFnPc2d1	známá
fotografií	fotografia	k1gFnPc2	fotografia
např.	např.	kA	např.
v	v	k7c6	v
Dášeňce	Dášeňka	k1gFnSc6	Dášeňka
svědčí	svědčit	k5eAaImIp3nS	svědčit
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgInPc2d1	další
dochovaných	dochovaný	k2eAgInPc2d1	dochovaný
snímků	snímek	k1gInPc2	snímek
včetně	včetně	k7c2	včetně
portrétů	portrét	k1gInPc2	portrét
známých	známý	k2eAgFnPc2d1	známá
osobností	osobnost	k1gFnPc2	osobnost
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
prezident	prezident	k1gMnSc1	prezident
Masaryk	Masaryk	k1gMnSc1	Masaryk
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
pátečníci	pátečník	k1gMnPc1	pátečník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Amatér	amatér	k1gMnSc1	amatér
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
byl	být	k5eAaImAgMnS	být
autorem	autor	k1gMnSc7	autor
nejprodávanější	prodávaný	k2eAgFnSc2d3	nejprodávanější
fotografické	fotografický	k2eAgFnSc2d1	fotografická
publikace	publikace	k1gFnSc2	publikace
období	období	k1gNnSc2	období
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
<g/>
:	:	kIx,	:
Dášeňka	Dášeňka	k1gFnSc1	Dášeňka
čili	čili	k8xC	čili
Život	život	k1gInSc1	život
štěněte	štěně	k1gNnSc2	štěně
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
byla	být	k5eAaImAgFnS	být
publikována	publikovat	k5eAaBmNgFnS	publikovat
v	v	k7c6	v
několika	několik	k4yIc6	několik
desítkách	desítka	k1gFnPc6	desítka
vydání	vydání	k1gNnSc2	vydání
<g/>
.	.	kIx.	.
<g/>
Méně	málo	k6eAd2	málo
známá	známý	k2eAgFnSc1d1	známá
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
záliba	záliba	k1gFnSc1	záliba
v	v	k7c6	v
etnické	etnický	k2eAgFnSc6d1	etnická
hudbě	hudba	k1gFnSc6	hudba
<g/>
,	,	kIx,	,
vyrostlá	vyrostlý	k2eAgFnSc1d1	vyrostlá
ze	z	k7c2	z
zájmu	zájem	k1gInSc2	zájem
o	o	k7c4	o
cizí	cizí	k2eAgFnPc4d1	cizí
kultury	kultura	k1gFnSc2	kultura
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Patřil	patřit	k5eAaImAgMnS	patřit
mezi	mezi	k7c4	mezi
přední	přední	k2eAgMnPc4d1	přední
sběratele	sběratel	k1gMnPc4	sběratel
<g/>
;	;	kIx,	;
celou	celý	k2eAgFnSc4d1	celá
dochovanou	dochovaný	k2eAgFnSc4d1	dochovaná
sbírku	sbírka	k1gFnSc4	sbírka
jeho	jeho	k3xOp3gMnPc1	jeho
dědicové	dědic	k1gMnPc1	dědic
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
věnovali	věnovat	k5eAaImAgMnP	věnovat
Náprstkovu	Náprstkův	k2eAgNnSc3d1	Náprstkovo
muzeu	muzeum	k1gNnSc3	muzeum
(	(	kIx(	(
<g/>
celkem	celkem	k6eAd1	celkem
462	[number]	k4	462
desek	deska	k1gFnPc2	deska
78	[number]	k4	78
ot	ot	k1gMnSc1	ot
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
<g/>
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
a	a	k8xC	a
115	[number]	k4	115
katalogů	katalog	k1gInPc2	katalog
světových	světový	k2eAgFnPc2d1	světová
gramofonových	gramofonový	k2eAgFnPc2d1	gramofonová
firem	firma	k1gFnPc2	firma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
byly	být	k5eAaImAgFnP	být
nahrávky	nahrávka	k1gFnPc1	nahrávka
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
UNESCO	UNESCO	kA	UNESCO
digitalizovány	digitalizován	k2eAgInPc1d1	digitalizován
a	a	k8xC	a
výběr	výběr	k1gInSc1	výběr
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
na	na	k7c6	na
pěti	pět	k4xCc6	pět
CD	CD	kA	CD
<g/>
.	.	kIx.	.
<g/>
Autorem	autor	k1gMnSc7	autor
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
robot	robot	k1gInSc1	robot
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
s	s	k7c7	s
divadelní	divadelní	k2eAgFnSc7d1	divadelní
hrou	hra	k1gFnSc7	hra
R.	R.	kA	R.
<g/>
U.	U.	kA	U.
<g/>
R.	R.	kA	R.
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
po	po	k7c6	po
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
bratr	bratr	k1gMnSc1	bratr
Josef	Josef	k1gMnSc1	Josef
Čapek	Čapek	k1gMnSc1	Čapek
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
chtěl	chtít	k5eAaImAgMnS	chtít
Karel	Karel	k1gMnSc1	Karel
Čapek	čapka	k1gFnPc2	čapka
roboty	robota	k1gFnSc2	robota
nazvat	nazvat	k5eAaBmF	nazvat
"	"	kIx"	"
<g/>
laboři	laboř	k1gFnSc3	laboř
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
robot	robota	k1gFnPc2	robota
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
ze	z	k7c2	z
slovesa	sloveso	k1gNnSc2	sloveso
robotovat	robotovat	k5eAaImF	robotovat
(	(	kIx(	(
<g/>
pracovat	pracovat	k5eAaImF	pracovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
do	do	k7c2	do
kin	kino	k1gNnPc2	kino
životopisný	životopisný	k2eAgInSc1d1	životopisný
film	film	k1gInSc1	film
Člověk	člověk	k1gMnSc1	člověk
proti	proti	k7c3	proti
zkáze	zkáza	k1gFnSc3	zkáza
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
režíroval	režírovat	k5eAaImAgMnS	režírovat
Štěpán	Štěpán	k1gMnSc1	Štěpán
Skalský	Skalský	k1gMnSc1	Skalský
a	a	k8xC	a
Jaromír	Jaromír	k1gMnSc1	Jaromír
Pleskot	Pleskot	k1gMnSc1	Pleskot
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc4d1	hlavní
postavu	postava	k1gFnSc4	postava
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapek	k1gMnSc2	Čapek
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
Josef	Josef	k1gMnSc1	Josef
Abrhám	Abrha	k1gFnPc3	Abrha
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
bratra	bratr	k1gMnSc4	bratr
Josefa	Josef	k1gMnSc4	Josef
hrál	hrát	k5eAaImAgMnS	hrát
František	František	k1gMnSc1	František
Řehák	Řehák	k1gMnSc1	Řehák
<g/>
,	,	kIx,	,
Olgu	Olga	k1gFnSc4	Olga
Scheinpflugovou	Scheinpflugový	k2eAgFnSc4d1	Scheinpflugová
Hana	Hana	k1gFnSc1	Hana
Maciuchová	Maciuchová	k1gFnSc1	Maciuchová
<g/>
,	,	kIx,	,
postavu	postava	k1gFnSc4	postava
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
.	.	kIx.	.
<g/>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
až	až	k9	až
70	[number]	k4	70
roků	rok	k1gInPc2	rok
po	po	k7c6	po
Čapkově	Čapkův	k2eAgFnSc6d1	Čapkova
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
knižně	knižně	k6eAd1	knižně
vydána	vydán	k2eAgFnSc1d1	vydána
obsáhlá	obsáhlý	k2eAgFnSc1d1	obsáhlá
korespondence	korespondence	k1gFnSc1	korespondence
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
se	s	k7c7	s
spisovatelem	spisovatel	k1gMnSc7	spisovatel
vedl	vést	k5eAaImAgMnS	vést
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
pacifismu	pacifismus	k1gInSc2	pacifismus
a	a	k8xC	a
odpírání	odpírání	k1gNnSc2	odpírání
vojenské	vojenský	k2eAgFnSc2d1	vojenská
služby	služba	k1gFnSc2	služba
brněnský	brněnský	k2eAgMnSc1d1	brněnský
advokát	advokát	k1gMnSc1	advokát
Jindřich	Jindřich	k1gMnSc1	Jindřich
Groag	Groag	k1gMnSc1	Groag
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byla	být	k5eAaImAgFnS	být
známa	znám	k2eAgFnSc1d1	známa
jen	jen	k9	jen
část	část	k1gFnSc1	část
těchto	tento	k3xDgInPc2	tento
dopisů	dopis	k1gInPc2	dopis
<g/>
.	.	kIx.	.
<g/>
Byl	být	k5eAaImAgMnS	být
sedmkrát	sedmkrát	k6eAd1	sedmkrát
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1932	[number]	k4	1932
až	až	k9	až
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Svoji	svůj	k3xOyFgFnSc4	svůj
literární	literární	k2eAgFnSc4d1	literární
tvorbu	tvorba	k1gFnSc4	tvorba
zahájil	zahájit	k5eAaPmAgInS	zahájit
před	před	k7c7	před
první	první	k4xOgFnSc7	první
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
zpočátku	zpočátku	k6eAd1	zpočátku
tvořil	tvořit	k5eAaImAgMnS	tvořit
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
Josefem	Josef	k1gMnSc7	Josef
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
především	především	k9	především
malířem	malíř	k1gMnSc7	malíř
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
tvorbu	tvorba	k1gFnSc4	tvorba
mělo	mít	k5eAaImAgNnS	mít
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
jeho	jeho	k3xOp3gNnSc1	jeho
filosofické	filosofický	k2eAgNnSc1d1	filosofické
a	a	k8xC	a
estetické	estetický	k2eAgNnSc1d1	estetické
vzdělání	vzdělání	k1gNnSc1	vzdělání
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
pragmatismus	pragmatismus	k1gInSc1	pragmatismus
a	a	k8xC	a
expresionismus	expresionismus	k1gInSc1	expresionismus
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
ho	on	k3xPp3gInSc4	on
velmi	velmi	k6eAd1	velmi
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
vědeckotechnická	vědeckotechnický	k2eAgFnSc1d1	vědeckotechnická
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
svých	svůj	k3xOyFgInPc6	svůj
dílech	díl	k1gInPc6	díl
vyjadřoval	vyjadřovat	k5eAaImAgMnS	vyjadřovat
obavu	obava	k1gFnSc4	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
jednou	jednou	k6eAd1	jednou
technika	technika	k1gFnSc1	technika
získá	získat	k5eAaPmIp3nS	získat
moc	moc	k6eAd1	moc
nad	nad	k7c7	nad
člověkem	člověk	k1gMnSc7	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Typickým	typický	k2eAgInSc7d1	typický
znakem	znak	k1gInSc7	znak
jeho	on	k3xPp3gMnSc2	on
děl	dělo	k1gNnPc2	dělo
je	být	k5eAaImIp3nS	být
využívání	využívání	k1gNnSc1	využívání
obrovské	obrovský	k2eAgFnSc2d1	obrovská
slovní	slovní	k2eAgFnSc2d1	slovní
zásoby	zásoba	k1gFnSc2	zásoba
<g/>
,	,	kIx,	,
používání	používání	k1gNnSc1	používání
neobvyklých	obvyklý	k2eNgNnPc2d1	neobvyklé
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
několikanásobných	několikanásobný	k2eAgInPc2d1	několikanásobný
větných	větný	k2eAgInPc2d1	větný
členů	člen	k1gInPc2	člen
a	a	k8xC	a
rozvitých	rozvitý	k2eAgNnPc2d1	rozvité
souvětí	souvětí	k1gNnPc2	souvětí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dílech	díl	k1gInPc6	díl
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapek	k1gMnSc2	Čapek
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
individualistický	individualistický	k2eAgInSc1d1	individualistický
přístup	přístup	k1gInSc1	přístup
<g/>
,	,	kIx,	,
a	a	k8xC	a
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vyplývající	vyplývající	k2eAgNnSc1d1	vyplývající
uznávání	uznávání	k1gNnSc1	uznávání
silných	silný	k2eAgMnPc2d1	silný
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
neuznává	uznávat	k5eNaImIp3nS	uznávat
jednoznačnou	jednoznačný	k2eAgFnSc4d1	jednoznačná
pravdu	pravda	k1gFnSc4	pravda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
pravdu	pravda	k1gFnSc4	pravda
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
jednotlivce	jednotlivec	k1gMnSc2	jednotlivec
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
samozřejmě	samozřejmě	k6eAd1	samozřejmě
také	také	k9	také
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
první	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
nástup	nástup	k1gInSc1	nástup
fašismu	fašismus	k1gInSc2	fašismus
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
nacismu	nacismus	k1gInSc2	nacismus
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
blížící	blížící	k2eAgFnSc4d1	blížící
se	se	k3xPyFc4	se
druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
a	a	k8xC	a
hrozby	hrozba	k1gFnSc2	hrozba
proti	proti	k7c3	proti
první	první	k4xOgFnSc3	první
Československé	československý	k2eAgFnSc3d1	Československá
republice	republika	k1gFnSc3	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
bývá	bývat	k5eAaImIp3nS	bývat
děleno	dělen	k2eAgNnSc1d1	děleno
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
základní	základní	k2eAgFnPc4d1	základní
části	část	k1gFnPc4	část
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Část	část	k1gFnSc4	část
zabývající	zabývající	k2eAgFnSc4d1	zabývající
se	s	k7c7	s
vnitřním	vnitřní	k2eAgInSc7d1	vnitřní
životem	život	k1gInSc7	život
člověka	člověk	k1gMnSc2	člověk
jako	jako	k8xS	jako
jedince	jedinec	k1gMnSc2	jedinec
a	a	k8xC	a
jehož	jehož	k3xOyRp3gNnSc7	jehož
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
se	se	k3xPyFc4	se
Čapek	Čapek	k1gMnSc1	Čapek
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
zkoumat	zkoumat	k5eAaImF	zkoumat
možnosti	možnost	k1gFnPc4	možnost
i	i	k8xC	i
hranice	hranice	k1gFnPc4	hranice
lidského	lidský	k2eAgNnSc2d1	lidské
poznání	poznání	k1gNnSc2	poznání
<g/>
,	,	kIx,	,
mnohost	mnohost	k1gFnSc1	mnohost
pohledů	pohled	k1gInPc2	pohled
na	na	k7c4	na
realitu	realita	k1gFnSc4	realita
<g/>
,	,	kIx,	,
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
noetikou	noetika	k1gFnSc7	noetika
(	(	kIx(	(
<g/>
Boží	boží	k2eAgFnSc1d1	boží
muka	muka	k1gFnSc1	muka
<g/>
,	,	kIx,	,
noetická	noetický	k2eAgFnSc1d1	noetická
trilogie	trilogie	k1gFnSc1	trilogie
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
utopická	utopický	k2eAgFnSc1d1	utopická
část	část	k1gFnSc1	část
–	–	k?	–
sem	sem	k6eAd1	sem
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nP	řadit
jeho	jeho	k3xOp3gInPc1	jeho
utopické	utopický	k2eAgInPc1d1	utopický
romány	román	k1gInPc1	román
a	a	k8xC	a
dramata	drama	k1gNnPc1	drama
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgNnPc6	který
Čapek	Čapek	k1gMnSc1	Čapek
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
společenské	společenský	k2eAgInPc4d1	společenský
problémy	problém	k1gInPc4	problém
celé	celý	k2eAgFnSc2d1	celá
moderní	moderní	k2eAgFnSc2d1	moderní
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
často	často	k6eAd1	často
vyjadřována	vyjadřován	k2eAgFnSc1d1	vyjadřována
obava	obava	k1gFnSc1	obava
ze	z	k7c2	z
zneužití	zneužití	k1gNnSc2	zneužití
techniky	technika	k1gFnSc2	technika
proti	proti	k7c3	proti
člověku	člověk	k1gMnSc3	člověk
a	a	k8xC	a
v	v	k7c6	v
dílech	dílo	k1gNnPc6	dílo
je	být	k5eAaImIp3nS	být
i	i	k9	i
patrná	patrný	k2eAgFnSc1d1	patrná
obava	obava	k1gFnSc1	obava
z	z	k7c2	z
nastupujícího	nastupující	k2eAgInSc2d1	nastupující
fašismu	fašismus	k1gInSc2	fašismus
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tato	tento	k3xDgNnPc4	tento
díla	dílo	k1gNnPc4	dílo
bývají	bývat	k5eAaImIp3nP	bývat
oba	dva	k4xCgInPc1	dva
<g/>
[	[	kIx(	[
<g/>
ujasnit	ujasnit	k5eAaPmF	ujasnit
<g/>
]	]	kIx)	]
bratři	bratr	k1gMnPc1	bratr
Čapkové	Čapková	k1gFnSc2	Čapková
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
předchůdce	předchůdce	k1gMnPc4	předchůdce
sci-fi	scii	k1gNnSc7	sci-fi
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Kritičnost	kritičnost	k1gFnSc1	kritičnost
jeho	jeho	k3xOp3gNnPc2	jeho
děl	dělo	k1gNnPc2	dělo
často	často	k6eAd1	často
oslabuje	oslabovat	k5eAaImIp3nS	oslabovat
idylizující	idylizující	k2eAgInSc4d1	idylizující
nebo	nebo	k8xC	nebo
harmonizující	harmonizující	k2eAgInSc4d1	harmonizující
závěr	závěr	k1gInSc4	závěr
<g/>
,	,	kIx,	,
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
vrcholných	vrcholný	k2eAgInPc6d1	vrcholný
dílech	díl	k1gInPc6	díl
<g/>
[	[	kIx(	[
<g/>
ujasnit	ujasnit	k5eAaPmF	ujasnit
<g/>
]	]	kIx)	]
tuto	tento	k3xDgFnSc4	tento
tendenci	tendence	k1gFnSc4	tendence
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Činnost	činnost	k1gFnSc1	činnost
novináře	novinář	k1gMnSc2	novinář
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
Čapkovi	Čapek	k1gMnSc3	Čapek
řadu	řada	k1gFnSc4	řada
tvůrčích	tvůrčí	k2eAgInPc2d1	tvůrčí
podnětů	podnět	k1gInPc2	podnět
a	a	k8xC	a
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
organizaci	organizace	k1gFnSc4	organizace
jeho	jeho	k3xOp3gNnPc2	jeho
literárních	literární	k2eAgNnPc2d1	literární
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc4	jejich
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
sloh	sloh	k1gInSc4	sloh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
výraz	výraz	k1gInSc4	výraz
a	a	k8xC	a
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
tvorba	tvorba	k1gFnSc1	tvorba
výrazně	výrazně	k6eAd1	výrazně
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
podobu	podoba	k1gFnSc4	podoba
tradičních	tradiční	k2eAgFnPc2d1	tradiční
i	i	k8xC	i
nově	nově	k6eAd1	nově
vzniklých	vzniklý	k2eAgInPc2d1	vzniklý
novinářských	novinářský	k2eAgInPc2d1	novinářský
útvarů	útvar	k1gInPc2	útvar
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
jazykovou	jazykový	k2eAgFnSc7d1	jazyková
svěžestí	svěžest	k1gFnSc7	svěžest
a	a	k8xC	a
slovesnou	slovesný	k2eAgFnSc7d1	slovesná
propracovaností	propracovanost	k1gFnSc7	propracovanost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
působení	působení	k1gNnSc1	působení
v	v	k7c6	v
tisku	tisk	k1gInSc6	tisk
je	být	k5eAaImIp3nS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
především	především	k9	především
s	s	k7c7	s
Lidovými	lidový	k2eAgFnPc7d1	lidová
novinami	novina	k1gFnPc7	novina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
jistém	jistý	k2eAgInSc6d1	jistý
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
navazoval	navazovat	k5eAaImAgInS	navazovat
na	na	k7c4	na
Jana	Jan	k1gMnSc4	Jan
Nerudu	Neruda	k1gMnSc4	Neruda
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaBmAgMnS	napsat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
fejetonů	fejeton	k1gInPc2	fejeton
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
později	pozdě	k6eAd2	pozdě
vycházely	vycházet	k5eAaImAgFnP	vycházet
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
souborech	soubor	k1gInPc6	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
vyjadřoval	vyjadřovat	k5eAaImAgInS	vyjadřovat
k	k	k7c3	k
aktuálním	aktuální	k2eAgInPc3d1	aktuální
problémům	problém	k1gInPc3	problém
ve	v	k7c6	v
sloupcích	sloupec	k1gInPc6	sloupec
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
v	v	k7c6	v
Lidových	lidový	k2eAgFnPc6d1	lidová
novinách	novina	k1gFnPc6	novina
zaváděl	zavádět	k5eAaImAgInS	zavádět
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
Čapkem	Čapek	k1gMnSc7	Čapek
Karel	Karel	k1gMnSc1	Karel
Poláček	Poláček	k1gMnSc1	Poláček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Próza	próza	k1gFnSc1	próza
===	===	k?	===
</s>
</p>
<p>
<s>
Zářivé	zářivý	k2eAgFnPc1d1	zářivá
hlubiny	hlubina	k1gFnPc1	hlubina
(	(	kIx(	(
<g/>
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
–	–	k?	–
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
knižní	knižní	k2eAgNnSc4d1	knižní
vydání	vydání	k1gNnSc4	vydání
jeho	jeho	k3xOp3gFnPc2	jeho
povídek	povídka	k1gFnPc2	povídka
otiskovaných	otiskovaný	k2eAgFnPc2d1	otiskovaná
v	v	k7c6	v
časopisech	časopis	k1gInPc6	časopis
<g/>
,	,	kIx,	,
napsaných	napsaný	k2eAgMnPc2d1	napsaný
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
Josefem	Josef	k1gMnSc7	Josef
</s>
</p>
<p>
<s>
Boží	boží	k2eAgFnSc1d1	boží
muka	muka	k1gFnSc1	muka
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
–	–	k?	–
filosofické	filosofický	k2eAgFnPc4d1	filosofická
povídky	povídka	k1gFnPc4	povídka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
zamýšlejí	zamýšlet	k5eAaImIp3nP	zamýšlet
(	(	kIx(	(
<g/>
filosofují	filosofovat	k5eAaImIp3nP	filosofovat
<g/>
)	)	kIx)	)
nad	nad	k7c7	nad
záhadami	záhada	k1gFnPc7	záhada
lidské	lidský	k2eAgFnSc2d1	lidská
duše	duše	k1gFnSc2	duše
a	a	k8xC	a
osudovými	osudový	k2eAgFnPc7d1	osudová
náhodami	náhoda	k1gFnPc7	náhoda
v	v	k7c6	v
životě	život	k1gInSc6	život
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
napsal	napsat	k5eAaPmAgMnS	napsat
sám	sám	k3xTgMnSc1	sám
</s>
</p>
<p>
<s>
Krakonošova	Krakonošův	k2eAgFnSc1d1	Krakonošova
zahrada	zahrada	k1gFnSc1	zahrada
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
–	–	k?	–
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
knižní	knižní	k2eAgNnSc4d1	knižní
vydání	vydání	k1gNnSc4	vydání
jeho	jeho	k3xOp3gFnPc2	jeho
próz	próza	k1gFnPc2	próza
otiskovaných	otiskovaný	k2eAgFnPc2d1	otiskovaná
v	v	k7c6	v
časopisech	časopis	k1gInPc6	časopis
<g/>
,	,	kIx,	,
napsaných	napsaný	k2eAgMnPc2d1	napsaný
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
Josefem	Josef	k1gMnSc7	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
prací	práce	k1gFnPc2	práce
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1908	[number]	k4	1908
<g/>
–	–	k?	–
<g/>
1911	[number]	k4	1911
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kritika	kritika	k1gFnSc1	kritika
slov	slovo	k1gNnPc2	slovo
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
–	–	k?	–
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
díle	díl	k1gInSc6	díl
pomocí	pomocí	k7c2	pomocí
sloupků	sloupek	k1gInPc2	sloupek
podrobil	podrobit	k5eAaPmAgMnS	podrobit
rozboru	rozbor	k1gInSc6	rozbor
frázi	fráze	k1gFnSc4	fráze
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
zneužití	zneužití	k1gNnSc4	zneužití
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Trapné	trapný	k2eAgFnPc1d1	trapná
povídky	povídka	k1gFnPc1	povídka
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
–	–	k?	–
skepticky	skepticky	k6eAd1	skepticky
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
o	o	k7c6	o
smyslu	smysl	k1gInSc6	smysl
lidské	lidský	k2eAgFnSc2d1	lidská
existence	existence	k1gFnSc2	existence
a	a	k8xC	a
jeho	on	k3xPp3gNnSc2	on
duševna	duševno	k1gNnSc2	duševno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
absolutno	absolutno	k1gNnSc4	absolutno
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
–	–	k?	–
fejetonový	fejetonový	k2eAgInSc4d1	fejetonový
román	román	k1gInSc4	román
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
románu	román	k1gInSc6	román
lidstvo	lidstvo	k1gNnSc1	lidstvo
získá	získat	k5eAaPmIp3nS	získat
od	od	k7c2	od
českého	český	k2eAgMnSc2d1	český
vynálezce	vynálezce	k1gMnSc2	vynálezce
<g/>
,	,	kIx,	,
inženýra	inženýr	k1gMnSc2	inženýr
Marka	Marek	k1gMnSc2	Marek
<g/>
,	,	kIx,	,
vynález	vynález	k1gInSc4	vynález
motoru	motor	k1gInSc2	motor
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
může	moct	k5eAaImIp3nS	moct
jako	jako	k8xC	jako
palivo	palivo	k1gNnSc4	palivo
sloužit	sloužit	k5eAaImF	sloužit
jakákoliv	jakýkoliv	k3yIgFnSc1	jakýkoliv
látka	látka	k1gFnSc1	látka
a	a	k8xC	a
kterou	který	k3yQgFnSc4	který
spálí	spálit	k5eAaPmIp3nS	spálit
beze	beze	k7c2	beze
zbytku	zbytek	k1gInSc2	zbytek
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
zdrojem	zdroj	k1gInSc7	zdroj
energie	energie	k1gFnSc2	energie
lidstvo	lidstvo	k1gNnSc1	lidstvo
postupně	postupně	k6eAd1	postupně
získá	získat	k5eAaPmIp3nS	získat
blahobyt	blahobyt	k1gInSc4	blahobyt
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
spalování	spalování	k1gNnSc6	spalování
se	se	k3xPyFc4	se
ale	ale	k9	ale
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
záření	záření	k1gNnSc1	záření
–	–	k?	–
absolutno	absolutno	k1gNnSc1	absolutno
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
dělá	dělat	k5eAaImIp3nS	dělat
z	z	k7c2	z
lidí	člověk	k1gMnPc2	člověk
náboženské	náboženský	k2eAgFnSc2d1	náboženská
fanatiky	fanatik	k1gMnPc4	fanatik
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
těchto	tento	k3xDgMnPc2	tento
lidí	člověk	k1gMnPc2	člověk
přibývá	přibývat	k5eAaImIp3nS	přibývat
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
začíná	začínat	k5eAaImIp3nS	začínat
chaos	chaos	k1gInSc4	chaos
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
vypuká	vypukat	k5eAaImIp3nS	vypukat
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
končí	končit	k5eAaImIp3nS	končit
úplným	úplný	k2eAgNnSc7d1	úplné
vyčerpáním	vyčerpání	k1gNnSc7	vyčerpání
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
si	se	k3xPyFc3	se
teprve	teprve	k6eAd1	teprve
lidé	člověk	k1gMnPc1	člověk
uvědomí	uvědomit	k5eAaPmIp3nP	uvědomit
viníka	viník	k1gMnSc4	viník
a	a	k8xC	a
všechny	všechen	k3xTgInPc4	všechen
motory	motor	k1gInPc4	motor
zničí	zničit	k5eAaPmIp3nS	zničit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krakatit	Krakatit	k1gInSc1	Krakatit
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
–	–	k?	–
román	román	k1gInSc1	román
<g/>
.	.	kIx.	.
</s>
<s>
Ing.	ing.	kA	ing.
Prokop	Prokop	k1gMnSc1	Prokop
objeví	objevit	k5eAaPmIp3nS	objevit
novou	nový	k2eAgFnSc4d1	nová
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
silnou	silný	k2eAgFnSc4d1	silná
trhavinu	trhavina	k1gFnSc4	trhavina
<g/>
,	,	kIx,	,
schopnou	schopný	k2eAgFnSc7d1	schopná
zničit	zničit	k5eAaPmF	zničit
celý	celý	k2eAgInSc4d1	celý
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
přítel	přítel	k1gMnSc1	přítel
Tomeš	Tomeš	k1gMnSc1	Tomeš
získá	získat	k5eAaPmIp3nS	získat
kvůli	kvůli	k7c3	kvůli
penězům	peníze	k1gInPc3	peníze
tajemství	tajemství	k1gNnSc2	tajemství
krakatitu	krakatit	k1gInSc2	krakatit
a	a	k8xC	a
uteče	utéct	k5eAaPmIp3nS	utéct
<g/>
.	.	kIx.	.
</s>
<s>
Prokop	Prokop	k1gMnSc1	Prokop
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
vydá	vydat	k5eAaPmIp3nS	vydat
hledat	hledat	k5eAaImF	hledat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zabránil	zabránit	k5eAaPmAgInS	zabránit
zneužití	zneužití	k1gNnSc3	zneužití
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
se	se	k3xPyFc4	se
setkává	setkávat	k5eAaImIp3nS	setkávat
s	s	k7c7	s
pohádkovým	pohádkový	k2eAgMnSc7d1	pohádkový
stařečkem	stařeček	k1gMnSc7	stařeček
a	a	k8xC	a
rozmlouvají	rozmlouvat	k5eAaImIp3nP	rozmlouvat
spolu	spolu	k6eAd1	spolu
o	o	k7c6	o
smyslu	smysl	k1gInSc6	smysl
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Závěr	závěr	k1gInSc1	závěr
je	být	k5eAaImIp3nS	být
postaven	postavit	k5eAaPmNgInS	postavit
na	na	k7c6	na
myšlence	myšlenka	k1gFnSc6	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
nemá	mít	k5eNaImIp3nS	mít
dělat	dělat	k5eAaImF	dělat
velké	velký	k2eAgInPc4d1	velký
skutky	skutek	k1gInPc4	skutek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
takové	takový	k3xDgFnPc1	takový
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mu	on	k3xPp3gMnSc3	on
mají	mít	k5eAaImIp3nP	mít
sloužit	sloužit	k5eAaImF	sloužit
<g/>
.	.	kIx.	.
</s>
<s>
Dvakrát	dvakrát	k6eAd1	dvakrát
zfilmováno	zfilmován	k2eAgNnSc1d1	zfilmováno
Otakarem	Otakar	k1gMnSc7	Otakar
Vávrou	Vávra	k1gMnSc7	Vávra
pod	pod	k7c4	pod
názvy	název	k1gInPc4	název
Krakatit	Krakatit	k1gInSc1	Krakatit
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
a	a	k8xC	a
Temné	temný	k2eAgNnSc1d1	temné
slunce	slunce	k1gNnSc1	slunce
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
nejbližších	blízký	k2eAgFnPc6d3	nejbližší
věcech	věc	k1gFnPc6	věc
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
–	–	k?	–
knižní	knižní	k2eAgNnSc4d1	knižní
vydání	vydání	k1gNnSc4	vydání
jeho	jeho	k3xOp3gInPc2	jeho
sloupků	sloupek	k1gInPc2	sloupek
<g/>
,	,	kIx,	,
drobné	drobný	k2eAgFnPc4d1	drobná
úvahy	úvaha	k1gFnPc4	úvaha
</s>
</p>
<p>
<s>
Skandální	skandální	k2eAgFnSc1d1	skandální
aféra	aféra	k1gFnSc1	aféra
Josefa	Josef	k1gMnSc2	Josef
Holouška	holoušek	k1gMnSc2	holoušek
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
–	–	k?	–
satirické	satirický	k2eAgFnSc2d1	satirická
povídky	povídka	k1gFnSc2	povídka
o	o	k7c6	o
novinách	novina	k1gFnPc6	novina
<g/>
,	,	kIx,	,
novinářích	novinář	k1gMnPc6	novinář
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgNnPc6	jenž
autor	autor	k1gMnSc1	autor
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
poměry	poměr	k1gInPc4	poměr
v	v	k7c6	v
tisku	tisk	k1gInSc6	tisk
v	v	k7c6	v
období	období	k1gNnSc6	období
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
světovými	světový	k2eAgFnPc7d1	světová
válkami	válka	k1gFnPc7	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Povídky	povídka	k1gFnPc1	povídka
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
kapsy	kapsa	k1gFnSc2	kapsa
<g/>
,	,	kIx,	,
Povídky	povídka	k1gFnPc1	povídka
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
kapsy	kapsa	k1gFnSc2	kapsa
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
–	–	k?	–
povídky	povídka	k1gFnPc4	povídka
s	s	k7c7	s
detektivní	detektivní	k2eAgFnSc7d1	detektivní
tematikou	tematika	k1gFnSc7	tematika
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
příběh	příběh	k1gInSc1	příběh
je	být	k5eAaImIp3nS	být
podáván	podávat	k5eAaImNgInS	podávat
humornou	humorný	k2eAgFnSc7d1	humorná
formou	forma	k1gFnSc7	forma
<g/>
.	.	kIx.	.
</s>
<s>
Hrdina	Hrdina	k1gMnSc1	Hrdina
většinou	většinou	k6eAd1	většinou
není	být	k5eNaImIp3nS	být
velký	velký	k2eAgMnSc1d1	velký
zločinec	zločinec	k1gMnSc1	zločinec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
malý	malý	k2eAgMnSc1d1	malý
zlodějíček	zlodějíček	k1gMnSc1	zlodějíček
<g/>
,	,	kIx,	,
také	také	k9	také
kriminalista	kriminalista	k1gMnSc1	kriminalista
je	být	k5eAaImIp3nS	být
obyčejný	obyčejný	k2eAgMnSc1d1	obyčejný
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
dílech	díl	k1gInPc6	díl
jde	jít	k5eAaImIp3nS	jít
především	především	k9	především
o	o	k7c4	o
vystižení	vystižení	k1gNnSc4	vystižení
různých	různý	k2eAgInPc2d1	různý
typů	typ	k1gInPc2	typ
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
povídkách	povídka	k1gFnPc6	povídka
se	se	k3xPyFc4	se
odráží	odrážet	k5eAaImIp3nS	odrážet
relativismus	relativismus	k1gInSc1	relativismus
–	–	k?	–
není	být	k5eNaImIp3nS	být
přesně	přesně	k6eAd1	přesně
dána	dán	k2eAgFnSc1d1	dána
pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
úhlu	úhel	k1gInSc6	úhel
pohledu	pohled	k1gInSc2	pohled
<g/>
.	.	kIx.	.
</s>
<s>
Čtení	čtení	k1gNnSc1	čtení
tak	tak	k9	tak
není	být	k5eNaImIp3nS	být
pouze	pouze	k6eAd1	pouze
humorné	humorný	k2eAgNnSc1d1	humorné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
k	k	k7c3	k
zamyšlení	zamyšlení	k1gNnSc3	zamyšlení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zahradníkův	Zahradníkův	k2eAgInSc1d1	Zahradníkův
rok	rok	k1gInSc1	rok
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
–	–	k?	–
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
fejetony	fejeton	k1gInPc4	fejeton
o	o	k7c6	o
zahradničení	zahradničený	k2eAgMnPc1d1	zahradničený
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
zde	zde	k6eAd1	zde
pomocí	pomocí	k7c2	pomocí
reakcí	reakce	k1gFnPc2	reakce
na	na	k7c4	na
děje	dít	k5eAaImIp3nS	dít
související	související	k2eAgMnSc1d1	související
se	se	k3xPyFc4	se
zahradničením	zahradničením	k?	zahradničením
(	(	kIx(	(
<g/>
např.	např.	kA	např.
počasí	počasí	k1gNnSc4	počasí
<g/>
)	)	kIx)	)
vystihuje	vystihovat	k5eAaImIp3nS	vystihovat
českou	český	k2eAgFnSc4d1	Česká
povahu	povaha	k1gFnSc4	povaha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Marsyas	Marsyas	k1gInSc1	Marsyas
čili	čili	k8xC	čili
na	na	k7c4	na
okraj	okraj	k1gInSc4	okraj
literatury	literatura	k1gFnSc2	literatura
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
–	–	k?	–
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
v	v	k7c6	v
několika	několik	k4yIc6	několik
esejích	esej	k1gInPc6	esej
zabývá	zabývat	k5eAaImIp3nS	zabývat
tzv.	tzv.	kA	tzv.
brakovou	brakový	k2eAgFnSc7d1	braková
literaturou	literatura	k1gFnSc7	literatura
–	–	k?	–
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gFnSc1	její
existence	existence	k1gFnSc1	existence
je	být	k5eAaImIp3nS	být
nutná	nutný	k2eAgFnSc1d1	nutná
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
poměrně	poměrně	k6eAd1	poměrně
kvalitní	kvalitní	k2eAgInSc4d1	kvalitní
rozbor	rozbor	k1gInSc4	rozbor
tohoto	tento	k3xDgInSc2	tento
žánru	žánr	k1gInSc2	žánr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Apokryfy	apokryf	k1gInPc1	apokryf
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
–	–	k?	–
povídky	povídka	k1gFnSc2	povídka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
stylizuje	stylizovat	k5eAaImIp3nS	stylizovat
historická	historický	k2eAgFnSc1d1	historická
a	a	k8xC	a
především	především	k6eAd1	především
biblická	biblický	k2eAgNnPc1d1	biblické
témata	téma	k1gNnPc1	téma
</s>
</p>
<p>
<s>
O	o	k7c6	o
věcech	věc	k1gFnPc6	věc
obecných	obecná	k1gFnPc2	obecná
čili	čili	k8xC	čili
Zoon	Zoona	k1gFnPc2	Zoona
politikon	politikona	k1gFnPc2	politikona
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
–	–	k?	–
fejetony	fejeton	k1gInPc1	fejeton
a	a	k8xC	a
stati	stať	k1gFnPc1	stať
</s>
</p>
<p>
<s>
Následující	následující	k2eAgNnPc1d1	následující
díla	dílo	k1gNnPc1	dílo
tvoří	tvořit	k5eAaImIp3nP	tvořit
volnou	volný	k2eAgFnSc4d1	volná
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
noetickou	noetický	k2eAgFnSc4d1	noetická
<g/>
)	)	kIx)	)
trilogii	trilogie	k1gFnSc4	trilogie
spojenou	spojený	k2eAgFnSc4d1	spojená
podobnými	podobný	k2eAgFnPc7d1	podobná
filosofickými	filosofický	k2eAgFnPc7d1	filosofická
myšlenkami	myšlenka	k1gFnPc7	myšlenka
a	a	k8xC	a
závěry	závěr	k1gInPc7	závěr
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
trilogie	trilogie	k1gFnSc1	trilogie
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
vrcholům	vrchol	k1gInPc3	vrchol
jeho	jeho	k3xOp3gNnPc2	jeho
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ze	z	k7c2	z
stejných	stejný	k2eAgNnPc2d1	stejné
fakt	faktum	k1gNnPc2	faktum
vyvozují	vyvozovat	k5eAaImIp3nP	vyvozovat
různí	různý	k2eAgMnPc1d1	různý
lidé	člověk	k1gMnPc1	člověk
odlišné	odlišný	k2eAgFnSc2d1	odlišná
závěry	závěra	k1gFnSc2	závěra
<g/>
,	,	kIx,	,
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
zde	zde	k6eAd1	zde
mnohost	mnohost	k1gFnSc4	mnohost
pohledů	pohled	k1gInPc2	pohled
na	na	k7c4	na
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
relativnost	relativnost	k1gFnSc4	relativnost
pravdy	pravda	k1gFnSc2	pravda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hordubal	Hordubat	k5eAaPmAgMnS	Hordubat
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
zfilmováno	zfilmován	k2eAgNnSc4d1	zfilmováno
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
–	–	k?	–
základem	základ	k1gInSc7	základ
této	tento	k3xDgFnSc2	tento
novely	novela	k1gFnSc2	novela
byla	být	k5eAaImAgFnS	být
skutečná	skutečný	k2eAgFnSc1d1	skutečná
událost	událost	k1gFnSc1	událost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
sedlák	sedlák	k1gMnSc1	sedlák
(	(	kIx(	(
<g/>
Hordubal	Hordubal	k1gMnSc1	Hordubal
<g/>
)	)	kIx)	)
po	po	k7c6	po
několika	několik	k4yIc6	několik
letech	léto	k1gNnPc6	léto
práce	práce	k1gFnSc2	práce
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
vrátil	vrátit	k5eAaPmAgMnS	vrátit
domů	domů	k6eAd1	domů
na	na	k7c4	na
Podkarpatskou	podkarpatský	k2eAgFnSc4d1	Podkarpatská
Rus	Rus	k1gFnSc4	Rus
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
zavražděn	zavražděn	k2eAgMnSc1d1	zavražděn
svojí	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
a	a	k8xC	a
jejím	její	k3xOp3gMnSc7	její
milencem	milenec	k1gMnSc7	milenec
<g/>
.	.	kIx.	.
</s>
<s>
Čapek	Čapek	k1gMnSc1	Čapek
si	se	k3xPyFc3	se
zde	zde	k6eAd1	zde
kladl	klást	k5eAaImAgMnS	klást
otázky	otázka	k1gFnPc4	otázka
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
zda	zda	k8xS	zda
jsme	být	k5eAaImIp1nP	být
vůbec	vůbec	k9	vůbec
oprávněni	oprávněn	k2eAgMnPc1d1	oprávněn
spravedlivě	spravedlivě	k6eAd1	spravedlivě
soudit	soudit	k5eAaImF	soudit
vinu	vina	k1gFnSc4	vina
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
usvědčení	usvědčení	k1gNnSc6	usvědčení
se	se	k3xPyFc4	se
ztratí	ztratit	k5eAaPmIp3nS	ztratit
Hordubalovo	Hordubalův	k2eAgNnSc1d1	Hordubalovo
srdce	srdce	k1gNnSc1	srdce
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
nevyřešený	vyřešený	k2eNgInSc1d1	nevyřešený
příběh	příběh	k1gInSc1	příběh
Hordubala	Hordubala	k1gFnSc2	Hordubala
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Povětroň	povětroň	k1gInSc1	povětroň
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
–	–	k?	–
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
novele	novela	k1gFnSc6	novela
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
trosek	troska	k1gFnPc2	troska
letadla	letadlo	k1gNnSc2	letadlo
vytažen	vytáhnout	k5eAaPmNgInS	vytáhnout
bez	bez	k1gInSc1	bez
jakýchkoliv	jakýkoliv	k3yIgInPc2	jakýkoliv
dokladů	doklad	k1gInPc2	doklad
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgMnPc1	tři
svědci	svědek	k1gMnPc1	svědek
jeho	on	k3xPp3gNnSc2	on
umírání	umírání	k1gNnSc2	umírání
(	(	kIx(	(
<g/>
zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
sestra	sestra	k1gFnSc1	sestra
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
jasnovidec	jasnovidec	k1gMnSc1	jasnovidec
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
na	na	k7c6	na
základě	základ	k1gInSc6	základ
vlastních	vlastní	k2eAgFnPc2d1	vlastní
představ	představa	k1gFnPc2	představa
a	a	k8xC	a
zkušeností	zkušenost	k1gFnPc2	zkušenost
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
představu	představa	k1gFnSc4	představa
o	o	k7c6	o
umírajícím	umírající	k2eAgInSc6d1	umírající
(	(	kIx(	(
<g/>
na	na	k7c6	na
základě	základ	k1gInSc6	základ
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jací	jaký	k3yIgMnPc1	jaký
jsou	být	k5eAaImIp3nP	být
sami	sám	k3xTgMnPc1	sám
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obyčejný	obyčejný	k2eAgInSc1d1	obyčejný
život	život	k1gInSc1	život
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
–	–	k?	–
železniční	železniční	k2eAgMnSc1d1	železniční
úředník	úředník	k1gMnSc1	úředník
v	v	k7c6	v
penzi	penze	k1gFnSc6	penze
líčí	líčit	k5eAaImIp3nS	líčit
svůj	svůj	k3xOyFgInSc4	svůj
"	"	kIx"	"
<g/>
obyčejný	obyčejný	k2eAgInSc4d1	obyčejný
život	život	k1gInSc4	život
<g/>
"	"	kIx"	"
a	a	k8xC	a
zamýšlí	zamýšlet	k5eAaImIp3nS	zamýšlet
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
přesvědčení	přesvědčení	k1gNnSc3	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vlastně	vlastně	k9	vlastně
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
osobností	osobnost	k1gFnPc2	osobnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
dohromady	dohromady	k6eAd1	dohromady
tvoří	tvořit	k5eAaImIp3nP	tvořit
jeho	jeho	k3xOp3gInSc4	jeho
osud	osud	k1gInSc4	osud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Válka	válka	k1gFnSc1	válka
s	s	k7c7	s
Mloky	mlok	k1gMnPc7	mlok
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
–	–	k?	–
Lidé	člověk	k1gMnPc1	člověk
objevují	objevovat	k5eAaImIp3nP	objevovat
zvláštní	zvláštní	k2eAgMnPc4d1	zvláštní
tvory	tvor	k1gMnPc4	tvor
–	–	k?	–
mloky	mlok	k1gMnPc7	mlok
<g/>
,	,	kIx,	,
inteligentní	inteligentní	k2eAgMnPc4d1	inteligentní
živočichy	živočich	k1gMnPc4	živočich
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
lidé	člověk	k1gMnPc1	člověk
začnou	začít	k5eAaPmIp3nP	začít
cvičit	cvičit	k5eAaImF	cvičit
jako	jako	k9	jako
levnou	levný	k2eAgFnSc4d1	levná
pracovní	pracovní	k2eAgFnSc4d1	pracovní
sílu	síla	k1gFnSc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Mloci	mlok	k1gMnPc1	mlok
se	se	k3xPyFc4	se
však	však	k9	však
vzbouří	vzbouřit	k5eAaPmIp3nP	vzbouřit
<g/>
,	,	kIx,	,
začnou	začít	k5eAaPmIp3nP	začít
organizovat	organizovat	k5eAaBmF	organizovat
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
stát	stát	k1gInSc4	stát
<g/>
,	,	kIx,	,
zvolí	zvolit	k5eAaPmIp3nS	zvolit
si	se	k3xPyFc3	se
svého	svůj	k3xOyFgMnSc4	svůj
vůdce	vůdce	k1gMnSc4	vůdce
a	a	k8xC	a
žádají	žádat	k5eAaImIp3nP	žádat
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
"	"	kIx"	"
<g/>
životní	životní	k2eAgInSc1d1	životní
prostor	prostor	k1gInSc1	prostor
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
spěje	spět	k5eAaImIp3nS	spět
nevyhnutelně	vyhnutelně	k6eNd1	vyhnutelně
k	k	k7c3	k
válečnému	válečný	k2eAgInSc3d1	válečný
konfliktu	konflikt	k1gInSc3	konflikt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
parta	parta	k1gFnSc1	parta
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
zfilmováno	zfilmován	k2eAgNnSc4d1	zfilmováno
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
–	–	k?	–
děj	děj	k1gInSc1	děj
tohoto	tento	k3xDgInSc2	tento
románu	román	k1gInSc2	román
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
dole	dol	k1gInSc6	dol
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
po	po	k7c6	po
závalu	zával	k1gInSc6	zával
zůstane	zůstat	k5eAaPmIp3nS	zůstat
několik	několik	k4yIc1	několik
horníků	horník	k1gMnPc2	horník
a	a	k8xC	a
vedení	vedení	k1gNnSc1	vedení
dolu	dol	k1gInSc2	dol
hledá	hledat	k5eAaImIp3nS	hledat
dobrovolníky	dobrovolník	k1gMnPc4	dobrovolník
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
záchranu	záchrana	k1gFnSc4	záchrana
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
oslavou	oslava	k1gFnSc7	oslava
lidské	lidský	k2eAgFnSc2d1	lidská
solidarity	solidarita	k1gFnSc2	solidarita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
co	co	k3yRnSc1	co
dělá	dělat	k5eAaImIp3nS	dělat
–	–	k?	–
soubor	soubor	k1gInSc1	soubor
causerií	causerie	k1gFnPc2	causerie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Život	život	k1gInSc1	život
a	a	k8xC	a
dílo	dílo	k1gNnSc1	dílo
skladatele	skladatel	k1gMnSc2	skladatel
Foltýna	Foltýn	k1gMnSc2	Foltýn
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
posmrtně	posmrtně	k6eAd1	posmrtně
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgFnSc1d1	televizní
inscenace	inscenace	k1gFnSc1	inscenace
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
–	–	k?	–
nedokončený	dokončený	k2eNgInSc4d1	nedokončený
román	román	k1gInSc4	román
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dramata	drama	k1gNnPc4	drama
===	===	k?	===
</s>
</p>
<p>
<s>
Lásky	láska	k1gFnPc4	láska
hra	hra	k1gFnSc1	hra
osudná	osudný	k2eAgFnSc1d1	osudná
(	(	kIx(	(
<g/>
napsáno	napsat	k5eAaPmNgNnS	napsat
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
–	–	k?	–
jednoaktovka	jednoaktovka	k1gFnSc1	jednoaktovka
<g/>
;	;	kIx,	;
napsáno	napsat	k5eAaPmNgNnS	napsat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
Josefem	Josef	k1gMnSc7	Josef
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Loupežník	loupežník	k1gMnSc1	loupežník
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
zfilmováno	zfilmovat	k5eAaPmNgNnS	zfilmovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
–	–	k?	–
komedie	komedie	k1gFnSc1	komedie
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	s	k7c7	s
vztahem	vztah	k1gInSc7	vztah
mladé	mladý	k2eAgFnSc2d1	mladá
a	a	k8xC	a
starší	starý	k2eAgFnSc2d2	starší
generace	generace	k1gFnSc2	generace
<g/>
.	.	kIx.	.
</s>
<s>
Loupežníkem	loupežník	k1gMnSc7	loupežník
je	být	k5eAaImIp3nS	být
student	student	k1gMnSc1	student
s	s	k7c7	s
pověstí	pověst	k1gFnSc7	pověst
lehkovážného	lehkovážný	k2eAgMnSc2d1	lehkovážný
mladíka	mladík	k1gMnSc2	mladík
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
do	do	k7c2	do
dcery	dcera	k1gFnSc2	dcera
svého	svůj	k3xOyFgMnSc2	svůj
profesora	profesor	k1gMnSc2	profesor
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
o	o	k7c6	o
jejich	jejich	k3xOp3gFnSc6	jejich
lásce	láska	k1gFnSc6	láska
nechce	chtít	k5eNaImIp3nS	chtít
ani	ani	k8xC	ani
slyšet	slyšet	k5eAaImF	slyšet
<g/>
.	.	kIx.	.
</s>
<s>
Loupežník	loupežník	k1gMnSc1	loupežník
nakonec	nakonec	k6eAd1	nakonec
využívá	využívat	k5eAaImIp3nS	využívat
nepřítomnosti	nepřítomnost	k1gFnSc3	nepřítomnost
rodičů	rodič	k1gMnPc2	rodič
a	a	k8xC	a
zamkne	zamknout	k5eAaPmIp3nS	zamknout
se	se	k3xPyFc4	se
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
láskou	láska	k1gFnSc7	láska
v	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
domě	dům	k1gInSc6	dům
a	a	k8xC	a
odmítá	odmítat	k5eAaImIp3nS	odmítat
je	on	k3xPp3gNnSc4	on
pustit	pustit	k5eAaPmF	pustit
dovnitř	dovnitř	k6eAd1	dovnitř
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
donutí	donutit	k5eAaPmIp3nS	donutit
pana	pan	k1gMnSc4	pan
profesora	profesor	k1gMnSc4	profesor
k	k	k7c3	k
přiznání	přiznání	k1gNnSc3	přiznání
<g/>
,	,	kIx,	,
že	že	k8xS	že
mladá	mladý	k2eAgFnSc1d1	mladá
generace	generace	k1gFnSc1	generace
není	být	k5eNaImIp3nS	být
tak	tak	k6eAd1	tak
špatná	špatný	k2eAgFnSc1d1	špatná
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
si	se	k3xPyFc3	se
jeho	jeho	k3xOp3gInPc4	jeho
generace	generace	k1gFnSc1	generace
představuje	představovat	k5eAaImIp3nS	představovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
R.	R.	kA	R.
<g/>
U.	U.	kA	U.
<g/>
R.	R.	kA	R.
(	(	kIx(	(
<g/>
Rossum	Rossum	k1gInSc1	Rossum
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Universal	Universal	k1gFnSc7	Universal
Robots	Robotsa	k1gFnPc2	Robotsa
–	–	k?	–
Rossumovi	Rossumův	k2eAgMnPc1d1	Rossumův
Univerzální	univerzální	k2eAgMnPc1d1	univerzální
Roboti	robot	k1gMnPc1	robot
<g/>
,	,	kIx,	,
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
–	–	k?	–
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
vědeckofantastickém	vědeckofantastický	k2eAgNnSc6d1	vědeckofantastické
dramatu	drama	k1gNnSc6	drama
poprvé	poprvé	k6eAd1	poprvé
zaznělo	zaznět	k5eAaImAgNnS	zaznět
slovo	slovo	k1gNnSc1	slovo
robot	robot	k1gInSc1	robot
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
Čapkovi	Čapek	k1gMnSc3	Čapek
údajně	údajně	k6eAd1	údajně
poradil	poradit	k5eAaPmAgMnS	poradit
bratr	bratr	k1gMnSc1	bratr
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Čapek	Čapek	k1gMnSc1	Čapek
zde	zde	k6eAd1	zde
varuje	varovat	k5eAaImIp3nS	varovat
před	před	k7c7	před
zneužitím	zneužití	k1gNnSc7	zneužití
techniky	technika	k1gFnSc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
Továrník	továrník	k1gMnSc1	továrník
Rossum	Rossum	k1gInSc4	Rossum
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
dokonalé	dokonalý	k2eAgMnPc4d1	dokonalý
roboty	robot	k1gMnPc4	robot
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
nerozeznání	nerozeznání	k1gNnSc3	nerozeznání
od	od	k7c2	od
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Roboti	robot	k1gMnPc1	robot
postupně	postupně	k6eAd1	postupně
vykonávají	vykonávat	k5eAaImIp3nP	vykonávat
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
práce	práce	k1gFnPc1	práce
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
postupně	postupně	k6eAd1	postupně
degenerují	degenerovat	k5eAaBmIp3nP	degenerovat
<g/>
.	.	kIx.	.
</s>
<s>
Roboti	robot	k1gMnPc1	robot
se	se	k3xPyFc4	se
vzbouří	vzbouřit	k5eAaPmIp3nP	vzbouřit
a	a	k8xC	a
začnou	začít	k5eAaPmIp3nP	začít
vyvražďovat	vyvražďovat	k5eAaImF	vyvražďovat
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
vyvraždí	vyvraždit	k5eAaPmIp3nS	vyvraždit
všechny	všechen	k3xTgFnPc4	všechen
až	až	k9	až
na	na	k7c4	na
starého	starý	k2eAgMnSc4d1	starý
Alquista	Alquist	k1gMnSc4	Alquist
<g/>
.	.	kIx.	.
</s>
<s>
Robotům	robot	k1gMnPc3	robot
hrozí	hrozit	k5eAaImIp3nS	hrozit
zánik	zánik	k1gInSc1	zánik
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nejsou	být	k5eNaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
vyrábět	vyrábět	k5eAaImF	vyrábět
sami	sám	k3xTgMnPc1	sám
sebe	sebe	k3xPyFc4	sebe
<g/>
;	;	kIx,	;
po	po	k7c6	po
Alquistovi	Alquista	k1gMnSc6	Alquista
chtějí	chtít	k5eAaImIp3nP	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zase	zase	k9	zase
objevil	objevit	k5eAaPmAgMnS	objevit
tajemství	tajemství	k1gNnSc4	tajemství
jejich	jejich	k3xOp3gFnSc2	jejich
výroby	výroba	k1gFnSc2	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Alquist	Alquist	k1gInSc1	Alquist
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
všemožně	všemožně	k6eAd1	všemožně
zjistit	zjistit	k5eAaPmF	zjistit
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
konstrukci	konstrukce	k1gFnSc6	konstrukce
robotů	robot	k1gInPc2	robot
<g/>
,	,	kIx,	,
dojde	dojít	k5eAaPmIp3nS	dojít
i	i	k9	i
na	na	k7c4	na
vivisekci	vivisekce	k1gFnSc4	vivisekce
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
na	na	k7c6	na
scéně	scéna	k1gFnSc6	scéna
objeví	objevit	k5eAaPmIp3nP	objevit
roboti	robot	k1gMnPc1	robot
z	z	k7c2	z
poslední	poslední	k2eAgFnSc2d1	poslední
série	série	k1gFnSc2	série
<g/>
,	,	kIx,	,
Primus	primus	k1gMnSc1	primus
a	a	k8xC	a
Helena	Helena	k1gFnSc1	Helena
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
některé	některý	k3yIgFnPc4	některý
lidské	lidský	k2eAgFnPc4d1	lidská
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
ostatním	ostatní	k2eAgMnPc3d1	ostatní
robotům	robot	k1gMnPc3	robot
chybí	chybět	k5eAaImIp3nS	chybět
<g/>
.	.	kIx.	.
</s>
<s>
Alquist	Alquist	k1gMnSc1	Alquist
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
milují	milovat	k5eAaImIp3nP	milovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
požehná	požehnat	k5eAaPmIp3nS	požehnat
jim	on	k3xPp3gMnPc3	on
jako	jako	k9	jako
zakladatelům	zakladatel	k1gMnPc3	zakladatel
nového	nový	k2eAgInSc2d1	nový
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
problém	problém	k1gInSc1	problém
–	–	k?	–
svět	svět	k1gInSc4	svět
zamoří	zamořit	k5eAaPmIp3nP	zamořit
roboti	robot	k1gMnPc1	robot
<g/>
;	;	kIx,	;
spása	spása	k1gFnSc1	spása
–	–	k?	–
v	v	k7c6	v
lásce	láska	k1gFnSc6	láska
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
života	život	k1gInSc2	život
hmyzu	hmyz	k1gInSc2	hmyz
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
zhudebněno	zhudebnit	k5eAaPmNgNnS	zhudebnit
J.	J.	kA	J.
Cikkerem	Cikker	k1gInSc7	Cikker
<g/>
)	)	kIx)	)
–	–	k?	–
alegorický	alegorický	k2eAgInSc4d1	alegorický
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
svět	svět	k1gInSc4	svět
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
představuje	představovat	k5eAaImIp3nS	představovat
různé	různý	k2eAgFnPc4d1	různá
lidské	lidský	k2eAgFnPc4d1	lidská
vlastnosti	vlastnost	k1gFnPc4	vlastnost
a	a	k8xC	a
lidské	lidský	k2eAgInPc4d1	lidský
typy	typ	k1gInPc4	typ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
záchranu	záchrana	k1gFnSc4	záchrana
skutečných	skutečný	k2eAgFnPc2d1	skutečná
hodnot	hodnota	k1gFnPc2	hodnota
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
tulák	tulák	k1gMnSc1	tulák
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nejprve	nejprve	k6eAd1	nejprve
mraveniště	mraveniště	k1gNnSc2	mraveniště
nezaujatě	zaujatě	k6eNd1	zaujatě
pozoruje	pozorovat	k5eAaImIp3nS	pozorovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
z	z	k7c2	z
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
stane	stanout	k5eAaPmIp3nS	stanout
aktivním	aktivní	k2eAgMnSc7d1	aktivní
účastníkem	účastník	k1gMnSc7	účastník
děje	děj	k1gInSc2	děj
a	a	k8xC	a
rozšlapává	rozšlapávat	k5eAaImIp3nS	rozšlapávat
mravenčího	mravenčí	k2eAgMnSc4d1	mravenčí
vůdce	vůdce	k1gMnSc4	vůdce
<g/>
.	.	kIx.	.
</s>
<s>
Napsáno	napsán	k2eAgNnSc4d1	napsáno
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
Josefem	Josef	k1gMnSc7	Josef
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Věc	věc	k1gFnSc1	věc
Makropulos	Makropulosa	k1gFnPc2	Makropulosa
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
<g/>
–	–	k?	–
tajemná	tajemný	k2eAgFnSc1d1	tajemná
žena	žena	k1gFnSc1	žena
Elina	Elina	k1gFnSc1	Elina
Makropulos	Makropulos	k1gInSc4	Makropulos
prozradí	prozradit	k5eAaPmIp3nS	prozradit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jí	on	k3xPp3gFnSc3	on
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
300	[number]	k4	300
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dcerou	dcera	k1gFnSc7	dcera
alchymisty	alchymista	k1gMnSc2	alchymista
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
působil	působit	k5eAaImAgMnS	působit
na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
císaře	císař	k1gMnSc2	císař
Rudolfa	Rudolf	k1gMnSc2	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
a	a	k8xC	a
který	který	k3yQgInSc1	který
objevil	objevit	k5eAaPmAgInS	objevit
elixír	elixír	k1gInSc4	elixír
mládí	mládí	k1gNnSc2	mládí
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
mu	on	k3xPp3gMnSc3	on
nařídil	nařídit	k5eAaPmAgMnS	nařídit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tento	tento	k3xDgInSc4	tento
přípravek	přípravek	k1gInSc4	přípravek
vyzkoušel	vyzkoušet	k5eAaPmAgMnS	vyzkoušet
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
dceři	dcera	k1gFnSc6	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
jí	jíst	k5eAaImIp3nS	jíst
její	její	k3xOp3gFnSc4	její
nesmrtelnost	nesmrtelnost	k1gFnSc4	nesmrtelnost
závidí	závidět	k5eAaImIp3nS	závidět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ona	onen	k3xDgFnSc1	onen
je	být	k5eAaImIp3nS	být
unavena	unavit	k5eAaPmNgFnS	unavit
žitím	žití	k1gNnSc7	žití
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
lidské	lidský	k2eAgNnSc1d1	lidské
bytí	bytí	k1gNnSc1	bytí
je	být	k5eAaImIp3nS	být
těžkým	těžký	k2eAgInSc7d1	těžký
údělem	úděl	k1gInSc7	úděl
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
300	[number]	k4	300
let	léto	k1gNnPc2	léto
poznala	poznat	k5eAaPmAgFnS	poznat
všechny	všechen	k3xTgFnPc4	všechen
lidské	lidský	k2eAgFnPc4d1	lidská
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
vidí	vidět	k5eAaImIp3nS	vidět
už	už	k6eAd1	už
jen	jen	k9	jen
něco	něco	k3yInSc1	něco
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
opakuje	opakovat	k5eAaImIp3nS	opakovat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ji	on	k3xPp3gFnSc4	on
unavuje	unavovat	k5eAaImIp3nS	unavovat
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
však	však	k9	však
bojí	bát	k5eAaImIp3nP	bát
smrti	smrt	k1gFnSc3	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
Čapkovo	Čapkův	k2eAgNnSc1d1	Čapkovo
dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
kongeniálně	kongeniálně	k6eAd1	kongeniálně
zhudebněno	zhudebněn	k2eAgNnSc1d1	zhudebněno
Leošem	Leoš	k1gMnSc7	Leoš
Janáčkem	Janáček	k1gMnSc7	Janáček
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
stejnojmenné	stejnojmenný	k2eAgFnSc6d1	stejnojmenná
opeře	opera	k1gFnSc6	opera
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
pravidelnou	pravidelný	k2eAgFnSc7d1	pravidelná
součástí	součást	k1gFnSc7	součást
repertoáru	repertoár	k1gInSc2	repertoár
světových	světový	k2eAgInPc2d1	světový
operních	operní	k2eAgInPc2d1	operní
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Adam	Adam	k1gMnSc1	Adam
stvořitel	stvořitel	k1gMnSc1	stvořitel
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
–	–	k?	–
Adam	Adam	k1gMnSc1	Adam
je	být	k5eAaImIp3nS	být
nespokojen	spokojen	k2eNgMnSc1d1	nespokojen
se	s	k7c7	s
světem	svět	k1gInSc7	svět
jaký	jaký	k9	jaký
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
jej	on	k3xPp3gMnSc4	on
zničí	zničit	k5eAaPmIp3nS	zničit
a	a	k8xC	a
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
nový	nový	k2eAgMnSc1d1	nový
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zklamán	zklamat	k5eAaPmNgMnS	zklamat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
brzy	brzy	k6eAd1	brzy
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nový	nový	k2eAgInSc1d1	nový
svět	svět	k1gInSc1	svět
není	být	k5eNaImIp3nS	být
lepší	dobrý	k2eAgMnSc1d2	lepší
než	než	k8xS	než
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zničil	zničit	k5eAaPmAgMnS	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Napsáno	napsán	k2eAgNnSc4d1	napsáno
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
Josefem	Josef	k1gMnSc7	Josef
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
nemoc	nemoc	k1gFnSc1	nemoc
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
zhudebněno	zhudebněn	k2eAgNnSc1d1	zhudebněno
T.	T.	kA	T.
Andrašovanem	Andrašovan	k1gMnSc7	Andrašovan
<g/>
,	,	kIx,	,
zfilmováno	zfilmovat	k5eAaPmNgNnS	zfilmovat
H.	H.	kA	H.
Haasem	Haas	k1gInSc7	Haas
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
–	–	k?	–
objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
diktátor	diktátor	k1gMnSc1	diktátor
(	(	kIx(	(
<g/>
Maršál	maršál	k1gMnSc1	maršál
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
chce	chtít	k5eAaImIp3nS	chtít
válku	válka	k1gFnSc4	válka
(	(	kIx(	(
<g/>
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
strašlivá	strašlivý	k2eAgFnSc1d1	strašlivá
nemoc	nemoc	k1gFnSc1	nemoc
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zabijí	zabít	k5eAaPmIp3nP	zabít
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
nemocí	nemoc	k1gFnSc7	nemoc
onemocní	onemocnět	k5eAaPmIp3nS	onemocnět
i	i	k9	i
diktátor	diktátor	k1gMnSc1	diktátor
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
mírumilovný	mírumilovný	k2eAgMnSc1d1	mírumilovný
lékař	lékař	k1gMnSc1	lékař
(	(	kIx(	(
<g/>
Galén	Galén	k1gInSc1	Galén
<g/>
)	)	kIx)	)
nachází	nacházet	k5eAaImIp3nS	nacházet
lék	lék	k1gInSc1	lék
<g/>
.	.	kIx.	.
</s>
<s>
Nabízí	nabízet	k5eAaImIp3nP	nabízet
diktátorovi	diktátorův	k2eAgMnPc1d1	diktátorův
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
vyléčí	vyléčit	k5eAaPmIp3nP	vyléčit
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
<s>
Diktátor	diktátor	k1gMnSc1	diktátor
nakonec	nakonec	k6eAd1	nakonec
přijímá	přijímat	k5eAaImIp3nS	přijímat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
lékař	lékař	k1gMnSc1	lékař
je	být	k5eAaImIp3nS	být
zabit	zabit	k2eAgMnSc1d1	zabit
zfanatizovaným	zfanatizovaný	k2eAgInSc7d1	zfanatizovaný
davem	dav	k1gInSc7	dav
cestou	cesta	k1gFnSc7	cesta
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
díla	dílo	k1gNnSc2	dílo
vypuká	vypukat	k5eAaImIp3nS	vypukat
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Matka	matka	k1gFnSc1	matka
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
–	–	k?	–
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
dramatu	drama	k1gNnSc6	drama
hlavní	hlavní	k2eAgFnSc1d1	hlavní
hrdinka	hrdinka	k1gFnSc1	hrdinka
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
Toniho	Toni	k1gMnSc2	Toni
<g/>
,	,	kIx,	,
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
rozhovory	rozhovor	k1gInPc7	rozhovor
snaží	snažit	k5eAaImIp3nS	snažit
zabránit	zabránit	k5eAaPmF	zabránit
svému	svůj	k3xOyFgMnSc3	svůj
jedinému	jediný	k2eAgMnSc3d1	jediný
žijícímu	žijící	k2eAgMnSc3d1	žijící
synovi	syn	k1gMnSc3	syn
(	(	kIx(	(
<g/>
její	její	k3xOp3gMnPc1	její
starší	starý	k2eAgMnPc1d2	starší
synové	syn	k1gMnPc1	syn
i	i	k9	i
manžel	manžel	k1gMnSc1	manžel
již	již	k6eAd1	již
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
nebo	nebo	k8xC	nebo
padli	padnout	k5eAaImAgMnP	padnout
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
odešel	odejít	k5eAaPmAgMnS	odejít
bojovat	bojovat	k5eAaImF	bojovat
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
poznání	poznání	k1gNnSc1	poznání
<g/>
,	,	kIx,	,
že	že	k8xS	že
nepřítel	nepřítel	k1gMnSc1	nepřítel
zabíjí	zabíjet	k5eAaImIp3nS	zabíjet
i	i	k9	i
malé	malý	k2eAgFnPc4d1	malá
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
že	že	k8xS	že
ona	onen	k3xDgFnSc1	onen
není	být	k5eNaImIp3nS	být
sama	sám	k3xTgMnSc4	sám
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
prožívá	prožívat	k5eAaImIp3nS	prožívat
ztrátu	ztráta	k1gFnSc4	ztráta
svých	svůj	k3xOyFgMnPc2	svůj
synů	syn	k1gMnPc2	syn
<g/>
,	,	kIx,	,
ji	on	k3xPp3gFnSc4	on
donutí	donutit	k5eAaPmIp3nS	donutit
dát	dát	k5eAaPmF	dát
Tonimu	Toni	k1gMnSc3	Toni
pušku	puška	k1gFnSc4	puška
a	a	k8xC	a
pustit	pustit	k5eAaPmF	pustit
ho	on	k3xPp3gMnSc4	on
do	do	k7c2	do
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnPc1	její
zemřelí	zemřelý	k2eAgMnPc1d1	zemřelý
synové	syn	k1gMnPc1	syn
a	a	k8xC	a
manžel	manžel	k1gMnSc1	manžel
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
jako	jako	k8xC	jako
normální	normální	k2eAgInPc1d1	normální
postavy	postav	k1gInPc1	postav
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yIgInPc7	který
matka	matka	k1gFnSc1	matka
rozmlouvá	rozmlouvat	k5eAaImIp3nS	rozmlouvat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Cestopisy	cestopis	k1gInPc4	cestopis
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
Čapkových	Čapkových	k2eAgInPc6d1	Čapkových
cestopisech	cestopis	k1gInPc6	cestopis
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
zeměpisnou	zeměpisný	k2eAgFnSc4d1	zeměpisná
studii	studie	k1gFnSc4	studie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
zachycení	zachycení	k1gNnSc4	zachycení
zvláštností	zvláštnost	k1gFnPc2	zvláštnost
<g/>
,	,	kIx,	,
zvyků	zvyk	k1gInPc2	zvyk
a	a	k8xC	a
životního	životní	k2eAgInSc2d1	životní
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Cestopisy	cestopis	k1gInPc1	cestopis
přibližují	přibližovat	k5eAaImIp3nP	přibližovat
čtenáři	čtenář	k1gMnSc3	čtenář
kulturu	kultura	k1gFnSc4	kultura
a	a	k8xC	a
ducha	duch	k1gMnSc2	duch
navštívených	navštívený	k2eAgInPc2d1	navštívený
států	stát	k1gInPc2	stát
v	v	k7c6	v
období	období	k1gNnSc6	období
mezi	mezi	k7c7	mezi
světovými	světový	k2eAgFnPc7d1	světová
válkami	válka	k1gFnPc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
Forma	forma	k1gFnSc1	forma
cestopisů	cestopis	k1gInPc2	cestopis
připomíná	připomínat	k5eAaImIp3nS	připomínat
jeho	jeho	k3xOp3gInPc4	jeho
sloupky	sloupek	k1gInPc4	sloupek
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
cestopisy	cestopis	k1gInPc1	cestopis
posílal	posílat	k5eAaImAgMnS	posílat
do	do	k7c2	do
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
<g/>
,	,	kIx,	,
až	až	k9	až
později	pozdě	k6eAd2	pozdě
vyšly	vyjít	k5eAaPmAgFnP	vyjít
knižně	knižně	k6eAd1	knižně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Italské	italský	k2eAgInPc1d1	italský
listy	list	k1gInPc1	list
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Anglické	anglický	k2eAgInPc1d1	anglický
listy	list	k1gInPc1	list
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Výlet	výlet	k1gInSc1	výlet
do	do	k7c2	do
Španěl	Španěly	k1gInPc2	Španěly
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
z	z	k7c2	z
Holandska	Holandsko	k1gNnSc2	Holandsko
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Cesta	cesta	k1gFnSc1	cesta
na	na	k7c4	na
sever	sever	k1gInSc4	sever
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Dětské	dětský	k2eAgFnPc1d1	dětská
knihy	kniha	k1gFnPc1	kniha
===	===	k?	===
</s>
</p>
<p>
<s>
Devatero	devatero	k1gNnSc1	devatero
pohádek	pohádka	k1gFnPc2	pohádka
a	a	k8xC	a
ještě	ještě	k9	ještě
jedna	jeden	k4xCgFnSc1	jeden
od	od	k7c2	od
Josefa	Josef	k1gMnSc2	Josef
Čapka	Čapek	k1gMnSc2	Čapek
jako	jako	k8xC	jako
přívažek	přívažek	k1gInSc1	přívažek
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
–	–	k?	–
pohádky	pohádka	k1gFnPc4	pohádka
z	z	k7c2	z
různých	různý	k2eAgNnPc2d1	různé
zaměstnání	zaměstnání	k1gNnPc2	zaměstnání
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Čapek	čapka	k1gFnPc2	čapka
dětem	dítě	k1gFnPc3	dítě
sděluje	sdělovat	k5eAaImIp3nS	sdělovat
určité	určitý	k2eAgInPc4d1	určitý
principy	princip	k1gInPc4	princip
lidského	lidský	k2eAgNnSc2d1	lidské
chování	chování	k1gNnSc2	chování
(	(	kIx(	(
<g/>
např.	např.	kA	např.
listovní	listovní	k2eAgNnSc1d1	listovní
tajemství	tajemství	k1gNnSc1	tajemství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dášeňka	Dášeňka	k1gFnSc1	Dášeňka
čili	čili	k8xC	čili
život	život	k1gInSc1	život
štěněte	štěně	k1gNnSc2	štěně
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
–	–	k?	–
toto	tento	k3xDgNnSc4	tento
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
napsané	napsaný	k2eAgNnSc4d1	napsané
a	a	k8xC	a
fotografované	fotografovaný	k2eAgNnSc4d1	fotografované
s	s	k7c7	s
Josefem	Josef	k1gMnSc7	Josef
Čapkem	Čapek	k1gMnSc7	Čapek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vyprávění	vyprávění	k1gNnSc1	vyprávění
o	o	k7c6	o
narození	narození	k1gNnSc6	narození
a	a	k8xC	a
růstu	růst	k1gInSc6	růst
štěňátka	štěňátko	k1gNnSc2	štěňátko
–	–	k?	–
foxteriéra	foxteriér	k1gMnSc4	foxteriér
Dášeňky	Dášeňka	k1gFnSc2	Dášeňka
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
tomuto	tento	k3xDgMnSc3	tento
pejskovi	pejsek	k1gMnSc3	pejsek
psí	psí	k2eAgFnSc2d1	psí
pohádky	pohádka	k1gFnSc2	pohádka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
trochu	trochu	k6eAd1	trochu
také	také	k9	také
bajkami	bajka	k1gFnPc7	bajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Měl	mít	k5eAaImAgMnS	mít
jsem	být	k5eAaImIp1nS	být
psa	pes	k1gMnSc4	pes
a	a	k8xC	a
kočku	kočka	k1gFnSc4	kočka
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pudlenka	Pudlenka	k1gFnSc1	Pudlenka
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
–	–	k?	–
toto	tento	k3xDgNnSc4	tento
posmrtně	posmrtně	k6eAd1	posmrtně
uspořádané	uspořádaný	k2eAgNnSc4d1	uspořádané
dílo	dílo	k1gNnSc4	dílo
je	být	k5eAaImIp3nS	být
o	o	k7c6	o
kočkách	kočka	k1gFnPc6	kočka
a	a	k8xC	a
o	o	k7c6	o
Čapkově	Čapkův	k2eAgFnSc6d1	Čapkova
kočce	kočka	k1gFnSc6	kočka
Pudlence	Pudlence	k1gFnSc2	Pudlence
zvlášť	zvlášť	k6eAd1	zvlášť
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
vydáváno	vydávat	k5eAaPmNgNnS	vydávat
jako	jako	k8xS	jako
Pudlenka	Pudlenka	k1gFnSc1	Pudlenka
aneb	aneb	k?	aneb
Měl	mít	k5eAaImAgMnS	mít
jsem	být	k5eAaImIp1nS	být
psa	pes	k1gMnSc4	pes
a	a	k8xC	a
kočku	kočka	k1gFnSc4	kočka
(	(	kIx(	(
<g/>
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
Čapkova	Čapkův	k2eAgNnPc4d1	Čapkovo
díla	dílo	k1gNnPc4	dílo
Měl	mít	k5eAaImAgInS	mít
jsem	být	k5eAaImIp1nS	být
psa	pes	k1gMnSc4	pes
a	a	k8xC	a
kočku	kočka	k1gFnSc4	kočka
<g/>
,	,	kIx,	,
Člověk	člověk	k1gMnSc1	člověk
a	a	k8xC	a
pes	pes	k1gMnSc1	pes
a	a	k8xC	a
doslov	doslov	k1gInSc1	doslov
Olgy	Olga	k1gFnSc2	Olga
Scheinpflugové	Scheinpflugový	k2eAgFnSc2d1	Scheinpflugová
O	o	k7c4	o
Karlu	Karla	k1gFnSc4	Karla
Čapkovi	Čapek	k1gMnSc3	Čapek
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
zvířátkách	zvířátko	k1gNnPc6	zvířátko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Filozofická	filozofický	k2eAgNnPc1d1	filozofické
díla	dílo	k1gNnPc1	dílo
===	===	k?	===
</s>
</p>
<p>
<s>
Pragmatismus	pragmatismus	k1gInSc1	pragmatismus
čili	čili	k8xC	čili
Filosofie	filosofie	k1gFnSc1	filosofie
praktického	praktický	k2eAgInSc2d1	praktický
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Překlady	překlad	k1gInPc4	překlad
===	===	k?	===
</s>
</p>
<p>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
poezie	poezie	k1gFnSc1	poezie
nové	nový	k2eAgFnSc2d1	nová
doby	doba	k1gFnSc2	doba
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
antologie	antologie	k1gFnSc1	antologie
moderních	moderní	k2eAgMnPc2d1	moderní
francouzských	francouzský	k2eAgMnPc2d1	francouzský
básníků	básník	k1gMnPc2	básník
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
Charlese	Charles	k1gMnSc2	Charles
Baudelaira	Baudelair	k1gMnSc2	Baudelair
po	po	k7c4	po
Guillauma	Guillaum	k1gMnSc4	Guillaum
Apollinaira	Apollinair	k1gMnSc4	Apollinair
<g/>
)	)	kIx)	)
výrazně	výrazně	k6eAd1	výrazně
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
vývoj	vývoj	k1gInSc4	vývoj
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
(	(	kIx(	(
<g/>
např.	např.	kA	např.
postoj	postoj	k1gInSc4	postoj
české	český	k2eAgFnSc2d1	Česká
poezie	poezie	k1gFnSc2	poezie
k	k	k7c3	k
asonanci	asonance	k1gFnSc3	asonance
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
českou	český	k2eAgFnSc4d1	Česká
metriku	metrika	k1gFnSc4	metrika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Překlady	překlad	k1gInPc1	překlad
byly	být	k5eAaImAgInP	být
jazykově	jazykově	k6eAd1	jazykově
dokonale	dokonale	k6eAd1	dokonale
propracované	propracovaný	k2eAgInPc1d1	propracovaný
<g/>
,	,	kIx,	,
na	na	k7c4	na
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
vzorem	vzor	k1gInSc7	vzor
překladu	překlad	k1gInSc2	překlad
a	a	k8xC	a
často	často	k6eAd1	často
i	i	k9	i
původní	původní	k2eAgFnSc2d1	původní
poezie	poezie	k1gFnSc2	poezie
(	(	kIx(	(
<g/>
velmi	velmi	k6eAd1	velmi
ovlivnily	ovlivnit	k5eAaPmAgFnP	ovlivnit
například	například	k6eAd1	například
Vítězslava	Vítězslav	k1gMnSc2	Vítězslav
Nezvala	Nezval	k1gMnSc2	Nezval
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klíčovou	klíčový	k2eAgFnSc7d1	klíčová
součástí	součást	k1gFnSc7	součást
antologie	antologie	k1gFnSc2	antologie
je	být	k5eAaImIp3nS	být
slavná	slavný	k2eAgFnSc1d1	slavná
Apollinairova	Apollinairův	k2eAgFnSc1d1	Apollinairova
báseň	báseň	k1gFnSc1	báseň
Pásmo	pásmo	k1gNnSc4	pásmo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Politická	politický	k2eAgNnPc1d1	politické
díla	dílo	k1gNnPc1	dílo
===	===	k?	===
</s>
</p>
<p>
<s>
Hovory	hovor	k1gInPc1	hovor
s	s	k7c7	s
T.	T.	kA	T.
G.	G.	kA	G.
Masarykem	Masaryk	k1gMnSc7	Masaryk
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
–	–	k?	–
<g/>
1935	[number]	k4	1935
<g/>
,	,	kIx,	,
3	[number]	k4	3
svazky	svazek	k1gInPc7	svazek
<g/>
)	)	kIx)	)
–	–	k?	–
jedinečné	jedinečný	k2eAgNnSc4d1	jedinečné
svědectví	svědectví	k1gNnSc4	svědectví
o	o	k7c6	o
Masarykovi	Masaryk	k1gMnSc6	Masaryk
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gInPc6	jeho
názorech	názor	k1gInPc6	názor
nejen	nejen	k6eAd1	nejen
politických	politický	k2eAgInPc2d1	politický
<g/>
,	,	kIx,	,
filozofických	filozofický	k2eAgInPc2d1	filozofický
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
lidských	lidský	k2eAgNnPc2d1	lidské
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnPc2	jeho
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mlčení	mlčení	k1gNnSc1	mlčení
s	s	k7c7	s
T.	T.	kA	T.
G.	G.	kA	G.
Masarykem	Masaryk	k1gMnSc7	Masaryk
–	–	k?	–
doplnění	doplnění	k1gNnSc2	doplnění
hovorů	hovor	k1gInPc2	hovor
<g/>
,	,	kIx,	,
esej	esej	k1gFnSc1	esej
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Posmrtně	posmrtně	k6eAd1	posmrtně
vydáno	vydat	k5eAaPmNgNnS	vydat
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
K.	K.	kA	K.
Čapka	Čapek	k1gMnSc2	Čapek
byly	být	k5eAaImAgInP	být
sebrány	sebrat	k5eAaPmNgInP	sebrat
a	a	k8xC	a
vydány	vydat	k5eAaPmNgInP	vydat
jeho	jeho	k3xOp3gInPc1	jeho
novinové	novinový	k2eAgInPc1d1	novinový
články	článek	k1gInPc1	článek
<g/>
,	,	kIx,	,
fejetony	fejeton	k1gInPc1	fejeton
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
Měl	mít	k5eAaImAgMnS	mít
jsem	být	k5eAaImIp1nS	být
psa	pes	k1gMnSc4	pes
a	a	k8xC	a
kočku	kočka	k1gFnSc4	kočka
–	–	k?	–
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
prózy	próza	k1gFnPc1	próza
a	a	k8xC	a
fejetony	fejeton	k1gInPc1	fejeton
</s>
</p>
<p>
<s>
Kalendář	kalendář	k1gInSc1	kalendář
–	–	k?	–
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
sloupky	sloupek	k1gInPc7	sloupek
</s>
</p>
<p>
<s>
O	o	k7c6	o
lidech	lid	k1gInPc6	lid
–	–	k?	–
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
sloupky	sloupek	k1gInPc7	sloupek
</s>
</p>
<p>
<s>
Vzrušené	vzrušený	k2eAgInPc4d1	vzrušený
tance	tanec	k1gInPc4	tanec
–	–	k?	–
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
básně	báseň	k1gFnPc1	báseň
</s>
</p>
<p>
<s>
Bajky	bajka	k1gFnPc4	bajka
a	a	k8xC	a
podpovídky	podpovídka	k1gFnPc4	podpovídka
–	–	k?	–
1946	[number]	k4	1946
</s>
</p>
<p>
<s>
Sedm	sedm	k4xCc1	sedm
rozhlásků	rozhlásek	k1gInPc2	rozhlásek
K.	K.	kA	K.
Č.	Č.	kA	Č.
–	–	k?	–
1946	[number]	k4	1946
</s>
</p>
<p>
<s>
Ratolest	ratolest	k1gFnSc1	ratolest
a	a	k8xC	a
vavřín	vavřín	k1gInSc1	vavřín
–	–	k?	–
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
sloupky	sloupek	k1gInPc7	sloupek
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
z	z	k7c2	z
domova	domov	k1gInSc2	domov
–	–	k?	–
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
fejetony	fejeton	k1gInPc7	fejeton
a	a	k8xC	a
sloupky	sloupek	k1gInPc7	sloupek
</s>
</p>
<p>
<s>
Věci	věc	k1gFnPc1	věc
kolem	kolem	k7c2	kolem
nás	my	k3xPp1nPc2	my
–	–	k?	–
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
fejetony	fejeton	k1gInPc7	fejeton
a	a	k8xC	a
sloupky	sloupek	k1gInPc7	sloupek
</s>
</p>
<p>
<s>
Sloupkový	sloupkový	k2eAgInSc1d1	sloupkový
ambit	ambit	k1gInSc1	ambit
–	–	k?	–
1957	[number]	k4	1957
</s>
</p>
<p>
<s>
Poznámky	poznámka	k1gFnPc1	poznámka
o	o	k7c6	o
tvorbě	tvorba	k1gFnSc6	tvorba
–	–	k?	–
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
prací	práce	k1gFnPc2	práce
o	o	k7c6	o
literatuře	literatura	k1gFnSc6	literatura
</s>
</p>
<p>
<s>
Na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
dnů	den	k1gInPc2	den
–	–	k?	–
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
novinářských	novinářský	k2eAgFnPc2d1	novinářská
prací	práce	k1gFnPc2	práce
</s>
</p>
<p>
<s>
Čtení	čtení	k1gNnSc1	čtení
o	o	k7c4	o
T.G.	T.G.	k1gFnSc4	T.G.
<g/>
Masarykovi	Masaryk	k1gMnSc3	Masaryk
–	–	k?	–
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Místo	místo	k1gNnSc1	místo
pro	pro	k7c4	pro
Jonathana	Jonathan	k1gMnSc4	Jonathan
<g/>
!	!	kIx.	!
</s>
<s>
–	–	k?	–
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
výbor	výbor	k1gInSc1	výbor
ze	z	k7c2	z
statí	stať	k1gFnPc2	stať
o	o	k7c6	o
literatuře	literatura	k1gFnSc6	literatura
a	a	k8xC	a
kultuře	kultura	k1gFnSc6	kultura
</s>
</p>
<p>
<s>
Listy	list	k1gInPc1	list
Olze	Olga	k1gFnSc6	Olga
–	–	k?	–
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
dopisy	dopis	k1gInPc1	dopis
Olze	Olga	k1gFnSc3	Olga
Scheinpflugové	Scheinpflugový	k2eAgMnPc4d1	Scheinpflugový
</s>
</p>
<p>
<s>
Drobty	drobet	k1gInPc1	drobet
pod	pod	k7c7	pod
stolem	stol	k1gInSc7	stol
doby	doba	k1gFnSc2	doba
–	–	k?	–
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
veršované	veršovaný	k2eAgInPc1d1	veršovaný
komentáře	komentář	k1gInPc1	komentář
ke	k	k7c3	k
dni	den	k1gInSc3	den
</s>
</p>
<p>
<s>
Skandální	skandální	k2eAgFnSc1d1	skandální
aféra	aféra	k1gFnSc1	aféra
Josefa	Josef	k1gMnSc2	Josef
Holouška	holoušek	k1gMnSc2	holoušek
<g/>
,	,	kIx,	,
Podivuhodné	podivuhodný	k2eAgInPc1d1	podivuhodný
sny	sen	k1gInPc1	sen
redaktora	redaktor	k1gMnSc2	redaktor
Koubka	Koubek	k1gMnSc2	Koubek
–	–	k?	–
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc1	dva
satirické	satirický	k2eAgFnPc1d1	satirická
povídky	povídka	k1gFnPc1	povídka
</s>
</p>
<p>
<s>
Listy	list	k1gInPc1	list
Anielce	Anielec	k1gInSc2	Anielec
–	–	k?	–
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
dopisy	dopis	k1gInPc1	dopis
Anně	Anna	k1gFnSc6	Anna
Nepeřené	Nepeřený	k2eAgFnSc6d1	Nepeřená
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
</s>
</p>
<p>
<s>
Dopisy	dopis	k1gInPc1	dopis
ze	z	k7c2	z
zásuvky	zásuvka	k1gFnSc2	zásuvka
–	–	k?	–
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
dopisy	dopis	k1gInPc4	dopis
Věře	Věra	k1gFnSc3	Věra
Hrůzové	Hrůzová	k1gFnSc2	Hrůzová
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1920	[number]	k4	1920
<g/>
–	–	k?	–
<g/>
1931	[number]	k4	1931
</s>
</p>
<p>
<s>
Filmová	filmový	k2eAgNnPc1d1	filmové
libreta	libreto	k1gNnPc1	libreto
–	–	k?	–
1988	[number]	k4	1988
</s>
</p>
<p>
<s>
Pseudonymní	pseudonymní	k2eAgFnSc1d1	pseudonymní
a	a	k8xC	a
anonymní	anonymní	k2eAgFnSc1d1	anonymní
tvorba	tvorba	k1gFnSc1	tvorba
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapek	k1gMnSc2	Čapek
v	v	k7c6	v
týdeníku	týdeník	k1gInSc6	týdeník
Nebojsa	nebojsa	k1gMnSc1	nebojsa
/	/	kIx~	/
<g/>
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1920	[number]	k4	1920
<g/>
/	/	kIx~	/
–	–	k?	–
2000	[number]	k4	2000
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Vojáku	voják	k1gMnSc5	voják
Vladimíre	Vladimír	k1gMnSc5	Vladimír
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
–	–	k?	–
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
korespondence	korespondence	k1gFnSc1	korespondence
s	s	k7c7	s
Jindřichem	Jindřich	k1gMnSc7	Jindřich
Groagem	Groag	k1gMnSc7	Groag
</s>
</p>
<p>
<s>
==	==	k?	==
Citát	citát	k1gInSc1	citát
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Divadelní	divadelní	k2eAgFnSc1d1	divadelní
režie	režie	k1gFnSc1	režie
–	–	k?	–
Vinohradské	vinohradský	k2eAgNnSc4d1	Vinohradské
divadlo	divadlo	k1gNnSc4	divadlo
==	==	k?	==
</s>
</p>
<p>
<s>
1921	[number]	k4	1921
Julius	Julius	k1gMnSc1	Julius
Zeyer	Zeyer	k1gMnSc1	Zeyer
<g/>
:	:	kIx,	:
Stará	starý	k2eAgFnSc1d1	stará
historie	historie	k1gFnSc1	historie
<g/>
,	,	kIx,	,
21	[number]	k4	21
repríz	repríza	k1gFnPc2	repríza
</s>
</p>
<p>
<s>
1922	[number]	k4	1922
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
<g/>
:	:	kIx,	:
Věc	věc	k1gFnSc1	věc
Makropulos	Makropulosa	k1gFnPc2	Makropulosa
<g/>
,	,	kIx,	,
24	[number]	k4	24
repríz	repríza	k1gFnPc2	repríza
</s>
</p>
<p>
<s>
1922	[number]	k4	1922
Percy	Perc	k2eAgInPc1d1	Perc
Bysshe	Byssh	k1gInPc1	Byssh
Shelley	Shellea	k1gFnSc2	Shellea
<g/>
:	:	kIx,	:
Cenci	Cence	k1gFnSc4	Cence
<g/>
,	,	kIx,	,
8	[number]	k4	8
repríz	repríza	k1gFnPc2	repríza
</s>
</p>
<p>
<s>
1922	[number]	k4	1922
Henri	Henri	k1gNnSc1	Henri
Ghéon	Ghéona	k1gFnPc2	Ghéona
<g/>
:	:	kIx,	:
Chléb	chléb	k1gInSc1	chléb
<g/>
,	,	kIx,	,
8	[number]	k4	8
repríz	repríza	k1gFnPc2	repríza
</s>
</p>
<p>
<s>
1922	[number]	k4	1922
Moliè	Moliè	k1gFnPc2	Moliè
<g/>
:	:	kIx,	:
Lékařem	lékař	k1gMnSc7	lékař
proti	proti	k7c3	proti
své	svůj	k3xOyFgFnSc3	svůj
vůli	vůle	k1gFnSc3	vůle
<g/>
,	,	kIx,	,
18	[number]	k4	18
repríz	repríza	k1gFnPc2	repríza
</s>
</p>
<p>
<s>
1922	[number]	k4	1922
Moliè	Moliè	k1gFnSc1	Moliè
<g/>
:	:	kIx,	:
Sganarelle	Sganarelle	k1gFnSc1	Sganarelle
čili	čili	k8xC	čili
Domnělý	domnělý	k2eAgMnSc1d1	domnělý
paroháč	paroháč	k1gMnSc1	paroháč
<g/>
,	,	kIx,	,
15	[number]	k4	15
repríz	repríza	k1gFnPc2	repríza
</s>
</p>
<p>
<s>
1922	[number]	k4	1922
Moliè	Moliè	k1gFnSc1	Moliè
<g/>
:	:	kIx,	:
Vynucený	vynucený	k2eAgInSc1d1	vynucený
sňatek	sňatek	k1gInSc1	sňatek
<g/>
,	,	kIx,	,
18	[number]	k4	18
repríz	repríza	k1gFnPc2	repríza
</s>
</p>
<p>
<s>
1923	[number]	k4	1923
Aristofanés	Aristofanés	k1gInSc1	Aristofanés
<g/>
:	:	kIx,	:
Jezdci	jezdec	k1gMnPc1	jezdec
<g/>
,	,	kIx,	,
14	[number]	k4	14
repríz	repríza	k1gFnPc2	repríza
</s>
</p>
<p>
<s>
1923	[number]	k4	1923
Aristofanés	Aristofanés	k1gInSc1	Aristofanés
<g/>
:	:	kIx,	:
Ženský	ženský	k2eAgInSc1d1	ženský
sněm	sněm	k1gInSc1	sněm
<g/>
,	,	kIx,	,
5	[number]	k4	5
repríz	repríza	k1gFnPc2	repríza
</s>
</p>
<p>
<s>
1923	[number]	k4	1923
Fráňa	Fráňa	k1gMnSc1	Fráňa
Šrámek	Šrámek	k1gMnSc1	Šrámek
<g/>
:	:	kIx,	:
Plačící	plačící	k2eAgMnSc1d1	plačící
satyr	satyr	k1gMnSc1	satyr
<g/>
,	,	kIx,	,
13	[number]	k4	13
repríz	repríza	k1gFnPc2	repríza
</s>
</p>
<p>
<s>
1923	[number]	k4	1923
Josef	Josef	k1gMnSc1	Josef
Čapek	Čapek	k1gMnSc1	Čapek
<g/>
:	:	kIx,	:
Země	země	k1gFnSc1	země
mnoha	mnoho	k4c2	mnoho
jmen	jméno	k1gNnPc2	jméno
<g/>
,	,	kIx,	,
9	[number]	k4	9
repríz	repríza	k1gFnPc2	repríza
</s>
</p>
<p>
<s>
1927	[number]	k4	1927
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
<g/>
:	:	kIx,	:
Věc	věc	k1gFnSc1	věc
Makropulos	Makropulosa	k1gFnPc2	Makropulosa
<g/>
,	,	kIx,	,
5	[number]	k4	5
repríz	repríza	k1gFnPc2	repríza
</s>
</p>
<p>
<s>
==	==	k?	==
Instituce	instituce	k1gFnSc2	instituce
pojmenované	pojmenovaný	k2eAgFnSc2d1	pojmenovaná
po	po	k7c6	po
Karlu	Karel	k1gMnSc3	Karel
Čapkovi	Čapek	k1gMnSc3	Čapek
==	==	k?	==
</s>
</p>
<p>
<s>
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapek	k1gMnSc2	Čapek
<g/>
,	,	kIx,	,
Kodaňská	kodaňský	k2eAgFnSc1d1	Kodaňská
16	[number]	k4	16
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
10	[number]	k4	10
</s>
</p>
<p>
<s>
Gymnázium	gymnázium	k1gNnSc1	gymnázium
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapek	k1gMnSc2	Čapek
<g/>
,	,	kIx,	,
Školní	školní	k2eAgFnSc1d1	školní
1530	[number]	k4	1530
<g/>
,	,	kIx,	,
Dobříš	Dobříš	k1gFnSc1	Dobříš
</s>
</p>
<p>
<s>
Společnost	společnost	k1gFnSc1	společnost
bratří	bratr	k1gMnPc2	bratr
Čapků	Čapek	k1gMnPc2	Čapek
<g/>
,	,	kIx,	,
sdružení	sdružení	k1gNnSc1	sdružení
fyzických	fyzický	k2eAgFnPc2d1	fyzická
a	a	k8xC	a
právnických	právnický	k2eAgFnPc2d1	právnická
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
nepřetržitě	přetržitě	k6eNd1	přetržitě
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
</s>
</p>
<p>
<s>
Expres	expres	k2eAgMnSc1d1	expres
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
(	(	kIx(	(
<g/>
Mnichov	Mnichov	k1gInSc1	Mnichov
–	–	k?	–
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Mnichov	Mnichov	k1gInSc1	Mnichov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ČD	ČD	kA	ČD
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BAUER	Bauer	k1gMnSc1	Bauer
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Vojáku	voják	k1gMnSc3	voják
Vladimíre--	Vladimíre--	k1gMnSc3	Vladimíre--
<g/>
"	"	kIx"	"
:	:	kIx,	:
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
Groag	Groag	k1gMnSc1	Groag
a	a	k8xC	a
odpírači	odpírač	k1gMnPc1	odpírač
vojenské	vojenský	k2eAgFnSc2d1	vojenská
služby	služba	k1gFnSc2	služba
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
svědomí	svědomí	k1gNnSc2	svědomí
:	:	kIx,	:
vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
komunikace	komunikace	k1gFnSc1	komunikace
mezi	mezi	k7c7	mezi
K.	K.	kA	K.
Čapkem	Čapek	k1gMnSc7	Čapek
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Groagem	Groag	k1gMnSc7	Groag
a	a	k8xC	a
V.	V.	kA	V.
Rajdou	rajda	k1gFnSc7	rajda
s	s	k7c7	s
úvodním	úvodní	k2eAgInSc7d1	úvodní
komentářem	komentář	k1gInSc7	komentář
vydavatele	vydavatel	k1gMnSc2	vydavatel
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Bauer	Bauer	k1gMnSc1	Bauer
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
232	[number]	k4	232
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
904272	[number]	k4	904272
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čeští	český	k2eAgMnPc1d1	český
spisovatelé	spisovatel	k1gMnPc1	spisovatel
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Československý	československý	k2eAgMnSc1d1	československý
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
71	[number]	k4	71
<g/>
–	–	k?	–
<g/>
78	[number]	k4	78
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čeští	český	k2eAgMnPc1d1	český
spisovatelé	spisovatel	k1gMnPc1	spisovatel
literatury	literatura	k1gFnSc2	literatura
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
mládež	mládež	k1gFnSc4	mládež
/	/	kIx~	/
redakce	redakce	k1gFnSc2	redakce
Otakar	Otakar	k1gMnSc1	Otakar
Chaloupka	Chaloupka	k1gMnSc1	Chaloupka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
60	[number]	k4	60
<g/>
–	–	k?	–
<g/>
65	[number]	k4	65
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČAPEK	Čapek	k1gMnSc1	Čapek
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Divadelníkem	divadelník	k1gMnSc7	divadelník
proti	proti	k7c3	proti
své	svůj	k3xOyFgFnSc3	svůj
vůli	vůle	k1gFnSc3	vůle
:	:	kIx,	:
Recenze	recenze	k1gFnPc1	recenze
<g/>
,	,	kIx,	,
stati	stať	k1gFnPc1	stať
<g/>
,	,	kIx,	,
kresby	kresba	k1gFnPc1	kresba
<g/>
,	,	kIx,	,
fotografie	fotografia	k1gFnPc1	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Recenze	recenze	k1gFnPc1	recenze
<g/>
,	,	kIx,	,
stati	stať	k1gFnPc1	stať
<g/>
,	,	kIx,	,
kresby	kresba	k1gFnPc1	kresba
a	a	k8xC	a
fotografie	fotografia	k1gFnPc1	fotografia
o	o	k7c6	o
divadle	divadlo	k1gNnSc6	divadlo
a	a	k8xC	a
dramatu	drama	k1gNnSc6	drama
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zachycují	zachycovat	k5eAaImIp3nP	zachycovat
významné	významný	k2eAgFnPc4d1	významná
události	událost	k1gFnPc4	událost
a	a	k8xC	a
portréty	portrét	k1gInPc4	portrét
osobností	osobnost	k1gFnPc2	osobnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
<g/>
..	..	k?	..
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Literatura	literatura	k1gFnSc1	literatura
od	od	k7c2	od
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
/	/	kIx~	/
hlavní	hlavní	k2eAgMnSc1d1	hlavní
redaktor	redaktor	k1gMnSc1	redaktor
Jan	Jan	k1gMnSc1	Jan
Mukařovský	Mukařovský	k1gMnSc1	Mukařovský
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Victoria	Victorium	k1gNnSc2	Victorium
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
714	[number]	k4	714
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85865	[number]	k4	85865
<g/>
-	-	kIx~	-
<g/>
48	[number]	k4	48
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
586	[number]	k4	586
<g/>
–	–	k?	–
<g/>
605	[number]	k4	605
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FORST	FORST	kA	FORST
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Lexikon	lexikon	k1gInSc1	lexikon
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
:	:	kIx,	:
osobnosti	osobnost	k1gFnPc1	osobnost
<g/>
,	,	kIx,	,
díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
instituce	instituce	k1gFnPc1	instituce
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
<g/>
–	–	k?	–
<g/>
G.	G.	kA	G.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
900	[number]	k4	900
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
797	[number]	k4	797
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
381	[number]	k4	381
<g/>
–	–	k?	–
<g/>
386	[number]	k4	386
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HART	HART	kA	HART
<g/>
,	,	kIx,	,
Gabriel	Gabriel	k1gMnSc1	Gabriel
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Městské	městský	k2eAgNnSc1d1	Městské
divadlo	divadlo	k1gNnSc1	divadlo
na	na	k7c6	na
Král	Král	k1gMnSc1	Král
<g/>
.	.	kIx.	.
<g/>
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
<g/>
,	,	kIx,	,
Ročenka	ročenka	k1gFnSc1	ročenka
"	"	kIx"	"
<g/>
Kruhu	kruh	k1gInSc2	kruh
solistů	solist	k1gInPc2	solist
<g/>
"	"	kIx"	"
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Městské	městský	k2eAgNnSc1d1	Městské
divadlo	divadlo	k1gNnSc1	divadlo
na	na	k7c6	na
Král	Král	k1gMnSc1	Král
<g/>
.	.	kIx.	.
<g/>
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
<g/>
,	,	kIx,	,
1923	[number]	k4	1923
<g/>
.	.	kIx.	.
168	[number]	k4	168
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Kdo	kdo	k3yInSc1	kdo
byl	být	k5eAaImAgMnS	být
kdo	kdo	k3yRnSc1	kdo
v	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
dějinách	dějiny	k1gFnPc6	dějiny
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
I.	I.	kA	I.
<g/>
,	,	kIx,	,
A	A	kA	A
<g/>
–	–	k?	–
<g/>
M	M	kA	M
/	/	kIx~	/
Milan	Milan	k1gMnSc1	Milan
Churaň	Churaň	k1gMnSc1	Churaň
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
..	..	k?	..
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
467	[number]	k4	467
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85983	[number]	k4	85983
<g/>
-	-	kIx~	-
<g/>
44	[number]	k4	44
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
85	[number]	k4	85
<g/>
–	–	k?	–
<g/>
87	[number]	k4	87
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOSATÍK	KOSATÍK	kA	KOSATÍK
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
demokraté	demokrat	k1gMnPc1	demokrat
:	:	kIx,	:
50	[number]	k4	50
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
osobností	osobnost	k1gFnPc2	osobnost
veřejného	veřejný	k2eAgInSc2d1	veřejný
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
280	[number]	k4	280
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
2307	[number]	k4	2307
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MED	med	k1gInSc1	med
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Literární	literární	k2eAgInSc1d1	literární
život	život	k1gInSc1	život
ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
Mnichova	Mnichov	k1gInSc2	Mnichov
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
–	–	k?	–
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
340	[number]	k4	340
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
1823	[number]	k4	1823
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MÜLLER	MÜLLER	kA	MÜLLER
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Padesát	padesát	k4xCc1	padesát
let	léto	k1gNnPc2	léto
Městských	městský	k2eAgNnPc2d1	Městské
divadel	divadlo	k1gNnPc2	divadlo
pražských	pražský	k2eAgInPc2d1	pražský
1907	[number]	k4	1907
<g/>
–	–	k?	–
<g/>
1957	[number]	k4	1957
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ústřední	ústřední	k2eAgInSc1d1	ústřední
národní	národní	k2eAgInSc1d1	národní
výbor	výbor	k1gInSc1	výbor
hl.	hl.	k?	hl.
m.	m.	k?	m.
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
185	[number]	k4	185
s.	s.	k?	s.
</s>
</p>
<p>
<s>
SÍLOVÁ	sílový	k2eAgFnSc1d1	Sílová
<g/>
,	,	kIx,	,
Zuzana	Zuzana	k1gFnSc1	Zuzana
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
na	na	k7c6	na
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
1907	[number]	k4	1907
<g/>
–	–	k?	–
<g/>
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
Vinohradský	vinohradský	k2eAgInSc1d1	vinohradský
ansámbl	ansámbl	k1gInSc1	ansámbl
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Divadlo	divadlo	k1gNnSc1	divadlo
na	na	k7c6	na
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
212	[number]	k4	212
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
239	[number]	k4	239
<g/>
-	-	kIx~	-
<g/>
9604	[number]	k4	9604
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŠULCOVÁ	Šulcová	k1gFnSc1	Šulcová
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
<g/>
.	.	kIx.	.
</s>
<s>
Čapci	Čapek	k1gMnPc1	Čapek
<g/>
,	,	kIx,	,
Ladění	laděný	k2eAgMnPc1d1	laděný
pro	pro	k7c4	pro
dvě	dva	k4xCgFnPc4	dva
struny	struna	k1gFnPc4	struna
<g/>
,	,	kIx,	,
Poločas	poločas	k1gInSc4	poločas
nadějí	naděje	k1gFnPc2	naděje
<g/>
,	,	kIx,	,
Brána	brána	k1gFnSc1	brána
věčnosti	věčnost	k1gFnSc2	věčnost
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Melantrich	Melantrich	k1gInSc1	Melantrich
1993-98	[number]	k4	1993-98
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7023	[number]	k4	7023
<g/>
-	-	kIx~	-
<g/>
267	[number]	k4	267
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7023	[number]	k4	7023
<g/>
-	-	kIx~	-
<g/>
279	[number]	k4	279
<g/>
-X	-X	k?	-X
<g/>
,	,	kIx,	,
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7023	[number]	k4	7023
<g/>
-	-	kIx~	-
<g/>
165	[number]	k4	165
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
80-7023--245-5	[number]	k4	80-7023--245-5
</s>
</p>
<p>
<s>
ŠULCOVÁ	Šulcová	k1gFnSc1	Šulcová
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
<g/>
.	.	kIx.	.
</s>
<s>
Prodloužený	prodloužený	k2eAgInSc1d1	prodloužený
čas	čas	k1gInSc1	čas
Josefa	Josef	k1gMnSc2	Josef
Čapka	Čapek	k1gMnSc2	Čapek
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Paseka	paseka	k1gFnSc1	paseka
2000	[number]	k4	2000
ISBN	ISBN	kA	ISBN
80-7185-332-1	[number]	k4	80-7185-332-1
</s>
</p>
<p>
<s>
TOMEŠ	Tomeš	k1gMnSc1	Tomeš
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
XX	XX	kA	XX
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
:	:	kIx,	:
I.	I.	kA	I.
díl	díl	k1gInSc1	díl
:	:	kIx,	:
A	a	k9	a
<g/>
–	–	k?	–
<g/>
J.	J.	kA	J.
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
;	;	kIx,	;
Petr	Petr	k1gMnSc1	Petr
Meissner	Meissner	k1gMnSc1	Meissner
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
634	[number]	k4	634
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
245	[number]	k4	245
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
183	[number]	k4	183
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VOŠAHLÍKOVÁ	Vošahlíková	k1gFnSc1	Vošahlíková
<g/>
,	,	kIx,	,
Pavla	Pavla	k1gFnSc1	Pavla
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
:	:	kIx,	:
10	[number]	k4	10
<g/>
.	.	kIx.	.
sešit	sešit	k1gInSc1	sešit
:	:	kIx,	:
Č	Č	kA	Č
<g/>
–	–	k?	–
<g/>
Čerma	Čerma	k1gFnSc1	Čerma
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
503	[number]	k4	503
<g/>
–	–	k?	–
<g/>
606	[number]	k4	606
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
367	[number]	k4	367
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
529	[number]	k4	529
<g/>
–	–	k?	–
<g/>
532	[number]	k4	532
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŽÁK	Žák	k1gMnSc1	Žák
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
na	na	k7c6	na
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
1907	[number]	k4	1907
<g/>
–	–	k?	–
<g/>
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
Vinohradský	vinohradský	k2eAgInSc1d1	vinohradský
příběh	příběh	k1gInSc1	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Divadlo	divadlo	k1gNnSc1	divadlo
na	na	k7c6	na
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
215	[number]	k4	215
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
239	[number]	k4	239
<g/>
-	-	kIx~	-
<g/>
9603	[number]	k4	9603
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
českých	český	k2eAgMnPc2d1	český
spisovatelů	spisovatel	k1gMnPc2	spisovatel
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
literatura	literatura	k1gFnSc1	literatura
v	v	k7c6	v
letech	let	k1gInPc6	let
1900	[number]	k4	1900
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
</s>
</p>
<p>
<s>
Expresionismus	expresionismus	k1gInSc1	expresionismus
</s>
</p>
<p>
<s>
Pragmatismus	pragmatismus	k1gInSc1	pragmatismus
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Čapek	Čapek	k1gMnSc1	Čapek
</s>
</p>
<p>
<s>
Bratři	bratr	k1gMnPc1	bratr
Čapkové	Čapková	k1gFnSc2	Čapková
</s>
</p>
<p>
<s>
Člověk	člověk	k1gMnSc1	člověk
proti	proti	k7c3	proti
zkáze	zkáza	k1gFnSc3	zkáza
<g/>
,	,	kIx,	,
životopisný	životopisný	k2eAgInSc1d1	životopisný
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnSc1	kategorie
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Karel	Karel	k1gMnSc1	Karel
Čapek	čapka	k1gFnPc2	čapka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Plné	plný	k2eAgInPc4d1	plný
texty	text	k1gInPc4	text
děl	dít	k5eAaBmAgMnS	dít
autora	autor	k1gMnSc2	autor
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
Gutenberg	Gutenberg	k1gMnSc1	Gutenberg
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
</s>
</p>
<p>
<s>
Kompletní	kompletní	k2eAgNnSc1d1	kompletní
dílo	dílo	k1gNnSc1	dílo
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapek	k1gMnSc2	Čapek
v	v	k7c6	v
elektronické	elektronický	k2eAgFnSc6d1	elektronická
podobě	podoba	k1gFnSc6	podoba
(	(	kIx(	(
<g/>
ePub	ePub	k1gInSc1	ePub
<g/>
,	,	kIx,	,
PDF	PDF	kA	PDF
<g/>
,	,	kIx,	,
HTML	HTML	kA	HTML
<g/>
,	,	kIx,	,
RTF	RTF	kA	RTF
...	...	k?	...
<g/>
)	)	kIx)	)
zdarma	zdarma	k6eAd1	zdarma
na	na	k7c6	na
webu	web	k1gInSc6	web
Městské	městský	k2eAgFnSc2d1	městská
knihovny	knihovna	k1gFnSc2	knihovna
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
na	na	k7c6	na
Čapek	Čapek	k1gMnSc1	Čapek
<g/>
.	.	kIx.	.
<g/>
misto	misto	k1gNnSc1	misto
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Památník	památník	k1gInSc1	památník
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapek	k1gMnSc2	Čapek
</s>
</p>
<p>
<s>
Životopis	životopis	k1gInSc1	životopis
a	a	k8xC	a
dílo	dílo	k1gNnSc1	dílo
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Gymnázia	gymnázium	k1gNnSc2	gymnázium
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapek	k1gMnSc2	Čapek
v	v	k7c6	v
Dobříši	Dobříš	k1gFnSc6	Dobříš
</s>
</p>
<p>
<s>
Společnost	společnost	k1gFnSc1	společnost
bratří	bratr	k1gMnPc2	bratr
Čapků	Čapek	k1gMnPc2	Čapek
</s>
</p>
<p>
<s>
Rozhlasové	rozhlasový	k2eAgNnSc1d1	rozhlasové
zpracování	zpracování	k1gNnSc1	zpracování
několika	několik	k4yIc2	několik
děl	dělo	k1gNnPc2	dělo
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapek	k1gMnSc2	Čapek
ve	v	k7c6	v
zvukovém	zvukový	k2eAgInSc6d1	zvukový
formátu	formát	k1gInSc6	formát
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
na	na	k7c6	na
webových	webový	k2eAgFnPc6d1	webová
stránkách	stránka	k1gFnPc6	stránka
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
</s>
</p>
<p>
<s>
Podrobný	podrobný	k2eAgInSc1d1	podrobný
životopis	životopis	k1gInSc1	životopis
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapek	k1gMnSc2	Čapek
na	na	k7c6	na
webu	web	k1gInSc6	web
provozovaném	provozovaný	k2eAgInSc6d1	provozovaný
PIS	Pisa	k1gFnPc2	Pisa
</s>
</p>
<p>
<s>
Nedělní	nedělní	k2eAgInPc1d1	nedělní
fejetony	fejeton	k1gInPc1	fejeton
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapek	k1gMnSc2	Čapek
v	v	k7c6	v
Lidových	lidový	k2eAgFnPc6d1	lidová
novinách	novina	k1gFnPc6	novina
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Čapkův	Čapkův	k2eAgInSc1d1	Čapkův
hlas	hlas	k1gInSc1	hlas
v	v	k7c6	v
Archivu	archiv	k1gInSc6	archiv
rozhlasu	rozhlas	k1gInSc2	rozhlas
</s>
</p>
