<s>
Ikarie	Ikarie	k1gFnSc1	Ikarie
XB	XB	kA	XB
1	[number]	k4	1
je	být	k5eAaImIp3nS	být
první	první	k4xOgInSc4	první
a	a	k8xC	a
jediný	jediný	k2eAgInSc4d1	jediný
snímek	snímek	k1gInSc4	snímek
československé	československý	k2eAgFnSc2d1	Československá
kinematografie	kinematografie	k1gFnSc2	kinematografie
v	v	k7c6	v
žánru	žánr	k1gInSc6	žánr
klasické	klasický	k2eAgFnPc1d1	klasická
(	(	kIx(	(
<g/>
hard	hard	k1gInSc1	hard
<g/>
)	)	kIx)	)
science	science	k1gFnSc1	science
fiction	fiction	k1gInSc1	fiction
<g/>
.	.	kIx.	.
</s>
<s>
Režisér	režisér	k1gMnSc1	režisér
Jindřich	Jindřich	k1gMnSc1	Jindřich
Polák	Polák	k1gMnSc1	Polák
natočil	natočit	k5eAaBmAgMnS	natočit
snímek	snímek	k1gInSc4	snímek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
podle	podle	k7c2	podle
románu	román	k1gInSc2	román
K	k	k7c3	k
Mrakům	mrak	k1gInPc3	mrak
Magellanovým	Magellanův	k2eAgInPc3d1	Magellanův
polského	polský	k2eAgMnSc2d1	polský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Stanisława	Stanisławus	k1gMnSc2	Stanisławus
Lema	Lemus	k1gMnSc2	Lemus
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2163	[number]	k4	2163
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
hvězdolet	hvězdolet	k1gInSc1	hvězdolet
Ikarie	Ikarie	k1gFnSc2	Ikarie
XB	XB	kA	XB
1	[number]	k4	1
s	s	k7c7	s
početnou	početný	k2eAgFnSc7d1	početná
vědeckou	vědecký	k2eAgFnSc7d1	vědecká
posádkou	posádka	k1gFnSc7	posádka
vydává	vydávat	k5eAaImIp3nS	vydávat
hledat	hledat	k5eAaImF	hledat
život	život	k1gInSc4	život
na	na	k7c4	na
planety	planeta	k1gFnPc4	planeta
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
Alfa	alfa	k1gFnSc1	alfa
Centauri	Centaur	k1gFnSc2	Centaur
<g/>
.	.	kIx.	.
</s>
<s>
Snímek	snímek	k1gInSc1	snímek
získal	získat	k5eAaPmAgInS	získat
ocenění	ocenění	k1gNnSc4	ocenění
na	na	k7c6	na
mezinárodních	mezinárodní	k2eAgInPc6d1	mezinárodní
filmových	filmový	k2eAgInPc6d1	filmový
festivalech	festival	k1gInPc6	festival
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
také	také	k6eAd1	také
uváděn	uvádět	k5eAaImNgInS	uvádět
s	s	k7c7	s
anglickým	anglický	k2eAgInSc7d1	anglický
dabingem	dabing	k1gInSc7	dabing
a	a	k8xC	a
upraveným	upravený	k2eAgInSc7d1	upravený
závěrem	závěr	k1gInSc7	závěr
v	v	k7c6	v
kinech	kino	k1gNnPc6	kino
USA	USA	kA	USA
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Voyage	Voyag	k1gInSc2	Voyag
to	ten	k3xDgNnSc1	ten
the	the	k?	the
End	End	k1gMnSc3	End
of	of	k?	of
the	the	k?	the
Universe	Universe	k1gFnSc2	Universe
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgMnPc1d1	hlavní
postavy	postava	k1gFnSc2	postava
ztvárnili	ztvárnit	k5eAaPmAgMnP	ztvárnit
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Štěpánek	Štěpánek	k1gMnSc1	Štěpánek
<g/>
,	,	kIx,	,
Radovan	Radovan	k1gMnSc1	Radovan
Lukavský	Lukavský	k2eAgMnSc1d1	Lukavský
<g/>
,	,	kIx,	,
Dana	Dana	k1gFnSc1	Dana
Medřická	Medřická	k1gFnSc1	Medřická
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Macháček	Macháček	k1gMnSc1	Macháček
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Píše	psát	k5eAaImIp3nS	psát
se	se	k3xPyFc4	se
rok	rok	k1gInSc4	rok
2163	[number]	k4	2163
a	a	k8xC	a
po	po	k7c6	po
osmi	osm	k4xCc6	osm
letech	léto	k1gNnPc6	léto
příprav	příprava	k1gFnPc2	příprava
se	se	k3xPyFc4	se
kosmická	kosmický	k2eAgFnSc1d1	kosmická
loď	loď	k1gFnSc1	loď
Ikarie	Ikarie	k1gFnSc2	Ikarie
XB	XB	kA	XB
1	[number]	k4	1
se	s	k7c7	s
40	[number]	k4	40
vědci	vědec	k1gMnPc7	vědec
a	a	k8xC	a
astronauty	astronaut	k1gMnPc7	astronaut
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
vydává	vydávat	k5eAaPmIp3nS	vydávat
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
cestu	cesta	k1gFnSc4	cesta
za	za	k7c7	za
hledáním	hledání	k1gNnSc7	hledání
života	život	k1gInSc2	život
na	na	k7c6	na
planetách	planeta	k1gFnPc6	planeta
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
Alfa	alfa	k1gFnSc1	alfa
Centauri	Centaur	k1gFnSc2	Centaur
<g/>
.	.	kIx.	.
</s>
<s>
Seznamujeme	seznamovat	k5eAaImIp1nP	seznamovat
se	se	k3xPyFc4	se
s	s	k7c7	s
velitelem	velitel	k1gMnSc7	velitel
lodi	loď	k1gFnSc2	loď
Abajevem	Abajev	k1gInSc7	Abajev
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc7	jeho
zástupcem	zástupce	k1gMnSc7	zástupce
MacDonaldem	Macdonald	k1gMnSc7	Macdonald
<g/>
,	,	kIx,	,
vědcem	vědec	k1gMnSc7	vědec
Anthonym	Anthonymum	k1gNnPc2	Anthonymum
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
zastaralým	zastaralý	k2eAgInSc7d1	zastaralý
robotem	robot	k1gInSc7	robot
a	a	k8xC	a
ostatními	ostatní	k2eAgMnPc7d1	ostatní
členy	člen	k1gMnPc7	člen
posádky	posádka	k1gFnSc2	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čtyřech	čtyři	k4xCgInPc6	čtyři
měsících	měsíc	k1gInPc6	měsíc
cesty	cesta	k1gFnSc2	cesta
slaví	slavit	k5eAaImIp3nS	slavit
posádka	posádka	k1gFnSc1	posádka
Anthonyho	Anthony	k1gMnSc2	Anthony
narozeniny	narozeniny	k1gFnPc4	narozeniny
<g/>
.	.	kIx.	.
</s>
<s>
Scéna	scéna	k1gFnSc1	scéna
oslavy	oslava	k1gFnSc2	oslava
byla	být	k5eAaImAgFnS	být
tvůrci	tvůrce	k1gMnPc7	tvůrce
pojata	pojmout	k5eAaPmNgNnP	pojmout
velmi	velmi	k6eAd1	velmi
futuristicky	futuristicky	k6eAd1	futuristicky
<g/>
,	,	kIx,	,
posádka	posádka	k1gFnSc1	posádka
předvádí	předvádět	k5eAaImIp3nS	předvádět
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
bude	být	k5eAaImBp3nS	být
vypadat	vypadat	k5eAaImF	vypadat
tanec	tanec	k1gInSc4	tanec
ve	v	k7c4	v
22	[number]	k4	22
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Oslava	oslava	k1gFnSc1	oslava
je	být	k5eAaImIp3nS	být
však	však	k9	však
přerušena	přerušit	k5eAaPmNgFnS	přerušit
poplachem	poplach	k1gInSc7	poplach
<g/>
.	.	kIx.	.
</s>
<s>
Automatický	automatický	k2eAgInSc1d1	automatický
řídící	řídící	k2eAgInSc1d1	řídící
systém	systém	k1gInSc1	systém
lodi	loď	k1gFnSc2	loď
hlásí	hlásit	k5eAaImIp3nS	hlásit
neznámé	známý	k2eNgNnSc4d1	neznámé
těleso	těleso	k1gNnSc4	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Posádka	posádka	k1gFnSc1	posádka
cizí	cizí	k2eAgNnSc4d1	cizí
těleso	těleso	k1gNnSc4	těleso
analyzuje	analyzovat	k5eAaImIp3nS	analyzovat
a	a	k8xC	a
po	po	k7c6	po
přiblížení	přiblížení	k1gNnSc6	přiblížení
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
že	že	k8xS	že
se	se	k3xPyFc4	se
nepochybně	pochybně	k6eNd1	pochybně
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
kosmické	kosmický	k2eAgNnSc4d1	kosmické
plavidlo	plavidlo	k1gNnSc4	plavidlo
talířového	talířový	k2eAgInSc2d1	talířový
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Velitel	velitel	k1gMnSc1	velitel
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
vyslat	vyslat	k5eAaPmF	vyslat
expediční	expediční	k2eAgFnSc4d1	expediční
raketu	raketa	k1gFnSc4	raketa
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
průzkumníky	průzkumník	k1gMnPc7	průzkumník
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
průzkumníci	průzkumník	k1gMnPc1	průzkumník
dostanou	dostat	k5eAaPmIp3nP	dostat
na	na	k7c4	na
palubu	paluba	k1gFnSc4	paluba
cizí	cizí	k2eAgFnSc2d1	cizí
lodi	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
plavidlo	plavidlo	k1gNnSc4	plavidlo
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
z	z	k7c2	z
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
lodi	loď	k1gFnSc2	loď
naleznou	nalézt	k5eAaBmIp3nP	nalézt
mrtvé	mrtvý	k2eAgMnPc4d1	mrtvý
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
smrt	smrt	k1gFnSc4	smrt
je	být	k5eAaImIp3nS	být
zastihla	zastihnout	k5eAaPmAgFnS	zastihnout
při	při	k7c6	při
hraní	hraní	k1gNnSc6	hraní
karet	kareta	k1gFnPc2	kareta
o	o	k7c4	o
peníze	peníz	k1gInPc4	peníz
<g/>
.	.	kIx.	.
</s>
<s>
Průzkumníci	průzkumník	k1gMnPc1	průzkumník
zjistí	zjistit	k5eAaPmIp3nP	zjistit
dále	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
že	že	k8xS	že
posádka	posádka	k1gFnSc1	posádka
byla	být	k5eAaImAgFnS	být
zabita	zabít	k5eAaPmNgFnS	zabít
smrtícím	smrtící	k2eAgInSc7d1	smrtící
plynem	plyn	k1gInSc7	plyn
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
vypustil	vypustit	k5eAaPmAgMnS	vypustit
velitel	velitel	k1gMnSc1	velitel
lodi	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
když	když	k8xS	když
docházel	docházet	k5eAaImAgInS	docházet
kyslík	kyslík	k1gInSc1	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dalším	další	k2eAgInSc6d1	další
průzkumu	průzkum	k1gInSc6	průzkum
loď	loď	k1gFnSc4	loď
automaticky	automaticky	k6eAd1	automaticky
aktivuje	aktivovat	k5eAaBmIp3nS	aktivovat
jaderné	jaderný	k2eAgFnPc4d1	jaderná
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
<g/>
,	,	kIx,	,
a	a	k8xC	a
vzápětí	vzápětí	k6eAd1	vzápětí
exploduje	explodovat	k5eAaBmIp3nS	explodovat
<g/>
;	;	kIx,	;
oba	dva	k4xCgMnPc1	dva
průzkumníci	průzkumník	k1gMnPc1	průzkumník
zahynou	zahynout	k5eAaPmIp3nP	zahynout
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
děje	děj	k1gInSc2	děj
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
protiválečně	protiválečně	k6eAd1	protiválečně
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
kritiků	kritik	k1gMnPc2	kritik
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
poplatná	poplatný	k2eAgFnSc1d1	poplatná
době	doba	k1gFnSc3	doba
a	a	k8xC	a
režimu	režim	k1gInSc3	režim
<g/>
,	,	kIx,	,
za	za	k7c2	za
jakého	jaký	k3yIgNnSc2	jaký
film	film	k1gInSc1	film
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
<g/>
.	.	kIx.	.
</s>
<s>
Kapitán	kapitán	k1gMnSc1	kapitán
pověří	pověřit	k5eAaPmIp3nS	pověřit
dva	dva	k4xCgInPc4	dva
členy	člen	k1gInPc4	člen
posádky	posádka	k1gFnSc2	posádka
(	(	kIx(	(
<g/>
Svenson	Svenson	k1gMnSc1	Svenson
a	a	k8xC	a
Michal	Michal	k1gMnSc1	Michal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
provedli	provést	k5eAaPmAgMnP	provést
usazení	usazení	k1gNnSc4	usazení
nového	nový	k2eAgInSc2d1	nový
expedičního	expediční	k2eAgInSc2d1	expediční
modulu	modul	k1gInSc2	modul
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Ikarie	Ikarie	k1gFnSc2	Ikarie
<g/>
.	.	kIx.	.
</s>
<s>
Zanedlouho	zanedlouho	k6eAd1	zanedlouho
se	se	k3xPyFc4	se
u	u	k7c2	u
Svensona	Svenson	k1gMnSc2	Svenson
a	a	k8xC	a
Michala	Michal	k1gMnSc2	Michal
začnou	začít	k5eAaPmIp3nP	začít
projevovat	projevovat	k5eAaImF	projevovat
příznaky	příznak	k1gInPc4	příznak
záhadné	záhadný	k2eAgFnSc2d1	záhadná
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
během	během	k7c2	během
práce	práce	k1gFnSc2	práce
upadají	upadat	k5eAaPmIp3nP	upadat
do	do	k7c2	do
spánku	spánek	k1gInSc2	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
posádka	posádka	k1gFnSc1	posádka
probere	probrat	k5eAaPmIp3nS	probrat
<g/>
,	,	kIx,	,
zdají	zdát	k5eAaPmIp3nP	zdát
se	se	k3xPyFc4	se
v	v	k7c6	v
pořádku	pořádek	k1gInSc6	pořádek
<g/>
.	.	kIx.	.
</s>
<s>
Nemoc	nemoc	k1gFnSc1	nemoc
se	se	k3xPyFc4	se
však	však	k9	však
rychle	rychle	k6eAd1	rychle
rozšíří	rozšířit	k5eAaPmIp3nS	rozšířit
mezi	mezi	k7c4	mezi
celou	celý	k2eAgFnSc4d1	celá
posádku	posádka	k1gFnSc4	posádka
<g/>
,	,	kIx,	,
všichni	všechen	k3xTgMnPc1	všechen
jsou	být	k5eAaImIp3nP	být
náhle	náhle	k6eAd1	náhle
velmi	velmi	k6eAd1	velmi
vyčerpaní	vyčerpaný	k2eAgMnPc1d1	vyčerpaný
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
ztrácejí	ztrácet	k5eAaImIp3nP	ztrácet
síly	síla	k1gFnPc1	síla
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
nemoci	nemoc	k1gFnSc2	nemoc
je	být	k5eAaImIp3nS	být
zjištěn	zjistit	k5eAaPmNgInS	zjistit
<g/>
,	,	kIx,	,
když	když	k8xS	když
Anthony	Anthona	k1gFnPc1	Anthona
zjistí	zjistit	k5eAaPmIp3nP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ikarie	Ikarie	k1gFnSc1	Ikarie
prolétá	prolétat	k5eAaPmIp3nS	prolétat
kolem	kolem	k7c2	kolem
záhadné	záhadný	k2eAgFnSc2d1	záhadná
tmavé	tmavý	k2eAgFnSc2d1	tmavá
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
záření	záření	k1gNnSc1	záření
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
útlum	útlum	k1gInSc4	útlum
biochemických	biochemický	k2eAgFnPc2d1	biochemická
reakcí	reakce	k1gFnPc2	reakce
v	v	k7c6	v
živých	živý	k2eAgInPc6d1	živý
organismech	organismus	k1gInPc6	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Posádka	posádka	k1gFnSc1	posádka
postupně	postupně	k6eAd1	postupně
usíná	usínat	k5eAaImIp3nS	usínat
<g/>
,	,	kIx,	,
působení	působení	k1gNnSc1	působení
tmavé	tmavý	k2eAgFnSc2d1	tmavá
hvězdy	hvězda	k1gFnSc2	hvězda
má	mít	k5eAaImIp3nS	mít
trvat	trvat	k5eAaImF	trvat
60	[number]	k4	60
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
MacDonald	Macdonald	k1gMnSc1	Macdonald
nevěří	věřit	k5eNaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
posádka	posádka	k1gFnSc1	posádka
opět	opět	k6eAd1	opět
probudí	probudit	k5eAaPmIp3nS	probudit
<g/>
,	,	kIx,	,
a	a	k8xC	a
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
Ikarii	Ikarie	k1gFnSc3	Ikarie
obrátit	obrátit	k5eAaPmF	obrátit
zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
Abajev	Abajet	k5eAaPmDgInS	Abajet
jej	on	k3xPp3gMnSc4	on
na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
chvíli	chvíle	k1gFnSc6	chvíle
zadrží	zadržet	k5eAaPmIp3nP	zadržet
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
upadají	upadat	k5eAaImIp3nP	upadat
do	do	k7c2	do
tvrdého	tvrdý	k2eAgInSc2d1	tvrdý
spánku	spánek	k1gInSc2	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
v	v	k7c6	v
původním	původní	k2eAgInSc6d1	původní
kurzu	kurz	k1gInSc6	kurz
se	s	k7c7	s
spící	spící	k2eAgFnSc7d1	spící
posádkou	posádka	k1gFnSc7	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
náhle	náhle	k6eAd1	náhle
probouzí	probouzet	k5eAaImIp3nS	probouzet
<g/>
.	.	kIx.	.
</s>
<s>
Všechno	všechen	k3xTgNnSc1	všechen
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
v	v	k7c6	v
pořádku	pořádek	k1gInSc6	pořádek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
posádka	posádka	k1gFnSc1	posádka
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
spala	spát	k5eAaImAgFnS	spát
pouze	pouze	k6eAd1	pouze
25	[number]	k4	25
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Anthony	Anthon	k1gInPc4	Anthon
pozoruje	pozorovat	k5eAaImIp3nS	pozorovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c7	mezi
Ikarií	Ikarie	k1gFnSc7	Ikarie
a	a	k8xC	a
tmavou	tmavý	k2eAgFnSc7d1	tmavá
hvězdou	hvězda	k1gFnSc7	hvězda
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
silové	silový	k2eAgNnSc1d1	silové
pole	pole	k1gNnSc1	pole
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zadržuje	zadržovat	k5eAaImIp3nS	zadržovat
záření	záření	k1gNnSc1	záření
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Svensona	Svenson	k1gMnSc2	Svenson
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Michalem	Michal	k1gMnSc7	Michal
vystaven	vystavit	k5eAaPmNgMnS	vystavit
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
plavidla	plavidlo	k1gNnSc2	plavidlo
přímému	přímý	k2eAgNnSc3d1	přímé
záření	záření	k1gNnSc3	záření
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
těžká	těžký	k2eAgFnSc1d1	těžká
nemoc	nemoc	k1gFnSc1	nemoc
z	z	k7c2	z
ozáření	ozáření	k1gNnSc2	ozáření
<g/>
.	.	kIx.	.
</s>
<s>
Michal	Michal	k1gMnSc1	Michal
zešílí	zešílet	k5eAaPmIp3nS	zešílet
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
se	se	k3xPyFc4	se
vrátit	vrátit	k5eAaPmF	vrátit
zpátky	zpátky	k6eAd1	zpátky
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Uzavře	uzavřít	k5eAaPmIp3nS	uzavřít
se	se	k3xPyFc4	se
v	v	k7c6	v
části	část	k1gFnSc6	část
paluby	paluba	k1gFnSc2	paluba
<g/>
,	,	kIx,	,
poškodí	poškodit	k5eAaPmIp3nS	poškodit
loď	loď	k1gFnSc1	loď
a	a	k8xC	a
nekomunikuje	komunikovat	k5eNaImIp3nS	komunikovat
s	s	k7c7	s
posádkou	posádka	k1gFnSc7	posádka
<g/>
.	.	kIx.	.
</s>
<s>
MacDonald	Macdonald	k1gMnSc1	Macdonald
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
dostane	dostat	k5eAaPmIp3nS	dostat
větracími	větrací	k2eAgFnPc7d1	větrací
šachtami	šachta	k1gFnPc7	šachta
a	a	k8xC	a
přesvědčí	přesvědčit	k5eAaPmIp3nS	přesvědčit
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
k	k	k7c3	k
posádce	posádka	k1gFnSc3	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Lodní	lodní	k2eAgMnSc1d1	lodní
lékař	lékař	k1gMnSc1	lékař
oba	dva	k4xCgMnPc4	dva
muže	muž	k1gMnPc4	muž
zachrání	zachránit	k5eAaPmIp3nS	zachránit
<g/>
.	.	kIx.	.
</s>
<s>
Silové	silový	k2eAgNnSc1d1	silové
pole	pole	k1gNnSc1	pole
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ikarií	Ikarie	k1gFnSc7	Ikarie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dolétá	dolétat	k5eAaImIp3nS	dolétat
k	k	k7c3	k
bílé	bílý	k2eAgFnSc3d1	bílá
planetě	planeta	k1gFnSc3	planeta
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgMnPc4	který
posádka	posádka	k1gFnSc1	posádka
objevuje	objevovat	k5eAaImIp3nS	objevovat
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
scéně	scéna	k1gFnSc6	scéna
filmu	film	k1gInSc2	film
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
skrz	skrz	k7c4	skrz
mraky	mrak	k1gInPc4	mrak
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
hustě	hustě	k6eAd1	hustě
osídlený	osídlený	k2eAgInSc4d1	osídlený
a	a	k8xC	a
industrializovaný	industrializovaný	k2eAgInSc4d1	industrializovaný
povrch	povrch	k1gInSc4	povrch
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
verzi	verze	k1gFnSc6	verze
filmu	film	k1gInSc2	film
se	se	k3xPyFc4	se
místo	místo	k7c2	místo
těchto	tento	k3xDgInPc2	tento
záběrů	záběr	k1gInPc2	záběr
objevuje	objevovat	k5eAaImIp3nS	objevovat
letecký	letecký	k2eAgInSc1d1	letecký
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
jižní	jižní	k2eAgInSc4d1	jižní
Manhattan	Manhattan	k1gInSc4	Manhattan
a	a	k8xC	a
Sochu	socha	k1gFnSc4	socha
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
1963	[number]	k4	1963
–	–	k?	–
velká	velký	k2eAgFnSc1d1	velká
cena	cena	k1gFnSc1	cena
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
kosmická	kosmický	k2eAgFnSc1d1	kosmická
loď	loď	k1gFnSc1	loď
(	(	kIx(	(
<g/>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
festival	festival	k1gInSc1	festival
vědecko-fantastických	vědeckoantastický	k2eAgInPc2d1	vědecko-fantastický
filmů	film	k1gInPc2	film
Terst	Terst	k1gInSc1	Terst
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2016	[number]	k4	2016
byl	být	k5eAaImAgInS	být
film	film	k1gInSc1	film
uveden	uvést	k5eAaPmNgInS	uvést
znovu	znovu	k6eAd1	znovu
do	do	k7c2	do
distribuce	distribuce	k1gFnSc2	distribuce
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
byl	být	k5eAaImAgMnS	být
podroben	podroben	k2eAgInSc4d1	podroben
digitální	digitální	k2eAgFnSc4d1	digitální
restauraci	restaurace	k1gFnSc4	restaurace
<g/>
.	.	kIx.	.
</s>
