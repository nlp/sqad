<s>
Fort	Fort	k?	Fort
Lauderdale	Lauderdala	k1gFnSc3	Lauderdala
[	[	kIx(	[
<g/>
Fɔ	Fɔ	k1gFnSc1	Fɔ
Lɔ	Lɔ	k1gFnSc2	Lɔ
<g/>
̩	̩	k?	̩
<g/>
dɛ	dɛ	k?	dɛ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
státě	stát	k1gInSc6	stát
Florida	Florida	k1gFnSc1	Florida
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
sídlo	sídlo	k1gNnSc1	sídlo
okresu	okres	k1gInSc2	okres
Broward	Browarda	k1gFnPc2	Browarda
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
Amerického	americký	k2eAgInSc2d1	americký
statistického	statistický	k2eAgInSc2d1	statistický
úřadu	úřad	k1gInSc2	úřad
zde	zde	k6eAd1	zde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
žilo	žít	k5eAaImAgNnS	žít
183	[number]	k4	183
606	[number]	k4	606
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Fort	Fort	k?	Fort
Lauderdale	Lauderdala	k1gFnSc6	Lauderdala
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
oblasti	oblast	k1gFnSc2	oblast
Jižní	jižní	k2eAgFnSc1d1	jižní
Florida	Florida	k1gFnSc1	Florida
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žije	žít	k5eAaImIp3nS	žít
5	[number]	k4	5
413	[number]	k4	413
212	[number]	k4	212
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
oblíbenou	oblíbený	k2eAgFnSc4d1	oblíbená
turistickou	turistický	k2eAgFnSc4d1	turistická
destinaci	destinace	k1gFnSc4	destinace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
jej	on	k3xPp3gMnSc4	on
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
10,35	[number]	k4	10,35
milionu	milion	k4xCgInSc2	milion
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
a	a	k8xC	a
složitý	složitý	k2eAgInSc4d1	složitý
systém	systém	k1gInSc4	systém
vodních	vodní	k2eAgInPc2d1	vodní
kanálů	kanál	k1gInPc2	kanál
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
americké	americký	k2eAgFnPc1d1	americká
Benátky	Benátky	k1gFnPc1	Benátky
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
K	k	k7c3	k
městu	město	k1gNnSc3	město
přiléhá	přiléhat	k5eAaImIp3nS	přiléhat
přes	přes	k7c4	přes
10	[number]	k4	10
kilometrů	kilometr	k1gInPc2	kilometr
pláží	pláž	k1gFnPc2	pláž
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
jachtařským	jachtařský	k2eAgNnSc7d1	jachtařské
centrem	centrum	k1gNnSc7	centrum
(	(	kIx(	(
<g/>
ve	v	k7c6	v
100	[number]	k4	100
přístavech	přístav	k1gInPc6	přístav
a	a	k8xC	a
loděnicích	loděnice	k1gFnPc6	loděnice
zde	zde	k6eAd1	zde
kotví	kotvit	k5eAaImIp3nS	kotvit
na	na	k7c4	na
42	[number]	k4	42
tisíc	tisíc	k4xCgInSc4	tisíc
jachet	jachta	k1gFnPc2	jachta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fort	Fort	k?	Fort
Lauderdale	Lauderdala	k1gFnSc3	Lauderdala
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
zhruba	zhruba	k6eAd1	zhruba
37	[number]	k4	37
kilometrů	kilometr	k1gInPc2	kilometr
severně	severně	k6eAd1	severně
od	od	k7c2	od
Miami	Miami	k1gNnSc2	Miami
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc6	jeho
okolí	okolí	k1gNnSc6	okolí
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
přes	přes	k7c4	přes
4100	[number]	k4	4100
restaurací	restaurace	k1gFnPc2	restaurace
a	a	k8xC	a
120	[number]	k4	120
nočních	noční	k2eAgInPc2d1	noční
klubů	klub	k1gInPc2	klub
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
po	po	k7c6	po
sérii	série	k1gFnSc6	série
pevností	pevnost	k1gFnPc2	pevnost
postavených	postavený	k2eAgFnPc2d1	postavená
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
seminolské	seminolský	k2eAgFnSc2d1	seminolský
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
byly	být	k5eAaImAgFnP	být
pojmenovány	pojmenovat	k5eAaPmNgFnP	pojmenovat
po	po	k7c6	po
majoru	major	k1gMnSc3	major
Williamu	William	k1gInSc2	William
Lauderdalovi	Lauderdal	k1gMnSc3	Lauderdal
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
velitelem	velitel	k1gMnSc7	velitel
výsadku	výsadek	k1gInSc2	výsadek
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
postavili	postavit	k5eAaPmAgMnP	postavit
první	první	k4xOgFnSc4	první
pevnost	pevnost	k1gFnSc4	pevnost
<g/>
.	.	kIx.	.
</s>
<s>
Letní	letní	k2eAgMnPc1d1	letní
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
mokré	mokrý	k2eAgFnSc2d1	mokrá
<g/>
"	"	kIx"	"
sezóny	sezóna	k1gFnSc2	sezóna
od	od	k7c2	od
května	květen	k1gInSc2	květen
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
jsou	být	k5eAaImIp3nP	být
horké	horký	k2eAgInPc1d1	horký
<g/>
,	,	kIx,	,
vlhké	vlhký	k2eAgInPc1d1	vlhký
a	a	k8xC	a
deštivé	deštivý	k2eAgInPc1d1	deštivý
s	s	k7c7	s
maximálními	maximální	k2eAgFnPc7d1	maximální
teplotami	teplota	k1gFnPc7	teplota
30	[number]	k4	30
-	-	kIx~	-
32	[number]	k4	32
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
minimálními	minimální	k2eAgFnPc7d1	minimální
teplotami	teplota	k1gFnPc7	teplota
22	[number]	k4	22
-	-	kIx~	-
24	[number]	k4	24
°	°	k?	°
<g/>
C.	C.	kA	C.
Tato	tento	k3xDgFnSc1	tento
sezóna	sezóna	k1gFnSc1	sezóna
přináší	přinášet	k5eAaImIp3nS	přinášet
také	také	k9	také
časté	častý	k2eAgFnPc4d1	častá
odpolední	odpolední	k2eAgFnPc4d1	odpolední
či	či	k8xC	či
večerní	večerní	k2eAgFnPc4d1	večerní
bouře	bouř	k1gFnPc4	bouř
<g/>
.	.	kIx.	.
</s>
<s>
Zimní	zimní	k2eAgFnSc1d1	zimní
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
suché	suchý	k2eAgFnSc2d1	suchá
<g/>
"	"	kIx"	"
sezóny	sezóna	k1gFnSc2	sezóna
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
do	do	k7c2	do
dubna	duben	k1gInSc2	duben
jsou	být	k5eAaImIp3nP	být
teplé	teplý	k2eAgInPc1d1	teplý
a	a	k8xC	a
většinou	většinou	k6eAd1	většinou
bezdeštné	bezdeštný	k2eAgFnPc1d1	bezdeštný
<g/>
,	,	kIx,	,
s	s	k7c7	s
průměrnými	průměrný	k2eAgFnPc7d1	průměrná
maximálními	maximální	k2eAgFnPc7d1	maximální
teplotami	teplota	k1gFnPc7	teplota
24	[number]	k4	24
-	-	kIx~	-
28	[number]	k4	28
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
minimálními	minimální	k2eAgFnPc7d1	minimální
teplotami	teplota	k1gFnPc7	teplota
15	[number]	k4	15
-	-	kIx~	-
19	[number]	k4	19
°	°	k?	°
<g/>
C.	C.	kA	C.
I	i	k8xC	i
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
město	město	k1gNnSc4	město
během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
sezóny	sezóna	k1gFnSc2	sezóna
přeženou	přehnat	k5eAaPmIp3nP	přehnat
časté	častý	k2eAgFnPc1d1	častá
studené	studený	k2eAgFnPc1d1	studená
fronty	fronta	k1gFnPc1	fronta
(	(	kIx(	(
<g/>
16	[number]	k4	16
-	-	kIx~	-
21	[number]	k4	21
°	°	k?	°
<g/>
C	C	kA	C
maxima	maximum	k1gNnSc2	maximum
a	a	k8xC	a
4	[number]	k4	4
-	-	kIx~	-
10	[number]	k4	10
°	°	k?	°
<g/>
C	C	kA	C
minima	minimum	k1gNnSc2	minimum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
sníh	sníh	k1gInSc1	sníh
nad	nad	k7c7	nad
městem	město	k1gNnSc7	město
padal	padat	k5eAaImAgInS	padat
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1	ekonomika
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
cestovním	cestovní	k2eAgInSc6d1	cestovní
ruchu	ruch	k1gInSc6	ruch
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1940	[number]	k4	1940
-	-	kIx~	-
1980	[number]	k4	1980
byla	být	k5eAaImAgFnS	být
Fort	Fort	k?	Fort
Lauderdale	Lauderdala	k1gFnSc3	Lauderdala
oblíbenou	oblíbený	k2eAgFnSc7d1	oblíbená
vysokoškolskou	vysokoškolský	k2eAgFnSc7d1	vysokoškolská
destinací	destinace	k1gFnSc7	destinace
během	během	k7c2	během
jarním	jarní	k2eAgNnSc6d1	jarní
prázdnin	prázdniny	k1gFnPc2	prázdniny
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
do	do	k7c2	do
města	město	k1gNnSc2	město
jezdí	jezdit	k5eAaImIp3nP	jezdit
především	především	k9	především
zámožní	zámožný	k2eAgMnPc1d1	zámožný
turisté	turist	k1gMnPc1	turist
<g/>
,	,	kIx,	,
studentský	studentský	k2eAgInSc1d1	studentský
zájem	zájem	k1gInSc1	zájem
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
zmenšuje	zmenšovat	k5eAaImIp3nS	zmenšovat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
leží	ležet	k5eAaImIp3nS	ležet
23	[number]	k4	23
veřejných	veřejný	k2eAgFnPc2d1	veřejná
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
instituty	institut	k1gInPc1	institut
a	a	k8xC	a
oddělení	oddělení	k1gNnSc1	oddělení
předních	přední	k2eAgFnPc2d1	přední
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Broward	Broward	k1gInSc4	Broward
Community	Communita	k1gFnSc2	Communita
College	College	k1gFnSc1	College
<g/>
,	,	kIx,	,
Florida	Florida	k1gFnSc1	Florida
Atlantic	Atlantice	k1gFnPc2	Atlantice
University	universita	k1gFnSc2	universita
<g/>
,	,	kIx,	,
Florida	Florida	k1gFnSc1	Florida
International	International	k1gFnSc2	International
university	universita	k1gFnSc2	universita
<g/>
,	,	kIx,	,
Kaplan	Kaplan	k1gMnSc1	Kaplan
University	universita	k1gFnSc2	universita
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fortlauderdaleská	Fortlauderdaleský	k2eAgFnSc1d1	Fortlauderdaleský
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
výstava	výstava	k1gFnSc1	výstava
lodí	loď	k1gFnPc2	loď
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgFnSc1	třetí
největší	veliký	k2eAgFnSc1d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Dopravu	doprava	k1gFnSc4	doprava
zde	zde	k6eAd1	zde
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
městské	městský	k2eAgFnPc1d1	městská
autobusové	autobusový	k2eAgFnPc1d1	autobusová
linky	linka	k1gFnPc1	linka
a	a	k8xC	a
železnice	železnice	k1gFnPc1	železnice
(	(	kIx(	(
<g/>
4	[number]	k4	4
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plánuje	plánovat	k5eAaImIp3nS	plánovat
se	se	k3xPyFc4	se
výstavba	výstavba	k1gFnSc1	výstavba
moderní	moderní	k2eAgFnSc2d1	moderní
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
sítě	síť	k1gFnSc2	síť
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
Fort	Fort	k?	Fort
Lauderdale	Lauderdala	k1gFnSc3	Lauderdala
-	-	kIx~	-
Hollywood	Hollywood	k1gInSc1	Hollywood
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
městské	městský	k2eAgNnSc1d1	Městské
letiště	letiště	k1gNnSc1	letiště
a	a	k8xC	a
díky	díky	k7c3	díky
vytíženému	vytížený	k2eAgInSc3d1	vytížený
provozu	provoz	k1gInSc3	provoz
především	především	k9	především
nízkonákladových	nízkonákladový	k2eAgFnPc2d1	nízkonákladová
leteckých	letecký	k2eAgFnPc2d1	letecká
společností	společnost	k1gFnPc2	společnost
se	se	k3xPyFc4	se
pomalu	pomalu	k6eAd1	pomalu
stává	stávat	k5eAaImIp3nS	stávat
největším	veliký	k2eAgNnSc7d3	veliký
letištěm	letiště	k1gNnSc7	letiště
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Městu	město	k1gNnSc3	město
dále	daleko	k6eAd2	daleko
slouží	sloužit	k5eAaImIp3nS	sloužit
letiště	letiště	k1gNnSc4	letiště
miamské	miamský	k2eAgNnSc1d1	miamské
letiště	letiště	k1gNnSc1	letiště
a	a	k8xC	a
letiště	letiště	k1gNnSc1	letiště
Palm	Palm	k1gMnSc1	Palm
Beach	Beach	k1gMnSc1	Beach
<g/>
.	.	kIx.	.
</s>
<s>
Port	port	k1gInSc1	port
Everglades	Everglades	k1gInSc1	Everglades
ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgInSc4	třetí
největší	veliký	k2eAgInSc4d3	veliký
americký	americký	k2eAgInSc4d1	americký
lodní	lodní	k2eAgInSc4d1	lodní
přístav	přístav	k1gInSc4	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
také	také	k9	také
jako	jako	k8xC	jako
kulturní	kulturní	k2eAgFnPc1d1	kulturní
<g/>
,	,	kIx,	,
zpravodajské	zpravodajský	k2eAgFnPc1d1	zpravodajská
a	a	k8xC	a
sportovní	sportovní	k2eAgNnSc1d1	sportovní
centrum	centrum	k1gNnSc1	centrum
Floridy	Florida	k1gFnSc2	Florida
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
řada	řada	k1gFnSc1	řada
muzeí	muzeum	k1gNnPc2	muzeum
<g/>
,	,	kIx,	,
kin	kino	k1gNnPc2	kino
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc1d1	známý
bleší	bleší	k2eAgInSc1d1	bleší
trh	trh	k1gInSc1	trh
<g/>
;	;	kIx,	;
ve	v	k7c6	v
městě	město	k1gNnSc6	město
leží	ležet	k5eAaImIp3nS	ležet
také	také	k9	také
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
plavecká	plavecký	k2eAgFnSc1d1	plavecká
síň	síň	k1gFnSc1	síň
slávy	sláva	k1gFnSc2	sláva
a	a	k8xC	a
přiléhá	přiléhat	k5eAaImIp3nS	přiléhat
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
národní	národní	k2eAgInSc4d1	národní
park	park	k1gInSc4	park
Hugh	Hugh	k1gMnSc1	Hugh
Taylor	Taylor	k1gMnSc1	Taylor
Birch	Birch	k1gMnSc1	Birch
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
umělý	umělý	k2eAgInSc4d1	umělý
útes	útes	k1gInSc4	útes
Osborne	Osborn	k1gInSc5	Osborn
Reef	Reef	k1gInSc1	Reef
<g/>
,	,	kIx,	,
zhotovený	zhotovený	k2eAgInSc1d1	zhotovený
z	z	k7c2	z
pneumatik	pneumatika	k1gFnPc2	pneumatika
<g/>
.	.	kIx.	.
</s>
