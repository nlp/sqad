<p>
<s>
BFU	BFU	kA	BFU
je	být	k5eAaImIp3nS	být
výhradně	výhradně	k6eAd1	výhradně
česká	český	k2eAgFnSc1d1	Česká
iniciálová	iniciálový	k2eAgFnSc1d1	iniciálová
zkratka	zkratka	k1gFnSc1	zkratka
počítačového	počítačový	k2eAgInSc2d1	počítačový
slangu	slang	k1gInSc2	slang
označující	označující	k2eAgNnPc1d1	označující
běžného	běžný	k2eAgMnSc4d1	běžný
či	či	k8xC	či
naprosto	naprosto	k6eAd1	naprosto
nezkušeného	zkušený	k2eNgMnSc4d1	nezkušený
uživatele	uživatel	k1gMnSc4	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
BFU	BFU	kA	BFU
se	se	k3xPyFc4	se
vykládá	vykládat	k5eAaImIp3nS	vykládat
jako	jako	k9	jako
Běžný	běžný	k2eAgMnSc1d1	běžný
Fyzický	fyzický	k2eAgMnSc1d1	fyzický
Uživatel	uživatel	k1gMnSc1	uživatel
nebo	nebo	k8xC	nebo
Běžný	běžný	k2eAgMnSc1d1	běžný
Franta	Franta	k1gMnSc1	Franta
Uživatel	uživatel	k1gMnSc1	uživatel
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
ostřeji	ostro	k6eAd2	ostro
Blbý	blbý	k2eAgMnSc1d1	blbý
Franta	Franta	k1gMnSc1	Franta
Uživatel	uživatel	k1gMnSc1	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
objevovat	objevovat	k5eAaImF	objevovat
podoba	podoba	k1gFnSc1	podoba
Běžný	běžný	k2eAgMnSc1d1	běžný
Facebookový	Facebookový	k2eAgMnSc1d1	Facebookový
Uživatel	uživatel	k1gMnSc1	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
neexistenci	neexistence	k1gFnSc3	neexistence
tohoto	tento	k3xDgInSc2	tento
pojmu	pojem	k1gInSc2	pojem
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
lze	lze	k6eAd1	lze
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
až	až	k9	až
následně	následně	k6eAd1	následně
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
anglické	anglický	k2eAgFnPc4d1	anglická
verze	verze	k1gFnPc4	verze
–	–	k?	–
Bloody	Blood	k1gInPc1	Blood
Fucking	Fucking	k1gInSc1	Fucking
User	usrat	k5eAaPmRp2nS	usrat
(	(	kIx(	(
<g/>
sakra	sakra	k0	sakra
debilní	debilní	k2eAgMnSc1d1	debilní
uživatel	uživatel	k1gMnSc1	uživatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Beginner	Beginner	k1gMnSc1	Beginner
For	forum	k1gNnPc2	forum
Unix	Unix	k1gInSc1	Unix
(	(	kIx(	(
<g/>
unixový	unixový	k2eAgMnSc1d1	unixový
začátečník	začátečník	k1gMnSc1	začátečník
<g/>
)	)	kIx)	)
či	či	k8xC	či
BrainFree	BrainFree	k1gFnSc1	BrainFree
User	usrat	k5eAaPmRp2nS	usrat
(	(	kIx(	(
<g/>
uživatel	uživatel	k1gMnSc1	uživatel
bez	bez	k7c2	bez
mozku	mozek	k1gInSc2	mozek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Takto	takto	k6eAd1	takto
označený	označený	k2eAgMnSc1d1	označený
uživatel	uživatel	k1gMnSc1	uživatel
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
administrátora	administrátor	k1gMnSc2	administrátor
<g/>
/	/	kIx~	/
<g/>
programátora	programátor	k1gMnSc2	programátor
úplně	úplně	k6eAd1	úplně
neschopný	schopný	k2eNgInSc1d1	neschopný
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
všeho	všecek	k3xTgNnSc2	všecek
schopný	schopný	k2eAgInSc1d1	schopný
<g/>
,	,	kIx,	,
umí	umět	k5eAaImIp3nS	umět
pouze	pouze	k6eAd1	pouze
klikat	klikat	k5eAaImF	klikat
myší	myš	k1gFnSc7	myš
na	na	k7c4	na
co	co	k3yRnSc4	co
přijde	přijít	k5eAaPmIp3nS	přijít
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
sebemenším	sebemenší	k2eAgInSc6d1	sebemenší
problému	problém	k1gInSc6	problém
zvedá	zvedat	k5eAaImIp3nS	zvedat
telefonní	telefonní	k2eAgNnSc4d1	telefonní
sluchátko	sluchátko	k1gNnSc4	sluchátko
(	(	kIx(	(
<g/>
píše	psát	k5eAaImIp3nS	psát
e-mail	eail	k1gInSc4	e-mail
<g/>
,	,	kIx,	,
posílá	posílat	k5eAaImIp3nS	posílat
ICQ	ICQ	kA	ICQ
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
)	)	kIx)	)
a	a	k8xC	a
volá	volat	k5eAaImIp3nS	volat
svému	svůj	k3xOyFgMnSc3	svůj
administrátorovi	administrátor	k1gMnSc3	administrátor
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
to	ten	k3xDgNnSc1	ten
<g/>
"	"	kIx"	"
pokazilo	pokazit	k5eAaPmAgNnS	pokazit
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
není	být	k5eNaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
popsat	popsat	k5eAaPmF	popsat
problém	problém	k1gInSc4	problém
a	a	k8xC	a
porozumět	porozumět	k5eAaPmF	porozumět
mu	on	k3xPp3gMnSc3	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Agresivní	agresivní	k2eAgMnSc1d1	agresivní
BFU	BFU	kA	BFU
==	==	k?	==
</s>
</p>
<p>
<s>
Zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
variantou	varianta	k1gFnSc7	varianta
BFU	BFU	kA	BFU
je	být	k5eAaImIp3nS	být
agresivní	agresivní	k2eAgMnSc1d1	agresivní
BFU	BFU	kA	BFU
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
svou	svůj	k3xOyFgFnSc4	svůj
neznalost	neznalost	k1gFnSc4	neznalost
a	a	k8xC	a
frustraci	frustrace	k1gFnSc4	frustrace
z	z	k7c2	z
používání	používání	k1gNnSc2	používání
výpočetní	výpočetní	k2eAgFnSc2d1	výpočetní
techniky	technika	k1gFnSc2	technika
svaluje	svalovat	k5eAaImIp3nS	svalovat
na	na	k7c4	na
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
.	.	kIx.	.
</s>
<s>
Jakýkoliv	jakýkoliv	k3yIgInSc1	jakýkoliv
problém	problém	k1gInSc1	problém
(	(	kIx(	(
<g/>
který	který	k3yQgInSc1	který
většinou	většinou	k6eAd1	většinou
způsobí	způsobit	k5eAaPmIp3nS	způsobit
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
mnohdy	mnohdy	k6eAd1	mnohdy
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
banální	banální	k2eAgFnSc4d1	banální
záležitost	záležitost	k1gFnSc4	záležitost
<g/>
)	)	kIx)	)
svaluje	svalovat	k5eAaImIp3nS	svalovat
na	na	k7c4	na
jiné	jiný	k2eAgNnSc4d1	jiné
a	a	k8xC	a
agresivně	agresivně	k6eAd1	agresivně
se	se	k3xPyFc4	se
domáhá	domáhat	k5eAaImIp3nS	domáhat
rychlé	rychlý	k2eAgFnPc4d1	rychlá
opravy	oprava	k1gFnPc4	oprava
problému	problém	k1gInSc2	problém
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
předtím	předtím	k6eAd1	předtím
to	ten	k3xDgNnSc1	ten
fungovalo	fungovat	k5eAaImAgNnS	fungovat
<g/>
"	"	kIx"	"
a	a	k8xC	a
tedy	tedy	k9	tedy
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jednoznačně	jednoznačně	k6eAd1	jednoznačně
chyba	chyba	k1gFnSc1	chyba
administrátora	administrátor	k1gMnSc2	administrátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Neagresivní	agresivní	k2eNgMnSc1d1	neagresivní
BFU	BFU	kA	BFU
==	==	k?	==
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
zmíněno	zmínit	k5eAaPmNgNnS	zmínit
<g/>
,	,	kIx,	,
BFU	BFU	kA	BFU
označuje	označovat	k5eAaImIp3nS	označovat
nezkušeného	zkušený	k2eNgMnSc2d1	nezkušený
<g/>
,	,	kIx,	,
běžného	běžný	k2eAgMnSc2d1	běžný
<g/>
,	,	kIx,	,
uživatele	uživatel	k1gMnSc2	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
BFU	BFU	kA	BFU
lze	lze	k6eAd1	lze
tedy	tedy	k9	tedy
zároveň	zároveň	k6eAd1	zároveň
vykládat	vykládat	k5eAaImF	vykládat
neagresivní	agresivní	k2eNgFnSc7d1	neagresivní
formou	forma	k1gFnSc7	forma
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Běžný	běžný	k2eAgMnSc1d1	běžný
Fyzický	fyzický	k2eAgMnSc1d1	fyzický
Uživatel	uživatel	k1gMnSc1	uživatel
<g/>
"	"	kIx"	"
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Český	český	k2eAgInSc1d1	český
původ	původ	k1gInSc1	původ
termínu	termín	k1gInSc2	termín
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
výzkumu	výzkum	k1gInSc2	výzkum
redaktorů	redaktor	k1gMnPc2	redaktor
časopisu	časopis	k1gInSc2	časopis
Root	Roota	k1gFnPc2	Roota
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
je	být	k5eAaImIp3nS	být
termín	termín	k1gInSc4	termín
původu	původ	k1gInSc2	původ
českého	český	k2eAgInSc2d1	český
<g/>
.	.	kIx.	.
</s>
<s>
BFU	BFU	kA	BFU
je	být	k5eAaImIp3nS	být
specifický	specifický	k2eAgInSc1d1	specifický
český	český	k2eAgInSc1d1	český
výraz	výraz	k1gInSc1	výraz
a	a	k8xC	a
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
cizojazyčném	cizojazyčný	k2eAgInSc6d1	cizojazyčný
kontaktu	kontakt	k1gInSc6	kontakt
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
raději	rád	k6eAd2	rád
používat	používat	k5eAaImF	používat
mezinárodně	mezinárodně	k6eAd1	mezinárodně
srozumitelné	srozumitelný	k2eAgNnSc4d1	srozumitelné
"	"	kIx"	"
<g/>
lamer	lamer	k1gInSc4	lamer
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
zkratek	zkratka	k1gFnPc2	zkratka
v	v	k7c6	v
online	onlinout	k5eAaPmIp3nS	onlinout
diskusích	diskuse	k1gFnPc6	diskuse
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
