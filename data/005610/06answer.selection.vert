<s>
Rybník	rybník	k1gInSc1	rybník
je	být	k5eAaImIp3nS	být
vodohospodářská	vodohospodářský	k2eAgFnSc1d1	vodohospodářská
stavba	stavba	k1gFnSc1	stavba
typu	typ	k1gInSc2	typ
umělé	umělý	k2eAgFnSc2d1	umělá
vodní	vodní	k2eAgFnSc2d1	vodní
nádrže	nádrž	k1gFnSc2	nádrž
určené	určený	k2eAgFnPc1d1	určená
především	především	k6eAd1	především
k	k	k7c3	k
chovu	chov	k1gInSc3	chov
ryb	ryba	k1gFnPc2	ryba
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgFnSc2d1	vodní
drůbeže	drůbež	k1gFnSc2	drůbež
a	a	k8xC	a
obecně	obecně	k6eAd1	obecně
plní	plnit	k5eAaImIp3nS	plnit
i	i	k9	i
funkci	funkce	k1gFnSc4	funkce
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
zadržování	zadržování	k1gNnSc2	zadržování
(	(	kIx(	(
<g/>
retence	retence	k1gFnSc2	retence
<g/>
)	)	kIx)	)
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
krajině	krajina	k1gFnSc6	krajina
<g/>
.	.	kIx.	.
</s>
