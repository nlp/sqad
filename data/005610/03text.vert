<s>
Rybník	rybník	k1gInSc1	rybník
je	být	k5eAaImIp3nS	být
vodohospodářská	vodohospodářský	k2eAgFnSc1d1	vodohospodářská
stavba	stavba	k1gFnSc1	stavba
typu	typ	k1gInSc2	typ
umělé	umělý	k2eAgFnSc2d1	umělá
vodní	vodní	k2eAgFnSc2d1	vodní
nádrže	nádrž	k1gFnSc2	nádrž
určené	určený	k2eAgFnPc1d1	určená
především	především	k6eAd1	především
k	k	k7c3	k
chovu	chov	k1gInSc3	chov
ryb	ryba	k1gFnPc2	ryba
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgFnSc2d1	vodní
drůbeže	drůbež	k1gFnSc2	drůbež
a	a	k8xC	a
obecně	obecně	k6eAd1	obecně
plní	plnit	k5eAaImIp3nS	plnit
i	i	k9	i
funkci	funkce	k1gFnSc4	funkce
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
zadržování	zadržování	k1gNnSc2	zadržování
(	(	kIx(	(
<g/>
retence	retence	k1gFnSc2	retence
<g/>
)	)	kIx)	)
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
krajině	krajina	k1gFnSc6	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
rybníku	rybník	k1gInSc3	rybník
je	být	k5eAaImIp3nS	být
pozemek	pozemek	k1gInSc4	pozemek
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
je	být	k5eAaImIp3nS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
<g/>
,	,	kIx,	,
přírodní	přírodní	k2eAgNnSc4d1	přírodní
dno	dno	k1gNnSc4	dno
<g/>
,	,	kIx,	,
hráz	hráz	k1gFnSc1	hráz
a	a	k8xC	a
technická	technický	k2eAgFnSc1d1	technická
vybavenost	vybavenost	k1gFnSc1	vybavenost
nutná	nutný	k2eAgFnSc1d1	nutná
k	k	k7c3	k
regulaci	regulace	k1gFnSc3	regulace
vodní	vodní	k2eAgFnSc2d1	vodní
hladiny	hladina	k1gFnSc2	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
většinou	většinou	k6eAd1	většinou
průtočné	průtočný	k2eAgFnSc2d1	průtočná
vodní	vodní	k2eAgFnSc2d1	vodní
nádrže	nádrž	k1gFnSc2	nádrž
se	s	k7c7	s
zatopenými	zatopený	k2eAgInPc7d1	zatopený
pozemky	pozemek	k1gInPc7	pozemek
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
hladiny	hladina	k1gFnSc2	hladina
vody	voda	k1gFnSc2	voda
při	při	k7c6	při
navrženém	navržený	k2eAgInSc6d1	navržený
průtoku	průtok	k1gInSc6	průtok
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
přítokovou	přítokový	k2eAgFnSc4d1	přítoková
a	a	k8xC	a
odtokovou	odtokový	k2eAgFnSc4d1	odtoková
část	část	k1gFnSc4	část
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
obtokovou	obtokový	k2eAgFnSc4d1	obtoková
stoku	stoka	k1gFnSc4	stoka
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
obtokový	obtokový	k2eAgInSc1d1	obtokový
rybník	rybník	k1gInSc1	rybník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
krajině	krajina	k1gFnSc6	krajina
tvoří	tvořit	k5eAaImIp3nP	tvořit
skupiny	skupina	k1gFnPc1	skupina
samostatných	samostatný	k2eAgFnPc2d1	samostatná
nebo	nebo	k8xC	nebo
vzájemně	vzájemně	k6eAd1	vzájemně
vodními	vodní	k2eAgInPc7d1	vodní
toky	tok	k1gInPc7	tok
propojených	propojený	k2eAgInPc2d1	propojený
rybníků	rybník	k1gInPc2	rybník
tzv.	tzv.	kA	tzv.
rybniční	rybniční	k2eAgFnSc2d1	rybniční
soustavy	soustava	k1gFnSc2	soustava
využívající	využívající	k2eAgFnPc4d1	využívající
přírodní	přírodní	k2eAgFnPc4d1	přírodní
poměry	poměra	k1gFnPc4	poměra
a	a	k8xC	a
terén	terén	k1gInSc4	terén
například	například	k6eAd1	například
ve	v	k7c6	v
vhodných	vhodný	k2eAgFnPc6d1	vhodná
hydrogeologických	hydrogeologický	k2eAgFnPc6d1	hydrogeologická
lokalitách	lokalita	k1gFnPc6	lokalita
<g/>
.	.	kIx.	.
</s>
<s>
Rybník	rybník	k1gInSc1	rybník
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
také	také	k9	také
význam	význam	k1gInSc4	význam
prvku	prvek	k1gInSc2	prvek
zahradní	zahradní	k2eAgFnSc2d1	zahradní
architektury	architektura	k1gFnSc2	architektura
používaného	používaný	k2eAgMnSc4d1	používaný
v	v	k7c6	v
sadovnické	sadovnický	k2eAgFnSc6d1	Sadovnická
tvorbě	tvorba	k1gFnSc6	tvorba
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
používá	používat	k5eAaImIp3nS	používat
rybníky	rybník	k1gInPc4	rybník
i	i	k8xC	i
soustavy	soustava	k1gFnSc2	soustava
rybníků	rybník	k1gInPc2	rybník
též	též	k9	též
k	k	k7c3	k
napodobení	napodobení	k1gNnSc3	napodobení
říčního	říční	k2eAgInSc2d1	říční
toku	tok	k1gInSc2	tok
<g/>
.	.	kIx.	.
</s>
<s>
Dělení	dělení	k1gNnSc1	dělení
podle	podle	k7c2	podle
zdroje	zdroj	k1gInSc2	zdroj
vody	voda	k1gFnSc2	voda
Rybníky	rybník	k1gInPc1	rybník
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
způsobu	způsob	k1gInSc2	způsob
zásobení	zásobení	k1gNnSc4	zásobení
vodou	voda	k1gFnSc7	voda
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
nebeské	nebeský	k2eAgMnPc4d1	nebeský
(	(	kIx(	(
<g/>
plněné	plněný	k2eAgNnSc1d1	plněné
dešťovou	dešťový	k2eAgFnSc7d1	dešťová
vodou	voda	k1gFnSc7	voda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pramenité	pramenitý	k2eAgInPc1d1	pramenitý
<g/>
,	,	kIx,	,
říční	říční	k2eAgNnPc1d1	říční
a	a	k8xC	a
potoční	potoční	k2eAgNnPc1d1	potoční
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
existují	existovat	k5eAaImIp3nP	existovat
rybníky	rybník	k1gInPc1	rybník
se	s	k7c7	s
slanou	slaný	k2eAgFnSc7d1	slaná
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
které	který	k3yQgFnSc3	který
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
rybníky	rybník	k1gInPc1	rybník
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
především	především	k9	především
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Dělení	dělení	k1gNnSc1	dělení
podle	podle	k7c2	podle
příslušnosti	příslušnost	k1gFnSc2	příslušnost
k	k	k7c3	k
rybniční	rybniční	k2eAgFnSc3d1	rybniční
soustavě	soustava	k1gFnSc3	soustava
Rybníky	rybník	k1gInPc4	rybník
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
ojedinělé	ojedinělý	k2eAgInPc1d1	ojedinělý
(	(	kIx(	(
<g/>
izolované	izolovaný	k2eAgInPc1d1	izolovaný
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
v	v	k7c6	v
rybniční	rybniční	k2eAgFnSc6d1	rybniční
soustavě	soustava	k1gFnSc6	soustava
s	s	k7c7	s
napájecími	napájecí	k2eAgFnPc7d1	napájecí
<g/>
,	,	kIx,	,
odvodňovacími	odvodňovací	k2eAgInPc7d1	odvodňovací
náhony	náhon	k1gInPc7	náhon
a	a	k8xC	a
obtokovými	obtokový	k2eAgFnPc7d1	obtoková
stokami	stoka	k1gFnPc7	stoka
pro	pro	k7c4	pro
regulaci	regulace	k1gFnSc4	regulace
přítoku	přítok	k1gInSc2	přítok
a	a	k8xC	a
odtoku	odtok	k1gInSc2	odtok
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnPc1d3	veliký
rybniční	rybniční	k2eAgFnPc1d1	rybniční
soustavy	soustava	k1gFnPc1	soustava
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Třeboně	Třeboň	k1gFnSc2	Třeboň
<g/>
,	,	kIx,	,
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
<g/>
,	,	kIx,	,
Hluboké	hluboký	k2eAgFnPc1d1	hluboká
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
,	,	kIx,	,
Vodňan	Vodňan	k1gInSc1	Vodňan
<g/>
,	,	kIx,	,
Blatné	blatný	k2eAgFnPc1d1	Blatná
<g/>
,	,	kIx,	,
Jindřichova	Jindřichův	k2eAgInSc2d1	Jindřichův
Hradce	Hradec	k1gInSc2	Hradec
<g/>
,	,	kIx,	,
Chlumce	Chlumec	k1gInSc2	Chlumec
nad	nad	k7c7	nad
Cidlinou	Cidlina	k1gFnSc7	Cidlina
a	a	k8xC	a
Pardubic	Pardubice	k1gInPc2	Pardubice
<g/>
.	.	kIx.	.
</s>
<s>
Dělení	dělení	k1gNnSc4	dělení
podle	podle	k7c2	podle
okolností	okolnost	k1gFnPc2	okolnost
ovlivňující	ovlivňující	k2eAgNnSc4d1	ovlivňující
prostředí	prostředí	k1gNnSc4	prostředí
v	v	k7c6	v
rybníce	rybník	k1gInSc6	rybník
Rybníky	rybník	k1gInPc4	rybník
dělíme	dělit	k5eAaImIp1nP	dělit
podle	podle	k7c2	podle
okolí	okolí	k1gNnSc2	okolí
na	na	k7c6	na
polní	polní	k2eAgFnSc6d1	polní
<g/>
,	,	kIx,	,
luční	luční	k2eAgFnSc6d1	luční
<g/>
,	,	kIx,	,
lesní	lesní	k2eAgFnSc6d1	lesní
<g/>
,	,	kIx,	,
návesní	návesní	k2eAgFnSc6d1	návesní
a	a	k8xC	a
ostatní	ostatní	k2eAgFnSc6d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Rybníky	rybník	k1gInPc4	rybník
dělíme	dělit	k5eAaImIp1nP	dělit
dále	daleko	k6eAd2	daleko
podle	podle	k7c2	podle
klimatu	klima	k1gNnSc2	klima
na	na	k7c6	na
vrchovinné	vrchovinný	k2eAgFnSc6d1	vrchovinná
a	a	k8xC	a
nížinné	nížinný	k2eAgFnSc6d1	nížinná
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
rybníky	rybník	k1gInPc1	rybník
dělí	dělit	k5eAaImIp3nP	dělit
podle	podle	k7c2	podle
složení	složení	k1gNnSc2	složení
vody	voda	k1gFnSc2	voda
přitékající	přitékající	k2eAgFnSc2d1	přitékající
do	do	k7c2	do
rybníka	rybník	k1gInSc2	rybník
<g/>
,	,	kIx,	,
velikosti	velikost	k1gFnSc2	velikost
rybníka	rybník	k1gInSc2	rybník
<g/>
,	,	kIx,	,
průměrné	průměrný	k2eAgFnSc2d1	průměrná
hloubky	hloubka	k1gFnSc2	hloubka
<g/>
,	,	kIx,	,
velikosti	velikost	k1gFnSc2	velikost
břehového	břehový	k2eAgInSc2d1	břehový
koeficientu	koeficient	k1gInSc2	koeficient
<g/>
,	,	kIx,	,
složení	složení	k1gNnSc2	složení
podloží	podloží	k1gNnSc2	podloží
rybníku	rybník	k1gInSc6	rybník
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
kritérií	kritérion	k1gNnPc2	kritérion
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
charakteristiky	charakteristika	k1gFnPc1	charakteristika
rybníka	rybník	k1gInSc2	rybník
určují	určovat	k5eAaImIp3nP	určovat
vhodnost	vhodnost	k1gFnSc4	vhodnost
pro	pro	k7c4	pro
konkrétní	konkrétní	k2eAgNnSc4d1	konkrétní
hospodářské	hospodářský	k2eAgNnSc4d1	hospodářské
využití	využití	k1gNnSc4	využití
<g/>
.	.	kIx.	.
</s>
<s>
Dělení	dělení	k1gNnSc1	dělení
podle	podle	k7c2	podle
druhového	druhový	k2eAgNnSc2d1	druhové
složení	složení	k1gNnSc2	složení
populace	populace	k1gFnSc2	populace
ryb	ryba	k1gFnPc2	ryba
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
dominují	dominovat	k5eAaImIp3nP	dominovat
kaprové	kaprový	k2eAgInPc1d1	kaprový
rybníky	rybník	k1gInPc1	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Časté	častý	k2eAgFnPc1d1	častá
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
pstruhové	pstruhový	k2eAgInPc1d1	pstruhový
rybníky	rybník	k1gInPc1	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
charakteru	charakter	k1gInSc2	charakter
rybníka	rybník	k1gInSc2	rybník
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
chovány	chován	k2eAgMnPc4d1	chován
i	i	k8xC	i
další	další	k2eAgMnPc4d1	další
druhy	druh	k1gMnPc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Dělení	dělení	k1gNnSc1	dělení
rybníků	rybník	k1gInPc2	rybník
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
rybníkářských	rybníkářský	k2eAgFnPc2d1	rybníkářská
nádrží	nádrž	k1gFnPc2	nádrž
podle	podle	k7c2	podle
funkce	funkce	k1gFnSc2	funkce
Hlavní	hlavní	k2eAgInPc1d1	hlavní
rybníky	rybník	k1gInPc1	rybník
jsou	být	k5eAaImIp3nP	být
plochou	plocha	k1gFnSc7	plocha
největší	veliký	k2eAgInPc1d3	veliký
rybníky	rybník	k1gInPc1	rybník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavních	hlavní	k2eAgInPc6d1	hlavní
rybnících	rybník	k1gInPc6	rybník
se	se	k3xPyFc4	se
dokončuje	dokončovat	k5eAaImIp3nS	dokončovat
produkční	produkční	k2eAgInSc1d1	produkční
turnus	turnus	k1gInSc1	turnus
ryb	ryba	k1gFnPc2	ryba
a	a	k8xC	a
výstupem	výstup	k1gInSc7	výstup
je	být	k5eAaImIp3nS	být
vážná	vážný	k2eAgFnSc1d1	vážná
(	(	kIx(	(
<g/>
tržní	tržní	k2eAgFnSc1d1	tržní
<g/>
)	)	kIx)	)
ryba	ryba	k1gFnSc1	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
nejznámější	známý	k2eAgInPc1d3	nejznámější
rybníky	rybník	k1gInPc1	rybník
jsou	být	k5eAaImIp3nP	být
rybníky	rybník	k1gInPc1	rybník
hlavní	hlavní	k2eAgInPc1d1	hlavní
<g/>
.	.	kIx.	.
</s>
<s>
Plůdkový	plůdkový	k2eAgInSc1d1	plůdkový
předvýtažník	předvýtažník	k1gInSc1	předvýtažník
<g/>
,	,	kIx,	,
plůdkový	plůdkový	k2eAgInSc1d1	plůdkový
výtažník	výtažník	k1gInSc1	výtažník
<g/>
,	,	kIx,	,
výtažník	výtažník	k1gInSc1	výtažník
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
chovu	chov	k1gInSc3	chov
mladých	mladý	k2eAgFnPc2d1	mladá
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Technologie	technologie	k1gFnSc1	technologie
chovu	chov	k1gInSc2	chov
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
konkrétním	konkrétní	k2eAgInSc6d1	konkrétní
druhu	druh	k1gInSc6	druh
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Komorový	komorový	k2eAgInSc1d1	komorový
rybník	rybník	k1gInSc1	rybník
slouží	sloužit	k5eAaImIp3nS	sloužit
ke	k	k7c3	k
komorování	komorování	k1gNnSc3	komorování
(	(	kIx(	(
<g/>
zimování	zimování	k1gNnSc3	zimování
<g/>
)	)	kIx)	)
ryb	ryba	k1gFnPc2	ryba
před	před	k7c7	před
vysazením	vysazení	k1gNnSc7	vysazení
do	do	k7c2	do
hlavních	hlavní	k2eAgInPc2d1	hlavní
rybníků	rybník	k1gInPc2	rybník
<g/>
.	.	kIx.	.
sádky	sádek	k1gInPc1	sádek
V	v	k7c6	v
matečních	mateční	k2eAgInPc6d1	mateční
rybnících	rybník	k1gInPc6	rybník
se	se	k3xPyFc4	se
chovají	chovat	k5eAaImIp3nP	chovat
generační	generační	k2eAgFnPc1d1	generační
ryby	ryba	k1gFnPc1	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Dubraviovy	Dubraviův	k2eAgInPc4d1	Dubraviův
(	(	kIx(	(
<g/>
výtěrové	výtěrový	k2eAgInPc4d1	výtěrový
<g/>
)	)	kIx)	)
rybníčky	rybníček	k1gInPc1	rybníček
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
tření	tření	k1gNnSc3	tření
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Karanténní	karanténní	k2eAgFnSc1d1	karanténní
nádrž	nádrž	k1gFnSc1	nádrž
izoluje	izolovat	k5eAaBmIp3nS	izolovat
ryby	ryba	k1gFnPc4	ryba
od	od	k7c2	od
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
při	při	k7c6	při
podezření	podezření	k1gNnSc6	podezření
na	na	k7c4	na
nákazu	nákaza	k1gFnSc4	nákaza
nebo	nebo	k8xC	nebo
při	při	k7c6	při
chovu	chov	k1gInSc6	chov
GMO	GMO	kA	GMO
<g/>
.	.	kIx.	.
</s>
<s>
Stabilizační	stabilizační	k2eAgInSc1d1	stabilizační
(	(	kIx(	(
<g/>
biologický	biologický	k2eAgInSc1d1	biologický
<g/>
)	)	kIx)	)
rybník	rybník	k1gInSc1	rybník
především	především	k6eAd1	především
stabilizuje	stabilizovat	k5eAaBmIp3nS	stabilizovat
složení	složení	k1gNnSc4	složení
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
produkce	produkce	k1gFnSc2	produkce
ryb	ryba	k1gFnPc2	ryba
má	mít	k5eAaImIp3nS	mít
druhotný	druhotný	k2eAgInSc4d1	druhotný
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
rybníky	rybník	k1gInPc1	rybník
se	se	k3xPyFc4	se
zařazují	zařazovat	k5eAaImIp3nP	zařazovat
za	za	k7c4	za
výpustě	výpustě	k6eAd1	výpustě
odpadů	odpad	k1gInPc2	odpad
z	z	k7c2	z
potravinářského	potravinářský	k2eAgInSc2d1	potravinářský
průmyslu	průmysl	k1gInSc2	průmysl
(	(	kIx(	(
<g/>
mlékárny	mlékárna	k1gFnPc1	mlékárna
<g/>
,	,	kIx,	,
jatka	jatka	k1gFnSc1	jatka
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kořenové	kořenový	k2eAgFnPc1d1	kořenová
čističky	čistička	k1gFnPc1	čistička
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
provozy	provoz	k1gInPc1	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Rybníky	rybník	k1gInPc1	rybník
pro	pro	k7c4	pro
rybníkářský	rybníkářský	k2eAgInSc4d1	rybníkářský
výzkum	výzkum	k1gInSc4	výzkum
Dělení	dělení	k1gNnSc2	dělení
podle	podle	k7c2	podle
intenzity	intenzita	k1gFnSc2	intenzita
hospodaření	hospodaření	k1gNnSc2	hospodaření
Rybníky	rybník	k1gInPc1	rybník
dělíme	dělit	k5eAaImIp1nP	dělit
na	na	k7c4	na
extenzivní	extenzivní	k2eAgInSc4d1	extenzivní
<g/>
,	,	kIx,	,
polointenzivní	polointenzivní	k2eAgInSc4d1	polointenzivní
a	a	k8xC	a
intenzivní	intenzivní	k2eAgInSc4d1	intenzivní
<g/>
.	.	kIx.	.
</s>
<s>
Kritériem	kritérion	k1gNnSc7	kritérion
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
rozsah	rozsah	k1gInSc4	rozsah
hnojení	hnojení	k1gNnSc2	hnojení
<g/>
,	,	kIx,	,
vápnění	vápnění	k1gNnSc2	vápnění
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
zásahů	zásah	k1gInPc2	zásah
<g/>
.	.	kIx.	.
</s>
<s>
Dělení	dělení	k1gNnSc1	dělení
podle	podle	k7c2	podle
bonity	bonita	k1gFnSc2	bonita
rybníky	rybník	k1gInPc1	rybník
s	s	k7c7	s
ročním	roční	k2eAgInSc7d1	roční
hektarovým	hektarový	k2eAgInSc7d1	hektarový
výnosem	výnos	k1gInSc7	výnos
nad	nad	k7c4	nad
200	[number]	k4	200
kg	kg	kA	kg
ryb	ryba	k1gFnPc2	ryba
rybníky	rybník	k1gInPc1	rybník
s	s	k7c7	s
ročním	roční	k2eAgInSc7d1	roční
hektarovým	hektarový	k2eAgInSc7d1	hektarový
výnosem	výnos	k1gInSc7	výnos
nad	nad	k7c4	nad
100	[number]	k4	100
kg	kg	kA	kg
a	a	k8xC	a
pod	pod	k7c4	pod
200	[number]	k4	200
kg	kg	kA	kg
ryb	ryba	k1gFnPc2	ryba
rybníky	rybník	k1gInPc1	rybník
s	s	k7c7	s
ročním	roční	k2eAgInSc7d1	roční
hektarovým	hektarový	k2eAgInSc7d1	hektarový
výnosem	výnos	k1gInSc7	výnos
nad	nad	k7c4	nad
50	[number]	k4	50
kg	kg	kA	kg
a	a	k8xC	a
pod	pod	k7c4	pod
100	[number]	k4	100
kg	kg	kA	kg
ryb	ryba	k1gFnPc2	ryba
rybníky	rybník	k1gInPc1	rybník
s	s	k7c7	s
ročním	roční	k2eAgInSc7d1	roční
hektarovým	hektarový	k2eAgInSc7d1	hektarový
výnosem	výnos	k1gInSc7	výnos
pod	pod	k7c4	pod
50	[number]	k4	50
kg	kg	kA	kg
ryb	ryba	k1gFnPc2	ryba
rybníky	rybník	k1gInPc1	rybník
hospodářsky	hospodářsky	k6eAd1	hospodářsky
nepoužitelné	použitelný	k2eNgInPc1d1	nepoužitelný
(	(	kIx(	(
<g/>
např.	např.	kA	např.
chemicky	chemicky	k6eAd1	chemicky
kontaminované	kontaminovaný	k2eAgFnPc4d1	kontaminovaná
laguny	laguna	k1gFnPc4	laguna
<g/>
)	)	kIx)	)
Vedle	vedle	k7c2	vedle
běžných	běžný	k2eAgFnPc2d1	běžná
ryb	ryba	k1gFnPc2	ryba
mohou	moct	k5eAaImIp3nP	moct
rybníky	rybník	k1gInPc4	rybník
poskytovat	poskytovat	k5eAaImF	poskytovat
i	i	k9	i
vedlejší	vedlejší	k2eAgInPc4d1	vedlejší
produkty	produkt	k1gInPc4	produkt
<g/>
:	:	kIx,	:
Na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
rybníků	rybník	k1gInPc2	rybník
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
zbudována	zbudován	k2eAgNnPc4d1	zbudováno
zařízení	zařízení	k1gNnPc4	zařízení
pro	pro	k7c4	pro
chov	chov	k1gInSc4	chov
hus	husa	k1gFnPc2	husa
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgFnSc2d1	jiná
vodní	vodní	k2eAgFnSc2d1	vodní
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
a	a	k8xC	a
zemích	zem	k1gFnPc6	zem
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Evropy	Evropa	k1gFnSc2	Evropa
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
rybniční	rybniční	k2eAgFnPc1d1	rybniční
rákosiny	rákosina	k1gFnPc1	rákosina
pravidelně	pravidelně	k6eAd1	pravidelně
sklízeny	sklízen	k2eAgFnPc1d1	sklízena
<g/>
.	.	kIx.	.
</s>
<s>
Rákosí	rákosí	k1gNnSc1	rákosí
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
doškových	doškový	k2eAgFnPc2d1	došková
střech	střecha	k1gFnPc2	střecha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
není	být	k5eNaImIp3nS	být
vhodné	vhodný	k2eAgNnSc4d1	vhodné
klima	klima	k1gNnSc4	klima
pro	pro	k7c4	pro
pěstování	pěstování	k1gNnSc4	pěstování
kvalitního	kvalitní	k2eAgNnSc2d1	kvalitní
rákosí	rákosí	k1gNnSc2	rákosí
<g/>
.	.	kIx.	.
</s>
<s>
Speciálním	speciální	k2eAgInSc7d1	speciální
chovem	chov	k1gInSc7	chov
mohou	moct	k5eAaImIp3nP	moct
ryby	ryba	k1gFnPc1	ryba
získávat	získávat	k5eAaImF	získávat
speciální	speciální	k2eAgFnPc4d1	speciální
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
superkapr	superkapr	k1gInSc1	superkapr
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
rybníky	rybník	k1gInPc1	rybník
se	se	k3xPyFc4	se
využívají	využívat	k5eAaPmIp3nP	využívat
k	k	k7c3	k
chovu	chov	k1gInSc3	chov
žab	žába	k1gFnPc2	žába
<g/>
,	,	kIx,	,
raků	rak	k1gMnPc2	rak
nebo	nebo	k8xC	nebo
k	k	k7c3	k
lovu	lov	k1gInSc3	lov
vybraných	vybraný	k2eAgInPc2d1	vybraný
druhů	druh	k1gInPc2	druh
volně	volně	k6eAd1	volně
žijících	žijící	k2eAgMnPc2d1	žijící
vodních	vodní	k2eAgMnPc2d1	vodní
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zamokřených	zamokřený	k2eAgFnPc6d1	zamokřená
zátopách	zátopa	k1gFnPc6	zátopa
rybníků	rybník	k1gInPc2	rybník
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
vysazována	vysazován	k2eAgNnPc1d1	vysazováno
stromořadí	stromořadí	k1gNnPc1	stromořadí
babek	babka	k1gFnPc2	babka
(	(	kIx(	(
<g/>
vhodně	vhodně	k6eAd1	vhodně
řezané	řezaný	k2eAgFnPc4d1	řezaná
vrby	vrba	k1gFnPc4	vrba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yRgInPc2	který
se	se	k3xPyFc4	se
sklízí	sklízet	k5eAaImIp3nS	sklízet
proutí	proutí	k1gNnSc2	proutí
pro	pro	k7c4	pro
košíkářství	košíkářství	k1gNnSc4	košíkářství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
rybniční	rybniční	k2eAgNnSc1d1	rybniční
bahno	bahno	k1gNnSc1	bahno
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
na	na	k7c4	na
hnojivo	hnojivo	k1gNnSc4	hnojivo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
zákonná	zákonný	k2eAgFnSc1d1	zákonná
úprava	úprava	k1gFnSc1	úprava
považuje	považovat	k5eAaImIp3nS	považovat
rybniční	rybniční	k2eAgNnSc4d1	rybniční
bahno	bahno	k1gNnSc4	bahno
za	za	k7c4	za
odpad	odpad	k1gInSc4	odpad
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
rybníce	rybník	k1gInSc6	rybník
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
instalována	instalovat	k5eAaBmNgFnS	instalovat
malá	malý	k2eAgFnSc1d1	malá
vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
rybník	rybník	k1gInSc1	rybník
Rožmberk	Rožmberk	k1gInSc1	Rožmberk
s	s	k7c7	s
instalovaným	instalovaný	k2eAgInSc7d1	instalovaný
výkonem	výkon	k1gInSc7	výkon
260	[number]	k4	260
kW	kW	kA	kW
<g/>
.	.	kIx.	.
</s>
<s>
Experimentálně	experimentálně	k6eAd1	experimentálně
se	se	k3xPyFc4	se
filtruje	filtrovat	k5eAaImIp3nS	filtrovat
nežádoucí	žádoucí	k2eNgFnSc1d1	nežádoucí
biomasa	biomasa	k1gFnSc1	biomasa
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
přemnožené	přemnožený	k2eAgFnSc2d1	přemnožená
sinice	sinice	k1gFnSc2	sinice
<g/>
)	)	kIx)	)
a	a	k8xC	a
zkoumají	zkoumat	k5eAaImIp3nP	zkoumat
se	se	k3xPyFc4	se
možnosti	možnost	k1gFnPc1	možnost
jejího	její	k3xOp3gNnSc2	její
zpracování	zpracování	k1gNnSc2	zpracování
na	na	k7c4	na
biopaliva	biopaliv	k1gMnSc4	biopaliv
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
využití	využití	k1gNnSc1	využití
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
fázi	fáze	k1gFnSc6	fáze
vývoje	vývoj	k1gInSc2	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
na	na	k7c6	na
rybnících	rybník	k1gInPc6	rybník
těžily	těžit	k5eAaImAgInP	těžit
bloky	blok	k1gInPc1	blok
ledu	led	k1gInSc2	led
pro	pro	k7c4	pro
ledárny	ledárna	k1gFnPc4	ledárna
<g/>
.	.	kIx.	.
</s>
<s>
Rybníky	rybník	k1gInPc1	rybník
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
využívány	využívat	k5eAaPmNgInP	využívat
k	k	k7c3	k
vodním	vodní	k2eAgInPc3d1	vodní
sportům	sport	k1gInPc3	sport
<g/>
,	,	kIx,	,
vodním	vodní	k2eAgFnPc3d1	vodní
radovánkám	radovánka	k1gFnPc3	radovánka
<g/>
,	,	kIx,	,
sportovnímu	sportovní	k2eAgNnSc3d1	sportovní
rybářství	rybářství	k1gNnSc3	rybářství
<g/>
,	,	kIx,	,
sportovnímu	sportovní	k2eAgInSc3d1	sportovní
lovu	lov	k1gInSc3	lov
ptáků	pták	k1gMnPc2	pták
a	a	k8xC	a
bruslení	bruslení	k1gNnPc2	bruslení
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Rybníky	rybník	k1gInPc1	rybník
pro	pro	k7c4	pro
rekreaci	rekreace	k1gFnSc4	rekreace
jsou	být	k5eAaImIp3nP	být
obhospodařovány	obhospodařován	k2eAgFnPc1d1	obhospodařována
zpravidla	zpravidla	k6eAd1	zpravidla
méně	málo	k6eAd2	málo
intenzivním	intenzivní	k2eAgInSc7d1	intenzivní
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
dobrou	dobrý	k2eAgFnSc4d1	dobrá
kvalitu	kvalita	k1gFnSc4	kvalita
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
funkcí	funkce	k1gFnSc7	funkce
rybníku	rybník	k1gInSc3	rybník
je	být	k5eAaImIp3nS	být
zadržování	zadržování	k1gNnSc1	zadržování
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
krajině	krajina	k1gFnSc6	krajina
<g/>
:	:	kIx,	:
Vodní	vodní	k2eAgFnSc1d1	vodní
kapacita	kapacita	k1gFnSc1	kapacita
rybníku	rybník	k1gInSc3	rybník
vyrovnává	vyrovnávat	k5eAaImIp3nS	vyrovnávat
průtok	průtok	k1gInSc1	průtok
<g/>
.	.	kIx.	.
</s>
<s>
Rybníky	rybník	k1gInPc1	rybník
především	především	k9	především
tlumí	tlumit	k5eAaImIp3nP	tlumit
povodňovou	povodňový	k2eAgFnSc4d1	povodňová
vlnu	vlna	k1gFnSc4	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
rybníky	rybník	k1gInPc1	rybník
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
protipožární	protipožární	k2eAgFnPc4d1	protipožární
nádrže	nádrž	k1gFnPc4	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
rybníky	rybník	k1gInPc1	rybník
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nenechávají	nechávat	k5eNaImIp3nP	nechávat
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
vypouštěné	vypouštěný	k2eAgFnPc1d1	vypouštěná
<g/>
.	.	kIx.	.
</s>
<s>
Rybníky	rybník	k1gInPc1	rybník
jsou	být	k5eAaImIp3nP	být
meliorační	meliorační	k2eAgFnSc7d1	meliorační
stavbou	stavba	k1gFnSc7	stavba
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
zamokřeném	zamokřený	k2eAgNnSc6d1	zamokřené
území	území	k1gNnSc6	území
stahují	stahovat	k5eAaImIp3nP	stahovat
vodu	voda	k1gFnSc4	voda
do	do	k7c2	do
jednoho	jeden	k4xCgNnSc2	jeden
místa	místo	k1gNnSc2	místo
výtoku	výtok	k1gInSc2	výtok
z	z	k7c2	z
rybníku	rybník	k1gInSc6	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Biologické	biologický	k2eAgInPc1d1	biologický
pochody	pochod	k1gInPc1	pochod
v	v	k7c6	v
rybníku	rybník	k1gInSc6	rybník
mohou	moct	k5eAaImIp3nP	moct
zlepšovat	zlepšovat	k5eAaImF	zlepšovat
kvalitu	kvalita	k1gFnSc4	kvalita
protékající	protékající	k2eAgFnSc2d1	protékající
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
výše	vysoce	k6eAd2	vysoce
zmiňované	zmiňovaný	k2eAgInPc4d1	zmiňovaný
biologické	biologický	k2eAgInPc4d1	biologický
rybníky	rybník	k1gInPc4	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Vypuštění	vypuštění	k1gNnSc1	vypuštění
a	a	k8xC	a
výlov	výlov	k1gInSc1	výlov
rybníka	rybník	k1gInSc2	rybník
Loviště	loviště	k1gNnSc2	loviště
je	být	k5eAaImIp3nS	být
odbahněná	odbahněný	k2eAgFnSc1d1	odbahněný
jáma	jáma	k1gFnSc1	jáma
v	v	k7c6	v
nejhlubší	hluboký	k2eAgFnSc6d3	nejhlubší
části	část	k1gFnSc6	část
rybníka	rybník	k1gInSc2	rybník
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
zpravidla	zpravidla	k6eAd1	zpravidla
tvar	tvar	k1gInSc4	tvar
kvádru	kvádr	k1gInSc2	kvádr
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vypuštění	vypuštění	k1gNnSc6	vypuštění
rybníka	rybník	k1gInSc2	rybník
se	se	k3xPyFc4	se
ryby	ryba	k1gFnPc1	ryba
hromadí	hromadit	k5eAaImIp3nP	hromadit
v	v	k7c6	v
lovišti	loviště	k1gNnSc6	loviště
<g/>
.	.	kIx.	.
</s>
<s>
Loviště	loviště	k1gNnSc1	loviště
se	se	k3xPyFc4	se
nevypouští	vypouštět	k5eNaImIp3nS	vypouštět
<g/>
.	.	kIx.	.
</s>
<s>
Kádiště	Kádiště	k1gNnSc1	Kádiště
je	být	k5eAaImIp3nS	být
zpevněné	zpevněný	k2eAgNnSc1d1	zpevněné
místo	místo	k1gNnSc1	místo
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
loviště	loviště	k1gNnSc2	loviště
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
vytažené	vytažený	k2eAgFnPc1d1	vytažená
ryby	ryba	k1gFnPc1	ryba
manipulovány	manipulován	k2eAgFnPc1d1	manipulována
<g/>
.	.	kIx.	.
</s>
<s>
Kádiště	Kádiště	k1gNnSc1	Kádiště
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
korunou	koruna	k1gFnSc7	koruna
hráze	hráz	k1gFnSc2	hráz
spojeno	spojit	k5eAaPmNgNnS	spojit
schody	schod	k1gInPc4	schod
(	(	kIx(	(
<g/>
malé	malý	k2eAgInPc1d1	malý
rybníky	rybník	k1gInPc1	rybník
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
sjezdem	sjezd	k1gInSc7	sjezd
a	a	k8xC	a
výjezdem	výjezd	k1gInSc7	výjezd
pro	pro	k7c4	pro
nákladní	nákladní	k2eAgInPc4d1	nákladní
automobily	automobil	k1gInPc4	automobil
(	(	kIx(	(
<g/>
velké	velký	k2eAgInPc1d1	velký
rybníky	rybník	k1gInPc1	rybník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bort	bort	k1gInSc1	bort
je	být	k5eAaImIp3nS	být
zpevněný	zpevněný	k2eAgInSc4d1	zpevněný
okraj	okraj	k1gInSc4	okraj
loviště	loviště	k1gNnSc2	loviště
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
stok	stok	k1gInSc1	stok
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
rybníka	rybník	k1gInSc2	rybník
brání	bránit	k5eAaImIp3nS	bránit
vzniku	vznik	k1gInSc3	vznik
tůní	tůně	k1gFnPc2	tůně
při	při	k7c6	při
vypouštění	vypouštění	k1gNnSc6	vypouštění
<g/>
,	,	kIx,	,
urychluje	urychlovat	k5eAaImIp3nS	urychlovat
vysychání	vysychání	k1gNnSc4	vysychání
dna	dno	k1gNnSc2	dno
při	při	k7c6	při
letnění	letnění	k1gNnSc6	letnění
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
vodoteče	vodoteč	k1gFnPc4	vodoteč
ve	v	k7c6	v
vypuštěném	vypuštěný	k2eAgInSc6d1	vypuštěný
rybníku	rybník	k1gInSc6	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Výpusť	výpusť	k1gFnSc1	výpusť
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vypuštění	vypuštění	k1gNnSc4	vypuštění
a	a	k8xC	a
napuštění	napuštění	k1gNnSc6	napuštění
rybníku	rybník	k1gInSc3	rybník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
různá	různý	k2eAgNnPc1d1	různé
konstrukční	konstrukční	k2eAgNnPc1d1	konstrukční
řešení	řešení	k1gNnPc1	řešení
(	(	kIx(	(
<g/>
stavidlo	stavidlo	k1gNnSc1	stavidlo
<g/>
,	,	kIx,	,
čap	čap	k1gInSc1	čap
<g/>
,	,	kIx,	,
požerák	požerák	k1gInSc1	požerák
<g/>
,	,	kIx,	,
lopata	lopata	k1gFnSc1	lopata
<g/>
,	,	kIx,	,
čerpadlo	čerpadlo	k1gNnSc1	čerpadlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kaberna	kaberna	k1gFnSc1	kaberna
(	(	kIx(	(
<g/>
též	též	k9	též
očapí	očapí	k1gNnSc1	očapí
<g/>
)	)	kIx)	)
je	on	k3xPp3gNnSc4	on
brlení	brlení	k1gNnSc4	brlení
ohrazují	ohrazovat	k5eAaImIp3nP	ohrazovat
čap	čap	k1gInSc4	čap
<g/>
.	.	kIx.	.
</s>
<s>
Hráz	hráz	k1gFnSc1	hráz
rybníka	rybník	k1gInSc2	rybník
Hráz	hráz	k1gFnSc1	hráz
je	být	k5eAaImIp3nS	být
postavena	postaven	k2eAgFnSc1d1	postavena
z	z	k7c2	z
místních	místní	k2eAgInPc2d1	místní
přírodních	přírodní	k2eAgInPc2d1	přírodní
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Vzdušná	vzdušný	k2eAgFnSc1d1	vzdušná
strana	strana	k1gFnSc1	strana
hráze	hráze	k1gFnSc1	hráze
a	a	k8xC	a
koruna	koruna	k1gFnSc1	koruna
je	být	k5eAaImIp3nS	být
zpevněna	zpevněn	k2eAgFnSc1d1	zpevněna
duby	dub	k1gInPc7	dub
a	a	k8xC	a
další	další	k2eAgFnSc7d1	další
vegetací	vegetace	k1gFnSc7	vegetace
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
hrázi	hráz	k1gFnSc6	hráz
obvykle	obvykle	k6eAd1	obvykle
vede	vést	k5eAaImIp3nS	vést
cesta	cesta	k1gFnSc1	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Berma	berma	k1gFnSc1	berma
je	být	k5eAaImIp3nS	být
nápadné	nápadný	k2eAgNnSc4d1	nápadné
zesílení	zesílení	k1gNnSc4	zesílení
hráze	hráz	k1gFnSc2	hráz
u	u	k7c2	u
paty	pata	k1gFnSc2	pata
<g/>
.	.	kIx.	.
</s>
<s>
Bezpečnostní	bezpečnostní	k2eAgInSc1d1	bezpečnostní
přeliv	přeliv	k1gInSc1	přeliv
(	(	kIx(	(
<g/>
jalový	jalový	k2eAgInSc1d1	jalový
splav	splav	k1gInSc1	splav
<g/>
)	)	kIx)	)
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
přelití	přelití	k1gNnSc1	přelití
vody	voda	k1gFnSc2	voda
přes	přes	k7c4	přes
korunu	koruna	k1gFnSc4	koruna
a	a	k8xC	a
následné	následný	k2eAgNnSc4d1	následné
protržení	protržení	k1gNnSc4	protržení
hráze	hráz	k1gFnSc2	hráz
<g/>
.	.	kIx.	.
</s>
<s>
Česle	česle	k1gFnPc1	česle
znemožňují	znemožňovat	k5eAaImIp3nP	znemožňovat
vyplavení	vyplavení	k1gNnSc1	vyplavení
ryb	ryba	k1gFnPc2	ryba
bezpečnostním	bezpečnostní	k2eAgInSc7d1	bezpečnostní
přelivem	přeliv	k1gInSc7	přeliv
<g/>
.	.	kIx.	.
</s>
<s>
Odtokové	odtokový	k2eAgNnSc1d1	odtokové
potrubí	potrubí	k1gNnSc1	potrubí
odvádí	odvádět	k5eAaImIp3nS	odvádět
vodu	voda	k1gFnSc4	voda
z	z	k7c2	z
rybníka	rybník	k1gInSc2	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Vzácně	vzácně	k6eAd1	vzácně
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
ražená	ražený	k2eAgFnSc1d1	ražená
odtoková	odtokový	k2eAgFnSc1d1	odtoková
štola	štola	k1gFnSc1	štola
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vývařišti	vývařiště	k1gNnSc6	vývařiště
se	se	k3xPyFc4	se
tlumí	tlumit	k5eAaImIp3nS	tlumit
kinetická	kinetický	k2eAgFnSc1d1	kinetická
energie	energie	k1gFnSc1	energie
vytékající	vytékající	k2eAgFnSc2d1	vytékající
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Napájení	napájení	k1gNnSc4	napájení
rybníky	rybník	k1gInPc4	rybník
a	a	k8xC	a
plocha	plocha	k1gFnSc1	plocha
rybníka	rybník	k1gInSc2	rybník
Náhon	náhon	k1gInSc1	náhon
přivádí	přivádět	k5eAaImIp3nS	přivádět
vodu	voda	k1gFnSc4	voda
k	k	k7c3	k
rybníku	rybník	k1gInSc3	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Náhon	náhon	k1gInSc1	náhon
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
délku	délka	k1gFnSc4	délka
až	až	k6eAd1	až
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
kilometrů	kilometr	k1gInPc2	kilometr
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
stoka	stoka	k1gFnSc1	stoka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obvodová	obvodový	k2eAgFnSc1d1	obvodová
stoka	stoka	k1gFnSc1	stoka
odkládání	odkládání	k1gNnSc2	odkládání
část	část	k1gFnSc4	část
přitékající	přitékající	k2eAgFnSc2d1	přitékající
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vůbec	vůbec	k9	vůbec
nedostane	dostat	k5eNaPmIp3nS	dostat
do	do	k7c2	do
rybníka	rybník	k1gInSc2	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Důvody	důvod	k1gInPc1	důvod
stavby	stavba	k1gFnSc2	stavba
obvodové	obvodový	k2eAgFnSc2d1	obvodová
hráze	hráz	k1gFnSc2	hráz
jsou	být	k5eAaImIp3nP	být
zpomalení	zpomalení	k1gNnPc4	zpomalení
výměny	výměna	k1gFnSc2	výměna
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
ochrana	ochrana	k1gFnSc1	ochrana
před	před	k7c7	před
povodní	povodeň	k1gFnSc7	povodeň
<g/>
,	,	kIx,	,
snížení	snížení	k1gNnSc4	snížení
množství	množství	k1gNnSc2	množství
naplavenin	naplavenina	k1gFnPc2	naplavenina
a	a	k8xC	a
zabránění	zabránění	k1gNnSc4	zabránění
splavování	splavování	k1gNnSc2	splavování
hnojiv	hnojivo	k1gNnPc2	hnojivo
z	z	k7c2	z
okolních	okolní	k2eAgFnPc2d1	okolní
polí	pole	k1gFnPc2	pole
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
rybníka	rybník	k1gInSc2	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Zátopa	zátopa	k1gFnSc1	zátopa
jsou	být	k5eAaImIp3nP	být
zaplavené	zaplavený	k2eAgInPc1d1	zaplavený
pozemky	pozemek	k1gInPc1	pozemek
při	při	k7c6	při
běžném	běžný	k2eAgNnSc6d1	běžné
vzdutí	vzdutí	k1gNnSc6	vzdutí
hladiny	hladina	k1gFnSc2	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Výtopa	výtopa	k1gFnSc1	výtopa
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
další	další	k2eAgInPc4d1	další
pozemky	pozemek	k1gInPc4	pozemek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
vyloučeno	vyloučit	k5eAaPmNgNnS	vyloučit
jiné	jiný	k2eAgNnSc1d1	jiné
hospodářské	hospodářský	k2eAgNnSc1d1	hospodářské
použití	použití	k1gNnSc1	použití
<g/>
.	.	kIx.	.
</s>
<s>
Pozemky	pozemka	k1gFnPc1	pozemka
ve	v	k7c6	v
výtopě	výtopa	k1gFnSc6	výtopa
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
silně	silně	k6eAd1	silně
podmáčené	podmáčený	k2eAgInPc4d1	podmáčený
<g/>
,	,	kIx,	,
zatopené	zatopený	k2eAgInPc4d1	zatopený
při	při	k7c6	při
vyšším	vysoký	k2eAgNnSc6d2	vyšší
vzdutí	vzdutí	k1gNnSc6	vzdutí
hladiny	hladina	k1gFnSc2	hladina
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Katastrální	katastrální	k2eAgNnSc1d1	katastrální
území	území	k1gNnSc1	území
rybníka	rybník	k1gInSc2	rybník
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
obsahovat	obsahovat	k5eAaImF	obsahovat
celou	celý	k2eAgFnSc4d1	celá
výtopu	výtopa	k1gFnSc4	výtopa
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářské	hospodářský	k2eAgInPc1d1	hospodářský
zásahy	zásah	k1gInPc1	zásah
Při	při	k7c6	při
letnění	letnění	k1gNnSc6	letnění
a	a	k8xC	a
zimování	zimování	k1gNnSc6	zimování
se	se	k3xPyFc4	se
rybník	rybník	k1gInSc1	rybník
nechává	nechávat	k5eAaImIp3nS	nechávat
vypuštěný	vypuštěný	k2eAgInSc1d1	vypuštěný
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
odbahňování	odbahňování	k1gNnSc6	odbahňování
se	se	k3xPyFc4	se
odstraňují	odstraňovat	k5eAaImIp3nP	odstraňovat
nežádoucí	žádoucí	k2eNgInPc1d1	nežádoucí
sedimenty	sediment	k1gInPc1	sediment
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vápnění	vápnění	k1gNnSc3	vápnění
rybníků	rybník	k1gInPc2	rybník
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pálené	pálený	k2eAgNnSc4d1	pálené
vápno	vápno	k1gNnSc4	vápno
nebo	nebo	k8xC	nebo
mletý	mletý	k2eAgInSc4d1	mletý
vápenec	vápenec	k1gInSc4	vápenec
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
úživnosti	úživnost	k1gFnSc2	úživnost
se	se	k3xPyFc4	se
rybníky	rybník	k1gInPc7	rybník
hnojí	hnojit	k5eAaImIp3nP	hnojit
nebo	nebo	k8xC	nebo
přikrmují	přikrmovat	k5eAaImIp3nP	přikrmovat
<g/>
.	.	kIx.	.
</s>
<s>
Pomůcky	pomůcka	k1gFnPc4	pomůcka
používané	používaný	k2eAgFnPc4d1	používaná
při	při	k7c6	při
výlovu	výlov	k1gInSc6	výlov
Nádobí	nádobí	k1gNnSc2	nádobí
je	být	k5eAaImIp3nS	být
souhrnný	souhrnný	k2eAgInSc1d1	souhrnný
výraz	výraz	k1gInSc1	výraz
pro	pro	k7c4	pro
všechno	všechen	k3xTgNnSc4	všechen
náčiní	náčiní	k1gNnSc4	náčiní
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
při	při	k7c6	při
výlovu	výlov	k1gInSc6	výlov
<g/>
.	.	kIx.	.
</s>
<s>
Plot	plot	k1gInSc1	plot
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
síť	síť	k1gFnSc1	síť
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
brání	bránit	k5eAaImIp3nS	bránit
rybám	ryba	k1gFnPc3	ryba
v	v	k7c6	v
úniku	únik	k1gInSc6	únik
z	z	k7c2	z
loviště	loviště	k1gNnSc2	loviště
<g/>
.	.	kIx.	.
</s>
<s>
Klika	klika	k1gFnSc1	klika
je	být	k5eAaImIp3nS	být
tyč	tyč	k1gFnSc1	tyč
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zaráží	zarážet	k5eAaImIp3nS	zarážet
do	do	k7c2	do
rybničního	rybniční	k2eAgNnSc2d1	rybniční
dna	dno	k1gNnSc2	dno
<g/>
.	.	kIx.	.
</s>
<s>
Nevod	nevod	k1gInSc1	nevod
Ferule	ferule	k1gFnSc2	ferule
(	(	kIx(	(
<g/>
též	též	k9	též
rybářské	rybářský	k2eAgNnSc4d1	rybářské
právo	právo	k1gNnSc4	právo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
odznak	odznak	k1gInSc1	odznak
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
při	při	k7c6	při
tradičních	tradiční	k2eAgInPc6d1	tradiční
výlovech	výlov	k1gInPc6	výlov
<g/>
.	.	kIx.	.
</s>
<s>
Bidlo	bidlo	k1gNnSc1	bidlo
je	být	k5eAaImIp3nS	být
tyč	tyč	k1gFnSc1	tyč
<g/>
.	.	kIx.	.
</s>
<s>
Údery	úder	k1gInPc1	úder
bidla	bidlo	k1gNnSc2	bidlo
o	o	k7c4	o
vodu	voda	k1gFnSc4	voda
se	se	k3xPyFc4	se
plaší	plašit	k5eAaImIp3nP	plašit
ryby	ryba	k1gFnPc1	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Mechanickým	mechanický	k2eAgInSc7d1	mechanický
keserem	keser	k1gInSc7	keser
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
vytahují	vytahovat	k5eAaImIp3nP	vytahovat
ryby	ryba	k1gFnPc1	ryba
z	z	k7c2	z
loviště	loviště	k1gNnSc2	loviště
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
z	z	k7c2	z
loviště	loviště	k1gNnSc2	loviště
ryby	ryba	k1gFnSc2	ryba
ručně	ručně	k6eAd1	ručně
vybíraly	vybírat	k5eAaImAgFnP	vybírat
do	do	k7c2	do
řešátek	řešátko	k1gNnPc2	řešátko
<g/>
.	.	kIx.	.
</s>
<s>
Rybníky	rybník	k1gInPc1	rybník
mají	mít	k5eAaImIp3nP	mít
po	po	k7c6	po
stránce	stránka	k1gFnSc6	stránka
biologické	biologický	k2eAgFnSc2d1	biologická
<g/>
,	,	kIx,	,
krajinářské	krajinářský	k2eAgFnSc2d1	krajinářská
i	i	k8xC	i
estetické	estetický	k2eAgFnSc2d1	estetická
mimořádný	mimořádný	k2eAgInSc4d1	mimořádný
význam	význam	k1gInSc4	význam
v	v	k7c6	v
intenzivně	intenzivně	k6eAd1	intenzivně
zemědělsky	zemědělsky	k6eAd1	zemědělsky
využívané	využívaný	k2eAgFnSc6d1	využívaná
krajině	krajina	k1gFnSc6	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Bývají	bývat	k5eAaImIp3nP	bývat
významným	významný	k2eAgNnSc7d1	významné
hnízdištěm	hnízdiště	k1gNnSc7	hnízdiště
vodního	vodní	k2eAgNnSc2d1	vodní
ptactva	ptactvo	k1gNnSc2	ptactvo
a	a	k8xC	a
důležitou	důležitý	k2eAgFnSc7d1	důležitá
zastávkou	zastávka	k1gFnSc7	zastávka
tažných	tažný	k2eAgMnPc2d1	tažný
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
okolí	okolí	k1gNnSc6	okolí
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
mnoho	mnoho	k4c1	mnoho
významných	významný	k2eAgInPc2d1	významný
chráněných	chráněný	k2eAgInPc2d1	chráněný
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
např.	např.	kA	např.
bekasina	bekasina	k1gFnSc1	bekasina
otavní	otavní	k2eAgFnSc1d1	otavní
<g/>
,	,	kIx,	,
bukáček	bukáček	k1gMnSc1	bukáček
malý	malý	k1gMnSc1	malý
<g/>
,	,	kIx,	,
čáp	čáp	k1gMnSc1	čáp
černý	černý	k1gMnSc1	černý
a	a	k8xC	a
čáp	čáp	k1gMnSc1	čáp
bílý	bílý	k1gMnSc1	bílý
<g/>
,	,	kIx,	,
volavka	volavka	k1gFnSc1	volavka
popelavá	popelavý	k2eAgFnSc1d1	popelavá
<g/>
,	,	kIx,	,
hohol	hohol	k1gMnSc1	hohol
severní	severní	k2eAgFnSc2d1	severní
<g/>
,	,	kIx,	,
chřástal	chřástal	k1gMnSc1	chřástal
vodní	vodní	k2eAgFnSc2d1	vodní
<g/>
,	,	kIx,	,
orlovec	orlovec	k1gInSc1	orlovec
říční	říční	k2eAgInSc1d1	říční
<g/>
,	,	kIx,	,
rákosník	rákosník	k1gInSc1	rákosník
velký	velký	k2eAgInSc1d1	velký
<g/>
,	,	kIx,	,
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
druhů	druh	k1gInPc2	druh
kachen	kachna	k1gFnPc2	kachna
<g/>
.	.	kIx.	.
</s>
<s>
Najdeme	najít	k5eAaPmIp1nP	najít
zde	zde	k6eAd1	zde
i	i	k9	i
raka	rak	k1gMnSc2	rak
říčního	říční	k2eAgMnSc2d1	říční
<g/>
,	,	kIx,	,
čolka	čolek	k1gMnSc4	čolek
horského	horský	k2eAgMnSc4d1	horský
<g/>
,	,	kIx,	,
ropuchu	ropucha	k1gFnSc4	ropucha
obecnou	obecný	k2eAgFnSc4d1	obecná
i	i	k9	i
ropuchu	ropucha	k1gFnSc4	ropucha
zelenou	zelený	k2eAgFnSc4d1	zelená
<g/>
,	,	kIx,	,
rosničku	rosnička	k1gFnSc4	rosnička
zelenou	zelený	k2eAgFnSc4d1	zelená
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
druhy	druh	k1gInPc4	druh
obojživelníků	obojživelník	k1gMnPc2	obojživelník
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
rostlin	rostlina	k1gFnPc2	rostlina
také	také	k9	také
např.	např.	kA	např.
kotvice	kotvice	k1gFnSc1	kotvice
plovoucí	plovoucí	k2eAgFnSc1d1	plovoucí
<g/>
.	.	kIx.	.
</s>
<s>
Rybník	rybník	k1gInSc1	rybník
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
rozmanitý	rozmanitý	k2eAgInSc4d1	rozmanitý
ekosystém	ekosystém	k1gInSc4	ekosystém
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
čele	čelo	k1gNnSc6	čelo
stojí	stát	k5eAaImIp3nP	stát
konzumenti	konzument	k1gMnPc1	konzument
jako	jako	k8xS	jako
štika	štika	k1gFnSc1	štika
<g/>
,	,	kIx,	,
a	a	k8xC	a
dole	dole	k6eAd1	dole
v	v	k7c6	v
pomyslné	pomyslný	k2eAgFnSc6d1	pomyslná
pyramidě	pyramida	k1gFnSc6	pyramida
jsou	být	k5eAaImIp3nP	být
řasy	řasa	k1gFnPc1	řasa
<g/>
,	,	kIx,	,
plankton	plankton	k1gInSc1	plankton
<g/>
,	,	kIx,	,
a	a	k8xC	a
nejspodněji	spodně	k6eAd3	spodně
anorganická	anorganický	k2eAgFnSc1d1	anorganická
příroda	příroda	k1gFnSc1	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
však	však	k9	však
rybník	rybník	k1gInSc1	rybník
dílem	dílem	k6eAd1	dílem
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
zdaleka	zdaleka	k6eAd1	zdaleka
tak	tak	k6eAd1	tak
vyrovnaný	vyrovnaný	k2eAgInSc4d1	vyrovnaný
ekosystém	ekosystém	k1gInSc4	ekosystém
jako	jako	k8xC	jako
horské	horský	k2eAgFnPc4d1	horská
říčky	říčka	k1gFnPc4	říčka
či	či	k8xC	či
přírodní	přírodní	k2eAgNnPc4d1	přírodní
jezera	jezero	k1gNnPc4	jezero
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
také	také	k9	také
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
stává	stávat	k5eAaImIp3nS	stávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
některé	některý	k3yIgInPc1	některý
rybníky	rybník	k1gInPc1	rybník
zarostlé	zarostlý	k2eAgNnSc4d1	zarostlé
rákosím	rákosí	k1gNnSc7	rákosí
či	či	k8xC	či
obsazené	obsazený	k2eAgFnPc1d1	obsazená
sinicemi	sinice	k1gFnPc7	sinice
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
jim	on	k3xPp3gMnPc3	on
chybí	chybět	k5eAaImIp3nS	chybět
přirozený	přirozený	k2eAgMnSc1d1	přirozený
konzument	konzument	k1gMnSc1	konzument
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
o	o	k7c4	o
rybníky	rybník	k1gInPc4	rybník
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jezer	jezero	k1gNnPc2	jezero
pečovat	pečovat	k5eAaImF	pečovat
<g/>
,	,	kIx,	,
čistit	čistit	k5eAaImF	čistit
je	on	k3xPp3gInPc4	on
a	a	k8xC	a
provádět	provádět	k5eAaImF	provádět
výlovy	výlov	k1gInPc4	výlov
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
sahá	sahat	k5eAaImIp3nS	sahat
tradice	tradice	k1gFnPc4	tradice
rybníkářství	rybníkářství	k1gNnPc2	rybníkářství
až	až	k9	až
do	do	k7c2	do
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgNnSc1d1	tradiční
rybníkářskou	rybníkářský	k2eAgFnSc7d1	rybníkářská
oblastí	oblast	k1gFnSc7	oblast
jsou	být	k5eAaImIp3nP	být
jižní	jižní	k2eAgFnPc1d1	jižní
Čechy	Čechy	k1gFnPc1	Čechy
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rybníky	rybník	k1gInPc1	rybník
byly	být	k5eAaImAgFnP	být
zakládány	zakládat	k5eAaImNgFnP	zakládat
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
horských	horský	k2eAgFnPc2d1	horská
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
tak	tak	k9	tak
dělo	dít	k5eAaImAgNnS	dít
jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
českým	český	k2eAgInSc7d1	český
rybníkem	rybník	k1gInSc7	rybník
je	být	k5eAaImIp3nS	být
Rožmberk	Rožmberk	k1gInSc1	Rožmberk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Lužnici	Lužnice	k1gFnSc6	Lužnice
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
severně	severně	k6eAd1	severně
od	od	k7c2	od
Třeboně	Třeboň	k1gFnSc2	Třeboň
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
plochu	plocha	k1gFnSc4	plocha
4,89	[number]	k4	4,89
km2	km2	k4	km2
a	a	k8xC	a
maximální	maximální	k2eAgFnSc4d1	maximální
hloubku	hloubka	k1gFnSc4	hloubka
6	[number]	k4	6
m.	m.	k?	m.
Také	také	k6eAd1	také
další	další	k2eAgInPc1d1	další
velké	velký	k2eAgInPc1d1	velký
rybníky	rybník	k1gInPc1	rybník
leží	ležet	k5eAaImIp3nP	ležet
v	v	k7c6	v
jižních	jižní	k2eAgFnPc6d1	jižní
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
:	:	kIx,	:
Horusický	horusický	k2eAgInSc1d1	horusický
rybník	rybník	k1gInSc1	rybník
(	(	kIx(	(
<g/>
4,16	[number]	k4	4,16
km2	km2	k4	km2
<g/>
)	)	kIx)	)
u	u	k7c2	u
Veselí	veselí	k1gNnSc2	veselí
nad	nad	k7c7	nad
Lužnicí	Lužnice	k1gFnSc7	Lužnice
<g/>
,	,	kIx,	,
Bezdrev	Bezdrev	k1gInSc1	Bezdrev
(	(	kIx(	(
<g/>
3,94	[number]	k4	3,94
km2	km2	k4	km2
<g/>
)	)	kIx)	)
u	u	k7c2	u
Hluboké	Hluboká	k1gFnSc2	Hluboká
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
,	,	kIx,	,
Dvořiště	dvořiště	k1gNnSc1	dvořiště
(	(	kIx(	(
<g/>
3,37	[number]	k4	3,37
<g />
.	.	kIx.	.
</s>
<s>
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Velký	velký	k2eAgMnSc1d1	velký
Tisý	Tisý	k1gMnSc1	Tisý
(	(	kIx(	(
<g/>
3,17	[number]	k4	3,17
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dehtář	dehtář	k1gMnSc1	dehtář
(	(	kIx(	(
<g/>
2,46	[number]	k4	2,46
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Svět	svět	k1gInSc1	svět
(	(	kIx(	(
<g/>
2,15	[number]	k4	2,15
km2	km2	k4	km2
<g/>
)	)	kIx)	)
a	a	k8xC	a
Opatovický	opatovický	k2eAgMnSc1d1	opatovický
(	(	kIx(	(
<g/>
1,65	[number]	k4	1,65
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgInS	být
před	před	k7c7	před
rozdělením	rozdělení	k1gNnSc7	rozdělení
jediný	jediný	k2eAgInSc1d1	jediný
rybník	rybník	k1gInSc1	rybník
(	(	kIx(	(
<g/>
3,80	[number]	k4	3,80
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
Třeboně	Třeboň	k1gFnSc2	Třeboň
<g/>
.	.	kIx.	.
</s>
<s>
Rybník	rybník	k1gInSc1	rybník
Svět	svět	k1gInSc1	svět
má	mít	k5eAaImIp3nS	mít
při	při	k7c6	při
normálním	normální	k2eAgInSc6d1	normální
stavu	stav	k1gInSc6	stav
objem	objem	k1gInSc1	objem
0,33	[number]	k4	0,33
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
povodních	povodeň	k1gFnPc6	povodeň
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
však	však	k9	však
dokázal	dokázat	k5eAaPmAgInS	dokázat
zadržet	zadržet	k5eAaPmF	zadržet
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
10	[number]	k4	10
mil	míle	k1gFnPc2	míle
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
Třeboňsko	Třeboňsko	k1gNnSc4	Třeboňsko
najdeme	najít	k5eAaPmIp1nP	najít
třeba	třeba	k6eAd1	třeba
Bohdanečský	bohdanečský	k2eAgInSc4d1	bohdanečský
v	v	k7c6	v
Polabí	Polabí	k1gNnSc6	Polabí
(	(	kIx(	(
<g/>
2,49	[number]	k4	2,49
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejhlubším	hluboký	k2eAgInSc7d3	nejhlubší
rybníkem	rybník	k1gInSc7	rybník
je	být	k5eAaImIp3nS	být
Máchovo	Máchův	k2eAgNnSc1d1	Máchovo
jezero	jezero	k1gNnSc1	jezero
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
hloubky	hloubka	k1gFnPc4	hloubka
až	až	k9	až
12	[number]	k4	12
metrů	metr	k1gInPc2	metr
(	(	kIx(	(
<g/>
2,84	[number]	k4	2,84
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
O	o	k7c4	o
tento	tento	k3xDgInSc4	tento
primát	primát	k1gInSc4	primát
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
s	s	k7c7	s
táborským	táborský	k2eAgInSc7d1	táborský
Jordánem	Jordán	k1gInSc7	Jordán
(	(	kIx(	(
<g/>
0,5	[number]	k4	0,5
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
celkem	celkem	k6eAd1	celkem
nachází	nacházet	k5eAaImIp3nS	nacházet
okolo	okolo	k7c2	okolo
21	[number]	k4	21
000	[number]	k4	000
rybníků	rybník	k1gInPc2	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
velkých	velký	k2eAgInPc2d1	velký
rybníků	rybník	k1gInPc2	rybník
se	se	k3xPyFc4	se
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
prakticky	prakticky	k6eAd1	prakticky
nemění	měnit	k5eNaImIp3nS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
menší	malý	k2eAgFnPc1d2	menší
však	však	k9	však
zanikají	zanikat	k5eAaImIp3nP	zanikat
<g/>
.	.	kIx.	.
</s>
<s>
Akvakultura	akvakultura	k1gFnSc1	akvakultura
Rybníkářství	rybníkářství	k1gNnSc6	rybníkářství
Největší	veliký	k2eAgInPc4d3	veliký
české	český	k2eAgInPc4d1	český
rybníky	rybník	k1gInPc4	rybník
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
rybník	rybník	k1gInSc4	rybník
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
rybník	rybník	k1gInSc1	rybník
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
