<p>
<s>
Tatankaceratops	Tatankaceratops	k1gInSc1	Tatankaceratops
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
bizoní	bizoní	k2eAgFnSc1d1	bizoní
rohatá	rohatý	k2eAgFnSc1d1	rohatá
tvář	tvář	k1gFnSc1	tvář
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
rod	rod	k1gInSc1	rod
velmi	velmi	k6eAd1	velmi
malého	malý	k2eAgMnSc2d1	malý
rohatého	rohatý	k1gMnSc2	rohatý
dinosaura	dinosaurus	k1gMnSc2	dinosaurus
(	(	kIx(	(
<g/>
ceratopsida	ceratopsid	k1gMnSc2	ceratopsid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žijícího	žijící	k2eAgMnSc2d1	žijící
v	v	k7c6	v
období	období	k1gNnSc6	období
pozdní	pozdní	k2eAgFnSc2d1	pozdní
svrchní	svrchní	k2eAgFnSc2d1	svrchní
křídy	křída	k1gFnSc2	křída
(	(	kIx(	(
<g/>
geologický	geologický	k2eAgInSc1d1	geologický
stupeň	stupeň	k1gInSc1	stupeň
maastricht	maastricht	k1gInSc1	maastricht
<g/>
,	,	kIx,	,
asi	asi	k9	asi
před	před	k7c7	před
68	[number]	k4	68
až	až	k8xS	až
66	[number]	k4	66
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgInSc2d1	dnešní
státu	stát	k1gInSc2	stát
Jižní	jižní	k2eAgFnSc1d1	jižní
Dakota	Dakota	k1gFnSc1	Dakota
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Fosilie	fosilie	k1gFnPc1	fosilie
tohoto	tento	k3xDgMnSc2	tento
býložravého	býložravý	k2eAgMnSc2d1	býložravý
dinosaura	dinosaurus	k1gMnSc2	dinosaurus
byly	být	k5eAaImAgFnP	být
objeveny	objeven	k2eAgFnPc1d1	objevena
v	v	k7c6	v
sedimentech	sediment	k1gInPc6	sediment
souvrství	souvrství	k1gNnSc2	souvrství
Hell	Hell	k1gMnSc1	Hell
Creek	Creek	k1gMnSc1	Creek
<g/>
.	.	kIx.	.
</s>
<s>
Objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
však	však	k9	však
domněnky	domněnka	k1gFnPc1	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
spíše	spíše	k9	spíše
o	o	k7c4	o
mladý	mladý	k2eAgInSc4d1	mladý
exemplář	exemplář	k1gInSc4	exemplář
rodu	rod	k1gInSc2	rod
Triceratops	Triceratopsa	k1gFnPc2	Triceratopsa
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
o	o	k7c4	o
samostatný	samostatný	k2eAgInSc4d1	samostatný
rod	rod	k1gInSc4	rod
<g/>
.	.	kIx.	.
</s>
<s>
Odhadovaná	odhadovaný	k2eAgFnSc1d1	odhadovaná
velikost	velikost	k1gFnSc1	velikost
tohoto	tento	k3xDgMnSc2	tento
malého	malý	k2eAgMnSc2d1	malý
ceratopsida	ceratopsid	k1gMnSc2	ceratopsid
činí	činit	k5eAaImIp3nS	činit
pouze	pouze	k6eAd1	pouze
asi	asi	k9	asi
1	[number]	k4	1
metr	metr	k1gInSc4	metr
<g/>
,	,	kIx,	,
šlo	jít	k5eAaImAgNnS	jít
by	by	kYmCp3nS	by
tedy	tedy	k9	tedy
o	o	k7c4	o
trpasličí	trpasličí	k2eAgInSc4d1	trpasličí
rod	rod	k1gInSc4	rod
ceratopsida	ceratopsid	k1gMnSc2	ceratopsid
<g/>
.	.	kIx.	.
</s>
<s>
Typový	typový	k2eAgMnSc1d1	typový
a	a	k8xC	a
dosud	dosud	k6eAd1	dosud
jediný	jediný	k2eAgInSc1d1	jediný
známý	známý	k2eAgInSc1d1	známý
druh	druh	k1gInSc1	druh
T.	T.	kA	T.
sacrisonorum	sacrisonorum	k1gInSc1	sacrisonorum
byl	být	k5eAaImAgInS	být
popsán	popsat	k5eAaPmNgInS	popsat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Christopher	Christophra	k1gFnPc2	Christophra
J.	J.	kA	J.
Ott	Ott	k1gMnSc1	Ott
and	and	k?	and
Peter	Peter	k1gMnSc1	Peter
L.	L.	kA	L.
Larson	Larson	k1gMnSc1	Larson
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
A	a	k9	a
New	New	k1gMnSc1	New
<g/>
,	,	kIx,	,
Small	Small	k1gMnSc1	Small
Ceratopsian	Ceratopsian	k1gMnSc1	Ceratopsian
Dinosaur	Dinosaur	k1gMnSc1	Dinosaur
from	from	k1gMnSc1	from
the	the	k?	the
Latest	Latest	k1gMnSc1	Latest
Cretaceous	Cretaceous	k1gMnSc1	Cretaceous
Hell	Hell	k1gMnSc1	Hell
Creek	Creek	k1gMnSc1	Creek
Formation	Formation	k1gInSc1	Formation
<g/>
,	,	kIx,	,
Northwest	Northwest	k1gInSc1	Northwest
South	South	k1gInSc1	South
Dakota	Dakota	k1gFnSc1	Dakota
<g/>
,	,	kIx,	,
United	United	k1gMnSc1	United
States	States	k1gMnSc1	States
<g/>
:	:	kIx,	:
A	a	k8xC	a
Preliminary	Preliminara	k1gFnPc1	Preliminara
Description	Description	k1gInSc1	Description
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
In	In	k1gMnSc1	In
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
Ryan	Ryan	k1gMnSc1	Ryan
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
J.	J.	kA	J.
<g/>
,	,	kIx,	,
Chinnery-Allgeier	Chinnery-Allgeier	k1gMnSc1	Chinnery-Allgeier
<g/>
,	,	kIx,	,
B.J.	B.J.	k1gMnSc1	B.J.
<g/>
,	,	kIx,	,
and	and	k?	and
Eberth	Eberth	k1gInSc1	Eberth
<g/>
,	,	kIx,	,
D.A.	D.A.	k1gFnSc1	D.A.
(	(	kIx(	(
<g/>
eds	eds	k?	eds
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
New	New	k1gFnSc1	New
Perspectives	Perspectives	k1gInSc4	Perspectives
on	on	k3xPp3gMnSc1	on
Horned	Horned	k1gMnSc1	Horned
Dinosaurs	Dinosaursa	k1gFnPc2	Dinosaursa
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Royal	Royal	k1gInSc4	Royal
Tyrrell	Tyrrell	k1gInSc1	Tyrrell
Museum	museum	k1gNnSc1	museum
Ceratopsian	Ceratopsiana	k1gFnPc2	Ceratopsiana
Symposium	symposium	k1gNnSc1	symposium
<g/>
,	,	kIx,	,
Bloomington	Bloomington	k1gInSc1	Bloomington
<g/>
,	,	kIx,	,
Indiana	Indiana	k1gFnSc1	Indiana
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
656	[number]	k4	656
pp	pp	k?	pp
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
článek	článek	k1gInSc1	článek
o	o	k7c6	o
objevu	objev	k1gInSc6	objev
tatankaceratopse	tatankaceratopse	k1gFnSc2	tatankaceratopse
na	na	k7c6	na
webu	web	k1gInSc6	web
DinosaurusBlog	DinosaurusBloga	k1gFnPc2	DinosaurusBloga
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
