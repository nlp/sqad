<s>
Josef	Josef	k1gMnSc1
Stanislav	Stanislav	k1gMnSc1
Menšík	Menšík	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Josef	Josef	k1gMnSc1
Stanislav	Stanislav	k1gMnSc1
Menšík	Menšík	k1gMnSc1
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1863	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
51	#num#	k4
<g/>
–	–	k?
<g/>
52	#num#	k4
let	let	k1gInSc1
<g/>
)	)	kIx)
Brno	Brno	k1gNnSc1
Rakouské	rakouský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
Rakouské	rakouský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
spisovatel	spisovatel	k1gMnSc1
Národnost	národnost	k1gFnSc1
</s>
<s>
Moravané	Moravan	k1gMnPc1
</s>
<s>
původní	původní	k2eAgInPc4d1
texty	text	k1gInPc4
na	na	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Josef	Josef	k1gMnSc1
Stanislav	Stanislav	k1gMnSc1
Menšík	Menšík	k1gMnSc1
(	(	kIx(
<g/>
asi	asi	k9
1811	#num#	k4
<g/>
?	?	kIx.
–	–	k?
6	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1862	#num#	k4
Brno	Brno	k1gNnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
moravský	moravský	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
<g/>
,	,	kIx,
sběratel	sběratel	k1gMnSc1
pohádek	pohádka	k1gFnPc2
a	a	k8xC
pověstí	pověst	k1gFnSc7
a	a	k8xC
národní	národní	k2eAgMnSc1d1
buditel	buditel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Kdy	kdy	k6eAd1
a	a	k8xC
kde	kde	k6eAd1
se	se	k3xPyFc4
Menšík	Menšík	k1gMnSc1
narodil	narodit	k5eAaPmAgMnS
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
známo	znám	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
rok	rok	k1gInSc4
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
narození	narození	k1gNnSc2
je	být	k5eAaImIp3nS
uváděn	uváděn	k2eAgInSc4d1
rok	rok	k1gInSc4
1811	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vychází	vycházet	k5eAaImIp3nS
se	se	k3xPyFc4
při	při	k7c6
tom	ten	k3xDgNnSc6
z	z	k7c2
údajů	údaj	k1gInPc2
uvedených	uvedený	k2eAgInPc2d1
ve	v	k7c6
sčítacích	sčítací	k2eAgInPc6d1
operátech	operát	k1gInPc6
Jemnice	Jemnice	k1gFnSc2
1857	#num#	k4
(	(	kIx(
<g/>
uveden	uveden	k2eAgInSc1d1
rok	rok	k1gInSc1
narození	narození	k1gNnSc1
1811	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
matričního	matriční	k2eAgInSc2d1
záznamu	záznam	k1gInSc2
v	v	k7c6
matrice	matrika	k1gFnSc6
oddaných	oddaný	k2eAgMnPc2d1
-	-	kIx~
při	při	k7c6
svatbě	svatba	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1851	#num#	k4
měl	mít	k5eAaImAgInS
věk	věk	k1gInSc1
40	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
Jemnice	Jemnice	k1gFnSc2
se	se	k3xPyFc4
rodina	rodina	k1gFnSc1
Menšíkova	Menšíkův	k2eAgFnSc1d1
přistěhovala	přistěhovat	k5eAaPmAgFnS
před	před	k7c7
rokem	rok	k1gInSc7
1814	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
si	se	k3xPyFc3
ji	on	k3xPp3gFnSc4
za	za	k7c4
své	své	k1gNnSc4
působiště	působiště	k1gNnSc2
zvolil	zvolit	k5eAaPmAgMnS
Josefův	Josefův	k2eAgMnSc1d1
otec	otec	k1gMnSc1
Jan	Jan	k1gMnSc1
<g/>
,	,	kIx,
krejčovský	krejčovský	k2eAgMnSc1d1
mistr	mistr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nebylo	být	k5eNaImAgNnS
to	ten	k3xDgNnSc1
náhodou	náhodou	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
tam	tam	k6eAd1
již	již	k6eAd1
žili	žít	k5eAaImAgMnP
jeho	jeho	k3xOp3gMnPc1
příbuzní	příbuzný	k1gMnPc1
–	–	k?
sestra	sestra	k1gFnSc1
Josefa	Josefa	k1gFnSc1
(	(	kIx(
<g/>
Jemnice	Jemnice	k1gFnSc1
č.	č.	k?
140	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
bratr	bratr	k1gMnSc1
Václav	Václav	k1gMnSc1
(	(	kIx(
<g/>
Jemnice	Jemnice	k1gFnSc1
č.	č.	k?
135	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Josef	Josef	k1gMnSc1
Stanislav	Stanislav	k1gMnSc1
byl	být	k5eAaImAgMnS
nejstarší	starý	k2eAgMnSc1d3
syn	syn	k1gMnSc1
krejčovského	krejčovský	k2eAgMnSc2d1
mistra	mistr	k1gMnSc2
Jana	Jan	k1gMnSc2
Menšíka	Menšík	k1gMnSc2
a	a	k8xC
Marie	Maria	k1gFnSc2
<g/>
,	,	kIx,
rozené	rozený	k2eAgFnSc2d1
Hronové	Hronová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Jemnici	Jemnice	k1gFnSc6
prožil	prožít	k5eAaPmAgMnS
své	svůj	k3xOyFgNnSc4
dětství	dětství	k1gNnSc4
a	a	k8xC
mládí	mládí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zprvu	zprvu	k6eAd1
rodina	rodina	k1gFnSc1
bydlela	bydlet	k5eAaImAgFnS
na	na	k7c6
domě	dům	k1gInSc6
č.	č.	k?
88	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
Josefův	Josefův	k2eAgMnSc1d1
bratr	bratr	k1gMnSc1
Jan	Jan	k1gMnSc1
Nepomuk	Nepomuk	k1gMnSc1
Ambrož	Ambrož	k1gMnSc1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
.	.	kIx.
12.181	12.181	k4
<g/>
4	#num#	k4
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
,	,	kIx,
zemřel	zemřít	k5eAaPmAgMnS
1815	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
sestra	sestra	k1gFnSc1
Františka	František	k1gMnSc2
Mariana	Marian	k1gMnSc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
4.181	4.181	k4
<g/>
6	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1816	#num#	k4
otec	otec	k1gMnSc1
koupil	koupit	k5eAaPmAgMnS
dům	dům	k1gInSc4
č.	č.	k?
29	#num#	k4
a	a	k8xC
stal	stát	k5eAaPmAgInS
se	se	k3xPyFc4
plnohodnotným	plnohodnotný	k2eAgMnSc7d1
měšťanem	měšťan	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
třem	tři	k4xCgFnPc3
ratolestem	ratolest	k1gFnPc3
rodičům	rodič	k1gMnPc3
přibyli	přibýt	k5eAaPmAgMnP
další	další	k2eAgMnPc1d1
potomci	potomek	k1gMnPc1
<g/>
:	:	kIx,
syn	syn	k1gMnSc1
Jiří	Jiří	k1gMnSc1
(	(	kIx(
<g/>
26	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
1817	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
dcera	dcera	k1gFnSc1
Marie	Marie	k1gFnSc1
Apolonie	Apolonie	k1gFnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
1821	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
matričního	matriční	k2eAgInSc2d1
záznamu	záznam	k1gInSc2
o	o	k7c6
úmrtí	úmrtí	k1gNnSc6
a	a	k8xC
pohřbu	pohřeb	k1gInSc6
zemřel	zemřít	k5eAaPmAgMnS
Menšík	Menšík	k1gMnSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
dne	den	k1gInSc2
6	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
1862	#num#	k4
ve	v	k7c6
věku	věk	k1gInSc6
45	#num#	k4
let	léto	k1gNnPc2
<g/>
;	;	kIx,
podle	podle	k7c2
uvedeného	uvedený	k2eAgInSc2d1
věku	věk	k1gInSc2
by	by	kYmCp3nS
se	se	k3xPyFc4
měl	mít	k5eAaImAgInS
narodit	narodit	k5eAaPmF
ale	ale	k8xC
až	až	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1817	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k9
noviny	novina	k1gFnPc1
Moravan	Moravan	k1gMnSc1
dne	den	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
2.186	2.186	k4
<g/>
2	#num#	k4
přinesly	přinést	k5eAaPmAgFnP
zprávu	zpráva	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
Josef	Josef	k1gMnSc1
Menšík	Menšík	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
6	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
toho	ten	k3xDgInSc2
roku	rok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moravské	moravský	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
začátkem	začátkem	k7c2
dubna	duben	k1gInSc2
1862	#num#	k4
ze	z	k7c2
sezení	sezení	k1gNnSc2
moravského	moravský	k2eAgInSc2d1
zemského	zemský	k2eAgInSc2d1
výboru	výbor	k1gInSc2
sdělovaly	sdělovat	k5eAaImAgInP
<g/>
,	,	kIx,
že	že	k8xS
vdově	vdova	k1gFnSc6
po	po	k7c6
zemřelém	zemřelý	k2eAgMnSc6d1
ingrosistovi	ingrosista	k1gMnSc6
(	(	kIx(
<g/>
kancelářském	kancelářský	k2eAgMnSc6d1
úředníkovi	úředník	k1gMnSc6
<g/>
)	)	kIx)
zemské	zemský	k2eAgFnSc2d1
účtárny	účtárna	k1gFnSc2
Josefu	Josef	k1gMnSc3
Menšíkovi	Menšík	k1gMnSc3
povolila	povolit	k5eAaPmAgFnS
se	se	k3xPyFc4
podpora	podpora	k1gFnSc1
50	#num#	k4
zlatých	zlatá	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgNnSc1d1
vzdělání	vzdělání	k1gNnSc1
Josef	Josef	k1gMnSc1
získal	získat	k5eAaPmAgMnS
v	v	k7c6
Jemnici	Jemnice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
studoval	studovat	k5eAaImAgMnS
na	na	k7c6
gymnáziu	gymnázium	k1gNnSc6
ve	v	k7c6
Znojmě	Znojmo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jaké	jaký	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
vzdělání	vzdělání	k1gNnSc1
skutečně	skutečně	k6eAd1
získal	získat	k5eAaPmAgMnS
<g/>
,	,	kIx,
nevíme	vědět	k5eNaImIp1nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
svého	svůj	k3xOyFgNnSc2
studia	studio	k1gNnSc2
se	se	k3xPyFc4
seznámil	seznámit	k5eAaPmAgMnS
s	s	k7c7
latinským	latinský	k2eAgInSc7d1
spisem	spis	k1gInSc7
Mars	Mars	k1gMnSc1
Moravicus	Moravicus	k1gMnSc1
(	(	kIx(
<g/>
Pragae	Pragae	k1gInSc1
1677	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc1
autor	autor	k1gMnSc1
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
Pešina	Pešina	k1gMnSc1
z	z	k7c2
Čechorodu	Čechorod	k1gInSc2
<g/>
,	,	kIx,
viděl	vidět	k5eAaImAgMnS
počátky	počátek	k1gInPc4
Jemnice	Jemnice	k1gFnSc2
už	už	k9
za	za	k7c2
římských	římský	k2eAgMnPc2d1
časů	čas	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
v	v	k7c6
něm	on	k3xPp3gInSc6
psal	psát	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
za	za	k7c2
knížete	kníže	k1gMnSc2
Svatopluka	Svatopluk	k1gMnSc2
byla	být	k5eAaImAgFnS
Jemnice	Jemnice	k1gFnSc1
velkou	velká	k1gFnSc7
pevností	pevnost	k1gFnSc7
<g/>
,	,	kIx,
za	za	k7c7
jejímiž	jejíž	k3xOyRp3gInPc7
valy	val	k1gInPc7
velkomoravská	velkomoravský	k2eAgNnPc4d1
vojska	vojsko	k1gNnPc4
odrážela	odrážet	k5eAaImAgFnS
útoky	útok	k1gInPc7
Franků	Frank	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
„	„	k?
<g/>
smyšlenky	smyšlenka	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
ovšem	ovšem	k9
prokázalo	prokázat	k5eAaPmAgNnS
až	až	k9
v	v	k7c6
době	doba	k1gFnSc6
pozdější	pozdní	k2eAgMnSc1d2
<g/>
,	,	kIx,
vyvolaly	vyvolat	k5eAaPmAgFnP
v	v	k7c6
duši	duše	k1gFnSc6
vnímavého	vnímavý	k2eAgInSc2d1
<g/>
,	,	kIx,
dozrávajícího	dozrávající	k2eAgMnSc2d1
mladíka	mladík	k1gMnSc2
hrdost	hrdost	k1gFnSc1
k	k	k7c3
městu	město	k1gNnSc3
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yRgInSc6,k3yQgInSc6
prožil	prožít	k5eAaPmAgMnS
dětství	dětství	k1gNnSc4
a	a	k8xC
později	pozdě	k6eAd2
žil	žít	k5eAaImAgMnS
<g/>
,	,	kIx,
pracoval	pracovat	k5eAaImAgMnS
a	a	k8xC
vlastnil	vlastnit	k5eAaImAgMnS
dům	dům	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
v	v	k7c6
titulatuře	titulatura	k1gFnSc6
svých	svůj	k3xOyFgInPc2
spisů	spis	k1gInPc2
používá	používat	k5eAaImIp3nS
přídomek	přídomek	k1gInSc1
„	„	k?
<g/>
měšťan	měšťan	k1gMnSc1
Jemnický	jemnický	k2eAgMnSc1d1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
Menšíka	Menšík	k1gMnSc2
je	být	k5eAaImIp3nS
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1848	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
zpět	zpět	k6eAd1
do	do	k7c2
Jemnice	Jemnice	k1gFnSc2
<g/>
,	,	kIx,
zahalen	zahalit	k5eAaPmNgInS
tajemstvím	tajemství	k1gNnSc7
<g/>
,	,	kIx,
protože	protože	k8xS
větší	veliký	k2eAgFnSc1d2
část	část	k1gFnSc1
svého	svůj	k3xOyFgInSc2
života	život	k1gInSc2
pobýval	pobývat	k5eAaImAgMnS
v	v	k7c6
cizině	cizina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
návrat	návrat	k1gInSc4
v	v	k7c6
tomto	tento	k3xDgInSc6
roce	rok	k1gInSc6
se	se	k3xPyFc4
usuzuje	usuzovat	k5eAaImIp3nS
z	z	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
něm	on	k3xPp3gNnSc6
jako	jako	k8xS,k8xC
důvěrník	důvěrník	k1gMnSc1
72	#num#	k4
Jemnických	jemnický	k2eAgMnPc2d1
měšťanů	měšťan	k1gMnPc2
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
Janem	Jan	k1gMnSc7
Böhmem	Böhm	k1gMnSc7
<g/>
,	,	kIx,
předkládá	předkládat	k5eAaImIp3nS
zemské	zemský	k2eAgFnSc3d1
Vládě	Vláďa	k1gFnSc3
v	v	k7c6
Brně	Brno	k1gNnSc6
prosbu	prosba	k1gFnSc4
o	o	k7c4
rozdělení	rozdělení	k1gNnSc4
městského	městský	k2eAgInSc2d1
lesa	les	k1gInSc2
Javor	javor	k1gInSc1
mezi	mezi	k7c4
jednotlivé	jednotlivý	k2eAgMnPc4d1
sousedy	soused	k1gMnPc4
(	(	kIx(
<g/>
měšťany	měšťan	k1gMnPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prosbě	prosba	k1gFnSc3
nebylo	být	k5eNaImAgNnS
vyhověno	vyhověn	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
mluvil	mluvit	k5eAaImAgMnS
a	a	k8xC
psal	psát	k5eAaImAgMnS
česky	česky	k6eAd1
<g/>
,	,	kIx,
německy	německy	k6eAd1
a	a	k8xC
maďarsky	maďarsky	k6eAd1
<g/>
,	,	kIx,
v	v	k7c6
malé	malý	k2eAgFnSc6d1
městské	městský	k2eAgFnSc6d1
společnosti	společnost	k1gFnSc6
zaujal	zaujmout	k5eAaPmAgInS
relativně	relativně	k6eAd1
slušné	slušný	k2eAgNnSc4d1
místo	místo	k1gNnSc4
-	-	kIx~
stal	stát	k5eAaPmAgInS
se	se	k3xPyFc4
městským	městský	k2eAgMnSc7d1
písařem	písař	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Menšík	Menšík	k1gMnSc1
se	se	k3xPyFc4
do	do	k7c2
Jemnice	Jemnice	k1gFnSc2
nevrátil	vrátit	k5eNaPmAgMnS
sám	sám	k3xTgMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
ním	on	k3xPp3gNnSc7
přišel	přijít	k5eAaPmAgMnS
i	i	k9
pětiletý	pětiletý	k2eAgMnSc1d1
syn	syn	k1gMnSc1
Julius	Julius	k1gMnSc1
(	(	kIx(
<g/>
narozen	narozen	k2eAgMnSc1d1
1843	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
1851	#num#	k4
se	se	k3xPyFc4
znovu	znovu	k6eAd1
oženil	oženit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
další	další	k2eAgFnSc4d1
<g/>
,	,	kIx,
třetí	třetí	k4xOgFnSc1
<g/>
,	,	kIx,
manželkou	manželka	k1gFnSc7
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
Alžběta	Alžběta	k1gFnSc1
<g/>
,	,	kIx,
dcera	dcera	k1gFnSc1
zemřelého	zemřelý	k2eAgMnSc2d1
řeznického	řeznický	k2eAgMnSc2d1
mistra	mistr	k1gMnSc2
v	v	k7c6
Jemnici	Jemnice	k1gFnSc6
Jakuba	Jakub	k1gMnSc2
Sušického	sušický	k2eAgMnSc2d1
a	a	k8xC
jeho	jeho	k3xOp3gFnSc2
manželky	manželka	k1gFnSc2
Františky	Františka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Otec	otec	k1gMnSc1
Jan	Jan	k1gMnSc1
jim	on	k3xPp3gMnPc3
přenechal	přenechat	k5eAaPmAgMnS
dům	dům	k1gInSc4
č.	č.	k?
29	#num#	k4
a	a	k8xC
do	do	k7c2
konce	konec	k1gInSc2
svého	svůj	k3xOyFgInSc2
života	život	k1gInSc2
žil	žít	k5eAaImAgMnS
v	v	k7c6
domě	dům	k1gInSc6
Alžbětiny	Alžbětin	k2eAgFnSc2d1
matky	matka	k1gFnSc2
Františky	Františka	k1gFnSc2
(	(	kIx(
<g/>
č.	č.	k?
47	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
zemřel	zemřít	k5eAaPmAgInS
18	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1853	#num#	k4
ve	v	k7c6
věku	věk	k1gInSc6
76	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
manželství	manželství	k1gNnSc2
se	se	k3xPyFc4
narodila	narodit	k5eAaPmAgFnS
dcera	dcera	k1gFnSc1
Alžběta	Alžběta	k1gFnSc1
(	(	kIx(
<g/>
10	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
1854	#num#	k4
<g/>
)	)	kIx)
<g/>
;	;	kIx,
ve	v	k7c6
sčítacím	sčítací	k2eAgInSc6d1
operátu	operát	k1gInSc6
1857	#num#	k4
je	být	k5eAaImIp3nS
uveden	uveden	k2eAgInSc1d1
mylný	mylný	k2eAgInSc1d1
rok	rok	k1gInSc1
jejího	její	k3xOp3gNnSc2
narození	narození	k1gNnSc2
1853	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Menšík	Menšík	k1gMnSc1
byl	být	k5eAaImAgMnS
známou	známý	k2eAgFnSc7d1
osobností	osobnost	k1gFnSc7
moravského	moravský	k2eAgNnSc2d1
národního	národní	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlásil	hlásit	k5eAaImAgMnS
se	se	k3xPyFc4
vždy	vždy	k6eAd1
k	k	k7c3
moravskému	moravský	k2eAgNnSc3d1
vlastenectví	vlastenectví	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počátky	počátek	k1gInPc4
těchto	tento	k3xDgFnPc2
jeho	jeho	k3xOp3gFnPc2
aktivit	aktivita	k1gFnPc2
spadají	spadat	k5eAaImIp3nP,k5eAaPmIp3nP
do	do	k7c2
roku	rok	k1gInSc2
1849	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
měl	mít	k5eAaImAgInS
velký	velký	k2eAgInSc1d1
podíl	podíl	k1gInSc1
na	na	k7c6
založení	založení	k1gNnSc6
prvního	první	k4xOgInSc2
vlasteneckého	vlastenecký	k2eAgInSc2d1
spolku	spolek	k1gInSc2
ve	v	k7c6
městě	město	k1gNnSc6
s	s	k7c7
názvem	název	k1gInSc7
„	„	k?
<g/>
Čtenářská	čtenářský	k2eAgFnSc1d1
jednota	jednota	k1gFnSc1
sv.	sv.	kA
Víta	Vít	k1gMnSc2
v	v	k7c6
Jemnici	Jemnice	k1gFnSc6
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátký	krátký	k2eAgInSc1d1
čas	čas	k1gInSc1
mu	on	k3xPp3gMnSc3
i	i	k9
předsedal	předsedat	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolku	spolek	k1gInSc2
patřila	patřit	k5eAaImAgFnS
malá	malý	k2eAgFnSc1d1
veřejná	veřejný	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
českých	český	k2eAgFnPc2d1
knih	kniha	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
větší	veliký	k2eAgFnSc1d2
část	část	k1gFnSc4
jednotě	jednota	k1gFnSc6
daroval	darovat	k5eAaPmAgMnS
sám	sám	k3xTgMnSc1
Menšík	Menšík	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolek	spolek	k1gInSc1
údajně	údajně	k6eAd1
zanikl	zaniknout	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1866	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1850	#num#	k4
na	na	k7c6
základě	základ	k1gInSc6
jeho	jeho	k3xOp3gNnSc4
pozvání	pozvání	k1gNnSc4
Beneš	Beneš	k1gMnSc1
Metod	Metod	k1gMnSc1
Kulda	Kulda	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
působil	působit	k5eAaImAgMnS
jako	jako	k9
kaplan	kaplan	k1gMnSc1
ve	v	k7c6
Starém	starý	k2eAgNnSc6d1
Hobzí	Hobzí	k1gNnSc6
<g/>
,	,	kIx,
přednesl	přednést	k5eAaPmAgMnS
o	o	k7c6
pouti	pouť	k1gFnSc6
sv.	sv.	kA
Víta	Vít	k1gMnSc4
k	k	k7c3
věřícím	věřící	k1gFnPc3
kázání	kázání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
pamětníků	pamětník	k1gMnPc2
mnozí	mnohý	k2eAgMnPc1d1
„	„	k?
<g/>
účastníci	účastník	k1gMnPc1
mše	mše	k1gFnSc2
ponejprv	ponejprv	k6eAd1
slyšeli	slyšet	k5eAaImAgMnP
jadrnou	jadrný	k2eAgFnSc4d1
promluvu	promluva	k1gFnSc4
v	v	k7c6
mateřském	mateřský	k2eAgInSc6d1
jazyce	jazyk	k1gInSc6
<g/>
“	“	k?
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
je	být	k5eAaImIp3nS
kazatel	kazatel	k1gMnSc1
nabádal	nabádat	k5eAaBmAgMnS,k5eAaImAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
měli	mít	k5eAaImAgMnP
v	v	k7c6
úctě	úcta	k1gFnSc6
jazyk	jazyk	k1gInSc1
předků	předek	k1gInPc2
a	a	k8xC
milovali	milovat	k5eAaImAgMnP
drahou	drahý	k2eAgFnSc4d1
vlast	vlast	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
ale	ale	k8xC
spolupráce	spolupráce	k1gFnSc1
obou	dva	k4xCgMnPc2
buditelů	buditel	k1gMnPc2
nekončí	končit	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
(	(	kIx(
<g/>
roku	rok	k1gInSc2
1856	#num#	k4
<g/>
)	)	kIx)
Kulda	Kulda	k1gMnSc1
upravil	upravit	k5eAaPmAgMnS
pohádky	pohádka	k1gFnPc4
a	a	k8xC
pověsti	pověst	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
Menšík	Menšík	k1gMnSc1
sebral	sebrat	k5eAaPmAgMnS
v	v	k7c6
okolí	okolí	k1gNnSc6
Jemnice	Jemnice	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
vydal	vydat	k5eAaPmAgMnS
je	být	k5eAaImIp3nS
jako	jako	k9
druhý	druhý	k4xOgInSc4
díl	díl	k1gInSc4
prvního	první	k4xOgNnSc2
vydání	vydání	k1gNnSc6
svých	svůj	k3xOyFgFnPc2
Moravských	moravský	k2eAgFnPc2d1
pohádek	pohádka	k1gFnPc2
pod	pod	k7c7
názvem	název	k1gInSc7
Moravské	moravský	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
pohádky	pohádka	k1gFnSc2
a	a	k8xC
pověsti	pověst	k1gFnSc2
z	z	k7c2
okolí	okolí	k1gNnSc2
Jemnického	jemnický	k2eAgNnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc1
výtisků	výtisk	k1gInPc2
byl	být	k5eAaImAgInS
malý	malý	k2eAgInSc1d1
a	a	k8xC
o	o	k7c4
knihu	kniha	k1gFnSc4
byl	být	k5eAaImAgInS
velký	velký	k2eAgInSc1d1
zájem	zájem	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
tak	tak	k6eAd1
ji	on	k3xPp3gFnSc4
Menšík	Menšík	k1gMnSc1
o	o	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
(	(	kIx(
<g/>
1858	#num#	k4
<g/>
)	)	kIx)
vydal	vydat	k5eAaPmAgMnS
znovu	znovu	k6eAd1
sám	sám	k3xTgMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naposledy	naposledy	k6eAd1
pohádky	pohádka	k1gFnPc1
vyšly	vyjít	k5eAaPmAgFnP
v	v	k7c6
roce	rok	k1gInSc6
1983	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Radnice	radnice	k1gFnSc1
města	město	k1gNnSc2
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
byla	být	k5eAaImAgFnS
v	v	k7c6
německých	německý	k2eAgFnPc6d1
rukách	ruka	k1gFnPc6
a	a	k8xC
aktivity	aktivita	k1gFnSc2
tohoto	tento	k3xDgInSc2
druhu	druh	k1gInSc2
patrně	patrně	k6eAd1
nebyly	být	k5eNaImAgFnP
některým	některý	k3yIgMnPc3
příslušníkům	příslušník	k1gMnPc3
tehdejší	tehdejší	k2eAgFnSc2d1
jemnické	jemnický	k2eAgFnSc2d1
maloměstské	maloměstský	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
po	po	k7c6
chuti	chuť	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
se	se	k3xPyFc4
údajně	údajně	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1858	#num#	k4
<g/>
,	,	kIx,
při	při	k7c6
sčítání	sčítání	k1gNnSc6
roku	rok	k1gInSc2
1857	#num#	k4
ještě	ještě	k6eAd1
žil	žít	k5eAaImAgMnS
v	v	k7c6
Jemnici	Jemnice	k1gFnSc6
<g/>
,	,	kIx,
stěhuje	stěhovat	k5eAaImIp3nS
do	do	k7c2
Brna	Brno	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
přijal	přijmout	k5eAaPmAgMnS
místo	místo	k1gNnSc4
úředníka	úředník	k1gMnSc2
na	na	k7c6
moravském	moravský	k2eAgNnSc6d1
místodržitelství	místodržitelství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
však	však	k9
činný	činný	k2eAgMnSc1d1
i	i	k8xC
v	v	k7c6
Matici	matice	k1gFnSc6
moravské	moravský	k2eAgFnSc2d1
a	a	k8xC
přispívá	přispívat	k5eAaImIp3nS
do	do	k7c2
Moravských	moravský	k2eAgFnPc2d1
novin	novina	k1gFnPc2
a	a	k8xC
časopisu	časopis	k1gInSc2
Lumír	Lumír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Menšík	Menšík	k1gMnSc1
není	být	k5eNaImIp3nS
jen	jen	k9
pravidelným	pravidelný	k2eAgMnSc7d1
dopisovatelem	dopisovatel	k1gMnSc7
do	do	k7c2
novin	novina	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
má	mít	k5eAaImIp3nS
i	i	k9
spisovatelské	spisovatelský	k2eAgNnSc1d1
<g/>
,	,	kIx,
lépe	dobře	k6eAd2
řečeno	řečen	k2eAgNnSc1d1
sběratelské	sběratelský	k2eAgNnSc1d1
<g/>
,	,	kIx,
ambice	ambice	k1gFnSc1
–	–	k?
sbírá	sbírat	k5eAaImIp3nS
a	a	k8xC
sepisuje	sepisovat	k5eAaImIp3nS
lidové	lidový	k2eAgFnPc4d1
pohádky	pohádka	k1gFnPc4
a	a	k8xC
pověsti	pověst	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
jiných	jiný	k2eAgMnPc2d1
buditelů	buditel	k1gMnPc2
<g/>
,	,	kIx,
např.	např.	kA
Erbena	Erben	k1gMnSc4
nebo	nebo	k8xC
Němcové	Němcová	k1gFnPc4
<g/>
,	,	kIx,
je	on	k3xPp3gFnPc4
nebeletrizoval	beletrizovat	k5eNaImAgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
jen	jen	k9
dokumentárně	dokumentárně	k6eAd1
reprodukoval	reprodukovat	k5eAaBmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
sebral	sebrat	k5eAaPmAgInS
a	a	k8xC
následně	následně	k6eAd1
vydal	vydat	k5eAaPmAgInS
125	#num#	k4
pohádek	pohádka	k1gFnPc2
<g/>
,	,	kIx,
pověstí	pověst	k1gFnPc2
<g/>
,	,	kIx,
vyprávění	vyprávění	k1gNnPc2
a	a	k8xC
bajek	bajka	k1gFnPc2
a	a	k8xC
další	další	k2eAgFnPc1d1
desítky	desítka	k1gFnPc1
jich	on	k3xPp3gMnPc2
nebyly	být	k5eNaImAgFnP
zpracovány	zpracovat	k5eAaPmNgFnP
a	a	k8xC
zůstaly	zůstat	k5eAaPmAgInP
jen	jen	k9
v	v	k7c6
poznámkách	poznámka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Menšík	Menšík	k1gMnSc1
měl	mít	k5eAaImAgMnS
široké	široký	k2eAgNnSc4d1
a	a	k8xC
rozmanité	rozmanitý	k2eAgInPc4d1
zájmy	zájem	k1gInPc4
hospodářské	hospodářský	k2eAgInPc4d1
<g/>
,	,	kIx,
dobročinné	dobročinný	k2eAgInPc4d1
<g/>
,	,	kIx,
kulturní	kulturní	k2eAgInPc4d1
a	a	k8xC
vlastivědné	vlastivědný	k2eAgInPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lze	lze	k6eAd1
ocenit	ocenit	k5eAaPmF
i	i	k9
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
humánní	humánní	k2eAgInSc4d1
přístup	přístup	k1gInSc4
ke	k	k7c3
zvířatům	zvíře	k1gNnPc3
včetně	včetně	k7c2
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
podílu	podíl	k1gInSc2
na	na	k7c6
založení	založení	k1gNnSc6
spolků	spolek	k1gInPc2
bojujících	bojující	k2eAgInPc2d1
na	na	k7c6
Moravě	Morava	k1gFnSc6
organizovaně	organizovaně	k6eAd1
proti	proti	k7c3
týrání	týrání	k1gNnSc3
zvířat	zvíře	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
přemíra	přemíra	k1gFnSc1
osobních	osobní	k2eAgNnPc2d1
zálib	zálib	k1gInSc4
mu	on	k3xPp3gMnSc3
zřejmě	zřejmě	k6eAd1
bránila	bránit	k5eAaImAgFnS
v	v	k7c6
některé	některý	k3yIgFnSc6
oblasti	oblast	k1gFnSc6
výrazněji	výrazně	k6eAd2
vyniknout	vyniknout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Menšík	Menšík	k1gMnSc1
psal	psát	k5eAaImAgMnS
česky	česky	k6eAd1
i	i	k9
německy	německy	k6eAd1
a	a	k8xC
byl	být	k5eAaImAgMnS
spíše	spíše	k9
písmák	písmák	k1gMnSc1
než	než	k8xS
spisovatel	spisovatel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
knize	kniha	k1gFnSc6
Moravské	moravský	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
pohádky	pohádka	k1gFnSc2
a	a	k8xC
pověsti	pověst	k1gFnSc2
z	z	k7c2
okolí	okolí	k1gNnSc2
Jemnického	jemnický	k2eAgNnSc2d1
<g/>
,	,	kIx,
o	o	k7c4
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
byla	být	k5eAaImAgFnS
zmínka	zmínka	k1gFnSc1
výše	výše	k1gFnSc2,k1gFnSc2wB
<g/>
,	,	kIx,
pokračoval	pokračovat	k5eAaImAgInS
dalšími	další	k2eAgNnPc7d1
díly	dílo	k1gNnPc7
<g/>
:	:	kIx,
I.	I.	kA
Moravské	moravský	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
pohádky	pohádka	k1gFnSc2
a	a	k8xC
pověsti	pověst	k1gFnSc2
z	z	k7c2
okolí	okolí	k1gNnSc2
Telčského	telčský	k2eAgNnSc2d1
<g/>
,	,	kIx,
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moravské	moravský	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
pohádky	pohádka	k1gFnSc2
a	a	k8xC
pověsti	pověst	k1gFnSc2
z	z	k7c2
okolí	okolí	k1gNnSc2
Dačického	dačický	k2eAgInSc2d1
aj.	aj.	kA
Do	do	k7c2
sebraných	sebraný	k2eAgFnPc2d1
pohádek	pohádka	k1gFnPc2
a	a	k8xC
pověstí	pověst	k1gFnPc2
nejednou	jednou	k6eNd1
vkládal	vkládat	k5eAaImAgInS
lokální	lokální	k2eAgInPc4d1
historické	historický	k2eAgInPc4d1
údaje	údaj	k1gInPc4
a	a	k8xC
občas	občas	k6eAd1
didaktické	didaktický	k2eAgInPc4d1
či	či	k8xC
mravoučné	mravoučný	k2eAgInPc4d1
prvky	prvek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kritika	kritika	k1gFnSc1
mu	on	k3xPp3gMnSc3
vytýkala	vytýkat	k5eAaImAgFnS
<g/>
,	,	kIx,
„	„	k?
<g/>
že	že	k8xS
nekriticky	kriticky	k6eNd1
sbírá	sbírat	k5eAaImIp3nS
různé	různý	k2eAgFnPc4d1
zprávy	zpráva	k1gFnPc4
bez	bez	k7c2
výběru	výběr	k1gInSc2
a	a	k8xC
má	mít	k5eAaImIp3nS
mnoho	mnoho	k4c1
mravoučných	mravoučný	k2eAgFnPc2d1
a	a	k8xC
historických	historický	k2eAgFnPc2d1
povídek	povídka	k1gFnPc2
podezřelého	podezřelý	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
těmto	tento	k3xDgFnPc3
poznámkám	poznámka	k1gFnPc3
nám	my	k3xPp1nPc3
ale	ale	k9
zůstalo	zůstat	k5eAaPmAgNnS
zachováno	zachovat	k5eAaPmNgNnS
něco	něco	k3yInSc1
z	z	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
měl	mít	k5eAaImAgMnS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
k	k	k7c3
dispozici	dispozice	k1gFnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
během	během	k7c2
času	čas	k1gInSc2
vzalo	vzít	k5eAaPmAgNnS
za	za	k7c4
své	své	k1gNnSc4
-	-	kIx~
např.	např.	kA
opis	opis	k1gInSc1
„	„	k?
<g/>
Listu	list	k1gInSc2
Puthy	Putha	k1gFnSc2
z	z	k7c2
Lychtenburgka	Lychtenburgek	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
1480	#num#	k4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Menšík	Menšík	k1gMnSc1
se	se	k3xPyFc4
pokusil	pokusit	k5eAaPmAgMnS
zpracovat	zpracovat	k5eAaPmF
dějiny	dějiny	k1gFnPc4
města	město	k1gNnSc2
Jemnice	Jemnice	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Rukopis	rukopis	k1gInSc1
obsahoval	obsahovat	k5eAaImAgInS
240	#num#	k4
číslovaných	číslovaný	k2eAgFnPc2d1
stran	strana	k1gFnPc2
a	a	k8xC
po	po	k7c4
dlouhá	dlouhý	k2eAgNnPc4d1
desetiletí	desetiletí	k1gNnPc4
byl	být	k5eAaImAgInS
uložen	uložen	k2eAgInSc1d1
ve	v	k7c6
farním	farní	k2eAgInSc6d1
archivu	archiv	k1gInSc6
Jemnice	Jemnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jistou	jistý	k2eAgFnSc4d1
proslulost	proslulost	k1gFnSc4
mu	on	k3xPp3gMnSc3
získalo	získat	k5eAaPmAgNnS
několik	několik	k4yIc1
spisků	spisek	k1gInPc2
s	s	k7c7
rozličnými	rozličný	k2eAgInPc7d1
náměty	námět	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1859	#num#	k4
vydal	vydat	k5eAaPmAgInS
drobnou	drobný	k2eAgFnSc4d1
knížečku	knížečka	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
věnována	věnovat	k5eAaImNgFnS,k5eAaPmNgFnS
hlavním	hlavní	k2eAgFnPc3d1
poutním	poutní	k2eAgFnPc3d1
místům	místo	k1gNnPc3
na	na	k7c6
Moravě	Morava	k1gFnSc6
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
historického	historický	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
je	být	k5eAaImIp3nS
cenná	cenný	k2eAgFnSc1d1
jeho	jeho	k3xOp3gFnSc1
stať	stať	k1gFnSc1
nazvaná	nazvaný	k2eAgFnSc1d1
„	„	k?
<g/>
Slavnost	slavnost	k1gFnSc1
Svatovítská	svatovítský	k2eAgFnSc1d1
v	v	k7c6
Jemnici	Jemnice	k1gFnSc6
na	na	k7c6
Moravě	Morava	k1gFnSc6
<g/>
“	“	k?
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
vyšla	vyjít	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1853	#num#	k4
v	v	k7c6
časopise	časopis	k1gInSc6
„	„	k?
<g/>
Lumír	Lumír	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Jsou	být	k5eAaImIp3nP
dvě	dva	k4xCgFnPc1
skutečnosti	skutečnost	k1gFnPc1
<g/>
,	,	kIx,
díky	díky	k7c3
nimž	jenž	k3xRgFnPc3
se	se	k3xPyFc4
Menšík	Menšík	k1gMnSc1
udržel	udržet	k5eAaPmAgMnS
dodnes	dodnes	k6eAd1
v	v	k7c4
povědomí	povědomí	k1gNnSc4
obyvatel	obyvatel	k1gMnPc2
Jemnice	Jemnice	k1gFnSc2
a	a	k8xC
jejího	její	k3xOp3gNnSc2
okolí	okolí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
z	z	k7c2
nich	on	k3xPp3gFnPc2
je	být	k5eAaImIp3nS
pohádka	pohádka	k1gFnSc1
„	„	k?
<g/>
O	o	k7c6
Budulínkovi	Budulínek	k1gMnSc6
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
před	před	k7c7
čtyřiceti	čtyřicet	k4xCc7
léty	léto	k1gNnPc7
v	v	k7c6
Jemnici	Jemnice	k1gFnSc6
nebyl	být	k5eNaImAgMnS
nikdo	nikdo	k3yNnSc1
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yRnSc1,k3yQnSc1
by	by	kYmCp3nS
tuto	tento	k3xDgFnSc4
krátkou	krátký	k2eAgFnSc4d1
<g/>
,	,	kIx,
dobře	dobře	k6eAd1
zapamatovatelnou	zapamatovatelný	k2eAgFnSc4d1
<g/>
,	,	kIx,
z	z	k7c2
části	část	k1gFnSc2
vyprávěnou	vyprávěný	k2eAgFnSc7d1
a	a	k8xC
z	z	k7c2
části	část	k1gFnSc2
zpívanou	zpívaná	k1gFnSc4
<g/>
,	,	kIx,
pohádku	pohádka	k1gFnSc4
neznal	neznat	k5eAaImAgInS,k5eNaImAgInS
zpaměti	zpaměti	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měla	mít	k5eAaImAgFnS
několik	několik	k4yIc4
verzí	verze	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
od	od	k7c2
sebe	se	k3xPyFc2
lišily	lišit	k5eAaImAgFnP
jen	jen	k6eAd1
počtem	počet	k1gInSc7
lištiček	lištička	k1gFnPc2
<g/>
;	;	kIx,
když	když	k8xS
ji	on	k3xPp3gFnSc4
např.	např.	kA
vyprávěly	vyprávět	k5eAaImAgInP
babičky	babička	k1gFnSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
jejich	jejich	k3xOp3gInSc1
počet	počet	k1gInSc1
vyšší	vysoký	k2eAgInSc1d2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
pohádka	pohádka	k1gFnSc1
trvala	trvat	k5eAaImAgFnS
déle	dlouho	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhou	druhý	k4xOgFnSc7
je	být	k5eAaImIp3nS
„	„	k?
<g/>
Slavnost	slavnost	k1gFnSc1
Svatovítská	svatovítský	k2eAgFnSc1d1
se	se	k3xPyFc4
v	v	k7c6
Jemnici	Jemnice	k1gFnSc6
<g/>
“	“	k?
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
koná	konat	k5eAaImIp3nS
každoročně	každoročně	k6eAd1
již	již	k6eAd1
více	hodně	k6eAd2
než	než	k8xS
300	#num#	k4
roků	rok	k1gInPc2
<g/>
;	;	kIx,
označení	označení	k1gNnSc2
„	„	k?
<g/>
Barchan	barchan	k1gInSc1
<g/>
“	“	k?
poprvé	poprvé	k6eAd1
použil	použít	k5eAaPmAgMnS
až	až	k9
Menšík	Menšík	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úvodem	úvodem	k7c2
slavnosti	slavnost	k1gFnSc2
zpravidla	zpravidla	k6eAd1
vystupují	vystupovat	k5eAaImIp3nP
představitelé	představitel	k1gMnPc1
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
svém	svůj	k3xOyFgInSc6
projevu	projev	k1gInSc6
se	se	k3xPyFc4
zabývají	zabývat	k5eAaImIp3nP
jejím	její	k3xOp3gInSc7
původem	původ	k1gInSc7
a	a	k8xC
historií	historie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jen	jen	k6eAd1
málokdy	málokdy	k6eAd1
se	se	k3xPyFc4
stává	stávat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
se	se	k3xPyFc4
řečník	řečník	k1gMnSc1
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
vystoupení	vystoupení	k1gNnSc6
nezmínil	zmínit	k5eNaPmAgMnS
o	o	k7c6
Menšíkovi	Menšík	k1gMnSc6
jako	jako	k8xC,k8xS
o	o	k7c6
zdroji	zdroj	k1gInSc6
svých	svůj	k3xOyFgFnPc2
informací	informace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Moravské	moravský	k2eAgFnPc1d1
národní	národní	k2eAgFnPc1d1
pohádky	pohádka	k1gFnPc1
a	a	k8xC
pověsti	pověst	k1gFnPc1
z	z	k7c2
okolí	okolí	k1gNnSc2
Jemnického	jemnický	k2eAgInSc2d1
(	(	kIx(
<g/>
1856	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Moravské	moravský	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
pohádky	pohádka	k1gFnSc2
a	a	k8xC
pověsti	pověst	k1gFnSc2
(	(	kIx(
<g/>
1862	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Minulost	minulost	k1gFnSc1
a	a	k8xC
přítomnost	přítomnost	k1gFnSc1
města	město	k1gNnSc2
Jemnice	Jemnice	k1gFnSc2
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kvítečka	kvítečko	k1gNnPc1
z	z	k7c2
Mariánského	mariánský	k2eAgInSc2d1
slávověnce	slávověnec	k1gInSc2
sebraná	sebraný	k2eAgNnPc1d1
od	od	k7c2
vlasteneckých	vlastenecký	k2eAgMnPc2d1
poutníků	poutník	k1gMnPc2
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Digitalizované	digitalizovaný	k2eAgFnPc1d1
knihy	kniha	k1gFnPc1
dostupné	dostupný	k2eAgFnPc1d1
online	onlinout	k5eAaPmIp3nS
</s>
<s>
Moravské	moravský	k2eAgFnPc1d1
národní	národní	k2eAgFnPc1d1
pohádky	pohádka	k1gFnPc1
a	a	k8xC
pověsti	pověst	k1gFnPc1
z	z	k7c2
okolí	okolí	k1gNnSc2
Jemnického	jemnický	k2eAgInSc2d1
(	(	kIx(
<g/>
1858	#num#	k4
<g/>
)	)	kIx)
Rakouská	rakouský	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
<g/>
,	,	kIx,
Digitalizováno	digitalizován	k2eAgNnSc1d1
5	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
2017	#num#	k4
</s>
<s>
https://books.google.cz/books?id=xrRBqYPIhrsC&	https://books.google.cz/books?id=xrRBqYPIhrsC&	k1gMnSc1
</s>
<s>
Moravské	moravský	k2eAgFnPc1d1
národní	národní	k2eAgFnPc1d1
pohádky	pohádka	k1gFnPc1
a	a	k8xC
pověsti	pověst	k1gFnPc1
z	z	k7c2
okolí	okolí	k1gNnSc2
Telčského	telčský	k2eAgInSc2d1
(	(	kIx(
<g/>
1862	#num#	k4
<g/>
)	)	kIx)
Rakouská	rakouský	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
<g/>
,	,	kIx,
Digitalizováno	digitalizován	k2eAgNnSc1d1
<g/>
:	:	kIx,
5	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
2017	#num#	k4
</s>
<s>
https://books.google.cz/books?id=42qLW6-LrScC&	https://books.google.cz/books?id=42qLW6-LrScC&	k6eAd1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Matriční	matriční	k2eAgInSc4d1
záznam	záznam	k1gInSc4
o	o	k7c4
úmrtí	úmrtí	k1gNnSc4
a	a	k8xC
pohřbu	pohřeb	k1gInSc2
<g/>
↑	↑	k?
jindy	jindy	k6eAd1
je	být	k5eAaImIp3nS
uváděno	uvádět	k5eAaImNgNnS
datum	datum	k1gNnSc1
úmrtí	úmrtí	k1gNnSc2
6	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1863	#num#	k4
nebo	nebo	k8xC
6	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1869	#num#	k4
↑	↑	k?
oznámení	oznámení	k1gNnSc4
o	o	k7c4
úmrtí	úmrtí	k1gNnSc4
J.	J.	kA
<g/>
S.	S.	kA
<g/>
Menšíka	Menšík	k1gMnSc2
<g/>
,	,	kIx,
Opavský	opavský	k2eAgInSc4d1
besedník	besedník	k1gInSc4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.1	.1	k4
1861-62	1861-62	k4
č.	č.	k?
<g/>
50	#num#	k4
str	str	kA
<g/>
.397	.397	k4
<g/>
↑	↑	k?
Jeho	jeho	k3xOp3gNnPc4
bratr	bratr	k1gMnSc1
Jan	Jan	k1gMnSc1
Nepomuk	Nepomuk	k1gMnSc1
se	se	k3xPyFc4
již	již	k6eAd1
v	v	k7c6
Jemnici	Jemnice	k1gFnSc6
narodil	narodit	k5eAaPmAgMnS
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
7	#num#	k4
<g/>
.	.	kIx.
12.181	12.181	k4
<g/>
4.1	4.1	k4
2	#num#	k4
Rukopis	rukopis	k1gInSc1
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yQgInSc6,k3yRgInSc6
rok	rok	k1gInSc4
po	po	k7c6
roce	rok	k1gInSc6
zaznamenává	zaznamenávat	k5eAaImIp3nS
události	událost	k1gFnPc4
vztahující	vztahující	k2eAgFnPc4d1
se	se	k3xPyFc4
k	k	k7c3
historii	historie	k1gFnSc3
města	město	k1gNnSc2
Jemnice	Jemnice	k1gFnSc2
<g/>
,	,	kIx,
o	o	k7c6
kterých	který	k3yIgInPc6,k3yRgInPc6,k3yQgInPc6
získal	získat	k5eAaPmAgInS
nějaký	nějaký	k3yIgInSc4
materiál	materiál	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Zdroje	zdroj	k1gInPc1
</s>
<s>
Fišer	Fišer	k1gMnSc1
Zdeněk	Zdeněk	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
<g/>
S.	S.	kA
Menšík	Menšík	k1gMnSc1
<g/>
,	,	kIx,
měšťan	měšťan	k1gMnSc1
jemnický	jemnický	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poznámky	poznámka	k1gFnPc4
k	k	k7c3
životu	život	k1gInSc3
a	a	k8xC
dílu	dílo	k1gNnSc3
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Vlastivědný	vlastivědný	k2eAgInSc1d1
sborník	sborník	k1gInSc1
Západní	západní	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
ročník	ročník	k1gInSc1
1999	#num#	k4
str	str	kA
<g/>
.48	.48	k4
<g/>
-	-	kIx~
<g/>
65	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Josef	Josef	k1gMnSc1
Stanislav	Stanislav	k1gMnSc1
Menšík	Menšík	k1gMnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jk	jk	k?
<g/>
0	#num#	k4
<g/>
1081247	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2213	#num#	k4
1456	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2008127020	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
83825760	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2008127020	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Literatura	literatura	k1gFnSc1
</s>
