<s>
Život	život	k1gInSc1
Menšíka	Menšík	k1gMnSc2
je	být	k5eAaImIp3nS
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1848	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
zpět	zpět	k6eAd1
do	do	k7c2
Jemnice	Jemnice	k1gFnSc2
<g/>
,	,	kIx,
zahalen	zahalit	k5eAaPmNgInS
tajemstvím	tajemství	k1gNnSc7
<g/>
,	,	kIx,
protože	protože	k8xS
větší	veliký	k2eAgFnSc1d2
část	část	k1gFnSc1
svého	svůj	k3xOyFgInSc2
života	život	k1gInSc2
pobýval	pobývat	k5eAaImAgMnS
v	v	k7c6
cizině	cizina	k1gFnSc6
<g/>
.	.	kIx.
</s>