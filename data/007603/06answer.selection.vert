<s>
Brno	Brno	k1gNnSc1	Brno
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Brünn	Brünn	k1gNnSc1	Brünn
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Bruna	Bruna	k1gMnSc1	Bruna
<g/>
,	,	kIx,	,
maďarsky	maďarsky	k6eAd1	maďarsky
Berén	Berén	k1gInSc1	Berén
<g/>
,	,	kIx,	,
v	v	k7c6	v
jidiš	jidiš	k1gNnSc6	jidiš
ב	ב	k?	ב
Brin	Brin	k1gInSc1	Brin
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
statutární	statutární	k2eAgNnSc1d1	statutární
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
počtem	počet	k1gInSc7	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
i	i	k8xC	i
rozlohou	rozloha	k1gFnSc7	rozloha
druhé	druhý	k4xOgNnSc4	druhý
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
bývalé	bývalý	k2eAgNnSc4d1	bývalé
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
