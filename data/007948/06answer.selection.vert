<s>
Pořádají	pořádat	k5eAaImIp3nP	pořádat
se	se	k3xPyFc4	se
jako	jako	k9	jako
<g/>
:	:	kIx,	:
Letní	letní	k2eAgFnPc1d1	letní
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
od	od	k7c2	od
obnovení	obnovení	k1gNnSc2	obnovení
her	hra	k1gFnPc2	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
Zimní	zimní	k2eAgFnPc1d1	zimní
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
Letní	letní	k2eAgFnSc1d1	letní
i	i	k8xC	i
zimní	zimní	k2eAgFnSc1d1	zimní
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
4	[number]	k4	4
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
