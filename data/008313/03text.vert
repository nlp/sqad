<p>
<s>
Rubín	rubín	k1gInSc1	rubín
je	být	k5eAaImIp3nS	být
růžový	růžový	k2eAgInSc1d1	růžový
až	až	k8xS	až
červený	červený	k2eAgInSc1d1	červený
drahokam	drahokam	k1gInSc1	drahokam
sestávající	sestávající	k2eAgInSc1d1	sestávající
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
minerálu	minerál	k1gInSc2	minerál
korundu	korund	k1gInSc2	korund
(	(	kIx(	(
<g/>
Al	ala	k1gFnPc2	ala
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
s	s	k7c7	s
příměsí	příměs	k1gFnSc7	příměs
chromu	chromat	k5eAaImIp1nS	chromat
způsobující	způsobující	k2eAgNnSc4d1	způsobující
zbarvení	zbarvení	k1gNnSc4	zbarvení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Těžba	těžba	k1gFnSc1	těžba
<g/>
,	,	kIx,	,
popis	popis	k1gInSc1	popis
<g/>
,	,	kIx,	,
kvalita	kvalita	k1gFnSc1	kvalita
a	a	k8xC	a
ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
největší	veliký	k2eAgNnSc4d3	veliký
naleziště	naleziště	k1gNnSc4	naleziště
rubínů	rubín	k1gInPc2	rubín
patří	patřit	k5eAaImIp3nS	patřit
zejména	zejména	k9	zejména
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
,	,	kIx,	,
Asie	Asie	k1gFnSc1	Asie
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
,	,	kIx,	,
Grónsko	Grónsko	k1gNnSc1	Grónsko
<g/>
,	,	kIx,	,
Madagaskar	Madagaskar	k1gInSc1	Madagaskar
a	a	k8xC	a
Severní	severní	k2eAgFnSc1d1	severní
Karolína	Karolína	k1gFnSc1	Karolína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rubíny	rubín	k1gInPc1	rubín
nejlepší	dobrý	k2eAgFnSc2d3	nejlepší
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
sytě	sytě	k6eAd1	sytě
červené	červený	k2eAgNnSc4d1	červené
(	(	kIx(	(
<g/>
v	v	k7c6	v
obchodním	obchodní	k2eAgInSc6d1	obchodní
styku	styk	k1gInSc6	styk
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
barva	barva	k1gFnSc1	barva
nazývána	nazývat	k5eAaImNgFnS	nazývat
barvou	barva	k1gFnSc7	barva
holubí	holubí	k2eAgFnSc2d1	holubí
krve	krev	k1gFnSc2	krev
<g/>
)	)	kIx)	)
s	s	k7c7	s
lehkým	lehký	k2eAgInSc7d1	lehký
modravým	modravý	k2eAgInSc7d1	modravý
odstínem	odstín	k1gInSc7	odstín
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Barmy	Barma	k1gFnSc2	Barma
<g/>
,	,	kIx,	,
Mogoku	Mogok	k1gInSc2	Mogok
<g/>
,	,	kIx,	,
rubíny	rubín	k1gInPc1	rubín
dobré	dobrý	k2eAgFnSc2d1	dobrá
barvy	barva	k1gFnSc2	barva
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
i	i	k9	i
rubíny	rubín	k1gInPc1	rubín
tanzanské	tanzanský	k2eAgInPc1d1	tanzanský
<g/>
,	,	kIx,	,
afghánské	afghánský	k2eAgInPc1d1	afghánský
<g/>
,	,	kIx,	,
vietnamské	vietnamský	k2eAgInPc1d1	vietnamský
a	a	k8xC	a
rubíny	rubín	k1gInPc1	rubín
z	z	k7c2	z
Nepálu	Nepál	k1gInSc2	Nepál
<g/>
,	,	kIx,	,
Srí	Srí	k1gFnSc2	Srí
Lanky	lanko	k1gNnPc7	lanko
a	a	k8xC	a
Indie	Indie	k1gFnSc1	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Rubíny	rubín	k1gInPc1	rubín
barvy	barva	k1gFnSc2	barva
holubí	holubí	k2eAgFnSc2d1	holubí
krve	krev	k1gFnSc2	krev
jsou	být	k5eAaImIp3nP	být
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
barvě	barva	k1gFnSc3	barva
nejcennějsí	jcennějsit	k5eNaPmIp3nP	jcennějsit
<g/>
,	,	kIx,	,
v	v	k7c6	v
Barmě	Barma	k1gFnSc6	Barma
navíc	navíc	k6eAd1	navíc
častokrát	častokrát	k6eAd1	častokrát
ve	v	k7c6	v
viditelném	viditelný	k2eAgNnSc6d1	viditelné
světle	světlo	k1gNnSc6	světlo
červeně	červeně	k6eAd1	červeně
fluoreskují	fluoreskovat	k5eAaImIp3nP	fluoreskovat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
barmské	barmský	k2eAgInPc4d1	barmský
rubíny	rubín	k1gInPc4	rubín
obvykle	obvykle	k6eAd1	obvykle
nejkvalitnější	kvalitní	k2eAgInPc4d3	nejkvalitnější
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
<g/>
Nejstarší	starý	k2eAgNnPc1d3	nejstarší
naleziště	naleziště	k1gNnSc4	naleziště
rubínů	rubín	k1gInPc2	rubín
jsou	být	k5eAaImIp3nP	být
náplavy	náplava	k1gFnSc2	náplava
řek	řeka	k1gFnPc2	řeka
jihozápadní	jihozápadní	k2eAgMnPc1d1	jihozápadní
Srí	Srí	k1gMnPc1	Srí
Lanky	lanko	k1gNnPc7	lanko
(	(	kIx(	(
<g/>
řeka	řeka	k1gFnSc1	řeka
Kaliganga	Kaliganga	k1gFnSc1	Kaliganga
<g/>
)	)	kIx)	)
se	s	k7c7	s
starým	starý	k2eAgInSc7d1	starý
centrem	centr	k1gInSc7	centr
Ratnapury	Ratnapura	k1gFnSc2	Ratnapura
<g/>
.	.	kIx.	.
</s>
<s>
Cejlonské	cejlonský	k2eAgInPc1d1	cejlonský
rubíny	rubín	k1gInPc1	rubín
jsou	být	k5eAaImIp3nP	být
malinové	malinový	k2eAgFnPc4d1	malinová
barvy	barva	k1gFnPc4	barva
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
asterické	asterický	k2eAgInPc1d1	asterický
kameny	kámen	k1gInPc1	kámen
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
zmínky	zmínka	k1gFnPc1	zmínka
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
Arthašástry	Arthašástra	k1gFnSc2	Arthašástra
<g/>
,	,	kIx,	,
Brhatamhíty	Brhatamhíta	k1gFnSc2	Brhatamhíta
a	a	k8xC	a
buddhistických	buddhistický	k2eAgFnPc2d1	buddhistická
kronik	kronika	k1gFnPc2	kronika
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
od	od	k7c2	od
cestovatelů	cestovatel	k1gMnPc2	cestovatel
(	(	kIx(	(
<g/>
i	i	k9	i
Marka	marka	k1gFnSc1	marka
Pola	pola	k1gFnSc1	pola
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
hovořit	hovořit	k5eAaImF	hovořit
o	o	k7c6	o
3000	[number]	k4	3000
letech	léto	k1gNnPc6	léto
těžeb	těžba	k1gFnPc2	těžba
zdejších	zdejší	k2eAgInPc2d1	zdejší
rubínů	rubín	k1gInPc2	rubín
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
až	až	k9	až
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
a	a	k8xC	a
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
na	na	k7c6	na
asijském	asijský	k2eAgInSc6d1	asijský
a	a	k8xC	a
západním	západní	k2eAgInSc6d1	západní
trhu	trh	k1gInSc6	trh
objevují	objevovat	k5eAaImIp3nP	objevovat
rubíny	rubín	k1gInPc1	rubín
z	z	k7c2	z
Barmy	Barma	k1gFnSc2	Barma
a	a	k8xC	a
Kambodži	Kambodža	k1gFnSc6	Kambodža
či	či	k8xC	či
Thajska	Thajsko	k1gNnSc2	Thajsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
novověku	novověk	k1gInSc2	novověk
se	se	k3xPyFc4	se
rubíny	rubín	k1gInPc1	rubín
těží	těžet	k5eAaImIp3nP	těžet
také	také	k9	také
na	na	k7c6	na
Madagaskaru	Madagaskar	k1gInSc6	Madagaskar
<g/>
,	,	kIx,	,
v	v	k7c6	v
Tanzanii	Tanzanie	k1gFnSc6	Tanzanie
<g/>
,	,	kIx,	,
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
<g/>
,	,	kIx,	,
Vietnamu	Vietnam	k1gInSc6	Vietnam
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
lokalitách	lokalita	k1gFnPc6	lokalita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
v	v	k7c6	v
Thajsku	Thajsko	k1gNnSc6	Thajsko
rubíny	rubín	k1gInPc4	rubín
také	také	k9	také
hodně	hodně	k6eAd1	hodně
těží	těžet	k5eAaImIp3nS	těžet
<g/>
,	,	kIx,	,
nedoporučuje	doporučovat	k5eNaImIp3nS	doporučovat
se	se	k3xPyFc4	se
kupovat	kupovat	k5eAaImF	kupovat
rubíny	rubín	k1gInPc4	rubín
od	od	k7c2	od
pouličních	pouliční	k2eAgMnPc2d1	pouliční
obchodníků	obchodník	k1gMnPc2	obchodník
<g/>
,	,	kIx,	,
thajští	thajský	k2eAgMnPc1d1	thajský
obchodníci	obchodník	k1gMnPc1	obchodník
bývají	bývat	k5eAaImIp3nP	bývat
totiž	totiž	k9	totiž
mistry	mistr	k1gMnPc4	mistr
v	v	k7c6	v
padělání	padělání	k1gNnSc6	padělání
a	a	k8xC	a
nezřídka	nezřídka	k6eAd1	nezřídka
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
madagaskarské	madagaskarský	k2eAgNnSc4d1	madagaskarské
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
kvalitní	kvalitní	k2eAgInPc1d1	kvalitní
rubíny	rubín	k1gInPc1	rubín
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
za	za	k7c4	za
ty	ten	k3xDgFnPc4	ten
thajské	thajský	k2eAgFnPc4d1	thajská
pouze	pouze	k6eAd1	pouze
vydávají	vydávat	k5eAaImIp3nP	vydávat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dělení	dělení	k1gNnSc1	dělení
===	===	k?	===
</s>
</p>
<p>
<s>
Za	za	k7c7	za
rubíny	rubín	k1gInPc7	rubín
se	se	k3xPyFc4	se
vydávají	vydávat	k5eAaPmIp3nP	vydávat
růžové	růžový	k2eAgInPc1d1	růžový
safíry	safír	k1gInPc1	safír
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
prý	prý	k9	prý
byl	být	k5eAaImAgInS	být
dle	dle	k7c2	dle
přednášky	přednáška	k1gFnSc2	přednáška
Roberta	Robert	k1gMnSc2	Robert
Nového	Nový	k1gMnSc2	Nový
o	o	k7c6	o
problému	problém	k1gInSc6	problém
určování	určování	k1gNnPc2	určování
staroindických	staroindický	k2eAgInPc2d1	staroindický
minerálů	minerál	k1gInPc2	minerál
v	v	k7c6	v
soudobé	soudobý	k2eAgFnSc6d1	soudobá
mineralogii	mineralogie	k1gFnSc6	mineralogie
<g/>
,	,	kIx,	,
padparádža	padparádža	k6eAd1	padparádža
safír	safír	k1gInSc1	safír
rubínem	rubín	k1gInSc7	rubín
padmarága	padmarág	k1gMnSc2	padmarág
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Ratnaparikshy	Ratnapariksha	k1gMnSc2	Ratnapariksha
tak	tak	k6eAd1	tak
měli	mít	k5eAaImAgMnP	mít
bráhmani	bráhman	k1gMnPc1	bráhman
dělit	dělit	k5eAaImF	dělit
rubíny	rubín	k1gInPc4	rubín
podle	podle	k7c2	podle
kast	kasta	k1gFnPc2	kasta
na	na	k7c4	na
jejich	jejich	k3xOp3gFnPc4	jejich
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Oceňování	oceňování	k1gNnSc1	oceňování
===	===	k?	===
</s>
</p>
<p>
<s>
Rubíny	rubín	k1gInPc1	rubín
špičkové	špičkový	k2eAgFnSc2d1	špičková
jakosti	jakost	k1gFnSc2	jakost
a	a	k8xC	a
velikosti	velikost	k1gFnPc4	velikost
mající	mající	k2eAgFnPc4d1	mající
nad	nad	k7c4	nad
deset	deset	k4xCc4	deset
karátů	karát	k1gInPc2	karát
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
závratných	závratný	k2eAgFnPc2d1	závratná
cen	cena	k1gFnPc2	cena
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
vzácnější	vzácný	k2eAgInPc1d2	vzácnější
a	a	k8xC	a
cennější	cenný	k2eAgInPc1d2	cennější
než	než	k8xS	než
velké	velký	k2eAgInPc1d1	velký
diamanty	diamant	k1gInPc1	diamant
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
vzácné	vzácný	k2eAgInPc1d1	vzácný
a	a	k8xC	a
výjimečné	výjimečný	k2eAgInPc1d1	výjimečný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zpracování	zpracování	k1gNnSc1	zpracování
a	a	k8xC	a
výroba	výroba	k1gFnSc1	výroba
==	==	k?	==
</s>
</p>
<p>
<s>
Přírodní	přírodní	k2eAgInPc1d1	přírodní
rubíny	rubín	k1gInPc1	rubín
se	se	k3xPyFc4	se
dopalují	dopalovat	k5eAaImIp3nP	dopalovat
(	(	kIx(	(
<g/>
heated	heated	k1gInSc1	heated
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
měly	mít	k5eAaImAgFnP	mít
lepší	dobrý	k2eAgFnSc4d2	lepší
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
berylliem	beryllium	k1gNnSc7	beryllium
(	(	kIx(	(
<g/>
treatment	treatment	k1gMnSc1	treatment
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
skelnou	skelný	k2eAgFnSc7d1	skelná
pastou	pasta	k1gFnSc7	pasta
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
úpravami	úprava	k1gFnPc7	úprava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rubíny	rubín	k1gInPc1	rubín
jsou	být	k5eAaImIp3nP	být
vzácné	vzácný	k2eAgInPc1d1	vzácný
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
rubínů	rubín	k1gInPc2	rubín
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
uměle	uměle	k6eAd1	uměle
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
Verneuillova	Verneuillův	k2eAgFnSc1d1	Verneuillův
metoda	metoda	k1gFnSc1	metoda
nebo	nebo	k8xC	nebo
Czochralského	Czochralský	k2eAgMnSc2d1	Czochralský
metoda	metoda	k1gFnSc1	metoda
pro	pro	k7c4	pro
postupný	postupný	k2eAgInSc4d1	postupný
růst	růst	k1gInSc4	růst
krystalu	krystal	k1gInSc2	krystal
z	z	k7c2	z
taveniny	tavenina	k1gFnSc2	tavenina
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
ale	ale	k9	ale
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
další	další	k2eAgFnPc1d1	další
metody	metoda	k1gFnPc1	metoda
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
třeba	třeba	k6eAd1	třeba
hydrotermální	hydrotermální	k2eAgFnSc1d1	hydrotermální
metoda	metoda	k1gFnSc1	metoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnPc4	použití
a	a	k8xC	a
zajímavosti	zajímavost	k1gFnPc4	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Rubín	rubín	k1gInSc1	rubín
manikja	manikj	k1gInSc2	manikj
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
dle	dle	k7c2	dle
Indů	Ind	k1gMnPc2	Ind
králem	král	k1gMnSc7	král
mezi	mezi	k7c4	mezi
drahokamy	drahokam	k1gInPc4	drahokam
<g/>
,	,	kIx,	,
darujícím	darující	k2eAgFnPc3d1	darující
životní	životní	k2eAgFnPc4d1	životní
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
sexuální	sexuální	k2eAgFnSc4d1	sexuální
plodnost	plodnost	k1gFnSc4	plodnost
<g/>
,	,	kIx,	,
lék	lék	k1gInSc4	lék
na	na	k7c4	na
srdce	srdce	k1gNnSc4	srdce
a	a	k8xC	a
slabosti	slabost	k1gFnPc4	slabost
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
dodává	dodávat	k5eAaImIp3nS	dodávat
pranu	prana	k1gFnSc4	prana
<g/>
,	,	kIx,	,
energii	energie	k1gFnSc4	energie
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
jemné	jemný	k2eAgFnSc2d1	jemná
energie	energie	k1gFnSc2	energie
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rubíny	rubín	k1gInPc4	rubín
jsou	být	k5eAaImIp3nP	být
žádané	žádaný	k2eAgInPc4d1	žádaný
magické	magický	k2eAgInPc4d1	magický
a	a	k8xC	a
klenotnické	klenotnický	k2eAgInPc4d1	klenotnický
minerály	minerál	k1gInPc4	minerál
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
oranžovočervená	oranžovočervený	k2eAgFnSc1d1	oranžovočervená
<g/>
,	,	kIx,	,
malinová	malinový	k2eAgFnSc1d1	malinová
<g/>
,	,	kIx,	,
slabě	slabě	k6eAd1	slabě
červená	červený	k2eAgFnSc1d1	červená
<g/>
,	,	kIx,	,
sytě	sytě	k6eAd1	sytě
červená	červený	k2eAgFnSc1d1	červená
<g/>
,	,	kIx,	,
rudá	rudý	k2eAgFnSc1d1	rudá
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
krev	krev	k1gFnSc1	krev
až	až	k6eAd1	až
purpurověčervená	purpurověčervený	k2eAgFnSc1d1	purpurověčervený
<g/>
,	,	kIx,	,
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
dokonce	dokonce	k9	dokonce
až	až	k9	až
trochu	trochu	k6eAd1	trochu
hnědočervená	hnědočervený	k2eAgFnSc1d1	hnědočervená
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgInPc1d1	historický
minerály	minerál	k1gInPc1	minerál
pocházejí	pocházet	k5eAaImIp3nP	pocházet
především	především	k9	především
ze	z	k7c2	z
Cejlonu	Cejlon	k1gInSc2	Cejlon
<g/>
..	..	k?	..
</s>
</p>
<p>
<s>
Nejchválenějším	Nejchválený	k2eAgMnPc3d2	Nejchválený
a	a	k8xC	a
nejvíce	nejvíce	k6eAd1	nejvíce
ceněným	ceněný	k2eAgInSc7d1	ceněný
rubínem	rubín	k1gInSc7	rubín
Indie	Indie	k1gFnSc2	Indie
byl	být	k5eAaImAgMnS	být
padmarága	padmarág	k1gMnSc4	padmarág
rubín	rubín	k1gInSc4	rubín
oranžovočervené	oranžovočervený	k2eAgFnSc2d1	oranžovočervená
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
ovšem	ovšem	k9	ovšem
ověřen	ověřen	k2eAgMnSc1d1	ověřen
jako	jako	k8xC	jako
safír	safír	k1gInSc1	safír
padparádža	padparádž	k1gInSc2	padparádž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Staří	starý	k2eAgMnPc1d1	starý
středověcí	středověký	k2eAgMnPc1d1	středověký
astronomové	astronom	k1gMnPc1	astronom
z	z	k7c2	z
rubínů	rubín	k1gInPc2	rubín
brousili	brousit	k5eAaImAgMnP	brousit
čočky	čočka	k1gFnPc4	čočka
do	do	k7c2	do
dalekohledů	dalekohled	k1gInPc2	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gFnSc4	jejich
práci	práce	k1gFnSc4	práce
důležitá	důležitý	k2eAgFnSc1d1	důležitá
podpora	podpora	k1gFnSc1	podpora
boháčů	boháč	k1gMnPc2	boháč
<g/>
,	,	kIx,	,
mecenášů	mecenáš	k1gMnPc2	mecenáš
a	a	k8xC	a
působení	působení	k1gNnSc2	působení
na	na	k7c6	na
dvorech	dvůr	k1gInPc6	dvůr
panovníků	panovník	k1gMnPc2	panovník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rubíny	rubín	k1gInPc1	rubín
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
používat	používat	k5eAaImF	používat
například	například	k6eAd1	například
v	v	k7c6	v
mechanických	mechanický	k2eAgFnPc6d1	mechanická
hodinách	hodina	k1gFnPc6	hodina
jako	jako	k8xS	jako
ložiska	ložisko	k1gNnSc2	ložisko
čepů	čep	k1gInPc2	čep
na	na	k7c4	na
snížení	snížení	k1gNnSc4	snížení
tření	tření	k1gNnSc2	tření
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
by	by	kYmCp3nS	by
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
při	při	k7c6	při
tření	tření	k1gNnSc6	tření
kovu	kov	k1gInSc2	kov
o	o	k7c4	o
kov	kov	k1gInSc4	kov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
nejnovějších	nový	k2eAgInPc2d3	nejnovější
výzkumů	výzkum	k1gInPc2	výzkum
usuzují	usuzovat	k5eAaImIp3nP	usuzovat
někteří	některý	k3yIgMnPc1	některý
odborníci	odborník	k1gMnPc1	odborník
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
třeba	třeba	k9	třeba
P.	P.	kA	P.
Malíková	Malíková	k1gFnSc1	Malíková
a	a	k8xC	a
RNDr.	RNDr.	kA	RNDr.
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Hyršl	Hyršl	k1gMnSc1	Hyršl
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
největší	veliký	k2eAgInSc1d3	veliký
rubín	rubín	k1gInSc1	rubín
<g/>
"	"	kIx"	"
na	na	k7c6	na
svatováclavské	svatováclavský	k2eAgFnSc6d1	Svatováclavská
koruně	koruna	k1gFnSc6	koruna
a	a	k8xC	a
i	i	k9	i
"	"	kIx"	"
<g/>
některé	některý	k3yIgInPc1	některý
další	další	k2eAgInPc1d1	další
<g/>
,	,	kIx,	,
menší	malý	k2eAgInPc1d2	menší
<g/>
"	"	kIx"	"
nejsou	být	k5eNaImIp3nP	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
rubíny	rubín	k1gInPc4	rubín
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rubelity	rubelit	k1gInPc1	rubelit
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
červené	červený	k2eAgInPc4d1	červený
turmalíny	turmalín	k1gInPc4	turmalín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Umělý	umělý	k2eAgInSc1d1	umělý
rubín	rubín	k1gInSc1	rubín
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
prvního	první	k4xOgInSc2	první
laseru	laser	k1gInSc2	laser
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Drahé	drahý	k2eAgInPc1d1	drahý
kameny	kámen	k1gInPc1	kámen
starověku	starověk	k1gInSc2	starověk
<g/>
,	,	kIx,	,
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Onyx	onyx	k1gInSc1	onyx
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Robert	Robert	k1gMnSc1	Robert
Nový	Nový	k1gMnSc1	Nový
<g/>
,	,	kIx,	,
O	o	k7c6	o
duševním	duševní	k2eAgMnSc6d1	duševní
a	a	k8xC	a
léčebném	léčebný	k2eAgNnSc6d1	léčebné
působení	působení	k1gNnSc6	působení
drahokamů	drahokam	k1gInPc2	drahokam
a	a	k8xC	a
cenných	cenný	k2eAgInPc2d1	cenný
nerostů	nerost	k1gInPc2	nerost
</s>
</p>
<p>
<s>
Rubin	Rubin	k1gInSc1	Rubin
<g/>
,	,	kIx,	,
saphir	saphir	k1gInSc1	saphir
<g/>
,	,	kIx,	,
extra	extra	k2eAgInSc1d1	extra
lapis	lapis	k1gInSc1	lapis
no	no	k9	no
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
,	,	kIx,	,
Edelsteine	Edelstein	k1gMnSc5	Edelstein
<g/>
,	,	kIx,	,
Max	Max	k1gMnSc1	Max
Weibel	Weibel	k1gMnSc1	Weibel
<g/>
,	,	kIx,	,
ABC	ABC	kA	ABC
Zurich	Zurich	k1gMnSc1	Zurich
Verlag	Verlag	k1gMnSc1	Verlag
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Korund	korund	k1gInSc1	korund
</s>
</p>
<p>
<s>
Safír	safír	k1gInSc1	safír
</s>
</p>
<p>
<s>
Smaragd	smaragd	k1gInSc1	smaragd
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
rubín	rubín	k1gInSc1	rubín
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
rubín	rubín	k1gInSc1	rubín
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
