<s>
Cibule	cibule	k1gFnSc1	cibule
kuchyňská	kuchyňská	k1gFnSc1	kuchyňská
(	(	kIx(	(
<g/>
Allium	Allium	k1gNnSc1	Allium
cepa	cep	k1gInSc2	cep
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
cibulovitá	cibulovitý	k2eAgFnSc1d1	cibulovitá
zelenina	zelenina	k1gFnSc1	zelenina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
amarylkovitých	amarylkovitý	k2eAgMnPc2d1	amarylkovitý
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
dvouletou	dvouletý	k2eAgFnSc4d1	dvouletá
až	až	k8xS	až
vytrvalou	vytrvalý	k2eAgFnSc4d1	vytrvalá
(	(	kIx(	(
<g/>
spíše	spíše	k9	spíše
jen	jen	k9	jen
teoreticky	teoreticky	k6eAd1	teoreticky
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
rostlinu	rostlina	k1gFnSc4	rostlina
<g/>
,	,	kIx,	,
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
cibulí	cibule	k1gFnSc7	cibule
<g/>
.	.	kIx.	.
</s>
<s>
Stonek	stonek	k1gInSc1	stonek
je	být	k5eAaImIp3nS	být
dosti	dosti	k6eAd1	dosti
robustní	robustní	k2eAgFnSc1d1	robustní
<g/>
,	,	kIx,	,
dole	dole	k6eAd1	dole
až	až	k9	až
3	[number]	k4	3
cm	cm	kA	cm
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dutý	dutý	k2eAgMnSc1d1	dutý
<g/>
.	.	kIx.	.
</s>
<s>
Listy	lista	k1gFnPc1	lista
jsou	být	k5eAaImIp3nP	být
jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
<g/>
,	,	kIx,	,
přisedlé	přisedlý	k2eAgFnPc1d1	přisedlá
<g/>
,	,	kIx,	,
s	s	k7c7	s
listovými	listový	k2eAgFnPc7d1	listová
pochvami	pochva	k1gFnPc7	pochva
<g/>
.	.	kIx.	.
</s>
<s>
Čepele	čepel	k1gFnPc1	čepel
jsou	být	k5eAaImIp3nP	být
celokrajné	celokrajný	k2eAgInPc4d1	celokrajný
<g/>
,	,	kIx,	,
polooblé	polooblý	k2eAgInPc4d1	polooblý
se	se	k3xPyFc4	se
souběžnou	souběžný	k2eAgFnSc7d1	souběžná
žilnatinou	žilnatina	k1gFnSc7	žilnatina
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
oboupohlavní	oboupohlavní	k2eAgInPc1d1	oboupohlavní
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vrcholovém	vrcholový	k2eAgNnSc6d1	vrcholové
květenství	květenství	k1gNnSc6	květenství
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
hlávkovitě	hlávkovitě	k6eAd1	hlávkovitě
stažený	stažený	k2eAgInSc4d1	stažený
zdánlivý	zdánlivý	k2eAgInSc4d1	zdánlivý
okolík	okolík	k1gInSc4	okolík
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
stažené	stažený	k2eAgNnSc4d1	stažené
vrcholičnaté	vrcholičnatý	k2eAgNnSc4d1	vrcholičnaté
květenství	květenství	k1gNnSc4	květenství
zvané	zvaný	k2eAgFnSc2d1	zvaná
šroubel	šroubel	k1gInSc4	šroubel
<g/>
.	.	kIx.	.
</s>
<s>
Květenství	květenství	k1gNnSc1	květenství
je	být	k5eAaImIp3nS	být
podepřeno	podepřít	k5eAaPmNgNnS	podepřít
toulcem	toulec	k1gInSc7	toulec
<g/>
.	.	kIx.	.
</s>
<s>
Pacibulky	pacibulka	k1gFnPc1	pacibulka
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
květenství	květenství	k1gNnSc6	květenství
přítomny	přítomen	k2eAgInPc1d1	přítomen
jen	jen	k9	jen
někdy	někdy	k6eAd1	někdy
<g/>
.	.	kIx.	.
</s>
<s>
Okvětí	okvětí	k1gNnSc1	okvětí
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
6	[number]	k4	6
okvětních	okvětní	k2eAgInPc2d1	okvětní
lístků	lístek	k1gInPc2	lístek
bílé	bílý	k2eAgFnSc2d1	bílá
až	až	k8xS	až
narůžovělé	narůžovělý	k2eAgFnSc2d1	narůžovělá
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
se	s	k7c7	s
středním	střední	k2eAgInSc7d1	střední
zeleným	zelený	k2eAgInSc7d1	zelený
pruhem	pruh	k1gInSc7	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Tyčinek	tyčinka	k1gFnPc2	tyčinka
je	být	k5eAaImIp3nS	být
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Gyneceum	Gyneceum	k1gNnSc1	Gyneceum
je	být	k5eAaImIp3nS	být
složeno	složit	k5eAaPmNgNnS	složit
ze	z	k7c2	z
3	[number]	k4	3
plodolistů	plodolist	k1gInPc2	plodolist
<g/>
,	,	kIx,	,
je	on	k3xPp3gInPc4	on
synkarpní	synkarpnit	k5eAaPmIp3nP	synkarpnit
<g/>
,	,	kIx,	,
semeník	semeník	k1gInSc1	semeník
je	být	k5eAaImIp3nS	být
svrchní	svrchní	k2eAgInSc1d1	svrchní
<g/>
.	.	kIx.	.
</s>
<s>
Plodem	plod	k1gInSc7	plod
je	být	k5eAaImIp3nS	být
tobolka	tobolka	k1gFnSc1	tobolka
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
není	být	k5eNaImIp3nS	být
s	s	k7c7	s
jistotou	jistota	k1gFnSc7	jistota
znám	znát	k5eAaImIp1nS	znát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
pravlastí	pravlast	k1gFnSc7	pravlast
cibule	cibule	k1gFnSc2	cibule
je	být	k5eAaImIp3nS	být
střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
z	z	k7c2	z
planého	planý	k2eAgInSc2d1	planý
druhu	druh	k1gInSc2	druh
Allium	Allium	k1gNnSc4	Allium
oschaninii	oschaninie	k1gFnSc4	oschaninie
(	(	kIx(	(
<g/>
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Allium	Allium	k1gNnSc1	Allium
cepa	cep	k1gInSc2	cep
var.	var.	k?	var.
sylvestre	sylvestr	k1gInSc5	sylvestr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cibule	cibule	k1gFnSc1	cibule
byla	být	k5eAaImAgFnS	být
známa	znám	k2eAgFnSc1d1	známa
již	již	k6eAd1	již
v	v	k7c6	v
dávnověku	dávnověk	k1gInSc6	dávnověk
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jejím	její	k3xOp3gNnSc6	její
užití	užití	k1gNnSc6	užití
se	se	k3xPyFc4	se
dozvídáme	dozvídat	k5eAaImIp1nP	dozvídat
např.	např.	kA	např.
z	z	k7c2	z
hliněných	hliněný	k2eAgFnPc2d1	hliněná
tabulek	tabulka	k1gFnPc2	tabulka
Sumerů	Sumer	k1gInPc2	Sumer
<g/>
,	,	kIx,	,
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
se	se	k3xPyFc4	se
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
Bible	bible	k1gFnPc1	bible
<g/>
,	,	kIx,	,
staroegyptské	staroegyptský	k2eAgInPc1d1	staroegyptský
papyry	papyr	k1gInPc1	papyr
i	i	k8xC	i
učené	učený	k2eAgFnPc1d1	učená
knihy	kniha	k1gFnPc1	kniha
starověké	starověký	k2eAgFnSc2d1	starověká
Indie	Indie	k1gFnSc2	Indie
a	a	k8xC	a
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
léčebnými	léčebný	k2eAgInPc7d1	léčebný
účinky	účinek	k1gInPc7	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
kdysi	kdysi	k6eAd1	kdysi
kromě	kromě	k7c2	kromě
soli	sůl	k1gFnSc2	sůl
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgNnPc2	první
koření	koření	k1gNnPc2	koření
lidových	lidový	k2eAgFnPc2d1	lidová
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Římští	římský	k2eAgMnPc1d1	římský
zemědělci	zemědělec	k1gMnPc1	zemědělec
cibuli	cibule	k1gFnSc4	cibule
nazývali	nazývat	k5eAaImAgMnP	nazývat
"	"	kIx"	"
<g/>
unio	unio	k6eAd1	unio
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
přeložit	přeložit	k5eAaPmF	přeložit
nejen	nejen	k6eAd1	nejen
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
cibule	cibule	k1gFnSc2	cibule
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
jako	jako	k9	jako
"	"	kIx"	"
<g/>
velká	velký	k2eAgFnSc1d1	velká
perla	perla	k1gFnSc1	perla
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Cibule	cibule	k1gFnSc1	cibule
obecná	obecná	k1gFnSc1	obecná
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
různých	různý	k2eAgFnPc6d1	různá
odrůdách	odrůda	k1gFnPc6	odrůda
<g/>
,	,	kIx,	,
lišících	lišící	k2eAgInPc6d1	lišící
se	s	k7c7	s
tvarem	tvar	k1gInSc7	tvar
i	i	k8xC	i
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
ze	z	k7c2	z
semen	semeno	k1gNnPc2	semeno
nebo	nebo	k8xC	nebo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
velká	velký	k2eAgFnSc1d1	velká
<g/>
,	,	kIx,	,
ze	z	k7c2	z
sazeničky	sazenička	k1gFnSc2	sazenička
<g/>
.	.	kIx.	.
</s>
<s>
Sazečky	sazečka	k1gFnPc1	sazečka
jsou	být	k5eAaImIp3nP	být
malé	malý	k2eAgFnPc1d1	malá
cibulky	cibulka	k1gFnPc1	cibulka
vypěstované	vypěstovaný	k2eAgFnPc1d1	vypěstovaná
ze	z	k7c2	z
semen	semeno	k1gNnPc2	semeno
během	během	k7c2	během
léta	léto	k1gNnSc2	léto
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
sklízejí	sklízet	k5eAaImIp3nP	sklízet
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
zimu	zima	k1gFnSc4	zima
se	se	k3xPyFc4	se
uchovávají	uchovávat	k5eAaImIp3nP	uchovávat
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
kolem	kolem	k7c2	kolem
23	[number]	k4	23
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
opět	opět	k6eAd1	opět
sázejí	sázet	k5eAaImIp3nP	sázet
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
teplým	teplý	k2eAgNnSc7d1	teplé
skladováním	skladování	k1gNnSc7	skladování
rostlinu	rostlina	k1gFnSc4	rostlina
oklameme	oklamat	k5eAaPmIp1nP	oklamat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nevykvete	vykvést	k5eNaPmIp3nS	vykvést
a	a	k8xC	a
cibule	cibule	k1gFnSc1	cibule
vyroste	vyrůst	k5eAaPmIp3nS	vyrůst
do	do	k7c2	do
velkých	velký	k2eAgInPc2d1	velký
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
16	[number]	k4	16
(	(	kIx(	(
<g/>
diploid	diploid	k1gInSc1	diploid
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
2	[number]	k4	2
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
32	[number]	k4	32
(	(	kIx(	(
<g/>
tetraploid	tetraploid	k1gInSc1	tetraploid
<g/>
)	)	kIx)	)
či	či	k8xC	či
2	[number]	k4	2
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
64	[number]	k4	64
(	(	kIx(	(
<g/>
oktoploid	oktoploid	k1gInSc1	oktoploid
<g/>
)	)	kIx)	)
Cibule	cibule	k1gFnSc1	cibule
je	být	k5eAaImIp3nS	být
široce	široko	k6eAd1	široko
užívaná	užívaný	k2eAgFnSc1d1	užívaná
při	při	k7c6	při
vaření	vaření	k1gNnSc6	vaření
jako	jako	k8xS	jako
zelenina	zelenina	k1gFnSc1	zelenina
či	či	k8xC	či
koření	koření	k1gNnSc1	koření
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
její	její	k3xOp3gFnPc4	její
cibule	cibule	k1gFnPc4	cibule
<g/>
,	,	kIx,	,
oloupané	oloupaný	k2eAgFnPc4d1	oloupaná
od	od	k7c2	od
vrchních	vrchní	k2eAgFnPc2d1	vrchní
uschlých	uschlý	k2eAgFnPc2d1	uschlá
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
nakrájené	nakrájený	k2eAgFnPc1d1	nakrájená
<g/>
,	,	kIx,	,
řidčeji	řídce	k6eAd2	řídce
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
i	i	k9	i
cibulová	cibulový	k2eAgFnSc1d1	cibulová
nať	nať	k1gFnSc1	nať
<g/>
.	.	kIx.	.
</s>
<s>
Syrová	syrový	k2eAgFnSc1d1	syrová
cibule	cibule	k1gFnSc1	cibule
má	mít	k5eAaImIp3nS	mít
pálivou	pálivý	k2eAgFnSc4d1	pálivá
chuť	chuť	k1gFnSc4	chuť
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
jako	jako	k8xS	jako
příloha	příloha	k1gFnSc1	příloha
k	k	k7c3	k
pečivu	pečivo	k1gNnSc3	pečivo
či	či	k8xC	či
uzeninám	uzenina	k1gFnPc3	uzenina
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
hořčicí	hořčice	k1gFnSc7	hořčice
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Vařenou	vařený	k2eAgFnSc4d1	vařená
cibuli	cibule	k1gFnSc4	cibule
mohou	moct	k5eAaImIp3nP	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
některé	některý	k3yIgFnPc4	některý
zeleninové	zeleninový	k2eAgFnPc4d1	zeleninová
polévky	polévka	k1gFnPc4	polévka
<g/>
.	.	kIx.	.
</s>
<s>
Osmažená	osmažený	k2eAgFnSc1d1	osmažená
cibule	cibule	k1gFnSc1	cibule
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
základ	základ	k1gInSc4	základ
pro	pro	k7c4	pro
cibulovou	cibulový	k2eAgFnSc4d1	cibulová
jíšku	jíška	k1gFnSc4	jíška
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
základ	základ	k1gInSc1	základ
gulášů	guláš	k1gInPc2	guláš
a	a	k8xC	a
perkeltů	perkelt	k1gInPc2	perkelt
<g/>
,	,	kIx,	,
omáček	omáčka	k1gFnPc2	omáčka
<g/>
,	,	kIx,	,
zeleninových	zeleninový	k2eAgFnPc2d1	zeleninová
polévek	polévka	k1gFnPc2	polévka
<g/>
,	,	kIx,	,
některých	některý	k3yIgInPc2	některý
luštěninových	luštěninový	k2eAgInPc2d1	luštěninový
pokrmů	pokrm	k1gInPc2	pokrm
<g/>
,	,	kIx,	,
špenátu	špenát	k1gInSc2	špenát
a	a	k8xC	a
zelí	zelí	k1gNnSc2	zelí
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
smažená	smažený	k2eAgFnSc1d1	smažená
cibulka	cibulka	k1gFnSc1	cibulka
s	s	k7c7	s
tukem	tuk	k1gInSc7	tuk
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
například	například	k6eAd1	například
k	k	k7c3	k
maštění	maštění	k1gNnSc3	maštění
a	a	k8xC	a
ochucení	ochucení	k1gNnSc3	ochucení
vařené	vařený	k2eAgFnSc2d1	vařená
čočky	čočka	k1gFnSc2	čočka
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
pokrmů	pokrm	k1gInPc2	pokrm
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
loupání	loupání	k1gNnSc6	loupání
<g/>
,	,	kIx,	,
krájení	krájení	k1gNnSc6	krájení
či	či	k8xC	či
smažení	smažení	k1gNnSc6	smažení
cibule	cibule	k1gFnSc2	cibule
se	se	k3xPyFc4	se
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
dráždivá	dráždivý	k2eAgFnSc1d1	dráždivá
slzotvorná	slzotvorný	k2eAgFnSc1d1	slzotvorná
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
těkavý	těkavý	k2eAgInSc1d1	těkavý
propathial	propathial	k1gInSc1	propathial
S-oxid	Sxid	k1gInSc1	S-oxid
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
enzymatickým	enzymatický	k2eAgInSc7d1	enzymatický
rozkladem	rozklad	k1gInSc7	rozklad
z	z	k7c2	z
s-I-propenylcysteinsulfoxidu	s-Iropenylcysteinsulfoxid	k1gInSc2	s-I-propenylcysteinsulfoxid
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
látka	látka	k1gFnSc1	látka
pak	pak	k6eAd1	pak
ve	v	k7c6	v
styku	styk	k1gInSc6	styk
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
v	v	k7c6	v
oku	oko	k1gNnSc6	oko
<g/>
)	)	kIx)	)
hydrolyzuje	hydrolyzovat	k5eAaBmIp3nS	hydrolyzovat
na	na	k7c4	na
propanol	propanol	k1gInSc4	propanol
<g/>
,	,	kIx,	,
kyselinu	kyselina	k1gFnSc4	kyselina
sírovou	sírový	k2eAgFnSc4d1	sírová
a	a	k8xC	a
sirovodík	sirovodík	k1gInSc4	sirovodík
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
smažení	smažení	k1gNnSc6	smažení
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
směs	směs	k1gFnSc1	směs
látek	látka	k1gFnPc2	látka
vnímána	vnímán	k2eAgFnSc1d1	vnímána
jako	jako	k8xS	jako
příjemné	příjemný	k2eAgNnSc1d1	příjemné
aroma	aroma	k1gNnSc1	aroma
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
návodů	návod	k1gInPc2	návod
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
pálení	pálení	k1gNnSc4	pálení
očí	oko	k1gNnPc2	oko
při	při	k7c6	při
krájení	krájení	k1gNnSc6	krájení
cibule	cibule	k1gFnSc2	cibule
předejít	předejít	k5eAaPmF	předejít
–	–	k?	–
například	například	k6eAd1	například
krájení	krájení	k1gNnSc1	krájení
cibule	cibule	k1gFnSc2	cibule
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
či	či	k8xC	či
průběžné	průběžný	k2eAgNnSc1d1	průběžné
vlhčení	vlhčení	k1gNnSc1	vlhčení
cibule	cibule	k1gFnSc2	cibule
během	během	k7c2	během
krájení	krájení	k1gNnSc2	krájení
<g/>
,	,	kIx,	,
nošení	nošení	k1gNnSc3	nošení
ochranných	ochranný	k2eAgFnPc2d1	ochranná
brýlí	brýle	k1gFnPc2	brýle
či	či	k8xC	či
kontaktních	kontaktní	k2eAgFnPc2d1	kontaktní
čoček	čočka	k1gFnPc2	čočka
<g/>
,	,	kIx,	,
použití	použití	k1gNnSc2	použití
co	co	k9	co
nejostřejšího	ostrý	k2eAgInSc2d3	nejostřejší
nože	nůž	k1gInSc2	nůž
atd.	atd.	kA	atd.
Z	z	k7c2	z
lékařského	lékařský	k2eAgNnSc2d1	lékařské
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
užití	užití	k1gNnSc1	užití
cibule	cibule	k1gFnSc2	cibule
při	při	k7c6	při
nechutenství	nechutenství	k1gNnSc6	nechutenství
<g/>
,	,	kIx,	,
arterioskleróze	arterioskleróza	k1gFnSc6	arterioskleróza
<g/>
,	,	kIx,	,
potížích	potíž	k1gFnPc6	potíž
zažívání	zažívání	k1gNnPc2	zažívání
<g/>
,	,	kIx,	,
vysokém	vysoký	k2eAgInSc6d1	vysoký
krevním	krevní	k2eAgInSc6d1	krevní
tlaku	tlak	k1gInSc6	tlak
<g/>
,	,	kIx,	,
zánětech	zánět	k1gInPc6	zánět
ústní	ústní	k2eAgFnSc2d1	ústní
dutiny	dutina	k1gFnSc2	dutina
a	a	k8xC	a
hltanu	hltan	k1gInSc2	hltan
,	,	kIx,	,
sirnaté	sirnatý	k2eAgFnPc1d1	sirnatá
sloučeniny	sloučenina	k1gFnPc1	sloučenina
působí	působit	k5eAaImIp3nP	působit
bakteriostaticky	bakteriostaticky	k6eAd1	bakteriostaticky
a	a	k8xC	a
obsažené	obsažený	k2eAgInPc1d1	obsažený
flavonoidy	flavonoid	k1gInPc1	flavonoid
a	a	k8xC	a
fytoncidy	fytoncid	k1gInPc1	fytoncid
protizánětlivě	protizánětlivě	k6eAd1	protizánětlivě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lidovém	lidový	k2eAgNnSc6d1	lidové
léčitelství	léčitelství	k1gNnSc6	léčitelství
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
výčtu	výčet	k1gInSc6	výčet
ještě	ještě	k9	ještě
astma	astma	k1gFnSc1	astma
<g/>
,	,	kIx,	,
zánět	zánět	k1gInSc1	zánět
močového	močový	k2eAgInSc2d1	močový
měchýře	měchýř	k1gInSc2	měchýř
<g/>
,	,	kIx,	,
nepravidelnost	nepravidelnost	k1gFnSc1	nepravidelnost
v	v	k7c6	v
menstruaci	menstruace	k1gFnSc6	menstruace
<g/>
,	,	kIx,	,
cukrovka	cukrovka	k1gFnSc1	cukrovka
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
proti	proti	k7c3	proti
zácpě	zácpa	k1gFnSc3	zácpa
a	a	k8xC	a
bodnutí	bodnutí	k1gNnSc4	bodnutí
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Červená	červený	k2eAgFnSc1d1	červená
cibule	cibule	k1gFnSc1	cibule
snižuje	snižovat	k5eAaImIp3nS	snižovat
křehkost	křehkost	k1gFnSc4	křehkost
cévních	cévní	k2eAgFnPc2d1	cévní
stěn	stěna	k1gFnPc2	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Cibule	cibule	k1gFnSc1	cibule
kuchyňská	kuchyňská	k1gFnSc1	kuchyňská
kvete	kvést	k5eAaImIp3nS	kvést
druhým	druhý	k4xOgInSc7	druhý
rokem	rok	k1gInSc7	rok
<g/>
.	.	kIx.	.
</s>
<s>
Semenné	semenný	k2eAgFnPc1d1	semenná
kultury	kultura	k1gFnPc1	kultura
cibule	cibule	k1gFnSc2	cibule
jsou	být	k5eAaImIp3nP	být
vynikajícím	vynikající	k2eAgInSc7d1	vynikající
zdrojem	zdroj	k1gInSc7	zdroj
nektaru	nektar	k1gInSc2	nektar
i	i	k8xC	i
pylu	pyl	k1gInSc2	pyl
<g/>
.	.	kIx.	.
</s>
<s>
Nektarium	nektarium	k1gNnSc1	nektarium
cibule	cibule	k1gFnSc2	cibule
vyprodukuje	vyprodukovat	k5eAaPmIp3nS	vyprodukovat
za	za	k7c4	za
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
1,32	[number]	k4	1,32
mg	mg	kA	mg
nektaru	nektar	k1gInSc2	nektar
s	s	k7c7	s
cukernatostí	cukernatost	k1gFnSc7	cukernatost
61	[number]	k4	61
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Cukerná	cukerný	k2eAgFnSc1d1	cukerná
hodnota	hodnota	k1gFnSc1	hodnota
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
množství	množství	k1gNnSc1	množství
cukru	cukr	k1gInSc2	cukr
vyprodukovaného	vyprodukovaný	k2eAgInSc2d1	vyprodukovaný
v	v	k7c6	v
květu	květ	k1gInSc6	květ
za	za	k7c4	za
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
0,81	[number]	k4	0,81
mg	mg	kA	mg
<g/>
.	.	kIx.	.
</s>
<s>
Pyl	pyl	k1gInSc4	pyl
cibule	cibule	k1gFnSc2	cibule
sbírají	sbírat	k5eAaImIp3nP	sbírat
včely	včela	k1gFnPc1	včela
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
množství	množství	k1gNnSc6	množství
a	a	k8xC	a
rouskují	rouskovat	k5eAaPmIp3nP	rouskovat
jej	on	k3xPp3gInSc4	on
do	do	k7c2	do
žlutavě	žlutavě	k6eAd1	žlutavě
šedých	šedý	k2eAgFnPc2d1	šedá
rousek	rouska	k1gFnPc2	rouska
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
výživná	výživný	k2eAgFnSc1d1	výživná
hodnota	hodnota	k1gFnSc1	hodnota
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
je	být	k5eAaImIp3nS	být
cibulový	cibulový	k2eAgInSc1d1	cibulový
pach	pach	k1gInSc1	pach
pylových	pylový	k2eAgFnPc2d1	pylová
zásob	zásoba	k1gFnPc2	zásoba
<g/>
,	,	kIx,	,
kterým	který	k3yIgFnPc3	který
mohou	moct	k5eAaImIp3nP	moct
načichnout	načichnout	k5eAaPmF	načichnout
i	i	k9	i
zásoby	zásoba	k1gFnPc4	zásoba
medu	med	k1gInSc2	med
<g/>
.	.	kIx.	.
</s>
<s>
Cibulový	cibulový	k2eAgInSc1d1	cibulový
druhový	druhový	k2eAgInSc1d1	druhový
med	med	k1gInSc1	med
je	být	k5eAaImIp3nS	být
vzácný	vzácný	k2eAgMnSc1d1	vzácný
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
světle	světle	k6eAd1	světle
jantarovou	jantarový	k2eAgFnSc4d1	jantarová
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
jemně	jemně	k6eAd1	jemně
krystalizuje	krystalizovat	k5eAaImIp3nS	krystalizovat
<g/>
.	.	kIx.	.
</s>
<s>
Typická	typický	k2eAgFnSc1d1	typická
je	být	k5eAaImIp3nS	být
pronikavá	pronikavý	k2eAgFnSc1d1	pronikavá
cibulová	cibulový	k2eAgFnSc1d1	cibulová
vůně	vůně	k1gFnSc1	vůně
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
však	však	k9	však
po	po	k7c6	po
uskladnění	uskladnění	k1gNnSc6	uskladnění
medu	med	k1gInSc2	med
zmizí	zmizet	k5eAaPmIp3nP	zmizet
<g/>
.	.	kIx.	.
</s>
<s>
Krájení	krájení	k1gNnSc1	krájení
cibule	cibule	k1gFnSc2	cibule
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
syntaktázu	syntaktáza	k1gFnSc4	syntaktáza
lakrimálního	lakrimální	k2eAgInSc2d1	lakrimální
faktoru	faktor	k1gInSc2	faktor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
aminokyselinami	aminokyselina	k1gFnPc7	aminokyselina
v	v	k7c6	v
cibuli	cibule	k1gFnSc6	cibule
a	a	k8xC	a
mění	měnit	k5eAaImIp3nS	měnit
je	on	k3xPp3gMnPc4	on
na	na	k7c4	na
sulfenovou	sulfenový	k2eAgFnSc4d1	sulfenový
kyselinu	kyselina	k1gFnSc4	kyselina
RSOH	RSOH	kA	RSOH
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
přecházejí	přecházet	k5eAaImIp3nP	přecházet
samovolně	samovolně	k6eAd1	samovolně
na	na	k7c4	na
dráždivý	dráždivý	k2eAgInSc4d1	dráždivý
plyn	plyn	k1gInSc4	plyn
syn-propanthial-S-oxid	synropanthial-Sxida	k1gFnPc2	syn-propanthial-S-oxida
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
CHSO	CHSO	kA	CHSO
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
do	do	k7c2	do
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
dostat	dostat	k5eAaPmF	dostat
i	i	k9	i
do	do	k7c2	do
očí	oko	k1gNnPc2	oko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
nervová	nervový	k2eAgNnPc4d1	nervové
vlákna	vlákno	k1gNnPc4	vlákno
na	na	k7c6	na
rohovce	rohovka	k1gFnSc6	rohovka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zareagují	zareagovat	k5eAaPmIp3nP	zareagovat
aktivací	aktivace	k1gFnSc7	aktivace
slzných	slzný	k2eAgFnPc2d1	slzná
žláz	žláza	k1gFnPc2	žláza
<g/>
.	.	kIx.	.
</s>
<s>
Lidově	lidově	k6eAd1	lidově
řečeno	říct	k5eAaPmNgNnS	říct
při	při	k7c6	při
krájení	krájení	k1gNnSc6	krájení
cibule	cibule	k1gFnSc2	cibule
slzí	slzet	k5eAaImIp3nP	slzet
oči	oko	k1gNnPc4	oko
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
klasické	klasický	k2eAgFnSc2d1	klasická
cibule	cibule	k1gFnSc2	cibule
kuchyňské	kuchyňská	k1gFnSc2	kuchyňská
jsou	být	k5eAaImIp3nP	být
pěstovány	pěstován	k2eAgInPc1d1	pěstován
i	i	k8xC	i
některé	některý	k3yIgInPc1	některý
odlišné	odlišný	k2eAgInPc1d1	odlišný
druhy	druh	k1gInPc1	druh
cibulí	cibule	k1gFnPc2	cibule
<g/>
.	.	kIx.	.
cibule	cibule	k1gFnSc1	cibule
šalotka	šalotka	k1gFnSc1	šalotka
–	–	k?	–
někdy	někdy	k6eAd1	někdy
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
též	též	k9	též
česnek	česnek	k1gInSc1	česnek
askalonský	askalonský	k2eAgInSc1d1	askalonský
či	či	k8xC	či
množilka	množilka	k1gFnSc1	množilka
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
udávána	udáván	k2eAgFnSc1d1	udávána
jako	jako	k8xS	jako
samostatný	samostatný	k2eAgInSc1d1	samostatný
druh	druh	k1gInSc1	druh
pod	pod	k7c7	pod
jmény	jméno	k1gNnPc7	jméno
Allium	Allium	k1gNnSc1	Allium
ascalonicum	ascalonicum	k1gInSc1	ascalonicum
auct	auct	k1gMnSc1	auct
<g/>
.	.	kIx.	.
non	non	k?	non
L.	L.	kA	L.
nebo	nebo	k8xC	nebo
Allium	Allium	k1gNnSc4	Allium
salota	salot	k1gMnSc4	salot
Dost	dost	k6eAd1	dost
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
většinou	většina	k1gFnSc7	většina
zahrnovaná	zahrnovaný	k2eAgFnSc1d1	zahrnovaná
<g />
.	.	kIx.	.
</s>
<s>
pod	pod	k7c4	pod
druh	druh	k1gInSc4	druh
Allium	Allium	k1gNnSc4	Allium
cepa	cep	k1gInSc2	cep
jako	jako	k8xS	jako
Allium	Allium	k1gNnSc1	Allium
cepa	cep	k1gInSc2	cep
var.	var.	k?	var.
aggegatum	aggegatum	k1gNnSc1	aggegatum
cibule	cibule	k1gFnSc2	cibule
zimní	zimní	k2eAgNnSc1d1	zimní
(	(	kIx(	(
<g/>
Allium	Allium	k1gNnSc1	Allium
fistulosum	fistulosum	k1gNnSc1	fistulosum
L.	L.	kA	L.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
zvaná	zvaný	k2eAgFnSc1d1	zvaná
ošlejch	ošlejch	k1gInSc1	ošlejch
cibule	cibule	k1gFnSc2	cibule
prorůstavá	prorůstavý	k2eAgFnSc1d1	prorůstavý
(	(	kIx(	(
<g/>
Allium	Allium	k1gNnSc1	Allium
×	×	k?	×
proliferum	proliferum	k1gNnSc1	proliferum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
také	také	k9	také
cibule	cibule	k1gFnSc1	cibule
poschoďová	poschoďový	k2eAgFnSc1d1	poschoďová
<g/>
,	,	kIx,	,
či	či	k8xC	či
méně	málo	k6eAd2	málo
správně	správně	k6eAd1	správně
jako	jako	k8xC	jako
zimní	zimní	k2eAgFnSc1d1	zimní
či	či	k8xC	či
sibiřská	sibiřský	k2eAgFnSc1d1	sibiřská
cibule	cibule	k1gFnSc1	cibule
Tabulka	tabulka	k1gFnSc1	tabulka
udává	udávat	k5eAaImIp3nS	udávat
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
průměrný	průměrný	k2eAgInSc1d1	průměrný
obsah	obsah	k1gInSc1	obsah
živin	živina	k1gFnPc2	živina
<g/>
,	,	kIx,	,
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
vitamínů	vitamín	k1gInPc2	vitamín
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
nutričních	nutriční	k2eAgInPc2d1	nutriční
parametrů	parametr	k1gInPc2	parametr
zjištěných	zjištěný	k2eAgInPc2d1	zjištěný
v	v	k7c6	v
cibuli	cibule	k1gFnSc6	cibule
<g/>
.	.	kIx.	.
</s>
<s>
Cibule	cibule	k1gFnSc1	cibule
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
prvním	první	k4xOgFnPc3	první
kulturním	kulturní	k2eAgFnPc3d1	kulturní
plodinám	plodina	k1gFnPc3	plodina
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
silice	silice	k1gFnSc1	silice
(	(	kIx(	(
<g/>
éterické	éterický	k2eAgInPc1d1	éterický
oleje	olej	k1gInPc1	olej
<g/>
)	)	kIx)	)
<g/>
́	́	k?	́
<g/>
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
síry	síra	k1gFnSc2	síra
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
množství	množství	k1gNnSc2	množství
jsou	být	k5eAaImIp3nP	být
cibule	cibule	k1gFnPc1	cibule
ostré	ostrý	k2eAgFnPc1d1	ostrá
<g/>
,	,	kIx,	,
středně	středně	k6eAd1	středně
ostré	ostrý	k2eAgFnSc2d1	ostrá
a	a	k8xC	a
sladké	sladký	k2eAgFnSc2d1	sladká
chuti	chuť	k1gFnSc2	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
vitamíny	vitamín	k1gInPc4	vitamín
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
více	hodně	k6eAd2	hodně
vitamínu	vitamín	k1gInSc2	vitamín
B1	B1	k1gMnSc2	B1
a	a	k8xC	a
B	B	kA	B
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
vitamínu	vitamín	k1gInSc2	vitamín
C.	C.	kA	C.
Provitamín	provitamín	k1gInSc1	provitamín
A	a	k9	a
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
především	především	k9	především
v	v	k7c6	v
nati	nať	k1gFnSc6	nať
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
i	i	k9	i
vyšší	vysoký	k2eAgInSc1d2	vyšší
obsah	obsah	k1gInSc1	obsah
vitamínu	vitamín	k1gInSc2	vitamín
C.	C.	kA	C.
Cibule	cibule	k1gFnPc1	cibule
působí	působit	k5eAaImIp3nP	působit
sedativně	sedativně	k6eAd1	sedativně
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
slizy	sliz	k1gInPc4	sliz
a	a	k8xC	a
pektiny	pektin	k1gInPc4	pektin
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
dobře	dobře	k6eAd1	dobře
i	i	k9	i
při	při	k7c6	při
některých	některý	k3yIgFnPc6	některý
střevních	střevní	k2eAgFnPc6d1	střevní
chorobách	choroba	k1gFnPc6	choroba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
cibuli	cibule	k1gFnSc6	cibule
přítomné	přítomný	k2eAgFnSc2d1	přítomná
fytoncidy	fytoncida	k1gFnSc2	fytoncida
ničí	ničit	k5eAaImIp3nS	ničit
škodlivé	škodlivý	k2eAgInPc4d1	škodlivý
mikroby	mikrob	k1gInPc4	mikrob
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mnoho	mnoho	k6eAd1	mnoho
křemíku	křemík	k1gInSc2	křemík
<g/>
,	,	kIx,	,
draslíku	draslík	k1gInSc2	draslík
<g/>
,	,	kIx,	,
síry	síra	k1gFnSc2	síra
a	a	k8xC	a
dusíkatých	dusíkatý	k2eAgFnPc2d1	dusíkatá
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
každoročně	každoročně	k6eAd1	každoročně
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
cibule	cibule	k1gFnSc2	cibule
pěstována	pěstovat	k5eAaImNgFnS	pěstovat
na	na	k7c4	na
3	[number]	k4	3
642	[number]	k4	642
000	[number]	k4	000
hektarů	hektar	k1gInPc2	hektar
<g/>
.	.	kIx.	.
</s>
<s>
Pěstování	pěstování	k1gNnSc1	pěstování
pro	pro	k7c4	pro
domácí	domácí	k2eAgInSc4d1	domácí
trh	trh	k1gInSc4	trh
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaPmIp3nS	věnovat
170	[number]	k4	170
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
8	[number]	k4	8
%	%	kIx~	%
světové	světový	k2eAgFnSc2d1	světová
produkce	produkce	k1gFnSc2	produkce
je	být	k5eAaImIp3nS	být
předmětem	předmět	k1gInSc7	předmět
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
původních	původní	k2eAgFnPc2d1	původní
pěstitelských	pěstitelský	k2eAgFnPc2d1	pěstitelská
oblastí	oblast	k1gFnPc2	oblast
si	se	k3xPyFc3	se
zachoval	zachovat	k5eAaPmAgInS	zachovat
přední	přední	k2eAgNnSc4d1	přední
postavení	postavení	k1gNnSc4	postavení
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
pouze	pouze	k6eAd1	pouze
Egypt	Egypt	k1gInSc1	Egypt
<g/>
;	;	kIx,	;
z	z	k7c2	z
dalších	další	k2eAgMnPc2d1	další
významných	významný	k2eAgMnPc2d1	významný
exportérů	exportér	k1gMnPc2	exportér
jmenujme	jmenovat	k5eAaImRp1nP	jmenovat
alespoň	alespoň	k9	alespoň
USA	USA	kA	USA
<g/>
,	,	kIx,	,
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
Holandsko	Holandsko	k1gNnSc1	Holandsko
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
a	a	k8xC	a
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Cibule	cibule	k1gFnSc1	cibule
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
česnek	česnek	k1gInSc4	česnek
<g/>
)	)	kIx)	)
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
látku	látka	k1gFnSc4	látka
s	s	k7c7	s
antibiotickými	antibiotický	k2eAgInPc7d1	antibiotický
účinky	účinek	k1gInPc7	účinek
allicin	allicin	k2eAgInSc1d1	allicin
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
níž	jenž	k3xRgFnSc3	jenž
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
jako	jako	k9	jako
přídatná	přídatný	k2eAgFnSc1d1	přídatná
léčba	léčba	k1gFnSc1	léčba
nachlazení	nachlazení	k1gNnSc2	nachlazení
(	(	kIx(	(
<g/>
bolesti	bolest	k1gFnPc1	bolest
horních	horní	k2eAgFnPc2d1	horní
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
kašel	kašel	k1gInSc1	kašel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vhodnou	vhodný	k2eAgFnSc7d1	vhodná
formou	forma	k1gFnSc7	forma
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
cibulový	cibulový	k2eAgInSc4d1	cibulový
čaj	čaj	k1gInSc4	čaj
či	či	k8xC	či
sirup	sirup	k1gInSc4	sirup
<g/>
.	.	kIx.	.
</s>
<s>
Stará	starý	k2eAgFnSc1d1	stará
moudrost	moudrost	k1gFnSc1	moudrost
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Člověk	člověk	k1gMnSc1	člověk
spíše	spíše	k9	spíše
může	moct	k5eAaImIp3nS	moct
vařit	vařit	k5eAaImF	vařit
bez	bez	k7c2	bez
ohně	oheň	k1gInSc2	oheň
než	než	k8xS	než
bez	bez	k7c2	bez
cibule	cibule	k1gFnSc2	cibule
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
DOSTÁL	Dostál	k1gMnSc1	Dostál
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Nová	nový	k2eAgFnSc1d1	nová
Květena	květena	k1gFnSc1	květena
ČSSR	ČSSR	kA	ČSSR
<g/>
,	,	kIx,	,
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
LEYNER	LEYNER	kA	LEYNER
<g/>
,	,	kIx,	,
Mark	Mark	k1gMnSc1	Mark
<g/>
;	;	kIx,	;
GOLDBERG	GOLDBERG	kA	GOLDBERG
<g/>
,	,	kIx,	,
Billy	Bill	k1gMnPc4	Bill
<g/>
.	.	kIx.	.
</s>
<s>
Proč	proč	k6eAd1	proč
i	i	k9	i
muži	muž	k1gMnPc1	muž
mají	mít	k5eAaImIp3nP	mít
bradavky	bradavka	k1gFnPc4	bradavka
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
253	[number]	k4	253
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
272	[number]	k4	272
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
cibule	cibule	k1gFnSc1	cibule
kuchyňská	kuchyňský	k2eAgFnSc1d1	kuchyňská
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc4	Commons
www.avicenna.cz	www.avicenna.cz	k1gInSc1	www.avicenna.cz
Flóra	Flóra	k1gFnSc1	Flóra
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
</s>
