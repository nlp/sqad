<s>
Cibule	cibule	k1gFnSc1	cibule
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
česnek	česnek	k1gInSc4	česnek
<g/>
)	)	kIx)	)
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
látku	látka	k1gFnSc4	látka
s	s	k7c7	s
antibiotickými	antibiotický	k2eAgInPc7d1	antibiotický
účinky	účinek	k1gInPc7	účinek
allicin	allicin	k2eAgInSc1d1	allicin
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
níž	jenž	k3xRgFnSc3	jenž
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
jako	jako	k9	jako
přídatná	přídatný	k2eAgFnSc1d1	přídatná
léčba	léčba	k1gFnSc1	léčba
nachlazení	nachlazení	k1gNnSc2	nachlazení
(	(	kIx(	(
<g/>
bolesti	bolest	k1gFnPc1	bolest
horních	horní	k2eAgFnPc2d1	horní
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
kašel	kašel	k1gInSc1	kašel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
