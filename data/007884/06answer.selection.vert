<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
je	být	k5eAaImIp3nS	být
Budapešť	Budapešť	k1gFnSc1	Budapešť
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
spojením	spojení	k1gNnSc7	spojení
Budína	Budín	k1gInSc2	Budín
(	(	kIx(	(
<g/>
západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pešti	Pešť	k1gFnPc4	Pešť
(	(	kIx(	(
<g/>
východní	východní	k2eAgFnSc4d1	východní
část	část	k1gFnSc4	část
<g/>
)	)	kIx)	)
a	a	k8xC	a
Starého	Starého	k2eAgInSc2d1	Starého
Budína	Budín	k1gInSc2	Budín
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
Óbuda	Óbud	k1gMnSc2	Óbud
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozkládajícího	rozkládající	k2eAgMnSc2d1	rozkládající
se	se	k3xPyFc4	se
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
části	část	k1gFnSc6	část
dnešního	dnešní	k2eAgNnSc2d1	dnešní
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
