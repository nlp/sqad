<s>
Vlajka	vlajka	k1gFnSc1
Itálie	Itálie	k1gFnSc1
(	(	kIx(
<g/>
italská	italský	k2eAgFnSc1d1
trikolóra	trikolóra	k1gFnSc1
<g/>
)	)	kIx)
má	mít	k5eAaImIp3nS
tři	tři	k4xCgInPc1
stejně	stejně	k6eAd1
široké	široký	k2eAgInPc1d1
<g/>
,	,	kIx,
svislé	svislý	k2eAgInPc1d1
pruhy	pruh	k1gInPc1
v	v	k7c6
barvě	barva	k1gFnSc6
zelené	zelená	k1gFnSc2
<g/>
,	,	kIx,
bílé	bílý	k2eAgFnSc2d1
a	a	k8xC
červené	červený	k2eAgFnSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Byla	být	k5eAaImAgFnS
odvozena	odvodit	k5eAaPmNgFnS
z	z	k7c2
francouzské	francouzský	k2eAgFnSc2d1
vlajky	vlajka	k1gFnSc2
<g/>
,	,	kIx,
modrý	modrý	k2eAgInSc1d1
pruh	pruh	k1gInSc1
však	však	k9
byl	být	k5eAaImAgInS
nahrazen	nahradit	k5eAaPmNgInS
zeleným	zelený	k2eAgInSc7d1
pruhem	pruh	k1gInSc7
(	(	kIx(
<g/>
tato	tento	k3xDgFnSc1
barva	barva	k1gFnSc1
byla	být	k5eAaImAgFnS
od	od	k7c2
francouzské	francouzský	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
považovaná	považovaný	k2eAgFnSc1d1
za	za	k7c4
symbol	symbol	k1gInSc4
svobody	svoboda	k1gFnSc2
<g/>
,	,	kIx,
rovnosti	rovnost	k1gFnSc2
a	a	k8xC
nového	nový	k2eAgInSc2d1
politického	politický	k2eAgInSc2d1
pořádku	pořádek	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>