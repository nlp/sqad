<s desamb="1">
Zelená	Zelená	k1gFnSc1
reprezentuje	reprezentovat	k5eAaImIp3nS
zemi	zem	k1gFnSc4
plání	pláň	k1gFnPc2
a	a	k8xC
kopců	kopec	k1gInPc2
<g/>
,	,	kIx,
bílá	bílý	k2eAgFnSc1d1
symbolizuje	symbolizovat	k5eAaImIp3nS
sníh	sníh	k1gInSc4
z	z	k7c2
Alp	Alpy	k1gFnPc2
a	a	k8xC
červená	červený	k2eAgFnSc1d1
symbolizuje	symbolizovat	k5eAaImIp3nS
krev	krev	k1gFnSc4
prolitou	prolitý	k2eAgFnSc4d1
ve	v	k7c6
válkách	válka	k1gFnPc6
za	za	k7c4
italskou	italský	k2eAgFnSc4d1
nezávislost	nezávislost	k1gFnSc4
<g/>
.	.	kIx.
</s>