<s>
Ivan	Ivan	k1gMnSc1	Ivan
Trojan	Trojan	k1gMnSc1	Trojan
(	(	kIx(	(
<g/>
*	*	kIx~	*
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1964	[number]	k4	1964
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
účinkuje	účinkovat	k5eAaImIp3nS	účinkovat
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
<g/>
,	,	kIx,	,
televizi	televize	k1gFnSc6	televize
i	i	k8xC	i
filmu	film	k1gInSc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
držitelem	držitel	k1gMnSc7	držitel
šesti	šest	k4xCc2	šest
Českých	český	k2eAgInPc2d1	český
lvů	lev	k1gInPc2	lev
<g/>
.	.	kIx.	.
</s>
<s>
Prosadil	prosadit	k5eAaPmAgInS	prosadit
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
dabingu	dabing	k1gInSc6	dabing
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
se	se	k3xPyFc4	se
aktivně	aktivně	k6eAd1	aktivně
věnoval	věnovat	k5eAaImAgMnS	věnovat
basketbalu	basketbal	k1gInSc2	basketbal
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
také	také	k9	také
studoval	studovat	k5eAaImAgMnS	studovat
sportovní	sportovní	k2eAgNnSc4d1	sportovní
gymnázium	gymnázium	k1gNnSc4	gymnázium
a	a	k8xC	a
chtěl	chtít	k5eAaImAgMnS	chtít
pokračovat	pokračovat	k5eAaImF	pokračovat
na	na	k7c6	na
FTVS	FTVS	kA	FTVS
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
nakonec	nakonec	k6eAd1	nakonec
místo	místo	k1gNnSc4	místo
sportovní	sportovní	k2eAgFnSc2d1	sportovní
kariéry	kariéra	k1gFnSc2	kariéra
zvolil	zvolit	k5eAaPmAgMnS	zvolit
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
otce	otec	k1gMnSc2	otec
kariéru	kariéra	k1gFnSc4	kariéra
hereckou	herecký	k2eAgFnSc7d1	herecká
a	a	k8xC	a
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
herectví	herectví	k1gNnSc4	herectví
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
DAMU	DAMU	kA	DAMU
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
ukončil	ukončit	k5eAaPmAgInS	ukončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
základní	základní	k2eAgFnSc4d1	základní
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
službu	služba	k1gFnSc4	služba
do	do	k7c2	do
Armádního	armádní	k2eAgInSc2d1	armádní
uměleckého	umělecký	k2eAgInSc2d1	umělecký
souboru	soubor	k1gInSc2	soubor
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
však	však	k9	však
byl	být	k5eAaImAgInS	být
převelen	převelet	k5eAaPmNgInS	převelet
k	k	k7c3	k
jednotce	jednotka	k1gFnSc3	jednotka
v	v	k7c6	v
Humenném	Humenné	k1gNnSc6	Humenné
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
podepsal	podepsat	k5eAaPmAgMnS	podepsat
petici	petice	k1gFnSc4	petice
za	za	k7c4	za
propuštění	propuštění	k1gNnSc4	propuštění
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
a	a	k8xC	a
následně	následně	k6eAd1	následně
i	i	k9	i
petici	petice	k1gFnSc4	petice
Několik	několik	k4yIc1	několik
vět	věta	k1gFnPc2	věta
a	a	k8xC	a
oba	dva	k4xCgInPc4	dva
své	svůj	k3xOyFgInPc4	svůj
podpisy	podpis	k1gInPc4	podpis
odmítal	odmítat	k5eAaImAgMnS	odmítat
odvolat	odvolat	k5eAaPmF	odvolat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Humenném	Humenné	k1gNnSc6	Humenné
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
zdravotník	zdravotník	k1gMnSc1	zdravotník
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
se	se	k3xPyFc4	se
do	do	k7c2	do
AUSu	AUSus	k1gInSc2	AUSus
vrátil	vrátit	k5eAaPmAgInS	vrátit
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
kolegy	kolega	k1gMnPc7	kolega
nacvičil	nacvičit	k5eAaBmAgMnS	nacvičit
pásmo	pásmo	k1gNnSc4	pásmo
s	s	k7c7	s
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
zakázanými	zakázaný	k2eAgMnPc7d1	zakázaný
autory	autor	k1gMnPc7	autor
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Karel	Karel	k1gMnSc1	Karel
Kryl	Kryl	k1gMnSc1	Kryl
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
založil	založit	k5eAaPmAgMnS	založit
pobočku	pobočka	k1gFnSc4	pobočka
Občanského	občanský	k2eAgNnSc2d1	občanské
fóra	fórum	k1gNnSc2	fórum
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
herecké	herecký	k2eAgFnSc2d1	herecká
rodiny	rodina	k1gFnSc2	rodina
-	-	kIx~	-
jeho	jeho	k3xOp3gMnSc7	jeho
otcem	otec	k1gMnSc7	otec
je	být	k5eAaImIp3nS	být
herec	herec	k1gMnSc1	herec
Ladislav	Ladislav	k1gMnSc1	Ladislav
Trojan	Trojan	k1gMnSc1	Trojan
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Ondřej	Ondřej	k1gMnSc1	Ondřej
Trojan	Trojan	k1gMnSc1	Trojan
je	být	k5eAaImIp3nS	být
filmový	filmový	k2eAgMnSc1d1	filmový
producent	producent	k1gMnSc1	producent
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
<g/>
,	,	kIx,	,
herečkou	herečka	k1gFnSc7	herečka
Klárou	Klára	k1gFnSc7	Klára
Pollertovou-Trojanovou	Pollertovou-Trojanová	k1gFnSc7	Pollertovou-Trojanová
(	(	kIx(	(
<g/>
sestrou	sestra	k1gFnSc7	sestra
olympijského	olympijský	k2eAgMnSc2d1	olympijský
vítěze	vítěz	k1gMnSc2	vítěz
Lukáše	Lukáš	k1gMnSc2	Lukáš
Pollerta	Pollert	k1gMnSc2	Pollert
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
čtyři	čtyři	k4xCgMnPc4	čtyři
syny	syn	k1gMnPc4	syn
-	-	kIx~	-
Františka	František	k1gMnSc4	František
(	(	kIx(	(
<g/>
*	*	kIx~	*
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Josefa	Josefa	k1gFnSc1	Josefa
(	(	kIx(	(
<g/>
*	*	kIx~	*
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Antonína	Antonín	k1gMnSc4	Antonín
(	(	kIx(	(
<g/>
*	*	kIx~	*
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
a	a	k8xC	a
Václava	Václav	k1gMnSc4	Václav
(	(	kIx(	(
<g/>
*	*	kIx~	*
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
začínal	začínat	k5eAaImAgMnS	začínat
v	v	k7c6	v
Realistickém	realistický	k2eAgNnSc6d1	realistické
divadle	divadlo	k1gNnSc6	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1990	[number]	k4	1990
-	-	kIx~	-
1997	[number]	k4	1997
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
na	na	k7c6	na
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
tehdy	tehdy	k6eAd1	tehdy
nově	nově	k6eAd1	nově
vznikajícího	vznikající	k2eAgInSc2d1	vznikající
souboru	soubor	k1gInSc2	soubor
Dejvického	dejvický	k2eAgNnSc2d1	Dejvické
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působí	působit	k5eAaImIp3nS	působit
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
byl	být	k5eAaImAgMnS	být
Ivan	Ivan	k1gMnSc1	Ivan
Trojan	Trojan	k1gMnSc1	Trojan
nominován	nominovat	k5eAaBmNgMnS	nominovat
za	za	k7c4	za
roli	role	k1gFnSc4	role
hejtmana	hejtman	k1gMnSc2	hejtman
v	v	k7c6	v
Revizorovi	revizor	k1gMnSc6	revizor
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc2	režie
Sergej	Sergej	k1gMnSc1	Sergej
Fedotov	Fedotov	k1gInSc1	Fedotov
<g/>
)	)	kIx)	)
na	na	k7c4	na
Cenu	cena	k1gFnSc4	cena
Thálie	Thálie	k1gFnSc2	Thálie
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
cenu	cena	k1gFnSc4	cena
pak	pak	k6eAd1	pak
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
získal	získat	k5eAaPmAgInS	získat
za	za	k7c4	za
titulní	titulní	k2eAgFnSc4d1	titulní
roli	role	k1gFnSc4	role
v	v	k7c6	v
adaptaci	adaptace	k1gFnSc6	adaptace
románu	román	k1gInSc2	román
I.	I.	kA	I.
A.	A.	kA	A.
Gončarova	Gončarův	k2eAgMnSc4d1	Gončarův
Oblomov	Oblomov	k1gMnSc1	Oblomov
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
Miroslav	Miroslav	k1gMnSc1	Miroslav
Krobot	Krobot	k?	Krobot
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
jeho	jeho	k3xOp3gFnPc7	jeho
výraznými	výrazný	k2eAgFnPc7d1	výrazná
rolemi	role	k1gFnPc7	role
v	v	k7c6	v
Dejvickém	dejvický	k2eAgNnSc6d1	Dejvické
divadle	divadlo	k1gNnSc6	divadlo
byli	být	k5eAaImAgMnP	být
Profesor	profesor	k1gMnSc1	profesor
Maillard	Maillard	k1gMnSc1	Maillard
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
Utišující	utišující	k2eAgFnSc1d1	utišující
metoda	metoda	k1gFnSc1	metoda
<g/>
,	,	kIx,	,
Karamazov	Karamazov	k1gInSc1	Karamazov
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
Bratři	bratr	k1gMnPc1	bratr
Karamazovi	Karamazův	k2eAgMnPc1d1	Karamazův
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
Příběhy	příběh	k1gInPc1	příběh
obyčejného	obyčejný	k2eAgNnSc2d1	obyčejné
šílenství	šílenství	k1gNnSc2	šílenství
<g/>
,	,	kIx,	,
Alexandr	Alexandr	k1gMnSc1	Alexandr
Veršinin	Veršinin	k2eAgMnSc1d1	Veršinin
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
Tři	tři	k4xCgFnPc1	tři
sestry	sestra	k1gFnPc1	sestra
<g/>
,	,	kIx,	,
či	či	k8xC	či
Lev	Lev	k1gMnSc1	Lev
Sergejevič	Sergejevič	k1gMnSc1	Sergejevič
Těrmen	Těrmen	k2eAgMnSc1d1	Těrmen
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
Teremin	Teremin	k1gInSc1	Teremin
<g/>
.	.	kIx.	.
1997	[number]	k4	1997
Profesor	profesor	k1gMnSc1	profesor
<g />
.	.	kIx.	.
</s>
<s>
Maillard	Maillard	k1gMnSc1	Maillard
-	-	kIx~	-
Edgar	Edgar	k1gMnSc1	Edgar
Allan	Allan	k1gMnSc1	Allan
Poe	Poe	k1gMnSc1	Poe
<g/>
:	:	kIx,	:
Utišující	utišující	k2eAgFnSc1d1	utišující
metoda	metoda	k1gFnSc1	metoda
-	-	kIx~	-
nominace	nominace	k1gFnSc1	nominace
na	na	k7c4	na
Cenu	cena	k1gFnSc4	cena
Thálie	Thálie	k1gFnSc2	Thálie
1998	[number]	k4	1998
Anton	Anton	k1gMnSc1	Anton
Antonovič	Antonovič	k1gMnSc1	Antonovič
Skvoznik	Skvoznik	k1gMnSc1	Skvoznik
-	-	kIx~	-
Duchanovskij	Duchanovskij	k1gMnSc1	Duchanovskij
<g/>
,	,	kIx,	,
hejtman	hejtman	k1gMnSc1	hejtman
-	-	kIx~	-
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Vasiljevič	Vasiljevič	k1gMnSc1	Vasiljevič
Gogol	Gogol	k1gMnSc1	Gogol
<g/>
:	:	kIx,	:
Revizor	revizor	k1gMnSc1	revizor
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
Clapcott	Clapcotta	k1gFnPc2	Clapcotta
-	-	kIx~	-
Howard	Howard	k1gMnSc1	Howard
Barker	Barker	k1gMnSc1	Barker
<g/>
:	:	kIx,	:
Pazour	Pazour	k1gMnSc1	Pazour
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
Indián	Indián	k1gMnSc1	Indián
-	-	kIx~	-
Gabriel	Gabriel	k1gMnSc1	Gabriel
García	García	k1gMnSc1	García
Márquez	Márquez	k1gMnSc1	Márquez
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
Neuvěřitelný	uvěřitelný	k2eNgInSc1d1	neuvěřitelný
a	a	k8xC	a
tklivý	tklivý	k2eAgInSc1d1	tklivý
příběh	příběh	k1gInSc1	příběh
o	o	k7c6	o
bezelstné	bezelstný	k2eAgFnSc6d1	bezelstná
Eréndiře	Eréndira	k1gFnSc6	Eréndira
a	a	k8xC	a
její	její	k3xOp3gFnSc3	její
ukrutné	ukrutný	k2eAgFnSc3d1	ukrutná
babičce	babička	k1gFnSc3	babička
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
Karamazov	Karamazovo	k1gNnPc2	Karamazovo
<g/>
,	,	kIx,	,
Čert	čert	k1gMnSc1	čert
-	-	kIx~	-
Fjodor	Fjodor	k1gMnSc1	Fjodor
Michajlovič	Michajlovič	k1gMnSc1	Michajlovič
Dostojevskij	Dostojevskij	k1gMnSc1	Dostojevskij
<g/>
,	,	kIx,	,
Evald	Evald	k1gMnSc1	Evald
Schorm	Schorm	k1gInSc1	Schorm
<g/>
:	:	kIx,	:
Bratři	bratr	k1gMnPc1	bratr
Karamazovi	Karamazův	k2eAgMnPc1d1	Karamazův
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
Ilja	Ilja	k1gMnSc1	Ilja
Iljič	Iljič	k1gMnSc1	Iljič
Oblomov	Oblomov	k1gMnSc1	Oblomov
-	-	kIx~	-
Ivan	Ivan	k1gMnSc1	Ivan
Alexandrovič	Alexandrovič	k1gMnSc1	Alexandrovič
Gončarov	Gončarov	k1gInSc1	Gončarov
<g/>
:	:	kIx,	:
Oblomov	Oblomov	k1gMnSc1	Oblomov
-	-	kIx~	-
Cena	cena	k1gFnSc1	cena
Thálie	Thálie	k1gFnSc1	Thálie
<g/>
,	,	kIx,	,
nominace	nominace	k1gFnSc1	nominace
na	na	k7c4	na
<g />
.	.	kIx.	.
</s>
<s>
Cenu	cena	k1gFnSc4	cena
A.	A.	kA	A.
Radoka	Radoka	k1gFnSc1	Radoka
2001	[number]	k4	2001
Petr	Petr	k1gMnSc1	Petr
-	-	kIx~	-
Petr	Petr	k1gMnSc1	Petr
Zelenka	Zelenka	k1gMnSc1	Zelenka
<g/>
:	:	kIx,	:
Příběhy	příběh	k1gInPc1	příběh
obyčejného	obyčejný	k2eAgNnSc2d1	obyčejné
šílenství	šílenství	k1gNnSc2	šílenství
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
Alexandr	Alexandr	k1gMnSc1	Alexandr
Veršinin	Veršinin	k2eAgMnSc1d1	Veršinin
-	-	kIx~	-
Anton	Anton	k1gMnSc1	Anton
Pavlovič	Pavlovič	k1gMnSc1	Pavlovič
Čechov	Čechov	k1gMnSc1	Čechov
<g/>
:	:	kIx,	:
Tři	tři	k4xCgFnPc1	tři
sestry	sestra	k1gFnPc1	sestra
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
Theo	Thea	k1gFnSc5	Thea
-	-	kIx~	-
Melissa	Melissa	k1gFnSc1	Melissa
James	James	k1gInSc1	James
Gibsonová	Gibsonová	k1gFnSc1	Gibsonová
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
sic	sic	k6eAd1	sic
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
Stanley	Stanlea	k1gFnPc1	Stanlea
-	-	kIx~	-
Tennessee	Tennessee	k1gNnPc1	Tennessee
Williams	Williamsa	k1gFnPc2	Williamsa
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
Tramvaj	tramvaj	k1gFnSc1	tramvaj
do	do	k7c2	do
stanice	stanice	k1gFnSc2	stanice
Touha	touha	k1gFnSc1	touha
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
William	William	k1gInSc1	William
-	-	kIx~	-
Karel	Karel	k1gMnSc1	Karel
František	František	k1gMnSc1	František
Tománek	Tománek	k1gMnSc1	Tománek
<g/>
:	:	kIx,	:
KFT	KFT	kA	KFT
<g/>
/	/	kIx~	/
<g/>
sendviče	sendvič	k1gInPc1	sendvič
reality	realita	k1gFnSc2	realita
<g/>
®	®	k?	®
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
Lev	Lev	k1gMnSc1	Lev
Sergejevič	Sergejevič	k1gMnSc1	Sergejevič
Teremin	Teremin	k2eAgMnSc1d1	Teremin
-	-	kIx~	-
Petr	Petr	k1gMnSc1	Petr
Zelenka	Zelenka	k1gMnSc1	Zelenka
<g/>
:	:	kIx,	:
Teremin	Teremin	k1gInSc1	Teremin
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
Agent	agent	k1gMnSc1	agent
-	-	kIx~	-
Doyle	Doyle	k1gInSc1	Doyle
Doubt	Doubt	k1gInSc1	Doubt
<g/>
:	:	kIx,	:
Černá	černý	k2eAgFnSc1d1	černá
díra	díra	k1gFnSc1	díra
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
Ivan	Ivana	k1gFnPc2	Ivana
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
Viliam	Viliam	k1gMnSc1	Viliam
Klimáček	Klimáček	k1gMnSc1	Klimáček
<g/>
:	:	kIx,	:
Dračí	dračí	k2eAgNnSc1d1	dračí
doupě	doupě	k1gNnSc1	doupě
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
Brooks	Brooksa	k1gFnPc2	Brooksa
-	-	kIx~	-
Joe	Joe	k1gFnSc1	Joe
Penhall	Penhall	k1gMnSc1	Penhall
<g/>
:	:	kIx,	:
Krajina	Krajina	k1gFnSc1	Krajina
se	s	k7c7	s
zbraní	zbraň	k1gFnSc7	zbraň
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
Podnikatel	podnikatel	k1gMnSc1	podnikatel
-	-	kIx~	-
Aki	Aki	k1gMnSc1	Aki
Kaurismäki	Kaurismäk	k1gFnSc2	Kaurismäk
<g/>
:	:	kIx,	:
Muž	muž	k1gMnSc1	muž
bez	bez	k7c2	bez
minulosti	minulost	k1gFnSc2	minulost
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
Ash	Ash	k1gFnPc2	Ash
-	-	kIx~	-
Patrick	Patrick	k1gMnSc1	Patrick
Marber	Marber	k1gMnSc1	Marber
<g/>
:	:	kIx,	:
Dealer	dealer	k1gMnSc1	dealer
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Choice	Choic	k1gMnSc2	Choic
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
Bůh	bůh	k1gMnSc1	bůh
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
Irvine	Irvinout	k5eAaPmIp3nS	Irvinout
Welsh	Welsh	k1gInSc1	Welsh
<g/>
:	:	kIx,	:
Ucpanej	Ucpanej	k?	Ucpanej
systém	systém	k1gInSc1	systém
-	-	kIx~	-
Cena	cena	k1gFnSc1	cena
A.	A.	kA	A.
Radoka	Radoka	k1gFnSc1	Radoka
2013	[number]	k4	2013
Boris	Boris	k1gMnSc1	Boris
Trigorin	Trigorin	k1gInSc1	Trigorin
-	-	kIx~	-
Anton	Anton	k1gMnSc1	Anton
Pavlovič	Pavlovič	k1gMnSc1	Pavlovič
Čechov	Čechov	k1gMnSc1	Čechov
<g/>
:	:	kIx,	:
Racek	racek	k1gMnSc1	racek
2016	[number]	k4	2016
Daniel	Daniela	k1gFnPc2	Daniela
Doubt	Doubta	k1gFnPc2	Doubta
<g/>
:	:	kIx,	:
Vzkříšení	vzkříšení	k1gNnSc2	vzkříšení
2015	[number]	k4	2015
David	David	k1gMnSc1	David
Doubt	Doubt	k1gMnSc1	Doubt
<g/>
:	:	kIx,	:
Zásek	zásek	k1gInSc1	zásek
1995	[number]	k4	1995
Jan	Jan	k1gMnSc1	Jan
Neruda	Neruda	k1gMnSc1	Neruda
Figurky	figurka	k1gFnSc2	figurka
<g/>
,	,	kIx,	,
Rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
komedie	komedie	k1gFnSc1	komedie
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
povídky	povídka	k1gFnSc2	povídka
ze	z	k7c2	z
sbírky	sbírka	k1gFnSc2	sbírka
Povídky	povídka	k1gFnSc2	povídka
malostranské	malostranský	k2eAgFnSc2d1	Malostranská
<g/>
.	.	kIx.	.
</s>
<s>
Dramatizace	dramatizace	k1gFnSc1	dramatizace
Jiří	Jiří	k1gMnSc1	Jiří
Just	just	k6eAd1	just
<g/>
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
Petr	Petr	k1gMnSc1	Petr
Mandel	mandel	k1gInSc1	mandel
<g/>
.	.	kIx.	.
</s>
<s>
Dramaturg	dramaturg	k1gMnSc1	dramaturg
Josef	Josef	k1gMnSc1	Josef
Hlavnička	hlavnička	k1gFnSc1	hlavnička
<g/>
.	.	kIx.	.
</s>
<s>
Režie	režie	k1gFnSc1	režie
Vlado	Vlado	k1gNnSc1	Vlado
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Osoby	osoba	k1gFnPc1	osoba
a	a	k8xC	a
obsazení	obsazení	k1gNnSc1	obsazení
<g/>
:	:	kIx,	:
Doktor	doktor	k1gMnSc1	doktor
(	(	kIx(	(
<g/>
Ivan	Ivan	k1gMnSc1	Ivan
Trojan	Trojan	k1gMnSc1	Trojan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Morousek	Morousek	k1gMnSc1	Morousek
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Novotný	Novotný	k1gMnSc1	Novotný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vilhelmová	Vilhelmová	k1gFnSc1	Vilhelmová
(	(	kIx(	(
<g/>
Naďa	Naďa	k1gFnSc1	Naďa
Konvalinková	Konvalinková	k1gFnSc1	Konvalinková
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Provazník	Provazník	k1gMnSc1	Provazník
(	(	kIx(	(
<g/>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Krška	Krška	k1gMnSc1	Krška
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Otylie	Otylie	k1gFnSc1	Otylie
(	(	kIx(	(
<g/>
Zuzana	Zuzana	k1gFnSc1	Zuzana
Petráňová	Petráňová	k1gFnSc1	Petráňová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Domácí	domácí	k1gMnPc1	domácí
(	(	kIx(	(
<g/>
Oldřich	Oldřich	k1gMnSc1	Oldřich
Velen	velen	k2eAgMnSc1d1	velen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Malíř	malíř	k1gMnSc1	malíř
(	(	kIx(	(
<g/>
Antonín	Antonín	k1gMnSc1	Antonín
Molčík	Molčík	k1gMnSc1	Molčík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Malířová	Malířová	k1gFnSc1	Malířová
(	(	kIx(	(
<g/>
Růžena	Růžena	k1gFnSc1	Růžena
Merunková	Merunková	k1gFnSc1	Merunková
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pepík	Pepík	k1gMnSc1	Pepík
(	(	kIx(	(
<g/>
Matěj	Matěj	k1gMnSc1	Matěj
Sviták	Sviták	k1gMnSc1	Sviták
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hostinský	hostinský	k1gMnSc1	hostinský
(	(	kIx(	(
<g/>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Moučka	Moučka	k1gMnSc1	Moučka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nadporučík	nadporučík	k1gMnSc1	nadporučík
(	(	kIx(	(
<g/>
Jiří	Jiří	k1gMnSc1	Jiří
Klem	Klema	k1gFnPc2	Klema
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sekundant	sekundant	k1gMnSc1	sekundant
(	(	kIx(	(
<g/>
Petr	Petr	k1gMnSc1	Petr
Křiváček	křiváček	k1gInSc1	křiváček
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lékař	lékař	k1gMnSc1	lékař
(	(	kIx(	(
<g/>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Pechan	Pechan	k1gMnSc1	Pechan
<g/>
)	)	kIx)	)
a	a	k8xC	a
Pouliční	pouliční	k2eAgMnSc1d1	pouliční
zpěvák	zpěvák	k1gMnSc1	zpěvák
(	(	kIx(	(
<g/>
Petr	Petr	k1gMnSc1	Petr
Křiváček	křiváček	k1gInSc1	křiváček
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
Tankred	Tankred	k1gMnSc1	Tankred
Dorst	Dorst	k1gMnSc1	Dorst
<g/>
:	:	kIx,	:
Fernando	Fernanda	k1gFnSc5	Fernanda
Krapp	Krapp	k1gInSc1	Krapp
mi	já	k3xPp1nSc3	já
napsal	napsat	k5eAaBmAgInS	napsat
dopis	dopis	k1gInSc4	dopis
(	(	kIx(	(
<g/>
Rernando	Rernanda	k1gFnSc5	Rernanda
Krapp	Krapp	k1gInSc1	Krapp
hat	hat	k0	hat
mir	mir	k1gInSc4	mir
diesen	diesen	k2eAgInSc1d1	diesen
Brief	Brief	k1gInSc1	Brief
geschrieben	geschriebna	k1gFnPc2	geschriebna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
povídky	povídka	k1gFnSc2	povídka
Miguela	Miguela	k1gFnSc1	Miguela
de	de	k?	de
Unamuna	Unamuna	k1gFnSc1	Unamuna
za	za	k7c2	za
autorské	autorský	k2eAgFnSc2d1	autorská
spolupráce	spolupráce	k1gFnSc2	spolupráce
Ursuly	Ursula	k1gFnSc2	Ursula
Ehlerové	Ehlerová	k1gFnSc2	Ehlerová
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Ondřej	Ondřej	k1gMnSc1	Ondřej
Černý	Černý	k1gMnSc1	Černý
<g/>
,	,	kIx,	,
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
úprava	úprava	k1gFnSc1	úprava
a	a	k8xC	a
dramaturgie	dramaturgie	k1gFnSc1	dramaturgie
Klára	Klára	k1gFnSc1	Klára
Novotná	Novotná	k1gFnSc1	Novotná
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Petr	Petr	k1gMnSc1	Petr
Mančal	Mančal	k1gMnSc1	Mančal
<g/>
.	.	kIx.	.
</s>
<s>
Hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
Krapp	Krapp	k1gInSc1	Krapp
(	(	kIx(	(
<g/>
Ivan	Ivan	k1gMnSc1	Ivan
Trojan	Trojan	k1gMnSc1	Trojan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Julie	Julie	k1gFnSc1	Julie
(	(	kIx(	(
<g/>
Tereza	Tereza	k1gFnSc1	Tereza
Dočkalová	Dočkalová	k1gFnSc1	Dočkalová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hrabě	hrabě	k1gMnSc1	hrabě
(	(	kIx(	(
<g/>
Martin	Martin	k1gMnSc1	Martin
Myšička	myšička	k1gFnSc1	myšička
<g/>
)	)	kIx)	)
a	a	k8xC	a
otec	otec	k1gMnSc1	otec
(	(	kIx(	(
<g/>
Oldřich	Oldřich	k1gMnSc1	Oldřich
Kaiser	Kaiser	k1gMnSc1	Kaiser
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Premiéra	premiéra	k1gFnSc1	premiéra
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
nás	my	k3xPp1nPc4	my
šest	šest	k4xCc1	šest
(	(	kIx(	(
<g/>
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
O	o	k7c6	o
štěstí	štěstí	k1gNnSc6	štěstí
a	a	k8xC	a
kráse	krása	k1gFnSc6	krása
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
Naděje	naděje	k1gFnSc1	naděje
má	mít	k5eAaImIp3nS	mít
hluboké	hluboký	k2eAgNnSc4d1	hluboké
dno	dno	k1gNnSc4	dno
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
Putování	putování	k1gNnSc3	putování
po	po	k7c6	po
Blažených	blažený	k2eAgInPc6d1	blažený
ostrovech	ostrov	k1gInPc6	ostrov
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
Dobrodružství	dobrodružství	k1gNnSc4	dobrodružství
kriminalistiky	kriminalistika	k1gFnSc2	kriminalistika
(	(	kIx(	(
<g/>
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
O	o	k7c6	o
čarovné	čarovný	k2eAgFnSc6d1	čarovná
<g />
.	.	kIx.	.
</s>
<s>
Laskonce	laskonka	k1gFnSc3	laskonka
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
O	o	k7c6	o
dvou	dva	k4xCgFnPc6	dva
sestrách	sestra	k1gFnPc6	sestra
a	a	k8xC	a
Noční	noční	k2eAgFnSc6d1	noční
květině	květina	k1gFnSc6	květina
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Kryštof	Kryštof	k1gMnSc1	Kryštof
a	a	k8xC	a
Kristina	Kristina	k1gFnSc1	Kristina
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Laskavý	laskavý	k2eAgMnSc1d1	laskavý
divák	divák	k1gMnSc1	divák
promine	prominout	k5eAaPmIp3nS	prominout
(	(	kIx(	(
<g/>
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Poprask	poprask	k1gInSc1	poprask
na	na	k7c6	na
laguně	laguna	k1gFnSc6	laguna
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Četnické	četnický	k2eAgFnSc2d1	četnická
humoresky	humoreska	k1gFnSc2	humoreska
(	(	kIx(	(
<g/>
seriál	seriál	k1gInSc1	seriál
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Můj	můj	k3xOp1gMnSc1	můj
otec	otec	k1gMnSc1	otec
a	a	k8xC	a
ostatní	ostatní	k2eAgMnPc1d1	ostatní
muži	muž	k1gMnPc1	muž
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Krásný	krásný	k2eAgInSc1d1	krásný
čas	čas	k1gInSc1	čas
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Operace	operace	k1gFnSc1	operace
Silver	Silver	k1gMnSc1	Silver
A	A	kA	A
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
BrainStorm	BrainStorm	k1gInSc1	BrainStorm
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Ďáblova	ďáblův	k2eAgFnSc1d1	Ďáblova
lest	lest	k1gFnSc1	lest
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Dívka	dívka	k1gFnSc1	dívka
a	a	k8xC	a
kouzelník	kouzelník	k1gMnSc1	kouzelník
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Šejdrem	šejdr	k1gInSc7	šejdr
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Okresní	okresní	k2eAgInSc1d1	okresní
přebor	přebor	k1gInSc1	přebor
(	(	kIx(	(
<g/>
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Jseš	Jseš	k?	Jseš
mrtvej	mrtvej	k?	mrtvej
<g/>
,	,	kIx,	,
tak	tak	k9	tak
nebreč	brečet	k5eNaImRp2nS	brečet
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Ztracená	ztracený	k2eAgFnSc1d1	ztracená
brána	brána	k1gFnSc1	brána
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Hořící	hořící	k2eAgInSc1d1	hořící
keř	keř	k1gInSc1	keř
(	(	kIx(	(
<g/>
minisérie	minisérie	k1gFnSc1	minisérie
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
České	český	k2eAgNnSc1d1	české
století	století	k1gNnSc1	století
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
-	-	kIx~	-
František	František	k1gMnSc1	František
Moravec	Moravec	k1gMnSc1	Moravec
Čtvrtá	čtvrtá	k1gFnSc1	čtvrtá
hvězda	hvězda	k1gFnSc1	hvězda
(	(	kIx(	(
<g/>
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
-	-	kIx~	-
František	František	k1gMnSc1	František
Osmy	osma	k1gFnSc2	osma
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Vraždy	vražda	k1gFnSc2	vražda
v	v	k7c6	v
kruhu	kruh	k1gInSc6	kruh
(	(	kIx(	(
<g/>
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
-	-	kIx~	-
major	major	k1gMnSc1	major
Marián	Marián	k1gMnSc1	Marián
Holina	holina	k1gFnSc1	holina
Svět	svět	k1gInSc1	svět
pod	pod	k7c7	pod
hlavou	hlava	k1gFnSc7	hlava
(	(	kIx(	(
<g/>
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
-	-	kIx~	-
Martin	Martin	k1gMnSc1	Martin
Plachý	Plachý	k1gMnSc1	Plachý
Z	z	k7c2	z
Trojanových	Trojanových	k2eAgFnPc2d1	Trojanových
televizních	televizní	k2eAgFnPc2d1	televizní
rolí	role	k1gFnPc2	role
si	se	k3xPyFc3	se
značný	značný	k2eAgInSc4d1	značný
divácký	divácký	k2eAgInSc4d1	divácký
ohlas	ohlas	k1gInSc4	ohlas
získala	získat	k5eAaPmAgFnS	získat
postava	postava	k1gFnSc1	postava
strážmistra	strážmistr	k1gMnSc2	strážmistr
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Jarého	jarý	k2eAgMnSc2d1	jarý
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Četnické	četnický	k2eAgFnSc2d1	četnická
humoresky	humoreska	k1gFnSc2	humoreska
režiséra	režisér	k1gMnSc2	režisér
Antonína	Antonín	k1gMnSc2	Antonín
Moskalyka	Moskalyek	k1gMnSc2	Moskalyek
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
již	již	k6eAd1	již
předtím	předtím	k6eAd1	předtím
obsadil	obsadit	k5eAaPmAgMnS	obsadit
Ivana	Ivan	k1gMnSc4	Ivan
Trojana	Trojan	k1gMnSc4	Trojan
do	do	k7c2	do
menší	malý	k2eAgFnSc2d2	menší
role	role	k1gFnSc2	role
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Dobrodružství	dobrodružství	k1gNnSc2	dobrodružství
kriminalistiky	kriminalistika	k1gFnSc2	kriminalistika
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
jeho	jeho	k3xOp3gMnPc4	jeho
výraznou	výrazný	k2eAgFnSc7d1	výrazná
filmovou	filmový	k2eAgFnSc7d1	filmová
rolí	role	k1gFnSc7	role
byla	být	k5eAaImAgFnS	být
postava	postava	k1gFnSc1	postava
neurotického	neurotický	k2eAgMnSc2d1	neurotický
doktora	doktor	k1gMnSc2	doktor
Ondřeje	Ondřej	k1gMnSc2	Ondřej
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Samotáři	samotář	k1gMnPc1	samotář
režiséra	režisér	k1gMnSc2	režisér
Davida	David	k1gMnSc2	David
Ondříčka	Ondříček	k1gMnSc2	Ondříček
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
roli	role	k1gFnSc3	role
Trojan	Trojan	k1gMnSc1	Trojan
získal	získat	k5eAaPmAgMnS	získat
značnou	značný	k2eAgFnSc4d1	značná
popularitu	popularita	k1gFnSc4	popularita
a	a	k8xC	a
také	také	k9	také
byl	být	k5eAaImAgMnS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Českého	český	k2eAgMnSc4d1	český
lva	lev	k1gMnSc4	lev
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
cenu	cena	k1gFnSc4	cena
však	však	k9	však
získal	získat	k5eAaPmAgInS	získat
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
za	za	k7c4	za
roli	role	k1gFnSc4	role
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
rodičů	rodič	k1gMnPc2	rodič
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Smradi	Smradi	k?	Smradi
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
byl	být	k5eAaImAgInS	být
oceněn	ocenit	k5eAaPmNgInS	ocenit
Českým	český	k2eAgInSc7d1	český
lvem	lev	k1gInSc7	lev
také	také	k9	také
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Musím	muset	k5eAaImIp1nS	muset
tě	ty	k3xPp2nSc4	ty
svést	svést	k5eAaPmF	svést
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
si	se	k3xPyFc3	se
tento	tento	k3xDgInSc4	tento
úspěch	úspěch	k1gInSc4	úspěch
zopakoval	zopakovat	k5eAaPmAgMnS	zopakovat
za	za	k7c4	za
roli	role	k1gFnSc4	role
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Jedna	jeden	k4xCgFnSc1	jeden
ruka	ruka	k1gFnSc1	ruka
netleská	tleskat	k5eNaImIp3nS	tleskat
režiséra	režisér	k1gMnSc4	režisér
Davida	David	k1gMnSc4	David
Ondříčka	Ondříček	k1gMnSc4	Ondříček
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
filmu	film	k1gInSc6	film
hrála	hrát	k5eAaImAgFnS	hrát
jeho	jeho	k3xOp3gFnSc4	jeho
filmovou	filmový	k2eAgFnSc4d1	filmová
manželku	manželka	k1gFnSc4	manželka
jeho	jeho	k3xOp3gFnSc1	jeho
skutečná	skutečný	k2eAgFnSc1d1	skutečná
manželka	manželka	k1gFnSc1	manželka
Klára	Klára	k1gFnSc1	Klára
a	a	k8xC	a
Trojan	Trojan	k1gMnSc1	Trojan
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
i	i	k9	i
na	na	k7c6	na
scénáři	scénář	k1gInSc6	scénář
<g/>
.	.	kIx.	.
</s>
<s>
Zahrál	zahrát	k5eAaPmAgMnS	zahrát
si	se	k3xPyFc3	se
i	i	k9	i
pod	pod	k7c7	pod
režisérským	režisérský	k2eAgNnSc7d1	režisérské
vedením	vedení	k1gNnSc7	vedení
svého	svůj	k3xOyFgMnSc2	svůj
bratra	bratr	k1gMnSc2	bratr
Ondřeje	Ondřej	k1gMnSc2	Ondřej
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Želary	Želara	k1gFnSc2	Želara
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavou	zajímavý	k2eAgFnSc7d1	zajímavá
rolí	role	k1gFnSc7	role
byla	být	k5eAaImAgFnS	být
i	i	k9	i
postava	postava	k1gFnSc1	postava
Petra	Petr	k1gMnSc2	Petr
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Příběhy	příběh	k1gInPc1	příběh
obyčejného	obyčejný	k2eAgNnSc2d1	obyčejné
šílenství	šílenství	k1gNnSc2	šílenství
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
divadelní	divadelní	k2eAgFnSc2d1	divadelní
hry	hra	k1gFnSc2	hra
režiséra	režisér	k1gMnSc2	režisér
Petra	Petr	k1gMnSc2	Petr
Zelenky	Zelenka	k1gMnSc2	Zelenka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
držitelem	držitel	k1gMnSc7	držitel
ceny	cena	k1gFnSc2	cena
TýTý	TýTý	k1gFnSc2	TýTý
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
kategorie	kategorie	k1gFnSc2	kategorie
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
herec	herec	k1gMnSc1	herec
<g/>
.	.	kIx.	.
2000	[number]	k4	2000
-	-	kIx~	-
Samotáři	samotář	k1gMnPc1	samotář
-	-	kIx~	-
Ondřej	Ondřej	k1gMnSc1	Ondřej
2002	[number]	k4	2002
-	-	kIx~	-
Musím	muset	k5eAaImIp1nS	muset
tě	ty	k3xPp2nSc4	ty
svést	svést	k5eAaPmF	svést
-	-	kIx~	-
Karel	Karel	k1gMnSc1	Karel
2002	[number]	k4	2002
-	-	kIx~	-
Smradi	Smradi	k?	Smradi
-	-	kIx~	-
Marek	Marek	k1gMnSc1	Marek
Šír	Šír	k1gMnSc1	Šír
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
2003	[number]	k4	2003
-	-	kIx~	-
Mazaný	mazaný	k2eAgMnSc1d1	mazaný
Filip	Filip	k1gMnSc1	Filip
-	-	kIx~	-
maskér	maskér	k1gMnSc1	maskér
2003	[number]	k4	2003
-	-	kIx~	-
Jedna	jeden	k4xCgFnSc1	jeden
ruka	ruka	k1gFnSc1	ruka
netleská	tleskat	k5eNaImIp3nS	tleskat
-	-	kIx~	-
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
2003	[number]	k4	2003
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
Želary	Želar	k1gInPc1	Želar
-	-	kIx~	-
Richard	Richard	k1gMnSc1	Richard
2005	[number]	k4	2005
-	-	kIx~	-
Anděl	Anděla	k1gFnPc2	Anděla
Páně	páně	k2eAgFnPc2d1	páně
-	-	kIx~	-
anděl	anděl	k1gMnSc1	anděl
Petronel	Petronela	k1gFnPc2	Petronela
2005	[number]	k4	2005
-	-	kIx~	-
Příběhy	příběh	k1gInPc1	příběh
obyčejného	obyčejný	k2eAgNnSc2d1	obyčejné
šílenství	šílenství	k1gNnSc2	šílenství
-	-	kIx~	-
Petr	Petr	k1gMnSc1	Petr
Hanek	Hanka	k1gFnPc2	Hanka
2007	[number]	k4	2007
-	-	kIx~	-
Medvídek	medvídek	k1gMnSc1	medvídek
-	-	kIx~	-
Ivan	Ivan	k1gMnSc1	Ivan
2007	[number]	k4	2007
-	-	kIx~	-
Václav	Václav	k1gMnSc1	Václav
-	-	kIx~	-
Václav	Václav	k1gMnSc1	Václav
2008	[number]	k4	2008
-	-	kIx~	-
Karamazovi	Karamazův	k2eAgMnPc1d1	Karamazův
-	-	kIx~	-
starý	starý	k2eAgInSc1d1	starý
Karamazov	Karamazov	k1gInSc1	Karamazov
2009	[number]	k4	2009
-	-	kIx~	-
Suplent	suplent	k1gMnSc1	suplent
-	-	kIx~	-
Oskar	Oskar	k1gMnSc1	Oskar
<g/>
/	/	kIx~	/
<g/>
Frederik	Frederik	k1gMnSc1	Frederik
2011	[number]	k4	2011
-	-	kIx~	-
Alois	Alois	k1gMnSc1	Alois
Nebel	Nebel	k1gMnSc1	Nebel
-	-	kIx~	-
ředitel	ředitel	k1gMnSc1	ředitel
ČSD	ČSD	kA	ČSD
<g />
.	.	kIx.	.
</s>
<s>
2011	[number]	k4	2011
-	-	kIx~	-
Viditeľný	Viditeľný	k2eAgInSc1d1	Viditeľný
svet	svet	k1gInSc1	svet
-	-	kIx~	-
Oliver	Oliver	k1gInSc1	Oliver
2012	[number]	k4	2012
-	-	kIx~	-
Ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
-	-	kIx~	-
Hakl	Hakl	k1gMnSc1	Hakl
2012	[number]	k4	2012
-	-	kIx~	-
Vrásky	vráska	k1gFnSc2	vráska
z	z	k7c2	z
lásky	láska	k1gFnSc2	láska
-	-	kIx~	-
Jan	Jan	k1gMnSc1	Jan
2012	[number]	k4	2012
-	-	kIx~	-
Šťastný	Šťastný	k1gMnSc1	Šťastný
smolař	smolař	k1gMnSc1	smolař
-	-	kIx~	-
Kdokoliv	kdokoliv	k3yInSc1	kdokoliv
2014	[number]	k4	2014
-	-	kIx~	-
Díra	díra	k1gFnSc1	díra
u	u	k7c2	u
Hanušovic	Hanušovice	k1gFnPc2	Hanušovice
-	-	kIx~	-
starosta	starosta	k1gMnSc1	starosta
2015	[number]	k4	2015
-	-	kIx~	-
Svatojánský	svatojánský	k2eAgInSc1d1	svatojánský
věneček	věneček	k1gInSc1	věneček
-	-	kIx~	-
Pastelíno	Pastelína	k1gFnSc5	Pastelína
2016	[number]	k4	2016
-	-	kIx~	-
Anděl	Anděla	k1gFnPc2	Anděla
Páně	páně	k2eAgInPc1d1	páně
2	[number]	k4	2
-	-	kIx~	-
anděl	anděl	k1gMnSc1	anděl
Petronel	Petronela	k1gFnPc2	Petronela
Trojanův	Trojanův	k2eAgInSc1d1	Trojanův
hlas	hlas	k1gInSc1	hlas
zazněl	zaznět	k5eAaImAgInS	zaznět
také	také	k9	také
v	v	k7c6	v
rozhlasových	rozhlasový	k2eAgFnPc6d1	rozhlasová
hrách	hra	k1gFnPc6	hra
a	a	k8xC	a
dabovaných	dabovaný	k2eAgInPc6d1	dabovaný
filmech	film	k1gInPc6	film
či	či	k8xC	či
seriálech	seriál	k1gInPc6	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
to	ten	k3xDgNnSc1	ten
kupříkladu	kupříkladu	k6eAd1	kupříkladu
:	:	kIx,	:
Návrat	návrat	k1gInSc1	návrat
do	do	k7c2	do
budoucnosti	budoucnost	k1gFnSc2	budoucnost
II	II	kA	II
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
Tajný	tajný	k2eAgInSc1d1	tajný
císařský	císařský	k2eAgInSc1d1	císařský
edikt	edikt	k1gInSc1	edikt
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
seriál	seriál	k1gInSc1	seriál
24	[number]	k4	24
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
namluvil	namluvit	k5eAaBmAgInS	namluvit
postavu	postava	k1gFnSc4	postava
agenta	agent	k1gMnSc2	agent
Jacka	Jacek	k1gMnSc2	Jacek
Bauera	Bauer	k1gMnSc2	Bauer
v	v	k7c6	v
podání	podání	k1gNnSc6	podání
Kiefera	Kiefera	k1gFnSc1	Kiefera
Sutherlanda	Sutherlanda	k1gFnSc1	Sutherlanda
seriál	seriál	k1gInSc4	seriál
Ally	Alla	k1gFnSc2	Alla
McBealová	McBealová	k1gFnSc1	McBealová
Hledá	hledat	k5eAaImIp3nS	hledat
se	se	k3xPyFc4	se
Nemo	Nemo	k1gMnSc1	Nemo
-	-	kIx~	-
Marlin	Marlin	k1gInSc1	Marlin
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
2000	[number]	k4	2000
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
Thálie	Thálie	k1gFnSc1	Thálie
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
činohra	činohra	k1gFnSc1	činohra
za	za	k7c4	za
roli	role	k1gFnSc4	role
Oblomova	Oblomov	k1gMnSc2	Oblomov
ve	v	k7c6	v
stejnojmenné	stejnojmenný	k2eAgFnSc6d1	stejnojmenná
hře	hra	k1gFnSc6	hra
Ivana	Ivan	k1gMnSc2	Ivan
Alexandroviče	Alexandrovič	k1gMnSc2	Alexandrovič
Gončarova	Gončarův	k2eAgMnSc2d1	Gončarův
<g/>
,	,	kIx,	,
Dejvické	dejvický	k2eAgNnSc1d1	Dejvické
divadlo	divadlo	k1gNnSc1	divadlo
2012	[number]	k4	2012
-	-	kIx~	-
Cena	cena	k1gFnSc1	cena
Alfréda	Alfréd	k1gMnSc2	Alfréd
Radoka	Radoek	k1gMnSc2	Radoek
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
-	-	kIx~	-
role	role	k1gFnSc1	role
Boha	bůh	k1gMnSc2	bůh
v	v	k7c6	v
inscenaci	inscenace	k1gFnSc6	inscenace
Ucpanej	Ucpanej	k?	Ucpanej
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
Dejvické	dejvický	k2eAgNnSc1d1	Dejvické
divadlo	divadlo	k1gNnSc1	divadlo
(	(	kIx(	(
<g/>
Hra	hra	k1gFnSc1	hra
obdržela	obdržet	k5eAaPmAgFnS	obdržet
cenu	cena	k1gFnSc4	cena
i	i	k9	i
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
"	"	kIx"	"
<g/>
Inscenace	inscenace	k1gFnSc1	inscenace
<g />
.	.	kIx.	.
</s>
<s>
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
<g/>
"	"	kIx"	"
a	a	k8xC	a
Dejvické	dejvický	k2eAgNnSc1d1	Dejvické
divadlo	divadlo	k1gNnSc1	divadlo
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
"	"	kIx"	"
<g/>
Divadlem	divadlo	k1gNnSc7	divadlo
roku	rok	k1gInSc2	rok
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
2013	[number]	k4	2013
-	-	kIx~	-
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
nymfa	nymfa	k1gFnSc1	nymfa
pro	pro	k7c4	pro
herce	herec	k1gMnPc4	herec
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
v	v	k7c6	v
roli	role	k1gFnSc6	role
v	v	k7c6	v
mini-sérii	miniérie	k1gFnSc6	mini-série
na	na	k7c4	na
53	[number]	k4	53
<g/>
.	.	kIx.	.
ročníku	ročník	k1gInSc2	ročník
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
televizního	televizní	k2eAgInSc2d1	televizní
festivalu	festival	k1gInSc2	festival
v	v	k7c6	v
Monte	Mont	k1gInSc5	Mont
Carlu	Carl	k1gInSc6	Carl
(	(	kIx(	(
<g/>
Monte-Carlo	Monte-Carlo	k1gNnSc1	Monte-Carlo
Television	Television	k1gInSc4	Television
Festival	festival	k1gInSc1	festival
<g/>
)	)	kIx)	)
za	za	k7c4	za
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c4	v
roli	role	k1gFnSc4	role
roli	role	k1gFnSc4	role
majora	major	k1gMnSc2	major
Jireše	Jireše	k1gMnSc2	Jireše
v	v	k7c6	v
třídílném	třídílný	k2eAgNnSc6d1	třídílné
dramatu	drama	k1gNnSc6	drama
Hořící	hořící	k2eAgInSc1d1	hořící
keř	keř	k1gInSc1	keř
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
Cena	cena	k1gFnSc1	cena
Divadelních	divadelní	k2eAgFnPc2d1	divadelní
novin	novina	k1gFnPc2	novina
za	za	k7c4	za
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
sezony	sezona	k1gFnSc2	sezona
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
žánry	žánr	k1gInPc4	žánr
za	za	k7c4	za
roli	role	k1gFnSc4	role
Andrewa	Andrewus	k1gMnSc2	Andrewus
Maxwella	Maxwell	k1gMnSc2	Maxwell
v	v	k7c6	v
inscenaci	inscenace	k1gFnSc6	inscenace
Vzkříšení	vzkříšení	k1gNnSc1	vzkříšení
2000	[number]	k4	2000
-	-	kIx~	-
Samotáři	samotář	k1gMnSc6	samotář
-	-	kIx~	-
nominace	nominace	k1gFnSc2	nominace
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
2002	[number]	k4	2002
-	-	kIx~	-
Musím	muset	k5eAaImIp1nS	muset
tě	ty	k3xPp2nSc4	ty
svést	svést	k5eAaPmF	svést
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
vítěz	vítěz	k1gMnSc1	vítěz
kategorie	kategorie	k1gFnSc2	kategorie
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
2002	[number]	k4	2002
-	-	kIx~	-
Smradi	Smradi	k?	Smradi
-	-	kIx~	-
vítěz	vítěz	k1gMnSc1	vítěz
kategorie	kategorie	k1gFnSc2	kategorie
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
2003	[number]	k4	2003
-	-	kIx~	-
Jedna	jeden	k4xCgFnSc1	jeden
ruka	ruka	k1gFnSc1	ruka
netleská	tleskat	k5eNaImIp3nS	tleskat
-	-	kIx~	-
vítěz	vítěz	k1gMnSc1	vítěz
kategorie	kategorie	k1gFnSc2	kategorie
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
2005	[number]	k4	2005
-	-	kIx~	-
Příběhy	příběh	k1gInPc1	příběh
obyčejného	obyčejný	k2eAgNnSc2d1	obyčejné
šílenství	šílenství	k1gNnSc2	šílenství
-	-	kIx~	-
nominace	nominace	k1gFnSc2	nominace
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
<g />
.	.	kIx.	.
</s>
<s>
výkon	výkon	k1gInSc1	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
2007	[number]	k4	2007
-	-	kIx~	-
Václav	Václav	k1gMnSc1	Václav
-	-	kIx~	-
vítěz	vítěz	k1gMnSc1	vítěz
kategorie	kategorie	k1gFnSc2	kategorie
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
2008	[number]	k4	2008
-	-	kIx~	-
Karamazovi	Karamaz	k1gMnSc6	Karamaz
-	-	kIx~	-
nominace	nominace	k1gFnSc2	nominace
na	na	k7c6	na
kategorii	kategorie	k1gFnSc6	kategorie
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
2012	[number]	k4	2012
-	-	kIx~	-
Ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
-	-	kIx~	-
vítěz	vítěz	k1gMnSc1	vítěz
kategorie	kategorie	k1gFnSc2	kategorie
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
2013	[number]	k4	2013
-	-	kIx~	-
Hořící	hořící	k2eAgInSc1d1	hořící
keř	keř	k1gInSc1	keř
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
nominace	nominace	k1gFnSc1	nominace
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
2014	[number]	k4	2014
-	-	kIx~	-
Díra	díra	k1gFnSc1	díra
u	u	k7c2	u
Hanušovic	Hanušovice	k1gFnPc2	Hanušovice
-	-	kIx~	-
vítěz	vítěz	k1gMnSc1	vítěz
kategorie	kategorie	k1gFnSc2	kategorie
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
byl	být	k5eAaImAgMnS	být
zapsán	zapsat	k5eAaPmNgMnS	zapsat
na	na	k7c4	na
soupisku	soupiska	k1gFnSc4	soupiska
týmu	tým	k1gInSc2	tým
Bohemians	Bohemians	k1gInSc1	Bohemians
1905	[number]	k4	1905
a	a	k8xC	a
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
posledního	poslední	k2eAgNnSc2d1	poslední
utkání	utkání	k1gNnSc2	utkání
2	[number]	k4	2
<g/>
.	.	kIx.	.
fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
ligy	liga	k1gFnSc2	liga
za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
tým	tým	k1gInSc4	tým
proti	proti	k7c3	proti
FK	FK	kA	FK
Ústí	ústit	k5eAaImIp3nP	ústit
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Trojan	Trojan	k1gMnSc1	Trojan
je	být	k5eAaImIp3nS	být
kmenový	kmenový	k2eAgMnSc1d1	kmenový
hráč	hráč	k1gMnSc1	hráč
týmu	tým	k1gInSc2	tým
PFK	PFK	kA	PFK
Union	union	k1gInSc1	union
Strašnice	Strašnice	k1gFnPc1	Strašnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hraje	hrát	k5eAaImIp3nS	hrát
za	za	k7c7	za
"	"	kIx"	"
<g/>
B	B	kA	B
tým	tým	k1gInSc1	tým
<g/>
"	"	kIx"	"
II	II	kA	II
<g/>
.	.	kIx.	.
třídu	třída	k1gFnSc4	třída
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
ho	on	k3xPp3gNnSc4	on
vedení	vedení	k1gNnSc4	vedení
Bohemians	Bohemians	k1gInSc1	Bohemians
získalo	získat	k5eAaPmAgNnS	získat
na	na	k7c4	na
půlroční	půlroční	k2eAgNnSc4d1	půlroční
hostování	hostování	k1gNnSc4	hostování
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
minutu	minuta	k1gFnSc4	minuta
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
Trojan	Trojan	k1gMnSc1	Trojan
strávil	strávit	k5eAaPmAgMnS	strávit
na	na	k7c6	na
hřišti	hřiště	k1gNnSc6	hřiště
<g/>
,	,	kIx,	,
věnuje	věnovat	k5eAaPmIp3nS	věnovat
klub	klub	k1gInSc4	klub
Bohemians	Bohemiansa	k1gFnPc2	Bohemiansa
1905	[number]	k4	1905
částku	částka	k1gFnSc4	částka
5000	[number]	k4	5000
Kč	Kč	kA	Kč
nemocným	mocný	k2eNgInSc7d1	mocný
cystickou	cystický	k2eAgFnSc7d1	cystická
fibrózou	fibróza	k1gFnSc7	fibróza
<g/>
.	.	kIx.	.
</s>
<s>
Mimoto	mimoto	k6eAd1	mimoto
se	se	k3xPyFc4	se
také	také	k9	také
zúčastňuje	zúčastňovat	k5eAaImIp3nS	zúčastňovat
charitativních	charitativní	k2eAgInPc2d1	charitativní
zápasů	zápas	k1gInPc2	zápas
mužstva	mužstvo	k1gNnSc2	mužstvo
Real	Real	k1gInSc1	Real
TOP	topit	k5eAaImRp2nS	topit
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
kromě	kromě	k7c2	kromě
něj	on	k3xPp3gInSc2	on
hrají	hrát	k5eAaImIp3nP	hrát
i	i	k9	i
herci	herec	k1gMnPc1	herec
Ondřej	Ondřej	k1gMnSc1	Ondřej
Vetchý	vetchý	k2eAgMnSc1d1	vetchý
či	či	k8xC	či
David	David	k1gMnSc1	David
Suchařípa	Suchařípa	k1gFnSc1	Suchařípa
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yInSc1	kdo
je	být	k5eAaImIp3nS	být
kdo	kdo	k3yQnSc1	kdo
=	=	kIx~	=
Who	Who	k1gMnPc1	Who
is	is	k?	is
who	who	k?	who
:	:	kIx,	:
osobnosti	osobnost	k1gFnPc4	osobnost
české	český	k2eAgFnSc2d1	Česká
současnosti	současnost	k1gFnSc2	současnost
:	:	kIx,	:
5000	[number]	k4	5000
životopisů	životopis	k1gInPc2	životopis
/	/	kIx~	/
(	(	kIx(	(
<g/>
Michael	Michael	k1gMnSc1	Michael
Třeštík	Třeštík	k1gMnSc1	Třeštík
editor	editor	k1gMnSc1	editor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Agentura	agentura	k1gFnSc1	agentura
Kdo	kdo	k3yInSc1	kdo
je	být	k5eAaImIp3nS	být
kdo	kdo	k3yQnSc1	kdo
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
775	[number]	k4	775
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902586	[number]	k4	902586
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
685	[number]	k4	685
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ivan	Ivan	k1gMnSc1	Ivan
Trojan	Trojan	k1gMnSc1	Trojan
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Ivan	Ivan	k1gMnSc1	Ivan
Trojan	Trojan	k1gMnSc1	Trojan
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Ivan	Ivan	k1gMnSc1	Ivan
Trojan	Trojan	k1gMnSc1	Trojan
Ivan	Ivan	k1gMnSc1	Ivan
Trojan	Trojan	k1gMnSc1	Trojan
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Dejvického	dejvický	k2eAgNnSc2d1	Dejvické
divadla	divadlo	k1gNnSc2	divadlo
Ivan	Ivan	k1gMnSc1	Ivan
Trojan	Trojan	k1gMnSc1	Trojan
-	-	kIx~	-
fanouškovské	fanouškovský	k2eAgFnSc2d1	fanouškovská
stránky	stránka	k1gFnSc2	stránka
Ivan	Ivan	k1gMnSc1	Ivan
Trojan	Trojan	k1gMnSc1	Trojan
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Real	Real	k1gInSc1	Real
TOP	topit	k5eAaImRp2nS	topit
<g />
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
Ivan	Ivan	k1gMnSc1	Ivan
Trojan	Trojan	k1gMnSc1	Trojan
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Ivan	Ivan	k1gMnSc1	Ivan
Trojan	Trojan	k1gMnSc1	Trojan
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
Ivan	Ivan	k1gMnSc1	Ivan
Trojan	Trojan	k1gMnSc1	Trojan
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Ivan	Ivan	k1gMnSc1	Ivan
Trojan	Trojan	k1gMnSc1	Trojan
na	na	k7c4	na
Dabingforum	Dabingforum	k1gInSc4	Dabingforum
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
(	(	kIx(	(
<g/>
video	video	k1gNnSc1	video
<g/>
)	)	kIx)	)
Ivan	Ivan	k1gMnSc1	Ivan
Trojan	Trojan	k1gMnSc1	Trojan
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Na	na	k7c6	na
plovárně	plovárna	k1gFnSc6	plovárna
<g/>
,	,	kIx,	,
Archiv	archiv	k1gInSc1	archiv
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
</s>
