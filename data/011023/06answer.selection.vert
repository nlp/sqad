<s>
Čepel	čepel	k1gFnSc1	čepel
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
deskovitá	deskovitý	k2eAgFnSc1d1	deskovitá
<g/>
,	,	kIx,	,
podlouhlá	podlouhlý	k2eAgFnSc1d1	podlouhlá
<g/>
,	,	kIx,	,
ocelová	ocelový	k2eAgFnSc1d1	ocelová
součást	součást	k1gFnSc1	součást
různých	různý	k2eAgInPc2d1	různý
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
na	na	k7c6	na
jednom	jeden	k4xCgNnSc6	jeden
(	(	kIx(	(
<g/>
jednosečná	jednosečný	k2eAgNnPc4d1	jednosečné
<g/>
)	)	kIx)	)
či	či	k8xC	či
obou	dva	k4xCgInPc6	dva
bocích	bok	k1gInPc6	bok
(	(	kIx(	(
<g/>
dvousečná	dvousečný	k2eAgFnSc1d1	dvousečná
<g/>
)	)	kIx)	)
zbroušená	zbroušený	k2eAgFnSc1d1	zbroušená
do	do	k7c2	do
ostří	ostří	k1gNnSc2	ostří
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
břitu	břit	k1gInSc2	břit
a	a	k8xC	a
zakončena	zakončit	k5eAaPmNgFnS	zakončit
často	často	k6eAd1	často
hrotem	hrot	k1gInSc7	hrot
<g/>
.	.	kIx.	.
</s>
