<p>
<s>
Čepel	čepel	k1gFnSc1	čepel
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
deskovitá	deskovitý	k2eAgFnSc1d1	deskovitá
<g/>
,	,	kIx,	,
podlouhlá	podlouhlý	k2eAgFnSc1d1	podlouhlá
<g/>
,	,	kIx,	,
ocelová	ocelový	k2eAgFnSc1d1	ocelová
součást	součást	k1gFnSc1	součást
různých	různý	k2eAgInPc2d1	různý
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
na	na	k7c6	na
jednom	jeden	k4xCgNnSc6	jeden
(	(	kIx(	(
<g/>
jednosečná	jednosečný	k2eAgNnPc4d1	jednosečné
<g/>
)	)	kIx)	)
či	či	k8xC	či
obou	dva	k4xCgInPc6	dva
bocích	bok	k1gInPc6	bok
(	(	kIx(	(
<g/>
dvousečná	dvousečný	k2eAgFnSc1d1	dvousečná
<g/>
)	)	kIx)	)
zbroušená	zbroušený	k2eAgFnSc1d1	zbroušená
do	do	k7c2	do
ostří	ostří	k1gNnSc2	ostří
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
břitu	břit	k1gInSc2	břit
a	a	k8xC	a
zakončena	zakončit	k5eAaPmNgFnS	zakončit
často	často	k6eAd1	často
hrotem	hrot	k1gInSc7	hrot
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
část	část	k1gFnSc1	část
zbraně	zbraň	k1gFnSc2	zbraň
(	(	kIx(	(
<g/>
např.	např.	kA	např.
meč	meč	k1gInSc1	meč
<g/>
,	,	kIx,	,
šavle	šavle	k1gFnSc1	šavle
<g/>
,	,	kIx,	,
píka	píka	k1gFnSc1	píka
<g/>
,	,	kIx,	,
halapartna	halapartna	k1gFnSc1	halapartna
<g/>
,	,	kIx,	,
kord	kord	k1gInSc1	kord
<g/>
,	,	kIx,	,
rapír	rapír	k1gInSc1	rapír
<g/>
,	,	kIx,	,
dýka	dýka	k1gFnSc1	dýka
<g/>
)	)	kIx)	)
,	,	kIx,	,
nástroje	nástroj	k1gInPc1	nástroj
(	(	kIx(	(
<g/>
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
nůž	nůž	k1gInSc1	nůž
<g/>
,	,	kIx,	,
sekera	sekera	k1gFnSc1	sekera
<g/>
,	,	kIx,	,
mačeta	mačeta	k1gFnSc1	mačeta
<g/>
,	,	kIx,	,
nůžky	nůžky	k1gFnPc1	nůžky
<g/>
,	,	kIx,	,
pila	pila	k1gFnSc1	pila
<g/>
,	,	kIx,	,
kosa	kosa	k1gFnSc1	kosa
<g/>
,	,	kIx,	,
žiletka	žiletka	k1gFnSc1	žiletka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čepele	čepel	k1gFnPc1	čepel
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
strojních	strojní	k2eAgInPc6d1	strojní
zařízeních	zařízení	k1gNnPc6	zařízení
a	a	k8xC	a
v	v	k7c6	v
nejrůznějších	různý	k2eAgInPc6d3	nejrůznější
oborech	obor	k1gInPc6	obor
lidské	lidský	k2eAgFnSc2d1	lidská
činnosti	činnost	k1gFnSc2	činnost
(	(	kIx(	(
<g/>
strojní	strojní	k2eAgFnPc1d1	strojní
nůžky	nůžky	k1gFnPc1	nůžky
<g/>
,	,	kIx,	,
lékařské	lékařský	k2eAgInPc1d1	lékařský
skalpely	skalpel	k1gInPc1	skalpel
<g/>
,	,	kIx,	,
travní	travní	k2eAgInPc1d1	travní
sekačky	sekačka	k1gFnPc4	sekačka
či	či	k8xC	či
obráběcí	obráběcí	k2eAgInPc4d1	obráběcí
nože	nůž	k1gInPc4	nůž
pro	pro	k7c4	pro
obráběcí	obráběcí	k2eAgInPc4d1	obráběcí
stroje	stroj	k1gInPc4	stroj
a	a	k8xC	a
nástroje	nástroj	k1gInPc4	nástroj
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čepel	čepel	k1gFnSc1	čepel
tak	tak	k9	tak
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
sekání	sekání	k1gNnSc3	sekání
<g/>
,	,	kIx,	,
krájení	krájení	k1gNnSc3	krájení
<g/>
,	,	kIx,	,
bodání	bodání	k1gNnSc3	bodání
<g/>
,	,	kIx,	,
řezání	řezání	k1gNnSc3	řezání
<g/>
,	,	kIx,	,
třískovému	třískový	k2eAgNnSc3d1	třískové
obrábění	obrábění	k1gNnSc3	obrábění
atp.	atp.	kA	atp.
</s>
</p>
<p>
<s>
Čepele	čepel	k1gFnPc1	čepel
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
vyrobeny	vyrobit	k5eAaPmNgInP	vyrobit
z	z	k7c2	z
kovu	kov	k1gInSc2	kov
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
z	z	k7c2	z
oceli	ocel	k1gFnSc2	ocel
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgFnSc2d1	jiná
slitiny	slitina	k1gFnSc2	slitina
železa	železo	k1gNnSc2	železo
(	(	kIx(	(
<g/>
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
i	i	k9	i
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
bronzu	bronz	k1gInSc2	bronz
<g/>
,	,	kIx,	,
příp	příp	kA	příp
<g/>
.	.	kIx.	.
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
materiálů	materiál	k1gInPc2	materiál
–	–	k?	–
kamene	kámen	k1gInSc2	kámen
<g/>
,	,	kIx,	,
kosti	kost	k1gFnPc1	kost
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
také	také	k9	také
keramické	keramický	k2eAgInPc1d1	keramický
nože	nůž	k1gInPc1	nůž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
čepel	čepel	k1gInSc1	čepel
<g/>
"	"	kIx"	"
v	v	k7c6	v
jiném	jiný	k2eAgInSc6d1	jiný
významu	význam	k1gInSc6	význam
slova	slovo	k1gNnSc2	slovo
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
botanice	botanika	k1gFnSc6	botanika
je	být	k5eAaImIp3nS	být
čepel	čepel	k1gFnSc1	čepel
(	(	kIx(	(
<g/>
lamina	lamin	k2eAgFnSc1d1	lamina
<g/>
)	)	kIx)	)
plochá	plochý	k2eAgFnSc1d1	plochá
lupenitá	lupenitý	k2eAgFnSc1d1	lupenitá
část	část	k1gFnSc1	část
listu	list	k1gInSc2	list
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
zejména	zejména	k9	zejména
k	k	k7c3	k
výměně	výměna	k1gFnSc3	výměna
plynů	plyn	k1gInPc2	plyn
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
dýchání	dýchání	k1gNnSc1	dýchání
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k	k	k7c3	k
fotosyntéze	fotosyntéza	k1gFnSc3	fotosyntéza
a	a	k8xC	a
k	k	k7c3	k
odpařování	odpařování	k1gNnSc3	odpařování
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
sportovním	sportovní	k2eAgNnSc6d1	sportovní
názvosloví	názvosloví	k1gNnSc6	názvosloví
je	být	k5eAaImIp3nS	být
čepel	čepel	k1gInSc1	čepel
částí	část	k1gFnPc2	část
hokejky	hokejka	k1gFnSc2	hokejka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
břit	břit	k1gInSc1	břit
</s>
</p>
<p>
<s>
hrot	hrot	k1gInSc1	hrot
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
čepel	čepel	k1gInSc1	čepel
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
čepel	čepel	k1gFnSc1	čepel
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Heslo	heslo	k1gNnSc1	heslo
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
-	-	kIx~	-
Ottova	Ottův	k2eAgFnSc1d1	Ottova
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
obecných	obecný	k2eAgFnPc2d1	obecná
vědomostí	vědomost	k1gFnPc2	vědomost
</s>
</p>
