<s>
Andrew	Andrew	k?	Andrew
Jackson	Jackson	k1gMnSc1	Jackson
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1767	[number]	k4	1767
-	-	kIx~	-
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1845	[number]	k4	1845
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
sedmý	sedmý	k4xOgMnSc1	sedmý
prezident	prezident	k1gMnSc1	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
(	(	kIx(	(
<g/>
1829	[number]	k4	1829
<g/>
-	-	kIx~	-
<g/>
1837	[number]	k4	1837
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
slogan	slogan	k1gInSc1	slogan
zněl	znět	k5eAaImAgInS	znět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nechť	nechť	k9	nechť
vládne	vládnout	k5eAaImIp3nS	vládnout
lid	lid	k1gInSc1	lid
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
lid	lid	k1gInSc4	lid
nepovažoval	považovat	k5eNaImAgMnS	považovat
indiánské	indiánský	k2eAgNnSc4d1	indiánské
a	a	k8xC	a
černošské	černošský	k2eAgNnSc4d1	černošské
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
neústupnost	neústupnost	k1gFnSc4	neústupnost
nosil	nosit	k5eAaImAgInS	nosit
přezdívku	přezdívka	k1gFnSc4	přezdívka
"	"	kIx"	"
<g/>
Starý	starý	k2eAgInSc1d1	starý
ořechovec	ořechovec	k1gInSc1	ořechovec
<g/>
"	"	kIx"	"
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
Old	Olda	k1gFnPc2	Olda
Hickory	Hickora	k1gFnSc2	Hickora
-	-	kIx~	-
ořechovec	ořechovec	k1gInSc1	ořechovec
je	být	k5eAaImIp3nS	být
strom	strom	k1gInSc1	strom
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
tvrdým	tvrdý	k2eAgNnSc7d1	tvrdé
dřevem	dřevo	k1gNnSc7	dřevo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
mylně	mylně	k6eAd1	mylně
překládána	překládat	k5eAaImNgFnS	překládat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Starý	starý	k2eAgInSc1d1	starý
ořešák	ořešák	k1gInSc1	ořešák
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
jiný	jiný	k2eAgInSc1d1	jiný
rod	rod	k1gInSc1	rod
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
emigrovali	emigrovat	k5eAaBmAgMnP	emigrovat
do	do	k7c2	do
USA	USA	kA	USA
z	z	k7c2	z
města	město	k1gNnSc2	město
Carrickfergus	Carrickfergus	k1gInSc1	Carrickfergus
v	v	k7c6	v
Severním	severní	k2eAgNnSc6d1	severní
Irsku	Irsko	k1gNnSc6	Irsko
roku	rok	k1gInSc2	rok
1765	[number]	k4	1765
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
jako	jako	k9	jako
nejmladší	mladý	k2eAgInPc1d3	nejmladší
ze	z	k7c2	z
třech	tři	k4xCgInPc2	tři
synů	syn	k1gMnPc2	syn
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
zaplétal	zaplétat	k5eAaImAgMnS	zaplétat
do	do	k7c2	do
duelů	duel	k1gInPc2	duel
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
považovaný	považovaný	k2eAgMnSc1d1	považovaný
za	za	k7c4	za
národního	národní	k2eAgMnSc4d1	národní
hrdinu	hrdina	k1gMnSc4	hrdina
<g/>
,	,	kIx,	,
když	když	k8xS	když
porazil	porazit	k5eAaPmAgInS	porazit
britská	britský	k2eAgNnPc4d1	Britské
vojska	vojsko	k1gNnPc4	vojsko
dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1815	[number]	k4	1815
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
New	New	k1gFnSc2	New
Orleans	Orleans	k1gInSc1	Orleans
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
bojoval	bojovat	k5eAaImAgInS	bojovat
v	v	k7c6	v
bitvách	bitva	k1gFnPc6	bitva
proti	proti	k7c3	proti
indiánským	indiánský	k2eAgInPc3d1	indiánský
kmenům	kmen	k1gInPc3	kmen
Kríkům	Kríek	k1gMnPc3	Kríek
a	a	k8xC	a
Seminolům	Seminol	k1gMnPc3	Seminol
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
útočili	útočit	k5eAaImAgMnP	útočit
na	na	k7c4	na
Floridu	Florida	k1gFnSc4	Florida
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roku	rok	k1gInSc6	rok
1819	[number]	k4	1819
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
jejím	její	k3xOp3gMnSc7	její
vojenským	vojenský	k2eAgMnSc7d1	vojenský
guvernérem	guvernér	k1gMnSc7	guvernér
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
ji	on	k3xPp3gFnSc4	on
dobyl	dobýt	k5eAaPmAgInS	dobýt
a	a	k8xC	a
připojil	připojit	k5eAaPmAgInS	připojit
ke	k	k7c3	k
Spojeným	spojený	k2eAgInPc3d1	spojený
státům	stát	k1gInPc3	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1821	[number]	k4	1821
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
řádným	řádný	k2eAgMnSc7d1	řádný
guvernérem	guvernér	k1gMnSc7	guvernér
Floridy	Florida	k1gFnSc2	Florida
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1823	[number]	k4	1823
senátorem	senátor	k1gMnSc7	senátor
za	za	k7c4	za
Tennessee	Tennessee	k1gFnSc4	Tennessee
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1824	[number]	k4	1824
soupeřil	soupeřit	k5eAaImAgInS	soupeřit
s	s	k7c7	s
Johnem	John	k1gMnSc7	John
Quincy	Quinca	k1gFnSc2	Quinca
Adamsem	Adams	k1gMnSc7	Adams
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
třemi	tři	k4xCgMnPc7	tři
kandidáty	kandidát	k1gMnPc7	kandidát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
nezískal	získat	k5eNaPmAgMnS	získat
většinu	většina	k1gFnSc4	většina
všech	všecek	k3xTgMnPc2	všecek
volitelů	volitel	k1gMnPc2	volitel
<g/>
.	.	kIx.	.
</s>
<s>
Sněmovna	sněmovna	k1gFnSc1	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
nakonec	nakonec	k6eAd1	nakonec
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
o	o	k7c6	o
Adamsovi	Adams	k1gMnSc6	Adams
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
Jackson	Jackson	k1gMnSc1	Jackson
získal	získat	k5eAaPmAgMnS	získat
o	o	k7c4	o
45	[number]	k4	45
000	[number]	k4	000
hlasů	hlas	k1gInPc2	hlas
více	hodně	k6eAd2	hodně
než	než	k8xS	než
on	on	k3xPp3gMnSc1	on
<g/>
.	.	kIx.	.
</s>
<s>
Jackson	Jackson	k1gMnSc1	Jackson
spustil	spustit	k5eAaPmAgMnS	spustit
poté	poté	k6eAd1	poté
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
celonárodní	celonárodní	k2eAgFnSc4d1	celonárodní
pomlouvačnou	pomlouvačný	k2eAgFnSc4d1	pomlouvačná
kampaň	kampaň	k1gFnSc4	kampaň
proti	proti	k7c3	proti
Adamsovi	Adams	k1gMnSc3	Adams
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgFnPc6d1	následující
volbách	volba	k1gFnPc6	volba
1828	[number]	k4	1828
po	po	k7c6	po
velmi	velmi	k6eAd1	velmi
ostré	ostrý	k2eAgFnSc6d1	ostrá
kampani	kampaň	k1gFnSc6	kampaň
plné	plný	k2eAgFnSc6d1	plná
osobních	osobní	k2eAgNnPc2d1	osobní
útoků	útok	k1gInPc2	útok
byl	být	k5eAaImAgMnS	být
již	již	k6eAd1	již
zvolen	zvolit	k5eAaPmNgMnS	zvolit
prezidentem	prezident	k1gMnSc7	prezident
(	(	kIx(	(
<g/>
první	první	k4xOgInPc1	první
z	z	k7c2	z
jižanských	jižanský	k2eAgInPc2d1	jižanský
států	stát	k1gInPc2	stát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1832	[number]	k4	1832
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
znovu	znovu	k6eAd1	znovu
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Cesta	cesta	k1gFnSc1	cesta
slz	slza	k1gFnPc2	slza
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
proslulý	proslulý	k2eAgInSc1d1	proslulý
svým	svůj	k3xOyFgNnSc7	svůj
celoživotním	celoživotní	k2eAgNnSc7d1	celoživotní
pronásledováním	pronásledování	k1gNnSc7	pronásledování
Indiánů	Indián	k1gMnPc2	Indián
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
populační	populační	k2eAgInSc1d1	populační
růst	růst	k1gInSc1	růst
amerického	americký	k2eAgNnSc2d1	americké
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
a	a	k8xC	a
objevení	objevení	k1gNnSc6	objevení
zlata	zlato	k1gNnSc2	zlato
způsobily	způsobit	k5eAaPmAgInP	způsobit
tlak	tlak	k1gInSc4	tlak
na	na	k7c4	na
území	území	k1gNnSc4	území
obývané	obývaný	k2eAgNnSc1d1	obývané
původním	původní	k2eAgNnSc7d1	původní
indiánským	indiánský	k2eAgNnSc7d1	indiánské
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1830	[number]	k4	1830
prošel	projít	k5eAaPmAgMnS	projít
Kongresem	kongres	k1gInSc7	kongres
souhlas	souhlas	k1gInSc4	souhlas
na	na	k7c4	na
vystěhovaní	vystěhovaný	k2eAgMnPc1d1	vystěhovaný
Indiánů	Indián	k1gMnPc2	Indián
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
Jackson	Jackson	k1gNnSc4	Jackson
podepsal	podepsat	k5eAaPmAgMnS	podepsat
<g/>
.	.	kIx.	.
</s>
<s>
Právní	právní	k2eAgInSc1d1	právní
akt	akt	k1gInSc1	akt
byl	být	k5eAaImAgInS	být
úspěšně	úspěšně	k6eAd1	úspěšně
napaden	napadnout	k5eAaPmNgInS	napadnout
národem	národ	k1gInSc7	národ
Čerokíů	Čerokí	k1gMnPc2	Čerokí
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1832	[number]	k4	1832
u	u	k7c2	u
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
soudu	soud	k1gInSc2	soud
Jackson	Jackson	k1gMnSc1	Jackson
vyžadoval	vyžadovat	k5eAaImAgMnS	vyžadovat
přehodnocení	přehodnocení	k1gNnSc4	přehodnocení
rozsudku	rozsudek	k1gInSc2	rozsudek
a	a	k8xC	a
fakticky	fakticky	k6eAd1	fakticky
byl	být	k5eAaImAgMnS	být
rozhodnutý	rozhodnutý	k2eAgMnSc1d1	rozhodnutý
to	ten	k3xDgNnSc4	ten
nerespektovat	respektovat	k5eNaImF	respektovat
<g/>
.	.	kIx.	.
</s>
<s>
Jackson	Jackson	k1gMnSc1	Jackson
nakonec	nakonec	k6eAd1	nakonec
poslal	poslat	k5eAaPmAgMnS	poslat
vojsko	vojsko	k1gNnSc4	vojsko
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vyhnalo	vyhnat	k5eAaPmAgNnS	vyhnat
domorodé	domorodý	k2eAgNnSc1d1	domorodé
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
nechvalně	chvalně	k6eNd1	chvalně
slavné	slavný	k2eAgFnSc3d1	slavná
"	"	kIx"	"
<g/>
Cestě	cesta	k1gFnSc3	cesta
slz	slza	k1gFnPc2	slza
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zabila	zabít	k5eAaPmAgFnS	zabít
při	při	k7c6	při
pochodu	pochod	k1gInSc6	pochod
téměř	téměř	k6eAd1	téměř
4	[number]	k4	4
000	[number]	k4	000
Čerokíů	Čerokí	k1gInPc2	Čerokí
(	(	kIx(	(
<g/>
asi	asi	k9	asi
čtvrtinu	čtvrtina	k1gFnSc4	čtvrtina
populace	populace	k1gFnSc2	populace
<g/>
)	)	kIx)	)
cestou	cesta	k1gFnSc7	cesta
do	do	k7c2	do
Oklahomy	Oklahom	k1gInPc4	Oklahom
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
svůj	svůj	k3xOyFgInSc4	svůj
největší	veliký	k2eAgInSc4d3	veliký
úspěch	úspěch	k1gInSc4	úspěch
na	na	k7c6	na
politickém	politický	k2eAgNnSc6d1	politické
poli	pole	k1gNnSc6	pole
sám	sám	k3xTgMnSc1	sám
Jackson	Jackson	k1gMnSc1	Jackson
ohodnotil	ohodnotit	k5eAaPmAgMnS	ohodnotit
svůj	svůj	k3xOyFgInSc4	svůj
úspěšný	úspěšný	k2eAgInSc4d1	úspěšný
boj	boj	k1gInSc4	boj
s	s	k7c7	s
The	The	k1gMnSc7	The
Second	Seconda	k1gFnPc2	Seconda
Bank	bank	k1gInSc1	bank
of	of	k?	of
America	Americ	k1gInSc2	Americ
(	(	kIx(	(
<g/>
ta	ten	k3xDgFnSc1	ten
usilovala	usilovat	k5eAaImAgFnS	usilovat
o	o	k7c4	o
dvacetiletou	dvacetiletý	k2eAgFnSc4d1	dvacetiletá
licenci	licence	k1gFnSc4	licence
vlády	vláda	k1gFnSc2	vláda
na	na	k7c4	na
vydávání	vydávání	k1gNnSc4	vydávání
fiat	fiat	k1gInSc4	fiat
currency	currency	k1gInPc7	currency
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
by	by	kYmCp3nS	by
inkasovala	inkasovat	k5eAaBmAgFnS	inkasovat
úrok	úrok	k1gInSc4	úrok
<g/>
)	)	kIx)	)
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
uzavření	uzavření	k1gNnSc4	uzavření
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zásadně	zásadně	k6eAd1	zásadně
zasadil	zasadit	k5eAaPmAgInS	zasadit
o	o	k7c4	o
neprodloužení	neprodloužení	k1gNnSc4	neprodloužení
jejího	její	k3xOp3gInSc2	její
mandátu	mandát	k1gInSc2	mandát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1836	[number]	k4	1836
a	a	k8xC	a
zabránil	zabránit	k5eAaPmAgInS	zabránit
jejímu	její	k3xOp3gMnSc3	její
znovuzavedení	znovuzavedení	k1gNnSc1	znovuzavedení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1841	[number]	k4	1841
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
kampaně	kampaň	k1gFnSc2	kampaň
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1828	[number]	k4	1828
Jackson	Jacksona	k1gFnPc2	Jacksona
při	při	k7c6	při
projevu	projev	k1gInSc6	projev
na	na	k7c4	na
adresu	adresa	k1gFnSc4	adresa
skupiny	skupina	k1gFnSc2	skupina
bankéřů	bankéř	k1gMnPc2	bankéř
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jste	být	k5eAaImIp2nP	být
doupě	doupě	k1gNnSc4	doupě
zmijí	zmije	k1gFnPc2	zmije
<g/>
.	.	kIx.	.
</s>
<s>
Mám	mít	k5eAaImIp1nS	mít
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
vaše	váš	k3xOp2gNnSc4	váš
hnízdo	hnízdo	k1gNnSc4	hnízdo
vypálit	vypálit	k5eAaPmF	vypálit
a	a	k8xC	a
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Boží	božit	k5eAaImIp3nP	božit
ho	on	k3xPp3gInSc4	on
vypálím	vypálit	k5eAaPmIp1nS	vypálit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Kdyby	kdyby	k9	kdyby
lidé	člověk	k1gMnPc1	člověk
jenom	jenom	k9	jenom
pochopili	pochopit	k5eAaPmAgMnP	pochopit
<g/>
,	,	kIx,	,
do	do	k7c2	do
jaké	jaký	k3yQgFnSc2	jaký
míry	míra	k1gFnSc2	míra
je	být	k5eAaImIp3nS	být
náš	náš	k3xOp1gInSc1	náš
peněžní	peněžní	k2eAgInSc1d1	peněžní
a	a	k8xC	a
bankovní	bankovní	k2eAgInSc1d1	bankovní
systém	systém	k1gInSc1	systém
nespravedlivý	spravedlivý	k2eNgInSc1d1	nespravedlivý
<g/>
,	,	kIx,	,
do	do	k7c2	do
rána	ráno	k1gNnSc2	ráno
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jackson	Jackson	k1gMnSc1	Jackson
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kdyby	kdyby	kYmCp3nS	kdyby
centrální	centrální	k2eAgFnSc1d1	centrální
banka	banka	k1gFnSc1	banka
měla	mít	k5eAaImAgFnS	mít
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
kontrole	kontrola	k1gFnSc6	kontrola
"	"	kIx"	"
<g/>
naší	náš	k3xOp1gFnSc2	náš
měny	měna	k1gFnSc2	měna
<g/>
,	,	kIx,	,
v	v	k7c6	v
dostávání	dostávání	k1gNnSc6	dostávání
našich	náš	k3xOp1gInPc2	náš
veřejných	veřejný	k2eAgInPc2d1	veřejný
peněz	peníze	k1gInPc2	peníze
a	a	k8xC	a
v	v	k7c4	v
držení	držení	k1gNnSc4	držení
tisíců	tisíc	k4xCgInPc2	tisíc
našich	naši	k1gMnPc2	naši
občanů	občan	k1gMnPc2	občan
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
strašnější	strašný	k2eAgNnSc1d2	strašnější
a	a	k8xC	a
nebezpečnější	bezpečný	k2eNgNnSc1d2	nebezpečnější
než	než	k8xS	než
námořní	námořní	k2eAgNnSc1d1	námořní
a	a	k8xC	a
pozemní	pozemní	k2eAgFnSc1d1	pozemní
síla	síla	k1gFnSc1	síla
nepřítele	nepřítel	k1gMnSc2	nepřítel
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
Návrh	návrh	k1gInSc1	návrh
zákona	zákon	k1gInSc2	zákon
na	na	k7c4	na
novou	nový	k2eAgFnSc4d1	nová
smlouvu	smlouva	k1gFnSc4	smlouva
prošel	projít	k5eAaPmAgInS	projít
Senátem	senát	k1gInSc7	senát
poměrem	poměr	k1gInSc7	poměr
28	[number]	k4	28
<g/>
/	/	kIx~	/
<g/>
20	[number]	k4	20
a	a	k8xC	a
Dolní	dolní	k2eAgFnSc7d1	dolní
komorou	komora	k1gFnSc7	komora
poměrem	poměr	k1gInSc7	poměr
107	[number]	k4	107
<g/>
/	/	kIx~	/
<g/>
85	[number]	k4	85
a	a	k8xC	a
každý	každý	k3xTgMnSc1	každý
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
Jackson	Jackson	k1gMnSc1	Jackson
zachová	zachovat	k5eAaPmIp3nS	zachovat
<g/>
.	.	kIx.	.
</s>
<s>
President	president	k1gMnSc1	president
banky	banka	k1gFnSc2	banka
Nicholas	Nicholas	k1gMnSc1	Nicholas
Biddle	Biddle	k1gMnSc1	Biddle
(	(	kIx(	(
<g/>
1786	[number]	k4	1786
<g/>
-	-	kIx~	-
<g/>
1844	[number]	k4	1844
<g/>
)	)	kIx)	)
vyhrožoval	vyhrožovat	k5eAaImAgMnS	vyhrožovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Když	když	k8xS	když
ho	on	k3xPp3gInSc4	on
Jackson	Jackson	k1gInSc4	Jackson
bude	být	k5eAaImBp3nS	být
vetovat	vetovat	k5eAaBmF	vetovat
<g/>
,	,	kIx,	,
já	já	k3xPp1nSc1	já
budu	být	k5eAaImBp1nS	být
vetovat	vetovat	k5eAaBmF	vetovat
jeho	jeho	k3xOp3gMnSc1	jeho
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Jackson	Jackson	k1gMnSc1	Jackson
smlouvu	smlouva	k1gFnSc4	smlouva
vetoval	vetovat	k5eAaBmAgMnS	vetovat
a	a	k8xC	a
Banku	banka	k1gFnSc4	banka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1832	[number]	k4	1832
zrušil	zrušit	k5eAaPmAgMnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Nařídil	nařídit	k5eAaPmAgInS	nařídit
sekretáři	sekretář	k1gInSc3	sekretář
státní	státní	k2eAgFnSc2d1	státní
pokladny	pokladna	k1gFnSc2	pokladna
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
odstranil	odstranit	k5eAaPmAgInS	odstranit
všechny	všechen	k3xTgInPc4	všechen
vklady	vklad	k1gInPc4	vklad
vlády	vláda	k1gFnSc2	vláda
z	z	k7c2	z
amerických	americký	k2eAgFnPc2d1	americká
bank	banka	k1gFnPc2	banka
a	a	k8xC	a
uložil	uložit	k5eAaPmAgMnS	uložit
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
státních	státní	k2eAgFnPc2d1	státní
bank	banka	k1gFnPc2	banka
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1835	[number]	k4	1835
Jackson	Jackson	k1gMnSc1	Jackson
zaplatil	zaplatit	k5eAaPmAgMnS	zaplatit
poslední	poslední	k2eAgFnSc4d1	poslední
splátku	splátka	k1gFnSc4	splátka
amerického	americký	k2eAgInSc2d1	americký
národního	národní	k2eAgInSc2d1	národní
dluhu	dluh	k1gInSc2	dluh
-	-	kIx~	-
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
jediný	jediný	k2eAgInSc1d1	jediný
okamžik	okamžik	k1gInSc1	okamžik
amerických	americký	k2eAgFnPc2d1	americká
dějin	dějiny	k1gFnPc2	dějiny
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
národní	národní	k2eAgInSc1d1	národní
dluh	dluh	k1gInSc1	dluh
byl	být	k5eAaImAgInS	být
redukován	redukovat	k5eAaBmNgInS	redukovat
na	na	k7c4	na
nulu	nula	k1gFnSc4	nula
a	a	k8xC	a
Američané	Američan	k1gMnPc1	Američan
byli	být	k5eAaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
nahromadit	nahromadit	k5eAaPmF	nahromadit
přebytek	přebytek	k1gInSc1	přebytek
35	[number]	k4	35
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
jednotlivým	jednotlivý	k2eAgInPc3d1	jednotlivý
státům	stát	k1gInPc3	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nicholas	Nicholas	k1gMnSc1	Nicholas
P.	P.	kA	P.
Trist	Trist	k1gMnSc1	Trist
<g/>
,	,	kIx,	,
prezidentův	prezidentův	k2eAgMnSc1d1	prezidentův
osobní	osobní	k2eAgMnSc1d1	osobní
tajemník	tajemník	k1gMnSc1	tajemník
<g/>
,	,	kIx,	,
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
koruna	koruna	k1gFnSc1	koruna
slávy	sláva	k1gFnSc2	sláva
Andrew	Andrew	k1gFnSc1	Andrew
Jacksona	Jacksona	k1gFnSc1	Jacksona
a	a	k8xC	a
nejdůležitější	důležitý	k2eAgFnSc1d3	nejdůležitější
služba	služba	k1gFnSc1	služba
<g/>
,	,	kIx,	,
jakou	jaký	k3yRgFnSc4	jaký
kdy	kdy	k6eAd1	kdy
své	svůj	k3xOyFgFnSc2	svůj
vlasti	vlast	k1gFnSc2	vlast
prokázal	prokázat	k5eAaPmAgInS	prokázat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Boston	Boston	k1gInSc1	Boston
Post	posta	k1gFnPc2	posta
ho	on	k3xPp3gInSc4	on
přirovnal	přirovnat	k5eAaPmAgMnS	přirovnat
k	k	k7c3	k
Ježíši	Ježíš	k1gMnSc3	Ježíš
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vyhnal	vyhnat	k5eAaPmAgMnS	vyhnat
penězoměnce	penězoměnec	k1gMnPc4	penězoměnec
z	z	k7c2	z
chrámu	chrám	k1gInSc2	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Jackson	Jackson	k1gMnSc1	Jackson
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
Kongres	kongres	k1gInSc1	kongres
podle	podle	k7c2	podle
Ústavy	ústava	k1gFnSc2	ústava
právo	právo	k1gNnSc1	právo
vydávat	vydávat	k5eAaImF	vydávat
papírové	papírový	k2eAgInPc4d1	papírový
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
toto	tento	k3xDgNnSc4	tento
právo	právo	k1gNnSc4	právo
dáno	dán	k2eAgNnSc4d1	dáno
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gMnSc4	on
používal	používat	k5eAaImAgInS	používat
a	a	k8xC	a
ne	ne	k9	ne
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gInSc4	on
delegoval	delegovat	k5eAaBmAgInS	delegovat
na	na	k7c4	na
jednotlivce	jednotlivec	k1gMnPc4	jednotlivec
nebo	nebo	k8xC	nebo
společnosti	společnost	k1gFnPc4	společnost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Jackson	Jackson	k1gMnSc1	Jackson
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
řízení	řízení	k1gNnSc1	řízení
<g />
.	.	kIx.	.
</s>
<s>
centrální	centrální	k2eAgFnSc2d1	centrální
banky	banka	k1gFnSc2	banka
"	"	kIx"	"
<g/>
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
prováděno	provádět	k5eAaImNgNnS	provádět
několika	několik	k4yIc7	několik
jednotlivci	jednotlivec	k1gMnPc7	jednotlivec
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
vládě	vláda	k1gFnSc6	vláda
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
by	by	kYmCp3nP	by
získali	získat	k5eAaPmAgMnP	získat
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
pracovní	pracovní	k2eAgFnSc7d1	pracovní
silou	síla	k1gFnSc7	síla
a	a	k8xC	a
příjmy	příjem	k1gInPc1	příjem
většiny	většina	k1gFnSc2	většina
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Zástupci	zástupce	k1gMnPc1	zástupce
Second	Second	k1gMnSc1	Second
Bank	bank	k1gInSc1	bank
of	of	k?	of
America	Americ	k1gInSc2	Americ
chtěli	chtít	k5eAaImAgMnP	chtít
převzít	převzít	k5eAaPmF	převzít
agendu	agenda	k1gFnSc4	agenda
tisku	tisk	k1gInSc2	tisk
dolarů	dolar	k1gInPc2	dolar
a	a	k8xC	a
ovládání	ovládání	k1gNnSc2	ovládání
přísunu	přísun	k1gInSc2	přísun
peněz	peníze	k1gInPc2	peníze
do	do	k7c2	do
americké	americký	k2eAgFnSc2d1	americká
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgInSc1	každý
dolar	dolar	k1gInSc1	dolar
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
zatížen	zatížit	k5eAaPmNgInS	zatížit
úrokem	úrok	k1gInSc7	úrok
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
vláda	vláda	k1gFnSc1	vláda
musela	muset	k5eAaImAgFnS	muset
bance	banka	k1gFnSc3	banka
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
splatit	splatit	k5eAaPmF	splatit
(	(	kIx(	(
<g/>
tento	tento	k3xDgInSc1	tento
princip	princip	k1gInSc1	princip
fungoval	fungovat	k5eAaImAgInS	fungovat
i	i	k9	i
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgInP	být
severoamerické	severoamerický	k2eAgInPc1d1	severoamerický
státy	stát	k1gInPc1	stát
britskými	britský	k2eAgFnPc7d1	britská
koloniemi	kolonie	k1gFnPc7	kolonie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jackson	Jackson	k1gMnSc1	Jackson
naopak	naopak	k6eAd1	naopak
prosadil	prosadit	k5eAaPmAgMnS	prosadit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vydávání	vydávání	k1gNnSc1	vydávání
dolarů	dolar	k1gInPc2	dolar
bude	být	k5eAaImBp3nS	být
bezúročné	bezúročný	k2eAgFnPc4d1	bezúročná
vůči	vůči	k7c3	vůči
nikomu	nikdo	k3yNnSc3	nikdo
a	a	k8xC	a
výši	výše	k1gFnSc4	výše
emisí	emise	k1gFnPc2	emise
bude	být	k5eAaImBp3nS	být
podle	podle	k7c2	podle
potřeb	potřeba	k1gFnPc2	potřeba
a	a	k8xC	a
růstu	růst	k1gInSc2	růst
ekonomiky	ekonomika	k1gFnSc2	ekonomika
řídit	řídit	k5eAaImF	řídit
občany	občan	k1gMnPc4	občan
volený	volený	k2eAgInSc1d1	volený
Kongres	kongres	k1gInSc1	kongres
(	(	kIx(	(
<g/>
čímž	což	k3yQnSc7	což
zůstane	zůstat	k5eAaPmIp3nS	zůstat
neporušena	porušen	k2eNgFnSc1d1	neporušena
možnost	možnost	k1gFnSc1	možnost
obyčejných	obyčejný	k2eAgMnPc2d1	obyčejný
občanů	občan	k1gMnPc2	občan
řídit	řídit	k5eAaImF	řídit
nepřímo	přímo	k6eNd1	přímo
finanční	finanční	k2eAgFnSc4d1	finanční
politiku	politika	k1gFnSc4	politika
své	svůj	k3xOyFgFnSc2	svůj
země	zem	k1gFnSc2	zem
skrze	skrze	k?	skrze
své	svůj	k3xOyFgFnSc6	svůj
volené	volená	k1gFnSc6	volená
zástupce	zástupce	k1gMnSc1	zástupce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
princip	princip	k1gInSc4	princip
bylo	být	k5eAaImAgNnS	být
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgNnPc2d1	hlavní
Jacksonových	Jacksonův	k2eAgNnPc2d1	Jacksonovo
témat	téma	k1gNnPc2	téma
při	při	k7c6	při
jeho	jeho	k3xOp3gFnSc6	jeho
kandidatuře	kandidatura	k1gFnSc6	kandidatura
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
<g/>
,	,	kIx,	,
i	i	k8xC	i
předtím	předtím	k6eAd1	předtím
na	na	k7c4	na
guvernéra	guvernér	k1gMnSc4	guvernér
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Jacksona	Jackson	k1gMnSc4	Jackson
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	jeho	k3xOp3gNnSc2	jeho
prezidentského	prezidentský	k2eAgNnSc2d1	prezidentské
období	období	k1gNnSc2	období
spáchány	spáchán	k2eAgInPc4d1	spáchán
dva	dva	k4xCgInPc4	dva
pokusy	pokus	k1gInPc4	pokus
o	o	k7c4	o
atentát	atentát	k1gInSc4	atentát
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
případ	případ	k1gInSc1	případ
ze	z	k7c2	z
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1833	[number]	k4	1833
je	být	k5eAaImIp3nS	být
prvním	první	k4xOgInSc7	první
zaznamenaným	zaznamenaný	k2eAgInSc7d1	zaznamenaný
fyzickým	fyzický	k2eAgInSc7d1	fyzický
útokem	útok	k1gInSc7	útok
na	na	k7c4	na
prezidenta	prezident	k1gMnSc2	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Útok	útok	k1gInSc1	útok
spáchal	spáchat	k5eAaPmAgInS	spáchat
Robert	Robert	k1gMnSc1	Robert
B.	B.	kA	B.
Randolph	Randolph	k1gMnSc1	Randolph
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
Jackson	Jackson	k1gMnSc1	Jackson
nařídil	nařídit	k5eAaPmAgMnS	nařídit
propustit	propustit	k5eAaPmF	propustit
z	z	k7c2	z
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
pro	pro	k7c4	pro
defraudaci	defraudace	k1gFnSc4	defraudace
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
pokus	pokus	k1gInSc1	pokus
se	se	k3xPyFc4	se
udál	udát	k5eAaPmAgInS	udát
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1835	[number]	k4	1835
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
atentátník	atentátník	k1gMnSc1	atentátník
Richard	Richard	k1gMnSc1	Richard
Lawrence	Lawrence	k1gFnSc1	Lawrence
<g/>
,	,	kIx,	,
nezaměstnaný	zaměstnaný	k2eNgMnSc1d1	nezaměstnaný
malíř	malíř	k1gMnSc1	malíř
pokojů	pokoj	k1gInPc2	pokoj
z	z	k7c2	z
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
vyzbrojený	vyzbrojený	k2eAgMnSc1d1	vyzbrojený
dvěma	dva	k4xCgFnPc7	dva
pistolemi	pistol	k1gFnPc7	pistol
každou	každý	k3xTgFnSc4	každý
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
ruce	ruka	k1gFnSc6	ruka
<g/>
,	,	kIx,	,
v	v	k7c4	v
nestřežený	střežený	k2eNgInSc4d1	nestřežený
moment	moment	k1gInSc4	moment
vypálil	vypálit	k5eAaPmAgMnS	vypálit
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
pistole	pistole	k1gFnSc1	pistole
však	však	k9	však
selhala	selhat	k5eAaPmAgFnS	selhat
a	a	k8xC	a
nevypálila	vypálit	k5eNaPmAgFnS	vypálit
<g/>
,	,	kIx,	,
použil	použít	k5eAaPmAgMnS	použít
tedy	tedy	k8xC	tedy
druhou	druhý	k4xOgFnSc7	druhý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
selhala	selhat	k5eAaPmAgFnS	selhat
i	i	k9	i
ta	ten	k3xDgFnSc1	ten
<g/>
.	.	kIx.	.
</s>
<s>
Historici	historik	k1gMnPc1	historik
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
díky	díky	k7c3	díky
vlhkému	vlhký	k2eAgNnSc3d1	vlhké
počasí	počasí	k1gNnSc3	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
veřejnému	veřejný	k2eAgInSc3d1	veřejný
podivu	podiv	k1gInSc3	podiv
nad	nad	k7c7	nad
dvojitým	dvojitý	k2eAgNnSc7d1	dvojité
selháním	selhání	k1gNnSc7	selhání
byly	být	k5eAaImAgFnP	být
obě	dva	k4xCgFnPc1	dva
pistole	pistol	k1gFnPc1	pistol
testovány	testován	k2eAgFnPc1d1	testována
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
testovány	testovat	k5eAaImNgInP	testovat
<g/>
.	.	kIx.	.
</s>
<s>
Pokaždé	pokaždé	k6eAd1	pokaždé
fungovaly	fungovat	k5eAaImAgFnP	fungovat
bezvadně	bezvadně	k6eAd1	bezvadně
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jackson	Jackson	k1gInSc1	Jackson
byl	být	k5eAaImAgInS	být
chráněn	chránit	k5eAaImNgInS	chránit
Prozřetelností	prozřetelnost	k1gFnSc7	prozřetelnost
<g/>
;	;	kIx,	;
událost	událost	k1gFnSc1	událost
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
Jacksonovského	Jacksonovský	k2eAgInSc2d1	Jacksonovský
mýtu	mýtus	k1gInSc2	mýtus
<g/>
.	.	kIx.	.
</s>
<s>
Jackson	Jackson	k1gMnSc1	Jackson
zemřel	zemřít	k5eAaPmAgMnS	zemřít
přirozenou	přirozený	k2eAgFnSc7d1	přirozená
smrtí	smrt	k1gFnSc7	smrt
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
78	[number]	k4	78
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
hrobce	hrobka	k1gFnSc6	hrobka
je	být	k5eAaImIp3nS	být
vytesáno	vytesat	k5eAaPmNgNnS	vytesat
jeho	jeho	k3xOp3gNnSc1	jeho
krédo	krédo	k1gNnSc1	krédo
I	i	k9	i
killed	killed	k1gInSc1	killed
the	the	k?	the
bank	banka	k1gFnPc2	banka
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Zabil	zabít	k5eAaPmAgMnS	zabít
jsem	být	k5eAaImIp1nS	být
tu	ten	k3xDgFnSc4	ten
banku	banka	k1gFnSc4	banka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Jacksonovi	Jackson	k1gMnSc6	Jackson
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
jako	jako	k9	jako
o	o	k7c6	o
jediném	jediný	k2eAgMnSc6d1	jediný
prezidentu	prezident	k1gMnSc6	prezident
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dokázal	dokázat	k5eAaPmAgInS	dokázat
zbavit	zbavit	k5eAaPmF	zbavit
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
národního	národní	k2eAgInSc2d1	národní
dluhu	dluh	k1gInSc2	dluh
<g/>
,	,	kIx,	,
a	a	k8xC	a
též	též	k9	též
jediném	jediný	k2eAgInSc6d1	jediný
prezidentu	prezident	k1gMnSc3	prezident
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
splnil	splnit	k5eAaPmAgMnS	splnit
vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
slíbil	slíbit	k5eAaPmAgMnS	slíbit
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
volební	volební	k2eAgFnSc6d1	volební
kampani	kampaň	k1gFnSc6	kampaň
<g/>
.	.	kIx.	.
</s>
