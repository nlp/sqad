<s>
Kosmické	kosmický	k2eAgNnSc1d1	kosmické
záření	záření	k1gNnSc1	záření
je	být	k5eAaImIp3nS	být
proud	proud	k1gInSc4	proud
energetických	energetický	k2eAgFnPc2d1	energetická
částic	částice	k1gFnPc2	částice
pocházejících	pocházející	k2eAgFnPc2d1	pocházející
z	z	k7c2	z
kosmu	kosmos	k1gInSc2	kosmos
<g/>
,	,	kIx,	,
pohybujících	pohybující	k2eAgInPc2d1	pohybující
se	se	k3xPyFc4	se
vysokou	vysoký	k2eAgFnSc7d1	vysoká
rychlostí	rychlost	k1gFnSc7	rychlost
a	a	k8xC	a
dopadajících	dopadající	k2eAgMnPc2d1	dopadající
do	do	k7c2	do
zemské	zemský	k2eAgFnSc2d1	zemská
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
