<p>
<s>
Kosmické	kosmický	k2eAgNnSc1d1	kosmické
záření	záření	k1gNnSc1	záření
je	být	k5eAaImIp3nS	být
proud	proud	k1gInSc4	proud
energetických	energetický	k2eAgFnPc2d1	energetická
částic	částice	k1gFnPc2	částice
pocházejících	pocházející	k2eAgFnPc2d1	pocházející
z	z	k7c2	z
kosmu	kosmos	k1gInSc2	kosmos
<g/>
,	,	kIx,	,
pohybujících	pohybující	k2eAgInPc2d1	pohybující
se	se	k3xPyFc4	se
vysokou	vysoký	k2eAgFnSc7d1	vysoká
rychlostí	rychlost	k1gFnSc7	rychlost
a	a	k8xC	a
dopadajících	dopadající	k2eAgMnPc2d1	dopadající
do	do	k7c2	do
zemské	zemský	k2eAgFnSc2d1	zemská
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
především	především	k9	především
o	o	k7c4	o
protony	proton	k1gInPc4	proton
(	(	kIx(	(
<g/>
85	[number]	k4	85
až	až	k9	až
90	[number]	k4	90
procent	procento	k1gNnPc2	procento
<g/>
)	)	kIx)	)
a	a	k8xC	a
jádra	jádro	k1gNnSc2	jádro
hélia	hélium	k1gNnSc2	hélium
(	(	kIx(	(
<g/>
9	[number]	k4	9
až	až	k9	až
14	[number]	k4	14
procent	procento	k1gNnPc2	procento
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
tvoří	tvořit	k5eAaImIp3nS	tvořit
elektrony	elektron	k1gInPc4	elektron
<g/>
,	,	kIx,	,
jádra	jádro	k1gNnPc4	jádro
jiných	jiný	k1gMnPc2	jiný
atomů	atom	k1gInPc2	atom
a	a	k8xC	a
další	další	k2eAgFnSc2d1	další
elementární	elementární	k2eAgFnSc2d1	elementární
částice	částice	k1gFnSc2	částice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
objevil	objevit	k5eAaPmAgMnS	objevit
rakouský	rakouský	k2eAgMnSc1d1	rakouský
fyzik	fyzik	k1gMnSc1	fyzik
Victor	Victor	k1gMnSc1	Victor
Franz	Franz	k1gMnSc1	Franz
Hess	Hess	k1gInSc4	Hess
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
intenzita	intenzita	k1gFnSc1	intenzita
roste	růst	k5eAaImIp3nS	růst
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
<g/>
.	.	kIx.	.
</s>
<s>
Usoudil	usoudit	k5eAaPmAgMnS	usoudit
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
záření	záření	k1gNnSc1	záření
je	být	k5eAaImIp3nS	být
kosmického	kosmický	k2eAgInSc2d1	kosmický
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
objevil	objevit	k5eAaPmAgMnS	objevit
francouzský	francouzský	k2eAgMnSc1d1	francouzský
astronom	astronom	k1gMnSc1	astronom
Pierre	Pierr	k1gInSc5	Pierr
Auger	Auger	k1gInSc4	Auger
spršky	sprška	k1gFnSc2	sprška
atmosférického	atmosférický	k2eAgNnSc2d1	atmosférické
kosmického	kosmický	k2eAgNnSc2d1	kosmické
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
jako	jako	k9	jako
sekundární	sekundární	k2eAgNnSc1d1	sekundární
záření	záření	k1gNnSc1	záření
vyvolané	vyvolaný	k2eAgNnSc1d1	vyvolané
dopadem	dopad	k1gInSc7	dopad
primárních	primární	k2eAgFnPc2d1	primární
částic	částice	k1gFnPc2	částice
z	z	k7c2	z
vesmíru	vesmír	k1gInSc2	vesmír
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
interakcí	interakce	k1gFnPc2	interakce
s	s	k7c7	s
atomy	atom	k1gInPc7	atom
vysoko	vysoko	k6eAd1	vysoko
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Energie	energie	k1gFnSc1	energie
těchto	tento	k3xDgFnPc2	tento
částic	částice	k1gFnPc2	částice
může	moct	k5eAaImIp3nS	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
až	až	k9	až
1020	[number]	k4	1020
elektronvoltů	elektronvolt	k1gInPc2	elektronvolt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původ	původ	k1gInSc1	původ
kosmického	kosmický	k2eAgNnSc2d1	kosmické
záření	záření	k1gNnSc2	záření
není	být	k5eNaImIp3nS	být
dosud	dosud	k6eAd1	dosud
zcela	zcela	k6eAd1	zcela
objasněn	objasnit	k5eAaPmNgInS	objasnit
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
v	v	k7c6	v
Argentině	Argentina	k1gFnSc6	Argentina
uvedena	uveden	k2eAgNnPc1d1	uvedeno
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
Observatoř	observatoř	k1gFnSc1	observatoř
Pierra	Pierra	k1gFnSc1	Pierra
Augera	Augera	k1gFnSc1	Augera
–	–	k?	–
největší	veliký	k2eAgInSc1d3	veliký
detektor	detektor	k1gInSc1	detektor
kosmického	kosmický	k2eAgNnSc2d1	kosmické
záření	záření	k1gNnSc2	záření
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
se	se	k3xPyFc4	se
podílejí	podílet	k5eAaImIp3nP	podílet
také	také	k9	také
vědci	vědec	k1gMnPc1	vědec
z	z	k7c2	z
Fyzikálního	fyzikální	k2eAgInSc2d1	fyzikální
ústavu	ústav	k1gInSc2	ústav
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
Univerzity	univerzita	k1gFnSc2	univerzita
Palackého	Palackého	k2eAgFnSc2d1	Palackého
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
.	.	kIx.	.
<g/>
Část	část	k1gFnSc1	část
kosmického	kosmický	k2eAgNnSc2d1	kosmické
záření	záření	k1gNnSc2	záření
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
z	z	k7c2	z
mezihvězdného	mezihvězdný	k2eAgInSc2d1	mezihvězdný
a	a	k8xC	a
mezigalaktického	mezigalaktický	k2eAgInSc2d1	mezigalaktický
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zdroje	zdroj	k1gInPc4	zdroj
částic	částice	k1gFnPc2	částice
s	s	k7c7	s
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
energií	energie	k1gFnSc7	energie
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
daleko	daleko	k6eAd1	daleko
mimo	mimo	k7c4	mimo
oblast	oblast	k1gFnSc4	oblast
naší	náš	k3xOp1gFnSc2	náš
galaxie	galaxie	k1gFnSc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nepotvrzené	potvrzený	k2eNgFnSc2d1	nepotvrzená
hypotézy	hypotéza	k1gFnSc2	hypotéza
italského	italský	k2eAgMnSc2d1	italský
fyzika	fyzik	k1gMnSc2	fyzik
Enrico	Enrico	k6eAd1	Enrico
Fermiho	Fermi	k1gMnSc4	Fermi
vzniká	vznikat	k5eAaImIp3nS	vznikat
záření	záření	k1gNnSc1	záření
při	při	k7c6	při
explozi	exploze	k1gFnSc6	exploze
supernov	supernova	k1gFnPc2	supernova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kosmické	kosmický	k2eAgFnSc2d1	kosmická
záření	záření	k1gNnSc4	záření
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Observatoř	observatoř	k1gFnSc1	observatoř
Pierre	Pierr	k1gInSc5	Pierr
Augera	Auger	k1gMnSc2	Auger
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
skupina	skupina	k1gFnSc1	skupina
spolupracující	spolupracující	k2eAgFnSc1d1	spolupracující
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
</s>
</p>
