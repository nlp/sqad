<s>
Cyrilice	cyrilice	k1gFnSc1	cyrilice
(	(	kIx(	(
<g/>
též	též	k9	též
cyrilika	cyrilika	k1gFnSc1	cyrilika
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
písmo	písmo	k1gNnSc4	písmo
původně	původně	k6eAd1	původně
vytvořené	vytvořený	k2eAgFnPc1d1	vytvořená
pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
staroslověnštiny	staroslověnština	k1gFnSc2	staroslověnština
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
používané	používaný	k2eAgInPc1d1	používaný
pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
církevní	církevní	k2eAgFnSc2d1	církevní
slovanštiny	slovanština	k1gFnSc2	slovanština
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
na	na	k7c4	na
staroslověnštinu	staroslověnština	k1gFnSc4	staroslověnština
navázala	navázat	k5eAaPmAgFnS	navázat
<g/>
.	.	kIx.	.
</s>
