<s>
Audrey	Audrea	k1gFnPc1	Audrea
Hepburnová	Hepburnová	k1gFnSc1	Hepburnová
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Audrey	Audrea	k1gFnSc2	Audrea
Kathleen	Kathleen	k2eAgInSc4d1	Kathleen
van	van	k1gInSc4	van
Heemstra	Heemstra	k1gFnSc1	Heemstra
Hepburn-Ruston	Hepburn-Ruston	k1gInSc1	Hepburn-Ruston
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
Audrey	Audre	k2eAgInPc1d1	Audre
Ruston	Ruston	k1gInSc1	Ruston
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1929	[number]	k4	1929
Brusel	Brusel	k1gInSc1	Brusel
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc1	Belgie
–	–	k?	–
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1993	[number]	k4	1993
Tolochenaz	Tolochenaz	k1gInSc4	Tolochenaz
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc4	Švýcarsko
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
britská	britský	k2eAgFnSc1d1	britská
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
velvyslankyně	velvyslankyně	k1gFnSc1	velvyslankyně
UNICEF	UNICEF	kA	UNICEF
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
tváří	tvář	k1gFnPc2	tvář
filmového	filmový	k2eAgNnSc2d1	filmové
plátna	plátno	k1gNnSc2	plátno
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
nositelka	nositelka	k1gFnSc1	nositelka
cen	cena	k1gFnPc2	cena
Oscar	Oscara	k1gFnPc2	Oscara
<g/>
,	,	kIx,	,
Tony	Tony	k1gFnPc2	Tony
<g/>
,	,	kIx,	,
Emmy	Emma	k1gFnSc2	Emma
i	i	k8xC	i
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
.	.	kIx.	.
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
dětství	dětství	k1gNnSc4	dětství
strávila	strávit	k5eAaPmAgFnS	strávit
vedle	vedle	k7c2	vedle
rodné	rodný	k2eAgFnSc2d1	rodná
Belgie	Belgie	k1gFnSc2	Belgie
také	také	k9	také
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
a	a	k8xC	a
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
<g/>
;	;	kIx,	;
v	v	k7c6	v
Arnhemu	Arnhem	k1gInSc6	Arnhem
prožila	prožít	k5eAaPmAgFnS	prožít
druhou	druhý	k4xOgFnSc4	druhý
světovou	světový	k2eAgFnSc4d1	světová
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
operace	operace	k1gFnSc2	operace
Market	market	k1gInSc1	market
Garden	Gardno	k1gNnPc2	Gardno
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
baronka	baronka	k1gFnSc1	baronka
Ella	Ella	k1gFnSc1	Ella
van	van	k1gInSc1	van
Heemstra	Heemstra	k1gFnSc1	Heemstra
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
–	–	k?	–
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
velmi	velmi	k6eAd1	velmi
staré	starý	k2eAgFnSc2d1	stará
a	a	k8xC	a
významné	významný	k2eAgFnSc2d1	významná
nizozemské	nizozemský	k2eAgFnSc2d1	nizozemská
rodové	rodový	k2eAgFnSc2d1	rodová
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
,	,	kIx,	,
příjmení	příjmení	k1gNnSc2	příjmení
Hepburn	Hepburna	k1gFnPc2	Hepburna
pak	pak	k6eAd1	pak
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
Irských	irský	k2eAgInPc2d1	irský
šlechtických	šlechtický	k2eAgInPc2d1	šlechtický
předků	předek	k1gInPc2	předek
jejího	její	k3xOp3gMnSc2	její
otce	otec	k1gMnSc2	otec
Josepha	Joseph	k1gMnSc2	Joseph
Rustona	Ruston	k1gMnSc2	Ruston
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1889	[number]	k4	1889
Úžice	Úžice	k1gFnSc2	Úžice
–	–	k?	–
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
údajně	údajně	k6eAd1	údajně
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gInSc7	jeho
předkem	předek	k1gInSc7	předek
James	James	k1gMnSc1	James
Hepburn	Hepburn	k1gMnSc1	Hepburn
<g/>
,	,	kIx,	,
hrabě	hrabě	k1gMnSc1	hrabě
Bothwell	Bothwell	k1gMnSc1	Bothwell
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgMnSc1	třetí
manžel	manžel	k1gMnSc1	manžel
skotské	skotský	k2eAgFnSc2d1	skotská
královny	královna	k1gFnSc2	královna
Marie	Maria	k1gFnSc2	Maria
Stuartovny	Stuartovna	k1gFnSc2	Stuartovna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
15	[number]	k4	15
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
Prahy	Praha	k1gFnSc2	Praha
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Úžice	Úžice	k1gFnSc2	Úžice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
cukrovar	cukrovar	k1gInSc4	cukrovar
Jos	Jos	k1gFnSc2	Jos
<g/>
.	.	kIx.	.
</s>
<s>
J.	J.	kA	J.
Ruston	Ruston	k1gInSc1	Ruston
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
chtěla	chtít	k5eAaImAgFnS	chtít
být	být	k5eAaImF	být
primabalerinou	primabalerina	k1gFnSc7	primabalerina
a	a	k8xC	a
baletu	balet	k1gInSc2	balet
se	se	k3xPyFc4	se
také	také	k9	také
plně	plně	k6eAd1	plně
věnovala	věnovat	k5eAaImAgFnS	věnovat
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
získala	získat	k5eAaPmAgFnS	získat
stipendium	stipendium	k1gNnSc4	stipendium
na	na	k7c6	na
prestižní	prestižní	k2eAgFnSc6d1	prestižní
baletní	baletní	k2eAgFnSc6d1	baletní
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
však	však	k9	však
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
tělesné	tělesný	k2eAgFnSc3d1	tělesná
výšce	výška	k1gFnSc3	výška
nemůže	moct	k5eNaImIp3nS	moct
s	s	k7c7	s
kariérou	kariéra	k1gFnSc7	kariéra
primabaleriny	primabalerin	k1gInPc1	primabalerin
počítat	počítat	k5eAaImF	počítat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
baletním	baletní	k2eAgInSc6d1	baletní
souboru	soubor	k1gInSc6	soubor
si	se	k3xPyFc3	se
jí	on	k3xPp3gFnSc7	on
ale	ale	k8xC	ale
všimli	všimnout	k5eAaPmAgMnP	všimnout
muzikáloví	muzikálový	k2eAgMnPc1d1	muzikálový
a	a	k8xC	a
filmoví	filmový	k2eAgMnPc1d1	filmový
producenti	producent	k1gMnPc1	producent
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
získala	získat	k5eAaPmAgFnS	získat
své	svůj	k3xOyFgFnPc4	svůj
první	první	k4xOgFnPc4	první
malé	malý	k2eAgFnPc4d1	malá
role	role	k1gFnPc4	role
v	v	k7c6	v
nevýznamných	významný	k2eNgInPc6d1	nevýznamný
britských	britský	k2eAgInPc6d1	britský
filmech	film	k1gInPc6	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
na	na	k7c4	na
popud	popud	k1gInSc4	popud
francouzské	francouzský	k2eAgFnSc2d1	francouzská
spisovatelky	spisovatelka	k1gFnSc2	spisovatelka
Colette	Colett	k1gInSc5	Colett
přijala	přijmout	k5eAaPmAgFnS	přijmout
nabídku	nabídka	k1gFnSc4	nabídka
na	na	k7c4	na
titulní	titulní	k2eAgFnSc4d1	titulní
roli	role	k1gFnSc4	role
broadwayské	broadwayský	k2eAgFnSc2d1	broadwayská
show	show	k1gFnSc2	show
Gigi	Gig	k1gFnSc2	Gig
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
opustila	opustit	k5eAaPmAgFnS	opustit
evropský	evropský	k2eAgInSc4d1	evropský
kontinent	kontinent	k1gInSc4	kontinent
a	a	k8xC	a
odjela	odjet	k5eAaPmAgFnS	odjet
do	do	k7c2	do
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
současně	současně	k6eAd1	současně
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
(	(	kIx(	(
<g/>
obsazení	obsazení	k1gNnSc4	obsazení
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
natáčení	natáčení	k1gNnSc1	natáčení
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
premiéra	premiéra	k1gFnSc1	premiéra
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
také	také	k6eAd1	také
přišel	přijít	k5eAaPmAgInS	přijít
druhý	druhý	k4xOgInSc1	druhý
zásadní	zásadní	k2eAgInSc1d1	zásadní
zlom	zlom	k1gInSc1	zlom
její	její	k3xOp3gFnSc2	její
herecké	herecký	k2eAgFnSc2d1	herecká
kariéry	kariéra	k1gFnSc2	kariéra
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
hlavní	hlavní	k2eAgFnSc2d1	hlavní
role	role	k1gFnSc2	role
v	v	k7c4	v
komedii	komedie	k1gFnSc4	komedie
Prázdniny	prázdniny	k1gFnPc1	prázdniny
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ji	on	k3xPp3gFnSc4	on
vynesla	vynést	k5eAaPmAgFnS	vynést
do	do	k7c2	do
hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
film	film	k1gInSc4	film
také	také	k9	také
získala	získat	k5eAaPmAgFnS	získat
(	(	kIx(	(
<g/>
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
filmu	film	k1gInSc6	film
debutující	debutující	k2eAgFnPc1d1	debutující
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
cenu	cena	k1gFnSc4	cena
americké	americký	k2eAgFnSc2d1	americká
filmové	filmový	k2eAgFnSc2d1	filmová
akademie	akademie	k1gFnSc2	akademie
–	–	k?	–
Oscar	Oscar	k1gMnSc1	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
krátce	krátce	k6eAd1	krátce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
také	také	k9	také
získala	získat	k5eAaPmAgFnS	získat
prestižní	prestižní	k2eAgFnSc4d1	prestižní
divadelní	divadelní	k2eAgNnSc1d1	divadelní
ocenění	ocenění	k1gNnSc1	ocenění
Tony	Tony	k1gFnSc2	Tony
za	za	k7c4	za
roli	role	k1gFnSc4	role
Ondiny	Ondina	k1gFnSc2	Ondina
ve	v	k7c6	v
stejnojmenné	stejnojmenný	k2eAgFnSc6d1	stejnojmenná
broadwayské	broadwayský	k2eAgFnSc6d1	broadwayská
divadelní	divadelní	k2eAgFnSc6d1	divadelní
hře	hra	k1gFnSc6	hra
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
objevovala	objevovat	k5eAaImAgFnS	objevovat
v	v	k7c6	v
nejklasičtějších	klasický	k2eAgInPc6d3	nejklasičtější
dílech	díl	k1gInPc6	díl
kinematografie	kinematografie	k1gFnSc2	kinematografie
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
Sabrina	Sabrina	k1gFnSc1	Sabrina
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
Vojna	vojna	k1gFnSc1	vojna
a	a	k8xC	a
mír	mír	k1gInSc1	mír
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
Usměvavá	usměvavý	k2eAgFnSc1d1	usměvavá
tvář	tvář	k1gFnSc1	tvář
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
Odpolední	odpolední	k2eAgFnSc1d1	odpolední
láska	láska	k1gFnSc1	láska
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Nun	Nun	k1gMnSc1	Nun
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
story	story	k1gFnSc7	story
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
Snídaně	snídaně	k1gFnSc1	snídaně
u	u	k7c2	u
Tiffanyho	Tiffany	k1gMnSc2	Tiffany
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
Šaráda	šaráda	k1gFnSc1	šaráda
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
My	my	k3xPp1nPc1	my
Fair	fair	k2eAgFnPc4d1	fair
Lady	lady	k1gFnSc7	lady
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
Čekej	čekat	k5eAaImRp2nS	čekat
do	do	k7c2	do
tmy	tma	k1gFnSc2	tma
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
Robin	Robina	k1gFnPc2	Robina
a	a	k8xC	a
Mariana	Mariana	k1gFnSc1	Mariana
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
Svoji	svůj	k3xOyFgFnSc4	svůj
poslední	poslední	k2eAgFnSc4d1	poslední
menší	malý	k2eAgFnSc4d2	menší
filmovou	filmový	k2eAgFnSc4d1	filmová
roli	role	k1gFnSc4	role
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
šedesáti	šedesát	k4xCc6	šedesát
letech	léto	k1gNnPc6	léto
ve	v	k7c6	v
Spielbergově	Spielbergův	k2eAgMnSc6d1	Spielbergův
Navždy	navždy	k6eAd1	navždy
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
velmi	velmi	k6eAd1	velmi
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
měkký	měkký	k2eAgInSc4d1	měkký
hlas	hlas	k1gInSc4	hlas
v	v	k7c6	v
mezzosopránové	mezzosopránový	k2eAgFnSc6d1	mezzosopránová
poloze	poloha	k1gFnSc6	poloha
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
také	také	k9	také
v	v	k7c6	v
několika	několik	k4yIc6	několik
filmech	film	k1gInPc6	film
předvedla	předvést	k5eAaPmAgFnS	předvést
i	i	k9	i
při	při	k7c6	při
zpěvu	zpěv	k1gInSc6	zpěv
-	-	kIx~	-
např.	např.	kA	např.
ve	v	k7c6	v
snímku	snímek	k1gInSc6	snímek
Usměvavá	usměvavý	k2eAgFnSc1d1	usměvavá
tvář	tvář	k1gFnSc1	tvář
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Snídaně	snídaně	k1gFnSc2	snídaně
u	u	k7c2	u
Tiffanyho	Tiffany	k1gMnSc2	Tiffany
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
světoznámém	světoznámý	k2eAgInSc6d1	světoznámý
muzikálu	muzikál	k1gInSc6	muzikál
My	my	k3xPp1nPc1	my
Fair	fair	k2eAgFnSc1d1	fair
Lady	lady	k1gFnSc1	lady
její	její	k3xOp3gInSc4	její
zpěv	zpěv	k1gInSc4	zpěv
dabovala	dabovat	k5eAaBmAgFnS	dabovat
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Marni	marnit	k5eAaImRp2nS	marnit
Nixonová	Nixonový	k2eAgFnSc5d1	Nixonový
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
mnohé	mnohé	k1gNnSc4	mnohé
představovala	představovat	k5eAaImAgFnS	představovat
nový	nový	k2eAgInSc4d1	nový
typ	typ	k1gInSc4	typ
krásy	krása	k1gFnSc2	krása
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
útlý	útlý	k2eAgInSc4d1	útlý
vzhled	vzhled	k1gInSc4	vzhled
často	často	k6eAd1	často
řídil	řídit	k5eAaImAgMnS	řídit
vývoj	vývoj	k1gInSc4	vývoj
módy	móda	k1gFnSc2	móda
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
nedostižným	nedostižným	k?	nedostižným
symbolem	symbol	k1gInSc7	symbol
elegance	elegance	k1gFnSc2	elegance
<g/>
,	,	kIx,	,
idolem	idol	k1gInSc7	idol
osobního	osobní	k2eAgInSc2d1	osobní
šarmu	šarm	k1gInSc2	šarm
<g/>
,	,	kIx,	,
noblesy	noblesa	k1gFnSc2	noblesa
i	i	k8xC	i
charisma	charisma	k1gNnSc1	charisma
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
svérázným	svérázný	k2eAgInSc7d1	svérázný
prototypem	prototyp	k1gInSc7	prototyp
nevšední	všední	k2eNgFnSc2d1	nevšední
krásy	krása	k1gFnSc2	krása
<g/>
,	,	kIx,	,
inteligence	inteligence	k1gFnSc2	inteligence
a	a	k8xC	a
mimořádné	mimořádný	k2eAgFnSc2d1	mimořádná
oduševnělosti	oduševnělost	k1gFnSc2	oduševnělost
(	(	kIx(	(
<g/>
Audrey	Audrea	k1gFnSc2	Audrea
hovořila	hovořit	k5eAaImAgFnS	hovořit
plynně	plynně	k6eAd1	plynně
sedmi	sedm	k4xCc7	sedm
světovými	světový	k2eAgInPc7d1	světový
jazyky	jazyk	k1gInPc7	jazyk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
vše	všechen	k3xTgNnSc1	všechen
se	se	k3xPyFc4	se
ale	ale	k9	ale
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
útlé	útlý	k2eAgFnSc6d1	útlá
osobě	osoba	k1gFnSc6	osoba
dosti	dosti	k6eAd1	dosti
vzácně	vzácně	k6eAd1	vzácně
snoubilo	snoubit	k5eAaImAgNnS	snoubit
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
hereckým	herecký	k2eAgNnSc7d1	herecké
a	a	k8xC	a
pohybovým	pohybový	k2eAgNnSc7d1	pohybové
nadáním	nadání	k1gNnSc7	nadání
do	do	k7c2	do
nenapodobitelného	napodobitelný	k2eNgInSc2d1	nenapodobitelný
originálu	originál	k1gInSc2	originál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
dodnes	dodnes	k6eAd1	dodnes
obdivují	obdivovat	k5eAaImIp3nP	obdivovat
miliony	milion	k4xCgInPc1	milion
filmových	filmový	k2eAgMnPc2d1	filmový
a	a	k8xC	a
televizních	televizní	k2eAgMnPc2d1	televizní
diváků	divák	k1gMnPc2	divák
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
porodila	porodit	k5eAaPmAgFnS	porodit
a	a	k8xC	a
úspěšně	úspěšně	k6eAd1	úspěšně
vychovala	vychovat	k5eAaPmAgFnS	vychovat
dva	dva	k4xCgMnPc4	dva
syny	syn	k1gMnPc4	syn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
letech	let	k1gInPc6	let
svého	své	k1gNnSc2	své
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
věnovala	věnovat	k5eAaImAgFnS	věnovat
především	především	k9	především
práci	práce	k1gFnSc4	práce
pro	pro	k7c4	pro
humanitární	humanitární	k2eAgFnPc4d1	humanitární
organizace	organizace	k1gFnPc4	organizace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
oficiálním	oficiální	k2eAgMnSc7d1	oficiální
velvyslancem	velvyslanec	k1gMnSc7	velvyslanec
dobré	dobrý	k2eAgFnSc2d1	dobrá
vůle	vůle	k1gFnSc2	vůle
UNICEF	UNICEF	kA	UNICEF
<g/>
.	.	kIx.	.
</s>
<s>
Cestovala	cestovat	k5eAaImAgFnS	cestovat
do	do	k7c2	do
rozvojových	rozvojový	k2eAgFnPc2d1	rozvojová
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
do	do	k7c2	do
Somálska	Somálsko	k1gNnSc2	Somálsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pomáhala	pomáhat	k5eAaImAgFnS	pomáhat
hladovějícím	hladovějící	k2eAgFnPc3d1	hladovějící
dětem	dítě	k1gFnPc3	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnPc1	její
synové	syn	k1gMnPc1	syn
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
,	,	kIx,	,
na	na	k7c6	na
její	její	k3xOp3gFnSc6	její
oficiální	oficiální	k2eAgFnSc6d1	oficiální
stránce	stránka	k1gFnSc6	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Zemřela	zemřít	k5eAaPmAgFnS	zemřít
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
slepého	slepý	k2eAgNnSc2d1	slepé
střeva	střevo	k1gNnSc2	střevo
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1993	[number]	k4	1993
ve	v	k7c6	v
švýcarském	švýcarský	k2eAgNnSc6d1	švýcarské
Tolochenaz	Tolochenaz	k1gInSc4	Tolochenaz
v	v	k7c6	v
kantonu	kanton	k1gInSc6	kanton
Vaud	Vauda	k1gFnPc2	Vauda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
také	také	k9	také
pochována	pochovat	k5eAaPmNgFnS	pochovat
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
hvězdu	hvězda	k1gFnSc4	hvězda
na	na	k7c6	na
Hollywoodském	hollywoodský	k2eAgInSc6d1	hollywoodský
chodníku	chodník	k1gInSc6	chodník
slávy	sláva	k1gFnSc2	sláva
<g/>
.	.	kIx.	.
</s>
<s>
Poznámka	poznámka	k1gFnSc1	poznámka
:	:	kIx,	:
zkratka	zkratka	k1gFnSc1	zkratka
BAFTA	BAFTA	kA	BAFTA
představuje	představovat	k5eAaImIp3nS	představovat
Britskou	britský	k2eAgFnSc4d1	britská
televizní	televizní	k2eAgFnSc4d1	televizní
a	a	k8xC	a
filmovou	filmový	k2eAgFnSc4d1	filmová
akademii	akademie	k1gFnSc4	akademie
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
USA	USA	kA	USA
natočen	natočit	k5eAaBmNgInS	natočit
dvoudílný	dvoudílný	k2eAgInSc1d1	dvoudílný
životopisný	životopisný	k2eAgInSc1d1	životopisný
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
Příběh	příběh	k1gInSc1	příběh
Audrey	Audrea	k1gFnSc2	Audrea
Hepburnové	Hepburnová	k1gFnSc2	Hepburnová
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
zde	zde	k6eAd1	zde
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
americká	americký	k2eAgFnSc1d1	americká
herečka	herečka	k1gFnSc1	herečka
Jennifer	Jennifra	k1gFnPc2	Jennifra
Love	lov	k1gInSc5	lov
Hewittová	Hewittový	k2eAgFnSc1d1	Hewittový
<g/>
.	.	kIx.	.
</s>
<s>
Snímek	snímek	k1gInSc1	snímek
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
přibližně	přibližně	k6eAd1	přibližně
první	první	k4xOgFnSc4	první
polovinu	polovina	k1gFnSc4	polovina
jejího	její	k3xOp3gInSc2	její
života	život	k1gInSc2	život
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
zhruba	zhruba	k6eAd1	zhruba
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
skončilo	skončit	k5eAaPmAgNnS	skončit
natáčení	natáčení	k1gNnSc1	natáčení
filmu	film	k1gInSc2	film
Snídaně	snídaně	k1gFnSc2	snídaně
u	u	k7c2	u
Tiffanyho	Tiffany	k1gMnSc2	Tiffany
a	a	k8xC	a
již	již	k9	již
probíhaly	probíhat	k5eAaImAgInP	probíhat
přípravy	příprava	k1gFnSc2	příprava
na	na	k7c6	na
natáčení	natáčení	k1gNnSc6	natáčení
filmu	film	k1gInSc2	film
My	my	k3xPp1nPc1	my
Fair	fair	k2eAgFnSc4d1	fair
Lady	lady	k1gFnSc1	lady
<g/>
.	.	kIx.	.
</s>
<s>
Barry	Barra	k1gFnPc1	Barra
Paris	Paris	k1gMnSc1	Paris
<g/>
,	,	kIx,	,
Audrey	Audrea	k1gMnSc2	Audrea
Hepburnová	Hepburnová	k1gFnSc1	Hepburnová
<g/>
:	:	kIx,	:
životopis	životopis	k1gInSc1	životopis
<g/>
,	,	kIx,	,
BB	BB	kA	BB
<g/>
/	/	kIx~	/
<g/>
art	art	k?	art
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7341-684-0	[number]	k4	80-7341-684-0
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Audrey	Audrea	k1gFnSc2	Audrea
Hepburnová	Hepburnová	k1gFnSc1	Hepburnová
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Audrey	Audrea	k1gFnSc2	Audrea
Hepburnová	Hepburnový	k2eAgFnSc1d1	Hepburnová
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
http://www.audrey1.com/	[url]	k4	http://www.audrey1.com/
http://www.Audreyhepburn.com/	[url]	k?	http://www.Audreyhepburn.com/
Audrey	Audrea	k1gFnSc2	Audrea
Hepburnová	Hepburnová	k1gFnSc1	Hepburnová
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
Audrey	Audrea	k1gFnSc2	Audrea
Hepburnová	Hepburnový	k2eAgFnSc1d1	Hepburnová
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
