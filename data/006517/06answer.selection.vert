<s>
Audrey	Audrea	k1gFnPc1	Audrea
Hepburnová	Hepburnová	k1gFnSc1	Hepburnová
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Audrey	Audrea	k1gFnSc2	Audrea
Kathleen	Kathleen	k2eAgInSc4d1	Kathleen
van	van	k1gInSc4	van
Heemstra	Heemstra	k1gFnSc1	Heemstra
Hepburn-Ruston	Hepburn-Ruston	k1gInSc1	Hepburn-Ruston
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
Audrey	Audre	k2eAgInPc1d1	Audre
Ruston	Ruston	k1gInSc1	Ruston
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1929	[number]	k4	1929
Brusel	Brusel	k1gInSc1	Brusel
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc1	Belgie
–	–	k?	–
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1993	[number]	k4	1993
Tolochenaz	Tolochenaz	k1gInSc4	Tolochenaz
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc4	Švýcarsko
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
britská	britský	k2eAgFnSc1d1	britská
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
velvyslankyně	velvyslankyně	k1gFnSc1	velvyslankyně
UNICEF	UNICEF	kA	UNICEF
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
tváří	tvář	k1gFnPc2	tvář
filmového	filmový	k2eAgNnSc2d1	filmové
plátna	plátno	k1gNnSc2	plátno
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
nositelka	nositelka	k1gFnSc1	nositelka
cen	cena	k1gFnPc2	cena
Oscar	Oscara	k1gFnPc2	Oscara
<g/>
,	,	kIx,	,
Tony	Tony	k1gFnPc2	Tony
<g/>
,	,	kIx,	,
Emmy	Emma	k1gFnSc2	Emma
i	i	k8xC	i
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
.	.	kIx.	.
</s>
