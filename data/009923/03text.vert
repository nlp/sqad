<p>
<s>
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1741	[number]	k4	1741
Vídeň	Vídeň	k1gFnSc1	Vídeň
–	–	k?	–
20	[number]	k4	20
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1790	[number]	k4	1790
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1765	[number]	k4	1765
až	až	k9	až
1790	[number]	k4	1790
císař	císař	k1gMnSc1	císař
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1780	[number]	k4	1780
až	až	k9	až
1790	[number]	k4	1790
král	král	k1gMnSc1	král
uherský	uherský	k2eAgMnSc1d1	uherský
a	a	k8xC	a
(	(	kIx(	(
<g/>
nekorunovaný	korunovaný	k2eNgMnSc1d1	nekorunovaný
<g/>
)	)	kIx)	)
král	král	k1gMnSc1	král
český	český	k2eAgMnSc1d1	český
<g/>
,	,	kIx,	,
markrabě	markrabě	k1gMnSc1	markrabě
moravský	moravský	k2eAgMnSc1d1	moravský
a	a	k8xC	a
arcivévoda	arcivévoda	k1gMnSc1	arcivévoda
rakouský	rakouský	k2eAgMnSc1d1	rakouský
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
panovnických	panovnický	k2eAgMnPc2d1	panovnický
představitelů	představitel	k1gMnPc2	představitel
evropského	evropský	k2eAgNnSc2d1	Evropské
osvícenství	osvícenství	k1gNnSc2	osvícenství
<g/>
,	,	kIx,	,
prosadil	prosadit	k5eAaPmAgInS	prosadit
řadu	řada	k1gFnSc4	řada
osvícenských	osvícenský	k2eAgFnPc2d1	osvícenská
reforem	reforma	k1gFnPc2	reforma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
těšil	těšit	k5eAaImAgMnS	těšit
obzvláštní	obzvláštní	k2eAgFnSc3d1	obzvláštní
oblibě	obliba	k1gFnSc3	obliba
<g/>
,	,	kIx,	,
např.	např.	kA	např.
častý	častý	k2eAgInSc1d1	častý
výskyt	výskyt	k1gInSc1	výskyt
jména	jméno	k1gNnSc2	jméno
Josef	Josefa	k1gFnPc2	Josefa
je	být	k5eAaImIp3nS	být
odrazem	odraz	k1gInSc7	odraz
jeho	jeho	k3xOp3gFnSc2	jeho
obliby	obliba	k1gFnSc2	obliba
mezi	mezi	k7c7	mezi
lidem	lido	k1gNnSc7	lido
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
postavení	postavení	k1gNnSc4	postavení
podstatně	podstatně	k6eAd1	podstatně
zlepšil	zlepšit	k5eAaPmAgMnS	zlepšit
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
jeho	jeho	k3xOp3gFnSc3	jeho
oblibě	obliba	k1gFnSc3	obliba
u	u	k7c2	u
sedláků	sedlák	k1gMnPc2	sedlák
mu	on	k3xPp3gMnSc3	on
začal	začít	k5eAaPmAgInS	začít
prostý	prostý	k2eAgInSc1d1	prostý
lid	lid	k1gInSc1	lid
přezdívat	přezdívat	k5eAaImF	přezdívat
"	"	kIx"	"
<g/>
selský	selský	k2eAgMnSc1d1	selský
panovník	panovník	k1gMnSc1	panovník
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
sochy	socha	k1gFnPc1	socha
byly	být	k5eAaImAgFnP	být
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2	Rakouska-Uhersk
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
z	z	k7c2	z
českých	český	k2eAgFnPc2d1	Česká
ulic	ulice	k1gFnPc2	ulice
jako	jako	k8xC	jako
symbol	symbol	k1gInSc1	symbol
habsburského	habsburský	k2eAgNnSc2d1	habsburské
mocnářství	mocnářství	k1gNnSc2	mocnářství
a	a	k8xC	a
germanizace	germanizace	k1gFnPc1	germanizace
odstraněny	odstranit	k5eAaPmNgFnP	odstranit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
alespoň	alespoň	k9	alespoň
přemístěny	přemístěn	k2eAgInPc1d1	přemístěn
do	do	k7c2	do
ústraní	ústraní	k1gNnSc2	ústraní
(	(	kIx(	(
<g/>
např.	např.	kA	např.
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
nebo	nebo	k8xC	nebo
roku	rok	k1gInSc6	rok
1920	[number]	k4	1920
v	v	k7c6	v
Teplicích	Teplice	k1gFnPc6	Teplice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
socha	socha	k1gFnSc1	socha
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
na	na	k7c6	na
nádvoří	nádvoří	k1gNnSc6	nádvoří
Všeobecné	všeobecný	k2eAgFnSc2d1	všeobecná
fakultní	fakultní	k2eAgFnSc2d1	fakultní
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
sochy	socha	k1gFnPc1	socha
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
opět	opět	k6eAd1	opět
vidět	vidět	k5eAaImF	vidět
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
městech	město	k1gNnPc6	město
a	a	k8xC	a
obcích	obec	k1gFnPc6	obec
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
Trutnově	Trutnov	k1gInSc6	Trutnov
<g/>
,	,	kIx,	,
Kadani	Kadaň	k1gFnSc6	Kadaň
<g/>
,	,	kIx,	,
Uničově	Uničův	k2eAgFnSc6d1	Uničova
<g/>
,	,	kIx,	,
Kunraticích	Kunratice	k1gFnPc6	Kunratice
u	u	k7c2	u
Cvikova	Cvikov	k1gInSc2	Cvikov
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
</s>
<s>
Socha	Socha	k1gMnSc1	Socha
ze	z	k7c2	z
zaniklého	zaniklý	k2eAgInSc2d1	zaniklý
pomníku	pomník	k1gInSc2	pomník
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
psychiatrické	psychiatrický	k2eAgFnSc2d1	psychiatrická
nemocnice	nemocnice	k1gFnSc2	nemocnice
v	v	k7c6	v
Brně-Černovicích	Brně-Černovice	k1gFnPc6	Brně-Černovice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rodina	rodina	k1gFnSc1	rodina
==	==	k?	==
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
jako	jako	k9	jako
arcivévoda	arcivévoda	k1gMnSc1	arcivévoda
Josef	Josef	k1gMnSc1	Josef
Habsbursko-Lotrinský	habsburskootrinský	k2eAgMnSc1d1	habsbursko-lotrinský
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgMnSc1d3	nejstarší
syn	syn	k1gMnSc1	syn
rakouské	rakouský	k2eAgFnSc2d1	rakouská
<g/>
,	,	kIx,	,
české	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
uherské	uherský	k2eAgFnSc2d1	uherská
panovnice	panovnice	k1gFnSc2	panovnice
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
a	a	k8xC	a
císaře	císař	k1gMnSc2	císař
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
národa	národ	k1gInSc2	národ
německého	německý	k2eAgMnSc2d1	německý
Františka	František	k1gMnSc2	František
I.	I.	kA	I.
a	a	k8xC	a
ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
byl	být	k5eAaImAgMnS	být
pokřtěn	pokřtěn	k2eAgMnSc1d1	pokřtěn
jako	jako	k8xC	jako
Josephus	Josephus	k1gMnSc1	Josephus
Benedictus	Benedictus	k1gMnSc1	Benedictus
Joannes	Joannes	k1gMnSc1	Joannes
Antonius	Antonius	k1gMnSc1	Antonius
Michael	Michael	k1gMnSc1	Michael
Adamus	Adamus	k1gMnSc1	Adamus
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gMnPc4	jeho
početné	početný	k2eAgMnPc4d1	početný
sourozence	sourozenec	k1gMnPc4	sourozenec
patřili	patřit	k5eAaImAgMnP	patřit
např.	např.	kA	např.
Leopold	Leopolda	k1gFnPc2	Leopolda
II	II	kA	II
<g/>
.	.	kIx.	.
či	či	k8xC	či
Marie	Marie	k1gFnSc1	Marie
Antonie	Antonie	k1gFnSc2	Antonie
provdaná	provdaný	k2eAgFnSc1d1	provdaná
za	za	k7c2	za
francouzského	francouzský	k2eAgMnSc2d1	francouzský
dauphina	dauphin	k1gMnSc2	dauphin
<g/>
,	,	kIx,	,
budoucího	budoucí	k2eAgMnSc4d1	budoucí
krále	král	k1gMnSc4	král
Ludvíka	Ludvík	k1gMnSc4	Ludvík
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
známější	známý	k2eAgFnSc1d2	známější
spíše	spíše	k9	spíše
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Marie	Maria	k1gFnSc2	Maria
Antoinetta	Antoinetta	k1gFnSc1	Antoinetta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
panovníkem	panovník	k1gMnSc7	panovník
pocházejícím	pocházející	k2eAgInSc6d1	pocházející
z	z	k7c2	z
Habsbursko-Lotrinské	habsburskootrinský	k2eAgFnSc2d1	habsbursko-lotrinská
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
dvakrát	dvakrát	k6eAd1	dvakrát
ženatý	ženatý	k2eAgMnSc1d1	ženatý
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
s	s	k7c7	s
Isabelou	Isabela	k1gFnSc7	Isabela
Parmskou	parmský	k2eAgFnSc7d1	parmská
(	(	kIx(	(
<g/>
1760	[number]	k4	1760
až	až	k9	až
1763	[number]	k4	1763
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
hluboce	hluboko	k6eAd1	hluboko
miloval	milovat	k5eAaImAgMnS	milovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
která	který	k3yIgFnSc1	který
jeho	jeho	k3xOp3gFnSc1	jeho
city	city	k1gFnSc1	city
neopětovala	opětovat	k5eNaImAgFnS	opětovat
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Isabelou	Isabela	k1gFnSc7	Isabela
měl	mít	k5eAaImAgMnS	mít
dvě	dva	k4xCgFnPc4	dva
dcery	dcera	k1gFnPc4	dcera
<g/>
:	:	kIx,	:
Marii	Maria	k1gFnSc4	Maria
Terezii	Terezie	k1gFnSc4	Terezie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
sedmi	sedm	k4xCc6	sedm
letech	léto	k1gNnPc6	léto
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1770	[number]	k4	1770
<g/>
)	)	kIx)	)
zemřela	zemřít	k5eAaPmAgFnS	zemřít
na	na	k7c4	na
zápal	zápal	k1gInSc4	zápal
plic	plíce	k1gFnPc2	plíce
<g/>
,	,	kIx,	,
a	a	k8xC	a
Kristýnu	Kristýna	k1gFnSc4	Kristýna
(	(	kIx(	(
<g/>
která	který	k3yRgFnSc1	který
zemřela	zemřít	k5eAaPmAgFnS	zemřít
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
předčasné	předčasný	k2eAgFnSc6d1	předčasná
smrti	smrt	k1gFnSc6	smrt
své	svůj	k3xOyFgFnSc2	svůj
první	první	k4xOgFnSc2	první
manželky	manželka	k1gFnSc2	manželka
již	již	k9	již
nebyl	být	k5eNaImAgMnS	být
schopen	schopen	k2eAgMnSc1d1	schopen
navázat	navázat	k5eAaPmF	navázat
žádný	žádný	k3yNgInSc4	žádný
bližší	blízký	k2eAgInSc4d2	bližší
citový	citový	k2eAgInSc4d1	citový
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
jiné	jiný	k2eAgFnSc3d1	jiná
ženě	žena	k1gFnSc3	žena
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ani	ani	k8xC	ani
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
druhé	druhý	k4xOgFnSc3	druhý
choti	choť	k1gFnSc3	choť
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gNnSc1	jeho
sestřenice	sestřenice	k1gFnSc1	sestřenice
druhého	druhý	k4xOgInSc2	druhý
stupně	stupeň	k1gInSc2	stupeň
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
císaře	císař	k1gMnSc2	císař
Karla	Karel	k1gMnSc2	Karel
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Bavorského	bavorský	k2eAgMnSc2d1	bavorský
a	a	k8xC	a
Marie	Maria	k1gFnPc4	Maria
Amálie	Amálie	k1gFnSc2	Amálie
Habsburské	habsburský	k2eAgFnSc2d1	habsburská
Marie	Maria	k1gFnSc2	Maria
Josefa	Josefa	k1gFnSc1	Josefa
Bavorská	bavorský	k2eAgFnSc1d1	bavorská
(	(	kIx(	(
<g/>
1764	[number]	k4	1764
<g/>
–	–	k?	–
<g/>
1767	[number]	k4	1767
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
druhému	druhý	k4xOgInSc3	druhý
sňatku	sňatek	k1gInSc3	sňatek
donutila	donutit	k5eAaPmAgFnS	donutit
Josefa	Josefa	k1gFnSc1	Josefa
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
císařovna	císařovna	k1gFnSc1	císařovna
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc1	Terezie
<g/>
,	,	kIx,	,
manželství	manželství	k1gNnSc1	manželství
však	však	k9	však
patrně	patrně	k6eAd1	patrně
nebylo	být	k5eNaImAgNnS	být
ani	ani	k9	ani
konzumováno	konzumován	k2eAgNnSc1d1	konzumováno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vláda	vláda	k1gFnSc1	vláda
==	==	k?	==
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
mládí	mládí	k1gNnSc2	mládí
se	se	k3xPyFc4	se
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
zajímal	zajímat	k5eAaImAgInS	zajímat
o	o	k7c4	o
správní	správní	k2eAgFnPc4d1	správní
otázky	otázka	k1gFnPc4	otázka
matčiny	matčin	k2eAgFnSc2d1	matčina
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Inkognito	inkognito	k1gNnSc1	inkognito
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
hrabě	hrabě	k1gMnSc1	hrabě
von	von	k1gInSc4	von
Falkenstein	Falkenstein	k2eAgInSc4d1	Falkenstein
<g/>
)	)	kIx)	)
podnikl	podniknout	k5eAaPmAgMnS	podniknout
řadu	řada	k1gFnSc4	řada
cest	cesta	k1gFnPc2	cesta
po	po	k7c6	po
korunních	korunní	k2eAgFnPc6d1	korunní
državách	država	k1gFnPc6	država
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1764	[number]	k4	1764
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
Josefova	Josefův	k2eAgFnSc1d1	Josefova
korunovace	korunovace	k1gFnSc1	korunovace
na	na	k7c4	na
římského	římský	k2eAgMnSc4d1	římský
krále	král	k1gMnSc4	král
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
Františka	František	k1gMnSc2	František
I.	I.	kA	I.
Štěpána	Štěpán	k1gMnSc2	Štěpán
Lotrinského	lotrinský	k2eAgMnSc2d1	lotrinský
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1765	[number]	k4	1765
stává	stávat	k5eAaImIp3nS	stávat
císařem	císař	k1gMnSc7	císař
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
a	a	k8xC	a
vládne	vládnout	k5eAaImIp3nS	vládnout
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
dvorního	dvorní	k2eAgMnSc4d1	dvorní
skladatele	skladatel	k1gMnSc4	skladatel
si	se	k3xPyFc3	se
zvolil	zvolit	k5eAaPmAgMnS	zvolit
Antonia	Antonio	k1gMnSc4	Antonio
Salieriho	Salieri	k1gMnSc4	Salieri
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
zmínění	zmínění	k1gNnSc4	zmínění
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gMnPc4	jeho
hudební	hudební	k2eAgMnPc4d1	hudební
oblíbence	oblíbenec	k1gMnPc4	oblíbenec
patřil	patřit	k5eAaImAgMnS	patřit
i	i	k8xC	i
Mozart	Mozart	k1gMnSc1	Mozart
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Faktickou	faktický	k2eAgFnSc4d1	faktická
moc	moc	k1gFnSc4	moc
však	však	k9	však
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
stále	stále	k6eAd1	stále
třímala	třímat	k5eAaImAgFnS	třímat
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
matkou	matka	k1gFnSc7	matka
často	často	k6eAd1	často
dostával	dostávat	k5eAaImAgMnS	dostávat
do	do	k7c2	do
ostrých	ostrý	k2eAgInPc2d1	ostrý
konfliktů	konflikt	k1gInPc2	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
přistupoval	přistupovat	k5eAaImAgMnS	přistupovat
k	k	k7c3	k
reformě	reforma	k1gFnSc3	reforma
říše	říš	k1gFnSc2	říš
jako	jako	k8xC	jako
osvícenský	osvícenský	k2eAgMnSc1d1	osvícenský
panovník	panovník	k1gMnSc1	panovník
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc1	Terezie
tento	tento	k3xDgInSc4	tento
myšlenkový	myšlenkový	k2eAgInSc4d1	myšlenkový
proud	proud	k1gInSc4	proud
nikdy	nikdy	k6eAd1	nikdy
plně	plně	k6eAd1	plně
nepřijala	přijmout	k5eNaPmAgFnS	přijmout
a	a	k8xC	a
dala	dát	k5eAaPmAgFnS	dát
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
vést	vést	k5eAaImF	vést
svou	svůj	k3xOyFgFnSc7	svůj
hlubokou	hluboký	k2eAgFnSc7d1	hluboká
katolickou	katolický	k2eAgFnSc7d1	katolická
zbožností	zbožnost	k1gFnSc7	zbožnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1766	[number]	k4	1766
císař	císař	k1gMnSc1	císař
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
otevřel	otevřít	k5eAaPmAgMnS	otevřít
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
bývalý	bývalý	k2eAgInSc1d1	bývalý
lovecký	lovecký	k2eAgInSc1d1	lovecký
revír	revír	k1gInSc1	revír
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tohoto	tento	k3xDgNnSc2	tento
jeho	on	k3xPp3gNnSc2	on
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
směl	smět	k5eAaImAgInS	smět
být	být	k5eAaImF	být
Prátr	Prátr	k1gInSc1	Prátr
využíván	využívat	k5eAaImNgInS	využívat
ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
ročních	roční	k2eAgNnPc6d1	roční
obdobích	období	k1gNnPc6	období
bez	bez	k7c2	bez
rozdílu	rozdíl	k1gInSc2	rozdíl
pro	pro	k7c4	pro
každého	každý	k3xTgMnSc4	každý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
smrti	smrt	k1gFnSc2	smrt
své	svůj	k3xOyFgFnSc2	svůj
matky	matka	k1gFnSc2	matka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1780	[number]	k4	1780
vládl	vládnout	k5eAaImAgMnS	vládnout
sám	sám	k3xTgMnSc1	sám
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
i	i	k9	i
králem	král	k1gMnSc7	král
českým	český	k2eAgMnSc7d1	český
a	a	k8xC	a
uherským	uherský	k2eAgMnSc7d1	uherský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
také	také	k9	také
prosadil	prosadit	k5eAaPmAgMnS	prosadit
své	svůj	k3xOyFgFnPc4	svůj
hlavní	hlavní	k2eAgFnPc4d1	hlavní
reformy	reforma	k1gFnPc4	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
vydal	vydat	k5eAaPmAgInS	vydat
přes	přes	k7c4	přes
6000	[number]	k4	6000
výnosů	výnos	k1gInPc2	výnos
(	(	kIx(	(
<g/>
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
téměř	téměř	k6eAd1	téměř
2	[number]	k4	2
denně	denně	k6eAd1	denně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejdůležitější	důležitý	k2eAgInPc4d3	nejdůležitější
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
zrušení	zrušení	k1gNnSc4	zrušení
cenzury	cenzura	k1gFnSc2	cenzura
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
ji	on	k3xPp3gFnSc4	on
znovu	znovu	k6eAd1	znovu
obnovil	obnovit	k5eAaPmAgInS	obnovit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podstatně	podstatně	k6eAd1	podstatně
omezenou	omezený	k2eAgFnSc4d1	omezená
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
toleranční	toleranční	k2eAgInSc4d1	toleranční
patent	patent	k1gInSc4	patent
a	a	k8xC	a
zrušení	zrušení	k1gNnSc4	zrušení
nevolnictví	nevolnictví	k1gNnSc2	nevolnictví
(	(	kIx(	(
<g/>
všechny	všechen	k3xTgFnPc4	všechen
tři	tři	k4xCgFnPc4	tři
vydány	vydán	k2eAgFnPc4d1	vydána
roku	rok	k1gInSc2	rok
1781	[number]	k4	1781
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byly	být	k5eAaImAgInP	být
ale	ale	k9	ale
i	i	k9	i
jiné	jiný	k2eAgFnPc4d1	jiná
změny	změna	k1gFnPc4	změna
<g/>
:	:	kIx,	:
zlepšení	zlepšení	k1gNnSc4	zlepšení
postavení	postavení	k1gNnSc2	postavení
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
srovnání	srovnání	k1gNnSc1	srovnání
daní	daň	k1gFnPc2	daň
a	a	k8xC	a
možností	možnost	k1gFnSc7	možnost
provozovat	provozovat	k5eAaImF	provozovat
řemesla	řemeslo	k1gNnPc4	řemeslo
<g/>
,	,	kIx,	,
změna	změna	k1gFnSc1	změna
doplňování	doplňování	k1gNnSc2	doplňování
vojska	vojsko	k1gNnSc2	vojsko
<g/>
,	,	kIx,	,
zřízení	zřízení	k1gNnSc1	zřízení
katastru	katastr	k1gInSc2	katastr
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
domů	dům	k1gInPc2	dům
(	(	kIx(	(
<g/>
josefínský	josefínský	k2eAgInSc1d1	josefínský
katastr	katastr	k1gInSc1	katastr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
reforma	reforma	k1gFnSc1	reforma
trestního	trestní	k2eAgNnSc2d1	trestní
práva	právo	k1gNnSc2	právo
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
zrušen	zrušit	k5eAaPmNgInS	zrušit
trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zavedení	zavedení	k1gNnSc1	zavedení
nových	nový	k2eAgInPc2d1	nový
úřadů	úřad	k1gInPc2	úřad
a	a	k8xC	a
úředních	úřední	k2eAgFnPc2d1	úřední
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
tajná	tajný	k2eAgFnSc1d1	tajná
policie	policie	k1gFnSc1	policie
<g/>
,	,	kIx,	,
zřízení	zřízení	k1gNnSc1	zřízení
sítě	síť	k1gFnSc2	síť
nemocnic	nemocnice	k1gFnPc2	nemocnice
<g/>
,	,	kIx,	,
porodnic	porodnice	k1gFnPc2	porodnice
<g/>
,	,	kIx,	,
nalezinců	nalezinec	k1gInPc2	nalezinec
<g/>
,	,	kIx,	,
blázinec	blázinec	k1gInSc1	blázinec
<g/>
,	,	kIx,	,
reformy	reforma	k1gFnPc1	reforma
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
reformy	reforma	k1gFnSc2	reforma
ovlivňující	ovlivňující	k2eAgFnSc4d1	ovlivňující
katolickou	katolický	k2eAgFnSc4d1	katolická
církev	církev	k1gFnSc4	církev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Řada	řada	k1gFnSc1	řada
reforem	reforma	k1gFnPc2	reforma
byla	být	k5eAaImAgFnS	být
namířena	namířit	k5eAaPmNgFnS	namířit
proti	proti	k7c3	proti
církvi	církev	k1gFnSc3	církev
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
zrušení	zrušení	k1gNnSc1	zrušení
klášterů	klášter	k1gInPc2	klášter
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
nezabývaly	zabývat	k5eNaImAgInP	zabývat
vzděláváním	vzdělávání	k1gNnSc7	vzdělávání
<g/>
,	,	kIx,	,
zdravotnictvím	zdravotnictví	k1gNnSc7	zdravotnictví
nebo	nebo	k8xC	nebo
vědou	věda	k1gFnSc7	věda
–	–	k?	–
téměř	téměř	k6eAd1	téměř
poloviny	polovina	k1gFnSc2	polovina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
však	však	k9	však
katolické	katolický	k2eAgFnSc6d1	katolická
církvi	církev	k1gFnSc6	církev
mohly	moct	k5eAaImAgInP	moct
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
i	i	k9	i
prospět	prospět	k5eAaPmF	prospět
(	(	kIx(	(
<g/>
možnost	možnost	k1gFnSc4	možnost
odchodu	odchod	k1gInSc2	odchod
do	do	k7c2	do
důchodu	důchod	k1gInSc2	důchod
pro	pro	k7c4	pro
staré	starý	k2eAgMnPc4d1	starý
kněze	kněz	k1gMnPc4	kněz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
rovněž	rovněž	k9	rovněž
převzal	převzít	k5eAaPmAgInS	převzít
výuku	výuka	k1gFnSc4	výuka
kněží	kněz	k1gMnPc2	kněz
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
reforem	reforma	k1gFnPc2	reforma
se	se	k3xPyFc4	se
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
jako	jako	k8xC	jako
nepřínosných	přínosný	k2eNgFnPc2d1	nepřínosná
nebo	nebo	k8xC	nebo
nepřijatelných	přijatelný	k2eNgFnPc2d1	nepřijatelná
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
byly	být	k5eAaImAgFnP	být
zrušeny	zrušit	k5eAaPmNgFnP	zrušit
už	už	k6eAd1	už
za	za	k7c2	za
Josefova	Josefův	k2eAgInSc2d1	Josefův
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
zákaz	zákaz	k1gInSc1	zákaz
pohřbívání	pohřbívání	k1gNnSc2	pohřbívání
v	v	k7c6	v
rakvích	rakev	k1gFnPc6	rakev
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
nahrazeno	nahradit	k5eAaPmNgNnS	nahradit
pohřbíváním	pohřbívání	k1gNnSc7	pohřbívání
v	v	k7c6	v
plátěných	plátěný	k2eAgInPc6d1	plátěný
pytlích	pytel	k1gInPc6	pytel
do	do	k7c2	do
hrobů	hrob	k1gInPc2	hrob
vysypaných	vysypaný	k2eAgInPc2d1	vysypaný
vápnem	vápno	k1gNnSc7	vápno
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
ušetřilo	ušetřit	k5eAaPmAgNnS	ušetřit
dřevo	dřevo	k1gNnSc1	dřevo
a	a	k8xC	a
stromy	strom	k1gInPc1	strom
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mnohé	mnohý	k2eAgFnPc1d1	mnohá
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
radikální	radikální	k2eAgFnSc1d1	radikální
reforma	reforma	k1gFnSc1	reforma
daňového	daňový	k2eAgInSc2d1	daňový
systému	systém	k1gInSc2	systém
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
pramenů	pramen	k1gInPc2	pramen
mu	on	k3xPp3gMnSc3	on
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
navíc	navíc	k6eAd1	navíc
i	i	k9	i
císařství	císařství	k1gNnPc4	císařství
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
těžké	těžký	k2eAgFnSc6d1	těžká
situaci	situace	k1gFnSc6	situace
<g/>
,	,	kIx,	,
úředníci	úředník	k1gMnPc1	úředník
podstrkovali	podstrkovat	k5eAaImAgMnP	podstrkovat
k	k	k7c3	k
podpisu	podpis	k1gInSc3	podpis
nařízení	nařízení	k1gNnSc4	nařízení
rušící	rušící	k2eAgFnSc4d1	rušící
řadu	řada	k1gFnSc4	řada
reforem	reforma	k1gFnPc2	reforma
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Josef	Josef	k1gMnSc1	Josef
už	už	k6eAd1	už
možná	možná	k9	možná
ani	ani	k9	ani
nevěděl	vědět	k5eNaImAgMnS	vědět
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
podepisuje	podepisovat	k5eAaImIp3nS	podepisovat
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Díky	díky	k7c3	díky
jeho	jeho	k3xOp3gMnSc3	jeho
rozhodnému	rozhodný	k2eAgMnSc3d1	rozhodný
a	a	k8xC	a
nekompromisnímu	kompromisní	k2eNgInSc3d1	nekompromisní
postupu	postup	k1gInSc3	postup
v	v	k7c6	v
předchozích	předchozí	k2eAgInPc6d1	předchozí
letech	léto	k1gNnPc6	léto
však	však	k8xC	však
řada	řada	k1gFnSc1	řada
reforem	reforma	k1gFnPc2	reforma
už	už	k6eAd1	už
odvolat	odvolat	k5eAaPmF	odvolat
nešla	jít	k5eNaImAgFnS	jít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1787	[number]	k4	1787
se	se	k3xPyFc4	se
Josef	Josef	k1gMnSc1	Josef
vypravil	vypravit	k5eAaPmAgMnS	vypravit
na	na	k7c4	na
návštěvu	návštěva	k1gFnSc4	návštěva
Krymu	Krym	k1gInSc2	Krym
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měl	mít	k5eAaImAgInS	mít
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ruskou	ruský	k2eAgFnSc7d1	ruská
carevnou	carevna	k1gFnSc7	carevna
Kateřinou	Kateřina	k1gFnSc7	Kateřina
Velikou	veliký	k2eAgFnSc4d1	veliká
dohodnout	dohodnout	k5eAaPmF	dohodnout
válku	válka	k1gFnSc4	válka
s	s	k7c7	s
Osmanskou	osmanský	k2eAgFnSc4d1	Osmanská
říši	říš	k1gFnSc3	říš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Válka	válka	k1gFnSc1	válka
s	s	k7c7	s
Tureckem	Turecko	k1gNnSc7	Turecko
===	===	k?	===
</s>
</p>
<p>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
vnitropolitickou	vnitropolitický	k2eAgFnSc4d1	vnitropolitická
situaci	situace	k1gFnSc4	situace
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
podstatně	podstatně	k6eAd1	podstatně
zhoršil	zhoršit	k5eAaPmAgInS	zhoršit
válkou	válka	k1gFnSc7	válka
s	s	k7c7	s
Tureckem	Turecko	k1gNnSc7	Turecko
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
situaci	situace	k1gFnSc6	situace
Rakouska	Rakousko	k1gNnSc2	Rakousko
nakonec	nakonec	k6eAd1	nakonec
zachránil	zachránit	k5eAaPmAgMnS	zachránit
generál	generál	k1gMnSc1	generál
Laudon	Laudon	k1gMnSc1	Laudon
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
roku	rok	k1gInSc2	rok
1789	[number]	k4	1789
dobyl	dobýt	k5eAaPmAgInS	dobýt
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1789	[number]	k4	1789
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
povstání	povstání	k1gNnSc1	povstání
v	v	k7c6	v
Rakouském	rakouský	k2eAgNnSc6d1	rakouské
Nizozemí	Nizozemí	k1gNnSc6	Nizozemí
a	a	k8xC	a
bouřila	bouřit	k5eAaImAgFnS	bouřit
se	se	k3xPyFc4	se
také	také	k9	také
uherská	uherský	k2eAgFnSc1d1	uherská
šlechta	šlechta	k1gFnSc1	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
zemřel	zemřít	k5eAaPmAgMnS	zemřít
(	(	kIx(	(
<g/>
částečně	částečně	k6eAd1	částečně
zřejmě	zřejmě	k6eAd1	zřejmě
i	i	k9	i
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
přepracování	přepracování	k1gNnSc2	přepracování
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1790	[number]	k4	1790
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jeho	jeho	k3xOp3gMnSc1	jeho
mladší	mladý	k2eAgMnSc1d2	mladší
bratr	bratr	k1gMnSc1	bratr
Leopold	Leopold	k1gMnSc1	Leopold
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reformy	reforma	k1gFnPc1	reforma
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Osvícenský	osvícenský	k2eAgInSc1d1	osvícenský
absolutismus	absolutismus	k1gInSc1	absolutismus
===	===	k?	===
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
předním	přední	k2eAgMnSc7d1	přední
českým	český	k2eAgMnSc7d1	český
představitelem	představitel	k1gMnSc7	představitel
osvícenského	osvícenský	k2eAgInSc2d1	osvícenský
absolutismu	absolutismus	k1gInSc2	absolutismus
a	a	k8xC	a
i	i	k9	i
v	v	k7c6	v
evropském	evropský	k2eAgNnSc6d1	Evropské
měřítku	měřítko	k1gNnSc6	měřítko
byl	být	k5eAaImAgMnS	být
panovníkem	panovník	k1gMnSc7	panovník
nevšedního	všední	k2eNgInSc2d1	nevšední
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
snaze	snaha	k1gFnSc6	snaha
o	o	k7c4	o
reformu	reforma	k1gFnSc4	reforma
říše	říš	k1gFnSc2	říš
podnikl	podniknout	k5eAaPmAgMnS	podniknout
velkou	velký	k2eAgFnSc4d1	velká
řadu	řada	k1gFnSc4	řada
kroků	krok	k1gInPc2	krok
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
prvotně	prvotně	k6eAd1	prvotně
zamýšleny	zamýšlet	k5eAaImNgInP	zamýšlet
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
účinnosti	účinnost	k1gFnSc2	účinnost
hospodářství	hospodářství	k1gNnSc2	hospodářství
a	a	k8xC	a
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
výsledku	výsledek	k1gInSc6	výsledek
přispěly	přispět	k5eAaPmAgInP	přispět
k	k	k7c3	k
nebývalému	nebývalý	k2eAgNnSc3d1	nebývalý
zkvalitnění	zkvalitnění	k1gNnSc3	zkvalitnění
života	život	k1gInSc2	život
jeho	jeho	k3xOp3gMnPc2	jeho
poddaných	poddaný	k1gMnPc2	poddaný
(	(	kIx(	(
<g/>
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
vycházel	vycházet	k5eAaImAgMnS	vycházet
z	z	k7c2	z
jednoduchého	jednoduchý	k2eAgInSc2d1	jednoduchý
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
poddaný	poddaný	k1gMnSc1	poddaný
relativně	relativně	k6eAd1	relativně
zdravý	zdravý	k2eAgMnSc1d1	zdravý
a	a	k8xC	a
spokojený	spokojený	k2eAgMnSc1d1	spokojený
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
říši	říš	k1gFnSc3	říš
prospěšnější	prospěšný	k2eAgFnSc3d2	prospěšnější
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
podnikl	podniknout	k5eAaPmAgMnS	podniknout
řadu	řada	k1gFnSc4	řada
regulací	regulace	k1gFnSc7	regulace
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
,	,	kIx,	,
hygieny	hygiena	k1gFnSc2	hygiena
a	a	k8xC	a
zdraví	zdraví	k1gNnSc2	zdraví
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zásadní	zásadní	k2eAgFnSc4d1	zásadní
změnu	změna	k1gFnSc4	změna
života	život	k1gInSc2	život
v	v	k7c6	v
habsburských	habsburský	k2eAgFnPc6d1	habsburská
državách	država	k1gFnPc6	država
však	však	k9	však
přinesly	přinést	k5eAaPmAgFnP	přinést
tzv.	tzv.	kA	tzv.
Toleranční	toleranční	k2eAgInSc1d1	toleranční
patent	patent	k1gInSc1	patent
a	a	k8xC	a
Patent	patent	k1gInSc1	patent
o	o	k7c4	o
zrušení	zrušení	k1gNnSc4	zrušení
nevolnictví	nevolnictví	k1gNnSc2	nevolnictví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Toleranční	toleranční	k2eAgInSc4d1	toleranční
patent	patent	k1gInSc4	patent
===	===	k?	===
</s>
</p>
<p>
<s>
Prvním	první	k4xOgInSc7	první
z	z	k7c2	z
velkých	velký	k2eAgInPc2d1	velký
patentů	patent	k1gInPc2	patent
byl	být	k5eAaImAgInS	být
tzv.	tzv.	kA	tzv.
toleranční	toleranční	k2eAgInSc4d1	toleranční
patent	patent	k1gInSc4	patent
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
datum	datum	k1gNnSc1	datum
jeho	jeho	k3xOp3gNnSc2	jeho
vydání	vydání	k1gNnSc2	vydání
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
13	[number]	k4	13
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1781	[number]	k4	1781
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
však	však	k9	však
šlo	jít	k5eAaImAgNnS	jít
spíš	spíš	k9	spíš
o	o	k7c4	o
sérii	série	k1gFnSc4	série
nařízení	nařízení	k1gNnSc2	nařízení
upravujících	upravující	k2eAgInPc2d1	upravující
problematiku	problematika	k1gFnSc4	problematika
tolerance	tolerance	k1gFnSc2	tolerance
různě	různě	k6eAd1	různě
pro	pro	k7c4	pro
různé	různý	k2eAgFnPc4d1	různá
země	zem	k1gFnPc4	zem
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
vydání	vydání	k1gNnSc2	vydání
patentu	patent	k1gInSc6	patent
byla	být	k5eAaImAgFnS	být
snaha	snaha	k1gFnSc1	snaha
vyjít	vyjít	k5eAaPmF	vyjít
vstříc	vstříc	k7c3	vstříc
osvícenskému	osvícenský	k2eAgInSc3d1	osvícenský
evropskému	evropský	k2eAgInSc3d1	evropský
trendu	trend	k1gInSc3	trend
a	a	k8xC	a
současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
kultivovaným	kultivovaný	k2eAgInSc7d1	kultivovaný
způsobem	způsob	k1gInSc7	způsob
vyrovnat	vyrovnat	k5eAaPmF	vyrovnat
se	s	k7c7	s
zbytky	zbytek	k1gInPc7	zbytek
zejména	zejména	k9	zejména
evangelického	evangelický	k2eAgNnSc2d1	evangelické
podzemí	podzemí	k1gNnSc2	podzemí
<g/>
.	.	kIx.	.
</s>
<s>
Toleranční	toleranční	k2eAgInSc1d1	toleranční
patent	patent	k1gInSc1	patent
tuto	tento	k3xDgFnSc4	tento
menšinu	menšina	k1gFnSc4	menšina
vyvedl	vyvést	k5eAaPmAgMnS	vyvést
z	z	k7c2	z
ilegality	ilegalita	k1gFnSc2	ilegalita
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zviditelnil	zviditelnit	k5eAaPmAgMnS	zviditelnit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
působeno	působit	k5eAaImNgNnS	působit
cíleně	cíleně	k6eAd1	cíleně
ideologicky	ideologicky	k6eAd1	ideologicky
<g/>
,	,	kIx,	,
politicky	politicky	k6eAd1	politicky
i	i	k9	i
ekonomicky	ekonomicky	k6eAd1	ekonomicky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Toleranční	toleranční	k2eAgInSc1d1	toleranční
patent	patent	k1gInSc1	patent
rušil	rušit	k5eAaImAgInS	rušit
církevní	církevní	k2eAgInSc4d1	církevní
monopol	monopol	k1gInSc4	monopol
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
v	v	k7c6	v
rakouských	rakouský	k2eAgFnPc6d1	rakouská
korunních	korunní	k2eAgFnPc6d1	korunní
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
dovoloval	dovolovat	k5eAaImAgMnS	dovolovat
vedle	vedle	k7c2	vedle
katolického	katolický	k2eAgNnSc2d1	katolické
vyznání	vyznání	k1gNnSc2	vyznání
také	také	k9	také
luteránské	luteránský	k2eAgNnSc4d1	luteránské
<g/>
,	,	kIx,	,
kalvínské	kalvínský	k2eAgNnSc4d1	kalvínské
a	a	k8xC	a
ortodoxní	ortodoxní	k2eAgNnSc4d1	ortodoxní
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Nešlo	jít	k5eNaImAgNnS	jít
o	o	k7c6	o
zrovnoprávnění	zrovnoprávnění	k1gNnSc6	zrovnoprávnění
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
"	"	kIx"	"
<g/>
toleranci	tolerance	k1gFnSc4	tolerance
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jakési	jakýsi	k3yIgNnSc4	jakýsi
trpění	trpění	k1gNnSc4	trpění
<g/>
,	,	kIx,	,
nekatolická	katolický	k2eNgNnPc1d1	nekatolické
vyznání	vyznání	k1gNnPc1	vyznání
podléhala	podléhat	k5eAaImAgNnP	podléhat
řadě	řada	k1gFnSc6	řada
znevýhodňujících	znevýhodňující	k2eAgNnPc2d1	znevýhodňující
nařízení	nařízení	k1gNnPc2	nařízení
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
zásadní	zásadní	k2eAgInSc4d1	zásadní
čin	čin	k1gInSc4	čin
v	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
svobody	svoboda	k1gFnSc2	svoboda
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podobným	podobný	k2eAgInSc7d1	podobný
způsobem	způsob	k1gInSc7	způsob
byla	být	k5eAaImAgFnS	být
víc	hodně	k6eAd2	hodně
tolerována	tolerován	k2eAgFnSc1d1	tolerována
i	i	k8xC	i
židovská	židovský	k2eAgFnSc1d1	židovská
víra	víra	k1gFnSc1	víra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1783	[number]	k4	1783
byl	být	k5eAaImAgInS	být
totiž	totiž	k9	totiž
uveden	uveden	k2eAgInSc1d1	uveden
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
patent	patent	k1gInSc4	patent
Systematica	Systematic	k1gInSc2	Systematic
Gentis	Gentis	k1gFnSc2	Gentis
Judaicae	Judaicae	k1gFnPc2	Judaicae
Regulatio	Regulatio	k1gMnSc1	Regulatio
<g/>
.	.	kIx.	.
</s>
<s>
Židé	Žid	k1gMnPc1	Žid
se	se	k3xPyFc4	se
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
mohli	moct	k5eAaImAgMnP	moct
věnovat	věnovat	k5eAaPmF	věnovat
většině	většina	k1gFnSc3	většina
běžných	běžný	k2eAgFnPc2d1	běžná
povolání	povolání	k1gNnPc2	povolání
<g/>
,	,	kIx,	,
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
univerzitách	univerzita	k1gFnPc6	univerzita
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
dokonce	dokonce	k9	dokonce
mohli	moct	k5eAaImAgMnP	moct
vlastnit	vlastnit	k5eAaImF	vlastnit
půdu	půda	k1gFnSc4	půda
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
potupné	potupný	k2eAgNnSc1d1	potupné
označování	označování	k1gNnSc1	označování
žlutým	žlutý	k2eAgInSc7d1	žlutý
kolečkem	koleček	k1gInSc7	koleček
<g/>
,	,	kIx,	,
někde	někde	k6eAd1	někde
mohli	moct	k5eAaImAgMnP	moct
být	být	k5eAaImF	být
oslovováni	oslovovat	k5eAaImNgMnP	oslovovat
"	"	kIx"	"
<g/>
pane	pan	k1gMnSc5	pan
<g/>
"	"	kIx"	"
místo	místo	k1gNnSc1	místo
"	"	kIx"	"
<g/>
žide	žid	k1gMnSc5	žid
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nadále	nadále	k6eAd1	nadále
např.	např.	kA	např.
nesměli	smět	k5eNaImAgMnP	smět
vstupovat	vstupovat	k5eAaImF	vstupovat
do	do	k7c2	do
státních	státní	k2eAgFnPc2d1	státní
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Zrušil	zrušit	k5eAaPmAgMnS	zrušit
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
židovské	židovský	k2eAgNnSc4d1	Židovské
mýto	mýto	k1gNnSc4	mýto
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Židé	Žid	k1gMnPc1	Žid
ho	on	k3xPp3gMnSc4	on
museli	muset	k5eAaImAgMnP	muset
platit	platit	k5eAaImF	platit
u	u	k7c2	u
městských	městský	k2eAgFnPc2d1	městská
bran	brána	k1gFnPc2	brána
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zakázal	zakázat	k5eAaPmAgMnS	zakázat
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
křty	křest	k1gInPc7	křest
z	z	k7c2	z
nouze	nouze	k1gFnSc2	nouze
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
židovské	židovský	k2eAgNnSc4d1	Židovské
dítě	dítě	k1gNnSc4	dítě
<g/>
,	,	kIx,	,
někým	někdo	k3yInSc7	někdo
zpravidla	zpravidla	k6eAd1	zpravidla
proti	proti	k7c3	proti
vůli	vůle	k1gFnSc3	vůle
rodičů	rodič	k1gMnPc2	rodič
pokřtěné	pokřtěný	k2eAgInPc1d1	pokřtěný
<g/>
,	,	kIx,	,
muselo	muset	k5eAaImAgNnS	muset
být	být	k5eAaImF	být
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
odebráno	odebrat	k5eAaPmNgNnS	odebrat
rodičům	rodič	k1gMnPc3	rodič
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
už	už	k6eAd1	už
se	se	k3xPyFc4	se
nemohlo	moct	k5eNaImAgNnS	moct
stát	stát	k5eAaImF	stát
židem	žid	k1gMnSc7	žid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byla	být	k5eAaImAgFnS	být
ovšem	ovšem	k9	ovšem
zároveň	zároveň	k6eAd1	zároveň
zrušena	zrušit	k5eAaPmNgFnS	zrušit
rabínská	rabínský	k2eAgFnSc1d1	rabínská
soudní	soudní	k2eAgFnSc1d1	soudní
pravomoc	pravomoc	k1gFnSc1	pravomoc
<g/>
,	,	kIx,	,
hebrejština	hebrejština	k1gFnSc1	hebrejština
a	a	k8xC	a
jidiš	jidiš	k6eAd1	jidiš
už	už	k6eAd1	už
pro	pro	k7c4	pro
Židy	Žid	k1gMnPc4	Žid
neplatily	platit	k5eNaImAgFnP	platit
jako	jako	k8xC	jako
úřední	úřední	k2eAgFnSc4d1	úřední
řeč	řeč	k1gFnSc4	řeč
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc4	každý
Žid	Žid	k1gMnSc1	Žid
musel	muset	k5eAaImAgMnS	muset
převzít	převzít	k5eAaPmF	převzít
německé	německý	k2eAgNnSc4d1	německé
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zrušení	zrušení	k1gNnSc1	zrušení
nevolnictví	nevolnictví	k1gNnSc2	nevolnictví
===	===	k?	===
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1781	[number]	k4	1781
byl	být	k5eAaImAgInS	být
vydán	vydán	k2eAgInSc1d1	vydán
tzv.	tzv.	kA	tzv.
Patent	patent	k1gInSc1	patent
o	o	k7c4	o
zrušení	zrušení	k1gNnSc4	zrušení
nevolnictví	nevolnictví	k1gNnSc2	nevolnictví
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
bylo	být	k5eAaImAgNnS	být
nevolnictví	nevolnictví	k1gNnSc1	nevolnictví
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
a	a	k8xC	a
nahrazeno	nahradit	k5eAaPmNgNnS	nahradit
mírnějším	mírný	k2eAgNnSc7d2	mírnější
poddanstvím	poddanství	k1gNnSc7	poddanství
<g/>
,	,	kIx,	,
a	a	k8xC	a
nadále	nadále	k6eAd1	nadále
byla	být	k5eAaImAgFnS	být
zachována	zachován	k2eAgFnSc1d1	zachována
robota	robota	k1gFnSc1	robota
(	(	kIx(	(
<g/>
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
byli	být	k5eAaImAgMnP	být
poddaní	poddaný	k1gMnPc1	poddaný
vyvázáni	vyvázat	k5eAaPmNgMnP	vyvázat
z	z	k7c2	z
přímé	přímý	k2eAgFnSc2d1	přímá
závislosti	závislost	k1gFnSc2	závislost
na	na	k7c6	na
vrchnosti	vrchnost	k1gFnSc6	vrchnost
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
šlechtické	šlechtický	k2eAgFnPc1d1	šlechtická
či	či	k8xC	či
církevní	církevní	k2eAgFnPc1d1	církevní
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
změna	změna	k1gFnSc1	změna
mj.	mj.	kA	mj.
znamenala	znamenat	k5eAaImAgFnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
poddaní	poddaný	k1gMnPc1	poddaný
nepotřebovali	potřebovat	k5eNaImAgMnP	potřebovat
souhlas	souhlas	k1gInSc4	souhlas
vrchnosti	vrchnost	k1gFnSc2	vrchnost
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
chtěli	chtít	k5eAaImAgMnP	chtít
odstěhovat	odstěhovat	k5eAaPmF	odstěhovat
z	z	k7c2	z
panství	panství	k1gNnSc2	panství
(	(	kIx(	(
<g/>
např.	např.	kA	např.
do	do	k7c2	do
města	město	k1gNnSc2	město
nebo	nebo	k8xC	nebo
na	na	k7c6	na
panství	panství	k1gNnSc6	panství
jiné	jiný	k2eAgFnSc2d1	jiná
vrchnosti	vrchnost	k1gFnSc2	vrchnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
když	když	k8xS	když
chtěli	chtít	k5eAaImAgMnP	chtít
uzavřít	uzavřít	k5eAaPmF	uzavřít
sňatek	sňatek	k1gInSc4	sňatek
s	s	k7c7	s
osobou	osoba	k1gFnSc7	osoba
žijící	žijící	k2eAgFnSc7d1	žijící
mimo	mimo	k7c4	mimo
panství	panství	k1gNnSc4	panství
<g/>
,	,	kIx,	,
když	když	k8xS	když
chtěli	chtít	k5eAaImAgMnP	chtít
dát	dát	k5eAaPmF	dát
své	svůj	k3xOyFgFnPc4	svůj
děti	dítě	k1gFnPc4	dítě
vyučit	vyučit	k5eAaPmF	vyučit
řemeslu	řemeslo	k1gNnSc3	řemeslo
či	či	k8xC	či
studovat	studovat	k5eAaImF	studovat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
dřív	dříve	k6eAd2	dříve
bez	bez	k7c2	bez
souhlasu	souhlas	k1gInSc2	souhlas
vrchnosti	vrchnost	k1gFnSc2	vrchnost
nesměli	smět	k5eNaImAgMnP	smět
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
poddanství	poddanství	k1gNnSc2	poddanství
bylo	být	k5eAaImAgNnS	být
rovněž	rovněž	k9	rovněž
možné	možný	k2eAgNnSc1d1	možné
se	se	k3xPyFc4	se
vykoupit	vykoupit	k5eAaPmF	vykoupit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Patent	patent	k1gInSc1	patent
umožnil	umožnit	k5eAaPmAgInS	umožnit
nárůst	nárůst	k1gInSc4	nárůst
vzdělanosti	vzdělanost	k1gFnSc2	vzdělanost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
víc	hodně	k6eAd2	hodně
lidí	člověk	k1gMnPc2	člověk
mohlo	moct	k5eAaImAgNnS	moct
svobodně	svobodně	k6eAd1	svobodně
studovat	studovat	k5eAaImF	studovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
podnikatelské	podnikatelský	k2eAgFnSc2d1	podnikatelská
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
rolníci	rolník	k1gMnPc1	rolník
(	(	kIx(	(
<g/>
sedláci	sedlák	k1gMnPc1	sedlák
<g/>
)	)	kIx)	)
mohli	moct	k5eAaImAgMnP	moct
odejít	odejít	k5eAaPmF	odejít
do	do	k7c2	do
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
zaměstnáni	zaměstnat	k5eAaPmNgMnP	zaměstnat
v	v	k7c6	v
továrnách	továrna	k1gFnPc6	továrna
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
výrazně	výrazně	k6eAd1	výrazně
podpořilo	podpořit	k5eAaPmAgNnS	podpořit
průmyslovou	průmyslový	k2eAgFnSc4d1	průmyslová
revoluci	revoluce	k1gFnSc4	revoluce
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Možnost	možnost	k1gFnSc1	možnost
stěhování	stěhování	k1gNnSc2	stěhování
do	do	k7c2	do
měst	město	k1gNnPc2	město
podpořila	podpořit	k5eAaPmAgFnS	podpořit
i	i	k9	i
český	český	k2eAgInSc4d1	český
živel	živel	k1gInSc4	živel
ve	v	k7c6	v
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rušení	rušení	k1gNnSc1	rušení
klášterů	klášter	k1gInPc2	klášter
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
Josefovy	Josefův	k2eAgInPc4d1	Josefův
kroky	krok	k1gInPc4	krok
patřilo	patřit	k5eAaImAgNnS	patřit
zrušení	zrušení	k1gNnSc4	zrušení
klášterů	klášter	k1gInPc2	klášter
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
nevěnovaly	věnovat	k5eNaImAgInP	věnovat
veřejně	veřejně	k6eAd1	veřejně
prospěšným	prospěšný	k2eAgFnPc3d1	prospěšná
činnostem	činnost	k1gFnPc3	činnost
(	(	kIx(	(
<g/>
špitály	špitál	k1gInPc1	špitál
nebo	nebo	k8xC	nebo
školy	škola	k1gFnPc1	škola
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Berní	berní	k2eAgFnSc1d1	berní
a	a	k8xC	a
urbariální	urbariální	k2eAgFnSc1d1	urbariální
reforma	reforma	k1gFnSc1	reforma
===	===	k?	===
</s>
</p>
<p>
<s>
Velké	velký	k2eAgFnPc1d1	velká
bouře	bouř	k1gFnPc1	bouř
a	a	k8xC	a
brzké	brzký	k2eAgNnSc1d1	brzké
zrušení	zrušení	k1gNnSc1	zrušení
vyvolaly	vyvolat	k5eAaPmAgFnP	vyvolat
tzv.	tzv.	kA	tzv.
Josefínský	josefínský	k2eAgInSc4d1	josefínský
katastr	katastr	k1gInSc4	katastr
a	a	k8xC	a
Berní	berní	k2eAgFnSc1d1	berní
a	a	k8xC	a
urbariální	urbariální	k2eAgFnSc1d1	urbariální
reforma	reforma	k1gFnSc1	reforma
(	(	kIx(	(
<g/>
patent	patent	k1gInSc1	patent
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Josefínský	josefínský	k2eAgInSc1d1	josefínský
katastr	katastr	k1gInSc1	katastr
byl	být	k5eAaImAgInS	být
soupis	soupis	k1gInSc4	soupis
veškeré	veškerý	k3xTgFnSc2	veškerý
půdy	půda	k1gFnSc2	půda
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
poddanské	poddanský	k2eAgFnPc4d1	poddanská
i	i	k8xC	i
panské	panský	k2eAgFnPc4d1	Panská
bez	bez	k7c2	bez
rozdílu	rozdíl	k1gInSc2	rozdíl
a	a	k8xC	a
zaváděl	zavádět	k5eAaImAgInS	zavádět
její	její	k3xOp3gNnSc4	její
rovné	rovný	k2eAgNnSc4d1	rovné
zdanění	zdanění	k1gNnSc4	zdanění
(	(	kIx(	(
<g/>
rušil	rušit	k5eAaImAgInS	rušit
tak	tak	k6eAd1	tak
dřívější	dřívější	k2eAgFnPc4d1	dřívější
mnohé	mnohý	k2eAgFnPc4d1	mnohá
daňové	daňový	k2eAgFnPc4d1	daňová
úlevy	úleva	k1gFnPc4	úleva
vrchnosti	vrchnost	k1gFnSc2	vrchnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Berní	berní	k2eAgInSc4d1	berní
a	a	k8xC	a
urbariální	urbariální	k2eAgInSc4d1	urbariální
patent	patent	k1gInSc4	patent
mj.	mj.	kA	mj.
rušil	rušit	k5eAaImAgMnS	rušit
povinnost	povinnost	k1gFnSc4	povinnost
roboty	robota	k1gFnSc2	robota
a	a	k8xC	a
nahrazoval	nahrazovat	k5eAaImAgInS	nahrazovat
ji	on	k3xPp3gFnSc4	on
peněžními	peněžní	k2eAgFnPc7d1	peněžní
dávkami	dávka	k1gFnPc7	dávka
<g/>
,	,	kIx,	,
v	v	k7c6	v
přechodném	přechodný	k2eAgNnSc6d1	přechodné
jednoročním	jednoroční	k2eAgNnSc6d1	jednoroční
období	období	k1gNnSc6	období
měli	mít	k5eAaImAgMnP	mít
poddaní	poddaný	k1gMnPc1	poddaný
robotovat	robotovat	k5eAaImF	robotovat
za	za	k7c4	za
mzdu	mzda	k1gFnSc4	mzda
<g/>
,	,	kIx,	,
daňové	daňový	k2eAgNnSc4d1	daňové
zatížení	zatížení	k1gNnSc4	zatížení
poddanských	poddanský	k2eAgFnPc2d1	poddanská
usedlostí	usedlost	k1gFnPc2	usedlost
nemělo	mít	k5eNaImAgNnS	mít
překročit	překročit	k5eAaPmF	překročit
30	[number]	k4	30
%	%	kIx~	%
výnosu	výnos	k1gInSc2	výnos
pozemku	pozemek	k1gInSc2	pozemek
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
byly	být	k5eAaImAgFnP	být
zrušeny	zrušit	k5eAaPmNgFnP	zrušit
ještě	ještě	k6eAd1	ještě
za	za	k7c2	za
Josefova	Josefův	k2eAgInSc2d1	Josefův
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Poněmčování	poněmčování	k1gNnSc2	poněmčování
===	===	k?	===
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
o	o	k7c4	o
centralizaci	centralizace	k1gFnSc4	centralizace
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
chtěl	chtít	k5eAaImAgInS	chtít
jako	jako	k9	jako
jediný	jediný	k2eAgInSc1d1	jediný
a	a	k8xC	a
jednotící	jednotící	k2eAgInSc1d1	jednotící
jazyk	jazyk	k1gInSc1	jazyk
prosadit	prosadit	k5eAaPmF	prosadit
němčinu	němčina	k1gFnSc4	němčina
oproti	oproti	k7c3	oproti
dosavadního	dosavadní	k2eAgInSc2d1	dosavadní
respektu	respekt	k1gInSc2	respekt
k	k	k7c3	k
zemským	zemský	k2eAgInPc3d1	zemský
jazykům	jazyk	k1gInPc3	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
a	a	k8xC	a
zrušením	zrušení	k1gNnSc7	zrušení
nevolnictví	nevolnictví	k1gNnSc2	nevolnictví
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
stěhování	stěhování	k1gNnSc1	stěhování
do	do	k7c2	do
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zavdal	zavdat	k5eAaPmAgInS	zavdat
nechtěně	chtěně	k6eNd1	chtěně
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
impulzů	impulz	k1gInPc2	impulz
k	k	k7c3	k
hnutí	hnutí	k1gNnSc3	hnutí
za	za	k7c4	za
obranu	obrana	k1gFnSc4	obrana
národních	národní	k2eAgInPc2d1	národní
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
znám	znát	k5eAaImIp1nS	znát
jako	jako	k9	jako
národní	národní	k2eAgNnSc4d1	národní
obrození	obrození	k1gNnSc4	obrození
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
panujících	panující	k2eAgMnPc2d1	panující
Habsburků	Habsburk	k1gMnPc2	Habsburk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
uměli	umět	k5eAaImAgMnP	umět
česky	česky	k6eAd1	česky
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
dominance	dominance	k1gFnSc2	dominance
němčiny	němčina	k1gFnSc2	němčina
také	také	k9	také
později	pozdě	k6eAd2	pozdě
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
uherské	uherský	k2eAgFnSc3d1	uherská
revoluci	revoluce	k1gFnSc3	revoluce
1848	[number]	k4	1848
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
k	k	k7c3	k
rakousko-uherskému	rakouskoherský	k2eAgNnSc3d1	rakousko-uherské
vyrovnání	vyrovnání	k1gNnSc3	vyrovnání
1867	[number]	k4	1867
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Příjmení	příjmení	k1gNnSc2	příjmení
===	===	k?	===
</s>
</p>
<p>
<s>
Císař	Císař	k1gMnSc1	Císař
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
vydal	vydat	k5eAaPmAgMnS	vydat
patent	patent	k1gInSc4	patent
<g/>
,	,	kIx,	,
kterým	který	k3yQgInPc3	který
byla	být	k5eAaImAgFnS	být
uložena	uložen	k2eAgFnSc1d1	uložena
povinnost	povinnost	k1gFnSc1	povinnost
používat	používat	k5eAaImF	používat
neměnná	neměnný	k2eAgNnPc4d1	neměnné
příjmení	příjmení	k1gNnPc4	příjmení
<g/>
.	.	kIx.	.
</s>
<s>
Příjmení	příjmení	k1gNnSc1	příjmení
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
bylo	být	k5eAaImAgNnS	být
jménem	jméno	k1gNnSc7	jméno
druhořadým	druhořadý	k2eAgNnSc7d1	druhořadé
(	(	kIx(	(
<g/>
doplňovalo	doplňovat	k5eAaImAgNnS	doplňovat
jméno	jméno	k1gNnSc1	jméno
křestní	křestní	k2eAgNnSc1d1	křestní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
mohl	moct	k5eAaImAgMnS	moct
během	během	k7c2	během
života	život	k1gInSc2	život
příjmení	příjmení	k1gNnSc2	příjmení
několikrát	několikrát	k6eAd1	několikrát
změnit	změnit	k5eAaPmF	změnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Lidové	lidový	k2eAgFnPc4d1	lidová
vrstvy	vrstva	k1gFnPc4	vrstva
seznamoval	seznamovat	k5eAaImAgMnS	seznamovat
s	s	k7c7	s
osobností	osobnost	k1gFnSc7	osobnost
a	a	k8xC	a
reformami	reforma	k1gFnPc7	reforma
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Matěj	Matěj	k1gMnSc1	Matěj
Kramerius	Kramerius	k1gMnSc1	Kramerius
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
zvaném	zvaný	k2eAgNnSc6d1	zvané
Kniha	kniha	k1gFnSc1	kniha
Josefova	Josefov	k1gInSc2	Josefov
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
sepsal	sepsat	k5eAaPmAgMnS	sepsat
podle	podle	k7c2	podle
německé	německý	k2eAgFnSc2d1	německá
předlohy	předloha	k1gFnSc2	předloha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1784	[number]	k4	1784
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
mj.	mj.	kA	mj.
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Babička	babička	k1gFnSc1	babička
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
<g/>
,	,	kIx,	,
jako	jako	k9	jako
"	"	kIx"	"
<g/>
pán	pán	k1gMnSc1	pán
s	s	k7c7	s
trubičkou	trubička	k1gFnSc7	trubička
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
dalekohledem	dalekohled	k1gInSc7	dalekohled
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
asi	asi	k9	asi
nejznámější	známý	k2eAgFnSc4d3	nejznámější
filmovou	filmový	k2eAgFnSc4d1	filmová
podobu	podoba	k1gFnSc4	podoba
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Jeffrey	Jeffrea	k1gMnSc2	Jeffrea
Jones	Jones	k1gMnSc1	Jones
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Amadeus	Amadeus	k1gMnSc1	Amadeus
Miloše	Miloš	k1gMnSc4	Miloš
Formana	Forman	k1gMnSc4	Forman
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
vyobrazen	vyobrazen	k2eAgInSc1d1	vyobrazen
jako	jako	k8xC	jako
laický	laický	k2eAgMnSc1d1	laický
milovník	milovník	k1gMnSc1	milovník
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývod	vývod	k1gInSc1	vývod
z	z	k7c2	z
předků	předek	k1gInPc2	předek
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Adamová	Adamová	k1gFnSc1	Adamová
<g/>
,	,	kIx,	,
K.	K.	kA	K.
<g/>
,	,	kIx,	,
Lojek	Lojek	k1gMnSc1	Lojek
<g/>
,	,	kIx,	,
A.	A.	kA	A.
<g/>
:	:	kIx,	:
Ohňové	ohňový	k2eAgInPc1d1	ohňový
patenty	patent	k1gInPc1	patent
(	(	kIx(	(
<g/>
protipožární	protipožární	k2eAgNnPc1d1	protipožární
opatření	opatření	k1gNnPc1	opatření
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
a	a	k8xC	a
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Bubelová	Bubelová	k1gFnSc1	Bubelová
<g/>
,	,	kIx,	,
K.	K.	kA	K.
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pocta	pocta	k1gFnSc1	pocta
Eduardu	Eduard	k1gMnSc3	Eduard
Vlčkovi	Vlček	k1gMnSc3	Vlček
k	k	k7c3	k
70	[number]	k4	70
<g/>
.	.	kIx.	.
narozeninám	narozeniny	k1gFnPc3	narozeniny
<g/>
,	,	kIx,	,
Olomouc	Olomouc	k1gFnSc1	Olomouc
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
s.	s.	k?	s.
9	[number]	k4	9
<g/>
–	–	k?	–
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
244	[number]	k4	244
<g/>
-	-	kIx~	-
<g/>
2491	[number]	k4	2491
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
BĚLINA	Bělina	k1gMnSc1	Bělina
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
;	;	kIx,	;
KAŠE	kaše	k1gFnSc1	kaše
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
KUČERA	Kučera	k1gMnSc1	Kučera
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
dějiny	dějiny	k1gFnPc1	dějiny
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
X.	X.	kA	X.
1740	[number]	k4	1740
<g/>
–	–	k?	–
<g/>
1792	[number]	k4	1792
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
768	[number]	k4	768
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
384	[number]	k4	384
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bělina	bělina	k1gFnSc1	bělina
P.	P.	kA	P.
<g/>
,	,	kIx,	,
Teoretické	teoretický	k2eAgInPc1d1	teoretický
kořeny	kořen	k1gInPc1	kořen
a	a	k8xC	a
státní	státní	k2eAgFnSc1d1	státní
praxe	praxe	k1gFnSc1	praxe
osvícenského	osvícenský	k2eAgInSc2d1	osvícenský
absolutismu	absolutismus	k1gInSc2	absolutismus
v	v	k7c6	v
habsburské	habsburský	k2eAgFnSc6d1	habsburská
monarchii	monarchie	k1gFnSc6	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Československý	československý	k2eAgInSc1d1	československý
časopis	časopis	k1gInSc1	časopis
historický	historický	k2eAgInSc1d1	historický
29	[number]	k4	29
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
s.	s.	k?	s.
879	[number]	k4	879
<g/>
–	–	k?	–
<g/>
905	[number]	k4	905
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CERMAN	Cerman	k1gMnSc1	Cerman
<g/>
,	,	kIx,	,
Ivo	Ivo	k1gMnSc1	Ivo
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
RYANTOVÁ	RYANTOVÁ	kA	RYANTOVÁ
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
<g/>
;	;	kIx,	;
VOREL	Vorel	k1gMnSc1	Vorel
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
králové	král	k1gMnPc1	král
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
940	[number]	k4	940
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
487	[number]	k4	487
<g/>
–	–	k?	–
<g/>
497	[number]	k4	497
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČORNEJOVÁ	ČORNEJOVÁ	kA	ČORNEJOVÁ
<g/>
,	,	kIx,	,
Ivana	Ivana	k1gFnSc1	Ivana
<g/>
;	;	kIx,	;
RAK	rak	k1gMnSc1	rak
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
VLNAS	VLNAS	kA	VLNAS
<g/>
,	,	kIx,	,
Vít	Vít	k1gMnSc1	Vít
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
tvých	tvůj	k3xOp2gNnPc2	tvůj
křídel	křídlo	k1gNnPc2	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Habsburkové	Habsburk	k1gMnPc1	Habsburk
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Grafoprint-Neubert	Grafoprint-Neubert	k1gMnSc1	Grafoprint-Neubert
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
289	[number]	k4	289
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85785	[number]	k4	85785
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
FRAIS	FRAIS	kA	FRAIS
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Reformy	reforma	k1gFnPc1	reforma
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
a	a	k8xC	a
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
a	a	k8xC	a
moravských	moravský	k2eAgFnPc6d1	Moravská
zemích	zem	k1gFnPc6	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
:	:	kIx,	:
Akcent	akcent	k1gInSc1	akcent
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
156	[number]	k4	156
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7268	[number]	k4	7268
<g/>
-	-	kIx~	-
<g/>
337	[number]	k4	337
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gutkas	Gutkas	k1gMnSc1	Gutkas
<g/>
,	,	kIx,	,
K.	K.	kA	K.
<g/>
,	,	kIx,	,
Kaiser	Kaiser	k1gMnSc1	Kaiser
Joseph	Joseph	k1gMnSc1	Joseph
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Eine	Eine	k1gFnSc1	Eine
Biographie	Biographie	k1gFnSc1	Biographie
<g/>
.	.	kIx.	.
</s>
<s>
Zsolnay	Zsolnay	k1gInPc1	Zsolnay
<g/>
,	,	kIx,	,
Wien	Wien	k1gNnSc1	Wien
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HAMANNOVÁ	HAMANNOVÁ	kA	HAMANNOVÁ
<g/>
,	,	kIx,	,
Brigitte	Brigitte	k1gFnSc1	Brigitte
<g/>
.	.	kIx.	.
</s>
<s>
Habsburkové	Habsburk	k1gMnPc1	Habsburk
<g/>
.	.	kIx.	.
</s>
<s>
Životopisná	životopisný	k2eAgFnSc1d1	životopisná
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Brána	brána	k1gFnSc1	brána
;	;	kIx,	;
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
408	[number]	k4	408
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85946	[number]	k4	85946
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
Magenschab	Magenschab	k1gMnSc1	Magenschab
<g/>
,	,	kIx,	,
Hans	Hans	k1gMnSc1	Hans
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Revolucionář	revolucionář	k1gMnSc1	revolucionář
z	z	k7c2	z
boží	boží	k2eAgFnSc2d1	boží
milosti	milost	k1gFnSc2	milost
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Magenschab	Magenschab	k1gMnSc1	Magenschab
<g/>
,	,	kIx,	,
Hans	Hans	k1gMnSc1	Hans
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
Rakouska	Rakousko	k1gNnSc2	Rakousko
do	do	k7c2	do
moderní	moderní	k2eAgFnSc2d1	moderní
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
Ikar	Ikar	k1gMnSc1	Ikar
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-249-1001-7	[number]	k4	978-80-249-1001-7
</s>
</p>
<p>
<s>
Melmuková	Melmukový	k2eAgFnSc1d1	Melmuková
<g/>
,	,	kIx,	,
E.	E.	kA	E.
<g/>
,	,	kIx,	,
Patent	patent	k1gInSc1	patent
zvaný	zvaný	k2eAgInSc1d1	zvaný
toleranční	toleranční	k2eAgInSc1d1	toleranční
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Reinalter	Reinalter	k1gMnSc1	Reinalter
<g/>
,	,	kIx,	,
H.	H.	kA	H.
<g/>
,	,	kIx,	,
Am	Am	k1gFnSc1	Am
Hofe	Hofe	k1gNnSc2	Hofe
Josephs	Josephsa	k1gFnPc2	Josephsa
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Leipzig	Leipzig	k1gMnSc1	Leipzig
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VONDRA	Vondra	k1gMnSc1	Vondra
<g/>
,	,	kIx,	,
Roman	Roman	k1gMnSc1	Roman
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
země	zem	k1gFnPc1	zem
v	v	k7c6	v
letech	let	k1gInPc6	let
1705	[number]	k4	1705
<g/>
–	–	k?	–
<g/>
1792	[number]	k4	1792
:	:	kIx,	:
doba	doba	k1gFnSc1	doba
absolutismu	absolutismus	k1gInSc2	absolutismus
<g/>
,	,	kIx,	,
osvícenství	osvícenství	k1gNnSc2	osvícenství
<g/>
,	,	kIx,	,
paruk	paruka	k1gFnPc2	paruka
a	a	k8xC	a
třírohých	třírohý	k2eAgInPc2d1	třírohý
klobouků	klobouk	k1gInPc2	klobouk
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
384	[number]	k4	384
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
448	[number]	k4	448
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VONDRA	Vondra	k1gMnSc1	Vondra
<g/>
,	,	kIx,	,
Roman	Roman	k1gMnSc1	Roman
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1741	[number]	k4	1741
<g/>
–	–	k?	–
<g/>
1790	[number]	k4	1790
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Historický	historický	k2eAgInSc1d1	historický
obzor	obzor	k1gInSc1	obzor
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
21	[number]	k4	21
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s.	s.	k?	s.
84	[number]	k4	84
<g/>
–	–	k?	–
<g/>
86	[number]	k4	86
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1210	[number]	k4	1210
<g/>
-	-	kIx~	-
<g/>
6097	[number]	k4	6097
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WEISSENSTEINER	WEISSENSTEINER	kA	WEISSENSTEINER
<g/>
,	,	kIx,	,
Friedrich	Friedrich	k1gMnSc1	Friedrich
<g/>
.	.	kIx.	.
</s>
<s>
Synové	syn	k1gMnPc1	syn
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ikar	Ikar	k1gMnSc1	Ikar
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
158	[number]	k4	158
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
249	[number]	k4	249
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
594	[number]	k4	594
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Winter	Winter	k1gMnSc1	Winter
<g/>
,	,	kIx,	,
E.	E.	kA	E.
<g/>
,	,	kIx,	,
Der	drát	k5eAaImRp2nS	drát
Josefinismus	josefinismus	k1gInSc4	josefinismus
<g/>
.	.	kIx.	.
</s>
<s>
Die	Die	k?	Die
Geschichte	Geschicht	k1gMnSc5	Geschicht
des	des	k1gNnSc7	des
österreichischen	österreichischen	k2eAgInSc1d1	österreichischen
Reformkatolizismus	Reformkatolizismus	k1gInSc1	Reformkatolizismus
1740	[number]	k4	1740
<g/>
–	–	k?	–
<g/>
1848	[number]	k4	1848
<g/>
.	.	kIx.	.
</s>
<s>
Berlin	berlina	k1gFnPc2	berlina
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Josefinismus	josefinismus	k1gInSc1	josefinismus
</s>
</p>
<p>
<s>
Josefinské	Josefinský	k2eAgFnPc1d1	Josefinská
reformy	reforma	k1gFnPc1	reforma
</s>
</p>
<p>
<s>
Josefinské	Josefinský	k2eAgNnSc1d1	Josefinský
vojenské	vojenský	k2eAgNnSc1d1	vojenské
mapování	mapování	k1gNnSc1	mapování
</s>
</p>
<p>
<s>
Josefínský	josefínský	k2eAgInSc4d1	josefínský
katastr	katastr	k1gInSc4	katastr
</s>
</p>
<p>
<s>
Josefov	Josefov	k1gInSc1	Josefov
</s>
</p>
<p>
<s>
Pomník	pomník	k1gInSc1	pomník
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Habsburkové	Habsburk	k1gMnPc1	Habsburk
</s>
</p>
<p>
<s>
Habsbursko-Lotrinská	habsburskootrinský	k2eAgFnSc1d1	habsbursko-lotrinská
dynastie	dynastie	k1gFnSc1	dynastie
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
panovníků	panovník	k1gMnPc2	panovník
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
hlav	hlava	k1gFnPc2	hlava
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
</s>
</p>
<p>
<s>
Mozart	Mozart	k1gMnSc1	Mozart
a	a	k8xC	a
neštovice	neštovice	k1gFnSc1	neštovice
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Josef	Josefa	k1gFnPc2	Josefa
II	II	kA	II
<g/>
.	.	kIx.	.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Josef	Josef	k1gMnSc1	Josef
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
sbírka	sbírka	k1gFnSc1	sbírka
nařízení	nařízení	k1gNnSc2	nařízení
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
Collectio	Collectio	k6eAd1	Collectio
ordinationum	ordinationum	k1gNnSc1	ordinationum
imperatoris	imperatoris	k1gFnSc2	imperatoris
Josephi	Joseph	k1gFnSc2	Joseph
II-i	II-	k1gFnSc2	II-
et	et	k?	et
repraesentationum	repraesentationum	k1gInSc1	repraesentationum
diversorum	diversorum	k1gNnSc1	diversorum
regni	regn	k1gMnPc1	regn
Hungariae	Hungaria	k1gFnSc2	Hungaria
comitatuum	comitatuum	k1gNnSc1	comitatuum
-	-	kIx~	-
dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
Digitální	digitální	k2eAgFnSc6d1	digitální
knihovně	knihovna	k1gFnSc6	knihovna
UKB	UKB	kA	UKB
</s>
</p>
<p>
<s>
Constituta	Constitut	k2eAgFnSc1d1	Constitut
regia	regia	k1gFnSc1	regia
quae	quaat	k5eAaPmIp3nS	quaat
regnante	regnant	k1gMnSc5	regnant
August	August	k1gMnSc1	August
<g/>
,	,	kIx,	,
Imperatore	Imperator	k1gMnSc5	Imperator
et	et	k?	et
rege	reg	k1gMnSc4	reg
Apostol	Apostol	k1gInSc1	Apostol
<g/>
.	.	kIx.	.
</s>
<s>
Josepho	Josepze	k6eAd1	Josepze
II	II	kA	II
<g/>
.	.	kIx.	.
politicorum	politicorum	k1gInSc1	politicorum
Pars	Pars	k1gInSc1	Pars
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
De	De	k?	De
publicorum	publicorum	k1gInSc1	publicorum
negotiorum	negotiorum	k1gInSc1	negotiorum
administratione	administration	k1gInSc5	administration
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
De	De	k?	De
Politia	Politia	k1gFnSc1	Politia
....	....	k?	....
Viennae	Viennae	k1gFnSc1	Viennae
<g/>
:	:	kIx,	:
Kurzbek	Kurzbek	k1gMnSc1	Kurzbek
<g/>
,	,	kIx,	,
1788	[number]	k4	1788
<g/>
.	.	kIx.	.
397	[number]	k4	397
s.	s.	k?	s.
-	-	kIx~	-
dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
Digitální	digitální	k2eAgFnSc6d1	digitální
knihovně	knihovna	k1gFnSc6	knihovna
UKB	UKB	kA	UKB
</s>
</p>
