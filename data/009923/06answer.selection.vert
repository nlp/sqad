<s>
Za	za	k7c4	za
nejdůležitější	důležitý	k2eAgInPc4d3	nejdůležitější
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
zrušení	zrušení	k1gNnSc4	zrušení
cenzury	cenzura	k1gFnSc2	cenzura
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
ji	on	k3xPp3gFnSc4	on
znovu	znovu	k6eAd1	znovu
obnovil	obnovit	k5eAaPmAgInS	obnovit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podstatně	podstatně	k6eAd1	podstatně
omezenou	omezený	k2eAgFnSc4d1	omezená
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
toleranční	toleranční	k2eAgInSc4d1	toleranční
patent	patent	k1gInSc4	patent
a	a	k8xC	a
zrušení	zrušení	k1gNnSc4	zrušení
nevolnictví	nevolnictví	k1gNnSc2	nevolnictví
(	(	kIx(	(
<g/>
všechny	všechen	k3xTgFnPc4	všechen
tři	tři	k4xCgFnPc4	tři
vydány	vydán	k2eAgFnPc4d1	vydána
roku	rok	k1gInSc2	rok
1781	[number]	k4	1781
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
