<s>
Pytláctví	pytláctví	k1gNnSc1	pytláctví
je	být	k5eAaImIp3nS	být
nelegální	legální	k2eNgInSc4d1	nelegální
lov	lov	k1gInSc4	lov
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
jít	jít	k5eAaImF	jít
o	o	k7c4	o
trestný	trestný	k2eAgInSc4d1	trestný
čin	čin	k1gInSc4	čin
nebo	nebo	k8xC	nebo
přestupek	přestupek	k1gInSc4	přestupek
při	při	k7c6	při
neoprávněném	oprávněný	k2eNgNnSc6d1	neoprávněné
provozování	provozování	k1gNnSc6	provozování
myslivosti	myslivost	k1gFnSc2	myslivost
nebo	nebo	k8xC	nebo
rybářství	rybářství	k1gNnSc2	rybářství
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
zisku	zisk	k1gInSc2	zisk
nebo	nebo	k8xC	nebo
ze	z	k7c2	z
vrozené	vrozený	k2eAgFnSc2d1	vrozená
náruživosti	náruživost	k1gFnSc2	náruživost
<g/>
.	.	kIx.	.
</s>
<s>
Nezákonnost	nezákonnost	k1gFnSc1	nezákonnost
tohoto	tento	k3xDgInSc2	tento
činu	čin	k1gInSc2	čin
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
doba	doba	k1gFnSc1	doba
lovu	lov	k1gInSc2	lov
pro	pro	k7c4	pro
danou	daný	k2eAgFnSc4d1	daná
zvěř	zvěř	k1gFnSc4	zvěř
<g/>
,	,	kIx,	,
lovec	lovec	k1gMnSc1	lovec
nebo	nebo	k8xC	nebo
rybář	rybář	k1gMnSc1	rybář
nevlastní	vlastnit	k5eNaImIp3nS	vlastnit
platnou	platný	k2eAgFnSc4d1	platná
licenci	licence	k1gFnSc4	licence
<g/>
,	,	kIx,	,
zvěř	zvěř	k1gFnSc1	zvěř
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
nehonební	honební	k2eNgFnSc6d1	nehonební
půdě	půda	k1gFnSc6	půda
<g/>
,	,	kIx,	,
právo	právo	k1gNnSc4	právo
lovit	lovit	k5eAaImF	lovit
tuto	tento	k3xDgFnSc4	tento
zvěř	zvěř	k1gFnSc4	zvěř
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
někdo	někdo	k3yInSc1	někdo
jiný	jiný	k2eAgMnSc1d1	jiný
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
nelegální	legální	k2eNgInSc1d1	nelegální
způsob	způsob	k1gInSc1	způsob
lovu	lov	k1gInSc2	lov
(	(	kIx(	(
<g/>
např.	např.	kA	např.
přisvětlení	přisvětlení	k1gNnSc1	přisvětlení
<g/>
,	,	kIx,	,
použití	použití	k1gNnSc1	použití
kuše	kuše	k1gFnSc2	kuše
nebo	nebo	k8xC	nebo
luku	luk	k1gInSc2	luk
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zvěř	zvěř	k1gFnSc1	zvěř
nebo	nebo	k8xC	nebo
ryby	ryba	k1gFnPc1	ryba
jsou	být	k5eAaImIp3nP	být
chráněny	chránit	k5eAaImNgFnP	chránit
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chudých	chudý	k2eAgInPc6d1	chudý
regionech	region	k1gInPc6	region
je	být	k5eAaImIp3nS	být
dominantní	dominantní	k2eAgFnSc7d1	dominantní
motivací	motivace	k1gFnSc7	motivace
regionální	regionální	k2eAgFnSc1d1	regionální
poptávka	poptávka	k1gFnSc1	poptávka
po	po	k7c6	po
zvěřině	zvěřina	k1gFnSc6	zvěřina
nebo	nebo	k8xC	nebo
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
trofejemi	trofej	k1gFnPc7	trofej
a	a	k8xC	a
živými	živý	k2eAgNnPc7d1	živé
zvířaty	zvíře	k1gNnPc7	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Pytláctví	pytláctví	k1gNnSc1	pytláctví
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
extrémních	extrémní	k2eAgInPc6d1	extrémní
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
likvidací	likvidace	k1gFnSc7	likvidace
přirozených	přirozený	k2eAgInPc2d1	přirozený
biotopů	biotop	k1gInPc2	biotop
<g/>
,	,	kIx,	,
vest	vesta	k1gFnPc2	vesta
k	k	k7c3	k
ohrožení	ohrožení	k1gNnSc3	ohrožení
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
vyhubení	vyhubení	k1gNnSc2	vyhubení
některých	některý	k3yIgInPc2	některý
živočišných	živočišný	k2eAgInPc2d1	živočišný
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
zákazy	zákaz	k1gInPc4	zákaz
nebo	nebo	k8xC	nebo
regulaci	regulace	k1gFnSc4	regulace
takového	takový	k3xDgInSc2	takový
obchodu	obchod	k1gInSc2	obchod
vedou	vést	k5eAaImIp3nP	vést
ke	k	k7c3	k
zvyšování	zvyšování	k1gNnSc3	zvyšování
cen	cena	k1gFnPc2	cena
na	na	k7c6	na
černém	černý	k2eAgInSc6d1	černý
trhu	trh	k1gInSc6	trh
a	a	k8xC	a
často	často	k6eAd1	často
problém	problém	k1gInSc4	problém
prohlubují	prohlubovat	k5eAaImIp3nP	prohlubovat
<g/>
.	.	kIx.	.
</s>
<s>
Nejúčinnější	účinný	k2eAgFnSc7d3	nejúčinnější
prevencí	prevence	k1gFnSc7	prevence
jsou	být	k5eAaImIp3nP	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
farmové	farmový	k2eAgInPc1d1	farmový
chovy	chov	k1gInPc1	chov
cílových	cílový	k2eAgInPc2d1	cílový
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
ochrana	ochrana	k1gFnSc1	ochrana
přirozených	přirozený	k2eAgInPc2d1	přirozený
ekosystémů	ekosystém	k1gInPc2	ekosystém
a	a	k8xC	a
současně	současně	k6eAd1	současně
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
rozvoj	rozvoj	k1gInSc4	rozvoj
chudých	chudý	k2eAgFnPc2d1	chudá
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Pytláctví	pytláctví	k1gNnSc1	pytláctví
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
i	i	k8xC	i
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
poměrně	poměrně	k6eAd1	poměrně
rozšířené	rozšířený	k2eAgFnPc1d1	rozšířená
a	a	k8xC	a
i	i	k9	i
přes	přes	k7c4	přes
určité	určitý	k2eAgInPc4d1	určitý
pokroky	pokrok	k1gInPc4	pokrok
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
nedaří	dařit	k5eNaImIp3nS	dařit
vymýtit	vymýtit	k5eAaPmF	vymýtit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
podmínkách	podmínka	k1gFnPc6	podmínka
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
starou	starý	k2eAgFnSc4d1	stará
"	"	kIx"	"
<g/>
lidovou	lidový	k2eAgFnSc4d1	lidová
tradici	tradice	k1gFnSc4	tradice
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
kořeny	kořen	k1gInPc4	kořen
ekonomicko-sociální	ekonomickoociální	k2eAgInPc4d1	ekonomicko-sociální
<g/>
.	.	kIx.	.
</s>
<s>
Lov	lov	k1gInSc1	lov
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
zvěře	zvěř	k1gFnSc2	zvěř
náležel	náležet	k5eAaImAgInS	náležet
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
panovníkovi	panovník	k1gMnSc3	panovník
nebo	nebo	k8xC	nebo
šlechtě	šlechta	k1gFnSc3	šlechta
a	a	k8xC	a
prostí	prostý	k2eAgMnPc1d1	prostý
lidé	člověk	k1gMnPc1	člověk
byli	být	k5eAaImAgMnP	být
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
lovu	lov	k1gInSc2	lov
vyloučeni	vyloučit	k5eAaPmNgMnP	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
Lesy	les	k1gInPc1	les
ale	ale	k8xC	ale
byly	být	k5eAaImAgInP	být
hluboké	hluboký	k2eAgInPc1d1	hluboký
a	a	k8xC	a
zvěřina	zvěřina	k1gFnSc1	zvěřina
vždy	vždy	k6eAd1	vždy
představovala	představovat	k5eAaImAgFnS	představovat
vítané	vítaný	k2eAgNnSc4d1	vítané
zpestření	zpestření	k1gNnSc4	zpestření
monotonního	monotonní	k2eAgInSc2d1	monotonní
jídelníčku	jídelníček	k1gInSc2	jídelníček
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
z	z	k7c2	z
těch	ten	k3xDgFnPc2	ten
dob	doba	k1gFnPc2	doba
pochází	pocházet	k5eAaImIp3nS	pocházet
rčení	rčení	k1gNnSc1	rčení
"	"	kIx"	"
<g/>
Krást	krást	k5eAaImF	krást
a	a	k8xC	a
pytlačit	pytlačit	k5eAaImF	pytlačit
na	na	k7c4	na
panským	panská	k1gFnPc3	panská
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
hřích	hřích	k1gInSc4	hřích
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jediným	jediný	k2eAgInSc7d1	jediný
povoleným	povolený	k2eAgInSc7d1	povolený
způsobem	způsob	k1gInSc7	způsob
lovu	lov	k1gInSc2	lov
pro	pro	k7c4	pro
lidové	lidový	k2eAgFnPc4d1	lidová
vrstvy	vrstva	k1gFnPc4	vrstva
čižba	čižba	k1gFnSc1	čižba
-	-	kIx~	-
chytání	chytání	k1gNnSc1	chytání
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
lov	lov	k1gInSc1	lov
srstnaté	srstnatý	k2eAgFnSc2d1	srstnatá
zvěře	zvěř	k1gFnSc2	zvěř
náležel	náležet	k5eAaImAgMnS	náležet
pouze	pouze	k6eAd1	pouze
šlechtě	šlechta	k1gFnSc3	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Dopadení	dopadený	k2eAgMnPc1d1	dopadený
pytláci	pytlák	k1gMnPc1	pytlák
byli	být	k5eAaImAgMnP	být
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
a	a	k8xC	a
raném	raný	k2eAgInSc6d1	raný
novověku	novověk	k1gInSc6	novověk
přísně	přísně	k6eAd1	přísně
trestáni	trestán	k2eAgMnPc1d1	trestán
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
pranýřem	pranýř	k1gInSc7	pranýř
či	či	k8xC	či
oslí	oslí	k2eAgFnSc7d1	oslí
jízdou	jízda	k1gFnSc7	jízda
<g/>
,	,	kIx,	,
v	v	k7c6	v
těžších	těžký	k2eAgInPc6d2	těžší
případech	případ	k1gInPc6	případ
vypálením	vypálení	k1gNnSc7	vypálení
cejchu	cejch	k1gInSc2	cejch
<g/>
,	,	kIx,	,
nucenými	nucený	k2eAgFnPc7d1	nucená
pracemi	práce	k1gFnPc7	práce
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
i	i	k9	i
smrtí	smrt	k1gFnSc7	smrt
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
při	při	k7c6	při
zatýkání	zatýkání	k1gNnSc6	zatýkání
bránili	bránit	k5eAaImAgMnP	bránit
se	s	k7c7	s
zbraní	zbraň	k1gFnSc7	zbraň
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
tvrdým	tvrdý	k2eAgInSc7d1	tvrdý
postupem	postup	k1gInSc7	postup
proti	proti	k7c3	proti
pytlákům	pytlák	k1gMnPc3	pytlák
proslulo	proslout	k5eAaPmAgNnS	proslout
Lovecké	lovecký	k2eAgNnSc1d1	lovecké
právo	právo	k1gNnSc1	právo
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
Buchlově	Buchlův	k2eAgInSc6d1	Buchlův
<g/>
,	,	kIx,	,
fungující	fungující	k2eAgInSc4d1	fungující
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
dochovaných	dochovaný	k2eAgFnPc2d1	dochovaná
smolných	smolný	k2eAgFnPc2d1	smolná
knih	kniha	k1gFnPc2	kniha
zde	zde	k6eAd1	zde
byli	být	k5eAaImAgMnP	být
pytláci	pytlák	k1gMnPc1	pytlák
většinou	většinou	k6eAd1	většinou
věšeni	věsit	k5eAaImNgMnP	věsit
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
ve	v	k7c6	v
středověké	středověký	k2eAgFnSc6d1	středověká
Anglii	Anglie	k1gFnSc6	Anglie
bylo	být	k5eAaImAgNnS	být
pytláctví	pytláctví	k1gNnSc4	pytláctví
trestáno	trestán	k2eAgNnSc4d1	trestáno
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
se	se	k3xPyFc4	se
trestalo	trestat	k5eAaImAgNnS	trestat
i	i	k9	i
nezákonné	zákonný	k2eNgNnSc1d1	nezákonné
chytání	chytání	k1gNnSc1	chytání
perlorodek	perlorodka	k1gFnPc2	perlorodka
v	v	k7c6	v
panských	panský	k2eAgInPc6d1	panský
revírech	revír	k1gInPc6	revír
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
chytání	chytání	k1gNnSc3	chytání
ryb	ryba	k1gFnPc2	ryba
a	a	k8xC	a
raků	rak	k1gMnPc2	rak
bylo	být	k5eAaImAgNnS	být
hodnoceno	hodnotit	k5eAaImNgNnS	hodnotit
daleko	daleko	k6eAd1	daleko
mírněji	mírně	k6eAd2	mírně
a	a	k8xC	a
trestáno	trestat	k5eAaImNgNnS	trestat
jen	jen	k6eAd1	jen
peněžitými	peněžitý	k2eAgInPc7d1	peněžitý
tresty	trest	k1gInPc7	trest
nebo	nebo	k8xC	nebo
pranýřem	pranýř	k1gInSc7	pranýř
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
minulosti	minulost	k1gFnSc2	minulost
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
našeho	náš	k3xOp1gNnSc2	náš
území	území	k1gNnSc2	území
i	i	k8xC	i
sousedních	sousední	k2eAgFnPc2d1	sousední
oblastí	oblast	k1gFnPc2	oblast
známy	znám	k2eAgInPc1d1	znám
četné	četný	k2eAgInPc1d1	četný
přestřelky	přestřelek	k1gInPc1	přestřelek
mezi	mezi	k7c7	mezi
lesnickým	lesnický	k2eAgInSc7d1	lesnický
personálem	personál	k1gInSc7	personál
a	a	k8xC	a
přistiženými	přistižený	k2eAgMnPc7d1	přistižený
pytláky	pytlák	k1gMnPc7	pytlák
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
skončily	skončit	k5eAaPmAgFnP	skončit
smrtí	smrtit	k5eAaImIp3nP	smrtit
nebo	nebo	k8xC	nebo
zmrzačením	zmrzačení	k1gNnSc7	zmrzačení
některého	některý	k3yIgMnSc2	některý
z	z	k7c2	z
aktérů	aktér	k1gMnPc2	aktér
<g/>
.	.	kIx.	.
</s>
<s>
Pytláci	pytlák	k1gMnPc1	pytlák
se	se	k3xPyFc4	se
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
psanci	psanec	k1gMnPc1	psanec
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
později	pozdě	k6eAd2	pozdě
pašeráci	pašerák	k1gMnPc1	pašerák
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
často	často	k6eAd1	často
stávali	stávat	k5eAaImAgMnP	stávat
opěvovanými	opěvovaný	k2eAgMnPc7d1	opěvovaný
lidovými	lidový	k2eAgMnPc7d1	lidový
hrdiny	hrdina	k1gMnPc7	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
platí	platit	k5eAaImIp3nS	platit
v	v	k7c6	v
ještě	ještě	k6eAd1	ještě
větší	veliký	k2eAgFnSc3d2	veliký
míře	míra	k1gFnSc3	míra
pro	pro	k7c4	pro
německé	německý	k2eAgNnSc4d1	německé
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
takto	takto	k6eAd1	takto
opěvování	opěvování	k1gNnSc2	opěvování
např.	např.	kA	např.
Matthias	Matthias	k1gInSc1	Matthias
Klostermeyer	Klostermeyer	k1gInSc1	Klostermeyer
zvaný	zvaný	k2eAgInSc1d1	zvaný
Bavorský	bavorský	k2eAgInSc1d1	bavorský
Hiesel	Hiesel	k1gInSc1	Hiesel
nebo	nebo	k8xC	nebo
Adam	Adam	k1gMnSc1	Adam
Hasenstab	Hasenstab	k1gMnSc1	Hasenstab
<g/>
.	.	kIx.	.
</s>
<s>
Památkou	památka	k1gFnSc7	památka
na	na	k7c6	na
takové	takový	k3xDgFnSc6	takový
události	událost	k1gFnSc6	událost
bývají	bývat	k5eAaImIp3nP	bývat
v	v	k7c6	v
lesích	les	k1gInPc6	les
umístěné	umístěný	k2eAgInPc1d1	umístěný
pomníčky	pomníček	k1gInPc1	pomníček
a	a	k8xC	a
kříže	kříž	k1gInPc1	kříž
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Stammelova	Stammelův	k2eAgFnSc1d1	Stammelův
smrt	smrt	k1gFnSc1	smrt
a	a	k8xC	a
Hennrichův	Hennrichův	k2eAgInSc1d1	Hennrichův
kříž	kříž	k1gInSc1	kříž
v	v	k7c6	v
Jizerských	jizerský	k2eAgFnPc6d1	Jizerská
horách	hora	k1gFnPc6	hora
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
pytláků	pytlák	k1gMnPc2	pytlák
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
loví	lovit	k5eAaImIp3nP	lovit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
opatřili	opatřit	k5eAaPmAgMnP	opatřit
vzrušení	vzrušení	k1gNnSc4	vzrušení
<g/>
,	,	kIx,	,
maso	maso	k1gNnSc4	maso
nebo	nebo	k8xC	nebo
trofeje	trofej	k1gFnPc4	trofej
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgInPc4	který
nemají	mít	k5eNaImIp3nP	mít
nárok	nárok	k1gInSc4	nárok
<g/>
,	,	kIx,	,
a	a	k8xC	a
případně	případně	k6eAd1	případně
je	on	k3xPp3gFnPc4	on
dál	daleko	k6eAd2	daleko
zpeněžili	zpeněžit	k5eAaPmAgMnP	zpeněžit
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
roli	role	k1gFnSc4	role
též	též	k9	též
pytláctví	pytláctví	k1gNnSc2	pytláctví
samotných	samotný	k2eAgMnPc2d1	samotný
myslivců	myslivec	k1gMnPc2	myslivec
<g/>
,	,	kIx,	,
zaměřené	zaměřený	k2eAgFnPc4d1	zaměřená
především	především	k6eAd1	především
na	na	k7c4	na
chráněné	chráněný	k2eAgFnPc4d1	chráněná
a	a	k8xC	a
přísně	přísně	k6eAd1	přísně
chráněné	chráněný	k2eAgMnPc4d1	chráněný
dravce	dravec	k1gMnPc4	dravec
živící	živící	k2eAgMnPc4d1	živící
se	se	k3xPyFc4	se
zvířaty	zvíře	k1gNnPc7	zvíře
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
jsou	být	k5eAaImIp3nP	být
objektem	objekt	k1gInSc7	objekt
zájmu	zájem	k1gInSc2	zájem
myslivců	myslivec	k1gMnPc2	myslivec
<g/>
.	.	kIx.	.
</s>
<s>
Myslivci	Myslivec	k1gMnPc1	Myslivec
považují	považovat	k5eAaImIp3nP	považovat
tyto	tento	k3xDgMnPc4	tento
dravce	dravec	k1gMnPc4	dravec
za	za	k7c4	za
nežádoucí	žádoucí	k2eNgFnSc4d1	nežádoucí
konkurenci	konkurence	k1gFnSc4	konkurence
a	a	k8xC	a
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
případů	případ	k1gInPc2	případ
zákony	zákon	k1gInPc4	zákon
a	a	k8xC	a
vyhlášky	vyhláška	k1gFnPc4	vyhláška
o	o	k7c6	o
jejich	jejich	k3xOp3gFnSc6	jejich
ochraně	ochrana	k1gFnSc6	ochrana
prostě	prostě	k9	prostě
ignorují	ignorovat	k5eAaImIp3nP	ignorovat
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
postoj	postoj	k1gInSc4	postoj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dal	dát	k5eAaPmAgInS	dát
přičíst	přičíst	k5eAaPmF	přičíst
pouze	pouze	k6eAd1	pouze
několika	několik	k4yIc3	několik
jednotlivcům	jednotlivec	k1gMnPc3	jednotlivec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
převažující	převažující	k2eAgFnSc4d1	převažující
mentalitu	mentalita	k1gFnSc4	mentalita
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
myslivecké	myslivecký	k2eAgFnSc6d1	myslivecká
komunitě	komunita	k1gFnSc6	komunita
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
např.	např.	kA	např.
rys	rys	k1gMnSc1	rys
ostrovid	ostrovid	k1gMnSc1	ostrovid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Kořeny	kořen	k1gInPc1	kořen
pytláctví	pytláctví	k1gNnSc2	pytláctví
lze	lze	k6eAd1	lze
vystopovat	vystopovat	k5eAaPmF	vystopovat
už	už	k9	už
na	na	k7c6	na
samém	samý	k3xTgInSc6	samý
úsvitu	úsvit	k1gInSc6	úsvit
lidské	lidský	k2eAgFnSc2d1	lidská
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Pytláctví	pytláctví	k1gNnSc1	pytláctví
bylo	být	k5eAaImAgNnS	být
vždy	vždy	k6eAd1	vždy
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
oblastí	oblast	k1gFnSc7	oblast
lovu	lov	k1gInSc2	lov
a	a	k8xC	a
ochrany	ochrana	k1gFnSc2	ochrana
volně	volně	k6eAd1	volně
žijících	žijící	k2eAgMnPc2d1	žijící
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
kdy	kdy	k6eAd1	kdy
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
právo	právo	k1gNnSc1	právo
lovu	lov	k1gInSc2	lov
a	a	k8xC	a
rozdělilo	rozdělit	k5eAaPmAgNnS	rozdělit
tak	tak	k6eAd1	tak
lidskou	lidský	k2eAgFnSc4d1	lidská
společnost	společnost	k1gFnSc4	společnost
na	na	k7c4	na
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
oprávněním	oprávnění	k1gNnSc7	oprávnění
k	k	k7c3	k
lovu	lov	k1gInSc3	lov
zvěře	zvěř	k1gFnSc2	zvěř
a	a	k8xC	a
ryb	ryba	k1gFnPc2	ryba
disponovali	disponovat	k5eAaBmAgMnP	disponovat
a	a	k8xC	a
na	na	k7c4	na
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
toto	tento	k3xDgNnSc4	tento
oprávnění	oprávnění	k1gNnSc4	oprávnění
neměli	mít	k5eNaImAgMnP	mít
<g/>
,	,	kIx,	,
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
fenomén	fenomén	k1gInSc1	fenomén
pytláctví	pytláctví	k1gNnSc2	pytláctví
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
bylo	být	k5eAaImAgNnS	být
pytláctví	pytláctví	k1gNnSc4	pytláctví
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
velmi	velmi	k6eAd1	velmi
přísně	přísně	k6eAd1	přísně
trestáno	trestat	k5eAaImNgNnS	trestat
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
nepodařilo	podařit	k5eNaPmAgNnS	podařit
zcela	zcela	k6eAd1	zcela
vymýtit	vymýtit	k5eAaPmF	vymýtit
<g/>
.	.	kIx.	.
</s>
<s>
Kvetlo	kvést	k5eAaImAgNnS	kvést
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
období	období	k1gNnSc6	období
válek	válka	k1gFnPc2	válka
a	a	k8xC	a
hladomorů	hladomor	k1gInPc2	hladomor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
klidnějších	klidný	k2eAgFnPc6d2	klidnější
dobách	doba	k1gFnPc6	doba
si	se	k3xPyFc3	se
mnozí	mnohý	k2eAgMnPc1d1	mnohý
přivydělávali	přivydělávat	k5eAaImAgMnP	přivydělávat
prodejem	prodej	k1gInSc7	prodej
upytlačené	upytlačený	k2eAgFnSc2d1	upytlačená
zvěře	zvěř	k1gFnSc2	zvěř
či	či	k8xC	či
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
a	a	k8xC	a
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
novověku	novověk	k1gInSc6	novověk
pytláci	pytlák	k1gMnPc1	pytlák
často	často	k6eAd1	často
končili	končit	k5eAaImAgMnP	končit
na	na	k7c4	na
šibenici	šibenice	k1gFnSc4	šibenice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1562	[number]	k4	1562
buchlovský	buchlovský	k2eAgInSc1d1	buchlovský
soud	soud	k1gInSc1	soud
odsoudil	odsoudit	k5eAaPmAgMnS	odsoudit
pytláka	pytlák	k1gMnSc4	pytlák
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
starým	starý	k2eAgInSc7d1	starý
způsobem	způsob	k1gInSc7	způsob
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
odsouzený	odsouzený	k1gMnSc1	odsouzený
byl	být	k5eAaImAgMnS	být
oběšen	oběsit	k5eAaPmNgMnS	oběsit
v	v	k7c6	v
srnčích	srnčí	k2eAgFnPc6d1	srnčí
a	a	k8xC	a
zvířecích	zvířecí	k2eAgFnPc6d1	zvířecí
kůžích	kůže	k1gFnPc6	kůže
s	s	k7c7	s
přibitými	přibitý	k2eAgInPc7d1	přibitý
parohy	paroh	k1gInPc7	paroh
nad	nad	k7c7	nad
hlavou	hlava	k1gFnSc7	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
trestní	trestní	k2eAgInSc1d1	trestní
zákoník	zákoník	k1gInSc1	zákoník
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
skutkovou	skutkový	k2eAgFnSc4d1	skutková
podstatu	podstata	k1gFnSc4	podstata
trestného	trestný	k2eAgInSc2d1	trestný
činu	čin	k1gInSc2	čin
pytláctví	pytláctví	k1gNnSc2	pytláctví
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
ustanovení	ustanovení	k1gNnSc6	ustanovení
§	§	k?	§
304	[number]	k4	304
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
trestný	trestný	k2eAgInSc4d1	trestný
čin	čin	k1gInSc4	čin
spáchá	spáchat	k5eAaPmIp3nS	spáchat
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
neoprávněně	oprávněně	k6eNd1	oprávněně
uloví	ulovit	k5eAaPmIp3nS	ulovit
zvěř	zvěř	k1gFnSc1	zvěř
nebo	nebo	k8xC	nebo
ryby	ryba	k1gFnPc1	ryba
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
nikoli	nikoli	k9	nikoli
nepatrné	nepatrný	k2eAgNnSc1d1	nepatrný
nebo	nebo	k8xC	nebo
ukryje	ukrýt	k5eAaPmIp3nS	ukrýt
<g/>
,	,	kIx,	,
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
nebo	nebo	k8xC	nebo
jiného	jiné	k1gNnSc2	jiné
převede	převést	k5eAaPmIp3nS	převést
nebo	nebo	k8xC	nebo
přechovává	přechovávat	k5eAaImIp3nS	přechovávat
neoprávněně	oprávněně	k6eNd1	oprávněně
ulovenou	ulovený	k2eAgFnSc4d1	ulovená
zvěř	zvěř	k1gFnSc4	zvěř
nebo	nebo	k8xC	nebo
ryby	ryba	k1gFnPc4	ryba
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
nikoli	nikoli	k9	nikoli
nepatrné	patrný	k2eNgFnPc1d1	patrný
<g/>
.	.	kIx.	.
</s>
<s>
Zvěří	zvěř	k1gFnSc7	zvěř
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
rozumí	rozumět	k5eAaImIp3nS	rozumět
druhy	druh	k1gInPc4	druh
volně	volně	k6eAd1	volně
žijících	žijící	k2eAgMnPc2d1	žijící
živočichů	živočich	k1gMnPc2	živočich
uvedených	uvedený	k2eAgMnPc2d1	uvedený
v	v	k7c6	v
§	§	k?	§
2	[number]	k4	2
písm	písmo	k1gNnPc2	písmo
<g/>
.	.	kIx.	.
c	c	k0	c
<g/>
)	)	kIx)	)
a	a	k8xC	a
d	d	k?	d
<g/>
)	)	kIx)	)
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
449	[number]	k4	449
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
myslivosti	myslivost	k1gFnSc6	myslivost
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
pytláctví	pytláctví	k1gNnSc4	pytláctví
hrozí	hrozit	k5eAaImIp3nS	hrozit
trest	trest	k1gInSc4	trest
odnětí	odnětí	k1gNnSc2	odnětí
svobody	svoboda	k1gFnSc2	svoboda
až	až	k9	až
na	na	k7c4	na
dvě	dva	k4xCgNnPc4	dva
léta	léto	k1gNnPc4	léto
<g/>
,	,	kIx,	,
zákazem	zákaz	k1gInSc7	zákaz
činnosti	činnost	k1gFnSc2	činnost
nebo	nebo	k8xC	nebo
propadnutím	propadnutí	k1gNnSc7	propadnutí
věci	věc	k1gFnSc2	věc
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgFnSc2d1	jiná
majetkové	majetkový	k2eAgFnSc2d1	majetková
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Přísněji	přísně	k6eAd2	přísně
zákon	zákon	k1gInSc1	zákon
trestá	trestat	k5eAaImIp3nS	trestat
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
pytlačí	pytlačit	k5eAaImIp3nS	pytlačit
jako	jako	k9	jako
člen	člen	k1gMnSc1	člen
organizované	organizovaný	k2eAgFnSc2d1	organizovaná
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
získá	získat	k5eAaPmIp3nS	získat
takovým	takový	k3xDgInSc7	takový
činem	čin	k1gInSc7	čin
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
jiného	jiný	k2eAgMnSc2d1	jiný
větší	veliký	k2eAgInSc4d2	veliký
prospěch	prospěch	k1gInSc4	prospěch
<g/>
,	,	kIx,	,
spáchá	spáchat	k5eAaPmIp3nS	spáchat
takový	takový	k3xDgInSc1	takový
čin	čin	k1gInSc1	čin
jako	jako	k8xC	jako
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
zvlášť	zvlášť	k6eAd1	zvlášť
uloženou	uložený	k2eAgFnSc4d1	uložená
povinnost	povinnost	k1gFnSc4	povinnost
chránit	chránit	k5eAaImF	chránit
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
(	(	kIx(	(
<g/>
např.	např.	kA	např.
lesní	lesní	k2eAgFnSc1d1	lesní
stráž	stráž	k1gFnSc1	stráž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spáchá	spáchat	k5eAaPmIp3nS	spáchat
takový	takový	k3xDgInSc1	takový
čin	čin	k1gInSc1	čin
zvlášť	zvlášť	k6eAd1	zvlášť
zavrženíhodným	zavrženíhodný	k2eAgInSc7d1	zavrženíhodný
způsobem	způsob	k1gInSc7	způsob
(	(	kIx(	(
<g/>
použije	použít	k5eAaPmIp3nS	použít
např.	např.	kA	např.
do	do	k7c2	do
železa	železo	k1gNnSc2	železo
či	či	k8xC	či
do	do	k7c2	do
oka	oko	k1gNnSc2	oko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hromadně	hromadně	k6eAd1	hromadně
účinným	účinný	k2eAgInSc7d1	účinný
způsobem	způsob	k1gInSc7	způsob
(	(	kIx(	(
<g/>
např.	např.	kA	např.
výbušnina	výbušnina	k1gFnSc1	výbušnina
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
v	v	k7c6	v
době	doba	k1gFnSc6	doba
hájení	hájení	k1gNnSc2	hájení
nebo	nebo	k8xC	nebo
byl	být	k5eAaImAgMnS	být
za	za	k7c4	za
takový	takový	k3xDgInSc4	takový
čin	čin	k1gInSc4	čin
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
nebo	nebo	k8xC	nebo
potrestán	potrestat	k5eAaPmNgMnS	potrestat
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
potrestán	potrestat	k5eAaPmNgMnS	potrestat
trestem	trest	k1gInSc7	trest
odnětí	odnětí	k1gNnSc1	odnětí
svobody	svoboda	k1gFnSc2	svoboda
na	na	k7c4	na
šest	šest	k4xCc4	šest
měsíců	měsíc	k1gInPc2	měsíc
až	až	k6eAd1	až
pět	pět	k4xCc1	pět
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
peněžitým	peněžitý	k2eAgInSc7d1	peněžitý
trestem	trest	k1gInSc7	trest
nebo	nebo	k8xC	nebo
propadnutím	propadnutí	k1gNnSc7	propadnutí
věci	věc	k1gFnSc2	věc
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
původní	původní	k2eAgFnSc3d1	původní
úpravě	úprava	k1gFnSc3	úprava
v	v	k7c6	v
trestním	trestní	k2eAgInSc6d1	trestní
zákoně	zákon	k1gInSc6	zákon
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
pytláctví	pytláctví	k1gNnSc2	pytláctví
dopustil	dopustit	k5eAaPmAgMnS	dopustit
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
neoprávněně	oprávněně	k6eNd1	oprávněně
pouze	pouze	k6eAd1	pouze
"	"	kIx"	"
<g/>
lovil	lovit	k5eAaImAgMnS	lovit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
k	k	k7c3	k
trestněprávnímu	trestněprávní	k2eAgInSc3d1	trestněprávní
postihu	postih	k1gInSc3	postih
vyžadováno	vyžadován	k2eAgNnSc1d1	vyžadováno
již	již	k9	již
"	"	kIx"	"
<g/>
ulovení	ulovení	k1gNnSc1	ulovení
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Sankce	sankce	k1gFnSc1	sankce
jen	jen	k9	jen
za	za	k7c4	za
pokusy	pokus	k1gInPc4	pokus
o	o	k7c6	o
ulovení	ulovení	k1gNnSc6	ulovení
dříve	dříve	k6eAd2	dříve
řešil	řešit	k5eAaImAgInS	řešit
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
přestupcích	přestupek	k1gInPc6	přestupek
(	(	kIx(	(
<g/>
č.	č.	k?	č.
200	[number]	k4	200
<g/>
/	/	kIx~	/
<g/>
1990	[number]	k4	1990
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2017	[number]	k4	2017
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
zákon	zákon	k1gInSc1	zákon
o	o	k7c4	o
myslivosti	myslivost	k1gFnPc4	myslivost
v	v	k7c6	v
§	§	k?	§
48	[number]	k4	48
<g/>
a	a	k8xC	a
a	a	k8xC	a
§	§	k?	§
63	[number]	k4	63
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
1	[number]	k4	1
písm	písmo	k1gNnPc2	písmo
<g/>
.	.	kIx.	.
b	b	k?	b
<g/>
)	)	kIx)	)
přestupek	přestupek	k1gInSc1	přestupek
neoprávněného	oprávněný	k2eNgInSc2d1	neoprávněný
lovu	lov	k1gInSc2	lov
zvěře	zvěř	k1gFnSc2	zvěř
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
rovněž	rovněž	k9	rovněž
spadá	spadat	k5eAaImIp3nS	spadat
i	i	k9	i
pokus	pokus	k1gInSc4	pokus
o	o	k7c4	o
ulovení	ulovení	k1gNnSc4	ulovení
bez	bez	k7c2	bez
platné	platný	k2eAgFnSc2d1	platná
povolenky	povolenka	k1gFnSc2	povolenka
k	k	k7c3	k
lovu	lov	k1gInSc3	lov
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
něj	on	k3xPp3gMnSc2	on
lze	lze	k6eAd1	lze
uložit	uložit	k5eAaPmF	uložit
pokutu	pokuta	k1gFnSc4	pokuta
až	až	k9	až
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
30	[number]	k4	30
000	[number]	k4	000
Kč	Kč	kA	Kč
(	(	kIx(	(
<g/>
při	při	k7c6	při
opakování	opakování	k1gNnSc6	opakování
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
roce	rok	k1gInSc6	rok
až	až	k9	až
60	[number]	k4	60
000	[number]	k4	000
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
také	také	k9	také
zákaz	zákaz	k1gInSc4	zákaz
činnosti	činnost	k1gFnSc2	činnost
až	až	k9	až
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
a	a	k8xC	a
odebrání	odebrání	k1gNnSc4	odebrání
loveckého	lovecký	k2eAgInSc2d1	lovecký
lístku	lístek	k1gInSc2	lístek
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
neoprávněně	oprávněně	k6eNd1	oprávněně
lovil	lovit	k5eAaImAgMnS	lovit
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
podle	podle	k7c2	podle
§	§	k?	§
56	[number]	k4	56
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
myslivosti	myslivost	k1gFnSc6	myslivost
i	i	k9	i
povinnost	povinnost	k1gFnSc4	povinnost
náhrady	náhrada	k1gFnSc2	náhrada
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
škody	škoda	k1gFnSc2	škoda
vůči	vůči	k7c3	vůči
oprávněnému	oprávněný	k2eAgMnSc3d1	oprávněný
uživateli	uživatel	k1gMnSc3	uživatel
honitby	honitba	k1gFnSc2	honitba
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
bylo	být	k5eAaImAgNnS	být
pytláctví	pytláctví	k1gNnSc1	pytláctví
trestáno	trestat	k5eAaImNgNnS	trestat
až	až	k6eAd1	až
deseti	deset	k4xCc7	deset
lety	let	k1gInPc7	let
nucených	nucený	k2eAgFnPc2d1	nucená
prací	práce	k1gFnPc2	práce
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
na	na	k7c6	na
stavbách	stavba	k1gFnPc6	stavba
vojenských	vojenský	k2eAgFnPc2d1	vojenská
pevností	pevnost	k1gFnPc2	pevnost
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vypovězením	vypovězení	k1gNnSc7	vypovězení
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Lovecký	lovecký	k2eAgInSc1d1	lovecký
řád	řád	k1gInSc1	řád
vydaný	vydaný	k2eAgInSc1d1	vydaný
za	za	k7c4	za
císaře	císař	k1gMnSc4	císař
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
ještě	ještě	k6eAd1	ještě
připouštěl	připouštět	k5eAaImAgMnS	připouštět
zastřelení	zastřelení	k1gNnSc4	zastřelení
pytláka	pytlák	k1gMnSc2	pytlák
<g/>
.	.	kIx.	.
</s>
