<s>
Zabiják	zabiják	k1gMnSc1	zabiják
(	(	kIx(	(
<g/>
orig	orig	k1gMnSc1	orig
<g/>
.	.	kIx.	.
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Assommoir	Assommoir	k1gMnSc1	Assommoir
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
sedmý	sedmý	k4xOgInSc1	sedmý
román	román	k1gInSc1	román
Emila	Emil	k1gMnSc2	Emil
Zoly	Zola	k1gMnSc2	Zola
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
Rougon-Macquartové	Rougon-Macquartové	k2eAgInSc2d1	Rougon-Macquartové
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1877	[number]	k4	1877
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
jedno	jeden	k4xCgNnSc4	jeden
ze	z	k7c2	z
Zolových	Zolův	k2eAgNnPc2d1	Zolův
nejlepších	dobrý	k2eAgNnPc2d3	nejlepší
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Popisuje	popisovat	k5eAaImIp3nS	popisovat
zhoubný	zhoubný	k2eAgInSc1d1	zhoubný
vliv	vliv	k1gInSc1	vliv
alkoholu	alkohol	k1gInSc2	alkohol
na	na	k7c4	na
osudy	osud	k1gInPc4	osud
chudých	chudý	k2eAgInPc2d1	chudý
a	a	k8xC	a
duchovně	duchovně	k6eAd1	duchovně
prázdných	prázdný	k2eAgMnPc2d1	prázdný
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
románu	román	k1gInSc6	román
jsou	být	k5eAaImIp3nP	být
detailní	detailní	k2eAgInPc4d1	detailní
popisy	popis	k1gInPc4	popis
alkoholismu	alkoholismus	k1gInSc2	alkoholismus
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
tím	ten	k3xDgInSc7	ten
zabijákem	zabiják	k1gInSc7	zabiják
<g/>
,	,	kIx,	,
co	co	k9	co
dal	dát	k5eAaPmAgMnS	dát
románu	román	k1gInSc2	román
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
začal	začít	k5eAaPmAgInS	začít
vycházet	vycházet	k5eAaImF	vycházet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1876	[number]	k4	1876
na	na	k7c4	na
pokračování	pokračování	k1gNnSc4	pokračování
v	v	k7c6	v
pařížských	pařížský	k2eAgFnPc6d1	Pařížská
novinách	novina	k1gFnPc6	novina
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
naturalistickým	naturalistický	k2eAgInPc3d1	naturalistický
popisům	popis	k1gInPc3	popis
ale	ale	k8xC	ale
začali	začít	k5eAaPmAgMnP	začít
čtenáři	čtenář	k1gMnPc1	čtenář
hromadně	hromadně	k6eAd1	hromadně
odhlašovat	odhlašovat	k5eAaImF	odhlašovat
předplatné	předplatné	k1gNnSc4	předplatné
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
Zabiják	zabiják	k1gInSc1	zabiják
stažen	stáhnout	k5eAaPmNgInS	stáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
však	však	k9	však
našel	najít	k5eAaPmAgMnS	najít
jiného	jiný	k2eAgMnSc4d1	jiný
vydavatele	vydavatel	k1gMnSc4	vydavatel
a	a	k8xC	a
i	i	k9	i
přes	přes	k7c4	přes
hrozby	hrozba	k1gFnPc4	hrozba
dílo	dílo	k1gNnSc4	dílo
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
<g/>
.	.	kIx.	.
</s>
<s>
Knižní	knižní	k2eAgNnSc1d1	knižní
vydání	vydání	k1gNnSc1	vydání
bylo	být	k5eAaImAgNnS	být
nakonec	nakonec	k6eAd1	nakonec
neúspěšné	úspěšný	k2eNgNnSc1d1	neúspěšné
<g/>
.	.	kIx.	.
</s>
<s>
Venkovanka	venkovanka	k1gFnSc1	venkovanka
Gervaisa	Gervaisa	k1gFnSc1	Gervaisa
přijede	přijet	k5eAaPmIp3nS	přijet
spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
druhem	druh	k1gMnSc7	druh
Lantierem	Lantier	k1gMnSc7	Lantier
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc7	jejich
dvěma	dva	k4xCgFnPc7	dva
dětmi	dítě	k1gFnPc7	dítě
Štěpánem	Štěpán	k1gMnSc7	Štěpán
a	a	k8xC	a
Klaudiem	Klaudium	k1gNnSc7	Klaudium
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
si	se	k3xPyFc3	se
díky	díky	k7c3	díky
našetřeným	našetřený	k2eAgInPc3d1	našetřený
penězům	peníze	k1gInPc3	peníze
žijí	žít	k5eAaImIp3nP	žít
"	"	kIx"	"
<g/>
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
noze	noha	k1gFnSc6	noha
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
postupně	postupně	k6eAd1	postupně
zastaví	zastavit	k5eAaPmIp3nS	zastavit
většinu	většina	k1gFnSc4	většina
svého	svůj	k3xOyFgInSc2	svůj
majetku	majetek	k1gInSc2	majetek
a	a	k8xC	a
přestěhují	přestěhovat	k5eAaPmIp3nP	přestěhovat
se	se	k3xPyFc4	se
do	do	k7c2	do
zapadlé	zapadlý	k2eAgFnSc2d1	zapadlá
části	část	k1gFnSc2	část
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Lantier	Lantier	k1gInSc1	Lantier
ji	on	k3xPp3gFnSc4	on
po	po	k7c6	po
čase	čas	k1gInSc6	čas
opustil	opustit	k5eAaPmAgMnS	opustit
s	s	k7c7	s
milenkou	milenka	k1gFnSc7	milenka
<g/>
,	,	kIx,	,
po	po	k7c6	po
čemž	což	k3yQnSc6	což
Gervaisa	Gervaisa	k1gFnSc1	Gervaisa
velmi	velmi	k6eAd1	velmi
smutnila	smutnit	k5eAaImAgFnS	smutnit
i	i	k9	i
přesto	přesto	k6eAd1	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
mlátil	mlátit	k5eAaImAgMnS	mlátit
a	a	k8xC	a
stále	stále	k6eAd1	stále
ji	on	k3xPp3gFnSc4	on
kvůli	kvůli	k7c3	kvůli
něčemu	něco	k3yInSc3	něco
kritizoval	kritizovat	k5eAaImAgInS	kritizovat
<g/>
.	.	kIx.	.
</s>
<s>
Gervaisa	Gervaisa	k1gFnSc1	Gervaisa
začne	začít	k5eAaPmIp3nS	začít
pracovat	pracovat	k5eAaImF	pracovat
jako	jako	k9	jako
pradlena	pradlena	k1gFnSc1	pradlena
<g/>
,	,	kIx,	,
stane	stanout	k5eAaPmIp3nS	stanout
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
oblíbenou	oblíbený	k2eAgFnSc4d1	oblíbená
a	a	k8xC	a
začne	začít	k5eAaPmIp3nS	začít
se	se	k3xPyFc4	se
o	o	k7c4	o
ni	on	k3xPp3gFnSc4	on
zajímat	zajímat	k5eAaImF	zajímat
klempíř	klempíř	k1gMnSc1	klempíř
Coupeau	Coupeaus	k1gInSc2	Coupeaus
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
ji	on	k3xPp3gFnSc4	on
přemluví	přemluvit	k5eAaPmIp3nS	přemluvit
ke	k	k7c3	k
svatbě	svatba	k1gFnSc3	svatba
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
ta	ten	k3xDgFnSc1	ten
nejprve	nejprve	k6eAd1	nejprve
odmítala	odmítat	k5eAaImAgFnS	odmítat
kvůli	kvůli	k7c3	kvůli
tehdejším	tehdejší	k2eAgFnPc3d1	tehdejší
zvyklostem	zvyklost	k1gFnPc3	zvyklost
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
Gervaisa	Gervais	k1gMnSc4	Gervais
i	i	k8xC	i
Coupeau	Coupeaa	k1gMnSc4	Coupeaa
jsou	být	k5eAaImIp3nP	být
pracovití	pracovitý	k2eAgMnPc1d1	pracovitý
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
si	se	k3xPyFc3	se
vydělávají	vydělávat	k5eAaImIp3nP	vydělávat
<g/>
,	,	kIx,	,
stěhují	stěhovat	k5eAaImIp3nP	stěhovat
se	se	k3xPyFc4	se
do	do	k7c2	do
lepšího	dobrý	k2eAgInSc2d2	lepší
bytu	byt	k1gInSc2	byt
a	a	k8xC	a
šetří	šetřit	k5eAaImIp3nS	šetřit
i	i	k9	i
na	na	k7c4	na
horší	zlý	k2eAgInPc4d2	horší
časy	čas	k1gInPc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
mají	mít	k5eAaImIp3nP	mít
dceru	dcera	k1gFnSc4	dcera
Nanu	Nana	k1gFnSc4	Nana
<g/>
,	,	kIx,	,
Klaudia	Klaudium	k1gNnPc1	Klaudium
pošlou	poslat	k5eAaPmIp3nP	poslat
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
na	na	k7c4	na
jih	jih	k1gInSc4	jih
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Rodinná	rodinný	k2eAgFnSc1d1	rodinná
idyla	idyla	k1gFnSc1	idyla
se	se	k3xPyFc4	se
začne	začít	k5eAaPmIp3nS	začít
hroutit	hroutit	k5eAaImF	hroutit
po	po	k7c6	po
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
jednoho	jeden	k4xCgNnSc2	jeden
odpoledne	odpoledne	k1gNnSc2	odpoledne
jde	jít	k5eAaImIp3nS	jít
Gervaisa	Gervaisa	k1gFnSc1	Gervaisa
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Nanou	Nana	k1gFnSc7	Nana
navštívit	navštívit	k5eAaPmF	navštívit
Coupeaua	Coupeauum	k1gNnPc4	Coupeauum
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Nana	Nana	k1gFnSc1	Nana
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
zavolá	zavolat	k5eAaPmIp3nS	zavolat
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnPc4	její
tatínek	tatínek	k1gMnSc1	tatínek
zakopne	zakopnout	k5eAaPmIp3nS	zakopnout
a	a	k8xC	a
spadne	spadnout	k5eAaPmIp3nS	spadnout
ze	z	k7c2	z
střechy	střecha	k1gFnSc2	střecha
<g/>
.	.	kIx.	.
</s>
<s>
Coupeau	Coupeau	k6eAd1	Coupeau
nemůže	moct	k5eNaImIp3nS	moct
asi	asi	k9	asi
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
pracovat	pracovat	k5eAaImF	pracovat
a	a	k8xC	a
všechny	všechen	k3xTgInPc1	všechen
našetřené	našetřený	k2eAgInPc1d1	našetřený
peníze	peníz	k1gInPc1	peníz
padnou	padnout	k5eAaPmIp3nP	padnout
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
uzdravení	uzdravení	k1gNnSc6	uzdravení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyléčení	vyléčení	k1gNnSc6	vyléčení
už	už	k6eAd1	už
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
znovu	znovu	k6eAd1	znovu
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
nechtělo	chtít	k5eNaImAgNnS	chtít
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
na	na	k7c4	na
vydělávání	vydělávání	k1gNnSc4	vydělávání
zůstala	zůstat	k5eAaPmAgFnS	zůstat
Gervaisa	Gervaisa	k1gFnSc1	Gervaisa
sama	sám	k3xTgFnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
Gervaisa	Gervaisa	k1gFnSc1	Gervaisa
dlouho	dlouho	k6eAd1	dlouho
toužila	toužit	k5eAaImAgFnS	toužit
po	po	k7c6	po
vlastní	vlastní	k2eAgFnSc6d1	vlastní
prádelně	prádelna	k1gFnSc6	prádelna
<g/>
,	,	kIx,	,
půjčili	půjčit	k5eAaPmAgMnP	půjčit
si	se	k3xPyFc3	se
od	od	k7c2	od
známých	známá	k1gFnPc2	známá
peníze	peníz	k1gInSc2	peníz
a	a	k8xC	a
zařídila	zařídit	k5eAaPmAgFnS	zařídit
si	se	k3xPyFc3	se
vlastní	vlastní	k2eAgInSc4d1	vlastní
krám	krám	k1gInSc4	krám
<g/>
.	.	kIx.	.
</s>
<s>
Zaměstnala	zaměstnat	k5eAaPmAgFnS	zaměstnat
pradleny	pradlena	k1gFnSc2	pradlena
a	a	k8xC	a
zpočátku	zpočátku	k6eAd1	zpočátku
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
i	i	k9	i
dařilo	dařit	k5eAaImAgNnS	dařit
úspěšně	úspěšně	k6eAd1	úspěšně
splácet	splácet	k5eAaImF	splácet
dluhy	dluh	k1gInPc4	dluh
<g/>
,	,	kIx,	,
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
známými	známý	k1gMnPc7	známý
uznávanou	uznávaný	k2eAgFnSc4d1	uznávaná
<g/>
.	.	kIx.	.
</s>
<s>
Coupeau	Coupeau	k6eAd1	Coupeau
ale	ale	k9	ale
začal	začít	k5eAaPmAgInS	začít
chodit	chodit	k5eAaImF	chodit
do	do	k7c2	do
hospody	hospody	k?	hospody
a	a	k8xC	a
holdovat	holdovat	k5eAaImF	holdovat
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
něhož	jenž	k3xRgMnSc2	jenž
utišoval	utišovat	k5eAaImAgInS	utišovat
svědomí	svědomí	k1gNnSc4	svědomí
a	a	k8xC	a
vyplňoval	vyplňovat	k5eAaImAgMnS	vyplňovat
pocit	pocit	k1gInSc4	pocit
prázdnoty	prázdnota	k1gFnSc2	prázdnota
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
oslavy	oslava	k1gFnSc2	oslava
Gervaisina	Gervaisin	k2eAgInSc2d1	Gervaisin
svátku	svátek	k1gInSc2	svátek
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yIgInSc4	který
pozvala	pozvat	k5eAaPmAgFnS	pozvat
mnoho	mnoho	k4c4	mnoho
známých	známá	k1gFnPc2	známá
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
náhle	náhle	k6eAd1	náhle
vrátil	vrátit	k5eAaPmAgMnS	vrátit
její	její	k3xOp3gMnSc1	její
bývalý	bývalý	k2eAgMnSc1d1	bývalý
druh	druh	k1gMnSc1	druh
Lantier	Lantier	k1gMnSc1	Lantier
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Coupeaův	Coupeaův	k2eAgMnSc1d1	Coupeaův
dobrý	dobrý	k2eAgMnSc1d1	dobrý
kamarád	kamarád	k1gMnSc1	kamarád
a	a	k8xC	a
podnájemník	podnájemník	k1gMnSc1	podnájemník
Coupeauových	Coupeauová	k1gFnPc2	Coupeauová
<g/>
,	,	kIx,	,
nejdříve	dříve	k6eAd3	dříve
platil	platit	k5eAaImAgInS	platit
nájemné	nájemné	k1gNnSc4	nájemné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
chovat	chovat	k5eAaImF	chovat
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
kdyby	kdyby	kYmCp3nS	kdyby
dům	dům	k1gInSc1	dům
patřil	patřit	k5eAaImAgInS	patřit
jemu	on	k3xPp3gMnSc3	on
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
rozkazovat	rozkazovat	k5eAaImF	rozkazovat
<g/>
.	.	kIx.	.
</s>
<s>
Rodině	rodina	k1gFnSc3	rodina
došly	dojít	k5eAaPmAgInP	dojít
peníze	peníz	k1gInPc1	peníz
<g/>
,	,	kIx,	,
Gervaisa	Gervaisa	k1gFnSc1	Gervaisa
musela	muset	k5eAaImAgFnS	muset
propustit	propustit	k5eAaPmF	propustit
zaměstnankyně	zaměstnankyně	k1gFnPc4	zaměstnankyně
a	a	k8xC	a
prodat	prodat	k5eAaPmF	prodat
prádelnu	prádelna	k1gFnSc4	prádelna
<g/>
.	.	kIx.	.
</s>
<s>
Vypočítavý	vypočítavý	k2eAgInSc1d1	vypočítavý
Lantier	Lantier	k1gInSc1	Lantier
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
k	k	k7c3	k
nové	nový	k2eAgFnSc3d1	nová
majitelce	majitelka	k1gFnSc3	majitelka
prádelny	prádelna	k1gFnSc2	prádelna
a	a	k8xC	a
jejímu	její	k3xOp3gMnSc3	její
manželovi	manžel	k1gMnSc3	manžel
<g/>
,	,	kIx,	,
Gervaisiným	Gervaisin	k2eAgMnPc3d1	Gervaisin
i	i	k8xC	i
jeho	jeho	k3xOp3gMnPc3	jeho
známým	známý	k2eAgMnPc3d1	známý
<g/>
.	.	kIx.	.
</s>
<s>
Coupeauvi	Coupeauev	k1gFnSc3	Coupeauev
jsou	být	k5eAaImIp3nP	být
nuceni	nucen	k2eAgMnPc1d1	nucen
se	se	k3xPyFc4	se
přestěhovat	přestěhovat	k5eAaPmF	přestěhovat
do	do	k7c2	do
činžáku	činžák	k1gInSc2	činžák
pro	pro	k7c4	pro
chudinu	chudina	k1gFnSc4	chudina
<g/>
.	.	kIx.	.
</s>
<s>
Coupeau	Coupeau	k6eAd1	Coupeau
začne	začít	k5eAaPmIp3nS	začít
pracovat	pracovat	k5eAaImF	pracovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
každou	každý	k3xTgFnSc4	každý
výplatu	výplata	k1gFnSc4	výplata
hned	hned	k6eAd1	hned
propije	propít	k5eAaPmIp3nS	propít
<g/>
.	.	kIx.	.
</s>
<s>
Gervaisa	Gervaisa	k1gFnSc1	Gervaisa
se	se	k3xPyFc4	se
také	také	k9	také
stane	stanout	k5eAaPmIp3nS	stanout
alkoholičkou	alkoholička	k1gFnSc7	alkoholička
<g/>
,	,	kIx,	,
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
jí	on	k3xPp3gFnSc3	on
to	ten	k3xDgNnSc4	ten
tak	tak	k6eAd1	tak
zapomenout	zapomenout	k5eAaPmF	zapomenout
na	na	k7c4	na
životní	životní	k2eAgFnPc4d1	životní
strasti	strast	k1gFnPc4	strast
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
přestane	přestat	k5eAaPmIp3nS	přestat
starat	starat	k5eAaImF	starat
o	o	k7c4	o
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
začne	začít	k5eAaPmIp3nS	začít
žebrat	žebrat	k5eAaImF	žebrat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Nany	Nana	k1gFnSc2	Nana
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
prostitutka	prostitutka	k1gFnSc1	prostitutka
<g/>
,	,	kIx,	,
Coupeau	Coupeaa	k1gFnSc4	Coupeaa
umírá	umírat	k5eAaImIp3nS	umírat
v	v	k7c6	v
blázinci	blázinec	k1gInSc6	blázinec
na	na	k7c4	na
delirium	delirium	k1gNnSc4	delirium
tremens	tremensa	k1gFnPc2	tremensa
<g/>
.	.	kIx.	.
</s>
<s>
Gervaisa	Gervaisa	k1gFnSc1	Gervaisa
dále	daleko	k6eAd2	daleko
pije	pít	k5eAaImIp3nS	pít
a	a	k8xC	a
umírá	umírat	k5eAaImIp3nS	umírat
bez	bez	k7c2	bez
přátel	přítel	k1gMnPc2	přítel
ve	v	k7c6	v
špinavé	špinavý	k2eAgFnSc6d1	špinavá
kobce	kobka	k1gFnSc6	kobka
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
ale	ale	k8xC	ale
vysvobozením	vysvobození	k1gNnSc7	vysvobození
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
knihy	kniha	k1gFnSc2	kniha
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
natočen	natočit	k5eAaBmNgInS	natočit
film	film	k1gInSc1	film
Gervaise	Gervaise	k1gFnSc2	Gervaise
<g/>
.	.	kIx.	.
</s>
<s>
MACURA	Macura	k1gMnSc1	Macura
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Slovník	slovník	k1gInSc4	slovník
světových	světový	k2eAgNnPc2d1	světové
literárních	literární	k2eAgNnPc2d1	literární
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
svazek	svazek	k1gInSc1	svazek
<g/>
,	,	kIx,	,
M	M	kA	M
<g/>
-	-	kIx~	-
<g/>
Ž.	Ž.	kA	Ž.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
459	[number]	k4	459
s.	s.	k?	s.
[	[	kIx(	[
<g/>
Stať	stať	k1gFnSc1	stať
"	"	kIx"	"
<g/>
Zabiják	zabiják	k1gInSc1	zabiják
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
str	str	kA	str
<g/>
.	.	kIx.	.
392	[number]	k4	392
<g/>
-	-	kIx~	-
<g/>
393	[number]	k4	393
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
FISCHER	Fischer	k1gMnSc1	Fischer
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Otokar	Otokar	k1gMnSc1	Otokar
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
francouzské	francouzský	k2eAgFnSc2d1	francouzská
literatury	literatura	k1gFnSc2	literatura
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
Díl	díl	k1gInSc1	díl
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1870	[number]	k4	1870
<g/>
-	-	kIx~	-
<g/>
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
<s>
Vydání	vydání	k1gNnSc1	vydání
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
770	[number]	k4	770
s.	s.	k?	s.
[	[	kIx(	[
<g/>
O	o	k7c6	o
románu	román	k1gInSc6	román
"	"	kIx"	"
<g/>
Zabiják	zabiják	k1gInSc4	zabiják
<g/>
"	"	kIx"	"
viz	vidět	k5eAaImRp2nS	vidět
str	str	kA	str
<g/>
.	.	kIx.	.
64	[number]	k4	64
<g/>
-	-	kIx~	-
<g/>
68	[number]	k4	68
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
</s>
