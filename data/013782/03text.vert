<s>
ABS	ABS	kA
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
protiblokovacím	protiblokovací	k2eAgInSc6d1
systému	systém	k1gInSc6
zablokování	zablokování	k1gNnSc4
kol	kola	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
ABS	ABS	kA
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Symbol	symbol	k1gInSc1
pro	pro	k7c4
ABS	ABS	kA
<g/>
.	.	kIx.
</s>
<s>
Různé	různý	k2eAgFnPc1d1
kontrolky	kontrolka	k1gFnPc1
ABS	ABS	kA
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
upozorňují	upozorňovat	k5eAaImIp3nP
na	na	k7c4
nefunkčnost	nefunkčnost	k1gFnSc4
ABS	ABS	kA
</s>
<s>
ABS	ABS	kA
je	být	k5eAaImIp3nS
zkratka	zkratka	k1gFnSc1
pro	pro	k7c4
Antiblockiersystem	Antiblockiersyst	k1gFnPc3
nebo	nebo	k8xC
také	také	k9
Anti-lock	Anti-lock	k1gInSc1
Brake	Brake	k1gNnSc2
System	Syst	k1gInSc7
což	což	k3yRnSc1,k3yQnSc1
znamená	znamenat	k5eAaImIp3nS
protiblokovací	protiblokovací	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
systém	systém	k1gInSc1
aktivní	aktivní	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
vozidla	vozidlo	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
zabraňuje	zabraňovat	k5eAaImIp3nS
zablokování	zablokování	k1gNnSc4
kola	kolo	k1gNnSc2
při	při	k7c6
brzdění	brzdění	k1gNnSc6
<g/>
,	,	kIx,
a	a	k8xC
tím	ten	k3xDgNnSc7
ztráty	ztráta	k1gFnPc1
adheze	adheze	k1gFnPc1
mezi	mezi	k7c7
kolem	kolo	k1gNnSc7
a	a	k8xC
vozovkou	vozovka	k1gFnSc7
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
umožňuje	umožňovat	k5eAaImIp3nS
zachování	zachování	k1gNnSc4
stability	stabilita	k1gFnSc2
<g/>
,	,	kIx,
ovladatelnosti	ovladatelnost	k1gFnSc2
a	a	k8xC
řiditelnosti	řiditelnost	k1gFnSc2
vozidla	vozidlo	k1gNnSc2
v	v	k7c6
mezních	mezní	k2eAgFnPc6d1
situacích	situace	k1gFnPc6
(	(	kIx(
<g/>
například	například	k6eAd1
při	při	k7c6
prudkém	prudký	k2eAgNnSc6d1
brzdění	brzdění	k1gNnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ABS	ABS	kA
bylo	být	k5eAaImAgNnS
vynalezeno	vynalezen	k2eAgNnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1929	#num#	k4
<g/>
,	,	kIx,
původně	původně	k6eAd1
pro	pro	k7c4
letadla	letadlo	k1gNnPc4
<g/>
,	,	kIx,
francouzským	francouzský	k2eAgMnSc7d1
vynálezcem	vynálezce	k1gMnSc7
Gabrielem	Gabriel	k1gMnSc7
Voisinem	Voisin	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Princip	princip	k1gInSc1
</s>
<s>
Systém	systém	k1gInSc1
ABS	ABS	kA
na	na	k7c6
motocyklu	motocykl	k1gInSc6
BMW	BMW	kA
<g/>
.	.	kIx.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3
brzdná	brzdný	k2eAgFnSc1d1
síla	síla	k1gFnSc1
mezi	mezi	k7c7
pneumatikou	pneumatika	k1gFnSc7
a	a	k8xC
vozovkou	vozovka	k1gFnSc7
je	být	k5eAaImIp3nS
přenášena	přenášen	k2eAgFnSc1d1
právě	právě	k9
na	na	k7c4
mezi	mezi	k7c4
adheze	adheze	k1gFnPc4
<g/>
,	,	kIx,
po	po	k7c6
jejím	její	k3xOp3gNnSc6
překročení	překročení	k1gNnSc6
prudce	prudko	k6eAd1
klesá	klesat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
řídicí	řídicí	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
systému	systém	k1gInSc2
ABS	ABS	kA
neustále	neustále	k6eAd1
zjišťuje	zjišťovat	k5eAaImIp3nS
aktuální	aktuální	k2eAgFnSc4d1
rychlost	rychlost	k1gFnSc4
otáčení	otáčení	k1gNnSc2
nejlépe	dobře	k6eAd3
každého	každý	k3xTgNnSc2
kola	kolo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
rychlostí	rychlost	k1gFnPc2
dvou	dva	k4xCgFnPc2
diagonálně	diagonálně	k6eAd1
umístěných	umístěný	k2eAgFnPc2d1
kol	kola	k1gFnPc2
(	(	kIx(
<g/>
popř.	popř.	kA
jinak	jinak	k6eAd1
<g/>
)	)	kIx)
určuje	určovat	k5eAaImIp3nS
tzv.	tzv.	kA
referenční	referenční	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
vozidla	vozidlo	k1gNnSc2
<g/>
,	,	kIx,
se	s	k7c7
kterou	který	k3yIgFnSc7,k3yQgFnSc7,k3yRgFnSc7
porovnává	porovnávat	k5eAaImIp3nS
otáčky	otáčka	k1gFnPc4
jednotlivých	jednotlivý	k2eAgNnPc2d1
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgNnSc7
neustálým	neustálý	k2eAgNnSc7d1
porovnáváním	porovnávání	k1gNnSc7
se	se	k3xPyFc4
zjišťuje	zjišťovat	k5eAaImIp3nS
aktuální	aktuální	k2eAgNnSc1d1
zrychlení	zrychlení	k1gNnSc1
<g/>
,	,	kIx,
zpomalení	zpomalení	k1gNnSc1
a	a	k8xC
skluz	skluz	k1gInSc4
každého	každý	k3xTgNnSc2
z	z	k7c2
kol	kola	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
dojde	dojít	k5eAaPmIp3nS
ke	k	k7c3
snížení	snížení	k1gNnSc3
rychlosti	rychlost	k1gFnSc2
některého	některý	k3yIgMnSc2
z	z	k7c2
kol	kolo	k1gNnPc2
pod	pod	k7c4
stanovenou	stanovený	k2eAgFnSc4d1
hodnotu	hodnota	k1gFnSc4
oproti	oproti	k7c3
referenční	referenční	k2eAgFnSc3d1
rychlosti	rychlost	k1gFnSc3
(	(	kIx(
<g/>
počátek	počátek	k1gInSc1
blokování	blokování	k1gNnSc2
kola	kolo	k1gNnSc2
a	a	k8xC
ztráty	ztráta	k1gFnSc2
adheze	adheze	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
řídicí	řídicí	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
odpustí	odpustit	k5eAaPmIp3nS
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
polohu	poloha	k1gFnSc4
brzdového	brzdový	k2eAgInSc2d1
pedálu	pedál	k1gInSc2
tlak	tlak	k1gInSc1
z	z	k7c2
brzdy	brzda	k1gFnSc2
pomalejšího	pomalý	k2eAgNnSc2d2
kola	kolo	k1gNnSc2
a	a	k8xC
ihned	ihned	k6eAd1
po	po	k7c6
jeho	jeho	k3xOp3gInSc6
roztočení	roztočení	k1gNnSc1
opět	opět	k6eAd1
tlak	tlak	k1gInSc1
napustí	napustit	k5eAaPmIp3nS
zpět	zpět	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tak	tak	k6eAd1
se	se	k3xPyFc4
brzdění	brzdění	k1gNnSc1
přibližuje	přibližovat	k5eAaImIp3nS
ideálu	ideál	k1gInSc3
na	na	k7c6
hranici	hranice	k1gFnSc6
adheze	adheze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
akci	akce	k1gFnSc4
jsou	být	k5eAaImIp3nP
systémy	systém	k1gInPc7
ABS	ABS	kA
schopné	schopný	k2eAgFnPc1d1
opakovat	opakovat	k5eAaImF
několikrát	několikrát	k6eAd1
za	za	k7c4
sekundu	sekunda	k1gFnSc4
a	a	k8xC
to	ten	k3xDgNnSc4
po	po	k7c4
celou	celý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
brzdění	brzdění	k1gNnSc2
až	až	k9
do	do	k7c2
minimální	minimální	k2eAgFnSc2d1
rychlosti	rychlost	k1gFnSc2
<g/>
,	,	kIx,
zpravidla	zpravidla	k6eAd1
4	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
systém	systém	k1gInSc1
ABS	ABS	kA
sám	sám	k3xTgMnSc1
odpojuje	odpojovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1
části	část	k1gFnPc1
systému	systém	k1gInSc2
ABS	ABS	kA
</s>
<s>
snímače	snímač	k1gInPc1
otáček	otáčka	k1gFnPc2
kol	kola	k1gFnPc2
(	(	kIx(
<g/>
induktivní	induktivní	k2eAgInSc1d1
snímač	snímač	k1gInSc1
a	a	k8xC
„	„	k?
<g/>
impulsní	impulsní	k2eAgInPc1d1
<g/>
“	“	k?
kroužky	kroužek	k1gInPc1
na	na	k7c6
nábojích	náboj	k1gInPc6
kol	kola	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
řídicí	řídicí	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
systému	systém	k1gInSc2
ABS	ABS	kA
</s>
<s>
elektrohydraulické	elektrohydraulický	k2eAgNnSc1d1
/	/	kIx~
elektropneumatické	elektropneumatický	k2eAgInPc1d1
řídící	řídící	k2eAgInPc1d1
ventily	ventil	k1gInPc1
</s>
<s>
Požadavky	požadavek	k1gInPc1
na	na	k7c4
ABS	ABS	kA
</s>
<s>
Brzdná	brzdný	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
</s>
<s>
regulace	regulace	k1gFnSc1
brzdné	brzdný	k2eAgFnSc2d1
síly	síla	k1gFnSc2
musí	muset	k5eAaImIp3nS
zajistit	zajistit	k5eAaPmF
stabilitu	stabilita	k1gFnSc4
a	a	k8xC
ovladatelnost	ovladatelnost	k1gFnSc4
vozidla	vozidlo	k1gNnSc2
při	při	k7c6
všech	všecek	k3xTgInPc6
stavech	stav	k1gInPc6
jízdní	jízdní	k2eAgFnPc1d1
dráhy	dráha	k1gFnPc1
od	od	k7c2
suché	suchý	k2eAgFnSc2d1
asfaltové	asfaltový	k2eAgFnSc2d1
vozovky	vozovka	k1gFnSc2
až	až	k9
po	po	k7c6
náledí	náledí	k1gNnSc6
</s>
<s>
regulace	regulace	k1gFnSc1
brzdné	brzdný	k2eAgFnSc2d1
síly	síla	k1gFnSc2
se	se	k3xPyFc4
musí	muset	k5eAaImIp3nS
rychle	rychle	k6eAd1
přizpůsobit	přizpůsobit	k5eAaPmF
změnám	změna	k1gFnPc3
adheze	adheze	k1gFnSc2
vozovky	vozovka	k1gFnSc2
</s>
<s>
musí	muset	k5eAaImIp3nS
zabránit	zabránit	k5eAaPmF
rozkývání	rozkývání	k1gNnSc4
vozidla	vozidlo	k1gNnSc2
</s>
<s>
systém	systém	k1gInSc1
ABS	ABS	kA
musí	muset	k5eAaImIp3nS
rozeznat	rozeznat	k5eAaPmF
aquaplaning	aquaplaning	k1gInSc4
a	a	k8xC
vhodně	vhodně	k6eAd1
na	na	k7c4
něj	on	k3xPp3gMnSc4
reagovat	reagovat	k5eAaBmF
</s>
<s>
bezpečnostní	bezpečnostní	k2eAgInPc1d1
systémy	systém	k1gInPc1
musí	muset	k5eAaImIp3nP
neustále	neustále	k6eAd1
kontrolovat	kontrolovat	k5eAaImF
bezchybnost	bezchybnost	k1gFnSc4
funkce	funkce	k1gFnSc2
ABS	ABS	kA
<g/>
;	;	kIx,
při	při	k7c6
zjištění	zjištění	k1gNnSc6
závady	závada	k1gFnSc2
systém	systém	k1gInSc4
vypnout	vypnout	k5eAaPmF
a	a	k8xC
o	o	k7c6
jeho	jeho	k3xOp3gFnSc6
nedostupnosti	nedostupnost	k1gFnSc6
informovat	informovat	k5eAaBmF
řidiče	řidič	k1gMnPc4
rozsvícením	rozsvícení	k1gNnSc7
kontrolky	kontrolka	k1gFnSc2
</s>
<s>
Rozdíly	rozdíl	k1gInPc1
v	v	k7c6
systémech	systém	k1gInPc6
ABS	ABS	kA
</s>
<s>
První	první	k4xOgInPc1
systémy	systém	k1gInPc1
byly	být	k5eAaImAgInP
čistě	čistě	k6eAd1
mechanické	mechanický	k2eAgInPc1d1
<g/>
,	,	kIx,
až	až	k9
později	pozdě	k6eAd2
byla	být	k5eAaImAgFnS
využita	využit	k2eAgFnSc1d1
elektronika	elektronika	k1gFnSc1
a	a	k8xC
její	její	k3xOp3gInSc1
bouřlivý	bouřlivý	k2eAgInSc1d1
rozvoj	rozvoj	k1gInSc1
umožnil	umožnit	k5eAaPmAgInS
podstatně	podstatně	k6eAd1
zkrátit	zkrátit	k5eAaPmF
reakční	reakční	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
a	a	k8xC
také	také	k9
celý	celý	k2eAgInSc4d1
systém	systém	k1gInSc4
rozměrově	rozměrově	k6eAd1
miniaturizovat	miniaturizovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Starší	starý	k2eAgInPc1d2
systémy	systém	k1gInPc1
ABS	ABS	kA
fungovaly	fungovat	k5eAaImAgInP
pouze	pouze	k6eAd1
na	na	k7c6
předních	přední	k2eAgNnPc6d1
kolech	kolo	k1gNnPc6
automobilu	automobil	k1gInSc2
<g/>
,	,	kIx,
při	při	k7c6
brzdění	brzdění	k1gNnSc6
více	hodně	k6eAd2
zatížených	zatížený	k2eAgInPc2d1
a	a	k8xC
důležitějších	důležitý	k2eAgInPc2d2
pro	pro	k7c4
zachování	zachování	k1gNnSc4
směru	směr	k1gInSc2
vozu	vůz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moderní	moderní	k2eAgInPc1d1
systémy	systém	k1gInPc1
ABS	ABS	kA
jsou	být	k5eAaImIp3nP
dnes	dnes	k6eAd1
použity	použít	k5eAaPmNgInP
na	na	k7c6
všech	všecek	k3xTgNnPc6
kolech	kolo	k1gNnPc6
automobilu	automobil	k1gInSc2
(	(	kIx(
<g/>
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
to	ten	k3xDgNnSc4
zda	zda	k8xS
jsou	být	k5eAaImIp3nP
na	na	k7c6
kole	kolo	k1gNnSc6
použity	použit	k2eAgFnPc1d1
kotoučové	kotoučový	k2eAgFnPc1d1
nebo	nebo	k8xC
bubnové	bubnový	k2eAgFnPc1d1
brzdy	brzda	k1gFnPc1
<g/>
)	)	kIx)
a	a	k8xC
systém	systém	k1gInSc1
funguje	fungovat	k5eAaImIp3nS
s	s	k7c7
mnohem	mnohem	k6eAd1
větší	veliký	k2eAgFnSc7d2
pracovní	pracovní	k2eAgFnSc7d1
frekvencí	frekvence	k1gFnSc7
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
je	být	k5eAaImIp3nS
stále	stále	k6eAd1
obtížnější	obtížný	k2eAgMnSc1d2
až	až	k9
nemožné	nemožná	k1gFnSc2
systému	systém	k1gInSc2
konkurovat	konkurovat	k5eAaImF
zkušenou	zkušená	k1gFnSc4
nohou	noha	k1gFnSc7
na	na	k7c6
brzdě	brzda	k1gFnSc6
bez	bez	k7c2
ABS	ABS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobně	podobně	k6eAd1
jako	jako	k8xC,k8xS
brzdový	brzdový	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
systém	systém	k1gInSc4
ABS	ABS	kA
vícekanálový	vícekanálový	k2eAgMnSc1d1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
každý	každý	k3xTgInSc1
kanál	kanál	k1gInSc1
reguluje	regulovat	k5eAaImIp3nS
brzdnou	brzdný	k2eAgFnSc4d1
sílu	síla	k1gFnSc4
jednoho	jeden	k4xCgMnSc2
nebo	nebo	k8xC
více	hodně	k6eAd2
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejdokonalejší	dokonalý	k2eAgInSc1d3
systém	systém	k1gInSc1
je	být	k5eAaImIp3nS
tedy	tedy	k9
ten	ten	k3xDgMnSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
počet	počet	k1gInSc1
kanálů	kanál	k1gInPc2
rovná	rovnat	k5eAaImIp3nS
počtu	počet	k1gInSc3
kol	kola	k1gFnPc2
vozu	vůz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
a	a	k8xC
rozšíření	rozšíření	k1gNnSc1
ABS	ABS	kA
</s>
<s>
Rozšířením	rozšíření	k1gNnSc7
systému	systém	k1gInSc2
ABS	ABS	kA
je	být	k5eAaImIp3nS
ASR	ASR	kA
a	a	k8xC
dalším	další	k2eAgNnSc7d1
rozšířením	rozšíření	k1gNnSc7
ASR	ASR	kA
je	být	k5eAaImIp3nS
ESP	ESP	kA
<g/>
.	.	kIx.
</s>
<s>
Pokračováním	pokračování	k1gNnSc7
vývoje	vývoj	k1gInSc2
je	být	k5eAaImIp3nS
i	i	k9
systém	systém	k1gInSc1
„	„	k?
<g/>
zvyšující	zvyšující	k2eAgInSc4d1
<g/>
“	“	k?
sílu	síla	k1gFnSc4
na	na	k7c4
brzdový	brzdový	k2eAgInSc4d1
pedál	pedál	k1gInSc4
–	–	k?
vychází	vycházet	k5eAaImIp3nS
se	se	k3xPyFc4
z	z	k7c2
poznatku	poznatek	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
ne	ne	k9
všichni	všechen	k3xTgMnPc1
řidiči	řidič	k1gMnPc1
v	v	k7c6
nebezpečí	nebezpečí	k1gNnSc6
sešlápnou	sešlápnout	k5eAaPmIp3nP
hned	hned	k6eAd1
brzdu	brzda	k1gFnSc4
naplno	naplno	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
za	za	k7c2
předpokladu	předpoklad	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
čidla	čidlo	k1gNnPc4
vyhodnotí	vyhodnotit	k5eAaPmIp3nS
situaci	situace	k1gFnSc4
vozu	vůz	k1gInSc2
a	a	k8xC
chování	chování	k1gNnSc2
řidiče	řidič	k1gInSc2
jako	jako	k8xC,k8xS
krizové	krizový	k2eAgNnSc4d1
brzdění	brzdění	k1gNnSc4
<g/>
,	,	kIx,
aplikuje	aplikovat	k5eAaBmIp3nS
systém	systém	k1gInSc1
okamžitě	okamžitě	k6eAd1
maximální	maximální	k2eAgNnSc4d1
brzdění	brzdění	k1gNnSc4
(	(	kIx(
<g/>
s	s	k7c7
funkcí	funkce	k1gFnSc7
ABS	ABS	kA
<g/>
)	)	kIx)
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
skutečnou	skutečný	k2eAgFnSc4d1
polohu	poloha	k1gFnSc4
pedálu	pedál	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
má	mít	k5eAaImIp3nS
eliminovat	eliminovat	k5eAaBmF
reakční	reakční	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
a	a	k8xC
přispět	přispět	k5eAaPmF
k	k	k7c3
včasnému	včasný	k2eAgNnSc3d1
zastavení	zastavení	k1gNnSc3
vozu	vůz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgInSc7d1
pochopitelným	pochopitelný	k2eAgInSc7d1
krokem	krok	k1gInSc7
(	(	kIx(
<g/>
nacházíte	nacházet	k5eAaImIp2nP
<g/>
-li	-li	k?
se	se	k3xPyFc4
za	za	k7c7
vozem	vůz	k1gInSc7
takto	takto	k6eAd1
vybaveným	vybavený	k2eAgMnPc3d1
<g/>
)	)	kIx)
pak	pak	k6eAd1
bude	být	k5eAaImBp3nS
vazba	vazba	k1gFnSc1
na	na	k7c4
čidla	čidlo	k1gNnPc4
vzdálenosti	vzdálenost	k1gFnSc2
od	od	k7c2
překážky	překážka	k1gFnSc2
před	před	k7c7
vozem	vůz	k1gInSc7
atd.	atd.	kA
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
ASR	ASR	kA
</s>
<s>
ESP	ESP	kA
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
ABS	ABS	kA
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
ABS	ABS	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Vysvětlení	vysvětlení	k1gNnSc1
systému	systém	k1gInSc2
ABS	ABS	kA
pomocí	pomocí	k7c2
diagramů	diagram	k1gInPc2
a	a	k8xC
videí	video	k1gNnPc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Podrobně	podrobně	k6eAd1
o	o	k7c6
ABS	ABS	kA
</s>
