<s>
Organizace	organizace	k1gFnSc1	organizace
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
,	,	kIx,	,
vědu	věda	k1gFnSc4	věda
a	a	k8xC	a
kulturu	kultura	k1gFnSc4	kultura
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
United	United	k1gMnSc1	United
Nations	Nationsa	k1gFnPc2	Nationsa
Educational	Educational	k1gMnSc1	Educational
<g/>
,	,	kIx,	,
Scientific	Scientific	k1gMnSc1	Scientific
and	and	k?	and
Cultural	Cultural	k1gFnSc1	Cultural
Organization	Organization	k1gInSc1	Organization
<g/>
,	,	kIx,	,
UNESCO	Unesco	k1gNnSc1	Unesco
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
15	[number]	k4	15
mezistátních	mezistátní	k2eAgFnPc2d1	mezistátní
odborných	odborný	k2eAgFnPc2d1	odborná
organizací	organizace	k1gFnPc2	organizace
(	(	kIx(	(
<g/>
agentur	agentura	k1gFnPc2	agentura
<g/>
)	)	kIx)	)
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
