<s>
Plíce	plíce	k1gFnSc1	plíce
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
pulmo	pulmo	k6eAd1	pulmo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
též	též	k9	též
pneumo	pneumo	k6eAd1	pneumo
z	z	k7c2	z
řec.	řec.	k?	řec.
π	π	k?	π
-	-	kIx~	-
dech	dech	k1gInSc1	dech
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
párový	párový	k2eAgInSc1d1	párový
orgán	orgán	k1gInSc1	orgán
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
výměnu	výměna	k1gFnSc4	výměna
plynů	plyn	k1gInPc2	plyn
mezi	mezi	k7c7	mezi
krví	krev	k1gFnSc7	krev
a	a	k8xC	a
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Plíce	plíce	k1gFnSc1	plíce
savců	savec	k1gMnPc2	savec
včetně	včetně	k7c2	včetně
člověka	člověk	k1gMnSc2	člověk
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
miliónů	milión	k4xCgInPc2	milión
tenkostěnných	tenkostěnný	k2eAgInPc2d1	tenkostěnný
váčků	váček	k1gInPc2	váček
<g/>
,	,	kIx,	,
plicních	plicní	k2eAgInPc2d1	plicní
sklípků	sklípek	k1gInPc2	sklípek
<g/>
,	,	kIx,	,
alveol	alveoly	k1gInPc2	alveoly
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
opředeny	opříst	k5eAaPmNgInP	opříst
krevními	krevní	k2eAgFnPc7d1	krevní
kapilárami	kapilára	k1gFnPc7	kapilára
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
alveol	alveoly	k1gInPc2	alveoly
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
vzduch	vzduch	k1gInSc1	vzduch
<g/>
,	,	kIx,	,
kyslík	kyslík	k1gInSc1	kyslík
difunduje	difundovat	k5eAaImIp3nS	difundovat
do	do	k7c2	do
krve	krev	k1gFnSc2	krev
v	v	k7c6	v
kapilárách	kapilára	k1gFnPc6	kapilára
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
se	se	k3xPyFc4	se
z	z	k7c2	z
alveol	alveoly	k1gInPc2	alveoly
při	při	k7c6	při
výdechu	výdech	k1gInSc6	výdech
dostává	dostávat	k5eAaImIp3nS	dostávat
z	z	k7c2	z
těla	tělo	k1gNnSc2	tělo
ven	ven	k6eAd1	ven
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepším	dobrý	k2eAgInSc7d3	nejlepší
zdrojem	zdroj	k1gInSc7	zdroj
energie	energie	k1gFnSc2	energie
pro	pro	k7c4	pro
buňku	buňka	k1gFnSc4	buňka
je	být	k5eAaImIp3nS	být
aerobní	aerobní	k2eAgFnSc1d1	aerobní
oxidace	oxidace	k1gFnSc1	oxidace
glukózy	glukóza	k1gFnSc2	glukóza
<g/>
.	.	kIx.	.
</s>
<s>
Glukóza	glukóza	k1gFnSc1	glukóza
je	být	k5eAaImIp3nS	být
sérií	série	k1gFnSc7	série
metabolických	metabolický	k2eAgInPc2d1	metabolický
dějů	děj	k1gInPc2	děj
přeměněna	přeměněn	k2eAgFnSc1d1	přeměněna
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgInSc4d1	uhličitý
a	a	k8xC	a
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
buňka	buňka	k1gFnSc1	buňka
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
k	k	k7c3	k
životu	život	k1gInSc3	život
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
ale	ale	k9	ale
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
kyslík	kyslík	k1gInSc1	kyslík
<g/>
,	,	kIx,	,
a	a	k8xC	a
musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
nějak	nějak	k6eAd1	nějak
zbavit	zbavit	k5eAaPmF	zbavit
odpadního	odpadní	k2eAgInSc2d1	odpadní
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jednobuněčných	jednobuněčný	k2eAgInPc2d1	jednobuněčný
i	i	k8xC	i
malých	malý	k2eAgInPc2d1	malý
a	a	k8xC	a
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
mnohobuněčných	mnohobuněčný	k2eAgInPc2d1	mnohobuněčný
organismů	organismus	k1gInPc2	organismus
může	moct	k5eAaImIp3nS	moct
docházet	docházet	k5eAaImF	docházet
k	k	k7c3	k
difúzi	difúze	k1gFnSc3	difúze
plynů	plyn	k1gInPc2	plyn
povrchem	povrch	k1gInSc7	povrch
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
živočichové	živočich	k1gMnPc1	živočich
tedy	tedy	k9	tedy
nepotřebují	potřebovat	k5eNaImIp3nP	potřebovat
žádné	žádný	k3yNgInPc1	žádný
dýchací	dýchací	k2eAgInPc1d1	dýchací
orgány	orgán	k1gInPc1	orgán
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ostatních	ostatní	k2eAgMnPc2d1	ostatní
živočichů	živočich	k1gMnPc2	živočich
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
složitější	složitý	k2eAgNnSc1d2	složitější
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
dýchají	dýchat	k5eAaImIp3nP	dýchat
vzduch	vzduch	k1gInSc4	vzduch
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vzduch	vzduch	k1gInSc1	vzduch
vdechován	vdechovat	k5eAaImNgInS	vdechovat
nosem	nos	k1gInSc7	nos
<g/>
,	,	kIx,	,
prochází	procházet	k5eAaImIp3nS	procházet
horními	horní	k2eAgFnPc7d1	horní
a	a	k8xC	a
dolními	dolní	k2eAgFnPc7d1	dolní
dýchacími	dýchací	k2eAgFnPc7d1	dýchací
cestami	cesta	k1gFnPc7	cesta
až	až	k6eAd1	až
do	do	k7c2	do
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Dýchací	dýchací	k2eAgFnPc1d1	dýchací
cesty	cesta	k1gFnPc1	cesta
přechází	přecházet	k5eAaImIp3nP	přecházet
do	do	k7c2	do
dýchacího	dýchací	k2eAgInSc2d1	dýchací
oddílu	oddíl	k1gInSc2	oddíl
plic	plíce	k1gFnPc2	plíce
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
hlavně	hlavně	k9	hlavně
alveoly	alveol	k1gInPc4	alveol
<g/>
.	.	kIx.	.
</s>
<s>
Dýchací	dýchací	k2eAgInPc1d1	dýchací
plyny	plyn	k1gInPc1	plyn
difundují	difundovat	k5eAaImIp3nP	difundovat
přes	přes	k7c4	přes
alveolární	alveolární	k2eAgFnSc4d1	alveolární
membránu	membrána	k1gFnSc4	membrána
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
tlakových	tlakový	k2eAgInPc2d1	tlakový
a	a	k8xC	a
koncentračních	koncentrační	k2eAgInPc2d1	koncentrační
gradientů	gradient	k1gInPc2	gradient
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
přechod	přechod	k1gInSc1	přechod
plynů	plyn	k1gInPc2	plyn
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
parciálním	parciální	k2eAgInSc6d1	parciální
tlaku	tlak	k1gInSc6	tlak
těchto	tento	k3xDgInPc2	tento
plynů	plyn	k1gInPc2	plyn
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
a	a	k8xC	a
na	na	k7c6	na
parciálním	parciální	k2eAgInSc6d1	parciální
tlaku	tlak	k1gInSc6	tlak
plynů	plyn	k1gInPc2	plyn
v	v	k7c6	v
neokysličené	okysličený	k2eNgFnSc6d1	neokysličená
krvi	krev	k1gFnSc6	krev
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
do	do	k7c2	do
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
nadmořských	nadmořský	k2eAgFnPc6d1	nadmořská
výškách	výška	k1gFnPc6	výška
je	být	k5eAaImIp3nS	být
parciální	parciální	k2eAgInSc1d1	parciální
tlak	tlak	k1gInSc1	tlak
kyslíku	kyslík	k1gInSc2	kyslík
menší	malý	k2eAgMnSc1d2	menší
než	než	k8xS	než
v	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
důvod	důvod	k1gInSc1	důvod
namáhavějšího	namáhavý	k2eAgNnSc2d2	namáhavější
dýchání	dýchání	k1gNnSc2	dýchání
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
nadmořských	nadmořský	k2eAgFnPc6d1	nadmořská
výškách	výška	k1gFnPc6	výška
<g/>
.	.	kIx.	.
</s>
<s>
Plíce	plíce	k1gFnPc1	plíce
savců	savec	k1gMnPc2	savec
jsou	být	k5eAaImIp3nP	být
párové	párový	k2eAgInPc1d1	párový
orgány	orgán	k1gInPc1	orgán
uložené	uložený	k2eAgInPc1d1	uložený
v	v	k7c6	v
hrudní	hrudní	k2eAgFnSc6d1	hrudní
dutině	dutina	k1gFnSc6	dutina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
jsou	být	k5eAaImIp3nP	být
hladké	hladký	k2eAgFnPc1d1	hladká
<g/>
,	,	kIx,	,
zdravé	zdravý	k2eAgFnPc1d1	zdravá
plíce	plíce	k1gFnPc1	plíce
jsou	být	k5eAaImIp3nP	být
narůžovělé	narůžovělý	k2eAgFnPc1d1	narůžovělá
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
barva	barva	k1gFnSc1	barva
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
pracovaly	pracovat	k5eAaImAgInP	pracovat
<g/>
,	,	kIx,	,
plíce	plíce	k1gFnPc1	plíce
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
i	i	k9	i
psů	pes	k1gMnPc2	pes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
žijí	žít	k5eAaImIp3nP	žít
ve	v	k7c6	v
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zašedlé	zašedlý	k2eAgFnPc1d1	zašedlá
<g/>
,	,	kIx,	,
plíce	plíce	k1gFnPc1	plíce
kuřáků	kuřák	k1gMnPc2	kuřák
jsou	být	k5eAaImIp3nP	být
černé	černý	k2eAgFnPc1d1	černá
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejich	jejich	k3xOp3gInSc6	jejich
povrchu	povrch	k1gInSc6	povrch
jsou	být	k5eAaImIp3nP	být
vidět	vidět	k5eAaImF	vidět
dehtové	dehtový	k2eAgFnPc4d1	dehtová
skvrny	skvrna	k1gFnPc4	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Plíce	plíce	k1gFnPc1	plíce
mají	mít	k5eAaImIp3nP	mít
houbovitou	houbovitý	k2eAgFnSc4d1	houbovitá
konzistenci	konzistence	k1gFnSc4	konzistence
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
po	po	k7c6	po
prvním	první	k4xOgInSc6	první
nádechu	nádech	k1gInSc6	nádech
se	se	k3xPyFc4	se
naplní	naplnit	k5eAaPmIp3nS	naplnit
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
,	,	kIx,	,
část	část	k1gFnSc4	část
tohoto	tento	k3xDgInSc2	tento
vzduchu	vzduch	k1gInSc2	vzduch
se	se	k3xPyFc4	se
už	už	k6eAd1	už
nikdy	nikdy	k6eAd1	nikdy
nedostane	dostat	k5eNaPmIp3nS	dostat
z	z	k7c2	z
plic	plíce	k1gFnPc2	plíce
ven	ven	k6eAd1	ven
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
plíce	plíce	k1gFnPc1	plíce
živě	živě	k6eAd1	živě
narozených	narozený	k2eAgFnPc2d1	narozená
dětí	dítě	k1gFnPc2	dítě
(	(	kIx(	(
<g/>
i	i	k8xC	i
mláďat	mládě	k1gNnPc2	mládě
ostatních	ostatní	k2eAgMnPc2d1	ostatní
savců	savec	k1gMnPc2	savec
<g/>
)	)	kIx)	)
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
plavou	plavat	k5eAaImIp3nP	plavat
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
plic	plíce	k1gFnPc2	plíce
mrtvě	mrtvě	k6eAd1	mrtvě
narozených	narozený	k2eAgFnPc2d1	narozená
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
klesnou	klesnout	k5eAaPmIp3nP	klesnout
ke	k	k7c3	k
dnu	dno	k1gNnSc3	dno
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
skutečnosti	skutečnost	k1gFnSc3	skutečnost
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
v	v	k7c6	v
soudním	soudní	k2eAgNnSc6d1	soudní
lékařství	lékařství	k1gNnSc6	lékařství
<g/>
.	.	kIx.	.
</s>
<s>
Hrudní	hrudní	k2eAgFnSc1d1	hrudní
dutina	dutina	k1gFnSc1	dutina
je	být	k5eAaImIp3nS	být
vystlána	vystlat	k5eAaPmNgFnS	vystlat
pleurou	pleura	k1gFnSc7	pleura
<g/>
,	,	kIx,	,
tenkou	tenký	k2eAgFnSc7d1	tenká
vazivovou	vazivový	k2eAgFnSc7d1	vazivová
blánou	blána	k1gFnSc7	blána
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
hrudní	hrudní	k2eAgFnSc4d1	hrudní
stěnu	stěna	k1gFnSc4	stěna
a	a	k8xC	a
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
pohrudnice	pohrudnice	k1gFnSc1	pohrudnice
(	(	kIx(	(
<g/>
parietální	parietální	k2eAgFnSc1d1	parietální
pleura	pleura	k1gFnSc1	pleura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pleura	pleura	k1gFnSc1	pleura
přechází	přecházet	k5eAaImIp3nS	přecházet
na	na	k7c4	na
plíce	plíce	k1gFnPc4	plíce
a	a	k8xC	a
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
je	on	k3xPp3gFnPc4	on
jako	jako	k9	jako
poplicnice	poplicnice	k1gFnSc1	poplicnice
(	(	kIx(	(
<g/>
viscerální	viscerální	k2eAgFnSc1d1	viscerální
pleura	pleura	k1gFnSc1	pleura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
pohrudnicí	pohrudnice	k1gFnSc7	pohrudnice
a	a	k8xC	a
poplicnicí	poplicnice	k1gFnSc7	poplicnice
je	být	k5eAaImIp3nS	být
úzký	úzký	k2eAgInSc4d1	úzký
prostor	prostor	k1gInSc4	prostor
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc4	zbytek
tělní	tělní	k2eAgFnSc2d1	tělní
dutiny	dutina	k1gFnSc2	dutina
coelomu	coelom	k1gInSc2	coelom
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
vyplněn	vyplněn	k2eAgInSc1d1	vyplněn
malým	malý	k2eAgNnSc7d1	malé
množstvím	množství	k1gNnSc7	množství
tekutiny	tekutina	k1gFnSc2	tekutina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
hladký	hladký	k2eAgInSc4d1	hladký
pohyb	pohyb	k1gInSc4	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pohrudniční	pohrudniční	k2eAgFnSc6d1	pohrudniční
dutině	dutina	k1gFnSc6	dutina
je	být	k5eAaImIp3nS	být
také	také	k9	také
podtlak	podtlak	k1gInSc1	podtlak
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
kolapsu	kolaps	k1gInSc3	kolaps
měkkých	měkký	k2eAgFnPc2d1	měkká
plic	plíce	k1gFnPc2	plíce
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
jejich	jejich	k3xOp3gNnSc1	jejich
naplnění	naplnění	k1gNnSc1	naplnění
vzduchem	vzduch	k1gInSc7	vzduch
při	při	k7c6	při
nádechu	nádech	k1gInSc6	nádech
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
proděravění	proděravění	k1gNnSc6	proděravění
pohrudnice	pohrudnice	k1gFnSc2	pohrudnice
se	se	k3xPyFc4	se
tlaky	tlak	k1gInPc7	tlak
v	v	k7c6	v
pleurální	pleurální	k2eAgFnSc6d1	pleurální
dutině	dutina	k1gFnSc6	dutina
vyrovnají	vyrovnat	k5eAaBmIp3nP	vyrovnat
a	a	k8xC	a
plíce	plíce	k1gFnPc4	plíce
zkolabuje	zkolabovat	k5eAaPmIp3nS	zkolabovat
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
pneumothorax	pneumothorax	k1gInSc1	pneumothorax
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
plícemi	plíce	k1gFnPc7	plíce
je	být	k5eAaImIp3nS	být
pleurou	pleura	k1gFnSc7	pleura
obalený	obalený	k2eAgInSc1d1	obalený
prostor	prostor	k1gInSc1	prostor
<g/>
,	,	kIx,	,
nazývané	nazývaný	k2eAgNnSc1d1	nazývané
středohrudí	středohrudí	k1gNnSc1	středohrudí
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
je	být	k5eAaImIp3nS	být
uloženo	uložen	k2eAgNnSc1d1	uloženo
srdce	srdce	k1gNnSc1	srdce
a	a	k8xC	a
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
procházejí	procházet	k5eAaImIp3nP	procházet
důležité	důležitý	k2eAgFnPc4d1	důležitá
cévy	céva	k1gFnPc4	céva
<g/>
,	,	kIx,	,
nervy	nerv	k1gInPc4	nerv
a	a	k8xC	a
jícen	jícen	k1gInSc1	jícen
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
plíce	plíce	k1gFnSc1	plíce
má	mít	k5eAaImIp3nS	mít
zhruba	zhruba	k6eAd1	zhruba
trojúhelníkovitý	trojúhelníkovitý	k2eAgInSc1d1	trojúhelníkovitý
tvar	tvar	k1gInSc1	tvar
<g/>
,	,	kIx,	,
levá	levý	k2eAgFnSc1d1	levá
plíce	plíce	k1gFnSc1	plíce
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
pravá	pravý	k2eAgFnSc1d1	pravá
<g/>
.	.	kIx.	.
</s>
<s>
Plicní	plicní	k2eAgFnSc1d1	plicní
základna	základna	k1gFnSc1	základna
(	(	kIx(	(
<g/>
basis	basis	k?	basis
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
hlavního	hlavní	k2eAgInSc2d1	hlavní
dýchacího	dýchací	k2eAgInSc2d1	dýchací
svalu	sval	k1gInSc2	sval
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
bránice	bránice	k1gFnPc1	bránice
<g/>
.	.	kIx.	.
</s>
<s>
Plicní	plicní	k2eAgInSc1d1	plicní
hrot	hrot	k1gInSc1	hrot
(	(	kIx(	(
<g/>
apex	apex	k1gInSc1	apex
<g/>
)	)	kIx)	)
směřuje	směřovat	k5eAaImIp3nS	směřovat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
hlavě	hlava	k1gFnSc3	hlava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
plicní	plicní	k2eAgFnSc6d1	plicní
brance	branka	k1gFnSc6	branka
(	(	kIx(	(
<g/>
hilus	hilus	k1gInSc1	hilus
<g/>
)	)	kIx)	)
do	do	k7c2	do
plíce	plíce	k1gFnSc2	plíce
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
průduška	průduška	k1gFnSc1	průduška
(	(	kIx(	(
<g/>
bronchus	bronchus	k1gInSc1	bronchus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plicní	plicní	k2eAgFnSc2d1	plicní
žíly	žíla	k1gFnSc2	žíla
a	a	k8xC	a
plicní	plicní	k2eAgFnSc1d1	plicní
tepna	tepna	k1gFnSc1	tepna
<g/>
,	,	kIx,	,
mízní	mízní	k2eAgFnPc4d1	mízní
žíly	žíla	k1gFnPc4	žíla
a	a	k8xC	a
nervy	nerv	k1gInPc4	nerv
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
ploše	plocha	k1gFnSc6	plocha
plic	plíce	k1gFnPc2	plíce
jsou	být	k5eAaImIp3nP	být
otisky	otisk	k1gInPc4	otisk
orgánů	orgán	k1gInPc2	orgán
středohrudí	středohruď	k1gFnPc2	středohruď
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
srdce	srdce	k1gNnSc1	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Plíce	plíce	k1gFnPc1	plíce
jsou	být	k5eAaImIp3nP	být
rozčleněné	rozčleněný	k2eAgInPc4d1	rozčleněný
na	na	k7c4	na
laloky	lalok	k1gInPc4	lalok
<g/>
,	,	kIx,	,
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
má	mít	k5eAaImIp3nS	mít
pravá	pravý	k2eAgFnSc1d1	pravá
plíce	plíce	k1gFnSc1	plíce
laloky	lalok	k1gInPc1	lalok
tři	tři	k4xCgFnPc4	tři
<g/>
,	,	kIx,	,
menší	malý	k2eAgFnSc1d2	menší
levá	levá	k1gFnSc1	levá
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc1	třetí
lalok	lalok	k1gInSc1	lalok
pravé	pravý	k2eAgFnSc2d1	pravá
plíce	plíce	k1gFnSc2	plíce
<g/>
,	,	kIx,	,
lalok	lalok	k1gInSc1	lalok
přídatný	přídatný	k2eAgInSc1d1	přídatný
(	(	kIx(	(
<g/>
lobus	lobus	k1gInSc1	lobus
accessorius	accessorius	k1gInSc1	accessorius
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vsouvá	vsouvat	k5eAaImIp3nS	vsouvat
do	do	k7c2	do
prostoru	prostor	k1gInSc2	prostor
mezi	mezi	k7c7	mezi
velkými	velký	k2eAgFnPc7d1	velká
žílami	žíla	k1gFnPc7	žíla
a	a	k8xC	a
srdcem	srdce	k1gNnSc7	srdce
<g/>
,	,	kIx,	,
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
má	mít	k5eAaImIp3nS	mít
jazýčkovitý	jazýčkovitý	k2eAgInSc4d1	jazýčkovitý
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
také	také	k9	také
říká	říkat	k5eAaImIp3nS	říkat
lingula	lingula	k1gFnSc1	lingula
<g/>
,	,	kIx,	,
jazýček	jazýček	k1gInSc1	jazýček
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průměrných	průměrný	k2eAgFnPc6d1	průměrná
plicích	plíce	k1gFnPc6	plíce
se	se	k3xPyFc4	se
udrží	udržet	k5eAaPmIp3nS	udržet
okolo	okolo	k7c2	okolo
3	[number]	k4	3
litrů	litr	k1gInPc2	litr
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
lalok	lalok	k1gInSc1	lalok
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
vazivovými	vazivový	k2eAgFnPc7d1	vazivová
přepážkami	přepážka	k1gFnPc7	přepážka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vycházejí	vycházet	k5eAaImIp3nP	vycházet
z	z	k7c2	z
poplicnice	poplicnice	k1gFnSc2	poplicnice
<g/>
,	,	kIx,	,
na	na	k7c4	na
bronchopulmonální	bronchopulmonální	k2eAgInPc4d1	bronchopulmonální
segmenty	segment	k1gInPc4	segment
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
segment	segment	k1gInSc1	segment
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgInSc4d1	vlastní
přívod	přívod	k1gInSc4	přívod
vzduchu	vzduch	k1gInSc2	vzduch
i	i	k8xC	i
krvení	krvení	k1gNnSc2	krvení
<g/>
.	.	kIx.	.
</s>
<s>
Segmenty	segment	k1gInPc1	segment
jsou	být	k5eAaImIp3nP	být
dále	daleko	k6eAd2	daleko
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
na	na	k7c4	na
lalůčky	lalůček	k1gInPc4	lalůček
<g/>
,	,	kIx,	,
aciny	acina	k1gFnPc4	acina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
pyramidový	pyramidový	k2eAgInSc4d1	pyramidový
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Základna	základna	k1gFnSc1	základna
acinu	acin	k1gInSc2	acin
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
plíce	plíce	k1gFnPc1	plíce
<g/>
,	,	kIx,	,
hrot	hrot	k1gInSc1	hrot
směřuje	směřovat	k5eAaImIp3nS	směřovat
k	k	k7c3	k
plicní	plicní	k2eAgFnSc3d1	plicní
brance	branka	k1gFnSc3	branka
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřek	vnitřek	k1gInSc1	vnitřek
plic	plíce	k1gFnPc2	plíce
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
rozvětvujících	rozvětvující	k2eAgFnPc2d1	rozvětvující
se	se	k3xPyFc4	se
průdušek	průduška	k1gFnPc2	průduška
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
dolních	dolní	k2eAgFnPc2d1	dolní
cest	cesta	k1gFnPc2	cesta
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
<g/>
,	,	kIx,	,
a	a	k8xC	a
dýchacího	dýchací	k2eAgInSc2d1	dýchací
oddílu	oddíl	k1gInSc2	oddíl
<g/>
,	,	kIx,	,
části	část	k1gFnSc2	část
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
výměně	výměna	k1gFnSc3	výměna
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Průduška	průduška	k1gFnSc1	průduška
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
do	do	k7c2	do
plic	plíce	k1gFnPc2	plíce
plicní	plicní	k2eAgFnSc7d1	plicní
brankou	branka	k1gFnSc7	branka
<g/>
,	,	kIx,	,
se	s	k7c7	s
25	[number]	k4	25
<g/>
krát	krát	k6eAd1	krát
větví	větvit	k5eAaImIp3nS	větvit
(	(	kIx(	(
<g/>
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nS	tvořit
tak	tak	k9	tak
průduškový	průduškový	k2eAgInSc1d1	průduškový
strom	strom	k1gInSc1	strom
(	(	kIx(	(
<g/>
arbor	arbor	k1gInSc1	arbor
bronchalis	bronchalis	k1gFnSc2	bronchalis
<g/>
)	)	kIx)	)
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
průduška	průduška	k1gFnSc1	průduška
(	(	kIx(	(
<g/>
bronchus	bronchus	k1gInSc1	bronchus
principalis	principalis	k1gFnSc2	principalis
<g/>
)	)	kIx)	)
→	→	k?	→
lalokové	lalokový	k2eAgFnSc2d1	laloková
průdušky	průduška	k1gFnSc2	průduška
(	(	kIx(	(
<g/>
bronchi	bronchit	k5eAaPmRp2nS	bronchit
lobares	lobares	k1gInSc1	lobares
<g/>
)	)	kIx)	)
→	→	k?	→
segmentální	segmentální	k2eAgFnSc2d1	segmentální
průdušky	průduška	k1gFnSc2	průduška
(	(	kIx(	(
<g/>
bronchi	bronchit	k5eAaImRp2nS	bronchit
segmentales	segmentales	k1gInSc1	segmentales
<g/>
)	)	kIx)	)
→	→	k?	→
subsegmentální	subsegmentální	k2eAgFnSc2d1	subsegmentální
průdušky	průduška	k1gFnSc2	průduška
→	→	k?	→
terminální	terminální	k2eAgFnSc2d1	terminální
průdušky	průduška	k1gFnSc2	průduška
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
větvení	větvení	k1gNnPc2	větvení
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
stavba	stavba	k1gFnSc1	stavba
stěny	stěna	k1gFnSc2	stěna
průdušek	průduška	k1gFnPc2	průduška
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc1d1	hlavní
průduška	průduška	k1gFnSc1	průduška
má	mít	k5eAaImIp3nS	mít
podobnou	podobný	k2eAgFnSc4d1	podobná
stavbu	stavba	k1gFnSc4	stavba
stěny	stěna	k1gFnSc2	stěna
jako	jako	k8xC	jako
průdušnice	průdušnice	k1gFnSc2	průdušnice
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
chrupavkou	chrupavka	k1gFnSc7	chrupavka
<g/>
,	,	kIx,	,
s	s	k7c7	s
každým	každý	k3xTgNnSc7	každý
dalším	další	k2eAgNnSc7d1	další
větvením	větvení	k1gNnSc7	větvení
chrupavky	chrupavka	k1gFnSc2	chrupavka
ubývá	ubývat	k5eAaImIp3nS	ubývat
a	a	k8xC	a
relativně	relativně	k6eAd1	relativně
přibývá	přibývat	k5eAaImIp3nS	přibývat
hladké	hladký	k2eAgFnPc4d1	hladká
svaloviny	svalovina	k1gFnPc4	svalovina
<g/>
.	.	kIx.	.
</s>
<s>
Sliznice	sliznice	k1gFnSc1	sliznice
je	být	k5eAaImIp3nS	být
pokrytá	pokrytý	k2eAgNnPc4d1	pokryté
řasinkovým	řasinkový	k2eAgInSc7d1	řasinkový
epitelem	epitel	k1gInSc7	epitel
<g/>
,	,	kIx,	,
řasinky	řasinka	k1gFnPc1	řasinka
kmitají	kmitat	k5eAaImIp3nP	kmitat
směrem	směr	k1gInSc7	směr
ven	ven	k6eAd1	ven
a	a	k8xC	a
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
odstraňovat	odstraňovat	k5eAaImF	odstraňovat
nečistoty	nečistota	k1gFnPc1	nečistota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
větvení	větvení	k1gNnPc2	větvení
časem	časem	k6eAd1	časem
zmizí	zmizet	k5eAaPmIp3nS	zmizet
i	i	k9	i
hladká	hladký	k2eAgFnSc1d1	hladká
svalovina	svalovina	k1gFnSc1	svalovina
<g/>
,	,	kIx,	,
stěna	stěna	k1gFnSc1	stěna
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
jen	jen	k9	jen
tenkou	tenký	k2eAgFnSc7d1	tenká
sliznicí	sliznice	k1gFnSc7	sliznice
<g/>
,	,	kIx,	,
s	s	k7c7	s
epitelem	epitel	k1gInSc7	epitel
s	s	k7c7	s
řasinkovými	řasinkový	k2eAgFnPc7d1	řasinkový
buňkami	buňka	k1gFnPc7	buňka
<g/>
,	,	kIx,	,
malým	malý	k2eAgNnSc7d1	malé
množstvím	množství	k1gNnSc7	množství
pohárkových	pohárkový	k2eAgFnPc2d1	pohárkový
buněk	buňka	k1gFnPc2	buňka
a	a	k8xC	a
buňkami	buňka	k1gFnPc7	buňka
Clarovými	Clarův	k2eAgInPc7d1	Clarův
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
produkují	produkovat	k5eAaImIp3nP	produkovat
surfaktant	surfaktant	k1gMnSc1	surfaktant
<g/>
,	,	kIx,	,
látku	látka	k1gFnSc4	látka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
snižuje	snižovat	k5eAaImIp3nS	snižovat
povrchové	povrchový	k2eAgNnSc4d1	povrchové
napětí	napětí	k1gNnSc4	napětí
a	a	k8xC	a
brání	bránit	k5eAaImIp3nS	bránit
tak	tak	k6eAd1	tak
kolapsu	kolaps	k1gInSc3	kolaps
průdušky	průduška	k1gFnSc2	průduška
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
terminální	terminální	k2eAgFnSc1d1	terminální
průduška	průduška	k1gFnSc1	průduška
ventiluje	ventilovat	k5eAaImIp3nS	ventilovat
jeden	jeden	k4xCgInSc1	jeden
plicní	plicní	k2eAgInSc1d1	plicní
lalůček	lalůček	k1gInSc1	lalůček
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
se	se	k3xPyFc4	se
větví	větvit	k5eAaImIp3nS	větvit
na	na	k7c4	na
průdušinky	průdušinka	k1gFnPc4	průdušinka
(	(	kIx(	(
<g/>
bronchioli	bronchioli	k6eAd1	bronchioli
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
už	už	k6eAd1	už
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
dýchacímu	dýchací	k2eAgInSc3d1	dýchací
oddílu	oddíl	k1gInSc3	oddíl
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Průdušinky	průdušinka	k1gFnPc1	průdušinka
se	se	k3xPyFc4	se
několikrát	několikrát	k6eAd1	několikrát
rozvětví	rozvětvit	k5eAaPmIp3nS	rozvětvit
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c4	na
respirační	respirační	k2eAgFnPc4d1	respirační
průdušinky	průdušinka	k1gFnPc4	průdušinka
(	(	kIx(	(
<g/>
bronchioli	bronchioli	k6eAd1	bronchioli
respiratorii	respiratorie	k1gFnSc3	respiratorie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
průdušinky	průdušinka	k1gFnPc1	průdušinka
ústí	ústit	k5eAaImIp3nP	ústit
do	do	k7c2	do
plicních	plicní	k2eAgInPc2d1	plicní
sklípků	sklípek	k1gInPc2	sklípek
<g/>
,	,	kIx,	,
alveolů	alveol	k1gInPc2	alveol
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
respirační	respirační	k2eAgInSc1d1	respirační
bronchiolus	bronchiolus	k1gInSc1	bronchiolus
zásobuje	zásobovat	k5eAaImIp3nS	zásobovat
vzduchem	vzduch	k1gInSc7	vzduch
asi	asi	k9	asi
200	[number]	k4	200
alveolů	alveol	k1gInPc2	alveol
<g/>
.	.	kIx.	.
</s>
<s>
Alveoly	alveol	k1gInPc1	alveol
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
plicní	plicní	k2eAgInPc1d1	plicní
sklípky	sklípek	k1gInPc1	sklípek
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
tenkostěnné	tenkostěnný	k2eAgInPc4d1	tenkostěnný
váčky	váček	k1gInPc4	váček
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
probíhá	probíhat	k5eAaImIp3nS	probíhat
difúze	difúze	k1gFnSc1	difúze
dýchacích	dýchací	k2eAgInPc2d1	dýchací
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
stěna	stěna	k1gFnSc1	stěna
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
pouze	pouze	k6eAd1	pouze
jednou	jeden	k4xCgFnSc7	jeden
vrstvou	vrstva	k1gFnSc7	vrstva
extrémně	extrémně	k6eAd1	extrémně
tenkých	tenký	k2eAgFnPc2d1	tenká
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
pneumocytů	pneumocyt	k1gInPc2	pneumocyt
I.	I.	kA	I.
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Bazální	bazální	k2eAgFnSc1d1	bazální
membrána	membrána	k1gFnSc1	membrána
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yQgFnSc4	který
pneumocyty	pneumocyt	k1gInPc4	pneumocyt
nasedají	nasedat	k5eAaImIp3nP	nasedat
<g/>
,	,	kIx,	,
přechází	přecházet	k5eAaImIp3nS	přecházet
ve	v	k7c4	v
stěnu	stěna	k1gFnSc4	stěna
okolních	okolní	k2eAgFnPc2d1	okolní
kapilár	kapilára	k1gFnPc2	kapilára
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohly	moct	k5eAaImAgInP	moct
plyny	plyn	k1gInPc1	plyn
volně	volně	k6eAd1	volně
přecházet	přecházet	k5eAaImF	přecházet
<g/>
.	.	kIx.	.
</s>
<s>
Alveolární	alveolární	k2eAgFnSc1d1	alveolární
membrána	membrána	k1gFnSc1	membrána
je	být	k5eAaImIp3nS	být
tlustá	tlustý	k2eAgFnSc1d1	tlustá
jen	jen	k9	jen
asi	asi	k9	asi
1	[number]	k4	1
μ	μ	k?	μ
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
pneumocytů	pneumocyt	k1gInPc2	pneumocyt
I.	I.	kA	I.
typu	typ	k1gInSc6	typ
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
stěně	stěna	k1gFnSc6	stěna
alveolů	alveol	k1gInPc2	alveol
i	i	k9	i
tlustší	tlustý	k2eAgInPc4d2	tlustší
pneumocyty	pneumocyt	k1gInPc4	pneumocyt
II	II	kA	II
<g/>
.	.	kIx.	.
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
Clarovy	Clarův	k2eAgFnPc1d1	Clarův
buňky	buňka	k1gFnPc1	buňka
průdušek	průduška	k1gFnPc2	průduška
produkují	produkovat	k5eAaImIp3nP	produkovat
surfaktant	surfaktant	k1gMnSc1	surfaktant
(	(	kIx(	(
<g/>
=	=	kIx~	=
látka	látka	k1gFnSc1	látka
pokrývající	pokrývající	k2eAgFnSc1d1	pokrývající
vnitřek	vnitřek	k1gInSc4	vnitřek
plicních	plicní	k2eAgInPc2d1	plicní
sklípků	sklípek	k1gInPc2	sklípek
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
snižuje	snižovat	k5eAaImIp3nS	snižovat
povrchové	povrchový	k2eAgNnSc4d1	povrchové
napětí	napětí	k1gNnSc4	napětí
a	a	k8xC	a
brání	bránit	k5eAaImIp3nS	bránit
tak	tak	k9	tak
smrštění	smrštění	k1gNnSc1	smrštění
sklípků	sklípek	k1gInPc2	sklípek
a	a	k8xC	a
následnému	následný	k2eAgInSc3d1	následný
kolapsu	kolaps	k1gInSc3	kolaps
plic	plíce	k1gFnPc2	plíce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
makrofágy	makrofág	k1gInPc1	makrofág
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
prašné	prašný	k2eAgFnPc4d1	prašná
buňky	buňka	k1gFnPc4	buňka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
fagocytují	fagocytovat	k5eAaImIp3nP	fagocytovat
prach	prach	k1gInSc1	prach
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
cizí	cizí	k2eAgFnPc1d1	cizí
částice	částice	k1gFnPc1	částice
v	v	k7c6	v
alveolu	alveol	k1gInSc6	alveol
<g/>
.	.	kIx.	.
</s>
<s>
Dohromady	dohromady	k6eAd1	dohromady
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
plicích	plíce	k1gFnPc6	plíce
je	být	k5eAaImIp3nS	být
přes	přes	k7c4	přes
300	[number]	k4	300
miliónů	milión	k4xCgInPc2	milión
plicních	plicní	k2eAgInPc2d1	plicní
sklípků	sklípek	k1gInPc2	sklípek
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
celkový	celkový	k2eAgInSc1d1	celkový
povrch	povrch	k1gInSc1	povrch
je	být	k5eAaImIp3nS	být
40	[number]	k4	40
-	-	kIx~	-
100	[number]	k4	100
m	m	kA	m
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Plicní	plicní	k2eAgFnSc7d1	plicní
brankou	branka	k1gFnSc7	branka
do	do	k7c2	do
plic	plíce	k1gFnPc2	plíce
vstupují	vstupovat	k5eAaImIp3nP	vstupovat
plicní	plicní	k2eAgFnPc1d1	plicní
tepny	tepna	k1gFnPc1	tepna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
přivádějí	přivádět	k5eAaImIp3nP	přivádět
neokysličenou	okysličený	k2eNgFnSc4d1	neokysličená
krev	krev	k1gFnSc4	krev
<g/>
.	.	kIx.	.
</s>
<s>
Tepna	tepna	k1gFnSc1	tepna
se	se	k3xPyFc4	se
větví	větvit	k5eAaImIp3nS	větvit
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
průdušnice	průdušnice	k1gFnSc2	průdušnice
a	a	k8xC	a
průdušinky	průdušinka	k1gFnSc2	průdušinka
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c4	na
kapiláry	kapilára	k1gFnPc4	kapilára
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
opřádají	opřádat	k5eAaImIp3nP	opřádat
alveoly	alveol	k1gInPc4	alveol
<g/>
.	.	kIx.	.
</s>
<s>
Cévy	céva	k1gFnPc1	céva
nesoucí	nesoucí	k2eAgFnSc4d1	nesoucí
okysličenou	okysličený	k2eAgFnSc4d1	okysličená
krev	krev	k1gFnSc4	krev
se	se	k3xPyFc4	se
spojují	spojovat	k5eAaImIp3nP	spojovat
v	v	k7c4	v
plicní	plicní	k2eAgFnPc4d1	plicní
žíly	žíla	k1gFnPc4	žíla
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
ústí	ústit	k5eAaImIp3nP	ústit
do	do	k7c2	do
levé	levý	k2eAgFnSc2d1	levá
předsíně	předsíň	k1gFnSc2	předsíň
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
srdce	srdce	k1gNnSc2	srdce
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
okysličená	okysličený	k2eAgFnSc1d1	okysličená
krev	krev	k1gFnSc1	krev
rozváděna	rozváděn	k2eAgFnSc1d1	rozváděna
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
těle	tělo	k1gNnSc6	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
existuje	existovat	k5eAaImIp3nS	existovat
také	také	k9	také
nutritivní	nutritivní	k2eAgInSc4d1	nutritivní
oběh	oběh	k1gInSc4	oběh
plic	plíce	k1gFnPc2	plíce
<g/>
,	,	kIx,	,
kterým	který	k3yRgFnPc3	který
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
plic	plíce	k1gFnPc2	plíce
přiváděna	přiváděn	k2eAgFnSc1d1	přiváděna
okysličená	okysličený	k2eAgFnSc1d1	okysličená
krev	krev	k1gFnSc1	krev
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	on	k3xPp3gFnPc4	on
vyživuje	vyživovat	k5eAaImIp3nS	vyživovat
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
plicní	plicní	k2eAgFnSc1d1	plicní
ventilace	ventilace	k1gFnSc1	ventilace
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
proudění	proudění	k1gNnSc1	proudění
vzduchu	vzduch	k1gInSc2	vzduch
v	v	k7c6	v
plicích	plíce	k1gFnPc6	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Nádech	nádech	k1gInSc1	nádech
(	(	kIx(	(
<g/>
inspirium	inspirium	k1gNnSc1	inspirium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
aktivní	aktivní	k2eAgInSc4d1	aktivní
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
stahem	stah	k1gInSc7	stah
bránice	bránice	k1gFnSc2	bránice
a	a	k8xC	a
vnějších	vnější	k2eAgInPc2d1	vnější
mezižeberních	mezižeberní	k2eAgInPc2d1	mezižeberní
svalů	sval	k1gInPc2	sval
zvětší	zvětšit	k5eAaPmIp3nS	zvětšit
objem	objem	k1gInSc1	objem
hrudní	hrudní	k2eAgFnSc2d1	hrudní
dutiny	dutina	k1gFnSc2	dutina
<g/>
,	,	kIx,	,
a	a	k8xC	a
plíce	plíce	k1gFnPc1	plíce
se	se	k3xPyFc4	se
vlivem	vliv	k1gInSc7	vliv
podtlaku	podtlak	k1gInSc2	podtlak
v	v	k7c6	v
pohrudniční	pohrudniční	k2eAgFnSc6d1	pohrudniční
dutině	dutina	k1gFnSc6	dutina
roztáhnou	roztáhnout	k5eAaPmIp3nP	roztáhnout
a	a	k8xC	a
naplní	naplnit	k5eAaPmIp3nP	naplnit
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Výdech	výdech	k1gInSc1	výdech
(	(	kIx(	(
<g/>
exspirium	exspirium	k1gNnSc1	exspirium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
okolností	okolnost	k1gFnPc2	okolnost
pasivní	pasivní	k2eAgInSc4d1	pasivní
děj	děj	k1gInSc4	děj
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
maximální	maximální	k2eAgNnSc4d1	maximální
množství	množství	k1gNnSc4	množství
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
lze	lze	k6eAd1	lze
vydechnout	vydechnout	k5eAaPmF	vydechnout
po	po	k7c6	po
největším	veliký	k2eAgInSc6d3	veliký
možném	možný	k2eAgInSc6d1	možný
nádechu	nádech	k1gInSc6	nádech
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
fyzické	fyzický	k2eAgFnSc6d1	fyzická
zdatnosti	zdatnost	k1gFnSc6	zdatnost
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Měří	měřit	k5eAaImIp3nS	měřit
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
spirometru	spirometr	k1gInSc2	spirometr
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yQgInSc2	který
vydechneme	vydechnout	k5eAaPmIp1nP	vydechnout
co	co	k9	co
největší	veliký	k2eAgNnSc4d3	veliký
množství	množství	k1gNnSc4	množství
vzduchu	vzduch	k1gInSc2	vzduch
po	po	k7c6	po
maximálním	maximální	k2eAgInSc6d1	maximální
nádechu	nádech	k1gInSc6	nádech
<g/>
.	.	kIx.	.
</s>
<s>
Jaká	jaký	k3yRgFnSc1	jaký
je	být	k5eAaImIp3nS	být
vitální	vitální	k2eAgFnSc1d1	vitální
kapacita	kapacita	k1gFnSc1	kapacita
plic	plíce	k1gFnPc2	plíce
jedince	jedinec	k1gMnSc4	jedinec
oproti	oproti	k7c3	oproti
průměrnému	průměrný	k2eAgMnSc3d1	průměrný
člověku	člověk	k1gMnSc3	člověk
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
zjistit	zjistit	k5eAaPmF	zjistit
pomocí	pomocí	k7c2	pomocí
jednoduchého	jednoduchý	k2eAgInSc2d1	jednoduchý
vzorečku	vzoreček	k1gInSc2	vzoreček
<g/>
.	.	kIx.	.
</s>
<s>
Hodnotu	hodnota	k1gFnSc4	hodnota
naměřenou	naměřený	k2eAgFnSc4d1	naměřená
spirometrem	spirometr	k1gInSc7	spirometr
se	se	k3xPyFc4	se
vynásobí	vynásobit	k5eAaPmIp3nS	vynásobit
stem	sto	k4xCgNnSc7	sto
a	a	k8xC	a
vydělí	vydělit	k5eAaPmIp3nS	vydělit
povrchem	povrch	k1gInSc7	povrch
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
koeficientem	koeficient	k1gInSc7	koeficient
daným	daný	k2eAgInSc7d1	daný
pro	pro	k7c4	pro
pohlaví	pohlaví	k1gNnPc4	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
2000	[number]	k4	2000
ml	ml	kA	ml
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
muže	muž	k1gMnSc4	muž
2500	[number]	k4	2500
ml.	ml.	kA	ml.
Vzorec	vzorec	k1gInSc1	vzorec
pro	pro	k7c4	pro
výpočet	výpočet	k1gInSc4	výpočet
povrchu	povrch	k1gInSc2	povrch
těla	tělo	k1gNnSc2	tělo
v	v	k7c6	v
m	m	kA	m
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
<g/>
:	:	kIx,	:
výška	výška	k1gFnSc1	výška
v	v	k7c6	v
centimetrech	centimetr	k1gInPc6	centimetr
0,725	[number]	k4	0,725
*	*	kIx~	*
váha	váha	k1gFnSc1	váha
v	v	k7c6	v
kilogramech	kilogram	k1gInPc6	kilogram
0,425	[number]	k4	0,425
*	*	kIx~	*
71,84	[number]	k4	71,84
/	/	kIx~	/
10000	[number]	k4	10000
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
mají	mít	k5eAaImIp3nP	mít
nejvýkonnější	výkonný	k2eAgFnSc4d3	nejvýkonnější
dýchací	dýchací	k2eAgFnSc4d1	dýchací
soustavu	soustava	k1gFnSc4	soustava
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
.	.	kIx.	.
</s>
<s>
Vzduch	vzduch	k1gInSc1	vzduch
vdechnutý	vdechnutý	k2eAgInSc1d1	vdechnutý
nozdrami	nozdra	k1gFnPc7	nozdra
na	na	k7c6	na
zobáku	zobák	k1gInSc6	zobák
proudí	proudit	k5eAaPmIp3nP	proudit
do	do	k7c2	do
plic	plíce	k1gFnPc2	plíce
průdušnicí	průdušnice	k1gFnSc7	průdušnice
a	a	k8xC	a
průduškami	průduška	k1gFnPc7	průduška
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
plicích	plíce	k1gFnPc6	plíce
rozvětvují	rozvětvovat	k5eAaImIp3nP	rozvětvovat
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnPc1	jejich
větve	větev	k1gFnPc1	větev
procházejí	procházet	k5eAaImIp3nP	procházet
plícemi	plíce	k1gFnPc7	plíce
a	a	k8xC	a
vstupují	vstupovat	k5eAaImIp3nP	vstupovat
do	do	k7c2	do
vzdušných	vzdušný	k2eAgInPc2d1	vzdušný
vaků	vak	k1gInPc2	vak
<g/>
,	,	kIx,	,
kterých	který	k3yQgInPc2	který
je	být	k5eAaImIp3nS	být
5	[number]	k4	5
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
jemné	jemný	k2eAgFnPc1d1	jemná
blanité	blanitý	k2eAgFnPc1d1	blanitá
stěny	stěna	k1gFnPc1	stěna
rozvětvující	rozvětvující	k2eAgFnPc1d1	rozvětvující
se	se	k3xPyFc4	se
ve	v	k7c4	v
výběžky	výběžek	k1gInPc4	výběžek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
prostupují	prostupovat	k5eAaImIp3nP	prostupovat
různými	různý	k2eAgFnPc7d1	různá
částmi	část	k1gFnPc7	část
těla	tělo	k1gNnSc2	tělo
včetně	včetně	k7c2	včetně
dutých	dutý	k2eAgFnPc2d1	dutá
kostí	kost	k1gFnPc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
plicích	plíce	k1gFnPc6	plíce
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
jen	jen	k9	jen
část	část	k1gFnSc1	část
vdechnutého	vdechnutý	k2eAgInSc2d1	vdechnutý
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
prochází	procházet	k5eAaImIp3nS	procházet
do	do	k7c2	do
vaků	vak	k1gInPc2	vak
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
při	při	k7c6	při
výdechu	výdech	k1gInSc6	výdech
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
plic	plíce	k1gFnPc2	plíce
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
probíhá	probíhat	k5eAaImIp3nS	probíhat
výměna	výměna	k1gFnSc1	výměna
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zcela	zcela	k6eAd1	zcela
jiný	jiný	k2eAgInSc1d1	jiný
mechanismus	mechanismus	k1gInSc1	mechanismus
dýchání	dýchání	k1gNnSc2	dýchání
než	než	k8xS	než
u	u	k7c2	u
savců	savec	k1gMnPc2	savec
<g/>
.	.	kIx.	.
</s>
<s>
Plíce	plíce	k1gFnPc1	plíce
jsou	být	k5eAaImIp3nP	být
malé	malý	k2eAgFnPc1d1	malá
<g/>
,	,	kIx,	,
přirostlé	přirostlý	k2eAgFnPc1d1	přirostlá
na	na	k7c4	na
strop	strop	k1gInSc4	strop
hrudní	hrudní	k2eAgFnSc2d1	hrudní
dutiny	dutina	k1gFnSc2	dutina
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
výměnu	výměna	k1gFnSc4	výměna
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Objemově	objemově	k6eAd1	objemově
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
jen	jen	k9	jen
málo	málo	k4c1	málo
<g/>
.	.	kIx.	.
</s>
<s>
Mechaniku	mechanika	k1gFnSc4	mechanika
pohybu	pohyb	k1gInSc2	pohyb
vzduchu	vzduch	k1gInSc2	vzduch
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
svalovina	svalovina	k1gFnSc1	svalovina
trupu	trup	k1gInSc2	trup
<g/>
.	.	kIx.	.
</s>
<s>
Alveolitida	Alveolitida	k1gFnSc1	Alveolitida
-	-	kIx~	-
Zánět	zánět	k1gInSc1	zánět
plicních	plicní	k2eAgInPc2d1	plicní
sklípků	sklípek	k1gInPc2	sklípek
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
způsobený	způsobený	k2eAgInSc1d1	způsobený
alergií	alergie	k1gFnSc7	alergie
na	na	k7c4	na
spóry	spóra	k1gFnPc4	spóra
nebo	nebo	k8xC	nebo
prach	prach	k1gInSc4	prach
<g/>
,	,	kIx,	,
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
dušnost	dušnost	k1gFnSc1	dušnost
nebo	nebo	k8xC	nebo
kašel	kašel	k1gInSc1	kašel
s	s	k7c7	s
vykašláváním	vykašlávání	k1gNnSc7	vykašlávání
<g/>
.	.	kIx.	.
</s>
<s>
Astma	astma	k1gFnSc1	astma
-	-	kIx~	-
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
obtížné	obtížný	k2eAgNnSc1d1	obtížné
dýchání	dýchání	k1gNnSc1	dýchání
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
chronické	chronický	k2eAgNnSc4d1	chronické
zánětlivé	zánětlivý	k2eAgNnSc4d1	zánětlivé
onemocnění	onemocnění	k1gNnSc4	onemocnění
dýchacího	dýchací	k2eAgInSc2d1	dýchací
(	(	kIx(	(
<g/>
respiračního	respirační	k2eAgInSc2d1	respirační
<g/>
)	)	kIx)	)
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Astmatický	astmatický	k2eAgInSc1d1	astmatický
záchvat	záchvat	k1gInSc1	záchvat
je	on	k3xPp3gMnPc4	on
obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
vyvolán	vyvolat	k5eAaPmNgInS	vyvolat
alergickou	alergický	k2eAgFnSc7d1	alergická
reakcí	reakce	k1gFnSc7	reakce
na	na	k7c4	na
vdechované	vdechovaný	k2eAgFnPc4d1	vdechovaná
částice	částice	k1gFnPc4	částice
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
pyl	pyl	k1gInSc1	pyl
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
tělesnou	tělesný	k2eAgFnSc7d1	tělesná
námahou	námaha	k1gFnSc7	námaha
či	či	k8xC	či
stresem	stres	k1gInSc7	stres
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Hugo	Hugo	k1gMnSc1	Hugo
J	J	kA	J
<g/>
,	,	kIx,	,
<g/>
.	.	kIx.	.
</s>
<s>
Vokurka	Vokurka	k1gMnSc1	Vokurka
M.	M.	kA	M.
<g/>
:	:	kIx,	:
Praktický	praktický	k2eAgInSc1d1	praktický
slovník	slovník	k1gInSc1	slovník
medicíny	medicína	k1gFnSc2	medicína
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Maxdorf	Maxdorf	k1gInSc1	Maxdorf
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Azbestóza	Azbestóza	k1gFnSc1	Azbestóza
-	-	kIx~	-
Zjizvení	zjizvení	k1gNnSc1	zjizvení
plic	plíce	k1gFnPc2	plíce
způsobené	způsobený	k2eAgFnPc1d1	způsobená
vdechováním	vdechování	k1gNnSc7	vdechování
azbestových	azbestový	k2eAgNnPc2d1	azbestové
vláken	vlákno	k1gNnPc2	vlákno
<g/>
,	,	kIx,	,
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
dušnost	dušnost	k1gFnSc1	dušnost
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Bronchitida	bronchitida	k1gFnSc1	bronchitida
-	-	kIx~	-
Zánět	zánět	k1gInSc1	zánět
průdušek	průduška	k1gFnPc2	průduška
<g/>
,	,	kIx,	,
spojujících	spojující	k2eAgFnPc2d1	spojující
plíce	plíce	k1gFnSc2	plíce
s	s	k7c7	s
průdušnicí	průdušnice	k1gFnSc7	průdušnice
<g/>
,	,	kIx,	,
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
kašel	kašel	k1gInSc1	kašel
s	s	k7c7	s
hojným	hojný	k2eAgNnSc7d1	hojné
vykašláváním	vykašlávání	k1gNnSc7	vykašlávání
<g/>
.	.	kIx.	.
</s>
<s>
Častější	častý	k2eAgMnSc1d2	častější
u	u	k7c2	u
kuřáků	kuřák	k1gMnPc2	kuřák
a	a	k8xC	a
lidí	člověk	k1gMnPc2	člověk
žijících	žijící	k2eAgMnPc2d1	žijící
ve	v	k7c6	v
znečištěných	znečištěný	k2eAgFnPc6d1	znečištěná
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Emfyzém	emfyzém	k1gInSc1	emfyzém
-	-	kIx~	-
Těžká	těžký	k2eAgFnSc1d1	těžká
dušnost	dušnost	k1gFnSc1	dušnost
způsobená	způsobený	k2eAgFnSc1d1	způsobená
nevratným	vratný	k2eNgNnSc7d1	nevratné
poškozením	poškození	k1gNnSc7	poškození
plicních	plicní	k2eAgInPc2d1	plicní
sklípků	sklípek	k1gInPc2	sklípek
kouřením	kouření	k1gNnSc7	kouření
<g/>
.	.	kIx.	.
</s>
<s>
Plicní	plicní	k2eAgInSc1d1	plicní
edém	edém	k1gInSc1	edém
-	-	kIx~	-
Těžká	těžký	k2eAgFnSc1d1	těžká
dušnost	dušnost	k1gFnSc1	dušnost
způsobená	způsobený	k2eAgFnSc1d1	způsobená
tekutinou	tekutina	k1gFnSc7	tekutina
v	v	k7c6	v
plicích	plíce	k1gFnPc6	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Pneumokonióza	Pneumokonióza	k1gFnSc1	Pneumokonióza
-	-	kIx~	-
Těžké	těžký	k2eAgNnSc1d1	těžké
poškození	poškození	k1gNnSc1	poškození
plic	plíce	k1gFnPc2	plíce
u	u	k7c2	u
horníků	horník	k1gMnPc2	horník
<g/>
,	,	kIx,	,
způsobené	způsobený	k2eAgInPc1d1	způsobený
zjizvením	zjizvení	k1gNnSc7	zjizvení
plicních	plicní	k2eAgInPc2d1	plicní
sklípků	sklípek	k1gInPc2	sklípek
po	po	k7c6	po
vdechování	vdechování	k1gNnSc6	vdechování
uhelného	uhelný	k2eAgInSc2d1	uhelný
prachu	prach	k1gInSc2	prach
<g/>
.	.	kIx.	.
</s>
<s>
Pneumonie	pneumonie	k1gFnSc1	pneumonie
-	-	kIx~	-
Zápal	zápal	k1gInSc1	zápal
plic	plíce	k1gFnPc2	plíce
-	-	kIx~	-
Zánětlivé	zánětlivý	k2eAgNnSc1d1	zánětlivé
onemocnění	onemocnění	k1gNnSc1	onemocnění
<g/>
,	,	kIx,	,
způsobené	způsobený	k2eAgNnSc1d1	způsobené
bakteriemi	bakterie	k1gFnPc7	bakterie
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
rodu	rod	k1gInSc2	rod
Streptococus	Streptococus	k1gMnSc1	Streptococus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
viry	vir	k1gInPc1	vir
(	(	kIx(	(
<g/>
rod	rod	k1gInSc1	rod
Herpes	herpes	k1gInSc1	herpes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzácně	vzácně	k6eAd1	vzácně
mykotický	mykotický	k2eAgMnSc1d1	mykotický
<g/>
.	.	kIx.	.
</s>
<s>
Pneumotorax	pneumotorax	k1gInSc1	pneumotorax
-	-	kIx~	-
Bolest	bolest	k1gFnSc1	bolest
na	na	k7c6	na
hrudi	hruď	k1gFnSc6	hruď
a	a	k8xC	a
dušnost	dušnost	k1gFnSc1	dušnost
způsobená	způsobený	k2eAgFnSc1d1	způsobená
přítomností	přítomnost	k1gFnSc7	přítomnost
vzduchu	vzduch	k1gInSc2	vzduch
v	v	k7c6	v
dutině	dutina	k1gFnSc6	dutina
hrudní	hrudní	k2eAgFnSc6d1	hrudní
mezi	mezi	k7c7	mezi
plícemi	plíce	k1gFnPc7	plíce
a	a	k8xC	a
žebry	žebr	k1gInPc7	žebr
<g/>
.	.	kIx.	.
</s>
<s>
Dušnost	dušnost	k1gFnSc1	dušnost
způsobena	způsoben	k2eAgFnSc1d1	způsobena
smrštěním	smrštění	k1gNnSc7	smrštění
plíce	plíce	k1gFnSc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
hemotoraxu	hemotorax	k1gInSc2	hemotorax
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
lékařské	lékařský	k2eAgNnSc1d1	lékařské
ošetření	ošetření	k1gNnSc1	ošetření
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
vzduch	vzduch	k1gInSc1	vzduch
odsát	odsát	k5eAaPmF	odsát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
plíce	plíce	k1gFnPc4	plíce
reexpandovat	reexpandovat	k5eAaImF	reexpandovat
<g/>
.	.	kIx.	.
</s>
<s>
Hemotorax	Hemotorax	k1gInSc1	Hemotorax
-	-	kIx~	-
Nahromadění	nahromadění	k1gNnSc1	nahromadění
krve	krev	k1gFnSc2	krev
v	v	k7c6	v
dutině	dutina	k1gFnSc6	dutina
hrudní	hrudní	k2eAgFnSc6d1	hrudní
mezi	mezi	k7c7	mezi
plícemi	plíce	k1gFnPc7	plíce
a	a	k8xC	a
žebry	žebr	k1gInPc7	žebr
<g/>
,	,	kIx,	,
způsobené	způsobený	k2eAgFnSc2d1	způsobená
úrazem	úraz	k1gInSc7	úraz
<g/>
.	.	kIx.	.
</s>
<s>
Projevuje	projevovat	k5eAaImIp3nS	projevovat
se	se	k3xPyFc4	se
bolestí	bolest	k1gFnSc7	bolest
a	a	k8xC	a
dušností	dušnost	k1gFnSc7	dušnost
<g/>
.	.	kIx.	.
</s>
<s>
Dušnost	dušnost	k1gFnSc1	dušnost
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
smrštěním	smrštění	k1gNnSc7	smrštění
plíce	plíce	k1gFnSc1	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
lékařského	lékařský	k2eAgInSc2d1	lékařský
zákroku	zákrok	k1gInSc2	zákrok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
krev	krev	k1gFnSc1	krev
odstraněna	odstranit	k5eAaPmNgFnS	odstranit
a	a	k8xC	a
plíce	plíce	k1gFnSc1	plíce
má	mít	k5eAaImIp3nS	mít
možnost	možnost	k1gFnSc4	možnost
reexpandovat	reexpandovat	k5eAaImF	reexpandovat
<g/>
.	.	kIx.	.
</s>
<s>
Karcinom	karcinom	k1gInSc1	karcinom
plic	plíce	k1gFnPc2	plíce
</s>
