<p>
<s>
ls	ls	k?	ls
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
název	název	k1gInSc1	název
pro	pro	k7c4	pro
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
základních	základní	k2eAgInPc2d1	základní
příkazů	příkaz	k1gInPc2	příkaz
unixových	unixový	k2eAgInPc2d1	unixový
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
příkazovém	příkazový	k2eAgInSc6d1	příkazový
řádku	řádek	k1gInSc6	řádek
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
vypsání	vypsání	k1gNnSc3	vypsání
obsahu	obsah	k1gInSc2	obsah
adresáře	adresář	k1gInSc2	adresář
<g/>
.	.	kIx.	.
</s>
<s>
Vypisuje	vypisovat	k5eAaImIp3nS	vypisovat
jména	jméno	k1gNnPc4	jméno
adresářů	adresář	k1gInPc2	adresář
a	a	k8xC	a
souborů	soubor	k1gInPc2	soubor
a	a	k8xC	a
v	v	k7c6	v
podrobném	podrobný	k2eAgInSc6d1	podrobný
výpisu	výpis	k1gInSc6	výpis
též	též	k9	též
unixová	unixový	k2eAgNnPc4d1	unixové
přístupová	přístupový	k2eAgNnPc4d1	přístupové
práva	právo	k1gNnPc4	právo
<g/>
,	,	kIx,	,
vlastníka	vlastník	k1gMnSc4	vlastník
<g/>
,	,	kIx,	,
skupinu	skupina	k1gFnSc4	skupina
<g/>
,	,	kIx,	,
velikost	velikost	k1gFnSc4	velikost
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
informace	informace	k1gFnPc4	informace
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
specifikován	specifikovat	k5eAaBmNgInS	specifikovat
ve	v	k7c6	v
standardu	standard	k1gInSc6	standard
POSIX	POSIX	kA	POSIX
a	a	k8xC	a
Single	singl	k1gInSc5	singl
UNIX	UNIX	kA	UNIX
Specification	Specification	k1gInSc1	Specification
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Příkaz	příkaz	k1gInSc1	příkaz
(	(	kIx(	(
<g/>
utilita	utilita	k1gFnSc1	utilita
<g/>
)	)	kIx)	)
ls	ls	k?	ls
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
původním	původní	k2eAgInSc6d1	původní
AT	AT	kA	AT
<g/>
&	&	k?	&
<g/>
T	T	kA	T
Unixu	Unix	k1gInSc6	Unix
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
příkazu	příkaz	k1gInSc2	příkaz
list	list	k1gInSc1	list
segments	segments	k1gInSc1	segments
v	v	k7c6	v
systému	systém	k1gInSc6	systém
Multics	Multicsa	k1gFnPc2	Multicsa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
paměťové	paměťový	k2eAgInPc1d1	paměťový
segmenty	segment	k1gInPc1	segment
byly	být	k5eAaImAgInP	být
de	de	k?	de
facto	facta	k1gFnSc5	facta
soubory	soubor	k1gInPc7	soubor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc1	dva
nejznámější	známý	k2eAgFnPc1d3	nejznámější
verze	verze	k1gFnPc1	verze
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
od	od	k7c2	od
Free	Fre	k1gInSc2	Fre
Software	software	k1gInSc1	software
Foundation	Foundation	k1gInSc1	Foundation
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
balíčku	balíček	k1gInSc2	balíček
GNU	gnu	k1gNnSc2	gnu
Core	Core	k1gNnSc1	Core
Utilities	Utilities	k1gInSc1	Utilities
<g/>
)	)	kIx)	)
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgFnPc4d1	různá
varianty	varianta	k1gFnPc4	varianta
BSD	BSD	kA	BSD
jako	jako	k8xC	jako
FreeBSD	FreeBSD	k1gMnSc1	FreeBSD
<g/>
,	,	kIx,	,
OpenBSD	OpenBSD	k1gMnSc1	OpenBSD
nebo	nebo	k8xC	nebo
Darwin	Darwin	k1gMnSc1	Darwin
firmy	firma	k1gFnSc2	firma
Apple	Apple	kA	Apple
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
svobodné	svobodný	k2eAgInPc1d1	svobodný
a	a	k8xC	a
open	open	k1gInSc1	open
source	sourec	k1gInSc2	sourec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chování	chování	k1gNnSc1	chování
==	==	k?	==
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
příkaz	příkaz	k1gInSc4	příkaz
ls	ls	k?	ls
volán	volán	k1gInSc1	volán
bez	bez	k7c2	bez
argumentů	argument	k1gInPc2	argument
<g/>
,	,	kIx,	,
vypíše	vypsat	k5eAaPmIp3nS	vypsat
soubory	soubor	k1gInPc4	soubor
a	a	k8xC	a
složky	složka	k1gFnPc4	složka
v	v	k7c6	v
aktuálním	aktuální	k2eAgInSc6d1	aktuální
pracovním	pracovní	k2eAgInSc6d1	pracovní
adresáři	adresář	k1gInSc6	adresář
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
parametr	parametr	k1gInSc1	parametr
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
specifikován	specifikovat	k5eAaBmNgInS	specifikovat
i	i	k9	i
jiný	jiný	k2eAgInSc1d1	jiný
adresář	adresář	k1gInSc1	adresář
nebo	nebo	k8xC	nebo
seznam	seznam	k1gInSc1	seznam
adresářů	adresář	k1gInPc2	adresář
a	a	k8xC	a
příkaz	příkaz	k1gInSc1	příkaz
ls	ls	k?	ls
potom	potom	k6eAd1	potom
vypíše	vypsat	k5eAaPmIp3nS	vypsat
jejich	jejich	k3xOp3gInSc4	jejich
obsah	obsah	k1gInSc4	obsah
<g/>
.	.	kIx.	.
</s>
<s>
Soubory	soubor	k1gInPc1	soubor
začínající	začínající	k2eAgFnSc2d1	začínající
tečkou	tečka	k1gFnSc7	tečka
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nejsou	být	k5eNaImIp3nP	být
vypisovány	vypisován	k2eAgFnPc1d1	vypisována
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
explicitně	explicitně	k6eAd1	explicitně
požadováno	požadován	k2eAgNnSc1d1	požadováno
pomocí	pomocí	k7c2	pomocí
parametru	parametr	k1gInSc2	parametr
-	-	kIx~	-
<g/>
a.	a.	k?	a.
Bez	bez	k7c2	bez
parametrů	parametr	k1gInPc2	parametr
vypisuje	vypisovat	k5eAaImIp3nS	vypisovat
příkaz	příkaz	k1gInSc4	příkaz
soubory	soubor	k1gInPc7	soubor
a	a	k8xC	a
adresáře	adresář	k1gInPc1	adresář
v	v	k7c6	v
holém	holý	k2eAgInSc6d1	holý
formátu	formát	k1gInSc6	formát
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ten	ten	k3xDgMnSc1	ten
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
vhodný	vhodný	k2eAgInSc1d1	vhodný
pro	pro	k7c4	pro
orientaci	orientace	k1gFnSc4	orientace
uživatele	uživatel	k1gMnSc2	uživatel
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
obtížně	obtížně	k6eAd1	obtížně
rozeznávají	rozeznávat	k5eAaImIp3nP	rozeznávat
adresáře	adresář	k1gInPc1	adresář
od	od	k7c2	od
souborů	soubor	k1gInPc2	soubor
nebo	nebo	k8xC	nebo
symbolických	symbolický	k2eAgInPc2d1	symbolický
odkazů	odkaz	k1gInPc2	odkaz
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
jsou	být	k5eAaImIp3nP	být
používány	používán	k2eAgInPc1d1	používán
následující	následující	k2eAgInPc1d1	následující
parametry	parametr	k1gInPc1	parametr
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
-l	-l	k?	-l
:	:	kIx,	:
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
formát	formát	k1gInSc4	formát
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
long	long	k1gInSc1	long
format	format	k1gInSc1	format
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
typ	typ	k1gInSc4	typ
souboru	soubor	k1gInSc2	soubor
<g/>
,	,	kIx,	,
<g/>
oprávnění	oprávnění	k1gNnSc1	oprávnění
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
pevných	pevný	k2eAgInPc2d1	pevný
odkazů	odkaz	k1gInPc2	odkaz
<g/>
,	,	kIx,	,
vlastníka	vlastník	k1gMnSc4	vlastník
<g/>
,	,	kIx,	,
skupinu	skupina	k1gFnSc4	skupina
<g/>
,	,	kIx,	,
velikost	velikost	k1gFnSc4	velikost
<g/>
,	,	kIx,	,
datum	datum	k1gNnSc4	datum
a	a	k8xC	a
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
-F	-F	k?	-F
:	:	kIx,	:
přidá	přidat	k5eAaPmIp3nS	přidat
za	za	k7c4	za
jméno	jméno	k1gNnSc4	jméno
znak	znak	k1gInSc1	znak
podle	podle	k7c2	podle
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdičku	hvězdička	k1gFnSc4	hvězdička
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
*	*	kIx~	*
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
spustitelný	spustitelný	k2eAgInSc4d1	spustitelný
soubor	soubor	k1gInSc4	soubor
<g/>
,	,	kIx,	,
lomítko	lomítko	k1gNnSc4	lomítko
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
/	/	kIx~	/
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
adresář	adresář	k1gInSc4	adresář
<g/>
.	.	kIx.	.
</s>
<s>
Normálním	normální	k2eAgInPc3d1	normální
souborům	soubor	k1gInPc3	soubor
není	být	k5eNaImIp3nS	být
přidáni	přidat	k5eAaPmNgMnP	přidat
nic	nic	k3yNnSc1	nic
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
-a	-a	k?	-a
:	:	kIx,	:
zobrazí	zobrazit	k5eAaPmIp3nP	zobrazit
i	i	k9	i
soubory	soubor	k1gInPc1	soubor
které	který	k3yQgMnPc4	který
začínají	začínat	k5eAaImIp3nP	začínat
tečkou	tečka	k1gFnSc7	tečka
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
jinak	jinak	k6eAd1	jinak
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
skryté	skrytý	k2eAgFnPc4d1	skrytá
a	a	k8xC	a
nejsou	být	k5eNaImIp3nP	být
vypisovány	vypisován	k2eAgFnPc1d1	vypisována
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
-R	-R	k?	-R
:	:	kIx,	:
rekurzivně	rekurzivně	k6eAd1	rekurzivně
vypisuje	vypisovat	k5eAaImIp3nS	vypisovat
i	i	k9	i
obsah	obsah	k1gInSc1	obsah
podadresářů	podadresář	k1gInPc2	podadresář
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
ls	ls	k?	ls
-R	-R	k?	-R
/	/	kIx~	/
vypíše	vypsat	k5eAaPmIp3nS	vypsat
všechny	všechen	k3xTgInPc4	všechen
soubory	soubor	k1gInPc4	soubor
v	v	k7c6	v
počítači	počítač	k1gInSc6	počítač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
-d	-d	k?	-d
:	:	kIx,	:
vypíše	vypsat	k5eAaPmIp3nS	vypsat
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
symbolickém	symbolický	k2eAgInSc6d1	symbolický
odkazu	odkaz	k1gInSc6	odkaz
nebo	nebo	k8xC	nebo
adresáři	adresář	k1gInSc6	adresář
namísto	namísto	k7c2	namísto
výpisu	výpis	k1gInSc2	výpis
jeho	jeho	k3xOp3gInSc2	jeho
obsahu	obsah	k1gInSc2	obsah
nebo	nebo	k8xC	nebo
cíli	cíl	k1gInSc3	cíl
odkazu	odkaz	k1gInSc2	odkaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
-t	-t	k?	-t
:	:	kIx,	:
seřadí	seřadit	k5eAaPmIp3nS	seřadit
výpis	výpis	k1gInSc4	výpis
podle	podle	k7c2	podle
času	čas	k1gInSc2	čas
poslední	poslední	k2eAgFnSc2d1	poslední
úpravy	úprava	k1gFnSc2	úprava
souboru	soubor	k1gInSc2	soubor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
-X	-X	k?	-X
:	:	kIx,	:
seřadí	seřadit	k5eAaPmIp3nS	seřadit
výpis	výpis	k1gInSc4	výpis
podle	podle	k7c2	podle
přípony	přípona	k1gFnSc2	přípona
souboru	soubor	k1gInSc2	soubor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
-h	-h	k?	-h
:	:	kIx,	:
velikost	velikost	k1gFnSc1	velikost
se	se	k3xPyFc4	se
vypíše	vypsat	k5eAaPmIp3nS	vypsat
ve	v	k7c6	v
srozumitelnějším	srozumitelný	k2eAgInSc6d2	srozumitelnější
formátu	formát	k1gInSc6	formát
(	(	kIx(	(
<g/>
např.	např.	kA	např.
1	[number]	k4	1
<g/>
K	K	kA	K
<g/>
,	,	kIx,	,
234	[number]	k4	234
<g/>
M	M	kA	M
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
G	G	kA	G
<g/>
)	)	kIx)	)
<g/>
V	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
prostředích	prostředí	k1gNnPc6	prostředí
lze	lze	k6eAd1	lze
pomocí	pomocí	k7c2	pomocí
parametru	parametr	k1gInSc2	parametr
--color	-olora	k1gFnPc2	--colora
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
GNU	gnu	k1gNnSc4	gnu
ls	ls	k?	ls
nebo	nebo	k8xC	nebo
-G	-G	k?	-G
(	(	kIx(	(
<g/>
FreeBSD	FreeBSD	k1gMnSc1	FreeBSD
ls	ls	k?	ls
<g/>
)	)	kIx)	)
výstup	výstup	k1gInSc4	výstup
obarvit	obarvit	k5eAaPmF	obarvit
<g/>
.	.	kIx.	.
</s>
<s>
Soubory	soubor	k1gInPc1	soubor
pak	pak	k6eAd1	pak
mají	mít	k5eAaImIp3nP	mít
různé	různý	k2eAgFnPc4d1	různá
barvy	barva	k1gFnPc4	barva
podle	podle	k7c2	podle
typů	typ	k1gInPc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
GNU	gnu	k1gMnSc1	gnu
ls	ls	k?	ls
pro	pro	k7c4	pro
určení	určení	k1gNnSc4	určení
barvy	barva	k1gFnSc2	barva
souboru	soubor	k1gInSc2	soubor
zhodnotí	zhodnotit	k5eAaPmIp3nS	zhodnotit
jeho	jeho	k3xOp3gInSc4	jeho
formát	formát	k1gInSc4	formát
<g/>
,	,	kIx,	,
oprávnění	oprávnění	k1gNnSc4	oprávnění
a	a	k8xC	a
příponu	přípona	k1gFnSc4	přípona
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
FreeBSD	FreeBSD	k1gFnSc1	FreeBSD
ls	ls	k?	ls
použije	použít	k5eAaPmIp3nS	použít
pouze	pouze	k6eAd1	pouze
formát	formát	k1gInSc4	formát
a	a	k8xC	a
oprávnění	oprávnění	k1gNnSc4	oprávnění
souboru	soubor	k1gInSc2	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
barevném	barevný	k2eAgNnSc6d1	barevné
zvýraznění	zvýraznění	k1gNnSc6	zvýraznění
vypadá	vypadat	k5eAaImIp3nS	vypadat
výpis	výpis	k1gInSc1	výpis
souborů	soubor	k1gInPc2	soubor
například	například	k6eAd1	například
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
brw-r--r--	brw---	k?	brw-r--r--
1	[number]	k4	1
unixguy	unixgua	k1gMnSc2	unixgua
staff	staff	k1gInSc4	staff
64	[number]	k4	64
<g/>
,	,	kIx,	,
64	[number]	k4	64
Jan	Jan	k1gMnSc1	Jan
27	[number]	k4	27
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
52	[number]	k4	52
blokove	blokov	k1gInSc5	blokov
zarizeni	zaridit	k5eAaPmNgMnP	zaridit
</s>
</p>
<p>
<s>
crw-r--r--	crw---	k?	crw-r--r--
1	[number]	k4	1
unixguy	unixgua	k1gMnSc2	unixgua
staff	staff	k1gInSc4	staff
64	[number]	k4	64
<g/>
,	,	kIx,	,
255	[number]	k4	255
Jan	Jan	k1gMnSc1	Jan
26	[number]	k4	26
13	[number]	k4	13
<g/>
:	:	kIx,	:
<g/>
57	[number]	k4	57
znakove	znakov	k1gInSc5	znakov
zarizeni	zarizen	k2eAgMnPc1d1	zarizen
</s>
</p>
<p>
<s>
-rw-r--r--	w---	k?	-rw-r--r--
1	[number]	k4	1
unixguy	unixgua	k1gMnSc2	unixgua
staff	staff	k1gInSc4	staff
290	[number]	k4	290
Jan	Jan	k1gMnSc1	Jan
26	[number]	k4	26
14	[number]	k4	14
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
komprimovany	komprimovany	k?	komprimovany
<g/>
.	.	kIx.	.
<g/>
gz	gz	k?	gz
</s>
</p>
<p>
<s>
-rw-r--r--	w---	k?	-rw-r--r--
1	[number]	k4	1
unixguy	unixgua	k1gMnSc2	unixgua
staff	staff	k1gInSc4	staff
331836	[number]	k4	331836
Jan	Jan	k1gMnSc1	Jan
26	[number]	k4	26
14	[number]	k4	14
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
data	datum	k1gNnPc1	datum
<g/>
.	.	kIx.	.
<g/>
ppm	ppm	k?	ppm
</s>
</p>
<p>
<s>
drwxrwx--x	drwxrwx-	k1gInSc1	drwxrwx--x
2	[number]	k4	2
unixguy	unixgua	k1gFnSc2	unixgua
staff	staff	k1gInSc4	staff
48	[number]	k4	48
Jan	Jan	k1gMnSc1	Jan
26	[number]	k4	26
11	[number]	k4	11
<g/>
:	:	kIx,	:
<g/>
28	[number]	k4	28
adresar	adresara	k1gFnPc2	adresara
</s>
</p>
<p>
<s>
-rwxrwx--x	wxrwx-	k1gInSc1	-rwxrwx--x
1	[number]	k4	1
unixguy	unixgua	k1gFnSc2	unixgua
staff	staff	k1gInSc4	staff
29	[number]	k4	29
Jan	Jan	k1gMnSc1	Jan
26	[number]	k4	26
14	[number]	k4	14
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
spustitelny	spustitelny	k?	spustitelny
</s>
</p>
<p>
<s>
prw-r--r--	prw---	k?	prw-r--r--
1	[number]	k4	1
unixguy	unixgua	k1gMnSc2	unixgua
staff	staff	k1gInSc4	staff
0	[number]	k4	0
Jan	Jan	k1gMnSc1	Jan
26	[number]	k4	26
11	[number]	k4	11
<g/>
:	:	kIx,	:
<g/>
50	[number]	k4	50
fifo	fifo	k6eAd1	fifo
</s>
</p>
<p>
<s>
lrwxrwxrwx	lrwxrwxrwx	k1gInSc1	lrwxrwxrwx
1	[number]	k4	1
unixguy	unixgua	k1gFnSc2	unixgua
staff	staff	k1gInSc4	staff
3	[number]	k4	3
Jan	Jan	k1gMnSc1	Jan
26	[number]	k4	26
11	[number]	k4	11
<g/>
:	:	kIx,	:
<g/>
44	[number]	k4	44
link	linka	k1gFnPc2	linka
-	-	kIx~	-
<g/>
>	>	kIx)	>
dir	dir	k?	dir
</s>
</p>
<p>
<s>
-rw-rw----	ww----	k?	-rw-rw----
1	[number]	k4	1
unixguy	unixgua	k1gMnSc2	unixgua
staff	staff	k1gInSc4	staff
217	[number]	k4	217
Jan	Jan	k1gMnSc1	Jan
26	[number]	k4	26
14	[number]	k4	14
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
bezny	bezna	k1gMnSc2	bezna
soubor	soubor	k1gInSc4	soubor
</s>
</p>
<p>
<s>
Příkaz	příkaz	k1gInSc1	příkaz
ls	ls	k?	ls
má	mít	k5eAaImIp3nS	mít
množství	množství	k1gNnSc4	množství
dalších	další	k2eAgInPc2d1	další
parametrů	parametr	k1gInPc2	parametr
a	a	k8xC	a
možností	možnost	k1gFnPc2	možnost
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
manuálová	manuálový	k2eAgFnSc1d1	manuálová
stránka	stránka	k1gFnSc1	stránka
příkazu	příkaz	k1gInSc2	příkaz
ls	ls	k?	ls
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
a	a	k8xC	a
používaný	používaný	k2eAgInSc1d1	používaný
nástroj	nástroj	k1gInSc1	nástroj
v	v	k7c6	v
příkazovém	příkazový	k2eAgInSc6d1	příkazový
řádku	řádek	k1gInSc6	řádek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
některých	některý	k3yIgInPc6	některý
systémech	systém	k1gInPc6	systém
(	(	kIx(	(
<g/>
např.	např.	kA	např.
PCLinuxOS	PCLinuxOS	k1gFnSc1	PCLinuxOS
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
ls	ls	k?	ls
alias	alias	k9	alias
l.	l.	k?	l.
Mnoho	mnoho	k6eAd1	mnoho
systémů	systém	k1gInPc2	systém
také	také	k9	také
používá	používat	k5eAaImIp3nS	používat
aliasy	aliasa	k1gFnPc4	aliasa
ls	ls	k?	ls
-l	-l	k?	-l
na	na	k7c6	na
ll	ll	k?	ll
nebo	nebo	k8xC	nebo
ls	ls	k?	ls
-la	a	k?	-la
na	na	k7c6	na
la	la	k1gNnSc6	la
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přidružené	přidružený	k2eAgInPc1d1	přidružený
příkazy	příkaz	k1gInPc1	příkaz
==	==	k?	==
</s>
</p>
<p>
<s>
K	k	k7c3	k
příkazu	příkaz	k1gInSc3	příkaz
ls	ls	k?	ls
patří	patřit	k5eAaImIp3nP	patřit
také	také	k9	také
příkazy	příkaz	k1gInPc1	příkaz
dir	dir	k?	dir
a	a	k8xC	a
vdir	vdir	k1gInSc1	vdir
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
oproti	oproti	k7c3	oproti
ls	ls	k?	ls
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
způsobu	způsob	k1gInSc6	způsob
výpisu	výpis	k1gInSc2	výpis
adresářové	adresářový	k2eAgFnSc2d1	adresářová
informace	informace	k1gFnSc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
tři	tři	k4xCgInPc1	tři
příkazy	příkaz	k1gInPc1	příkaz
vypisují	vypisovat	k5eAaImIp3nP	vypisovat
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
zadaných	zadaný	k2eAgInPc6d1	zadaný
argumentech	argument	k1gInPc6	argument
<g/>
.	.	kIx.	.
</s>
<s>
Obsahy	obsah	k1gInPc1	obsah
adresářů	adresář	k1gInPc2	adresář
se	se	k3xPyFc4	se
vypíšou	vypsat	k5eAaPmIp3nP	vypsat
setříděné	setříděný	k2eAgInPc1d1	setříděný
podle	podle	k7c2	podle
ASCII	ascii	kA	ascii
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
aktivní	aktivní	k2eAgFnSc2d1	aktivní
podpory	podpora	k1gFnSc2	podpora
locale	locale	k6eAd1	locale
pak	pak	k6eAd1	pak
podle	podle	k7c2	podle
aktuálního	aktuální	k2eAgNnSc2d1	aktuální
národního	národní	k2eAgNnSc2d1	národní
nastavení	nastavení	k1gNnSc2	nastavení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příkaz	příkaz	k1gInSc4	příkaz
ls	ls	k?	ls
soubory	soubor	k1gInPc4	soubor
implicitně	implicitně	k6eAd1	implicitně
vypisuje	vypisovat	k5eAaImIp3nS	vypisovat
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
sloupcích	sloupec	k1gInPc6	sloupec
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
pokud	pokud	k8xS	pokud
standardní	standardní	k2eAgInSc1d1	standardní
výstup	výstup	k1gInSc1	výstup
směřuje	směřovat	k5eAaImIp3nS	směřovat
na	na	k7c4	na
terminál	terminál	k1gInSc4	terminál
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
implicitní	implicitní	k2eAgInSc1d1	implicitní
výstup	výstup	k1gInSc1	výstup
přesměrován	přesměrován	k2eAgInSc1d1	přesměrován
do	do	k7c2	do
souboru	soubor	k1gInSc2	soubor
či	či	k8xC	či
do	do	k7c2	do
roury	roura	k1gFnSc2	roura
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
vypsání	vypsání	k1gNnSc3	vypsání
informace	informace	k1gFnSc2	informace
do	do	k7c2	do
jediného	jediný	k2eAgInSc2d1	jediný
sloupce	sloupec	k1gInSc2	sloupec
(	(	kIx(	(
<g/>
jeden	jeden	k4xCgInSc1	jeden
soubor	soubor	k1gInSc1	soubor
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
řádek	řádek	k1gInSc4	řádek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příkaz	příkaz	k1gInSc1	příkaz
dir	dir	k?	dir
soubory	soubor	k1gInPc7	soubor
vypíše	vypsat	k5eAaPmIp3nS	vypsat
vždy	vždy	k6eAd1	vždy
ve	v	k7c6	v
sloupcích	sloupec	k1gInPc6	sloupec
<g/>
,	,	kIx,	,
totéž	týž	k3xTgNnSc1	týž
co	co	k9	co
ls	ls	k?	ls
-	-	kIx~	-
<g/>
C.	C.	kA	C.
Příkaz	příkaz	k1gInSc1	příkaz
vdir	vdir	k1gMnSc1	vdir
soubory	soubor	k1gInPc4	soubor
implicitně	implicitně	k6eAd1	implicitně
vypíše	vypsat	k5eAaPmIp3nS	vypsat
v	v	k7c6	v
dlouhém	dlouhý	k2eAgInSc6d1	dlouhý
formátu	formát	k1gInSc6	formát
<g/>
,	,	kIx,	,
totéž	týž	k3xTgNnSc1	týž
co	co	k9	co
ls	ls	k?	ls
-	-	kIx~	-
<g/>
l.	l.	k?	l.
Výpis	výpis	k1gInSc1	výpis
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
sloupce	sloupec	k1gInSc2	sloupec
lze	lze	k6eAd1	lze
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
případě	případ	k1gInSc6	případ
vynutit	vynutit	k5eAaPmF	vynutit
použitím	použití	k1gNnSc7	použití
přepínače	přepínač	k1gInSc2	přepínač
ls	ls	k?	ls
-1	-1	k4	-1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příklad	příklad	k1gInSc1	příklad
použití	použití	k1gNnSc1	použití
==	==	k?	==
</s>
</p>
<p>
<s>
Následující	následující	k2eAgInSc1d1	následující
příklad	příklad	k1gInSc1	příklad
jednoduše	jednoduše	k6eAd1	jednoduše
demonstruje	demonstrovat	k5eAaBmIp3nS	demonstrovat
odlišné	odlišný	k2eAgNnSc4d1	odlišné
chování	chování	k1gNnSc4	chování
ls	ls	k?	ls
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
dvou	dva	k4xCgInPc2	dva
různých	různý	k2eAgInPc2d1	různý
argumentů	argument	k1gInPc2	argument
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
příkladu	příklad	k1gInSc6	příklad
má	mít	k5eAaImIp3nS	mít
uživatel	uživatel	k1gMnSc1	uživatel
pepa	pepa	k1gMnSc1	pepa
adresář	adresář	k1gInSc4	adresář
navrhy	navrha	k1gFnSc2	navrha
<g/>
,	,	kIx,	,
soubor	soubor	k1gInSc1	soubor
edition-	edition-	k?	edition-
<g/>
32	[number]	k4	32
a	a	k8xC	a
spustitelný	spustitelný	k2eAgInSc4d1	spustitelný
soubor	soubor	k1gInSc4	soubor
edit	edit	k5eAaPmF	edit
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
domovském	domovský	k2eAgInSc6d1	domovský
adresáři	adresář	k1gInSc6	adresář
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výpisu	výpis	k1gInSc6	výpis
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
jaká	jaký	k3yQgNnPc4	jaký
oprávnění	oprávnění	k1gNnPc4	oprávnění
mají	mít	k5eAaImIp3nP	mít
vzhledem	vzhledem	k7c3	vzhledem
souboru	soubor	k1gInSc6	soubor
jeho	jeho	k3xOp3gMnSc3	jeho
majiteli	majitel	k1gMnSc3	majitel
(	(	kIx(	(
<g/>
u	u	k7c2	u
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
skupině	skupina	k1gFnSc6	skupina
(	(	kIx(	(
<g/>
g	g	kA	g
<g/>
)	)	kIx)	)
a	a	k8xC	a
všem	všecek	k3xTgMnPc3	všecek
ostatním	ostatní	k2eAgMnPc3d1	ostatní
(	(	kIx(	(
<g/>
o	o	k0	o
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tudíž	tudíž	k8xC	tudíž
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
následující	následující	k2eAgNnSc1d1	následující
výpis	výpis	k1gInSc4	výpis
interpretovat	interpretovat	k5eAaBmF	interpretovat
podle	podle	k7c2	podle
šablony	šablona	k1gFnSc2	šablona
"	"	kIx"	"
<g/>
duuugggooo	duuugggooo	k6eAd1	duuugggooo
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
počet	počet	k1gInSc1	počet
pevných	pevný	k2eAgInPc2d1	pevný
odkazů	odkaz	k1gInPc2	odkaz
<g/>
,	,	kIx,	,
majitel	majitel	k1gMnSc1	majitel
<g/>
,	,	kIx,	,
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
velikost	velikost	k1gFnSc1	velikost
<g/>
,	,	kIx,	,
datum	datum	k1gNnSc1	datum
úpravy	úprava	k1gFnSc2	úprava
<g/>
,	,	kIx,	,
jméno	jméno	k1gNnSc1	jméno
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
drwxr--r--	drwxr---	k?	drwxr--r--
1	[number]	k4	1
pepa	pepum	k1gNnSc2	pepum
editors	editors	k6eAd1	editors
4096	[number]	k4	4096
29	[number]	k4	29
<g/>
.	.	kIx.	.
kvě	kvě	k?	kvě
2008	[number]	k4	2008
navrhy	navrha	k1gFnSc2	navrha
</s>
</p>
<p>
<s>
Výstup	výstup	k1gInSc1	výstup
tedy	tedy	k9	tedy
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
navrhy	navrh	k1gMnPc4	navrh
je	být	k5eAaImIp3nS	být
složka	složka	k1gFnSc1	složka
<g/>
,	,	kIx,	,
vlastník	vlastník	k1gMnSc1	vlastník
má	mít	k5eAaImIp3nS	mít
rwx	rwx	k?	rwx
(	(	kIx(	(
<g/>
číst	číst	k5eAaImF	číst
<g/>
,	,	kIx,	,
zapisovat	zapisovat	k5eAaImF	zapisovat
<g/>
,	,	kIx,	,
spouštět	spouštět	k5eAaImF	spouštět
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
r	r	kA	r
(	(	kIx(	(
<g/>
číst	číst	k5eAaImF	číst
<g/>
)	)	kIx)	)
a	a	k8xC	a
ostatní	ostatní	k2eAgMnPc1d1	ostatní
r	r	kA	r
(	(	kIx(	(
<g/>
číst	číst	k5eAaImF	číst
<g/>
)	)	kIx)	)
–	–	k?	–
viz	vidět	k5eAaImRp2nS	vidět
přístupová	přístupový	k2eAgNnPc4d1	přístupové
práva	právo	k1gNnPc4	právo
v	v	k7c6	v
Unixu	Unix	k1gInSc6	Unix
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
BRANDEJS	Brandejs	k1gMnSc1	Brandejs
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
<g/>
.	.	kIx.	.
</s>
<s>
UNIX	UNIX	kA	UNIX
–	–	k?	–
LINUX	linux	k1gInSc1	linux
<g/>
.	.	kIx.	.
</s>
<s>
Praktický	praktický	k2eAgMnSc1d1	praktický
průvodce	průvodce	k1gMnSc1	průvodce
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
341	[number]	k4	341
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7169	[number]	k4	7169
<g/>
-	-	kIx~	-
<g/>
170	[number]	k4	170
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
