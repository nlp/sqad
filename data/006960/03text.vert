<s>
Spenglerův	Spenglerův	k2eAgInSc1d1	Spenglerův
pohár	pohár	k1gInSc1	pohár
(	(	kIx(	(
<g/>
Spengler	Spengler	k1gInSc1	Spengler
Cup	cup	k1gInSc1	cup
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pravidelně	pravidelně	k6eAd1	pravidelně
se	se	k3xPyFc4	se
konající	konající	k2eAgInSc1d1	konající
turnaj	turnaj	k1gInSc1	turnaj
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
<g/>
,	,	kIx,	,
odehrávající	odehrávající	k2eAgMnSc1d1	odehrávající
se	se	k3xPyFc4	se
ve	v	k7c6	v
švýcarskem	švýcarsko	k1gNnSc7	švýcarsko
Davosu	Davos	k1gInSc2	Davos
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejstarším	starý	k2eAgInSc7d3	nejstarší
hokejovým	hokejový	k2eAgInSc7d1	hokejový
turnajem	turnaj	k1gInSc7	turnaj
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
pořadatelem	pořadatel	k1gMnSc7	pořadatel
je	být	k5eAaImIp3nS	být
švýcarský	švýcarský	k2eAgInSc1d1	švýcarský
tým	tým	k1gInSc1	tým
HC	HC	kA	HC
Davos	Davos	k1gInSc1	Davos
<g/>
.	.	kIx.	.
</s>
<s>
Účastníci	účastník	k1gMnPc1	účastník
turnaje	turnaj	k1gInSc2	turnaj
jsou	být	k5eAaImIp3nP	být
zváni	zvát	k5eAaImNgMnP	zvát
právě	právě	k9	právě
tímto	tento	k3xDgInSc7	tento
týmem	tým	k1gInSc7	tým
<g/>
.	.	kIx.	.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
v	v	k7c6	v
termínu	termín	k1gInSc6	termín
mezi	mezi	k7c7	mezi
Vánoci	Vánoce	k1gFnPc7	Vánoce
a	a	k8xC	a
Novým	nový	k2eAgInSc7d1	nový
rokem	rok	k1gInSc7	rok
<g/>
.	.	kIx.	.
</s>
