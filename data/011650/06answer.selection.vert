<s>
Malý	malý	k2eAgInSc1d1	malý
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
gotickým	gotický	k2eAgInSc7d1	gotický
štítem	štít	k1gInSc7	štít
s	s	k7c7	s
červeným	červený	k2eAgNnSc7d1	červené
polem	pole	k1gNnSc7	pole
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
je	být	k5eAaImIp3nS	být
umístěn	umístěn	k2eAgInSc1d1	umístěn
historický	historický	k2eAgInSc1d1	historický
znak	znak	k1gInSc1	znak
Čech	Čechy	k1gFnPc2	Čechy
<g/>
:	:	kIx,	:
stříbrný	stříbrný	k2eAgInSc1d1	stříbrný
dvouocasý	dvouocasý	k2eAgInSc1d1	dvouocasý
lev	lev	k1gInSc1	lev
ve	v	k7c6	v
skoku	skok	k1gInSc6	skok
se	s	k7c7	s
zlatou	zlatý	k2eAgFnSc7d1	zlatá
zbrojí	zbroj	k1gFnSc7	zbroj
a	a	k8xC	a
zlatou	zlatý	k2eAgFnSc7d1	zlatá
korunou	koruna	k1gFnSc7	koruna
<g/>
.	.	kIx.	.
</s>
