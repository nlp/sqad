<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
používá	používat	k5eAaImIp3nS	používat
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
varianty	varianta	k1gFnPc4	varianta
<g/>
:	:	kIx,	:
malý	malý	k2eAgInSc1d1	malý
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
a	a	k8xC	a
velký	velký	k2eAgInSc1d1	velký
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
současná	současný	k2eAgFnSc1d1	současná
podoba	podoba	k1gFnSc1	podoba
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k8xC	i
způsoby	způsob	k1gInPc1	způsob
užití	užití	k1gNnSc2	užití
jsou	být	k5eAaImIp3nP	být
stanoveny	stanovit	k5eAaPmNgFnP	stanovit
ústavním	ústavní	k2eAgInSc7d1	ústavní
zákonem	zákon	k1gInSc7	zákon
číslo	číslo	k1gNnSc4	číslo
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
přijatým	přijatý	k2eAgInSc7d1	přijatý
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velký	velký	k2eAgInSc1d1	velký
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
i	i	k8xC	i
malý	malý	k2eAgInSc1d1	malý
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
státní	státní	k2eAgInPc4d1	státní
symboly	symbol	k1gInPc4	symbol
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Velký	velký	k2eAgInSc1d1	velký
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
==	==	k?	==
</s>
</p>
<p>
<s>
Velký	velký	k2eAgInSc1d1	velký
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
tvoří	tvořit	k5eAaImIp3nS	tvořit
čtvrcený	čtvrcený	k2eAgInSc1d1	čtvrcený
francouzský	francouzský	k2eAgInSc1d1	francouzský
štít	štít	k1gInSc1	štít
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
nenalezeno	nalezen	k2eNgNnSc1d1	nenalezeno
v	v	k7c6	v
uvedeném	uvedený	k2eAgInSc6d1	uvedený
zdroji	zdroj	k1gInSc6	zdroj
<g/>
]	]	kIx)	]
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
a	a	k8xC	a
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
poli	pole	k1gNnSc6	pole
je	být	k5eAaImIp3nS	být
historický	historický	k2eAgInSc1d1	historický
znak	znak	k1gInSc1	znak
Čech	Čechy	k1gFnPc2	Čechy
-	-	kIx~	-
stříbrný	stříbrný	k2eAgInSc1d1	stříbrný
dvouocasý	dvouocasý	k2eAgInSc1d1	dvouocasý
lev	lev	k1gInSc1	lev
ve	v	k7c6	v
skoku	skok	k1gInSc6	skok
se	s	k7c7	s
zlatou	zlatý	k2eAgFnSc7d1	zlatá
zbrojí	zbroj	k1gFnSc7	zbroj
a	a	k8xC	a
zlatou	zlatý	k2eAgFnSc7d1	zlatá
korunou	koruna	k1gFnSc7	koruna
na	na	k7c6	na
červeném	červený	k2eAgNnSc6d1	červené
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
poli	pole	k1gNnSc6	pole
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
znak	znak	k1gInSc1	znak
Moravy	Morava	k1gFnSc2	Morava
–	–	k?	–
orlice	orlice	k1gFnSc1	orlice
stříbrno-červeně	stříbrno-červeně	k6eAd1	stříbrno-červeně
šachovaná	šachovaný	k2eAgFnSc1d1	šachovaná
se	s	k7c7	s
zlatou	zlatý	k2eAgFnSc7d1	zlatá
zbrojí	zbroj	k1gFnSc7	zbroj
a	a	k8xC	a
korunou	koruna	k1gFnSc7	koruna
na	na	k7c6	na
modrém	modrý	k2eAgNnSc6d1	modré
poli	pole	k1gNnSc6	pole
(	(	kIx(	(
<g/>
moravská	moravský	k2eAgFnSc1d1	Moravská
orlice	orlice	k1gFnSc1	orlice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
poli	pole	k1gNnSc6	pole
je	být	k5eAaImIp3nS	být
znak	znak	k1gInSc1	znak
Slezska	Slezsko	k1gNnSc2	Slezsko
–	–	k?	–
černá	černý	k2eAgFnSc1d1	černá
orlice	orlice	k1gFnSc1	orlice
se	s	k7c7	s
stříbrným	stříbrný	k2eAgNnSc7d1	stříbrné
perisoniem	perisonium	k1gNnSc7	perisonium
(	(	kIx(	(
<g/>
půlměsícového	půlměsícový	k2eAgInSc2d1	půlměsícový
tvaru	tvar	k1gInSc2	tvar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
uprostřed	uprostřed	k6eAd1	uprostřed
s	s	k7c7	s
křížkem	křížek	k1gInSc7	křížek
<g/>
,	,	kIx,	,
ukončeným	ukončený	k2eAgNnSc7d1	ukončené
trojlístky	trojlístek	k1gInPc7	trojlístek
<g/>
,	,	kIx,	,
se	s	k7c7	s
zlatou	zlatý	k2eAgFnSc7d1	zlatá
korunou	koruna	k1gFnSc7	koruna
a	a	k8xC	a
červenou	červený	k2eAgFnSc7d1	červená
zbrojí	zbroj	k1gFnSc7	zbroj
na	na	k7c6	na
zlatém	zlatý	k2eAgInSc6d1	zlatý
podkladu	podklad	k1gInSc6	podklad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velký	velký	k2eAgInSc1d1	velký
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
spojení	spojení	k1gNnSc4	spojení
všech	všecek	k3xTgFnPc2	všecek
historických	historický	k2eAgFnPc2d1	historická
zemí	zem	k1gFnPc2	zem
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
návrhu	návrh	k1gInSc2	návrh
i	i	k8xC	i
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
provedení	provedení	k1gNnSc2	provedení
je	být	k5eAaImIp3nS	být
heraldik	heraldik	k1gMnSc1	heraldik
Jiří	Jiří	k1gMnSc1	Jiří
Louda	Louda	k1gMnSc1	Louda
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
vnější	vnější	k2eAgFnSc3d1	vnější
reprezentaci	reprezentace	k1gFnSc3	reprezentace
státu	stát	k1gInSc2	stát
a	a	k8xC	a
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgFnPc6	který
sídlí	sídlet	k5eAaImIp3nP	sídlet
orgány	orgán	k1gInPc1	orgán
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
státní	státní	k2eAgInPc1d1	státní
úřady	úřad	k1gInPc1	úřad
apod.	apod.	kA	apod.
Jeho	jeho	k3xOp3gMnSc1	jeho
originál	originál	k1gMnSc1	originál
je	být	k5eAaImIp3nS	být
v	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
chvíli	chvíle	k1gFnSc4	chvíle
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
ztracen	ztratit	k5eAaPmNgInS	ztratit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Oficiální	oficiální	k2eAgInSc4d1	oficiální
popis	popis	k1gInSc4	popis
===	===	k?	===
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgInSc4d1	oficiální
popis	popis	k1gInSc4	popis
velkého	velký	k2eAgInSc2d1	velký
státního	státní	k2eAgInSc2d1	státní
znaku	znak	k1gInSc2	znak
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Malý	malý	k2eAgInSc1d1	malý
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
==	==	k?	==
</s>
</p>
<p>
<s>
Malý	malý	k2eAgInSc1d1	malý
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
gotickým	gotický	k2eAgInSc7d1	gotický
štítem	štít	k1gInSc7	štít
s	s	k7c7	s
červeným	červený	k2eAgNnSc7d1	červené
polem	pole	k1gNnSc7	pole
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
je	být	k5eAaImIp3nS	být
umístěn	umístěn	k2eAgInSc1d1	umístěn
historický	historický	k2eAgInSc1d1	historický
znak	znak	k1gInSc1	znak
Čech	Čechy	k1gFnPc2	Čechy
<g/>
:	:	kIx,	:
stříbrný	stříbrný	k2eAgInSc1d1	stříbrný
dvouocasý	dvouocasý	k2eAgInSc1d1	dvouocasý
lev	lev	k1gInSc1	lev
ve	v	k7c6	v
skoku	skok	k1gInSc6	skok
se	s	k7c7	s
zlatou	zlatý	k2eAgFnSc7d1	zlatá
zbrojí	zbroj	k1gFnSc7	zbroj
a	a	k8xC	a
zlatou	zlatý	k2eAgFnSc7d1	zlatá
korunou	koruna	k1gFnSc7	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
malým	malý	k2eAgInSc7d1	malý
státním	státní	k2eAgInSc7d1	státní
znakem	znak	k1gInSc7	znak
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
nejprve	nejprve	k6eAd1	nejprve
jako	jako	k8xS	jako
znak	znak	k1gInSc1	znak
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
ČSFR	ČSFR	kA	ČSFR
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgMnS	používat
na	na	k7c6	na
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
ze	z	k7c2	z
státní	státní	k2eAgFnSc2d1	státní
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
na	na	k7c6	na
razítkách	razítko	k1gNnPc6	razítko
a	a	k8xC	a
pečetidlech	pečetidlo	k1gNnPc6	pečetidlo
úřadů	úřad	k1gInPc2	úřad
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
a	a	k8xC	a
samosprávy	samospráva	k1gFnSc2	samospráva
nebo	nebo	k8xC	nebo
na	na	k7c6	na
cedulích	cedule	k1gFnPc6	cedule
označujících	označující	k2eAgInPc2d1	označující
památné	památný	k2eAgInPc4d1	památný
stromy	strom	k1gInPc4	strom
<g/>
.	.	kIx.	.
</s>
<s>
Malý	malý	k2eAgInSc1d1	malý
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
se	se	k3xPyFc4	se
také	také	k9	také
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
sídel	sídlo	k1gNnPc2	sídlo
orgánů	orgán	k1gInPc2	orgán
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
pověřeny	pověřit	k5eAaPmNgInP	pověřit
výkonem	výkon	k1gInSc7	výkon
státní	státní	k2eAgFnSc2d1	státní
moci	moc	k1gFnSc2	moc
–	–	k?	–
soudy	soud	k1gInPc1	soud
<g/>
,	,	kIx,	,
exekutoři	exekutor	k1gMnPc1	exekutor
<g/>
,	,	kIx,	,
hygienické	hygienický	k2eAgFnSc2d1	hygienická
stanice	stanice	k1gFnSc2	stanice
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
===	===	k?	===
Oficiální	oficiální	k2eAgInSc4d1	oficiální
popis	popis	k1gInSc4	popis
===	===	k?	===
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgInSc4d1	oficiální
popis	popis	k1gInSc4	popis
malého	malý	k2eAgInSc2d1	malý
státního	státní	k2eAgInSc2d1	státní
znaku	znak	k1gInSc2	znak
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Dvouocasý	dvouocasý	k2eAgMnSc1d1	dvouocasý
lev	lev	k1gMnSc1	lev
==	==	k?	==
</s>
</p>
<p>
<s>
Český	český	k2eAgMnSc1d1	český
lev	lev	k1gMnSc1	lev
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
jednoocasý	jednoocasý	k2eAgInSc1d1	jednoocasý
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgInS	nahradit
ve	v	k7c6	v
12	[number]	k4	12
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
plamennou	plamenný	k2eAgFnSc4d1	plamenná
orlici	orlice	k1gFnSc4	orlice
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
ocas	ocas	k1gInSc1	ocas
byl	být	k5eAaImAgInS	být
ke	k	k7c3	k
lvu	lev	k1gInSc3	lev
ve	v	k7c6	v
znaku	znak	k1gInSc3	znak
přidán	přidán	k2eAgMnSc1d1	přidán
patrně	patrně	k6eAd1	patrně
jako	jako	k8xC	jako
odměna	odměna	k1gFnSc1	odměna
za	za	k7c4	za
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
císaři	císař	k1gMnSc3	císař
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
I.	I.	kA	I.
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
Sasům	Sas	k1gMnPc3	Sas
<g/>
,	,	kIx,	,
přesvědčivým	přesvědčivý	k2eAgInSc7d1	přesvědčivý
důkazem	důkaz	k1gInSc7	důkaz
je	být	k5eAaImIp3nS	být
však	však	k9	však
až	až	k9	až
pečeť	pečeť	k1gFnSc4	pečeť
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
používal	používat	k5eAaImAgMnS	používat
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Heraldika	heraldika	k1gFnSc1	heraldika
však	však	k9	však
obecně	obecně	k6eAd1	obecně
chápe	chápat	k5eAaImIp3nS	chápat
zdvojenost	zdvojenost	k1gFnSc4	zdvojenost
nepárových	párový	k2eNgInPc2d1	nepárový
orgánů	orgán	k1gInPc2	orgán
(	(	kIx(	(
<g/>
ocas	ocas	k1gInSc1	ocas
či	či	k8xC	či
hlava	hlava	k1gFnSc1	hlava
<g/>
)	)	kIx)	)
erbovních	erbovní	k2eAgNnPc2d1	erbovní
zvířat	zvíře	k1gNnPc2	zvíře
jako	jako	k8xS	jako
znak	znak	k1gInSc1	znak
mužského	mužský	k2eAgNnSc2d1	mužské
pohlaví	pohlaví	k1gNnSc2	pohlaví
vyobrazeného	vyobrazený	k2eAgNnSc2d1	vyobrazené
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
symboly	symbol	k1gInPc7	symbol
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
Heraldické	heraldický	k2eAgInPc1d1	heraldický
znaky	znak	k1gInPc1	znak
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
</s>
</p>
<p>
<s>
Symboly	symbol	k1gInPc1	symbol
krajů	kraj	k1gInPc2	kraj
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInPc1d1	státní
znaky	znak	k1gInPc1	znak
Československa	Československo	k1gNnSc2	Československo
</s>
</p>
<p>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
československých	československý	k2eAgInPc2d1	československý
státních	státní	k2eAgInPc2d1	státní
symbolů	symbol	k1gInPc2	symbol
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Státní	státní	k2eAgInSc4d1	státní
znak	znak	k1gInSc4	znak
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Vexilolognet	Vexilolognet	k1gInSc1	Vexilolognet
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
Historie	historie	k1gFnSc1	historie
českých	český	k2eAgInPc2d1	český
státních	státní	k2eAgInPc2d1	státní
symbolů	symbol	k1gInPc2	symbol
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Vlastenci	vlastenec	k1gMnPc1	vlastenec
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
Znaky	znak	k1gInPc7	znak
a	a	k8xC	a
symboly	symbol	k1gInPc7	symbol
naší	náš	k3xOp1gFnSc2	náš
vlasti	vlast	k1gFnSc2	vlast
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Ceskavlajka	Ceskavlajka	k1gFnSc1	Ceskavlajka
<g/>
.	.	kIx.	.
<g/>
blog	blog	k1gInSc1	blog
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Hrad	hrad	k1gInSc1	hrad
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
Státní	státní	k2eAgInPc4d1	státní
symboly	symbol	k1gInPc4	symbol
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Senát	senát	k1gInSc1	senát
PČR	PČR	kA	PČR
–	–	k?	–
Státní	státní	k2eAgInSc4d1	státní
znak	znak	k1gInSc4	znak
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc1	jeho
předchůdci	předchůdce	k1gMnPc1	předchůdce
a	a	k8xC	a
současná	současný	k2eAgFnSc1d1	současná
podoba	podoba	k1gFnSc1	podoba
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Vláda	vláda	k1gFnSc1	vláda
ČR	ČR	kA	ČR
–	–	k?	–
Historie	historie	k1gFnPc4	historie
státního	státní	k2eAgInSc2d1	státní
znaku	znak	k1gInSc2	znak
</s>
</p>
