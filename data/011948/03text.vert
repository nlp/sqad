<p>
<s>
V	v	k7c6	v
křesťanském	křesťanský	k2eAgInSc6d1	křesťanský
kalendáři	kalendář	k1gInSc6	kalendář
je	být	k5eAaImIp3nS	být
Velký	velký	k2eAgInSc4d1	velký
pátek	pátek	k1gInSc4	pátek
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
dies	dies	k1gInSc1	dies
passionis	passionis	k1gFnSc2	passionis
Domini	Domin	k2eAgMnPc1d1	Domin
<g/>
)	)	kIx)	)
pátkem	pátek	k1gInSc7	pátek
před	před	k7c7	před
Velikonocemi	Velikonoce	k1gFnPc7	Velikonoce
<g/>
,	,	kIx,	,
součást	součást	k1gFnSc4	součást
Svatého	svatý	k2eAgInSc2d1	svatý
týdne	týden	k1gInSc2	týden
a	a	k8xC	a
velikonočního	velikonoční	k2eAgInSc2d1	velikonoční
tridua	tridu	k1gInSc2	tridu
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
den	den	k1gInSc1	den
je	být	k5eAaImIp3nS	být
připomínkou	připomínka	k1gFnSc7	připomínka
utrpení	utrpení	k1gNnSc2	utrpení
(	(	kIx(	(
<g/>
pašijí	pašije	k1gFnPc2	pašije
<g/>
)	)	kIx)	)
a	a	k8xC	a
smrti	smrt	k1gFnSc6	smrt
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
na	na	k7c6	na
kříži	kříž	k1gInSc6	kříž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
církvích	církev	k1gFnPc6	církev
může	moct	k5eAaImIp3nS	moct
tento	tento	k3xDgInSc4	tento
pohyblivý	pohyblivý	k2eAgInSc4d1	pohyblivý
svátek	svátek	k1gInSc4	svátek
připadnout	připadnout	k5eAaPmF	připadnout
na	na	k7c4	na
datum	datum	k1gNnSc4	datum
od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
do	do	k7c2	do
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
včetně	včetně	k7c2	včetně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Liturgie	liturgie	k1gFnSc2	liturgie
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c4	na
Velký	velký	k2eAgInSc4d1	velký
pátek	pátek	k1gInSc4	pátek
sa	sa	k?	sa
neslaví	slavit	k5eNaImIp3nS	slavit
mše	mše	k1gFnSc1	mše
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
katolické	katolický	k2eAgFnSc6d1	katolická
liturgii	liturgie	k1gFnSc6	liturgie
se	se	k3xPyFc4	se
čtou	číst	k5eAaImIp3nP	číst
z	z	k7c2	z
Písma	písmo	k1gNnSc2	písmo
Janovy	Janův	k2eAgFnSc2d1	Janova
pašije	pašije	k1gFnSc2	pašije
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
podané	podaný	k2eAgNnSc1d1	podané
dramaticky	dramaticky	k6eAd1	dramaticky
nebo	nebo	k8xC	nebo
hudebně	hudebně	k6eAd1	hudebně
<g/>
.	.	kIx.	.
</s>
<s>
Obřady	obřad	k1gInPc1	obřad
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
přímluvnou	přímluvný	k2eAgFnSc7d1	Přímluvná
modlitbou	modlitba	k1gFnSc7	modlitba
za	za	k7c4	za
církev	církev	k1gFnSc4	církev
<g/>
,	,	kIx,	,
za	za	k7c4	za
papeže	papež	k1gMnSc4	papež
<g/>
,	,	kIx,	,
za	za	k7c4	za
služebníky	služebník	k1gMnPc4	služebník
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
za	za	k7c4	za
všechny	všechen	k3xTgMnPc4	všechen
věřící	věřící	k1gMnPc4	věřící
<g/>
,	,	kIx,	,
za	za	k7c4	za
katechumeny	katechumen	k1gMnPc4	katechumen
<g/>
,	,	kIx,	,
za	za	k7c4	za
jednotu	jednota	k1gFnSc4	jednota
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
,	,	kIx,	,
za	za	k7c4	za
židy	žid	k1gMnPc4	žid
<g/>
,	,	kIx,	,
za	za	k7c4	za
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
věří	věřit	k5eAaImIp3nS	věřit
v	v	k7c4	v
Boha	bůh	k1gMnSc4	bůh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nevěří	věřit	k5eNaImIp3nS	věřit
v	v	k7c4	v
Krista	Kristus	k1gMnSc4	Kristus
<g/>
,	,	kIx,	,
za	za	k7c4	za
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
nevěří	věřit	k5eNaImIp3nS	věřit
v	v	k7c4	v
Boha	bůh	k1gMnSc4	bůh
<g/>
,	,	kIx,	,
za	za	k7c4	za
politiky	politik	k1gMnPc4	politik
a	a	k8xC	a
státníky	státník	k1gMnPc4	státník
<g/>
,	,	kIx,	,
za	za	k7c4	za
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
trpí	trpět	k5eAaImIp3nS	trpět
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholem	vrchol	k1gInSc7	vrchol
obřadů	obřad	k1gInPc2	obřad
je	být	k5eAaImIp3nS	být
uctívání	uctívání	k1gNnSc1	uctívání
kříže	kříž	k1gInSc2	kříž
<g/>
.	.	kIx.	.
</s>
<s>
Eucharistie	eucharistie	k1gFnSc1	eucharistie
se	se	k3xPyFc4	se
v	v	k7c4	v
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
neslaví	slavit	k5eNaImIp3nP	slavit
<g/>
.	.	kIx.	.
</s>
<s>
Podává	podávat	k5eAaImIp3nS	podávat
se	se	k3xPyFc4	se
však	však	k9	však
svaté	svatý	k2eAgNnSc1d1	svaté
přijímání	přijímání	k1gNnSc1	přijímání
(	(	kIx(	(
<g/>
z	z	k7c2	z
hostií	hostie	k1gFnPc2	hostie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
byly	být	k5eAaImAgFnP	být
proměněny	proměnit	k5eAaPmNgFnP	proměnit
na	na	k7c4	na
Zelený	zelený	k2eAgInSc4d1	zelený
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výjimky	výjimka	k1gFnPc1	výjimka
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
vzácně	vzácně	k6eAd1	vzácně
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
Svatý	svatý	k2eAgInSc1d1	svatý
stolec	stolec	k1gInSc1	stolec
povolil	povolit	k5eAaPmAgInS	povolit
sloužení	sloužení	k1gNnSc4	sloužení
pohřební	pohřební	k2eAgFnSc2d1	pohřební
mše	mše	k1gFnSc2	mše
při	při	k7c6	při
státním	státní	k2eAgInSc6d1	státní
pohřbu	pohřeb	k1gInSc6	pohřeb
205	[number]	k4	205
obětí	oběť	k1gFnPc2	oběť
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
postihlo	postihnout	k5eAaPmAgNnS	postihnout
kraj	kraj	k1gInSc4	kraj
Abruzzo	Abruzza	k1gFnSc5	Abruzza
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c4	na
Velký	velký	k2eAgInSc4d1	velký
pátek	pátek	k1gInSc4	pátek
se	se	k3xPyFc4	se
papež	papež	k1gMnSc1	papež
modlí	modlit	k5eAaImIp3nS	modlit
Křížovou	křížový	k2eAgFnSc4d1	křížová
cestu	cesta	k1gFnSc4	cesta
v	v	k7c6	v
římském	římský	k2eAgNnSc6d1	římské
Koloseu	Koloseum	k1gNnSc6	Koloseum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pravoslavní	pravoslavný	k2eAgMnPc1d1	pravoslavný
též	též	k9	též
eucharistii	eucharistie	k1gFnSc3	eucharistie
neslaví	slavit	k5eNaImIp3nP	slavit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
třikrát	třikrát	k6eAd1	třikrát
se	se	k3xPyFc4	se
během	během	k7c2	během
dne	den	k1gInSc2	den
scházejí	scházet	k5eAaImIp3nP	scházet
k	k	k7c3	k
modlitbě	modlitba	k1gFnSc3	modlitba
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
pátek	pátek	k1gInSc1	pátek
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
dnů	den	k1gInPc2	den
přísného	přísný	k2eAgInSc2d1	přísný
půstu	půst	k1gInSc2	půst
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zvláštní	zvláštní	k2eAgFnPc1d1	zvláštní
bohoslužby	bohoslužba	k1gFnPc1	bohoslužba
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
i	i	k9	i
v	v	k7c6	v
protestantských	protestantský	k2eAgFnPc6d1	protestantská
církvích	církev	k1gFnPc6	církev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úřední	úřední	k2eAgInSc4d1	úřední
status	status	k1gInSc4	status
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Asi	asi	k9	asi
čtyřicet	čtyřicet	k4xCc1	čtyřicet
států	stát	k1gInPc2	stát
uznává	uznávat	k5eAaImIp3nS	uznávat
Velký	velký	k2eAgInSc4d1	velký
pátek	pátek	k1gInSc4	pátek
jako	jako	k8xC	jako
den	den	k1gInSc4	den
pracovního	pracovní	k2eAgInSc2d1	pracovní
klidu	klid	k1gInSc2	klid
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
například	například	k6eAd1	například
Austrálie	Austrálie	k1gFnPc1	Austrálie
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
Česko	Česko	k1gNnSc1	Česko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
mají	mít	k5eAaImIp3nP	mít
nárok	nárok	k1gInSc4	nárok
na	na	k7c4	na
den	den	k1gInSc4	den
volna	volno	k1gNnSc2	volno
pouze	pouze	k6eAd1	pouze
evangelíci	evangelík	k1gMnPc1	evangelík
<g/>
,	,	kIx,	,
starokatolíci	starokatolík	k1gMnPc1	starokatolík
a	a	k8xC	a
metodisté	metodista	k1gMnPc1	metodista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
německých	německý	k2eAgFnPc2d1	německá
spolkových	spolkový	k2eAgFnPc2d1	spolková
zemí	zem	k1gFnPc2	zem
platí	platit	k5eAaImIp3nS	platit
na	na	k7c4	na
Velký	velký	k2eAgInSc4d1	velký
pátek	pátek	k1gInSc4	pátek
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
v	v	k7c4	v
Den	den	k1gInSc4	den
národního	národní	k2eAgInSc2d1	národní
smutku	smutek	k1gInSc2	smutek
<g/>
)	)	kIx)	)
zákaz	zákaz	k1gInSc1	zákaz
pořádání	pořádání	k1gNnSc2	pořádání
tanečních	taneční	k2eAgFnPc2d1	taneční
slavností	slavnost	k1gFnPc2	slavnost
a	a	k8xC	a
sportovních	sportovní	k2eAgFnPc2d1	sportovní
akcí	akce	k1gFnPc2	akce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Velký	velký	k2eAgInSc4d1	velký
pátek	pátek	k1gInSc4	pátek
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
===	===	k?	===
</s>
</p>
<p>
<s>
Velký	velký	k2eAgInSc1d1	velký
pátek	pátek	k1gInSc1	pátek
byl	být	k5eAaImAgInS	být
slaven	slaven	k2eAgInSc4d1	slaven
jako	jako	k8xC	jako
státní	státní	k2eAgInSc4d1	státní
svátek	svátek	k1gInSc4	svátek
do	do	k7c2	do
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Debata	debata	k1gFnSc1	debata
o	o	k7c4	o
obnovení	obnovení	k1gNnSc4	obnovení
Velkého	velký	k2eAgInSc2d1	velký
pátku	pátek	k1gInSc2	pátek
jako	jako	k8xS	jako
státního	státní	k2eAgInSc2d1	státní
svátku	svátek	k1gInSc2	svátek
probíhala	probíhat	k5eAaImAgFnS	probíhat
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
periodicky	periodicky	k6eAd1	periodicky
kolem	kolem	k7c2	kolem
oslav	oslava	k1gFnPc2	oslava
Velikonoc	Velikonoce	k1gFnPc2	Velikonoce
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
KDU-ČSL	KDU-ČSL	k1gFnSc2	KDU-ČSL
usilovali	usilovat	k5eAaImAgMnP	usilovat
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
o	o	k7c4	o
obnovu	obnova	k1gFnSc4	obnova
Velkého	velký	k2eAgInSc2d1	velký
pátku	pátek	k1gInSc2	pátek
jako	jako	k8xS	jako
dne	den	k1gInSc2	den
pracovního	pracovní	k2eAgNnSc2d1	pracovní
volna	volno	k1gNnSc2	volno
například	například	k6eAd1	například
senátoři	senátor	k1gMnPc1	senátor
Martin	Martin	k1gMnSc1	Martin
Mejstřík	Mejstřík	k1gMnSc1	Mejstřík
<g/>
,	,	kIx,	,
Soňa	Soňa	k1gFnSc1	Soňa
Paukrtová	Paukrtová	k1gFnSc1	Paukrtová
a	a	k8xC	a
Jaromír	Jaromír	k1gMnSc1	Jaromír
Štětina	štětina	k1gFnSc1	štětina
<g/>
.2	.2	k4	.2
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2015	[number]	k4	2015
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
premiér	premiér	k1gMnSc1	premiér
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Sobotka	Sobotka	k1gMnSc1	Sobotka
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
podporu	podpora	k1gFnSc4	podpora
návrhu	návrh	k1gInSc2	návrh
koaliční	koaliční	k2eAgFnSc2d1	koaliční
KDU-ČSL	KDU-ČSL	k1gFnSc2	KDU-ČSL
na	na	k7c4	na
obnovení	obnovení	k1gNnSc4	obnovení
Velkého	velký	k2eAgInSc2d1	velký
pátku	pátek	k1gInSc2	pátek
jako	jako	k8xC	jako
státního	státní	k2eAgInSc2d1	státní
svátku	svátek	k1gInSc2	svátek
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2015	[number]	k4	2015
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
schválila	schválit	k5eAaPmAgFnS	schválit
návrh	návrh	k1gInSc4	návrh
zákona	zákon	k1gInSc2	zákon
obsahující	obsahující	k2eAgInSc4d1	obsahující
Velký	velký	k2eAgInSc4d1	velký
pátek	pátek	k1gInSc4	pátek
jako	jako	k8xC	jako
svátek	svátek	k1gInSc4	svátek
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2015	[number]	k4	2015
tato	tento	k3xDgFnSc1	tento
novela	novela	k1gFnSc1	novela
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
ostatních	ostatní	k2eAgInPc6d1	ostatní
svátcích	svátek	k1gInPc6	svátek
<g/>
,	,	kIx,	,
o	o	k7c6	o
významných	významný	k2eAgInPc6d1	významný
dnech	den	k1gInPc6	den
a	a	k8xC	a
o	o	k7c6	o
dnech	den	k1gInPc6	den
pracovního	pracovní	k2eAgInSc2d1	pracovní
klidu	klid	k1gInSc2	klid
(	(	kIx(	(
<g/>
č.	č.	k?	č.
245	[number]	k4	245
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
prošla	projít	k5eAaPmAgFnS	projít
nejtěsnější	těsný	k2eAgFnSc7d3	nejtěsnější
možnou	možný	k2eAgFnSc7d1	možná
většinou	většina	k1gFnSc7	většina
v	v	k7c6	v
Senátu	senát	k1gInSc6	senát
a	a	k8xC	a
11	[number]	k4	11
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2015	[number]	k4	2015
prezident	prezident	k1gMnSc1	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
příslušný	příslušný	k2eAgInSc1d1	příslušný
zákon	zákon	k1gInSc1	zákon
podepsal	podepsat	k5eAaPmAgInS	podepsat
<g/>
.	.	kIx.	.
</s>
<s>
Vyšel	vyjít	k5eAaPmAgInS	vyjít
pod	pod	k7c7	pod
č.	č.	k?	č.
359	[number]	k4	359
<g/>
/	/	kIx~	/
<g/>
2015	[number]	k4	2015
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tradice	tradice	k1gFnSc1	tradice
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c4	na
Velký	velký	k2eAgInSc4d1	velký
pátek	pátek	k1gInSc4	pátek
se	se	k3xPyFc4	se
vážou	vázat	k5eAaImIp3nP	vázat
i	i	k9	i
některé	některý	k3yIgFnPc1	některý
pověry	pověra	k1gFnPc1	pověra
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
součástí	součást	k1gFnSc7	součást
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jenom	jenom	k9	jenom
lidovými	lidový	k2eAgFnPc7d1	lidová
tradicemi	tradice	k1gFnPc7	tradice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lidových	lidový	k2eAgFnPc6d1	lidová
pověrách	pověra	k1gFnPc6	pověra
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
den	den	k1gInSc1	den
spojován	spojován	k2eAgInSc1d1	spojován
s	s	k7c7	s
magickými	magický	k2eAgFnPc7d1	magická
silami	síla	k1gFnPc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgFnP	mít
otevírat	otevírat	k5eAaImF	otevírat
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vydávaly	vydávat	k5eAaImAgFnP	vydávat
poklady	poklad	k1gInPc4	poklad
<g/>
;	;	kIx,	;
v	v	k7c4	v
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
se	se	k3xPyFc4	se
nemělo	mít	k5eNaImAgNnS	mít
nic	nic	k3yNnSc1	nic
půjčovat	půjčovat	k5eAaImF	půjčovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
půjčená	půjčený	k2eAgFnSc1d1	půjčená
věc	věc	k1gFnSc1	věc
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
očarována	očarován	k2eAgFnSc1d1	očarována
<g/>
;	;	kIx,	;
nesmělo	smět	k5eNaImAgNnS	smět
se	se	k3xPyFc4	se
hýbat	hýbat	k5eAaImF	hýbat
se	s	k7c7	s
zemí	zem	k1gFnSc7	zem
(	(	kIx(	(
<g/>
rýt	rýt	k1gInSc1	rýt
<g/>
,	,	kIx,	,
kopat	kopat	k5eAaImF	kopat
<g/>
,	,	kIx,	,
okopávat	okopávat	k5eAaImF	okopávat
<g/>
)	)	kIx)	)
ani	ani	k8xC	ani
prát	prát	k5eAaImF	prát
prádlo	prádlo	k1gNnSc4	prádlo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
namáčeno	namáčet	k5eAaImNgNnS	namáčet
do	do	k7c2	do
Kristovy	Kristův	k2eAgFnSc2d1	Kristova
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pranostiky	pranostika	k1gFnSc2	pranostika
===	===	k?	===
</s>
</p>
<p>
<s>
Česko	Česko	k1gNnSc1	Česko
</s>
</p>
<p>
<s>
Na	na	k7c4	na
Zelený	zelený	k2eAgInSc4d1	zelený
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
hrachy	hrách	k1gInPc7	hrách
zasívej	zasívat	k5eAaImRp2nS	zasívat
<g/>
,	,	kIx,	,
na	na	k7c4	na
Velký	velký	k2eAgInSc4d1	velký
pátek	pátek	k1gInSc4	pátek
se	s	k7c7	s
zemí	zem	k1gFnSc7	zem
nehýbej	hýbat	k5eNaImRp2nS	hýbat
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
Prší	pršet	k5eAaImIp3nS	pršet
<g/>
-li	i	k?	-li
na	na	k7c4	na
Velký	velký	k2eAgInSc4d1	velký
pátek	pátek	k1gInSc4	pátek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
doufání	doufání	k1gNnSc3	doufání
úroda	úroda	k1gFnSc1	úroda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velký	velký	k2eAgInSc1d1	velký
pátek	pátek	k1gInSc1	pátek
deštivý	deštivý	k2eAgInSc1d1	deštivý
-	-	kIx~	-
dělá	dělat	k5eAaImIp3nS	dělat
rok	rok	k1gInSc4	rok
žíznivý	žíznivý	k2eAgInSc4d1	žíznivý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velký	velký	k2eAgInSc1d1	velký
pátek	pátek	k1gInSc1	pátek
vláha	vláha	k1gFnSc1	vláha
-	-	kIx~	-
úrodu	úroda	k1gFnSc4	úroda
zmáhá	zmáhat	k5eAaImIp3nS	zmáhat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
na	na	k7c4	na
Velký	velký	k2eAgInSc4d1	velký
pátek	pátek	k1gInSc4	pátek
hřmí	hřmět	k5eAaImIp3nS	hřmět
<g/>
,	,	kIx,	,
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
se	se	k3xPyFc4	se
urodí	urodit	k5eAaPmIp3nS	urodit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Pašije	pašije	k1gFnSc1	pašije
</s>
</p>
<p>
<s>
Pilát	Pilát	k1gMnSc1	Pilát
Pontský	pontský	k2eAgMnSc1d1	pontský
</s>
</p>
<p>
<s>
Křížová	křížový	k2eAgFnSc1d1	křížová
cesta	cesta	k1gFnSc1	cesta
(	(	kIx(	(
<g/>
Nesení	nesení	k1gNnSc1	nesení
kříže	kříž	k1gInSc2	kříž
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Kříž	Kříž	k1gMnSc1	Kříž
(	(	kIx(	(
<g/>
Krucifix	krucifix	k1gInSc1	krucifix
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ukřižování	ukřižování	k1gNnSc1	ukřižování
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
</s>
</p>
<p>
<s>
Bolestný	bolestný	k2eAgInSc4d1	bolestný
pátek	pátek	k1gInSc4	pátek
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Velký	velký	k2eAgInSc4d1	velký
pátek	pátek	k1gInSc4	pátek
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
Velký	velký	k2eAgInSc1d1	velký
pátek	pátek	k1gInSc1	pátek
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Průvodce	průvodce	k1gMnSc1	průvodce
Velikonocemi	Velikonoce	k1gFnPc7	Velikonoce
</s>
</p>
