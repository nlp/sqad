<p>
<s>
Populární	populární	k2eAgFnSc1d1	populární
hudba	hudba	k1gFnSc1	hudba
je	být	k5eAaImIp3nS	být
kategorie	kategorie	k1gFnSc1	kategorie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
veškeré	veškerý	k3xTgInPc4	veškerý
hudební	hudební	k2eAgInPc4d1	hudební
styly	styl	k1gInPc4	styl
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
umělecké	umělecký	k2eAgFnSc2d1	umělecká
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
tradiční	tradiční	k2eAgFnSc2d1	tradiční
<g/>
"	"	kIx"	"
folkové	folkový	k2eAgFnSc2d1	folková
a	a	k8xC	a
klasické	klasický	k2eAgFnSc2d1	klasická
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
někdy	někdy	k6eAd1	někdy
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
označení	označení	k1gNnSc1	označení
pop	pop	k1gMnSc1	pop
music	music	k1gMnSc1	music
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
hudební	hudební	k2eAgInSc4d1	hudební
žánr	žánr	k1gInSc4	žánr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Populární	populární	k2eAgFnSc1d1	populární
hudba	hudba	k1gFnSc1	hudba
se	se	k3xPyFc4	se
po	po	k7c6	po
dekádách	dekáda	k1gFnPc6	dekáda
měnila	měnit	k5eAaImAgFnS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
big	big	k?	big
bandová	bandový	k2eAgFnSc1d1	bandový
hudba	hudba	k1gFnSc1	hudba
má	mít	k5eAaImIp3nS	mít
i	i	k8xC	i
dnes	dnes	k6eAd1	dnes
nějaké	nějaký	k3yIgMnPc4	nějaký
fanoušky	fanoušek	k1gMnPc4	fanoušek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ti	ten	k3xDgMnPc1	ten
už	už	k6eAd1	už
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
starší	starý	k2eAgFnSc4d2	starší
generaci	generace	k1gFnSc4	generace
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ve	v	k7c6	v
čtyřicátých	čtyřicátý	k4xOgNnPc6	čtyřicátý
letech	léto	k1gNnPc6	léto
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
byl	být	k5eAaImAgInS	být
žánr	žánr	k1gInSc4	žánr
oblíben	oblíben	k2eAgInSc4d1	oblíben
i	i	k9	i
u	u	k7c2	u
mladých	mladý	k2eAgMnPc2d1	mladý
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgInPc2	některý
žánrů	žánr	k1gInPc2	žánr
jako	jako	k9	jako
třeba	třeba	k6eAd1	třeba
ragtimu	ragtime	k1gInSc2	ragtime
dávno	dávno	k6eAd1	dávno
vymizela	vymizet	k5eAaPmAgFnS	vymizet
cílová	cílový	k2eAgFnSc1d1	cílová
generace	generace	k1gFnSc1	generace
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
již	již	k6eAd1	již
umřeli	umřít	k5eAaPmAgMnP	umřít
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
mladými	mladý	k1gMnPc7	mladý
občas	občas	k6eAd1	občas
setkáváme	setkávat	k5eAaImIp1nP	setkávat
s	s	k7c7	s
talentovanými	talentovaný	k2eAgMnPc7d1	talentovaný
lidmi	člověk	k1gMnPc7	člověk
stvořenými	stvořený	k2eAgFnPc7d1	stvořená
na	na	k7c4	na
hraní	hraní	k1gNnSc4	hraní
jazzu	jazz	k1gInSc2	jazz
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gFnSc3	jeho
velmi	velmi	k6eAd1	velmi
obohacenému	obohacený	k2eAgNnSc3d1	obohacené
harmonickému	harmonický	k2eAgNnSc3d1	harmonické
cítění	cítění	k1gNnSc3	cítění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Žánry	žánr	k1gInPc7	žánr
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
==	==	k?	==
</s>
</p>
<p>
<s>
kytarové	kytarový	k2eAgFnPc1d1	kytarová
<g/>
:	:	kIx,	:
beat	beat	k1gInSc1	beat
<g/>
,	,	kIx,	,
rock	rock	k1gInSc1	rock
<g/>
,	,	kIx,	,
heavy	heava	k1gFnPc1	heava
metal	metal	k1gInSc1	metal
<g/>
,	,	kIx,	,
punk	punk	k1gInSc1	punk
rock	rock	k1gInSc1	rock
</s>
</p>
<p>
<s>
vokální	vokální	k2eAgNnSc1d1	vokální
<g/>
:	:	kIx,	:
Hip	hip	k0	hip
hop	hop	k0	hop
<g/>
,	,	kIx,	,
rap	rap	k1gMnSc1	rap
<g/>
,	,	kIx,	,
rhythm	rhythm	k1gMnSc1	rhythm
and	and	k?	and
blues	blues	k1gNnSc2	blues
</s>
</p>
<p>
<s>
elektronické	elektronický	k2eAgNnSc1d1	elektronické
<g/>
:	:	kIx,	:
techno	techno	k1gNnSc1	techno
<g/>
,	,	kIx,	,
house	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
rave	rave	k1gNnSc1	rave
</s>
</p>
<p>
<s>
pop	pop	k1gMnSc1	pop
</s>
</p>
<p>
<s>
===	===	k?	===
Vliv	vliv	k1gInSc4	vliv
hudby	hudba	k1gFnSc2	hudba
na	na	k7c4	na
populární	populární	k2eAgFnSc4d1	populární
hudbu	hudba	k1gFnSc4	hudba
===	===	k?	===
</s>
</p>
<p>
<s>
jazz	jazz	k1gInSc1	jazz
obecně	obecně	k6eAd1	obecně
–	–	k?	–
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgInP	vyvinout
kytarové	kytarový	k2eAgInPc1d1	kytarový
žánry	žánr	k1gInPc1	žánr
</s>
</p>
<p>
<s>
swing	swing	k1gInSc1	swing
–	–	k?	–
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
taneční	taneční	k1gFnPc1	taneční
žánryblues	žánryblues	k1gInSc1	žánryblues
–	–	k?	–
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
kytarové	kytarový	k2eAgInPc4d1	kytarový
žánry	žánr	k1gInPc4	žánr
</s>
</p>
<p>
<s>
folk	folk	k1gInSc1	folk
-	-	kIx~	-
melodie	melodie	k1gFnPc1	melodie
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
