<s>
Jak	jak	k6eAd1	jak
říkáme	říkat	k5eAaImIp1nP	říkat
hudbě	hudba	k1gFnSc3	hudba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
veškeré	veškerý	k3xTgInPc4	veškerý
hudební	hudební	k2eAgInPc4d1	hudební
styly	styl	k1gInPc4	styl
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
umělecké	umělecký	k2eAgFnSc2d1	umělecká
<g/>
,	,	kIx,	,
folkové	folkový	k2eAgFnSc2d1	folková
a	a	k8xC	a
klasické	klasický	k2eAgFnSc2d1	klasická
hudby	hudba	k1gFnSc2	hudba
<g/>
?	?	kIx.	?
</s>
