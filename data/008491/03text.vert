<p>
<s>
Václav	Václav	k1gMnSc1	Václav
<g/>
,	,	kIx,	,
alias	alias	k9	alias
Věnceslav	Věnceslava	k1gFnPc2	Věnceslava
Metelka	Metelka	k?	Metelka
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
vysocké	vysocký	k2eAgFnSc2d1	Vysocká
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
matriky	matrika	k1gFnSc2	matrika
narozených	narozený	k2eAgInPc2d1	narozený
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
nebo	nebo	k8xC	nebo
podle	podle	k7c2	podle
poznámky	poznámka	k1gFnSc2	poznámka
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
rodinných	rodinný	k2eAgInPc2d1	rodinný
záznamů	záznam	k1gInPc2	záznam
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1807	[number]	k4	1807
Sklenařice	Sklenařice	k1gFnSc2	Sklenařice
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1867	[number]	k4	1867
Paseky	paseka	k1gFnSc2	paseka
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
učitel	učitel	k1gMnSc1	učitel
<g/>
,	,	kIx,	,
houslař	houslař	k1gMnSc1	houslař
<g/>
,	,	kIx,	,
houslista	houslista	k1gMnSc1	houslista
a	a	k8xC	a
písmák	písmák	k1gMnSc1	písmák
<g/>
,	,	kIx,	,
prototyp	prototyp	k1gInSc1	prototyp
"	"	kIx"	"
<g/>
zapadlého	zapadlý	k2eAgMnSc4d1	zapadlý
vlastence	vlastenec	k1gMnSc4	vlastenec
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
nejstarší	starý	k2eAgMnSc1d3	nejstarší
syn	syn	k1gMnSc1	syn
chalupníka	chalupník	k1gMnSc2	chalupník
Jáchyma	Jáchym	k1gMnSc2	Jáchym
Metelky	Metelky	k?	Metelky
ve	v	k7c6	v
Sklenařicích	Sklenařik	k1gInPc6	Sklenařik
čp.	čp.	k?	čp.
11	[number]	k4	11
<g/>
,	,	kIx,	,
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
písmákem	písmák	k1gMnSc7	písmák
a	a	k8xC	a
hudebníkem	hudebník	k1gMnSc7	hudebník
Matějem	Matěj	k1gMnSc7	Matěj
Hájkem	Hájek	k1gMnSc7	Hájek
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ho	on	k3xPp3gMnSc4	on
učil	učít	k5eAaPmAgMnS	učít
také	také	k9	také
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
housle	housle	k1gFnPc4	housle
<g/>
,	,	kIx,	,
na	na	k7c4	na
flétnu	flétna	k1gFnSc4	flétna
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
deseti	deset	k4xCc2	deset
let	léto	k1gNnPc2	léto
chodíval	chodívat	k5eAaImAgInS	chodívat
hrát	hrát	k5eAaImF	hrát
po	po	k7c6	po
hospodách	hospodách	k?	hospodách
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
zkušené	zkušená	k1gFnSc6	zkušená
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
učil	učít	k5eAaPmAgMnS	učít
se	s	k7c7	s
truhlářem	truhlář	k1gMnSc7	truhlář
a	a	k8xC	a
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
Náchodě	Náchod	k1gInSc6	Náchod
v	v	k7c6	v
houslařské	houslařský	k2eAgFnSc6d1	houslařská
dílně	dílna	k1gFnSc6	dílna
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1829	[number]	k4	1829
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Jičína	Jičín	k1gInSc2	Jičín
studovat	studovat	k5eAaImF	studovat
učitelství	učitelství	k1gNnSc4	učitelství
a	a	k8xC	a
po	po	k7c6	po
úspěšném	úspěšný	k2eAgNnSc6d1	úspěšné
absolvování	absolvování	k1gNnSc6	absolvování
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1831	[number]	k4	1831
stal	stát	k5eAaPmAgInS	stát
pomocným	pomocný	k2eAgMnSc7d1	pomocný
učitelem	učitel	k1gMnSc7	učitel
v	v	k7c6	v
Pasekách	paseka	k1gFnPc6	paseka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1835	[number]	k4	1835
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
ochotnickém	ochotnický	k2eAgNnSc6d1	ochotnické
divadle	divadlo	k1gNnSc6	divadlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
protože	protože	k8xS	protože
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
učitelstvím	učitelství	k1gNnSc7	učitelství
nebyl	být	k5eNaImAgInS	být
spojen	spojen	k2eAgInSc1d1	spojen
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
plat	plat	k1gInSc1	plat
<g/>
,	,	kIx,	,
žil	žít	k5eAaImAgMnS	žít
velice	velice	k6eAd1	velice
chudě	chudě	k6eAd1	chudě
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1836	[number]	k4	1836
dostal	dostat	k5eAaPmAgMnS	dostat
výjimku	výjimka	k1gFnSc4	výjimka
a	a	k8xC	a
mohl	moct	k5eAaImAgMnS	moct
se	se	k3xPyFc4	se
i	i	k9	i
jako	jako	k9	jako
školní	školní	k2eAgMnSc1d1	školní
pomocník	pomocník	k1gMnSc1	pomocník
oženit	oženit	k5eAaPmF	oženit
s	s	k7c7	s
dcerou	dcera	k1gFnSc7	dcera
učitele	učitel	k1gMnSc2	učitel
Kateřinou	Kateřina	k1gFnSc7	Kateřina
Šimůnkovou	Šimůnkův	k2eAgFnSc7d1	Šimůnkova
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
měl	mít	k5eAaImAgMnS	mít
čtyři	čtyři	k4xCgFnPc4	čtyři
děti	dítě	k1gFnPc4	dítě
<g/>
:	:	kIx,	:
Pavlínu	Pavlína	k1gFnSc4	Pavlína
(	(	kIx(	(
<g/>
1836	[number]	k4	1836
<g/>
–	–	k?	–
<g/>
1857	[number]	k4	1857
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Václava	Václava	k1gFnSc1	Václava
(	(	kIx(	(
<g/>
1838	[number]	k4	1838
<g/>
–	–	k?	–
<g/>
1856	[number]	k4	1856
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Josefa	Josefa	k1gFnSc1	Josefa
(	(	kIx(	(
<g/>
1841	[number]	k4	1841
<g/>
–	–	k?	–
<g/>
1880	[number]	k4	1880
<g/>
)	)	kIx)	)
a	a	k8xC	a
Johanku	Johanka	k1gFnSc4	Johanka
(	(	kIx(	(
<g/>
1843	[number]	k4	1843
<g/>
–	–	k?	–
<g/>
1868	[number]	k4	1868
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
nejstarší	starý	k2eAgNnPc4d3	nejstarší
dvě	dva	k4xCgNnPc4	dva
ale	ale	k9	ale
zemřely	zemřít	k5eAaPmAgFnP	zemřít
na	na	k7c4	na
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
sobě	se	k3xPyFc3	se
v	v	k7c6	v
mladém	mladý	k2eAgInSc6d1	mladý
věku	věk	k1gInSc6	věk
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
tchán	tchán	k1gMnSc1	tchán
roku	rok	k1gInSc2	rok
1838	[number]	k4	1838
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
z	z	k7c2	z
dědictví	dědictví	k1gNnSc2	dědictví
koupil	koupit	k5eAaPmAgMnS	koupit
domek	domek	k1gInSc4	domek
v	v	k7c6	v
Pasekách	paseka	k1gFnPc6	paseka
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
také	také	k9	také
hospodařit	hospodařit	k5eAaImF	hospodařit
<g/>
.	.	kIx.	.
</s>
<s>
Přivydělával	přivydělávat	k5eAaImAgMnS	přivydělávat
si	se	k3xPyFc3	se
hraním	hranit	k5eAaImIp1nS	hranit
a	a	k8xC	a
hlavně	hlavně	k6eAd1	hlavně
stavěl	stavět	k5eAaImAgMnS	stavět
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
dřevěné	dřevěný	k2eAgInPc4d1	dřevěný
hudební	hudební	k2eAgInPc4d1	hudební
nástroje	nástroj	k1gInPc4	nástroj
včetně	včetně	k7c2	včetně
kytar	kytara	k1gFnPc2	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1841	[number]	k4	1841
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
řádným	řádný	k2eAgMnSc7d1	řádný
učitelem	učitel	k1gMnSc7	učitel
a	a	k8xC	a
1850	[number]	k4	1850
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
rychtářem	rychtář	k1gMnSc7	rychtář
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
funkci	funkce	k1gFnSc4	funkce
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
a	a	k8xC	a
zanechal	zanechat	k5eAaPmAgMnS	zanechat
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
ženu	hnát	k5eAaImIp1nS	hnát
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
ale	ale	k8xC	ale
také	také	k6eAd1	také
brzy	brzy	k6eAd1	brzy
zemřely	zemřít	k5eAaPmAgFnP	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Pohřben	pohřben	k2eAgMnSc1d1	pohřben
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
v	v	k7c6	v
Pasekách	paseka	k1gFnPc6	paseka
nad	nad	k7c7	nad
Jizerou	Jizera	k1gFnSc7	Jizera
u	u	k7c2	u
zdejšího	zdejší	k2eAgInSc2d1	zdejší
kostela	kostel	k1gInSc2	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkaz	odkaz	k1gInSc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Houslařství	houslařství	k1gNnSc4	houslařství
naučil	naučit	k5eAaPmAgMnS	naučit
své	svůj	k3xOyFgFnPc4	svůj
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
založil	založit	k5eAaPmAgMnS	založit
tak	tak	k6eAd1	tak
významnou	významný	k2eAgFnSc4d1	významná
houslařskou	houslařský	k2eAgFnSc4d1	houslařská
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
vzešly	vzejít	k5eAaPmAgFnP	vzejít
i	i	k9	i
houslařské	houslařský	k2eAgInPc4d1	houslařský
rody	rod	k1gInPc4	rod
Pilařů	Pilař	k1gMnPc2	Pilař
a	a	k8xC	a
Špidlenů	Špidlen	k1gInPc2	Špidlen
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
četných	četný	k2eAgInPc2d1	četný
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
velice	velice	k6eAd1	velice
cení	cenit	k5eAaImIp3nS	cenit
<g/>
,	,	kIx,	,
zanechal	zanechat	k5eAaPmAgMnS	zanechat
po	po	k7c6	po
sobě	se	k3xPyFc3	se
deníky	deník	k1gInPc1	deník
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1835	[number]	k4	1835
<g/>
–	–	k?	–
<g/>
1844	[number]	k4	1844
a	a	k8xC	a
1856	[number]	k4	1856
<g/>
–	–	k?	–
<g/>
1866	[number]	k4	1866
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
čerpal	čerpat	k5eAaImAgMnS	čerpat
Karel	Karel	k1gMnSc1	Karel
Václav	Václav	k1gMnSc1	Václav
Rais	Rais	k1gMnSc1	Rais
inspiraci	inspirace	k1gFnSc4	inspirace
i	i	k9	i
materiál	materiál	k1gInSc4	materiál
pro	pro	k7c4	pro
román	román	k1gInSc4	román
Zapadlí	zapadlý	k2eAgMnPc1d1	zapadlý
vlastenci	vlastenec	k1gMnPc1	vlastenec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Významná	významný	k2eAgFnSc1d1	významná
část	část	k1gFnSc1	část
Památníku	památník	k1gInSc2	památník
zapadlých	zapadlý	k2eAgMnPc2d1	zapadlý
vlastenců	vlastenec	k1gMnPc2	vlastenec
v	v	k7c6	v
Pasekách	paseka	k1gFnPc6	paseka
je	být	k5eAaImIp3nS	být
věnována	věnovat	k5eAaPmNgFnS	věnovat
Metelkovi	Metelkův	k2eAgMnPc1d1	Metelkův
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
se	se	k3xPyFc4	se
v	v	k7c6	v
Pasekách	paseka	k1gFnPc6	paseka
slavil	slavit	k5eAaImAgMnS	slavit
"	"	kIx"	"
<g/>
Rok	rok	k1gInSc1	rok
Věnceslava	Věnceslava	k1gFnSc1	Věnceslava
Metelky	Metelky	k?	Metelky
<g/>
"	"	kIx"	"
a	a	k8xC	a
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
mu	on	k3xPp3gMnSc3	on
postavili	postavit	k5eAaPmAgMnP	postavit
pomník	pomník	k1gInSc4	pomník
s	s	k7c7	s
bustou	busta	k1gFnSc7	busta
od	od	k7c2	od
sochaře	sochař	k1gMnSc2	sochař
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Preclíka	Preclík	k1gMnSc2	Preclík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Jalovec	jalovec	k1gInSc1	jalovec
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
:	:	kIx,	:
Čeští	český	k2eAgMnPc1d1	český
houslaři	houslař	k1gMnPc1	houslař
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Metelka	Metelka	k?	Metelka
<g/>
,	,	kIx,	,
Věnceslav	Věnceslava	k1gFnPc2	Věnceslava
<g/>
:	:	kIx,	:
Ze	z	k7c2	z
života	život	k1gInSc2	život
zapadlého	zapadlý	k2eAgMnSc2d1	zapadlý
vlastence	vlastenec	k1gMnSc2	vlastenec
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jech	Jech	k1gMnSc1	Jech
<g/>
,	,	kIx,	,
Jaromír	Jaromír	k1gMnSc1	Jaromír
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Kde	kde	k6eAd1	kde
na	na	k7c6	na
jabloních	jabloň	k1gFnPc6	jabloň
harmoniky	harmonika	k1gFnSc2	harmonika
rostou	růst	k5eAaImIp3nP	růst
:	:	kIx,	:
Dokumenty	dokument	k1gInPc7	dokument
ze	z	k7c2	z
života	život	k1gInSc2	život
písmácké	písmácký	k2eAgFnSc2d1	písmácká
a	a	k8xC	a
houslařské	houslařský	k2eAgFnSc2d1	houslařská
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
:	:	kIx,	:
Kruh	kruh	k1gInSc1	kruh
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Houslařství	houslařství	k1gNnSc1	houslařství
</s>
</p>
<p>
<s>
Paseky	paseka	k1gFnPc1	paseka
nad	nad	k7c7	nad
Jizerou	Jizera	k1gFnSc7	Jizera
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Věnceslav	Věnceslava	k1gFnPc2	Věnceslava
Metelka	Metelka	k?	Metelka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Věnceslav	Věnceslava	k1gFnPc2	Věnceslava
Metelka	Metelka	k?	Metelka
</s>
</p>
<p>
<s>
Kalvínská	kalvínský	k2eAgFnSc1d1	kalvínská
<g/>
,	,	kIx,	,
Petra	Petra	k1gFnSc1	Petra
<g/>
:	:	kIx,	:
Věnceslav	Věnceslava	k1gFnPc2	Věnceslava
Metelka	Metelka	k?	Metelka
</s>
</p>
<p>
<s>
Rok	rok	k1gInSc1	rok
2007	[number]	k4	2007
–	–	k?	–
rok	rok	k1gInSc1	rok
Věnceslava	Věnceslava	k1gFnSc1	Věnceslava
Metelky	Metelky	k?	Metelky
v	v	k7c6	v
Pasekách	paseka	k1gFnPc6	paseka
</s>
</p>
<p>
<s>
801	[number]	k4	801
<g/>
.	.	kIx.	.
a	a	k8xC	a
802	[number]	k4	802
<g/>
.	.	kIx.	.
</s>
<s>
Toulky	toulka	k1gFnSc2	toulka
českou	český	k2eAgFnSc7d1	Česká
minulostí	minulost	k1gFnSc7	minulost
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
</s>
</p>
