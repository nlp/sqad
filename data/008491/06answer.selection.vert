<s>
Václav	Václav	k1gMnSc1	Václav
<g/>
,	,	kIx,	,
alias	alias	k9	alias
Věnceslav	Věnceslava	k1gFnPc2	Věnceslava
Metelka	Metelka	k?	Metelka
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
vysocké	vysocký	k2eAgFnSc2d1	Vysocká
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
matriky	matrika	k1gFnSc2	matrika
narozených	narozený	k2eAgInPc2d1	narozený
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
nebo	nebo	k8xC	nebo
podle	podle	k7c2	podle
poznámky	poznámka	k1gFnSc2	poznámka
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
rodinných	rodinný	k2eAgInPc2d1	rodinný
záznamů	záznam	k1gInPc2	záznam
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1807	[number]	k4	1807
Sklenařice	Sklenařice	k1gFnSc2	Sklenařice
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1867	[number]	k4	1867
Paseky	paseka	k1gFnSc2	paseka
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
učitel	učitel	k1gMnSc1	učitel
<g/>
,	,	kIx,	,
houslař	houslař	k1gMnSc1	houslař
<g/>
,	,	kIx,	,
houslista	houslista	k1gMnSc1	houslista
a	a	k8xC	a
písmák	písmák	k1gMnSc1	písmák
<g/>
,	,	kIx,	,
prototyp	prototyp	k1gInSc1	prototyp
"	"	kIx"	"
<g/>
zapadlého	zapadlý	k2eAgMnSc4d1	zapadlý
vlastence	vlastenec	k1gMnSc4	vlastenec
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
