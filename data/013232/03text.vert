<p>
<s>
Chad	Chad	k1gMnSc1	Chad
Robert	Robert	k1gMnSc1	Robert
Kroeger	Kroeger	k1gMnSc1	Kroeger
(	(	kIx(	(
<g/>
*	*	kIx~	*
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kanadský	kanadský	k2eAgMnSc1d1	kanadský
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgMnSc1d1	hlavní
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
hard	hard	k1gMnSc1	hard
rockové	rockový	k2eAgFnSc2d1	rocková
skupiny	skupina	k1gFnSc2	skupina
Nickelback	Nickelbacka	k1gFnPc2	Nickelbacka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dětství	dětství	k1gNnSc4	dětství
==	==	k?	==
</s>
</p>
<p>
<s>
Chad	Chad	k1gMnSc1	Chad
Kroeger	Kroeger	k1gMnSc1	Kroeger
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1974	[number]	k4	1974
v	v	k7c6	v
kanadském	kanadský	k2eAgNnSc6d1	kanadské
městě	město	k1gNnSc6	město
Hanna	Hann	k1gInSc2	Hann
ležícím	ležící	k2eAgInPc3d1	ležící
v	v	k7c6	v
kanadském	kanadský	k2eAgInSc6d1	kanadský
státě	stát	k1gInSc6	stát
Alberta	Albert	k1gMnSc2	Albert
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
hudebně	hudebně	k6eAd1	hudebně
založené	založený	k2eAgFnSc2d1	založená
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jeho	jeho	k3xOp3gFnSc1	jeho
babička	babička	k1gFnSc1	babička
uměla	umět	k5eAaImAgFnS	umět
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
bicí	bicí	k2eAgNnSc4d1	bicí
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc4	jeho
dědeček	dědeček	k1gMnSc1	dědeček
na	na	k7c4	na
baskytaru	baskytara	k1gFnSc4	baskytara
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
dávala	dávat	k5eAaImAgFnS	dávat
hodiny	hodina	k1gFnPc4	hodina
hraní	hraní	k1gNnSc2	hraní
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
rozvrácené	rozvrácený	k2eAgFnSc2d1	rozvrácená
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
rodinu	rodina	k1gFnSc4	rodina
opustil	opustit	k5eAaPmAgMnS	opustit
v	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
dvou	dva	k4xCgInPc6	dva
letech	let	k1gInPc6	let
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
biologickým	biologický	k2eAgMnSc7d1	biologický
otcem	otec	k1gMnSc7	otec
neměl	mít	k5eNaImAgMnS	mít
příliš	příliš	k6eAd1	příliš
dobré	dobrý	k2eAgInPc4d1	dobrý
vztahy	vztah	k1gInPc4	vztah
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gFnSc3	jeho
nábožensky	nábožensky	k6eAd1	nábožensky
směrovaným	směrovaný	k2eAgInPc3d1	směrovaný
názorům	názor	k1gInPc3	názor
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
se	se	k3xPyFc4	se
rodinný	rodinný	k2eAgInSc1d1	rodinný
vztah	vztah	k1gInSc1	vztah
zhoršil	zhoršit	k5eAaPmAgInS	zhoršit
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
18	[number]	k4	18
letech	léto	k1gNnPc6	léto
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnSc1	jeho
jediný	jediný	k2eAgMnSc1d1	jediný
bratr	bratr	k1gMnSc1	bratr
Mike	Mike	k1gNnSc2	Mike
je	být	k5eAaImIp3nS	být
nevlastní	vlastní	k2eNgFnSc1d1	nevlastní
<g/>
.	.	kIx.	.
</s>
<s>
Kořeny	kořen	k1gInPc1	kořen
jeho	jeho	k3xOp3gFnSc2	jeho
rodiny	rodina	k1gFnSc2	rodina
sahají	sahat	k5eAaImIp3nP	sahat
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
i	i	k8xC	i
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tvorba	tvorba	k1gFnSc1	tvorba
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jeho	jeho	k3xOp3gFnSc1	jeho
kapela	kapela	k1gFnSc1	kapela
Nickelback	Nickelbacko	k1gNnPc2	Nickelbacko
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnPc4	jejíž
název	název	k1gInSc1	název
byl	být	k5eAaImAgInS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
fráze	fráze	k1gFnSc2	fráze
"	"	kIx"	"
<g/>
there	ther	k1gInSc5	ther
is	is	k?	is
your	your	k1gMnSc1	your
nickel	nickel	k1gMnSc1	nickel
back	back	k1gMnSc1	back
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
:	:	kIx,	:
tady	tady	k6eAd1	tady
máte	mít	k5eAaImIp2nP	mít
niklák	niklák	k?	niklák
nazpátek	nazpátek	k6eAd1	nazpátek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
bubeníka	bubeník	k1gMnSc2	bubeník
Daniela	Daniel	k1gMnSc2	Daniel
Adaira	Adair	k1gMnSc2	Adair
<g/>
,	,	kIx,	,
kytaristy	kytarista	k1gMnSc2	kytarista
Ryana	Ryan	k1gMnSc2	Ryan
Peaka	Peaek	k1gMnSc2	Peaek
a	a	k8xC	a
Chadova	Chadův	k2eAgMnSc2d1	Chadův
bratra	bratr	k1gMnSc2	bratr
a	a	k8xC	a
baskytaristy	baskytarista	k1gMnSc2	baskytarista
Mikea	Mikeus	k1gMnSc2	Mikeus
Kroegera	Kroeger	k1gMnSc2	Kroeger
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
negativní	negativní	k2eAgInPc4d1	negativní
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
Chadovým	Chadův	k2eAgMnSc7d1	Chadův
biologickým	biologický	k2eAgMnSc7d1	biologický
otcem	otec	k1gMnSc7	otec
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
pomohl	pomoct	k5eAaPmAgInS	pomoct
zafinancovat	zafinancovat	k5eAaPmF	zafinancovat
vznik	vznik	k1gInSc4	vznik
úplně	úplně	k6eAd1	úplně
první	první	k4xOgFnPc1	první
nahrávky	nahrávka	k1gFnPc1	nahrávka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyšla	vyjít	k5eAaPmAgFnS	vyjít
jako	jako	k9	jako
album	album	k1gNnSc4	album
Curb	Curba	k1gFnPc2	Curba
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
druhé	druhý	k4xOgNnSc1	druhý
album	album	k1gNnSc1	album
The	The	k1gFnSc2	The
State	status	k1gInSc5	status
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgInSc4d2	veliký
úspěch	úspěch	k1gInSc4	úspěch
zaznamenalo	zaznamenat	k5eAaPmAgNnS	zaznamenat
třetí	třetí	k4xOgNnSc1	třetí
album	album	k1gNnSc1	album
Silver	Silver	k1gMnSc1	Silver
Side	Sid	k1gMnSc2	Sid
Up	Up	k1gFnSc2	Up
vydané	vydaný	k2eAgFnSc2d1	vydaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nacházely	nacházet	k5eAaImAgFnP	nacházet
jedny	jeden	k4xCgFnPc1	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
písní	píseň	k1gFnPc2	píseň
kapely	kapela	k1gFnSc2	kapela
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
How	How	k1gFnSc1	How
You	You	k1gMnSc1	You
Remind	Remind	k1gMnSc1	Remind
Me	Me	k1gMnSc1	Me
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Never	Never	k1gMnSc1	Never
Again	Again	k1gMnSc1	Again
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
údajně	údajně	k6eAd1	údajně
inspirována	inspirovat	k5eAaBmNgFnS	inspirovat
domácím	domácí	k2eAgNnSc7d1	domácí
násilím	násilí	k1gNnSc7	násilí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
dělo	dít	k5eAaBmAgNnS	dít
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
okolí	okolí	k1gNnSc6	okolí
<g/>
)	)	kIx)	)
a	a	k8xC	a
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Too	Too	k1gFnSc1	Too
Bad	Bad	k1gFnSc2	Bad
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
nahrál	nahrát	k5eAaPmAgInS	nahrát
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
píseň	píseň	k1gFnSc4	píseň
pro	pro	k7c4	pro
film	film	k1gInSc4	film
Spider-Man	Spider-Man	k1gInSc4	Spider-Man
(	(	kIx(	(
<g/>
film	film	k1gInSc4	film
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Hero	Hero	k6eAd1	Hero
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
písní	píseň	k1gFnPc2	píseň
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Následovali	následovat	k5eAaImAgMnP	následovat
alba	album	k1gNnPc4	album
jako	jako	k8xS	jako
The	The	k1gFnSc4	The
Long	Long	k1gMnSc1	Long
Road	Road	k1gMnSc1	Road
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
All	All	k1gMnSc1	All
the	the	k?	the
Right	Right	k1gInSc1	Right
Reasons	Reasons	k1gInSc1	Reasons
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dark	Dark	k1gMnSc1	Dark
Horse	Horse	k1gFnSc2	Horse
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Here	Here	k1gFnSc1	Here
and	and	k?	and
Now	Now	k1gFnSc1	Now
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
No	no	k9	no
Fixed	Fixed	k1gInSc1	Fixed
Address	Addressa	k1gFnPc2	Addressa
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
a	a	k8xC	a
naposledy	naposledy	k6eAd1	naposledy
vydané	vydaný	k2eAgInPc4d1	vydaný
Feed	Feed	k1gInSc4	Feed
the	the	k?	the
Machine	Machin	k1gInSc5	Machin
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
byl	být	k5eAaImAgMnS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
za	za	k7c4	za
řízení	řízení	k1gNnSc4	řízení
v	v	k7c6	v
opilosti	opilost	k1gFnSc6	opilost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2012	[number]	k4	2012
začal	začít	k5eAaPmAgInS	začít
chodil	chodit	k5eAaImAgMnS	chodit
se	s	k7c7	s
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
Avril	Avrila	k1gFnPc2	Avrila
Lavigne	Lavign	k1gInSc5	Lavign
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
předtím	předtím	k6eAd1	předtím
provdána	provdán	k2eAgFnSc1d1	provdána
za	za	k7c2	za
frontamana	frontaman	k1gMnSc2	frontaman
kapely	kapela	k1gFnSc2	kapela
Sum	suma	k1gFnPc2	suma
41	[number]	k4	41
-	-	kIx~	-
Derycka	Derycko	k1gNnSc2	Derycko
Whibley	Whiblea	k1gFnSc2	Whiblea
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
vztah	vztah	k1gInSc1	vztah
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
prohlubovat	prohlubovat	k5eAaImF	prohlubovat
od	od	k7c2	od
května	květen	k1gInSc2	květen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Chad	Chad	k1gMnSc1	Chad
Kroeger	Kroeger	k1gMnSc1	Kroeger
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
pátém	pátý	k4xOgNnSc6	pátý
albu	album	k1gNnSc6	album
<g/>
.	.	kIx.	.
</s>
<s>
Vzali	vzít	k5eAaPmAgMnP	vzít
se	se	k3xPyFc4	se
přesně	přesně	k6eAd1	přesně
rok	rok	k1gInSc4	rok
poté	poté	k6eAd1	poté
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
září	září	k1gNnSc2	září
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
šířit	šířit	k5eAaImF	šířit
pomluvy	pomluva	k1gFnPc4	pomluva
o	o	k7c6	o
jejich	jejich	k3xOp3gInSc6	jejich
uvadajícím	uvadající	k2eAgInSc6d1	uvadající
vztahu	vztah	k1gInSc6	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Kroeger	Kroeger	k1gMnSc1	Kroeger
je	být	k5eAaImIp3nS	být
popřel	popřít	k5eAaPmAgMnS	popřít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Lavigne	Lavign	k1gInSc5	Lavign
v	v	k7c6	v
září	září	k1gNnSc6	září
2015	[number]	k4	2015
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
žijí	žít	k5eAaImIp3nP	žít
odděleně	odděleně	k6eAd1	odděleně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
se	se	k3xPyFc4	se
musela	muset	k5eAaImAgFnS	muset
odložit	odložit	k5eAaPmF	odložit
evropská	evropský	k2eAgFnSc1d1	Evropská
část	část	k1gFnSc1	část
turné	turné	k1gNnSc2	turné
kvůli	kvůli	k7c3	kvůli
problému	problém	k1gInSc3	problém
v	v	k7c6	v
cystě	cysta	k1gFnSc6	cysta
a	a	k8xC	a
jeho	on	k3xPp3gInSc2	on
hrtanu	hrtan	k1gInSc2	hrtan
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
musel	muset	k5eAaImAgMnS	muset
jít	jít	k5eAaImF	jít
na	na	k7c4	na
operaci	operace	k1gFnSc4	operace
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
evropského	evropský	k2eAgNnSc2d1	Evropské
turné	turné	k1gNnSc2	turné
byla	být	k5eAaImAgFnS	být
zahrnutá	zahrnutý	k2eAgFnSc1d1	zahrnutá
i	i	k8xC	i
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Chad	Chada	k1gFnPc2	Chada
Kroeger	Kroegero	k1gNnPc2	Kroegero
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
