<s>
Henryk	Henryk	k6eAd1
Orzyszek	Orzyszek	k1gInSc1
</s>
<s>
Henryk	Henryk	k6eAd1
Orzyszek	Orzyszek	k1gInSc1
Narození	narození	k1gNnSc2
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1935	#num#	k4
<g/>
Chorzów	Chorzów	k1gFnSc2
Úmrtí	úmrť	k1gFnPc2
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2021	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
86	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc2
</s>
<s>
Akademia	Akademia	k1gFnSc1
Muzyczna	Muzyczna	k1gFnSc1
im	im	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karola	Karola	k1gFnSc1
Szymanowskiego	Szymanowskiego	k1gMnSc1
w	w	k?
KatowicachUniwersytet	KatowicachUniwersytet	k1gMnSc1
Śląski	Śląski	k1gNnSc2
Povolání	povolání	k1gNnSc2
</s>
<s>
varhaník	varhaník	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Henryk	Henryk	k1gInSc1
Orzyszek	Orzyszek	k1gInSc1
(	(	kIx(
<g/>
11	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1935	#num#	k4
<g/>
,	,	kIx,
Pawłów	Pawłów	k1gFnSc1
–	–	k?
14	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2021	#num#	k4
<g/>
,	,	kIx,
Chořov	Chořov	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
polský	polský	k2eAgMnSc1d1
varhaník	varhaník	k1gMnSc1
<g/>
,	,	kIx,
organolog	organolog	k1gMnSc1
<g/>
,	,	kIx,
pedagog	pedagog	k1gMnSc1
a	a	k8xC
publicista	publicista	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS
Hudební	hudební	k2eAgFnSc4d1
akademii	akademie	k1gFnSc4
v	v	k7c6
Katovicích	Katovice	k1gFnPc6
a	a	k8xC
Slezskou	slezský	k2eAgFnSc4d1
univerzitu	univerzita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Působil	působit	k5eAaImAgMnS
jako	jako	k9
pedagog	pedagog	k1gMnSc1
na	na	k7c6
hudební	hudební	k2eAgFnSc6d1
škole	škola	k1gFnSc6
v	v	k7c6
Zabrzu	Zabrz	k1gInSc6
a	a	k8xC
jako	jako	k8xC,k8xS
varhaník	varhaník	k1gMnSc1
evangelickém	evangelický	k2eAgInSc6d1
kostele	kostel	k1gInSc6
Martina	Martin	k1gMnSc2
Luthera	Luther	k1gMnSc2
v	v	k7c6
Chořově	Chořův	k2eAgNnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
2003	#num#	k4
dokončil	dokončit	k5eAaPmAgMnS
liturgický	liturgický	k2eAgMnSc1d1
chorálník	chorálník	k1gMnSc1
Evangelicko-augsburské	evangelicko-augsburský	k2eAgFnSc2d1
církve	církev	k1gFnSc2
v	v	k7c6
Polské	polský	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Publikoval	publikovat	k5eAaBmAgMnS
řadu	řada	k1gFnSc4
prací	práce	k1gFnPc2
<g/>
,	,	kIx,
zejména	zejména	k9
k	k	k7c3
dějinám	dějiny	k1gFnPc3
hudby	hudba	k1gFnSc2
ve	v	k7c6
Slezsku	Slezsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
mj.	mj.	kA
autorem	autor	k1gMnSc7
monografie	monografie	k1gFnSc2
Dzieje	Dziej	k1gInSc2
organów	organów	k?
w	w	k?
kościołach	kościołach	k1gMnSc1
ewangelickich	ewangelickich	k1gMnSc1
Diecezji	Diecezje	k1gFnSc4
Katowickiej	Katowickiej	k1gInSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
polsky	polsky	k6eAd1
<g/>
)	)	kIx)
Profil	profil	k1gInSc1
na	na	k7c4
luteranie	luteranie	k1gFnPc4
<g/>
.	.	kIx.
<g/>
pl	pl	k?
</s>
<s>
(	(	kIx(
<g/>
polsky	polsky	k6eAd1
<g/>
)	)	kIx)
Nekrolog	nekrolog	k1gInSc1
na	na	k7c4
luteranie	luteranie	k1gFnPc4
<g/>
.	.	kIx.
<g/>
pl	pl	k?
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
121246191	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
0953	#num#	k4
1797	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
2006048124	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
9275493	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
2006048124	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Polsko	Polsko	k1gNnSc1
</s>
