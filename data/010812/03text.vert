<p>
<s>
Ján	Ján	k1gMnSc1	Ján
Simonides	Simonides	k1gMnSc1	Simonides
(	(	kIx(	(
<g/>
1648	[number]	k4	1648
<g/>
,	,	kIx,	,
Spišské	spišský	k2eAgInPc1d1	spišský
Vlachy	Vlachy	k1gInPc1	Vlachy
–	–	k?	–
7	[number]	k4	7
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
1708	[number]	k4	1708
<g/>
,	,	kIx,	,
Banská	banský	k2eAgFnSc1d1	Banská
Bystrica	Bystrica	k1gFnSc1	Bystrica
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
slovenský	slovenský	k2eAgMnSc1d1	slovenský
spisovatel	spisovatel	k1gMnSc1	spisovatel
memoárové	memoárový	k2eAgFnSc2d1	memoárová
a	a	k8xC	a
cestopisné	cestopisný	k2eAgFnSc2d1	cestopisná
prózy	próza	k1gFnSc2	próza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
evangelického	evangelický	k2eAgMnSc2d1	evangelický
kněze	kněz	k1gMnSc2	kněz
<g/>
,	,	kIx,	,
vzdělání	vzdělání	k1gNnSc1	vzdělání
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
Prešově	Prešov	k1gInSc6	Prešov
a	a	k8xC	a
ve	v	k7c6	v
Wittenbergu	Wittenberg	k1gInSc6	Wittenberg
(	(	kIx(	(
<g/>
1667	[number]	k4	1667
<g/>
–	–	k?	–
<g/>
1674	[number]	k4	1674
<g/>
)	)	kIx)	)
a	a	k8xC	a
po	po	k7c6	po
studiích	studie	k1gFnPc6	studie
působil	působit	k5eAaImAgMnS	působit
v	v	k7c4	v
Brezně	Brezeň	k1gFnPc4	Brezeň
jako	jako	k8xC	jako
rektor	rektor	k1gMnSc1	rektor
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odhalení	odhalení	k1gNnSc6	odhalení
Vesselényiho	Vesselényi	k1gMnSc2	Vesselényi
spiknutí	spiknutí	k1gNnSc6	spiknutí
ho	on	k3xPp3gMnSc4	on
věznili	věznit	k5eAaImAgMnP	věznit
v	v	k7c6	v
Leopoldově	Leopoldův	k2eAgFnSc6d1	Leopoldova
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
odsouzen	odsouzet	k5eAaImNgInS	odsouzet
na	na	k7c4	na
neapolské	neapolský	k2eAgFnPc4d1	neapolská
galeje	galej	k1gFnPc4	galej
<g/>
.	.	kIx.	.
</s>
<s>
Cestou	cesta	k1gFnSc7	cesta
na	na	k7c4	na
galeje	galej	k1gFnPc4	galej
ujel	ujet	k5eAaPmAgMnS	ujet
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Tobiášem	Tobiáš	k1gMnSc7	Tobiáš
Masníkem	Masník	k1gMnSc7	Masník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
pěti	pět	k4xCc6	pět
dnech	den	k1gInPc6	den
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
chytili	chytit	k5eAaPmAgMnP	chytit
a	a	k8xC	a
uvěznili	uvěznit	k5eAaPmAgMnP	uvěznit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
svobodu	svoboda	k1gFnSc4	svoboda
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
vykoupil	vykoupit	k5eAaPmAgMnS	vykoupit
jistý	jistý	k2eAgMnSc1d1	jistý
německý	německý	k2eAgMnSc1d1	německý
kupec	kupec	k1gMnSc1	kupec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Procestoval	procestovat	k5eAaPmAgMnS	procestovat
mnoho	mnoho	k4c4	mnoho
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
usadil	usadit	k5eAaPmAgInS	usadit
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žil	žít	k5eAaImAgMnS	žít
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1683	[number]	k4	1683
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
vlasti	vlast	k1gFnSc2	vlast
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
evangelický	evangelický	k2eAgMnSc1d1	evangelický
kněz	kněz	k1gMnSc1	kněz
v	v	k7c6	v
Banské	banský	k2eAgFnSc6d1	Banská
Bystrici-Radvani	Bystrici-Radvaň	k1gFnSc6	Bystrici-Radvaň
(	(	kIx(	(
<g/>
1683	[number]	k4	1683
<g/>
–	–	k?	–
<g/>
1687	[number]	k4	1687
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kráľovej	Kráľovej	k1gMnSc1	Kráľovej
(	(	kIx(	(
<g/>
1688	[number]	k4	1688
<g/>
–	–	k?	–
<g/>
1690	[number]	k4	1690
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hronseku	Hronsek	k1gInSc2	Hronsek
(	(	kIx(	(
<g/>
1691	[number]	k4	1691
<g/>
–	–	k?	–
<g/>
1695	[number]	k4	1695
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Banské	banský	k2eAgFnSc6d1	Banská
Bystrici	Bystrica	k1gFnSc6	Bystrica
(	(	kIx(	(
<g/>
1695	[number]	k4	1695
<g/>
–	–	k?	–
<g/>
1708	[number]	k4	1708
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
superintendantem	superintendant	k1gMnSc7	superintendant
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1704	[number]	k4	1704
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tvorba	tvorba	k1gFnSc1	tvorba
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
zejména	zejména	k9	zejména
jako	jako	k9	jako
autor	autor	k1gMnSc1	autor
cestopisných	cestopisný	k2eAgFnPc2d1	cestopisná
a	a	k8xC	a
memoárové	memoárový	k2eAgInPc1d1	memoárový
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgInS	věnovat
svým	svůj	k3xOyFgInSc7	svůj
zážitkům	zážitek	k1gInPc3	zážitek
od	od	k7c2	od
odsouzení	odsouzení	k1gNnSc2	odsouzení
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
až	až	k9	až
po	po	k7c4	po
návrat	návrat	k1gInSc4	návrat
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Vycházel	vycházet	k5eAaImAgInS	vycházet
přitom	přitom	k6eAd1	přitom
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
deníků	deník	k1gInPc2	deník
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
si	se	k3xPyFc3	se
cestou	cesta	k1gFnSc7	cesta
psal	psát	k5eAaImAgMnS	psát
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
dílech	díl	k1gInPc6	díl
však	však	k9	však
nepopisoval	popisovat	k5eNaImAgInS	popisovat
jen	jen	k9	jen
své	svůj	k3xOyFgInPc4	svůj
zážitky	zážitek	k1gInPc4	zážitek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
věnoval	věnovat	k5eAaPmAgMnS	věnovat
se	se	k3xPyFc4	se
i	i	k9	i
popisem	popis	k1gInSc7	popis
krajů	kraj	k1gInPc2	kraj
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
navštívil	navštívit	k5eAaPmAgMnS	navštívit
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnPc2	jejich
regionálních	regionální	k2eAgFnPc2d1	regionální
zvyklostí	zvyklost	k1gFnPc2	zvyklost
<g/>
,	,	kIx,	,
etnografických	etnografický	k2eAgInPc2d1	etnografický
poměrů	poměr	k1gInPc2	poměr
a	a	k8xC	a
architektury	architektura	k1gFnSc2	architektura
(	(	kIx(	(
<g/>
antické	antický	k2eAgFnSc2d1	antická
památky	památka	k1gFnSc2	památka
<g/>
,	,	kIx,	,
klášterní	klášterní	k2eAgFnSc2d1	klášterní
a	a	k8xC	a
chrámové	chrámový	k2eAgFnSc2d1	chrámová
stavby	stavba	k1gFnSc2	stavba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Používal	používat	k5eAaImAgInS	používat
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
<g/>
,	,	kIx,	,
bezprostřední	bezprostřední	k2eAgInSc1d1	bezprostřední
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nevkládal	vkládat	k5eNaImAgMnS	vkládat
do	do	k7c2	do
textu	text	k1gInSc2	text
téměř	téměř	k6eAd1	téměř
žádné	žádný	k3yNgInPc4	žádný
náboženské	náboženský	k2eAgInPc4d1	náboženský
detaily	detail	k1gInPc4	detail
(	(	kIx(	(
<g/>
biblické	biblický	k2eAgInPc1d1	biblický
texty	text	k1gInPc1	text
<g/>
,	,	kIx,	,
příklady	příklad	k1gInPc1	příklad
<g/>
,	,	kIx,	,
náboženské	náboženský	k2eAgFnPc1d1	náboženská
reflexe	reflexe	k1gFnPc1	reflexe
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
víceméně	víceméně	k9	víceméně
světskou	světský	k2eAgFnSc4d1	světská
prózu	próza	k1gFnSc4	próza
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
vzácná	vzácný	k2eAgFnSc1d1	vzácná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgInS	být
též	též	k9	též
autorem	autor	k1gMnSc7	autor
latinské	latinský	k2eAgFnSc2d1	Latinská
příležitostné	příležitostný	k2eAgFnSc2d1	příležitostná
poezie	poezie	k1gFnSc2	poezie
<g/>
,	,	kIx,	,
náboženských	náboženský	k2eAgFnPc2d1	náboženská
polemik	polemika	k1gFnPc2	polemika
a	a	k8xC	a
náboženských	náboženský	k2eAgInPc2d1	náboženský
výchovných	výchovný	k2eAgInPc2d1	výchovný
spisů	spis	k1gInPc2	spis
<g/>
.	.	kIx.	.
</s>
<s>
Obhajoval	obhajovat	k5eAaImAgInS	obhajovat
význam	význam	k1gInSc4	význam
mateřské	mateřský	k2eAgFnSc2d1	mateřská
řeči	řeč	k1gFnSc2	řeč
<g/>
,	,	kIx,	,
slovakizoval	slovakizovat	k5eAaBmAgInS	slovakizovat
biblickou	biblický	k2eAgFnSc4d1	biblická
češtinu	čeština	k1gFnSc4	čeština
<g/>
,	,	kIx,	,
dokladem	doklad	k1gInSc7	doklad
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
úvod	úvod	k1gInSc1	úvod
ke	k	k7c3	k
katechismu	katechismus	k1gInSc3	katechismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
1676	[number]	k4	1676
–	–	k?	–
Krátke	Krátk	k1gFnSc2	Krátk
opísanie	opísanie	k1gFnSc1	opísanie
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
čo	čo	k?	čo
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
s	s	k7c7	s
kňazmi	kňaz	k1gFnPc7	kňaz
uhorskej	uhorskat	k5eAaPmRp2nS	uhorskat
evanjelickej	evanjelickat	k5eAaImRp2nS	evanjelickat
cirkvi	cirkvit	k5eAaPmRp2nS	cirkvit
(	(	kIx(	(
<g/>
Brevis	Brevis	k1gInSc1	Brevis
consignatio	consignatio	k1gNnSc1	consignatio
eorum	eorum	k1gInSc1	eorum
<g/>
,	,	kIx,	,
quae	quaat	k5eAaPmIp3nS	quaat
facta	facta	k1gMnSc1	facta
sunt	sunt	k1gMnSc1	sunt
cum	cum	k?	cum
ministris	ministris	k1gInSc1	ministris
ev.	ev.	k?	ev.
ecclesiae	ecclesiae	k1gInSc1	ecclesiae
Hungaricae	Hungaricae	k1gFnSc1	Hungaricae
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1676	[number]	k4	1676
–	–	k?	–
Galéria	Galérium	k1gNnSc2	Galérium
všetkých	všetká	k1gFnPc2	všetká
Bohu	bůh	k1gMnSc3	bůh
oddaných	oddaný	k2eAgMnPc2d1	oddaný
<g/>
,	,	kIx,	,
spútaných	spútaný	k2eAgMnPc2d1	spútaný
navzájom	navzájom	k1gInSc4	navzájom
reťazami	reťaza	k1gFnPc7	reťaza
kresťanskej	kresťanskat	k5eAaImRp2nS	kresťanskat
cnosti	cnost	k1gFnSc2	cnost
(	(	kIx(	(
<g/>
Galeria	Galerium	k1gNnSc2	Galerium
omnium	omnium	k1gNnSc1	omnium
sanctorum	sanctorum	k1gInSc1	sanctorum
<g/>
,	,	kIx,	,
catenis	catenis	k1gInSc1	catenis
christianae	christianaat	k5eAaPmIp3nS	christianaat
virtutis	virtutis	k1gFnSc4	virtutis
sibi	sib	k1gFnSc2	sib
devincotorum	devincotorum	k1gInSc1	devincotorum
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1679	[number]	k4	1679
–	–	k?	–
Prenasledovaný	Prenasledovaný	k2eAgMnSc1d1	Prenasledovaný
vyhnanec	vyhnanec	k1gMnSc1	vyhnanec
(	(	kIx(	(
<g/>
Exul	exul	k1gMnSc1	exul
praedicamentalis	praedicamentalis	k1gFnSc2	praedicamentalis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1681	[number]	k4	1681
–	–	k?	–
Božia	Božia	k1gFnSc1	Božia
moc	moc	k1gFnSc1	moc
a	a	k8xC	a
milosť	milostit	k5eAaPmRp2nS	milostit
(	(	kIx(	(
<g/>
Gottes	Gottes	k1gMnSc1	Gottes
Kraft	Kraft	k1gMnSc1	Kraft
und	und	k?	und
Gnade	Gnad	k1gInSc5	Gnad
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1704	[number]	k4	1704
–	–	k?	–
Vysvětlení	vysvětlení	k1gNnSc2	vysvětlení
krestianského	krestianský	k2eAgNnSc2d1	krestianský
učení	učení	k1gNnSc2	učení
</s>
</p>
<p>
<s>
1707	[number]	k4	1707
–	–	k?	–
Řeči	řeč	k1gFnSc2	řeč
<g/>
,	,	kIx,	,
v	v	k7c6	v
kterých	který	k3yRgNnPc6	který
se	se	k3xPyFc4	se
lid	lid	k1gInSc1	lid
boží	boží	k2eAgInSc1d1	boží
napomíná	napomínat	k5eAaImIp3nS	napomínat
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Jána	Ján	k1gMnSc2	Ján
Simonidese	Simonidese	k1gFnSc2	Simonidese
ve	v	k7c6	v
Zlatém	zlatý	k2eAgInSc6d1	zlatý
fondu	fond	k1gInSc6	fond
deníku	deník	k1gInSc2	deník
SME	SME	k?	SME
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Ján	Ján	k1gMnSc1	Ján
Simonides	Simonides	k1gMnSc1	Simonides
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
