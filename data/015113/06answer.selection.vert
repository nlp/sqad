<s>
Fotbalová	fotbalový	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
FAČR	FAČR	kA
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
Football	Football	k1gInSc1
Association	Association	k1gInSc1
of	of	k?
the	the	k?
Czech	Czech	k1gInSc1
Republic	Republic	k1gFnPc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
členský	členský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
organizací	organizace	k1gFnPc2
FIFA	FIFA	kA
a	a	k8xC
UEFA	UEFA	kA
a	a	k8xC
orgán	orgán	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
organizuje	organizovat	k5eAaBmIp3nS
fotbal	fotbal	k1gInSc4
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
</s>