<s>
Fotbalová	fotbalový	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Fotbalová	fotbalový	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
Zkratka	zkratka	k1gFnSc1
</s>
<s>
FAČR	FAČR	kA
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Československý	československý	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Vznik	vznik	k1gInSc1
</s>
<s>
1901	#num#	k4
Typ	typ	k1gInSc1
</s>
<s>
národní	národní	k2eAgInSc1d1
sportovní	sportovní	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Účel	účel	k1gInSc1
</s>
<s>
fotbal	fotbal	k1gInSc1
<g/>
,	,	kIx,
futsal	futsat	k5eAaImAgInS,k5eAaPmAgInS
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Místo	místo	k7c2
</s>
<s>
Atletická	atletický	k2eAgFnSc1d1
2474	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
169	#num#	k4
00	#num#	k4
Praha	Praha	k1gFnSc1
6	#num#	k4
-	-	kIx~
Strahov	Strahov	k1gInSc1
<g/>
,	,	kIx,
169	#num#	k4
00	#num#	k4
Působnost	působnost	k1gFnSc1
</s>
<s>
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Úřední	úřední	k2eAgFnSc1d1
jazyk	jazyk	k1gInSc4
</s>
<s>
čeština	čeština	k1gFnSc1
Členové	člen	k1gMnPc1
</s>
<s>
3	#num#	k4
500	#num#	k4
klubů	klub	k1gInPc2
<g/>
,	,	kIx,
250	#num#	k4
000	#num#	k4
hráčů	hráč	k1gMnPc2
Předseda	předseda	k1gMnSc1
</s>
<s>
Martin	Martin	k2eAgInSc1d1
Malík	malík	k1gInSc1
Mateřská	mateřský	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
</s>
<s>
FIFA	FIFA	kA
<g/>
,	,	kIx,
UEFA	UEFA	kA
Přidružení	přidružení	k1gNnSc1
</s>
<s>
ČOV	ČOV	kA
<g/>
,	,	kIx,
ČUS	ČUS	k?
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.fotbal.cz	www.fotbal.cz	k1gInSc1
Datová	datový	k2eAgFnSc1d1
schránka	schránka	k1gFnSc1
</s>
<s>
s	s	k7c7
<g/>
8	#num#	k4
<g/>
cbspf	cbspf	k1gInSc1
Dřívější	dřívější	k2eAgInSc4d1
název	název	k1gInSc4
</s>
<s>
Českomoravský	českomoravský	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
svaz	svaz	k1gInSc1
IČO	IČO	kA
</s>
<s>
00406741	#num#	k4
(	(	kIx(
<g/>
VR	vr	k0
<g/>
)	)	kIx)
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Fotbalová	fotbalový	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
FAČR	FAČR	kA
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
Football	Football	k1gInSc1
Association	Association	k1gInSc1
of	of	k?
the	the	k?
Czech	Czech	k1gInSc1
Republic	Republic	k1gFnPc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
členský	členský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
organizací	organizace	k1gFnPc2
FIFA	FIFA	kA
a	a	k8xC
UEFA	UEFA	kA
a	a	k8xC
orgán	orgán	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
organizuje	organizovat	k5eAaBmIp3nS
fotbal	fotbal	k1gInSc4
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
dalším	další	k2eAgInPc3d1
z	z	k7c2
pokračovatelů	pokračovatel	k1gMnPc2
Českého	český	k2eAgInSc2d1
svazu	svaz	k1gInSc2
fotbalového	fotbalový	k2eAgInSc2d1
<g/>
,	,	kIx,
založeného	založený	k2eAgInSc2d1
v	v	k7c6
roce	rok	k1gInSc6
1901	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
června	červen	k1gInSc2
2011	#num#	k4
se	se	k3xPyFc4
označoval	označovat	k5eAaImAgInS
názvem	název	k1gInSc7
Českomoravský	českomoravský	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
svaz	svaz	k1gInSc1
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
ČMFS	ČMFS	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Organizuje	organizovat	k5eAaBmIp3nS
nižší	nízký	k2eAgFnSc2d2
fotbalové	fotbalový	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
(	(	kIx(
<g/>
nejvyšší	vysoký	k2eAgFnSc2d3
profesionální	profesionální	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
od	od	k7c2
roku	rok	k1gInSc2
2016	#num#	k4
spravuje	spravovat	k5eAaImIp3nS
Ligová	ligový	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pořádá	pořádat	k5eAaImIp3nS
národní	národní	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
,	,	kIx,
do	do	k7c2
jeho	jeho	k3xOp3gFnSc2
kompetence	kompetence	k1gFnSc2
spadá	spadat	k5eAaPmIp3nS,k5eAaImIp3nS
i	i	k9
česká	český	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
nebo	nebo	k8xC
futsal	futsat	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
asociaci	asociace	k1gFnSc6
je	být	k5eAaImIp3nS
zaregistrováno	zaregistrovat	k5eAaPmNgNnS
3	#num#	k4
500	#num#	k4
klubů	klub	k1gInPc2
a	a	k8xC
přes	přes	k7c4
330	#num#	k4
000	#num#	k4
hráčů	hráč	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Český	český	k2eAgInSc1d1
svaz	svaz	k1gInSc1
footballový	footballový	k2eAgInSc1d1
(	(	kIx(
<g/>
ČSF	ČSF	kA
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
19	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1901	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc4
podnět	podnět	k1gInSc4
k	k	k7c3
jeho	jeho	k3xOp3gNnSc3
založení	založení	k1gNnSc3
dalo	dát	k5eAaPmAgNnS
představenstvo	představenstvo	k1gNnSc1
klubu	klub	k1gInSc2
S.	S.	kA
<g/>
K.	K.	kA
Slavia	Slavia	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1900	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc3
prvním	první	k4xOgMnSc6
předsedou	předseda	k1gMnSc7
byl	být	k5eAaImAgMnS
zvolen	zvolen	k2eAgMnSc1d1
dr	dr	kA
<g/>
.	.	kIx.
Freja	Freja	k1gMnSc1
z	z	k7c2
klubu	klub	k1gInSc2
ČAFC	ČAFC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1906	#num#	k4
byl	být	k5eAaImAgInS
svaz	svaz	k1gInSc1
přijat	přijmout	k5eAaPmNgInS
do	do	k7c2
mezinárodní	mezinárodní	k2eAgFnSc2d1
fotbalové	fotbalový	k2eAgFnSc2d1
federace	federace	k1gFnSc2
FIFA	FIFA	kA
<g/>
,	,	kIx,
ale	ale	k8xC
po	po	k7c6
protestech	protest	k1gInPc6
Rakouského	rakouský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
pouze	pouze	k6eAd1
coby	coby	k?
provizorní	provizorní	k2eAgMnSc1d1
člen	člen	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1918	#num#	k4
se	se	k3xPyFc4
ČSF	ČSF	kA
stal	stát	k5eAaPmAgInS
řídícím	řídící	k2eAgInSc7d1
orgánem	orgán	k1gInSc7
v	v	k7c6
Československu	Československo	k1gNnSc6
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1921	#num#	k4
změnil	změnit	k5eAaPmAgInS
svůj	svůj	k3xOyFgInSc4
název	název	k1gInSc4
na	na	k7c4
Československý	československý	k2eAgInSc4d1
svaz	svaz	k1gInSc4
fotbalový	fotbalový	k2eAgInSc4d1
(	(	kIx(
<g/>
ČSSF	ČSSF	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1922	#num#	k4
zakládajícím	zakládající	k2eAgInSc7d1
členem	člen	k1gInSc7
a	a	k8xC
součástí	součást	k1gFnSc7
Československé	československý	k2eAgFnSc2d1
fotbalové	fotbalový	k2eAgFnSc2d1
asociace	asociace	k1gFnSc2
<g/>
,	,	kIx,
původním	původní	k2eAgInSc7d1
názvem	název	k1gInSc7
„	„	k?
<g/>
Československá	československý	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
fotbalová	fotbalový	k2eAgFnSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
“	“	k?
(	(	kIx(
<g/>
dále	daleko	k6eAd2
ČSAF	ČSAF	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
sdružující	sdružující	k2eAgMnSc1d1
kromě	kromě	k7c2
něj	on	k3xPp3gMnSc2
i	i	k9
fotbalové	fotbalový	k2eAgInPc4d1
svazy	svaz	k1gInPc4
národnostních	národnostní	k2eAgFnPc2d1
menšin	menšina	k1gFnPc2
(	(	kIx(
<g/>
maďarský	maďarský	k2eAgInSc1d1
<g/>
,	,	kIx,
židovský	židovský	k2eAgInSc1d1
<g/>
,	,	kIx,
německý	německý	k2eAgMnSc1d1
a	a	k8xC
polský	polský	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1923	#num#	k4
byla	být	k5eAaImAgFnS
ČSAF	ČSAF	kA
přijata	přijmout	k5eAaPmNgFnS
definitivně	definitivně	k6eAd1
za	za	k7c4
člena	člen	k1gMnSc4
FIFA	FIFA	kA
na	na	k7c6
kongresu	kongres	k1gInSc6
v	v	k7c6
Ženevě	Ženeva	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
německé	německý	k2eAgFnSc2d1
okupace	okupace	k1gFnSc2
působil	působit	k5eAaImAgMnS
v	v	k7c6
protektorátu	protektorát	k1gInSc6
ČSF	ČSF	kA
<g/>
,	,	kIx,
po	po	k7c4
osvobození	osvobození	k1gNnSc4
v	v	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
vytvořil	vytvořit	k5eAaPmAgInS
nejprve	nejprve	k6eAd1
jednotné	jednotný	k2eAgNnSc4d1
ústředí	ústředí	k1gNnSc4
se	s	k7c7
Slovenským	slovenský	k2eAgInSc7d1
futbalovým	futbalův	k2eAgInSc7d1
zväzem	zväz	k1gInSc7
<g/>
,	,	kIx,
v	v	k7c6
krátké	krátký	k2eAgFnSc6d1
době	doba	k1gFnSc6
však	však	k9
byla	být	k5eAaImAgFnS
obnovena	obnovit	k5eAaPmNgFnS
opět	opět	k6eAd1
ČSAF	ČSAF	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
po	po	k7c6
komunistickém	komunistický	k2eAgInSc6d1
převratu	převrat	k1gInSc6
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1957	#num#	k4
<g/>
)	)	kIx)
měl	mít	k5eAaImAgInS
Československý	československý	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
svaz	svaz	k1gInSc1
(	(	kIx(
<g/>
ČSFS	ČSFS	kA
<g/>
)	)	kIx)
statut	statut	k1gInSc1
součásti	součást	k1gFnSc2
Československého	československý	k2eAgInSc2d1
svazu	svaz	k1gInSc2
tělesné	tělesný	k2eAgFnSc2d1
výchovy	výchova	k1gFnSc2
a	a	k8xC
sportu	sport	k1gInSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc2
existence	existence	k1gFnSc2
byla	být	k5eAaImAgFnS
spíše	spíše	k9
formální	formální	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
svaz	svaz	k1gInSc1
se	se	k3xPyFc4
jako	jako	k8xC,k8xS
samosprávná	samosprávný	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
obnovil	obnovit	k5eAaPmAgInS
po	po	k7c6
listopadu	listopad	k1gInSc6
1989	#num#	k4
<g/>
,	,	kIx,
tehdy	tehdy	k6eAd1
ještě	ještě	k9
v	v	k7c6
rámci	rámec	k1gInSc6
ČSFS	ČSFS	kA
<g/>
,	,	kIx,
ale	ale	k8xC
brzy	brzy	k6eAd1
změnil	změnit	k5eAaPmAgInS
název	název	k1gInSc1
na	na	k7c6
„	„	k?
<g/>
Českomoravský	českomoravský	k2eAgInSc4d1
<g/>
“	“	k?
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
po	po	k7c6
rozdělení	rozdělení	k1gNnSc6
Československa	Československo	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
ujal	ujmout	k5eAaPmAgInS
svrchovaného	svrchovaný	k2eAgNnSc2d1
vedení	vedení	k1gNnSc2
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
roku	rok	k1gInSc2
2011	#num#	k4
užívá	užívat	k5eAaImIp3nS
název	název	k1gInSc1
Fotbalová	fotbalový	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
FAČR	FAČR	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Policejní	policejní	k2eAgNnSc1d1
vyšetřování	vyšetřování	k1gNnSc1
</s>
<s>
2017	#num#	k4
</s>
<s>
Policie	policie	k1gFnSc1
provedla	provést	k5eAaPmAgFnS
3	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2017	#num#	k4
razii	razie	k1gFnSc4
v	v	k7c6
sídle	sídlo	k1gNnSc6
asociace	asociace	k1gFnSc2
a	a	k8xC
dalších	další	k2eAgFnPc2d1
institucí	instituce	k1gFnPc2
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
přerozdělení	přerozdělení	k1gNnSc1
státních	státní	k2eAgFnPc2d1
dotací	dotace	k1gFnPc2
ze	z	k7c2
strany	strana	k1gFnSc2
ministerstva	ministerstvo	k1gNnSc2
školství	školství	k1gNnSc2
pro	pro	k7c4
rok	rok	k1gInSc4
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
akce	akce	k1gFnSc2
byl	být	k5eAaImAgInS
kromě	kromě	k7c2
jiných	jiný	k1gMnPc2
zadržen	zadržet	k5eAaPmNgMnS
i	i	k9
předseda	předseda	k1gMnSc1
asociace	asociace	k1gFnSc2
Miroslav	Miroslav	k1gMnSc1
Pelta	Pelta	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2020	#num#	k4
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Korupční	korupční	k2eAgInSc1d1
skandál	skandál	k1gInSc1
v	v	k7c6
českém	český	k2eAgInSc6d1
fotbale	fotbal	k1gInSc6
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgInSc1d1
zásah	zásah	k1gInSc1
v	v	k7c6
sídle	sídlo	k1gNnSc6
asociace	asociace	k1gFnSc2
proběhl	proběhnout	k5eAaPmAgInS
16	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zadrženo	zadržet	k5eAaPmNgNnS
bylo	být	k5eAaImAgNnS
celkem	celkem	k6eAd1
20	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
místopředsedy	místopředseda	k1gMnSc2
Romana	Roman	k1gMnSc2
Berbra	Berbr	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Předsedové	předseda	k1gMnPc1
</s>
<s>
Předposlední	předposlední	k2eAgMnSc1d1
předseda	předseda	k1gMnSc1
Miroslav	Miroslav	k1gMnSc1
Pelta	Pelta	k1gMnSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
František	František	k1gMnSc1
Chvalovský	Chvalovský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1993	#num#	k4
do	do	k7c2
17	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2001	#num#	k4
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
</s>
<s>
Jan	Jan	k1gMnSc1
Obst	Obst	k1gMnSc1
(	(	kIx(
<g/>
od	od	k7c2
17	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2001	#num#	k4
do	do	k7c2
21	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Pavel	Pavel	k1gMnSc1
Mokrý	Mokrý	k1gMnSc1
(	(	kIx(
<g/>
od	od	k7c2
21	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2005	#num#	k4
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
do	do	k7c2
27	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2009	#num#	k4
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
</s>
<s>
Ivan	Ivan	k1gMnSc1
Hašek	Hašek	k1gMnSc1
(	(	kIx(
<g/>
od	od	k7c2
27	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2009	#num#	k4
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
do	do	k7c2
26	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2011	#num#	k4
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
</s>
<s>
Do	do	k7c2
zvolení	zvolení	k1gNnPc2
nového	nový	k2eAgMnSc2d1
předsedy	předseda	k1gMnSc2
vedl	vést	k5eAaImAgInS
asociaci	asociace	k1gFnSc4
její	její	k3xOp3gMnSc1
první	první	k4xOgMnSc1
místopředseda	místopředseda	k1gMnSc1
Dalibor	Dalibor	k1gMnSc1
Kučera	Kučera	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Miroslav	Miroslav	k1gMnSc1
Pelta	Pelta	k1gMnSc1
(	(	kIx(
<g/>
od	od	k7c2
17	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2011	#num#	k4
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
do	do	k7c2
6	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
</s>
<s>
Do	do	k7c2
zvolení	zvolení	k1gNnPc2
nového	nový	k2eAgMnSc2d1
předsedy	předseda	k1gMnSc2
vedl	vést	k5eAaImAgInS
asociaci	asociace	k1gFnSc4
její	její	k3xOp3gMnSc1
první	první	k4xOgMnSc1
místopředseda	místopředseda	k1gMnSc1
Roman	Roman	k1gMnSc1
Berbr	Berbr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Martin	Martin	k1gMnSc1
Malík	Malík	k1gMnSc1
(	(	kIx(
<g/>
od	od	k7c2
12	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Český	český	k2eAgInSc1d1
fotbal	fotbal	k1gInSc1
nyní	nyní	k6eAd1
řídí	řídit	k5eAaImIp3nS
Fotbalová	fotbalový	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
ČR	ČR	kA
Archivováno	archivován	k2eAgNnSc4d1
27	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
ČT	ČT	kA
24	#num#	k4
<g/>
,	,	kIx,
26	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
↑	↑	k?
Kolektiv	kolektiv	k1gInSc1
autorů	autor	k1gMnPc2
<g/>
:	:	kIx,
Svět	svět	k1gInSc1
devadesáti	devadesát	k4xCc2
minut	minuta	k1gFnPc2
<g/>
,	,	kIx,
Olympia	Olympia	k1gFnSc1
<g/>
,	,	kIx,
1980	#num#	k4
<g/>
↑	↑	k?
Archivováno	archivován	k2eAgNnSc1d1
2	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
Archiv	archiv	k1gInSc1
sportu	sport	k1gInSc2
<g/>
↑	↑	k?
Policie	policie	k1gFnSc1
spustila	spustit	k5eAaPmAgFnS
razii	razie	k1gFnSc4
kvůli	kvůli	k7c3
sportovním	sportovní	k2eAgFnPc3d1
dotacím	dotace	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zadržen	zadržet	k5eAaPmNgMnS
Jansta	Jansta	k1gMnSc1
<g/>
,	,	kIx,
Pelta	Pelta	k1gMnSc1
i	i	k8xC
Březina	Březina	k1gMnSc1
<g/>
↑	↑	k?
MÁDL	MÁDL	kA
<g/>
,	,	kIx,
Luděk	Luděk	k1gMnSc1
<g/>
;	;	kIx,
NOHL	NOHL	kA
<g/>
,	,	kIx,
Radek	Radek	k1gMnSc1
<g/>
;	;	kIx,
KOUTNÍK	Koutník	k1gMnSc1
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Policejní	policejní	k2eAgInSc1d1
zásah	zásah	k1gInSc1
v	v	k7c6
českém	český	k2eAgInSc6d1
fotbale	fotbal	k1gInSc6
<g/>
,	,	kIx,
obviněn	obviněn	k2eAgMnSc1d1
Berbr	Berbr	k1gMnSc1
a	a	k8xC
19	#num#	k4
dalších	další	k2eAgFnPc2d1
osob	osoba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc1
Zprávy	zpráva	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-10-16	2020-10-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Bossové	boss	k1gMnPc1
českého	český	k2eAgInSc2d1
fotbalu	fotbal	k1gInSc2
<g/>
:	:	kIx,
Chvalovský	Chvalovský	k2eAgMnSc1d1
-	-	kIx~
zapomenutý	zapomenutý	k2eAgMnSc1d1
vůdce	vůdce	k1gMnSc1
<g/>
↑	↑	k?
Český	český	k2eAgInSc1d1
fotbal	fotbal	k1gInSc1
bude	být	k5eAaImBp3nS
mít	mít	k5eAaImF
nového	nový	k2eAgMnSc4d1
předsedu	předseda	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
byli	být	k5eAaImAgMnP
jeho	jeho	k3xOp3gMnPc7
předchůdci	předchůdce	k1gMnPc7
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
↑	↑	k?
Novým	nový	k2eAgMnSc7d1
předsedou	předseda	k1gMnSc7
ČMFS	ČMFS	kA
byl	být	k5eAaImAgMnS
zvolen	zvolen	k2eAgMnSc1d1
Pavel	Pavel	k1gMnSc1
Mokrý	Mokrý	k1gMnSc1
<g/>
↑	↑	k?
ON-LINE	ON-LINE	k1gFnSc1
přenos	přenos	k1gInSc1
<g/>
:	:	kIx,
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Valná	valný	k2eAgFnSc1d1
hromada	hromada	k1gFnSc1
ČMFS	ČMFS	kA
<g/>
↑	↑	k?
Předsedou	předseda	k1gMnSc7
ČMFS	ČMFS	kA
zvolen	zvolit	k5eAaPmNgMnS
Ivan	Ivan	k1gMnSc1
Hašek	Hašek	k1gMnSc1
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Šéf	šéf	k1gMnSc1
českého	český	k2eAgInSc2d1
fotbalu	fotbal	k1gInSc2
Ivan	Ivan	k1gMnSc1
Hašek	Hašek	k1gMnSc1
rezignoval	rezignovat	k5eAaBmAgMnS
Archivováno	archivován	k2eAgNnSc4d1
5	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
ČT	ČT	kA
24	#num#	k4
<g/>
,	,	kIx,
26	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
↑	↑	k?
Pyrrhovo	Pyrrhův	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
Moravy	Morava	k1gFnSc2
<g/>
:	:	kIx,
Svého	svůj	k3xOyFgMnSc4
Čecha	Čech	k1gMnSc4
Peltu	Pelta	k1gMnSc4
dosadila	dosadit	k5eAaPmAgFnS
do	do	k7c2
křesla	křeslo	k1gNnSc2
předsedy	předseda	k1gMnSc2
<g/>
,	,	kIx,
sama	sám	k3xTgFnSc1
oslabila	oslabit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sport	sport	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-11-17	2011-11-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Stíhaný	stíhaný	k2eAgMnSc1d1
Pelta	Pelta	k1gMnSc1
už	už	k6eAd1
není	být	k5eNaImIp3nS
šéfem	šéf	k1gMnSc7
fotbalu	fotbal	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c4
pozici	pozice	k1gFnSc4
předsedy	předseda	k1gMnSc2
rezignoval	rezignovat	k5eAaBmAgMnS
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-06-06	2017-06-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
česká	český	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
Fotbalová	fotbalový	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
Česká	český	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
Česká	český	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
</s>
<s>
Juniorská	juniorský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
FAČR	FAČR	kA
</s>
<s>
Od	od	k7c2
Chvalovského	Chvalovský	k2eAgMnSc2d1
po	po	k7c4
Haška	Hašek	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
speciál	speciál	k1gInSc1
Českého	český	k2eAgInSc2d1
rozhlasu	rozhlas	k1gInSc2
o	o	k7c6
Fotbalové	fotbalový	k2eAgFnSc6d1
asociaci	asociace	k1gFnSc6
před	před	k7c7
volbami	volba	k1gFnPc7
předsedy	předseda	k1gMnSc2
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hosté	host	k1gMnPc1
<g/>
:	:	kIx,
Miroslav	Miroslav	k1gMnSc1
Pelta	Pelta	k1gMnSc1
<g/>
,	,	kIx,
Markéta	Markéta	k1gFnSc1
Haindlová	Haindlový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
Mašek	Mašek	k1gMnSc1
<g/>
,	,	kIx,
novinář	novinář	k1gMnSc1
Jan	Jan	k1gMnSc1
Kaliba	Kaliba	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Fotbal	fotbal	k1gInSc1
v	v	k7c6
Česku	Česko	k1gNnSc6
Fotbalová	fotbalový	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
Reprezentace	reprezentace	k1gFnSc2
</s>
<s>
Muži	muž	k1gMnPc1
</s>
<s>
Česko	Česko	k1gNnSc1
A	a	k8xC
•	•	k?
Česko	Česko	k1gNnSc1
21	#num#	k4
•	•	k?
Česko	Česko	k1gNnSc1
20	#num#	k4
•	•	k?
Česko	Česko	k1gNnSc1
19	#num#	k4
•	•	k?
Česko	Česko	k1gNnSc1
18	#num#	k4
•	•	k?
Česko	Česko	k1gNnSc1
17	#num#	k4
•	•	k?
Česko	Česko	k1gNnSc1
16	#num#	k4
•	•	k?
Futsal	Futsal	k1gFnSc2
•	•	k?
Plážový	plážový	k2eAgInSc4d1
fotbal	fotbal	k1gInSc4
Ženy	žena	k1gFnPc5
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
Ligové	ligový	k2eAgFnPc1d1
soutěže	soutěž	k1gFnPc1
</s>
<s>
Muži	muž	k1gMnPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
česká	český	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
•	•	k?
2	#num#	k4
<g/>
.	.	kIx.
česká	český	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
•	•	k?
ČFL	ČFL	kA
&	&	k?
MSFL	MSFL	kA
•	•	k?
Divize	divize	k1gFnSc1
A	a	k8xC
•	•	k?
Divize	divize	k1gFnSc2
B	B	kA
•	•	k?
Divize	divize	k1gFnSc2
C	C	kA
•	•	k?
Divize	divize	k1gFnSc2
D	D	kA
•	•	k?
Krajské	krajský	k2eAgInPc1d1
přebory	přebor	k1gInPc1
•	•	k?
Pražský	pražský	k2eAgInSc1d1
přebor	přebor	k1gInSc1
•	•	k?
I.	I.	kA
A	a	k8xC
třídy	třída	k1gFnSc2
•	•	k?
I.	I.	kA
B	B	kA
třídy	třída	k1gFnSc2
•	•	k?
II	II	kA
<g/>
.	.	kIx.
třídy	třída	k1gFnSc2
•	•	k?
III	III	kA
<g/>
.	.	kIx.
třídy	třída	k1gFnSc2
•	•	k?
IV	IV	kA
<g/>
.	.	kIx.
třídy	třída	k1gFnSc2
Ženy	žena	k1gFnPc5
</s>
<s>
I.	I.	kA
liga	liga	k1gFnSc1
žen	žena	k1gFnPc2
<g/>
•	•	k?
II	II	kA
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
žen	žena	k1gFnPc2
</s>
<s>
Pohárové	pohárový	k2eAgFnPc1d1
soutěže	soutěž	k1gFnPc1
</s>
<s>
Pohár	pohár	k1gInSc1
FAČR	FAČR	kA
•	•	k?
Český	český	k2eAgInSc4d1
Superpohár	superpohár	k1gInSc4
•	•	k?
Pohár	pohár	k1gInSc1
KFŽ	KFŽ	kA
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Zlatý	zlatý	k2eAgInSc1d1
míč	míč	k1gInSc1
•	•	k?
Fotbalista	fotbalista	k1gMnSc1
roku	rok	k1gInSc2
•	•	k?
Klub	klub	k1gInSc1
ligových	ligový	k2eAgMnPc2d1
kanonýrů	kanonýr	k1gMnPc2
•	•	k?
Klub	klub	k1gInSc4
ligových	ligový	k2eAgMnPc2d1
brankářů	brankář	k1gMnPc2
•	•	k?
Cena	cena	k1gFnSc1
Václava	Václav	k1gMnSc2
Jíry	Jíra	k1gMnSc2
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
Juniorská	juniorský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
•	•	k?
Tipsport	Tipsport	k1gInSc1
liga	liga	k1gFnSc1
•	•	k?
Fortuna	Fortuna	k1gFnSc1
Víkend	víkend	k1gInSc1
šampionů	šampion	k1gMnPc2
•	•	k?
Pražské	pražský	k2eAgNnSc1d1
derby	derby	k1gNnSc1
•	•	k?
Kauza	kauza	k1gFnSc1
Bohemians	Bohemians	k1gInSc1
•	•	k?
Umístění	umístění	k1gNnSc1
českých	český	k2eAgMnPc2d1
fotbalistů	fotbalista	k1gMnPc2
v	v	k7c6
anketě	anketa	k1gFnSc6
Zlatý	zlatý	k2eAgInSc1d1
míč	míč	k1gInSc1
•	•	k?
Seznam	seznam	k1gInSc4
cizinců	cizinec	k1gMnPc2
v	v	k7c6
1	#num#	k4
<g/>
.	.	kIx.
české	český	k2eAgFnSc6d1
fotbalové	fotbalový	k2eAgFnSc6d1
lize	liga	k1gFnSc6
•	•	k?
České	český	k2eAgInPc4d1
fotbalové	fotbalový	k2eAgInPc4d1
kluby	klub	k1gInPc4
v	v	k7c6
evropských	evropský	k2eAgInPc6d1
pohárech	pohár	k1gInPc6
•	•	k?
Seznam	seznam	k1gInSc4
českých	český	k2eAgMnPc2d1
vítězů	vítěz	k1gMnPc2
nadnárodních	nadnárodní	k2eAgFnPc2d1
fotbalových	fotbalový	k2eAgFnPc2d1
klubových	klubový	k2eAgFnPc2d1
soutěží	soutěž	k1gFnPc2
•	•	k?
Malá	malý	k2eAgFnSc1d1
kopaná	kopaná	k1gFnSc1
</s>
<s>
Sportovní	sportovní	k2eAgInPc1d1
svazy	svaz	k1gInPc1
v	v	k7c6
Českém	český	k2eAgInSc6d1
olympijském	olympijský	k2eAgInSc6d1
výboru	výbor	k1gInSc6
(	(	kIx(
<g/>
ČOV	ČOV	kA
<g/>
)	)	kIx)
Sporty	sport	k1gInPc1
LOH	LOH	kA
</s>
<s>
ČAS	čas	k1gInSc1
(	(	kIx(
<g/>
atletika	atletika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ČBaS	ČBaS	k?
(	(	kIx(
<g/>
badminton	badminton	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČBF	ČBF	kA
(	(	kIx(
<g/>
basketbal	basketbal	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
ČBA	ČBA	kA
(	(	kIx(
<g/>
box	box	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČSC	ČSC	kA
(	(	kIx(
<g/>
cyklistika	cyklistika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
FAČR	FAČR	kA
(	(	kIx(
<g/>
fotbal	fotbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČGF	ČGF	kA
(	(	kIx(
<g/>
golf	golf	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
ČGF	ČGF	kA
(	(	kIx(
<g/>
gymnastika	gymnastik	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
ČSH	ČSH	kA
(	(	kIx(
<g/>
házená	házená	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ČSJ	ČSJ	kA
(	(	kIx(
<g/>
jachting	jachting	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČJF	ČJF	kA
(	(	kIx(
<g/>
jezdectví	jezdectví	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ČSJu	ČSJu	k5eAaPmIp1nS
(	(	kIx(
<g/>
judo	judo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ČSK	ČSK	kA
(	(	kIx(
<g/>
kanoistika	kanoistika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ČLS	ČLS	kA
(	(	kIx(
<g/>
lukostřelba	lukostřelba	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ČSMP	ČSMP	kA
(	(	kIx(
<g/>
moderního	moderní	k2eAgNnSc2d1
pětiboj	pětiboj	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
ČSPS	ČSPS	kA
(	(	kIx(
<g/>
plavání	plavání	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Český	český	k2eAgInSc1d1
svaz	svaz	k1gInSc1
pozemního	pozemní	k2eAgInSc2d1
hokeje	hokej	k1gInSc2
(	(	kIx(
<g/>
pozemní	pozemní	k2eAgInSc1d1
hokej	hokej	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČSRU	ČSRU	kA
(	(	kIx(
<g/>
ragby	ragby	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ČSS	ČSS	kA
(	(	kIx(
<g/>
sportovní	sportovní	k2eAgFnSc1d1
střelba	střelba	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ČAST	ČAST	kA
(	(	kIx(
<g/>
stolní	stolní	k2eAgInSc1d1
tenis	tenis	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČŠS	ČŠS	kA
(	(	kIx(
<g/>
šerm	šerm	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČST	ČST	kA
(	(	kIx(
<g/>
taekwondo	taekwondo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ČTS	ČTS	kA
(	(	kIx(
<g/>
tenis	tenis	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČTA	číst	k5eAaImSgMnS
(	(	kIx(
<g/>
triatlon	triatlon	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
ČVS	ČVS	kA
(	(	kIx(
<g/>
veslování	veslování	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ČVS	ČVS	kA
(	(	kIx(
<g/>
volejbal	volejbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČSV	ČSV	kA
(	(	kIx(
<g/>
vzpírání	vzpírání	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
SZČR	SZČR	kA
(	(	kIx(
<g/>
zápas	zápas	k1gInSc1
<g/>
)	)	kIx)
Sporty	sport	k1gInPc1
ZOH	ZOH	kA
</s>
<s>
ČSB	ČSB	kA
(	(	kIx(
<g/>
biatlon	biatlon	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČSBS	ČSBS	kA
(	(	kIx(
<g/>
boby	bob	k1gInPc1
a	a	k8xC
skeleton	skeleton	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČKS	ČKS	kA
(	(	kIx(
<g/>
krasobruslení	krasobruslení	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ČSR	ČSR	kA
(	(	kIx(
<g/>
rychlobruslení	rychlobruslení	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ČSC	ČSC	kA
(	(	kIx(
<g/>
curling	curling	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČSLH	ČSLH	kA
(	(	kIx(
<g/>
lední	lední	k2eAgInSc4d1
hokej	hokej	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
SLČR	SLČR	kA
(	(	kIx(
<g/>
lyžování	lyžování	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
ČSA	ČSA	kA
(	(	kIx(
<g/>
saně	saně	k1gFnPc1
<g/>
)	)	kIx)
Sporty	sport	k1gInPc1
uznané	uznaný	k2eAgInPc1d1
MOV	MOV	kA
</s>
<s>
ČSAE	ČSAE	kA
(	(	kIx(
<g/>
aerobik	aerobik	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
AČR	AČR	kA
(	(	kIx(
<g/>
automobilový	automobilový	k2eAgInSc4d1
a	a	k8xC
motocyklový	motocyklový	k2eAgInSc4d1
sport	sport	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
ČAB	ČAB	kA
(	(	kIx(
<g/>
bandy	bandy	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ČBA	ČBA	kA
(	(	kIx(
<g/>
baseball	baseball	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
ČMBS	ČMBS	kA
(	(	kIx(
<g/>
billiard	billiard	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
ČBS	ČBS	kA
(	(	kIx(
<g/>
bridž	bridž	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČF	ČF	kA
(	(	kIx(
<g/>
florbal	florbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČHS	ČHS	kA
(	(	kIx(
<g/>
horolezectví	horolezectví	k1gNnSc1
a	a	k8xC
sportovní	sportovní	k2eAgNnPc1d1
lezení	lezení	k1gNnPc1
<g/>
)	)	kIx)
</s>
<s>
ČSKe	ČSKe	k1gNnSc1
(	(	kIx(
<g/>
karate	karate	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ČUKB	ČUKB	kA
(	(	kIx(
<g/>
kolečkové	kolečkový	k2eAgInPc4d1
sporty	sport	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
ČKS	ČKS	kA
(	(	kIx(
<g/>
korfbal	korfbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
není	být	k5eNaImIp3nS
(	(	kIx(
<g/>
koulové	koulový	k2eAgInPc4d1
sporty	sport	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
ČMKS	ČMKS	kA
(	(	kIx(
<g/>
kriket	kriket	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČKBF	ČKBF	kA
(	(	kIx(
<g/>
kuželky	kuželka	k1gFnPc1
a	a	k8xC
bowling	bowling	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
AeČR	AeČR	k?
(	(	kIx(
<g/>
letecký	letecký	k2eAgInSc1d1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
C.	C.	kA
<g/>
M.	M.	kA
<g/>
T.A.	T.A.	k1gFnPc2
(	(	kIx(
<g/>
muay	mua	k2eAgFnPc1d1
thai	tha	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
není	být	k5eNaImIp3nS
(	(	kIx(
<g/>
netball	netball	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČSOS	ČSOS	kA
(	(	kIx(
<g/>
orientační	orientační	k2eAgInPc4d1
sporty	sport	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
není	být	k5eNaImIp3nS
(	(	kIx(
<g/>
pelota	pelota	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ČFP	ČFP	kA
<g/>
?	?	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
pólo	pólo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
SPČR	SPČR	kA
(	(	kIx(
<g/>
potápění	potápění	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
není	být	k5eNaImIp3nS
(	(	kIx(
<g/>
přetahování	přetahování	k1gNnPc2
lanem	lano	k1gNnSc7
<g/>
)	)	kIx)
</s>
<s>
není	být	k5eNaImIp3nS
(	(	kIx(
<g/>
raketbal	raketbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČSA	ČSA	kA
(	(	kIx(
<g/>
softball	softball	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČASQ	ČASQ	kA
(	(	kIx(
<g/>
squash	squash	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČSS	ČSS	kA
(	(	kIx(
<g/>
sumó	sumó	k?
<g/>
)	)	kIx)
</s>
<s>
AČSF	AČSF	kA
(	(	kIx(
<g/>
surfing	surfing	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ŠSČR	ŠSČR	kA
(	(	kIx(
<g/>
šachy	šach	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
ČSTS	ČSTS	kA
(	(	kIx(
<g/>
taneční	taneční	k2eAgInSc1d1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČSVLW	ČSVLW	kA
(	(	kIx(
<g/>
vodní	vodní	k2eAgNnSc1d1
lyžování	lyžování	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ČSVM	ČSVM	kA
(	(	kIx(
<g/>
vodní	vodní	k2eAgInSc1d1
motorismus	motorismus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
???	???	k?
(	(	kIx(
<g/>
wu-šu	wu-sat	k5eAaPmIp1nS
<g/>
)	)	kIx)
</s>
<s>
VZS	VZS	kA
ČČK	ČČK	kA
(	(	kIx(
<g/>
záchranářský	záchranářský	k2eAgInSc1d1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
Mezinárodní	mezinárodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
</s>
<s>
SportAccord	SportAccord	k6eAd1
</s>
<s>
MOV	MOV	kA
</s>
<s>
ASOIF	ASOIF	kA
</s>
<s>
AIOWF	AIOWF	kA
</s>
<s>
ARISF	ARISF	kA
</s>
<s>
IWGA	IWGA	kA
</s>
<s>
IPC	IPC	kA
</s>
<s>
Sportovní	sportovní	k2eAgInPc1d1
svazy	svaz	k1gInPc1
v	v	k7c6
České	český	k2eAgFnSc6d1
unii	unie	k1gFnSc6
sportu	sport	k1gInSc2
(	(	kIx(
<g/>
ČUS	ČUS	k?
<g/>
)	)	kIx)
Sportovní	sportovní	k2eAgFnSc6d1
svazysdružené	svazysdružený	k2eAgFnSc6d1
v	v	k7c6
ČUS	ČUS	k?
</s>
<s>
ČSAE	ČSAE	kA
(	(	kIx(
<g/>
aerobik	aerobik	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČSAR	ČSAR	kA
(	(	kIx(
<g/>
akrobatický	akrobatický	k2eAgInSc1d1
rock	rock	k1gInSc1
and	and	k?
roll	roll	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČAAF	ČAAF	kA
(	(	kIx(
<g/>
americký	americký	k2eAgInSc1d1
fotbal	fotbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČAS	čas	k1gInSc1
(	(	kIx(
<g/>
atletika	atletika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ČBaS	ČBaS	k?
(	(	kIx(
<g/>
badminton	badminton	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČBA	ČBA	kA
(	(	kIx(
<g/>
baseball	baseball	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
ČBF	ČBF	kA
(	(	kIx(
<g/>
basketbal	basketbal	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
ČMBS	ČMBS	kA
(	(	kIx(
<g/>
billiard	billiard	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
ČSBS	ČSBS	kA
(	(	kIx(
<g/>
boby	bob	k1gInPc1
a	a	k8xC
skeleton	skeleton	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČUBU	čuba	k1gFnSc4
(	(	kIx(
<g/>
bojová	bojový	k2eAgNnPc1d1
umění	umění	k1gNnPc1
<g/>
)	)	kIx)
</s>
<s>
ČBA	ČBA	kA
(	(	kIx(
<g/>
box	box	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČSC	ČSC	kA
(	(	kIx(
<g/>
curling	curling	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČSC	ČSC	kA
(	(	kIx(
<g/>
cyklistika	cyklistika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ČAES	ČAES	kA
(	(	kIx(
<g/>
extrémní	extrémní	k2eAgInPc4d1
sporty	sport	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
ČF	ČF	kA
(	(	kIx(
<g/>
florbal	florbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FAČR	FAČR	kA
(	(	kIx(
<g/>
fotbal	fotbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČGF	ČGF	kA
(	(	kIx(
<g/>
golf	golf	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
ČGF	ČGF	kA
(	(	kIx(
<g/>
gymnastika	gymnastik	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
ČSH	ČSH	kA
(	(	kIx(
<g/>
házená	házená	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ČMSHb	ČMSHb	k1gInSc1
(	(	kIx(
<g/>
hokejbal	hokejbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČHS	ČHS	kA
(	(	kIx(
<g/>
horolezectví	horolezectví	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
HS	HS	kA
ČR	ČR	kA
(	(	kIx(
<g/>
Horská	horský	k2eAgFnSc1d1
služba	služba	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ČACH	ČACH	kA
(	(	kIx(
<g/>
cheerleading	cheerleading	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČSJ	ČSJ	kA
(	(	kIx(
<g/>
jachting	jachting	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČJF	ČJF	kA
(	(	kIx(
<g/>
jezdectví	jezdectví	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ČSJ	ČSJ	kA
(	(	kIx(
<g/>
jóga	jóga	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ČSJu	ČSJu	k5eAaPmIp1nS
(	(	kIx(
<g/>
judo	judo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ČSK	ČSK	kA
(	(	kIx(
<g/>
kanoistika	kanoistika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ČUKB	ČUKB	kA
(	(	kIx(
<g/>
kolečkové	kolečkový	k2eAgFnSc2d1
brusle	brusle	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
ČKS	ČKS	kA
(	(	kIx(
<g/>
korfbal	korfbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČKS	ČKS	kA
(	(	kIx(
<g/>
krasobruslení	krasobruslení	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
SKFČR	SKFČR	kA
(	(	kIx(
<g/>
kulturistika	kulturistika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ČSK	ČSK	kA
(	(	kIx(
<g/>
kuše	kuše	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ČKBF	ČKBF	kA
(	(	kIx(
<g/>
kuželky	kuželka	k1gFnPc1
a	a	k8xC
bowling	bowling	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČLU	ČLU	kA
(	(	kIx(
<g/>
lakros	lakros	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČSLH	ČSLH	kA
(	(	kIx(
<g/>
lední	lední	k2eAgInSc4d1
hokej	hokej	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
ČLS	ČLS	kA
(	(	kIx(
<g/>
lukostřelba	lukostřelba	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
SLČR	SLČR	kA
(	(	kIx(
<g/>
lyžování	lyžování	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
ČSM	ČSM	kA
(	(	kIx(
<g/>
metaná	metaný	k2eAgFnSc1d1
<g/>
)	)	kIx)
</s>
<s>
ČMGS	ČMGS	kA
(	(	kIx(
<g/>
minigolf	minigolf	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČSMG	ČSMG	kA
(	(	kIx(
<g/>
moderní	moderní	k2eAgFnSc1d1
gymnastika	gymnastika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ČSMP	ČSMP	kA
(	(	kIx(
<g/>
moderního	moderní	k2eAgNnSc2d1
pětiboj	pětiboj	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
C.	C.	kA
<g/>
M.	M.	kA
<g/>
T.A.	T.A.	k1gFnPc2
(	(	kIx(
<g/>
muay	mua	k2eAgFnPc1d1
thai	tha	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
SNH	SNH	kA
(	(	kIx(
<g/>
národní	národní	k2eAgFnSc1d1
házená	házená	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ČNS	ČNS	kA
(	(	kIx(
<g/>
nohejbal	nohejbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČSOS	ČSOS	kA
(	(	kIx(
<g/>
orientační	orientační	k2eAgInSc1d1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČAPEK	Čapek	k1gMnSc1
(	(	kIx(
<g/>
pétanque	pétanque	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČSPS	ČSPS	kA
(	(	kIx(
<g/>
plavání	plavání	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Český	český	k2eAgInSc1d1
svaz	svaz	k1gInSc1
pozemního	pozemní	k2eAgInSc2d1
hokeje	hokej	k1gInSc2
(	(	kIx(
<g/>
pozemní	pozemní	k2eAgInSc1d1
hokej	hokej	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČASS	ČASS	kA
(	(	kIx(
<g/>
psí	psí	k2eAgNnSc1d1
spřežení	spřežení	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
AROB	AROB	kA
(	(	kIx(
<g/>
rádiový	rádiový	k2eAgInSc1d1
orientační	orientační	k2eAgInSc1d1
běh	běh	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČSRU	ČSRU	kA
(	(	kIx(
<g/>
ragby	ragby	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ČSR	ČSR	kA
(	(	kIx(
<g/>
rychlobruslení	rychlobruslení	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ČSA	ČSA	kA
(	(	kIx(
<g/>
saně	saně	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ČSST	ČSST	kA
(	(	kIx(
<g/>
silový	silový	k2eAgInSc1d1
trojboj	trojboj	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Český	český	k2eAgInSc1d1
svaz	svaz	k1gInSc1
skibobistů	skibobista	k1gMnPc2
(	(	kIx(
<g/>
skiboby	skibob	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
ČSA	ČSA	kA
(	(	kIx(
<g/>
softball	softball	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČASQ	ČASQ	kA
(	(	kIx(
<g/>
squash	squash	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČAST	ČAST	kA
(	(	kIx(
<g/>
stolní	stolní	k2eAgInSc1d1
tenis	tenis	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ŠSČR	ŠSČR	kA
(	(	kIx(
<g/>
šachy	šach	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
ČŠS	ČŠS	kA
(	(	kIx(
<g/>
šerm	šerm	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČST	ČST	kA
(	(	kIx(
<g/>
taekwondo	taekwondo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ČTS	ČTS	kA
(	(	kIx(
<g/>
tenis	tenis	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČTA	číst	k5eAaImSgMnS
(	(	kIx(
<g/>
triatlon	triatlon	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
ČAUS	ČAUS	kA
(	(	kIx(
<g/>
univerzitní	univerzitní	k2eAgInSc1d1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČVS	ČVS	kA
(	(	kIx(
<g/>
veslování	veslování	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ČSVLW	ČSVLW	kA
(	(	kIx(
<g/>
vodní	vodní	k2eAgNnSc1d1
lyžování	lyžování	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ČSVM	ČSVM	kA
(	(	kIx(
<g/>
vodní	vodní	k2eAgInSc1d1
motorismus	motorismus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČSVP	ČSVP	kA
(	(	kIx(
<g/>
vodní	vodní	k2eAgNnSc1d1
pólo	pólo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ČVS	ČVS	kA
(	(	kIx(
<g/>
volejbal	volejbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ČSV	ČSV	kA
(	(	kIx(
<g/>
vzpírání	vzpírání	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
SZČR	SZČR	kA
(	(	kIx(
<g/>
zápas	zápas	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
UZPS	UZPS	kA
(	(	kIx(
<g/>
paralympijské	paralympijský	k2eAgInPc1d1
sporty	sport	k1gInPc1
<g/>
)	)	kIx)
Sportovní	sportovní	k2eAgFnPc1d1
svazypřidružené	svazypřidružený	k2eAgFnPc1d1
k	k	k7c3
ČUS	ČUS	k?
</s>
<s>
Asociace	asociace	k1gFnSc1
bazénů	bazén	k1gInPc2
a	a	k8xC
saun	sauna	k1gFnPc2
ČR	ČR	kA
</s>
<s>
Asociace	asociace	k1gFnSc1
plaveckých	plavecký	k2eAgFnPc2d1
škol	škola	k1gFnPc2
</s>
<s>
Asociace	asociace	k1gFnSc1
pracovníků	pracovník	k1gMnPc2
v	v	k7c6
regeneraci	regenerace	k1gFnSc6
</s>
<s>
Česká	český	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
Go	Go	k1gFnSc2
</s>
<s>
Česká	český	k2eAgFnSc1d1
foosballová	foosballový	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
</s>
<s>
Český	český	k2eAgInSc1d1
bridžový	bridžový	k2eAgInSc1d1
svaz	svaz	k1gInSc1
</s>
<s>
Česká	český	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
racketlonu	racketlon	k1gInSc2
</s>
<s>
Český	český	k2eAgInSc1d1
rybářský	rybářský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
</s>
<s>
Český	český	k2eAgInSc1d1
svaz	svaz	k1gInSc1
koloběhu	koloběh	k1gInSc2
</s>
<s>
Český	český	k2eAgInSc1d1
svaz	svaz	k1gInSc1
mariáše	mariáš	k1gInSc2
</s>
<s>
Unie	unie	k1gFnSc1
šipkových	šipkový	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
ČR	ČR	kA
</s>
<s>
Paintball	paintball	k1gInSc1
Games	Gamesa	k1gFnPc2
Bohemia	bohemia	k1gFnSc1
Association	Association	k1gInSc1
</s>
<s>
Sdružení	sdružení	k1gNnSc1
zimních	zimní	k2eAgInPc2d1
stadionů	stadion	k1gInPc2
v	v	k7c6
ČR	ČR	kA
</s>
<s>
Unie	unie	k1gFnSc1
armádních	armádní	k2eAgInPc2d1
sportovních	sportovní	k2eAgInPc2d1
klubů	klub	k1gInPc2
ČR	ČR	kA
</s>
<s>
Unie	unie	k1gFnSc1
hráčů	hráč	k1gMnPc2
stolního	stolní	k2eAgInSc2d1
hokeje	hokej	k1gInSc2
</s>
<s>
Česká	český	k2eAgFnSc1d1
unie	unie	k1gFnSc1
sportu	sport	k1gInSc2
v	v	k7c6
přetahování	přetahování	k1gNnSc6
lanem	lano	k1gNnSc7
</s>
<s>
Česká	český	k2eAgFnSc1d1
Bowls	Bowls	k1gInSc1
asociace	asociace	k1gFnSc2
</s>
<s>
Česká	český	k2eAgFnSc1d1
ricochetová	ricochetový	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
</s>
<s>
Česká	český	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
Nordic	Nordic	k1gMnSc1
Walking	Walking	k1gInSc1
</s>
<s>
Česká	český	k2eAgFnSc1d1
šipková	šipkový	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
</s>
<s>
Česká	český	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
Stiga	Stiga	k1gFnSc1
Game	game	k1gInSc4
</s>
<s>
Asociace	asociace	k1gFnSc1
českomoravského	českomoravský	k2eAgInSc2d1
kroketu	kroket	k1gInSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
mzk	mzk	k?
<g/>
2011658396	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
186246494	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Fotbal	fotbal	k1gInSc1
</s>
