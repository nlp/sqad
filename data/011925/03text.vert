<p>
<s>
Falso	Falsa	k1gFnSc5	Falsa
Azufre	Azufr	k1gInSc5	Azufr
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
neaktivního	aktivní	k2eNgInSc2d1	neaktivní
vulkanického	vulkanický	k2eAgInSc2d1	vulkanický
komplexu	komplex	k1gInSc2	komplex
<g/>
,	,	kIx,	,
tvořeného	tvořený	k2eAgInSc2d1	tvořený
soustavou	soustava	k1gFnSc7	soustava
překrývajících	překrývající	k2eAgInPc2d1	překrývající
se	se	k3xPyFc4	se
kráterů	kráter	k1gInPc2	kráter
<g/>
,	,	kIx,	,
dómů	dóm	k1gInPc2	dóm
a	a	k8xC	a
lávových	lávový	k2eAgInPc2d1	lávový
proudů	proud	k1gInPc2	proud
<g/>
,	,	kIx,	,
ležících	ležící	k2eAgFnPc2d1	ležící
v	v	k7c6	v
15	[number]	k4	15
km	km	kA	km
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
linii	linie	k1gFnSc6	linie
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
Chile	Chile	k1gNnSc2	Chile
a	a	k8xC	a
Argentiny	Argentina	k1gFnSc2	Argentina
<g/>
,	,	kIx,	,
táhnoucí	táhnoucí	k2eAgInPc4d1	táhnoucí
se	se	k3xPyFc4	se
od	od	k7c2	od
východu	východ	k1gInSc2	východ
k	k	k7c3	k
západu	západ	k1gInSc3	západ
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
komplexu	komplex	k1gInSc2	komplex
je	být	k5eAaImIp3nS	být
odhadován	odhadovat	k5eAaImNgInS	odhadovat
na	na	k7c4	na
období	období	k1gNnSc4	období
pleistocénu	pleistocén	k1gInSc2	pleistocén
až	až	k8xS	až
holocénu	holocén	k1gInSc2	holocén
<g/>
.	.	kIx.	.
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
krátery	kráter	k1gInPc1	kráter
a	a	k8xC	a
lávové	lávový	k2eAgInPc1d1	lávový
dómy	dóm	k1gInPc1	dóm
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
vrchol	vrchol	k1gInSc1	vrchol
Cerro	Cerro	k1gNnSc1	Cerro
Falso	Falsa	k1gFnSc5	Falsa
Azufre	Azufr	k1gInSc5	Azufr
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
konci	konec	k1gInSc6	konec
komplexu	komplex	k1gInSc2	komplex
v	v	k7c6	v
Chile	Chile	k1gNnSc6	Chile
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
probíhá	probíhat	k5eAaImIp3nS	probíhat
většina	většina	k1gFnSc1	většina
vyvrhování	vyvrhování	k1gNnPc2	vyvrhování
pyroklastického	pyroklastický	k2eAgInSc2d1	pyroklastický
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Mladší	mladý	k2eAgFnSc1d2	mladší
část	část	k1gFnSc1	část
komplexu	komplex	k1gInSc2	komplex
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Argentině	Argentina	k1gFnSc6	Argentina
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
dva	dva	k4xCgInPc1	dva
lávové	lávový	k2eAgInPc1d1	lávový
dómy	dóm	k1gInPc1	dóm
a	a	k8xC	a
dva	dva	k4xCgInPc1	dva
sopečné	sopečný	k2eAgInPc1d1	sopečný
kužely	kužel	k1gInPc1	kužel
<g/>
.	.	kIx.	.
<g/>
Láva	láva	k1gFnSc1	láva
produkovaná	produkovaný	k2eAgFnSc1d1	produkovaná
sopkou	sopka	k1gFnSc7	sopka
je	být	k5eAaImIp3nS	být
andezitická	andezitický	k2eAgFnSc1d1	andezitický
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
hornblend	hornblend	k1gInSc1	hornblend
a	a	k8xC	a
pyroxeny	pyroxen	k1gInPc1	pyroxen
s	s	k7c7	s
celkovým	celkový	k2eAgInSc7d1	celkový
obsahem	obsah	k1gInSc7	obsah
58-61	[number]	k4	58-61
%	%	kIx~	%
SiO	SiO	k1gFnSc2	SiO
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Andezity	andezit	k1gInPc1	andezit
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
úpatí	úpatí	k1gNnSc6	úpatí
sopky	sopka	k1gFnSc2	sopka
jsou	být	k5eAaImIp3nP	být
staré	stará	k1gFnPc1	stará
0,7	[number]	k4	0,7
±	±	k?	±
0,2	[number]	k4	0,2
milionu	milion	k4xCgInSc2	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Falso	Falsa	k1gFnSc5	Falsa
Azufre	Azufr	k1gInSc5	Azufr
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc3d1	slovenská
Wikipedii	Wikipedie	k1gFnSc3	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
www.volcano.si.edu	www.volcano.si.et	k5eAaPmIp1nS	www.volcano.si.et
-	-	kIx~	-
komplex	komplex	k1gInSc1	komplex
Falso	Falsa	k1gFnSc5	Falsa
Azufre	Azufr	k1gInSc5	Azufr
na	na	k7c4	na
Global	globat	k5eAaImAgInS	globat
Volcanism	Volcanism	k1gInSc1	Volcanism
Program	program	k1gInSc1	program
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
