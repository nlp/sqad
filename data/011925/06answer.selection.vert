<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
vrchol	vrchol	k1gInSc1	vrchol
Cerro	Cerro	k1gNnSc1	Cerro
Falso	Falsa	k1gFnSc5	Falsa
Azufre	Azufr	k1gInSc5	Azufr
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
konci	konec	k1gInSc6	konec
komplexu	komplex	k1gInSc2	komplex
v	v	k7c6	v
Chile	Chile	k1gNnSc6	Chile
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
probíhá	probíhat	k5eAaImIp3nS	probíhat
většina	většina	k1gFnSc1	většina
vyvrhování	vyvrhování	k1gNnPc2	vyvrhování
pyroklastického	pyroklastický	k2eAgInSc2d1	pyroklastický
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
