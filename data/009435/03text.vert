<p>
<s>
Tenká	tenký	k2eAgFnSc1d1	tenká
růžová	růžový	k2eAgFnSc1d1	růžová
čára	čára	k1gFnSc1	čára
(	(	kIx(	(
<g/>
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
originále	originál	k1gInSc6	originál
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Thin	Thin	k1gMnSc1	Thin
Pink	pink	k6eAd1	pink
Line	linout	k5eAaImIp3nS	linout
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
filmová	filmový	k2eAgFnSc1d1	filmová
komedie	komedie	k1gFnSc1	komedie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Režisérem	režisér	k1gMnSc7	režisér
filmu	film	k1gInSc2	film
je	být	k5eAaImIp3nS	být
duo	duo	k1gNnSc1	duo
Joe	Joe	k1gFnSc2	Joe
Dietl	Dietl	k1gFnPc2	Dietl
a	a	k8xC	a
Michael	Michaela	k1gFnPc2	Michaela
Irpino	Irpino	k1gNnSc4	Irpino
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
role	role	k1gFnSc1	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
ztvárnili	ztvárnit	k5eAaPmAgMnP	ztvárnit
Jennifer	Jennifer	k1gMnSc1	Jennifer
Aniston	Aniston	k1gInSc4	Aniston
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Schwimmer	Schwimmer	k1gMnSc1	Schwimmer
<g/>
,	,	kIx,	,
Jason	Jason	k1gMnSc1	Jason
Priestley	Priestlea	k1gFnSc2	Priestlea
<g/>
,	,	kIx,	,
Mike	Mike	k1gNnSc1	Mike
Myers	Myersa	k1gFnPc2	Myersa
a	a	k8xC	a
Christine	Christin	k1gInSc5	Christin
Elise	elise	k1gFnSc2	elise
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reakce	reakce	k1gFnPc1	reakce
==	==	k?	==
</s>
</p>
<p>
<s>
aktuální	aktuální	k2eAgInPc1d1	aktuální
k	k	k7c3	k
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
2015	[number]	k4	2015
<g/>
csfd	csfda	k1gFnPc2	csfda
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
imdb	imdb	k1gInSc1	imdb
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
fdb	fdb	k?	fdb
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
:	:	kIx,	:
-	-	kIx~	-
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Tenká	tenký	k2eAgFnSc1d1	tenká
růžová	růžový	k2eAgFnSc1d1	růžová
čára	čára	k1gFnSc1	čára
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Tenká	tenký	k2eAgFnSc1d1	tenká
růžová	růžový	k2eAgFnSc1d1	růžová
čára	čára	k1gFnSc1	čára
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tenká	tenký	k2eAgFnSc1d1	tenká
růžová	růžový	k2eAgFnSc1d1	růžová
čára	čára	k1gFnSc1	čára
ve	v	k7c6	v
Filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
