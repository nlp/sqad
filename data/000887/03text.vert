<s>
Got	Got	k?	Got
to	ten	k3xDgNnSc1	ten
Be	Be	k1gMnSc1	Be
There	Ther	k1gInSc5	Ther
je	být	k5eAaImIp3nS	být
sólové	sólový	k2eAgNnSc4d1	sólové
debutové	debutový	k2eAgNnSc4d1	debutové
album	album	k1gNnSc4	album
tehdy	tehdy	k6eAd1	tehdy
teenagera	teenager	k1gMnSc4	teenager
Michaela	Michael	k1gMnSc4	Michael
Jacksona	Jackson	k1gMnSc4	Jackson
<g/>
,	,	kIx,	,
vydané	vydaný	k2eAgMnPc4d1	vydaný
v	v	k7c4	v
Motown	Motown	k1gNnSc4	Motown
Records	Recordsa	k1gFnPc2	Recordsa
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
stejnojmennou	stejnojmenný	k2eAgFnSc4d1	stejnojmenná
skladbu	skladba	k1gFnSc4	skladba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
jako	jako	k8xC	jako
sólový	sólový	k2eAgInSc1d1	sólový
debutový	debutový	k2eAgInSc1d1	debutový
singl	singl	k1gInSc1	singl
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Ain	Ain	k1gFnSc1	Ain
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
No	no	k9	no
Sunshine	Sunshin	k1gInSc5	Sunshin
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Withers	Withers	k1gInSc1	Withers
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
originál	originál	k1gInSc1	originál
nazpíval	nazpívat	k5eAaBmAgMnS	nazpívat
Bill	Bill	k1gMnSc1	Bill
Withers	Withersa	k1gFnPc2	Withersa
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
I	i	k9	i
Wanna	Wanen	k2eAgFnSc1d1	Wanna
Be	Be	k1gFnSc1	Be
Where	Wher	k1gInSc5	Wher
You	You	k1gMnSc1	You
Are	ar	k1gInSc5	ar
<g />
.	.	kIx.	.
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Ware	Ware	k1gInSc1	Ware
<g/>
/	/	kIx~	/
<g/>
Ross	Ross	k1gInSc1	Ross
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Girl	girl	k1gFnSc1	girl
Don	Don	k1gMnSc1	Don
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Take	Take	k1gInSc1	Take
Your	Your	k1gInSc1	Your
Love	lov	k1gInSc5	lov
From	From	k1gInSc4	From
Me	Me	k1gFnPc4	Me
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Hutch	Hutch	k1gInSc1	Hutch
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
In	In	k1gMnSc1	In
Our	Our	k1gMnSc1	Our
Small	Small	k1gMnSc1	Small
Way	Way	k1gMnSc1	Way
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Verdi	Verd	k1gMnPc1	Verd
<g/>
/	/	kIx~	/
<g/>
Yarian	Yarian	k1gMnSc1	Yarian
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Got	Got	k?	Got
to	ten	k3xDgNnSc1	ten
Be	Be	k1gMnSc5	Be
There	Ther	k1gMnSc5	Ther
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Willensky	Willensky	k1gFnSc1	Willensky
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Rockin	Rockin	k2eAgMnSc1d1	Rockin
<g/>
'	'	kIx"	'
Robin	robin	k2eAgMnSc1d1	robin
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Thomas	Thomas	k1gMnSc1	Thomas
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
originál	originál	k1gInSc1	originál
nazpíval	nazpívat	k5eAaBmAgInS	nazpívat
Bobby	Bobb	k1gInPc4	Bobb
Day	Day	k1gFnSc2	Day
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Wings	Wings	k1gInSc1	Wings
of	of	k?	of
My	my	k3xPp1nPc1	my
Love	lov	k1gInSc5	lov
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Corporation	Corporation	k1gInSc1	Corporation
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Maria	Maria	k1gFnSc1	Maria
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
You	You	k?	You
Were	Were	k1gFnSc1	Were
the	the	k?	the
Only	Onl	k2eAgFnPc4d1	Onl
One	One	k1gFnPc4	One
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Brown	Brown	k1gMnSc1	Brown
<g/>
/	/	kIx~	/
<g/>
Glover	Glover	k1gMnSc1	Glover
<g/>
/	/	kIx~	/
<g/>
Gordy	Gord	k1gInPc1	Gord
<g/>
/	/	kIx~	/
<g/>
Story	story	k1gFnSc1	story
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Love	lov	k1gInSc5	lov
Is	Is	k1gMnPc7	Is
Here	Here	k1gNnSc1	Here
and	and	k?	and
Now	Now	k1gMnSc2	Now
You	You	k1gMnSc2	You
<g/>
'	'	kIx"	'
<g/>
re	re	k9	re
Gone	Gone	k1gNnSc2	Gone
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Holland-Dozier-Holland	Holland-Dozier-Holland	k1gInSc1	Holland-Dozier-Holland
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
originál	originál	k1gMnSc1	originál
nazpívali	nazpívat	k5eAaPmAgMnP	nazpívat
<g />
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Supremes	Supremes	k1gInSc1	Supremes
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
You	You	k1gMnSc1	You
<g/>
'	'	kIx"	'
<g/>
ve	v	k7c6	v
Got	Got	k1gFnSc6	Got
a	a	k8xC	a
Friend	Friend	k1gInSc4	Friend
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
King	King	k1gMnSc1	King
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
originál	originál	k1gInSc4	originál
nazpívala	nazpívat	k5eAaBmAgFnS	nazpívat
Carole	Carole	k1gFnSc1	Carole
King	King	k1gMnSc1	King
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Got	Got	k1gMnSc1	Got
to	ten	k3xDgNnSc4	ten
Be	Be	k1gMnSc5	Be
There	Ther	k1gMnSc5	Ther
<g/>
"	"	kIx"	"
-	-	kIx~	-
#	#	kIx~	#
<g/>
4	[number]	k4	4
Billboard	billboard	k1gInSc1	billboard
Pop	pop	k1gMnSc1	pop
Singles	Singles	k1gMnSc1	Singles
<g/>
;	;	kIx,	;
#	#	kIx~	#
<g/>
4	[number]	k4	4
Hot	hot	k0	hot
<g />
.	.	kIx.	.
</s>
<s>
Soul	Soul	k1gInSc1	Soul
Singles	Singlesa	k1gFnPc2	Singlesa
<g/>
,	,	kIx,	,
#	#	kIx~	#
<g/>
14	[number]	k4	14
Easy	Easa	k1gFnSc2	Easa
Listening	Listening	k1gInSc1	Listening
Singles	Singles	k1gMnSc1	Singles
Chart	charta	k1gFnPc2	charta
"	"	kIx"	"
<g/>
Rockin	Rockin	k1gInSc1	Rockin
<g/>
'	'	kIx"	'
Robin	robin	k2eAgMnSc1d1	robin
<g/>
"	"	kIx"	"
-	-	kIx~	-
#	#	kIx~	#
<g/>
2	[number]	k4	2
Billboard	billboard	k1gInSc1	billboard
Pop	pop	k1gMnSc1	pop
Singles	Singles	k1gMnSc1	Singles
<g/>
;	;	kIx,	;
#	#	kIx~	#
<g/>
2	[number]	k4	2
Hot	hot	k0	hot
Soul	Soul	k1gInSc1	Soul
Singles	Singles	k1gInSc4	Singles
"	"	kIx"	"
<g/>
I	i	k9	i
Wanna	Wanen	k2eAgFnSc1d1	Wanna
Be	Be	k1gFnSc1	Be
Where	Wher	k1gInSc5	Wher
You	You	k1gMnSc1	You
Are	ar	k1gInSc5	ar
<g/>
"	"	kIx"	"
-	-	kIx~	-
#	#	kIx~	#
<g/>
16	[number]	k4	16
Billboard	billboard	k1gInSc1	billboard
Pop	pop	k1gMnSc1	pop
Singles	Singles	k1gMnSc1	Singles
<g/>
;	;	kIx,	;
#	#	kIx~	#
<g/>
2	[number]	k4	2
Hot	hot	k0	hot
Soul	Soul	k1gInSc1	Soul
Singles	Singles	k1gInSc4	Singles
</s>
