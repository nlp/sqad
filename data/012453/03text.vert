<p>
<s>
Stanley	Stanlea	k1gMnSc2	Stanlea
Grauman	Grauman	k1gMnSc1	Grauman
Weinbaum	Weinbaum	k1gInSc1	Weinbaum
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
duben	duben	k1gInSc1	duben
1902	[number]	k4	1902
<g/>
,	,	kIx,	,
Louisville	Louisville	k1gInSc1	Louisville
<g/>
,	,	kIx,	,
Kentucky	Kentuck	k1gInPc1	Kentuck
<g/>
,	,	kIx,	,
USA	USA	kA	USA
–	–	k?	–
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc1	prosinec
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
spisovatel	spisovatel	k1gMnSc1	spisovatel
vědeckofantastické	vědeckofantastický	k2eAgFnSc2d1	vědeckofantastická
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
kariéra	kariéra	k1gFnSc1	kariéra
byla	být	k5eAaImAgFnS	být
krátká	krátký	k2eAgFnSc1d1	krátká
(	(	kIx(	(
<g/>
začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
profesionálně	profesionálně	k6eAd1	profesionálně
věnovat	věnovat	k5eAaPmF	věnovat
psaní	psaní	k1gNnSc4	psaní
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
celý	celý	k2eAgInSc4d1	celý
žánr	žánr	k1gInSc4	žánr
sci-fi	scii	k1gFnSc2	sci-fi
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
především	především	k9	především
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
povídce	povídka	k1gFnSc3	povídka
"	"	kIx"	"
<g/>
Odysea	odysea	k1gFnSc1	odysea
z	z	k7c2	z
Marsu	Mars	k1gInSc2	Mars
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
A	a	k9	a
Martian	Martian	k1gInSc1	Martian
Odyssey	Odyssea	k1gFnSc2	Odyssea
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
publikované	publikovaný	k2eAgFnPc4d1	publikovaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k1gNnSc1	další
jeho	jeho	k3xOp3gFnSc2	jeho
povídky	povídka	k1gFnSc2	povídka
pojednávají	pojednávat	k5eAaImIp3nP	pojednávat
většinou	většina	k1gFnSc7	většina
o	o	k7c6	o
dobrodruhovi	dobrodruh	k1gMnSc6	dobrodruh
a	a	k8xC	a
výzkumníkovi	výzkumník	k1gMnSc6	výzkumník
Hamu	Ham	k1gMnSc6	Ham
Hammondovi	Hammond	k1gMnSc6	Hammond
a	a	k8xC	a
o	o	k7c6	o
bláznivém	bláznivý	k2eAgInSc6d1	bláznivý
vědci	vědec	k1gMnPc1	vědec
a	a	k8xC	a
vynálezci	vynálezce	k1gMnPc1	vynálezce
Haskelu	Haskel	k1gInSc2	Haskel
van	van	k1gInSc4	van
Manderpootzovi	Manderpootz	k1gMnSc3	Manderpootz
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Romány	román	k1gInPc4	román
===	===	k?	===
</s>
</p>
<p>
<s>
The	The	k?	The
Lady	lady	k1gFnSc1	lady
Dances	Dances	k1gMnSc1	Dances
(	(	kIx(	(
<g/>
King-Features	King-Features	k1gMnSc1	King-Features
Syndicate	Syndicat	k1gInSc5	Syndicat
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
New	New	k1gMnSc1	New
Adam	Adam	k1gMnSc1	Adam
(	(	kIx(	(
<g/>
Ziff-Davis	Ziff-Davis	k1gInSc1	Ziff-Davis
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Black	Black	k1gInSc1	Black
Flame	Flam	k1gInSc5	Flam
(	(	kIx(	(
<g/>
Fantasy	fantas	k1gInPc4	fantas
Press	Press	k1gInSc1	Press
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Dark	Dark	k1gMnSc1	Dark
Other	Other	k1gMnSc1	Other
(	(	kIx(	(
<g/>
jiným	jiný	k2eAgInSc7d1	jiný
názvem	název	k1gInSc7	název
The	The	k1gMnSc1	The
Mad	Mad	k1gMnSc1	Mad
Brain	Brain	k1gMnSc1	Brain
<g/>
,	,	kIx,	,
Fantasy	fantas	k1gInPc1	fantas
Publishing	Publishing	k1gInSc1	Publishing
Company	Compana	k1gFnSc2	Compana
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Povídkové	povídkový	k2eAgFnSc2d1	povídková
a	a	k8xC	a
básnické	básnický	k2eAgFnSc2d1	básnická
sbírky	sbírka	k1gFnSc2	sbírka
===	===	k?	===
</s>
</p>
<p>
<s>
The	The	k?	The
Best	Best	k1gMnSc1	Best
of	of	k?	of
Stanley	Stanlea	k1gFnSc2	Stanlea
G.	G.	kA	G.
Weinbaum	Weinbaum	k1gInSc1	Weinbaum
<g/>
,	,	kIx,	,
Ballantine	Ballantin	k1gInSc5	Ballantin
Books	Booksa	k1gFnPc2	Booksa
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
</s>
</p>
<p>
<s>
Lunaria	Lunarium	k1gNnSc2	Lunarium
and	and	k?	and
Other	Other	k1gInSc1	Other
Poems	Poems	k1gInSc1	Poems
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Strange	Strang	k1gFnSc2	Strang
Publishing	Publishing	k1gInSc1	Publishing
Company	Compana	k1gFnSc2	Compana
1988	[number]	k4	1988
</s>
</p>
<p>
<s>
The	The	k?	The
Black	Black	k1gInSc1	Black
Heart	Heart	k1gInSc1	Heart
<g/>
,	,	kIx,	,
Leonaur	Leonaur	k1gMnSc1	Leonaur
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
</s>
</p>
<p>
<s>
Dawn	Dawn	k1gInSc1	Dawn
of	of	k?	of
Flame	Flam	k1gMnSc5	Flam
<g/>
:	:	kIx,	:
The	The	k1gMnSc6	The
Stanley	Stanlea	k1gFnSc2	Stanlea
G.	G.	kA	G.
Weinbaum	Weinbaum	k1gInSc1	Weinbaum
Memorial	Memorial	k1gInSc1	Memorial
Volume	volum	k1gInSc5	volum
<g/>
,	,	kIx,	,
Conrad	Conrad	k1gInSc1	Conrad
H.	H.	kA	H.
Ruppert	Ruppert	k1gInSc1	Ruppert
<g/>
,	,	kIx,	,
1936	[number]	k4	1936
</s>
</p>
<p>
<s>
Interplanetary	Interplanetara	k1gFnPc1	Interplanetara
Odysseys	Odysseysa	k1gFnPc2	Odysseysa
<g/>
,	,	kIx,	,
Leonaur	Leonaur	k1gMnSc1	Leonaur
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
</s>
</p>
<p>
<s>
A	a	k9	a
Martian	Martian	k1gMnSc1	Martian
Odyssey	Odyssea	k1gFnSc2	Odyssea
and	and	k?	and
Other	Other	k1gMnSc1	Other
Science	Science	k1gFnSc2	Science
Fiction	Fiction	k1gInSc1	Fiction
Tales	Tales	k1gInSc1	Tales
<g/>
,	,	kIx,	,
Hyperion	Hyperion	k1gInSc1	Hyperion
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
</s>
</p>
<p>
<s>
A	a	k9	a
Martian	Martian	k1gMnSc1	Martian
Odyssey	Odyssea	k1gFnSc2	Odyssea
and	and	k?	and
Others	Others	k1gInSc1	Others
<g/>
,	,	kIx,	,
Fantasy	fantas	k1gInPc1	fantas
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1949	[number]	k4	1949
</s>
</p>
<p>
<s>
A	a	k9	a
Martian	Martian	k1gMnSc1	Martian
Odyssey	Odyssea	k1gFnSc2	Odyssea
and	and	k?	and
Other	Other	k1gInSc1	Other
Classics	Classics	k1gInSc1	Classics
of	of	k?	of
Science	Science	k1gFnSc1	Science
Fiction	Fiction	k1gInSc1	Fiction
<g/>
,	,	kIx,	,
Lancer	Lancer	k1gInSc1	Lancer
Books	Booksa	k1gFnPc2	Booksa
<g/>
,	,	kIx,	,
1962	[number]	k4	1962
</s>
</p>
<p>
<s>
Other	Other	k1gInSc1	Other
Earths	Earths	k1gInSc1	Earths
<g/>
,	,	kIx,	,
Leonaur	Leonaur	k1gMnSc1	Leonaur
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
</s>
</p>
<p>
<s>
The	The	k?	The
Red	Red	k1gFnSc1	Red
Peri	peri	k1gFnSc1	peri
<g/>
,	,	kIx,	,
Fantasy	fantas	k1gInPc1	fantas
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1952	[number]	k4	1952
</s>
</p>
<p>
<s>
Strange	Strange	k6eAd1	Strange
Genius	genius	k1gMnSc1	genius
<g/>
,	,	kIx,	,
Leonaur	Leonaur	k1gMnSc1	Leonaur
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
</s>
</p>
<p>
<s>
===	===	k?	===
Povídky	povídka	k1gFnPc1	povídka
===	===	k?	===
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Odysea	odysea	k1gFnSc1	odysea
z	z	k7c2	z
Marsu	Mars	k1gInSc2	Mars
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
A	a	k9	a
Martian	Martian	k1gInSc1	Martian
Odyssey	Odyssea	k1gFnSc2	Odyssea
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
–	–	k?	–
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
čísle	číslo	k1gNnSc6	číslo
7	[number]	k4	7
<g/>
/	/	kIx~	/
<g/>
1934	[number]	k4	1934
Wonder	Wondero	k1gNnPc2	Wondero
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Valley	Valley	k1gInPc4	Valley
of	of	k?	of
Dreams	Dreams	k1gInSc1	Dreams
<g/>
"	"	kIx"	"
–	–	k?	–
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
čísle	číslo	k1gNnSc6	číslo
11	[number]	k4	11
<g/>
/	/	kIx~	/
<g/>
1934	[number]	k4	1934
Wonder	Wondra	k1gFnPc2	Wondra
–	–	k?	–
pokračování	pokračování	k1gNnSc4	pokračování
"	"	kIx"	"
<g/>
Odysey	odysea	k1gFnSc2	odysea
z	z	k7c2	z
Marsu	Mars	k1gInSc2	Mars
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Flight	Flight	k2eAgInSc1d1	Flight
on	on	k3xPp3gInSc1	on
Titan	titan	k1gInSc1	titan
<g/>
"	"	kIx"	"
–	–	k?	–
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
čísle	číslo	k1gNnSc6	číslo
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
1935	[number]	k4	1935
Astounding	Astounding	k1gInSc1	Astounding
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Parasite	parasit	k1gMnSc5	parasit
Planet	planeta	k1gFnPc2	planeta
<g/>
"	"	kIx"	"
–	–	k?	–
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
čísle	číslo	k1gNnSc6	číslo
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
1935	[number]	k4	1935
Astounding	Astounding	k1gInSc1	Astounding
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
The	The	k1gMnSc3	The
Lotus	Lotus	kA	Lotus
Eaters	Eaters	k1gInSc1	Eaters
<g/>
"	"	kIx"	"
–	–	k?	–
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
čísle	číslo	k1gNnSc6	číslo
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
1935	[number]	k4	1935
Astounding	Astounding	k1gInSc1	Astounding
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Pygmalion	Pygmalion	k1gInSc1	Pygmalion
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Spectacles	Spectaclesa	k1gFnPc2	Spectaclesa
<g/>
"	"	kIx"	"
–	–	k?	–
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
čísle	číslo	k1gNnSc6	číslo
6	[number]	k4	6
<g/>
/	/	kIx~	/
<g/>
1935	[number]	k4	1935
Wonder	Wondero	k1gNnPc2	Wondero
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
The	The	k1gMnSc2	The
Worlds	Worlds	k1gInSc1	Worlds
of	of	k?	of
If	If	k1gFnSc1	If
<g/>
"	"	kIx"	"
–	–	k?	–
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
čísle	číslo	k1gNnSc6	číslo
8	[number]	k4	8
<g/>
/	/	kIx~	/
<g/>
1935	[number]	k4	1935
Wonder	Wondero	k1gNnPc2	Wondero
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Challenge	Challeng	k1gFnSc2	Challeng
From	From	k1gMnSc1	From
Beyond	Beyond	k1gMnSc1	Beyond
<g/>
"	"	kIx"	"
–	–	k?	–
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
čísle	číslo	k1gNnSc6	číslo
9	[number]	k4	9
<g/>
/	/	kIx~	/
<g/>
1935	[number]	k4	1935
Fantasy	fantas	k1gInPc4	fantas
Magazine	Magazin	k1gInSc5	Magazin
(	(	kIx(	(
<g/>
Weinbaum	Weinbaum	k1gInSc1	Weinbaum
napsal	napsat	k5eAaBmAgInS	napsat
úvodních	úvodní	k2eAgNnPc6d1	úvodní
více	hodně	k6eAd2	hodně
než	než	k8xS	než
800	[number]	k4	800
slov	slovo	k1gNnPc2	slovo
této	tento	k3xDgFnSc2	tento
povídky	povídka	k1gFnSc2	povídka
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
podílelo	podílet	k5eAaImAgNnS	podílet
vícero	vícero	k1gNnSc1	vícero
autorů	autor	k1gMnPc2	autor
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Ideal	Ideal	k1gMnSc1	Ideal
<g/>
"	"	kIx"	"
–	–	k?	–
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
čísle	číslo	k1gNnSc6	číslo
9	[number]	k4	9
<g/>
/	/	kIx~	/
<g/>
35	[number]	k4	35
Wonder	Wondero	k1gNnPc2	Wondero
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
The	The	k1gFnSc6	The
Planet	planeta	k1gFnPc2	planeta
of	of	k?	of
Doubt	Doubta	k1gFnPc2	Doubta
<g/>
"	"	kIx"	"
–	–	k?	–
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
čísle	číslo	k1gNnSc6	číslo
10	[number]	k4	10
<g/>
/	/	kIx~	/
<g/>
1935	[number]	k4	1935
Astounding	Astounding	k1gInSc1	Astounding
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Adaptive	Adaptiv	k1gInSc5	Adaptiv
Ultimate	Ultimat	k1gInSc5	Ultimat
<g/>
"	"	kIx"	"
–	–	k?	–
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
čísle	číslo	k1gNnSc6	číslo
11	[number]	k4	11
<g/>
/	/	kIx~	/
<g/>
1935	[number]	k4	1935
Astounding	Astounding	k1gInSc1	Astounding
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
The	The	k1gMnPc4	The
Red	Red	k1gFnSc1	Red
Peri	peri	k1gFnSc2	peri
<g/>
"	"	kIx"	"
–	–	k?	–
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
čísle	číslo	k1gNnSc6	číslo
11	[number]	k4	11
<g/>
/	/	kIx~	/
<g/>
35	[number]	k4	35
Astounding	Astounding	k1gInSc1	Astounding
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Mad	Mad	k1gMnSc1	Mad
Moon	Moon	k1gMnSc1	Moon
<g/>
"	"	kIx"	"
–	–	k?	–
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
čísle	číslo	k1gNnSc6	číslo
12	[number]	k4	12
<g/>
/	/	kIx~	/
<g/>
35	[number]	k4	35
AstoundingPovídky	AstoundingPovídka	k1gFnSc2	AstoundingPovídka
vydané	vydaný	k2eAgFnPc1d1	vydaná
posmrtně	posmrtně	k6eAd1	posmrtně
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
The	The	k1gFnSc6	The
Point	pointa	k1gFnPc2	pointa
of	of	k?	of
View	View	k1gFnPc2	View
<g/>
"	"	kIx"	"
–	–	k?	–
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
čísle	číslo	k1gNnSc6	číslo
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
36	[number]	k4	36
Wonder	Wondero	k1gNnPc2	Wondero
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Smothered	Smothered	k1gInSc1	Smothered
Seas	Seas	k1gInSc1	Seas
<g/>
"	"	kIx"	"
–	–	k?	–
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
čísle	číslo	k1gNnSc6	číslo
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
36	[number]	k4	36
Astounding	Astounding	k1gInSc1	Astounding
(	(	kIx(	(
<g/>
napsáno	napsat	k5eAaPmNgNnS	napsat
společně	společně	k6eAd1	společně
s	s	k7c7	s
Rogerem	Roger	k1gMnSc7	Roger
Shermanem	Sherman	k1gMnSc7	Sherman
Hoarem	Hoar	k1gMnSc7	Hoar
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
používal	používat	k5eAaImAgMnS	používat
pseudonym	pseudonym	k1gInSc4	pseudonym
Ralph	Ralph	k1gMnSc1	Ralph
Milne	Miln	k1gInSc5	Miln
Farley	Farley	k1gInPc1	Farley
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Yellow	Yellow	k1gMnSc1	Yellow
Slaves	Slaves	k1gMnSc1	Slaves
<g/>
"	"	kIx"	"
–	–	k?	–
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
čísle	číslo	k1gNnSc6	číslo
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
36	[number]	k4	36
True	True	k1gNnPc2	True
Gang	Ganga	k1gFnPc2	Ganga
Life	Life	k1gFnPc2	Life
(	(	kIx(	(
<g/>
napsáno	napsat	k5eAaBmNgNnS	napsat
společně	společně	k6eAd1	společně
s	s	k7c7	s
Rogerem	Roger	k1gMnSc7	Roger
Shermanem	Sherman	k1gMnSc7	Sherman
Hoarem	Hoar	k1gMnSc7	Hoar
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
používal	používat	k5eAaImAgMnS	používat
pseudonym	pseudonym	k1gInSc4	pseudonym
Ralph	Ralph	k1gMnSc1	Ralph
Milne	Miln	k1gInSc5	Miln
Farley	Farle	k2eAgFnPc1d1	Farle
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Redemption	Redemption	k1gInSc1	Redemption
Cairn	Cairn	k1gNnSc1	Cairn
<g/>
"	"	kIx"	"
–	–	k?	–
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
čísle	číslo	k1gNnSc6	číslo
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
36	[number]	k4	36
Astounding	Astounding	k1gInSc1	Astounding
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
The	The	k1gFnSc3	The
Circle	Circle	k1gNnSc2	Circle
of	of	k?	of
Zero	Zero	k1gMnSc1	Zero
<g/>
"	"	kIx"	"
–	–	k?	–
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
čísle	číslo	k1gNnSc6	číslo
8	[number]	k4	8
<g/>
/	/	kIx~	/
<g/>
36	[number]	k4	36
Thrilling	Thrilling	k1gInSc1	Thrilling
Wonder	Wonder	k1gInSc4	Wonder
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Proteus	Proteus	k1gMnSc1	Proteus
Island	Island	k1gInSc1	Island
<g/>
"	"	kIx"	"
–	–	k?	–
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
čísle	číslo	k1gNnSc6	číslo
8	[number]	k4	8
<g/>
/	/	kIx~	/
<g/>
36	[number]	k4	36
Astounding	Astounding	k1gInSc1	Astounding
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Graph	Grapha	k1gFnPc2	Grapha
<g/>
"	"	kIx"	"
–	–	k?	–
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
čísle	číslo	k1gNnSc6	číslo
9	[number]	k4	9
<g/>
/	/	kIx~	/
<g/>
36	[number]	k4	36
Fantasy	fantas	k1gInPc4	fantas
Magazine	Magazin	k1gInSc5	Magazin
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
The	The	k1gFnPc2	The
Brink	Brinko	k1gNnPc2	Brinko
of	of	k?	of
Infinity	Infinita	k1gFnSc2	Infinita
<g/>
"	"	kIx"	"
–	–	k?	–
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
čísle	číslo	k1gNnSc6	číslo
12	[number]	k4	12
<g/>
/	/	kIx~	/
<g/>
36	[number]	k4	36
Thrilling	Thrilling	k1gInSc1	Thrilling
Wonder	Wonder	k1gInSc4	Wonder
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Shifting	Shifting	k1gInSc1	Shifting
Seas	Seas	k1gInSc1	Seas
<g/>
"	"	kIx"	"
–	–	k?	–
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
čísle	číslo	k1gNnSc6	číslo
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
37	[number]	k4	37
Amazing	Amazing	k1gInSc1	Amazing
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Revolution	Revolution	k1gInSc1	Revolution
of	of	k?	of
1950	[number]	k4	1950
<g/>
"	"	kIx"	"
–	–	k?	–
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
čísle	číslo	k1gNnSc6	číslo
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
/	/	kIx~	/
<g/>
38	[number]	k4	38
Amazing	Amazing	k1gInSc1	Amazing
(	(	kIx(	(
<g/>
napsáno	napsat	k5eAaBmNgNnS	napsat
společně	společně	k6eAd1	společně
s	s	k7c7	s
Rogerem	Roger	k1gMnSc7	Roger
Shermanem	Sherman	k1gMnSc7	Sherman
Hoarem	Hoar	k1gMnSc7	Hoar
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
používal	používat	k5eAaImAgMnS	používat
pseudonym	pseudonym	k1gInSc4	pseudonym
Ralph	Ralph	k1gMnSc1	Ralph
Milne	Miln	k1gInSc5	Miln
Farley	Farlea	k1gFnSc2	Farlea
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Tidal	Tidal	k1gMnSc1	Tidal
Moon	Moon	k1gMnSc1	Moon
<g/>
"	"	kIx"	"
–	–	k?	–
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
čísle	číslo	k1gNnSc6	číslo
12	[number]	k4	12
<g/>
/	/	kIx~	/
<g/>
38	[number]	k4	38
Thrilling	Thrilling	k1gInSc1	Thrilling
Wonder	Wonder	k1gInSc4	Wonder
(	(	kIx(	(
<g/>
napsáno	napsat	k5eAaBmNgNnS	napsat
společně	společně	k6eAd1	společně
s	s	k7c7	s
Helen	Helena	k1gFnPc2	Helena
Weinbaumovou	Weinbaumový	k2eAgFnSc7d1	Weinbaumový
<g/>
,	,	kIx,	,
autorovou	autorův	k2eAgFnSc7d1	autorova
sestrou	sestra	k1gFnSc7	sestra
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Black	Black	k1gMnSc1	Black
Flame	Flam	k1gInSc5	Flam
<g/>
"	"	kIx"	"
–	–	k?	–
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
čísle	číslo	k1gNnSc6	číslo
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
39	[number]	k4	39
Startling	Startling	k1gInSc1	Startling
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Dawn	Dawn	k1gInSc1	Dawn
of	of	k?	of
Flame	Flam	k1gInSc5	Flam
<g/>
"	"	kIx"	"
–	–	k?	–
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
čísle	číslo	k1gNnSc6	číslo
6	[number]	k4	6
<g/>
/	/	kIx~	/
<g/>
39	[number]	k4	39
Thrilling	Thrilling	k1gInSc1	Thrilling
Wonder	Wonder	k1gInSc4	Wonder
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Green	Green	k2eAgMnSc1d1	Green
Glow	Glow	k1gMnSc1	Glow
of	of	k?	of
Death	Death	k1gMnSc1	Death
<g/>
"	"	kIx"	"
–	–	k?	–
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
čísle	číslo	k1gNnSc6	číslo
7	[number]	k4	7
<g/>
/	/	kIx~	/
<g/>
57	[number]	k4	57
Crack	Cracko	k1gNnPc2	Cracko
Detective	Detectiv	k1gInSc5	Detectiv
and	and	k?	and
Mystery	Myster	k1gInPc1	Myster
Stories	Stories	k1gInSc1	Stories
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
King	King	k1gMnSc1	King
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Watch	Watcha	k1gFnPc2	Watcha
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Posthumous	Posthumous	k1gInSc4	Posthumous
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
(	(	kIx(	(
<g/>
varianta	varianta	k1gFnSc1	varianta
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Green	Grena	k1gFnPc2	Grena
Glow	Glow	k1gMnSc1	Glow
of	of	k?	of
Death	Death	k1gMnSc1	Death
<g/>
"	"	kIx"	"
z	z	k7c2	z
7	[number]	k4	7
<g/>
/	/	kIx~	/
<g/>
57	[number]	k4	57
Crack	Cracko	k1gNnPc2	Cracko
Detective	Detectiv	k1gInSc5	Detectiv
and	and	k?	and
Mystery	Myster	k1gInPc1	Myster
Stories	Stories	k1gInSc1	Stories
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Stanley	Stanlea	k1gMnSc2	Stanlea
G.	G.	kA	G.
Weinbaum	Weinbaum	k1gInSc1	Weinbaum
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Stanley	Stanley	k1gInPc1	Stanley
G.	G.	kA	G.
Weinbaum	Weinbaum	k1gNnSc4	Weinbaum
na	na	k7c6	na
Databázi	databáze	k1gFnSc6	databáze
knih	kniha	k1gFnPc2	kniha
</s>
</p>
<p>
<s>
Stanley	Stanley	k1gInPc1	Stanley
G.	G.	kA	G.
Weinbaum	Weinbaum	k1gNnSc4	Weinbaum
na	na	k7c6	na
sci-fi	scii	k1gFnSc6	sci-fi
databázi	databáze	k1gFnSc6	databáze
LEGIE	legie	k1gFnSc2	legie
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Stanley	Stanlea	k1gFnSc2	Stanlea
G.	G.	kA	G.
Weinbaum	Weinbaum	k1gInSc1	Weinbaum
na	na	k7c4	na
sci-fi	scii	k1gFnSc4	sci-fi
databázi	databáze	k1gFnSc4	databáze
Isfdb	Isfdba	k1gFnPc2	Isfdba
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
</p>
