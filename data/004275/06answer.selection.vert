<s>
Skřivánci	Skřivánek	k1gMnPc1	Skřivánek
na	na	k7c6	na
niti	nit	k1gFnSc6	nit
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
hořce	hořko	k6eAd1	hořko
poetický	poetický	k2eAgInSc1d1	poetický
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Jiřího	Jiří	k1gMnSc2	Jiří
Menzela	Menzela	k1gMnSc2	Menzela
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
natočený	natočený	k2eAgInSc1d1	natočený
podle	podle	k7c2	podle
knihy	kniha	k1gFnSc2	kniha
Bohumila	Bohumila	k1gFnSc1	Bohumila
Hrabala	hrabat	k5eAaImAgFnS	hrabat
Inzerát	inzerát	k1gInSc4	inzerát
na	na	k7c4	na
dům	dům	k1gInSc4	dům
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
už	už	k6eAd1	už
nechci	chtít	k5eNaImIp1nS	chtít
bydlet	bydlet	k5eAaImF	bydlet
<g/>
.	.	kIx.	.
</s>
