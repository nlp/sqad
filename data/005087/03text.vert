<s>
Microsoft	Microsoft	kA	Microsoft
Corporation	Corporation	k1gInSc1	Corporation
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
akciová	akciový	k2eAgFnSc1d1	akciová
nadnárodní	nadnárodní	k2eAgFnSc1d1	nadnárodní
společnost	společnost	k1gFnSc1	společnost
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Redmondu	Redmond	k1gInSc6	Redmond
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Washington	Washington	k1gInSc1	Washington
<g/>
.	.	kIx.	.
</s>
<s>
Zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
vývojem	vývoj	k1gInSc7	vývoj
<g/>
,	,	kIx,	,
výrobou	výroba	k1gFnSc7	výroba
<g/>
,	,	kIx,	,
licencováním	licencování	k1gNnSc7	licencování
a	a	k8xC	a
podporou	podpora	k1gFnSc7	podpora
široké	široký	k2eAgFnSc2d1	široká
škály	škála	k1gFnSc2	škála
produktů	produkt	k1gInPc2	produkt
a	a	k8xC	a
služeb	služba	k1gFnPc2	služba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
spjaté	spjatý	k2eAgInPc1d1	spjatý
především	především	k9	především
s	s	k7c7	s
počítači	počítač	k1gInPc7	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1975	[number]	k4	1975
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
vývoje	vývoj	k1gInSc2	vývoj
a	a	k8xC	a
prodeje	prodej	k1gInSc2	prodej
interpretů	interpret	k1gMnPc2	interpret
BASIC	Basic	kA	Basic
pro	pro	k7c4	pro
Altair	Altair	k1gInSc4	Altair
8800	[number]	k4	8800
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
ale	ale	k8xC	ale
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
dominovat	dominovat	k5eAaImF	dominovat
trhu	trh	k1gInSc6	trh
s	s	k7c7	s
operačními	operační	k2eAgInPc7d1	operační
systémy	systém	k1gInPc7	systém
pro	pro	k7c4	pro
domácí	domácí	k2eAgInPc4d1	domácí
počítače	počítač	k1gInPc4	počítač
se	s	k7c7	s
systémem	systém	k1gInSc7	systém
MS-DOS	MS-DOS	k1gFnSc2	MS-DOS
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
následovala	následovat	k5eAaImAgFnS	následovat
série	série	k1gFnSc1	série
operačních	operační	k2eAgInPc2d1	operační
systému	systém	k1gInSc2	systém
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
<g/>
.	.	kIx.	.
</s>
<s>
Časem	časem	k6eAd1	časem
Microsoft	Microsoft	kA	Microsoft
převzal	převzít	k5eAaPmAgInS	převzít
i	i	k9	i
vedení	vedení	k1gNnSc4	vedení
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
s	s	k7c7	s
kancelářskými	kancelářský	k2eAgInPc7d1	kancelářský
programy	program	k1gInPc7	program
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mu	on	k3xPp3gMnSc3	on
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
pomohl	pomoct	k5eAaPmAgMnS	pomoct
Microsoft	Microsoft	kA	Microsoft
Office	Office	kA	Office
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
se	se	k3xPyFc4	se
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
zaměřila	zaměřit	k5eAaPmAgFnS	zaměřit
také	také	k9	také
na	na	k7c4	na
herní	herní	k2eAgInSc4d1	herní
průmysl	průmysl	k1gInSc4	průmysl
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
jejími	její	k3xOp3gInPc7	její
nejvýznamnějšími	významný	k2eAgInPc7d3	nejvýznamnější
produkty	produkt	k1gInPc7	produkt
Xbox	Xbox	k1gInSc1	Xbox
<g/>
,	,	kIx,	,
Xbox	Xbox	k1gInSc1	Xbox
360	[number]	k4	360
a	a	k8xC	a
Xbox	Xbox	k1gInSc1	Xbox
One	One	k1gFnSc2	One
<g/>
;	;	kIx,	;
na	na	k7c4	na
spotřební	spotřební	k2eAgFnSc4d1	spotřební
elektroniku	elektronika	k1gFnSc4	elektronika
a	a	k8xC	a
digitální	digitální	k2eAgFnPc1d1	digitální
služby	služba	k1gFnPc1	služba
se	s	k7c7	s
Zune	Zune	k1gFnPc7	Zune
<g/>
,	,	kIx,	,
MSN	MSN	kA	MSN
a	a	k8xC	a
Windows	Windows	kA	Windows
Phone	Phon	k1gMnSc5	Phon
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
vzrůst	vzrůst	k1gInSc4	vzrůst
akcií	akcie	k1gFnPc2	akcie
z	z	k7c2	z
initial	initiat	k5eAaImAgInS	initiat
public	publicum	k1gNnPc2	publicum
offeringu	offering	k1gInSc3	offering
udělal	udělat	k5eAaPmAgMnS	udělat
ze	z	k7c2	z
čtyř	čtyři	k4xCgMnPc2	čtyři
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
Microsoftu	Microsofta	k1gMnSc4	Microsofta
miliardáře	miliardář	k1gMnSc4	miliardář
a	a	k8xC	a
asi	asi	k9	asi
z	z	k7c2	z
dvanácti	dvanáct	k4xCc2	dvanáct
tisíc	tisíc	k4xCgInPc2	tisíc
milionáře	milionář	k1gMnSc2	milionář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2011	[number]	k4	2011
Microsoft	Microsoft	kA	Microsoft
zakoupil	zakoupit	k5eAaPmAgInS	zakoupit
za	za	k7c4	za
8,5	[number]	k4	8,5
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
společnost	společnost	k1gFnSc1	společnost
Skype	Skyp	k1gInSc5	Skyp
Communications	Communications	k1gInSc4	Communications
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
v	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
společnost	společnost	k1gFnSc1	společnost
kritizována	kritizovat	k5eAaImNgFnS	kritizovat
za	za	k7c4	za
užívání	užívání	k1gNnSc4	užívání
monopolistických	monopolistický	k2eAgInPc2d1	monopolistický
obchodních	obchodní	k2eAgInPc2d1	obchodní
postupů	postup	k1gInPc2	postup
a	a	k8xC	a
proti-konkurenčních	protionkurenční	k2eAgFnPc2d1	proti-konkurenční
strategií	strategie	k1gFnPc2	strategie
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
skupinový	skupinový	k2eAgInSc4d1	skupinový
bojkot	bojkot	k1gInSc4	bojkot
<g/>
,	,	kIx,	,
vázání	vázání	k1gNnSc4	vázání
produktů	produkt	k1gInPc2	produkt
<g/>
,	,	kIx,	,
neodůvodněné	odůvodněný	k2eNgNnSc1d1	neodůvodněné
omezování	omezování	k1gNnSc1	omezování
užívání	užívání	k1gNnSc2	užívání
softwaru	software	k1gInSc2	software
nebo	nebo	k8xC	nebo
používání	používání	k1gNnSc2	používání
klamných	klamný	k2eAgFnPc2d1	klamná
marketingových	marketingový	k2eAgFnPc2d1	marketingová
taktik	taktika	k1gFnPc2	taktika
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xC	jak
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
Evropská	evropský	k2eAgFnSc1d1	Evropská
komise	komise	k1gFnSc1	komise
shledaly	shledat	k5eAaPmAgFnP	shledat
společnost	společnost	k1gFnSc4	společnost
vinnou	vinný	k2eAgFnSc4d1	vinná
z	z	k7c2	z
porušení	porušení	k1gNnSc2	porušení
antimonopolních	antimonopolní	k2eAgInPc2d1	antimonopolní
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Paul	Paul	k1gMnSc1	Paul
Allen	Allen	k1gMnSc1	Allen
a	a	k8xC	a
Bill	Bill	k1gMnSc1	Bill
Gates	Gates	k1gMnSc1	Gates
<g/>
,	,	kIx,	,
přátele	přítel	k1gMnPc4	přítel
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
s	s	k7c7	s
vášní	vášeň	k1gFnSc7	vášeň
pro	pro	k7c4	pro
programování	programování	k1gNnSc4	programování
<g/>
,	,	kIx,	,
hledali	hledat	k5eAaImAgMnP	hledat
v	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
příležitost	příležitost	k1gFnSc4	příležitost
k	k	k7c3	k
založení	založení	k1gNnSc3	založení
úspěšného	úspěšný	k2eAgInSc2d1	úspěšný
podniku	podnik	k1gInSc2	podnik
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
by	by	kYmCp3nP	by
využili	využít	k5eAaPmAgMnP	využít
své	svůj	k3xOyFgFnPc4	svůj
společné	společný	k2eAgFnPc4d1	společná
dovednosti	dovednost	k1gFnPc4	dovednost
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Časopis	časopis	k1gInSc1	časopis
Popular	Popular	k1gInSc1	Popular
Electronics	Electronics	k1gInSc1	Electronics
ve	v	k7c6	v
vydání	vydání	k1gNnSc6	vydání
z	z	k7c2	z
ledna	leden	k1gInSc2	leden
1975	[number]	k4	1975
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
recenzi	recenze	k1gFnSc4	recenze
na	na	k7c4	na
mikropočítač	mikropočítač	k1gInSc4	mikropočítač
Altair	Altair	k1gInSc4	Altair
8800	[number]	k4	8800
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
MITS	MITS	kA	MITS
<g/>
.	.	kIx.	.
</s>
<s>
Allen	Allen	k1gMnSc1	Allen
zamýšlel	zamýšlet	k5eAaImAgMnS	zamýšlet
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
mohli	moct	k5eAaImAgMnP	moct
pro	pro	k7c4	pro
mikropočítač	mikropočítač	k1gInSc4	mikropočítač
naprogramovat	naprogramovat	k5eAaPmF	naprogramovat
interpreter	interpreter	k1gInSc4	interpreter
<g/>
,	,	kIx,	,
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
to	ten	k3xDgNnSc4	ten
této	tento	k3xDgFnSc2	tento
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
MITS	MITS	kA	MITS
si	se	k3xPyFc3	se
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
ukázku	ukázka	k1gFnSc4	ukázka
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
žádnou	žádný	k3yNgFnSc4	žádný
ukázku	ukázka	k1gFnSc4	ukázka
ještě	ještě	k6eAd1	ještě
neměli	mít	k5eNaImAgMnP	mít
<g/>
,	,	kIx,	,
Allen	allen	k1gInSc4	allen
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
simulátoru	simulátor	k1gInSc6	simulátor
pro	pro	k7c4	pro
Altair	Altair	k1gInSc4	Altair
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Gates	Gates	k1gMnSc1	Gates
vyvíjel	vyvíjet	k5eAaImAgMnS	vyvíjet
interpreter	interpreter	k1gInSc4	interpreter
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1975	[number]	k4	1975
v	v	k7c6	v
Albuquerque	Albuquerque	k1gFnSc6	Albuquerque
<g/>
,	,	kIx,	,
ukázali	ukázat	k5eAaPmAgMnP	ukázat
zástupcům	zástupce	k1gMnPc3	zástupce
společnosti	společnost	k1gFnSc2	společnost
MITS	MITS	kA	MITS
ukázku	ukázka	k1gFnSc4	ukázka
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
simulátoru	simulátor	k1gInSc6	simulátor
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
interpreter	interpreter	k1gInSc1	interpreter
pracoval	pracovat	k5eAaImAgInS	pracovat
bezchybně	bezchybně	k6eAd1	bezchybně
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
MITS	MITS	kA	MITS
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
distribuovat	distribuovat	k5eAaBmF	distribuovat
ho	on	k3xPp3gNnSc4	on
a	a	k8xC	a
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Altair	Altaira	k1gFnPc2	Altaira
BASIC	Basic	kA	Basic
<g/>
.	.	kIx.	.
</s>
<s>
Microsoft	Microsoft	kA	Microsoft
byl	být	k5eAaImAgInS	být
oficiálně	oficiálně	k6eAd1	oficiálně
založen	založit	k5eAaPmNgInS	založit
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1975	[number]	k4	1975
s	s	k7c7	s
Billem	Bill	k1gMnSc7	Bill
Gatesem	Gates	k1gMnSc7	Gates
jako	jako	k8xS	jako
ředitelem	ředitel	k1gMnSc7	ředitel
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
CEO	CEO	kA	CEO
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
článku	článek	k1gInSc2	článek
časopisu	časopis	k1gInSc2	časopis
Fortune	Fortun	k1gInSc5	Fortun
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
jméno	jméno	k1gNnSc1	jméno
Micro-Soft	Micro-Softa	k1gFnPc2	Micro-Softa
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
Allen	Allen	k1gMnSc1	Allen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1977	[number]	k4	1977
společnost	společnost	k1gFnSc1	společnost
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
dohodu	dohoda	k1gFnSc4	dohoda
s	s	k7c7	s
japonským	japonský	k2eAgInSc7d1	japonský
časopisem	časopis	k1gInSc7	časopis
ASCII	ascii	kA	ascii
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
založila	založit	k5eAaPmAgFnS	založit
první	první	k4xOgFnSc4	první
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
pobočku	pobočka	k1gFnSc4	pobočka
<g/>
,	,	kIx,	,
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
ASCII	ascii	kA	ascii
Microsoft	Microsoft	kA	Microsoft
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1979	[number]	k4	1979
se	se	k3xPyFc4	se
společnost	společnost	k1gFnSc1	společnost
Microsoft	Microsoft	kA	Microsoft
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
washingtonského	washingtonský	k2eAgInSc2d1	washingtonský
Bellevue	bellevue	k1gFnPc7	bellevue
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
Microsoft	Microsoft	kA	Microsoft
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
na	na	k7c4	na
trh	trh	k1gInSc4	trh
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
vlastní	vlastní	k2eAgFnSc7d1	vlastní
verzí	verze	k1gFnSc7	verze
Unixu	Unix	k1gInSc2	Unix
<g/>
,	,	kIx,	,
zvanou	zvaný	k2eAgFnSc7d1	zvaná
Xenix	Xenix	k1gInSc4	Xenix
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
až	až	k9	až
systém	systém	k1gInSc1	systém
DOS	DOS	kA	DOS
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dostal	dostat	k5eAaPmAgMnS	dostat
společnost	společnost	k1gFnSc4	společnost
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
softwarového	softwarový	k2eAgInSc2d1	softwarový
trhu	trh	k1gInSc2	trh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1980	[number]	k4	1980
IBM	IBM	kA	IBM
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
musela	muset	k5eAaImAgFnS	muset
skousnout	skousnout	k5eAaPmF	skousnout
neúspěšné	úspěšný	k2eNgNnSc4d1	neúspěšné
vyjednávání	vyjednávání	k1gNnSc4	vyjednávání
se	s	k7c7	s
společností	společnost	k1gFnSc7	společnost
Digital	Digital	kA	Digital
Research	Research	k1gMnSc1	Research
<g/>
,	,	kIx,	,
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
Microsoftu	Microsoft	k2eAgFnSc4d1	Microsoft
smlouvu	smlouva	k1gFnSc4	smlouva
na	na	k7c4	na
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
CP	CP	kA	CP
<g/>
/	/	kIx~	/
<g/>
M	M	kA	M
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
chtěla	chtít	k5eAaImAgFnS	chtít
použít	použít	k5eAaPmF	použít
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
počítači	počítač	k1gInSc6	počítač
IBM	IBM	kA	IBM
Personal	Personal	k1gMnSc1	Personal
Computer	computer	k1gInSc1	computer
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
Microsoft	Microsoft	kA	Microsoft
koupil	koupit	k5eAaPmAgInS	koupit
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
Seattle	Seattle	k1gFnSc2	Seattle
Computer	computer	k1gInSc1	computer
Products	Productsa	k1gFnPc2	Productsa
klon	klon	k1gInSc1	klon
systému	systém	k1gInSc2	systém
CP	CP	kA	CP
<g/>
/	/	kIx~	/
<g/>
M	M	kA	M
<g/>
,	,	kIx,	,
86	[number]	k4	86
<g/>
-DOS	-DOS	k?	-DOS
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
přejmenoval	přejmenovat	k5eAaPmAgMnS	přejmenovat
na	na	k7c6	na
MS-DOS	MS-DOS	k1gFnSc6	MS-DOS
a	a	k8xC	a
IBM	IBM	kA	IBM
ho	on	k3xPp3gInSc4	on
později	pozdě	k6eAd2	pozdě
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
na	na	k7c4	na
PC	PC	kA	PC
DOS	DOS	kA	DOS
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
uvedení	uvedení	k1gNnSc6	uvedení
počítače	počítač	k1gInSc2	počítač
IBM	IBM	kA	IBM
PC	PC	kA	PC
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1981	[number]	k4	1981
na	na	k7c4	na
trh	trh	k1gInSc4	trh
si	se	k3xPyFc3	se
Microsoft	Microsoft	kA	Microsoft
ponechal	ponechat	k5eAaPmAgMnS	ponechat
vlastnictví	vlastnictví	k1gNnSc4	vlastnictví
systému	systém	k1gInSc2	systém
MS-DOS	MS-DOS	k1gFnSc2	MS-DOS
a	a	k8xC	a
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
Microsoft	Microsoft	kA	Microsoft
stal	stát	k5eAaPmAgMnS	stát
vůdcem	vůdce	k1gMnSc7	vůdce
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
počítačovými	počítačový	k2eAgInPc7d1	počítačový
operačními	operační	k2eAgInPc7d1	operační
systémy	systém	k1gInPc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
společnost	společnost	k1gFnSc1	společnost
svůj	svůj	k3xOyFgInSc4	svůj
vliv	vliv	k1gInSc4	vliv
dál	daleko	k6eAd2	daleko
díky	díky	k7c3	díky
uvedení	uvedení	k1gNnSc3	uvedení
na	na	k7c4	na
trh	trh	k1gInSc4	trh
počítačové	počítačový	k2eAgFnSc2d1	počítačová
myši	myš	k1gFnSc2	myš
Microsoft	Microsoft	kA	Microsoft
Mouse	Mouse	k1gFnSc2	Mouse
a	a	k8xC	a
založení	založení	k1gNnSc2	založení
nakladatelské	nakladatelský	k2eAgFnSc2d1	nakladatelská
divize	divize	k1gFnSc2	divize
Microsoft	Microsoft	kA	Microsoft
Press	Press	k1gInSc1	Press
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1983	[number]	k4	1983
Allen	Allen	k1gMnSc1	Allen
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
odešel	odejít	k5eAaPmAgMnS	odejít
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
Hodkingovu	Hodkingův	k2eAgFnSc4d1	Hodkingův
nemoc	nemoc	k1gFnSc4	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
společném	společný	k2eAgInSc6d1	společný
vývoji	vývoj	k1gInSc6	vývoj
nového	nový	k2eAgInSc2d1	nový
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
OS	OS	kA	OS
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
s	s	k7c7	s
IBM	IBM	kA	IBM
<g/>
,	,	kIx,	,
Microsoft	Microsoft	kA	Microsoft
vydal	vydat	k5eAaPmAgMnS	vydat
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
<g/>
,	,	kIx,	,
grafickou	grafický	k2eAgFnSc4d1	grafická
extenzi	extenze	k1gFnSc4	extenze
pro	pro	k7c4	pro
MS-DOS	MS-DOS	k1gFnSc4	MS-DOS
<g/>
,	,	kIx,	,
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1986	[number]	k4	1986
společnost	společnost	k1gFnSc1	společnost
přemístila	přemístit	k5eAaPmAgFnS	přemístit
své	svůj	k3xOyFgNnSc4	svůj
sídlo	sídlo	k1gNnSc4	sídlo
do	do	k7c2	do
nedalekého	daleký	k2eNgInSc2d1	nedaleký
Redmondu	Redmond	k1gInSc2	Redmond
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
stala	stát	k5eAaPmAgFnS	stát
akciovou	akciový	k2eAgFnSc7d1	akciová
společností	společnost	k1gFnSc7	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
vzrůst	vzrůst	k1gInSc4	vzrůst
akcií	akcie	k1gFnPc2	akcie
z	z	k7c2	z
uvedení	uvedení	k1gNnSc2	uvedení
akcií	akcie	k1gFnPc2	akcie
na	na	k7c4	na
burzu	burza	k1gFnSc4	burza
(	(	kIx(	(
<g/>
initial	initial	k1gInSc1	initial
public	publicum	k1gNnPc2	publicum
offering	offering	k1gInSc1	offering
<g/>
)	)	kIx)	)
udělal	udělat	k5eAaPmAgInS	udělat
ze	z	k7c2	z
čtyř	čtyři	k4xCgMnPc2	čtyři
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
Microsoftu	Microsofta	k1gMnSc4	Microsofta
miliardáře	miliardář	k1gMnSc4	miliardář
a	a	k8xC	a
asi	asi	k9	asi
z	z	k7c2	z
dvanácti	dvanáct	k4xCc2	dvanáct
tisíc	tisíc	k4xCgInPc2	tisíc
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
milionáře	milionář	k1gMnSc2	milionář
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byli	být	k5eAaImAgMnP	být
odměňováni	odměňovat	k5eAaImNgMnP	odměňovat
částečně	částečně	k6eAd1	částečně
akciemi	akcie	k1gFnPc7	akcie
firmy	firma	k1gFnSc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
partnerství	partnerství	k1gNnSc3	partnerství
s	s	k7c7	s
IBM	IBM	kA	IBM
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
začala	začít	k5eAaPmAgFnS	začít
Federální	federální	k2eAgFnSc1d1	federální
obchodní	obchodní	k2eAgFnSc1d1	obchodní
komise	komise	k1gFnSc1	komise
zaměřovat	zaměřovat	k5eAaImF	zaměřovat
na	na	k7c6	na
Microsoft	Microsoft	kA	Microsoft
kvůli	kvůli	k7c3	kvůli
možné	možný	k2eAgFnSc3d1	možná
tajné	tajný	k2eAgFnSc3d1	tajná
dohodě	dohoda	k1gFnSc3	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
začalo	začít	k5eAaPmAgNnS	začít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
deset	deset	k4xCc1	deset
let	léto	k1gNnPc2	léto
právních	právní	k2eAgInPc2d1	právní
sporů	spor	k1gInPc2	spor
s	s	k7c7	s
americkou	americký	k2eAgFnSc7d1	americká
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1987	[number]	k4	1987
Microsoft	Microsoft	kA	Microsoft
vydal	vydat	k5eAaPmAgMnS	vydat
systém	systém	k1gInSc4	systém
OS	OS	kA	OS
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
OEM	OEM	kA	OEM
výrobcům	výrobce	k1gMnPc3	výrobce
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
práci	práce	k1gFnSc6	práce
na	na	k7c6	na
32	[number]	k4	32
<g/>
bitovém	bitový	k2eAgInSc6d1	bitový
systému	systém	k1gInSc6	systém
Windows	Windows	kA	Windows
NT	NT	kA	NT
(	(	kIx(	(
<g/>
New	New	k1gMnSc1	New
Technology	technolog	k1gMnPc7	technolog
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
použil	použít	k5eAaPmAgMnS	použít
některé	některý	k3yIgFnPc4	některý
funkce	funkce	k1gFnPc4	funkce
z	z	k7c2	z
OS	OS	kA	OS
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
a	a	k8xC	a
na	na	k7c4	na
trh	trh	k1gInSc4	trh
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1993	[number]	k4	1993
s	s	k7c7	s
novým	nový	k2eAgNnSc7d1	nové
modulárním	modulární	k2eAgNnSc7d1	modulární
jádrem	jádro	k1gNnSc7	jádro
(	(	kIx(	(
<g/>
kernelem	kernel	k1gInSc7	kernel
<g/>
)	)	kIx)	)
a	a	k8xC	a
s	s	k7c7	s
uživatelským	uživatelský	k2eAgNnSc7d1	Uživatelské
rozhraním	rozhraní	k1gNnSc7	rozhraní
Windows	Windows	kA	Windows
API	API	kA	API
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
převézt	převézt	k5eAaPmF	převézt
programy	program	k1gInPc4	program
z	z	k7c2	z
16	[number]	k4	16
<g/>
bitových	bitový	k2eAgInPc2d1	bitový
systémů	systém	k1gInPc2	systém
na	na	k7c4	na
32	[number]	k4	32
<g/>
bitové	bitový	k2eAgInPc1d1	bitový
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
Microsoft	Microsoft	kA	Microsoft
informoval	informovat	k5eAaBmAgInS	informovat
IBM	IBM	kA	IBM
o	o	k7c6	o
novém	nový	k2eAgInSc6d1	nový
systému	systém	k1gInSc6	systém
<g/>
,	,	kIx,	,
partnerství	partnerství	k1gNnSc4	partnerství
mezi	mezi	k7c7	mezi
firmami	firma	k1gFnPc7	firma
začalo	začít	k5eAaPmAgNnS	začít
upadat	upadat	k5eAaPmF	upadat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
uvedl	uvést	k5eAaPmAgMnS	uvést
na	na	k7c4	na
trh	trh	k1gInSc4	trh
Microsoft	Microsoft	kA	Microsoft
svůj	svůj	k3xOyFgInSc4	svůj
kancelářský	kancelářský	k2eAgInSc4d1	kancelářský
balík	balík	k1gInSc4	balík
programů	program	k1gInPc2	program
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc1d1	zvaný
Microsoft	Microsoft	kA	Microsoft
Office	Office	kA	Office
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yRgInSc2	který
patřily	patřit	k5eAaImAgInP	patřit
programy	program	k1gInPc1	program
pro	pro	k7c4	pro
několik	několik	k4yIc4	několik
různých	různý	k2eAgFnPc2d1	různá
kancelářských	kancelářský	k2eAgFnPc2d1	kancelářská
aktivit	aktivita	k1gFnPc2	aktivita
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Microsoft	Microsoft	kA	Microsoft
Word	Word	kA	Word
nebo	nebo	k8xC	nebo
Microsoft	Microsoft	kA	Microsoft
Excel	Excel	kA	Excel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1990	[number]	k4	1990
Microsoft	Microsoft	kA	Microsoft
spustil	spustit	k5eAaPmAgInS	spustit
rozhraní	rozhraní	k1gNnSc6	rozhraní
Windows	Windows	kA	Windows
3.0	[number]	k4	3.0
s	s	k7c7	s
aerodynamickou	aerodynamický	k2eAgFnSc7d1	aerodynamická
grafikou	grafika	k1gFnSc7	grafika
uživatelského	uživatelský	k2eAgNnSc2d1	Uživatelské
rozhraní	rozhraní	k1gNnSc2	rozhraní
a	a	k8xC	a
větší	veliký	k2eAgFnSc7d2	veliký
schopností	schopnost	k1gFnSc7	schopnost
chráněného	chráněný	k2eAgInSc2d1	chráněný
režimu	režim	k1gInSc2	režim
pro	pro	k7c4	pro
procesory	procesor	k1gInPc4	procesor
Intel	Intel	kA	Intel
80386	[number]	k4	80386
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
Office	Office	kA	Office
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
Windows	Windows	kA	Windows
ovládly	ovládnout	k5eAaPmAgFnP	ovládnout
trh	trh	k1gInSc4	trh
<g/>
.	.	kIx.	.
</s>
<s>
Konkurent	konkurent	k1gMnSc1	konkurent
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Wordu	Word	k1gInSc2	Word
<g/>
,	,	kIx,	,
Novell	Novell	kA	Novell
<g/>
,	,	kIx,	,
podal	podat	k5eAaPmAgMnS	podat
později	pozdě	k6eAd2	pozdě
žádost	žádost	k1gFnSc4	žádost
k	k	k7c3	k
soudu	soud	k1gInSc3	soud
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgNnSc4	který
si	se	k3xPyFc3	se
stěžoval	stěžovat	k5eAaImAgMnS	stěžovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Microsoft	Microsoft	kA	Microsoft
nechal	nechat	k5eAaPmAgInS	nechat
část	část	k1gFnSc4	část
API	API	kA	API
nezdokumentovanou	zdokumentovaný	k2eNgFnSc4d1	nezdokumentovaná
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
získal	získat	k5eAaPmAgMnS	získat
konkurenční	konkurenční	k2eAgFnSc4d1	konkurenční
výhodu	výhoda	k1gFnSc4	výhoda
a	a	k8xC	a
zneužil	zneužít	k5eAaPmAgMnS	zneužít
tak	tak	k9	tak
svou	svůj	k3xOyFgFnSc4	svůj
dominanci	dominance	k1gFnSc4	dominance
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1995	[number]	k4	1995
Microsoft	Microsoft	kA	Microsoft
začal	začít	k5eAaPmAgInS	začít
měnit	měnit	k5eAaImF	měnit
své	svůj	k3xOyFgFnPc4	svůj
nabídky	nabídka	k1gFnPc4	nabídka
a	a	k8xC	a
rozšiřovat	rozšiřovat	k5eAaImF	rozšiřovat
působení	působení	k1gNnSc4	působení
svých	svůj	k3xOyFgInPc2	svůj
produktů	produkt	k1gInPc2	produkt
do	do	k7c2	do
oblastí	oblast	k1gFnPc2	oblast
počítačových	počítačový	k2eAgFnPc2d1	počítačová
sítí	síť	k1gFnPc2	síť
a	a	k8xC	a
internetu	internet	k1gInSc2	internet
(	(	kIx(	(
<g/>
World	World	k1gInSc1	World
Wide	Wid	k1gFnSc2	Wid
Webu	web	k1gInSc2	web
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1995	[number]	k4	1995
vydala	vydat	k5eAaPmAgFnS	vydat
společnost	společnost	k1gFnSc4	společnost
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
Windows	Windows	kA	Windows
95	[number]	k4	95
s	s	k7c7	s
preemptivním	preemptivní	k2eAgInSc7d1	preemptivní
multitaskingem	multitasking	k1gInSc7	multitasking
<g/>
,	,	kIx,	,
kompletně	kompletně	k6eAd1	kompletně
novým	nový	k2eAgNnSc7d1	nové
uživatelským	uživatelský	k2eAgNnSc7d1	Uživatelské
rozhraním	rozhraní	k1gNnSc7	rozhraní
s	s	k7c7	s
nabídkou	nabídka	k1gFnSc7	nabídka
Start	start	k1gInSc1	start
MSN	MSN	kA	MSN
a	a	k8xC	a
pro	pro	k7c4	pro
OEM	OEM	kA	OEM
výrobce	výrobce	k1gMnSc1	výrobce
také	také	k6eAd1	také
s	s	k7c7	s
prohlížečem	prohlížeč	k1gInSc7	prohlížeč
Internet	Internet	k1gInSc1	Internet
Explorer	Explorra	k1gFnPc2	Explorra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prodejních	prodejní	k2eAgFnPc6d1	prodejní
krabicích	krabice	k1gFnPc6	krabice
se	se	k3xPyFc4	se
prohlížeč	prohlížeč	k1gMnSc1	prohlížeč
nenacházel	nacházet	k5eNaImAgMnS	nacházet
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
když	když	k8xS	když
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgFnP	dostat
do	do	k7c2	do
oběhu	oběh	k1gInSc2	oběh
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
prohlížeč	prohlížeč	k1gInSc1	prohlížeč
ještě	ještě	k6eAd1	ještě
nebyl	být	k5eNaImAgInS	být
hotov	hotov	k2eAgInSc1d1	hotov
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
zahrnut	zahrnout	k5eAaPmNgInS	zahrnout
až	až	k9	až
v	v	k7c6	v
balíčcích	balíček	k1gMnPc6	balíček
Windows	Windows	kA	Windows
95	[number]	k4	95
Plus	plus	k1gInSc1	plus
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
se	se	k3xPyFc4	se
Microsoft	Microsoft	kA	Microsoft
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
ještě	ještě	k9	ještě
do	do	k7c2	do
dalších	další	k2eAgNnPc2d1	další
odvětví	odvětví	k1gNnPc2	odvětví
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	s	k7c7	s
společností	společnost	k1gFnSc7	společnost
NBC	NBC	kA	NBC
Universal	Universal	k1gMnSc1	Universal
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
zpravodajskou	zpravodajský	k2eAgFnSc4d1	zpravodajská
televizní	televizní	k2eAgFnSc4d1	televizní
stanici	stanice	k1gFnSc4	stanice
MSNBC	MSNBC	kA	MSNBC
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
společnost	společnost	k1gFnSc1	společnost
vydala	vydat	k5eAaPmAgFnS	vydat
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
Windows	Windows	kA	Windows
CE	CE	kA	CE
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
určen	určit	k5eAaPmNgInS	určit
pro	pro	k7c4	pro
zařízení	zařízení	k1gNnSc4	zařízení
s	s	k7c7	s
nízkou	nízký	k2eAgFnSc7d1	nízká
pamětí	paměť	k1gFnSc7	paměť
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
PDA	PDA	kA	PDA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
zažalovalo	zažalovat	k5eAaPmAgNnS	zažalovat
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
USA	USA	kA	USA
Microsoft	Microsoft	kA	Microsoft
u	u	k7c2	u
federálního	federální	k2eAgInSc2d1	federální
soudu	soud	k1gInSc2	soud
pro	pro	k7c4	pro
porušení	porušení	k1gNnSc4	porušení
prohlášení	prohlášení	k1gNnSc2	prohlášení
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Microsoft	Microsoft	kA	Microsoft
prodával	prodávat	k5eAaImAgInS	prodávat
Internet	Internet	k1gInSc1	Internet
Explorer	Explorero	k1gNnPc2	Explorero
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
balíčku	balíček	k1gInSc6	balíček
s	s	k7c7	s
operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
Windows	Windows	kA	Windows
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
se	se	k3xPyFc4	se
Bill	Bill	k1gMnSc1	Bill
Gates	Gates	k1gMnSc1	Gates
vzdal	vzdát	k5eAaPmAgMnS	vzdát
své	svůj	k3xOyFgFnPc4	svůj
funkce	funkce	k1gFnPc4	funkce
CEO	CEO	kA	CEO
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
předal	předat	k5eAaPmAgMnS	předat
svému	svůj	k3xOyFgMnSc3	svůj
příteli	přítel	k1gMnSc3	přítel
z	z	k7c2	z
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
Stevu	Steve	k1gMnSc3	Steve
Ballmerovi	Ballmer	k1gMnSc3	Ballmer
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
pracoval	pracovat	k5eAaImAgInS	pracovat
pro	pro	k7c4	pro
Microsoft	Microsoft	kA	Microsoft
už	už	k6eAd1	už
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Gates	Gates	k1gInSc1	Gates
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
novou	nový	k2eAgFnSc4d1	nová
pozici	pozice	k1gFnSc4	pozice
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
hlavním	hlavní	k2eAgMnSc7d1	hlavní
softwarovým	softwarový	k2eAgMnSc7d1	softwarový
architektem	architekt	k1gMnSc7	architekt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
se	se	k3xPyFc4	se
společnost	společnost	k1gFnSc1	společnost
přidala	přidat	k5eAaPmAgFnS	přidat
k	k	k7c3	k
alianci	aliance	k1gFnSc3	aliance
Trusted	Trusted	k1gInSc1	Trusted
Computer	computer	k1gInSc1	computer
Group	Group	k1gInSc1	Group
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zaměřila	zaměřit	k5eAaPmAgFnS	zaměřit
na	na	k7c4	na
zlepšení	zlepšení	k1gNnSc4	zlepšení
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
a	a	k8xC	a
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
duševního	duševní	k2eAgNnSc2d1	duševní
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
identifikováním	identifikování	k1gNnSc7	identifikování
změn	změna	k1gFnPc2	změna
v	v	k7c6	v
hardwaru	hardware	k1gInSc6	hardware
a	a	k8xC	a
softwaru	software	k1gInSc6	software
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
nelíbí	líbit	k5eNaImIp3nS	líbit
kritikům	kritik	k1gMnPc3	kritik
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
Microsoftu	Microsofta	k1gFnSc4	Microsofta
vytýkají	vytýkat	k5eAaImIp3nP	vytýkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
poškozuje	poškozovat	k5eAaImIp3nS	poškozovat
konkurenci	konkurence	k1gFnSc3	konkurence
a	a	k8xC	a
diskriminuje	diskriminovat	k5eAaBmIp3nS	diskriminovat
zákazníky	zákazník	k1gMnPc4	zákazník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2000	[number]	k4	2000
bylo	být	k5eAaImAgNnS	být
ukončeno	ukončit	k5eAaPmNgNnS	ukončit
soudní	soudní	k2eAgNnSc1d1	soudní
řízení	řízení	k1gNnSc1	řízení
mezi	mezi	k7c7	mezi
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
a	a	k8xC	a
společností	společnost	k1gFnSc7	společnost
Microsoft	Microsoft	kA	Microsoft
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
finální	finální	k2eAgInSc1d1	finální
verdikt	verdikt	k1gInSc1	verdikt
zněl	znět	k5eAaImAgInS	znět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Microsoft	Microsoft	kA	Microsoft
je	být	k5eAaImIp3nS	být
nezákonným	zákonný	k2eNgInSc7d1	nezákonný
monopolem	monopol	k1gInSc7	monopol
<g/>
,	,	kIx,	,
a	a	k8xC	a
celá	celý	k2eAgFnSc1d1	celá
záležitost	záležitost	k1gFnSc1	záležitost
byla	být	k5eAaImAgFnS	být
s	s	k7c7	s
Ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
vyřešena	vyřešit	k5eAaPmNgFnS	vyřešit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2001	[number]	k4	2001
vydala	vydat	k5eAaPmAgFnS	vydat
společnost	společnost	k1gFnSc4	společnost
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
Windows	Windows	kA	Windows
XP	XP	kA	XP
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
také	také	k9	také
herní	herní	k2eAgInPc1d1	herní
konzoli	konzoli	k6eAd1	konzoli
Xbox	Xbox	k1gInSc4	Xbox
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
na	na	k7c4	na
nový	nový	k2eAgInSc4d1	nový
trh	trh	k1gInSc4	trh
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgInSc3	jenž
dosud	dosud	k6eAd1	dosud
dominovaly	dominovat	k5eAaImAgFnP	dominovat
společnosti	společnost	k1gFnPc1	společnost
Sony	Sony	kA	Sony
a	a	k8xC	a
Nintendo	Nintendo	k6eAd1	Nintendo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2004	[number]	k4	2004
začala	začít	k5eAaPmAgFnS	začít
Evropská	evropský	k2eAgFnSc1d1	Evropská
komise	komise	k1gFnSc1	komise
antimonopolní	antimonopolní	k2eAgFnSc1d1	antimonopolní
soudní	soudní	k2eAgNnSc4d1	soudní
řízení	řízení	k1gNnSc4	řízení
proti	proti	k7c3	proti
společnosti	společnost	k1gFnSc3	společnost
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zneužila	zneužít	k5eAaPmAgFnS	zneužít
svého	svůj	k3xOyFgNnSc2	svůj
postavení	postavení	k1gNnSc2	postavení
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
s	s	k7c7	s
operačními	operační	k2eAgInPc7d1	operační
systémy	systém	k1gInPc7	systém
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyústilo	vyústit	k5eAaPmAgNnS	vyústit
v	v	k7c4	v
pokutu	pokuta	k1gFnSc4	pokuta
o	o	k7c6	o
výši	výše	k1gFnSc6	výše
613	[number]	k4	613
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
a	a	k8xC	a
zákaz	zákaz	k1gInSc1	zákaz
vydání	vydání	k1gNnSc2	vydání
balíčku	balíček	k1gInSc2	balíček
Windows	Windows	kA	Windows
XP	XP	kA	XP
s	s	k7c7	s
programem	program	k1gInSc7	program
Windows	Windows	kA	Windows
Media	medium	k1gNnSc2	medium
Player	Player	k1gInSc1	Player
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2007	[number]	k4	2007
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
Windows	Windows	kA	Windows
Vista	vista	k2eAgInSc1d1	vista
s	s	k7c7	s
novými	nový	k2eAgFnPc7d1	nová
funkcemi	funkce	k1gFnPc7	funkce
<g/>
,	,	kIx,	,
lepší	dobrý	k2eAgFnPc1d2	lepší
bezpečností	bezpečnost	k1gFnSc7	bezpečnost
a	a	k8xC	a
novým	nový	k2eAgInSc7d1	nový
stylem	styl	k1gInSc7	styl
uživatelského	uživatelský	k2eAgNnSc2d1	Uživatelské
rozhraní	rozhraní	k1gNnSc2	rozhraní
<g/>
,	,	kIx,	,
nazvaným	nazvaný	k2eAgMnSc7d1	nazvaný
Windows	Windows	kA	Windows
Aero	aero	k1gNnSc1	aero
<g/>
.	.	kIx.	.
</s>
<s>
Microsoft	Microsoft	kA	Microsoft
Office	Office	kA	Office
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
ve	v	k7c4	v
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
zase	zase	k9	zase
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
nový	nový	k2eAgInSc4d1	nový
styl	styl	k1gInSc4	styl
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
pásem	pás	k1gInSc7	pás
ovládacích	ovládací	k2eAgFnPc2d1	ovládací
karet	kareta	k1gFnPc2	kareta
Ribbon	Ribbon	k1gMnSc1	Ribbon
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
jej	on	k3xPp3gNnSc4	on
význačně	význačně	k6eAd1	význačně
odlišil	odlišit	k5eAaPmAgMnS	odlišit
od	od	k7c2	od
jeho	jeho	k3xOp3gMnPc2	jeho
předchůdců	předchůdce	k1gMnPc2	předchůdce
<g/>
.	.	kIx.	.
</s>
<s>
Relativně	relativně	k6eAd1	relativně
velké	velký	k2eAgInPc4d1	velký
prodeje	prodej	k1gInPc4	prodej
obou	dva	k4xCgInPc2	dva
produktů	produkt	k1gInPc2	produkt
pomohly	pomoct	k5eAaPmAgInP	pomoct
společnosti	společnost	k1gFnSc3	společnost
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
rekordní	rekordní	k2eAgInSc4d1	rekordní
profit	profit	k1gInSc4	profit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
unie	unie	k1gFnSc1	unie
pak	pak	k6eAd1	pak
požadovala	požadovat	k5eAaImAgFnS	požadovat
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
další	další	k2eAgFnSc4d1	další
pokutu	pokuta	k1gFnSc4	pokuta
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
1,4	[number]	k4	1,4
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
svých	svůj	k3xOyFgMnPc2	svůj
rivalů	rival	k1gMnPc2	rival
požadovala	požadovat	k5eAaImAgFnS	požadovat
nemyslitelné	myslitelný	k2eNgFnPc4d1	nemyslitelná
částky	částka	k1gFnPc4	částka
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
klíčové	klíčový	k2eAgFnPc4d1	klíčová
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
síťovém	síťový	k2eAgInSc6d1	síťový
operačním	operační	k2eAgInSc6d1	operační
systému	systém	k1gInSc6	systém
Microsoft	Microsoft	kA	Microsoft
SQL	SQL	kA	SQL
Server	server	k1gInSc4	server
a	a	k8xC	a
také	také	k9	také
o	o	k7c6	o
systému	systém	k1gInSc6	systém
BackOffice	BackOffice	k1gFnSc1	BackOffice
Server	server	k1gInSc1	server
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2008	[number]	k4	2008
Gates	Gates	k1gMnSc1	Gates
odešel	odejít	k5eAaPmAgMnS	odejít
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
hlavního	hlavní	k2eAgMnSc2d1	hlavní
softwarového	softwarový	k2eAgMnSc2d1	softwarový
architekta	architekt	k1gMnSc2	architekt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ponechal	ponechat	k5eAaPmAgMnS	ponechat
si	se	k3xPyFc3	se
všechny	všechen	k3xTgFnPc4	všechen
své	svůj	k3xOyFgFnPc4	svůj
ostatní	ostatní	k2eAgFnPc4d1	ostatní
funkce	funkce	k1gFnPc4	funkce
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
poradcem	poradce	k1gMnSc7	poradce
klíčových	klíčový	k2eAgInPc2d1	klíčový
projektů	projekt	k1gInPc2	projekt
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2008	[number]	k4	2008
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
společnost	společnost	k1gFnSc1	společnost
do	do	k7c2	do
cloud	clouda	k1gFnPc2	clouda
computingu	computing	k1gInSc2	computing
s	s	k7c7	s
platformou	platforma	k1gFnSc7	platforma
Windows	Windows	kA	Windows
Azure	azur	k1gInSc5	azur
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2009	[number]	k4	2009
se	se	k3xPyFc4	se
společnost	společnost	k1gFnSc1	společnost
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
expandovat	expandovat	k5eAaImF	expandovat
založením	založení	k1gNnSc7	založení
vlastního	vlastní	k2eAgInSc2d1	vlastní
řetězce	řetězec	k1gInSc2	řetězec
maloobchodů	maloobchod	k1gInPc2	maloobchod
zvaných	zvaný	k2eAgInPc2d1	zvaný
Microsoft	Microsoft	kA	Microsoft
Store	Stor	k1gMnSc5	Stor
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
první	první	k4xOgFnSc1	první
pobočka	pobočka	k1gFnSc1	pobočka
<g/>
,	,	kIx,	,
v	v	k7c6	v
arizonském	arizonský	k2eAgInSc6d1	arizonský
Scottsdale	Scottsdala	k1gFnSc6	Scottsdala
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
v	v	k7c4	v
den	den	k1gInSc4	den
vydání	vydání	k1gNnSc2	vydání
nového	nový	k2eAgInSc2d1	nový
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
Windows	Windows	kA	Windows
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vývoji	vývoj	k1gInSc6	vývoj
nového	nový	k2eAgInSc2d1	nový
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
se	se	k3xPyFc4	se
soustředil	soustředit	k5eAaPmAgMnS	soustředit
především	především	k6eAd1	především
na	na	k7c6	na
odstranění	odstranění	k1gNnSc6	odstranění
chyb	chyba	k1gFnPc2	chyba
Visty	Vista	k1gFnSc2	Vista
a	a	k8xC	a
zrychlení	zrychlení	k1gNnSc1	zrychlení
práce	práce	k1gFnSc2	práce
systému	systém	k1gInSc2	systém
místo	místo	k7c2	místo
celkového	celkový	k2eAgNnSc2d1	celkové
předělávání	předělávání	k1gNnSc2	předělávání
OS	OS	kA	OS
Windows	Windows	kA	Windows
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2011	[number]	k4	2011
se	se	k3xPyFc4	se
Microsoft	Microsoft	kA	Microsoft
stal	stát	k5eAaPmAgInS	stát
zakládajícím	zakládající	k2eAgInSc7d1	zakládající
členem	člen	k1gInSc7	člen
organizace	organizace	k1gFnSc2	organizace
Open	Open	k1gInSc1	Open
Networking	Networking	k1gInSc1	Networking
Foundation	Foundation	k1gInSc4	Foundation
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
se	s	k7c7	s
společnostmi	společnost	k1gFnPc7	společnost
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
Google	Google	k1gNnSc4	Google
<g/>
,	,	kIx,	,
Yahoo	Yahoo	k1gNnSc4	Yahoo
nebo	nebo	k8xC	nebo
Verizon	Verizon	k1gNnSc4	Verizon
<g/>
.	.	kIx.	.
</s>
<s>
Organizace	organizace	k1gFnSc1	organizace
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
podporou	podpora	k1gFnSc7	podpora
nového	nový	k2eAgInSc2d1	nový
způsobu	způsob	k1gInSc2	způsob
cloud	cloud	k1gInSc1	cloud
computingu	computing	k1gInSc2	computing
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
nazván	nazván	k2eAgInSc1d1	nazván
Software-Defined	Software-Defined	k1gInSc1	Software-Defined
Networking	Networking	k1gInSc1	Networking
<g/>
.	.	kIx.	.
</s>
<s>
Snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
o	o	k7c4	o
urychlení	urychlení	k1gNnSc4	urychlení
inovací	inovace	k1gFnPc2	inovace
spojených	spojený	k2eAgFnPc2d1	spojená
se	s	k7c7	s
softwarovými	softwarový	k2eAgFnPc7d1	softwarová
změnami	změna	k1gFnPc7	změna
telekomunikačních	telekomunikační	k2eAgFnPc2d1	telekomunikační
a	a	k8xC	a
bezdrátových	bezdrátový	k2eAgFnPc2d1	bezdrátová
sítí	síť	k1gFnPc2	síť
<g/>
,	,	kIx,	,
datových	datový	k2eAgNnPc2d1	datové
center	centrum	k1gNnPc2	centrum
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
networkingových	tworkingový	k2eNgFnPc2d1	networkingová
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
Windows	Windows	kA	Windows
8	[number]	k4	8
bez	bez	k7c2	bez
nabídky	nabídka	k1gFnSc2	nabídka
start	start	k1gInSc1	start
<g/>
,	,	kIx,	,
s	s	k7c7	s
novým	nový	k2eAgInSc7d1	nový
stylem	styl	k1gInSc7	styl
uživatelského	uživatelský	k2eAgNnSc2d1	Uživatelské
rozhraní	rozhraní	k1gNnSc2	rozhraní
<g/>
,	,	kIx,	,
nazývaným	nazývaný	k2eAgInSc7d1	nazývaný
Metro	metro	k1gNnSc4	metro
a	a	k8xC	a
novými	nový	k2eAgFnPc7d1	nová
moderními	moderní	k2eAgFnPc7d1	moderní
aplikacemi	aplikace	k1gFnPc7	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2013	[number]	k4	2013
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
opravná	opravný	k2eAgFnSc1d1	opravná
verze	verze	k1gFnSc1	verze
s	s	k7c7	s
názvem	název	k1gInSc7	název
Windows	Windows	kA	Windows
8.1	[number]	k4	8.1
s	s	k7c7	s
opravenými	opravený	k2eAgFnPc7d1	opravená
chybami	chyba	k1gFnPc7	chyba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2013	[number]	k4	2013
koupil	koupit	k5eAaPmAgMnS	koupit
Microsoft	Microsoft	kA	Microsoft
telefonní	telefonní	k2eAgFnSc4d1	telefonní
divizi	divize	k1gFnSc4	divize
(	(	kIx(	(
<g/>
Nokia	Nokia	kA	Nokia
Devices	Devices	k1gInSc1	Devices
&	&	k?	&
Services	Services	k1gInSc1	Services
<g/>
)	)	kIx)	)
společnosti	společnost	k1gFnSc2	společnost
Nokia	Nokia	kA	Nokia
za	za	k7c4	za
7	[number]	k4	7
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2014	[number]	k4	2014
Microsoft	Microsoft	kA	Microsoft
oficiálně	oficiálně	k6eAd1	oficiálně
oznámil	oznámit	k5eAaPmAgMnS	oznámit
odkoupení	odkoupení	k1gNnSc4	odkoupení
společnosti	společnost	k1gFnSc2	společnost
Mojang	Mojanga	k1gFnPc2	Mojanga
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c7	za
vývojem	vývoj	k1gInSc7	vývoj
a	a	k8xC	a
distribucí	distribuce	k1gFnSc7	distribuce
hry	hra	k1gFnSc2	hra
Minecraft	Minecrafta	k1gFnPc2	Minecrafta
<g/>
,	,	kIx,	,
za	za	k7c4	za
2,5	[number]	k4	2,5
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Mojang	Mojang	k1gInSc1	Mojang
tak	tak	k9	tak
nyní	nyní	k6eAd1	nyní
spadá	spadat	k5eAaPmIp3nS	spadat
pod	pod	k7c7	pod
Microsoft	Microsoft	kA	Microsoft
Studios	Studios	k?	Studios
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
Microsoft	Microsoft	kA	Microsoft
představil	představit	k5eAaPmAgInS	představit
Windows	Windows	kA	Windows
10	[number]	k4	10
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
29	[number]	k4	29
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
první	první	k4xOgInSc1	první
balíček	balíček	k1gInSc1	balíček
vylepšení	vylepšení	k1gNnSc2	vylepšení
s	s	k7c7	s
názvem	název	k1gInSc7	název
November	November	k1gMnSc1	November
Update	update	k1gInSc1	update
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2016	[number]	k4	2016
koupil	koupit	k5eAaPmAgMnS	koupit
Microsoft	Microsoft	kA	Microsoft
firmu	firma	k1gFnSc4	firma
Xamarin	Xamarin	k1gInSc1	Xamarin
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c7	za
vývojem	vývoj	k1gInSc7	vývoj
.	.	kIx.	.
<g/>
NET	NET	kA	NET
<g/>
/	/	kIx~	/
<g/>
C	C	kA	C
<g/>
#	#	kIx~	#
pro	pro	k7c4	pro
jiné	jiný	k2eAgFnPc4d1	jiná
platformy	platforma	k1gFnPc4	platforma
(	(	kIx(	(
<g/>
Mac	Mac	kA	Mac
OS	OS	kA	OS
<g/>
,	,	kIx,	,
iOS	iOS	k?	iOS
<g/>
,	,	kIx,	,
Android	android	k1gInSc1	android
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
čele	čelo	k1gNnSc6	čelo
stojí	stát	k5eAaImIp3nS	stát
známý	známý	k2eAgInSc1d1	známý
open-source	openourka	k1gFnSc3	open-sourka
vývojář	vývojář	k1gMnSc1	vývojář
Miguel	Miguel	k1gMnSc1	Miguel
de	de	k?	de
Icaza	Icaza	k1gFnSc1	Icaza
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
fiskálním	fiskální	k2eAgInSc6d1	fiskální
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
měla	mít	k5eAaImAgFnS	mít
společnost	společnost	k1gFnSc1	společnost
hned	hned	k6eAd1	hned
pět	pět	k4xCc4	pět
produkčních	produkční	k2eAgFnPc2d1	produkční
divizí	divize	k1gFnPc2	divize
<g/>
:	:	kIx,	:
Windows	Windows	kA	Windows
&	&	k?	&
Windows	Windows	kA	Windows
Live	Live	k1gInSc1	Live
Division	Division	k1gInSc1	Division
<g/>
,	,	kIx,	,
Server	server	k1gInSc1	server
and	and	k?	and
Tools	Tools	k1gInSc1	Tools
<g/>
,	,	kIx,	,
Online	Onlin	k1gInSc5	Onlin
Services	Servicesa	k1gFnPc2	Servicesa
Division	Division	k1gInSc1	Division
<g/>
,	,	kIx,	,
Microsoft	Microsoft	kA	Microsoft
Business	business	k1gInSc1	business
Division	Division	k1gInSc1	Division
a	a	k8xC	a
Entertainment	Entertainment	k1gInSc1	Entertainment
and	and	k?	and
Devices	Devices	k1gInSc1	Devices
Division	Division	k1gInSc1	Division
<g/>
.	.	kIx.	.
</s>
<s>
Divize	divize	k1gFnSc1	divize
Windows	Windows	kA	Windows
a	a	k8xC	a
Windows	Windows	kA	Windows
Live	Live	k1gNnSc6	Live
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
vlajkové	vlajkový	k2eAgInPc4d1	vlajkový
operační	operační	k2eAgInPc4d1	operační
systémy	systém	k1gInPc4	systém
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
Windows	Windows	kA	Windows
7	[number]	k4	7
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
také	také	k9	také
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
služby	služba	k1gFnPc4	služba
Windows	Windows	kA	Windows
Live	Liv	k1gFnSc2	Liv
<g/>
.	.	kIx.	.
</s>
<s>
Divize	divize	k1gFnSc1	divize
serverů	server	k1gInPc2	server
a	a	k8xC	a
nástrojů	nástroj	k1gInPc2	nástroj
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
serverové	serverový	k2eAgFnPc4d1	serverová
verze	verze	k1gFnPc4	verze
Windowsu	Windows	k1gInSc2	Windows
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
Windows	Windows	kA	Windows
Server	server	k1gInSc4	server
2008	[number]	k4	2008
R	R	kA	R
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
i	i	k9	i
sadu	sada	k1gFnSc4	sada
vývojových	vývojový	k2eAgInPc2d1	vývojový
nástrojů	nástroj	k1gInPc2	nástroj
Microsoft	Microsoft	kA	Microsoft
Visual	Visual	k1gInSc1	Visual
Studio	studio	k1gNnSc1	studio
<g/>
,	,	kIx,	,
Microsoft	Microsoft	kA	Microsoft
Silverlight	Silverlight	k1gInSc1	Silverlight
<g/>
,	,	kIx,	,
struktury	struktura	k1gFnPc1	struktura
webových	webový	k2eAgFnPc2d1	webová
aplikací	aplikace	k1gFnPc2	aplikace
a	a	k8xC	a
System	Systo	k1gNnSc7	Systo
Center	centrum	k1gNnPc2	centrum
Configuration	Configuration	k1gInSc4	Configuration
Manager	manager	k1gMnSc1	manager
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
dálkové	dálkový	k2eAgNnSc4d1	dálkové
ovládání	ovládání	k1gNnSc4	ovládání
<g/>
,	,	kIx,	,
správu	správa	k1gFnSc4	správa
patchů	patch	k1gInPc2	patch
<g/>
,	,	kIx,	,
distribuci	distribuce	k1gFnSc4	distribuce
softwaru	software	k1gInSc2	software
nebo	nebo	k8xC	nebo
inventuru	inventura	k1gFnSc4	inventura
softwaru	software	k1gInSc2	software
a	a	k8xC	a
hardwaru	hardware	k1gInSc2	hardware
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
serverové	serverový	k2eAgInPc4d1	serverový
výrobky	výrobek	k1gInPc4	výrobek
patří	patřit	k5eAaImIp3nS	patřit
Microsoft	Microsoft	kA	Microsoft
SQL	SQL	kA	SQL
Server	server	k1gInSc1	server
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
spravuje	spravovat	k5eAaImIp3nS	spravovat
relační	relační	k2eAgFnSc3d1	relační
databázi	databáze	k1gFnSc3	databáze
<g/>
,	,	kIx,	,
Microsoft	Microsoft	kA	Microsoft
Exchange	Exchange	k1gInSc1	Exchange
Server	server	k1gInSc1	server
pro	pro	k7c4	pro
obchodní	obchodní	k2eAgInPc4d1	obchodní
e-maily	eail	k1gInPc4	e-mail
a	a	k8xC	a
kalendáře	kalendář	k1gInPc4	kalendář
<g/>
,	,	kIx,	,
Windows	Windows	kA	Windows
Small	Small	k1gInSc1	Small
Business	business	k1gInSc1	business
Server	server	k1gInSc4	server
pro	pro	k7c4	pro
komunikaci	komunikace	k1gFnSc4	komunikace
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
malé	malý	k2eAgFnPc4d1	malá
obchodní	obchodní	k2eAgFnPc4d1	obchodní
funkce	funkce	k1gFnPc4	funkce
a	a	k8xC	a
Microsoft	Microsoft	kA	Microsoft
BizTalk	BizTalk	k1gInSc1	BizTalk
Server	server	k1gInSc1	server
pro	pro	k7c4	pro
procesní	procesní	k2eAgNnSc4d1	procesní
řízení	řízení	k1gNnSc4	řízení
<g/>
.	.	kIx.	.
</s>
<s>
Microsoft	Microsoft	kA	Microsoft
také	také	k9	také
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
IT	IT	kA	IT
poradnu	poradna	k1gFnSc4	poradna
a	a	k8xC	a
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
sadu	sada	k1gFnSc4	sada
certifikačních	certifikační	k2eAgInPc2d1	certifikační
programů	program	k1gInPc2	program
Microsoft	Microsoft	kA	Microsoft
Certified	Certified	k1gInSc1	Certified
Professional	Professional	k1gMnPc2	Professional
od	od	k7c2	od
divize	divize	k1gFnSc2	divize
Server	server	k1gInSc1	server
and	and	k?	and
Tools	Tools	k1gInSc1	Tools
pro	pro	k7c4	pro
jedince	jedinec	k1gMnPc4	jedinec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
málo	málo	k6eAd1	málo
zdatní	zdatný	k2eAgMnPc1d1	zdatný
ve	v	k7c6	v
specifické	specifický	k2eAgFnSc6d1	specifická
roli	role	k1gFnSc6	role
–	–	k?	–
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
vývojáře	vývojář	k1gMnSc4	vývojář
<g/>
,	,	kIx,	,
systémové	systémový	k2eAgMnPc4d1	systémový
nebo	nebo	k8xC	nebo
síťové	síťový	k2eAgMnPc4d1	síťový
analytiky	analytik	k1gMnPc4	analytik
<g/>
,	,	kIx,	,
instruktory	instruktor	k1gMnPc4	instruktor
a	a	k8xC	a
administrátory	administrátor	k1gMnPc4	administrátor
<g/>
.	.	kIx.	.
</s>
<s>
Divize	divize	k1gFnSc1	divize
serverů	server	k1gInPc2	server
a	a	k8xC	a
nástrojů	nástroj	k1gInPc2	nástroj
také	také	k9	také
spravuje	spravovat	k5eAaImIp3nS	spravovat
knižní	knižní	k2eAgNnSc1d1	knižní
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Microsoft	Microsoft	kA	Microsoft
Press	Press	k1gInSc1	Press
<g/>
.	.	kIx.	.
</s>
<s>
Divize	divize	k1gFnSc1	divize
online	onlinout	k5eAaPmIp3nS	onlinout
služeb	služba	k1gFnPc2	služba
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
starosti	starost	k1gFnSc6	starost
službu	služba	k1gFnSc4	služba
MSN	MSN	kA	MSN
a	a	k8xC	a
vyhledávač	vyhledávač	k1gMnSc1	vyhledávač
Bing	bingo	k1gNnPc2	bingo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2009	[number]	k4	2009
společnost	společnost	k1gFnSc4	společnost
také	také	k6eAd1	také
měla	mít	k5eAaImAgFnS	mít
18	[number]	k4	18
<g/>
procentní	procentní	k2eAgInSc4d1	procentní
vlastnictví	vlastnictví	k1gNnSc1	vlastnictví
zpravodajské	zpravodajský	k2eAgFnSc2d1	zpravodajská
TV	TV	kA	TV
stanice	stanice	k1gFnSc2	stanice
MSNBC	MSNBC	kA	MSNBC
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
divize	divize	k1gFnSc2	divize
online	onlinout	k5eAaPmIp3nS	onlinout
služeb	služba	k1gFnPc2	služba
spravuje	spravovat	k5eAaImIp3nS	spravovat
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
se	s	k7c7	s
spoluvlastníkem	spoluvlastník	k1gMnSc7	spoluvlastník
stanice	stanice	k1gFnSc2	stanice
<g/>
,	,	kIx,	,
firmou	firma	k1gFnSc7	firma
NBC	NBC	kA	NBC
Universal	Universal	k1gFnSc7	Universal
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
webovou	webový	k2eAgFnSc4d1	webová
stránku	stránka	k1gFnSc4	stránka
msnbc	msnbc	k6eAd1	msnbc
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
.	.	kIx.	.
</s>
<s>
Obchodní	obchodní	k2eAgFnSc1d1	obchodní
divize	divize	k1gFnSc1	divize
Microsoftu	Microsoft	k1gInSc2	Microsoft
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
kancelářské	kancelářský	k2eAgInPc1d1	kancelářský
programy	program	k1gInPc1	program
Microsoft	Microsoft	kA	Microsoft
Office	Office	kA	Office
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
patří	patřit	k5eAaImIp3nS	patřit
textový	textový	k2eAgInSc4d1	textový
editor	editor	k1gInSc4	editor
Microsoft	Microsoft	kA	Microsoft
Word	Word	kA	Word
<g/>
,	,	kIx,	,
program	program	k1gInSc1	program
relační	relační	k2eAgFnSc2d1	relační
databáze	databáze	k1gFnSc2	databáze
Microsoft	Microsoft	kA	Microsoft
Access	Access	k1gInSc1	Access
<g/>
,	,	kIx,	,
tabulkový	tabulkový	k2eAgInSc1d1	tabulkový
procesor	procesor	k1gInSc1	procesor
Microsoft	Microsoft	kA	Microsoft
Excel	Excel	kA	Excel
<g/>
,	,	kIx,	,
groupware	groupwar	k1gMnSc5	groupwar
Microsoft	Microsoft	kA	Microsoft
Outlook	Outlook	k1gInSc4	Outlook
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
používaný	používaný	k2eAgInSc4d1	používaný
společně	společně	k6eAd1	společně
s	s	k7c7	s
Microsoft	Microsoft	kA	Microsoft
Exchange	Exchange	k1gFnSc7	Exchange
Serverem	server	k1gInSc7	server
<g/>
,	,	kIx,	,
prezentační	prezentační	k2eAgInSc1d1	prezentační
software	software	k1gInSc1	software
Microsoft	Microsoft	kA	Microsoft
PowerPoint	PowerPoint	k1gInSc1	PowerPoint
a	a	k8xC	a
program	program	k1gInSc1	program
na	na	k7c4	na
desktop	desktop	k1gInSc4	desktop
publishing	publishing	k1gInSc1	publishing
Microsoft	Microsoft	kA	Microsoft
Publisher	Publishra	k1gFnPc2	Publishra
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vydáním	vydání	k1gNnSc7	vydání
verze	verze	k1gFnSc2	verze
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
přibyly	přibýt	k5eAaPmAgFnP	přibýt
také	také	k9	také
Microsoft	Microsoft	kA	Microsoft
Visio	Visio	k1gMnSc1	Visio
<g/>
,	,	kIx,	,
Microsoft	Microsoft	kA	Microsoft
Project	Project	k1gInSc1	Project
<g/>
,	,	kIx,	,
Microsoft	Microsoft	kA	Microsoft
OneNote	OneNot	k1gInSc5	OneNot
<g/>
,	,	kIx,	,
Microsoft	Microsoft	kA	Microsoft
MapPoint	MapPoint	k1gInSc1	MapPoint
a	a	k8xC	a
Microsoft	Microsoft	kA	Microsoft
InfoPath	InfoPath	k1gInSc1	InfoPath
<g/>
.	.	kIx.	.
</s>
<s>
Divize	divize	k1gFnSc1	divize
dále	daleko	k6eAd2	daleko
nabízí	nabízet	k5eAaImIp3nS	nabízet
enterprise	enterprise	k1gFnSc1	enterprise
resource	resourka	k1gFnSc6	resourka
planning	planning	k1gInSc4	planning
pod	pod	k7c7	pod
značkou	značka	k1gFnSc7	značka
Microsoft	Microsoft	kA	Microsoft
Dynamics	Dynamics	k1gInSc1	Dynamics
<g/>
.	.	kIx.	.
</s>
<s>
Zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
tyto	tento	k3xDgInPc4	tento
programy	program	k1gInPc4	program
mají	mít	k5eAaImIp3nP	mít
společností	společnost	k1gFnSc7	společnost
různých	různý	k2eAgInPc2d1	různý
typů	typ	k1gInPc2	typ
a	a	k8xC	a
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
méně	málo	k6eAd2	málo
než	než	k8xS	než
7	[number]	k4	7
500	[number]	k4	500
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
značkou	značka	k1gFnSc7	značka
Dynamics	Dynamicsa	k1gFnPc2	Dynamicsa
se	se	k3xPyFc4	se
také	také	k9	také
skrývá	skrývat	k5eAaImIp3nS	skrývat
program	program	k1gInSc1	program
na	na	k7c4	na
řízení	řízení	k1gNnSc4	řízení
vztahů	vztah	k1gInPc2	vztah
se	s	k7c7	s
zákazníky	zákazník	k1gMnPc7	zákazník
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
částí	část	k1gFnSc7	část
Windows	Windows	kA	Windows
Azure	azur	k1gInSc5	azur
Platform	Platform	k1gInSc1	Platform
<g/>
.	.	kIx.	.
</s>
<s>
Divize	divize	k1gFnSc1	divize
zábavy	zábava	k1gFnSc2	zábava
a	a	k8xC	a
zařízení	zařízení	k1gNnSc2	zařízení
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
Windows	Windows	kA	Windows
CE	CE	kA	CE
pro	pro	k7c4	pro
vestavěné	vestavěný	k2eAgInPc4d1	vestavěný
systémy	systém	k1gInPc4	systém
a	a	k8xC	a
Windows	Windows	kA	Windows
Phone	Phon	k1gInSc5	Phon
7	[number]	k4	7
pro	pro	k7c4	pro
smartphony	smartphon	k1gInPc4	smartphon
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
trh	trh	k1gInSc4	trh
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonů	telefon	k1gInPc2	telefon
se	se	k3xPyFc4	se
Microsoft	Microsoft	kA	Microsoft
dostal	dostat	k5eAaPmAgMnS	dostat
vydáním	vydání	k1gNnSc7	vydání
Windowsu	Windows	k1gInSc2	Windows
CE	CE	kA	CE
pro	pro	k7c4	pro
mobilní	mobilní	k2eAgNnSc4d1	mobilní
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
se	se	k3xPyFc4	se
časem	čas	k1gInSc7	čas
vyvinuly	vyvinout	k5eAaPmAgInP	vyvinout
operační	operační	k2eAgInPc1d1	operační
systémy	systém	k1gInPc1	systém
Windows	Windows	kA	Windows
Mobile	mobile	k1gNnSc2	mobile
a	a	k8xC	a
Windows	Windows	kA	Windows
Phone	Phon	k1gInSc5	Phon
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Windows	Windows	kA	Windows
CE	CE	kA	CE
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
zařízením	zařízení	k1gNnSc7	zařízení
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
není	být	k5eNaImIp3nS	být
pro	pro	k7c4	pro
uživatele	uživatel	k1gMnPc4	uživatel
přímo	přímo	k6eAd1	přímo
viditelný	viditelný	k2eAgMnSc1d1	viditelný
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
ve	v	k7c6	v
spotřebičích	spotřebič	k1gInPc6	spotřebič
nebo	nebo	k8xC	nebo
automobilech	automobil	k1gInPc6	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Divize	divize	k1gFnSc1	divize
také	také	k9	také
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
počítačové	počítačový	k2eAgFnPc4d1	počítačová
hry	hra	k1gFnPc4	hra
pro	pro	k7c4	pro
počítače	počítač	k1gInPc4	počítač
s	s	k7c7	s
Windowsem	Windows	k1gInSc7	Windows
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
Age	Age	k1gMnSc1	Age
of	of	k?	of
Empires	Empires	k1gMnSc1	Empires
nebo	nebo	k8xC	nebo
Microsoft	Microsoft	kA	Microsoft
Flight	Flight	k1gMnSc1	Flight
Simulator	Simulator	k1gMnSc1	Simulator
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
sdílí	sdílet	k5eAaImIp3nS	sdílet
střechu	střecha	k1gFnSc4	střecha
s	s	k7c7	s
obchodní	obchodní	k2eAgFnSc7d1	obchodní
divizí	divize	k1gFnSc7	divize
MacIntoshe	MacIntosh	k1gFnSc2	MacIntosh
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
programy	program	k1gInPc7	program
pro	pro	k7c4	pro
Mac	Mac	kA	Mac
OS	OS	kA	OS
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Microsoft	Microsoft	kA	Microsoft
Office	Office	kA	Office
2008	[number]	k4	2008
for	forum	k1gNnPc2	forum
Mac	Mac	kA	Mac
<g/>
.	.	kIx.	.
</s>
<s>
Divize	divize	k1gFnSc1	divize
dále	daleko	k6eAd2	daleko
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
<g/>
,	,	kIx,	,
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
a	a	k8xC	a
prodává	prodávat	k5eAaImIp3nS	prodávat
spotřební	spotřební	k2eAgFnSc4d1	spotřební
elektroniku	elektronika	k1gFnSc4	elektronika
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
herní	herní	k2eAgFnSc2d1	herní
konzole	konzola	k1gFnSc3	konzola
Xbox	Xbox	k1gInSc1	Xbox
360	[number]	k4	360
<g/>
,	,	kIx,	,
kapesního	kapesní	k2eAgInSc2d1	kapesní
přehrávače	přehrávač	k1gInSc2	přehrávač
Zune	Zun	k1gFnSc2	Zun
a	a	k8xC	a
televizní	televizní	k2eAgFnSc2d1	televizní
internetové	internetový	k2eAgFnSc2d1	internetová
služby	služba	k1gFnSc2	služba
MSN	MSN	kA	MSN
TV	TV	kA	TV
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k6eAd1	také
prodává	prodávat	k5eAaImIp3nS	prodávat
myši	myš	k1gFnSc2	myš
<g/>
,	,	kIx,	,
klávesnice	klávesnice	k1gFnSc2	klávesnice
a	a	k8xC	a
různé	různý	k2eAgInPc1d1	různý
herní	herní	k2eAgInPc1d1	herní
ovladače	ovladač	k1gInPc1	ovladač
<g/>
,	,	kIx,	,
např.	např.	kA	např.
joysticky	joysticky	k6eAd1	joysticky
nebo	nebo	k8xC	nebo
gamepady	gamepada	k1gFnSc2	gamepada
<g/>
.	.	kIx.	.
</s>
<s>
Divize	divize	k1gFnSc1	divize
společnosti	společnost	k1gFnSc2	společnost
Nokia	Nokia	kA	Nokia
odkoupená	odkoupený	k2eAgFnSc1d1	odkoupená
společností	společnost	k1gFnSc7	společnost
Microsoft	Microsoft	kA	Microsoft
Corporation	Corporation	k1gInSc1	Corporation
za	za	k7c4	za
7	[number]	k4	7
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Microsoft	Microsoft	kA	Microsoft
tímto	tento	k3xDgInSc7	tento
nákupem	nákup	k1gInSc7	nákup
získal	získat	k5eAaPmAgInS	získat
několik	několik	k4yIc4	několik
stovek	stovka	k1gFnPc2	stovka
patentů	patent	k1gInPc2	patent
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
jiné	jiný	k2eAgFnPc4d1	jiná
služby	služba	k1gFnPc4	služba
již	již	k6eAd1	již
rozpůlené	rozpůlený	k2eAgFnPc4d1	rozpůlená
společnosti	společnost	k1gFnPc4	společnost
Nokia	Nokia	kA	Nokia
<g/>
.	.	kIx.	.
</s>
<s>
Nokia	Nokia	kA	Nokia
(	(	kIx(	(
<g/>
jejím	její	k3xOp3gMnSc7	její
zástupcem	zástupce	k1gMnSc7	zástupce
je	být	k5eAaImIp3nS	být
dceřiná	dceřiný	k2eAgFnSc1d1	dceřiná
společnost	společnost	k1gFnSc1	společnost
společnosti	společnost	k1gFnSc2	společnost
Microsoft	Microsoft	kA	Microsoft
Corporation	Corporation	k1gInSc1	Corporation
–	–	k?	–
Microsoft	Microsoft	kA	Microsoft
Mobile	mobile	k1gNnSc4	mobile
Oy	Oy	k1gFnSc2	Oy
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
(	(	kIx(	(
<g/>
Nokia	Nokia	kA	Nokia
Devices	Devices	k1gMnSc1	Devices
&	&	k?	&
Services	Services	k1gMnSc1	Services
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
společnosti	společnost	k1gFnSc2	společnost
Microsoft	Microsoft	kA	Microsoft
Corporation	Corporation	k1gInSc1	Corporation
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
část	část	k1gFnSc1	část
jsou	být	k5eAaImIp3nP	být
zbylé	zbylý	k2eAgFnPc4d1	zbylá
služby	služba	k1gFnPc4	služba
společnosti	společnost	k1gFnSc2	společnost
Nokia	Nokia	kA	Nokia
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
mapové	mapový	k2eAgFnPc4d1	mapová
služby	služba	k1gFnPc4	služba
HERE	HERE	kA	HERE
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Nokia	Nokia	kA	Nokia
z	z	k7c2	z
telefonů	telefon	k1gInPc2	telefon
<g/>
,	,	kIx,	,
produkovaných	produkovaný	k2eAgInPc2d1	produkovaný
společností	společnost	k1gFnSc7	společnost
Microsoft	Microsoft	kA	Microsoft
<g/>
,	,	kIx,	,
nadobro	nadobro	k6eAd1	nadobro
zmizel	zmizet	k5eAaPmAgMnS	zmizet
-	-	kIx~	-
nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jen	jen	k9	jen
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
Lumia	Lumia	k1gFnSc1	Lumia
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
ve	v	k7c6	v
Windows	Windows	kA	Windows
Phone	Phon	k1gInSc5	Phon
Store	Stor	k1gInSc5	Stor
se	se	k3xPyFc4	se
změnila	změnit	k5eAaPmAgFnS	změnit
kategorie	kategorie	k1gFnSc1	kategorie
Nokia	Nokia	kA	Nokia
collection	collection	k1gInSc1	collection
na	na	k7c4	na
Lumia	Lumius	k1gMnSc4	Lumius
collection	collection	k1gInSc4	collection
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Technické	technický	k2eAgInPc1d1	technický
manuály	manuál	k1gInPc1	manuál
pro	pro	k7c4	pro
vývojáře	vývojář	k1gMnSc4	vývojář
a	a	k8xC	a
články	článek	k1gInPc4	článek
různých	různý	k2eAgInPc2d1	různý
časopisů	časopis	k1gInPc2	časopis
Microsoftu	Microsoft	k1gInSc2	Microsoft
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
Microsoft	Microsoft	kA	Microsoft
Systems	Systemsa	k1gFnPc2	Systemsa
Journal	Journal	k1gFnPc2	Journal
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
přístupny	přístupen	k2eAgInPc1d1	přístupen
přes	přes	k7c4	přes
síť	síť	k1gFnSc4	síť
MSDN	MSDN	kA	MSDN
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
také	také	k9	také
nabízí	nabízet	k5eAaImIp3nS	nabízet
předplatné	předplatný	k2eAgNnSc1d1	předplatné
pro	pro	k7c4	pro
společnosti	společnost	k1gFnPc4	společnost
nebo	nebo	k8xC	nebo
osoby	osoba	k1gFnPc4	osoba
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
ty	ty	k3xPp2nSc1	ty
dražší	drahý	k2eAgFnSc1d2	dražší
nabízí	nabízet	k5eAaImIp3nS	nabízet
také	také	k9	také
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
nevydaným	vydaný	k2eNgFnPc3d1	nevydaná
betaverzím	betaverze	k1gFnPc3	betaverze
softwaru	software	k1gInSc2	software
od	od	k7c2	od
Microsoftu	Microsoft	k1gInSc2	Microsoft
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2004	[number]	k4	2004
spustil	spustit	k5eAaPmAgInS	spustit
Microsoft	Microsoft	kA	Microsoft
komunitní	komunitní	k2eAgFnSc4d1	komunitní
stránku	stránka	k1gFnSc4	stránka
pro	pro	k7c4	pro
vývojáře	vývojář	k1gMnSc4	vývojář
a	a	k8xC	a
uživatele	uživatel	k1gMnSc4	uživatel
<g/>
,	,	kIx,	,
zvanou	zvaný	k2eAgFnSc4d1	zvaná
Channel	Channel	k1gMnSc1	Channel
9	[number]	k4	9
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
wiki	wike	k1gFnSc4	wike
a	a	k8xC	a
internetovou	internetový	k2eAgFnSc4d1	internetová
diskusi	diskuse	k1gFnSc4	diskuse
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
komunitní	komunitní	k2eAgFnSc1d1	komunitní
stránka	stránka	k1gFnSc1	stránka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
každodenní	každodenní	k2eAgInPc4d1	každodenní
videocasty	videocast	k1gInPc4	videocast
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
služby	služba	k1gFnPc4	služba
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgFnSc1d1	zvaná
On	on	k3xPp3gMnSc1	on
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
spuštěna	spustit	k5eAaPmNgFnS	spustit
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Technická	technický	k2eAgFnSc1d1	technická
podpora	podpora	k1gFnSc1	podpora
zdarma	zdarma	k6eAd1	zdarma
je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
dostupná	dostupný	k2eAgFnSc1d1	dostupná
přes	přes	k7c4	přes
Usenetové	Usenetový	k2eAgFnPc4d1	Usenetový
skupiny	skupina	k1gFnPc4	skupina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
monitorují	monitorovat	k5eAaImIp3nP	monitorovat
zaměstnanci	zaměstnanec	k1gMnSc3	zaměstnanec
Microsoftu	Microsoft	k1gMnSc3	Microsoft
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
každý	každý	k3xTgInSc4	každý
produkt	produkt	k1gInSc4	produkt
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vztahovat	vztahovat	k5eAaImF	vztahovat
několik	několik	k4yIc4	několik
takových	takový	k3xDgFnPc2	takový
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
pomáhající	pomáhající	k2eAgMnPc1d1	pomáhající
lidé	člověk	k1gMnPc1	člověk
pak	pak	k6eAd1	pak
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
oceněni	oceněn	k2eAgMnPc1d1	oceněn
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
Microsoftu	Microsoft	k1gInSc2	Microsoft
statusem	status	k1gInSc7	status
Microsoft	Microsoft	kA	Microsoft
Most	most	k1gInSc1	most
Valuable	Valuable	k1gMnSc2	Valuable
Professional	Professional	k1gMnSc2	Professional
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jim	on	k3xPp3gMnPc3	on
kromě	kromě	k7c2	kromě
speciálního	speciální	k2eAgNnSc2d1	speciální
sociálního	sociální	k2eAgNnSc2d1	sociální
postavení	postavení	k1gNnSc2	postavení
dává	dávat	k5eAaImIp3nS	dávat
také	také	k9	také
možnost	možnost	k1gFnSc1	možnost
výhry	výhra	k1gFnSc2	výhra
ocenění	ocenění	k1gNnSc4	ocenění
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
výhody	výhoda	k1gFnPc4	výhoda
<g/>
.	.	kIx.	.
</s>
<s>
Microsoft	Microsoft	kA	Microsoft
je	být	k5eAaImIp3nS	být
také	také	k9	také
známý	známý	k2eAgInSc1d1	známý
svým	svůj	k3xOyFgInSc7	svůj
interním	interní	k2eAgInSc7d1	interní
lexikonem	lexikon	k1gInSc7	lexikon
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
eating	eating	k1gInSc1	eating
our	our	k?	our
own	own	k?	own
dog	doga	k1gFnPc2	doga
food	food	k6eAd1	food
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
společnost	společnost	k1gFnSc1	společnost
se	se	k3xPyFc4	se
před	před	k7c7	před
vydáním	vydání	k1gNnSc7	vydání
snaží	snažit	k5eAaImIp3nP	snažit
otestovat	otestovat	k5eAaPmF	otestovat
programy	program	k1gInPc4	program
"	"	kIx"	"
<g/>
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
kůži	kůže	k1gFnSc4	kůže
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vlastní	vlastní	k2eAgFnSc6d1	vlastní
firmě	firma	k1gFnSc6	firma
a	a	k8xC	a
v	v	k7c6	v
opravdových	opravdový	k2eAgFnPc6d1	opravdová
situacích	situace	k1gFnPc6	situace
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
příkladem	příklad	k1gInSc7	příklad
firemního	firemní	k2eAgInSc2d1	firemní
slangu	slang	k1gInSc2	slang
je	být	k5eAaImIp3nS	být
zkratka	zkratka	k1gFnSc1	zkratka
FYIFV	FYIFV	kA	FYIFV
<g/>
,	,	kIx,	,
znamenající	znamenající	k2eAgMnSc1d1	znamenající
Fuck	Fuck	k1gMnSc1	Fuck
You	You	k1gMnSc1	You
<g/>
,	,	kIx,	,
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
(	(	kIx(	(
<g/>
Fully	Fulla	k1gMnSc2	Fulla
<g/>
)	)	kIx)	)
Vested	Vested	k1gInSc1	Vested
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
finanční	finanční	k2eAgFnSc4d1	finanční
nezávislost	nezávislost	k1gFnSc4	nezávislost
zaměstnance	zaměstnanec	k1gMnSc2	zaměstnanec
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
může	moct	k5eAaImIp3nS	moct
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
chodit	chodit	k5eAaImF	chodit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
chce	chtít	k5eAaImIp3nS	chtít
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
je	být	k5eAaImIp3nS	být
také	také	k9	také
známá	známý	k2eAgNnPc4d1	známé
originálním	originální	k2eAgInSc7d1	originální
přístupem	přístup	k1gInSc7	přístup
k	k	k7c3	k
přijímání	přijímání	k1gNnSc3	přijímání
nových	nový	k2eAgMnPc2d1	nový
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
jiné	jiný	k2eAgFnPc1d1	jiná
společnosti	společnost	k1gFnPc1	společnost
napodobují	napodobovat	k5eAaImIp3nP	napodobovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
zvaný	zvaný	k2eAgInSc1d1	zvaný
Microsoft	Microsoft	kA	Microsoft
interview	interview	k1gInSc1	interview
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
například	například	k6eAd1	například
i	i	k9	i
(	(	kIx(	(
<g/>
zdánlivě	zdánlivě	k6eAd1	zdánlivě
<g/>
)	)	kIx)	)
nepodstatné	podstatný	k2eNgFnPc1d1	nepodstatná
otázky	otázka	k1gFnPc1	otázka
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
"	"	kIx"	"
<g/>
Proč	proč	k6eAd1	proč
jsou	být	k5eAaImIp3nP	být
poklopy	poklop	k1gInPc1	poklop
od	od	k7c2	od
kanálů	kanál	k1gInPc2	kanál
(	(	kIx(	(
<g/>
a	a	k8xC	a
podobných	podobný	k2eAgFnPc2d1	podobná
děr	děra	k1gFnPc2	děra
<g/>
)	)	kIx)	)
kulaté	kulatý	k2eAgFnPc1d1	kulatá
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
Microsoft	Microsoft	kA	Microsoft
je	být	k5eAaImIp3nS	být
upřímným	upřímný	k2eAgMnSc7d1	upřímný
odpůrcem	odpůrce	k1gMnSc7	odpůrce
limitů	limit	k1gInPc2	limit
amerického	americký	k2eAgNnSc2d1	americké
víza	vízo	k1gNnSc2	vízo
H-	H-	k1gFnSc2	H-
<g/>
1	[number]	k4	1
<g/>
B	B	kA	B
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
omezuje	omezovat	k5eAaImIp3nS	omezovat
společnosti	společnost	k1gFnPc4	společnost
v	v	k7c6	v
najímání	najímání	k1gNnSc6	najímání
určitých	určitý	k2eAgMnPc2d1	určitý
pracovníků	pracovník	k1gMnPc2	pracovník
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Bill	Bill	k1gMnSc1	Bill
Gates	Gates	k1gMnSc1	Gates
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
limit	limit	k1gInSc1	limit
ztěžuje	ztěžovat	k5eAaImIp3nS	ztěžovat
společnostem	společnost	k1gFnPc3	společnost
najmout	najmout	k5eAaPmF	najmout
cizí	cizí	k2eAgMnPc4d1	cizí
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc2	on
zbavil	zbavit	k5eAaPmAgMnS	zbavit
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Obhájci	obhájce	k1gMnPc1	obhájce
víza	vízo	k1gNnSc2	vízo
H-1B	H-1B	k1gFnSc2	H-1B
mu	on	k3xPp3gMnSc3	on
ale	ale	k9	ale
oponují	oponovat	k5eAaImIp3nP	oponovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zahraniční	zahraniční	k2eAgMnPc1d1	zahraniční
pracovníci	pracovník	k1gMnPc1	pracovník
berou	brát	k5eAaImIp3nP	brát
práci	práce	k1gFnSc3	práce
americkým	americký	k2eAgMnPc3d1	americký
občanům	občan	k1gMnPc3	občan
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
levné	levný	k2eAgFnSc2d1	levná
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
pracovní	pracovní	k2eAgFnSc2d1	pracovní
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
řídí	řídit	k5eAaImIp3nS	řídit
představenstvo	představenstvo	k1gNnSc4	představenstvo
většinově	většinově	k6eAd1	většinově
z	z	k7c2	z
nečlenů	nečlen	k1gMnPc2	nečlen
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
akciových	akciový	k2eAgFnPc2d1	akciová
společností	společnost	k1gFnPc2	společnost
obvyklé	obvyklý	k2eAgFnPc1d1	obvyklá
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
červnu	červen	k1gInSc3	červen
2010	[number]	k4	2010
tam	tam	k6eAd1	tam
patří	patřit	k5eAaImIp3nS	patřit
Steve	Steve	k1gMnSc1	Steve
Ballmer	Ballmer	k1gMnSc1	Ballmer
<g/>
,	,	kIx,	,
Bill	Bill	k1gMnSc1	Bill
Gates	Gates	k1gMnSc1	Gates
<g/>
,	,	kIx,	,
Dina	Dina	k1gMnSc1	Dina
Dublon	Dublon	k1gInSc1	Dublon
a	a	k8xC	a
Raymond	Raymond	k1gMnSc1	Raymond
Gilmartin	Gilmartin	k1gMnSc1	Gilmartin
z	z	k7c2	z
Harvardovy	Harvardův	k2eAgFnSc2d1	Harvardova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
bostonský	bostonský	k2eAgMnSc1d1	bostonský
podnikatel	podnikatel	k1gMnSc1	podnikatel
Reed	Reeda	k1gFnPc2	Reeda
Hastings	Hastings	k1gInSc1	Hastings
<g/>
,	,	kIx,	,
Maria	Maria	k1gFnSc1	Maria
Klawe	Klawe	k1gFnSc1	Klawe
z	z	k7c2	z
Harvey	Harvea	k1gFnSc2	Harvea
Mudd	Mudda	k1gFnPc2	Mudda
College	College	k1gInSc1	College
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
firmy	firma	k1gFnSc2	firma
August	August	k1gMnSc1	August
Capital	Capital	k1gMnSc1	Capital
David	David	k1gMnSc1	David
Marquardt	Marquardt	k1gMnSc1	Marquardt
<g/>
,	,	kIx,	,
CFO	CFO	kA	CFO
Bank	bank	k1gInSc1	bank
of	of	k?	of
America	Americ	k1gInSc2	Americ
Charles	Charles	k1gMnSc1	Charles
Noski	Noski	k1gNnPc2	Noski
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
prezident	prezident	k1gMnSc1	prezident
BMW	BMW	kA	BMW
Helmut	Helmut	k1gMnSc1	Helmut
Panke	Pank	k1gInSc2	Pank
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
představenstva	představenstvo	k1gNnSc2	představenstvo
jsou	být	k5eAaImIp3nP	být
voleni	volen	k2eAgMnPc1d1	volen
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
na	na	k7c4	na
setkání	setkání	k1gNnPc4	setkání
akcionářů	akcionář	k1gMnPc2	akcionář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
představenstvu	představenstvo	k1gNnSc6	představenstvo
je	být	k5eAaImIp3nS	být
pět	pět	k4xCc1	pět
komisařů	komisař	k1gMnPc2	komisař
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
dohlíží	dohlížet	k5eAaImIp3nP	dohlížet
na	na	k7c4	na
specifické	specifický	k2eAgFnPc4d1	specifická
záležitosti	záležitost	k1gFnPc4	záležitost
–	–	k?	–
kontrolní	kontrolní	k2eAgMnSc1d1	kontrolní
komisař	komisař	k1gMnSc1	komisař
<g/>
,	,	kIx,	,
dohlížející	dohlížející	k2eAgMnPc1d1	dohlížející
na	na	k7c4	na
účetní	účetní	k2eAgFnPc4d1	účetní
záležitosti	záležitost	k1gFnPc4	záležitost
a	a	k8xC	a
jejich	jejich	k3xOp3gNnPc4	jejich
hlášení	hlášení	k1gNnPc4	hlášení
<g/>
,	,	kIx,	,
kompenzační	kompenzační	k2eAgMnSc1d1	kompenzační
komisař	komisař	k1gMnSc1	komisař
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
schvaluje	schvalovat	k5eAaImIp3nS	schvalovat
výplaty	výplata	k1gFnPc4	výplata
zaměstnancům	zaměstnanec	k1gMnPc3	zaměstnanec
<g/>
,	,	kIx,	,
finanční	finanční	k2eAgMnSc1d1	finanční
komisař	komisař	k1gMnSc1	komisař
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
stará	starat	k5eAaImIp3nS	starat
o	o	k7c4	o
finanční	finanční	k2eAgFnPc4d1	finanční
záležitosti	záležitost	k1gFnPc4	záležitost
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
sloučení	sloučení	k1gNnSc1	sloučení
nebo	nebo	k8xC	nebo
koupě	koupě	k1gFnSc1	koupě
<g/>
,	,	kIx,	,
dozorčí	dozorčí	k1gMnSc1	dozorčí
a	a	k8xC	a
nominovací	nominovací	k2eAgMnSc1d1	nominovací
komisař	komisař	k1gMnSc1	komisař
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
kromě	kromě	k7c2	kromě
jiného	jiný	k1gMnSc2	jiný
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
práci	práce	k1gFnSc4	práce
nominaci	nominace	k1gFnSc4	nominace
do	do	k7c2	do
představenstva	představenstvo	k1gNnSc2	představenstvo
<g/>
,	,	kIx,	,
a	a	k8xC	a
antimonopolní	antimonopolní	k2eAgMnSc1d1	antimonopolní
komisař	komisař	k1gMnSc1	komisař
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
zabránit	zabránit	k5eAaPmF	zabránit
společnosti	společnost	k1gFnPc4	společnost
v	v	k7c6	v
porušování	porušování	k1gNnSc6	porušování
antimonopolních	antimonopolní	k2eAgInPc2d1	antimonopolní
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
Microsoft	Microsoft	kA	Microsoft
stal	stát	k5eAaPmAgMnS	stát
akciovou	akciový	k2eAgFnSc7d1	akciová
společností	společnost	k1gFnSc7	společnost
a	a	k8xC	a
spustil	spustit	k5eAaPmAgMnS	spustit
svůj	svůj	k3xOyFgInSc4	svůj
initial	initial	k1gInSc4	initial
public	publicum	k1gNnPc2	publicum
offering	offering	k1gInSc1	offering
(	(	kIx(	(
<g/>
IPO	IPO	kA	IPO
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
počáteční	počáteční	k2eAgFnSc1d1	počáteční
cena	cena	k1gFnSc1	cena
akcií	akcie	k1gFnPc2	akcie
byla	být	k5eAaImAgFnS	být
21	[number]	k4	21
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
skončila	skončit	k5eAaPmAgFnS	skončit
na	na	k7c6	na
27,75	[number]	k4	27,75
dolarech	dolar	k1gInPc6	dolar
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
červenci	červenec	k1gInSc3	červenec
2010	[number]	k4	2010
by	by	k9	by
kvůli	kvůli	k7c3	kvůli
devíti	devět	k4xCc3	devět
dělením	dělení	k1gNnSc7	dělení
stála	stát	k5eAaImAgFnS	stát
jedna	jeden	k4xCgFnSc1	jeden
akci	akce	k1gFnSc3	akce
z	z	k7c2	z
IPO	IPO	kA	IPO
devět	devět	k4xCc4	devět
centů	cent	k1gInPc2	cent
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
akcií	akcie	k1gFnPc2	akcie
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
svého	svůj	k3xOyFgInSc2	svůj
vrcholu	vrchol	k1gInSc2	vrchol
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
na	na	k7c6	na
119	[number]	k4	119
dolarech	dolar	k1gInPc6	dolar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2003	[number]	k4	2003
začala	začít	k5eAaPmAgFnS	začít
společnost	společnost	k1gFnSc4	společnost
nabízet	nabízet	k5eAaImF	nabízet
dividenda	dividenda	k1gFnSc1	dividenda
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
hodnota	hodnota	k1gFnSc1	hodnota
stále	stále	k6eAd1	stále
stoupá	stoupat	k5eAaImIp3nS	stoupat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
cena	cena	k1gFnSc1	cena
akcií	akcie	k1gFnPc2	akcie
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
stálá	stálý	k2eAgFnSc1d1	stálá
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
obchodních	obchodní	k2eAgFnPc2d1	obchodní
strategií	strategie	k1gFnPc2	strategie
Microsoftu	Microsoft	k1gInSc2	Microsoft
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
emabrace	emabrace	k1gFnSc1	emabrace
<g/>
,	,	kIx,	,
extend	extend	k1gInSc1	extend
and	and	k?	and
extinguish	extinguish	k1gInSc1	extinguish
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
soustředí	soustředit	k5eAaPmIp3nS	soustředit
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
produktu	produkt	k1gInSc2	produkt
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
ho	on	k3xPp3gMnSc4	on
vydá	vydat	k5eAaPmIp3nS	vydat
na	na	k7c4	na
veřejnost	veřejnost	k1gFnSc4	veřejnost
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vyskytnout	vyskytnout	k5eAaPmF	vyskytnout
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
kompatibilitou	kompatibilita	k1gFnSc7	kompatibilita
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
tak	tak	k6eAd1	tak
vyřadí	vyřadit	k5eAaPmIp3nP	vyřadit
ostatní	ostatní	k2eAgMnPc1d1	ostatní
konkurenci	konkurence	k1gFnSc4	konkurence
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
nemůže	moct	k5eNaImIp3nS	moct
používat	používat	k5eAaImF	používat
nejnovější	nový	k2eAgFnSc4d3	nejnovější
verzi	verze	k1gFnSc4	verze
od	od	k7c2	od
Microsoftu	Microsoft	k1gInSc2	Microsoft
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgFnPc1d1	různá
společnosti	společnost	k1gFnPc1	společnost
nebo	nebo	k8xC	nebo
také	také	k9	také
vlády	vláda	k1gFnPc1	vláda
Microsoft	Microsoft	kA	Microsoft
za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
taktiku	taktika	k1gFnSc4	taktika
žalují	žalovat	k5eAaImIp3nP	žalovat
<g/>
,	,	kIx,	,
čemuž	což	k3yQnSc3	což
se	se	k3xPyFc4	se
Microsoft	Microsoft	kA	Microsoft
brání	bránit	k5eAaImIp3nP	bránit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
anti-konkurenční	antionkurenční	k2eAgFnSc4d1	anti-konkurenční
taktiku	taktika	k1gFnSc4	taktika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
způsob	způsob	k1gInSc4	způsob
doručování	doručování	k1gNnSc2	doručování
zákazníkům	zákazník	k1gMnPc3	zákazník
přesně	přesně	k6eAd1	přesně
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
<g/>
.	.	kIx.	.
</s>
<s>
Žebříčky	žebříček	k1gInPc1	žebříček
společností	společnost	k1gFnPc2	společnost
Standard	standard	k1gInSc1	standard
&	&	k?	&
Poor	Poor	k1gInSc1	Poor
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
a	a	k8xC	a
Moody	Mood	k1gInPc4	Mood
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
oba	dva	k4xCgMnPc1	dva
zařadily	zařadit	k5eAaPmAgInP	zařadit
Microsoft	Microsoft	kA	Microsoft
s	s	k7c7	s
hodnocením	hodnocení	k1gNnSc7	hodnocení
AAA	AAA	kA	AAA
mezi	mezi	k7c7	mezi
nejstabilnější	stabilní	k2eAgFnSc7d3	nejstabilnější
a	a	k8xC	a
nejspolehlivější	spolehlivý	k2eAgFnSc2d3	nejspolehlivější
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Majetek	majetek	k1gInSc1	majetek
Microsoftu	Microsoft	k1gInSc2	Microsoft
byl	být	k5eAaImAgInS	být
přitom	přitom	k6eAd1	přitom
odhadnut	odhadnout	k5eAaPmNgInS	odhadnout
na	na	k7c4	na
41	[number]	k4	41
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jeho	jeho	k3xOp3gInPc4	jeho
nekryté	krytý	k2eNgInPc4d1	nekrytý
dluhy	dluh	k1gInPc4	dluh
pouze	pouze	k6eAd1	pouze
8,5	[number]	k4	8,5
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
<g/>
,	,	kIx,	,
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
vydala	vydat	k5eAaPmAgFnS	vydat
společnost	společnost	k1gFnSc1	společnost
dluhopis	dluhopis	k1gInSc1	dluhopis
s	s	k7c7	s
hodnotou	hodnota	k1gFnSc7	hodnota
až	až	k9	až
2,5	[number]	k4	2,5
milionu	milion	k4xCgInSc2	milion
dolarů	dolar	k1gInPc2	dolar
s	s	k7c7	s
relativně	relativně	k6eAd1	relativně
nízkými	nízký	k2eAgInPc7d1	nízký
úroky	úrok	k1gInPc7	úrok
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
se	s	k7c7	s
státními	státní	k2eAgInPc7d1	státní
dluhopisy	dluhopis	k1gInPc7	dluhopis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
čtvrtletí	čtvrtletí	k1gNnSc6	čtvrtletí
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
poprvé	poprvé	k6eAd1	poprvé
po	po	k7c6	po
dvaceti	dvacet	k4xCc6	dvacet
letech	let	k1gInPc6	let
překonal	překonat	k5eAaPmAgMnS	překonat
Apple	Apple	kA	Apple
Microsoft	Microsoft	kA	Microsoft
jak	jak	k8xC	jak
v	v	k7c6	v
profitech	profit	k1gInPc6	profit
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
obratu	obrat	k1gInSc3	obrat
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
snížení	snížení	k1gNnSc3	snížení
poptávky	poptávka	k1gFnSc2	poptávka
o	o	k7c4	o
počítače	počítač	k1gInPc4	počítač
a	a	k8xC	a
velkým	velký	k2eAgFnPc3d1	velká
ztrátám	ztráta	k1gFnPc3	ztráta
divize	divize	k1gFnSc2	divize
online	onlinout	k5eAaPmIp3nS	onlinout
služeb	služba	k1gFnPc2	služba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
provozuje	provozovat	k5eAaImIp3nS	provozovat
také	také	k9	také
vyhledávač	vyhledávač	k1gMnSc1	vyhledávač
Bing	bingo	k1gNnPc2	bingo
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
profit	profit	k1gInSc1	profit
Microsoftu	Microsoft	k1gInSc2	Microsoft
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
5,2	[number]	k4	5,2
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
a	a	k8xC	a
obrat	obrat	k1gInSc1	obrat
14,5	[number]	k4	14,5
miliard	miliarda	k4xCgFnPc2	miliarda
<g/>
,	,	kIx,	,
profit	profit	k1gInSc1	profit
Applu	Appl	k1gInSc2	Appl
byl	být	k5eAaImAgInS	být
6	[number]	k4	6
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
a	a	k8xC	a
obrat	obrat	k1gInSc1	obrat
24,7	[number]	k4	24,7
miliard	miliarda	k4xCgFnPc2	miliarda
<g/>
.	.	kIx.	.
</s>
<s>
Divize	divize	k1gFnSc1	divize
online	onlinout	k5eAaPmIp3nS	onlinout
služeb	služba	k1gFnPc2	služba
je	být	k5eAaImIp3nS	být
prodělečná	prodělečný	k2eAgFnSc1d1	prodělečná
už	už	k6eAd1	už
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
a	a	k8xC	a
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
čtvrtletí	čtvrtletí	k1gNnSc6	čtvrtletí
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
prodělala	prodělat	k5eAaPmAgFnS	prodělat
hned	hned	k6eAd1	hned
726	[number]	k4	726
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
2010	[number]	k4	2010
toto	tento	k3xDgNnSc1	tento
číslo	číslo	k1gNnSc1	číslo
činilo	činit	k5eAaImAgNnS	činit
2,5	[number]	k4	2,5
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Organizace	organizace	k1gFnSc1	organizace
Greenpeace	Greenpeace	k1gFnSc2	Greenpeace
umístila	umístit	k5eAaPmAgFnS	umístit
Microsoft	Microsoft	kA	Microsoft
na	na	k7c4	na
sedmnácté	sedmnáctý	k4xOgNnSc4	sedmnáctý
místo	místo	k1gNnSc4	místo
při	při	k7c6	při
porovnávání	porovnávání	k1gNnSc6	porovnávání
osmnácti	osmnáct	k4xCc2	osmnáct
výrobců	výrobce	k1gMnPc2	výrobce
elektroniky	elektronika	k1gFnSc2	elektronika
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
toxickým	toxický	k2eAgFnPc3d1	toxická
chemikáliím	chemikálie	k1gFnPc3	chemikálie
<g/>
,	,	kIx,	,
recyklaci	recyklace	k1gFnSc3	recyklace
a	a	k8xC	a
globálnímu	globální	k2eAgNnSc3d1	globální
oteplování	oteplování	k1gNnSc3	oteplování
<g/>
.	.	kIx.	.
</s>
<s>
Microsoft	Microsoft	kA	Microsoft
plánuje	plánovat	k5eAaImIp3nS	plánovat
vyřadit	vyřadit	k5eAaPmF	vyřadit
bromované	bromovaný	k2eAgInPc4d1	bromovaný
zpomalovače	zpomalovač	k1gInPc4	zpomalovač
hoření	hoření	k1gNnSc2	hoření
(	(	kIx(	(
<g/>
BFR	BFR	kA	BFR
<g/>
)	)	kIx)	)
a	a	k8xC	a
ftaláty	ftalát	k1gInPc1	ftalát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ještě	ještě	k9	ještě
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vyřadí	vyřadit	k5eAaPmIp3nS	vyřadit
polyvynilchlorid	polyvynilchlorid	k1gInSc1	polyvynilchlorid
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
BFR	BFR	kA	BFR
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
každý	každý	k3xTgInSc4	každý
jeho	jeho	k3xOp3gInSc4	jeho
výrobek	výrobek	k1gInSc4	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
americký	americký	k2eAgInSc1d1	americký
kampus	kampus	k1gInSc1	kampus
Microsoftu	Microsoft	k1gInSc2	Microsoft
dostal	dostat	k5eAaPmAgInS	dostat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
stříbrné	stříbrná	k1gFnSc2	stříbrná
ocenění	ocenění	k1gNnPc2	ocenění
od	od	k7c2	od
programu	program	k1gInSc2	program
LEED	LEED	kA	LEED
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
instaloval	instalovat	k5eAaBmAgMnS	instalovat
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
kampusu	kampus	k1gInSc6	kampus
v	v	k7c6	v
Silikonovém	silikonový	k2eAgNnSc6d1	silikonové
údolí	údolí	k1gNnSc6	údolí
dva	dva	k4xCgInPc1	dva
tisíce	tisíc	k4xCgInPc1	tisíc
solárních	solární	k2eAgMnPc2d1	solární
panelů	panel	k1gInPc2	panel
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vygenerují	vygenerovat	k5eAaPmIp3nP	vygenerovat
až	až	k9	až
15	[number]	k4	15
procent	procento	k1gNnPc2	procento
elektřiny	elektřina	k1gFnSc2	elektřina
pro	pro	k7c4	pro
všechna	všechen	k3xTgNnPc4	všechen
zázemí	zázemí	k1gNnPc4	zázemí
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
využívá	využívat	k5eAaImIp3nS	využívat
alternativních	alternativní	k2eAgInPc2d1	alternativní
zdrojů	zdroj	k1gInPc2	zdroj
v	v	k7c6	v
dopravě	doprava	k1gFnSc6	doprava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
soukromých	soukromý	k2eAgInPc2d1	soukromý
autobusových	autobusový	k2eAgInPc2d1	autobusový
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc1d1	zvaný
Connector	Connector	k1gInSc1	Connector
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
dopravu	doprava	k1gFnSc4	doprava
na	na	k7c6	na
kampusech	kampus	k1gInPc6	kampus
pak	pak	k6eAd1	pak
využívá	využívat	k5eAaPmIp3nS	využívat
Shuttle	Shuttle	k1gFnSc1	Shuttle
Connect	Connectum	k1gNnPc2	Connectum
<g/>
,	,	kIx,	,
velkou	velký	k2eAgFnSc4d1	velká
flotilu	flotila	k1gFnSc4	flotila
hybridních	hybridní	k2eAgInPc2d1	hybridní
automobilů	automobil	k1gInPc2	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
účelem	účel	k1gInSc7	účel
motivace	motivace	k1gFnSc2	motivace
Microsoft	Microsoft	kA	Microsoft
podporuje	podporovat	k5eAaImIp3nS	podporovat
také	také	k9	také
regionální	regionální	k2eAgFnSc4d1	regionální
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2010	[number]	k4	2010
ale	ale	k8xC	ale
společnost	společnost	k1gFnSc1	společnost
zaujala	zaujmout	k5eAaPmAgFnS	zaujmout
negativní	negativní	k2eAgInSc4d1	negativní
postoj	postoj	k1gInSc4	postoj
vůči	vůči	k7c3	vůči
přidání	přidání	k1gNnSc3	přidání
pruhů	pruh	k1gInPc2	pruh
pro	pro	k7c4	pro
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
dopravu	doprava	k1gFnSc4	doprava
a	a	k8xC	a
dopravní	dopravní	k2eAgInPc4d1	dopravní
prostředky	prostředek	k1gInPc4	prostředek
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
počtem	počet	k1gInSc7	počet
míst	místo	k1gNnPc2	místo
na	na	k7c4	na
most	most	k1gInSc4	most
spojující	spojující	k2eAgInSc1d1	spojující
Redmond	Redmond	k1gInSc1	Redmond
a	a	k8xC	a
Seattle	Seattle	k1gFnSc1	Seattle
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
brzo	brzo	k6eAd1	brzo
nahradí	nahradit	k5eAaPmIp3nS	nahradit
Evergreen	evergreen	k1gInSc1	evergreen
Point	pointa	k1gFnPc2	pointa
Floating	Floating	k1gInSc1	Floating
Bridge	Bridg	k1gInSc2	Bridg
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
nechce	chtít	k5eNaImIp3nS	chtít
dále	daleko	k6eAd2	daleko
zdržovat	zdržovat	k5eAaImF	zdržovat
konstrukci	konstrukce	k1gFnSc4	konstrukce
mostu	most	k1gInSc2	most
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
Microsoft	Microsoft	kA	Microsoft
pověřil	pověřit	k5eAaPmAgMnS	pověřit
výzkumné	výzkumný	k2eAgFnSc2d1	výzkumná
firmy	firma	k1gFnSc2	firma
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
provedly	provést	k5eAaPmAgFnP	provést
nezávislé	závislý	k2eNgFnPc1d1	nezávislá
studie	studie	k1gFnPc1	studie
porovnávající	porovnávající	k2eAgInPc4d1	porovnávající
celkové	celkový	k2eAgInPc4d1	celkový
náklady	náklad	k1gInPc4	náklad
na	na	k7c6	na
pořízení	pořízení	k1gNnSc6	pořízení
a	a	k8xC	a
držení	držení	k1gNnSc6	držení
(	(	kIx(	(
<g/>
TCO	TCO	kA	TCO
<g/>
)	)	kIx)	)
systémů	systém	k1gInPc2	systém
Windows	Windows	kA	Windows
Server	server	k1gInSc4	server
2003	[number]	k4	2003
a	a	k8xC	a
Linux	Linux	kA	Linux
<g/>
.	.	kIx.	.
</s>
<s>
Firmy	firma	k1gFnPc1	firma
zjistily	zjistit	k5eAaPmAgFnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Windows	Windows	kA	Windows
je	být	k5eAaImIp3nS	být
snadněji	snadno	k6eAd2	snadno
spravovatelný	spravovatelný	k2eAgMnSc1d1	spravovatelný
než	než	k8xS	než
Linux	linux	k1gInSc1	linux
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
používající	používající	k2eAgMnPc1d1	používající
Windows	Windows	kA	Windows
mohou	moct	k5eAaImIp3nP	moct
spravovat	spravovat	k5eAaImF	spravovat
rychleji	rychle	k6eAd2	rychle
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vyústí	vyústit	k5eAaPmIp3nS	vyústit
v	v	k7c4	v
nižší	nízký	k2eAgFnPc4d2	nižší
ceny	cena	k1gFnPc4	cena
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gFnSc4	jejich
společnost	společnost	k1gFnSc4	společnost
<g/>
,	,	kIx,	,
např.	např.	kA	např.
nižší	nízký	k2eAgFnSc2d2	nižší
TCO	TCO	kA	TCO
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
studie	studie	k1gFnSc1	studie
začala	začít	k5eAaPmAgFnS	začít
vlnu	vlna	k1gFnSc4	vlna
podobných	podobný	k2eAgInPc2d1	podobný
výzkumů	výzkum	k1gInPc2	výzkum
<g/>
,	,	kIx,	,
Yankee	yankee	k1gMnSc1	yankee
Group	Group	k1gMnSc1	Group
například	například	k6eAd1	například
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
přepnutí	přepnutí	k1gNnSc1	přepnutí
ze	z	k7c2	z
starší	starý	k2eAgFnSc2d2	starší
verze	verze	k1gFnSc2	verze
Windows	Windows	kA	Windows
Serveru	server	k1gInSc2	server
na	na	k7c4	na
novější	nový	k2eAgFnSc4d2	novější
stojí	stát	k5eAaImIp3nS	stát
pouze	pouze	k6eAd1	pouze
zlomek	zlomek	k1gInSc4	zlomek
ceny	cena	k1gFnSc2	cena
přepnutí	přepnutí	k1gNnSc2	přepnutí
z	z	k7c2	z
Windows	Windows	kA	Windows
Serveru	server	k1gInSc2	server
na	na	k7c6	na
Linux	Linux	kA	Linux
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
zúčastněné	zúčastněný	k2eAgFnSc2d1	zúčastněná
společnosti	společnost	k1gFnSc2	společnost
zajímala	zajímat	k5eAaImAgFnS	zajímat
větší	veliký	k2eAgFnSc4d2	veliký
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
a	a	k8xC	a
spolehlivost	spolehlivost	k1gFnSc4	spolehlivost
Linux	Linux	kA	Linux
serverů	server	k1gInPc2	server
a	a	k8xC	a
obávaly	obávat	k5eAaImAgFnP	obávat
nutnosti	nutnost	k1gFnPc1	nutnost
používat	používat	k5eAaImF	používat
pouze	pouze	k6eAd1	pouze
výrobky	výrobek	k1gInPc4	výrobek
od	od	k7c2	od
Microsoftu	Microsoft	k1gInSc2	Microsoft
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
studie	studie	k1gFnSc1	studie
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
provedla	provést	k5eAaPmAgFnS	provést
organizace	organizace	k1gFnSc1	organizace
OSDL	OSDL	kA	OSDL
<g/>
,	,	kIx,	,
tvrdila	tvrdit	k5eAaImAgFnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
studie	studie	k1gFnSc1	studie
Microsoftu	Microsoft	k1gInSc2	Microsoft
byla	být	k5eAaImAgFnS	být
už	už	k6eAd1	už
prošlá	prošlý	k2eAgFnSc1d1	prošlá
a	a	k8xC	a
jednostranná	jednostranný	k2eAgFnSc1d1	jednostranná
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
Linux	linux	k1gInSc1	linux
má	mít	k5eAaImIp3nS	mít
nižší	nízký	k2eAgMnSc1d2	nižší
TCO	TCO	kA	TCO
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
administrátoři	administrátor	k1gMnPc1	administrátor
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
spravují	spravovat	k5eAaImIp3nP	spravovat
průměrně	průměrně	k6eAd1	průměrně
více	hodně	k6eAd2	hodně
serverů	server	k1gInPc2	server
apod.	apod.	kA	apod.
Jako	jako	k8xC	jako
část	část	k1gFnSc1	část
kampaně	kampaň	k1gFnSc2	kampaň
Get	Get	k1gFnSc1	Get
the	the	k?	the
Facts	Factsa	k1gFnPc2	Factsa
společnost	společnost	k1gFnSc1	společnost
upozornila	upozornit	k5eAaPmAgFnS	upozornit
<g/>
,	,	kIx,	,
že	že	k8xS	že
obchodovací	obchodovací	k2eAgFnSc1d1	obchodovací
platforma	platforma	k1gFnSc1	platforma
.	.	kIx.	.
<g/>
NET	NET	kA	NET
byla	být	k5eAaImAgFnS	být
vyvinuta	vyvinout	k5eAaPmNgFnS	vyvinout
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Accenture	Accentur	k1gMnSc5	Accentur
pro	pro	k7c4	pro
London	London	k1gMnSc1	London
Stock	Stock	k1gInSc4	Stock
Exchange	Exchang	k1gInSc2	Exchang
<g/>
,	,	kIx,	,
a	a	k8xC	a
tvrdila	tvrdit	k5eAaImAgFnS	tvrdit
že	že	k8xS	že
platforma	platforma	k1gFnSc1	platforma
nabízí	nabízet	k5eAaImIp3nS	nabízet
vysokou	vysoký	k2eAgFnSc4d1	vysoká
spolehlivost	spolehlivost	k1gFnSc4	spolehlivost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dlouhém	dlouhý	k2eAgInSc6d1	dlouhý
prostoji	prostoj	k1gInSc6	prostoj
a	a	k8xC	a
nespolehlivosti	nespolehlivost	k1gFnSc6	nespolehlivost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
londýnská	londýnský	k2eAgFnSc1d1	londýnská
burza	burza	k1gFnSc1	burza
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
další	další	k2eAgInSc1d1	další
rok	rok	k1gInSc1	rok
přepne	přepnout	k5eAaPmIp3nS	přepnout
z	z	k7c2	z
Microsoftu	Microsoft	k1gInSc2	Microsoft
na	na	k7c4	na
Linux	linux	k1gInSc4	linux
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
Microsoft	Microsoft	kA	Microsoft
převzal	převzít	k5eAaPmAgMnS	převzít
tzv.	tzv.	kA	tzv.
Pac-Manovské	Pac-Manovský	k2eAgNnSc1d1	Pac-Manovský
logo	logo	k1gNnSc1	logo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Scott	Scott	k1gMnSc1	Scott
Baker	Baker	k1gMnSc1	Baker
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
prohlašoval	prohlašovat	k5eAaImAgMnS	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
řez	řez	k1gInSc1	řez
mezi	mezi	k7c7	mezi
písmeny	písmeno	k1gNnPc7	písmeno
o	o	k7c4	o
a	a	k8xC	a
s	s	k7c7	s
zvýrazňuje	zvýrazňovat	k5eAaImIp3nS	zvýrazňovat
část	část	k1gFnSc4	část
jména	jméno	k1gNnSc2	jméno
soft	soft	k?	soft
a	a	k8xC	a
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
pohyb	pohyb	k1gInSc4	pohyb
a	a	k8xC	a
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Dave	Dav	k1gInSc5	Dav
Norris	Norris	k1gInSc1	Norris
poté	poté	k6eAd1	poté
zahájil	zahájit	k5eAaPmAgInS	zahájit
interní	interní	k2eAgFnSc4d1	interní
kampaň	kampaň	k1gFnSc4	kampaň
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgFnPc4	který
chtěl	chtít	k5eAaImAgMnS	chtít
obhájit	obhájit	k5eAaPmF	obhájit
staré	starý	k2eAgNnSc4d1	staré
logo	logo	k1gNnSc4	logo
a	a	k8xC	a
nabízel	nabízet	k5eAaImAgInS	nabízet
jeho	jeho	k3xOp3gFnSc4	jeho
zelenou	zelený	k2eAgFnSc4d1	zelená
podobu	podoba	k1gFnSc4	podoba
s	s	k7c7	s
pestrým	pestrý	k2eAgNnSc7d1	pestré
písmenem	písmeno	k1gNnSc7	písmeno
o	o	k7c6	o
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ale	ale	k9	ale
byla	být	k5eAaImAgFnS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc4d1	další
logo	logo	k1gNnSc4	logo
<g/>
,	,	kIx,	,
se	s	k7c7	s
sloganem	slogan	k1gInSc7	slogan
Váš	váš	k3xOp2gInSc4	váš
potenciál	potenciál	k1gInSc1	potenciál
<g/>
,	,	kIx,	,
naše	náš	k3xOp1gFnSc1	náš
vášeň	vášeň	k1gFnSc1	vášeň
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
sloganu	slogan	k1gInSc6	slogan
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
společnost	společnost	k1gFnSc1	společnost
používala	používat	k5eAaImAgFnS	používat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Logo	logo	k1gNnSc1	logo
ale	ale	k8xC	ale
spustila	spustit	k5eAaPmAgFnS	spustit
v	v	k7c6	v
USA	USA	kA	USA
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
novou	nový	k2eAgFnSc7d1	nová
reklamní	reklamní	k2eAgFnSc7d1	reklamní
kampaní	kampaň	k1gFnSc7	kampaň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nový	nový	k2eAgInSc1d1	nový
slogan	slogan	k1gInSc1	slogan
nahradil	nahradit	k5eAaPmAgInS	nahradit
hlášku	hláška	k1gFnSc4	hláška
Kam	kam	k6eAd1	kam
chcete	chtít	k5eAaImIp2nP	chtít
dnes	dnes	k6eAd1	dnes
<g/>
?	?	kIx.	?
</s>
<s>
Při	při	k7c6	při
soukromé	soukromý	k2eAgFnSc6d1	soukromá
konferenci	konference	k1gFnSc6	konference
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
společnost	společnost	k1gFnSc1	společnost
odhalila	odhalit	k5eAaPmAgFnS	odhalit
nový	nový	k2eAgInSc4d1	nový
slogan	slogan	k1gInSc4	slogan
Buď	buď	k8xC	buď
co	co	k3yQnSc1	co
je	být	k5eAaImIp3nS	být
příští	příští	k2eAgInSc4d1	příští
a	a	k8xC	a
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
plánuje	plánovat	k5eAaImIp3nS	plánovat
nové	nový	k2eAgNnSc1d1	nové
<g/>
.	.	kIx.	.
</s>
<s>
Microsoft	Microsoft	kA	Microsoft
nabízí	nabízet	k5eAaImIp3nS	nabízet
velmi	velmi	k6eAd1	velmi
širokou	široký	k2eAgFnSc4d1	široká
škálu	škála	k1gFnSc4	škála
programového	programový	k2eAgNnSc2d1	programové
vybavení	vybavení	k1gNnSc2	vybavení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
široce	široko	k6eAd1	široko
používáno	používat	k5eAaImNgNnS	používat
<g/>
:	:	kIx,	:
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
–	–	k?	–
klíčovým	klíčový	k2eAgInSc7d1	klíčový
produktem	produkt	k1gInSc7	produkt
jsou	být	k5eAaImIp3nP	být
graficky	graficky	k6eAd1	graficky
orientované	orientovaný	k2eAgInPc4d1	orientovaný
operační	operační	k2eAgInPc4d1	operační
systémy	systém	k1gInPc4	systém
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
dodávány	dodávat	k5eAaImNgInP	dodávat
pod	pod	k7c7	pod
značkou	značka	k1gFnSc7	značka
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
existujících	existující	k2eAgFnPc2d1	existující
verzí	verze	k1gFnPc2	verze
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
nedávna	nedávno	k1gNnSc2	nedávno
nejrozšířenější	rozšířený	k2eAgInPc4d3	nejrozšířenější
Windows	Windows	kA	Windows
XP	XP	kA	XP
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
2006	[number]	k4	2006
<g/>
/	/	kIx~	/
<g/>
2007	[number]	k4	2007
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
verze	verze	k1gFnSc1	verze
Windows	Windows	kA	Windows
Vista	vista	k2eAgFnSc1d1	vista
(	(	kIx(	(
<g/>
multilicenční	multilicenční	k2eAgFnSc1d1	multilicenční
anglická	anglický	k2eAgFnSc1d1	anglická
verze	verze	k1gFnSc1	verze
pro	pro	k7c4	pro
velké	velký	k2eAgMnPc4d1	velký
zákazníky	zákazník	k1gMnPc4	zákazník
vyšla	vyjít	k5eAaPmAgFnS	vyjít
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
krabicová	krabicový	k2eAgFnSc1d1	krabicová
<g/>
"	"	kIx"	"
verze	verze	k1gFnSc1	verze
byla	být	k5eAaImAgFnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
vydána	vydán	k2eAgFnSc1d1	vydána
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2007	[number]	k4	2007
<g/>
;	;	kIx,	;
česká	český	k2eAgFnSc1d1	Česká
pak	pak	k6eAd1	pak
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
Windows	Windows	kA	Windows
7	[number]	k4	7
v	v	k7c6	v
edicích	edice	k1gFnPc6	edice
starter	startrum	k1gNnPc2	startrum
(	(	kIx(	(
<g/>
neprodává	prodávat	k5eNaImIp3nS	prodávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
na	na	k7c6	na
nových	nový	k2eAgInPc6d1	nový
počítačích	počítač	k1gInPc6	počítač
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Home	Home	k1gNnSc1	Home
basic	basice	k1gFnPc2	basice
<g/>
,	,	kIx,	,
Home	Hom	k1gFnPc1	Hom
premium	premium	k1gNnSc1	premium
<g/>
,	,	kIx,	,
Business	business	k1gInSc1	business
<g/>
,	,	kIx,	,
Ultimate	Ultimat	k1gInSc5	Ultimat
<g/>
,	,	kIx,	,
Windows	Windows	kA	Windows
7	[number]	k4	7
byla	být	k5eAaImAgFnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
vydána	vydán	k2eAgFnSc1d1	vydána
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
Server	server	k1gInSc1	server
–	–	k?	–
serverová	serverový	k2eAgFnSc1d1	serverová
řada	řada	k1gFnSc1	řada
Windows	Windows	kA	Windows
založená	založený	k2eAgFnSc1d1	založená
Windows	Windows	kA	Windows
NT	NT	kA	NT
3.5	[number]	k4	3.5
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dnes	dnes	k6eAd1	dnes
existuje	existovat	k5eAaImIp3nS	existovat
jako	jako	k9	jako
samostatná	samostatný	k2eAgFnSc1d1	samostatná
řada	řada	k1gFnSc1	řada
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
Windows	Windows	kA	Windows
Server	server	k1gInSc1	server
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
nejrozšířenější	rozšířený	k2eAgInSc4d3	nejrozšířenější
Windows	Windows	kA	Windows
Server	server	k1gInSc4	server
2003	[number]	k4	2003
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
je	on	k3xPp3gMnPc4	on
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
nový	nový	k2eAgInSc1d1	nový
systém	systém	k1gInSc1	systém
Windows	Windows	kA	Windows
Server	server	k1gInSc4	server
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
Mobile	mobile	k1gNnPc2	mobile
–	–	k?	–
speciální	speciální	k2eAgFnSc1d1	speciální
upravená	upravený	k2eAgFnSc1d1	upravená
verze	verze	k1gFnSc1	verze
Windows	Windows	kA	Windows
pro	pro	k7c4	pro
přenosná	přenosný	k2eAgNnPc4d1	přenosné
zařízení	zařízení	k1gNnPc4	zařízení
většinou	většinou	k6eAd1	většinou
s	s	k7c7	s
procesorem	procesor	k1gInSc7	procesor
ARM	ARM	kA	ARM
koncipovaná	koncipovaný	k2eAgFnSc1d1	koncipovaná
jako	jako	k9	jako
chytrý	chytrý	k2eAgInSc4d1	chytrý
telefon	telefon	k1gInSc4	telefon
(	(	kIx(	(
<g/>
Smartphone	Smartphon	k1gMnSc5	Smartphon
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
PDA	PDA	kA	PDA
<g/>
.	.	kIx.	.
</s>
<s>
Windows	Windows	kA	Windows
Mobile	mobile	k1gNnSc1	mobile
bývají	bývat	k5eAaImIp3nP	bývat
nasazovány	nasazován	k2eAgInPc1d1	nasazován
zejména	zejména	k9	zejména
do	do	k7c2	do
zařízení	zařízení	k1gNnSc2	zařízení
malých	malý	k2eAgInPc2d1	malý
rozměrů	rozměr	k1gInPc2	rozměr
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
dlaně	dlaň	k1gFnSc2	dlaň
lidské	lidský	k2eAgFnSc2d1	lidská
ruky	ruka	k1gFnSc2	ruka
(	(	kIx(	(
<g/>
kapesní	kapesní	k2eAgInSc1d1	kapesní
přístroj	přístroj	k1gInSc1	přístroj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
Embedded	Embedded	k1gMnSc1	Embedded
–	–	k?	–
řada	řada	k1gFnSc1	řada
Windows	Windows	kA	Windows
vycházející	vycházející	k2eAgInPc4d1	vycházející
ze	z	k7c2	z
systémů	systém	k1gInPc2	systém
Windows	Windows	kA	Windows
CE	CE	kA	CE
–	–	k?	–
systém	systém	k1gInSc1	systém
pro	pro	k7c4	pro
středně	středně	k6eAd1	středně
výkonný	výkonný	k2eAgInSc4d1	výkonný
HW	HW	kA	HW
pro	pro	k7c4	pro
ovládání	ovládání	k1gNnSc4	ovládání
zařízení	zařízení	k1gNnPc2	zařízení
nebo	nebo	k8xC	nebo
do	do	k7c2	do
přístrojů	přístroj	k1gInPc2	přístroj
pro	pro	k7c4	pro
sběr	sběr	k1gInSc4	sběr
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
Windows	Windows	kA	Windows
XP	XP	kA	XP
Embedded	Embedded	k1gMnSc1	Embedded
/	/	kIx~	/
Windows	Windows	kA	Windows
2000	[number]	k4	2000
Embedded	Embedded	k1gInSc4	Embedded
–	–	k?	–
speciální	speciální	k2eAgFnSc1d1	speciální
řada	řada	k1gFnSc1	řada
Windows	Windows	kA	Windows
upravena	upravit	k5eAaPmNgFnS	upravit
do	do	k7c2	do
modulární	modulární	k2eAgFnSc2d1	modulární
podoby	podoba	k1gFnSc2	podoba
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
výběru	výběr	k1gInSc2	výběr
komponent	komponenta	k1gFnPc2	komponenta
a	a	k8xC	a
ovladačů	ovladač	k1gInPc2	ovladač
<g/>
.	.	kIx.	.
</s>
<s>
Microsoft	Microsoft	kA	Microsoft
Office	Office	kA	Office
je	být	k5eAaImIp3nS	být
sada	sada	k1gFnSc1	sada
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
,	,	kIx,	,
určených	určený	k2eAgFnPc2d1	určená
pro	pro	k7c4	pro
kancelářskou	kancelářský	k2eAgFnSc4d1	kancelářská
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Sada	sada	k1gFnSc1	sada
Microsoft	Microsoft	kA	Microsoft
Office	Office	kA	Office
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
<g/>
:	:	kIx,	:
Microsoft	Microsoft	kA	Microsoft
Word	Word	kA	Word
–	–	k?	–
textový	textový	k2eAgInSc4d1	textový
editor	editor	k1gInSc4	editor
<g/>
.	.	kIx.	.
</s>
<s>
Microsoft	Microsoft	kA	Microsoft
Excel	Excel	kA	Excel
–	–	k?	–
tabulkový	tabulkový	k2eAgInSc4d1	tabulkový
kalkulátor	kalkulátor	k1gInSc4	kalkulátor
<g/>
.	.	kIx.	.
</s>
<s>
Microsoft	Microsoft	kA	Microsoft
PowerPoint	PowerPoint	k1gInSc1	PowerPoint
–	–	k?	–
program	program	k1gInSc1	program
pro	pro	k7c4	pro
vytváření	vytváření	k1gNnSc4	vytváření
prezentací	prezentace	k1gFnPc2	prezentace
<g/>
.	.	kIx.	.
</s>
<s>
Microsoft	Microsoft	kA	Microsoft
Outlook	Outlook	k1gInSc1	Outlook
–	–	k?	–
program	program	k1gInSc1	program
pro	pro	k7c4	pro
správu	správa	k1gFnSc4	správa
kontaktů	kontakt	k1gInPc2	kontakt
<g/>
,	,	kIx,	,
e-mailů	eail	k1gInPc2	e-mail
<g/>
,	,	kIx,	,
kalendáře	kalendář	k1gInSc2	kalendář
a	a	k8xC	a
úkolů	úkol	k1gInPc2	úkol
<g/>
.	.	kIx.	.
</s>
<s>
Microsoft	Microsoft	kA	Microsoft
Access	Access	k1gInSc1	Access
–	–	k?	–
program	program	k1gInSc1	program
pro	pro	k7c4	pro
správu	správa	k1gFnSc4	správa
databází	databáze	k1gFnPc2	databáze
<g/>
.	.	kIx.	.
</s>
<s>
Microsoft	Microsoft	kA	Microsoft
Publisher	Publishra	k1gFnPc2	Publishra
–	–	k?	–
textový	textový	k2eAgInSc4d1	textový
a	a	k8xC	a
grafický	grafický	k2eAgInSc4d1	grafický
editor	editor	k1gInSc4	editor
<g/>
.	.	kIx.	.
</s>
<s>
Microsoft	Microsoft	kA	Microsoft
OneNote	OneNot	k1gInSc5	OneNot
–	–	k?	–
poznámkový	poznámkový	k2eAgInSc4d1	poznámkový
blok	blok	k1gInSc4	blok
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
Microsoft	Microsoft	kA	Microsoft
nabízí	nabízet	k5eAaImIp3nS	nabízet
1	[number]	k4	1
TB	TB	kA	TB
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
cloudovém	cloudový	k2eAgNnSc6d1	cloudový
úložišti	úložiště	k1gNnSc6	úložiště
OneDrive	OneDriev	k1gFnSc2	OneDriev
a	a	k8xC	a
60	[number]	k4	60
minut	minuta	k1gFnPc2	minuta
volání	volání	k1gNnSc4	volání
na	na	k7c4	na
mobilní	mobilní	k2eAgInPc4d1	mobilní
telefony	telefon	k1gInPc4	telefon
přes	přes	k7c4	přes
službu	služba	k1gFnSc4	služba
Skype	Skyp	k1gInSc5	Skyp
<g/>
.	.	kIx.	.
</s>
<s>
Microsoft	Microsoft	kA	Microsoft
Works	Works	kA	Works
–	–	k?	–
jednodušší	jednoduchý	k2eAgFnSc1d2	jednodušší
varianta	varianta	k1gFnSc1	varianta
sady	sada	k1gFnSc2	sada
Office	Office	kA	Office
pro	pro	k7c4	pro
domácnosti	domácnost	k1gFnPc4	domácnost
<g/>
,	,	kIx,	,
prodává	prodávat	k5eAaImIp3nS	prodávat
se	se	k3xPyFc4	se
prakticky	prakticky	k6eAd1	prakticky
jen	jen	k9	jen
v	v	k7c6	v
OEM	OEM	kA	OEM
podobě	podoba	k1gFnSc6	podoba
s	s	k7c7	s
novými	nový	k2eAgInPc7d1	nový
PC	PC	kA	PC
a	a	k8xC	a
notebooky	notebook	k1gInPc1	notebook
<g/>
.	.	kIx.	.
</s>
<s>
Windows	Windows	kA	Windows
Internet	Internet	k1gInSc1	Internet
Explorer	Explorer	k1gInSc1	Explorer
–	–	k?	–
webový	webový	k2eAgInSc1d1	webový
prohlížeč	prohlížeč	k1gInSc1	prohlížeč
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
známý	známý	k2eAgMnSc1d1	známý
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Microsoft	Microsoft	kA	Microsoft
Internet	Internet	k1gInSc1	Internet
Explorer	Explorer	k1gInSc1	Explorer
<g/>
.	.	kIx.	.
</s>
<s>
Microsoft	Microsoft	kA	Microsoft
Edge	Edg	k1gFnPc1	Edg
–	–	k?	–
nástupce	nástupce	k1gMnSc1	nástupce
webového	webový	k2eAgInSc2d1	webový
prohlížeče	prohlížeč	k1gInSc2	prohlížeč
Internet	Internet	k1gInSc1	Internet
Explorer	Explorer	k1gInSc1	Explorer
<g/>
.	.	kIx.	.
</s>
<s>
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
Mail	mail	k1gInSc1	mail
–	–	k?	–
jednoduchý	jednoduchý	k2eAgMnSc1d1	jednoduchý
poštovní	poštovní	k2eAgMnSc1d1	poštovní
klient	klient	k1gMnSc1	klient
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xC	jako
Outlook	Outlook	k1gInSc1	Outlook
Express	express	k1gInSc1	express
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Windows	Windows	kA	Windows
98	[number]	k4	98
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
systému	systém	k1gInSc2	systém
Windows	Windows	kA	Windows
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
upgrade	upgrade	k1gInSc4	upgrade
s	s	k7c7	s
prohlížečem	prohlížeč	k1gInSc7	prohlížeč
Internet	Internet	k1gInSc1	Internet
Explorer	Explorra	k1gFnPc2	Explorra
<g/>
.	.	kIx.	.
</s>
<s>
Microsoft	Microsoft	kA	Microsoft
FrontPage	FrontPage	k1gNnPc2	FrontPage
–	–	k?	–
aplikace	aplikace	k1gFnSc2	aplikace
pro	pro	k7c4	pro
vytváření	vytváření	k1gNnSc4	vytváření
webových	webový	k2eAgFnPc2d1	webová
stránek	stránka	k1gFnPc2	stránka
v	v	k7c6	v
HTML	HTML	kA	HTML
(	(	kIx(	(
<g/>
verze	verze	k1gFnSc1	verze
Office	Office	kA	Office
2007	[number]	k4	2007
ji	on	k3xPp3gFnSc4	on
již	již	k6eAd1	již
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
aplikací	aplikace	k1gFnSc7	aplikace
Microsoft	Microsoft	kA	Microsoft
SharePoint	SharePoint	k1gMnSc1	SharePoint
Designer	Designer	k1gMnSc1	Designer
nebo	nebo	k8xC	nebo
Microsoft	Microsoft	kA	Microsoft
Expression	Expression	k1gInSc1	Expression
Web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
Microsoft	Microsoft	kA	Microsoft
Expression	Expression	k1gInSc1	Expression
Web	web	k1gInSc1	web
–	–	k?	–
program	program	k1gInSc1	program
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
vývojáře	vývojář	k1gMnSc4	vývojář
a	a	k8xC	a
webdesignery	webdesigner	k1gMnPc7	webdesigner
webových	webový	k2eAgFnPc2d1	webová
aplikací	aplikace	k1gFnPc2	aplikace
Microsoft	Microsoft	kA	Microsoft
Visual	Visual	k1gMnSc1	Visual
Studio	studio	k1gNnSc1	studio
–	–	k?	–
sada	sada	k1gFnSc1	sada
vývojových	vývojový	k2eAgInPc2d1	vývojový
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
vytváření	vytváření	k1gNnSc3	vytváření
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Aktuální	aktuální	k2eAgFnPc1d1	aktuální
verze	verze	k1gFnPc1	verze
těchto	tento	k3xDgInPc2	tento
nástrojů	nástroj	k1gInPc2	nástroj
je	být	k5eAaImIp3nS	být
nabízena	nabízet	k5eAaImNgFnS	nabízet
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Visual	Visual	k1gInSc1	Visual
Studio	studio	k1gNnSc1	studio
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Microsoft	Microsoft	kA	Microsoft
Dynamics	Dynamics	k1gInSc1	Dynamics
–	–	k?	–
řada	řada	k1gFnSc1	řada
integrovaných	integrovaný	k2eAgNnPc2d1	integrované
podnikových	podnikový	k2eAgNnPc2d1	podnikové
řešení	řešení	k1gNnPc2	řešení
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
také	také	k9	také
on-line	onin	k1gInSc5	on-lin
hostované	hostovaný	k2eAgInPc4d1	hostovaný
Microsoft	Microsoft	kA	Microsoft
AXAPTA	AXAPTA	kA	AXAPTA
<g/>
/	/	kIx~	/
<g/>
AX	AX	kA	AX
–	–	k?	–
podnikové	podnikový	k2eAgFnSc2d1	podniková
CRM	CRM	kA	CRM
včetně	včetně	k7c2	včetně
účetnictví	účetnictví	k1gNnSc2	účetnictví
<g/>
,	,	kIx,	,
skladu	sklad	k1gInSc2	sklad
apod.	apod.	kA	apod.
Microsoft	Microsoft	kA	Microsoft
Flow	Flow	k1gMnSc1	Flow
–	–	k?	–
online	onlinout	k5eAaPmIp3nS	onlinout
nástroj	nástroj	k1gInSc4	nástroj
pro	pro	k7c4	pro
automatické	automatický	k2eAgNnSc4d1	automatické
propojování	propojování	k1gNnSc4	propojování
různých	různý	k2eAgFnPc2d1	různá
služeb	služba	k1gFnPc2	služba
třetích	třetí	k4xOgFnPc2	třetí
stran	strana	k1gFnPc2	strana
pomocí	pomocí	k7c2	pomocí
API	API	kA	API
Původní	původní	k2eAgFnSc1d1	původní
sada	sada	k1gFnSc1	sada
binárních	binární	k2eAgInPc2d1	binární
formátů	formát	k1gInPc2	formát
Microsoft	Microsoft	kA	Microsoft
Office	Office	kA	Office
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
formátované	formátovaný	k2eAgInPc4d1	formátovaný
textové	textový	k2eAgInPc4d1	textový
dokumenty	dokument	k1gInPc4	dokument
.	.	kIx.	.
<g/>
DOC	doc	kA	doc
otevírané	otevíraný	k2eAgNnSc1d1	otevírané
v	v	k7c4	v
aplikaci	aplikace	k1gFnSc4	aplikace
Word	Word	kA	Word
<g/>
,	,	kIx,	,
tabulky	tabulka	k1gFnPc4	tabulka
XLS	XLS	kA	XLS
aplikace	aplikace	k1gFnSc2	aplikace
Excel	Excel	kA	Excel
a	a	k8xC	a
celoobrazovkové	celoobrazovkový	k2eAgFnSc2d1	celoobrazovková
prezentace	prezentace	k1gFnSc2	prezentace
aplikace	aplikace	k1gFnSc2	aplikace
PowerPoint	PowerPoint	k1gInSc1	PowerPoint
<g/>
.	.	kIx.	.
</s>
<s>
Sada	sada	k1gFnSc1	sada
formátů	formát	k1gInPc2	formát
Office	Office	kA	Office
Open	Open	k1gInSc1	Open
XML	XML	kA	XML
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
OOXML	OOXML	kA	OOXML
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
schválen	schválit	k5eAaPmNgInS	schválit
jako	jako	k8xC	jako
ISO	ISO	kA	ISO
<g/>
/	/	kIx~	/
<g/>
IEC	IEC	kA	IEC
29500	[number]	k4	29500
<g/>
:	:	kIx,	:
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
název	název	k1gInSc4	název
podobný	podobný	k2eAgInSc4d1	podobný
standardu	standard	k1gInSc2	standard
OpenPocument	OpenPocument	k1gInSc1	OpenPocument
(	(	kIx(	(
<g/>
ODF	ODF	kA	ODF
<g/>
)	)	kIx)	)
z	z	k7c2	z
dílny	dílna	k1gFnSc2	dílna
organizace	organizace	k1gFnSc2	organizace
OASIS	OASIS	kA	OASIS
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
schválen	schválit	k5eAaPmNgInS	schválit
jako	jako	k8xC	jako
ISO	ISO	kA	ISO
<g/>
/	/	kIx~	/
<g/>
IEC	IEC	kA	IEC
26300	[number]	k4	26300
<g/>
:	:	kIx,	:
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
OOXML	OOXML	kA	OOXML
je	být	k5eAaImIp3nS	být
dominantní	dominantní	k2eAgFnSc1d1	dominantní
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
programů	program	k1gInPc2	program
Microsoftu	Microsoft	k1gInSc2	Microsoft
(	(	kIx(	(
<g/>
výchozí	výchozí	k2eAgInSc1d1	výchozí
formát	formát	k1gInSc1	formát
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ODF	ODF	kA	ODF
je	být	k5eAaImIp3nS	být
výchozím	výchozí	k2eAgInSc7d1	výchozí
formátem	formát	k1gInSc7	formát
konkurenčních	konkurenční	k2eAgInPc2d1	konkurenční
kancelářských	kancelářský	k2eAgInPc2d1	kancelářský
balíků	balík	k1gInPc2	balík
OpenOffice	OpenOffice	k1gFnSc2	OpenOffice
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
<g/>
,	,	kIx,	,
Koffice	Koffice	k1gFnSc2	Koffice
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
OOXML	OOXML	kA	OOXML
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
formáty	formát	k1gInPc4	formát
textového	textový	k2eAgInSc2d1	textový
dokumentu	dokument	k1gInSc2	dokument
DOCX	DOCX	kA	DOCX
<g/>
,	,	kIx,	,
tabulkového	tabulkový	k2eAgInSc2d1	tabulkový
sešitu	sešit	k1gInSc2	sešit
XSLX	XSLX	kA	XSLX
a	a	k8xC	a
počítačové	počítačový	k2eAgFnSc2d1	počítačová
prezentace	prezentace	k1gFnSc2	prezentace
PPTX	PPTX	kA	PPTX
<g/>
.	.	kIx.	.
</s>
<s>
ANI	ani	k8xC	ani
–	–	k?	–
animovaný	animovaný	k2eAgInSc1d1	animovaný
kurzor	kurzor	k1gInSc1	kurzor
myši	myš	k1gFnSc2	myš
(	(	kIx(	(
<g/>
ukazatele	ukazatel	k1gInSc2	ukazatel
<g/>
)	)	kIx)	)
BMP	BMP	kA	BMP
–	–	k?	–
formát	formát	k1gInSc1	formát
rastrových	rastrový	k2eAgInPc2d1	rastrový
obrázků	obrázek	k1gInPc2	obrázek
Podrobnější	podrobný	k2eAgFnSc2d2	podrobnější
informace	informace	k1gFnSc2	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Microsoft	Microsoft	kA	Microsoft
Studios	Studios	k?	Studios
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
počítačové	počítačový	k2eAgFnPc1d1	počítačová
hry	hra	k1gFnPc1	hra
nejsou	být	k5eNaImIp3nP	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
stránkou	stránka	k1gFnSc7	stránka
Microsoftu	Microsoft	k1gInSc2	Microsoft
<g/>
,	,	kIx,	,
neznamená	znamenat	k5eNaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jimi	on	k3xPp3gInPc7	on
nezabývá	zabývat	k5eNaImIp3nS	zabývat
<g/>
.	.	kIx.	.
</s>
<s>
Microsoft	Microsoft	kA	Microsoft
vydal	vydat	k5eAaPmAgInS	vydat
několik	několik	k4yIc4	několik
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yIgFnPc4	který
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
Zoo	zoo	k1gFnSc1	zoo
Tycoon	Tycoon	k1gMnSc1	Tycoon
(	(	kIx(	(
<g/>
budovatelská	budovatelský	k2eAgFnSc1d1	budovatelská
strategie	strategie	k1gFnSc1	strategie
<g/>
)	)	kIx)	)
Age	Age	k1gFnSc1	Age
of	of	k?	of
Empires	Empires	k1gInSc1	Empires
(	(	kIx(	(
<g/>
historická	historický	k2eAgFnSc1d1	historická
realtimová	realtimový	k2eAgFnSc1d1	realtimová
strategie	strategie	k1gFnSc1	strategie
<g/>
)	)	kIx)	)
Rise	Rise	k1gInSc1	Rise
of	of	k?	of
Nations	Nations	k1gInSc1	Nations
(	(	kIx(	(
<g/>
strategická	strategický	k2eAgFnSc1d1	strategická
<g/>
)	)	kIx)	)
Flight	Flight	k1gMnSc1	Flight
Simulator	Simulator	k1gMnSc1	Simulator
Combat	Combat	k1gMnSc1	Combat
Flight	Flight	k1gMnSc1	Flight
Simulator	Simulator	k1gMnSc1	Simulator
Train	Train	k2eAgInSc1d1	Train
Simulator	Simulator	k1gInSc1	Simulator
HALO	halo	k1gNnSc1	halo
Gears	Gearsa	k1gFnPc2	Gearsa
of	of	k?	of
War	War	k1gFnPc2	War
<g/>
,	,	kIx,	,
Gears	Gearsa	k1gFnPc2	Gearsa
of	of	k?	of
War	War	k1gFnPc2	War
2	[number]	k4	2
<g/>
,	,	kIx,	,
Gears	Gears	k1gInSc1	Gears
of	of	k?	of
War	War	k1gFnSc1	War
3	[number]	k4	3
Microsoft	Microsoft	kA	Microsoft
tyto	tento	k3xDgFnPc4	tento
hry	hra	k1gFnPc4	hra
nevyvinul	vyvinout	k5eNaPmAgMnS	vyvinout
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
figuruje	figurovat	k5eAaImIp3nS	figurovat
jako	jako	k9	jako
jejich	jejich	k3xOp3gInSc4	jejich
vydavatel	vydavatel	k1gMnSc1	vydavatel
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
úspěchu	úspěch	k1gInSc2	úspěch
často	často	k6eAd1	často
vývojáře	vývojář	k1gMnSc2	vývojář
hry	hra	k1gFnSc2	hra
koupí	koupě	k1gFnPc2	koupě
<g/>
.	.	kIx.	.
</s>
<s>
Microsoft	Microsoft	kA	Microsoft
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
studentské	studentský	k2eAgFnPc4d1	studentská
slevy	sleva	k1gFnPc4	sleva
na	na	k7c4	na
některé	některý	k3yIgInPc4	některý
své	svůj	k3xOyFgInPc4	svůj
produkty	produkt	k1gInPc4	produkt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2011	[number]	k4	2011
na	na	k7c4	na
FDS	FDS	kA	FDS
(	(	kIx(	(
<g/>
Fusion	Fusion	k1gInSc1	Fusion
Developer	developer	k1gMnSc1	developer
Summit	summit	k1gInSc1	summit
<g/>
)	)	kIx)	)
oznámil	oznámit	k5eAaPmAgInS	oznámit
Microsoft	Microsoft	kA	Microsoft
započatý	započatý	k2eAgInSc4d1	započatý
vývoj	vývoj	k1gInSc4	vývoj
na	na	k7c6	na
vlastní	vlastní	k2eAgFnSc6d1	vlastní
GPGPU	GPGPU	kA	GPGPU
API	API	kA	API
pojmenovaném	pojmenovaný	k2eAgInSc6d1	pojmenovaný
C	C	kA	C
<g/>
++	++	k?	++
AMP	AMP	kA	AMP
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
++	++	k?	++
Accelerated	Accelerated	k1gMnSc1	Accelerated
Massive	Massiev	k1gFnSc2	Massiev
Parallelism	Parallelism	k1gMnSc1	Parallelism
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bude	být	k5eAaImBp3nS	být
součástí	součást	k1gFnSc7	součást
další	další	k2eAgFnSc2d1	další
verze	verze	k1gFnSc2	verze
Visual	Visual	k1gMnSc1	Visual
Studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
API	API	kA	API
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zjednodušilo	zjednodušit	k5eAaPmAgNnS	zjednodušit
programování	programování	k1gNnSc1	programování
pro	pro	k7c4	pro
GPGPU	GPGPU	kA	GPGPU
a	a	k8xC	a
začalo	začít	k5eAaPmAgNnS	začít
se	se	k3xPyFc4	se
používat	používat	k5eAaImF	používat
i	i	k9	i
u	u	k7c2	u
programů	program	k1gInPc2	program
s	s	k7c7	s
menším	malý	k2eAgInSc7d2	menší
počtem	počet	k1gInSc7	počet
vývojářů	vývojář	k1gMnPc2	vývojář
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
akceleraci	akcelerace	k1gFnSc3	akcelerace
bude	být	k5eAaImBp3nS	být
využívat	využívat	k5eAaPmF	využívat
API	API	kA	API
DirectX	DirectX	k1gFnPc4	DirectX
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
je	on	k3xPp3gNnSc4	on
Microsoft	Microsoft	kA	Microsoft
především	především	k6eAd1	především
softwarovou	softwarový	k2eAgFnSc7d1	softwarová
společností	společnost	k1gFnSc7	společnost
<g/>
,	,	kIx,	,
nabízí	nabízet	k5eAaImIp3nS	nabízet
rovněž	rovněž	k9	rovněž
řadu	řada	k1gFnSc4	řada
produktů	produkt	k1gInPc2	produkt
z	z	k7c2	z
kategorie	kategorie	k1gFnSc2	kategorie
hardware	hardware	k1gInSc1	hardware
<g/>
.	.	kIx.	.
</s>
<s>
Produkuje	produkovat	k5eAaImIp3nS	produkovat
vlastní	vlastní	k2eAgNnSc1d1	vlastní
příslušenství	příslušenství	k1gNnSc1	příslušenství
k	k	k7c3	k
počítačům	počítač	k1gInPc3	počítač
<g/>
,	,	kIx,	,
řadu	řada	k1gFnSc4	řada
tabletů	tablet	k1gInPc2	tablet
Surface	Surface	k1gFnSc2	Surface
<g/>
,	,	kIx,	,
odkoupil	odkoupit	k5eAaPmAgInS	odkoupit
výrobu	výroba	k1gFnSc4	výroba
telefonů	telefon	k1gInPc2	telefon
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
Nokia	Nokia	kA	Nokia
a	a	k8xC	a
v	v	k7c6	v
nedávné	dávný	k2eNgFnSc6d1	nedávná
době	doba	k1gFnSc6	doba
představil	představit	k5eAaPmAgInS	představit
vlastní	vlastní	k2eAgInSc1d1	vlastní
chytrý	chytrý	k2eAgInSc1d1	chytrý
náramek	náramek	k1gInSc1	náramek
<g/>
:	:	kIx,	:
Microsoft	Microsoft	kA	Microsoft
Band	band	k1gInSc1	band
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Microsoft	Microsoft	kA	Microsoft
Surface	Surface	k1gFnPc4	Surface
<g/>
.	.	kIx.	.
</s>
<s>
Microsoft	Microsoft	kA	Microsoft
Surface	Surface	k1gFnSc1	Surface
je	být	k5eAaImIp3nS	být
značka	značka	k1gFnSc1	značka
tabletů	tablet	k1gInPc2	tablet
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
Microsoft	Microsoft	kA	Microsoft
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
Surface	Surface	k1gFnSc1	Surface
2	[number]	k4	2
s	s	k7c7	s
10	[number]	k4	10
<g/>
'	'	kIx"	'
displejem	displej	k1gInSc7	displej
a	a	k8xC	a
Windows	Windows	kA	Windows
RT	RT	kA	RT
<g/>
,	,	kIx,	,
Surface	Surface	k1gFnSc1	Surface
2	[number]	k4	2
Pro	pro	k7c4	pro
<g/>
,	,	kIx,	,
taktéž	taktéž	k?	taktéž
s	s	k7c7	s
10	[number]	k4	10
<g/>
'	'	kIx"	'
displejem	displej	k1gInSc7	displej
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
Windows	Windows	kA	Windows
8.1	[number]	k4	8.1
a	a	k8xC	a
Surface	Surface	k1gFnSc1	Surface
3	[number]	k4	3
Pro	pro	k7c4	pro
s	s	k7c7	s
12	[number]	k4	12
<g/>
'	'	kIx"	'
displejem	displej	k1gInSc7	displej
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
s	s	k7c7	s
Windows	Windows	kA	Windows
8.1	[number]	k4	8.1
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
dražší	drahý	k2eAgNnPc4d2	dražší
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
u	u	k7c2	u
Surfacu	Surfacus	k1gInSc2	Surfacus
3	[number]	k4	3
Pro	pro	k7c4	pro
ale	ale	k9	ale
Microsoft	Microsoft	kA	Microsoft
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
plnohodnotnou	plnohodnotný	k2eAgFnSc4d1	plnohodnotná
náhradu	náhrada	k1gFnSc4	náhrada
přenosného	přenosný	k2eAgInSc2d1	přenosný
počítače	počítač	k1gInSc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Divizi	divize	k1gFnSc3	divize
Devices	Devices	k1gMnSc1	Devices
and	and	k?	and
Services	Services	k1gInSc1	Services
odkoupil	odkoupit	k5eAaPmAgInS	odkoupit
Microsoft	Microsoft	kA	Microsoft
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
Nokia	Nokia	kA	Nokia
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Staví	stavit	k5eAaBmIp3nS	stavit
na	na	k7c6	na
základech	základ	k1gInPc6	základ
kdysi	kdysi	k6eAd1	kdysi
největšího	veliký	k2eAgMnSc4d3	veliký
výrobce	výrobce	k1gMnSc4	výrobce
telefonů	telefon	k1gInPc2	telefon
<g/>
,	,	kIx,	,
u	u	k7c2	u
základních	základní	k2eAgInPc2d1	základní
modelů	model	k1gInPc2	model
si	se	k3xPyFc3	se
dokonce	dokonce	k9	dokonce
ponechává	ponechávat	k5eAaImIp3nS	ponechávat
jeho	jeho	k3xOp3gFnSc4	jeho
značku	značka	k1gFnSc4	značka
<g/>
,	,	kIx,	,
telefony	telefon	k1gInPc1	telefon
Lumia	Lumium	k1gNnSc2	Lumium
ale	ale	k8xC	ale
ponesou	nést	k5eAaImIp3nP	nést
logo	logo	k1gNnSc4	logo
společnosti	společnost	k1gFnSc2	společnost
Microsoft	Microsoft	kA	Microsoft
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
nadále	nadále	k6eAd1	nadále
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
klasické	klasický	k2eAgInPc4d1	klasický
tlačítkové	tlačítkový	k2eAgInPc4d1	tlačítkový
telefony	telefon	k1gInPc4	telefon
<g/>
,	,	kIx,	,
telefony	telefon	k1gInPc4	telefon
Asha	Ash	k1gInSc2	Ash
s	s	k7c7	s
platformou	platforma	k1gFnSc7	platforma
Series	Series	k1gMnSc1	Series
40	[number]	k4	40
a	a	k8xC	a
telefony	telefon	k1gInPc7	telefon
Lumia	Lumium	k1gNnSc2	Lumium
s	s	k7c7	s
operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
Windows	Windows	kA	Windows
Phone	Phon	k1gMnSc5	Phon
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Microsoft	Microsoft	kA	Microsoft
Band	banda	k1gFnPc2	banda
<g/>
.	.	kIx.	.
</s>
<s>
Chytrý	chytrý	k2eAgInSc1d1	chytrý
náramek	náramek	k1gInSc1	náramek
Microsoft	Microsoft	kA	Microsoft
Band	band	k1gInSc1	band
<g/>
,	,	kIx,	,
postavený	postavený	k2eAgInSc1d1	postavený
na	na	k7c6	na
cloudové	cloudový	k2eAgFnSc6d1	cloudová
platformě	platforma	k1gFnSc6	platforma
Microsoft	Microsoft	kA	Microsoft
Health	Health	k1gInSc1	Health
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
představen	představit	k5eAaPmNgInS	představit
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
o	o	k7c4	o
zařízení	zařízení	k1gNnPc4	zařízení
určené	určený	k2eAgFnPc1d1	určená
ke	k	k7c3	k
sledování	sledování	k1gNnSc3	sledování
zdravotních	zdravotní	k2eAgFnPc2d1	zdravotní
funkcí	funkce	k1gFnPc2	funkce
ve	v	k7c6	v
sportu	sport	k1gInSc6	sport
nebo	nebo	k8xC	nebo
ke	k	k7c3	k
sledování	sledování	k1gNnSc3	sledování
polohy	poloha	k1gFnSc2	poloha
<g/>
,	,	kIx,	,
náramek	náramek	k1gInSc4	náramek
ale	ale	k8xC	ale
také	také	k9	také
umí	umět	k5eAaImIp3nS	umět
zobrazovat	zobrazovat	k5eAaImF	zobrazovat
upozornění	upozornění	k1gNnSc4	upozornění
z	z	k7c2	z
telefonu	telefon	k1gInSc2	telefon
<g/>
,	,	kIx,	,
odpovídat	odpovídat	k5eAaImF	odpovídat
na	na	k7c4	na
textové	textový	k2eAgFnPc4d1	textová
zprávy	zpráva	k1gFnPc4	zpráva
nebo	nebo	k8xC	nebo
odmítat	odmítat	k5eAaImF	odmítat
příchozí	příchozí	k1gMnPc4	příchozí
hovory	hovor	k1gInPc7	hovor
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
jej	on	k3xPp3gMnSc4	on
také	také	k9	také
ovládat	ovládat	k5eAaImF	ovládat
pomocí	pomocí	k7c2	pomocí
hlasové	hlasový	k2eAgFnSc2d1	hlasová
asistentky	asistentka	k1gFnSc2	asistentka
Cortana	Cortana	k1gFnSc1	Cortana
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
velkou	velký	k2eAgFnSc7d1	velká
výhodou	výhoda	k1gFnSc7	výhoda
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
multiplatformnost	multiplatformnost	k1gFnSc1	multiplatformnost
<g/>
.	.	kIx.	.
</s>
<s>
Spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
se	s	k7c7	s
systémy	systém	k1gInPc7	systém
Windows	Windows	kA	Windows
Phone	Phon	k1gMnSc5	Phon
<g/>
,	,	kIx,	,
iOS	iOS	k?	iOS
i	i	k8xC	i
Android	android	k1gInSc1	android
<g/>
.	.	kIx.	.
</s>
<s>
Platforma	platforma	k1gFnSc1	platforma
Health	Healtha	k1gFnPc2	Healtha
navíc	navíc	k6eAd1	navíc
umí	umět	k5eAaImIp3nS	umět
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
hodinkami	hodinka	k1gFnPc7	hodinka
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
Apple	Apple	kA	Apple
Watch	Watcha	k1gFnPc2	Watcha
nebo	nebo	k8xC	nebo
FitBit	FitBita	k1gFnPc2	FitBita
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Xbox	Xbox	k1gInSc1	Xbox
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
herní	herní	k2eAgFnSc1d1	herní
konzole	konzola	k1gFnSc6	konzola
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jako	jako	k9	jako
reakce	reakce	k1gFnSc1	reakce
na	na	k7c4	na
PlayStation	PlayStation	k1gInSc4	PlayStation
2	[number]	k4	2
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
opravdovým	opravdový	k2eAgMnSc7d1	opravdový
konkurentem	konkurent	k1gMnSc7	konkurent
pro	pro	k7c4	pro
společnost	společnost	k1gFnSc4	společnost
Sony	Sony	kA	Sony
je	být	k5eAaImIp3nS	být
až	až	k9	až
nejnovější	nový	k2eAgFnSc1d3	nejnovější
generace	generace	k1gFnSc1	generace
výrobku	výrobek	k1gInSc2	výrobek
<g/>
,	,	kIx,	,
Xbox	Xbox	k1gInSc1	Xbox
One	One	k1gFnSc2	One
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
hardwarem	hardware	k1gInSc7	hardware
firmy	firma	k1gFnSc2	firma
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
hudební	hudební	k2eAgInSc1d1	hudební
přehrávač	přehrávač	k1gInSc1	přehrávač
Zune	Zun	k1gInSc2	Zun
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
také	také	k9	také
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
klávesnice	klávesnice	k1gFnSc1	klávesnice
<g/>
,	,	kIx,	,
myši	myš	k1gFnPc1	myš
nebo	nebo	k8xC	nebo
webové	webový	k2eAgFnPc1d1	webová
kamery	kamera	k1gFnPc1	kamera
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
mají	mít	k5eAaImIp3nP	mít
stěžejní	stěžejní	k2eAgInPc1d1	stěžejní
produkty	produkt	k1gInPc1	produkt
Microsoftu	Microsoft	k1gInSc2	Microsoft
–	–	k?	–
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
a	a	k8xC	a
Microsoft	Microsoft	kA	Microsoft
Office	Office	kA	Office
–	–	k?	–
výrazně	výrazně	k6eAd1	výrazně
většinový	většinový	k2eAgInSc4d1	většinový
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
Propojováním	propojování	k1gNnSc7	propojování
dalších	další	k2eAgInPc2d1	další
svých	svůj	k3xOyFgFnPc2	svůj
aplikací	aplikace	k1gFnPc2	aplikace
k	k	k7c3	k
těmto	tento	k3xDgInPc3	tento
produktům	produkt	k1gInPc3	produkt
se	se	k3xPyFc4	se
Microsoft	Microsoft	kA	Microsoft
někdy	někdy	k6eAd1	někdy
dopouští	dopouštět	k5eAaImIp3nS	dopouštět
chování	chování	k1gNnSc1	chování
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
poškozuje	poškozovat	k5eAaImIp3nS	poškozovat
konkurenční	konkurenční	k2eAgNnSc4d1	konkurenční
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
byl	být	k5eAaImAgMnS	být
několikrát	několikrát	k6eAd1	několikrát
žalován	žalován	k2eAgMnSc1d1	žalován
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
shledán	shledán	k2eAgInSc4d1	shledán
vinným	vinný	k1gMnSc7	vinný
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
propojení	propojení	k1gNnSc1	propojení
multimediálního	multimediální	k2eAgInSc2d1	multimediální
přehrávače	přehrávač	k1gInSc2	přehrávač
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
Media	medium	k1gNnPc4	medium
Player	Playra	k1gFnPc2	Playra
s	s	k7c7	s
operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
získal	získat	k5eAaPmAgMnS	získat
Microsoft	Microsoft	kA	Microsoft
konkurenční	konkurenční	k2eAgFnSc4d1	konkurenční
výhodu	výhoda	k1gFnSc4	výhoda
při	při	k7c6	při
prosazování	prosazování	k1gNnSc6	prosazování
vlastních	vlastní	k2eAgInPc2d1	vlastní
video	video	k1gNnSc4	video
a	a	k8xC	a
audio	audio	k2eAgInPc2d1	audio
formátů	formát	k1gInPc2	formát
(	(	kIx(	(
<g/>
WMA	WMA	kA	WMA
<g/>
,	,	kIx,	,
WMV	WMV	kA	WMV
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
unie	unie	k1gFnSc1	unie
ho	on	k3xPp3gMnSc4	on
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
pokutovala	pokutovat	k5eAaImAgFnS	pokutovat
a	a	k8xC	a
Microsoft	Microsoft	kA	Microsoft
musel	muset	k5eAaImAgInS	muset
upravit	upravit	k5eAaPmF	upravit
své	svůj	k3xOyFgInPc4	svůj
produkty	produkt	k1gInPc4	produkt
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
prodávat	prodávat	k5eAaImF	prodávat
v	v	k7c6	v
EU	EU	kA	EU
jiné	jiný	k2eAgFnPc4d1	jiná
verze	verze	k1gFnPc4	verze
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
tvrzení	tvrzení	k1gNnPc1	tvrzení
o	o	k7c6	o
poškozování	poškozování	k1gNnSc6	poškozování
trhu	trh	k1gInSc2	trh
se	se	k3xPyFc4	se
opírají	opírat	k5eAaImIp3nP	opírat
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Halloween	Halloween	k2eAgInSc1d1	Halloween
documents	documents	k1gInSc1	documents
<g/>
"	"	kIx"	"
–	–	k?	–
sérii	série	k1gFnSc4	série
interních	interní	k2eAgMnPc2d1	interní
dokumentů	dokument	k1gInPc2	dokument
Microsoftu	Microsoft	k1gInSc2	Microsoft
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
čelit	čelit	k5eAaImF	čelit
sílící	sílící	k2eAgFnSc3d1	sílící
konkurenci	konkurence	k1gFnSc3	konkurence
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
svobodného	svobodný	k2eAgInSc2d1	svobodný
softwaru	software	k1gInSc2	software
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
2006	[number]	k4	2006
<g/>
/	/	kIx~	/
<g/>
2007	[number]	k4	2007
byly	být	k5eAaImAgFnP	být
na	na	k7c4	na
trh	trh	k1gInSc4	trh
uváděny	uvádět	k5eAaImNgInP	uvádět
Windows	Windows	kA	Windows
Vista	vista	k2eAgInSc2d1	vista
v	v	k7c6	v
sedmi	sedm	k4xCc6	sedm
verzích	verze	k1gFnPc6	verze
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
novinkou	novinka	k1gFnSc7	novinka
tohoto	tento	k3xDgInSc2	tento
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
je	být	k5eAaImIp3nS	být
nové	nový	k2eAgNnSc1d1	nové
grafické	grafický	k2eAgNnSc1d1	grafické
rozhraní	rozhraní	k1gNnSc1	rozhraní
Aero	aero	k1gNnSc1	aero
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
nabízí	nabízet	k5eAaImIp3nS	nabízet
průhledná	průhledný	k2eAgFnSc1d1	průhledná
a	a	k8xC	a
3D	[number]	k4	3D
okna	okno	k1gNnPc1	okno
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
grafické	grafický	k2eAgInPc1d1	grafický
efekty	efekt	k1gInPc1	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
zlepšena	zlepšen	k2eAgFnSc1d1	zlepšena
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgFnPc2d1	další
funkcí	funkce	k1gFnPc2	funkce
včetně	včetně	k7c2	včetně
vyhledávání	vyhledávání	k1gNnSc2	vyhledávání
<g/>
,	,	kIx,	,
organizace	organizace	k1gFnSc2	organizace
souborů	soubor	k1gInPc2	soubor
aj.	aj.	kA	aj.
<g/>
Microsoft	Microsoft	kA	Microsoft
také	také	k9	také
vydal	vydat	k5eAaPmAgInS	vydat
nový	nový	k2eAgInSc1d1	nový
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
Windows	Windows	kA	Windows
7	[number]	k4	7
Prakticky	prakticky	k6eAd1	prakticky
současně	současně	k6eAd1	současně
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
i	i	k8xC	i
nová	nový	k2eAgFnSc1d1	nová
verze	verze	k1gFnSc1	verze
kancelářského	kancelářský	k2eAgInSc2d1	kancelářský
balíku	balík	k1gInSc2	balík
Microsoft	Microsoft	kA	Microsoft
Office	Office	kA	Office
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgInP	být
na	na	k7c4	na
trh	trh	k1gInSc4	trh
uvedeny	uvést	k5eAaPmNgFnP	uvést
nové	nový	k2eAgFnPc1d1	nová
verze	verze	k1gFnPc1	verze
<g/>
:	:	kIx,	:
Windows	Windows	kA	Windows
Server	server	k1gInSc4	server
2008	[number]	k4	2008
–	–	k?	–
nová	nový	k2eAgFnSc1d1	nová
verze	verze	k1gFnSc1	verze
serverového	serverový	k2eAgInSc2d1	serverový
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
virtualizace	virtualizace	k1gFnSc2	virtualizace
Hyper-V	Hyper-V	k1gFnSc2	Hyper-V
Služby	služba	k1gFnSc2	služba
Windows	Windows	kA	Windows
Live	Liv	k1gMnSc2	Liv
–	–	k?	–
služby	služba	k1gFnSc2	služba
pro	pro	k7c4	pro
komunikaci	komunikace	k1gFnSc4	komunikace
a	a	k8xC	a
sdílení	sdílení	k1gNnSc4	sdílení
informací	informace	k1gFnPc2	informace
napříč	napříč	k7c7	napříč
platformami	platforma	k1gFnPc7	platforma
Microsoft	Microsoft	kA	Microsoft
Dynamics	Dynamics	k1gInSc1	Dynamics
–	–	k?	–
podnikový	podnikový	k2eAgInSc1d1	podnikový
informační	informační	k2eAgInSc1d1	informační
systém	systém	k1gInSc1	systém
a	a	k8xC	a
nástroje	nástroj	k1gInPc1	nástroj
pro	pro	k7c4	pro
řízení	řízení	k1gNnSc4	řízení
zákaznických	zákaznický	k2eAgInPc2d1	zákaznický
vztahů	vztah	k1gInPc2	vztah
(	(	kIx(	(
<g/>
CRP	CRP	kA	CRP
a	a	k8xC	a
ERP	ERP	kA	ERP
<g/>
)	)	kIx)	)
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
(	(	kIx(	(
<g/>
9.201	[number]	k4	9.201
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
aktuální	aktuální	k2eAgInSc1d1	aktuální
hlavní	hlavní	k2eAgInSc1d1	hlavní
software	software	k1gInSc1	software
je	být	k5eAaImIp3nS	být
:	:	kIx,	:
Windows	Windows	kA	Windows
10	[number]	k4	10
Office	Office	kA	Office
2016	[number]	k4	2016
Windows	Windows	kA	Windows
Server	server	k1gInSc4	server
2012	[number]	k4	2012
R2	R2	k1gFnSc2	R2
Windows	Windows	kA	Windows
Phone	Phon	k1gInSc5	Phon
8.1	[number]	k4	8.1
Visual	Visual	k1gInSc1	Visual
Studio	studio	k1gNnSc1	studio
2015	[number]	k4	2015
</s>
