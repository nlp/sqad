<s>
Plejtvák	plejtvák	k1gMnSc1	plejtvák
obrovský	obrovský	k2eAgMnSc1d1	obrovský
(	(	kIx(	(
<g/>
Balaenoptera	Balaenopter	k1gMnSc2	Balaenopter
musculus	musculus	k1gMnSc1	musculus
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
také	také	k9	také
'	'	kIx"	'
<g/>
Modrá	modrý	k2eAgFnSc1d1	modrá
velryba	velryba	k1gFnSc1	velryba
<g/>
'	'	kIx"	'
anglicky	anglicky	k6eAd1	anglicky
Blue	Blue	k1gFnSc1	Blue
whale	whale	k1gFnSc2	whale
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Blauwal	Blauwal	k1gInSc1	Blauwal
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mořský	mořský	k2eAgMnSc1d1	mořský
savec	savec	k1gMnSc1	savec
z	z	k7c2	z
řádu	řád	k1gInSc2	řád
kytovců	kytovec	k1gMnPc2	kytovec
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
největšího	veliký	k2eAgMnSc2d3	veliký
současného	současný	k2eAgMnSc2d1	současný
žijícího	žijící	k2eAgMnSc2d1	žijící
živočicha	živočich	k1gMnSc2	živočich
a	a	k8xC	a
podle	podle	k7c2	podle
dosavadních	dosavadní	k2eAgFnPc2d1	dosavadní
informací	informace	k1gFnPc2	informace
i	i	k9	i
o	o	k7c4	o
největšího	veliký	k2eAgMnSc4d3	veliký
živočicha	živočich	k1gMnSc4	živočich
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
alespoň	alespoň	k9	alespoň
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
tělesné	tělesný	k2eAgFnSc2d1	tělesná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Délkou	délka	k1gFnSc7	délka
jej	on	k3xPp3gNnSc2	on
však	však	k9	však
předčí	předčít	k5eAaPmIp3nS	předčít
Trubýš	trubýš	k1gMnSc1	trubýš
pochybný	pochybný	k2eAgMnSc1d1	pochybný
a	a	k8xC	a
několik	několik	k4yIc4	několik
rodů	rod	k1gInPc2	rod
již	již	k6eAd1	již
vyhynulých	vyhynulý	k2eAgMnPc2d1	vyhynulý
sauropodních	sauropodní	k2eAgMnPc2d1	sauropodní
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
<g/>
.	.	kIx.	.
</s>
<s>
Plejtvák	plejtvák	k1gMnSc1	plejtvák
obrovský	obrovský	k2eAgMnSc1d1	obrovský
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
délky	délka	k1gFnSc2	délka
33	[number]	k4	33
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
váží	vážit	k5eAaImIp3nS	vážit
až	až	k9	až
přes	přes	k7c4	přes
200	[number]	k4	200
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Delší	dlouhý	k2eAgMnPc1d2	delší
byli	být	k5eAaImAgMnP	být
pouze	pouze	k6eAd1	pouze
někteří	některý	k3yIgMnPc1	některý
sauropodní	sauropodní	k2eAgMnPc1d1	sauropodní
dinosauři	dinosaurus	k1gMnPc1	dinosaurus
<g/>
,	,	kIx,	,
nebyli	být	k5eNaImAgMnP	být
ale	ale	k9	ale
tak	tak	k6eAd1	tak
těžcí	těžký	k2eAgMnPc1d1	těžký
(	(	kIx(	(
<g/>
hmotnost	hmotnost	k1gFnSc1	hmotnost
největších	veliký	k2eAgMnPc2d3	veliký
nepřesahovala	přesahovat	k5eNaImAgFnS	přesahovat
asi	asi	k9	asi
120	[number]	k4	120
tun	tuna	k1gFnPc2	tuna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
jeho	jeho	k3xOp3gInSc1	jeho
jazyk	jazyk	k1gInSc1	jazyk
váží	vážit	k5eAaImIp3nS	vážit
cca	cca	kA	cca
4	[number]	k4	4
tuny	tuna	k1gFnSc2	tuna
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
prakticky	prakticky	k6eAd1	prakticky
tolik	tolik	k6eAd1	tolik
jako	jako	k8xC	jako
dospělý	dospělý	k1gMnSc1	dospělý
slon	slon	k1gMnSc1	slon
<g/>
.	.	kIx.	.
</s>
<s>
Proudnicový	proudnicový	k2eAgInSc1d1	proudnicový
tvar	tvar	k1gInSc1	tvar
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
především	především	k9	především
kůže	kůže	k1gFnSc1	kůže
všech	všecek	k3xTgMnPc2	všecek
kytovců	kytovec	k1gMnPc2	kytovec
je	být	k5eAaImIp3nS	být
speciálně	speciálně	k6eAd1	speciálně
uzpůsobena	uzpůsobit	k5eAaPmNgFnS	uzpůsobit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
snižovala	snižovat	k5eAaImAgFnS	snižovat
tření	tření	k1gNnSc4	tření
a	a	k8xC	a
zabraňovala	zabraňovat	k5eAaImAgFnS	zabraňovat
vzniku	vznik	k1gInSc3	vznik
turbulencí	turbulence	k1gFnPc2	turbulence
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
mu	on	k3xPp3gMnSc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
plout	plout	k5eAaImF	plout
rychlostí	rychlost	k1gFnSc7	rychlost
až	až	k9	až
37	[number]	k4	37
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
a	a	k8xC	a
proto	proto	k8xC	proto
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejrychlejším	rychlý	k2eAgMnPc3d3	nejrychlejší
mořským	mořský	k2eAgMnPc3d1	mořský
tvorům	tvor	k1gMnPc3	tvor
<g/>
.	.	kIx.	.
</s>
<s>
Plejtvák	plejtvák	k1gMnSc1	plejtvák
obrovský	obrovský	k2eAgMnSc1d1	obrovský
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
výlučně	výlučně	k6eAd1	výlučně
krilem	kril	k1gInSc7	kril
tvořený	tvořený	k2eAgInSc1d1	tvořený
drobnými	drobný	k2eAgMnPc7d1	drobný
korýši	korýš	k1gMnPc7	korýš
<g/>
.	.	kIx.	.
</s>
<s>
Denně	denně	k6eAd1	denně
pozře	pozřít	k5eAaPmIp3nS	pozřít
odhadem	odhad	k1gInSc7	odhad
na	na	k7c4	na
40	[number]	k4	40
miliónů	milión	k4xCgInPc2	milión
těchto	tento	k3xDgMnPc2	tento
drobných	drobný	k2eAgMnPc2d1	drobný
korýšů	korýš	k1gMnPc2	korýš
<g/>
.	.	kIx.	.
</s>
<s>
Plejtváci	plejtvák	k1gMnPc1	plejtvák
jsou	být	k5eAaImIp3nP	být
savci	savec	k1gMnPc1	savec
<g/>
,	,	kIx,	,
dýchají	dýchat	k5eAaImIp3nP	dýchat
vzduch	vzduch	k1gInSc4	vzduch
a	a	k8xC	a
rodí	rodit	k5eAaImIp3nP	rodit
živá	živý	k2eAgNnPc4d1	živé
mláďata	mládě	k1gNnPc4	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
mléčné	mléčný	k2eAgFnPc4d1	mléčná
žlázy	žláza	k1gFnPc4	žláza
<g/>
,	,	kIx,	,
kterými	který	k3yIgFnPc7	který
krmí	krmit	k5eAaImIp3nP	krmit
svá	svůj	k3xOyFgNnPc4	svůj
mláďata	mládě	k1gNnPc4	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Mládě	mládě	k1gNnSc1	mládě
dokáže	dokázat	k5eAaPmIp3nS	dokázat
vypít	vypít	k5eAaPmF	vypít
až	až	k9	až
230	[number]	k4	230
litrů	litr	k1gInPc2	litr
mléka	mléko	k1gNnSc2	mléko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
50	[number]	k4	50
%	%	kIx~	%
tuku	tuk	k1gInSc2	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
mléka	mléko	k1gNnSc2	mléko
je	být	k5eAaImIp3nS	být
mládě	mládě	k1gNnSc1	mládě
odstavené	odstavený	k2eAgNnSc1d1	odstavené
až	až	k9	až
po	po	k7c6	po
7-8	[number]	k4	7-8
měsících	měsíc	k1gInPc6	měsíc
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
váží	vážit	k5eAaImIp3nS	vážit
cca	cca	kA	cca
23	[number]	k4	23
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
nabrání	nabrání	k1gNnSc1	nabrání
potravy	potrava	k1gFnSc2	potrava
plejtvákem	plejtvák	k1gMnSc7	plejtvák
(	(	kIx(	(
<g/>
směsi	směs	k1gFnSc6	směs
krilu	kril	k1gInSc2	kril
a	a	k8xC	a
vody	voda	k1gFnSc2	voda
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
hmotnost	hmotnost	k1gFnSc4	hmotnost
až	až	k9	až
40	[number]	k4	40
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
množství	množství	k1gNnSc1	množství
pak	pak	k6eAd1	pak
přecedí	přecedit	k5eAaPmIp3nS	přecedit
přes	přes	k7c4	přes
kosticovité	kosticovitý	k2eAgInPc4d1	kosticovitý
zuby	zub	k1gInPc4	zub
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
přes	přes	k7c4	přes
mohutné	mohutný	k2eAgNnSc4d1	mohutné
síto	síto	k1gNnSc4	síto
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
jsou	být	k5eAaImIp3nP	být
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
až	až	k9	až
jeden	jeden	k4xCgInSc4	jeden
metr	metr	k1gInSc4	metr
a	a	k8xC	a
široké	široký	k2eAgInPc1d1	široký
50-55	[number]	k4	50-55
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
útvar	útvar	k1gInSc1	útvar
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
chybějící	chybějící	k2eAgInPc4d1	chybějící
zuby	zub	k1gInPc4	zub
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
počtu	počet	k1gInSc6	počet
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
čelisti	čelist	k1gFnSc6	čelist
<g/>
.	.	kIx.	.
</s>
<s>
Kostic	kostice	k1gFnPc2	kostice
bývá	bývat	k5eAaImIp3nS	bývat
230	[number]	k4	230
až	až	k9	až
470	[number]	k4	470
<g/>
,	,	kIx,	,
fungují	fungovat	k5eAaImIp3nP	fungovat
jako	jako	k9	jako
zvláštní	zvláštní	k2eAgInPc1d1	zvláštní
kartáče	kartáč	k1gInPc1	kartáč
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
filtrují	filtrovat	k5eAaImIp3nP	filtrovat
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
zachytávají	zachytávat	k5eAaImIp3nP	zachytávat
drobné	drobný	k2eAgInPc1d1	drobný
mikroorganismy	mikroorganismus	k1gInPc1	mikroorganismus
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
velryba	velryba	k1gFnSc1	velryba
sytá	sytý	k2eAgFnSc1d1	sytá
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
zpracovat	zpracovat	k5eAaPmF	zpracovat
denně	denně	k6eAd1	denně
asi	asi	k9	asi
1,5	[number]	k4	1,5
miliónu	milión	k4xCgInSc2	milión
kilokalorií	kilokalorie	k1gFnPc2	kilokalorie
<g/>
.	.	kIx.	.
</s>
<s>
Plejtvák	plejtvák	k1gMnSc1	plejtvák
obrovský	obrovský	k2eAgMnSc1d1	obrovský
denně	denně	k6eAd1	denně
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
3	[number]	k4	3
až	až	k9	až
5	[number]	k4	5
tun	tuna	k1gFnPc2	tuna
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Sezóna	sezóna	k1gFnSc1	sezóna
<g/>
,	,	kIx,	,
po	po	k7c4	po
kterou	který	k3yQgFnSc4	který
se	se	k3xPyFc4	se
kytovci	kytovec	k1gMnPc7	kytovec
"	"	kIx"	"
<g/>
pasou	pást	k5eAaImIp3nP	pást
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
trvá	trvat	k5eAaImIp3nS	trvat
4	[number]	k4	4
až	až	k9	až
5	[number]	k4	5
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
sezony	sezona	k1gFnSc2	sezona
spotřebují	spotřebovat	k5eAaPmIp3nP	spotřebovat
velcí	velký	k2eAgMnPc1d1	velký
plejtváci	plejtvák	k1gMnPc1	plejtvák
450	[number]	k4	450
až	až	k8xS	až
750	[number]	k4	750
tun	tuna	k1gFnPc2	tuna
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Získat	získat	k5eAaPmF	získat
takové	takový	k3xDgNnSc4	takový
množství	množství	k1gNnSc4	množství
potravy	potrava	k1gFnSc2	potrava
není	být	k5eNaImIp3nS	být
nejjednodušší	jednoduchý	k2eAgFnSc1d3	nejjednodušší
a	a	k8xC	a
tak	tak	k6eAd1	tak
kytovci	kytovec	k1gMnPc1	kytovec
musejí	muset	k5eAaImIp3nP	muset
podstupovat	podstupovat	k5eAaImF	podstupovat
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
cesty	cesta	k1gFnPc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
při	při	k7c6	při
nich	on	k3xPp3gFnPc6	on
vždy	vždy	k6eAd1	vždy
drží	držet	k5eAaImIp3nP	držet
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
mateřské	mateřský	k2eAgFnSc6d1	mateřská
polokouli	polokoule	k1gFnSc6	polokoule
a	a	k8xC	a
nikdy	nikdy	k6eAd1	nikdy
nepřekračují	překračovat	k5eNaImIp3nP	překračovat
rovník	rovník	k1gInSc4	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Velryby	velryba	k1gFnPc1	velryba
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
srdce	srdce	k1gNnSc4	srdce
se	s	k7c7	s
čtyřmi	čtyři	k4xCgFnPc7	čtyři
komorami	komora	k1gFnPc7	komora
<g/>
.	.	kIx.	.
</s>
<s>
Tepny	tepna	k1gFnPc1	tepna
"	"	kIx"	"
<g/>
modré	modrý	k2eAgFnPc1d1	modrá
velryby	velryba	k1gFnPc1	velryba
<g/>
"	"	kIx"	"
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
velké	velká	k1gFnSc2	velká
asi	asi	k9	asi
20	[number]	k4	20
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
jsou	být	k5eAaImIp3nP	být
velryby	velryba	k1gFnPc1	velryba
výborně	výborně	k6eAd1	výborně
adaptovány	adaptovat	k5eAaBmNgFnP	adaptovat
k	k	k7c3	k
životu	život	k1gInSc3	život
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
vědci	vědec	k1gMnPc1	vědec
zatím	zatím	k6eAd1	zatím
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
ze	z	k7c2	z
suchozemských	suchozemský	k2eAgMnPc2d1	suchozemský
čtyřnohých	čtyřnohý	k2eAgMnPc2d1	čtyřnohý
savců	savec	k1gMnPc2	savec
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
vynikající	vynikající	k2eAgInSc4d1	vynikající
sluch	sluch	k1gInSc4	sluch
a	a	k8xC	a
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
schopnost	schopnost	k1gFnSc4	schopnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jim	on	k3xPp3gMnPc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
využít	využít	k5eAaPmF	využít
kyslík	kyslík	k1gInSc4	kyslík
z	z	k7c2	z
nadechovaného	nadechovaný	k2eAgInSc2d1	nadechovaný
vzduchu	vzduch	k1gInSc2	vzduch
dvakrát	dvakrát	k6eAd1	dvakrát
až	až	k9	až
třikrát	třikrát	k6eAd1	třikrát
účinněji	účinně	k6eAd2	účinně
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
suchozemských	suchozemský	k2eAgMnPc2d1	suchozemský
savců	savec	k1gMnPc2	savec
<g/>
.	.	kIx.	.
</s>
<s>
Plejtváci	plejtvák	k1gMnPc1	plejtvák
mají	mít	k5eAaImIp3nP	mít
stlačitelný	stlačitelný	k2eAgInSc4d1	stlačitelný
hrudník	hrudník	k1gInSc4	hrudník
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
jim	on	k3xPp3gMnPc3	on
napomáhá	napomáhat	k5eAaImIp3nS	napomáhat
při	při	k7c6	při
ponorech	ponor	k1gInPc6	ponor
do	do	k7c2	do
velkých	velký	k2eAgFnPc2d1	velká
hloubek	hloubka	k1gFnPc2	hloubka
<g/>
.	.	kIx.	.
</s>
<s>
Plaví	plavit	k5eAaImIp3nS	plavit
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
na	na	k7c6	na
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
sami	sám	k3xTgMnPc1	sám
nebo	nebo	k8xC	nebo
po	po	k7c6	po
dvojicích	dvojice	k1gFnPc6	dvojice
<g/>
,	,	kIx,	,
maximálně	maximálně	k6eAd1	maximálně
však	však	k9	však
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
lidského	lidský	k2eAgInSc2d1	lidský
pohledu	pohled	k1gInSc2	pohled
jsou	být	k5eAaImIp3nP	být
sice	sice	k8xC	sice
samotáři	samotář	k1gMnPc1	samotář
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
dorozumívání	dorozumívání	k1gNnSc3	dorozumívání
plují	plout	k5eAaImIp3nP	plout
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
společnosti	společnost	k1gFnSc6	společnost
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
vzdáleni	vzdálen	k2eAgMnPc1d1	vzdálen
desítky	desítka	k1gFnPc4	desítka
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Kdysi	kdysi	k6eAd1	kdysi
těchto	tento	k3xDgFnPc2	tento
velryb	velryba	k1gFnPc2	velryba
žilo	žít	k5eAaImAgNnS	žít
ve	v	k7c6	v
vodách	voda	k1gFnPc6	voda
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Austrálie	Austrálie	k1gFnSc2	Austrálie
přibližně	přibližně	k6eAd1	přibližně
250	[number]	k4	250
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
se	se	k3xPyFc4	se
však	však	k9	však
po	po	k7c6	po
příchodu	příchod	k1gInSc6	příchod
velrybářů	velrybář	k1gMnPc2	velrybář
dostaly	dostat	k5eAaPmAgFnP	dostat
až	až	k6eAd1	až
na	na	k7c4	na
samý	samý	k3xTgInSc4	samý
pokraj	pokraj	k1gInSc4	pokraj
vyhubení	vyhubení	k1gNnSc2	vyhubení
<g/>
.	.	kIx.	.
</s>
<s>
Plejtváků	plejtvák	k1gMnPc2	plejtvák
obrovských	obrovský	k2eAgFnPc6d1	obrovská
přežilo	přežít	k5eAaPmAgNnS	přežít
do	do	k7c2	do
dnešního	dnešní	k2eAgInSc2d1	dnešní
dne	den	k1gInSc2	den
jen	jen	k9	jen
asi	asi	k9	asi
500	[number]	k4	500
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Plejtvák	plejtvák	k1gMnSc1	plejtvák
obrovský	obrovský	k2eAgMnSc1d1	obrovský
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Článek	článek	k1gInSc4	článek
srovnávající	srovnávající	k2eAgFnSc2d1	srovnávající
plejtváka	plejtvák	k1gMnSc4	plejtvák
obrovského	obrovský	k2eAgMnSc4d1	obrovský
a	a	k8xC	a
největší	veliký	k2eAgMnPc4d3	veliký
sauropodní	sauropodní	k2eAgMnPc4d1	sauropodní
dinosaury	dinosaurus	k1gMnPc4	dinosaurus
<g/>
,	,	kIx,	,
blog	blog	k1gMnSc1	blog
DinosaurusBlog	DinosaurusBlog	k1gMnSc1	DinosaurusBlog
Článek	článek	k1gInSc4	článek
srovnávající	srovnávající	k2eAgFnSc1d1	srovnávající
argentinosaura	argentinosaura	k1gFnSc1	argentinosaura
s	s	k7c7	s
plejtvákem	plejtvák	k1gMnSc7	plejtvák
obrovským	obrovský	k2eAgMnSc7d1	obrovský
<g/>
,	,	kIx,	,
blog	blog	k1gMnSc1	blog
DinosaurusBlog	DinosaurusBlog	k1gMnSc1	DinosaurusBlog
</s>
