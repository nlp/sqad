<s>
Porsche	Porsche	k1gNnSc1
912	#num#	k4
</s>
<s>
Porsche	Porsche	k1gNnSc1
912	#num#	k4
Porsche	Porsche	k1gNnSc2
912	#num#	k4
<g/>
Výrobce	výrobce	k1gMnSc1
</s>
<s>
Porsche	Porsche	k1gNnSc1
Roky	rok	k1gInPc1
produkce	produkce	k1gFnSc2
</s>
<s>
1965	#num#	k4
–	–	k?
1969	#num#	k4
1976	#num#	k4
(	(	kIx(
<g/>
pod	pod	k7c7
názvem	název	k1gInSc7
912	#num#	k4
<g/>
E	E	kA
<g/>
)	)	kIx)
Místa	místo	k1gNnPc4
výroby	výroba	k1gFnSc2
</s>
<s>
Německo	Německo	k1gNnSc1
Předchůdce	předchůdce	k1gMnSc2
</s>
<s>
Porsche	Porsche	k1gNnSc1
356	#num#	k4
Nástupce	nástupce	k1gMnSc5
</s>
<s>
Porsche	Porsche	k1gNnSc1
914	#num#	k4
Příbuzné	příbuzný	k2eAgInPc4d1
vozy	vůz	k1gInPc4
</s>
<s>
Porsche	Porsche	k1gNnSc1
356	#num#	k4
<g/>
,	,	kIx,
Porsche	Porsche	k1gNnSc2
911	#num#	k4
Karoserie	karoserie	k1gFnSc2
</s>
<s>
2	#num#	k4
<g/>
-dveřové	-dveřové	k2eAgNnSc7d1
kupé	kupé	k1gNnSc7
Koncepce	koncepce	k1gFnSc2
pohonu	pohon	k1gInSc2
</s>
<s>
Motor	motor	k1gInSc1
vzadu	vzadu	k6eAd1
a	a	k8xC
pohon	pohon	k1gInSc4
zadních	zadní	k2eAgNnPc2d1
kol	kolo	k1gNnPc2
<g/>
,	,	kIx,
RWD	RWD	kA
nebo	nebo	k8xC
RR	RR	kA
Technické	technický	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Délka	délka	k1gFnSc1
</s>
<s>
4135	#num#	k4
mm	mm	kA
Šířka	šířka	k1gFnSc1
</s>
<s>
1600	#num#	k4
mm	mm	kA
Výška	výška	k1gFnSc1
</s>
<s>
1320	#num#	k4
mm	mm	kA
Rozvor	rozvora	k1gFnPc2
</s>
<s>
2255	#num#	k4
mm	mm	kA
Hmotnost	hmotnost	k1gFnSc4
</s>
<s>
Šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Zastaralý	zastaralý	k2eAgInSc1d1
parametr	parametr	k1gInSc1
infoboxu	infobox	k1gInSc2
<g/>
}}	}}	k?
prohlašuje	prohlašovat	k5eAaImIp3nS
parametr	parametr	k1gInSc1
"	"	kIx"
<g/>
hmotnost	hmotnost	k1gFnSc1
<g/>
"	"	kIx"
(	(	kIx(
<g/>
nyní	nyní	k6eAd1
s	s	k7c7
hodnotou	hodnota	k1gFnSc7
"	"	kIx"
<g/>
965	#num#	k4
<g/>
"	"	kIx"
<g/>
)	)	kIx)
šablony	šablona	k1gFnPc1
"	"	kIx"
<g/>
Infobox	Infobox	k1gInSc1
-	-	kIx~
automobil	automobil	k1gInSc1
<g/>
"	"	kIx"
za	za	k7c4
zastaralý	zastaralý	k2eAgInSc4d1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
965	#num#	k4
kg	kg	kA
(	(	kIx(
<g/>
parametr	parametr	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
upřesnit	upřesnit	k5eAaPmF
<g/>
)	)	kIx)
Objem	objem	k1gInSc1
nádrže	nádrž	k1gFnSc2
</s>
<s>
61	#num#	k4
l	l	kA
Motor	motor	k1gInSc1
Motor	motor	k1gInSc1
</s>
<s>
1,6	1,6	k4
l	l	kA
(	(	kIx(
<g/>
66	#num#	k4
kW	kW	kA
<g/>
)	)	kIx)
2	#num#	k4
l	l	kA
(	(	kIx(
<g/>
66	#num#	k4
kW	kW	kA
<g/>
)	)	kIx)
-	-	kIx~
912E	912E	k4
Převodovky	převodovka	k1gFnSc2
Převodovka	převodovka	k1gFnSc1
</s>
<s>
čtyřstupňová	čtyřstupňový	k2eAgFnSc1d1
manuální	manuální	k2eAgFnSc1d1
pětistupňová	pětistupňový	k2eAgFnSc1d1
manuální	manuální	k2eAgFnSc1d1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Porsche	Porsche	k1gNnSc1
912	#num#	k4
je	být	k5eAaImIp3nS
německý	německý	k2eAgInSc1d1
sportovní	sportovní	k2eAgInSc1d1
automobil	automobil	k1gInSc1
firmy	firma	k1gFnSc2
Porsche	Porsche	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
vyráběl	vyrábět	k5eAaImAgInS
od	od	k7c2
roku	rok	k1gInSc2
1965	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
1969	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
vůz	vůz	k1gInSc1
byl	být	k5eAaImAgInS
uveden	uvést	k5eAaPmNgInS
na	na	k7c4
trh	trh	k1gInSc4
hlavně	hlavně	k9
z	z	k7c2
finančního	finanční	k2eAgInSc2d1
důvodu	důvod	k1gInSc2
-	-	kIx~
Porsche	Porsche	k1gNnSc2
911	#num#	k4
bylo	být	k5eAaImAgNnS
společností	společnost	k1gFnPc2
vnímáno	vnímat	k5eAaImNgNnS
jako	jako	k8xC,k8xS
velmi	velmi	k6eAd1
luxusní	luxusní	k2eAgNnSc1d1
a	a	k8xC
drahé	drahý	k2eAgNnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
Porsche	Porsche	k1gNnSc1
356	#num#	k4
už	už	k6eAd1
bylo	být	k5eAaImAgNnS
velmi	velmi	k6eAd1
staré	starý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
přišel	přijít	k5eAaPmAgMnS
do	do	k7c2
výroby	výroba	k1gFnSc2
model	model	k1gInSc1
912	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
vůz	vůz	k1gInSc4
s	s	k7c7
prakticky	prakticky	k6eAd1
totožnou	totožný	k2eAgFnSc7d1
karoserií	karoserie	k1gFnSc7
s	s	k7c7
modelem	model	k1gInSc7
911	#num#	k4
a	a	k8xC
motorem	motor	k1gInSc7
z	z	k7c2
předchůdce	předchůdce	k1gMnSc2
356	#num#	k4
<g/>
C.	C.	kA
Ze	z	k7c2
svého	svůj	k3xOyFgMnSc4
předchůdce	předchůdce	k1gMnSc4
zdědil	zdědit	k5eAaPmAgMnS
také	také	k9
mnoho	mnoho	k4c4
prvků	prvek	k1gInPc2
v	v	k7c6
interiéru	interiér	k1gInSc6
i	i	k8xC
exteriéru	exteriér	k1gInSc6
-	-	kIx~
sedadla	sedadlo	k1gNnPc4
<g/>
,	,	kIx,
palubní	palubní	k2eAgInPc4d1
přístroje	přístroj	k1gInPc4
<g/>
,	,	kIx,
řadicí	řadicí	k2eAgFnSc4d1
páku	páka	k1gFnSc4
<g/>
,	,	kIx,
mlhová	mlhový	k2eAgNnPc1d1
světla	světlo	k1gNnPc1
a	a	k8xC
také	také	k9
hlavní	hlavní	k2eAgInPc4d1
přední	přední	k2eAgInPc4d1
světlomety	světlomet	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Vůz	vůz	k1gInSc1
byl	být	k5eAaImAgInS
velmi	velmi	k6eAd1
dobře	dobře	k6eAd1
přijat	přijmout	k5eAaPmNgInS
jako	jako	k9
společností	společnost	k1gFnSc7
<g/>
,	,	kIx,
tak	tak	k9
závodníky	závodník	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkově	celkově	k6eAd1
bylo	být	k5eAaImAgNnS
vyrobeno	vyrobit	k5eAaPmNgNnS
34	#num#	k4
959	#num#	k4
kusů	kus	k1gInPc2
a	a	k8xC
v	v	k7c6
některých	některý	k3yIgNnPc6
letech	léto	k1gNnPc6
se	se	k3xPyFc4
modelu	model	k1gInSc6
912	#num#	k4
prodalo	prodat	k5eAaPmAgNnS
dokonce	dokonce	k9
více	hodně	k6eAd2
než	než	k8xS
světoznámého	světoznámý	k2eAgInSc2d1
modelu	model	k1gInSc2
911	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1968	#num#	k4
však	však	k9
začal	začít	k5eAaPmAgInS
zájem	zájem	k1gInSc1
o	o	k7c4
tento	tento	k3xDgInSc4
model	model	k1gInSc4
pomalu	pomalu	k6eAd1
ustávat	ustávat	k5eAaImF
a	a	k8xC
tak	tak	k6eAd1
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
později	pozdě	k6eAd2
uveden	uveden	k2eAgInSc1d1
nový	nový	k2eAgInSc1d1
model	model	k1gInSc1
-	-	kIx~
Porsche	Porsche	k1gNnSc1
914	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Speciální	speciální	k2eAgFnSc1d1
verze	verze	k1gFnSc1
</s>
<s>
Porsche	Porsche	k1gNnSc1
912	#num#	k4
Targa	Targ	k1gMnSc4
</s>
<s>
Porsche	Porsche	k1gNnSc1
912	#num#	k4
se	se	k3xPyFc4
také	také	k9
dělalo	dělat	k5eAaImAgNnS
ve	v	k7c6
verzi	verze	k1gFnSc6
targa	targ	k1gMnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
polokabriolet	polokabriolet	k5eAaPmF,k5eAaImF
s	s	k7c7
odjímatelnou	odjímatelný	k2eAgFnSc7d1
střechou	střecha	k1gFnSc7
a	a	k8xC
ochranným	ochranný	k2eAgInSc7d1
rámem	rám	k1gInSc7
za	za	k7c7
sedadly	sedadlo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
provedení	provedení	k1gNnSc1
bylo	být	k5eAaImAgNnS
poprvé	poprvé	k6eAd1
uvedeno	uvést	k5eAaPmNgNnS
u	u	k7c2
vozu	vůz	k1gInSc2
Porsche	Porsche	k1gNnSc1
911	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prototyp	prototyp	k1gInSc1
Porsche	Porsche	k1gNnSc1
912	#num#	k4
Targa	Targ	k1gMnSc2
byl	být	k5eAaImAgInS
představen	představit	k5eAaPmNgInS
na	na	k7c6
autosalonu	autosalon	k1gInSc6
ve	v	k7c6
Frankfurtu	Frankfurt	k1gInSc6
nad	nad	k7c7
Mohanem	Mohan	k1gInSc7
v	v	k7c6
roce	rok	k1gInSc6
1965	#num#	k4
a	a	k8xC
první	první	k4xOgInPc1
vozy	vůz	k1gInPc1
se	se	k3xPyFc4
začaly	začít	k5eAaPmAgInP
vyrábět	vyrábět	k5eAaImF
roku	rok	k1gInSc2
1966	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkově	celkově	k6eAd1
bylo	být	k5eAaImAgNnS
vyrobeno	vyrobit	k5eAaPmNgNnS
2562	#num#	k4
kusů	kus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc1
z	z	k7c2
kusů	kus	k1gInPc2
se	se	k3xPyFc4
také	také	k9
může	moct	k5eAaImIp3nS
pochlubit	pochlubit	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
byl	být	k5eAaImAgInS
vyroben	vyrobit	k5eAaPmNgInS
jako	jako	k8xC,k8xS
stotisící	stotisící	k2eAgInSc1d1
vůz	vůz	k1gInSc1
Porsche	Porsche	k1gNnSc2
a	a	k8xC
byl	být	k5eAaImAgInS
slavnostně	slavnostně	k6eAd1
předán	předat	k5eAaPmNgInS
německé	německý	k2eAgFnSc3d1
policii	policie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
model	model	k1gInSc1
se	se	k3xPyFc4
také	také	k9
vyráběl	vyrábět	k5eAaImAgInS
ve	v	k7c6
speciální	speciální	k2eAgFnSc6d1
verzi	verze	k1gFnSc6
přímo	přímo	k6eAd1
pro	pro	k7c4
německou	německý	k2eAgFnSc4d1
policii	policie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Porsche	Porsche	k1gNnSc1
912E	912E	k4
</s>
<s>
Tento	tento	k3xDgInSc1
model	model	k1gInSc1
byl	být	k5eAaImAgInS
vyráběn	vyrábět	k5eAaImNgInS
pouze	pouze	k6eAd1
jeden	jeden	k4xCgInSc4
rok	rok	k1gInSc4
(	(	kIx(
<g/>
1976	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Firma	firma	k1gFnSc1
Porsche	Porsche	k1gNnSc2
ho	on	k3xPp3gMnSc4
vyvinula	vyvinout	k5eAaPmAgFnS
z	z	k7c2
důvodu	důvod	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
model	model	k1gInSc1
914	#num#	k4
se	se	k3xPyFc4
již	již	k6eAd1
přestal	přestat	k5eAaPmAgInS
prodávat	prodávat	k5eAaImF
a	a	k8xC
model	model	k1gInSc1
924	#num#	k4
se	se	k3xPyFc4
ještě	ještě	k6eAd1
prodávat	prodávat	k5eAaImF
nezačal	začít	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nestal	stát	k5eNaPmAgMnS
se	se	k3xPyFc4
slavným	slavný	k2eAgInSc7d1
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
také	také	k9
z	z	k7c2
důvodu	důvod	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
prodával	prodávat	k5eAaImAgInS
pouze	pouze	k6eAd1
v	v	k7c6
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
produkt	produkt	k1gInSc4
své	svůj	k3xOyFgFnSc2
doby	doba	k1gFnSc2
-	-	kIx~
USA	USA	kA
prožívaly	prožívat	k5eAaImAgInP
ropný	ropný	k2eAgInSc4d1
šok	šok	k1gInSc4
a	a	k8xC
proto	proto	k8xC
byla	být	k5eAaImAgFnS
poptávka	poptávka	k1gFnSc1
po	po	k7c6
sportovním	sportovní	k2eAgInSc6d1
vozu	vůz	k1gInSc6
s	s	k7c7
menší	malý	k2eAgFnSc7d2
spotřebou	spotřeba	k1gFnSc7
paliva	palivo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Vůz	vůz	k1gInSc1
měl	mít	k5eAaImAgInS
hmotnost	hmotnost	k1gFnSc4
1087	#num#	k4
kg	kg	kA
a	a	k8xC
dvoulitrový	dvoulitrový	k2eAgInSc1d1
motor	motor	k1gInSc1
o	o	k7c6
výkonu	výkon	k1gInSc6
66	#num#	k4
kW	kW	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměrná	průměrný	k2eAgFnSc1d1
spotřeba	spotřeba	k1gFnSc1
byla	být	k5eAaImAgFnS
8,4	8,4	k4
litrů	litr	k1gInPc2
na	na	k7c4
100	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dosahoval	dosahovat	k5eAaImAgMnS
maximální	maximální	k2eAgFnPc4d1
rychlosti	rychlost	k1gFnPc4
180	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
Vyrobeno	vyrobit	k5eAaPmNgNnS
bylo	být	k5eAaImAgNnS
celkově	celkově	k6eAd1
pouze	pouze	k6eAd1
2099	#num#	k4
kusů	kus	k1gInPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
SPENCER	SPENCER	kA
<g/>
,	,	kIx,
Duane	Duan	k1gMnSc5
<g/>
:	:	kIx,
The	The	k1gMnSc6
Complete	Complet	k1gInSc5
Porsche	Porsche	k1gNnPc1
912	#num#	k4
Guide	Guid	k1gInSc5
<g/>
,	,	kIx,
168	#num#	k4
s.	s.	k?
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
</s>
<s>
CLYMER	CLYMER	kA
<g/>
,	,	kIx,
Floyd	Floyd	k1gInSc1
<g/>
:	:	kIx,
Porsche	Porsche	k1gNnSc1
912	#num#	k4
workshop	workshop	k1gInSc1
manual	manuat	k5eAaBmAgInS,k5eAaPmAgInS,k5eAaImAgInS
1965	#num#	k4
<g/>
-	-	kIx~
<g/>
1968	#num#	k4
<g/>
,	,	kIx,
308	#num#	k4
s.	s.	k?
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-1588501011	978-1588501011	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
AUTOFUN	AUTOFUN	kA
<g/>
.	.	kIx.
<g/>
BLESK	blesk	k1gInSc1
<g/>
.	.	kIx.
<g/>
CZ	CZ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Čeština	čeština	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Porsche	Porsche	k1gNnSc1
912	#num#	k4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Porsche	Porsche	k1gNnSc1
<g/>
912	#num#	k4
<g/>
e.	e.	k?
<g/>
com	com	k?
-	-	kIx~
stránka	stránka	k1gFnSc1
věnovaná	věnovaný	k2eAgFnSc1d1
verzi	verze	k1gFnSc4
912E	912E	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
</s>
<s>
•	•	k?
kz	kz	k?
•	•	k?
d	d	k?
•	•	k?
e	e	k0
Automobily	automobil	k1gInPc1
značky	značka	k1gFnPc1
Porsche	Porsche	k1gNnSc1
1960	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
</s>
<s>
Třída	třída	k1gFnSc1
</s>
<s>
1960	#num#	k4
</s>
<s>
19701980199020002010	#num#	k4
</s>
<s>
012345678901234567890123456789012345678901234567890123456789	#num#	k4
</s>
<s>
Sportovní	sportovní	k2eAgFnPc1d1
</s>
<s>
912	#num#	k4
</s>
<s>
924	#num#	k4
</s>
<s>
Boxster	Boxster	k1gInSc1
(	(	kIx(
<g/>
986	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Boxster	Boxster	k1gInSc1
(	(	kIx(
<g/>
987	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Boxster	Boxster	k1gInSc1
(	(	kIx(
<g/>
981	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
718	#num#	k4
Boxster	Boxster	k1gInSc1
(	(	kIx(
<g/>
981	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
356	#num#	k4
</s>
<s>
914	#num#	k4
</s>
<s>
944	#num#	k4
</s>
<s>
968	#num#	k4
</s>
<s>
Cayman	Cayman	k1gMnSc1
(	(	kIx(
<g/>
987	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Cayman	Cayman	k1gMnSc1
(	(	kIx(
<g/>
981	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
718	#num#	k4
Cayman	Cayman	k1gMnSc1
(	(	kIx(
<g/>
982	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Řada	řada	k1gFnSc1
911	#num#	k4
</s>
<s>
911	#num#	k4
</s>
<s>
911	#num#	k4
(	(	kIx(
<g/>
964	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
911	#num#	k4
(	(	kIx(
<g/>
993	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
911	#num#	k4
(	(	kIx(
<g/>
996	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
911	#num#	k4
(	(	kIx(
<g/>
997	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
911	#num#	k4
(	(	kIx(
<g/>
991	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
911	#num#	k4
(	(	kIx(
<g/>
992	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
GT	GT	kA
</s>
<s>
928	#num#	k4
</s>
<s>
Supersporty	supersport	k1gInPc1
</s>
<s>
959	#num#	k4
</s>
<s>
GT1	GT1	k4
</s>
<s>
CGT	CGT	kA
</s>
<s>
918	#num#	k4
Spyder	Spyder	k1gInSc1
</s>
<s>
SUV	SUV	kA
</s>
<s>
Cayenne	Cayennout	k5eAaImIp3nS,k5eAaPmIp3nS
(	(	kIx(
<g/>
955	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Cayenne	Cayennout	k5eAaPmIp3nS,k5eAaImIp3nS
(	(	kIx(
<g/>
957	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Cayenne	Cayennout	k5eAaPmIp3nS,k5eAaImIp3nS
(	(	kIx(
<g/>
958	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Cayenne	Cayennout	k5eAaImIp3nS,k5eAaPmIp3nS
(	(	kIx(
<g/>
PO	po	k7c6
<g/>
536	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Čtyřdvéřové	čtyřdvéřový	k2eAgNnSc1d1
kupé	kupé	k1gNnSc1
</s>
<s>
Panamera	Panamera	k1gFnSc1
(	(	kIx(
<g/>
970	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Panamera	Panamera	k1gFnSc1
(	(	kIx(
<g/>
971	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Taycan	Taycan	k1gMnSc1
</s>
<s>
Crossover	Crossover	k1gMnSc1
</s>
<s>
Macan	Macan	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Automobil	automobil	k1gInSc1
</s>
