<s>
Zlatá	zlatý	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
Překážky	překážka	k1gFnPc1
na	na	k7c4
Bislett	Bislett	k2eAgInSc4d1
Games	Games	k1gInSc4
<g/>
,	,	kIx,
jednom	jeden	k4xCgMnSc6
z	z	k7c2
mítinků	mítink	k1gInPc2
Zlaté	zlatý	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1
liga	liga	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
IAAF	IAAF	kA
Golden	Goldna	k2eAgFnSc1d1
League	League	k1gFnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
prestižní	prestižní	k2eAgInSc1d1
atletický	atletický	k2eAgInSc1d1
seriál	seriál	k1gInSc1
pořádaný	pořádaný	k2eAgInSc1d1
mezinárodní	mezinárodní	k2eAgFnSc7d1
atletickou	atletický	k2eAgFnSc7d1
federací	federace	k1gFnSc7
IAAF	IAAF	kA
v	v	k7c6
letech	léto	k1gNnPc6
1998	#num#	k4
až	až	k9
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítězové	vítěz	k1gMnPc1
všech	všecek	k3xTgInPc2
mítinků	mítink	k1gInPc2
si	se	k3xPyFc3
mezi	mezi	k7c7
sebou	se	k3xPyFc7
dělili	dělit	k5eAaImAgMnP
jackpot	jackpot	k1gInSc4
ve	v	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
jednoho	jeden	k4xCgInSc2
milionu	milion	k4xCgInSc2
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2006	#num#	k4
se	se	k3xPyFc4
tento	tento	k3xDgInSc4
milion	milion	k4xCgInSc4
dělil	dělit	k5eAaImAgMnS
na	na	k7c4
dvě	dva	k4xCgFnPc4
části	část	k1gFnPc4
<g/>
:	:	kIx,
první	první	k4xOgFnSc4
polovinu	polovina	k1gFnSc4
si	se	k3xPyFc3
rozdělili	rozdělit	k5eAaPmAgMnP
všichni	všechen	k3xTgMnPc1
účastníci	účastník	k1gMnPc1
s	s	k7c7
pěti	pět	k4xCc2
vítězstvími	vítězství	k1gNnPc7
<g/>
,	,	kIx,
druhou	druhý	k4xOgFnSc4
pak	pak	k6eAd1
absolutní	absolutní	k2eAgMnPc1d1
vítězové	vítěz	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mítinky	mítink	k1gInPc1
Zlaté	zlatý	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
</s>
<s>
Mítink	mítink	k1gInSc1
</s>
<s>
Stadion	stadion	k1gInSc1
</s>
<s>
Město	město	k1gNnSc1
</s>
<s>
Stát	stát	k1gInSc1
</s>
<s>
ISTAF	ISTAF	kA
</s>
<s>
Olympiastadion	Olympiastadion	k1gInSc1
Berlín	Berlín	k1gInSc1
</s>
<s>
Berlín	Berlín	k1gInSc1
</s>
<s>
Německo	Německo	k1gNnSc1
Německo	Německo	k1gNnSc1
</s>
<s>
Bislett	Bislett	k2eAgInSc1d1
Games	Games	k1gInSc1
</s>
<s>
Bislett	Bislett	k2eAgInSc1d1
Stadion	stadion	k1gInSc1
</s>
<s>
Oslo	Oslo	k1gNnSc1
</s>
<s>
Norsko	Norsko	k1gNnSc1
Norsko	Norsko	k1gNnSc1
</s>
<s>
Golden	Goldna	k1gFnPc2
Gala	gala	k1gNnSc2
</s>
<s>
Stadio	Stadio	k1gMnSc1
Olimpico	Olimpico	k1gMnSc1
</s>
<s>
Řím	Řím	k1gInSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
Itálie	Itálie	k1gFnSc2
</s>
<s>
Meeting	meeting	k1gInSc1
Areva	Arevo	k1gNnSc2
</s>
<s>
Stade	Stade	k6eAd1
de	de	k?
France	Franc	k1gMnSc4
</s>
<s>
Saint-Denis	Saint-Denis	k1gFnSc1
</s>
<s>
Francie	Francie	k1gFnSc1
Francie	Francie	k1gFnSc2
</s>
<s>
Weltklasse	Weltklasse	k1gFnSc1
Zürich	Züricha	k1gFnPc2
</s>
<s>
Letzigrund	Letzigrund	k1gMnSc1
</s>
<s>
Curych	Curych	k1gInSc1
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
Memorial	Memorial	k1gInSc1
Van	vana	k1gFnPc2
Damme	Damm	k1gInSc5
</s>
<s>
Stadion	stadion	k1gInSc1
krále	král	k1gMnSc2
Baudouina	Baudouin	k1gMnSc2
</s>
<s>
Brusel	Brusel	k1gInSc1
</s>
<s>
Belgie	Belgie	k1gFnSc1
Belgie	Belgie	k1gFnSc1
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2010	#num#	k4
byla	být	k5eAaImAgFnS
nahrazena	nahradit	k5eAaPmNgFnS
novým	nový	k2eAgInSc7d1
seriálem	seriál	k1gInSc7
Diamantové	diamantový	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vítězové	vítěz	k1gMnPc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Atlet	atlet	k1gMnSc1
nebo	nebo	k8xC
atletka	atletka	k1gFnSc1
</s>
<s>
Disciplína	disciplína	k1gFnSc1
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1
liga	liga	k1gFnSc1
1998	#num#	k4
Marion	Marion	k1gInSc1
Jonesová	Jonesový	k2eAgFnSc1d1
<g/>
100	#num#	k4
m	m	kA
</s>
<s>
Haile	Haile	k1gFnSc1
Gebrselassie	Gebrselassie	k1gFnSc1
<g/>
3000	#num#	k4
/	/	kIx~
5000	#num#	k4
m	m	kA
</s>
<s>
Hicham	Hicham	k1gInSc1
El	Ela	k1gFnPc2
Guerrouj	Guerrouj	k1gFnSc4
<g/>
1500	#num#	k4
m	m	kA
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1
liga	liga	k1gFnSc1
1999	#num#	k4
Gabriela	Gabriela	k1gFnSc1
Szabóová	Szabóová	k1gFnSc1
<g/>
3000	#num#	k4
m	m	kA
</s>
<s>
Wilson	Wilson	k1gMnSc1
Kipketer	Kipketer	k1gMnSc1
<g/>
800	#num#	k4
m	m	kA
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1
liga	liga	k1gFnSc1
2000	#num#	k4
Gail	Gailum	k1gNnPc2
Deversová	Deversová	k1gFnSc1
<g/>
100	#num#	k4
m	m	kA
přek	přek	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Hicham	Hicham	k1gInSc1
El	Ela	k1gFnPc2
Guerrouj	Guerrouj	k1gFnSc4
<g/>
1500	#num#	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
mile	mile	k6eAd1
</s>
<s>
Maurice	Maurika	k1gFnSc3
Greene	Green	k1gInSc5
<g/>
100	#num#	k4
m	m	kA
</s>
<s>
Trine	Trinout	k5eAaImIp3nS,k5eAaPmIp3nS
Hattestadováoštěp	Hattestadováoštěp	k1gInSc1
</s>
<s>
Taťjana	Taťjana	k1gFnSc1
Kotovovádálka	Kotovovádálka	k1gFnSc1
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1
liga	liga	k1gFnSc1
2001	#num#	k4
André	André	k1gMnPc2
Bucher	Buchra	k1gFnPc2
<g/>
800	#num#	k4
m	m	kA
</s>
<s>
Hicham	Hicham	k1gInSc1
El	Ela	k1gFnPc2
Guerrouj	Guerrouj	k1gFnSc4
<g/>
1500	#num#	k4
<g/>
m	m	kA
/	/	kIx~
mile	mile	k6eAd1
/	/	kIx~
2000	#num#	k4
m	m	kA
</s>
<s>
Allen	Allen	k1gMnSc1
Johnson	Johnson	k1gMnSc1
<g/>
110	#num#	k4
m	m	kA
přek	přek	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Marion	Marion	k1gInSc1
Jonesová	Jonesový	k2eAgFnSc1d1
<g/>
100	#num#	k4
<g/>
m	m	kA
</s>
<s>
Violeta	Violeta	k1gFnSc1
Szekely	Szekela	k1gFnSc2
<g/>
1500	#num#	k4
<g/>
m	m	kA
</s>
<s>
Olga	Olga	k1gFnSc1
Jegorovová	Jegorovová	k1gFnSc1
<g/>
3000	#num#	k4
<g/>
m	m	kA
<g/>
/	/	kIx~
<g/>
5000	#num#	k4
m	m	kA
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1
liga	liga	k1gFnSc1
2002	#num#	k4
Hicham	Hicham	k1gInSc1
El	Ela	k1gFnPc2
Guerrouj	Guerrouj	k1gFnSc4
<g/>
1500	#num#	k4
m	m	kA
</s>
<s>
Ana	Ana	k?
Guevaraová	Guevaraová	k1gFnSc1
<g/>
400	#num#	k4
m	m	kA
</s>
<s>
Marion	Marion	k1gInSc1
Jonesová	Jonesový	k2eAgFnSc1d1
<g/>
100	#num#	k4
m	m	kA
</s>
<s>
Félix	Félix	k1gInSc1
Sánchez	Sánchez	k1gInSc1
<g/>
400	#num#	k4
m	m	kA
přek	přek	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1
liga	liga	k1gFnSc1
2003	#num#	k4
Maria	Maria	k1gFnSc1
Mutolaová	Mutolaová	k1gFnSc1
<g/>
800	#num#	k4
m	m	kA
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1
liga	liga	k1gFnSc1
2004	#num#	k4
Christian	Christian	k1gMnSc1
Olssontrojskok	Olssontrojskok	k1gInSc1
</s>
<s>
Tonique	Toniquat	k5eAaPmIp3nS
Williams-Darling	Williams-Darling	k1gInSc4
<g/>
400	#num#	k4
m	m	kA
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1
liga	liga	k1gFnSc1
2005	#num#	k4
Taťána	Taťána	k1gFnSc1
Lebeděvovátrojskok	Lebeděvovátrojskok	k1gInSc1
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1
liga	liga	k1gFnSc1
2006	#num#	k4
Asafa	Asaf	k1gMnSc4
Powell	Powell	k1gInSc4
<g/>
100	#num#	k4
m	m	kA
</s>
<s>
Jeremy	Jerem	k1gInPc1
Wariner	Wariner	k1gInSc1
<g/>
400	#num#	k4
m	m	kA
</s>
<s>
Sanya	Sany	k2eAgFnSc1d1
Richardsová	Richardsová	k1gFnSc1
<g/>
400	#num#	k4
m	m	kA
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1
liga	liga	k1gFnSc1
2007	#num#	k4
Sanya	Sanya	k1gFnSc1
Richardsová	Richardsová	k1gFnSc1
<g/>
400	#num#	k4
m	m	kA
</s>
<s>
Jelena	Jelena	k1gFnSc1
Isinbajevovátyč	Isinbajevovátyč	k1gFnSc1
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1
liga	liga	k1gFnSc1
2008	#num#	k4
Pamela	Pamela	k1gFnSc1
Jelimová	Jelimová	k1gFnSc1
<g/>
800	#num#	k4
m	m	kA
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1
liga	liga	k1gFnSc1
2009	#num#	k4
Jelena	Jelena	k1gFnSc1
Isinbajevovátyč	Isinbajevovátyč	k1gFnSc1
</s>
<s>
Sanya	Sany	k2eAgFnSc1d1
Richardsová	Richardsová	k1gFnSc1
<g/>
400	#num#	k4
m	m	kA
</s>
<s>
Kenenisa	Kenenisa	k1gFnSc1
Bekele	Bekel	k1gInSc2
<g/>
10000	#num#	k4
m	m	kA
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Zlatá	zlatý	k2eAgFnSc1d1
liga	liga	k1gFnSc1
startuje	startovat	k5eAaBmIp3nS
v	v	k7c6
Oslu	Oslo	k1gNnSc6
v	v	k7c6
novém	nový	k2eAgInSc6d1
hávu	háv	k1gInSc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Zlatá	zlatý	k2eAgFnSc1d1
liga	liga	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
IAAF	IAAF	kA
Golden	Goldno	k1gNnPc2
League	League	k1gFnSc1
2008	#num#	k4
oficiální	oficiální	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gNnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc4
<g/>
}	}	kIx)
Atletické	atletický	k2eAgInPc4d1
mítinky	mítink	k1gInPc4
Zlatá	zlatý	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
ZL	ZL	kA
1998	#num#	k4
•	•	k?
ZL	ZL	kA
1999	#num#	k4
•	•	k?
ZL	ZL	kA
2000	#num#	k4
•	•	k?
ZL	ZL	kA
2001	#num#	k4
•	•	k?
ZL	ZL	kA
2002	#num#	k4
•	•	k?
ZL	ZL	kA
2003	#num#	k4
•	•	k?
ZL	ZL	kA
2004	#num#	k4
•	•	k?
ZL	ZL	kA
2005	#num#	k4
•	•	k?
ZL	ZL	kA
2006	#num#	k4
•	•	k?
ZL	ZL	kA
2007	#num#	k4
•	•	k?
ZL	ZL	kA
2008	#num#	k4
•	•	k?
ZL	ZL	kA
2009	#num#	k4
Diamantová	diamantový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
DL	DL	kA
2010	#num#	k4
•	•	k?
DL	DL	kA
2011	#num#	k4
•	•	k?
DL	DL	kA
2012	#num#	k4
•	•	k?
DL	DL	kA
2013	#num#	k4
•	•	k?
DL	DL	kA
2014	#num#	k4
•	•	k?
DL	DL	kA
2015	#num#	k4
•	•	k?
DL	DL	kA
2016	#num#	k4
•	•	k?
DL	DL	kA
2017	#num#	k4
•	•	k?
DL	DL	kA
2018	#num#	k4
•	•	k?
DL	DL	kA
2019	#num#	k4
•	•	k?
DL	DL	kA
2020	#num#	k4
Mítinky	mítink	k1gInPc7
diamantové	diamantový	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
</s>
<s>
Doha	Doha	k1gFnSc1
Diamond	Diamond	k1gInSc1
League	League	k1gFnSc1
(	(	kIx(
<g/>
Dauhá	Dauhý	k2eAgFnSc1d1
<g/>
)	)	kIx)
•	•	k?
Shanghai	Shangha	k1gFnSc2
Golden	Goldna	k1gFnPc2
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
(	(	kIx(
<g/>
Šanghaj	Šanghaj	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Bauhaus-Galan	Bauhaus-Galan	k1gInSc1
(	(	kIx(
<g/>
Stockholm	Stockholm	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Golden	Goldna	k1gFnPc2
Gala	Gala	k1gMnSc1
(	(	kIx(
<g/>
Řím	Řím	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Bislett	Bislett	k1gInSc1
Games	Games	k1gInSc1
(	(	kIx(
<g/>
Oslo	Oslo	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Meeting	meeting	k1gInSc1
international	internationat	k5eAaImAgInS,k5eAaPmAgInS
Mohammed-VI	Mohammed-VI	k1gFnSc3
(	(	kIx(
<g/>
Rabat	rabat	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Prefontaine	Prefontain	k1gInSc5
Classic	Classice	k1gFnPc2
(	(	kIx(
<g/>
Palo	Pala	k1gMnSc5
Alto	Alta	k1gMnSc5
<g/>
)	)	kIx)
•	•	k?
Athletissima	Athletissima	k1gFnSc1
(	(	kIx(
<g/>
Lausanne	Lausanne	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Meeting	meeting	k1gInSc1
Herculis	Herculis	k1gFnSc1
(	(	kIx(
<g/>
Monako	Monako	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
London	London	k1gMnSc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
(	(	kIx(
<g/>
Londýn	Londýn	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
British	British	k1gInSc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
(	(	kIx(
<g/>
Birmingham	Birmingham	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Meeting	meeting	k1gInSc1
de	de	k?
Paris	Paris	k1gMnSc1
(	(	kIx(
<g/>
Paříž	Paříž	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Weltklasse	Weltklasse	k1gFnSc1
Zürich	Zürich	k1gMnSc1
(	(	kIx(
<g/>
Curych	Curych	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Memoriál	memoriál	k1gInSc1
Van	van	k1gInSc1
Dammeho	Damme	k1gMnSc2
(	(	kIx(
<g/>
Brusel	Brusel	k1gInSc1
<g/>
)	)	kIx)
Další	další	k2eAgInPc1d1
mítinky	mítink	k1gInPc1
</s>
<s>
New	New	k?
York	York	k1gInSc1
•	•	k?
Berlín	Berlín	k1gInSc1
•	•	k?
Ostrava	Ostrava	k1gFnSc1
Vítězové	vítěz	k1gMnPc1
DL	DL	kA
muži	muž	k1gMnPc7
a	a	k8xC
ženy	žena	k1gFnPc1
</s>
<s>
100	#num#	k4
m	m	kA
•	•	k?
200	#num#	k4
m	m	kA
•	•	k?
400	#num#	k4
m	m	kA
•	•	k?
800	#num#	k4
m	m	kA
•	•	k?
1500	#num#	k4
m	m	kA
•	•	k?
5000	#num#	k4
m	m	kA
•	•	k?
110	#num#	k4
metrů	metr	k1gInPc2
překážek	překážka	k1gFnPc2
•	•	k?
400	#num#	k4
metrů	metr	k1gInPc2
překážek	překážka	k1gFnPc2
•	•	k?
3000	#num#	k4
metrů	metr	k1gInPc2
překážek	překážka	k1gFnPc2
•	•	k?
Skok	skok	k1gInSc1
daleký	daleký	k2eAgInSc1d1
•	•	k?
Trojskok	trojskok	k1gInSc1
•	•	k?
Skok	skok	k1gInSc1
do	do	k7c2
výšky	výška	k1gFnSc2
•	•	k?
Skok	skok	k1gInSc1
o	o	k7c6
tyči	tyč	k1gFnSc6
•	•	k?
Vrh	vrh	k1gInSc4
koulí	koulet	k5eAaImIp3nS
•	•	k?
Hod	hod	k1gInSc4
diskem	disk	k1gInSc7
•	•	k?
Hod	hod	k1gInSc1
oštěpem	oštěp	k1gInSc7
</s>
