<s>
Třída	třída	k1gFnSc1
Aratù	Aratù	k1gFnSc2
</s>
<s>
Třída	třída	k1gFnSc1
Aratù	Aratù	k1gFnSc1
Aratù	Aratù	k1gFnSc1
(	(	kIx(
<g/>
M	M	kA
15	#num#	k4
<g/>
)	)	kIx)
<g/>
Obecné	obecný	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Uživatelé	uživatel	k1gMnPc1
</s>
<s>
Brazilské	brazilský	k2eAgNnSc1d1
námořnictvo	námořnictvo	k1gNnSc1
Typ	typ	k1gInSc1
</s>
<s>
minolovka	minolovka	k1gFnSc1
Lodě	loď	k1gFnSc2
</s>
<s>
6	#num#	k4
Osud	osud	k1gInSc1
</s>
<s>
aktivní	aktivní	k2eAgMnSc1d1
(	(	kIx(
<g/>
2021	#num#	k4
<g/>
)	)	kIx)
Technické	technický	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Výtlak	výtlak	k1gInSc4
</s>
<s>
230	#num#	k4
t	t	k?
(	(	kIx(
<g/>
standardní	standardní	k2eAgFnSc7d1
<g/>
)	)	kIx)
<g/>
280	#num#	k4
t	t	k?
(	(	kIx(
<g/>
plný	plný	k2eAgInSc4d1
<g/>
)	)	kIx)
Délka	délka	k1gFnSc1
</s>
<s>
47,2	47,2	k4
m	m	kA
Šířka	šířka	k1gFnSc1
</s>
<s>
7,16	7,16	k4
m	m	kA
Ponor	ponor	k1gInSc1
</s>
<s>
2,1	2,1	k4
m	m	kA
Pohon	pohon	k1gInSc1
</s>
<s>
4	#num#	k4
diesely	diesel	k1gInPc1
<g/>
,	,	kIx,
2	#num#	k4
cykloidní	cykloidní	k2eAgInPc1d1
šrouby	šroub	k1gInPc1
Rychlost	rychlost	k1gFnSc1
</s>
<s>
24	#num#	k4
uzlů	uzel	k1gInPc2
Dosah	dosah	k1gInSc1
</s>
<s>
710	#num#	k4
nám.	nám.	k?
mil	míle	k1gFnPc2
při	při	k7c6
20	#num#	k4
uzlech	uzel	k1gInPc6
Posádka	posádka	k1gFnSc1
</s>
<s>
32	#num#	k4
Výzbroj	výzbroj	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
×	×	k?
40	#num#	k4
<g/>
mm	mm	kA
kanón	kanón	k1gInSc1
Bofors	Bofors	k1gInSc1
Radar	radar	k1gInSc4
</s>
<s>
ZW-06	ZW-06	k4
</s>
<s>
Třída	třída	k1gFnSc1
Aratù	Aratù	k1gFnSc2
je	být	k5eAaImIp3nS
třída	třída	k1gFnSc1
minolovek	minolovka	k1gFnPc2
brazilského	brazilský	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
derivát	derivát	k1gInSc4
německých	německý	k2eAgFnPc2d1
pobřežních	pobřežní	k2eAgFnPc2d1
minolovek	minolovka	k1gFnPc2
třídy	třída	k1gFnSc2
Schütze	Schütze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
bylo	být	k5eAaImAgNnS
postaveno	postavit	k5eAaPmNgNnS
šest	šest	k4xCc1
jednotek	jednotka	k1gFnPc2
této	tento	k3xDgFnSc2
třídy	třída	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
službě	služba	k1gFnSc6
jsou	být	k5eAaImIp3nP
od	od	k7c2
roku	rok	k1gInSc2
1971	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
roku	rok	k1gInSc3
2018	#num#	k4
zůstávaly	zůstávat	k5eAaImAgFnP
ve	v	k7c6
službě	služba	k1gFnSc6
čtyři	čtyři	k4xCgFnPc4
jednotky	jednotka	k1gFnPc4
této	tento	k3xDgFnSc2
třídy	třída	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stavba	stavba	k1gFnSc1
</s>
<s>
Celkem	celkem	k6eAd1
šest	šest	k4xCc4
jendotek	jendotka	k1gFnPc2
této	tento	k3xDgFnSc2
třídy	třída	k1gFnSc2
postavila	postavit	k5eAaPmAgFnS
západoněmecké	západoněmecký	k2eAgFnPc4d1
loděnice	loděnice	k1gFnPc4
Abeking	Abeking	k1gInSc1
&	&	k?
Rasmussen	Rasmussen	k1gInSc1
v	v	k7c6
Lemwerderu	Lemwerder	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
služby	služba	k1gFnSc2
byly	být	k5eAaImAgInP
přijaty	přijmout	k5eAaPmNgInP
v	v	k7c6
letech	let	k1gInPc6
1971	#num#	k4
<g/>
–	–	k?
<g/>
1975	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jednotky	jednotka	k1gFnPc1
třídy	třída	k1gFnSc2
Aratù	Aratù	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
JménoSpuštěnaVstup	JménoSpuštěnaVstup	k1gInSc1
do	do	k7c2
službyStatus	službyStatus	k1gInSc1
</s>
<s>
Aratù	Aratù	k?
(	(	kIx(
<g/>
M	M	kA
15	#num#	k4
<g/>
)	)	kIx)
<g/>
19705	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1971	#num#	k4
<g/>
Aktivní	aktivní	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Anhatomirim	Anhatomirim	k1gMnSc1
(	(	kIx(
<g/>
M	M	kA
16	#num#	k4
<g/>
)	)	kIx)
<g/>
197030	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1971	#num#	k4
<g/>
Roku	rok	k1gInSc2
2016	#num#	k4
převedena	převést	k5eAaPmNgFnS
do	do	k7c2
rezervy	rezerva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Atalaia	Atalaia	k1gFnSc1
(	(	kIx(
<g/>
M	M	kA
17	#num#	k4
<g/>
)	)	kIx)
<g/>
197113	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1972	#num#	k4
<g/>
Aktivní	aktivní	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Araçatuba	Araçatuba	k1gMnSc1
(	(	kIx(
<g/>
M	M	kA
18	#num#	k4
<g/>
)	)	kIx)
<g/>
197113	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1972	#num#	k4
<g/>
Aktivní	aktivní	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Abrolhos	Abrolhos	k1gMnSc1
(	(	kIx(
<g/>
M	M	kA
19	#num#	k4
<g/>
)	)	kIx)
<g/>
197416	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1975	#num#	k4
<g/>
Roku	rok	k1gInSc2
2015	#num#	k4
převedena	převést	k5eAaPmNgFnS
do	do	k7c2
rezervy	rezerva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Albardã	Albardã	k1gMnSc1
(	(	kIx(
<g/>
M	M	kA
20	#num#	k4
<g/>
)	)	kIx)
<g/>
197421	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1975	#num#	k4
<g/>
Aktivní	aktivní	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1
</s>
<s>
Atalaia	Atalaia	k1gFnSc1
(	(	kIx(
<g/>
M	M	kA
17	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Plavidla	plavidlo	k1gNnPc1
mají	mít	k5eAaImIp3nP
dřevěný	dřevěný	k2eAgInSc4d1
trup	trup	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nesou	nést	k5eAaImIp3nP
navigační	navigační	k2eAgInSc4d1
radar	radar	k1gInSc4
ZW-06	ZW-06	k1gFnSc2
(	(	kIx(
<g/>
později	pozdě	k6eAd2
nahrazen	nahradit	k5eAaPmNgInS
radarem	radar	k1gInSc7
Furuno	Furuna	k1gFnSc5
1831	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výzbroj	výzbroj	k1gInSc1
tvoří	tvořit	k5eAaImIp3nS
jeden	jeden	k4xCgInSc1
40	#num#	k4
<g/>
mm	mm	kA
kanón	kanón	k1gInSc1
Bofors	Bofors	k1gInSc1
na	na	k7c6
přídi	příď	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohonný	pohonný	k2eAgInSc1d1
systém	systém	k1gInSc1
tvoří	tvořit	k5eAaImIp3nS
čtyři	čtyři	k4xCgInPc4
diesely	diesel	k1gInPc4
Maybach	Maybacha	k1gFnPc2
o	o	k7c6
výkonu	výkon	k1gInSc6
4500	#num#	k4
hp	hp	k?
<g/>
,	,	kIx,
pohánějící	pohánějící	k2eAgInPc4d1
dva	dva	k4xCgInPc4
cykloidní	cykloidní	k2eAgInPc4d1
šrouby	šroub	k1gInPc4
Escher-Wyss	Escher-Wyssa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšší	vysoký	k2eAgFnSc1d3
rychlost	rychlost	k1gFnSc1
dosahuje	dosahovat	k5eAaImIp3nS
24	#num#	k4
uzlů	uzel	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dosah	dosah	k1gInSc1
je	být	k5eAaImIp3nS
710	#num#	k4
námořních	námořní	k2eAgFnPc2d1
mil	míle	k1gFnPc2
při	při	k7c6
20	#num#	k4
uzlech	uzel	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
ARATU	arat	k1gMnSc3
minesweepers	minesweepers	k6eAd1
(	(	kIx(
<g/>
1971	#num#	k4
<g/>
-	-	kIx~
<g/>
1975	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navypedia	Navypedium	k1gNnPc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Třída	třída	k1gFnSc1
Aratù	Aratù	k1gMnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Lokální	lokální	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
odkazuje	odkazovat	k5eAaImIp3nS
na	na	k7c4
jinou	jiný	k2eAgFnSc4d1
kategorii	kategorie	k1gFnSc4
Commons	Commonsa	k1gFnPc2
než	než	k8xS
přiřazená	přiřazený	k2eAgFnSc1d1
položka	položka	k1gFnSc1
Wikidat	Wikidat	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
Lokální	lokální	k2eAgInSc4d1
odkaz	odkaz	k1gInSc4
<g/>
:	:	kIx,
Aratu	arat	k1gMnSc3
class	class	k6eAd1
</s>
<s>
Wikidata	Wikidata	k1gFnSc1
<g/>
:	:	kIx,
Schütze	Schütze	k1gFnSc1
class	class	k6eAd1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Loďstvo	loďstvo	k1gNnSc1
</s>
