<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1
svaz	svaz	k1gInSc1
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1
svaz	svaz	k1gInSc1
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
Logo	logo	k1gNnSc4
mezinárodní	mezinárodní	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
IUCN	IUCN	kA
Zkratka	zkratka	k1gFnSc1
</s>
<s>
IUCN	IUCN	kA
Vznik	vznik	k1gInSc1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1948	#num#	k4
Typ	typ	k1gInSc1
</s>
<s>
mezinárodní	mezinárodní	k2eAgFnSc1d1
nevládní	vládní	k2eNgFnSc1d1
organizace	organizace	k1gFnSc1
(	(	kIx(
<g/>
INGO	INGO	kA
<g/>
)	)	kIx)
Právní	právní	k2eAgFnSc1d1
forma	forma	k1gFnSc1
</s>
<s>
mezinárodní	mezinárodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
Účel	účel	k1gInSc1
</s>
<s>
uchování	uchování	k1gNnSc3
přírodních	přírodní	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Rue	Rue	k?
Mauverney	Mauvernea	k1gFnPc4
28	#num#	k4
<g/>
,	,	kIx,
1196	#num#	k4
Gland	Glanda	k1gFnPc2
<g/>
,	,	kIx,
Switzerland	Switzerlando	k1gNnPc2
Místo	místo	k7c2
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
Švýcarsko	Švýcarsko	k1gNnSc1
Úřední	úřední	k2eAgMnSc1d1
jazyk	jazyk	k1gMnSc1
</s>
<s>
francouzština	francouzština	k1gFnSc1
<g/>
,	,	kIx,
angličtina	angličtina	k1gFnSc1
<g/>
,	,	kIx,
španělština	španělština	k1gFnSc1
Lídr	lídr	k1gMnSc1
</s>
<s>
Inger	Inger	k1gInSc1
Andersenová	Andersenová	k1gFnSc1
(	(	kIx(
<g/>
*	*	kIx~
1958	#num#	k4
<g/>
)	)	kIx)
Zaměstnanců	zaměstnanec	k1gMnPc2
</s>
<s>
1	#num#	k4
100	#num#	k4
Ocenění	ocenění	k1gNnPc2
</s>
<s>
Cena	cena	k1gFnSc1
kněžny	kněžna	k1gFnSc2
asturské	asturský	k2eAgFnSc2d1
za	za	k7c4
svornost	svornost	k1gFnSc4
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.iucn.org	www.iucn.org	k1gInSc1
Poznámky	poznámka	k1gFnSc2
</s>
<s>
je	být	k5eAaImIp3nS
aktivní	aktivní	k2eAgNnSc1d1
na	na	k7c6
sociálních	sociální	k2eAgFnPc6d1
sítích	síť	k1gFnPc6
Facebook	Facebook	k1gInSc1
a	a	k8xC
Twitter	Twitter	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1
svaz	svaz	k1gInSc1
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
International	International	k1gFnSc1
Union	union	k1gInSc1
for	forum	k7c4
Conservation	Conservation	k1gInSc4
of	of	k7
Nature	Nature	k1gFnSc1
<g/>
,	,	kIx,
IUCN	IUCN	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
zaměřená	zaměřený	k2eAgFnSc1d1
na	na	k7c4
uchování	uchování	k1gNnSc4
přírodních	přírodní	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1948	#num#	k4
a	a	k8xC
má	mít	k5eAaImIp3nS
sídlo	sídlo	k1gNnSc4
ve	v	k7c6
švýcarském	švýcarský	k2eAgInSc6d1
Glandu	Gland	k1gInSc6
u	u	k7c2
Ženevského	ženevský	k2eAgNnSc2d1
jezera	jezero	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
IUCN	IUCN	kA
spojuje	spojovat	k5eAaImIp3nS
83	#num#	k4
států	stát	k1gInPc2
<g/>
,	,	kIx,
108	#num#	k4
státních	státní	k2eAgFnPc2d1
institucí	instituce	k1gFnPc2
<g/>
,	,	kIx,
766	#num#	k4
nevládních	vládní	k2eNgFnPc2d1
organizací	organizace	k1gFnPc2
<g/>
,	,	kIx,
81	#num#	k4
mezinárodních	mezinárodní	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
a	a	k8xC
kolem	kolem	k7c2
10	#num#	k4
000	#num#	k4
odborníků	odborník	k1gMnPc2
a	a	k8xC
vědců	vědec	k1gMnPc2
z	z	k7c2
celého	celý	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Poslání	poslání	k1gNnSc1
</s>
<s>
Posláním	poslání	k1gNnSc7
IUCN	IUCN	kA
je	být	k5eAaImIp3nS
ovlivňovat	ovlivňovat	k5eAaImF
<g/>
,	,	kIx,
podporovat	podporovat	k5eAaImF
a	a	k8xC
pomáhat	pomáhat	k5eAaImF
společnostem	společnost	k1gFnPc3
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
v	v	k7c6
ochraně	ochrana	k1gFnSc6
celistvosti	celistvost	k1gFnSc2
a	a	k8xC
rozmanitosti	rozmanitost	k1gFnSc2
přírody	příroda	k1gFnSc2
a	a	k8xC
zajišťovat	zajišťovat	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
jakékoli	jakýkoli	k3yIgNnSc1
využívání	využívání	k1gNnSc1
přírodních	přírodní	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
bude	být	k5eAaImBp3nS
spravedlivé	spravedlivý	k2eAgNnSc1d1
a	a	k8xC
ekologicky	ekologicky	k6eAd1
udržitelné	udržitelný	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
První	první	k4xOgMnSc1
generální	generální	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
UNESCO	UNESCO	kA
<g/>
,	,	kIx,
Sir	sir	k1gMnSc1
Julian	Julian	k1gMnSc1
Huxley	Huxlea	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
si	se	k3xPyFc3
přál	přát	k5eAaImAgMnS
postavit	postavit	k5eAaPmF
UNESCO	Unesco	k1gNnSc4
na	na	k7c6
vědečtějších	vědecký	k2eAgInPc6d2
základech	základ	k1gInPc6
<g/>
,	,	kIx,
zorganizoval	zorganizovat	k5eAaPmAgMnS
kongres	kongres	k1gInSc4
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yRgInSc6,k3yQgInSc6,k3yIgInSc6
měla	mít	k5eAaImAgFnS
vzniknout	vzniknout	k5eAaPmF
nová	nový	k2eAgFnSc1d1
ekologická	ekologický	k2eAgFnSc1d1
instituce	instituce	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
by	by	kYmCp3nS
sloužila	sloužit	k5eAaImAgFnS
tomuto	tento	k3xDgInSc3
účelu	účel	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
tomto	tento	k3xDgInSc6
prvním	první	k4xOgInSc6
kongresu	kongres	k1gInSc6
konaném	konaný	k2eAgNnSc6d1
ve	v	k7c6
francouzském	francouzský	k2eAgNnSc6d1
Fontainebleau	Fontainebleaum	k1gNnSc6
5	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
1948	#num#	k4
se	se	k3xPyFc4
shodlo	shodnout	k5eAaBmAgNnS,k5eAaPmAgNnS
18	#num#	k4
vlád	vláda	k1gFnPc2
<g/>
,	,	kIx,
7	#num#	k4
mezinárodních	mezinárodní	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
a	a	k8xC
107	#num#	k4
národních	národní	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
na	na	k7c6
založení	založení	k1gNnSc6
zastřešující	zastřešující	k2eAgFnSc2d1
instituce	instituce	k1gFnSc2
a	a	k8xC
podepsalo	podepsat	k5eAaPmAgNnS
ustavující	ustavující	k2eAgInSc4d1
akt	akt	k1gInSc4
International	International	k1gFnSc1
Union	union	k1gInSc1
for	forum	k1gNnPc2
the	the	k?
Protection	Protection	k1gInSc1
of	of	k?
Nature	Natur	k1gMnSc5
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
tohoto	tento	k3xDgInSc2
začátku	začátek	k1gInSc2
bylo	být	k5eAaImAgNnS
převažující	převažující	k2eAgFnSc7d1
strategií	strategie	k1gFnSc7
nové	nový	k2eAgFnSc2d1
instituce	instituce	k1gFnSc2
zkoumat	zkoumat	k5eAaImF
a	a	k8xC
propagovat	propagovat	k5eAaImF
vzájemně	vzájemně	k6eAd1
výhodná	výhodný	k2eAgNnPc1d1
ochranná	ochranný	k2eAgNnPc1d1
opatření	opatření	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yRgNnPc1,k3yQgNnPc1
budou	být	k5eAaImBp3nP
vyhovovat	vyhovovat	k5eAaImF
jak	jak	k6eAd1
zastáncům	zastánce	k1gMnPc3
hospodářského	hospodářský	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
<g/>
,	,	kIx,
tak	tak	k8xC,k8xS
pomáhat	pomáhat	k5eAaImF
lidem	člověk	k1gMnPc3
a	a	k8xC
státům	stát	k1gInPc3
lépe	dobře	k6eAd2
chránit	chránit	k5eAaImF
jejich	jejich	k3xOp3gFnSc4
flóru	flóra	k1gFnSc4
a	a	k8xC
faunu	fauna	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Organizace	organizace	k1gFnSc1
vždy	vždy	k6eAd1
zdůrazňovala	zdůrazňovat	k5eAaImAgFnS
nutnost	nutnost	k1gFnSc1
respektovat	respektovat	k5eAaImF
potřeby	potřeba	k1gFnPc4
místních	místní	k2eAgMnPc2d1
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
mohli	moct	k5eAaImAgMnP
převzít	převzít	k5eAaPmF
budoucí	budoucí	k2eAgInPc4d1
dlouhodobé	dlouhodobý	k2eAgInPc4d1
cíle	cíl	k1gInPc4
ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
místních	místní	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
a	a	k8xC
ohrožené	ohrožený	k2eAgInPc1d1
druhy	druh	k1gInPc1
budou	být	k5eAaImBp3nP
nejlépe	dobře	k6eAd3
chráněny	chránit	k5eAaImNgInP
<g/>
,	,	kIx,
jestliže	jestliže	k8xS
jejich	jejich	k3xOp3gFnSc4
ochranu	ochrana	k1gFnSc4
budou	být	k5eAaImBp3nP
místní	místní	k2eAgMnPc1d1
obyvatelé	obyvatel	k1gMnPc1
považovat	považovat	k5eAaImF
za	za	k7c4
svůj	svůj	k3xOyFgInSc4
zájem	zájem	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Práce	práce	k1gFnSc1
s	s	k7c7
místními	místní	k2eAgMnPc7d1
lidmi	člověk	k1gMnPc7
a	a	k8xC
nikoli	nikoli	k9
proti	proti	k7c3
nim	on	k3xPp3gMnPc3
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
hlavní	hlavní	k2eAgFnSc7d1
zásadou	zásada	k1gFnSc7
IUCN	IUCN	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
tomto	tento	k3xDgInSc6
principu	princip	k1gInSc6
byla	být	k5eAaImAgFnS
založena	založen	k2eAgFnSc1d1
Strategie	strategie	k1gFnSc1
zachování	zachování	k1gNnSc2
světa	svět	k1gInSc2
(	(	kIx(
<g/>
World	World	k1gInSc1
Conservation	Conservation	k1gInSc1
Strategy	Stratega	k1gFnSc2
<g/>
)	)	kIx)
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
IUCN	IUCN	kA
v	v	k7c6
ní	on	k3xPp3gFnSc6
jasně	jasně	k6eAd1
deklarovala	deklarovat	k5eAaBmAgFnS
svůj	svůj	k3xOyFgInSc4
zájem	zájem	k1gInSc4
na	na	k7c6
dialogu	dialog	k1gInSc6
se	se	k3xPyFc4
zastánci	zastánce	k1gMnPc1
hospodářského	hospodářský	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostalo	dostat	k5eAaPmAgNnS
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
pozitivního	pozitivní	k2eAgInSc2d1
mezinárodního	mezinárodní	k2eAgInSc2d1
ohlasu	ohlas	k1gInSc2
a	a	k8xC
zajistila	zajistit	k5eAaPmAgFnS
IUCN	IUCN	kA
podporu	podpora	k1gFnSc4
řady	řada	k1gFnSc2
sponzorů	sponzor	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
sami	sám	k3xTgMnPc1
nebyli	být	k5eNaImAgMnP
schopni	schopen	k2eAgMnPc1d1
otevřít	otevřít	k5eAaPmF
dialog	dialog	k1gInSc4
v	v	k7c6
rozvojových	rozvojový	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
,	,	kIx,
ani	ani	k8xC
neměli	mít	k5eNaImAgMnP
pocit	pocit	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
OSN	OSN	kA
a	a	k8xC
mezinárodní	mezinárodní	k2eAgFnPc1d1
banky	banka	k1gFnPc1
se	se	k3xPyFc4
hodlají	hodlat	k5eAaImIp3nP
v	v	k7c6
takovém	takový	k3xDgInSc6
dialogu	dialog	k1gInSc6
efektivně	efektivně	k6eAd1
angažovat	angažovat	k5eAaBmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
IUCN	IUCN	kA
se	se	k3xPyFc4
rozšířila	rozšířit	k5eAaPmAgFnS
do	do	k7c2
mnoha	mnoho	k4c2
zemí	zem	k1gFnPc2
světa	svět	k1gInSc2
a	a	k8xC
má	mít	k5eAaImIp3nS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
služby	služba	k1gFnSc2
mnoha	mnoho	k4c2
odborníků-dobrovolníků	odborníků-dobrovolník	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
poskytují	poskytovat	k5eAaImIp3nP
rady	rada	k1gFnPc4
a	a	k8xC
konzervační	konzervační	k2eAgFnPc4d1
služby	služba	k1gFnPc4
na	na	k7c6
místní	místní	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Síť	síť	k1gFnSc1
výborů	výbor	k1gInPc2
a	a	k8xC
regionálních	regionální	k2eAgInPc2d1
poradních	poradní	k2eAgInPc2d1
orgánů	orgán	k1gInPc2
se	se	k3xPyFc4
rozšiřuje	rozšiřovat	k5eAaImIp3nS
do	do	k7c2
stále	stále	k6eAd1
většího	veliký	k2eAgInSc2d2
počtu	počet	k1gInSc2
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Následuje	následovat	k5eAaImIp3nS
přehled	přehled	k1gInSc4
některých	některý	k3yIgNnPc2
dat	datum	k1gNnPc2
klíčových	klíčový	k2eAgNnPc2d1
pro	pro	k7c4
růst	růst	k1gInSc4
a	a	k8xC
rozvoj	rozvoj	k1gInSc4
této	tento	k3xDgFnSc2
organizace	organizace	k1gFnSc2
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1956	#num#	k4
<g/>
:	:	kIx,
Jméno	jméno	k1gNnSc4
změněno	změněn	k2eAgNnSc4d1
z	z	k7c2
International	International	k1gFnSc2
Union	union	k1gInSc1
for	forum	k1gNnPc2
the	the	k?
Preservation	Preservation	k1gInSc1
of	of	k?
Nature	Natur	k1gMnSc5
(	(	kIx(
<g/>
IUPN	IUPN	kA
<g/>
,	,	kIx,
Mezinárodní	mezinárodní	k2eAgInSc1d1
svaz	svaz	k1gInSc1
za	za	k7c4
zachování	zachování	k1gNnSc4
přírody	příroda	k1gFnSc2
<g/>
)	)	kIx)
na	na	k7c6
International	International	k1gFnSc6
Union	union	k1gInSc1
for	forum	k1gNnPc2
the	the	k?
Conservation	Conservation	k1gInSc1
of	of	k?
Nature	Natur	k1gMnSc5
and	and	k?
Natural	Natural	k?
Resources	Resources	k1gMnSc1
(	(	kIx(
<g/>
IUCN	IUCN	kA
<g/>
,	,	kIx,
Mezinárodní	mezinárodní	k2eAgInSc1d1
svaz	svaz	k1gInSc1
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
a	a	k8xC
přírodních	přírodní	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
<g/>
)	)	kIx)
</s>
<s>
1959	#num#	k4
<g/>
:	:	kIx,
UNESCO	Unesco	k1gNnSc1
se	se	k3xPyFc4
rozhodlo	rozhodnout	k5eAaPmAgNnS
sestavit	sestavit	k5eAaPmF
mezinárodní	mezinárodní	k2eAgInSc4d1
seznam	seznam	k1gInSc4
přírodních	přírodní	k2eAgInPc2d1
parků	park	k1gInPc2
a	a	k8xC
obdobných	obdobný	k2eAgFnPc2d1
rezervací	rezervace	k1gFnPc2
a	a	k8xC
generální	generální	k2eAgMnSc1d1
tajemník	tajemník	k1gMnSc1
OSN	OSN	kA
požádal	požádat	k5eAaPmAgMnS
IUCN	IUCN	kA
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
tento	tento	k3xDgInSc4
seznam	seznam	k1gInSc4
připravila	připravit	k5eAaPmAgFnS
</s>
<s>
1961	#num#	k4
<g/>
:	:	kIx,
Po	po	k7c6
více	hodně	k6eAd2
než	než	k8xS
desetiletí	desetiletí	k1gNnSc4
problémů	problém	k1gInPc2
s	s	k7c7
financováním	financování	k1gNnSc7
organizace	organizace	k1gFnSc2
se	se	k3xPyFc4
přední	přední	k2eAgFnSc2d1
osobnosti	osobnost	k1gFnSc2
vědy	věda	k1gFnSc2
i	i	k8xC
podnikatelské	podnikatelský	k2eAgFnSc2d1
sféry	sféra	k1gFnSc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
Sira	sir	k1gMnSc2
Juliana	Julian	k1gMnSc2
Huxleyho	Huxley	k1gMnSc2
<g/>
)	)	kIx)
rozhodli	rozhodnout	k5eAaPmAgMnP
založit	založit	k5eAaPmF
doplňkový	doplňkový	k2eAgInSc1d1
fond	fond	k1gInSc1
(	(	kIx(
<g/>
World	World	k1gInSc1
Wildlife	Wildlif	k1gInSc5
Fund	fund	k1gInSc1
<g/>
,	,	kIx,
WWF	WWF	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
zaměří	zaměřit	k5eAaPmIp3nS
na	na	k7c4
vztahy	vztah	k1gInPc4
s	s	k7c7
veřejností	veřejnost	k1gFnSc7
a	a	k8xC
zvyšování	zvyšování	k1gNnSc1
podpory	podpora	k1gFnSc2
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
</s>
<s>
1969	#num#	k4
<g/>
:	:	kIx,
IUCN	IUCN	kA
získává	získávat	k5eAaImIp3nS
grant	grant	k1gInSc4
Fordovy	Fordův	k2eAgFnSc2d1
nadace	nadace	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
jí	on	k3xPp3gFnSc3
umožňuje	umožňovat	k5eAaImIp3nS
podstatně	podstatně	k6eAd1
rozšířit	rozšířit	k5eAaPmF
svůj	svůj	k3xOyFgInSc4
mezinárodní	mezinárodní	k2eAgInSc4d1
sekretariát	sekretariát	k1gInSc4
</s>
<s>
1972	#num#	k4
<g/>
:	:	kIx,
UNESCO	Unesco	k1gNnSc1
přijímá	přijímat	k5eAaImIp3nS
Konvenci	konvence	k1gFnSc4
o	o	k7c6
ochraně	ochrana	k1gFnSc6
světového	světový	k2eAgNnSc2d1
kulturního	kulturní	k2eAgNnSc2d1
a	a	k8xC
přírodního	přírodní	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
a	a	k8xC
IUCN	IUCN	kA
je	být	k5eAaImIp3nS
požádána	požádán	k2eAgFnSc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zajistila	zajistit	k5eAaPmAgFnS
sledování	sledování	k1gNnSc4
a	a	k8xC
vyhodnocování	vyhodnocování	k1gNnSc4
po	po	k7c6
technické	technický	k2eAgFnSc6d1
stránce	stránka	k1gFnSc6
</s>
<s>
1974	#num#	k4
<g/>
:	:	kIx,
IUCN	IUCN	kA
se	se	k3xPyFc4
zasazuje	zasazovat	k5eAaImIp3nS
o	o	k7c4
získání	získání	k1gNnSc4
souhlasu	souhlas	k1gInSc2
svých	svůj	k3xOyFgMnPc2
členů	člen	k1gMnPc2
s	s	k7c7
Konvencí	konvence	k1gFnSc7
o	o	k7c6
mezinárodním	mezinárodní	k2eAgInSc6d1
obchodu	obchod	k1gInSc6
s	s	k7c7
ohroženými	ohrožený	k2eAgInPc7d1
druhy	druh	k1gInPc7
divokých	divoký	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
a	a	k8xC
zvířat	zvíře	k1gNnPc2
(	(	kIx(
<g/>
CITES	CITES	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gInSc1
sekretariát	sekretariát	k1gInSc1
původně	původně	k6eAd1
sídlil	sídlit	k5eAaImAgInS
v	v	k7c6
IUCN	IUCN	kA
</s>
<s>
1975	#num#	k4
<g/>
:	:	kIx,
Nabývá	nabývat	k5eAaImIp3nS
účinnosti	účinnost	k1gFnPc4
Konvence	konvence	k1gFnSc2
o	o	k7c6
mokřadech	mokřad	k1gInPc6
mezinárodní	mezinárodní	k2eAgFnSc2d1
důležitosti	důležitost	k1gFnSc2
(	(	kIx(
<g/>
Ramsar	Ramsar	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
její	její	k3xOp3gInSc1
sekretariát	sekretariát	k1gInSc1
je	být	k5eAaImIp3nS
spravován	spravovat	k5eAaImNgInS
ze	z	k7c2
sídla	sídlo	k1gNnSc2
IUCN	IUCN	kA
</s>
<s>
1980	#num#	k4
<g/>
:	:	kIx,
IUCN	IUCN	kA
(	(	kIx(
<g/>
spolu	spolu	k6eAd1
s	s	k7c7
Programem	program	k1gInSc7
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
Spojených	spojený	k2eAgInPc2d1
národů	národ	k1gInPc2
<g/>
)	)	kIx)
a	a	k8xC
WWF	WWF	kA
spolupracují	spolupracovat	k5eAaImIp3nP
s	s	k7c7
UNESCO	UNESCO	kA
na	na	k7c6
vydání	vydání	k1gNnSc6
Světové	světový	k2eAgFnSc2d1
strategie	strategie	k1gFnSc2
ochrany	ochrana	k1gFnSc2
(	(	kIx(
<g/>
přírody	příroda	k1gFnSc2
a	a	k8xC
památek	památka	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
1982	#num#	k4
<g/>
:	:	kIx,
Valné	valný	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
OSN	OSN	kA
přijímá	přijímat	k5eAaImIp3nS
Světovou	světový	k2eAgFnSc4d1
chartu	charta	k1gFnSc4
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
<g/>
,	,	kIx,
připravenou	připravený	k2eAgFnSc4d1
IUCN	IUCN	kA
</s>
<s>
1990	#num#	k4
<g/>
:	:	kIx,
Organizace	organizace	k1gFnSc1
začala	začít	k5eAaPmAgFnS
používat	používat	k5eAaImF
název	název	k1gInSc4
World	World	k1gInSc1
Conservation	Conservation	k1gInSc1
Union	union	k1gInSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
Světový	světový	k2eAgInSc1d1
ochranářský	ochranářský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
ponechala	ponechat	k5eAaPmAgFnS
si	se	k3xPyFc3
zkratku	zkratka	k1gFnSc4
IUCN	IUCN	kA
</s>
<s>
1993	#num#	k4
<g/>
:	:	kIx,
IUCN	IUCN	kA
(	(	kIx(
<g/>
spolu	spolu	k6eAd1
s	s	k7c7
Programem	program	k1gInSc7
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
OSN	OSN	kA
a	a	k8xC
s	s	k7c7
WWF	WWF	kA
<g/>
)	)	kIx)
vydává	vydávat	k5eAaImIp3nS,k5eAaPmIp3nS
Caring	Caring	k1gInSc1
for	forum	k1gNnPc2
the	the	k?
Earth	Earth	k1gMnSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
Péče	péče	k1gFnSc1
o	o	k7c6
Zemi	zem	k1gFnSc6
<g/>
“	“	k?
<g/>
)	)	kIx)
</s>
<s>
2008	#num#	k4
<g/>
:	:	kIx,
Organizace	organizace	k1gFnSc1
přestala	přestat	k5eAaPmAgFnS
používat	používat	k5eAaImF
název	název	k1gInSc4
World	World	k1gInSc1
Conservation	Conservation	k1gInSc1
Union	union	k1gInSc1
a	a	k8xC
vrátila	vrátit	k5eAaPmAgFnS
se	se	k3xPyFc4
k	k	k7c3
International	International	k1gFnSc3
Union	union	k1gInSc1
for	forum	k1gNnPc2
Conservation	Conservation	k1gInSc1
of	of	k?
Nature	Natur	k1gInSc5
</s>
<s>
Organizační	organizační	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
</s>
<s>
Svaz	svaz	k1gInSc1
má	mít	k5eAaImIp3nS
tři	tři	k4xCgFnPc4
složky	složka	k1gFnPc4
<g/>
:	:	kIx,
své	svůj	k3xOyFgFnSc2
členské	členský	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
<g/>
,	,	kIx,
svých	svůj	k3xOyFgFnPc2
6	#num#	k4
vědeckých	vědecký	k2eAgFnPc2d1
komisí	komise	k1gFnPc2
a	a	k8xC
svůj	svůj	k3xOyFgInSc4
profesionální	profesionální	k2eAgInSc4d1
sekretariát	sekretariát	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Členové	člen	k1gMnPc1
</s>
<s>
Svaz	svaz	k1gInSc1
sdružuje	sdružovat	k5eAaImIp3nS
jak	jak	k6eAd1
státy	stát	k1gInPc4
<g/>
,	,	kIx,
tak	tak	k6eAd1
nevládní	vládní	k2eNgFnSc1d1
organizace	organizace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Členové	člen	k1gMnPc1
určují	určovat	k5eAaImIp3nP
postoje	postoj	k1gInPc4
Svazu	svaz	k1gInSc2
<g/>
,	,	kIx,
definují	definovat	k5eAaBmIp3nP
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
globální	globální	k2eAgInSc4d1
pracovní	pracovní	k2eAgInSc4d1
program	program	k1gInSc4
a	a	k8xC
volí	volit	k5eAaImIp3nP
jeho	jeho	k3xOp3gFnSc4
Radu	rada	k1gFnSc4
(	(	kIx(
<g/>
srovnatelnou	srovnatelný	k2eAgFnSc4d1
s	s	k7c7
představenstvem	představenstvo	k1gNnSc7
společnosti	společnost	k1gFnSc2
<g/>
)	)	kIx)
na	na	k7c6
Světovém	světový	k2eAgInSc6d1
ochranářském	ochranářský	k2eAgInSc6d1
kongresu	kongres	k1gInSc6
IUCN	IUCN	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Členské	členský	k2eAgFnPc1d1
organizace	organizace	k1gFnPc1
se	se	k3xPyFc4
sdružují	sdružovat	k5eAaImIp3nP
do	do	k7c2
národních	národní	k2eAgInPc2d1
a	a	k8xC
regionálních	regionální	k2eAgInPc2d1
výborů	výbor	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Členové	člen	k1gMnPc1
IUCN	IUCN	kA
z	z	k7c2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Agentura	agentura	k1gFnSc1
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
a	a	k8xC
krajiny	krajina	k1gFnSc2
ČR	ČR	kA
(	(	kIx(
<g/>
AOPK	AOPK	kA
ČR	ČR	kA
<g/>
)	)	kIx)
–	–	k?
Nature	Natur	k1gMnSc5
Conservation	Conservation	k1gInSc1
Agency	Agency	k1gInPc1
of	of	k?
the	the	k?
Czech	Czech	k1gInSc1
Republic	Republice	k1gFnPc2
(	(	kIx(
<g/>
NCA	NCA	kA
CR	cr	k0
<g/>
)	)	kIx)
</s>
<s>
Český	český	k2eAgInSc1d1
svaz	svaz	k1gInSc1
ochránců	ochránce	k1gMnPc2
přírody	příroda	k1gFnSc2
(	(	kIx(
<g/>
ÚVR	ÚVR	kA
ČSOP	ČSOP	kA
<g/>
)	)	kIx)
–	–	k?
Czech	Czech	k1gInSc1
Union	union	k1gInSc1
for	forum	k1gNnPc2
Nature	Natur	k1gMnSc5
Conservation	Conservation	k1gInSc1
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
ČR	ČR	kA
–	–	k?
Ministry	ministr	k1gMnPc4
of	of	k?
the	the	k?
Environment	Environment	k1gInSc1
of	of	k?
the	the	k?
Czech	Czech	k1gInSc1
Republic	Republice	k1gFnPc2
</s>
<s>
Správa	správa	k1gFnSc1
Krkonošského	krkonošský	k2eAgInSc2d1
národního	národní	k2eAgInSc2d1
parku	park	k1gInSc2
(	(	kIx(
<g/>
KRNAP	KRNAP	kA
<g/>
)	)	kIx)
–	–	k?
The	The	k1gFnSc4
Krkonose	Krkonosa	k1gFnSc3
Mountains	Mountainsa	k1gFnPc2
National	National	k1gFnSc2
Park	park	k1gInSc1
</s>
<s>
Unie	unie	k1gFnSc1
českých	český	k2eAgFnPc2d1
a	a	k8xC
slovenských	slovenský	k2eAgFnPc2d1
zoologických	zoologický	k2eAgFnPc2d1
zahrad	zahrada	k1gFnPc2
(	(	kIx(
<g/>
UCSZOO	UCSZOO	kA
<g/>
)	)	kIx)
–	–	k?
Union	union	k1gInSc1
of	of	k?
Czech	Czech	k1gInSc1
and	and	k?
Slovak	Slovak	k1gInSc1
Zoos	Zoos	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Komise	komise	k1gFnSc1
</s>
<s>
Sedm	sedm	k4xCc1
komisí	komise	k1gFnPc2
sleduje	sledovat	k5eAaImIp3nS
stav	stav	k1gInSc1
světových	světový	k2eAgInPc2d1
přírodních	přírodní	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
<g/>
,	,	kIx,
poskytuje	poskytovat	k5eAaImIp3nS
IUCN	IUCN	kA
know-how	know-how	k?
a	a	k8xC
poradenství	poradenství	k1gNnSc2
v	v	k7c6
ochranářských	ochranářský	k2eAgFnPc6d1
otázkách	otázka	k1gFnPc6
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Komise	komise	k1gFnSc1
pro	pro	k7c4
správu	správa	k1gFnSc4
ekosystémů	ekosystém	k1gInPc2
</s>
<s>
Commission	Commission	k1gInSc1
on	on	k3xPp3gMnSc1
Ecosystem	Ecosyst	k1gInSc7
Management	management	k1gInSc1
(	(	kIx(
<g/>
CEM	CEM	kA
<g/>
)	)	kIx)
<g/>
:	:	kIx,
CEM	CEM	kA
poskytuje	poskytovat	k5eAaImIp3nS
odborné	odborný	k2eAgNnSc4d1
poradenství	poradenství	k1gNnSc4
k	k	k7c3
zacházení	zacházení	k1gNnSc3
s	s	k7c7
přírodními	přírodní	k2eAgInPc7d1
i	i	k8xC
lidmi	člověk	k1gMnPc7
upravenými	upravený	k2eAgInPc7d1
ekosystémy	ekosystém	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
květnu	květen	k1gInSc6
2008	#num#	k4
měla	mít	k5eAaImAgFnS
400	#num#	k4
členů	člen	k1gMnPc2
a	a	k8xC
vedla	vést	k5eAaImAgFnS
ji	on	k3xPp3gFnSc4
předsedkyně	předsedkyně	k1gFnSc1
Hillary	Hillara	k1gFnSc2
Masundire	Masundir	k1gInSc5
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Komise	komise	k1gFnSc1
pro	pro	k7c4
vzdělávání	vzdělávání	k1gNnSc4
a	a	k8xC
komunikaci	komunikace	k1gFnSc4
</s>
<s>
Commission	Commission	k1gInSc1
on	on	k3xPp3gMnSc1
Education	Education	k1gInSc1
and	and	k?
Communication	Communication	k1gInSc4
(	(	kIx(
<g/>
CEC	cecat	k5eAaImRp2nS
<g/>
)	)	kIx)
<g/>
:	:	kIx,
CEC	cecat	k5eAaImRp2nS
prosazuje	prosazovat	k5eAaImIp3nS
strategické	strategický	k2eAgNnSc4d1
využívání	využívání	k1gNnSc4
komunikace	komunikace	k1gFnSc2
a	a	k8xC
vzdělávání	vzdělávání	k1gNnSc2
zainteresovaných	zainteresovaný	k2eAgFnPc2d1
osob	osoba	k1gFnPc2
o	o	k7c6
udržitelném	udržitelný	k2eAgNnSc6d1
využívání	využívání	k1gNnSc6
přírodních	přírodní	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
květnu	květen	k1gInSc6
2008	#num#	k4
měla	mít	k5eAaImAgFnS
500	#num#	k4
členů	člen	k1gMnPc2
a	a	k8xC
vedl	vést	k5eAaImAgMnS
ji	on	k3xPp3gFnSc4
předseda	předseda	k1gMnSc1
Keith	Keith	k1gMnSc1
Wheeler	Wheeler	k1gMnSc1
a	a	k8xC
místopředsedkyně	místopředsedkyně	k1gFnSc1
Juanita	Juanita	k1gFnSc1
Castañ	Castañ	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Komise	komise	k1gFnSc1
pro	pro	k7c4
environmentální	environmentální	k2eAgFnSc4d1
<g/>
,	,	kIx,
ekonomickou	ekonomický	k2eAgFnSc4d1
a	a	k8xC
sociální	sociální	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
</s>
<s>
Commission	Commission	k1gInSc4
on	on	k3xPp3gMnSc1
Environmental	Environmental	k1gMnSc1
<g/>
,	,	kIx,
Economic	Economic	k1gMnSc1
<g/>
,	,	kIx,
and	and	k?
Social	Social	k1gInSc1
Policy	Polica	k1gFnSc2
(	(	kIx(
<g/>
CEESP	CEESP	kA
<g/>
)	)	kIx)
<g/>
:	:	kIx,
CEESP	CEESP	kA
poskytuje	poskytovat	k5eAaImIp3nS
odborné	odborný	k2eAgFnPc4d1
znalosti	znalost	k1gFnPc4
a	a	k8xC
poradenství	poradenství	k1gNnSc4
k	k	k7c3
hospodářským	hospodářský	k2eAgInPc3d1
a	a	k8xC
sociálním	sociální	k2eAgInPc3d1
faktorům	faktor	k1gInPc3
a	a	k8xC
jejich	jejich	k3xOp3gFnSc1
vlivům	vliv	k1gInPc3
na	na	k7c4
ochranu	ochrana	k1gFnSc4
a	a	k8xC
udržitelné	udržitelný	k2eAgNnSc4d1
využívání	využívání	k1gNnSc4
biologické	biologický	k2eAgFnSc2d1
rozmanitosti	rozmanitost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
květnu	květen	k1gInSc6
2008	#num#	k4
měla	mít	k5eAaImAgFnS
500	#num#	k4
členů	člen	k1gMnPc2
a	a	k8xC
vedl	vést	k5eAaImAgMnS
ji	on	k3xPp3gFnSc4
předseda	předseda	k1gMnSc1
Taghi	Tagh	k1gFnSc2
Farvar	Farvar	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Komise	komise	k1gFnSc1
pro	pro	k7c4
zákony	zákon	k1gInPc4
o	o	k7c6
životním	životní	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
</s>
<s>
Commission	Commission	k1gInSc4
on	on	k3xPp3gMnSc1
Environmental	Environmental	k1gMnSc1
Law	Law	k1gMnSc1
(	(	kIx(
<g/>
CEL	cela	k1gFnPc2
<g/>
)	)	kIx)
<g/>
:	:	kIx,
CEL	cela	k1gFnPc2
rozšiřuje	rozšiřovat	k5eAaImIp3nS
legislativu	legislativa	k1gFnSc4
o	o	k7c6
životním	životní	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
<g/>
,	,	kIx,
vyvíjí	vyvíjet	k5eAaImIp3nS
nové	nový	k2eAgInPc4d1
právní	právní	k2eAgInPc4d1
koncepty	koncept	k1gInPc4
a	a	k8xC
nástroje	nástroj	k1gInPc4
a	a	k8xC
pomáhá	pomáhat	k5eAaImIp3nS
společnostem	společnost	k1gFnPc3
ve	v	k7c6
využívání	využívání	k1gNnSc6
legislativy	legislativa	k1gFnSc2
pro	pro	k7c4
ochranu	ochrana	k1gFnSc4
a	a	k8xC
udržitelný	udržitelný	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
květnu	květen	k1gInSc6
2008	#num#	k4
měla	mít	k5eAaImAgFnS
800	#num#	k4
členů	člen	k1gMnPc2
a	a	k8xC
vedla	vést	k5eAaImAgFnS
ji	on	k3xPp3gFnSc4
předsedkyně	předsedkyně	k1gFnSc1
Sheila	Sheila	k1gFnSc1
Abed	Abed	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Komise	komise	k1gFnSc1
pro	pro	k7c4
přežití	přežití	k1gNnSc4
druhů	druh	k1gInPc2
</s>
<s>
Species	species	k1gFnSc1
Survival	Survival	k1gMnSc1
Commission	Commission	k1gInSc1
(	(	kIx(
<g/>
SSC	SSC	kA
<g/>
)	)	kIx)
<g/>
:	:	kIx,
SSC	SSC	kA
radí	radit	k5eAaImIp3nS
Svazu	svaz	k1gInSc2
s	s	k7c7
technickými	technický	k2eAgInPc7d1
aspekty	aspekt	k1gInPc7
zachování	zachování	k1gNnSc1
druhů	druh	k1gInPc2
a	a	k8xC
mobilizuje	mobilizovat	k5eAaBmIp3nS
síly	síla	k1gFnPc4
na	na	k7c4
ochranu	ochrana	k1gFnSc4
těch	ten	k3xDgInPc2
druhů	druh	k1gInPc2
<g/>
,	,	kIx,
kterým	který	k3yQgInSc7,k3yRgInSc7,k3yIgInSc7
hrozí	hrozit	k5eAaImIp3nS
vyhubení	vyhubení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vydává	vydávat	k5eAaImIp3nS,k5eAaPmIp3nS
Červený	červený	k2eAgInSc4d1
seznam	seznam	k1gInSc4
ohrožených	ohrožený	k2eAgInPc2d1
druhů	druh	k1gInPc2
IUCN	IUCN	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
květnu	květen	k1gInSc6
2008	#num#	k4
bylo	být	k5eAaImAgNnS
jeho	jeho	k3xOp3gMnPc4
členy	člen	k1gMnPc4
přes	přes	k7c4
7000	#num#	k4
odborníků	odborník	k1gMnPc2
na	na	k7c4
ohrožené	ohrožený	k2eAgInPc4d1
druhy	druh	k1gInPc4
a	a	k8xC
biodiverzitu	biodiverzita	k1gFnSc4
z	z	k7c2
celého	celý	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
,	,	kIx,
předsedkyní	předsedkyně	k1gFnPc2
byla	být	k5eAaImAgFnS
Holly	Holla	k1gFnPc4
Dublin	Dublin	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Světová	světový	k2eAgFnSc1d1
komise	komise	k1gFnSc1
pro	pro	k7c4
chráněná	chráněný	k2eAgNnPc4d1
území	území	k1gNnPc4
</s>
<s>
World	World	k6eAd1
Commission	Commission	k1gInSc4
on	on	k3xPp3gMnSc1
Protected	Protected	k1gMnSc1
Areas	Areas	k1gMnSc1
(	(	kIx(
<g/>
WCPA	WCPA	kA
<g/>
)	)	kIx)
</s>
<s>
Sekretariát	sekretariát	k1gInSc1
</s>
<s>
Členové	člen	k1gMnPc1
a	a	k8xC
komise	komise	k1gFnPc1
spolupracují	spolupracovat	k5eAaImIp3nP
s	s	k7c7
placeným	placený	k2eAgInSc7d1
sekretariátem	sekretariát	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
tvoří	tvořit	k5eAaImIp3nS
přes	přes	k7c4
1100	#num#	k4
zaměstnanců	zaměstnanec	k1gMnPc2
v	v	k7c6
62	#num#	k4
zemích	zem	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Generální	generální	k2eAgFnSc7d1
ředitelkou	ředitelka	k1gFnSc7
je	být	k5eAaImIp3nS
od	od	k7c2
ledna	leden	k1gInSc2
2015	#num#	k4
ekoložka	ekoložka	k1gFnSc1
a	a	k8xC
ekonomka	ekonomka	k1gFnSc1
Inger	Inger	k1gMnSc1
Andersen	Andersen	k1gMnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Od	od	k7c2
2	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2007	#num#	k4
byla	být	k5eAaImAgFnS
generální	generální	k2eAgFnSc7d1
ředitelkou	ředitelka	k1gFnSc7
Julia	Julius	k1gMnSc2
Marton-Lefè	Marton-Lefè	k1gInSc5
—	—	k?
světově	světově	k6eAd1
uznávaná	uznávaný	k2eAgFnSc1d1
odbornice	odbornice	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
v	v	k7c4
USA	USA	kA
a	a	k8xC
ve	v	k7c6
Francii	Francie	k1gFnSc6
studovala	studovat	k5eAaImAgFnS
historii	historie	k1gFnSc4
<g/>
,	,	kIx,
ekologii	ekologie	k1gFnSc4
a	a	k8xC
plánování	plánování	k1gNnSc4
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
postu	post	k1gInSc6
vystřídala	vystřídat	k5eAaPmAgFnS
ekologa	ekolog	k1gMnSc4
Achima	Achim	k1gMnSc4
Steinera	Steiner	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
v	v	k7c6
červnu	červen	k1gInSc6
2006	#num#	k4
stal	stát	k5eAaPmAgMnS
výkonným	výkonný	k2eAgMnSc7d1
ředitelem	ředitel	k1gMnSc7
Programu	program	k1gInSc2
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
OSN	OSN	kA
(	(	kIx(
<g/>
UNEP	UNEP	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1
výstupy	výstup	k1gInPc1
</s>
<s>
Mezi	mezi	k7c4
nejvýznamnější	významný	k2eAgInPc4d3
produkty	produkt	k1gInPc4
a	a	k8xC
služby	služba	k1gFnSc2
IUCN	IUCN	kA
patří	patřit	k5eAaImIp3nP
<g/>
:	:	kIx,
</s>
<s>
Kategorizace	kategorizace	k1gFnSc1
chráněných	chráněný	k2eAgNnPc2d1
území	území	k1gNnPc2
(	(	kIx(
<g/>
kategorie	kategorie	k1gFnSc2
IUCN	IUCN	kA
<g/>
)	)	kIx)
</s>
<s>
Červený	červený	k2eAgInSc4d1
seznam	seznam	k1gInSc4
IUCN	IUCN	kA
ohrožených	ohrožený	k2eAgInPc2d1
druhů	druh	k1gInPc2
</s>
<s>
Kategorie	kategorie	k1gFnSc1
chráněných	chráněný	k2eAgNnPc2d1
území	území	k1gNnPc2
IUCN	IUCN	kA
</s>
<s>
Komise	komise	k1gFnSc1
pro	pro	k7c4
chráněná	chráněný	k2eAgNnPc4d1
území	území	k1gNnPc4
definovala	definovat	k5eAaBmAgFnS
následující	následující	k2eAgFnSc1d1
mezinárodní	mezinárodní	k2eAgFnSc1d1
kategorie	kategorie	k1gFnSc1
chráněných	chráněný	k2eAgNnPc2d1
území	území	k1gNnPc2
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
:	:	kIx,
</s>
<s>
Ia	ia	k0
–	–	k?
Přísná	přísný	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnSc1d1
rezervace	rezervace	k1gFnSc1
(	(	kIx(
<g/>
Natural	Natural	k?
Reserve	Reserev	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Oblast	oblast	k1gFnSc1
země	země	k1gFnSc1
nebo	nebo	k8xC
moře	moře	k1gNnSc1
obsahující	obsahující	k2eAgInPc1d1
mimořádné	mimořádný	k2eAgInPc1d1
nebo	nebo	k8xC
reprezentativní	reprezentativní	k2eAgInPc1d1
ekosystémy	ekosystém	k1gInPc1
<g/>
,	,	kIx,
geologické	geologický	k2eAgFnPc1d1
nebo	nebo	k8xC
fyziologické	fyziologický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
a	a	k8xC
<g/>
/	/	kIx~
<g/>
nebo	nebo	k8xC
druhy	druh	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
přístupná	přístupný	k2eAgFnSc1d1
primárně	primárně	k6eAd1
pro	pro	k7c4
vědecký	vědecký	k2eAgInSc4d1
výzkum	výzkum	k1gInSc4
a	a	k8xC
sledování	sledování	k1gNnSc4
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ib	Ib	k?
–	–	k?
Divočina	divočina	k1gFnSc1
(	(	kIx(
<g/>
Wilderness	Wilderness	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Rozsáhlá	rozsáhlý	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
původní	původní	k2eAgFnSc1d1
nebo	nebo	k8xC
jen	jen	k9
lehce	lehko	k6eAd1
pozměněné	pozměněný	k2eAgFnPc1d1
země	zem	k1gFnPc1
a	a	k8xC
<g/>
/	/	kIx~
<g/>
nebo	nebo	k8xC
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
si	se	k3xPyFc3
zachovává	zachovávat	k5eAaImIp3nS
svůj	svůj	k3xOyFgInSc4
přírodní	přírodní	k2eAgInSc4d1
charakter	charakter	k1gInSc4
<g/>
,	,	kIx,
bez	bez	k7c2
trvalého	trvalý	k2eAgNnSc2d1
nebo	nebo	k8xC
významného	významný	k2eAgNnSc2d1
osídlení	osídlení	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
chráněna	chránit	k5eAaImNgFnS
a	a	k8xC
spravována	spravovat	k5eAaImNgFnS
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
uchovala	uchovat	k5eAaPmAgFnS
v	v	k7c6
nedotčeném	dotčený	k2eNgInSc6d1
přírodním	přírodní	k2eAgInSc6d1
stavu	stav	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
II	II	kA
–	–	k?
Národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
(	(	kIx(
<g/>
National	National	k1gFnSc1
Park	park	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Přírodní	přírodní	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
země	zem	k1gFnSc2
a	a	k8xC
<g/>
/	/	kIx~
<g/>
nebo	nebo	k8xC
moře	moře	k1gNnSc1
<g/>
,	,	kIx,
určená	určený	k2eAgFnSc1d1
k	k	k7c3
<g/>
:	:	kIx,
i.	i.	k?
ochraně	ochrana	k1gFnSc6
ekologické	ekologický	k2eAgFnSc2d1
integrity	integrita	k1gFnSc2
jednoho	jeden	k4xCgMnSc2
nebo	nebo	k8xC
více	hodně	k6eAd2
ekosystémů	ekosystém	k1gInPc2
pro	pro	k7c4
současnou	současný	k2eAgFnSc4d1
i	i	k8xC
pro	pro	k7c4
budoucí	budoucí	k2eAgFnPc4d1
generace	generace	k1gFnPc4
<g/>
;	;	kIx,
ii	ii	k?
<g/>
.	.	kIx.
vyloučení	vyloučení	k1gNnSc1
využívání	využívání	k1gNnSc2
neslučitelného	slučitelný	k2eNgNnSc2d1
s	s	k7c7
účely	účel	k1gInPc7
ochrany	ochrana	k1gFnSc2
daného	daný	k2eAgNnSc2d1
území	území	k1gNnSc2
<g/>
;	;	kIx,
iii	iii	k?
<g/>
.	.	kIx.
poskytování	poskytování	k1gNnSc1
duchovních	duchovní	k2eAgFnPc2d1
<g/>
,	,	kIx,
vědeckých	vědecký	k2eAgFnPc2d1
<g/>
,	,	kIx,
vzdělávacích	vzdělávací	k2eAgFnPc2d1
<g/>
,	,	kIx,
rekreačních	rekreační	k2eAgFnPc2d1
a	a	k8xC
návštěvnických	návštěvnický	k2eAgFnPc2d1
možností	možnost	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
slučitelné	slučitelný	k2eAgNnSc1d1
s	s	k7c7
ochranou	ochrana	k1gFnSc7
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
a	a	k8xC
kultury	kultura	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
III	III	kA
–	–	k?
Přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
(	(	kIx(
<g/>
National	National	k1gFnSc1
Monument	monument	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Oblast	oblast	k1gFnSc1
obsahující	obsahující	k2eAgFnSc2d1
jeden	jeden	k4xCgInSc4
nebo	nebo	k8xC
více	hodně	k6eAd2
specifických	specifický	k2eAgInPc2d1
přírodních	přírodní	k2eAgInPc2d1
nebo	nebo	k8xC
přírodně-kulturních	přírodně-kulturní	k2eAgInPc2d1
rysů	rys	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
mají	mít	k5eAaImIp3nP
mimořádnou	mimořádný	k2eAgFnSc4d1
nebo	nebo	k8xC
jedinečnou	jedinečný	k2eAgFnSc4d1
hodnotu	hodnota	k1gFnSc4
spočívající	spočívající	k2eAgFnSc7d1
v	v	k7c6
jejich	jejich	k3xOp3gFnSc6
vzácnosti	vzácnost	k1gFnSc6
<g/>
,	,	kIx,
reprezentativních	reprezentativní	k2eAgFnPc6d1
či	či	k8xC
estetických	estetický	k2eAgFnPc6d1
kvalitách	kvalita	k1gFnPc6
nebo	nebo	k8xC
kulturním	kulturní	k2eAgInSc6d1
významu	význam	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
IV	IV	kA
–	–	k?
Místo	místo	k7c2
výskytu	výskyt	k1gInSc2
druhu	druh	k1gInSc2
(	(	kIx(
<g/>
Habitat	Habitat	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Oblast	oblast	k1gFnSc1
země	zem	k1gFnSc2
nebo	nebo	k8xC
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
aktivním	aktivní	k2eAgInPc3d1
zásahům	zásah	k1gInPc3
správy	správa	k1gFnSc2
území	území	k1gNnSc2
za	za	k7c7
účelem	účel	k1gInSc7
ochrany	ochrana	k1gFnSc2
přirozeného	přirozený	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
a	a	k8xC
uspokojení	uspokojení	k1gNnSc1
potřeb	potřeba	k1gFnPc2
konkrétních	konkrétní	k2eAgInPc2d1
druhů	druh	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
–	–	k?
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
(	(	kIx(
<g/>
Protected	Protected	k1gInSc1
Landscape	Landscap	k1gInSc5
<g/>
/	/	kIx~
<g/>
Seascape	Seascap	k1gMnSc5
<g/>
)	)	kIx)
</s>
<s>
Oblast	oblast	k1gFnSc1
země	zem	k1gFnSc2
<g/>
,	,	kIx,
případně	případně	k6eAd1
pobřeží	pobřeží	k1gNnSc2
a	a	k8xC
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
letitou	letitý	k2eAgFnSc7d1
interakcí	interakce	k1gFnSc7
člověka	člověk	k1gMnSc2
a	a	k8xC
přírody	příroda	k1gFnSc2
vznikla	vzniknout	k5eAaPmAgFnS
krajina	krajina	k1gFnSc1
významné	významný	k2eAgFnSc2d1
estetické	estetický	k2eAgFnSc2d1
<g/>
,	,	kIx,
ekologické	ekologický	k2eAgFnSc2d1
nebo	nebo	k8xC
kulturní	kulturní	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
<g/>
,	,	kIx,
často	často	k6eAd1
s	s	k7c7
vysokou	vysoký	k2eAgFnSc7d1
biologickou	biologický	k2eAgFnSc7d1
rozmanitostí	rozmanitost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
ochranu	ochrana	k1gFnSc4
<g/>
,	,	kIx,
správu	správa	k1gFnSc4
a	a	k8xC
vývoj	vývoj	k1gInSc4
takové	takový	k3xDgFnSc2
oblasti	oblast	k1gFnSc2
je	být	k5eAaImIp3nS
důležité	důležitý	k2eAgNnSc1d1
zachování	zachování	k1gNnSc1
zmíněné	zmíněný	k2eAgFnSc2d1
interakce	interakce	k1gFnSc2
člověka	člověk	k1gMnSc2
s	s	k7c7
přírodou	příroda	k1gFnSc7
v	v	k7c6
její	její	k3xOp3gFnSc6
tradiční	tradiční	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
VI	VI	kA
–	–	k?
Oblast	oblast	k1gFnSc1
ochrany	ochrana	k1gFnSc2
přírodních	přírodní	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
(	(	kIx(
<g/>
Managed	Managed	k1gInSc4
Resource	Resourka	k1gFnSc3
Protected	Protected	k1gInSc1
Area	area	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Oblast	oblast	k1gFnSc1
obsahující	obsahující	k2eAgFnSc1d1
převážně	převážně	k6eAd1
nezměněné	změněný	k2eNgInPc4d1
přírodní	přírodní	k2eAgInPc4d1
systémy	systém	k1gInPc4
<g/>
,	,	kIx,
spravovaná	spravovaný	k2eAgNnPc4d1
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
zajistila	zajistit	k5eAaPmAgFnS
dlouhodobá	dlouhodobý	k2eAgFnSc1d1
ochrana	ochrana	k1gFnSc1
a	a	k8xC
správa	správa	k1gFnSc1
biologické	biologický	k2eAgFnSc2d1
rozmanitosti	rozmanitost	k1gFnSc2
za	za	k7c2
současného	současný	k2eAgNnSc2d1
udržitelného	udržitelný	k2eAgNnSc2d1
využívání	využívání	k1gNnSc2
přírodních	přírodní	k2eAgInPc2d1
produktů	produkt	k1gInPc2
a	a	k8xC
služeb	služba	k1gFnPc2
k	k	k7c3
uspokojení	uspokojení	k1gNnSc3
potřeb	potřeba	k1gFnPc2
komunity	komunita	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgInP
použity	použit	k2eAgInPc1d1
překlady	překlad	k1gInPc1
textů	text	k1gInPc2
z	z	k7c2
článků	článek	k1gInPc2
International	International	k1gFnSc1
Union	union	k1gInSc1
for	forum	k1gNnPc2
Conservation	Conservation	k1gInSc1
of	of	k?
Nature	Natur	k1gMnSc5
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
a	a	k8xC
World	World	k1gMnSc1
Commission	Commission	k1gInSc4
on	on	k3xPp3gMnSc1
Protected	Protected	k1gMnSc1
Areas	Areas	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
IUCN	IUCN	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
©	©	k?
<g/>
2019	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
z	z	k7c2
<g/>
:	:	kIx,
https://www.iucn.org/1	https://www.iucn.org/1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
CHRISTOFFERSEN	CHRISTOFFERSEN	kA
<g/>
,	,	kIx,
Leif	Leif	k1gMnSc1
E.	E.	kA
IUCN	IUCN	kA
<g/>
:	:	kIx,
A	a	k9
Bridge-Builder	Bridge-Builder	k1gInSc1
for	forum	k1gNnPc2
Nature	Natur	k1gMnSc5
Conservation	Conservation	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Green	Green	k1gInSc1
Globe	globus	k1gInSc5
Yearbook	Yearbook	k1gInSc1
of	of	k?
International	International	k1gFnSc3
Co-operation	Co-operation	k1gInSc4
on	on	k3xPp3gMnSc1
Environment	Environment	k1gMnSc1
and	and	k?
Development	Development	k1gMnSc1
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oxford	Oxford	k1gInSc1
(	(	kIx(
<g/>
UK	UK	kA
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Oxford	Oxford	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
59	#num#	k4
<g/>
–	–	k?
<g/>
69	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
-	-	kIx~
<g/>
823348	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
také	také	k9
z	z	k7c2
<g/>
:	:	kIx,
http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.512.1461&	http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.512.1461&	k1gInSc1
<g/>
↑	↑	k?
CHRISTOFFERSEN	CHRISTOFFERSEN	kA
<g/>
,	,	kIx,
Leif	Leif	k1gMnSc1
E.	E.	kA
IUCN	IUCN	kA
<g/>
:	:	kIx,
A	a	k9
Bridge-Builder	Bridge-Builder	k1gInSc1
for	forum	k1gNnPc2
Nature	Natur	k1gMnSc5
Conservation	Conservation	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Green	Green	k1gInSc1
Globe	globus	k1gInSc5
Yearbook	Yearbook	k1gInSc1
of	of	k?
International	International	k1gFnSc3
Co-operation	Co-operation	k1gInSc4
on	on	k3xPp3gMnSc1
Environment	Environment	k1gMnSc1
and	and	k?
Development	Development	k1gMnSc1
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oxford	Oxford	k1gInSc1
(	(	kIx(
<g/>
UK	UK	kA
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Oxford	Oxford	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
61	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
-	-	kIx~
<g/>
823348	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
také	také	k9
z	z	k7c2
<g/>
:	:	kIx,
http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.512.1461&	http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.512.1461&	k1gInSc1
<g/>
↑	↑	k?
IUCN	IUCN	kA
Members	Members	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
IUCN	IUCN	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
©	©	k?
<g/>
2019	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přístup	přístup	k1gInSc1
z	z	k7c2
<g/>
:	:	kIx,
https://www.iucn.org/about/members/iucn-members	https://www.iucn.org/about/members/iucn-members	k1gInSc1
<g/>
↑	↑	k?
IUCN	IUCN	kA
-	-	kIx~
Commissions	Commissions	k1gInSc1
<g/>
.	.	kIx.
cms	cms	k?
<g/>
.	.	kIx.
<g/>
iucn	iucn	k1gMnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
IUCN	IUCN	kA
-	-	kIx~
Commission	Commission	k1gInSc1
Chairs	Chairs	k1gInSc1
<g/>
.	.	kIx.
cms	cms	k?
<g/>
.	.	kIx.
<g/>
iucn	iucn	k1gMnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
BADMAN	BADMAN	kA
<g/>
,	,	kIx,
Tim	Tim	k?
<g/>
,	,	kIx,
BOMHARD	BOMHARD	kA
<g/>
,	,	kIx,
Bastian	Bastian	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
World	World	k1gInSc1
Heritage	Heritag	k1gFnSc2
and	and	k?
Protected	Protected	k1gMnSc1
Areas	Areas	k1gMnSc1
2008	#num#	k4
Edition	Edition	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
IUCN	IUCN	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Červený	červený	k2eAgInSc1d1
seznam	seznam	k1gInSc1
IUCN	IUCN	kA
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Mezinárodní	mezinárodní	k2eAgInSc4d1
svaz	svaz	k1gInSc4
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
IUCN	IUCN	kA
</s>
<s>
Review	Review	k?
of	of	k?
the	the	k?
2008	#num#	k4
Red	Red	k1gFnSc2
List	lista	k1gFnPc2
of	of	k?
Threatened	Threatened	k1gMnSc1
Species	species	k1gFnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ko	ko	k?
<g/>
2008471710	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
30267-3	30267-3	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
8486	#num#	k4
2070	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79058420	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
135664451	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79058420	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
