<s>
Karel	Karel	k1gMnSc1	Karel
Jaromír	Jaromír	k1gMnSc1	Jaromír
Erben	Erben	k1gMnSc1	Erben
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1811	[number]	k4	1811
Miletín	Miletín	k1gInSc1	Miletín
–	–	k?	–
21.	[number]	k4	21.
listopadu	listopad	k1gInSc2	listopad
1870	[number]	k4	1870
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
<g/>
,	,	kIx,	,
archivář	archivář	k1gMnSc1	archivář
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
překladatel	překladatel	k1gMnSc1	překladatel
a	a	k8xC	a
sběratel	sběratel	k1gMnSc1	sběratel
českých	český	k2eAgFnPc2d1	Česká
lidových	lidový	k2eAgFnPc2d1	lidová
písní	píseň	k1gFnPc2	píseň
a	a	k8xC	a
pohádek	pohádka	k1gFnPc2	pohádka
<g/>
;	;	kIx,	;
představitel	představitel	k1gMnSc1	představitel
literárního	literární	k2eAgInSc2d1	literární
romantismu	romantismus	k1gInSc2	romantismus
<g/>
.	.	kIx.	.
</s>
