<s>
Jak	jak	k6eAd1
se	se	k3xPyFc4
jmenuje	jmenovat	k5eAaImIp3nS,k5eAaBmIp3nS
ekonom	ekonom	k1gMnSc1
rakouské	rakouský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
založil	založit	k5eAaPmAgMnS
proud	proud	k1gInSc4
tržního	tržní	k2eAgInSc2d1
anarchismu	anarchismus	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
nazýval	nazývat	k5eAaImAgInS
anarchokapitalismus	anarchokapitalismus	k1gInSc4
<g/>
?	?	kIx.
</s>