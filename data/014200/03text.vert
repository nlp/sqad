<s>
Murray	Murraa	k1gFnPc1
Rothbard	Rothbarda	k1gFnPc2
</s>
<s>
Murray	Murraa	k1gMnSc2
Newton	Newton	k1gMnSc1
Rothbard	Rothbard	k1gMnSc1
</s>
<s>
Datum	datum	k1gNnSc1
narození	narození	k1gNnSc2
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
1926	#num#	k4
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
New	New	k?
York	York	k1gInSc1
Datum	datum	k1gNnSc1
úmrtí	úmrť	k1gFnPc2
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
1995	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
68	#num#	k4
<g/>
)	)	kIx)
Místo	místo	k7c2
úmrtí	úmrtí	k1gNnSc2
</s>
<s>
New	New	k?
York	York	k1gInSc1
Národnost	národnost	k1gFnSc1
</s>
<s>
Americká	americký	k2eAgFnSc1d1
Ekonomická	ekonomický	k2eAgFnSc1d1
škola	škola	k1gFnSc1
</s>
<s>
Rakouská	rakouský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
Instituce	instituce	k1gFnSc2
</s>
<s>
New	New	k?
York	York	k1gInSc1
University	universita	k1gFnSc2
University	universita	k1gFnSc2
of	of	k?
Nevada	Nevada	k1gFnSc1
Pole	pole	k1gFnSc2
působení	působení	k1gNnSc2
</s>
<s>
Ekonomie	ekonomie	k1gFnSc1
Politická	politický	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
Přirozené	přirozený	k2eAgNnSc1d1
právo	právo	k1gNnSc1
Právní	právní	k2eAgFnSc2d1
filozofie	filozofie	k1gFnSc2
Hospodářské	hospodářský	k2eAgFnPc1d1
dějiny	dějiny	k1gFnPc1
Vzdělání	vzdělání	k1gNnSc1
</s>
<s>
Columbia	Columbia	k1gFnSc1
University	universita	k1gFnSc2
New	New	k1gFnSc2
York	York	k1gInSc1
University	universita	k1gFnSc2
Vlivy	vliv	k1gInPc5
</s>
<s>
Eugen	Eugen	k2eAgInSc1d1
Böhm	Böhm	k1gInSc1
von	von	k1gInSc1
Bawerk	Bawerk	k1gInSc1
Friedrich	Friedrich	k1gMnSc1
August	August	k1gMnSc1
von	von	k1gInSc4
Hayek	Hayek	k1gMnSc1
Carl	Carl	k1gMnSc1
Menger	Menger	k1gMnSc1
Ludwig	Ludwiga	k1gFnPc2
von	von	k1gInSc1
Mises	Mises	k1gMnSc1
Joseph	Joseph	k1gMnSc1
Schumpeter	Schumpeter	k1gMnSc1
Ovlivnil	ovlivnit	k5eAaPmAgMnS
</s>
<s>
Robert	Robert	k1gMnSc1
Nozick	Nozick	k1gMnSc1
Thomas	Thomas	k1gMnSc1
DiLorenzo	DiLorenza	k1gFnSc5
Robert	Robert	k1gMnSc1
Higgs	Higgs	k1gInSc4
Příspěvky	příspěvek	k1gInPc1
</s>
<s>
Anarchokapitalismus	Anarchokapitalismus	k1gInSc1
Některá	některý	k3yIgNnPc4
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Murray	Murray	k1gMnSc1
Newton	Newton	k1gMnSc1
Rothbard	Rothbard	k1gMnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1926	#num#	k4
–	–	k?
7	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1995	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
americký	americký	k2eAgMnSc1d1
ekonom	ekonom	k1gMnSc1
rakouské	rakouský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
pomohl	pomoct	k5eAaPmAgInS
definovat	definovat	k5eAaBmF
moderní	moderní	k2eAgInSc1d1
libertarianismus	libertarianismus	k1gInSc1
a	a	k8xC
založil	založit	k5eAaPmAgInS
proud	proud	k1gInSc1
tržního	tržní	k2eAgInSc2d1
anarchismu	anarchismus	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
nazýval	nazývat	k5eAaImAgInS
anarchokapitalismus	anarchokapitalismus	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Rothbard	Rothbard	k1gMnSc1
kladl	klást	k5eAaImAgMnS
důraz	důraz	k1gInSc4
na	na	k7c4
spontánní	spontánní	k2eAgInSc4d1
řád	řád	k1gInSc4
a	a	k8xC
prosazování	prosazování	k1gNnSc4
individuálního	individuální	k2eAgInSc2d1
anarchismu	anarchismus	k1gInSc2
a	a	k8xC
odsuzoval	odsuzovat	k5eAaImAgMnS
centrální	centrální	k2eAgNnSc4d1
plánování	plánování	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Život	život	k1gInSc1
a	a	k8xC
dílo	dílo	k1gNnSc1
</s>
<s>
Rothbard	Rothbard	k1gMnSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
Davidu	David	k1gMnSc3
a	a	k8xC
Rae	Rae	k1gMnSc3
Rothbardovým	Rothbardův	k2eAgMnPc3d1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
ho	on	k3xPp3gMnSc4
vychovávali	vychovávat	k5eAaImAgMnP
v	v	k7c6
židovské	židovský	k2eAgFnSc6d1
rodině	rodina	k1gFnSc6
v	v	k7c6
Bronxu	Bronx	k1gInSc6
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Vyrůstal	vyrůstat	k5eAaImAgMnS
jsem	být	k5eAaImIp1nS
v	v	k7c6
komunistické	komunistický	k2eAgFnSc6d1
kultuře	kultura	k1gFnSc6
<g/>
,	,	kIx,
<g/>
"	"	kIx"
vzpomínal	vzpomínat	k5eAaImAgMnS
později	pozdě	k6eAd2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Studoval	studovat	k5eAaImAgInS
Columbia	Columbia	k1gFnSc1
University	universita	k1gFnPc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
získal	získat	k5eAaPmAgInS
titul	titul	k1gInSc1
bakalář	bakalář	k1gMnSc1
v	v	k7c6
matematice	matematika	k1gFnSc6
a	a	k8xC
ekonomii	ekonomie	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
a	a	k8xC
titul	titul	k1gInSc1
magistr	magistr	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1946	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titul	titul	k1gInSc4
doktor	doktor	k1gMnSc1
filosofie	filosofie	k1gFnSc2
za	za	k7c4
ekonomii	ekonomie	k1gFnSc4
obdržel	obdržet	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1956	#num#	k4
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Arthura	Arthur	k1gMnSc2
Burnse	Burns	k1gMnSc2
<g/>
,	,	kIx,
pozdějšího	pozdní	k2eAgMnSc2d2
předsedy	předseda	k1gMnSc2
Fedu	Fedus	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Počátkem	počátkem	k7c2
50	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
studoval	studovat	k5eAaImAgInS
u	u	k7c2
Ludwiga	Ludwig	k1gMnSc2
von	von	k1gInSc4
Misese	Misese	k1gFnPc1
na	na	k7c6
jeho	jeho	k3xOp3gInPc6
seminářích	seminář	k1gInPc6
na	na	k7c4
New	New	k1gFnSc4
York	York	k1gInSc4
University	universita	k1gFnSc2
a	a	k8xC
byl	být	k5eAaImAgInS
velmi	velmi	k6eAd1
ovlivněn	ovlivnit	k5eAaPmNgInS
Misesovou	Misesový	k2eAgFnSc7d1
knihou	kniha	k1gFnSc7
Lidské	lidský	k2eAgNnSc1d1
jednání	jednání	k1gNnSc1
(	(	kIx(
<g/>
Human	Human	k1gInSc1
Action	Action	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
50	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
pracoval	pracovat	k5eAaImAgInS
pro	pro	k7c4
klasicky	klasicky	k6eAd1
liberální	liberální	k2eAgInSc4d1
William	William	k1gInSc4
Volker	Volker	k1gInSc1
Fund	fund	k1gInSc1
na	na	k7c6
knižním	knižní	k2eAgInSc6d1
projektu	projekt	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
završen	završit	k5eAaPmNgInS
knihou	kniha	k1gFnSc7
Zásady	zásada	k1gFnSc2
ekonomie	ekonomie	k1gFnSc2
(	(	kIx(
<g/>
Man	Man	k1gMnSc1
<g/>
,	,	kIx,
Economy	Econom	k1gInPc1
<g/>
,	,	kIx,
and	and	k?
State	status	k1gInSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
publikovanou	publikovaný	k2eAgFnSc4d1
v	v	k7c6
roce	rok	k1gInSc6
1962	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1963	#num#	k4
až	až	k9
1985	#num#	k4
vyučoval	vyučovat	k5eAaImAgInS
na	na	k7c6
Polytechnickém	polytechnický	k2eAgInSc6d1
institutu	institut	k1gInSc6
New	New	k1gFnSc1
York	York	k1gInSc1
University	universita	k1gFnSc2
v	v	k7c6
Brooklynu	Brooklyn	k1gInSc6
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1986	#num#	k4
do	do	k7c2
své	svůj	k3xOyFgFnSc2
smrti	smrt	k1gFnSc2
byl	být	k5eAaImAgMnS
emeritním	emeritní	k2eAgMnSc7d1
profesorem	profesor	k1gMnSc7
na	na	k7c4
University	universita	k1gFnPc4
of	of	k?
Nevada	Nevada	k1gFnSc1
<g/>
,	,	kIx,
Las	laso	k1gNnPc2
Vegas	Vegasa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rothbard	Rothbard	k1gMnSc1
založil	založit	k5eAaPmAgMnS
Center	centrum	k1gNnPc2
for	forum	k1gNnPc2
Libertarian	Libertarian	k1gMnSc1
Studies	Studies	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1976	#num#	k4
a	a	k8xC
Journal	Journal	k1gMnSc1
of	of	k?
Libertarian	Libertarian	k1gMnSc1
Studies	Studies	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1977	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
spojen	spojit	k5eAaPmNgInS
se	s	k7c7
vznikem	vznik	k1gInSc7
Institutu	institut	k1gInSc2
Ludwiga	Ludwig	k1gMnSc2
von	von	k1gInSc1
Mises	Mises	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
1982	#num#	k4
a	a	k8xC
později	pozdě	k6eAd2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
jeho	jeho	k3xOp3gMnSc7
akademickým	akademický	k2eAgMnSc7d1
viceprezidentem	viceprezident	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1987	#num#	k4
zahájil	zahájit	k5eAaPmAgMnS
vydávání	vydávání	k1gNnSc4
"	"	kIx"
<g/>
Review	Review	k1gFnSc1
of	of	k?
Austrian	Austrian	k1gInSc1
Economics	Economicsa	k1gFnPc2
<g/>
,	,	kIx,
<g/>
"	"	kIx"
nyní	nyní	k6eAd1
nazývaný	nazývaný	k2eAgInSc1d1
Quarterly	Quarterl	k1gInPc7
Journal	Journal	k1gFnSc2
of	of	k?
Austrian	Austrian	k1gInSc1
Economics	Economics	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1953	#num#	k4
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
JoAnn	JoAnno	k1gNnPc2
Schumacher	Schumachra	k1gFnPc2
v	v	k7c6
New	New	k1gFnSc6
York	York	k1gInSc1
City	City	k1gFnPc2
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
nazýval	nazývat	k5eAaImAgInS
„	„	k?
<g/>
nepostradatelným	postradatelný	k2eNgInSc7d1
rámcem	rámec	k1gInSc7
<g/>
“	“	k?
pro	pro	k7c4
svůj	svůj	k3xOyFgInSc4
život	život	k1gInSc4
a	a	k8xC
dílo	dílo	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
na	na	k7c6
Manhattanu	Manhattan	k1gInSc6
na	na	k7c4
srdeční	srdeční	k2eAgInSc4d1
infarkt	infarkt	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nekrolog	nekrolog	k1gInSc1
v	v	k7c4
New	New	k1gFnSc4
York	York	k1gInSc1
Times	Times	k1gMnSc1
nazval	nazvat	k5eAaBmAgMnS,k5eAaPmAgMnS
Rothbarda	Rothbard	k1gMnSc4
"	"	kIx"
<g/>
ekonomem	ekonom	k1gMnSc7
a	a	k8xC
sociálním	sociální	k2eAgMnSc7d1
filosofem	filosof	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
ostře	ostro	k6eAd1
hájil	hájit	k5eAaImAgMnS
individuální	individuální	k2eAgFnSc4d1
svobodu	svoboda	k1gFnSc4
proti	proti	k7c3
vládním	vládní	k2eAgFnPc3d1
intervencím	intervence	k1gFnPc3
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
William	William	k1gInSc1
F.	F.	kA
Buckley	Bucklea	k1gFnSc2
napsal	napsat	k5eAaBmAgInS,k5eAaPmAgInS
ostrý	ostrý	k2eAgInSc1d1
nekrolog	nekrolog	k1gInSc1
v	v	k7c6
National	National	k1gFnSc6
Review	Review	k1gFnPc2
kritizující	kritizující	k2eAgFnSc4d1
Rothbardovu	Rothbardův	k2eAgFnSc4d1
radikální	radikální	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rakouská	rakouský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Rakouská	rakouský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Rakouská	rakouský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nS
odhalit	odhalit	k5eAaPmF
axiomy	axiom	k1gInPc4
lidského	lidský	k2eAgNnSc2d1
jednání	jednání	k1gNnSc2
(	(	kIx(
<g/>
nazývané	nazývaný	k2eAgFnSc2d1
praxeologie	praxeologie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podporuje	podporovat	k5eAaImIp3nS
volný	volný	k2eAgInSc4d1
trh	trh	k1gInSc4
a	a	k8xC
kritizuje	kritizovat	k5eAaImIp3nS
řízené	řízený	k2eAgFnSc2d1
ekonomiky	ekonomika	k1gFnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
ty	ten	k3xDgFnPc1
ničí	ničit	k5eAaImIp3nS
informační	informační	k2eAgFnSc4d1
funkci	funkce	k1gFnSc4
cen	cena	k1gFnPc2
a	a	k8xC
nevyhnutelně	vyhnutelně	k6eNd1
vedou	vést	k5eAaImIp3nP
k	k	k7c3
totalitě	totalita	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlivní	vlivný	k2eAgMnPc1d1
představitelé	představitel	k1gMnPc1
byli	být	k5eAaImAgMnP
Eugen	Eugen	k2eAgInSc4d1
von	von	k1gInSc4
Böhm-Bawerk	Böhm-Bawerk	k1gInSc1
<g/>
,	,	kIx,
Friedrich	Friedrich	k1gMnSc1
Hayek	Hayek	k1gMnSc1
a	a	k8xC
Ludwig	Ludwig	k1gMnSc1
von	von	k1gInSc4
Mises	Misesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rothbard	Rothbard	k1gMnSc1
argumentoval	argumentovat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
celá	celý	k2eAgFnSc1d1
ekonomická	ekonomický	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
Rakouské	rakouský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
logické	logický	k2eAgFnSc2d1
implikace	implikace	k1gFnSc2
skutečnosti	skutečnost	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
lidé	člověk	k1gMnPc1
se	se	k3xPyFc4
zapojují	zapojovat	k5eAaImIp3nP
do	do	k7c2
záměrných	záměrná	k1gFnPc2
jednání	jednání	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rothbard	Rothbard	k1gInSc1
byl	být	k5eAaImAgInS
rovněž	rovněž	k9
velmi	velmi	k6eAd1
vzdělaný	vzdělaný	k2eAgMnSc1d1
v	v	k7c6
historii	historie	k1gFnSc6
a	a	k8xC
politické	politický	k2eAgFnSc3d1
filosofii	filosofie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rothbardovy	Rothbardův	k2eAgFnPc4d1
knihy	kniha	k1gFnPc4
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
Zásady	zásada	k1gFnPc1
ekonomie	ekonomie	k1gFnSc2
(	(	kIx(
<g/>
Man	Man	k1gMnSc1
<g/>
,	,	kIx,
Economy	Econom	k1gInPc1
<g/>
,	,	kIx,
and	and	k?
State	status	k1gInSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Ekonomie	ekonomie	k1gFnSc1
státních	státní	k2eAgInPc2d1
zásahů	zásah	k1gInPc2
(	(	kIx(
<g/>
Power	Power	k1gInSc1
and	and	k?
Market	market	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Etika	etika	k1gFnSc1
svobody	svoboda	k1gFnSc2
(	(	kIx(
<g/>
The	The	k1gFnSc1
Ethics	Ethics	k1gInSc4
of	of	k?
Liberty	Libert	k1gInPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
For	forum	k1gNnPc2
a	a	k8xC
New	New	k1gFnSc7
Liberty	Libert	k1gInPc7
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
považovány	považován	k2eAgInPc1d1
<g/>
[	[	kIx(
<g/>
kým	kdo	k3yQnSc7,k3yInSc7,k3yRnSc7
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
za	za	k7c4
libertariánské	libertariánský	k2eAgFnPc4d1
představy	představa	k1gFnPc4
přirozeného	přirozený	k2eAgNnSc2d1
práva	právo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Studoval	studovat	k5eAaImAgMnS
ekonomické	ekonomický	k2eAgFnPc4d1
školy	škola	k1gFnPc4
předcházející	předcházející	k2eAgFnPc4d1
Adama	Adam	k1gMnSc4
Smithe	Smithe	k1gFnSc6
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
scholastiky	scholastik	k1gMnPc4
a	a	k8xC
fyziokraty	fyziokrat	k1gMnPc4
a	a	k8xC
diskutoval	diskutovat	k5eAaImAgMnS
je	být	k5eAaImIp3nS
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
nedokončeném	dokončený	k2eNgNnSc6d1
<g/>
,	,	kIx,
mnohasvazkovém	mnohasvazkový	k2eAgNnSc6d1
díle	dílo	k1gNnSc6
An	An	k1gMnSc1
Austrian	Austrian	k1gMnSc1
Perspective	Perspectiv	k1gInSc5
on	on	k3xPp3gMnSc1
the	the	k?
History	Histor	k1gInPc1
of	of	k?
Economic	Economice	k1gInPc2
Thought	Thoughtum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Rothbard	Rothbard	k1gInSc1
rozděluje	rozdělovat	k5eAaImIp3nS
různé	různý	k2eAgInPc4d1
druhy	druh	k1gInPc4
státních	státní	k2eAgFnPc2d1
intervencí	intervence	k1gFnPc2
do	do	k7c2
tří	tři	k4xCgFnPc2
kategorií	kategorie	k1gFnPc2
<g/>
:	:	kIx,
autistické	autistický	k2eAgFnSc2d1
intervence	intervence	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
zásah	zásah	k1gInSc4
do	do	k7c2
soukromé	soukromý	k2eAgFnSc2d1
nesměnné	směnný	k2eNgFnSc2d1
aktivity	aktivita	k1gFnSc2
<g/>
;	;	kIx,
podvojné	podvojný	k2eAgFnSc2d1
intervence	intervence	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
vynucená	vynucený	k2eAgFnSc1d1
směna	směna	k1gFnSc1
mezi	mezi	k7c7
jednotlivcem	jednotlivec	k1gMnSc7
a	a	k8xC
státem	stát	k1gInSc7
<g/>
;	;	kIx,
a	a	k8xC
trojstranné	trojstranný	k2eAgFnSc2d1
intervence	intervence	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
státem	stát	k1gInSc7
nařízená	nařízený	k2eAgFnSc1d1
směna	směna	k1gFnSc1
mezi	mezi	k7c7
jednotlivci	jednotlivec	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Sanford	Sanforda	k1gFnPc2
Ikeda	Ikeda	k1gFnSc1
<g/>
,	,	kIx,
Rothbardova	Rothbardův	k2eAgFnSc1d1
typologie	typologie	k1gFnSc1
"	"	kIx"
<g/>
eliminuje	eliminovat	k5eAaBmIp3nS
mezery	mezera	k1gFnPc4
a	a	k8xC
nekozistence	nekozistence	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
objevují	objevovat	k5eAaImIp3nP
v	v	k7c6
Misesových	Misesový	k2eAgFnPc6d1
původních	původní	k2eAgFnPc6d1
formulacích	formulace	k1gFnPc6
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rothbard	Rothbard	k1gMnSc1
byl	být	k5eAaImAgMnS
ostrý	ostrý	k2eAgMnSc1d1
kritik	kritik	k1gMnSc1
vlivného	vlivný	k2eAgMnSc2d1
ekonoma	ekonom	k1gMnSc2
Johna	John	k1gMnSc2
Maynarda	Maynard	k1gMnSc2
Keynese	Keynese	k1gFnSc2
a	a	k8xC
keynesiánského	keynesiánský	k2eAgNnSc2d1
ekonomického	ekonomický	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
esej	esej	k1gFnSc4
Keynes	Keynes	k1gMnSc1
<g/>
,	,	kIx,
the	the	k?
Man	Man	k1gMnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
útok	útok	k1gInSc1
na	na	k7c4
Keynesovy	Keynesův	k2eAgFnPc4d1
ekonomické	ekonomický	k2eAgFnPc4d1
ideje	idea	k1gFnPc4
i	i	k9
na	na	k7c4
jeho	jeho	k3xOp3gFnSc4
osobu	osoba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rothbard	Rothbard	k1gInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
eseji	esej	k1gFnSc6
Jeremy	Jerema	k1gFnSc2
Bentham	Bentham	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Utilitarian	Utilitarian	k1gMnSc1
as	as	k9
Big	Big	k1gMnSc1
Brother	Brother	kA
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
publikované	publikovaný	k2eAgNnSc1d1
v	v	k7c6
jeho	jeho	k3xOp3gNnSc6
díle	dílo	k1gNnSc6
Classical	Classical	k1gMnPc4
Economics	Economicsa	k1gFnPc2
<g/>
,	,	kIx,
velmi	velmi	k6eAd1
kritický	kritický	k2eAgInSc1d1
mezi	mezi	k7c7
jinými	jiná	k1gFnPc7
také	také	k9
k	k	k7c3
utilitaristickému	utilitaristický	k2eAgMnSc3d1
filosofu	filosof	k1gMnSc3
Jeremy	Jerema	k1gFnSc2
Benthamovi	Bentham	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rothbard	Rothbard	k1gMnSc1
je	být	k5eAaImIp3nS
autorem	autor	k1gMnSc7
tzv.	tzv.	kA
"	"	kIx"
<g/>
Rothbardova	Rothbardův	k2eAgFnSc1d1
zákonu	zákon	k1gInSc3
<g/>
"	"	kIx"
<g/>
,	,	kIx,
tedy	tedy	k8xC
že	že	k8xS
"	"	kIx"
<g/>
lidé	člověk	k1gMnPc1
mají	mít	k5eAaImIp3nP
tendenci	tendence	k1gFnSc4
specializovat	specializovat	k5eAaBmF
se	se	k3xPyFc4
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
v	v	k7c6
čem	co	k3yInSc6,k3yRnSc6,k3yQnSc6
jsou	být	k5eAaImIp3nP
nejhorší	zlý	k2eAgInPc1d3
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
<g/>
,	,	kIx,
Henry	Henry	k1gMnSc1
George	Georg	k1gMnSc2
skvěle	skvěle	k6eAd1
rozumí	rozumět	k5eAaImIp3nS
všemu	všecek	k3xTgNnSc3
kromě	kromě	k7c2
půdy	půda	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
tudíž	tudíž	k8xC
90	#num#	k4
<g/>
%	%	kIx~
času	čas	k1gInSc2
píše	psát	k5eAaImIp3nS
o	o	k7c6
půdě	půda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Friedman	Friedman	k1gMnSc1
skvěle	skvěle	k6eAd1
rozumí	rozumět	k5eAaImIp3nS
všemu	všecek	k3xTgNnSc3
kromě	kromě	k7c2
peněz	peníze	k1gInPc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
se	se	k3xPyFc4
zaměřuje	zaměřovat	k5eAaImIp3nS
na	na	k7c4
peníze	peníz	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Murray	Murraa	k1gMnSc2
Rothbard	Rothbard	k1gMnSc1
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
kapitolu	kapitola	k1gFnSc4
Ekonomie	ekonomie	k1gFnSc2
státních	státní	k2eAgInPc2d1
zásahů	zásah	k1gInPc2
tradiční	tradiční	k2eAgFnSc4d1
roli	role	k1gFnSc4
ekonoma	ekonom	k1gMnSc2
ve	v	k7c6
veřejném	veřejný	k2eAgInSc6d1
životě	život	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rothbard	Rothbard	k1gMnSc1
poznamenal	poznamenat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
funkce	funkce	k1gFnSc1
ekonoma	ekonom	k1gMnSc2
na	na	k7c6
volném	volný	k2eAgInSc6d1
trhu	trh	k1gInSc6
se	se	k3xPyFc4
velmi	velmi	k6eAd1
liší	lišit	k5eAaImIp3nS
od	od	k7c2
jeho	jeho	k3xOp3gFnSc2
funkce	funkce	k1gFnSc2
na	na	k7c6
trhu	trh	k1gInSc6
omezeném	omezený	k2eAgInSc6d1
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
může	moct	k5eAaImIp3nS
ekonom	ekonom	k1gMnSc1
dělat	dělat	k5eAaImF
na	na	k7c6
zcela	zcela	k6eAd1
svobodném	svobodný	k2eAgInSc6d1
trhu	trh	k1gInSc6
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
"	"	kIx"
<g/>
,	,	kIx,
ptá	ptat	k5eAaImIp3nS
se	se	k3xPyFc4
Rothbard	Rothbard	k1gMnSc1
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Může	moct	k5eAaImIp3nS
vysvětlit	vysvětlit	k5eAaPmF
fungování	fungování	k1gNnSc1
tržní	tržní	k2eAgFnSc2d1
ekonomiky	ekonomika	k1gFnSc2
(	(	kIx(
<g/>
zásadní	zásadní	k2eAgInSc4d1
úkol	úkol	k1gInSc4
<g/>
,	,	kIx,
zvláště	zvláště	k6eAd1
když	když	k8xS
má	mít	k5eAaImIp3nS
nevzdělaný	vzdělaný	k2eNgMnSc1d1
člověk	člověk	k1gMnSc1
odjakživa	odjakživa	k6eAd1
sklon	sklon	k1gInSc4
považovat	považovat	k5eAaImF
tržní	tržní	k2eAgFnSc4d1
ekonomii	ekonomie	k1gFnSc4
za	za	k7c4
naprostý	naprostý	k2eAgInSc4d1
chaos	chaos	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
víc	hodně	k6eAd2
toho	ten	k3xDgNnSc2
už	už	k6eAd1
moc	moc	k6eAd1
udělat	udělat	k5eAaPmF
nemůže	moct	k5eNaImIp3nS
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Anarchokapitalismus	Anarchokapitalismus	k1gInSc1
</s>
<s>
Murray	Murraa	k1gFnPc1
Rothbard	Rothbarda	k1gFnPc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Anarchokapitalismus	Anarchokapitalismus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Kapitalismus	kapitalismus	k1gInSc1
je	být	k5eAaImIp3nS
vrcholné	vrcholný	k2eAgNnSc4d1
vyjádření	vyjádření	k1gNnSc4
anarchismu	anarchismus	k1gInSc2
a	a	k8xC
anarchismus	anarchismus	k1gInSc1
je	být	k5eAaImIp3nS
vrcholné	vrcholný	k2eAgNnSc4d1
vyjádření	vyjádření	k1gNnSc4
kapitalismu	kapitalismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
—	—	k?
<g/>
Murray	Murray	k1gInPc1
Rothbard	Rothbard	k1gInSc1
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rothbard	Rothbard	k1gInSc1
"	"	kIx"
<g/>
kombinuje	kombinovat	k5eAaImIp3nS
laissez-faire	laissez-fair	k1gInSc5
svého	svůj	k3xOyFgMnSc2
učitele	učitel	k1gMnSc2
Ludwiga	Ludwig	k1gMnSc2
von	von	k1gInSc1
Misese	Misese	k1gFnPc4
s	s	k7c7
absolutistickým	absolutistický	k2eAgInSc7d1
pohledem	pohled	k1gInSc7
na	na	k7c4
lidská	lidský	k2eAgNnPc4d1
práva	právo	k1gNnPc4
a	a	k8xC
odmítáním	odmítání	k1gNnSc7
státu	stát	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yQgInPc4,k3yIgInPc4
získal	získat	k5eAaPmAgInS
ze	z	k7c2
studia	studio	k1gNnSc2
individualistických	individualistický	k2eAgMnPc2d1
amerických	americký	k2eAgMnPc2d1
anarchistů	anarchista	k1gMnPc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
jako	jako	k9
byli	být	k5eAaImAgMnP
Lysander	Lysander	k1gMnSc1
Spooner	Spooner	k1gMnSc1
a	a	k8xC
Benjamin	Benjamin	k1gMnSc1
Tucker	Tucker	k1gMnSc1
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Rothbard	Rothbard	k1gInSc4
o	o	k7c6
Spoonerovi	Spooner	k1gMnSc6
a	a	k8xC
Tuckerovi	Tucker	k1gMnSc6
psal	psát	k5eAaImAgInS
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Lysander	Lysander	k1gMnSc1
Spooner	Spooner	k1gMnSc1
a	a	k8xC
Benjamin	Benjamin	k1gMnSc1
T.	T.	kA
Tucker	Tucker	k1gMnSc1
byli	být	k5eAaImAgMnP
nepřekonatelní	překonatelný	k2eNgMnPc1d1
jako	jako	k8xC,k8xS
političtí	politický	k2eAgMnPc1d1
filozofové	filozof	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zejména	zejména	k6eAd1
dnes	dnes	k6eAd1
je	být	k5eAaImIp3nS
toliko	toliko	k6eAd1
potřebné	potřebný	k2eAgNnSc1d1
obrodit	obrodit	k5eAaPmF
a	a	k8xC
zpřístupnit	zpřístupnit	k5eAaPmF
jejich	jejich	k3xOp3gNnPc4
z	z	k7c2
velké	velký	k2eAgFnSc2d1
části	část	k1gFnSc2
zapomenutý	zapomenutý	k2eAgInSc4d1
odkaz	odkaz	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
zanechali	zanechat	k5eAaPmAgMnP
politické	politický	k2eAgFnSc3d1
filozofii	filozofie	k1gFnSc3
<g/>
…	…	k?
Právě	právě	k6eAd1
v	v	k7c6
jejich	jejich	k3xOp3gFnSc6
práci	práce	k1gFnSc6
najdeme	najít	k5eAaPmIp1nP
vědecké	vědecký	k2eAgNnSc4d1
vysvětlení	vysvětlení	k1gNnSc4
fungování	fungování	k1gNnSc2
volného	volný	k2eAgInSc2d1
trhu	trh	k1gInSc2
(	(	kIx(
<g/>
a	a	k8xC
následků	následek	k1gInPc2
vládních	vládní	k2eAgFnPc2d1
intervencí	intervence	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
individualističtí	individualistický	k2eAgMnPc1d1
anarchisté	anarchista	k1gMnPc1
mohou	moct	k5eAaImIp3nP
jednoduše	jednoduše	k6eAd1
včlenit	včlenit	k5eAaPmF
do	do	k7c2
svého	svůj	k3xOyFgInSc2
politického	politický	k2eAgInSc2d1
a	a	k8xC
sociálního	sociální	k2eAgInSc2d1
Weltanschauung	Weltanschauung	k1gMnSc1
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
jeho	jeho	k3xOp3gInSc6
anarchokapitalistickém	anarchokapitalistický	k2eAgInSc6d1
modelu	model	k1gInSc6
ochranné	ochranný	k2eAgFnSc2d1
agentury	agentura	k1gFnSc2
soutěží	soutěž	k1gFnPc2
na	na	k7c6
volném	volný	k2eAgInSc6d1
trhu	trh	k1gInSc6
a	a	k8xC
jsou	být	k5eAaImIp3nP
dobrovolně	dobrovolně	k6eAd1
podporovány	podporován	k2eAgFnPc4d1
spotřebiteli	spotřebitel	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
si	se	k3xPyFc3
vybírají	vybírat	k5eAaImIp3nP
jejich	jejich	k3xOp3gFnSc4
ochranu	ochrana	k1gFnSc4
a	a	k8xC
právní	právní	k2eAgFnPc4d1
služby	služba	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anarchokapitalismus	Anarchokapitalismus	k1gInSc1
by	by	kYmCp3nS
znamenal	znamenat	k5eAaImAgInS
konec	konec	k1gInSc1
státního	státní	k2eAgInSc2d1
monopolu	monopol	k1gInSc2
síly	síla	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rothbard	Rothbard	k1gInSc1
byl	být	k5eAaImAgInS
proti	proti	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
považoval	považovat	k5eAaImAgMnS
za	za	k7c4
přílišnou	přílišný	k2eAgFnSc4d1
specializaci	specializace	k1gFnSc4
vědy	věda	k1gFnSc2
a	a	k8xC
snažil	snažit	k5eAaImAgMnS
se	se	k3xPyFc4
sjednotit	sjednotit	k5eAaPmF
vědní	vědní	k2eAgFnPc4d1
obory	obora	k1gFnPc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
ekonomie	ekonomie	k1gFnSc1
<g/>
,	,	kIx,
historie	historie	k1gFnSc1
<g/>
,	,	kIx,
etika	etika	k1gFnSc1
a	a	k8xC
politologie	politologie	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
vytvořit	vytvořit	k5eAaPmF
"	"	kIx"
<g/>
vědu	věda	k1gFnSc4
svobody	svoboda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
"	"	kIx"
Rothbard	Rothbard	k1gMnSc1
popsal	popsat	k5eAaPmAgMnS
morální	morální	k2eAgInPc4d1
základy	základ	k1gInPc4
pro	pro	k7c4
svůj	svůj	k3xOyFgInSc4
anarchokapitalistický	anarchokapitalistický	k2eAgInSc4d1
postoj	postoj	k1gInSc4
ve	v	k7c6
dvou	dva	k4xCgFnPc6
knihách	kniha	k1gFnPc6
<g/>
:	:	kIx,
For	forum	k1gNnPc2
a	a	k8xC
New	New	k1gFnSc7
Liberty	Libert	k1gInPc7
<g/>
,	,	kIx,
publikováno	publikovat	k5eAaBmNgNnS
1972	#num#	k4
a	a	k8xC
The	The	k1gMnSc1
Ethics	Ethicsa	k1gFnPc2
of	of	k?
Liberty	Libert	k1gInPc1
<g/>
,	,	kIx,
publikováno	publikovat	k5eAaBmNgNnS
1982	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
díle	díl	k1gInSc6
Power	Power	k1gMnSc1
and	and	k?
Market	market	k1gInSc1
(	(	kIx(
<g/>
1970	#num#	k4
<g/>
)	)	kIx)
Rothbard	Rothbard	k1gMnSc1
popsal	popsat	k5eAaPmAgMnS
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
by	by	kYmCp3nS
mohla	moct	k5eAaImAgFnS
fungovat	fungovat	k5eAaImF
ekonomika	ekonomika	k1gFnSc1
bez	bez	k7c2
státních	státní	k2eAgInPc2d1
zásahů	zásah	k1gInPc2
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
knize	kniha	k1gFnSc6
The	The	k1gFnPc2
Ethics	Ethicsa	k1gFnPc2
of	of	k?
Liberty	Libert	k1gInPc1
<g/>
,	,	kIx,
Rothbard	Rothbard	k1gMnSc1
prosazoval	prosazovat	k5eAaImAgMnS
právo	právo	k1gNnSc4
úplného	úplný	k2eAgNnSc2d1
sebevlastnictví	sebevlastnictví	k1gNnSc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jediný	jediný	k2eAgInSc1d1
princip	princip	k1gInSc1
slučitelný	slučitelný	k2eAgInSc1d1
s	s	k7c7
morálními	morální	k2eAgFnPc7d1
zásadami	zásada	k1gFnPc7
aplikovatelnými	aplikovatelný	k2eAgFnPc7d1
na	na	k7c4
každého	každý	k3xTgMnSc4
člověka	člověk	k1gMnSc4
a	a	k8xC
"	"	kIx"
<g/>
univerzální	univerzální	k2eAgFnSc7d1
etikou	etika	k1gFnSc7
<g/>
"	"	kIx"
<g/>
;	;	kIx,
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
přirozené	přirozený	k2eAgNnSc4d1
právo	právo	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
je	být	k5eAaImIp3nS
zcela	zcela	k6eAd1
přirozeně	přirozeně	k6eAd1
pro	pro	k7c4
člověka	člověk	k1gMnSc4
tím	ten	k3xDgNnSc7
nejlepším	dobrý	k2eAgMnSc7d3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
Věřil	věřit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
důsledku	důsledek	k1gInSc6
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
pak	pak	k6eAd1
jednotlivci	jednotlivec	k1gMnPc1
skutečně	skutečně	k6eAd1
vlastní	vlastnit	k5eAaImIp3nP
plody	plod	k1gInPc4
své	svůj	k3xOyFgFnSc2
práce	práce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
základě	základ	k1gInSc6
toho	ten	k3xDgNnSc2
má	mít	k5eAaImIp3nS
každý	každý	k3xTgMnSc1
člověk	člověk	k1gMnSc1
právo	právo	k1gNnSc4
vyměnit	vyměnit	k5eAaPmF
svůj	svůj	k3xOyFgInSc4
majetek	majetek	k1gInSc4
s	s	k7c7
druhými	druhý	k4xOgInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Věřil	věřit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
když	když	k8xS
jedinec	jedinec	k1gMnSc1
spojí	spojit	k5eAaPmIp3nS
svou	svůj	k3xOyFgFnSc4
práci	práce	k1gFnSc4
s	s	k7c7
půdou	půda	k1gFnSc7
bez	bez	k7c2
vlastníka	vlastník	k1gMnSc2
<g/>
,	,	kIx,
stává	stávat	k5eAaImIp3nS
se	se	k3xPyFc4
jejím	její	k3xOp3gMnSc7
řádným	řádný	k2eAgMnSc7d1
majitelem	majitel	k1gMnSc7
a	a	k8xC
z	z	k7c2
tohoto	tento	k3xDgInSc2
pohledu	pohled	k1gInSc2
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
jeho	jeho	k3xOp3gInSc7
soukromým	soukromý	k2eAgInSc7d1
majetkem	majetek	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
lze	lze	k6eAd1
směnit	směnit	k5eAaPmF
či	či	k8xC
darovat	darovat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Argumentoval	argumentovat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
taková	takový	k3xDgFnSc1
půda	půda	k1gFnSc1
by	by	kYmCp3nS
neměla	mít	k5eNaImAgFnS
tendenci	tendence	k1gFnSc4
zůstat	zůstat	k5eAaPmF
nevyužita	využit	k2eNgMnSc4d1
<g/>
,	,	kIx,
pokud	pokud	k8xS
by	by	kYmCp3nS
dávalo	dávat	k5eAaImAgNnS
ekonomický	ekonomický	k2eAgInSc4d1
smysl	smysl	k1gInSc4
ji	on	k3xPp3gFnSc4
využít	využít	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Knihy	kniha	k1gFnPc1
</s>
<s>
Man	Man	k1gMnSc1
<g/>
,	,	kIx,
Economy	Econom	k1gInPc1
<g/>
,	,	kIx,
and	and	k?
State	status	k1gInSc5
(	(	kIx(
<g/>
Full	Fulla	k1gFnPc2
Text	text	k1gInSc1
<g/>
;	;	kIx,
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
945466	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
1962	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Panic	panic	k1gMnSc1
of	of	k?
1819	#num#	k4
<g/>
.	.	kIx.
1962	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
edition	edition	k1gInSc1
<g/>
:	:	kIx,
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
933550	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
America	America	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Great	Great	k1gInSc1
Depression	Depression	k1gInSc4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Full	Full	k1gInSc1
Text	text	k1gInSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
[	[	kIx(
<g/>
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
945466	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1963	#num#	k4
<g/>
,	,	kIx,
1972	#num#	k4
<g/>
,	,	kIx,
1975	#num#	k4
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
What	What	k1gMnSc1
Has	hasit	k5eAaImRp2nS
Government	Government	k1gInSc1
Done	Don	k1gInSc5
to	ten	k3xDgNnSc1
Our	Our	k1gFnSc3
Money	Monea	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
Full	Full	k1gInSc1
Text	text	k1gInSc1
/	/	kIx~
Audio	audio	k2eAgInSc1d1
Book	Book	k1gInSc1
<g/>
)	)	kIx)
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
945466	#num#	k4
<g/>
-	-	kIx~
<g/>
44	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1963	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
česky	česky	k6eAd1
jako	jako	k8xS,k8xC
Peníze	peníz	k1gInPc1
v	v	k7c6
rukou	ruka	k1gFnPc6
státu	stát	k1gInSc2
<g/>
,	,	kIx,
vydal	vydat	k5eAaPmAgInS
Liberální	liberální	k2eAgInSc1d1
institut	institut	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Economic	Economic	k1gMnSc1
Depressions	Depressions	k1gInSc1
<g/>
:	:	kIx,
Causes	Causes	k1gInSc1
and	and	k?
Cures	Cures	k1gInSc1
(	(	kIx(
<g/>
1969	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Power	Power	k1gInSc1
and	and	k?
Market	market	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
933550	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1970	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
restored	restored	k1gInSc1
to	ten	k3xDgNnSc1
Man	Man	k1gMnSc1
<g/>
,	,	kIx,
Economy	Econom	k1gInPc1
<g/>
,	,	kIx,
and	and	k?
State	status	k1gInSc5
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
945466	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Education	Education	k1gInSc1
<g/>
:	:	kIx,
Free	Free	k1gFnSc1
and	and	k?
Compulsory	Compulsor	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
945466	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1972	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Left	Left	k1gMnSc1
and	and	k?
Right	Right	k1gMnSc1
<g/>
,	,	kIx,
Selected	Selected	k1gMnSc1
Essays	Essays	k1gInSc4
1954-65	1954-65	k4
(	(	kIx(
<g/>
1972	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
For	forum	k1gNnPc2
a	a	k8xC
New	New	k1gFnSc1
Liberty	Libert	k1gInPc4
<g/>
:	:	kIx,
The	The	k1gMnSc1
Libertarian	Libertarian	k1gMnSc1
Manifesto	Manifesta	k1gMnSc5
(	(	kIx(
<g/>
Full	Fulla	k1gFnPc2
text	text	k1gInSc1
/	/	kIx~
Audio	audio	k2eAgInSc1d1
book	book	k1gInSc1
<g/>
)	)	kIx)
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
945466	#num#	k4
<g/>
-	-	kIx~
<g/>
47	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1973	#num#	k4
<g/>
,	,	kIx,
1978	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
V	v	k7c6
češtině	čeština	k1gFnSc6
vyšlo	vyjít	k5eAaPmAgNnS
jako	jako	k9
Manifest	manifest	k1gInSc4
svobody	svoboda	k1gFnSc2
na	na	k7c4
mises	mises	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Essential	Essential	k1gInSc1
von	von	k1gInSc1
Mises	Mises	k1gInSc1
(	(	kIx(
<g/>
1973	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Case	Case	k1gNnPc2
for	forum	k1gNnPc2
the	the	k?
100	#num#	k4
Percent	percent	k1gInSc1
Gold	Gold	k1gMnSc1
Dollar	dollar	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
945466	#num#	k4
<g/>
-	-	kIx~
<g/>
34	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
(	(	kIx(
<g/>
Full	Full	k1gInSc1
Text	text	k1gInSc1
/	/	kIx~
Audio	audio	k2eAgInSc1d1
Book	Book	k1gInSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Egalitarianism	Egalitarianism	k1gInSc1
as	as	k1gNnSc2
a	a	k8xC
Revolt	revolta	k1gFnPc2
Against	Against	k1gMnSc1
Nature	Natur	k1gMnSc5
and	and	k?
Other	Other	k1gInSc1
Essays	Essays	k1gInSc1
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
945466	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Conceived	Conceived	k1gInSc1
in	in	k?
Liberty	Libert	k1gMnPc7
(	(	kIx(
<g/>
4	#num#	k4
vol	vol	k6eAd1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
945466	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1975	#num#	k4
<g/>
-	-	kIx~
<g/>
79	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Individualism	Individualism	k1gInSc1
and	and	k?
the	the	k?
Philosophy	Philosopha	k1gFnSc2
of	of	k?
the	the	k?
Social	Social	k1gMnSc1
Sciences	Sciences	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
932790	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Ethics	Ethics	k1gInSc4
of	of	k?
Liberty	Libert	k1gInPc4
(	(	kIx(
<g/>
Full	Fulla	k1gFnPc2
Text	text	k1gInSc1
/	/	kIx~
Audio	audio	k2eAgInSc1d1
Book	Book	k1gInSc1
<g/>
)	)	kIx)
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
8147	#num#	k4
<g/>
-	-	kIx~
<g/>
7559	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Mystery	Myster	k1gInPc1
of	of	k?
Banking	Banking	k1gInSc1
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
943940	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ludwig	Ludwig	k1gInSc1
von	von	k1gInSc1
Mises	Mises	k1gInSc1
<g/>
:	:	kIx,
Scholar	Scholar	k1gMnSc1
<g/>
,	,	kIx,
Creator	Creator	k1gMnSc1
<g/>
,	,	kIx,
Hero	Hero	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
20856420	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Freedom	Freedom	k1gInSc1
<g/>
,	,	kIx,
Inequality	Inequalit	k1gInPc1
<g/>
,	,	kIx,
Primitivism	Primitivism	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
the	the	k?
Division	Division	k1gInSc1
of	of	k?
Labor	Labor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Full	Full	k1gInSc1
text	text	k1gInSc4
(	(	kIx(
<g/>
included	included	k1gInSc1
as	as	k1gInSc1
Chapter	Chapter	k1gInSc1
16	#num#	k4
in	in	k?
Egalitarianism	Egalitarianism	k1gMnSc1
above	abovat	k5eAaPmIp3nS
<g/>
)	)	kIx)
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Case	Case	k1gNnSc1
Against	Against	k1gMnSc1
the	the	k?
Fed	Fed	k1gMnSc1
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
945466	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
An	An	k?
Austrian	Austrian	k1gInSc1
Perspective	Perspectiv	k1gInSc5
on	on	k3xPp3gMnSc1
the	the	k?
History	Histor	k1gInPc1
of	of	k?
Economic	Economice	k1gInPc2
Thought	Thoughtum	k1gNnPc2
(	(	kIx(
<g/>
2	#num#	k4
vol	vol	k6eAd1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
945466	#num#	k4
<g/>
-	-	kIx~
<g/>
48	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Wall	Wall	k1gMnSc1
Street	Street	k1gMnSc1
<g/>
,	,	kIx,
Banks	Banks	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
American	American	k1gInSc1
Foreign	Foreigna	k1gFnPc2
Policy	Polica	k1gFnSc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Full	Full	k1gInSc1
Text	text	k1gInSc1
<g/>
)	)	kIx)
with	with	k1gInSc1
an	an	k?
introduction	introduction	k1gInSc4
by	by	kYmCp3nS
Justin	Justin	k1gMnSc1
Raimondo	Raimondo	k6eAd1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
V	v	k7c6
češtině	čeština	k1gFnSc6
vyšlo	vyjít	k5eAaPmAgNnS
jako	jako	k8xS,k8xC
Wall	Wall	k1gMnSc1
Street	Street	k1gMnSc1
<g/>
,	,	kIx,
banky	banka	k1gFnPc1
a	a	k8xC
americká	americký	k2eAgFnSc1d1
zahraniční	zahraniční	k2eAgFnSc1d1
politika	politika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kknihy	Kknih	k1gInPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
2020	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Making	Making	k1gInSc1
Economic	Economice	k1gInPc2
Sense	Sense	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
945466	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Logic	Logic	k1gMnSc1
of	of	k?
Action	Action	k1gInSc1
(	(	kIx(
<g/>
2	#num#	k4
vol	vol	k6eAd1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
ISBN	ISBN	kA
1-85898-015-1	1-85898-015-1	k4
and	and	k?
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
85898	#num#	k4
<g/>
-	-	kIx~
<g/>
570	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Austrian	Austrian	k1gMnSc1
Theory	Theora	k1gFnSc2
of	of	k?
the	the	k?
Trade	Trad	k1gMnSc5
Cycle	Cycl	k1gMnSc5
and	and	k?
Other	Other	k1gInSc4
Essays	Essaysa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
945466	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
also	also	k6eAd1
by	by	kYmCp3nS
Mises	Mises	k1gInSc1
<g/>
,	,	kIx,
Hayek	Hayek	k1gInSc1
<g/>
,	,	kIx,
&	&	k?
Haberler	Haberler	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Irrepressible	Irrepressible	k6eAd1
Rothbard	Rothbard	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Rothbard-Rockwell	Rothbard-Rockwell	k1gInSc4
Report	report	k1gInSc1
Essays	Essays	k1gInSc1
of	of	k?
Murray	Murraa	k1gFnSc2
N.	N.	kA
Rothbard	Rothbard	k1gMnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Full	Full	k1gInSc1
Text	text	k1gInSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
883959	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
A	a	k9
History	Histor	k1gInPc1
of	of	k?
Money	Monea	k1gFnSc2
and	and	k?
Banking	Banking	k1gInSc1
in	in	k?
the	the	k?
United	United	k1gMnSc1
States	States	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
945466	#num#	k4
<g/>
-	-	kIx~
<g/>
33	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Complete	Comple	k1gNnSc2
Libertarian	Libertariany	k1gInPc2
Forum	forum	k1gNnSc1
(	(	kIx(
<g/>
2	#num#	k4
vol	vol	k6eAd1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
(	(	kIx(
<g/>
Full	Full	k1gInSc1
Text	text	k1gInSc1
<g/>
)	)	kIx)
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
933550	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Economic	Economic	k1gMnSc1
Controversies	Controversies	k1gMnSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Betrayal	Betrayal	k1gInSc1
of	of	k?
the	the	k?
American	American	k1gInSc4
Right	Right	k2eAgInSc4d1
ISBN	ISBN	kA
978-1-933550-13-8	978-1-933550-13-8	k4
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Miller	Miller	k1gMnSc1
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Blackwell	Blackwell	k1gInSc1
Encyclopaedia	Encyclopaedium	k1gNnSc2
of	of	k?
Political	Political	k1gMnSc1
Thought	Thought	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Blackwell	Blackwell	k1gInSc1
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
631	#num#	k4
<g/>
-	-	kIx~
<g/>
17944	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Wendy	Wend	k1gMnPc4
McElroy	McElroa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Murray	Murraa	k1gMnSc2
N.	N.	kA
Rothbard	Rothbard	k1gMnSc1
<g/>
:	:	kIx,
Mr	Mr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Libertarian	Libertarian	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lew	Lew	k1gMnSc1
Rockwell	Rockwell	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
July	Jula	k1gFnSc2
6	#num#	k4
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
..	..	k?
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Noce	Nocus	k1gMnSc5
<g/>
,	,	kIx,
Jaime	Jaim	k1gMnSc5
E.	E.	kA
&	&	k?
Miskelly	Miskella	k1gFnSc2
<g/>
,	,	kIx,
Matthew	Matthew	k1gMnSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anarchism	Anarchisma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Political	Political	k1gMnSc1
Theories	Theories	k1gMnSc1
for	forum	k1gNnPc2
Students	Students	k1gInSc1
(	(	kIx(
<g/>
p.	p.	k?
7	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Gale	Gale	k1gMnSc1
Group	Group	k1gMnSc1
<g/>
,	,	kIx,
Inc	Inc	k1gMnSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Life	Life	k1gInSc1
in	in	k?
the	the	k?
Old	Olda	k1gFnPc2
Right	Rightum	k1gNnPc2
by	by	kYmCp3nP
Murray	Murraa	k1gFnSc2
N.	N.	kA
Rothbard	Rothbard	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
6	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
LewRockwell	LewRockwell	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
first	first	k1gMnSc1
published	published	k1gMnSc1
in	in	k?
Chronicles	Chronicles	k1gMnSc1
<g/>
,	,	kIx,
August	August	k1gMnSc1
1994.1	1994.1	k4
2	#num#	k4
3	#num#	k4
David	David	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Gordon	Gordon	k1gMnSc1
<g/>
,	,	kIx,
Murray	Murraa	k1gMnSc2
N.	N.	kA
Rothbard	Rothbard	k1gMnSc1
(	(	kIx(
<g/>
1926	#num#	k4
<g/>
-	-	kIx~
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
biography	biographa	k1gFnPc1
<g/>
,	,	kIx,
Ludwig	Ludwig	k1gInSc1
Von	von	k1gInSc1
Mises	Mises	k1gInSc1
Institute	institut	k1gInSc5
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Gary	Gara	k1gFnSc2
North	North	k1gMnSc1
<g/>
,	,	kIx,
Ron	Ron	k1gMnSc1
Paul	Paul	k1gMnSc1
on	on	k3xPp3gMnSc1
Greenspan	Greenspan	k1gMnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Fed	Fed	k1gFnSc7
Archivováno	archivován	k2eAgNnSc4d1
11	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
Lew	Lew	k1gFnSc1
Rockwell	Rockwell	k1gMnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
February	Februar	k1gInPc1
28	#num#	k4
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
David	David	k1gMnSc1
Stout	Stout	k1gMnSc1
<g/>
,	,	kIx,
Obituary	Obituara	k1gFnSc2
<g/>
:	:	kIx,
Murray	Murraa	k1gFnSc2
N.	N.	kA
Rothbard	Rothbard	k1gMnSc1
<g/>
,	,	kIx,
Economist	Economist	k1gMnSc1
And	Anda	k1gFnPc2
Free-Market	Free-Market	k1gMnSc1
Exponent	exponent	k1gMnSc1
<g/>
,	,	kIx,
68	#num#	k4
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
Times	Times	k1gInSc1
<g/>
,	,	kIx,
January	Januar	k1gInPc1
11	#num#	k4
<g/>
,	,	kIx,
1995	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
↑	↑	k?
William	William	k1gInSc1
F.	F.	kA
Buckley	Buckley	k1gInPc1
<g/>
,	,	kIx,
Murray	Murraa	k1gFnPc1
Rothbard	Rothbard	k1gInSc1
<g/>
,	,	kIx,
RIP	RIP	kA
-	-	kIx~
professor	professor	k1gMnSc1
and	and	k?
Libertarian	Libertarian	k1gMnSc1
Party	parta	k1gFnSc2
founder	founder	k1gMnSc1
<g/>
,	,	kIx,
National	National	k1gMnSc1
Review	Review	k1gFnSc2
<g/>
,	,	kIx,
February	Februara	k1gFnSc2
6	#num#	k4
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Grimm	Grimm	k1gInSc1
<g/>
,	,	kIx,
Curtis	Curtis	k1gInSc1
M.	M.	kA
<g/>
;	;	kIx,
Hunn	Hunn	k1gMnSc1
<g/>
,	,	kIx,
Lee	Lea	k1gFnSc6
<g/>
;	;	kIx,
Smith	Smith	k1gMnSc1
<g/>
,	,	kIx,
Ken	Ken	k1gMnSc1
G.	G.	kA
Strategy	Strateg	k1gInPc1
as	as	k9
Action	Action	k1gInSc1
<g/>
:	:	kIx,
Competitive	Competitiv	k1gInSc5
Dynamics	Dynamics	k1gInSc1
and	and	k?
Competitive	Competitiv	k1gInSc5
Advantage	Advantage	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gFnSc1
York	York	k1gInSc1
Oxford	Oxford	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
(	(	kIx(
<g/>
US	US	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
.	.	kIx.
p.	p.	k?
43	#num#	k4
<g/>
↑	↑	k?
Ikeda	Ikeda	k1gMnSc1
<g/>
,	,	kIx,
Sanford	Sanford	k1gMnSc1
<g/>
,	,	kIx,
Dyamics	Dyamics	k1gInSc1
of	of	k?
the	the	k?
Mixed	Mixed	k1gInSc1
Economy	Econom	k1gInPc1
<g/>
:	:	kIx,
Toward	Toward	k1gInSc1
a	a	k8xC
Theory	Theor	k1gInPc1
of	of	k?
Interventionism	Interventionism	k1gInSc1
<g/>
,	,	kIx,
Routledge	Routledge	k1gInSc1
UK	UK	kA
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
,	,	kIx,
245	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Murray	Murraa	k1gFnSc2
N.	N.	kA
Rothbard	Rothbard	k1gInSc1
<g/>
,	,	kIx,
Keynes	Keynes	k1gInSc1
the	the	k?
Man	mana	k1gFnPc2
Archivováno	archivovat	k5eAaBmNgNnS
28	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
originally	originalla	k1gFnPc1
published	published	k1gInSc1
in	in	k?
Dissent	Dissent	k1gInSc4
on	on	k3xPp3gMnSc1
Keynes	Keynes	k1gMnSc1
<g/>
:	:	kIx,
A	A	kA
Critical	Critical	k1gMnSc1
Appraisal	Appraisal	k1gMnSc1
of	of	k?
Keynesian	Keynesian	k1gInSc1
Economics	Economicsa	k1gFnPc2
<g/>
,	,	kIx,
Edited	Edited	k1gInSc4
by	by	kYmCp3nS
Mark	Mark	k1gMnSc1
Skousen	Skousen	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Praeger	Praeger	k1gInSc1
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
,	,	kIx,
171	#num#	k4
<g/>
–	–	k?
<g/>
198	#num#	k4
<g/>
;	;	kIx,
</s>
<s>
Online	Onlinout	k5eAaPmIp3nS
edition	edition	k1gInSc4
at	at	k?
The	The	k1gFnSc2
Ludwig	Ludwiga	k1gFnPc2
von	von	k1gInSc1
Mises	Mises	k1gMnSc1
Institute	institut	k1gInSc5
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ROTHBARD	ROTHBARD	kA
<g/>
,	,	kIx,
Murray	Murraa	k1gFnPc1
N.	N.	kA
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeremy	Jerem	k1gInPc4
Bentham	Bentham	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Utilitarian	Utilitarian	k1gMnSc1
as	as	k9
Big	Big	k1gMnSc1
Brother	Brother	kA
(	(	kIx(
<g/>
continued	continued	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mises	Mises	k1gInSc1
Institute	institut	k1gInSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-08-18	2014-08-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Interview	interview	k1gNnSc1
with	with	k1gMnSc1
Murray	Murraa	k1gMnSc2
N.	N.	kA
Rothbard	Rothbard	k1gInSc1
<g/>
,	,	kIx,
Ludwig	Ludwig	k1gInSc1
von	von	k1gInSc1
Mises	Mises	k1gInSc1
Institute	institut	k1gInSc5
<g/>
,	,	kIx,
Summer	Summer	k1gInSc1
1990	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Peter	Peter	k1gMnSc1
G.	G.	kA
Klein	Klein	k1gMnSc1
<g/>
,	,	kIx,
Why	Why	k1gFnSc1
Intellectuals	Intellectuals	k1gInSc1
Still	Still	k1gInSc1
Support	support	k1gInSc1
Socialism	Socialism	k1gInSc1
<g/>
,	,	kIx,
Ludwig	Ludwig	k1gInSc1
von	von	k1gInSc1
Mises	Mises	k1gInSc1
Institute	institut	k1gInSc5
<g/>
,	,	kIx,
November	November	k1gInSc1
15	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
2006	#num#	k4
<g/>
↑	↑	k?
Man	mana	k1gFnPc2
<g/>
,	,	kIx,
Economy	Econom	k1gInPc1
<g/>
,	,	kIx,
and	and	k?
State	status	k1gInSc5
<g/>
,	,	kIx,
Chapter	Chapter	k1gInSc4
7	#num#	k4
<g/>
-Conclusion	-Conclusion	k1gInSc1
<g/>
:	:	kIx,
Economics	Economics	k1gInSc1
and	and	k?
Public	publicum	k1gNnPc2
Policy	Polic	k2eAgInPc1d1
<g/>
,	,	kIx,
Ludwig	Ludwig	k1gInSc1
Von	von	k1gInSc1
Mises	Mises	k1gInSc1
Institute	institut	k1gInSc5
<g/>
.	.	kIx.
<g/>
↑	↑	k?
[	[	kIx(
<g/>
http://www.lewrockwell.com/rothbard/rothbard103.html	http://www.lewrockwell.com/rothbard/rothbard103.html	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
7	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
"	"	kIx"
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
Exclusive	Exclusiev	k1gFnSc2
Interview	interview	k1gNnPc2
With	With	k1gMnSc1
Murray	Murraa	k1gFnSc2
Rothbard	Rothbard	k1gMnSc1
<g/>
"	"	kIx"
The	The	k1gMnSc1
New	New	k1gFnSc2
Banner	banner	k1gInSc1
<g/>
:	:	kIx,
A	a	k9
Fortnightly	Fortnightly	k1gFnSc4
Libertarian	Libertariany	k1gInPc2
Journal	Journal	k1gFnSc2
(	(	kIx(
<g/>
February	Februara	k1gFnSc2
25	#num#	k4
<g/>
,	,	kIx,
1972	#num#	k4
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Blackwell	Blackwell	k1gMnSc1
Encyclopaedia	Encyclopaedium	k1gNnSc2
of	of	k?
Political	Political	k1gMnSc1
Thought	Thought	k1gMnSc1
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
631	#num#	k4
<g/>
-	-	kIx~
<g/>
17944	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
,	,	kIx,
p.	p.	k?
290	#num#	k4
<g/>
↑	↑	k?
"	"	kIx"
<g/>
The	The	k1gMnSc1
Spooner-Tucker	Spooner-Tucker	k1gMnSc1
Doctrine	Doctrin	k1gInSc5
<g/>
:	:	kIx,
An	An	k1gMnSc1
Economist	Economist	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
View	View	k1gFnSc7
<g/>
"	"	kIx"
<g/>
↑	↑	k?
Roberta	Robert	k1gMnSc2
Modugno	Modugno	k6eAd1
Crocetta	Crocett	k1gMnSc4
<g/>
,	,	kIx,
Murray	Murra	k1gMnPc4
Rothbard	Rothbarda	k1gFnPc2
<g/>
’	’	k?
<g/>
s	s	k7c7
anarcho-capitalism	anarcho-capitalism	k1gMnSc1
in	in	k?
the	the	k?
contemporary	contemporara	k1gFnSc2
debate	debat	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
critical	criticat	k5eAaPmAgMnS
defense	defense	k1gFnPc4
<g/>
,	,	kIx,
Ludwig	Ludwig	k1gInSc1
Von	von	k1gInSc1
Mises	Mises	k1gInSc1
Institute	institut	k1gInSc5
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hans-Hermann	Hans-Hermann	k1gInSc1
Hoppe	Hopp	k1gInSc5
<g/>
,	,	kIx,
Anarcho-Capitalism	Anarcho-Capitalism	k1gInPc3
<g/>
:	:	kIx,
An	An	k1gFnSc1
Annotated	Annotated	k1gInSc4
Bibliography	Bibliographa	k1gFnSc2
Archivováno	archivovat	k5eAaBmNgNnS
3	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
LewRockwell	LewRockwell	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Rothbard	Rothbarda	k1gFnPc2
<g/>
,	,	kIx,
Murray	Murra	k2eAgInPc4d1
Newton	newton	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Ethics	Ethicsa	k1gFnPc2
of	of	k?
Liberty	Libert	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
NYU	NYU	kA
Press	Pressa	k1gFnPc2
<g/>
.	.	kIx.
2003	#num#	k4
<g/>
.	.	kIx.
pp	pp	k?
<g/>
.	.	kIx.
45	#num#	k4
-	-	kIx~
45	#num#	k4
<g/>
↑	↑	k?
Kyriazi	Kyriaze	k1gFnSc4
<g/>
,	,	kIx,
Harold	Harold	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reckoning	Reckoning	k1gInSc1
With	With	k1gMnSc1
Rothbard	Rothbard	k1gMnSc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
American	American	k1gMnSc1
Journal	Journal	k1gMnSc1
of	of	k?
Economics	Economics	k1gInSc1
and	and	k?
Sociology	sociolog	k1gMnPc4
63	#num#	k4
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
451	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Gordon	Gordon	k1gMnSc1
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Essential	Essential	k1gMnSc1
Rothbard	Rothbard	k1gMnSc1
<g/>
,	,	kIx,
Ludwig	Ludwig	k1gInSc1
von	von	k1gInSc1
Mises	Mises	k1gInSc1
Institute	institut	k1gInSc5
<g/>
,	,	kIx,
1	#num#	k4
<g/>
st	st	kA
edition	edition	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
February	Februara	k1gFnSc2
26	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1-933550-10-4	1-933550-10-4	k4
</s>
<s>
Raimondo	Raimondo	k6eAd1
<g/>
,	,	kIx,
Justin	Justin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
An	An	k1gFnSc1
Enemy	Enema	k1gFnSc2
of	of	k?
the	the	k?
State	status	k1gInSc5
<g/>
:	:	kIx,
The	The	k1gMnSc3
Life	Lif	k1gFnSc2
of	of	k?
Murray	Murraa	k1gFnSc2
N.	N.	kA
Rothbard	Rothbard	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prometheus	Prometheus	k1gMnSc1
Books	Books	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
July	Jula	k1gFnSc2
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1-57392-809-7	1-57392-809-7	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Rakouská	rakouský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
</s>
<s>
Anarchokapitalismus	Anarchokapitalismus	k1gInSc1
</s>
<s>
Liberalismus	liberalismus	k1gInSc1
</s>
<s>
Libertarianismus	Libertarianismus	k1gInSc1
</s>
<s>
Praxeologie	Praxeologie	k1gFnSc1
</s>
<s>
Ludwig	Ludwig	k1gInSc1
von	von	k1gInSc1
Mises	Mises	k1gInSc4
</s>
<s>
Ludwig	Ludwig	k1gInSc1
von	von	k1gInSc1
Mises	Mises	k1gInSc1
Institut	institut	k1gInSc1
</s>
<s>
Liberální	liberální	k2eAgInSc1d1
institut	institut	k1gInSc1
</s>
<s>
Josef	Josef	k1gMnSc1
Šíma	Šíma	k1gMnSc1
(	(	kIx(
<g/>
ekonom	ekonom	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Murray	Murraa	k1gFnSc2
Rothbard	Rothbard	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Osoba	osoba	k1gFnSc1
Murray	Murraa	k1gFnSc2
Rothbard	Rothbarda	k1gFnPc2
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
ROTHBARD	ROTHBARD	kA
<g/>
,	,	kIx,
Murray	Murra	k2eAgInPc4d1
Newton	newton	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ekonomické	ekonomický	k2eAgNnSc1d1
myšlení	myšlení	k1gNnSc1
před	před	k7c7
Adamem	Adam	k1gMnSc7
Smithem	Smith	k1gInSc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mises	Mises	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
"	"	kIx"
<g/>
Murray	Murraa	k1gFnSc2
N.	N.	kA
Rothbard	Rothbard	k1gMnSc1
<g/>
"	"	kIx"
by	by	kYmCp3nS
David	David	k1gMnSc1
Gordon	Gordon	k1gMnSc1
<g/>
,	,	kIx,
also	also	k1gMnSc1
includes	includes	k1gMnSc1
links	links	k6eAd1
to	ten	k3xDgNnSc1
audio	audio	k2eAgInSc1d1
clips	clips	k1gInSc1
of	of	k?
Rothbard	Rothbard	k1gInSc1
and	and	k?
the	the	k?
complete	complést	k5eAaPmIp3nS
text	text	k1gInSc1
of	of	k?
several	severat	k5eAaPmAgInS,k5eAaImAgInS
books	books	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Murray	Murray	k1gInPc1
Rothbard	Rothbarda	k1gFnPc2
na	na	k7c6
YouTube	YouTub	k1gInSc5
</s>
<s>
Murray	Murray	k1gInPc1
N.	N.	kA
Rothbard	Rothbard	k1gMnSc1
Library	Librara	k1gFnSc2
and	and	k?
Resources	Resources	k1gMnSc1
</s>
<s>
Murray	Murray	k1gInPc1
N.	N.	kA
Rothbard	Rothbard	k1gInSc4
Media	medium	k1gNnSc2
Archive	archiv	k1gInSc5
at	at	k?
Mises	Mises	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
</s>
<s>
BlackCrayon	BlackCrayon	k1gNnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
:	:	kIx,
People	People	k1gMnSc2
<g/>
:	:	kIx,
Murray	Murraa	k1gMnSc2
Rothbard	Rothbard	k1gMnSc1
</s>
<s>
Chronological	Chronologicat	k5eAaPmAgMnS
Bibliography	Bibliographa	k1gFnSc2
of	of	k?
Murray	Murraa	k1gFnSc2
Rothbard	Rothbard	k1gMnSc1
</s>
<s>
The	The	k?
Complete	Comple	k1gNnSc2
Archives	Archivesa	k1gFnPc2
of	of	k?
The	The	k1gFnSc2
Libertarian	Libertariana	k1gFnPc2
Forum	forum	k1gNnSc1
<g/>
,	,	kIx,
written	written	k2eAgMnSc1d1
about	about	k1gMnSc1
twice	twice	k1gMnSc1
a	a	k8xC
month	month	k1gMnSc1
between	between	k2eAgMnSc1d1
1969	#num#	k4
and	and	k?
1984	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
New	New	k?
York	York	k1gInSc1
Times	Timesa	k1gFnPc2
obituary	obituara	k1gFnSc2
</s>
<s>
Sociology	sociolog	k1gMnPc4
of	of	k?
the	the	k?
Ayn	Ayn	k1gFnSc3
Rand	rand	k1gInSc4
Cult	Cult	k2eAgInSc4d1
(	(	kIx(
<g/>
pamphlet	pamphlet	k1gInSc1
published	published	k1gInSc1
by	by	kYmCp3nS
the	the	k?
Center	centrum	k1gNnPc2
for	forum	k1gNnPc2
Libertarian	Libertarian	k1gMnSc1
Studies	Studies	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
"	"	kIx"
<g/>
It	It	k1gMnPc7
Usually	Usualla	k1gFnSc2
Ends	Ends	k1gInSc1
With	With	k1gMnSc1
Ed	Ed	k1gMnSc1
Crane	Cran	k1gInSc5
<g/>
"	"	kIx"
-	-	kIx~
Rothbard	Rothbard	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
story	story	k1gFnSc7
of	of	k?
what	what	k1gMnSc1
happened	happened	k1gMnSc1
with	with	k1gMnSc1
Ed	Ed	k1gMnSc1
Crane	Cran	k1gMnSc5
<g/>
,	,	kIx,
the	the	k?
CATO	CATO	kA
institute	institut	k1gInSc5
and	and	k?
the	the	k?
Libertarian	Libertarian	k1gInSc1
party	party	k1gFnSc1
</s>
<s>
A	a	k9
1972	#num#	k4
New	New	k1gMnSc2
Banner	banner	k1gInSc4
interview	interview	k1gNnSc2
with	witha	k1gFnPc2
Murray	Murraa	k1gFnSc2
Rothbard	Rothbard	k1gMnSc1
</s>
<s>
A	a	k9
1990	#num#	k4
interview	interview	k1gNnPc2
with	with	k1gMnSc1
Murray	Murraa	k1gFnSc2
Rothbard	Rothbard	k1gMnSc1
</s>
<s>
Why	Why	k?
Hans-Hermann	Hans-Hermann	k1gInSc1
Hoppe	Hopp	k1gInSc5
considers	considersit	k5eAaPmRp2nS
Rothbard	Rothbard	k1gInSc1
the	the	k?
key	key	k?
Austro-libertarian	Austro-libertarian	k1gMnSc1
intellectual	intellectual	k1gMnSc1
</s>
<s>
Murray	Murray	k1gInPc1
N.	N.	kA
Rothbard	Rothbard	k1gMnSc1
<g/>
,	,	kIx,
The	The	k1gMnSc1
Origins	Originsa	k1gFnPc2
of	of	k?
the	the	k?
Federal	Federal	k1gFnSc2
Reserve	Reserev	k1gFnSc2
<g/>
,	,	kIx,
The	The	k1gMnSc1
Quarterly	Quarterla	k1gFnSc2
Journal	Journal	k1gMnSc1
of	of	k?
Austrian	Austrian	k1gInSc1
Economics	Economicsa	k1gFnPc2
vol	vol	k6eAd1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
no	no	k9
<g/>
.	.	kIx.
3	#num#	k4
(	(	kIx(
<g/>
Fall	Fall	k1gInSc1
1999	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
3	#num#	k4
–	–	k?
51	#num#	k4
</s>
<s>
Murray	Murraa	k1gFnPc1
Rothbard	Rothbarda	k1gFnPc2
Institute	institut	k1gInSc5
<g/>
,	,	kIx,
Belgium	Belgium	k1gNnSc1
</s>
<s>
Murray	Murray	k1gInPc1
N.	N.	kA
Rotbard	Rotbard	k1gInSc1
<g/>
,	,	kIx,
Left	Left	k2eAgInSc1d1
and	and	k?
Right	Right	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gMnPc1
Prospects	Prospectsa	k1gFnPc2
for	forum	k1gNnPc2
Liberty	Libert	k1gInPc7
<g/>
,	,	kIx,
"	"	kIx"
<g/>
LEFT	LEFT	kA
AND	Anda	k1gFnPc2
RIGHT	RIGHT	kA
<g/>
:	:	kIx,
A	A	kA
Journal	Journal	k1gMnSc1
of	of	k?
Libertarian	Libertarian	k1gMnSc1
Thought	Thought	k1gMnSc1
<g/>
,	,	kIx,
<g/>
"	"	kIx"
Spring	Spring	k1gInSc1
1965	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Ekonomové	ekonom	k1gMnPc1
Rakouské	rakouský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
Zakladatelé	zakladatel	k1gMnPc1
</s>
<s>
Carl	Carl	k1gMnSc1
Menger	Menger	k1gMnSc1
•	•	k?
Eugen	Eugen	k1gInSc1
von	von	k1gInSc1
Böhm-Bawerk	Böhm-Bawerk	k1gInSc1
•	•	k?
Friedrich	Friedrich	k1gMnSc1
von	von	k1gInSc1
Wieser	Wieser	k1gInSc1
•	•	k?
Ludwig	Ludwig	k1gInSc1
von	von	k1gInSc1
Mises	Mises	k1gInSc1
•	•	k?
Friedrich	Friedrich	k1gMnSc1
August	August	k1gMnSc1
von	von	k1gInSc4
Hayek	Hayek	k6eAd1
Klasický	klasický	k2eAgInSc4d1
liberalismus	liberalismus	k1gInSc4
</s>
<s>
Benjamin	Benjamin	k1gMnSc1
Anderson	Anderson	k1gMnSc1
•	•	k?
Herbert	Herbert	k1gMnSc1
J.	J.	kA
Davenport	Davenport	k1gInSc1
•	•	k?
Frank	Frank	k1gMnSc1
Fetter	Fetter	k1gMnSc1
•	•	k?
Roger	Roger	k1gMnSc1
Garrison	Garrison	k1gMnSc1
•	•	k?
Gottfried	Gottfried	k1gMnSc1
Haberler	Haberler	k1gMnSc1
•	•	k?
Henry	Henry	k1gMnSc1
Hazlitt	Hazlitt	k1gMnSc1
•	•	k?
Jeffrey	Jeffrea	k1gFnSc2
Herbener	Herbener	k1gMnSc1
•	•	k?
William	William	k1gInSc1
Harold	Harold	k1gMnSc1
Hutt	Hutt	k1gMnSc1
•	•	k?
Israel	Israel	k1gMnSc1
Kirzner	Kirzner	k1gMnSc1
•	•	k?
Ludwig	Ludwig	k1gMnSc1
Lachmann	Lachmann	k1gMnSc1
•	•	k?
Yuri	Yuri	k1gNnPc2
Maltsev	Maltsvo	k1gNnPc2
•	•	k?
Ron	Ron	k1gMnSc1
Paul	Paul	k1gMnSc1
•	•	k?
David	David	k1gMnSc1
Prychitko	Prychitka	k1gFnSc5
•	•	k?
Yaraslau	Yaraslaus	k1gInSc6
Ramanchuk	Ramanchuk	k1gInSc1
•	•	k?
George	George	k1gInSc1
Reisman	Reisman	k1gMnSc1
•	•	k?
Lionel	Lionel	k1gInSc1
Robbins	Robbins	k1gInSc1
•	•	k?
Peter	Peter	k1gMnSc1
Schiff	Schiff	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
Alois	Alois	k1gMnSc1
Schumpeter	Schumpeter	k1gMnSc1
•	•	k?
George	George	k1gInSc1
Selgin	Selgin	k1gMnSc1
•	•	k?
Hans	Hans	k1gMnSc1
Sennholz	Sennholz	k1gMnSc1
•	•	k?
Knut	knuta	k1gFnPc2
Wicksell	Wicksell	k1gMnSc1
•	•	k?
Sanford	Sanford	k1gInSc1
Ikeda	Ikeda	k1gMnSc1
Anarchokapitalismus	Anarchokapitalismus	k1gInSc1
<g/>
/	/	kIx~
<g/>
Volný	volný	k2eAgInSc1d1
trh	trh	k1gInSc1
</s>
<s>
William	William	k1gInSc1
L.	L.	kA
Anderson	Anderson	k1gMnSc1
•	•	k?
Walter	Walter	k1gMnSc1
Block	Block	k1gMnSc1
•	•	k?
Gene	gen	k1gInSc5
Callahan	Callahan	k1gMnSc1
•	•	k?
Hans-Hermann	Hans-Hermann	k1gMnSc1
Hoppe	Hopp	k1gInSc5
•	•	k?
Jörg	Jörg	k1gMnSc1
Guido	Guido	k1gNnSc1
Hülsmann	Hülsmann	k1gMnSc1
•	•	k?
Jesús	Jesús	k1gInSc1
Huerta	Huert	k1gMnSc2
de	de	k?
Soto	Soto	k1gMnSc1
•	•	k?
Steven	Steven	k2eAgMnSc1d1
Horwitz	Horwitz	k1gMnSc1
•	•	k?
Peter	Peter	k1gMnSc1
Leeson	Leeson	k1gMnSc1
•	•	k?
Murray	Murraa	k1gFnSc2
Rothbard	Rothbard	k1gMnSc1
•	•	k?
Robert	Robert	k1gMnSc1
P.	P.	kA
Murphy	Murpha	k1gFnPc1
•	•	k?
Joseph	Joseph	k1gMnSc1
T.	T.	kA
Salerno	Salerna	k1gFnSc5
•	•	k?
Mark	Mark	k1gMnSc1
Thornton	Thornton	k1gInSc1
•	•	k?
Peter	Peter	k1gMnSc1
Boettke	Boettk	k1gFnSc2
•	•	k?
Thomas	Thomas	k1gMnSc1
E.	E.	kA
Woods	Woods	k1gInSc1
Jr	Jr	k1gFnSc1
<g/>
.	.	kIx.
•	•	k?
Peter	Peter	k1gMnSc1
Klein	Klein	k1gMnSc1
•	•	k?
Randall	Randall	k1gMnSc1
G.	G.	kA
Holcombe	Holcomb	k1gMnSc5
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Ekonomie	ekonomie	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
20011018422	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118951769	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2146	#num#	k4
4806	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79059349	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
107013507	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79059349	#num#	k4
</s>
