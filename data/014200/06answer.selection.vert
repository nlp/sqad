<s>
Murray	Murray	k1gMnSc1
Newton	Newton	k1gMnSc1
Rothbard	Rothbard	k1gMnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1926	#num#	k4
–	–	k?
7	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1995	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
americký	americký	k2eAgMnSc1d1
ekonom	ekonom	k1gMnSc1
rakouské	rakouský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
pomohl	pomoct	k5eAaPmAgInS
definovat	definovat	k5eAaBmF
moderní	moderní	k2eAgInSc1d1
libertarianismus	libertarianismus	k1gInSc1
a	a	k8xC
založil	založit	k5eAaPmAgInS
proud	proud	k1gInSc1
tržního	tržní	k2eAgInSc2d1
anarchismu	anarchismus	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
nazýval	nazývat	k5eAaImAgInS
anarchokapitalismus	anarchokapitalismus	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Rothbard	Rothbard	k1gMnSc1
kladl	klást	k5eAaImAgMnS
důraz	důraz	k1gInSc4
na	na	k7c4
spontánní	spontánní	k2eAgInSc4d1
řád	řád	k1gInSc4
a	a	k8xC
prosazování	prosazování	k1gNnSc4
individuálního	individuální	k2eAgInSc2d1
anarchismu	anarchismus	k1gInSc2
a	a	k8xC
odsuzoval	odsuzovat	k5eAaImAgMnS
centrální	centrální	k2eAgNnSc4d1
plánování	plánování	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>