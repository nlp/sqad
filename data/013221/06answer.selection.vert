<s>
Xenon	xenon	k1gInSc1	xenon
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Xe	Xe	k1gFnSc2	Xe
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Xenon	xenon	k1gInSc1	xenon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
plynný	plynný	k2eAgInSc4d1	plynný
chemický	chemický	k2eAgInSc4d1	chemický
prvek	prvek	k1gInSc4	prvek
patřící	patřící	k2eAgInSc4d1	patřící
mezi	mezi	k7c4	mezi
vzácné	vzácný	k2eAgInPc4d1	vzácný
plyny	plyn	k1gInPc4	plyn
<g/>
.	.	kIx.	.
</s>
