<s>
Přimda	Přimda	k1gFnSc1	Přimda
je	být	k5eAaImIp3nS	být
zřícenina	zřícenina	k1gFnSc1	zřícenina
nejstaršího	starý	k2eAgInSc2d3	nejstarší
známého	známý	k2eAgInSc2d1	známý
kamenného	kamenný	k2eAgInSc2d1	kamenný
hradu	hrad	k1gInSc2	hrad
na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
městem	město	k1gNnSc7	město
Přimda	Přimdo	k1gNnSc2	Přimdo
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Tachov	Tachov	k1gInSc1	Tachov
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
raný	raný	k2eAgMnSc1d1	raný
představitel	představitel	k1gMnSc1	představitel
románské	románský	k2eAgFnSc2d1	románská
hradní	hradní	k2eAgFnSc2d1	hradní
architektury	architektura	k1gFnSc2	architektura
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1121	[number]	k4	1121
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Vladislava	Vladislava	k1gFnSc1	Vladislava
I.	I.	kA	I.
Zřícenina	zřícenina	k1gFnSc1	zřícenina
je	být	k5eAaImIp3nS	být
chráněna	chránit	k5eAaImNgFnS	chránit
jako	jako	k9	jako
národní	národní	k2eAgFnSc1d1	národní
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
Státní	státní	k2eAgInSc1d1	státní
hrad	hrad	k1gInSc1	hrad
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
správě	správa	k1gFnSc6	správa
Národního	národní	k2eAgInSc2d1	národní
památkového	památkový	k2eAgInSc2d1	památkový
ústavu	ústav	k1gInSc2	ústav
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
přístupný	přístupný	k2eAgInSc1d1	přístupný
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInSc1d3	nejstarší
písemný	písemný	k2eAgInSc1d1	písemný
doklad	doklad	k1gInSc1	doklad
o	o	k7c4	o
existenci	existence	k1gFnSc4	existence
tohoto	tento	k3xDgInSc2	tento
hradu	hrad	k1gInSc2	hrad
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Kosmově	Kosmův	k2eAgFnSc6d1	Kosmova
kronice	kronika	k1gFnSc6	kronika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
postavili	postavit	k5eAaPmAgMnP	postavit
nějací	nějaký	k3yIgMnPc1	nějaký
Němci	Němec	k1gMnPc1	Němec
uvnitř	uvnitř	k7c2	uvnitř
hranic	hranice	k1gFnPc2	hranice
českých	český	k2eAgFnPc2d1	Česká
ve	v	k7c6	v
hvozdu	hvozd	k1gInSc6	hvozd
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgInSc3	jenž
se	se	k3xPyFc4	se
jde	jít	k5eAaImIp3nS	jít
přes	přes	k7c4	přes
ves	ves	k1gFnSc4	ves
Bělou	Běla	k1gFnSc7	Běla
<g/>
,	,	kIx,	,
hrad	hrad	k1gInSc1	hrad
na	na	k7c6	na
strmé	strmý	k2eAgFnSc6d1	strmá
skále	skála	k1gFnSc6	skála
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
stavby	stavba	k1gFnSc2	stavba
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Přimda	Přimda	k1gFnSc1	Přimda
dobyta	dobýt	k5eAaPmNgFnS	dobýt
Vladislavem	Vladislav	k1gMnSc7	Vladislav
I.	I.	kA	I.
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
její	její	k3xOp3gFnSc4	její
výstavbu	výstavba	k1gFnSc4	výstavba
za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
pohraničního	pohraniční	k2eAgInSc2d1	pohraniční
hvozdu	hvozd	k1gInSc2	hvozd
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c2	za
porušení	porušení	k1gNnSc2	porušení
svých	svůj	k3xOyFgNnPc2	svůj
svrchovaných	svrchovaný	k2eAgNnPc2d1	svrchované
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Dokladem	doklad	k1gInSc7	doklad
o	o	k7c6	o
německém	německý	k2eAgInSc6d1	německý
původu	původ	k1gInSc6	původ
tohoto	tento	k3xDgInSc2	tento
hradu	hrad	k1gInSc2	hrad
je	být	k5eAaImIp3nS	být
i	i	k9	i
srovnání	srovnání	k1gNnSc1	srovnání
s	s	k7c7	s
některými	některý	k3yIgFnPc7	některý
stavbami	stavba	k1gFnPc7	stavba
podobného	podobný	k2eAgInSc2d1	podobný
charakteru	charakter	k1gInSc2	charakter
na	na	k7c6	na
bavorské	bavorský	k2eAgFnSc6d1	bavorská
straně	strana	k1gFnSc6	strana
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Flossenbürg	Flossenbürg	k1gInSc1	Flossenbürg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
takováto	takovýto	k3xDgFnSc1	takovýto
stavební	stavební	k2eAgFnSc1d1	stavební
tradice	tradice	k1gFnSc1	tradice
naopak	naopak	k6eAd1	naopak
chybí	chybit	k5eAaPmIp3nS	chybit
<g/>
.	.	kIx.	.
</s>
<s>
Přimda	Přimda	k1gFnSc1	Přimda
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
knížecím	knížecí	k2eAgInSc7d1	knížecí
majetkem	majetek	k1gInSc7	majetek
<g/>
,	,	kIx,	,
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
strážní	strážní	k2eAgNnSc4d1	strážní
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
norimberské	norimberský	k2eAgFnSc6d1	Norimberská
cestě	cesta	k1gFnSc6	cesta
u	u	k7c2	u
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
pohraničních	pohraniční	k2eAgInPc2d1	pohraniční
přechodů	přechod	k1gInPc2	přechod
do	do	k7c2	do
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
užívána	užívat	k5eAaImNgFnS	užívat
i	i	k9	i
jako	jako	k8xS	jako
vězení	vězení	k1gNnSc1	vězení
(	(	kIx(	(
<g/>
syn	syn	k1gMnSc1	syn
Soběslava	Soběslav	k1gMnSc2	Soběslav
I.	I.	kA	I.
Soběslav	Soběslav	k1gMnSc1	Soběslav
nebo	nebo	k8xC	nebo
Přemysl	Přemysl	k1gMnSc1	Přemysl
po	po	k7c6	po
nezdařeném	zdařený	k2eNgNnSc6d1	nezdařené
povstání	povstání	k1gNnSc6	povstání
proti	proti	k7c3	proti
svému	svůj	k3xOyFgMnSc3	svůj
otci	otec	k1gMnSc3	otec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1247	[number]	k4	1247
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
důležitost	důležitost	k1gFnSc4	důležitost
norimberské	norimberský	k2eAgFnSc2d1	Norimberská
cesty	cesta	k1gFnSc2	cesta
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
a	a	k8xC	a
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
rostla	růst	k5eAaImAgFnS	růst
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Přimda	Přimda	k1gFnSc1	Přimda
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
a	a	k8xC	a
zesílena	zesílit	k5eAaPmNgFnS	zesílit
během	během	k7c2	během
vlády	vláda	k1gFnSc2	vláda
Soběslava	Soběslav	k1gMnSc2	Soběslav
I.	I.	kA	I.
<g/>
,	,	kIx,	,
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
patřil	patřit	k5eAaImAgMnS	patřit
kastelán	kastelán	k1gMnSc1	kastelán
Přimdy	Přimda	k1gFnSc2	Přimda
mezi	mezi	k7c4	mezi
nejdůležitější	důležitý	k2eAgInPc4d3	nejdůležitější
úřady	úřad	k1gInPc4	úřad
v	v	k7c6	v
přemyslovském	přemyslovský	k2eAgInSc6d1	přemyslovský
státě	stát	k1gInSc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Jana	Jan	k1gMnSc2	Jan
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
byl	být	k5eAaImAgInS	být
hrad	hrad	k1gInSc1	hrad
zastaven	zastavit	k5eAaPmNgInS	zastavit
Vilémovi	Vilém	k1gMnSc3	Vilém
Zajícovi	Zajíc	k1gMnSc3	Zajíc
z	z	k7c2	z
Valdeka	Valdeko	k1gNnSc2	Valdeko
a	a	k8xC	a
poté	poté	k6eAd1	poté
rychtáři	rychtář	k1gMnSc3	rychtář
Jakubovi	Jakub	k1gMnSc3	Jakub
Frenclínovi	Frenclín	k1gMnSc3	Frenclín
<g/>
.	.	kIx.	.
</s>
<s>
Zpět	zpět	k6eAd1	zpět
Přimdu	Přimda	k1gFnSc4	Přimda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1344	[number]	k4	1344
získal	získat	k5eAaPmAgMnS	získat
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Václava	Václav	k1gMnSc2	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
byla	být	k5eAaImAgFnS	být
opět	opět	k6eAd1	opět
zastavována	zastavován	k2eAgFnSc1d1	zastavována
<g/>
,	,	kIx,	,
kontrolu	kontrola	k1gFnSc4	kontrola
panovník	panovník	k1gMnSc1	panovník
získal	získat	k5eAaPmAgInS	získat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1418	[number]	k4	1418
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Přimdu	Přimda	k1gFnSc4	Přimda
drženou	držený	k2eAgFnSc4d1	držená
loupeživým	loupeživý	k2eAgMnSc7d1	loupeživý
Borešem	Boreš	k1gMnSc7	Boreš
z	z	k7c2	z
Rýzmburka	Rýzmburek	k1gMnSc4	Rýzmburek
dobylo	dobýt	k5eAaPmAgNnS	dobýt
královské	královský	k2eAgNnSc1d1	královské
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1429	[number]	k4	1429
se	se	k3xPyFc4	se
pokusilo	pokusit	k5eAaPmAgNnS	pokusit
katolickou	katolický	k2eAgFnSc4d1	katolická
baštu	bašta	k1gFnSc4	bašta
drženou	držený	k2eAgFnSc4d1	držená
Jindřichem	Jindřich	k1gMnSc7	Jindřich
Žitem	žito	k1gNnSc7	žito
z	z	k7c2	z
Jivjan	Jivjany	k1gInPc2	Jivjany
neúspěšně	úspěšně	k6eNd1	úspěšně
dobýt	dobýt	k5eAaPmF	dobýt
husitské	husitský	k2eAgNnSc4d1	husitské
vojsko	vojsko	k1gNnSc4	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
hrad	hrad	k1gInSc1	hrad
opět	opět	k6eAd1	opět
často	často	k6eAd1	často
zastavován	zastavován	k2eAgInSc1d1	zastavován
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1454	[number]	k4	1454
<g/>
-	-	kIx~	-
<g/>
1592	[number]	k4	1592
např.	např.	kA	např.
pánům	pan	k1gMnPc3	pan
ze	z	k7c2	z
Švamberka	Švamberka	k1gFnSc1	Švamberka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1989	[number]	k4	1989
se	se	k3xPyFc4	se
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
po	po	k7c6	po
otřesu	otřes	k1gInSc6	otřes
půdy	půda	k1gFnSc2	půda
sesula	sesout	k5eAaPmAgFnS	sesout
část	část	k1gFnSc1	část
zdi	zeď	k1gFnSc2	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
zasypáni	zasypat	k5eAaPmNgMnP	zasypat
tři	tři	k4xCgMnPc1	tři
dvanáctiletí	dvanáctiletý	k2eAgMnPc1d1	dvanáctiletý
chlapci	chlapec	k1gMnPc1	chlapec
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
jeden	jeden	k4xCgInSc1	jeden
na	na	k7c4	na
následky	následek	k1gInPc4	následek
zranění	zranění	k1gNnSc2	zranění
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
události	událost	k1gFnSc6	událost
se	se	k3xPyFc4	se
v	v	k7c6	v
novinách	novina	k1gFnPc6	novina
objevovalo	objevovat	k5eAaImAgNnS	objevovat
tvrzení	tvrzení	k1gNnSc1	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
zeď	zeď	k1gFnSc1	zeď
spadla	spadnout	k5eAaPmAgFnS	spadnout
následkem	následkem	k7c2	následkem
hluku	hluk	k1gInSc2	hluk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
skupina	skupina	k1gFnSc1	skupina
dětí	dítě	k1gFnPc2	dítě
způsobovala	způsobovat	k5eAaImAgFnS	způsobovat
-	-	kIx~	-
objevila	objevit	k5eAaPmAgFnS	objevit
se	se	k3xPyFc4	se
i	i	k9	i
hypotéza	hypotéza	k1gFnSc1	hypotéza
<g/>
,	,	kIx,	,
že	že	k8xS	že
neštěstí	neštěstí	k1gNnSc1	neštěstí
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
předchozími	předchozí	k2eAgInPc7d1	předchozí
nedokončenými	dokončený	k2eNgInPc7d1	nedokončený
stavebními	stavební	k2eAgInPc7d1	stavební
zásahy	zásah	k1gInPc7	zásah
firmy	firma	k1gFnSc2	firma
Armabeton	Armabeton	k1gInSc1	Armabeton
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
2001	[number]	k4	2001
<g/>
-	-	kIx~	-
<g/>
2010	[number]	k4	2010
byla	být	k5eAaImAgFnS	být
zřícenina	zřícenina	k1gFnSc1	zřícenina
staticky	staticky	k6eAd1	staticky
zajištěna	zajistit	k5eAaPmNgFnS	zajistit
a	a	k8xC	a
restaurována	restaurovat	k5eAaBmNgFnS	restaurovat
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
na	na	k7c6	na
dominantním	dominantní	k2eAgInSc6d1	dominantní
vrcholu	vrchol	k1gInSc6	vrchol
tvořila	tvořit	k5eAaImAgFnS	tvořit
vysoká	vysoký	k2eAgFnSc1d1	vysoká
hranolová	hranolový	k2eAgFnSc1d1	hranolová
věž	věž	k1gFnSc1	věž
o	o	k7c6	o
čtvercovém	čtvercový	k2eAgInSc6d1	čtvercový
půdorysu	půdorys	k1gInSc6	půdorys
(	(	kIx(	(
<g/>
cca	cca	kA	cca
16	[number]	k4	16
×	×	k?	×
16	[number]	k4	16
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
donjon	donjon	k1gInSc1	donjon
<g/>
,	,	kIx,	,
postavená	postavený	k2eAgFnSc1d1	postavená
z	z	k7c2	z
žulových	žulový	k2eAgInPc2d1	žulový
kvádrů	kvádr	k1gInPc2	kvádr
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
obytnou	obytný	k2eAgFnSc4d1	obytná
funkci	funkce	k1gFnSc4	funkce
<g/>
:	:	kIx,	:
k	k	k7c3	k
příslušenství	příslušenství	k1gNnSc3	příslušenství
patřil	patřit	k5eAaImAgInS	patřit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
krb	krb	k1gInSc4	krb
a	a	k8xC	a
prevét	prevét	k?	prevét
(	(	kIx(	(
<g/>
nejstarší	starý	k2eAgInSc1d3	nejstarší
dochovaný	dochovaný	k2eAgInSc1d1	dochovaný
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
dalších	další	k2eAgFnPc6d1	další
původních	původní	k2eAgFnPc6d1	původní
stavbách	stavba	k1gFnPc6	stavba
nemáme	mít	k5eNaImIp1nP	mít
doklady	doklad	k1gInPc1	doklad
vlivem	vlivem	k7c2	vlivem
úprav	úprava	k1gFnPc2	úprava
a	a	k8xC	a
oprav	oprava	k1gFnPc2	oprava
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zde	zde	k6eAd1	zde
probíhaly	probíhat	k5eAaImAgFnP	probíhat
v	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vrcholném	vrcholný	k2eAgInSc6d1	vrcholný
středověku	středověk	k1gInSc6	středověk
byl	být	k5eAaImAgInS	být
hrad	hrad	k1gInSc1	hrad
dále	daleko	k6eAd2	daleko
rozšiřován	rozšiřovat	k5eAaImNgInS	rozšiřovat
<g/>
,	,	kIx,	,
okolo	okolo	k7c2	okolo
donjonu	donjon	k1gInSc2	donjon
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
kamenná	kamenný	k2eAgFnSc1d1	kamenná
hradba	hradba	k1gFnSc1	hradba
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
byly	být	k5eAaImAgFnP	být
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
obranného	obranný	k2eAgInSc2d1	obranný
systému	systém	k1gInSc2	systém
vystavěny	vystavěn	k2eAgFnPc4d1	vystavěna
další	další	k2eAgFnPc4d1	další
dvě	dva	k4xCgFnPc4	dva
věže	věž	k1gFnPc4	věž
<g/>
.	.	kIx.	.
</s>
<s>
Legenda	legenda	k1gFnSc1	legenda
o	o	k7c6	o
hradě	hrad	k1gInSc6	hrad
Přimda	Přimdo	k1gNnSc2	Přimdo
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
i	i	k9	i
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
kniha	kniha	k1gFnSc1	kniha
historických	historický	k2eAgInPc2d1	historický
příběhů	příběh	k1gInPc2	příběh
<g/>
,	,	kIx,	,
v	v	k7c6	v
příběhu	příběh	k1gInSc6	příběh
Vítek	Vítek	k1gMnSc1	Vítek
Vítkovič	Vítkovič	k1gMnSc1	Vítkovič
(	(	kIx(	(
<g/>
autor	autor	k1gMnSc1	autor
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Kliment	Kliment	k1gMnSc1	Kliment
Klicpera	Klicpera	k1gFnSc1	Klicpera
<g/>
)	)	kIx)	)
o	o	k7c6	o
rytíři	rytíř	k1gMnSc6	rytíř
Vítkovi	Vítek	k1gMnSc6	Vítek
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
chránit	chránit	k5eAaImF	chránit
svého	svůj	k3xOyFgMnSc4	svůj
pána	pán	k1gMnSc4	pán
kníže	kníže	k1gMnSc1	kníže
Oldřicha	Oldřich	k1gMnSc4	Oldřich
i	i	k9	i
přes	přes	k7c4	přes
všechny	všechen	k3xTgFnPc4	všechen
útrapy	útrapa	k1gFnPc4	útrapa
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Vršovců	Vršovec	k1gInPc2	Vršovec
<g/>
.	.	kIx.	.
</s>
<s>
Vítek	Vítek	k1gMnSc1	Vítek
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
hradě	hrad	k1gInSc6	hrad
coby	coby	k?	coby
vězeň	vězeň	k1gMnSc1	vězeň
a	a	k8xC	a
uslyšel	uslyšet	k5eAaPmAgMnS	uslyšet
jeho	on	k3xPp3gInSc4	on
příběh	příběh	k1gInSc4	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Kdysi	kdysi	k6eAd1	kdysi
dávno	dávno	k6eAd1	dávno
dal	dát	k5eAaPmAgMnS	dát
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
legendy	legenda	k1gFnSc2	legenda
<g/>
,	,	kIx,	,
dal	dát	k5eAaPmAgMnS	dát
hrad	hrad	k1gInSc4	hrad
vystavět	vystavět	k5eAaPmF	vystavět
Albert	Albert	k1gMnSc1	Albert
markrabě	markrabě	k1gMnSc1	markrabě
vysokohradský	vysokohradský	k2eAgMnSc1d1	vysokohradský
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
unesl	unést	k5eAaPmAgMnS	unést
Helenu	Helena	k1gFnSc4	Helena
<g/>
,	,	kIx,	,
dceru	dcera	k1gFnSc4	dcera
císaře	císař	k1gMnSc2	císař
Jindřicha	Jindřich	k1gMnSc2	Jindřich
zvaného	zvaný	k2eAgInSc2d1	zvaný
Ptáčník	Ptáčník	k1gMnSc1	Ptáčník
<g/>
.	.	kIx.	.
</s>
<s>
Dívku	dívka	k1gFnSc4	dívka
chtěl	chtít	k5eAaImAgMnS	chtít
ukrýt	ukrýt	k5eAaPmF	ukrýt
na	na	k7c6	na
tajném	tajný	k2eAgNnSc6d1	tajné
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
tak	tak	k6eAd1	tak
svolal	svolat	k5eAaPmAgMnS	svolat
mnoho	mnoho	k4c4	mnoho
dělníků	dělník	k1gMnPc2	dělník
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
slíbil	slíbit	k5eAaPmAgInS	slíbit
vysokou	vysoký	k2eAgFnSc4d1	vysoká
odměnu	odměna	k1gFnSc4	odměna
<g/>
.	.	kIx.	.
</s>
<s>
Podmínkou	podmínka	k1gFnSc7	podmínka
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
nikdo	nikdo	k3yNnSc1	nikdo
nesmí	smět	k5eNaImIp3nS	smět
opustit	opustit	k5eAaPmF	opustit
staveniště	staveniště	k1gNnSc4	staveniště
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
hrad	hrad	k1gInSc1	hrad
nebude	být	k5eNaImBp3nS	být
zcela	zcela	k6eAd1	zcela
hotov	hotov	k2eAgInSc1d1	hotov
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
byl	být	k5eAaImAgInS	být
vymyšlen	vymyslet	k5eAaPmNgInS	vymyslet
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
jediná	jediný	k2eAgFnSc1d1	jediná
přístupová	přístupový	k2eAgFnSc1d1	přístupová
cesta	cesta	k1gFnSc1	cesta
vedla	vést	k5eAaImAgFnS	vést
přes	přes	k7c4	přes
dubové	dubový	k2eAgFnPc4d1	dubová
větve	větev	k1gFnPc4	větev
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byly	být	k5eAaImAgFnP	být
práce	práce	k1gFnPc1	práce
hotovy	hotov	k2eAgFnPc1d1	hotova
<g/>
,	,	kIx,	,
uspořádal	uspořádat	k5eAaPmAgMnS	uspořádat
markrabě	markrabě	k1gMnSc1	markrabě
hodokvas	hodokvasit	k5eAaImRp2nS	hodokvasit
<g/>
.	.	kIx.	.
</s>
<s>
Dělníci	dělník	k1gMnPc1	dělník
se	se	k3xPyFc4	se
usadili	usadit	k5eAaPmAgMnP	usadit
u	u	k7c2	u
dlouhého	dlouhý	k2eAgInSc2d1	dlouhý
stolu	stol	k1gInSc2	stol
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
v	v	k7c6	v
hodování	hodování	k1gNnSc6	hodování
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
se	se	k3xPyFc4	se
však	však	k9	však
markrabě	markrabě	k1gMnSc1	markrabě
začal	začít	k5eAaPmAgMnS	začít
modlit	modlit	k5eAaImF	modlit
tak	tak	k9	tak
vroucně	vroucně	k6eAd1	vroucně
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
vstal	vstát	k5eAaPmAgMnS	vstát
<g/>
.	.	kIx.	.
</s>
<s>
Dělníci	dělník	k1gMnPc1	dělník
učinili	učinit	k5eAaImAgMnP	učinit
totéž	týž	k3xTgNnSc4	týž
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gInSc2	jeho
příkladu	příklad	k1gInSc2	příklad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
ten	ten	k3xDgInSc4	ten
okamžik	okamžik	k1gInSc4	okamžik
však	však	k9	však
markrabě	markrabě	k1gMnSc1	markrabě
spustil	spustit	k5eAaPmAgMnS	spustit
skrytý	skrytý	k2eAgInSc4d1	skrytý
mechanismus	mechanismus	k1gInSc4	mechanismus
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
dělníci	dělník	k1gMnPc1	dělník
skončili	skončit	k5eAaPmAgMnP	skončit
po	po	k7c6	po
desítkách	desítka	k1gFnPc6	desítka
metrů	metr	k1gInPc2	metr
pádu	pád	k1gInSc2	pád
v	v	k7c6	v
hluboké	hluboký	k2eAgFnSc6d1	hluboká
jámě	jáma	k1gFnSc6	jáma
pod	pod	k7c7	pod
hodovní	hodovní	k2eAgFnSc7d1	hodovní
místností	místnost	k1gFnSc7	místnost
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc4	hrad
pak	pak	k6eAd1	pak
našel	najít	k5eAaPmAgMnS	najít
rytíř	rytíř	k1gMnSc1	rytíř
Příma	Přím	k1gMnSc2	Přím
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
za	za	k7c2	za
jeho	jeho	k3xOp3gMnPc2	jeho
služby	služba	k1gFnPc4	služba
knížeti	kníže	k1gMnSc3	kníže
Oldřichovi	Oldřich	k1gMnSc3	Oldřich
<g/>
,	,	kIx,	,
směl	smět	k5eAaImAgMnS	smět
hrad	hrad	k1gInSc4	hrad
pojmenovat	pojmenovat	k5eAaPmF	pojmenovat
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
hrad	hrad	k1gInSc1	hrad
dostal	dostat	k5eAaPmAgInS	dostat
jméno	jméno	k1gNnSc4	jméno
Přimda	Přimdo	k1gNnSc2	Přimdo
<g/>
.	.	kIx.	.
</s>
<s>
BENEŠOVSKÁ	benešovský	k2eAgFnSc1d1	Benešovská
<g/>
,	,	kIx,	,
Klára	Klára	k1gFnSc1	Klára
<g/>
;	;	kIx,	;
SOUKUPOVÁ	Soukupová	k1gFnSc1	Soukupová
<g/>
,	,	kIx,	,
Helena	Helena	k1gFnSc1	Helena
<g/>
;	;	kIx,	;
MENCL	Mencl	k1gMnSc1	Mencl
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Předrománská	předrománský	k2eAgFnSc1d1	předrománská
a	a	k8xC	a
románská	románský	k2eAgFnSc1d1	románská
architektura	architektura	k1gFnSc1	architektura
v	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Plzeň	Plzeň	k1gFnSc1	Plzeň
:	:	kIx,	:
Západočeské	západočeský	k2eAgNnSc1d1	Západočeské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
62	[number]	k4	62
s.	s.	k?	s.
DURDÍK	DURDÍK	kA	DURDÍK
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
<g/>
;	;	kIx,	;
SUŠICKÝ	sušický	k2eAgMnSc1d1	sušický
<g/>
,	,	kIx,	,
Viktor	Viktor	k1gMnSc1	Viktor
<g/>
.	.	kIx.	.
</s>
<s>
Zříceniny	zřícenina	k1gFnPc1	zřícenina
hradů	hrad	k1gInPc2	hrad
<g/>
,	,	kIx,	,
tvrzí	tvrz	k1gFnPc2	tvrz
a	a	k8xC	a
zámků	zámek	k1gInPc2	zámek
<g/>
:	:	kIx,	:
Západní	západní	k2eAgFnPc1d1	západní
Čechy	Čechy	k1gFnPc1	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Agentura	agentura	k1gFnSc1	agentura
Pankrác	Pankrác	k1gFnSc1	Pankrác
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
322	[number]	k4	322
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86781	[number]	k4	86781
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Přimda	Přimda	k1gFnSc1	Přimda
<g/>
,	,	kIx,	,
s.	s.	k?	s.
164	[number]	k4	164
<g/>
-	-	kIx~	-
<g/>
167	[number]	k4	167
<g/>
.	.	kIx.	.
</s>
<s>
HÝZLER	HÝZLER	kA	HÝZLER
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
;	;	kIx,	;
HOLANOVÁ	Holanová	k1gFnSc1	Holanová
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
<g/>
;	;	kIx,	;
NYKODÝMOVÁ	NYKODÝMOVÁ	kA	NYKODÝMOVÁ
<g/>
,	,	kIx,	,
Milada	Milada	k1gFnSc1	Milada
<g/>
.	.	kIx.	.
</s>
<s>
Přimda	Přimda	k1gFnSc1	Přimda
<g/>
,	,	kIx,	,
stavebně	stavebně	k6eAd1	stavebně
historický	historický	k2eAgInSc4d1	historický
průzkum	průzkum	k1gInSc4	průzkum
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Státní	státní	k2eAgInSc1d1	státní
ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
rekonstrukce	rekonstrukce	k1gFnPc4	rekonstrukce
památkových	památkový	k2eAgNnPc2d1	památkové
měst	město	k1gNnPc2	město
a	a	k8xC	a
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
</s>
<s>
PROCHÁZKA	Procházka	k1gMnSc1	Procházka
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
;	;	kIx,	;
ÚLOVEC	ÚLOVEC	kA	ÚLOVEC
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Hrady	hrad	k1gInPc1	hrad
<g/>
,	,	kIx,	,
zámky	zámek	k1gInPc1	zámek
a	a	k8xC	a
tvrze	tvrz	k1gFnPc1	tvrz
okresu	okres	k1gInSc2	okres
Tachov	Tachov	k1gInSc1	Tachov
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Tachov	Tachov	k1gInSc1	Tachov
:	:	kIx,	:
Okresní	okresní	k2eAgNnSc1d1	okresní
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Tachově	Tachov	k1gInSc6	Tachov
<g/>
.	.	kIx.	.
</s>
<s>
TUREK	Turek	k1gMnSc1	Turek
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
.	.	kIx.	.
</s>
<s>
Stavební	stavební	k2eAgInSc1d1	stavební
ráz	ráz	k1gInSc1	ráz
středověkých	středověký	k2eAgMnPc2d1	středověký
hradů	hrad	k1gInPc2	hrad
na	na	k7c6	na
Tachovsku	Tachovsko	k1gNnSc6	Tachovsko
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
Sborník	sborník	k1gInSc1	sborník
okresního	okresní	k2eAgNnSc2d1	okresní
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Tachově	Tachov	k1gInSc6	Tachov
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Tachov	Tachov	k1gInSc1	Tachov
:	:	kIx,	:
Okresní	okresní	k2eAgNnSc1d1	okresní
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Tachově	Tachov	k1gInSc6	Tachov
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
</s>
<s>
SEDLÁČEK	Sedláček	k1gMnSc1	Sedláček
<g/>
,	,	kIx,	,
August	August	k1gMnSc1	August
<g/>
.	.	kIx.	.
</s>
<s>
Hrady	hrad	k1gInPc1	hrad
<g/>
,	,	kIx,	,
zámky	zámek	k1gInPc1	zámek
a	a	k8xC	a
tvrze	tvrz	k1gFnPc1	tvrz
Království	království	k1gNnSc2	království
českého	český	k2eAgNnSc2d1	české
<g/>
:	:	kIx,	:
Plzeňsko	Plzeňsko	k1gNnSc1	Plzeňsko
a	a	k8xC	a
Loketsko	Loketsko	k1gNnSc1	Loketsko
<g/>
.	.	kIx.	.
</s>
<s>
Svazek	svazek	k1gInSc1	svazek
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Jiří	Jiří	k1gMnSc1	Jiří
Čížek	Čížek	k1gMnSc1	Čížek
-	-	kIx~	-
ViGo	ViGo	k1gMnSc1	ViGo
agency	agenca	k1gFnSc2	agenca
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
250	[number]	k4	250
s.	s.	k?	s.
Kapitola	kapitola	k1gFnSc1	kapitola
Přimda	Přimda	k1gFnSc1	Přimda
<g/>
,	,	kIx,	,
s.	s.	k?	s.
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Přimda	Přimdo	k1gNnSc2	Přimdo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
webové	webový	k2eAgFnSc2d1	webová
stránky	stránka	k1gFnSc2	stránka
Hrad	hrad	k1gInSc1	hrad
na	na	k7c6	na
webu	web	k1gInSc6	web
města	město	k1gNnSc2	město
Přimda	Přimd	k1gMnSc2	Přimd
</s>
