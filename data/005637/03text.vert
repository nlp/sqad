<s>
Michal	Michal	k1gMnSc1	Michal
Viewegh	Viewegh	k1gMnSc1	Viewegh
(	(	kIx(	(
<g/>
*	*	kIx~	*
31	[number]	k4	31
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1962	[number]	k4	1962
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
publicista	publicista	k1gMnSc1	publicista
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
držitelem	držitel	k1gMnSc7	držitel
prestižní	prestižní	k2eAgFnSc2d1	prestižní
Ceny	cena	k1gFnSc2	cena
Jiřího	Jiří	k1gMnSc2	Jiří
Ortena	Orten	k1gMnSc2	Orten
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nejprodávanějším	prodávaný	k2eAgMnSc7d3	nejprodávanější
českým	český	k2eAgMnSc7d1	český
spisovatelem	spisovatel	k1gMnSc7	spisovatel
<g/>
,	,	kIx,	,
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
milionem	milion	k4xCgInSc7	milion
výtisků	výtisk	k1gInPc2	výtisk
<g/>
.	.	kIx.	.
</s>
<s>
Přežil	přežít	k5eAaPmAgMnS	přežít
prasknutí	prasknutí	k1gNnSc1	prasknutí
aorty	aorta	k1gFnSc2	aorta
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
osobnosti	osobnost	k1gFnPc4	osobnost
českého	český	k2eAgInSc2d1	český
společenského	společenský	k2eAgInSc2d1	společenský
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
soudí	soudit	k5eAaImIp3nP	soudit
s	s	k7c7	s
bulvárním	bulvární	k2eAgInSc7d1	bulvární
tiskem	tisk	k1gInSc7	tisk
o	o	k7c4	o
odškodnění	odškodnění	k1gNnSc4	odškodnění
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
Michala	Michal	k1gMnSc2	Michal
Viewegha	Viewegh	k1gMnSc2	Viewegh
je	být	k5eAaImIp3nS	být
právnička	právnička	k1gFnSc1	právnička
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
inženýr-chemik	inženýrhemik	k1gMnSc1	inženýr-chemik
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
starosta	starosta	k1gMnSc1	starosta
Sázavy	Sázava	k1gFnSc2	Sázava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
maturoval	maturovat	k5eAaBmAgMnS	maturovat
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Benešově	Benešov	k1gInSc6	Benešov
<g/>
.	.	kIx.	.
</s>
<s>
Nedokončil	dokončit	k5eNaPmAgMnS	dokončit
studium	studium	k1gNnSc4	studium
ekonomie	ekonomie	k1gFnSc2	ekonomie
na	na	k7c4	na
VŠE	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
češtinu	čeština	k1gFnSc4	čeština
a	a	k8xC	a
pedagogiku	pedagogika	k1gFnSc4	pedagogika
na	na	k7c6	na
Filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
studium	studium	k1gNnSc4	studium
dokončil	dokončit	k5eAaPmAgMnS	dokončit
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
svého	svůj	k3xOyFgNnSc2	svůj
studia	studio	k1gNnSc2	studio
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
publikoval	publikovat	k5eAaBmAgInS	publikovat
povídky	povídka	k1gFnSc2	povídka
v	v	k7c6	v
Mladé	mladý	k2eAgFnSc6d1	mladá
frontě	fronta	k1gFnSc6	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
se	se	k3xPyFc4	se
krátce	krátce	k6eAd1	krátce
věnoval	věnovat	k5eAaPmAgMnS	věnovat
vystudovanému	vystudovaný	k2eAgInSc3d1	vystudovaný
oboru	obor	k1gInSc3	obor
<g/>
,	,	kIx,	,
učil	učit	k5eAaImAgInS	učit
na	na	k7c6	na
základní	základní	k2eAgFnSc6d1	základní
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
5	[number]	k4	5
–	–	k?	–
Zbraslavi	Zbraslav	k1gFnSc6	Zbraslav
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
učitelské	učitelský	k2eAgNnSc4d1	učitelské
povolání	povolání	k1gNnPc4	povolání
opustil	opustit	k5eAaPmAgInS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
redaktorem	redaktor	k1gMnSc7	redaktor
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
definitivně	definitivně	k6eAd1	definitivně
spisovatelem	spisovatel	k1gMnSc7	spisovatel
z	z	k7c2	z
povolání	povolání	k1gNnSc2	povolání
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
románů	román	k1gInPc2	román
je	být	k5eAaImIp3nS	být
i	i	k9	i
autorem	autor	k1gMnSc7	autor
literárních	literární	k2eAgFnPc2d1	literární
parodií	parodie	k1gFnPc2	parodie
(	(	kIx(	(
<g/>
Nápady	nápad	k1gInPc1	nápad
laskavého	laskavý	k2eAgMnSc2d1	laskavý
čtenáře	čtenář	k1gMnSc2	čtenář
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
udělena	udělen	k2eAgFnSc1d1	udělena
prestižní	prestižní	k2eAgFnSc1d1	prestižní
Ceny	cena	k1gFnPc1	cena
Jiřího	Jiří	k1gMnSc2	Jiří
Ortena	Orten	k1gMnSc2	Orten
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
prvním	první	k4xOgMnSc7	první
českým	český	k2eAgMnSc7d1	český
spisovatelem	spisovatel	k1gMnSc7	spisovatel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
vyzkoušel	vyzkoušet	k5eAaPmAgMnS	vyzkoušet
psaní	psaní	k1gNnSc4	psaní
románu	román	k1gInSc2	román
spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
čtenáři	čtenář	k1gMnPc1	čtenář
na	na	k7c6	na
Internetu	Internet	k1gInSc6	Internet
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Blogový	Blogový	k2eAgInSc1d1	Blogový
román	román	k1gInSc1	román
Srdce	srdce	k1gNnSc2	srdce
domova	domov	k1gInSc2	domov
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
otevřenou	otevřený	k2eAgFnSc7d1	otevřená
školou	škola	k1gFnSc7	škola
tvůrčího	tvůrčí	k2eAgNnSc2d1	tvůrčí
psaní	psaní	k1gNnSc2	psaní
<g/>
,	,	kIx,	,
vznikající	vznikající	k2eAgInPc4d1	vznikající
mezi	mezi	k7c7	mezi
srpnem	srpen	k1gInSc7	srpen
a	a	k8xC	a
prosincem	prosinec	k1gInSc7	prosinec
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Michal	Michal	k1gMnSc1	Michal
Viewegh	Viewegh	k1gMnSc1	Viewegh
však	však	k9	však
přiznává	přiznávat	k5eAaImIp3nS	přiznávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
spisovatelství	spisovatelství	k1gNnSc1	spisovatelství
je	být	k5eAaImIp3nS	být
osamělé	osamělý	k2eAgNnSc4d1	osamělé
povolání	povolání	k1gNnSc4	povolání
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgInSc6	který
tráví	trávit	k5eAaImIp3nS	trávit
s	s	k7c7	s
počítačem	počítač	k1gInSc7	počítač
i	i	k9	i
10	[number]	k4	10
hodin	hodina	k1gFnPc2	hodina
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2012	[number]	k4	2012
mu	on	k3xPp3gNnSc3	on
praskla	prasknout	k5eAaPmAgFnS	prasknout
aorta	aorta	k1gFnSc1	aorta
<g/>
,	,	kIx,	,
ležel	ležet	k5eAaImAgMnS	ležet
v	v	k7c6	v
IKEMu	IKEMus	k1gInSc6	IKEMus
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
stav	stav	k1gInSc1	stav
byl	být	k5eAaImAgInS	být
stabilizován	stabilizovat	k5eAaBmNgInS	stabilizovat
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
rehabilitoval	rehabilitovat	k5eAaBmAgMnS	rehabilitovat
na	na	k7c6	na
klinice	klinika	k1gFnSc6	klinika
Malvazinky	malvazinka	k1gFnSc2	malvazinka
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
trpí	trpět	k5eAaImIp3nP	trpět
melancholií	melancholie	k1gFnPc2	melancholie
a	a	k8xC	a
výpadky	výpadek	k1gInPc4	výpadek
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
událost	událost	k1gFnSc1	událost
mu	on	k3xPp3gMnSc3	on
pomohla	pomoct	k5eAaPmAgFnS	pomoct
si	se	k3xPyFc3	se
uvědomit	uvědomit	k5eAaPmF	uvědomit
relativitu	relativita	k1gFnSc4	relativita
některých	některý	k3yIgInPc2	některý
problémů	problém	k1gInPc2	problém
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Dívám	dívat	k5eAaImIp1nS	dívat
se	se	k3xPyFc4	se
do	do	k7c2	do
zahrady	zahrada	k1gFnSc2	zahrada
a	a	k8xC	a
najednou	najednou	k6eAd1	najednou
se	se	k3xPyFc4	se
vnitřně	vnitřně	k6eAd1	vnitřně
zklidním	zklidnit	k5eAaPmIp1nS	zklidnit
a	a	k8xC	a
připomenu	připomenout	k5eAaPmIp1nS	připomenout
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
90	[number]	k4	90
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
prasklou	prasklý	k2eAgFnSc7d1	prasklá
aortou	aorta	k1gFnSc7	aorta
nepřežije	přežít	k5eNaPmIp3nS	přežít
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
nesleduju	sledovat	k5eNaImIp1nS	sledovat
kritiku	kritika	k1gFnSc4	kritika
<g/>
,	,	kIx,	,
bulvár	bulvár	k1gInSc4	bulvár
ani	ani	k8xC	ani
politiku	politika	k1gFnSc4	politika
<g/>
.	.	kIx.	.
</s>
<s>
Nenakupuju	nakupovat	k5eNaBmIp1nS	nakupovat
<g/>
,	,	kIx,	,
nic	nic	k3yNnSc1	nic
nesháním	shánět	k5eNaImIp1nS	shánět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Michal	Michal	k1gMnSc1	Michal
Viewegh	Viewegh	k1gMnSc1	Viewegh
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgMnPc4	dva
bratry	bratr	k1gMnPc4	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Viewegh	Viewegh	k1gMnSc1	Viewegh
je	být	k5eAaImIp3nS	být
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
producent	producent	k1gMnSc1	producent
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
dvakrát	dvakrát	k6eAd1	dvakrát
ženatý	ženatý	k2eAgMnSc1d1	ženatý
<g/>
,	,	kIx,	,
nejdřív	dříve	k6eAd3	dříve
s	s	k7c7	s
Jaroslavou	Jaroslava	k1gFnSc7	Jaroslava
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Mackovou	Macková	k1gFnSc4	Macková
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
si	se	k3xPyFc3	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
vzal	vzít	k5eAaPmAgInS	vzít
Veronikou	Veronika	k1gFnSc7	Veronika
Vieweghovou	Vieweghový	k2eAgFnSc7d1	Vieweghový
<g/>
,	,	kIx,	,
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Kodýtkovou	Kodýtková	k1gFnSc4	Kodýtková
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
dcery	dcera	k1gFnPc4	dcera
<g/>
,	,	kIx,	,
Michaelu	Michaela	k1gFnSc4	Michaela
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sáru	Sára	k1gFnSc4	Sára
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
a	a	k8xC	a
Barboru	Barbora	k1gFnSc4	Barbora
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
života	život	k1gInSc2	život
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Sázavě	Sázava	k1gFnSc6	Sázava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
se	se	k3xPyFc4	se
rozvedl	rozvést	k5eAaPmAgMnS	rozvést
<g/>
.	.	kIx.	.
</s>
<s>
Báječná	báječný	k2eAgNnPc1d1	báječné
léta	léto	k1gNnPc1	léto
pod	pod	k7c4	pod
psa	pes	k1gMnSc4	pes
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
–	–	k?	–
autobiografický	autobiografický	k2eAgInSc4d1	autobiografický
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
zachycující	zachycující	k2eAgFnSc4d1	zachycující
atmosféru	atmosféra	k1gFnSc4	atmosféra
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
normalizace	normalizace	k1gFnSc2	normalizace
až	až	k9	až
po	po	k7c4	po
pád	pád	k1gInSc4	pád
komunismu	komunismus	k1gInSc2	komunismus
<g/>
;	;	kIx,	;
zfilmováno	zfilmovat	k5eAaPmNgNnS	zfilmovat
1997	[number]	k4	1997
(	(	kIx(	(
<g/>
Báječná	báječný	k2eAgNnPc4d1	báječné
léta	léto	k1gNnPc4	léto
pod	pod	k7c4	pod
psa	pes	k1gMnSc4	pes
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Petr	Petr	k1gMnSc1	Petr
Nikolaev	Nikolaev	k1gFnSc1	Nikolaev
<g/>
)	)	kIx)	)
Výchova	výchova	k1gFnSc1	výchova
dívek	dívka	k1gFnPc2	dívka
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
–	–	k?	–
tragická	tragický	k2eAgFnSc1d1	tragická
love	lov	k1gInSc5	lov
<g />
.	.	kIx.	.
</s>
<s>
story	story	k1gFnSc1	story
o	o	k7c6	o
zbraslavském	zbraslavský	k2eAgMnSc6d1	zbraslavský
učiteli	učitel	k1gMnSc6	učitel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
přijme	přijmout	k5eAaPmIp3nS	přijmout
nabídku	nabídka	k1gFnSc4	nabídka
podnikatele	podnikatel	k1gMnSc2	podnikatel
Krále	Král	k1gMnSc2	Král
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dával	dávat	k5eAaImAgMnS	dávat
jeho	jeho	k3xOp3gFnSc3	jeho
dceři	dcera	k1gFnSc3	dcera
kurz	kurz	k1gInSc4	kurz
tvůrčího	tvůrčí	k2eAgNnSc2d1	tvůrčí
psaní	psaní	k1gNnSc2	psaní
<g/>
,	,	kIx,	,
vztah	vztah	k1gInSc1	vztah
Beáty	Beáta	k1gFnSc2	Beáta
s	s	k7c7	s
učitelem	učitel	k1gMnSc7	učitel
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
<g/>
;	;	kIx,	;
zfilmováno	zfilmovat	k5eAaPmNgNnS	zfilmovat
1996	[number]	k4	1996
(	(	kIx(	(
<g/>
Výchova	výchova	k1gFnSc1	výchova
dívek	dívka	k1gFnPc2	dívka
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Petr	Petr	k1gMnSc1	Petr
Koliha	koliha	k1gFnSc1	koliha
<g/>
)	)	kIx)	)
Účastníci	účastník	k1gMnPc1	účastník
zájezdu	zájezd	k1gInSc2	zájezd
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
1996	[number]	k4	1996
–	–	k?	–
základem	základ	k1gInSc7	základ
zápletky	zápletka	k1gFnSc2	zápletka
je	být	k5eAaImIp3nS	být
setkání	setkání	k1gNnPc1	setkání
velmi	velmi	k6eAd1	velmi
rozdílných	rozdílný	k2eAgMnPc2d1	rozdílný
lidí	člověk	k1gMnPc2	člověk
na	na	k7c6	na
autobusovém	autobusový	k2eAgInSc6d1	autobusový
zájezdu	zájezd	k1gInSc6	zájezd
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
;	;	kIx,	;
zfilmováno	zfilmovat	k5eAaPmNgNnS	zfilmovat
2006	[number]	k4	2006
(	(	kIx(	(
<g/>
Účastníci	účastník	k1gMnPc1	účastník
zájezdu	zájezd	k1gInSc2	zájezd
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Jiří	Jiří	k1gMnSc1	Jiří
Vejdělek	Vejdělek	k1gInSc1	Vejdělek
<g/>
)	)	kIx)	)
Zapisovatelé	zapisovatel	k1gMnPc1	zapisovatel
otcovský	otcovský	k2eAgInSc4d1	otcovský
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
–	–	k?	–
román	román	k1gInSc1	román
o	o	k7c6	o
rodině	rodina	k1gFnSc6	rodina
mladého	mladý	k2eAgMnSc2d1	mladý
devianta	deviant	k1gMnSc2	deviant
Román	Román	k1gMnSc1	Román
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
–	–	k?	–
zfilmováno	zfilmován	k2eAgNnSc4d1	zfilmováno
2005	[number]	k4	2005
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Román	román	k1gInSc1	román
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Filip	Filip	k1gMnSc1	Filip
Renč	Renč	k?	Renč
<g/>
)	)	kIx)	)
Báječná	báječný	k2eAgNnPc1d1	báječné
léta	léto	k1gNnPc1	léto
s	s	k7c7	s
Klausem	Klaus	k1gMnSc7	Klaus
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
–	–	k?	–
volné	volný	k2eAgNnSc4d1	volné
pokračování	pokračování	k1gNnSc4	pokračování
úspěšných	úspěšný	k2eAgNnPc2d1	úspěšné
Báječných	báječný	k2eAgNnPc2d1	báječné
let	léto	k1gNnPc2	léto
pod	pod	k7c4	pod
psa	pes	k1gMnSc4	pes
Případ	případ	k1gInSc4	případ
nevěrné	věrný	k2eNgFnSc2d1	nevěrná
Kláry	Klára	k1gFnSc2	Klára
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
–	–	k?	–
populární	populární	k2eAgMnSc1d1	populární
spisovatel	spisovatel	k1gMnSc1	spisovatel
si	se	k3xPyFc3	se
najme	najmout	k5eAaPmIp3nS	najmout
soukromého	soukromý	k2eAgMnSc4d1	soukromý
detektiva	detektiv	k1gMnSc4	detektiv
na	na	k7c4	na
sledování	sledování	k1gNnSc4	sledování
svojí	svůj	k3xOyFgFnSc2	svůj
manželky	manželka	k1gFnSc2	manželka
<g/>
;	;	kIx,	;
zfilmováno	zfilmovat	k5eAaPmNgNnS	zfilmovat
2009	[number]	k4	2009
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Případ	případ	k1gInSc1	případ
nevěrné	věrný	k2eNgFnSc2d1	nevěrná
Kláry	Klára	k1gFnSc2	Klára
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc2	režie
Roberto	Roberta	k1gFnSc5	Roberta
Faenza	Faenza	k1gFnSc1	Faenza
<g/>
)	)	kIx)	)
Vybíjená	vybíjená	k1gFnSc1	vybíjená
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
–	–	k?	–
osudy	osud	k1gInPc1	osud
několika	několik	k4yIc7	několik
spolužáků	spolužák	k1gMnPc2	spolužák
z	z	k7c2	z
gymnázia	gymnázium	k1gNnSc2	gymnázium
od	od	k7c2	od
jejich	jejich	k3xOp3gNnSc2	jejich
mládí	mládí	k1gNnSc2	mládí
až	až	k9	až
do	do	k7c2	do
čtyřicítky	čtyřicítka	k1gFnSc2	čtyřicítka
<g/>
;	;	kIx,	;
zfilmováno	zfilmovat	k5eAaPmNgNnS	zfilmovat
2015	[number]	k4	2015
(	(	kIx(	(
<g/>
Vybíjená	vybíjená	k1gFnSc1	vybíjená
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Petr	Petr	k1gMnSc1	Petr
Nikolaev	Nikolaev	k1gFnSc1	Nikolaev
<g/>
)	)	kIx)	)
Tři	tři	k4xCgFnPc1	tři
v	v	k7c6	v
háji	háj	k1gInSc6	háj
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
–	–	k?	–
společné	společný	k2eAgNnSc4d1	společné
dílo	dílo	k1gNnSc4	dílo
Haliny	Halina	k1gFnSc2	Halina
<g />
.	.	kIx.	.
</s>
<s>
Pawlowské	Pawlowská	k1gFnPc1	Pawlowská
<g/>
,	,	kIx,	,
Ivy	Iva	k1gFnPc1	Iva
Hercíkové	Hercíková	k1gFnSc2	Hercíková
a	a	k8xC	a
Michala	Michala	k1gFnSc1	Michala
Viewegha	Viewegha	k1gFnSc1	Viewegha
Román	román	k1gInSc4	román
pro	pro	k7c4	pro
muže	muž	k1gMnPc4	muž
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
-	-	kIx~	-
román	román	k1gInSc4	román
o	o	k7c6	o
ženském	ženský	k2eAgNnSc6d1	ženské
zklamání	zklamání	k1gNnSc6	zklamání
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
řízeného	řízený	k2eAgNnSc2d1	řízené
přestárlými	přestárlý	k2eAgMnPc7d1	přestárlý
kluky	kluk	k1gMnPc7	kluk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
bilancující	bilancující	k2eAgFnSc1d1	bilancující
<g/>
,	,	kIx,	,
mnohdy	mnohdy	k6eAd1	mnohdy
až	až	k9	až
sebeironickou	sebeironický	k2eAgFnSc7d1	sebeironická
reflexí	reflexe	k1gFnSc7	reflexe
úspěšného	úspěšný	k2eAgNnSc2d1	úspěšné
spisovatelství	spisovatelství	k1gNnSc2	spisovatelství
<g/>
;	;	kIx,	;
zfilmováno	zfilmovat	k5eAaPmNgNnS	zfilmovat
2010	[number]	k4	2010
(	(	kIx(	(
<g/>
Román	román	k1gInSc1	román
pro	pro	k7c4	pro
muže	muž	k1gMnPc4	muž
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
Tomáš	Tomáš	k1gMnSc1	Tomáš
Bařina	Bařina	k1gMnSc1	Bařina
<g/>
)	)	kIx)	)
Biomanželka	Biomanželka	k1gMnSc1	Biomanželka
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
Mafie	mafie	k1gFnSc1	mafie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
Mráz	mráz	k1gInSc1	mráz
přichází	přicházet	k5eAaImIp3nS	přicházet
z	z	k7c2	z
Hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
Čarodějka	čarodějka	k1gFnSc1	čarodějka
z	z	k7c2	z
Křemelky	křemelka	k1gFnSc2	křemelka
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
–	–	k?	–
chystaný	chystaný	k2eAgInSc4d1	chystaný
román	román	k1gInSc4	román
Názory	názor	k1gInPc1	názor
na	na	k7c4	na
vraždu	vražda	k1gFnSc4	vražda
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
–	–	k?	–
autorova	autorův	k2eAgFnSc1d1	autorova
knižní	knižní	k2eAgFnSc1d1	knižní
prvotina	prvotina	k1gFnSc1	prvotina
<g/>
,	,	kIx,	,
kniha	kniha	k1gFnSc1	kniha
s	s	k7c7	s
detektivní	detektivní	k2eAgFnSc7d1	detektivní
zápletkou	zápletka	k1gFnSc7	zápletka
Lekce	lekce	k1gFnSc2	lekce
tvůrčího	tvůrčí	k2eAgNnSc2d1	tvůrčí
psaní	psaní	k1gNnSc2	psaní
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
–	–	k?	–
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
knize	kniha	k1gFnSc6	kniha
zúročil	zúročit	k5eAaPmAgMnS	zúročit
zkušenosti	zkušenost	k1gFnPc4	zkušenost
z	z	k7c2	z
lekcí	lekce	k1gFnPc2	lekce
tvůrčího	tvůrčí	k2eAgNnSc2d1	tvůrčí
psaní	psaní	k1gNnSc2	psaní
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vyučuje	vyučovat	k5eAaImIp3nS	vyučovat
na	na	k7c4	na
Literární	literární	k2eAgFnSc4d1	literární
akademii	akademie	k1gFnSc4	akademie
Josefa	Josef	k1gMnSc2	Josef
Škvoreckého	Škvorecký	k2eAgNnSc2d1	Škvorecké
Andělé	anděl	k1gMnPc1	anděl
všedního	všední	k2eAgInSc2d1	všední
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
-	-	kIx~	-
novela	novela	k1gFnSc1	novela
se	s	k7c7	s
zachycenými	zachycený	k2eAgFnPc7d1	zachycená
zkušenostmi	zkušenost	k1gFnPc7	zkušenost
se	se	k3xPyFc4	se
smrtí	smrt	k1gFnSc7	smrt
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Druhé	druhý	k4xOgFnSc2	druhý
město	město	k1gNnSc1	město
Nápady	nápad	k1gInPc1	nápad
laskavého	laskavý	k2eAgMnSc2d1	laskavý
čtenáře	čtenář	k1gMnSc2	čtenář
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
soubor	soubor	k1gInSc1	soubor
parodií	parodie	k1gFnPc2	parodie
na	na	k7c4	na
známé	známý	k1gMnPc4	známý
české	český	k2eAgMnPc4d1	český
i	i	k8xC	i
světové	světový	k2eAgMnPc4d1	světový
autory	autor	k1gMnPc4	autor
Nové	Nové	k2eAgInPc1d1	Nové
nápady	nápad	k1gInPc1	nápad
laskavého	laskavý	k2eAgMnSc2d1	laskavý
čtenáře	čtenář	k1gMnSc2	čtenář
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
–	–	k?	–
druhý	druhý	k4xOgInSc4	druhý
díl	díl	k1gInSc4	díl
souboru	soubor	k1gInSc2	soubor
literárních	literární	k2eAgFnPc2d1	literární
parodií	parodie	k1gFnPc2	parodie
Povídky	povídka	k1gFnSc2	povídka
o	o	k7c6	o
manželství	manželství	k1gNnSc6	manželství
a	a	k8xC	a
o	o	k7c6	o
sexu	sex	k1gInSc6	sex
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
zfilmováno	zfilmován	k2eAgNnSc1d1	zfilmováno
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Nestyda	nestyda	k1gMnSc1	nestyda
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
Jan	Jan	k1gMnSc1	Jan
Hřebejk	Hřebejk	k1gMnSc1	Hřebejk
<g/>
)	)	kIx)	)
Krátké	Krátké	k2eAgFnSc2d1	Krátké
pohádky	pohádka	k1gFnSc2	pohádka
pro	pro	k7c4	pro
unavené	unavený	k2eAgNnSc4d1	unavené
<g />
.	.	kIx.	.
</s>
<s>
rodiče	rodič	k1gMnPc1	rodič
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
Bůh	bůh	k1gMnSc1	bůh
v	v	k7c6	v
renaultu	renault	k1gInSc6	renault
<g/>
,	,	kIx,	,
2017	[number]	k4	2017
Mafie	mafie	k1gFnSc1	mafie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
Audiotéka	Audiotéek	k1gInSc2	Audiotéek
Mráz	mráz	k1gInSc1	mráz
přichází	přicházet	k5eAaImIp3nS	přicházet
z	z	k7c2	z
Hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
Audiotéka	Audiotéek	k1gInSc2	Audiotéek
Zpátky	zpátky	k6eAd1	zpátky
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
Audiotéka	Audiotéka	k1gMnSc1	Audiotéka
Biomanželka	Biomanželka	k1gMnSc1	Biomanželka
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
Audiotéka	Audiotéek	k1gInSc2	Audiotéek
Biomanžel	Biomanžel	k1gFnSc2	Biomanžel
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
Audiotéka	Audiotéek	k1gInSc2	Audiotéek
Melouch	Melouch	k?	Melouch
<g/>
,	,	kIx,	,
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
Audiotéka	Audiotéek	k1gInSc2	Audiotéek
<g/>
,	,	kIx,	,
načetl	načíst	k5eAaBmAgMnS	načíst
Otakar	Otakar	k1gMnSc1	Otakar
Brousek	brousek	k1gInSc4	brousek
ml.	ml.	kA	ml.
Bůh	bůh	k1gMnSc1	bůh
v	v	k7c6	v
renaultu	renault	k1gInSc6	renault
<g/>
,	,	kIx,	,
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
Audiotéka	Audiotéek	k1gInSc2	Audiotéek
<g/>
,	,	kIx,	,
načetl	načíst	k5eAaPmAgMnS	načíst
Jiří	Jiří	k1gMnSc1	Jiří
Dvořák	Dvořák	k1gMnSc1	Dvořák
Švédské	švédský	k2eAgInPc4d1	švédský
stoly	stol	k1gInPc4	stol
aneb	aneb	k?	aneb
Jací	jaký	k3yQgMnPc1	jaký
jsme	být	k5eAaImIp1nP	být
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
–	–	k?	–
sbírka	sbírka	k1gFnSc1	sbírka
novinových	novinový	k2eAgInPc2d1	novinový
fejetonů	fejeton	k1gInPc2	fejeton
Na	na	k7c4	na
dvou	dva	k4xCgInPc2	dva
<g />
.	.	kIx.	.
</s>
<s>
židlích	židle	k1gFnPc6	židle
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
–	–	k?	–
sbírka	sbírka	k1gFnSc1	sbírka
z	z	k7c2	z
fejetonů	fejeton	k1gInPc2	fejeton
psaných	psaný	k2eAgInPc2d1	psaný
do	do	k7c2	do
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
v	v	k7c6	v
letech	let	k1gInPc6	let
2002	[number]	k4	2002
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
Báječný	báječný	k2eAgInSc4d1	báječný
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
–	–	k?	–
deník	deník	k1gInSc1	deník
autora	autor	k1gMnSc2	autor
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
nekomunikoval	komunikovat	k5eNaImAgInS	komunikovat
s	s	k7c7	s
novináři	novinář	k1gMnPc7	novinář
Další	další	k2eAgFnSc2d1	další
báječný	báječný	k2eAgInSc4d1	báječný
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
-	-	kIx~	-
deník	deník	k1gInSc1	deník
autora	autor	k1gMnSc2	autor
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2010	[number]	k4	2010
<g />
.	.	kIx.	.
</s>
<s>
Můj	můj	k3xOp1gInSc1	můj
život	život	k1gInSc1	život
po	po	k7c6	po
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
-	-	kIx~	-
autobiografické	autobiografický	k2eAgInPc1d1	autobiografický
zápisky	zápisek	k1gInPc1	zápisek
o	o	k7c4	o
rekonvalescenci	rekonvalescence	k1gFnSc4	rekonvalescence
po	po	k7c6	po
prodělané	prodělaný	k2eAgFnSc6d1	prodělaná
srdeční	srdeční	k2eAgFnSc6d1	srdeční
příhodě	příhoda	k1gFnSc6	příhoda
Růže	růž	k1gFnSc2	růž
pro	pro	k7c4	pro
Markétu	Markéta	k1gFnSc4	Markéta
aneb	aneb	k?	aneb
Večírky	večírek	k1gInPc7	večírek
revolucionářů	revolucionář	k1gMnPc2	revolucionář
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
-	-	kIx~	-
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
o	o	k7c6	o
Sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
Michal	Michal	k1gMnSc1	Michal
Viewegh	Viewegh	k1gMnSc1	Viewegh
bojuje	bojovat	k5eAaImIp3nS	bojovat
s	s	k7c7	s
bulvárním	bulvární	k2eAgInSc7d1	bulvární
tiskem	tisk	k1gInSc7	tisk
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
soudní	soudní	k2eAgFnSc7d1	soudní
cestou	cesta	k1gFnSc7	cesta
kvůli	kvůli	k7c3	kvůli
svému	svůj	k3xOyFgNnSc3	svůj
právu	právo	k1gNnSc3	právo
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
osobnosti	osobnost	k1gFnSc2	osobnost
a	a	k8xC	a
právu	práv	k2eAgFnSc4d1	práva
na	na	k7c4	na
přiměřené	přiměřený	k2eAgNnSc4d1	přiměřené
zadostiučinění	zadostiučinění	k1gNnSc4	zadostiučinění
za	za	k7c4	za
zveřejňování	zveřejňování	k1gNnSc4	zveřejňování
nepravdivých	pravdivý	k2eNgFnPc2d1	nepravdivá
a	a	k8xC	a
difamačních	difamační	k2eAgFnPc2d1	difamační
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
hercem	herec	k1gMnSc7	herec
Markem	Marek	k1gMnSc7	Marek
Vašutem	Vašut	k1gMnSc7	Vašut
iniciovali	iniciovat	k5eAaBmAgMnP	iniciovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
petici	petice	k1gFnSc4	petice
proti	proti	k7c3	proti
nevybíravým	vybíravý	k2eNgFnPc3d1	nevybíravá
praktikám	praktika	k1gFnPc3	praktika
českého	český	k2eAgInSc2d1	český
bulváru	bulvár	k1gInSc2	bulvár
–	–	k?	–
Vzkaz	vzkaz	k1gInSc1	vzkaz
bulváru	bulvár	k1gInSc2	bulvár
a	a	k8xC	a
výzva	výzva	k1gFnSc1	výzva
kolegům	kolega	k1gMnPc3	kolega
<g/>
.	.	kIx.	.
</s>
<s>
Připojila	připojit	k5eAaPmAgFnS	připojit
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
řada	řada	k1gFnSc1	řada
významných	významný	k2eAgMnPc2d1	významný
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
uznal	uznat	k5eAaPmAgInS	uznat
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
ČR	ČR	kA	ČR
jeho	jeho	k3xOp3gFnSc4	jeho
stížnost	stížnost	k1gFnSc4	stížnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
odškodnění	odškodnění	k1gNnSc1	odškodnění
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
200	[number]	k4	200
tis	tis	k1gInSc4	tis
<g/>
.	.	kIx.	.
korun	koruna	k1gFnPc2	koruna
není	být	k5eNaImIp3nS	být
dostatečné	dostatečný	k2eAgNnSc1d1	dostatečné
za	za	k7c4	za
porušení	porušení	k1gNnSc4	porušení
jeho	on	k3xPp3gNnSc2	on
práva	právo	k1gNnSc2	právo
na	na	k7c4	na
respekt	respekt	k1gInSc4	respekt
k	k	k7c3	k
lidské	lidský	k2eAgFnSc3d1	lidská
důstojnosti	důstojnost	k1gFnSc3	důstojnost
a	a	k8xC	a
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
respekt	respekt	k1gInSc4	respekt
k	k	k7c3	k
soukromému	soukromý	k2eAgInSc3d1	soukromý
životu	život	k1gInSc3	život
<g/>
.	.	kIx.	.
</s>
<s>
Žaloval	žalovat	k5eAaImAgInS	žalovat
deník	deník	k1gInSc1	deník
Aha	aha	k1gNnSc2	aha
<g/>
!	!	kIx.	!
</s>
<s>
za	za	k7c4	za
článek	článek	k1gInSc4	článek
"	"	kIx"	"
<g/>
Má	mít	k5eAaImIp3nS	mít
tajnou	tajný	k2eAgFnSc4d1	tajná
milenku	milenka	k1gFnSc4	milenka
<g/>
"	"	kIx"	"
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
takové	takový	k3xDgFnPc1	takový
publikované	publikovaný	k2eAgFnPc1d1	publikovaná
dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
