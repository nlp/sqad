<p>
<s>
Šedesát	šedesát	k4xCc1	šedesát
jedna	jeden	k4xCgFnSc1	jeden
je	být	k5eAaImIp3nS	být
přirozené	přirozený	k2eAgNnSc4d1	přirozené
číslo	číslo	k1gNnSc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
po	po	k7c6	po
číslu	číslo	k1gNnSc3	číslo
šedesát	šedesát	k4xCc1	šedesát
a	a	k8xC	a
předchází	předcházet	k5eAaImIp3nS	předcházet
číslu	číslo	k1gNnSc3	číslo
šedesát	šedesát	k4xCc1	šedesát
dva	dva	k4xCgInPc4	dva
<g/>
.	.	kIx.	.
</s>
<s>
Řadová	řadový	k2eAgFnSc1d1	řadová
číslovka	číslovka	k1gFnSc1	číslovka
je	být	k5eAaImIp3nS	být
šedesátý	šedesátý	k4xOgInSc1	šedesátý
první	první	k4xOgMnSc1	první
nebo	nebo	k8xC	nebo
jednašedesátý	jednašedesátý	k4xOgMnSc1	jednašedesátý
<g/>
.	.	kIx.	.
</s>
<s>
Římskými	římský	k2eAgFnPc7d1	římská
číslicemi	číslice	k1gFnPc7	číslice
se	se	k3xPyFc4	se
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
LXI	LXI	kA	LXI
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Matematika	matematika	k1gFnSc1	matematika
==	==	k?	==
</s>
</p>
<p>
<s>
osmé	osmý	k4xOgNnSc1	osmý
Pentanacciho	Pentanacci	k1gMnSc2	Pentanacci
číslo	číslo	k1gNnSc1	číslo
</s>
</p>
<p>
<s>
pátý	pátý	k4xOgInSc1	pátý
repfigit	repfigit	k1gInSc1	repfigit
</s>
</p>
<p>
<s>
osmnácté	osmnáctý	k4xOgNnSc4	osmnáctý
prvočíslo	prvočíslo	k1gNnSc4	prvočíslo
a	a	k8xC	a
nejmenší	malý	k2eAgNnSc4d3	nejmenší
prvočíslo	prvočíslo	k1gNnSc4	prvočíslo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
napsané	napsaný	k2eAgNnSc1d1	napsané
"	"	kIx"	"
<g/>
pozpátku	pozpátku	k6eAd1	pozpátku
<g/>
"	"	kIx"	"
dává	dávat	k5eAaImIp3nS	dávat
druhou	druhý	k4xOgFnSc4	druhý
mocninu	mocnina	k1gFnSc4	mocnina
celého	celý	k2eAgNnSc2d1	celé
čísla	číslo	k1gNnSc2	číslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chemie	chemie	k1gFnSc1	chemie
==	==	k?	==
</s>
</p>
<p>
<s>
61	[number]	k4	61
je	být	k5eAaImIp3nS	být
atomové	atomový	k2eAgNnSc1d1	atomové
číslo	číslo	k1gNnSc1	číslo
promethia	promethia	k1gFnSc1	promethia
<g/>
,	,	kIx,	,
neutronové	neutronový	k2eAgNnSc1d1	neutronové
číslo	číslo	k1gNnSc1	číslo
nejběžnějšího	běžný	k2eAgInSc2d3	nejběžnější
izotopu	izotop	k1gInSc2	izotop
stříbra	stříbro	k1gNnSc2	stříbro
</s>
</p>
<p>
<s>
==	==	k?	==
Astronomie	astronomie	k1gFnSc1	astronomie
==	==	k?	==
</s>
</p>
<p>
<s>
61	[number]	k4	61
Cygni	Cygň	k1gMnPc7	Cygň
je	být	k5eAaImIp3nS	být
hvězda	hvězda	k1gFnSc1	hvězda
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Labutě	labuť	k1gFnSc2	labuť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kosmonautika	kosmonautika	k1gFnSc1	kosmonautika
==	==	k?	==
</s>
</p>
<p>
<s>
STS-61	STS-61	k4	STS-61
byla	být	k5eAaImAgFnS	být
mise	mise	k1gFnSc1	mise
amerického	americký	k2eAgInSc2d1	americký
raketoplánu	raketoplán	k1gInSc2	raketoplán
Endeavour	Endeavour	k1gInSc1	Endeavour
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
probíhala	probíhat	k5eAaImAgFnS	probíhat
od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
do	do	k7c2	do
13	[number]	k4	13
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
mise	mise	k1gFnSc2	mise
bylo	být	k5eAaImAgNnS	být
opravit	opravit	k5eAaPmF	opravit
Hubbleův	Hubbleův	k2eAgInSc4d1	Hubbleův
vesmírný	vesmírný	k2eAgInSc4d1	vesmírný
dalekohled	dalekohled	k1gInSc4	dalekohled
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Roky	rok	k1gInPc1	rok
==	==	k?	==
</s>
</p>
<p>
<s>
61	[number]	k4	61
</s>
</p>
<p>
<s>
61	[number]	k4	61
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
</s>
</p>
<p>
<s>
1961	[number]	k4	1961
</s>
</p>
