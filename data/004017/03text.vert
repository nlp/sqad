<s>
2000	[number]	k4	2000
(	(	kIx(	(
<g/>
MM	mm	kA	mm
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
dle	dle	k7c2	dle
gregoriánského	gregoriánský	k2eAgInSc2d1	gregoriánský
kalendáře	kalendář	k1gInSc2	kalendář
započal	započnout	k5eAaPmAgMnS	započnout
sobotou	sobota	k1gFnSc7	sobota
<g/>
.	.	kIx.	.
</s>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
–	–	k?	–
Začalo	začít	k5eAaPmAgNnS	začít
fungovat	fungovat	k5eAaImF	fungovat
14	[number]	k4	14
nových	nový	k2eAgInPc2d1	nový
samosprávných	samosprávný	k2eAgInPc2d1	samosprávný
krajů	kraj	k1gInPc2	kraj
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
–	–	k?	–
Komerční	komerční	k2eAgNnSc1d1	komerční
spuštění	spuštění	k1gNnSc1	spuštění
třetího	třetí	k4xOgMnSc2	třetí
mobilního	mobilní	k2eAgMnSc2d1	mobilní
operátora	operátor	k1gMnSc2	operátor
Oskar	Oskar	k1gMnSc1	Oskar
od	od	k7c2	od
<g />
.	.	kIx.	.
</s>
<s>
Českého	český	k2eAgInSc2d1	český
Mobilu	mobil	k1gInSc2	mobil
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Vodafone	Vodafon	k1gInSc5	Vodafon
Czech	Czecha	k1gFnPc2	Czecha
Republic	Republice	k1gFnPc2	Republice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
–	–	k?	–
Založena	založen	k2eAgFnSc1d1	založena
akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
Škoda	Škoda	k1gMnSc1	Škoda
Holding	holding	k1gInSc1	holding
<g/>
.	.	kIx.	.
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
tzv.	tzv.	kA	tzv.
Aféra	aféra	k1gFnSc1	aféra
Olovo	olovo	k1gNnSc4	olovo
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
–	–	k?	–
čeští	český	k2eAgMnPc1d1	český
hokejisté	hokejista	k1gMnPc1	hokejista
získali	získat	k5eAaPmAgMnP	získat
na	na	k7c4	na
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
zlaté	zlatý	k2eAgFnSc2d1	zlatá
medaile	medaile	k1gFnSc2	medaile
27	[number]	k4	27
<g/>
<g />
.	.	kIx.	.
</s>
<s>
května	květen	k1gInSc2	květen
–	–	k?	–
Spadla	spadnout	k5eAaPmAgFnS	spadnout
při	při	k7c6	při
vichřici	vichřice	k1gFnSc6	vichřice
památná	památný	k2eAgFnSc1d1	památná
Semtinská	Semtinský	k2eAgFnSc1d1	Semtinská
lípa	lípa	k1gFnSc1	lípa
<g/>
.	.	kIx.	.
v	v	k7c6	v
září	září	k1gNnSc6	září
–	–	k?	–
Kongres	kongres	k1gInSc4	kongres
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
měnového	měnový	k2eAgInSc2d1	měnový
fondu	fond	k1gInSc2	fond
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
-	-	kIx~	-
Protiglobalizační	Protiglobalizační	k2eAgFnPc1d1	Protiglobalizační
demonstrace	demonstrace	k1gFnPc1	demonstrace
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
–	–	k?	–
Krajské	krajský	k2eAgFnSc2d1	krajská
a	a	k8xC	a
senátní	senátní	k2eAgFnSc2d1	senátní
volby	volba	k1gFnSc2	volba
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
–	–	k?	–
Vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
krize	krize	k1gFnSc1	krize
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
televizi	televize	k1gFnSc6	televize
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
–	–	k?	–
První	první	k4xOgInSc1	první
blok	blok	k1gInSc1	blok
Jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
Temelín	Temelín	k1gInSc1	Temelín
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
připojen	připojit	k5eAaPmNgInS	připojit
do	do	k7c2	do
rozvodné	rozvodný	k2eAgFnSc2d1	rozvodná
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Dokončení	dokončení	k1gNnSc1	dokončení
přestavby	přestavba	k1gFnSc2	přestavba
pražského	pražský	k2eAgInSc2d1	pražský
Smíchova	Smíchov	k1gInSc2	Smíchov
<g/>
.	.	kIx.	.
společnost	společnost	k1gFnSc1	společnost
AHOLD	AHOLD	kA	AHOLD
Czech	Czecha	k1gFnPc2	Czecha
Republic	Republice	k1gFnPc2	Republice
<g/>
,	,	kIx,	,
a.s	a.s	k?	a.s
začala	začít	k5eAaPmAgFnS	začít
provozovat	provozovat	k5eAaImF	provozovat
supermarkety	supermarket	k1gInPc4	supermarket
Albert	Alberta	k1gFnPc2	Alberta
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Mana	Man	k1gMnSc2	Man
<g/>
)	)	kIx)	)
3	[number]	k4	3
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
–	–	k?	–
National	National	k1gMnSc1	National
Geographic	Geographic	k1gMnSc1	Geographic
na	na	k7c6	na
tiskové	tiskový	k2eAgFnSc6d1	tisková
konferenci	konference	k1gFnSc6	konference
připustil	připustit	k5eAaPmAgMnS	připustit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jím	jíst	k5eAaImIp1nS	jíst
oznámený	oznámený	k2eAgInSc1d1	oznámený
objev	objev	k1gInSc1	objev
zkameněliny	zkamenělina	k1gFnSc2	zkamenělina
opeřeného	opeřený	k2eAgMnSc2d1	opeřený
dinosaura	dinosaurus	k1gMnSc2	dinosaurus
rodu	rod	k1gInSc2	rod
Archaeoraptor	Archaeoraptor	k1gMnSc1	Archaeoraptor
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
<g />
.	.	kIx.	.
</s>
<s>
chybějícím	chybějící	k2eAgInSc7d1	chybějící
článkem	článek	k1gInSc7	článek
mezi	mezi	k7c4	mezi
dinosaury	dinosaurus	k1gMnPc4	dinosaurus
a	a	k8xC	a
ptáky	pták	k1gMnPc4	pták
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
padělkem	padělek	k1gInSc7	padělek
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
–	–	k?	–
Rusko	Rusko	k1gNnSc1	Rusko
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
poslední	poslední	k2eAgNnSc1d1	poslední
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
dosud	dosud	k6eAd1	dosud
zůstávalo	zůstávat	k5eAaImAgNnS	zůstávat
v	v	k7c6	v
čečenských	čečenský	k2eAgFnPc6d1	čečenská
rukou	ruka	k1gFnPc6	ruka
–	–	k?	–
Šatoj	Šatoj	k1gInSc1	Šatoj
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
–	–	k?	–
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
do	do	k7c2	do
EUROCONTROLu	EUROCONTROLus	k1gInSc2	EUROCONTROLus
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
Vydána	vydán	k2eAgFnSc1d1	vydána
hra	hra	k1gFnSc1	hra
Hogs	Hogsa	k1gFnPc2	Hogsa
of	of	k?	of
War	War	k1gMnSc1	War
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
16	[number]	k4	16
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
–	–	k?	–
Proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
zatmění	zatmění	k1gNnSc1	zatmění
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
–	–	k?	–
Pád	Pád	k1gInSc1	Pád
letadla	letadlo	k1gNnSc2	letadlo
Concorde	Concord	k1gInSc5	Concord
společnosti	společnost	k1gFnSc2	společnost
Air	Air	k1gMnSc2	Air
France	Franc	k1gMnSc2	Franc
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
113	[number]	k4	113
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
109	[number]	k4	109
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
letadla	letadlo	k1gNnSc2	letadlo
<g/>
,	,	kIx,	,
4	[number]	k4	4
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
)	)	kIx)	)
15	[number]	k4	15
<g />
.	.	kIx.	.
</s>
<s>
<g/>
–	–	k?	–
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
–	–	k?	–
Proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
Světové	světový	k2eAgFnPc1d1	světová
dny	dna	k1gFnPc1	dna
mládeže	mládež	k1gFnSc2	mládež
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
–	–	k?	–
Vydána	vydán	k2eAgFnSc1d1	vydána
hra	hra	k1gFnSc1	hra
Hogs	Hogsa	k1gFnPc2	Hogsa
of	of	k?	of
War	War	k1gMnSc1	War
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
Kvůli	kvůli	k7c3	kvůli
zfalšování	zfalšování	k1gNnSc3	zfalšování
výsledků	výsledek	k1gInPc2	výsledek
prezidentských	prezidentský	k2eAgFnPc2d1	prezidentská
voleb	volba	k1gFnPc2	volba
vypukly	vypuknout	k5eAaPmAgInP	vypuknout
v	v	k7c6	v
Bělehradě	Bělehrad	k1gInSc6	Bělehrad
demonstrace	demonstrace	k1gFnPc1	demonstrace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
rezignaci	rezignace	k1gFnSc3	rezignace
prezidenta	prezident	k1gMnSc2	prezident
<g />
.	.	kIx.	.
</s>
<s>
Slobodana	Slobodan	k1gMnSc2	Slobodan
Miloševiče	Miloševič	k1gMnSc2	Miloševič
12	[number]	k4	12
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
V	v	k7c6	v
Jemenu	Jemen	k1gInSc6	Jemen
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
útoku	útok	k1gInSc3	útok
na	na	k7c4	na
americký	americký	k2eAgInSc4d1	americký
torpédoborec	torpédoborec	k1gInSc4	torpédoborec
USS	USS	kA	USS
Cole	cola	k1gFnSc6	cola
<g/>
,	,	kIx,	,
z	z	k7c2	z
jehož	jehož	k3xOyRp3gFnSc2	jehož
přípravy	příprava	k1gFnSc2	příprava
je	být	k5eAaImIp3nS	být
podezřelá	podezřelý	k2eAgFnSc1d1	podezřelá
al-Kajdá	al-Kajdý	k2eAgFnSc1d1	al-Kajdá
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
–	–	k?	–
Vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
první	první	k4xOgFnSc1	první
stálá	stálý	k2eAgFnSc1d1	stálá
posádka	posádka	k1gFnSc1	posádka
na	na	k7c4	na
Mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
vesmírnou	vesmírný	k2eAgFnSc4d1	vesmírná
stanici	stanice	k1gFnSc4	stanice
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
–	–	k?	–
Irák	Irák	k1gInSc1	Irák
začíná	začínat	k5eAaImIp3nS	začínat
s	s	k7c7	s
ropou	ropa	k1gFnSc7	ropa
obchodovat	obchodovat	k5eAaImF	obchodovat
v	v	k7c4	v
Euro	euro	k1gNnSc4	euro
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
–	–	k?	–
Prezidentské	prezidentský	k2eAgFnPc1d1	prezidentská
volby	volba	k1gFnPc1	volba
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
–	–	k?	–
v	v	k7c6	v
rakouském	rakouský	k2eAgInSc6d1	rakouský
Kaprunu	Kaprun	k1gInSc6	Kaprun
155	[number]	k4	155
lidí	člověk	k1gMnPc2	člověk
uhořelo	uhořet	k5eAaPmAgNnS	uhořet
v	v	k7c6	v
lanovce	lanovka	k1gFnSc6	lanovka
na	na	k7c4	na
ledovec	ledovec	k1gInSc4	ledovec
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
–	–	k?	–
Je	být	k5eAaImIp3nS	být
pořádán	pořádán	k2eAgInSc1d1	pořádán
již	již	k9	již
50	[number]	k4	50
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
soutěže	soutěž	k1gFnSc2	soutěž
krásy	krása	k1gFnSc2	krása
Miss	miss	k1gFnSc2	miss
World	Worlda	k1gFnPc2	Worlda
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
vyhrává	vyhrávat	k5eAaImIp3nS	vyhrávat
Miss	miss	k1gFnSc4	miss
Indie	Indie	k1gFnSc2	Indie
Priyanka	Priyanka	k1gFnSc1	Priyanka
Chopra	Chopra	k1gFnSc1	Chopra
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
–	–	k?	–
Státy	stát	k1gInPc1	stát
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
byla	být	k5eAaImAgFnS	být
podepsána	podepsán	k2eAgFnSc1d1	podepsána
smlouva	smlouva	k1gFnSc1	smlouva
z	z	k7c2	z
Nice	Nice	k1gFnSc2	Nice
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
–	–	k?	–
Český	český	k2eAgMnSc1d1	český
premiér	premiér	k1gMnSc1	premiér
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
a	a	k8xC	a
rakouský	rakouský	k2eAgMnSc1d1	rakouský
kancléř	kancléř	k1gMnSc1	kancléř
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Schüssel	Schüssel	k1gMnSc1	Schüssel
podepsali	podepsat	k5eAaPmAgMnP	podepsat
Protokol	protokol	k1gInSc4	protokol
z	z	k7c2	z
Melku	Melk	k1gInSc2	Melk
o	o	k7c6	o
jaderné	jaderný	k2eAgFnSc6d1	jaderná
elektrárně	elektrárna	k1gFnSc6	elektrárna
Temelín	Temelín	k1gInSc1	Temelín
<g/>
.	.	kIx.	.
</s>
<s>
Sat	Sat	k?	Sat
<g/>
.1	.1	k4	.1
se	se	k3xPyFc4	se
spojila	spojit	k5eAaPmAgNnP	spojit
s	s	k7c7	s
Pro	pro	k7c4	pro
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Dzurinda	Dzurind	k1gMnSc4	Dzurind
založil	založit	k5eAaPmAgMnS	založit
novou	nový	k2eAgFnSc4d1	nová
politickou	politický	k2eAgFnSc4d1	politická
stranu	strana	k1gFnSc4	strana
–	–	k?	–
Slovenskou	slovenský	k2eAgFnSc4d1	slovenská
demokratickou	demokratický	k2eAgFnSc4d1	demokratická
a	a	k8xC	a
křesťanskou	křesťanský	k2eAgFnSc4d1	křesťanská
unii	unie	k1gFnSc4	unie
<g/>
.	.	kIx.	.
</s>
<s>
Bill	Bill	k1gMnSc1	Bill
Gates	Gates	k1gMnSc1	Gates
odešel	odejít	k5eAaPmAgMnS	odejít
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
generálního	generální	k2eAgMnSc2d1	generální
ředitele	ředitel	k1gMnSc2	ředitel
společnosti	společnost	k1gFnSc2	společnost
Microsoft	Microsoft	kA	Microsoft
<g/>
.	.	kIx.	.
</s>
<s>
Slovensko	Slovensko	k1gNnSc1	Slovensko
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
do	do	k7c2	do
Organizace	organizace	k1gFnSc2	organizace
pro	pro	k7c4	pro
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
spolupráci	spolupráce	k1gFnSc4	spolupráce
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Africká	africký	k2eAgFnSc1d1	africká
unie	unie	k1gFnSc1	unie
přijala	přijmout	k5eAaPmAgFnS	přijmout
ustavující	ustavující	k2eAgFnSc4d1	ustavující
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Abdiqasim	Abdiqasim	k6eAd1	Abdiqasim
Salad	Salad	k1gInSc1	Salad
Hassan	Hassana	k1gFnPc2	Hassana
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prezidentem	prezident	k1gMnSc7	prezident
přechodné	přechodný	k2eAgFnSc2d1	přechodná
národní	národní	k2eAgFnSc2d1	národní
vlády	vláda	k1gFnSc2	vláda
Somálska	Somálsko	k1gNnSc2	Somálsko
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
–	–	k?	–
Založena	založen	k2eAgFnSc1d1	založena
Univerzita	univerzita	k1gFnSc1	univerzita
Tomáše	Tomáš	k1gMnSc2	Tomáš
Bati	Baťa	k1gMnSc2	Baťa
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
–	–	k?	–
Byl	být	k5eAaImAgInS	být
oznámen	oznámen	k2eAgInSc1d1	oznámen
objev	objev	k1gInSc1	objev
pátého	pátý	k4xOgNnSc2	pátý
skupenství	skupenství	k1gNnSc2	skupenství
<g/>
.	.	kIx.	.
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
–	–	k?	–
Vyšel	vyjít	k5eAaPmAgInS	vyjít
internetový	internetový	k2eAgInSc1d1	internetový
básnický	básnický	k2eAgInSc1d1	básnický
almanach	almanach	k1gInSc1	almanach
Adieu	adieu	k0	adieu
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
duben	duben	k1gInSc1	duben
–	–	k?	–
činnost	činnost	k1gFnSc1	činnost
skupiny	skupina	k1gFnSc2	skupina
NARVAN	NARVAN	kA	NARVAN
ukončila	ukončit	k5eAaPmAgFnS	ukončit
tragická	tragický	k2eAgFnSc1d1	tragická
dopravní	dopravní	k2eAgFnSc1d1	dopravní
nehoda	nehoda	k1gFnSc1	nehoda
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
zahynuli	zahynout	k5eAaPmAgMnP	zahynout
zpěvák	zpěvák	k1gMnSc1	zpěvák
Mario	Mario	k1gMnSc1	Mario
Feinberg	Feinberg	k1gMnSc1	Feinberg
a	a	k8xC	a
kytarista	kytarista	k1gMnSc1	kytarista
Martin	Martin	k1gMnSc1	Martin
Pokora	pokora	k1gFnSc1	pokora
<g/>
.	.	kIx.	.
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
–	–	k?	–
Viděno	viděn	k2eAgNnSc4d1	viděno
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
seřadilo	seřadit	k5eAaPmAgNnS	seřadit
5	[number]	k4	5
nejjasnějších	jasný	k2eAgFnPc2d3	nejjasnější
planet	planeta	k1gFnPc2	planeta
uvnitř	uvnitř	k7c2	uvnitř
úhlu	úhel	k1gInSc2	úhel
20	[number]	k4	20
<g/>
°	°	k?	°
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
přílišnou	přílišný	k2eAgFnSc4d1	přílišná
blízkost	blízkost	k1gFnSc4	blízkost
Slunci	slunce	k1gNnSc3	slunce
nebylo	být	k5eNaImAgNnS	být
možno	možno	k9	možno
úkaz	úkaz	k1gInSc4	úkaz
pozorovat	pozorovat	k5eAaImF	pozorovat
<g/>
.	.	kIx.	.
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
–	–	k?	–
Firma	firma	k1gFnSc1	firma
Sun	sun	k1gInSc1	sun
Microsystems	Microsystems	k1gInSc1	Microsystems
založila	založit	k5eAaPmAgFnS	založit
Open	Open	k1gNnSc4	Open
Source	Sourec	k1gInSc2	Sourec
projekt	projekt	k1gInSc1	projekt
NetBeans	NetBeans	k1gInSc1	NetBeans
<g/>
.	.	kIx.	.
</s>
<s>
Starobylé	starobylý	k2eAgFnPc4d1	starobylá
vesnice	vesnice	k1gFnPc4	vesnice
Si-ti	Sii	k1gNnSc2	Si-ti
a	a	k8xC	a
Chung-cchun	Chungchuna	k1gFnPc2	Chung-cchuna
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
zapsány	zapsat	k5eAaPmNgInP	zapsat
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
An-chuej	Anhuej	k1gInSc4	An-chuej
<g/>
.	.	kIx.	.
</s>
<s>
Intel	Intel	kA	Intel
představil	představit	k5eAaPmAgInS	představit
Pentium	Pentium	kA	Pentium
4	[number]	k4	4
–	–	k?	–
42	[number]	k4	42
milionů	milion	k4xCgInPc2	milion
tranzistorů	tranzistor	k1gInPc2	tranzistor
a	a	k8xC	a
frekvence	frekvence	k1gFnSc1	frekvence
1,5	[number]	k4	1,5
gigahertzu	gigahertz	k1gInSc2	gigahertz
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
evropských	evropský	k2eAgNnPc2d1	Evropské
měst	město	k1gNnPc2	město
kultury	kultura	k1gFnSc2	kultura
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Objevena	objeven	k2eAgFnSc1d1	objevena
pátá	pátá	k1gFnSc1	pátá
lidská	lidský	k2eAgFnSc1d1	lidská
chuť	chuť	k1gFnSc1	chuť
umami	uma	k1gFnPc7	uma
<g/>
.	.	kIx.	.
</s>
<s>
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
–	–	k?	–
Žores	Žores	k1gInSc1	Žores
Ivanovič	Ivanovič	k1gMnSc1	Ivanovič
Alfjorov	Alfjorov	k1gInSc1	Alfjorov
<g/>
,	,	kIx,	,
Herbert	Herbert	k1gMnSc1	Herbert
Kroemer	Kroemer	k1gMnSc1	Kroemer
<g/>
,	,	kIx,	,
Jack	Jack	k1gMnSc1	Jack
Kilby	Kilba	k1gFnSc2	Kilba
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
–	–	k?	–
Alan	Alan	k1gMnSc1	Alan
J.	J.	kA	J.
Heeger	Heeger	k1gMnSc1	Heeger
<g/>
,	,	kIx,	,
Alan	Alan	k1gMnSc1	Alan
G	G	kA	G
MacDiarmid	MacDiarmid	k1gInSc1	MacDiarmid
<g/>
,	,	kIx,	,
Hideki	Hideki	k1gNnSc4	Hideki
Širakawa	Širakaw	k1gInSc2	Širakaw
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
a	a	k8xC	a
medicínu	medicína	k1gFnSc4	medicína
–	–	k?	–
Arvid	Arvida	k1gFnPc2	Arvida
Carlsson	Carlsson	k1gMnSc1	Carlsson
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Greengard	Greengard	k1gMnSc1	Greengard
<g/>
,	,	kIx,	,
Eric	Eric	k1gFnSc1	Eric
R.	R.	kA	R.
Kandel	kandela	k1gFnPc2	kandela
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
–	–	k?	–
Kao	Kao	k1gFnSc1	Kao
Sing-ťien	Sing-ťina	k1gFnPc2	Sing-ťina
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
míru	mír	k1gInSc2	mír
–	–	k?	–
Kim	Kim	k1gFnSc2	Kim
Te-džung	Težung	k1gInSc4	Te-džung
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
ekonomii	ekonomie	k1gFnSc4	ekonomie
–	–	k?	–
James	James	k1gMnSc1	James
Heckman	Heckman	k1gMnSc1	Heckman
<g/>
,	,	kIx,	,
Daniel	Daniel	k1gMnSc1	Daniel
McFadden	McFaddna	k1gFnPc2	McFaddna
Související	související	k2eAgFnSc2d1	související
informace	informace	k1gFnSc2	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
2000	[number]	k4	2000
ve	v	k7c6	v
sportu	sport	k1gInSc6	sport
<g/>
.	.	kIx.	.
</s>
<s>
Česko	Česko	k1gNnSc1	Česko
29	[number]	k4	29
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
–	–	k?	–
Dalibor	Dalibor	k1gMnSc1	Dalibor
Slepčík	Slepčík	k1gMnSc1	Slepčík
<g/>
,	,	kIx,	,
finalista	finalista	k1gMnSc1	finalista
Česko	Česko	k1gNnSc4	Česko
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
Superstar	superstar	k1gFnSc2	superstar
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
30	[number]	k4	30
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
–	–	k?	–
Teri	Ter	k1gFnSc2	Ter
Blitzen	Blitzna	k1gFnPc2	Blitzna
(	(	kIx(	(
<g/>
Tereza	Tereza	k1gFnSc1	Tereza
Hodanová	Hodanová	k1gFnSc1	Hodanová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
youtuberka	youtuberka	k1gFnSc1	youtuberka
<g/>
,	,	kIx,	,
vítězka	vítězka	k1gFnSc1	vítězka
ankety	anketa	k1gFnSc2	anketa
Blogerka	Blogerka	k1gFnSc1	Blogerka
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
19	[number]	k4	19
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
-	-	kIx~	-
Jakub	Jakub	k1gMnSc1	Jakub
<g />
.	.	kIx.	.
</s>
<s>
Marek	Marek	k1gMnSc1	Marek
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
student	student	k1gMnSc1	student
a	a	k8xC	a
skvělý	skvělý	k2eAgMnSc1d1	skvělý
hráč	hráč	k1gMnSc1	hráč
na	na	k7c4	na
violoncello	violoncello	k1gNnSc4	violoncello
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
David	David	k1gMnSc1	David
Strnad	Strnad	k1gMnSc1	Strnad
<g/>
,	,	kIx,	,
mladý	mladý	k2eAgMnSc1d1	mladý
spisovatel	spisovatel	k1gMnSc1	spisovatel
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
-	-	kIx~	-
Štěpán	Štěpán	k1gMnSc1	Štěpán
Růta	Růta	k1gMnSc1	Růta
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
hokejista	hokejista	k1gMnSc1	hokejista
Svět	svět	k1gInSc4	svět
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
–	–	k?	–
Noah	Noah	k1gMnSc1	Noah
Cyrus	Cyrus	k1gMnSc1	Cyrus
<g/>
,	,	kIx,	,
sestra	sestra	k1gFnSc1	sestra
známé	známý	k2eAgFnSc2d1	známá
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
Miley	Milea	k1gFnSc2	Milea
Cyrus	Cyrus	k1gMnSc1	Cyrus
<g/>
,	,	kIx,	,
<g/>
<g />
.	.	kIx.	.
</s>
<s>
dětská	dětský	k2eAgFnSc1d1	dětská
herečka	herečka	k1gFnSc1	herečka
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
–	–	k?	–
Sophie	Sophie	k1gFnSc1	Sophie
Nélisse	Nélisse	k1gFnSc1	Nélisse
<g/>
,	,	kIx,	,
kanadská	kanadský	k2eAgFnSc1d1	kanadská
herečka	herečka	k1gFnSc1	herečka
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
–	–	k?	–
Willow	Willow	k1gFnSc2	Willow
Shields	Shieldsa	k1gFnPc2	Shieldsa
<g/>
,	,	kIx,	,
americká	americký	k2eAgFnSc1d1	americká
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
–	–	k?	–
Nico	Nico	k1gMnSc1	Nico
Liersch	Liersch	k1gMnSc1	Liersch
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
televizní	televizní	k2eAgMnSc1d1	televizní
a	a	k8xC	a
filmový	filmový	k2eAgMnSc1d1	filmový
herec	herec	k1gMnSc1	herec
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
–	–	k?	–
Vincenzo	Vincenza	k1gFnSc5	Vincenza
Cantiello	Cantiello	k1gNnSc1	Cantiello
<g/>
,	,	kIx,	,
italský	italský	k2eAgInSc1d1	italský
<g />
.	.	kIx.	.
</s>
<s>
dětský	dětský	k2eAgMnSc1d1	dětský
zpěvák	zpěvák	k1gMnSc1	zpěvák
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
Willow	Willow	k1gMnSc1	Willow
Smith	Smith	k1gMnSc1	Smith
<g/>
,	,	kIx,	,
americká	americký	k2eAgFnSc1d1	americká
dětská	dětský	k2eAgFnSc1d1	dětská
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
Willa	Willa	k1gFnSc1	Willa
Smithe	Smithe	k1gFnSc1	Smithe
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
–	–	k?	–
Jasmine	Jasmin	k1gInSc5	Jasmin
Thompson	Thompson	k1gMnSc1	Thompson
<g/>
,	,	kIx,	,
britská	britský	k2eAgFnSc1d1	britská
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
a	a	k8xC	a
skladatelka	skladatelka	k1gFnSc1	skladatelka
10	[number]	k4	10
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
–	–	k?	–
Mackenzie	Mackenzie	k1gFnSc1	Mackenzie
Foy	Foy	k1gFnSc1	Foy
<g/>
,	,	kIx,	,
americká	americký	k2eAgFnSc1d1	americká
dětská	dětský	k2eAgFnSc1d1	dětská
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
modelka	modelka	k1gFnSc1	modelka
Automatický	automatický	k2eAgInSc4d1	automatický
abecedně	abecedně	k6eAd1	abecedně
<g />
.	.	kIx.	.
</s>
<s>
řazený	řazený	k2eAgInSc1d1	řazený
seznam	seznam	k1gInSc1	seznam
existujících	existující	k2eAgFnPc2d1	existující
biografií	biografie	k1gFnPc2	biografie
viz	vidět	k5eAaImRp2nS	vidět
Kategorie	kategorie	k1gFnSc1	kategorie
<g/>
:	:	kIx,	:
<g/>
Úmrtí	úmrtí	k1gNnSc1	úmrtí
2000	[number]	k4	2000
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
–	–	k?	–
Heda	Heda	k1gFnSc1	Heda
Halířová	halířový	k2eAgFnSc1d1	Halířová
<g/>
,	,	kIx,	,
autorka	autorka	k1gFnSc1	autorka
knih	kniha	k1gFnPc2	kniha
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
(	(	kIx(	(
<g/>
*	*	kIx~	*
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
–	–	k?	–
Josef	Josef	k1gMnSc1	Josef
Šimek	Šimek	k1gMnSc1	Šimek
<g/>
,	,	kIx,	,
kreslíř	kreslíř	k1gMnSc1	kreslíř
a	a	k8xC	a
hudebník	hudebník	k1gMnSc1	hudebník
(	(	kIx(	(
<g/>
*	*	kIx~	*
4	[number]	k4	4
<g />
.	.	kIx.	.
</s>
<s>
<g/>
září	září	k1gNnSc1	září
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
–	–	k?	–
Karel	Karel	k1gMnSc1	Karel
Herfort	Herfort	k1gInSc1	Herfort
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
gastroenterolog	gastroenterolog	k1gMnSc1	gastroenterolog
(	(	kIx(	(
<g/>
*	*	kIx~	*
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
6	[number]	k4	6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
–	–	k?	–
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Klíma	Klíma	k1gMnSc1	Klíma
starší	starší	k1gMnSc1	starší
<g/>
,	,	kIx,	,
archeolog	archeolog	k1gMnSc1	archeolog
a	a	k8xC	a
geolog	geolog	k1gMnSc1	geolog
(	(	kIx(	(
<g/>
*	*	kIx~	*
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1925	[number]	k4	1925
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
–	–	k?	–
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Preis	Preis	k1gFnSc2	Preis
<g/>
,	,	kIx,	,
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
reprezentant	reprezentant	k1gMnSc1	reprezentant
(	(	kIx(	(
<g/>
*	*	kIx~	*
12	[number]	k4	12
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
9	[number]	k4	9
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
–	–	k?	–
Jaromír	Jaromír	k1gMnSc1	Jaromír
Rubeš	Rubeš	k1gMnSc1	Rubeš
<g/>
,	,	kIx,	,
psychoterapeut	psychoterapeut	k1gMnSc1	psychoterapeut
a	a	k8xC	a
psychiatr	psychiatr	k1gMnSc1	psychiatr
(	(	kIx(	(
<g/>
*	*	kIx~	*
16	[number]	k4	16
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
11	[number]	k4	11
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
Praveček	Praveček	k1gMnSc1	Praveček
mladší	mladý	k2eAgMnSc1d2	mladší
<g/>
,	,	kIx,	,
dirigent	dirigent	k1gMnSc1	dirigent
a	a	k8xC	a
skladatel	skladatel	k1gMnSc1	skladatel
(	(	kIx(	(
<g/>
*	*	kIx~	*
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
–	–	k?	–
Marie	Marie	k1gFnSc1	Marie
Glázrová	Glázrová	k1gFnSc1	Glázrová
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
(	(	kIx(	(
<g/>
*	*	kIx~	*
11	[number]	k4	11
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
–	–	k?	–
Kamila	Kamila	k1gFnSc1	Kamila
Sojková	Sojková	k1gFnSc1	Sojková
<g/>
,	,	kIx,	,
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
*	*	kIx~	*
3	[number]	k4	3
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1901	[number]	k4	1901
<g/>
)	)	kIx)	)
25	[number]	k4	25
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
–	–	k?	–
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Koutecký	Koutecký	k2eAgMnSc1d1	Koutecký
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
a	a	k8xC	a
scénograf	scénograf	k1gMnSc1	scénograf
(	(	kIx(	(
<g/>
*	*	kIx~	*
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
5	[number]	k4	5
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
–	–	k?	–
Lubomír	Lubomír	k1gMnSc1	Lubomír
Emil	Emil	k1gMnSc1	Emil
Havlík	Havlík	k1gMnSc1	Havlík
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
medievalista	medievalista	k1gMnSc1	medievalista
(	(	kIx(	(
<g/>
*	*	kIx~	*
30	[number]	k4	30
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
srpna	srpen	k1gInSc2	srpen
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
–	–	k?	–
Jiří	Jiří	k1gMnSc1	Jiří
Bielecki	Bieleck	k1gMnPc1	Bieleck
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
výtvarník	výtvarník	k1gMnSc1	výtvarník
(	(	kIx(	(
<g/>
*	*	kIx~	*
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
–	–	k?	–
Ladislav	Ladislav	k1gMnSc1	Ladislav
Lis	lis	k1gInSc1	lis
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
(	(	kIx(	(
<g/>
*	*	kIx~	*
24	[number]	k4	24
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Váňa	Váňa	k1gMnSc1	Váňa
<g/>
,	,	kIx,	,
esperantista	esperantista	k1gMnSc1	esperantista
(	(	kIx(	(
<g/>
*	*	kIx~	*
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
–	–	k?	–
Miroslav	Miroslav	k1gMnSc1	Miroslav
Pinc	Pinc	k1gFnSc4	Pinc
<g/>
,	,	kIx,	,
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
knih	kniha	k1gFnPc2	kniha
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
botaniky	botanika	k1gFnSc2	botanika
(	(	kIx(	(
<g/>
*	*	kIx~	*
5	[number]	k4	5
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
Eva	Eva	k1gFnSc1	Eva
Sadková	Sadková	k1gFnSc1	Sadková
<g/>
,	,	kIx,	,
filmová	filmový	k2eAgFnSc1d1	filmová
režisérka	režisérka	k1gFnSc1	režisérka
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
scenáristka	scenáristka	k1gFnSc1	scenáristka
(	(	kIx(	(
<g/>
*	*	kIx~	*
16	[number]	k4	16
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
František	František	k1gMnSc1	František
Šťastný	Šťastný	k1gMnSc1	Šťastný
<g/>
,	,	kIx,	,
motocyklový	motocyklový	k2eAgMnSc1d1	motocyklový
závodník	závodník	k1gMnSc1	závodník
(	(	kIx(	(
<g/>
*	*	kIx~	*
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
–	–	k?	–
Jindřich	Jindřich	k1gMnSc1	Jindřich
Šilhán	šilhán	k2eAgMnSc1d1	šilhán
<g/>
,	,	kIx,	,
astronom	astronom	k1gMnSc1	astronom
a	a	k8xC	a
pedagog	pedagog	k1gMnSc1	pedagog
(	(	kIx(	(
<g/>
*	*	kIx~	*
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
30	[number]	k4	30
<g />
.	.	kIx.	.
</s>
<s>
<g/>
dubna	duben	k1gInSc2	duben
–	–	k?	–
Jiří	Jiří	k1gMnSc1	Jiří
Karen	Karen	k2eAgMnSc1d1	Karen
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
(	(	kIx(	(
<g/>
*	*	kIx~	*
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
Jan	Jan	k1gMnSc1	Jan
Firbas	Firbas	k1gMnSc1	Firbas
<g/>
,	,	kIx,	,
jazykovědec	jazykovědec	k1gMnSc1	jazykovědec
<g/>
,	,	kIx,	,
anglista	anglista	k1gMnSc1	anglista
(	(	kIx(	(
<g/>
*	*	kIx~	*
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
Miroslav	Miroslav	k1gMnSc1	Miroslav
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
patriarcha	patriarcha	k1gMnSc1	patriarcha
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
*	*	kIx~	*
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
–	–	k?	–
Josef	Josef	k1gMnSc1	Josef
Vágner	Vágner	k1gMnSc1	Vágner
<g/>
,	,	kIx,	,
cestovatel	cestovatel	k1gMnSc1	cestovatel
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
*	*	kIx~	*
26	[number]	k4	26
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
12	[number]	k4	12
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
Ladislav	Ladislav	k1gMnSc1	Ladislav
Pokorný	Pokorný	k1gMnSc1	Pokorný
<g/>
,	,	kIx,	,
teolog	teolog	k1gMnSc1	teolog
<g/>
,	,	kIx,	,
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
liturgista	liturgista	k1gMnSc1	liturgista
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g />
.	.	kIx.	.
</s>
<s>
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
Bohuslav	Bohuslava	k1gFnPc2	Bohuslava
Niederle	Niederle	k1gInSc1	Niederle
<g/>
,	,	kIx,	,
chirurg	chirurg	k1gMnSc1	chirurg
(	(	kIx(	(
<g/>
*	*	kIx~	*
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
–	–	k?	–
Jarmila	Jarmila	k1gFnSc1	Jarmila
Urbánková	Urbánková	k1gFnSc1	Urbánková
<g/>
,	,	kIx,	,
překladatelka	překladatelka	k1gFnSc1	překladatelka
a	a	k8xC	a
lyrická	lyrický	k2eAgFnSc1d1	lyrická
básnířka	básnířka	k1gFnSc1	básnířka
(	(	kIx(	(
<g/>
*	*	kIx~	*
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Starý	Starý	k1gMnSc1	Starý
<g/>
,	,	kIx,	,
vysokoškolský	vysokoškolský	k2eAgMnSc1d1	vysokoškolský
pedagog	pedagog	k1gMnSc1	pedagog
a	a	k8xC	a
ministr	ministr	k1gMnSc1	ministr
dopravy	doprava	k1gFnSc2	doprava
(	(	kIx(	(
<g/>
*	*	kIx~	*
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
–	–	k?	–
Josef	Josef	k1gMnSc1	Josef
Adámek	Adámek	k1gMnSc1	Adámek
<g/>
,	,	kIx,	,
řezbář	řezbář	k1gMnSc1	řezbář
a	a	k8xC	a
akademický	akademický	k2eAgMnSc1d1	akademický
sochař	sochař	k1gMnSc1	sochař
(	(	kIx(	(
<g/>
*	*	kIx~	*
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
–	–	k?	–
Inka	Inka	k1gFnSc1	Inka
Zemánková	Zemánková	k1gFnSc1	Zemánková
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
swingová	swingový	k2eAgFnSc1d1	swingová
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
a	a	k8xC	a
herečka	herečka	k1gFnSc1	herečka
(	(	kIx(	(
<g/>
*	*	kIx~	*
14	[number]	k4	14
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
–	–	k?	–
Karel	Karel	k1gMnSc1	Karel
Vladimír	Vladimír	k1gMnSc1	Vladimír
Burian	Burian	k1gMnSc1	Burian
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
(	(	kIx(	(
<g/>
*	*	kIx~	*
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
–	–	k?	–
Jana	Jana	k1gFnSc1	Jana
Březinová	Březinová	k1gFnSc1	Březinová
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
*	*	kIx~	*
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
–	–	k?	–
František	František	k1gMnSc1	František
Trpík	Trpík	k1gMnSc1	Trpík
<g/>
,	,	kIx,	,
voják	voják	k1gMnSc1	voják
<g/>
,	,	kIx,	,
příslušník	příslušník	k1gMnSc1	příslušník
výsadku	výsadek	k1gInSc2	výsadek
Glucinium	Glucinium	k1gNnSc1	Glucinium
(	(	kIx(	(
<g/>
*	*	kIx~	*
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
–	–	k?	–
Josef	Josef	k1gMnSc1	Josef
Kozák	Kozák	k1gMnSc1	Kozák
<g/>
,	,	kIx,	,
volejbalový	volejbalový	k2eAgMnSc1d1	volejbalový
reprezentant	reprezentant	k1gMnSc1	reprezentant
a	a	k8xC	a
trenér	trenér	k1gMnSc1	trenér
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g />
.	.	kIx.	.
</s>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
Renáta	Renáta	k1gFnSc1	Renáta
Doleželová	Doleželová	k1gFnSc1	Doleželová
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
(	(	kIx(	(
<g/>
*	*	kIx~	*
2	[number]	k4	2
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
Štěpán	Štěpán	k1gMnSc1	Štěpán
Benda	Benda	k1gMnSc1	Benda
<g/>
,	,	kIx,	,
vysokoškolský	vysokoškolský	k2eAgMnSc1d1	vysokoškolský
učitel	učitel	k1gMnSc1	učitel
a	a	k8xC	a
exilový	exilový	k2eAgMnSc1d1	exilový
politik	politik	k1gMnSc1	politik
(	(	kIx(	(
<g/>
*	*	kIx~	*
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
13	[number]	k4	13
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
Alois	Alois	k1gMnSc1	Alois
Jedlička	Jedlička	k1gMnSc1	Jedlička
<g/>
,	,	kIx,	,
bohemista	bohemista	k1gMnSc1	bohemista
a	a	k8xC	a
profesor	profesor	k1gMnSc1	profesor
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
(	(	kIx(	(
<g/>
*	*	kIx~	*
20	[number]	k4	20
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
19	[number]	k4	19
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
–	–	k?	–
Josef	Josef	k1gMnSc1	Josef
Istler	Istler	k1gMnSc1	Istler
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
(	(	kIx(	(
<g/>
*	*	kIx~	*
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
–	–	k?	–
Václav	Václav	k1gMnSc1	Václav
Hübner	Hübner	k1gMnSc1	Hübner
<g/>
,	,	kIx,	,
amatérský	amatérský	k2eAgInSc1d1	amatérský
<g />
.	.	kIx.	.
</s>
<s>
astronom	astronom	k1gMnSc1	astronom
a	a	k8xC	a
skautský	skautský	k2eAgMnSc1d1	skautský
pracovník	pracovník	k1gMnSc1	pracovník
(	(	kIx(	(
<g/>
*	*	kIx~	*
18	[number]	k4	18
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
–	–	k?	–
Karel	Karel	k1gMnSc1	Karel
Filsak	Filsak	k1gMnSc1	Filsak
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
(	(	kIx(	(
<g/>
*	*	kIx~	*
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
–	–	k?	–
Vladimír	Vladimír	k1gMnSc1	Vladimír
Ráž	Ráž	k1gMnSc1	Ráž
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
(	(	kIx(	(
<g/>
*	*	kIx~	*
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
července	červenec	k1gInSc2	červenec
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
5	[number]	k4	5
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
František	František	k1gMnSc1	František
Bělský	Bělský	k1gMnSc1	Bělský
<g/>
,	,	kIx,	,
sochař	sochař	k1gMnSc1	sochař
(	(	kIx(	(
<g/>
*	*	kIx~	*
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
Jiří	Jiří	k1gMnSc1	Jiří
Kosina	Kosina	k1gMnSc1	Kosina
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
sbormistr	sbormistr	k1gMnSc1	sbormistr
(	(	kIx(	(
<g/>
*	*	kIx~	*
24	[number]	k4	24
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
–	–	k?	–
Pavel	Pavel	k1gMnSc1	Pavel
Eckstein	Eckstein	k1gMnSc1	Eckstein
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
muzikolog	muzikolog	k1gMnSc1	muzikolog
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
kritik	kritik	k1gMnSc1	kritik
a	a	k8xC	a
dramaturg	dramaturg	k1gMnSc1	dramaturg
(	(	kIx(	(
<g/>
*	*	kIx~	*
27	[number]	k4	27
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
–	–	k?	–
Jiří	Jiří	k1gMnSc1	Jiří
Roll	Roll	k1gMnSc1	Roll
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
scenárista	scenárista	k1gMnSc1	scenárista
(	(	kIx(	(
<g/>
*	*	kIx~	*
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
–	–	k?	–
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Dudek	Dudek	k1gMnSc1	Dudek
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
divadelní	divadelní	k2eAgMnSc1d1	divadelní
a	a	k8xC	a
televizní	televizní	k2eAgMnSc1d1	televizní
režisér	režisér	k1gMnSc1	režisér
(	(	kIx(	(
<g/>
*	*	kIx~	*
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
–	–	k?	–
Oldřich	Oldřich	k1gMnSc1	Oldřich
Daněk	Daněk	k1gMnSc1	Daněk
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
scenárista	scenárista	k1gMnSc1	scenárista
(	(	kIx(	(
<g/>
*	*	kIx~	*
16	[number]	k4	16
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
6	[number]	k4	6
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
–	–	k?	–
Jiří	Jiří	k1gMnSc1	Jiří
Sovák	Sovák	k1gMnSc1	Sovák
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
(	(	kIx(	(
<g/>
*	*	kIx~	*
27	[number]	k4	27
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
–	–	k?	–
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Míka	Míka	k1gMnSc1	Míka
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
divadelní	divadelní	k2eAgMnSc1d1	divadelní
ředitel	ředitel	k1gMnSc1	ředitel
(	(	kIx(	(
<g/>
*	*	kIx~	*
25	[number]	k4	25
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
12	[number]	k4	12
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
–	–	k?	–
Václav	Václav	k1gMnSc1	Václav
Vojta	Vojta	k1gMnSc1	Vojta
<g/>
,	,	kIx,	,
dětský	dětský	k2eAgMnSc1d1	dětský
neurolog	neurolog	k1gMnSc1	neurolog
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
*	*	kIx~	*
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
–	–	k?	–
Václav	Václav	k1gMnSc1	Václav
Migas	Migas	k1gMnSc1	Migas
<g/>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
(	(	kIx(	(
<g/>
*	*	kIx~	*
16	[number]	k4	16
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
26	[number]	k4	26
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Cháňa	Cháňa	k1gMnSc1	Cháňa
<g/>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
(	(	kIx(	(
<g/>
*	*	kIx~	*
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1899	[number]	k4	1899
<g/>
)	)	kIx)	)
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g />
.	.	kIx.	.
</s>
<s>
Janda	Janda	k1gMnSc1	Janda
<g/>
,	,	kIx,	,
fotograf	fotograf	k1gMnSc1	fotograf
(	(	kIx(	(
<g/>
*	*	kIx~	*
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
Karel	Karel	k1gMnSc1	Karel
Teissig	Teissig	k1gMnSc1	Teissig
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
a	a	k8xC	a
grafik	grafik	k1gMnSc1	grafik
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
*	*	kIx~	*
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
Karel	Karel	k1gMnSc1	Karel
Toman	Toman	k1gMnSc1	Toman
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
grafik	grafik	k1gMnSc1	grafik
a	a	k8xC	a
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
(	(	kIx(	(
<g/>
*	*	kIx~	*
27	[number]	k4	27
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
4	[number]	k4	4
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
Ludvík	Ludvík	k1gMnSc1	Ludvík
Ráža	Ráža	k?	Ráža
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
scenárista	scenárista	k1gMnSc1	scenárista
(	(	kIx(	(
<g/>
*	*	kIx~	*
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
Ota	Ota	k1gMnSc1	Ota
B.	B.	kA	B.
Kraus	Kraus	k1gMnSc1	Kraus
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
*	*	kIx~	*
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
Ladislav	Ladislav	k1gMnSc1	Ladislav
Čepelák	Čepelák	k1gMnSc1	Čepelák
<g/>
,	,	kIx,	,
grafik	grafik	k1gMnSc1	grafik
<g/>
,	,	kIx,	,
kreslíř	kreslíř	k1gMnSc1	kreslíř
a	a	k8xC	a
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
(	(	kIx(	(
<g/>
*	*	kIx~	*
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
12	[number]	k4	12
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
Věnceslav	Věnceslava	k1gFnPc2	Věnceslava
Patrovský	Patrovský	k2eAgMnSc1d1	Patrovský
<g/>
,	,	kIx,	,
chemik	chemik	k1gMnSc1	chemik
a	a	k8xC	a
záhadolog	záhadolog	k1gMnSc1	záhadolog
(	(	kIx(	(
<g/>
*	*	kIx~	*
28	[number]	k4	28
<g />
.	.	kIx.	.
</s>
<s>
<g/>
května	květen	k1gInSc2	květen
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
13	[number]	k4	13
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
František	František	k1gMnSc1	František
Štěpánek	Štěpánek	k1gMnSc1	Štěpánek
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgMnSc1d1	divadelní
režisér	režisér	k1gMnSc1	režisér
(	(	kIx(	(
<g/>
*	*	kIx~	*
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
27	[number]	k4	27
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
Lída	Lída	k1gFnSc1	Lída
Baarová	Baarová	k1gFnSc1	Baarová
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
(	(	kIx(	(
<g/>
*	*	kIx~	*
7	[number]	k4	7
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
7	[number]	k4	7
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
listopadu	listopad	k1gInSc2	listopad
–	–	k?	–
Miroslav	Miroslav	k1gMnSc1	Miroslav
Malura	Malura	k1gFnSc1	Malura
<g/>
,	,	kIx,	,
muzikolog	muzikolog	k1gMnSc1	muzikolog
<g/>
,	,	kIx,	,
folklorista	folklorista	k1gMnSc1	folklorista
(	(	kIx(	(
<g/>
*	*	kIx~	*
21	[number]	k4	21
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
–	–	k?	–
Jaromír	Jaromír	k1gMnSc1	Jaromír
Podešva	Podešva	k1gMnSc1	Podešva
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
pedagog	pedagog	k1gMnSc1	pedagog
(	(	kIx(	(
<g/>
*	*	kIx~	*
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
–	–	k?	–
Václav	Václav	k1gMnSc1	Václav
Horák	Horák	k1gMnSc1	Horák
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
reprezentant	reprezentant	k1gMnSc1	reprezentant
(	(	kIx(	(
<g/>
*	*	kIx~	*
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
–	–	k?	–
Emil	Emil	k1gMnSc1	Emil
Zátopek	Zátopek	k1gInSc1	Zátopek
<g/>
,	,	kIx,	,
vytrvalec	vytrvalec	k1gMnSc1	vytrvalec
<g/>
,	,	kIx,	,
čtyřnásobný	čtyřnásobný	k2eAgMnSc1d1	čtyřnásobný
olympijský	olympijský	k2eAgMnSc1d1	olympijský
vítěz	vítěz	k1gMnSc1	vítěz
(	(	kIx(	(
<g/>
*	*	kIx~	*
19	[number]	k4	19
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
Eliška	Eliška	k1gFnSc1	Eliška
Nováková	Nováková	k1gFnSc1	Nováková
<g/>
,	,	kIx,	,
lesnická	lesnický	k2eAgFnSc1d1	lesnická
zooložka	zooložka	k1gFnSc1	zooložka
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
ekoložka	ekoložka	k1gFnSc1	ekoložka
(	(	kIx(	(
<g/>
*	*	kIx~	*
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
Miloš	Miloš	k1gMnSc1	Miloš
Konvalinka	Konvalinka	k1gMnSc1	Konvalinka
<g/>
,	,	kIx,	,
dirigent	dirigent	k1gMnSc1	dirigent
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
rozhlasový	rozhlasový	k2eAgMnSc1d1	rozhlasový
pracovník	pracovník	k1gMnSc1	pracovník
(	(	kIx(	(
<g/>
*	*	kIx~	*
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
11	[number]	k4	11
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
–	–	k?	–
Josef	Josef	k1gMnSc1	Josef
Jirka	Jirka	k1gMnSc1	Jirka
<g/>
,	,	kIx,	,
hokejový	hokejový	k2eAgMnSc1d1	hokejový
reprezentant	reprezentant	k1gMnSc1	reprezentant
<g/>
,	,	kIx,	,
politický	politický	k2eAgMnSc1d1	politický
vězeň	vězeň	k1gMnSc1	vězeň
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g />
.	.	kIx.	.
</s>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
–	–	k?	–
Jindřich	Jindřich	k1gMnSc1	Jindřich
Marco	Marco	k1gMnSc1	Marco
<g/>
,	,	kIx,	,
fotograf	fotograf	k1gMnSc1	fotograf
a	a	k8xC	a
numismatik	numismatik	k1gMnSc1	numismatik
(	(	kIx(	(
<g/>
*	*	kIx~	*
10	[number]	k4	10
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
–	–	k?	–
Stanislav	Stanislav	k1gMnSc1	Stanislav
Hojný	hojný	k2eAgMnSc1d1	hojný
<g/>
,	,	kIx,	,
hráč	hráč	k1gMnSc1	hráč
na	na	k7c4	na
bicí	bicí	k2eAgInPc4d1	bicí
nástroje	nástroj	k1gInPc4	nástroj
(	(	kIx(	(
<g/>
*	*	kIx~	*
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
24	[number]	k4	24
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
–	–	k?	–
Jiří	Jiří	k1gMnSc1	Jiří
Novotný	Novotný	k1gMnSc1	Novotný
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
a	a	k8xC	a
urbanista	urbanista	k1gMnSc1	urbanista
(	(	kIx(	(
<g/>
*	*	kIx~	*
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
29	[number]	k4	29
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
–	–	k?	–
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Jakubíček	Jakubíček	k1gMnSc1	Jakubíček
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
dramaturg	dramaturg	k1gMnSc1	dramaturg
(	(	kIx(	(
<g/>
*	*	kIx~	*
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
?	?	kIx.	?
</s>
<s>
–	–	k?	–
Jiří	Jiří	k1gMnSc1	Jiří
Čepelák	Čepelák	k1gMnSc1	Čepelák
<g/>
,	,	kIx,	,
zoolog	zoolog	k1gMnSc1	zoolog
<g/>
,	,	kIx,	,
entomolog	entomolog	k1gMnSc1	entomolog
a	a	k8xC	a
pedagog	pedagog	k1gMnSc1	pedagog
(	(	kIx(	(
<g/>
*	*	kIx~	*
21	[number]	k4	21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
?	?	kIx.	?
</s>
<s>
–	–	k?	–
Josef	Josef	k1gMnSc1	Josef
Mleziva	mlezivo	k1gNnSc2	mlezivo
<g/>
,	,	kIx,	,
chemický	chemický	k2eAgMnSc1d1	chemický
inženýr	inženýr	k1gMnSc1	inženýr
a	a	k8xC	a
vysokoškolský	vysokoškolský	k2eAgMnSc1d1	vysokoškolský
pedagog	pedagog	k1gMnSc1	pedagog
(	(	kIx(	(
<g/>
*	*	kIx~	*
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
Patrick	Patrick	k1gInSc1	Patrick
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Brian	Brian	k1gMnSc1	Brian
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgMnSc1d1	anglický
romanopisec	romanopisec	k1gMnSc1	romanopisec
(	(	kIx(	(
<g/>
*	*	kIx~	*
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
Marie	Marie	k1gFnSc1	Marie
Mercedes	mercedes	k1gInSc1	mercedes
Bourbonsko-Sicilská	bourbonskoicilský	k2eAgFnSc1d1	bourbonsko-sicilský
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
<g />
.	.	kIx.	.
</s>
<s>
Juana	Juan	k1gMnSc4	Juan
Carlose	Carlosa	k1gFnSc3	Carlosa
I.	I.	kA	I.
<g/>
,	,	kIx,	,
krále	král	k1gMnSc2	král
Španělska	Španělsko	k1gNnSc2	Španělsko
(	(	kIx(	(
<g/>
*	*	kIx~	*
23	[number]	k4	23
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1910	[number]	k4	1910
<g/>
)	)	kIx)	)
Nat	Nat	k1gFnSc2	Nat
Adderley	Adderlea	k1gFnSc2	Adderlea
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
jazzový	jazzový	k2eAgMnSc1d1	jazzový
kornetista	kornetista	k1gMnSc1	kornetista
a	a	k8xC	a
skladatel	skladatel	k1gMnSc1	skladatel
(	(	kIx(	(
<g/>
*	*	kIx~	*
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
5	[number]	k4	5
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
–	–	k?	–
Pavel	Pavel	k1gMnSc1	Pavel
Bunčák	Bunčák	k1gMnSc1	Bunčák
<g/>
,	,	kIx,	,
slovenský	slovenský	k2eAgMnSc1d1	slovenský
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
literární	literární	k2eAgMnSc1d1	literární
vědec	vědec	k1gMnSc1	vědec
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
*	*	kIx~	*
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
Henry	Henry	k1gMnSc1	Henry
Eriksson	Eriksson	k1gMnSc1	Eriksson
<g/>
,	,	kIx,	,
švédský	švédský	k2eAgMnSc1d1	švédský
olympijský	olympijský	k2eAgMnSc1d1	olympijský
vítěz	vítěz	k1gMnSc1	vítěz
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c4	na
1500	[number]	k4	1500
metrů	metr	k1gInPc2	metr
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
(	(	kIx(	(
<g/>
*	*	kIx~	*
23	[number]	k4	23
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
Ray	Ray	k1gMnSc1	Ray
Huang	Huang	k1gMnSc1	Huang
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
historik	historik	k1gMnSc1	historik
(	(	kIx(	(
<g/>
*	*	kIx~	*
25	[number]	k4	25
<g/>
<g />
.	.	kIx.	.
</s>
<s>
června	červen	k1gInSc2	červen
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
13	[number]	k4	13
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
John	John	k1gMnSc1	John
Ljunggren	Ljunggrno	k1gNnPc2	Ljunggrno
<g/>
,	,	kIx,	,
švédský	švédský	k2eAgMnSc1d1	švédský
olympijský	olympijský	k2eAgMnSc1d1	olympijský
vítěz	vítěz	k1gMnSc1	vítěz
v	v	k7c6	v
chůzi	chůze	k1gFnSc6	chůze
(	(	kIx(	(
<g/>
*	*	kIx~	*
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
Enric	Enrice	k1gInPc2	Enrice
Valor	valor	k1gInSc1	valor
i	i	k8xC	i
Vives	Vives	k1gInSc1	Vives
<g/>
,	,	kIx,	,
valencijský	valencijský	k2eAgMnSc1d1	valencijský
vypravěč	vypravěč	k1gMnSc1	vypravěč
a	a	k8xC	a
filolog	filolog	k1gMnSc1	filolog
(	(	kIx(	(
<g/>
*	*	kIx~	*
22	[number]	k4	22
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
15	[number]	k4	15
<g/>
<g />
.	.	kIx.	.
</s>
<s>
ledna	leden	k1gInSc2	leden
–	–	k?	–
Željko	Željka	k1gFnSc5	Željka
Ražnatović	Ražnatović	k1gMnSc1	Ražnatović
<g/>
,	,	kIx,	,
srbský	srbský	k2eAgMnSc1d1	srbský
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
agent	agent	k1gMnSc1	agent
jugoslávské	jugoslávský	k2eAgFnSc2d1	jugoslávská
tajné	tajný	k2eAgFnSc2d1	tajná
služby	služba	k1gFnSc2	služba
UDBA	UDBA	kA	UDBA
(	(	kIx(	(
<g/>
*	*	kIx~	*
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
16	[number]	k4	16
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
–	–	k?	–
Gene	gen	k1gInSc5	gen
Harris	Harris	k1gInSc1	Harris
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
jazzový	jazzový	k2eAgMnSc1d1	jazzový
klavírista	klavírista	k1gMnSc1	klavírista
(	(	kIx(	(
<g/>
*	*	kIx~	*
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
18	[number]	k4	18
<g/>
<g />
.	.	kIx.	.
</s>
<s>
ledna	leden	k1gInSc2	leden
–	–	k?	–
Július	Július	k1gMnSc1	Július
Lenko	Lenka	k1gFnSc5	Lenka
<g/>
,	,	kIx,	,
slovenský	slovenský	k2eAgMnSc1d1	slovenský
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
překladatel	překladatel	k1gMnSc1	překladatel
(	(	kIx(	(
<g/>
*	*	kIx~	*
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
Bettino	Bettina	k1gFnSc5	Bettina
Craxi	Craxe	k1gFnSc3	Craxe
<g/>
,	,	kIx,	,
italský	italský	k2eAgMnSc1d1	italský
sociálnědemokratický	sociálnědemokratický	k2eAgMnSc1d1	sociálnědemokratický
politik	politik	k1gMnSc1	politik
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
*	*	kIx~	*
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
Hedy	Heda	k1gFnPc1	Heda
Kieslerová	Kieslerový	k2eAgFnSc1d1	Kieslerový
<g/>
,	,	kIx,	,
rakousko-americká	rakouskomerický	k2eAgFnSc1d1	rakousko-americký
herečka	herečka	k1gFnSc1	herečka
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
*	*	kIx~	*
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
–	–	k?	–
Antoni	Anton	k1gMnPc1	Anton
Adamiuk	Adamiuk	k1gMnSc1	Adamiuk
<g/>
,	,	kIx,	,
polský	polský	k2eAgMnSc1d1	polský
katolický	katolický	k2eAgMnSc1d1	katolický
biskup	biskup	k1gMnSc1	biskup
(	(	kIx(	(
<g/>
*	*	kIx~	*
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
26	[number]	k4	26
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
–	–	k?	–
Don	Don	k1gMnSc1	Don
Budge	Budg	k1gFnSc2	Budg
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
tenista	tenista	k1gMnSc1	tenista
(	(	kIx(	(
<g/>
*	*	kIx~	*
13	[number]	k4	13
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g />
.	.	kIx.	.
</s>
<s>
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
–	–	k?	–
Friedrich	Friedrich	k1gMnSc1	Friedrich
Gulda	Gulda	k1gMnSc1	Gulda
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
klavírista	klavírista	k1gMnSc1	klavírista
a	a	k8xC	a
skladatel	skladatel	k1gMnSc1	skladatel
(	(	kIx(	(
<g/>
*	*	kIx~	*
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
–	–	k?	–
Martin	Martin	k1gMnSc1	Martin
Benrath	Benrath	k1gMnSc1	Benrath
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
herec	herec	k1gMnSc1	herec
(	(	kIx(	(
<g/>
*	*	kIx~	*
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
6	[number]	k4	6
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
února	únor	k1gInSc2	únor
–	–	k?	–
Steve	Steve	k1gMnSc1	Steve
Waller	Waller	k1gMnSc1	Waller
<g/>
,	,	kIx,	,
britský	britský	k2eAgMnSc1d1	britský
rockový	rockový	k2eAgMnSc1d1	rockový
kytarista	kytarista	k1gMnSc1	kytarista
a	a	k8xC	a
zpěvák	zpěvák	k1gMnSc1	zpěvák
(	(	kIx(	(
<g/>
*	*	kIx~	*
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
–	–	k?	–
Ion	ion	k1gInSc1	ion
Gheorghe	Gheorghe	k1gFnSc1	Gheorghe
Maurer	Maurer	k1gMnSc1	Maurer
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
Rady	rada	k1gFnSc2	rada
ministrů	ministr	k1gMnPc2	ministr
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
(	(	kIx(	(
<g/>
*	*	kIx~	*
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
9	[number]	k4	9
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
Michal	Michal	k1gMnSc1	Michal
Maximilián	Maximilián	k1gMnSc1	Maximilián
Scheer	Scheer	k1gMnSc1	Scheer
<g/>
,	,	kIx,	,
slovenský	slovenský	k2eAgMnSc1d1	slovenský
architekt	architekt	k1gMnSc1	architekt
(	(	kIx(	(
<g/>
*	*	kIx~	*
7	[number]	k4	7
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
12	[number]	k4	12
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
Screamin	Screamin	k1gInSc1	Screamin
<g/>
'	'	kIx"	'
Jay	Jay	k1gFnSc1	Jay
Hawkins	Hawkins	k1gInSc1	Hawkins
<g/>
,	,	kIx,	,
afroamerický	afroamerický	k2eAgMnSc1d1	afroamerický
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
herec	herec	k1gMnSc1	herec
(	(	kIx(	(
<g/>
*	*	kIx~	*
18	[number]	k4	18
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
Charles	Charles	k1gMnSc1	Charles
Schulz	Schulz	k1gMnSc1	Schulz
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
tvůrce	tvůrce	k1gMnSc1	tvůrce
komiksů	komiks	k1gInPc2	komiks
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
*	*	kIx~	*
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
–	–	k?	–
Friedensreich	Friedensreich	k1gMnSc1	Friedensreich
Hundertwasser	Hundertwasser	k1gMnSc1	Hundertwasser
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
architekt	architekt	k1gMnSc1	architekt
<g/>
,	,	kIx,	,
grafik	grafik	k1gMnSc1	grafik
a	a	k8xC	a
malíř	malíř	k1gMnSc1	malíř
(	(	kIx(	(
<g/>
*	*	kIx~	*
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
Ofra	Ofra	k1gFnSc1	Ofra
Haza	Haza	k1gFnSc1	Haza
<g/>
,	,	kIx,	,
izraelská	izraelský	k2eAgFnSc1d1	izraelská
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g />
.	.	kIx.	.
</s>
<s>
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
Stanley	Stanlea	k1gFnSc2	Stanlea
Matthews	Matthews	k1gInSc1	Matthews
<g/>
,	,	kIx,	,
legenda	legenda	k1gFnSc1	legenda
anglické	anglický	k2eAgFnSc2d1	anglická
i	i	k8xC	i
světové	světový	k2eAgFnSc2d1	světová
kopané	kopaná	k1gFnSc2	kopaná
(	(	kIx(	(
<g/>
*	*	kIx~	*
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
26	[number]	k4	26
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
–	–	k?	–
Jana	Jana	k1gFnSc1	Jana
Savojská	savojský	k2eAgFnSc1d1	Savojská
<g/>
,	,	kIx,	,
bulharská	bulharský	k2eAgFnSc1d1	bulharská
carevna	carevna	k1gFnSc1	carevna
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
Borise	Boris	k1gMnSc2	Boris
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
*	*	kIx~	*
13	[number]	k4	13
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
listopadu	listopad	k1gInSc2	listopad
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
27	[number]	k4	27
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
–	–	k?	–
Jurij	Jurij	k1gMnSc1	Jurij
Andrejevič	Andrejevič	k1gMnSc1	Andrejevič
Tregubov	Tregubov	k1gInSc4	Tregubov
<g/>
,	,	kIx,	,
ruský	ruský	k2eAgMnSc1d1	ruský
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
*	*	kIx~	*
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
John	John	k1gMnSc1	John
Thomas	Thomas	k1gMnSc1	Thomas
Sladek	Sladek	k1gMnSc1	Sladek
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
spisovatel	spisovatel	k1gMnSc1	spisovatel
vědeckofantastické	vědeckofantastický	k2eAgFnSc2d1	vědeckofantastická
literatury	literatura	k1gFnSc2	literatura
(	(	kIx(	(
<g/>
*	*	kIx~	*
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
William	William	k1gInSc1	William
<g />
.	.	kIx.	.
</s>
<s>
Porter	porter	k1gInSc1	porter
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
olympijský	olympijský	k2eAgMnSc1d1	olympijský
vítěz	vítěz	k1gMnSc1	vítěz
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c4	na
110	[number]	k4	110
metrů	metr	k1gInPc2	metr
překážek	překážka	k1gFnPc2	překážka
(	(	kIx(	(
<g/>
*	*	kIx~	*
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
11	[number]	k4	11
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
–	–	k?	–
Kazimierz	Kazimierz	k1gInSc1	Kazimierz
Brandys	Brandys	k1gInSc1	Brandys
<g/>
,	,	kIx,	,
polský	polský	k2eAgMnSc1d1	polský
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
scenárista	scenárista	k1gMnSc1	scenárista
(	(	kIx(	(
<g/>
*	*	kIx~	*
27	[number]	k4	27
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
16	[number]	k4	16
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Prudnikau	Prudnikaus	k1gInSc2	Prudnikaus
<g/>
,	,	kIx,	,
běloruský	běloruský	k2eAgMnSc1d1	běloruský
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
*	*	kIx~	*
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
27	[number]	k4	27
<g/>
.	.	kIx.	.
březen	březen	k1gInSc1	březen
–	–	k?	–
Ian	Ian	k1gMnSc2	Ian
Dury	Dura	k1gMnSc2	Dura
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgInSc4d1	anglický
rock	rock	k1gInSc4	rock
and	and	k?	and
rollový	rollový	k2eAgMnSc1d1	rollový
zpěvák	zpěvák	k1gMnSc1	zpěvák
(	(	kIx(	(
<g/>
*	*	kIx~	*
12	[number]	k4	12
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
30	[number]	k4	30
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
–	–	k?	–
Rudolf	Rudolf	k1gMnSc1	Rudolf
Kirchschläger	Kirchschläger	k1gMnSc1	Kirchschläger
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
Rakouska	Rakousko	k1gNnSc2	Rakousko
(	(	kIx(	(
<g/>
*	*	kIx~	*
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
31	[number]	k4	31
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
–	–	k?	–
Gisè	Gisè	k1gFnSc1	Gisè
Freundová	Freundový	k2eAgFnSc1d1	Freundová
<g/>
,	,	kIx,	,
francouzská	francouzský	k2eAgFnSc1d1	francouzská
fotografka	fotografka	k1gFnSc1	fotografka
(	(	kIx(	(
<g/>
*	*	kIx~	*
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
Tommaso	Tommasa	k1gFnSc5	Tommasa
Buscetta	Buscetta	k1gMnSc1	Buscetta
<g/>
,	,	kIx,	,
sicilský	sicilský	k2eAgMnSc1d1	sicilský
mafián	mafián	k1gMnSc1	mafián
(	(	kIx(	(
<g/>
*	*	kIx~	*
13	[number]	k4	13
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
července	červenec	k1gInSc2	červenec
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
Ľudovít	Ľudovít	k1gMnSc1	Ľudovít
Filan	Filan	k1gMnSc1	Filan
<g/>
,	,	kIx,	,
slovenský	slovenský	k2eAgMnSc1d1	slovenský
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
(	(	kIx(	(
<g/>
*	*	kIx~	*
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
3	[number]	k4	3
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
–	–	k?	–
Terence	Terenec	k1gMnSc2	Terenec
McKenna	McKenn	k1gMnSc2	McKenn
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
filosof	filosof	k1gMnSc1	filosof
a	a	k8xC	a
etnobotanik	etnobotanik	k1gMnSc1	etnobotanik
(	(	kIx(	(
<g/>
*	*	kIx~	*
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1946	[number]	k4	1946
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
–	–	k?	–
Vitalij	Vitalij	k1gMnSc1	Vitalij
Hubarenko	Hubarenka	k1gFnSc5	Hubarenka
<g/>
,	,	kIx,	,
ukrajinský	ukrajinský	k2eAgMnSc1d1	ukrajinský
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
(	(	kIx(	(
<g/>
*	*	kIx~	*
13	[number]	k4	13
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
Bertram	Bertram	k1gInSc1	Bertram
Forer	Forer	k1gInSc1	Forer
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
psycholog	psycholog	k1gMnSc1	psycholog
(	(	kIx(	(
<g/>
*	*	kIx~	*
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
Habíb	Habíb	k1gMnSc1	Habíb
Burgiba	Burgiba	k1gMnSc1	Burgiba
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
tuniský	tuniský	k2eAgMnSc1d1	tuniský
prezident	prezident	k1gMnSc1	prezident
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
*	*	kIx~	*
3	[number]	k4	3
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1903	[number]	k4	1903
<g/>
)	)	kIx)	)
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
–	–	k?	–
Larry	Larra	k1gMnSc2	Larra
Linville	Linvill	k1gMnSc2	Linvill
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
komik	komik	k1gMnSc1	komik
(	(	kIx(	(
<g/>
*	*	kIx~	*
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
–	–	k?	–
Karol	Karola	k1gFnPc2	Karola
Šiška	Šiška	k1gMnSc1	Šiška
<g/>
,	,	kIx,	,
slovenský	slovenský	k2eAgMnSc1d1	slovenský
chirurg	chirurg	k1gMnSc1	chirurg
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
(	(	kIx(	(
<g/>
*	*	kIx~	*
19	[number]	k4	19
<g />
.	.	kIx.	.
</s>
<s>
<g/>
března	březen	k1gInSc2	březen
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
–	–	k?	–
Giorgio	Giorgio	k1gMnSc1	Giorgio
Bassani	Bassan	k1gMnPc1	Bassan
<g/>
,	,	kIx,	,
italský	italský	k2eAgMnSc1d1	italský
romanopisec	romanopisec	k1gMnSc1	romanopisec
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
esejista	esejista	k1gMnSc1	esejista
(	(	kIx(	(
<g/>
*	*	kIx~	*
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
–	–	k?	–
Nína	Nín	k1gInSc2	Nín
Björk	Björk	k1gInSc1	Björk
Árnadóttir	Árnadóttir	k1gInSc1	Árnadóttir
<g/>
,	,	kIx,	,
islandská	islandský	k2eAgFnSc1d1	islandská
básnířka	básnířka	k1gFnSc1	básnířka
(	(	kIx(	(
<g/>
*	*	kIx~	*
7	[number]	k4	7
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
června	červen	k1gInSc2	červen
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
18	[number]	k4	18
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
–	–	k?	–
Jicchak	Jicchak	k1gInSc1	Jicchak
Berenblum	Berenblum	k1gInSc1	Berenblum
<g/>
,	,	kIx,	,
izraelský	izraelský	k2eAgMnSc1d1	izraelský
biochemik	biochemik	k1gMnSc1	biochemik
(	(	kIx(	(
<g/>
*	*	kIx~	*
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1903	[number]	k4	1903
<g/>
)	)	kIx)	)
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
–	–	k?	–
Ivan	Ivan	k1gMnSc1	Ivan
Čajda	Čajda	k1gMnSc1	Čajda
<g/>
,	,	kIx,	,
slovenský	slovenský	k2eAgMnSc1d1	slovenský
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
*	*	kIx~	*
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
28	[number]	k4	28
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
Christian	Christian	k1gMnSc1	Christian
Norberg-Schulz	Norberg-Schulz	k1gMnSc1	Norberg-Schulz
<g/>
,	,	kIx,	,
norský	norský	k2eAgMnSc1d1	norský
historik	historik	k1gMnSc1	historik
a	a	k8xC	a
teoretik	teoretik	k1gMnSc1	teoretik
architektury	architektura	k1gFnSc2	architektura
(	(	kIx(	(
<g/>
*	*	kIx~	*
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
–	–	k?	–
Poul	Poul	k1gInSc1	Poul
Hartling	Hartling	k1gInSc1	Hartling
<g/>
,	,	kIx,	,
premiér	premiér	k1gMnSc1	premiér
Dánska	Dánsko	k1gNnSc2	Dánsko
(	(	kIx(	(
<g/>
*	*	kIx~	*
14	[number]	k4	14
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
–	–	k?	–
Steve	Steve	k1gMnSc1	Steve
Reeves	Reeves	k1gMnSc1	Reeves
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
americký	americký	k2eAgMnSc1d1	americký
kulturista	kulturista	k1gMnSc1	kulturista
a	a	k8xC	a
herec	herec	k1gMnSc1	herec
(	(	kIx(	(
<g/>
*	*	kIx~	*
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
4	[number]	k4	4
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
–	–	k?	–
Hendrik	Hendrik	k1gMnSc1	Hendrik
Casimir	Casimir	k1gMnSc1	Casimir
<g/>
,	,	kIx,	,
nizozemský	nizozemský	k2eAgMnSc1d1	nizozemský
fyzik	fyzik	k1gMnSc1	fyzik
(	(	kIx(	(
<g/>
*	*	kIx~	*
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
–	–	k?	–
Gino	Gino	k1gMnSc1	Gino
Bartali	Bartali	k1gFnSc2	Bartali
italský	italský	k2eAgMnSc1d1	italský
profesionální	profesionální	k2eAgMnSc1d1	profesionální
cyklista	cyklista	k1gMnSc1	cyklista
(	(	kIx(	(
<g/>
*	*	kIx~	*
18	[number]	k4	18
<g/>
<g />
.	.	kIx.	.
</s>
<s>
června	červen	k1gInSc2	červen
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
–	–	k?	–
Keizó	Keizó	k1gFnSc1	Keizó
Obuči	Obuč	k1gInSc3	Obuč
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
japonské	japonský	k2eAgFnSc2d1	japonská
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
*	*	kIx~	*
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
–	–	k?	–
Alfred	Alfred	k1gMnSc1	Alfred
Kučevskij	Kučevskij	k1gMnSc1	Kučevskij
<g/>
,	,	kIx,	,
sovětský	sovětský	k2eAgMnSc1d1	sovětský
hokejový	hokejový	k2eAgMnSc1d1	hokejový
reprezentant	reprezentant	k1gMnSc1	reprezentant
(	(	kIx(	(
<g/>
*	*	kIx~	*
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
18	[number]	k4	18
<g />
.	.	kIx.	.
</s>
<s>
<g/>
května	květen	k1gInSc2	květen
–	–	k?	–
Domingos	Domingos	k1gMnSc1	Domingos
da	da	k?	da
Guia	Guia	k1gMnSc1	Guia
<g/>
,	,	kIx,	,
brazilský	brazilský	k2eAgMnSc1d1	brazilský
fotbalista	fotbalista	k1gMnSc1	fotbalista
(	(	kIx(	(
<g/>
*	*	kIx~	*
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
19	[number]	k4	19
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
–	–	k?	–
Jevgenij	Jevgenij	k1gFnSc1	Jevgenij
Chrunov	Chrunovo	k1gNnPc2	Chrunovo
<g/>
,	,	kIx,	,
sovětský	sovětský	k2eAgMnSc1d1	sovětský
kosmonaut	kosmonaut	k1gMnSc1	kosmonaut
<g/>
,	,	kIx,	,
velitel	velitel	k1gMnSc1	velitel
Sojuzu	Sojuz	k1gInSc2	Sojuz
5	[number]	k4	5
(	(	kIx(	(
<g/>
*	*	kIx~	*
10	[number]	k4	10
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
20	[number]	k4	20
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
května	květen	k1gInSc2	květen
–	–	k?	–
Jean-Pierre	Jean-Pierr	k1gMnSc5	Jean-Pierr
Rampal	Rampal	k1gMnSc5	Rampal
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
flétnista	flétnista	k1gMnSc1	flétnista
(	(	kIx(	(
<g/>
*	*	kIx~	*
7	[number]	k4	7
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
Erich	Erich	k1gMnSc1	Erich
Mielke	Mielke	k1gFnSc1	Mielke
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
pro	pro	k7c4	pro
státní	státní	k2eAgFnSc4d1	státní
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
(	(	kIx(	(
<g/>
Stasi	stase	k1gFnSc4	stase
<g/>
)	)	kIx)	)
NDR	NDR	kA	NDR
(	(	kIx(	(
<g/>
*	*	kIx~	*
28	[number]	k4	28
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
Barbara	Barbara	k1gFnSc1	Barbara
Cartland	Cartland	k1gInSc1	Cartland
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
anglická	anglický	k2eAgFnSc1d1	anglická
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
(	(	kIx(	(
<g/>
*	*	kIx~	*
9	[number]	k4	9
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1901	[number]	k4	1901
<g/>
)	)	kIx)	)
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
–	–	k?	–
Francis	Francis	k1gFnPc2	Francis
Lederer	Lederer	k1gMnSc1	Lederer
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
herec	herec	k1gMnSc1	herec
(	(	kIx(	(
<g/>
*	*	kIx~	*
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1899	[number]	k4	1899
<g/>
)	)	kIx)	)
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
–	–	k?	–
Maurice	Maurika	k1gFnSc3	Maurika
Richard	Richard	k1gMnSc1	Richard
<g/>
,	,	kIx,	,
kanadský	kanadský	k2eAgMnSc1d1	kanadský
hokejový	hokejový	k2eAgMnSc1d1	hokejový
útočník	útočník	k1gMnSc1	útočník
(	(	kIx(	(
<g/>
*	*	kIx~	*
4	[number]	k4	4
<g/>
<g />
.	.	kIx.	.
</s>
<s>
srpna	srpen	k1gInSc2	srpen
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
28	[number]	k4	28
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
–	–	k?	–
Vincentas	Vincentas	k1gMnSc1	Vincentas
Sladkevičius	Sladkevičius	k1gMnSc1	Sladkevičius
<g/>
,	,	kIx,	,
litevský	litevský	k2eAgMnSc1d1	litevský
kardinál	kardinál	k1gMnSc1	kardinál
<g/>
,	,	kIx,	,
kaunaský	kaunaský	k1gMnSc1	kaunaský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
(	(	kIx(	(
<g/>
*	*	kIx~	*
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
Petar	Petar	k1gInSc1	Petar
Mladenov	Mladenov	k1gInSc1	Mladenov
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgMnSc1d1	poslední
prezident	prezident	k1gMnSc1	prezident
socialistického	socialistický	k2eAgNnSc2d1	socialistické
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
(	(	kIx(	(
<g/>
*	*	kIx~	*
22	[number]	k4	22
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1936	[number]	k4	1936
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Erich	Erich	k1gMnSc1	Erich
Kähler	Kähler	k1gMnSc1	Kähler
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
matematik	matematik	k1gMnSc1	matematik
(	(	kIx(	(
<g/>
*	*	kIx~	*
16	[number]	k4	16
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
–	–	k?	–
Merton	Merton	k1gInSc1	Merton
Miller	Miller	k1gMnSc1	Miller
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
ekonom	ekonom	k1gMnSc1	ekonom
<g/>
,	,	kIx,	,
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
1990	[number]	k4	1990
(	(	kIx(	(
<g/>
*	*	kIx~	*
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
–	–	k?	–
Franco	Franco	k1gMnSc1	Franco
<g />
.	.	kIx.	.
</s>
<s>
Rossi	Rosse	k1gFnSc4	Rosse
<g/>
,	,	kIx,	,
italský	italský	k2eAgMnSc1d1	italský
filmový	filmový	k2eAgMnSc1d1	filmový
scenárista	scenárista	k1gMnSc1	scenárista
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
(	(	kIx(	(
<g/>
*	*	kIx~	*
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
9	[number]	k4	9
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
Gwynne	Gwynn	k1gInSc5	Gwynn
Edwards	Edwards	k1gInSc1	Edwards
<g/>
,	,	kIx,	,
velšský	velšský	k2eAgMnSc1d1	velšský
violista	violista	k1gMnSc1	violista
a	a	k8xC	a
pedagog	pedagog	k1gMnSc1	pedagog
(	(	kIx(	(
<g/>
*	*	kIx~	*
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
Ernst	Ernst	k1gMnSc1	Ernst
Jandl	Jandl	k1gMnSc1	Jandl
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g />
.	.	kIx.	.
</s>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
–	–	k?	–
Háfiz	Háfiz	k1gInSc1	Háfiz
al-Asad	al-Asad	k1gInSc1	al-Asad
<g/>
,	,	kIx,	,
syrský	syrský	k2eAgMnSc1d1	syrský
prezident	prezident	k1gMnSc1	prezident
(	(	kIx(	(
<g/>
*	*	kIx~	*
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
–	–	k?	–
Alojz	Alojz	k1gMnSc1	Alojz
Habovštiak	Habovštiak	k1gMnSc1	Habovštiak
<g/>
,	,	kIx,	,
slovenský	slovenský	k2eAgMnSc1d1	slovenský
archeolog	archeolog	k1gMnSc1	archeolog
(	(	kIx(	(
<g/>
*	*	kIx~	*
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
19	[number]	k4	19
<g />
.	.	kIx.	.
</s>
<s>
<g/>
června	červen	k1gInSc2	červen
–	–	k?	–
Noboru	Nobor	k1gInSc2	Nobor
Takešita	Takešit	k2eAgFnSc1d1	Takešit
<g/>
,	,	kIx,	,
premiér	premiér	k1gMnSc1	premiér
Japonska	Japonsko	k1gNnSc2	Japonsko
(	(	kIx(	(
<g/>
*	*	kIx~	*
26	[number]	k4	26
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
Günther	Günthra	k1gFnPc2	Günthra
Sabetzki	Sabetzk	k1gFnSc2	Sabetzk
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
hokejové	hokejový	k2eAgFnSc2d1	hokejová
federace	federace	k1gFnSc2	federace
(	(	kIx(	(
<g/>
*	*	kIx~	*
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
Alan	Alan	k1gMnSc1	Alan
Hovhaness	Hovhanessa	k1gFnPc2	Hovhanessa
<g/>
,	,	kIx,	,
arménsko-americký	arménskomerický	k2eAgMnSc1d1	arménsko-americký
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
*	*	kIx~	*
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
–	–	k?	–
Jerónimo	Jerónima	k1gFnSc5	Jerónima
Podestá	Podestý	k2eAgFnSc1d1	Podestá
<g/>
,	,	kIx,	,
argentinský	argentinský	k2eAgMnSc1d1	argentinský
katolický	katolický	k2eAgMnSc1d1	katolický
kněz	kněz	k1gMnSc1	kněz
a	a	k8xC	a
biskup	biskup	k1gMnSc1	biskup
(	(	kIx(	(
<g/>
*	*	kIx~	*
8	[number]	k4	8
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
26	[number]	k4	26
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
–	–	k?	–
Ken	Ken	k1gFnSc1	Ken
Bell	bell	k1gInSc1	bell
<g/>
,	,	kIx,	,
kanadský	kanadský	k2eAgMnSc1d1	kanadský
fotograf	fotograf	k1gMnSc1	fotograf
(	(	kIx(	(
<g/>
*	*	kIx~	*
30	[number]	k4	30
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
července	červenec	k1gInSc2	červenec
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
–	–	k?	–
Vittorio	Vittorio	k1gMnSc1	Vittorio
Gassman	Gassman	k1gMnSc1	Gassman
<g/>
,	,	kIx,	,
italský	italský	k2eAgMnSc1d1	italský
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
(	(	kIx(	(
<g/>
*	*	kIx~	*
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
Cub	Cub	k1gMnSc1	Cub
Koda	Koda	k1gMnSc1	Koda
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
rockový	rockový	k2eAgMnSc1d1	rockový
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
kytarista	kytarista	k1gMnSc1	kytarista
skladatel	skladatel	k1gMnSc1	skladatel
(	(	kIx(	(
<g/>
*	*	kIx~	*
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1948	[number]	k4	1948
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Walter	Walter	k1gMnSc1	Walter
Matthau	Matthaus	k1gInSc2	Matthaus
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
herec	herec	k1gMnSc1	herec
(	(	kIx(	(
<g/>
*	*	kIx~	*
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
3	[number]	k4	3
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
–	–	k?	–
John	John	k1gMnSc1	John
Hejduk	Hejduk	k1gMnSc1	Hejduk
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
architekt	architekt	k1gMnSc1	architekt
(	(	kIx(	(
<g/>
*	*	kIx~	*
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
–	–	k?	–
Wladyslaw	Wladyslaw	k1gMnSc1	Wladyslaw
Szpilman	Szpilman	k1gMnSc1	Szpilman
<g/>
,	,	kIx,	,
polský	polský	k2eAgMnSc1d1	polský
klavírista	klavírista	k1gMnSc1	klavírista
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
(	(	kIx(	(
<g/>
*	*	kIx~	*
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
–	–	k?	–
FM-	FM-	k1gMnSc1	FM-
<g/>
2030	[number]	k4	2030
<g/>
,	,	kIx,	,
Fereidoun	Fereidoun	k1gInSc1	Fereidoun
M.	M.	kA	M.
Esfandiary	Esfandiara	k1gFnSc2	Esfandiara
<g/>
,	,	kIx,	,
americko-íránský	americko-íránský	k2eAgMnSc1d1	americko-íránský
transhumanistický	transhumanistický	k2eAgMnSc1d1	transhumanistický
filozof	filozof	k1gMnSc1	filozof
a	a	k8xC	a
futurolog	futurolog	k1gMnSc1	futurolog
(	(	kIx(	(
<g/>
*	*	kIx~	*
15	[number]	k4	15
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
11	[number]	k4	11
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
–	–	k?	–
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Filip	Filip	k1gMnSc1	Filip
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
slovenský	slovenský	k2eAgMnSc1d1	slovenský
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
humorista	humorista	k1gMnSc1	humorista
<g/>
,	,	kIx,	,
dramaturg	dramaturg	k1gMnSc1	dramaturg
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
(	(	kIx(	(
<g/>
*	*	kIx~	*
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
–	–	k?	–
Jan	Jan	k1gMnSc1	Jan
Karski	Karsk	k1gMnPc1	Karsk
<g/>
,	,	kIx,	,
polský	polský	k2eAgMnSc1d1	polský
odbojář	odbojář	k1gMnSc1	odbojář
(	(	kIx(	(
<g/>
*	*	kIx~	*
24	[number]	k4	24
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
15	[number]	k4	15
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
července	červenec	k1gInSc2	červenec
–	–	k?	–
Juan	Juan	k1gMnSc1	Juan
Filloy	Filloa	k1gFnSc2	Filloa
<g/>
,	,	kIx,	,
argentinský	argentinský	k2eAgMnSc1d1	argentinský
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
*	*	kIx~	*
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1894	[number]	k4	1894
<g/>
)	)	kIx)	)
18	[number]	k4	18
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
–	–	k?	–
José	Josá	k1gFnSc2	Josá
Ángel	Ángela	k1gFnPc2	Ángela
Valente	Valent	k1gInSc5	Valent
<g/>
,	,	kIx,	,
španělský	španělský	k2eAgMnSc1d1	španělský
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
*	*	kIx~	*
25	[number]	k4	25
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
–	–	k?	–
Alexis	Alexis	k1gInSc1	Alexis
Vlasto	Vlasta	k1gFnSc5	Vlasta
<g/>
,	,	kIx,	,
britský	britský	k2eAgInSc1d1	britský
<g />
.	.	kIx.	.
</s>
<s>
slavista	slavista	k1gMnSc1	slavista
(	(	kIx(	(
<g/>
*	*	kIx~	*
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
24	[number]	k4	24
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
–	–	k?	–
Anatolij	Anatolij	k1gFnSc1	Anatolij
Firsov	Firsovo	k1gNnPc2	Firsovo
<g/>
,	,	kIx,	,
sovětský	sovětský	k2eAgMnSc1d1	sovětský
hokejista	hokejista	k1gMnSc1	hokejista
(	(	kIx(	(
<g/>
*	*	kIx~	*
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
–	–	k?	–
Julia	Julius	k1gMnSc2	Julius
Pirotte	Pirott	k1gInSc5	Pirott
<g/>
,	,	kIx,	,
polská	polský	k2eAgFnSc1d1	polská
fotoreportérka	fotoreportérka	k1gFnSc1	fotoreportérka
(	(	kIx(	(
<g/>
*	*	kIx~	*
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g />
.	.	kIx.	.
</s>
<s>
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
–	–	k?	–
Árpád	Árpáda	k1gFnPc2	Árpáda
Göncz	Göncz	k1gMnSc1	Göncz
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
překladatel	překladatel	k1gMnSc1	překladatel
(	(	kIx(	(
<g/>
*	*	kIx~	*
10	[number]	k4	10
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
5	[number]	k4	5
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
–	–	k?	–
Alec	Alec	k1gInSc1	Alec
Guinness	Guinness	k1gInSc1	Guinness
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgMnSc1d1	anglický
herec	herec	k1gMnSc1	herec
(	(	kIx(	(
<g/>
*	*	kIx~	*
2	[number]	k4	2
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1914	[number]	k4	1914
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
7	[number]	k4	7
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
–	–	k?	–
Georges	Georges	k1gMnSc1	Georges
Matheron	Matheron	k1gMnSc1	Matheron
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
geolog	geolog	k1gMnSc1	geolog
(	(	kIx(	(
<g/>
*	*	kIx~	*
2	[number]	k4	2
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
–	–	k?	–
John	John	k1gMnSc1	John
Harsanyi	Harsany	k1gMnPc1	Harsany
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
ekonom	ekonom	k1gMnSc1	ekonom
<g/>
,	,	kIx,	,
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
1994	[number]	k4	1994
(	(	kIx(	(
<g/>
*	*	kIx~	*
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
–	–	k?	–
Loretta	Loretta	k1gFnSc1	Loretta
Youngová	Youngový	k2eAgFnSc1d1	Youngová
<g/>
,	,	kIx,	,
americká	americký	k2eAgFnSc1d1	americká
herečka	herečka	k1gFnSc1	herečka
(	(	kIx(	(
<g/>
*	*	kIx~	*
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
–	–	k?	–
Belo	Bela	k1gFnSc5	Bela
Polla	Polla	k1gMnSc1	Polla
<g/>
,	,	kIx,	,
slovenský	slovenský	k2eAgMnSc1d1	slovenský
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
archeolog	archeolog	k1gMnSc1	archeolog
a	a	k8xC	a
archivář	archivář	k1gMnSc1	archivář
(	(	kIx(	(
<g/>
*	*	kIx~	*
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
22	[number]	k4	22
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
srpna	srpen	k1gInSc2	srpen
–	–	k?	–
Abulfaz	Abulfaz	k1gInSc1	Abulfaz
Elčibej	Elčibej	k1gFnSc1	Elčibej
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
Ázerbájdžánu	Ázerbájdžán	k1gInSc2	Ázerbájdžán
(	(	kIx(	(
<g/>
*	*	kIx~	*
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
Jack	Jack	k1gInSc1	Jack
Nitzsche	Nitzsche	k1gInSc1	Nitzsche
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
producent	producent	k1gMnSc1	producent
a	a	k8xC	a
skladatel	skladatel	k1gMnSc1	skladatel
(	(	kIx(	(
<g/>
*	*	kIx~	*
22	[number]	k4	22
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
Ivan	Ivan	k1gMnSc1	Ivan
Stambolić	Stambolić	k1gMnSc1	Stambolić
<g/>
,	,	kIx,	,
srbský	srbský	k2eAgMnSc1d1	srbský
komunistický	komunistický	k2eAgMnSc1d1	komunistický
<g />
.	.	kIx.	.
</s>
<s>
politik	politik	k1gMnSc1	politik
(	(	kIx(	(
<g/>
*	*	kIx~	*
5	[number]	k4	5
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
–	–	k?	–
Alexander	Alexandra	k1gFnPc2	Alexandra
Žabka	Žabka	k1gMnSc1	Žabka
<g/>
,	,	kIx,	,
slovenský	slovenský	k2eAgMnSc1d1	slovenský
geolog	geolog	k1gMnSc1	geolog
(	(	kIx(	(
<g/>
*	*	kIx~	*
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
–	–	k?	–
Walter	Walter	k1gMnSc1	Walter
Gotschke	Gotschk	k1gFnSc2	Gotschk
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
malíř	malíř	k1gMnSc1	malíř
(	(	kIx(	(
<g/>
*	*	kIx~	*
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g />
.	.	kIx.	.
</s>
<s>
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
–	–	k?	–
Conrad	Conrad	k1gInSc1	Conrad
Marca-Relli	Marca-Rell	k1gMnPc1	Marca-Rell
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
malíř	malíř	k1gMnSc1	malíř
(	(	kIx(	(
<g/>
*	*	kIx~	*
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
12	[number]	k4	12
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
–	–	k?	–
Stanley	Stanley	k1gInPc1	Stanley
Turrentine	Turrentin	k1gInSc5	Turrentin
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
jazzový	jazzový	k2eAgMnSc1d1	jazzový
saxofonista	saxofonista	k1gMnSc1	saxofonista
(	(	kIx(	(
<g/>
*	*	kIx~	*
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
David	David	k1gMnSc1	David
Flusser	Flusser	k1gMnSc1	Flusser
<g/>
,	,	kIx,	,
izraelský	izraelský	k2eAgMnSc1d1	izraelský
historik	historik	k1gMnSc1	historik
(	(	kIx(	(
<g/>
*	*	kIx~	*
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
–	–	k?	–
Hester	Hestra	k1gFnPc2	Hestra
Burtonová	Burtonový	k2eAgFnSc1d1	Burtonová
<g/>
,	,	kIx,	,
anglická	anglický	k2eAgFnSc1d1	anglická
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
(	(	kIx(	(
<g/>
*	*	kIx~	*
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
19	[number]	k4	19
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
–	–	k?	–
Milan	Milan	k1gMnSc1	Milan
Peťovský	Peťovský	k1gMnSc1	Peťovský
<g/>
,	,	kIx,	,
slovenský	slovenský	k2eAgMnSc1d1	slovenský
kameraman	kameraman	k1gMnSc1	kameraman
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
(	(	kIx(	(
<g/>
*	*	kIx~	*
11	[number]	k4	11
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
20	[number]	k4	20
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
Stanislav	Stanislav	k1gMnSc1	Stanislav
Stratiev	Strativa	k1gFnPc2	Strativa
<g/>
,	,	kIx,	,
bulharský	bulharský	k2eAgMnSc1d1	bulharský
prozaik	prozaik	k1gMnSc1	prozaik
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
scenárista	scenárista	k1gMnSc1	scenárista
(	(	kIx(	(
<g/>
*	*	kIx~	*
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
German	German	k1gMnSc1	German
Titov	Titov	k1gInSc4	Titov
<g/>
,	,	kIx,	,
sovětský	sovětský	k2eAgMnSc1d1	sovětský
vojenský	vojenský	k2eAgMnSc1d1	vojenský
letec	letec	k1gMnSc1	letec
a	a	k8xC	a
kosmonaut	kosmonaut	k1gMnSc1	kosmonaut
(	(	kIx(	(
<g/>
*	*	kIx~	*
11	[number]	k4	11
<g />
.	.	kIx.	.
</s>
<s>
<g/>
září	září	k1gNnSc1	září
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
22	[number]	k4	22
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
–	–	k?	–
Jehuda	Jehuda	k1gMnSc1	Jehuda
Amichai	Amicha	k1gMnPc1	Amicha
<g/>
,	,	kIx,	,
izraelsko-německý	izraelskoěmecký	k2eAgMnSc1d1	izraelsko-německý
básník	básník	k1gMnSc1	básník
(	(	kIx(	(
<g/>
*	*	kIx~	*
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
–	–	k?	–
Pierre	Pierr	k1gInSc5	Pierr
Trudeau	Trudeaum	k1gNnSc6	Trudeaum
<g/>
,	,	kIx,	,
kanadský	kanadský	k2eAgMnSc1d1	kanadský
ministerský	ministerský	k2eAgMnSc1d1	ministerský
předseda	předseda	k1gMnSc1	předseda
(	(	kIx(	(
<g/>
*	*	kIx~	*
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g />
.	.	kIx.	.
</s>
<s>
<g/>
října	říjen	k1gInSc2	říjen
–	–	k?	–
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Trofimovič	Trofimovič	k1gMnSc1	Trofimovič
Fedorenko	Fedorenka	k1gFnSc5	Fedorenka
<g/>
,	,	kIx,	,
stálý	stálý	k2eAgMnSc1d1	stálý
zástupce	zástupce	k1gMnSc1	zástupce
SSSR	SSSR	kA	SSSR
při	při	k7c6	při
OSN	OSN	kA	OSN
(	(	kIx(	(
<g/>
*	*	kIx~	*
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
4	[number]	k4	4
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
Michael	Michael	k1gMnSc1	Michael
Smith	Smith	k1gMnSc1	Smith
<g/>
,	,	kIx,	,
kanadský	kanadský	k2eAgMnSc1d1	kanadský
chemik	chemik	k1gMnSc1	chemik
<g/>
,	,	kIx,	,
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
1993	[number]	k4	1993
(	(	kIx(	(
<g/>
*	*	kIx~	*
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1932	[number]	k4	1932
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
Sonja	Sonja	k1gMnSc1	Sonja
Bullaty	Bulle	k1gNnPc7	Bulle
<g/>
,	,	kIx,	,
americká	americký	k2eAgFnSc1d1	americká
fotografka	fotografka	k1gFnSc1	fotografka
českého	český	k2eAgInSc2d1	český
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
*	*	kIx~	*
17	[number]	k4	17
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
Sirimavo	Sirimava	k1gFnSc5	Sirimava
Bandaranaike	Bandaranaike	k1gFnSc1	Bandaranaike
<g/>
,	,	kIx,	,
ministerská	ministerský	k2eAgFnSc1d1	ministerská
předsedkyně	předsedkyně	k1gFnSc1	předsedkyně
Srí	Srí	k1gFnSc1	Srí
Lanky	lanko	k1gNnPc7	lanko
(	(	kIx(	(
<g/>
*	*	kIx~	*
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
11	[number]	k4	11
<g/>
<g />
.	.	kIx.	.
</s>
<s>
října	říjen	k1gInSc2	říjen
–	–	k?	–
Pietro	Pietro	k1gNnSc4	Pietro
Palazzini	Palazzin	k1gMnPc1	Palazzin
<g/>
,	,	kIx,	,
italský	italský	k2eAgMnSc1d1	italský
kardinál	kardinál	k1gMnSc1	kardinál
<g/>
,	,	kIx,	,
Spravedlivý	spravedlivý	k2eAgMnSc1d1	spravedlivý
mezi	mezi	k7c7	mezi
národy	národ	k1gInPc7	národ
(	(	kIx(	(
<g/>
*	*	kIx~	*
19	[number]	k4	19
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
13	[number]	k4	13
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
Gus	Gus	k1gMnSc1	Gus
Hall	Hall	k1gMnSc1	Hall
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgFnPc2d1	americká
(	(	kIx(	(
<g/>
*	*	kIx~	*
8	[number]	k4	8
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1910	[number]	k4	1910
<g/>
)	)	kIx)	)
15	[number]	k4	15
<g />
.	.	kIx.	.
</s>
<s>
<g/>
října	říjen	k1gInSc2	říjen
Tito	tento	k3xDgMnPc1	tento
Gómez	Gómez	k1gMnSc1	Gómez
<g/>
,	,	kIx,	,
kubánský	kubánský	k2eAgMnSc1d1	kubánský
zpěvák	zpěvák	k1gMnSc1	zpěvák
(	(	kIx(	(
<g/>
*	*	kIx~	*
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
Konrad	Konrada	k1gFnPc2	Konrada
Bloch	Bloch	k1gInSc1	Bloch
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
biochemik	biochemik	k1gMnSc1	biochemik
<g/>
,	,	kIx,	,
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
a	a	k8xC	a
medicínu	medicína	k1gFnSc4	medicína
1964	[number]	k4	1964
(	(	kIx(	(
<g/>
*	*	kIx~	*
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
Steve	Steve	k1gMnSc1	Steve
Allen	Allen	k1gMnSc1	Allen
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
(	(	kIx(	(
<g/>
*	*	kIx~	*
26	[number]	k4	26
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
Štefan	Štefan	k1gMnSc1	Štefan
Mašlonka	Mašlonka	k1gFnSc1	Mašlonka
<g/>
,	,	kIx,	,
slovenský	slovenský	k2eAgMnSc1d1	slovenský
sportovní	sportovní	k2eAgMnSc1d1	sportovní
novinář	novinář	k1gMnSc1	novinář
a	a	k8xC	a
rozhlasový	rozhlasový	k2eAgMnSc1d1	rozhlasový
reportér	reportér	k1gMnSc1	reportér
(	(	kIx(	(
<g/>
*	*	kIx~	*
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
–	–	k?	–
Steven	Steven	k2eAgMnSc1d1	Steven
Runciman	Runciman	k1gMnSc1	Runciman
<g/>
,	,	kIx,	,
britský	britský	k2eAgMnSc1d1	britský
<g />
.	.	kIx.	.
</s>
<s>
historik	historik	k1gMnSc1	historik
(	(	kIx(	(
<g/>
*	*	kIx~	*
7	[number]	k4	7
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1903	[number]	k4	1903
<g/>
)	)	kIx)	)
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
–	–	k?	–
Charles	Charles	k1gMnSc1	Charles
F.	F.	kA	F.
Hockett	Hockett	k1gMnSc1	Hockett
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
jazykovědec	jazykovědec	k1gMnSc1	jazykovědec
(	(	kIx(	(
<g/>
*	*	kIx~	*
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
Hilmar	Hilmar	k1gMnSc1	Hilmar
Pabel	Pabel	k1gMnSc1	Pabel
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
fotograf	fotograf	k1gMnSc1	fotograf
a	a	k8xC	a
žurnalista	žurnalista	k1gMnSc1	žurnalista
(	(	kIx(	(
<g/>
*	*	kIx~	*
17	[number]	k4	17
<g/>
<g />
.	.	kIx.	.
</s>
<s>
září	září	k1gNnSc1	září
1910	[number]	k4	1910
<g/>
)	)	kIx)	)
Lyon	Lyon	k1gInSc1	Lyon
Sprague	Spragu	k1gFnSc2	Spragu
de	de	k?	de
Camp	camp	k1gInSc1	camp
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
*	*	kIx~	*
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
Boris	Boris	k1gMnSc1	Boris
Vladimirovič	Vladimirovič	k1gMnSc1	Vladimirovič
Zachoder	Zachoder	k1gMnSc1	Zachoder
<g/>
,	,	kIx,	,
ruský	ruský	k2eAgMnSc1d1	ruský
spisovatel	spisovatel	k1gMnSc1	spisovatel
dětské	dětský	k2eAgFnSc2d1	dětská
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
(	(	kIx(	(
<g/>
*	*	kIx~	*
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1918	[number]	k4	1918
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Ingrid	Ingrid	k1gFnSc1	Ingrid
Švédská	švédský	k2eAgFnSc1d1	švédská
<g/>
,	,	kIx,	,
dánská	dánský	k2eAgFnSc1d1	dánská
královna	královna	k1gFnSc1	královna
(	(	kIx(	(
<g/>
*	*	kIx~	*
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1910	[number]	k4	1910
<g/>
)	)	kIx)	)
10	[number]	k4	10
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
–	–	k?	–
Jacques	Jacques	k1gMnSc1	Jacques
Chaban-Delmas	Chaban-Delmas	k1gMnSc1	Chaban-Delmas
<g/>
,	,	kIx,	,
premiér	premiér	k1gMnSc1	premiér
Francie	Francie	k1gFnSc2	Francie
(	(	kIx(	(
<g/>
*	*	kIx~	*
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
–	–	k?	–
Jacob	Jacoba	k1gFnPc2	Jacoba
Willem	Willo	k1gNnSc7	Willo
Cohen	Cohen	k2eAgInSc1d1	Cohen
<g/>
,	,	kIx,	,
holandský	holandský	k2eAgInSc1d1	holandský
<g />
.	.	kIx.	.
</s>
<s>
matematik	matematik	k1gMnSc1	matematik
(	(	kIx(	(
<g/>
*	*	kIx~	*
27	[number]	k4	27
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
–	–	k?	–
Louis	Louis	k1gMnSc1	Louis
Eugè	Eugè	k1gFnSc2	Eugè
Félix	Félix	k1gInSc1	Félix
Néel	Néel	k1gInSc1	Néel
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
1970	[number]	k4	1970
(	(	kIx(	(
<g/>
*	*	kIx~	*
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
–	–	k?	–
Vjačeslav	Vjačeslav	k1gMnSc1	Vjačeslav
Michajlovič	Michajlovič	k1gMnSc1	Michajlovič
Koťjonočkin	Koťjonočkin	k1gMnSc1	Koťjonočkin
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
ruský	ruský	k2eAgMnSc1d1	ruský
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
malíř	malíř	k1gMnSc1	malíř
(	(	kIx(	(
<g/>
*	*	kIx~	*
20	[number]	k4	20
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
–	–	k?	–
Malcolm	Malcol	k1gNnSc7	Malcol
Bradbury	Bradbura	k1gFnSc2	Bradbura
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgMnSc1d1	anglický
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
*	*	kIx~	*
7	[number]	k4	7
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
–	–	k?	–
Arthur	Arthur	k1gMnSc1	Arthur
Troop	Troop	k1gInSc1	Troop
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgMnSc1d1	anglický
policista	policista	k1gMnSc1	policista
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g />
.	.	kIx.	.
</s>
<s>
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
4	[number]	k4	4
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
Manuel	Manuela	k1gFnPc2	Manuela
Puntillita	Puntillita	k1gFnSc1	Puntillita
Licea	Licea	k1gFnSc1	Licea
<g/>
,	,	kIx,	,
kubánský	kubánský	k2eAgMnSc1d1	kubánský
zpěvák	zpěvák	k1gMnSc1	zpěvák
(	(	kIx(	(
<g/>
*	*	kIx~	*
4	[number]	k4	4
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
Hans	Hans	k1gMnSc1	Hans
Carl	Carl	k1gMnSc1	Carl
Artmann	Artmann	k1gMnSc1	Artmann
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
lyrik	lyrik	k1gMnSc1	lyrik
a	a	k8xC	a
prozaik	prozaik	k1gMnSc1	prozaik
(	(	kIx(	(
<g/>
*	*	kIx~	*
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
11	[number]	k4	11
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
prosince	prosinec	k1gInSc2	prosinec
–	–	k?	–
Nathaniel	Nathaniel	k1gMnSc1	Nathaniel
Richard	Richard	k1gMnSc1	Richard
Nash	Nash	k1gMnSc1	Nash
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
dramatik	dramatik	k1gMnSc1	dramatik
(	(	kIx(	(
<g/>
*	*	kIx~	*
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
–	–	k?	–
Stephen	Stephen	k2eAgMnSc1d1	Stephen
Mitchell	Mitchell	k1gMnSc1	Mitchell
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
psychoanalytik	psychoanalytik	k1gMnSc1	psychoanalytik
(	(	kIx(	(
<g/>
*	*	kIx~	*
23	[number]	k4	23
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
26	[number]	k4	26
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
–	–	k?	–
Jason	Jason	k1gInSc1	Jason
Robards	Robards	k1gInSc1	Robards
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
herec	herec	k1gMnSc1	herec
(	(	kIx(	(
<g/>
*	*	kIx~	*
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
30	[number]	k4	30
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
–	–	k?	–
Bohdan	Bohdan	k1gMnSc1	Bohdan
Warchal	Warchal	k1gMnSc1	Warchal
<g/>
,	,	kIx,	,
slovenský	slovenský	k2eAgMnSc1d1	slovenský
houslista	houslista	k1gMnSc1	houslista
a	a	k8xC	a
dirigent	dirigent	k1gMnSc1	dirigent
(	(	kIx(	(
<g/>
*	*	kIx~	*
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
?	?	kIx.	?
</s>
<s>
–	–	k?	–
Svetlana	Svetlana	k1gFnSc1	Svetlana
Radović	Radović	k1gFnSc1	Radović
<g/>
,	,	kIx,	,
černohorská	černohorský	k2eAgFnSc1d1	černohorská
architektka	architektka	k1gFnSc1	architektka
(	(	kIx(	(
<g/>
*	*	kIx~	*
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
:	:	kIx,	:
Albánie	Albánie	k1gFnSc1	Albánie
–	–	k?	–
Rexhep	Rexhep	k1gMnSc1	Rexhep
Mejdani	Mejdaň	k1gFnSc3	Mejdaň
Belgie	Belgie	k1gFnSc1	Belgie
–	–	k?	–
Albert	Alberta	k1gFnPc2	Alberta
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Belgický	belgický	k2eAgInSc1d1	belgický
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
–	–	k?	–
Alexander	Alexandra	k1gFnPc2	Alexandra
Lukašenko	Lukašenka	k1gFnSc5	Lukašenka
Bulharsko	Bulharsko	k1gNnSc4	Bulharsko
–	–	k?	–
Petar	Petar	k1gInSc1	Petar
Stojanov	Stojanov	k1gInSc1	Stojanov
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
–	–	k?	–
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
Dánsko	Dánsko	k1gNnSc1	Dánsko
–	–	k?	–
Markéta	Markéta	k1gFnSc1	Markéta
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Estonsko	Estonsko	k1gNnSc4	Estonsko
–	–	k?	–
Lennart	Lennart	k1gInSc1	Lennart
Meri	Mer	k1gFnSc2	Mer
Finsko	Finsko	k1gNnSc1	Finsko
–	–	k?	–
Martti	Martť	k1gFnSc2	Martť
Ahtisaari	Ahtisaar	k1gFnSc2	Ahtisaar
Francie	Francie	k1gFnSc2	Francie
–	–	k?	–
Jacques	Jacques	k1gMnSc1	Jacques
Chirac	Chirac	k1gMnSc1	Chirac
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
–	–	k?	–
Stjepan	Stjepany	k1gInPc2	Stjepany
Mesić	Mesić	k1gFnSc2	Mesić
Irsko	Irsko	k1gNnSc1	Irsko
–	–	k?	–
Mary	Mary	k1gFnSc1	Mary
McAleeseová	McAleeseová	k1gFnSc1	McAleeseová
Island	Island	k1gInSc1	Island
–	–	k?	–
Ólafur	Ólafur	k1gMnSc1	Ólafur
Ragnar	Ragnar	k1gMnSc1	Ragnar
Grímsson	Grímsson	k1gMnSc1	Grímsson
Itálie	Itálie	k1gFnSc2	Itálie
–	–	k?	–
Carlo	Carlo	k1gNnSc4	Carlo
Azeglio	Azeglio	k6eAd1	Azeglio
Ciampi	Ciamp	k1gMnSc3	Ciamp
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
–	–	k?	–
Slobodan	Slobodan	k1gMnSc1	Slobodan
Milošević	Milošević	k1gMnSc1	Milošević
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1	Lichtenštejnsko
–	–	k?	–
Hans	Hans	k1gMnSc1	Hans
Adam	Adam	k1gMnSc1	Adam
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Litva	Litva	k1gFnSc1	Litva
–	–	k?	–
Valdas	Valdas	k1gMnSc1	Valdas
Adamkus	Adamkus	k1gMnSc1	Adamkus
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
–	–	k?	–
Vaira	Vaira	k1gFnSc1	Vaira
Vike-Freiberga	Vike-Freiberga	k1gFnSc1	Vike-Freiberga
Lucembursko	Lucembursko	k1gNnSc1	Lucembursko
–	–	k?	–
Jean	Jean	k1gMnSc1	Jean
I.	I.	kA	I.
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
–	–	k?	–
Árpád	Árpáda	k1gFnPc2	Árpáda
Göncz	Göncz	k1gMnSc1	Göncz
<g/>
/	/	kIx~	/
<g/>
Ferenc	Ferenc	k1gMnSc1	Ferenc
Mádl	Mádl	k1gMnSc1	Mádl
Makedonie	Makedonie	k1gFnSc2	Makedonie
–	–	k?	–
Boris	Boris	k1gMnSc1	Boris
Trajkovski	Trajkovsk	k1gFnSc2	Trajkovsk
Malta	Malta	k1gFnSc1	Malta
–	–	k?	–
Guido	Guido	k1gNnSc1	Guido
de	de	k?	de
Marco	Marco	k1gMnSc1	Marco
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
–	–	k?	–
Petru	Petr	k1gMnSc3	Petr
Lucinschi	Lucinsch	k1gMnSc3	Lucinsch
Monako	Monako	k1gNnSc4	Monako
–	–	k?	–
Rainier	Rainira	k1gFnPc2	Rainira
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
–	–	k?	–
Johannes	Johannes	k1gMnSc1	Johannes
Rau	Rau	k1gMnSc1	Rau
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
–	–	k?	–
Beatrix	Beatrix	k1gInSc1	Beatrix
Norsko	Norsko	k1gNnSc1	Norsko
–	–	k?	–
Harald	Harald	k1gMnSc1	Harald
V.	V.	kA	V.
Polsko	Polsko	k1gNnSc1	Polsko
–	–	k?	–
Aleksander	Aleksander	k1gMnSc1	Aleksander
Kwaśniewski	Kwaśniewske	k1gFnSc4	Kwaśniewske
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
–	–	k?	–
Jorge	Jorg	k1gInSc2	Jorg
Sampaio	Sampaio	k6eAd1	Sampaio
Rakousko	Rakousko	k1gNnSc1	Rakousko
–	–	k?	–
Thomas	Thomas	k1gMnSc1	Thomas
Klestil	klestit	k5eAaImAgMnS	klestit
Rumunsko	Rumunsko	k1gNnSc4	Rumunsko
–	–	k?	–
Émile	Émile	k1gInSc1	Émile
Constantinescu	Constantinescus	k1gInSc2	Constantinescus
Rusko	Rusko	k1gNnSc4	Rusko
–	–	k?	–
Vladimir	Vladimir	k1gInSc1	Vladimir
Putin	putin	k2eAgInSc1d1	putin
Řecko	Řecko	k1gNnSc4	Řecko
–	–	k?	–
Konstantinos	Konstantinos	k1gInSc1	Konstantinos
Stefanopulos	Stefanopulosa	k1gFnPc2	Stefanopulosa
Slovensko	Slovensko	k1gNnSc1	Slovensko
–	–	k?	–
Rudolf	Rudolf	k1gMnSc1	Rudolf
Schuster	Schuster	k1gMnSc1	Schuster
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
–	–	k?	–
Milan	Milan	k1gMnSc1	Milan
Kučan	Kučan	k1gMnSc1	Kučan
Španělsko	Španělsko	k1gNnSc1	Španělsko
–	–	k?	–
Juan	Juan	k1gMnSc1	Juan
Carlos	Carlos	k1gMnSc1	Carlos
I.	I.	kA	I.
Švédsko	Švédsko	k1gNnSc1	Švédsko
–	–	k?	–
Karel	Karel	k1gMnSc1	Karel
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
</s>
<s>
Gustav	Gustav	k1gMnSc1	Gustav
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
–	–	k?	–
Adolf	Adolf	k1gMnSc1	Adolf
Ogi	Ogi	k1gMnSc1	Ogi
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
–	–	k?	–
Leonid	Leonida	k1gFnPc2	Leonida
Kučma	kučma	k1gFnSc1	kučma
Vatikán	Vatikán	k1gInSc1	Vatikán
–	–	k?	–
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
–	–	k?	–
Alžběta	Alžběta	k1gFnSc1	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
:	:	kIx,	:
Argentina	Argentina	k1gFnSc1	Argentina
–	–	k?	–
Fernando	Fernanda	k1gFnSc5	Fernanda
de	de	k?	de
la	la	k1gNnSc3	la
Rua	Rua	k1gFnSc2	Rua
Brazílie	Brazílie	k1gFnSc2	Brazílie
–	–	k?	–
Fernando	Fernanda	k1gFnSc5	Fernanda
Henrique	Henriquus	k1gInSc5	Henriquus
Cardoso	Cardosa	k1gFnSc5	Cardosa
Chile	Chile	k1gNnSc2	Chile
–	–	k?	–
Eduardo	Eduardo	k1gNnSc1	Eduardo
Frei	Fre	k1gFnSc2	Fre
Ruíz-Tagle	Ruíz-Tagle	k1gFnSc2	Ruíz-Tagle
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
–	–	k?	–
Andrés	Andrés	k1gInSc1	Andrés
Pastrana	Pastrana	k1gFnSc1	Pastrana
Arango	Arango	k1gMnSc1	Arango
Kuba	Kuba	k1gMnSc1	Kuba
–	–	k?	–
Fidel	Fidel	k1gMnSc1	Fidel
Castro	Castro	k1gNnSc1	Castro
Mexiko	Mexiko	k1gNnSc1	Mexiko
–	–	k?	–
Ernesto	Ernesta	k1gMnSc5	Ernesta
Zedillo	Zedilla	k1gMnSc5	Zedilla
Ponce	Ponce	k1gMnSc5	Ponce
Peru	Peru	k1gNnSc7	Peru
–	–	k?	–
Alberto	Alberta	k1gFnSc5	Alberta
Fujimori	Fujimor	k1gInSc6	Fujimor
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
americké	americký	k2eAgFnSc2d1	americká
–	–	k?	–
Bill	Bill	k1gMnSc1	Bill
Clinton	Clinton	k1gMnSc1	Clinton
Venezuela	Venezuela	k1gFnSc1	Venezuela
–	–	k?	–
Hugo	Hugo	k1gMnSc1	Hugo
Rafael	Rafael	k1gMnSc1	Rafael
Chávez	Chávez	k1gMnSc1	Chávez
Frías	Frías	k1gMnSc1	Frías
Afrika	Afrika	k1gFnSc1	Afrika
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
Alžírsko	Alžírsko	k1gNnSc1	Alžírsko
–	–	k?	–
Abdelazíz	Abdelazíza	k1gFnPc2	Abdelazíza
Buteflika	Buteflika	k1gFnSc1	Buteflika
Egypt	Egypt	k1gInSc1	Egypt
–	–	k?	–
Muhammad	Muhammad	k1gInSc1	Muhammad
Husní	Husní	k2eAgMnSc1d1	Husní
Mubarak	Mubarak	k1gMnSc1	Mubarak
Guinea	guinea	k1gFnSc2	guinea
–	–	k?	–
Lansana	Lansana	k1gFnSc1	Lansana
Conté	Contý	k2eAgFnPc1d1	Contý
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
republika	republika	k1gFnSc1	republika
–	–	k?	–
Thabo	Thaba	k1gFnSc5	Thaba
Mbeki	Mbek	k1gFnSc6	Mbek
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
Kongo	Kongo	k1gNnSc1	Kongo
–	–	k?	–
Laurent-Désiré	Laurent-Désirý	k2eAgFnSc2d1	Laurent-Désirý
Kabila	Kabila	k1gFnSc1	Kabila
Maroko	Maroko	k1gNnSc1	Maroko
–	–	k?	–
Mohammed	Mohammed	k1gMnSc1	Mohammed
VI	VI	kA	VI
Mosambik	Mosambik	k1gInSc1	Mosambik
–	–	k?	–
Joaquim	Joaquim	k1gInSc1	Joaquim
Chissano	Chissana	k1gFnSc5	Chissana
Nigérie	Nigérie	k1gFnSc1	Nigérie
–	–	k?	–
Olusegun	Olusegun	k1gInSc1	Olusegun
Obasanjo	Obasanjo	k6eAd1	Obasanjo
Somálsko	Somálsko	k1gNnSc4	Somálsko
–	–	k?	–
Abdiqasim	Abdiqasim	k1gInSc1	Abdiqasim
Salad	Salad	k1gInSc1	Salad
Hassan	Hassan	k1gInSc4	Hassan
(	(	kIx(	(
<g/>
formálně	formálně	k6eAd1	formálně
<g/>
,	,	kIx,	,
reálně	reálně	k6eAd1	reálně
jen	jen	k9	jen
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
Tunisko	Tunisko	k1gNnSc1	Tunisko
–	–	k?	–
Adolf	Adolf	k1gMnSc1	Adolf
Ogi	Ogi	k1gMnSc2	Ogi
Asie	Asie	k1gFnSc2	Asie
<g/>
:	:	kIx,	:
Arménie	Arménie	k1gFnSc2	Arménie
–	–	k?	–
Robert	Robert	k1gMnSc1	Robert
Kočarjan	Kočarjan	k1gMnSc1	Kočarjan
Gruzie	Gruzie	k1gFnSc2	Gruzie
–	–	k?	–
Eduard	Eduard	k1gMnSc1	Eduard
Ševardnadze	Ševardnadze	k1gFnSc2	Ševardnadze
Čína	Čína	k1gFnSc1	Čína
–	–	k?	–
Ťiang	Ťiang	k1gInSc1	Ťiang
Ce-min	Cein	k1gInSc1	Ce-min
Indie	Indie	k1gFnSc2	Indie
–	–	k?	–
Kočeril	Kočeril	k1gMnSc1	Kočeril
Rámán	Rámán	k1gMnSc1	Rámán
Nárájanan	Nárájanan	k1gMnSc1	Nárájanan
Indonésie	Indonésie	k1gFnSc2	Indonésie
–	–	k?	–
Abdurrahnam	Abdurrahnam	k1gInSc1	Abdurrahnam
Wahid	Wahid	k1gInSc1	Wahid
Irák	Irák	k1gInSc1	Irák
–	–	k?	–
Saddám	Saddám	k1gMnSc1	Saddám
Husajn	Husajn	k1gMnSc1	Husajn
Írán	Írán	k1gInSc1	Írán
–	–	k?	–
Muhammad	Muhammad	k1gInSc1	Muhammad
Chátamí	Chátamí	k1gNnSc1	Chátamí
Izrael	Izrael	k1gInSc1	Izrael
–	–	k?	–
Ezer	Ezer	k1gMnSc1	Ezer
Weizman	Weizman	k1gMnSc1	Weizman
Japonsko	Japonsko	k1gNnSc1	Japonsko
–	–	k?	–
Akihito	Akihit	k2eAgNnSc1d1	Akihito
Jižní	jižní	k2eAgNnSc1d1	jižní
<g />
.	.	kIx.	.
</s>
<s>
Korea	Korea	k1gFnSc1	Korea
–	–	k?	–
Kim	Kim	k1gFnSc1	Kim
Taë-džung	Taëžung	k1gMnSc1	Taë-džung
Kypr	Kypr	k1gInSc1	Kypr
–	–	k?	–
Glafkos	Glafkos	k1gInSc1	Glafkos
Kleridis	Kleridis	k1gInSc1	Kleridis
Pákistán	Pákistán	k1gInSc4	Pákistán
–	–	k?	–
Muhammad	Muhammad	k1gInSc1	Muhammad
Rafík	Rafík	k1gMnSc1	Rafík
Tarar	Tarar	k1gInSc1	Tarar
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
–	–	k?	–
Fahd	Fahd	k1gMnSc1	Fahd
ibn	ibn	k?	ibn
Abdul	Abdul	k1gMnSc1	Abdul
Azíz	Azíz	k1gMnSc1	Azíz
al-Saúd	al-Saúd	k1gMnSc1	al-Saúd
Tádžikistán	Tádžikistán	k1gInSc1	Tádžikistán
–	–	k?	–
Emomali	Emomali	k1gFnSc1	Emomali
Rachmanov	Rachmanov	k1gInSc1	Rachmanov
Turecko	Turecko	k1gNnSc1	Turecko
–	–	k?	–
Süleyman	Süleyman	k1gMnSc1	Süleyman
Demirel	Demirel	k1gMnSc1	Demirel
Turkmenistán	Turkmenistán	k1gInSc1	Turkmenistán
–	–	k?	–
Saparmurat	Saparmurat	k1gInSc1	Saparmurat
Nijazov	Nijazov	k1gInSc1	Nijazov
Uzbekistán	Uzbekistán	k1gInSc1	Uzbekistán
–	–	k?	–
Islom	Islom	k1gInSc1	Islom
Karimov	Karimov	k1gInSc1	Karimov
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
:	:	kIx,	:
Austrálie	Austrálie	k1gFnSc2	Austrálie
–	–	k?	–
John	John	k1gMnSc1	John
Howard	Howard	k1gMnSc1	Howard
Nový	nový	k2eAgInSc4d1	nový
Zéland	Zéland	k1gInSc4	Zéland
–	–	k?	–
Helen	Helena	k1gFnPc2	Helena
Clarková	Clarková	k1gFnSc1	Clarková
Galerie	galerie	k1gFnSc1	galerie
2000	[number]	k4	2000
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc4	Commons
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
2000	[number]	k4	2000
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
