<p>
<s>
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
(	(	kIx(	(
<g/>
bulharsky	bulharsky	k6eAd1	bulharsky
Б	Б	k?	Б
/	/	kIx~	/
Bălgarija	Bălgarija	k1gMnSc1	Bălgarija
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaImF	stát
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
na	na	k7c6	na
Balkánském	balkánský	k2eAgInSc6d1	balkánský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
sousedí	sousedit	k5eAaImIp3nS	sousedit
na	na	k7c6	na
severu	sever	k1gInSc6	sever
s	s	k7c7	s
Rumunskem	Rumunsko	k1gNnSc7	Rumunsko
(	(	kIx(	(
<g/>
hranici	hranice	k1gFnSc4	hranice
tvoří	tvořit	k5eAaImIp3nS	tvořit
převážně	převážně	k6eAd1	převážně
Dunaj	Dunaj	k1gInSc1	Dunaj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
se	s	k7c7	s
Srbskem	Srbsko	k1gNnSc7	Srbsko
a	a	k8xC	a
Severní	severní	k2eAgFnSc7d1	severní
Makedonií	Makedonie	k1gFnSc7	Makedonie
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
Řeckem	Řecko	k1gNnSc7	Řecko
a	a	k8xC	a
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
s	s	k7c7	s
Tureckem	Turecko	k1gNnSc7	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgFnSc4d1	východní
hranici	hranice	k1gFnSc4	hranice
tvoří	tvořit	k5eAaImIp3nP	tvořit
Černé	Černé	k2eAgNnSc4d1	Černé
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
a	a	k8xC	a
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Sofie	Sofie	k1gFnSc1	Sofie
<g/>
,	,	kIx,	,
s	s	k7c7	s
1,2	[number]	k4	1,2
miliony	milion	k4xCgInPc1	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
představují	představovat	k5eAaImIp3nP	představovat
17	[number]	k4	17
procent	procento	k1gNnPc2	procento
celkové	celkový	k2eAgFnSc2d1	celková
populace	populace	k1gFnSc2	populace
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
důležitými	důležitý	k2eAgInPc7d1	důležitý
městy	město	k1gNnPc7	město
jsou	být	k5eAaImIp3nP	být
Plovdiv	Plovdiv	k1gInSc1	Plovdiv
<g/>
,	,	kIx,	,
Varna	Varna	k1gFnSc1	Varna
a	a	k8xC	a
Burgas	Burgas	k1gInSc1	Burgas
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
110	[number]	k4	110
994	[number]	k4	994
kilometrů	kilometr	k1gInPc2	kilometr
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
je	být	k5eAaImIp3nS	být
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
16	[number]	k4	16
<g/>
.	.	kIx.	.
největší	veliký	k2eAgFnSc7d3	veliký
zemí	zem	k1gFnSc7	zem
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
11	[number]	k4	11
<g/>
.	.	kIx.	.
největší	veliký	k2eAgInSc1d3	veliký
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
má	mít	k5eAaImIp3nS	mít
velkou	velký	k2eAgFnSc4d1	velká
tradici	tradice	k1gFnSc4	tradice
státnosti	státnost	k1gFnSc2	státnost
<g/>
,	,	kIx,	,
první	první	k4xOgInSc1	první
bulharský	bulharský	k2eAgInSc1d1	bulharský
stát	stát	k1gInSc1	stát
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
681	[number]	k4	681
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
svou	svůj	k3xOyFgFnSc4	svůj
samostatnost	samostatnost	k1gFnSc4	samostatnost
však	však	k9	však
muselo	muset	k5eAaImAgNnS	muset
po	po	k7c6	po
staletí	staletí	k1gNnSc6	staletí
svádět	svádět	k5eAaImF	svádět
tuhý	tuhý	k2eAgInSc4d1	tuhý
boj	boj	k1gInSc4	boj
s	s	k7c7	s
mocnými	mocný	k2eAgMnPc7d1	mocný
sousedy	soused	k1gMnPc7	soused
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
s	s	k7c7	s
Byzancí	Byzanc	k1gFnSc7	Byzanc
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
s	s	k7c7	s
Osmanskou	osmanský	k2eAgFnSc7d1	Osmanská
říší	říš	k1gFnSc7	říš
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
stát	stát	k1gInSc1	stát
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
roku	rok	k1gInSc2	rok
1878	[number]	k4	1878
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
monarchie	monarchie	k1gFnSc1	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
bylo	být	k5eAaImAgNnS	být
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
republikou	republika	k1gFnSc7	republika
a	a	k8xC	a
součástí	součást	k1gFnSc7	součást
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
<g/>
.	.	kIx.	.
</s>
<s>
Komunistický	komunistický	k2eAgInSc1d1	komunistický
režim	režim	k1gInSc1	režim
byl	být	k5eAaImAgInS	být
svržen	svrhnout	k5eAaPmNgInS	svrhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
Bulhaři	Bulhar	k1gMnPc1	Bulhar
rozvíjí	rozvíjet	k5eAaImIp3nP	rozvíjet
demokracii	demokracie	k1gFnSc4	demokracie
a	a	k8xC	a
volný	volný	k2eAgInSc4d1	volný
trh	trh	k1gInSc4	trh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
země	země	k1gFnSc1	země
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
NATO	NATO	kA	NATO
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2007	[number]	k4	2007
do	do	k7c2	do
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
je	být	k5eAaImIp3nS	být
unitární	unitární	k2eAgInSc4d1	unitární
stát	stát	k1gInSc4	stát
a	a	k8xC	a
parlamentní	parlamentní	k2eAgFnSc1d1	parlamentní
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
zažívá	zažívat	k5eAaImIp3nS	zažívat
populační	populační	k2eAgInSc4d1	populační
pokles	pokles	k1gInSc4	pokles
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
značným	značný	k2eAgInSc7d1	značný
politickým	politický	k2eAgInSc7d1	politický
problémem	problém	k1gInSc7	problém
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2017	[number]	k4	2017
činil	činit	k5eAaImAgInS	činit
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
7	[number]	k4	7
050	[number]	k4	050
0	[number]	k4	0
<g/>
34	[number]	k4	34
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ještě	ještě	k9	ještě
na	na	k7c6	na
konci	konec	k1gInSc6	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
atakoval	atakovat	k5eAaBmAgInS	atakovat
devítimilionovou	devítimilionový	k2eAgFnSc4d1	devítimilionová
hranici	hranice	k1gFnSc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
významná	významný	k2eAgFnSc1d1	významná
turecká	turecký	k2eAgFnSc1d1	turecká
a	a	k8xC	a
romská	romský	k2eAgFnSc1d1	romská
menšina	menšina	k1gFnSc1	menšina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Rané	raný	k2eAgFnPc1d1	raná
dějiny	dějiny	k1gFnPc1	dějiny
===	===	k?	===
</s>
</p>
<p>
<s>
Z	z	k7c2	z
období	období	k1gNnSc2	období
150	[number]	k4	150
000	[number]	k4	000
let	léto	k1gNnPc2	léto
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
pocházejí	pocházet	k5eAaImIp3nP	pocházet
stopy	stopa	k1gFnPc4	stopa
po	po	k7c6	po
komunitách	komunita	k1gFnPc6	komunita
neandrtálců	neandrtálec	k1gMnPc2	neandrtálec
<g/>
.	.	kIx.	.
</s>
<s>
Karanovská	Karanovský	k2eAgFnSc1d1	Karanovský
kultura	kultura	k1gFnSc1	kultura
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
archeologové	archeolog	k1gMnPc1	archeolog
umísťují	umísťovat	k5eAaImIp3nP	umísťovat
do	do	k7c2	do
období	období	k1gNnSc2	období
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
6	[number]	k4	6
500	[number]	k4	500
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
dospěla	dochvít	k5eAaPmAgFnS	dochvít
k	k	k7c3	k
zemědělství	zemědělství	k1gNnSc3	zemědělství
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
neolitických	neolitický	k2eAgFnPc2d1	neolitická
kultur	kultura	k1gFnPc2	kultura
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
něco	něco	k3yInSc4	něco
mladší	mladý	k2eAgFnSc1d2	mladší
kultura	kultura	k1gFnSc1	kultura
varnská	varnská	k1gFnSc1	varnská
(	(	kIx(	(
<g/>
asi	asi	k9	asi
4400-4100	[number]	k4	4400-4100
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
pak	pak	k6eAd1	pak
ke	k	k7c3	k
zpracování	zpracování	k1gNnSc3	zpracování
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
ba	ba	k9	ba
dokonce	dokonce	k9	dokonce
patrně	patrně	k6eAd1	patrně
právě	právě	k9	právě
tato	tento	k3xDgFnSc1	tento
kultura	kultura	k1gFnSc1	kultura
jako	jako	k8xS	jako
první	první	k4xOgFnSc1	první
na	na	k7c6	na
světě	svět	k1gInSc6	svět
začala	začít	k5eAaPmAgFnS	začít
zpracovávat	zpracovávat	k5eAaImF	zpracovávat
zlato	zlato	k1gNnSc4	zlato
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hrobech	hrob	k1gInPc6	hrob
blízko	blízko	k7c2	blízko
Varny	Varna	k1gFnSc2	Varna
se	se	k3xPyFc4	se
našly	najít	k5eAaPmAgInP	najít
nejstarší	starý	k2eAgInPc1d3	nejstarší
zlaté	zlatý	k2eAgInPc1d1	zlatý
šperky	šperk	k1gInPc1	šperk
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejstarším	starý	k2eAgInSc7d3	nejstarší
známým	známý	k2eAgInSc7d1	známý
kmenem	kmen	k1gInSc7	kmen
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
byli	být	k5eAaImAgMnP	být
Thrákové	Thrák	k1gMnPc1	Thrák
<g/>
.	.	kIx.	.
</s>
<s>
Působili	působit	k5eAaImAgMnP	působit
na	na	k7c6	na
území	území	k1gNnSc6	území
současného	současný	k2eAgNnSc2d1	současné
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
stali	stát	k5eAaPmAgMnP	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
tří	tři	k4xCgNnPc2	tři
etnik	etnikum	k1gNnPc2	etnikum
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
byl	být	k5eAaImAgInS	být
složen	složit	k5eAaPmNgInS	složit
bulharský	bulharský	k2eAgInSc1d1	bulharský
národ	národ	k1gInSc1	národ
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
turkickými	turkický	k2eAgMnPc7d1	turkický
Protobulhary	Protobulhar	k1gMnPc7	Protobulhar
a	a	k8xC	a
Slovany	Slovan	k1gMnPc7	Slovan
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
vznikaly	vznikat	k5eAaImAgFnP	vznikat
na	na	k7c6	na
černomořském	černomořský	k2eAgNnSc6d1	černomořské
pobřeží	pobřeží	k1gNnSc6	pobřeží
početné	početný	k2eAgFnSc2d1	početná
řecké	řecký	k2eAgFnSc2d1	řecká
osady	osada	k1gFnSc2	osada
<g/>
.	.	kIx.	.
</s>
<s>
Perská	perský	k2eAgFnSc1d1	perská
Achaimenovská	Achaimenovský	k2eAgFnSc1d1	Achaimenovský
říše	říše	k1gFnSc1	říše
dobyla	dobýt	k5eAaPmAgFnS	dobýt
většinu	většina	k1gFnSc4	většina
současného	současný	k2eAgNnSc2d1	současné
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
udržovala	udržovat	k5eAaImAgFnS	udržovat
si	se	k3xPyFc3	se
nad	nad	k7c7	nad
tímto	tento	k3xDgNnSc7	tento
územím	území	k1gNnSc7	území
kontrolu	kontrola	k1gFnSc4	kontrola
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
479	[number]	k4	479
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Odpor	odpor	k1gInSc4	odpor
k	k	k7c3	k
Peršanům	Peršan	k1gMnPc3	Peršan
Thráky	Thrák	k1gMnPc4	Thrák
sjednotil	sjednotit	k5eAaPmAgMnS	sjednotit
a	a	k8xC	a
vedeni	vést	k5eAaImNgMnP	vést
Teresem	Teres	k1gInSc7	Teres
I.	I.	kA	I.
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
Odryské	Odryské	k1gNnSc4	Odryské
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc3	rok
341	[number]	k4	341
ovládnuto	ovládnout	k5eAaPmNgNnS	ovládnout
Filipem	Filip	k1gMnSc7	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Makedonským	makedonský	k2eAgInSc7d1	makedonský
<g/>
,	,	kIx,	,
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
napadeno	napadnout	k5eAaPmNgNnS	napadnout
Kelty	Kelt	k1gMnPc7	Kelt
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
roku	rok	k1gInSc2	rok
45	[number]	k4	45
stalo	stát	k5eAaPmAgNnS	stát
součástí	součást	k1gFnSc7	součást
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
provincie	provincie	k1gFnSc1	provincie
Thrákie	Thrákie	k1gFnSc1	Thrákie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
římské	římský	k2eAgFnSc6d1	římská
osadě	osada	k1gFnSc6	osada
Nikopolis	Nikopolis	k1gFnSc2	Nikopolis
ad	ad	k7c4	ad
Istrum	Istrum	k1gInSc4	Istrum
(	(	kIx(	(
<g/>
asi	asi	k9	asi
30	[number]	k4	30
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
Veliko	Velika	k1gFnSc5	Velika
Tarnovo	Tarnův	k2eAgNnSc1d1	Tarnovo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
působil	působit	k5eAaImAgMnS	působit
křesťanský	křesťanský	k2eAgMnSc1d1	křesťanský
ariánský	ariánský	k2eAgMnSc1d1	ariánský
kněz	kněz	k1gMnSc1	kněz
Wulfila	Wulfil	k1gMnSc2	Wulfil
a	a	k8xC	a
přeložil	přeložit	k5eAaPmAgMnS	přeložit
zde	zde	k6eAd1	zde
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
Bible	bible	k1gFnSc2	bible
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
do	do	k7c2	do
gótštiny	gótština	k1gFnSc2	gótština
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
476	[number]	k4	476
<g/>
,	,	kIx,	,
oblast	oblast	k1gFnSc1	oblast
ovládala	ovládat	k5eAaImAgFnS	ovládat
Byzanc	Byzanc	k1gFnSc4	Byzanc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vytížena	vytížen	k2eAgFnSc1d1	vytížena
boji	boj	k1gInSc6	boj
s	s	k7c7	s
Peršany	Peršan	k1gMnPc7	Peršan
nedokázala	dokázat	k5eNaPmAgFnS	dokázat
zabránit	zabránit	k5eAaPmF	zabránit
vpádu	vpád	k1gInSc2	vpád
nových	nový	k2eAgInPc2d1	nový
kmenů	kmen	k1gInPc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
tudíž	tudíž	k8xC	tudíž
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
na	na	k7c4	na
území	území	k1gNnSc4	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
první	první	k4xOgMnPc1	první
Slované	Slovan	k1gMnPc1	Slovan
<g/>
,	,	kIx,	,
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
pak	pak	k6eAd1	pak
kočovní	kočovní	k2eAgMnPc1d1	kočovní
turkičtí	turkický	k2eAgMnPc1d1	turkický
Protobulhaři	Protobulhař	k1gMnPc1	Protobulhař
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
přišli	přijít	k5eAaPmAgMnP	přijít
z	z	k7c2	z
útvaru	útvar	k1gInSc2	útvar
zvaného	zvaný	k2eAgInSc2d1	zvaný
Velké	velký	k2eAgNnSc4d1	velké
Bulharsko	Bulharsko	k1gNnSc4	Bulharsko
(	(	kIx(	(
<g/>
též	též	k9	též
Onogurský	Onogurský	k2eAgInSc1d1	Onogurský
chanát	chanát	k1gInSc1	chanát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
založil	založit	k5eAaPmAgMnS	založit
kagan	kagan	k1gMnSc1	kagan
Kuvrat	Kuvrat	k1gMnSc1	Kuvrat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
leželo	ležet	k5eAaImAgNnS	ležet
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Vůdcem	vůdce	k1gMnSc7	vůdce
těchto	tento	k3xDgInPc2	tento
migrujících	migrující	k2eAgInPc2d1	migrující
Protobulharů	Protobulhar	k1gInPc2	Protobulhar
byl	být	k5eAaImAgMnS	být
Asparuch	Asparuch	k1gMnSc1	Asparuch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgFnSc1	první
bulharská	bulharský	k2eAgFnSc1d1	bulharská
říše	říše	k1gFnSc1	říše
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
bulharský	bulharský	k2eAgInSc1d1	bulharský
stát	stát	k1gInSc1	stát
založil	založit	k5eAaPmAgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
681	[number]	k4	681
chán	chán	k1gMnSc1	chán
Asparuch	Asparuch	k1gMnSc1	Asparuch
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
uzavřel	uzavřít	k5eAaPmAgMnS	uzavřít
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
byzantským	byzantský	k2eAgMnSc7d1	byzantský
císařem	císař	k1gMnSc7	císař
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
bylo	být	k5eAaImAgNnS	být
Protobulharům	Protobulhar	k1gInPc3	Protobulhar
(	(	kIx(	(
<g/>
vládnoucí	vládnoucí	k2eAgFnSc3d1	vládnoucí
menšině	menšina	k1gFnSc3	menšina
<g/>
)	)	kIx)	)
a	a	k8xC	a
Slovanům	Slovan	k1gMnPc3	Slovan
(	(	kIx(	(
<g/>
ovládané	ovládaný	k2eAgFnSc6d1	ovládaná
většině	většina	k1gFnSc6	většina
<g/>
)	)	kIx)	)
dovoleno	dovolit	k5eAaPmNgNnS	dovolit
sídlit	sídlit	k5eAaImF	sídlit
v	v	k7c6	v
Moesii	Moesie	k1gFnSc6	Moesie
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
bulharského	bulharský	k2eAgInSc2d1	bulharský
státu	stát	k1gInSc2	stát
byla	být	k5eAaImAgFnS	být
Pliska	Pliska	k?	Pliska
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
Kruma	Krumum	k1gNnSc2	Krumum
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
zdvojnásobilo	zdvojnásobit	k5eAaPmAgNnS	zdvojnásobit
své	své	k1gNnSc4	své
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Krum	Krum	k1gMnSc1	Krum
též	též	k9	též
nechal	nechat	k5eAaPmAgMnS	nechat
sepsat	sepsat	k5eAaPmF	sepsat
první	první	k4xOgInSc1	první
zákoník	zákoník	k1gInSc1	zákoník
a	a	k8xC	a
zastavil	zastavit	k5eAaPmAgInS	zastavit
byzantskou	byzantský	k2eAgFnSc4d1	byzantská
invazi	invaze	k1gFnSc4	invaze
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Plisky	Plisky	k?	Plisky
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
byl	být	k5eAaImAgMnS	být
zabit	zabit	k2eAgMnSc1d1	zabit
byzantský	byzantský	k2eAgMnSc1d1	byzantský
císař	císař	k1gMnSc1	císař
Nikeforos	Nikeforosa	k1gFnPc2	Nikeforosa
I.	I.	kA	I.
Král	Král	k1gMnSc1	Král
Omurtag	Omurtag	k1gMnSc1	Omurtag
začal	začít	k5eAaPmAgMnS	začít
politicky	politicky	k6eAd1	politicky
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c4	na
spojení	spojení	k1gNnSc4	spojení
Protobulharů	Protobulhar	k1gInPc2	Protobulhar
a	a	k8xC	a
Slovanů	Slovan	k1gInPc2	Slovan
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Symeon	Symeon	k1gInSc1	Symeon
I.	I.	kA	I.
<g/>
,	,	kIx,	,
za	za	k7c2	za
jehož	jehož	k3xOyRp3gFnSc2	jehož
vlády	vláda	k1gFnSc2	vláda
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
bulharský	bulharský	k2eAgInSc1d1	bulharský
stát	stát	k1gInSc1	stát
největšího	veliký	k2eAgInSc2d3	veliký
rozmachu	rozmach	k1gInSc2	rozmach
a	a	k8xC	a
politického	politický	k2eAgInSc2d1	politický
významu	význam	k1gInSc2	význam
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
dílo	dílo	k1gNnSc4	dílo
dokončil	dokončit	k5eAaPmAgMnS	dokončit
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
Bulharům	Bulhar	k1gMnPc3	Bulhar
natrvalo	natrvalo	k6eAd1	natrvalo
slovanský	slovanský	k2eAgMnSc1d1	slovanský
jazyk	jazyk	k1gMnSc1	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
za	za	k7c2	za
Borise	Boris	k1gMnSc2	Boris
I.	I.	kA	I.
<g/>
,	,	kIx,	,
přijali	přijmout	k5eAaPmAgMnP	přijmout
Bulhaři	Bulhar	k1gMnPc1	Bulhar
křesťanství	křesťanství	k1gNnSc2	křesťanství
jako	jako	k9	jako
státní	státní	k2eAgNnSc1d1	státní
náboženství	náboženství	k1gNnSc1	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
kulturní	kulturní	k2eAgNnSc4d1	kulturní
klima	klima	k1gNnSc4	klima
a	a	k8xC	a
náboženské	náboženský	k2eAgInPc4d1	náboženský
obyčeje	obyčej	k1gInPc4	obyčej
značně	značně	k6eAd1	značně
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
bogomilství	bogomilství	k1gNnSc1	bogomilství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
také	také	k9	také
zesílil	zesílit	k5eAaPmAgInS	zesílit
tlak	tlak	k1gInSc1	tlak
Byzance	Byzanc	k1gFnSc2	Byzanc
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgMnSc7d1	poslední
panovníkem	panovník	k1gMnSc7	panovník
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
dokázal	dokázat	k5eAaPmAgMnS	dokázat
ještě	ještě	k6eAd1	ještě
čelit	čelit	k5eAaImF	čelit
jejímu	její	k3xOp3gInSc3	její
náporu	nápor	k1gInSc3	nápor
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Samuel	Samuel	k1gMnSc1	Samuel
I.	I.	kA	I.
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
první	první	k4xOgNnSc4	první
bulharské	bulharský	k2eAgNnSc4d1	bulharské
carství	carství	k1gNnSc4	carství
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
(	(	kIx(	(
<g/>
1018	[number]	k4	1018
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
území	území	k1gNnSc1	území
bylo	být	k5eAaImAgNnS	být
začleněno	začlenit	k5eAaPmNgNnS	začlenit
do	do	k7c2	do
Byzantské	byzantský	k2eAgFnSc2d1	byzantská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Druhá	druhý	k4xOgFnSc1	druhý
bulharská	bulharský	k2eAgFnSc1d1	bulharská
říše	říše	k1gFnSc1	říše
===	===	k?	===
</s>
</p>
<p>
<s>
Bulhaři	Bulhar	k1gMnPc1	Bulhar
se	se	k3xPyFc4	se
několikrát	několikrát	k6eAd1	několikrát
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
byzantské	byzantský	k2eAgFnSc3d1	byzantská
nadvládě	nadvláda	k1gFnSc3	nadvláda
vzepřít	vzepřít	k5eAaPmF	vzepřít
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc4d3	veliký
vzpouru	vzpoura	k1gFnSc4	vzpoura
vedl	vést	k5eAaImAgMnS	vést
roku	rok	k1gInSc2	rok
1040	[number]	k4	1040
Peter	Peter	k1gMnSc1	Peter
Deljan	Deljan	k1gMnSc1	Deljan
<g/>
.	.	kIx.	.
</s>
<s>
Samostatnost	samostatnost	k1gFnSc4	samostatnost
získalo	získat	k5eAaPmAgNnS	získat
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
ovšem	ovšem	k9	ovšem
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1185	[number]	k4	1185
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
povstání	povstání	k1gNnSc2	povstání
bratrů	bratr	k1gMnPc2	bratr
Asena	Asen	k1gMnSc2	Asen
<g/>
,	,	kIx,	,
Petra	Petr	k1gMnSc2	Petr
a	a	k8xC	a
Kalojana	Kalojan	k1gMnSc2	Kalojan
<g/>
,	,	kIx,	,
budoucích	budoucí	k2eAgMnPc2d1	budoucí
panovníků	panovník	k1gMnPc2	panovník
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
Druhá	druhý	k4xOgFnSc1	druhý
Bulharská	bulharský	k2eAgFnSc1d1	bulharská
říše	říše	k1gFnSc1	říše
s	s	k7c7	s
centrem	centrum	k1gNnSc7	centrum
ve	v	k7c6	v
Velikém	veliký	k2eAgInSc6d1	veliký
Tarnovu	Tarnův	k2eAgMnSc3d1	Tarnův
<g/>
.	.	kIx.	.
</s>
<s>
Kalojan	Kalojan	k1gInSc1	Kalojan
mj.	mj.	kA	mj.
dobyl	dobýt	k5eAaPmAgInS	dobýt
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
a	a	k8xC	a
uznal	uznat	k5eAaPmAgInS	uznat
svrchovanost	svrchovanost	k1gFnSc4	svrchovanost
římského	římský	k2eAgMnSc2d1	římský
papeže	papež	k1gMnSc2	papež
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholu	vrchol	k1gInSc3	vrchol
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
říše	říše	k1gFnSc1	říše
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
cara	car	k1gMnSc2	car
Ivana	Ivan	k1gMnSc2	Ivan
Asena	Asen	k1gMnSc2	Asen
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1218	[number]	k4	1218
<g/>
–	–	k?	–
<g/>
1241	[number]	k4	1241
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ji	on	k3xPp3gFnSc4	on
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
o	o	k7c4	o
Albánii	Albánie	k1gFnSc4	Albánie
a	a	k8xC	a
část	část	k1gFnSc4	část
Řecka	Řecko	k1gNnSc2	Řecko
(	(	kIx(	(
<g/>
Epirus	Epirus	k1gInSc1	Epirus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
náboženských	náboženský	k2eAgFnPc6d1	náboženská
záležitostech	záležitost	k1gFnPc6	záležitost
se	se	k3xPyFc4	se
odklonil	odklonit	k5eAaPmAgMnS	odklonit
od	od	k7c2	od
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1257	[number]	k4	1257
říši	říš	k1gFnSc6	říš
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Mongolové	Mongol	k1gMnPc1	Mongol
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
nim	on	k3xPp3gFnPc3	on
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
selské	selský	k2eAgNnSc4d1	selské
povstání	povstání	k1gNnSc4	povstání
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
vedl	vést	k5eAaImAgMnS	vést
pastýř	pastýř	k1gMnSc1	pastýř
Ivajlo	Ivajlo	k1gNnSc4	Ivajlo
<g/>
.	.	kIx.	.
</s>
<s>
Mongoly	mongol	k1gInPc1	mongol
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
vyhnat	vyhnat	k5eAaPmF	vyhnat
a	a	k8xC	a
Ivajlo	Ivajlo	k1gNnSc4	Ivajlo
usedl	usednout	k5eAaPmAgMnS	usednout
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
však	však	k9	však
nesmířili	smířit	k5eNaPmAgMnP	smířit
šlechtici	šlechtic	k1gMnPc1	šlechtic
<g/>
,	,	kIx,	,
cara	car	k1gMnSc2	car
svrhli	svrhnout	k5eAaPmAgMnP	svrhnout
a	a	k8xC	a
říše	říše	k1gFnSc1	říše
se	se	k3xPyFc4	se
rozdrobila	rozdrobit	k5eAaPmAgFnS	rozdrobit
na	na	k7c4	na
malá	malý	k2eAgNnPc4d1	malé
feudální	feudální	k2eAgNnPc4d1	feudální
panství	panství	k1gNnPc4	panství
<g/>
.	.	kIx.	.
</s>
<s>
Car	car	k1gMnSc1	car
Ivan	Ivan	k1gMnSc1	Ivan
Alexandr	Alexandr	k1gMnSc1	Alexandr
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
dostal	dostat	k5eAaPmAgMnS	dostat
pod	pod	k7c4	pod
tlak	tlak	k1gInSc4	tlak
nového	nový	k2eAgMnSc2d1	nový
mocného	mocný	k2eAgMnSc2d1	mocný
souseda	soused	k1gMnSc2	soused
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
<g/>
,	,	kIx,	,
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Sultán	sultán	k1gMnSc1	sultán
Murad	Murad	k1gInSc1	Murad
I.	I.	kA	I.
dobyl	dobýt	k5eAaPmAgInS	dobýt
roku	rok	k1gInSc2	rok
1382	[number]	k4	1382
Sofii	Sofia	k1gFnSc6	Sofia
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pádem	pád	k1gInSc7	pád
Vidinu	vidina	k1gFnSc4	vidina
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1396	[number]	k4	1396
bylo	být	k5eAaImAgNnS	být
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
do	do	k7c2	do
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
začleněno	začlenit	k5eAaPmNgNnS	začlenit
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
téměř	téměř	k6eAd1	téměř
pět	pět	k4xCc4	pět
dalších	další	k2eAgNnPc2d1	další
století	století	k1gNnPc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bulharská	bulharský	k2eAgFnSc1d1	bulharská
šlechta	šlechta	k1gFnSc1	šlechta
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
<g/>
,	,	kIx,	,
rolnictvo	rolnictvo	k1gNnSc1	rolnictvo
podrobeno	podroben	k2eAgNnSc1d1	podrobeno
osmanským	osmanský	k2eAgMnPc3d1	osmanský
vládcům	vládce	k1gMnPc3	vládce
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c1	mnoho
vzdělaných	vzdělaný	k2eAgMnPc2d1	vzdělaný
duchovních	duchovní	k1gMnPc2	duchovní
uprchlo	uprchnout	k5eAaPmAgNnS	uprchnout
do	do	k7c2	do
ciziny	cizina	k1gFnSc2	cizina
<g/>
.	.	kIx.	.
</s>
<s>
Křesťané	křesťan	k1gMnPc1	křesťan
byli	být	k5eAaImAgMnP	být
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
podřadnou	podřadný	k2eAgFnSc4d1	podřadná
třídu	třída	k1gFnSc4	třída
a	a	k8xC	a
Bulhaři	Bulhar	k1gMnPc1	Bulhar
byli	být	k5eAaImAgMnP	být
podrobeni	podrobit	k5eAaPmNgMnP	podrobit
těžkým	těžký	k2eAgFnPc3d1	těžká
daním	daň	k1gFnPc3	daň
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
devširme	devširm	k1gInSc5	devširm
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
krevní	krevní	k2eAgFnPc1d1	krevní
daně	daň	k1gFnPc1	daň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
ztratila	ztratit	k5eAaPmAgFnS	ztratit
národní	národní	k2eAgNnSc4d1	národní
povědomí	povědomí	k1gNnSc4	povědomí
a	a	k8xC	a
identifikovala	identifikovat	k5eAaBmAgFnS	identifikovat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
pravoslavnou	pravoslavný	k2eAgFnSc7d1	pravoslavná
vírou	víra	k1gFnSc7	víra
<g/>
.	.	kIx.	.
</s>
<s>
Bulharská	bulharský	k2eAgFnSc1d1	bulharská
identita	identita	k1gFnSc1	identita
se	se	k3xPyFc4	se
uchovávala	uchovávat	k5eAaImAgFnS	uchovávat
jen	jen	k9	jen
u	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
duchovních	duchovní	k1gMnPc2	duchovní
v	v	k7c6	v
izolovaných	izolovaný	k2eAgInPc6d1	izolovaný
klášterech	klášter	k1gInPc6	klášter
a	a	k8xC	a
v	v	k7c6	v
militantním	militantní	k2eAgNnSc6d1	militantní
katolickém	katolický	k2eAgNnSc6d1	katolické
společenství	společenství	k1gNnSc6	společenství
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
beznadějná	beznadějný	k2eAgFnSc1d1	beznadějná
situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
zlepšila	zlepšit	k5eAaPmAgFnS	zlepšit
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
začali	začít	k5eAaPmAgMnP	začít
Bulhary	Bulhar	k1gMnPc4	Bulhar
jako	jako	k8xC	jako
potenciální	potenciální	k2eAgFnSc4d1	potenciální
zbraň	zbraň	k1gFnSc4	zbraň
proti	proti	k7c3	proti
Turkům	Turek	k1gMnPc3	Turek
vnímat	vnímat	k5eAaImF	vnímat
Rusové	Rus	k1gMnPc1	Rus
a	a	k8xC	a
Rakušané	Rakušan	k1gMnPc1	Rakušan
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
skrytě	skrytě	k6eAd1	skrytě
podporovali	podporovat	k5eAaImAgMnP	podporovat
řadu	řada	k1gFnSc4	řada
bulharských	bulharský	k2eAgNnPc2d1	bulharské
povstání	povstání	k1gNnPc2	povstání
-	-	kIx~	-
v	v	k7c6	v
Tarnově	Tarnov	k1gInSc6	Tarnov
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1598	[number]	k4	1598
a	a	k8xC	a
1686	[number]	k4	1686
<g/>
,	,	kIx,	,
Čiprovské	Čiprovský	k2eAgNnSc1d1	Čiprovský
povstání	povstání	k1gNnSc1	povstání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1688	[number]	k4	1688
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
Karpošovo	Karpošův	k2eAgNnSc4d1	Karpošův
povstání	povstání	k1gNnSc4	povstání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1689	[number]	k4	1689
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Třetí	třetí	k4xOgInSc1	třetí
bulharský	bulharský	k2eAgInSc1d1	bulharský
stát	stát	k1gInSc1	stát
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
začíná	začínat	k5eAaImIp3nS	začínat
bulharské	bulharský	k2eAgNnSc1d1	bulharské
národní	národní	k2eAgNnSc1d1	národní
obrození	obrození	k1gNnSc1	obrození
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
klíčovým	klíčový	k2eAgMnSc7d1	klíčový
politickým	politický	k2eAgMnSc7d1	politický
představitelem	představitel	k1gMnSc7	představitel
byl	být	k5eAaImAgMnS	být
Vasil	Vasil	k1gMnSc1	Vasil
Levski	Levski	k1gNnSc2	Levski
<g/>
,	,	kIx,	,
intelektuálním	intelektuální	k2eAgNnSc6d1	intelektuální
pak	pak	k6eAd1	pak
Paisij	Paisij	k1gFnSc6	Paisij
Chilendarski	Chilendarsk	k1gFnSc6	Chilendarsk
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
čistě	čistě	k6eAd1	čistě
kulturní	kulturní	k2eAgNnPc1d1	kulturní
hnutí	hnutí	k1gNnPc1	hnutí
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
měnilo	měnit	k5eAaImAgNnS	měnit
v	v	k7c6	v
hnutí	hnutí	k1gNnSc6	hnutí
s	s	k7c7	s
politickými	politický	k2eAgInPc7d1	politický
požadavky	požadavek	k1gInPc7	požadavek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1876	[number]	k4	1876
bylo	být	k5eAaImAgNnS	být
krvavě	krvavě	k6eAd1	krvavě
potlačeno	potlačen	k2eAgNnSc1d1	potlačeno
protiosmanské	protiosmanský	k2eAgNnSc1d1	protiosmanský
Dubnové	dubnový	k2eAgNnSc1d1	dubnové
povstání	povstání	k1gNnSc1	povstání
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
padl	padnout	k5eAaPmAgMnS	padnout
i	i	k9	i
další	další	k2eAgMnSc1d1	další
z	z	k7c2	z
vůdců	vůdce	k1gMnPc2	vůdce
národního	národní	k2eAgNnSc2d1	národní
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
Christo	Christa	k1gMnSc5	Christa
Botev	Botva	k1gFnPc2	Botva
<g/>
,	,	kIx,	,
vedle	vedle	k7c2	vedle
30	[number]	k4	30
000	[number]	k4	000
dalších	další	k2eAgMnPc2d1	další
Bulharů	Bulhar	k1gMnPc2	Bulhar
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
propukla	propuknout	k5eAaPmAgFnS	propuknout
Rusko-turecká	ruskourecký	k2eAgFnSc1d1	rusko-turecká
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
1877	[number]	k4	1877
<g/>
–	–	k?	–
<g/>
1878	[number]	k4	1878
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důsledků	důsledek	k1gInPc2	důsledek
ruského	ruský	k2eAgNnSc2d1	ruské
vítězství	vítězství	k1gNnSc2	vítězství
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
byl	být	k5eAaImAgMnS	být
i	i	k8xC	i
vznik	vznik	k1gInSc4	vznik
Bulharského	bulharský	k2eAgNnSc2d1	bulharské
knížectví	knížectví	k1gNnSc2	knížectví
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1878	[number]	k4	1878
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
Sanstefanská	Sanstefanský	k2eAgFnSc1d1	Sanstefanský
mírová	mírový	k2eAgFnSc1d1	mírová
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
,	,	kIx,	,
Berlínský	berlínský	k2eAgInSc1d1	berlínský
kongres	kongres	k1gInSc1	kongres
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
knížetem	kníže	k1gMnSc7	kníže
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Alexandr	Alexandr	k1gMnSc1	Alexandr
I.	I.	kA	I.
<g/>
,	,	kIx,	,
klíčovou	klíčový	k2eAgFnSc7d1	klíčová
osobností	osobnost	k1gFnSc7	osobnost
premiér	premiér	k1gMnSc1	premiér
Stefan	Stefan	k1gMnSc1	Stefan
Stambolov	Stambolovo	k1gNnPc2	Stambolovo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
byla	být	k5eAaImAgFnS	být
ke	k	k7c3	k
knížectví	knížectví	k1gNnSc3	knížectví
připojena	připojen	k2eAgFnSc1d1	připojena
i	i	k9	i
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
autonomní	autonomní	k2eAgFnSc4d1	autonomní
součást	součást	k1gFnSc4	součást
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
Berlínským	berlínský	k2eAgInSc7d1	berlínský
kongresem	kongres	k1gInSc7	kongres
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
Východní	východní	k2eAgFnSc1d1	východní
Rumélie	Rumélie	k1gFnSc1	Rumélie
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
nového	nový	k2eAgNnSc2d1	nové
knížectví	knížectví	k1gNnSc2	knížectví
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
přesto	přesto	k8xC	přesto
velmi	velmi	k6eAd1	velmi
nespokojeno	spokojen	k2eNgNnSc1d1	nespokojeno
s	s	k7c7	s
faktem	fakt	k1gInSc7	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
část	část	k1gFnSc1	část
Bulharů	Bulhar	k1gMnPc2	Bulhar
zůstala	zůstat	k5eAaPmAgFnS	zůstat
stále	stále	k6eAd1	stále
na	na	k7c6	na
území	území	k1gNnSc6	území
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Aktivní	aktivní	k2eAgMnPc1d1	aktivní
odpůrci	odpůrce	k1gMnPc1	odpůrce
založili	založit	k5eAaPmAgMnP	založit
hnutí	hnutí	k1gNnSc4	hnutí
Vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
makedonská	makedonský	k2eAgFnSc1d1	makedonská
revoluční	revoluční	k2eAgFnSc1d1	revoluční
organizace	organizace	k1gFnSc1	organizace
(	(	kIx(	(
<g/>
Goce	Gocus	k1gMnSc5	Gocus
Delčev	Delčev	k1gMnSc5	Delčev
<g/>
,	,	kIx,	,
Jane	Jan	k1gMnSc5	Jan
Sandanski	Sandansk	k1gMnSc5	Sandansk
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
proti	proti	k7c3	proti
Osmanům	Osman	k1gMnPc3	Osman
provádělo	provádět	k5eAaImAgNnS	provádět
bombové	bombový	k2eAgInPc4d1	bombový
útoky	útok	k1gInPc4	útok
a	a	k8xC	a
atentáty	atentát	k1gInPc4	atentát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
páchalo	páchat	k5eAaImAgNnS	páchat
i	i	k9	i
vraždy	vražda	k1gFnSc2	vražda
na	na	k7c6	na
nevinném	vinný	k2eNgNnSc6d1	nevinné
obyvatelstvu	obyvatelstvo	k1gNnSc6	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
<g/>
,	,	kIx,	,
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
bulharský	bulharský	k2eAgMnSc1d1	bulharský
kníže	kníže	k1gMnSc1	kníže
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
ve	v	k7c6	v
Velikém	veliký	k2eAgInSc6d1	veliký
Tǎ	Tǎ	k1gMnPc7	Tǎ
nezávislost	nezávislost	k1gFnSc4	nezávislost
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
na	na	k7c6	na
Osmanské	osmanský	k2eAgFnSc6d1	Osmanská
říši	říš	k1gFnSc6	říš
a	a	k8xC	a
přijal	přijmout	k5eAaPmAgMnS	přijmout
titul	titul	k1gInSc4	titul
cara	car	k1gMnSc2	car
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
se	se	k3xPyFc4	se
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
zapojilo	zapojit	k5eAaPmAgNnS	zapojit
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
spojenci	spojenec	k1gMnPc7	spojenec
z	z	k7c2	z
Balkánského	balkánský	k2eAgInSc2d1	balkánský
spolku	spolek	k1gInSc2	spolek
do	do	k7c2	do
první	první	k4xOgFnSc2	první
balkánské	balkánský	k2eAgFnSc2d1	balkánská
války	válka	k1gFnSc2	válka
s	s	k7c7	s
Osmanskou	osmanský	k2eAgFnSc7d1	Osmanská
říší	říš	k1gFnSc7	říš
<g/>
.	.	kIx.	.
</s>
<s>
Bulharská	bulharský	k2eAgFnSc1d1	bulharská
armáda	armáda	k1gFnSc1	armáda
dobyla	dobýt	k5eAaPmAgFnS	dobýt
město	město	k1gNnSc4	město
Odrin	Odrina	k1gFnPc2	Odrina
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Edirne	Edirn	k1gInSc5	Edirn
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
)	)	kIx)	)
a	a	k8xC	a
jen	jen	k9	jen
o	o	k7c4	o
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
prohrála	prohrát	k5eAaPmAgFnS	prohrát
s	s	k7c7	s
Řeckem	Řecko	k1gNnSc7	Řecko
"	"	kIx"	"
<g/>
závod	závod	k1gInSc1	závod
<g/>
"	"	kIx"	"
o	o	k7c6	o
obsazení	obsazení	k1gNnSc6	obsazení
Soluně	Soluň	k1gFnSc2	Soluň
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
sporům	spor	k1gInPc3	spor
se	se	k3xPyFc4	se
Srbskem	Srbsko	k1gNnSc7	Srbsko
o	o	k7c6	o
Makedonii	Makedonie	k1gFnSc6	Makedonie
(	(	kIx(	(
<g/>
Srbové	Srb	k1gMnPc1	Srb
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
vydat	vydat	k5eAaPmF	vydat
tzv.	tzv.	kA	tzv.
nesporné	sporný	k2eNgNnSc1d1	nesporné
pásmo	pásmo	k1gNnSc1	pásmo
<g/>
)	)	kIx)	)
a	a	k8xC	a
bezvýsledným	bezvýsledný	k2eAgNnSc7d1	bezvýsledné
mírovým	mírový	k2eAgNnSc7d1	Mírové
jednáním	jednání	k1gNnSc7	jednání
vydal	vydat	k5eAaPmAgMnS	vydat
bulharský	bulharský	k2eAgMnSc1d1	bulharský
car	car	k1gMnSc1	car
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1913	[number]	k4	1913
rozkaz	rozkaz	k1gInSc1	rozkaz
zaútočit	zaútočit	k5eAaPmF	zaútočit
na	na	k7c4	na
srbské	srbský	k2eAgFnPc4d1	Srbská
pozice	pozice	k1gFnPc4	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
balkánská	balkánský	k2eAgFnSc1d1	balkánská
válka	válka	k1gFnSc1	válka
dopadla	dopadnout	k5eAaPmAgFnS	dopadnout
pro	pro	k7c4	pro
Bulhary	Bulhar	k1gMnPc4	Bulhar
katastrofálně	katastrofálně	k6eAd1	katastrofálně
<g/>
,	,	kIx,	,
Osmanské	osmanský	k2eAgFnSc6d1	Osmanská
říši	říš	k1gFnSc6	říš
museli	muset	k5eAaImAgMnP	muset
odstoupit	odstoupit	k5eAaPmF	odstoupit
Odrin	Odrin	k1gInSc4	Odrin
<g/>
,	,	kIx,	,
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
si	se	k3xPyFc3	se
vynutilo	vynutit	k5eAaPmAgNnS	vynutit
předání	předání	k1gNnSc1	předání
jižní	jižní	k2eAgFnSc2d1	jižní
Dobrudže	Dobrudža	k1gFnSc2	Dobrudža
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
počáteční	počáteční	k2eAgFnSc6d1	počáteční
neutralitě	neutralita	k1gFnSc6	neutralita
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
do	do	k7c2	do
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
ústředních	ústřední	k2eAgFnPc2d1	ústřední
mocností	mocnost	k1gFnPc2	mocnost
a	a	k8xC	a
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
válku	válka	k1gFnSc4	válka
Srbsku	Srbsko	k1gNnSc3	Srbsko
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
zvrátit	zvrátit	k5eAaPmF	zvrátit
výsledky	výsledek	k1gInPc4	výsledek
druhé	druhý	k4xOgFnSc2	druhý
balkánské	balkánský	k2eAgFnSc2d1	balkánská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
však	však	k9	však
skončila	skončit	k5eAaPmAgFnS	skončit
pro	pro	k7c4	pro
Bulharsko	Bulharsko	k1gNnSc4	Bulharsko
další	další	k2eAgFnSc7d1	další
národní	národní	k2eAgFnSc7d1	národní
katastrofou	katastrofa	k1gFnSc7	katastrofa
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
smluvně	smluvně	k6eAd1	smluvně
garantován	garantován	k2eAgInSc4d1	garantován
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
Egejskému	egejský	k2eAgNnSc3d1	Egejské
moři	moře	k1gNnSc3	moře
<g/>
,	,	kIx,	,
po	po	k7c6	po
řecko-turecké	řeckourecký	k2eAgFnSc6d1	řecko-turecká
válce	válka	k1gFnSc6	válka
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
přesídlování	přesídlování	k1gNnSc2	přesídlování
maloasijských	maloasijský	k2eAgMnPc2d1	maloasijský
Řeků	Řek	k1gMnPc2	Řek
přišlo	přijít	k5eAaPmAgNnS	přijít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
car	car	k1gMnSc1	car
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
abdikoval	abdikovat	k5eAaBmAgMnS	abdikovat
a	a	k8xC	a
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
dosedl	dosednout	k5eAaPmAgMnS	dosednout
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
car	car	k1gMnSc1	car
Boris	Boris	k1gMnSc1	Boris
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klíčovou	klíčový	k2eAgFnSc7d1	klíčová
osobností	osobnost	k1gFnSc7	osobnost
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
agrárnický	agrárnický	k2eAgMnSc1d1	agrárnický
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
premiér	premiér	k1gMnSc1	premiér
Aleksandar	Aleksandar	k1gInSc4	Aleksandar
Stambolijski	Stambolijski	k1gNnSc2	Stambolijski
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
však	však	k9	však
smetl	smetnout	k5eAaPmAgInS	smetnout
státní	státní	k2eAgInSc1d1	státní
převrat	převrat	k1gInSc1	převrat
nacionalistických	nacionalistický	k2eAgMnPc2d1	nacionalistický
radikálů	radikál	k1gMnPc2	radikál
z	z	k7c2	z
VMRO	VMRO	kA	VMRO
<g/>
,	,	kIx,	,
nespokojených	spokojený	k2eNgMnPc2d1	nespokojený
se	se	k3xPyFc4	se
ztrátou	ztráta	k1gFnSc7	ztráta
mnohých	mnohý	k2eAgNnPc2d1	mnohé
území	území	k1gNnPc2	území
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Makedonie	Makedonie	k1gFnSc1	Makedonie
<g/>
,	,	kIx,	,
po	po	k7c6	po
prohrané	prohraný	k2eAgFnSc6d1	prohraná
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
Bulharsko	Bulharsko	k1gNnSc4	Bulharsko
neutralitu	neutralita	k1gFnSc4	neutralita
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
ekonomických	ekonomický	k2eAgFnPc2d1	ekonomická
okolností	okolnost	k1gFnPc2	okolnost
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
do	do	k7c2	do
válečného	válečný	k2eAgInSc2d1	válečný
konfliktu	konflikt	k1gInSc2	konflikt
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
nicméně	nicméně	k8xC	nicméně
zabránit	zabránit	k5eAaPmF	zabránit
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
zásluhou	zásluha	k1gFnSc7	zásluha
aktivity	aktivita	k1gFnSc2	aktivita
předsedy	předseda	k1gMnSc2	předseda
parlamentu	parlament	k1gInSc2	parlament
Dimitara	Dimitar	k1gMnSc4	Dimitar
Peševa	Pešev	k1gMnSc4	Pešev
<g/>
,	,	kIx,	,
deportaci	deportace	k1gFnSc4	deportace
40	[number]	k4	40
000	[number]	k4	000
bulharských	bulharský	k2eAgMnPc2d1	bulharský
Židů	Žid	k1gMnPc2	Žid
do	do	k7c2	do
německých	německý	k2eAgInPc2d1	německý
táborů	tábor	k1gInPc2	tábor
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
také	také	k9	také
nevyhlásilo	vyhlásit	k5eNaPmAgNnS	vyhlásit
formálně	formálně	k6eAd1	formálně
válku	válka	k1gFnSc4	válka
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1944	[number]	k4	1944
naopak	naopak	k6eAd1	naopak
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
válku	válka	k1gFnSc4	válka
Bulharsku	Bulharsko	k1gNnSc3	Bulharsko
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
a	a	k8xC	a
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
bez	bez	k7c2	bez
jakéhokoliv	jakýkoliv	k3yIgInSc2	jakýkoliv
odporu	odpor	k1gInSc2	odpor
a	a	k8xC	a
za	za	k7c2	za
nadšeného	nadšený	k2eAgNnSc2d1	nadšené
vítání	vítání	k1gNnSc2	vítání
lidu	lid	k1gInSc2	lid
na	na	k7c6	na
území	území	k1gNnSc6	území
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
uzavřeno	uzavřít	k5eAaPmNgNnS	uzavřít
příměří	příměří	k1gNnSc1	příměří
se	s	k7c7	s
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Konce	konec	k1gInPc1	konec
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
už	už	k6eAd1	už
se	se	k3xPyFc4	se
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Spojenců	spojenec	k1gMnPc2	spojenec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
následovalo	následovat	k5eAaImAgNnS	následovat
vyřizování	vyřizování	k1gNnSc1	vyřizování
starých	starý	k2eAgInPc2d1	starý
účtů	účet	k1gInPc2	účet
pod	pod	k7c7	pod
heslem	heslo	k1gNnSc7	heslo
odstranění	odstranění	k1gNnSc2	odstranění
"	"	kIx"	"
<g/>
monarchofašistické	monarchofašistický	k2eAgFnSc2d1	monarchofašistický
diktatury	diktatura	k1gFnSc2	diktatura
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Popraveno	popraven	k2eAgNnSc1d1	popraveno
bylo	být	k5eAaImAgNnS	být
mnoho	mnoho	k4c1	mnoho
významných	významný	k2eAgMnPc2d1	významný
politiků	politik	k1gMnPc2	politik
i	i	k8xC	i
všichni	všechen	k3xTgMnPc1	všechen
tři	tři	k4xCgMnPc1	tři
carští	carský	k2eAgMnPc1d1	carský
regenti	regent	k1gMnPc1	regent
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
carova	carův	k2eAgMnSc2d1	carův
bratra	bratr	k1gMnSc2	bratr
Kirila	Kiril	k1gMnSc2	Kiril
<g/>
.	.	kIx.	.
</s>
<s>
Brutalita	brutalita	k1gFnSc1	brutalita
a	a	k8xC	a
rozsah	rozsah	k1gInSc1	rozsah
represí	represe	k1gFnPc2	represe
nemá	mít	k5eNaImIp3nS	mít
obdoby	obdoba	k1gFnPc4	obdoba
u	u	k7c2	u
žádné	žádný	k3yNgFnSc2	žádný
jiné	jiný	k2eAgFnSc2d1	jiná
východoevropské	východoevropský	k2eAgFnSc2d1	východoevropská
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
historikové	historik	k1gMnPc1	historik
počet	počet	k1gInSc4	počet
poprav	poprava	k1gFnPc2	poprava
odhadují	odhadovat	k5eAaImIp3nP	odhadovat
na	na	k7c4	na
1000	[number]	k4	1000
<g/>
-	-	kIx~	-
<g/>
3000	[number]	k4	3000
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
referendu	referendum	k1gNnSc6	referendum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
organizovaném	organizovaný	k2eAgInSc6d1	organizovaný
bulharskou	bulharský	k2eAgFnSc7d1	bulharská
komunistickou	komunistický	k2eAgFnSc7d1	komunistická
stranou	strana	k1gFnSc7	strana
za	za	k7c2	za
dohledu	dohled	k1gInSc2	dohled
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
bylo	být	k5eAaImAgNnS	být
zrušeno	zrušen	k2eAgNnSc1d1	zrušeno
carství	carství	k1gNnSc1	carství
a	a	k8xC	a
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bulharské	bulharský	k2eAgFnSc6d1	bulharská
lidové	lidový	k2eAgFnSc6d1	lidová
republice	republika	k1gFnSc6	republika
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
vládla	vládnout	k5eAaImAgFnS	vládnout
komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
bylo	být	k5eAaImAgNnS	být
součástí	součást	k1gFnSc7	součást
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
<g/>
,	,	kIx,	,
politicky	politicky	k6eAd1	politicky
a	a	k8xC	a
ekonomicky	ekonomicky	k6eAd1	ekonomicky
spjaté	spjatý	k2eAgFnSc6d1	spjatá
se	s	k7c7	s
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Známou	známý	k2eAgFnSc7d1	známá
postavou	postava	k1gFnSc7	postava
novodobých	novodobý	k2eAgFnPc2d1	novodobá
bulharských	bulharský	k2eAgFnPc2d1	bulharská
dějin	dějiny	k1gFnPc2	dějiny
je	být	k5eAaImIp3nS	být
Georgi	Georg	k1gMnSc3	Georg
Dimitrov	Dimitrov	k1gInSc4	Dimitrov
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
bouřlivých	bouřlivý	k2eAgInPc6d1	bouřlivý
letech	let	k1gInPc6	let
1946	[number]	k4	1946
<g/>
–	–	k?	–
<g/>
1949	[number]	k4	1949
a	a	k8xC	a
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
on	on	k3xPp3gMnSc1	on
a	a	k8xC	a
jugoslávský	jugoslávský	k2eAgMnSc1d1	jugoslávský
vůdce	vůdce	k1gMnSc1	vůdce
Josip	Josip	k1gMnSc1	Josip
Broz	Broz	k1gMnSc1	Broz
Tito	tento	k3xDgMnPc1	tento
vedli	vést	k5eAaImAgMnP	vést
rozhovory	rozhovor	k1gInPc4	rozhovor
se	s	k7c7	s
Stalinem	Stalin	k1gMnSc7	Stalin
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
vládl	vládnout	k5eAaImAgInS	vládnout
v	v	k7c6	v
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
dlouhá	dlouhý	k2eAgNnPc1d1	dlouhé
léta	léto	k1gNnPc1	léto
Valko	Valko	k1gNnSc4	Valko
Červenkov	Červenkov	k1gInSc4	Červenkov
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgMnSc7d1	poslední
dlouholetým	dlouholetý	k2eAgMnSc7d1	dlouholetý
komunistickým	komunistický	k2eAgMnSc7d1	komunistický
vůdcem	vůdce	k1gMnSc7	vůdce
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
byl	být	k5eAaImAgMnS	být
Todor	Todor	k1gMnSc1	Todor
Živkov	Živkov	k1gInSc4	Živkov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
procházela	procházet	k5eAaImAgFnS	procházet
země	země	k1gFnSc1	země
procesem	proces	k1gInSc7	proces
demokratizace	demokratizace	k1gFnSc2	demokratizace
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
parlamentní	parlamentní	k2eAgFnSc3d1	parlamentní
republice	republika	k1gFnSc3	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
NATO	NATO	kA	NATO
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
členským	členský	k2eAgInSc7d1	členský
státem	stát	k1gInSc7	stát
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
hostilo	hostit	k5eAaImAgNnS	hostit
předsednictví	předsednictví	k1gNnSc4	předsednictví
Rady	rada	k1gFnSc2	rada
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
v	v	k7c6	v
renovovaném	renovovaný	k2eAgInSc6d1	renovovaný
Národním	národní	k2eAgInSc6d1	národní
paláci	palác	k1gInSc6	palác
kultury	kultura	k1gFnSc2	kultura
v	v	k7c6	v
Sofii	Sofia	k1gFnSc6	Sofia
<g/>
,	,	kIx,	,
postaveném	postavený	k2eAgInSc6d1	postavený
v	v	k7c6	v
Živkovově	Živkovův	k2eAgFnSc6d1	Živkovova
éře	éra	k1gFnSc6	éra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
je	být	k5eAaImIp3nS	být
země	zem	k1gFnPc4	zem
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
Balkánského	balkánský	k2eAgInSc2d1	balkánský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gMnSc4	on
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
pět	pět	k4xCc4	pět
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
má	mít	k5eAaImIp3nS	mít
240	[number]	k4	240
km	km	kA	km
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
Tureckem	Turecko	k1gNnSc7	Turecko
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
494	[number]	k4	494
km	km	kA	km
s	s	k7c7	s
Řeckem	Řecko	k1gNnSc7	Řecko
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
148	[number]	k4	148
km	km	kA	km
s	s	k7c7	s
Makedonií	Makedonie	k1gFnSc7	Makedonie
a	a	k8xC	a
318	[number]	k4	318
km	km	kA	km
se	s	k7c7	s
Srbskem	Srbsko	k1gNnSc7	Srbsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
608	[number]	k4	608
km	km	kA	km
s	s	k7c7	s
Rumunskem	Rumunsko	k1gNnSc7	Rumunsko
<g/>
.	.	kIx.	.
470	[number]	k4	470
km	km	kA	km
hranice	hranice	k1gFnSc1	hranice
s	s	k7c7	s
Rumunskem	Rumunsko	k1gNnSc7	Rumunsko
tvoří	tvořit	k5eAaImIp3nS	tvořit
řeka	řeka	k1gFnSc1	řeka
Dunaj	Dunaj	k1gInSc1	Dunaj
<g/>
.	.	kIx.	.
</s>
<s>
Suchozemská	suchozemský	k2eAgFnSc1d1	suchozemská
hranice	hranice	k1gFnSc1	hranice
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
1	[number]	k4	1
808	[number]	k4	808
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
má	mít	k5eAaImIp3nS	mít
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
354	[number]	k4	354
km	km	kA	km
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
pobřeží	pobřeží	k1gNnSc1	pobřeží
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
110	[number]	k4	110
994	[number]	k4	994
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
11	[number]	k4	11
<g/>
.	.	kIx.	.
největší	veliký	k2eAgFnSc7d3	veliký
zemí	zem	k1gFnSc7	zem
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
2,5	[number]	k4	2,5
%	%	kIx~	%
jejího	její	k3xOp3gNnSc2	její
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
1,4	[number]	k4	1,4
<g/>
×	×	k?	×
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
povrchu	povrch	k1gInSc2	povrch
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
je	být	k5eAaImIp3nS	být
hornatá	hornatý	k2eAgFnSc1d1	hornatá
<g/>
.	.	kIx.	.
</s>
<s>
Centrální	centrální	k2eAgFnSc7d1	centrální
částí	část	k1gFnSc7	část
země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
na	na	k7c4	na
východ	východ	k1gInSc4	východ
pohoří	pohořet	k5eAaPmIp3nS	pohořet
Stara	Stara	k1gFnSc1	Stara
planina	planina	k1gFnSc1	planina
<g/>
.	.	kIx.	.
</s>
<s>
Jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
masiv	masiv	k1gInSc4	masiv
Vitoša	Vitoš	k1gInSc2	Vitoš
měřící	měřící	k2eAgInSc4d1	měřící
2	[number]	k4	2
290	[number]	k4	290
m.	m.	k?	m.
n.	n.	k?	n.
m.	m.	k?	m.
Jihozápad	jihozápad	k1gInSc1	jihozápad
země	zem	k1gFnSc2	zem
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
mohutný	mohutný	k2eAgInSc1d1	mohutný
horský	horský	k2eAgInSc1d1	horský
masiv	masiv	k1gInSc1	masiv
s	s	k7c7	s
pohořími	pohoří	k1gNnPc7	pohoří
Rila	Rilum	k1gNnSc2	Rilum
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
také	také	k9	také
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
a	a	k8xC	a
také	také	k9	také
Balkánského	balkánský	k2eAgInSc2d1	balkánský
poloostrova	poloostrov	k1gInSc2	poloostrov
Musala	Musala	k1gFnSc1	Musala
měřící	měřící	k2eAgFnSc1d1	měřící
2	[number]	k4	2
925	[number]	k4	925
m.	m.	k?	m.
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Pirin	Pirin	k2eAgMnSc1d1	Pirin
a	a	k8xC	a
plošně	plošně	k6eAd1	plošně
rozsáhlejší	rozsáhlý	k2eAgFnPc4d2	rozsáhlejší
Rodopy	Rodopa	k1gFnPc4	Rodopa
<g/>
.	.	kIx.	.
</s>
<s>
Pirin	Pirin	k1gInSc1	Pirin
a	a	k8xC	a
Rodopy	Rodopa	k1gFnPc1	Rodopa
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
starobylého	starobylý	k2eAgInSc2d1	starobylý
rodopského	rodopský	k2eAgInSc2d1	rodopský
horského	horský	k2eAgInSc2d1	horský
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
prostředkem	prostředek	k1gInSc7	prostředek
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
od	od	k7c2	od
srbských	srbský	k2eAgFnPc2d1	Srbská
hranic	hranice	k1gFnPc2	hranice
na	na	k7c6	na
západě	západ	k1gInSc6	západ
po	po	k7c4	po
Černé	Černé	k2eAgNnSc4d1	Černé
moře	moře	k1gNnSc4	moře
na	na	k7c6	na
východě	východ	k1gInSc6	východ
pohoří	pohoří	k1gNnSc2	pohoří
Stara	Stara	k1gFnSc1	Stara
planina	planina	k1gFnSc1	planina
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Staré	Staré	k2eAgFnPc1d1	Staré
hory	hora	k1gFnPc1	hora
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
systému	systém	k1gInSc2	systém
Balkanid	Balkanida	k1gFnPc2	Balkanida
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
leží	ležet	k5eAaImIp3nP	ležet
Dolnodunajská	Dolnodunajský	k2eAgFnSc1d1	Dolnodunajská
nížina	nížina	k1gFnSc1	nížina
<g/>
,	,	kIx,	,
na	na	k7c4	na
jih	jih	k1gInSc4	jih
Hornothrácká	Hornothrácký	k2eAgFnSc1d1	Hornothrácký
nížina	nížina	k1gFnSc1	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Stara	Stara	k6eAd1	Stara
planina	planina	k1gFnSc1	planina
je	být	k5eAaImIp3nS	být
pokračováním	pokračování	k1gNnSc7	pokračování
Karpat	Karpaty	k1gInPc2	Karpaty
a	a	k8xC	a
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Rodop	Rodop	k1gInSc1	Rodop
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
Alpsko-himálajského	alpskoimálajský	k2eAgInSc2d1	alpsko-himálajský
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
odvádí	odvádět	k5eAaImIp3nS	odvádět
své	svůj	k3xOyFgFnPc4	svůj
vody	voda	k1gFnPc4	voda
největší	veliký	k2eAgFnSc1d3	veliký
řeka	řeka	k1gFnSc1	řeka
země	zem	k1gFnSc2	zem
Dunaj	Dunaj	k1gInSc1	Dunaj
<g/>
,	,	kIx,	,
k	k	k7c3	k
úmoří	úmoří	k1gNnSc3	úmoří
Egejského	egejský	k2eAgNnSc2d1	Egejské
moře	moře	k1gNnSc2	moře
patří	patřit	k5eAaImIp3nS	patřit
Marica	Marica	k1gFnSc1	Marica
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
severu	sever	k1gInSc6	sever
ráz	ráz	k1gInSc4	ráz
kontinentální	kontinentální	k2eAgInSc1d1	kontinentální
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
a	a	k8xC	a
východě	východ	k1gInSc6	východ
středomořský	středomořský	k2eAgInSc1d1	středomořský
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgFnPc1d1	průměrná
lednové	lednový	k2eAgFnPc1d1	lednová
teploty	teplota	k1gFnPc1	teplota
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
od	od	k7c2	od
-2	-2	k4	-2
do	do	k7c2	do
2	[number]	k4	2
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
červencové	červencový	k2eAgNnSc1d1	červencové
od	od	k7c2	od
21	[number]	k4	21
do	do	k7c2	do
25	[number]	k4	25
°	°	k?	°
<g/>
C.	C.	kA	C.
Průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc1d1	roční
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
kolísá	kolísat	k5eAaImIp3nS	kolísat
mezi	mezi	k7c7	mezi
450	[number]	k4	450
<g/>
–	–	k?	–
<g/>
850	[number]	k4	850
mm	mm	kA	mm
<g/>
,	,	kIx,	,
na	na	k7c6	na
horách	hora	k1gFnPc6	hora
přes	přes	k7c4	přes
1	[number]	k4	1
000	[number]	k4	000
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzájemné	vzájemný	k2eAgNnSc1d1	vzájemné
působení	působení	k1gNnSc1	působení
klimatických	klimatický	k2eAgFnPc2d1	klimatická
<g/>
,	,	kIx,	,
hydrologických	hydrologický	k2eAgFnPc2d1	hydrologická
<g/>
,	,	kIx,	,
geologických	geologický	k2eAgFnPc2d1	geologická
a	a	k8xC	a
topografických	topografický	k2eAgFnPc2d1	topografická
podmínek	podmínka	k1gFnPc2	podmínka
dalo	dát	k5eAaPmAgNnS	dát
Bulharsku	Bulharsko	k1gNnSc3	Bulharsko
širokou	široký	k2eAgFnSc4d1	široká
škálu	škála	k1gFnSc4	škála
rostlinných	rostlinný	k2eAgInPc2d1	rostlinný
a	a	k8xC	a
živočišných	živočišný	k2eAgInPc2d1	živočišný
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
tak	tak	k6eAd1	tak
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
úrovní	úroveň	k1gFnPc2	úroveň
biodiverzity	biodiverzita	k1gFnSc2	biodiverzita
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Cenné	cenný	k2eAgInPc1d1	cenný
přírodní	přírodní	k2eAgInPc1d1	přírodní
celky	celek	k1gInPc1	celek
jsou	být	k5eAaImIp3nP	být
uchovávány	uchovávat	k5eAaImNgInP	uchovávat
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
národních	národní	k2eAgInPc6d1	národní
parcích	park	k1gInPc6	park
<g/>
,	,	kIx,	,
11	[number]	k4	11
přírodních	přírodní	k2eAgInPc6d1	přírodní
parcích	park	k1gInPc6	park
<g/>
,	,	kIx,	,
10	[number]	k4	10
biosférických	biosférický	k2eAgFnPc6d1	biosférická
rezervacích	rezervace	k1gFnPc6	rezervace
a	a	k8xC	a
565	[number]	k4	565
chráněných	chráněný	k2eAgFnPc6d1	chráněná
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
území	území	k1gNnSc6	území
některé	některý	k3yIgFnPc4	některý
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
oblastí	oblast	k1gFnPc2	oblast
Natura	Natura	k1gFnSc1	Natura
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
33,8	[number]	k4	33,8
<g/>
%	%	kIx~	%
jeho	on	k3xPp3gNnSc2	on
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
233	[number]	k4	233
druhů	druh	k1gInPc2	druh
evropských	evropský	k2eAgMnPc2d1	evropský
savců	savec	k1gMnPc2	savec
se	se	k3xPyFc4	se
v	v	k7c6	v
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
93	[number]	k4	93
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
49	[number]	k4	49
<g/>
%	%	kIx~	%
evropských	evropský	k2eAgInPc2d1	evropský
druhů	druh	k1gInPc2	druh
motýlů	motýl	k1gMnPc2	motýl
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
je	být	k5eAaImIp3nS	být
evidováno	evidovat	k5eAaImNgNnS	evidovat
41	[number]	k4	41
493	[number]	k4	493
druhů	druh	k1gInPc2	druh
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Většími	veliký	k2eAgMnPc7d2	veliký
savci	savec	k1gMnPc7	savec
se	s	k7c7	s
značnou	značný	k2eAgFnSc7d1	značná
populací	populace	k1gFnSc7	populace
jsou	být	k5eAaImIp3nP	být
jeleni	jelen	k1gMnPc1	jelen
(	(	kIx(	(
<g/>
106	[number]	k4	106
323	[number]	k4	323
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
divoká	divoký	k2eAgNnPc1d1	divoké
prasata	prase	k1gNnPc1	prase
(	(	kIx(	(
<g/>
88	[number]	k4	88
948	[number]	k4	948
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šakalové	šakal	k1gMnPc1	šakal
(	(	kIx(	(
<g/>
47	[number]	k4	47
293	[number]	k4	293
<g/>
)	)	kIx)	)
a	a	k8xC	a
lišky	liška	k1gFnPc1	liška
(	(	kIx(	(
<g/>
32	[number]	k4	32
326	[number]	k4	326
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnSc1	populace
koroptví	koroptev	k1gFnPc2	koroptev
činí	činit	k5eAaImIp3nS	činit
328	[number]	k4	328
000	[number]	k4	000
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejrozšířenější	rozšířený	k2eAgInSc4d3	nejrozšířenější
druh	druh	k1gInSc4	druh
loveného	lovený	k2eAgMnSc2d1	lovený
ptáka	pták	k1gMnSc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Třetina	třetina	k1gFnSc1	třetina
všech	všecek	k3xTgMnPc2	všecek
hnízdících	hnízdící	k2eAgMnPc2d1	hnízdící
ptáků	pták	k1gMnPc2	pták
v	v	k7c6	v
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
Rila	Ril	k1gInSc2	Ril
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
také	také	k9	také
hostí	hostit	k5eAaImIp3nS	hostit
ve	v	k7c6	v
vysokých	vysoký	k2eAgFnPc6d1	vysoká
polohách	poloha	k1gFnPc6	poloha
arktické	arktický	k2eAgInPc1d1	arktický
a	a	k8xC	a
alpské	alpský	k2eAgInPc1d1	alpský
druhy	druh	k1gInPc1	druh
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
35	[number]	k4	35
<g/>
%	%	kIx~	%
plochy	plocha	k1gFnSc2	plocha
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
pokryto	pokrýt	k5eAaPmNgNnS	pokrýt
lesem	les	k1gInSc7	les
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
přijala	přijmout	k5eAaPmAgFnS	přijmout
bulharská	bulharský	k2eAgFnSc1d1	bulharská
vláda	vláda	k1gFnSc1	vláda
národní	národní	k2eAgFnSc4d1	národní
strategii	strategie	k1gFnSc4	strategie
ochrany	ochrana	k1gFnSc2	ochrana
biologické	biologický	k2eAgFnSc2d1	biologická
rozmanitosti	rozmanitost	k1gFnSc2	rozmanitost
a	a	k8xC	a
komplexní	komplexní	k2eAgInSc4d1	komplexní
program	program	k1gInSc4	program
zaměřený	zaměřený	k2eAgInSc4d1	zaměřený
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
místních	místní	k2eAgInPc2d1	místní
ekosystémů	ekosystém	k1gInPc2	ekosystém
<g/>
,	,	kIx,	,
ochranu	ochrana	k1gFnSc4	ochrana
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
druhů	druh	k1gInPc2	druh
a	a	k8xC	a
ochranu	ochrana	k1gFnSc4	ochrana
genetických	genetický	k2eAgInPc2d1	genetický
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k6eAd1	rovněž
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
cíle	cíl	k1gInPc4	cíl
Kjótského	Kjótský	k2eAgInSc2d1	Kjótský
protokolu	protokol	k1gInSc2	protokol
snížit	snížit	k5eAaPmF	snížit
emise	emise	k1gFnPc4	emise
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
o	o	k7c4	o
30	[number]	k4	30
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Problémem	problém	k1gInSc7	problém
však	však	k9	však
je	být	k5eAaImIp3nS	být
znečištění	znečištění	k1gNnSc4	znečištění
ovzduší	ovzduší	k1gNnSc6	ovzduší
pevnými	pevný	k2eAgFnPc7d1	pevná
částicemi	částice	k1gFnPc7	částice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
je	být	k5eAaImIp3nS	být
nejhorší	zlý	k2eAgNnSc1d3	nejhorší
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
městských	městský	k2eAgFnPc6d1	městská
oblastech	oblast	k1gFnPc6	oblast
postižených	postižený	k2eAgFnPc2d1	postižená
automobilovou	automobilový	k2eAgFnSc7d1	automobilová
dopravou	doprava	k1gFnSc7	doprava
a	a	k8xC	a
uhelnými	uhelný	k2eAgFnPc7d1	uhelná
elektrárnami	elektrárna	k1gFnPc7	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
,	,	kIx,	,
Marica	Marica	k1gFnSc1	Marica
Iztok-	Iztok-	k1gFnSc1	Iztok-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
největší	veliký	k2eAgFnPc4d3	veliký
škody	škoda	k1gFnPc4	škoda
na	na	k7c6	na
zdraví	zdraví	k1gNnSc6	zdraví
a	a	k8xC	a
životním	životní	k2eAgNnSc6d1	životní
prostředí	prostředí	k1gNnSc6	prostředí
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politika	politikum	k1gNnSc2	politikum
==	==	k?	==
</s>
</p>
<p>
<s>
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
je	být	k5eAaImIp3nS	být
parlamentní	parlamentní	k2eAgFnSc7d1	parlamentní
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
Hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
prezident	prezident	k1gMnSc1	prezident
volený	volený	k2eAgInSc1d1	volený
přímou	přímý	k2eAgFnSc7d1	přímá
volbou	volba	k1gFnSc7	volba
na	na	k7c4	na
pětileté	pětiletý	k2eAgNnSc4d1	pětileté
volební	volební	k2eAgNnSc4d1	volební
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Neustále	neustále	k6eAd1	neustále
činným	činný	k2eAgInSc7d1	činný
zákonodárným	zákonodárný	k2eAgInSc7d1	zákonodárný
orgánem	orgán	k1gInSc7	orgán
<g/>
,	,	kIx,	,
složeným	složený	k2eAgInSc7d1	složený
ze	z	k7c2	z
240	[number]	k4	240
poslanců	poslanec	k1gMnPc2	poslanec
volených	volený	k2eAgMnPc2d1	volený
na	na	k7c4	na
4	[number]	k4	4
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Výkonná	výkonný	k2eAgFnSc1d1	výkonná
moc	moc	k1gFnSc1	moc
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
dosavadní	dosavadní	k2eAgFnSc1d1	dosavadní
konzervativní	konzervativní	k2eAgFnSc1d1	konzervativní
vláda	vláda	k1gFnSc1	vláda
utrpěla	utrpět	k5eAaPmAgFnS	utrpět
porážku	porážka	k1gFnSc4	porážka
<g/>
,	,	kIx,	,
když	když	k8xS	když
klíčová	klíčový	k2eAgFnSc1d1	klíčová
politická	politický	k2eAgFnSc1d1	politická
síla	síla	k1gFnSc1	síla
Národní	národní	k2eAgNnSc1d1	národní
hnutí	hnutí	k1gNnSc1	hnutí
Simeona	Simeon	k1gMnSc2	Simeon
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
[	[	kIx(	[
<g/>
bulharsky	bulharsky	k6eAd1	bulharsky
<g/>
:	:	kIx,	:
Н	Н	k?	Н
д	д	k?	д
С	С	k?	С
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Н	Н	k?	Н
)	)	kIx)	)
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
kolem	kolem	k7c2	kolem
22	[number]	k4	22
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc4d3	veliký
vliv	vliv	k1gInSc4	vliv
získaly	získat	k5eAaPmAgFnP	získat
bulharští	bulharský	k2eAgMnPc1d1	bulharský
socialisté	socialist	k1gMnPc1	socialist
vedení	vedení	k1gNnSc2	vedení
Sergejem	Sergej	k1gMnSc7	Sergej
Staniševem	Stanišev	k1gInSc7	Stanišev
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nakonec	nakonec	k6eAd1	nakonec
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
velkou	velký	k2eAgFnSc4d1	velká
koalici	koalice	k1gFnSc4	koalice
s	s	k7c7	s
NDSV	NDSV	kA	NDSV
a	a	k8xC	a
Hnutím	hnutí	k1gNnSc7	hnutí
za	za	k7c2	za
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
svobody	svoboda	k1gFnSc2	svoboda
(	(	kIx(	(
<g/>
bulharsky	bulharsky	k6eAd1	bulharsky
<g/>
:	:	kIx,	:
Д	Д	k?	Д
з	з	k?	з
п	п	k?	п
и	и	k?	и
с	с	k?	с
<g/>
,	,	kIx,	,
Д	Д	k?	Д
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
podporováno	podporovat	k5eAaImNgNnS	podporovat
zejména	zejména	k9	zejména
bulharskými	bulharský	k2eAgInPc7d1	bulharský
Turky	turek	k1gInPc7	turek
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgNnSc7d3	veliký
překvapením	překvapení	k1gNnSc7	překvapení
voleb	volba	k1gFnPc2	volba
byl	být	k5eAaImAgMnS	být
úspěch	úspěch	k1gInSc4	úspěch
strany	strana	k1gFnSc2	strana
Ataka	ataka	k1gFnSc1	ataka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
svůj	svůj	k3xOyFgInSc4	svůj
program	program	k1gInSc4	program
postavila	postavit	k5eAaPmAgFnS	postavit
na	na	k7c6	na
nacionalistických	nacionalistický	k2eAgNnPc6d1	nacionalistické
heslech	heslo	k1gNnPc6	heslo
a	a	k8xC	a
odporu	odpor	k1gInSc2	odpor
vůči	vůči	k7c3	vůči
Evropské	evropský	k2eAgFnSc3d1	Evropská
unii	unie	k1gFnSc3	unie
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
téměř	téměř	k6eAd1	téměř
9	[number]	k4	9
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
ledna	leden	k1gInSc2	leden
2017	[number]	k4	2017
je	být	k5eAaImIp3nS	být
prezidentem	prezident	k1gMnSc7	prezident
Rumen	Rumna	k1gFnPc2	Rumna
Radev	Radva	k1gFnPc2	Radva
a	a	k8xC	a
od	od	k7c2	od
května	květen	k1gInSc2	květen
stejného	stejný	k2eAgInSc2d1	stejný
roku	rok	k1gInSc2	rok
je	být	k5eAaImIp3nS	být
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
Bojko	Bojko	k1gNnSc1	Bojko
Borisov	Borisov	k1gInSc1	Borisov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velkým	velký	k2eAgInSc7d1	velký
problémem	problém	k1gInSc7	problém
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
je	být	k5eAaImIp3nS	být
korupce	korupce	k1gFnSc1	korupce
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
korupčních	korupční	k2eAgInPc2d1	korupční
skandálů	skandál	k1gInPc2	skandál
byli	být	k5eAaImAgMnP	být
zapleteni	zaplést	k5eAaPmNgMnP	zaplést
i	i	k9	i
bulharský	bulharský	k2eAgMnSc1d1	bulharský
premiér	premiér	k1gMnSc1	premiér
Bojko	Bojko	k1gNnSc4	Bojko
Borisov	Borisov	k1gInSc1	Borisov
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
předchůdce	předchůdce	k1gMnSc1	předchůdce
Sergej	Sergej	k1gMnSc1	Sergej
Stanišev	Stanišev	k1gMnSc1	Stanišev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
se	se	k3xPyFc4	se
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
28	[number]	k4	28
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
nazvaných	nazvaný	k2eAgFnPc2d1	nazvaná
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gNnSc2	jejich
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Odvĕ	Odvĕ	k1gFnSc1	Odvĕ
struktura	struktura	k1gFnSc1	struktura
a	a	k8xC	a
výkonnost	výkonnost	k1gFnSc1	výkonnost
===	===	k?	===
</s>
</p>
<p>
<s>
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
je	být	k5eAaImIp3nS	být
průmyslově-zemědělský	průmyslověemědělský	k2eAgInSc4d1	průmyslově-zemědělský
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnPc1d1	hlavní
průmyslová	průmyslový	k2eAgNnPc1d1	průmyslové
odvětví	odvětví	k1gNnPc1	odvětví
jsou	být	k5eAaImIp3nP	být
hutnictví	hutnictví	k1gNnSc1	hutnictví
<g/>
,	,	kIx,	,
strojírenství	strojírenství	k1gNnSc2	strojírenství
<g/>
,	,	kIx,	,
chemie	chemie	k1gFnSc2	chemie
<g/>
,	,	kIx,	,
textilní	textilní	k2eAgInSc4d1	textilní
průmysl	průmysl	k1gInSc4	průmysl
a	a	k8xC	a
potravinářství	potravinářství	k1gNnSc4	potravinářství
<g/>
.	.	kIx.	.
</s>
<s>
Těží	těžet	k5eAaImIp3nS	těžet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
hnědé	hnědý	k2eAgNnSc4d1	hnědé
uhlí	uhlí	k1gNnSc4	uhlí
a	a	k8xC	a
barevné	barevný	k2eAgInPc4d1	barevný
kovy	kov	k1gInPc4	kov
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgNnPc1d3	nejdůležitější
průmyslová	průmyslový	k2eAgNnPc1d1	průmyslové
centra	centrum	k1gNnPc1	centrum
jsou	být	k5eAaImIp3nP	být
Sofie	Sofia	k1gFnPc1	Sofia
<g/>
,	,	kIx,	,
Plovdiv	Plovdiv	k1gInSc1	Plovdiv
<g/>
,	,	kIx,	,
Burgas	Burgas	k1gInSc1	Burgas
<g/>
,	,	kIx,	,
Pernik	Pernik	k1gInSc1	Pernik
a	a	k8xC	a
Stara	Stara	k1gFnSc1	Stara
Zagora	Zagora	k1gFnSc1	Zagora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
převládá	převládat	k5eAaImIp3nS	převládat
rostlinná	rostlinný	k2eAgFnSc1d1	rostlinná
produkce	produkce	k1gFnSc1	produkce
nad	nad	k7c4	nad
živočišnou	živočišný	k2eAgFnSc4d1	živočišná
<g/>
.	.	kIx.	.
</s>
<s>
Využití	využití	k1gNnSc1	využití
půdy	půda	k1gFnSc2	půda
<g/>
:	:	kIx,	:
orná	orný	k2eAgFnSc1d1	orná
půda	půda	k1gFnSc1	půda
37	[number]	k4	37
%	%	kIx~	%
<g/>
,	,	kIx,	,
louky	louka	k1gFnPc1	louka
a	a	k8xC	a
pastviny	pastvina	k1gFnPc1	pastvina
18	[number]	k4	18
%	%	kIx~	%
a	a	k8xC	a
lesy	les	k1gInPc1	les
35	[number]	k4	35
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
pšenice	pšenice	k1gFnSc1	pšenice
<g/>
,	,	kIx,	,
ječmen	ječmen	k1gInSc1	ječmen
<g/>
,	,	kIx,	,
kukuřice	kukuřice	k1gFnSc1	kukuřice
<g/>
,	,	kIx,	,
slunečnice	slunečnice	k1gFnSc1	slunečnice
<g/>
,	,	kIx,	,
brambory	brambora	k1gFnPc1	brambora
<g/>
,	,	kIx,	,
cukrová	cukrový	k2eAgFnSc1d1	cukrová
řepa	řepa	k1gFnSc1	řepa
<g/>
,	,	kIx,	,
zelenina	zelenina	k1gFnSc1	zelenina
<g/>
,	,	kIx,	,
broskve	broskev	k1gFnPc1	broskev
<g/>
,	,	kIx,	,
vinná	vinný	k2eAgFnSc1d1	vinná
réva	réva	k1gFnSc1	réva
<g/>
,	,	kIx,	,
rajčata	rajče	k1gNnPc1	rajče
<g/>
,	,	kIx,	,
melouny	meloun	k1gInPc1	meloun
a	a	k8xC	a
tabák	tabák	k1gInSc1	tabák
<g/>
.	.	kIx.	.
</s>
<s>
Chovají	chovat	k5eAaImIp3nP	chovat
se	se	k3xPyFc4	se
ovce	ovce	k1gFnPc1	ovce
<g/>
,	,	kIx,	,
prasata	prase	k1gNnPc1	prase
<g/>
,	,	kIx,	,
skot	skot	k1gInSc1	skot
<g/>
,	,	kIx,	,
buvoli	buvol	k1gMnPc1	buvol
<g/>
,	,	kIx,	,
koně	kůň	k1gMnPc1	kůň
<g/>
,	,	kIx,	,
osli	osel	k1gMnPc1	osel
<g/>
,	,	kIx,	,
kozy	koza	k1gFnPc1	koza
a	a	k8xC	a
drůbež	drůbež	k1gFnSc1	drůbež
<g/>
.	.	kIx.	.
</s>
<s>
Rozvinutá	rozvinutý	k2eAgFnSc1d1	rozvinutá
je	být	k5eAaImIp3nS	být
též	též	k9	též
produkce	produkce	k1gFnSc1	produkce
vlny	vlna	k1gFnSc2	vlna
<g/>
,	,	kIx,	,
medu	med	k1gInSc2	med
a	a	k8xC	a
rybích	rybí	k2eAgInPc2d1	rybí
výrobků	výrobek	k1gInPc2	výrobek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Turistický	turistický	k2eAgInSc4d1	turistický
ruch	ruch	k1gInSc4	ruch
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
příjmy	příjem	k1gInPc1	příjem
z	z	k7c2	z
turistického	turistický	k2eAgInSc2d1	turistický
ruchu	ruch	k1gInSc2	ruch
v	v	k7c6	v
Bulharsku	Bulharsko	k1gNnSc3	Bulharsko
rapidně	rapidně	k6eAd1	rapidně
vzrostly	vzrůst	k5eAaPmAgFnP	vzrůst
<g/>
.	.	kIx.	.
</s>
<s>
Přímořská	přímořský	k2eAgNnPc1d1	přímořské
letoviska	letovisko	k1gNnPc1	letovisko
(	(	kIx(	(
<g/>
Sozopol	Sozopol	k1gInSc1	Sozopol
<g/>
,	,	kIx,	,
Nesebar	Nesebar	k1gInSc1	Nesebar
<g/>
,	,	kIx,	,
Zlaté	zlatý	k2eAgInPc1d1	zlatý
písky	písek	k1gInPc1	písek
<g/>
,	,	kIx,	,
Slunečné	slunečný	k2eAgNnSc1d1	slunečné
pobřeží	pobřeží	k1gNnSc1	pobřeží
<g/>
,	,	kIx,	,
Primorsko	Primorsko	k1gNnSc1	Primorsko
<g/>
,	,	kIx,	,
Albena	Albena	k1gFnSc1	Albena
<g/>
,	,	kIx,	,
Varna	Varna	k1gFnSc1	Varna
<g/>
,	,	kIx,	,
Burgas	Burgas	k1gInSc1	Burgas
<g/>
,	,	kIx,	,
Kranevo	Kranevo	k1gNnSc1	Kranevo
a	a	k8xC	a
Balčik	Balčik	k1gInSc1	Balčik
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
oblíbená	oblíbený	k2eAgNnPc1d1	oblíbené
turisty	turist	k1gMnPc4	turist
ze	z	k7c2	z
západních	západní	k2eAgFnPc2d1	západní
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
i	i	k9	i
z	z	k7c2	z
Česka	Česko	k1gNnSc2	Česko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimní	zimní	k2eAgFnSc6d1	zimní
sezoně	sezona	k1gFnSc6	sezona
jsou	být	k5eAaImIp3nP	být
populární	populární	k2eAgNnPc1d1	populární
lyžařská	lyžařský	k2eAgNnPc1d1	lyžařské
střediska	středisko	k1gNnPc1	středisko
Bansko	Bansko	k1gNnSc1	Bansko
v	v	k7c6	v
Pirinu	Pirin	k1gInSc6	Pirin
<g/>
,	,	kIx,	,
Pamporovo	Pamporův	k2eAgNnSc1d1	Pamporovo
v	v	k7c6	v
Rodopech	Rodop	k1gInPc6	Rodop
a	a	k8xC	a
Borovec	Borovec	k1gMnSc1	Borovec
v	v	k7c6	v
Rile	Rile	k1gFnSc6	Rile
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Turistický	turistický	k2eAgInSc1d1	turistický
ruch	ruch	k1gInSc1	ruch
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
potýká	potýkat	k5eAaImIp3nS	potýkat
s	s	k7c7	s
rostoucím	rostoucí	k2eAgInSc7d1	rostoucí
nedostatkem	nedostatek	k1gInSc7	nedostatek
kvalifikovaných	kvalifikovaný	k2eAgFnPc2d1	kvalifikovaná
pracovních	pracovní	k2eAgFnPc2d1	pracovní
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
bulharští	bulharský	k2eAgMnPc1d1	bulharský
kuchaři	kuchař	k1gMnPc1	kuchař
<g/>
,	,	kIx,	,
servírky	servírka	k1gFnPc1	servírka
a	a	k8xC	a
pracovnice	pracovnice	k1gFnPc1	pracovnice
v	v	k7c6	v
hotelech	hotel	k1gInPc6	hotel
odcházejí	odcházet	k5eAaImIp3nP	odcházet
na	na	k7c4	na
západ	západ	k1gInSc4	západ
Evropy	Evropa	k1gFnSc2	Evropa
i	i	k9	i
do	do	k7c2	do
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
,	,	kIx,	,
bulharské	bulharský	k2eAgFnSc2d1	bulharská
restaurace	restaurace	k1gFnSc2	restaurace
a	a	k8xC	a
hotely	hotel	k1gInPc1	hotel
jsou	být	k5eAaImIp3nP	být
nuceny	nutit	k5eAaImNgFnP	nutit
hledat	hledat	k5eAaImF	hledat
za	za	k7c4	za
ně	on	k3xPp3gMnPc4	on
náhradu	náhrada	k1gFnSc4	náhrada
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
a	a	k8xC	a
v	v	k7c6	v
Moldavsku	Moldavsko	k1gNnSc6	Moldavsko
<g/>
,	,	kIx,	,
zemích	zem	k1gFnPc6	zem
s	s	k7c7	s
obdobně	obdobně	k6eAd1	obdobně
nízkou	nízký	k2eAgFnSc7d1	nízká
nebo	nebo	k8xC	nebo
ještě	ještě	k6eAd1	ještě
nižší	nízký	k2eAgFnSc7d2	nižší
životní	životní	k2eAgFnSc7d1	životní
úrovní	úroveň	k1gFnSc7	úroveň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Letectví	letectví	k1gNnSc2	letectví
===	===	k?	===
</s>
</p>
<p>
<s>
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
má	mít	k5eAaImIp3nS	mít
pět	pět	k4xCc4	pět
mezinárodních	mezinárodní	k2eAgNnPc2d1	mezinárodní
letišť	letiště	k1gNnPc2	letiště
s	s	k7c7	s
pravidelným	pravidelný	k2eAgInSc7d1	pravidelný
mezinárodním	mezinárodní	k2eAgInSc7d1	mezinárodní
provozem	provoz	k1gInSc7	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
letiště	letiště	k1gNnSc1	letiště
Sofie	Sofia	k1gFnSc2	Sofia
přepravilo	přepravit	k5eAaPmAgNnS	přepravit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
5	[number]	k4	5
milionů	milion	k4xCgInPc2	milion
cestujících	cestující	k1gMnPc2	cestující
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
letiště	letiště	k1gNnSc4	letiště
Burgas	Burgas	k1gInSc1	Burgas
<g/>
,	,	kIx,	,
Varna	Varna	k1gFnSc1	Varna
<g/>
,	,	kIx,	,
Plovdiv	Plovdiv	k1gInSc1	Plovdiv
a	a	k8xC	a
Letiště	letiště	k1gNnSc1	letiště
Gorna	Gorn	k1gMnSc2	Gorn
Oryahovitsa	Oryahovits	k1gMnSc2	Oryahovits
(	(	kIx(	(
<g/>
u	u	k7c2	u
města	město	k1gNnSc2	město
Veliko	Velika	k1gFnSc5	Velika
Tarnovo	Tarnův	k2eAgNnSc1d1	Tarnovo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vlajková	vlajkový	k2eAgFnSc1d1	vlajková
letecká	letecký	k2eAgFnSc1d1	letecká
společnost	společnost	k1gFnSc1	společnost
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Bulgaria	Bulgarium	k1gNnSc2	Bulgarium
Air	Air	k1gFnPc2	Air
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
letecké	letecký	k2eAgFnPc4d1	letecká
společnosti	společnost	k1gFnPc4	společnost
patří	patřit	k5eAaImIp3nS	patřit
BH	BH	kA	BH
Air	Air	k1gMnSc1	Air
či	či	k8xC	či
Bulgarian	Bulgarian	k1gMnSc1	Bulgarian
Air	Air	k1gMnSc1	Air
Charter	Charter	k1gMnSc1	Charter
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
má	mít	k5eAaImIp3nS	mít
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
značně	značně	k6eAd1	značně
negativní	negativní	k2eAgInSc4d1	negativní
populační	populační	k2eAgInSc4d1	populační
růst	růst	k1gInSc4	růst
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
způsoben	způsobit	k5eAaPmNgInS	způsobit
jednak	jednak	k8xC	jednak
malou	malý	k2eAgFnSc7d1	malá
porodností	porodnost	k1gFnSc7	porodnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
významnou	významný	k2eAgFnSc7d1	významná
emigrací	emigrace	k1gFnSc7	emigrace
kvůli	kvůli	k7c3	kvůli
špatnému	špatný	k2eAgInSc3d1	špatný
hospodářskému	hospodářský	k2eAgInSc3d1	hospodářský
vývoji	vývoj	k1gInSc3	vývoj
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
probíhalo	probíhat	k5eAaImAgNnS	probíhat
rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
vystěhovalectví	vystěhovalectví	k1gNnSc1	vystěhovalectví
Turků	turek	k1gInPc2	turek
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
vláda	vláda	k1gFnSc1	vláda
Todora	Todora	k1gFnSc1	Todora
Živkova	Živkov	k1gInSc2	Živkov
přijala	přijmout	k5eAaPmAgFnS	přijmout
zákon	zákon	k1gInSc4	zákon
o	o	k7c4	o
pobulharštění	pobulharštění	k1gNnPc4	pobulharštění
jejich	jejich	k3xOp3gNnPc2	jejich
jmen	jméno	k1gNnPc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
320	[number]	k4	320
tisíc	tisíc	k4xCgInPc2	tisíc
příslušníků	příslušník	k1gMnPc2	příslušník
turecké	turecký	k2eAgFnSc2d1	turecká
menšiny	menšina	k1gFnSc2	menšina
odešlo	odejít	k5eAaPmAgNnS	odejít
z	z	k7c2	z
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
do	do	k7c2	do
sousedního	sousední	k2eAgNnSc2d1	sousední
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Exodus	Exodus	k1gInSc1	Exodus
ovšem	ovšem	k9	ovšem
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
i	i	k9	i
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
komunismu	komunismus	k1gInSc2	komunismus
a	a	k8xC	a
po	po	k7c6	po
vstupu	vstup	k1gInSc6	vstup
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
EU	EU	kA	EU
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
řadě	řada	k1gFnSc3	řada
Bulharů	Bulhar	k1gMnPc2	Bulhar
otevřela	otevřít	k5eAaPmAgFnS	otevřít
možnost	možnost	k1gFnSc4	možnost
pracovat	pracovat	k5eAaImF	pracovat
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
zemi	zem	k1gFnSc6	zem
opustilo	opustit	k5eAaPmAgNnS	opustit
937	[number]	k4	937
000	[number]	k4	000
až	až	k9	až
1	[number]	k4	1
200	[number]	k4	200
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
mladých	mladý	k2eAgMnPc2d1	mladý
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
má	mít	k5eAaImIp3nS	mít
země	země	k1gFnSc1	země
sotva	sotva	k6eAd1	sotva
7	[number]	k4	7
milionů	milion	k4xCgInPc2	milion
trvale	trvale	k6eAd1	trvale
bydlících	bydlící	k2eAgMnPc2d1	bydlící
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
než	než	k8xS	než
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
<g/>
.	.	kIx.	.
</s>
<s>
Masovou	masový	k2eAgFnSc4d1	masová
emigraci	emigrace	k1gFnSc4	emigrace
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
zhruba	zhruba	k6eAd1	zhruba
pětinásobně	pětinásobně	k6eAd1	pětinásobně
vyšší	vysoký	k2eAgFnPc4d2	vyšší
mzdy	mzda	k1gFnPc4	mzda
ve	v	k7c6	v
vyspělých	vyspělý	k2eAgFnPc6d1	vyspělá
západoevropských	západoevropský	k2eAgFnPc6d1	západoevropská
zemích	zem	k1gFnPc6	zem
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
Bulharskem	Bulharsko	k1gNnSc7	Bulharsko
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
osídlení	osídlení	k1gNnSc2	osídlení
je	být	k5eAaImIp3nS	být
65	[number]	k4	65
lidí	člověk	k1gMnPc2	člověk
na	na	k7c4	na
kilometr	kilometr	k1gInSc4	kilometr
čtvereční	čtvereční	k2eAgInSc4d1	čtvereční
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
polovina	polovina	k1gFnSc1	polovina
průměru	průměr	k1gInSc2	průměr
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
porodnosti	porodnost	k1gFnSc2	porodnost
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
odhadnuta	odhadnut	k2eAgFnSc1d1	odhadnuta
na	na	k7c4	na
1,46	[number]	k4	1,46
dětí	dítě	k1gFnPc2	dítě
na	na	k7c4	na
ženu	žena	k1gFnSc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
dětí	dítě	k1gFnPc2	dítě
se	se	k3xPyFc4	se
narodí	narodit	k5eAaPmIp3nS	narodit
svobodným	svobodný	k2eAgFnPc3d1	svobodná
matkám	matka	k1gFnPc3	matka
<g/>
.	.	kIx.	.
</s>
<s>
Jednu	jeden	k4xCgFnSc4	jeden
třetinu	třetina	k1gFnSc4	třetina
všech	všecek	k3xTgFnPc2	všecek
domácností	domácnost	k1gFnPc2	domácnost
tvoří	tvořit	k5eAaImIp3nS	tvořit
pouze	pouze	k6eAd1	pouze
jedna	jeden	k4xCgFnSc1	jeden
osoba	osoba	k1gFnSc1	osoba
a	a	k8xC	a
75,5	[number]	k4	75,5
<g/>
%	%	kIx~	%
rodin	rodina	k1gFnPc2	rodina
nemá	mít	k5eNaImIp3nS	mít
děti	dítě	k1gFnPc1	dítě
mladší	mladý	k2eAgFnPc1d2	mladší
16	[number]	k4	16
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
je	být	k5eAaImIp3nS	být
74,7	[number]	k4	74,7
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Míra	Míra	k1gFnSc1	Míra
úmrtnosti	úmrtnost	k1gFnSc2	úmrtnost
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
z	z	k7c2	z
kombinace	kombinace	k1gFnSc2	kombinace
stárnoucí	stárnoucí	k2eAgFnSc2d1	stárnoucí
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
vysokého	vysoký	k2eAgInSc2d1	vysoký
počtu	počet	k1gInSc2	počet
osob	osoba	k1gFnPc2	osoba
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
chudobou	chudoba	k1gFnSc7	chudoba
a	a	k8xC	a
slabého	slabý	k2eAgInSc2d1	slabý
systému	systém	k1gInSc2	systém
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
péče	péče	k1gFnSc2	péče
<g/>
.	.	kIx.	.
</s>
<s>
Odborníci	odborník	k1gMnPc1	odborník
odhadli	odhadnout	k5eAaPmAgMnP	odhadnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
téměř	téměř	k6eAd1	téměř
pětině	pětina	k1gFnSc3	pětina
úmrtí	úmrť	k1gFnPc2	úmrť
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
předejít	předejít	k5eAaPmF	předejít
kvalitní	kvalitní	k2eAgFnSc7d1	kvalitní
péčí	péče	k1gFnSc7	péče
a	a	k8xC	a
včasným	včasný	k2eAgInSc7d1	včasný
přístupem	přístup	k1gInSc7	přístup
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Problémy	problém	k1gInPc1	problém
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
narušují	narušovat	k5eAaImIp3nP	narušovat
poskytování	poskytování	k1gNnSc4	poskytování
péče	péče	k1gFnSc2	péče
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
emigrace	emigrace	k1gFnPc1	emigrace
lékařů	lékař	k1gMnPc2	lékař
kvůli	kvůli	k7c3	kvůli
nízké	nízký	k2eAgFnSc3d1	nízká
mzdě	mzda	k1gFnSc3	mzda
<g/>
,	,	kIx,	,
nedostatečný	dostatečný	k2eNgInSc1d1	nedostatečný
počet	počet	k1gInSc1	počet
zdravotnického	zdravotnický	k2eAgInSc2d1	zdravotnický
personálu	personál	k1gInSc2	personál
a	a	k8xC	a
nedostatečně	dostatečně	k6eNd1	dostatečně
vybavené	vybavený	k2eAgFnSc2d1	vybavená
regionální	regionální	k2eAgFnSc2d1	regionální
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
balíček	balíček	k1gInSc1	balíček
služeb	služba	k1gFnPc2	služba
pro	pro	k7c4	pro
pojištěnce	pojištěnec	k1gMnPc4	pojištěnec
se	se	k3xPyFc4	se
také	také	k9	také
často	často	k6eAd1	často
mění	měnit	k5eAaImIp3nS	měnit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
systém	systém	k1gInSc1	systém
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
deficitů	deficit	k1gInPc2	deficit
<g/>
.	.	kIx.	.
</s>
<s>
Bloomberg	Bloomberg	k1gMnSc1	Bloomberg
Health	Health	k1gMnSc1	Health
Efficiency	Efficienca	k1gFnSc2	Efficienca
Index	index	k1gInSc1	index
zařadil	zařadit	k5eAaPmAgMnS	zařadit
Bulharsko	Bulharsko	k1gNnSc4	Bulharsko
na	na	k7c4	na
poslední	poslední	k2eAgNnSc4d1	poslední
místo	místo	k1gNnSc4	místo
z	z	k7c2	z
56	[number]	k4	56
zemí	zem	k1gFnPc2	zem
v	v	k7c6	v
kvalitě	kvalita	k1gFnSc6	kvalita
lékařské	lékařský	k2eAgFnSc2d1	lékařská
péče	péče	k1gFnSc2	péče
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Etnické	etnický	k2eAgNnSc4d1	etnické
složení	složení	k1gNnSc4	složení
===	===	k?	===
</s>
</p>
<p>
<s>
Bulhaři	Bulhar	k1gMnPc1	Bulhar
84	[number]	k4	84
%	%	kIx~	%
(	(	kIx(	(
<g/>
6	[number]	k4	6
655	[number]	k4	655
210	[number]	k4	210
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Turci	Turek	k1gMnPc1	Turek
9,3	[number]	k4	9,3
%	%	kIx~	%
(	(	kIx(	(
<g/>
746	[number]	k4	746
664	[number]	k4	664
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Romové	Rom	k1gMnPc1	Rom
4,7	[number]	k4	4,7
%	%	kIx~	%
(	(	kIx(	(
<g/>
370	[number]	k4	370
908	[number]	k4	908
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
dále	daleko	k6eAd2	daleko
Makedonci	Makedonec	k1gMnPc1	Makedonec
<g/>
,	,	kIx,	,
Arméni	Armén	k1gMnPc1	Armén
<g/>
,	,	kIx,	,
Rusové	Rus	k1gMnPc1	Rus
<g/>
,	,	kIx,	,
Řekové	Řek	k1gMnPc1	Řek
<g/>
,	,	kIx,	,
ŽidéRomská	ŽidéRomský	k2eAgFnSc1d1	ŽidéRomský
menšina	menšina	k1gFnSc1	menšina
je	být	k5eAaImIp3nS	být
však	však	k9	však
obvykle	obvykle	k6eAd1	obvykle
při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
lidu	lid	k1gInSc2	lid
podceňována	podceňovat	k5eAaImNgFnS	podceňovat
a	a	k8xC	a
podle	podle	k7c2	podle
alternativních	alternativní	k2eAgInPc2d1	alternativní
odhadů	odhad	k1gInPc2	odhad
může	moct	k5eAaImIp3nS	moct
představovat	představovat	k5eAaImF	představovat
až	až	k9	až
11	[number]	k4	11
<g/>
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
nejméně	málo	k6eAd3	málo
800	[number]	k4	800
tisíc	tisíc	k4xCgInPc2	tisíc
Romů	Rom	k1gMnPc2	Rom
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
etnické	etnický	k2eAgFnPc1d1	etnická
skupiny	skupina	k1gFnPc1	skupina
hovoří	hovořit	k5eAaImIp3nP	hovořit
bulharsky	bulharsky	k6eAd1	bulharsky
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
bulharštinu	bulharština	k1gFnSc4	bulharština
buď	buď	k8xC	buď
jako	jako	k9	jako
první	první	k4xOgNnSc1	první
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
druhý	druhý	k4xOgInSc1	druhý
jazyk	jazyk	k1gInSc1	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Bulharština	bulharština	k1gFnSc1	bulharština
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgInSc7d1	jediný
oficiálním	oficiální	k2eAgInSc7d1	oficiální
jazykem	jazyk	k1gInSc7	jazyk
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Bulhaři	Bulhar	k1gMnPc1	Bulhar
nebyli	být	k5eNaImAgMnP	být
původně	původně	k6eAd1	původně
zcela	zcela	k6eAd1	zcela
Slované	Slovan	k1gMnPc1	Slovan
<g/>
,	,	kIx,	,
z	z	k7c2	z
etnického	etnický	k2eAgNnSc2d1	etnické
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bulharština	bulharština	k1gFnSc1	bulharština
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
přijali	přijmout	k5eAaPmAgMnP	přijmout
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
slovanským	slovanský	k2eAgInSc7d1	slovanský
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
slovanských	slovanský	k2eAgInPc2d1	slovanský
jazyků	jazyk	k1gInPc2	jazyk
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
určitými	určitý	k2eAgFnPc7d1	určitá
gramatickými	gramatický	k2eAgFnPc7d1	gramatická
zvláštnostmi	zvláštnost	k1gFnPc7	zvláštnost
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
neskloňování	neskloňování	k1gNnSc1	neskloňování
podstatných	podstatný	k2eAgNnPc2d1	podstatné
jmen	jméno	k1gNnPc2	jméno
a	a	k8xC	a
neexistence	neexistence	k1gFnSc2	neexistence
infinitivů	infinitiv	k1gInPc2	infinitiv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Náboženství	náboženství	k1gNnSc2	náboženství
===	===	k?	===
</s>
</p>
<p>
<s>
pravoslavní	pravoslavný	k2eAgMnPc1d1	pravoslavný
(	(	kIx(	(
<g/>
Bulharská	bulharský	k2eAgFnSc1d1	bulharská
pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
<g/>
)	)	kIx)	)
–	–	k?	–
85	[number]	k4	85
%	%	kIx~	%
</s>
</p>
<p>
<s>
muslimové-sunnité	muslimovéunnitý	k2eAgInPc1d1	muslimové-sunnitý
–	–	k?	–
13	[number]	k4	13
%	%	kIx~	%
</s>
</p>
<p>
<s>
judaismus	judaismus	k1gInSc1	judaismus
–	–	k?	–
0,8	[number]	k4	0,8
%	%	kIx~	%
</s>
</p>
<p>
<s>
katolíci	katolík	k1gMnPc1	katolík
–	–	k?	–
0,5	[number]	k4	0,5
%	%	kIx~	%
<g/>
Kromě	kromě	k7c2	kromě
etnických	etnický	k2eAgInPc2d1	etnický
Turků	turek	k1gInPc2	turek
se	se	k3xPyFc4	se
k	k	k7c3	k
islámu	islám	k1gInSc3	islám
hlásí	hlásit	k5eAaImIp3nS	hlásit
také	také	k9	také
část	část	k1gFnSc1	část
Romů	Rom	k1gMnPc2	Rom
a	a	k8xC	a
Bulhaři	Bulhar	k1gMnPc1	Bulhar
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
během	během	k7c2	během
turecké	turecký	k2eAgFnSc2d1	turecká
nadvlády	nadvláda	k1gFnSc2	nadvláda
přijali	přijmout	k5eAaPmAgMnP	přijmout
islám	islám	k1gInSc4	islám
<g/>
,	,	kIx,	,
takzvaní	takzvaný	k2eAgMnPc1d1	takzvaný
Pomaci	Pomak	k1gMnPc1	Pomak
<g/>
.	.	kIx.	.
</s>
<s>
Bulharská	bulharský	k2eAgFnSc1d1	bulharská
pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
získala	získat	k5eAaPmAgFnS	získat
autokefální	autokefální	k2eAgInSc4d1	autokefální
status	status	k1gInSc4	status
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
927	[number]	k4	927
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
hlavního	hlavní	k2eAgMnSc4d1	hlavní
patrona	patron	k1gMnSc4	patron
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
a	a	k8xC	a
nejdůležitějšího	důležitý	k2eAgMnSc4d3	nejdůležitější
svatého	svatý	k1gMnSc4	svatý
bulharské	bulharský	k2eAgFnSc2d1	bulharská
pravoslavné	pravoslavný	k2eAgFnSc2d1	pravoslavná
církve	církev	k1gFnSc2	církev
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
Ivan	Ivan	k1gMnSc1	Ivan
Rilský	rilský	k2eAgMnSc1d1	rilský
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k9	už
za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
těšil	těšit	k5eAaImAgInS	těšit
vážnosti	vážnost	k1gFnSc3	vážnost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
přiváděla	přivádět	k5eAaImAgFnS	přivádět
žáky	žák	k1gMnPc4	žák
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
založili	založit	k5eAaPmAgMnP	založit
nedaleko	nedaleko	k7c2	nedaleko
jeskyně	jeskyně	k1gFnSc2	jeskyně
<g/>
,	,	kIx,	,
v	v	k7c4	v
niž	jenž	k3xRgFnSc4	jenž
žil	žít	k5eAaImAgInS	žít
<g/>
,	,	kIx,	,
Rilský	rilský	k2eAgInSc4d1	rilský
klášter	klášter	k1gInSc4	klášter
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Bulharská	bulharský	k2eAgFnSc1d1	bulharská
literatura	literatura	k1gFnSc1	literatura
má	mít	k5eAaImIp3nS	mít
hluboké	hluboký	k2eAgInPc4d1	hluboký
kořeny	kořen	k1gInPc4	kořen
<g/>
,	,	kIx,	,
preslavská	preslavský	k2eAgFnSc1d1	preslavský
a	a	k8xC	a
ochridská	ochridský	k2eAgFnSc1d1	ochridský
škola	škola	k1gFnSc1	škola
učinily	učinit	k5eAaPmAgInP	učinit
v	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Bulharskou	bulharský	k2eAgFnSc4d1	bulharská
říši	říše	k1gFnSc4	říše
středem	středem	k7c2	středem
slovanské	slovanský	k2eAgFnSc2d1	Slovanská
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
hlavním	hlavní	k2eAgFnPc3d1	hlavní
osobnostem	osobnost	k1gFnPc3	osobnost
patřil	patřit	k5eAaImAgMnS	patřit
žák	žák	k1gMnSc1	žák
Cyrila	Cyril	k1gMnSc2	Cyril
a	a	k8xC	a
Metoděje	Metoděj	k1gMnSc2	Metoděj
<g/>
,	,	kIx,	,
Kliment	Kliment	k1gMnSc1	Kliment
Ochridský	Ochridský	k2eAgMnSc1d1	Ochridský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Preslavi	Preslaev	k1gFnSc6	Preslaev
bylo	být	k5eAaImAgNnS	být
vyvinuto	vyvinout	k5eAaPmNgNnS	vyvinout
písmo	písmo	k1gNnSc1	písmo
cyrilice	cyrilice	k1gFnSc2	cyrilice
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
pak	pak	k6eAd1	pak
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
celou	celý	k2eAgFnSc4d1	celá
východoevropskou	východoevropský	k2eAgFnSc4d1	východoevropská
kulturu	kultura	k1gFnSc4	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Preslavsko-ochridská	Preslavskochridský	k2eAgFnSc1d1	Preslavsko-ochridský
éra	éra	k1gFnSc1	éra
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
nazývána	nazývat	k5eAaImNgFnS	nazývat
zlatým	zlatý	k2eAgInSc7d1	zlatý
věkem	věk	k1gInSc7	věk
bulharské	bulharský	k2eAgFnSc2d1	bulharská
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Věkem	věk	k1gInSc7	věk
stříbrným	stříbrný	k2eAgInSc7d1	stříbrný
bývá	bývat	k5eAaImIp3nS	bývat
nazýváno	nazýván	k2eAgNnSc4d1	nazýváno
dílo	dílo	k1gNnSc4	dílo
pozdější	pozdní	k2eAgNnSc4d2	pozdější
tarnovské	tarnovský	k2eAgFnPc4d1	tarnovský
literární	literární	k2eAgFnPc4d1	literární
školy	škola	k1gFnPc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Důležitými	důležitý	k2eAgFnPc7d1	důležitá
postavami	postava	k1gFnPc7	postava
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
literárního	literární	k2eAgNnSc2d1	literární
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
Ljuben	Ljuben	k2eAgInSc4d1	Ljuben
Karavelov	Karavelov	k1gInSc4	Karavelov
a	a	k8xC	a
Georgi	George	k1gFnSc4	George
Rakovski	Rakovsk	k1gFnSc2	Rakovsk
<g/>
.	.	kIx.	.
</s>
<s>
Aleko	Aleko	k6eAd1	Aleko
Konstantinov	Konstantinov	k1gInSc1	Konstantinov
je	být	k5eAaImIp3nS	být
tvůrcem	tvůrce	k1gMnSc7	tvůrce
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
nejpopulárnějších	populární	k2eAgFnPc2d3	nejpopulárnější
postav	postava	k1gFnPc2	postava
bulharské	bulharský	k2eAgFnSc2d1	bulharská
literatury	literatura	k1gFnSc2	literatura
-	-	kIx~	-
Baj	bájit	k5eAaImRp2nS	bájit
Gaňa	Gaň	k1gInSc2	Gaň
<g/>
.	.	kIx.	.
</s>
<s>
Klasiky	klasika	k1gFnPc1	klasika
realistické	realistický	k2eAgFnSc2d1	realistická
prózy	próza	k1gFnSc2	próza
jsou	být	k5eAaImIp3nP	být
Elin	Elin	k2eAgMnSc1d1	Elin
Pelin	Pelin	k1gMnSc1	Pelin
a	a	k8xC	a
Jordan	Jordan	k1gMnSc1	Jordan
Jovkov	Jovkov	k1gInSc1	Jovkov
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
bulharsky	bulharsky	k6eAd1	bulharsky
píšícím	píšící	k2eAgMnSc7d1	píšící
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
Ivan	Ivan	k1gMnSc1	Ivan
Vazov	Vazov	k1gInSc1	Vazov
<g/>
.	.	kIx.	.
</s>
<s>
Komunistický	komunistický	k2eAgMnSc1d1	komunistický
básník	básník	k1gMnSc1	básník
Nikola	Nikola	k1gMnSc1	Nikola
Vapcarov	Vapcarovo	k1gNnPc2	Vapcarovo
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
legendou	legenda	k1gFnSc7	legenda
jako	jako	k9	jako
oběť	oběť	k1gFnSc4	oběť
fašismu	fašismus	k1gInSc2	fašismus
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgMnS	prosadit
Stanislav	Stanislav	k1gMnSc1	Stanislav
Stratiev	Strativa	k1gFnPc2	Strativa
či	či	k8xC	či
disident	disident	k1gMnSc1	disident
Georgi	Georg	k1gMnSc3	Georg
Markov	Markov	k1gInSc4	Markov
<g/>
,	,	kIx,	,
zavražděný	zavražděný	k2eAgInSc1d1	zavražděný
bulharskou	bulharský	k2eAgFnSc7d1	bulharská
tajnou	tajný	k2eAgFnSc7d1	tajná
službou	služba	k1gFnSc7	služba
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Deštníková	deštníkový	k2eAgFnSc1d1	deštníková
vražda	vražda	k1gFnSc1	vražda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
Elias	Eliasa	k1gFnPc2	Eliasa
Canetti	Canetť	k1gFnSc2	Canetť
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
však	však	k9	však
z	z	k7c2	z
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
odešel	odejít	k5eAaPmAgMnS	odejít
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
ještě	ještě	k9	ještě
jako	jako	k8xC	jako
dítě	dítě	k1gNnSc1	dítě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celosvětové	celosvětový	k2eAgFnPc4d1	celosvětová
proslulosti	proslulost	k1gFnPc4	proslulost
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
harfistka	harfistka	k1gFnSc1	harfistka
a	a	k8xC	a
hudební	hudební	k2eAgFnSc1d1	hudební
skladatelka	skladatelka	k1gFnSc1	skladatelka
Anna-Maria	Anna-Marium	k1gNnSc2	Anna-Marium
Ravnopolska-Dean	Ravnopolska-Deany	k1gInPc2	Ravnopolska-Deany
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
operní	operní	k2eAgMnPc1d1	operní
pěvci	pěvec	k1gMnPc1	pěvec
Boris	Boris	k1gMnSc1	Boris
Christov	Christov	k1gInSc1	Christov
<g/>
,	,	kIx,	,
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Gjaurov	Gjaurovo	k1gNnPc2	Gjaurovo
a	a	k8xC	a
Raina	Raien	k2eAgNnPc4d1	Raien
Kabaivanska	Kabaivansko	k1gNnPc4	Kabaivansko
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
věhlasu	věhlas	k1gInSc2	věhlas
<g/>
.	.	kIx.	.
</s>
<s>
Unikátní	unikátní	k2eAgFnSc1d1	unikátní
je	být	k5eAaImIp3nS	být
bulharská	bulharský	k2eAgFnSc1d1	bulharská
lidová	lidový	k2eAgFnSc1d1	lidová
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
rytmický	rytmický	k2eAgInSc1d1	rytmický
čas	čas	k1gInSc1	čas
nemá	mít	k5eNaImIp3nS	mít
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
obdoby	obdoba	k1gFnSc2	obdoba
<g/>
.	.	kIx.	.
</s>
<s>
Folklórní	folklórní	k2eAgInPc4d1	folklórní
motivy	motiv	k1gInPc4	motiv
se	se	k3xPyFc4	se
ve	v	k7c6	v
vážné	vážný	k2eAgFnSc6d1	vážná
hudbě	hudba	k1gFnSc6	hudba
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
využít	využít	k5eAaPmF	využít
zejména	zejména	k9	zejména
Pančo	Pančo	k6eAd1	Pančo
Vladigerov	Vladigerov	k1gInSc4	Vladigerov
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
klíčovým	klíčový	k2eAgFnPc3d1	klíčová
osobnostem	osobnost	k1gFnPc3	osobnost
bulharské	bulharský	k2eAgFnSc2d1	bulharská
pop-music	popusic	k1gFnSc2	pop-music
patří	patřit	k5eAaImIp3nS	patřit
Lili	Lili	k1gFnSc1	Lili
Ivanovová	Ivanovová	k1gFnSc1	Ivanovová
či	či	k8xC	či
Sylvie	Sylvie	k1gFnSc1	Sylvie
Vartanová	Vartanový	k2eAgFnSc1d1	Vartanový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
výtvarných	výtvarný	k2eAgMnPc2d1	výtvarný
umělců	umělec	k1gMnPc2	umělec
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
největšího	veliký	k2eAgInSc2d3	veliký
úspěchu	úspěch	k1gInSc2	úspěch
představitel	představitel	k1gMnSc1	představitel
land	land	k1gMnSc1	land
artu	aríst	k5eAaPmIp1nS	aríst
Christo	Christa	k1gMnSc5	Christa
Javačev	Javačev	k1gMnSc5	Javačev
<g/>
.	.	kIx.	.
</s>
<s>
Stavy	stav	k1gInPc1	stav
bohémy	bohéma	k1gFnSc2	bohéma
na	na	k7c6	na
pařížském	pařížský	k2eAgInSc6d1	pařížský
Montparnassu	Montparnass	k1gInSc6	Montparnass
doplnil	doplnit	k5eAaPmAgMnS	doplnit
malíř	malíř	k1gMnSc1	malíř
Jules	Jules	k1gMnSc1	Jules
Pascin	Pascin	k1gMnSc1	Pascin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
zámořském	zámořský	k2eAgInSc6d1	zámořský
televizním	televizní	k2eAgInSc6d1	televizní
průmyslu	průmysl	k1gInSc6	průmysl
se	se	k3xPyFc4	se
prosadila	prosadit	k5eAaPmAgFnS	prosadit
herečka	herečka	k1gFnSc1	herečka
Nina	Nina	k1gFnSc1	Nina
Dobrevová	Dobrevová	k1gFnSc1	Dobrevová
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
Upíří	upíří	k2eAgInPc1d1	upíří
deníky	deník	k1gInPc1	deník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
sérii	série	k1gFnSc6	série
o	o	k7c4	o
Harrym	Harrym	k1gInSc4	Harrym
Potterovi	Potter	k1gMnSc3	Potter
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
Stanislav	Stanislav	k1gMnSc1	Stanislav
Janevski	Janevsk	k1gFnSc2	Janevsk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Architektonickými	architektonický	k2eAgFnPc7d1	architektonická
památkami	památka	k1gFnPc7	památka
zapsanými	zapsaný	k2eAgFnPc7d1	zapsaná
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
kulturního	kulturní	k2eAgNnSc2d1	kulturní
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	Unesco	k1gNnSc1	Unesco
jsou	být	k5eAaImIp3nP	být
Bojanský	Bojanský	k2eAgInSc4d1	Bojanský
kostel	kostel	k1gInSc4	kostel
<g/>
,	,	kIx,	,
Skalní	skalní	k2eAgInPc1d1	skalní
kostely	kostel	k1gInPc1	kostel
v	v	k7c4	v
Ivanovu	Ivanův	k2eAgFnSc4d1	Ivanova
<g/>
,	,	kIx,	,
antické	antický	k2eAgNnSc4d1	antické
město	město	k1gNnSc4	město
Nesebar	Nesebara	k1gFnPc2	Nesebara
či	či	k8xC	či
Rilský	rilský	k2eAgInSc1d1	rilský
klášter	klášter	k1gInSc1	klášter
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInSc1d3	veliký
klášter	klášter	k1gInSc1	klášter
pravoslavné	pravoslavný	k2eAgFnSc2d1	pravoslavná
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
klíčové	klíčový	k2eAgNnSc1d1	klíčové
centrum	centrum	k1gNnSc1	centrum
duchovního	duchovní	k2eAgInSc2d1	duchovní
a	a	k8xC	a
společenského	společenský	k2eAgInSc2d1	společenský
životě	život	k1gInSc6	život
středověkého	středověký	k2eAgNnSc2d1	středověké
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Živý	živý	k1gMnSc1	živý
je	být	k5eAaImIp3nS	být
také	také	k9	také
bulharský	bulharský	k2eAgInSc1d1	bulharský
folklór	folklór	k1gInSc1	folklór
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
nehmotného	hmotný	k2eNgNnSc2d1	nehmotné
kulturního	kulturní	k2eAgNnSc2d1	kulturní
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc1	tento
památky	památka	k1gFnPc1	památka
<g/>
:	:	kIx,	:
rituál	rituál	k1gInSc1	rituál
nestinarstvo	nestinarstvo	k1gNnSc1	nestinarstvo
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
zahrnující	zahrnující	k2eAgInSc1d1	zahrnující
tanec	tanec	k1gInSc1	tanec
po	po	k7c6	po
žhavých	žhavý	k2eAgInPc6d1	žhavý
uhlících	uhlík	k1gInPc6	uhlík
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
tradicí	tradice	k1gFnSc7	tradice
zejména	zejména	k9	zejména
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Strandža	Strandž	k1gInSc2	Strandž
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
soubor	soubor	k1gInSc1	soubor
Bistriškite	Bistriškit	k1gInSc5	Bistriškit
babi	babi	k?	babi
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tradiční	tradiční	k2eAgInSc1d1	tradiční
<g />
.	.	kIx.	.
</s>
<s>
výroba	výroba	k1gFnSc1	výroba
koberců	koberec	k1gInPc2	koberec
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
uvazování	uvazování	k1gNnSc4	uvazování
martenici	martenice	k1gFnSc4	martenice
(	(	kIx(	(
<g/>
červeno-bílé	červenoílý	k2eAgFnPc1d1	červeno-bílá
panenky	panenka	k1gFnPc1	panenka
<g/>
)	)	kIx)	)
na	na	k7c4	na
stromy	strom	k1gInPc4	strom
k	k	k7c3	k
uvítání	uvítání	k1gNnSc3	uvítání
jara	jaro	k1gNnSc2	jaro
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
..	..	k?	..
Mezi	mezi	k7c4	mezi
památky	památka	k1gFnPc4	památka
nehmotného	hmotný	k2eNgNnSc2d1	nehmotné
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	Unesco	k1gNnSc1	Unesco
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
fesivaly	fesival	k1gInPc4	fesival
v	v	k7c6	v
Koprivštici	Koprivštice	k1gFnSc6	Koprivštice
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
a	a	k8xC	a
Surva	Surva	k1gFnSc1	Surva
v	v	k7c6	v
Perniku	Pernik	k1gInSc6	Pernik
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
byla	být	k5eAaImAgFnS	být
zapsána	zapsat	k5eAaPmNgFnS	zapsat
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
Čitalište	Čitalište	k1gFnPc4	Čitalište
(	(	kIx(	(
<g/>
kulturní	kulturní	k2eAgFnPc4d1	kulturní
instituce	instituce	k1gFnPc4	instituce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Známý	známý	k2eAgInSc1d1	známý
je	být	k5eAaImIp3nS	být
také	také	k9	také
rituál	rituál	k1gInSc1	rituál
zahánění	zahánění	k1gNnSc2	zahánění
zlých	zlý	k2eAgMnPc2d1	zlý
duchů	duch	k1gMnPc2	duch
kukeri	kuker	k1gFnSc2	kuker
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Věda	věda	k1gFnSc1	věda
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
pocházejí	pocházet	k5eAaImIp3nP	pocházet
dva	dva	k4xCgMnPc1	dva
významní	významný	k2eAgMnPc1d1	významný
představitelé	představitel	k1gMnPc1	představitel
humanitních	humanitní	k2eAgInPc2d1	humanitní
a	a	k8xC	a
sociálních	sociální	k2eAgFnPc2d1	sociální
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
se	se	k3xPyFc4	se
oba	dva	k4xCgMnPc1	dva
stali	stát	k5eAaPmAgMnP	stát
emigranty	emigrant	k1gMnPc4	emigrant
<g/>
:	:	kIx,	:
feministická	feministický	k2eAgFnSc1d1	feministická
sémiotička	sémiotička	k1gFnSc1	sémiotička
Julia	Julius	k1gMnSc2	Julius
Kristeva	Kristeva	k1gFnSc1	Kristeva
a	a	k8xC	a
literární	literární	k2eAgMnSc1d1	literární
teoretik	teoretik	k1gMnSc1	teoretik
Tzvetan	Tzvetan	k1gInSc4	Tzvetan
Todorov	Todorovo	k1gNnPc2	Todorovo
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
nakonec	nakonec	k6eAd1	nakonec
zakořenili	zakořenit	k5eAaPmAgMnP	zakořenit
ve	v	k7c6	v
francouzském	francouzský	k2eAgNnSc6d1	francouzské
intelektuálním	intelektuální	k2eAgNnSc6d1	intelektuální
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bulhaři	Bulhar	k1gMnPc1	Bulhar
mají	mít	k5eAaImIp3nP	mít
tradici	tradice	k1gFnSc4	tradice
kosmického	kosmický	k2eAgInSc2d1	kosmický
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Georgi	Georgi	k6eAd1	Georgi
Ivanov	Ivanov	k1gInSc4	Ivanov
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
bulharským	bulharský	k2eAgMnSc7d1	bulharský
kosmonautem	kosmonaut	k1gMnSc7	kosmonaut
<g/>
,	,	kIx,	,
Alexandr	Alexandr	k1gMnSc1	Alexandr
Panajotov	Panajotov	k1gInSc1	Panajotov
Alexandrov	Alexandrov	k1gInSc1	Alexandrov
druhým	druhý	k4xOgNnSc7	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
bylo	být	k5eAaImAgNnS	být
rovněž	rovněž	k9	rovněž
první	první	k4xOgNnSc1	první
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pěstovala	pěstovat	k5eAaImAgFnS	pěstovat
pšenici	pšenice	k1gFnSc4	pšenice
a	a	k8xC	a
zeleninu	zelenina	k1gFnSc4	zelenina
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
,	,	kIx,	,
ve	v	k7c6	v
sklenících	skleník	k1gInPc6	skleník
na	na	k7c6	na
vesmírné	vesmírný	k2eAgFnSc6d1	vesmírná
stanici	stanice	k1gFnSc6	stanice
Mir	Mira	k1gFnPc2	Mira
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
Sověty	Sovět	k1gMnPc7	Sovět
Bulhaři	Bulhar	k1gMnPc1	Bulhar
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
Vega	Vegum	k1gNnSc2	Vegum
<g/>
,	,	kIx,	,
od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
spolupráce	spolupráce	k1gFnSc1	spolupráce
i	i	k9	i
se	s	k7c7	s
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
bulharská	bulharský	k2eAgNnPc1d1	bulharské
zařízení	zařízení	k1gNnPc1	zařízení
byla	být	k5eAaImAgNnP	být
použita	použít	k5eAaPmNgNnP	použít
při	při	k7c6	při
průzkumu	průzkum	k1gInSc6	průzkum
Marsu	Mars	k1gInSc2	Mars
<g/>
,	,	kIx,	,
bulharský	bulharský	k2eAgInSc1d1	bulharský
spektrometr	spektrometr	k1gInSc1	spektrometr
používaly	používat	k5eAaImAgInP	používat
například	například	k6eAd1	například
sondy	sonda	k1gFnPc4	sonda
Phobos	Phobosa	k1gFnPc2	Phobosa
a	a	k8xC	a
Phobos	Phobos	k1gInSc1	Phobos
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2017	[number]	k4	2017
americká	americký	k2eAgFnSc1d1	americká
společnost	společnost	k1gFnSc1	společnost
SpaceX	SpaceX	k1gMnPc2	SpaceX
vynesla	vynést	k5eAaPmAgFnS	vynést
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
první	první	k4xOgFnSc4	první
bulharskou	bulharský	k2eAgFnSc4d1	bulharská
geostacionární	geostacionární	k2eAgFnSc4d1	geostacionární
komunikační	komunikační	k2eAgFnSc4d1	komunikační
družici	družice	k1gFnSc4	družice
BulgariaSat-	BulgariaSat-	k1gFnSc1	BulgariaSat-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
sloužit	sloužit	k5eAaImF	sloužit
především	především	k9	především
pro	pro	k7c4	pro
televizní	televizní	k2eAgNnSc4d1	televizní
vysílání	vysílání	k1gNnSc4	vysílání
ve	v	k7c6	v
vysokém	vysoký	k2eAgNnSc6d1	vysoké
rozlišení	rozlišení	k1gNnSc6	rozlišení
<g/>
.	.	kIx.	.
</s>
<s>
Satelit	satelit	k1gInSc1	satelit
je	být	k5eAaImIp3nS	být
napájen	napájet	k5eAaImNgInS	napájet
šesti	šest	k4xCc7	šest
solárními	solární	k2eAgInPc7d1	solární
panely	panel	k1gInPc7	panel
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
dohromady	dohromady	k6eAd1	dohromady
generují	generovat	k5eAaImIp3nP	generovat
10	[number]	k4	10
kW	kW	kA	kW
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládaná	předpokládaný	k2eAgFnSc1d1	předpokládaná
životnost	životnost	k1gFnSc1	životnost
je	být	k5eAaImIp3nS	být
15	[number]	k4	15
let	léto	k1gNnPc2	léto
a	a	k8xC	a
palivo	palivo	k1gNnSc1	palivo
by	by	kYmCp3nP	by
mělo	mít	k5eAaImAgNnS	mít
stačit	stačit	k5eAaBmF	stačit
na	na	k7c4	na
20	[number]	k4	20
let	léto	k1gNnPc2	léto
provozu	provoz	k1gInSc2	provoz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tři	tři	k4xCgNnPc1	tři
procenta	procento	k1gNnPc1	procento
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
produkce	produkce	k1gFnSc2	produkce
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
generuje	generovat	k5eAaImIp3nS	generovat
odvětví	odvětví	k1gNnSc1	odvětví
informačních	informační	k2eAgFnPc2d1	informační
a	a	k8xC	a
komunikačních	komunikační	k2eAgFnPc2d1	komunikační
technologií	technologie	k1gFnPc2	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
zaměstnáno	zaměstnat	k5eAaPmNgNnS	zaměstnat
40	[number]	k4	40
000	[number]	k4	000
až	až	k9	až
50	[number]	k4	50
000	[number]	k4	000
softwarových	softwarový	k2eAgMnPc2d1	softwarový
inženýrů	inženýr	k1gMnPc2	inženýr
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
čtvrtinu	čtvrtina	k1gFnSc4	čtvrtina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
tvoří	tvořit	k5eAaImIp3nP	tvořit
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
procento	procento	k1gNnSc1	procento
žen	žena	k1gFnPc2	žena
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
oboru	obor	k1gInSc6	obor
ze	z	k7c2	z
všech	všecek	k3xTgNnPc2	všecek
zemí	zem	k1gFnPc2	zem
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
úroveň	úroveň	k1gFnSc1	úroveň
bulharské	bulharský	k2eAgFnSc2d1	bulharská
informatiky	informatika	k1gFnSc2	informatika
je	být	k5eAaImIp3nS	být
dědictvím	dědictví	k1gNnSc7	dědictví
už	už	k6eAd1	už
komunistické	komunistický	k2eAgFnSc2d1	komunistická
éry	éra	k1gFnSc2	éra
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
nazýváno	nazýván	k2eAgNnSc1d1	nazýváno
"	"	kIx"	"
<g/>
rudé	rudý	k2eAgInPc1d1	rudý
Silicon	Silicon	kA	Silicon
Valley	Valley	k1gInPc1	Valley
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
produkci	produkce	k1gFnSc3	produkce
výpočetní	výpočetní	k2eAgFnSc2d1	výpočetní
techniky	technika	k1gFnSc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
je	být	k5eAaImIp3nS	být
také	také	k9	také
regionálním	regionální	k2eAgMnSc7d1	regionální
lídrem	lídr	k1gMnSc7	lídr
ve	v	k7c6	v
vysoce	vysoce	k6eAd1	vysoce
výkonných	výkonný	k2eAgInPc6d1	výkonný
počítačích	počítač	k1gInPc6	počítač
a	a	k8xC	a
provozuje	provozovat	k5eAaImIp3nS	provozovat
Avitohol	Avitohol	k1gInSc1	Avitohol
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInSc1d3	veliký
superpočítač	superpočítač	k1gInSc1	superpočítač
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sport	sport	k1gInSc1	sport
==	==	k?	==
</s>
</p>
<p>
<s>
Nejpopulárnějším	populární	k2eAgMnSc7d3	nejpopulárnější
sportovcem	sportovec	k1gMnSc7	sportovec
všech	všecek	k3xTgInPc2	všecek
časů	čas	k1gInPc2	čas
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
fotbalista	fotbalista	k1gMnSc1	fotbalista
Christo	Christa	k1gMnSc5	Christa
Stoičkov	Stoičkov	k1gInSc4	Stoičkov
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
získal	získat	k5eAaPmAgInS	získat
Zlatý	zlatý	k2eAgInSc1d1	zlatý
míč	míč	k1gInSc1	míč
pro	pro	k7c4	pro
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
fotbalistu	fotbalista	k1gMnSc4	fotbalista
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
dotáhl	dotáhnout	k5eAaPmAgMnS	dotáhnout
bulharskou	bulharský	k2eAgFnSc4d1	bulharská
reprezentaci	reprezentace	k1gFnSc4	reprezentace
k	k	k7c3	k
jejímu	její	k3xOp3gInSc3	její
největšímu	veliký	k2eAgInSc3d3	veliký
úspěchu	úspěch	k1gInSc3	úspěch
na	na	k7c6	na
světovém	světový	k2eAgInSc6d1	světový
šampionátu	šampionát	k1gInSc6	šampionát
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
a	a	k8xC	a
se	s	k7c7	s
španělským	španělský	k2eAgInSc7d1	španělský
klubem	klub	k1gInSc7	klub
FC	FC	kA	FC
Barcelona	Barcelona	k1gFnSc1	Barcelona
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
Ligu	liga	k1gFnSc4	liga
mistrů	mistr	k1gMnPc2	mistr
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mimořádnou	mimořádný	k2eAgFnSc4d1	mimořádná
tradici	tradice	k1gFnSc4	tradice
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
zápas	zápas	k1gInSc1	zápas
řecko-římský	řecko-římský	k2eAgMnSc1d1	řecko-římský
a	a	k8xC	a
vzpírání	vzpírání	k1gNnSc1	vzpírání
<g/>
.	.	kIx.	.
</s>
<s>
Zápasníci	zápasník	k1gMnPc1	zápasník
přivezli	přivézt	k5eAaPmAgMnP	přivézt
již	již	k6eAd1	již
16	[number]	k4	16
zlatých	zlatý	k2eAgFnPc2d1	zlatá
medailí	medaile	k1gFnPc2	medaile
z	z	k7c2	z
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
vzpěrači	vzpěrač	k1gMnSc3	vzpěrač
12	[number]	k4	12
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
tři	tři	k4xCgInPc1	tři
Naim	Naim	k1gInSc1	Naim
Süleymanoğ	Süleymanoğ	k1gFnSc1	Süleymanoğ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
díky	díky	k7c3	díky
inovativním	inovativní	k2eAgFnPc3d1	inovativní
tréninkovým	tréninkový	k2eAgFnPc3d1	tréninková
metodám	metoda	k1gFnPc3	metoda
Ivana	Ivan	k1gMnSc2	Ivan
Abadžieva	Abadžiev	k1gMnSc2	Abadžiev
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
zlatou	zlatá	k1gFnSc4	zlatá
z	z	k7c2	z
jiného	jiný	k2eAgInSc2d1	jiný
sportu	sport	k1gInSc2	sport
<g/>
,	,	kIx,	,
než	než	k8xS	než
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc1	tento
<g/>
,	,	kIx,	,
přivezl	přivézt	k5eAaPmAgMnS	přivézt
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
boxer	boxer	k1gInSc4	boxer
Georgi	Georg	k1gFnSc2	Georg
Kostadinov	Kostadinov	k1gInSc1	Kostadinov
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
atletickou	atletický	k2eAgFnSc4d1	atletická
pak	pak	k6eAd1	pak
z	z	k7c2	z
Montrealu	Montreal	k1gInSc2	Montreal
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
koulařka	koulařka	k1gFnSc1	koulařka
Ivanka	Ivanka	k1gFnSc1	Ivanka
Christovová	Christovová	k1gFnSc1	Christovová
<g/>
.	.	kIx.	.
</s>
<s>
Bulharům	Bulhar	k1gMnPc3	Bulhar
se	se	k3xPyFc4	se
daří	dařit	k5eAaImIp3nS	dařit
v	v	k7c6	v
trojskoku	trojskok	k1gInSc6	trojskok
<g/>
,	,	kIx,	,
olympijské	olympijský	k2eAgNnSc4d1	Olympijské
zlato	zlato	k1gNnSc4	zlato
mají	mít	k5eAaImIp3nP	mít
Christo	Christa	k1gMnSc5	Christa
Markov	Markov	k1gInSc1	Markov
a	a	k8xC	a
Tereza	Tereza	k1gFnSc1	Tereza
Marinovová	Marinovová	k1gFnSc1	Marinovová
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
nejpopulárnější	populární	k2eAgFnSc7d3	nejpopulárnější
bulharskou	bulharský	k2eAgFnSc7d1	bulharská
atletkou	atletka	k1gFnSc7	atletka
byla	být	k5eAaImAgFnS	být
skokanka	skokanka	k1gFnSc1	skokanka
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
Stefka	Stefka	k1gFnSc1	Stefka
Kostadinovová	Kostadinovová	k1gFnSc1	Kostadinovová
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
drží	držet	k5eAaImIp3nS	držet
světový	světový	k2eAgInSc4d1	světový
výškařský	výškařský	k2eAgInSc4d1	výškařský
rekord	rekord	k1gInSc4	rekord
mezi	mezi	k7c7	mezi
ženami	žena	k1gFnPc7	žena
(	(	kIx(	(
<g/>
209	[number]	k4	209
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výjimečnou	výjimečný	k2eAgFnSc4d1	výjimečná
žeň	žeň	k1gFnSc4	žeň
zažili	zažít	k5eAaPmAgMnP	zažít
Bulhaři	Bulhar	k1gMnPc1	Bulhar
na	na	k7c6	na
olympiádě	olympiáda	k1gFnSc6	olympiáda
v	v	k7c6	v
Soulu	Soul	k1gInSc6	Soul
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
přivezli	přivézt	k5eAaPmAgMnP	přivézt
rekordních	rekordní	k2eAgInPc2d1	rekordní
deset	deset	k4xCc4	deset
zlatých	zlatý	k1gInPc2	zlatý
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc4d1	jediná
zlatou	zlatá	k1gFnSc4	zlatá
ze	z	k7c2	z
zimních	zimní	k2eAgFnPc2d1	zimní
olympiád	olympiáda	k1gFnPc2	olympiáda
Bulhaři	Bulhar	k1gMnPc1	Bulhar
získali	získat	k5eAaPmAgMnP	získat
díky	díky	k7c3	díky
biatlonistce	biatlonistka	k1gFnSc3	biatlonistka
Ekaterině	Ekaterina	k1gFnSc3	Ekaterina
Dafovské	Dafovská	k1gFnSc3	Dafovská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
volejbalové	volejbalový	k2eAgFnSc2d1	volejbalová
federace	federace	k1gFnSc2	federace
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
Dimitar	Dimitar	k1gInSc1	Dimitar
Zlatanov	Zlatanov	k1gInSc1	Zlatanov
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
64	[number]	k4	64
polích	pole	k1gNnPc6	pole
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
prosadil	prosadit	k5eAaPmAgMnS	prosadit
šachový	šachový	k2eAgMnSc1d1	šachový
velmistr	velmistr	k1gMnSc1	velmistr
Veselin	Veselin	k2eAgInSc4d1	Veselin
Topalov	Topalov	k1gInSc4	Topalov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
stopách	stopa	k1gFnPc6	stopa
jde	jít	k5eAaImIp3nS	jít
Antoaneta	Antoanet	k2eAgFnSc1d1	Antoanet
Stefanovová	Stefanovová	k1gFnSc1	Stefanovová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úřední	úřední	k2eAgInSc1d1	úřední
název	název	k1gInSc1	název
státu	stát	k1gInSc2	stát
==	==	k?	==
</s>
</p>
<p>
<s>
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
1990	[number]	k4	1990
–	–	k?	–
Bulharská	bulharský	k2eAgFnSc1d1	bulharská
republika	republika	k1gFnSc1	republika
/	/	kIx~	/
Republika	republika	k1gFnSc1	republika
Bulharsko	Bulharsko	k1gNnSc4	Bulharsko
(	(	kIx(	(
<g/>
bulharsky	bulharsky	k6eAd1	bulharsky
Р	Р	k?	Р
Б	Б	k?	Б
[	[	kIx(	[
<g/>
Republika	republika	k1gFnSc1	republika
Bălgarija	Bălgarija	k1gFnSc1	Bălgarija
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Dřívější	dřívější	k2eAgInPc1d1	dřívější
úřední	úřední	k2eAgInPc1d1	úřední
názvy	název	k1gInPc1	název
===	===	k?	===
</s>
</p>
<p>
<s>
1946	[number]	k4	1946
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
Bulharská	bulharský	k2eAgFnSc1d1	bulharská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
Narodna	Narodna	k1gFnSc1	Narodna
republika	republika	k1gFnSc1	republika
Bălgarija	Bălgarija	k1gFnSc1	Bălgarija
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1908	[number]	k4	1908
<g/>
–	–	k?	–
<g/>
1946	[number]	k4	1946
Bulharské	bulharský	k2eAgNnSc1d1	bulharské
carství	carství	k1gNnSc1	carství
(	(	kIx(	(
<g/>
Carstvo	carstvo	k1gNnSc1	carstvo
Bălgarija	Bălgarij	k1gInSc2	Bălgarij
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1878	[number]	k4	1878
<g/>
–	–	k?	–
<g/>
1908	[number]	k4	1908
Bulharské	bulharský	k2eAgNnSc1d1	bulharské
knížectví	knížectví	k1gNnSc1	knížectví
(	(	kIx(	(
<g/>
Knjažestvo	Knjažestvo	k1gNnSc1	Knjažestvo
Bălgarija	Bălgarij	k1gInSc2	Bălgarij
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
MARTÍNEK	Martínek	k1gMnSc1	Martínek
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
136	[number]	k4	136
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
422	[number]	k4	422
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
RYCHLÍK	Rychlík	k1gMnSc1	Rychlík
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
508	[number]	k4	508
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
404	[number]	k4	404
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
bulharských	bulharský	k2eAgMnPc2d1	bulharský
panovníkůSeznam	panovníkůSeznam	k1gInSc1	panovníkůSeznam
prezidentů	prezident	k1gMnPc2	prezident
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
měst	město	k1gNnPc2	město
v	v	k7c6	v
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
řek	řeka	k1gFnPc2	řeka
v	v	k7c6	v
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
</s>
</p>
<p>
<s>
Velcí	velký	k2eAgMnPc1d1	velký
BulhařiBulharská	BulhařiBulharský	k2eAgNnPc4d1	BulhařiBulharský
armáda	armáda	k1gFnSc1	armáda
</s>
</p>
<p>
<s>
Bulharská	bulharský	k2eAgFnSc1d1	bulharská
ulice	ulice	k1gFnSc1	ulice
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bulharsko	Bulharsko	k1gNnSc4	Bulharsko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Bulharsko	Bulharsko	k1gNnSc4	Bulharsko
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnSc1	kategorie
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
<p>
<s>
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
na	na	k7c4	na
OpenStreetMap	OpenStreetMap	k1gInSc4	OpenStreetMap
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
bulharsky	bulharsky	k6eAd1	bulharsky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
bulharské	bulharský	k2eAgFnSc2d1	bulharská
vlády	vláda	k1gFnSc2	vláda
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Cestování	cestování	k1gNnSc1	cestování
|	|	kIx~	|
MZV	MZV	kA	MZV
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Velvyslanectví	velvyslanectví	k1gNnSc1	velvyslanectví
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
Sofii	Sofia	k1gFnSc6	Sofia
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Kontaktní	kontaktní	k2eAgInSc1d1	kontaktní
Bulharský	bulharský	k2eAgInSc1d1	bulharský
úřad	úřad	k1gInSc1	úřad
v	v	k7c6	v
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
Bulgaria	Bulgarium	k1gNnPc1	Bulgarium
-	-	kIx~	-
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc2	International
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bulgaria	Bulgarium	k1gNnPc1	Bulgarium
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc1	house
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
.	.	kIx.	.
</s>
<s>
BTI	BTI	kA	BTI
2010	[number]	k4	2010
–	–	k?	–
Bulgaria	Bulgarium	k1gNnSc2	Bulgarium
Country	country	k2eAgInSc4d1	country
Report	report	k1gInSc4	report
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Gütersloh	Gütersloh	k1gMnSc1	Gütersloh
<g/>
:	:	kIx,	:
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
European	European	k1gInSc1	European
and	and	k?	and
Eurasian	Eurasian	k1gInSc1	Eurasian
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Not	k1gInSc2	Not
<g/>
:	:	kIx,	:
Bulgaria	Bulgarium	k1gNnSc2	Bulgarium
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011-05-03	[number]	k4	2011-05-03
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Bulgaria	Bulgarium	k1gNnSc2	Bulgarium
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2011-08-16	[number]	k4	2011-08-16
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Library	Librar	k1gInPc1	Librar
of	of	k?	of
Congress	Congress	k1gInSc1	Congress
<g/>
.	.	kIx.	.
</s>
<s>
Country	country	k2eAgFnPc1d1	country
Profile	profil	k1gInSc5	profil
<g/>
:	:	kIx,	:
Bulgaria	Bulgarium	k1gNnSc2	Bulgarium
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2006-10-20	[number]	k4	2006-10-20
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Sofii	Sofia	k1gFnSc6	Sofia
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2011-03-31	[number]	k4	2011-03-31
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2013	[number]	k4	2013
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
BELL	bell	k1gInSc1	bell
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
D	D	kA	D
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Bulgaria	Bulgarium	k1gNnPc1	Bulgarium
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
