<p>
<s>
Přelet	přelet	k1gInSc1	přelet
nad	nad	k7c7	nad
kukaččím	kukaččí	k2eAgNnSc7d1	kukaččí
hnízdem	hnízdo	k1gNnSc7	hnízdo
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
One	One	k1gMnSc1	One
Flew	Flew	k1gMnSc1	Flew
Over	Over	k1gMnSc1	Over
the	the	k?	the
Cuckoo	Cuckoo	k1gMnSc1	Cuckoo
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Nest	Nesta	k1gFnPc2	Nesta
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc1d1	americký
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
režiséra	režisér	k1gMnSc2	režisér
Miloše	Miloš	k1gMnSc2	Miloš
Formana	Forman	k1gMnSc2	Forman
natočený	natočený	k2eAgInSc4d1	natočený
podle	podle	k7c2	podle
románové	románový	k2eAgFnSc2d1	románová
předlohy	předloha	k1gFnSc2	předloha
spisovatele	spisovatel	k1gMnSc2	spisovatel
Kena	Kenus	k1gMnSc2	Kenus
Keseyho	Kesey	k1gMnSc2	Kesey
Vyhoďme	vyhodit	k5eAaPmRp1nP	vyhodit
ho	on	k3xPp3gInSc4	on
z	z	k7c2	z
kola	kolo	k1gNnSc2	kolo
ven	ven	k6eAd1	ven
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
s	s	k7c7	s
Jackem	Jacek	k1gMnSc7	Jacek
Nicholsonem	Nicholson	k1gMnSc7	Nicholson
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
měl	mít	k5eAaImAgInS	mít
roli	role	k1gFnSc4	role
McMurphyho	McMurphy	k1gMnSc2	McMurphy
ztvárnit	ztvárnit	k5eAaPmF	ztvárnit
James	James	k1gMnSc1	James
Caan	Caan	k1gMnSc1	Caan
<g/>
,	,	kIx,	,
Marlon	Marlon	k1gInSc1	Marlon
Brando	Brando	k6eAd1	Brando
nebo	nebo	k8xC	nebo
Gene	gen	k1gInSc5	gen
Hackman	Hackman	k1gMnSc1	Hackman
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Film	film	k1gInSc1	film
se	se	k3xPyFc4	se
natáčel	natáčet	k5eAaImAgInS	natáčet
v	v	k7c6	v
psychiatrickém	psychiatrický	k2eAgNnSc6d1	psychiatrické
zařízení	zařízení	k1gNnSc6	zařízení
Oregon	Oregona	k1gFnPc2	Oregona
State	status	k1gInSc5	status
Hospital	Hospital	k1gMnSc1	Hospital
v	v	k7c6	v
Salemu	Salem	k1gInSc6	Salem
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Oregon	Oregon	k1gNnSc1	Oregon
<g/>
,	,	kIx,	,
stejné	stejný	k2eAgNnSc1d1	stejné
místo	místo	k1gNnSc1	místo
je	být	k5eAaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
i	i	k9	i
v	v	k7c6	v
románu	román	k1gInSc6	román
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
převzat	převzít	k5eAaPmNgInS	převzít
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
americké	americký	k2eAgFnSc2d1	americká
říkanky	říkanka	k1gFnSc2	říkanka
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
románové	románový	k2eAgFnSc6d1	románová
předloze	předloha	k1gFnSc6	předloha
(	(	kIx(	(
<g/>
ve	v	k7c6	v
filmu	film	k1gInSc6	film
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
obsaženo	obsažen	k2eAgNnSc1d1	obsaženo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
říkanku	říkanka	k1gFnSc4	říkanka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
si	se	k3xPyFc3	se
indiánský	indiánský	k2eAgMnSc1d1	indiánský
"	"	kIx"	"
<g/>
náčelník	náčelník	k1gMnSc1	náčelník
<g/>
"	"	kIx"	"
Bromden	Bromdna	k1gFnPc2	Bromdna
zapamatoval	zapamatovat	k5eAaPmAgMnS	zapamatovat
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vintery	Vinter	k1gInPc1	Vinter
<g/>
,	,	kIx,	,
mintery	minter	k1gInPc1	minter
<g/>
,	,	kIx,	,
cutery	cuter	k1gInPc1	cuter
<g/>
,	,	kIx,	,
corn	corn	k1gNnSc1	corn
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Apple	Apple	kA	Apple
seed	seed	k1gMnSc1	seed
and	and	k?	and
apple	apple	k1gInSc1	apple
thorn	thorn	k1gInSc1	thorn
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Wire	Wire	k1gFnSc1	Wire
<g/>
,	,	kIx,	,
briar	briar	k1gMnSc1	briar
<g/>
,	,	kIx,	,
limber	limber	k1gMnSc1	limber
lock	lock	k1gMnSc1	lock
</s>
</p>
<p>
<s>
Three	Three	k1gFnSc1	Three
geese	geese	k1gFnSc2	geese
in	in	k?	in
a	a	k8xC	a
flock	flock	k6eAd1	flock
</s>
</p>
<p>
<s>
One	One	k?	One
flew	flew	k?	flew
East	East	k1gInSc1	East
</s>
</p>
<p>
<s>
One	One	k?	One
flew	flew	k?	flew
West	West	k1gInSc1	West
</s>
</p>
<p>
<s>
And	Anda	k1gFnPc2	Anda
one	one	k?	one
flew	flew	k?	flew
over	over	k1gMnSc1	over
the	the	k?	the
cuckoo	cuckoo	k1gMnSc1	cuckoo
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
nest	nesta	k1gFnPc2	nesta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Děj	děj	k1gInSc1	děj
filmu	film	k1gInSc2	film
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
psychiatrické	psychiatrický	k2eAgFnSc6d1	psychiatrická
léčebně	léčebna	k1gFnSc6	léčebna
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yRgFnSc2	který
přichází	přicházet	k5eAaImIp3nS	přicházet
profesionální	profesionální	k2eAgMnPc1d1	profesionální
karetní	karetní	k2eAgMnSc1d1	karetní
hráč	hráč	k1gMnSc1	hráč
Randall	Randalla	k1gFnPc2	Randalla
Patrick	Patrick	k1gMnSc1	Patrick
McMurphy	McMurpha	k1gFnSc2	McMurpha
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
soudem	soud	k1gInSc7	soud
nařízenou	nařízený	k2eAgFnSc4d1	nařízená
léčbu	léčba	k1gFnSc4	léčba
<g/>
.	.	kIx.	.
</s>
<s>
Kriminálník	kriminálník	k1gMnSc1	kriminálník
McMurphy	McMurpha	k1gFnSc2	McMurpha
hraje	hrát	k5eAaImIp3nS	hrát
pomateného	pomatený	k2eAgMnSc4d1	pomatený
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vyhnul	vyhnout	k5eAaPmAgMnS	vyhnout
vězeňskému	vězeňský	k2eAgInSc3d1	vězeňský
trestu	trest	k1gInSc3	trest
za	za	k7c4	za
sexuální	sexuální	k2eAgNnSc4d1	sexuální
zneužití	zneužití	k1gNnSc4	zneužití
a	a	k8xC	a
násilné	násilný	k2eAgNnSc4d1	násilné
přepadení	přepadení	k1gNnSc4	přepadení
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
představa	představa	k1gFnSc1	představa
pokojného	pokojný	k2eAgInSc2d1	pokojný
života	život	k1gInSc2	život
v	v	k7c6	v
ústavu	ústav	k1gInSc6	ústav
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
konfrontovaná	konfrontovaný	k2eAgFnSc1d1	konfrontovaná
s	s	k7c7	s
praktikami	praktika	k1gFnPc7	praktika
despotické	despotický	k2eAgFnSc2d1	despotická
vrchní	vrchní	k2eAgFnSc2d1	vrchní
sestry	sestra	k1gFnSc2	sestra
Mildred	Mildred	k1gMnSc1	Mildred
Ratchedové	Ratchedový	k2eAgFnSc2d1	Ratchedová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
součinnosti	součinnost	k1gFnSc6	součinnost
s	s	k7c7	s
vedoucím	vedoucí	k1gMnSc7	vedoucí
lékařem	lékař	k1gMnSc7	lékař
svému	svůj	k3xOyFgNnSc3	svůj
oddělení	oddělení	k1gNnSc3	oddělení
vládne	vládnout	k5eAaImIp3nS	vládnout
pevnou	pevný	k2eAgFnSc7d1	pevná
rukou	ruka	k1gFnSc7	ruka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Randall	Randall	k1gInSc1	Randall
chce	chtít	k5eAaImIp3nS	chtít
apatické	apatický	k2eAgMnPc4d1	apatický
muže	muž	k1gMnPc4	muž
probudit	probudit	k5eAaPmF	probudit
k	k	k7c3	k
životu	život	k1gInSc3	život
a	a	k8xC	a
vlít	vlít	k5eAaPmF	vlít
jim	on	k3xPp3gMnPc3	on
do	do	k7c2	do
žil	žíla	k1gFnPc2	žíla
trochu	trochu	k6eAd1	trochu
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
dokáže	dokázat	k5eAaPmIp3nS	dokázat
naplno	naplno	k6eAd1	naplno
užívat	užívat	k5eAaImF	užívat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
mezi	mezi	k7c4	mezi
pacienty	pacient	k1gMnPc4	pacient
zorganizuje	zorganizovat	k5eAaPmIp3nS	zorganizovat
hlasování	hlasování	k1gNnSc1	hlasování
pro	pro	k7c4	pro
sledování	sledování	k1gNnSc4	sledování
televizního	televizní	k2eAgInSc2d1	televizní
přenosu	přenos	k1gInSc2	přenos
baseballového	baseballový	k2eAgInSc2d1	baseballový
zápasu	zápas	k1gInSc2	zápas
<g/>
,	,	kIx,	,
vrchní	vrchní	k2eAgFnSc1d1	vrchní
sestra	sestra	k1gFnSc1	sestra
si	se	k3xPyFc3	se
vymyslí	vymyslet	k5eAaPmIp3nS	vymyslet
výmluvu	výmluva	k1gFnSc4	výmluva
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
tomu	ten	k3xDgNnSc3	ten
zabránit	zabránit	k5eAaPmF	zabránit
<g/>
.	.	kIx.	.
</s>
<s>
McMurphy	McMurpha	k1gFnSc2	McMurpha
se	se	k3xPyFc4	se
nevzdává	vzdávat	k5eNaImIp3nS	vzdávat
<g/>
,	,	kIx,	,
sedne	sednout	k5eAaPmIp3nS	sednout
si	se	k3xPyFc3	se
před	před	k7c4	před
televizi	televize	k1gFnSc4	televize
a	a	k8xC	a
nahlas	nahlas	k6eAd1	nahlas
komentuje	komentovat	k5eAaBmIp3nS	komentovat
fiktivní	fiktivní	k2eAgInSc4d1	fiktivní
zápas	zápas	k1gInSc4	zápas
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
vysílal	vysílat	k5eAaImAgInS	vysílat
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k6eAd1	jednou
se	se	k3xPyFc4	se
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
hluchoněmého	hluchoněmý	k2eAgMnSc2d1	hluchoněmý
indiánského	indiánský	k2eAgMnSc2d1	indiánský
obra	obr	k1gMnSc2	obr
Bromdena	Bromden	k1gMnSc2	Bromden
(	(	kIx(	(
<g/>
kterému	který	k3yRgInSc3	který
zde	zde	k6eAd1	zde
obvykle	obvykle	k6eAd1	obvykle
říkají	říkat	k5eAaImIp3nP	říkat
náčelník	náčelník	k1gInSc4	náčelník
<g/>
)	)	kIx)	)
dostane	dostat	k5eAaPmIp3nS	dostat
přes	přes	k7c4	přes
plot	plot	k1gInSc4	plot
<g/>
,	,	kIx,	,
sedne	sednout	k5eAaPmIp3nS	sednout
za	za	k7c4	za
volant	volant	k1gInSc4	volant
autobusu	autobus	k1gInSc2	autobus
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnPc3	který
pacienti	pacient	k1gMnPc1	pacient
jezdí	jezdit	k5eAaImIp3nP	jezdit
na	na	k7c4	na
výlety	výlet	k1gInPc4	výlet
<g/>
,	,	kIx,	,
a	a	k8xC	a
vezme	vzít	k5eAaPmIp3nS	vzít
kamarády	kamarád	k1gMnPc4	kamarád
na	na	k7c4	na
loď	loď	k1gFnSc4	loď
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
zarybařili	zarybařit	k5eAaPmAgMnP	zarybařit
<g/>
.	.	kIx.	.
</s>
<s>
Zorganizuje	zorganizovat	k5eAaPmIp3nS	zorganizovat
i	i	k9	i
basketbalový	basketbalový	k2eAgInSc4d1	basketbalový
zápas	zápas	k1gInSc4	zápas
proti	proti	k7c3	proti
dozorcům	dozorce	k1gMnPc3	dozorce
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podaří	podařit	k5eAaPmIp3nS	podařit
zapojit	zapojit	k5eAaPmF	zapojit
i	i	k9	i
náčelníka	náčelník	k1gMnSc2	náčelník
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
šokován	šokovat	k5eAaBmNgMnS	šokovat
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
během	během	k7c2	během
jednoho	jeden	k4xCgNnSc2	jeden
sezení	sezení	k1gNnSc2	sezení
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
pacientů	pacient	k1gMnPc2	pacient
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
dobrovolně	dobrovolně	k6eAd1	dobrovolně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
jednou	jednou	k6eAd1	jednou
napadne	napadnout	k5eAaPmIp3nS	napadnout
dozorce	dozorce	k1gMnSc4	dozorce
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bránil	bránit	k5eAaImAgMnS	bránit
svého	svůj	k3xOyFgMnSc4	svůj
kamaráda	kamarád	k1gMnSc4	kamarád
<g/>
,	,	kIx,	,
přidá	přidat	k5eAaPmIp3nS	přidat
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
i	i	k8xC	i
náčelník	náčelník	k1gMnSc1	náčelník
<g/>
.	.	kIx.	.
</s>
<s>
Randall	Randall	k1gMnSc1	Randall
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
náčelník	náčelník	k1gMnSc1	náčelník
hluchoněmost	hluchoněmost	k1gFnSc4	hluchoněmost
jen	jen	k9	jen
předstírá	předstírat	k5eAaImIp3nS	předstírat
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
si	se	k3xPyFc3	se
domluví	domluvit	k5eAaPmIp3nP	domluvit
útěk	útěk	k1gInSc4	útěk
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
napadení	napadení	k1gNnSc2	napadení
dostanou	dostat	k5eAaPmIp3nP	dostat
oba	dva	k4xCgInPc1	dva
elektrošoky	elektrošok	k1gInPc1	elektrošok
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
i	i	k9	i
pacient	pacient	k1gMnSc1	pacient
Cheswick	Cheswick	k1gMnSc1	Cheswick
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
bránili	bránit	k5eAaImAgMnP	bránit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Později	pozdě	k6eAd2	pozdě
McMurphy	McMurpha	k1gFnPc4	McMurpha
uplatí	uplatit	k5eAaPmIp3nP	uplatit
starého	starý	k2eAgMnSc4d1	starý
dozorce	dozorce	k1gMnSc4	dozorce
Turkla	Turkla	k1gMnSc4	Turkla
<g/>
,	,	kIx,	,
přivede	přivést	k5eAaPmIp3nS	přivést
do	do	k7c2	do
léčebny	léčebna	k1gFnSc2	léčebna
na	na	k7c4	na
noc	noc	k1gFnSc4	noc
dámskou	dámský	k2eAgFnSc4d1	dámská
společnost	společnost	k1gFnSc4	společnost
a	a	k8xC	a
zorganizuje	zorganizovat	k5eAaPmIp3nS	zorganizovat
mejdan	mejdan	k1gInSc4	mejdan
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
ránem	ráno	k1gNnSc7	ráno
plánuje	plánovat	k5eAaImIp3nS	plánovat
útěk	útěk	k1gInSc4	útěk
<g/>
.	.	kIx.	.
</s>
<s>
Alkohol	alkohol	k1gInSc1	alkohol
však	však	k9	však
vykonal	vykonat	k5eAaPmAgInS	vykonat
své	své	k1gNnSc4	své
a	a	k8xC	a
dozorci	dozorce	k1gMnPc1	dozorce
se	s	k7c7	s
sestrou	sestra	k1gFnSc7	sestra
Ratchedovou	Ratchedův	k2eAgFnSc7d1	Ratchedův
najdou	najít	k5eAaPmIp3nP	najít
ráno	ráno	k6eAd1	ráno
pacienty	pacient	k1gMnPc4	pacient
opilé	opilý	k2eAgNnSc1d1	opilé
<g/>
.	.	kIx.	.
</s>
<s>
Vrchní	vrchní	k2eAgFnSc1d1	vrchní
sestra	sestra	k1gFnSc1	sestra
nechá	nechat	k5eAaPmIp3nS	nechat
pacienty	pacient	k1gMnPc4	pacient
přepočítat	přepočítat	k5eAaPmF	přepočítat
<g/>
.	.	kIx.	.
</s>
<s>
Chybí	chybět	k5eAaImIp3nS	chybět
jen	jen	k9	jen
koktavý	koktavý	k2eAgInSc1d1	koktavý
Billy	Bill	k1gMnPc7	Bill
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
kontrolách	kontrola	k1gFnPc6	kontrola
všech	všecek	k3xTgFnPc2	všecek
místností	místnost	k1gFnPc2	místnost
je	být	k5eAaImIp3nS	být
nalezen	nalézt	k5eAaBmNgInS	nalézt
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
v	v	k7c6	v
objetí	objetí	k1gNnSc6	objetí
s	s	k7c7	s
dívkou	dívka	k1gFnSc7	dívka
Candy	Canda	k1gFnSc2	Canda
<g/>
.	.	kIx.	.
</s>
<s>
Ratchedová	Ratchedový	k2eAgFnSc1d1	Ratchedová
mu	on	k3xPp3gMnSc3	on
vyhrožuje	vyhrožovat	k5eAaImIp3nS	vyhrožovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vše	všechen	k3xTgNnSc1	všechen
řekne	říct	k5eAaPmIp3nS	říct
jeho	jeho	k3xOp3gFnSc3	jeho
matce	matka	k1gFnSc3	matka
<g/>
.	.	kIx.	.
</s>
<s>
Labilní	labilní	k2eAgMnPc4d1	labilní
Billy	Bill	k1gMnPc4	Bill
se	se	k3xPyFc4	se
této	tento	k3xDgFnSc3	tento
možnosti	možnost	k1gFnSc3	možnost
tak	tak	k9	tak
vyděsí	vyděsit	k5eAaPmIp3nS	vyděsit
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
podřízne	podříznout	k5eAaPmIp3nS	podříznout
hrdlo	hrdlo	k1gNnSc4	hrdlo
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
to	ten	k3xDgNnSc4	ten
vidí	vidět	k5eAaImIp3nP	vidět
McMurphy	McMurph	k1gInPc1	McMurph
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
už	už	k6eAd1	už
chystal	chystat	k5eAaImAgMnS	chystat
s	s	k7c7	s
náčelníkem	náčelník	k1gInSc7	náčelník
k	k	k7c3	k
útěku	útěk	k1gInSc3	útěk
<g/>
,	,	kIx,	,
neudrží	udržet	k5eNaPmIp3nS	udržet
se	se	k3xPyFc4	se
a	a	k8xC	a
začne	začít	k5eAaPmIp3nS	začít
Ratchedovou	Ratchedový	k2eAgFnSc4d1	Ratchedová
škrtit	škrtit	k5eAaImF	škrtit
<g/>
.	.	kIx.	.
</s>
<s>
Nepodaří	podařit	k5eNaPmIp3nS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
dozorců	dozorce	k1gMnPc2	dozorce
jej	on	k3xPp3gMnSc4	on
udeří	udeřit	k5eAaPmIp3nP	udeřit
zezadu	zezadu	k6eAd1	zezadu
do	do	k7c2	do
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
McMurphy	McMurph	k1gInPc1	McMurph
na	na	k7c4	na
čas	čas	k1gInSc4	čas
z	z	k7c2	z
oddělení	oddělení	k1gNnSc2	oddělení
zmizí	zmizet	k5eAaPmIp3nP	zmizet
<g/>
.	.	kIx.	.
</s>
<s>
Lékař	lékař	k1gMnSc1	lékař
mu	on	k3xPp3gMnSc3	on
provede	provést	k5eAaPmIp3nS	provést
lobotomii	lobotomie	k1gFnSc4	lobotomie
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jej	on	k3xPp3gMnSc4	on
zbavil	zbavit	k5eAaPmAgMnS	zbavit
temperamentu	temperament	k1gInSc3	temperament
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
jej	on	k3xPp3gMnSc4	on
zřízenci	zřízenec	k1gMnPc1	zřízenec
ústavu	ústav	k1gInSc2	ústav
dovezou	dovézt	k5eAaPmIp3nP	dovézt
zpět	zpět	k6eAd1	zpět
na	na	k7c6	na
oddělení	oddělení	k1gNnSc6	oddělení
<g/>
.	.	kIx.	.
</s>
<s>
Náčelník	náčelník	k1gInSc1	náčelník
má	mít	k5eAaImIp3nS	mít
radost	radost	k1gFnSc4	radost
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
konečně	konečně	k6eAd1	konečně
mohou	moct	k5eAaImIp3nP	moct
spolu	spolu	k6eAd1	spolu
utéci	utéct	k5eAaPmF	utéct
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
muž	muž	k1gMnSc1	muž
bezvládně	bezvládně	k6eAd1	bezvládně
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
posteli	postel	k1gFnSc6	postel
<g/>
;	;	kIx,	;
už	už	k9	už
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
dřívější	dřívější	k2eAgInSc4d1	dřívější
Randall	Randall	k1gInSc4	Randall
McMurphy	McMurpha	k1gFnSc2	McMurpha
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
apatická	apatický	k2eAgFnSc1d1	apatická
přežívající	přežívající	k2eAgFnSc1d1	přežívající
troska	troska	k1gFnSc1	troska
<g/>
,	,	kIx,	,
oběť	oběť	k1gFnSc1	oběť
zvůle	zvůle	k1gFnSc2	zvůle
osoby	osoba	k1gFnSc2	osoba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
si	se	k3xPyFc3	se
vybudovala	vybudovat	k5eAaPmAgFnS	vybudovat
moc	moc	k6eAd1	moc
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
o	o	k7c6	o
životech	život	k1gInPc6	život
druhých	druhý	k4xOgMnPc2	druhý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Náčelník	náčelník	k1gMnSc1	náčelník
nechce	chtít	k5eNaImIp3nS	chtít
dopustit	dopustit	k5eAaPmF	dopustit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
McMurphy	McMurpha	k1gFnPc4	McMurpha
sestře	sestra	k1gFnSc3	sestra
sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k8xS	jako
důkaz	důkaz	k1gInSc4	důkaz
jejího	její	k3xOp3gNnSc2	její
vítězství	vítězství	k1gNnSc2	vítězství
a	a	k8xC	a
pokoření	pokoření	k1gNnSc1	pokoření
neposlušného	poslušný	k2eNgMnSc2d1	neposlušný
pacienta	pacient	k1gMnSc2	pacient
a	a	k8xC	a
raději	rád	k6eAd2	rád
udusí	udusit	k5eAaPmIp3nS	udusit
přítele	přítel	k1gMnSc4	přítel
polštářem	polštář	k1gInSc7	polštář
<g/>
.	.	kIx.	.
</s>
<s>
Vzpomene	vzpomenout	k5eAaPmIp3nS	vzpomenout
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
Randall	Randall	k1gInSc4	Randall
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
návod	návod	k1gInSc4	návod
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
z	z	k7c2	z
léčebny	léčebna	k1gFnSc2	léčebna
utéct	utéct	k5eAaPmF	utéct
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
koupelně	koupelna	k1gFnSc6	koupelna
zvedne	zvednout	k5eAaPmIp3nS	zvednout
těžké	těžký	k2eAgNnSc1d1	těžké
těleso	těleso	k1gNnSc1	těleso
<g/>
,	,	kIx,	,
prohodí	prohodit	k5eAaPmIp3nS	prohodit
je	být	k5eAaImIp3nS	být
zadrátovaným	zadrátovaný	k2eAgNnSc7d1	zadrátované
oknem	okno	k1gNnSc7	okno
a	a	k8xC	a
octne	octnout	k5eAaPmIp3nS	octnout
se	se	k3xPyFc4	se
konečně	konečně	k6eAd1	konečně
na	na	k7c6	na
svobodě	svoboda	k1gFnSc6	svoboda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgMnS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
v	v	k7c6	v
devíti	devět	k4xCc6	devět
kategoriích	kategorie	k1gFnPc6	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgMnS	získat
pět	pět	k4xCc4	pět
ocenění	ocenění	k1gNnPc2	ocenění
<g/>
:	:	kIx,	:
Oscar	Oscar	k1gInSc1	Oscar
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
Oscar	Oscar	k1gInSc4	Oscar
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
režii	režie	k1gFnSc4	režie
(	(	kIx(	(
<g/>
Miloš	Miloš	k1gMnSc1	Miloš
Forman	Forman	k1gMnSc1	Forman
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Oscar	Oscar	k1gInSc1	Oscar
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
(	(	kIx(	(
<g/>
Jack	Jack	k1gMnSc1	Jack
Nicholson	Nicholson	k1gMnSc1	Nicholson
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Oscar	Oscar	k1gInSc1	Oscar
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
ženský	ženský	k2eAgInSc4d1	ženský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
(	(	kIx(	(
<g/>
Louise	Louis	k1gMnSc2	Louis
Fletcher	Fletchra	k1gFnPc2	Fletchra
<g/>
)	)	kIx)	)
a	a	k8xC	a
Oscar	Oscar	k1gInSc1	Oscar
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
adaptovaný	adaptovaný	k2eAgInSc4d1	adaptovaný
scénář	scénář	k1gInSc4	scénář
(	(	kIx(	(
<g/>
Laurence	Laurence	k1gFnSc1	Laurence
Hauben	Haubna	k1gFnPc2	Haubna
a	a	k8xC	a
Bo	Bo	k?	Bo
Goldman	Goldman	k1gMnSc1	Goldman
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Odborníci	odborník	k1gMnPc1	odborník
filmu	film	k1gInSc2	film
vyčítali	vyčítat	k5eAaImAgMnP	vyčítat
zkreslený	zkreslený	k2eAgInSc4d1	zkreslený
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
psychiatrické	psychiatrický	k2eAgFnPc4d1	psychiatrická
léčebny	léčebna	k1gFnPc4	léčebna
a	a	k8xC	a
léčbu	léčba	k1gFnSc4	léčba
duševních	duševní	k2eAgFnPc2d1	duševní
poruch	porucha	k1gFnPc2	porucha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Získat	získat	k5eAaPmF	získat
všech	všecek	k3xTgInPc2	všecek
pět	pět	k4xCc4	pět
hlavních	hlavní	k2eAgInPc2d1	hlavní
filmových	filmový	k2eAgInPc2d1	filmový
Oscarů	Oscar	k1gInPc2	Oscar
se	se	k3xPyFc4	se
nepodaří	podařit	k5eNaPmIp3nS	podařit
často	často	k6eAd1	často
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
Caprův	Caprův	k2eAgInSc1d1	Caprův
film	film	k1gInSc1	film
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
jedné	jeden	k4xCgFnSc6	jeden
noci	noc	k1gFnSc6	noc
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dostal	dostat	k5eAaPmAgMnS	dostat
ocenění	ocenění	k1gNnSc4	ocenění
za	za	k7c4	za
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
režii	režie	k1gFnSc4	režie
<g/>
,	,	kIx,	,
scénář	scénář	k1gInSc4	scénář
a	a	k8xC	a
mužský	mužský	k2eAgInSc4d1	mužský
a	a	k8xC	a
ženský	ženský	k2eAgInSc4d1	ženský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
právě	právě	k9	právě
Přelet	přelet	k1gInSc1	přelet
přes	přes	k7c4	přes
kukaččí	kukaččí	k2eAgNnSc4d1	kukaččí
hnízdo	hnízdo	k1gNnSc4	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
uběhlo	uběhnout	k5eAaPmAgNnS	uběhnout
16	[number]	k4	16
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
podařilo	podařit	k5eAaPmAgNnS	podařit
i	i	k9	i
snímku	snímka	k1gFnSc4	snímka
Mlčení	mlčení	k1gNnPc2	mlčení
jehňátek	jehňátko	k1gNnPc2	jehňátko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Text	text	k1gInSc1	text
písně	píseň	k1gFnSc2	píseň
Welcome	Welcom	k1gInSc5	Welcom
Home	Home	k1gNnSc1	Home
(	(	kIx(	(
<g/>
Sanitarium	Sanitarium	k1gNnSc1	Sanitarium
<g/>
)	)	kIx)	)
od	od	k7c2	od
thrashmetalové	thrashmetalový	k2eAgFnSc2d1	thrashmetalová
kapely	kapela	k1gFnSc2	kapela
Metallica	Metallica	k1gFnSc1	Metallica
byl	být	k5eAaImAgInS	být
inspirován	inspirován	k2eAgInSc1d1	inspirován
filmem	film	k1gInSc7	film
Přelet	přelet	k1gInSc4	přelet
nad	nad	k7c7	nad
kukaččím	kukaččí	k2eAgNnSc7d1	kukaččí
hnízdem	hnízdo	k1gNnSc7	hnízdo
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
One	One	k1gMnSc1	One
Flew	Flew	k1gMnSc1	Flew
Over	Over	k1gMnSc1	Over
the	the	k?	the
Cuckoo	Cuckoo	k1gMnSc1	Cuckoo
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Nest	Nesta	k1gFnPc2	Nesta
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Přelet	přelet	k1gInSc1	přelet
nad	nad	k7c7	nad
kukaččím	kukaččí	k2eAgNnSc7d1	kukaččí
hnízdem	hnízdo	k1gNnSc7	hnízdo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Přelet	přelet	k1gInSc1	přelet
nad	nad	k7c7	nad
kukaččím	kukaččí	k2eAgNnSc7d1	kukaččí
hnízdem	hnízdo	k1gNnSc7	hnízdo
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
One	One	k?	One
Flew	Flew	k1gMnSc1	Flew
Over	Over	k1gMnSc1	Over
the	the	k?	the
Cuckoo	Cuckoo	k1gMnSc1	Cuckoo
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Nest	Nest	k1gInSc1	Nest
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
