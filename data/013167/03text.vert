<p>
<s>
Alkalické	alkalický	k2eAgInPc1d1	alkalický
kovy	kov	k1gInPc1	kov
jsou	být	k5eAaImIp3nP	být
členy	člen	k1gInPc7	člen
1	[number]	k4	1
<g/>
.	.	kIx.	.
skupiny	skupina	k1gFnSc2	skupina
periodické	periodický	k2eAgFnSc2d1	periodická
tabulky	tabulka	k1gFnSc2	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
alkalické	alkalický	k2eAgInPc4d1	alkalický
kovy	kov	k1gInPc4	kov
patří	patřit	k5eAaImIp3nS	patřit
lithium	lithium	k1gNnSc1	lithium
<g/>
,	,	kIx,	,
sodík	sodík	k1gInSc1	sodík
<g/>
,	,	kIx,	,
draslík	draslík	k1gInSc1	draslík
<g/>
,	,	kIx,	,
rubidium	rubidium	k1gNnSc1	rubidium
<g/>
,	,	kIx,	,
cesium	cesium	k1gNnSc1	cesium
a	a	k8xC	a
francium	francium	k1gNnSc1	francium
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
prvky	prvek	k1gInPc1	prvek
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
reaktivní	reaktivní	k2eAgFnPc1d1	reaktivní
a	a	k8xC	a
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
skladovány	skladovat	k5eAaImNgInP	skladovat
pod	pod	k7c7	pod
vrstvou	vrstva	k1gFnSc7	vrstva
nereaktivní	reaktivní	k2eNgFnSc2d1	nereaktivní
<g/>
,	,	kIx,	,
bezvodé	bezvodý	k2eAgFnSc2d1	bezvodá
kapaliny	kapalina	k1gFnSc2	kapalina
(	(	kIx(	(
<g/>
např.	např.	kA	např.
petroleje	petrolej	k1gInPc1	petrolej
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Alkalické	alkalický	k2eAgInPc1d1	alkalický
kovy	kov	k1gInPc1	kov
jsou	být	k5eAaImIp3nP	být
měkké	měkký	k2eAgInPc1d1	měkký
<g/>
,	,	kIx,	,
lehké	lehký	k2eAgInPc1d1	lehký
a	a	k8xC	a
stříbrolesklé	stříbrolesklý	k2eAgInPc1d1	stříbrolesklý
kovy	kov	k1gInPc1	kov
(	(	kIx(	(
<g/>
cesium	cesium	k1gNnSc1	cesium
je	být	k5eAaImIp3nS	být
nazlátlé	nazlátlý	k2eAgNnSc1d1	nazlátlé
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
lze	lze	k6eAd1	lze
krájet	krájet	k5eAaImF	krájet
nožem	nůž	k1gInSc7	nůž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Mohsově	Mohsův	k2eAgFnSc6d1	Mohsova
stupnici	stupnice	k1gFnSc6	stupnice
tvrdosti	tvrdost	k1gFnSc2	tvrdost
mají	mít	k5eAaImIp3nP	mít
hodnoty	hodnota	k1gFnPc1	hodnota
menší	malý	k2eAgFnPc1d2	menší
než	než	k8xS	než
1	[number]	k4	1
(	(	kIx(	(
<g/>
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
měkčí	měkký	k2eAgInSc1d2	měkčí
než	než	k8xS	než
mastek	mastek	k1gInSc1	mastek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejtvrdší	tvrdý	k2eAgMnPc1d3	nejtvrdší
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
alkalických	alkalický	k2eAgInPc2d1	alkalický
kovů	kov	k1gInPc2	kov
je	být	k5eAaImIp3nS	být
lithium	lithium	k1gNnSc1	lithium
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
dobře	dobře	k6eAd1	dobře
vedou	vést	k5eAaImIp3nP	vést
elektrický	elektrický	k2eAgInSc4d1	elektrický
proud	proud	k1gInSc4	proud
i	i	k8xC	i
teplo	teplo	k1gNnSc4	teplo
<g/>
,	,	kIx,	,
lithium	lithium	k1gNnSc1	lithium
<g/>
,	,	kIx,	,
sodík	sodík	k1gInSc1	sodík
a	a	k8xC	a
draslík	draslík	k1gInSc1	draslík
jsou	být	k5eAaImIp3nP	být
lehčí	lehký	k2eAgFnPc1d2	lehčí
než	než	k8xS	než
voda	voda	k1gFnSc1	voda
a	a	k8xC	a
plovou	plovat	k5eAaImIp3nP	plovat
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rubidium	rubidium	k1gNnSc1	rubidium
<g/>
,	,	kIx,	,
cesium	cesium	k1gNnSc1	cesium
a	a	k8xC	a
francium	francium	k1gNnSc1	francium
jsou	být	k5eAaImIp3nP	být
těžší	těžký	k2eAgInSc4d2	těžší
a	a	k8xC	a
klesají	klesat	k5eAaImIp3nP	klesat
tedy	tedy	k9	tedy
ke	k	k7c3	k
dnu	dno	k1gNnSc3	dno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parách	para	k1gFnPc6	para
alkalických	alkalický	k2eAgInPc2d1	alkalický
kovů	kov	k1gInPc2	kov
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
jednoatomových	jednoatomový	k2eAgFnPc2d1	jednoatomový
částic	částice	k1gFnPc2	částice
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
s	s	k7c7	s
dvouatomovými	dvouatomový	k2eAgFnPc7d1	dvouatomová
molekulami	molekula	k1gFnPc7	molekula
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kapalném	kapalný	k2eAgInSc6d1	kapalný
amoniaku	amoniak	k1gInSc6	amoniak
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
temně	temně	k6eAd1	temně
modrého	modrý	k2eAgInSc2d1	modrý
roztoku	roztok	k1gInSc2	roztok
<g/>
.	.	kIx.	.
</s>
<s>
Kationty	kation	k1gInPc1	kation
alkalických	alkalický	k2eAgInPc2d1	alkalický
kovů	kov	k1gInPc2	kov
barví	barvit	k5eAaImIp3nS	barvit
plamen	plamen	k1gInSc1	plamen
různými	různý	k2eAgFnPc7d1	různá
barvami	barva	k1gFnPc7	barva
(	(	kIx(	(
<g/>
Li	li	k8xS	li
-	-	kIx~	-
karmínová	karmínový	k2eAgFnSc1d1	karmínová
<g/>
/	/	kIx~	/
<g/>
červená	červený	k2eAgFnSc1d1	červená
<g/>
;	;	kIx,	;
Na	na	k7c4	na
-	-	kIx~	-
světlá	světlý	k2eAgFnSc1d1	světlá
oranžová	oranžový	k2eAgFnSc1d1	oranžová
<g/>
/	/	kIx~	/
<g/>
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
;	;	kIx,	;
K	k	k7c3	k
-	-	kIx~	-
fialová	fialový	k2eAgFnSc1d1	fialová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Elementární	elementární	k2eAgInPc1d1	elementární
kovy	kov	k1gInPc1	kov
lze	lze	k6eAd1	lze
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
uchovávat	uchovávat	k5eAaImF	uchovávat
pod	pod	k7c7	pod
vrstvou	vrstva	k1gFnSc7	vrstva
alifatických	alifatický	k2eAgInPc2d1	alifatický
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
jako	jako	k8xC	jako
petrolej	petrolej	k1gInSc1	petrolej
nebo	nebo	k8xC	nebo
nafta	nafta	k1gFnSc1	nafta
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgInPc7	jenž
nereaguje	reagovat	k5eNaBmIp3nS	reagovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Alkalické	alkalický	k2eAgInPc1d1	alkalický
kovy	kov	k1gInPc1	kov
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
velmi	velmi	k6eAd1	velmi
nízkou	nízký	k2eAgFnSc4d1	nízká
teplotu	teplota	k1gFnSc4	teplota
tání	tání	k1gNnSc2	tání
<g/>
,	,	kIx,	,
u	u	k7c2	u
lithia	lithium	k1gNnSc2	lithium
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
180	[number]	k4	180
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
u	u	k7c2	u
ostatních	ostatní	k2eAgFnPc2d1	ostatní
dokonce	dokonce	k9	dokonce
pod	pod	k7c4	pod
100	[number]	k4	100
°	°	k?	°
<g/>
C	C	kA	C
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Reaktivita	reaktivita	k1gFnSc1	reaktivita
==	==	k?	==
</s>
</p>
<p>
<s>
Alkalické	alkalický	k2eAgInPc1d1	alkalický
kovy	kov	k1gInPc1	kov
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
reaktivní	reaktivní	k2eAgFnPc1d1	reaktivní
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
nejnižší	nízký	k2eAgInSc4d3	nejnižší
ionizační	ionizační	k2eAgInSc4d1	ionizační
potenciál	potenciál	k1gInSc4	potenciál
v	v	k7c6	v
příslušné	příslušný	k2eAgFnSc6d1	příslušná
periodě	perioda	k1gFnSc6	perioda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hodnota	hodnota	k1gFnSc1	hodnota
jejich	jejich	k3xOp3gInSc2	jejich
druhého	druhý	k4xOgInSc2	druhý
ionizačního	ionizační	k2eAgInSc2d1	ionizační
potenciálu	potenciál	k1gInSc2	potenciál
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
valenční	valenční	k2eAgFnSc6d1	valenční
slupce	slupka	k1gFnSc6	slupka
elektronového	elektronový	k2eAgInSc2d1	elektronový
obalu	obal	k1gInSc2	obal
mají	mít	k5eAaImIp3nP	mít
jeden	jeden	k4xCgInSc4	jeden
elektron	elektron	k1gInSc4	elektron
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
snadno	snadno	k6eAd1	snadno
tvoří	tvořit	k5eAaImIp3nP	tvořit
kationty	kation	k1gInPc1	kation
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
kladným	kladný	k2eAgInSc7d1	kladný
nábojem	náboj	k1gInSc7	náboj
<g/>
.	.	kIx.	.
</s>
<s>
Reaktivita	reaktivita	k1gFnSc1	reaktivita
alkalických	alkalický	k2eAgInPc2d1	alkalický
kovů	kov	k1gInPc2	kov
stoupá	stoupat	k5eAaImIp3nS	stoupat
s	s	k7c7	s
protonovým	protonový	k2eAgNnSc7d1	protonové
číslem	číslo	k1gNnSc7	číslo
prvku	prvek	k1gInSc2	prvek
-	-	kIx~	-
lithium	lithium	k1gNnSc1	lithium
je	být	k5eAaImIp3nS	být
nejméně	málo	k6eAd3	málo
reaktivní	reaktivní	k2eAgNnSc1d1	reaktivní
a	a	k8xC	a
francium	francium	k1gNnSc1	francium
je	být	k5eAaImIp3nS	být
nejreaktivnější	reaktivní	k2eAgNnSc1d3	reaktivní
<g/>
.	.	kIx.	.
</s>
<s>
Reagují	reagovat	k5eAaBmIp3nP	reagovat
přímo	přímo	k6eAd1	přímo
s	s	k7c7	s
halogeny	halogen	k1gInPc7	halogen
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
iontových	iontový	k2eAgFnPc2d1	iontová
solí	sůl	k1gFnPc2	sůl
a	a	k8xC	a
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
silných	silný	k2eAgInPc2d1	silný
hydroxidů	hydroxid	k1gInPc2	hydroxid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Alkalické	alkalický	k2eAgInPc1d1	alkalický
kovy	kov	k1gInPc1	kov
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgInPc1d1	známý
bouřlivou	bouřlivý	k2eAgFnSc7d1	bouřlivá
reakcí	reakce	k1gFnSc7	reakce
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
reaktivita	reaktivita	k1gFnSc1	reaktivita
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
se	s	k7c7	s
stoupajícím	stoupající	k2eAgNnSc7d1	stoupající
protonovým	protonový	k2eAgNnSc7d1	protonové
číslem	číslo	k1gNnSc7	číslo
prvku	prvek	k1gInSc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
reakci	reakce	k1gFnSc6	reakce
vzniká	vznikat	k5eAaImIp3nS	vznikat
hydroxid	hydroxid	k1gInSc4	hydroxid
<g/>
,	,	kIx,	,
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
se	se	k3xPyFc4	se
plynný	plynný	k2eAgInSc1d1	plynný
vodík	vodík	k1gInSc1	vodík
a	a	k8xC	a
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
tepla	teplo	k1gNnSc2	teplo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2	[number]	k4	2
K	k	k7c3	k
+	+	kIx~	+
2	[number]	k4	2
H2O	H2O	k1gFnSc2	H2O
→	→	k?	→
2	[number]	k4	2
KOH	KOH	kA	KOH
+	+	kIx~	+
H2V	H2V	k1gFnSc4	H2V
kapalném	kapalný	k2eAgInSc6d1	kapalný
amoniaku	amoniak	k1gInSc6	amoniak
se	se	k3xPyFc4	se
alkalické	alkalický	k2eAgInPc1d1	alkalický
kovy	kov	k1gInPc1	kov
rozpouštějí	rozpouštět	k5eAaImIp3nP	rozpouštět
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
tmavě	tmavě	k6eAd1	tmavě
modrých	modrý	k2eAgInPc2d1	modrý
<g/>
,	,	kIx,	,
paramagnetických	paramagnetický	k2eAgInPc2d1	paramagnetický
roztoků	roztok	k1gInPc2	roztok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
+	+	kIx~	+
NH3	NH3	k1gFnSc3	NH3
→	→	k?	→
K	K	kA	K
<g/>
+	+	kIx~	+
+	+	kIx~	+
e	e	k0	e
<g/>
−	−	k?	−
<g/>
Vzniklé	vzniklý	k2eAgInPc4d1	vzniklý
solvatované	solvatovaný	k2eAgInPc4d1	solvatovaný
elektrony	elektron	k1gInPc4	elektron
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
dobrým	dobrý	k2eAgNnSc7d1	dobré
redukčním	redukční	k2eAgNnSc7d1	redukční
činidlem	činidlo	k1gNnSc7	činidlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
těchto	tento	k3xDgFnPc2	tento
dvou	dva	k4xCgFnPc2	dva
základních	základní	k2eAgFnPc2d1	základní
reakcí	reakce	k1gFnPc2	reakce
reagují	reagovat	k5eAaBmIp3nP	reagovat
alkalické	alkalický	k2eAgInPc1d1	alkalický
kovy	kov	k1gInPc1	kov
také	také	k9	také
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
oxidů	oxid	k1gInPc2	oxid
<g/>
,	,	kIx,	,
peroxidů	peroxid	k1gInPc2	peroxid
nebo	nebo	k8xC	nebo
hyperoxidů	hyperoxid	k1gInPc2	hyperoxid
<g/>
,	,	kIx,	,
za	za	k7c2	za
mírného	mírný	k2eAgNnSc2d1	mírné
zahřátí	zahřátí	k1gNnSc2	zahřátí
s	s	k7c7	s
vodíkem	vodík	k1gInSc7	vodík
a	a	k8xC	a
dusíkem	dusík	k1gInSc7	dusík
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
lithium	lithium	k1gNnSc1	lithium
nejméně	málo	k6eAd3	málo
reaktivní	reaktivní	k2eAgMnSc1d1	reaktivní
<g/>
,	,	kIx,	,
tak	tak	k9	tak
jako	jako	k8xC	jako
jediné	jediné	k1gNnSc1	jediné
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
dusíkem	dusík	k1gInSc7	dusík
za	za	k7c4	za
normální	normální	k2eAgFnPc4d1	normální
teploty	teplota	k1gFnPc4	teplota
a	a	k8xC	a
při	při	k7c6	při
zahřátí	zahřátí	k1gNnSc6	zahřátí
také	také	k9	také
dokonce	dokonce	k9	dokonce
s	s	k7c7	s
uhlíkem	uhlík	k1gInSc7	uhlík
a	a	k8xC	a
křemíkem	křemík	k1gInSc7	křemík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
==	==	k?	==
</s>
</p>
<p>
<s>
Díky	díky	k7c3	díky
vysoké	vysoký	k2eAgFnSc3d1	vysoká
reaktivitě	reaktivita	k1gFnSc3	reaktivita
se	se	k3xPyFc4	se
alkalické	alkalický	k2eAgInPc1d1	alkalický
kovy	kov	k1gInPc1	kov
volně	volně	k6eAd1	volně
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
hojně	hojně	k6eAd1	hojně
se	se	k3xPyFc4	se
však	však	k9	však
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
svých	svůj	k3xOyFgFnPc2	svůj
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Sodík	sodík	k1gInSc1	sodík
a	a	k8xC	a
draslík	draslík	k1gInSc1	draslík
dokonce	dokonce	k9	dokonce
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
deset	deset	k4xCc4	deset
nejhojněji	hojně	k6eAd3	hojně
se	se	k3xPyFc4	se
vyskytujících	vyskytující	k2eAgInPc2d1	vyskytující
prvků	prvek	k1gInPc2	prvek
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
alkalických	alkalický	k2eAgInPc2d1	alkalický
kovů	kov	k1gInPc2	kov
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
svých	svůj	k3xOyFgFnPc2	svůj
iontových	iontový	k2eAgFnPc2d1	iontová
sloučenin	sloučenina	k1gFnPc2	sloučenina
-	-	kIx~	-
solí	sůl	k1gFnPc2	sůl
(	(	kIx(	(
<g/>
nejvíce	nejvíce	k6eAd1	nejvíce
je	být	k5eAaImIp3nS	být
zastoupena	zastoupen	k2eAgFnSc1d1	zastoupena
sůl	sůl	k1gFnSc1	sůl
NaCl	NaCla	k1gFnPc2	NaCla
neboli	neboli	k8xC	neboli
chlorid	chlorid	k1gInSc1	chlorid
sodný	sodný	k2eAgInSc1d1	sodný
a	a	k8xC	a
sylvín	sylvín	k1gInSc1	sylvín
KCl	KCl	k1gFnSc2	KCl
neboli	neboli	k8xC	neboli
chlorid	chlorid	k1gInSc1	chlorid
draselný	draselný	k2eAgInSc1d1	draselný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
také	také	k9	také
získávají	získávat	k5eAaImIp3nP	získávat
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
rozpuštěné	rozpuštěný	k2eAgInPc1d1	rozpuštěný
minerály	minerál	k1gInPc1	minerál
se	se	k3xPyFc4	se
také	také	k9	také
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dříve	dříve	k6eAd2	dříve
bylo	být	k5eAaImAgNnS	být
moře	moře	k1gNnSc1	moře
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
při	při	k7c6	při
vrásnění	vrásnění	k1gNnSc6	vrásnění
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
moře	moře	k1gNnSc1	moře
vysušilo	vysušit	k5eAaPmAgNnS	vysušit
a	a	k8xC	a
minerály	minerál	k1gInPc1	minerál
zkrystalizovaly	zkrystalizovat	k5eAaPmAgInP	zkrystalizovat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Solnohradu	Solnohrad	k1gInSc2	Solnohrad
-	-	kIx~	-
Salzburgu	Salzburg	k1gInSc2	Salzburg
<g/>
)	)	kIx)	)
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
velká	velký	k2eAgNnPc1d1	velké
podzemní	podzemní	k2eAgNnPc1d1	podzemní
naleziště	naleziště	k1gNnPc1	naleziště
kamenné	kamenný	k2eAgFnSc2d1	kamenná
soli	sůl	k1gFnSc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poměrně	poměrně	k6eAd1	poměrně
velkém	velký	k2eAgNnSc6d1	velké
množství	množství	k1gNnSc6	množství
se	se	k3xPyFc4	se
také	také	k9	také
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
ledky	ledek	k1gInPc1	ledek
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c6	na
chilském	chilský	k2eAgNnSc6d1	Chilské
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
mineralizací	mineralizace	k1gFnSc7	mineralizace
rostlinných	rostlinný	k2eAgInPc2d1	rostlinný
zbytků	zbytek	k1gInPc2	zbytek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Získávání	získávání	k1gNnSc1	získávání
a	a	k8xC	a
výroba	výroba	k1gFnSc1	výroba
==	==	k?	==
</s>
</p>
<p>
<s>
Všechny	všechen	k3xTgInPc1	všechen
alkalické	alkalický	k2eAgInPc1d1	alkalický
kovy	kov	k1gInPc1	kov
se	se	k3xPyFc4	se
získávají	získávat	k5eAaImIp3nP	získávat
především	především	k9	především
z	z	k7c2	z
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
lithium	lithium	k1gNnSc1	lithium
se	se	k3xPyFc4	se
ve	v	k7c6	v
větším	veliký	k2eAgNnSc6d2	veliký
množství	množství	k1gNnSc6	množství
získává	získávat	k5eAaImIp3nS	získávat
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
minerálů	minerál	k1gInPc2	minerál
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ostatních	ostatní	k2eAgInPc2d1	ostatní
alkalických	alkalický	k2eAgInPc2d1	alkalický
kovů	kov	k1gInPc2	kov
se	se	k3xPyFc4	se
nejdříve	dříve	k6eAd3	dříve
odpaří	odpařit	k5eAaPmIp3nS	odpařit
mořská	mořský	k2eAgFnSc1d1	mořská
voda	voda	k1gFnSc1	voda
a	a	k8xC	a
nechají	nechat	k5eAaPmIp3nP	nechat
se	se	k3xPyFc4	se
zkrystalizovat	zkrystalizovat	k5eAaPmF	zkrystalizovat
minerály	minerál	k1gInPc4	minerál
rozpuštěné	rozpuštěný	k2eAgInPc4d1	rozpuštěný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
sloučeniny	sloučenina	k1gFnPc1	sloučenina
alkalických	alkalický	k2eAgInPc2d1	alkalický
kovů	kov	k1gInPc2	kov
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	se	k3xPyFc2	se
odseparují	odseparovat	k5eAaImIp3nP	odseparovat
a	a	k8xC	a
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
získávají	získávat	k5eAaImIp3nP	získávat
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
jejich	jejich	k3xOp3gFnSc2	jejich
taveniny	tavenina	k1gFnSc2	tavenina
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
rovnou	rovnou	k6eAd1	rovnou
elektrolyzují	elektrolyzovat	k5eAaImIp3nP	elektrolyzovat
a	a	k8xC	a
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
kovy	kov	k1gInPc1	kov
se	se	k3xPyFc4	se
odseparují	odseparovat	k5eAaPmIp3nP	odseparovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
různých	různý	k2eAgFnPc2d1	různá
teplot	teplota	k1gFnPc2	teplota
tání	tání	k1gNnSc2	tání
a	a	k8xC	a
varu	var	k1gInSc2	var
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Alkalické	alkalický	k2eAgInPc1d1	alkalický
kovy	kov	k1gInPc1	kov
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
použít	použít	k5eAaPmF	použít
především	především	k6eAd1	především
jako	jako	k8xC	jako
dobrá	dobrý	k2eAgNnPc1d1	dobré
redukovadla	redukovadlo	k1gNnPc1	redukovadlo
v	v	k7c6	v
organické	organický	k2eAgFnSc6d1	organická
chemii	chemie	k1gFnSc6	chemie
nebo	nebo	k8xC	nebo
analytické	analytický	k2eAgFnSc6d1	analytická
chemii	chemie	k1gFnSc6	chemie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
vysoké	vysoký	k2eAgFnSc3d1	vysoká
reaktivitě	reaktivita	k1gFnSc3	reaktivita
se	se	k3xPyFc4	se
na	na	k7c4	na
tyto	tento	k3xDgFnPc4	tento
reakce	reakce	k1gFnPc4	reakce
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
nepoužívají	používat	k5eNaImIp3nP	používat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
čistých	čistý	k2eAgInPc2d1	čistý
kovů	kov	k1gInPc2	kov
má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgNnSc4d3	veliký
využití	využití	k1gNnSc4	využití
lithium	lithium	k1gNnSc1	lithium
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
nejstálejší	stálý	k2eAgMnSc1d3	nejstálejší
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
a	a	k8xC	a
nejméně	málo	k6eAd3	málo
reaktivní	reaktivní	k2eAgInSc1d1	reaktivní
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ostatních	ostatní	k2eAgInPc2d1	ostatní
alkalických	alkalický	k2eAgInPc2d1	alkalický
kovů	kov	k1gInPc2	kov
jsou	být	k5eAaImIp3nP	být
významné	významný	k2eAgFnPc1d1	významná
především	především	k9	především
jejich	jejich	k3xOp3gFnPc1	jejich
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Jursík	Jursík	k1gMnSc1	Jursík
F.	F.	kA	F.
<g/>
:	:	kIx,	:
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7080-504-8	[number]	k4	80-7080-504-8
(	(	kIx(	(
<g/>
elektronická	elektronický	k2eAgFnSc1d1	elektronická
verze	verze	k1gFnSc1	verze
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc4	vydání
1961	[number]	k4	1961
</s>
</p>
<p>
<s>
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwooda	k1gFnPc2	Greenwooda
-	-	kIx~	-
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc1	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Mnemotechnické	mnemotechnický	k2eAgFnPc1d1	mnemotechnická
pomůcky	pomůcka	k1gFnPc1	pomůcka
–	–	k?	–
chemie	chemie	k1gFnSc2	chemie
</s>
</p>
