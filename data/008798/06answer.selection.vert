<s>
Lví	lví	k2eAgMnSc1d1	lví
král	král	k1gMnSc1	král
2	[number]	k4	2
<g/>
:	:	kIx,	:
Simbův	Simbův	k2eAgInSc1d1	Simbův
příběh	příběh	k1gInSc1	příběh
(	(	kIx(	(
<g/>
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
originále	originál	k1gInSc6	originál
The	The	k1gMnSc1	The
Lion	Lion	k1gMnSc1	Lion
King	King	k1gMnSc1	King
II	II	kA	II
<g/>
:	:	kIx,	:
Simba	Simba	k1gMnSc1	Simba
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Pride	Prid	k1gMnSc5	Prid
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc1d1	americký
animovaný	animovaný	k2eAgInSc1d1	animovaný
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
natočil	natočit	k5eAaBmAgMnS	natočit
režisér	režisér	k1gMnSc1	režisér
Darrell	Darrell	k1gInSc4	Darrell
Rooney	Roonea	k1gFnSc2	Roonea
<g/>
.	.	kIx.	.
</s>
