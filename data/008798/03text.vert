<p>
<s>
Lví	lví	k2eAgMnSc1d1	lví
král	král	k1gMnSc1	král
2	[number]	k4	2
<g/>
:	:	kIx,	:
Simbův	Simbův	k2eAgInSc1d1	Simbův
příběh	příběh	k1gInSc1	příběh
(	(	kIx(	(
<g/>
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
originále	originál	k1gInSc6	originál
The	The	k1gMnSc1	The
Lion	Lion	k1gMnSc1	Lion
King	King	k1gMnSc1	King
II	II	kA	II
<g/>
:	:	kIx,	:
Simba	Simba	k1gMnSc1	Simba
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Pride	Prid	k1gMnSc5	Prid
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc1d1	americký
animovaný	animovaný	k2eAgInSc1d1	animovaný
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
natočil	natočit	k5eAaBmAgMnS	natočit
režisér	režisér	k1gMnSc1	režisér
Darrell	Darrell	k1gInSc4	Darrell
Rooney	Roonea	k1gFnSc2	Roonea
<g/>
.	.	kIx.	.
</s>
<s>
Snímek	snímek	k1gInSc1	snímek
je	být	k5eAaImIp3nS	být
volným	volný	k2eAgNnSc7d1	volné
pokračováním	pokračování	k1gNnSc7	pokračování
filmu	film	k1gInSc2	film
Lví	lví	k2eAgMnSc1d1	lví
král	král	k1gMnSc1	král
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Film	film	k1gInSc1	film
nebyl	být	k5eNaImAgMnS	být
promítán	promítat	k5eAaImNgMnS	promítat
v	v	k7c6	v
kinech	kino	k1gNnPc6	kino
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
VHS	VHS	kA	VHS
27	[number]	k4	27
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
na	na	k7c6	na
DVD	DVD	kA	DVD
potom	potom	k6eAd1	potom
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
limitované	limitovaný	k2eAgFnSc6d1	limitovaná
edici	edice	k1gFnSc6	edice
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Simbovi	Simba	k1gMnSc3	Simba
a	a	k8xC	a
Nale	Nale	k1gFnSc3	Nale
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
neposedná	posedný	k2eNgFnSc1d1	neposedná
a	a	k8xC	a
svéhlavá	svéhlavý	k2eAgFnSc1d1	svéhlavá
dcera	dcera	k1gFnSc1	dcera
Kiara	Kiara	k1gFnSc1	Kiara
<g/>
.	.	kIx.	.
</s>
<s>
Simba	Simba	k1gFnSc1	Simba
ji	on	k3xPp3gFnSc4	on
přehnaně	přehnaně	k6eAd1	přehnaně
chrání	chránit	k5eAaImIp3nS	chránit
před	před	k7c7	před
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
<g/>
;	;	kIx,	;
proto	proto	k8xC	proto
pověří	pověřit	k5eAaPmIp3nS	pověřit
své	svůj	k3xOyFgMnPc4	svůj
dva	dva	k4xCgMnPc4	dva
přátele	přítel	k1gMnPc4	přítel
<g/>
,	,	kIx,	,
surikatu	surikata	k1gFnSc4	surikata
Timona	Timon	k1gMnSc2	Timon
a	a	k8xC	a
prase	prase	k1gNnSc1	prase
Pumbu	Pumb	k1gInSc2	Pumb
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ji	on	k3xPp3gFnSc4	on
hlídali	hlídat	k5eAaImAgMnP	hlídat
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
Kiaře	Kiař	k1gFnSc2	Kiař
jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
podaří	podařit	k5eAaPmIp3nS	podařit
utéci	utéct	k5eAaPmF	utéct
až	až	k9	až
za	za	k7c4	za
hranice	hranice	k1gFnPc4	hranice
Lví	lví	k2eAgFnSc2d1	lví
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žijí	žít	k5eAaImIp3nP	žít
Vyhnanci	vyhnanec	k1gMnPc1	vyhnanec
–	–	k?	–
příznivci	příznivec	k1gMnPc1	příznivec
krutovládce	krutovládce	k1gMnSc1	krutovládce
Scara	Scara	k1gMnSc1	Scara
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
dílu	díl	k1gInSc6	díl
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
seznámí	seznámit	k5eAaPmIp3nS	seznámit
s	s	k7c7	s
malým	malé	k1gNnSc7	malé
Kovu	kov	k1gInSc2	kov
<g/>
,	,	kIx,	,
synem	syn	k1gMnSc7	syn
Ziry	Zira	k1gFnSc2	Zira
<g/>
.	.	kIx.	.
</s>
<s>
Kovu	kov	k1gInSc3	kov
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
samostatný	samostatný	k2eAgInSc1d1	samostatný
a	a	k8xC	a
milý	milý	k2eAgInSc1d1	milý
<g/>
,	,	kIx,	,
když	když	k8xS	když
společně	společně	k6eAd1	společně
utečou	utéct	k5eAaPmIp3nP	utéct
divokým	divoký	k2eAgMnPc3d1	divoký
krokodýlům	krokodýl	k1gMnPc3	krokodýl
<g/>
,	,	kIx,	,
spřátelí	spřátelit	k5eAaPmIp3nS	spřátelit
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kiaru	Kiara	k1gFnSc4	Kiara
však	však	k9	však
hledají	hledat	k5eAaImIp3nP	hledat
její	její	k3xOp3gMnPc1	její
rodiče	rodič	k1gMnPc1	rodič
a	a	k8xC	a
Kovua	Kovu	k2eAgFnSc1d1	Kovu
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
Zira	Zira	k1gFnSc1	Zira
<g/>
,	,	kIx,	,
vůdkyně	vůdkyně	k1gFnSc1	vůdkyně
Vyhnanců	vyhnanec	k1gMnPc2	vyhnanec
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
nahradila	nahradit	k5eAaPmAgFnS	nahradit
Scara	Scar	k1gMnSc4	Scar
a	a	k8xC	a
jež	jenž	k3xRgNnSc1	jenž
je	být	k5eAaImIp3nS	být
plná	plný	k2eAgFnSc1d1	plná
nenávisti	nenávist	k1gFnSc2	nenávist
k	k	k7c3	k
Simbovi	Simba	k1gMnSc3	Simba
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc7	který
stále	stále	k6eAd1	stále
vštěpuje	vštěpovat	k5eAaBmIp3nS	vštěpovat
i	i	k9	i
Kovuovi	Kovuův	k2eAgMnPc1d1	Kovuův
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vzájemném	vzájemný	k2eAgNnSc6d1	vzájemné
setkání	setkání	k1gNnSc6	setkání
Simbovi	Simbův	k2eAgMnPc1d1	Simbův
s	s	k7c7	s
výhrůžkami	výhrůžka	k1gFnPc7	výhrůžka
vyčítá	vyčítat	k5eAaImIp3nS	vyčítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gFnSc4	její
smečku	smečka	k1gFnSc4	smečka
vyhnal	vyhnat	k5eAaPmAgMnS	vyhnat
mimo	mimo	k7c4	mimo
Lví	lví	k2eAgFnSc4d1	lví
říši	říše	k1gFnSc4	říše
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
Kiara	Kiara	k1gMnSc1	Kiara
vyroste	vyrůst	k5eAaPmIp3nS	vyrůst
<g/>
,	,	kIx,	,
vydává	vydávat	k5eAaImIp3nS	vydávat
se	se	k3xPyFc4	se
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
lov	lov	k1gInSc4	lov
<g/>
.	.	kIx.	.
</s>
<s>
Simba	Simba	k1gFnSc1	Simba
slíbí	slíbit	k5eAaPmIp3nS	slíbit
své	svůj	k3xOyFgFnSc3	svůj
dceři	dcera	k1gFnSc3	dcera
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
nechá	nechat	k5eAaPmIp3nS	nechat
jen	jen	k9	jen
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
slib	slib	k1gInSc4	slib
poruší	porušit	k5eAaPmIp3nS	porušit
a	a	k8xC	a
Kiara	Kiara	k1gFnSc1	Kiara
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dokázala	dokázat	k5eAaPmAgFnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
dokáže	dokázat	k5eAaPmIp3nS	dokázat
<g/>
,	,	kIx,	,
uteče	utéct	k5eAaPmIp3nS	utéct
do	do	k7c2	do
země	zem	k1gFnSc2	zem
vyhnanců	vyhnanec	k1gMnPc2	vyhnanec
lovit	lovit	k5eAaImF	lovit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Zira	Zira	k1gMnSc1	Zira
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kiara	Kiara	k1gFnSc1	Kiara
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
vyhnanců	vyhnanec	k1gMnPc2	vyhnanec
<g/>
,	,	kIx,	,
vymyslí	vymyslet	k5eAaPmIp3nS	vymyslet
záludný	záludný	k2eAgInSc1d1	záludný
plán	plán	k1gInSc1	plán
<g/>
:	:	kIx,	:
pověří	pověřit	k5eAaPmIp3nS	pověřit
Kovuovy	Kovuův	k2eAgMnPc4d1	Kovuův
sourozence	sourozenec	k1gMnPc4	sourozenec
Nuku	nuk	k1gInSc2	nuk
a	a	k8xC	a
Vitani	Vitan	k1gMnPc1	Vitan
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zapálili	zapálit	k5eAaPmAgMnP	zapálit
savanu	savana	k1gFnSc4	savana
<g/>
.	.	kIx.	.
</s>
<s>
Kiara	Kiara	k1gFnSc1	Kiara
je	být	k5eAaImIp3nS	být
uvězněna	uvěznit	k5eAaPmNgFnS	uvěznit
v	v	k7c6	v
ohnivém	ohnivý	k2eAgInSc6d1	ohnivý
kruhu	kruh	k1gInSc6	kruh
a	a	k8xC	a
nelze	lze	k6eNd1	lze
ji	on	k3xPp3gFnSc4	on
zachránit	zachránit	k5eAaPmF	zachránit
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
Kovua	Kovua	k1gMnSc1	Kovua
učili	učít	k5eAaPmAgMnP	učít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nenáviděl	nenávidět	k5eAaImAgMnS	nenávidět
a	a	k8xC	a
jednou	jednou	k6eAd1	jednou
i	i	k9	i
zabil	zabít	k5eAaPmAgMnS	zabít
Simbu	Simba	k1gMnSc4	Simba
<g/>
,	,	kIx,	,
mladý	mladý	k2eAgMnSc1d1	mladý
lev	lev	k1gMnSc1	lev
Kiaru	Kiar	k1gInSc2	Kiar
zachrání	zachránit	k5eAaPmIp3nS	zachránit
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
si	se	k3xPyFc3	se
získá	získat	k5eAaPmIp3nS	získat
Simbovu	Simbův	k2eAgFnSc4d1	Simbova
důvěru	důvěra	k1gFnSc4	důvěra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Plán	plán	k1gInSc1	plán
Ziry	Zira	k1gFnSc2	Zira
na	na	k7c4	na
zabití	zabití	k1gNnSc4	zabití
Simby	Simba	k1gFnSc2	Simba
Kovuem	Kovu	k1gInSc7	Kovu
však	však	k9	však
dostane	dostat	k5eAaPmIp3nS	dostat
trhlinu	trhlina	k1gFnSc4	trhlina
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
Kiara	Kiar	k1gInSc2	Kiar
a	a	k8xC	a
Kovu	kov	k1gInSc2	kov
do	do	k7c2	do
sebe	se	k3xPyFc2	se
zamilují	zamilovat	k5eAaPmIp3nP	zamilovat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc4	ten
Kovua	Kovu	k2eAgFnSc1d1	Kovu
promění	proměnit	k5eAaPmIp3nS	proměnit
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
říci	říct	k5eAaPmF	říct
Simbovi	Simba	k1gMnSc3	Simba
o	o	k7c6	o
Ziřině	Ziřina	k1gFnSc6	Ziřina
lsti	lest	k1gFnSc2	lest
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
vzdálí	vzdálit	k5eAaPmIp3nS	vzdálit
od	od	k7c2	od
Lví	lví	k2eAgFnSc2d1	lví
skály	skála	k1gFnSc2	skála
<g/>
,	,	kIx,	,
Zira	Zir	k2eAgFnSc1d1	Zir
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
smečkou	smečka	k1gFnSc7	smečka
na	na	k7c4	na
Simbu	Simba	k1gFnSc4	Simba
zaútočí	zaútočit	k5eAaPmIp3nP	zaútočit
se	s	k7c7	s
slovy	slovo	k1gNnPc7	slovo
"	"	kIx"	"
<g/>
Výborně	výborně	k6eAd1	výborně
<g/>
,	,	kIx,	,
Kovu	kov	k1gInSc2	kov
<g/>
,	,	kIx,	,
přesně	přesně	k6eAd1	přesně
jak	jak	k6eAd1	jak
jsme	být	k5eAaImIp1nP	být
to	ten	k3xDgNnSc4	ten
plánovali	plánovat	k5eAaImAgMnP	plánovat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
útoku	útok	k1gInSc6	útok
je	být	k5eAaImIp3nS	být
Simba	Simba	k1gMnSc1	Simba
zraněn	zranit	k5eAaPmNgMnS	zranit
a	a	k8xC	a
Nuka	Nuka	k1gMnSc1	Nuka
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Kovu	kov	k1gInSc2	kov
se	se	k3xPyFc4	se
odvrátí	odvrátit	k5eAaPmIp3nS	odvrátit
od	od	k7c2	od
své	svůj	k3xOyFgFnSc2	svůj
manipulující	manipulující	k2eAgFnSc2d1	manipulující
matky	matka	k1gFnSc2	matka
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
Simbovu	Simbův	k2eAgFnSc4d1	Simbova
smečku	smečka	k1gFnSc4	smečka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
využila	využít	k5eAaPmAgFnS	využít
jeho	jeho	k3xOp3gNnPc4	jeho
zranění	zranění	k1gNnPc4	zranění
<g/>
,	,	kIx,	,
napadnout	napadnout	k5eAaPmF	napadnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Simba	Simba	k1gFnSc1	Simba
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gInSc2	on
Kovu	kov	k1gInSc2	kov
zradil	zradit	k5eAaPmAgMnS	zradit
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
jej	on	k3xPp3gMnSc4	on
vykáže	vykázat	k5eAaPmIp3nS	vykázat
ze	z	k7c2	z
Lví	lví	k2eAgFnSc2d1	lví
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Kiara	Kiara	k1gFnSc1	Kiara
se	se	k3xPyFc4	se
otce	otka	k1gFnSc3	otka
snaží	snažit	k5eAaImIp3nS	snažit
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
ale	ale	k9	ale
chce	chtít	k5eAaImIp3nS	chtít
být	být	k5eAaImF	být
jako	jako	k8xC	jako
svůj	svůj	k3xOyFgMnSc1	svůj
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Kiara	Kiara	k1gFnSc1	Kiara
uteče	utéct	k5eAaPmIp3nS	utéct
se	s	k7c7	s
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nikdy	nikdy	k6eAd1	nikdy
nebudeš	být	k5eNaImBp2nS	být
jako	jako	k9	jako
Mufasa	Mufasa	k1gFnSc1	Mufasa
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Kovua	Kovua	k1gFnSc1	Kovua
přesvědčí	přesvědčit	k5eAaPmIp3nS	přesvědčit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
a	a	k8xC	a
požádal	požádat	k5eAaPmAgMnS	požádat
Simbu	Simba	k1gFnSc4	Simba
o	o	k7c4	o
přijetí	přijetí	k1gNnSc4	přijetí
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
přijdou	přijít	k5eAaPmIp3nP	přijít
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
spolu	spolu	k6eAd1	spolu
obě	dva	k4xCgFnPc1	dva
smečky	smečka	k1gFnPc1	smečka
bojují	bojovat	k5eAaImIp3nP	bojovat
<g/>
.	.	kIx.	.
</s>
<s>
Kiara	Kiara	k6eAd1	Kiara
však	však	k9	však
Simbovi	Simbův	k2eAgMnPc1d1	Simbův
domluví	domluvit	k5eAaPmIp3nP	domluvit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
všichni	všechen	k3xTgMnPc1	všechen
stejní	stejný	k2eAgMnPc1d1	stejný
a	a	k8xC	a
měli	mít	k5eAaImAgMnP	mít
by	by	kYmCp3nP	by
žít	žít	k5eAaImF	žít
v	v	k7c6	v
míru	mír	k1gInSc6	mír
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
malá	malý	k2eAgFnSc1d1	malá
<g/>
,	,	kIx,	,
řekl	říct	k5eAaPmAgMnS	říct
jí	on	k3xPp3gFnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
všichni	všechen	k3xTgMnPc1	všechen
jedna	jeden	k4xCgFnSc1	jeden
rodina	rodina	k1gFnSc1	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Simbovu	Simbův	k2eAgFnSc4d1	Simbova
nabídku	nabídka	k1gFnSc4	nabídka
smíru	smír	k1gInSc2	smír
přijme	přijmout	k5eAaPmIp3nS	přijmout
nejprve	nejprve	k6eAd1	nejprve
Vitani	Vitaň	k1gFnSc3	Vitaň
a	a	k8xC	a
poté	poté	k6eAd1	poté
všechny	všechen	k3xTgFnPc1	všechen
ostatní	ostatní	k2eAgFnPc1d1	ostatní
lvice	lvice	k1gFnPc1	lvice
na	na	k7c6	na
Ziřině	Ziřin	k2eAgFnSc6d1	Ziřin
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Ziru	Ziru	k6eAd1	Ziru
to	ten	k3xDgNnSc1	ten
rozzuří	rozzuřit	k5eAaPmIp3nS	rozzuřit
a	a	k8xC	a
vrhne	vrhnout	k5eAaImIp3nS	vrhnout
se	se	k3xPyFc4	se
na	na	k7c4	na
Simbu	Simba	k1gFnSc4	Simba
<g/>
,	,	kIx,	,
před	před	k7c4	před
kterého	který	k3yIgMnSc4	který
ale	ale	k8xC	ale
skočí	skočit	k5eAaPmIp3nS	skočit
Kiara	Kiara	k1gFnSc1	Kiara
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
lvice	lvice	k1gFnPc1	lvice
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
zřítí	zřítit	k5eAaPmIp3nS	zřítit
na	na	k7c4	na
útes	útes	k1gInSc4	útes
nad	nad	k7c7	nad
rozvodněnou	rozvodněný	k2eAgFnSc7d1	rozvodněná
řekou	řeka	k1gFnSc7	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Zira	Zira	k6eAd1	Zira
odtud	odtud	k6eAd1	odtud
spadne	spadnout	k5eAaPmIp3nS	spadnout
a	a	k8xC	a
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
Kiařinu	Kiařin	k2eAgFnSc4d1	Kiařin
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
Kiara	Kiar	k1gInSc2	Kiar
a	a	k8xC	a
Kovu	kov	k1gInSc2	kov
vezmou	vzít	k5eAaPmIp3nP	vzít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Hudba	hudba	k1gFnSc1	hudba
==	==	k?	==
</s>
</p>
<p>
<s>
Hudbu	hudba	k1gFnSc4	hudba
ve	v	k7c6	v
filmu	film	k1gInSc6	film
napsal	napsat	k5eAaBmAgMnS	napsat
skladatel	skladatel	k1gMnSc1	skladatel
Nick	Nick	k1gMnSc1	Nick
Glennie-Smith	Glennie-Smith	k1gMnSc1	Glennie-Smith
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
snímku	snímek	k1gInSc6	snímek
dále	daleko	k6eAd2	daleko
zaznělo	zaznět	k5eAaImAgNnS	zaznět
šest	šest	k4xCc1	šest
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc7	jejichž
autory	autor	k1gMnPc7	autor
jsou	být	k5eAaImIp3nP	být
další	další	k2eAgMnPc1d1	další
skladatelé	skladatel	k1gMnPc1	skladatel
a	a	k8xC	a
textaři	textař	k1gMnPc1	textař
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
He	he	k0	he
Lives	Lives	k1gMnSc1	Lives
in	in	k?	in
You	You	k1gMnSc1	You
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
autoři	autor	k1gMnPc1	autor
<g/>
:	:	kIx,	:
Mark	Mark	k1gMnSc1	Mark
Mancina	Mancina	k1gMnSc1	Mancina
<g/>
,	,	kIx,	,
Jay	Jay	k1gMnSc1	Jay
Rifkin	Rifkin	k1gMnSc1	Rifkin
<g/>
,	,	kIx,	,
Lebo	Lebo	k?	Lebo
M.	M.	kA	M.
<g/>
;	;	kIx,	;
český	český	k2eAgInSc4d1	český
překlad	překlad	k1gInSc4	překlad
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
tobě	ty	k3xPp2nSc6	ty
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
–	–	k?	–
Úvodní	úvodní	k2eAgFnSc4d1	úvodní
píseň	píseň	k1gFnSc4	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Rafiki	Rafiki	k6eAd1	Rafiki
říká	říkat	k5eAaImIp3nS	říkat
Simbovi	Simba	k1gMnSc3	Simba
<g/>
,	,	kIx,	,
že	že	k8xS	že
zesnulý	zesnulý	k1gMnSc1	zesnulý
Mufasa	Mufas	k1gMnSc2	Mufas
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
a	a	k8xC	a
na	na	k7c4	na
vše	všechen	k3xTgNnSc4	všechen
dohlíží	dohlížet	k5eAaImIp3nS	dohlížet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
We	We	k1gMnSc1	We
Are	ar	k1gInSc5	ar
One	One	k1gMnPc6	One
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
autoři	autor	k1gMnPc1	autor
<g/>
:	:	kIx,	:
Tom	Tom	k1gMnSc1	Tom
Snow	Snow	k1gMnSc1	Snow
<g/>
,	,	kIx,	,
Marty	Marta	k1gFnPc1	Marta
Panzer	Panzer	k1gMnSc1	Panzer
<g/>
,	,	kIx,	,
Jack	Jack	k1gMnSc1	Jack
Feldman	Feldman	k1gMnSc1	Feldman
<g/>
;	;	kIx,	;
český	český	k2eAgInSc1d1	český
překlad	překlad	k1gInSc1	překlad
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jsme	být	k5eAaImIp1nP	být
jako	jako	k9	jako
jeden	jeden	k4xCgInSc4	jeden
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
–	–	k?	–
Simba	Simba	k1gFnSc1	Simba
zpívá	zpívat	k5eAaImIp3nS	zpívat
Kiaře	Kiař	k1gFnPc4	Kiař
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gFnSc1	její
rodina	rodina	k1gFnSc1	rodina
je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
My	my	k3xPp1nPc1	my
Lullaby	Lullab	k1gMnPc4	Lullab
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
autoři	autor	k1gMnPc1	autor
<g/>
:	:	kIx,	:
Joss	Joss	k1gInSc1	Joss
Whedon	Whedon	k1gInSc1	Whedon
<g/>
,	,	kIx,	,
Scott	Scott	k2eAgInSc1d1	Scott
Warrender	Warrender	k1gInSc1	Warrender
<g/>
;	;	kIx,	;
český	český	k2eAgInSc1d1	český
překlad	překlad	k1gInSc1	překlad
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Má	mít	k5eAaImIp3nS	mít
ukolébavka	ukolébavka	k1gFnSc1	ukolébavka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
–	–	k?	–
Zira	Zira	k1gFnSc1	Zira
se	se	k3xPyFc4	se
těší	těšit	k5eAaImIp3nS	těšit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
Kovu	kov	k1gInSc2	kov
zabije	zabít	k5eAaPmIp3nS	zabít
Simbu	Simba	k1gFnSc4	Simba
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
vyroste	vyrůst	k5eAaPmIp3nS	vyrůst
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Upendi	Upend	k1gMnPc1	Upend
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
autoři	autor	k1gMnPc1	autor
<g/>
:	:	kIx,	:
Kevin	Kevin	k1gMnSc1	Kevin
Quinn	Quinn	k1gMnSc1	Quinn
<g/>
,	,	kIx,	,
Randy	rand	k1gInPc1	rand
Petersen	Petersen	k1gInSc1	Petersen
<g/>
;	;	kIx,	;
český	český	k2eAgInSc1d1	český
překlad	překlad	k1gInSc1	překlad
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Upendi	Upend	k1gMnPc1	Upend
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
–	–	k?	–
Kovu	kov	k1gInSc2	kov
a	a	k8xC	a
Kiara	Kiar	k1gInSc2	Kiar
se	se	k3xPyFc4	se
do	do	k7c2	do
sebe	se	k3xPyFc2	se
zamilují	zamilovat	k5eAaPmIp3nP	zamilovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
One	One	k1gFnPc3	One
of	of	k?	of
Us	Us	k1gFnSc4	Us
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
autoři	autor	k1gMnPc1	autor
<g/>
:	:	kIx,	:
Tom	Tom	k1gMnSc1	Tom
Snow	Snow	k1gMnSc1	Snow
<g/>
,	,	kIx,	,
Jack	Jack	k1gMnSc1	Jack
Feldman	Feldman	k1gMnSc1	Feldman
<g/>
;	;	kIx,	;
český	český	k2eAgInSc1d1	český
překlad	překlad	k1gInSc1	překlad
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nás	my	k3xPp1nPc2	my
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
–	–	k?	–
Simba	Simba	k1gFnSc1	Simba
vyhání	vyhánět	k5eAaImIp3nS	vyhánět
Kovua	Kovu	k2eAgFnSc1d1	Kovu
ze	z	k7c2	z
Lví	lví	k2eAgFnSc2d1	lví
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Love	lov	k1gInSc5	lov
Will	Willa	k1gFnPc2	Willa
Find	Find	k?	Find
a	a	k8xC	a
Way	Way	k1gMnSc1	Way
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
autoři	autor	k1gMnPc1	autor
<g/>
:	:	kIx,	:
Tom	Tom	k1gMnSc1	Tom
Snow	Snow	k1gMnSc1	Snow
<g/>
,	,	kIx,	,
Jack	Jack	k1gMnSc1	Jack
Feldman	Feldman	k1gMnSc1	Feldman
<g/>
;	;	kIx,	;
český	český	k2eAgInSc1d1	český
překlad	překlad	k1gInSc1	překlad
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Láska	láska	k1gFnSc1	láska
si	se	k3xPyFc3	se
najde	najít	k5eAaPmIp3nS	najít
cestu	cesta	k1gFnSc4	cesta
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
–	–	k?	–
Kiara	Kiar	k1gInSc2	Kiar
a	a	k8xC	a
Kovu	kov	k1gInSc2	kov
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
společně	společně	k6eAd1	společně
překonají	překonat	k5eAaPmIp3nP	překonat
obtíže	obtíž	k1gFnPc4	obtíž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
The	The	k?	The
Lion	Lion	k1gMnSc1	Lion
King	King	k1gMnSc1	King
II	II	kA	II
<g/>
:	:	kIx,	:
Simba	Simba	k1gMnSc1	Simba
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Pride	Prid	k1gInSc5	Prid
v	v	k7c6	v
Internet	Internet	k1gInSc1	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lví	lví	k2eAgMnSc1d1	lví
král	král	k1gMnSc1	král
2	[number]	k4	2
<g/>
:	:	kIx,	:
Simbův	Simbův	k2eAgInSc1d1	Simbův
příběh	příběh	k1gInSc1	příběh
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
