<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
východní	východní	k2eAgInSc1d1	východní
blok	blok	k1gInSc1	blok
ze	z	k7c2	z
států	stát	k1gInPc2	stát
východní	východní	k2eAgFnSc2d1	východní
a	a	k8xC	a
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zprvu	zprvu	k6eAd1	zprvu
také	také	k9	také
vojensky	vojensky	k6eAd1	vojensky
okupoval	okupovat	k5eAaBmAgMnS	okupovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
si	se	k3xPyFc3	se
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
upevnil	upevnit	k5eAaPmAgInS	upevnit
moc	moc	k1gFnSc4	moc
vytvořením	vytvoření	k1gNnSc7	vytvoření
vojenského	vojenský	k2eAgNnSc2d1	vojenské
uskupení	uskupení	k1gNnSc2	uskupení
zvaného	zvaný	k2eAgInSc2d1	zvaný
Varšavská	varšavský	k2eAgFnSc1d1	Varšavská
smlouva	smlouva	k1gFnSc1	smlouva
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
–	–	k?	–
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
a	a	k8xC	a
hospodářského	hospodářský	k2eAgNnSc2d1	hospodářské
společenství	společenství	k1gNnSc2	společenství
zvaného	zvaný	k2eAgMnSc4d1	zvaný
Rada	Rada	k1gMnSc1	Rada
vzájemné	vzájemný	k2eAgFnSc2d1	vzájemná
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
pomoci	pomoc	k1gFnSc2	pomoc
(	(	kIx(	(
<g/>
RVHP	RVHP	kA	RVHP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
