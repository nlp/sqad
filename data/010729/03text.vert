<p>
<s>
Petr	Petr	k1gMnSc1	Petr
"	"	kIx"	"
<g/>
Hraboš	hraboš	k1gMnSc1	hraboš
<g/>
"	"	kIx"	"
Hrabalik	Hrabalik	k1gMnSc1	Hrabalik
(	(	kIx(	(
<g/>
*	*	kIx~	*
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
textař	textař	k1gMnSc1	textař
souboru	soubor	k1gInSc2	soubor
Našrot	Našrot	k1gMnSc1	Našrot
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
v	v	k7c6	v
Havlíčkově	Havlíčkův	k2eAgInSc6d1	Havlíčkův
Brodě	Brod	k1gInSc6	Brod
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
-	-	kIx~	-
v	v	k7c6	v
době	doba	k1gFnSc6	doba
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
ve	v	k7c6	v
fotbalovém	fotbalový	k2eAgInSc6d1	fotbalový
dorosteneckém	dorostenecký	k2eAgInSc6d1	dorostenecký
oddílu	oddíl	k1gInSc6	oddíl
Slovanu	Slovan	k1gInSc2	Slovan
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
-	-	kIx~	-
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
zajímat	zajímat	k5eAaImF	zajímat
o	o	k7c4	o
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
zejména	zejména	k6eAd1	zejména
ho	on	k3xPp3gMnSc4	on
zaujaly	zaujmout	k5eAaPmAgFnP	zaujmout
skupiny	skupina	k1gFnPc1	skupina
Beatles	Beatles	k1gFnSc1	Beatles
a	a	k8xC	a
Led	led	k1gInSc1	led
Zeppelin	Zeppelin	k2eAgInSc1d1	Zeppelin
<g/>
.	.	kIx.	.
</s>
<s>
Poslouchal	poslouchat	k5eAaImAgMnS	poslouchat
ale	ale	k9	ale
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
všechny	všechen	k3xTgInPc1	všechen
možné	možný	k2eAgInPc1d1	možný
styly	styl	k1gInPc1	styl
(	(	kIx(	(
<g/>
beat	beat	k1gInSc1	beat
<g/>
,	,	kIx,	,
hard	hard	k1gInSc1	hard
rock	rock	k1gInSc1	rock
<g/>
,	,	kIx,	,
art	art	k?	art
rock	rock	k1gInSc1	rock
<g/>
,	,	kIx,	,
jazz-rock	jazzock	k1gInSc1	jazz-rock
<g/>
,	,	kIx,	,
folk-rock	folkock	k1gInSc1	folk-rock
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
několik	několik	k4yIc1	několik
akordů	akord	k1gInPc2	akord
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
a	a	k8xC	a
figur	figura	k1gFnPc2	figura
na	na	k7c4	na
baskytaru	baskytara	k1gFnSc4	baskytara
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
si	se	k3xPyFc3	se
svoje	svůj	k3xOyFgInPc4	svůj
songy	song	k1gInPc4	song
psal	psát	k5eAaImAgInS	psát
sám	sám	k3xTgInSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
snahy	snaha	k1gFnPc1	snaha
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
vlastní	vlastní	k2eAgFnSc2d1	vlastní
skupiny	skupina	k1gFnSc2	skupina
vedou	vést	k5eAaImIp3nP	vést
někam	někam	k6eAd1	někam
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
podařilo	podařit	k5eAaPmAgNnS	podařit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
založil	založit	k5eAaPmAgInS	založit
skupinu	skupina	k1gFnSc4	skupina
Oxygen	oxygen	k1gInSc1	oxygen
(	(	kIx(	(
<g/>
fungovala	fungovat	k5eAaImAgFnS	fungovat
zhruba	zhruba	k6eAd1	zhruba
rok	rok	k1gInSc4	rok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
zpíval	zpívat	k5eAaImAgMnS	zpívat
a	a	k8xC	a
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
baskytaru	baskytara	k1gFnSc4	baskytara
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
ovšem	ovšem	k9	ovšem
kromě	kromě	k7c2	kromě
studia	studio	k1gNnSc2	studio
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
začal	začít	k5eAaPmAgInS	začít
jezdit	jezdit	k5eAaImF	jezdit
s	s	k7c7	s
novými	nový	k2eAgMnPc7d1	nový
brodskými	brodský	k2eAgMnPc7d1	brodský
přáteli	přítel	k1gMnPc7	přítel
z	z	k7c2	z
undergroundu	underground	k1gInSc2	underground
na	na	k7c4	na
neoficiální	oficiální	k2eNgInPc4d1	neoficiální
koncerty	koncert	k1gInPc4	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Zajímal	zajímat	k5eAaImAgInS	zajímat
se	se	k3xPyFc4	se
i	i	k9	i
o	o	k7c4	o
alternativní	alternativní	k2eAgFnSc4d1	alternativní
hudbu	hudba	k1gFnSc4	hudba
a	a	k8xC	a
nahrávky	nahrávka	k1gFnSc2	nahrávka
hnutí	hnutí	k1gNnSc1	hnutí
Rock	rock	k1gInSc1	rock
In	In	k1gMnSc1	In
Opposition	Opposition	k1gInSc1	Opposition
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
spoluzakladatelem	spoluzakladatel	k1gMnSc7	spoluzakladatel
souboru	soubor	k1gInSc2	soubor
Maama	Maama	k1gFnSc1	Maama
<g/>
,	,	kIx,	,
tvořícího	tvořící	k2eAgMnSc2d1	tvořící
různé	různý	k2eAgFnPc4d1	různá
zvukové	zvukový	k2eAgFnPc4d1	zvuková
koláže	koláž	k1gFnPc4	koláž
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
Maamy	Maama	k1gFnSc2	Maama
společně	společně	k6eAd1	společně
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
přáteli	přítel	k1gMnPc7	přítel
z	z	k7c2	z
havlíčkobrodských	havlíčkobrodský	k2eAgFnPc2d1	Havlíčkobrodská
kapel	kapela	k1gFnPc2	kapela
Křečový	křečový	k2eAgInSc1d1	křečový
Žíly	žíla	k1gFnSc2	žíla
a	a	k8xC	a
Gumovej	Gumovej	k?	Gumovej
Knedlík	knedlík	k1gInSc4	knedlík
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
společenství	společenství	k1gNnSc2	společenství
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgFnSc2d1	zvaná
HB-underground	HBnderground	k1gInSc4	HB-underground
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
odjel	odjet	k5eAaPmAgMnS	odjet
Hraboš	hraboš	k1gMnSc1	hraboš
pracovat	pracovat	k5eAaImF	pracovat
do	do	k7c2	do
Liberce	Liberec	k1gInSc2	Liberec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
během	během	k7c2	během
roku	rok	k1gInSc2	rok
stal	stát	k5eAaPmAgMnS	stát
hospodským	hospodský	k2eAgMnSc7d1	hospodský
folk-bluesovým	folkluesový	k2eAgMnSc7d1	folk-bluesový
interpretem	interpret	k1gMnSc7	interpret
a	a	k8xC	a
hráčem	hráč	k1gMnSc7	hráč
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Též	též	k9	též
sepisoval	sepisovat	k5eAaImAgMnS	sepisovat
texty	text	k1gInPc4	text
a	a	k8xC	a
básně	báseň	k1gFnPc4	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Víkendy	víkend	k1gInPc1	víkend
ale	ale	k9	ale
patřily	patřit	k5eAaImAgInP	patřit
jeho	jeho	k3xOp3gNnSc3	jeho
rodnému	rodný	k2eAgNnSc3d1	rodné
městu	město	k1gNnSc3	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
činnosti	činnost	k1gFnSc2	činnost
Maamy	Maama	k1gFnSc2	Maama
se	se	k3xPyFc4	se
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
skupiny	skupina	k1gFnSc2	skupina
Křečový	křečový	k2eAgInSc1d1	křečový
Žíly	žíla	k1gFnPc5	žíla
<g/>
.	.	kIx.	.
</s>
<s>
Společenství	společenství	k1gNnSc1	společenství
organizovalo	organizovat	k5eAaBmAgNnS	organizovat
podzemní	podzemní	k2eAgFnPc4d1	podzemní
koncertní	koncertní	k2eAgFnPc4d1	koncertní
akce	akce	k1gFnPc4	akce
"	"	kIx"	"
<g/>
Pod	pod	k7c7	pod
tou	ten	k3xDgFnSc7	ten
strážní	strážní	k2eAgFnSc7d1	strážní
věží	věž	k1gFnSc7	věž
<g/>
"	"	kIx"	"
nazvané	nazvaný	k2eAgInPc1d1	nazvaný
podle	podle	k7c2	podle
písně	píseň	k1gFnSc2	píseň
Boba	Bob	k1gMnSc2	Bob
Dylana	Dylan	k1gMnSc2	Dylan
<g/>
.	.	kIx.	.
</s>
<s>
Proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
jich	on	k3xPp3gMnPc2	on
asi	asi	k9	asi
deset	deset	k4xCc1	deset
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
buď	buď	k8xC	buď
v	v	k7c6	v
domě	dům	k1gInSc6	dům
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
členů	člen	k1gMnPc2	člen
KŽ	KŽ	kA	KŽ
nebo	nebo	k8xC	nebo
v	v	k7c6	v
přilehlém	přilehlý	k2eAgInSc6d1	přilehlý
lomu	lom	k1gInSc6	lom
u	u	k7c2	u
řeky	řeka	k1gFnSc2	řeka
Šlapanky	Šlapanka	k1gFnSc2	Šlapanka
<g/>
.	.	kIx.	.
</s>
<s>
Vyvrcholením	vyvrcholení	k1gNnSc7	vyvrcholení
těchto	tento	k3xDgFnPc2	tento
snah	snaha	k1gFnPc2	snaha
je	být	k5eAaImIp3nS	být
Underground	underground	k1gInSc1	underground
Festival	festival	k1gInSc1	festival
Of	Of	k1gFnPc2	Of
Music	Musice	k1gFnPc2	Musice
z	z	k7c2	z
podzimu	podzim	k1gInSc2	podzim
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yQgInSc4	který
do	do	k7c2	do
lomu	lom	k1gInSc2	lom
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
koutů	kout	k1gInPc2	kout
republiky	republika	k1gFnSc2	republika
přijelo	přijet	k5eAaPmAgNnS	přijet
okolo	okolo	k7c2	okolo
250	[number]	k4	250
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
základní	základní	k2eAgFnSc1d1	základní
vojenská	vojenský	k2eAgFnSc1d1	vojenská
služba	služba	k1gFnSc1	služba
a	a	k8xC	a
po	po	k7c6	po
příchodu	příchod	k1gInSc6	příchod
do	do	k7c2	do
civilu	civil	k1gMnSc6	civil
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
se	se	k3xPyFc4	se
Hrabalik	Hrabalik	k1gMnSc1	Hrabalik
odstěhoval	odstěhovat	k5eAaPmAgMnS	odstěhovat
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
jej	on	k3xPp3gMnSc4	on
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
punk	punk	k1gInSc1	punk
a	a	k8xC	a
hardcore	hardcor	k1gMnSc5	hardcor
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Dead	Dead	k1gInSc1	Dead
Kennedys	Kennedysa	k1gFnPc2	Kennedysa
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
víkendech	víkend	k1gInPc6	víkend
zpíval	zpívat	k5eAaImAgMnS	zpívat
s	s	k7c7	s
místní	místní	k2eAgFnSc7d1	místní
tancovačkovou	tancovačkův	k2eAgFnSc7d1	tancovačkův
kapelou	kapela	k1gFnSc7	kapela
Experiment	experiment	k1gInSc1	experiment
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
byl	být	k5eAaImAgMnS	být
nakonec	nakonec	k9	nakonec
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
vyhozen	vyhozen	k2eAgInSc1d1	vyhozen
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
spíše	spíše	k9	spíše
uvítal	uvítat	k5eAaPmAgMnS	uvítat
a	a	k8xC	a
vzápětí	vzápětí	k6eAd1	vzápětí
založil	založit	k5eAaPmAgMnS	založit
HB-hardcoreový	HBardcoreový	k2eAgInSc4d1	HB-hardcoreový
soubor	soubor	k1gInSc4	soubor
Našrot	Našrota	k1gFnPc2	Našrota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
zpívá	zpívat	k5eAaImIp3nS	zpívat
a	a	k8xC	a
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
spoluhráči	spoluhráč	k1gMnPc7	spoluhráč
jsou	být	k5eAaImIp3nP	být
Jiří	Jiří	k1gMnSc1	Jiří
"	"	kIx"	"
<g/>
Martha	Martha	k1gMnSc1	Martha
<g/>
"	"	kIx"	"
Dvořák	Dvořák	k1gMnSc1	Dvořák
(	(	kIx(	(
<g/>
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
<g/>
)	)	kIx)	)
a	a	k8xC	a
Josef	Josef	k1gMnSc1	Josef
"	"	kIx"	"
<g/>
Jouza	Jouza	k1gFnSc1	Jouza
<g/>
"	"	kIx"	"
Bárta	Bárta	k1gMnSc1	Bárta
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgMnSc1d1	bicí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
přišel	přijít	k5eAaPmAgMnS	přijít
ještě	ještě	k9	ještě
Tomáš	Tomáš	k1gMnSc1	Tomáš
"	"	kIx"	"
<g/>
Ceemek	Ceemek	k1gMnSc1	Ceemek
<g/>
"	"	kIx"	"
Hájek	Hájek	k1gMnSc1	Hájek
(	(	kIx(	(
<g/>
sólová	sólový	k2eAgFnSc1d1	sólová
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hrabošův	hrabošův	k2eAgInSc1d1	hrabošův
hudební	hudební	k2eAgInSc1d1	hudební
život	život	k1gInSc1	život
je	být	k5eAaImIp3nS	být
spjat	spjat	k2eAgMnSc1d1	spjat
se	s	k7c7	s
souborem	soubor	k1gInSc7	soubor
Našrot	Našrot	k1gMnSc1	Našrot
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
doby	doba	k1gFnSc2	doba
vydal	vydat	k5eAaPmAgMnS	vydat
sedm	sedm	k4xCc4	sedm
řadových	řadový	k2eAgNnPc2d1	řadové
alb	album	k1gNnPc2	album
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
se	se	k3xPyFc4	se
spojili	spojit	k5eAaPmAgMnP	spojit
členové	člen	k1gMnPc1	člen
Našrotu	Našrot	k1gInSc2	Našrot
(	(	kIx(	(
<g/>
Hraboš	hraboš	k1gMnSc1	hraboš
<g/>
,	,	kIx,	,
Martha	Martha	k1gMnSc1	Martha
<g/>
,	,	kIx,	,
Jouza	Jouza	k1gFnSc1	Jouza
<g/>
)	)	kIx)	)
s	s	k7c7	s
hráčem	hráč	k1gMnSc7	hráč
na	na	k7c4	na
foukací	foukací	k2eAgFnSc4d1	foukací
harmoniku	harmonika	k1gFnSc4	harmonika
Jerrym	Jerrym	k1gInSc4	Jerrym
a	a	k8xC	a
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
pub	pub	k?	pub
rockovou	rockový	k2eAgFnSc4d1	rocková
skupinu	skupina	k1gFnSc4	skupina
Jerry	Jerra	k1gFnSc2	Jerra
&	&	k?	&
The	The	k1gFnSc2	The
Alligators	Alligatorsa	k1gFnPc2	Alligatorsa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vydržela	vydržet	k5eAaPmAgFnS	vydržet
cca	cca	kA	cca
1	[number]	k4	1
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
1998-99	[number]	k4	1998-99
Hraboš	hraboš	k1gMnSc1	hraboš
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
členové	člen	k1gMnPc1	člen
Našrot	Našrota	k1gFnPc2	Našrota
společně	společně	k6eAd1	společně
s	s	k7c7	s
bubeníkem	bubeník	k1gMnSc7	bubeník
Jiřím	Jiří	k1gMnSc7	Jiří
"	"	kIx"	"
<g/>
Komínem	Komín	k1gInSc7	Komín
<g/>
"	"	kIx"	"
Komínkem	Komínek	k1gMnSc7	Komínek
natočili	natočit	k5eAaBmAgMnP	natočit
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Hraboš	hraboš	k1gMnSc1	hraboš
&	&	k?	&
Friends	Friends	k1gInSc1	Friends
4	[number]	k4	4
rockové	rockový	k2eAgFnPc1d1	rocková
písničky	písnička	k1gFnPc1	písnička
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
se	se	k3xPyFc4	se
dali	dát	k5eAaPmAgMnP	dát
dohromady	dohromady	k6eAd1	dohromady
tři	tři	k4xCgMnPc1	tři
kamarádi	kamarád	k1gMnPc1	kamarád
z	z	k7c2	z
kapel	kapela	k1gFnPc2	kapela
Našrot	Našrota	k1gFnPc2	Našrota
<g/>
,	,	kIx,	,
Plexis	Plexis	k1gFnPc2	Plexis
a	a	k8xC	a
Michael	Michael	k1gMnSc1	Michael
<g/>
́	́	k?	́
<g/>
s	s	k7c7	s
Uncle	Uncle	k1gNnSc7	Uncle
<g/>
,	,	kIx,	,
jmenovitě	jmenovitě	k6eAd1	jmenovitě
MC	MC	kA	MC
"	"	kIx"	"
<g/>
El	Ela	k1gFnPc2	Ela
Hraboš	hraboš	k1gMnSc1	hraboš
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
zpěv	zpěv	k1gInSc1	zpěv
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
"	"	kIx"	"
<g/>
Mucho	Mucha	k1gMnSc5	Mucha
<g/>
"	"	kIx"	"
Hošek	Hošek	k1gMnSc1	Hošek
(	(	kIx(	(
<g/>
dj-ing	djng	k1gInSc1	dj-ing
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
"	"	kIx"	"
<g/>
Seňor	seňor	k1gMnSc1	seňor
<g/>
"	"	kIx"	"
Stuchlý	Stuchlý	k2eAgMnSc1d1	Stuchlý
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgMnSc1d1	bicí
<g/>
)	)	kIx)	)
a	a	k8xC	a
založili	založit	k5eAaPmAgMnP	založit
improvizační	improvizační	k2eAgNnSc4d1	improvizační
těleso	těleso	k1gNnSc4	těleso
Maradona	Maradon	k1gMnSc2	Maradon
Jazz	jazz	k1gInSc4	jazz
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
zaobírá	zaobírat	k5eAaImIp3nS	zaobírat
(	(	kIx(	(
<g/>
většinou	většina	k1gFnSc7	většina
<g/>
)	)	kIx)	)
latino-americkou	latinomerický	k2eAgFnSc7d1	latino-americký
hudbou	hudba	k1gFnSc7	hudba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
se	se	k3xPyFc4	se
k	k	k7c3	k
tělesu	těleso	k1gNnSc3	těleso
připojili	připojit	k5eAaPmAgMnP	připojit
ještě	ještě	k9	ještě
DJ	DJ	kA	DJ
Baltazar	Baltazar	k1gMnSc1	Baltazar
(	(	kIx(	(
<g/>
vl	vl	k?	vl
<g/>
.	.	kIx.	.
jménem	jméno	k1gNnSc7	jméno
Pavel	Pavel	k1gMnSc1	Pavel
Melichařík	Melichařík	k1gMnSc1	Melichařík
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
čas	čas	k1gInSc4	čas
také	také	k9	také
Šimon	Šimon	k1gMnSc1	Šimon
"	"	kIx"	"
<g/>
Šimonito	Šimonit	k2eAgNnSc1d1	Šimonit
<g/>
"	"	kIx"	"
Ornest	Ornest	k1gFnSc1	Ornest
(	(	kIx(	(
<g/>
saxofon	saxofon	k1gInSc1	saxofon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
veselé	veselý	k2eAgNnSc1d1	veselé
uskupení	uskupení	k1gNnSc1	uskupení
působí	působit	k5eAaImIp3nS	působit
dodnes	dodnes	k6eAd1	dodnes
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
oblíbenou	oblíbený	k2eAgFnSc7d1	oblíbená
položkou	položka	k1gFnSc7	položka
různých	různý	k2eAgInPc2d1	různý
tanečních	taneční	k2eAgInPc2d1	taneční
mejdanů	mejdan	k1gInPc2	mejdan
a	a	k8xC	a
parties	partiesa	k1gFnPc2	partiesa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
2005-2006	[number]	k4	2005-2006
působil	působit	k5eAaImAgMnS	působit
Hraboš	hraboš	k1gMnSc1	hraboš
v	v	k7c6	v
další	další	k2eAgFnSc6d1	další
HC-crossoverové	HCrossoverový	k2eAgFnSc6d1	HC-crossoverový
skupině	skupina	k1gFnSc6	skupina
Ström	Ström	k1gInSc4	Ström
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
sešli	sejít	k5eAaPmAgMnP	sejít
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Stuchlý	Stuchlý	k2eAgMnSc1d1	Stuchlý
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgInSc1d1	bicí
<g/>
,	,	kIx,	,
ex-Michael	ex-Michaet	k5eAaImAgInS	ex-Michaet
<g/>
́	́	k?	́
<g/>
s	s	k7c7	s
Uncle	Uncle	k1gNnSc7	Uncle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Radek	Radek	k1gMnSc1	Radek
"	"	kIx"	"
<g/>
Hrádek	hrádek	k1gInSc1	hrádek
<g/>
"	"	kIx"	"
Albl	Albl	k1gInSc1	Albl
(	(	kIx(	(
<g/>
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
ex-Slut	ex-Slut	k1gInSc1	ex-Slut
<g/>
)	)	kIx)	)
a	a	k8xC	a
Filip	Filip	k1gMnSc1	Filip
Kolaci	kolace	k1gFnSc4	kolace
(	(	kIx(	(
<g/>
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
ex-Plexis	ex-Plexis	k1gFnSc1	ex-Plexis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Hrabalik	Hrabalik	k1gMnSc1	Hrabalik
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaImIp3nS	věnovat
také	také	k9	také
psaní	psaní	k1gNnSc4	psaní
prózy	próza	k1gFnSc2	próza
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
též	též	k9	též
jako	jako	k9	jako
rockový	rockový	k2eAgMnSc1d1	rockový
historik	historik	k1gMnSc1	historik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1995-2000	[number]	k4	1995-2000
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgInS	podílet
na	na	k7c6	na
seriálu	seriál	k1gInSc6	seriál
"	"	kIx"	"
<g/>
Bigbít	bigbít	k1gInSc1	bigbít
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
42	[number]	k4	42
<g/>
-dílném	ílný	k2eAgInSc6d1	-dílný
dokumentu	dokument	k1gInSc6	dokument
ČT	ČT	kA	ČT
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
pražského	pražský	k2eAgInSc2d1	pražský
Popmusea	Popmuse	k1gInSc2	Popmuse
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
vytvářel	vytvářet	k5eAaImAgMnS	vytvářet
na	na	k7c6	na
webu	web	k1gInSc6	web
ČT	ČT	kA	ČT
stránky	stránka	k1gFnSc2	stránka
seriálu	seriál	k1gInSc2	seriál
"	"	kIx"	"
<g/>
Bigbít	bigbít	k1gInSc1	bigbít
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
-	-	kIx~	-
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
-	-	kIx~	-
včetně	včetně	k7c2	včetně
rozsáhlého	rozsáhlý	k2eAgInSc2d1	rozsáhlý
přehledu	přehled	k1gInSc2	přehled
světové	světový	k2eAgFnSc2d1	světová
rockové	rockový	k2eAgFnSc2d1	rocková
hudby	hudba	k1gFnSc2	hudba
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2012-2014	[number]	k4	2012-2014
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
na	na	k7c6	na
výrobě	výroba	k1gFnSc6	výroba
seriálu	seriál	k1gInSc2	seriál
"	"	kIx"	"
<g/>
Fenomén	fenomén	k1gInSc1	fenomén
Underground	underground	k1gInSc1	underground
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
39	[number]	k4	39
<g/>
-dílném	ílný	k2eAgInSc6d1	-dílný
dokumentu	dokument	k1gInSc6	dokument
ČT	ČT	kA	ČT
<g/>
,	,	kIx,	,
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
i	i	k9	i
jeho	jeho	k3xOp3gFnPc4	jeho
webové	webový	k2eAgFnPc4d1	webová
stránky	stránka	k1gFnPc4	stránka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
vydal	vydat	k5eAaPmAgMnS	vydat
své	svůj	k3xOyFgNnSc4	svůj
sólové	sólový	k2eAgNnSc4d1	sólové
album	album	k1gNnSc4	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
Songy	song	k1gInPc4	song
z	z	k7c2	z
bědných	bědný	k2eAgInPc2d1	bědný
časů	čas	k1gInPc2	čas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Člen	člen	k1gMnSc1	člen
skupin	skupina	k1gFnPc2	skupina
==	==	k?	==
</s>
</p>
<p>
<s>
Oxygen	oxygen	k1gInSc1	oxygen
(	(	kIx(	(
<g/>
rock	rock	k1gInSc1	rock
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
<g/>
-	-	kIx~	-
<g/>
82	[number]	k4	82
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Maama	Maama	k1gFnSc1	Maama
(	(	kIx(	(
<g/>
alternativa	alternativa	k1gFnSc1	alternativa
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
-	-	kIx~	-
<g/>
84	[number]	k4	84
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hayaya	Hayaya	k1gFnSc1	Hayaya
Band	banda	k1gFnPc2	banda
(	(	kIx(	(
<g/>
hard-rock	hardock	k1gMnSc1	hard-rock
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
-	-	kIx~	-
<g/>
83	[number]	k4	83
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
H-Band	H-Band	k1gInSc1	H-Band
(	(	kIx(	(
<g/>
hard	hard	k1gInSc1	hard
rock	rock	k1gInSc1	rock
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
-	-	kIx~	-
<g/>
84	[number]	k4	84
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Křečový	křečový	k2eAgMnSc1d1	křečový
Žíly	žíla	k1gFnPc4	žíla
(	(	kIx(	(
<g/>
undergroundový	undergroundový	k2eAgMnSc1d1	undergroundový
freak-out	freakut	k1gMnSc1	freak-out
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
-	-	kIx~	-
<g/>
86	[number]	k4	86
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sfinx	sfinx	k1gFnSc1	sfinx
(	(	kIx(	(
<g/>
hard	hard	k1gInSc1	hard
rock	rock	k1gInSc1	rock
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Experiment	experiment	k1gInSc1	experiment
(	(	kIx(	(
<g/>
rock	rock	k1gInSc1	rock
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Našrot	Našrot	k1gMnSc1	Našrot
(	(	kIx(	(
<g/>
HC-crossover	HCrossover	k1gMnSc1	HC-crossover
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
–	–	k?	–
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jerry	Jerra	k1gFnPc1	Jerra
&	&	k?	&
The	The	k1gFnSc1	The
Alligators	Alligators	k1gInSc1	Alligators
(	(	kIx(	(
<g/>
pub-rock	pubock	k1gInSc1	pub-rock
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
-	-	kIx~	-
<g/>
91	[number]	k4	91
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Maradona	Maradona	k1gFnSc1	Maradona
Jazz	jazz	k1gInSc1	jazz
(	(	kIx(	(
<g/>
latino	latina	k1gFnSc5	latina
<g/>
,	,	kIx,	,
jazz	jazz	k1gInSc1	jazz
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
–	–	k?	–
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ström	Ström	k1gMnSc1	Ström
(	(	kIx(	(
<g/>
HC-crossover	HCrossover	k1gMnSc1	HC-crossover
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Důležité	důležitý	k2eAgFnPc1d1	důležitá
nahrávky	nahrávka	k1gFnPc1	nahrávka
a	a	k8xC	a
desky	deska	k1gFnPc1	deska
==	==	k?	==
</s>
</p>
<p>
<s>
Křečový	křečový	k2eAgMnSc1d1	křečový
Žíly	žíla	k1gFnPc4	žíla
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Live	Live	k1gFnSc1	Live
In	In	k1gMnSc1	In
Baráček	baráček	k1gInSc1	baráček
(	(	kIx(	(
<g/>
Cirhosa	Cirhosa	k1gFnSc1	Cirhosa
jater	játra	k1gNnPc2	játra
aneb	aneb	k?	aneb
živě	živě	k6eAd1	živě
v	v	k7c6	v
opilosti	opilost	k1gFnSc6	opilost
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
neoficiální	neoficiální	k2eAgNnSc1d1	neoficiální
demo	demo	k2eAgNnSc3d1	demo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Našrot	Našrot	k1gInSc1	Našrot
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Totálně	totálně	k6eAd1	totálně
našrot	našrot	k1gInSc1	našrot
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
neoficiální	oficiální	k2eNgFnSc1d1	neoficiální
demo	demo	k2eAgFnSc1d1	demo
<g/>
,	,	kIx,	,
reedice	reedice	k1gFnSc1	reedice
na	na	k7c4	na
LP	LP	kA	LP
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Punk	punk	k1gInSc1	punk
<g/>
́	́	k?	́
<g/>
n	n	k0	n
<g/>
́	́	k?	́
<g/>
oi	oi	k?	oi
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
LP	LP	kA	LP
<g/>
,	,	kIx,	,
MC	MC	kA	MC
<g/>
,	,	kIx,	,
kompilace	kompilace	k1gFnPc1	kompilace
čs	čs	kA	čs
<g/>
.	.	kIx.	.
Punk	punk	k1gMnSc1	punk
a	a	k8xC	a
HC	HC	kA	HC
skupin	skupina	k1gFnPc2	skupina
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Našrot	Našrot	k1gInSc1	Našrot
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Destructive	Destructiv	k1gInSc5	Destructiv
Tour	Tour	k1gInSc4	Tour
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
LP	LP	kA	LP
<g/>
,	,	kIx,	,
CD	CD	kA	CD
<g/>
,	,	kIx,	,
MC	MC	kA	MC
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Našrot	Našrot	k1gInSc1	Našrot
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Brain	Brain	k2eAgInSc1d1	Brain
Investigator	Investigator	k1gInSc1	Investigator
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
CD	CD	kA	CD
<g/>
,	,	kIx,	,
MC	MC	kA	MC
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Našrot	Našrot	k1gInSc1	Našrot
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Cornered	Cornered	k1gMnSc1	Cornered
Animal	animal	k1gMnSc1	animal
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
CD	CD	kA	CD
<g/>
,	,	kIx,	,
MC	MC	kA	MC
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Našrot	Našrot	k1gInSc1	Našrot
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
The	The	k1gMnSc5	The
Guide	Guid	k1gMnSc5	Guid
To	ten	k3xDgNnSc4	ten
The	The	k1gFnPc1	The
Wild	Wilda	k1gFnPc2	Wilda
Years	Yearsa	k1gFnPc2	Yearsa
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
CD	CD	kA	CD
<g/>
,	,	kIx,	,
MC	MC	kA	MC
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Našrot	Našrot	k1gInSc1	Našrot
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Mirror	Mirror	k1gMnSc1	Mirror
&	&	k?	&
The	The	k1gMnSc1	The
Mask	Mask	k1gMnSc1	Mask
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
CD	CD	kA	CD
<g/>
,	,	kIx,	,
MC	MC	kA	MC
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Našrot	Našrot	k1gInSc1	Našrot
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Psychorama	Psychorama	k1gFnSc1	Psychorama
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
CD	CD	kA	CD
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Příběhy	příběh	k1gInPc1	příběh
obyčejného	obyčejný	k2eAgNnSc2d1	obyčejné
šílenství	šílenství	k1gNnSc2	šílenství
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
CD	CD	kA	CD
<g/>
,	,	kIx,	,
film	film	k1gInSc1	film
soundtrack	soundtrack	k1gInSc1	soundtrack
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Našrot	Našrot	k1gInSc1	Našrot
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Rag	Rag	k1gFnSc1	Rag
<g/>
(	(	kIx(	(
<g/>
e	e	k0	e
<g/>
)	)	kIx)	)
<g/>
time	tim	k1gInPc4	tim
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
CD	CD	kA	CD
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Našrot	Našrot	k1gInSc1	Našrot
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Live	Live	k1gFnSc1	Live
In	In	k1gFnSc1	In
Praha	Praha	k1gFnSc1	Praha
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
DVD	DVD	kA	DVD
<g/>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
r.	r.	kA	r.
2011	[number]	k4	2011
jako	jako	k8xC	jako
příloha	příloha	k1gFnSc1	příloha
knihy	kniha	k1gFnSc2	kniha
"	"	kIx"	"
<g/>
Totálně	totálně	k6eAd1	totálně
Našrot	Našrot	k1gInSc1	Našrot
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Našrot	Našrot	k1gInSc1	Našrot
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Unplugged	Unplugged	k1gInSc1	Unplugged
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
CD	CD	kA	CD
<g/>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
jako	jako	k9	jako
příloha	příloha	k1gFnSc1	příloha
knihy	kniha	k1gFnSc2	kniha
"	"	kIx"	"
<g/>
Totálně	totálně	k6eAd1	totálně
Našrot	Našrot	k1gInSc1	Našrot
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hraboš	hraboš	k1gMnSc1	hraboš
&	&	k?	&
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Songy	song	k1gInPc7	song
z	z	k7c2	z
bědných	bědný	k2eAgInPc2d1	bědný
časů	čas	k1gInPc2	čas
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
CD	CD	kA	CD
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Našrot	Našrot	k1gInSc1	Našrot
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Flashbacks	Flashbacks	k1gInSc1	Flashbacks
-	-	kIx~	-
Live	Live	k1gFnSc1	Live
Unplugged	Unplugged	k1gMnSc1	Unplugged
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
CD	CD	kA	CD
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Našrot	Našrot	k1gInSc1	Našrot
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Best	Best	k1gMnSc1	Best
Of	Of	k1gMnSc1	Of
Našrot	Našrot	k1gMnSc1	Našrot
-	-	kIx~	-
Back	Back	k1gMnSc1	Back
To	ten	k3xDgNnSc4	ten
The	The	k1gFnSc4	The
Past	past	k1gFnSc4	past
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
CD	CD	kA	CD
<g/>
,	,	kIx,	,
kompilace	kompilace	k1gFnSc1	kompilace
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Sbírky	sbírka	k1gFnPc4	sbírka
a	a	k8xC	a
knihy	kniha	k1gFnPc4	kniha
==	==	k?	==
</s>
</p>
<p>
<s>
Bujaré	bujarý	k2eAgInPc1d1	bujarý
výlety	výlet	k1gInPc1	výlet
perverzní	perverzní	k2eAgFnSc2d1	perverzní
pomazánky	pomazánka	k1gFnSc2	pomazánka
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
texty	text	k1gInPc4	text
a	a	k8xC	a
básně	báseň	k1gFnPc4	báseň
z	z	k7c2	z
80	[number]	k4	80
<g/>
.	.	kIx.	.
<g/>
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pokusné	pokusný	k2eAgInPc1d1	pokusný
stavy	stav	k1gInPc1	stav
šílenství	šílenství	k1gNnSc2	šílenství
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
báseň	báseň	k1gFnSc1	báseň
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Expedice	expedice	k1gFnSc1	expedice
Paris	Paris	k1gMnSc1	Paris
́	́	k?	́
<g/>
90	[number]	k4	90
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
ulítlý	ulítlý	k2eAgInSc1d1	ulítlý
mini-cestopis	miniestopis	k1gInSc1	mini-cestopis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Intim	Intim	k1gInSc1	Intim
spray	spraa	k1gFnSc2	spraa
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
texty	text	k1gInPc4	text
a	a	k8xC	a
básně	báseň	k1gFnPc4	báseň
z	z	k7c2	z
80	[number]	k4	80
<g/>
.	.	kIx.	.
<g/>
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Viděno	vidět	k5eAaImNgNnS	vidět
Sudem	sud	k1gInSc7	sud
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
humorná	humorný	k2eAgFnSc1d1	humorná
próza	próza	k1gFnSc1	próza
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mimo	mimo	k7c4	mimo
Sud	sud	k1gInSc4	sud
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
humorná	humorný	k2eAgFnSc1d1	humorná
próza	próza	k1gFnSc1	próza
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sběrné	sběrný	k2eAgFnPc1d1	sběrná
Sudoviny	sudovina	k1gFnPc1	sudovina
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
mikro-úlety	mikro-úleta	k1gFnPc1	mikro-úleta
a	a	k8xC	a
povídky	povídka	k1gFnPc1	povídka
<g/>
,	,	kIx,	,
humorná	humorný	k2eAgFnSc1d1	humorná
próza	próza	k1gFnSc1	próza
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Návrat	návrat	k1gInSc1	návrat
k	k	k7c3	k
Sudu	sud	k1gInSc3	sud
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
humorná	humorný	k2eAgFnSc1d1	humorná
próza	próza	k1gFnSc1	próza
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Perštýnská	Perštýnský	k2eAgFnSc1d1	Perštýnská
vražda	vražda	k1gFnSc1	vražda
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
povídky	povídka	k1gFnPc1	povídka
z	z	k7c2	z
přelomu	přelom	k1gInSc2	přelom
80	[number]	k4	80
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
90	[number]	k4	90
<g/>
.	.	kIx.	.
<g/>
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Viděno	viděn	k2eAgNnSc1d1	viděno
dvojmo	dvojmo	k6eAd1	dvojmo
(	(	kIx(	(
<g/>
společně	společně	k6eAd1	společně
s	s	k7c7	s
R.	R.	kA	R.
<g/>
Diestlerem	Diestler	k1gMnSc7	Diestler
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
humorná	humorný	k2eAgFnSc1d1	humorná
próza	próza	k1gFnSc1	próza
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Viděno	viděn	k2eAgNnSc1d1	viděno
dvojmo	dvojmo	k6eAd1	dvojmo
2	[number]	k4	2
(	(	kIx(	(
<g/>
společně	společně	k6eAd1	společně
s	s	k7c7	s
R.	R.	kA	R.
<g/>
Diestlerem	Diestler	k1gMnSc7	Diestler
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
humorná	humorný	k2eAgFnSc1d1	humorná
próza	próza	k1gFnSc1	próza
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Viděno	viděn	k2eAgNnSc1d1	viděno
dvojmo	dvojmo	k6eAd1	dvojmo
3	[number]	k4	3
(	(	kIx(	(
<g/>
společně	společně	k6eAd1	společně
s	s	k7c7	s
R.	R.	kA	R.
<g/>
Diestlerem	Diestler	k1gMnSc7	Diestler
<g/>
,	,	kIx,	,
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
humorná	humorný	k2eAgFnSc1d1	humorná
próza	próza	k1gFnSc1	próza
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Práce	práce	k1gFnPc1	práce
pro	pro	k7c4	pro
Českou	český	k2eAgFnSc4d1	Česká
televizi	televize	k1gFnSc4	televize
==	==	k?	==
</s>
</p>
<p>
<s>
mj.	mj.	kA	mj.
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Bigbít	bigbít	k1gInSc4	bigbít
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
-	-	kIx~	-
<g/>
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
42	[number]	k4	42
<g/>
-dílný	ílný	k2eAgInSc1d1	-dílný
dokument	dokument	k1gInSc1	dokument
o	o	k7c6	o
historii	historie	k1gFnSc6	historie
československého	československý	k2eAgInSc2d1	československý
rocku	rock	k1gInSc2	rock
za	za	k7c2	za
totality	totalita	k1gFnSc2	totalita
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Bigbít	bigbít	k1gInSc4	bigbít
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
webové	webový	k2eAgFnPc4d1	webová
stránky	stránka	k1gFnPc4	stránka
ČT	ČT	kA	ČT
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Největší	veliký	k2eAgMnSc1d3	veliký
Čech	Čech	k1gMnSc1	Čech
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Fenomén	fenomén	k1gInSc1	fenomén
underground	underground	k1gInSc1	underground
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
2014,40	[number]	k4	2014,40
<g/>
-dílný	ílný	k2eAgInSc1d1	-dílný
dokument	dokument	k1gInSc1	dokument
o	o	k7c6	o
historii	historie	k1gFnSc6	historie
tuzemského	tuzemský	k2eAgInSc2d1	tuzemský
undergroundu	underground	k1gInSc2	underground
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Fenomén	fenomén	k1gInSc1	fenomén
underground	underground	k1gInSc1	underground
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
webové	webový	k2eAgFnPc4d1	webová
stránky	stránka	k1gFnPc4	stránka
ČT	ČT	kA	ČT
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Bigbít	bigbít	k1gInSc1	bigbít
<g/>
"	"	kIx"	"
ČT	ČT	kA	ČT
-	-	kIx~	-
FB	FB	kA	FB
(	(	kIx(	(
<g/>
od	od	k7c2	od
r.	r.	kA	r.
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Našrot	Našrot	k1gMnSc1	Našrot
</s>
</p>
<p>
<s>
Hraboš	hraboš	k1gMnSc1	hraboš
</s>
</p>
