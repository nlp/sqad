<p>
<s>
Viola	Viola	k1gFnSc1	Viola
(	(	kIx(	(
<g/>
zastarale	zastarale	k6eAd1	zastarale
nebo	nebo	k8xC	nebo
zřídkavě	zřídkavě	k6eAd1	zřídkavě
bráč	bráč	k1gInSc1	bráč
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
strunný	strunný	k2eAgInSc1d1	strunný
smyčcový	smyčcový	k2eAgInSc1d1	smyčcový
hudební	hudební	k2eAgInSc1d1	hudební
nástroj	nástroj	k1gInSc1	nástroj
se	s	k7c7	s
čtyřmi	čtyři	k4xCgFnPc7	čtyři
strunami	struna	k1gFnPc7	struna
laděnými	laděný	k2eAgFnPc7d1	laděná
v	v	k7c6	v
čistých	čistý	k2eAgFnPc6d1	čistá
kvintách	kvinta	k1gFnPc6	kvinta
<g/>
:	:	kIx,	:
c	c	k0	c
<g/>
,	,	kIx,	,
g	g	kA	g
<g/>
,	,	kIx,	,
d1	d1	k4	d1
<g/>
,	,	kIx,	,
a1	a1	k4	a1
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
orchestru	orchestr	k1gInSc6	orchestr
a	a	k8xC	a
smyčcových	smyčcový	k2eAgInPc6d1	smyčcový
souborech	soubor	k1gInPc6	soubor
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
střední	střední	k2eAgInSc4d1	střední
altový	altový	k2eAgInSc4d1	altový
hlas	hlas	k1gInSc4	hlas
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vyšší	vysoký	k2eAgInPc1d2	vyšší
hlasy	hlas	k1gInPc1	hlas
hrají	hrát	k5eAaImIp3nP	hrát
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
nižší	nízký	k2eAgNnPc1d2	nižší
violoncella	violoncello	k1gNnPc1	violoncello
a	a	k8xC	a
kontrabasy	kontrabas	k1gInPc1	kontrabas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
==	==	k?	==
</s>
</p>
<p>
<s>
Viola	Viola	k1gFnSc1	Viola
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
podobnou	podobný	k2eAgFnSc4d1	podobná
stavbu	stavba	k1gFnSc4	stavba
jako	jako	k8xC	jako
housle	housle	k1gFnPc4	housle
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
o	o	k7c4	o
málo	málo	k6eAd1	málo
větší	veliký	k2eAgFnSc4d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc1	její
struny	struna	k1gFnPc1	struna
jsou	být	k5eAaImIp3nP	být
laděné	laděný	k2eAgInPc1d1	laděný
o	o	k7c4	o
kvintu	kvinta	k1gFnSc4	kvinta
níže	nízce	k6eAd2	nízce
než	než	k8xS	než
houslové	houslový	k2eAgFnPc1d1	houslová
<g/>
.	.	kIx.	.
</s>
<s>
Zvuk	zvuk	k1gInSc1	zvuk
violy	viola	k1gFnSc2	viola
je	být	k5eAaImIp3nS	být
plný	plný	k2eAgInSc1d1	plný
a	a	k8xC	a
poněkud	poněkud	k6eAd1	poněkud
temný	temný	k2eAgInSc1d1	temný
a	a	k8xC	a
melancholický	melancholický	k2eAgInSc1d1	melancholický
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
struně	struna	k1gFnSc6	struna
a	a	k8xC	a
svým	svůj	k3xOyFgInSc7	svůj
tónem	tón	k1gInSc7	tón
částečně	částečně	k6eAd1	částečně
připomíná	připomínat	k5eAaImIp3nS	připomínat
housle	housle	k1gFnPc4	housle
<g/>
,	,	kIx,	,
chybí	chybět	k5eAaImIp3nS	chybět
jí	on	k3xPp3gFnSc3	on
ale	ale	k9	ale
jejich	jejich	k3xOp3gFnSc4	jejich
světlost	světlost	k1gFnSc4	světlost
a	a	k8xC	a
brilantnost	brilantnost	k1gFnSc4	brilantnost
<g/>
,	,	kIx,	,
zvukem	zvuk	k1gInSc7	zvuk
tedy	tedy	k9	tedy
spíše	spíše	k9	spíše
připomíná	připomínat	k5eAaImIp3nS	připomínat
hoboj	hoboj	k1gInSc1	hoboj
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
hrou	hra	k1gFnSc7	hra
na	na	k7c6	na
nejnižší	nízký	k2eAgFnSc6d3	nejnižší
struně	struna	k1gFnSc6	struna
c	c	k0	c
(	(	kIx(	(
<g/>
zvláště	zvláště	k6eAd1	zvláště
ve	v	k7c6	v
forte	forte	k1gNnSc6	forte
<g/>
)	)	kIx)	)
vydává	vydávat	k5eAaImIp3nS	vydávat
viola	viola	k1gFnSc1	viola
divoký	divoký	k2eAgInSc4d1	divoký
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
výhrůžný	výhrůžný	k2eAgInSc4d1	výhrůžný
zvuk	zvuk	k1gInSc4	zvuk
<g/>
,	,	kIx,	,
čehož	což	k3yRnSc2	což
se	se	k3xPyFc4	se
často	často	k6eAd1	často
využívá	využívat	k5eAaImIp3nS	využívat
například	například	k6eAd1	například
ve	v	k7c6	v
filmové	filmový	k2eAgFnSc6d1	filmová
hudbě	hudba	k1gFnSc6	hudba
nebo	nebo	k8xC	nebo
operách	opera	k1gFnPc6	opera
<g/>
.	.	kIx.	.
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
barvy	barva	k1gFnSc2	barva
tónu	tón	k1gInSc2	tón
violy	viola	k1gFnSc2	viola
mají	mít	k5eAaImIp3nP	mít
souvislost	souvislost	k1gFnSc4	souvislost
s	s	k7c7	s
její	její	k3xOp3gFnSc7	její
velikostí	velikost	k1gFnSc7	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
laděna	ladit	k5eAaImNgFnS	ladit
o	o	k7c4	o
kvintu	kvinta	k1gFnSc4	kvinta
níže	nízce	k6eAd2	nízce
než	než	k8xS	než
housle	housle	k1gFnPc4	housle
(	(	kIx(	(
<g/>
čili	čili	k8xC	čili
poměr	poměr	k1gInSc1	poměr
příslušných	příslušný	k2eAgFnPc2d1	příslušná
frekvencí	frekvence	k1gFnPc2	frekvence
violových	violový	k2eAgFnPc2d1	violová
a	a	k8xC	a
houslových	houslový	k2eAgFnPc2d1	houslová
strun	struna	k1gFnPc2	struna
je	být	k5eAaImIp3nS	být
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
měla	mít	k5eAaImAgFnS	mít
stejný	stejný	k2eAgInSc4d1	stejný
zvuk	zvuk	k1gInSc4	zvuk
jako	jako	k8xC	jako
housle	housle	k1gFnPc4	housle
<g/>
,	,	kIx,	,
musela	muset	k5eAaImAgFnS	muset
by	by	kYmCp3nS	by
být	být	k5eAaImF	být
o	o	k7c4	o
stejný	stejný	k2eAgInSc4d1	stejný
poměr	poměr	k1gInSc4	poměr
větší	veliký	k2eAgFnSc2d2	veliký
–	–	k?	–
při	při	k7c6	při
délce	délka	k1gFnSc6	délka
trupu	trup	k1gInSc2	trup
houslí	houslit	k5eAaImIp3nS	houslit
36	[number]	k4	36
cm	cm	kA	cm
by	by	kYmCp3nS	by
tedy	tedy	k9	tedy
musela	muset	k5eAaImAgFnS	muset
mít	mít	k5eAaImF	mít
trup	trup	k1gInSc4	trup
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
cca	cca	kA	cca
54	[number]	k4	54
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
viola	viola	k1gFnSc1	viola
menší	malý	k2eAgFnSc1d2	menší
<g/>
,	,	kIx,	,
než	než	k8xS	než
by	by	kYmCp3nS	by
z	z	k7c2	z
akustického	akustický	k2eAgNnSc2d1	akustické
hlediska	hledisko	k1gNnSc2	hledisko
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
<g/>
,	,	kIx,	,
vyšší	vysoký	k2eAgFnPc1d2	vyšší
harmonické	harmonický	k2eAgFnPc1d1	harmonická
frekvence	frekvence	k1gFnPc1	frekvence
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
ní	on	k3xPp3gFnSc2	on
utlumeny	utlumen	k2eAgInPc4d1	utlumen
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
důvodem	důvod	k1gInSc7	důvod
pro	pro	k7c4	pro
jiné	jiný	k2eAgFnPc4d1	jiná
tónové	tónový	k2eAgFnPc4d1	tónová
charakteristiky	charakteristika	k1gFnPc4	charakteristika
<g/>
,	,	kIx,	,
než	než	k8xS	než
mají	mít	k5eAaImIp3nP	mít
housle	housle	k1gFnPc4	housle
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Trup	trup	k1gInSc1	trup
violy	viola	k1gFnSc2	viola
měří	měřit	k5eAaImIp3nS	měřit
nejčastěji	často	k6eAd3	často
42	[number]	k4	42
cm	cm	kA	cm
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
člověk	člověk	k1gMnSc1	člověk
průměrné	průměrný	k2eAgFnSc2d1	průměrná
velikosti	velikost	k1gFnSc2	velikost
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
potíže	potíž	k1gFnPc4	potíž
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
větší	veliký	k2eAgInSc4d2	veliký
nástroj	nástroj	k1gInSc4	nástroj
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
jen	jen	k9	jen
s	s	k7c7	s
obtížemi	obtíž	k1gFnPc7	obtíž
by	by	k9	by
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
na	na	k7c4	na
začátek	začátek	k1gInSc4	začátek
hmatníku	hmatník	k1gInSc2	hmatník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
se	se	k3xPyFc4	se
i	i	k9	i
menší	malý	k2eAgFnPc1d2	menší
varianty	varianta	k1gFnPc1	varianta
viol	viola	k1gFnPc2	viola
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
ale	ale	k8xC	ale
mají	mít	k5eAaImIp3nP	mít
nižší	nízký	k2eAgFnSc4d2	nižší
kvalitu	kvalita	k1gFnSc4	kvalita
zvuku	zvuk	k1gInSc2	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Violový	violový	k2eAgInSc1d1	violový
smyčec	smyčec	k1gInSc1	smyčec
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
kratší	krátký	k2eAgInSc1d2	kratší
a	a	k8xC	a
mohutnější	mohutný	k2eAgInSc1d2	mohutnější
než	než	k8xS	než
houslový	houslový	k2eAgInSc1d1	houslový
<g/>
.	.	kIx.	.
</s>
<s>
Smyčcové	smyčcový	k2eAgFnPc1d1	smyčcová
techniky	technika	k1gFnPc1	technika
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
totožné	totožný	k2eAgInPc1d1	totožný
s	s	k7c7	s
houslovými	houslový	k2eAgInPc7d1	houslový
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
rozezvučení	rozezvučení	k1gNnSc4	rozezvučení
strun	struna	k1gFnPc2	struna
je	být	k5eAaImIp3nS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
větší	veliký	k2eAgFnPc4d2	veliký
síly	síla	k1gFnPc4	síla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Viola	Viola	k1gFnSc1	Viola
je	být	k5eAaImIp3nS	být
notována	notován	k2eAgFnSc1d1	notována
ve	v	k7c6	v
violovém	violový	k2eAgInSc6d1	violový
klíči	klíč	k1gInSc6	klíč
<g/>
;	;	kIx,	;
zápis	zápis	k1gInSc1	zápis
v	v	k7c6	v
houslovém	houslový	k2eAgInSc6d1	houslový
klíči	klíč	k1gInSc6	klíč
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
mnoho	mnoho	k6eAd1	mnoho
pomocných	pomocný	k2eAgFnPc2d1	pomocná
linek	linka	k1gFnPc2	linka
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
menší	malý	k2eAgFnSc4d2	menší
přehlednost	přehlednost	k1gFnSc4	přehlednost
<g/>
.	.	kIx.	.
</s>
<s>
Houslový	houslový	k2eAgInSc1d1	houslový
klíč	klíč	k1gInSc1	klíč
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
používá	používat	k5eAaImIp3nS	používat
jen	jen	k9	jen
pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
těch	ten	k3xDgFnPc2	ten
pasáží	pasáž	k1gFnPc2	pasáž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
viola	viola	k1gFnSc1	viola
hraje	hrát	k5eAaImIp3nS	hrát
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
vysoké	vysoký	k2eAgInPc4d1	vysoký
tóny	tón	k1gInPc4	tón
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Typy	typ	k1gInPc1	typ
viol	viola	k1gFnPc2	viola
==	==	k?	==
</s>
</p>
<p>
<s>
kvinton	kvinton	k1gInSc1	kvinton
–	–	k?	–
pětistrunná	pětistrunný	k2eAgFnSc1d1	pětistrunná
viola	viola	k1gFnSc1	viola
s	s	k7c7	s
laděním	ladění	k1gNnSc7	ladění
C	C	kA	C
-	-	kIx~	-
G	G	kA	G
-	-	kIx~	-
D	D	kA	D
<g/>
́	́	k?	́
<g/>
-	-	kIx~	-
A	a	k9	a
<g/>
́	́	k?	́
<g/>
-	-	kIx~	-
E	E	kA	E
<g/>
́ ́	́ ́	k?	́ ́
</s>
</p>
<p>
<s>
elektrická	elektrický	k2eAgFnSc1d1	elektrická
viola	viola	k1gFnSc1	viola
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Sólová	sólový	k2eAgFnSc1d1	sólová
hra	hra	k1gFnSc1	hra
===	===	k?	===
</s>
</p>
<p>
<s>
K	k	k7c3	k
nejdůležitějším	důležitý	k2eAgInPc3d3	nejdůležitější
violovým	violový	k2eAgInPc3d1	violový
koncertům	koncert	k1gInPc3	koncert
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Johann	Johann	k1gMnSc1	Johann
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bach	Bach	k1gMnSc1	Bach
<g/>
:	:	kIx,	:
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Braniborský	braniborský	k2eAgInSc1d1	braniborský
koncert	koncert	k1gInSc1	koncert
B-Dur	B-Dura	k1gFnPc2	B-Dura
BWV	BWV	kA	BWV
1051	[number]	k4	1051
</s>
</p>
<p>
<s>
Georg	Georg	k1gMnSc1	Georg
Philipp	Philipp	k1gMnSc1	Philipp
Telemann	Telemann	k1gMnSc1	Telemann
<g/>
:	:	kIx,	:
Violový	violový	k2eAgInSc1d1	violový
koncert	koncert	k1gInSc1	koncert
G-Dur	G-Dura	k1gFnPc2	G-Dura
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Stamic	Stamic	k1gMnSc1	Stamic
<g/>
:	:	kIx,	:
Violový	violový	k2eAgInSc1d1	violový
koncert	koncert	k1gInSc1	koncert
č.	č.	k?	č.
1	[number]	k4	1
D-Dur	D-Dura	k1gFnPc2	D-Dura
</s>
</p>
<p>
<s>
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Amadeus	Amadeus	k1gMnSc1	Amadeus
Mozart	Mozart	k1gMnSc1	Mozart
<g/>
:	:	kIx,	:
Sinfonia	sinfonia	k1gFnSc1	sinfonia
concertante	concertant	k1gMnSc5	concertant
KV	KV	kA	KV
364	[number]	k4	364
(	(	kIx(	(
<g/>
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
viola	viola	k1gFnSc1	viola
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Niccolò	Niccolò	k?	Niccolò
Paganini	Paganin	k2eAgMnPc1d1	Paganin
<g/>
:	:	kIx,	:
Sonata	Sonata	k1gFnSc1	Sonata
per	prát	k5eAaImRp2nS	prát
La	la	k1gNnSc4	la
Gran	Gran	k1gInSc1	Gran
<g/>
'	'	kIx"	'
Viola	Viola	k1gFnSc1	Viola
ed	ed	k?	ed
Orchestra	orchestra	k1gFnSc1	orchestra
</s>
</p>
<p>
<s>
Hector	Hector	k1gMnSc1	Hector
Berlioz	Berlioz	k1gMnSc1	Berlioz
<g/>
:	:	kIx,	:
Harold	Harold	k1gMnSc1	Harold
en	en	k?	en
Italie	Italie	k1gFnSc1	Italie
<g/>
,	,	kIx,	,
Symphonie	Symphonie	k1gFnSc1	Symphonie
mit	mit	k?	mit
konzertanter	konzertanter	k1gInSc1	konzertanter
Viola	Viola	k1gFnSc1	Viola
(	(	kIx(	(
<g/>
1834	[number]	k4	1834
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
William	William	k1gInSc1	William
Walton	Walton	k1gInSc1	Walton
<g/>
:	:	kIx,	:
Koncert	koncert	k1gInSc1	koncert
pro	pro	k7c4	pro
violu	viola	k1gFnSc4	viola
a	a	k8xC	a
orchestr	orchestr	k1gInSc4	orchestr
</s>
</p>
<p>
<s>
Paul	Paul	k1gMnSc1	Paul
Hindemith	Hindemitha	k1gFnPc2	Hindemitha
<g/>
:	:	kIx,	:
Der	drát	k5eAaImRp2nS	drát
Schwanendreher	Schwanendrehra	k1gFnPc2	Schwanendrehra
</s>
</p>
<p>
<s>
Paul	Paul	k1gMnSc1	Paul
Hindemith	Hindemith	k1gMnSc1	Hindemith
<g/>
:	:	kIx,	:
Kammermusik	Kammermusik	k1gMnSc1	Kammermusik
Nr	Nr	k1gMnSc1	Nr
<g/>
.5	.5	k4	.5
</s>
</p>
<p>
<s>
Béla	Béla	k1gMnSc1	Béla
Bartók	Bartók	k1gMnSc1	Bartók
<g/>
:	:	kIx,	:
Koncert	koncert	k1gInSc1	koncert
pro	pro	k7c4	pro
violu	viola	k1gFnSc4	viola
a	a	k8xC	a
orchestr	orchestr	k1gInSc4	orchestr
</s>
</p>
<p>
<s>
Alfred	Alfred	k1gMnSc1	Alfred
Schnittke	Schnittk	k1gFnSc2	Schnittk
<g/>
:	:	kIx,	:
Koncert	koncert	k1gInSc1	koncert
pro	pro	k7c4	pro
violu	viola	k1gFnSc4	viola
a	a	k8xC	a
orchestr	orchestr	k1gInSc4	orchestr
</s>
</p>
<p>
<s>
Toru	torus	k1gInSc3	torus
Takemitsu	Takemits	k1gInSc2	Takemits
<g/>
:	:	kIx,	:
Koncert	koncert	k1gInSc1	koncert
pro	pro	k7c4	pro
violu	viola	k1gFnSc4	viola
a	a	k8xC	a
orchestr	orchestr	k1gInSc4	orchestr
</s>
</p>
<p>
<s>
Richard	Richard	k1gMnSc1	Richard
Strauss	Straussa	k1gFnPc2	Straussa
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Don	Don	k1gMnSc1	Don
Quijote	Quijot	k1gMnSc5	Quijot
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
35	[number]	k4	35
(	(	kIx(	(
<g/>
violoncello	violoncello	k1gNnSc1	violoncello
a	a	k8xC	a
viola	viola	k1gFnSc1	viola
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Carl	Carl	k1gMnSc1	Carl
Friedrich	Friedrich	k1gMnSc1	Friedrich
Zelter	Zelter	k1gMnSc1	Zelter
<g/>
:	:	kIx,	:
Koncert	koncert	k1gInSc1	koncert
pro	pro	k7c4	pro
violu	viola	k1gFnSc4	viola
a	a	k8xC	a
orchestr	orchestr	k1gInSc4	orchestr
Es-Dur	Es-Dura	k1gFnPc2	Es-Dura
</s>
</p>
<p>
<s>
===	===	k?	===
Komorní	komorní	k2eAgFnSc1d1	komorní
hudba	hudba	k1gFnSc1	hudba
===	===	k?	===
</s>
</p>
<p>
<s>
Viola	Viola	k1gFnSc1	Viola
je	být	k5eAaImIp3nS	být
nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
každého	každý	k3xTgInSc2	každý
smyčcového	smyčcový	k2eAgInSc2d1	smyčcový
kvartetu	kvartet	k1gInSc2	kvartet
i	i	k8xC	i
kvintetu	kvintet	k1gInSc2	kvintet
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
houslemi	housle	k1gFnPc7	housle
a	a	k8xC	a
violoncellem	violoncello	k1gNnSc7	violoncello
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
také	také	k9	také
kontrabasem	kontrabas	k1gInSc7	kontrabas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
skladby	skladba	k1gFnPc1	skladba
pro	pro	k7c4	pro
neobvyklejší	obvyklý	k2eNgNnSc4d2	neobvyklejší
obsazení	obsazení	k1gNnSc4	obsazení
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Claude	Claude	k6eAd1	Claude
Debussy	Debuss	k1gInPc1	Debuss
<g/>
:	:	kIx,	:
Sonáta	sonáta	k1gFnSc1	sonáta
pro	pro	k7c4	pro
flétnu	flétna	k1gFnSc4	flétna
<g/>
,	,	kIx,	,
violu	viola	k1gFnSc4	viola
a	a	k8xC	a
harfu	harfa	k1gFnSc4	harfa
</s>
</p>
<p>
<s>
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Amadeus	Amadeus	k1gMnSc1	Amadeus
Mozart	Mozart	k1gMnSc1	Mozart
<g/>
:	:	kIx,	:
Trio	trio	k1gNnSc1	trio
pro	pro	k7c4	pro
klarinet	klarinet	k1gInSc4	klarinet
<g/>
,	,	kIx,	,
violu	viola	k1gFnSc4	viola
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
</s>
</p>
<p>
<s>
Ludwig	Ludwig	k1gMnSc1	Ludwig
van	vana	k1gFnPc2	vana
Beethoven	Beethoven	k1gMnSc1	Beethoven
<g/>
:	:	kIx,	:
Serenáda	serenáda	k1gFnSc1	serenáda
pro	pro	k7c4	pro
flétnu	flétna	k1gFnSc4	flétna
<g/>
,	,	kIx,	,
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
violu	viola	k1gFnSc4	viola
</s>
</p>
<p>
<s>
===	===	k?	===
Orchestr	orchestr	k1gInSc4	orchestr
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
symfonických	symfonický	k2eAgInPc6d1	symfonický
orchestrech	orchestr	k1gInPc6	orchestr
sedí	sedit	k5eAaImIp3nS	sedit
nejčastěji	často	k6eAd3	často
12	[number]	k4	12
violistů	violista	k1gMnPc2	violista
<g/>
.	.	kIx.	.
</s>
<s>
Viola	Viola	k1gFnSc1	Viola
má	mít	k5eAaImIp3nS	mít
často	často	k6eAd1	často
v	v	k7c4	v
orchestru	orchestra	k1gFnSc4	orchestra
důležitou	důležitý	k2eAgFnSc4d1	důležitá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
doplňující	doplňující	k2eAgFnSc6d1	doplňující
funkci	funkce	k1gFnSc6	funkce
–	–	k?	–
její	její	k3xOp3gInSc4	její
part	part	k1gInSc4	part
se	se	k3xPyFc4	se
omezuje	omezovat	k5eAaImIp3nS	omezovat
na	na	k7c6	na
vyplňování	vyplňování	k1gNnSc6	vyplňování
harmonie	harmonie	k1gFnSc1	harmonie
mezi	mezi	k7c7	mezi
druhými	druhý	k4xOgFnPc7	druhý
houslemi	housle	k1gFnPc7	housle
a	a	k8xC	a
violoncellem	violoncello	k1gNnSc7	violoncello
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc4	několik
výjimek	výjimka	k1gFnPc2	výjimka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
skladatelé	skladatel	k1gMnPc1	skladatel
přidělili	přidělit	k5eAaPmAgMnP	přidělit
viole	viola	k1gFnSc6	viola
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
melodii	melodie	k1gFnSc4	melodie
<g/>
;	;	kIx,	;
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
pasáže	pasáž	k1gFnPc4	pasáž
pro	pro	k7c4	pro
violu	viola	k1gFnSc4	viola
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
orchestrálních	orchestrální	k2eAgInPc6d1	orchestrální
dílech	díl	k1gInPc6	díl
Antona	Anton	k1gMnSc4	Anton
Brucknera	Bruckner	k1gMnSc4	Bruckner
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
symfonie	symfonie	k1gFnPc1	symfonie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Gustava	Gustav	k1gMnSc2	Gustav
Mahlera	Mahler	k1gMnSc2	Mahler
(	(	kIx(	(
<g/>
Adagio	adagio	k6eAd1	adagio
v	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
symfonii	symfonie	k1gFnSc6	symfonie
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
symfonické	symfonický	k2eAgFnSc6d1	symfonická
básní	báseň	k1gFnPc2	báseň
Richarda	Richard	k1gMnSc2	Richard
Strausse	Strauss	k1gMnSc2	Strauss
Don	Don	k1gMnSc1	Don
Quijote	Quijot	k1gMnSc5	Quijot
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zvuková	zvukový	k2eAgFnSc1d1	zvuková
ukázka	ukázka	k1gFnSc1	ukázka
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
violistů	violista	k1gMnPc2	violista
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
viola	viola	k1gFnSc1	viola
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
viola	viola	k1gFnSc1	viola
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
viola	viola	k1gFnSc1	viola
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
