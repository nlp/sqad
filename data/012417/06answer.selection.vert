<s>
Viola	Viola	k1gFnSc1	Viola
(	(	kIx(	(
<g/>
zastarale	zastarale	k6eAd1	zastarale
nebo	nebo	k8xC	nebo
zřídkavě	zřídkavě	k6eAd1	zřídkavě
bráč	bráč	k1gInSc1	bráč
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
strunný	strunný	k2eAgInSc1d1	strunný
smyčcový	smyčcový	k2eAgInSc1d1	smyčcový
hudební	hudební	k2eAgInSc1d1	hudební
nástroj	nástroj	k1gInSc1	nástroj
se	s	k7c7	s
čtyřmi	čtyři	k4xCgFnPc7	čtyři
strunami	struna	k1gFnPc7	struna
laděnými	laděný	k2eAgFnPc7d1	laděná
v	v	k7c6	v
čistých	čistý	k2eAgFnPc6d1	čistá
kvintách	kvinta	k1gFnPc6	kvinta
<g/>
:	:	kIx,	:
c	c	k0	c
<g/>
,	,	kIx,	,
g	g	kA	g
<g/>
,	,	kIx,	,
d1	d1	k4	d1
<g/>
,	,	kIx,	,
a1	a1	k4	a1
<g/>
.	.	kIx.	.
</s>
