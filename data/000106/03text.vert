<s>
Vulcano	Vulcana	k1gFnSc5	Vulcana
/	/	kIx~	/
<g/>
vulkaː	vulkaː	k?	vulkaː
<g/>
/	/	kIx~	/
(	(	kIx(	(
<g/>
22	[number]	k4	22
km2	km2	k4	km2
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejjižnějším	jižní	k2eAgInSc7d3	nejjižnější
ostrovem	ostrov	k1gInSc7	ostrov
Liparského	Liparský	k2eAgNnSc2d1	Liparské
souostroví	souostroví	k1gNnSc2	souostroví
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
ostrov	ostrov	k1gInSc1	ostrov
patří	patřit	k5eAaImIp3nS	patřit
administrativně	administrativně	k6eAd1	administrativně
k	k	k7c3	k
městu	město	k1gNnSc3	město
Lipari	Lipar	k1gFnSc2	Lipar
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
právě	právě	k9	právě
ostrov	ostrov	k1gInSc1	ostrov
Vulcano	Vulcana	k1gFnSc5	Vulcana
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
dal	dát	k5eAaPmAgMnS	dát
jméno	jméno	k1gNnSc4	jméno
celé	celá	k1gFnSc2	celá
vědě	věda	k1gFnSc3	věda
<g/>
,	,	kIx,	,
vulkanologii	vulkanologie	k1gFnSc3	vulkanologie
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
vulkán	vulkán	k1gInSc1	vulkán
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
volcano	volcana	k1gFnSc5	volcana
<g/>
,	,	kIx,	,
itl	itl	k?	itl
<g/>
.	.	kIx.	.
vulcano	vulcana	k1gFnSc5	vulcana
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
sopky	sopka	k1gFnPc4	sopka
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
Vulcano	Vulcana	k1gFnSc5	Vulcana
aktivní	aktivní	k2eAgFnSc4d1	aktivní
sopkou	sopka	k1gFnSc7	sopka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
domnívali	domnívat	k5eAaImAgMnP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
páry	pára	k1gFnPc1	pára
stoupající	stoupající	k2eAgFnPc1d1	stoupající
z	z	k7c2	z
kráteru	kráter	k1gInSc2	kráter
jsou	být	k5eAaImIp3nP	být
dílem	dílo	k1gNnSc7	dílo
řeckého	řecký	k2eAgMnSc2d1	řecký
boha	bůh	k1gMnSc2	bůh
ohně	oheň	k1gInSc2	oheň
a	a	k8xC	a
kovářů	kovář	k1gMnPc2	kovář
<g/>
,	,	kIx,	,
Hefaista	Hefaista	k1gMnSc1	Hefaista
<g/>
.	.	kIx.	.
</s>
<s>
Římským	římský	k2eAgInSc7d1	římský
ekvivalentem	ekvivalent	k1gInSc7	ekvivalent
boha	bůh	k1gMnSc2	bůh
ohně	oheň	k1gInSc2	oheň
byl	být	k5eAaImAgInS	být
ovšem	ovšem	k9	ovšem
Vulkán	vulkán	k1gInSc1	vulkán
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
sopečnému	sopečný	k2eAgInSc3d1	sopečný
kuželu	kužel	k1gInSc3	kužel
začalo	začít	k5eAaPmAgNnS	začít
říkat	říkat	k5eAaImF	říkat
"	"	kIx"	"
<g/>
Vulkánova	Vulkánův	k2eAgFnSc1d1	Vulkánova
propast	propast	k1gFnSc1	propast
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
La	la	k1gNnSc4	la
Fossa	Fossa	k1gFnSc1	Fossa
di	di	k?	di
Vulcano	Vulcana	k1gFnSc5	Vulcana
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
projevů	projev	k1gInPc2	projev
sopečné	sopečný	k2eAgFnSc2d1	sopečná
činnosti	činnost	k1gFnSc2	činnost
měl	mít	k5eAaImAgInS	mít
mít	mít	k5eAaImF	mít
Vulcanus	Vulcanus	k1gInSc1	Vulcanus
v	v	k7c6	v
hoře	hora	k1gFnSc6	hora
svou	svůj	k3xOyFgFnSc4	svůj
kovárnu	kovárna	k1gFnSc4	kovárna
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
pochází	pocházet	k5eAaImIp3nS	pocházet
také	také	k9	také
další	další	k2eAgInSc1d1	další
známý	známý	k2eAgInSc1d1	známý
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Stará	starý	k2eAgFnSc1d1	stará
kovárna	kovárna	k1gFnSc1	kovárna
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
La	la	k1gNnSc7	la
Forgia	Forgius	k1gMnSc2	Forgius
vecchia	vecchius	k1gMnSc2	vecchius
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
kráter	kráter	k1gInSc1	kráter
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Velký	velký	k2eAgInSc1d1	velký
kráter	kráter	k1gInSc1	kráter
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Gran	Gran	k1gMnSc1	Gran
Cratere	Crater	k1gInSc5	Crater
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ostatní	ostatní	k2eAgInPc4d1	ostatní
Liparské	Liparský	k2eAgInPc4d1	Liparský
ostrovy	ostrov	k1gInPc4	ostrov
je	být	k5eAaImIp3nS	být
i	i	k9	i
ostrov	ostrov	k1gInSc1	ostrov
Vulcano	Vulcana	k1gFnSc5	Vulcana
součástí	součást	k1gFnSc7	součást
vulkanického	vulkanický	k2eAgInSc2d1	vulkanický
oblouku	oblouk	k1gInSc2	oblouk
nacházejícího	nacházející	k2eAgInSc2d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
střetu	střet	k1gInSc2	střet
eurasijské	eurasijský	k2eAgFnSc2d1	eurasijská
a	a	k8xC	a
africké	africký	k2eAgFnSc2d1	africká
litosférické	litosférický	k2eAgFnSc2d1	litosférická
desky	deska	k1gFnSc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
sopečných	sopečný	k2eAgInPc2d1	sopečný
ostrovů	ostrov	k1gInPc2	ostrov
v	v	k7c6	v
Tyrhénském	tyrhénský	k2eAgNnSc6d1	Tyrhénské
moři	moře	k1gNnSc6	moře
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
doprovodným	doprovodný	k2eAgInSc7d1	doprovodný
jevem	jev	k1gInSc7	jev
vulkanické	vulkanický	k2eAgFnSc2d1	vulkanická
aktivity	aktivita	k1gFnSc2	aktivita
probíhající	probíhající	k2eAgFnSc2d1	probíhající
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
subdukcí	subdukce	k1gFnSc7	subdukce
(	(	kIx(	(
<g/>
podsouváním	podsouvání	k1gNnSc7	podsouvání
<g/>
)	)	kIx)	)
africké	africký	k2eAgFnPc1d1	africká
desky	deska	k1gFnPc1	deska
pod	pod	k7c7	pod
eurasijskou	eurasijský	k2eAgFnSc7d1	eurasijská
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
Vulcano	Vulcana	k1gFnSc5	Vulcana
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
tvořen	tvořit	k5eAaImNgInS	tvořit
pěti	pět	k4xCc2	pět
různými	různý	k2eAgFnPc7d1	různá
sopečnými	sopečný	k2eAgFnPc7d1	sopečná
strukturami	struktura	k1gFnPc7	struktura
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
vývoje	vývoj	k1gInSc2	vývoj
částečně	částečně	k6eAd1	částečně
deformovány	deformován	k2eAgFnPc1d1	deformována
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
strukturu	struktura	k1gFnSc4	struktura
představuje	představovat	k5eAaImIp3nS	představovat
tzv.	tzv.	kA	tzv.
Primordial	Primordial	k1gInSc1	Primordial
Vulcano	Vulcana	k1gFnSc5	Vulcana
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
prvotní	prvotní	k2eAgFnSc1d1	prvotní
sopka	sopka	k1gFnSc1	sopka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
sopka	sopka	k1gFnSc1	sopka
byla	být	k5eAaImAgFnS	být
částečně	částečně	k6eAd1	částečně
zničena	zničit	k5eAaPmNgFnS	zničit
při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
druhé	druhý	k4xOgFnSc2	druhý
struktury	struktura	k1gFnSc2	struktura
<g/>
,	,	kIx,	,
Piano	piano	k1gNnSc1	piano
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
třetí	třetí	k4xOgFnSc4	třetí
sopečnou	sopečný	k2eAgFnSc4d1	sopečná
strukturu	struktura	k1gFnSc4	struktura
<g/>
,	,	kIx,	,
Lentia	Lentia	k1gFnSc1	Lentia
<g/>
,	,	kIx,	,
narušil	narušit	k5eAaPmAgMnS	narušit
přibližně	přibližně	k6eAd1	přibližně
před	před	k7c7	před
14	[number]	k4	14
miliony	milion	k4xCgInPc7	milion
lety	léto	k1gNnPc7	léto
další	další	k2eAgInSc4d1	další
výbuch	výbuch	k1gInSc4	výbuch
a	a	k8xC	a
kolaps	kolaps	k1gInSc4	kolaps
kaldery	kaldera	k1gFnSc2	kaldera
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
dnešní	dnešní	k2eAgInSc1d1	dnešní
hlavní	hlavní	k2eAgInSc1d1	hlavní
sopečný	sopečný	k2eAgInSc1d1	sopečný
kužel	kužel	k1gInSc1	kužel
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
Fossa	Fossa	k1gFnSc1	Fossa
<g/>
.	.	kIx.	.
</s>
<s>
Kráter	kráter	k1gInSc1	kráter
tohoto	tento	k3xDgInSc2	tento
kužele	kužel	k1gInSc2	kužel
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
sopečné	sopečný	k2eAgFnSc2d1	sopečná
činnosti	činnost	k1gFnSc2	činnost
posledních	poslední	k2eAgInPc2d1	poslední
5	[number]	k4	5
500	[number]	k4	500
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nejsevernější	severní	k2eAgFnSc1d3	nejsevernější
část	část	k1gFnSc1	část
Vulcana	Vulcan	k1gMnSc2	Vulcan
představuje	představovat	k5eAaImIp3nS	představovat
poloostrov	poloostrov	k1gInSc1	poloostrov
Vulcanello	Vulcanello	k1gNnSc1	Vulcanello
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
samostatný	samostatný	k2eAgInSc4d1	samostatný
ostrůvek	ostrůvek	k1gInSc4	ostrůvek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
úžině	úžina	k1gFnSc6	úžina
mezi	mezi	k7c7	mezi
Vulcanem	Vulcan	k1gInSc7	Vulcan
a	a	k8xC	a
Lipari	Lipari	k1gNnSc1	Lipari
začal	začít	k5eAaPmAgInS	začít
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
po	po	k7c6	po
podmořské	podmořský	k2eAgFnSc6d1	podmořská
erupci	erupce	k1gFnSc6	erupce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
183	[number]	k4	183
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Během	během	k7c2	během
středověkých	středověký	k2eAgFnPc2d1	středověká
erupcí	erupce	k1gFnPc2	erupce
se	se	k3xPyFc4	se
Vulcanello	Vulcanello	k1gNnSc1	Vulcanello
díky	díky	k7c3	díky
výlevům	výlev	k1gInPc3	výlev
lávy	láva	k1gFnSc2	láva
a	a	k8xC	a
výbuchům	výbuch	k1gInPc3	výbuch
pyroklastik	pyroklastika	k1gFnPc2	pyroklastika
napojilo	napojit	k5eAaPmAgNnS	napojit
na	na	k7c4	na
Vulcano	Vulcana	k1gFnSc5	Vulcana
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
erupce	erupce	k1gFnSc1	erupce
Vulcanella	Vulcanella	k1gFnSc1	Vulcanella
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
posledních	poslední	k2eAgNnPc2d1	poslední
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
vulkanická	vulkanický	k2eAgFnSc1d1	vulkanická
aktivita	aktivita	k1gFnSc1	aktivita
koncentrovala	koncentrovat	k5eAaBmAgFnS	koncentrovat
především	především	k6eAd1	především
do	do	k7c2	do
prostoru	prostor	k1gInSc2	prostor
kráteru	kráter	k1gInSc2	kráter
La	la	k1gNnSc2	la
Fossa	Fossa	k1gFnSc1	Fossa
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
eruptivní	eruptivní	k2eAgInSc1d1	eruptivní
cyklus	cyklus	k1gInSc1	cyklus
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1727	[number]	k4	1727
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
12	[number]	k4	12
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozsáhlému	rozsáhlý	k2eAgInSc3d1	rozsáhlý
a	a	k8xC	a
doposud	doposud	k6eAd1	doposud
poslednímu	poslední	k2eAgInSc3d1	poslední
výlevu	výlev	k1gInSc3	výlev
ryolitické	ryolitický	k2eAgFnSc2d1	ryolitický
lávy	láva	k1gFnSc2	láva
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
nazván	nazvat	k5eAaBmNgInS	nazvat
jako	jako	k8xS	jako
vařící	vařící	k2eAgNnSc1d1	vařící
kamení	kamení	k1gNnSc1	kamení
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Pietre	Pietr	k1gMnSc5	Pietr
Cotte	Cott	k1gMnSc5	Cott
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
150	[number]	k4	150
letech	let	k1gInPc6	let
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
erupcím	erupce	k1gFnPc3	erupce
jen	jen	k9	jen
sporadicky	sporadicky	k6eAd1	sporadicky
<g/>
.	.	kIx.	.
</s>
<s>
Vyvrcholením	vyvrcholení	k1gNnSc7	vyvrcholení
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
byla	být	k5eAaImAgFnS	být
zatím	zatím	k6eAd1	zatím
poslední	poslední	k2eAgFnSc1d1	poslední
série	série	k1gFnSc1	série
erupcí	erupce	k1gFnPc2	erupce
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
došlo	dojít	k5eAaPmAgNnS	dojít
mezi	mezi	k7c7	mezi
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpnem	srpen	k1gInSc7	srpen
1888	[number]	k4	1888
a	a	k8xC	a
22	[number]	k4	22
<g/>
.	.	kIx.	.
březnem	březen	k1gInSc7	březen
1890	[number]	k4	1890
<g/>
.	.	kIx.	.
</s>
<s>
Charakter	charakter	k1gInSc1	charakter
erupce	erupce	k1gFnSc2	erupce
připomínal	připomínat	k5eAaImAgInS	připomínat
poslední	poslední	k2eAgNnSc4d1	poslední
vývojové	vývojový	k2eAgNnSc4d1	vývojové
stádium	stádium	k1gNnSc4	stádium
v	v	k7c6	v
cyklu	cyklus	k1gInSc6	cyklus
sopky	sopka	k1gFnSc2	sopka
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
poslední	poslední	k2eAgFnSc1d1	poslední
eruptivní	eruptivní	k2eAgFnSc1d1	eruptivní
činnost	činnost	k1gFnSc1	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
ní	on	k3xPp3gFnSc3	on
docházelo	docházet	k5eAaImAgNnS	docházet
na	na	k7c6	na
Vulcanu	Vulcan	k1gInSc6	Vulcan
k	k	k7c3	k
vyvrhování	vyvrhování	k1gNnSc3	vyvrhování
množství	množství	k1gNnSc2	množství
pyroklastik	pyroklastika	k1gFnPc2	pyroklastika
<g/>
,	,	kIx,	,
od	od	k7c2	od
sopečného	sopečný	k2eAgInSc2d1	sopečný
prachu	prach	k1gInSc2	prach
a	a	k8xC	a
popela	popel	k1gInSc2	popel
až	až	k9	až
po	po	k7c4	po
metrové	metrový	k2eAgInPc4d1	metrový
kusy	kus	k1gInPc4	kus
sopečných	sopečný	k2eAgFnPc2d1	sopečná
bomb	bomba	k1gFnPc2	bomba
<g/>
.	.	kIx.	.
</s>
<s>
Sopečné	sopečný	k2eAgFnSc2d1	sopečná
bomby	bomba	k1gFnSc2	bomba
<g/>
,	,	kIx,	,
vyznačující	vyznačující	k2eAgFnSc2d1	vyznačující
se	se	k3xPyFc4	se
tzv.	tzv.	kA	tzv.
chlebovou	chlebový	k2eAgFnSc7d1	chlebová
kůrkou	kůrka	k1gFnSc7	kůrka
<g/>
,	,	kIx,	,
dopadaly	dopadat	k5eAaImAgFnP	dopadat
převážně	převážně	k6eAd1	převážně
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
dnešního	dnešní	k2eAgInSc2d1	dnešní
přístavu	přístav	k1gInSc2	přístav
(	(	kIx(	(
<g/>
Vulcano	Vulcana	k1gFnSc5	Vulcana
Porto	porto	k1gNnSc1	porto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
původně	původně	k6eAd1	původně
neobydlené	obydlený	k2eNgFnSc6d1	neobydlená
oblasti	oblast	k1gFnSc6	oblast
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
žádným	žádný	k3yNgFnPc3	žádný
újmám	újma	k1gFnPc3	újma
na	na	k7c6	na
lidských	lidský	k2eAgInPc6d1	lidský
životech	život	k1gInPc6	život
ani	ani	k8xC	ani
poškození	poškození	k1gNnSc1	poškození
majetku	majetek	k1gInSc2	majetek
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
projevy	projev	k1gInPc1	projev
erupce	erupce	k1gFnSc2	erupce
Vulcana	Vulcan	k1gMnSc2	Vulcan
pocítili	pocítit	k5eAaPmAgMnP	pocítit
až	až	k9	až
na	na	k7c4	na
Lipari	Lipare	k1gFnSc4	Lipare
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
Vulcano	Vulcana	k1gFnSc5	Vulcana
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
klidné	klidný	k2eAgFnSc6d1	klidná
fázi	fáze	k1gFnSc6	fáze
<g/>
.	.	kIx.	.
</s>
<s>
Spící	spící	k2eAgFnSc1d1	spící
sopka	sopka	k1gFnSc1	sopka
o	o	k7c6	o
sobě	se	k3xPyFc3	se
nicméně	nicméně	k8xC	nicméně
občas	občas	k6eAd1	občas
dává	dávat	k5eAaImIp3nS	dávat
vědět	vědět	k5eAaImF	vědět
zemětřesnou	zemětřesný	k2eAgFnSc7d1	zemětřesná
činností	činnost	k1gFnSc7	činnost
a	a	k8xC	a
výrony	výron	k1gInPc1	výron
plynu	plynout	k5eAaImIp1nS	plynout
z	z	k7c2	z
fumarol	fumarola	k1gFnPc2	fumarola
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
aktivita	aktivita	k1gFnSc1	aktivita
byla	být	k5eAaImAgFnS	být
zaznamenána	zaznamenat	k5eAaPmNgFnS	zaznamenat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
datu	datum	k1gNnSc3	datum
a	a	k8xC	a
pravidelnosti	pravidelnost	k1gFnSc3	pravidelnost
cyklů	cyklus	k1gInPc2	cyklus
sopky	sopka	k1gFnSc2	sopka
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
mohlo	moct	k5eAaImAgNnS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
znovuoživení	znovuoživení	k1gNnSc3	znovuoživení
vulkanické	vulkanický	k2eAgFnSc2d1	vulkanická
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
přístavu	přístav	k1gInSc2	přístav
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dnes	dnes	k6eAd1	dnes
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
výronům	výron	k1gInPc3	výron
žhavých	žhavý	k2eAgInPc2d1	žhavý
sopečných	sopečný	k2eAgInPc2d1	sopečný
plynů	plyn	k1gInPc2	plyn
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
fumarol	fumarola	k1gFnPc2	fumarola
a	a	k8xC	a
solfatarů	solfatar	k1gInPc2	solfatar
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
úzké	úzký	k2eAgFnSc2d1	úzká
šíje	šíj	k1gFnSc2	šíj
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
lemují	lemovat	k5eAaImIp3nP	lemovat
břehy	břeh	k1gInPc1	břeh
Porto	porto	k1gNnSc1	porto
Ponente	Ponent	k1gInSc5	Ponent
(	(	kIx(	(
<g/>
Západní	západní	k2eAgInSc1d1	západní
přístav	přístav	k1gInSc1	přístav
<g/>
)	)	kIx)	)
a	a	k8xC	a
Porto	porto	k1gNnSc1	porto
Levante	Levant	k1gMnSc5	Levant
(	(	kIx(	(
<g/>
Východní	východní	k2eAgInSc1d1	východní
přístav	přístav	k1gInSc1	přístav
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
vulkanicky	vulkanicky	k6eAd1	vulkanicky
nejaktivnějších	aktivní	k2eAgFnPc2d3	nejaktivnější
oblastí	oblast	k1gFnPc2	oblast
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
a	a	k8xC	a
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
fumaroly	fumarola	k1gFnPc1	fumarola
i	i	k8xC	i
solfatary	solfatara	k1gFnPc1	solfatara
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
bahenním	bahenní	k2eAgNnSc7d1	bahenní
jezírkem	jezírko	k1gNnSc7	jezírko
fungují	fungovat	k5eAaImIp3nP	fungovat
jako	jako	k9	jako
lázně	lázeň	k1gFnPc1	lázeň
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrcholem	vrchol	k1gInSc7	vrchol
Vulcana	Vulcan	k1gMnSc2	Vulcan
je	být	k5eAaImIp3nS	být
Monte	Mont	k1gInSc5	Mont
Aria	Arium	k1gNnPc1	Arium
(	(	kIx(	(
<g/>
500	[number]	k4	500
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Okraj	okraj	k1gInSc1	okraj
hlavního	hlavní	k2eAgInSc2d1	hlavní
kráteru	kráter	k1gInSc2	kráter
<g/>
,	,	kIx,	,
Gran	Gran	k1gMnSc1	Gran
Cratere	Crater	k1gInSc5	Crater
o	o	k7c6	o
la	la	k1gNnSc6	la
Fossa	Foss	k1gMnSc2	Foss
di	di	k?	di
Vulcano	Vulcana	k1gFnSc5	Vulcana
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nP	ležet
o	o	k7c4	o
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
níže	nízce	k6eAd2	nízce
(	(	kIx(	(
<g/>
391	[number]	k4	391
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
kráteru	kráter	k1gInSc2	kráter
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
fumaroly	fumarola	k1gFnPc1	fumarola
<g/>
,	,	kIx,	,
kterými	který	k3yRgInPc7	který
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
sirné	sirný	k2eAgInPc1d1	sirný
páry	pár	k1gInPc1	pár
<g/>
,	,	kIx,	,
horké	horký	k2eAgInPc1d1	horký
až	až	k9	až
1000	[number]	k4	1000
°	°	k?	°
<g/>
C.	C.	kA	C.
Bahenní	bahenní	k2eAgFnPc4d1	bahenní
lázně	lázeň	k1gFnPc4	lázeň
a	a	k8xC	a
fumaroly	fumarola	k1gFnPc4	fumarola
na	na	k7c6	na
Vulcanu	Vulcan	k1gInSc6	Vulcan
jsou	být	k5eAaImIp3nP	být
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
sopkou	sopka	k1gFnSc7	sopka
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Stromboli	Strombole	k1gFnSc4	Strombole
vulkanicky	vulkanicky	k6eAd1	vulkanicky
nejaktivnější	aktivní	k2eAgFnSc4d3	nejaktivnější
a	a	k8xC	a
turisticky	turisticky	k6eAd1	turisticky
nejatraktivnější	atraktivní	k2eAgFnSc1d3	nejatraktivnější
část	část	k1gFnSc1	část
Liparských	Liparský	k2eAgInPc2d1	Liparský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
množství	množství	k1gNnSc1	množství
ubytovacích	ubytovací	k2eAgNnPc2d1	ubytovací
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
:	:	kIx,	:
hotely	hotel	k1gInPc1	hotel
<g/>
,	,	kIx,	,
bungalovy	bungalov	k1gInPc1	bungalov
<g/>
,	,	kIx,	,
kempy	kemp	k1gInPc1	kemp
<g/>
,	,	kIx,	,
...	...	k?	...
Turistickým	turistický	k2eAgNnSc7d1	turistické
lákadlem	lákadlo	k1gNnSc7	lákadlo
je	být	k5eAaImIp3nS	být
nejen	nejen	k6eAd1	nejen
bahenní	bahenní	k2eAgNnSc4d1	bahenní
jezírko	jezírko	k1gNnSc4	jezírko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
vyhlášená	vyhlášený	k2eAgFnSc1d1	vyhlášená
černá	černý	k2eAgFnSc1d1	černá
pláž	pláž	k1gFnSc1	pláž
nacházející	nacházející	k2eAgFnSc1d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
straně	strana	k1gFnSc6	strana
šíje	šíj	k1gFnSc2	šíj
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
na	na	k7c4	na
Porto	porto	k1gNnSc4	porto
ponente	ponent	k1gMnSc5	ponent
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
hlavní	hlavní	k2eAgInSc4d1	hlavní
sopečný	sopečný	k2eAgInSc4d1	sopečný
vrchol	vrchol	k1gInSc4	vrchol
trvá	trvat	k5eAaImIp3nS	trvat
výstup	výstup	k1gInSc4	výstup
2,5	[number]	k4	2,5
hodiny	hodina	k1gFnSc2	hodina
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
odtud	odtud	k6eAd1	odtud
panoramatické	panoramatický	k2eAgInPc1d1	panoramatický
rozhledy	rozhled	k1gInPc1	rozhled
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
nižší	nízký	k2eAgNnSc1d2	nižší
(	(	kIx(	(
<g/>
123	[number]	k4	123
m.	m.	k?	m.
n.	n.	k?	n.
<g/>
.	.	kIx.	.
<g/>
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
severně	severně	k6eAd1	severně
položený	položený	k2eAgInSc1d1	položený
Vulcanello	Vulcanello	k1gNnSc4	Vulcanello
<g/>
,	,	kIx,	,
trvá	trvat	k5eAaImIp3nS	trvat
výstup	výstup	k1gInSc4	výstup
zhruba	zhruba	k6eAd1	zhruba
hodinu	hodina	k1gFnSc4	hodina
<g/>
,	,	kIx,	,
i	i	k8xC	i
odtud	odtud	k6eAd1	odtud
jsou	být	k5eAaImIp3nP	být
pěkné	pěkný	k2eAgInPc4d1	pěkný
výhledy	výhled	k1gInPc4	výhled
<g/>
.	.	kIx.	.
</s>
