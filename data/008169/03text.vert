<p>
<s>
Dobře	dobře	k6eAd1	dobře
temperovaný	temperovaný	k2eAgInSc1d1	temperovaný
klavír	klavír	k1gInSc1	klavír
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
dvou	dva	k4xCgInPc2	dva
cyklů	cyklus	k1gInPc2	cyklus
skladeb	skladba	k1gFnPc2	skladba
Johanna	Johann	k1gMnSc4	Johann
Sebastiana	Sebastian	k1gMnSc4	Sebastian
Bacha	Bacha	k?	Bacha
psaných	psaný	k2eAgInPc6d1	psaný
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
clavier	clavier	k1gInSc4	clavier
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
=	=	kIx~	=
<g/>
klávesový	klávesový	k2eAgInSc1d1	klávesový
nástroj	nástroj	k1gInSc1	nástroj
<g/>
,	,	kIx,	,
v	v	k7c6	v
baroku	barok	k1gInSc6	barok
nejčastěji	často	k6eAd3	často
cembalo	cembalo	k1gNnSc1	cembalo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
cyklu	cyklus	k1gInSc6	cyklus
jsou	být	k5eAaImIp3nP	být
zastoupeny	zastoupen	k2eAgFnPc1d1	zastoupena
skladby	skladba	k1gFnPc1	skladba
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
tóninách	tónina	k1gFnPc6	tónina
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
tóninu	tónina	k1gFnSc4	tónina
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
preludium	preludium	k1gNnSc1	preludium
a	a	k8xC	a
fuga	fuga	k1gFnSc1	fuga
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
temperovaný	temperovaný	k2eAgInSc1d1	temperovaný
klavír	klavír	k1gInSc1	klavír
I.	I.	kA	I.
čítá	čítat	k5eAaImIp3nS	čítat
24	[number]	k4	24
skladeb	skladba	k1gFnPc2	skladba
(	(	kIx(	(
<g/>
BVW	BVW	kA	BVW
846	[number]	k4	846
<g/>
-	-	kIx~	-
<g/>
869	[number]	k4	869
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
temperovaný	temperovaný	k2eAgInSc1d1	temperovaný
klavír	klavír	k1gInSc1	klavír
II	II	kA	II
<g/>
.	.	kIx.	.
čítá	čítat	k5eAaImIp3nS	čítat
též	též	k9	též
24	[number]	k4	24
skladeb	skladba	k1gFnPc2	skladba
(	(	kIx(	(
<g/>
BVW	BVW	kA	BVW
870	[number]	k4	870
<g/>
-	-	kIx~	-
<g/>
893	[number]	k4	893
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Preludia	preludium	k1gNnPc1	preludium
a	a	k8xC	a
fugy	fuga	k1gFnPc1	fuga
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
dílu	díl	k1gInSc6	díl
jsou	být	k5eAaImIp3nP	být
řazeny	řadit	k5eAaImNgInP	řadit
chromaticky	chromaticky	k6eAd1	chromaticky
vzestupně	vzestupně	k6eAd1	vzestupně
v	v	k7c6	v
dur	dur	k1gNnSc6	dur
i	i	k8xC	i
moll	moll	k1gNnSc6	moll
<g/>
:	:	kIx,	:
C	C	kA	C
dur	dur	k1gNnSc1	dur
<g/>
,	,	kIx,	,
c	c	k0	c
moll	moll	k1gNnSc2	moll
<g/>
,	,	kIx,	,
Cis	cis	k1gNnSc2	cis
dur	dur	k1gNnSc2	dur
<g/>
,	,	kIx,	,
cis	cis	k1gNnSc2	cis
moll	moll	k1gNnSc2	moll
<g/>
,	,	kIx,	,
D	D	kA	D
dur	dur	k1gNnSc1	dur
<g/>
,	,	kIx,	,
d	d	k?	d
moll	moll	k1gNnSc2	moll
<g/>
...	...	k?	...
</s>
</p>
<p>
<s>
Bach	Bach	k1gMnSc1	Bach
tu	tu	k6eAd1	tu
ukázal	ukázat	k5eAaPmAgMnS	ukázat
nové	nový	k2eAgFnPc4d1	nová
skladatelské	skladatelský	k2eAgFnPc4d1	skladatelská
možnosti	možnost	k1gFnPc4	možnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
ladění	ladění	k1gNnSc4	ladění
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
převažujícím	převažující	k2eAgFnPc3d1	převažující
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
kompozici	kompozice	k1gFnSc4	kompozice
na	na	k7c4	na
klávesový	klávesový	k2eAgInSc4d1	klávesový
nástroj	nástroj	k1gInSc4	nástroj
nedaly	dát	k5eNaPmAgInP	dát
některé	některý	k3yIgFnPc4	některý
tóniny	tónina	k1gFnPc4	tónina
použít	použít	k5eAaPmF	použít
<g/>
.	.	kIx.	.
</s>
<s>
Totéž	týž	k3xTgNnSc1	týž
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
o	o	k7c6	o
některých	některý	k3yIgInPc6	některý
akordech	akord	k1gInPc6	akord
<g/>
.	.	kIx.	.
</s>
<s>
Proč	proč	k6eAd1	proč
tomu	ten	k3xDgNnSc3	ten
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
vysvětlují	vysvětlovat	k5eAaImIp3nP	vysvětlovat
hesla	heslo	k1gNnPc1	heslo
ladění	ladění	k1gNnSc2	ladění
<g/>
,	,	kIx,	,
přirozené	přirozený	k2eAgNnSc1d1	přirozené
ladění	ladění	k1gNnSc1	ladění
<g/>
,	,	kIx,	,
rovnoměrná	rovnoměrný	k2eAgFnSc1d1	rovnoměrná
temperatura	temperatura	k1gFnSc1	temperatura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nahrávky	nahrávka	k1gFnSc2	nahrávka
==	==	k?	==
</s>
</p>
<p>
<s>
Bachův	Bachův	k2eAgInSc4d1	Bachův
Temperovaný	temperovaný	k2eAgInSc4d1	temperovaný
klavír	klavír	k1gInSc4	klavír
náhrála	náhrála	k1gFnSc1	náhrála
řada	řada	k1gFnSc1	řada
interpretů	interpret	k1gMnPc2	interpret
na	na	k7c4	na
různé	různý	k2eAgInPc4d1	různý
nástroje	nástroj	k1gInPc4	nástroj
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Wanda	Wanda	k1gFnSc1	Wanda
Landowska	Landowska	k1gFnSc1	Landowska
(	(	kIx(	(
<g/>
cembalo	cembalo	k1gNnSc1	cembalo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Taťjana	Taťjana	k1gFnSc1	Taťjana
Nikolajeva	Nikolajeva	k1gFnSc1	Nikolajeva
(	(	kIx(	(
<g/>
klavír	klavír	k1gInSc1	klavír
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Helmut	Helmut	k1gMnSc1	Helmut
Walcha	Walcha	k1gMnSc1	Walcha
(	(	kIx(	(
<g/>
cembalo	cembalo	k1gNnSc1	cembalo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Glenn	Glenn	k1gMnSc1	Glenn
Gould	Gould	k1gMnSc1	Gould
(	(	kIx(	(
<g/>
klavír	klavír	k1gInSc1	klavír
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Gustav	Gustav	k1gMnSc1	Gustav
Leonhardt	Leonhardt	k1gMnSc1	Leonhardt
(	(	kIx(	(
<g/>
cembalo	cembalo	k1gNnSc1	cembalo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sviatoslav	Sviatoslav	k1gMnSc1	Sviatoslav
Richter	Richter	k1gMnSc1	Richter
(	(	kIx(	(
<g/>
klavír	klavír	k1gInSc1	klavír
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zuzana	Zuzana	k1gFnSc1	Zuzana
Růžičková	Růžičková	k1gFnSc1	Růžičková
(	(	kIx(	(
<g/>
cembalo	cembalo	k1gNnSc1	cembalo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Friedrich	Friedrich	k1gMnSc1	Friedrich
Gulda	Gulda	k1gMnSc1	Gulda
(	(	kIx(	(
<g/>
klavír	klavír	k1gInSc1	klavír
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Louis	Louis	k1gMnSc1	Louis
Thiry	Thira	k1gFnSc2	Thira
(	(	kIx(	(
<g/>
varhany	varhany	k1gFnPc1	varhany
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ton	Ton	k1gMnSc1	Ton
Koopman	Koopman	k1gMnSc1	Koopman
(	(	kIx(	(
<g/>
cembalo	cembalo	k1gNnSc1	cembalo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Keith	Keith	k1gMnSc1	Keith
Jarrett	Jarrett	k1gMnSc1	Jarrett
(	(	kIx(	(
<g/>
cembalo	cembalo	k1gNnSc1	cembalo
<g/>
,	,	kIx,	,
klavír	klavír	k1gInSc1	klavír
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Tůma	Tůma	k1gMnSc1	Tůma
(	(	kIx(	(
<g/>
clavichord	clavichord	k1gInSc1	clavichord
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Levin	Levin	k1gMnSc1	Levin
(	(	kIx(	(
<g/>
klavír	klavír	k1gInSc1	klavír
<g/>
,	,	kIx,	,
clavichord	clavichord	k1gInSc1	clavichord
<g/>
,	,	kIx,	,
varhany	varhany	k1gInPc1	varhany
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vladimir	Vladimir	k1gInSc1	Vladimir
Ashkenazy	Ashkenaz	k1gInPc1	Ashkenaz
(	(	kIx(	(
<g/>
klavír	klavír	k1gInSc1	klavír
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Dobře	dobře	k6eAd1	dobře
temperovaný	temperovaný	k2eAgInSc4d1	temperovaný
klavír	klavír	k1gInSc4	klavír
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Rozhlas	rozhlas	k1gInSc1	rozhlas
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Bach-cantatas	Bachantatas	k1gInSc1	Bach-cantatas
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
Dobře	dobře	k6eAd1	dobře
temperovaný	temperovaný	k2eAgInSc4d1	temperovaný
klavír	klavír	k1gInSc4	klavír
<g/>
:	:	kIx,	:
Tim	Tim	k?	Tim
Smith	Smith	k1gMnSc1	Smith
/	/	kIx~	/
David	David	k1gMnSc1	David
Korevaar	Korevaar	k1gMnSc1	Korevaar
</s>
</p>
<p>
<s>
Dobře	dobře	k6eAd1	dobře
temperovaný	temperovaný	k2eAgInSc4d1	temperovaný
klavír	klavír	k1gInSc4	klavír
preludium	preludium	k1gNnSc1	preludium
a	a	k8xC	a
fuga	fuga	k1gFnSc1	fuga
<g/>
,	,	kIx,	,
fuga	fuga	k1gFnSc1	fuga
-	-	kIx~	-
Korevaar	Korevaar	k1gInSc1	Korevaar
(	(	kIx(	(
<g/>
Klavír	klavír	k1gInSc1	klavír
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Goeth	Goeth	k1gInSc1	Goeth
(	(	kIx(	(
<g/>
Varhany	varhany	k1gFnPc1	varhany
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Parmentier	Parmentier	k1gInSc1	Parmentier
(	(	kIx(	(
<g/>
Cembalo	cembalo	k1gNnSc1	cembalo
<g/>
)	)	kIx)	)
</s>
</p>
