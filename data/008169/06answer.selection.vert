<s>
Dobře	dobře	k6eAd1	dobře
temperovaný	temperovaný	k2eAgInSc1d1	temperovaný
klavír	klavír	k1gInSc1	klavír
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
dvou	dva	k4xCgInPc2	dva
cyklů	cyklus	k1gInPc2	cyklus
skladeb	skladba	k1gFnPc2	skladba
Johanna	Johann	k1gMnSc4	Johann
Sebastiana	Sebastian	k1gMnSc4	Sebastian
Bacha	Bacha	k?	Bacha
psaných	psaný	k2eAgInPc6d1	psaný
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
clavier	clavier	k1gInSc4	clavier
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
=	=	kIx~	=
<g/>
klávesový	klávesový	k2eAgInSc1d1	klávesový
nástroj	nástroj	k1gInSc1	nástroj
<g/>
,	,	kIx,	,
v	v	k7c6	v
baroku	barok	k1gInSc6	barok
nejčastěji	často	k6eAd3	často
cembalo	cembalo	k1gNnSc1	cembalo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
