<s>
Manga	mango	k1gNnPc1	mango
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
漫	漫	k?	漫
<g/>
,	,	kIx,	,
hiragana	hiragana	k1gFnSc1	hiragana
<g/>
:	:	kIx,	:
ま	ま	k?	ま
<g/>
,	,	kIx,	,
katakana	katakana	k1gFnSc1	katakana
<g/>
:	:	kIx,	:
マ	マ	k?	マ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
japonské	japonský	k2eAgNnSc4d1	Japonské
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
japonský	japonský	k2eAgInSc4d1	japonský
komiks	komiks	k1gInSc4	komiks
<g/>
,	,	kIx,	,
nakreslený	nakreslený	k2eAgInSc4d1	nakreslený
stylem	styl	k1gInSc7	styl
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
vyvíjel	vyvíjet	k5eAaImAgInS	vyvíjet
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
však	však	k9	však
má	mít	k5eAaImIp3nS	mít
tento	tento	k3xDgInSc4	tento
styl	styl	k1gInSc4	styl
kresby	kresba	k1gFnSc2	kresba
mnohem	mnohem	k6eAd1	mnohem
delší	dlouhý	k2eAgFnSc4d2	delší
historii	historie	k1gFnSc4	historie
<g/>
,	,	kIx,	,
sahající	sahající	k2eAgInSc4d1	sahající
do	do	k7c2	do
11	[number]	k4	11
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Manga	mango	k1gNnSc2	mango
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
velmi	velmi	k6eAd1	velmi
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
a	a	k8xC	a
čtou	číst	k5eAaImIp3nP	číst
ji	on	k3xPp3gFnSc4	on
nejenom	nejenom	k6eAd1	nejenom
děti	dítě	k1gFnPc1	dítě
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
dospělí	dospělí	k1gMnPc1	dospělí
a	a	k8xC	a
údajně	údajně	k6eAd1	údajně
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
40	[number]	k4	40
%	%	kIx~	%
všech	všecek	k3xTgFnPc2	všecek
tištěných	tištěný	k2eAgFnPc2d1	tištěná
publikací	publikace	k1gFnPc2	publikace
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Manga	mango	k1gNnPc1	mango
se	se	k3xPyFc4	se
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
vydává	vydávat	k5eAaPmIp3nS	vydávat
nejdříve	dříve	k6eAd3	dříve
v	v	k7c6	v
časopisech	časopis	k1gInPc6	časopis
(	(	kIx(	(
<g/>
po	po	k7c6	po
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
kapitolách	kapitola	k1gFnPc6	kapitola
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgFnPc6	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
více	hodně	k6eAd2	hodně
sérií	série	k1gFnSc7	série
od	od	k7c2	od
různých	různý	k2eAgMnPc2d1	různý
autorů	autor	k1gMnPc2	autor
(	(	kIx(	(
<g/>
autor	autor	k1gMnSc1	autor
mangy	mango	k1gNnPc7	mango
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
mangaka	mangak	k1gMnSc4	mangak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
série	série	k1gFnSc1	série
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
celý	celý	k2eAgInSc1d1	celý
příběh	příběh	k1gInSc1	příběh
vydán	vydat	k5eAaPmNgInS	vydat
samostatně	samostatně	k6eAd1	samostatně
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
sešit	sešit	k1gInSc1	sešit
s	s	k7c7	s
několika	několik	k4yIc7	několik
kapitolami	kapitola	k1gFnPc7	kapitola
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgInSc3	tento
sešitu	sešit	k1gInSc3	sešit
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
říká	říkat	k5eAaImIp3nS	říkat
tankóbon	tankóbon	k1gInSc1	tankóbon
<g/>
.	.	kIx.	.
</s>
<s>
Příběhy	příběh	k1gInPc1	příběh
vyskytující	vyskytující	k2eAgInPc1d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
manze	manza	k1gFnSc6	manza
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
častým	častý	k2eAgInSc7d1	častý
námětem	námět	k1gInSc7	námět
animovaných	animovaný	k2eAgInPc2d1	animovaný
seriálů	seriál	k1gInPc2	seriál
a	a	k8xC	a
filmů	film	k1gInPc2	film
známých	známý	k2eAgInPc2d1	známý
jako	jako	k8xS	jako
anime	animat	k5eAaPmIp3nS	animat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
úspěšných	úspěšný	k2eAgNnPc2d1	úspěšné
mang	mango	k1gNnPc2	mango
také	také	k9	také
občas	občas	k6eAd1	občas
vznikají	vznikat	k5eAaImIp3nP	vznikat
konzolové	konzolový	k2eAgFnPc4d1	konzolová
či	či	k8xC	či
počítačové	počítačový	k2eAgFnPc4d1	počítačová
hry	hra	k1gFnPc4	hra
<g/>
,	,	kIx,	,
knihy	kniha	k1gFnPc4	kniha
či	či	k8xC	či
hrané	hraný	k2eAgInPc4d1	hraný
filmy	film	k1gInPc4	film
<g/>
/	/	kIx~	/
<g/>
seriály	seriál	k1gInPc4	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Styl	styl	k1gInSc1	styl
kresby	kresba	k1gFnSc2	kresba
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
mangu	mango	k1gNnSc6	mango
typický	typický	k2eAgInSc4d1	typický
<g/>
,	,	kIx,	,
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
velké	velký	k2eAgNnSc4d1	velké
oči	oko	k1gNnPc4	oko
<g/>
,	,	kIx,	,
rafinované	rafinovaný	k2eAgInPc4d1	rafinovaný
tvary	tvar	k1gInPc4	tvar
účesů	účes	k1gInPc2	účes
<g/>
,	,	kIx,	,
malý	malý	k2eAgInSc4d1	malý
a	a	k8xC	a
špičatý	špičatý	k2eAgInSc4d1	špičatý
nos	nos	k1gInSc4	nos
<g/>
,	,	kIx,	,
různé	různý	k2eAgFnPc4d1	různá
velikosti	velikost	k1gFnPc4	velikost
a	a	k8xC	a
tvary	tvar	k1gInPc4	tvar
bublin	bublina	k1gFnPc2	bublina
a	a	k8xC	a
časté	častý	k2eAgNnSc1d1	časté
používání	používání	k1gNnSc1	používání
citoslovcí	citoslovce	k1gNnPc2	citoslovce
<g/>
,	,	kIx,	,
především	především	k9	především
pak	pak	k6eAd1	pak
onomatopoí	onomatopoí	k6eAd1	onomatopoí
(	(	kIx(	(
<g/>
zvukomalebných	zvukomalebný	k2eAgNnPc2d1	zvukomalebné
slov	slovo	k1gNnPc2	slovo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Manga	mango	k1gNnSc2	mango
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
amerických	americký	k2eAgInPc2d1	americký
komiksů	komiks	k1gInPc2	komiks
<g/>
,	,	kIx,	,
tištěna	tištěn	k2eAgFnSc1d1	tištěna
jedinou	jediný	k2eAgFnSc7d1	jediná
barvou	barva	k1gFnSc7	barva
(	(	kIx(	(
<g/>
s	s	k7c7	s
občasnou	občasný	k2eAgFnSc7d1	občasná
výjimkou	výjimka	k1gFnSc7	výjimka
prvních	první	k4xOgFnPc2	první
stran	strana	k1gFnPc2	strana
knihy	kniha	k1gFnSc2	kniha
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
opačným	opačný	k2eAgInSc7d1	opačný
směrem	směr	k1gInSc7	směr
čtení	čtení	k1gNnSc2	čtení
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
zprava	zprava	k6eAd1	zprava
doleva	doleva	k6eAd1	doleva
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
světě	svět	k1gInSc6	svět
se	se	k3xPyFc4	se
výrazem	výraz	k1gInSc7	výraz
manga	mango	k1gNnSc2	mango
označují	označovat	k5eAaImIp3nP	označovat
především	především	k9	především
komiksy	komiks	k1gInPc4	komiks
vydané	vydaný	k2eAgInPc4d1	vydaný
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Komiksy	komiks	k1gInPc1	komiks
s	s	k7c7	s
podobným	podobný	k2eAgNnSc7d1	podobné
výtvarným	výtvarný	k2eAgNnSc7d1	výtvarné
pojetím	pojetí	k1gNnSc7	pojetí
ale	ale	k8xC	ale
vznikají	vznikat	k5eAaImIp3nP	vznikat
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
a	a	k8xC	a
Severní	severní	k2eAgFnSc6d1	severní
Koreji	Korea	k1gFnSc6	Korea
(	(	kIx(	(
<g/>
těmto	tento	k3xDgInPc3	tento
komiksům	komiks	k1gInPc3	komiks
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
říká	říkat	k5eAaImIp3nS	říkat
manhwa	manhwa	k1gFnSc1	manhwa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Číně	Čína	k1gFnSc6	Čína
(	(	kIx(	(
<g/>
především	především	k9	především
v	v	k7c4	v
Hong	Hong	k1gInSc4	Hong
Kongu	Kongo	k1gNnSc6	Kongo
a	a	k8xC	a
na	na	k7c6	na
Tchaj-wanu	Tchajan	k1gInSc6	Tchaj-wan
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
říká	říkat	k5eAaImIp3nS	říkat
manhua	manhua	k6eAd1	manhua
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
USA	USA	kA	USA
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
vydané	vydaný	k2eAgInPc1d1	vydaný
tituly	titul	k1gInPc1	titul
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
Amerimanga	Amerimang	k1gMnSc4	Amerimang
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
začínají	začínat	k5eAaImIp3nP	začínat
vznikat	vznikat	k5eAaImF	vznikat
první	první	k4xOgNnPc1	první
díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
napodobují	napodobovat	k5eAaImIp3nP	napodobovat
tento	tento	k3xDgInSc4	tento
styl	styl	k1gInSc4	styl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
vyšel	vyjít	k5eAaPmAgInS	vyjít
první	první	k4xOgInSc4	první
český	český	k2eAgInSc4d1	český
sborník	sborník	k1gInSc4	sborník
mangy	mango	k1gNnPc7	mango
<g/>
,	,	kIx,	,
Vějíř	vějíř	k1gInSc4	vějíř
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
následoval	následovat	k5eAaImAgInS	následovat
jeho	jeho	k3xOp3gInSc4	jeho
další	další	k2eAgInSc4d1	další
díl	díl	k1gInSc4	díl
<g/>
.	.	kIx.	.
</s>
<s>
Manga	mango	k1gNnPc1	mango
<g/>
,	,	kIx,	,
japonsky	japonsky	k6eAd1	japonsky
漫	漫	k?	漫
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
znamená	znamenat	k5eAaImIp3nS	znamenat
humorné	humorný	k2eAgInPc4d1	humorný
obrázky	obrázek	k1gInPc4	obrázek
nebo	nebo	k8xC	nebo
improvizované	improvizovaný	k2eAgFnPc4d1	improvizovaná
kresby	kresba	k1gFnPc4	kresba
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
slovo	slovo	k1gNnSc1	slovo
objevilo	objevit	k5eAaPmAgNnS	objevit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
začátku	začátek	k1gInSc2	začátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Santó	Santó	k1gMnSc2	Santó
Kjódena	Kjóden	k1gMnSc2	Kjóden
Šidži	Šidž	k1gFnSc6	Šidž
no	no	k9	no
jukikai	jukikai	k6eAd1	jukikai
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
pak	pak	k6eAd1	pak
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Manga	mango	k1gNnSc2	mango
hjakudžo	hjakudžo	k6eAd1	hjakudžo
od	od	k7c2	od
Aikawa	Aikaw	k1gInSc2	Aikaw
Minwa	Minw	k1gInSc2	Minw
(	(	kIx(	(
<g/>
1814	[number]	k4	1814
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
i	i	k8xC	i
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
známém	známý	k2eAgNnSc6d1	známé
díle	dílo	k1gNnSc6	dílo
Hokusai	Hokusa	k1gFnSc2	Hokusa
manga	mango	k1gNnSc2	mango
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
nakreslil	nakreslit	k5eAaPmAgMnS	nakreslit
Kacušika	Kacušikum	k1gNnPc4	Kacušikum
Hokusai	Hokusa	k1gFnSc2	Hokusa
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	on	k3xPp3gNnPc4	on
manga	mango	k1gNnPc4	mango
překládána	překládán	k2eAgNnPc4d1	překládáno
jako	jako	k8xC	jako
komiks	komiks	k1gInSc1	komiks
<g/>
,	,	kIx,	,
kresba	kresba	k1gFnSc1	kresba
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
karikatura	karikatura	k1gFnSc1	karikatura
<g/>
.	.	kIx.	.
</s>
<s>
Vyprávění	vyprávění	k1gNnSc1	vyprávění
příběhů	příběh	k1gInPc2	příběh
skrze	skrze	k?	skrze
obrazy	obraz	k1gInPc1	obraz
se	se	k3xPyFc4	se
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
začalo	začít	k5eAaPmAgNnS	začít
používat	používat	k5eAaImF	používat
v	v	k7c6	v
období	období	k1gNnSc6	období
Nara	Nar	k1gInSc2	Nar
(	(	kIx(	(
<g/>
710	[number]	k4	710
<g/>
–	–	k?	–
<g/>
794	[number]	k4	794
n.	n.	k?	n.
<g/>
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
rozmachem	rozmach	k1gInSc7	rozmach
štočkového	štočkový	k2eAgInSc2d1	štočkový
tisku	tisk	k1gInSc2	tisk
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
obrazy	obraz	k1gInPc1	obraz
neobsahovaly	obsahovat	k5eNaImAgFnP	obsahovat
text	text	k1gInSc4	text
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
nevyprávěly	vyprávět	k5eNaImAgFnP	vyprávět
ucelený	ucelený	k2eAgInSc4d1	ucelený
příběh	příběh	k1gInSc4	příběh
<g/>
,	,	kIx,	,
sloužily	sloužit	k5eAaImAgInP	sloužit
pouze	pouze	k6eAd1	pouze
jako	jako	k8xS	jako
pomůcka	pomůcka	k1gFnSc1	pomůcka
vypravěčům	vypravěč	k1gMnPc3	vypravěč
a	a	k8xC	a
buddhistickým	buddhistický	k2eAgMnPc3d1	buddhistický
mnichům	mnich	k1gMnPc3	mnich
při	při	k7c6	při
vyprávění	vyprávění	k1gNnSc6	vyprávění
příběhů	příběh	k1gInPc2	příběh
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pozdějšími	pozdní	k2eAgNnPc7d2	pozdější
díly	dílo	k1gNnPc7	dílo
a	a	k8xC	a
moderní	moderní	k2eAgFnSc7d1	moderní
mangou	manga	k1gFnSc7	manga
mají	mít	k5eAaImIp3nP	mít
společný	společný	k2eAgInSc4d1	společný
jen	jen	k9	jen
směr	směr	k1gInSc4	směr
čtení	čtení	k1gNnSc2	čtení
–	–	k?	–
zprava	zprava	k6eAd1	zprava
doleva	doleva	k6eAd1	doleva
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
se	se	k3xPyFc4	se
historie	historie	k1gFnSc1	historie
moderní	moderní	k2eAgFnSc2d1	moderní
mangy	mango	k1gNnPc7	mango
datuje	datovat	k5eAaImIp3nS	datovat
do	do	k7c2	do
11	[number]	k4	11
<g/>
.	.	kIx.	.
až	až	k9	až
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
pochází	pocházet	k5eAaImIp3nS	pocházet
přibližně	přibližně	k6eAd1	přibližně
1200	[number]	k4	1200
svitků	svitek	k1gInPc2	svitek
<g/>
,	,	kIx,	,
známých	známý	k2eAgInPc2d1	známý
jako	jako	k8xC	jako
Čoju	Čojum	k1gNnSc3	Čojum
džinbucu	džinbucu	k5eAaPmIp1nS	džinbucu
giga	giga	k1gFnSc1	giga
emaki	emaki	k1gNnSc2	emaki
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
údajně	údajně	k6eAd1	údajně
nakreslil	nakreslit	k5eAaPmAgMnS	nakreslit
buddhistický	buddhistický	k2eAgMnSc1d1	buddhistický
mnich	mnich	k1gMnSc1	mnich
Toba	Toba	k1gMnSc1	Toba
Sódžó	Sódžó	k1gMnSc1	Sódžó
(	(	kIx(	(
<g/>
1054	[number]	k4	1054
<g/>
–	–	k?	–
<g/>
1140	[number]	k4	1140
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svitky	svitek	k1gInPc1	svitek
vyobrazují	vyobrazovat	k5eAaImIp3nP	vyobrazovat
zvířata	zvíře	k1gNnPc4	zvíře
a	a	k8xC	a
lidi	člověk	k1gMnPc4	člověk
v	v	k7c6	v
humorných	humorný	k2eAgFnPc6d1	humorná
situacích	situace	k1gFnPc6	situace
<g/>
,	,	kIx,	,
zachovávají	zachovávat	k5eAaImIp3nP	zachovávat
směr	směr	k1gInSc4	směr
čtení	čtení	k1gNnSc2	čtení
zprava	zprava	k6eAd1	zprava
doleva	doleva	k6eAd1	doleva
a	a	k8xC	a
také	také	k9	také
nevyprávějí	vyprávět	k5eNaImIp3nP	vyprávět
ucelený	ucelený	k2eAgInSc4d1	ucelený
příběh	příběh	k1gInSc4	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Styl	styl	k1gInSc1	styl
kresby	kresba	k1gFnSc2	kresba
Šódžových	Šódžový	k2eAgInPc2d1	Šódžový
svitků	svitek	k1gInPc2	svitek
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
pozdější	pozdní	k2eAgMnPc4d2	pozdější
karikaturisty	karikaturista	k1gMnPc4	karikaturista
a	a	k8xC	a
umělce	umělec	k1gMnPc4	umělec
v	v	k7c6	v
období	období	k1gNnSc6	období
Edo	Eda	k1gMnSc5	Eda
(	(	kIx(	(
<g/>
1603	[number]	k4	1603
až	až	k9	až
1867	[number]	k4	1867
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
vydávali	vydávat	k5eAaPmAgMnP	vydávat
ilustrované	ilustrovaný	k2eAgFnPc4d1	ilustrovaná
knihy	kniha	k1gFnPc4	kniha
nazývané	nazývaný	k2eAgFnPc4d1	nazývaná
toba-e	toba	k1gFnSc7	toba-e
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
鳥	鳥	k?	鳥
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obsahem	obsah	k1gInSc7	obsah
knih	kniha	k1gFnPc2	kniha
toba-e	toba	k6eAd1	toba-e
byly	být	k5eAaImAgFnP	být
krátké	krátký	k2eAgInPc4d1	krátký
příběhy	příběh	k1gInPc4	příběh
vyobrazující	vyobrazující	k2eAgInPc4d1	vyobrazující
zvířata	zvíře	k1gNnPc4	zvíře
imitující	imitující	k2eAgFnSc4d1	imitující
lidskou	lidský	k2eAgFnSc4d1	lidská
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
velmi	velmi	k6eAd1	velmi
výrazným	výrazný	k2eAgInSc7d1	výrazný
žánrem	žánr	k1gInSc7	žánr
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
bylo	být	k5eAaImAgNnS	být
ukijo-e	ukijo	k6eAd1	ukijo-e
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
浮	浮	k?	浮
<g/>
)	)	kIx)	)
neboli	neboli	k8xC	neboli
obrazy	obraz	k1gInPc1	obraz
z	z	k7c2	z
plovoucího	plovoucí	k2eAgInSc2d1	plovoucí
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
oblíbené	oblíbený	k2eAgInPc4d1	oblíbený
náměty	námět	k1gInPc4	námět
ukijo-e	ukijo	k6eAd1	ukijo-e
patřily	patřit	k5eAaImAgFnP	patřit
krásné	krásný	k2eAgFnPc1d1	krásná
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
zápasníci	zápasník	k1gMnPc1	zápasník
sumó	sumó	k?	sumó
<g/>
,	,	kIx,	,
historické	historický	k2eAgFnPc1d1	historická
scenérie	scenérie	k1gFnPc1	scenérie
a	a	k8xC	a
pověsti	pověst	k1gFnPc1	pověst
<g/>
,	,	kIx,	,
zvířata	zvíře	k1gNnPc1	zvíře
a	a	k8xC	a
příroda	příroda	k1gFnSc1	příroda
a	a	k8xC	a
mnohdy	mnohdy	k6eAd1	mnohdy
i	i	k9	i
erotické	erotický	k2eAgFnSc2d1	erotická
scény	scéna	k1gFnSc2	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Frederik	Frederika	k1gFnPc2	Frederika
a	a	k8xC	a
Petersen	Petersna	k1gFnPc2	Petersna
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
moderní	moderní	k2eAgNnPc1d1	moderní
manga	mango	k1gNnPc1	mango
vyvinula	vyvinout	k5eAaPmAgNnP	vyvinout
z	z	k7c2	z
toba-e	toba-	k1gInSc2	toba-
a	a	k8xC	a
ukijo-	ukijo-	k?	ukijo-
<g/>
e.	e.	k?	e.
Jiní	jiný	k2eAgMnPc1d1	jiný
však	však	k8xC	však
tato	tento	k3xDgNnPc4	tento
díla	dílo	k1gNnPc4	dílo
za	za	k7c4	za
přímé	přímý	k2eAgInPc4d1	přímý
předchůdce	předchůdce	k1gMnSc2	předchůdce
mangy	mango	k1gNnPc7	mango
nepovažují	považovat	k5eNaImIp3nP	považovat
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
manga	mango	k1gNnSc2	mango
se	se	k3xPyFc4	se
v	v	k7c6	v
Japonském	japonský	k2eAgInSc6d1	japonský
jazyce	jazyk	k1gInSc6	jazyk
začalo	začít	k5eAaPmAgNnS	začít
objevovat	objevovat	k5eAaImF	objevovat
až	až	k9	až
koncem	koncem	k7c2	koncem
období	období	k1gNnSc2	období
Eda	Eda	k1gMnSc1	Eda
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
jej	on	k3xPp3gMnSc4	on
použil	použít	k5eAaPmAgMnS	použít
Santó	Santó	k1gMnSc1	Santó
Kjóden	Kjódna	k1gFnPc2	Kjódna
(	(	kIx(	(
<g/>
1761	[number]	k4	1761
<g/>
–	–	k?	–
<g/>
1816	[number]	k4	1816
<g/>
)	)	kIx)	)
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Šidži	Šidž	k1gFnSc6	Šidž
no	no	k9	no
jukikai	jukikai	k6eAd1	jukikai
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
jej	on	k3xPp3gMnSc4	on
zpopularizoval	zpopularizovat	k5eAaPmAgMnS	zpopularizovat
umělec	umělec	k1gMnSc1	umělec
Kacušika	Kacušik	k1gMnSc2	Kacušik
Hokusai	Hokusai	k1gNnSc2	Hokusai
(	(	kIx(	(
<g/>
1760	[number]	k4	1760
<g/>
–	–	k?	–
<g/>
1849	[number]	k4	1849
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1814	[number]	k4	1814
až	až	k9	až
1849	[number]	k4	1849
vydal	vydat	k5eAaPmAgInS	vydat
své	své	k1gNnSc4	své
13	[number]	k4	13
<g/>
svazkové	svazkový	k2eAgNnSc4d1	svazkové
dílo	dílo	k1gNnSc4	dílo
(	(	kIx(	(
<g/>
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
pak	pak	k6eAd1	pak
vyšly	vyjít	k5eAaPmAgInP	vyjít
ještě	ještě	k6eAd1	ještě
další	další	k2eAgInPc1d1	další
dva	dva	k4xCgInPc1	dva
svazky	svazek	k1gInPc1	svazek
<g/>
)	)	kIx)	)
Hokusai	Hokusai	k1gNnSc1	Hokusai
manga	mango	k1gNnSc2	mango
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
styl	styl	k1gInSc4	styl
kresby	kresba	k1gFnSc2	kresba
a	a	k8xC	a
humoru	humor	k1gInSc2	humor
pozdějších	pozdní	k2eAgNnPc2d2	pozdější
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Hokusai	Hokusai	k1gNnSc1	Hokusai
manga	mango	k1gNnSc2	mango
nevypráví	vyprávět	k5eNaImIp3nS	vyprávět
ucelený	ucelený	k2eAgInSc4d1	ucelený
příběh	příběh	k1gInSc4	příběh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mělo	mít	k5eAaImAgNnS	mít
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
reference	reference	k1gFnPc4	reference
pro	pro	k7c4	pro
Hokusaiovy	Hokusaiův	k2eAgMnPc4d1	Hokusaiův
žáky	žák	k1gMnPc4	žák
a	a	k8xC	a
pozdější	pozdní	k2eAgMnPc4d2	pozdější
umělce	umělec	k1gMnPc4	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
konce	konec	k1gInSc2	konec
období	období	k1gNnSc2	období
Eda	Eda	k1gMnSc1	Eda
pocházejí	pocházet	k5eAaImIp3nP	pocházet
i	i	k9	i
první	první	k4xOgInPc4	první
kreslené	kreslený	k2eAgInPc4d1	kreslený
erotické	erotický	k2eAgInPc4d1	erotický
a	a	k8xC	a
komické	komický	k2eAgInPc4d1	komický
příběhy	příběh	k1gInPc4	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
známější	známý	k2eAgFnSc7d2	známější
a	a	k8xC	a
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	době	k6eAd1	době
populárnější	populární	k2eAgNnPc1d2	populárnější
erotická	erotický	k2eAgNnPc1d1	erotické
díla	dílo	k1gNnPc1	dílo
patří	patřit	k5eAaImIp3nP	patřit
například	například	k6eAd1	například
Fúrjú	Fúrjú	k1gMnPc1	Fúrjú
enšoku	enšok	k1gInSc2	enšok
Maneemon	Maneemon	k1gInSc1	Maneemon
od	od	k7c2	od
Suzuki	suzuki	k1gNnSc2	suzuki
Harunobu	Harunoba	k1gFnSc4	Harunoba
(	(	kIx(	(
<g/>
1724	[number]	k4	1724
<g/>
–	–	k?	–
<g/>
1770	[number]	k4	1770
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
příběhu	příběh	k1gInSc2	příběh
<g/>
,	,	kIx,	,
Maneemon	Maneemon	k1gMnSc1	Maneemon
<g/>
,	,	kIx,	,
dokázal	dokázat	k5eAaPmAgMnS	dokázat
zmenšit	zmenšit	k5eAaPmF	zmenšit
svou	svůj	k3xOyFgFnSc4	svůj
výšku	výška	k1gFnSc4	výška
a	a	k8xC	a
mohl	moct	k5eAaImAgInS	moct
tak	tak	k6eAd1	tak
sledovat	sledovat	k5eAaImF	sledovat
sexuální	sexuální	k2eAgFnPc1d1	sexuální
hrátky	hrátky	k1gFnPc1	hrátky
cizích	cizí	k2eAgMnPc2d1	cizí
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
scény	scéna	k1gFnPc1	scéna
příběhu	příběh	k1gInSc2	příběh
byly	být	k5eAaImAgFnP	být
propracované	propracovaný	k2eAgFnPc1d1	propracovaná
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
přes	přes	k7c4	přes
celou	celý	k2eAgFnSc4d1	celá
jednu	jeden	k4xCgFnSc4	jeden
nebo	nebo	k8xC	nebo
dvě	dva	k4xCgFnPc4	dva
strany	strana	k1gFnPc4	strana
a	a	k8xC	a
navzájem	navzájem	k6eAd1	navzájem
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
navazovaly	navazovat	k5eAaImAgInP	navazovat
jen	jen	k9	jen
nepřímo	přímo	k6eNd1	přímo
<g/>
.	.	kIx.	.
</s>
<s>
Kjóden	Kjódno	k1gNnPc2	Kjódno
<g/>
,	,	kIx,	,
Hanurobu	Hanuroba	k1gFnSc4	Hanuroba
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
autoři	autor	k1gMnPc1	autor
publikovali	publikovat	k5eAaBmAgMnP	publikovat
svá	svůj	k3xOyFgNnPc4	svůj
díla	dílo	k1gNnPc4	dílo
na	na	k7c4	na
pokračování	pokračování	k1gNnSc4	pokračování
v	v	k7c6	v
knihách	kniha	k1gFnPc6	kniha
nazývaných	nazývaný	k2eAgMnPc2d1	nazývaný
kibjóši	kibjóše	k1gFnSc4	kibjóše
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
žluté	žlutý	k2eAgFnPc4d1	žlutá
knihy	kniha	k1gFnPc4	kniha
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
barvy	barva	k1gFnSc2	barva
inkoustu	inkoust	k1gInSc2	inkoust
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnPc3	který
byla	být	k5eAaImAgFnS	být
často	často	k6eAd1	často
tištěna	tištěn	k2eAgFnSc1d1	tištěna
obálka	obálka	k1gFnSc1	obálka
knihy	kniha	k1gFnSc2	kniha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
však	však	k9	však
kibjóši	kibjósat	k5eAaPmIp1nSwK	kibjósat
vlivem	vliv	k1gInSc7	vliv
cenzury	cenzura	k1gFnSc2	cenzura
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
státu	stát	k1gInSc2	stát
úplně	úplně	k6eAd1	úplně
vymizely	vymizet	k5eAaPmAgFnP	vymizet
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
jej	on	k3xPp3gNnSc4	on
někteří	některý	k3yIgMnPc1	některý
také	také	k9	také
nepovažují	považovat	k5eNaImIp3nP	považovat
za	za	k7c4	za
přímé	přímý	k2eAgInPc4d1	přímý
předky	předek	k1gInPc4	předek
mangy	mango	k1gNnPc7	mango
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
opravdový	opravdový	k2eAgInSc4d1	opravdový
vznik	vznik	k1gInSc4	vznik
mangy	mango	k1gNnPc7	mango
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
až	až	k9	až
období	období	k1gNnSc1	období
před	před	k7c7	před
a	a	k8xC	a
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
začátku	začátek	k1gInSc2	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vycházely	vycházet	k5eAaImAgInP	vycházet
levné	levný	k2eAgInPc1d1	levný
časopisy	časopis	k1gInPc1	časopis
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
obsahovaly	obsahovat	k5eAaImAgFnP	obsahovat
jednoduché	jednoduchý	k2eAgInPc4d1	jednoduchý
kreslené	kreslený	k2eAgInPc4d1	kreslený
příběhy	příběh	k1gInPc4	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
tato	tento	k3xDgNnPc1	tento
periodika	periodikum	k1gNnPc1	periodikum
zaměřila	zaměřit	k5eAaPmAgNnP	zaměřit
na	na	k7c4	na
užší	úzký	k2eAgFnPc4d2	užší
skupiny	skupina	k1gFnPc4	skupina
čtenářů	čtenář	k1gMnPc2	čtenář
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
kvůli	kvůli	k7c3	kvůli
rozdělení	rozdělení	k1gNnSc3	rozdělení
škol	škola	k1gFnPc2	škola
na	na	k7c6	na
dívčí	dívčí	k2eAgFnSc6d1	dívčí
a	a	k8xC	a
chlapecké	chlapecký	k2eAgFnSc6d1	chlapecká
začaly	začít	k5eAaPmAgInP	začít
vznikat	vznikat	k5eAaImF	vznikat
tituly	titul	k1gInPc1	titul
určené	určený	k2eAgInPc1d1	určený
pro	pro	k7c4	pro
chlapce	chlapec	k1gMnPc4	chlapec
a	a	k8xC	a
dívky	dívka	k1gFnPc4	dívka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
výhradně	výhradně	k6eAd1	výhradně
dívčí	dívčí	k2eAgInSc1d1	dívčí
časopis	časopis	k1gInSc1	časopis
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
a	a	k8xC	a
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
se	se	k3xPyFc4	se
Šódžo	Šódžo	k6eAd1	Šódžo
sekai	sekai	k6eAd1	sekai
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
Svět	svět	k1gInSc1	svět
děvčat	děvče	k1gNnPc2	děvče
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgFnSc4d2	veliký
popularitu	popularita	k1gFnSc4	popularita
a	a	k8xC	a
vývoj	vývoj	k1gInSc4	vývoj
však	však	k8xC	však
manga	mango	k1gNnPc4	mango
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
až	až	k6eAd1	až
v	v	k7c6	v
době	doba	k1gFnSc6	doba
okupace	okupace	k1gFnSc2	okupace
Japonska	Japonsko	k1gNnSc2	Japonsko
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
měnila	měnit	k5eAaImAgFnS	měnit
japonská	japonský	k2eAgFnSc1d1	japonská
ekonomika	ekonomika	k1gFnSc1	ekonomika
i	i	k8xC	i
politická	politický	k2eAgFnSc1d1	politická
situace	situace	k1gFnSc1	situace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
veškeré	veškerý	k3xTgNnSc4	veškerý
nově	nově	k6eAd1	nově
vznikající	vznikající	k2eAgNnSc1d1	vznikající
díla	dílo	k1gNnPc1	dílo
se	se	k3xPyFc4	se
vztahovala	vztahovat	k5eAaImAgFnS	vztahovat
přísná	přísný	k2eAgFnSc1d1	přísná
cenzura	cenzura	k1gFnSc1	cenzura
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zakazovala	zakazovat	k5eAaImAgFnS	zakazovat
jakákoliv	jakýkoliv	k3yIgNnPc4	jakýkoliv
válečná	válečný	k2eAgNnPc4d1	válečné
témata	téma	k1gNnPc4	téma
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
takto	takto	k6eAd1	takto
nepříznivou	příznivý	k2eNgFnSc4d1	nepříznivá
situaci	situace	k1gFnSc4	situace
začaly	začít	k5eAaPmAgFnP	začít
vznikat	vznikat	k5eAaImF	vznikat
nové	nový	k2eAgInPc4d1	nový
časopisy	časopis	k1gInPc4	časopis
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
se	se	k3xPyFc4	se
objevovaly	objevovat	k5eAaImAgInP	objevovat
jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
kreslené	kreslený	k2eAgInPc1d1	kreslený
příběhy	příběh	k1gInPc1	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
časopisy	časopis	k1gInPc1	časopis
se	se	k3xPyFc4	se
tiskly	tisknout	k5eAaImAgInP	tisknout
na	na	k7c4	na
levný	levný	k2eAgInSc4d1	levný
papír	papír	k1gInSc4	papír
a	a	k8xC	a
titulní	titulní	k2eAgFnPc4d1	titulní
stránky	stránka	k1gFnPc4	stránka
byly	být	k5eAaImAgFnP	být
jasně	jasně	k6eAd1	jasně
červené	červený	k2eAgFnPc1d1	červená
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
říkalo	říkat	k5eAaImAgNnS	říkat
akahon	akahon	k1gNnSc1	akahon
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
červená	červený	k2eAgFnSc1d1	červená
kniha	kniha	k1gFnSc1	kniha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
umělcem	umělec	k1gMnSc7	umělec
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byl	být	k5eAaImAgInS	být
Osamu	Osama	k1gFnSc4	Osama
Tezuka	Tezuk	k1gMnSc2	Tezuk
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
práce	práce	k1gFnSc1	práce
výrazně	výrazně	k6eAd1	výrazně
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
styl	styl	k1gInSc4	styl
kresby	kresba	k1gFnSc2	kresba
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
umělců	umělec	k1gMnPc2	umělec
a	a	k8xC	a
některé	některý	k3yIgInPc4	některý
prvky	prvek	k1gInPc4	prvek
se	se	k3xPyFc4	se
v	v	k7c6	v
manze	manza	k1gFnSc6	manza
objevují	objevovat	k5eAaImIp3nP	objevovat
i	i	k9	i
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
ilustrace	ilustrace	k1gFnPc1	ilustrace
kreslily	kreslit	k5eAaImAgFnP	kreslit
bez	bez	k7c2	bez
perspektivy	perspektiva	k1gFnSc2	perspektiva
<g/>
,	,	kIx,	,
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
třetí	třetí	k4xOgFnSc2	třetí
osoby	osoba	k1gFnSc2	osoba
a	a	k8xC	a
úhel	úhel	k1gInSc4	úhel
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
jakým	jaký	k3yQgInSc7	jaký
čtenář	čtenář	k1gMnSc1	čtenář
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
obraz	obraz	k1gInSc4	obraz
se	se	k3xPyFc4	se
neměnil	měnit	k5eNaImAgMnS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
Tezuka	Tezuk	k1gMnSc4	Tezuk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
ve	v	k7c6	v
filmu	film	k1gInSc6	film
a	a	k8xC	a
především	především	k9	především
v	v	k7c4	v
Disneyho	Disney	k1gMnSc4	Disney
animacích	animace	k1gFnPc6	animace
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
využívat	využívat	k5eAaPmF	využívat
moderních	moderní	k2eAgInPc2d1	moderní
vypravěčských	vypravěčský	k2eAgInPc2d1	vypravěčský
postupů	postup	k1gInPc2	postup
a	a	k8xC	a
aktivní	aktivní	k2eAgFnSc4d1	aktivní
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
"	"	kIx"	"
<g/>
kamerou	kamera	k1gFnSc7	kamera
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
první	první	k4xOgNnSc1	první
dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
výrazněji	výrazně	k6eAd2	výrazně
proslavilo	proslavit	k5eAaPmAgNnS	proslavit
<g/>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Šin	Šin	k1gMnSc2	Šin
Takaradžima	Takaradžim	k1gMnSc2	Takaradžim
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Nový	nový	k2eAgInSc1d1	nový
ostrov	ostrov	k1gInSc1	ostrov
pokladů	poklad	k1gInPc2	poklad
<g/>
)	)	kIx)	)
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
pak	pak	k6eAd1	pak
Tecuwan	Tecuwan	k1gInSc1	Tecuwan
Atomu	atom	k1gInSc2	atom
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
鉄	鉄	k?	鉄
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xS	jako
Astro	astra	k1gFnSc5	astra
Boy	boa	k1gFnPc5	boa
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
významnou	významný	k2eAgFnSc7d1	významná
kreslířkou	kreslířka	k1gFnSc7	kreslířka
byla	být	k5eAaImAgFnS	být
Mačiko	Mačika	k1gFnSc5	Mačika
Hasegawa	Hasegawa	k1gFnSc1	Hasegawa
<g/>
,	,	kIx,	,
známá	známá	k1gFnSc1	známá
svou	svůj	k3xOyFgFnSc7	svůj
sérií	série	k1gFnSc7	série
Sazae-san	Sazaeana	k1gFnPc2	Sazae-sana
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
poprvé	poprvé	k6eAd1	poprvé
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
a	a	k8xC	a
vychází	vycházet	k5eAaImIp3nS	vycházet
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
začala	začít	k5eAaPmAgFnS	začít
manga	mango	k1gNnSc2	mango
získávat	získávat	k5eAaImF	získávat
na	na	k7c6	na
popularitě	popularita	k1gFnSc6	popularita
a	a	k8xC	a
trh	trh	k1gInSc4	trh
s	s	k7c7	s
komiksovými	komiksový	k2eAgInPc7d1	komiksový
příběhy	příběh	k1gInPc7	příběh
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
rozšiřovat	rozšiřovat	k5eAaImF	rozšiřovat
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
generace	generace	k1gFnPc1	generace
mladých	mladý	k2eAgMnPc2d1	mladý
čtenářů	čtenář	k1gMnPc2	čtenář
mangy	mango	k1gNnPc7	mango
začaly	začít	k5eAaPmAgFnP	začít
dospívat	dospívat	k5eAaImF	dospívat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
svých	svůj	k3xOyFgMnPc2	svůj
předchůdců	předchůdce	k1gMnPc2	předchůdce
komiksové	komiksový	k2eAgInPc4d1	komiksový
příběhy	příběh	k1gInPc4	příběh
četli	číst	k5eAaImAgMnP	číst
i	i	k9	i
nadále	nadále	k6eAd1	nadále
<g/>
.	.	kIx.	.
</s>
<s>
Začaly	začít	k5eAaPmAgFnP	začít
tak	tak	k6eAd1	tak
vznikat	vznikat	k5eAaImF	vznikat
první	první	k4xOgInPc4	první
příběhy	příběh	k1gInPc4	příběh
pro	pro	k7c4	pro
starší	starý	k2eAgFnPc4d2	starší
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
i	i	k9	i
pro	pro	k7c4	pro
dospělé	dospělí	k1gMnPc4	dospělí
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
umělců	umělec	k1gMnPc2	umělec
a	a	k8xC	a
autorů	autor	k1gMnPc2	autor
také	také	k6eAd1	také
navázalo	navázat	k5eAaPmAgNnS	navázat
na	na	k7c4	na
úspěšný	úspěšný	k2eAgInSc4d1	úspěšný
Tezukův	Tezukův	k2eAgInSc4d1	Tezukův
styl	styl	k1gInSc4	styl
a	a	k8xC	a
následně	následně	k6eAd1	následně
jej	on	k3xPp3gMnSc4	on
zdokonalovali	zdokonalovat	k5eAaImAgMnP	zdokonalovat
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
starších	starý	k2eAgMnPc2d2	starší
čtenářů	čtenář	k1gMnPc2	čtenář
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
i	i	k9	i
nové	nový	k2eAgInPc1d1	nový
žánry	žánr	k1gInPc1	žánr
a	a	k8xC	a
příběhy	příběh	k1gInPc1	příběh
se	se	k3xPyFc4	se
stávaly	stávat	k5eAaImAgInP	stávat
složitější	složitý	k2eAgInPc1d2	složitější
a	a	k8xC	a
delší	dlouhý	k2eAgInPc1d2	delší
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
příběhy	příběh	k1gInPc1	příběh
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
začaly	začít	k5eAaPmAgFnP	začít
více	hodně	k6eAd2	hodně
zaměřovat	zaměřovat	k5eAaImF	zaměřovat
na	na	k7c4	na
užší	úzký	k2eAgFnPc4d2	užší
skupiny	skupina	k1gFnPc4	skupina
čtenářů	čtenář	k1gMnPc2	čtenář
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
pochází	pocházet	k5eAaImIp3nS	pocházet
i	i	k9	i
dva	dva	k4xCgInPc4	dva
hlavní	hlavní	k2eAgInPc4d1	hlavní
směry	směr	k1gInPc4	směr
–	–	k?	–
šónen	šónen	k1gInSc4	šónen
<g/>
,	,	kIx,	,
zaměřen	zaměřit	k5eAaPmNgInS	zaměřit
svým	svůj	k3xOyFgInSc7	svůj
obsahem	obsah	k1gInSc7	obsah
na	na	k7c4	na
chlapce	chlapec	k1gMnPc4	chlapec
<g/>
,	,	kIx,	,
a	a	k8xC	a
šódžo	šódžo	k1gMnSc1	šódžo
<g/>
,	,	kIx,	,
určen	určen	k2eAgMnSc1d1	určen
primárně	primárně	k6eAd1	primárně
pro	pro	k7c4	pro
dívky	dívka	k1gFnPc4	dívka
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
však	však	k9	však
dívčí	dívčí	k2eAgNnSc1d1	dívčí
šódžo	šódžo	k1gNnSc1	šódžo
mangy	mango	k1gNnPc7	mango
nezískaly	získat	k5eNaPmAgInP	získat
na	na	k7c6	na
popularitě	popularita	k1gFnSc6	popularita
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
tvůrců	tvůrce	k1gMnPc2	tvůrce
a	a	k8xC	a
umělců	umělec	k1gMnPc2	umělec
byli	být	k5eAaImAgMnP	být
muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
svými	svůj	k3xOyFgInPc7	svůj
příběhy	příběh	k1gInPc7	příběh
nedokázali	dokázat	k5eNaPmAgMnP	dokázat
mladé	mladý	k2eAgFnPc4d1	mladá
dívky	dívka	k1gFnPc4	dívka
a	a	k8xC	a
ženy	žena	k1gFnPc4	žena
zaujmout	zaujmout	k5eAaPmF	zaujmout
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
skupina	skupina	k1gFnSc1	skupina
Nidžújonen	Nidžújonno	k1gNnPc2	Nidžújonno
Gumi	Gum	k1gFnSc2	Gum
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Skupina	skupina	k1gFnSc1	skupina
roku	rok	k1gInSc2	rok
24	[number]	k4	24
<g/>
;	;	kIx,	;
většina	většina	k1gFnSc1	většina
autorek	autorka	k1gFnPc2	autorka
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
24	[number]	k4	24
období	období	k1gNnSc4	období
Šówa	Šówum	k1gNnSc2	Šówum
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
působily	působit	k5eAaImAgFnP	působit
mladé	mladý	k2eAgFnPc1d1	mladá
autorky	autorka	k1gFnPc1	autorka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
se	se	k3xPyFc4	se
zaměřily	zaměřit	k5eAaPmAgFnP	zaměřit
se	se	k3xPyFc4	se
na	na	k7c4	na
žánr	žánr	k1gInSc4	žánr
šódžo	šódžo	k6eAd1	šódžo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
období	období	k1gNnSc6	období
(	(	kIx(	(
<g/>
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
dodnes	dodnes	k6eAd1	dodnes
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
žánr	žánr	k1gInSc4	žánr
šódžo	šódžo	k6eAd1	šódžo
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
vlastní	vlastní	k2eAgInSc4d1	vlastní
unikátní	unikátní	k2eAgInSc4d1	unikátní
vzhled	vzhled	k1gInSc4	vzhled
i	i	k8xC	i
podžánry	podžánra	k1gFnPc4	podžánra
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ty	ten	k3xDgInPc4	ten
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
šódžo	šódžo	k6eAd1	šódžo
manga	mango	k1gNnPc4	mango
zaměřená	zaměřený	k2eAgNnPc4d1	zaměřené
na	na	k7c4	na
dospělé	dospělý	k2eAgFnPc4d1	dospělá
ženy	žena	k1gFnPc4	žena
–	–	k?	–
džosei	džose	k1gFnSc2	džose
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
女	女	k?	女
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
redísu	redís	k1gMnSc3	redís
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
レ	レ	k?	レ
<g/>
)	)	kIx)	)
či	či	k8xC	či
redikomi	rediko	k1gFnPc7	rediko
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
レ	レ	k?	レ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
příkladem	příklad	k1gInSc7	příklad
podžánru	podžánr	k1gInSc2	podžánr
šódžo	šódžo	k6eAd1	šódžo
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
mahó	mahó	k?	mahó
šódžo	šódžo	k1gMnSc1	šódžo
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
魔	魔	k?	魔
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
čarodějné	čarodějný	k2eAgFnPc1d1	čarodějná
dívky	dívka	k1gFnPc1	dívka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgNnPc6	který
jsou	být	k5eAaImIp3nP	být
hlavní	hlavní	k2eAgFnPc4d1	hlavní
hrdinky	hrdinka	k1gFnPc4	hrdinka
obdařeny	obdařit	k5eAaPmNgFnP	obdařit
magickou	magický	k2eAgFnSc7d1	magická
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Manga	mango	k1gNnPc4	mango
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
na	na	k7c4	na
chlapce	chlapec	k1gMnPc4	chlapec
a	a	k8xC	a
muže	muž	k1gMnPc4	muž
se	se	k3xPyFc4	se
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Příběhy	příběh	k1gInPc1	příběh
označené	označený	k2eAgInPc1d1	označený
jako	jako	k8xC	jako
šónen	šónen	k1gInSc1	šónen
se	se	k3xPyFc4	se
zaměřily	zaměřit	k5eAaPmAgInP	zaměřit
na	na	k7c4	na
mladé	mladý	k2eAgMnPc4d1	mladý
chlapce	chlapec	k1gMnPc4	chlapec
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
do	do	k7c2	do
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
seinen	seinen	k1gInSc1	seinen
mangy	mango	k1gNnPc7	mango
byly	být	k5eAaImAgInP	být
určeny	určen	k2eAgInPc1d1	určen
pro	pro	k7c4	pro
dospělé	dospělý	k2eAgMnPc4d1	dospělý
muže	muž	k1gMnPc4	muž
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
podžánry	podžánr	k1gInPc1	podžánr
šónen	šónen	k2eAgInSc4d1	šónen
mangy	mango	k1gNnPc7	mango
často	často	k6eAd1	často
zobrazovaly	zobrazovat	k5eAaImAgInP	zobrazovat
příběhy	příběh	k1gInPc4	příběh
superhrdinů	superhrdin	k1gMnPc2	superhrdin
<g/>
,	,	kIx,	,
akční	akční	k2eAgInSc4d1	akční
<g/>
,	,	kIx,	,
humorné	humorný	k2eAgInPc4d1	humorný
i	i	k8xC	i
hororové	hororový	k2eAgInPc4d1	hororový
příběhy	příběh	k1gInPc4	příběh
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
i	i	k9	i
mírně	mírně	k6eAd1	mírně
explicitní	explicitní	k2eAgNnSc1d1	explicitní
<g/>
,	,	kIx,	,
či	či	k8xC	či
přímo	přímo	k6eAd1	přímo
sexuální	sexuální	k2eAgNnPc1d1	sexuální
témata	téma	k1gNnPc1	téma
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
vývoj	vývoj	k1gInSc1	vývoj
trhu	trh	k1gInSc2	trh
s	s	k7c7	s
mangou	manga	k1gFnSc7	manga
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
s	s	k7c7	s
rozšířením	rozšíření	k1gNnSc7	rozšíření
levného	levný	k2eAgInSc2d1	levný
a	a	k8xC	a
dostupného	dostupný	k2eAgInSc2d1	dostupný
tisku	tisk	k1gInSc2	tisk
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Začala	začít	k5eAaPmAgFnS	začít
tak	tak	k6eAd1	tak
vznikat	vznikat	k5eAaImF	vznikat
malá	malý	k2eAgFnSc1d1	malá
<g/>
,	,	kIx,	,
nezávislá	závislý	k2eNgNnPc1d1	nezávislé
vydavatelství	vydavatelství	k1gNnPc1	vydavatelství
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
umožnila	umožnit	k5eAaPmAgFnS	umožnit
amatérům	amatér	k1gMnPc3	amatér
publikovat	publikovat	k5eAaBmF	publikovat
jejich	jejich	k3xOp3gFnSc4	jejich
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Té	ten	k3xDgFnSc3	ten
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
začalo	začít	k5eAaPmAgNnS	začít
říkat	říkat	k5eAaImF	říkat
dódžinši	dódžinše	k1gFnSc4	dódžinše
a	a	k8xC	a
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
původní	původní	k2eAgFnSc4d1	původní
autorskou	autorský	k2eAgFnSc4d1	autorská
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
velká	velký	k2eAgNnPc1d1	velké
nakladatelství	nakladatelství	k1gNnPc1	nakladatelství
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
na	na	k7c4	na
mainstream	mainstream	k1gInSc4	mainstream
vydávala	vydávat	k5eAaPmAgFnS	vydávat
známé	známý	k2eAgInPc4d1	známý
příběhy	příběh	k1gInPc4	příběh
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc4	tento
nový	nový	k2eAgInSc4d1	nový
trh	trh	k1gInSc4	trh
s	s	k7c7	s
amatérskou	amatérský	k2eAgFnSc7d1	amatérská
tvorbou	tvorba	k1gFnSc7	tvorba
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
experimentů	experiment	k1gInPc2	experiment
a	a	k8xC	a
nových	nový	k2eAgInPc2d1	nový
žánrů	žánr	k1gInPc2	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Obrovský	obrovský	k2eAgInSc1d1	obrovský
zájem	zájem	k1gInSc1	zájem
o	o	k7c6	o
dódžinši	dódžinše	k1gFnSc6	dódžinše
dal	dát	k5eAaPmAgMnS	dát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
vzniknout	vzniknout	k5eAaPmF	vzniknout
setkání	setkání	k1gNnSc1	setkání
(	(	kIx(	(
<g/>
conu	conu	k6eAd1	conu
<g/>
)	)	kIx)	)
známé	známý	k2eAgNnSc1d1	známé
jako	jako	k8xC	jako
Comic	Comic	k1gMnSc1	Comic
Market	market	k1gInSc4	market
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
schází	scházet	k5eAaImIp3nS	scházet
skupiny	skupina	k1gFnPc4	skupina
amatérů	amatér	k1gMnPc2	amatér
i	i	k8xC	i
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
publikující	publikující	k2eAgInSc1d1	publikující
mangu	mango	k1gNnSc3	mango
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
setkání	setkání	k1gNnSc1	setkání
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
místem	místo	k1gNnSc7	místo
pro	pro	k7c4	pro
prodej	prodej	k1gInSc4	prodej
a	a	k8xC	a
šíření	šíření	k1gNnSc4	šíření
dódžinši	dódžinsat	k5eAaPmIp1nSwK	dódžinsat
a	a	k8xC	a
během	během	k7c2	během
následujících	následující	k2eAgNnPc2d1	následující
10	[number]	k4	10
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
rozrostlo	rozrůst	k5eAaPmAgNnS	rozrůst
co	co	k9	co
do	do	k7c2	do
počtu	počet	k1gInSc2	počet
autorů	autor	k1gMnPc2	autor
i	i	k8xC	i
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
boom	boom	k1gInSc1	boom
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1986	[number]	k4	1986
až	až	k9	až
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Comic	Comice	k1gFnPc2	Comice
Market	market	k1gInSc1	market
začal	začít	k5eAaPmAgInS	začít
pořádat	pořádat	k5eAaImF	pořádat
dvakrát	dvakrát	k6eAd1	dvakrát
do	do	k7c2	do
roka	rok	k1gInSc2	rok
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
největší	veliký	k2eAgFnSc7d3	veliký
akcí	akce	k1gFnSc7	akce
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
již	již	k6eAd1	již
58	[number]	k4	58
<g/>
.	.	kIx.	.
</s>
<s>
Comiket	Comiket	k1gInSc1	Comiket
a	a	k8xC	a
během	během	k7c2	během
dvou	dva	k4xCgInPc2	dva
dní	den	k1gInPc2	den
jej	on	k3xPp3gMnSc4	on
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
800	[number]	k4	800
000	[number]	k4	000
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
také	také	k9	také
nové	nový	k2eAgInPc4d1	nový
časopisy	časopis	k1gInPc4	časopis
zaměřující	zaměřující	k2eAgInPc4d1	zaměřující
se	se	k3xPyFc4	se
na	na	k7c4	na
amatérskou	amatérský	k2eAgFnSc4d1	amatérská
tvorbu	tvorba	k1gFnSc4	tvorba
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
June	jun	k1gMnSc5	jun
či	či	k8xC	či
Manga	mango	k1gNnPc1	mango
kissatengai	kissatengai	k1gNnPc2	kissatengai
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
Puff	puff	k1gInSc1	puff
nebo	nebo	k8xC	nebo
Comic	Comic	k1gMnSc1	Comic
Box	box	k1gInSc1	box
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
malé	malý	k2eAgFnPc1d1	malá
společnosti	společnost	k1gFnPc1	společnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vydávaly	vydávat	k5eAaImAgFnP	vydávat
dódžinši	dódžinše	k1gFnSc4	dódžinše
<g/>
.	.	kIx.	.
</s>
<s>
Amatérská	amatérský	k2eAgFnSc1d1	amatérská
tvorba	tvorba	k1gFnSc1	tvorba
byla	být	k5eAaImAgFnS	být
oproti	oproti	k7c3	oproti
mainstreamovému	mainstreamový	k2eAgInSc3d1	mainstreamový
trhu	trh	k1gInSc3	trh
zvláštní	zvláštní	k2eAgNnSc1d1	zvláštní
také	také	k9	také
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
dívčí	dívčí	k2eAgFnSc2d1	dívčí
mangy	mango	k1gNnPc7	mango
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
hlavní	hlavní	k2eAgInSc1d1	hlavní
směr	směr	k1gInSc1	směr
se	se	k3xPyFc4	se
soustředil	soustředit	k5eAaPmAgInS	soustředit
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
na	na	k7c4	na
šónen	šónen	k1gInSc4	šónen
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
až	až	k9	až
o	o	k7c4	o
78	[number]	k4	78
%	%	kIx~	%
veškerých	veškerý	k3xTgInPc2	veškerý
titulů	titul	k1gInPc2	titul
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
Comic	Comice	k1gFnPc2	Comice
Marketu	market	k1gInSc2	market
bylo	být	k5eAaImAgNnS	být
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
80	[number]	k4	80
%	%	kIx~	%
autorek	autorka	k1gFnPc2	autorka
tvořících	tvořící	k2eAgFnPc2d1	tvořící
dívčí	dívčí	k2eAgFnPc4d1	dívčí
šódžo	šódžo	k1gNnSc1	šódžo
mangu	mango	k1gNnSc3	mango
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
mangy	mango	k1gNnPc7	mango
vydané	vydaný	k2eAgFnPc1d1	vydaná
česky	česky	k6eAd1	česky
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
historie	historie	k1gFnSc1	historie
mangy	mango	k1gNnPc7	mango
spjata	spjat	k2eAgFnSc1d1	spjata
s	s	k7c7	s
anime	animat	k5eAaPmIp3nS	animat
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
objevovat	objevovat	k5eAaImF	objevovat
na	na	k7c6	na
českých	český	k2eAgFnPc6d1	Česká
sci-fi	scii	k1gFnPc6	sci-fi
a	a	k8xC	a
fantasy	fantas	k1gInPc7	fantas
conech	conech	k1gInSc1	conech
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
a	a	k8xC	a
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
se	se	k3xPyFc4	se
na	na	k7c6	na
akci	akce	k1gFnSc6	akce
jménem	jméno	k1gNnSc7	jméno
Raucon	Raucon	k1gMnSc1	Raucon
objevil	objevit	k5eAaPmAgMnS	objevit
první	první	k4xOgInSc4	první
program	program	k1gInSc4	program
zaměřený	zaměřený	k2eAgInSc4d1	zaměřený
na	na	k7c4	na
Japonsko	Japonsko	k1gNnSc4	Japonsko
<g/>
,	,	kIx,	,
anime	animat	k5eAaPmIp3nS	animat
a	a	k8xC	a
mangu	mango	k1gNnSc6	mango
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
také	také	k9	také
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
sdružení	sdružení	k1gNnSc1	sdružení
fanoušků	fanoušek	k1gMnPc2	fanoušek
s	s	k7c7	s
názvem	název	k1gInSc7	název
Česko-slovenský	českolovenský	k2eAgInSc1d1	česko-slovenský
klub	klub	k1gInSc1	klub
přátel	přítel	k1gMnPc2	přítel
mangy	mango	k1gNnPc7	mango
a	a	k8xC	a
anime	animat	k5eAaPmIp3nS	animat
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
záštitou	záštita	k1gFnSc7	záštita
sci-fi	scii	k1gNnSc2	sci-fi
klubu	klub	k1gInSc2	klub
Terminus	terminus	k1gInSc1	terminus
a	a	k8xC	a
Československého	československý	k2eAgInSc2d1	československý
fandomu	fandom	k1gInSc2	fandom
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
sdružení	sdružení	k1gNnSc1	sdružení
později	pozdě	k6eAd2	pozdě
pořádalo	pořádat	k5eAaImAgNnS	pořádat
program	program	k1gInSc4	program
zaměřený	zaměřený	k2eAgInSc4d1	zaměřený
na	na	k7c6	na
mangu	mango	k1gNnSc6	mango
a	a	k8xC	a
anime	aniit	k5eAaBmRp1nP	aniit
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
součástí	součást	k1gFnSc7	součást
setkání	setkání	k1gNnSc2	setkání
Parcon	Parcon	k1gMnSc1	Parcon
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
fanouškové	fanoušek	k1gMnPc1	fanoušek
anime	anim	k1gInSc5	anim
a	a	k8xC	a
mangy	mango	k1gNnPc7	mango
pořádali	pořádat	k5eAaImAgMnP	pořádat
setkání	setkání	k1gNnSc4	setkání
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
conech	con	k1gFnPc6	con
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
malou	malý	k2eAgFnSc4d1	malá
část	část	k1gFnSc4	část
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
samostatná	samostatný	k2eAgFnSc1d1	samostatná
akce	akce	k1gFnSc1	akce
s	s	k7c7	s
názvem	název	k1gInSc7	název
Animefest	Animefest	k1gFnSc1	Animefest
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
až	až	k9	až
8	[number]	k4	8
<g/>
.	.	kIx.	.
a	a	k8xC	a
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2004	[number]	k4	2004
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
sešlo	sejít	k5eAaPmAgNnS	sejít
přes	přes	k7c4	přes
200	[number]	k4	200
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
setkání	setkání	k1gNnSc1	setkání
fanoušků	fanoušek	k1gMnPc2	fanoušek
probíhá	probíhat	k5eAaImIp3nS	probíhat
každoročně	každoročně	k6eAd1	každoročně
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
května	květen	k1gInSc2	květen
a	a	k8xC	a
pořádá	pořádat	k5eAaImIp3nS	pořádat
jej	on	k3xPp3gMnSc4	on
občanské	občanský	k2eAgNnSc1d1	občanské
sdružení	sdružení	k1gNnSc1	sdružení
Brněnští	brněnský	k2eAgMnPc1d1	brněnský
otaku	otaku	k6eAd1	otaku
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
začaly	začít	k5eAaPmAgFnP	začít
vznikat	vznikat	k5eAaImF	vznikat
také	také	k9	také
první	první	k4xOgInPc4	první
neoficiální	neoficiální	k2eAgInPc4d1	neoficiální
překlady	překlad	k1gInPc4	překlad
mang	mango	k1gNnPc2	mango
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
čtenáři	čtenář	k1gMnPc7	čtenář
a	a	k8xC	a
fanoušky	fanoušek	k1gMnPc7	fanoušek
šířily	šířit	k5eAaImAgFnP	šířit
přes	přes	k7c4	přes
internet	internet	k1gInSc4	internet
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
oficiálním	oficiální	k2eAgInSc7d1	oficiální
titulem	titul	k1gInSc7	titul
byla	být	k5eAaImAgFnS	být
manga	mango	k1gNnSc2	mango
Gon	Gon	k1gFnSc2	Gon
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Calibre	Calibr	k1gInSc5	Calibr
Publishing	Publishing	k1gInSc1	Publishing
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
trhu	trh	k1gInSc6	trh
objevovat	objevovat	k5eAaImF	objevovat
některé	některý	k3yIgInPc1	některý
korejské	korejský	k2eAgInPc1d1	korejský
tituly	titul	k1gInPc1	titul
nazývané	nazývaný	k2eAgInPc1d1	nazývaný
manhwa	manhwa	k6eAd1	manhwa
<g/>
.	.	kIx.	.
</s>
<s>
Manga	mango	k1gNnPc1	mango
se	se	k3xPyFc4	se
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
začala	začít	k5eAaPmAgFnS	začít
oficiálně	oficiálně	k6eAd1	oficiálně
vydávat	vydávat	k5eAaImF	vydávat
až	až	k9	až
po	po	k7c6	po
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
Zoner	Zonero	k1gNnPc2	Zonero
Press	Pressa	k1gFnPc2	Pressa
vydalo	vydat	k5eAaPmAgNnS	vydat
Dveře	dveře	k1gFnPc1	dveře
chaosu	chaos	k1gInSc2	chaos
<g/>
.	.	kIx.	.
</s>
<s>
Tituly	titul	k1gInPc1	titul
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
následovaly	následovat	k5eAaImAgInP	následovat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
Gravitation	Gravitation	k1gInSc1	Gravitation
a	a	k8xC	a
Temné	temný	k2eAgNnSc1d1	temné
metro	metro	k1gNnSc1	metro
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
nakladatelství	nakladatelství	k1gNnPc1	nakladatelství
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
později	pozdě	k6eAd2	pozdě
začala	začít	k5eAaPmAgFnS	začít
vydávat	vydávat	k5eAaImF	vydávat
mangu	mango	k1gNnSc3	mango
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
Albatros	albatros	k1gMnSc1	albatros
(	(	kIx(	(
<g/>
manga	mango	k1gNnSc2	mango
Romeo	Romeo	k1gMnSc1	Romeo
a	a	k8xC	a
Julie	Julie	k1gFnSc1	Julie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Talpress	Talpress	k1gInSc1	Talpress
(	(	kIx(	(
<g/>
manga	mango	k1gNnSc2	mango
Motýlek	motýlek	k1gInSc4	motýlek
<g/>
)	)	kIx)	)
či	či	k8xC	či
polské	polský	k2eAgNnSc1d1	polské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Hanami	Hana	k1gFnPc7	Hana
(	(	kIx(	(
<g/>
manga	mango	k1gNnPc1	mango
Balzamovač	balzamovač	k1gMnSc1	balzamovač
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
vychází	vycházet	k5eAaImIp3nS	vycházet
manga	mango	k1gNnSc2	mango
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Crew	Crew	k1gFnSc2	Crew
(	(	kIx(	(
<g/>
Naruto	Narut	k2eAgNnSc4d1	Naruto
<g/>
,	,	kIx,	,
Death	Death	k1gMnSc1	Death
Note	Note	k1gFnSc1	Note
<g/>
,	,	kIx,	,
Bleach	Bleach	k1gInSc1	Bleach
<g/>
,	,	kIx,	,
Útok	útok	k1gInSc1	útok
titánů	titán	k1gMnPc2	titán
<g/>
,	,	kIx,	,
Gantz	Gantz	k1gInSc1	Gantz
<g/>
,	,	kIx,	,
Crying	Crying	k1gInSc1	Crying
Freeman	Freeman	k1gMnSc1	Freeman
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Trh	trh	k1gInSc1	trh
s	s	k7c7	s
mangou	manga	k1gFnSc7	manga
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
obrovský	obrovský	k2eAgInSc1d1	obrovský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
byla	být	k5eAaImAgFnS	být
celková	celkový	k2eAgFnSc1d1	celková
cena	cena	k1gFnSc1	cena
prodaných	prodaný	k2eAgInPc2d1	prodaný
manga	mango	k1gNnPc1	mango
publikací	publikace	k1gFnPc2	publikace
406	[number]	k4	406
miliard	miliarda	k4xCgFnPc2	miliarda
jenů	jen	k1gInPc2	jen
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
84	[number]	k4	84
miliard	miliarda	k4xCgFnPc2	miliarda
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
až	až	k9	až
40	[number]	k4	40
%	%	kIx~	%
veškerých	veškerý	k3xTgFnPc2	veškerý
tištěných	tištěný	k2eAgFnPc2d1	tištěná
publikací	publikace	k1gFnPc2	publikace
<g/>
.	.	kIx.	.
</s>
<s>
Manga	mango	k1gNnPc4	mango
však	však	k9	však
nezůstává	zůstávat	k5eNaImIp3nS	zůstávat
jen	jen	k9	jen
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
se	se	k3xPyFc4	se
i	i	k9	i
do	do	k7c2	do
jiných	jiný	k2eAgFnPc2d1	jiná
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
především	především	k9	především
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vychází	vycházet	k5eAaImIp3nS	vycházet
nejenom	nejenom	k6eAd1	nejenom
přeložená	přeložený	k2eAgNnPc4d1	přeložené
manga	mango	k1gNnPc4	mango
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
anime	animat	k5eAaPmIp3nS	animat
<g/>
.	.	kIx.	.
</s>
<s>
Způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgInSc7	jaký
se	se	k3xPyFc4	se
nové	nový	k2eAgInPc1d1	nový
příběhy	příběh	k1gInPc1	příběh
dostávají	dostávat	k5eAaImIp3nP	dostávat
mezi	mezi	k7c4	mezi
čtenáře	čtenář	k1gMnPc4	čtenář
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
případě	případ	k1gInSc6	případ
mangy	mango	k1gNnPc7	mango
velmi	velmi	k6eAd1	velmi
specifický	specifický	k2eAgInSc1d1	specifický
<g/>
.	.	kIx.	.
</s>
<s>
Příběhy	příběh	k1gInPc1	příběh
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
autorů	autor	k1gMnPc2	autor
vycházejí	vycházet	k5eAaImIp3nP	vycházet
nejprve	nejprve	k6eAd1	nejprve
po	po	k7c6	po
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
kapitolách	kapitola	k1gFnPc6	kapitola
v	v	k7c6	v
levných	levný	k2eAgInPc6d1	levný
časopisech	časopis	k1gInPc6	časopis
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
zaměřeny	zaměřit	k5eAaPmNgInP	zaměřit
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
cílovou	cílový	k2eAgFnSc4d1	cílová
skupinu	skupina	k1gFnSc4	skupina
(	(	kIx(	(
<g/>
šónen	šónen	k1gInSc1	šónen
<g/>
,	,	kIx,	,
šódžo	šódžo	k1gNnSc1	šódžo
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Těchto	tento	k3xDgInPc2	tento
časopisů	časopis	k1gInPc2	časopis
je	být	k5eAaImIp3nS	být
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
<g/>
,	,	kIx,	,
od	od	k7c2	od
nízkonákladových	nízkonákladový	k2eAgNnPc2d1	nízkonákladové
až	až	k9	až
po	po	k7c4	po
velmi	velmi	k6eAd1	velmi
známé	známý	k2eAgInPc4d1	známý
tituly	titul	k1gInPc4	titul
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Šúkan	Šúkan	k1gMnSc1	Šúkan
šónen	šónna	k1gFnPc2	šónna
Jump	Jump	k1gMnSc1	Jump
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
příběh	příběh	k1gInSc1	příběh
oblíbený	oblíbený	k2eAgInSc1d1	oblíbený
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
kapitoly	kapitola	k1gFnPc1	kapitola
vydají	vydat	k5eAaPmIp3nP	vydat
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
tankóbon	tankóbon	k1gInSc1	tankóbon
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
formát	formát	k1gInSc1	formát
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
podobný	podobný	k2eAgMnSc1d1	podobný
evropským	evropský	k2eAgFnPc3d1	Evropská
paperbackovým	paperbackový	k2eAgFnPc3d1	paperbacková
knihám	kniha	k1gFnPc3	kniha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
je	být	k5eAaImIp3nS	být
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
,	,	kIx,	,
že	že	k8xS	že
skupiny	skupina	k1gFnPc1	skupina
začínajících	začínající	k2eAgMnPc2d1	začínající
autorů	autor	k1gMnPc2	autor
vydávají	vydávat	k5eAaPmIp3nP	vydávat
své	svůj	k3xOyFgInPc4	svůj
vlastní	vlastní	k2eAgInPc4d1	vlastní
<g/>
,	,	kIx,	,
nízkonákladové	nízkonákladový	k2eAgInPc4d1	nízkonákladový
časopisy	časopis	k1gInPc4	časopis
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
pak	pak	k6eAd1	pak
začínající	začínající	k2eAgMnPc1d1	začínající
autoři	autor	k1gMnPc1	autor
sdílí	sdílet	k5eAaImIp3nP	sdílet
své	svůj	k3xOyFgInPc4	svůj
nápady	nápad	k1gInPc4	nápad
a	a	k8xC	a
příběhy	příběh	k1gInPc4	příběh
se	s	k7c7	s
čtenáři	čtenář	k1gMnPc7	čtenář
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
autor	autor	k1gMnSc1	autor
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
si	se	k3xPyFc3	se
jej	on	k3xPp3gMnSc4	on
všimne	všimnout	k5eAaPmIp3nS	všimnout
některé	některý	k3yIgNnSc1	některý
větší	veliký	k2eAgNnSc1d2	veliký
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
vyhraje	vyhrát	k5eAaPmIp3nS	vyhrát
některou	některý	k3yIgFnSc4	některý
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
soutěží	soutěž	k1gFnPc2	soutěž
pro	pro	k7c4	pro
začínající	začínající	k2eAgMnPc4d1	začínající
autory	autor	k1gMnPc4	autor
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
periodikum	periodikum	k1gNnSc1	periodikum
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
můžeme	moct	k5eAaImIp1nP	moct
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
předchůdce	předchůdce	k1gMnSc4	předchůdce
dnešních	dnešní	k2eAgInPc2d1	dnešní
manga	mango	k1gNnPc1	mango
časopisů	časopis	k1gInPc2	časopis
<g/>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1874	[number]	k4	1874
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Ešinbun	Ešinbun	k1gMnSc1	Ešinbun
Nipponči	Nipponči	k1gMnSc1	Nipponči
<g/>
.	.	kIx.	.
</s>
<s>
Autoři	autor	k1gMnPc1	autor
Kanagaki	Kanagak	k1gFnSc2	Kanagak
Robun	Robun	k1gNnSc1	Robun
a	a	k8xC	a
Kawanabe	Kawanab	k1gInSc5	Kawanab
Kjosai	Kjosai	k1gNnPc4	Kjosai
se	se	k3xPyFc4	se
inspirovali	inspirovat	k5eAaBmAgMnP	inspirovat
britským	britský	k2eAgInSc7d1	britský
časopisem	časopis	k1gInSc7	časopis
Japan	japan	k1gInSc1	japan
Punch	Puncha	k1gFnPc2	Puncha
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
vydával	vydávat	k5eAaImAgMnS	vydávat
Charles	Charles	k1gMnSc1	Charles
Wargman	Wargman	k1gMnSc1	Wargman
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
magazín	magazín	k1gInSc1	magazín
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
velmi	velmi	k6eAd1	velmi
jednoduché	jednoduchý	k2eAgFnPc4d1	jednoduchá
kresby	kresba	k1gFnPc4	kresba
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
se	se	k3xPyFc4	se
dotýkal	dotýkat	k5eAaImAgMnS	dotýkat
velmi	velmi	k6eAd1	velmi
kontroverzních	kontroverzní	k2eAgNnPc2d1	kontroverzní
témat	téma	k1gNnPc2	téma
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
se	se	k3xPyFc4	se
příliš	příliš	k6eAd1	příliš
nerozšířil	rozšířit	k5eNaPmAgMnS	rozšířit
a	a	k8xC	a
po	po	k7c6	po
třetím	třetí	k4xOgInSc6	třetí
vydání	vydání	k1gNnSc6	vydání
zaniknul	zaniknout	k5eAaPmAgMnS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
časopis	časopis	k1gInSc4	časopis
pak	pak	k6eAd1	pak
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1877	[number]	k4	1877
<g/>
,	,	kIx,	,
navázal	navázat	k5eAaPmAgInS	navázat
Marumaru	Marumara	k1gFnSc4	Marumara
Šinbun	Šinbuna	k1gFnPc2	Šinbuna
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1879	[number]	k4	1879
pak	pak	k6eAd1	pak
Garakuta	Garakut	k2eAgFnSc1d1	Garakut
Činpo	Činpa	k1gFnSc5	Činpa
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
šónen	šónen	k2eAgInSc1d1	šónen
magazín	magazín	k1gInSc1	magazín
začal	začít	k5eAaPmAgInS	začít
vydávat	vydávat	k5eAaImF	vydávat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
Iwaja	Iwaja	k1gFnSc1	Iwaja
Sazanami	Sazana	k1gFnPc7	Sazana
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
periodikum	periodikum	k1gNnSc1	periodikum
pro	pro	k7c4	pro
mladé	mladý	k2eAgMnPc4d1	mladý
chlapce	chlapec	k1gMnPc4	chlapec
bylo	být	k5eAaImAgNnS	být
výrazně	výrazně	k6eAd1	výrazně
zaměřeno	zaměřit	k5eAaPmNgNnS	zaměřit
na	na	k7c4	na
první	první	k4xOgFnSc4	první
čínsko-japonskou	čínskoaponský	k2eAgFnSc4d1	čínsko-japonská
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgFnP	být
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
fronty	fronta	k1gFnSc2	fronta
a	a	k8xC	a
úspěchy	úspěch	k1gInPc4	úspěch
japonské	japonský	k2eAgFnSc2d1	japonská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
časopis	časopis	k1gInSc1	časopis
přestal	přestat	k5eAaPmAgInS	přestat
vycházet	vycházet	k5eAaImF	vycházet
záhy	záhy	k6eAd1	záhy
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
nových	nový	k2eAgNnPc2d1	nové
periodik	periodikum	k1gNnPc2	periodikum
začalo	začít	k5eAaPmAgNnS	začít
vycházet	vycházet	k5eAaImF	vycházet
až	až	k9	až
o	o	k7c4	o
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
(	(	kIx(	(
<g/>
během	během	k7c2	během
rusko-japonské	ruskoaponský	k2eAgFnSc2d1	rusko-japonská
války	válka	k1gFnSc2	válka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
i	i	k9	i
první	první	k4xOgInPc4	první
časopisy	časopis	k1gInPc4	časopis
zaměřené	zaměřený	k2eAgInPc4d1	zaměřený
na	na	k7c4	na
dívky	dívka	k1gFnPc4	dívka
(	(	kIx(	(
<g/>
šódžo	šódžo	k6eAd1	šódžo
sekai	sekai	k6eAd1	sekai
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
na	na	k7c4	na
menší	malý	k2eAgFnPc4d2	menší
děti	dítě	k1gFnPc4	dítě
(	(	kIx(	(
<g/>
Šónen	Šónen	k1gInSc1	Šónen
Pakku	Pakk	k1gInSc2	Pakk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc4d1	další
tituly	titul	k1gInPc4	titul
<g/>
,	,	kIx,	,
zaměřené	zaměřený	k2eAgInPc4d1	zaměřený
na	na	k7c4	na
různé	různý	k2eAgFnPc4d1	různá
skupiny	skupina	k1gFnPc4	skupina
čtenářů	čtenář	k1gMnPc2	čtenář
vznikaly	vznikat	k5eAaImAgFnP	vznikat
a	a	k8xC	a
zanikaly	zanikat	k5eAaImAgFnP	zanikat
velkou	velký	k2eAgFnSc7d1	velká
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
Poten	Poten	k1gInSc1	Poten
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
jediné	jediný	k2eAgNnSc1d1	jediné
číslo	číslo	k1gNnSc1	číslo
tohoto	tento	k3xDgInSc2	tento
časopisu	časopis	k1gInSc2	časopis
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
vytištěno	vytisknout	k5eAaPmNgNnS	vytisknout
barevně	barevně	k6eAd1	barevně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kodomo	Kodoma	k1gFnSc5	Kodoma
Pakku	Pakek	k1gMnSc6	Pakek
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
velmi	velmi	k6eAd1	velmi
kvalitní	kvalitní	k2eAgFnPc4d1	kvalitní
kresby	kresba	k1gFnPc4	kresba
a	a	k8xC	a
příběhy	příběh	k1gInPc4	příběh
od	od	k7c2	od
známých	známá	k1gFnPc2	známá
mangaka	mangak	k1gMnSc2	mangak
tvůrců	tvůrce	k1gMnPc2	tvůrce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Manga	mango	k1gNnSc2	mango
no	no	k9	no
Kuni	Kuni	k1gNnSc2	Kuni
(	(	kIx(	(
<g/>
od	od	k7c2	od
1935	[number]	k4	1935
do	do	k7c2	do
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
vycházel	vycházet	k5eAaImAgMnS	vycházet
v	v	k7c6	v
období	období	k1gNnSc6	období
druhé	druhý	k4xOgFnSc2	druhý
čínsko-japonské	čínskoaponský	k2eAgFnSc2d1	čínsko-japonská
války	válka	k1gFnSc2	válka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
začaly	začít	k5eAaPmAgInP	začít
počátkem	počátkem	k7c2	počátkem
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
vycházet	vycházet	k5eAaImF	vycházet
další	další	k2eAgFnPc4d1	další
periodika	periodikum	k1gNnPc4	periodikum
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
již	již	k6eAd1	již
začala	začít	k5eAaPmAgFnS	začít
vycházet	vycházet	k5eAaImF	vycházet
jednou	jednou	k6eAd1	jednou
měsíčně	měsíčně	k6eAd1	měsíčně
a	a	k8xC	a
vycházela	vycházet	k5eAaImAgFnS	vycházet
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
několika	několik	k4yIc2	několik
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
také	také	k9	také
vyšel	vyjít	k5eAaPmAgMnS	vyjít
první	první	k4xOgInSc4	první
týdenní	týdenní	k2eAgInSc4d1	týdenní
časopis	časopis	k1gInSc4	časopis
pro	pro	k7c4	pro
chlapce	chlapec	k1gMnSc4	chlapec
–	–	k?	–
Šúkan	Šúkan	k1gMnSc1	Šúkan
šónen	šónen	k2eAgInSc4d1	šónen
Magadžin	Magadžin	k1gInSc4	Magadžin
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
週	週	k?	週
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vychází	vycházet	k5eAaImIp3nS	vycházet
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dódžinši	Dódžinše	k1gFnSc4	Dódžinše
<g/>
.	.	kIx.	.
</s>
<s>
Dódžinši	Dódžinše	k1gFnSc4	Dódžinše
je	být	k5eAaImIp3nS	být
výraz	výraz	k1gInSc1	výraz
pro	pro	k7c4	pro
nízkonákladové	nízkonákladový	k2eAgFnPc4d1	nízkonákladová
<g/>
,	,	kIx,	,
nezávislé	závislý	k2eNgFnPc4d1	nezávislá
publikace	publikace	k1gFnPc4	publikace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vycházejí	vycházet	k5eAaImIp3nP	vycházet
mimo	mimo	k7c4	mimo
hlavní	hlavní	k2eAgInSc4d1	hlavní
trh	trh	k1gInSc4	trh
s	s	k7c7	s
mangou	manga	k1gFnSc7	manga
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	on	k3xPp3gMnPc4	on
vydávají	vydávat	k5eAaPmIp3nP	vydávat
malé	malý	k2eAgFnPc1d1	malá
společnosti	společnost	k1gFnPc1	společnost
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
samotní	samotný	k2eAgMnPc1d1	samotný
autoři	autor	k1gMnPc1	autor
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
označení	označení	k1gNnSc1	označení
chápáno	chápat	k5eAaImNgNnS	chápat
jako	jako	k8xC	jako
sborník	sborník	k1gInSc1	sborník
původních	původní	k2eAgNnPc2d1	původní
děl	dělo	k1gNnPc2	dělo
začínajících	začínající	k2eAgMnPc2d1	začínající
autorů	autor	k1gMnPc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
je	on	k3xPp3gNnSc4	on
dódžinši	dódžinsat	k5eAaPmIp1nSwK	dódžinsat
chápáno	chápán	k2eAgNnSc1d1	chápáno
i	i	k9	i
jako	jako	k8xC	jako
fanartová	fanartová	k1gFnSc1	fanartová
publikace	publikace	k1gFnSc2	publikace
obsahující	obsahující	k2eAgFnSc2d1	obsahující
parodie	parodie	k1gFnSc2	parodie
nebo	nebo	k8xC	nebo
převyprávění	převyprávění	k1gNnSc2	převyprávění
známých	známý	k2eAgInPc2d1	známý
příběhů	příběh	k1gInPc2	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Japonská	japonský	k2eAgNnPc1d1	Japonské
nakladatelství	nakladatelství	k1gNnPc1	nakladatelství
vydávající	vydávající	k2eAgNnPc1d1	vydávající
mangu	mango	k1gNnSc6	mango
často	často	k6eAd1	často
vyhlašují	vyhlašovat	k5eAaImIp3nP	vyhlašovat
soutěže	soutěž	k1gFnPc4	soutěž
a	a	k8xC	a
ocenění	ocenění	k1gNnSc4	ocenění
pro	pro	k7c4	pro
začínající	začínající	k2eAgMnPc4d1	začínající
a	a	k8xC	a
neznámé	známý	k2eNgMnPc4d1	neznámý
autory	autor	k1gMnPc4	autor
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
do	do	k7c2	do
známých	známý	k2eAgInPc2d1	známý
časopisů	časopis	k1gInPc2	časopis
dostávají	dostávat	k5eAaImIp3nP	dostávat
noví	nový	k2eAgMnPc1d1	nový
autoři	autor	k1gMnPc1	autor
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc4	jejich
příběhy	příběh	k1gInPc4	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
soutěží	soutěž	k1gFnPc2	soutěž
a	a	k8xC	a
ocenění	ocenění	k1gNnSc4	ocenění
uveďme	uvést	k5eAaPmRp1nP	uvést
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
Akacuka	Akacuk	k1gMnSc2	Akacuk
šó	šó	k?	šó
za	za	k7c4	za
komediální	komediální	k2eAgFnSc4d1	komediální
mangu	mango	k1gNnSc3	mango
<g/>
,	,	kIx,	,
vyhlašuje	vyhlašovat	k5eAaImIp3nS	vyhlašovat
Šúeiša	Šúeiša	k1gMnSc1	Šúeiša
<g/>
.	.	kIx.	.
</s>
<s>
Dengeki	Dengek	k1gMnSc3	Dengek
Komikku	Komikek	k1gMnSc3	Komikek
Guran	Guran	k1gInSc1	Guran
Puri	Puri	k1gNnSc2	Puri
za	za	k7c4	za
krátké	krátký	k2eAgInPc4d1	krátký
<g/>
,	,	kIx,	,
jednodílné	jednodílný	k2eAgInPc4d1	jednodílný
příběhy	příběh	k1gInPc4	příběh
<g/>
,	,	kIx,	,
vyhlašuje	vyhlašovat	k5eAaImIp3nS	vyhlašovat
ASCII	ascii	kA	ascii
Media	medium	k1gNnSc2	medium
Works	Works	kA	Works
<g/>
.	.	kIx.	.
</s>
<s>
Kódanša	Kódanša	k1gMnSc1	Kódanša
manga	mango	k1gNnSc2	mango
šó	šó	k?	šó
je	být	k5eAaImIp3nS	být
ocenění	ocenění	k1gNnSc4	ocenění
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vyhlašuje	vyhlašovat	k5eAaImIp3nS	vyhlašovat
Kódanša	Kódanša	k1gFnSc1	Kódanša
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
kategoriích	kategorie	k1gFnPc6	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Seiunšó	Seiunšó	k?	Seiunšó
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
sci-fi	scii	k1gFnSc4	sci-fi
mangu	mango	k1gNnSc3	mango
předchozího	předchozí	k2eAgInSc2d1	předchozí
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Šógakukan	Šógakukan	k1gInSc1	Šógakukan
mangašó	mangašó	k?	mangašó
ocenění	ocenění	k1gNnSc1	ocenění
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
kategoriích	kategorie	k1gFnPc6	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Tezuka	Tezuk	k1gMnSc4	Tezuk
šó	šó	k?	šó
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
nově	nově	k6eAd1	nově
vydanou	vydaný	k2eAgFnSc4d1	vydaná
sérii	série	k1gFnSc4	série
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
mimojaponští	mimojaponský	k2eAgMnPc1d1	mimojaponský
autoři	autor	k1gMnPc1	autor
a	a	k8xC	a
tvůrci	tvůrce	k1gMnPc1	tvůrce
jsou	být	k5eAaImIp3nP	být
odměňováni	odměňovat	k5eAaImNgMnP	odměňovat
cenou	cena	k1gFnSc7	cena
Kokusai	Kokusa	k1gFnSc2	Kokusa
manga	mango	k1gNnSc2	mango
šó	šó	k?	šó
(	(	kIx(	(
<g/>
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
ocenění	ocenění	k1gNnSc4	ocenění
za	za	k7c4	za
mangu	mango	k1gNnSc6	mango
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vyhlašuje	vyhlašovat	k5eAaImIp3nS	vyhlašovat
japonské	japonský	k2eAgNnSc4d1	Japonské
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
