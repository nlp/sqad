<s>
Manga	mango	k1gNnPc1	mango
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
漫	漫	k?	漫
<g/>
,	,	kIx,	,
hiragana	hiragana	k1gFnSc1	hiragana
<g/>
:	:	kIx,	:
ま	ま	k?	ま
<g/>
,	,	kIx,	,
katakana	katakana	k1gFnSc1	katakana
<g/>
:	:	kIx,	:
マ	マ	k?	マ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
japonské	japonský	k2eAgNnSc4d1	Japonské
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
japonský	japonský	k2eAgInSc4d1	japonský
komiks	komiks	k1gInSc4	komiks
<g/>
,	,	kIx,	,
nakreslený	nakreslený	k2eAgInSc4d1	nakreslený
stylem	styl	k1gInSc7	styl
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
vyvíjel	vyvíjet	k5eAaImAgInS	vyvíjet
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
