<s>
Setkání	setkání	k1gNnSc1	setkání
mělo	mít	k5eAaImAgNnS	mít
krycí	krycí	k2eAgInSc4d1	krycí
název	název	k1gInSc4	název
Argonaut	argonaut	k1gMnSc1	argonaut
a	a	k8xC	a
hlavními	hlavní	k2eAgFnPc7d1	hlavní
otázkami	otázka	k1gFnPc7	otázka
projednávanými	projednávaný	k2eAgFnPc7d1	projednávaná
byl	být	k5eAaImAgInS	být
vztah	vztah	k1gInSc4	vztah
Spojenců	spojenec	k1gMnPc2	spojenec
k	k	k7c3	k
Německu	Německo	k1gNnSc3	Německo
a	a	k8xC	a
Francii	Francie	k1gFnSc3	Francie
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
se	se	k3xPyFc4	se
projednávala	projednávat	k5eAaImAgFnS	projednávat
polská	polský	k2eAgFnSc1d1	polská
otázka	otázka	k1gFnSc1	otázka
a	a	k8xC	a
také	také	k9	také
vznik	vznik	k1gInSc1	vznik
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
