<s>
Kulajda	kulajda	k1gFnSc1	kulajda
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
bílá	bílý	k2eAgFnSc1d1	bílá
polévka	polévka	k1gFnSc1	polévka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
smetany	smetana	k1gFnSc2	smetana
<g/>
,	,	kIx,	,
koření	koření	k1gNnSc2	koření
<g/>
,	,	kIx,	,
vajíčka	vajíčko	k1gNnSc2	vajíčko
<g/>
,	,	kIx,	,
kopru	kopr	k1gInSc2	kopr
a	a	k8xC	a
brambor	brambora	k1gFnPc2	brambora
<g/>
.	.	kIx.	.
</s>
<s>
Typická	typický	k2eAgFnSc1d1	typická
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
vysokou	vysoký	k2eAgFnSc4d1	vysoká
hustotu	hustota	k1gFnSc4	hustota
<g/>
,	,	kIx,	,
bílou	bílý	k2eAgFnSc4d1	bílá
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
charakteristickou	charakteristický	k2eAgFnSc4d1	charakteristická
chuť	chuť	k1gFnSc4	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
přípravu	příprava	k1gFnSc4	příprava
kulajdy	kulajda	k1gFnSc2	kulajda
neexistuje	existovat	k5eNaImIp3nS	existovat
jednotný	jednotný	k2eAgInSc4d1	jednotný
recept	recept	k1gInSc4	recept
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
univerzální	univerzální	k2eAgMnSc1d1	univerzální
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
mnoho	mnoho	k4c1	mnoho
variant	varianta	k1gFnPc2	varianta
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
kořením	koření	k1gNnSc7	koření
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
přísadami	přísada	k1gFnPc7	přísada
přidávanými	přidávaný	k2eAgFnPc7d1	přidávaná
do	do	k7c2	do
polévky	polévka	k1gFnSc2	polévka
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
kulajda	kulajda	k1gFnSc1	kulajda
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
sytost	sytost	k1gFnSc4	sytost
podává	podávat	k5eAaImIp3nS	podávat
jako	jako	k9	jako
samostatný	samostatný	k2eAgInSc1d1	samostatný
hlavní	hlavní	k2eAgInSc1d1	hlavní
chod	chod	k1gInSc1	chod
místo	místo	k7c2	místo
předkrmu	předkrm	k1gInSc2	předkrm
<g/>
.	.	kIx.	.
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
kulajda	kulajda	k1gFnSc1	kulajda
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Recept	recept	k1gInSc1	recept
jihočeská	jihočeský	k2eAgFnSc1d1	Jihočeská
kulajda	kulajda	k1gFnSc1	kulajda
</s>
