<s>
Kulajda	kulajda	k1gFnSc1	kulajda
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
bílá	bílý	k2eAgFnSc1d1	bílá
polévka	polévka	k1gFnSc1	polévka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
smetany	smetana	k1gFnSc2	smetana
<g/>
,	,	kIx,	,
koření	koření	k1gNnSc2	koření
<g/>
,	,	kIx,	,
vajíčka	vajíčko	k1gNnSc2	vajíčko
<g/>
,	,	kIx,	,
kopru	kopr	k1gInSc2	kopr
a	a	k8xC	a
brambor	brambora	k1gFnPc2	brambora
<g/>
.	.	kIx.	.
</s>
