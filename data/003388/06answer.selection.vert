<s>
Ptáci	pták	k1gMnPc1	pták
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
obojživelníci	obojživelník	k1gMnPc1	obojživelník
a	a	k8xC	a
plazi	plaz	k1gMnPc1	plaz
snášejí	snášet	k5eAaImIp3nP	snášet
vejce	vejce	k1gNnPc4	vejce
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
obvykle	obvykle	k6eAd1	obvykle
ukládají	ukládat	k5eAaImIp3nP	ukládat
do	do	k7c2	do
hnízd	hnízdo	k1gNnPc2	hnízdo
<g/>
,	,	kIx,	,
sedí	sedit	k5eAaImIp3nS	sedit
na	na	k7c6	na
vejcích	vejce	k1gNnPc6	vejce
a	a	k8xC	a
zahřívají	zahřívat	k5eAaImIp3nP	zahřívat
je	on	k3xPp3gFnPc4	on
až	až	k9	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
vylíhnutí	vylíhnutí	k1gNnPc2	vylíhnutí
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
