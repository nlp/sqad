<s>
Lahve	lahev	k1gFnPc1	lahev
plněné	plněný	k2eAgFnPc1d1	plněná
parciální	parciální	k2eAgFnSc7d1	parciální
metodou	metoda	k1gFnSc7	metoda
musejí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
kyslíkově	kyslíkově	k6eAd1	kyslíkově
čisté	čistý	k2eAgNnSc4d1	čisté
i	i	k9	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
výsledná	výsledný	k2eAgFnSc1d1	výsledná
směs	směs	k1gFnSc1	směs
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
méně	málo	k6eAd2	málo
než	než	k8xS	než
40	[number]	k4	40
<g/>
%	%	kIx~	%
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
