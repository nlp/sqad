<p>
<s>
Nitrox	Nitrox	k1gInSc1	Nitrox
je	být	k5eAaImIp3nS	být
jakákoliv	jakýkoliv	k3yIgFnSc1	jakýkoliv
směs	směs	k1gFnSc1	směs
plynů	plyn	k1gInPc2	plyn
kyslíku	kyslík	k1gInSc2	kyslík
a	a	k8xC	a
dusíku	dusík	k1gInSc2	dusík
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
vzduchu	vzduch	k1gInSc2	vzduch
(	(	kIx(	(
<g/>
78	[number]	k4	78
<g/>
%	%	kIx~	%
dusík	dusík	k1gInSc1	dusík
<g/>
,	,	kIx,	,
21	[number]	k4	21
<g/>
%	%	kIx~	%
kyslík	kyslík	k1gInSc1	kyslík
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
%	%	kIx~	%
zbytkové	zbytkový	k2eAgInPc1d1	zbytkový
plyny	plyn	k1gInPc1	plyn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
potápění	potápění	k1gNnSc6	potápění
se	se	k3xPyFc4	se
nicméně	nicméně	k8xC	nicméně
za	za	k7c4	za
Nitrox	Nitrox	k1gInSc4	Nitrox
považuje	považovat	k5eAaImIp3nS	považovat
směs	směs	k1gFnSc1	směs
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgFnPc4	který
obsah	obsah	k1gInSc1	obsah
kyslíku	kyslík	k1gInSc2	kyslík
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
21	[number]	k4	21
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
dusíku	dusík	k1gInSc2	dusík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Snížení	snížení	k1gNnSc1	snížení
dusíku	dusík	k1gInSc2	dusík
ve	v	k7c6	v
směsi	směs	k1gFnSc6	směs
má	mít	k5eAaImIp3nS	mít
následující	následující	k2eAgInSc1d1	následující
efekt	efekt	k1gInSc1	efekt
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Prodloužení	prodloužení	k1gNnSc1	prodloužení
nulového	nulový	k2eAgInSc2d1	nulový
času	čas	k1gInSc2	čas
</s>
</p>
<p>
<s>
Zkrácení	zkrácení	k1gNnSc1	zkrácení
nebo	nebo	k8xC	nebo
eliminace	eliminace	k1gFnSc1	eliminace
stupňovité	stupňovitý	k2eAgFnSc2d1	stupňovitá
dekomprese	dekomprese	k1gFnSc2	dekomprese
</s>
</p>
<p>
<s>
Snížení	snížení	k1gNnSc1	snížení
únavy	únava	k1gFnSc2	únava
po	po	k7c6	po
ponoru	ponor	k1gInSc6	ponor
</s>
</p>
<p>
<s>
Zkrácení	zkrácení	k1gNnSc1	zkrácení
povrchového	povrchový	k2eAgInSc2d1	povrchový
intervalu	interval	k1gInSc2	interval
</s>
</p>
<p>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
použití	použití	k1gNnSc2	použití
vzduchových	vzduchový	k2eAgFnPc2d1	vzduchová
tabulek	tabulka	k1gFnPc2	tabulka
snížení	snížení	k1gNnSc4	snížení
rizika	riziko	k1gNnSc2	riziko
dekompresní	dekompresní	k2eAgFnSc2d1	dekompresní
nemoci	nemoc	k1gFnSc2	nemoc
</s>
</p>
<p>
<s>
==	==	k?	==
Princip	princip	k1gInSc1	princip
fungování	fungování	k1gNnSc2	fungování
směsi	směs	k1gFnSc2	směs
==	==	k?	==
</s>
</p>
<p>
<s>
Při	při	k7c6	při
potápění	potápění	k1gNnSc6	potápění
do	do	k7c2	do
hloubek	hloubka	k1gFnPc2	hloubka
vzrůstá	vzrůstat	k5eAaImIp3nS	vzrůstat
okolní	okolní	k2eAgInSc4d1	okolní
tlak	tlak	k1gInSc4	tlak
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
parciální	parciální	k2eAgNnSc1d1	parciální
tlak	tlak	k1gInSc4	tlak
vdechovaného	vdechovaný	k2eAgInSc2d1	vdechovaný
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Nitrox	Nitrox	k1gInSc1	Nitrox
snižuje	snižovat	k5eAaImIp3nS	snižovat
nasycení	nasycení	k1gNnSc4	nasycení
potápěčova	potápěčův	k2eAgNnSc2d1	potápěčův
těla	tělo	k1gNnSc2	tělo
dusíkem	dusík	k1gInSc7	dusík
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšení	zvýšení	k1gNnSc1	zvýšení
parciálního	parciální	k2eAgInSc2d1	parciální
tlaku	tlak	k1gInSc2	tlak
dusíku	dusík	k1gInSc2	dusík
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
menší	malý	k2eAgFnSc1d2	menší
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
potápěč	potápěč	k1gInSc1	potápěč
může	moct	k5eAaImIp3nS	moct
zůstat	zůstat	k5eAaPmF	zůstat
v	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
hloubce	hloubka	k1gFnSc6	hloubka
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
se	s	k7c7	s
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
musel	muset	k5eAaImAgMnS	muset
absolvovat	absolvovat	k5eAaPmF	absolvovat
dekompresní	dekompresní	k2eAgFnSc2d1	dekompresní
zastávky	zastávka	k1gFnSc2	zastávka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zvýšení	zvýšení	k1gNnSc1	zvýšení
parciálních	parciální	k2eAgInPc2d1	parciální
tlaků	tlak	k1gInPc2	tlak
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
všech	všecek	k3xTgInPc2	všecek
plynů	plyn	k1gInPc2	plyn
v	v	k7c4	v
dýchací	dýchací	k2eAgFnPc4d1	dýchací
směsi	směs	k1gFnPc4	směs
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
i	i	k8xC	i
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Kyslík	kyslík	k1gInSc1	kyslík
ale	ale	k8xC	ale
při	při	k7c6	při
určitém	určitý	k2eAgInSc6d1	určitý
parciálním	parciální	k2eAgInSc6d1	parciální
tlaku	tlak	k1gInSc6	tlak
začíná	začínat	k5eAaImIp3nS	začínat
být	být	k5eAaImF	být
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
toxický	toxický	k2eAgInSc1d1	toxický
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yInSc7	co
více	hodně	k6eAd2	hodně
kyslíku	kyslík	k1gInSc2	kyslík
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
dýchací	dýchací	k2eAgFnSc6d1	dýchací
směsi	směs	k1gFnSc6	směs
obsaženo	obsáhnout	k5eAaPmNgNnS	obsáhnout
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
dříve	dříve	k6eAd2	dříve
je	být	k5eAaImIp3nS	být
směs	směs	k1gFnSc1	směs
toxická	toxický	k2eAgFnSc1d1	toxická
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgInSc1d1	maximální
parciální	parciální	k2eAgInSc1d1	parciální
tlak	tlak	k1gInSc1	tlak
kyslíku	kyslík	k1gInSc2	kyslík
používaný	používaný	k2eAgInSc1d1	používaný
při	při	k7c6	při
rekreačním	rekreační	k2eAgNnSc6d1	rekreační
potápění	potápění	k1gNnSc6	potápění
je	být	k5eAaImIp3nS	být
160	[number]	k4	160
kPa	kPa	k?	kPa
(	(	kIx(	(
<g/>
1,6	[number]	k4	1,6
bar	bar	k1gInSc1	bar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
maximální	maximální	k2eAgFnSc1d1	maximální
operační	operační	k2eAgFnSc1d1	operační
hloubka	hloubka	k1gFnSc1	hloubka
<g/>
.	.	kIx.	.
<g/>
Pro	pro	k7c4	pro
potápění	potápění	k1gNnSc4	potápění
s	s	k7c7	s
Nitroxem	Nitrox	k1gInSc7	Nitrox
je	být	k5eAaImIp3nS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
speciální	speciální	k2eAgInSc1d1	speciální
kurz	kurz	k1gInSc1	kurz
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
potápěči	potápěč	k1gMnPc1	potápěč
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
jeho	jeho	k3xOp3gFnSc4	jeho
stávající	stávající	k2eAgFnSc4d1	stávající
kvalifikaci	kvalifikace	k1gFnSc4	kvalifikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Označení	označení	k1gNnSc1	označení
Nitroxu	Nitrox	k1gInSc2	Nitrox
==	==	k?	==
</s>
</p>
<p>
<s>
Nitrox	Nitrox	k1gInSc1	Nitrox
se	se	k3xPyFc4	se
až	až	k9	až
na	na	k7c4	na
malé	malý	k2eAgFnPc4d1	malá
výjimky	výjimka	k1gFnPc4	výjimka
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
EANx	EANx	k1gInSc1	EANx
<g/>
.	.	kIx.	.
</s>
<s>
EAN	EAN	kA	EAN
je	být	k5eAaImIp3nS	být
anglická	anglický	k2eAgFnSc1d1	anglická
zkratka	zkratka	k1gFnSc1	zkratka
pro	pro	k7c4	pro
vzduch	vzduch	k1gInSc4	vzduch
obohacený	obohacený	k2eAgInSc4d1	obohacený
o	o	k7c4	o
kyslík	kyslík	k1gInSc4	kyslík
(	(	kIx(	(
<g/>
Enriched	Enriched	k1gInSc1	Enriched
Air	Air	k1gMnSc1	Air
Nitrox	Nitrox	k1gInSc1	Nitrox
<g/>
)	)	kIx)	)
a	a	k8xC	a
za	za	k7c4	za
x	x	k?	x
se	se	k3xPyFc4	se
dosazuje	dosazovat	k5eAaImIp3nS	dosazovat
procentuální	procentuální	k2eAgNnSc1d1	procentuální
zastoupení	zastoupení	k1gNnSc1	zastoupení
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
například	například	k6eAd1	například
EAN50	EAN50	k1gFnSc4	EAN50
je	být	k5eAaImIp3nS	být
nitrox	nitrox	k1gInSc1	nitrox
s	s	k7c7	s
50	[number]	k4	50
<g/>
%	%	kIx~	%
kyslíku	kyslík	k1gInSc2	kyslík
a	a	k8xC	a
50	[number]	k4	50
<g/>
%	%	kIx~	%
<g/>
dusíku	dusík	k1gInSc2	dusík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výcviková	výcvikový	k2eAgFnSc1d1	výcviková
společnost	společnost	k1gFnSc1	společnost
ANDI	ANDI	kA	ANDI
(	(	kIx(	(
<g/>
American	American	k1gInSc1	American
Nitrox	Nitrox	k1gInSc1	Nitrox
Divers	Divers	k1gInSc1	Divers
International	International	k1gFnSc1	International
<g/>
)	)	kIx)	)
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
Nitrox	Nitrox	k1gInSc4	Nitrox
označení	označení	k1gNnSc2	označení
"	"	kIx"	"
<g/>
SafeAir	SafeAir	k1gInSc1	SafeAir
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
označení	označení	k1gNnSc1	označení
nitroxu	nitrox	k1gInSc2	nitrox
však	však	k9	však
přísluší	příslušet	k5eAaImIp3nS	příslušet
pouze	pouze	k6eAd1	pouze
směsím	směs	k1gFnPc3	směs
připraveným	připravený	k2eAgFnPc3d1	připravená
podle	podle	k7c2	podle
vysokých	vysoký	k2eAgInPc2d1	vysoký
standardů	standard	k1gInPc2	standard
ANDI	ANDI	kA	ANDI
a	a	k8xC	a
ve	v	k7c6	v
schváleném	schválený	k2eAgNnSc6d1	schválené
středisku	středisko	k1gNnSc6	středisko
ANDI	ANDI	kA	ANDI
(	(	kIx(	(
<g/>
ANDI	ANDI	kA	ANDI
facility	facilita	k1gFnSc2	facilita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Standardizované	standardizovaný	k2eAgFnPc4d1	standardizovaná
směsi	směs	k1gFnPc4	směs
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
běžné	běžný	k2eAgNnSc4d1	běžné
potápění	potápění	k1gNnSc4	potápění
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
používají	používat	k5eAaImIp3nP	používat
tyto	tento	k3xDgInPc4	tento
plyny	plyn	k1gInPc4	plyn
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
EAN32	EAN32	k4	EAN32
</s>
</p>
<p>
<s>
32	[number]	k4	32
<g/>
%	%	kIx~	%
kyslíku	kyslík	k1gInSc2	kyslík
</s>
</p>
<p>
<s>
EAN36	EAN36	k4	EAN36
</s>
</p>
<p>
<s>
36	[number]	k4	36
<g/>
%	%	kIx~	%
kyslíkuPro	kyslíkuPro	k6eAd1	kyslíkuPro
akcelerovanou	akcelerovaný	k2eAgFnSc4d1	akcelerovaná
dekompresi	dekomprese	k1gFnSc4	dekomprese
se	se	k3xPyFc4	se
standardně	standardně	k6eAd1	standardně
používá	používat	k5eAaImIp3nS	používat
EAN	EAN	kA	EAN
<g/>
50	[number]	k4	50
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
náročnější	náročný	k2eAgFnSc1d2	náročnější
dekomprese	dekomprese	k1gFnSc1	dekomprese
se	se	k3xPyFc4	se
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
čistým	čistý	k2eAgInSc7d1	čistý
kyslíkem	kyslík	k1gInSc7	kyslík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rizika	riziko	k1gNnSc2	riziko
používání	používání	k1gNnSc2	používání
Nitroxových	Nitroxový	k2eAgFnPc2d1	Nitroxový
směsí	směs	k1gFnPc2	směs
==	==	k?	==
</s>
</p>
<p>
<s>
Zvýšené	zvýšený	k2eAgNnSc1d1	zvýšené
množství	množství	k1gNnSc1	množství
kyslíku	kyslík	k1gInSc2	kyslík
má	mít	k5eAaImIp3nS	mít
kromě	kromě	k7c2	kromě
výše	výše	k1gFnSc2	výše
zmíněné	zmíněný	k2eAgFnSc2d1	zmíněná
kyslíkové	kyslíkový	k2eAgFnSc2d1	kyslíková
toxicity	toxicita	k1gFnSc2	toxicita
ještě	ještě	k9	ještě
jednu	jeden	k4xCgFnSc4	jeden
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
nevýhodu	nevýhoda	k1gFnSc4	nevýhoda
<g/>
.	.	kIx.	.
</s>
<s>
Tou	ten	k3xDgFnSc7	ten
je	být	k5eAaImIp3nS	být
vysoká	vysoká	k1gFnSc1	vysoká
reaktivnost	reaktivnost	k1gFnSc1	reaktivnost
kyslíku	kyslík	k1gInSc2	kyslík
s	s	k7c7	s
organickými	organický	k2eAgInPc7d1	organický
tuky	tuk	k1gInPc7	tuk
jež	jenž	k3xRgInPc1	jenž
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
prudké	prudký	k2eAgNnSc4d1	prudké
hoření	hoření	k1gNnSc4	hoření
až	až	k9	až
výbuch	výbuch	k1gInSc1	výbuch
<g/>
.	.	kIx.	.
</s>
<s>
Potápěčská	potápěčský	k2eAgFnSc1d1	potápěčská
výstroj	výstroj	k1gFnSc1	výstroj
přicházející	přicházející	k2eAgFnSc1d1	přicházející
do	do	k7c2	do
styku	styk	k1gInSc2	styk
s	s	k7c7	s
nitroxovými	nitroxový	k2eAgFnPc7d1	nitroxový
směsmi	směs	k1gFnPc7	směs
o	o	k7c6	o
větším	veliký	k2eAgInSc6d2	veliký
obsahu	obsah	k1gInSc6	obsah
kyslíku	kyslík	k1gInSc2	kyslík
než	než	k8xS	než
40	[number]	k4	40
<g/>
%	%	kIx~	%
(	(	kIx(	(
<g/>
lahve	lahev	k1gFnPc1	lahev
<g/>
,	,	kIx,	,
plicní	plicní	k2eAgFnSc1d1	plicní
automatika	automatika	k1gFnSc1	automatika
<g/>
)	)	kIx)	)
tedy	tedy	k9	tedy
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
kyslíkově	kyslíkově	k6eAd1	kyslíkově
kompatibilní	kompatibilní	k2eAgMnSc1d1	kompatibilní
a	a	k8xC	a
dokonale	dokonale	k6eAd1	dokonale
odmaštěna	odmaštěn	k2eAgFnSc1d1	odmaštěn
<g/>
.	.	kIx.	.
</s>
<s>
Lahve	lahev	k1gFnPc1	lahev
plněné	plněný	k2eAgFnPc1d1	plněná
parciální	parciální	k2eAgFnSc7d1	parciální
metodou	metoda	k1gFnSc7	metoda
musejí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
kyslíkově	kyslíkově	k6eAd1	kyslíkově
čisté	čistý	k2eAgNnSc4d1	čisté
i	i	k9	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
výsledná	výsledný	k2eAgFnSc1d1	výsledná
směs	směs	k1gFnSc1	směs
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
méně	málo	k6eAd2	málo
než	než	k8xS	než
40	[number]	k4	40
<g/>
%	%	kIx~	%
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Taková	takový	k3xDgFnSc1	takový
výstroj	výstroj	k1gFnSc1	výstroj
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
nezaměnitelně	zaměnitelně	k6eNd1	zaměnitelně
označena	označit	k5eAaPmNgNnP	označit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
kontaminaci	kontaminace	k1gFnSc3	kontaminace
např.	např.	kA	např.
silikonovými	silikonový	k2eAgInPc7d1	silikonový
tuky	tuk	k1gInPc7	tuk
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
lubrikaci	lubrikace	k1gFnSc3	lubrikace
ostatní	ostatní	k2eAgFnSc2d1	ostatní
potápěčské	potápěčský	k2eAgFnSc2d1	potápěčská
techniky	technika	k1gFnSc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
Standardně	standardně	k6eAd1	standardně
se	se	k3xPyFc4	se
nitroxová	nitroxový	k2eAgFnSc1d1	nitroxový
výstroj	výstroj	k1gFnSc1	výstroj
označuje	označovat	k5eAaImIp3nS	označovat
kombinací	kombinace	k1gFnSc7	kombinace
žluté	žlutý	k2eAgFnPc1d1	žlutá
a	a	k8xC	a
zelené	zelený	k2eAgFnPc1d1	zelená
barvy	barva	k1gFnPc1	barva
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
NITROX	NITROX	kA	NITROX
a	a	k8xC	a
popisem	popis	k1gInSc7	popis
směsi	směs	k1gFnSc2	směs
u	u	k7c2	u
hrdla	hrdlo	k1gNnSc2	hrdlo
lahve	lahev	k1gFnSc2	lahev
<g/>
.	.	kIx.	.
</s>
</p>
