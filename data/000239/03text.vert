<s>
Pekárna	pekárna	k1gFnSc1	pekárna
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgFnSc1d2	menší
nebo	nebo	k8xC	nebo
větší	veliký	k2eAgFnSc1d2	veliký
provozovna	provozovna	k1gFnSc1	provozovna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
peče	péct	k5eAaImIp3nS	péct
pečivo	pečivo	k1gNnSc1	pečivo
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgFnPc1d2	menší
provozovny	provozovna	k1gFnPc1	provozovna
zpravidla	zpravidla	k6eAd1	zpravidla
zásobují	zásobovat	k5eAaImIp3nP	zásobovat
nevelká	velký	k2eNgNnPc1d1	nevelké
sídla	sídlo	k1gNnPc1	sídlo
s	s	k7c7	s
jejich	jejich	k3xOp3gNnSc7	jejich
okolím	okolí	k1gNnSc7	okolí
<g/>
,	,	kIx,	,
častěji	často	k6eAd2	často
používají	používat	k5eAaImIp3nP	používat
nekontinuální	kontinuální	k2eNgFnPc1d1	nekontinuální
neboli	neboli	k8xC	neboli
periodické	periodický	k2eAgFnPc1d1	periodická
pece	pec	k1gFnPc1	pec
s	s	k7c7	s
přetržitým	přetržitý	k2eAgInSc7d1	přetržitý
provozem	provoz	k1gInSc7	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
elektrická	elektrický	k2eAgFnSc1d1	elektrická
výtažná	výtažný	k2eAgFnSc1d1	výtažná
etážová	etážový	k2eAgFnSc1d1	etážová
pec	pec	k1gFnSc1	pec
VP10	VP10	k1gFnSc2	VP10
EK	EK	kA	EK
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgFnPc4	čtyři
etáže	etáž	k1gFnPc4	etáž
<g/>
,	,	kIx,	,
nezávisle	závisle	k6eNd1	závisle
vytápěné	vytápěný	k2eAgFnSc2d1	vytápěná
horní	horní	k2eAgFnSc2d1	horní
a	a	k8xC	a
dolní	dolní	k2eAgFnSc2d1	dolní
řadou	řada	k1gFnSc7	řada
odporových	odporový	k2eAgFnPc2d1	odporová
topných	topný	k2eAgFnPc2d1	topná
tyčí	tyč	k1gFnPc2	tyč
<g/>
,	,	kIx,	,
pečnou	pečnout	k5eAaImIp3nP	pečnout
plochu	plocha	k1gFnSc4	plocha
vytahovací	vytahovací	k2eAgFnSc2d1	vytahovací
etáže	etáž	k1gFnSc2	etáž
tvoří	tvořit	k5eAaImIp3nP	tvořit
síťový	síťový	k2eAgInSc4d1	síťový
pás	pás	k1gInSc4	pás
podložený	podložený	k2eAgInSc4d1	podložený
plechem	plech	k1gInSc7	plech
<g/>
.	.	kIx.	.
</s>
<s>
Podobná	podobný	k2eAgFnSc1d1	podobná
elektrická	elektrický	k2eAgFnSc1d1	elektrická
sázecí	sázecí	k2eAgFnSc1d1	sázecí
etážová	etážový	k2eAgFnSc1d1	etážová
pec	pec	k1gFnSc1	pec
SP10E	SP10E	k1gFnSc2	SP10E
má	mít	k5eAaImIp3nS	mít
opět	opět	k6eAd1	opět
čtyři	čtyři	k4xCgFnPc1	čtyři
etáže	etáž	k1gFnPc1	etáž
vzájemně	vzájemně	k6eAd1	vzájemně
nezávislé	závislý	k2eNgInPc4d1	nezávislý
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
vytápění	vytápění	k1gNnSc2	vytápění
<g/>
,	,	kIx,	,
přívodu	přívod	k1gInSc2	přívod
a	a	k8xC	a
odvodu	odvod	k1gInSc2	odvod
páry	pára	k1gFnSc2	pára
<g/>
.	.	kIx.	.
</s>
<s>
Pečnou	Pečný	k2eAgFnSc4d1	Pečný
plochu	plocha	k1gFnSc4	plocha
tvoří	tvořit	k5eAaImIp3nP	tvořit
betonové	betonový	k2eAgFnPc1d1	betonová
desky	deska	k1gFnPc1	deska
<g/>
,	,	kIx,	,
osazované	osazovaný	k2eAgFnPc1d1	osazovaná
pomocí	pomoc	k1gFnSc7	pomoc
lopaty	lopata	k1gFnSc2	lopata
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejvýhodnější	výhodný	k2eAgInPc4d3	nejvýhodnější
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
obsluhy	obsluha	k1gFnSc2	obsluha
<g/>
,	,	kIx,	,
úsporného	úsporný	k2eAgNnSc2d1	úsporné
řešení	řešení	k1gNnSc2	řešení
pečné	pečný	k2eAgFnSc2d1	pečný
plochy	plocha	k1gFnSc2	plocha
i	i	k8xC	i
menších	malý	k2eAgFnPc2d2	menší
tepelných	tepelný	k2eAgFnPc2d1	tepelná
ztrát	ztráta	k1gFnPc2	ztráta
se	se	k3xPyFc4	se
pokládají	pokládat	k5eAaImIp3nP	pokládat
vozíčkové	vozíčkový	k2eAgFnPc1d1	vozíčkový
konvekční	konvekční	k2eAgFnPc1d1	konvekční
pece	pec	k1gFnPc1	pec
<g/>
.	.	kIx.	.
</s>
<s>
Pečnou	Pečný	k2eAgFnSc4d1	Pečný
plochu	plocha	k1gFnSc4	plocha
těchto	tento	k3xDgFnPc2	tento
pecí	pec	k1gFnPc2	pec
tvoří	tvořit	k5eAaImIp3nP	tvořit
samostatné	samostatný	k2eAgFnPc1d1	samostatná
plochy	plocha	k1gFnPc1	plocha
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
zasunují	zasunovat	k5eAaImIp3nP	zasunovat
nad	nad	k7c7	nad
sebou	se	k3xPyFc7	se
do	do	k7c2	do
rámu	rám	k1gInSc2	rám
vozíku	vozík	k1gInSc2	vozík
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
16	[number]	k4	16
až	až	k9	až
24	[number]	k4	24
<g/>
.	.	kIx.	.
</s>
<s>
Vozík	vozík	k1gInSc1	vozík
s	s	k7c7	s
osazenými	osazený	k2eAgInPc7d1	osazený
plechy	plech	k1gInPc7	plech
se	se	k3xPyFc4	se
zaváží	zavážit	k5eAaPmIp3nS	zavážit
do	do	k7c2	do
boxu	box	k1gInSc2	box
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
upevní	upevnit	k5eAaPmIp3nS	upevnit
na	na	k7c4	na
závěsné	závěsný	k2eAgNnSc4d1	závěsné
a	a	k8xC	a
otáčecí	otáčecí	k2eAgNnSc4d1	otáčecí
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
zavření	zavření	k1gNnSc4	zavření
dveří	dveře	k1gFnPc2	dveře
boxu	box	k1gInSc2	box
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
zařízení	zařízení	k1gNnSc1	zařízení
uvede	uvést	k5eAaPmIp3nS	uvést
do	do	k7c2	do
chodu	chod	k1gInSc2	chod
a	a	k8xC	a
vozík	vozík	k1gInSc1	vozík
s	s	k7c7	s
plechy	plech	k1gInPc7	plech
rotuje	rotovat	k5eAaImIp3nS	rotovat
kolem	kolem	k7c2	kolem
svislé	svislý	k2eAgFnSc2d1	svislá
osy	osa	k1gFnSc2	osa
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
pečení	pečení	k1gNnSc2	pečení
<g/>
.	.	kIx.	.
</s>
<s>
Teplo	teplo	k1gNnSc1	teplo
se	se	k3xPyFc4	se
předává	předávat	k5eAaImIp3nS	předávat
pečivu	pečivo	k1gNnSc3	pečivo
převážně	převážně	k6eAd1	převážně
prouděním	proudění	k1gNnSc7	proudění
horkého	horký	k2eAgInSc2d1	horký
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
starších	starý	k2eAgFnPc6d2	starší
pekárnách	pekárna	k1gFnPc6	pekárna
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
ještě	ještě	k9	ještě
parní	parní	k2eAgFnPc1d1	parní
sázecí	sázecí	k2eAgFnPc1d1	sázecí
pece	pec	k1gFnPc1	pec
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
vytápěny	vytápět	k5eAaImNgFnP	vytápět
nepřímo	přímo	k6eNd1	přímo
pomocí	pomocí	k7c2	pomocí
Perkinsových	Perkinsův	k2eAgFnPc2d1	Perkinsova
trubek	trubka	k1gFnPc2	trubka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
zataveny	zataven	k2eAgFnPc1d1	zatavena
a	a	k8xC	a
z	z	k7c2	z
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
naplněny	naplněn	k2eAgInPc1d1	naplněn
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Snížené	snížený	k2eAgInPc1d1	snížený
konce	konec	k1gInPc1	konec
těchto	tento	k3xDgFnPc2	tento
trubek	trubka	k1gFnPc2	trubka
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
do	do	k7c2	do
topeniště	topeniště	k1gNnSc2	topeniště
a	a	k8xC	a
pára	pára	k1gFnSc1	pára
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
tvoří	tvořit	k5eAaImIp3nP	tvořit
<g/>
,	,	kIx,	,
předává	předávat	k5eAaImIp3nS	předávat
teplo	teplo	k6eAd1	teplo
pečnému	pečný	k2eAgInSc3d1	pečný
prostoru	prostor	k1gInSc3	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
pece	pec	k1gFnPc1	pec
se	se	k3xPyFc4	se
stavěly	stavět	k5eAaImAgFnP	stavět
jako	jako	k8xC	jako
dvou	dva	k4xCgMnPc2	dva
nebo	nebo	k8xC	nebo
tříetážové	tříetážový	k2eAgFnSc6d1	tříetážová
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
jsou	být	k5eAaImIp3nP	být
menší	malý	k2eAgFnPc1d2	menší
provozovny	provozovna	k1gFnPc1	provozovna
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
ekonomického	ekonomický	k2eAgNnSc2d1	ekonomické
a	a	k8xC	a
potřeby	potřeba	k1gFnPc4	potřeba
pracovní	pracovní	k2eAgFnSc2d1	pracovní
síly	síla	k1gFnSc2	síla
náročnější	náročný	k2eAgNnSc1d2	náročnější
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc1	jejich
výrobky	výrobek	k1gInPc1	výrobek
jsou	být	k5eAaImIp3nP	být
oblíbené	oblíbený	k2eAgInPc1d1	oblíbený
pro	pro	k7c4	pro
kvalitu	kvalita	k1gFnSc4	kvalita
<g/>
,	,	kIx,	,
čerstvost	čerstvost	k1gFnSc4	čerstvost
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
osobitý	osobitý	k2eAgInSc4d1	osobitý
sortiment	sortiment	k1gInSc4	sortiment
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
spotřebitelé	spotřebitel	k1gMnPc1	spotřebitel
v	v	k7c6	v
menších	malý	k2eAgFnPc6d2	menší
oblastech	oblast	k1gFnPc6	oblast
výrobce	výrobce	k1gMnSc2	výrobce
znají	znát	k5eAaImIp3nP	znát
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgFnPc1d2	veliký
provozovny	provozovna	k1gFnPc1	provozovna
(	(	kIx(	(
<g/>
velkopekárny	velkopekárna	k1gFnPc1	velkopekárna
<g/>
)	)	kIx)	)
zpravidla	zpravidla	k6eAd1	zpravidla
zásobují	zásobovat	k5eAaImIp3nP	zásobovat
větší	veliký	k2eAgNnPc4d2	veliký
spádová	spádový	k2eAgNnPc4d1	spádové
území	území	k1gNnPc4	území
či	či	k8xC	či
velká	velký	k2eAgNnPc4d1	velké
města	město	k1gNnPc4	město
<g/>
,	,	kIx,	,
častěji	často	k6eAd2	často
používají	používat	k5eAaImIp3nP	používat
zařízení	zařízení	k1gNnPc1	zařízení
s	s	k7c7	s
kontinuálním	kontinuální	k2eAgInSc7d1	kontinuální
(	(	kIx(	(
<g/>
nepřetržitým	přetržitý	k2eNgInSc7d1	nepřetržitý
<g/>
)	)	kIx)	)
procesem	proces	k1gInSc7	proces
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
pece	pec	k1gFnPc1	pec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
zkrácené	zkrácený	k2eAgInPc1d1	zkrácený
postupy	postup	k1gInPc1	postup
přípravy	příprava	k1gFnSc2	příprava
těst	těsto	k1gNnPc2	těsto
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
rychlohnětacích	rychlohnětací	k2eAgInPc2d1	rychlohnětací
strojů	stroj	k1gInPc2	stroj
-	-	kIx~	-
mixérů	mixér	k1gInPc2	mixér
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhody	nevýhoda	k1gFnPc1	nevýhoda
jsou	být	k5eAaImIp3nP	být
zvýšené	zvýšený	k2eAgInPc4d1	zvýšený
náklady	náklad	k1gInPc4	náklad
na	na	k7c4	na
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
droždí	droždí	k1gNnSc4	droždí
a	a	k8xC	a
na	na	k7c4	na
oxidační	oxidační	k2eAgNnPc4d1	oxidační
zlepšovadla	zlepšovadlo	k1gNnPc4	zlepšovadlo
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
nás	my	k3xPp1nPc4	my
nezvyklý	zvyklý	k2eNgInSc1d1	nezvyklý
charakter	charakter	k1gInSc1	charakter
výrobku	výrobek	k1gInSc2	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Pečivo	pečivo	k1gNnSc1	pečivo
takto	takto	k6eAd1	takto
vyrobené	vyrobený	k2eAgNnSc1d1	vyrobené
má	mít	k5eAaImIp3nS	mít
příliš	příliš	k6eAd1	příliš
jemně	jemně	k6eAd1	jemně
pórovitou	pórovitý	k2eAgFnSc4d1	pórovitá
<g/>
,	,	kIx,	,
až	až	k9	až
vatovitou	vatovitý	k2eAgFnSc4d1	vatovitá
střídu	střída	k1gFnSc4	střída
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
špatně	špatně	k6eAd1	špatně
krájí	krájet	k5eAaImIp3nS	krájet
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
méně	málo	k6eAd2	málo
výraznou	výrazný	k2eAgFnSc4d1	výrazná
chuť	chuť	k1gFnSc4	chuť
a	a	k8xC	a
vůni	vůně	k1gFnSc4	vůně
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokud	pokud	k8xS	pokud
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
nejsou	být	k5eNaImIp3nP	být
přidány	přidán	k2eAgInPc4d1	přidán
další	další	k2eAgInPc4d1	další
typy	typ	k1gInPc4	typ
zlepšovadel	zlepšovadlo	k1gNnPc2	zlepšovadlo
<g/>
,	,	kIx,	,
také	také	k9	také
dříve	dříve	k6eAd2	dříve
tvrdne	tvrdnout	k5eAaImIp3nS	tvrdnout
<g/>
.	.	kIx.	.
</s>
<s>
Mívají	mívat	k5eAaImIp3nP	mívat
také	také	k9	také
průběžnou	průběžný	k2eAgFnSc4d1	průběžná
kynárnu	kynárna	k1gFnSc4	kynárna
(	(	kIx(	(
<g/>
prostor	prostor	k1gInSc4	prostor
-	-	kIx~	-
box	box	k1gInSc4	box
pro	pro	k7c4	pro
dokynutí	dokynutí	k1gNnSc4	dokynutí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yRgInPc1	který
těstové	těstový	k2eAgInPc1d1	těstový
kusy	kus	k1gInPc1	kus
plynule	plynule	k6eAd1	plynule
procházejí	procházet	k5eAaImIp3nP	procházet
na	na	k7c4	na
sázecí	sázecí	k2eAgInSc4d1	sázecí
stůl	stůl	k1gInSc4	stůl
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
probíhá	probíhat	k5eAaImIp3nS	probíhat
vlažení	vlažení	k1gNnSc4	vlažení
i	i	k8xC	i
sypání	sypání	k1gNnSc4	sypání
<g/>
.	.	kIx.	.
</s>
<s>
Funkci	funkce	k1gFnSc4	funkce
vlažícího	vlažící	k2eAgNnSc2d1	vlažící
zařízení	zařízení	k1gNnSc2	zařízení
plní	plnit	k5eAaImIp3nS	plnit
buď	buď	k8xC	buď
rotující	rotující	k2eAgInSc1d1	rotující
kartáč	kartáč	k1gInSc1	kartáč
smáčený	smáčený	k2eAgInSc1d1	smáčený
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
řada	řada	k1gFnSc1	řada
trysek	tryska	k1gFnPc2	tryska
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
rozprašují	rozprašovat	k5eAaImIp3nP	rozprašovat
na	na	k7c4	na
pečivo	pečivo	k1gNnSc4	pečivo
vodní	vodní	k2eAgFnSc4d1	vodní
mlhu	mlha	k1gFnSc4	mlha
<g/>
,	,	kIx,	,
k	k	k7c3	k
sypání	sypání	k1gNnSc3	sypání
slouží	sloužit	k5eAaImIp3nS	sloužit
rotující	rotující	k2eAgInSc4d1	rotující
drážkovaný	drážkovaný	k2eAgInSc4d1	drážkovaný
válec	válec	k1gInSc4	válec
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
sázení	sázení	k1gNnSc1	sázení
do	do	k7c2	do
průběžných	průběžný	k2eAgFnPc2d1	průběžná
pecí	pec	k1gFnPc2	pec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
výrobky	výrobek	k1gInPc7	výrobek
pečou	péct	k5eAaImIp3nP	péct
bez	bez	k7c2	bez
plechů	plech	k1gInPc2	plech
<g/>
,	,	kIx,	,
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
ocelovém	ocelový	k2eAgInSc6d1	ocelový
pletivovém	pletivový	k2eAgInSc6d1	pletivový
dopravníku	dopravník	k1gInSc6	dopravník
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
pečnou	pečný	k2eAgFnSc4d1	pečný
plochu	plocha	k1gFnSc4	plocha
a	a	k8xC	a
upečené	upečený	k2eAgNnSc4d1	upečené
pečivo	pečivo	k1gNnSc4	pečivo
potom	potom	k6eAd1	potom
z	z	k7c2	z
něho	on	k3xPp3gMnSc2	on
padá	padat	k5eAaImIp3nS	padat
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
přepravek	přepravka	k1gFnPc2	přepravka
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgFnPc1d2	veliký
i	i	k8xC	i
menší	malý	k2eAgFnPc1d2	menší
provozovny	provozovna	k1gFnPc1	provozovna
mají	mít	k5eAaImIp3nP	mít
ještě	ještě	k9	ještě
zařízení	zařízení	k1gNnSc4	zařízení
na	na	k7c4	na
krájení	krájení	k1gNnSc4	krájení
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
pečiva	pečivo	k1gNnSc2	pečivo
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
chléb	chléb	k1gInSc4	chléb
nebo	nebo	k8xC	nebo
vánočky	vánočka	k1gFnPc4	vánočka
a	a	k8xC	a
na	na	k7c6	na
balení	balení	k1gNnSc6	balení
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
pečiva	pečivo	k1gNnSc2	pečivo
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
buď	buď	k8xC	buď
do	do	k7c2	do
mírně	mírně	k6eAd1	mírně
lepkavé	lepkavý	k2eAgFnSc2d1	lepkavá
průhledné	průhledný	k2eAgFnSc2d1	průhledná
tenké	tenký	k2eAgFnSc2d1	tenká
polyetylénové	polyetylénový	k2eAgFnSc2d1	polyetylénová
fólie	fólie	k1gFnSc2	fólie
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jiné	jiná	k1gFnPc1	jiná
do	do	k7c2	do
nejčastěji	často	k6eAd3	často
papírových	papírový	k2eAgInPc2d1	papírový
sáčků	sáček	k1gInPc2	sáček
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
maloprovozu	maloprovoz	k1gInSc2	maloprovoz
tato	tento	k3xDgNnPc1	tento
zařízení	zařízení	k1gNnPc1	zařízení
bývají	bývat	k5eAaImIp3nP	bývat
ovládána	ovládat	k5eAaImNgNnP	ovládat
ručně	ručně	k6eAd1	ručně
<g/>
,	,	kIx,	,
ve	v	k7c6	v
větších	veliký	k2eAgFnPc6d2	veliký
provozovnách	provozovna	k1gFnPc6	provozovna
bývají	bývat	k5eAaImIp3nP	bývat
více	hodně	k6eAd2	hodně
automatizovaná	automatizovaný	k2eAgNnPc1d1	automatizované
<g/>
.	.	kIx.	.
</s>
