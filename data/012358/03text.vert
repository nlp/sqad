<p>
<s>
Ještěrka	ještěrka	k1gFnSc1	ještěrka
zední	zední	k2eAgFnSc1d1	zední
(	(	kIx(	(
<g/>
Podarcis	Podarcis	k1gFnSc1	Podarcis
muralis	muralis	k1gFnSc1	muralis
(	(	kIx(	(
<g/>
Laurenti	Laurent	k1gMnPc1	Laurent
<g/>
,	,	kIx,	,
1768	[number]	k4	1768
<g/>
))	))	k?	))
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
ještěrkovití	ještěrkovitý	k2eAgMnPc1d1	ještěrkovitý
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
kriticky	kriticky	k6eAd1	kriticky
ohroženým	ohrožený	k2eAgMnSc7d1	ohrožený
druhem	druh	k1gMnSc7	druh
plazů	plaz	k1gInPc2	plaz
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nP	živit
se	s	k7c7	s
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Synonyma	synonymum	k1gNnSc2	synonymum
==	==	k?	==
</s>
</p>
<p>
<s>
Lacerta	Lacerta	k1gFnSc1	Lacerta
muralis	muralis	k1gFnSc1	muralis
(	(	kIx(	(
<g/>
Laurenti	Laurent	k1gMnPc1	Laurent
<g/>
,	,	kIx,	,
1768	[number]	k4	1768
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Zbarvení	zbarvení	k1gNnSc1	zbarvení
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
rozmanité	rozmanitý	k2eAgNnSc1d1	rozmanité
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
převládá	převládat	k5eAaImIp3nS	převládat
hnědá	hnědý	k2eAgFnSc1d1	hnědá
barva	barva	k1gFnSc1	barva
s	s	k7c7	s
výrazným	výrazný	k2eAgNnSc7d1	výrazné
síťováním	síťování	k1gNnSc7	síťování
<g/>
,	,	kIx,	,
samci	samec	k1gMnPc1	samec
mají	mít	k5eAaImIp3nP	mít
nápadněji	nápadně	k6eAd2	nápadně
zbarvené	zbarvený	k2eAgNnSc4d1	zbarvené
a	a	k8xC	a
sytější	sytý	k2eAgNnSc4d2	sytější
břicho	břicho	k1gNnSc4	břicho
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
s	s	k7c7	s
černými	černý	k2eAgFnPc7d1	černá
skvrnami	skvrna	k1gFnPc7	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
okolo	okolo	k7c2	okolo
20	[number]	k4	20
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Oblast	oblast	k1gFnSc1	oblast
rozšíření	rozšíření	k1gNnSc2	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Ještěrka	ještěrka	k1gFnSc1	ještěrka
zední	zední	k1gNnSc2	zední
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
jižní	jižní	k2eAgFnSc6d1	jižní
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
oblast	oblast	k1gFnSc1	oblast
rozšíření	rozšíření	k1gNnSc2	rozšíření
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
až	až	k9	až
do	do	k7c2	do
západní	západní	k2eAgFnSc2d1	západní
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
žije	žít	k5eAaImIp3nS	žít
především	především	k9	především
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
a	a	k8xC	a
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
zastižení	zastižení	k1gNnSc3	zastižení
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
dvou	dva	k4xCgFnPc6	dva
lokalitách	lokalita	k1gFnPc6	lokalita
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
Štramberském	štramberský	k2eAgInSc6d1	štramberský
krasu	kras	k1gInSc6	kras
(	(	kIx(	(
<g/>
350	[number]	k4	350
<g/>
–	–	k?	–
<g/>
500	[number]	k4	500
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Štramberk	Štramberk	k1gInSc4	Štramberk
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
stabilní	stabilní	k2eAgFnSc6d1	stabilní
populaci	populace	k1gFnSc6	populace
asi	asi	k9	asi
150	[number]	k4	150
jedinců	jedinec	k1gMnPc2	jedinec
v	v	k7c6	v
bývalém	bývalý	k2eAgInSc6d1	bývalý
vápencovém	vápencový	k2eAgInSc6d1	vápencový
lomu	lom	k1gInSc6	lom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stanoviště	stanoviště	k1gNnSc2	stanoviště
==	==	k?	==
</s>
</p>
<p>
<s>
Ještěrka	ještěrka	k1gFnSc1	ještěrka
zední	zedeň	k1gFnPc2	zedeň
obývá	obývat	k5eAaImIp3nS	obývat
slunné	slunný	k2eAgNnSc1d1	slunné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
vlhčí	vlhký	k2eAgInPc1d2	vlhčí
okraje	okraj	k1gInPc1	okraj
listnatých	listnatý	k2eAgInPc2d1	listnatý
lesů	les	k1gInPc2	les
<g/>
,	,	kIx,	,
skalnaté	skalnatý	k2eAgInPc1d1	skalnatý
svahy	svah	k1gInPc1	svah
kopců	kopec	k1gInPc2	kopec
<g/>
,	,	kIx,	,
zarostlé	zarostlý	k2eAgFnSc2d1	zarostlá
skalky	skalka	k1gFnSc2	skalka
a	a	k8xC	a
hromady	hromada	k1gFnSc2	hromada
kamenů	kámen	k1gInPc2	kámen
<g/>
,	,	kIx,	,
náspy	násep	k1gInPc1	násep
silnic	silnice	k1gFnPc2	silnice
a	a	k8xC	a
železnic	železnice	k1gFnPc2	železnice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
přehradní	přehradní	k2eAgFnSc2d1	přehradní
hráze	hráz	k1gFnSc2	hráz
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
zastihnout	zastihnout	k5eAaPmF	zastihnout
i	i	k9	i
na	na	k7c6	na
chemicky	chemicky	k6eAd1	chemicky
neošetřovaných	ošetřovaný	k2eNgFnPc6d1	neošetřovaná
vinicích	vinice	k1gFnPc6	vinice
<g/>
,	,	kIx,	,
zříceninách	zřícenina	k1gFnPc6	zřícenina
hradů	hrad	k1gInPc2	hrad
a	a	k8xC	a
polorozbořených	polorozbořený	k2eAgFnPc6d1	polorozbořená
zídkách	zídka	k1gFnPc6	zídka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ráda	rád	k2eAgFnSc1d1	ráda
vyhřívá	vyhřívat	k5eAaImIp3nS	vyhřívat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Způsob	způsob	k1gInSc1	způsob
života	život	k1gInSc2	život
==	==	k?	==
</s>
</p>
<p>
<s>
Patrně	patrně	k6eAd1	patrně
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
naši	náš	k3xOp1gFnSc4	náš
nejmrštnější	mrštný	k2eAgFnSc4d3	nejmrštnější
ještěrku	ještěrka	k1gFnSc4	ještěrka
<g/>
.	.	kIx.	.
</s>
<s>
Nejenže	nejenže	k6eAd1	nejenže
dokáže	dokázat	k5eAaPmIp3nS	dokázat
lézt	lézt	k5eAaImF	lézt
téměř	téměř	k6eAd1	téměř
po	po	k7c6	po
kolmých	kolmý	k2eAgFnPc6d1	kolmá
stěnách	stěna	k1gFnPc6	stěna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
díky	díky	k7c3	díky
poměrně	poměrně	k6eAd1	poměrně
dlouhým	dlouhý	k2eAgFnPc3d1	dlouhá
nohám	noha	k1gFnPc3	noha
dokonce	dokonce	k9	dokonce
i	i	k9	i
výborně	výborně	k6eAd1	výborně
skákat	skákat	k5eAaImF	skákat
<g/>
.	.	kIx.	.
</s>
<s>
Zimuje	zimovat	k5eAaImIp3nS	zimovat
jen	jen	k9	jen
krátce	krátce	k6eAd1	krátce
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
je	být	k5eAaImIp3nS	být
zdokumentováno	zdokumentován	k2eAgNnSc1d1	zdokumentováno
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
příznivějším	příznivý	k2eAgNnSc6d2	příznivější
klimatu	klima	k1gNnSc6	klima
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Evropy	Evropa	k1gFnSc2	Evropa
nemusí	muset	k5eNaImIp3nS	muset
hibernovat	hibernovat	k5eAaImF	hibernovat
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
===	===	k?	===
</s>
</p>
<p>
<s>
Samečkové	sameček	k1gMnPc1	sameček
jsou	být	k5eAaImIp3nP	být
silně	silně	k6eAd1	silně
teritoriální	teritoriální	k2eAgFnPc1d1	teritoriální
a	a	k8xC	a
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
svádějí	svádět	k5eAaImIp3nP	svádět
drsné	drsný	k2eAgInPc1d1	drsný
boje	boj	k1gInPc1	boj
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yQgFnPc6	který
ztrácejí	ztrácet	k5eAaImIp3nP	ztrácet
ostražitost	ostražitost	k1gFnSc4	ostražitost
a	a	k8xC	a
nechají	nechat	k5eAaPmIp3nP	nechat
se	se	k3xPyFc4	se
snadno	snadno	k6eAd1	snadno
polapit	polapit	k5eAaPmF	polapit
<g/>
.	.	kIx.	.
</s>
<s>
Páří	pářit	k5eAaImIp3nS	pářit
se	se	k3xPyFc4	se
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
samičky	samička	k1gFnSc2	samička
snášejí	snášet	k5eAaImIp3nP	snášet
do	do	k7c2	do
jamek	jamka	k1gFnPc2	jamka
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
9	[number]	k4	9
vajíček	vajíčko	k1gNnPc2	vajíčko
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
po	po	k7c6	po
několika	několik	k4yIc6	několik
týdnech	týden	k1gInPc6	týden
inkubace	inkubace	k1gFnSc2	inkubace
líhnou	líhnout	k5eAaImIp3nP	líhnout
mláďata	mládě	k1gNnPc4	mládě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Ještěrka	ještěrka	k1gFnSc1	ještěrka
zední	zední	k1gNnSc2	zední
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
hmyzem	hmyz	k1gInSc7	hmyz
všeho	všecek	k3xTgInSc2	všecek
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
pavouky	pavouk	k1gMnPc7	pavouk
<g/>
,	,	kIx,	,
červy	červ	k1gMnPc7	červ
či	či	k8xC	či
housenkami	housenka	k1gFnPc7	housenka
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
má	mít	k5eAaImIp3nS	mít
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
i	i	k9	i
některé	některý	k3yIgInPc4	některý
druhy	druh	k1gInPc4	druh
herpetofágních	herpetofágní	k2eAgFnPc2d1	herpetofágní
(	(	kIx(	(
<g/>
živících	živící	k2eAgFnPc2d1	živící
se	se	k3xPyFc4	se
plazy	plaz	k1gInPc4	plaz
<g/>
)	)	kIx)	)
užovek	užovka	k1gFnPc2	užovka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
MORAVEC	Moravec	k1gMnSc1	Moravec
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Plazi	plaz	k1gMnPc1	plaz
<g/>
.	.	kIx.	.
</s>
<s>
Fauna	fauna	k1gFnSc1	fauna
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
531	[number]	k4	531
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
2416	[number]	k4	2416
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Podarcis	Podarcis	k1gFnSc1	Podarcis
muralis	muralis	k1gFnSc1	muralis
–	–	k?	–
ještěrka	ještěrka	k1gFnSc1	ještěrka
zední	zední	k2eAgFnSc1d1	zední
<g/>
,	,	kIx,	,
s.	s.	k?	s.
180	[number]	k4	180
<g/>
–	–	k?	–
<g/>
202	[number]	k4	202
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
ještěrka	ještěrka	k1gFnSc1	ještěrka
zední	zednit	k5eAaImIp3nS	zednit
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
ještěrka	ještěrka	k1gFnSc1	ještěrka
zední	zednět	k5eAaPmIp3nS	zednět
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
