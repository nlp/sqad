<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Hašek	Hašek	k1gMnSc1	Hašek
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1883	[number]	k4	1883
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1923	[number]	k4	1923
Lipnice	Lipnice	k1gFnSc1	Lipnice
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
publicista	publicista	k1gMnSc1	publicista
a	a	k8xC	a
novinář	novinář	k1gMnSc1	novinář
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
nejpřekládanější	překládaný	k2eAgFnSc2d3	nejpřekládanější
knihy	kniha	k1gFnSc2	kniha
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
(	(	kIx(	(
<g/>
58	[number]	k4	58
jazyků	jazyk	k1gInPc2	jazyk
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
proslulých	proslulý	k2eAgInPc2d1	proslulý
Osudů	osud	k1gInPc2	osud
dobrého	dobrý	k2eAgMnSc2d1	dobrý
vojáka	voják	k1gMnSc2	voják
Švejka	Švejk	k1gMnSc4	Švejk
za	za	k7c2	za
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
