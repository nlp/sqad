<s>
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1936	[number]	k4	1936
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
Vlčice-Hrádeček	Vlčice-Hrádečka	k1gFnPc2	Vlčice-Hrádečka
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
esejista	esejista	k1gMnSc1	esejista
<g/>
,	,	kIx,	,
kritik	kritik	k1gMnSc1	kritik
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
politik	politik	k1gMnSc1	politik
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
devátým	devátý	k4xOgFnPc3	devátý
a	a	k8xC	a
posledním	poslední	k2eAgMnSc7d1	poslední
prezidentem	prezident	k1gMnSc7	prezident
Československa	Československo	k1gNnSc2	Československo
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
a	a	k8xC	a
prvním	první	k4xOgMnSc7	první
prezidentem	prezident	k1gMnSc7	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
Na	na	k7c6	na
zábradlí	zábradlí	k1gNnSc6	zábradlí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jej	on	k3xPp3gMnSc4	on
také	také	k6eAd1	také
proslavily	proslavit	k5eAaPmAgFnP	proslavit
hry	hra	k1gFnPc1	hra
Zahradní	zahradní	k2eAgFnSc4d1	zahradní
slavnost	slavnost	k1gFnSc4	slavnost
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
a	a	k8xC	a
Vyrozumění	vyrozumění	k1gNnSc2	vyrozumění
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
kolem	kolem	k7c2	kolem
Pražského	pražský	k2eAgNnSc2d1	Pražské
jara	jaro	k1gNnSc2	jaro
se	se	k3xPyFc4	se
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
politické	politický	k2eAgFnSc2d1	politická
diskuse	diskuse	k1gFnSc2	diskuse
a	a	k8xC	a
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
zavedení	zavedení	k1gNnSc4	zavedení
demokratické	demokratický	k2eAgFnSc2d1	demokratická
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
násilném	násilný	k2eAgNnSc6d1	násilné
potlačení	potlačení	k1gNnSc6	potlačení
reforem	reforma	k1gFnPc2	reforma
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
invazí	invaze	k1gFnSc7	invaze
států	stát	k1gInPc2	stát
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
byl	být	k5eAaImAgInS	být
postižen	postihnout	k5eAaPmNgInS	postihnout
zákazem	zákaz	k1gInSc7	zákaz
publikovat	publikovat	k5eAaBmF	publikovat
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
prominentních	prominentní	k2eAgMnPc2d1	prominentní
disidentů	disident	k1gMnPc2	disident
<g/>
,	,	kIx,	,
kritiků	kritik	k1gMnPc2	kritik
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
normalizačního	normalizační	k2eAgInSc2d1	normalizační
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
politických	politický	k2eAgMnPc2d1	politický
vězňů	vězeň	k1gMnPc2	vězeň
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
spoluzakladatelem	spoluzakladatel	k1gMnSc7	spoluzakladatel
a	a	k8xC	a
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
mluvčích	mluvčí	k1gMnPc2	mluvčí
občanské	občanský	k2eAgFnSc2d1	občanská
iniciativy	iniciativa	k1gFnSc2	iniciativa
za	za	k7c4	za
dodržování	dodržování	k1gNnSc4	dodržování
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
Charta	charta	k1gFnSc1	charta
77	[number]	k4	77
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
upevnilo	upevnit	k5eAaPmAgNnS	upevnit
jeho	jeho	k3xOp3gFnSc4	jeho
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
prestiž	prestiž	k1gFnSc4	prestiž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
mu	on	k3xPp3gNnSc3	on
vyneslo	vynést	k5eAaPmAgNnS	vynést
celkem	celkem	k6eAd1	celkem
asi	asi	k9	asi
pět	pět	k4xCc1	pět
let	léto	k1gNnPc2	léto
věznění	věznění	k1gNnSc2	věznění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
napsal	napsat	k5eAaBmAgInS	napsat
kromě	kromě	k7c2	kromě
dalších	další	k2eAgFnPc2d1	další
divadelních	divadelní	k2eAgFnPc2d1	divadelní
her	hra	k1gFnPc2	hra
také	také	k6eAd1	také
vlivné	vlivný	k2eAgFnSc2d1	vlivná
eseje	esej	k1gFnSc2	esej
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Moc	moc	k6eAd1	moc
bezmocných	bezmocný	k2eAgMnPc2d1	bezmocný
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
sametové	sametový	k2eAgFnSc2d1	sametová
revoluce	revoluce	k1gFnSc2	revoluce
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1989	[number]	k4	1989
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
spoluzakladatelů	spoluzakladatel	k1gMnPc2	spoluzakladatel
protikomunistického	protikomunistický	k2eAgNnSc2d1	protikomunistické
hnutí	hnutí	k1gNnSc2	hnutí
Občanské	občanský	k2eAgNnSc1d1	občanské
fórum	fórum	k1gNnSc1	fórum
a	a	k8xC	a
jako	jako	k8xS	jako
jeho	jeho	k3xOp3gMnSc1	jeho
kandidát	kandidát	k1gMnSc1	kandidát
byl	být	k5eAaImAgMnS	být
29	[number]	k4	29
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1989	[number]	k4	1989
zvolen	zvolit	k5eAaPmNgMnS	zvolit
prezidentem	prezident	k1gMnSc7	prezident
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
zásadní	zásadní	k2eAgInSc4d1	zásadní
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
směřování	směřování	k1gNnSc4	směřování
země	zem	k1gFnSc2	zem
k	k	k7c3	k
parlamentní	parlamentní	k2eAgFnSc3d1	parlamentní
demokracii	demokracie	k1gFnSc3	demokracie
a	a	k8xC	a
zapojení	zapojení	k1gNnSc1	zapojení
do	do	k7c2	do
politických	politický	k2eAgFnPc2d1	politická
struktur	struktura	k1gFnPc2	struktura
západní	západní	k2eAgFnSc2d1	západní
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
nepodařilo	podařit	k5eNaPmAgNnS	podařit
zabránit	zabránit	k5eAaPmF	zabránit
rozpadu	rozpad	k1gInSc3	rozpad
československého	československý	k2eAgInSc2d1	československý
státu	stát	k1gInSc2	stát
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
samostatné	samostatný	k2eAgFnPc4d1	samostatná
republiky	republika	k1gFnPc4	republika
<g/>
:	:	kIx,	:
Českou	český	k2eAgFnSc4d1	Česká
a	a	k8xC	a
Slovenskou	slovenský	k2eAgFnSc4d1	slovenská
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
byl	být	k5eAaImAgInS	být
po	po	k7c4	po
dvě	dva	k4xCgFnPc4	dva
funkční	funkční	k2eAgFnPc4d1	funkční
období	období	k1gNnSc6	období
prezidentem	prezident	k1gMnSc7	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
prezident	prezident	k1gMnSc1	prezident
vyvedl	vyvést	k5eAaPmAgMnS	vyvést
Československo	Československo	k1gNnSc4	Československo
z	z	k7c2	z
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
a	a	k8xC	a
výrazně	výrazně	k6eAd1	výrazně
přispěl	přispět	k5eAaPmAgMnS	přispět
ke	k	k7c3	k
vstupu	vstup	k1gInSc3	vstup
nástupnické	nástupnický	k2eAgFnSc2d1	nástupnická
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
do	do	k7c2	do
Severoatlantické	severoatlantický	k2eAgFnSc2d1	Severoatlantická
aliance	aliance	k1gFnSc2	aliance
(	(	kIx(	(
<g/>
NATO	NATO	kA	NATO
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Výrazně	výrazně	k6eAd1	výrazně
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
také	také	k9	také
přijetí	přijetí	k1gNnSc4	přijetí
země	zem	k1gFnSc2	zem
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
uskutečněné	uskutečněný	k2eAgInPc1d1	uskutečněný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
literát	literát	k1gMnSc1	literát
se	se	k3xPyFc4	se
světově	světově	k6eAd1	světově
proslavil	proslavit	k5eAaPmAgMnS	proslavit
svými	svůj	k3xOyFgNnPc7	svůj
dramaty	drama	k1gNnPc7	drama
v	v	k7c4	v
duchu	duch	k1gMnSc3	duch
absurdního	absurdní	k2eAgNnSc2d1	absurdní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
zabýval	zabývat	k5eAaImAgMnS	zabývat
tématy	téma	k1gNnPc7	téma
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
byrokracie	byrokracie	k1gFnSc2	byrokracie
a	a	k8xC	a
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
svým	svůj	k3xOyFgNnSc7	svůj
esejistickým	esejistický	k2eAgNnSc7d1	esejistické
dílem	dílo	k1gNnSc7	dílo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
esejích	esej	k1gFnPc6	esej
a	a	k8xC	a
dopisech	dopis	k1gInPc6	dopis
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
se	se	k3xPyFc4	se
vedle	vedle	k7c2	vedle
politických	politický	k2eAgFnPc2d1	politická
analýz	analýza	k1gFnPc2	analýza
zaobíral	zaobírat	k5eAaImAgInS	zaobírat
filozofickými	filozofický	k2eAgFnPc7d1	filozofická
otázkami	otázka	k1gFnPc7	otázka
svobody	svoboda	k1gFnSc2	svoboda
<g/>
,	,	kIx,	,
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
morálky	morálka	k1gFnSc2	morálka
či	či	k8xC	či
transcendence	transcendence	k1gFnSc2	transcendence
<g/>
.	.	kIx.	.
</s>
<s>
Věnoval	věnovat	k5eAaImAgInS	věnovat
se	se	k3xPyFc4	se
také	také	k9	také
experimentální	experimentální	k2eAgFnSc3d1	experimentální
poezii	poezie	k1gFnSc3	poezie
<g/>
;	;	kIx,	;
básně	báseň	k1gFnPc1	báseň
psané	psaný	k2eAgFnPc1d1	psaná
především	především	k9	především
v	v	k7c4	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jeho	jeho	k3xOp3gFnSc1	jeho
sbírka	sbírka	k1gFnSc1	sbírka
Antikódy	Antikóda	k1gFnSc2	Antikóda
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
ve	v	k7c6	v
známé	známá	k1gFnSc6	známá
pražské	pražský	k2eAgFnSc6d1	Pražská
podnikatelské	podnikatelský	k2eAgFnSc3d1	podnikatelská
a	a	k8xC	a
intelektuálské	intelektuálský	k2eAgFnSc3d1	intelektuálská
rodině	rodina	k1gFnSc3	rodina
Václava	Václav	k1gMnSc2	Václav
M.	M.	kA	M.
Havla	Havel	k1gMnSc2	Havel
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
–	–	k?	–
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
manželky	manželka	k1gFnPc1	manželka
Boženy	Božena	k1gFnSc2	Božena
<g/>
,	,	kIx,	,
rozené	rozený	k2eAgFnSc2d1	rozená
Vavrečkové	Vavrečková	k1gFnSc2	Vavrečková
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
–	–	k?	–
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vnukem	vnuk	k1gMnSc7	vnuk
československého	československý	k2eAgNnSc2d1	Československé
diplomata	diplomat	k1gMnSc2	diplomat
Huga	Hugo	k1gMnSc2	Hugo
Vavrečky	Vavrečka	k1gFnSc2	Vavrečka
i	i	k8xC	i
stavitele	stavitel	k1gMnSc2	stavitel
paláce	palác	k1gInSc2	palác
Lucerna	lucerna	k1gFnSc1	lucerna
Vácslava	Vácslav	k1gMnSc4	Vácslav
Havla	Havel	k1gMnSc4	Havel
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
strýcem	strýc	k1gMnSc7	strýc
byl	být	k5eAaImAgMnS	být
zakladatel	zakladatel	k1gMnSc1	zakladatel
AB	AB	kA	AB
Barrandov	Barrandov	k1gInSc1	Barrandov
Miloš	Miloš	k1gMnSc1	Miloš
Havel	Havel	k1gMnSc1	Havel
<g/>
,	,	kIx,	,
mladším	mladý	k2eAgMnSc7d2	mladší
bratrem	bratr	k1gMnSc7	bratr
vědec	vědec	k1gMnSc1	vědec
Ivan	Ivan	k1gMnSc1	Ivan
M.	M.	kA	M.
Havel	Havel	k1gMnSc1	Havel
<g/>
.	.	kIx.	.
</s>
<s>
Válku	válek	k1gInSc3	válek
rodina	rodina	k1gFnSc1	rodina
přečkala	přečkat	k5eAaPmAgFnS	přečkat
v	v	k7c6	v
ústraní	ústraní	k1gNnSc6	ústraní
na	na	k7c6	na
Havlově	Havlův	k2eAgInSc6d1	Havlův
na	na	k7c6	na
Českomoravské	českomoravský	k2eAgFnSc6d1	Českomoravská
vrchovině	vrchovina	k1gFnSc6	vrchovina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
osvobození	osvobození	k1gNnSc6	osvobození
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
elitní	elitní	k2eAgFnSc4d1	elitní
internátní	internátní	k2eAgFnSc4d1	internátní
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
Poděbradech	Poděbrady	k1gInPc6	Poděbrady
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Milošem	Miloš	k1gMnSc7	Miloš
Formanem	Forman	k1gMnSc7	Forman
či	či	k8xC	či
bratry	bratr	k1gMnPc7	bratr
Mašínovými	Mašínová	k1gFnPc7	Mašínová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
však	však	k9	však
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
zrušena	zrušit	k5eAaPmNgFnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
základní	základní	k2eAgFnSc2d1	základní
školní	školní	k2eAgFnSc2d1	školní
docházky	docházka	k1gFnSc2	docházka
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
komunistickém	komunistický	k2eAgInSc6d1	komunistický
režimu	režim	k1gInSc6	režim
kvůli	kvůli	k7c3	kvůli
svému	svůj	k3xOyFgNnSc3	svůj
"	"	kIx"	"
<g/>
buržoaznímu	buržoazní	k2eAgMnSc3d1	buržoazní
<g/>
"	"	kIx"	"
původu	původ	k1gInSc3	původ
potíže	potíž	k1gFnSc2	potíž
získat	získat	k5eAaPmF	získat
umístění	umístění	k1gNnSc4	umístění
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
podle	podle	k7c2	podle
vlastní	vlastní	k2eAgFnSc2d1	vlastní
volby	volba	k1gFnSc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
do	do	k7c2	do
učebního	učební	k2eAgInSc2d1	učební
oboru	obor	k1gInSc2	obor
jako	jako	k8xS	jako
chemický	chemický	k2eAgMnSc1d1	chemický
laborant	laborant	k1gMnSc1	laborant
a	a	k8xC	a
pražskou	pražský	k2eAgFnSc4d1	Pražská
Střední	střední	k2eAgFnSc4d1	střední
všeobecně	všeobecně	k6eAd1	všeobecně
vzdělávací	vzdělávací	k2eAgFnSc4d1	vzdělávací
školu	škola	k1gFnSc4	škola
pro	pro	k7c4	pro
pracující	pracující	k1gMnPc4	pracující
ve	v	k7c6	v
Štěpánské	štěpánský	k2eAgFnSc6d1	Štěpánská
ulici	ulice	k1gFnSc6	ulice
22	[number]	k4	22
studoval	studovat	k5eAaImAgMnS	studovat
večerně	večerně	k6eAd1	večerně
<g/>
.	.	kIx.	.
</s>
<s>
Maturitu	maturita	k1gFnSc4	maturita
složil	složit	k5eAaPmAgMnS	složit
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
levák	levák	k1gMnSc1	levák
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
literát	literát	k1gMnSc1	literát
Havel	Havel	k1gMnSc1	Havel
debutoval	debutovat	k5eAaBmAgMnS	debutovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Květen	květen	k1gInSc1	květen
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
publikování	publikování	k1gNnSc4	publikování
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
<g/>
,	,	kIx,	,
uveřejňoval	uveřejňovat	k5eAaImAgInS	uveřejňovat
své	svůj	k3xOyFgInPc4	svůj
texty	text	k1gInPc4	text
v	v	k7c6	v
periodicích	periodice	k1gFnPc6	periodice
Divadelní	divadelní	k2eAgFnSc2d1	divadelní
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
Host	host	k1gMnSc1	host
do	do	k7c2	do
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
Literární	literární	k2eAgFnPc1d1	literární
noviny	novina	k1gFnPc1	novina
<g/>
,	,	kIx,	,
Sešity	sešit	k1gInPc1	sešit
pro	pro	k7c4	pro
mladou	mladý	k2eAgFnSc4d1	mladá
literaturu	literatura	k1gFnSc4	literatura
(	(	kIx(	(
<g/>
Sešity	sešit	k1gInPc1	sešit
pro	pro	k7c4	pro
literaturu	literatura	k1gFnSc4	literatura
a	a	k8xC	a
diskusi	diskuse	k1gFnSc4	diskuse
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tvář	tvář	k1gFnSc1	tvář
a	a	k8xC	a
Zítřek	zítřek	k1gInSc1	zítřek
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
kádrových	kádrový	k2eAgInPc2d1	kádrový
důvodů	důvod	k1gInPc2	důvod
nebyl	být	k5eNaImAgInS	být
přijat	přijat	k2eAgInSc1d1	přijat
na	na	k7c4	na
žádnou	žádný	k3yNgFnSc4	žádný
vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
humanitního	humanitní	k2eAgNnSc2d1	humanitní
zaměření	zaměření	k1gNnSc2	zaměření
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
v	v	k7c6	v
letech	let	k1gInPc6	let
1955	[number]	k4	1955
<g/>
–	–	k?	–
<g/>
1957	[number]	k4	1957
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
ekonomické	ekonomický	k2eAgFnSc6d1	ekonomická
fakultě	fakulta	k1gFnSc6	fakulta
Českého	český	k2eAgNnSc2d1	české
vysokého	vysoký	k2eAgNnSc2d1	vysoké
učení	učení	k1gNnSc2	učení
technického	technický	k2eAgNnSc2d1	technické
<g/>
.	.	kIx.	.
</s>
<s>
Pokusil	pokusit	k5eAaPmAgMnS	pokusit
se	se	k3xPyFc4	se
přestoupit	přestoupit	k5eAaPmF	přestoupit
na	na	k7c4	na
filmovou	filmový	k2eAgFnSc4d1	filmová
fakultu	fakulta	k1gFnSc4	fakulta
Akademie	akademie	k1gFnSc2	akademie
múzických	múzický	k2eAgNnPc2d1	múzické
umění	umění	k1gNnPc2	umění
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
však	však	k9	však
odmítnut	odmítnout	k5eAaPmNgInS	odmítnout
a	a	k8xC	a
nebyl	být	k5eNaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
zpět	zpět	k6eAd1	zpět
na	na	k7c6	na
České	český	k2eAgFnSc6d1	Česká
vysoké	vysoká	k1gFnSc6	vysoká
učení	učení	k1gNnSc4	učení
technické	technický	k2eAgNnSc4d1	technické
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
vystoupil	vystoupit	k5eAaPmAgInS	vystoupit
s	s	k7c7	s
kritickým	kritický	k2eAgInSc7d1	kritický
projevem	projev	k1gInSc7	projev
na	na	k7c6	na
konferenci	konference	k1gFnSc6	konference
Svazu	svaz	k1gInSc2	svaz
spisovatelů	spisovatel	k1gMnPc2	spisovatel
v	v	k7c6	v
Dobříši	Dobříš	k1gFnSc6	Dobříš
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
základní	základní	k2eAgFnSc2d1	základní
vojenské	vojenský	k2eAgFnSc2d1	vojenská
služby	služba	k1gFnSc2	služba
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
–	–	k?	–
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
jevištní	jevištní	k2eAgMnSc1d1	jevištní
technik	technik	k1gMnSc1	technik
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
ABC	ABC	kA	ABC
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
pozici	pozice	k1gFnSc6	pozice
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
Na	na	k7c6	na
zábradlí	zábradlí	k1gNnSc6	zábradlí
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
napsal	napsat	k5eAaBmAgMnS	napsat
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
divadelní	divadelní	k2eAgFnSc4d1	divadelní
hru	hra	k1gFnSc4	hra
<g/>
,	,	kIx,	,
jednoaktovku	jednoaktovka	k1gFnSc4	jednoaktovka
Rodinný	rodinný	k2eAgInSc4d1	rodinný
večer	večer	k1gInSc4	večer
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
druhou	druhý	k4xOgFnSc4	druhý
hru	hra	k1gFnSc4	hra
<g/>
,	,	kIx,	,
Zahradní	zahradní	k2eAgFnSc4d1	zahradní
slavnost	slavnost	k1gFnSc4	slavnost
<g/>
,	,	kIx,	,
uvedlo	uvést	k5eAaPmAgNnS	uvést
Divadlo	divadlo	k1gNnSc1	divadlo
Na	na	k7c6	na
zábradlí	zábradlí	k1gNnSc6	zábradlí
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
zároveň	zároveň	k6eAd1	zároveň
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k8xC	jako
asistent	asistent	k1gMnSc1	asistent
režie	režie	k1gFnSc1	režie
Alfréda	Alfréd	k1gMnSc2	Alfréd
Radoka	Radoek	k1gMnSc2	Radoek
v	v	k7c6	v
Městských	městský	k2eAgNnPc6d1	Městské
divadlech	divadlo	k1gNnPc6	divadlo
pražských	pražský	k2eAgNnPc6d1	Pražské
<g/>
.	.	kIx.	.
</s>
<s>
Dálkově	dálkově	k6eAd1	dálkově
studoval	studovat	k5eAaImAgMnS	studovat
divadelní	divadelní	k2eAgFnSc4d1	divadelní
fakultu	fakulta	k1gFnSc4	fakulta
Akademie	akademie	k1gFnSc2	akademie
múzických	múzický	k2eAgNnPc2d1	múzické
umění	umění	k1gNnPc2	umění
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
posléze	posléze	k6eAd1	posléze
dramaturgem	dramaturg	k1gMnSc7	dramaturg
Divadla	divadlo	k1gNnSc2	divadlo
Na	na	k7c6	na
zábradlí	zábradlí	k1gNnSc6	zábradlí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
osmileté	osmiletý	k2eAgFnSc6d1	osmiletá
známosti	známost	k1gFnSc6	známost
se	s	k7c7	s
9	[number]	k4	9
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1964	[number]	k4	1964
oženil	oženit	k5eAaPmAgInS	oženit
s	s	k7c7	s
Olgou	Olga	k1gFnSc7	Olga
Šplíchalovou	Šplíchalová	k1gFnSc7	Šplíchalová
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
redakční	redakční	k2eAgFnSc2d1	redakční
rady	rada	k1gFnSc2	rada
literárního	literární	k2eAgInSc2d1	literární
měsíčníku	měsíčník	k1gInSc2	měsíčník
Tvář	tvář	k1gFnSc1	tvář
a	a	k8xC	a
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
Na	na	k7c6	na
zábradlí	zábradlí	k1gNnSc6	zábradlí
uvedl	uvést	k5eAaPmAgInS	uvést
hru	hra	k1gFnSc4	hra
Vyrozumění	vyrozumění	k1gNnSc2	vyrozumění
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
vydal	vydat	k5eAaPmAgMnS	vydat
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
knihu	kniha	k1gFnSc4	kniha
Protokoly	protokol	k1gInPc1	protokol
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
vedle	vedle	k7c2	vedle
her	hra	k1gFnPc2	hra
Zahradní	zahradní	k2eAgFnSc4d1	zahradní
slavnost	slavnost	k1gFnSc4	slavnost
a	a	k8xC	a
Vyrozumění	vyrozumění	k1gNnSc4	vyrozumění
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
sbírku	sbírka	k1gFnSc4	sbírka
typogramů	typogram	k1gInPc2	typogram
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
eseje	esej	k1gInPc4	esej
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Pražského	pražský	k2eAgNnSc2d1	Pražské
jara	jaro	k1gNnSc2	jaro
se	se	k3xPyFc4	se
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
stal	stát	k5eAaPmAgMnS	stát
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
důležitých	důležitý	k2eAgFnPc2d1	důležitá
postav	postava	k1gFnPc2	postava
liberálního	liberální	k2eAgMnSc2d1	liberální
<g/>
,	,	kIx,	,
nekomunistického	komunistický	k2eNgNnSc2d1	nekomunistické
křídla	křídlo	k1gNnSc2	křídlo
podporovatelů	podporovatel	k1gMnPc2	podporovatel
reforem	reforma	k1gFnPc2	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Upozornil	upozornit	k5eAaPmAgMnS	upozornit
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
již	již	k6eAd1	již
v	v	k7c4	v
předvečer	předvečer	k1gInSc4	předvečer
Pražského	pražský	k2eAgNnSc2d1	Pražské
jara	jaro	k1gNnSc2	jaro
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1967	[number]	k4	1967
na	na	k7c4	na
IV	IV	kA	IV
<g/>
.	.	kIx.	.
sjezdu	sjezd	k1gInSc6	sjezd
československých	československý	k2eAgMnPc2d1	československý
spisovatelů	spisovatel	k1gMnPc2	spisovatel
pronesl	pronést	k5eAaPmAgInS	pronést
kritický	kritický	k2eAgInSc1d1	kritický
projev	projev	k1gInSc1	projev
odsuzující	odsuzující	k2eAgFnSc2d1	odsuzující
dobové	dobový	k2eAgFnSc2d1	dobová
cenzurní	cenzurní	k2eAgFnSc2d1	cenzurní
praktiky	praktika	k1gFnSc2	praktika
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterém	který	k3yIgMnSc6	který
byl	být	k5eAaImAgMnS	být
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
Ústředního	ústřední	k2eAgInSc2d1	ústřední
výboru	výbor	k1gInSc2	výbor
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Československa	Československo	k1gNnSc2	Československo
(	(	kIx(	(
<g/>
ÚV	ÚV	kA	ÚV
KSČ	KSČ	kA	KSČ
<g/>
)	)	kIx)	)
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ivanem	Ivan	k1gMnSc7	Ivan
Klímou	Klíma	k1gMnSc7	Klíma
<g/>
,	,	kIx,	,
Pavlem	Pavel	k1gMnSc7	Pavel
Kohoutem	Kohout	k1gMnSc7	Kohout
a	a	k8xC	a
Ludvíkem	Ludvík	k1gMnSc7	Ludvík
Vaculíkem	Vaculík	k1gMnSc7	Vaculík
vyškrtnut	vyškrtnout	k5eAaPmNgMnS	vyškrtnout
z	z	k7c2	z
kandidátky	kandidátka	k1gFnSc2	kandidátka
vedení	vedení	k1gNnSc2	vedení
Svazu	svaz	k1gInSc2	svaz
československých	československý	k2eAgMnPc2d1	československý
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1968	[number]	k4	1968
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgInS	připojit
k	k	k7c3	k
otevřenému	otevřený	k2eAgInSc3d1	otevřený
dopisu	dopis	k1gInSc3	dopis
sto	sto	k4xCgNnSc4	sto
padesáti	padesát	k4xCc2	padesát
spisovatelů	spisovatel	k1gMnPc2	spisovatel
a	a	k8xC	a
kulturních	kulturní	k2eAgMnPc2d1	kulturní
pracovníků	pracovník	k1gMnPc2	pracovník
adresovanému	adresovaný	k2eAgInSc3d1	adresovaný
ÚV	ÚV	kA	ÚV
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
předsedou	předseda	k1gMnSc7	předseda
Kruhu	kruh	k1gInSc2	kruh
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
spisovatelů	spisovatel	k1gMnPc2	spisovatel
a	a	k8xC	a
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Literární	literární	k2eAgFnSc2d1	literární
listy	lista	k1gFnSc2	lista
uveřejnil	uveřejnit	k5eAaPmAgInS	uveřejnit
text	text	k1gInSc1	text
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
požadoval	požadovat	k5eAaImAgMnS	požadovat
ukončení	ukončení	k1gNnSc4	ukončení
mocenského	mocenský	k2eAgInSc2d1	mocenský
monopolu	monopol	k1gInSc2	monopol
KSČ	KSČ	kA	KSČ
a	a	k8xC	a
zavedení	zavedení	k1gNnSc1	zavedení
systému	systém	k1gInSc2	systém
více	hodně	k6eAd2	hodně
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
měsíci	měsíc	k1gInSc6	měsíc
se	se	k3xPyFc4	se
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
Na	na	k7c6	na
zábradlí	zábradlí	k1gNnSc6	zábradlí
konala	konat	k5eAaImAgFnS	konat
premiéra	premiéra	k1gFnSc1	premiéra
jeho	jeho	k3xOp3gFnSc2	jeho
hry	hra	k1gFnSc2	hra
Ztížená	ztížený	k2eAgFnSc1d1	ztížená
možnost	možnost	k1gFnSc1	možnost
soustředění	soustředění	k1gNnSc2	soustředění
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1968	[number]	k4	1968
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
ústředního	ústřední	k2eAgInSc2d1	ústřední
výboru	výbor	k1gInSc2	výbor
Svazu	svaz	k1gInSc2	svaz
československých	československý	k2eAgMnPc2d1	československý
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zůstal	zůstat	k5eAaPmAgInS	zůstat
až	až	k9	až
do	do	k7c2	do
rozpuštění	rozpuštění	k1gNnSc2	rozpuštění
svazu	svaz	k1gInSc2	svaz
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
Svaz	svaz	k1gInSc1	svaz
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
čele	čelo	k1gNnSc6	čelo
stál	stát	k5eAaImAgMnS	stát
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Seifert	Seifert	k1gMnSc1	Seifert
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1969	[number]	k4	1969
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
sjezdu	sjezd	k1gInSc6	sjezd
protestoval	protestovat	k5eAaBmAgMnS	protestovat
proti	proti	k7c3	proti
okupační	okupační	k2eAgFnSc3d1	okupační
politice	politika	k1gFnSc3	politika
a	a	k8xC	a
cenzuře	cenzura	k1gFnSc3	cenzura
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
však	však	k9	však
brzy	brzy	k6eAd1	brzy
nato	nato	k6eAd1	nato
komunistickou	komunistický	k2eAgFnSc7d1	komunistická
mocí	moc	k1gFnSc7	moc
potlačen	potlačen	k2eAgMnSc1d1	potlačen
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
čelní	čelní	k2eAgMnPc1d1	čelní
představitelé	představitel	k1gMnPc1	představitel
ztratili	ztratit	k5eAaPmAgMnP	ztratit
možnost	možnost	k1gFnSc4	možnost
publikovat	publikovat	k5eAaBmF	publikovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1969	[number]	k4	1969
se	se	k3xPyFc4	se
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Tvář	tvář	k1gFnSc4	tvář
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
polemiky	polemika	k1gFnSc2	polemika
s	s	k7c7	s
článkem	článek	k1gInSc7	článek
Milana	Milan	k1gMnSc2	Milan
Kundery	Kundera	k1gFnSc2	Kundera
"	"	kIx"	"
<g/>
Český	český	k2eAgInSc1d1	český
úděl	úděl	k1gInSc1	úděl
<g/>
"	"	kIx"	"
z	z	k7c2	z
prosince	prosinec	k1gInSc2	prosinec
předchozího	předchozí	k2eAgInSc2d1	předchozí
roku	rok	k1gInSc2	rok
a	a	k8xC	a
v	v	k7c6	v
opozici	opozice	k1gFnSc6	opozice
proti	proti	k7c3	proti
Kunderově	Kunderův	k2eAgFnSc3d1	Kunderova
odkazu	odkaz	k1gInSc3	odkaz
na	na	k7c4	na
minulé	minulý	k2eAgNnSc4d1	Minulé
národní	národní	k2eAgNnSc4d1	národní
hrdinství	hrdinství	k1gNnSc4	hrdinství
nastolil	nastolit	k5eAaPmAgInS	nastolit
program	program	k1gInSc1	program
aktivního	aktivní	k2eAgInSc2d1	aktivní
<g/>
,	,	kIx,	,
do	do	k7c2	do
budoucnosti	budoucnost	k1gFnSc2	budoucnost
zaměřeného	zaměřený	k2eAgInSc2d1	zaměřený
boje	boj	k1gInSc2	boj
za	za	k7c4	za
trvalé	trvalá	k1gFnPc4	trvalá
humánní	humánní	k2eAgFnPc4d1	humánní
a	a	k8xC	a
kulturní	kulturní	k2eAgFnPc4d1	kulturní
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
potlačení	potlačení	k1gNnSc6	potlačení
Pražského	pražský	k2eAgNnSc2d1	Pražské
jara	jaro	k1gNnSc2	jaro
musel	muset	k5eAaImAgMnS	muset
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
opustit	opustit	k5eAaPmF	opustit
divadlo	divadlo	k1gNnSc4	divadlo
a	a	k8xC	a
jeho	jeho	k3xOp3gNnPc1	jeho
díla	dílo	k1gNnPc1	dílo
se	se	k3xPyFc4	se
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
přestala	přestat	k5eAaPmAgFnS	přestat
vydávat	vydávat	k5eAaPmF	vydávat
a	a	k8xC	a
hrát	hrát	k5eAaImF	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
však	však	k9	však
již	již	k9	již
autorem	autor	k1gMnSc7	autor
uznávaným	uznávaný	k2eAgMnSc7d1	uznávaný
i	i	k9	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
–	–	k?	–
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
obdržel	obdržet	k5eAaPmAgInS	obdržet
Velkou	velký	k2eAgFnSc4d1	velká
rakouskou	rakouský	k2eAgFnSc4d1	rakouská
státní	státní	k2eAgFnSc4d1	státní
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
evropskou	evropský	k2eAgFnSc4d1	Evropská
literaturu	literatura	k1gFnSc4	literatura
a	a	k8xC	a
dvakrát	dvakrát	k6eAd1	dvakrát
v	v	k7c6	v
New	New	k1gMnSc6	New
Yorku	York	k1gInSc2	York
získal	získat	k5eAaPmAgInS	získat
cenu	cena	k1gFnSc4	cena
Obie	Obi	k1gInSc2	Obi
za	za	k7c2	za
hry	hra	k1gFnSc2	hra
Vyrozumění	vyrozumění	k1gNnSc2	vyrozumění
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ztížená	ztížený	k2eAgFnSc1d1	ztížená
možnost	možnost	k1gFnSc1	možnost
soustředění	soustředění	k1gNnSc2	soustředění
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
pro	pro	k7c4	pro
něho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
dvaceti	dvacet	k4xCc6	dvacet
letech	léto	k1gNnPc6	léto
normalizace	normalizace	k1gFnSc2	normalizace
znamenalo	znamenat	k5eAaImAgNnS	znamenat
jistou	jistý	k2eAgFnSc4d1	jistá
finanční	finanční	k2eAgFnSc4d1	finanční
nezávislost	nezávislost	k1gFnSc4	nezávislost
a	a	k8xC	a
podporu	podpora	k1gFnSc4	podpora
světového	světový	k2eAgNnSc2d1	světové
veřejného	veřejný	k2eAgNnSc2d1	veřejné
mínění	mínění	k1gNnSc2	mínění
<g/>
,	,	kIx,	,
kdykoli	kdykoli	k6eAd1	kdykoli
byl	být	k5eAaImAgInS	být
komunistickým	komunistický	k2eAgInSc7d1	komunistický
režimem	režim	k1gInSc7	režim
za	za	k7c4	za
své	svůj	k3xOyFgInPc4	svůj
názory	názor	k1gInPc4	názor
vězněn	vězněn	k2eAgInSc1d1	vězněn
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
výročí	výročí	k1gNnSc6	výročí
obsazení	obsazení	k1gNnSc1	obsazení
Československa	Československo	k1gNnSc2	Československo
vojsky	vojsky	k6eAd1	vojsky
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
vyšla	vyjít	k5eAaPmAgFnS	vyjít
petice	petice	k1gFnSc1	petice
Deset	deset	k4xCc4	deset
bodů	bod	k1gInPc2	bod
ostře	ostro	k6eAd1	ostro
odmítající	odmítající	k2eAgFnSc4d1	odmítající
okupaci	okupace	k1gFnSc4	okupace
a	a	k8xC	a
její	její	k3xOp3gInPc4	její
politické	politický	k2eAgInPc4d1	politický
a	a	k8xC	a
kulturní	kulturní	k2eAgInPc4d1	kulturní
důsledky	důsledek	k1gInPc4	důsledek
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
prominentními	prominentní	k2eAgMnPc7d1	prominentní
signatáři	signatář	k1gMnPc7	signatář
byl	být	k5eAaImAgMnS	být
policií	policie	k1gFnSc7	policie
vyšetřován	vyšetřován	k2eAgMnSc1d1	vyšetřován
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
obviněn	obviněn	k2eAgInSc1d1	obviněn
z	z	k7c2	z
"	"	kIx"	"
<g/>
podvracení	podvracení	k1gNnSc2	podvracení
republiky	republika	k1gFnSc2	republika
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
však	však	k9	však
byl	být	k5eAaImAgInS	být
uveřejněn	uveřejnit	k5eAaPmNgInS	uveřejnit
Havlem	Havel	k1gMnSc7	Havel
uspořádaný	uspořádaný	k2eAgInSc1d1	uspořádaný
sborník	sborník	k1gInSc1	sborník
prací	práce	k1gFnSc7	práce
osmnácti	osmnáct	k4xCc2	osmnáct
autorů	autor	k1gMnPc2	autor
Podoby	podoba	k1gFnSc2	podoba
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
podepsal	podepsat	k5eAaPmAgMnS	podepsat
další	další	k2eAgFnSc4d1	další
spisovatelskou	spisovatelský	k2eAgFnSc4d1	spisovatelská
petici	petice	k1gFnSc4	petice
<g/>
,	,	kIx,	,
požadující	požadující	k2eAgNnSc4d1	požadující
propuštění	propuštění	k1gNnSc4	propuštění
politických	politický	k2eAgMnPc2d1	politický
vězňů	vězeň	k1gMnPc2	vězeň
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
devět	devět	k4xCc4	devět
měsíců	měsíc	k1gInPc2	měsíc
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
trutnovském	trutnovský	k2eAgInSc6d1	trutnovský
pivovaru	pivovar	k1gInSc6	pivovar
jako	jako	k9	jako
dělník	dělník	k1gMnSc1	dělník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
založil	založit	k5eAaPmAgMnS	založit
samizdatovou	samizdatový	k2eAgFnSc4d1	samizdatová
strojopisnou	strojopisný	k2eAgFnSc4d1	strojopisná
ediční	ediční	k2eAgFnSc4d1	ediční
řadu	řada	k1gFnSc4	řada
pro	pro	k7c4	pro
nezávislou	závislý	k2eNgFnSc4d1	nezávislá
literaturu	literatura	k1gFnSc4	literatura
Expedice	expedice	k1gFnSc2	expedice
a	a	k8xC	a
uveřejnil	uveřejnit	k5eAaPmAgInS	uveřejnit
otevřený	otevřený	k2eAgInSc1d1	otevřený
dopis	dopis	k1gInSc1	dopis
prezidentu	prezident	k1gMnSc3	prezident
Gustávu	Gustáv	k1gMnSc3	Gustáv
Husákovi	Husák	k1gMnSc3	Husák
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
kritizoval	kritizovat	k5eAaImAgInS	kritizovat
stav	stav	k1gInSc1	stav
"	"	kIx"	"
<g/>
normalizované	normalizovaný	k2eAgFnSc2d1	normalizovaná
<g/>
"	"	kIx"	"
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
plné	plný	k2eAgFnSc2d1	plná
strachu	strach	k1gInSc2	strach
<g/>
,	,	kIx,	,
zbavené	zbavený	k2eAgFnSc2d1	zbavená
svobody	svoboda	k1gFnSc2	svoboda
a	a	k8xC	a
sebereflexe	sebereflexe	k1gFnSc2	sebereflexe
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1975	[number]	k4	1975
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
premiéra	premiéra	k1gFnSc1	premiéra
další	další	k2eAgFnSc2d1	další
Havlovy	Havlův	k2eAgFnSc2d1	Havlova
hry	hra	k1gFnSc2	hra
Žebrácká	žebrácký	k2eAgFnSc1d1	Žebrácká
opera	opera	k1gFnSc1	opera
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
bez	bez	k7c2	bez
uvedení	uvedení	k1gNnSc2	uvedení
jména	jméno	k1gNnSc2	jméno
autora	autor	k1gMnSc2	autor
nastudovalo	nastudovat	k5eAaBmAgNnS	nastudovat
amatérské	amatérský	k2eAgNnSc4d1	amatérské
divadlo	divadlo	k1gNnSc4	divadlo
Na	na	k7c6	na
tahu	tah	k1gInSc6	tah
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
Andreje	Andrej	k1gMnSc2	Andrej
Kroba	Krob	k1gMnSc2	Krob
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
začal	začít	k5eAaPmAgInS	začít
komunistický	komunistický	k2eAgInSc1d1	komunistický
režim	režim	k1gInSc4	režim
pronásledovat	pronásledovat	k5eAaImF	pronásledovat
hudebníky	hudebník	k1gMnPc4	hudebník
z	z	k7c2	z
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
rockové	rockový	k2eAgFnSc2d1	rocková
scény	scéna	k1gFnSc2	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgMnPc1	čtyři
nekonformní	konformní	k2eNgMnPc1d1	nekonformní
umělci	umělec	k1gMnPc1	umělec
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gMnSc1	The
Plastic	Plastice	k1gFnPc2	Plastice
People	People	k1gMnSc1	People
of	of	k?	of
the	the	k?	the
Universe	Universe	k1gFnSc2	Universe
byli	být	k5eAaImAgMnP	být
uvězněni	uvězněn	k2eAgMnPc1d1	uvězněn
a	a	k8xC	a
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jiřím	Jiří	k1gMnSc7	Jiří
Němcem	Němec	k1gMnSc7	Němec
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
postavil	postavit	k5eAaPmAgMnS	postavit
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
obranu	obrana	k1gFnSc4	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Vlna	vlna	k1gFnSc1	vlna
solidarity	solidarita	k1gFnSc2	solidarita
světové	světový	k2eAgFnSc2d1	světová
veřejnosti	veřejnost	k1gFnSc2	veřejnost
i	i	k8xC	i
domácích	domácí	k2eAgMnPc2d1	domácí
disidentů	disident	k1gMnPc2	disident
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
pomohli	pomoct	k5eAaPmAgMnP	pomoct
vyvolat	vyvolat	k5eAaPmF	vyvolat
<g/>
,	,	kIx,	,
vedla	vést	k5eAaImAgFnS	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
občanské	občanský	k2eAgFnSc2d1	občanská
iniciativy	iniciativa	k1gFnSc2	iniciativa
Charta	charta	k1gFnSc1	charta
77	[number]	k4	77
<g/>
,	,	kIx,	,
zaměřené	zaměřený	k2eAgNnSc1d1	zaměřené
na	na	k7c4	na
dodržování	dodržování	k1gNnSc4	dodržování
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc1	jenž
své	svůj	k3xOyFgFnSc3	svůj
první	první	k4xOgFnSc2	první
prohlášení	prohlášení	k1gNnPc2	prohlášení
datovala	datovat	k5eAaImAgFnS	datovat
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
se	s	k7c7	s
–	–	k?	–
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Patočkou	Patočka	k1gMnSc7	Patočka
a	a	k8xC	a
Jiřím	Jiří	k1gMnSc7	Jiří
Hájkem	Hájek	k1gMnSc7	Hájek
–	–	k?	–
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
mluvčích	mluvčí	k1gMnPc2	mluvčí
Charty	charta	k1gFnSc2	charta
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
toho	ten	k3xDgNnSc2	ten
strávil	strávit	k5eAaPmAgInS	strávit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
až	až	k8xS	až
květnu	květen	k1gInSc6	květen
1977	[number]	k4	1977
pět	pět	k4xCc4	pět
měsíců	měsíc	k1gInPc2	měsíc
ve	v	k7c6	v
vyšetřovací	vyšetřovací	k2eAgFnSc6d1	vyšetřovací
vazbě	vazba	k1gFnSc6	vazba
<g/>
,	,	kIx,	,
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgMnS	být
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
za	za	k7c4	za
poškozování	poškozování	k1gNnSc4	poškozování
zájmů	zájem	k1gInPc2	zájem
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
cizině	cizina	k1gFnSc6	cizina
na	na	k7c4	na
14	[number]	k4	14
měsíců	měsíc	k1gInPc2	měsíc
podmíněně	podmíněně	k6eAd1	podmíněně
<g/>
,	,	kIx,	,
další	další	k2eAgFnSc1d1	další
vyšetřovací	vyšetřovací	k2eAgFnSc1d1	vyšetřovací
vazba	vazba	k1gFnSc1	vazba
ho	on	k3xPp3gInSc4	on
čekala	čekat	k5eAaImAgFnS	čekat
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
až	až	k8xS	až
březnu	březen	k1gInSc6	březen
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1978	[number]	k4	1978
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
spoluzakladatelem	spoluzakladatel	k1gMnSc7	spoluzakladatel
a	a	k8xC	a
mluvčím	mluvčí	k1gMnSc7	mluvčí
Výboru	výbor	k1gInSc2	výbor
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
nespravedlivě	spravedlivě	k6eNd1	spravedlivě
stíhaných	stíhaný	k2eAgMnPc2d1	stíhaný
(	(	kIx(	(
<g/>
VONS	VONS	kA	VONS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
monitoroval	monitorovat	k5eAaImAgInS	monitorovat
případy	případ	k1gInPc4	případ
politických	politický	k2eAgMnPc2d1	politický
vězňů	vězeň	k1gMnPc2	vězeň
a	a	k8xC	a
zasazoval	zasazovat	k5eAaImAgMnS	zasazovat
se	se	k3xPyFc4	se
o	o	k7c4	o
jejich	jejich	k3xOp3gNnSc4	jejich
propuštění	propuštění	k1gNnSc4	propuštění
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
uveřejnil	uveřejnit	k5eAaPmAgInS	uveřejnit
esej	esej	k1gInSc1	esej
Moc	moc	k6eAd1	moc
bezmocných	bezmocný	k1gMnPc2	bezmocný
<g/>
,	,	kIx,	,
analýzu	analýza	k1gFnSc4	analýza
fungování	fungování	k1gNnSc2	fungování
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
program	program	k1gInSc4	program
nenásilného	násilný	k2eNgInSc2d1	nenásilný
odporu	odpor	k1gInSc2	odpor
vůči	vůči	k7c3	vůči
němu	on	k3xPp3gInSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
tehdy	tehdy	k6eAd1	tehdy
také	také	k9	také
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
signatářů	signatář	k1gMnPc2	signatář
prohlášení	prohlášení	k1gNnSc2	prohlášení
Sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
českého	český	k2eAgInSc2d1	český
socialismu	socialismus	k1gInSc2	socialismus
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgNnSc2	jenž
československý	československý	k2eAgInSc4d1	československý
režim	režim	k1gInSc4	režim
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
označoval	označovat	k5eAaImAgInS	označovat
jako	jako	k9	jako
socialistický	socialistický	k2eAgInSc1d1	socialistický
<g/>
,	,	kIx,	,
nedostál	dostát	k5eNaPmAgInS	dostát
požadavkům	požadavek	k1gInPc3	požadavek
dělnického	dělnický	k2eAgNnSc2d1	dělnické
hnutí	hnutí	k1gNnSc2	hnutí
formulovaným	formulovaný	k2eAgInPc3d1	formulovaný
před	před	k7c7	před
sto	sto	k4xCgNnSc4	sto
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1979	[number]	k4	1979
byl	být	k5eAaImAgMnS	být
Havel	Havel	k1gMnSc1	Havel
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
členy	člen	k1gInPc7	člen
VONS	VONS	kA	VONS
zatčen	zatčen	k2eAgMnSc1d1	zatčen
a	a	k8xC	a
strávil	strávit	k5eAaPmAgInS	strávit
následujících	následující	k2eAgInPc2d1	následující
pět	pět	k4xCc4	pět
měsíců	měsíc	k1gInPc2	měsíc
ve	v	k7c6	v
vazbě	vazba	k1gFnSc6	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
začal	začít	k5eAaPmAgInS	začít
psát	psát	k5eAaImF	psát
své	svůj	k3xOyFgFnSc3	svůj
ženě	žena	k1gFnSc3	žena
dopisy	dopis	k1gInPc4	dopis
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
základem	základ	k1gInSc7	základ
knihy	kniha	k1gFnSc2	kniha
Dopisy	dopis	k1gInPc1	dopis
Olze	Olga	k1gFnSc3	Olga
<g/>
,	,	kIx,	,
vydané	vydaný	k2eAgFnSc3d1	vydaná
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1979	[number]	k4	1979
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
proces	proces	k1gInSc1	proces
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
disidentů	disident	k1gMnPc2	disident
a	a	k8xC	a
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
byl	být	k5eAaImAgMnS	být
za	za	k7c2	za
podvracení	podvracení	k1gNnSc2	podvracení
republiky	republika	k1gFnSc2	republika
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
na	na	k7c4	na
čtyři	čtyři	k4xCgInPc4	čtyři
a	a	k8xC	a
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
nepodmíněně	podmíněně	k6eNd1	podmíněně
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
biskupa	biskup	k1gMnSc2	biskup
Václava	Václav	k1gMnSc2	Václav
Malého	Malý	k1gMnSc2	Malý
tehdy	tehdy	k6eAd1	tehdy
Havel	Havel	k1gMnSc1	Havel
dostal	dostat	k5eAaPmAgMnS	dostat
nabídku	nabídka	k1gFnSc4	nabídka
vyhnout	vyhnout	k5eAaPmF	vyhnout
se	se	k3xPyFc4	se
vězení	vězení	k1gNnSc2	vězení
a	a	k8xC	a
působit	působit	k5eAaImF	působit
jako	jako	k9	jako
dramaturg	dramaturg	k1gMnSc1	dramaturg
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
ale	ale	k9	ale
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
přijmout	přijmout	k5eAaPmF	přijmout
pouze	pouze	k6eAd1	pouze
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nP	kdyby
byli	být	k5eAaImAgMnP	být
osvobozeni	osvobodit	k5eAaPmNgMnP	osvobodit
i	i	k9	i
všichni	všechen	k3xTgMnPc1	všechen
ostatní	ostatní	k2eAgMnPc1d1	ostatní
členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
podzimu	podzim	k1gInSc2	podzim
1982	[number]	k4	1982
trpěl	trpět	k5eAaImAgMnS	trpět
opakovanými	opakovaný	k2eAgInPc7d1	opakovaný
zápaly	zápal	k1gInPc7	zápal
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1983	[number]	k4	1983
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
výkon	výkon	k1gInSc1	výkon
trestu	trest	k1gInSc2	trest
ze	z	k7c2	z
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
důvodů	důvod	k1gInPc2	důvod
přerušen	přerušen	k2eAgMnSc1d1	přerušen
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
ještě	ještě	k9	ještě
v	v	k7c6	v
domácím	domácí	k2eAgNnSc6d1	domácí
vězení	vězení	k1gNnSc6	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
asi	asi	k9	asi
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
komunistickém	komunistický	k2eAgNnSc6d1	komunistické
vězení	vězení	k1gNnSc6	vězení
trvale	trvale	k6eAd1	trvale
zhoršilo	zhoršit	k5eAaPmAgNnS	zhoršit
jeho	jeho	k3xOp3gNnSc1	jeho
zdraví	zdraví	k1gNnSc1	zdraví
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
však	však	k9	však
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
vlna	vlna	k1gFnSc1	vlna
solidarity	solidarita	k1gFnSc2	solidarita
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
jeho	jeho	k3xOp3gFnSc4	jeho
prestiž	prestiž	k1gFnSc4	prestiž
doma	doma	k6eAd1	doma
i	i	k9	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Havlův	Havlův	k2eAgMnSc1d1	Havlův
blízký	blízký	k2eAgMnSc1d1	blízký
přítel	přítel	k1gMnSc1	přítel
Ivan	Ivan	k1gMnSc1	Ivan
Martin	Martin	k1gMnSc1	Martin
Jirous	Jirous	k1gMnSc1	Jirous
uvítal	uvítat	k5eAaPmAgMnS	uvítat
Havlovo	Havlův	k2eAgNnSc4d1	Havlovo
propuštění	propuštění	k1gNnSc4	propuštění
příležitostnou	příležitostný	k2eAgFnSc7d1	příležitostná
básní	báseň	k1gFnSc7	báseň
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgNnSc4	který
si	se	k3xPyFc3	se
povšiml	povšimnout	k5eAaPmAgMnS	povšimnout
časové	časový	k2eAgFnPc4d1	časová
shody	shoda	k1gFnPc4	shoda
a	a	k8xC	a
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
Havla	Havel	k1gMnSc4	Havel
i	i	k9	i
sebe	sebe	k3xPyFc4	sebe
k	k	k7c3	k
odkazu	odkaz	k1gInSc3	odkaz
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Haška	Hašek	k1gMnSc2	Hašek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
normalizace	normalizace	k1gFnSc2	normalizace
nepřestával	přestávat	k5eNaImAgMnS	přestávat
psát	psát	k5eAaImF	psát
divadelní	divadelní	k2eAgFnSc4d1	divadelní
hry	hra	k1gFnPc4	hra
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
však	však	k9	však
tehdy	tehdy	k6eAd1	tehdy
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
uváděny	uvádět	k5eAaImNgFnP	uvádět
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gFnSc7	jeho
jevištní	jevištní	k2eAgFnSc7d1	jevištní
díla	dílo	k1gNnSc2	dílo
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Horský	horský	k2eAgInSc1d1	horský
hotel	hotel	k1gInSc1	hotel
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Largo	largo	k6eAd1	largo
desolato	desolato	k6eAd1	desolato
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
a	a	k8xC	a
Pokoušení	pokoušení	k1gNnSc1	pokoušení
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nadále	nadále	k6eAd1	nadále
aktivně	aktivně	k6eAd1	aktivně
podporoval	podporovat	k5eAaImAgMnS	podporovat
samizdat	samizdat	k1gInSc4	samizdat
<g/>
;	;	kIx,	;
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgInS	podílet
na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
samizdatových	samizdatový	k2eAgFnPc2d1	samizdatová
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgInS	zůstat
jejich	jejich	k3xOp3gMnSc7	jejich
častým	častý	k2eAgMnSc7d1	častý
přispěvatelem	přispěvatel	k1gMnSc7	přispěvatel
a	a	k8xC	a
členem	člen	k1gMnSc7	člen
redakční	redakční	k2eAgFnSc2d1	redakční
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
se	se	k3xPyFc4	se
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
Českého	český	k2eAgInSc2d1	český
helsinského	helsinský	k2eAgInSc2d1	helsinský
výboru	výbor	k1gInSc2	výbor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
sledoval	sledovat	k5eAaImAgInS	sledovat
dodržování	dodržování	k1gNnSc4	dodržování
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
a	a	k8xC	a
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1988	[number]	k4	1988
vystoupil	vystoupit	k5eAaPmAgInS	vystoupit
na	na	k7c4	na
první	první	k4xOgNnSc4	první
oficiálně	oficiálně	k6eAd1	oficiálně
povolené	povolený	k2eAgFnSc3d1	povolená
manifestaci	manifestace	k1gFnSc3	manifestace
opozičních	opoziční	k2eAgNnPc2d1	opoziční
seskupení	seskupení	k1gNnPc2	seskupení
v	v	k7c6	v
období	období	k1gNnSc6	období
normalizace	normalizace	k1gFnSc2	normalizace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
dne	den	k1gInSc2	den
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
konala	konat	k5eAaImAgFnS	konat
na	na	k7c6	na
pražském	pražský	k2eAgNnSc6d1	Pražské
Škroupově	Škroupův	k2eAgNnSc6d1	Škroupovo
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1989	[number]	k4	1989
byl	být	k5eAaImAgMnS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
za	za	k7c4	za
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
demonstraci	demonstrace	k1gFnSc6	demonstrace
během	během	k7c2	během
Palachova	Palachův	k2eAgInSc2d1	Palachův
týdne	týden	k1gInSc2	týden
a	a	k8xC	a
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
k	k	k7c3	k
devíti	devět	k4xCc3	devět
měsícům	měsíc	k1gInPc3	měsíc
vězení	vězení	k1gNnSc2	vězení
<g/>
,	,	kIx,	,
po	po	k7c6	po
odvolání	odvolání	k1gNnSc6	odvolání
byl	být	k5eAaImAgInS	být
trest	trest	k1gInSc1	trest
snížen	snížen	k2eAgInSc1d1	snížen
na	na	k7c4	na
osm	osm	k4xCc4	osm
měsíců	měsíc	k1gInPc2	měsíc
<g/>
;	;	kIx,	;
podmíněně	podmíněně	k6eAd1	podmíněně
propuštěn	propuštěn	k2eAgMnSc1d1	propuštěn
byl	být	k5eAaImAgMnS	být
již	již	k6eAd1	již
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1989	[number]	k4	1989
stál	stát	k5eAaImAgMnS	stát
u	u	k7c2	u
zrodu	zrod	k1gInSc2	zrod
petice	petice	k1gFnSc2	petice
Několik	několik	k4yIc4	několik
vět	věta	k1gFnPc2	věta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
mluvčím	mluvčí	k1gMnSc7	mluvčí
Charty	charta	k1gFnSc2	charta
77	[number]	k4	77
za	za	k7c4	za
uvězněného	uvězněný	k2eAgMnSc4d1	uvězněný
Alexandra	Alexandr	k1gMnSc4	Alexandr
Vondru	Vondra	k1gMnSc4	Vondra
<g/>
.	.	kIx.	.
</s>
<s>
Naposledy	naposledy	k6eAd1	naposledy
zatčen	zatčen	k2eAgMnSc1d1	zatčen
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
nato	nato	k6eAd1	nato
však	však	k9	však
byl	být	k5eAaImAgInS	být
propuštěn	propustit	k5eAaPmNgInS	propustit
na	na	k7c4	na
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1989	[number]	k4	1989
byla	být	k5eAaImAgFnS	být
Václavu	Václav	k1gMnSc3	Václav
Havlovi	Havel	k1gMnSc3	Havel
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
nepřítomnosti	nepřítomnost	k1gFnSc6	nepřítomnost
udělena	udělen	k2eAgFnSc1d1	udělena
významná	významný	k2eAgFnSc1d1	významná
Mírová	mírový	k2eAgFnSc1d1	mírová
cena	cena	k1gFnSc1	cena
německých	německý	k2eAgMnPc2d1	německý
knihkupců	knihkupec	k1gMnPc2	knihkupec
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostního	slavnostní	k2eAgInSc2d1	slavnostní
aktu	akt	k1gInSc2	akt
v	v	k7c6	v
bývalém	bývalý	k2eAgInSc6d1	bývalý
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Pavla	Pavel	k1gMnSc2	Pavel
(	(	kIx(	(
<g/>
Paulskirche	Paulskirch	k1gMnSc2	Paulskirch
<g/>
)	)	kIx)	)
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
nejvyšších	vysoký	k2eAgMnPc2d3	nejvyšší
představitelů	představitel	k1gMnPc2	představitel
Spolkové	spolkový	k2eAgFnSc2d1	spolková
republiky	republika	k1gFnSc2	republika
Německo	Německo	k1gNnSc4	Německo
se	se	k3xPyFc4	se
Havel	Havel	k1gMnSc1	Havel
nemohl	moct	k5eNaImAgMnS	moct
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
děkovnou	děkovný	k2eAgFnSc4d1	děkovná
řeč	řeč	k1gFnSc4	řeč
za	za	k7c4	za
něj	on	k3xPp3gNnSc4	on
přednesl	přednést	k5eAaPmAgMnS	přednést
filmový	filmový	k2eAgMnSc1d1	filmový
herec	herec	k1gMnSc1	herec
Maximilian	Maximilian	k1gMnSc1	Maximilian
Schell	Schell	k1gMnSc1	Schell
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1989	[number]	k4	1989
se	se	k3xPyFc4	se
na	na	k7c6	na
oslavách	oslava	k1gFnPc6	oslava
30	[number]	k4	30
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc1	výročí
divadla	divadlo	k1gNnSc2	divadlo
Semafor	Semafor	k1gInSc4	Semafor
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Dagmar	Dagmar	k1gFnSc7	Dagmar
Veškrnovou	Veškrnová	k1gFnSc7	Veškrnová
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc4	svůj
pozdější	pozdní	k2eAgMnSc1d2	pozdější
druhou	druhý	k4xOgFnSc7	druhý
manželkou	manželka	k1gFnSc7	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Seznámil	seznámit	k5eAaPmAgMnS	seznámit
je	on	k3xPp3gNnSc4	on
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Sametová	sametový	k2eAgFnSc1d1	sametová
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1989	[number]	k4	1989
začala	začít	k5eAaPmAgFnS	začít
rozehnáním	rozehnání	k1gNnSc7	rozehnání
studentské	studentská	k1gFnSc2	studentská
demonstrace	demonstrace	k1gFnSc1	demonstrace
tzv.	tzv.	kA	tzv.
sametová	sametový	k2eAgFnSc1d1	sametová
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vedla	vést	k5eAaImAgFnS	vést
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
čtyřicetileté	čtyřicetiletý	k2eAgFnSc2d1	čtyřicetiletá
vlády	vláda	k1gFnSc2	vláda
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
se	s	k7c7	s
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
založení	založení	k1gNnSc4	založení
Občanského	občanský	k2eAgNnSc2d1	občanské
fóra	fórum	k1gNnSc2	fórum
(	(	kIx(	(
<g/>
OF	OF	kA	OF
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protitotalitního	protitotalitní	k2eAgNnSc2d1	protitotalitní
hnutí	hnutí	k1gNnSc2	hnutí
sdružujícího	sdružující	k2eAgInSc2d1	sdružující
reformní	reformní	k2eAgInPc4d1	reformní
a	a	k8xC	a
demokratické	demokratický	k2eAgFnPc4d1	demokratická
síly	síla	k1gFnPc4	síla
české	český	k2eAgFnSc2d1	Česká
části	část	k1gFnSc2	část
federace	federace	k1gFnSc2	federace
<g/>
,	,	kIx,	,
a	a	k8xC	a
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
patřil	patřit	k5eAaImAgInS	patřit
k	k	k7c3	k
jeho	jeho	k3xOp3gMnPc3	jeho
nejvlivnějším	vlivný	k2eAgMnPc3d3	nejvlivnější
představitelům	představitel	k1gMnPc3	představitel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úterý	úterý	k1gNnSc6	úterý
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
pak	pak	k6eAd1	pak
z	z	k7c2	z
balkónu	balkón	k1gInSc2	balkón
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
Melantrich	Melantricha	k1gFnPc2	Melantricha
poprvé	poprvé	k6eAd1	poprvé
promluvil	promluvit	k5eAaPmAgMnS	promluvit
jménem	jméno	k1gNnSc7	jméno
OF	OF	kA	OF
k	k	k7c3	k
demonstrantům	demonstrant	k1gMnPc3	demonstrant
shromážděným	shromážděný	k2eAgMnPc3d1	shromážděný
na	na	k7c6	na
zcela	zcela	k6eAd1	zcela
zaplněném	zaplněný	k2eAgNnSc6d1	zaplněné
Václavském	václavský	k2eAgNnSc6d1	Václavské
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
necelý	celý	k2eNgInSc4d1	necelý
týden	týden	k1gInSc4	týden
později	pozdě	k6eAd2	pozdě
již	již	k6eAd1	již
komunistický	komunistický	k2eAgInSc4d1	komunistický
režim	režim	k1gInSc4	režim
pod	pod	k7c7	pod
stálým	stálý	k2eAgInSc7d1	stálý
tlakem	tlak	k1gInSc7	tlak
masových	masový	k2eAgFnPc2d1	masová
demonstrací	demonstrace	k1gFnPc2	demonstrace
zeslábl	zeslábnout	k5eAaPmAgMnS	zeslábnout
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
demonstrace	demonstrace	k1gFnPc1	demonstrace
a	a	k8xC	a
Havlovy	Havlův	k2eAgInPc1d1	Havlův
projevy	projev	k1gInPc1	projev
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
mohla	moct	k5eAaImAgFnS	moct
přenášet	přenášet	k5eAaImF	přenášet
do	do	k7c2	do
celé	celý	k2eAgFnSc2d1	celá
země	zem	k1gFnSc2	zem
a	a	k8xC	a
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
i	i	k9	i
Československá	československý	k2eAgFnSc1d1	Československá
televize	televize	k1gFnSc1	televize
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
padla	padnout	k5eAaPmAgFnS	padnout
komunistická	komunistický	k2eAgFnSc1d1	komunistická
vláda	vláda	k1gFnSc1	vláda
Ladislava	Ladislav	k1gMnSc2	Ladislav
Adamce	Adamec	k1gMnSc2	Adamec
a	a	k8xC	a
prezident	prezident	k1gMnSc1	prezident
Gustáv	Gustáva	k1gFnPc2	Gustáva
Husák	Husák	k1gMnSc1	Husák
přislíbil	přislíbit	k5eAaPmAgMnS	přislíbit
svou	svůj	k3xOyFgFnSc4	svůj
abdikaci	abdikace	k1gFnSc4	abdikace
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
nutno	nutno	k6eAd1	nutno
hledat	hledat	k5eAaImF	hledat
nového	nový	k2eAgMnSc4d1	nový
prezidenta	prezident	k1gMnSc4	prezident
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Havla	Havel	k1gMnSc2	Havel
připadal	připadat	k5eAaImAgMnS	připadat
pro	pro	k7c4	pro
OF	OF	kA	OF
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
ještě	ještě	k9	ještě
Alexander	Alexandra	k1gFnPc2	Alexandra
Dubček	Dubček	k1gMnSc1	Dubček
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
se	se	k3xPyFc4	se
vedení	vedení	k1gNnSc1	vedení
OF	OF	kA	OF
definitivně	definitivně	k6eAd1	definitivně
shodlo	shodnout	k5eAaBmAgNnS	shodnout
na	na	k7c6	na
Havlově	Havlův	k2eAgFnSc6d1	Havlova
kandidatuře	kandidatura	k1gFnSc6	kandidatura
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Havla	Havel	k1gMnSc4	Havel
jako	jako	k8xC	jako
kandidáta	kandidát	k1gMnSc4	kandidát
OF	OF	kA	OF
se	se	k3xPyFc4	se
vyslovilo	vyslovit	k5eAaPmAgNnS	vyslovit
37	[number]	k4	37
členů	člen	k1gMnPc2	člen
rozšířeného	rozšířený	k2eAgInSc2d1	rozšířený
krizového	krizový	k2eAgInSc2d1	krizový
štábu	štáb	k1gInSc2	štáb
<g/>
,	,	kIx,	,
šest	šest	k4xCc1	šest
se	se	k3xPyFc4	se
zdrželo	zdržet	k5eAaPmAgNnS	zdržet
hlasování	hlasování	k1gNnSc1	hlasování
<g/>
.	.	kIx.	.
</s>
<s>
Dubčekovi	Dubčeek	k1gMnSc3	Dubčeek
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
podpory	podpora	k1gFnSc2	podpora
jako	jako	k8xC	jako
budoucímu	budoucí	k2eAgMnSc3d1	budoucí
předsedovi	předseda	k1gMnSc3	předseda
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
;	;	kIx,	;
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
následnému	následný	k2eAgNnSc3d1	následné
zvolení	zvolení	k1gNnSc3	zvolení
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
funkce	funkce	k1gFnSc2	funkce
bylo	být	k5eAaImAgNnS	být
ovšem	ovšem	k9	ovšem
z	z	k7c2	z
formálních	formální	k2eAgInPc2d1	formální
důvodů	důvod	k1gInPc2	důvod
nutné	nutný	k2eAgNnSc1d1	nutné
prosadit	prosadit	k5eAaPmF	prosadit
jeho	jeho	k3xOp3gFnSc4	jeho
kooptaci	kooptace	k1gFnSc4	kooptace
jako	jako	k8xC	jako
poslance	poslanec	k1gMnPc4	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Kandidatura	kandidatura	k1gFnSc1	kandidatura
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
republiky	republika	k1gFnSc2	republika
byla	být	k5eAaImAgFnS	být
oznámena	oznámit	k5eAaPmNgFnS	oznámit
veřejnosti	veřejnost	k1gFnSc2	veřejnost
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
OF	OF	kA	OF
zahájilo	zahájit	k5eAaPmAgNnS	zahájit
mohutnou	mohutný	k2eAgFnSc4d1	mohutná
celostátní	celostátní	k2eAgFnSc4d1	celostátní
volební	volební	k2eAgFnSc4d1	volební
kampaň	kampaň	k1gFnSc4	kampaň
pod	pod	k7c7	pod
heslem	heslo	k1gNnSc7	heslo
"	"	kIx"	"
<g/>
Havel	Havel	k1gMnSc1	Havel
na	na	k7c4	na
Hrad	hrad	k1gInSc4	hrad
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kombinací	kombinace	k1gFnSc7	kombinace
všelidového	všelidový	k2eAgInSc2d1	všelidový
nátlaku	nátlak	k1gInSc2	nátlak
a	a	k8xC	a
vyjednávání	vyjednávání	k1gNnSc2	vyjednávání
v	v	k7c6	v
poslaneckých	poslanecký	k2eAgInPc6d1	poslanecký
kruzích	kruh	k1gInPc6	kruh
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
osvědčil	osvědčit	k5eAaPmAgMnS	osvědčit
zejména	zejména	k9	zejména
Marián	Marián	k1gMnSc1	Marián
Čalfa	Čalf	k1gMnSc2	Čalf
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
i	i	k9	i
komunistické	komunistický	k2eAgMnPc4d1	komunistický
poslance	poslanec	k1gMnPc4	poslanec
k	k	k7c3	k
volbě	volba	k1gFnSc3	volba
jejich	jejich	k3xOp3gMnSc2	jejich
nedávného	dávný	k2eNgMnSc2d1	nedávný
úhlavního	úhlavní	k2eAgMnSc2d1	úhlavní
nepřítele	nepřítel	k1gMnSc2	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
byl	být	k5eAaImAgMnS	být
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
29	[number]	k4	29
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1989	[number]	k4	1989
ve	v	k7c6	v
Vladislavském	vladislavský	k2eAgInSc6d1	vladislavský
sále	sál	k1gInSc6	sál
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
jednomyslně	jednomyslně	k6eAd1	jednomyslně
zvolen	zvolit	k5eAaPmNgMnS	zvolit
prezidentem	prezident	k1gMnSc7	prezident
Československé	československý	k2eAgFnSc2d1	Československá
socialistické	socialistický	k2eAgFnSc2d1	socialistická
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgInS	být
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
oficiální	oficiální	k2eAgInSc4d1	oficiální
název	název	k1gInSc4	název
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
volbu	volba	k1gFnSc4	volba
navázalo	navázat	k5eAaPmAgNnS	navázat
slavnostní	slavnostní	k2eAgFnSc4d1	slavnostní
Te	Te	k1gFnSc4	Te
Deum	Deuma	k1gFnPc2	Deuma
ve	v	k7c6	v
Svatovítské	svatovítský	k2eAgFnSc6d1	Svatovítská
katedrále	katedrála	k1gFnSc6	katedrála
vedené	vedený	k2eAgFnSc2d1	vedená
kardinálem	kardinál	k1gMnSc7	kardinál
Tomáškem	Tomášek	k1gMnSc7	Tomášek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
41	[number]	k4	41
letech	léto	k1gNnPc6	léto
tak	tak	k6eAd1	tak
opět	opět	k6eAd1	opět
na	na	k7c6	na
Hradě	hrad	k1gInSc6	hrad
zasedl	zasednout	k5eAaPmAgMnS	zasednout
nekomunistický	komunistický	k2eNgMnSc1d1	nekomunistický
prezident	prezident	k1gMnSc1	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
symbolickým	symbolický	k2eAgNnSc7d1	symbolické
vítězstvím	vítězství	k1gNnSc7	vítězství
dosavadní	dosavadní	k2eAgFnSc2d1	dosavadní
opozice	opozice	k1gFnSc2	opozice
zároveň	zároveň	k6eAd1	zároveň
skončilo	skončit	k5eAaPmAgNnS	skončit
období	období	k1gNnSc1	období
velkých	velký	k2eAgFnPc2d1	velká
demonstrací	demonstrace	k1gFnPc2	demonstrace
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
odvolány	odvolán	k2eAgFnPc1d1	odvolána
stávky	stávka	k1gFnPc1	stávka
a	a	k8xC	a
stávkové	stávkový	k2eAgFnPc1d1	stávková
pohotovosti	pohotovost	k1gFnPc1	pohotovost
a	a	k8xC	a
společnost	společnost	k1gFnSc1	společnost
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
vracet	vracet	k5eAaImF	vracet
do	do	k7c2	do
normálního	normální	k2eAgInSc2d1	normální
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
první	první	k4xOgNnSc1	první
funkční	funkční	k2eAgNnSc1d1	funkční
období	období	k1gNnSc1	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
posledním	poslední	k2eAgMnSc7d1	poslední
prezidentem	prezident	k1gMnSc7	prezident
komunistického	komunistický	k2eAgNnSc2d1	komunistické
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
trvalo	trvat	k5eAaImAgNnS	trvat
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
do	do	k7c2	do
prvních	první	k4xOgFnPc2	první
svobodných	svobodný	k2eAgFnPc2d1	svobodná
voleb	volba	k1gFnPc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
své	svůj	k3xOyFgFnSc2	svůj
moci	moc	k1gFnSc2	moc
<g/>
;	;	kIx,	;
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
hluboké	hluboký	k2eAgFnSc6d1	hluboká
defenzivě	defenziva	k1gFnSc6	defenziva
a	a	k8xC	a
nová	nový	k2eAgFnSc1d1	nová
demokratická	demokratický	k2eAgFnSc1d1	demokratická
politická	politický	k2eAgFnSc1d1	politická
scéna	scéna	k1gFnSc1	scéna
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
neustálila	ustálit	k5eNaPmAgFnS	ustálit
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
Havlova	Havlův	k2eAgNnPc1d1	Havlovo
rozhodnutí	rozhodnutí	k1gNnPc1	rozhodnutí
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
jsou	být	k5eAaImIp3nP	být
dodnes	dodnes	k6eAd1	dodnes
kontroverzní	kontroverzní	k2eAgInPc1d1	kontroverzní
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
mimořádně	mimořádně	k6eAd1	mimořádně
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
amnestie	amnestie	k1gFnSc1	amnestie
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
byla	být	k5eAaImAgFnS	být
počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
propuštěna	propuštěn	k2eAgFnSc1d1	propuštěna
řada	řada	k1gFnSc1	řada
zločinců	zločinec	k1gMnPc2	zločinec
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
mnozí	mnohý	k2eAgMnPc1d1	mnohý
brzy	brzy	k6eAd1	brzy
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
v	v	k7c6	v
páchání	páchání	k1gNnSc6	páchání
závažné	závažný	k2eAgFnSc2d1	závažná
trestné	trestný	k2eAgFnSc2d1	trestná
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
amnestii	amnestie	k1gFnSc6	amnestie
následovala	následovat	k5eAaImAgFnS	následovat
také	také	k9	také
vězeňská	vězeňský	k2eAgFnSc1d1	vězeňská
vzpoura	vzpoura	k1gFnSc1	vzpoura
v	v	k7c6	v
Leopoldově	Leopoldův	k2eAgMnSc6d1	Leopoldův
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
nikdy	nikdy	k6eAd1	nikdy
neprosazoval	prosazovat	k5eNaImAgMnS	prosazovat
radikální	radikální	k2eAgNnSc4d1	radikální
zúčtování	zúčtování	k1gNnSc4	zúčtování
s	s	k7c7	s
přisluhovači	přisluhovač	k1gMnPc7	přisluhovač
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Splnil	splnit	k5eAaPmAgInS	splnit
však	však	k9	však
svůj	svůj	k3xOyFgInSc4	svůj
hlavní	hlavní	k2eAgInSc4d1	hlavní
úkol	úkol	k1gInSc4	úkol
<g/>
,	,	kIx,	,
přípravu	příprava	k1gFnSc4	příprava
svobodných	svobodný	k2eAgFnPc2d1	svobodná
voleb	volba	k1gFnPc2	volba
a	a	k8xC	a
budování	budování	k1gNnSc6	budování
základů	základ	k1gInPc2	základ
demokratické	demokratický	k2eAgFnSc2d1	demokratická
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Řadou	řada	k1gFnSc7	řada
projevů	projev	k1gInPc2	projev
i	i	k8xC	i
symbolických	symbolický	k2eAgInPc2d1	symbolický
aktů	akt	k1gInPc2	akt
dodával	dodávat	k5eAaImAgMnS	dodávat
občanům	občan	k1gMnPc3	občan
sebevědomí	sebevědomí	k1gNnSc2	sebevědomí
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nezakrýval	zakrývat	k5eNaImAgMnS	zakrývat
obtížnost	obtížnost	k1gFnSc4	obtížnost
situace	situace	k1gFnSc2	situace
<g/>
.	.	kIx.	.
</s>
<s>
Známým	známý	k2eAgMnSc7d1	známý
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
smyslu	smysl	k1gInSc6	smysl
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gInSc4	jeho
první	první	k4xOgInSc4	první
novoroční	novoroční	k2eAgInSc4d1	novoroční
prezidentský	prezidentský	k2eAgInSc4d1	prezidentský
projev	projev	k1gInSc4	projev
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Podruhé	podruhé	k6eAd1	podruhé
byl	být	k5eAaImAgMnS	být
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
zvolen	zvolit	k5eAaPmNgMnS	zvolit
prezidentem	prezident	k1gMnSc7	prezident
5	[number]	k4	5
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1990	[number]	k4	1990
již	již	k6eAd1	již
svobodně	svobodně	k6eAd1	svobodně
zvoleným	zvolený	k2eAgNnSc7d1	zvolené
Federálním	federální	k2eAgNnSc7d1	federální
shromážděním	shromáždění	k1gNnSc7	shromáždění
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
nyní	nyní	k6eAd1	nyní
převažovali	převažovat	k5eAaImAgMnP	převažovat
zástupci	zástupce	k1gMnPc1	zástupce
Občanského	občanský	k2eAgNnSc2d1	občanské
fóra	fórum	k1gNnSc2	fórum
a	a	k8xC	a
Verejnosti	Verejnost	k1gFnSc2	Verejnost
proti	proti	k7c3	proti
násiliu	násilium	k1gNnSc3	násilium
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
politické	politický	k2eAgFnSc6d1	politická
scéně	scéna	k1gFnSc6	scéna
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
nové	nový	k2eAgFnPc1d1	nová
silné	silný	k2eAgFnPc1d1	silná
osobnosti	osobnost	k1gFnPc1	osobnost
konkurující	konkurující	k2eAgFnPc1d1	konkurující
Havlovi	Havel	k1gMnSc3	Havel
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc3	jeho
přívržencům	přívrženec	k1gMnPc3	přívrženec
<g/>
;	;	kIx,	;
především	především	k9	především
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
Vladimír	Vladimír	k1gMnSc1	Vladimír
Mečiar	Mečiar	k1gMnSc1	Mečiar
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
přispěli	přispět	k5eAaPmAgMnP	přispět
k	k	k7c3	k
formování	formování	k1gNnSc3	formování
nového	nový	k2eAgInSc2d1	nový
stranického	stranický	k2eAgInSc2d1	stranický
systému	systém	k1gInSc2	systém
a	a	k8xC	a
dokázali	dokázat	k5eAaPmAgMnP	dokázat
získat	získat	k5eAaPmF	získat
masovou	masový	k2eAgFnSc4d1	masová
podporu	podpora	k1gFnSc4	podpora
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
pomlčková	pomlčkový	k2eAgFnSc1d1	pomlčková
válka	válka	k1gFnSc1	válka
o	o	k7c4	o
pojmenování	pojmenování	k1gNnSc4	pojmenování
společného	společný	k2eAgInSc2d1	společný
státu	stát	k1gInSc2	stát
ukázala	ukázat	k5eAaPmAgFnS	ukázat
rychle	rychle	k6eAd1	rychle
rostoucí	rostoucí	k2eAgNnSc1d1	rostoucí
napětí	napětí	k1gNnSc1	napětí
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
republikami	republika	k1gFnPc7	republika
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
ještě	ještě	k6eAd1	ještě
umocňováno	umocňovat	k5eAaImNgNnS	umocňovat
jak	jak	k6eAd1	jak
odlišnou	odlišný	k2eAgFnSc7d1	odlišná
politickou	politický	k2eAgFnSc7d1	politická
orientací	orientace	k1gFnSc7	orientace
slovenské	slovenský	k2eAgFnSc2d1	slovenská
a	a	k8xC	a
české	český	k2eAgFnSc2d1	Česká
reprezentace	reprezentace	k1gFnSc2	reprezentace
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
nepružným	pružný	k2eNgInSc7d1	nepružný
ústavním	ústavní	k2eAgInSc7d1	ústavní
rámcem	rámec	k1gInSc7	rámec
federace	federace	k1gFnSc2	federace
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
po	po	k7c6	po
odstranění	odstranění	k1gNnSc6	odstranění
komunistického	komunistický	k2eAgInSc2d1	komunistický
diktátu	diktát	k1gInSc2	diktát
vůči	vůči	k7c3	vůči
parlamentům	parlament	k1gInPc3	parlament
již	již	k6eAd1	již
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
správně	správně	k6eAd1	správně
fungovat	fungovat	k5eAaImF	fungovat
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
jednoznačně	jednoznačně	k6eAd1	jednoznačně
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
zachování	zachování	k1gNnSc2	zachování
společného	společný	k2eAgInSc2d1	společný
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Volební	volební	k2eAgNnSc1d1	volební
období	období	k1gNnSc1	období
počínající	počínající	k2eAgNnSc1d1	počínající
rokem	rok	k1gInSc7	rok
1990	[number]	k4	1990
bylo	být	k5eAaImAgNnS	být
koncipováno	koncipován	k2eAgNnSc1d1	koncipováno
jako	jako	k8xS	jako
zkrácené	zkrácený	k2eAgNnSc1d1	zkrácené
<g/>
;	;	kIx,	;
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
1992	[number]	k4	1992
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
Klausova	Klausův	k2eAgFnSc1d1	Klausova
pravicová	pravicový	k2eAgFnSc1d1	pravicová
Občanská	občanský	k2eAgFnSc1d1	občanská
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
(	(	kIx(	(
<g/>
ODS	ODS	kA	ODS
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
Mečiarovo	Mečiarův	k2eAgNnSc1d1	Mečiarovo
nacionalistické	nacionalistický	k2eAgNnSc1d1	nacionalistické
Hnutí	hnutí	k1gNnSc1	hnutí
za	za	k7c4	za
demokratické	demokratický	k2eAgNnSc4d1	demokratické
Slovensko	Slovensko	k1gNnSc4	Slovensko
(	(	kIx(	(
<g/>
HZDS	HZDS	kA	HZDS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Havlovi	Havel	k1gMnSc3	Havel
blízké	blízký	k2eAgFnSc2d1	blízká
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Občanské	občanský	k2eAgNnSc1d1	občanské
hnutí	hnutí	k1gNnSc1	hnutí
<g/>
,	,	kIx,	,
propadly	propadnout	k5eAaPmAgInP	propadnout
<g/>
.	.	kIx.	.
</s>
<s>
Rozpadu	rozpad	k1gInSc3	rozpad
společného	společný	k2eAgInSc2d1	společný
státu	stát	k1gInSc2	stát
nezabránilo	zabránit	k5eNaPmAgNnS	zabránit
ani	ani	k8xC	ani
hlasování	hlasování	k1gNnSc1	hlasování
federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
o	o	k7c6	o
Československu	Československo	k1gNnSc6	Československo
jako	jako	k8xS	jako
Unii	unie	k1gFnSc6	unie
(	(	kIx(	(
<g/>
na	na	k7c4	na
popud	popud	k1gInSc4	popud
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
výsledek	výsledek	k1gInSc1	výsledek
byl	být	k5eAaImAgInS	být
oběma	dva	k4xCgFnPc7	dva
stranami	strana	k1gFnPc7	strana
ignorován	ignorovat	k5eAaImNgInS	ignorovat
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
z	z	k7c2	z
prezidentského	prezidentský	k2eAgInSc2d1	prezidentský
úřadu	úřad	k1gInSc2	úřad
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
před	před	k7c7	před
vypršením	vypršení	k1gNnSc7	vypršení
mandátu	mandát	k1gInSc2	mandát
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1992	[number]	k4	1992
(	(	kIx(	(
<g/>
s	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
k	k	k7c3	k
20	[number]	k4	20
<g/>
.	.	kIx.	.
červenci	červenec	k1gInSc6	červenec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
nebyl	být	k5eNaImAgInS	být
Federálním	federální	k2eAgNnSc7d1	federální
shromážděním	shromáždění	k1gNnSc7	shromáždění
zvolen	zvolit	k5eAaPmNgInS	zvolit
na	na	k7c4	na
další	další	k2eAgNnSc4d1	další
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
konec	konec	k1gInSc1	konec
Československa	Československo	k1gNnSc2	Československo
je	být	k5eAaImIp3nS	být
nevyhnutelný	vyhnutelný	k2eNgInSc1d1	nevyhnutelný
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
na	na	k7c4	na
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
z	z	k7c2	z
veřejného	veřejný	k2eAgInSc2d1	veřejný
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
tento	tento	k3xDgInSc4	tento
zásadní	zásadní	k2eAgInSc4d1	zásadní
vnitropolitický	vnitropolitický	k2eAgInSc4d1	vnitropolitický
neúspěch	neúspěch	k1gInSc4	neúspěch
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
jako	jako	k9	jako
prezidentovi	prezident	k1gMnSc3	prezident
podařilo	podařit	k5eAaPmAgNnS	podařit
Československo	Československo	k1gNnSc4	Československo
úspěšně	úspěšně	k6eAd1	úspěšně
vrátit	vrátit	k5eAaPmF	vrátit
do	do	k7c2	do
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
politiky	politika	k1gFnSc2	politika
a	a	k8xC	a
přispět	přispět	k5eAaPmF	přispět
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
západní	západní	k2eAgFnSc3d1	západní
orientaci	orientace	k1gFnSc3	orientace
<g/>
.	.	kIx.	.
</s>
<s>
Nadstandardní	nadstandardní	k2eAgInPc1d1	nadstandardní
vztahy	vztah	k1gInPc1	vztah
se	s	k7c7	s
Západem	západ	k1gInSc7	západ
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
i	i	k9	i
státní	státní	k2eAgFnPc1d1	státní
návštěvy	návštěva	k1gFnPc1	návštěva
z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
Československo	Československo	k1gNnSc4	Československo
navštívili	navštívit	k5eAaPmAgMnP	navštívit
papež	papež	k1gMnSc1	papež
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
i	i	k8xC	i
prezident	prezident	k1gMnSc1	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgFnPc2d1	americká
George	George	k1gFnPc2	George
Bush	Bush	k1gMnSc1	Bush
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
únorové	únorový	k2eAgFnSc2d1	únorová
návštěvy	návštěva	k1gFnSc2	návštěva
v	v	k7c6	v
USA	USA	kA	USA
Havel	Havel	k1gMnSc1	Havel
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
s	s	k7c7	s
projevem	projev	k1gInSc7	projev
také	také	k9	také
na	na	k7c6	na
zvláštním	zvláštní	k2eAgNnSc6d1	zvláštní
společném	společný	k2eAgNnSc6d1	společné
zasedání	zasedání	k1gNnSc6	zasedání
obou	dva	k4xCgFnPc2	dva
komor	komora	k1gFnPc2	komora
Kongresu	kongres	k1gInSc2	kongres
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
důležitější	důležitý	k2eAgMnSc1d2	důležitější
bylo	být	k5eAaImAgNnS	být
vymanění	vymanění	k1gNnSc1	vymanění
se	se	k3xPyFc4	se
z	z	k7c2	z
vlivu	vliv	k1gInSc2	vliv
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
symbolizované	symbolizovaný	k2eAgInPc1d1	symbolizovaný
odchodem	odchod	k1gInSc7	odchod
sovětských	sovětský	k2eAgFnPc2d1	sovětská
vojenských	vojenský	k2eAgFnPc2d1	vojenská
posádek	posádka	k1gFnPc2	posádka
a	a	k8xC	a
zánikem	zánik	k1gInSc7	zánik
politických	politický	k2eAgFnPc2d1	politická
struktur	struktura	k1gFnPc2	struktura
sovětského	sovětský	k2eAgInSc2d1	sovětský
bloku	blok	k1gInSc2	blok
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gNnSc7	jeho
vedením	vedení	k1gNnSc7	vedení
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c4	na
získání	získání	k1gNnSc4	získání
členství	členství	k1gNnSc2	členství
v	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
organizacích	organizace	k1gFnPc6	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Havel	Havel	k1gMnSc1	Havel
byl	být	k5eAaImAgMnS	být
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
členem	člen	k1gInSc7	člen
Římského	římský	k2eAgInSc2d1	římský
klubu	klub	k1gInSc2	klub
<g/>
.	.	kIx.	.
</s>
<s>
Podílel	podílet	k5eAaImAgInS	podílet
se	se	k3xPyFc4	se
také	také	k9	také
na	na	k7c4	na
navázání	navázání	k1gNnSc4	navázání
vztahů	vztah	k1gInPc2	vztah
mezi	mezi	k7c7	mezi
státy	stát	k1gInPc7	stát
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
na	na	k7c6	na
novém	nový	k2eAgInSc6d1	nový
základě	základ	k1gInSc6	základ
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Visegrádské	visegrádský	k2eAgFnSc2d1	Visegrádská
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
samostatné	samostatný	k2eAgFnSc2d1	samostatná
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gFnSc4	jeho
prvním	první	k4xOgMnSc6	první
prezidentem	prezident	k1gMnSc7	prezident
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgMnS	zůstat
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
funkci	funkce	k1gFnSc6	funkce
dvě	dva	k4xCgNnPc1	dva
funkční	funkční	k2eAgNnPc1d1	funkční
období	období	k1gNnPc1	období
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
maximální	maximální	k2eAgFnSc7d1	maximální
ústavou	ústava	k1gFnSc7	ústava
povolenou	povolený	k2eAgFnSc4d1	povolená
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
26	[number]	k4	26
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
podruhé	podruhé	k6eAd1	podruhé
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
funkci	funkce	k1gFnSc4	funkce
opustil	opustit	k5eAaPmAgMnS	opustit
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2003	[number]	k4	2003
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
nástupcem	nástupce	k1gMnSc7	nástupce
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
volbě	volba	k1gFnSc6	volba
prezidenta	prezident	k1gMnSc2	prezident
dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1998	[number]	k4	1998
byl	být	k5eAaImAgMnS	být
Havel	Havel	k1gMnSc1	Havel
zvolen	zvolit	k5eAaPmNgMnS	zvolit
v	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
kole	kolo	k1gNnSc6	kolo
první	první	k4xOgFnSc2	první
volby	volba	k1gFnSc2	volba
<g/>
,	,	kIx,	,
když	když	k8xS	když
získal	získat	k5eAaPmAgMnS	získat
47	[number]	k4	47
z	z	k7c2	z
81	[number]	k4	81
hlasů	hlas	k1gInPc2	hlas
senátorů	senátor	k1gMnPc2	senátor
a	a	k8xC	a
99	[number]	k4	99
ze	z	k7c2	z
197	[number]	k4	197
hlasů	hlas	k1gInPc2	hlas
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
Republikánské	republikánský	k2eAgFnSc2d1	republikánská
strany	strana	k1gFnSc2	strana
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
poslanec	poslanec	k1gMnSc1	poslanec
Miroslav	Miroslav	k1gMnSc1	Miroslav
Sládek	Sládek	k1gMnSc1	Sládek
se	se	k3xPyFc4	se
tuto	tento	k3xDgFnSc4	tento
volbu	volba	k1gFnSc4	volba
neúspěšně	úspěšně	k6eNd1	úspěšně
pokusil	pokusit	k5eAaPmAgMnS	pokusit
napadnout	napadnout	k5eAaPmF	napadnout
u	u	k7c2	u
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byl	být	k5eAaImAgMnS	být
před	před	k7c7	před
volbou	volba	k1gFnSc7	volba
vzat	vzít	k5eAaPmNgInS	vzít
do	do	k7c2	do
vazby	vazba	k1gFnSc2	vazba
a	a	k8xC	a
nebylo	být	k5eNaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
umožněno	umožnit	k5eAaPmNgNnS	umožnit
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
se	se	k3xPyFc4	se
hlasování	hlasování	k1gNnSc4	hlasování
v	v	k7c6	v
Parlamentu	parlament	k1gInSc6	parlament
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
desetiletého	desetiletý	k2eAgNnSc2d1	desetileté
prezidentství	prezidentství	k1gNnSc2	prezidentství
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
se	se	k3xPyFc4	se
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
stala	stát	k5eAaPmAgFnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
členem	člen	k1gMnSc7	člen
Severoatlantické	severoatlantický	k2eAgFnSc2d1	Severoatlantická
aliance	aliance	k1gFnSc2	aliance
(	(	kIx(	(
<g/>
NATO	NATO	kA	NATO
<g/>
)	)	kIx)	)
a	a	k8xC	a
do	do	k7c2	do
závěrečné	závěrečný	k2eAgFnSc2d1	závěrečná
fáze	fáze	k1gFnSc2	fáze
pokročilo	pokročit	k5eAaPmAgNnS	pokročit
přijímání	přijímání	k1gNnSc1	přijímání
za	za	k7c4	za
člena	člen	k1gMnSc4	člen
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
bylo	být	k5eAaImAgNnS	být
Česko	Česko	k1gNnSc1	Česko
přijato	přijmout	k5eAaPmNgNnS	přijmout
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
naplnilo	naplnit	k5eAaPmAgNnS	naplnit
heslo	heslo	k1gNnSc1	heslo
sametové	sametový	k2eAgFnSc2d1	sametová
revoluce	revoluce	k1gFnSc2	revoluce
"	"	kIx"	"
<g/>
Zpátky	zpátky	k6eAd1	zpátky
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
"	"	kIx"	"
a	a	k8xC	a
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
se	se	k3xPyFc4	se
integrovala	integrovat	k5eAaBmAgFnS	integrovat
do	do	k7c2	do
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
politických	politický	k2eAgFnPc2d1	politická
struktur	struktura	k1gFnPc2	struktura
Západu	západ	k1gInSc2	západ
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
mohl	moct	k5eAaImAgMnS	moct
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
prestiži	prestiž	k1gFnSc3	prestiž
působit	působit	k5eAaImF	působit
na	na	k7c4	na
západní	západní	k2eAgNnSc4d1	západní
veřejné	veřejný	k2eAgNnSc4d1	veřejné
mínění	mínění	k1gNnSc4	mínění
i	i	k8xC	i
politické	politický	k2eAgFnSc2d1	politická
elity	elita	k1gFnSc2	elita
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
zapojení	zapojení	k1gNnSc4	zapojení
Česka	Česko	k1gNnSc2	Česko
i	i	k9	i
ostatních	ostatní	k2eAgInPc2d1	ostatní
středoevropských	středoevropský	k2eAgInPc2d1	středoevropský
států	stát	k1gInPc2	stát
do	do	k7c2	do
NATO	NATO	kA	NATO
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
organizací	organizace	k1gFnPc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
pozornost	pozornost	k1gFnSc4	pozornost
věnoval	věnovat	k5eAaImAgMnS	věnovat
česko-německým	českoěmecký	k2eAgInPc3d1	česko-německý
vztahům	vztah	k1gInPc3	vztah
<g/>
,	,	kIx,	,
poškozeným	poškozený	k2eAgInPc3d1	poškozený
druhou	druhý	k4xOgFnSc7	druhý
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
a	a	k8xC	a
jejími	její	k3xOp3gInPc7	její
následky	následek	k1gInPc7	následek
<g/>
.	.	kIx.	.
</s>
<s>
Česko-německá	českoěmecký	k2eAgFnSc1d1	česko-německá
deklarace	deklarace	k1gFnSc1	deklarace
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
omluvily	omluvit	k5eAaPmAgFnP	omluvit
za	za	k7c4	za
vzájemně	vzájemně	k6eAd1	vzájemně
způsobené	způsobený	k2eAgNnSc4d1	způsobené
bezpráví	bezpráví	k1gNnSc4	bezpráví
a	a	k8xC	a
deklarovaly	deklarovat	k5eAaBmAgFnP	deklarovat
společné	společný	k2eAgFnPc4d1	společná
demokratické	demokratický	k2eAgFnPc4d1	demokratická
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
vyvrcholením	vyvrcholení	k1gNnSc7	vyvrcholení
tohoto	tento	k3xDgInSc2	tento
směru	směr	k1gInSc2	směr
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
podporoval	podporovat	k5eAaImAgInS	podporovat
politiku	politika	k1gFnSc4	politika
NATO	NATO	kA	NATO
včetně	včetně	k7c2	včetně
vojenských	vojenský	k2eAgFnPc2d1	vojenská
operací	operace	k1gFnPc2	operace
<g/>
,	,	kIx,	,
jakou	jaký	k3yQgFnSc4	jaký
bylo	být	k5eAaImAgNnS	být
bombardování	bombardování	k1gNnSc1	bombardování
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
mu	on	k3xPp3gMnSc3	on
kritici	kritik	k1gMnPc1	kritik
přisuzují	přisuzovat	k5eAaImIp3nP	přisuzovat
autorství	autorství	k1gNnSc4	autorství
výrazu	výraz	k1gInSc2	výraz
"	"	kIx"	"
<g/>
humanitární	humanitární	k2eAgNnSc1d1	humanitární
bombardování	bombardování	k1gNnSc1	bombardování
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
vyjádření	vyjádření	k1gNnSc2	vyjádření
Richarda	Richard	k1gMnSc2	Richard
Falbra	Falbr	k1gMnSc2	Falbr
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
překroutil	překroutit	k5eAaPmAgInS	překroutit
Havlova	Havlův	k2eAgNnSc2d1	Havlovo
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Podporoval	podporovat	k5eAaImAgMnS	podporovat
také	také	k9	také
operaci	operace	k1gFnSc4	operace
Trvalá	trvalý	k2eAgFnSc1d1	trvalá
svoboda	svoboda	k1gFnSc1	svoboda
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
intervenci	intervence	k1gFnSc3	intervence
USA	USA	kA	USA
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc2	jejich
spojenců	spojenec	k1gMnPc2	spojenec
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Vnitropoliticky	vnitropoliticky	k6eAd1	vnitropoliticky
se	se	k3xPyFc4	se
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
opíral	opírat	k5eAaImAgMnS	opírat
o	o	k7c4	o
menší	malý	k2eAgFnPc4d2	menší
středové	středový	k2eAgFnPc4d1	středová
strany	strana	k1gFnPc4	strana
a	a	k8xC	a
často	často	k6eAd1	často
se	se	k3xPyFc4	se
projevovalo	projevovat	k5eAaImAgNnS	projevovat
napětí	napětí	k1gNnSc1	napětí
mezi	mezi	k7c7	mezi
ním	on	k3xPp3gInSc7	on
a	a	k8xC	a
ODS	ODS	kA	ODS
vedenou	vedený	k2eAgFnSc4d1	vedená
Václavem	Václav	k1gMnSc7	Václav
Klausem	Klaus	k1gMnSc7	Klaus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
vnitropolitické	vnitropolitický	k2eAgFnSc2d1	vnitropolitická
krize	krize	k1gFnSc2	krize
a	a	k8xC	a
rozštěpení	rozštěpení	k1gNnSc2	rozštěpení
ODS	ODS	kA	ODS
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
Havel	Havel	k1gMnSc1	Havel
podpořil	podpořit	k5eAaPmAgMnS	podpořit
protiklausovské	protiklausovský	k2eAgNnSc4d1	protiklausovské
křídlo	křídlo	k1gNnSc4	křídlo
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
vzešla	vzejít	k5eAaPmAgFnS	vzejít
Unie	unie	k1gFnSc1	unie
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
velký	velký	k2eAgInSc4d1	velký
podíl	podíl	k1gInSc4	podíl
na	na	k7c4	na
sestavení	sestavení	k1gNnSc4	sestavení
poloúřednické	poloúřednický	k2eAgFnSc2d1	poloúřednická
vlády	vláda	k1gFnSc2	vláda
Josefa	Josef	k1gMnSc2	Josef
Tošovského	Tošovský	k1gMnSc2	Tošovský
bez	bez	k7c2	bez
Klausovy	Klausův	k2eAgFnSc2d1	Klausova
účasti	účast	k1gFnSc2	účast
<g/>
.	.	kIx.	.
</s>
<s>
Nesoulad	nesoulad	k1gInSc1	nesoulad
mezi	mezi	k7c7	mezi
prezidentem	prezident	k1gMnSc7	prezident
a	a	k8xC	a
předsedou	předseda	k1gMnSc7	předseda
ODS	ODS	kA	ODS
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
zřetelný	zřetelný	k2eAgInSc1d1	zřetelný
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
Havlových	Havlových	k2eAgFnPc2d1	Havlových
dvou	dva	k4xCgFnPc2	dva
funkčních	funkční	k2eAgFnPc2d1	funkční
období	období	k1gNnPc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
česká	český	k2eAgFnSc1d1	Česká
veřejnost	veřejnost	k1gFnSc1	veřejnost
ostře	ostro	k6eAd1	ostro
sledovala	sledovat	k5eAaImAgFnS	sledovat
i	i	k9	i
Havlův	Havlův	k2eAgInSc4d1	Havlův
soukromý	soukromý	k2eAgInSc4d1	soukromý
život	život	k1gInSc4	život
<g/>
:	:	kIx,	:
úmrtí	úmrtí	k1gNnSc2	úmrtí
manželky	manželka	k1gFnSc2	manželka
Olgy	Olga	k1gFnSc2	Olga
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1996	[number]	k4	1996
<g/>
;	;	kIx,	;
vážné	vážný	k2eAgFnSc2d1	vážná
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
prezidenta	prezident	k1gMnSc4	prezident
několikrát	několikrát	k6eAd1	několikrát
ohrožovaly	ohrožovat	k5eAaImAgInP	ohrožovat
na	na	k7c6	na
životě	život	k1gInSc6	život
<g/>
;	;	kIx,	;
nový	nový	k2eAgInSc4d1	nový
sňatek	sňatek	k1gInSc4	sňatek
s	s	k7c7	s
herečkou	herečka	k1gFnSc7	herečka
Dagmar	Dagmar	k1gFnSc7	Dagmar
Veškrnovou	Veškrnová	k1gFnSc7	Veškrnová
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1997	[number]	k4	1997
<g/>
;	;	kIx,	;
spory	spor	k1gInPc1	spor
se	s	k7c7	s
švagrovou	švagrův	k2eAgFnSc7d1	švagrova
Dagmar	Dagmar	k1gFnSc7	Dagmar
o	o	k7c4	o
Palác	palác	k1gInSc4	palác
Lucerna	lucerna	k1gFnSc1	lucerna
a	a	k8xC	a
Barrandovské	barrandovský	k2eAgFnPc1d1	Barrandovská
terasy	terasa	k1gFnPc1	terasa
<g/>
,	,	kIx,	,
restituované	restituovaný	k2eAgNnSc1d1	restituovaný
rodinné	rodinný	k2eAgNnSc1d1	rodinné
dědictví	dědictví	k1gNnSc1	dědictví
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
část	část	k1gFnSc1	část
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
prodal	prodat	k5eAaPmAgMnS	prodat
společnosti	společnost	k1gFnSc3	společnost
Chemapol	chemapol	k1gInSc4	chemapol
Reality	realita	k1gFnSc2	realita
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
předsedou	předseda	k1gMnSc7	předseda
představenstva	představenstvo	k1gNnSc2	představenstvo
byl	být	k5eAaImAgMnS	být
Václav	Václav	k1gMnSc1	Václav
Junek	Junek	k1gMnSc1	Junek
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
se	se	k3xPyFc4	se
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
obou	dva	k4xCgFnPc2	dva
svých	svůj	k3xOyFgFnPc2	svůj
manželek	manželka	k1gFnPc2	manželka
věnoval	věnovat	k5eAaImAgMnS	věnovat
i	i	k9	i
charitativní	charitativní	k2eAgFnPc4d1	charitativní
činnosti	činnost	k1gFnPc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Nadace	nadace	k1gFnSc1	nadace
Dagmar	Dagmar	k1gFnSc2	Dagmar
a	a	k8xC	a
Václava	Václav	k1gMnSc2	Václav
Havlových	Havlových	k2eAgFnSc2d1	Havlových
Vize	vize	k1gFnSc2	vize
97	[number]	k4	97
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yRgFnSc2	který
vložil	vložit	k5eAaPmAgInS	vložit
50	[number]	k4	50
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
především	především	k9	především
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
sociální	sociální	k2eAgFnSc2d1	sociální
<g/>
,	,	kIx,	,
zdravotnické	zdravotnický	k2eAgFnSc2d1	zdravotnická
<g/>
,	,	kIx,	,
vzdělávací	vzdělávací	k2eAgFnSc2d1	vzdělávací
a	a	k8xC	a
kulturní	kulturní	k2eAgFnSc2d1	kulturní
<g/>
.	.	kIx.	.
</s>
<s>
Nadace	nadace	k1gFnSc1	nadace
Forum	forum	k1gNnSc1	forum
2000	[number]	k4	2000
pořádá	pořádat	k5eAaImIp3nS	pořádat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
každoroční	každoroční	k2eAgNnSc1d1	každoroční
setkání	setkání	k1gNnSc1	setkání
významných	významný	k2eAgFnPc2d1	významná
osobností	osobnost	k1gFnPc2	osobnost
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
úřadu	úřad	k1gInSc2	úřad
se	se	k3xPyFc4	se
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
nadále	nadále	k6eAd1	nadále
vyjadřoval	vyjadřovat	k5eAaImAgMnS	vyjadřovat
k	k	k7c3	k
politice	politika	k1gFnSc3	politika
a	a	k8xC	a
podporoval	podporovat	k5eAaImAgMnS	podporovat
Stranu	strana	k1gFnSc4	strana
zelených	zelený	k2eAgMnPc2d1	zelený
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tzv.	tzv.	kA	tzv.
zelenou	zelený	k2eAgFnSc4d1	zelená
politiku	politika	k1gFnSc4	politika
se	se	k3xPyFc4	se
vyslovoval	vyslovovat	k5eAaImAgInS	vyslovovat
už	už	k6eAd1	už
od	od	k7c2	od
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
podporoval	podporovat	k5eAaImAgMnS	podporovat
německou	německý	k2eAgFnSc4d1	německá
stranu	strana	k1gFnSc4	strana
Die	Die	k1gFnSc2	Die
Grünen	Grünna	k1gFnPc2	Grünna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
přestávce	přestávka	k1gFnSc6	přestávka
napsal	napsat	k5eAaBmAgMnS	napsat
další	další	k2eAgFnSc4d1	další
divadelní	divadelní	k2eAgFnSc4d1	divadelní
hru	hra	k1gFnSc4	hra
Odcházení	odcházení	k1gNnSc2	odcházení
(	(	kIx(	(
<g/>
premiéra	premiéra	k1gFnSc1	premiéra
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
inspirovanou	inspirovaný	k2eAgFnSc4d1	inspirovaná
vlastními	vlastní	k2eAgFnPc7d1	vlastní
zkušenostmi	zkušenost	k1gFnPc7	zkušenost
z	z	k7c2	z
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
amerických	americký	k2eAgMnPc2d1	americký
prezidentů	prezident	k1gMnPc2	prezident
založil	založit	k5eAaPmAgInS	založit
Knihovnu	knihovna	k1gFnSc4	knihovna
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
i	i	k8xC	i
badatele	badatel	k1gMnPc4	badatel
shromažďuje	shromažďovat	k5eAaImIp3nS	shromažďovat
materiály	materiál	k1gInPc4	materiál
vztahující	vztahující	k2eAgInPc4d1	vztahující
se	se	k3xPyFc4	se
k	k	k7c3	k
Havlově	Havlův	k2eAgFnSc3d1	Havlova
tvorbě	tvorba	k1gFnSc3	tvorba
a	a	k8xC	a
politickému	politický	k2eAgNnSc3d1	politické
působení	působení	k1gNnSc3	působení
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
místopředsedou	místopředseda	k1gMnSc7	místopředseda
amerického	americký	k2eAgNnSc2d1	americké
neokonzervativního	okonzervativní	k2eNgNnSc2d1	neokonzervativní
think	thinko	k1gNnPc2	thinko
tanku	tank	k1gInSc2	tank
The	The	k1gMnSc2	The
Committee	Committe	k1gMnSc2	Committe
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Present	Present	k1gMnSc1	Present
Danger	Danger	k1gMnSc1	Danger
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
ocenění	ocenění	k1gNnSc2	ocenění
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
byl	být	k5eAaImAgMnS	být
nositelem	nositel	k1gMnSc7	nositel
státních	státní	k2eAgFnPc2d1	státní
vyznamenání	vyznamenání	k1gNnPc2	vyznamenání
řady	řada	k1gFnSc2	řada
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
mnoha	mnoho	k4c2	mnoho
cen	cena	k1gFnPc2	cena
za	za	k7c4	za
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
tvorbu	tvorba	k1gFnSc4	tvorba
i	i	k8xC	i
občanské	občanský	k2eAgInPc4d1	občanský
postoje	postoj	k1gInPc4	postoj
a	a	k8xC	a
desítek	desítka	k1gFnPc2	desítka
čestných	čestný	k2eAgInPc2d1	čestný
doktorátů	doktorát	k1gInPc2	doktorát
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1997	[number]	k4	1997
obdržel	obdržet	k5eAaPmAgMnS	obdržet
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
jako	jako	k9	jako
první	první	k4xOgFnSc4	první
Čestnou	čestný	k2eAgFnSc4d1	čestná
medaili	medaile	k1gFnSc4	medaile
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
za	za	k7c4	za
věrnost	věrnost	k1gFnSc4	věrnost
jeho	on	k3xPp3gInSc2	on
odkazu	odkaz	k1gInSc2	odkaz
a	a	k8xC	a
jeho	on	k3xPp3gNnSc2	on
uskutečňování	uskutečňování	k1gNnSc2	uskutečňování
od	od	k7c2	od
Masarykova	Masarykův	k2eAgNnSc2d1	Masarykovo
demokratického	demokratický	k2eAgNnSc2d1	demokratické
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
založit	založit	k5eAaPmF	založit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
byl	být	k5eAaImAgMnS	být
Havlovi	Havel	k1gMnSc3	Havel
usnesením	usnesení	k1gNnSc7	usnesení
Senátu	senát	k1gInSc2	senát
a	a	k8xC	a
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
propůjčen	propůjčen	k2eAgInSc1d1	propůjčen
Řád	řád	k1gInSc1	řád
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc4	Masaryk
a	a	k8xC	a
Řád	řád	k1gInSc1	řád
Bílého	bílý	k2eAgInSc2d1	bílý
lva	lev	k1gInSc2	lev
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Senát	senát	k1gInSc1	senát
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
usnesení	usnesení	k1gNnSc6	usnesení
také	také	k9	také
schválil	schválit	k5eAaPmAgInS	schválit
formulaci	formulace	k1gFnSc3	formulace
"	"	kIx"	"
<g/>
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
se	se	k3xPyFc4	se
o	o	k7c4	o
stát	stát	k1gInSc4	stát
<g/>
"	"	kIx"	"
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
zásluhách	zásluha	k1gFnPc6	zásluha
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
vláda	vláda	k1gFnSc1	vláda
ČR	ČR	kA	ČR
vypracovala	vypracovat	k5eAaPmAgFnS	vypracovat
návrh	návrh	k1gInSc4	návrh
Zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
zásluhách	zásluha	k1gFnPc6	zásluha
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
.	.	kIx.	.
</s>
<s>
Poslanci	poslanec	k1gMnPc1	poslanec
jej	on	k3xPp3gMnSc4	on
schválili	schválit	k5eAaPmAgMnP	schválit
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
senátoři	senátor	k1gMnPc1	senátor
29	[number]	k4	29
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
zákon	zákon	k1gInSc4	zákon
podepsal	podepsat	k5eAaPmAgMnS	podepsat
16	[number]	k4	16
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
byl	být	k5eAaImAgMnS	být
také	také	k9	také
několikrát	několikrát	k6eAd1	několikrát
navržen	navrhnout	k5eAaPmNgInS	navrhnout
na	na	k7c4	na
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
i	i	k9	i
čestným	čestný	k2eAgMnSc7d1	čestný
členem	člen	k1gMnSc7	člen
Římského	římský	k2eAgInSc2d1	římský
klubu	klub	k1gInSc2	klub
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
byl	být	k5eAaImAgMnS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
majitelem	majitel	k1gMnSc7	majitel
venkovské	venkovský	k2eAgFnSc2d1	venkovská
usedlosti	usedlost	k1gFnSc2	usedlost
v	v	k7c6	v
Hrádečku	hrádeček	k1gInSc6	hrádeček
u	u	k7c2	u
Trutnova	Trutnov	k1gInSc2	Trutnov
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
zakoupil	zakoupit	k5eAaPmAgInS	zakoupit
přes	přes	k7c4	přes
novinový	novinový	k2eAgInSc4d1	novinový
inzerát	inzerát	k1gInSc4	inzerát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
získal	získat	k5eAaPmAgMnS	získat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
restitučního	restituční	k2eAgInSc2d1	restituční
zákona	zákon	k1gInSc2	zákon
společně	společně	k6eAd1	společně
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
Ivanem	Ivan	k1gMnSc7	Ivan
třetinu	třetina	k1gFnSc4	třetina
vily	vila	k1gFnSc2	vila
jejich	jejich	k3xOp3gMnSc2	jejich
děda	děd	k1gMnSc2	děd
Huga	Hugo	k1gMnSc2	Hugo
Vavrečky	Vavrečka	k1gFnSc2	Vavrečka
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
architekt	architekt	k1gMnSc1	architekt
Vladimír	Vladimír	k1gMnSc1	Vladimír
Karfík	Karfík	k1gMnSc1	Karfík
<g/>
,	,	kIx,	,
a	a	k8xC	a
čtyři	čtyři	k4xCgFnPc4	čtyři
nemovitosti	nemovitost	k1gFnPc4	nemovitost
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
:	:	kIx,	:
Palác	palác	k1gInSc1	palác
Lucerna	lucerna	k1gFnSc1	lucerna
na	na	k7c6	na
Václavském	václavský	k2eAgNnSc6d1	Václavské
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
činžovní	činžovní	k2eAgInSc1d1	činžovní
dům	dům	k1gInSc1	dům
na	na	k7c6	na
Rašínově	Rašínův	k2eAgNnSc6d1	Rašínovo
nábřeží	nábřeží	k1gNnSc6	nábřeží
<g/>
,	,	kIx,	,
vyhlídkovou	vyhlídkový	k2eAgFnSc4d1	vyhlídková
restauraci	restaurace	k1gFnSc4	restaurace
Barrandovské	barrandovský	k2eAgFnSc2d1	Barrandovská
terasy	terasa	k1gFnSc2	terasa
postavenou	postavený	k2eAgFnSc4d1	postavená
architektem	architekt	k1gMnSc7	architekt
Maxem	Max	k1gMnSc7	Max
Urbanem	Urban	k1gMnSc7	Urban
a	a	k8xC	a
vilu	vila	k1gFnSc4	vila
Miloše	Miloš	k1gMnSc2	Miloš
Havla	Havel	k1gMnSc2	Havel
od	od	k7c2	od
architekta	architekt	k1gMnSc2	architekt
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Grégra	Grégr	k1gMnSc2	Grégr
<g/>
.	.	kIx.	.
</s>
<s>
Vilu	vila	k1gFnSc4	vila
Miloše	Miloš	k1gMnSc2	Miloš
Havla	Havel	k1gMnSc2	Havel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
společně	společně	k6eAd1	společně
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
prodal	prodat	k5eAaPmAgInS	prodat
společnosti	společnost	k1gFnSc2	společnost
Charouz	Charouz	k1gInSc1	Charouz
Holding	holding	k1gInSc1	holding
řízené	řízený	k2eAgInPc1d1	řízený
Antonínem	Antonín	k1gMnSc7	Antonín
Charouzem	Charouz	k1gInSc7	Charouz
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
zřídila	zřídit	k5eAaPmAgFnS	zřídit
soukromý	soukromý	k2eAgInSc4d1	soukromý
klub	klub	k1gInSc4	klub
Playbon	Playbona	k1gFnPc2	Playbona
<g/>
.	.	kIx.	.
</s>
<s>
Tentýž	týž	k3xTgInSc4	týž
rok	rok	k1gInSc4	rok
společně	společně	k6eAd1	společně
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
první	první	k4xOgFnSc7	první
ženou	žena	k1gFnSc7	žena
Olgou	Olga	k1gFnSc7	Olga
koupil	koupit	k5eAaPmAgMnS	koupit
vilu	vila	k1gFnSc4	vila
v	v	k7c6	v
Dělostřelecké	dělostřelecký	k2eAgFnSc6d1	dělostřelecká
ulici	ulice	k1gFnSc6	ulice
poblíž	poblíž	k7c2	poblíž
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Šestinu	šestina	k1gFnSc4	šestina
Vavrečkovy	Vavrečkův	k2eAgFnSc2d1	Vavrečkova
vily	vila	k1gFnSc2	vila
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
daroval	darovat	k5eAaPmAgInS	darovat
pod	pod	k7c7	pod
podmínkou	podmínka	k1gFnSc7	podmínka
využití	využití	k1gNnSc2	využití
ke	k	k7c3	k
vzdělávacím	vzdělávací	k2eAgInPc3d1	vzdělávací
účelům	účel	k1gInPc3	účel
městu	město	k1gNnSc3	město
Zlín	Zlín	k1gInSc1	Zlín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
prodal	prodat	k5eAaPmAgMnS	prodat
svou	svůj	k3xOyFgFnSc4	svůj
polovinu	polovina	k1gFnSc4	polovina
Paláce	palác	k1gInSc2	palác
Lucerna	lucerna	k1gFnSc1	lucerna
společnosti	společnost	k1gFnSc2	společnost
Chemapol	chemapol	k1gInSc1	chemapol
Reality	realita	k1gFnSc2	realita
řízené	řízený	k2eAgFnSc2d1	řízená
Václavem	Václav	k1gMnSc7	Václav
Junkem	Junek	k1gMnSc7	Junek
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
podíl	podíl	k1gInSc4	podíl
v	v	k7c6	v
Barrandovských	barrandovský	k2eAgFnPc6d1	Barrandovská
terasách	terasa	k1gFnPc6	terasa
tehdy	tehdy	k6eAd1	tehdy
převedl	převést	k5eAaPmAgMnS	převést
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
druhou	druhý	k4xOgFnSc4	druhý
ženu	žena	k1gFnSc4	žena
Dagmar	Dagmar	k1gFnSc4	Dagmar
Havlovou	Havlová	k1gFnSc4	Havlová
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yQgFnSc4	který
pak	pak	k6eAd1	pak
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
přepsal	přepsat	k5eAaPmAgMnS	přepsat
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
svého	svůj	k3xOyFgInSc2	svůj
ostatního	ostatní	k2eAgInSc2d1	ostatní
majetku	majetek	k1gInSc2	majetek
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
všechny	všechen	k3xTgFnPc4	všechen
své	svůj	k3xOyFgFnPc4	svůj
nemovitosti	nemovitost	k1gFnPc4	nemovitost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
si	se	k3xPyFc3	se
pořídil	pořídit	k5eAaPmAgInS	pořídit
i	i	k9	i
rezidenci	rezidence	k1gFnSc4	rezidence
v	v	k7c6	v
přímořském	přímořský	k2eAgNnSc6d1	přímořské
letovisku	letovisko	k1gNnSc6	letovisko
Olhos	Olhos	k1gMnSc1	Olhos
de	de	k?	de
Água	Água	k1gMnSc1	Água
u	u	k7c2	u
portugalského	portugalský	k2eAgNnSc2d1	portugalské
města	město	k1gNnSc2	město
Albufeira	Albufeiro	k1gNnSc2	Albufeiro
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
pak	pak	k6eAd1	pak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
prodal	prodat	k5eAaPmAgMnS	prodat
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
nadace	nadace	k1gFnSc2	nadace
Vize	vize	k1gFnSc1	vize
97	[number]	k4	97
při	při	k7c6	při
jejím	její	k3xOp3gInSc6	její
vzniku	vznik	k1gInSc6	vznik
vložil	vložit	k5eAaPmAgMnS	vložit
50	[number]	k4	50
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
do	do	k7c2	do
předchozích	předchozí	k2eAgFnPc2d1	předchozí
nadací	nadace	k1gFnPc2	nadace
stejného	stejný	k2eAgNnSc2d1	stejné
zaměření	zaměření	k1gNnSc2	zaměření
pak	pak	k6eAd1	pak
9	[number]	k4	9
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
daroval	darovat	k5eAaPmAgMnS	darovat
v	v	k7c6	v
letech	let	k1gInPc6	let
1986	[number]	k4	1986
<g/>
–	–	k?	–
<g/>
1997	[number]	k4	1997
na	na	k7c6	na
věci	věc	k1gFnSc6	věc
veřejného	veřejný	k2eAgInSc2d1	veřejný
zájmu	zájem	k1gInSc2	zájem
85	[number]	k4	85
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Státní	státní	k2eAgInSc1d1	státní
pohřeb	pohřeb	k1gInSc1	pohřeb
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
v	v	k7c4	v
9.46	[number]	k4	9.46
hodin	hodina	k1gFnPc2	hodina
na	na	k7c4	na
následky	následek	k1gInPc4	následek
dlouhodobých	dlouhodobý	k2eAgInPc2d1	dlouhodobý
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
spánku	spánek	k1gInSc6	spánek
na	na	k7c4	na
oběhové	oběhový	k2eAgNnSc4d1	oběhové
selhání	selhání	k1gNnSc4	selhání
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
u	u	k7c2	u
něj	on	k3xPp3gInSc2	on
jeho	jeho	k3xOp3gFnPc6	jeho
manželka	manželka	k1gFnSc1	manželka
Dagmar	Dagmar	k1gFnSc1	Dagmar
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
sester	sestra	k1gFnPc2	sestra
boromejek	boromejka	k1gFnPc2	boromejka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
o	o	k7c4	o
Havla	Havel	k1gMnSc4	Havel
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
měsících	měsíc	k1gInPc6	měsíc
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
staraly	starat	k5eAaImAgFnP	starat
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
oznámeným	oznámený	k2eAgInSc7d1	oznámený
údajem	údaj	k1gInSc7	údaj
pro	pro	k7c4	pro
čas	čas	k1gInSc4	čas
smrti	smrt	k1gFnSc3	smrt
bylo	být	k5eAaImAgNnS	být
10.15	[number]	k4	10.15
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
Dagmar	Dagmar	k1gFnSc1	Dagmar
Havlová	Havlová	k1gFnSc1	Havlová
však	však	k9	však
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
Lidové	lidový	k2eAgFnPc4d1	lidová
noviny	novina	k1gFnPc4	novina
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
poté	poté	k6eAd1	poté
upřesnila	upřesnit	k5eAaPmAgFnS	upřesnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
zemřel	zemřít	k5eAaPmAgMnS	zemřít
o	o	k7c4	o
téměř	téměř	k6eAd1	téměř
půl	půl	k1xP	půl
hodiny	hodina	k1gFnSc2	hodina
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
v	v	k7c4	v
9.46	[number]	k4	9.46
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
měsících	měsíc	k1gInPc6	měsíc
svého	své	k1gNnSc2	své
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
Havel	Havel	k1gMnSc1	Havel
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
svých	svůj	k3xOyFgFnPc2	svůj
dlouhotrvajících	dlouhotrvající	k2eAgFnPc2d1	dlouhotrvající
zdravotních	zdravotní	k2eAgFnPc2d1	zdravotní
komplikací	komplikace	k1gFnPc2	komplikace
veřejného	veřejný	k2eAgInSc2d1	veřejný
života	život	k1gInSc2	život
již	již	k6eAd1	již
spíše	spíše	k9	spíše
stranil	stranit	k5eAaImAgInS	stranit
<g/>
.	.	kIx.	.
</s>
<s>
Odpočíval	odpočívat	k5eAaImAgMnS	odpočívat
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
chalupě	chalupa	k1gFnSc6	chalupa
v	v	k7c6	v
Hrádečku	hrádeček	k1gInSc6	hrádeček
u	u	k7c2	u
Trutnova	Trutnov	k1gInSc2	Trutnov
a	a	k8xC	a
jezdil	jezdit	k5eAaImAgMnS	jezdit
do	do	k7c2	do
lázní	lázeň	k1gFnPc2	lázeň
<g/>
.	.	kIx.	.
</s>
<s>
Naposledy	naposledy	k6eAd1	naposledy
veřejně	veřejně	k6eAd1	veřejně
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
týden	týden	k1gInSc4	týden
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
tibetským	tibetský	k2eAgMnSc7d1	tibetský
dalajlámou	dalajláma	k1gMnSc7	dalajláma
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
jeho	jeho	k3xOp3gFnSc2	jeho
návštěvy	návštěva	k1gFnSc2	návštěva
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Havlovo	Havlův	k2eAgNnSc1d1	Havlovo
úplně	úplně	k6eAd1	úplně
poslední	poslední	k2eAgNnSc1d1	poslední
veřejné	veřejný	k2eAgNnSc1d1	veřejné
vystoupení	vystoupení	k1gNnSc1	vystoupení
se	se	k3xPyFc4	se
odehrálo	odehrát	k5eAaPmAgNnS	odehrát
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
galerii	galerie	k1gFnSc6	galerie
DOX	DOX	kA	DOX
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
převzal	převzít	k5eAaPmAgInS	převzít
cenu	cena	k1gFnSc4	cena
slovenské	slovenský	k2eAgFnSc2d1	slovenská
Nadace	nadace	k1gFnSc2	nadace
Jána	Ján	k1gMnSc2	Ján
Langoše	langoš	k1gInSc2	langoš
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
úmrtí	úmrtí	k1gNnSc6	úmrtí
Václava	Václav	k1gMnSc4	Václav
Havla	Havel	k1gMnSc4	Havel
reagovali	reagovat	k5eAaBmAgMnP	reagovat
čeští	český	k2eAgMnPc1d1	český
i	i	k8xC	i
světoví	světový	k2eAgMnPc1d1	světový
politici	politik	k1gMnPc1	politik
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
běžní	běžný	k2eAgMnPc1d1	běžný
občané	občan	k1gMnPc1	občan
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c4	v
podvečer	podvečer	k1gInSc4	podvečer
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
začali	začít	k5eAaPmAgMnP	začít
spontánně	spontánně	k6eAd1	spontánně
scházet	scházet	k5eAaImF	scházet
<g/>
,	,	kIx,	,
např.	např.	kA	např.
na	na	k7c6	na
Václavském	václavský	k2eAgNnSc6d1	Václavské
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
či	či	k8xC	či
Moravském	moravský	k2eAgNnSc6d1	Moravské
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
uctili	uctít	k5eAaPmAgMnP	uctít
jeho	jeho	k3xOp3gFnSc4	jeho
památku	památka	k1gFnSc4	památka
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
v	v	k7c4	v
18	[number]	k4	18
hodin	hodina	k1gFnPc2	hodina
se	se	k3xPyFc4	se
také	také	k9	také
na	na	k7c4	na
popud	popud	k1gInSc4	popud
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
Dominika	Dominik	k1gMnSc2	Dominik
Duky	Duk	k2eAgInPc1d1	Duk
rozezněly	rozeznět	k5eAaPmAgInP	rozeznět
zvony	zvon	k1gInPc1	zvon
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
soustrast	soustrast	k1gFnSc4	soustrast
jeho	jeho	k3xOp3gFnSc3	jeho
vdově	vdova	k1gFnSc3	vdova
Dagmar	Dagmar	k1gFnSc4	Dagmar
Havlové	Havlová	k1gFnSc2	Havlová
a	a	k8xC	a
inicioval	iniciovat	k5eAaBmAgMnS	iniciovat
ustavení	ustavení	k1gNnSc4	ustavení
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
státní	státní	k2eAgInSc4d1	státní
pohřeb	pohřeb	k1gInSc4	pohřeb
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
10	[number]	k4	10
<g/>
.	.	kIx.	.
hodiny	hodina	k1gFnSc2	hodina
v	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
Rotmayerově	Rotmayerův	k2eAgInSc6d1	Rotmayerův
sále	sál	k1gInSc6	sál
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
pod	pod	k7c7	pod
Štursovou	Štursův	k2eAgFnSc7d1	Štursova
mramorovou	mramorový	k2eAgFnSc7d1	mramorová
sochou	socha	k1gFnSc7	socha
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
vystaveny	vystavit	k5eAaPmNgInP	vystavit
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
kondolenční	kondolenční	k2eAgFnSc2d1	kondolenční
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
do	do	k7c2	do
nichž	jenž	k3xRgMnPc2	jenž
zapsali	zapsat	k5eAaPmAgMnP	zapsat
jako	jako	k8xS	jako
první	první	k4xOgFnSc1	první
svou	svůj	k3xOyFgFnSc4	svůj
kondolenci	kondolence	k1gFnSc6	kondolence
představitelé	představitel	k1gMnPc1	představitel
státu	stát	k1gInSc2	stát
a	a	k8xC	a
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Dominik	Dominik	k1gMnSc1	Dominik
Duka	Duka	k1gMnSc1	Duka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dnech	den	k1gInPc6	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
byla	být	k5eAaImAgFnS	být
rakev	rakev	k1gFnSc1	rakev
s	s	k7c7	s
ostatky	ostatek	k1gInPc7	ostatek
vystavena	vystavit	k5eAaPmNgFnS	vystavit
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
odsvěceného	odsvěcený	k2eAgInSc2d1	odsvěcený
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Anny	Anna	k1gFnSc2	Anna
(	(	kIx(	(
<g/>
sídlo	sídlo	k1gNnSc1	sídlo
kulturního	kulturní	k2eAgNnSc2d1	kulturní
centra	centrum	k1gNnSc2	centrum
Pražská	pražský	k2eAgFnSc1d1	Pražská
křižovatka	křižovatka	k1gFnSc1	křižovatka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
poklonily	poklonit	k5eAaPmAgFnP	poklonit
tisíce	tisíc	k4xCgInPc1	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byly	být	k5eAaImAgInP	být
ostatky	ostatek	k1gInPc1	ostatek
převezeny	převezen	k2eAgInPc1d1	převezen
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
desetitisíců	desetitisíce	k1gInPc2	desetitisíce
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
pohřebním	pohřební	k2eAgInSc6d1	pohřební
průvodu	průvod	k1gInSc6	průvod
přes	přes	k7c4	přes
Karlův	Karlův	k2eAgInSc4d1	Karlův
most	most	k1gInSc4	most
do	do	k7c2	do
Toskánského	toskánský	k2eAgInSc2d1	toskánský
paláce	palác	k1gInSc2	palác
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
na	na	k7c6	na
lafetě	lafeta	k1gFnSc6	lafeta
děla	dělo	k1gNnSc2	dělo
<g/>
,	,	kIx,	,
na	na	k7c4	na
níž	nízce	k6eAd2	nízce
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1937	[number]	k4	1937
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
svou	svůj	k3xOyFgFnSc4	svůj
poslední	poslední	k2eAgFnSc4d1	poslední
cestu	cesta	k1gFnSc4	cesta
T.	T.	kA	T.
G.	G.	kA	G.
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
,	,	kIx,	,
a	a	k8xC	a
za	za	k7c2	za
doprovodu	doprovod	k1gInSc2	doprovod
vojenské	vojenský	k2eAgFnSc2d1	vojenská
hudby	hudba	k1gFnSc2	hudba
do	do	k7c2	do
Vladislavského	vladislavský	k2eAgInSc2d1	vladislavský
sálu	sál	k1gInSc2	sál
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
ostatkům	ostatek	k1gInPc3	ostatek
Václava	Václav	k1gMnSc4	Václav
Havla	Havel	k1gMnSc2	Havel
během	během	k7c2	během
středy	středa	k1gFnSc2	středa
a	a	k8xC	a
čtvrtka	čtvrtek	k1gInSc2	čtvrtek
poklonilo	poklonit	k5eAaPmAgNnS	poklonit
20	[number]	k4	20
až	až	k8xS	až
30	[number]	k4	30
tisíc	tisíc	k4xCgInPc2	tisíc
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnPc1	jejich
květiny	květina	k1gFnPc1	květina
byly	být	k5eAaImAgFnP	být
ve	v	k7c6	v
dnech	den	k1gInPc6	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
až	až	k9	až
2	[number]	k4	2
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2012	[number]	k4	2012
převezeny	převézt	k5eAaPmNgFnP	převézt
na	na	k7c6	na
lodích	loď	k1gFnPc6	loď
do	do	k7c2	do
Děčína	Děčín	k1gInSc2	Děčín
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgFnP	být
"	"	kIx"	"
<g/>
odstrojeny	odstrojen	k2eAgFnPc1d1	odstrojen
<g/>
"	"	kIx"	"
a	a	k8xC	a
vypuštěny	vypustit	k5eAaPmNgInP	vypustit
do	do	k7c2	do
Labe	Labe	k1gNnSc2	Labe
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Severnímu	severní	k2eAgNnSc3d1	severní
moři	moře	k1gNnSc3	moře
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgInSc1d1	státní
pohřeb	pohřeb	k1gInSc1	pohřeb
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
hostí	host	k1gMnPc2wK	host
byl	být	k5eAaImAgInS	být
uspořádán	uspořádán	k2eAgInSc1d1	uspořádán
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
23	[number]	k4	23
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c6	na
úmrtí	úmrtí	k1gNnSc6	úmrtí
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
od	od	k7c2	od
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
třídenní	třídenní	k2eAgInSc1d1	třídenní
státní	státní	k2eAgInSc1d1	státní
smutek	smutek	k1gInSc1	smutek
a	a	k8xC	a
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
přijmout	přijmout	k5eAaPmF	přijmout
zákon	zákon	k1gInSc4	zákon
oceňující	oceňující	k2eAgFnSc2d1	oceňující
Havlovy	Havlův	k2eAgFnSc2d1	Havlova
zásluhy	zásluha	k1gFnSc2	zásluha
o	o	k7c4	o
svobodu	svoboda	k1gFnSc4	svoboda
a	a	k8xC	a
demokracii	demokracie	k1gFnSc4	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Pietním	pietní	k2eAgNnSc7d1	pietní
shromážděním	shromáždění	k1gNnSc7	shromáždění
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
aule	aula	k1gFnSc6	aula
Karolina	Karolinum	k1gNnSc2	Karolinum
uctila	uctít	k5eAaPmAgFnS	uctít
jeho	jeho	k3xOp3gFnSc4	jeho
památku	památka	k1gFnSc4	památka
i	i	k9	i
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
den	den	k1gInSc4	den
pohřbu	pohřeb	k1gInSc2	pohřeb
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
státní	státní	k2eAgInSc4d1	státní
smutek	smutek	k1gInSc4	smutek
také	také	k9	také
Slovensko	Slovensko	k1gNnSc4	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
je	být	k5eAaImIp3nS	být
pochován	pochován	k2eAgMnSc1d1	pochován
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
první	první	k4xOgFnSc7	první
ženou	žena	k1gFnSc7	žena
Olgou	Olga	k1gFnSc7	Olga
v	v	k7c6	v
rodinné	rodinný	k2eAgFnSc6d1	rodinná
hrobce	hrobka	k1gFnSc6	hrobka
na	na	k7c6	na
boční	boční	k2eAgFnSc6d1	boční
straně	strana	k1gFnSc6	strana
kaple	kaple	k1gFnSc2	kaple
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
na	na	k7c6	na
Vinohradském	vinohradský	k2eAgInSc6d1	vinohradský
hřbitově	hřbitov	k1gInSc6	hřbitov
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Literární	literární	k2eAgNnSc1d1	literární
dílo	dílo	k1gNnSc1	dílo
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
spadá	spadat	k5eAaPmIp3nS	spadat
do	do	k7c2	do
tří	tři	k4xCgInPc2	tři
žánrů	žánr	k1gInPc2	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
pochází	pocházet	k5eAaImIp3nS	pocházet
poezie	poezie	k1gFnSc1	poezie
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
básně	báseň	k1gFnPc1	báseň
psané	psaný	k2eAgFnPc1d1	psaná
zejména	zejména	k9	zejména
ve	v	k7c6	v
volném	volný	k2eAgInSc6d1	volný
verši	verš	k1gInSc6	verš
(	(	kIx(	(
<g/>
sbírky	sbírka	k1gFnSc2	sbírka
Čtyři	čtyři	k4xCgFnPc1	čtyři
rané	raný	k2eAgFnPc1d1	raná
básně	báseň	k1gFnPc1	báseň
[	[	kIx(	[
<g/>
1953	[number]	k4	1953
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
Záchvěvy	záchvěv	k1gInPc1	záchvěv
[	[	kIx(	[
<g/>
1954	[number]	k4	1954
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
První	první	k4xOgInPc1	první
úpisy	úpis	k1gInPc1	úpis
[	[	kIx(	[
<g/>
1955	[number]	k4	1955
<g />
.	.	kIx.	.
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
Prostory	prostor	k1gInPc1	prostor
a	a	k8xC	a
časy	čas	k1gInPc1	čas
[	[	kIx(	[
<g/>
1956	[number]	k4	1956
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
Na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
jara	jaro	k1gNnSc2	jaro
[	[	kIx(	[
<g/>
1956	[number]	k4	1956
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
strojopisné	strojopisný	k2eAgInPc1d1	strojopisný
kaligramy	kaligram	k1gInPc1	kaligram
–	–	k?	–
typogramy	typogram	k1gInPc4	typogram
(	(	kIx(	(
<g/>
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
Antikódy	Antikóda	k1gFnSc2	Antikóda
<g/>
:	:	kIx,	:
Antikódy	Antikóda	k1gFnSc2	Antikóda
I	I	kA	I
[	[	kIx(	[
<g/>
1964	[number]	k4	1964
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
Antikódy	Antikóda	k1gFnPc4	Antikóda
II	II	kA	II
[	[	kIx(	[
<g/>
1969	[number]	k4	1969
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
Antikódy	Antikód	k1gInPc1	Antikód
III	III	kA	III
a	a	k8xC	a
IV	IV	kA	IV
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
světovou	světový	k2eAgFnSc4d1	světová
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
scénu	scéna	k1gFnSc4	scéna
Havel	Havel	k1gMnSc1	Havel
pronikl	proniknout	k5eAaPmAgMnS	proniknout
svou	svůj	k3xOyFgFnSc7	svůj
dramatickou	dramatický	k2eAgFnSc7d1	dramatická
tvorbou	tvorba	k1gFnSc7	tvorba
<g/>
;	;	kIx,	;
první	první	k4xOgFnSc4	první
významnou	významný	k2eAgFnSc4d1	významná
divadelní	divadelní	k2eAgFnSc4d1	divadelní
hru	hra	k1gFnSc4	hra
Zahradní	zahradní	k2eAgFnSc4d1	zahradní
slavnost	slavnost	k1gFnSc4	slavnost
uvedl	uvést	k5eAaPmAgMnS	uvést
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgNnSc4d1	poslední
Odcházení	odcházení	k1gNnSc4	odcházení
napsal	napsat	k5eAaBmAgMnS	napsat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
autorem	autor	k1gMnSc7	autor
scénáře	scénář	k1gInSc2	scénář
a	a	k8xC	a
režisérem	režisér	k1gMnSc7	režisér
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
celovečerního	celovečerní	k2eAgInSc2d1	celovečerní
filmu	film	k1gInSc2	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
tvůrčí	tvůrčí	k2eAgInSc4d1	tvůrčí
život	život	k1gInSc4	život
se	se	k3xPyFc4	se
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
věnoval	věnovat	k5eAaImAgMnS	věnovat
esejistice	esejistika	k1gFnSc3	esejistika
<g/>
,	,	kIx,	,
zpočátku	zpočátku	k6eAd1	zpočátku
šlo	jít	k5eAaImAgNnS	jít
především	především	k9	především
o	o	k7c4	o
texty	text	k1gInPc4	text
o	o	k7c6	o
divadle	divadlo	k1gNnSc6	divadlo
a	a	k8xC	a
literatuře	literatura	k1gFnSc6	literatura
<g/>
,	,	kIx,	,
brzo	brzo	k6eAd1	brzo
(	(	kIx(	(
<g/>
kvůli	kvůli	k7c3	kvůli
totalitním	totalitní	k2eAgFnPc3d1	totalitní
společenským	společenský	k2eAgFnPc3d1	společenská
podmínkám	podmínka	k1gFnPc3	podmínka
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
vlastní	vlastní	k2eAgFnSc3d1	vlastní
zodpovědnosti	zodpovědnost	k1gFnSc3	zodpovědnost
<g/>
,	,	kIx,	,
poctivosti	poctivost	k1gFnSc3	poctivost
a	a	k8xC	a
snaze	snaha	k1gFnSc3	snaha
hledat	hledat	k5eAaImF	hledat
pravdu	pravda	k1gFnSc4	pravda
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
druhé	druhý	k4xOgFnSc6	druhý
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
textech	text	k1gInPc6	text
<g />
.	.	kIx.	.
</s>
<s>
zákonitě	zákonitě	k6eAd1	zákonitě
začaly	začít	k5eAaPmAgFnP	začít
objevovat	objevovat	k5eAaImF	objevovat
i	i	k9	i
úvahy	úvaha	k1gFnPc4	úvaha
nad	nad	k7c7	nad
společenskou	společenský	k2eAgFnSc7d1	společenská
situací	situace	k1gFnSc7	situace
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
normalizace	normalizace	k1gFnSc1	normalizace
<g/>
,	,	kIx,	,
Charta	charta	k1gFnSc1	charta
77	[number]	k4	77
<g/>
)	)	kIx)	)
politické	politický	k2eAgInPc1d1	politický
texty	text	k1gInPc1	text
převažují	převažovat	k5eAaImIp3nP	převažovat
<g/>
;	;	kIx,	;
patří	patřit	k5eAaImIp3nP	patřit
sem	sem	k6eAd1	sem
rovněž	rovněž	k9	rovněž
jeho	jeho	k3xOp3gFnSc1	jeho
epistolární	epistolární	k2eAgFnSc1d1	epistolární
tvorba	tvorba	k1gFnSc1	tvorba
<g/>
,	,	kIx,	,
literární	literární	k2eAgFnSc1d1	literární
a	a	k8xC	a
divadelní	divadelní	k2eAgFnSc1d1	divadelní
kritika	kritika	k1gFnSc1	kritika
<g/>
,	,	kIx,	,
projevy	projev	k1gInPc1	projev
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
příležitostné	příležitostný	k2eAgInPc1d1	příležitostný
texty	text	k1gInPc1	text
<g/>
.	.	kIx.	.
</s>
<s>
Havlovy	Havlův	k2eAgInPc1d1	Havlův
typogramy	typogram	k1gInPc1	typogram
ze	z	k7c2	z
sbírky	sbírka	k1gFnSc2	sbírka
Antikódy	Antikóda	k1gMnSc2	Antikóda
formálně	formálně	k6eAd1	formálně
navazovaly	navazovat	k5eAaImAgFnP	navazovat
na	na	k7c4	na
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
hnutí	hnutí	k1gNnSc4	hnutí
konkrétní	konkrétní	k2eAgFnSc2d1	konkrétní
či	či	k8xC	či
experimentální	experimentální	k2eAgFnSc2d1	experimentální
poezie	poezie	k1gFnSc2	poezie
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
z	z	k7c2	z
českých	český	k2eAgMnPc2d1	český
autorů	autor	k1gMnPc2	autor
mají	mít	k5eAaImIp3nP	mít
blízko	blízko	k6eAd1	blízko
k	k	k7c3	k
poezii	poezie	k1gFnSc3	poezie
Jiřího	Jiří	k1gMnSc2	Jiří
Koláře	Kolář	k1gMnSc2	Kolář
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
je	být	k5eAaImIp3nS	být
kniha	kniha	k1gFnSc1	kniha
věnována	věnovat	k5eAaPmNgFnS	věnovat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
ostatních	ostatní	k2eAgMnPc2d1	ostatní
autorů	autor	k1gMnPc2	autor
tohoto	tento	k3xDgInSc2	tento
směru	směr	k1gInSc2	směr
se	se	k3xPyFc4	se
Havel	Havel	k1gMnSc1	Havel
odlišoval	odlišovat	k5eAaImAgMnS	odlišovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
experiment	experiment	k1gInSc1	experiment
neprotestoval	protestovat	k5eNaBmAgInS	protestovat
jen	jen	k9	jen
proti	proti	k7c3	proti
konvencím	konvence	k1gFnPc3	konvence
dosavadní	dosavadní	k2eAgFnSc2d1	dosavadní
poetiky	poetika	k1gFnSc2	poetika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
i	i	k9	i
"	"	kIx"	"
<g/>
protestem	protest	k1gInSc7	protest
sociálně	sociálně	k6eAd1	sociálně
kritickým	kritický	k2eAgInSc7d1	kritický
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
i	i	k9	i
jakousi	jakýsi	k3yIgFnSc7	jakýsi
artikulací	artikulace	k1gFnSc7	artikulace
absurdity	absurdita	k1gFnSc2	absurdita
tehdejších	tehdejší	k2eAgInPc2d1	tehdejší
politických	politický	k2eAgInPc2d1	politický
poměrů	poměr	k1gInPc2	poměr
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Některá	některý	k3yIgNnPc1	některý
čísla	číslo	k1gNnPc1	číslo
sbírky	sbírka	k1gFnSc2	sbírka
jsou	být	k5eAaImIp3nP	být
spíše	spíše	k9	spíše
obrazy	obraz	k1gInPc4	obraz
sestavené	sestavený	k2eAgInPc4d1	sestavený
ze	z	k7c2	z
znaků	znak	k1gInPc2	znak
psacího	psací	k2eAgInSc2d1	psací
stroje	stroj	k1gInSc2	stroj
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
kříž	kříž	k1gInSc4	kříž
vytvořený	vytvořený	k2eAgInSc4d1	vytvořený
z	z	k7c2	z
paragrafů	paragraf	k1gInPc2	paragraf
věnovaný	věnovaný	k2eAgInSc4d1	věnovaný
Bedřichu	Bedřich	k1gMnSc6	Bedřich
Fučíkovi	Fučík	k1gMnSc6	Fučík
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
však	však	k9	však
autor	autor	k1gMnSc1	autor
pracuje	pracovat	k5eAaImIp3nS	pracovat
se	s	k7c7	s
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
grafickými	grafický	k2eAgInPc7d1	grafický
i	i	k8xC	i
gramatickými	gramatický	k2eAgInPc7d1	gramatický
prostředky	prostředek	k1gInPc7	prostředek
uvádí	uvádět	k5eAaImIp3nS	uvádět
do	do	k7c2	do
nečekaných	čekaný	k2eNgFnPc2d1	nečekaná
souvislostí	souvislost	k1gFnPc2	souvislost
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
tím	ten	k3xDgNnSc7	ten
daří	dařit	k5eAaImIp3nS	dařit
demaskovat	demaskovat	k5eAaBmF	demaskovat
lživost	lživost	k1gFnSc4	lživost
dobových	dobový	k2eAgFnPc2d1	dobová
politických	politický	k2eAgFnPc2d1	politická
frází	fráze	k1gFnPc2	fráze
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
zneužívání	zneužívání	k1gNnSc1	zneužívání
nacionalistického	nacionalistický	k2eAgInSc2d1	nacionalistický
slovníku	slovník	k1gInSc2	slovník
v	v	k7c6	v
básni	báseň	k1gFnSc6	báseň
Vzor	vzor	k1gInSc1	vzor
lid	lid	k1gInSc1	lid
<g/>
:	:	kIx,	:
Havlovy	Havlův	k2eAgFnSc2d1	Havlova
divadelní	divadelní	k2eAgFnSc2d1	divadelní
hry	hra	k1gFnSc2	hra
jsou	být	k5eAaImIp3nP	být
ovlivněny	ovlivnit	k5eAaPmNgInP	ovlivnit
především	především	k6eAd1	především
tradicí	tradice	k1gFnSc7	tradice
absurdního	absurdní	k2eAgNnSc2d1	absurdní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
;	;	kIx,	;
mimochodem	mimochodem	k9	mimochodem
Samuel	Samuel	k1gMnSc1	Samuel
Beckett	Beckett	k1gMnSc1	Beckett
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
jejích	její	k3xOp3gMnPc2	její
zakladatelů	zakladatel	k1gMnPc2	zakladatel
<g/>
,	,	kIx,	,
uvězněnému	uvězněný	k2eAgMnSc3d1	uvězněný
Havlovi	Havel	k1gMnSc3	Havel
věnoval	věnovat	k5eAaImAgMnS	věnovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
hru	hra	k1gFnSc4	hra
Katastrofa	katastrofa	k1gFnSc1	katastrofa
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
ovšem	ovšem	k9	ovšem
jediný	jediný	k2eAgInSc1d1	jediný
inspirační	inspirační	k2eAgInSc1d1	inspirační
zdroj	zdroj	k1gInSc1	zdroj
<g/>
;	;	kIx,	;
například	například	k6eAd1	například
k	k	k7c3	k
Čechovovi	Čechov	k1gMnSc3	Čechov
se	se	k3xPyFc4	se
Havel	Havel	k1gMnSc1	Havel
sám	sám	k3xTgMnSc1	sám
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
Odcházení	odcházení	k1gNnPc2	odcházení
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
citací	citace	k1gFnSc7	citace
motivu	motiv	k1gInSc2	motiv
z	z	k7c2	z
Višňového	višňový	k2eAgInSc2d1	višňový
sadu	sad	k1gInSc2	sad
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
první	první	k4xOgFnSc1	první
velká	velký	k2eAgFnSc1d1	velká
Havlova	Havlův	k2eAgFnSc1d1	Havlova
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
Zahradní	zahradní	k2eAgFnSc1d1	zahradní
slavnost	slavnost	k1gFnSc1	slavnost
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
klíčová	klíčový	k2eAgNnPc4d1	klíčové
témata	téma	k1gNnPc4	téma
jeho	jeho	k3xOp3gFnPc2	jeho
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
problémy	problém	k1gInPc4	problém
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
byrokracie	byrokracie	k1gFnSc2	byrokracie
a	a	k8xC	a
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
hrdina	hrdina	k1gMnSc1	hrdina
Hugo	Hugo	k1gMnSc1	Hugo
Pludek	Pludka	k1gFnPc2	Pludka
příslušností	příslušnost	k1gFnPc2	příslušnost
k	k	k7c3	k
absurdní	absurdní	k2eAgFnSc3d1	absurdní
byrokratické	byrokratický	k2eAgFnSc3d1	byrokratická
organizaci	organizace	k1gFnSc3	organizace
a	a	k8xC	a
přijetím	přijetí	k1gNnSc7	přijetí
jejího	její	k3xOp3gInSc2	její
jazyka	jazyk	k1gInSc2	jazyk
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
vlastní	vlastní	k2eAgFnSc4d1	vlastní
identitu	identita	k1gFnSc4	identita
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
Vyrozumění	vyrozumění	k1gNnSc2	vyrozumění
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
tématem	téma	k1gNnSc7	téma
odcizení	odcizení	k1gNnSc2	odcizení
jazyka	jazyk	k1gMnSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Umělé	umělý	k2eAgInPc4d1	umělý
jazyky	jazyk	k1gInPc4	jazyk
ptydepe	ptydepat	k5eAaPmIp3nS	ptydepat
a	a	k8xC	a
chorukor	chorukor	k1gInSc1	chorukor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
zpřesnit	zpřesnit	k5eAaPmF	zpřesnit
a	a	k8xC	a
usnadnit	usnadnit	k5eAaPmF	usnadnit
komunikaci	komunikace	k1gFnSc4	komunikace
mezi	mezi	k7c4	mezi
byrokraty	byrokrat	k1gMnPc4	byrokrat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
jako	jako	k9	jako
její	její	k3xOp3gInPc1	její
nepřekonatelná	překonatelný	k2eNgFnSc1d1	nepřekonatelná
překážka	překážka	k1gFnSc1	překážka
<g/>
.	.	kIx.	.
</s>
<s>
Byrokratickou	byrokratický	k2eAgFnSc4d1	byrokratická
aroganci	arogance	k1gFnSc4	arogance
a	a	k8xC	a
ničení	ničení	k1gNnSc4	ničení
přirozeného	přirozený	k2eAgInSc2d1	přirozený
lidského	lidský	k2eAgInSc2d1	lidský
světa	svět	k1gInSc2	svět
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
například	například	k6eAd1	například
hra	hra	k1gFnSc1	hra
Asanace	asanace	k1gFnSc2	asanace
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
Havlovy	Havlův	k2eAgFnPc1d1	Havlova
divadelní	divadelní	k2eAgFnPc1d1	divadelní
hry	hra	k1gFnPc1	hra
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
silný	silný	k2eAgInSc4d1	silný
autobiografický	autobiografický	k2eAgInSc4d1	autobiografický
prvek	prvek	k1gInSc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Trojici	trojice	k1gFnSc4	trojice
her	hra	k1gFnPc2	hra
Audience	audience	k1gFnSc1	audience
<g/>
,	,	kIx,	,
Vernisáž	vernisáž	k1gFnSc1	vernisáž
(	(	kIx(	(
<g/>
obě	dva	k4xCgFnPc1	dva
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
a	a	k8xC	a
Protest	protest	k1gInSc1	protest
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
jsou	být	k5eAaImIp3nP	být
asi	asi	k9	asi
nejpřístupnější	přístupný	k2eAgNnPc1d3	nejpřístupnější
z	z	k7c2	z
celého	celý	k2eAgNnSc2d1	celé
Havlova	Havlův	k2eAgNnSc2d1	Havlovo
dramatického	dramatický	k2eAgNnSc2d1	dramatické
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
spojuje	spojovat	k5eAaImIp3nS	spojovat
postava	postava	k1gFnSc1	postava
disidenta	disident	k1gMnSc2	disident
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Vaňka	Vaněk	k1gMnSc2	Vaněk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
pivovaru	pivovar	k1gInSc6	pivovar
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
sám	sám	k3xTgMnSc1	sám
Havel	Havel	k1gMnSc1	Havel
<g/>
.	.	kIx.	.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Vaněk	Vaněk	k1gMnSc1	Vaněk
přitom	přitom	k6eAd1	přitom
hraje	hrát	k5eAaImIp3nS	hrát
spíše	spíše	k9	spíše
"	"	kIx"	"
<g/>
roli	role	k1gFnSc4	role
dramatického	dramatický	k2eAgInSc2d1	dramatický
principu	princip	k1gInSc2	princip
než	než	k8xS	než
postavy	postava	k1gFnSc2	postava
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
sám	sám	k3xTgInSc1	sám
toho	ten	k3xDgMnSc4	ten
dělá	dělat	k5eAaImIp3nS	dělat
a	a	k8xC	a
říká	říkat	k5eAaImIp3nS	říkat
málo	málo	k1gNnSc1	málo
<g/>
,	,	kIx,	,
děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
točí	točit	k5eAaImIp3nS	točit
kolem	kolem	k7c2	kolem
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
na	na	k7c4	na
Vaňkův	Vaňkův	k2eAgInSc4d1	Vaňkův
pravdivý	pravdivý	k2eAgInSc4d1	pravdivý
postoj	postoj	k1gInSc4	postoj
k	k	k7c3	k
životu	život	k1gInSc3	život
reagují	reagovat	k5eAaBmIp3nP	reagovat
druzí	druhý	k4xOgMnPc1	druhý
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
tak	tak	k6eAd1	tak
mohou	moct	k5eAaImIp3nP	moct
demonstrovat	demonstrovat	k5eAaBmF	demonstrovat
svoji	svůj	k3xOyFgFnSc4	svůj
zbabělost	zbabělost	k1gFnSc4	zbabělost
<g/>
,	,	kIx,	,
pokrytectví	pokrytectví	k1gNnSc4	pokrytectví
a	a	k8xC	a
kariérismus	kariérismus	k1gInSc4	kariérismus
<g/>
.	.	kIx.	.
</s>
<s>
Largo	largo	k6eAd1	largo
desolato	desolato	k6eAd1	desolato
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
studií	studie	k1gFnSc7	studie
strachu	strach	k1gInSc2	strach
pronásledovaného	pronásledovaný	k2eAgMnSc2d1	pronásledovaný
disidentského	disidentský	k2eAgMnSc2d1	disidentský
spisovatele	spisovatel	k1gMnSc2	spisovatel
o	o	k7c4	o
vlastní	vlastní	k2eAgFnSc4d1	vlastní
identitu	identita	k1gFnSc4	identita
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
postava	postava	k1gFnSc1	postava
Odcházení	odcházení	k1gNnSc2	odcházení
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
Havel	Havel	k1gMnSc1	Havel
napsal	napsat	k5eAaBmAgMnS	napsat
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
své	svůj	k3xOyFgFnSc2	svůj
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
funkce	funkce	k1gFnSc2	funkce
<g/>
,	,	kIx,	,
prožívá	prožívat	k5eAaImIp3nS	prožívat
situaci	situace	k1gFnSc4	situace
po	po	k7c6	po
ztrátě	ztráta	k1gFnSc6	ztráta
politické	politický	k2eAgFnSc2d1	politická
funkce	funkce	k1gFnSc2	funkce
a	a	k8xC	a
hra	hra	k1gFnSc1	hra
se	se	k3xPyFc4	se
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
problémů	problém	k1gInPc2	problém
korupce	korupce	k1gFnSc2	korupce
nebo	nebo	k8xC	nebo
bulvárního	bulvární	k2eAgInSc2d1	bulvární
tisku	tisk	k1gInSc2	tisk
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
uvedení	uvedení	k1gNnSc6	uvedení
zfilmované	zfilmovaný	k2eAgFnSc2d1	zfilmovaná
verze	verze	k1gFnSc2	verze
Odcházení	odcházení	k1gNnSc4	odcházení
(	(	kIx(	(
<g/>
kterou	který	k3yQgFnSc4	který
Havel	Havel	k1gMnSc1	Havel
i	i	k9	i
režíroval	režírovat	k5eAaImAgMnS	režírovat
a	a	k8xC	a
za	za	k7c4	za
niž	jenž	k3xRgFnSc4	jenž
byl	být	k5eAaImAgMnS	být
posmrtně	posmrtně	k6eAd1	posmrtně
oceněn	ocenit	k5eAaPmNgMnS	ocenit
Českým	český	k2eAgMnSc7d1	český
lvem	lev	k1gMnSc7	lev
pro	pro	k7c4	pro
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
scénář	scénář	k1gInSc4	scénář
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Havel	Havel	k1gMnSc1	Havel
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
chystá	chystat	k5eAaImIp3nS	chystat
napsat	napsat	k5eAaPmF	napsat
poslední	poslední	k2eAgFnSc4d1	poslední
divadelní	divadelní	k2eAgFnSc4d1	divadelní
hru	hra	k1gFnSc4	hra
Sanatorium	sanatorium	k1gNnSc4	sanatorium
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
rozepsal	rozepsat	k5eAaPmAgMnS	rozepsat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k6eAd1	už
nestihl	stihnout	k5eNaPmAgMnS	stihnout
dokončit	dokončit	k5eAaPmF	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
začal	začít	k5eAaPmAgMnS	začít
psát	psát	k5eAaImF	psát
literárně	literárně	k6eAd1	literárně
a	a	k8xC	a
divadelně	divadelně	k6eAd1	divadelně
kritické	kritický	k2eAgInPc4d1	kritický
i	i	k8xC	i
úvahové	úvahový	k2eAgInPc4d1	úvahový
texty	text	k1gInPc4	text
již	již	k6eAd1	již
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
známými	známý	k2eAgMnPc7d1	známý
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInPc1	jeho
projevy	projev	k1gInPc1	projev
a	a	k8xC	a
eseje	esej	k1gFnPc1	esej
staly	stát	k5eAaPmAgFnP	stát
v	v	k7c6	v
době	doba	k1gFnSc6	doba
kolem	kolem	k7c2	kolem
Pražského	pražský	k2eAgNnSc2d1	Pražské
jara	jaro	k1gNnSc2	jaro
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
část	část	k1gFnSc1	část
jeho	on	k3xPp3gNnSc2	on
esejistického	esejistický	k2eAgNnSc2d1	esejistické
a	a	k8xC	a
epistolárního	epistolární	k2eAgNnSc2d1	epistolární
díla	dílo	k1gNnSc2	dílo
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
normalizace	normalizace	k1gFnSc2	normalizace
<g/>
:	:	kIx,	:
Dopis	dopis	k1gInSc1	dopis
Gustávu	Gustáv	k1gMnSc3	Gustáv
Husákovi	Husák	k1gMnSc3	Husák
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Moc	moc	k6eAd1	moc
bezmocných	bezmocný	k2eAgMnPc2d1	bezmocný
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dopisy	dopis	k1gInPc1	dopis
Olze	Olga	k1gFnSc3	Olga
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Slovo	slovo	k1gNnSc1	slovo
o	o	k7c6	o
slovu	slovo	k1gNnSc6	slovo
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
a	a	k8xC	a
mnohé	mnohý	k2eAgFnPc4d1	mnohá
další	další	k2eAgNnPc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Nepřestal	přestat	k5eNaPmAgMnS	přestat
však	však	k9	však
psát	psát	k5eAaImF	psát
esejistickou	esejistický	k2eAgFnSc4d1	esejistická
literaturu	literatura	k1gFnSc4	literatura
ani	ani	k8xC	ani
v	v	k7c6	v
době	doba	k1gFnSc6	doba
prezidentství	prezidentství	k1gNnSc2	prezidentství
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Letní	letní	k2eAgNnSc1d1	letní
přemítání	přemítání	k1gNnSc1	přemítání
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
Havel	Havel	k1gMnSc1	Havel
nechtěl	chtít	k5eNaImAgMnS	chtít
být	být	k5eAaImF	být
filozof	filozof	k1gMnSc1	filozof
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
výklad	výklad	k1gInSc1	výklad
nepostupoval	postupovat	k5eNaImAgInS	postupovat
rigorózní	rigorózní	k2eAgFnSc7d1	rigorózní
cestou	cesta	k1gFnSc7	cesta
<g/>
,	,	kIx,	,
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
textech	text	k1gInPc6	text
se	se	k3xPyFc4	se
dotýkal	dotýkat	k5eAaImAgMnS	dotýkat
řady	řada	k1gFnSc2	řada
zásadních	zásadní	k2eAgNnPc2d1	zásadní
filozofických	filozofický	k2eAgNnPc2d1	filozofické
témat	téma	k1gNnPc2	téma
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
svoboda	svoboda	k1gFnSc1	svoboda
<g/>
,	,	kIx,	,
moc	moc	k1gFnSc1	moc
<g/>
,	,	kIx,	,
morálka	morálka	k1gFnSc1	morálka
či	či	k8xC	či
transcendence	transcendence	k1gFnSc1	transcendence
<g/>
,	,	kIx,	,
jakkoliv	jakkoliv	k6eAd1	jakkoliv
tak	tak	k6eAd1	tak
činil	činit	k5eAaImAgInS	činit
spíše	spíše	k9	spíše
eklektickým	eklektický	k2eAgInSc7d1	eklektický
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
snahy	snaha	k1gFnSc2	snaha
o	o	k7c4	o
vytvoření	vytvoření	k1gNnSc4	vytvoření
samostatné	samostatný	k2eAgFnSc2d1	samostatná
školy	škola	k1gFnSc2	škola
či	či	k8xC	či
směru	směr	k1gInSc2	směr
<g/>
.	.	kIx.	.
</s>
<s>
Navazoval	navazovat	k5eAaImAgMnS	navazovat
na	na	k7c4	na
české	český	k2eAgMnPc4d1	český
myslitele	myslitel	k1gMnPc4	myslitel
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigue	k1gFnPc2	Garrigue
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
,	,	kIx,	,
Emanuel	Emanuel	k1gMnSc1	Emanuel
Rádl	Rádl	k1gMnSc1	Rádl
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Šafařík	Šafařík	k1gMnSc1	Šafařík
a	a	k8xC	a
především	především	k6eAd1	především
Jan	Jan	k1gMnSc1	Jan
Patočka	Patočka	k1gMnSc1	Patočka
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
něhož	jenž	k3xRgMnSc4	jenž
přijal	přijmout	k5eAaPmAgInS	přijmout
tradici	tradice	k1gFnSc3	tradice
fenomenologické	fenomenologický	k2eAgFnSc2d1	fenomenologická
filozofie	filozofie	k1gFnSc2	filozofie
(	(	kIx(	(
<g/>
Edmund	Edmund	k1gMnSc1	Edmund
Husserl	Husserl	k1gMnSc1	Husserl
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Heidegger	Heidegger	k1gMnSc1	Heidegger
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tzv.	tzv.	kA	tzv.
Kampademie	Kampademie	k1gFnSc2	Kampademie
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
s	s	k7c7	s
filozofy	filozof	k1gMnPc7	filozof
Zdeňkem	Zdeněk	k1gMnSc7	Zdeněk
Neubauerem	Neubauer	k1gMnSc7	Neubauer
<g/>
,	,	kIx,	,
Radimem	Radim	k1gMnSc7	Radim
Paloušem	Palouš	k1gMnSc7	Palouš
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gInSc1	jeho
"	"	kIx"	"
<g/>
myšlení	myšlení	k1gNnSc1	myšlení
o	o	k7c6	o
světě	svět	k1gInSc6	svět
<g/>
"	"	kIx"	"
bylo	být	k5eAaImAgNnS	být
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
i	i	k9	i
ranými	raný	k2eAgInPc7d1	raný
texty	text	k1gInPc7	text
Václava	Václav	k1gMnSc2	Václav
Bělohradského	Bělohradský	k1gMnSc2	Bělohradský
(	(	kIx(	(
<g/>
Krize	krize	k1gFnSc1	krize
eschatologie	eschatologie	k1gFnSc2	eschatologie
neosobnosti	neosobnost	k1gFnSc2	neosobnost
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
kritické	kritický	k2eAgNnSc4d1	kritické
zhodnocení	zhodnocení	k1gNnSc4	zhodnocení
Havlovy	Havlův	k2eAgFnSc2d1	Havlova
(	(	kIx(	(
<g/>
politické	politický	k2eAgFnSc2d1	politická
<g/>
)	)	kIx)	)
filozofie	filozofie	k1gFnSc2	filozofie
se	se	k3xPyFc4	se
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
v	v	k7c6	v
domácím	domácí	k2eAgNnSc6d1	domácí
prostředí	prostředí	k1gNnSc6	prostředí
zejména	zejména	k9	zejména
filozof	filozof	k1gMnSc1	filozof
Petr	Petr	k1gMnSc1	Petr
Rezek	Rezek	k1gMnSc1	Rezek
(	(	kIx(	(
<g/>
Filosofie	filosofie	k1gFnSc1	filosofie
a	a	k8xC	a
politika	politika	k1gFnSc1	politika
kýče	kýč	k1gInSc2	kýč
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ze	z	k7c2	z
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
autorů	autor	k1gMnPc2	autor
pak	pak	k8xC	pak
např.	např.	kA	např.
neokonzervativec	neokonzervativec	k1gMnSc1	neokonzervativec
Aviezer	Aviezer	k1gMnSc1	Aviezer
Tucker	Tucker	k1gMnSc1	Tucker
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Philosophy	Philosopha	k1gFnSc2	Philosopha
and	and	k?	and
Politics	Politics	k1gInSc1	Politics
of	of	k?	of
Czech	Czech	k1gInSc4	Czech
Dissidence	Dissidence	k1gFnSc2	Dissidence
from	from	k1gFnPc3	from
Patocka	Patocko	k1gNnSc2	Patocko
to	ten	k3xDgNnSc1	ten
Havel	Havel	k1gMnSc1	Havel
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
za	za	k7c2	za
života	život	k1gInSc2	život
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
amerických	americký	k2eAgFnPc2d1	americká
prezidentských	prezidentský	k2eAgFnPc2d1	prezidentská
knihoven	knihovna	k1gFnPc2	knihovna
zřízena	zřízen	k2eAgFnSc1d1	zřízena
Knihovna	knihovna	k1gFnSc1	knihovna
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
den	den	k1gInSc4	den
jeho	jeho	k3xOp3gInSc2	jeho
pohřbu	pohřeb	k1gInSc2	pohřeb
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Gdaňsku	Gdaňsk	k1gInSc6	Gdaňsk
otevřena	otevřen	k2eAgFnSc1d1	otevřena
třída	třída	k1gFnSc1	třída
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
(	(	kIx(	(
<g/>
aleja	alej	k2eAgFnSc1d1	alej
Vaclava	Vaclava	k1gFnSc1	Vaclava
Havla	Havel	k1gMnSc2	Havel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2012	[number]	k4	2012
se	se	k3xPyFc4	se
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
Na	na	k7c6	na
Valech	val	k1gInPc6	val
v	v	k7c6	v
Poděbradech	Poděbrady	k1gInPc6	Poděbrady
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2012	[number]	k4	2012
vláda	vláda	k1gFnSc1	vláda
odsouhlasila	odsouhlasit	k5eAaPmAgFnS	odsouhlasit
přejmenování	přejmenování	k1gNnSc3	přejmenování
pražského	pražský	k2eAgNnSc2d1	Pražské
ruzyňského	ruzyňský	k2eAgNnSc2d1	ruzyňské
letiště	letiště	k1gNnSc2	letiště
na	na	k7c4	na
Letiště	letiště	k1gNnSc4	letiště
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
v	v	k7c4	v
den	den	k1gInSc4	den
jeho	on	k3xPp3gNnSc2	on
nedožitých	dožitý	k2eNgNnPc2d1	nedožité
76	[number]	k4	76
<g/>
.	.	kIx.	.
narozenin	narozeniny	k1gFnPc2	narozeniny
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
popud	popud	k1gInSc4	popud
uměleckého	umělecký	k2eAgMnSc2d1	umělecký
ředitele	ředitel	k1gMnSc2	ředitel
Divadla	divadlo	k1gNnSc2	divadlo
Husa	Hus	k1gMnSc2	Hus
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Morávka	Morávek	k1gMnSc2	Morávek
po	po	k7c6	po
Václavu	Václav	k1gMnSc6	Václav
Havlovi	Havel	k1gMnSc6	Havel
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
ulička	ulička	k1gFnSc1	ulička
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
jdoucí	jdoucí	k2eAgInPc1d1	jdoucí
od	od	k7c2	od
tohoto	tento	k3xDgNnSc2	tento
divadla	divadlo	k1gNnSc2	divadlo
k	k	k7c3	k
Petrovu	Petrov	k1gInSc3	Petrov
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
starou	starý	k2eAgFnSc4d1	stará
uličku	ulička	k1gFnSc4	ulička
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
však	však	k9	však
dosud	dosud	k6eAd1	dosud
neměla	mít	k5eNaImAgFnS	mít
žádné	žádný	k3yNgNnSc4	žádný
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
Havlových	Havlových	k2eAgInPc2d1	Havlových
nedožitých	dožitý	k2eNgInPc2d1	dožitý
80	[number]	k4	80
<g/>
.	.	kIx.	.
narozenin	narozeniny	k1gFnPc2	narozeniny
<g/>
,	,	kIx,	,
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
pražské	pražský	k2eAgNnSc1d1	Pražské
zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
schválilo	schválit	k5eAaPmAgNnS	schválit
přejmenování	přejmenování	k1gNnSc3	přejmenování
piazzety	piazzeta	k1gFnSc2	piazzeta
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Trutnově	Trutnov	k1gInSc6	Trutnov
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2016	[number]	k4	2016
přejmenována	přejmenován	k2eAgFnSc1d1	přejmenována
Revoluční	revoluční	k2eAgFnSc1d1	revoluční
ulice	ulice	k1gFnSc1	ulice
podél	podél	k7c2	podél
řeky	řeka	k1gFnSc2	řeka
Úpy	Úpa	k1gFnSc2	Úpa
na	na	k7c4	na
nábřeží	nábřeží	k1gNnSc4	nábřeží
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2016	[number]	k4	2016
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
kyjevská	kyjevský	k2eAgFnSc1d1	Kyjevská
městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
třídu	třída	k1gFnSc4	třída
Ivana	Ivan	k1gMnSc2	Ivan
Lepseho	Lepse	k1gMnSc2	Lepse
na	na	k7c4	na
třídu	třída	k1gFnSc4	třída
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2017	[number]	k4	2017
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
Štrasburku	Štrasburk	k1gInSc6	Štrasburk
slavnostně	slavnostně	k6eAd1	slavnostně
otevřena	otevřen	k2eAgFnSc1d1	otevřena
nová	nový	k2eAgFnSc1d1	nová
budova	budova	k1gFnSc1	budova
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
a	a	k8xC	a
úřad	úřad	k1gInSc4	úřad
Evropského	evropský	k2eAgMnSc2d1	evropský
ombudsmana	ombudsman	k1gMnSc2	ombudsman
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nese	nést	k5eAaImIp3nS	nést
jméno	jméno	k1gNnSc4	jméno
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
vchodem	vchod	k1gInSc7	vchod
byla	být	k5eAaImAgFnS	být
zároveň	zároveň	k6eAd1	zároveň
odhalena	odhalit	k5eAaPmNgFnS	odhalit
jeho	jeho	k3xOp3gFnSc1	jeho
bronzová	bronzový	k2eAgFnSc1d1	bronzová
busta	busta	k1gFnSc1	busta
od	od	k7c2	od
sochařky	sochařka	k1gFnSc2	sochařka
Marie	Maria	k1gFnSc2	Maria
Šeborové	Šeborová	k1gFnSc2	Šeborová
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Lavička	Lavička	k1gMnSc1	Lavička
Václava	Václav	k1gMnSc4	Václav
Havla	Havel	k1gMnSc4	Havel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
několika	několik	k4yIc6	několik
místech	místo	k1gNnPc6	místo
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
i	i	k8xC	i
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
vznikají	vznikat	k5eAaImIp3nP	vznikat
Lavičky	lavička	k1gFnPc4	lavička
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
dvě	dva	k4xCgFnPc4	dva
židle	židle	k1gFnPc4	židle
(	(	kIx(	(
<g/>
křesílka	křesílko	k1gNnSc2	křesílko
<g/>
)	)	kIx)	)
pevně	pevně	k6eAd1	pevně
spojené	spojený	k2eAgInPc4d1	spojený
se	s	k7c7	s
stolečkem	stoleček	k1gInSc7	stoleček
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
středem	střed	k1gInSc7	střed
prorůstá	prorůstat	k5eAaImIp3nS	prorůstat
lípa	lípa	k1gFnSc1	lípa
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
návrhu	návrh	k1gInSc2	návrh
je	být	k5eAaImIp3nS	být
Bořek	Bořek	k1gMnSc1	Bořek
Šípek	Šípek	k1gMnSc1	Šípek
<g/>
,	,	kIx,	,
cena	cena	k1gFnSc1	cena
lavičky	lavička	k1gFnSc2	lavička
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
350	[number]	k4	350
000	[number]	k4	000
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
díla	dílo	k1gNnPc1	dílo
se	se	k3xPyFc4	se
v	v	k7c6	v
několika	několik	k4yIc6	několik
městech	město	k1gNnPc6	město
stala	stát	k5eAaPmAgFnS	stát
terčem	terč	k1gInSc7	terč
vandalismu	vandalismus	k1gInSc2	vandalismus
<g/>
.	.	kIx.	.
</s>
<s>
Umístění	umístění	k1gNnSc1	umístění
laviček	lavička	k1gFnPc2	lavička
<g/>
:	:	kIx,	:
Washington	Washington	k1gInSc1	Washington
<g/>
,	,	kIx,	,
D.C.	D.C.	k1gFnSc1	D.C.
Dublin	Dublin	k1gInSc1	Dublin
Barcelona	Barcelona	k1gFnSc1	Barcelona
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
Maltézské	maltézský	k2eAgNnSc1d1	Maltézské
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
)	)	kIx)	)
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
(	(	kIx(	(
<g/>
areál	areál	k1gInSc1	areál
Jihočeské	jihočeský	k2eAgFnSc2d1	Jihočeská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
)	)	kIx)	)
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
Benátky	Benátky	k1gFnPc1	Benátky
Plzeň	Plzeň	k1gFnSc4	Plzeň
Nový	nový	k2eAgInSc1d1	nový
Bor	bor	k1gInSc1	bor
Pardubice	Pardubice	k1gInPc1	Pardubice
(	(	kIx(	(
<g/>
areál	areál	k1gInSc1	areál
Univerzity	univerzita	k1gFnSc2	univerzita
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
)	)	kIx)	)
Liberec	Liberec	k1gInSc1	Liberec
(	(	kIx(	(
<g/>
před	před	k7c7	před
Krajskou	krajský	k2eAgFnSc7d1	krajská
vědeckou	vědecký	k2eAgFnSc7d1	vědecká
knihovnou	knihovna	k1gFnSc7	knihovna
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
(	(	kIx(	(
<g/>
kampus	kampus	k1gInSc1	kampus
Univerzity	univerzita	k1gFnSc2	univerzita
Jana	Jan	k1gMnSc2	Jan
Evangelisty	evangelista	k1gMnSc2	evangelista
Purkyně	Purkyně	k1gFnSc1	Purkyně
v	v	k7c6	v
Ústí	ústí	k1gNnSc6	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
)	)	kIx)	)
Ostrava	Ostrava	k1gFnSc1	Ostrava
(	(	kIx(	(
<g/>
před	před	k7c7	před
vchodem	vchod	k1gInSc7	vchod
věznice	věznice	k1gFnSc2	věznice
Heřmanice	Heřmanice	k1gFnSc2	Heřmanice
<g/>
)	)	kIx)	)
Oxford	Oxford	k1gInSc1	Oxford
Dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2014	[number]	k4	2014
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
amerického	americký	k2eAgInSc2d1	americký
Kongresu	kongres	k1gInSc2	kongres
k	k	k7c3	k
25	[number]	k4	25
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
pádu	pád	k1gInSc2	pád
železné	železný	k2eAgFnSc2d1	železná
opony	opona	k1gFnSc2	opona
odhalena	odhalen	k2eAgFnSc1d1	odhalena
Havlova	Havlův	k2eAgFnSc1d1	Havlova
busta	busta	k1gFnSc1	busta
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
českoamerický	českoamerický	k2eAgMnSc1d1	českoamerický
sochař	sochař	k1gMnSc1	sochař
Lubomír	Lubomír	k1gMnSc1	Lubomír
Janečka	Janečka	k1gMnSc1	Janečka
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
po	po	k7c6	po
Winstonu	Winston	k1gInSc2	Winston
Churchillovi	Churchill	k1gMnSc3	Churchill
<g/>
,	,	kIx,	,
Raoulu	Raoul	k1gMnSc3	Raoul
Wallenbergovi	Wallenberg	k1gMnSc3	Wallenberg
a	a	k8xC	a
Lájosi	Lájos	k1gMnSc3	Lájos
Kossuthovi	Kossuth	k1gMnSc3	Kossuth
čtvrtým	čtvrtý	k4xOgMnSc7	čtvrtý
Evropanem	Evropan	k1gMnSc7	Evropan
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
se	se	k3xPyFc4	se
této	tento	k3xDgFnSc3	tento
cti	čest	k1gFnSc3	čest
dostalo	dostat	k5eAaPmAgNnS	dostat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
nedožitých	dožitý	k2eNgInPc2d1	dožitý
80	[number]	k4	80
<g/>
.	.	kIx.	.
narozenin	narozeniny	k1gFnPc2	narozeniny
a	a	k8xC	a
20	[number]	k4	20
let	léto	k1gNnPc2	léto
od	od	k7c2	od
jeho	jeho	k3xOp3gFnSc2	jeho
státní	státní	k2eAgFnSc2d1	státní
návštěvy	návštěva	k1gFnSc2	návštěva
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2016	[number]	k4	2016
odhalena	odhalen	k2eAgFnSc1d1	odhalena
Havlova	Havlův	k2eAgFnSc1d1	Havlova
busta	busta	k1gFnSc1	busta
v	v	k7c6	v
předsálí	předsálí	k1gNnSc6	předsálí
zasedací	zasedací	k2eAgFnSc2d1	zasedací
místnosti	místnost	k1gFnSc2	místnost
v	v	k7c4	v
Leinster	Leinster	k1gInSc4	Leinster
Housu	Hous	k1gInSc2	Hous
<g/>
,	,	kIx,	,
historickém	historický	k2eAgNnSc6d1	historické
sídle	sídlo	k1gNnSc6	sídlo
irského	irský	k2eAgInSc2d1	irský
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc2	tento
pocty	pocta	k1gFnSc2	pocta
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
dostalo	dostat	k5eAaPmAgNnS	dostat
jako	jako	k9	jako
vůbec	vůbec	k9	vůbec
prvnímu	první	k4xOgNnSc3	první
cizinci	cizinec	k1gMnPc1	cizinec
<g/>
.	.	kIx.	.
</s>
<s>
Cenu	cena	k1gFnSc4	cena
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
za	za	k7c4	za
přínos	přínos	k1gInSc4	přínos
občanské	občanský	k2eAgFnSc2d1	občanská
společnosti	společnost	k1gFnSc2	společnost
uděluje	udělovat	k5eAaImIp3nS	udělovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
Český	český	k2eAgInSc1d1	český
filmový	filmový	k2eAgInSc1d1	filmový
a	a	k8xC	a
televizní	televizní	k2eAgInSc1d1	televizní
svaz	svaz	k1gInSc1	svaz
FITES	FITES	kA	FITES
společně	společně	k6eAd1	společně
s	s	k7c7	s
městem	město	k1gNnSc7	město
Beroun	Beroun	k1gInSc1	Beroun
<g/>
.	.	kIx.	.
</s>
<s>
Cenu	cena	k1gFnSc4	cena
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
za	za	k7c4	za
kreativní	kreativní	k2eAgInSc4d1	kreativní
disent	disent	k1gInSc4	disent
uděluje	udělovat	k5eAaImIp3nS	udělovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
Human	Humana	k1gFnPc2	Humana
Rights	Rightsa	k1gFnPc2	Rightsa
Foundation	Foundation	k1gInSc1	Foundation
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
je	být	k5eAaImIp3nS	být
Radou	rada	k1gFnSc7	rada
Evropy	Evropa	k1gFnSc2	Evropa
udělována	udělován	k2eAgFnSc1d1	udělována
Cena	cena	k1gFnSc1	cena
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
za	za	k7c4	za
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
<g/>
,	,	kIx,	,
historicky	historicky	k6eAd1	historicky
prvním	první	k4xOgMnSc7	první
nositelem	nositel	k1gMnSc7	nositel
se	s	k7c7	s
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2013	[number]	k4	2013
stal	stát	k5eAaPmAgMnS	stát
běloruský	běloruský	k2eAgMnSc1d1	běloruský
aktivista	aktivista	k1gMnSc1	aktivista
Ales	Alesa	k1gFnPc2	Alesa
Bjaljacki	Bjaljack	k1gFnSc2	Bjaljack
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
,	,	kIx,	,
v	v	k7c4	v
den	den	k1gInSc4	den
úmrtí	úmrtí	k1gNnPc2	úmrtí
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
,	,	kIx,	,
uctívají	uctívat	k5eAaImIp3nP	uctívat
lidé	člověk	k1gMnPc1	člověk
jeho	jeho	k3xOp3gFnSc4	jeho
památku	památka	k1gFnSc4	památka
mnoha	mnoho	k4c2	mnoho
vzpomínkovými	vzpomínkový	k2eAgFnPc7d1	vzpomínková
akcemi	akce	k1gFnPc7	akce
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
den	den	k1gInSc1	den
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k8xS	jako
Den	den	k1gInSc1	den
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
i	i	k9	i
díky	díky	k7c3	díky
iniciativě	iniciativa	k1gFnSc3	iniciativa
Krátké	Krátké	k2eAgFnPc4d1	Krátké
kalhoty	kalhoty	k1gFnPc4	kalhoty
pro	pro	k7c4	pro
Václava	Václav	k1gMnSc4	Václav
Havla	Havel	k1gMnSc4	Havel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
1	[number]	k4	1
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
úmrtí	úmrtí	k1gNnSc2	úmrtí
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
iniciativa	iniciativa	k1gFnSc1	iniciativa
s	s	k7c7	s
názvem	název	k1gInSc7	název
Krátké	Krátké	k2eAgFnPc1d1	Krátké
kalhoty	kalhoty	k1gFnPc1	kalhoty
pro	pro	k7c4	pro
Václava	Václav	k1gMnSc4	Václav
Havla	Havel	k1gMnSc4	Havel
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
uctít	uctít	k5eAaPmF	uctít
jeho	jeho	k3xOp3gFnSc4	jeho
památku	památka	k1gFnSc4	památka
gestem	gesto	k1gNnSc7	gesto
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bude	být	k5eAaImBp3nS	být
jedinečné	jedinečný	k2eAgNnSc1d1	jedinečné
<g/>
,	,	kIx,	,
zapamatovatelné	zapamatovatelný	k2eAgNnSc1d1	zapamatovatelné
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
lehce	lehko	k6eAd1	lehko
proveditelné	proveditelný	k2eAgNnSc1d1	proveditelné
<g/>
.	.	kIx.	.
</s>
<s>
Krátké	Krátké	k2eAgFnPc1d1	Krátké
kalhoty	kalhoty	k1gFnPc1	kalhoty
připomínají	připomínat	k5eAaImIp3nP	připomínat
inauguraci	inaugurace	k1gFnSc4	inaugurace
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
do	do	k7c2	do
prezidentského	prezidentský	k2eAgInSc2d1	prezidentský
úřadu	úřad	k1gInSc2	úřad
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
ve	v	k7c6	v
viditelně	viditelně	k6eAd1	viditelně
povytažených	povytažený	k2eAgFnPc6d1	povytažená
kalhotách	kalhoty	k1gFnPc6	kalhoty
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc4	rok
si	se	k3xPyFc3	se
tak	tak	k6eAd1	tak
na	na	k7c4	na
Den	den	k1gInSc4	den
Václava	Václav	k1gMnSc4	Václav
Havla	Havel	k1gMnSc4	Havel
lidé	člověk	k1gMnPc1	člověk
vyhrnují	vyhrnovat	k5eAaImIp3nP	vyhrnovat
kalhoty	kalhoty	k1gFnPc4	kalhoty
a	a	k8xC	a
připomínají	připomínat	k5eAaImIp3nP	připomínat
si	se	k3xPyFc3	se
jeho	jeho	k3xOp3gFnSc4	jeho
památku	památka	k1gFnSc4	památka
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
iniciativě	iniciativa	k1gFnSc3	iniciativa
Krátké	Krátké	k2eAgFnPc4d1	Krátké
kalhoty	kalhoty	k1gFnPc4	kalhoty
pro	pro	k7c4	pro
Václava	Václav	k1gMnSc4	Václav
Havla	Havel	k1gMnSc4	Havel
se	se	k3xPyFc4	se
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
pravidelně	pravidelně	k6eAd1	pravidelně
připojuje	připojovat	k5eAaImIp3nS	připojovat
také	také	k6eAd1	také
velvyslanectví	velvyslanectví	k1gNnSc1	velvyslanectví
USA	USA	kA	USA
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2016	[number]	k4	2016
newyorský	newyorský	k2eAgMnSc1d1	newyorský
starosta	starosta	k1gMnSc1	starosta
Bill	Bill	k1gMnSc1	Bill
de	de	k?	de
Blasio	Blasio	k6eAd1	Blasio
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
Dnem	den	k1gInSc7	den
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
předvečer	předvečer	k1gInSc4	předvečer
80	[number]	k4	80
<g/>
.	.	kIx.	.
narozenin	narozeniny	k1gFnPc2	narozeniny
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
,	,	kIx,	,
dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
fontánách	fontána	k1gFnPc6	fontána
umístěných	umístěný	k2eAgFnPc2d1	umístěná
u	u	k7c2	u
Národní	národní	k2eAgFnSc2d1	národní
a	a	k8xC	a
univerzitní	univerzitní	k2eAgFnSc2d1	univerzitní
knihovny	knihovna	k1gFnSc2	knihovna
v	v	k7c6	v
Záhřebu	Záhřeb	k1gInSc6	Záhřeb
promítána	promítán	k2eAgFnSc1d1	promítána
Havlova	Havlův	k2eAgFnSc1d1	Havlova
fotografie	fotografie	k1gFnSc1	fotografie
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
Chorvatsko-české	chorvatsko-český	k2eAgFnSc2d1	chorvatsko-český
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Kontroverze	kontroverze	k1gFnPc4	kontroverze
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
již	již	k6eAd1	již
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
kroků	krok	k1gInPc2	krok
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
vykonal	vykonat	k5eAaPmAgMnS	vykonat
v	v	k7c6	v
úřadu	úřad	k1gInSc6	úřad
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
jím	on	k3xPp3gNnSc7	on
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
amnestie	amnestie	k1gFnSc2	amnestie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
propuštění	propuštění	k1gNnSc3	propuštění
23	[number]	k4	23
000	[number]	k4	000
vězňů	vězeň	k1gMnPc2	vězeň
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
31	[number]	k4	31
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
osoby	osoba	k1gFnPc4	osoba
odsouzené	odsouzená	k1gFnSc2	odsouzená
na	na	k7c4	na
maximálně	maximálně	k6eAd1	maximálně
3	[number]	k4	3
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
propuštěnými	propuštěný	k2eAgMnPc7d1	propuštěný
však	však	k9	však
byli	být	k5eAaImAgMnP	být
i	i	k9	i
pachatelé	pachatel	k1gMnPc1	pachatel
těžkých	těžký	k2eAgInPc2d1	těžký
zločinů	zločin	k1gInPc2	zločin
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
zmínit	zmínit	k5eAaPmF	zmínit
případ	případ	k1gInSc4	případ
vraha	vrah	k1gMnSc4	vrah
Jozefa	Jozef	k1gMnSc4	Jozef
Slováka	Slovák	k1gMnSc4	Slovák
souzeného	souzený	k2eAgMnSc4d1	souzený
za	za	k7c4	za
vraždu	vražda	k1gFnSc4	vražda
jugoslávské	jugoslávský	k2eAgFnSc2d1	jugoslávská
občanky	občanka	k1gFnSc2	občanka
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
podmínečně	podmínečně	k6eAd1	podmínečně
propuštěn	propustit	k5eAaPmNgInS	propustit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
amnestie	amnestie	k1gFnSc2	amnestie
<g/>
.	.	kIx.	.
</s>
<s>
Slovák	Slovák	k1gMnSc1	Slovák
bývá	bývat	k5eAaImIp3nS	bývat
zmiňován	zmiňován	k2eAgMnSc1d1	zmiňován
jako	jako	k8xC	jako
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
pochybení	pochybení	k1gNnPc2	pochybení
amnestie	amnestie	k1gFnSc2	amnestie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
se	se	k3xPyFc4	se
do	do	k7c2	do
vězení	vězení	k1gNnSc2	vězení
vrátil	vrátit	k5eAaPmAgMnS	vrátit
<g/>
,	,	kIx,	,
mezitím	mezitím	k6eAd1	mezitím
však	však	k9	však
zabil	zabít	k5eAaPmAgMnS	zabít
další	další	k2eAgMnPc4d1	další
čtyři	čtyři	k4xCgMnPc4	čtyři
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
propuštěnými	propuštěný	k2eAgMnPc7d1	propuštěný
vrahy	vrah	k1gMnPc7	vrah
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
budoucími	budoucí	k2eAgMnPc7d1	budoucí
vrahy	vrah	k1gMnPc7	vrah
<g/>
)	)	kIx)	)
byli	být	k5eAaImAgMnP	být
i	i	k9	i
Roman	Roman	k1gMnSc1	Roman
Kučerovský	Kučerovský	k2eAgMnSc1d1	Kučerovský
a	a	k8xC	a
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Gančarčík	Gančarčík	k1gMnSc1	Gančarčík
(	(	kIx(	(
<g/>
který	který	k3yRgMnSc1	který
během	během	k7c2	během
jedné	jeden	k4xCgFnSc2	jeden
noci	noc	k1gFnSc2	noc
stihl	stihnout	k5eAaPmAgInS	stihnout
vyvraždit	vyvraždit	k5eAaPmF	vyvraždit
celou	celý	k2eAgFnSc4d1	celá
rodinu	rodina	k1gFnSc4	rodina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ostatním	ostatní	k2eAgMnPc3d1	ostatní
vězňům	vězeň	k1gMnPc3	vězeň
byla	být	k5eAaImAgFnS	být
odpuštěna	odpuštěn	k2eAgFnSc1d1	odpuštěna
třetina	třetina	k1gFnSc1	třetina
až	až	k8xS	až
polovina	polovina	k1gFnSc1	polovina
trestu	trest	k1gInSc2	trest
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
bylo	být	k5eAaImAgNnS	být
zastaveno	zastavit	k5eAaPmNgNnS	zastavit
stíhání	stíhání	k1gNnSc4	stíhání
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
kterým	který	k3yQgFnPc3	který
hrozily	hrozit	k5eAaImAgInP	hrozit
nanejvýše	nanejvýše	k6eAd1	nanejvýše
3	[number]	k4	3
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Havel	Havel	k1gMnSc1	Havel
amnestii	amnestie	k1gFnSc4	amnestie
hájil	hájit	k5eAaImAgMnS	hájit
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Znalci	znalec	k1gMnPc1	znalec
říkají	říkat	k5eAaImIp3nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ta	ten	k3xDgFnSc1	ten
amnestie	amnestie	k1gFnSc1	amnestie
byla	být	k5eAaImAgFnS	být
smysluplná	smysluplný	k2eAgFnSc1d1	smysluplná
<g/>
.	.	kIx.	.
</s>
<s>
Samozřejmě	samozřejmě	k6eAd1	samozřejmě
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
pohnuté	pohnutý	k2eAgFnSc6d1	pohnutá
době	doba	k1gFnSc6	doba
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
bezpočet	bezpočet	k1gInSc1	bezpočet
rozmanitých	rozmanitý	k2eAgInPc2d1	rozmanitý
bludů	blud	k1gInPc2	blud
–	–	k?	–
například	například	k6eAd1	například
<g/>
,	,	kIx,	,
že	že	k8xS	že
kriminalita	kriminalita	k1gFnSc1	kriminalita
vinou	vinou	k7c2	vinou
této	tento	k3xDgFnSc2	tento
amnestie	amnestie	k1gFnSc2	amnestie
rapidně	rapidně	k6eAd1	rapidně
stoupla	stoupnout	k5eAaPmAgFnS	stoupnout
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Kritiku	kritika	k1gFnSc4	kritika
si	se	k3xPyFc3	se
vysloužily	vysloužit	k5eAaPmAgFnP	vysloužit
i	i	k8xC	i
některé	některý	k3yIgFnSc2	některý
milosti	milost	k1gFnSc2	milost
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
Havel	Havel	k1gMnSc1	Havel
udělil	udělit	k5eAaPmAgMnS	udělit
<g/>
.	.	kIx.	.
</s>
<s>
Kontroverzní	kontroverzní	k2eAgNnSc1d1	kontroverzní
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
udělená	udělený	k2eAgFnSc1d1	udělená
milost	milost	k1gFnSc1	milost
Zdeňku	Zdeněk	k1gMnSc3	Zdeněk
Růžičkovi	Růžička	k1gMnSc3	Růžička
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
dvojnásobné	dvojnásobný	k2eAgFnSc2d1	dvojnásobná
vraždy	vražda	k1gFnSc2	vražda
<g/>
.	.	kIx.	.
</s>
<s>
Milost	milost	k1gFnSc1	milost
byla	být	k5eAaImAgFnS	být
zdůvodněna	zdůvodnit	k5eAaPmNgFnS	zdůvodnit
"	"	kIx"	"
<g/>
vážným	vážný	k2eAgInSc7d1	vážný
justičním	justiční	k2eAgInSc7d1	justiční
omylem	omyl	k1gInSc7	omyl
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Růžička	Růžička	k1gMnSc1	Růžička
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
za	za	k7c4	za
vydírání	vydírání	k1gNnSc4	vydírání
k	k	k7c3	k
17	[number]	k4	17
letům	let	k1gInPc3	let
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Kontroverze	kontroverze	k1gFnPc1	kontroverze
vzbudily	vzbudit	k5eAaPmAgFnP	vzbudit
i	i	k9	i
milosti	milost	k1gFnPc1	milost
udělené	udělený	k2eAgFnPc1d1	udělená
známému	známý	k1gMnSc3	známý
sportovci	sportovec	k1gMnSc3	sportovec
Radomíru	Radomír	k1gMnSc3	Radomír
Šimůnkovi	Šimůnek	k1gMnSc3	Šimůnek
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
způsobil	způsobit	k5eAaPmAgMnS	způsobit
automobilovou	automobilový	k2eAgFnSc4d1	automobilová
nehodu	nehoda	k1gFnSc4	nehoda
s	s	k7c7	s
následkem	následek	k1gInSc7	následek
tří	tři	k4xCgFnPc2	tři
úmrtí	úmrť	k1gFnPc2	úmrť
či	či	k8xC	či
Martinu	Martin	k1gMnSc3	Martin
Odložilovi	Odložil	k1gMnSc3	Odložil
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
během	během	k7c2	během
hádky	hádka	k1gFnSc2	hádka
zabil	zabít	k5eAaPmAgMnS	zabít
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
<g/>
.	.	kIx.	.
</s>
<s>
Matkou	matka	k1gFnSc7	matka
Odložila	odložit	k5eAaPmAgFnS	odložit
byla	být	k5eAaImAgFnS	být
Havlova	Havlův	k2eAgFnSc1d1	Havlova
přítelkyně	přítelkyně	k1gFnSc1	přítelkyně
Věra	Věra	k1gFnSc1	Věra
Čáslavská	Čáslavská	k1gFnSc1	Čáslavská
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
kontroverzním	kontroverzní	k2eAgInSc7d1	kontroverzní
krokem	krok	k1gInSc7	krok
byla	být	k5eAaImAgFnS	být
omluva	omluva	k1gFnSc1	omluva
sudetským	sudetský	k2eAgMnPc3d1	sudetský
Němcům	Němec	k1gMnPc3	Němec
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
krok	krok	k1gInSc4	krok
si	se	k3xPyFc3	se
Havel	Havel	k1gMnSc1	Havel
vysloužil	vysloužit	k5eAaPmAgMnS	vysloužit
kritiku	kritika	k1gFnSc4	kritika
části	část	k1gFnSc3	část
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Omluva	omluva	k1gFnSc1	omluva
zaskočila	zaskočit	k5eAaPmAgFnS	zaskočit
i	i	k9	i
funkcionáře	funkcionář	k1gMnPc4	funkcionář
Sudetoněmeckého	sudetoněmecký	k2eAgNnSc2d1	Sudetoněmecké
krajanského	krajanský	k2eAgNnSc2d1	krajanské
sdružení	sdružení	k1gNnSc2	sdružení
<g/>
.	.	kIx.	.
</s>
<s>
Havlův	Havlův	k2eAgMnSc1d1	Havlův
přítel	přítel	k1gMnSc1	přítel
Jan	Jan	k1gMnSc1	Jan
Petránek	Petránek	k1gMnSc1	Petránek
později	pozdě	k6eAd2	pozdě
o	o	k7c6	o
Havlovi	Havel	k1gMnSc6	Havel
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Já	já	k3xPp1nSc1	já
si	se	k3xPyFc3	se
Havla	Havel	k1gMnSc4	Havel
vážím	vážit	k5eAaImIp1nS	vážit
za	za	k7c4	za
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
spoustu	spousta	k1gFnSc4	spousta
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
vykonal	vykonat	k5eAaPmAgMnS	vykonat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
on	on	k3xPp3gMnSc1	on
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
necitlivý	citlivý	k2eNgMnSc1d1	necitlivý
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
cítí	cítit	k5eAaImIp3nS	cítit
veřejnost	veřejnost	k1gFnSc1	veřejnost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Své	svůj	k3xOyFgNnSc4	svůj
tvrzení	tvrzení	k1gNnSc4	tvrzení
zdůvodnil	zdůvodnit	k5eAaPmAgMnS	zdůvodnit
právě	právě	k9	právě
omluvou	omluva	k1gFnSc7	omluva
sudetským	sudetský	k2eAgMnPc3d1	sudetský
Němcům	Němec	k1gMnPc3	Němec
a	a	k8xC	a
také	také	k9	také
Havlovým	Havlův	k2eAgInSc7d1	Havlův
odporem	odpor	k1gInSc7	odpor
k	k	k7c3	k
Rusku	Rusko	k1gNnSc3	Rusko
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dle	dle	k7c2	dle
jeho	on	k3xPp3gInSc2	on
názoru	názor	k1gInSc2	názor
Česko	Česko	k1gNnSc1	Česko
stál	stát	k5eAaImAgInS	stát
mnoho	mnoho	k4c4	mnoho
obchodních	obchodní	k2eAgFnPc2d1	obchodní
příležitostí	příležitost	k1gFnPc2	příležitost
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
bývá	bývat	k5eAaImIp3nS	bývat
také	také	k6eAd1	také
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
za	za	k7c4	za
podporu	podpora	k1gFnSc4	podpora
bombardování	bombardování	k1gNnSc2	bombardování
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgNnSc6	který
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
použit	použít	k5eAaPmNgInS	použít
pojem	pojem	k1gInSc1	pojem
bombardování	bombardování	k1gNnSc2	bombardování
ve	v	k7c6	v
spojitosti	spojitost	k1gFnSc6	spojitost
se	s	k7c7	s
slovem	slovo	k1gNnSc7	slovo
"	"	kIx"	"
<g/>
humanitární	humanitární	k2eAgFnSc2d1	humanitární
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
spojení	spojení	k1gNnSc1	spojení
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Havlova	Havlův	k2eAgInSc2d1	Havlův
rozhovoru	rozhovor	k1gInSc2	rozhovor
pro	pro	k7c4	pro
deník	deník	k1gInSc4	deník
Le	Le	k1gFnSc2	Le
Monde	Mond	k1gMnSc5	Mond
ze	z	k7c2	z
dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
Havel	Havel	k1gMnSc1	Havel
sdělil	sdělit	k5eAaPmAgMnS	sdělit
<g/>
,	,	kIx,	,
že	že	k8xS	že
aktuální	aktuální	k2eAgInPc1d1	aktuální
nálety	nálet	k1gInPc1	nálet
a	a	k8xC	a
bomby	bomba	k1gFnPc1	bomba
nejsou	být	k5eNaImIp3nP	být
vyvolány	vyvolat	k5eAaPmNgFnP	vyvolat
hmotným	hmotný	k2eAgInSc7d1	hmotný
zájmem	zájem	k1gInSc7	zájem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gFnSc1	jejich
povaha	povaha	k1gFnSc1	povaha
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
Havla	Havel	k1gMnSc2	Havel
vysloveně	vysloveně	k6eAd1	vysloveně
humanitární	humanitární	k2eAgFnSc7d1	humanitární
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
tento	tento	k3xDgInSc4	tento
popis	popis	k1gInSc4	popis
označil	označit	k5eAaPmAgMnS	označit
Richard	Richard	k1gMnSc1	Richard
Falbr	Falbr	k1gMnSc1	Falbr
za	za	k7c4	za
"	"	kIx"	"
<g/>
humanitární	humanitární	k2eAgNnSc4d1	humanitární
bombardování	bombardování	k1gNnSc4	bombardování
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
často	často	k6eAd1	často
bývá	bývat	k5eAaImIp3nS	bývat
neprávem	neprávo	k1gNnSc7	neprávo
připisován	připisovat	k5eAaImNgInS	připisovat
právě	právě	k6eAd1	právě
Václavu	Václav	k1gMnSc3	Václav
Havlovi	Havel	k1gMnSc3	Havel
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
však	však	k9	však
odmítal	odmítat	k5eAaImAgMnS	odmítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc7	jeho
autorem	autor	k1gMnSc7	autor
<g/>
.	.	kIx.	.
</s>
<s>
Havel	Havel	k1gMnSc1	Havel
později	pozdě	k6eAd2	pozdě
podpořil	podpořit	k5eAaPmAgMnS	podpořit
i	i	k9	i
americkou	americký	k2eAgFnSc4d1	americká
invazi	invaze	k1gFnSc4	invaze
do	do	k7c2	do
Iráku	Irák	k1gInSc2	Irák
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
řada	řada	k1gFnSc1	řada
politiků	politik	k1gMnPc2	politik
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
premiéra	premiér	k1gMnSc2	premiér
Špidly	Špidla	k1gMnSc2	Špidla
<g/>
,	,	kIx,	,
s	s	k7c7	s
invazí	invaze	k1gFnSc7	invaze
nesouhlasila	souhlasit	k5eNaImAgFnS	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
neochotu	neochota	k1gFnSc4	neochota
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
Německa	Německo	k1gNnSc2	Německo
podpořit	podpořit	k5eAaPmF	podpořit
americký	americký	k2eAgInSc4d1	americký
vojenský	vojenský	k2eAgInSc4d1	vojenský
zásah	zásah	k1gInSc4	zásah
proti	proti	k7c3	proti
Iráku	Irák	k1gInSc3	Irák
podepsal	podepsat	k5eAaPmAgMnS	podepsat
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
a	a	k8xC	a
sedm	sedm	k4xCc1	sedm
dalších	další	k2eAgMnPc2d1	další
evropských	evropský	k2eAgMnPc2d1	evropský
státníků	státník	k1gMnPc2	státník
takzvaný	takzvaný	k2eAgInSc4d1	takzvaný
Dopis	dopis	k1gInSc4	dopis
osmi	osm	k4xCc2	osm
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
vyjádřili	vyjádřit	k5eAaPmAgMnP	vyjádřit
podporu	podpora	k1gFnSc4	podpora
vojenské	vojenský	k2eAgFnSc3d1	vojenská
invazi	invaze	k1gFnSc3	invaze
<g/>
.	.	kIx.	.
</s>
<s>
Dokument	dokument	k1gInSc1	dokument
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
řadu	řada	k1gFnSc4	řada
protichůdných	protichůdný	k2eAgFnPc2d1	protichůdná
reakcí	reakce	k1gFnPc2	reakce
na	na	k7c6	na
domácí	domácí	k2eAgFnSc6d1	domácí
i	i	k8xC	i
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
politické	politický	k2eAgFnSc6d1	politická
scéně	scéna	k1gFnSc6	scéna
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
po	po	k7c6	po
následné	následný	k2eAgFnSc6d1	následná
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
žádný	žádný	k3yNgInSc4	žádný
z	z	k7c2	z
oficiálně	oficiálně	k6eAd1	oficiálně
uváděných	uváděný	k2eAgInPc2d1	uváděný
důvodů	důvod	k1gInPc2	důvod
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
konflikt	konflikt	k1gInSc4	konflikt
prokázat	prokázat	k5eAaPmF	prokázat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Dopisu	dopis	k1gInSc2	dopis
osmi	osm	k4xCc2	osm
se	se	k3xPyFc4	se
Havel	Havel	k1gMnSc1	Havel
později	pozdě	k6eAd2	pozdě
distancoval	distancovat	k5eAaBmAgMnS	distancovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
bývá	bývat	k5eAaImIp3nS	bývat
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
obviňován	obviňovat	k5eAaImNgMnS	obviňovat
z	z	k7c2	z
likvidace	likvidace	k1gFnSc2	likvidace
tamního	tamní	k2eAgInSc2d1	tamní
zbrojního	zbrojní	k2eAgInSc2d1	zbrojní
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
když	když	k8xS	když
po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zastavení	zastavení	k1gNnSc3	zastavení
zbrojařské	zbrojařský	k2eAgFnSc2d1	zbrojařská
výroby	výroba	k1gFnSc2	výroba
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
jej	on	k3xPp3gMnSc4	on
obvinil	obvinit	k5eAaPmAgMnS	obvinit
i	i	k9	i
slovenský	slovenský	k2eAgMnSc1d1	slovenský
premiér	premiér	k1gMnSc1	premiér
Robert	Robert	k1gMnSc1	Robert
Fico	Fico	k1gMnSc1	Fico
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
tvrzení	tvrzení	k1gNnPc1	tvrzení
však	však	k9	však
bývají	bývat	k5eAaImIp3nP	bývat
zpochybňována	zpochybňován	k2eAgNnPc1d1	zpochybňováno
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
o	o	k7c6	o
útlumu	útlum	k1gInSc6	útlum
slovenského	slovenský	k2eAgInSc2d1	slovenský
zbrojního	zbrojní	k2eAgInSc2d1	zbrojní
průmyslu	průmysl	k1gInSc2	průmysl
bylo	být	k5eAaImAgNnS	být
údajně	údajně	k6eAd1	údajně
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
ještě	ještě	k9	ještě
před	před	k7c7	před
sametovou	sametový	k2eAgFnSc7d1	sametová
revolucí	revoluce	k1gFnSc7	revoluce
<g/>
.	.	kIx.	.
</s>
