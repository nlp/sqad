<s>
Rada	rada	k1gFnSc1	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
je	být	k5eAaImIp3nS	být
orgán	orgán	k1gInSc1	orgán
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
odpovědný	odpovědný	k2eAgMnSc1d1	odpovědný
za	za	k7c4	za
zajištění	zajištění	k1gNnSc4	zajištění
světového	světový	k2eAgInSc2d1	světový
míru	mír	k1gInSc2	mír
<g/>
,	,	kIx,	,
mezinárodní	mezinárodní	k2eAgFnSc3d1	mezinárodní
bezpečnosti	bezpečnost	k1gFnSc3	bezpečnost
a	a	k8xC	a
řešení	řešení	k1gNnSc3	řešení
konfliktů	konflikt	k1gInPc2	konflikt
<g/>
.	.	kIx.	.
</s>
