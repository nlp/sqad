<s>
Rada	rada	k1gFnSc1	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
je	být	k5eAaImIp3nS	být
orgán	orgán	k1gInSc1	orgán
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
odpovědný	odpovědný	k2eAgMnSc1d1	odpovědný
za	za	k7c4	za
zajištění	zajištění	k1gNnSc4	zajištění
světového	světový	k2eAgInSc2d1	světový
míru	mír	k1gInSc2	mír
<g/>
,	,	kIx,	,
mezinárodní	mezinárodní	k2eAgFnSc3d1	mezinárodní
bezpečnosti	bezpečnost	k1gFnSc3	bezpečnost
a	a	k8xC	a
řešení	řešení	k1gNnSc3	řešení
konfliktů	konflikt	k1gInPc2	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
usnesení	usnesení	k1gNnSc1	usnesení
ostatních	ostatní	k2eAgInPc2d1	ostatní
orgánů	orgán	k1gInPc2	orgán
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
členským	členský	k2eAgInPc3d1	členský
státům	stát	k1gInPc3	stát
OSN	OSN	kA	OSN
pouze	pouze	k6eAd1	pouze
doporučující	doporučující	k2eAgFnSc1d1	doporučující
<g/>
,	,	kIx,	,
rezoluce	rezoluce	k1gFnSc1	rezoluce
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
jsou	být	k5eAaImIp3nP	být
závazné	závazný	k2eAgInPc1d1	závazný
a	a	k8xC	a
Rada	rada	k1gFnSc1	rada
může	moct	k5eAaImIp3nS	moct
jejich	jejich	k3xOp3gNnSc4	jejich
splnění	splnění	k1gNnSc4	splnění
vynucovat	vynucovat	k5eAaImF	vynucovat
i	i	k9	i
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
orgán	orgán	k1gInSc1	orgán
zasedá	zasedat	k5eAaImIp3nS	zasedat
stále	stále	k6eAd1	stále
a	a	k8xC	a
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
kompetence	kompetence	k1gFnSc2	kompetence
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
zachování	zachování	k1gNnSc4	zachování
míru	mír	k1gInSc2	mír
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
řešení	řešení	k1gNnSc2	řešení
sporů	spor	k1gInPc2	spor
úprava	úprava	k1gFnSc1	úprava
zbrojení	zbrojení	k1gNnSc3	zbrojení
používání	používání	k1gNnSc6	používání
sankcí	sankce	k1gFnSc7	sankce
vůči	vůči	k7c3	vůči
narušiteli	narušitel	k1gMnSc3	narušitel
podnikání	podnikání	k1gNnSc2	podnikání
vojenských	vojenský	k2eAgNnPc2d1	vojenské
akcí	akce	k1gFnSc7	akce
proti	proti	k7c3	proti
agresoru	agresor	k1gMnSc3	agresor
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
kandidáta	kandidát	k1gMnSc4	kandidát
na	na	k7c4	na
generálního	generální	k2eAgMnSc4d1	generální
tajemníka	tajemník	k1gMnSc4	tajemník
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Valným	valný	k2eAgNnSc7d1	Valné
shromážděním	shromáždění	k1gNnSc7	shromáždění
volí	volit	k5eAaImIp3nS	volit
soudce	soudce	k1gMnSc1	soudce
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
soudního	soudní	k2eAgInSc2d1	soudní
dvora	dvůr	k1gInSc2	dvůr
předkládá	předkládat	k5eAaImIp3nS	předkládat
zprávy	zpráva	k1gFnPc4	zpráva
o	o	k7c6	o
svojí	svůj	k3xOyFgFnSc6	svůj
činnosti	činnost	k1gFnSc6	činnost
valnému	valný	k2eAgInSc3d1	valný
shromáždění	shromáždění	k1gNnSc6	shromáždění
Rada	rada	k1gFnSc1	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
jako	jako	k8xS	jako
jediný	jediný	k2eAgInSc1d1	jediný
orgán	orgán	k1gInSc1	orgán
může	moct	k5eAaImIp3nS	moct
nařídit	nařídit	k5eAaPmF	nařídit
použití	použití	k1gNnSc4	použití
síly	síla	k1gFnSc2	síla
při	při	k7c6	při
řešení	řešení	k1gNnSc6	řešení
ohrožení	ohrožení	k1gNnSc2	ohrožení
míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
je	být	k5eAaImIp3nS	být
patnáctičlenná	patnáctičlenný	k2eAgFnSc1d1	patnáctičlenná
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
členů	člen	k1gMnPc2	člen
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
stálí	stálý	k2eAgMnPc1d1	stálý
členové	člen	k1gMnPc1	člen
se	s	k7c7	s
zvláštními	zvláštní	k2eAgFnPc7d1	zvláštní
pravomocemi	pravomoc	k1gFnPc7	pravomoc
<g/>
,	,	kIx,	,
zbylých	zbylý	k2eAgMnPc2d1	zbylý
10	[number]	k4	10
členů	člen	k1gMnPc2	člen
(	(	kIx(	(
<g/>
nestálí	stálit	k5eNaImIp3nP	stálit
členové	člen	k1gMnPc1	člen
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
voleno	volit	k5eAaImNgNnS	volit
Valným	valný	k2eAgNnSc7d1	Valné
shromážděním	shromáždění	k1gNnSc7	shromáždění
OSN	OSN	kA	OSN
na	na	k7c6	na
dvouleté	dvouletý	k2eAgNnSc4d1	dvouleté
období	období	k1gNnSc4	období
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
každým	každý	k3xTgInSc7	každý
rokem	rok	k1gInSc7	rok
se	se	k3xPyFc4	se
obmění	obměnit	k5eAaPmIp3nS	obměnit
pět	pět	k4xCc1	pět
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavým	zajímavý	k2eAgInSc7d1	zajímavý
faktem	fakt	k1gInSc7	fakt
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
všech	všecek	k3xTgNnPc2	všecek
pět	pět	k4xCc1	pět
stálých	stálý	k2eAgMnPc2d1	stálý
členů	člen	k1gMnPc2	člen
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
je	být	k5eAaImIp3nS	být
shodných	shodný	k2eAgFnPc2d1	shodná
s	s	k7c7	s
pěti	pět	k4xCc7	pět
jadernými	jaderný	k2eAgFnPc7d1	jaderná
mocnostmi	mocnost	k1gFnPc7	mocnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
jsou	být	k5eAaImIp3nP	být
signatáři	signatář	k1gMnPc1	signatář
Smlouvy	smlouva	k1gFnPc4	smlouva
o	o	k7c4	o
nešíření	nešíření	k1gNnSc4	nešíření
jaderných	jaderný	k2eAgFnPc2d1	jaderná
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
stálých	stálý	k2eAgMnPc2d1	stálý
členů	člen	k1gMnPc2	člen
představovalo	představovat	k5eAaImAgNnS	představovat
vítězné	vítězný	k2eAgFnSc3d1	vítězná
velmoci	velmoc	k1gFnSc3	velmoc
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
to	ten	k3xDgNnSc1	ten
<g/>
:	:	kIx,	:
Čínská	čínský	k2eAgFnSc1d1	čínská
republika	republika	k1gFnSc1	republika
Francie	Francie	k1gFnSc2	Francie
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
USA	USA	kA	USA
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
Čína	Čína	k1gFnSc1	Čína
byla	být	k5eAaImAgFnS	být
reprezentována	reprezentovat	k5eAaImNgFnS	reprezentovat
Čínskou	čínský	k2eAgFnSc7d1	čínská
republikou	republika	k1gFnSc7	republika
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Tchaj-wan	Tchajan	k1gInSc1	Tchaj-wan
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
je	být	k5eAaImIp3nS	být
Čína	Čína	k1gFnSc1	Čína
v	v	k7c6	v
OSN	OSN	kA	OSN
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
v	v	k7c6	v
Radě	rada	k1gFnSc6	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
zastoupena	zastoupit	k5eAaPmNgFnS	zastoupit
pevninskou	pevninský	k2eAgFnSc7d1	pevninská
Čínskou	čínský	k2eAgFnSc7d1	čínská
lidovou	lidový	k2eAgFnSc7d1	lidová
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
zaujalo	zaujmout	k5eAaPmAgNnS	zaujmout
post	post	k1gInSc4	post
stálého	stálý	k2eAgMnSc2d1	stálý
člena	člen	k1gMnSc2	člen
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Aktuálními	aktuální	k2eAgMnPc7d1	aktuální
stálými	stálý	k2eAgMnPc7d1	stálý
členy	člen	k1gMnPc7	člen
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
Francie	Francie	k1gFnSc2	Francie
Rusko	Rusko	k1gNnSc1	Rusko
USA	USA	kA	USA
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
Nestálé	stálý	k2eNgNnSc1d1	nestálé
členy	člen	k1gMnPc7	člen
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
volí	volit	k5eAaImIp3nS	volit
Valné	valný	k2eAgNnSc1d1	Valné
shromáždění	shromáždění	k1gNnSc1	shromáždění
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
každoročním	každoroční	k2eAgNnSc6d1	každoroční
podzimním	podzimní	k2eAgNnSc6d1	podzimní
zasedání	zasedání	k1gNnSc6	zasedání
a	a	k8xC	a
zvolení	zvolený	k2eAgMnPc1d1	zvolený
členové	člen	k1gMnPc1	člen
nastupují	nastupovat	k5eAaImIp3nP	nastupovat
do	do	k7c2	do
Rady	rada	k1gFnSc2	rada
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	ledno	k1gNnSc6wB	ledno
nadcházejícího	nadcházející	k2eAgInSc2d1	nadcházející
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
stanoveny	stanovit	k5eAaPmNgFnP	stanovit
regionální	regionální	k2eAgFnPc1d1	regionální
skupiny	skupina	k1gFnPc1	skupina
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
navrhují	navrhovat	k5eAaImIp3nP	navrhovat
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
středu	střed	k1gInSc2	střed
jednu	jeden	k4xCgFnSc4	jeden
nebo	nebo	k8xC	nebo
několik	několik	k4yIc4	několik
kandidátských	kandidátský	k2eAgFnPc2d1	kandidátská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Afrika	Afrika	k1gFnSc1	Afrika
vysílá	vysílat	k5eAaImIp3nS	vysílat
do	do	k7c2	do
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc6	bezpečnost
tři	tři	k4xCgInPc4	tři
nestálé	stálý	k2eNgInPc4d1	nestálý
členy	člen	k1gInPc4	člen
<g/>
,	,	kIx,	,
Asie	Asie	k1gFnSc1	Asie
<g/>
,	,	kIx,	,
Latinská	latinský	k2eAgFnSc1d1	Latinská
Amerika	Amerika	k1gFnSc1	Amerika
a	a	k8xC	a
západní	západní	k2eAgFnSc1d1	západní
Evropa	Evropa	k1gFnSc1	Evropa
po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
nestálých	stálý	k2eNgInPc6d1	nestálý
členech	člen	k1gInPc6	člen
a	a	k8xC	a
východní	východní	k2eAgFnSc1d1	východní
Evropa	Evropa	k1gFnSc1	Evropa
(	(	kIx(	(
<g/>
kam	kam	k6eAd1	kam
spadá	spadat	k5eAaPmIp3nS	spadat
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
jednoho	jeden	k4xCgMnSc2	jeden
zástupce	zástupce	k1gMnSc2	zástupce
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
zástupců	zástupce	k1gMnPc2	zástupce
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
arabská	arabský	k2eAgFnSc1d1	arabská
země	země	k1gFnSc1	země
spadající	spadající	k2eAgFnSc1d1	spadající
střídavě	střídavě	k6eAd1	střídavě
pod	pod	k7c4	pod
Afriku	Afrika	k1gFnSc4	Afrika
nebo	nebo	k8xC	nebo
Asii	Asie	k1gFnSc4	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Žádná	žádný	k3yNgFnSc1	žádný
země	země	k1gFnSc1	země
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
do	do	k7c2	do
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
dvakrát	dvakrát	k6eAd1	dvakrát
bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
musí	muset	k5eAaImIp3nS	muset
následovat	následovat	k5eAaImF	následovat
alespoň	alespoň	k9	alespoň
jednoletá	jednoletý	k2eAgFnSc1d1	jednoletá
přestávka	přestávka	k1gFnSc1	přestávka
<g/>
.	.	kIx.	.
</s>
<s>
Nestálými	stálý	k2eNgMnPc7d1	nestálý
členy	člen	k1gMnPc7	člen
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
období	období	k1gNnSc6	období
1	[number]	k4	1
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
–	–	k?	–
31	[number]	k4	31
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
státy	stát	k1gInPc7	stát
<g/>
:	:	kIx,	:
Angola	Angola	k1gFnSc1	Angola
(	(	kIx(	(
<g/>
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
)	)	kIx)	)
Malajsie	Malajsie	k1gFnSc1	Malajsie
(	(	kIx(	(
<g/>
Asie	Asie	k1gFnSc1	Asie
<g/>
)	)	kIx)	)
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
(	(	kIx(	(
<g/>
Západní	západní	k2eAgFnSc1d1	západní
Evropa	Evropa	k1gFnSc1	Evropa
a	a	k8xC	a
jiní	jiný	k1gMnPc1	jiný
<g/>
)	)	kIx)	)
Španělsko	Španělsko	k1gNnSc1	Španělsko
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnSc1d1	západní
Evropa	Evropa	k1gFnSc1	Evropa
a	a	k8xC	a
jiní	jiný	k1gMnPc1	jiný
<g/>
)	)	kIx)	)
Venezuela	Venezuela	k1gFnSc1	Venezuela
(	(	kIx(	(
<g/>
Latinská	latinský	k2eAgFnSc1d1	Latinská
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
)	)	kIx)	)
Aktuálními	aktuální	k2eAgMnPc7d1	aktuální
nestálými	stálý	k2eNgMnPc7d1	nestálý
členy	člen	k1gMnPc7	člen
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
–	–	k?	–
31	[number]	k4	31
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
<g/>
:	:	kIx,	:
Egypt	Egypt	k1gInSc1	Egypt
(	(	kIx(	(
<g/>
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
)	)	kIx)	)
Japonsko	Japonsko	k1gNnSc1	Japonsko
(	(	kIx(	(
<g/>
Asie	Asie	k1gFnSc1	Asie
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Senegal	Senegal	k1gInSc1	Senegal
(	(	kIx(	(
<g/>
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
)	)	kIx)	)
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
(	(	kIx(	(
<g/>
Východní	východní	k2eAgFnSc1d1	východní
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
)	)	kIx)	)
Uruguay	Uruguay	k1gFnSc1	Uruguay
(	(	kIx(	(
<g/>
Latinská	latinský	k2eAgFnSc1d1	Latinská
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
–	–	k?	–
31	[number]	k4	31
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2018	[number]	k4	2018
<g/>
:	:	kIx,	:
Etiopie	Etiopie	k1gFnSc1	Etiopie
(	(	kIx(	(
<g/>
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
)	)	kIx)	)
Kazachstán	Kazachstán	k1gInSc1	Kazachstán
(	(	kIx(	(
<g/>
Asie	Asie	k1gFnSc1	Asie
<g/>
)	)	kIx)	)
Bolívie	Bolívie	k1gFnSc1	Bolívie
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Latinská	latinský	k2eAgFnSc1d1	Latinská
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
)	)	kIx)	)
Švédsko	Švédsko	k1gNnSc1	Švédsko
(	(	kIx(	(
<g/>
Západní	západní	k2eAgFnSc1d1	západní
Evropa	Evropa	k1gFnSc1	Evropa
a	a	k8xC	a
jiní	jiný	k1gMnPc1	jiný
<g/>
)	)	kIx)	)
Itálie	Itálie	k1gFnSc1	Itálie
(	(	kIx(	(
<g/>
Západní	západní	k2eAgFnSc1d1	západní
Evropa	Evropa	k1gFnSc1	Evropa
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
-	-	kIx~	-
1.1	[number]	k4	1.1
<g/>
.2017	.2017	k4	.2017
-	-	kIx~	-
31.12	[number]	k4	31.12
<g/>
.2017	.2017	k4	.2017
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
(	(	kIx(	(
<g/>
Západní	západní	k2eAgFnSc1d1	západní
Evropa	Evropa	k1gFnSc1	Evropa
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
-	-	kIx~	-
1.1	[number]	k4	1.1
<g/>
.2018	.2018	k4	.2018
-	-	kIx~	-
31.12	[number]	k4	31.12
<g/>
.2018	.2018	k4	.2018
<g/>
)	)	kIx)	)
Zástupci	zástupce	k1gMnPc1	zástupce
všech	všecek	k3xTgFnPc2	všecek
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
musejí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
stále	stále	k6eAd1	stále
přítomni	přítomen	k2eAgMnPc1d1	přítomen
v	v	k7c6	v
newyorském	newyorský	k2eAgNnSc6d1	newyorské
sídle	sídlo	k1gNnSc6	sídlo
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
Rada	rada	k1gFnSc1	rada
mohla	moct	k5eAaImAgFnS	moct
v	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
kdykoliv	kdykoliv	k6eAd1	kdykoliv
sejít	sejít	k5eAaPmF	sejít
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
zasedání	zasedání	k1gNnSc3	zasedání
<g/>
.	.	kIx.	.
</s>
<s>
Předsednictví	předsednictví	k1gNnSc1	předsednictví
v	v	k7c6	v
radě	rada	k1gFnSc6	rada
se	se	k3xPyFc4	se
střídá	střídat	k5eAaImIp3nS	střídat
po	po	k7c6	po
měsíci	měsíc	k1gInSc6	měsíc
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
abecedního	abecední	k2eAgInSc2d1	abecední
pořádku	pořádek	k1gInSc2	pořádek
anglických	anglický	k2eAgInPc2d1	anglický
názvů	název	k1gInPc2	název
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Reforma	reforma	k1gFnSc1	reforma
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
probíhá	probíhat	k5eAaImIp3nS	probíhat
diskuse	diskuse	k1gFnSc1	diskuse
o	o	k7c4	o
rozšíření	rozšíření	k1gNnSc4	rozšíření
stálého	stálý	k2eAgNnSc2d1	stálé
členství	členství	k1gNnSc2	členství
v	v	k7c6	v
Radě	rada	k1gFnSc6	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Nejsilnější	silný	k2eAgInPc1d3	nejsilnější
předpoklady	předpoklad	k1gInPc1	předpoklad
pro	pro	k7c4	pro
obsazení	obsazení	k1gNnSc4	obsazení
nových	nový	k2eAgNnPc2d1	nové
míst	místo	k1gNnPc2	místo
mají	mít	k5eAaImIp3nP	mít
tyto	tento	k3xDgInPc1	tento
státy	stát	k1gInPc1	stát
<g/>
:	:	kIx,	:
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
a	a	k8xC	a
Německo	Německo	k1gNnSc1	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Brazílie	Brazílie	k1gFnSc1	Brazílie
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc7d3	veliký
zemí	zem	k1gFnSc7	zem
latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Indie	Indie	k1gFnSc1	Indie
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc7d3	veliký
demokracií	demokracie	k1gFnSc7	demokracie
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
druhou	druhý	k4xOgFnSc7	druhý
nejlidnatější	lidnatý	k2eAgFnSc7d3	nejlidnatější
zemí	zem	k1gFnSc7	zem
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
má	mít	k5eAaImIp3nS	mít
třetí	třetí	k4xOgFnSc4	třetí
největší	veliký	k2eAgFnSc4d3	veliký
armádu	armáda	k1gFnSc4	armáda
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
vlastní	vlastní	k2eAgFnPc4d1	vlastní
jaderné	jaderný	k2eAgFnPc4d1	jaderná
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Japonsko	Japonsko	k1gNnSc1	Japonsko
a	a	k8xC	a
Německo	Německo	k1gNnSc1	Německo
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
největší	veliký	k2eAgMnPc4d3	veliký
přispívatele	přispívatel	k1gMnPc4	přispívatel
do	do	k7c2	do
rozpočtu	rozpočet	k1gInSc2	rozpočet
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
rozšíření	rozšíření	k1gNnSc3	rozšíření
má	mít	k5eAaImIp3nS	mít
mnoho	mnoho	k4c4	mnoho
kritiků	kritik	k1gMnPc2	kritik
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejhlasitější	hlasitý	k2eAgMnPc4d3	nejhlasitější
odpůrce	odpůrce	k1gMnPc4	odpůrce
patří	patřit	k5eAaImIp3nS	patřit
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
a	a	k8xC	a
Pákistán	Pákistán	k1gInSc1	Pákistán
<g/>
.	.	kIx.	.
</s>
<s>
Evropští	evropský	k2eAgMnPc1d1	evropský
odpůrci	odpůrce	k1gMnPc1	odpůrce
prosazují	prosazovat	k5eAaImIp3nP	prosazovat
místo	místo	k1gNnSc4	místo
německého	německý	k2eAgNnSc2d1	německé
křesla	křeslo	k1gNnSc2	křeslo
křeslo	křesnout	k5eAaPmAgNnS	křesnout
pro	pro	k7c4	pro
Evropskou	evropský	k2eAgFnSc4d1	Evropská
unii	unie	k1gFnSc4	unie
<g/>
.	.	kIx.	.
</s>
<s>
Uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
o	o	k7c4	o
rozšíření	rozšíření	k1gNnSc4	rozšíření
o	o	k7c4	o
členské	členský	k2eAgInPc4d1	členský
státy	stát	k1gInPc4	stát
z	z	k7c2	z
Africké	africký	k2eAgFnSc2d1	africká
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
Egypt	Egypt	k1gInSc1	Egypt
<g/>
,	,	kIx,	,
Nigérie	Nigérie	k1gFnSc1	Nigérie
a	a	k8xC	a
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
a	a	k8xC	a
muslimský	muslimský	k2eAgInSc1d1	muslimský
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
pravomocí	pravomoc	k1gFnSc7	pravomoc
stálých	stálý	k2eAgInPc2d1	stálý
členů	člen	k1gInPc2	člen
RB	RB	kA	RB
OSN	OSN	kA	OSN
je	být	k5eAaImIp3nS	být
neomezené	omezený	k2eNgNnSc1d1	neomezené
právo	právo	k1gNnSc1	právo
veta	veto	k1gNnSc2	veto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
kterýkoliv	kterýkoliv	k3yIgInSc1	kterýkoliv
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
může	moct	k5eAaImIp3nS	moct
zablokovat	zablokovat	k5eAaPmF	zablokovat
jakékoliv	jakýkoliv	k3yIgNnSc1	jakýkoliv
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
RB	RB	kA	RB
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
veto	veto	k1gNnSc1	veto
nelze	lze	k6eNd1	lze
žádným	žádný	k3yNgInSc7	žádný
způsobem	způsob	k1gInSc7	způsob
obejít	obejít	k5eAaPmF	obejít
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1946	[number]	k4	1946
a	a	k8xC	a
2016	[number]	k4	2016
bylo	být	k5eAaImAgNnS	být
veto	veto	k1gNnSc1	veto
použito	použít	k5eAaPmNgNnS	použít
v	v	k7c6	v
celkem	celkem	k6eAd1	celkem
195	[number]	k4	195
případech	případ	k1gInPc6	případ
(	(	kIx(	(
<g/>
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
otázkách	otázka	k1gFnPc6	otázka
jej	on	k3xPp3gInSc2	on
použilo	použít	k5eAaPmAgNnS	použít
více	hodně	k6eAd2	hodně
členů	člen	k1gMnPc2	člen
současně	současně	k6eAd1	současně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2016	[number]	k4	2016
použilo	použít	k5eAaPmAgNnS	použít
veto	veto	k1gNnSc1	veto
nejvícekrát	jvícekrát	k6eNd1	jvícekrát
Rusko	Rusko	k1gNnSc1	Rusko
(	(	kIx(	(
<g/>
90	[number]	k4	90
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jako	jako	k9	jako
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgMnSc1d1	sovětský
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1957	[number]	k4	1957
<g/>
–	–	k?	–
<g/>
1985	[number]	k4	1985
Andrej	Andrej	k1gMnSc1	Andrej
Andrejevič	Andrejevič	k1gMnSc1	Andrejevič
Gromyko	Gromyko	k1gNnSc4	Gromyko
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
uplatnil	uplatnit	k5eAaPmAgMnS	uplatnit
nejvíce	hodně	k6eAd3	hodně
vet	veto	k1gNnPc2	veto
v	v	k7c6	v
Radě	rada	k1gFnSc6	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
od	od	k7c2	od
západních	západní	k2eAgMnPc2d1	západní
diplomatů	diplomat	k1gMnPc2	diplomat
přezdívku	přezdívka	k1gFnSc4	přezdívka
"	"	kIx"	"
<g/>
Mr	Mr	k1gFnSc4	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Nět	Nět	k?	Nět
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Pan	Pan	k1gMnSc1	Pan
Ne	ne	k9	ne
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
