<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Ludwig	Ludwig	k1gMnSc1	Ludwig
Mössbauer	Mössbauer	k1gMnSc1	Mössbauer
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
Mnichov	Mnichov	k1gInSc1	Mnichov
-	-	kIx~	-
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
Mnichov	Mnichov	k1gInSc1	Mnichov
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
studoval	studovat	k5eAaImAgMnS	studovat
gama	gama	k1gNnSc4	gama
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
výzkum	výzkum	k1gInSc4	výzkum
rezonanční	rezonanční	k2eAgFnSc2d1	rezonanční
absorpce	absorpce	k1gFnSc2	absorpce
gama	gama	k1gNnSc1	gama
záření	záření	k1gNnSc2	záření
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
spojený	spojený	k2eAgInSc1d1	spojený
objev	objev	k1gInSc1	objev
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
pojmenovaného	pojmenovaný	k2eAgInSc2d1	pojmenovaný
jevu	jev	k1gInSc2	jev
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
získal	získat	k5eAaPmAgInS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
společně	společně	k6eAd1	společně
s	s	k7c7	s
Robertem	Robert	k1gMnSc7	Robert
Hofstadterem	Hofstadter	k1gMnSc7	Hofstadter
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
na	na	k7c4	na
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Rudolf	Rudolf	k1gMnSc1	Rudolf
Ludwig	Ludwiga	k1gFnPc2	Ludwiga
Mössbauer	Mössbaura	k1gFnPc2	Mössbaura
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Biografie	biografie	k1gFnSc1	biografie
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Nobelprize	Nobelprize	k1gFnSc2	Nobelprize
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
