<s>
Nejvýznamnějším	významný	k2eAgNnSc7d3	nejvýznamnější
muzeem	muzeum	k1gNnSc7	muzeum
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
je	být	k5eAaImIp3nS	být
Moravské	moravský	k2eAgNnSc1d1	Moravské
zemské	zemský	k2eAgNnSc1d1	zemské
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
druhou	druhý	k4xOgFnSc7	druhý
nejstarší	starý	k2eAgFnSc7d3	nejstarší
a	a	k8xC	a
nejrozsáhlejší	rozsáhlý	k2eAgFnSc7d3	nejrozsáhlejší
muzejní	muzejní	k2eAgFnSc7d1	muzejní
institucí	instituce	k1gFnSc7	instituce
v	v	k7c6	v
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
první	první	k4xOgMnSc1	první
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
