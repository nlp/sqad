<s>
Bludný	bludný	k2eAgMnSc1d1
Holanďan	Holanďan	k1gMnSc1
(	(	kIx(
<g/>
Der	drát	k5eAaImRp2nS
fliegende	fliegend	k1gMnSc5
Holländer	Holländer	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
romantická	romantický	k2eAgFnSc1d1
opera	opera	k1gFnSc1
o	o	k7c6
třech	tři	k4xCgNnPc6
dějstvích	dějství	k1gNnPc6
německého	německý	k2eAgMnSc2d1
skladatele	skladatel	k1gMnSc2
Richarda	Richard	k1gMnSc2
Wagnera	Wagner	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
je	být	k5eAaImIp3nS
i	i	k9
autorem	autor	k1gMnSc7
libreta	libreto	k1gNnSc2
<g/>
.	.	kIx.
</s>