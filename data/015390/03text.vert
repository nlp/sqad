<s>
Bludný	bludný	k2eAgMnSc1d1
Holanďan	Holanďan	k1gMnSc1
(	(	kIx(
<g/>
opera	opera	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Bludný	bludný	k2eAgInSc1d1
HolanďanPer	HolanďanPer	k1gInSc1
fliegende	fliegend	k1gInSc5
Holländer	Holländer	k1gInSc1
Světová	světový	k2eAgFnSc1d1
premiéra	premiéra	k1gFnSc1
opery	opera	k1gFnSc2
v	v	k7c6
Drážďanech	Drážďany	k1gInPc6
roku	rok	k1gInSc2
1843	#num#	k4
<g/>
,	,	kIx,
skica	skica	k1gFnSc1
poslední	poslední	k2eAgFnSc2d1
scény	scéna	k1gFnSc2
zveřejněná	zveřejněný	k2eAgFnSc1d1
v	v	k7c6
Illustrirte	Illustrirt	k1gInSc5
ZeitungZákladní	ZeitungZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Žánr	žánr	k1gInSc1
</s>
<s>
romantická	romantický	k2eAgFnSc1d1
opera	opera	k1gFnSc1
Skladatel	skladatel	k1gMnSc1
</s>
<s>
Richard	Richard	k1gMnSc1
Wagner	Wagner	k1gMnSc1
Libretista	libretista	k1gMnSc1
</s>
<s>
Richard	Richard	k1gMnSc1
Wagner	Wagner	k1gMnSc1
Počet	počet	k1gInSc4
dějství	dějství	k1gNnSc2
</s>
<s>
tři	tři	k4xCgFnPc1
Originální	originální	k2eAgFnPc1d1
jazyk	jazyk	k1gInSc1
</s>
<s>
němčina	němčina	k1gFnSc1
Literární	literární	k2eAgFnSc1d1
předloha	předloha	k1gFnSc1
</s>
<s>
Heinrich	Heinrich	k1gMnSc1
Heine	Hein	k1gInSc5
Datum	datum	k1gNnSc1
vzniku	vznik	k1gInSc3
</s>
<s>
1841	#num#	k4
Premiéra	premiéra	k1gFnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1843	#num#	k4
<g/>
,	,	kIx,
Královské	královský	k2eAgNnSc4d1
dvorní	dvorní	k2eAgNnSc4d1
divadlo	divadlo	k1gNnSc4
<g/>
,	,	kIx,
Drážďany	Drážďany	k1gInPc4
Česká	český	k2eAgFnSc1d1
premiéra	premiéra	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1906	#num#	k4
<g/>
,	,	kIx,
Velké	velký	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
<g/>
,	,	kIx,
Plzeň	Plzeň	k1gFnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Bludný	bludný	k2eAgMnSc1d1
Holanďan	Holanďan	k1gMnSc1
(	(	kIx(
<g/>
Der	drát	k5eAaImRp2nS
fliegende	fliegend	k1gMnSc5
Holländer	Holländer	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
romantická	romantický	k2eAgFnSc1d1
opera	opera	k1gFnSc1
o	o	k7c6
třech	tři	k4xCgNnPc6
dějstvích	dějství	k1gNnPc6
německého	německý	k2eAgMnSc2d1
skladatele	skladatel	k1gMnSc2
Richarda	Richard	k1gMnSc2
Wagnera	Wagner	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
je	být	k5eAaImIp3nS
i	i	k9
autorem	autor	k1gMnSc7
libreta	libreto	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Světovou	světový	k2eAgFnSc4d1
premiéru	premiéra	k1gFnSc4
měla	mít	k5eAaImAgFnS
opera	opera	k1gFnSc1
2	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1843	#num#	k4
v	v	k7c6
Královském	královský	k2eAgNnSc6d1
dvorním	dvorní	k2eAgNnSc6d1
divadle	divadlo	k1gNnSc6
v	v	k7c6
Drážďanech	Drážďany	k1gInPc6
pod	pod	k7c7
taktovkou	taktovka	k1gFnSc7
samotného	samotný	k2eAgMnSc2d1
skladatele	skladatel	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
zde	zde	k6eAd1
byl	být	k5eAaImAgInS
dvorním	dvorní	k2eAgMnSc7d1
dirigentem	dirigent	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Námět	námět	k1gInSc1
a	a	k8xC
historie	historie	k1gFnSc1
vzniku	vznik	k1gInSc2
</s>
<s>
Richard	Richard	k1gMnSc1
Wagner	Wagner	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1842	#num#	k4
</s>
<s>
Touto	tento	k3xDgFnSc7
operou	opera	k1gFnSc7
nastoupil	nastoupit	k5eAaPmAgMnS
Wagner	Wagner	k1gMnSc1
na	na	k7c4
cestu	cesta	k1gFnSc4
umění	umění	k1gNnSc2
<g/>
,	,	kIx,
vycházejícího	vycházející	k2eAgMnSc2d1
z	z	k7c2
lidového	lidový	k2eAgInSc2d1
světa	svět	k1gInSc2
bájí	báj	k1gFnPc2
a	a	k8xC
pověstí	pověst	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
jejím	její	k3xOp3gInSc7
námětem	námět	k1gInSc7
se	se	k3xPyFc4
poprvé	poprvé	k6eAd1
seznámil	seznámit	k5eAaPmAgMnS
v	v	k7c6
Rize	Riga	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
v	v	k7c6
letech	let	k1gInPc6
1837	#num#	k4
<g/>
–	–	k?
<g/>
1839	#num#	k4
působil	působit	k5eAaImAgMnS
jako	jako	k9
dirigent	dirigent	k1gMnSc1
<g/>
,	,	kIx,
při	při	k7c6
četbě	četba	k1gFnSc6
knihy	kniha	k1gFnSc2
Heinricha	Heinrich	k1gMnSc2
Heina	Hein	k1gMnSc2
Z	z	k7c2
pamětí	paměť	k1gFnPc2
svobodného	svobodný	k2eAgMnSc2d1
pána	pán	k1gMnSc2
Schnabelewopského	Schnabelewopský	k2eAgMnSc2d1
((	((	k?
<g/>
Aus	Aus	k1gMnSc2
den	den	k1gInSc1
Memoiren	Memoirna	k1gFnPc2
des	des	k1gNnSc1
Herrn	Herrn	k1gMnSc1
von	von	k1gInSc4
Schnabelewopski	Schnabelewopsk	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
do	do	k7c2
níž	jenž	k3xRgFnSc2
je	být	k5eAaImIp3nS
zařazena	zařazen	k2eAgFnSc1d1
pověst	pověst	k1gFnSc1
o	o	k7c6
zakleté	zakletý	k2eAgFnSc6d1
lodi	loď	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
místo	místo	k7c2
dirigenta	dirigent	k1gMnSc2
ztratil	ztratit	k5eAaPmAgMnS
<g/>
,	,	kIx,
odplul	odplout	k5eAaPmAgMnS
Wagner	Wagner	k1gMnSc1
ze	z	k7c2
strachu	strach	k1gInSc2
před	před	k7c7
svými	svůj	k3xOyFgMnPc7
věřiteli	věřitel	k1gMnPc7
se	se	k3xPyFc4
svou	svůj	k3xOyFgFnSc7
ženou	žena	k1gFnSc7
na	na	k7c6
malé	malý	k2eAgFnSc6d1
plachetnici	plachetnice	k1gFnSc6
do	do	k7c2
Londýna	Londýn	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nebezpečná	bezpečný	k2eNgFnSc1d1
cesta	cesta	k1gFnSc1
na	na	k7c6
rozbouřeném	rozbouřený	k2eAgNnSc6d1
moři	moře	k1gNnSc6
ho	on	k3xPp3gMnSc4
jen	jen	k9
utvrdila	utvrdit	k5eAaPmAgFnS
v	v	k7c6
rozhodnutí	rozhodnutí	k1gNnSc6
zkomponovat	zkomponovat	k5eAaPmF
operu	opera	k1gFnSc4
na	na	k7c4
námět	námět	k1gInSc4
severské	severský	k2eAgFnSc2d1
báje	báj	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Operu	oprat	k5eAaPmIp1nS
dokončil	dokončit	k5eAaPmAgMnS
roku	rok	k1gInSc2
1841	#num#	k4
v	v	k7c6
Paříži	Paříž	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
s	s	k7c7
jejím	její	k3xOp3gNnSc7
uvedením	uvedení	k1gNnSc7
měl	mít	k5eAaImAgInS
problémy	problém	k1gInPc4
<g/>
,	,	kIx,
když	když	k8xS
ji	on	k3xPp3gFnSc4
divadla	divadlo	k1gNnSc2
v	v	k7c6
Mnichově	Mnichov	k1gInSc6
a	a	k8xC
v	v	k7c6
Lipsku	Lipsko	k1gNnSc6
odmítly	odmítnout	k5eAaPmAgFnP
jako	jako	k9
dílo	dílo	k1gNnSc4
pro	pro	k7c4
Německo	Německo	k1gNnSc4
nevhodné	vhodný	k2eNgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
popud	popud	k1gInSc4
Giacoma	Giacoma	k1gFnSc1
Meyerbeera	Meyerbeera	k1gFnSc1
se	se	k3xPyFc4
měla	mít	k5eAaImAgFnS
opera	opera	k1gFnSc1
provést	provést	k5eAaPmF
v	v	k7c6
Berlíně	Berlín	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
po	po	k7c6
výměně	výměna	k1gFnSc6
vedení	vedení	k1gNnSc2
divadla	divadlo	k1gNnSc2
z	z	k7c2
toho	ten	k3xDgNnSc2
sešlo	sejít	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
Drážďanech	Drážďany	k1gInPc6
úspěšně	úspěšně	k6eAd1
uvedena	uvést	k5eAaPmNgFnS
Wagnerova	Wagnerův	k2eAgFnSc1d1
opera	opera	k1gFnSc1
Rienzi	Rienze	k1gFnSc4
<g/>
,	,	kIx,
začalo	začít	k5eAaPmAgNnS
se	se	k3xPyFc4
drážďanské	drážďanský	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
zajímat	zajímat	k5eAaImF
i	i	k9
o	o	k7c4
tuto	tento	k3xDgFnSc4
operu	opera	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
uvedlo	uvést	k5eAaPmAgNnS
na	na	k7c6
počátku	počátek	k1gInSc6
roku	rok	k1gInSc2
1843	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okamžitý	okamžitý	k2eAgInSc1d1
úspěch	úspěch	k1gInSc1
se	se	k3xPyFc4
však	však	k9
neopakoval	opakovat	k5eNaImAgMnS
<g/>
,	,	kIx,
takže	takže	k8xS
po	po	k7c6
čtyřech	čtyři	k4xCgInPc6
večerech	večer	k1gInPc6
bylo	být	k5eAaImAgNnS
dílo	dílo	k1gNnSc1
staženo	stáhnout	k5eAaPmNgNnS
z	z	k7c2
repertoáru	repertoár	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Wagner	Wagner	k1gMnSc1
tuto	tento	k3xDgFnSc4
operu	opera	k1gFnSc4
považoval	považovat	k5eAaImAgMnS
za	za	k7c4
skutečný	skutečný	k2eAgInSc4d1
počátek	počátek	k1gInSc4
své	svůj	k3xOyFgFnSc2
hudební	hudební	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1860	#num#	k4
původní	původní	k2eAgFnSc4d1
verzi	verze	k1gFnSc4
zrevidoval	zrevidovat	k5eAaPmAgMnS
(	(	kIx(
<g/>
zejména	zejména	k9
předehra	předehra	k1gFnSc1
a	a	k8xC
konec	konec	k1gInSc1
byly	být	k5eAaImAgInP
hudebně	hudebně	k6eAd1
změněny	změněn	k2eAgInPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Bayreuthu	Bayreuth	k1gInSc6
byl	být	k5eAaImAgMnS
Bludný	bludný	k2eAgMnSc1d1
Holanďan	Holanďan	k1gMnSc1
uveden	uveden	k2eAgMnSc1d1
poprvé	poprvé	k6eAd1
roku	rok	k1gInSc2
1901	#num#	k4
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
pak	pak	k6eAd1
opakovaně	opakovaně	k6eAd1
uváděným	uváděný	k2eAgNnSc7d1
dílem	dílo	k1gNnSc7
na	na	k7c6
Bayreuthských	Bayreuthský	k2eAgFnPc6d1
hudebních	hudební	k2eAgFnPc6d1
slavnostech	slavnost	k1gFnPc6
(	(	kIx(
<g/>
Bayreuther	Bayreuthra	k1gFnPc2
Festspiele	Festspiel	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1
postavy	postava	k1gFnPc1
</s>
<s>
Daland	Daland	k1gInSc1
<g/>
,	,	kIx,
norský	norský	k2eAgMnSc1d1
námořník	námořník	k1gMnSc1
–	–	k?
bas	bas	k1gInSc1
</s>
<s>
Senta	Senta	k1gFnSc1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc1
dcera	dcera	k1gFnSc1
–	–	k?
soprán	soprán	k1gInSc1
</s>
<s>
Kormidelník	kormidelník	k1gMnSc1
Dalandův	Dalandův	k2eAgMnSc1d1
–	–	k?
tenor	tenor	k1gInSc1
</s>
<s>
Holanďan	Holanďan	k1gMnSc1
–	–	k?
baryton	baryton	k1gMnSc1
</s>
<s>
Erik	Erik	k1gMnSc1
<g/>
,	,	kIx,
myslivec	myslivec	k1gMnSc1
–	–	k?
tenor	tenor	k1gMnSc1
</s>
<s>
Mary	Mary	k1gFnSc1
<g/>
,	,	kIx,
Sentina	Sentin	k2eAgFnSc1d1
chůva	chůva	k1gFnSc1
–	–	k?
alt	alt	k1gInSc1
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1
postavy	postava	k1gFnPc1
opery	opera	k1gFnSc2
–	–	k?
skici	skica	k1gFnSc2
z	z	k7c2
premiéry	premiéra	k1gFnSc2
roku	rok	k1gInSc2
1843	#num#	k4
uveřejněné	uveřejněný	k2eAgFnSc2d1
v	v	k7c6
Illustrirte	Illustrirt	k1gMnSc5
Zeitung	Zeitunga	k1gFnPc2
</s>
<s>
Daland	Daland	k1gInSc1
</s>
<s>
Holanďan	Holanďan	k1gMnSc1
</s>
<s>
Senta	Senta	k1gFnSc1
</s>
<s>
Erik	Erik	k1gMnSc1
</s>
<s>
Mary	Mary	k1gFnSc1
</s>
<s>
Obsah	obsah	k1gInSc1
opery	opera	k1gFnSc2
</s>
<s>
PřeskočitVarování	PřeskočitVarování	k1gNnSc1
<g/>
:	:	kIx,
Následující	následující	k2eAgFnSc1d1
část	část	k1gFnSc1
článku	článek	k1gInSc2
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Místo	místo	k1gNnSc1
a	a	k8xC
doba	doba	k1gFnSc1
<g/>
:	:	kIx,
Norské	norský	k2eAgNnSc1d1
pobřeží	pobřeží	k1gNnSc1
<g/>
,	,	kIx,
kolem	kolem	k7c2
roku	rok	k1gInSc2
1650	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
dějství	dějství	k1gNnSc6
</s>
<s>
Kulisy	kulisa	k1gFnPc1
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
dějství	dějství	k1gNnSc6
opery	opera	k1gFnSc2
(	(	kIx(
<g/>
Mnichov	Mnichov	k1gInSc1
1917	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Francouzský	francouzský	k2eAgMnSc1d1
barytonista	barytonista	k1gMnSc1
Maurice	Maurika	k1gFnSc3
Renaud	Renaud	k1gMnSc1
jako	jako	k8xC,k8xS
Holanďan	Holanďan	k1gMnSc1
</s>
<s>
Silná	silný	k2eAgFnSc1d1
bouře	bouře	k1gFnSc1
donutí	donutit	k5eAaPmIp3nS
loď	loď	k1gFnSc4
norského	norský	k2eAgMnSc4d1
kupce	kupec	k1gMnSc4
Dalanda	Dalando	k1gNnSc2
zakotvit	zakotvit	k5eAaPmF
u	u	k7c2
skalnatého	skalnatý	k2eAgNnSc2d1
pobřeží	pobřeží	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
vyčerpané	vyčerpaný	k2eAgNnSc4d1
mužstvo	mužstvo	k1gNnSc4
<g/>
,	,	kIx,
včetně	včetně	k7c2
kormidelníka	kormidelník	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
měl	mít	k5eAaImAgMnS
být	být	k5eAaImF
na	na	k7c6
stráži	stráž	k1gFnSc6
<g/>
,	,	kIx,
usne	usnout	k5eAaPmIp3nS
<g/>
,	,	kIx,
zakotví	zakotvit	k5eAaPmIp3nS
vedle	vedle	k7c2
Dalandovy	Dalandův	k2eAgFnSc2d1
lodi	loď	k1gFnSc2
podivný	podivný	k2eAgInSc4d1
černý	černý	k2eAgInSc4d1
koráb	koráb	k1gInSc4
<g/>
,	,	kIx,
ze	z	k7c2
kterého	který	k3yQgNnSc2,k3yIgNnSc2,k3yRgNnSc2
vystoupí	vystoupit	k5eAaPmIp3nS
na	na	k7c4
břeh	břeh	k1gInSc4
bledý	bledý	k2eAgInSc4d1
<g/>
,	,	kIx,
černě	černě	k6eAd1
oděný	oděný	k2eAgMnSc1d1
muž	muž	k1gMnSc1
–	–	k?
Holanďan	Holanďan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
proklet	proklít	k5eAaPmNgMnS
za	za	k7c4
své	svůj	k3xOyFgNnSc4
rouhání	rouhání	k1gNnSc4
Bohu	bůh	k1gMnSc3
a	a	k8xC
jen	jen	k9
jednou	jednou	k6eAd1
za	za	k7c4
sedm	sedm	k4xCc4
let	léto	k1gNnPc2
může	moct	k5eAaImIp3nS
přistát	přistát	k5eAaImF,k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Daland	Daland	k1gInSc1
se	se	k3xPyFc4
probudí	probudit	k5eAaPmIp3nS
a	a	k8xC
na	na	k7c6
břehu	břeh	k1gInSc6
se	se	k3xPyFc4
setká	setkat	k5eAaPmIp3nS
s	s	k7c7
Holanďanem	Holanďan	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
Holanďan	Holanďan	k1gMnSc1
zjistí	zjistit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Daland	Daland	k1gInSc1
má	mít	k5eAaImIp3nS
mladou	mladý	k2eAgFnSc4d1
dceru	dcera	k1gFnSc4
<g/>
,	,	kIx,
žádá	žádat	k5eAaImIp3nS
o	o	k7c4
její	její	k3xOp3gFnSc4
ruku	ruka	k1gFnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
ho	on	k3xPp3gInSc4
z	z	k7c2
prokletí	prokletí	k1gNnSc2
může	moct	k5eAaImIp3nS
vysvobodit	vysvobodit	k5eAaPmF
jen	jen	k9
žena	žena	k1gFnSc1
<g/>
,	,	kIx,
věrná	věrný	k2eAgFnSc1d1
až	až	k9
za	za	k7c4
hrob	hrob	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Daland	Daland	k1gInSc1
<g/>
,	,	kIx,
zlákán	zlákán	k2eAgInSc1d1
nabízenou	nabízený	k2eAgFnSc7d1
velkou	velký	k2eAgFnSc7d1
odměnou	odměna	k1gFnSc7
<g/>
,	,	kIx,
mu	on	k3xPp3gMnSc3
dceru	dcera	k1gFnSc4
slíbí	slíbit	k5eAaPmIp3nS
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
ví	vědět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
zasnoubena	zasnouben	k2eAgFnSc1d1
s	s	k7c7
myslivcem	myslivec	k1gMnSc7
Erikem	Erik	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
dějství	dějství	k1gNnSc6
</s>
<s>
V	v	k7c6
Dalandově	Dalandův	k2eAgInSc6d1
domě	dům	k1gInSc6
děvčata	děvče	k1gNnPc4
předou	příst	k5eAaImIp3nP
na	na	k7c6
kolovrátcích	kolovrátek	k1gInPc6
a	a	k8xC
přitom	přitom	k6eAd1
si	se	k3xPyFc3
vesele	vesele	k6eAd1
prozpěvují	prozpěvovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jen	jen	k6eAd1
Senta	Senta	k1gFnSc1
<g/>
,	,	kIx,
Dalandova	Dalandův	k2eAgFnSc1d1
dcera	dcera	k1gFnSc1
<g/>
,	,	kIx,
si	se	k3xPyFc3
prohlíží	prohlížet	k5eAaImIp3nS
obraz	obraz	k1gInSc4
„	„	k?
<g/>
Bludného	bludný	k2eAgMnSc2d1
Holanďana	Holanďan	k1gMnSc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
o	o	k7c6
kterém	který	k3yRgInSc6,k3yIgInSc6,k3yQgInSc6
ji	on	k3xPp3gFnSc4
její	její	k3xOp3gFnSc1
chůva	chůva	k1gFnSc1
zpívala	zpívat	k5eAaImAgFnS
baladu	balada	k1gFnSc4
a	a	k8xC
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
si	se	k3xPyFc3
nyní	nyní	k6eAd1
sama	sám	k3xTgMnSc4
zpívá	zpívat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cítí	cítit	k5eAaImIp3nS
s	s	k7c7
Holanďanem	Holanďan	k1gMnSc7
soucit	soucit	k1gInSc4
a	a	k8xC
chtěla	chtít	k5eAaImAgFnS
by	by	kYmCp3nS
se	se	k3xPyFc4
stát	stát	k5eAaImF,k5eAaPmF
jeho	jeho	k3xOp3gFnSc7
vykupitelkou	vykupitelka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dívky	dívka	k1gFnSc2
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
smějí	smát	k5eAaImIp3nP
a	a	k8xC
její	její	k3xOp3gFnSc1
chůva	chůva	k1gFnSc1
jí	on	k3xPp3gFnSc3
marně	marně	k6eAd1
napomíná	napomínat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přichází	přicházet	k5eAaImIp3nS
Erik	Erik	k1gMnSc1
<g/>
,	,	kIx,
oznamuje	oznamovat	k5eAaImIp3nS
všem	všecek	k3xTgMnPc3
Dalandův	Dalandův	k2eAgInSc1d1
příjezd	příjezd	k1gInSc1
a	a	k8xC
prosí	prosit	k5eAaImIp3nS
Sentu	Senta	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
za	za	k7c4
něho	on	k3xPp3gInSc4
konečně	konečně	k6eAd1
provdala	provdat	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
však	však	k9
vstoupí	vstoupit	k5eAaPmIp3nS
Daland	Daland	k1gInSc1
s	s	k7c7
Holanďanem	Holanďan	k1gMnSc7
<g/>
,	,	kIx,
mužem	muž	k1gMnSc7
z	z	k7c2
obrazu	obraz	k1gInSc2
<g/>
,	,	kIx,
Senta	Senta	k1gFnSc1
bez	bez	k7c2
váhání	váhání	k1gNnSc2
souhlasí	souhlasit	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
stane	stanout	k5eAaPmIp3nS
Holanďanovou	Holanďanův	k2eAgFnSc7d1
ženou	žena	k1gFnSc7
a	a	k8xC
přísahá	přísahat	k5eAaImIp3nS
mu	on	k3xPp3gMnSc3
věčnou	věčný	k2eAgFnSc4d1
věrnost	věrnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
dějství	dějství	k1gNnSc6
</s>
<s>
Na	na	k7c6
pobřeží	pobřeží	k1gNnSc6
<g/>
,	,	kIx,
u	u	k7c2
kterého	který	k3yIgMnSc2,k3yRgMnSc2,k3yQgMnSc2
kotví	kotvit	k5eAaImIp3nP
vedle	vedle	k7c2
sebe	sebe	k3xPyFc4
dvě	dva	k4xCgFnPc1
lodě	loď	k1gFnPc1
<g/>
,	,	kIx,
Dalandova	Dalandův	k2eAgFnSc1d1
a	a	k8xC
Holanďanova	Holanďanův	k2eAgFnSc1d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
veselý	veselý	k2eAgInSc1d1
zpěv	zpěv	k1gInSc1
Dalandových	Dalandový	k2eAgMnPc2d1
námořníků	námořník	k1gMnPc2
postupně	postupně	k6eAd1
umlčen	umlčet	k5eAaPmNgInS
hrůznými	hrůzný	k2eAgInPc7d1
zvuky	zvuk	k1gInPc7
prokletých	prokletý	k2eAgMnPc2d1
plavců	plavec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Erik	Erik	k1gMnSc1
vyčítá	vyčítat	k5eAaImIp3nS
Sentě	Senta	k1gFnSc3
<g/>
,	,	kIx,
že	že	k8xS
porušila	porušit	k5eAaPmAgFnS
slib	slib	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
mu	on	k3xPp3gMnSc3
dala	dát	k5eAaPmAgFnS
<g/>
,	,	kIx,
když	když	k8xS
její	její	k3xOp3gMnSc1
otec	otec	k1gMnSc1
odjížděl	odjíždět	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Holanďan	Holanďan	k1gMnSc1
jejich	jejich	k3xOp3gInSc4
rozhovor	rozhovor	k1gInSc4
zaslechne	zaslechnout	k5eAaPmIp3nS
a	a	k8xC
vyčte	vyčíst	k5eAaPmIp3nS
Sentě	Senta	k1gFnSc3
<g/>
,	,	kIx,
že	že	k8xS
ani	ani	k8xC
ona	onen	k3xDgFnSc1,k3xPp3gFnSc1
není	být	k5eNaImIp3nS
věrnou	věrný	k2eAgFnSc7d1
ženou	žena	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
jí	on	k3xPp3gFnSc3
nevěří	věřit	k5eNaImIp3nS
<g/>
,	,	kIx,
odmítá	odmítat	k5eAaImIp3nS
její	její	k3xOp3gFnPc4
prosby	prosba	k1gFnPc4
a	a	k8xC
přísahy	přísaha	k1gFnPc4
<g/>
,	,	kIx,
vrací	vracet	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
loď	loď	k1gFnSc4
a	a	k8xC
vyplouvá	vyplouvat	k5eAaImIp3nS
na	na	k7c4
moře	moře	k1gNnSc4
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gNnSc1
prokletí	prokletí	k1gNnSc1
bude	být	k5eAaImBp3nS
pokračovat	pokračovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Senta	Senta	k1gFnSc1
se	se	k3xPyFc4
vyrve	vyrvat	k5eAaPmIp3nS
Dalandovi	Daland	k1gMnSc3
a	a	k8xC
Erikovi	Erik	k1gMnSc3
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
ji	on	k3xPp3gFnSc4
snaží	snažit	k5eAaImIp3nS
zadržet	zadržet	k5eAaPmF
<g/>
,	,	kIx,
a	a	k8xC
se	s	k7c7
slovy	slovo	k1gNnPc7
Anděla	Anděl	k1gMnSc2
svého	svůj	k3xOyFgMnSc2
ty	ty	k3xPp2nSc5
příkaz	příkaz	k1gInSc4
chval	chvála	k1gFnPc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
stojím	stát	k5eAaImIp1nS
věrna	věren	k2eAgNnPc4d1
<g/>
,	,	kIx,
za	za	k7c4
hrob	hrob	k1gInSc4
dál	daleko	k6eAd2
<g/>
!	!	kIx.
</s>
<s desamb="1">
se	se	k3xPyFc4
vrhne	vrhnout	k5eAaPmIp3nS,k5eAaImIp3nS
ze	z	k7c2
skály	skála	k1gFnSc2
do	do	k7c2
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
je	být	k5eAaImIp3nS
Holanďanova	Holanďanův	k2eAgFnSc1d1
kletba	kletba	k1gFnSc1
prolomena	prolomen	k2eAgFnSc1d1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc1
loď	loď	k1gFnSc1
se	se	k3xPyFc4
potopí	potopit	k5eAaPmIp3nS
a	a	k8xC
z	z	k7c2
vln	vlna	k1gFnPc2
se	se	k3xPyFc4
vznáší	vznášet	k5eAaImIp3nS
jeho	jeho	k3xOp3gNnSc1
a	a	k8xC
Sentina	Sentin	k2eAgFnSc1d1
postava	postava	k1gFnSc1
k	k	k7c3
nebesům	nebesa	k1gNnPc3
<g/>
.	.	kIx.
</s>
<s>
Konec	konec	k1gInSc1
části	část	k1gFnSc2
článku	článek	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Hudba	hudba	k1gFnSc1
</s>
<s>
Rukopis	rukopis	k1gInSc1
předehry	předehra	k1gFnSc2
</s>
<s>
Bludný	bludný	k2eAgMnSc1d1
Holanďan	Holanďan	k1gMnSc1
není	být	k5eNaImIp3nS
ještě	ještě	k6eAd1
hudebním	hudební	k2eAgNnSc7d1
dramatem	drama	k1gNnSc7
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
například	například	k6eAd1
Lohengrin	Lohengrin	k1gInSc4
a	a	k8xC
další	další	k2eAgFnPc4d1
Wagnerovy	Wagnerův	k2eAgFnPc4d1
opery	opera	k1gFnPc4
s	s	k7c7
tzv.	tzv.	kA
„	„	k?
<g/>
nekonečnou	konečný	k2eNgFnSc4d1
melodii	melodie	k1gFnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Recitativy	recitativ	k1gInPc1
<g/>
,	,	kIx,
árie	árie	k1gFnPc1
<g/>
,	,	kIx,
duety	duet	k1gInPc1
a	a	k8xC
sbory	sbor	k1gInPc1
jsou	být	k5eAaImIp3nP
ještě	ještě	k9
stále	stále	k6eAd1
jasně	jasně	k6eAd1
oddělené	oddělený	k2eAgFnPc1d1
<g/>
,	,	kIx,
Wagner	Wagner	k1gMnSc1
však	však	k9
již	již	k6eAd1
požadoval	požadovat	k5eAaImAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
opera	opera	k1gFnSc1
hrála	hrát	k5eAaImAgFnS
bez	bez	k7c2
přestávek	přestávka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
se	se	k3xPyFc4
zde	zde	k6eAd1
objevují	objevovat	k5eAaImIp3nP
leitmotivy	leitmotiv	k1gInPc4
a	a	k8xC
dramatický	dramatický	k2eAgInSc4d1
„	„	k?
<g/>
sprechgesang	sprechgesang	k1gInSc1
<g/>
“	“	k?
založený	založený	k2eAgInSc1d1
na	na	k7c6
rytmu	rytmus	k1gInSc6
řeči	řeč	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Předehra	předehra	k1gFnSc1
opery	opera	k1gFnSc2
je	být	k5eAaImIp3nS
velká	velký	k2eAgFnSc1d1
baladická	baladický	k2eAgFnSc1d1
skladba	skladba	k1gFnSc1
a	a	k8xC
stále	stále	k6eAd1
ji	on	k3xPp3gFnSc4
tvoří	tvořit	k5eAaImIp3nS
souhrn	souhrn	k1gInSc1
hudebních	hudební	k2eAgInPc2d1
motivů	motiv	k1gInPc2
<g/>
,	,	kIx,
navozující	navozující	k2eAgFnSc4d1
atmosféru	atmosféra	k1gFnSc4
celého	celý	k2eAgNnSc2d1
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začíná	začínat	k5eAaImIp3nS
bouří	bouř	k1gFnSc7
a	a	k8xC
motivem	motiv	k1gInSc7
Holanďana	Holanďan	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
je	být	k5eAaImIp3nS
vystřídán	vystřídat	k5eAaPmNgMnS
motivem	motiv	k1gInSc7
Sentiny	Sentin	k2eAgFnPc4d1
balady	balada	k1gFnPc4
z	z	k7c2
2	#num#	k4
<g/>
.	.	kIx.
dějství	dějství	k1gNnSc6
<g/>
,	,	kIx,
naznačující	naznačující	k2eAgInSc4d1
soucit	soucit	k1gInSc4
s	s	k7c7
prokletou	prokletý	k2eAgFnSc7d1
bytostí	bytost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrací	vracet	k5eAaImIp3nS
se	se	k3xPyFc4
motiv	motiv	k1gInSc1
Holanďanův	Holanďanův	k2eAgInSc1d1
a	a	k8xC
následně	následně	k6eAd1
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
motiv	motiv	k1gInSc1
zpěvu	zpěv	k1gInSc2
námořníků	námořník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dramatický	dramatický	k2eAgInSc1d1
závěr	závěr	k1gInSc1
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
zjasněn	zjasněn	k2eAgInSc1d1
motivem	motiv	k1gInSc7
Senty	Senta	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1
opera	opera	k1gFnSc1
je	být	k5eAaImIp3nS
impozantní	impozantní	k2eAgFnSc7d1
reprezentací	reprezentace	k1gFnSc7
přírodních	přírodní	k2eAgFnPc2d1
sil	síla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strunné	strunný	k2eAgInPc1d1
nástroje	nástroj	k1gInPc1
reprezentují	reprezentovat	k5eAaImIp3nP
vysoké	vysoký	k2eAgFnPc1d1
vlny	vlna	k1gFnPc1
narážející	narážející	k2eAgFnPc1d1
na	na	k7c4
norské	norský	k2eAgNnSc4d1
pobřeží	pobřeží	k1gNnSc4
<g/>
,	,	kIx,
bouře	bouř	k1gFnPc4
a	a	k8xC
blesky	blesk	k1gInPc4
jsou	být	k5eAaImIp3nP
charakterizovány	charakterizovat	k5eAaBmNgInP
nástroji	nástroj	k1gInPc7
žesťovými	žesťový	k2eAgInPc7d1
<g/>
,	,	kIx,
zejména	zejména	k6eAd1
pozouny	pozouna	k1gFnSc2
a	a	k8xC
trubkami	trubka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgInSc2
se	se	k3xPyFc4
ve	v	k7c6
sboru	sbor	k1gInSc6
námořníků	námořník	k1gMnPc2
a	a	k8xC
ve	v	k7c6
sboru	sbor	k1gInSc6
předoucích	předoucí	k2eAgFnPc2d1
dívek	dívka	k1gFnPc2
<g/>
,	,	kIx,
objevuje	objevovat	k5eAaImIp3nS
ohlas	ohlas	k1gInSc1
lidových	lidový	k2eAgInPc2d1
nápěvů	nápěv	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
Wagnerově	Wagnerův	k2eAgInSc6d1
díle	díl	k1gInSc6
zcela	zcela	k6eAd1
výjimečné	výjimečný	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
K	k	k7c3
nejznámějším	známý	k2eAgNnPc3d3
hudebním	hudební	k2eAgNnPc3d1
číslům	číslo	k1gNnPc3
opery	opera	k1gFnSc2
patří	patřit	k5eAaImIp3nS
<g/>
:	:	kIx,
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
dějství	dějství	k1gNnSc1
<g/>
:	:	kIx,
píseň	píseň	k1gFnSc1
kormidelníka	kormidelník	k1gMnSc2
Mit	Mit	k1gMnSc2
Gewittter	Gewittter	k1gInSc1
und	und	k?
Sturm	Sturm	k1gInSc1
aus	aus	k?
fernem	ferno	k1gNnSc7
Meer	Meera	k1gFnPc2
(	(	kIx(
<g/>
Z	z	k7c2
bouře	bouř	k1gFnSc2
divé	divý	k2eAgFnSc2d1
<g/>
,	,	kIx,
kde	kde	k6eAd1
moře	moře	k1gNnSc2
vládu	vláda	k1gFnSc4
má	mít	k5eAaImIp3nS
<g/>
)	)	kIx)
a	a	k8xC
vstup	vstup	k1gInSc1
Holanďanův	Holanďanův	k2eAgMnSc1d1
Die	Die	k1gMnSc1
Frist	Frist	k1gMnSc1
ist	ist	k?
um	um	k1gInSc1
(	(	kIx(
<g/>
Je	být	k5eAaImIp3nS
lhůta	lhůta	k1gFnSc1
pryč	pryč	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
dějství	dějství	k1gNnSc1
<g/>
:	:	kIx,
píseň	píseň	k1gFnSc1
přadlen	přadlena	k1gFnPc2
Summ	Summ	k1gMnSc1
und	und	k?
brumm	brumm	k1gMnSc1
<g/>
,	,	kIx,
du	du	k?
gutes	gutes	k1gInSc1
Rädchen	Rädchna	k1gFnPc2
(	(	kIx(
<g/>
Huč	hučet	k5eAaImRp2nS
a	a	k8xC
bruč	bručet	k5eAaImRp2nS
můj	můj	k3xOp1gInSc1
kolovrátku	kolovrátek	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Sentina	Sentin	k2eAgFnSc1d1
balada	balada	k1gFnSc1
Johohoe	Johoho	k1gFnSc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
a	a	k8xC
duet	duet	k1gInSc1
Senty	Senta	k1gFnSc2
a	a	k8xC
Holanďana	Holanďan	k1gMnSc2
Wie	Wie	k1gMnSc2
aus	aus	k?
der	drát	k5eAaImRp2nS
Ferne	Fern	k1gInSc5
längst	längst	k5eAaPmF
vergangener	vergangener	k1gMnSc1
Zeiten	Zeiten	k2eAgMnSc1d1
(	(	kIx(
<g/>
Jak	jak	k8xC,k8xS
temné	temný	k2eAgFnPc1d1
dálky	dálka	k1gFnPc1
dávno	dávno	k6eAd1
zašlých	zašlý	k2eAgInPc2d1
časů	čas	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
dějství	dějství	k1gNnSc1
<g/>
:	:	kIx,
sbor	sbor	k1gInSc1
námořníků	námořník	k1gMnPc2
Steuermann	Steuermanno	k1gNnPc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
Lass	Lass	k1gInSc1
die	die	k?
Wacht	Wacht	k1gInSc4
(	(	kIx(
<g/>
Lodivode	lodivod	k1gMnSc5
<g/>
,	,	kIx,
stráže	stráž	k1gFnSc2
nech	nechat	k5eAaPmRp2nS
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
a	a	k8xC
Erikova	Erikův	k2eAgFnSc1d1
cavatina	cavatina	k1gFnSc1
Willst	Willst	k1gInSc1
jenes	jenes	k1gMnSc1
Tags	Tagsa	k1gFnPc2
du	du	k?
nicht	nicht	k1gMnSc1
dich	dich	k1gMnSc1
mehr	mehr	k1gMnSc1
entsinnen	entsinnen	k2eAgMnSc1d1
(	(	kIx(
<g/>
Nechceš	chtít	k5eNaImIp2nS
si	se	k3xPyFc3
na	na	k7c4
onen	onen	k3xDgInSc4
čas	čas	k1gInSc4
vzpomenouti	vzpomenout	k5eAaPmF
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
České	český	k2eAgFnSc2d1
inscenace	inscenace	k1gFnSc2
(	(	kIx(
<g/>
výběr	výběr	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Poprvé	poprvé	k6eAd1
byl	být	k5eAaImAgInS
v	v	k7c6
Praze	Praha	k1gFnSc6
Bludný	bludný	k2eAgMnSc1d1
Holanďan	Holanďan	k1gMnSc1
uveden	uveden	k2eAgInSc1d1
roku	rok	k1gInSc2
1856	#num#	k4
v	v	k7c6
tehdejším	tehdejší	k2eAgNnSc6d1
Stavovském	stavovský	k2eAgNnSc6d1
divadle	divadlo	k1gNnSc6
<g/>
,	,	kIx,
orchestr	orchestr	k1gInSc4
řídil	řídit	k5eAaImAgMnS
František	František	k1gMnSc1
Škroup	Škroup	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
První	první	k4xOgFnSc1
inscenace	inscenace	k1gFnSc1
v	v	k7c6
českém	český	k2eAgInSc6d1
jazyce	jazyk	k1gInSc6
se	se	k3xPyFc4
uskutečnila	uskutečnit	k5eAaPmAgFnS
1	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
roku	rok	k1gInSc2
1906	#num#	k4
ve	v	k7c6
Velkém	velký	k2eAgNnSc6d1
divadle	divadlo	k1gNnSc6
v	v	k7c6
Plzní	Plzeň	k1gFnSc7
v	v	k7c6
překladu	překlad	k1gInSc6
Václava	Václav	k1gMnSc2
Judy	judo	k1gNnPc7
Novotného	Novotný	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
Národním	národní	k2eAgNnSc6d1
divadle	divadlo	k1gNnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
měla	mít	k5eAaImAgFnS
opera	opera	k1gFnSc1
premiéru	premiéra	k1gFnSc4
17	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
roku	rok	k1gInSc2
1907	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Česká	český	k2eAgNnPc1d1
vydání	vydání	k1gNnPc1
libreta	libreto	k1gNnSc2
</s>
<s>
Hudební	hudební	k2eAgInSc1d1
odbor	odbor	k1gInSc1
Umělecké	umělecký	k2eAgFnSc2d1
besedy	beseda	k1gFnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1907	#num#	k4
<g/>
,	,	kIx,
přeložil	přeložit	k5eAaPmAgMnS
Václav	Václav	k1gMnSc1
Juda	judo	k1gNnSc2
Novotný	Novotný	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
SNKLHU	SNKLHU	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1959	#num#	k4
<g/>
,	,	kIx,
přeložili	přeložit	k5eAaPmAgMnP
Pavel	Pavel	k1gMnSc1
Eisner	Eisner	k1gMnSc1
a	a	k8xC
Jaroslav	Jaroslav	k1gMnSc1
Vogel	Vogel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
BRANBERGER	BRANBERGER	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svět	svět	k1gInSc1
v	v	k7c6
opeře	opera	k1gFnSc6
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Orbis	orbis	k1gInSc1
1946	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S-	S-	k1gFnSc1
<g/>
786	#num#	k4
<g/>
-	-	kIx~
<g/>
787.1	787.1	k4
2	#num#	k4
HOLDEN	HOLDEN	kA
<g/>
,	,	kIx,
Amanda	Amanda	k1gFnSc1
(	(	kIx(
<g/>
ed	ed	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Viking	Viking	k1gMnSc1
Opera	opera	k1gFnSc1
Guide	Guid	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
Viking	Viking	k1gMnSc1
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
670	#num#	k4
<g/>
-	-	kIx~
<g/>
81292	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
S.	S.	kA
11791	#num#	k4
2	#num#	k4
ZÖCHLING	ZÖCHLING	kA
<g/>
,	,	kIx,
Dieter	Dieter	k1gInSc1
(	(	kIx(
<g/>
ed	ed	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kronika	kronika	k1gFnSc1
opery	opera	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Fortuna	Fortuna	k1gFnSc1
Print	Printa	k1gFnPc2
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86144	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
<g/>
114	#num#	k4
<g/>
-	-	kIx~
<g/>
115	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
WARRACK	WARRACK	kA
John	John	k1gMnSc1
<g/>
,	,	kIx,
WEST	WEST	kA
<g/>
,	,	kIx,
Evan	Evan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oxfordský	oxfordský	k2eAgInSc1d1
slovník	slovník	k1gInSc1
opery	opera	k1gFnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Iris	iris	k1gInSc1
a	a	k8xC
Knižní	knižní	k2eAgInSc1d1
klub	klub	k1gInSc1
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-85893-14-2	80-85893-14-2	k4
S.	S.	kA
541	#num#	k4
2	#num#	k4
3	#num#	k4
HOSTOMSKÁ	HOSTOMSKÁ	kA
<g/>
,	,	kIx,
Anna	Anna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opera	opera	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průvodce	průvodce	k1gMnSc1
operní	operní	k2eAgFnSc2d1
tvorbou	tvorba	k1gFnSc7
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
SNKLHU	SNKLHU	kA
<g/>
,	,	kIx,
1955	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
S.	S.	kA
294	#num#	k4
<g/>
-	-	kIx~
<g/>
297	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ČT	ČT	kA
<g/>
24	#num#	k4
<g/>
:	:	kIx,
Bludný	bludný	k2eAgMnSc1d1
Holanďan	Holanďan	k1gMnSc1
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
na	na	k7c4
prkna	prkno	k1gNnPc4
Státní	státní	k2eAgFnSc2d1
opery	opera	k1gFnSc2
<g/>
↑	↑	k?
Česká	český	k2eAgFnSc1d1
divadelní	divadelní	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
-	-	kIx~
Novotný	Novotný	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
Juda	judo	k1gNnSc2
<g/>
↑	↑	k?
Archiv	archiv	k1gInSc1
Národního	národní	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
<g/>
:	:	kIx,
Bludný	bludný	k2eAgMnSc1d1
Holanďan	Holanďan	k1gMnSc1
1907	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
PIVODA	PIVODA	kA
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
hudbě	hudba	k1gFnSc6
Wagnerově	Wagnerův	k2eAgFnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
J.	J.	kA
Otto	Otto	k1gMnSc1
<g/>
,	,	kIx,
1881	#num#	k4
<g/>
.	.	kIx.
46	#num#	k4
s.	s.	k?
</s>
<s>
ŠTĚPÁNEK	Štěpánek	k1gMnSc1
<g/>
,	,	kIx,
Miloslav	Miloslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Richard	Richard	k1gMnSc1
Wagner	Wagner	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gNnSc1
dílo	dílo	k1gNnSc1
u	u	k7c2
nás	my	k3xPp1nPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratislava	Bratislava	k1gFnSc1
<g/>
:	:	kIx,
FF	ff	kA
UK	UK	kA
<g/>
,	,	kIx,
1935	#num#	k4
<g/>
.	.	kIx.
43	#num#	k4
s.	s.	k?
</s>
<s>
ČERNÝ	Černý	k1gMnSc1
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
K.	K.	kA
Ohlas	ohlas	k1gInSc1
díla	dílo	k1gNnSc2
R.	R.	kA
Wagnera	Wagner	k1gMnSc2
v	v	k7c6
české	český	k2eAgFnSc6d1
hudební	hudební	k2eAgFnSc6d1
kritice	kritika	k1gFnSc6
let	léto	k1gNnPc2
1847	#num#	k4
<g/>
-	-	kIx~
<g/>
1883	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Hudební	hudební	k2eAgFnSc1d1
věda	věda	k1gFnSc1
22	#num#	k4
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
216	#num#	k4
<g/>
–	–	k?
<g/>
235	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Richard	Richard	k1gMnSc1
Wagner	Wagner	k1gMnSc1
</s>
<s>
Bludný	bludný	k2eAgMnSc1d1
Holanďan	Holanďan	k1gMnSc1
</s>
<s>
Dějiny	dějiny	k1gFnPc1
opery	opera	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Bludný	bludný	k2eAgMnSc1d1
Holanďan	Holanďan	k1gMnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Bludný	bludný	k2eAgMnSc1d1
Holanďan	Holanďan	k1gMnSc1
(	(	kIx(
<g/>
opera	opera	k1gFnSc1
<g/>
)	)	kIx)
v	v	k7c6
databázi	databáze	k1gFnSc6
Archivu	archiv	k1gInSc2
Národního	národní	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
</s>
<s>
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Der	drát	k5eAaImRp2nS
Fliegende	Fliegend	k1gMnSc5
Holländer	Holländra	k1gFnPc2
–	–	k?
Libretto	Libretto	k1gNnSc4
–	–	k?
OperaGlass	OperaGlass	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Český	český	k2eAgInSc1d1
překlad	překlad	k1gInSc1
libreta	libreto	k1gNnSc2
od	od	k7c2
V.	V.	kA
J.	J.	kA
Novotného	Novotný	k1gMnSc2
na	na	k7c6
webu	web	k1gInSc6
Společnosti	společnost	k1gFnSc2
Richarda	Richard	k1gMnSc2
Wagnera	Wagner	k1gMnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Opery	opera	k1gFnSc2
Richarda	Richard	k1gMnSc2
Wagnera	Wagner	k1gMnSc2
</s>
<s>
Víly	víla	k1gFnPc1
(	(	kIx(
<g/>
1834	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Zákaz	zákaz	k1gInSc1
lásky	láska	k1gFnSc2
(	(	kIx(
<g/>
1834	#num#	k4
<g/>
–	–	k?
<g/>
1836	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Rienzi	Rienh	k1gMnPc1
(	(	kIx(
<g/>
1837	#num#	k4
<g/>
–	–	k?
<g/>
1840	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bludný	bludný	k2eAgMnSc1d1
Holanďan	Holanďan	k1gMnSc1
(	(	kIx(
<g/>
1840	#num#	k4
<g/>
–	–	k?
<g/>
1841	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Tannhäuser	Tannhäuser	k1gInSc1
(	(	kIx(
<g/>
1842	#num#	k4
<g/>
–	–	k?
<g/>
1845	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Lohengrin	Lohengrin	k1gInSc1
(	(	kIx(
<g/>
1845	#num#	k4
<g/>
–	–	k?
<g/>
1848	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Tristan	Tristan	k1gInSc1
a	a	k8xC
Isolda	Isolda	k1gFnSc1
(	(	kIx(
<g/>
1856	#num#	k4
<g/>
–	–	k?
<g/>
1859	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Mistři	mistr	k1gMnPc1
pěvci	pěvec	k1gMnPc1
norimberští	norimberský	k2eAgMnPc1d1
(	(	kIx(
<g/>
1845	#num#	k4
<g/>
–	–	k?
<g/>
1867	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Prsten	prsten	k1gInSc1
Nibelungův	Nibelungův	k2eAgInSc1d1
(	(	kIx(
<g/>
1851	#num#	k4
<g/>
–	–	k?
<g/>
1874	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Parsifal	Parsifal	k1gFnSc1
(	(	kIx(
<g/>
1865	#num#	k4
<g/>
–	–	k?
<g/>
1882	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
aun	aun	k?
<g/>
2008478999	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
300169752	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79107248	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
181785317	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79107248	#num#	k4
</s>
