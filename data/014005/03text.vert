<s>
Seznam	seznam	k1gInSc1
největších	veliký	k2eAgNnPc2d3
měst	město	k1gNnPc2
v	v	k7c6
Evropské	evropský	k2eAgFnSc6d1
unii	unie	k1gFnSc6
</s>
<s>
Tento	tento	k3xDgInSc1
seznam	seznam	k1gInSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
45	#num#	k4
největších	veliký	k2eAgNnPc2d3
měst	město	k1gNnPc2
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
jednotlivých	jednotlivý	k2eAgNnPc2d1
měst	město	k1gNnPc2
se	se	k3xPyFc4
považuje	považovat	k5eAaImIp3nS
obyvatelstvo	obyvatelstvo	k1gNnSc1
města	město	k1gNnSc2
v	v	k7c6
jeho	jeho	k3xOp3gFnPc6
administrativních	administrativní	k2eAgFnPc6d1
hranicích	hranice	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nezapočítávají	započítávat	k5eNaImIp3nP
se	se	k3xPyFc4
okolní	okolní	k2eAgMnPc1d1
politicky	politicky	k6eAd1
samostatná	samostatný	k2eAgNnPc4d1
města	město	k1gNnPc4
a	a	k8xC
obce	obec	k1gFnPc4
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
jsou	být	k5eAaImIp3nP
součástí	součást	k1gFnSc7
urbanizovaného	urbanizovaný	k2eAgNnSc2d1
území	území	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Výsledný	výsledný	k2eAgInSc1d1
seznam	seznam	k1gInSc1
je	být	k5eAaImIp3nS
tedy	tedy	k9
velmi	velmi	k6eAd1
ovlivněn	ovlivnit	k5eAaPmNgInS
administrativními	administrativní	k2eAgFnPc7d1
hranicemi	hranice	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgMnPc4
zaragozské	zaragozský	k2eAgMnPc4d1
například	například	k6eAd1
zahrnují	zahrnovat	k5eAaImIp3nP
mnoho	mnoho	k6eAd1
okolního	okolní	k2eAgNnSc2d1
neurbanizovaného	urbanizovaný	k2eNgNnSc2d1
území	území	k1gNnSc2
<g/>
,	,	kIx,
výsledná	výsledný	k2eAgFnSc1d1
hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
je	být	k5eAaImIp3nS
tak	tak	k9
jen	jen	k9
613	#num#	k4
obyvatel	obyvatel	k1gMnPc2
na	na	k7c6
km²	km²	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oproti	oproti	k7c3
tomu	ten	k3xDgNnSc3
administrativní	administrativní	k2eAgFnPc1d1
hranice	hranice	k1gFnPc1
Paříže	Paříž	k1gFnSc2
zahrnují	zahrnovat	k5eAaImIp3nP
jen	jen	k9
centrum	centrum	k1gNnSc4
města	město	k1gNnSc2
(	(	kIx(
<g/>
v	v	k7c6
okolních	okolní	k2eAgNnPc6d1
urbanizovaných	urbanizovaný	k2eAgNnPc6d1
předměstích	předměstí	k1gNnPc6
žije	žít	k5eAaImIp3nS
několikanásobně	několikanásobně	k6eAd1
více	hodně	k6eAd2
obyvatel	obyvatel	k1gMnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
výsledná	výsledný	k2eAgFnSc1d1
hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
je	být	k5eAaImIp3nS
přes	přes	k7c4
20	#num#	k4
000	#num#	k4
obyvatel	obyvatel	k1gMnPc2
na	na	k7c6
km²	km²	k?
<g/>
,	,	kIx,
oproti	oproti	k7c3
Římu	Řím	k1gInSc3
tedy	tedy	k9
více	hodně	k6eAd2
než	než	k8xS
třicetinásobná	třicetinásobný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pořadí	pořadí	k1gNnSc1
v	v	k7c6
tomto	tento	k3xDgInSc6
seznamu	seznam	k1gInSc6
nemusí	muset	k5eNaImIp3nS
být	být	k5eAaImF
vždy	vždy	k6eAd1
zcela	zcela	k6eAd1
přesné	přesný	k2eAgNnSc1d1
<g/>
,	,	kIx,
hlavním	hlavní	k2eAgInSc7d1
důvodem	důvod	k1gInSc7
jsou	být	k5eAaImIp3nP
rozdíly	rozdíl	k1gInPc1
v	v	k7c6
datech	datum	k1gNnPc6
<g/>
,	,	kIx,
ke	k	k7c3
kterým	který	k3yQgFnPc3,k3yRgFnPc3,k3yIgFnPc3
se	se	k3xPyFc4
počty	počet	k1gInPc1
obyvatel	obyvatel	k1gMnPc2
vztahují	vztahovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Seznam	seznam	k1gInSc1
největších	veliký	k2eAgNnPc2d3
měst	město	k1gNnPc2
v	v	k7c6
Evropské	evropský	k2eAgFnSc6d1
unii	unie	k1gFnSc6
</s>
<s>
#	#	kIx~
</s>
<s>
Článek	článek	k1gInSc1
</s>
<s>
Obrázek	obrázek	k1gInSc1
</s>
<s>
stát	stát	k1gInSc1
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
K	k	k7c3
datu	datum	k1gNnSc3
</s>
<s>
Rozloha	rozloha	k1gFnSc1
(	(	kIx(
<g/>
km	km	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
</s>
<s>
Berlín	Berlín	k1gInSc1
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
3	#num#	k4
644	#num#	k4
826	#num#	k4
</s>
<s>
2019-09-30	2019-09-30	k4
</s>
<s>
891	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Madrid	Madrid	k1gInSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
3	#num#	k4
266	#num#	k4
126	#num#	k4
</s>
<s>
2019-01-01	2019-01-01	k4
</s>
<s>
604	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
Řím	Řím	k1gInSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
2	#num#	k4
872	#num#	k4
800	#num#	k4
</s>
<s>
2018-01-01	2018-01-01	k4
</s>
<s>
1287	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
Paříž	Paříž	k1gFnSc1
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
2	#num#	k4
187	#num#	k4
526	#num#	k4
</s>
<s>
2017-01-01	2017-01-01	k4
</s>
<s>
105	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
Bukurešť	Bukurešť	k1gFnSc1
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1
</s>
<s>
1	#num#	k4
883	#num#	k4
425	#num#	k4
</s>
<s>
2011-10-31	2011-10-31	k4
</s>
<s>
226	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Hamburk	Hamburk	k1gInSc1
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
1	#num#	k4
841	#num#	k4
179	#num#	k4
</s>
<s>
2019-09-30	2019-09-30	k4
</s>
<s>
755	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
Vídeň	Vídeň	k1gFnSc1
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
1	#num#	k4
840	#num#	k4
573	#num#	k4
</s>
<s>
2016-01-01	2016-01-01	k4
</s>
<s>
415	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
Varšava	Varšava	k1gFnSc1
</s>
<s>
Polsko	Polsko	k1gNnSc1
</s>
<s>
1	#num#	k4
790	#num#	k4
658	#num#	k4
</s>
<s>
2019-12-31	2019-12-31	k4
</s>
<s>
517	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
Budapešť	Budapešť	k1gFnSc1
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
1	#num#	k4
752	#num#	k4
286	#num#	k4
</s>
<s>
2019-01-01	2019-01-01	k4
</s>
<s>
525	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
Barcelona	Barcelona	k1gFnSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
1	#num#	k4
636	#num#	k4
762	#num#	k4
</s>
<s>
2019-01-01	2019-01-01	k4
</s>
<s>
101	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
Mnichov	Mnichov	k1gInSc1
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
1	#num#	k4
471	#num#	k4
508	#num#	k4
</s>
<s>
2019-09-30	2019-09-30	k4
</s>
<s>
311	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
Sofie	Sofie	k1gFnSc1
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
1	#num#	k4
355	#num#	k4
142	#num#	k4
</s>
<s>
2020-06-15	2020-06-15	k4
</s>
<s>
492	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
Milán	Milán	k1gInSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
1	#num#	k4
351	#num#	k4
562	#num#	k4
</s>
<s>
2017-01-01	2017-01-01	k4
</s>
<s>
182	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
Praha	Praha	k1gFnSc1
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
1	#num#	k4
324	#num#	k4
277	#num#	k4
</s>
<s>
2020-01-01	2020-01-01	k4
</s>
<s>
496	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
Bruselský	bruselský	k2eAgInSc1d1
region	region	k1gInSc1
</s>
<s>
Belgie	Belgie	k1gFnSc1
</s>
<s>
1	#num#	k4
175	#num#	k4
173	#num#	k4
</s>
<s>
2017-01-01	2017-01-01	k4
</s>
<s>
161	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
Dublin	Dublin	k1gInSc1
</s>
<s>
Irsko	Irsko	k1gNnSc1
</s>
<s>
1	#num#	k4
173	#num#	k4
179	#num#	k4
</s>
<s>
2016-04-24	2016-04-24	k4
</s>
<s>
115	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
Kolín	Kolín	k1gInSc1
nad	nad	k7c7
Rýnem	Rýn	k1gInSc7
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
1	#num#	k4
085	#num#	k4
664	#num#	k4
</s>
<s>
2019-09-302019-10-31	2019-09-302019-10-31	k4
</s>
<s>
405	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
Neapol	Neapol	k1gFnSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
972	#num#	k4
212	#num#	k4
</s>
<s>
2016-06-30	2016-06-30	k4
</s>
<s>
119	#num#	k4
</s>
<s>
19	#num#	k4
</s>
<s>
Turín	Turín	k1gInSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
870	#num#	k4
952	#num#	k4
</s>
<s>
2020-01-01	2020-01-01	k4
</s>
<s>
130	#num#	k4
</s>
<s>
20	#num#	k4
</s>
<s>
Marseille	Marseille	k1gFnSc1
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
869	#num#	k4
815	#num#	k4
</s>
<s>
2017-01-01	2017-01-01	k4
</s>
<s>
241	#num#	k4
</s>
<s>
21	#num#	k4
</s>
<s>
Amsterdam	Amsterdam	k1gInSc1
</s>
<s>
NizozemskoNizozemské	nizozemskonizozemský	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
860	#num#	k4
124	#num#	k4
</s>
<s>
2018-01-01	2018-01-01	k4
</s>
<s>
219	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
Valencie	Valencie	k1gFnSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
794	#num#	k4
288	#num#	k4
</s>
<s>
2019-01-01	2019-01-01	k4
</s>
<s>
135	#num#	k4
</s>
<s>
23	#num#	k4
</s>
<s>
Záhřeb	Záhřeb	k1gInSc1
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1
</s>
<s>
790	#num#	k4
017	#num#	k4
</s>
<s>
2011-03-31	2011-03-31	k4
</s>
<s>
641	#num#	k4
</s>
<s>
24	#num#	k4
</s>
<s>
Krakov	Krakov	k1gInSc1
</s>
<s>
Polsko	Polsko	k1gNnSc1
</s>
<s>
766	#num#	k4
739	#num#	k4
</s>
<s>
2017-01-01	2017-01-01	k4
</s>
<s>
327	#num#	k4
</s>
<s>
25	#num#	k4
</s>
<s>
Frankfurt	Frankfurt	k1gInSc1
nad	nad	k7c7
Mohanem	Mohan	k1gInSc7
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
753	#num#	k4
056	#num#	k4
</s>
<s>
2019-09-30	2019-09-30	k4
</s>
<s>
248	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
Riga	Riga	k1gFnSc1
</s>
<s>
Lotyšsko	Lotyšsko	k1gNnSc1
</s>
<s>
698	#num#	k4
529	#num#	k4
</s>
<s>
2016-01-01	2016-01-01	k4
</s>
<s>
304	#num#	k4
</s>
<s>
27	#num#	k4
</s>
<s>
Lodž	Lodž	k1gFnSc1
</s>
<s>
Polsko	Polsko	k1gNnSc1
</s>
<s>
690	#num#	k4
422	#num#	k4
</s>
<s>
2017-12-31	2017-12-31	k4
</s>
<s>
293	#num#	k4
</s>
<s>
28	#num#	k4
</s>
<s>
Sevilla	Sevilla	k1gFnSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
688	#num#	k4
592	#num#	k4
</s>
<s>
2019-01-01	2019-01-01	k4
</s>
<s>
140	#num#	k4
</s>
<s>
29	#num#	k4
</s>
<s>
Zaragoza	Zaragoza	k1gFnSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
674	#num#	k4
997	#num#	k4
</s>
<s>
2019-01-01	2019-01-01	k4
</s>
<s>
974	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
Palermo	Palermo	k1gNnSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
674	#num#	k4
435	#num#	k4
</s>
<s>
2016-01-01	2016-01-01	k4
</s>
<s>
161159	#num#	k4
</s>
<s>
31	#num#	k4
</s>
<s>
Athény	Athéna	k1gFnPc1
</s>
<s>
Řecko	Řecko	k1gNnSc1
</s>
<s>
664	#num#	k4
046	#num#	k4
</s>
<s>
2011-01-01	2011-01-01	k4
</s>
<s>
39	#num#	k4
</s>
<s>
32	#num#	k4
</s>
<s>
Svobodné	svobodný	k2eAgNnSc1d1
hanzovní	hanzovní	k2eAgNnSc1d1
město	město	k1gNnSc1
Brémy	Brémy	k1gFnPc1
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
661	#num#	k4
000	#num#	k4
</s>
<s>
2014-11-30	2014-11-30	k4
</s>
<s>
419	#num#	k4
</s>
<s>
33	#num#	k4
</s>
<s>
Helsinky	Helsinky	k1gFnPc1
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
642	#num#	k4
045	#num#	k4
</s>
<s>
2017-08-31	2017-08-31	k4
</s>
<s>
214	#num#	k4
</s>
<s>
34	#num#	k4
</s>
<s>
Vratislav	Vratislav	k1gMnSc1
</s>
<s>
Polsko	Polsko	k1gNnSc1
</s>
<s>
638	#num#	k4
586	#num#	k4
</s>
<s>
2017-12-31	2017-12-31	k4
</s>
<s>
293	#num#	k4
</s>
<s>
35	#num#	k4
</s>
<s>
Stuttgart	Stuttgart	k1gInSc1
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
634	#num#	k4
830	#num#	k4
</s>
<s>
2019-09-30	2019-09-30	k4
</s>
<s>
207	#num#	k4
</s>
<s>
36	#num#	k4
</s>
<s>
Düsseldorf	Düsseldorf	k1gMnSc1
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
619	#num#	k4
294	#num#	k4
</s>
<s>
2019-09-302019-10-31	2019-09-302019-10-31	k4
</s>
<s>
217	#num#	k4
</s>
<s>
37	#num#	k4
</s>
<s>
Kodaň	Kodaň	k1gFnSc1
</s>
<s>
Dánsko	Dánsko	k1gNnSc1
</s>
<s>
602	#num#	k4
481	#num#	k4
</s>
<s>
2017-01-01	2017-01-01	k4
</s>
<s>
86	#num#	k4
</s>
<s>
38	#num#	k4
</s>
<s>
Göteborg	Göteborg	k1gMnSc1
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
600	#num#	k4
473	#num#	k4
</s>
<s>
2019-12-31	2019-12-31	k4
</s>
<s>
234	#num#	k4
</s>
<s>
39	#num#	k4
</s>
<s>
Lipsko	Lipsko	k1gNnSc1
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
593	#num#	k4
197	#num#	k4
</s>
<s>
2020-04-30	2020-04-30	k4
</s>
<s>
298	#num#	k4
</s>
<s>
40	#num#	k4
</s>
<s>
Dortmund	Dortmund	k1gInSc1
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
587	#num#	k4
010	#num#	k4
</s>
<s>
2019-09-302019-10-31	2019-09-302019-10-31	k4
</s>
<s>
281	#num#	k4
</s>
<s>
41	#num#	k4
</s>
<s>
Essen	Essen	k1gInSc1
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
583	#num#	k4
109	#num#	k4
</s>
<s>
2019-09-30	2019-09-30	k4
</s>
<s>
210	#num#	k4
</s>
<s>
42	#num#	k4
</s>
<s>
Janov	Janov	k1gInSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
580	#num#	k4
097	#num#	k4
</s>
<s>
2018-01-01	2018-01-01	k4
</s>
<s>
240	#num#	k4
</s>
<s>
43	#num#	k4
</s>
<s>
Málaga	Málaga	k1gFnSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
574	#num#	k4
654	#num#	k4
</s>
<s>
2019-01-01	2019-01-01	k4
</s>
<s>
395	#num#	k4
</s>
<s>
44	#num#	k4
</s>
<s>
Vilnius	Vilnius	k1gMnSc1
</s>
<s>
Litva	Litva	k1gFnSc1
</s>
<s>
574	#num#	k4
147	#num#	k4
</s>
<s>
2018-01-01	2018-01-01	k4
</s>
<s>
401	#num#	k4
</s>
<s>
45	#num#	k4
</s>
<s>
Brémy	Brémy	k1gFnPc1
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
569	#num#	k4
352	#num#	k4
</s>
<s>
2019-09-302019-10-31	2019-09-302019-10-31	k4
</s>
<s>
326	#num#	k4
</s>
<s>
Konec	konec	k1gInSc4
automaticky	automaticky	k6eAd1
generovaného	generovaný	k2eAgInSc2d1
seznamu	seznam	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
Wikidata	Wikidata	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Evropská	evropský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
|	|	kIx~
Geografie	geografie	k1gFnSc1
</s>
