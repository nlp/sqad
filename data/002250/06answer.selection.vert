<s>
Král	Král	k1gMnSc1	Král
je	být	k5eAaImIp3nS	být
nejen	nejen	k6eAd1	nejen
hlavou	hlava	k1gFnSc7	hlava
Norského	norský	k2eAgNnSc2d1	norské
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stojí	stát	k5eAaImIp3nS	stát
také	také	k9	také
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
Norské	norský	k2eAgFnSc2d1	norská
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
Norských	norský	k2eAgFnPc2d1	norská
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
a	a	k8xC	a
plní	plnit	k5eAaImIp3nS	plnit
reprezentativní	reprezentativní	k2eAgFnPc4d1	reprezentativní
funkce	funkce	k1gFnPc4	funkce
<g/>
.	.	kIx.	.
</s>
