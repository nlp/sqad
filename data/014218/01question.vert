<s>
Pod	pod	k7c7
jakým	jaký	k3yRgNnSc7,k3yIgNnSc7,k3yQgNnSc7
jménem	jméno	k1gNnSc7
je	být	k5eAaImIp3nS
dnes	dnes	k6eAd1
známá	známý	k2eAgFnSc1d1
anglická	anglický	k2eAgFnSc1d1
matematička	matematička	k1gFnSc1
a	a	k8xC
první	první	k4xOgFnSc1
programátorka	programátorka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
známá	známý	k2eAgFnSc1d1
především	především	k6eAd1
detailním	detailní	k2eAgInSc7d1
popisem	popis	k1gInSc7
fungování	fungování	k1gNnSc2
Babbageova	Babbageův	k2eAgInSc2d1
mechanického	mechanický	k2eAgInSc2d1
počítače	počítač	k1gInSc2
<g/>
?	?	kIx.
</s>