<s>
Ada	Ada	kA
Lovelace	Lovelace	k1gFnSc1
</s>
<s>
Ada	Ada	kA
Lovelace	Lovelace	k1gFnSc1
Rodné	rodný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Augusta	Augusta	k1gMnSc1
Ada	Ada	kA
Byron	Byron	k1gInSc1
Narození	narození	k1gNnSc2
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1815	#num#	k4
<g/>
Londýn	Londýn	k1gInSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1852	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
36	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Marylebone	Marylebon	k1gInSc5
Příčina	příčina	k1gFnSc1
úmrtí	úmrtí	k1gNnPc2
</s>
<s>
rakovina	rakovina	k1gFnSc1
dělohy	děloha	k1gFnSc2
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
kostel	kostel	k1gInSc1
svaté	svatý	k2eAgFnSc2d1
Máří	Máří	k?
Magdaleny	Magdalena	k1gFnSc2
Národnost	národnost	k1gFnSc1
</s>
<s>
Angličané	Angličan	k1gMnPc1
Povolání	povolání	k1gNnSc2
</s>
<s>
matematička	matematička	k1gFnSc1
<g/>
,	,	kIx,
programátorka	programátorka	k1gFnSc1
<g/>
,	,	kIx,
básnířka	básnířka	k1gFnSc1
<g/>
,	,	kIx,
informatička	informatička	k1gFnSc1
<g/>
,	,	kIx,
vynálezkyně	vynálezkyně	k1gFnSc1
<g/>
,	,	kIx,
překladatelka	překladatelka	k1gFnSc1
<g/>
,	,	kIx,
spisovatelka	spisovatelka	k1gFnSc1
a	a	k8xC
inženýrka	inženýrka	k1gFnSc1
Zaměstnavatel	zaměstnavatel	k1gMnSc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
v	v	k7c6
Cambridgi	Cambridge	k1gFnSc6
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
William	William	k6eAd1
King-Noel	King-Noel	k1gMnSc1
<g/>
,	,	kIx,
první	první	k4xOgMnSc1
hrabě	hrabě	k1gMnSc1
z	z	k7c2
Lovelace	Lovelace	k1gFnSc2
(	(	kIx(
<g/>
1835	#num#	k4
<g/>
–	–	k?
<g/>
1852	#num#	k4
<g/>
)	)	kIx)
Děti	dítě	k1gFnPc1
</s>
<s>
Anne	Annat	k5eAaPmIp3nS
Blunt	Blunt	k1gInSc1
<g/>
,	,	kIx,
patnáctá	patnáctý	k4xOgFnSc1
baronka	baronka	k1gFnSc1
WentworthRalph	WentworthRalpha	k1gFnPc2
King-Milbanke	King-Milbanke	k1gFnPc2
<g/>
,	,	kIx,
druhý	druhý	k4xOgMnSc1
hrabě	hrabě	k1gMnSc1
z	z	k7c2
LovelaceByron	LovelaceByron	k1gMnSc1
King-Noel	King-Noel	k1gMnSc1
<g/>
,	,	kIx,
vikomt	vikomt	k1gMnSc1
Ockham	Ockham	k1gInSc1
Rodiče	rodič	k1gMnPc1
</s>
<s>
George	Georg	k1gMnSc2
Gordon	Gordon	k1gMnSc1
Byron	Byron	k1gMnSc1
a	a	k8xC
Anne	Anne	k1gFnSc1
Isabella	Isabello	k1gNnSc2
Byron	Byrona	k1gFnPc2
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Allegra	allegro	k1gNnPc1
Byron	Byron	k1gNnSc1
a	a	k8xC
Elizabeth	Elizabeth	k1gFnSc1
Medora	Medora	k1gFnSc1
Leighová	Leighová	k1gFnSc1
(	(	kIx(
<g/>
sourozenci	sourozenec	k1gMnPc1
<g/>
)	)	kIx)
<g/>
Judith	Judith	k1gInSc1
Blunt-Lytton	Blunt-Lytton	k1gInSc1
<g/>
,	,	kIx,
16	#num#	k4
<g/>
th	th	k?
Baroness	Baroness	k1gInSc1
Wentworth	Wentworth	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
vnučka	vnučka	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Augusta	Augusta	k1gMnSc1
Ada	Ada	kA
King	King	k1gMnSc1
<g/>
,	,	kIx,
hraběnka	hraběnka	k1gFnSc1
z	z	k7c2
Lovelace	Lovelace	k1gFnSc2
(	(	kIx(
<g/>
10	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1815	#num#	k4
Londýn	Londýn	k1gInSc1
–	–	k?
27	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1852	#num#	k4
tamtéž	tamtéž	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
narozená	narozený	k2eAgNnPc1d1
jako	jako	k8xC,k8xS
Augusta	Augusta	k1gMnSc1
Ada	Ada	kA
Byron	Byron	k1gMnSc1
a	a	k8xC
nyní	nyní	k6eAd1
známá	známý	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
Ada	k1gFnSc1	kA
Lovelace	k1gFnSc1	k1gFnSc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
anglická	anglický	k2eAgFnSc1d1
matematička	matematička	k1gFnSc1
a	a	k8xC
první	první	k4xOgFnSc1
programátorka	programátorka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
známá	známý	k2eAgFnSc1d1
především	především	k6eAd1
detailním	detailní	k2eAgInSc7d1
popisem	popis	k1gInSc7
fungování	fungování	k1gNnSc2
Babbageova	Babbageův	k2eAgInSc2d1
mechanického	mechanický	k2eAgInSc2d1
počítače	počítač	k1gInSc2
(	(	kIx(
<g/>
analytického	analytický	k2eAgInSc2d1
stroje	stroj	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
vývoj	vývoj	k1gInSc4
podporovala	podporovat	k5eAaImAgFnS
i	i	k9
finančně	finančně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
jejími	její	k3xOp3gFnPc7
poznámkami	poznámka	k1gFnPc7
k	k	k7c3
analytickému	analytický	k2eAgInSc3d1
stroji	stroj	k1gInSc3
byl	být	k5eAaImAgMnS
i	i	k9
algoritmus	algoritmus	k1gInSc4
Bernoulliho	Bernoulli	k1gMnSc2
čísel	číslo	k1gNnPc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
považován	považován	k2eAgInSc1d1
za	za	k7c4
první	první	k4xOgInSc4
algoritmus	algoritmus	k1gInSc4
zpracovatelný	zpracovatelný	k2eAgInSc4d1
počítačem	počítač	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS
se	s	k7c7
10	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1815	#num#	k4
jako	jako	k8xS,k8xC
jediné	jediný	k2eAgNnSc4d1
legitimní	legitimní	k2eAgNnSc4d1
dítě	dítě	k1gNnSc4
básníka	básník	k1gMnSc2
lorda	lord	k1gMnSc2
Byrona	Byron	k1gMnSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc2
manželky	manželka	k1gFnSc2
Anny	Anna	k1gFnSc2
Isabelly	Isabella	k1gFnSc2
Byronové	Byronová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc1
ostatní	ostatní	k2eAgFnPc1d1
Byronovy	Byronův	k2eAgFnPc1d1
děti	dítě	k1gFnPc1
se	se	k3xPyFc4
narodily	narodit	k5eAaPmAgFnP
mimo	mimo	k7c4
manželství	manželství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byron	Byron	k1gMnSc1
opustil	opustit	k5eAaPmAgMnS
svou	svůj	k3xOyFgFnSc4
ženu	žena	k1gFnSc4
měsíc	měsíc	k1gInSc4
po	po	k7c6
dceřině	dceřin	k2eAgNnSc6d1
narození	narození	k1gNnSc6
a	a	k8xC
za	za	k7c4
další	další	k2eAgInPc4d1
čtyři	čtyři	k4xCgInPc4
měsíce	měsíc	k1gInPc4
opustil	opustit	k5eAaPmAgMnS
nadobro	nadobro	k6eAd1
i	i	k8xC
Anglii	Anglie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
když	když	k8xS
Adě	Adě	k1gFnSc2
bylo	být	k5eAaImAgNnS
osm	osm	k4xCc1
let	léto	k1gNnPc2
<g/>
,	,	kIx,
podlehl	podlehnout	k5eAaPmAgMnS
nemoci	nemoc	k1gFnPc4
během	během	k7c2
Řecké	řecký	k2eAgFnSc2d1
osvobozenecké	osvobozenecký	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skončila	skončit	k5eAaPmAgFnS
tak	tak	k9
v	v	k7c6
opatrovnictví	opatrovnictví	k1gNnSc6
matky	matka	k1gFnSc2
<g/>
,	,	kIx,
spisovatelky	spisovatelka	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
nicméně	nicméně	k8xC
podporovala	podporovat	k5eAaImAgFnS
její	její	k3xOp3gFnSc4
zálibu	záliba	k1gFnSc4
v	v	k7c6
matematice	matematika	k1gFnSc6
a	a	k8xC
logice	logika	k1gFnSc6
(	(	kIx(
<g/>
také	také	k9
jako	jako	k9
součást	součást	k1gFnSc4
pokusu	pokus	k1gInSc2
předejít	předejít	k5eAaPmF
rozvinutí	rozvinutí	k1gNnSc4
„	„	k?
<g/>
šílenství	šílenství	k1gNnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yQgInPc4,k3yIgInPc4
viděla	vidět	k5eAaImAgFnS
v	v	k7c6
jejím	její	k3xOp3gNnSc6
otci	otec	k1gMnPc7
–	–	k?
ovšem	ovšem	k9
na	na	k7c6
otce	otka	k1gFnSc6
nezanevřela	zanevřít	k5eNaPmAgFnS
a	a	k8xC
nechala	nechat	k5eAaPmAgFnS
se	se	k3xPyFc4
vedle	vedle	k7c2
něj	on	k3xPp3gMnSc2
i	i	k9
pohřbít	pohřbít	k5eAaPmF
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
jedna	jeden	k4xCgFnSc1
z	z	k7c2
mála	málo	k4c2
žen	žena	k1gFnPc2
ve	v	k7c6
viktoriánské	viktoriánský	k2eAgFnSc6d1
Anglii	Anglie	k1gFnSc6
proto	proto	k8xC
studovala	studovat	k5eAaImAgFnS
matematiku	matematika	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
prahu	práh	k1gInSc6
dospělosti	dospělost	k1gFnSc2
se	se	k3xPyFc4
díky	díky	k7c3
svému	svůj	k3xOyFgInSc3
talentu	talent	k1gInSc3
seznámila	seznámit	k5eAaPmAgFnS
s	s	k7c7
britským	britský	k2eAgMnSc7d1
matematikem	matematik	k1gMnSc7
Charlesem	Charles	k1gMnSc7
Babbagem	Babbag	k1gMnSc7
a	a	k8xC
konkrétně	konkrétně	k6eAd1
jeho	jeho	k3xOp3gFnSc7
prací	práce	k1gFnSc7
na	na	k7c6
vývoji	vývoj	k1gInSc6
„	„	k?
<g/>
analytického	analytický	k2eAgInSc2d1
stroje	stroj	k1gInSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
začal	začít	k5eAaPmAgMnS
jejich	jejich	k3xOp3gInSc4
dlouhodobý	dlouhodobý	k2eAgInSc4d1
pracovní	pracovní	k2eAgInSc4d1
i	i	k8xC
přátelský	přátelský	k2eAgInSc4d1
vztah	vztah	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1842	#num#	k4
<g/>
–	–	k?
<g/>
1843	#num#	k4
přeložila	přeložit	k5eAaPmAgFnS
článek	článek	k1gInSc4
italského	italský	k2eAgMnSc2d1
vojenského	vojenský	k2eAgMnSc2d1
analytika	analytik	k1gMnSc2
Luigiho	Luigi	k1gMnSc2
Menabrea	Menabreus	k1gMnSc2
o	o	k7c6
tomto	tento	k3xDgInSc6
stroji	stroj	k1gInSc6
a	a	k8xC
doplnila	doplnit	k5eAaPmAgFnS
ho	on	k3xPp3gMnSc4
rozsáhlými	rozsáhlý	k2eAgFnPc7d1
poznámkami	poznámka	k1gFnPc7
(	(	kIx(
<g/>
nadepsanými	nadepsaný	k2eAgFnPc7d1
jednoduše	jednoduše	k6eAd1
Notes	notes	k1gInSc1
–	–	k?
„	„	k?
<g/>
poznámky	poznámka	k1gFnPc4
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
mimo	mimo	k6eAd1
jiné	jiný	k2eAgFnPc1d1
obsahují	obsahovat	k5eAaImIp3nP
první	první	k4xOgMnSc1
známý	známý	k2eAgInSc1d1
počítačový	počítačový	k2eAgInSc1d1
program	program	k1gInSc1
–	–	k?
algoritmus	algoritmus	k1gInSc1
<g/>
,	,	kIx,
určený	určený	k2eAgInSc1d1
k	k	k7c3
provedení	provedení	k1gNnSc3
strojem	stroj	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
příspěvku	příspěvek	k1gInSc2
k	k	k7c3
počátečním	počáteční	k2eAgFnPc3d1
dějinám	dějiny	k1gFnPc3
počítačů	počítač	k1gMnPc2
se	se	k3xPyFc4
ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
poznámkách	poznámka	k1gFnPc6
věnovala	věnovat	k5eAaPmAgFnS,k5eAaImAgFnS
také	také	k6eAd1
vizím	vize	k1gFnPc3
ohledně	ohledně	k7c2
pokročilých	pokročilý	k2eAgFnPc2d1
schopností	schopnost	k1gFnPc2
počítačů	počítač	k1gMnPc2
<g/>
:	:	kIx,
skládání	skládání	k1gNnSc1
hudby	hudba	k1gFnSc2
a	a	k8xC
kreslení	kreslení	k1gNnSc2
obrazů	obraz	k1gInPc2
a	a	k8xC
využití	využití	k1gNnSc2
techniky	technika	k1gFnSc2
ke	k	k7c3
spolupráci	spolupráce	k1gFnSc3
jednotlivců	jednotlivec	k1gMnPc2
i	i	k8xC
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ada	Ada	kA
popisovala	popisovat	k5eAaImAgFnS
svou	svůj	k3xOyFgFnSc4
metodu	metoda	k1gFnSc4
jako	jako	k8xS,k8xC
„	„	k?
<g/>
poetickou	poetický	k2eAgFnSc4d1
vědu	věda	k1gFnSc4
<g/>
“	“	k?
a	a	k8xC
sebe	sebe	k3xPyFc4
jako	jako	k9
„	„	k?
<g/>
analytičku	analytička	k1gFnSc4
(	(	kIx(
<g/>
a	a	k8xC
metafyzičku	metafyzička	k1gFnSc4
<g/>
)	)	kIx)
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
její	její	k3xOp3gFnSc6
počest	počest	k1gFnSc6
byl	být	k5eAaImAgInS
pojmenován	pojmenovat	k5eAaPmNgInS
programovací	programovací	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
Ada	Ada	kA
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
vznikl	vzniknout	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1979	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
zvolil	zvolit	k5eAaPmAgInS
zkratku	zkratka	k1gFnSc4
ADA	Ada	kA
americký	americký	k2eAgMnSc1d1
matematik	matematik	k1gMnSc1
<g/>
,	,	kIx,
programátor	programátor	k1gMnSc1
a	a	k8xC
generální	generální	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
IOHK	IOHK	kA
Charles	Charles	k1gMnSc1
Hoskinson	Hoskinson	k1gMnSc1
jako	jako	k8xS,k8xC
obchodní	obchodní	k2eAgFnSc4d1
zkratku	zkratka	k1gFnSc4
své	svůj	k3xOyFgFnPc4
kryptoměny	kryptoměn	k2eAgFnPc4d1
Cardano	Cardana	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fragmenty	fragment	k1gInPc1
této	tento	k3xDgFnSc2
mince	mince	k1gFnSc2
jsou	být	k5eAaImIp3nP
pak	pak	k6eAd1
nazývány	nazývat	k5eAaImNgFnP
Lovelace	Lovelace	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Dětství	dětství	k1gNnSc1
</s>
<s>
Ada	Ada	kA
Lovelace	Lovelace	k1gFnSc1
se	se	k3xPyFc4
narodila	narodit	k5eAaPmAgFnS
jako	jako	k8xC,k8xS
Augusta	Augusta	k1gMnSc1
Ada	Ada	kA
Byron	Byron	k1gMnSc1
10	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1815	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
dcerou	dcera	k1gFnSc7
slavného	slavný	k2eAgMnSc2d1
anglického	anglický	k2eAgMnSc2d1
básníka	básník	k1gMnSc2
George	Georg	k1gMnSc2
Gordona	Gordon	k1gMnSc2
Byrona	Byron	k1gMnSc2
a	a	k8xC
Annabelle	Annabelle	k1gFnSc2
Milbank	Milbanka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Augusta	Augusta	k1gMnSc1
byla	být	k5eAaImAgFnS
pojmenována	pojmenovat	k5eAaPmNgFnS
po	po	k7c6
Byronově	Byronův	k2eAgFnSc6d1
nevlastní	vlastní	k2eNgFnSc6d1
sestře	sestra	k1gFnSc6
Augustě	Augusta	k1gFnSc3
Leigh	Leigh	k1gMnSc1
a	a	k8xC
Byron	Byron	k1gMnSc1
používal	používat	k5eAaImAgMnS
její	její	k3xOp3gNnSc4
druhé	druhý	k4xOgNnSc4
jméno	jméno	k1gNnSc4
<g/>
,	,	kIx,
Ada	Ada	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ada	Ada	kA
Lovelaceová	Lovelaceová	k1gFnSc1
ve	v	k7c6
čtyřech	čtyři	k4xCgFnPc6
letechMéně	letechMéně	k6eAd1
než	než	k8xS
za	za	k7c4
rok	rok	k1gInSc4
po	po	k7c6
jejím	její	k3xOp3gNnSc6
narození	narození	k1gNnSc6
se	se	k3xPyFc4
otec	otec	k1gMnSc1
rozhodl	rozhodnout	k5eAaPmAgMnS
pro	pro	k7c4
rozvod	rozvod	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sám	sám	k3xTgMnSc1
byl	být	k5eAaImAgMnS
znepokojen	znepokojit	k5eAaPmNgMnS
skutečností	skutečnost	k1gFnSc7
<g/>
,	,	kIx,
že	že	k8xS
nepřivedl	přivést	k5eNaPmAgMnS
na	na	k7c4
svět	svět	k1gInSc4
syna	syn	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Přestože	přestože	k8xS
podle	podle	k7c2
tehdejších	tehdejší	k2eAgMnPc2d1
britských	britský	k2eAgMnPc2d1
zákonů	zákon	k1gInPc2
měl	mít	k5eAaImAgInS
na	na	k7c6
výchovu	výchov	k1gInSc6
dcery	dcera	k1gFnSc2
právo	právo	k1gNnSc4
především	především	k9
on	on	k3xPp3gMnSc1
<g/>
,	,	kIx,
neprojevil	projevit	k5eNaPmAgInS
o	o	k7c4
výchovu	výchova	k1gFnSc4
žádný	žádný	k3yNgInSc4
zájem	zájem	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
požádal	požádat	k5eAaPmAgMnS
svou	svůj	k3xOyFgFnSc4
sestru	sestra	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
ho	on	k3xPp3gInSc4
informovala	informovat	k5eAaBmAgFnS
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
daří	dařit	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Sama	Sam	k1gMnSc4
svého	svůj	k3xOyFgMnSc4
otce	otec	k1gMnSc4
již	již	k6eAd1
nikdy	nikdy	k6eAd1
poté	poté	k6eAd1
nespatřila	spatřit	k5eNaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1824	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
jí	on	k3xPp3gFnSc3
bylo	být	k5eAaImAgNnS
osm	osm	k4xCc4
let	léto	k1gNnPc2
a	a	k8xC
její	její	k3xOp3gFnSc1
matka	matka	k1gFnSc1
tak	tak	k6eAd1
zůstala	zůstat	k5eAaPmAgFnS
jedinou	jediný	k2eAgFnSc7d1
rodičovskou	rodičovský	k2eAgFnSc7d1
figurou	figura	k1gFnSc7
v	v	k7c6
jejím	její	k3xOp3gInSc6
životě	život	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Až	až	k6eAd1
do	do	k7c2
svých	svůj	k3xOyFgNnPc2
dvaceti	dvacet	k4xCc2
let	léto	k1gNnPc2
měla	mít	k5eAaImAgNnP
zakázáno	zakázat	k5eAaPmNgNnS
podívat	podívat	k5eAaPmF,k5eAaImF
se	se	k3xPyFc4
na	na	k7c4
jakýkoliv	jakýkoliv	k3yIgInSc4
portrét	portrét	k1gInSc4
svého	svůj	k3xOyFgMnSc2
otce	otec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Matka	matka	k1gFnSc1
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1856	#num#	k4
stala	stát	k5eAaPmAgFnS
baronkou	baronka	k1gFnSc7
z	z	k7c2
Wenworthu	Wenworth	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
dcerou	dcera	k1gFnSc7
neměla	mít	k5eNaImAgFnS
blízký	blízký	k2eAgInSc4d1
vztah	vztah	k1gInSc4
<g/>
,	,	kIx,
když	když	k8xS
ji	on	k3xPp3gFnSc4
často	často	k6eAd1
ponechávala	ponechávat	k5eAaImAgFnS
v	v	k7c6
péči	péče	k1gFnSc6
babičky	babička	k1gFnSc2
<g/>
,	,	kIx,
Judith	Judith	k1gMnSc1
<g/>
,	,	kIx,
Lady	lady	k1gFnSc1
Milbankeové	Milbankeová	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
si	se	k3xPyFc3
vnučku	vnučka	k1gFnSc4
zamilovala	zamilovat	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
společenským	společenský	k2eAgInPc3d1
postojům	postoj	k1gInPc3
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
při	při	k7c6
odloučení	odloučení	k1gNnSc6
rodičů	rodič	k1gMnPc2
vždy	vždy	k6eAd1
stranily	stranit	k5eAaImAgInP
otci	otec	k1gMnSc3
dítěte	dítě	k1gNnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
však	však	k9
musela	muset	k5eAaImAgFnS
na	na	k7c6
veřejnosti	veřejnost	k1gFnSc6
prezentovat	prezentovat	k5eAaBmF
jako	jako	k9
milující	milující	k2eAgFnSc1d1
matka	matka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
zahrnovalo	zahrnovat	k5eAaImAgNnS
psaní	psaní	k1gNnSc4
dopisů	dopis	k1gInPc2
Lady	Lada	k1gFnSc2
Milbankeové	Milbankeová	k1gFnSc2
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yIgInPc6,k3yQgInPc6,k3yRgInPc6
se	se	k3xPyFc4
starostlivě	starostlivě	k6eAd1
vyptávala	vyptávat	k5eAaImAgFnS
na	na	k7c4
zdraví	zdraví	k1gNnSc4
své	svůj	k3xOyFgFnSc2
dcery	dcera	k1gFnSc2
a	a	k8xC
na	na	k7c6
jejich	jejich	k3xOp3gInSc6
konci	konec	k1gInSc6
matce	matka	k1gFnSc3
připomínala	připomínat	k5eAaImAgFnS
<g/>
,	,	kIx,
ať	ať	k8xS,k8xC
je	on	k3xPp3gMnPc4
neztratí	ztratit	k5eNaPmIp3nP
<g/>
,	,	kIx,
pro	pro	k7c4
případ	případ	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
musela	muset	k5eAaImAgFnS
dokazovat	dokazovat	k5eAaImF
svou	svůj	k3xOyFgFnSc4
řádnou	řádný	k2eAgFnSc4d1
starost	starost	k1gFnSc4
o	o	k7c4
dceru	dcera	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jednom	jeden	k4xCgInSc6
z	z	k7c2
dopisů	dopis	k1gInPc2
ji	on	k3xPp3gFnSc4
popsala	popsat	k5eAaPmAgFnS
jako	jako	k8xC,k8xS
„	„	k?
<g/>
to	ten	k3xDgNnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
období	období	k1gNnSc6
dospívání	dospívání	k1gNnSc2
byla	být	k5eAaImAgFnS
sledována	sledovat	k5eAaImNgFnS
několika	několik	k4yIc7
blízkými	blízký	k2eAgFnPc7d1
přítelkyněmi	přítelkyně	k1gFnPc7
matky	matka	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
ujistily	ujistit	k5eAaPmAgFnP
<g/>
,	,	kIx,
že	že	k8xS
nevykazuje	vykazovat	k5eNaImIp3nS
známky	známka	k1gFnPc4
morální	morální	k2eAgFnSc2d1
nekázně	nekázeň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těmto	tento	k3xDgFnPc3
pozorovatelkám	pozorovatelka	k1gFnPc3
říkala	říkat	k5eAaImAgFnS
fúrie	fúrie	k1gFnSc1
a	a	k8xC
později	pozdě	k6eAd2
si	se	k3xPyFc3
stěžovala	stěžovat	k5eAaImAgFnS
<g/>
,	,	kIx,
že	že	k8xS
přeháněly	přehánět	k5eAaImAgFnP
a	a	k8xC
vymýšlely	vymýšlet	k5eAaImAgFnP
si	se	k3xPyFc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
17	#num#	k4
<g/>
letá	letý	k2eAgFnSc1d1
Ada	Ada	kA
LovelaceováOd	LovelaceováOd	k1gInSc1
raného	raný	k2eAgNnSc2d1
dětství	dětství	k1gNnSc2
byla	být	k5eAaImAgFnS
často	často	k6eAd1
nemocná	nemocný	k2eAgFnSc1d1,k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
věku	věk	k1gInSc6
osmi	osm	k4xCc2
let	léto	k1gNnPc2
trpěla	trpět	k5eAaImAgFnS
migrénami	migréna	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
ji	on	k3xPp3gFnSc4
oslepovaly	oslepovat	k5eAaImAgInP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
červnu	červen	k1gInSc6
1829	#num#	k4
byla	být	k5eAaImAgFnS
paralyzována	paralyzován	k2eAgFnSc1d1
po	po	k7c6
protrpění	protrpění	k1gNnSc6
spalniček	spalničky	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
byla	být	k5eAaImAgFnS
nucena	nutit	k5eAaImNgFnS
k	k	k7c3
odpočinku	odpočinek	k1gInSc3
na	na	k7c6
lůžku	lůžko	k1gNnSc6
v	v	k7c6
délce	délka	k1gFnSc6
téměř	téměř	k6eAd1
jednoho	jeden	k4xCgInSc2
roku	rok	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
pravděpodobně	pravděpodobně	k6eAd1
oddálilo	oddálit	k5eAaPmAgNnS
její	její	k3xOp3gFnSc4
uzdravení	uzdravení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1831	#num#	k4
už	už	k9
byla	být	k5eAaImAgFnS
schopna	schopen	k2eAgFnSc1d1
pohybu	pohyb	k1gInSc2
o	o	k7c6
berlích	berle	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Navzdory	navzdory	k7c3
častým	častý	k2eAgFnPc3d1
nemocem	nemoc	k1gFnPc3
rozvíjela	rozvíjet	k5eAaImAgFnS
své	svůj	k3xOyFgFnPc4
matematické	matematický	k2eAgFnPc4d1
a	a	k8xC
technické	technický	k2eAgFnPc4d1
dovednosti	dovednost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
dvanácti	dvanáct	k4xCc6
letech	léto	k1gNnPc6
se	se	k3xPyFc4
rozhodla	rozhodnout	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
chtěla	chtít	k5eAaImAgFnS
létat	létat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupovala	postupovat	k5eAaImAgFnS
rozumně	rozumně	k6eAd1
a	a	k8xC
metodicky	metodicky	k6eAd1
<g/>
,	,	kIx,
s	s	k7c7
představivostí	představivost	k1gFnSc7
a	a	k8xC
vášní	vášeň	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgInSc7
krokem	krok	k1gInSc7
<g/>
,	,	kIx,
v	v	k7c6
únoru	únor	k1gInSc6
1828	#num#	k4
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
vyrobení	vyrobení	k1gNnSc3
křídel	křídlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvažovala	zvažovat	k5eAaImAgFnS
různé	různý	k2eAgFnPc4d1
velikosti	velikost	k1gFnPc4
a	a	k8xC
materiály	materiál	k1gInPc4
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
křídel	křídlo	k1gNnPc2
<g/>
,	,	kIx,
například	například	k6eAd1
papír	papír	k1gInSc1
<g/>
,	,	kIx,
hedvábí	hedvábí	k1gNnSc1
napuštěné	napuštěný	k2eAgNnSc1d1
olejem	olej	k1gInSc7
<g/>
,	,	kIx,
dráty	drát	k1gInPc7
a	a	k8xC
peří	peří	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studovala	studovat	k5eAaImAgFnS
anatomii	anatomie	k1gFnSc4
ptáků	pták	k1gMnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
dokázala	dokázat	k5eAaPmAgFnS
odhadnout	odhadnout	k5eAaPmF
správné	správný	k2eAgFnPc4d1
proporce	proporce	k1gFnPc4
křídel	křídlo	k1gNnPc2
a	a	k8xC
těla	tělo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhodovala	rozhodovat	k5eAaImAgFnS
se	se	k3xPyFc4
<g/>
,	,	kIx,
jaké	jaký	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
vybavení	vybavení	k1gNnSc1
bude	být	k5eAaImBp3nS
potřebovat	potřebovat	k5eAaImF
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
například	například	k6eAd1
kompas	kompas	k1gInSc4
k	k	k7c3
nalezení	nalezení	k1gNnSc3
té	ten	k3xDgFnSc2
nejpřímočařejší	přímočarý	k2eAgFnSc2d3
trasy	trasa	k1gFnSc2
přes	přes	k7c4
hory	hora	k1gFnPc4
<g/>
,	,	kIx,
řeky	řeka	k1gFnPc4
a	a	k8xC
údolí	údolí	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posledním	poslední	k2eAgInSc7d1
krokem	krok	k1gInSc7
by	by	kYmCp3nS
byla	být	k5eAaImAgFnS
integrace	integrace	k1gFnSc1
páry	pára	k1gFnSc2
jako	jako	k8xC,k8xS
pohonu	pohon	k1gInSc2
k	k	k7c3
létání	létání	k1gNnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1833	#num#	k4
měla	mít	k5eAaImAgFnS
románek	románek	k1gInSc4
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
poručníkem	poručník	k1gMnSc7
a	a	k8xC
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
byli	být	k5eAaImAgMnP
přistiženi	přistihnout	k5eAaPmNgMnP
<g/>
,	,	kIx,
s	s	k7c7
ním	on	k3xPp3gMnSc7
chtěla	chtít	k5eAaImAgFnS
utéci	utéct	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc3
příbuzní	příbuzný	k1gMnPc1
ji	on	k3xPp3gFnSc4
ale	ale	k9
poznali	poznat	k5eAaPmAgMnP
a	a	k8xC
kontaktovali	kontaktovat	k5eAaImAgMnP
její	její	k3xOp3gFnSc4
matku	matka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anabelle	Anabelle	k1gInSc1
se	se	k3xPyFc4
s	s	k7c7
pomocí	pomoc	k1gFnSc7
svých	svůj	k3xOyFgMnPc2
přátel	přítel	k1gMnPc2
podařilo	podařit	k5eAaPmAgNnS
aféru	aféra	k1gFnSc4
ututlat	ututlat	k5eAaPmF
a	a	k8xC
zabránit	zabránit	k5eAaPmF
tak	tak	k6eAd1
skandálu	skandál	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nikdy	nikdy	k6eAd1
se	se	k3xPyFc4
nesetkala	setkat	k5eNaPmAgFnS
s	s	k7c7
nevlastní	vlastní	k2eNgFnSc7d1
sestrou	sestra	k1gFnSc7
<g/>
,	,	kIx,
Allegrou	Allegrý	k2eAgFnSc7d1
Byronovou	Byronová	k1gFnSc7
<g/>
,	,	kIx,
dcerou	dcera	k1gFnSc7
Lorda	lord	k1gMnSc2
Byrona	Byron	k1gMnSc2
a	a	k8xC
Claire	Clair	k1gInSc5
Clairmontové	Clairmontový	k2eAgMnPc4d1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
zemřela	zemřít	k5eAaPmAgFnS
ve	v	k7c6
věku	věk	k1gInSc6
pěti	pět	k4xCc2
let	léto	k1gNnPc2
v	v	k7c6
roce	rok	k1gInSc6
1822	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jisté	jistý	k2eAgFnSc6d1
míře	míra	k1gFnSc6
se	se	k3xPyFc4
ale	ale	k8xC
stýkala	stýkat	k5eAaImAgFnS
s	s	k7c7
Elizabeth	Elizabeth	k1gFnSc7
Medorou	Medora	k1gFnSc7
Leighovou	Leighová	k1gFnSc7
<g/>
,	,	kIx,
dcerou	dcera	k1gFnSc7
Byronovy	Byronův	k2eAgFnSc2d1
nevlastní	vlastní	k2eNgFnSc2d1
sestry	sestra	k1gFnSc2
Augusty	Augusta	k1gMnSc2
Leighové	Leighová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
byla	být	k5eAaImAgFnS
Ada	Ada	kA
představena	představit	k5eAaPmNgFnS
u	u	k7c2
dvora	dvůr	k1gInSc2
<g/>
,	,	kIx,
Augusta	Augusta	k1gMnSc1
se	se	k3xPyFc4
jí	jíst	k5eAaImIp3nS
stranila	stranit	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s>
Dospělost	dospělost	k1gFnSc1
</s>
<s>
V	v	k7c6
dospělosti	dospělost	k1gFnSc6
poznala	poznat	k5eAaPmAgFnS
Mary	Mary	k1gFnSc1
Somervilleovou	Somervilleův	k2eAgFnSc7d1
z	z	k7c2
Královské	královský	k2eAgFnSc2d1
astronomické	astronomický	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
považovala	považovat	k5eAaImAgFnS
za	za	k7c4
svůj	svůj	k3xOyFgInSc4
vzor	vzor	k1gInSc4
<g/>
,	,	kIx,
především	především	k9
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
byla	být	k5eAaImAgFnS
také	také	k9
matematička	matematička	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Somervilleová	Somervilleová	k1gFnSc1
ji	on	k3xPp3gFnSc4
také	také	k6eAd1
uvedla	uvést	k5eAaPmAgFnS
do	do	k7c2
společnosti	společnost	k1gFnSc2
Charlese	Charles	k1gMnSc2
Babbage	Babbag	k1gMnSc2
<g/>
,	,	kIx,
se	s	k7c7
kterým	který	k3yIgMnSc7,k3yRgMnSc7,k3yQgMnSc7
se	se	k3xPyFc4
seznámila	seznámit	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1833	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
si	se	k3xPyFc3
oba	dva	k4xCgMnPc1
intenzivně	intenzivně	k6eAd1
dopisovali	dopisovat	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lovelaceová	Lovelaceová	k1gFnSc1
se	se	k3xPyFc4
znala	znát	k5eAaImAgFnS
také	také	k9
s	s	k7c7
dalšími	další	k2eAgFnPc7d1
významnými	významný	k2eAgFnPc7d1
osobnostmi	osobnost	k1gFnPc7
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
byli	být	k5eAaImAgMnP
například	například	k6eAd1
Andrew	Andrew	k1gMnPc1
Crosse	Crosse	k1gFnSc2
<g/>
,	,	kIx,
Sir	sir	k1gMnSc1
David	David	k1gMnSc1
Brewster	Brewster	k1gMnSc1
<g/>
,	,	kIx,
Charles	Charles	k1gMnSc1
Wheatstone	Wheatston	k1gInSc5
<g/>
,	,	kIx,
Charles	Charles	k1gMnSc1
Dickens	Dickens	k1gInSc1
a	a	k8xC
Michael	Michael	k1gMnSc1
Faraday	Faradaa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ada	Ada	kA
Lovelaceová	Lovelaceová	k1gFnSc1
ve	v	k7c6
dvaceti	dvacet	k4xCc6
jedna	jeden	k4xCgFnSc1
letech	léto	k1gNnPc6
</s>
<s>
Kolem	kolem	k7c2
roku	rok	k1gInSc2
1834	#num#	k4
byla	být	k5eAaImAgFnS
často	často	k6eAd1
vídána	vídat	k5eAaImNgFnS
u	u	k7c2
dvora	dvůr	k1gInSc2
a	a	k8xC
začala	začít	k5eAaPmAgFnS
se	se	k3xPyFc4
účastnit	účastnit	k5eAaImF
společenských	společenský	k2eAgFnPc2d1
událostí	událost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
chodila	chodit	k5eAaImAgFnS
tančit	tančit	k5eAaImF
a	a	k8xC
svým	svůj	k3xOyFgInSc7
šarmem	šarm	k1gInSc7
dokázala	dokázat	k5eAaPmAgFnS
okouzlit	okouzlit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
lidí	člověk	k1gMnPc2
ji	on	k3xPp3gFnSc4
popisovala	popisovat	k5eAaImAgFnS
jako	jako	k8xS,k8xC
líbeznou	líbezný	k2eAgFnSc4d1
dívku	dívka	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
přítel	přítel	k1gMnSc1
Lorda	lord	k1gMnSc4
Byrona	Byron	k1gMnSc4
<g/>
,	,	kIx,
John	John	k1gMnSc1
Hobhouse	Hobhouse	k1gFnSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
opačného	opačný	k2eAgInSc2d1
názoru	názor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednou	jednou	k6eAd1
ji	on	k3xPp3gFnSc4
popsal	popsat	k5eAaPmAgMnS
jako	jako	k9
„	„	k?
<g/>
…	…	k?
<g/>
mohutnou	mohutný	k2eAgFnSc4d1
ženu	žena	k1gFnSc4
s	s	k7c7
hroší	hroší	k2eAgFnSc7d1
kůží	kůže	k1gFnSc7
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
ovšem	ovšem	k9
sdílí	sdílet	k5eAaImIp3nS
některé	některý	k3yIgInPc4
rysy	rys	k1gInPc4
svého	svůj	k1gMnSc4
otce	otec	k1gMnSc4
<g/>
,	,	kIx,
obzvláště	obzvláště	k6eAd1
ústa	ústa	k1gNnPc4
<g/>
.	.	kIx.
<g/>
“	“	k?
Takto	takto	k6eAd1
se	se	k3xPyFc4
o	o	k7c6
ní	on	k3xPp3gFnSc6
zmínil	zmínit	k5eAaPmAgMnS
po	po	k7c6
jejich	jejich	k3xOp3gNnSc6
prvním	první	k4xOgNnSc6
setkání	setkání	k1gNnSc6
24	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1834	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
dala	dát	k5eAaPmAgFnS
najevo	najevo	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc2
Hobhouse	Hobhouse	k1gFnSc2
nezamlouvá	zamlouvat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravděpodobně	pravděpodobně	k6eAd1
zde	zde	k6eAd1
sehrál	sehrát	k5eAaPmAgMnS
svou	svůj	k3xOyFgFnSc4
roli	role	k1gFnSc4
vliv	vliv	k1gInSc4
její	její	k3xOp3gFnSc2
matky	matka	k1gFnSc2
<g/>
,	,	kIx,
díky	díky	k7c3
níž	jenž	k3xRgFnSc3
neměla	mít	k5eNaImAgFnS
dobré	dobrý	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
s	s	k7c7
žádným	žádný	k3yNgMnSc7
z	z	k7c2
přátel	přítel	k1gMnPc2
svého	svůj	k1gMnSc2
otce	otec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevraživost	nevraživost	k1gFnSc1
vůči	vůči	k7c3
Hobhousovi	Hobhous	k1gMnSc3
ovšem	ovšem	k9
netrvala	trvat	k5eNaImAgFnS
dlouho	dlouho	k6eAd1
a	a	k8xC
později	pozdě	k6eAd2
se	se	k3xPyFc4
stali	stát	k5eAaPmAgMnP
přáteli	přítel	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
Matematiku	matematika	k1gFnSc4
studovala	studovat	k5eAaImAgFnS
na	na	k7c6
Londýnské	londýnský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Svá	svůj	k3xOyFgNnPc4
studia	studio	k1gNnPc4
přerušila	přerušit	k5eAaPmAgFnS
pouze	pouze	k6eAd1
kvůli	kvůli	k7c3
svatbě	svatba	k1gFnSc3
a	a	k8xC
také	také	k9
kvůli	kvůli	k7c3
rodině	rodina	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vdala	vdát	k5eAaPmAgFnS
se	se	k3xPyFc4
za	za	k7c2
barona	baron	k1gMnSc2
Williama	William	k1gMnSc2
Kinga	King	k1gMnSc2
8	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1835	#num#	k4
a	a	k8xC
sama	sám	k3xTgFnSc1
se	se	k3xPyFc4
tak	tak	k6eAd1
stala	stát	k5eAaPmAgFnS
baronkou	baronka	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Jejich	jejich	k3xOp3gNnSc7
sídlem	sídlo	k1gNnSc7
byly	být	k5eAaImAgFnP
rozsáhlé	rozsáhlý	k2eAgFnPc1d1
pozemky	pozemka	k1gFnPc1
Ockham	Ockham	k1gInSc1
Parku	park	k1gInSc3
v	v	k7c4
Surrey	Surrea	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlastnili	vlastnit	k5eAaImAgMnP
také	také	k9
další	další	k2eAgInPc4d1
pozemky	pozemek	k1gInPc4
u	u	k7c2
jezera	jezero	k1gNnSc2
Loch	loch	k1gInSc4
Torridon	Torridon	k1gNnSc1
a	a	k8xC
dům	dům	k1gInSc1
v	v	k7c6
Londýně	Londýn	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Líbánky	líbánky	k1gInPc4
strávili	strávit	k5eAaPmAgMnP
v	v	k7c6
panství	panství	k1gNnSc6
Worthy	Wortha	k1gFnSc2
Manor	Manora	k1gFnPc2
blízko	blízko	k7c2
městečka	městečko	k1gNnSc2
Porlock	Porlock	k1gMnSc1
Weir	Weir	k1gMnSc1
v	v	k7c6
Somersetu	Somerset	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
bylo	být	k5eAaImAgNnS
vyspraveno	vyspraven	k2eAgNnSc1d1
a	a	k8xC
stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
jejich	jejich	k3xOp3gNnSc7
letním	letní	k2eAgNnSc7d1
sídlem	sídlo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Byla	být	k5eAaImAgFnS
matkou	matka	k1gFnSc7
tří	tři	k4xCgFnPc2
dětí	dítě	k1gFnPc2
<g/>
:	:	kIx,
Byrona	Byrona	k1gFnSc1
(	(	kIx(
<g/>
1837	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Anne	Anne	k1gFnSc1
Isabelly	Isabella	k1gFnSc2
(	(	kIx(
<g/>
1837	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Ralpha	Ralph	k1gMnSc2
Gordona	Gordon	k1gMnSc2
(	(	kIx(
<g/>
1839	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
narození	narození	k1gNnSc6
Anny	Anna	k1gFnSc2
protrpěla	protrpět	k5eAaPmAgFnS
náročnou	náročný	k2eAgFnSc7d1
a	a	k8xC
bolestivou	bolestivý	k2eAgFnSc4d1
nemoc	nemoc	k1gFnSc4
<g/>
,	,	kIx,
ze	z	k7c2
které	který	k3yQgFnSc2,k3yRgFnSc2,k3yIgFnSc2
se	se	k3xPyFc4
léčila	léčit	k5eAaImAgNnP
měsíce	měsíc	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátce	krátce	k6eAd1
po	po	k7c6
narození	narození	k1gNnSc6
jejího	její	k3xOp3gMnSc2
třetího	třetí	k4xOgMnSc2
potomka	potomek	k1gMnSc2
se	se	k3xPyFc4
však	však	k9
opět	opět	k6eAd1
vrátily	vrátit	k5eAaPmAgFnP
časté	častý	k2eAgFnPc4d1
nemoci	nemoc	k1gFnPc4
a	a	k8xC
bolesti	bolest	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předepsány	předepsán	k2eAgFnPc4d1
ji	on	k3xPp3gFnSc4
byly	být	k5eAaImAgInP
silné	silný	k2eAgInPc1d1
léky	lék	k1gInPc1
<g/>
;	;	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
alkoholem	alkohol	k1gInSc7
<g/>
,	,	kIx,
kterému	který	k3yRgInSc3,k3yIgInSc3,k3yQgInSc3
však	však	k9
nedlouho	dlouho	k6eNd1
poté	poté	k6eAd1
propadla	propadnout	k5eAaPmAgNnP
a	a	k8xC
její	její	k3xOp3gInSc1
zdravotní	zdravotní	k2eAgInSc1d1
stav	stav	k1gInSc1
se	se	k3xPyFc4
začal	začít	k5eAaPmAgInS
silně	silně	k6eAd1
zhoršovat	zhoršovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1838	#num#	k4
se	se	k3xPyFc4
z	z	k7c2
jejího	její	k3xOp3gMnSc2
manžela	manžel	k1gMnSc2
stal	stát	k5eAaPmAgMnS
hrabě	hrabě	k1gMnSc1
z	z	k7c2
Lovelace	Lovelace	k1gFnSc2
a	a	k8xC
odtud	odtud	k6eAd1
pochází	pocházet	k5eAaImIp3nS
jméno	jméno	k1gNnSc1
<g/>
,	,	kIx,
pod	pod	k7c7
kterým	který	k3yRgInSc7,k3yQgInSc7,k3yIgInSc7
je	být	k5eAaImIp3nS
dnes	dnes	k6eAd1
známá	známý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
lety	léto	k1gNnPc7
1843	#num#	k4
a	a	k8xC
1844	#num#	k4
pověřila	pověřit	k5eAaPmAgFnS
její	její	k3xOp3gFnSc1
matka	matka	k1gFnSc1
Williama	William	k1gMnSc4
Benjamina	Benjamin	k1gMnSc4
Carpentera	Carpenter	k1gMnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
vyučoval	vyučovat	k5eAaImAgMnS
její	její	k3xOp3gFnPc4
děti	dítě	k1gFnPc4
a	a	k8xC
aby	aby	kYmCp3nS
figuroval	figurovat	k5eAaImAgMnS
jako	jako	k8xS,k8xC
morální	morální	k2eAgMnSc1d1
instruktor	instruktor	k1gMnSc1
v	v	k7c6
životě	život	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Carpenter	Carpentrum	k1gNnPc2
se	se	k3xPyFc4
do	do	k7c2
ní	on	k3xPp3gFnSc2
rychle	rychle	k6eAd1
zamiloval	zamilovat	k5eAaPmAgMnS
a	a	k8xC
dal	dát	k5eAaPmAgMnS
jí	jíst	k5eAaImIp3nS
najevo	najevo	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
měla	mít	k5eAaImAgFnS
jednat	jednat	k5eAaImF
na	na	k7c6
základě	základ	k1gInSc6
svých	svůj	k3xOyFgFnPc2
potlačených	potlačený	k2eAgFnPc2d1
emocí	emoce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
ženatý	ženatý	k2eAgMnSc1d1
a	a	k8xC
tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
tento	tento	k3xDgInSc1
fakt	fakt	k1gInSc1
mu	on	k3xPp3gMnSc3
zabraňuje	zabraňovat	k5eAaImIp3nS
jednat	jednat	k5eAaImF
neslušným	slušný	k2eNgInSc7d1
způsobem	způsob	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jakmile	jakmile	k8xS
se	se	k3xPyFc4
dovtípila	dovtípit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
s	s	k7c7
ní	on	k3xPp3gFnSc7
snaží	snažit	k5eAaImIp3nS
navázat	navázat	k5eAaPmF
vztah	vztah	k1gInSc4
<g/>
,	,	kIx,
přerušila	přerušit	k5eAaPmAgFnS
s	s	k7c7
ním	on	k3xPp3gMnSc7
kontakt	kontakt	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ada	Ada	kA
Lovelaceová	Lovelaceová	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1852	#num#	k4
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1841	#num#	k4
se	se	k3xPyFc4
společně	společně	k6eAd1
s	s	k7c7
Medorou	Medorý	k2eAgFnSc7d1
Leighovou	Leighová	k1gFnSc7
(	(	kIx(
<g/>
dcerou	dcera	k1gFnSc7
Byronovy	Byronův	k2eAgFnSc2d1
nevlastní	vlastní	k2eNgFnSc2d1
sestry	sestra	k1gFnSc2
Augusty	Augusta	k1gMnSc2
Leighové	Leighová	k1gFnSc2
<g/>
)	)	kIx)
od	od	k7c2
své	svůj	k3xOyFgFnSc2
matky	matka	k1gFnSc2
dozvěděla	dozvědět	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
Medořiným	Medořin	k2eAgMnSc7d1
otcem	otec	k1gMnSc7
byl	být	k5eAaImAgInS
taktéž	taktéž	k?
Byron	Byron	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
napsala	napsat	k5eAaBmAgFnS,k5eAaPmAgFnS
matce	matka	k1gFnSc3
v	v	k7c6
dopise	dopis	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
tato	tento	k3xDgFnSc1
skutečnost	skutečnost	k1gFnSc1
nepřekvapila	překvapit	k5eNaPmAgFnS
a	a	k8xC
že	že	k8xS
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
jen	jen	k9
potvrdilo	potvrdit	k5eAaPmAgNnS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yQnSc4,k3yRnSc4
už	už	k6eAd1
dlouho	dlouho	k6eAd1
považovala	považovat	k5eAaImAgFnS
za	za	k7c4
pravdu	pravda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevinila	vinit	k5eNaImAgFnS
z	z	k7c2
incestního	incestní	k2eAgInSc2d1
vztahu	vztah	k1gInSc2
svého	svůj	k3xOyFgMnSc2
otce	otec	k1gMnSc2
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
Augustu	Augusta	k1gMnSc4
Leighovou	Leighový	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	to	k9
ovšem	ovšem	k9
její	její	k3xOp3gFnSc3
matce	matka	k1gFnSc3
nezabránilo	zabránit	k5eNaPmAgNnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
snažila	snažit	k5eAaImAgFnS
v	v	k7c6
očích	oko	k1gNnPc6
své	svůj	k3xOyFgFnSc2
dcery	dcera	k1gFnSc2
poškodit	poškodit	k5eAaPmF
Byronovu	Byronův	k2eAgFnSc4d1
pověst	pověst	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
její	její	k3xOp3gFnSc2
snahy	snaha	k1gFnSc2
jen	jen	k9
matematičku	matematička	k1gFnSc4
navedly	navést	k5eAaPmAgFnP
blíže	blízce	k6eAd2
otcově	otcův	k2eAgFnSc3d1
památce	památka	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
čtyřicátých	čtyřicátý	k4xOgNnPc6
letech	léto	k1gNnPc6
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
předmětem	předmět	k1gInSc7
několika	několik	k4yIc2
skandálů	skandál	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měla	mít	k5eAaImAgFnS
velmi	velmi	k6eAd1
uvolněné	uvolněný	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
s	s	k7c7
muži	muž	k1gMnPc7
<g/>
,	,	kIx,
za	za	k7c2
které	který	k3yQgFnSc2,k3yIgFnSc2,k3yRgFnSc2
nebyla	být	k5eNaImAgFnS
vdaná	vdaný	k2eAgFnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
pomluvám	pomluva	k1gFnPc3
o	o	k7c6
různých	různý	k2eAgFnPc6d1
aférách	aféra	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Dále	daleko	k6eAd2
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
dokázala	dokázat	k5eAaPmAgFnS
vzdát	vzdát	k5eAaPmF
závislosti	závislost	k1gFnSc3
na	na	k7c6
alkoholu	alkohol	k1gInSc6
<g/>
,	,	kIx,
propadla	propadnout	k5eAaPmAgFnS
sázení	sázení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1851	#num#	k4
se	se	k3xPyFc4
s	s	k7c7
řadou	řada	k1gFnSc7
svých	svůj	k3xOyFgMnPc2
přátel	přítel	k1gMnPc2
rozhodla	rozhodnout	k5eAaPmAgFnS
sestavit	sestavit	k5eAaPmF
algoritmus	algoritmus	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
by	by	kYmCp3nS
předvídal	předvídat	k5eAaImAgInS
výsledky	výsledek	k1gInPc4
sázek	sázka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgFnSc1d1
práce	práce	k1gFnSc1
Charlese	Charles	k1gMnSc2
Babbage	Babbag	k1gMnSc2
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
její	její	k3xOp3gMnPc1
však	však	k9
již	již	k6eAd1
ztratila	ztratit	k5eAaPmAgFnS
kredit	kredit	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
měla	mít	k5eAaImAgFnS
v	v	k7c6
dřívějších	dřívější	k2eAgNnPc6d1
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
dva	dva	k4xCgMnPc1
byli	být	k5eAaImAgMnP
značně	značně	k6eAd1
zadlužení	zadlužení	k1gNnSc4
a	a	k8xC
doufali	doufat	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
odkrytím	odkrytí	k1gNnSc7
revolučního	revoluční	k2eAgInSc2d1
mechanismu	mechanismus	k1gInSc2
pro	pro	k7c4
vypočítání	vypočítání	k1gNnSc4
úspěchu	úspěch	k1gInSc2
v	v	k7c6
sázkách	sázka	k1gFnPc6
budou	být	k5eAaImBp3nP
moci	moct	k5eAaImF
dluhy	dluh	k1gInPc4
splatit	splatit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celá	celý	k2eAgFnSc1d1
akce	akce	k1gFnSc1
však	však	k9
přilákala	přilákat	k5eAaPmAgFnS
pozornost	pozornost	k1gFnSc1
zločineckých	zločinecký	k2eAgInPc2d1
kruhů	kruh	k1gInPc2
a	a	k8xC
Ada	Ada	kA
musela	muset	k5eAaImAgFnS
veškerou	veškerý	k3xTgFnSc4
svoji	svůj	k3xOyFgFnSc4
práci	práce	k1gFnSc4
připsat	připsat	k5eAaPmF
svému	svůj	k3xOyFgMnSc3
manželovi	manžel	k1gMnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
vyhnula	vyhnout	k5eAaPmAgFnS
výhrůžkám	výhrůžka	k1gFnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Měla	mít	k5eAaImAgFnS
také	také	k9
nejasný	jasný	k2eNgInSc4d1
<g/>
,	,	kIx,
pravděpodobně	pravděpodobně	k6eAd1
intimní	intimní	k2eAgInSc4d1
vztah	vztah	k1gInSc4
se	s	k7c7
synem	syn	k1gMnSc7
Andrewa	Andrewus	k1gMnSc2
Crosse	Crosse	k1gFnSc2
<g/>
,	,	kIx,
Johnem	John	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
této	tento	k3xDgFnSc6
záležitosti	záležitost	k1gFnSc6
však	však	k9
není	být	k5eNaImIp3nS
mnoho	mnoho	k6eAd1
známo	znám	k2eAgNnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
Crosse	Crosse	k1gFnSc1
po	po	k7c6
Adině	Adin	k1gInSc6
smrti	smrt	k1gFnSc2
spálil	spálit	k5eAaPmAgInS
veškerou	veškerý	k3xTgFnSc4
jejich	jejich	k3xOp3gFnSc4
korespondenci	korespondence	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jeho	jeho	k3xOp3gFnSc4
vážnost	vážnost	k1gFnSc4
lze	lze	k6eAd1
usuzovat	usuzovat	k5eAaImF
z	z	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
mu	on	k3xPp3gMnSc3
Ada	Ada	kA
odkázala	odkázat	k5eAaPmAgFnS
majetky	majetek	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yRgInPc4,k3yQgInPc4
získala	získat	k5eAaPmAgFnS
po	po	k7c6
otci	otec	k1gMnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Smrt	smrt	k1gFnSc1
</s>
<s>
Zemřela	zemřít	k5eAaPmAgFnS
ve	v	k7c6
věku	věk	k1gInSc6
36	#num#	k4
let	léto	k1gNnPc2
(	(	kIx(
<g/>
stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
její	její	k3xOp3gMnSc1
otec	otec	k1gMnSc1
<g/>
)	)	kIx)
27	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1852	#num#	k4
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
jí	jíst	k5eAaImIp3nS
její	její	k3xOp3gMnSc1
lékař	lékař	k1gMnSc1
pustil	pustit	k5eAaPmAgMnS
žilou	žíla	k1gFnSc7
kvůli	kvůli	k7c3
komplikacím	komplikace	k1gFnPc3
s	s	k7c7
rakovinou	rakovina	k1gFnSc7
dělohy	děloha	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Její	její	k3xOp3gFnSc1
nemoc	nemoc	k1gFnSc1
trvala	trvat	k5eAaImAgFnS
měsíce	měsíc	k1gInPc4
<g/>
,	,	kIx,
během	během	k7c2
kterých	který	k3yIgFnPc2,k3yQgFnPc2,k3yRgFnPc2
Anabella	Anabella	k1gFnSc1
rozhodovala	rozhodovat	k5eAaImAgFnS
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
kdo	kdo	k3yQnSc1,k3yInSc1,k3yRnSc1
její	její	k3xOp3gFnSc4
dceru	dcera	k1gFnSc4
směl	smět	k5eAaImAgMnS
navštívit	navštívit	k5eAaPmF
a	a	k8xC
bránila	bránit	k5eAaImAgFnS
v	v	k7c6
tom	ten	k3xDgNnSc6
všem	všecek	k3xTgMnPc3
jejím	její	k3xOp3gMnPc3
přátelům	přítel	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovlivněna	ovlivněn	k2eAgFnSc1d1
svou	svůj	k3xOyFgFnSc7
matkou	matka	k1gFnSc7
<g/>
,	,	kIx,
změnila	změnit	k5eAaPmAgFnS
náboženství	náboženství	k1gNnSc4
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
byla	být	k5eAaImAgFnS
materialistka	materialistka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
byla	být	k5eAaImAgFnS
přinucena	přinutit	k5eAaPmNgFnS
ke	k	k7c3
změně	změna	k1gFnSc3
své	svůj	k3xOyFgFnSc2
poslední	poslední	k2eAgFnSc2d1
vůle	vůle	k1gFnSc2
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
matka	matka	k1gFnSc1
byla	být	k5eAaImAgFnS
její	její	k3xOp3gFnSc7
vykonavatelkou	vykonavatelka	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Se	s	k7c7
svým	svůj	k3xOyFgMnSc7
manželem	manžel	k1gMnSc7
nebyla	být	k5eNaImAgFnS
v	v	k7c6
kontaktu	kontakt	k1gInSc6
od	od	k7c2
30	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
mu	on	k3xPp3gNnSc3
svěřila	svěřit	k5eAaPmAgFnS
s	s	k7c7
něčím	něco	k3yInSc7
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
čemu	co	k3yQnSc3,k3yRnSc3,k3yInSc3
od	od	k7c2
ní	on	k3xPp3gFnSc3
odešel	odejít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsah	obsah	k1gInSc1
jejich	jejich	k3xOp3gInSc2
rozhovoru	rozhovor	k1gInSc2
není	být	k5eNaImIp3nS
znám	znám	k2eAgMnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
pravděpodobně	pravděpodobně	k6eAd1
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c6
přiznání	přiznání	k1gNnSc6
nevěry	nevěra	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
Zanechala	zanechat	k5eAaPmAgFnS
po	po	k7c6
sobě	se	k3xPyFc3
dva	dva	k4xCgMnPc4
syny	syn	k1gMnPc4
a	a	k8xC
jednu	jeden	k4xCgFnSc4
dceru	dcera	k1gFnSc4
<g/>
,	,	kIx,
lady	lady	k1gFnSc4
Anne	Ann	k1gFnSc2
Bluntovou	Bluntový	k2eAgFnSc4d1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
známá	známý	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
spoluzakladatelka	spoluzakladatelka	k1gFnSc1
chovu	chov	k1gInSc2
arabských	arabský	k2eAgMnPc2d1
plnokrevných	plnokrevný	k2eAgMnPc2d1
koní	kůň	k1gMnPc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
svou	svůj	k3xOyFgFnSc4
žádost	žádost	k1gFnSc4
byla	být	k5eAaImAgFnS
pohřbena	pohřbít	k5eAaPmNgFnS
vedle	vedle	k7c2
svého	svůj	k3xOyFgMnSc2
otce	otec	k1gMnSc2
v	v	k7c6
kostele	kostel	k1gInSc6
svaté	svatý	k2eAgFnSc2d1
Marie	Maria	k1gFnSc2
Magdalény	Magdaléna	k1gFnSc2
v	v	k7c6
Nottinghamu	Nottingham	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Poznámka	poznámka	k1gFnSc1
G	G	kA
od	od	k7c2
Lovelaceové	Lovelaceová	k1gFnSc2
v	v	k7c6
jejím	její	k3xOp3gInSc6
překladu	překlad	k1gInSc6
Menabreova	Menabreův	k2eAgInSc2d1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
některými	některý	k3yIgFnPc7
historiky	historik	k1gMnPc7
považována	považován	k2eAgNnPc1d1
za	za	k7c4
vůbec	vůbec	k9
první	první	k4xOgInSc1
počítačový	počítačový	k2eAgInSc1d1
program	program	k1gInSc1
na	na	k7c6
světě	svět	k1gInSc6
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ada	Ada	kA
Lovelaceová	Lovelaceová	k1gFnSc1
byla	být	k5eAaImAgFnS
nadaná	nadaný	k2eAgFnSc1d1
matematička	matematička	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
seznámení	seznámení	k1gNnSc6
s	s	k7c7
Charlesem	Charles	k1gMnSc7
Babbagem	Babbag	k1gMnSc7
se	se	k3xPyFc4
však	však	k9
začala	začít	k5eAaPmAgFnS
zajímat	zajímat	k5eAaImF
i	i	k9
o	o	k7c4
mechanické	mechanický	k2eAgInPc4d1
počítače	počítač	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
již	již	k6eAd1
bylo	být	k5eAaImAgNnS
zmíněno	zmínit	k5eAaPmNgNnS
výše	vysoce	k6eAd2
<g/>
,	,	kIx,
detailně	detailně	k6eAd1
popsala	popsat	k5eAaPmAgFnS
funkci	funkce	k1gFnSc4
Babbageova	Babbageův	k2eAgMnSc2d1
Analytical	Analytical	k1gMnSc2
Engine	Engin	k1gMnSc5
a	a	k8xC
také	také	k9
se	se	k3xPyFc4
podílela	podílet	k5eAaImAgFnS
na	na	k7c6
jeho	jeho	k3xOp3gInSc6
vývoji	vývoj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Navrhla	navrhnout	k5eAaPmAgFnS
způsob	způsob	k1gInSc4
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
tento	tento	k3xDgInSc4
počítač	počítač	k1gInSc4
naprogramovat	naprogramovat	k5eAaPmF
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
pomocí	pomocí	k7c2
děrných	děrný	k2eAgInPc2d1
štítků	štítek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zavedla	zavést	k5eAaPmAgFnS
pojmy	pojem	k1gInPc4
jako	jako	k8xC,k8xS
podmíněný	podmíněný	k2eAgInSc4d1
a	a	k8xC
nepodmíněný	podmíněný	k2eNgInSc4d1
skok	skok	k1gInSc4
<g/>
,	,	kIx,
cyklus	cyklus	k1gInSc1
a	a	k8xC
podprogram	podprogram	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
se	se	k3xPyFc4
objevují	objevovat	k5eAaImIp3nP
i	i	k9
zmínky	zmínka	k1gFnPc1
o	o	k7c6
algoritmizaci	algoritmizace	k1gFnSc6
jakožto	jakožto	k8xS
základu	základ	k1gInSc6
programování	programování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lze	lze	k6eAd1
tedy	tedy	k9
říci	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
vymyslela	vymyslet	k5eAaPmAgFnS
programování	programování	k1gNnSc4
tak	tak	k6eAd1
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
je	být	k5eAaImIp3nS
známé	známý	k2eAgNnSc1d1
dnes	dnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Tyto	tento	k3xDgFnPc4
základní	základní	k2eAgFnPc4d1
myšlenky	myšlenka	k1gFnPc4
zveřejnila	zveřejnit	k5eAaPmAgFnS
v	v	k7c6
upraveném	upravený	k2eAgInSc6d1
překladu	překlad	k1gInSc6
článku	článek	k1gInSc2
italského	italský	k2eAgMnSc2d1
matematika	matematik	k1gMnSc2
Luigiho	Luigi	k1gMnSc2
Menabrea	Menabreus	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc4
přeložila	přeložit	k5eAaPmAgFnS
z	z	k7c2
francouzštiny	francouzština	k1gFnSc2
do	do	k7c2
angličtiny	angličtina	k1gFnSc2
a	a	k8xC
doplnila	doplnit	k5eAaPmAgFnS
vlastními	vlastní	k2eAgFnPc7d1
poznámkami	poznámka	k1gFnPc7
a	a	k8xC
závěry	závěr	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Předpovědi	předpověď	k1gFnPc1
do	do	k7c2
budoucna	budoucno	k1gNnSc2
</s>
<s>
Ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
díle	dílo	k1gNnSc6
se	se	k3xPyFc4
zabývala	zabývat	k5eAaImAgFnS
využitím	využití	k1gNnSc7
počítačů	počítač	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předpověděla	předpovědět	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
počítače	počítač	k1gInPc1
jednou	jednou	k6eAd1
budou	být	k5eAaImBp3nP
používat	používat	k5eAaImF
pro	pro	k7c4
řešení	řešení	k1gNnSc4
složitých	složitý	k2eAgFnPc2d1
matematických	matematický	k2eAgFnPc2d1
úloh	úloha	k1gFnPc2
<g/>
,	,	kIx,
komponování	komponování	k1gNnSc4
hudby	hudba	k1gFnSc2
a	a	k8xC
kreslení	kreslení	k1gNnSc2
obrazů	obraz	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Dále	daleko	k6eAd2
formulovala	formulovat	k5eAaImAgFnS
myšlenku	myšlenka	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
počítač	počítač	k1gInSc1
není	být	k5eNaImIp3nS
schopen	schopen	k2eAgInSc1d1
tvořivého	tvořivý	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
(	(	kIx(
<g/>
Babbage	Babbage	k1gFnSc1
s	s	k7c7
touto	tento	k3xDgFnSc7
myšlenkou	myšlenka	k1gFnSc7
však	však	k9
nesouhlasil	souhlasit	k5eNaImAgMnS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
neboť	neboť	k8xC
je	být	k5eAaImIp3nS
omezen	omezit	k5eAaPmNgMnS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
jsou	být	k5eAaImIp3nP
lidé	člověk	k1gMnPc1
do	do	k7c2
něj	on	k3xPp3gNnSc2
schopni	schopen	k2eAgMnPc1d1
naprogramovat	naprogramovat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
posledních	poslední	k2eAgNnPc6d1
letech	léto	k1gNnPc6
svého	svůj	k3xOyFgInSc2
života	život	k1gInSc2
se	se	k3xPyFc4
soustředila	soustředit	k5eAaPmAgFnS
na	na	k7c4
pochopení	pochopení	k1gNnSc4
procesu	proces	k1gInSc2
tvořivého	tvořivý	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
a	a	k8xC
jeho	jeho	k3xOp3gNnSc1
převedení	převedení	k1gNnSc1
do	do	k7c2
strojové	strojový	k2eAgFnSc2d1
podoby	podoba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Kindred	Kindred	k1gMnSc1
Britain	Britain	k1gMnSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
STRADIOTOVÁ	STRADIOTOVÁ	kA
<g/>
,	,	kIx,
Marie	Marie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mechanické	mechanický	k2eAgInPc1d1
a	a	k8xC
elektromechanické	elektromechanický	k2eAgInPc1d1
počítací	počítací	k2eAgInPc1d1
stroje	stroj	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
,	,	kIx,
2015	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
160	#num#	k4
s.	s.	k?
Diplomová	diplomový	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Univerzita	univerzita	k1gFnSc1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedoucí	vedoucí	k2eAgFnSc1d1
práce	práce	k1gFnSc1
Michal	Michal	k1gMnSc1
Musílek	Musílek	k1gMnSc1
<g/>
.	.	kIx.
s.	s.	k?
61	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Ada	Ada	kA
Lovelace	Lovelace	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Computer	computer	k1gInSc1
History	Histor	k1gInPc7
Museum	museum	k1gNnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
STEINOVÁ	Steinová	k1gFnSc1
<g/>
,	,	kIx,
Dorothy	Doroth	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ada	Ada	kA
<g/>
:	:	kIx,
A	a	k9
Life	Life	k1gNnSc6
and	and	k?
a	a	k8xC
Legacy	Legaca	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Massachusetts	Massachusetts	k1gNnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
MIT	MIT	kA
Press	Press	k1gInSc1
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
17	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Stein	Stein	k1gMnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
BATES	BATES	kA
<g/>
,	,	kIx,
Laura	Laura	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Women	Women	k1gInSc1
in	in	k?
science	science	k1gFnSc1
<g/>
:	:	kIx,
'	'	kIx"
<g/>
Whoa	Whoa	k1gMnSc1
<g/>
,	,	kIx,
what	what	k1gMnSc1
are	ar	k1gInSc5
you	you	k?
doing	doing	k1gInSc4
here	her	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
'	'	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-10-17	2013-10-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
261	#num#	k4
<g/>
-	-	kIx~
<g/>
3077	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
WOOLLEY	WOOLLEY	kA
<g/>
,	,	kIx,
Benjamin	Benjamin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Bride	Brid	k1gInSc5
of	of	k?
Science	Science	k1gFnSc1
<g/>
:	:	kIx,
Romance	romance	k1gFnSc1
<g/>
,	,	kIx,
Reason	Reason	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
Byron	Byron	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Daughter	Daughtra	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Pan	Pan	k1gMnSc1
Macmillan	Macmillan	k1gMnSc1
Australia	Australium	k1gNnSc2
Pty	Pty	k1gMnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
80	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Woolley	Woollea	k1gFnSc2
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Woolley	Woollea	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
10	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Woolley	Woollea	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
85	#num#	k4
<g/>
-	-	kIx~
<g/>
87	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Woolley	Woollea	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
119	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Stein	Stein	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
17	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
TOOLE	TOOLE	kA
<g/>
,	,	kIx,
Betty	Betty	k1gFnSc1
Alexandra	Alexandra	k1gFnSc1
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Poetical	Poetical	k1gFnSc1
Science	Science	k1gFnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Byron	Byron	k1gMnSc1
Journal	Journal	k1gMnSc1
<g/>
.	.	kIx.
1987	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
15	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
55	#num#	k4
<g/>
-	-	kIx~
<g/>
65	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Woolley	Woollea	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
120	#num#	k4
<g/>
-	-	kIx~
<g/>
121	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Článek	článek	k1gInSc1
na	na	k7c6
portálu	portál	k1gInSc6
Národního	národní	k2eAgNnSc2d1
historického	historický	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
Austrálie	Austrálie	k1gFnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
www.nhm.ac.uk	www.nhm.ac.uk	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
The	The	k1gMnPc4
Peerage	Peerag	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Woolley	Woollea	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
285	#num#	k4
<g/>
-	-	kIx~
<g/>
286	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Woolley	Woollea	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
289	#num#	k4
<g/>
-	-	kIx~
<g/>
296	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Woolley	Woollea	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
302	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Woolley	Woollea	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
340	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
-	-	kIx~
<g/>
342	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Woolley	Woollea	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
336	#num#	k4
<g/>
-	-	kIx~
<g/>
337	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Článek	článek	k1gInSc1
na	na	k7c6
portálu	portál	k1gInSc6
express	express	k1gInSc1
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
<g/>
↑	↑	k?
Woolley	Woollea	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
361	#num#	k4
<g/>
-	-	kIx~
<g/>
362	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Woolley	Woollea	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
370	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Woolley	Woollea	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
369	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
J.	J.	kA
Fuegi	Fueg	k1gMnSc3
and	and	k?
J.	J.	kA
Francis	Francis	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lovelace	Lovelace	k1gFnSc1
&	&	k?
Babbage	Babbag	k1gFnSc2
and	and	k?
the	the	k?
creation	creation	k1gInSc1
of	of	k?
the	the	k?
1843	#num#	k4
'	'	kIx"
<g/>
notes	notes	k1gInSc1
<g/>
'	'	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annals	Annals	k1gInSc1
of	of	k?
the	the	k?
History	Histor	k1gInPc1
of	of	k?
Computing	Computing	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
October	October	k1gInSc1
<g/>
–	–	k?
<g/>
December	December	k1gInSc1
2003	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
25	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
4	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
16	#num#	k4
<g/>
,	,	kIx,
19	#num#	k4
<g/>
,	,	kIx,
25	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.110	10.110	k4
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
MAHC	MAHC	kA
<g/>
.2003	.2003	k4
<g/>
.1253887	.1253887	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Galerie	galerie	k1gFnSc1
Ada	Ada	kA
Lovelace	Lovelace	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Ada	Ada	kA
Lovelace	Lovelace	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Web	web	k1gInSc1
Futurologie	futurologie	k1gFnSc2
</s>
<s>
Ada	Ada	kA
Lovelace	Lovelace	k1gFnSc1
—	—	k?
Zkus	zkusit	k5eAaPmRp2nS
IT	IT	kA
</s>
<s>
Ada	Ada	kA
picture	pictur	k1gMnSc5
gallery	galler	k1gInPc5
</s>
<s>
Augusta	Augusta	k1gMnSc1
Ada	Ada	kA
King	King	k1gMnSc1
<g/>
,	,	kIx,
countess	countess	k6eAd1
of	of	k?
Lovelace	Lovelace	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jo	jo	k9
<g/>
2016900225	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
119232022	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
7689	#num#	k4
7871	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
78030997	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
61632881	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
78030997	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Matematika	matematika	k1gFnSc1
|	|	kIx~
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
