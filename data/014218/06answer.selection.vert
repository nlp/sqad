<s>
Augusta	Augusta	k1gMnSc1
Ada	Ada	kA
King	King	k1gMnSc1
<g/>
,	,	kIx,
hraběnka	hraběnka	k1gFnSc1
z	z	k7c2
Lovelace	Lovelace	k1gFnSc2
(	(	kIx(
<g/>
10	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1815	#num#	k4
Londýn	Londýn	k1gInSc1
–	–	k?
27	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1852	#num#	k4
tamtéž	tamtéž	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
narozená	narozený	k2eAgNnPc1d1
jako	jako	k8xC,k8xS
Augusta	Augusta	k1gMnSc1
Ada	Ada	kA
Byron	Byron	k1gMnSc1
a	a	k8xC
nyní	nyní	k6eAd1
známá	známý	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
Ada	k1gFnSc1	kA
Lovelace	k1gFnSc1	k1gFnSc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
anglická	anglický	k2eAgFnSc1d1
matematička	matematička	k1gFnSc1
a	a	k8xC
první	první	k4xOgFnSc1
programátorka	programátorka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
známá	známý	k2eAgFnSc1d1
především	především	k6eAd1
detailním	detailní	k2eAgInSc7d1
popisem	popis	k1gInSc7
fungování	fungování	k1gNnSc2
Babbageova	Babbageův	k2eAgInSc2d1
mechanického	mechanický	k2eAgInSc2d1
počítače	počítač	k1gInSc2
(	(	kIx(
<g/>
analytického	analytický	k2eAgInSc2d1
stroje	stroj	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
vývoj	vývoj	k1gInSc4
podporovala	podporovat	k5eAaImAgFnS
i	i	k9
finančně	finančně	k6eAd1
<g/>
.	.	kIx.
</s>