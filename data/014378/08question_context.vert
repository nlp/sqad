<s>
Petrohrad	Petrohrad	k1gInSc1
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
С	С	k?
[	[	kIx(
<g/>
sankt	sankt	k1gInSc1
pʲ	pʲ	k1gInSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
v	v	k7c6
českém	český	k2eAgInSc6d1
přepisu	přepis	k1gInSc6
latinkou	latinka	k1gFnSc7
Sankt-Petěrburg	Sankt-Petěrburg	k1gInSc1
<g/>
,	,	kIx,
rusky	rusky	k6eAd1
hovorově	hovorově	k6eAd1
zkracováno	zkracovat	k5eAaImNgNnS
na	na	k7c6
П	П	k?
(	(	kIx(
<g/>
Pitěr	Pitěr	k1gMnSc1
<g/>
))	))	k?
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
s	s	k7c7
populací	populace	k1gFnSc7
čítající	čítající	k2eAgFnSc7d1
přes	přes	k7c4
5	#num#	k4
milionů	milion	k4xCgInPc2
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
druhým	druhý	k4xOgMnSc7
největším	veliký	k2eAgMnSc7d3
městem	město	k1gNnSc7
Ruska	Rusko	k1gNnSc2
<g/>
.	.	kIx.
</s>