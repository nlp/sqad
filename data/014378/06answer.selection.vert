<s>
Město	město	k1gNnSc1
bylo	být	k5eAaImAgNnS
založeno	založit	k5eAaPmNgNnS
27	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
16	#num#	k4
<g/>
.	.	kIx.
jul	jul	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
května	květen	k1gInSc2
1703	#num#	k4
carem	car	k1gMnSc7
Petrem	Petr	k1gMnSc7
Velikým	veliký	k2eAgMnSc7d1
<g/>
,	,	kIx,
během	během	k7c2
Severní	severní	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
Zaječím	zaječí	k2eAgInSc6d1
ostrově	ostrov	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
byly	být	k5eAaImAgInP
položeny	položen	k2eAgInPc1d1
základy	základ	k1gInPc1
Petropavlovské	petropavlovský	k2eAgFnSc2d1
pevnosti	pevnost	k1gFnSc2
<g/>
.	.	kIx.
</s>