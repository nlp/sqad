<s>
Petrohrad	Petrohrad	k1gInSc1
</s>
<s>
Na	na	k7c4
tento	tento	k3xDgInSc4
článek	článek	k1gInSc4
je	být	k5eAaImIp3nS
přesměrováno	přesměrován	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Sankt	Sankt	k1gInSc1
Petersburg	Petersburg	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
městě	město	k1gNnSc6
v	v	k7c6
Rusku	Rusko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránkách	stránka	k1gFnPc6
St.	st.	kA
Petersburg	Petersburg	k1gInSc4
a	a	k8xC
Petrohrad	Petrohrad	k1gInSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Petrohrad	Petrohrad	k1gInSc1
С	С	k?
Proti	proti	k7c3
směru	směr	k1gInSc3
hodinových	hodinový	k2eAgFnPc2d1
ručiček	ručička	k1gFnPc2
počínaje	počínaje	k7c7
obrázkem	obrázek	k1gInSc7
v	v	k7c6
pravém	pravý	k2eAgInSc6d1
horním	horní	k2eAgInSc6d1
rohu	roh	k1gInSc6
<g/>
:	:	kIx,
Smolenská	Smolenský	k2eAgFnSc1d1
katedrála	katedrála	k1gFnSc1
<g/>
,	,	kIx,
Petropavlovská	petropavlovský	k2eAgFnSc1d1
pevnost	pevnost	k1gFnSc1
<g/>
,	,	kIx,
Generální	generální	k2eAgInSc1d1
štáb	štáb	k1gInSc1
na	na	k7c6
řece	řeka	k1gFnSc6
Mojka	Mojko	k1gNnSc2
<g/>
,	,	kIx,
Chrám	chrám	k1gInSc1
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Trojice	trojice	k1gFnSc2
<g/>
,	,	kIx,
Zimní	zimní	k2eAgInSc1d1
palác	palác	k1gInSc1
<g/>
,	,	kIx,
Měděný	měděný	k2eAgMnSc1d1
jezdec	jezdec	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
59	#num#	k4
<g/>
°	°	k?
<g/>
57	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
30	#num#	k4
<g/>
°	°	k?
<g/>
19	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
3	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Časové	časový	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
</s>
<s>
UTC	UTC	kA
<g/>
+	+	kIx~
<g/>
3	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Stát	stát	k1gInSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
Rusko	Rusko	k1gNnSc1
Federální	federální	k2eAgNnSc1d1
okruh	okruh	k1gInSc4
</s>
<s>
Severozápadní	severozápadní	k2eAgNnSc1d1
Federální	federální	k2eAgNnSc1d1
město	město	k1gNnSc1
</s>
<s>
Petrohrad	Petrohrad	k1gInSc1
Administrativní	administrativní	k2eAgInSc1d1
dělení	dělení	k1gNnSc3
</s>
<s>
18	#num#	k4
obvodů	obvod	k1gInPc2
</s>
<s>
Petrohrad	Petrohrad	k1gInSc1
</s>
<s>
Petrohrad	Petrohrad	k1gInSc1
<g/>
,	,	kIx,
Evropa	Evropa	k1gFnSc1
</s>
<s>
Petrohrad	Petrohrad	k1gInSc1
</s>
<s>
Petrohrad	Petrohrad	k1gInSc1
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
1	#num#	k4
439	#num#	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
5	#num#	k4
351	#num#	k4
935	#num#	k4
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
3	#num#	k4
719,2	719,2	k4
obyv	obyva	k1gFnPc2
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
gubernátor	gubernátor	k1gMnSc1
</s>
<s>
Alexandr	Alexandr	k1gMnSc1
Dmitrijevič	Dmitrijevič	k1gMnSc1
Beglov	Beglov	k1gInSc1
(	(	kIx(
<g/>
dočasně	dočasně	k6eAd1
pověřen	pověřit	k5eAaPmNgInS
<g/>
)	)	kIx)
Vznik	vznik	k1gInSc1
</s>
<s>
1703	#num#	k4
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
gov	gov	k?
<g/>
.	.	kIx.
<g/>
spb	spb	k?
<g/>
.	.	kIx.
<g/>
ru	ru	k?
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
+7	+7	k4
812	#num#	k4
PSČ	PSČ	kA
</s>
<s>
190000-199406	190000-199406	k4
Označení	označení	k1gNnSc1
vozidel	vozidlo	k1gNnPc2
</s>
<s>
78	#num#	k4
<g/>
,	,	kIx,
98	#num#	k4
<g/>
,	,	kIx,
178	#num#	k4
a	a	k8xC
198	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Petrohrad	Petrohrad	k1gInSc1
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
С	С	k?
[	[	kIx(
<g/>
sankt	sankt	k1gInSc1
pʲ	pʲ	k1gInSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
v	v	k7c6
českém	český	k2eAgInSc6d1
přepisu	přepis	k1gInSc6
latinkou	latinka	k1gFnSc7
Sankt-Petěrburg	Sankt-Petěrburg	k1gInSc1
<g/>
,	,	kIx,
rusky	rusky	k6eAd1
hovorově	hovorově	k6eAd1
zkracováno	zkracovat	k5eAaImNgNnS
na	na	k7c6
П	П	k?
(	(	kIx(
<g/>
Pitěr	Pitěr	k1gMnSc1
<g/>
))	))	k?
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
s	s	k7c7
populací	populace	k1gFnSc7
čítající	čítající	k2eAgFnSc7d1
přes	přes	k7c4
5	#num#	k4
milionů	milion	k4xCgInPc2
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
druhým	druhý	k4xOgMnSc7
největším	veliký	k2eAgMnSc7d3
městem	město	k1gNnSc7
Ruska	Rusko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
v	v	k7c6
severozápadní	severozápadní	k2eAgFnSc6d1
části	část	k1gFnSc6
Ruska	Rusko	k1gNnSc2
<g/>
,	,	kIx,
při	při	k7c6
ústí	ústí	k1gNnSc6
řeky	řeka	k1gFnSc2
Něvy	Něva	k1gFnSc2
<g/>
,	,	kIx,
ve	v	k7c6
Finském	finský	k2eAgInSc6d1
zálivu	záliv	k1gInSc6
(	(	kIx(
<g/>
ten	ten	k3xDgInSc1
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
Baltského	baltský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
velkých	velký	k2eAgNnPc2d1
měst	město	k1gNnPc2
(	(	kIx(
<g/>
nad	nad	k7c4
milión	milión	k4xCgInSc4
obyvatel	obyvatel	k1gMnPc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
nejseverněji	severně	k6eAd3
položené	položený	k2eAgNnSc1d1
město	město	k1gNnSc1
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
Leningradské	leningradský	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
součástí	součást	k1gFnSc7
Severozápadního	severozápadní	k2eAgInSc2d1
federálního	federální	k2eAgInSc2d1
okruhu	okruh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
status	status	k1gInSc4
federálního	federální	k2eAgNnSc2d1
města	město	k1gNnSc2
Ruské	ruský	k2eAgFnSc2d1
federace	federace	k1gFnSc2
<g/>
,	,	kIx,
společně	společně	k6eAd1
s	s	k7c7
hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
Moskvou	Moskva	k1gFnSc7
a	a	k8xC
Sevastopolem	Sevastopol	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společně	společně	k6eAd1
s	s	k7c7
Moskvou	Moskva	k1gFnSc7
je	být	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
kulturním	kulturní	k2eAgNnSc7d1
střediskem	středisko	k1gNnSc7
celostátního	celostátní	k2eAgInSc2d1
významu	význam	k1gInSc2
<g/>
,	,	kIx,
zapsaným	zapsaný	k2eAgInSc7d1
od	od	k7c2
roku	rok	k1gInSc2
1990	#num#	k4
do	do	k7c2
seznamu	seznam	k1gInSc2
Světového	světový	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
mezinárodní	mezinárodní	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
UNESCO	Unesco	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
také	také	k9
jedním	jeden	k4xCgInSc7
z	z	k7c2
nejdůležitějších	důležitý	k2eAgInPc2d3
ruských	ruský	k2eAgInPc2d1
přístavů	přístav	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Město	město	k1gNnSc1
při	při	k7c6
svém	svůj	k3xOyFgNnSc6
založení	založení	k1gNnSc6
dostalo	dostat	k5eAaPmAgNnS
název	název	k1gInSc4
Sankt-Pitěr-Burch	Sankt-Pitěr-Burch	k1gInSc4
(	(	kIx(
<g/>
С	С	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
podle	podle	k7c2
nizozemského	nizozemský	k2eAgInSc2d1
Sankt-Pieterburchu	Sankt-Pieterburch	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Car	car	k1gMnSc1
Petr	Petr	k1gMnSc1
I.	I.	kA
Veliký	veliký	k2eAgInSc1d1
byl	být	k5eAaImAgInS
ovlivněn	ovlivnit	k5eAaPmNgInS
pobytem	pobyt	k1gInSc7
v	v	k7c6
Nizozemsku	Nizozemsko	k1gNnSc6
<g/>
,	,	kIx,
přitom	přitom	k6eAd1
pojmenoval	pojmenovat	k5eAaPmAgMnS
nové	nový	k2eAgNnSc4d1
město	město	k1gNnSc4
po	po	k7c6
svém	svůj	k3xOyFgMnSc6
patronovi	patron	k1gMnSc6
<g/>
,	,	kIx,
apoštolu	apoštol	k1gMnSc3
Petrovi	Petr	k1gMnSc3
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
po	po	k7c6
sobě	sebe	k3xPyFc6
samém	samý	k3xTgMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
let	léto	k1gNnPc2
se	se	k3xPyFc4
však	však	k9
ujal	ujmout	k5eAaPmAgInS
název	název	k1gInSc1
Sankt-Petěrburg	Sankt-Petěrburg	k1gInSc4
(	(	kIx(
<g/>
německy	německy	k6eAd1
Sankt	Sankt	k1gInSc1
Petersburg	Petersburg	k1gInSc1
<g/>
,	,	kIx,
avšak	avšak	k8xC
v	v	k7c6
poruštěné	poruštěný	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
stálo	stát	k5eAaImAgNnS
Rusko	Rusko	k1gNnSc1
proti	proti	k7c3
Německu	Německo	k1gNnSc3
a	a	k8xC
Rakousku-Uhersku	Rakousku-Uhersko	k1gNnSc3
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
oficiální	oficiální	k2eAgInSc1d1
název	název	k1gInSc1
města	město	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1914	#num#	k4
změněn	změnit	k5eAaPmNgInS
z	z	k7c2
německy	německy	k6eAd1
znějícího	znějící	k2eAgInSc2d1
názvu	název	k1gInSc2
na	na	k7c4
Petrograd	Petrograd	k1gInSc4
(	(	kIx(
<g/>
П	П	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
smrti	smrt	k1gFnSc6
Vladimira	Vladimir	k1gMnSc2
Iljiče	Iljič	k1gMnSc2
Lenina	Lenin	k1gMnSc2
bylo	být	k5eAaImAgNnS
26	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1924	#num#	k4
město	město	k1gNnSc4
přejmenováno	přejmenován	k2eAgNnSc4d1
na	na	k7c4
Leningrad	Leningrad	k1gInSc4
(	(	kIx(
<g/>
Л	Л	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
rozpadu	rozpad	k1gInSc6
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
byl	být	k5eAaImAgInS
na	na	k7c6
základě	základ	k1gInSc6
místního	místní	k2eAgNnSc2d1
referenda	referendum	k1gNnSc2
v	v	k7c6
létě	léto	k1gNnSc6
1991	#num#	k4
městu	město	k1gNnSc3
vrácen	vrátit	k5eAaPmNgInS
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
dřívější	dřívější	k2eAgInSc4d1
oficiální	oficiální	k2eAgInSc4d1
název	název	k1gInSc4
<g/>
,	,	kIx,
Sankt-Petěrburg	Sankt-Petěrburg	k1gInSc4
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
С	С	k?
<g/>
́	́	k?
<g/>
н	н	k?
<g/>
́	́	k?
<g/>
р	р	k?
[	[	kIx(
<g/>
sankt	sankt	k1gInSc1
pʲ	pʲ	k1gInSc1
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
obyvatelé	obyvatel	k1gMnPc1
používají	používat	k5eAaImIp3nP
často	často	k6eAd1
hovorového	hovorový	k2eAgInSc2d1
názvu	název	k1gInSc2
Pitěr	Pitěra	k1gFnPc2
(	(	kIx(
<g/>
П	П	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
České	český	k2eAgNnSc1d1
exonymum	exonymum	k1gNnSc1
Petrohrad	Petrohrad	k1gInSc1
nepochází	pocházet	k5eNaImIp3nS
z	z	k7c2
období	období	k1gNnSc2
1914	#num#	k4
<g/>
–	–	k?
<g/>
1924	#num#	k4
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
někdy	někdy	k6eAd1
mylně	mylně	k6eAd1
uvádí	uvádět	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svůj	svůj	k3xOyFgInSc4
původ	původ	k1gInSc1
má	mít	k5eAaImIp3nS
nejspíše	nejspíše	k9
v	v	k7c6
Jungmannově	Jungmannův	k2eAgInSc6d1
Slovníku	slovník	k1gInSc6
česko-německém	česko-německý	k2eAgInSc6d1
z	z	k7c2
roku	rok	k1gInSc2
1837	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
uveden	uveden	k2eAgInSc1d1
ekvivalent	ekvivalent	k1gInSc1
německého	německý	k2eAgInSc2d1
výrazu	výraz	k1gInSc2
Petersburg	Petersburg	k1gMnSc1
<g/>
,	,	kIx,
označující	označující	k2eAgMnSc1d1
„	„	k?
<g/>
město	město	k1gNnSc1
hlavní	hlavní	k2eAgFnSc1d1
v	v	k7c6
Rusích	Rus	k1gFnPc6
<g/>
“	“	k?
<g/>
,	,	kIx,
česky	česky	k6eAd1
Petrohrad	Petrohrad	k1gInSc1
<g/>
,	,	kIx,
popřípadě	popřípadě	k6eAd1
Petrov	Petrov	k1gInSc1
(	(	kIx(
<g/>
a	a	k8xC
bylo	být	k5eAaImAgNnS
doporučeno	doporučit	k5eAaPmNgNnS
užívat	užívat	k5eAaImF
první	první	k4xOgFnSc4
variantu	varianta	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
pravděpodobně	pravděpodobně	k6eAd1
o	o	k7c4
obrozenecký	obrozenecký	k2eAgInSc4d1
kalk	kalk	k1gInSc4
z	z	k7c2
němčiny	němčina	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Palácové	palácový	k2eAgNnSc1d1
náměstí	náměstí	k1gNnSc1
</s>
<s>
Petrohrad	Petrohrad	k1gInSc1
<g/>
,	,	kIx,
1903	#num#	k4
</s>
<s>
Město	město	k1gNnSc1
bylo	být	k5eAaImAgNnS
založeno	založit	k5eAaPmNgNnS
27	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
16	#num#	k4
<g/>
.	.	kIx.
jul	jul	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
května	květen	k1gInSc2
1703	#num#	k4
carem	car	k1gMnSc7
Petrem	Petr	k1gMnSc7
Velikým	veliký	k2eAgMnSc7d1
<g/>
,	,	kIx,
během	během	k7c2
Severní	severní	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
Zaječím	zaječí	k2eAgInSc6d1
ostrově	ostrov	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
byly	být	k5eAaImAgInP
položeny	položen	k2eAgInPc1d1
základy	základ	k1gInPc1
Petropavlovské	petropavlovský	k2eAgFnSc2d1
pevnosti	pevnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1
sídelní	sídelní	k2eAgNnSc1d1
město	město	k1gNnSc1
vybudoval	vybudovat	k5eAaPmAgInS
na	na	k7c6
močálech	močál	k1gInPc6
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yRgInPc4,k3yQgInPc4
nechal	nechat	k5eAaPmAgMnS
vysušit	vysušit	k5eAaPmF
(	(	kIx(
<g/>
za	za	k7c4
cenu	cena	k1gFnSc4
mnoha	mnoho	k4c2
životů	život	k1gInPc2
dělníků	dělník	k1gMnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
na	na	k7c6
více	hodně	k6eAd2
než	než	k8xS
40	#num#	k4
ostrovech	ostrov	k1gInPc6
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
spojených	spojený	k2eAgFnPc2d1
300	#num#	k4
mosty	most	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
oknem	okno	k1gNnSc7
do	do	k7c2
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1712	#num#	k4
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
Ruského	ruský	k2eAgNnSc2d1
carství	carství	k1gNnSc2
<g/>
,	,	kIx,
poté	poté	k6eAd1
Ruského	ruský	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
postavení	postavení	k1gNnSc1
si	se	k3xPyFc3
udrželo	udržet	k5eAaPmAgNnS
<g/>
,	,	kIx,
kromě	kromě	k7c2
let	léto	k1gNnPc2
1728	#num#	k4
<g/>
-	-	kIx~
<g/>
1732	#num#	k4
<g/>
,	,	kIx,
až	až	k6eAd1
do	do	k7c2
vzniku	vznik	k1gInSc2
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
začátkem	začátkem	k7c2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1716	#num#	k4
jmenoval	jmenovat	k5eAaBmAgMnS,k5eAaImAgMnS
Petr	Petr	k1gMnSc1
Veliký	veliký	k2eAgMnSc1d1
hlavním	hlavní	k2eAgMnSc7d1
architektem	architekt	k1gMnSc7
města	město	k1gNnSc2
francouzského	francouzský	k2eAgMnSc2d1
architekta	architekt	k1gMnSc2
Jean-Baptista	Jean-Baptista	k1gMnSc1
Alexandra	Alexandr	k1gMnSc2
Le	Le	k1gMnSc1
Blonda	blonda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
vzhledu	vzhled	k1gInSc6
města	město	k1gNnSc2
má	mít	k5eAaImIp3nS
zásluhu	zásluha	k1gFnSc4
i	i	k9
švýcarsko-italský	švýcarsko-italský	k2eAgMnSc1d1
architekt	architekt	k1gMnSc1
Domenico	Domenico	k1gMnSc1
Trezzini	Trezzin	k2eAgMnPc1d1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
dal	dát	k5eAaPmAgMnS
vzniknout	vzniknout	k5eAaPmF
tzv.	tzv.	kA
„	„	k?
<g/>
Petrovskému	petrovský	k2eAgInSc3d1
baroku	barok	k1gInSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
a	a	k8xC
také	také	k9
italský	italský	k2eAgMnSc1d1
architekt	architekt	k1gMnSc1
Bartolomeo	Bartolomeo	k1gMnSc1
Rastrelli	Rastrelle	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgFnP
postaveny	postavit	k5eAaPmNgFnP
další	další	k2eAgFnPc1d1
významné	významný	k2eAgFnPc1d1
stavby	stavba	k1gFnPc1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
Chrám	chrám	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Petra	Petr	k1gMnSc2
a	a	k8xC
Pavla	Pavel	k1gMnSc2
<g/>
,	,	kIx,
Palác	palác	k1gInSc1
Menšikov	Menšikov	k1gInSc1
<g/>
,	,	kIx,
Kunstkamera	Kunstkamera	k1gFnSc1
či	či	k8xC
Dvenadcať	Dvenadcať	k1gFnSc1
Kollegij	Kollegij	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
staly	stát	k5eAaPmAgFnP
dominantami	dominanta	k1gFnPc7
v	v	k7c6
centrální	centrální	k2eAgFnSc6d1
architektuře	architektura	k1gFnSc6
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1724	#num#	k4
bylo	být	k5eAaImAgNnS
založeno	založit	k5eAaPmNgNnS
sídlo	sídlo	k1gNnSc1
Akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
<g/>
,	,	kIx,
Petrohradské	petrohradský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
a	a	k8xC
Akademického	akademický	k2eAgNnSc2d1
gymnázia	gymnázium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc4
vystřídala	vystřídat	k5eAaPmAgFnS
baroko	baroko	k1gNnSc4
architektura	architektura	k1gFnSc1
neoklasicistní	neoklasicistní	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
vlády	vláda	k1gFnSc2
Kateřiny	Kateřina	k1gFnSc2
Veliké	veliký	k2eAgFnSc2d1
(	(	kIx(
<g/>
1762	#num#	k4
<g/>
-	-	kIx~
<g/>
1796	#num#	k4
<g/>
)	)	kIx)
se	se	k3xPyFc4
město	město	k1gNnSc1
rozvíjelo	rozvíjet	k5eAaImAgNnS
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
postavena	postaven	k2eAgFnSc1d1
Ermitáž	Ermitáž	k1gFnSc1
<g/>
,	,	kIx,
břehy	břeh	k1gInPc1
řeky	řeka	k1gFnSc2
Něvy	Něva	k1gFnSc2
byly	být	k5eAaImAgInP
olemovány	olemován	k2eAgInPc1d1
žulovými	žulový	k2eAgInPc7d1
náspy	násep	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
památku	památka	k1gFnSc4
vítězství	vítězství	k1gNnSc2
nad	nad	k7c7
Napoleonem	napoleon	k1gInSc7
(	(	kIx(
<g/>
1812	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgInS
Alexandrův	Alexandrův	k2eAgInSc1d1
sloup	sloup	k1gInSc1
a	a	k8xC
Narvská	Narvský	k2eAgFnSc1d1
brána	brána	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
40	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
klasicistní	klasicistní	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
ustupuje	ustupovat	k5eAaImIp3nS
romantismu	romantismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
průmyslové	průmyslový	k2eAgFnSc6d1
revoluci	revoluce	k1gFnSc6
Petrohrad	Petrohrad	k1gInSc1
překonal	překonat	k5eAaPmAgInS
Moskvu	Moskva	k1gFnSc4
<g/>
,	,	kIx,
v	v	k7c6
růstu	růst	k1gInSc6
populace	populace	k1gFnSc2
i	i	k8xC
průmyslovém	průmyslový	k2eAgInSc6d1
rozvoji	rozvoj	k1gInSc6
<g/>
,	,	kIx,
a	a	k8xC
rozvinul	rozvinout	k5eAaPmAgMnS
se	se	k3xPyFc4
v	v	k7c4
jedno	jeden	k4xCgNnSc4
z	z	k7c2
největších	veliký	k2eAgNnPc2d3
průmyslových	průmyslový	k2eAgNnPc2d1
měst	město	k1gNnPc2
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
s	s	k7c7
říčním	říční	k2eAgMnSc7d1
a	a	k8xC
námořním	námořní	k2eAgInSc7d1
přístavem	přístav	k1gInSc7
<g/>
,	,	kIx,
s	s	k7c7
velkou	velký	k2eAgFnSc7d1
námořní	námořní	k2eAgFnSc7d1
základnou	základna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
15	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1917	#num#	k4
přijel	přijet	k5eAaPmAgMnS
do	do	k7c2
Petrohradu	Petrohrad	k1gInSc2
T.	T.	kA
G.	G.	kA
Masaryk	Masaryk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
předseda	předseda	k1gMnSc1
Československé	československý	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
zde	zde	k6eAd1
osobně	osobně	k6eAd1
budoval	budovat	k5eAaImAgMnS
československé	československý	k2eAgNnSc4d1
legionářské	legionářský	k2eAgNnSc4d1
vojsko	vojsko	k1gNnSc4
<g/>
,	,	kIx,
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
a	a	k8xC
to	ten	k3xDgNnSc1
až	až	k9
do	do	k7c2
7	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1918	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
září	září	k1gNnSc6
a	a	k8xC
říjnu	říjen	k1gInSc6
1917	#num#	k4
obsadila	obsadit	k5eAaPmAgFnS
německá	německý	k2eAgFnSc1d1
vojska	vojsko	k1gNnSc2
západoestonské	západoestonský	k2eAgNnSc4d1
souostroví	souostroví	k1gNnSc4
<g/>
,	,	kIx,
a	a	k8xC
Petrohrad	Petrohrad	k1gInSc4
bombardovala	bombardovat	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1917	#num#	k4
(	(	kIx(
<g/>
25	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
podle	podle	k7c2
juliánského	juliánský	k2eAgInSc2d1
kalendáře	kalendář	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
tehdy	tehdy	k6eAd1
v	v	k7c6
zemi	zem	k1gFnSc6
platil	platit	k5eAaImAgMnS
<g/>
)	)	kIx)
výstřelem	výstřel	k1gInSc7
z	z	k7c2
křižníku	křižník	k1gInSc2
Aurora	Aurora	k1gFnSc1
začal	začít	k5eAaPmAgInS
ozbrojený	ozbrojený	k2eAgInSc1d1
převrat	převrat	k1gInSc1
–	–	k?
Říjnová	říjnový	k2eAgFnSc1d1
revoluce	revoluce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
zásadně	zásadně	k6eAd1
změnila	změnit	k5eAaPmAgFnS
dějiny	dějiny	k1gFnPc4
nejen	nejen	k6eAd1
Ruska	Rusko	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
celého	celý	k2eAgInSc2d1
světa	svět	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aurora	Aurora	k1gFnSc1
byla	být	k5eAaImAgFnS
dříve	dříve	k6eAd2
nasazena	nasadit	k5eAaPmNgFnS
v	v	k7c6
rusko-japonské	rusko-japonský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
první	první	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
operovala	operovat	k5eAaImAgFnS
v	v	k7c6
Baltském	baltský	k2eAgNnSc6d1
moři	moře	k1gNnSc6
<g/>
,	,	kIx,
poté	poté	k6eAd1
sloužila	sloužit	k5eAaImAgFnS
jako	jako	k9
výcviková	výcvikový	k2eAgFnSc1d1
loď	loď	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
byla	být	k5eAaImAgFnS
poškozena	poškodit	k5eAaPmNgFnS
a	a	k8xC
potopena	potopen	k2eAgFnSc1d1
<g/>
,	,	kIx,
po	po	k7c6
válce	válka	k1gFnSc6
byla	být	k5eAaImAgFnS
(	(	kIx(
<g/>
v	v	k7c6
letech	let	k1gInPc6
1945	#num#	k4
<g/>
–	–	k?
<g/>
1947	#num#	k4
<g/>
)	)	kIx)
opravena	opraven	k2eAgFnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
1957	#num#	k4
slouží	sloužit	k5eAaImIp3nS
jako	jako	k9
petrohradské	petrohradský	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Bolševici	bolševik	k1gMnPc1
<g/>
,	,	kIx,
vedení	vedení	k1gNnSc1
Leninem	Lenin	k1gMnSc7
<g/>
,	,	kIx,
zaútočili	zaútočit	k5eAaPmAgMnP
na	na	k7c4
Zimní	zimní	k2eAgInSc4d1
palác	palác	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
událost	událost	k1gFnSc1
<g/>
,	,	kIx,
poté	poté	k6eAd1
známá	známý	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
Velká	velký	k2eAgFnSc1d1
říjnová	říjnový	k2eAgFnSc1d1
revoluce	revoluce	k1gFnSc1
<g/>
,	,	kIx,
vedla	vést	k5eAaImAgFnS
ke	k	k7c3
konci	konec	k1gInSc3
ruské	ruský	k2eAgFnSc2d1
prozatímní	prozatímní	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
řídící	řídící	k2eAgNnSc1d1
Rusko	Rusko	k1gNnSc1
po	po	k7c6
odstoupení	odstoupení	k1gNnSc6
cara	car	k1gMnSc2
Mikuláše	Mikuláš	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
sověti	sovět	k5eAaImF,k5eAaPmF
přenesli	přenést	k5eAaPmAgMnP
(	(	kIx(
<g/>
12	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1918	#num#	k4
<g/>
)	)	kIx)
vládu	vláda	k1gFnSc4
do	do	k7c2
Moskvy	Moskva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
říjnu	říjen	k1gInSc6
1919	#num#	k4
se	se	k3xPyFc4
generál	generál	k1gMnSc1
Nikolaj	Nikolaj	k1gMnSc1
Judenič	Judenič	k1gMnSc1
pokusil	pokusit	k5eAaPmAgMnS
<g/>
,	,	kIx,
s	s	k7c7
Bílou	bílý	k2eAgFnSc7d1
armádou	armáda	k1gFnSc7
<g/>
,	,	kIx,
dobýt	dobýt	k5eAaPmF
město	město	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
Lev	Lev	k1gMnSc1
Trockij	Trockij	k1gMnSc1
mobilizoval	mobilizovat	k5eAaBmAgMnS
rudoarmějce	rudoarmějec	k1gMnPc4
a	a	k8xC
donutil	donutit	k5eAaPmAgInS
ho	on	k3xPp3gMnSc4
k	k	k7c3
ústupu	ústup	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Město	město	k1gNnSc1
během	během	k7c2
obležení	obležení	k1gNnSc2
německými	německý	k2eAgMnPc7d1
vojsky	vojsko	k1gNnPc7
</s>
<s>
Křižník	křižník	k1gInSc1
Aurora	Aurora	k1gFnSc1
</s>
<s>
Od	od	k7c2
září	září	k1gNnSc2
1941	#num#	k4
až	až	k9
do	do	k7c2
ledna	leden	k1gInSc2
1944	#num#	k4
bylo	být	k5eAaImAgNnS
město	město	k1gNnSc1
v	v	k7c6
obležení	obležení	k1gNnSc6
německými	německý	k2eAgFnPc7d1
vojsky	vojsky	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
hladomor	hladomor	k1gInSc4
tehdy	tehdy	k6eAd1
zemřelo	zemřít	k5eAaPmAgNnS
více	hodně	k6eAd2
než	než	k8xS
jeden	jeden	k4xCgInSc4
milion	milion	k4xCgInSc1
civilistů	civilista	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obležení	obležení	k1gNnSc1
města	město	k1gNnSc2
bylo	být	k5eAaImAgNnS
jedním	jeden	k4xCgInSc7
z	z	k7c2
nejdelších	dlouhý	k2eAgInPc2d3
<g/>
,	,	kIx,
a	a	k8xC
s	s	k7c7
nejtragičtějšími	tragický	k2eAgInPc7d3
následky	následek	k1gInPc7
<g/>
,	,	kIx,
v	v	k7c6
novodobé	novodobý	k2eAgFnSc6d1
historii	historie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
Leningradské	leningradský	k2eAgFnSc2d1
aféry	aféra	k1gFnSc2
(	(	kIx(
<g/>
1949	#num#	k4
<g/>
–	–	k?
<g/>
1952	#num#	k4
<g/>
)	)	kIx)
bylo	být	k5eAaImAgNnS
<g/>
,	,	kIx,
ve	v	k7c6
Stalinem	Stalin	k1gMnSc7
inscenovaném	inscenovaný	k2eAgInSc6d1
procesu	proces	k1gInSc6
<g/>
,	,	kIx,
popraveno	popraven	k2eAgNnSc4d1
celé	celý	k2eAgNnSc4d1
vedení	vedení	k1gNnSc4
města	město	k1gNnSc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
prvního	první	k4xOgMnSc2
tajemníka	tajemník	k1gMnSc2
městského	městský	k2eAgInSc2d1
výboru	výbor	k1gInSc2
VKS	VKS	kA
<g/>
(	(	kIx(
<g/>
b	b	k?
<g/>
)	)	kIx)
Pjotra	Pjotr	k1gMnSc2
Sergejeviče	Sergejevič	k1gMnSc2
Popkova	Popkův	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
(	(	kIx(
<g/>
23	#num#	k4
<g/>
)	)	kIx)
tehdejších	tehdejší	k2eAgMnPc2d1
vůdců	vůdce	k1gMnPc2
bylo	být	k5eAaImAgNnS
odsouzeno	odsouzet	k5eAaImNgNnS,k5eAaPmNgNnS
k	k	k7c3
trestu	trest	k1gInSc3
smrti	smrt	k1gFnSc2
<g/>
,	,	kIx,
dalších	další	k2eAgFnPc2d1
181	#num#	k4
jich	on	k3xPp3gFnPc2
putovalo	putovat	k5eAaImAgNnS
do	do	k7c2
vězení	vězení	k1gNnSc2
nebo	nebo	k8xC
do	do	k7c2
vyhnanství	vyhnanství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Metro	metro	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
dnes	dnes	k6eAd1
tvoří	tvořit	k5eAaImIp3nP
pět	pět	k4xCc4
linek	linka	k1gFnPc2
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
uvedeno	uvést	k5eAaPmNgNnS
do	do	k7c2
provozu	provoz	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1955	#num#	k4
(	(	kIx(
<g/>
s	s	k7c7
prvními	první	k4xOgInPc7
osmi	osm	k4xCc7
stanicemi	stanice	k1gFnPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
zatopení	zatopení	k1gNnSc3
tunelu	tunel	k1gInSc2
uprostřed	uprostřed	k7c2
linky	linka	k1gFnSc2
Kirovsko-Vyborgskaja	Kirovsko-Vyborgskaja	k1gFnSc1
<g/>
,	,	kIx,
ta	ten	k3xDgFnSc1
byla	být	k5eAaImAgFnS
poté	poté	k6eAd1
rozdělena	rozdělen	k2eAgFnSc1d1
(	(	kIx(
<g/>
na	na	k7c4
příštích	příští	k2eAgNnPc2d1
devět	devět	k4xCc4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
do	do	k7c2
dvou	dva	k4xCgInPc2
samostatných	samostatný	k2eAgFnPc2d1
částí	část	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dubnu	duben	k1gInSc6
2017	#num#	k4
došlo	dojít	k5eAaPmAgNnS
mezi	mezi	k7c7
stanicemi	stanice	k1gFnPc7
Technologičeskij	Technologičeskij	k1gFnSc1
institut	institut	k1gInSc1
a	a	k8xC
Sennaja	Sennaja	k1gFnSc1
ploščaď	ploščadit	k5eAaPmRp2nS
k	k	k7c3
bombovému	bombový	k2eAgInSc3d1
útoku	útok	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
šedesátých	šedesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
bylo	být	k5eAaImAgNnS
ve	v	k7c6
městě	město	k1gNnSc6
obnoveno	obnovit	k5eAaPmNgNnS
<g/>
,	,	kIx,
a	a	k8xC
otevřeno	otevřít	k5eAaPmNgNnS
veřejnosti	veřejnost	k1gFnSc3
<g/>
,	,	kIx,
muzeum	muzeum	k1gNnSc4
malíře	malíř	k1gMnSc2
Ilji	Ilja	k1gMnSc2
Repina	Repin	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
se	se	k3xPyFc4
po	po	k7c6
referendu	referendum	k1gNnSc6
město	město	k1gNnSc4
navrátilo	navrátit	k5eAaPmAgNnS
ke	k	k7c3
svému	svůj	k3xOyFgInSc3
původnímu	původní	k2eAgInSc3d1
názvu	název	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgInSc1d1
<g/>
,	,	kIx,
nebo	nebo	k8xC
nové	nový	k2eAgInPc1d1
názvy	název	k1gInPc1
<g/>
,	,	kIx,
dostalo	dostat	k5eAaPmAgNnS
také	také	k9
39	#num#	k4
ulic	ulice	k1gFnPc2
<g/>
,	,	kIx,
šest	šest	k4xCc4
mostů	most	k1gInPc2
<g/>
,	,	kIx,
tři	tři	k4xCgFnPc4
stanice	stanice	k1gFnPc4
metra	metro	k1gNnSc2
a	a	k8xC
šest	šest	k4xCc1
parků	park	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Kateřinském	kateřinský	k2eAgInSc6d1
paláci	palác	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
Carském	carský	k2eAgNnSc6d1
Selu	selo	k1gNnSc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
od	od	k7c2
roku	rok	k1gInSc2
2003	#num#	k4
nachází	nacházet	k5eAaImIp3nS
věrná	věrný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
Jantarové	jantarový	k2eAgFnSc2d1
komnaty	komnata	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgNnPc1
zde	zde	k6eAd1
existovala	existovat	k5eAaImAgNnP
od	od	k7c2
roku	rok	k1gInSc2
1716	#num#	k4
(	(	kIx(
<g/>
téměř	téměř	k6eAd1
dvě	dva	k4xCgNnPc4
stě	sto	k4xCgFnPc1
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
během	během	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
zmizela	zmizet	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Památky	památka	k1gFnPc1
a	a	k8xC
pamětihodnosti	pamětihodnost	k1gFnPc1
</s>
<s>
Katedrála	katedrála	k1gFnSc1
sv.	sv.	kA
Izáka	Izák	k1gMnSc2
</s>
<s>
Petrohrad	Petrohrad	k1gInSc1
je	být	k5eAaImIp3nS
městem	město	k1gNnSc7
s	s	k7c7
mnoha	mnoho	k4c7
historicky	historicky	k6eAd1
významnými	významný	k2eAgFnPc7d1
památkami	památka	k1gFnPc7
<g/>
,	,	kIx,
také	také	k9
se	s	k7c7
širokými	široký	k2eAgInPc7d1
bulváry	bulvár	k1gInPc7
<g/>
,	,	kIx,
parky	park	k1gInPc7
a	a	k8xC
pobřežními	pobřežní	k2eAgFnPc7d1
promenádami	promenáda	k1gFnPc7
<g/>
,	,	kIx,
a	a	k8xC
stále	stále	k6eAd1
vyzařuje	vyzařovat	k5eAaImIp3nS
své	svůj	k3xOyFgFnPc4
otevřené	otevřený	k2eAgFnPc4d1
<g/>
,	,	kIx,
seversky	seversky	k6eAd1
svěží	svěží	k2eAgNnSc1d1
fluidum	fluidum	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proslulé	proslulý	k2eAgInPc1d1
(	(	kIx(
<g/>
a	a	k8xC
turisticky	turisticky	k6eAd1
atraktivní	atraktivní	k2eAgFnSc4d1
<g/>
)	)	kIx)
letní	letní	k2eAgFnSc4d1
„	„	k?
<g/>
bílé	bílý	k2eAgFnSc2d1
noci	noc	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
fenoménem	fenomén	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
přírodní	přírodní	k2eAgInSc4d1
jev	jev	k1gInSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
slunce	slunce	k1gNnSc2
na	na	k7c4
pár	pár	k4xCyI
hodin	hodina	k1gFnPc2
zapadá	zapadat	k5eAaImIp3nS,k5eAaPmIp3nS
za	za	k7c4
obzor	obzor	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
úplná	úplný	k2eAgFnSc1d1
tma	tma	k1gFnSc1
nikdy	nikdy	k6eAd1
nenastane	nastat	k5eNaPmIp3nS
<g/>
,	,	kIx,
díky	díky	k7c3
blízkosti	blízkost	k1gFnSc3
polárního	polární	k2eAgInSc2d1
kruhu	kruh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Katedrála	katedrála	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Izáka	Izák	k1gMnSc2
(	(	kIx(
<g/>
stavělo	stavět	k5eAaImAgNnS
jí	on	k3xPp3gFnSc3
440	#num#	k4
tisíc	tisíc	k4xCgInSc4
dělníků	dělník	k1gMnPc2
40	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
1816	#num#	k4
-	-	kIx~
1858	#num#	k4
<g/>
)	)	kIx)
měří	měřit	k5eAaImIp3nS
na	na	k7c4
výšku	výška	k1gFnSc4
102	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zlatá	zlatý	k2eAgFnSc1d1
kupole	kupole	k1gFnSc1
je	být	k5eAaImIp3nS
obložena	obložen	k2eAgFnSc1d1
100	#num#	k4
kg	kg	kA
čistého	čistý	k2eAgNnSc2d1
zlata	zlato	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nabízí	nabízet	k5eAaImIp3nS
výhled	výhled	k1gInSc4
na	na	k7c4
nejkrásnější	krásný	k2eAgNnPc4d3
místa	místo	k1gNnPc4
historického	historický	k2eAgNnSc2d1
centra	centrum	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
uvnitř	uvnitř	k7c2
14	#num#	k4
tisíc	tisíc	k4xCgInPc2
míst	místo	k1gNnPc2
pro	pro	k7c4
návštěvníky	návštěvník	k1gMnPc4
<g/>
,	,	kIx,
a	a	k8xC
skvostný	skvostný	k2eAgInSc1d1
pohled	pohled	k1gInSc1
na	na	k7c4
skutečnou	skutečný	k2eAgFnSc4d1
symfonii	symfonie	k1gFnSc4
z	z	k7c2
mramoru	mramor	k1gInSc2
<g/>
,	,	kIx,
drahokamů	drahokam	k1gInPc2
a	a	k8xC
polodrahokamů	polodrahokam	k1gInPc2
všech	všecek	k3xTgFnPc2
barev	barva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Tzv.	tzv.	kA
Zimní	zimní	k2eAgInSc4d1
palác	palác	k1gInSc4
nechala	nechat	k5eAaPmAgFnS
postavit	postavit	k5eAaPmF
dcera	dcera	k1gFnSc1
Petra	Petr	k1gMnSc2
Velikého	veliký	k2eAgMnSc2d1
<g/>
,	,	kIx,
Alžběta	Alžběta	k1gFnSc1
<g/>
,	,	kIx,
ve	v	k7c6
stylu	styl	k1gInSc6
italského	italský	k2eAgNnSc2d1
baroka	baroko	k1gNnSc2
architektem	architekt	k1gMnSc7
Bartolomeem	Bartolomeus	k1gMnSc7
Rastrellim	Rastrellim	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
bílé	bílý	k2eAgFnSc6d1
a	a	k8xC
zelené	zelený	k2eAgFnSc6d1
barvě	barva	k1gFnSc6
<g/>
,	,	kIx,
později	pozdě	k6eAd2
doplněného	doplněný	k2eAgInSc2d1
klasicistní	klasicistní	k2eAgNnSc4d1
přístavbou	přístavba	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kateřina	Kateřina	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veliká	veliký	k2eAgFnSc1d1
z	z	k7c2
budovy	budova	k1gFnSc2
učinila	učinit	k5eAaImAgFnS,k5eAaPmAgFnS
roku	rok	k1gInSc2
1764	#num#	k4
galerii	galerie	k1gFnSc6
Ermitáž	Ermitáž	k1gFnSc4
a	a	k8xC
umělecké	umělecký	k2eAgInPc4d1
skvosty	skvost	k1gInPc4
pro	pro	k7c4
ni	on	k3xPp3gFnSc4
nakupovala	nakupovat	k5eAaBmAgFnS
v	v	k7c6
celé	celý	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc1
exponátů	exponát	k1gInPc2
přesahuje	přesahovat	k5eAaImIp3nS
tři	tři	k4xCgNnPc1
milióny	milión	k4xCgInPc1
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
část	část	k1gFnSc1
je	být	k5eAaImIp3nS
přístupná	přístupný	k2eAgFnSc1d1
veřejnosti	veřejnost	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc1
obrazů	obraz	k1gInPc2
řadí	řadit	k5eAaImIp3nS
Ermitáž	Ermitáž	k1gFnSc1
mezi	mezi	k7c4
největší	veliký	k2eAgFnPc4d3
obrazárny	obrazárna	k1gFnPc4
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
byste	by	kYmCp2nP
chtěli	chtít	k5eAaImAgMnP
projít	projít	k5eAaPmF
všechny	všechen	k3xTgInPc4
sály	sál	k1gInPc4
s	s	k7c7
exponáty	exponát	k1gInPc7
<g/>
,	,	kIx,
čeká	čekat	k5eAaImIp3nS
vás	vy	k3xPp2nPc4
10	#num#	k4
km	km	kA
dlouhá	dlouhý	k2eAgFnSc1d1
trasa	trasa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
nejoblíbenější	oblíbený	k2eAgFnPc4d3
atrakce	atrakce	k1gFnPc4
patří	patřit	k5eAaImIp3nS
první	první	k4xOgFnSc1
stavba	stavba	k1gFnSc1
v	v	k7c6
Petrohradu	Petrohrad	k1gInSc2
–	–	k?
Petropavlovská	petropavlovský	k2eAgFnSc1d1
pevnost	pevnost	k1gFnSc1
s	s	k7c7
Katedrálou	katedrála	k1gFnSc7
Petra	Petr	k1gMnSc2
a	a	k8xC
Pavla	Pavel	k1gMnSc2
<g/>
,	,	kIx,
palác	palác	k1gInSc4
Admirality	admiralita	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
svou	svůj	k3xOyFgFnSc4
věží	věžit	k5eAaImIp3nS
dominuje	dominovat	k5eAaImIp3nS
městu	město	k1gNnSc3
<g/>
,	,	kIx,
Triumfální	triumfální	k2eAgInSc1d1
oblouk	oblouk	k1gInSc1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
památka	památka	k1gFnSc1
na	na	k7c6
vítězství	vítězství	k1gNnSc6
nad	nad	k7c7
Napoleonem	napoleon	k1gInSc7
<g/>
,	,	kIx,
žulový	žulový	k2eAgInSc1d1
monolit	monolit	k1gInSc1
–	–	k?
Alexandrův	Alexandrův	k2eAgInSc1d1
sloup	sloup	k1gInSc1
<g/>
,	,	kIx,
socha	socha	k1gFnSc1
Petra	Petr	k1gMnSc2
Velikého	veliký	k2eAgMnSc2d1
<g/>
,	,	kIx,
zvaná	zvaný	k2eAgNnPc1d1
Měděný	měděný	k2eAgMnSc1d1
jezdec	jezdec	k1gMnSc1
<g/>
,	,	kIx,
Kazaňský	kazaňský	k2eAgInSc1d1
chrám	chrám	k1gInSc1
<g/>
,	,	kIx,
Křižník	křižník	k1gInSc1
Aurora	Aurora	k1gFnSc1
<g/>
,	,	kIx,
Vasiljevský	Vasiljevský	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
<g/>
,	,	kIx,
Smolenská	Smolenský	k2eAgFnSc1d1
katedrála	katedrála	k1gFnSc1
a	a	k8xC
další	další	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Nejnavštěvovanějším	navštěvovaný	k2eAgInSc7d3
bulvárem	bulvár	k1gInSc7
města	město	k1gNnSc2
je	být	k5eAaImIp3nS
4	#num#	k4
km	km	kA
dlouhý	dlouhý	k2eAgInSc1d1
Něvský	něvský	k2eAgInSc1d1
prospekt	prospekt	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Paláce	palác	k1gInPc1
a	a	k8xC
úřední	úřední	k2eAgFnPc1d1
budovy	budova	k1gFnPc1
</s>
<s>
Zimní	zimní	k2eAgInSc1d1
palác	palác	k1gInSc1
s	s	k7c7
muzeem	muzeum	k1gNnSc7
Ermitáž	Ermitáž	k1gFnSc4
<g/>
,	,	kIx,
zahrnujícím	zahrnující	k2eAgMnSc6d1
přes	přes	k7c4
3	#num#	k4
miliony	milion	k4xCgInPc4
položek	položka	k1gFnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
největší	veliký	k2eAgFnSc2d3
kolekce	kolekce	k1gFnSc2
obrazů	obraz	k1gInPc2
na	na	k7c6
světě	svět	k1gInSc6
</s>
<s>
Muzeum	muzeum	k1gNnSc1
Kunstkamera	Kunstkamero	k1gNnSc2
</s>
<s>
Dům	dům	k1gInSc1
knihy	kniha	k1gFnSc2
(	(	kIx(
<g/>
Singer	Singer	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
sídlo	sídlo	k1gNnSc1
VKontakte	VKontakt	k1gInSc5
</s>
<s>
Admiralita	admiralita	k1gFnSc1
(	(	kIx(
<g/>
ul	ul	kA
<g/>
.	.	kIx.
Admiralitějskaja	Admiralitějskaja	k1gMnSc1
naběrežnaja	naběrežnaja	k1gMnSc1
<g/>
)	)	kIx)
–	–	k?
původně	původně	k6eAd1
zde	zde	k6eAd1
byly	být	k5eAaImAgFnP
od	od	k7c2
roku	rok	k1gInSc2
1704	#num#	k4
loděnice	loděnice	k1gFnSc2
<g/>
,	,	kIx,
založené	založený	k2eAgNnSc1d1
Petrem	Petr	k1gMnSc7
I.	I.	kA
Velikým	veliký	k2eAgFnPc3d1
<g/>
,	,	kIx,
ve	v	k7c6
stejné	stejný	k2eAgFnSc6d1
době	doba	k1gFnSc6
vznikla	vzniknout	k5eAaPmAgFnS
dřevěná	dřevěný	k2eAgFnSc1d1
budova	budova	k1gFnSc1
Admiralitního	admiralitní	k2eAgNnSc2d1
kolegia	kolegium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
20	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
30	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byla	být	k5eAaImAgFnS
budova	budova	k1gFnSc1
vyzděna	vyzdít	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
roky	rok	k1gInPc4
1806	#num#	k4
-	-	kIx~
1823	#num#	k4
byla	být	k5eAaImAgFnS
přestavěna	přestavět	k5eAaPmNgFnS
Andrejem	Andrej	k1gMnSc7
Zacharovem	Zacharov	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Zimní	zimní	k2eAgInSc1d1
palác	palác	k1gInSc1
(	(	kIx(
<g/>
ul	ul	kA
<g/>
.	.	kIx.
Dvorcovaja	Dvorcovaja	k1gMnSc1
naběrežnaja	naběrežnaja	k1gMnSc1
<g/>
)	)	kIx)
–	–	k?
barokní	barokní	k2eAgFnSc1d1
budova	budova	k1gFnSc1
byla	být	k5eAaImAgFnS
vystavěna	vystavět	k5eAaPmNgFnS
v	v	k7c6
letech	léto	k1gNnPc6
1754	#num#	k4
-	-	kIx~
1762	#num#	k4
<g/>
,	,	kIx,
dle	dle	k7c2
návrhu	návrh	k1gInSc2
architekta	architekt	k1gMnSc2
Bartolomea	Bartolomeus	k1gMnSc2
Rastrelliho	Rastrelli	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Palác	palác	k1gInSc1
má	mít	k5eAaImIp3nS
1	#num#	k4
500	#num#	k4
místností	místnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
1917	#num#	k4
sloužil	sloužit	k5eAaImAgInS
jako	jako	k8xS,k8xC
carská	carský	k2eAgFnSc1d1
residence	residence	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1922	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
součástí	součást	k1gFnSc7
musea	museum	k1gNnSc2
Ermitáž	Ermitáž	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Museum	museum	k1gNnSc1
Ermitáž	Ermitáž	k1gFnSc1
(	(	kIx(
<g/>
Dvorcovaja	Dvorcovaja	k1gFnSc1
ploščaď	ploščadit	k5eAaPmRp2nS
<g/>
)	)	kIx)
–	–	k?
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
pěti	pět	k4xCc2
budov	budova	k1gFnPc2
<g/>
,	,	kIx,
první	první	k4xOgFnSc4
je	být	k5eAaImIp3nS
Zimní	zimní	k2eAgInSc1d1
palác	palác	k1gInSc1
<g/>
,	,	kIx,
další	další	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
Malá	malý	k2eAgFnSc1d1
Ermitáž	Ermitáž	k1gFnSc1
(	(	kIx(
<g/>
postavena	postaven	k2eAgFnSc1d1
v	v	k7c6
letech	léto	k1gNnPc6
1764	#num#	k4
-	-	kIx~
1775	#num#	k4
pro	pro	k7c4
sbírku	sbírka	k1gFnSc4
obrazů	obraz	k1gInPc2
<g/>
,	,	kIx,
koupených	koupený	k2eAgInPc2d1
Kateřinou	Kateřina	k1gFnSc7
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velikou	veliký	k2eAgFnSc4d1
v	v	k7c6
roce	rok	k1gInSc6
1764	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Stará	starý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Velká	velký	k2eAgFnSc1d1
<g/>
)	)	kIx)
Ermitáž	Ermitáž	k1gFnSc1
(	(	kIx(
<g/>
postavena	postaven	k2eAgFnSc1d1
v	v	k7c6
letech	léto	k1gNnPc6
1771	#num#	k4
-	-	kIx~
1781	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Nová	nový	k2eAgFnSc1d1
Ermitáž	Ermitáž	k1gFnSc1
(	(	kIx(
<g/>
postavena	postaven	k2eAgFnSc1d1
v	v	k7c6
letech	léto	k1gNnPc6
1839	#num#	k4
-	-	kIx~
1851	#num#	k4
<g/>
,	,	kIx,
vstup	vstup	k1gInSc1
dekoruje	dekorovat	k5eAaBmIp3nS
portikus	portikus	k1gInSc4
s	s	k7c7
atlanty	atlant	k1gInPc7
z	z	k7c2
granitu	granit	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
divadlo	divadlo	k1gNnSc4
Ermitáž	Ermitáž	k1gFnSc1
(	(	kIx(
<g/>
postaveno	postavit	k5eAaPmNgNnS
v	v	k7c6
letech	léto	k1gNnPc6
1783	#num#	k4
-	-	kIx~
1789	#num#	k4
na	na	k7c6
místě	místo	k1gNnSc6
zimního	zimní	k2eAgInSc2d1
paláce	palác	k1gInSc2
cara	car	k1gMnSc2
Petra	Petr	k1gMnSc2
I.	I.	kA
Velikého	veliký	k2eAgInSc2d1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
zde	zde	k6eAd1
i	i	k9
zemřel	zemřít	k5eAaPmAgMnS
<g/>
,	,	kIx,
z	z	k7c2
tohoto	tento	k3xDgInSc2
paláce	palác	k1gInSc2
se	se	k3xPyFc4
zachovalo	zachovat	k5eAaPmAgNnS
několik	několik	k4yIc1
místností	místnost	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Budova	budova	k1gFnSc1
generálního	generální	k2eAgInSc2d1
štábu	štáb	k1gInSc2
(	(	kIx(
<g/>
Dvorcovaja	Dvorcovaja	k1gFnSc1
ploščaď	ploščadit	k5eAaPmRp2nS
<g/>
)	)	kIx)
–	–	k?
klasicistní	klasicistní	k2eAgFnSc1d1
budova	budova	k1gFnSc1
<g/>
,	,	kIx,
podle	podle	k7c2
návrhu	návrh	k1gInSc2
Carla	Carl	k1gMnSc2
Rossiho	Rossi	k1gMnSc2
<g/>
,	,	kIx,
z	z	k7c2
roku	rok	k1gInSc2
1827	#num#	k4
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
300	#num#	k4
metrů	metr	k1gInPc2
dlouhou	dlouhý	k2eAgFnSc7d1
<g/>
,	,	kIx,
do	do	k7c2
oblouku	oblouk	k1gInSc2
formovanou	formovaný	k2eAgFnSc4d1
fasádu	fasáda	k1gFnSc4
<g/>
,	,	kIx,
s	s	k7c7
triumfálním	triumfální	k2eAgInSc7d1
obloukem	oblouk	k1gInSc7
<g/>
,	,	kIx,
korunovaným	korunovaný	k2eAgInSc7d1
vozem	vůz	k1gInSc7
vítězství	vítězství	k1gNnSc2
<g/>
,	,	kIx,
symbolem	symbol	k1gInSc7
ruského	ruský	k2eAgNnSc2d1
vítězství	vítězství	k1gNnSc2
nad	nad	k7c7
Napoleonem	napoleon	k1gInSc7
I.	I.	kA
</s>
<s>
Aničkův	Aničkův	k2eAgInSc1d1
palác	palác	k1gInSc1
(	(	kIx(
<g/>
Něvskij	Něvskij	k1gFnSc1
prospekt	prospekt	k1gInSc1
č.	č.	k?
39	#num#	k4
<g/>
)	)	kIx)
–	–	k?
postaven	postaven	k2eAgInSc4d1
ve	v	k7c4
40	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
50	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
pro	pro	k7c4
hraběte	hrabě	k1gMnSc4
Alexeje	Alexej	k1gMnSc4
Grigorjeviče	Grigorjevič	k1gMnSc4
Razumovského	Razumovský	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1794	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
palác	palác	k1gInSc1
sídlem	sídlo	k1gNnSc7
carské	carský	k2eAgFnSc2d1
kanceláře	kancelář	k1gFnSc2
(	(	kIx(
<g/>
kabinet	kabinet	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1816	#num#	k4
začal	začít	k5eAaPmAgInS
palác	palác	k1gInSc1
sloužit	sloužit	k5eAaImF
jako	jako	k9
rezidence	rezidence	k1gFnSc1
velkoknížete	velkokníže	k1gMnSc2
Mikuláše	Mikuláš	k1gMnSc2
Pavloviče	Pavlovič	k1gMnSc2
(	(	kIx(
<g/>
pozdějšího	pozdní	k2eAgMnSc2d2
cara	car	k1gMnSc2
Mikuláše	Mikuláš	k1gMnSc2
I.	I.	kA
Pavloviče	Pavlovič	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Architekt	architekt	k1gMnSc1
Carlo	Carlo	k1gNnSc1
Rossi	Rosse	k1gFnSc4
palác	palác	k1gInSc1
upravil	upravit	k5eAaPmAgInS
a	a	k8xC
rekonstruoval	rekonstruovat	k5eAaBmAgInS
zahradu	zahrada	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podél	podél	k7c2
západní	západní	k2eAgFnSc2d1
strany	strana	k1gFnSc2
zahrady	zahrada	k1gFnSc2
umístil	umístit	k5eAaPmAgMnS
dva	dva	k4xCgInPc4
empírové	empírový	k2eAgInPc4d1
pavilony	pavilon	k1gInPc4
<g/>
,	,	kIx,
se	s	k7c7
sochami	socha	k1gFnPc7
starých	starý	k2eAgMnPc2d1
ruských	ruský	k2eAgMnPc2d1
válečníků	válečník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Beloselsky-Belozersky	Beloselsky-Belozersky	k6eAd1
palác	palác	k1gInSc1
(	(	kIx(
<g/>
Něvskij	Něvskij	k1gFnSc1
prospekt	prospekt	k1gInSc1
č.	č.	k?
41	#num#	k4
<g/>
)	)	kIx)
–	–	k?
klasicistní	klasicistní	k2eAgFnSc1d1
budova	budova	k1gFnSc1
<g/>
,	,	kIx,
postavená	postavený	k2eAgFnSc1d1
na	na	k7c6
sklonku	sklonek	k1gInSc6
40	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Jusupovský	Jusupovský	k2eAgInSc1d1
palác	palác	k1gInSc1
(	(	kIx(
<g/>
ul	ul	kA
<g/>
.	.	kIx.
Naběrežnaja	Naběrežnaja	k1gMnSc1
reki	rek	k1gFnSc2
Mojki	Mojk	k1gFnSc2
č.	č.	k?
94	#num#	k4
<g/>
)	)	kIx)
–	–	k?
klasicistní	klasicistní	k2eAgFnSc1d1
budova	budova	k1gFnSc1
<g/>
,	,	kIx,
postavená	postavený	k2eAgFnSc1d1
v	v	k7c6
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
pro	pro	k7c4
hraběte	hrabě	k1gMnSc4
Šuvalova	Šuvalův	k2eAgMnSc4d1
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1830	#num#	k4
ho	on	k3xPp3gMnSc4
získala	získat	k5eAaPmAgFnS
knížata	kníže	k1gMnPc4wR
Jusopovská	Jusopovský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Palác	palác	k1gInSc1
proslul	proslout	k5eAaPmAgInS
svým	svůj	k3xOyFgNnSc7
divadlem	divadlo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
17	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1916	#num#	k4
zde	zde	k6eAd1
byl	být	k5eAaImAgInS
zavražděn	zavraždit	k5eAaPmNgMnS
Grigorij	Grigorij	k1gMnSc1
Jefimovič	Jefimovič	k1gMnSc1
Rasputin	Rasputin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Kunstkamera	Kunstkamera	k1gFnSc1
(	(	kIx(
<g/>
ul	ul	kA
<g/>
.	.	kIx.
Universitětskaja	Universitětskaj	k2eAgFnSc1d1
naběrežnaja	naběrežnaja	k1gFnSc1
č.	č.	k?
3	#num#	k4
<g/>
)	)	kIx)
–	–	k?
první	první	k4xOgNnSc1
ruské	ruský	k2eAgNnSc1d1
museum	museum	k1gNnSc1
<g/>
,	,	kIx,
založené	založený	k2eAgNnSc1d1
carem	car	k1gMnSc7
Petrem	Petr	k1gMnSc7
I.	I.	kA
Velikým	veliký	k2eAgNnSc7d1
v	v	k7c6
10	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
budova	budova	k1gFnSc1
postavena	postaven	k2eAgFnSc1d1
v	v	k7c6
letech	léto	k1gNnPc6
1718	#num#	k4
-	-	kIx~
1734	#num#	k4
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
muzeum	muzeum	k1gNnSc1
přírodních	přírodní	k2eAgFnPc2d1
věd	věda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Expozice	expozice	k1gFnSc1
je	být	k5eAaImIp3nS
rozdělená	rozdělený	k2eAgFnSc1d1
do	do	k7c2
několika	několik	k4yIc2
částí	část	k1gFnPc2
<g/>
:	:	kIx,
Indie	Indie	k1gFnSc1
<g/>
,	,	kIx,
Afrika	Afrika	k1gFnSc1
<g/>
,	,	kIx,
Čína	Čína	k1gFnSc1
<g/>
,	,	kIx,
Japonsko	Japonsko	k1gNnSc1
<g/>
,	,	kIx,
Blízký	blízký	k2eAgInSc1d1
východ	východ	k1gInSc1
a	a	k8xC
Severní	severní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
hala	hala	k1gFnSc1
je	být	k5eAaImIp3nS
také	také	k9
věnovaná	věnovaný	k2eAgFnSc1d1
výstavě	výstava	k1gFnSc6
deformovaných	deformovaný	k2eAgNnPc2d1
embryí	embryo	k1gNnPc2
<g/>
,	,	kIx,
dětí	dítě	k1gFnPc2
a	a	k8xC
zvířat	zvíře	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Mariinský	Mariinský	k2eAgInSc1d1
palác	palác	k1gInSc1
(	(	kIx(
<g/>
Isaakijevskaja	Isaakijevskaja	k1gFnSc1
ploščaď	ploščadit	k5eAaPmRp2nS
<g/>
)	)	kIx)
–	–	k?
klasicistní	klasicistní	k2eAgFnSc1d1
budova	budova	k1gFnSc1
<g/>
,	,	kIx,
postavená	postavený	k2eAgFnSc1d1
v	v	k7c6
letech	léto	k1gNnPc6
1839	#num#	k4
-	-	kIx~
1840	#num#	k4
<g/>
,	,	kIx,
podle	podle	k7c2
návrhu	návrh	k1gInSc2
architekta	architekt	k1gMnSc2
Andreje	Andrej	k1gMnSc2
Stackenschneidera	Stackenschneider	k1gMnSc2
<g/>
,	,	kIx,
pro	pro	k7c4
velkokněžnu	velkokněžna	k1gFnSc4
Marii	Maria	k1gFnSc4
Nikolajevnu	Nikolajevna	k1gFnSc4
(	(	kIx(
<g/>
dceru	dcera	k1gFnSc4
cara	car	k1gMnSc2
Mikuláše	Mikuláš	k1gMnSc2
I.	I.	kA
<g/>
)	)	kIx)
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
změněna	změnit	k5eAaPmNgFnS
na	na	k7c4
sídlo	sídlo	k1gNnSc4
Státní	státní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
zde	zde	k6eAd1
sídlí	sídlet	k5eAaImIp3nS
petrohradská	petrohradský	k2eAgFnSc1d1
duma	duma	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Menšikovův	Menšikovův	k2eAgInSc1d1
palác	palác	k1gInSc1
(	(	kIx(
<g/>
ul	ul	kA
<g/>
.	.	kIx.
Universitětskaja	Universitětskaj	k2eAgFnSc1d1
naběrežnaja	naběrežnaja	k1gFnSc1
č.	č.	k?
15	#num#	k4
<g/>
)	)	kIx)
–	–	k?
první	první	k4xOgFnSc1
zděná	zděný	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
v	v	k7c6
Petrohradě	Petrohrad	k1gInSc6
<g/>
,	,	kIx,
palác	palác	k1gInSc1
je	být	k5eAaImIp3nS
známý	známý	k2eAgMnSc1d1
svými	svůj	k3xOyFgInPc7
unikátními	unikátní	k2eAgInPc7d1
interiéry	interiér	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Michajlovský	Michajlovský	k2eAgInSc1d1
palác	palác	k1gInSc1
–	–	k?
klasicistní	klasicistní	k2eAgFnSc1d1
budova	budova	k1gFnSc1
<g/>
,	,	kIx,
vzniklá	vzniklý	k2eAgFnSc1d1
v	v	k7c6
letech	léto	k1gNnPc6
1819	#num#	k4
-	-	kIx~
1825	#num#	k4
dle	dle	k7c2
návrhu	návrh	k1gInSc2
architekta	architekt	k1gMnSc2
Carla	Carl	k1gMnSc2
Rossiho	Rossi	k1gMnSc2
<g/>
,	,	kIx,
pro	pro	k7c4
velkoknížete	velkokníže	k1gNnSc4wR
Michajla	Michajl	k1gMnSc2
Pavloviče	Pavlovič	k1gMnSc2
(	(	kIx(
<g/>
mladšího	mladý	k2eAgMnSc2d2
syna	syn	k1gMnSc2
cara	car	k1gMnSc2
Pavla	Pavel	k1gMnSc2
I.	I.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
budově	budova	k1gFnSc3
přiléhá	přiléhat	k5eAaImIp3nS
park	park	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1894	#num#	k4
byl	být	k5eAaImAgInS
palác	palác	k1gInSc1
předán	předat	k5eAaPmNgInS
Ruskému	ruský	k2eAgNnSc3d1
museu	museum	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Michajlovský	Michajlovský	k2eAgInSc4d1
(	(	kIx(
<g/>
Inženýrský	inženýrský	k2eAgInSc4d1
<g/>
)	)	kIx)
zámek	zámek	k1gInSc4
(	(	kIx(
<g/>
Sadovaja	Sadovaj	k2eAgFnSc1d1
ulica	ulica	k1gFnSc1
č.	č.	k?
2	#num#	k4
<g/>
)	)	kIx)
–	–	k?
rezidence	rezidence	k1gFnSc2
cara	car	k1gMnSc4
Pavla	Pavel	k1gMnSc4
I.	I.	kA
stojí	stát	k5eAaImIp3nS
na	na	k7c6
místě	místo	k1gNnSc6
staršího	starý	k2eAgInSc2d2
letního	letní	k2eAgInSc2d1
paláce	palác	k1gInSc2
carevny	carevna	k1gFnSc2
Alžběty	Alžběta	k1gFnSc2
Petrovny	Petrovna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zámek	zámek	k1gInSc1
byl	být	k5eAaImAgInS
navržen	navrhnout	k5eAaPmNgInS
Vincencem	Vincenc	k1gMnSc7
Brennem	Brenn	k1gMnSc7
v	v	k7c6
roce	rok	k1gInSc6
1796	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1801	#num#	k4
se	se	k3xPyFc4
do	do	k7c2
něj	on	k3xPp3gMnSc2
car	car	k1gMnSc1
Pavel	Pavel	k1gMnSc1
I.	I.	kA
s	s	k7c7
rodinou	rodina	k1gFnSc7
nastěhoval	nastěhovat	k5eAaPmAgInS
a	a	k8xC
byl	být	k5eAaImAgInS
zde	zde	k6eAd1
zanedlouho	zanedlouho	k6eAd1
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
ložnici	ložnice	k1gFnSc6
zavražděn	zavraždit	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1820	#num#	k4
byl	být	k5eAaImAgInS
zámek	zámek	k1gInSc1
předán	předat	k5eAaPmNgInS
do	do	k7c2
užívání	užívání	k1gNnSc2
Inženýrské	inženýrský	k2eAgFnSc6d1
škole	škola	k1gFnSc6
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
navštěvoval	navštěvovat	k5eAaImAgMnS
například	například	k6eAd1
Fjodor	Fjodor	k1gMnSc1
Michajlovič	Michajlovič	k1gMnSc1
Dostojevskij	Dostojevskij	k1gMnSc1
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
Michajlovič	Michajlovič	k1gMnSc1
Sečenov	Sečenovo	k1gNnPc2
nebo	nebo	k8xC
hrabě	hrabě	k1gMnSc1
Eduard	Eduard	k1gMnSc1
Totleben	Totleben	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Městská	městský	k2eAgFnSc1d1
duma	duma	k1gFnSc1
s	s	k7c7
hodinovou	hodinový	k2eAgFnSc7d1
věží	věž	k1gFnSc7
(	(	kIx(
<g/>
Něvskij	Něvskij	k1gFnSc1
prospekt	prospekt	k1gInSc1
č.	č.	k?
33	#num#	k4
<g/>
/	/	kIx~
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Mramorový	mramorový	k2eAgInSc1d1
palác	palác	k1gInSc1
(	(	kIx(
<g/>
Millijonnaja	Millijonnaj	k2eAgFnSc1d1
ulica	ulica	k1gFnSc1
č.	č.	k?
5	#num#	k4
<g/>
/	/	kIx~
<g/>
1	#num#	k4
<g/>
)	)	kIx)
–	–	k?
palác	palác	k1gInSc1
<g/>
,	,	kIx,
postavený	postavený	k2eAgInSc1d1
na	na	k7c4
příkaz	příkaz	k1gInSc4
Kateřiny	Kateřina	k1gFnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veliké	veliký	k2eAgFnPc4d1
<g/>
,	,	kIx,
pro	pro	k7c4
jejího	její	k3xOp3gMnSc4
favorita	favorit	k1gMnSc4
Grigorije	Grigorije	k1gFnSc2
Orlova	Orlův	k2eAgInSc2d1
<g/>
,	,	kIx,
dle	dle	k7c2
návrhu	návrh	k1gInSc2
Antonia	Antonio	k1gMnSc2
Rinaldiho	Rinaldi	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc1
odvozen	odvozen	k2eAgInSc1d1
od	od	k7c2
mramorových	mramorový	k2eAgFnPc2d1
fasád	fasáda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgInSc1d1
schodiště	schodiště	k1gNnSc2
a	a	k8xC
mramorová	mramorový	k2eAgFnSc1d1
hala	hala	k1gFnSc1
zůstaly	zůstat	k5eAaPmAgFnP
od	od	k7c2
dob	doba	k1gFnPc2
Kateřiny	Kateřina	k1gFnSc2
II	II	kA
<g/>
.	.	kIx.
nezměněny	změnit	k5eNaPmNgInP
<g/>
.	.	kIx.
</s>
<s>
Palác	palác	k1gInSc1
Kikin	Kikin	k1gInSc1
(	(	kIx(
<g/>
Stavropolskaja	Stavropolskaj	k2eAgFnSc1d1
ulica	ulica	k1gFnSc1
č.	č.	k?
9	#num#	k4
<g/>
)	)	kIx)
–	–	k?
palác	palác	k1gInSc1
<g/>
,	,	kIx,
vystavěný	vystavěný	k2eAgInSc1d1
v	v	k7c6
letech	léto	k1gNnPc6
1714	#num#	k4
-	-	kIx~
1720	#num#	k4
pro	pro	k7c4
Alexandra	Alexandr	k1gMnSc4
Kikina	Kikin	k1gMnSc4
<g/>
,	,	kIx,
v	v	k7c6
letech	léto	k1gNnPc6
1719	#num#	k4
-	-	kIx~
1727	#num#	k4
zde	zde	k6eAd1
byly	být	k5eAaImAgFnP
sbírky	sbírka	k1gFnPc1
a	a	k8xC
knihovna	knihovna	k1gFnSc1
cara	car	k1gMnSc2
Petra	Petr	k1gMnSc2
I.	I.	kA
</s>
<s>
Smolný	smolný	k2eAgInSc1d1
palác	palác	k1gInSc1
(	(	kIx(
<g/>
ul	ul	kA
<g/>
.	.	kIx.
Smolnyj	Smolnyj	k1gInSc1
projezd	projezd	k1gInSc1
č.	č.	k?
1	#num#	k4
<g/>
)	)	kIx)
–	–	k?
postaven	postavit	k5eAaPmNgMnS
v	v	k7c6
letech	léto	k1gNnPc6
1806	#num#	k4
-	-	kIx~
1808	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1917	#num#	k4
zde	zde	k6eAd1
byla	být	k5eAaImAgFnS
rezidence	rezidence	k1gFnSc1
Sovětu	sovět	k1gInSc2
dělníků	dělník	k1gMnPc2
a	a	k8xC
vojáků	voják	k1gMnPc2
<g/>
,	,	kIx,
bydlel	bydlet	k5eAaImAgMnS
zde	zde	k6eAd1
i	i	k9
Vladimir	Vladimir	k1gMnSc1
Iljič	Iljič	k1gMnSc1
Lenin	Lenin	k1gMnSc1
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
je	být	k5eAaImIp3nS
tu	tu	k6eAd1
petrohradská	petrohradský	k2eAgFnSc1d1
radnice	radnice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Stroganovský	Stroganovský	k2eAgInSc1d1
palác	palác	k1gInSc1
(	(	kIx(
<g/>
Něvskij	Něvskij	k1gFnSc1
prospekt	prospekt	k1gInSc1
č.	č.	k?
17	#num#	k4
<g/>
)	)	kIx)
–	–	k?
postaven	postavit	k5eAaPmNgInS
uprostřed	uprostřed	k7c2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
dle	dle	k7c2
návrhu	návrh	k1gInSc2
Bartolomea	Bartolomeus	k1gMnSc2
Rastrelliho	Rastrelli	k1gMnSc2
<g/>
,	,	kIx,
pro	pro	k7c4
barona	baron	k1gMnSc4
Sergeje	Sergej	k1gMnSc4
Stroganova	Stroganův	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Šeremetěvův	Šeremetěvův	k2eAgInSc1d1
palác	palác	k1gInSc1
(	(	kIx(
<g/>
ul	ul	kA
<g/>
.	.	kIx.
Naběrežnaja	Naběrežnaja	k1gMnSc1
reki	rek	k1gFnSc2
Fontanki	Fontank	k1gFnSc2
č.	č.	k?
34	#num#	k4
<g/>
)	)	kIx)
–	–	k?
palác	palác	k1gInSc1
vojevůdce	vojevůdce	k1gMnSc2
hraběte	hrabě	k1gMnSc2
Borise	Boris	k1gMnSc2
Šeremetěva	Šeremetěv	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Tauridský	Tauridský	k2eAgInSc1d1
palác	palác	k1gInSc1
(	(	kIx(
<g/>
Špalernaja	Špalernaj	k2eAgFnSc1d1
ulica	ulica	k1gFnSc1
č.	č.	k?
47	#num#	k4
<g/>
)	)	kIx)
–	–	k?
postavený	postavený	k2eAgInSc1d1
v	v	k7c6
letech	léto	k1gNnPc6
1782	#num#	k4
-	-	kIx~
1790	#num#	k4
<g/>
,	,	kIx,
na	na	k7c4
příkaz	příkaz	k1gInSc4
carevny	carevna	k1gFnSc2
Kateřiny	Kateřina	k1gFnSc2
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
pro	pro	k7c4
jejího	její	k3xOp3gMnSc4
favorita	favorit	k1gMnSc4
Grigorije	Grigorije	k1gMnSc4
Potěmkina	Potěmkin	k1gMnSc4
<g/>
,	,	kIx,
u	u	k7c2
paláce	palác	k1gInSc2
je	být	k5eAaImIp3nS
velký	velký	k2eAgInSc1d1
park	park	k1gInSc1
(	(	kIx(
<g/>
okolo	okolo	k7c2
30	#num#	k4
hektarů	hektar	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1906	#num#	k4
<g/>
–	–	k?
<g/>
1917	#num#	k4
v	v	k7c6
paláci	palác	k1gInSc6
zasedala	zasedat	k5eAaImAgFnS
Státní	státní	k2eAgFnSc1d1
imperiální	imperiální	k2eAgFnSc1d1
duma	duma	k1gFnSc1
</s>
<s>
Divadla	divadlo	k1gNnSc2
</s>
<s>
Alexandrino	Alexandrin	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
(	(	kIx(
<g/>
Ploščaď	Ploščaď	k1gMnSc1
Ostrovskogo	Ostrovskogo	k1gMnSc1
<g/>
)	)	kIx)
–	–	k?
zbudováno	zbudován	k2eAgNnSc1d1
za	za	k7c2
cara	car	k1gMnSc2
Mikuláše	Mikuláš	k1gMnSc2
I.	I.	kA
<g/>
,	,	kIx,
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
době	doba	k1gFnSc6
jedno	jeden	k4xCgNnSc1
z	z	k7c2
největších	veliký	k2eAgNnPc2d3
a	a	k8xC
technicky	technicky	k6eAd1
nejpropracovanějších	propracovaný	k2eAgNnPc2d3
divadel	divadlo	k1gNnPc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Mariinske	Mariinske	k6eAd1
divadlo	divadlo	k1gNnSc1
(	(	kIx(
<g/>
Těatralnaja	Těatralnaja	k1gFnSc1
ploščaď	ploščadit	k5eAaPmRp2nS
<g/>
)	)	kIx)
–	–	k?
postaveno	postaven	k2eAgNnSc4d1
v	v	k7c6
letech	léto	k1gNnPc6
1859	#num#	k4
-	-	kIx~
1860	#num#	k4
<g/>
,	,	kIx,
pojmenováno	pojmenovat	k5eAaPmNgNnS
po	po	k7c6
Marii	Maria	k1gFnSc6
Alexandrovně	Alexandrovna	k1gFnSc6
<g/>
,	,	kIx,
manželce	manželka	k1gFnSc3
cara	car	k1gMnSc2
Alexandra	Alexandr	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Michajlovské	Michajlovský	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
(	(	kIx(
<g/>
Ploščaď	Ploščaď	k1gMnSc1
Iskusstv	Iskusstv	k1gMnSc1
<g/>
)	)	kIx)
–	–	k?
budova	budova	k1gFnSc1
postavena	postaven	k2eAgFnSc1d1
v	v	k7c6
letech	léto	k1gNnPc6
1833	#num#	k4
-	-	kIx~
1835	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
letech	léto	k1gNnPc6
1859	#num#	k4
-	-	kIx~
1860	#num#	k4
přestavěna	přestavět	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
rokem	rok	k1gInSc7
1917	#num#	k4
divadlo	divadlo	k1gNnSc1
patřilo	patřit	k5eAaImAgNnS
k	k	k7c3
carskému	carský	k2eAgInSc3d1
dvoru	dvůr	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Monumenty	monument	k1gInPc1
</s>
<s>
Monument	monument	k1gInSc1
Petra	Petr	k1gMnSc2
Velikého	veliký	k2eAgMnSc2d1
</s>
<s>
Alexandrův	Alexandrův	k2eAgInSc1d1
sloup	sloup	k1gInSc1
(	(	kIx(
<g/>
Dvorcovaja	Dvorcovaja	k1gFnSc1
ploščaď	ploščadit	k5eAaPmRp2nS
<g/>
)	)	kIx)
–	–	k?
postaven	postavit	k5eAaPmNgMnS
v	v	k7c6
letech	léto	k1gNnPc6
1829	#num#	k4
-	-	kIx~
1834	#num#	k4
<g/>
,	,	kIx,
dle	dle	k7c2
návrhu	návrh	k1gInSc2
architekta	architekt	k1gMnSc2
Auguste	August	k1gMnSc5
de	de	k?
Montferranda	Montferranda	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c4
příkaz	příkaz	k1gInSc4
cara	car	k1gMnSc2
Mikuláše	Mikuláš	k1gMnSc2
I.	I.	kA
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
monument	monument	k1gInSc1
cara	car	k1gMnSc2
Alexandra	Alexandr	k1gMnSc2
I.	I.	kA
Výška	výška	k1gFnSc1
celé	celá	k1gFnSc2
památky	památka	k1gFnSc2
je	být	k5eAaImIp3nS
47,5	47,5	k4
m	m	kA
<g/>
,	,	kIx,
samotný	samotný	k2eAgInSc1d1
sloup	sloup	k1gInSc1
z	z	k7c2
leštěného	leštěný	k2eAgInSc2d1
granitu	granit	k1gInSc2
je	být	k5eAaImIp3nS
vysoký	vysoký	k2eAgMnSc1d1
25,5	25,5	k4
m.	m.	k?
Sloup	sloup	k1gInSc1
je	být	k5eAaImIp3nS
ukončen	ukončit	k5eAaPmNgInS
6	#num#	k4
m	m	kA
vysokou	vysoký	k2eAgFnSc7d1
<g/>
,	,	kIx,
bronzovou	bronzový	k2eAgFnSc7d1
sochou	socha	k1gFnSc7
anděla	anděl	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Socha	socha	k1gFnSc1
cara	car	k1gMnSc2
Petra	Petr	k1gMnSc2
I.	I.	kA
Velikého	veliký	k2eAgNnSc2d1
(	(	kIx(
<g/>
ul	ul	kA
<g/>
.	.	kIx.
Klenovaja	Klenovaja	k1gMnSc1
Aleja	Aleja	k1gMnSc1
<g/>
)	)	kIx)
–	–	k?
monument	monument	k1gInSc1
<g/>
,	,	kIx,
odlitý	odlitý	k2eAgInSc1d1
v	v	k7c6
pol.	pol.	k?
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
dle	dle	k7c2
modelu	model	k1gInSc2
Bartolomea	Bartolomeus	k1gMnSc2
Rastrelliho	Rastrelli	k1gMnSc2
<g/>
,	,	kIx,
z	z	k7c2
roku	rok	k1gInSc2
1716	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Socha	socha	k1gFnSc1
cara	car	k1gMnSc2
Mikuláše	Mikuláš	k1gMnSc2
I.	I.	kA
(	(	kIx(
<g/>
Isaakijevskaja	Isaakijevskaja	k1gFnSc1
ploščaď	ploščadit	k5eAaPmRp2nS
<g/>
)	)	kIx)
–	–	k?
navržen	navrhnout	k5eAaPmNgInS
sochařem	sochař	k1gMnSc7
Peterem	Peter	k1gMnSc7
Klodtem	Klodt	k1gMnSc7
a	a	k8xC
architektem	architekt	k1gMnSc7
Augustem	August	k1gMnSc7
de	de	k?
Montferrand	Montferrand	k1gInSc1
<g/>
,	,	kIx,
odhalen	odhalen	k2eAgInSc1d1
roku	rok	k1gInSc2
1859	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Socha	Socha	k1gMnSc1
cara	car	k1gMnSc4
Petra	Petr	k1gMnSc4
I.	I.	kA
Velikého	veliký	k2eAgMnSc4d1
(	(	kIx(
<g/>
Měděný	měděný	k2eAgInSc4d1
jezdec	jezdec	k1gInSc4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
Senatskaja	Senatskaja	k1gFnSc1
ploščaď	ploščadit	k5eAaPmRp2nS
<g/>
)	)	kIx)
–	–	k?
objednán	objednat	k5eAaPmNgInS
carevnou	carevna	k1gFnSc7
Kateřinou	Kateřina	k1gFnSc7
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velikou	veliký	k2eAgFnSc7d1
u	u	k7c2
sochaře	sochař	k1gMnSc2
Etienna	Etienn	k1gMnSc2
Maurice	Maurika	k1gFnSc3
Falconeta	Falconeta	k1gFnSc1
<g/>
,	,	kIx,
dílo	dílo	k1gNnSc1
vzniklo	vzniknout	k5eAaPmAgNnS
v	v	k7c6
letech	léto	k1gNnPc6
1766	#num#	k4
-	-	kIx~
1782	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Chrám	chrám	k1gInSc1
sv.	sv.	kA
Petra	Petr	k1gMnSc2
a	a	k8xC
Pavla	Pavel	k1gMnSc2
v	v	k7c6
Petropavlovské	petropavlovský	k2eAgFnSc6d1
pevnosti	pevnost	k1gFnSc6
s	s	k7c7
nejvyšší	vysoký	k2eAgFnSc7d3
pravoslavnou	pravoslavný	k2eAgFnSc7d1
věží	věž	k1gFnSc7
na	na	k7c6
světě	svět	k1gInSc6
</s>
<s>
Kazaňská	kazaňský	k2eAgFnSc1d1
katedrála	katedrála	k1gFnSc1
</s>
<s>
Chrám	chrám	k1gInSc1
Vzkříšení	vzkříšení	k1gNnSc2
Ježíše	Ježíš	k1gMnSc2
Krista	Kristus	k1gMnSc2
</s>
<s>
Katedrála	katedrála	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Mikuláše	Mikuláš	k1gMnSc2
</s>
<s>
Kostely	kostel	k1gInPc1
a	a	k8xC
kláštery	klášter	k1gInPc1
</s>
<s>
Katedrála	katedrála	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Petra	Petr	k1gMnSc2
a	a	k8xC
Pavla	Pavel	k1gMnSc2
(	(	kIx(
<g/>
Petropavlovskaja	Petropavlovskaj	k2eAgFnSc1d1
Kreposť	Kreposť	k1gFnSc1
č.	č.	k?
7	#num#	k4
<g/>
)	)	kIx)
–	–	k?
první	první	k4xOgInSc4
kamenný	kamenný	k2eAgInSc4d1
kostel	kostel	k1gInSc4
v	v	k7c6
Petrohradu	Petrohrad	k1gInSc3
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgMnS
v	v	k7c6
letech	léto	k1gNnPc6
1712	#num#	k4
-	-	kIx~
1733	#num#	k4
podle	podle	k7c2
návrhu	návrh	k1gInSc2
architekta	architekt	k1gMnSc2
Domenica	Domenicus	k1gMnSc2
Trezziniho	Trezzini	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Věž	věž	k1gFnSc1
s	s	k7c7
pozlacenou	pozlacený	k2eAgFnSc7d1
střechou	střecha	k1gFnSc7
se	se	k3xPyFc4
tyčí	tyčit	k5eAaImIp3nP
do	do	k7c2
výše	výše	k1gFnSc2,k1gFnSc2wB
122,5	122,5	k4
m	m	kA
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
je	být	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgFnSc7d3
pravoslavnou	pravoslavný	k2eAgFnSc7d1
zvonicí	zvonice	k1gFnSc7
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
zde	zde	k6eAd1
pohřbeni	pohřben	k2eAgMnPc1d1
významní	významný	k2eAgMnPc1d1
ruští	ruský	k2eAgMnPc1d1
panovníci	panovník	k1gMnPc1
od	od	k7c2
Petra	Petr	k1gMnSc2
I.	I.	kA
po	po	k7c4
Mikuláše	mikuláš	k1gInPc4
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Katedrála	katedrála	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Izáka	Izák	k1gMnSc2
(	(	kIx(
<g/>
Isaakijevskaja	Isaakijevskaja	k1gFnSc1
ploščaď	ploščadit	k5eAaPmRp2nS
<g/>
)	)	kIx)
–	–	k?
zasvěcena	zasvěcen	k2eAgFnSc1d1
svatému	svatý	k1gMnSc3
Izákovi	Izák	k1gMnSc3
Dalmatskému	dalmatský	k2eAgMnSc3d1
<g/>
,	,	kIx,
první	první	k4xOgInSc4
kostel	kostel	k1gInSc4
zde	zde	k6eAd1
nechal	nechat	k5eAaPmAgMnS
vystavět	vystavět	k5eAaPmF
car	car	k1gMnSc1
Petr	Petr	k1gMnSc1
I.	I.	kA
Veliký	veliký	k2eAgInSc1d1
roku	rok	k1gInSc2
1707	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současný	současný	k2eAgInSc1d1
kostel	kostel	k1gInSc1
vznikl	vzniknout	k5eAaPmAgInS
dle	dle	k7c2
návrhu	návrh	k1gInSc2
a	a	k8xC
pod	pod	k7c7
dohledem	dohled	k1gInSc7
architekta	architekt	k1gMnSc2
Auguste	August	k1gMnSc5
de	de	k?
Montferranda	Montferranda	k1gFnSc1
<g/>
,	,	kIx,
mezi	mezi	k7c4
roky	rok	k1gInPc4
1818	#num#	k4
-	-	kIx~
1858	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kostel	kostel	k1gInSc1
přišel	přijít	k5eAaPmAgInS
na	na	k7c4
více	hodně	k6eAd2
než	než	k8xS
23	#num#	k4
milionů	milion	k4xCgInPc2
stříbrných	stříbrný	k2eAgMnPc2d1
rublů	rubl	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Kazaňská	kazaňský	k2eAgFnSc1d1
katedrála	katedrála	k1gFnSc1
(	(	kIx(
<g/>
Něvskij	Něvskij	k1gFnSc1
Prospekt	prospekt	k1gInSc1
č.	č.	k?
25	#num#	k4
<g/>
)	)	kIx)
–	–	k?
klasicistní	klasicistní	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
s	s	k7c7
kopulí	kopule	k1gFnSc7
<g/>
,	,	kIx,
uvnitř	uvnitř	k6eAd1
se	se	k3xPyFc4
sloupy	sloup	k1gInPc7
z	z	k7c2
leštěného	leštěný	k2eAgInSc2d1
granitu	granit	k1gInSc2
<g/>
,	,	kIx,
s	s	k7c7
bronzovými	bronzový	k2eAgFnPc7d1
hlavicemi	hlavice	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgInSc1d1
kostel	kostel	k1gInSc1
byl	být	k5eAaImAgInS
založen	založen	k2eAgInSc1d1
roku	rok	k1gInSc2
1733	#num#	k4
<g/>
,	,	kIx,
carevnou	carevna	k1gFnSc7
Annou	Anna	k1gFnSc7
Ivanovnou	Ivanovna	k1gFnSc7
a	a	k8xC
vysvěcen	vysvěcen	k2eAgInSc1d1
roku	rok	k1gInSc2
1737	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
umístěna	umístěn	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
ikony	ikona	k1gFnSc2
Matky	matka	k1gFnSc2
Boží	božit	k5eAaImIp3nS
Kazaňské	kazaňský	k2eAgNnSc1d1
z	z	k7c2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1762	#num#	k4
zde	zde	k6eAd1
byla	být	k5eAaImAgFnS
korunována	korunovat	k5eAaBmNgFnS
carevnou	carevna	k1gFnSc7
Kateřina	Kateřina	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veliká	veliký	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byl	být	k5eAaImAgMnS
nahrazen	nahradit	k5eAaPmNgMnS
novou	nový	k2eAgFnSc7d1
stavbou	stavba	k1gFnSc7
<g/>
,	,	kIx,
dle	dle	k7c2
návrhu	návrh	k1gInSc2
Andreje	Andrej	k1gMnSc2
Voronichina	Voronichin	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
vysvěcena	vysvěcen	k2eAgFnSc1d1
roku	rok	k1gInSc2
1811	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1813	#num#	k4
zde	zde	k6eAd1
byl	být	k5eAaImAgInS
pohřben	pohřbít	k5eAaPmNgMnS
vojevůdce	vojevůdce	k1gMnSc1
Michail	Michail	k1gMnSc1
Illarionovič	Illarionovič	k1gMnSc1
Kutuzov	Kutuzovo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Katedrála	katedrála	k1gFnSc1
Vzkříšení	vzkříšení	k1gNnSc2
Ježíše	Ježíš	k1gMnSc2
Krista	Kristus	k1gMnSc2
(	(	kIx(
<g/>
ul	ul	kA
<g/>
.	.	kIx.
Naběrežnaja	Naběrežnaj	k2eAgFnSc1d1
kanala	kanala	k1gFnSc1
Gribojedova	Gribojedův	k2eAgInSc2d1
č.	č.	k?
2	#num#	k4
<g/>
a	a	k8xC
<g/>
)	)	kIx)
–	–	k?
založena	založit	k5eAaPmNgFnS
na	na	k7c6
místě	místo	k1gNnSc6
atentátu	atentát	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1881	#num#	k4
zabit	zabít	k5eAaPmNgMnS
car	car	k1gMnSc1
Alexandr	Alexandr	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postaven	postaven	k2eAgInSc4d1
v	v	k7c4
16	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
dle	dle	k7c2
návrhu	návrh	k1gInSc2
architekta	architekt	k1gMnSc2
Alfreda	Alfred	k1gMnSc2
Parlanda	parlando	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výška	výška	k1gFnSc1
je	být	k5eAaImIp3nS
81	#num#	k4
m	m	kA
<g/>
,	,	kIx,
a	a	k8xC
je	být	k5eAaImIp3nS
korunována	korunovat	k5eAaBmNgFnS
devíti	devět	k4xCc7
kopulemi	kopule	k1gFnPc7
<g/>
,	,	kIx,
čtyřmi	čtyři	k4xCgFnPc7
pozlacenými	pozlacený	k2eAgFnPc7d1
a	a	k8xC
pět	pět	k4xCc4
je	být	k5eAaImIp3nS
pokryto	pokrýt	k5eAaPmNgNnS
ušlechtilým	ušlechtilý	k2eAgInSc7d1
smaltem	smalt	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fasádu	fasáda	k1gFnSc4
pokrývají	pokrývat	k5eAaImIp3nP
majolikové	majolikový	k2eAgFnPc4d1
destičky	destička	k1gFnPc4
a	a	k8xC
mozaikové	mozaikový	k2eAgInPc4d1
panely	panel	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Katedrála	katedrála	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Mikuláše	Mikuláš	k1gMnSc2
(	(	kIx(
<g/>
Nikolskaja	Nikolskaja	k1gFnSc1
ploščaď	ploščadit	k5eAaPmRp2nS
<g/>
)	)	kIx)
–	–	k?
založena	založit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1753	#num#	k4
<g/>
,	,	kIx,
návrh	návrh	k1gInSc4
architekta	architekt	k1gMnSc2
Savy	Savy	k?
Čevakinského	Čevakinský	k2eAgMnSc2d1
<g/>
,	,	kIx,
sestává	sestávat	k5eAaImIp3nS
z	z	k7c2
horního	horní	k2eAgInSc2d1
a	a	k8xC
dolního	dolní	k2eAgInSc2d1
kostela	kostel	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1900	#num#	k4
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgInSc7
z	z	k7c2
hlavních	hlavní	k2eAgInPc2d1
kostelů	kostel	k1gInPc2
petrohradských	petrohradský	k2eAgMnPc2d1
námořníků	námořník	k1gMnPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
pod	pod	k7c7
jejich	jejich	k3xOp3gFnSc7
správou	správa	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Přeobraženská	Přeobraženský	k2eAgFnSc1d1
katedrála	katedrála	k1gFnSc1
(	(	kIx(
<g/>
Preobraženskaja	Preobraženskaja	k1gFnSc1
ploščaď	ploščadit	k5eAaPmRp2nS
<g/>
)	)	kIx)
–	–	k?
vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
oblasti	oblast	k1gFnSc6
ubytování	ubytování	k1gNnSc2
Přeobraženského	Přeobraženský	k2eAgInSc2d1
gardového	gardový	k2eAgInSc2d1
pluku	pluk	k1gInSc2
a	a	k8xC
příbuzných	příbuzná	k1gFnPc2
Petra	Petr	k1gMnSc2
I.	I.	kA
Tímto	tento	k3xDgInSc7
plukem	pluk	k1gInSc7
byla	být	k5eAaImAgFnS
carevna	carevna	k1gFnSc1
Alžběta	Alžběta	k1gFnSc1
Petrovna	Petrovna	k1gFnSc1
roku	rok	k1gInSc2
1741	#num#	k4
provolána	provolán	k2eAgFnSc1d1
carevnou	carevna	k1gFnSc7
<g/>
,	,	kIx,
na	na	k7c4
počest	počest	k1gFnSc4
této	tento	k3xDgFnSc2
události	událost	k1gFnSc2
zde	zde	k6eAd1
roku	rok	k1gInSc2
1743	#num#	k4
založila	založit	k5eAaPmAgFnS
kostel	kostel	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysvěcen	vysvěcen	k2eAgInSc1d1
byl	být	k5eAaImAgInS
roku	rok	k1gInSc2
1754	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1825	#num#	k4
vyhořel	vyhořet	k5eAaPmAgInS
<g/>
,	,	kIx,
rekonstruován	rekonstruován	k2eAgInSc1d1
byl	být	k5eAaImAgInS
dle	dle	k7c2
návrhu	návrh	k1gInSc2
Vasilije	Vasilije	k1gMnSc2
Stasova	Stasův	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
byl	být	k5eAaImAgInS
kolem	kolem	k7c2
katedrály	katedrála	k1gFnSc2
vytvořen	vytvořen	k2eAgInSc4d1
plot	plot	k1gInSc4
z	z	k7c2
tureckých	turecký	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
<g/>
,	,	kIx,
ukořistěných	ukořistěný	k2eAgInPc2d1
během	během	k7c2
Osmé	osmý	k4xOgFnSc2
rusko-turecké	rusko-turecký	k2eAgFnSc2d1
války	válka	k1gFnSc2
roku	rok	k1gInSc2
1828	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Arménský	arménský	k2eAgInSc1d1
kostel	kostel	k1gInSc1
(	(	kIx(
<g/>
Něvskij	Něvskij	k1gFnSc1
Prospekt	prospekt	k1gInSc1
č.	č.	k?
40	#num#	k4
<g/>
-	-	kIx~
<g/>
42	#num#	k4
<g/>
)	)	kIx)
–	–	k?
klasicistní	klasicistní	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
<g/>
,	,	kIx,
postavená	postavený	k2eAgFnSc1d1
v	v	k7c6
letech	léto	k1gNnPc6
1771	#num#	k4
-	-	kIx~
1779	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Kostel	kostel	k1gInSc1
Ikony	ikona	k1gFnSc2
Matky	matka	k1gFnSc2
Boží	božit	k5eAaImIp3nS
Vladimirské	Vladimirský	k2eAgNnSc1d1
(	(	kIx(
<g/>
Vladimirskij	Vladimirskij	k1gFnSc1
Prospekt	prospekt	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
klasicistní	klasicistní	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
<g/>
,	,	kIx,
postavená	postavený	k2eAgFnSc1d1
v	v	k7c6
letech	léto	k1gNnPc6
1761	#num#	k4
-	-	kIx~
1769	#num#	k4
<g/>
,	,	kIx,
skládá	skládat	k5eAaImIp3nS
se	se	k3xPyFc4
z	z	k7c2
horního	horní	k2eAgInSc2d1
a	a	k8xC
dolního	dolní	k2eAgInSc2d1
kostela	kostel	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
horním	horní	k2eAgInSc6d1
kostele	kostel	k1gInSc6
najdeme	najít	k5eAaPmIp1nP
ikonostas	ikonostas	k1gInSc4
<g/>
,	,	kIx,
navržený	navržený	k2eAgInSc4d1
Bartolomeem	Bartolomeus	k1gMnSc7
Rastrellim	Rastrellima	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvonice	zvonice	k1gFnSc1
postavena	postaven	k2eAgFnSc1d1
roku	rok	k1gInSc2
1786	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Kostel	kostel	k1gInSc1
svaté	svatý	k2eAgFnSc2d1
Kateřiny	Kateřina	k1gFnSc2
(	(	kIx(
<g/>
Katolický	katolický	k2eAgMnSc1d1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
Něvskij	Něvskij	k1gFnSc1
Prospekt	prospekt	k1gInSc1
č.	č.	k?
32	#num#	k4
<g/>
-	-	kIx~
<g/>
34	#num#	k4
<g/>
)	)	kIx)
–	–	k?
klasicistní	klasicistní	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
<g/>
,	,	kIx,
postavená	postavený	k2eAgFnSc1d1
v	v	k7c6
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
vysvěcena	vysvěcen	k2eAgFnSc1d1
roku	rok	k1gInSc2
1782	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Pantaleona	Pantaleon	k1gMnSc2
(	(	kIx(
<g/>
ul	ul	kA
<g/>
.	.	kIx.
Solianovy	Solianův	k2eAgFnSc2d1
Pereulok	Pereulok	k1gInSc4
č.	č.	k?
17	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
–	–	k?
barokní	barokní	k2eAgInSc1d1
<g/>
,	,	kIx,
založen	založen	k2eAgInSc1d1
carem	car	k1gMnSc7
Petrem	Petr	k1gMnSc7
I.	I.	kA
v	v	k7c6
roce	rok	k1gInSc6
1721	#num#	k4
<g/>
,	,	kIx,
na	na	k7c4
památku	památka	k1gFnSc4
námořních	námořní	k2eAgNnPc2d1
vítězství	vítězství	k1gNnPc2
u	u	k7c2
Gangutu	Gangut	k1gInSc2
a	a	k8xC
Grenhamu	Grenham	k1gInSc2
(	(	kIx(
<g/>
1714	#num#	k4
a	a	k8xC
1720	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přestavěn	přestavěn	k2eAgInSc1d1
v	v	k7c6
letech	léto	k1gNnPc6
1735	#num#	k4
-	-	kIx~
1739	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Petra	Petr	k1gMnSc2
a	a	k8xC
Pavla	Pavel	k1gMnSc2
(	(	kIx(
<g/>
Luteránský	luteránský	k2eAgMnSc1d1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
Něvskij	Něvskij	k1gFnSc1
Prospekt	prospekt	k1gInSc1
č.	č.	k?
22	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
)	)	kIx)
–	–	k?
klasicistní	klasicistní	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
<g/>
,	,	kIx,
založena	založen	k2eAgFnSc1d1
roku	rok	k1gInSc2
1727	#num#	k4
a	a	k8xC
ve	v	k7c6
30	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
rekonstruována	rekonstruován	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Klášter	klášter	k1gInSc1
Alexandra	Alexandr	k1gMnSc2
Něvského	něvský	k2eAgMnSc2d1
(	(	kIx(
<g/>
Ploščaď	Ploščaď	k1gMnSc2
Alexandra	Alexandr	k1gMnSc2
Něvskego	Něvskego	k1gMnSc1
<g/>
)	)	kIx)
–	–	k?
první	první	k4xOgInSc4
klášter	klášter	k1gInSc4
v	v	k7c6
Petrohradě	Petrohrad	k1gInSc6
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
založen	založit	k5eAaPmNgMnS
Petrem	Petr	k1gMnSc7
I.	I.	kA
Roku	rok	k1gInSc2
1724	#num#	k4
zde	zde	k6eAd1
byl	být	k5eAaImAgInS
pohřben	pohřbít	k5eAaPmNgMnS
svatý	svatý	k2eAgMnSc1d1
kníže	kníže	k1gMnSc1
Alexandr	Alexandr	k1gMnSc1
Něvský	něvský	k2eAgMnSc1d1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInPc1
ostatky	ostatek	k1gInPc1
byly	být	k5eAaImAgInP
přeneseny	přenést	k5eAaPmNgInP
z	z	k7c2
Vladimira	Vladimiro	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Návrh	návrh	k1gInSc1
Domenico	Domenico	k6eAd1
Trezzini	Trezzin	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součástí	součást	k1gFnPc2
je	být	k5eAaImIp3nS
kostel	kostel	k1gInSc1
Zvěstování	zvěstování	k1gNnSc2
<g/>
,	,	kIx,
z	z	k7c2
let	léto	k1gNnPc2
1717	#num#	k4
-	-	kIx~
1722	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
pohřben	pohřbít	k5eAaPmNgMnS
i	i	k9
vojevůdce	vojevůdce	k1gMnSc1
Alexandr	Alexandr	k1gMnSc1
Vasiljevič	Vasiljevič	k1gMnSc1
Suvorov	Suvorov	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
dále	daleko	k6eAd2
katedrála	katedrála	k1gFnSc1
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Trojice	trojice	k1gFnSc2
<g/>
,	,	kIx,
z	z	k7c2
let	léto	k1gNnPc2
1776	#num#	k4
-	-	kIx~
1790	#num#	k4
<g/>
,	,	kIx,
zřízená	zřízený	k2eAgFnSc1d1
na	na	k7c6
místě	místo	k1gNnSc6
staršího	starý	k2eAgInSc2d2
kostela	kostel	k1gInSc2
<g/>
,	,	kIx,
dle	dle	k7c2
návrhu	návrh	k1gInSc2
Ivana	Ivan	k1gMnSc2
Starova	Starov	k1gInSc2
<g/>
,	,	kIx,
právě	právě	k6eAd1
v	v	k7c6
ní	on	k3xPp3gFnSc6
jsou	být	k5eAaImIp3nP
uloženy	uložit	k5eAaPmNgFnP
ostatky	ostatek	k1gInPc1
Alexandra	Alexandr	k1gMnSc2
Něvského	něvský	k2eAgMnSc2d1
.	.	kIx.
</s>
<s>
Smolný	smolný	k2eAgInSc4d1
klášter	klášter	k1gInSc4
(	(	kIx(
<g/>
Ploščaď	Ploščaď	k1gMnSc3
Rastrelli	Rastrelle	k1gFnSc4
č.	č.	k?
3	#num#	k4
<g/>
/	/	kIx~
<g/>
1	#num#	k4
<g/>
)	)	kIx)
–	–	k?
barokní	barokní	k2eAgInSc4d1
komplex	komplex	k1gInSc4
<g/>
,	,	kIx,
navržený	navržený	k2eAgInSc4d1
architektem	architekt	k1gMnSc7
Bartolomeem	Bartolomeus	k1gMnSc7
Rastrellim	Rastrellim	k1gInSc4
<g/>
,	,	kIx,
založen	založen	k2eAgInSc4d1
carevnou	carevna	k1gFnSc7
Alžbětou	Alžběta	k1gFnSc7
Petrovnou	Petrovna	k1gFnSc7
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1744	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1748	#num#	k4
byla	být	k5eAaImAgFnS
dokončena	dokončen	k2eAgFnSc1d1
katedrála	katedrála	k1gFnSc1
Zmrtvýchvstání	zmrtvýchvstání	k1gNnSc1
Ježíše	Ježíš	k1gMnSc2
Krista	Kristus	k1gMnSc2
<g/>
,	,	kIx,
v	v	k7c6
50	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
postaven	postavit	k5eAaPmNgInS
klášter	klášter	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Kolomjažská	Kolomjažský	k2eAgFnSc1d1
mešita	mešita	k1gFnSc1
na	na	k7c6
severu	sever	k1gInSc6
města	město	k1gNnSc2
<g/>
,	,	kIx,
pro	pro	k7c4
tisíc	tisíc	k4xCgInSc4
věřících	věřící	k1gMnPc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
otevřena	otevřen	k2eAgFnSc1d1
16	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součástí	součást	k1gFnSc7
komplexu	komplex	k1gInSc2
je	být	k5eAaImIp3nS
dvoupodlažní	dvoupodlažní	k2eAgFnSc1d1
kancelářská	kancelářský	k2eAgFnSc1d1
budova	budova	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
Lachta	Lachta	k1gMnSc1
Centr	centr	k1gMnSc1
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgInSc1d3
mrakodrap	mrakodrap	k1gInSc1
Ruska	Rusko	k1gNnSc2
i	i	k8xC
Evropy	Evropa	k1gFnSc2
</s>
<s>
Dům	dům	k1gInSc1
knihy	kniha	k1gFnSc2
(	(	kIx(
<g/>
Singer	Singer	k1gMnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
Něvskij	Něvskij	k1gFnSc1
Prospekt	prospekt	k1gInSc1
č.	č.	k?
28	#num#	k4
<g/>
)	)	kIx)
–	–	k?
budova	budova	k1gFnSc1
<g/>
,	,	kIx,
vystavěna	vystavěn	k2eAgFnSc1d1
na	na	k7c6
přelomu	přelom	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
pro	pro	k7c4
americkou	americký	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
Singer	Singra	k1gFnPc2
<g/>
,	,	kIx,
dle	dle	k7c2
návrhu	návrh	k1gInSc2
architekta	architekt	k1gMnSc2
Pavla	Pavel	k1gMnSc2
Suroza	Suroz	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
roce	rok	k1gInSc6
1917	#num#	k4
zde	zde	k6eAd1
bylo	být	k5eAaImAgNnS
otevřeno	otevřít	k5eAaPmNgNnS
největší	veliký	k2eAgNnSc1d3
knihkupectví	knihkupectví	k1gNnSc1
ve	v	k7c6
městě	město	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Gostinij	Gostinít	k5eAaPmRp2nS
dvor	dvor	k1gInSc1
(	(	kIx(
<g/>
Něvskij	Něvskij	k1gFnSc1
Prospekt	prospekt	k1gInSc1
č.	č.	k?
35	#num#	k4
<g/>
)	)	kIx)
–	–	k?
původně	původně	k6eAd1
dřevěné	dřevěný	k2eAgInPc1d1
prodejní	prodejní	k2eAgInPc1d1
krámy	krám	k1gInPc1
<g/>
,	,	kIx,
nahrazované	nahrazovaný	k2eAgNnSc1d1
od	od	k7c2
50	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
zděnými	zděný	k2eAgFnPc7d1
galeriemi	galerie	k1gFnPc7
<g/>
,	,	kIx,
dokončeny	dokončit	k5eAaPmNgInP
roku	rok	k1gInSc2
1785	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Martovo	Martův	k2eAgNnSc1d1
pole	pole	k1gNnSc1
–	–	k?
rozlehlé	rozlehlý	k2eAgNnSc4d1
náměstí	náměstí	k1gNnSc4
<g/>
,	,	kIx,
vytvořené	vytvořený	k2eAgNnSc1d1
carem	car	k1gMnSc7
Petrem	Petr	k1gMnSc7
I.	I.	kA
<g/>
,	,	kIx,
na	na	k7c6
místě	místo	k1gNnSc6
bažinaté	bažinatý	k2eAgFnSc2d1
doliny	dolina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
Únorové	únorový	k2eAgFnSc6d1
revoluci	revoluce	k1gFnSc6
z	z	k7c2
roku	rok	k1gInSc2
1917	#num#	k4
zde	zde	k6eAd1
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
hřbitov	hřbitov	k1gInSc1
pro	pro	k7c4
oběti	oběť	k1gFnPc4
této	tento	k3xDgFnSc2
revoluce	revoluce	k1gFnSc2
(	(	kIx(
<g/>
památník	památník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1
Holandsko	Holandsko	k1gNnSc1
(	(	kIx(
<g/>
ul	ul	kA
<g/>
.	.	kIx.
Naběrežnaja	Naběrežnajus	k1gMnSc2
Krjukova	Krjukův	k2eAgMnSc2d1
<g/>
)	)	kIx)
–	–	k?
umělý	umělý	k2eAgInSc4d1
ostrov	ostrov	k1gInSc4
<g/>
,	,	kIx,
vytvořený	vytvořený	k2eAgInSc4d1
za	za	k7c2
vlády	vláda	k1gFnSc2
cara	car	k1gMnSc2
Petra	Petr	k1gMnSc2
I.	I.	kA
Velikého	veliký	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1756	#num#	k4
-	-	kIx~
1780	#num#	k4
byl	být	k5eAaImAgInS
přestavěn	přestavět	k5eAaPmNgInS
podle	podle	k7c2
vzoru	vzor	k1gInSc2
římské	římský	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Letní	letní	k2eAgInSc1d1
sad	sad	k1gInSc1
a	a	k8xC
Letní	letní	k2eAgInSc1d1
palác	palác	k1gInSc1
Petra	Petr	k1gMnSc2
I.	I.	kA
Velikého	veliký	k2eAgMnSc4d1
–	–	k?
první	první	k4xOgFnSc1
zahrada	zahrada	k1gFnSc1
evropského	evropský	k2eAgInSc2d1
typu	typ	k1gInSc2
v	v	k7c6
Rusku	Rusko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Založena	založen	k2eAgFnSc1d1
roku	rok	k1gInSc2
1704	#num#	k4
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
část	část	k1gFnSc1
letního	letní	k2eAgNnSc2d1
sídla	sídlo	k1gNnSc2
cara	car	k1gMnSc2
Petra	Petr	k1gMnSc2
I.	I.	kA
Velikého	veliký	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozloha	rozloha	k1gFnSc1
ostrova	ostrov	k1gInSc2
11	#num#	k4
ha	ha	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jihozápadní	jihozápadní	k2eAgFnSc6d1
rohu	roh	k1gInSc6
postaven	postavit	k5eAaPmNgInS
jednopatrový	jednopatrový	k2eAgInSc1d1
letní	letní	k2eAgInSc1d1
palác	palác	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
holandském	holandský	k2eAgInSc6d1
stylu	styl	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zahrada	zahrada	k1gFnSc1
osazena	osazen	k2eAgFnSc1d1
sochami	socha	k1gFnPc7
<g/>
,	,	kIx,
většinou	většina	k1gFnSc7
italské	italský	k2eAgFnSc2d1
provenience	provenience	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
ze	z	k7c2
strany	strana	k1gFnSc2
od	od	k7c2
Něvy	Něva	k1gFnSc2
postavena	postaven	k2eAgFnSc1d1
zeď	zeď	k1gFnSc1
<g/>
,	,	kIx,
s	s	k7c7
36	#num#	k4
granitovými	granitový	k2eAgInPc7d1
pilíři	pilíř	k1gInPc7
<g/>
,	,	kIx,
spojenými	spojený	k2eAgInPc7d1
litinovou	litinový	k2eAgFnSc4d1
mříží	mříží	k1gNnSc3
<g/>
,	,	kIx,
od	od	k7c2
tulských	tulský	k2eAgMnPc2d1
řemeslníků	řemeslník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
20	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
postavena	postaven	k2eAgFnSc1d1
čajovna	čajovna	k1gFnSc1
a	a	k8xC
kavárna	kavárna	k1gFnSc1
atd.	atd.	kA
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
řada	řada	k1gFnSc1
kašen	kašna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Molo	molo	k1gNnSc1
se	s	k7c7
sfingami	sfinga	k1gFnPc7
(	(	kIx(
<g/>
ul	ul	kA
<g/>
.	.	kIx.
Universitětskaja	Universitětskaj	k2eAgFnSc1d1
Naběrežnaja	Naběrežnaja	k1gFnSc1
č.	č.	k?
3	#num#	k4
<g/>
)	)	kIx)
–	–	k?
stavba	stavba	k1gFnSc1
z	z	k7c2
let	léto	k1gNnPc2
1832	#num#	k4
-	-	kIx~
1834	#num#	k4
<g/>
,	,	kIx,
se	s	k7c7
dvěma	dva	k4xCgFnPc7
originálními	originální	k2eAgFnPc7d1
egyptskými	egyptský	k2eAgFnPc7d1
sfingami	sfinga	k1gFnPc7
z	z	k7c2
doby	doba	k1gFnSc2
vlády	vláda	k1gFnSc2
faraona	faraon	k1gMnSc2
Amenhotepa	Amenhotep	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Strelka	Strelka	k1gFnSc1
Vasilevského	Vasilevský	k2eAgInSc2d1
ostrova	ostrov	k1gInSc2
–	–	k?
zde	zde	k6eAd1
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nS
Něva	Něva	k1gFnSc1
na	na	k7c4
Malou	malý	k2eAgFnSc4d1
a	a	k8xC
Velkou	velký	k2eAgFnSc4d1
Něvu	Něva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1805	#num#	k4
-	-	kIx~
1810	#num#	k4
zde	zde	k6eAd1
byla	být	k5eAaImAgFnS
postavena	postaven	k2eAgFnSc1d1
budova	budova	k1gFnSc1
burzy	burza	k1gFnSc2
<g/>
,	,	kIx,
dle	dle	k7c2
návrhu	návrh	k1gInSc2
Jeana	Jean	k1gMnSc2
Francoise	Francois	k1gMnSc2
Thomase	Thomas	k1gMnSc2
de	de	k?
Thomona	Thomon	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
vznikla	vzniknout	k5eAaPmAgFnS
i	i	k9
polokruhová	polokruhový	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
s	s	k7c7
přístavním	přístavní	k2eAgInSc7d1
molem	mol	k1gInSc7
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc7
součástí	součást	k1gFnSc7
jsou	být	k5eAaImIp3nP
i	i	k9
dva	dva	k4xCgInPc1
Rostrální	Rostrální	k2eAgInPc1d1
sloupy	sloup	k1gInPc1
<g/>
,	,	kIx,
ty	ten	k3xDgInPc1
sloužily	sloužit	k5eAaImAgInP
jako	jako	k9
majáky	maják	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nalézají	nalézat	k5eAaImIp3nP
se	se	k3xPyFc4
zde	zde	k6eAd1
alegorie	alegorie	k1gFnSc1
ruských	ruský	k2eAgFnPc2d1
řek	řeka	k1gFnPc2
(	(	kIx(
<g/>
Volha	Volha	k1gFnSc1
<g/>
,	,	kIx,
Dněpr	Dněpr	k1gInSc1
<g/>
,	,	kIx,
Vlochov	Vlochov	k1gInSc1
a	a	k8xC
Něva	Něva	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2003	#num#	k4
stojí	stát	k5eAaImIp3nS
ve	v	k7c6
městě	město	k1gNnSc6
socha	socha	k1gFnSc1
Švejka	Švejk	k1gMnSc2
(	(	kIx(
<g/>
postavy	postava	k1gFnPc1
z	z	k7c2
díla	dílo	k1gNnSc2
Jaroslava	Jaroslav	k1gMnSc2
Haška	Hašek	k1gMnSc2
-	-	kIx~
Osudy	osud	k1gInPc1
dobrého	dobrý	k2eAgMnSc2d1
vojáka	voják	k1gMnSc2
Švejka	Švejk	k1gMnSc4
za	za	k7c2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Lachta	Lachta	k1gMnSc1
Centr	centr	k1gMnSc1
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgInSc1d3
mrakodrap	mrakodrap	k1gInSc1
Evropy	Evropa	k1gFnSc2
a	a	k8xC
sídlo	sídlo	k1gNnSc4
firmy	firma	k1gFnSc2
Gazprom	Gazprom	k1gInSc4
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
Lobanova	Lobanův	k2eAgNnSc2d1
T.	T.	kA
Saint	Saint	k1gMnSc1
Petersburg	Petersburg	k1gMnSc1
and	and	k?
its	its	k?
Environs	Environs	k1gInSc1
<g/>
:	:	kIx,
One-day	One-daa	k1gFnSc2
Pedestrian	Pedestrian	k1gMnSc1
Routes	Routes	k1gMnSc1
<g/>
,	,	kIx,
Sankt-Peterburg	Sankt-Peterburg	k1gMnSc1
<g/>
:	:	kIx,
Novator	Novator	k1gMnSc1
<g/>
,	,	kIx,
P-2	P-2	k1gMnSc1
Art	Art	k1gMnSc1
Publishers	Publishers	k1gInSc4
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Mosty	most	k1gInPc1
</s>
<s>
Dvorcový	Dvorcový	k2eAgInSc1d1
most	most	k1gInSc1
</s>
<s>
Petr	Petr	k1gMnSc1
Veliký	veliký	k2eAgMnSc1d1
navrhoval	navrhovat	k5eAaImAgMnS
město	město	k1gNnSc4
jako	jako	k8xC,k8xS
další	další	k2eAgInSc1d1
Amsterdam	Amsterdam	k1gInSc1
a	a	k8xC
Benátky	Benátky	k1gFnPc1
<g/>
,	,	kIx,
s	s	k7c7
kanály	kanál	k1gInPc7
namísto	namísto	k7c2
ulic	ulice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
existuje	existovat	k5eAaImIp3nS
v	v	k7c6
Petrohradu	Petrohrad	k1gInSc2
342	#num#	k4
mostů	most	k1gInPc2
<g/>
,	,	kIx,
přes	přes	k7c4
různé	různý	k2eAgInPc4d1
kanály	kanál	k1gInPc4
a	a	k8xC
řeky	řeka	k1gFnPc4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
22	#num#	k4
je	být	k5eAaImIp3nS
zvedacích	zvedací	k2eAgFnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ty	k3xPp2nSc5
se	se	k3xPyFc4
každou	každý	k3xTgFnSc4
noc	noc	k1gFnSc4
se	s	k7c7
(	(	kIx(
<g/>
od	od	k7c2
dubna	duben	k1gInSc2
do	do	k7c2
listopadu	listopad	k1gInSc2
<g/>
)	)	kIx)
na	na	k7c4
několik	několik	k4yIc4
hodin	hodina	k1gFnPc2
zvedají	zvedat	k5eAaImIp3nP
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
mohly	moct	k5eAaImAgFnP
lodě	loď	k1gFnPc1
proplout	proplout	k5eAaPmF
z	z	k7c2
Baltského	baltský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
do	do	k7c2
Volgo-Baltského	Volgo-Baltský	k2eAgInSc2d1
vodního	vodní	k2eAgInSc2d1
systému	systém	k1gInSc2
a	a	k8xC
zpět	zpět	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
složité	složitý	k2eAgFnSc3d1
síti	síť	k1gFnSc3
kanálů	kanál	k1gInPc2
se	se	k3xPyFc4
Petrohradu	Petrohrad	k1gInSc3
často	často	k6eAd1
přezdívá	přezdívat	k5eAaImIp3nS
„	„	k?
<g/>
Benátky	Benátky	k1gFnPc4
severu	sever	k1gInSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgInSc1
trvalý	trvalý	k2eAgInSc1d1
most	most	k1gInSc1
přes	přes	k7c4
Něvu	Něva	k1gFnSc4
<g/>
,	,	kIx,
Blagoveščenský	blagoveščenský	k2eAgInSc4d1
most	most	k1gInSc4
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
otevřen	otevřen	k2eAgInSc1d1
roku	rok	k1gInSc2
1850	#num#	k4
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
byly	být	k5eAaImAgInP
povoleny	povolit	k5eAaPmNgInP
jen	jen	k9
pontonové	pontonový	k2eAgInPc1d1
mosty	most	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skoro	skoro	k6eAd1
100	#num#	k4
m	m	kA
široký	široký	k2eAgInSc1d1
„	„	k?
<g/>
Modrý	modrý	k2eAgInSc1d1
most	most	k1gInSc1
<g/>
“	“	k?
přes	přes	k7c4
řeku	řeka	k1gFnSc4
Mojku	Mojk	k1gInSc2
je	být	k5eAaImIp3nS
(	(	kIx(
<g/>
podle	podle	k7c2
některých	některý	k3yIgInPc2
zdrojů	zdroj	k1gInPc2
<g/>
)	)	kIx)
nejširší	široký	k2eAgMnSc1d3
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejdelší	dlouhý	k2eAgInSc1d3
je	být	k5eAaImIp3nS
most	most	k1gInSc1
„	„	k?
<g/>
Bolšoj	Bolšoj	k1gInSc1
Obuchovskij	Obuchovskij	k1gFnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
měří	měřit	k5eAaImIp3nS
2824	#num#	k4
m.	m.	k?
K	k	k7c3
dalším	další	k2eAgInPc3d1
známým	známý	k2eAgInPc3d1
mostům	most	k1gInPc3
patří	patřit	k5eAaImIp3nS
<g/>
:	:	kIx,
„	„	k?
<g/>
Dvorcový	Dvorcový	k2eAgInSc1d1
(	(	kIx(
<g/>
Palácový	palácový	k2eAgInSc1d1
<g/>
)	)	kIx)
most	most	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
Trojický	trojický	k2eAgInSc1d1
Most	most	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
Most	most	k1gInSc1
Alexandra	Alexandr	k1gMnSc2
Něvského	něvský	k2eAgMnSc2d1
<g/>
“	“	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
„	„	k?
<g/>
Bolšeochtinský	Bolšeochtinský	k2eAgInSc1d1
most	most	k1gInSc1
(	(	kIx(
<g/>
Most	most	k1gInSc1
Petra	Petr	k1gMnSc2
Velikého	veliký	k2eAgMnSc2d1
<g/>
)	)	kIx)
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
Bankovskij	Bankovskij	k1gFnSc1
most	most	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
Aničkov	Aničkov	k1gInSc1
most	most	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
Most	most	k1gInSc1
Lomonosov	Lomonosov	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
Egyptský	egyptský	k2eAgInSc1d1
most	most	k1gInSc1
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
Most	most	k1gInSc1
čtyř	čtyři	k4xCgInPc2
lvů	lev	k1gInPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
řeka	řeka	k1gFnSc1
Něva	Něva	k1gFnSc1
<g/>
,	,	kIx,
Petropavlovská	petropavlovský	k2eAgFnSc1d1
pevnost	pevnost	k1gFnSc1
a	a	k8xC
Zimní	zimní	k2eAgInSc1d1
palác	palác	k1gInSc1
</s>
<s>
Velká	velký	k2eAgFnSc1d1
kaskáda	kaskáda	k1gFnSc1
v	v	k7c6
paláci	palác	k1gInSc6
Petěrgof	Petěrgof	k1gMnSc1
</s>
<s>
Okolí	okolí	k1gNnSc1
města	město	k1gNnSc2
</s>
<s>
Z	z	k7c2
Petrohradu	Petrohrad	k1gInSc2
je	být	k5eAaImIp3nS
snadno	snadno	k6eAd1
dostupný	dostupný	k2eAgInSc1d1
slavný	slavný	k2eAgInSc1d1
Petěrhof	Petěrhof	k1gInSc1
<g/>
,	,	kIx,
proslulý	proslulý	k2eAgInSc1d1
svým	svůj	k3xOyFgInSc7
palácovým	palácový	k2eAgInSc7d1
a	a	k8xC
parkovým	parkový	k2eAgInSc7d1
komplexem	komplex	k1gInSc7
<g/>
,	,	kIx,
a	a	k8xC
další	další	k2eAgNnPc4d1
místa	místo	k1gNnPc4
s	s	k7c7
bývalými	bývalý	k2eAgFnPc7d1
rezidencemi	rezidence	k1gFnPc7
ruských	ruský	k2eAgMnPc2d1
carů	car	k1gMnPc2
<g/>
,	,	kIx,
např.	např.	kA
Carské	carský	k2eAgNnSc1d1
Selo	selo	k1gNnSc1
(	(	kIx(
<g/>
město	město	k1gNnSc1
Puškin	Puškina	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Pavlovsk	Pavlovsk	k1gInSc1
či	či	k8xC
Lomonosov	Lomonosov	k1gInSc1
<g/>
;	;	kIx,
pozoruhodná	pozoruhodný	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
také	také	k9
ostrovní	ostrovní	k2eAgFnSc1d1
pevnost	pevnost	k1gFnSc1
Kronštadt	Kronštadt	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Sport	sport	k1gInSc1
</s>
<s>
Arena	Areen	k2eAgFnSc1d1
Ledovyj	Ledovyj	k1gFnSc1
dvorec	dvorec	k1gInSc1
hokejové	hokejový	k2eAgFnSc2d1
SKA	SKA	kA
Petrohrad	Petrohrad	k1gInSc1
</s>
<s>
Ve	v	k7c6
městě	město	k1gNnSc6
působí	působit	k5eAaImIp3nS
přední	přední	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
Zenit	zenit	k1gInSc1
<g/>
,	,	kIx,
dvojnásobný	dvojnásobný	k2eAgMnSc1d1
vítěz	vítěz	k1gMnSc1
ruské	ruský	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
v	v	k7c6
sezóně	sezóna	k1gFnSc6
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
vyhrál	vyhrát	k5eAaPmAgMnS
také	také	k9
Pohár	pohár	k1gInSc4
UEFA	UEFA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
ledním	lední	k2eAgInSc6d1
hokeji	hokej	k1gInSc6
reprezentuje	reprezentovat	k5eAaImIp3nS
město	město	k1gNnSc1
SKA	SKA	kA
Petrohrad	Petrohrad	k1gInSc1
<g/>
,	,	kIx,
účastník	účastník	k1gMnSc1
KHL	KHL	kA
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
dříve	dříve	k6eAd2
působil	působit	k5eAaImAgMnS
český	český	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
Miloš	Miloš	k1gMnSc1
Říha	Říha	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1995	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
petrohradském	petrohradský	k2eAgInSc6d1
Sportovním	sportovní	k2eAgInSc6d1
a	a	k8xC
koncertním	koncertní	k2eAgInSc6d1
komplexu	komplex	k1gInSc6
pořádá	pořádat	k5eAaImIp3nS
halový	halový	k2eAgInSc4d1
tenisový	tenisový	k2eAgInSc4d1
turnaj	turnaj	k1gInSc4
kategorie	kategorie	k1gFnSc2
ATP	atp	kA
World	World	k1gMnSc1
Tour	Tour	k1gMnSc1
250	#num#	k4
St.	st.	kA
Petersburg	Petersburg	k1gMnSc1
Open	Open	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1980	#num#	k4
se	s	k7c7
<g/>
,	,	kIx,
v	v	k7c6
tehdejším	tehdejší	k2eAgMnSc6d1
Leningradu	Leningrad	k1gInSc3
<g/>
,	,	kIx,
odehrála	odehrát	k5eAaPmAgFnS
část	část	k1gFnSc1
fotbalového	fotbalový	k2eAgInSc2d1
turnaje	turnaj	k1gInSc2
v	v	k7c6
rámci	rámec	k1gInSc6
LOH	LOH	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
čtrnáct	čtrnáct	k4xCc4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
bylo	být	k5eAaImAgNnS
město	město	k1gNnSc1
pořadatelem	pořadatel	k1gMnSc7
her	hra	k1gFnPc2
dobré	dobrý	k2eAgFnSc2d1
vůle	vůle	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
vyrůstá	vyrůstat	k5eAaImIp3nS
na	na	k7c6
Krestovském	Krestovský	k2eAgInSc6d1
ostrově	ostrov	k1gInSc6
stadion	stadion	k1gInSc4
<g/>
,	,	kIx,
pro	pro	k7c4
spolupořádání	spolupořádání	k1gNnSc4
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
Petrohradu	Petrohrad	k1gInSc2
1725	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
</s>
<s>
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
172575	#num#	k4
000	#num#	k4
</s>
<s>
1750150	#num#	k4
000	#num#	k4
</s>
<s>
1800300	#num#	k4
000	#num#	k4
</s>
<s>
1846336	#num#	k4
000	#num#	k4
</s>
<s>
1852485	#num#	k4
000	#num#	k4
</s>
<s>
1858520	#num#	k4
100	#num#	k4
</s>
<s>
1867667	#num#	k4
000	#num#	k4
</s>
<s>
1873842	#num#	k4
900	#num#	k4
</s>
<s>
1881876	#num#	k4
600	#num#	k4
</s>
<s>
1886928	#num#	k4
600	#num#	k4
</s>
<s>
18911	#num#	k4
035	#num#	k4
400	#num#	k4
</s>
<s>
18971	#num#	k4
264	#num#	k4
900	#num#	k4
</s>
<s>
19011	#num#	k4
439	#num#	k4
400	#num#	k4
</s>
<s>
19081	#num#	k4
678	#num#	k4
000	#num#	k4
</s>
<s>
19101	#num#	k4
962	#num#	k4
000	#num#	k4
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
19152	#num#	k4
318	#num#	k4
600	#num#	k4
</s>
<s>
1920722	#num#	k4
000	#num#	k4
</s>
<s>
19261	#num#	k4
616	#num#	k4
100	#num#	k4
</s>
<s>
19362	#num#	k4
739	#num#	k4
800	#num#	k4
</s>
<s>
19393	#num#	k4
191	#num#	k4
300	#num#	k4
</s>
<s>
19442	#num#	k4
559	#num#	k4
000	#num#	k4
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
19592	#num#	k4
888	#num#	k4
000	#num#	k4
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
19703	#num#	k4
512	#num#	k4
974	#num#	k4
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
19794	#num#	k4
072	#num#	k4
528	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1988	#num#	k4
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
5	#num#	k4
000	#num#	k4
000	#num#	k4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1989	#num#	k4
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
4	#num#	k4
990	#num#	k4
749	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
20024	#num#	k4
661	#num#	k4
219	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
20084	#num#	k4
568	#num#	k4
047	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
20135	#num#	k4
028	#num#	k4
000	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
20195	#num#	k4
383	#num#	k4
968	#num#	k4
</s>
<s>
Národnostní	národnostní	k2eAgNnSc1d1
složení	složení	k1gNnSc1
</s>
<s>
Skupiny	skupina	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
podílely	podílet	k5eAaImAgFnP
na	na	k7c6
etnickém	etnický	k2eAgNnSc6d1
složení	složení	k1gNnSc6
alespoň	alespoň	k9
desetinou	desetina	k1gFnSc7
procenta	procento	k1gNnSc2
<g/>
,	,	kIx,
byly	být	k5eAaImAgFnP
při	při	k7c6
sčítání	sčítání	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
následující	následující	k2eAgFnSc6d1
<g/>
:	:	kIx,
Rusové	Rusová	k1gFnSc6
84,72	84,72	k4
%	%	kIx~
<g/>
;	;	kIx,
Ukrajinci	Ukrajinec	k1gMnSc3
1,87	1,87	k4
%	%	kIx~
<g/>
;	;	kIx,
Bělorusové	Bělorus	k1gMnPc1
1,17	1,17	k4
%	%	kIx~
<g/>
;	;	kIx,
Židé	Žid	k1gMnPc1
0,78	0,78	k4
%	%	kIx~
<g/>
;	;	kIx,
Tataři	Tatar	k1gMnPc1
0,76	0,76	k4
%	%	kIx~
<g/>
;	;	kIx,
Arméni	Armén	k1gMnPc1
0,41	0,41	k4
%	%	kIx~
<g/>
;	;	kIx,
Ázerbájdžánci	Ázerbájdžánec	k1gMnSc3
0,36	0,36	k4
%	%	kIx~
<g/>
;	;	kIx,
Gruzíni	Gruzín	k1gMnPc1
0,22	0,22	k4
%	%	kIx~
<g/>
;	;	kIx,
Čuvaši	Čuvaš	k1gMnSc3
0,13	0,13	k4
%	%	kIx~
<g/>
;	;	kIx,
Poláci	Polák	k1gMnPc1
0,10	0,10	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
přišel	přijít	k5eAaPmAgInS
Petrohrad	Petrohrad	k1gInSc1
především	především	k9
o	o	k7c4
německou	německý	k2eAgFnSc4d1
a	a	k8xC
polskou	polský	k2eAgFnSc4d1
menšinu	menšina	k1gFnSc4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
v	v	k7c6
roce	rok	k1gInSc6
1897	#num#	k4
tvořily	tvořit	k5eAaImAgFnP
4,01	4,01	k4
%	%	kIx~
<g/>
,	,	kIx,
resp.	resp.	kA
2,9	2,9	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
zde	zde	k6eAd1
žilo	žít	k5eAaImAgNnS
také	také	k9
více	hodně	k6eAd2
Finů	Fin	k1gMnPc2
a	a	k8xC
Estonců	Estonec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Doprava	doprava	k1gFnSc1
</s>
<s>
Metro	metro	k1gNnSc1
v	v	k7c6
Petrohradu	Petrohrad	k1gInSc3
<g/>
,	,	kIx,
stanice	stanice	k1gFnSc1
Avtovo	Avtovo	k1gNnSc1
</s>
<s>
Petrohrad	Petrohrad	k1gInSc1
má	mít	k5eAaImIp3nS
rozsáhlý	rozsáhlý	k2eAgInSc1d1
systém	systém	k1gInSc1
veřejné	veřejný	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
tramvaje	tramvaj	k1gFnPc4
<g/>
,	,	kIx,
autobusy	autobus	k1gInPc4
<g/>
,	,	kIx,
metro	metro	k1gNnSc1
či	či	k8xC
množství	množství	k1gNnSc1
říčních	říční	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
přepravují	přepravovat	k5eAaImIp3nP
cestující	cestující	k1gMnPc4
po	po	k7c6
celém	celý	k2eAgNnSc6d1
městě	město	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Petrohradské	petrohradský	k2eAgNnSc1d1
metro	metro	k1gNnSc1
má	mít	k5eAaImIp3nS
pět	pět	k4xCc4
linek	linka	k1gFnPc2
a	a	k8xC
přepraví	přepravit	k5eAaPmIp3nS
2,5	2,5	k4
milionu	milion	k4xCgInSc2
cestujících	cestující	k1gMnPc2
denně	denně	k6eAd1
<g/>
,	,	kIx,
některé	některý	k3yIgFnPc1
stanice	stanice	k1gFnPc1
jsou	být	k5eAaImIp3nP
bohatě	bohatě	k6eAd1
zdobené	zdobený	k2eAgFnPc1d1
mramorem	mramor	k1gInSc7
a	a	k8xC
bronzem	bronz	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
S	s	k7c7
Moskvou	Moskva	k1gFnSc7
a	a	k8xC
Helsinkami	Helsinky	k1gFnPc7
spojuje	spojovat	k5eAaImIp3nS
Petrohrad	Petrohrad	k1gInSc1
vysokorychlostní	vysokorychlostní	k2eAgFnSc1d1
železnice	železnice	k1gFnSc1
<g/>
,	,	kIx,
vlaky	vlak	k1gInPc1
zde	zde	k6eAd1
dosahují	dosahovat	k5eAaImIp3nP
rychlosti	rychlost	k1gFnSc3
až	až	k9
250	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
Mezinárodní	mezinárodní	k2eAgNnSc1d1
letiště	letiště	k1gNnSc1
Pulkovo	Pulkův	k2eAgNnSc1d1
obsluhuje	obsluhovat	k5eAaImIp3nS
většinu	většina	k1gFnSc4
letecké	letecký	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Federální	federální	k2eAgNnSc1d1
město	město	k1gNnSc1
Sankt-Petěrburg	Sankt-Petěrburg	k1gInSc1
</s>
<s>
Územněsprávní	územněsprávní	k2eAgNnSc1d1
dělení	dělení	k1gNnSc1
Petrohradu	Petrohrad	k1gInSc2
</s>
<s>
Vedle	vedle	k7c2
Moskvy	Moskva	k1gFnSc2
je	být	k5eAaImIp3nS
Petrohrad	Petrohrad	k1gInSc1
druhým	druhý	k4xOgInSc7
federálním	federální	k2eAgNnSc7d1
městem	město	k1gNnSc7
Ruska	Rusko	k1gNnSc2
<g/>
;	;	kIx,
je	být	k5eAaImIp3nS
tedy	tedy	k9
samostatným	samostatný	k2eAgInSc7d1
a	a	k8xC
samosprávným	samosprávný	k2eAgInSc7d1
federálním	federální	k2eAgInSc7d1
subjektem	subjekt	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
nenáleží	náležet	k5eNaImIp3nS
do	do	k7c2
okolní	okolní	k2eAgFnSc2d1
Leningradské	leningradský	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
administrativa	administrativa	k1gFnSc1
v	v	k7c6
Petrohradu	Petrohrad	k1gInSc3
sídlí	sídlet	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
je	být	k5eAaImIp3nS
také	také	k9
sídlem	sídlo	k1gNnSc7
Severozápadního	severozápadní	k2eAgInSc2d1
federálního	federální	k2eAgInSc2d1
okruhu	okruh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
současných	současný	k2eAgFnPc6d1
hranicích	hranice	k1gFnPc6
Petrohradu	Petrohrad	k1gInSc2
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
také	také	k9
dříve	dříve	k6eAd2
samostatná	samostatný	k2eAgNnPc4d1
sídla	sídlo	k1gNnPc4
a	a	k8xC
ostrovní	ostrovní	k2eAgNnSc4d1
město	město	k1gNnSc4
Kronštadt	Kronštadt	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
je	být	k5eAaImIp3nS
rozděleno	rozdělit	k5eAaPmNgNnS
na	na	k7c4
18	#num#	k4
rajónů	rajón	k1gInPc2
(	(	kIx(
<g/>
jejich	jejich	k3xOp3gInSc1
počet	počet	k1gInSc1
se	se	k3xPyFc4
často	často	k6eAd1
měnil	měnit	k5eAaImAgMnS
<g/>
;	;	kIx,
do	do	k7c2
r.	r.	kA
2005	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
zanikl	zaniknout	k5eAaPmAgInS
Lomonosovský	Lomonosovský	k2eAgInSc1d1
a	a	k8xC
Pavlovský	pavlovský	k2eAgInSc1d1
rajón	rajón	k1gInSc1
<g/>
,	,	kIx,
měl	mít	k5eAaImAgInS
Petrohrad	Petrohrad	k1gInSc1
rajónů	rajón	k1gInPc2
20	#num#	k4
<g/>
,	,	kIx,
ještě	ještě	k6eAd1
dříve	dříve	k6eAd2
pak	pak	k6eAd1
27	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Admiralitějský	Admiralitějský	k2eAgInSc1d1
rajón	rajón	k1gInSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vasileostrovský	Vasileostrovský	k2eAgInSc1d1
rajón	rajón	k1gInSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyborský	Vyborský	k2eAgInSc1d1
rajón	rajón	k1gInSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kalininský	Kalininský	k2eAgInSc1d1
rajón	rajón	k1gInSc1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kirovský	Kirovský	k2eAgInSc1d1
rajón	rajón	k1gInSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolpinský	Kolpinský	k2eAgInSc1d1
rajón	rajón	k1gInSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krasnogvardějský	Krasnogvardějský	k2eAgInSc1d1
rajón	rajón	k1gInSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krasnoselský	Krasnoselský	k2eAgInSc1d1
rajón	rajón	k1gInSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kronštadtský	kronštadtský	k2eAgInSc1d1
rajón	rajón	k1gInSc1
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kurortný	Kurortný	k2eAgInSc1d1
rajón	rajón	k1gInSc1
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moskevský	moskevský	k2eAgInSc1d1
rajón	rajón	k1gInSc1
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Něvský	něvský	k2eAgInSc1d1
rajón	rajón	k1gInSc1
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Petrohradský	petrohradský	k2eAgInSc1d1
rajón	rajón	k1gInSc1
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Petrodvorecký	Petrodvorecký	k2eAgInSc1d1
rajón	rajón	k1gInSc1
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přímořský	přímořský	k2eAgInSc1d1
rajón	rajón	k1gInSc1
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Puškinský	puškinský	k2eAgInSc1d1
rajón	rajón	k1gInSc1
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Frunzenský	Frunzenský	k2eAgInSc1d1
rajón	rajón	k1gInSc1
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Centrální	centrální	k2eAgInSc1d1
rajón	rajón	k1gInSc1
</s>
<s>
Gubernátoři	gubernátor	k1gMnPc1
Petrohradu	Petrohrad	k1gInSc2
</s>
<s>
2011	#num#	k4
<g/>
–	–	k?
Georgij	Georgij	k1gMnSc1
Sergejevič	Sergejevič	k1gMnSc1
Poltavčenko	Poltavčenka	k1gFnSc5
</s>
<s>
2003	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
Valentina	Valentina	k1gFnSc1
Ivanovna	Ivanovna	k1gFnSc1
Matvijenko	Matvijenka	k1gFnSc5
</s>
<s>
2011	#num#	k4
<g/>
–	–	k?
<g/>
2018	#num#	k4
Georgij	Georgij	k1gMnSc2
Sergejevič	Sergejevič	k1gInSc4
Poltavčenko	Poltavčenka	k1gFnSc5
</s>
<s>
2018	#num#	k4
<g/>
–	–	k?
Alexandr	Alexandr	k1gMnSc1
Dmitrijevič	Dmitrijevič	k1gMnSc1
Beglov	Beglov	k1gInSc1
(	(	kIx(
<g/>
dočasně	dočasně	k6eAd1
pověřen	pověřit	k5eAaPmNgInS
výkonem	výkon	k1gInSc7
funkce	funkce	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Osobnosti	osobnost	k1gFnPc1
města	město	k1gNnSc2
</s>
<s>
Viz	vidět	k5eAaImRp2nS
Seznam	seznam	k1gInSc4
osobností	osobnost	k1gFnPc2
Petrohradu	Petrohrad	k1gInSc2
</s>
<s>
Panorama	panorama	k1gNnSc1
</s>
<s>
Palácové	palácový	k2eAgNnSc1d1
náměstí	náměstí	k1gNnSc1
</s>
<s>
Partnerská	partnerský	k2eAgNnPc1d1
města	město	k1gNnPc1
</s>
<s>
Aarhus	Aarhus	k1gInSc1
<g/>
,	,	kIx,
Dánsko	Dánsko	k1gNnSc1
</s>
<s>
Akhisar	Akhisar	k1gInSc1
<g/>
,	,	kIx,
Turecko	Turecko	k1gNnSc1
</s>
<s>
Alexandria	Alexandrium	k1gNnPc1
<g/>
,	,	kIx,
Egypt	Egypt	k1gInSc1
</s>
<s>
Alma-Ata	Alma-Ata	k1gFnSc1
<g/>
,	,	kIx,
Kazachstán	Kazachstán	k1gInSc1
</s>
<s>
Antverpy	Antverpy	k1gFnPc1
<g/>
,	,	kIx,
Belgie	Belgie	k1gFnSc1
</s>
<s>
Nur-Sultan	Nur-Sultan	k1gInSc1
<g/>
,	,	kIx,
Kazachstán	Kazachstán	k1gInSc1
</s>
<s>
Baku	Baku	k1gNnSc1
<g/>
,	,	kIx,
Ázerbájdžán	Ázerbájdžán	k1gInSc1
</s>
<s>
Barcelona	Barcelona	k1gFnSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Bělehrad	Bělehrad	k1gInSc1
<g/>
,	,	kIx,
Srbsko	Srbsko	k1gNnSc1
</s>
<s>
Benátky	Benátky	k1gFnPc1
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
</s>
<s>
Bombaj	Bombaj	k1gFnSc1
<g/>
,	,	kIx,
India	indium	k1gNnPc1
</s>
<s>
Bordeaux	Bordeaux	k1gNnSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
</s>
<s>
Colombo	Colomba	k1gFnSc5
<g/>
,	,	kIx,
Srí	Srí	k1gMnSc1
Lanka	lanko	k1gNnSc2
</s>
<s>
Daugavpils	Daugavpils	k1gInSc1
<g/>
,	,	kIx,
Lotyšsko	Lotyšsko	k1gNnSc1
</s>
<s>
Debrecín	Debrecín	k1gInSc1
<g/>
,	,	kIx,
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
Drážďany	Drážďany	k1gInPc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
</s>
<s>
Edinburgh	Edinburgh	k1gInSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Florencie	Florencie	k1gFnSc1
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
</s>
<s>
Graz	Graz	k1gInSc1
<g/>
,	,	kIx,
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
Thà	Thà	k1gMnSc1
phố	phố	k?
Hồ	Hồ	k1gMnSc1
Chí	chí	k1gNnSc1
Minh	Minh	k1gMnSc1
<g/>
,	,	kIx,
Vietnam	Vietnam	k1gInSc1
</s>
<s>
Havana	Havana	k1gFnSc1
<g/>
,	,	kIx,
Kuba	Kuba	k1gFnSc1
</s>
<s>
Chartúm	Chartúm	k1gInSc1
<g/>
,	,	kIx,
Súdán	Súdán	k1gInSc1
</s>
<s>
Isfahán	Isfahán	k1gInSc1
<g/>
,	,	kIx,
Írán	Írán	k1gInSc1
</s>
<s>
Istanbul	Istanbul	k1gInSc1
<g/>
,	,	kIx,
Turecko	Turecko	k1gNnSc1
</s>
<s>
Janov	Janov	k1gInSc1
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
</s>
<s>
Džidda	Džidda	k1gFnSc1
<g/>
,	,	kIx,
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
</s>
<s>
Jerevan	Jerevan	k1gMnSc1
<g/>
,	,	kIx,
Arménie	Arménie	k1gFnSc1
</s>
<s>
Košice	Košice	k1gInPc1
<g/>
,	,	kIx,
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
Kotka	Kotek	k1gMnSc2
<g/>
,	,	kIx,
Finsko	Finsko	k1gNnSc1
</s>
<s>
Le	Le	k?
Havre	Havr	k1gMnSc5
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc2
</s>
<s>
Los	los	k1gMnSc1
Angeles	Angeles	k1gMnSc1
<g/>
,	,	kIx,
USA	USA	kA
</s>
<s>
Luxembourg	Luxembourg	k1gInSc1
<g/>
,	,	kIx,
Lucembursko	Lucembursko	k1gNnSc1
</s>
<s>
Lvov	Lvov	k1gInSc1
<g/>
,	,	kIx,
Ukrajina	Ukrajina	k1gFnSc1
</s>
<s>
Lyon	Lyon	k1gInSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
</s>
<s>
Manchester	Manchester	k1gInSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Maribor	Maribor	k1gInSc1
<g/>
,	,	kIx,
Slovinsko	Slovinsko	k1gNnSc1
</s>
<s>
Melbourne	Melbourne	k1gNnSc1
<g/>
,	,	kIx,
Austrálie	Austrálie	k1gFnSc1
</s>
<s>
Lansing	Lansing	k1gInSc1
<g/>
,	,	kIx,
USA	USA	kA
</s>
<s>
Miláno	Milán	k2eAgNnSc1d1
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
</s>
<s>
Montevideo	Montevideo	k1gNnSc1
<g/>
,	,	kIx,
Uruguay	Uruguay	k1gFnSc1
</s>
<s>
Nampho	Nampze	k6eAd1
<g/>
,	,	kIx,
Severní	severní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
</s>
<s>
Nepjito	Nepjita	k1gFnSc5
<g/>
,	,	kIx,
Myanmar	Myanmar	k1gMnSc1
</s>
<s>
Nice	Nice	k1gFnSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
</s>
<s>
Osaka	Osaka	k1gFnSc1
<g/>
,	,	kIx,
Japonsko	Japonsko	k1gNnSc1
</s>
<s>
Oš	Oš	k?
<g/>
,	,	kIx,
Kyrgyzstán	Kyrgyzstán	k1gInSc1
</s>
<s>
Oslo	Oslo	k1gNnSc1
<g/>
,	,	kIx,
Norsko	Norsko	k1gNnSc1
</s>
<s>
Paříž	Paříž	k1gFnSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
</s>
<s>
Plovdiv	Plovdiv	k1gInSc1
<g/>
,	,	kIx,
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
Porto	porto	k1gNnSc1
Alegre	Alegr	k1gInSc5
<g/>
,	,	kIx,
Brazílie	Brazílie	k1gFnSc2
</s>
<s>
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Quebec	Quebec	k1gInSc1
<g/>
,	,	kIx,
Kanada	Kanada	k1gFnSc1
</s>
<s>
Reykjavík	Reykjavík	k1gInSc1
<g/>
,	,	kIx,
Island	Island	k1gInSc1
</s>
<s>
Riga	Riga	k1gFnSc1
<g/>
,	,	kIx,
Lotyšsko	Lotyšsko	k1gNnSc1
</s>
<s>
Rio	Rio	k?
de	de	k?
Janeiro	Janeiro	k1gNnSc1
<g/>
,	,	kIx,
Brazílie	Brazílie	k1gFnSc1
</s>
<s>
Rišon	Rišon	k1gMnSc1
le-Cijon	le-Cijon	k1gMnSc1
<g/>
,	,	kIx,
Izrael	Izrael	k1gInSc1
</s>
<s>
Santiago	Santiago	k1gNnSc1
<g/>
,	,	kIx,
Kuba	Kuba	k1gFnSc1
</s>
<s>
Sevastopol	Sevastopol	k1gInSc1
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
</s>
<s>
Sofie	Sofie	k1gFnSc1
<g/>
,	,	kIx,
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
Soluň	Soluň	k1gFnSc1
<g/>
,	,	kIx,
Řecko	Řecko	k1gNnSc1
</s>
<s>
Šanghaj	Šanghaj	k1gFnSc1
<g/>
,	,	kIx,
Čína	Čína	k1gFnSc1
</s>
<s>
Stockholm	Stockholm	k1gInSc1
<g/>
,	,	kIx,
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Tallinn	Tallinn	k1gNnSc1
<g/>
,	,	kIx,
Estonsko	Estonsko	k1gNnSc1
</s>
<s>
Tegu	Tegu	k6eAd1
<g/>
,	,	kIx,
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
</s>
<s>
Turku	Turek	k1gMnSc3
<g/>
,	,	kIx,
Finsko	Finsko	k1gNnSc1
</s>
<s>
Varna	Varna	k1gFnSc1
<g/>
,	,	kIx,
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
Varšava	Varšava	k1gFnSc1
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
</s>
<s>
Vilnius	Vilnius	k1gInSc1
<g/>
,	,	kIx,
Litva	Litva	k1gFnSc1
</s>
<s>
Záhřeb	Záhřeb	k1gInSc1
<g/>
,	,	kIx,
Chorvatsko	Chorvatsko	k1gNnSc1
</s>
<s>
Český	český	k2eAgInSc1d1
konzulát	konzulát	k1gInSc1
v	v	k7c6
Petrohradu	Petrohrad	k1gInSc2
(	(	kIx(
<g/>
ul	ul	kA
<g/>
.	.	kIx.
Tverskaja	Tverskaja	k1gFnSc1
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Ruský	ruský	k2eAgInSc1d1
federální	federální	k2eAgInSc1d1
zákon	zákon	k1gInSc1
248-Ф	248-Ф	k?
Moskva	Moskva	k1gFnSc1
<g/>
:	:	kIx,
П	П	k?
Р	Р	k?
Ф	Ф	k?
<g/>
,	,	kIx,
2014-07-21	2014-07-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
26	#num#	k4
<g/>
.	.	kIx.
Ч	Ч	k?
п	п	k?
н	н	k?
Р	Р	k?
Ф	Ф	k?
п	п	k?
м	м	k?
о	о	k?
н	н	k?
1	#num#	k4
я	я	k?
2018	#num#	k4
г	г	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
George	George	k1gInSc1
<g/>
,	,	kIx,
Elena	Elena	k1gFnSc1
<g/>
:	:	kIx,
St.	st.	kA
Petersburg	Petersburg	k1gInSc1
<g/>
:	:	kIx,
Russia	Russia	k1gFnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
window	window	k?
to	ten	k3xDgNnSc1
the	the	k?
future	futur	k1gMnSc5
<g/>
—	—	k?
<g/>
the	the	k?
first	first	k1gInSc1
three	threat	k5eAaPmIp3nS
centuries	centuries	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taylor	Taylor	k1gMnSc1
Trade	Trad	k1gInSc5
Publishing	Publishing	k1gInSc1
2003	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
30	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
JUNGMANN	JUNGMANN	kA
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovník	slovník	k1gInSc1
česko-německý	česko-německý	k2eAgInSc1d1
<g/>
,	,	kIx,
Díl	díl	k1gInSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
P	P	kA
-	-	kIx~
R.	R.	kA
1	#num#	k4
<g/>
..	..	k?
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Knížecí	knížecí	k2eAgFnSc1d1
arcibiskupská	arcibiskupský	k2eAgFnSc1d1
tiskárna	tiskárna	k1gFnSc1
<g/>
,	,	kIx,
1837	#num#	k4
<g/>
.	.	kIx.
974	#num#	k4
s.	s.	k?
S.	S.	kA
75	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
PRECLÍK	preclík	k1gInSc1
<g/>
,	,	kIx,
Vratislav	Vratislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Masaryk	Masaryk	k1gMnSc1
a	a	k8xC
legie	legie	k1gFnSc1
<g/>
,	,	kIx,
váz	váza	k1gFnPc2
<g/>
.	.	kIx.
kniha	kniha	k1gFnSc1
<g/>
,	,	kIx,
219	#num#	k4
str	str	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
vydalo	vydat	k5eAaPmAgNnS
nakladatelství	nakladatelství	k1gNnSc1
Paris	Paris	k1gMnSc1
Karviná	Karviná	k1gFnSc1
<g/>
,	,	kIx,
Žižkova	Žižkův	k2eAgFnSc1d1
2379	#num#	k4
(	(	kIx(
<g/>
734	#num#	k4
01	#num#	k4
Karviná	Karviná	k1gFnSc1
<g/>
)	)	kIx)
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
Masarykovým	Masarykův	k2eAgNnSc7d1
demokratickým	demokratický	k2eAgNnSc7d1
hnutím	hnutí	k1gNnSc7
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
87173	#num#	k4
<g/>
-	-	kIx~
<g/>
47	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.12	.12	k4
-	-	kIx~
25	#num#	k4
<g/>
,	,	kIx,
26	#num#	k4
-	-	kIx~
111	#num#	k4
<g/>
,	,	kIx,
140	#num#	k4
-	-	kIx~
148	#num#	k4
</s>
<s>
↑	↑	k?
</s>
<s>
П	П	k?
с	с	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
↑	↑	k?
VOTRUBA	Votruba	k1gMnSc1
<g/>
,	,	kIx,
Viktor	Viktor	k1gMnSc1
<g/>
;	;	kIx,
SAPÍK	SAPÍK	kA
<g/>
,	,	kIx,
Lukáš	Lukáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Švejk	Švejk	k1gMnSc1
se	s	k7c7
psem	pes	k1gMnSc7
a	a	k8xC
hovínkem	hovínkem	k?
pro	pro	k7c4
štěstí	štěstí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nadšenci	nadšenec	k1gMnPc1
chtějí	chtít	k5eAaImIp3nP
v	v	k7c6
Kralupech	Kralupy	k1gInPc6
sochu	socha	k1gFnSc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-04-27	2014-04-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Д	Д	k?
р	р	k?
п	п	k?
ж	ж	k?
Л	Л	k?
<g/>
.	.	kIx.
petersburg	petersburg	k1gMnSc1
<g/>
.	.	kIx.
<g/>
rfn	rfn	k?
<g/>
.	.	kIx.
<g/>
ru	ru	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Д	Д	k?
В	В	k?
п	п	k?
н	н	k?
С	С	k?
1989	#num#	k4
г	г	k?
<g/>
.	.	kIx.
www.soc.pu.ru	www.soc.pu.r	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Partnerská	partnerský	k2eAgNnPc1d1
města	město	k1gNnPc1
HMP	HMP	kA
(	(	kIx(
<g/>
Oddělení	oddělení	k1gNnSc4
cestovního	cestovní	k2eAgInSc2d1
ruchu	ruch	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
<g/>
eu	eu	k?
Dostupné	dostupný	k2eAgNnSc4d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archivováno	archivován	k2eAgNnSc4d1
25	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Vladimir	Vladimir	k1gInSc1
Černov	Černov	k1gInSc1
<g/>
,	,	kIx,
Stanislav	Stanislav	k1gMnSc1
Hýnar	Hýnar	k1gMnSc1
<g/>
:	:	kIx,
Moskva	Moskva	k1gFnSc1
–	–	k?
Leningrad	Leningrad	k1gInSc1
<g/>
,	,	kIx,
Olympia	Olympia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1987	#num#	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Tramvajová	tramvajový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
v	v	k7c6
Petrohradu	Petrohrad	k1gInSc3
</s>
<s>
Metro	metro	k1gNnSc1
v	v	k7c6
Petrohradu	Petrohrad	k1gInSc3
</s>
<s>
Michajlovské	Michajlovský	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
</s>
<s>
Obležení	obležení	k1gNnSc1
Leningradu	Leningrad	k1gInSc2
</s>
<s>
Seznam	seznam	k1gInSc1
osobností	osobnost	k1gFnPc2
Petrohradu	Petrohrad	k1gInSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Petrohrad	Petrohrad	k1gInSc1
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Petrohrad	Petrohrad	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Petrohrad	Petrohrad	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc7
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Petrohrad	Petrohrad	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
města	město	k1gNnSc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnPc1d1
turistické	turistický	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
města	město	k1gNnSc2
</s>
<s>
Dějiny	dějiny	k1gFnPc1
Sankt	Sankt	k1gInSc1
Petěrburgu	Petěrburg	k1gInSc2
</s>
<s>
Památky	památka	k1gFnPc1
a	a	k8xC
zajímavosti	zajímavost	k1gFnPc1
Petrohradu	Petrohrad	k1gInSc2
–	–	k?
fotocestopis	fotocestopis	k1gInSc1
</s>
<s>
Petrohrad	Petrohrad	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
-	-	kIx~
Historie	historie	k1gFnSc1
<g/>
,	,	kIx,
fotografie	fotografie	k1gFnSc1
<g/>
,	,	kIx,
výběr	výběr	k1gInSc4
videí	video	k1gNnPc2
a	a	k8xC
odkazů	odkaz	k1gInPc2
</s>
<s>
Petrohrad	Petrohrad	k1gInSc1
Poznej	poznat	k5eAaPmRp2nS
Sever	sever	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
-	-	kIx~
Průvodce	průvodce	k1gMnSc4
Petrohradem	Petrohrad	k1gInSc7
</s>
<s>
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
Petrohrad	Petrohrad	k1gInSc1
-	-	kIx~
Virtuální	virtuální	k2eAgMnSc1d1
průvodce	průvodce	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krásná	krásný	k2eAgNnPc4d1
panoramata	panorama	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Rusko	Rusko	k1gNnSc1
–	–	k?
Р	Р	k?
–	–	k?
(	(	kIx(
<g/>
RUS	Rus	k1gMnSc1
<g/>
)	)	kIx)
Subjekty	subjekt	k1gInPc1
Ruské	ruský	k2eAgFnSc2d1
federace	federace	k1gFnSc2
Republiky	republika	k1gFnSc2
</s>
<s>
Adygejsko	Adygejsko	k1gNnSc1
(	(	kIx(
<g/>
Majkop	Majkop	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Altajsko	Altajsko	k1gNnSc1
(	(	kIx(
<g/>
Gorno-Altajsk	Gorno-Altajsk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Baškortostán	Baškortostán	k1gInSc1
(	(	kIx(
<g/>
Ufa	Ufa	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Burjatsko	Burjatsko	k1gNnSc1
(	(	kIx(
<g/>
Ulan-Ude	Ulan-Ud	k1gMnSc5
<g/>
)	)	kIx)
</s>
<s>
Čečensko	Čečensko	k1gNnSc1
(	(	kIx(
<g/>
Grozný	Grozný	k2eAgMnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Čuvašsko	Čuvašsko	k1gNnSc1
(	(	kIx(
<g/>
Čeboksary	Čeboksara	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Dagestán	Dagestán	k1gInSc1
(	(	kIx(
<g/>
Machačkala	Machačkala	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Chakasie	Chakasie	k1gFnSc1
(	(	kIx(
<g/>
Abakan	Abakan	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Ingušsko	Ingušsko	k1gNnSc1
(	(	kIx(
<g/>
Magas	Magas	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Kabardsko-Balkarsko	Kabardsko-Balkarsko	k1gNnSc1
(	(	kIx(
<g/>
Nalčik	Nalčik	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Kalmycko	Kalmycko	k1gNnSc1
(	(	kIx(
<g/>
Elista	Elista	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Karačajevsko-Čerkesko	Karačajevsko-Čerkesko	k1gNnSc1
(	(	kIx(
<g/>
Čerkesk	Čerkesk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Karélie	Karélie	k1gFnSc1
(	(	kIx(
<g/>
Petrozavodsk	Petrozavodsk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Komi	Komi	k1gNnSc1
(	(	kIx(
<g/>
Syktyvkar	Syktyvkar	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Krym	Krym	k1gInSc1
(	(	kIx(
<g/>
Simferopol	Simferopol	k1gInSc1
<g/>
)	)	kIx)
<g/>
¹	¹	k?
</s>
<s>
Marijsko	Marijsko	k1gNnSc1
(	(	kIx(
<g/>
Joškar-Ola	Joškar-Ola	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Mordvinsko	Mordvinsko	k1gNnSc1
(	(	kIx(
<g/>
Saransk	Saransk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Sacha	Sacha	k1gFnSc1
(	(	kIx(
<g/>
Jakutsk	Jakutsk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Severní	severní	k2eAgFnSc1d1
Osetie-Alanie	Osetie-Alanie	k1gFnSc1
(	(	kIx(
<g/>
Vladikavkaz	Vladikavkaz	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Tatarstán	Tatarstán	k1gInSc1
(	(	kIx(
<g/>
Kazaň	Kazaň	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Tuva	Tuva	k1gFnSc1
(	(	kIx(
<g/>
Kyzyl	Kyzyl	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Udmurtsko	Udmurtsko	k1gNnSc1
(	(	kIx(
<g/>
Iževsk	Iževsk	k1gInSc1
<g/>
)	)	kIx)
Kraje	kraj	k1gInPc1
</s>
<s>
Altajský	altajský	k2eAgInSc1d1
(	(	kIx(
<g/>
Barnaul	Barnaul	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Chabarovský	chabarovský	k2eAgInSc1d1
(	(	kIx(
<g/>
Chabarovsk	Chabarovsk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Kamčatský	kamčatský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Petropavlovsk-Kamčatskij	Petropavlovsk-Kamčatskij	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Krasnodarský	krasnodarský	k2eAgInSc1d1
(	(	kIx(
<g/>
Krasnodar	Krasnodar	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Krasnojarský	Krasnojarský	k2eAgInSc1d1
(	(	kIx(
<g/>
Krasnojarsk	Krasnojarsk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Permský	permský	k2eAgInSc1d1
(	(	kIx(
<g/>
Perm	perm	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Přímořský	přímořský	k2eAgInSc4d1
(	(	kIx(
<g/>
Vladivostok	Vladivostok	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Stavropolský	stavropolský	k2eAgInSc1d1
(	(	kIx(
<g/>
Stavropol	Stavropol	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Zabajkalský	zabajkalský	k2eAgInSc1d1
(	(	kIx(
<g/>
Čita	čit	k2eAgFnSc1d1
<g/>
)	)	kIx)
Oblasti	oblast	k1gFnPc1
</s>
<s>
Amurská	amurský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Blagověščensk	Blagověščensk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Archangelská	archangelský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Archangelsk	Archangelsk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Astrachaňská	astrachaňský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Astrachaň	Astrachaň	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Bělgorodská	Bělgorodský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Bělgorod	Bělgorod	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Brjanská	Brjanský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Brjansk	Brjansk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Čeljabinská	čeljabinský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Čeljabinsk	Čeljabinsk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Irkutská	irkutský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Irkutsk	Irkutsk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Ivanovská	Ivanovský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Ivanovo	Ivanův	k2eAgNnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Jaroslavská	Jaroslavský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Jaroslavl	Jaroslavl	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Kaliningradská	kaliningradský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Kaliningrad	Kaliningrad	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Kalužská	Kalužský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Kaluga	Kaluga	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Kemerovská	Kemerovský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Kemerovo	Kemerův	k2eAgNnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Kirovská	Kirovská	k1gFnSc1
(	(	kIx(
<g/>
Kirov	Kirov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Kostromská	Kostromský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Kostroma	Kostroma	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Kurganská	Kurganský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Kurgan	kurgan	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Kurská	kurský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Kursk	Kursk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Leningradská	leningradský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Petrohrad	Petrohrad	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Lipecká	Lipecká	k1gFnSc1
(	(	kIx(
<g/>
Lipeck	Lipeck	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Magadanská	Magadanský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Magadan	Magadan	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Moskevská	moskevský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Moskva	Moskva	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Murmanská	murmanský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Murmansk	Murmansk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Nižněnovgorodská	Nižněnovgorodský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
(	(	kIx(
<g/>
Nižnij	Nižnij	k1gFnSc1
Novgorod	Novgorod	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Novgorodská	novgorodský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Veliký	veliký	k2eAgInSc1d1
Novgorod	Novgorod	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Novosibirská	novosibirský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Novosibirsk	Novosibirsk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Omská	omský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Omsk	Omsk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Orelská	orelský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Orel	Orel	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Orenburská	orenburský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Orenburg	Orenburg	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Penzenská	Penzenský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Penza	Penza	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Pskovská	Pskovská	k1gFnSc1
(	(	kIx(
<g/>
Pskov	Pskov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Rjazaňská	rjazaňský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Rjazaň	Rjazaň	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Rostovská	Rostovský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Rostov	Rostov	k1gInSc1
na	na	k7c4
Donu	dona	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Sachalinská	sachalinský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Južno-Sachalinsk	Južno-Sachalinsk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Samarská	samarský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Samara	Samara	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Saratovská	Saratovský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Saratov	Saratovo	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Smolenská	Smolenský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Smolensk	Smolensk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Sverdlovská	sverdlovský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Jekatěrinburg	Jekatěrinburg	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Tambovská	Tambovská	k1gFnSc1
(	(	kIx(
<g/>
Tambov	Tambov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Tomská	Tomská	k1gFnSc1
(	(	kIx(
<g/>
Tomsk	Tomsk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Tverská	Tverský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Tver	Tver	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Tulská	tulský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Tula	Tula	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Ťumeňská	Ťumeňský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Ťumeň	Ťumeň	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Uljanovská	Uljanovský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Uljanovsk	Uljanovsk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Vladimirská	Vladimirský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Vladimir	Vladimir	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Volgogradská	volgogradský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Volgograd	Volgograd	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Vologdská	Vologdský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Vologda	Vologda	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Voroněžská	voroněžský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Voroněž	Voroněž	k1gFnSc1
<g/>
)	)	kIx)
Federální	federální	k2eAgNnSc1d1
města	město	k1gNnSc2
</s>
<s>
Moskva	Moskva	k1gFnSc1
</s>
<s>
Petrohrad	Petrohrad	k1gInSc1
</s>
<s>
Sevastopol¹	Sevastopol¹	k?
Autonomní	autonomní	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
</s>
<s>
Židovská	židovská	k1gFnSc1
(	(	kIx(
<g/>
Birobidžan	Birobidžan	k1gInSc1
<g/>
)	)	kIx)
Autonomní	autonomní	k2eAgInPc1d1
okruhy	okruh	k1gInPc1
</s>
<s>
Čukotský	čukotský	k2eAgInSc1d1
(	(	kIx(
<g/>
Anadyr	Anadyr	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Chantymansijský	Chantymansijský	k2eAgInSc1d1
(	(	kIx(
<g/>
Chanty-Mansijsk	Chanty-Mansijsk	k1gInSc1
<g/>
)	)	kIx)
<g/>
²	²	k?
</s>
<s>
Jamalo-něnecký	Jamalo-něnecký	k2eAgInSc1d1
(	(	kIx(
<g/>
Salechard	Salechard	k1gInSc1
<g/>
)	)	kIx)
<g/>
²	²	k?
</s>
<s>
Něnecký	Něnecký	k2eAgInSc1d1
(	(	kIx(
<g/>
Narjan-Mar	Narjan-Mar	k1gInSc1
<g/>
)	)	kIx)
<g/>
³	³	k?
</s>
<s>
¹	¹	k?
Nárokováno	nárokován	k2eAgNnSc1d1
Ukrajinou	Ukrajina	k1gFnSc7
<g/>
,	,	kIx,
mezinárodně	mezinárodně	k6eAd1
považováno	považován	k2eAgNnSc1d1
za	za	k7c4
součást	součást	k1gFnSc4
Ukrajiny	Ukrajina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
²	²	k?
Spravováno	spravovat	k5eAaImNgNnS
orgány	orgán	k1gInPc7
Ťumeňské	Ťumeňský	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
³	³	k?
Spravováno	spravovat	k5eAaImNgNnS
orgány	orgán	k1gInPc7
Archangelské	archangelský	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Města	město	k1gNnPc1
s	s	k7c7
vyznamenáním	vyznamenání	k1gNnSc7
Město-hrdina	Město-hrdin	k2eAgInSc2d1
</s>
<s>
Brestská	brestský	k2eAgFnSc1d1
pevnost	pevnost	k1gFnSc1
(	(	kIx(
<g/>
pevnost-hrdina	pevnost-hrdina	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Kerč	Kerč	k1gInSc1
•	•	k?
Kyjev	Kyjev	k1gInSc1
•	•	k?
Leningrad	Leningrad	k1gInSc1
•	•	k?
Minsk	Minsk	k1gInSc1
•	•	k?
Moskva	Moskva	k1gFnSc1
•	•	k?
Murmansk	Murmansk	k1gInSc1
•	•	k?
Novorossijsk	Novorossijsk	k1gInSc1
•	•	k?
Oděsa	Oděsa	k1gFnSc1
•	•	k?
Sevastopol	Sevastopol	k1gInSc1
•	•	k?
Smolensk	Smolensk	k1gInSc1
•	•	k?
Stalingrad	Stalingrad	k1gInSc1
•	•	k?
Tula	Tula	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
128542	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4267026-3	4267026-3	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
81039599	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
139487820	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
81039599	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Rusko	Rusko	k1gNnSc1
</s>
