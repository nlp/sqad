<p>
<s>
Mordor	Mordor	k1gMnSc1	Mordor
je	být	k5eAaImIp3nS	být
země	zem	k1gFnPc4	zem
ve	v	k7c6	v
fiktivním	fiktivní	k2eAgInSc6d1	fiktivní
světě	svět	k1gInSc6	svět
anglického	anglický	k2eAgMnSc2d1	anglický
spisovatele	spisovatel	k1gMnSc2	spisovatel
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkiena	Tolkien	k1gMnSc2	Tolkien
<g/>
.	.	kIx.	.
</s>
<s>
Mordor	Mordor	k1gInSc1	Mordor
čili	čili	k8xC	čili
Černá	černý	k2eAgFnSc1d1	černá
země	země	k1gFnSc1	země
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
Středozemě	Středozem	k1gFnSc2	Středozem
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pustý	pustý	k2eAgInSc1d1	pustý
kraj	kraj	k1gInSc1	kraj
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
především	především	k9	především
jako	jako	k9	jako
panství	panství	k1gNnSc1	panství
Temného	temný	k2eAgMnSc2d1	temný
pána	pán	k1gMnSc2	pán
Saurona	Sauron	k1gMnSc2	Sauron
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Země	země	k1gFnSc1	země
Mordor	Mordora	k1gFnPc2	Mordora
je	být	k5eAaImIp3nS	být
nejvíce	hodně	k6eAd3	hodně
popsána	popsat	k5eAaPmNgFnS	popsat
v	v	k7c6	v
třetím	třetí	k4xOgNnSc6	třetí
díle	dílo	k1gNnSc6	dílo
Tolkienovy	Tolkienův	k2eAgFnSc2d1	Tolkienova
trilogie	trilogie	k1gFnSc2	trilogie
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
–	–	k?	–
Návrat	návrat	k1gInSc1	návrat
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Mordor	Mordor	k1gInSc1	Mordor
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
popisován	popisovat	k5eAaImNgInS	popisovat
především	především	k9	především
při	při	k7c6	při
líčení	líčení	k1gNnSc6	líčení
putování	putování	k1gNnSc2	putování
hlavního	hlavní	k2eAgMnSc2d1	hlavní
hrdiny	hrdina	k1gMnSc2	hrdina
románu	román	k1gInSc2	román
<g/>
,	,	kIx,	,
Frodo	Frodo	k1gNnSc4	Frodo
Pytlíka	pytlík	k1gMnSc2	pytlík
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
historii	historie	k1gFnSc6	historie
Černé	Černé	k2eAgFnSc2d1	Černé
země	zem	k1gFnSc2	zem
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
a	a	k8xC	a
třetím	třetí	k4xOgInSc6	třetí
věku	věk	k1gInSc6	věk
lze	lze	k6eAd1	lze
číst	číst	k5eAaImF	číst
v	v	k7c6	v
Silmarillionu	Silmarillion	k1gInSc6	Silmarillion
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
Mordoru	Mordor	k1gInSc6	Mordor
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
v	v	k7c6	v
Tolkienově	Tolkienův	k2eAgFnSc6d1	Tolkienova
knize	kniha	k1gFnSc6	kniha
Nedokončené	dokončený	k2eNgInPc4d1	nedokončený
příběhy	příběh	k1gInPc4	příběh
Númenoru	Númenor	k1gInSc2	Númenor
a	a	k8xC	a
Středozemě	Středozem	k1gFnSc2	Středozem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pojmenování	pojmenování	k1gNnPc1	pojmenování
a	a	k8xC	a
symboly	symbol	k1gInPc1	symbol
==	==	k?	==
</s>
</p>
<p>
<s>
Mordor	Mordor	k1gInSc1	Mordor
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
znamenající	znamenající	k2eAgFnSc1d1	znamenající
Černá	černý	k2eAgFnSc1d1	černá
země	země	k1gFnSc1	země
(	(	kIx(	(
<g/>
Mor	mor	k1gInSc1	mor
-	-	kIx~	-
"	"	kIx"	"
<g/>
Černý	černý	k2eAgMnSc1d1	černý
<g/>
/	/	kIx~	/
<g/>
Temný	temný	k2eAgMnSc1d1	temný
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Dor	Dora	k1gFnPc2	Dora
-	-	kIx~	-
"	"	kIx"	"
<g/>
Země	země	k1gFnSc1	země
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
odkazu	odkaz	k1gInSc6	odkaz
na	na	k7c4	na
Temného	temný	k2eAgMnSc4d1	temný
pána	pán	k1gMnSc4	pán
Saurona	Sauron	k1gMnSc2	Sauron
označován	označovat	k5eAaImNgInS	označovat
též	též	k9	též
jako	jako	k8xC	jako
Země	země	k1gFnSc1	země
stínu	stín	k1gInSc2	stín
či	či	k8xC	či
Země	zem	k1gFnSc2	zem
Nepřítele	nepřítel	k1gMnSc2	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Sauronovým	Sauronův	k2eAgInSc7d1	Sauronův
symbolem	symbol	k1gInSc7	symbol
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
znakem	znak	k1gInSc7	znak
Mordoru	Mordor	k1gInSc2	Mordor
bylo	být	k5eAaImAgNnS	být
zlé	zlý	k2eAgNnSc1d1	zlé
rudé	rudý	k2eAgNnSc1d1	Rudé
oko	oko	k1gNnSc1	oko
bez	bez	k7c2	bez
víčka	víčko	k1gNnSc2	víčko
na	na	k7c6	na
černém	černý	k2eAgNnSc6d1	černé
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Mordor	Mordor	k1gInSc1	Mordor
byl	být	k5eAaImAgInS	být
nehostinnou	hostinný	k2eNgFnSc7d1	nehostinná
zemí	zem	k1gFnSc7	zem
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
stran	strana	k1gFnPc2	strana
ohraničenou	ohraničený	k2eAgFnSc4d1	ohraničená
vysokými	vysoký	k2eAgFnPc7d1	vysoká
horami	hora	k1gFnPc7	hora
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
ji	on	k3xPp3gFnSc4	on
pohoří	pohořet	k5eAaPmIp3nS	pohořet
Ered	Ered	k1gInSc4	Ered
Lithui	Lithui	k1gNnSc2	Lithui
dělilo	dělit	k5eAaImAgNnS	dělit
od	od	k7c2	od
Bitevní	bitevní	k2eAgFnSc2d1	bitevní
pláně	pláň	k1gFnSc2	pláň
Dagorlad	Dagorlad	k1gInSc1	Dagorlad
a	a	k8xC	a
zbytku	zbytek	k1gInSc2	zbytek
Rhovanionu	Rhovanion	k1gInSc2	Rhovanion
<g/>
.	.	kIx.	.
</s>
<s>
Pohoří	pohoří	k1gNnSc1	pohoří
Ephel	Ephela	k1gFnPc2	Ephela
Dúath	Dúatha	k1gFnPc2	Dúatha
tvořilo	tvořit	k5eAaImAgNnS	tvořit
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
západní	západní	k2eAgFnSc4d1	západní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc4d1	jižní
hranici	hranice	k1gFnSc4	hranice
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
Mordoru	Mordor	k1gInSc2	Mordor
leželo	ležet	k5eAaImAgNnS	ležet
sousední	sousední	k2eAgNnSc4d1	sousední
království	království	k1gNnSc4	království
Gondor	Gondora	k1gFnPc2	Gondora
a	a	k8xC	a
jižněji	jižně	k6eAd2	jižně
se	se	k3xPyFc4	se
rozprostíraly	rozprostírat	k5eAaImAgFnP	rozprostírat
země	zem	k1gFnPc1	zem
Haradu	Harad	k1gInSc2	Harad
<g/>
.	.	kIx.	.
</s>
<s>
Nejpřístupnější	přístupný	k2eAgInSc1d3	nejpřístupnější
byl	být	k5eAaImAgInS	být
Mordor	Mordor	k1gInSc1	Mordor
z	z	k7c2	z
východu	východ	k1gInSc2	východ
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sousedil	sousedit	k5eAaImAgMnS	sousedit
s	s	k7c7	s
Rhû	Rhû	k1gMnSc7	Rhû
a	a	k8xC	a
Chandem	Chand	k1gMnSc7	Chand
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
části	část	k1gFnSc6	část
Mordoru	Mordor	k1gInSc2	Mordor
se	se	k3xPyFc4	se
tyčila	tyčit	k5eAaImAgFnS	tyčit
ohnivá	ohnivý	k2eAgFnSc1d1	ohnivá
Hora	hora	k1gFnSc1	hora
osudu	osud	k1gInSc2	osud
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
rozprostírala	rozprostírat	k5eAaImAgFnS	rozprostírat
nehostinná	hostinný	k2eNgFnSc1d1	nehostinná
a	a	k8xC	a
pustá	pustý	k2eAgFnSc1d1	pustá
pláň	pláň	k1gFnSc1	pláň
Gorgoroth	Gorgorotha	k1gFnPc2	Gorgorotha
<g/>
.	.	kIx.	.
</s>
<s>
Severně	severně	k6eAd1	severně
od	od	k7c2	od
Gorgorothu	Gorgoroth	k1gInSc2	Gorgoroth
<g/>
,	,	kIx,	,
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
ostrohů	ostroh	k1gInPc2	ostroh
Popelavých	popelavý	k2eAgFnPc2d1	popelavá
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
ležela	ležet	k5eAaImAgFnS	ležet
Sauronova	Sauronův	k2eAgFnSc1d1	Sauronova
Temná	temný	k2eAgFnSc1d1	temná
věž	věž	k1gFnSc1	věž
Barad-dû	Baradû	k1gFnSc2	Barad-dû
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
Mordoru	Mordor	k1gInSc2	Mordor
zabírala	zabírat	k5eAaImAgFnS	zabírat
nížinná	nížinný	k2eAgFnSc1d1	nížinná
pláň	pláň	k1gFnSc1	pláň
Nurn	Nurna	k1gFnPc2	Nurna
rozkládající	rozkládající	k2eAgFnSc2d1	rozkládající
se	se	k3xPyFc4	se
okolo	okolo	k7c2	okolo
jezera	jezero	k1gNnSc2	jezero
Núrnen	Núrnna	k1gFnPc2	Núrnna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
opevněného	opevněný	k2eAgInSc2d1	opevněný
Mordoru	Mordor	k1gInSc2	Mordor
vedly	vést	k5eAaImAgInP	vést
přes	přes	k7c4	přes
hraniční	hraniční	k2eAgFnPc4d1	hraniční
hory	hora	k1gFnPc4	hora
dva	dva	k4xCgInPc4	dva
hlavní	hlavní	k2eAgInPc4d1	hlavní
průchody	průchod	k1gInPc4	průchod
<g/>
.	.	kIx.	.
</s>
<s>
Významnější	významný	k2eAgMnPc1d2	významnější
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k2eAgMnPc1d1	vedoucí
průsmykem	průsmyk	k1gInSc7	průsmyk
Cirith	Cirith	k1gInSc1	Cirith
Gorgor	Gorgora	k1gFnPc2	Gorgora
a	a	k8xC	a
dolinou	dolina	k1gFnSc7	dolina
Udû	Udû	k1gFnSc2	Udû
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
sbíhaly	sbíhat	k5eAaImAgFnP	sbíhat
Popelavé	popelavý	k2eAgFnPc1d1	popelavá
hory	hora	k1gFnPc1	hora
a	a	k8xC	a
Hory	hora	k1gFnPc1	hora
stínu	stín	k1gInSc2	stín
<g/>
,	,	kIx,	,
bránila	bránit	k5eAaImAgFnS	bránit
před	před	k7c7	před
proniknutím	proniknutí	k1gNnSc7	proniknutí
nepřítele	nepřítel	k1gMnSc2	nepřítel
Černá	černý	k2eAgFnSc1d1	černá
brána	brána	k1gFnSc1	brána
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
průchod	průchod	k1gInSc1	průchod
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgInS	nacházet
přibližně	přibližně	k6eAd1	přibližně
uprostřed	uprostřed	k7c2	uprostřed
západní	západní	k2eAgFnSc2d1	západní
části	část	k1gFnSc2	část
Hor	hora	k1gFnPc2	hora
stínu	stín	k1gInSc2	stín
a	a	k8xC	a
procházel	procházet	k5eAaImAgMnS	procházet
skrz	skrz	k7c4	skrz
Morgulské	Morgulský	k2eAgNnSc4d1	Morgulský
údolí	údolí	k1gNnSc4	údolí
nedaleko	nedaleko	k7c2	nedaleko
věže	věž	k1gFnSc2	věž
Minas	Minasa	k1gFnPc2	Minasa
Morgul	Morgula	k1gFnPc2	Morgula
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
hory	hora	k1gFnPc4	hora
vedly	vést	k5eAaImAgInP	vést
i	i	k9	i
další	další	k2eAgInPc1d1	další
průsmyky	průsmyk	k1gInPc1	průsmyk
<g/>
,	,	kIx,	,
nebyly	být	k5eNaImAgFnP	být
však	však	k9	však
téměř	téměř	k6eAd1	téměř
využívány	využívat	k5eAaPmNgInP	využívat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejznámějším	známý	k2eAgMnSc6d3	nejznámější
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
<g/>
,	,	kIx,	,
Cirith	Cirith	k1gInSc4	Cirith
Ungolu	Ungol	k1gInSc2	Ungol
<g/>
,	,	kIx,	,
sídlila	sídlit	k5eAaImAgFnS	sídlit
obří	obří	k2eAgFnSc1d1	obří
pavoučice	pavoučice	k1gFnSc1	pavoučice
Odula	odout	k5eAaPmAgFnS	odout
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
průsmykem	průsmyk	k1gInSc7	průsmyk
byl	být	k5eAaImAgInS	být
hobitem	hobit	k1gMnSc7	hobit
Frodo	Frodo	k1gNnSc1	Frodo
Pytlíkem	pytlík	k1gInSc7	pytlík
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
Hoře	hora	k1gFnSc3	hora
osudu	osud	k1gInSc2	osud
pronesen	pronést	k5eAaPmNgInS	pronést
Jeden	jeden	k4xCgInSc1	jeden
prsten	prsten	k1gInSc1	prsten
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Regiony	region	k1gInPc4	region
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Gorgoroth	Gorgoroth	k1gInSc4	Gorgoroth
===	===	k?	===
</s>
</p>
<p>
<s>
Pláň	pláň	k1gFnSc1	pláň
Gorgoroth	Gorgorotha	k1gFnPc2	Gorgorotha
se	se	k3xPyFc4	se
rozprostírala	rozprostírat	k5eAaImAgFnS	rozprostírat
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
části	část	k1gFnSc6	část
Mordoru	Mordor	k1gInSc2	Mordor
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
této	tento	k3xDgFnSc2	tento
sopečné	sopečný	k2eAgFnSc2d1	sopečná
pustiny	pustina	k1gFnSc2	pustina
sevřené	sevřený	k2eAgFnSc2d1	sevřená
horami	hora	k1gFnPc7	hora
Ephel	Ephel	k1gMnSc1	Ephel
Dúath	Dúath	k1gMnSc1	Dúath
a	a	k8xC	a
Ered	Ered	k1gMnSc1	Ered
Lithui	Lithu	k1gFnSc2	Lithu
se	se	k3xPyFc4	se
tyčila	tyčit	k5eAaImAgFnS	tyčit
Hora	hora	k1gFnSc1	hora
osudu	osud	k1gInSc2	osud
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
Orodruině	Orodruina	k1gFnSc3	Orodruina
museli	muset	k5eAaImAgMnP	muset
Gorgoroth	Gorgoroth	k1gInSc4	Gorgoroth
překročit	překročit	k5eAaPmF	překročit
také	také	k9	také
hobiti	hobit	k1gMnPc1	hobit
Frodo	Frodo	k1gNnSc4	Frodo
a	a	k8xC	a
Sam	Sam	k1gMnSc1	Sam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nurn	Nurno	k1gNnPc2	Nurno
===	===	k?	===
</s>
</p>
<p>
<s>
Nurn	Nurn	k1gInSc4	Nurn
byla	být	k5eAaImAgFnS	být
oblast	oblast	k1gFnSc1	oblast
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
části	část	k1gFnSc6	část
Mordoru	Mordor	k1gInSc2	Mordor
rozprostírající	rozprostírající	k2eAgFnSc2d1	rozprostírající
se	se	k3xPyFc4	se
okolo	okolo	k7c2	okolo
velkého	velký	k2eAgNnSc2d1	velké
jezera	jezero	k1gNnSc2	jezero
Núrnen	Núrnna	k1gFnPc2	Núrnna
<g/>
.	.	kIx.	.
</s>
<s>
Nurn	Nurn	k1gInSc1	Nurn
byl	být	k5eAaImAgInS	být
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
a	a	k8xC	a
jihu	jih	k1gInSc2	jih
ohraničen	ohraničen	k2eAgMnSc1d1	ohraničen
pohořím	pohořet	k5eAaPmIp1nS	pohořet
Ephel	Ephel	k1gMnSc1	Ephel
Dúath	Dúath	k1gMnSc1	Dúath
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
hraničil	hraničit	k5eAaImAgMnS	hraničit
s	s	k7c7	s
pláněmi	pláň	k1gFnPc7	pláň
Gorgorothu	Gorgoroth	k1gInSc2	Gorgoroth
<g/>
,	,	kIx,	,
na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
ležel	ležet	k5eAaImAgInS	ležet
Jižní	jižní	k2eAgInSc1d1	jižní
Ithilien	Ithilien	k1gInSc1	Ithilien
a	a	k8xC	a
jižněji	jižně	k6eAd2	jižně
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgInS	nacházet
Blízký	blízký	k2eAgInSc1d1	blízký
Harad	Harad	k1gInSc1	Harad
a	a	k8xC	a
Chand	Chand	k1gInSc1	Chand
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
země	země	k1gFnSc1	země
sloužila	sloužit	k5eAaImAgFnS	sloužit
Temnému	temný	k2eAgMnSc3d1	temný
pánu	pán	k1gMnSc3	pán
jako	jako	k9	jako
potravinová	potravinový	k2eAgFnSc1d1	potravinová
základna	základna	k1gFnSc1	základna
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gNnPc4	jeho
vojska	vojsko	k1gNnPc4	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
Saurona	Sauron	k1gMnSc2	Sauron
udělil	udělit	k5eAaPmAgMnS	udělit
král	král	k1gMnSc1	král
Elessar	Elessar	k1gMnSc1	Elessar
oblast	oblast	k1gFnSc4	oblast
Nurnu	Nurna	k1gFnSc4	Nurna
zdejším	zdejší	k2eAgMnPc3d1	zdejší
osvobozeným	osvobozený	k2eAgMnPc3d1	osvobozený
otrokům	otrok	k1gMnPc3	otrok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Udû	Udû	k1gMnPc5	Udû
===	===	k?	===
</s>
</p>
<p>
<s>
Udû	Udû	k?	Udû
čili	čili	k1gNnSc1	čili
"	"	kIx"	"
<g/>
Peklo	peklo	k1gNnSc4	peklo
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
dolina	dolina	k1gFnSc1	dolina
v	v	k7c6	v
nejseverozápadnější	severozápadný	k2eAgFnSc6d3	severozápadný
části	část	k1gFnSc6	část
Mordoru	Mordor	k1gInSc2	Mordor
<g/>
.	.	kIx.	.
</s>
<s>
Udû	Udû	k?	Udû
sevřeným	sevřený	k2eAgMnSc7d1	sevřený
mezi	mezi	k7c7	mezi
Ered	Ereda	k1gFnPc2	Ereda
Lithui	Lithui	k1gNnSc1	Lithui
a	a	k8xC	a
Ephel	Ephel	k1gInSc1	Ephel
Dúath	Dúatha	k1gFnPc2	Dúatha
se	se	k3xPyFc4	se
táhl	táhnout	k5eAaImAgInS	táhnout
Strašidelný	strašidelný	k2eAgInSc1d1	strašidelný
průsmyk	průsmyk	k1gInSc1	průsmyk
-	-	kIx~	-
severní	severní	k2eAgInSc4d1	severní
průchod	průchod	k1gInSc4	průchod
do	do	k7c2	do
Mordoru	Mordor	k1gInSc2	Mordor
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgMnSc1d1	celý
Udû	Udû	k1gMnSc1	Udû
<g/>
,	,	kIx,	,
pojmenovaný	pojmenovaný	k2eAgMnSc1d1	pojmenovaný
po	po	k7c6	po
první	první	k4xOgFnSc4	první
z	z	k7c2	z
Morgothových	Morgothův	k2eAgFnPc2d1	Morgothova
pevností	pevnost	k1gFnPc2	pevnost
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
provrtán	provrtat	k5eAaPmNgInS	provrtat
tunely	tunel	k1gInPc7	tunel
a	a	k8xC	a
podzemními	podzemní	k2eAgFnPc7d1	podzemní
zbrojnicemi	zbrojnice	k1gFnPc7	zbrojnice
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgInSc1d1	severní
vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
Udû	Udû	k1gFnSc2	Udû
zabezpečovala	zabezpečovat	k5eAaImAgFnS	zabezpečovat
Černá	černý	k2eAgFnSc1d1	černá
brána	brána	k1gFnSc1	brána
Morannon	Morannona	k1gFnPc2	Morannona
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
pak	pak	k6eAd1	pak
průsmyk	průsmyk	k1gInSc4	průsmyk
Cirith	Cirith	k1gInSc4	Cirith
Gorgor	Gorgora	k1gFnPc2	Gorgora
uzavírala	uzavírat	k5eAaImAgFnS	uzavírat
soutěska	soutěska	k1gFnSc1	soutěska
Carach	Caracha	k1gFnPc2	Caracha
Angren	Angrna	k1gFnPc2	Angrna
neboli	neboli	k8xC	neboli
Železná	železný	k2eAgFnSc1d1	železná
tlama	tlama	k1gFnSc1	tlama
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
navršen	navršen	k2eAgInSc1d1	navršen
hliněný	hliněný	k2eAgInSc1d1	hliněný
val	val	k1gInSc1	val
a	a	k8xC	a
vykopán	vykopán	k2eAgInSc1d1	vykopán
hluboký	hluboký	k2eAgInSc1d1	hluboký
příkop	příkop	k1gInSc1	příkop
překročitelný	překročitelný	k2eAgInSc1d1	překročitelný
jen	jen	k9	jen
po	po	k7c6	po
jediném	jediný	k2eAgInSc6d1	jediný
mostě	most	k1gInSc6	most
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
bitvou	bitva	k1gFnSc7	bitva
u	u	k7c2	u
Morannonu	Morannon	k1gInSc2	Morannon
zde	zde	k6eAd1	zde
Temný	temný	k2eAgMnSc1d1	temný
pán	pán	k1gMnSc1	pán
shromáždil	shromáždit	k5eAaPmAgMnS	shromáždit
většinu	většina	k1gFnSc4	většina
svých	svůj	k3xOyFgNnPc2	svůj
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přírodní	přírodní	k2eAgInPc1d1	přírodní
objekty	objekt	k1gInPc1	objekt
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Cirith	Cirith	k1gMnSc1	Cirith
Gorgor	Gorgor	k1gMnSc1	Gorgor
===	===	k?	===
</s>
</p>
<p>
<s>
Cirith	Cirith	k1gMnSc1	Cirith
Gorgor	Gorgor	k1gMnSc1	Gorgor
čili	čili	k8xC	čili
"	"	kIx"	"
<g/>
Strašidelný	strašidelný	k2eAgInSc1d1	strašidelný
průsmyk	průsmyk	k1gInSc1	průsmyk
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgInS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
cest	cesta	k1gFnPc2	cesta
vedoucích	vedoucí	k2eAgMnPc2d1	vedoucí
do	do	k7c2	do
Sauronovy	Sauronův	k2eAgFnSc2d1	Sauronova
opevněné	opevněný	k2eAgFnSc2d1	opevněná
země	zem	k1gFnSc2	zem
Mordor	Mordora	k1gFnPc2	Mordora
<g/>
.	.	kIx.	.
</s>
<s>
Nacházel	nacházet	k5eAaImAgMnS	nacházet
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
svahy	svah	k1gInPc7	svah
pohoří	pohořet	k5eAaPmIp3nP	pohořet
Ephel	Ephel	k1gMnSc1	Ephel
Dúath	Dúath	k1gMnSc1	Dúath
a	a	k8xC	a
Ered	Ered	k1gMnSc1	Ered
Lithui	Lithu	k1gFnSc2	Lithu
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
od	od	k7c2	od
Cirith	Ciritha	k1gFnPc2	Ciritha
Gorgoru	Gorgor	k1gInSc2	Gorgor
se	se	k3xPyFc4	se
rozkládala	rozkládat	k5eAaImAgFnS	rozkládat
Bitevní	bitevní	k2eAgFnSc1d1	bitevní
pláň	pláň	k1gFnSc1	pláň
Dagorlad	Dagorlad	k1gInSc1	Dagorlad
a	a	k8xC	a
Mrtvé	mrtvý	k2eAgInPc1d1	mrtvý
močály	močál	k1gInPc1	močál
<g/>
,	,	kIx,	,
dál	daleko	k6eAd2	daleko
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
pak	pak	k6eAd1	pak
ležel	ležet	k5eAaImAgMnS	ležet
Udû	Udû	k1gMnSc1	Udû
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Sauronově	Sauronův	k2eAgNnSc6d1	Sauronovo
svržení	svržení	k1gNnSc6	svržení
zbudovali	zbudovat	k5eAaPmAgMnP	zbudovat
muži	muž	k1gMnPc1	muž
z	z	k7c2	z
Gondoru	Gondor	k1gInSc2	Gondor
ve	v	k7c6	v
vstupu	vstup	k1gInSc6	vstup
do	do	k7c2	do
Strašidelného	strašidelný	k2eAgInSc2d1	strašidelný
průsmyku	průsmyk	k1gInSc2	průsmyk
mocné	mocný	k2eAgFnPc1d1	mocná
věže	věž	k1gFnPc1	věž
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
bránit	bránit	k5eAaImF	bránit
jeho	jeho	k3xOp3gInSc3	jeho
návratu	návrat	k1gInSc3	návrat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zeslábnutí	zeslábnutí	k1gNnSc6	zeslábnutí
Jižního	jižní	k2eAgNnSc2d1	jižní
království	království	k1gNnSc2	království
a	a	k8xC	a
zchátrání	zchátrání	k1gNnSc2	zchátrání
opevnění	opevnění	k1gNnSc2	opevnění
se	se	k3xPyFc4	se
však	však	k9	však
Temný	temný	k2eAgMnSc1d1	temný
pán	pán	k1gMnSc1	pán
přesto	přesto	k8xC	přesto
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Mordoru	Mordor	k1gInSc2	Mordor
a	a	k8xC	a
pevnost	pevnost	k1gFnSc4	pevnost
v	v	k7c4	v
Cirith	Cirith	k1gInSc4	Cirith
Gorgoru	Gorgor	k1gInSc2	Gorgor
nechal	nechat	k5eAaPmAgMnS	nechat
opravit	opravit	k5eAaPmF	opravit
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
nedobytná	dobytný	k2eNgFnSc1d1	nedobytná
brána	brána	k1gFnSc1	brána
<g/>
,	,	kIx,	,
u	u	k7c2	u
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
na	na	k7c6	na
konci	konec	k1gInSc6	konec
třetího	třetí	k4xOgInSc2	třetí
věku	věk	k1gInSc2	věk
odehrála	odehrát	k5eAaPmAgFnS	odehrát
velká	velký	k2eAgFnSc1d1	velká
bitva	bitva	k1gFnSc1	bitva
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
známa	znám	k2eAgFnSc1d1	známa
jako	jako	k8xS	jako
Morannon	Morannon	k1gInSc1	Morannon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Cirith	Ciritha	k1gFnPc2	Ciritha
Ungol	Ungol	k1gInSc1	Ungol
===	===	k?	===
</s>
</p>
<p>
<s>
Cirith	Cirith	k1gInSc1	Cirith
Ungol	Ungol	k1gInSc1	Ungol
tedy	tedy	k8xC	tedy
"	"	kIx"	"
<g/>
Pavoučí	pavoučí	k2eAgInSc1d1	pavoučí
průsmyk	průsmyk	k1gInSc1	průsmyk
<g/>
"	"	kIx"	"
překračoval	překračovat	k5eAaImAgMnS	překračovat
hřebeny	hřeben	k1gInPc4	hřeben
Ephel	Ephela	k1gFnPc2	Ephela
Dúath	Dúatha	k1gFnPc2	Dúatha
nedaleko	nedaleko	k7c2	nedaleko
Morgulského	Morgulský	k2eAgNnSc2d1	Morgulský
údolí	údolí	k1gNnSc2	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
dostal	dostat	k5eAaPmAgMnS	dostat
tento	tento	k3xDgInSc4	tento
průsmyk	průsmyk	k1gInSc4	průsmyk
po	po	k7c6	po
potomcích	potomek	k1gMnPc6	potomek
dávné	dávný	k2eAgMnPc4d1	dávný
pavoučice	pavoučice	k1gFnSc2	pavoučice
Ungoliant	Ungolianta	k1gFnPc2	Ungolianta
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc4	její
strašlivé	strašlivý	k2eAgNnSc4d1	strašlivé
potomstvo	potomstvo	k1gNnSc4	potomstvo
toto	tento	k3xDgNnSc4	tento
místo	místo	k1gNnSc1	místo
obývalo	obývat	k5eAaImAgNnS	obývat
od	od	k7c2	od
nepaměti	nepaměť	k1gFnSc2	nepaměť
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgMnSc1d3	nejznámější
a	a	k8xC	a
poslední	poslední	k2eAgMnSc1d1	poslední
z	z	k7c2	z
Ungoliantiných	Ungoliantin	k2eAgMnPc2d1	Ungoliantin
potomků	potomek	k1gMnPc2	potomek
byla	být	k5eAaImAgFnS	být
Odula	odout	k5eAaPmAgFnS	odout
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterou	který	k3yRgFnSc7	který
se	se	k3xPyFc4	se
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
cestě	cesta	k1gFnSc6	cesta
přes	přes	k7c4	přes
Cirith	Cirith	k1gInSc4	Cirith
Ungol	Ungola	k1gFnPc2	Ungola
utkali	utkat	k5eAaPmAgMnP	utkat
i	i	k9	i
hobiti	hobit	k1gMnPc1	hobit
Frodo	Frodo	k1gNnSc4	Frodo
Pytlík	pytlík	k1gMnSc1	pytlík
a	a	k8xC	a
Samvěd	Samvěd	k1gMnSc1	Samvěd
Křepelka	křepelka	k1gFnSc1	křepelka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
sem	sem	k6eAd1	sem
zákeřně	zákeřně	k6eAd1	zákeřně
přivedl	přivést	k5eAaPmAgMnS	přivést
jejich	jejich	k3xOp3gNnSc4	jejich
průvodce	průvodce	k1gMnSc1	průvodce
Glum	Glum	k1gMnSc1	Glum
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
straně	strana	k1gFnSc6	strana
průsmyku	průsmyk	k1gInSc2	průsmyk
stávala	stávat	k5eAaImAgFnS	stávat
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
třetího	třetí	k4xOgInSc2	třetí
věku	věk	k1gInSc2	věk
silná	silný	k2eAgFnSc1d1	silná
věž	věž	k1gFnSc1	věž
vystavěná	vystavěný	k2eAgFnSc1d1	vystavěná
Gondorskými	Gondorský	k2eAgFnPc7d1	Gondorská
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
proti	proti	k7c3	proti
Sauronově	Sauronův	k2eAgInSc6d1	Sauronův
návratu	návrat	k1gInSc6	návrat
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
další	další	k2eAgInPc1d1	další
z	z	k7c2	z
Dúnadanských	Dúnadanský	k2eAgFnPc2d1	Dúnadanský
pevností	pevnost	k1gFnPc2	pevnost
v	v	k7c6	v
hraničních	hraniční	k2eAgFnPc6d1	hraniční
horách	hora	k1gFnPc6	hora
byla	být	k5eAaImAgFnS	být
opuštěna	opuštěn	k2eAgFnSc1d1	opuštěna
a	a	k8xC	a
záhy	záhy	k6eAd1	záhy
obsazena	obsadit	k5eAaPmNgFnS	obsadit
služebníky	služebník	k1gMnPc4	služebník
Temného	temný	k2eAgMnSc4d1	temný
pána	pán	k1gMnSc4	pán
<g/>
.	.	kIx.	.
</s>
<s>
Skřeti	skřet	k1gMnPc1	skřet
z	z	k7c2	z
věže	věž	k1gFnSc2	věž
v	v	k7c4	v
Cirith	Cirith	k1gInSc4	Cirith
Ungolu	Ungol	k1gInSc2	Ungol
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
kapitánem	kapitán	k1gMnSc7	kapitán
Šagratem	Šagrat	k1gMnSc7	Šagrat
objevili	objevit	k5eAaPmAgMnP	objevit
nedaleko	nedaleko	k7c2	nedaleko
pavoučího	pavoučí	k2eAgInSc2d1	pavoučí
tunelu	tunel	k1gInSc2	tunel
paralyzovaného	paralyzovaný	k2eAgMnSc4d1	paralyzovaný
Froda	Frod	k1gMnSc4	Frod
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
odnesli	odnést	k5eAaPmAgMnP	odnést
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
věže	věž	k1gFnSc2	věž
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
došlo	dojít	k5eAaPmAgNnS	dojít
kvůli	kvůli	k7c3	kvůli
hobitovým	hobitův	k2eAgFnPc3d1	hobitova
cennostem	cennost	k1gFnPc3	cennost
ke	k	k7c3	k
krvavému	krvavý	k2eAgInSc3d1	krvavý
souboji	souboj	k1gInSc3	souboj
se	se	k3xPyFc4	se
skřety	skřet	k1gMnPc7	skřet
z	z	k7c2	z
Minas	Minasa	k1gFnPc2	Minasa
Morgul	Morgula	k1gFnPc2	Morgula
a	a	k8xC	a
Samovi	Samův	k2eAgMnPc1d1	Samův
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
podařilo	podařit	k5eAaPmAgNnS	podařit
svého	svůj	k3xOyFgMnSc4	svůj
pána	pán	k1gMnSc4	pán
osvobodit	osvobodit	k5eAaPmF	osvobodit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Morgai	Morgai	k1gNnPc3	Morgai
===	===	k?	===
</s>
</p>
<p>
<s>
Morgai	Morgai	k6eAd1	Morgai
neboli	neboli	k8xC	neboli
"	"	kIx"	"
<g/>
Černý	černý	k2eAgInSc1d1	černý
plot	plot	k1gInSc1	plot
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgInS	být
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
hřeben	hřeben	k1gInSc4	hřeben
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgInSc4d1	ležící
východně	východně	k6eAd1	východně
od	od	k7c2	od
Ephel	Ephela	k1gFnPc2	Ephela
Dúath	Dúatha	k1gFnPc2	Dúatha
<g/>
.	.	kIx.	.
</s>
<s>
Táhl	táhnout	k5eAaImAgMnS	táhnout
se	se	k3xPyFc4	se
rovnoběžně	rovnoběžně	k6eAd1	rovnoběžně
s	s	k7c7	s
mnohem	mnohem	k6eAd1	mnohem
vyššími	vysoký	k2eAgFnPc7d2	vyšší
Horami	hora	k1gFnPc7	hora
stínu	stín	k1gInSc2	stín
a	a	k8xC	a
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
zelo	zet	k5eAaImAgNnS	zet
hluboké	hluboký	k2eAgNnSc4d1	hluboké
koryto	koryto	k1gNnSc4	koryto
<g/>
.	.	kIx.	.
</s>
<s>
Pohoří	pohoří	k1gNnSc1	pohoří
Morgai	Morgai	k1gNnSc2	Morgai
sloužilo	sloužit	k5eAaImAgNnS	sloužit
jako	jako	k8xC	jako
druhý	druhý	k4xOgInSc1	druhý
či	či	k9wB	či
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
obranný	obranný	k2eAgInSc1d1	obranný
kruh	kruh	k1gInSc1	kruh
Mordoru	Mordor	k1gInSc2	Mordor
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
Hoře	hora	k1gFnSc3	hora
osudu	osud	k1gInSc2	osud
se	se	k3xPyFc4	se
podél	podél	k7c2	podél
štítu	štít	k1gInSc2	štít
Morgai	Morga	k1gFnSc2	Morga
přesouvali	přesouvat	k5eAaImAgMnP	přesouvat
hobiti	hobit	k1gMnPc1	hobit
Frodo	Frodo	k1gNnSc4	Frodo
Pytlík	pytlík	k1gMnSc1	pytlík
a	a	k8xC	a
Samvěd	Samvěd	k1gMnSc1	Samvěd
Křepelka	křepelka	k1gFnSc1	křepelka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Morgulské	Morgulský	k2eAgNnSc1d1	Morgulský
údolí	údolí	k1gNnSc1	údolí
===	===	k?	===
</s>
</p>
<p>
<s>
Morgulské	Morgulský	k2eAgNnSc1d1	Morgulský
údolí	údolí	k1gNnSc1	údolí
známé	známý	k2eAgNnSc1d1	známé
jako	jako	k9	jako
Imlad	Imlad	k1gInSc4	Imlad
Morgul	Morgula	k1gFnPc2	Morgula
bylo	být	k5eAaImAgNnS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
průchodů	průchod	k1gInPc2	průchod
do	do	k7c2	do
Mordoru	Mordor	k1gInSc2	Mordor
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
západu	západ	k1gInSc2	západ
na	na	k7c4	na
východ	východ	k1gInSc4	východ
protínalo	protínat	k5eAaImAgNnS	protínat
hřebeny	hřeben	k1gInPc7	hřeben
Ephel	Ephel	k1gMnSc1	Ephel
Dúath	Dúath	k1gMnSc1	Dúath
a	a	k8xC	a
spojovalo	spojovat	k5eAaImAgNnS	spojovat
zemi	zem	k1gFnSc4	zem
Mordor	Mordora	k1gFnPc2	Mordora
se	s	k7c7	s
západněji	západně	k6eAd2	západně
položeným	položený	k2eAgInSc7d1	položený
Ithilienem	Ithilien	k1gInSc7	Ithilien
<g/>
.	.	kIx.	.
</s>
<s>
Morgulským	Morgulský	k2eAgNnSc7d1	Morgulský
údolím	údolí	k1gNnSc7	údolí
protékala	protékat	k5eAaImAgFnS	protékat
řeka	řeka	k1gFnSc1	řeka
Morgulduina	Morgulduina	k1gFnSc1	Morgulduina
a	a	k8xC	a
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
západním	západní	k2eAgInSc6d1	západní
okraji	okraj	k1gInSc6	okraj
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
čarodějnická	čarodějnický	k2eAgFnSc1d1	čarodějnická
pevnost	pevnost	k1gFnSc1	pevnost
Minas	Minasa	k1gFnPc2	Minasa
Morgul	Morgula	k1gFnPc2	Morgula
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
níž	jenž	k3xRgFnSc2	jenž
prakticky	prakticky	k6eAd1	prakticky
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
do	do	k7c2	do
Mordoru	Mordor	k1gInSc2	Mordor
proklouznout	proklouznout	k5eAaPmF	proklouznout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Núrnen	Núrnno	k1gNnPc2	Núrnno
===	===	k?	===
</s>
</p>
<p>
<s>
Jezero	jezero	k1gNnSc1	jezero
Núrnen	Núrnna	k1gFnPc2	Núrnna
<g/>
,	,	kIx,	,
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
znamenající	znamenající	k2eAgNnSc1d1	znamenající
"	"	kIx"	"
<g/>
Smutná	smutný	k2eAgFnSc1d1	smutná
voda	voda	k1gFnSc1	voda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
vnitrozemské	vnitrozemský	k2eAgNnSc1d1	vnitrozemské
moře	moře	k1gNnSc1	moře
nacházejícím	nacházející	k2eAgInSc7d1	nacházející
se	se	k3xPyFc4	se
uprostřed	uprostřed	k7c2	uprostřed
jižní	jižní	k2eAgFnSc2d1	jižní
části	část	k1gFnSc2	část
Mordoru	Mordor	k1gInSc2	Mordor
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
známé	známá	k1gFnSc2	známá
jako	jako	k8xC	jako
Nurn	Nurna	k1gFnPc2	Nurna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Núrnenu	Núrnen	k1gInSc2	Núrnen
pracovali	pracovat	k5eAaImAgMnP	pracovat
Sauronovi	Sauronův	k2eAgMnPc1d1	Sauronův
otroci	otrok	k1gMnPc1	otrok
produkující	produkující	k2eAgFnSc2d1	produkující
potraviny	potravina	k1gFnSc2	potravina
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gNnPc4	jeho
obrovská	obrovský	k2eAgNnPc4d1	obrovské
vojska	vojsko	k1gNnPc4	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
Saurona	Sauron	k1gMnSc2	Sauron
udělil	udělit	k5eAaPmAgMnS	udělit
král	král	k1gMnSc1	král
Elessar	Elessar	k1gMnSc1	Elessar
oblasti	oblast	k1gFnSc2	oblast
okolo	okolo	k7c2	okolo
Núrnenu	Núrnen	k1gInSc2	Núrnen
osvobozeným	osvobozený	k2eAgMnPc3d1	osvobozený
Sauronovým	Sauronův	k2eAgMnPc3d1	Sauronův
otrokům	otrok	k1gMnPc3	otrok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Mordoru	Mordor	k1gInSc6	Mordor
se	se	k3xPyFc4	se
na	na	k7c6	na
konci	konec	k1gInSc6	konec
prvního	první	k4xOgNnSc2	první
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
druhého	druhý	k4xOgMnSc2	druhý
věku	věk	k1gInSc2	věk
opevnil	opevnit	k5eAaPmAgMnS	opevnit
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
z	z	k7c2	z
Morgothových	Morgothův	k2eAgMnPc2d1	Morgothův
pobočníků	pobočník	k1gMnPc2	pobočník
<g/>
,	,	kIx,	,
Sauron	Sauron	k1gInSc4	Sauron
<g/>
.	.	kIx.	.
</s>
<s>
Učinil	učinit	k5eAaPmAgInS	učinit
tak	tak	k9	tak
nepochybně	pochybně	k6eNd1	pochybně
pro	pro	k7c4	pro
přírodní	přírodní	k2eAgInSc4d1	přírodní
charakter	charakter	k1gInSc4	charakter
této	tento	k3xDgFnSc2	tento
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
obklopená	obklopený	k2eAgFnSc1d1	obklopená
horami	hora	k1gFnPc7	hora
a	a	k8xC	a
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
tak	tak	k6eAd1	tak
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
velká	velký	k2eAgFnSc1d1	velká
pevnost	pevnost	k1gFnSc1	pevnost
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
skrývala	skrývat	k5eAaImAgFnS	skrývat
Sauronovy	Sauronův	k2eAgInPc4d1	Sauronův
plány	plán	k1gInPc4	plán
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vykování	vykování	k1gNnSc6	vykování
Prstenů	prsten	k1gInPc2	prsten
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
dokončení	dokončení	k1gNnSc2	dokončení
věže	věž	k1gFnSc2	věž
Barad-dû	Baradû	k1gFnSc2	Barad-dû
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
nový	nový	k2eAgMnSc1d1	nový
Temný	temný	k2eAgMnSc1d1	temný
pán	pán	k1gMnSc1	pán
obsazovat	obsazovat	k5eAaImF	obsazovat
okolní	okolní	k2eAgFnPc4d1	okolní
země	zem	k1gFnPc4	zem
osídlené	osídlený	k2eAgFnPc4d1	osídlená
lidmi	člověk	k1gMnPc7	člověk
a	a	k8xC	a
z	z	k7c2	z
Mordoru	Mordor	k1gInSc2	Mordor
zahájil	zahájit	k5eAaPmAgMnS	zahájit
válku	válka	k1gFnSc4	válka
s	s	k7c7	s
elfy	elf	k1gMnPc7	elf
<g/>
.	.	kIx.	.
</s>
<s>
Mordorská	Mordorský	k2eAgNnPc1d1	Mordorský
vojska	vojsko	k1gNnPc1	vojsko
však	však	k9	však
byla	být	k5eAaImAgNnP	být
poražena	poražen	k2eAgNnPc1d1	poraženo
díky	díky	k7c3	díky
vojenské	vojenský	k2eAgFnSc3d1	vojenská
pomoci	pomoc	k1gFnSc3	pomoc
Númenorejců	Númenorejec	k1gMnPc2	Númenorejec
a	a	k8xC	a
Sauron	Sauron	k1gInSc1	Sauron
se	se	k3xPyFc4	se
raději	rád	k6eAd2	rád
nechal	nechat	k5eAaPmAgMnS	nechat
lstivě	lstivě	k6eAd1	lstivě
zajmout	zajmout	k5eAaPmF	zajmout
a	a	k8xC	a
odvést	odvést	k5eAaPmF	odvést
na	na	k7c4	na
Númenor	Númenor	k1gInSc4	Númenor
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
zkazil	zkazit	k5eAaPmAgMnS	zkazit
srdce	srdce	k1gNnPc4	srdce
většiny	většina	k1gFnSc2	většina
tamních	tamní	k2eAgMnPc2d1	tamní
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
přispěl	přispět	k5eAaPmAgMnS	přispět
ke	k	k7c3	k
zničení	zničení	k1gNnSc3	zničení
celého	celý	k2eAgInSc2d1	celý
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Mordoru	Mordor	k1gInSc2	Mordor
a	a	k8xC	a
rozpoutal	rozpoutat	k5eAaPmAgMnS	rozpoutat
válku	válka	k1gFnSc4	válka
s	s	k7c7	s
přeživšími	přeživší	k2eAgMnPc7d1	přeživší
Dúnadany	Dúnadan	k1gMnPc7	Dúnadan
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
nepřítomnosti	nepřítomnost	k1gFnSc6	nepřítomnost
založili	založit	k5eAaPmAgMnP	založit
ve	v	k7c6	v
Středozemi	Středozem	k1gFnSc6	Středozem
království	království	k1gNnSc2	království
Arnor	Arnora	k1gFnPc2	Arnora
a	a	k8xC	a
Gondor	Gondora	k1gFnPc2	Gondora
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
3434	[number]	k4	3434
druhého	druhý	k4xOgInSc2	druhý
věku	věk	k1gInSc2	věk
dojednali	dojednat	k5eAaPmAgMnP	dojednat
králové	králová	k1gFnSc3	králová
elfů	elf	k1gMnPc2	elf
a	a	k8xC	a
lidí	člověk	k1gMnPc2	člověk
Gil-galad	Gilalad	k1gInSc1	Gil-galad
a	a	k8xC	a
Elendil	Elendil	k1gFnSc1	Elendil
Poslední	poslední	k2eAgNnSc4d1	poslední
spojenectví	spojenectví	k1gNnSc4	spojenectví
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgFnPc1d1	spojená
armády	armáda	k1gFnPc1	armáda
porazily	porazit	k5eAaPmAgFnP	porazit
Sauronova	Sauronův	k2eAgNnPc1d1	Sauronovo
mordorská	mordorský	k2eAgNnPc1d1	mordorský
vojska	vojsko	k1gNnPc1	vojsko
<g/>
,	,	kIx,	,
zbořily	zbořit	k5eAaPmAgInP	zbořit
Černou	černý	k2eAgFnSc4d1	černá
bránu	brána	k1gFnSc4	brána
<g/>
,	,	kIx,	,
pronikly	proniknout	k5eAaPmAgFnP	proniknout
do	do	k7c2	do
Mordoru	Mordor	k1gInSc2	Mordor
a	a	k8xC	a
oblehly	oblehnout	k5eAaPmAgFnP	oblehnout
Barad-dû	Baradû	k1gFnPc1	Barad-dû
<g/>
.	.	kIx.	.
</s>
<s>
Sauron	Sauron	k1gInSc1	Sauron
byl	být	k5eAaImAgInS	být
tehdy	tehdy	k6eAd1	tehdy
dík	dík	k7c3	dík
Elendilovu	Elendilův	k2eAgMnSc3d1	Elendilův
synovi	syn	k1gMnSc3	syn
Isildurovi	Isildur	k1gMnSc3	Isildur
poražen	poražen	k2eAgMnSc1d1	poražen
<g/>
,	,	kIx,	,
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
a	a	k8xC	a
také	také	k9	také
všichni	všechen	k3xTgMnPc1	všechen
jeho	jeho	k3xOp3gMnPc1	jeho
přeživší	přeživší	k2eAgMnPc1d1	přeživší
služebníci	služebník	k1gMnPc1	služebník
z	z	k7c2	z
Mordoru	Mordor	k1gInSc2	Mordor
unikli	uniknout	k5eAaPmAgMnP	uniknout
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
třetího	třetí	k4xOgInSc2	třetí
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
následoval	následovat	k5eAaImAgInS	následovat
po	po	k7c6	po
Sauronově	Sauronův	k2eAgFnSc6d1	Sauronova
porážce	porážka	k1gFnSc6	porážka
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Mordor	Mordor	k1gMnSc1	Mordor
neobývaný	obývaný	k2eNgMnSc1d1	neobývaný
a	a	k8xC	a
na	na	k7c6	na
jeho	jeho	k3xOp3gFnPc6	jeho
hranicích	hranice	k1gFnPc6	hranice
hlídali	hlídat	k5eAaImAgMnP	hlídat
Gondorští	Gondorský	k2eAgMnPc1d1	Gondorský
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
v	v	k7c6	v
hraničních	hraniční	k2eAgFnPc6d1	hraniční
horách	hora	k1gFnPc6	hora
zbudovali	zbudovat	k5eAaPmAgMnP	zbudovat
mnoho	mnoho	k4c1	mnoho
pohraničních	pohraniční	k2eAgFnPc2d1	pohraniční
pevností	pevnost	k1gFnPc2	pevnost
jako	jako	k8xS	jako
byla	být	k5eAaImAgFnS	být
věž	věž	k1gFnSc4	věž
v	v	k7c4	v
Cirith	Cirith	k1gInSc4	Cirith
Ungolu	Ungol	k1gInSc2	Ungol
či	či	k8xC	či
hrad	hrad	k1gInSc4	hrad
Durthang	Durthanga	k1gFnPc2	Durthanga
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
zabránit	zabránit	k5eAaPmF	zabránit
Sauronovi	Sauron	k1gMnSc3	Sauron
v	v	k7c6	v
návratu	návrat	k1gInSc6	návrat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
velkém	velký	k2eAgInSc6d1	velký
moru	mor	k1gInSc6	mor
však	však	k9	však
pohraniční	pohraniční	k2eAgFnPc4d1	pohraniční
pevnosti	pevnost	k1gFnPc4	pevnost
zpustly	zpustnout	k5eAaPmAgInP	zpustnout
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
opuštěny	opustit	k5eAaPmNgInP	opustit
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
je	on	k3xPp3gMnPc4	on
obsadili	obsadit	k5eAaPmAgMnP	obsadit
vracející	vracející	k2eAgMnPc1d1	vracející
se	s	k7c7	s
skřeti	skřet	k1gMnPc5	skřet
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Sauron	Sauron	k1gMnSc1	Sauron
svůj	svůj	k3xOyFgInSc4	svůj
návrat	návrat	k1gInSc4	návrat
dlouho	dlouho	k6eAd1	dlouho
připravoval	připravovat	k5eAaImAgMnS	připravovat
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc1	jeho
duch	duch	k1gMnSc1	duch
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
nazgû	nazgû	k?	nazgû
do	do	k7c2	do
Mordoru	Mordor	k1gInSc2	Mordor
roku	rok	k1gInSc2	rok
2942	[number]	k4	2942
třetího	třetí	k4xOgInSc2	třetí
věku	věk	k1gInSc2	věk
skutečně	skutečně	k6eAd1	skutečně
vrátil	vrátit	k5eAaPmAgInS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
válka	válka	k1gFnSc1	válka
o	o	k7c4	o
Prsten	prsten	k1gInSc4	prsten
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
Temný	temný	k2eAgMnSc1d1	temný
pán	pán	k1gMnSc1	pán
pokusil	pokusit	k5eAaPmAgMnS	pokusit
obsadit	obsadit	k5eAaPmF	obsadit
svobodné	svobodný	k2eAgInPc1d1	svobodný
národy	národ	k1gInPc1	národ
Středozemě	Středozem	k1gFnSc2	Středozem
a	a	k8xC	a
během	během	k7c2	během
níž	jenž	k3xRgFnSc2	jenž
jeho	jeho	k3xOp3gMnSc1	jeho
služebníci	služebník	k1gMnPc1	služebník
pátrali	pátrat	k5eAaImAgMnP	pátrat
po	po	k7c6	po
Vládnoucím	vládnoucí	k2eAgInSc6d1	vládnoucí
prstenu	prsten	k1gInSc6	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
zkázu	zkáza	k1gFnSc4	zkáza
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
zničení	zničení	k1gNnSc3	zničení
Jednoho	jeden	k4xCgInSc2	jeden
prstenu	prsten	k1gInSc2	prsten
v	v	k7c6	v
Hoře	hora	k1gFnSc6	hora
osudu	osud	k1gInSc2	osud
<g/>
.	.	kIx.	.
</s>
<s>
Sauronův	Sauronův	k2eAgMnSc1d1	Sauronův
duch	duch	k1gMnSc1	duch
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
všechny	všechen	k3xTgFnPc1	všechen
zlé	zlý	k2eAgFnPc1d1	zlá
bytosti	bytost	k1gFnPc1	bytost
jež	jenž	k3xRgFnSc1	jenž
svou	svůj	k3xOyFgFnSc7	svůj
vůlí	vůle	k1gFnSc7	vůle
ovládal	ovládat	k5eAaImAgInS	ovládat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
Mordoru	Mordor	k1gInSc2	Mordor
nadobro	nadobro	k6eAd1	nadobro
vyhnán	vyhnán	k2eAgInSc1d1	vyhnán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
království	království	k1gNnSc6	království
Gondor	Gondor	k1gMnSc1	Gondor
usednul	usednout	k5eAaPmAgMnS	usednout
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
král	král	k1gMnSc1	král
Elessar	Elessar	k1gMnSc1	Elessar
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
osvobodil	osvobodit	k5eAaPmAgMnS	osvobodit
otroky	otrok	k1gMnPc4	otrok
Temného	temný	k2eAgMnSc4d1	temný
pána	pán	k1gMnSc4	pán
pracující	pracující	k1gFnSc2	pracující
v	v	k7c6	v
Nurnu	Nurn	k1gInSc6	Nurn
na	na	k7c4	na
zásobování	zásobování	k1gNnSc4	zásobování
jeho	jeho	k3xOp3gFnPc2	jeho
armád	armáda	k1gFnPc2	armáda
a	a	k8xC	a
planina	planina	k1gFnSc1	planina
Nurn	Nurn	k1gInSc4	Nurn
jim	on	k3xPp3gMnPc3	on
byla	být	k5eAaImAgFnS	být
odevzdána	odevzdat	k5eAaPmNgFnS	odevzdat
do	do	k7c2	do
užívání	užívání	k1gNnSc2	užívání
pod	pod	k7c7	pod
svrchovaností	svrchovanost	k1gFnSc7	svrchovanost
Gondorského	Gondorský	k2eAgNnSc2d1	Gondorské
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Severozápadní	severozápadní	k2eAgFnSc1d1	severozápadní
oblast	oblast	k1gFnSc1	oblast
Mordoru	Mordor	k1gInSc2	Mordor
byla	být	k5eAaImAgFnS	být
nehostinná	hostinný	k2eNgFnSc1d1	nehostinná
pustina	pustina	k1gFnSc1	pustina
obydlená	obydlený	k2eAgFnSc1d1	obydlená
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
řídce	řídce	k6eAd1	řídce
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
Saurona	Saurona	k1gFnSc1	Saurona
ani	ani	k8xC	ani
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
pádu	pád	k1gInSc6	pád
tyto	tento	k3xDgFnPc4	tento
oblasti	oblast	k1gFnPc4	oblast
lidé	člověk	k1gMnPc1	člověk
neobývali	obývat	k5eNaImAgMnP	obývat
a	a	k8xC	a
Gondorští	Gondorský	k2eAgMnPc1d1	Gondorský
se	se	k3xPyFc4	se
spokojili	spokojit	k5eAaPmAgMnP	spokojit
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
opevněním	opevnění	k1gNnSc7	opevnění
hraničních	hraniční	k2eAgNnPc2d1	hraniční
pohoří	pohoří	k1gNnPc2	pohoří
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
množství	množství	k1gNnSc3	množství
hradů	hrad	k1gInPc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
třetího	třetí	k4xOgInSc2	třetí
věku	věk	k1gInSc2	věk
se	se	k3xPyFc4	se
vojska	vojsko	k1gNnSc2	vojsko
Temného	temný	k2eAgMnSc4d1	temný
pána	pán	k1gMnSc4	pán
shromažďovaná	shromažďovaný	k2eAgNnPc4d1	shromažďované
k	k	k7c3	k
válce	válka	k1gFnSc3	válka
o	o	k7c4	o
Prsten	prsten	k1gInSc4	prsten
nacházela	nacházet	k5eAaImAgFnS	nacházet
především	především	k9	především
v	v	k7c6	v
pevnostech	pevnost	k1gFnPc6	pevnost
Barad-dû	Baradû	k1gFnPc2	Barad-dû
<g/>
,	,	kIx,	,
Minas	Minasa	k1gFnPc2	Minasa
Morgul	Morgula	k1gFnPc2	Morgula
<g/>
,	,	kIx,	,
v	v	k7c6	v
Udunu	Uduno	k1gNnSc6	Uduno
či	či	k8xC	či
na	na	k7c6	na
západě	západ	k1gInSc6	západ
pláně	pláň	k1gFnPc4	pláň
Gorgoroth	Gorgorotha	k1gFnPc2	Gorgorotha
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
Mordoru	Mordor	k1gInSc2	Mordor
obývali	obývat	k5eAaImAgMnP	obývat
skřeti	skřet	k1gMnPc1	skřet
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
žili	žít	k5eAaImAgMnP	žít
zde	zde	k6eAd1	zde
též	též	k9	též
lidé	člověk	k1gMnPc1	člověk
dobrovolně	dobrovolně	k6eAd1	dobrovolně
či	či	k8xC	či
nedobrovolně	dobrovolně	k6eNd1	dobrovolně
sloužící	sloužící	k1gFnSc4	sloužící
Sauronovi	Sauron	k1gMnSc3	Sauron
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
Sauronových	Sauronův	k2eAgMnPc2d1	Sauronův
otroků	otrok	k1gMnPc2	otrok
živících	živící	k2eAgMnPc2d1	živící
jeho	jeho	k3xOp3gFnSc3	jeho
armády	armáda	k1gFnSc2	armáda
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
jezera	jezero	k1gNnSc2	jezero
Núrnen	Núrnna	k1gFnPc2	Núrnna
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
plání	pláň	k1gFnPc2	pláň
Gorgorothu	Gorgoroth	k1gInSc2	Gorgoroth
pocházeli	pocházet	k5eAaImAgMnP	pocházet
velcí	velký	k2eAgMnPc1d1	velký
horští	horský	k2eAgMnPc1d1	horský
obři	obr	k1gMnPc1	obr
s	s	k7c7	s
rohovinovatou	rohovinovatý	k2eAgFnSc7d1	rohovinovatý
kůží	kůže	k1gFnSc7	kůže
a	a	k8xC	a
černou	černý	k2eAgFnSc7d1	černá
krví	krev	k1gFnSc7	krev
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
náčelníka	náčelník	k1gMnSc2	náčelník
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
usmrtit	usmrtit	k5eAaPmF	usmrtit
hobitu	hobit	k1gMnSc3	hobit
Pipinovi	Pipin	k1gMnSc3	Pipin
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
před	před	k7c7	před
Černou	Černá	k1gFnSc7	Černá
bránou	brána	k1gFnSc7	brána
<g/>
.	.	kIx.	.
<g/>
Jazykem	jazyk	k1gInSc7	jazyk
Sauronových	Sauronův	k2eAgMnPc2d1	Sauronův
služebníků	služebník	k1gMnPc2	služebník
byla	být	k5eAaImAgFnS	být
Černá	černý	k2eAgFnSc1d1	černá
řeč	řeč	k1gFnSc1	řeč
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
mnozí	mnohý	k2eAgMnPc1d1	mnohý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
při	při	k7c6	při
komunikaci	komunikace	k1gFnSc6	komunikace
uchylovali	uchylovat	k5eAaImAgMnP	uchylovat
k	k	k7c3	k
užívání	užívání	k1gNnSc3	užívání
Obecné	obecný	k2eAgFnSc2d1	obecná
řeči	řeč	k1gFnSc2	řeč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Adaptace	adaptace	k1gFnSc1	adaptace
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Jacksonově	Jacksonův	k2eAgFnSc6d1	Jacksonova
filmové	filmový	k2eAgFnSc6d1	filmová
trilogii	trilogie	k1gFnSc6	trilogie
byl	být	k5eAaImAgInS	být
Mordor	Mordor	k1gInSc1	Mordor
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
ostatní	ostatní	k2eAgNnPc1d1	ostatní
místa	místo	k1gNnPc1	místo
Středozemě	Středozem	k1gFnSc2	Středozem
natáčen	natáčet	k5eAaImNgInS	natáčet
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
předloha	předloha	k1gFnSc1	předloha
pro	pro	k7c4	pro
Horu	hora	k1gFnSc4	hora
Osudu	osud	k1gInSc2	osud
posloužila	posloužit	k5eAaPmAgFnS	posloužit
sopka	sopka	k1gFnSc1	sopka
Ngauruhoe	Ngauruho	k1gFnSc2	Ngauruho
a	a	k8xC	a
pozadí	pozadí	k1gNnSc2	pozadí
úvodní	úvodní	k2eAgFnSc2d1	úvodní
bitvy	bitva	k1gFnSc2	bitva
<g/>
,	,	kIx,	,
pláně	pláně	k1gNnSc1	pláně
Gorgorothu	Gorgoroth	k1gInSc2	Gorgoroth
či	či	k8xC	či
další	další	k2eAgFnPc1d1	další
mordorské	mordorský	k2eAgFnPc1d1	mordorská
scény	scéna	k1gFnPc1	scéna
byly	být	k5eAaImAgFnP	být
točeny	točit	k5eAaImNgFnP	točit
na	na	k7c6	na
svazích	svah	k1gInPc6	svah
vulkánu	vulkán	k1gInSc2	vulkán
Ruapehu	Ruapeh	k1gInSc2	Ruapeh
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c4	na
námět	námět	k1gInSc4	námět
Tolkienovy	Tolkienův	k2eAgFnSc2d1	Tolkienova
předlohy	předloha	k1gFnSc2	předloha
je	být	k5eAaImIp3nS	být
dostupná	dostupný	k2eAgFnSc1d1	dostupná
počítačová	počítačový	k2eAgFnSc1d1	počítačová
RPG	RPG	kA	RPG
hra	hra	k1gFnSc1	hra
Middle-earth	Middlearth	k1gMnSc1	Middle-earth
<g/>
:	:	kIx,	:
Shadow	Shadow	k1gMnSc1	Shadow
of	of	k?	of
Mordor	Mordor	k1gMnSc1	Mordor
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
černé	černý	k2eAgFnSc6d1	černá
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
i	i	k8xC	i
tmavá	tmavý	k2eAgFnSc1d1	tmavá
oblast	oblast	k1gFnSc1	oblast
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
severního	severní	k2eAgInSc2d1	severní
pólu	pól	k1gInSc2	pól
měsíce	měsíc	k1gInSc2	měsíc
Charona	Charon	k1gMnSc2	Charon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
TOLKIEN	TOLKIEN	kA	TOLKIEN
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Ronald	Ronald	k1gMnSc1	Ronald
Reuel	Reuel	k1gMnSc1	Reuel
<g/>
.	.	kIx.	.
</s>
<s>
Silmarillion	Silmarillion	k1gInSc1	Silmarillion
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
999	[number]	k4	999
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TOLKIEN	TOLKIEN	kA	TOLKIEN
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Ronald	Ronald	k1gMnSc1	Ronald
Reuel	Reuel	k1gMnSc1	Reuel
<g/>
.	.	kIx.	.
</s>
<s>
Pán	pán	k1gMnSc1	pán
Prstenů	prsten	k1gInPc2	prsten
-	-	kIx~	-
Společenstvo	společenstvo	k1gNnSc1	společenstvo
prstenu	prsten	k1gInSc2	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
362	[number]	k4	362
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TOLKIEN	TOLKIEN	kA	TOLKIEN
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Ronald	Ronald	k1gMnSc1	Ronald
Reuel	Reuel	k1gMnSc1	Reuel
<g/>
.	.	kIx.	.
</s>
<s>
Pán	pán	k1gMnSc1	pán
Prstenů	prsten	k1gInPc2	prsten
-	-	kIx~	-
Dvě	dva	k4xCgFnPc1	dva
věže	věž	k1gFnPc1	věž
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
372	[number]	k4	372
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TOLKIEN	TOLKIEN	kA	TOLKIEN
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Ronald	Ronald	k1gMnSc1	Ronald
Reuel	Reuel	k1gMnSc1	Reuel
<g/>
.	.	kIx.	.
</s>
<s>
Pán	pán	k1gMnSc1	pán
Prstenů	prsten	k1gInPc2	prsten
-	-	kIx~	-
Návrat	návrat	k1gInSc1	návrat
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
373	[number]	k4	373
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TOLKIEN	TOLKIEN	kA	TOLKIEN
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Ronald	Ronald	k1gMnSc1	Ronald
Reuel	Reuel	k1gMnSc1	Reuel
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Nedokončené	dokončený	k2eNgInPc4d1	nedokončený
příběhy	příběh	k1gInPc4	příběh
Númenoru	Númenor	k1gInSc2	Númenor
a	a	k8xC	a
Středozemě	Středozem	k1gFnSc2	Středozem
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Christopher	Christophra	k1gFnPc2	Christophra
Tolkien	Tolkina	k1gFnPc2	Tolkina
<g/>
;	;	kIx,	;
překlad	překlad	k1gInSc1	překlad
Stanislava	Stanislav	k1gMnSc2	Stanislav
Pošustová-Menšíková	Pošustová-Menšíková	k1gFnSc1	Pošustová-Menšíková
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Argo	Argo	k1gMnSc1	Argo
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
257	[number]	k4	257
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
193	[number]	k4	193
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Sauron	Sauron	k1gMnSc1	Sauron
</s>
</p>
<p>
<s>
Skřeti	skřet	k1gMnPc1	skřet
(	(	kIx(	(
<g/>
Středozem	Středozem	k1gFnSc1	Středozem
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Válka	válka	k1gFnSc1	válka
o	o	k7c4	o
Prsten	prsten	k1gInSc4	prsten
</s>
</p>
<p>
<s>
Gondor	Gondor	k1gMnSc1	Gondor
</s>
</p>
<p>
<s>
Skvrna	skvrna	k1gFnSc1	skvrna
Mordor	Mordora	k1gFnPc2	Mordora
</s>
</p>
