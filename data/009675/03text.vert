<p>
<s>
Kostel	kostel	k1gInSc1	kostel
Povýšení	povýšení	k1gNnSc2	povýšení
svatého	svatý	k2eAgInSc2d1	svatý
Kříže	kříž	k1gInSc2	kříž
je	být	k5eAaImIp3nS	být
římskokatolický	římskokatolický	k2eAgInSc4d1	římskokatolický
kostel	kostel	k1gInSc4	kostel
zasvěcený	zasvěcený	k2eAgInSc4d1	zasvěcený
Povýšení	povýšení	k1gNnSc4	povýšení
svatého	svatý	k2eAgMnSc2d1	svatý
Kříže	Kříž	k1gMnSc2	Kříž
v	v	k7c6	v
Údlicích	Údlice	k1gFnPc6	Údlice
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Chomutov	Chomutov	k1gInSc1	Chomutov
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
jako	jako	k8xC	jako
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
současná	současný	k2eAgFnSc1d1	současná
podoba	podoba	k1gFnSc1	podoba
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
úprav	úprava	k1gFnPc2	úprava
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Pozdně	pozdně	k6eAd1	pozdně
románský	románský	k2eAgInSc1d1	románský
kostel	kostel	k1gInSc1	kostel
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
koncem	koncem	k7c2	koncem
první	první	k4xOgFnSc2	první
třetiny	třetina	k1gFnSc2	třetina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
přistavěna	přistavěn	k2eAgFnSc1d1	přistavěna
věž	věž	k1gFnSc1	věž
a	a	k8xC	a
kostel	kostel	k1gInSc1	kostel
byl	být	k5eAaImAgInS	být
opevněn	opevnit	k5eAaPmNgInS	opevnit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
hradbou	hradba	k1gFnSc7	hradba
a	a	k8xC	a
vodním	vodní	k2eAgInSc7d1	vodní
příkopem	příkop	k1gInSc7	příkop
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
doložen	doložit	k5eAaPmNgInS	doložit
na	na	k7c6	na
mapě	mapa	k1gFnSc6	mapa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1723	[number]	k4	1723
a	a	k8xC	a
otisku	otisk	k1gInSc2	otisk
mapy	mapa	k1gFnSc2	mapa
stabilního	stabilní	k2eAgInSc2d1	stabilní
katastru	katastr	k1gInSc2	katastr
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1843	[number]	k4	1843
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1450	[number]	k4	1450
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
Údlicích	Údlice	k1gFnPc6	Údlice
k	k	k7c3	k
vojenské	vojenský	k2eAgFnSc3d1	vojenská
potyčce	potyčka	k1gFnSc3	potyčka
<g/>
,	,	kIx,	,
během	během	k7c2	během
které	který	k3yRgFnSc2	který
byl	být	k5eAaImAgMnS	být
opevněný	opevněný	k2eAgInSc4d1	opevněný
kostel	kostel	k1gInSc4	kostel
dobyt	dobyt	k2eAgInSc4d1	dobyt
žateckým	žatecký	k2eAgInSc7d1	žatecký
a	a	k8xC	a
lounským	lounský	k2eAgNnSc7d1	Lounské
vojskem	vojsko	k1gNnSc7	vojsko
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
tu	tu	k6eAd1	tu
zajato	zajmout	k5eAaPmNgNnS	zajmout
sedmnáct	sedmnáct	k4xCc1	sedmnáct
jezdců	jezdec	k1gInPc2	jezdec
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
z	z	k7c2	z
Lobkovic	Lobkovice	k1gInPc2	Lobkovice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
prošel	projít	k5eAaPmAgMnS	projít
za	za	k7c4	za
Jana	Jan	k1gMnSc4	Jan
Adama	Adam	k1gMnSc4	Adam
Hrzána	Hrzán	k1gMnSc4	Hrzán
z	z	k7c2	z
Harasova	Harasův	k2eAgMnSc2d1	Harasův
raně	raně	k6eAd1	raně
barokní	barokní	k2eAgFnSc7d1	barokní
úpravou	úprava	k1gFnSc7	úprava
<g/>
,	,	kIx,	,
během	během	k7c2	během
které	který	k3yQgFnSc2	který
byla	být	k5eAaImAgFnS	být
prodloužena	prodloužen	k2eAgFnSc1d1	prodloužena
chrámová	chrámový	k2eAgFnSc1d1	chrámová
loď	loď	k1gFnSc1	loď
a	a	k8xC	a
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
straně	strana	k1gFnSc6	strana
přistavěna	přistavěn	k2eAgFnSc1d1	přistavěna
kaple	kaple	k1gFnSc1	kaple
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnPc1d1	poslední
úpravy	úprava	k1gFnPc1	úprava
v	v	k7c6	v
klasicistním	klasicistní	k2eAgInSc6d1	klasicistní
stylu	styl	k1gInSc6	styl
proběhly	proběhnout	k5eAaPmAgInP	proběhnout
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
zjednodušeno	zjednodušen	k2eAgNnSc1d1	zjednodušeno
západní	západní	k2eAgNnSc1d1	západní
průčelí	průčelí	k1gNnSc1	průčelí
<g/>
,	,	kIx,	,
opraven	opraven	k2eAgInSc1d1	opraven
krov	krov	k1gInSc1	krov
a	a	k8xC	a
loď	loď	k1gFnSc1	loď
byla	být	k5eAaImAgFnS	být
zakryta	zakrýt	k5eAaPmNgFnS	zakrýt
plochým	plochý	k2eAgInSc7d1	plochý
stropem	strop	k1gInSc7	strop
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
stojí	stát	k5eAaImIp3nS	stát
volně	volně	k6eAd1	volně
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
loď	loď	k1gFnSc1	loď
je	být	k5eAaImIp3nS	být
orientována	orientovat	k5eAaBmNgFnS	orientovat
k	k	k7c3	k
východu	východ	k1gInSc3	východ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
přistavěna	přistavěn	k2eAgFnSc1d1	přistavěna
gotická	gotický	k2eAgFnSc1d1	gotická
věž	věž	k1gFnSc1	věž
s	s	k7c7	s
křížově	křížově	k6eAd1	křížově
zaklenutou	zaklenutý	k2eAgFnSc7d1	zaklenutá
sakristií	sakristie	k1gFnSc7	sakristie
v	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
<g/>
.	.	kIx.	.
</s>
<s>
Vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
vyšších	vysoký	k2eAgNnPc2d2	vyšší
pater	patro	k1gNnPc2	patro
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
předsíň	předsíň	k1gFnSc1	předsíň
u	u	k7c2	u
západní	západní	k2eAgFnSc2d1	západní
zdi	zeď	k1gFnSc2	zeď
věže	věž	k1gFnSc2	věž
<g/>
.	.	kIx.	.
</s>
<s>
Chrámová	chrámový	k2eAgFnSc1d1	chrámová
loď	loď	k1gFnSc1	loď
je	být	k5eAaImIp3nS	být
obdélná	obdélný	k2eAgFnSc1d1	obdélná
a	a	k8xC	a
plochostropá	plochostropý	k2eAgFnSc1d1	plochostropá
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
opevňovacích	opevňovací	k2eAgFnPc2d1	opevňovací
úprav	úprava	k1gFnPc2	úprava
byla	být	k5eAaImAgFnS	být
zvýšena	zvýšit	k5eAaPmNgFnS	zvýšit
o	o	k7c4	o
vyložené	vyložený	k2eAgNnSc4d1	vyložené
obranné	obranný	k2eAgNnSc4d1	obranné
patro	patro	k1gNnSc4	patro
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
existenci	existence	k1gFnSc4	existence
dokládají	dokládat	k5eAaImIp3nP	dokládat
mohutné	mohutný	k2eAgInPc1d1	mohutný
pískovcové	pískovcový	k2eAgInPc1d1	pískovcový
krakorce	krakorec	k1gInPc1	krakorec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
barokních	barokní	k2eAgFnPc2d1	barokní
úprav	úprava	k1gFnPc2	úprava
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
byla	být	k5eAaImAgFnS	být
vložena	vložen	k2eAgFnSc1d1	vložena
podklenutá	podklenutý	k2eAgFnSc1d1	podklenutá
kruchta	kruchta	k1gFnSc1	kruchta
<g/>
.	.	kIx.	.
</s>
<s>
Pozdně	pozdně	k6eAd1	pozdně
barokní	barokní	k2eAgFnSc1d1	barokní
je	být	k5eAaImIp3nS	být
také	také	k9	také
obdélná	obdélný	k2eAgFnSc1d1	obdélná
kaple	kaple	k1gFnSc1	kaple
s	s	k7c7	s
křížovou	křížový	k2eAgFnSc7d1	křížová
klenbou	klenba	k1gFnSc7	klenba
přistavěná	přistavěný	k2eAgNnPc1d1	přistavěné
k	k	k7c3	k
jižní	jižní	k2eAgFnSc3d1	jižní
straně	strana	k1gFnSc3	strana
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Kaple	kaple	k1gFnSc1	kaple
se	se	k3xPyFc4	se
do	do	k7c2	do
lodi	loď	k1gFnSc2	loď
otevírá	otevírat	k5eAaImIp3nS	otevírat
vítězným	vítězný	k2eAgInSc7d1	vítězný
obloukem	oblouk	k1gInSc7	oblouk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původní	původní	k2eAgInSc1d1	původní
presbytář	presbytář	k1gInSc1	presbytář
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
půlkruhovou	půlkruhový	k2eAgFnSc7d1	půlkruhová
románskou	románský	k2eAgFnSc7d1	románská
apsidou	apsida	k1gFnSc7	apsida
o	o	k7c6	o
vnějším	vnější	k2eAgInSc6d1	vnější
průměru	průměr	k1gInSc6	průměr
8	[number]	k4	8
m	m	kA	m
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
zaklenuta	zaklenout	k5eAaPmNgFnS	zaklenout
románskou	románský	k2eAgFnSc7d1	románská
žebrovou	žebrový	k2eAgFnSc7d1	žebrová
klenbou	klenba	k1gFnSc7	klenba
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgFnSc1d1	vnější
stěna	stěna	k1gFnSc1	stěna
apsidy	apsida	k1gFnSc2	apsida
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
čtyřmi	čtyři	k4xCgFnPc7	čtyři
lizénami	lizéna	k1gFnPc7	lizéna
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vybíhají	vybíhat	k5eAaImIp3nP	vybíhat
do	do	k7c2	do
obloučkového	obloučkový	k2eAgInSc2d1	obloučkový
vlysu	vlys	k1gInSc2	vlys
neseného	nesený	k2eAgInSc2d1	nesený
rozdílně	rozdílně	k6eAd1	rozdílně
zdobenými	zdobený	k2eAgInPc7d1	zdobený
dříky	dřík	k1gInPc7	dřík
s	s	k7c7	s
krychlovými	krychlový	k2eAgFnPc7d1	krychlová
hlavičkami	hlavička	k1gFnPc7	hlavička
<g/>
.	.	kIx.	.
</s>
<s>
Stěnu	stěna	k1gFnSc4	stěna
apsidy	apsida	k1gFnSc2	apsida
ukončuje	ukončovat	k5eAaImIp3nS	ukončovat
podstřešní	podstřešní	k2eAgFnSc1d1	podstřešní
římsa	římsa	k1gFnSc1	římsa
se	s	k7c7	s
zubořezem	zubořez	k1gInSc7	zubořez
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pětici	pětice	k1gFnSc6	pětice
polí	pole	k1gFnPc2	pole
mezi	mezi	k7c7	mezi
lizénami	lizéna	k1gFnPc7	lizéna
jsou	být	k5eAaImIp3nP	být
umístěna	umístěn	k2eAgNnPc4d1	umístěno
oblouková	obloukový	k2eAgNnPc4d1	obloukové
okna	okno	k1gNnPc4	okno
<g/>
.	.	kIx.	.
</s>
<s>
Oknům	okno	k1gNnPc3	okno
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
chybí	chybět	k5eAaImIp3nS	chybět
ostění	ostění	k1gNnSc1	ostění
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
venkovní	venkovní	k2eAgFnSc6d1	venkovní
zdi	zeď	k1gFnSc6	zeď
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
apsidy	apsida	k1gFnSc2	apsida
je	být	k5eAaImIp3nS	být
odhalené	odhalený	k2eAgNnSc1d1	odhalené
pravidelné	pravidelný	k2eAgNnSc1d1	pravidelné
románské	románský	k2eAgNnSc1d1	románské
zdivo	zdivo	k1gNnSc1	zdivo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vybavení	vybavení	k1gNnSc1	vybavení
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgMnSc1d1	hlavní
raně	raně	k6eAd1	raně
barokní	barokní	k2eAgInSc1d1	barokní
oltář	oltář	k1gInSc1	oltář
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1677	[number]	k4	1677
a	a	k8xC	a
pískovcová	pískovcový	k2eAgFnSc1d1	pískovcová
křtitelnice	křtitelnice	k1gFnSc1	křtitelnice
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1725	[number]	k4	1725
<g/>
.	.	kIx.	.
</s>
<s>
Cenná	cenný	k2eAgFnSc1d1	cenná
je	být	k5eAaImIp3nS	být
gotická	gotický	k2eAgFnSc1d1	gotická
socha	socha	k1gFnSc1	socha
Madony	Madona	k1gFnSc2	Madona
z	z	k7c2	z
období	období	k1gNnSc2	období
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1410	[number]	k4	1410
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Okolí	okolí	k1gNnSc1	okolí
kostela	kostel	k1gInSc2	kostel
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
těsném	těsný	k2eAgNnSc6d1	těsné
sousedství	sousedství	k1gNnSc6	sousedství
kostela	kostel	k1gInSc2	kostel
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
několik	několik	k4yIc1	několik
plastik	plastika	k1gFnPc2	plastika
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
sem	sem	k6eAd1	sem
byly	být	k5eAaImAgFnP	být
přemístěny	přemístit	k5eAaPmNgFnP	přemístit
ze	z	k7c2	z
zaniklých	zaniklý	k2eAgNnPc2d1	zaniklé
sídel	sídlo	k1gNnPc2	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
u	u	k7c2	u
kostelní	kostelní	k2eAgFnSc2d1	kostelní
zdi	zeď	k1gFnSc2	zeď
stojí	stát	k5eAaImIp3nS	stát
smírčí	smírčí	k2eAgInSc4d1	smírčí
kříž	kříž	k1gInSc4	kříž
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Nového	Nového	k2eAgNnSc2d1	Nového
Sedla	sedlo	k1gNnSc2	sedlo
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
několik	několik	k4yIc4	několik
metrů	metr	k1gInPc2	metr
dál	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
socha	socha	k1gFnSc1	socha
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
ze	z	k7c2	z
zbořeného	zbořený	k2eAgInSc2d1	zbořený
kostela	kostel	k1gInSc2	kostel
v	v	k7c6	v
Hrušovanech	Hrušovany	k1gInPc6	Hrušovany
a	a	k8xC	a
Pieta	pieta	k1gFnSc1	pieta
od	od	k7c2	od
Karla	Karel	k1gMnSc2	Karel
Waitzmanna	Waitzmann	k1gMnSc2	Waitzmann
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1741	[number]	k4	1741
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
původně	původně	k6eAd1	původně
stála	stát	k5eAaImAgFnS	stát
v	v	k7c6	v
Milžanech	Milžan	k1gInPc6	Milžan
<g/>
.	.	kIx.	.
</s>
<s>
Nejdál	daleko	k6eAd3	daleko
od	od	k7c2	od
kostela	kostel	k1gInSc2	kostel
stojí	stát	k5eAaImIp3nS	stát
mariánský	mariánský	k2eAgInSc1d1	mariánský
sloup	sloup	k1gInSc1	sloup
s	s	k7c7	s
korintskou	korintský	k2eAgFnSc7d1	Korintská
hlavicí	hlavice	k1gFnSc7	hlavice
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1700	[number]	k4	1700
a	a	k8xC	a
dříve	dříve	k6eAd2	dříve
stál	stát	k5eAaImAgInS	stát
v	v	k7c6	v
Miřeticích	Miřetik	k1gMnPc6	Miřetik
u	u	k7c2	u
silnice	silnice	k1gFnSc2	silnice
do	do	k7c2	do
Kadaně	Kadaň	k1gFnSc2	Kadaň
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
kostela	kostel	k1gInSc2	kostel
<g/>
,	,	kIx,	,
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
mezi	mezi	k7c7	mezi
ním	on	k3xPp3gMnSc7	on
a	a	k8xC	a
farou	fara	k1gFnSc7	fara
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nP	stát
sochy	socha	k1gFnPc1	socha
svaté	svatý	k2eAgFnSc2d1	svatá
Barbory	Barbora	k1gFnSc2	Barbora
a	a	k8xC	a
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kostel	kostel	k1gInSc1	kostel
Povýšení	povýšení	k1gNnSc2	povýšení
svatého	svatý	k2eAgInSc2d1	svatý
Kříže	kříž	k1gInSc2	kříž
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
Povýšení	povýšení	k1gNnSc2	povýšení
svatého	svatý	k2eAgInSc2d1	svatý
Kříže	kříž	k1gInSc2	kříž
na	na	k7c4	na
Hrady	hrad	k1gInPc4	hrad
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
