<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
význam	význam	k1gInSc1	význam
velštiny	velština	k1gFnSc2	velština
několik	několik	k4yIc4	několik
desetiletí	desetiletí	k1gNnPc2	desetiletí
upadal	upadat	k5eAaImAgInS	upadat
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
obrození	obrození	k1gNnSc3	obrození
<g/>
,	,	kIx,	,
když	když	k8xS	když
Britský	britský	k2eAgInSc1d1	britský
parlament	parlament	k1gInSc1	parlament
postavil	postavit	k5eAaPmAgInS	postavit
velštinu	velština	k1gFnSc4	velština
na	na	k7c4	na
stejnou	stejný	k2eAgFnSc4d1	stejná
úroveň	úroveň	k1gFnSc4	úroveň
jako	jako	k8xC	jako
angličtinu	angličtina	k1gFnSc4	angličtina
pro	pro	k7c4	pro
používání	používání	k1gNnSc4	používání
v	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
veřejném	veřejný	k2eAgInSc6d1	veřejný
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
