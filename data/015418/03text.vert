<s>
Velká	velký	k2eAgFnSc1d1
francouzská	francouzský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Velká	velký	k2eAgFnSc1d1
francouzská	francouzský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
1792	#num#	k4
až	až	k6eAd1
1815	#num#	k4
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Evropa	Evropa	k1gFnSc1
<g/>
,	,	kIx,
Střední	střední	k2eAgInSc1d1
východ	východ	k1gInSc1
<g/>
,	,	kIx,
Atlantský	atlantský	k2eAgInSc1d1
oceán	oceán	k1gInSc1
<g/>
,	,	kIx,
Indie	Indie	k1gFnSc1
a	a	k8xC
Severní	severní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
</s>
<s>
casus	casus	k1gInSc4
belli	bell	k1gMnPc1
<g/>
:	:	kIx,
</s>
<s>
Vypuknutí	vypuknutí	k1gNnSc1
velké	velký	k2eAgFnSc2d1
francouzské	francouzský	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Porážka	porážka	k1gFnSc1
Francie	Francie	k1gFnSc1
<g/>
;	;	kIx,
Restaurace	restaurace	k1gFnSc1
Bourbonů	bourbon	k1gInPc2
<g/>
;	;	kIx,
Napoleon	Napoleon	k1gMnSc1
na	na	k7c6
Svaté	svatý	k2eAgFnSc6d1
Heleně	Helena	k1gFnSc6
<g/>
;	;	kIx,
Vídeňský	vídeňský	k2eAgInSc1d1
kongres	kongres	k1gInSc1
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
Francie	Francie	k1gFnSc1
se	s	k7c7
spojenci	spojenec	k1gMnPc7
<g/>
:	:	kIx,
</s>
<s>
Revoluční	revoluční	k2eAgFnSc1d1
a	a	k8xC
napoleonská	napoleonský	k2eAgFnSc1d1
Francie	Francie	k1gFnSc1
</s>
<s>
sesterské	sesterský	k2eAgFnPc1d1
republiky	republika	k1gFnPc1
a	a	k8xC
klientelistické	klientelistický	k2eAgInPc1d1
státy	stát	k1gInPc1
Francie	Francie	k1gFnSc2
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
Varšavské	varšavský	k2eAgNnSc1d1
knížectví	knížectví	k1gNnSc1
</s>
<s>
polské	polský	k2eAgFnPc4d1
legie	legie	k1gFnPc4
</s>
<s>
Batávská	batávský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Holandsko	Holandsko	k1gNnSc1
</s>
<s>
Neapolsko	Neapolsko	k6eAd1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Bavorsko	Bavorsko	k1gNnSc1
</s>
<s>
Etrurie	Etrurie	k1gFnSc1
</s>
<s>
Lucca-Piombino	Lucca-Piombin	k2eAgNnSc1d1
</s>
<s>
Septinsulární	Septinsulární	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
Rýnský	rýnský	k2eAgInSc1d1
spolek	spolek	k1gInSc1
</s>
<s>
Dánsko-Norsko	Dánsko-Norsko	k6eAd1
</s>
<s>
Norsko	Norsko	k1gNnSc1
(	(	kIx(
<g/>
1814	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
Osmanská	osmanský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
Osmanská	osmanský	k2eAgFnSc1d1
říše	říš	k1gFnSc2
(	(	kIx(
<g/>
1806	#num#	k4
<g/>
–	–	k?
<g/>
1812	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Maráthská	Maráthský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
(	(	kIx(
<g/>
1803	#num#	k4
<g/>
–	–	k?
<g/>
1808	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Prusko	Prusko	k1gNnSc1
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
(	(	kIx(
<g/>
1809	#num#	k4
<g/>
–	–	k?
<g/>
1813	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Persie	Persie	k1gFnSc1
</s>
<s>
Fantská	Fantský	k2eAgFnSc1d1
konfederace	konfederace	k1gFnSc1
</s>
<s>
protivníci	protivník	k1gMnPc1
Francie	Francie	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
Svatá	svatý	k2eAgFnSc1d1
říše	říše	k1gFnSc1
římská	římský	k2eAgFnSc1d1
(	(	kIx(
<g/>
do	do	k7c2
roku	rok	k1gInSc2
1806	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Prusko	Prusko	k1gNnSc1
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
francouzští	francouzský	k2eAgMnPc1d1
roajalisté	roajalista	k1gMnPc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Sicílie	Sicílie	k1gFnSc1
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
</s>
<s>
Sardinie	Sardinie	k1gFnSc1
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Hannoversko	Hannoversko	k6eAd1
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
</s>
<s>
Osmanská	osmanský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
Osmanská	osmanský	k2eAgFnSc1d1
říše	říš	k1gFnSc2
(	(	kIx(
<g/>
1798	#num#	k4
<g/>
–	–	k?
<g/>
1801	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Uhersko	Uhersko	k1gNnSc1
</s>
<s>
Papežský	papežský	k2eAgInSc1d1
stát	stát	k1gInSc1
</s>
<s>
Bavorsko	Bavorsko	k1gNnSc1
</s>
<s>
Württembersko	Württembersko	k1gNnSc1
</s>
<s>
Sasko	Sasko	k1gNnSc1
</s>
<s>
Toskánsko	Toskánsko	k1gNnSc1
</s>
<s>
Persie	Persie	k1gFnSc1
</s>
<s>
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1
</s>
<s>
Maltézský	maltézský	k2eAgInSc1d1
řád	řád	k1gInSc1
</s>
<s>
Francouzské	francouzský	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Ašantská	Ašantský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
</s>
<s>
Imerina	Imerina	k1gFnSc1
</s>
<s>
haitští	haitský	k2eAgMnPc1d1
povstalci	povstalec	k1gMnPc1
(	(	kIx(
<g/>
1803	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Tecumsehova	Tecumsehův	k2eAgFnSc1d1
unie	unie	k1gFnSc1
</s>
<s>
velitelé	velitel	k1gMnPc1
</s>
<s>
Napoleon	Napoleon	k1gMnSc1
Bonapartemaršál	Bonapartemaršál	k1gMnSc1
Neymaršál	Neymaršál	k1gMnSc1
MuratLouis-Nicolas	MuratLouis-Nicolas	k1gMnSc1
DavoutJean-Victor	DavoutJean-Victor	k1gMnSc1
MoreauKarel	MoreauKarel	k1gMnSc1
Francois	Francois	k1gMnSc1
DumoriezJean	DumoriezJean	k1gMnSc1
LannesJosef	LannesJosef	k1gMnSc1
BonaparteEvžen	BonaparteEvžna	k1gFnPc2
de	de	k?
BeauharnaisJózef	BeauharnaisJózef	k1gMnSc1
Antoni	Anton	k1gMnPc1
PoniatowskiFridrich	PoniatowskiFridrich	k1gMnSc1
August	August	k1gMnSc1
I.	I.	kA
<g/>
James	James	k1gMnSc1
MadisonAndrew	MadisonAndrew	k1gMnSc1
Jackson	Jackson	k1gMnSc1
</s>
<s>
Alexandr	Alexandr	k1gMnSc1
I.	I.	kA
<g/>
maršál	maršál	k1gMnSc1
Suvorovmaršál	Suvorovmaršál	k1gMnSc1
KutuzovFrantišek	KutuzovFrantišek	k1gMnSc1
I.	I.	kA
<g/>
Arcivévoda	arcivévoda	k1gMnSc1
KarelKarel	KarelKarel	k1gMnSc1
Filip	Filip	k1gMnSc1
SchwarzenbergArcivévoda	SchwarzenbergArcivévoda	k1gMnSc1
JanGebhart	JanGebhart	k1gInSc4
von	von	k1gInSc1
BlücherKarle	BlücherKarle	k1gFnSc1
Vilém	Vilém	k1gMnSc1
vévoda	vévoda	k1gMnSc1
BrunšvickýArthur	BrunšvickýArthur	k1gMnSc1
WellesleyHoratio	WellesleyHoratio	k1gMnSc1
NelsonFrancisco	NelsonFrancisco	k1gMnSc1
Javier	Javier	k1gMnSc1
Castañ	Castañ	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
AdolfKarel	AdolfKarel	k1gInSc1
XIV	XIV	kA
<g/>
.	.	kIx.
<g/>
Ludvík	Ludvík	k1gMnSc1
VIII	VIII	kA
<g/>
.	.	kIx.
</s>
<s>
síla	síla	k1gFnSc1
</s>
<s>
Revoluční	revoluční	k2eAgFnPc1d1
války	válka	k1gFnPc1
<g/>
:	:	kIx,
neznámáNapoleonské	známáNapoleonský	k2eNgFnPc1d1
války	válka	k1gFnPc1
<g/>
:	:	kIx,
1	#num#	k4
000	#num#	k4
000	#num#	k4
</s>
<s>
Revoluční	revoluční	k2eAgFnPc1d1
války	válka	k1gFnPc1
<g/>
:	:	kIx,
neznámáNapoleonské	známáNapoleonský	k2eNgFnPc1d1
války	válka	k1gFnPc1
<g/>
:	:	kIx,
1	#num#	k4
500	#num#	k4
000	#num#	k4
</s>
<s>
ztráty	ztráta	k1gFnPc1
</s>
<s>
Revoluční	revoluční	k2eAgFnPc1d1
války	válka	k1gFnPc1
<g/>
:	:	kIx,
méně	málo	k6eAd2
než	než	k8xS
1	#num#	k4
100	#num#	k4
000	#num#	k4
vojákůNapoleonské	vojákůNapoleonský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
:	:	kIx,
371	#num#	k4
000	#num#	k4
padlo	padnout	k5eAaPmAgNnS,k5eAaImAgNnS
v	v	k7c6
boji	boj	k1gInSc6
<g/>
400	#num#	k4
000	#num#	k4
zemřelo	zemřít	k5eAaPmAgNnS
v	v	k7c6
důsledku	důsledek	k1gInSc6
nemocí	nemoc	k1gFnPc2
<g/>
1	#num#	k4
000	#num#	k4
000	#num#	k4
celkové	celkový	k2eAgFnSc2d1
ztráty	ztráta	k1gFnSc2
</s>
<s>
Revoluční	revoluční	k2eAgFnPc1d1
války	válka	k1gFnPc1
<g/>
:	:	kIx,
méně	málo	k6eAd2
než	než	k8xS
1	#num#	k4
100	#num#	k4
000	#num#	k4
vojákůNapoleonské	vojákůNapoleonský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
:	:	kIx,
400	#num#	k4
000	#num#	k4
Rusů	Rus	k1gMnPc2
<g/>
300	#num#	k4
000	#num#	k4
Rakušanů	Rakušan	k1gMnPc2
<g/>
300	#num#	k4
000	#num#	k4
Španělů	Španěl	k1gMnPc2
<g/>
200	#num#	k4
000	#num#	k4
Prusů	Prus	k1gMnPc2
<g/>
311	#num#	k4
806	#num#	k4
Britů	Brit	k1gMnPc2
</s>
<s>
Velká	velký	k2eAgFnSc1d1
francouzská	francouzský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
je	být	k5eAaImIp3nS
označení	označení	k1gNnSc4
používané	používaný	k2eAgNnSc4d1
pro	pro	k7c4
sérii	série	k1gFnSc4
konfliktů	konflikt	k1gInPc2
v	v	k7c6
letech	léto	k1gNnPc6
1792	#num#	k4
až	až	k9
1815	#num#	k4
<g/>
,	,	kIx,
známých	známý	k1gMnPc2
jako	jako	k8xC,k8xS
francouzské	francouzský	k2eAgFnSc2d1
revoluční	revoluční	k2eAgFnSc2d1
a	a	k8xC
napoleonské	napoleonský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bojovala	bojovat	k5eAaImAgFnS
v	v	k7c6
ní	on	k3xPp3gFnSc6
Francie	Francie	k1gFnSc1
proti	proti	k7c3
většině	většina	k1gFnSc3
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Průběh	průběh	k1gInSc1
</s>
<s>
Roku	rok	k1gInSc2
1789	#num#	k4
začala	začít	k5eAaPmAgFnS
Velká	velký	k2eAgFnSc1d1
francouzská	francouzský	k2eAgFnSc1d1
revoluce	revoluce	k1gFnSc1
a	a	k8xC
roku	rok	k1gInSc2
1792	#num#	k4
začala	začít	k5eAaPmAgFnS
válka	válka	k1gFnSc1
první	první	k4xOgFnSc2
koalice	koalice	k1gFnSc2
jako	jako	k9
odpověď	odpověď	k1gFnSc4
na	na	k7c4
popravu	poprava	k1gFnSc4
francouzského	francouzský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Ludvíka	Ludvík	k1gMnSc2
XVI	XVI	kA
<g/>
.	.	kIx.
a	a	k8xC
jeho	jeho	k3xOp3gFnPc1
manželky	manželka	k1gFnPc1
<g/>
,	,	kIx,
královny	královna	k1gFnPc1
Marie	Maria	k1gFnSc2
Antoinetty	Antoinetta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
začaly	začít	k5eAaPmAgFnP
revoluční	revoluční	k2eAgFnPc1d1
války	válka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
koalice	koalice	k1gFnSc1
se	se	k3xPyFc4
rozpadla	rozpadnout	k5eAaPmAgFnS
roku	rok	k1gInSc2
1797	#num#	k4
po	po	k7c6
italském	italský	k2eAgNnSc6d1
tažení	tažení	k1gNnSc6
vedeném	vedený	k2eAgNnSc6d1
generálem	generál	k1gMnSc7
Bonapartem	bonapart	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brzy	brzy	k6eAd1
poté	poté	k6eAd1
zahájil	zahájit	k5eAaPmAgMnS
Bonaparte	bonapart	k1gInSc5
tažení	tažení	k1gNnSc6
do	do	k7c2
Egypta	Egypt	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
nepovedlo	povést	k5eNaPmAgNnS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
dosáhnout	dosáhnout	k5eAaPmF
rozhodného	rozhodný	k2eAgNnSc2d1
vítězství	vítězství	k1gNnSc2
a	a	k8xC
nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
musel	muset	k5eAaImAgMnS
vrátit	vrátit	k5eAaPmF
<g/>
,	,	kIx,
protože	protože	k8xS
v	v	k7c6
Evropě	Evropa	k1gFnSc6
mezitím	mezitím	k6eAd1
vznikla	vzniknout	k5eAaPmAgFnS
druhá	druhý	k4xOgFnSc1
koalice	koalice	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
pod	pod	k7c7
velením	velení	k1gNnSc7
ruského	ruský	k2eAgMnSc2d1
maršála	maršál	k1gMnSc2
Suvorova	Suvorův	k2eAgInSc2d1
porážela	porážet	k5eAaImAgNnP
revoluční	revoluční	k2eAgNnPc1d1
vojska	vojsko	k1gNnPc1
v	v	k7c6
Itálii	Itálie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Napoleon	napoleon	k1gInSc1
14	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1800	#num#	k4
porazil	porazit	k5eAaPmAgMnS
koalici	koalice	k1gFnSc4
u	u	k7c2
Marenga	marengo	k1gNnSc2
a	a	k8xC
postupoval	postupovat	k5eAaImAgMnS
do	do	k7c2
Rakouska	Rakousko	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
raději	rád	k6eAd2
uzavřelo	uzavřít	k5eAaPmAgNnS
mír	mír	k1gInSc4
<g/>
;	;	kIx,
ten	ten	k3xDgMnSc1
nakonec	nakonec	k6eAd1
uzavřely	uzavřít	k5eAaPmAgFnP
i	i	k8xC
ostatní	ostatní	k2eAgMnPc1d1
členové	člen	k1gMnPc1
koalice	koalice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1803	#num#	k4
vpadl	vpadnout	k5eAaPmAgMnS
Napoleon	Napoleon	k1gMnSc1
do	do	k7c2
Hanoverska	Hanoversk	k1gInSc2
a	a	k8xC
tím	ten	k3xDgNnSc7
zahájil	zahájit	k5eAaPmAgMnS
napoleonské	napoleonský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
korunovaci	korunovace	k1gFnSc6
císařem	císař	k1gMnSc7
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1804	#num#	k4
<g/>
)	)	kIx)
vznikla	vzniknout	k5eAaPmAgFnS
třetí	třetí	k4xOgFnSc1
koalice	koalice	k1gFnSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
se	se	k3xPyFc4
však	však	k9
po	po	k7c6
bitvách	bitva	k1gFnPc6
u	u	k7c2
Ulmu	Ulmus	k1gInSc2
a	a	k8xC
Slavkova	Slavkov	k1gInSc2
rozpadla	rozpadnout	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napoleon	napoleon	k1gInSc1
poté	poté	k6eAd1
zvítězil	zvítězit	k5eAaPmAgInS
nad	nad	k7c7
čtvrtou	čtvrtý	k4xOgFnSc7
koalicí	koalice	k1gFnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
válka	válka	k1gFnSc1
ve	v	k7c6
Španělsku	Španělsko	k1gNnSc6
skončila	skončit	k5eAaPmAgFnS
neúspěchem	neúspěch	k1gInSc7
a	a	k8xC
ve	v	k7c6
válce	válka	k1gFnSc6
páté	pátý	k4xOgFnSc2
koalice	koalice	k1gFnSc2
prohrál	prohrát	k5eAaPmAgInS
bitvu	bitva	k1gFnSc4
u	u	k7c2
Aspern	Asperna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Aspern	Asperna	k1gFnPc2
Napoleon	napoleon	k1gInSc1
nakonec	nakonec	k6eAd1
zvítězil	zvítězit	k5eAaPmAgInS
u	u	k7c2
Wagramu	Wagram	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
to	ten	k3xDgNnSc1
již	již	k6eAd1
bylo	být	k5eAaImAgNnS
jasné	jasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
není	být	k5eNaImIp3nS
neporazitelný	porazitelný	k2eNgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Tažení	tažení	k1gNnSc1
do	do	k7c2
Ruska	Rusko	k1gNnSc2
roku	rok	k1gInSc2
1812	#num#	k4
skončilo	skončit	k5eAaPmAgNnS
zničením	zničení	k1gNnSc7
jeho	jeho	k3xOp3gFnSc2
půlmilionové	půlmilionový	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
a	a	k8xC
následným	následný	k2eAgInSc7d1
vznikem	vznik	k1gInSc7
šesté	šestý	k4xOgFnSc2
koalice	koalice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
mu	on	k3xPp3gMnSc3
uštědřila	uštědřit	k5eAaPmAgFnS
zdrcující	zdrcující	k2eAgFnSc4d1
porážku	porážka	k1gFnSc4
u	u	k7c2
Lipska	Lipsko	k1gNnSc2
a	a	k8xC
30	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1814	#num#	k4
obsadila	obsadit	k5eAaPmAgFnS
Paříž	Paříž	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napoleon	napoleon	k1gInSc1
musel	muset	k5eAaImAgInS
abdikovat	abdikovat	k5eAaBmF
a	a	k8xC
odejít	odejít	k5eAaPmF
do	do	k7c2
exilu	exil	k1gInSc2
na	na	k7c6
ostrově	ostrov	k1gInSc6
Elba	Elb	k1gInSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1815	#num#	k4
z	z	k7c2
Elby	Elba	k1gFnSc2
utekl	utéct	k5eAaPmAgInS
a	a	k8xC
později	pozdě	k6eAd2
se	se	k3xPyFc4
vylodil	vylodit	k5eAaPmAgMnS
v	v	k7c6
zátoce	zátoka	k1gFnSc6
San	San	k1gMnSc1
Juan	Juan	k1gMnSc1
u	u	k7c2
Cannes	Cannes	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francouzský	francouzský	k2eAgMnSc1d1
král	král	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
XVIII	XVIII	kA
<g/>
.	.	kIx.
musel	muset	k5eAaImAgMnS
prchnout	prchnout	k5eAaPmF
<g/>
,	,	kIx,
ale	ale	k8xC
Napoleon	Napoleon	k1gMnSc1
zůstal	zůstat	k5eAaPmAgMnS
císařem	císař	k1gMnSc7
jen	jen	k6eAd1
sto	sto	k4xCgNnSc1
dní	den	k1gInPc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
18	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1815	#num#	k4
mu	on	k3xPp3gMnSc3
nově	nově	k6eAd1
vzniklá	vzniklý	k2eAgFnSc1d1
sedmá	sedmý	k4xOgFnSc1
koalice	koalice	k1gFnSc1
uštědřila	uštědřit	k5eAaPmAgFnS
zdrcující	zdrcující	k2eAgFnSc4d1
porážku	porážka	k1gFnSc4
u	u	k7c2
Waterloo	Waterloo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napoleon	napoleon	k1gInSc1
nyní	nyní	k6eAd1
byl	být	k5eAaImAgInS
deportován	deportovat	k5eAaBmNgInS
na	na	k7c4
ostrov	ostrov	k1gInSc4
Svatá	svatý	k2eAgFnSc1d1
Helena	Helena	k1gFnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
roku	rok	k1gInSc2
1821	#num#	k4
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poválečné	poválečný	k2eAgNnSc1d1
uspořádání	uspořádání	k1gNnSc1
Evropy	Evropa	k1gFnSc2
bylo	být	k5eAaImAgNnS
dohodnuto	dohodnout	k5eAaPmNgNnS
na	na	k7c6
Vídeňském	vídeňský	k2eAgInSc6d1
kongresu	kongres	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
francouzských	francouzský	k2eAgFnPc6d1
revolučních	revoluční	k2eAgFnPc6d1
válkých	válká	k1gFnPc6
zahynulo	zahynout	k5eAaPmAgNnS
1	#num#	k4
100	#num#	k4
000	#num#	k4
vojáků	voják	k1gMnPc2
a	a	k8xC
v	v	k7c6
napoleonských	napoleonský	k2eAgInPc6d1
3	#num#	k4
350	#num#	k4
000	#num#	k4
až	až	k9
6	#num#	k4
500	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šlo	jít	k5eAaImAgNnS
až	až	k6eAd1
do	do	k7c2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
o	o	k7c4
největší	veliký	k2eAgInSc4d3
ozbrojený	ozbrojený	k2eAgInSc4d1
konflikt	konflikt	k1gInSc4
historie	historie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bojovalo	bojovat	k5eAaImAgNnS
se	se	k3xPyFc4
v	v	k7c6
severní	severní	k2eAgFnSc6d1
a	a	k8xC
jižní	jižní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
<g/>
,	,	kIx,
jižní	jižní	k2eAgFnSc6d1
a	a	k8xC
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
,	,	kIx,
Karibské	karibský	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
,	,	kIx,
na	na	k7c6
Blízkém	blízký	k2eAgInSc6d1
východě	východ	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
Indii	Indie	k1gFnSc6
a	a	k8xC
většině	většina	k1gFnSc6
Atlantského	atlantský	k2eAgMnSc2d1
a	a	k8xC
Indické	indický	k2eAgNnSc1d1
oceánu	oceán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Kolektiv	kolektiv	k1gInSc1
autorů	autor	k1gMnPc2
<g/>
:	:	kIx,
Války	válka	k1gFnPc1
a	a	k8xC
válečníci	válečník	k1gMnPc1
</s>
<s>
Lev	Lev	k1gMnSc1
Tolstoj	Tolstoj	k1gMnSc1
<g/>
:	:	kIx,
Vojna	vojna	k1gFnSc1
a	a	k8xC
mír	mír	k1gInSc1
</s>
<s>
P.	P.	kA
A.	A.	kA
Žilin	Žilina	k1gFnPc2
<g/>
:	:	kIx,
Kutuzov	Kutuzov	k1gInSc1
</s>
<s>
Josef	Josef	k1gMnSc1
Hotmar	Hotmar	k1gMnSc1
<g/>
:	:	kIx,
Dobrodružství	dobrodružství	k1gNnSc1
Velké	velký	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
1789	#num#	k4
<g/>
–	–	k?
<g/>
1799	#num#	k4
</s>
<s>
Geofrey	Geofrey	k1gInPc1
Regan	Regana	k1gFnPc2
<g/>
:	:	kIx,
Guinnesova	Guinnesův	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
válečných	válečný	k2eAgInPc2d1
omylů	omyl	k1gInPc2
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Moderní	moderní	k2eAgFnPc1d1
dějiny	dějiny	k1gFnPc1
</s>
<s>
Velká	velký	k2eAgFnSc1d1
francouzská	francouzský	k2eAgFnSc1d1
revoluce	revoluce	k1gFnSc1
</s>
<s>
Napoleon	Napoleon	k1gMnSc1
Bonaparte	bonapart	k1gInSc5
</s>
<s>
Vídeňský	vídeňský	k2eAgInSc1d1
kongres	kongres	k1gInSc1
</s>
<s>
Francouzské	francouzský	k2eAgFnPc1d1
revoluční	revoluční	k2eAgFnPc1d1
války	válka	k1gFnPc1
</s>
<s>
Napoleonské	napoleonský	k2eAgFnPc1d1
války	válka	k1gFnPc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Bitvy	bitva	k1gFnPc1
napoleonských	napoleonský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Wertingen	Wertingen	k1gInSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Elchingenu	Elchingen	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Ulmu	Ulmus	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Trafalgaru	Trafalgar	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Caldiera	Caldiero	k1gNnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Amstettenu	Amstetten	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Dürnsteinu	Dürnstein	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Schöngrabernu	Schöngraberna	k1gFnSc4
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Slavkova	Slavkov	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Štoků	štok	k1gInPc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Maidy	Maida	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Saalfeldu	Saalfeld	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Jeny	jen	k1gInPc7
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Auerstedtu	Auerstedt	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Prenzlau	Prenzlaus	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Pultuska	Pultuska	k1gFnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Jílového	jílový	k2eAgInSc2d1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Heilsbergu	Heilsberg	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Friedlandu	Friedland	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Medina	Medina	k1gFnSc1
del	del	k?
Rio	Rio	k1gMnSc1
Seco	Seco	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Bailénu	Bailén	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Somosierry	Somosierra	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
La	la	k1gNnSc2
Coruñ	Coruñ	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Tarragony	Tarragona	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
u	u	k7c2
Eggmühlu	Eggmühl	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Aspern	Asperna	k1gFnPc2
a	a	k8xC
Esslingu	Essling	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Wagramu	Wagram	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Znojma	Znojmo	k1gNnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Ciudad	Ciudad	k1gInSc4
Rodrigo	Rodrigo	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Salamancy	Salamanca	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Ostrovna	Ostrovna	k1gFnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Smolensk	Smolensk	k1gInSc4
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Borodina	Borodin	k2eAgInSc2d1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Tarutina	Tarutin	k2eAgInSc2d1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Malojaroslavce	Malojaroslavec	k1gMnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Krasného	Krasný	k2eAgInSc2d1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Druhá	druhý	k4xOgFnSc1
bitva	bitva	k1gFnSc1
u	u	k7c2
Polocku	Polock	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
na	na	k7c6
Berezině	Berezina	k1gFnSc6
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Möckern	Möckern	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Lützenu	Lützen	k2eAgFnSc4d1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Budyšína	Budyšín	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Vitorie	Vitorie	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Grossbeeren	Grossbeerna	k1gFnPc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Drážďan	Drážďany	k1gInPc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Chlumce	Chlumec	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
na	na	k7c6
Kačavě	Kačava	k1gFnSc6
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Dennewitz	Dennewitz	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Wartenburgu	Wartenburg	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
u	u	k7c2
Lipska	Lipsko	k1gNnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Hanau	Hanaus	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Brienne	Brienn	k1gInSc5
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
La	la	k1gNnSc2
Rothiè	Rothiè	k1gInSc5
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Champaubertu	Champaubert	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Montmirailu	Montmirail	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Chateau-Thierry	Chateau-Thierra	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Montereau	Montereaus	k1gInSc2
•	•	k?
Obležení	obležení	k1gNnPc2
Soissons	Soissons	k1gInSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Craonne	Craonn	k1gInSc5
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Laonu	Laon	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Remeše	Remeš	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Arcis-sur-Aube	Arcis-sur-Aub	k1gInSc5
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Paříž	Paříž	k1gFnSc4
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Tolentina	Tolentin	k2eAgInSc2d1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
San	San	k1gFnSc2
Germana	German	k1gMnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Ligny	Ligna	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Quatre-Bras	Quatre-Bras	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Waterloo	Waterloo	k1gNnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Válka	válka	k1gFnSc1
</s>
