<s>
Velká	velký	k2eAgFnSc1d1
francouzská	francouzský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
je	být	k5eAaImIp3nS
označení	označení	k1gNnSc4
používané	používaný	k2eAgNnSc4d1
pro	pro	k7c4
sérii	série	k1gFnSc4
konfliktů	konflikt	k1gInPc2
v	v	k7c6
letech	léto	k1gNnPc6
1792	#num#	k4
až	až	k9
1815	#num#	k4
<g/>
,	,	kIx,
známých	známý	k1gMnPc2
jako	jako	k8xC,k8xS
francouzské	francouzský	k2eAgFnSc2d1
revoluční	revoluční	k2eAgFnSc2d1
a	a	k8xC
napoleonské	napoleonský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>