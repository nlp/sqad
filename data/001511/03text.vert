<s>
Karl	Karl	k1gMnSc1	Karl
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Braun	Braun	k1gMnSc1	Braun
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1850	[number]	k4	1850
<g/>
,	,	kIx,	,
Fulda	Fulda	k1gFnSc1	Fulda
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
-	-	kIx~	-
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
fyzik	fyzik	k1gMnSc1	fyzik
a	a	k8xC	a
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
Braun	Braun	k1gMnSc1	Braun
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Marburgu	Marburg	k1gInSc6	Marburg
a	a	k8xC	a
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1872	[number]	k4	1872
<g/>
;	;	kIx,	;
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
ředitelem	ředitel	k1gMnSc7	ředitel
Fyzikálního	fyzikální	k2eAgInSc2d1	fyzikální
ústavu	ústav	k1gInSc2	ústav
a	a	k8xC	a
profesorem	profesor	k1gMnSc7	profesor
fyziky	fyzika	k1gFnSc2	fyzika
ve	v	k7c6	v
Štrasburku	Štrasburk	k1gInSc6	Štrasburk
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
výzkumy	výzkum	k1gInPc1	výzkum
se	se	k3xPyFc4	se
týkaly	týkat	k5eAaImAgInP	týkat
oscilace	oscilace	k1gFnPc4	oscilace
strun	struna	k1gFnPc2	struna
a	a	k8xC	a
elastických	elastický	k2eAgNnPc2d1	elastické
lan	lano	k1gNnPc2	lano
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
vedení	vedení	k1gNnSc1	vedení
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
v	v	k7c6	v
elektrolytech	elektrolyt	k1gInPc6	elektrolyt
a	a	k8xC	a
mezi	mezi	k7c7	mezi
jinými	jiný	k2eAgFnPc7d1	jiná
i	i	k8xC	i
mechanické	mechanický	k2eAgFnPc1d1	mechanická
teorie	teorie	k1gFnPc1	teorie
tepla	teplo	k1gNnSc2	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
jeho	jeho	k3xOp3gInPc4	jeho
příspěvky	příspěvek	k1gInPc4	příspěvek
do	do	k7c2	do
fondu	fond	k1gInSc2	fond
vědy	věda	k1gFnSc2	věda
bylo	být	k5eAaImAgNnS	být
vypracování	vypracování	k1gNnSc1	vypracování
teoretických	teoretický	k2eAgInPc2d1	teoretický
základů	základ	k1gInPc2	základ
Le	Le	k1gFnSc2	Le
Chatelierova	Chatelierův	k2eAgInSc2d1	Chatelierův
principu	princip	k1gInSc2	princip
pohyblivé	pohyblivý	k2eAgFnSc2d1	pohyblivá
chemické	chemický	k2eAgFnSc2d1	chemická
rovnováhy	rovnováha	k1gFnSc2	rovnováha
<g/>
,	,	kIx,	,
objev	objev	k1gInSc1	objev
usměrňovacího	usměrňovací	k2eAgInSc2d1	usměrňovací
účinku	účinek	k1gInSc2	účinek
polovodičů	polovodič	k1gInPc2	polovodič
(	(	kIx(	(
<g/>
1874	[number]	k4	1874
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
konstrukce	konstrukce	k1gFnSc1	konstrukce
Braunovy	Braunův	k2eAgFnSc2d1	Braunova
trubice	trubice	k1gFnSc2	trubice
a	a	k8xC	a
oscilografu	oscilograf	k1gInSc2	oscilograf
na	na	k7c6	na
principu	princip	k1gInSc6	princip
katodových	katodový	k2eAgInPc2d1	katodový
paprsků	paprsek	k1gInPc2	paprsek
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
bezdrátové	bezdrátový	k2eAgFnSc3d1	bezdrátová
telegrafii	telegrafie	k1gFnSc3	telegrafie
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
přineslo	přinést	k5eAaPmAgNnS	přinést
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
-	-	kIx~	-
druhou	druhý	k4xOgFnSc4	druhý
polovinu	polovina	k1gFnSc4	polovina
získal	získat	k5eAaPmAgMnS	získat
Guglielmo	Guglielma	k1gFnSc5	Guglielma
Marconi	Marcon	k1gMnPc5	Marcon
<g/>
.	.	kIx.	.
</s>
<s>
Lubomír	Lubomír	k1gMnSc1	Lubomír
Sodomka	Sodomka	k1gFnSc1	Sodomka
<g/>
,	,	kIx,	,
Magdalena	Magdalena	k1gFnSc1	Magdalena
Sodomková	Sodomková	k1gFnSc1	Sodomková
<g/>
:	:	kIx,	:
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
,	,	kIx,	,
Set	set	k1gInSc4	set
out	out	k?	out
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-902058-5-2	[number]	k4	80-902058-5-2
Rádio	rádio	k1gNnSc1	rádio
Guglielmo	Guglielma	k1gFnSc5	Guglielma
Marconi	Marcon	k1gMnPc1	Marcon
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Karl	Karla	k1gFnPc2	Karla
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Braun	Braun	k1gMnSc1	Braun
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
