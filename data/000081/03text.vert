<s>
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
,	,	kIx,	,
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
Valpurga	Valpurga	k1gFnSc1	Valpurga
Amálie	Amálie	k1gFnSc1	Amálie
Kristýna	Kristýna	k1gFnSc1	Kristýna
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Maria	Mario	k1gMnSc4	Mario
Theresia	Theresius	k1gMnSc4	Theresius
Walburga	Walburg	k1gMnSc4	Walburg
Amalia	Amalius	k1gMnSc4	Amalius
Christiana	Christian	k1gMnSc4	Christian
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1717	[number]	k4	1717
Vídeň	Vídeň	k1gFnSc1	Vídeň
–	–	k?	–
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1780	[number]	k4	1780
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
rodu	rod	k1gInSc6	rod
Habsburků	Habsburk	k1gMnPc2	Habsburk
byla	být	k5eAaImAgFnS	být
arcivévodkyní	arcivévodkyně	k1gFnSc7	arcivévodkyně
rakouskou	rakouský	k2eAgFnSc7d1	rakouská
<g/>
,	,	kIx,	,
královnou	královna	k1gFnSc7	královna
uherskou	uherský	k2eAgFnSc7d1	uherská
(	(	kIx(	(
<g/>
1741	[number]	k4	1741
<g/>
–	–	k?	–
<g/>
1780	[number]	k4	1780
<g/>
)	)	kIx)	)
a	a	k8xC	a
českou	český	k2eAgFnSc7d1	Česká
(	(	kIx(	(
<g/>
1743	[number]	k4	1743
<g/>
–	–	k?	–
<g/>
1780	[number]	k4	1780
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
markraběnkou	markraběnka	k1gFnSc7	markraběnka
moravskou	moravský	k2eAgFnSc7d1	Moravská
atd.	atd.	kA	atd.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
vládnoucí	vládnoucí	k2eAgFnSc7d1	vládnoucí
ženou	žena	k1gFnSc7	žena
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
trůně	trůn	k1gInSc6	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
její	její	k3xOp3gFnSc2	její
vlády	vláda	k1gFnSc2	vláda
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
mnohým	mnohý	k2eAgFnPc3d1	mnohá
reformám	reforma	k1gFnPc3	reforma
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
školství	školství	k1gNnSc2	školství
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
označována	označovat	k5eAaImNgFnS	označovat
také	také	k9	také
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
císařovna	císařovna	k1gFnSc1	císařovna
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k8xS	jako
žena	žena	k1gFnSc1	žena
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nemohla	moct	k5eNaImAgFnS	moct
stát	stát	k5eAaImF	stát
vládnoucí	vládnoucí	k2eAgFnSc7d1	vládnoucí
císařovnou	císařovna	k1gFnSc7	císařovna
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
<g/>
.	.	kIx.	.
</s>
<s>
Císařem	Císař	k1gMnSc7	Císař
byl	být	k5eAaImAgInS	být
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1745	[number]	k4	1745
zvolen	zvolit	k5eAaPmNgMnS	zvolit
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
František	František	k1gMnSc1	František
I.	I.	kA	I.
Štěpán	Štěpán	k1gMnSc1	Štěpán
Lotrinský	lotrinský	k2eAgMnSc1d1	lotrinský
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
císařskou	císařský	k2eAgFnSc7d1	císařská
manželkou	manželka	k1gFnSc7	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
nezůstala	zůstat	k5eNaPmAgFnS	zůstat
jen	jen	k9	jen
dcerou	dcera	k1gFnSc7	dcera
císaře	císař	k1gMnSc2	císař
a	a	k8xC	a
císařovnou	císařovna	k1gFnSc7	císařovna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
i	i	k9	i
matkou	matka	k1gFnSc7	matka
dvou	dva	k4xCgMnPc2	dva
císařů	císař	k1gMnPc2	císař
<g/>
,	,	kIx,	,
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
Leopolda	Leopolda	k1gFnSc1	Leopolda
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
byla	být	k5eAaImAgFnS	být
pokřtěna	pokřtít	k5eAaPmNgFnS	pokřtít
hned	hned	k6eAd1	hned
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
dnu	den	k1gInSc6	den
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
nejstarší	starý	k2eAgFnSc7d3	nejstarší
dcerou	dcera	k1gFnSc7	dcera
císaře	císař	k1gMnSc2	císař
Karla	Karel	k1gMnSc2	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
a	a	k8xC	a
Alžběty	Alžběta	k1gFnSc2	Alžběta
Kristýny	Kristýna	k1gFnPc4	Kristýna
Brunšvické	brunšvický	k2eAgFnPc4d1	Brunšvická
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
jediný	jediný	k2eAgMnSc1d1	jediný
bratr	bratr	k1gMnSc1	bratr
Leopold	Leopold	k1gMnSc1	Leopold
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
necelých	celý	k2eNgInPc2d1	necelý
sedmi	sedm	k4xCc2	sedm
měsíců	měsíc	k1gInPc2	měsíc
(	(	kIx(	(
<g/>
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
jejím	její	k3xOp3gNnSc7	její
narozením	narození	k1gNnSc7	narození
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1730	[number]	k4	1730
zemřela	zemřít	k5eAaPmAgFnS	zemřít
i	i	k9	i
její	její	k3xOp3gFnSc1	její
nejmladší	mladý	k2eAgFnSc1d3	nejmladší
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
šestiletá	šestiletý	k2eAgFnSc1d1	šestiletá
<g/>
,	,	kIx,	,
sestra	sestra	k1gFnSc1	sestra
Marie	Marie	k1gFnSc1	Marie
Amálie	Amálie	k1gFnSc1	Amálie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
pragmatické	pragmatický	k2eAgFnSc2d1	pragmatická
sankce	sankce	k1gFnSc2	sankce
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1713	[number]	k4	1713
byla	být	k5eAaImAgFnS	být
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
de	de	k?	de
facto	fact	k2eAgNnSc1d1	facto
předpokládanou	předpokládaný	k2eAgFnSc7d1	předpokládaná
dědičkou	dědička	k1gFnSc7	dědička
habsburských	habsburský	k2eAgFnPc2d1	habsburská
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
rakouského	rakouský	k2eAgNnSc2d1	rakouské
dědictví	dědictví	k1gNnSc2	dědictví
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
mládí	mládí	k1gNnSc2	mládí
ji	on	k3xPp3gFnSc4	on
vychovávali	vychovávat	k5eAaImAgMnP	vychovávat
jezuité	jezuita	k1gMnPc1	jezuita
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
hlavního	hlavní	k2eAgInSc2d1	hlavní
předmětu	předmět	k1gInSc2	předmět
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
bylo	být	k5eAaImAgNnS	být
náboženství	náboženství	k1gNnSc1	náboženství
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
učila	učít	k5eAaPmAgFnS	učít
dějinám	dějiny	k1gFnPc3	dějiny
<g/>
,	,	kIx,	,
latině	latina	k1gFnSc3	latina
<g/>
,	,	kIx,	,
francouzštině	francouzština	k1gFnSc6	francouzština
a	a	k8xC	a
němčině	němčina	k1gFnSc6	němčina
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
ještě	ještě	k9	ještě
například	například	k6eAd1	například
kreslení	kreslení	k1gNnSc4	kreslení
<g/>
,	,	kIx,	,
tanci	tanec	k1gInSc3	tanec
a	a	k8xC	a
hudbě	hudba	k1gFnSc3	hudba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmi	sedm	k4xCc6	sedm
letech	léto	k1gNnPc6	léto
tančila	tančit	k5eAaImAgFnS	tančit
společně	společně	k6eAd1	společně
se	s	k7c7	s
sestrou	sestra	k1gFnSc7	sestra
Marií	Maria	k1gFnSc7	Maria
Annou	Anna	k1gFnSc7	Anna
v	v	k7c6	v
opeře	opera	k1gFnSc6	opera
Euristeo	Euristeo	k1gNnSc1	Euristeo
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
autorem	autor	k1gMnSc7	autor
byl	být	k5eAaImAgMnS	být
Zenón	Zenón	k1gMnSc1	Zenón
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
ve	v	k7c6	v
13	[number]	k4	13
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
v	v	k7c4	v
den	den	k1gInSc4	den
otcových	otcův	k2eAgFnPc2d1	otcova
jmenin	jmeniny	k1gFnPc2	jmeniny
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
4	[number]	k4	4
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1730	[number]	k4	1730
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
zpívala	zpívat	k5eAaImAgFnS	zpívat
před	před	k7c7	před
publikem	publikum	k1gNnSc7	publikum
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
Germania	germanium	k1gNnSc2	germanium
il	il	k?	il
di	di	k?	di
che	che	k0	che
spende	spend	k1gInSc5	spend
Sagro	Sagro	k1gNnSc4	Sagro
all	all	k?	all
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
si	se	k3xPyFc3	se
užila	užít	k5eAaPmAgFnS	užít
i	i	k8xC	i
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
hereckou	herecký	k2eAgFnSc4d1	herecká
premiéru	premiéra	k1gFnSc4	premiéra
v	v	k7c6	v
komedii	komedie	k1gFnSc6	komedie
Il	Il	k1gMnSc1	Il
cicisbeo	cicisbeo	k1gMnSc1	cicisbeo
consolato	consolato	k6eAd1	consolato
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1728	[number]	k4	1728
se	se	k3xPyFc4	se
její	její	k3xOp3gFnSc7	její
vychovatelkou	vychovatelka	k1gFnSc7	vychovatelka
stala	stát	k5eAaPmAgFnS	stát
říšská	říšský	k2eAgFnSc1d1	říšská
hraběnka	hraběnka	k1gFnSc1	hraběnka
Maria	Maria	k1gFnSc1	Maria
Karolina	Karolinum	k1gNnPc1	Karolinum
von	von	k1gInSc1	von
Fuchs-Mollard	Fuchs-Mollard	k1gInSc1	Fuchs-Mollard
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgFnSc1d1	zvaná
"	"	kIx"	"
<g/>
Charlotte	Charlott	k1gMnSc5	Charlott
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
zůstala	zůstat	k5eAaPmAgFnS	zůstat
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1754	[number]	k4	1754
<g/>
.	.	kIx.	.
</s>
<s>
Charlottu	Charlotta	k1gFnSc4	Charlotta
si	se	k3xPyFc3	se
velmi	velmi	k6eAd1	velmi
oblíbila	oblíbit	k5eAaPmAgFnS	oblíbit
<g/>
,	,	kIx,	,
říkávala	říkávat	k5eAaImAgFnS	říkávat
jí	on	k3xPp3gFnSc2	on
"	"	kIx"	"
<g/>
mami	mami	k1gFnSc2	mami
<g/>
"	"	kIx"	"
a	a	k8xC	a
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
ji	on	k3xPp3gFnSc4	on
jako	jako	k8xC	jako
jediného	jediný	k2eAgMnSc4d1	jediný
nečlena	nečlen	k1gMnSc4	nečlen
habsburského	habsburský	k2eAgInSc2d1	habsburský
rodu	rod	k1gInSc2	rod
nechala	nechat	k5eAaPmAgFnS	nechat
pochovat	pochovat	k5eAaPmF	pochovat
do	do	k7c2	do
rodinné	rodinný	k2eAgFnSc2d1	rodinná
Císařské	císařský	k2eAgFnSc2d1	císařská
hrobky	hrobka	k1gFnSc2	hrobka
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
leží	ležet	k5eAaImIp3nS	ležet
hned	hned	k6eAd1	hned
vedle	vedle	k7c2	vedle
ní	on	k3xPp3gFnSc2	on
a	a	k8xC	a
jejího	její	k3xOp3gMnSc2	její
chotě	choť	k1gMnSc2	choť
Františka	František	k1gMnSc2	František
I.	I.	kA	I.
Štěpána	Štěpán	k1gMnSc2	Štěpán
Lotrinského	lotrinský	k2eAgMnSc2d1	lotrinský
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
jí	jíst	k5eAaImIp3nS	jíst
pomohla	pomoct	k5eAaPmAgFnS	pomoct
získat	získat	k5eAaPmF	získat
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Františkem	František	k1gMnSc7	František
Štěpánem	Štěpán	k1gMnSc7	Štěpán
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
setkala	setkat	k5eAaPmAgFnS	setkat
již	již	k6eAd1	již
v	v	k7c6	v
pěti	pět	k4xCc6	pět
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
přišel	přijít	k5eAaPmAgMnS	přijít
z	z	k7c2	z
Nancy	Nancy	k1gFnSc2	Nancy
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
oblíbil	oblíbit	k5eAaPmAgMnS	oblíbit
její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
–	–	k?	–
Karel	Karel	k1gMnSc1	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
viděl	vidět	k5eAaImAgMnS	vidět
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
mu	on	k3xPp3gMnSc3	on
osud	osud	k1gInSc1	osud
odepřel	odepřít	k5eAaPmAgInS	odepřít
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
největším	veliký	k2eAgInSc7d3	veliký
problémem	problém	k1gInSc7	problém
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
vztahu	vztah	k1gInSc2	vztah
mezi	mezi	k7c7	mezi
ní	on	k3xPp3gFnSc7	on
a	a	k8xC	a
Františkem	František	k1gMnSc7	František
Štěpánem	Štěpán	k1gMnSc7	Štěpán
byl	být	k5eAaImAgInS	být
věkový	věkový	k2eAgInSc1d1	věkový
rozdíl	rozdíl	k1gInSc1	rozdíl
<g/>
:	:	kIx,	:
jí	on	k3xPp3gFnSc2	on
bylo	být	k5eAaImAgNnS	být
tehdy	tehdy	k6eAd1	tehdy
šest	šest	k4xCc1	šest
<g/>
,	,	kIx,	,
Františkovi	František	k1gMnSc3	František
Štěpánovi	Štěpán	k1gMnSc3	Štěpán
patnáct	patnáct	k4xCc4	patnáct
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Štěpán	Štěpán	k1gMnSc1	Štěpán
byl	být	k5eAaImAgMnS	být
velký	velký	k2eAgMnSc1d1	velký
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
hrál	hrát	k5eAaImAgMnS	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
oficiálních	oficiální	k2eAgFnPc6d1	oficiální
oslavách	oslava	k1gFnPc6	oslava
<g/>
,	,	kIx,	,
bálech	bál	k1gInPc6	bál
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
rozděloval	rozdělovat	k5eAaImAgInS	rozdělovat
i	i	k9	i
protokol	protokol	k1gInSc1	protokol
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
problémem	problém	k1gInSc7	problém
byl	být	k5eAaImAgInS	být
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
případné	případný	k2eAgNnSc4d1	případné
spojenectví	spojenectví	k1gNnSc4	spojenectví
mezi	mezi	k7c4	mezi
Habsburky	Habsburk	k1gMnPc4	Habsburk
a	a	k8xC	a
Lotrinčany	Lotrinčan	k1gMnPc4	Lotrinčan
by	by	kYmCp3nP	by
ještě	ještě	k6eAd1	ještě
více	hodně	k6eAd2	hodně
popudilo	popudit	k5eAaPmAgNnS	popudit
Francii	Francie	k1gFnSc4	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
již	již	k6eAd1	již
nenarodí	narodit	k5eNaPmIp3nP	narodit
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
také	také	k9	také
později	pozdě	k6eAd2	pozdě
potvrdilo	potvrdit	k5eAaPmAgNnS	potvrdit
<g/>
)	)	kIx)	)
mužský	mužský	k2eAgMnSc1d1	mužský
následník	následník	k1gMnSc1	následník
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
hrál	hrát	k5eAaImAgInS	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
při	při	k7c6	při
úvahách	úvaha	k1gFnPc6	úvaha
Karla	Karel	k1gMnSc2	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
za	za	k7c4	za
koho	kdo	k3yQnSc4	kdo
provdá	provdat	k5eAaPmIp3nS	provdat
svou	svůj	k3xOyFgFnSc4	svůj
nejstarší	starý	k2eAgFnSc4d3	nejstarší
dceru	dcera	k1gFnSc4	dcera
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
existoval	existovat	k5eAaImAgMnS	existovat
mužský	mužský	k2eAgMnSc1d1	mužský
následník	následník	k1gMnSc1	následník
<g/>
,	,	kIx,	,
vše	všechen	k3xTgNnSc1	všechen
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
jednodušší	jednoduchý	k2eAgNnSc1d2	jednodušší
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
přicházely	přicházet	k5eAaImAgInP	přicházet
sňatky	sňatek	k1gInPc1	sňatek
s	s	k7c7	s
wittelsbašským	wittelsbašský	k2eAgMnSc7d1	wittelsbašský
kurfiřtem	kurfiřt	k1gMnSc7	kurfiřt
(	(	kIx(	(
<g/>
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
případě	případ	k1gInSc6	případ
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
k	k	k7c3	k
Rakousku	Rakousko	k1gNnSc6	Rakousko
přidalo	přidat	k5eAaPmAgNnS	přidat
Bavorsko	Bavorsko	k1gNnSc1	Bavorsko
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
ale	ale	k9	ale
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
rozkol	rozkol	k1gInSc4	rozkol
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	s	k7c7	s
španělským	španělský	k2eAgMnSc7d1	španělský
následníkem	následník	k1gMnSc7	následník
trůnu	trůn	k1gInSc2	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
Anglii	Anglie	k1gFnSc4	Anglie
zavázal	zavázat	k5eAaPmAgMnS	zavázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
svou	svůj	k3xOyFgFnSc4	svůj
dceru	dcera	k1gFnSc4	dcera
provdá	provdat	k5eAaPmIp3nS	provdat
pouze	pouze	k6eAd1	pouze
za	za	k7c4	za
prince	princ	k1gMnPc4	princ
s	s	k7c7	s
menší	malý	k2eAgFnSc7d2	menší
rolí	role	k1gFnSc7	role
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zase	zase	k9	zase
nahrávalo	nahrávat	k5eAaImAgNnS	nahrávat
Františkovi	František	k1gMnSc3	František
Štěpánovi	Štěpán	k1gMnSc3	Štěpán
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1729	[number]	k4	1729
navíc	navíc	k6eAd1	navíc
stal	stát	k5eAaPmAgInS	stát
<g/>
,	,	kIx,	,
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
lotrinským	lotrinský	k2eAgMnSc7d1	lotrinský
vévodou	vévoda	k1gMnSc7	vévoda
<g/>
,	,	kIx,	,
čehož	což	k3yRnSc2	což
se	se	k3xPyFc4	se
obávala	obávat	k5eAaImAgFnS	obávat
Francie	Francie	k1gFnSc1	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
usnadnila	usnadnit	k5eAaPmAgFnS	usnadnit
Válka	válka	k1gFnSc1	válka
o	o	k7c4	o
polské	polský	k2eAgNnSc4d1	polské
následnictví	následnictví	k1gNnSc4	následnictví
v	v	k7c6	v
letech	let	k1gInPc6	let
1733	[number]	k4	1733
<g/>
–	–	k?	–
<g/>
1735	[number]	k4	1735
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
bojovalo	bojovat	k5eAaImAgNnS	bojovat
Rakousko	Rakousko	k1gNnSc1	Rakousko
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
proti	proti	k7c3	proti
Francii	Francie	k1gFnSc3	Francie
a	a	k8xC	a
Španělsku	Španělsko	k1gNnSc3	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
nakonec	nakonec	k6eAd1	nakonec
dopadlo	dopadnout	k5eAaPmAgNnS	dopadnout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rakousko	Rakousko	k1gNnSc1	Rakousko
získalo	získat	k5eAaPmAgNnS	získat
Parmu	Parma	k1gFnSc4	Parma
a	a	k8xC	a
Piacenzu	Piacenza	k1gFnSc4	Piacenza
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
Neapol	Neapol	k1gFnSc4	Neapol
a	a	k8xC	a
Sicílii	Sicílie	k1gFnSc4	Sicílie
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Štěpán	Štěpán	k1gMnSc1	Štěpán
ztratil	ztratit	k5eAaPmAgMnS	ztratit
Lotrinsko	Lotrinsko	k1gNnSc4	Lotrinsko
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
ztrátě	ztráta	k1gFnSc3	ztráta
Lotrinska	Lotrinsko	k1gNnSc2	Lotrinsko
a	a	k8xC	a
uznání	uznání	k1gNnSc2	uznání
pragmatické	pragmatický	k2eAgFnSc2d1	pragmatická
sankce	sankce	k1gFnSc2	sankce
Francií	Francie	k1gFnPc2	Francie
již	již	k9	již
nic	nic	k3yNnSc1	nic
nestálo	stát	k5eNaImAgNnS	stát
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
svatbě	svatba	k1gFnSc6	svatba
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
a	a	k8xC	a
Františka	František	k1gMnSc2	František
Štěpána	Štěpán	k1gMnSc2	Štěpán
Lotrinského	lotrinský	k2eAgMnSc2d1	lotrinský
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
udává	udávat	k5eAaImIp3nS	udávat
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc1	Terezie
do	do	k7c2	do
svého	svůj	k3xOyFgMnSc2	svůj
budoucího	budoucí	k2eAgMnSc2d1	budoucí
manžela	manžel	k1gMnSc2	manžel
zamilovaná	zamilovaná	k1gFnSc1	zamilovaná
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
ani	ani	k8xC	ani
odpor	odpor	k1gInSc4	odpor
z	z	k7c2	z
její	její	k3xOp3gFnSc2	její
strany	strana	k1gFnSc2	strana
se	se	k3xPyFc4	se
neočekával	očekávat	k5eNaImAgInS	očekávat
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Štěpán	Štěpán	k1gMnSc1	Štěpán
požádal	požádat	k5eAaPmAgMnS	požádat
Marii	Maria	k1gFnSc4	Maria
Terezii	Terezie	k1gFnSc3	Terezie
o	o	k7c4	o
ruku	ruka	k1gFnSc4	ruka
dne	den	k1gInSc2	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1736	[number]	k4	1736
<g/>
,	,	kIx,	,
tentýž	týž	k3xTgInSc4	týž
den	den	k1gInSc4	den
byli	být	k5eAaImAgMnP	být
ale	ale	k9	ale
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
odloučeni	odloučen	k2eAgMnPc1d1	odloučen
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
zůstala	zůstat	k5eAaPmAgFnS	zůstat
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
a	a	k8xC	a
František	František	k1gMnSc1	František
Štěpán	Štěpán	k1gMnSc1	Štěpán
se	se	k3xPyFc4	se
přesunul	přesunout	k5eAaPmAgMnS	přesunout
do	do	k7c2	do
Bratislavy	Bratislava	k1gFnSc2	Bratislava
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
to	ten	k3xDgNnSc1	ten
vyžadovala	vyžadovat	k5eAaImAgFnS	vyžadovat
etiketa	etiketa	k1gFnSc1	etiketa
<g/>
.	.	kIx.	.
</s>
<s>
Svatba	svatba	k1gFnSc1	svatba
byla	být	k5eAaImAgFnS	být
naplánována	naplánovat	k5eAaBmNgFnS	naplánovat
na	na	k7c4	na
12	[number]	k4	12
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1736	[number]	k4	1736
v	v	k7c6	v
Augustiniánském	augustiniánský	k2eAgInSc6d1	augustiniánský
kostele	kostel	k1gInSc6	kostel
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Svatbu	svatba	k1gFnSc4	svatba
prováděl	provádět	k5eAaImAgMnS	provádět
papežský	papežský	k2eAgMnSc1d1	papežský
nuncius	nuncius	k1gMnSc1	nuncius
Domenico	Domenico	k1gMnSc1	Domenico
Passionei	Passionei	k1gNnPc2	Passionei
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc4	dva
své	svůj	k3xOyFgInPc4	svůj
ano	ano	k9	ano
řekli	říct	k5eAaPmAgMnP	říct
latinsky	latinsky	k6eAd1	latinsky
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
svatba	svatba	k1gFnSc1	svatba
a	a	k8xC	a
svatební	svatební	k2eAgNnSc1d1	svatební
veselení	veselení	k1gNnSc1	veselení
bylo	být	k5eAaImAgNnS	být
ukončeno	ukončit	k5eAaPmNgNnS	ukončit
posledním	poslední	k2eAgInSc7d1	poslední
dnem	den	k1gInSc7	den
masopustu	masopust	k1gInSc2	masopust
–	–	k?	–
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1736	[number]	k4	1736
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
líbánky	líbánky	k1gFnPc4	líbánky
odjeli	odjet	k5eAaPmAgMnP	odjet
do	do	k7c2	do
Mariazell	Mariazella	k1gFnPc2	Mariazella
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
nechali	nechat	k5eAaPmAgMnP	nechat
požehnat	požehnat	k5eAaPmF	požehnat
velkou	velký	k2eAgFnSc4d1	velká
rodinu	rodina	k1gFnSc4	rodina
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
také	také	k9	také
povedlo	povést	k5eAaPmAgNnS	povést
-	-	kIx~	-
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
porodila	porodit	k5eAaPmAgFnS	porodit
16	[number]	k4	16
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1740	[number]	k4	1740
zemřela	zemřít	k5eAaPmAgFnS	zemřít
nejstarší	starý	k2eAgFnSc1d3	nejstarší
dcera	dcera	k1gFnSc1	dcera
Marie	Marie	k1gFnSc1	Marie
Alžběta	Alžběta	k1gFnSc1	Alžběta
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
žádný	žádný	k3yNgInSc1	žádný
ze	z	k7c2	z
tří	tři	k4xCgMnPc2	tři
potomků	potomek	k1gMnPc2	potomek
nebyl	být	k5eNaImAgMnS	být
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1740	[number]	k4	1740
se	se	k3xPyFc4	se
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc1	Terezie
stala	stát	k5eAaPmAgFnS	stát
panovnicí	panovnice	k1gFnSc7	panovnice
nad	nad	k7c7	nad
habsburskými	habsburský	k2eAgFnPc7d1	habsburská
zeměmi	zem	k1gFnPc7	zem
<g/>
,	,	kIx,	,
rakouskou	rakouský	k2eAgFnSc7d1	rakouská
arcivévodkyní	arcivévodkyně	k1gFnSc7	arcivévodkyně
<g/>
,	,	kIx,	,
vévodkyní	vévodkyně	k1gFnSc7	vévodkyně
lotrinskou	lotrinský	k2eAgFnSc7d1	lotrinská
a	a	k8xC	a
vévodkyní	vévodkyně	k1gFnSc7	vévodkyně
toskánskou	toskánský	k2eAgFnSc7d1	Toskánská
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
tituly	titul	k1gInPc1	titul
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
královny	královna	k1gFnSc2	královna
české	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
markraběnky	markraběnka	k1gFnSc2	markraběnka
moravské	moravský	k2eAgFnSc2d1	Moravská
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
musela	muset	k5eAaImAgFnS	muset
postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
nechat	nechat	k5eAaPmF	nechat
nejprve	nejprve	k6eAd1	nejprve
potvrdit	potvrdit	k5eAaPmF	potvrdit
<g/>
,	,	kIx,	,
takových	takový	k3xDgInPc2	takový
titulů	titul	k1gInPc2	titul
bylo	být	k5eAaImAgNnS	být
celkem	celkem	k6eAd1	celkem
20	[number]	k4	20
včetně	včetně	k7c2	včetně
císařovny-manželky	císařovnyanželka	k1gFnSc2	císařovny-manželka
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
také	také	k9	také
neskýtal	skýtat	k5eNaImAgInS	skýtat
mnoho	mnoho	k4c4	mnoho
panovnických	panovnický	k2eAgFnPc2d1	panovnická
jistot	jistota	k1gFnPc2	jistota
<g/>
.	.	kIx.	.
</s>
<s>
Problémy	problém	k1gInPc1	problém
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
měla	mít	k5eAaImAgFnS	mít
nejen	nejen	k6eAd1	nejen
s	s	k7c7	s
okolními	okolní	k2eAgFnPc7d1	okolní
zeměmi	zem	k1gFnPc7	zem
<g/>
,	,	kIx,	,
na	na	k7c4	na
něž	jenž	k3xRgMnPc4	jenž
si	se	k3xPyFc3	se
činila	činit	k5eAaImAgFnS	činit
nárok	nárok	k1gInSc4	nárok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
uvnitř	uvnitř	k7c2	uvnitř
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
nepovažovali	považovat	k5eNaImAgMnP	považovat
za	za	k7c4	za
dobré	dobrý	k2eAgNnSc4d1	dobré
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jim	on	k3xPp3gMnPc3	on
vládla	vládnout	k5eAaImAgFnS	vládnout
žena	žena	k1gFnSc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
následujícího	následující	k2eAgInSc2d1	následující
měsíce	měsíc	k1gInSc2	měsíc
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
uklidnila	uklidnit	k5eAaPmAgFnS	uklidnit
a	a	k8xC	a
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
pátém	pátý	k4xOgInSc6	pátý
měsíci	měsíc	k1gInSc6	měsíc
těhotenství	těhotenství	k1gNnSc2	těhotenství
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
věnovat	věnovat	k5eAaPmF	věnovat
dalším	další	k2eAgFnPc3d1	další
záležitostem	záležitost	k1gFnPc3	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
naléhavým	naléhavý	k2eAgInSc7d1	naléhavý
problémem	problém	k1gInSc7	problém
byl	být	k5eAaImAgInS	být
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
Karel	Karel	k1gMnSc1	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
nezasvětil	zasvětit	k5eNaPmAgInS	zasvětit
svou	svůj	k3xOyFgFnSc4	svůj
dceru	dcera	k1gFnSc4	dcera
do	do	k7c2	do
"	"	kIx"	"
<g/>
věcí	věc	k1gFnPc2	věc
státních	státní	k2eAgFnPc2d1	státní
<g/>
"	"	kIx"	"
a	a	k8xC	a
ona	onen	k3xDgFnSc1	onen
tak	tak	k6eAd1	tak
musela	muset	k5eAaImAgFnS	muset
převzít	převzít	k5eAaPmF	převzít
celou	celý	k2eAgFnSc4d1	celá
vládu	vláda	k1gFnSc4	vláda
po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
otci	otec	k1gMnSc6	otec
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
neměla	mít	k5eNaImAgFnS	mít
znalosti	znalost	k1gFnPc4	znalost
místních	místní	k2eAgInPc2d1	místní
poměrů	poměr	k1gInPc2	poměr
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Války	válka	k1gFnSc2	válka
o	o	k7c4	o
rakouské	rakouský	k2eAgNnSc4d1	rakouské
dědictví	dědictví	k1gNnSc4	dědictví
<g/>
.	.	kIx.	.
</s>
<s>
Uplynulo	uplynout	k5eAaPmAgNnS	uplynout
jen	jen	k9	jen
několik	několik	k4yIc1	několik
dní	den	k1gInPc2	den
vlády	vláda	k1gFnSc2	vláda
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc1	Terezie
a	a	k8xC	a
Prusko	Prusko	k1gNnSc1	Prusko
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Fridrichem	Fridrich	k1gMnSc7	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
žádalo	žádat	k5eAaImAgNnS	žádat
od	od	k7c2	od
Rakouska	Rakousko	k1gNnSc2	Rakousko
celé	celý	k2eAgNnSc4d1	celé
Slezsko	Slezsko	k1gNnSc4	Slezsko
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
milióny	milión	k4xCgInPc4	milión
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
,	,	kIx,	,
za	za	k7c4	za
záruku	záruka	k1gFnSc4	záruka
rakouských	rakouský	k2eAgFnPc2d1	rakouská
držav	država	k1gFnPc2	država
v	v	k7c6	v
Říši	říš	k1gFnSc6	říš
a	a	k8xC	a
za	za	k7c2	za
zasazení	zasazení	k1gNnSc2	zasazení
se	se	k3xPyFc4	se
o	o	k7c4	o
zvolení	zvolení	k1gNnSc4	zvolení
jejího	její	k3xOp3gMnSc2	její
manžela	manžel	k1gMnSc2	manžel
císařem	císař	k1gMnSc7	císař
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
nabídka	nabídka	k1gFnSc1	nabídka
byla	být	k5eAaImAgFnS	být
Marii	Maria	k1gFnSc3	Maria
Terezii	Terezie	k1gFnSc3	Terezie
učiněna	učiněn	k2eAgFnSc1d1	učiněna
dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1740	[number]	k4	1740
<g/>
.	.	kIx.	.
</s>
<s>
Fridrich	Fridrich	k1gMnSc1	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
čekal	čekat	k5eAaImAgInS	čekat
na	na	k7c4	na
odpověď	odpověď	k1gFnSc4	odpověď
měsíc	měsíc	k1gInSc4	měsíc
a	a	k8xC	a
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1740	[number]	k4	1740
<g/>
,	,	kIx,	,
když	když	k8xS	když
nedostal	dostat	k5eNaPmAgMnS	dostat
odpověď	odpověď	k1gFnSc4	odpověď
<g/>
,	,	kIx,	,
obsadila	obsadit	k5eAaPmAgFnS	obsadit
pruská	pruský	k2eAgNnPc1d1	pruské
vojska	vojsko	k1gNnPc4	vojsko
postupně	postupně	k6eAd1	postupně
celé	celý	k2eAgNnSc4d1	celé
Slezsko	Slezsko	k1gNnSc4	Slezsko
a	a	k8xC	a
začaly	začít	k5eAaPmAgFnP	začít
Války	válka	k1gFnPc1	válka
o	o	k7c4	o
rakouské	rakouský	k2eAgNnSc4d1	rakouské
dědictví	dědictví	k1gNnSc4	dědictví
<g/>
.	.	kIx.	.
</s>
<s>
Protiakcí	protiakce	k1gFnPc2	protiakce
bylo	být	k5eAaImAgNnS	být
vyslání	vyslání	k1gNnSc4	vyslání
přibližně	přibližně	k6eAd1	přibližně
16	[number]	k4	16
000	[number]	k4	000
rakouských	rakouský	k2eAgMnPc2d1	rakouský
vojáků	voják	k1gMnPc2	voják
proti	proti	k7c3	proti
dvojnásobné	dvojnásobný	k2eAgFnSc3d1	dvojnásobná
přesile	přesila	k1gFnSc3	přesila
pruských	pruský	k2eAgMnPc2d1	pruský
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
armády	armáda	k1gFnPc1	armáda
se	se	k3xPyFc4	se
střetly	střetnout	k5eAaPmAgFnP	střetnout
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1741	[number]	k4	1741
u	u	k7c2	u
Małujowic	Małujowice	k1gFnPc2	Małujowice
(	(	kIx(	(
<g/>
Mollwitz	Mollwitz	k1gInSc1	Mollwitz
<g/>
)	)	kIx)	)
a	a	k8xC	a
rakouská	rakouský	k2eAgNnPc1d1	rakouské
vojska	vojsko	k1gNnPc1	vojsko
byla	být	k5eAaImAgNnP	být
poražena	porazit	k5eAaPmNgNnP	porazit
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
důvodem	důvod	k1gInSc7	důvod
neúspěchu	neúspěch	k1gInSc2	neúspěch
bylo	být	k5eAaImAgNnS	být
osamocení	osamocení	k1gNnSc1	osamocení
Rakouska	Rakousko	k1gNnSc2	Rakousko
v	v	k7c6	v
boji	boj	k1gInSc6	boj
<g/>
,	,	kIx,	,
nezanedbatelnou	zanedbatelný	k2eNgFnSc4d1	nezanedbatelná
roli	role	k1gFnSc4	role
hrála	hrát	k5eAaImAgFnS	hrát
i	i	k9	i
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
Karel	Karel	k1gMnSc1	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
zanechal	zanechat	k5eAaPmAgMnS	zanechat
své	svůj	k3xOyFgFnSc3	svůj
dceři	dcera	k1gFnSc3	dcera
armádu	armáda	k1gFnSc4	armáda
v	v	k7c6	v
nepříliš	příliš	k6eNd1	příliš
dobrém	dobrý	k2eAgInSc6d1	dobrý
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
spojencem	spojenec	k1gMnSc7	spojenec
Rakouska	Rakousko	k1gNnSc2	Rakousko
pouze	pouze	k6eAd1	pouze
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jej	on	k3xPp3gMnSc4	on
podporovala	podporovat	k5eAaImAgFnS	podporovat
penězi	peníze	k1gInPc7	peníze
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
Rakousko	Rakousko	k1gNnSc1	Rakousko
daleko	daleko	k6eAd1	daleko
více	hodně	k6eAd2	hodně
potřebovalo	potřebovat	k5eAaImAgNnS	potřebovat
posily	posila	k1gFnPc4	posila
vojenské	vojenský	k2eAgFnPc4d1	vojenská
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1741	[number]	k4	1741
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc1	Terezie
porodila	porodit	k5eAaPmAgFnS	porodit
syna	syn	k1gMnSc4	syn
Josefa	Josef	k1gMnSc4	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Neúspěch	neúspěch	k1gInSc1	neúspěch
u	u	k7c2	u
Małujowic	Małujowice	k1gFnPc2	Małujowice
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
pobídkou	pobídka	k1gFnSc7	pobídka
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgInPc4d1	ostatní
nepřátelské	přátelský	k2eNgInPc4d1	nepřátelský
státy	stát	k1gInPc4	stát
–	–	k?	–
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
Bavorsko	Bavorsko	k1gNnSc1	Bavorsko
a	a	k8xC	a
Sasko	Sasko	k1gNnSc1	Sasko
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
spojily	spojit	k5eAaPmAgFnP	spojit
s	s	k7c7	s
Pruskem	Prusko	k1gNnSc7	Prusko
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
učinit	učinit	k5eAaPmF	učinit
z	z	k7c2	z
Rakouska	Rakousko	k1gNnSc2	Rakousko
slabý	slabý	k2eAgInSc4d1	slabý
stát	stát	k1gInSc4	stát
a	a	k8xC	a
získat	získat	k5eAaPmF	získat
na	na	k7c4	na
jeho	on	k3xPp3gInSc4	on
úkor	úkor	k1gInSc4	úkor
co	co	k9	co
nejvíce	hodně	k6eAd3	hodně
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1741	[number]	k4	1741
odcestovala	odcestovat	k5eAaPmAgFnS	odcestovat
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc1	Terezie
do	do	k7c2	do
Prešpurku	Prešpurku	k?	Prešpurku
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zjistila	zjistit	k5eAaPmAgFnS	zjistit
postoj	postoj	k1gInSc4	postoj
Uher	Uhry	k1gFnPc2	Uhry
k	k	k7c3	k
pragmatické	pragmatický	k2eAgFnSc3d1	pragmatická
sankci	sankce	k1gFnSc3	sankce
(	(	kIx(	(
<g/>
která	který	k3yRgFnSc1	který
zajišťovala	zajišťovat	k5eAaImAgFnS	zajišťovat
dědičné	dědičný	k2eAgNnSc4d1	dědičné
právo	právo	k1gNnSc4	právo
pro	pro	k7c4	pro
ženské	ženský	k2eAgMnPc4d1	ženský
následovníky	následovník	k1gMnPc4	následovník
trůnu	trůn	k1gInSc2	trůn
v	v	k7c4	v
Habsburské	habsburský	k2eAgFnPc4d1	habsburská
monarchie	monarchie	k1gFnPc4	monarchie
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
Habsburků	Habsburk	k1gMnPc2	Habsburk
včetně	včetně	k7c2	včetně
Uher	Uhry	k1gFnPc2	Uhry
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
podpoří	podpořit	k5eAaPmIp3nS	podpořit
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Uherští	uherský	k2eAgMnPc1d1	uherský
se	se	k3xPyFc4	se
vyjádřili	vyjádřit	k5eAaPmAgMnP	vyjádřit
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
listiny	listina	k1gFnSc2	listina
<g/>
,	,	kIx,	,
potvrdili	potvrdit	k5eAaPmAgMnP	potvrdit
svůj	svůj	k3xOyFgInSc4	svůj
podpis	podpis	k1gInSc4	podpis
a	a	k8xC	a
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
se	se	k3xPyFc4	se
podpořit	podpořit	k5eAaPmF	podpořit
Rakousko	Rakousko	k1gNnSc4	Rakousko
i	i	k9	i
vojensky	vojensky	k6eAd1	vojensky
<g/>
.	.	kIx.	.
</s>
<s>
Cenou	cena	k1gFnSc7	cena
ovšem	ovšem	k9	ovšem
byla	být	k5eAaImAgFnS	být
větší	veliký	k2eAgFnSc4d2	veliký
samostatnost	samostatnost	k1gFnSc4	samostatnost
pro	pro	k7c4	pro
Uhry	Uher	k1gMnPc4	Uher
<g/>
.	.	kIx.	.
</s>
<s>
Panovnice	panovnice	k1gFnSc1	panovnice
před	před	k7c7	před
říšským	říšský	k2eAgInSc7d1	říšský
sněmem	sněm	k1gInSc7	sněm
Maďarům	Maďar	k1gMnPc3	Maďar
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
předešlá	předešlý	k2eAgNnPc1d1	předešlé
privilegia	privilegium	k1gNnPc4	privilegium
<g/>
,	,	kIx,	,
osvobodila	osvobodit	k5eAaPmAgFnS	osvobodit
šlechtu	šlechta	k1gFnSc4	šlechta
od	od	k7c2	od
daní	daň	k1gFnPc2	daň
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
již	již	k6eAd1	již
slíbil	slíbit	k5eAaPmAgMnS	slíbit
Karel	Karel	k1gMnSc1	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
zemřou	zemřít	k5eAaPmIp3nP	zemřít
potomci	potomek	k1gMnPc1	potomek
Karla	Karel	k1gMnSc2	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Josefa	Josef	k1gMnSc2	Josef
I.	I.	kA	I.
a	a	k8xC	a
Leopolda	Leopolda	k1gFnSc1	Leopolda
I.	I.	kA	I.
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
si	se	k3xPyFc3	se
Uhry	Uhry	k1gFnPc4	Uhry
budou	být	k5eAaImBp3nP	být
moci	moct	k5eAaImF	moct
zvolit	zvolit	k5eAaPmF	zvolit
vlastního	vlastní	k2eAgMnSc4d1	vlastní
krále	král	k1gMnSc4	král
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
vymřeli	vymřít	k5eAaPmAgMnP	vymřít
tito	tento	k3xDgMnPc1	tento
následovníci	následovník	k1gMnPc1	následovník
byla	být	k5eAaImAgFnS	být
malá	malý	k2eAgNnPc1d1	malé
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
měla	mít	k5eAaImAgFnS	mít
už	už	k6eAd1	už
budoucího	budoucí	k2eAgMnSc2d1	budoucí
dědice	dědic	k1gMnSc2	dědic
trůnu	trůn	k1gInSc2	trůn
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
tři	tři	k4xCgFnPc4	tři
dcery	dcera	k1gFnPc4	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
stala	stát	k5eAaPmAgFnS	stát
královnou	královna	k1gFnSc7	královna
uherskou	uherský	k2eAgFnSc7d1	uherská
<g/>
,	,	kIx,	,
korunována	korunovat	k5eAaBmNgFnS	korunovat
byla	být	k5eAaImAgFnS	být
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
roku	rok	k1gInSc2	rok
1741	[number]	k4	1741
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
si	se	k3xPyFc3	se
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
získala	získat	k5eAaPmAgFnS	získat
mnoho	mnoho	k4c4	mnoho
příznivců	příznivec	k1gMnPc2	příznivec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1742	[number]	k4	1742
získala	získat	k5eAaPmAgFnS	získat
první	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
slíbené	slíbený	k2eAgFnSc2d1	slíbená
uherské	uherský	k2eAgFnSc2d1	uherská
vojenské	vojenský	k2eAgFnSc2d1	vojenská
podpory	podpora	k1gFnSc2	podpora
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
armáda	armáda	k1gFnSc1	armáda
se	se	k3xPyFc4	se
zvětšila	zvětšit	k5eAaPmAgFnS	zvětšit
na	na	k7c4	na
přibližně	přibližně	k6eAd1	přibližně
30	[number]	k4	30
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
ve	v	k7c6	v
zbrani	zbraň	k1gFnSc6	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Uhry	Uhry	k1gFnPc1	Uhry
tímto	tento	k3xDgMnSc7	tento
poprvé	poprvé	k6eAd1	poprvé
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
existenci	existence	k1gFnSc6	existence
povolaly	povolat	k5eAaPmAgFnP	povolat
své	svůj	k3xOyFgMnPc4	svůj
vojáky	voják	k1gMnPc4	voják
k	k	k7c3	k
obraně	obrana	k1gFnSc3	obrana
celé	celý	k2eAgFnSc2d1	celá
Rakouské	rakouský	k2eAgFnSc2d1	rakouská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
jen	jen	k9	jen
uherské	uherský	k2eAgFnPc4d1	uherská
části	část	k1gFnPc4	část
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
dosud	dosud	k6eAd1	dosud
<g/>
.	.	kIx.	.
</s>
<s>
Podruhé	podruhé	k6eAd1	podruhé
před	před	k7c4	před
říšský	říšský	k2eAgInSc4d1	říšský
sněm	sněm	k1gInSc4	sněm
předstoupila	předstoupit	k5eAaPmAgFnS	předstoupit
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
v	v	k7c6	v
září	září	k1gNnSc6	září
1741	[number]	k4	1741
(	(	kIx(	(
<g/>
legenda	legenda	k1gFnSc1	legenda
praví	pravit	k5eAaImIp3nS	pravit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
to	ten	k3xDgNnSc4	ten
s	s	k7c7	s
Rakouskem	Rakousko	k1gNnSc7	Rakousko
vypadalo	vypadat	k5eAaImAgNnS	vypadat
špatně	špatně	k6eAd1	špatně
<g/>
;	;	kIx,	;
dle	dle	k7c2	dle
legendy	legenda	k1gFnSc2	legenda
předstoupila	předstoupit	k5eAaPmAgFnS	předstoupit
před	před	k7c4	před
sněm	sněm	k1gInSc4	sněm
v	v	k7c6	v
černých	černý	k2eAgInPc6d1	černý
šatech	šat	k1gInPc6	šat
držíc	držet	k5eAaImSgFnS	držet
na	na	k7c6	na
rukou	ruka	k1gFnPc6	ruka
půlročního	půlroční	k2eAgMnSc2d1	půlroční
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
se	s	k7c7	s
slzami	slza	k1gFnPc7	slza
v	v	k7c6	v
očích	oko	k1gNnPc6	oko
sdělovala	sdělovat	k5eAaImAgFnS	sdělovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
všemi	všecek	k3xTgFnPc7	všecek
opuštěná	opuštěný	k2eAgFnSc1d1	opuštěná
a	a	k8xC	a
že	že	k8xS	že
nyní	nyní	k6eAd1	nyní
má	mít	k5eAaImIp3nS	mít
již	již	k6eAd1	již
pouze	pouze	k6eAd1	pouze
Uhry	Uhry	k1gFnPc1	Uhry
<g/>
.	.	kIx.	.
</s>
<s>
Uherské	uherský	k2eAgNnSc1d1	Uherské
šlechtice	šlechtic	k1gMnSc2	šlechtic
to	ten	k3xDgNnSc1	ten
povzbudilo	povzbudit	k5eAaPmAgNnS	povzbudit
a	a	k8xC	a
ihned	ihned	k6eAd1	ihned
poskytli	poskytnout	k5eAaPmAgMnP	poskytnout
Marii	Maria	k1gFnSc3	Maria
Terezii	Terezie	k1gFnSc4	Terezie
peníze	peníz	k1gInPc4	peníz
a	a	k8xC	a
vojsko	vojsko	k1gNnSc4	vojsko
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
dokonce	dokonce	k9	dokonce
uznali	uznat	k5eAaPmAgMnP	uznat
jejího	její	k3xOp3gMnSc4	její
manžela	manžel	k1gMnSc4	manžel
jako	jako	k8xS	jako
spoluvládce	spoluvládce	k1gMnSc4	spoluvládce
Uher	uher	k1gInSc4	uher
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1741	[number]	k4	1741
překročily	překročit	k5eAaPmAgFnP	překročit
Rýn	Rýn	k1gInSc4	Rýn
<g/>
,	,	kIx,	,
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
Německou	německý	k2eAgFnSc7d1	německá
říší	říš	k1gFnSc7	říš
<g/>
,	,	kIx,	,
francouzské	francouzský	k2eAgInPc1d1	francouzský
oddíly	oddíl	k1gInPc1	oddíl
pod	pod	k7c7	pod
záminkou	záminka	k1gFnSc7	záminka
pomoci	pomoct	k5eAaPmF	pomoct
bavorskému	bavorský	k2eAgMnSc3d1	bavorský
kurfiřtovi	kurfiřt	k1gMnSc3	kurfiřt
Karlu	Karel	k1gMnSc3	Karel
Albrechtovi	Albrecht	k1gMnSc3	Albrecht
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
již	již	k9	již
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1741	[number]	k4	1741
dobyly	dobýt	k5eAaPmAgFnP	dobýt
Linec	Linec	k1gInSc4	Linec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
nechal	nechat	k5eAaPmAgMnS	nechat
Karel	Karel	k1gMnSc1	Karel
Albrecht	Albrecht	k1gMnSc1	Albrecht
odpřisáhnout	odpřisáhnout	k5eAaPmF	odpřisáhnout
věrnost	věrnost	k1gFnSc4	věrnost
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
nerada	nerad	k2eAgFnSc1d1	nerada
<g/>
,	,	kIx,	,
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
s	s	k7c7	s
Pruskem	Prusko	k1gNnSc7	Prusko
dohodu	dohoda	k1gFnSc4	dohoda
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
těžkých	těžký	k2eAgFnPc6d1	těžká
chvílích	chvíle	k1gFnPc6	chvíle
se	se	k3xPyFc4	se
Prusko	Prusko	k1gNnSc1	Prusko
nespojí	spojit	k5eNaPmIp3nS	spojit
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
Albrechtem	Albrecht	k1gMnSc7	Albrecht
a	a	k8xC	a
nezaútočí	zaútočit	k5eNaPmIp3nS	zaútočit
<g/>
;	;	kIx,	;
za	za	k7c4	za
klid	klid	k1gInSc4	klid
zbraní	zbraň	k1gFnPc2	zbraň
se	se	k3xPyFc4	se
vzdala	vzdát	k5eAaPmAgFnS	vzdát
Slezska	Slezsko	k1gNnSc2	Slezsko
až	až	k9	až
po	po	k7c4	po
Kladskou	kladský	k2eAgFnSc4d1	Kladská
Nisu	Nisa	k1gFnSc4	Nisa
<g/>
.	.	kIx.	.
</s>
<s>
Dohoda	dohoda	k1gFnSc1	dohoda
byla	být	k5eAaImAgFnS	být
podepsána	podepsat	k5eAaPmNgFnS	podepsat
v	v	k7c6	v
Klein-Schenellendorfu	Klein-Schenellendorf	k1gInSc6	Klein-Schenellendorf
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1741	[number]	k4	1741
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Francouzi	Francouz	k1gMnPc1	Francouz
<g/>
,	,	kIx,	,
Sasové	Sas	k1gMnPc1	Sas
a	a	k8xC	a
Bavoři	Bavor	k1gMnPc1	Bavor
Prahu	práh	k1gInSc2	práh
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Bavorský	bavorský	k2eAgMnSc1d1	bavorský
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
provolat	provolat	k5eAaPmF	provolat
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jím	on	k3xPp3gInSc7	on
7	[number]	k4	7
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1741	[number]	k4	1741
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1742	[number]	k4	1742
dobylo	dobýt	k5eAaPmAgNnS	dobýt
Rakousko	Rakousko	k1gNnSc1	Rakousko
Bavorské	bavorský	k2eAgNnSc1d1	bavorské
kurfiřtství	kurfiřtství	k1gNnSc1	kurfiřtství
a	a	k8xC	a
Mnichov	Mnichov	k1gInSc1	Mnichov
12	[number]	k4	12
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1742	[number]	k4	1742
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
Karel	Karel	k1gMnSc1	Karel
VII	VII	kA	VII
<g/>
.	.	kIx.	.
nechal	nechat	k5eAaPmAgInS	nechat
korunovat	korunovat	k5eAaBmF	korunovat
římskoněmeckým	římskoněmecký	k2eAgMnSc7d1	římskoněmecký
králem	král	k1gMnSc7	král
a	a	k8xC	a
císařem	císař	k1gMnSc7	císař
a	a	k8xC	a
František	František	k1gMnSc1	František
Štěpán	Štěpán	k1gMnSc1	Štěpán
tak	tak	k6eAd1	tak
ztratil	ztratit	k5eAaPmAgMnS	ztratit
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
chvíli	chvíle	k1gFnSc4	chvíle
šance	šance	k1gFnSc1	šance
na	na	k7c4	na
jeho	on	k3xPp3gInSc4	on
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Fridrich	Fridrich	k1gMnSc1	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Rakousku	Rakousko	k1gNnSc6	Rakousko
začíná	začínat	k5eAaImIp3nS	začínat
konečně	konečně	k6eAd1	konečně
dařit	dařit	k5eAaImF	dařit
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
odstoupit	odstoupit	k5eAaPmF	odstoupit
od	od	k7c2	od
smlouvy	smlouva	k1gFnSc2	smlouva
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
Terezií	Terezie	k1gFnPc2	Terezie
a	a	k8xC	a
dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1742	[number]	k4	1742
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
bitvu	bitva	k1gFnSc4	bitva
u	u	k7c2	u
Čáslavi	Čáslav	k1gFnSc2	Čáslav
a	a	k8xC	a
Chotusic	Chotusice	k1gFnPc2	Chotusice
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
Anglii	Anglie	k1gFnSc3	Anglie
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
Vratislavi	Vratislav	k1gFnSc6	Vratislav
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1742	[number]	k4	1742
podepsán	podepsán	k2eAgInSc1d1	podepsán
předběžný	předběžný	k2eAgInSc1d1	předběžný
mír	mír	k1gInSc1	mír
mezi	mezi	k7c7	mezi
Rakouskem	Rakousko	k1gNnSc7	Rakousko
a	a	k8xC	a
Pruskem	Prusko	k1gNnSc7	Prusko
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1742	[number]	k4	1742
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
podepsána	podepsat	k5eAaPmNgFnS	podepsat
konečná	konečný	k2eAgFnSc1d1	konečná
mírová	mírový	k2eAgFnSc1d1	mírová
dohoda	dohoda	k1gFnSc1	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Prusko	Prusko	k1gNnSc1	Prusko
získalo	získat	k5eAaPmAgNnS	získat
Horní	horní	k2eAgNnSc1d1	horní
i	i	k8xC	i
Dolní	dolní	k2eAgNnSc1d1	dolní
Slezsko	Slezsko	k1gNnSc1	Slezsko
a	a	k8xC	a
hrabství	hrabství	k1gNnSc1	hrabství
kladské	kladský	k2eAgNnSc1d1	Kladské
<g/>
.	.	kIx.	.
</s>
<s>
Fridrich	Fridrich	k1gMnSc1	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
dále	daleko	k6eAd2	daleko
ještě	ještě	k9	ještě
požadoval	požadovat	k5eAaImAgMnS	požadovat
území	území	k1gNnSc4	území
dnešního	dnešní	k2eAgInSc2d1	dnešní
Královéhradeckého	královéhradecký	k2eAgInSc2d1	královéhradecký
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
ovšem	ovšem	k9	ovšem
již	již	k6eAd1	již
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
vydat	vydat	k5eAaPmF	vydat
<g/>
,	,	kIx,	,
i	i	k8xC	i
kdyby	kdyby	kYmCp3nS	kdyby
to	ten	k3xDgNnSc1	ten
mělo	mít	k5eAaImAgNnS	mít
rozpoutat	rozpoutat	k5eAaPmF	rozpoutat
peklo	peklo	k1gNnSc1	peklo
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
řekla	říct	k5eAaPmAgFnS	říct
<g/>
.	.	kIx.	.
</s>
<s>
Rakousku	Rakousko	k1gNnSc6	Rakousko
z	z	k7c2	z
bývalého	bývalý	k2eAgNnSc2d1	bývalé
Slezska	Slezsko	k1gNnSc2	Slezsko
zbývá	zbývat	k5eAaImIp3nS	zbývat
již	již	k6eAd1	již
jen	jen	k9	jen
Těšínsko	Těšínsko	k1gNnSc1	Těšínsko
<g/>
,	,	kIx,	,
Opava	Opava	k1gFnSc1	Opava
a	a	k8xC	a
Krnov	Krnov	k1gInSc1	Krnov
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
berlínskému	berlínský	k2eAgInSc3d1	berlínský
míru	mír	k1gInSc3	mír
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgFnP	moct
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
původně	původně	k6eAd1	původně
začleněny	začlenit	k5eAaPmNgFnP	začlenit
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
proti	proti	k7c3	proti
Prusku	Prusko	k1gNnSc3	Prusko
<g/>
,	,	kIx,	,
soustředit	soustředit	k5eAaPmF	soustředit
na	na	k7c4	na
dobytí	dobytí	k1gNnSc4	dobytí
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mezitím	mezitím	k6eAd1	mezitím
francouzská	francouzský	k2eAgFnSc1d1	francouzská
šlechta	šlechta	k1gFnSc1	šlechta
chovala	chovat	k5eAaImAgFnS	chovat
jako	jako	k9	jako
pravý	pravý	k2eAgMnSc1d1	pravý
diktátor	diktátor	k1gMnSc1	diktátor
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
české	český	k2eAgFnSc2d1	Česká
šlechty	šlechta	k1gFnSc2	šlechta
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
od	od	k7c2	od
Karla	Karel	k1gMnSc2	Karel
VII	VII	kA	VII
<g/>
.	.	kIx.	.
distancovat	distancovat	k5eAaBmF	distancovat
a	a	k8xC	a
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
nesouhlasili	souhlasit	k5eNaImAgMnP	souhlasit
již	již	k6eAd1	již
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
<g/>
,	,	kIx,	,
začali	začít	k5eAaPmAgMnP	začít
odporovat	odporovat	k5eAaImF	odporovat
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
byly	být	k5eAaImAgFnP	být
země	zem	k1gFnPc1	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnPc1d1	Česká
dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1742	[number]	k4	1742
dobyty	dobýt	k5eAaPmNgInP	dobýt
zpět	zpět	k6eAd1	zpět
Rakouskem	Rakousko	k1gNnSc7	Rakousko
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
Karla	Karel	k1gMnSc2	Karel
VII	VII	kA	VII
<g/>
.	.	kIx.	.
byla	být	k5eAaImAgFnS	být
ukončena	ukončit	k5eAaPmNgFnS	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1743	[number]	k4	1743
se	se	k3xPyFc4	se
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
vypravila	vypravit	k5eAaPmAgFnS	vypravit
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nechala	nechat	k5eAaPmAgFnS	nechat
korunovat	korunovat	k5eAaBmF	korunovat
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
dojela	dojet	k5eAaPmAgFnS	dojet
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1743	[number]	k4	1743
a	a	k8xC	a
korunovace	korunovace	k1gFnSc1	korunovace
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
12	[number]	k4	12
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1743	[number]	k4	1743
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těch	ten	k3xDgFnPc6	ten
dobách	doba	k1gFnPc6	doba
se	se	k3xPyFc4	se
již	již	k6eAd1	již
Marii	Maria	k1gFnSc3	Maria
Terezii	Terezie	k1gFnSc4	Terezie
poměrně	poměrně	k6eAd1	poměrně
dařilo	dařit	k5eAaImAgNnS	dařit
<g/>
,	,	kIx,	,
27	[number]	k4	27
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1743	[number]	k4	1743
Rakousko	Rakousko	k1gNnSc1	Rakousko
s	s	k7c7	s
Anglií	Anglie	k1gFnSc7	Anglie
porazilo	porazit	k5eAaPmAgNnS	porazit
Francii	Francie	k1gFnSc4	Francie
v	v	k7c6	v
Bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Dettingenu	Dettingen	k1gInSc2	Dettingen
a	a	k8xC	a
Francouzi	Francouz	k1gMnPc1	Francouz
se	se	k3xPyFc4	se
museli	muset	k5eAaImAgMnP	muset
stáhnout	stáhnout	k5eAaPmF	stáhnout
opět	opět	k6eAd1	opět
za	za	k7c4	za
Rýn	Rýn	k1gInSc4	Rýn
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomuto	tento	k3xDgNnSc3	tento
vítězství	vítězství	k1gNnSc3	vítězství
mohla	moct	k5eAaImAgFnS	moct
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
začít	začít	k5eAaPmF	začít
uvažovat	uvažovat	k5eAaImF	uvažovat
o	o	k7c4	o
znovu	znovu	k6eAd1	znovu
dobytí	dobytí	k1gNnSc4	dobytí
Lotrinska	Lotrinsko	k1gNnSc2	Lotrinsko
a	a	k8xC	a
také	také	k9	také
Štrasburku	Štrasburk	k1gInSc2	Štrasburk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
si	se	k3xPyFc3	se
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
hodně	hodně	k6eAd1	hodně
oblíbila	oblíbit	k5eAaPmAgFnS	oblíbit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
svého	svůj	k3xOyFgNnSc2	svůj
snažení	snažení	k1gNnSc2	snažení
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1743	[number]	k4	1743
ve	v	k7c6	v
Wormsu	Worms	k1gInSc6	Worms
spojenectví	spojenectví	k1gNnSc2	spojenectví
s	s	k7c7	s
Anglií	Anglie	k1gFnSc7	Anglie
a	a	k8xC	a
sardinsko-piemontským	sardinskoiemontský	k2eAgNnSc7d1	sardinsko-piemontský
královstvím	království	k1gNnSc7	království
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1744	[number]	k4	1744
vpadl	vpadnout	k5eAaPmAgMnS	vpadnout
Fridrich	Fridrich	k1gMnSc1	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
podruhé	podruhé	k6eAd1	podruhé
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
porušil	porušit	k5eAaPmAgInS	porušit
mír	mír	k1gInSc1	mír
sjednaný	sjednaný	k2eAgInSc1d1	sjednaný
roku	rok	k1gInSc2	rok
1742	[number]	k4	1742
Anglií	Anglie	k1gFnSc7	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
alianci	aliance	k1gFnSc4	aliance
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1744	[number]	k4	1744
opět	opět	k6eAd1	opět
dobyl	dobýt	k5eAaPmAgInS	dobýt
Prahu	Praha	k1gFnSc4	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
roku	rok	k1gInSc2	rok
1744	[number]	k4	1744
navíc	navíc	k6eAd1	navíc
porodila	porodit	k5eAaPmAgFnS	porodit
sestra	sestra	k1gFnSc1	sestra
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
–	–	k?	–
Marie	Marie	k1gFnSc1	Marie
Anna	Anna	k1gFnSc1	Anna
mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
dítě	dítě	k1gNnSc1	dítě
a	a	k8xC	a
sama	sám	k3xTgFnSc1	sám
dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1744	[number]	k4	1744
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1745	[number]	k4	1745
přivedla	přivést	k5eAaPmAgFnS	přivést
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
na	na	k7c4	na
svět	svět	k1gInSc4	svět
dalšího	další	k2eAgMnSc2d1	další
syna	syn	k1gMnSc2	syn
–	–	k?	–
Karla	Karel	k1gMnSc2	Karel
Josefa	Josef	k1gMnSc2	Josef
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
tuto	tento	k3xDgFnSc4	tento
radostnou	radostný	k2eAgFnSc4d1	radostná
událost	událost	k1gFnSc4	událost
vládla	vládnout	k5eAaImAgFnS	vládnout
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
spíše	spíše	k9	spíše
špatná	špatný	k2eAgFnSc1d1	špatná
nálada	nálada	k1gFnSc1	nálada
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1745	[number]	k4	1745
u	u	k7c2	u
Dobroměře	Dobroměra	k1gFnSc6	Dobroměra
a	a	k8xC	a
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1745	[number]	k4	1745
u	u	k7c2	u
Ždáru	Ždár	k1gInSc2	Ždár
u	u	k7c2	u
Trutnova	Trutnov	k1gInSc2	Trutnov
prohrálo	prohrát	k5eAaPmAgNnS	prohrát
Rakousko	Rakousko	k1gNnSc1	Rakousko
bitvy	bitva	k1gFnSc2	bitva
proti	proti	k7c3	proti
Fridrichu	Fridrich	k1gMnSc3	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Rakousko	Rakousko	k1gNnSc1	Rakousko
společně	společně	k6eAd1	společně
s	s	k7c7	s
Anglií	Anglie	k1gFnSc7	Anglie
prohrálo	prohrát	k5eAaPmAgNnS	prohrát
ještě	ještě	k9	ještě
bitvu	bitva	k1gFnSc4	bitva
proti	proti	k7c3	proti
Francii	Francie	k1gFnSc3	Francie
dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1745	[number]	k4	1745
u	u	k7c2	u
Fontenoy	Fontenoa	k1gFnSc2	Fontenoa
v	v	k7c6	v
Rakouském	rakouský	k2eAgNnSc6d1	rakouské
Nizozemí	Nizozemí	k1gNnSc6	Nizozemí
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
těmto	tento	k3xDgFnPc3	tento
prohraným	prohraný	k2eAgFnPc3d1	prohraná
bitvám	bitva	k1gFnPc3	bitva
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
kvůli	kvůli	k7c3	kvůli
strachu	strach	k1gInSc3	strach
z	z	k7c2	z
útoku	útok	k1gInSc2	útok
na	na	k7c4	na
Čechy	Čech	k1gMnPc4	Čech
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
podepsala	podepsat	k5eAaPmAgFnS	podepsat
další	další	k2eAgInSc4d1	další
mír	mír	k1gInSc4	mír
(	(	kIx(	(
<g/>
už	už	k9	už
třetí	třetí	k4xOgFnSc7	třetí
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
)	)	kIx)	)
s	s	k7c7	s
Fridrichem	Fridrich	k1gMnSc7	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1745	[number]	k4	1745
v	v	k7c6	v
Drážďanech	Drážďany	k1gInPc6	Drážďany
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgNnSc6	který
se	se	k3xPyFc4	se
definitivně	definitivně	k6eAd1	definitivně
zřekla	zřeknout	k5eAaPmAgFnS	zřeknout
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
Fridrich	Fridrich	k1gMnSc1	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
přislíbil	přislíbit	k5eAaPmAgMnS	přislíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
uzná	uznat	k5eAaPmIp3nS	uznat
Františka	František	k1gMnSc4	František
Štěpána	Štěpán	k1gMnSc4	Štěpán
za	za	k7c4	za
římskoněmeckého	římskoněmecký	k2eAgMnSc4d1	římskoněmecký
císaře	císař	k1gMnSc4	císař
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
platil	platit	k5eAaImAgInS	platit
mír	mír	k1gInSc1	mír
s	s	k7c7	s
Pruskem	Prusko	k1gNnSc7	Prusko
<g/>
,	,	kIx,	,
zbývala	zbývat	k5eAaImAgFnS	zbývat
Marii	Maria	k1gFnSc3	Maria
Terezii	Terezie	k1gFnSc3	Terezie
poslední	poslední	k2eAgNnPc4d1	poslední
dvě	dva	k4xCgNnPc4	dva
válčiště	válčiště	k1gNnPc4	válčiště
–	–	k?	–
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
Nizozemí	Nizozemí	k1gNnSc6	Nizozemí
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1746	[number]	k4	1746
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
rakouská	rakouský	k2eAgFnSc1d1	rakouská
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
které	který	k3yQgFnSc2	který
velel	velet	k5eAaImAgMnS	velet
Josef	Josef	k1gMnSc1	Josef
Václav	Václav	k1gMnSc1	Václav
z	z	k7c2	z
Lichtenštejna	Lichtenštejn	k1gInSc2	Lichtenštejn
<g/>
,	,	kIx,	,
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
s	s	k7c7	s
francouzsko-španělským	francouzsko-španělský	k2eAgNnSc7d1	francouzsko-španělské
vojskem	vojsko	k1gNnSc7	vojsko
u	u	k7c2	u
Piacenzy	Piacenza	k1gFnSc2	Piacenza
a	a	k8xC	a
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
si	se	k3xPyFc3	se
tak	tak	k9	tak
mohla	moct	k5eAaImAgFnS	moct
připsat	připsat	k5eAaPmF	připsat
další	další	k2eAgInSc4d1	další
vojenský	vojenský	k2eAgInSc4d1	vojenský
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1744	[number]	k4	1744
vydala	vydat	k5eAaPmAgFnS	vydat
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc1	Terezie
dekret	dekret	k1gInSc4	dekret
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
vypověděla	vypovědět	k5eAaPmAgNnP	vypovědět
Židy	Žid	k1gMnPc4	Žid
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
i	i	k9	i
z	z	k7c2	z
Moravy	Morava	k1gFnSc2	Morava
i	i	k8xC	i
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pod	pod	k7c7	pod
záminkou	záminka	k1gFnSc7	záminka
údajné	údajný	k2eAgFnSc2d1	údajná
spolupráce	spolupráce	k1gFnSc2	spolupráce
židovské	židovský	k2eAgFnSc2d1	židovská
komunity	komunita	k1gFnSc2	komunita
s	s	k7c7	s
nepřítelem	nepřítel	k1gMnSc7	nepřítel
během	během	k7c2	během
pruské	pruský	k2eAgFnSc2d1	pruská
okupace	okupace	k1gFnSc2	okupace
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Odchod	odchod	k1gInSc1	odchod
Židů	Žid	k1gMnPc2	Žid
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
znamenal	znamenat	k5eAaImAgInS	znamenat
pro	pro	k7c4	pro
město	město	k1gNnSc4	město
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
pokles	pokles	k1gInSc1	pokles
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
od	od	k7c2	od
dalšího	další	k2eAgNnSc2d1	další
vyhánění	vyhánění	k1gNnSc2	vyhánění
upuštěno	upustit	k5eAaPmNgNnS	upustit
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1748	[number]	k4	1748
byl	být	k5eAaImAgMnS	být
Židům	Žid	k1gMnPc3	Žid
povolen	povolen	k2eAgInSc4d1	povolen
návrat	návrat	k1gInSc4	návrat
za	za	k7c4	za
tzv.	tzv.	kA	tzv.
toleranční	toleranční	k2eAgFnSc4d1	toleranční
daň	daň	k1gFnSc4	daň
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1758	[number]	k4	1758
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
přikázala	přikázat	k5eAaPmAgFnS	přikázat
Židům	Žid	k1gMnPc3	Žid
nosit	nosit	k5eAaImF	nosit
zvláštní	zvláštní	k2eAgFnPc4d1	zvláštní
označení	označení	k1gNnPc4	označení
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
diskriminační	diskriminační	k2eAgFnSc1d1	diskriminační
povinnost	povinnost	k1gFnSc1	povinnost
byla	být	k5eAaImAgFnS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
jejím	její	k3xOp3gMnSc7	její
synem	syn	k1gMnSc7	syn
Josefem	Josef	k1gMnSc7	Josef
roku	rok	k1gInSc2	rok
1781	[number]	k4	1781
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1746	[number]	k4	1746
se	se	k3xPyFc4	se
k	k	k7c3	k
Rakousku	Rakousko	k1gNnSc3	Rakousko
a	a	k8xC	a
Anglii	Anglie	k1gFnSc3	Anglie
přidalo	přidat	k5eAaPmAgNnS	přidat
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
o	o	k7c4	o
něco	něco	k3yInSc4	něco
dříve	dříve	k6eAd2	dříve
získalo	získat	k5eAaPmAgNnS	získat
vliv	vliv	k1gInSc4	vliv
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
vládla	vládnout	k5eAaImAgFnS	vládnout
Alžběta	Alžběta	k1gFnSc1	Alžběta
I.	I.	kA	I.
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
na	na	k7c4	na
ruský	ruský	k2eAgInSc4d1	ruský
trůn	trůn	k1gInSc4	trůn
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
než	než	k8xS	než
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
měla	mít	k5eAaImAgFnS	mít
společnou	společný	k2eAgFnSc4d1	společná
nechuť	nechuť	k1gFnSc4	nechuť
k	k	k7c3	k
Prusku	Prusko	k1gNnSc3	Prusko
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
k	k	k7c3	k
jeho	jeho	k3xOp3gMnSc3	jeho
panovníku	panovník	k1gMnSc3	panovník
–	–	k?	–
Fridrichu	Fridrich	k1gMnSc3	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
vzájemné	vzájemný	k2eAgFnSc2d1	vzájemná
vojenské	vojenský	k2eAgFnSc2d1	vojenská
podpory	podpora	k1gFnSc2	podpora
spolu	spolu	k6eAd1	spolu
tyto	tento	k3xDgFnPc4	tento
dvě	dva	k4xCgFnPc4	dva
evropské	evropský	k2eAgFnPc4d1	Evropská
vládkyně	vládkyně	k1gFnPc4	vládkyně
sepsaly	sepsat	k5eAaPmAgFnP	sepsat
dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1746	[number]	k4	1746
dohodu	dohoda	k1gFnSc4	dohoda
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
si	se	k3xPyFc3	se
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc1	Terezie
v	v	k7c6	v
případě	případ	k1gInSc6	případ
napadení	napadení	k1gNnSc2	napadení
Pruskem	Prusko	k1gNnSc7	Prusko
dělá	dělat	k5eAaImIp3nS	dělat
opětovný	opětovný	k2eAgInSc1d1	opětovný
nárok	nárok	k1gInSc1	nárok
na	na	k7c4	na
Slezsko	Slezsko	k1gNnSc4	Slezsko
<g/>
,	,	kIx,	,
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
vzdala	vzdát	k5eAaPmAgFnS	vzdát
kvůli	kvůli	k7c3	kvůli
míru	mír	k1gInSc3	mír
ze	z	k7c2	z
dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1745	[number]	k4	1745
<g/>
.	.	kIx.	.
</s>
<s>
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
,	,	kIx,	,
znechucena	znechucen	k2eAgFnSc1d1	znechucena
nekonečnými	konečný	k2eNgFnPc7d1	nekonečná
válkami	válka	k1gFnPc7	válka
a	a	k8xC	a
bitvami	bitva	k1gFnPc7	bitva
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
<g/>
,	,	kIx,	,
tlačila	tlačit	k5eAaImAgFnS	tlačit
na	na	k7c4	na
Marii	Maria	k1gFnSc4	Maria
Terezii	Terezie	k1gFnSc4	Terezie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
souhlasila	souhlasit	k5eAaImAgFnS	souhlasit
s	s	k7c7	s
mírem	mír	k1gInSc7	mír
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
Anglie	Anglie	k1gFnSc1	Anglie
pomohla	pomoct	k5eAaPmAgFnS	pomoct
dojednat	dojednat	k5eAaPmF	dojednat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
odpověď	odpověď	k1gFnSc4	odpověď
měla	mít	k5eAaImAgFnS	mít
dostatek	dostatek	k1gInSc4	dostatek
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
bylo	být	k5eAaImAgNnS	být
tehdy	tehdy	k6eAd1	tehdy
rakouskou	rakouský	k2eAgFnSc7d1	rakouská
tradicí	tradice	k1gFnSc7	tradice
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc1	Terezie
začínala	začínat	k5eAaImAgFnS	začínat
vše	všechen	k3xTgNnSc1	všechen
zdržovat	zdržovat	k5eAaImF	zdržovat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
Anglii	Anglie	k1gFnSc4	Anglie
došla	dojít	k5eAaPmAgFnS	dojít
trpělivost	trpělivost	k1gFnSc1	trpělivost
a	a	k8xC	a
Anglie	Anglie	k1gFnSc1	Anglie
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
se	se	k3xPyFc4	se
domluvily	domluvit	k5eAaPmAgFnP	domluvit
předběžně	předběžně	k6eAd1	předběžně
o	o	k7c6	o
míru	mír	k1gInSc6	mír
a	a	k8xC	a
Marii	Maria	k1gFnSc3	Maria
Terezii	Terezie	k1gFnSc3	Terezie
postavily	postavit	k5eAaPmAgInP	postavit
dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1748	[number]	k4	1748
před	před	k7c4	před
hotovou	hotový	k2eAgFnSc4d1	hotová
věc	věc	k1gFnSc4	věc
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
byla	být	k5eAaImAgFnS	být
donucena	donucen	k2eAgFnSc1d1	donucena
k	k	k7c3	k
souhlasu	souhlas	k1gInSc3	souhlas
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
předběžným	předběžný	k2eAgNnSc7d1	předběžné
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
s	s	k7c7	s
konečným	konečný	k2eAgInSc7d1	konečný
mírem	mír	k1gInSc7	mír
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1748	[number]	k4	1748
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
smlouvou	smlouva	k1gFnSc7	smlouva
Rakousko	Rakousko	k1gNnSc1	Rakousko
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
jen	jen	k9	jen
několik	několik	k4yIc4	několik
menších	malý	k2eAgNnPc2d2	menší
území	území	k1gNnPc2	území
(	(	kIx(	(
<g/>
Parmu	Parma	k1gFnSc4	Parma
<g/>
,	,	kIx,	,
Piacenzu	Piacenza	k1gFnSc4	Piacenza
a	a	k8xC	a
Guastallu	Guastalla	k1gFnSc4	Guastalla
<g/>
)	)	kIx)	)
a	a	k8xC	a
jedno	jeden	k4xCgNnSc1	jeden
větší	veliký	k2eAgNnSc4d2	veliký
<g/>
,	,	kIx,	,
Slezsko	Slezsko	k1gNnSc4	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
si	se	k3xPyFc3	se
obhájila	obhájit	k5eAaPmAgFnS	obhájit
Rakouské	rakouský	k2eAgNnSc4d1	rakouské
Nizozemí	Nizozemí	k1gNnSc4	Nizozemí
a	a	k8xC	a
Milán	Milán	k1gInSc1	Milán
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
všech	všecek	k3xTgFnPc2	všecek
osm	osm	k4xCc1	osm
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
podepsaly	podepsat	k5eAaPmAgFnP	podepsat
pod	pod	k7c7	pod
touto	tento	k3xDgFnSc7	tento
smlouvou	smlouva	k1gFnSc7	smlouva
(	(	kIx(	(
<g/>
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
,	,	kIx,	,
Holandsko	Holandsko	k1gNnSc1	Holandsko
<g/>
,	,	kIx,	,
Sardinie	Sardinie	k1gFnSc1	Sardinie
–	–	k?	–
Piemont	Piemont	k1gInSc1	Piemont
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
a	a	k8xC	a
Janov	Janov	k1gInSc1	Janov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
definitivně	definitivně	k6eAd1	definitivně
potvrdilo	potvrdit	k5eAaPmAgNnS	potvrdit
platnost	platnost	k1gFnSc4	platnost
pragmatické	pragmatický	k2eAgFnSc2d1	pragmatická
sankce	sankce	k1gFnSc2	sankce
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
skončila	skončit	k5eAaPmAgFnS	skončit
válka	válka	k1gFnSc1	válka
o	o	k7c4	o
rakouské	rakouský	k2eAgNnSc4d1	rakouské
dědictví	dědictví	k1gNnSc4	dědictví
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c4	na
ztrátu	ztráta	k1gFnSc4	ztráta
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
,	,	kIx,	,
mladá	mladý	k2eAgFnSc1d1	mladá
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
obstála	obstát	k5eAaPmAgFnS	obstát
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
ztrátou	ztráta	k1gFnSc7	ztráta
Slezska	Slezsko	k1gNnSc2	Slezsko
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
nehodlala	hodlat	k5eNaImAgFnS	hodlat
smířit	smířit	k5eAaPmF	smířit
a	a	k8xC	a
hodlala	hodlat	k5eAaImAgFnS	hodlat
si	se	k3xPyFc3	se
jej	on	k3xPp3gNnSc4	on
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
vzít	vzít	k5eAaPmF	vzít
zpět	zpět	k6eAd1	zpět
od	od	k7c2	od
Pruska	Prusko	k1gNnSc2	Prusko
<g/>
,	,	kIx,	,
k	k	k7c3	k
tomu	ten	k3xDgMnSc3	ten
ale	ale	k8xC	ale
musela	muset	k5eAaImAgFnS	muset
jednak	jednak	k8xC	jednak
naplnit	naplnit	k5eAaPmF	naplnit
státní	státní	k2eAgFnSc4d1	státní
pokladnu	pokladna	k1gFnSc4	pokladna
penězi	peníze	k1gInPc7	peníze
a	a	k8xC	a
zmodernizovat	zmodernizovat	k5eAaPmF	zmodernizovat
armádu	armáda	k1gFnSc4	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
splnění	splnění	k1gNnSc3	splnění
obojího	obojí	k4xRgMnSc2	obojí
byly	být	k5eAaImAgFnP	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
reformy	reforma	k1gFnPc1	reforma
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
mohla	moct	k5eAaImAgFnS	moct
konečně	konečně	k6eAd1	konečně
vrhnout	vrhnout	k5eAaPmF	vrhnout
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1745	[number]	k4	1745
zemřel	zemřít	k5eAaPmAgMnS	zemřít
Karel	Karel	k1gMnSc1	Karel
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Bavorský	bavorský	k2eAgMnSc1d1	bavorský
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
otevřela	otevřít	k5eAaPmAgFnS	otevřít
cesta	cesta	k1gFnSc1	cesta
k	k	k7c3	k
titulu	titul	k1gInSc3	titul
římskoněmeckého	římskoněmecký	k2eAgMnSc2d1	římskoněmecký
císaře	císař	k1gMnSc2	císař
pro	pro	k7c4	pro
Františka	František	k1gMnSc4	František
Štěpána	Štěpán	k1gMnSc4	Štěpán
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
také	také	k9	také
začala	začít	k5eAaPmAgFnS	začít
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
přesvědčovat	přesvědčovat	k5eAaImF	přesvědčovat
některé	některý	k3yIgMnPc4	některý
kurfiřty	kurfiřt	k1gMnPc4	kurfiřt
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
o	o	k7c6	o
volbě	volba	k1gFnSc6	volba
nového	nový	k2eAgMnSc2d1	nový
římskoněmeckého	římskoněmecký	k2eAgMnSc2d1	římskoněmecký
císaře	císař	k1gMnSc2	císař
rozhodovali	rozhodovat	k5eAaImAgMnP	rozhodovat
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
kurfiřtem	kurfiřt	k1gMnSc7	kurfiřt
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yIgInSc2	který
"	"	kIx"	"
<g/>
lobovala	lobovat	k5eAaBmAgFnS	lobovat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
mohučský	mohučský	k2eAgMnSc1d1	mohučský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
zároveň	zároveň	k6eAd1	zároveň
arcikancléřem	arcikancléř	k1gMnSc7	arcikancléř
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
tak	tak	k6eAd1	tak
důležitý	důležitý	k2eAgInSc1d1	důležitý
hlas	hlas	k1gInSc1	hlas
při	při	k7c6	při
volbě	volba	k1gFnSc6	volba
<g/>
.	.	kIx.	.
</s>
<s>
Poslala	poslat	k5eAaPmAgFnS	poslat
mu	on	k3xPp3gNnSc3	on
dopis	dopis	k1gInSc4	dopis
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
přimlouvala	přimlouvat	k5eAaImAgFnS	přimlouvat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
stal	stát	k5eAaPmAgMnS	stát
císařem	císař	k1gMnSc7	císař
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
obrátila	obrátit	k5eAaPmAgFnS	obrátit
na	na	k7c4	na
syna	syn	k1gMnSc4	syn
Karla	Karel	k1gMnSc2	Karel
VII	VII	kA	VII
<g/>
.	.	kIx.	.
–	–	k?	–
Maxmiliána	Maxmiliána	k1gFnSc1	Maxmiliána
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Josefa	Josef	k1gMnSc2	Josef
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
Marii	Maria	k1gFnSc4	Maria
Terezii	Terezie	k1gFnSc4	Terezie
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
dohodu	dohoda	k1gFnSc4	dohoda
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
také	také	k9	také
dá	dát	k5eAaPmIp3nS	dát
hlas	hlas	k1gInSc4	hlas
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
získá	získat	k5eAaPmIp3nS	získat
hlas	hlas	k1gInSc4	hlas
i	i	k9	i
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
strýce	strýc	k1gMnSc2	strýc
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
byl	být	k5eAaImAgMnS	být
kurfiřt	kurfiřt	k1gMnSc1	kurfiřt
v	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gNnSc3	on
odstoupí	odstoupit	k5eAaPmIp3nP	odstoupit
zabraná	zabraný	k2eAgNnPc1d1	zabrané
území	území	k1gNnPc1	území
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
však	však	k9	však
s	s	k7c7	s
dohodou	dohoda	k1gFnSc7	dohoda
nesouhlasila	souhlasit	k5eNaImAgFnS	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
snahu	snaha	k1gFnSc4	snaha
Francie	Francie	k1gFnSc2	Francie
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1745	[number]	k4	1745
stal	stát	k5eAaPmAgMnS	stát
František	František	k1gMnSc1	František
Štěpán	Štěpán	k1gMnSc1	Štěpán
<g/>
,	,	kIx,	,
manžel	manžel	k1gMnSc1	manžel
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
,	,	kIx,	,
římskoněmeckým	římskoněmecký	k2eAgMnSc7d1	římskoněmecký
císařem	císař	k1gMnSc7	císař
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
<g/>
,	,	kIx,	,
a	a	k8xC	a
získává	získávat	k5eAaImIp3nS	získávat
tak	tak	k9	tak
jméno	jméno	k1gNnSc4	jméno
František	František	k1gMnSc1	František
I.	I.	kA	I.
Z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
se	se	k3xPyFc4	se
tím	ten	k3xDgInSc7	ten
stává	stávat	k5eAaImIp3nS	stávat
říšské	říšský	k2eAgNnSc1d1	říšské
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
<g/>
,	,	kIx,	,
dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1746	[number]	k4	1746
porodila	porodit	k5eAaPmAgFnS	porodit
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
další	další	k2eAgNnSc4d1	další
dítě	dítě	k1gNnSc4	dítě
–	–	k?	–
Marii	Maria	k1gFnSc3	Maria
Amálii	Amálie	k1gFnSc3	Amálie
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nevyrovnala	vyrovnat	k5eNaPmAgNnP	vyrovnat
se	s	k7c7	s
ztrátou	ztráta	k1gFnSc7	ztráta
Slezska	Slezsko	k1gNnSc2	Slezsko
a	a	k8xC	a
nikdy	nikdy	k6eAd1	nikdy
si	se	k3xPyFc3	se
nehodlala	hodlat	k5eNaImAgFnS	hodlat
připustit	připustit	k5eAaPmF	připustit
jeho	jeho	k3xOp3gFnSc4	jeho
definitivní	definitivní	k2eAgFnSc4d1	definitivní
ztrátu	ztráta	k1gFnSc4	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
si	se	k3xPyFc3	se
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc1	Terezie
uvědomovala	uvědomovat	k5eAaImAgFnS	uvědomovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Prusko	Prusko	k1gNnSc1	Prusko
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
porazit	porazit	k5eAaPmF	porazit
jen	jen	k9	jen
s	s	k7c7	s
jiným	jiný	k2eAgMnSc7d1	jiný
vojenským	vojenský	k2eAgMnSc7d1	vojenský
partnerem	partner	k1gMnSc7	partner
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnSc7	který
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
stát	stát	k5eAaPmF	stát
Francie	Francie	k1gFnSc1	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
nedokázala	dokázat	k5eNaPmAgFnS	dokázat
odpustit	odpustit	k5eAaPmF	odpustit
zradu	zrada	k1gFnSc4	zrada
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ji	on	k3xPp3gFnSc4	on
prakticky	prakticky	k6eAd1	prakticky
donutila	donutit	k5eAaPmAgFnS	donutit
podepsat	podepsat	k5eAaPmF	podepsat
mírovou	mírový	k2eAgFnSc4d1	mírová
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
definitivně	definitivně	k6eAd1	definitivně
Rakousko	Rakousko	k1gNnSc1	Rakousko
zříkalo	zříkat	k5eAaImAgNnS	zříkat
Slezska	Slezsko	k1gNnSc2	Slezsko
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
důvodem	důvod	k1gInSc7	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
chtěla	chtít	k5eAaImAgFnS	chtít
spojit	spojit	k5eAaPmF	spojit
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
to	ten	k3xDgNnSc4	ten
podařilo	podařit	k5eAaPmAgNnS	podařit
<g/>
,	,	kIx,	,
Prusko	Prusko	k1gNnSc1	Prusko
by	by	kYmCp3nS	by
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
svého	svůj	k3xOyFgMnSc4	svůj
důležitého	důležitý	k2eAgMnSc4d1	důležitý
spojence	spojenec	k1gMnSc4	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
neměli	mít	k5eNaImAgMnP	mít
Marii	Maria	k1gFnSc4	Maria
Terezii	Terezie	k1gFnSc4	Terezie
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
Francouzi	Francouz	k1gMnPc1	Francouz
moc	moc	k6eAd1	moc
v	v	k7c6	v
lásce	láska	k1gFnSc6	láska
<g/>
,	,	kIx,	,
musela	muset	k5eAaImAgFnS	muset
nejdříve	dříve	k6eAd3	dříve
poslat	poslat	k5eAaPmF	poslat
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
svého	svůj	k3xOyFgMnSc2	svůj
vyslance	vyslanec	k1gMnSc2	vyslanec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
připravil	připravit	k5eAaPmAgMnS	připravit
půdu	půda	k1gFnSc4	půda
pro	pro	k7c4	pro
konečná	konečný	k2eAgNnPc4d1	konečné
jednání	jednání	k1gNnPc4	jednání
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgMnSc7	tento
vyslancem	vyslanec	k1gMnSc7	vyslanec
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Moravan	Moravan	k1gMnSc1	Moravan
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Antonín	Antonín	k1gMnSc1	Antonín
Kounic	Kounice	k1gFnPc2	Kounice
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
poslala	poslat	k5eAaPmAgFnS	poslat
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1750	[number]	k4	1750
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
po	po	k7c6	po
zjištění	zjištění	k1gNnSc6	zjištění
poměrů	poměr	k1gInPc2	poměr
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
došel	dojít	k5eAaPmAgInS	dojít
nejprve	nejprve	k6eAd1	nejprve
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
má	mít	k5eAaImIp3nS	mít
upustit	upustit	k5eAaPmF	upustit
od	od	k7c2	od
záměru	záměr	k1gInSc2	záměr
spojenectví	spojenectví	k1gNnPc2	spojenectví
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
kořeny	kořen	k1gInPc1	kořen
nepřátelství	nepřátelství	k1gNnSc2	nepřátelství
Francouzů	Francouz	k1gMnPc2	Francouz
k	k	k7c3	k
Rakousku	Rakousko	k1gNnSc3	Rakousko
byly	být	k5eAaImAgInP	být
stále	stále	k6eAd1	stále
hodně	hodně	k6eAd1	hodně
patrné	patrný	k2eAgNnSc1d1	patrné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
domluvě	domluva	k1gFnSc6	domluva
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
Terezií	Terezie	k1gFnPc2	Terezie
přece	přece	k8xC	přece
jen	jen	k6eAd1	jen
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
snažení	snažení	k1gNnSc6	snažení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úspěšných	úspěšný	k2eAgInPc6d1	úspěšný
prvních	první	k4xOgInPc6	první
jednání	jednání	k1gNnPc1	jednání
jej	on	k3xPp3gMnSc4	on
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1753	[number]	k4	1753
povolala	povolat	k5eAaPmAgFnS	povolat
zpátky	zpátky	k6eAd1	zpátky
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
udělala	udělat	k5eAaPmAgFnS	udělat
dvorního	dvorní	k2eAgMnSc2d1	dvorní
a	a	k8xC	a
státního	státní	k2eAgMnSc2d1	státní
kancléře	kancléř	k1gMnSc2	kancléř
i	i	k8xC	i
šéfa	šéf	k1gMnSc2	šéf
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1755	[number]	k4	1755
se	se	k3xPyFc4	se
přiostřila	přiostřit	k5eAaPmAgFnS	přiostřit
situace	situace	k1gFnSc1	situace
mezi	mezi	k7c7	mezi
Anglií	Anglie	k1gFnSc7	Anglie
a	a	k8xC	a
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
hrozila	hrozit	k5eAaImAgFnS	hrozit
nová	nový	k2eAgFnSc1d1	nová
válka	válka	k1gFnSc1	válka
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
nahrávalo	nahrávat	k5eAaImAgNnS	nahrávat
do	do	k7c2	do
karet	kareta	k1gFnPc2	kareta
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1755	[number]	k4	1755
začala	začít	k5eAaPmAgFnS	začít
první	první	k4xOgFnSc1	první
velká	velká	k1gFnSc1	velká
jednání	jednání	k1gNnSc2	jednání
mezi	mezi	k7c7	mezi
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
Rakouskem	Rakousko	k1gNnSc7	Rakousko
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
byla	být	k5eAaImAgNnP	být
stále	stále	k6eAd1	stále
vedena	vést	k5eAaImNgNnP	vést
tajně	tajně	k6eAd1	tajně
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ani	ani	k8xC	ani
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
zúčastněných	zúčastněný	k2eAgInPc2d1	zúčastněný
si	se	k3xPyFc3	se
nemohl	moct	k5eNaImAgMnS	moct
dovolit	dovolit	k5eAaPmF	dovolit
ztrátu	ztráta	k1gFnSc4	ztráta
svého	svůj	k3xOyFgMnSc2	svůj
spojence	spojenec	k1gMnSc2	spojenec
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
jednání	jednání	k1gNnSc4	jednání
selhala	selhat	k5eAaPmAgFnS	selhat
a	a	k8xC	a
o	o	k7c6	o
všem	všecek	k3xTgNnSc6	všecek
se	se	k3xPyFc4	se
dozvěděli	dozvědět	k5eAaPmAgMnP	dozvědět
ostatní	ostatní	k2eAgMnPc1d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
jednání	jednání	k1gNnSc6	jednání
padlo	padnout	k5eAaImAgNnS	padnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rakousko	Rakousko	k1gNnSc4	Rakousko
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
přálo	přát	k5eAaImAgNnS	přát
co	co	k3yInSc1	co
nejrychlejší	rychlý	k2eAgNnSc1d3	nejrychlejší
vyjednání	vyjednání	k1gNnSc1	vyjednání
dohody	dohoda	k1gFnSc2	dohoda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
případě	případ	k1gInSc6	případ
vypuknutí	vypuknutí	k1gNnSc2	vypuknutí
války	válka	k1gFnSc2	válka
mezi	mezi	k7c4	mezi
Anglii	Anglie	k1gFnSc4	Anglie
a	a	k8xC	a
Francií	Francie	k1gFnSc7	Francie
nemuselo	muset	k5eNaImAgNnS	muset
zasahovat	zasahovat	k5eAaImF	zasahovat
do	do	k7c2	do
sporu	spor	k1gInSc2	spor
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
nesvědčilo	svědčit	k5eNaImAgNnS	svědčit
pozdějšímu	pozdní	k2eAgNnSc3d2	pozdější
vyjednávání	vyjednávání	k1gNnSc3	vyjednávání
<g/>
.	.	kIx.	.
</s>
<s>
Rakousko	Rakousko	k1gNnSc1	Rakousko
tedy	tedy	k9	tedy
chtělo	chtít	k5eAaImAgNnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Francie	Francie	k1gFnSc1	Francie
zrušila	zrušit	k5eAaPmAgFnS	zrušit
svůj	svůj	k3xOyFgInSc4	svůj
spolek	spolek	k1gInSc4	spolek
s	s	k7c7	s
Pruskem	Prusko	k1gNnSc7	Prusko
a	a	k8xC	a
Francie	Francie	k1gFnSc1	Francie
žádala	žádat	k5eAaImAgFnS	žádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Rakousko	Rakousko	k1gNnSc1	Rakousko
přerušilo	přerušit	k5eAaPmAgNnS	přerušit
své	své	k1gNnSc4	své
spojenectví	spojenectví	k1gNnSc2	spojenectví
s	s	k7c7	s
Anglií	Anglie	k1gFnSc7	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Jednání	jednání	k1gNnPc1	jednání
poté	poté	k6eAd1	poté
byla	být	k5eAaImAgNnP	být
zastavena	zastavit	k5eAaPmNgNnP	zastavit
až	až	k6eAd1	až
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
1755	[number]	k4	1755
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
obnovila	obnovit	k5eAaPmAgFnS	obnovit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
si	se	k3xPyFc3	se
Francie	Francie	k1gFnSc1	Francie
uvědomovala	uvědomovat	k5eAaImAgFnS	uvědomovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rakousko	Rakousko	k1gNnSc1	Rakousko
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
chvíli	chvíle	k1gFnSc6	chvíle
lepším	lepšit	k5eAaImIp1nS	lepšit
spojencem	spojenec	k1gMnSc7	spojenec
než	než	k8xS	než
Prusko	Prusko	k1gNnSc1	Prusko
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
povolit	povolit	k5eAaPmF	povolit
vstup	vstup	k1gInSc4	vstup
francouzských	francouzský	k2eAgFnPc2d1	francouzská
jednotek	jednotka	k1gFnPc2	jednotka
do	do	k7c2	do
Holandska	Holandsko	k1gNnSc2	Holandsko
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgInSc2	který
by	by	kYmCp3nP	by
útočily	útočit	k5eAaImAgFnP	útočit
francouzské	francouzský	k2eAgFnPc1d1	francouzská
jednotky	jednotka	k1gFnPc1	jednotka
na	na	k7c6	na
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Anglie	Anglie	k1gFnSc1	Anglie
si	se	k3xPyFc3	se
naopak	naopak	k6eAd1	naopak
uvědomila	uvědomit	k5eAaPmAgFnS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Prusko	Prusko	k1gNnSc1	Prusko
je	být	k5eAaImIp3nS	být
lepším	dobrý	k2eAgMnSc7d2	lepší
spojencem	spojenec	k1gMnSc7	spojenec
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
bála	bát	k5eAaImAgFnS	bát
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
Prusové	Prus	k1gMnPc1	Prus
zaútočili	zaútočit	k5eAaPmAgMnP	zaútočit
na	na	k7c4	na
Hannoversko	Hannoversko	k1gNnSc4	Hannoversko
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
již	již	k6eAd1	již
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
tušila	tušit	k5eAaImAgFnS	tušit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rakousko	Rakousko	k1gNnSc1	Rakousko
má	mít	k5eAaImIp3nS	mít
nekalé	kalý	k2eNgInPc4d1	nekalý
úmysly	úmysl	k1gInPc4	úmysl
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1756	[number]	k4	1756
uzavírají	uzavírat	k5eAaImIp3nP	uzavírat
Prusko	Prusko	k1gNnSc4	Prusko
a	a	k8xC	a
Anglie	Anglie	k1gFnSc1	Anglie
tzv.	tzv.	kA	tzv.
westminsterskou	westminsterský	k2eAgFnSc4d1	westminsterská
konvenci	konvence	k1gFnSc4	konvence
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
konvenci	konvence	k1gFnSc3	konvence
nestálo	stát	k5eNaImAgNnS	stát
již	již	k6eAd1	již
nic	nic	k3yNnSc1	nic
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
uzavření	uzavření	k1gNnSc3	uzavření
spojenectví	spojenectví	k1gNnSc2	spojenectví
mezi	mezi	k7c7	mezi
Rakouskem	Rakousko	k1gNnSc7	Rakousko
a	a	k8xC	a
Francií	Francie	k1gFnSc7	Francie
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
věcí	věc	k1gFnSc7	věc
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
bylo	být	k5eAaImAgNnS	být
nutno	nutno	k6eAd1	nutno
vyjednat	vyjednat	k5eAaPmF	vyjednat
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
cena	cena	k1gFnSc1	cena
dohody	dohoda	k1gFnSc2	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Rakousko	Rakousko	k1gNnSc1	Rakousko
mělo	mít	k5eAaImAgNnS	mít
jediný	jediný	k2eAgInSc4d1	jediný
požadavek	požadavek	k1gInSc4	požadavek
–	–	k?	–
Slezsko	Slezsko	k1gNnSc1	Slezsko
a	a	k8xC	a
Francie	Francie	k1gFnSc1	Francie
žádala	žádat	k5eAaImAgFnS	žádat
rakouské	rakouský	k2eAgNnSc4d1	rakouské
Lucembursko	Lucembursko	k1gNnSc4	Lucembursko
a	a	k8xC	a
zbytek	zbytek	k1gInSc4	zbytek
Nizozemí	Nizozemí	k1gNnSc2	Nizozemí
<g/>
.	.	kIx.	.
</s>
<s>
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
to	ten	k3xDgNnSc1	ten
nežádalo	žádat	k5eNaImAgNnS	žádat
<g/>
,	,	kIx,	,
dostalo	dostat	k5eAaPmAgNnS	dostat
navíc	navíc	k6eAd1	navíc
Parmu	Parma	k1gFnSc4	Parma
a	a	k8xC	a
Piacenzu	Piacenza	k1gFnSc4	Piacenza
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
dohodly	dohodnout	k5eAaPmAgFnP	dohodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
napadení	napadení	k1gNnSc6	napadení
jedné	jeden	k4xCgFnSc2	jeden
země	zem	k1gFnSc2	zem
ta	ten	k3xDgNnPc1	ten
druhá	druhý	k4xOgFnSc1	druhý
první	první	k4xOgNnSc4	první
pomůže	pomoct	k5eAaPmIp3nS	pomoct
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
si	se	k3xPyFc3	se
navzájem	navzájem	k6eAd1	navzájem
potvrdily	potvrdit	k5eAaPmAgFnP	potvrdit
državy	država	k1gFnPc4	država
a	a	k8xC	a
potvrdily	potvrdit	k5eAaPmAgInP	potvrdit
neutralitu	neutralita	k1gFnSc4	neutralita
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úspěšných	úspěšný	k2eAgNnPc6d1	úspěšné
jednáních	jednání	k1gNnPc6	jednání
byla	být	k5eAaImAgFnS	být
dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1756	[number]	k4	1756
sepsána	sepsán	k2eAgFnSc1d1	sepsána
dohoda	dohoda	k1gFnSc1	dohoda
ve	v	k7c6	v
francouzském	francouzský	k2eAgInSc6d1	francouzský
Jouy-en-Josas	Jouyn-Josas	k1gInSc1	Jouy-en-Josas
<g/>
,	,	kIx,	,
sídle	sídlo	k1gNnSc6	sídlo
francouzského	francouzský	k2eAgMnSc2d1	francouzský
ministra	ministr	k1gMnSc2	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
XV	XV	kA	XV
<g/>
.	.	kIx.	.
podepsal	podepsat	k5eAaPmAgMnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
hned	hned	k6eAd1	hned
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
a	a	k8xC	a
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
až	až	k9	až
19	[number]	k4	19
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1756	[number]	k4	1756
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
vojenské	vojenský	k2eAgInPc1d1	vojenský
manévry	manévr	k1gInPc1	manévr
Rakouska	Rakousko	k1gNnSc2	Rakousko
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
nenechaly	nechat	k5eNaPmAgFnP	nechat
dlouho	dlouho	k6eAd1	dlouho
čekat	čekat	k5eAaImF	čekat
a	a	k8xC	a
koncem	koncem	k7c2	koncem
července	červenec	k1gInSc2	červenec
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
své	svůj	k3xOyFgFnPc4	svůj
jednotky	jednotka	k1gFnPc4	jednotka
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Prusko	Prusko	k1gNnSc1	Prusko
ihned	ihned	k6eAd1	ihned
reagovalo	reagovat	k5eAaBmAgNnS	reagovat
a	a	k8xC	a
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1756	[number]	k4	1756
bez	bez	k7c2	bez
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
války	válka	k1gFnSc2	válka
Rakousku	Rakousko	k1gNnSc6	Rakousko
vpadlo	vpadnout	k5eAaPmAgNnS	vpadnout
do	do	k7c2	do
Saska	Sasko	k1gNnSc2	Sasko
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
vypuká	vypukat	k5eAaImIp3nS	vypukat
válka	válka	k1gFnSc1	válka
sedmiletá	sedmiletý	k2eAgFnSc1d1	sedmiletá
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
třetí	třetí	k4xOgFnSc1	třetí
slezská	slezský	k2eAgFnSc1d1	Slezská
válka	válka	k1gFnSc1	válka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
nutno	nutno	k6eAd1	nutno
provést	provést	k5eAaPmF	provést
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
svatba	svatba	k1gFnSc1	svatba
Josefa	Josef	k1gMnSc2	Josef
<g/>
,	,	kIx,	,
nejstaršího	starý	k2eAgMnSc2d3	nejstarší
syna	syn	k1gMnSc2	syn
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
svatby	svatba	k1gFnSc2	svatba
mělo	mít	k5eAaImAgNnS	mít
Rakousko	Rakousko	k1gNnSc1	Rakousko
co	co	k8xS	co
největší	veliký	k2eAgInSc1d3	veliký
užitek	užitek	k1gInSc1	užitek
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
plánováním	plánování	k1gNnSc7	plánování
sňatku	sňatek	k1gInSc2	sňatek
Josefa	Josef	k1gMnSc2	Josef
začala	začít	k5eAaPmAgFnS	začít
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
ještě	ještě	k9	ještě
za	za	k7c2	za
třetí	třetí	k4xOgFnSc2	třetí
slezské	slezský	k2eAgFnSc2d1	Slezská
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1760	[number]	k4	1760
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
nabídka	nabídka	k1gFnSc1	nabídka
přišla	přijít	k5eAaPmAgFnS	přijít
od	od	k7c2	od
Neapolského	neapolský	k2eAgMnSc2d1	neapolský
krále	král	k1gMnSc2	král
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
Josef	Josef	k1gMnSc1	Josef
vzal	vzít	k5eAaPmAgMnS	vzít
jeho	jeho	k3xOp3gFnSc4	jeho
nejstarší	starý	k2eAgFnSc4d3	nejstarší
dceru	dcera	k1gFnSc4	dcera
a	a	k8xC	a
některá	některý	k3yIgFnSc1	některý
dcera	dcera	k1gFnSc1	dcera
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc1	Terezie
si	se	k3xPyFc3	se
vzala	vzít	k5eAaPmAgFnS	vzít
nejstaršího	starý	k2eAgMnSc4d3	nejstarší
syna	syn	k1gMnSc4	syn
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Výhodnost	výhodnost	k1gFnSc4	výhodnost
toho	ten	k3xDgInSc2	ten
sňatku	sňatek	k1gInSc2	sňatek
byla	být	k5eAaImAgFnS	být
dvojí	dvojí	k4xRgFnSc1	dvojí
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
zaručovala	zaručovat	k5eAaImAgFnS	zaručovat
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
Rakouska	Rakousko	k1gNnSc2	Rakousko
alespoň	alespoň	k9	alespoň
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
světové	světový	k2eAgFnSc2d1	světová
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
jednak	jednak	k8xC	jednak
i	i	k9	i
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
v	v	k7c6	v
dohledné	dohledný	k2eAgFnSc6d1	dohledná
době	doba	k1gFnSc6	doba
nastoupí	nastoupit	k5eAaPmIp3nS	nastoupit
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
Španělska	Španělsko	k1gNnSc2	Španělsko
za	za	k7c2	za
svého	svůj	k3xOyFgMnSc2	svůj
bezdětného	bezdětný	k2eAgMnSc2d1	bezdětný
nevlastního	vlastní	k2eNgMnSc2d1	nevlastní
bratra	bratr	k1gMnSc2	bratr
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
po	po	k7c6	po
pozdější	pozdní	k2eAgFnSc6d2	pozdější
smrti	smrt	k1gFnSc6	smrt
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
zakládalo	zakládat	k5eAaImAgNnS	zakládat
nárok	nárok	k1gInSc4	nárok
na	na	k7c4	na
španělský	španělský	k2eAgInSc4d1	španělský
trůn	trůn	k1gInSc4	trůn
pro	pro	k7c4	pro
syna	syn	k1gMnSc4	syn
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
–	–	k?	–
Josefa	Josef	k1gMnSc2	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
možností	možnost	k1gFnSc7	možnost
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
výhodně	výhodně	k6eAd1	výhodně
oženit	oženit	k5eAaPmF	oženit
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
Josefa	Josef	k1gMnSc4	Josef
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
sňatek	sňatek	k1gInSc1	sňatek
s	s	k7c7	s
vnučkou	vnučka	k1gFnSc7	vnučka
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XV	XV	kA	XV
<g/>
.	.	kIx.	.
–	–	k?	–
s	s	k7c7	s
Isabelou	Isabela	k1gFnSc7	Isabela
Parmskou	parmský	k2eAgFnSc7d1	parmská
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
i	i	k9	i
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
nepopudila	popudit	k5eNaPmAgFnS	popudit
španělské	španělský	k2eAgInPc4d1	španělský
Bourbony	bourbon	k1gInPc4	bourbon
<g/>
,	,	kIx,	,
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
se	se	k3xPyFc4	se
oženit	oženit	k5eAaPmF	oženit
svého	svůj	k3xOyFgMnSc2	svůj
druhého	druhý	k4xOgMnSc2	druhý
syna	syn	k1gMnSc2	syn
Leopolda	Leopold	k1gMnSc2	Leopold
s	s	k7c7	s
Marii	Mario	k1gMnPc7	Mario
Ludovikou	Ludovika	k1gFnSc7	Ludovika
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
si	se	k3xPyFc3	se
měl	mít	k5eAaImAgMnS	mít
původně	původně	k6eAd1	původně
brát	brát	k5eAaImF	brát
právě	právě	k9	právě
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Syn	syn	k1gMnSc1	syn
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
–	–	k?	–
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
IV	IV	kA	IV
<g/>
.	.	kIx.	.
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgMnS	vzít
dceru	dcera	k1gFnSc4	dcera
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
–	–	k?	–
Marii	Maria	k1gFnSc3	Maria
Karolinu	Karolinum	k1gNnSc6	Karolinum
<g/>
.	.	kIx.	.
</s>
<s>
Svatba	svatba	k1gFnSc1	svatba
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
Isabely	Isabela	k1gFnPc1	Isabela
Parmské	parmský	k2eAgFnPc1d1	parmská
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1760	[number]	k4	1760
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ještě	ještě	k6eAd1	ještě
probíhala	probíhat	k5eAaImAgFnS	probíhat
sedmiletá	sedmiletý	k2eAgFnSc1d1	sedmiletá
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
přesto	přesto	k8xC	přesto
byla	být	k5eAaImAgNnP	být
velmi	velmi	k6eAd1	velmi
nákladná	nákladný	k2eAgNnPc1d1	nákladné
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
dokázat	dokázat	k5eAaPmF	dokázat
nepřátelům	nepřítel	k1gMnPc3	nepřítel
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
že	že	k8xS	že
státní	státní	k2eAgFnSc1d1	státní
kasa	kasa	k1gFnSc1	kasa
je	být	k5eAaImIp3nS	být
ještě	ještě	k9	ještě
poměrně	poměrně	k6eAd1	poměrně
plná	plný	k2eAgFnSc1d1	plná
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
následující	následující	k2eAgNnPc1d1	následující
léta	léto	k1gNnPc1	léto
nebyla	být	k5eNaImAgNnP	být
pro	pro	k7c4	pro
Rakousko	Rakousko	k1gNnSc4	Rakousko
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
Habsburky	Habsburk	k1gMnPc4	Habsburk
příliš	příliš	k6eAd1	příliš
příznivá	příznivý	k2eAgFnSc1d1	příznivá
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1761	[number]	k4	1761
zemřel	zemřít	k5eAaPmAgMnS	zemřít
Karel	Karel	k1gMnSc1	Karel
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1762	[number]	k4	1762
Johanna	Johanen	k2eAgFnSc1d1	Johanna
Gabriela	Gabriela	k1gFnSc1	Gabriela
(	(	kIx(	(
<g/>
oba	dva	k4xCgMnPc1	dva
potomci	potomek	k1gMnPc1	potomek
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
)	)	kIx)	)
a	a	k8xC	a
dovršením	dovršení	k1gNnSc7	dovršení
všeho	všecek	k3xTgInSc2	všecek
roku	rok	k1gInSc2	rok
1763	[number]	k4	1763
zemřela	zemřít	k5eAaPmAgFnS	zemřít
i	i	k9	i
sotva	sotva	k6eAd1	sotva
dvaadvacetiletá	dvaadvacetiletý	k2eAgFnSc1d1	dvaadvacetiletá
Isabella	Isabella	k1gFnSc1	Isabella
Parmská	parmský	k2eAgFnSc1d1	parmská
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
tři	tři	k4xCgMnPc1	tři
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
na	na	k7c4	na
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
obávanou	obávaný	k2eAgFnSc4d1	obávaná
metlu	metla	k1gFnSc4	metla
–	–	k?	–
neštovice	neštovice	k1gFnPc1	neštovice
<g/>
.	.	kIx.	.
</s>
<s>
Isabella	Isabella	k1gFnSc1	Isabella
Parmská	parmský	k2eAgFnSc1d1	parmská
po	po	k7c6	po
sobě	se	k3xPyFc3	se
nechala	nechat	k5eAaPmAgFnS	nechat
dceru	dcera	k1gFnSc4	dcera
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
však	však	k9	však
jako	jako	k8xS	jako
sedmiletá	sedmiletý	k2eAgFnSc1d1	sedmiletá
zemřela	zemřít	k5eAaPmAgFnS	zemřít
rovněž	rovněž	k9	rovněž
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1764	[number]	k4	1764
zvolen	zvolit	k5eAaPmNgMnS	zvolit
římskoněmeckým	římskoněmecký	k2eAgMnSc7d1	římskoněmecký
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
neměl	mít	k5eNaImAgMnS	mít
mužského	mužský	k2eAgMnSc4d1	mužský
potomka	potomek	k1gMnSc4	potomek
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
Habsburkové	Habsburk	k1gMnPc1	Habsburk
po	po	k7c4	po
generace	generace	k1gFnPc4	generace
utužovali	utužovat	k5eAaImAgMnP	utužovat
svou	svůj	k3xOyFgFnSc4	svůj
moc	moc	k1gFnSc4	moc
především	především	k6eAd1	především
sňatkovou	sňatkový	k2eAgFnSc7d1	sňatková
politikou	politika	k1gFnSc7	politika
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnPc1	Terezie
<g/>
,	,	kIx,	,
i	i	k9	i
přes	přes	k7c4	přes
synův	synův	k2eAgInSc4d1	synův
odpor	odpor	k1gInSc4	odpor
<g/>
,	,	kIx,	,
hledat	hledat	k5eAaImF	hledat
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
nevěstu	nevěsta	k1gFnSc4	nevěsta
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Vítězkou	vítězka	k1gFnSc7	vítězka
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
německá	německý	k2eAgFnSc1d1	německá
princezna	princezna	k1gFnSc1	princezna
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Wittelsbachů	Wittelsbach	k1gInPc2	Wittelsbach
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
Josefa	Josefa	k1gFnSc1	Josefa
Bavorská	bavorský	k2eAgFnSc1d1	bavorská
<g/>
,	,	kIx,	,
po	po	k7c6	po
jejíž	jejíž	k3xOyRp3gFnSc6	jejíž
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1767	[number]	k4	1767
se	se	k3xPyFc4	se
Josef	Josef	k1gMnSc1	Josef
už	už	k6eAd1	už
neoženil	oženit	k5eNaPmAgMnS	oženit
<g/>
.	.	kIx.	.
</s>
<s>
Svatba	svatba	k1gFnSc1	svatba
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
23	[number]	k4	23
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1765	[number]	k4	1765
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1767	[number]	k4	1767
si	se	k3xPyFc3	se
neštovice	neštovice	k1gFnSc1	neštovice
opět	opět	k6eAd1	opět
vybraly	vybrat	k5eAaPmAgFnP	vybrat
svou	svůj	k3xOyFgFnSc4	svůj
daň	daň	k1gFnSc4	daň
v	v	k7c6	v
královské	královský	k2eAgFnSc6d1	královská
rodině	rodina	k1gFnSc6	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
onemocněla	onemocnět	k5eAaPmAgFnS	onemocnět
nemilovaná	milovaný	k2eNgFnSc1d1	nemilovaná
manželka	manželka	k1gFnSc1	manželka
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Josefa	Josef	k1gMnSc2	Josef
i	i	k8xC	i
sama	sám	k3xTgFnSc1	sám
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
z	z	k7c2	z
žen	žena	k1gFnPc2	žena
nemoci	nemoc	k1gFnSc2	nemoc
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
28	[number]	k4	28
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
a	a	k8xC	a
velká	velký	k2eAgFnSc1d1	velká
panovnice	panovnice	k1gFnSc1	panovnice
přijala	přijmout	k5eAaPmAgFnS	přijmout
poslední	poslední	k2eAgNnSc4d1	poslední
pomazání	pomazání	k1gNnSc4	pomazání
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
však	však	k9	však
uzdravila	uzdravit	k5eAaPmAgFnS	uzdravit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
června	červen	k1gInSc2	červen
onemocněl	onemocnět	k5eAaPmAgMnS	onemocnět
její	její	k3xOp3gMnSc1	její
zeť	zeť	k1gMnSc1	zeť
Albert	Albert	k1gMnSc1	Albert
Saský	saský	k2eAgMnSc1d1	saský
<g/>
,	,	kIx,	,
manžel	manžel	k1gMnSc1	manžel
Marie	Maria	k1gFnSc2	Maria
Kristiny	Kristina	k1gFnSc2	Kristina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
vzpamatovávala	vzpamatovávat	k5eAaImAgFnS	vzpamatovávat
z	z	k7c2	z
horečky	horečka	k1gFnSc2	horečka
omladnic	omladnice	k1gFnPc2	omladnice
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
jediné	jediný	k2eAgFnSc2d1	jediná
dcery	dcera	k1gFnSc2	dcera
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
on	on	k3xPp3gMnSc1	on
měl	mít	k5eAaImAgMnS	mít
štěstí	štěstí	k1gNnSc4	štěstí
a	a	k8xC	a
přežil	přežít	k5eAaPmAgMnS	přežít
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
onemocněly	onemocnět	k5eAaPmAgFnP	onemocnět
dvě	dva	k4xCgFnPc4	dva
z	z	k7c2	z
dcer	dcera	k1gFnPc2	dcera
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
,	,	kIx,	,
snoubenka	snoubenka	k1gFnSc1	snoubenka
neapolsko-sicilského	neapolskoicilský	k2eAgMnSc2d1	neapolsko-sicilský
krále	král	k1gMnSc2	král
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
Josefa	Josefa	k1gFnSc1	Josefa
a	a	k8xC	a
Marie	Marie	k1gFnSc1	Marie
Alžběta	Alžběta	k1gFnSc1	Alžběta
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Marie	Marie	k1gFnSc1	Marie
Josefa	Josef	k1gMnSc2	Josef
nemoci	nemoc	k1gFnSc2	nemoc
15	[number]	k4	15
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
Alžběta	Alžběta	k1gFnSc1	Alžběta
neštovice	neštovice	k1gFnSc1	neštovice
i	i	k9	i
přes	přes	k7c4	přes
těžký	těžký	k2eAgInSc4d1	těžký
průběh	průběh	k1gInSc4	průběh
nemoci	nemoc	k1gFnSc2	nemoc
přestála	přestát	k5eAaPmAgFnS	přestát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zajištění	zajištění	k1gNnSc6	zajištění
druhého	druhý	k4xOgMnSc2	druhý
sňatku	sňatek	k1gInSc3	sňatek
nejstaršího	starý	k2eAgMnSc2d3	nejstarší
syna	syn	k1gMnSc2	syn
se	se	k3xPyFc4	se
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
"	"	kIx"	"
<g/>
vrhla	vrhnout	k5eAaImAgFnS	vrhnout
<g/>
"	"	kIx"	"
na	na	k7c4	na
svatbu	svatba	k1gFnSc4	svatba
druhého	druhý	k4xOgMnSc2	druhý
nejstaršího	starý	k2eAgMnSc2d3	nejstarší
syna	syn	k1gMnSc2	syn
–	–	k?	–
Leopolda	Leopold	k1gMnSc2	Leopold
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
si	se	k3xPyFc3	se
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
vybrala	vybrat	k5eAaPmAgFnS	vybrat
Marii	Maria	k1gFnSc3	Maria
Ludoviku	Ludovik	k1gInSc2	Ludovik
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
porodila	porodit	k5eAaPmAgFnS	porodit
16	[number]	k4	16
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Svatba	svatba	k1gFnSc1	svatba
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1765	[number]	k4	1765
v	v	k7c6	v
Innsbrucku	Innsbruck	k1gInSc6	Innsbruck
<g/>
.	.	kIx.	.
</s>
<s>
Svatbu	svatba	k1gFnSc4	svatba
a	a	k8xC	a
svatební	svatební	k2eAgFnPc1d1	svatební
oslavy	oslava	k1gFnPc1	oslava
ovšem	ovšem	k9	ovšem
postihly	postihnout	k5eAaPmAgFnP	postihnout
dvě	dva	k4xCgFnPc4	dva
vážné	vážný	k2eAgFnPc4d1	vážná
události	událost	k1gFnPc4	událost
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
svatba	svatba	k1gFnSc1	svatba
byla	být	k5eAaImAgFnS	být
urychlena	urychlen	k2eAgFnSc1d1	urychlena
kvůli	kvůli	k7c3	kvůli
špatnému	špatný	k2eAgNnSc3d1	špatné
zdraví	zdraví	k1gNnSc3	zdraví
ženicha	ženich	k1gMnSc2	ženich
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
vážné	vážný	k2eAgInPc4d1	vážný
problémy	problém	k1gInPc4	problém
se	s	k7c7	s
zažíváním	zažívání	k1gNnSc7	zažívání
<g/>
,	,	kIx,	,
a	a	k8xC	a
jednak	jednak	k8xC	jednak
pár	pár	k4xCyI	pár
dní	den	k1gInPc2	den
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
dne	den	k1gInSc2	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1765	[number]	k4	1765
večer	večer	k6eAd1	večer
náhle	náhle	k6eAd1	náhle
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ženichův	ženichův	k2eAgMnSc1d1	ženichův
otec	otec	k1gMnSc1	otec
–	–	k?	–
František	František	k1gMnSc1	František
I.	I.	kA	I.
na	na	k7c4	na
infarkt	infarkt	k1gInSc4	infarkt
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
náhlé	náhlý	k2eAgFnSc6d1	náhlá
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc2	svůj
milovaného	milovaný	k2eAgMnSc2d1	milovaný
muže	muž	k1gMnSc2	muž
se	se	k3xPyFc4	se
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc1	Terezie
dozvěděla	dozvědět	k5eAaPmAgFnS	dozvědět
tentýž	týž	k3xTgInSc4	týž
den	den	k1gInSc4	den
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
nejstaršího	starý	k2eAgMnSc2d3	nejstarší
syna	syn	k1gMnSc2	syn
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
smrtí	smrt	k1gFnSc7	smrt
se	se	k3xPyFc4	se
nedokázala	dokázat	k5eNaPmAgFnS	dokázat
vyrovnat	vyrovnat	k5eAaBmF	vyrovnat
po	po	k7c4	po
zbytek	zbytek	k1gInSc4	zbytek
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
;	;	kIx,	;
od	od	k7c2	od
dalšího	další	k2eAgInSc2d1	další
dne	den	k1gInSc2	den
chodila	chodit	k5eAaImAgFnS	chodit
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
černém	černý	k2eAgInSc6d1	černý
a	a	k8xC	a
své	svůj	k3xOyFgInPc4	svůj
šperky	šperk	k1gInPc4	šperk
a	a	k8xC	a
jiné	jiný	k2eAgNnSc4d1	jiné
oblečení	oblečení	k1gNnSc4	oblečení
rozdala	rozdat	k5eAaPmAgFnS	rozdat
svým	svůj	k3xOyFgFnPc3	svůj
dcerám	dcera	k1gFnPc3	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Ostatky	ostatek	k1gInPc1	ostatek
Františka	František	k1gMnSc2	František
I.	I.	kA	I.
byly	být	k5eAaImAgFnP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
do	do	k7c2	do
rodinné	rodinný	k2eAgFnSc2d1	rodinná
kapucínské	kapucínský	k2eAgFnSc2d1	Kapucínská
krypty	krypta	k1gFnSc2	krypta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
pochována	pochovat	k5eAaPmNgFnS	pochovat
i	i	k9	i
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
I.	I.	kA	I.
po	po	k7c6	po
sobě	se	k3xPyFc3	se
zanechal	zanechat	k5eAaPmAgMnS	zanechat
obrovských	obrovský	k2eAgFnPc2d1	obrovská
18	[number]	k4	18
miliónů	milión	k4xCgInPc2	milión
zlatých	zlatá	k1gFnPc2	zlatá
a	a	k8xC	a
dvě	dva	k4xCgNnPc4	dva
sídla	sídlo	k1gNnPc4	sídlo
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
12	[number]	k4	12
miliónů	milión	k4xCgInPc2	milión
umořila	umořit	k5eAaPmAgFnS	umořit
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
státní	státní	k2eAgInSc4d1	státní
dluh	dluh	k1gInSc4	dluh
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc4	zbytek
si	se	k3xPyFc3	se
nechala	nechat	k5eAaPmAgFnS	nechat
pro	pro	k7c4	pro
zaopatření	zaopatření	k1gNnSc4	zaopatření
početné	početný	k2eAgFnSc2d1	početná
rodiny	rodina	k1gFnSc2	rodina
(	(	kIx(	(
<g/>
11	[number]	k4	11
dětí	dítě	k1gFnPc2	dítě
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
stal	stát	k5eAaPmAgMnS	stát
automaticky	automaticky	k6eAd1	automaticky
římskoněmeckým	římskoněmecký	k2eAgMnSc7d1	římskoněmecký
císařem	císař	k1gMnSc7	císař
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
římskoněmeckým	římskoněmecký	k2eAgMnSc7d1	římskoněmecký
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
<g/>
,	,	kIx,	,
dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1765	[number]	k4	1765
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc1	Terezie
jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
Josefa	Josef	k1gMnSc4	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
jako	jako	k8xC	jako
spoluvládce	spoluvládce	k1gMnSc2	spoluvládce
habsburských	habsburský	k2eAgFnPc2d1	habsburská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
docházelo	docházet	k5eAaImAgNnS	docházet
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
k	k	k7c3	k
značným	značný	k2eAgInPc3d1	značný
rozporům	rozpor	k1gInPc3	rozpor
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
vládnout	vládnout	k5eAaImF	vládnout
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
matčiny	matčin	k2eAgFnSc2d1	matčina
pomoci	pomoc	k1gFnSc2	pomoc
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
si	se	k3xPyFc3	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
užíval	užívat	k5eAaImAgMnS	užívat
nevázaného	vázaný	k2eNgInSc2d1	nevázaný
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
hledal	hledat	k5eAaImAgMnS	hledat
nevěstu	nevěsta	k1gFnSc4	nevěsta
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
by	by	kYmCp3nP	by
zajistil	zajistit	k5eAaPmAgMnS	zajistit
mužského	mužský	k2eAgMnSc4d1	mužský
potomka	potomek	k1gMnSc4	potomek
a	a	k8xC	a
následníka	následník	k1gMnSc4	následník
<g/>
,	,	kIx,	,
raději	rád	k6eAd2	rád
běhal	běhat	k5eAaImAgMnS	běhat
za	za	k7c7	za
"	"	kIx"	"
<g/>
lehkými	lehký	k2eAgFnPc7d1	lehká
ženami	žena	k1gFnPc7	žena
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
od	od	k7c2	od
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
dostal	dostat	k5eAaPmAgMnS	dostat
kapavku	kapavka	k1gFnSc4	kapavka
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
udává	udávat	k5eAaImIp3nS	udávat
literatura	literatura	k1gFnSc1	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
čím	co	k3yQnSc7	co
dál	daleko	k6eAd2	daleko
více	hodně	k6eAd2	hodně
ztotožňoval	ztotožňovat	k5eAaImAgMnS	ztotožňovat
s	s	k7c7	s
úhlavním	úhlavní	k2eAgMnSc7d1	úhlavní
nepřítelem	nepřítel	k1gMnSc7	nepřítel
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
Fridrichem	Fridrich	k1gMnSc7	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ba	ba	k9	ba
co	co	k9	co
víc	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
o	o	k7c4	o
schůzku	schůzka	k1gFnSc4	schůzka
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yIgFnSc3	který
nakonec	nakonec	k6eAd1	nakonec
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
vlády	vláda	k1gFnSc2	vláda
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
došlo	dojít	k5eAaPmAgNnS	dojít
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1779	[number]	k4	1779
a	a	k8xC	a
1780	[number]	k4	1780
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
mu	on	k3xPp3gMnSc3	on
také	také	k6eAd1	také
vyčítala	vyčítat	k5eAaImAgFnS	vyčítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
neudrží	udržet	k5eNaPmIp3nS	udržet
žádného	žádný	k3yNgMnSc4	žádný
přítele	přítel	k1gMnSc4	přítel
a	a	k8xC	a
jediný	jediný	k2eAgMnSc1d1	jediný
<g/>
,	,	kIx,	,
s	s	k7c7	s
kým	kdo	k3yInSc7	kdo
si	se	k3xPyFc3	se
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
rozumí	rozumět	k5eAaImIp3nS	rozumět
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
úhlavní	úhlavní	k2eAgMnSc1d1	úhlavní
nepřítel	nepřítel	k1gMnSc1	nepřítel
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
němuž	jenž	k3xRgNnSc3	jenž
přišla	přijít	k5eAaPmAgFnS	přijít
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
o	o	k7c4	o
Slezsko	Slezsko	k1gNnSc4	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnPc1d1	poslední
léta	léto	k1gNnPc1	léto
svého	svůj	k1gMnSc2	svůj
života	život	k1gInSc2	život
tak	tak	k8xS	tak
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
prožila	prožít	k5eAaPmAgFnS	prožít
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
stresu	stres	k1gInSc6	stres
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc4d3	veliký
ránu	rána	k1gFnSc4	rána
ji	on	k3xPp3gFnSc4	on
způsobila	způsobit	k5eAaPmAgFnS	způsobit
smrt	smrt	k1gFnSc1	smrt
jejího	její	k3xOp3gMnSc2	její
manžela	manžel	k1gMnSc2	manžel
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
věčné	věčný	k2eAgInPc1d1	věčný
rozpory	rozpor	k1gInPc1	rozpor
se	s	k7c7	s
synem	syn	k1gMnSc7	syn
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
5	[number]	k4	5
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1772	[number]	k4	1772
byla	být	k5eAaImAgFnS	být
donucena	donucen	k2eAgFnSc1d1	donucena
souhlasit	souhlasit	k5eAaImF	souhlasit
s	s	k7c7	s
rozdělením	rozdělení	k1gNnSc7	rozdělení
Polska	Polsko	k1gNnSc2	Polsko
mezi	mezi	k7c4	mezi
Prusko	Prusko	k1gNnSc4	Prusko
a	a	k8xC	a
Rusko	Rusko	k1gNnSc4	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Kdyby	kdyby	kYmCp3nS	kdyby
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
postavila	postavit	k5eAaPmAgFnS	postavit
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
vše	všechen	k3xTgNnSc1	všechen
by	by	kYmCp3nS	by
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
válce	válka	k1gFnSc3	válka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
by	by	kYmCp3nS	by
osamocené	osamocený	k2eAgNnSc1d1	osamocené
Rakousko	Rakousko	k1gNnSc1	Rakousko
jistě	jistě	k6eAd1	jistě
prohrálo	prohrát	k5eAaPmAgNnS	prohrát
<g/>
.	.	kIx.	.
</s>
<s>
Rakousko	Rakousko	k1gNnSc1	Rakousko
sice	sice	k8xC	sice
získalo	získat	k5eAaPmAgNnS	získat
70	[number]	k4	70
000	[number]	k4	000
čtverečných	čtverečný	k2eAgInPc2d1	čtverečný
kilometrů	kilometr	k1gInPc2	kilometr
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gMnSc1	jeho
nový	nový	k2eAgMnSc1d1	nový
nepřítel	nepřítel	k1gMnSc1	nepřítel
Rusko	Rusko	k1gNnSc1	Rusko
se	se	k3xPyFc4	se
rozrostlo	rozrůst	k5eAaPmAgNnS	rozrůst
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
tohoto	tento	k3xDgNnSc2	tento
dělení	dělení	k1gNnSc2	dělení
ještě	ještě	k9	ještě
blíž	blízce	k6eAd2	blízce
k	k	k7c3	k
rakouským	rakouský	k2eAgFnPc3d1	rakouská
hranicím	hranice	k1gFnPc3	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
hladomoru	hladomor	k1gInSc2	hladomor
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
v	v	k7c6	v
letech	let	k1gInPc6	let
1771	[number]	k4	1771
<g/>
–	–	k?	–
<g/>
1772	[number]	k4	1772
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
nejméně	málo	k6eAd3	málo
250	[number]	k4	250
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
rozsáhlým	rozsáhlý	k2eAgInPc3d1	rozsáhlý
nevolnickým	nevolnický	k2eAgInPc3d1	nevolnický
nepokojům	nepokoj	k1gInPc3	nepokoj
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1778	[number]	k4	1778
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
Válka	válka	k1gFnSc1	válka
o	o	k7c4	o
bavorské	bavorský	k2eAgNnSc4d1	bavorské
dědictví	dědictví	k1gNnSc4	dědictví
a	a	k8xC	a
opět	opět	k6eAd1	opět
ji	on	k3xPp3gFnSc4	on
zahájil	zahájit	k5eAaPmAgMnS	zahájit
Fridrich	Fridrich	k1gMnSc1	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
vpádem	vpád	k1gInSc7	vpád
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
byla	být	k5eAaImAgFnS	být
ovšem	ovšem	k9	ovšem
velmi	velmi	k6eAd1	velmi
rychlá	rychlý	k2eAgFnSc1d1	rychlá
a	a	k8xC	a
neodehrály	odehrát	k5eNaPmAgInP	odehrát
se	se	k3xPyFc4	se
během	během	k7c2	během
ní	on	k3xPp3gFnSc2	on
žádné	žádný	k3yNgFnSc2	žádný
velké	velký	k2eAgFnSc2d1	velká
bitvy	bitva	k1gFnSc2	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Mír	mír	k1gInSc1	mír
byl	být	k5eAaImAgInS	být
sepsán	sepsat	k5eAaPmNgInS	sepsat
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1779	[number]	k4	1779
a	a	k8xC	a
dostal	dostat	k5eAaPmAgMnS	dostat
označení	označení	k1gNnSc4	označení
těšínský	těšínský	k2eAgMnSc1d1	těšínský
<g/>
.	.	kIx.	.
</s>
<s>
Rakousko	Rakousko	k1gNnSc1	Rakousko
si	se	k3xPyFc3	se
mohlo	moct	k5eAaImAgNnS	moct
ponechat	ponechat	k5eAaPmF	ponechat
bavorskou	bavorský	k2eAgFnSc4d1	bavorská
Innskou	Innský	k2eAgFnSc4d1	Innský
čtvrť	čtvrť	k1gFnSc4	čtvrť
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc4	zbytek
si	se	k3xPyFc3	se
vzalo	vzít	k5eAaPmAgNnS	vzít
Prusko	Prusko	k1gNnSc1	Prusko
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
mír	mír	k1gInSc1	mír
byl	být	k5eAaImAgInS	být
poslední	poslední	k2eAgFnSc7d1	poslední
velkou	velký	k2eAgFnSc7d1	velká
politickou	politický	k2eAgFnSc7d1	politická
akcí	akce	k1gFnSc7	akce
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc1	Terezie
stihla	stihnout	k5eAaPmAgFnS	stihnout
za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
udělat	udělat	k5eAaPmF	udělat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
byl	být	k5eAaImAgInS	být
diagnostikován	diagnostikován	k2eAgInSc1d1	diagnostikován
vysoký	vysoký	k2eAgInSc1d1	vysoký
krevní	krevní	k2eAgInSc1d1	krevní
tlak	tlak	k1gInSc1	tlak
a	a	k8xC	a
pulmonální	pulmonální	k2eAgFnSc1d1	pulmonální
neprůchodnost	neprůchodnost	k1gFnSc1	neprůchodnost
s	s	k7c7	s
rozedmou	rozedma	k1gFnSc7	rozedma
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1780	[number]	k4	1780
se	se	k3xPyFc4	se
nachladila	nachladit	k5eAaPmAgFnS	nachladit
na	na	k7c6	na
lovu	lov	k1gInSc6	lov
bažantů	bažant	k1gMnPc2	bažant
a	a	k8xC	a
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1780	[number]	k4	1780
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Pitva	pitva	k1gFnSc1	pitva
ukázala	ukázat	k5eAaPmAgFnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jí	on	k3xPp3gFnSc3	on
nefungovala	fungovat	k5eNaImAgFnS	fungovat
jedna	jeden	k4xCgFnSc1	jeden
plíce	plíce	k1gFnSc1	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
pochována	pochovat	k5eAaPmNgFnS	pochovat
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
vychovatelka	vychovatelka	k1gFnSc1	vychovatelka
Charlotta	Charlotta	k1gFnSc1	Charlotta
Fuchsová	Fuchsová	k1gFnSc1	Fuchsová
<g/>
,	,	kIx,	,
v	v	k7c6	v
rodinné	rodinný	k2eAgFnSc6d1	rodinná
kapucínské	kapucínský	k2eAgFnSc6d1	Kapucínská
kryptě	krypta	k1gFnSc6	krypta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
tehdejším	tehdejší	k2eAgNnSc6d1	tehdejší
Rakousku	Rakousko	k1gNnSc6	Rakousko
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
bohoslužby	bohoslužba	k1gFnPc1	bohoslužba
a	a	k8xC	a
i	i	k9	i
její	její	k3xOp3gMnSc1	její
celoživotní	celoživotní	k2eAgMnSc1d1	celoživotní
úhlavní	úhlavní	k2eAgMnSc1d1	úhlavní
nepřítel	nepřítel	k1gMnSc1	nepřítel
Fridrich	Fridrich	k1gMnSc1	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
jí	on	k3xPp3gFnSc7	on
vzdal	vzdát	k5eAaPmAgMnS	vzdát
čest	čest	k1gFnSc1	čest
<g/>
.	.	kIx.	.
</s>
<s>
Zanechala	zanechat	k5eAaPmAgFnS	zanechat
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c4	na
Slezsko	Slezsko	k1gNnSc4	Slezsko
a	a	k8xC	a
získanou	získaný	k2eAgFnSc4d1	získaná
část	část	k1gFnSc4	část
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
<g/>
,	,	kIx,	,
neporušené	porušený	k2eNgNnSc1d1	neporušené
Rakousko	Rakousko	k1gNnSc1	Rakousko
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
vlády	vláda	k1gFnSc2	vláda
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
sedm	sedm	k4xCc4	sedm
potomků	potomek	k1gMnPc2	potomek
na	na	k7c6	na
sedmi	sedm	k4xCc6	sedm
evropských	evropský	k2eAgInPc6d1	evropský
trůnech	trůn	k1gInPc6	trůn
<g/>
.	.	kIx.	.
královna	královna	k1gFnSc1	královna
uherská	uherský	k2eAgFnSc1d1	uherská
1740	[number]	k4	1740
<g/>
–	–	k?	–
<g/>
1780	[number]	k4	1780
(	(	kIx(	(
<g/>
korunována	korunován	k2eAgFnSc1d1	korunována
roku	rok	k1gInSc2	rok
1741	[number]	k4	1741
v	v	k7c6	v
Prešpurku	Prešpurku	k?	Prešpurku
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
<g />
.	.	kIx.	.
</s>
<s>
))	))	k?	))
královna	královna	k1gFnSc1	královna
česká	český	k2eAgFnSc1d1	Česká
1740	[number]	k4	1740
<g/>
–	–	k?	–
<g/>
1780	[number]	k4	1780
(	(	kIx(	(
<g/>
korunována	korunován	k2eAgFnSc1d1	korunována
roku	rok	k1gInSc2	rok
1743	[number]	k4	1743
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
arcivévodkyně	arcivévodkyně	k1gFnSc1	arcivévodkyně
rakouská	rakouský	k2eAgFnSc1d1	rakouská
1740	[number]	k4	1740
<g/>
–	–	k?	–
<g/>
1780	[number]	k4	1780
vévodkyně	vévodkyně	k1gFnSc1	vévodkyně
parmská	parmský	k2eAgFnSc1d1	parmská
1740	[number]	k4	1740
<g/>
–	–	k?	–
<g/>
1748	[number]	k4	1748
plus	plus	k1gInSc1	plus
v	v	k7c6	v
desítkách	desítka	k1gFnPc6	desítka
dalších	další	k2eAgInPc2d1	další
malých	malý	k2eAgInPc2d1	malý
územních	územní	k2eAgInPc2d1	územní
celků	celek	k1gInPc2	celek
V	v	k7c6	v
letech	let	k1gInPc6	let
1740	[number]	k4	1740
<g/>
–	–	k?	–
<g/>
1780	[number]	k4	1780
vládla	vládnout	k5eAaImAgFnS	vládnout
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc1	Terezie
jako	jako	k8xS	jako
arcivévodkyně	arcivévodkyně	k1gFnSc1	arcivévodkyně
rakouská	rakouský	k2eAgFnSc1d1	rakouská
a	a	k8xC	a
královna	královna	k1gFnSc1	královna
uherská	uherský	k2eAgFnSc1d1	uherská
a	a	k8xC	a
česká	český	k2eAgFnSc1d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
panování	panování	k1gNnSc1	panování
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
Pragmatickou	pragmatický	k2eAgFnSc7d1	pragmatická
sankcí	sankce	k1gFnSc7	sankce
císaře	císař	k1gMnSc2	císař
Karla	Karel	k1gMnSc2	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
který	který	k3yIgInSc4	který
neměl	mít	k5eNaImAgMnS	mít
mužského	mužský	k2eAgMnSc4d1	mužský
potomka	potomek	k1gMnSc4	potomek
<g/>
)	)	kIx)	)
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
"	"	kIx"	"
<g/>
vládkyně	vládkyně	k1gFnSc1	vládkyně
habsburské	habsburský	k2eAgFnSc2d1	habsburská
říše	říš	k1gFnSc2	říš
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
jako	jako	k9	jako
"	"	kIx"	"
<g/>
císařovny	císařovna	k1gFnPc4	císařovna
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
označována	označovat	k5eAaImNgFnS	označovat
<g/>
.	.	kIx.	.
</s>
<s>
Tou	ten	k3xDgFnSc7	ten
totiž	totiž	k9	totiž
nebyla	být	k5eNaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
nebyla	být	k5eNaImAgFnS	být
takto	takto	k6eAd1	takto
korunována	korunovat	k5eAaBmNgFnS	korunovat
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
svého	svůj	k3xOyFgMnSc4	svůj
manžela	manžel	k1gMnSc4	manžel
do	do	k7c2	do
Frankfurtu	Frankfurt	k1gInSc2	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
korunovaci	korunovace	k1gFnSc3	korunovace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1745	[number]	k4	1745
doprovodila	doprovodit	k5eAaPmAgFnS	doprovodit
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
titul	titul	k1gInSc1	titul
císařovny	císařovna	k1gFnSc2	císařovna
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
třeba	třeba	k6eAd1	třeba
považovat	považovat	k5eAaImF	považovat
v	v	k7c6	v
letech	let	k1gInPc6	let
1745	[number]	k4	1745
<g/>
–	–	k?	–
<g/>
1765	[number]	k4	1765
za	za	k7c4	za
titul	titul	k1gInSc4	titul
"	"	kIx"	"
<g/>
císařovny-manželky	císařovnyanželka	k1gFnSc2	císařovny-manželka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
jako	jako	k9	jako
"	"	kIx"	"
<g/>
císařovny-matky	císařovnyatek	k1gInPc4	císařovny-matek
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
jejího	její	k3xOp3gMnSc2	její
manžela	manžel	k1gMnSc2	manžel
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
císařem	císař	k1gMnSc7	císař
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
jejich	jejich	k3xOp3gNnPc4	jejich
syn	syn	k1gMnSc1	syn
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
v	v	k7c6	v
užívání	užívání	k1gNnSc6	užívání
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
titulů	titul	k1gInPc2	titul
názorně	názorně	k6eAd1	názorně
ilustruje	ilustrovat	k5eAaBmIp3nS	ilustrovat
vývoj	vývoj	k1gInSc1	vývoj
mincovnictví	mincovnictví	k1gNnSc2	mincovnictví
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
její	její	k3xOp3gFnSc2	její
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mincích	mince	k1gFnPc6	mince
a	a	k8xC	a
medailích	medaile	k1gFnPc6	medaile
s	s	k7c7	s
podobiznou	podobizna	k1gFnSc7	podobizna
panovnice	panovnice	k1gFnSc2	panovnice
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
nápis	nápis	k1gInSc1	nápis
"	"	kIx"	"
<g/>
M	M	kA	M
THERESIA	THERESIA	kA	THERESIA
D	D	kA	D
G	G	kA	G
R	R	kA	R
IMP	IMP	kA	IMP
HU	hu	k0	hu
BO	BO	k?	BO
REG	REG	kA	REG
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
=	=	kIx~	=
Maria	Maria	k1gFnSc1	Maria
Theresia	Theresia	k1gFnSc1	Theresia
<g/>
,	,	kIx,	,
Dei	Dei	k1gFnSc1	Dei
Gratia	Gratia	k1gFnSc1	Gratia
Romanorum	Romanorum	k1gInSc4	Romanorum
Imperatrix	Imperatrix	k1gInSc1	Imperatrix
<g/>
,	,	kIx,	,
Hungariae	Hungariae	k1gFnSc1	Hungariae
Bohemiaeque	Bohemiaeque	k1gFnSc1	Bohemiaeque
Regina	Regina	k1gFnSc1	Regina
=	=	kIx~	=
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
z	z	k7c2	z
Boží	boží	k2eAgFnSc2d1	boží
vůle	vůle	k1gFnSc2	vůle
římská	římský	k2eAgFnSc1d1	římská
císařovna	císařovna	k1gFnSc1	císařovna
<g/>
,	,	kIx,	,
uherská	uherský	k2eAgFnSc1d1	uherská
a	a	k8xC	a
česká	český	k2eAgFnSc1d1	Česká
královna	královna	k1gFnSc1	královna
<g/>
)	)	kIx)	)
až	až	k9	až
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1745	[number]	k4	1745
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
František	František	k1gMnSc1	František
Lotrinský	lotrinský	k2eAgMnSc1d1	lotrinský
zvolen	zvolen	k2eAgMnSc1d1	zvolen
císařem	císař	k1gMnSc7	císař
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
císaře	císař	k1gMnSc2	císař
Karla	Karel	k1gMnSc2	Karel
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Albrechta	Albrecht	k1gMnSc4	Albrecht
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
,	,	kIx,	,
zřídil	zřídit	k5eAaPmAgMnS	zřídit
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
císařskou	císařský	k2eAgFnSc4d1	císařská
mincovnu	mincovna	k1gFnSc4	mincovna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mincmistrem	mincmistr	k1gMnSc7	mincmistr
byl	být	k5eAaImAgMnS	být
Matthäus	Matthäus	k1gMnSc1	Matthäus
Donner	Donner	k1gMnSc1	Donner
(	(	kIx(	(
<g/>
bratr	bratr	k1gMnSc1	bratr
sochaře	sochař	k1gMnSc2	sochař
Georga	Georg	k1gMnSc2	Georg
Raphaela	Raphael	k1gMnSc2	Raphael
Donnera	Donner	k1gMnSc2	Donner
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
býval	bývat	k5eAaImAgInS	bývat
na	na	k7c6	na
spodním	spodní	k2eAgInSc6d1	spodní
okraji	okraj	k1gInSc6	okraj
mincí	mince	k1gFnPc2	mince
podepsán	podepsat	k5eAaPmNgInS	podepsat
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
zkratkou	zkratka	k1gFnSc7	zkratka
MD	MD	kA	MD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Donner	Donner	k1gMnSc1	Donner
vytvářel	vytvářet	k5eAaImAgMnS	vytvářet
a	a	k8xC	a
razil	razit	k5eAaImAgMnS	razit
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
císaře	císař	k1gMnSc2	císař
mince	mince	k1gFnSc2	mince
(	(	kIx(	(
<g/>
české	český	k2eAgInPc1d1	český
tolary	tolar	k1gInPc1	tolar
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yRgFnPc2	který
později	pozdě	k6eAd2	pozdě
převzal	převzít	k5eAaPmAgInS	převzít
jméno	jméno	k1gNnSc4	jméno
americký	americký	k2eAgInSc1d1	americký
dolar	dolar	k1gInSc1	dolar
<g/>
,	,	kIx,	,
krejcary	krejcar	k1gInPc1	krejcar
<g/>
,	,	kIx,	,
dukáty	dukát	k1gInPc1	dukát
<g/>
,	,	kIx,	,
feniky	fenik	k1gInPc1	fenik
<g/>
,	,	kIx,	,
liardy	liard	k1gInPc1	liard
<g/>
.	.	kIx.	.
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
medaile	medaile	k1gFnPc4	medaile
z	z	k7c2	z
milosti	milost	k1gFnSc2	milost
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
panovník	panovník	k1gMnSc1	panovník
uděloval	udělovat	k5eAaImAgMnS	udělovat
za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
významným	významný	k2eAgFnPc3d1	významná
osobnostem	osobnost	k1gFnPc3	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Vyjadřovaly	vyjadřovat	k5eAaImAgFnP	vyjadřovat
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
poctu	pocta	k1gFnSc4	pocta
od	od	k7c2	od
"	"	kIx"	"
<g/>
milostivého	milostivý	k2eAgMnSc2d1	milostivý
<g/>
"	"	kIx"	"
panovníka	panovník	k1gMnSc2	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Tereziánské	tereziánský	k2eAgFnSc2d1	Tereziánská
reformy	reforma	k1gFnSc2	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
panování	panování	k1gNnSc4	panování
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
byly	být	k5eAaImAgFnP	být
provedeny	provést	k5eAaPmNgFnP	provést
osvícenské	osvícenský	k2eAgFnPc1d1	osvícenská
reformy	reforma	k1gFnPc1	reforma
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
ně	on	k3xPp3gInPc4	on
pak	pak	k6eAd1	pak
navázaly	navázat	k5eAaPmAgFnP	navázat
josefinské	josefinský	k2eAgFnPc1d1	josefinská
reformy	reforma	k1gFnPc1	reforma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
správní	správní	k2eAgFnSc2d1	správní
reformy	reforma	k1gFnSc2	reforma
byla	být	k5eAaImAgFnS	být
omezena	omezit	k5eAaPmNgFnS	omezit
zemská	zemský	k2eAgFnSc1d1	zemská
samospráva	samospráva	k1gFnSc1	samospráva
<g/>
,	,	kIx,	,
vytvořena	vytvořen	k2eAgFnSc1d1	vytvořena
centralizovaná	centralizovaný	k2eAgFnSc1d1	centralizovaná
správa	správa	k1gFnSc1	správa
monarchie	monarchie	k1gFnSc2	monarchie
a	a	k8xC	a
státní	státní	k2eAgFnSc1d1	státní
správa	správa	k1gFnSc1	správa
byla	být	k5eAaImAgFnS	být
profesionalizována	profesionalizovat	k5eAaImNgFnS	profesionalizovat
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
sjednoceno	sjednotit	k5eAaPmNgNnS	sjednotit
trestní	trestní	k2eAgNnSc1d1	trestní
právo	právo	k1gNnSc1	právo
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
a	a	k8xC	a
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zákoníkem	zákoník	k1gInSc7	zákoník
zvaným	zvaný	k2eAgInSc7d1	zvaný
Constitutio	Constitutio	k1gNnSc4	Constitutio
Criminalis	Criminalis	k1gFnPc2	Criminalis
Theresiana	Theresian	k1gMnSc2	Theresian
(	(	kIx(	(
<g/>
vešel	vejít	k5eAaPmAgInS	vejít
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
roku	rok	k1gInSc2	rok
1768	[number]	k4	1768
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zákoník	zákoník	k1gInSc1	zákoník
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
mnohém	mnohé	k1gNnSc6	mnohé
stále	stále	k6eAd1	stále
poplatný	poplatný	k2eAgInSc4d1	poplatný
právní	právní	k2eAgInSc4d1	právní
soustavě	soustava	k1gFnSc6	soustava
středověku	středověk	k1gInSc6	středověk
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
důkazní	důkazní	k2eAgInSc1d1	důkazní
prostředek	prostředek	k1gInSc1	prostředek
připouštěl	připouštět	k5eAaImAgInS	připouštět
například	například	k6eAd1	například
mučení	mučení	k1gNnSc4	mučení
a	a	k8xC	a
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
ještě	ještě	k6eAd1	ještě
skutkové	skutkový	k2eAgFnPc4d1	skutková
podstaty	podstata	k1gFnPc4	podstata
trestných	trestný	k2eAgInPc2d1	trestný
činů	čin	k1gInPc2	čin
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
čarodějnictví	čarodějnictví	k1gNnSc1	čarodějnictví
nebo	nebo	k8xC	nebo
magie	magie	k1gFnSc1	magie
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
vytvořeny	vytvořit	k5eAaPmNgInP	vytvořit
tereziánské	tereziánský	k2eAgInPc1d1	tereziánský
katastry	katastr	k1gInPc1	katastr
<g/>
,	,	kIx,	,
soupisy	soupis	k1gInPc1	soupis
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
domů	dům	k1gInPc2	dům
a	a	k8xC	a
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Tereziánský	tereziánský	k2eAgInSc1d1	tereziánský
urbář	urbář	k1gInSc1	urbář
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1767	[number]	k4	1767
definoval	definovat	k5eAaBmAgInS	definovat
vztahy	vztah	k1gInPc4	vztah
mezi	mezi	k7c7	mezi
vlastníky	vlastník	k1gMnPc7	vlastník
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc7	jejich
poddanými	poddaná	k1gFnPc7	poddaná
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
narazilo	narazit	k5eAaPmAgNnS	narazit
na	na	k7c4	na
ostrý	ostrý	k2eAgInSc4d1	ostrý
odpor	odpor	k1gInSc4	odpor
uherské	uherský	k2eAgFnSc2d1	uherská
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Poddanská	poddanský	k2eAgFnSc1d1	poddanská
a	a	k8xC	a
pozemková	pozemkový	k2eAgFnSc1d1	pozemková
reforma	reforma	k1gFnSc1	reforma
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
a	a	k8xC	a
v	v	k7c6	v
několika	několik	k4yIc6	několik
zemích	zem	k1gFnPc6	zem
(	(	kIx(	(
<g/>
Čechy	Čech	k1gMnPc4	Čech
<g/>
,	,	kIx,	,
Dolní	dolní	k2eAgInPc1d1	dolní
Rakousy	Rakousy	k1gInPc1	Rakousy
<g/>
,	,	kIx,	,
Morava	Morava	k1gFnSc1	Morava
<g/>
,	,	kIx,	,
Štýrsko	Štýrsko	k1gNnSc1	Štýrsko
a	a	k8xC	a
Halič	Halič	k1gFnSc1	Halič
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
provedl	provést	k5eAaPmAgMnS	provést
František	František	k1gMnSc1	František
Antonín	Antonín	k1gMnSc1	Antonín
Raab	Raab	k1gMnSc1	Raab
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
raabizace	raabizace	k1gFnSc1	raabizace
(	(	kIx(	(
<g/>
aboliční	aboliční	k2eAgFnSc1d1	aboliční
raabovská	raabovský	k2eAgFnSc1d1	raabovský
soustava	soustava	k1gFnSc1	soustava
<g/>
,	,	kIx,	,
raabovský	raabovský	k2eAgInSc1d1	raabovský
robotní	robotní	k2eAgInSc1d1	robotní
svod	svod	k1gInSc1	svod
nebo	nebo	k8xC	nebo
raabovský	raabovský	k2eAgInSc1d1	raabovský
emfyteut	emfyteut	k1gInSc1	emfyteut
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
vrchnostenské	vrchnostenský	k2eAgFnSc2d1	vrchnostenská
půdy	půda	k1gFnSc2	půda
byla	být	k5eAaImAgFnS	být
za	za	k7c4	za
poplatek	poplatek	k1gInSc4	poplatek
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
mezi	mezi	k7c4	mezi
rolníky	rolník	k1gMnPc4	rolník
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
pilotního	pilotní	k2eAgInSc2d1	pilotní
projektu	projekt	k1gInSc2	projekt
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1775	[number]	k4	1775
rozdělena	rozdělen	k2eAgFnSc1d1	rozdělena
část	část	k1gFnSc1	část
půdy	půda	k1gFnSc2	půda
po	po	k7c6	po
zrušeném	zrušený	k2eAgInSc6d1	zrušený
jezuitském	jezuitský	k2eAgInSc6d1	jezuitský
řádu	řád	k1gInSc6	řád
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1775	[number]	k4	1775
vydala	vydat	k5eAaPmAgFnS	vydat
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
poslední	poslední	k2eAgFnSc2d1	poslední
robotní	robotní	k2eAgInSc4d1	robotní
patent	patent	k1gInSc4	patent
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zmírnil	zmírnit	k5eAaPmAgInS	zmírnit
robotní	robotní	k2eAgFnSc4d1	robotní
povinnost	povinnost	k1gFnSc4	povinnost
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
raabovské	raabovský	k2eAgFnSc2d1	raabovský
reformy	reforma	k1gFnSc2	reforma
zastavil	zastavit	k5eAaPmAgMnS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
podporoval	podporovat	k5eAaImAgInS	podporovat
podnikání	podnikání	k1gNnSc4	podnikání
(	(	kIx(	(
<g/>
textilní	textilní	k2eAgFnSc2d1	textilní
a	a	k8xC	a
sklářské	sklářský	k2eAgFnSc2d1	sklářská
manufaktury	manufaktura	k1gFnSc2	manufaktura
<g/>
,	,	kIx,	,
papírny	papírna	k1gFnSc2	papírna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zavedl	zavést	k5eAaPmAgInS	zavést
celní	celní	k2eAgFnSc4d1	celní
unii	unie	k1gFnSc4	unie
a	a	k8xC	a
cla	clo	k1gNnPc4	clo
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
před	před	k7c7	před
zahraniční	zahraniční	k2eAgFnSc7d1	zahraniční
konkurencí	konkurence	k1gFnSc7	konkurence
<g/>
.	.	kIx.	.
</s>
<s>
Modernizovalo	modernizovat	k5eAaBmAgNnS	modernizovat
se	se	k3xPyFc4	se
obdělávání	obdělávání	k1gNnSc1	obdělávání
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
prosadily	prosadit	k5eAaPmAgFnP	prosadit
se	se	k3xPyFc4	se
brambory	brambora	k1gFnPc1	brambora
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
jednotná	jednotný	k2eAgFnSc1d1	jednotná
měna	měna	k1gFnSc1	měna
(	(	kIx(	(
<g/>
1	[number]	k4	1
tolar	tolar	k1gInSc1	tolar
=	=	kIx~	=
2	[number]	k4	2
zlaté	zlatý	k1gInPc4	zlatý
<g/>
)	)	kIx)	)
a	a	k8xC	a
první	první	k4xOgInPc4	první
papírové	papírový	k2eAgInPc4d1	papírový
peníze	peníz	k1gInPc4	peníz
(	(	kIx(	(
<g/>
bankocetle	bankocetle	k1gFnPc4	bankocetle
<g/>
)	)	kIx)	)
a	a	k8xC	a
sjednoceny	sjednocen	k2eAgFnPc4d1	sjednocena
míry	míra	k1gFnPc4	míra
a	a	k8xC	a
váhy	váha	k1gFnPc4	váha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1751	[number]	k4	1751
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
ohňový	ohňový	k2eAgInSc1d1	ohňový
patent	patent	k1gInSc1	patent
<g/>
,	,	kIx,	,
stanovující	stanovující	k2eAgFnPc1d1	stanovující
průlomové	průlomový	k2eAgFnPc1d1	průlomová
zásady	zásada	k1gFnPc1	zásada
požární	požární	k2eAgFnSc2d1	požární
ochrany	ochrana	k1gFnSc2	ochrana
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
stavbu	stavba	k1gFnSc4	stavba
domů	dům	k1gInPc2	dům
z	z	k7c2	z
nehořlavých	hořlavý	k2eNgInPc2d1	nehořlavý
materiálů	materiál	k1gInPc2	materiál
a	a	k8xC	a
vybavení	vybavení	k1gNnSc2	vybavení
vesnic	vesnice	k1gFnPc2	vesnice
zvoničkami	zvonička	k1gFnPc7	zvonička
s	s	k7c7	s
nepřetržitou	přetržitý	k2eNgFnSc7d1	nepřetržitá
vartýřskou	vartýřský	k2eAgFnSc7d1	vartýřský
službou	služba	k1gFnSc7	služba
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1773	[number]	k4	1773
byl	být	k5eAaImAgInS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
jezuitský	jezuitský	k2eAgInSc1d1	jezuitský
řád	řád	k1gInSc1	řád
<g/>
,	,	kIx,	,
jezuité	jezuita	k1gMnPc1	jezuita
tím	ten	k3xDgNnSc7	ten
přišli	přijít	k5eAaPmAgMnP	přijít
i	i	k8xC	i
o	o	k7c4	o
dohled	dohled	k1gInSc4	dohled
nad	nad	k7c7	nad
školstvím	školství	k1gNnSc7	školství
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
řízení	řízení	k1gNnSc3	řízení
reformy	reforma	k1gFnSc2	reforma
školství	školství	k1gNnSc2	školství
byl	být	k5eAaImAgInS	být
pozván	pozván	k2eAgInSc1d1	pozván
Johann	Johann	k1gInSc1	Johann
Ignaz	Ignaz	k1gInSc1	Ignaz
Felbiger	Felbiger	k1gInSc1	Felbiger
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1774	[number]	k4	1774
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
Všeobecný	všeobecný	k2eAgInSc1d1	všeobecný
školní	školní	k2eAgInSc1d1	školní
řád	řád	k1gInSc1	řád
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
císařsko-královské	císařskorálovský	k2eAgFnPc4d1	císařsko-královská
země	zem	k1gFnPc4	zem
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
stanovil	stanovit	k5eAaPmAgInS	stanovit
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
všeobecnou	všeobecný	k2eAgFnSc4d1	všeobecná
vzdělávací	vzdělávací	k2eAgFnSc4d1	vzdělávací
povinnost	povinnost	k1gFnSc4	povinnost
pro	pro	k7c4	pro
chlapce	chlapec	k1gMnPc4	chlapec
i	i	k8xC	i
dívky	dívka	k1gFnPc4	dívka
(	(	kIx(	(
<g/>
nikoli	nikoli	k9	nikoli
povinnou	povinný	k2eAgFnSc4d1	povinná
školní	školní	k2eAgFnSc4d1	školní
docházku	docházka	k1gFnSc4	docházka
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
povinná	povinný	k2eAgFnSc1d1	povinná
šestiletá	šestiletý	k2eAgFnSc1d1	šestiletá
školní	školní	k2eAgFnSc1d1	školní
docházka	docházka	k1gFnSc1	docházka
a	a	k8xC	a
systém	systém	k1gInSc1	systém
škol	škola	k1gFnPc2	škola
<g/>
:	:	kIx,	:
triviální	triviální	k2eAgFnSc2d1	triviální
školy	škola	k1gFnSc2	škola
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
i	i	k8xC	i
ve	v	k7c6	v
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
při	při	k7c6	při
farách	fara	k1gFnPc6	fara
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
výuku	výuka	k1gFnSc4	výuka
čtení	čtení	k1gNnSc2	čtení
<g/>
,	,	kIx,	,
psaní	psaní	k1gNnSc2	psaní
a	a	k8xC	a
počtů	počet	k1gInPc2	počet
<g/>
,	,	kIx,	,
výuku	výuka	k1gFnSc4	výuka
náboženství	náboženství	k1gNnSc2	náboženství
a	a	k8xC	a
základů	základ	k1gInPc2	základ
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
dívky	dívka	k1gFnPc4	dívka
též	též	k9	též
domácích	domácí	k2eAgFnPc2d1	domácí
prací	práce	k1gFnPc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
obsah	obsah	k1gInSc4	obsah
výuky	výuka	k1gFnSc2	výuka
dohlížel	dohlížet	k5eAaImAgMnS	dohlížet
farář	farář	k1gMnSc1	farář
<g/>
,	,	kIx,	,
na	na	k7c4	na
hospodaření	hospodaření	k1gNnSc4	hospodaření
školy	škola	k1gFnSc2	škola
školní	školní	k2eAgMnPc4d1	školní
dozorce	dozorce	k1gMnPc4	dozorce
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgInS	být
obvykle	obvykle	k6eAd1	obvykle
nejbohatší	bohatý	k2eAgInSc1d3	nejbohatší
sedlák	sedlák	k1gInSc1	sedlák
z	z	k7c2	z
vesnice	vesnice	k1gFnSc2	vesnice
<g/>
.	.	kIx.	.
hlavní	hlavní	k2eAgFnSc2d1	hlavní
školy	škola	k1gFnSc2	škola
ve	v	k7c6	v
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
alespoň	alespoň	k9	alespoň
jedna	jeden	k4xCgFnSc1	jeden
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
základních	základní	k2eAgFnPc2d1	základní
znalostí	znalost	k1gFnPc2	znalost
se	se	k3xPyFc4	se
učila	učit	k5eAaImAgFnS	učit
též	též	k9	též
německá	německý	k2eAgFnSc1d1	německá
gramatika	gramatika	k1gFnSc1	gramatika
a	a	k8xC	a
latina	latina	k1gFnSc1	latina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
hlavních	hlavní	k2eAgFnPc6d1	hlavní
školách	škola	k1gFnPc6	škola
se	se	k3xPyFc4	se
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
ročníku	ročník	k1gInSc6	ročník
učilo	učit	k5eAaImAgNnS	učit
česky	česky	k6eAd1	česky
<g/>
,	,	kIx,	,
ve	v	k7c6	v
druhém	druhý	k4xOgMnSc6	druhý
částečně	částečně	k6eAd1	částečně
česky	česky	k6eAd1	česky
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
německy	německy	k6eAd1	německy
<g/>
.	.	kIx.	.
normální	normální	k2eAgFnSc2d1	normální
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byly	být	k5eAaImAgFnP	být
učitelské	učitelský	k2eAgInPc4d1	učitelský
ústavy	ústav	k1gInPc4	ústav
v	v	k7c6	v
zemských	zemský	k2eAgNnPc6d1	zemské
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
s	s	k7c7	s
výukou	výuka	k1gFnSc7	výuka
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
<g/>
.	.	kIx.	.
preparandy	preparanda	k1gFnSc2	preparanda
<g/>
,	,	kIx,	,
přípravné	přípravný	k2eAgInPc4d1	přípravný
kursy	kurs	k1gInPc4	kurs
pro	pro	k7c4	pro
učitele	učitel	k1gMnPc4	učitel
<g/>
,	,	kIx,	,
trvaly	trvat	k5eAaImAgFnP	trvat
při	při	k7c6	při
normálních	normální	k2eAgFnPc6d1	normální
školách	škola	k1gFnPc6	škola
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
při	při	k7c6	při
hlavních	hlavní	k2eAgFnPc6d1	hlavní
školách	škola	k1gFnPc6	škola
3	[number]	k4	3
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Dozor	dozor	k1gInSc4	dozor
nad	nad	k7c7	nad
hlavními	hlavní	k2eAgFnPc7d1	hlavní
a	a	k8xC	a
normálními	normální	k2eAgFnPc7d1	normální
školami	škola	k1gFnPc7	škola
měly	mít	k5eAaImAgFnP	mít
zemské	zemský	k2eAgFnPc4d1	zemská
školní	školní	k2eAgFnPc4d1	školní
komise	komise	k1gFnPc4	komise
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1775	[number]	k4	1775
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
reorganizace	reorganizace	k1gFnSc1	reorganizace
středních	střední	k2eAgFnPc2d1	střední
škol	škola	k1gFnPc2	škola
podle	podle	k7c2	podle
Gratiana	Gratian	k1gMnSc2	Gratian
Marxe	Marx	k1gMnSc2	Marx
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
pětitřídní	pětitřídní	k2eAgFnSc1d1	pětitřídní
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
šestitřídní	šestitřídní	k2eAgNnPc1d1	šestitřídní
<g/>
)	)	kIx)	)
gymnázia	gymnázium	k1gNnPc1	gymnázium
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
učilo	učít	k5eAaPmAgNnS	učít
německy	německy	k6eAd1	německy
<g/>
,	,	kIx,	,
od	od	k7c2	od
3	[number]	k4	3
<g/>
.	.	kIx.	.
třídy	třída	k1gFnPc4	třída
latinsky	latinsky	k6eAd1	latinsky
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1751	[number]	k4	1751
založila	založit	k5eAaPmAgFnS	založit
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
akademii	akademie	k1gFnSc4	akademie
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
osobně	osobně	k6eAd1	osobně
vyšívala	vyšívat	k5eAaImAgFnS	vyšívat
jedny	jeden	k4xCgFnPc4	jeden
ze	z	k7c2	z
šatiček	šatičky	k1gFnPc2	šatičky
pro	pro	k7c4	pro
Pražské	pražský	k2eAgNnSc4d1	Pražské
Jezulátko	Jezulátko	k1gNnSc4	Jezulátko
–	–	k?	–
zelené	zelená	k1gFnSc2	zelená
se	s	k7c7	s
zlatou	zlatý	k2eAgFnSc7d1	zlatá
výšivkou	výšivka	k1gFnSc7	výšivka
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgMnPc4d3	nejvýznamnější
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
manželství	manželství	k1gNnSc2	manželství
s	s	k7c7	s
císařem	císař	k1gMnSc7	císař
Františkem	František	k1gMnSc7	František
I.	I.	kA	I.
Štěpánem	Štěpán	k1gMnSc7	Štěpán
vzešlo	vzejít	k5eAaPmAgNnS	vzejít
16	[number]	k4	16
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
5	[number]	k4	5
synů	syn	k1gMnPc2	syn
a	a	k8xC	a
11	[number]	k4	11
dcer	dcera	k1gFnPc2	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Šest	šest	k4xCc1	šest
dětí	dítě	k1gFnPc2	dítě
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
ještě	ještě	k6eAd1	ještě
za	za	k7c2	za
života	život	k1gInSc2	život
jejich	jejich	k3xOp3gFnSc2	jejich
matky	matka	k1gFnSc2	matka
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Alžběta	Alžběta	k1gFnSc1	Alžběta
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Karolína	Karolína	k1gFnSc1	Karolína
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Karolína	Karolína	k1gFnSc1	Karolína
<g/>
,	,	kIx,	,
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
Johanna	Johanen	k2eAgFnSc1d1	Johanna
Gabriela	Gabriela	k1gFnSc1	Gabriela
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Josefa	Josef	k1gMnSc2	Josef
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc1	tři
dcery	dcera	k1gFnPc1	dcera
se	se	k3xPyFc4	se
jmenovaly	jmenovat	k5eAaImAgFnP	jmenovat
Marie	Marie	k1gFnSc1	Marie
Karolína	Karolína	k1gFnSc1	Karolína
(	(	kIx(	(
<g/>
č.	č.	k?	č.
3	[number]	k4	3
<g/>
,	,	kIx,	,
10	[number]	k4	10
a	a	k8xC	a
13	[number]	k4	13
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
dcery	dcera	k1gFnPc1	dcera
se	se	k3xPyFc4	se
jmenovaly	jmenovat	k5eAaBmAgFnP	jmenovat
Marie	Marie	k1gFnSc1	Marie
Alžběta	Alžběta	k1gFnSc1	Alžběta
(	(	kIx(	(
<g/>
č.	č.	k?	č.
1	[number]	k4	1
a	a	k8xC	a
6	[number]	k4	6
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgMnPc1	dva
synové	syn	k1gMnPc1	syn
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
císaři	císař	k1gMnPc1	císař
(	(	kIx(	(
<g/>
č.	č.	k?	č.
4	[number]	k4	4
a	a	k8xC	a
9	[number]	k4	9
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Alžběta	Alžběta	k1gFnSc1	Alžběta
(	(	kIx(	(
<g/>
1737	[number]	k4	1737
<g/>
–	–	k?	–
<g/>
1740	[number]	k4	1740
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Anna	Anna	k1gFnSc1	Anna
(	(	kIx(	(
<g/>
1738	[number]	k4	1738
<g/>
–	–	k?	–
<g/>
1789	[number]	k4	1789
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žila	žít	k5eAaImAgFnS	žít
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
Klagenfurtu	Klagenfurt	k1gInSc6	Klagenfurt
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Karolína	Karolína	k1gFnSc1	Karolína
(	(	kIx(	(
<g/>
1740	[number]	k4	1740
<g/>
–	–	k?	–
<g/>
1741	[number]	k4	1741
<g/>
)	)	kIx)	)
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1741	[number]	k4	1741
<g/>
–	–	k?	–
<g/>
1790	[number]	k4	1790
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
∞	∞	k?	∞
1760	[number]	k4	1760
princezna	princezna	k1gFnSc1	princezna
Isabela	Isabela	k1gFnSc1	Isabela
Bourbonsko-Parmská	bourbonskoarmský	k2eAgFnSc1d1	bourbonsko-parmský
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
Filipa	Filip	k1gMnSc2	Filip
Parmského	parmský	k2eAgMnSc2d1	parmský
vévody	vévoda	k1gMnSc2	vévoda
z	z	k7c2	z
Parmy	Parma	k1gFnSc2	Parma
<g/>
,	,	kIx,	,
Piacenzy	Piacenza	k1gFnSc2	Piacenza
<g/>
,	,	kIx,	,
Guastelly	Guastella	k1gFnSc2	Guastella
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
∞	∞	k?	∞
1765	[number]	k4	1765
princezna	princezna	k1gFnSc1	princezna
Marie	Marie	k1gFnSc1	Marie
Josefa	Josefa	k1gFnSc1	Josefa
Bavorská	bavorský	k2eAgFnSc1d1	bavorská
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
císaře	císař	k1gMnSc2	císař
Karla	Karel	k1gMnSc2	Karel
VII	VII	kA	VII
<g/>
.	.	kIx.	.
a	a	k8xC	a
Marie	Marie	k1gFnSc1	Marie
Amálie	Amálie	k1gFnSc2	Amálie
Habsburské	habsburský	k2eAgFnSc2d1	habsburská
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Kristina	Kristina	k1gFnSc1	Kristina
(	(	kIx(	(
<g/>
1742	[number]	k4	1742
<g/>
–	–	k?	–
<g/>
1798	[number]	k4	1798
<g/>
)	)	kIx)	)
∞	∞	k?	∞
1766	[number]	k4	1766
vévoda	vévoda	k1gMnSc1	vévoda
Albert	Albert	k1gMnSc1	Albert
Kazimír	Kazimír	k1gMnSc1	Kazimír
Sasko-Těšínský	saskoěšínský	k2eAgMnSc1d1	sasko-těšínský
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
polského	polský	k2eAgMnSc2d1	polský
krále	král	k1gMnSc2	král
Augusta	August	k1gMnSc2	August
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
Marie	Marie	k1gFnSc1	Marie
Josefy	Josefa	k1gFnSc2	Josefa
Habsburské	habsburský	k2eAgFnSc2d1	habsburská
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Alžběta	Alžběta	k1gFnSc1	Alžběta
(	(	kIx(	(
<g/>
1743	[number]	k4	1743
<g/>
–	–	k?	–
<g/>
1808	[number]	k4	1808
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
abatyše	abatyše	k1gFnSc1	abatyše
v	v	k7c6	v
Innsbrucku	Innsbruck	k1gInSc6	Innsbruck
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Josef	Josef	k1gMnSc1	Josef
(	(	kIx(	(
<g/>
1745	[number]	k4	1745
<g/>
–	–	k?	–
<g/>
1761	[number]	k4	1761
<g/>
)	)	kIx)	)
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Amálie	Amálie	k1gFnSc1	Amálie
(	(	kIx(	(
<g/>
1746	[number]	k4	1746
<g/>
–	–	k?	–
<g/>
1804	[number]	k4	1804
<g/>
)	)	kIx)	)
∞	∞	k?	∞
1769	[number]	k4	1769
vévoda	vévoda	k1gMnSc1	vévoda
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Parmský	parmský	k2eAgMnSc1d1	parmský
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Filipa	Filip	k1gMnSc2	Filip
Parmského	parmský	k2eAgMnSc2d1	parmský
vévody	vévoda	k1gMnSc2	vévoda
z	z	k7c2	z
Parmy	Parma	k1gFnSc2	Parma
<g/>
,	,	kIx,	,
Piacenzy	Piacenza	k1gFnSc2	Piacenza
<g/>
,	,	kIx,	,
Guastelly	Guastella	k1gFnSc2	Guastella
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Leopold	Leopold	k1gMnSc1	Leopold
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1747	[number]	k4	1747
<g/>
–	–	k?	–
<g/>
1792	[number]	k4	1792
<g/>
)	)	kIx)	)
∞	∞	k?	∞
1765	[number]	k4	1765
infantka	infantka	k1gFnSc1	infantka
Marie	Marie	k1gFnSc1	Marie
Ludovika	Ludovika	k1gFnSc1	Ludovika
Španělská	španělský	k2eAgFnSc1d1	španělská
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Bourbon-Anjou	Bourbon-Anjá	k1gFnSc4	Bourbon-Anjá
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
krále	král	k1gMnSc2	král
Karla	Karel	k1gMnSc2	Karel
III	III	kA	III
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Karolína	Karolína	k1gFnSc1	Karolína
(	(	kIx(	(
<g/>
1748	[number]	k4	1748
<g/>
–	–	k?	–
<g/>
1748	[number]	k4	1748
<g/>
)	)	kIx)	)
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
Johanna	Johanen	k2eAgFnSc1d1	Johanna
Gabriela	Gabriela	k1gFnSc1	Gabriela
(	(	kIx(	(
<g/>
1750	[number]	k4	1750
<g/>
–	–	k?	–
<g/>
1762	[number]	k4	1762
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
zasnoubena	zasnouben	k2eAgFnSc1d1	zasnoubena
<g/>
)	)	kIx)	)
král	král	k1gMnSc1	král
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
Neapolsko-Sicilský	neapolskoicilský	k2eAgInSc1d1	neapolsko-sicilský
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Josefa	Josef	k1gMnSc2	Josef
(	(	kIx(	(
<g/>
1751	[number]	k4	1751
<g/>
–	–	k?	–
<g/>
1767	[number]	k4	1767
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
zasnoubena	zasnouben	k2eAgFnSc1d1	zasnoubena
<g/>
)	)	kIx)	)
král	král	k1gMnSc1	král
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
Neapolsko-Sicilský	neapolskoicilský	k2eAgInSc1d1	neapolsko-sicilský
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Karolína	Karolína	k1gFnSc1	Karolína
(	(	kIx(	(
<g/>
1752	[number]	k4	1752
<g/>
–	–	k?	–
<g/>
1814	[number]	k4	1814
<g/>
)	)	kIx)	)
∞	∞	k?	∞
1768	[number]	k4	1768
král	král	k1gMnSc1	král
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
Neapolsko-Sicilský	neapolskoicilský	k2eAgMnSc1d1	neapolsko-sicilský
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
španělského	španělský	k2eAgMnSc2d1	španělský
krále	král	k1gMnSc2	král
Karla	Karel	k1gMnSc2	Karel
III	III	kA	III
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Karel	Karel	k1gMnSc1	Karel
(	(	kIx(	(
<g/>
1754	[number]	k4	1754
<g/>
–	–	k?	–
<g/>
1806	[number]	k4	1806
<g/>
)	)	kIx)	)
∞	∞	k?	∞
1771	[number]	k4	1771
vévodkyně	vévodkyně	k1gFnSc1	vévodkyně
Marie	Marie	k1gFnSc1	Marie
Beatrice	Beatrice	k1gFnSc1	Beatrice
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Este	Est	k1gMnSc5	Est
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
vévody	vévoda	k1gMnSc2	vévoda
Herkula	Herkul	k1gMnSc2	Herkul
III	III	kA	III
<g/>
.	.	kIx.	.
z	z	k7c2	z
Modeny-d	Modenya	k1gFnPc2	Modeny-da
<g/>
'	'	kIx"	'
<g/>
Este	Est	k1gMnSc5	Est
15	[number]	k4	15
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Antoinetta	Antoinetta	k1gFnSc1	Antoinetta
(	(	kIx(	(
<g/>
Marie	Marie	k1gFnSc1	Marie
Antonie	Antonie	k1gFnSc2	Antonie
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1755	[number]	k4	1755
<g/>
–	–	k?	–
<g/>
1793	[number]	k4	1793
<g/>
)	)	kIx)	)
∞	∞	k?	∞
1770	[number]	k4	1770
Ludvík	Ludvík	k1gMnSc1	Ludvík
(	(	kIx(	(
<g/>
1754	[number]	k4	1754
<g/>
–	–	k?	–
<g/>
1793	[number]	k4	1793
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dauphin	dauphin	k1gMnSc1	dauphin
<g/>
,	,	kIx,	,
od	od	k7c2	od
1774	[number]	k4	1774
jako	jako	k8xS	jako
Ludvík	Ludvík	k1gMnSc1	Ludvík
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
král	král	k1gMnSc1	král
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
dauphina	dauphin	k1gMnSc2	dauphin
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Bourbonského	bourbonský	k2eAgNnSc2d1	Bourbonské
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
<s>
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
František	František	k1gMnSc1	František
(	(	kIx(	(
<g/>
1756	[number]	k4	1756
<g/>
–	–	k?	–
<g/>
1801	[number]	k4	1801
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
<g/>
,	,	kIx,	,
kurfiřt	kurfiřt	k1gMnSc1	kurfiřt
kolínský	kolínský	k2eAgMnSc1d1	kolínský
číslování	číslování	k1gNnSc1	číslování
domů	dům	k1gInPc2	dům
školství	školství	k1gNnSc2	školství
ulice	ulice	k1gFnPc4	ulice
Za	za	k7c2	za
jejího	její	k3xOp3gNnSc2	její
panování	panování	k1gNnSc2	panování
byly	být	k5eAaImAgInP	být
očíslovány	očíslován	k2eAgInPc1d1	očíslován
domy	dům	k1gInPc1	dům
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
domy	dům	k1gInPc7	dům
značily	značit	k5eAaImAgInP	značit
názvy	název	k1gInPc1	název
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Dům	dům	k1gInSc1	dům
U	u	k7c2	u
dvou	dva	k4xCgInPc2	dva
slunců	slunec	k1gInPc2	slunec
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
Malé	Malé	k2eAgFnSc6d1	Malé
Straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
nejstarší	starý	k2eAgNnSc1d3	nejstarší
číslování	číslování	k1gNnSc1	číslování
provedeno	provést	k5eAaPmNgNnS	provést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1770	[number]	k4	1770
<g/>
.	.	kIx.	.
</s>
