<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
papež	papež	k1gMnSc1	papež
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
mír	mír	k1gInSc4	mír
se	s	k7c7	s
španělským	španělský	k2eAgMnSc7d1	španělský
králem	král	k1gMnSc7	král
Filipem	Filip	k1gMnSc7	Filip
II.	II.	kA	II.
v	v	k7c6	v
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
1557	[number]	k4	1557
<g/>
?	?	kIx.	?
</s>
