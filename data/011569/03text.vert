<p>
<s>
Mír	mír	k1gInSc1	mír
dohodnutý	dohodnutý	k2eAgInSc1d1	dohodnutý
mezi	mezi	k7c7	mezi
papežem	papež	k1gMnSc7	papež
Pavlem	Pavel	k1gMnSc7	Pavel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
a	a	k8xC	a
španělským	španělský	k2eAgMnSc7d1	španělský
králem	král	k1gMnSc7	král
Filipem	Filip	k1gMnSc7	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
1557	[number]	k4	1557
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
mírem	mír	k1gInSc7	mír
skončil	skončit	k5eAaPmAgMnS	skončit
poslední	poslední	k2eAgInSc4d1	poslední
konflikt	konflikt	k1gInSc4	konflikt
mezi	mezi	k7c7	mezi
Římem	Řím	k1gInSc7	Řím
a	a	k8xC	a
Španělskem	Španělsko	k1gNnSc7	Španělsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Konflikt	konflikt	k1gInSc1	konflikt
papeže	papež	k1gMnSc2	papež
se	s	k7c7	s
Španělskem	Španělsko	k1gNnSc7	Španělsko
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
papeže	papež	k1gMnSc2	papež
Pavla	Pavel	k1gMnSc2	Pavel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
zhoršily	zhoršit	k5eAaPmAgInP	zhoršit
vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
Církevním	církevní	k2eAgInSc7d1	církevní
státem	stát	k1gInSc7	stát
a	a	k8xC	a
španělskou	španělský	k2eAgFnSc7d1	španělská
korunou	koruna	k1gFnSc7	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Nemalou	malý	k2eNgFnSc4d1	nemalá
<g/>
,	,	kIx,	,
ba	ba	k9	ba
přímo	přímo	k6eAd1	přímo
zásadní	zásadní	k2eAgFnSc7d1	zásadní
měrou	míra	k1gFnSc7wR	míra
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
přispěla	přispět	k5eAaPmAgFnS	přispět
samotná	samotný	k2eAgFnSc1d1	samotná
komplikovaná	komplikovaný	k2eAgFnSc1d1	komplikovaná
osobnost	osobnost	k1gFnSc1	osobnost
papeže	papež	k1gMnSc2	papež
Pavla	Pavel	k1gMnSc2	Pavel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nenáviděl	návidět	k5eNaImAgInS	návidět
Habsburky	Habsburk	k1gInPc4	Habsburk
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
míry	míra	k1gFnSc2	míra
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
Filipu	Filip	k1gMnSc3	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
neváhal	váhat	k5eNaImAgMnS	váhat
spojit	spojit	k5eAaPmF	spojit
ani	ani	k8xC	ani
s	s	k7c7	s
francouzským	francouzský	k2eAgMnSc7d1	francouzský
králem	král	k1gMnSc7	král
Jindřichem	Jindřich	k1gMnSc7	Jindřich
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
konfliktu	konflikt	k1gInSc6	konflikt
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
se	se	k3xPyFc4	se
Filip	Filip	k1gMnSc1	Filip
sice	sice	k8xC	sice
nemohl	moct	k5eNaImAgMnS	moct
spolehnout	spolehnout	k5eAaPmF	spolehnout
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
své	svůj	k3xOyFgFnSc2	svůj
manželky	manželka	k1gFnSc2	manželka
<g/>
,	,	kIx,	,
anglické	anglický	k2eAgFnSc2d1	anglická
královny	královna	k1gFnSc2	královna
Marie	Maria	k1gFnSc2	Maria
I.	I.	kA	I.
Krvavé	krvavý	k2eAgFnPc1d1	krvavá
<g/>
,	,	kIx,	,
i	i	k9	i
tak	tak	k9	tak
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
ale	ale	k9	ale
podařilo	podařit	k5eAaPmAgNnS	podařit
zvítězit	zvítězit	k5eAaPmF	zvítězit
nad	nad	k7c7	nad
Francouzi	Francouz	k1gMnPc7	Francouz
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Saint-Quentin	Saint-Quentina	k1gFnPc2	Saint-Quentina
dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1557	[number]	k4	1557
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyhrané	vyhraný	k2eAgFnSc6d1	vyhraná
válce	válka	k1gFnSc6	válka
s	s	k7c7	s
Francouzi	Francouz	k1gMnPc7	Francouz
(	(	kIx(	(
<g/>
kteří	který	k3yQgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
i	i	k9	i
tak	tak	k6eAd1	tak
dost	dost	k6eAd1	dost
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
v	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
roce	rok	k1gInSc6	rok
porazili	porazit	k5eAaPmAgMnP	porazit
Angličany	Angličan	k1gMnPc4	Angličan
a	a	k8xC	a
uzmuli	uzmout	k5eAaPmAgMnP	uzmout
jim	on	k3xPp3gMnPc3	on
přístav	přístav	k1gInSc1	přístav
Calais	Calais	k1gNnSc4	Calais
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgFnSc4d1	poslední
baštu	bašta	k1gFnSc4	bašta
z	z	k7c2	z
kdysi	kdysi	k6eAd1	kdysi
mocného	mocný	k2eAgNnSc2d1	mocné
Anjouovského	Anjouovský	k2eAgNnSc2d1	Anjouovský
impéria	impérium	k1gNnSc2	impérium
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
Filip	Filip	k1gMnSc1	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
vypořádat	vypořádat	k5eAaPmF	vypořádat
se	se	k3xPyFc4	se
i	i	k9	i
s	s	k7c7	s
mocným	mocný	k2eAgMnSc7d1	mocný
francouzským	francouzský	k2eAgMnSc7d1	francouzský
spojencem	spojenec	k1gMnSc7	spojenec
<g/>
,	,	kIx,	,
papežem	papež	k1gMnSc7	papež
Pavlem	Pavel	k1gMnSc7	Pavel
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
</s>
<s>
Nic	nic	k3yNnSc1	nic
papeži	papež	k1gMnSc3	papež
nepomohla	pomoct	k5eNaPmAgFnS	pomoct
snaha	snaha	k1gFnSc1	snaha
exkomunikovat	exkomunikovat	k5eAaBmF	exkomunikovat
císaře	císař	k1gMnSc2	císař
Karla	Karel	k1gMnSc2	Karel
V.	V.	kA	V.
i	i	k8xC	i
jeho	on	k3xPp3gMnSc2	on
syna	syn	k1gMnSc2	syn
Filipa	Filip	k1gMnSc2	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
na	na	k7c6	na
konci	konec	k1gInSc6	konec
léta	léto	k1gNnSc2	léto
1557	[number]	k4	1557
ocitl	ocitnout	k5eAaPmAgInS	ocitnout
Řím	Řím	k1gInSc1	Řím
v	v	k7c6	v
obležení	obležení	k1gNnSc6	obležení
španělského	španělský	k2eAgNnSc2d1	španělské
vojska	vojsko	k1gNnSc2	vojsko
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
generálem	generál	k1gMnSc7	generál
Albou	alba	k1gFnSc7	alba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c4	v
září	září	k1gNnSc4	září
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
však	však	k9	však
Filip	Filip	k1gMnSc1	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
papeži	papež	k1gMnSc3	papež
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
napjaté	napjatý	k2eAgInPc4d1	napjatý
vztahy	vztah	k1gInPc4	vztah
ukončil	ukončit	k5eAaPmAgMnS	ukončit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgMnS	být
posledním	poslední	k2eAgMnSc7d1	poslední
papežem	papež	k1gMnSc7	papež
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
odvážil	odvážit	k5eAaPmAgMnS	odvážit
vojensky	vojensky	k6eAd1	vojensky
vystoupit	vystoupit	k5eAaPmF	vystoupit
proti	proti	k7c3	proti
Španělsku	Španělsko	k1gNnSc3	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc4d1	poslední
ozbrojený	ozbrojený	k2eAgInSc4d1	ozbrojený
konflikt	konflikt	k1gInSc4	konflikt
mezi	mezi	k7c7	mezi
papežem	papež	k1gMnSc7	papež
a	a	k8xC	a
císařem	císař	k1gMnSc7	císař
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
odehrál	odehrát	k5eAaPmAgMnS	odehrát
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
1708	[number]	k4	1708
a	a	k8xC	a
1709	[number]	k4	1709
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Miroslav	Miroslav	k1gMnSc1	Miroslav
Hroch	Hroch	k1gMnSc1	Hroch
<g/>
,	,	kIx,	,
Anna	Anna	k1gFnSc1	Anna
Skýbová	Skýbová	k1gFnSc1	Skýbová
-	-	kIx~	-
Králové	Král	k1gMnPc1	Král
<g/>
,	,	kIx,	,
kacíři	kacíř	k1gMnPc1	kacíř
<g/>
,	,	kIx,	,
inkvizitoři	inkvizitor	k1gMnPc1	inkvizitor
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1987	[number]	k4	1987
</s>
</p>
<p>
<s>
Vít	Vít	k1gMnSc1	Vít
Vlnas	Vlnas	k1gMnSc1	Vlnas
-	-	kIx~	-
Princ	princ	k1gMnSc1	princ
Evžen	Evžen	k1gMnSc1	Evžen
Savojský	savojský	k2eAgMnSc1d1	savojský
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
a	a	k8xC	a
sláva	sláva	k1gFnSc1	sláva
barokního	barokní	k2eAgMnSc2d1	barokní
válečníka	válečník	k1gMnSc2	válečník
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
2001	[number]	k4	2001
</s>
</p>
