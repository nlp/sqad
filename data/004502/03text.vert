<s>
Eurotunel	Eurotunel	k1gInSc1	Eurotunel
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Channel	Channel	k1gMnSc1	Channel
Tunnel	Tunnel	k1gMnSc1	Tunnel
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
používaný	používaný	k2eAgInSc4d1	používaný
výraz	výraz	k1gInSc4	výraz
pro	pro	k7c4	pro
podmořský	podmořský	k2eAgInSc4d1	podmořský
tunel	tunel	k1gInSc4	tunel
pod	pod	k7c7	pod
Lamanšským	lamanšský	k2eAgInSc7d1	lamanšský
průlivem	průliv	k1gInSc7	průliv
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k2eAgMnPc1d1	vedoucí
mezi	mezi	k7c7	mezi
anglickým	anglický	k2eAgInSc7d1	anglický
Folkestone	Folkeston	k1gInSc5	Folkeston
a	a	k8xC	a
francouzským	francouzský	k2eAgNnSc7d1	francouzské
Calais	Calais	k1gNnSc7	Calais
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgMnSc1d1	dlouhý
přes	přes	k7c4	přes
50	[number]	k4	50
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Tunel	tunel	k1gInSc1	tunel
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1994	[number]	k4	1994
přímé	přímý	k2eAgNnSc4d1	přímé
železniční	železniční	k2eAgNnSc4d1	železniční
spojení	spojení	k1gNnSc4	spojení
mezi	mezi	k7c7	mezi
Londýnem	Londýn	k1gInSc7	Londýn
a	a	k8xC	a
kontinentální	kontinentální	k2eAgFnSc7d1	kontinentální
Evropou	Evropa	k1gFnSc7	Evropa
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
provozován	provozovat	k5eAaImNgInS	provozovat
společností	společnost	k1gFnSc7	společnost
Eurotunnel	Eurotunnela	k1gFnPc2	Eurotunnela
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
projekt	projekt	k1gInSc1	projekt
byl	být	k5eAaImAgInS	být
zorganizován	zorganizovat	k5eAaPmNgInS	zorganizovat
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
i	i	k9	i
taky	taky	k6eAd1	taky
financován	financovat	k5eAaBmNgInS	financovat
společností	společnost	k1gFnSc7	společnost
Eurotunnel	Eurotunnela	k1gFnPc2	Eurotunnela
Group	Group	k1gInSc1	Group
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
hodnotu	hodnota	k1gFnSc4	hodnota
5,5	[number]	k4	5,5
miliard	miliarda	k4xCgFnPc2	miliarda
USD	USD	kA	USD
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ročně	ročně	k6eAd1	ročně
vydělává	vydělávat	k5eAaImIp3nS	vydělávat
v	v	k7c6	v
cca	cca	kA	cca
kolem	kolem	k7c2	kolem
50	[number]	k4	50
-	-	kIx~	-
60	[number]	k4	60
milionů	milion	k4xCgInPc2	milion
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Plány	plán	k1gInPc1	plán
spojit	spojit	k5eAaPmF	spojit
Anglii	Anglie	k1gFnSc4	Anglie
a	a	k8xC	a
Francii	Francie	k1gFnSc6	Francie
tunelem	tunel	k1gInSc7	tunel
jsou	být	k5eAaImIp3nP	být
staré	starý	k2eAgFnPc4d1	stará
již	již	k6eAd1	již
několik	několik	k4yIc4	několik
set	sto	k4xCgNnPc2	sto
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Patřily	patřit	k5eAaImAgInP	patřit
ale	ale	k9	ale
vždy	vždy	k6eAd1	vždy
do	do	k7c2	do
skupin	skupina	k1gFnPc2	skupina
velmi	velmi	k6eAd1	velmi
odvážných	odvážný	k2eAgInPc2d1	odvážný
až	až	k8xS	až
bláznivých	bláznivý	k2eAgInPc2d1	bláznivý
plánů	plán	k1gInPc2	plán
<g/>
,	,	kIx,	,
kterými	který	k3yRgInPc7	který
chtěli	chtít	k5eAaImAgMnP	chtít
lidé	člověk	k1gMnPc1	člověk
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
upoutat	upoutat	k5eAaPmF	upoutat
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
však	však	k9	však
byl	být	k5eAaImAgInS	být
projekt	projekt	k1gInSc1	projekt
vzat	vzít	k5eAaPmNgInS	vzít
seriózně	seriózně	k6eAd1	seriózně
<g/>
,	,	kIx,	,
založena	založit	k5eAaPmNgFnS	založit
byla	být	k5eAaImAgFnS	být
společnost	společnost	k1gFnSc1	společnost
Eurotunnel	Eurotunnel	k1gInSc1	Eurotunnel
a	a	k8xC	a
začalo	začít	k5eAaPmAgNnS	začít
se	se	k3xPyFc4	se
kopat	kopat	k5eAaImF	kopat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
důvodů	důvod	k1gInPc2	důvod
se	se	k3xPyFc4	se
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
tunel	tunel	k1gInSc1	tunel
železniční	železniční	k2eAgInPc1d1	železniční
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
vyraženy	vyrazit	k5eAaPmNgInP	vyrazit
dva	dva	k4xCgInPc1	dva
provozní	provozní	k2eAgInPc1d1	provozní
tunely	tunel	k1gInPc1	tunel
a	a	k8xC	a
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
jeden	jeden	k4xCgInSc1	jeden
servisní	servisní	k2eAgMnSc1d1	servisní
<g/>
.	.	kIx.	.
</s>
<s>
Otevření	otevření	k1gNnSc1	otevření
tunelu	tunel	k1gInSc2	tunel
pro	pro	k7c4	pro
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
dopravu	doprava	k1gFnSc4	doprava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
se	se	k3xPyFc4	se
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
královna	královna	k1gFnSc1	královna
Alžběta	Alžběta	k1gFnSc1	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
francouzský	francouzský	k2eAgMnSc1d1	francouzský
prezident	prezident	k1gMnSc1	prezident
François	François	k1gFnSc2	François
Mitterrand	Mitterrand	k1gInSc1	Mitterrand
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
tunel	tunel	k1gInSc1	tunel
zachvátil	zachvátit	k5eAaPmAgInS	zachvátit
požár	požár	k1gInSc4	požár
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
jeho	jeho	k3xOp3gFnSc4	jeho
pověst	pověst	k1gFnSc4	pověst
poškodil	poškodit	k5eAaPmAgInS	poškodit
<g/>
,	,	kIx,	,
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
stále	stále	k6eAd1	stále
velmi	velmi	k6eAd1	velmi
důležitým	důležitý	k2eAgInSc7d1	důležitý
dopravním	dopravní	k2eAgInSc7d1	dopravní
prostředkem	prostředek	k1gInSc7	prostředek
mezi	mezi	k7c7	mezi
kdysi	kdysi	k6eAd1	kdysi
znepřátelenými	znepřátelený	k2eAgFnPc7d1	znepřátelená
zeměmi	zem	k1gFnPc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2006	[number]	k4	2006
společnost	společnost	k1gFnSc4	společnost
provozující	provozující	k2eAgInSc4d1	provozující
Eurotunel	Eurotunel	k1gInSc4	Eurotunel
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
bankrot	bankrot	k1gInSc4	bankrot
a	a	k8xC	a
požádala	požádat	k5eAaPmAgFnS	požádat
o	o	k7c4	o
ochranu	ochrana	k1gFnSc4	ochrana
před	před	k7c7	před
věřiteli	věřitel	k1gMnPc7	věřitel
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
že	že	k8xS	že
zisky	zisk	k1gInPc1	zisk
tunelu	tunel	k1gInSc2	tunel
nepokrývají	pokrývat	k5eNaImIp3nP	pokrývat
splácení	splácení	k1gNnSc4	splácení
dluhů	dluh	k1gInPc2	dluh
za	za	k7c4	za
jeho	jeho	k3xOp3gFnSc4	jeho
stavbu	stavba	k1gFnSc4	stavba
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
přibližně	přibližně	k6eAd1	přibližně
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
později	pozdě	k6eAd2	pozdě
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2007	[number]	k4	2007
schválil	schválit	k5eAaPmAgInS	schválit
francouzský	francouzský	k2eAgInSc4d1	francouzský
soud	soud	k1gInSc4	soud
restrukturalizaci	restrukturalizace	k1gFnSc4	restrukturalizace
firmy	firma	k1gFnSc2	firma
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
setkalo	setkat	k5eAaPmAgNnS	setkat
s	s	k7c7	s
nevolí	nevole	k1gFnSc7	nevole
britských	britský	k2eAgMnPc2d1	britský
věřitelů	věřitel	k1gMnPc2	věřitel
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
také	také	k9	také
stojí	stát	k5eAaImIp3nS	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
lokomotivy	lokomotiva	k1gFnPc1	lokomotiva
vhodné	vhodný	k2eAgFnPc1d1	vhodná
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
tunel	tunel	k1gInSc4	tunel
byly	být	k5eAaImAgFnP	být
zkoušeny	zkoušen	k2eAgFnPc1d1	zkoušena
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
Železničním	železniční	k2eAgInSc6d1	železniční
zkušebním	zkušební	k2eAgInSc6d1	zkušební
okruhu	okruh	k1gInSc6	okruh
Cerhenice	Cerhenice	k1gFnSc2	Cerhenice
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Nákladní	nákladní	k2eAgFnSc1d1	nákladní
doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
provozována	provozovat	k5eAaImNgFnS	provozovat
vlaky	vlak	k1gInPc7	vlak
SNCF	SNCF	kA	SNCF
a	a	k8xC	a
EWS	EWS	kA	EWS
<g/>
,	,	kIx,	,
z	z	k7c2	z
francouzské	francouzský	k2eAgFnSc2d1	francouzská
strany	strana	k1gFnSc2	strana
vede	vést	k5eAaImIp3nS	vést
vysokorychlostní	vysokorychlostní	k2eAgFnSc1d1	vysokorychlostní
trať	trať	k1gFnSc1	trať
LGV	LGV	kA	LGV
Nord	Nord	k1gMnSc1	Nord
napojující	napojující	k2eAgMnSc1d1	napojující
se	se	k3xPyFc4	se
na	na	k7c4	na
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
železniční	železniční	k2eAgFnSc4d1	železniční
síť	síť	k1gFnSc4	síť
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
britské	britský	k2eAgFnSc6d1	britská
straně	strana	k1gFnSc6	strana
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
dokončena	dokončen	k2eAgFnSc1d1	dokončena
vysokorychlostní	vysokorychlostní	k2eAgFnSc1d1	vysokorychlostní
trať	trať	k1gFnSc1	trať
pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
High	High	k1gInSc1	High
Speed	Speed	k1gInSc1	Speed
1	[number]	k4	1
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
Channel	Channel	k1gMnSc1	Channel
Tunnel	Tunnel	k1gMnSc1	Tunnel
Rail	Rail	k1gMnSc1	Rail
Link	Link	k1gMnSc1	Link
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rychlovlaky	rychlovlak	k1gInPc1	rychlovlak
Eurostar	Eurostara	k1gFnPc2	Eurostara
už	už	k9	už
nemusí	muset	k5eNaImIp3nP	muset
být	být	k5eAaImF	být
napájeny	napájet	k5eAaImNgInP	napájet
ze	z	k7c2	z
3	[number]	k4	3
<g/>
.	.	kIx.	.
kolejnice	kolejnice	k1gFnSc2	kolejnice
<g/>
,	,	kIx,	,
na	na	k7c6	na
celé	celá	k1gFnSc6	celá
jeho	jeho	k3xOp3gFnSc6	jeho
trase	trasa	k1gFnSc6	trasa
je	být	k5eAaImIp3nS	být
povolena	povolen	k2eAgFnSc1d1	povolena
rychlost	rychlost	k1gFnSc1	rychlost
300	[number]	k4	300
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
vyjma	vyjma	k7c2	vyjma
Eurotunelu	Eurotunel	k1gInSc2	Eurotunel
a	a	k8xC	a
městských	městský	k2eAgFnPc2d1	městská
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Osobní	osobní	k2eAgInPc4d1	osobní
vlaky	vlak	k1gInPc4	vlak
ukončily	ukončit	k5eAaPmAgFnP	ukončit
svůj	svůj	k3xOyFgInSc4	svůj
provoz	provoz	k1gInSc4	provoz
na	na	k7c4	na
nádraží	nádraží	k1gNnSc4	nádraží
Londýn	Londýn	k1gInSc1	Londýn
-	-	kIx~	-
Waterloo	Waterloo	k1gNnSc1	Waterloo
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
nyní	nyní	k6eAd1	nyní
zajíždějí	zajíždět	k5eAaImIp3nP	zajíždět
na	na	k7c4	na
nově	nově	k6eAd1	nově
zrekonstruované	zrekonstruovaný	k2eAgNnSc4d1	zrekonstruované
nádraží	nádraží	k1gNnSc4	nádraží
Londýn	Londýn	k1gInSc1	Londýn
-	-	kIx~	-
St.	st.	kA	st.
Pancras	Pancras	k1gInSc1	Pancras
<g/>
.	.	kIx.	.
</s>
<s>
Osobní	osobní	k2eAgInPc1d1	osobní
vlaky	vlak	k1gInPc1	vlak
jsou	být	k5eAaImIp3nP	být
vedeny	vést	k5eAaImNgInP	vést
jednotkami	jednotka	k1gFnPc7	jednotka
Eurostar	Eurostara	k1gFnPc2	Eurostara
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pobřežních	pobřežní	k2eAgNnPc6d1	pobřežní
městech	město	k1gNnPc6	město
Folkestone	Folkeston	k1gInSc5	Folkeston
a	a	k8xC	a
Calais	Calais	k1gNnPc1	Calais
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
velké	velký	k2eAgInPc1d1	velký
terminály	terminál	k1gInPc1	terminál
<g/>
,	,	kIx,	,
umožňující	umožňující	k2eAgInPc1d1	umožňující
naložit	naložit	k5eAaPmF	naložit
do	do	k7c2	do
nákladních	nákladní	k2eAgInPc2d1	nákladní
vagónů	vagón	k1gInPc2	vagón
automobily	automobil	k1gInPc4	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Tunel	tunel	k1gInSc1	tunel
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
50	[number]	k4	50
km	km	kA	km
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
38	[number]	k4	38
km	km	kA	km
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
mořským	mořský	k2eAgInSc7d1	mořský
dnem	den	k1gInSc7	den
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
hloubka	hloubka	k1gFnSc1	hloubka
je	být	k5eAaImIp3nS	být
45	[number]	k4	45
m	m	kA	m
pode	pod	k7c7	pod
dnem	den	k1gInSc7	den
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dokončení	dokončení	k1gNnSc2	dokončení
Gotthardského	Gotthardský	k2eAgInSc2d1	Gotthardský
tunelu	tunel	k1gInSc2	tunel
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2016	[number]	k4	2016
je	být	k5eAaImIp3nS	být
Eurotunel	Eurotunel	k1gInSc1	Eurotunel
třetím	třetí	k4xOgInSc7	třetí
nejdelším	dlouhý	k2eAgInSc7d3	nejdelší
železničním	železniční	k2eAgInSc7d1	železniční
tunelem	tunel	k1gInSc7	tunel
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
po	po	k7c6	po
zmíněném	zmíněný	k2eAgInSc6d1	zmíněný
švýcarském	švýcarský	k2eAgInSc6d1	švýcarský
Gotthardském	Gotthardský	k2eAgInSc6d1	Gotthardský
tunelu	tunel	k1gInSc6	tunel
(	(	kIx(	(
<g/>
57	[number]	k4	57
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
japonském	japonský	k2eAgInSc6d1	japonský
tunelu	tunel	k1gInSc6	tunel
Seikan	Seikana	k1gFnPc2	Seikana
(	(	kIx(	(
<g/>
54	[number]	k4	54
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Eurostar	Eurostar	k1gMnSc1	Eurostar
SNCF	SNCF	kA	SNCF
High	High	k1gMnSc1	High
Speed	Speed	k1gMnSc1	Speed
1	[number]	k4	1
LGV	LGV	kA	LGV
Nord	Nord	k1gMnSc1	Nord
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Eurotunel	Eurotunela	k1gFnPc2	Eurotunela
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Stránky	stránka	k1gFnSc2	stránka
společnosti	společnost	k1gFnSc2	společnost
Eurotunnel	Eurotunnela	k1gFnPc2	Eurotunnela
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
nizozemsky	nizozemsky	k6eAd1	nizozemsky
<g/>
)	)	kIx)	)
Galerie	galerie	k1gFnSc1	galerie
Eurotunel	Eurotunela	k1gFnPc2	Eurotunela
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
