<s>
Eurotunel	Eurotunel	k1gInSc1	Eurotunel
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Channel	Channel	k1gMnSc1	Channel
Tunnel	Tunnel	k1gMnSc1	Tunnel
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
používaný	používaný	k2eAgInSc4d1	používaný
výraz	výraz	k1gInSc4	výraz
pro	pro	k7c4	pro
podmořský	podmořský	k2eAgInSc4d1	podmořský
tunel	tunel	k1gInSc4	tunel
pod	pod	k7c7	pod
Lamanšským	lamanšský	k2eAgInSc7d1	lamanšský
průlivem	průliv	k1gInSc7	průliv
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k2eAgMnPc1d1	vedoucí
mezi	mezi	k7c7	mezi
anglickým	anglický	k2eAgInSc7d1	anglický
Folkestone	Folkeston	k1gInSc5	Folkeston
a	a	k8xC	a
francouzským	francouzský	k2eAgNnSc7d1	francouzské
Calais	Calais	k1gNnSc7	Calais
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgMnSc1d1	dlouhý
přes	přes	k7c4	přes
50	[number]	k4	50
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
