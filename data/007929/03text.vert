<s>
Lymfa	lymfa	k1gFnSc1	lymfa
(	(	kIx(	(
<g/>
míza	míza	k1gFnSc1	míza
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nažloutlá	nažloutlý	k2eAgFnSc1d1	nažloutlá
tekutina	tekutina	k1gFnSc1	tekutina
některých	některý	k3yIgMnPc2	některý
živočichů	živočich	k1gMnPc2	živočich
včetně	včetně	k7c2	včetně
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
koluje	kolovat	k5eAaImIp3nS	kolovat
v	v	k7c6	v
lymfatickém	lymfatický	k2eAgInSc6d1	lymfatický
systému	systém	k1gInSc6	systém
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
podobné	podobný	k2eAgNnSc1d1	podobné
složení	složení	k1gNnSc1	složení
jako	jako	k8xC	jako
krevní	krevní	k2eAgFnSc1d1	krevní
plazma	plazma	k1gFnSc1	plazma
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
lymfatická	lymfatický	k2eAgFnSc1d1	lymfatická
soustava	soustava	k1gFnSc1	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Lymfa	lymfa	k1gFnSc1	lymfa
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
mezibuněčných	mezibuněčný	k2eAgInPc6d1	mezibuněčný
prostorech	prostor	k1gInPc6	prostor
z	z	k7c2	z
tkáňového	tkáňový	k2eAgInSc2d1	tkáňový
moku	mok	k1gInSc2	mok
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
se	se	k3xPyFc4	se
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
asi	asi	k9	asi
2,5	[number]	k4	2,5
l	l	kA	l
lymfy	lymfa	k1gFnSc2	lymfa
<g/>
.	.	kIx.	.
</s>
<s>
Sbírá	sbírat	k5eAaImIp3nS	sbírat
se	se	k3xPyFc4	se
do	do	k7c2	do
mízních	mízní	k2eAgFnPc2d1	mízní
vlásečnic	vlásečnice	k1gFnPc2	vlásečnice
(	(	kIx(	(
<g/>
lymfatických	lymfatický	k2eAgFnPc2d1	lymfatická
kapilár	kapilára	k1gFnPc2	kapilára
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
dál	daleko	k6eAd2	daleko
širšími	široký	k2eAgFnPc7d2	širší
cévami	céva	k1gFnPc7	céva
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
nachází	nacházet	k5eAaImIp3nP	nacházet
lymfatické	lymfatický	k2eAgFnPc1d1	lymfatická
uzliny	uzlina	k1gFnPc1	uzlina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
lymfu	lymfa	k1gFnSc4	lymfa
filtrují	filtrovat	k5eAaImIp3nP	filtrovat
<g/>
.	.	kIx.	.
</s>
<s>
Lymfatické	lymfatický	k2eAgFnPc1d1	lymfatická
cévy	céva	k1gFnPc1	céva
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
spojují	spojovat	k5eAaImIp3nP	spojovat
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
ductus	ductus	k1gInSc1	ductus
thoracicus	thoracicus	k1gInSc1	thoracicus
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
lymfu	lymfa	k1gFnSc4	lymfa
odvádí	odvádět	k5eAaImIp3nS	odvádět
do	do	k7c2	do
žilního	žilní	k2eAgInSc2d1	žilní
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Lymfa	lymfa	k1gFnSc1	lymfa
tedy	tedy	k9	tedy
necirkuluje	cirkulovat	k5eNaImIp3nS	cirkulovat
v	v	k7c6	v
uzavřeném	uzavřený	k2eAgInSc6d1	uzavřený
oběhu	oběh	k1gInSc6	oběh
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
například	například	k6eAd1	například
u	u	k7c2	u
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Protrhnutí	protrhnutí	k1gNnSc1	protrhnutí
stěn	stěna	k1gFnPc2	stěna
mízních	mízní	k2eAgFnPc2d1	mízní
cév	céva	k1gFnPc2	céva
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
otokům	otok	k1gInPc3	otok
<g/>
.	.	kIx.	.
</s>
<s>
Složení	složení	k1gNnSc1	složení
lymfy	lymfa	k1gFnSc2	lymfa
je	být	k5eAaImIp3nS	být
proměnlivé	proměnlivý	k2eAgNnSc1d1	proměnlivé
-	-	kIx~	-
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
v	v	k7c6	v
kterém	který	k3yIgInSc6	který
orgánu	orgán	k1gInSc6	orgán
vzniká	vznikat	k5eAaImIp3nS	vznikat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
míry	míra	k1gFnSc2	míra
podobné	podobný	k2eAgNnSc4d1	podobné
složení	složení	k1gNnSc4	složení
krevní	krevní	k2eAgFnSc2d1	krevní
plazmy	plazma	k1gFnSc2	plazma
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
mnohem	mnohem	k6eAd1	mnohem
méně	málo	k6eAd2	málo
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíc	nejvíc	k6eAd1	nejvíc
jich	on	k3xPp3gMnPc2	on
v	v	k7c6	v
lymfě	lymfa	k1gFnSc6	lymfa
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
jater	játra	k1gNnPc2	játra
<g/>
.	.	kIx.	.
</s>
<s>
Lymfa	lymfa	k1gFnSc1	lymfa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
přichází	přicházet	k5eAaImIp3nS	přicházet
z	z	k7c2	z
trávicí	trávicí	k2eAgFnSc2d1	trávicí
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
zase	zase	k9	zase
vysoký	vysoký	k2eAgInSc4d1	vysoký
obsah	obsah	k1gInSc4	obsah
tuků	tuk	k1gInPc2	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Lymfa	lymfa	k1gFnSc1	lymfa
v	v	k7c6	v
kapilárách	kapilára	k1gFnPc6	kapilára
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
buňky	buňka	k1gFnPc4	buňka
<g/>
;	;	kIx,	;
ty	ty	k3xPp2nSc1	ty
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
dostávají	dostávat	k5eAaImIp3nP	dostávat
až	až	k6eAd1	až
po	po	k7c6	po
průchodu	průchod	k1gInSc6	průchod
některou	některý	k3yIgFnSc7	některý
lymfatickou	lymfatický	k2eAgFnSc7d1	lymfatická
uzlinou	uzlina	k1gFnSc7	uzlina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
ductus	ductus	k1gInSc4	ductus
thoracicus	thoracicus	k1gInSc4	thoracicus
má	můj	k3xOp1gFnSc1	můj
lymfa	lymfa	k1gFnSc1	lymfa
narůžovělou	narůžovělý	k2eAgFnSc4d1	narůžovělá
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
kalná	kalný	k2eAgFnSc1d1	kalná
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mnoho	mnoho	k4c4	mnoho
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
i	i	k9	i
schopnost	schopnost	k1gFnSc4	schopnost
srážet	srážet	k5eAaImF	srážet
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
krev	krev	k1gFnSc1	krev
<g/>
.	.	kIx.	.
99	[number]	k4	99
%	%	kIx~	%
jejích	její	k3xOp3gFnPc2	její
buněk	buňka	k1gFnPc2	buňka
tvoří	tvořit	k5eAaImIp3nP	tvořit
lymfocyty	lymfocyt	k1gInPc1	lymfocyt
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
95	[number]	k4	95
%	%	kIx~	%
jsou	být	k5eAaImIp3nP	být
malé	malý	k2eAgInPc1d1	malý
lymfocyty	lymfocyt	k1gInPc1	lymfocyt
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1	ostatní
buňky	buňka	k1gFnPc1	buňka
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
lymfě	lymfa	k1gFnSc6	lymfa
ojedinělé	ojedinělý	k2eAgInPc1d1	ojedinělý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Lymfa	lymfa	k1gFnSc1	lymfa
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Lymfatická	lymfatický	k2eAgFnSc1d1	lymfatická
soustava	soustava	k1gFnSc1	soustava
Céva	céva	k1gFnSc1	céva
</s>
