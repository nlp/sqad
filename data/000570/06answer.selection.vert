<s>
Harry	Harr	k1gInPc1	Harr
Potter	Pottra	k1gFnPc2	Pottra
a	a	k8xC	a
tajemná	tajemný	k2eAgFnSc1d1	tajemná
komnata	komnata	k1gFnSc1	komnata
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Harry	Harr	k1gInPc4	Harr
Potter	Pottrum	k1gNnPc2	Pottrum
and	and	k?	and
the	the	k?	the
Chamber	Chamber	k1gInSc1	Chamber
of	of	k?	of
Secrets	Secrets	k1gInSc1	Secrets
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kniha	kniha	k1gFnSc1	kniha
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
J.	J.	kA	J.
K.	K.	kA	K.
Rowlingové	Rowlingový	k2eAgFnPc1d1	Rowlingová
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc4	druhý
díl	díl	k1gInSc4	díl
cyklu	cyklus	k1gInSc2	cyklus
o	o	k7c4	o
Harrym	Harrym	k1gInSc4	Harrym
Potterovi	Potterův	k2eAgMnPc1d1	Potterův
<g/>
.	.	kIx.	.
</s>
