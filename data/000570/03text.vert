<s>
Harry	Harr	k1gInPc1	Harr
Potter	Pottra	k1gFnPc2	Pottra
a	a	k8xC	a
tajemná	tajemný	k2eAgFnSc1d1	tajemná
komnata	komnata	k1gFnSc1	komnata
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Harry	Harr	k1gInPc4	Harr
Potter	Pottrum	k1gNnPc2	Pottrum
and	and	k?	and
the	the	k?	the
Chamber	Chamber	k1gInSc1	Chamber
of	of	k?	of
Secrets	Secrets	k1gInSc1	Secrets
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kniha	kniha	k1gFnSc1	kniha
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
J.	J.	kA	J.
K.	K.	kA	K.
Rowlingové	Rowlingový	k2eAgFnPc1d1	Rowlingová
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc4	druhý
díl	díl	k1gInSc4	díl
cyklu	cyklus	k1gInSc2	cyklus
o	o	k7c4	o
Harrym	Harrym	k1gInSc4	Harrym
Potterovi	Potterův	k2eAgMnPc1d1	Potterův
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
prázdninách	prázdniny	k1gFnPc6	prázdniny
Harryho	Harry	k1gMnSc4	Harry
navštíví	navštívit	k5eAaPmIp3nS	navštívit
domácí	domácí	k2eAgMnSc1d1	domácí
skřítek	skřítek	k1gMnSc1	skřítek
Dobby	Dobba	k1gFnSc2	Dobba
<g/>
.	.	kIx.	.
</s>
<s>
Varuje	varovat	k5eAaImIp3nS	varovat
ho	on	k3xPp3gNnSc4	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nesmí	smět	k5eNaImIp3nS	smět
vrátit	vrátit	k5eAaPmF	vrátit
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
chystají	chystat	k5eAaImIp3nP	chystat
tajemné	tajemný	k2eAgFnPc1d1	tajemná
a	a	k8xC	a
nebezpečné	bezpečný	k2eNgFnPc1d1	nebezpečná
věci	věc	k1gFnPc1	věc
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc4	Harra
Dobbymu	Dobbym	k1gInSc2	Dobbym
odpoví	odpovědět	k5eAaPmIp3nS	odpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
do	do	k7c2	do
Bradavic	bradavice	k1gFnPc2	bradavice
vrátit	vrátit	k5eAaPmF	vrátit
musí	muset	k5eAaImIp3nS	muset
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
Dobbyho	Dobby	k1gMnSc4	Dobby
rozčílí	rozčílit	k5eAaPmIp3nS	rozčílit
<g/>
.	.	kIx.	.
</s>
<s>
Rozzlobený	rozzlobený	k2eAgMnSc1d1	rozzlobený
Dobby	Dobba	k1gFnSc2	Dobba
vzápětí	vzápětí	k6eAd1	vzápětí
použije	použít	k5eAaPmIp3nS	použít
vznášecí	vznášecí	k2eAgNnSc4d1	vznášecí
kouzlo	kouzlo	k1gNnSc4	kouzlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
způsobí	způsobit	k5eAaPmIp3nS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
dort	dort	k1gInSc1	dort
Dursleyů	Dursley	k1gInPc2	Dursley
spadne	spadnout	k5eAaPmIp3nS	spadnout
jejich	jejich	k3xOp3gFnSc3	jejich
návštěvě	návštěva	k1gFnSc3	návštěva
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Strýc	strýc	k1gMnSc1	strýc
Vernon	Vernon	k1gMnSc1	Vernon
dá	dát	k5eAaPmIp3nS	dát
Harrymu	Harrymum	k1gNnSc3	Harrymum
domácí	domácí	k1gFnSc2	domácí
vězení	vězení	k1gNnSc2	vězení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vysvobodí	vysvobodit	k5eAaPmIp3nS	vysvobodit
ho	on	k3xPp3gInSc4	on
jeho	jeho	k3xOp3gMnSc1	jeho
kamarád	kamarád	k1gMnSc1	kamarád
Ron	Ron	k1gMnSc1	Ron
Weasley	Weaslea	k1gFnSc2	Weaslea
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
bratry	bratr	k1gMnPc7	bratr
Fredem	Fred	k1gMnSc7	Fred
a	a	k8xC	a
Georgem	Georg	k1gMnSc7	Georg
pomocí	pomocí	k7c2	pomocí
létajícího	létající	k2eAgNnSc2d1	létající
auta	auto	k1gNnSc2	auto
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gMnPc4	Harr
pak	pak	k6eAd1	pak
prožije	prožít	k5eAaPmIp3nS	prožít
poslední	poslední	k2eAgInSc4d1	poslední
týden	týden	k1gInSc4	týden
prázdnin	prázdniny	k1gFnPc2	prázdniny
u	u	k7c2	u
Weasleyových	Weasleyová	k1gFnPc2	Weasleyová
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
vypraví	vypravit	k5eAaPmIp3nS	vypravit
na	na	k7c4	na
nástupiště	nástupiště	k1gNnSc4	nástupiště
devět	devět	k4xCc1	devět
a	a	k8xC	a
tři	tři	k4xCgFnPc4	tři
čtvrtě	čtvrt	k1gFnPc4	čtvrt
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgInSc2	který
odjíždí	odjíždět	k5eAaImIp3nS	odjíždět
vlak	vlak	k1gInSc1	vlak
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gInPc1	Harr
s	s	k7c7	s
Ronem	ron	k1gInSc7	ron
jdou	jít	k5eAaImIp3nP	jít
poslední	poslední	k2eAgFnPc1d1	poslední
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
minuta	minuta	k1gFnSc1	minuta
před	před	k7c7	před
jedenáctou	jedenáctý	k4xOgFnSc7	jedenáctý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přepážka	přepážka	k1gFnSc1	přepážka
na	na	k7c4	na
nástupiště	nástupiště	k1gNnSc4	nástupiště
je	být	k5eAaImIp3nS	být
už	už	k6eAd1	už
zavřená	zavřený	k2eAgFnSc1d1	zavřená
<g/>
.	.	kIx.	.
</s>
<s>
Rona	Ron	k1gMnSc2	Ron
napadne	napadnout	k5eAaPmIp3nS	napadnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
si	se	k3xPyFc3	se
měli	mít	k5eAaImAgMnP	mít
půjčit	půjčit	k5eAaPmF	půjčit
očarované	očarovaný	k2eAgNnSc4d1	očarované
auto	auto	k1gNnSc4	auto
jeho	jeho	k3xOp3gMnSc2	jeho
otce	otec	k1gMnSc2	otec
a	a	k8xC	a
odletět	odletět	k5eAaPmF	odletět
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Jenže	jenže	k8xC	jenže
na	na	k7c6	na
pozemcích	pozemek	k1gInPc6	pozemek
Bradavického	Bradavický	k2eAgInSc2d1	Bradavický
hradu	hrad	k1gInSc2	hrad
havarují	havarovat	k5eAaPmIp3nP	havarovat
a	a	k8xC	a
naletí	naletět	k5eAaPmIp3nP	naletět
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
mlátivé	mlátivý	k2eAgFnSc2d1	mlátivá
vrby	vrba	k1gFnSc2	vrba
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
okázalý	okázalý	k2eAgInSc1d1	okázalý
příjezd	příjezd	k1gInSc1	příjezd
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
moc	moc	k6eAd1	moc
nezdařil	zdařit	k5eNaPmAgInS	zdařit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pár	pár	k4xCyI	pár
měsících	měsíc	k1gInPc6	měsíc
se	se	k3xPyFc4	se
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
začínají	začínat	k5eAaImIp3nP	začínat
dít	dít	k5eAaImF	dít
zvláštní	zvláštní	k2eAgFnPc4d1	zvláštní
věci	věc	k1gFnPc4	věc
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
napadení	napadení	k1gNnSc3	napadení
paní	paní	k1gFnSc2	paní
Norrisové	Norrisový	k2eAgFnPc1d1	Norrisová
<g/>
,	,	kIx,	,
kočky	kočka	k1gFnPc1	kočka
školníka	školník	k1gMnSc2	školník
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
jednoho	jeden	k4xCgMnSc4	jeden
studenta	student	k1gMnSc4	student
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
najde	najít	k5eAaPmIp3nS	najít
nápis	nápis	k1gInSc1	nápis
na	na	k7c6	na
stěně	stěna	k1gFnSc6	stěna
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Tajemná	tajemný	k2eAgFnSc1d1	tajemná
komnata	komnata	k1gFnSc1	komnata
je	být	k5eAaImIp3nS	být
znovu	znovu	k6eAd1	znovu
otevřena	otevřít	k5eAaPmNgFnS	otevřít
<g/>
,	,	kIx,	,
nepřátelé	nepřítel	k1gMnPc1	nepřítel
dědice	dědic	k1gMnSc2	dědic
<g/>
,	,	kIx,	,
mějte	mít	k5eAaImRp2nP	mít
se	se	k3xPyFc4	se
na	na	k7c6	na
pozoru	pozor	k1gInSc6	pozor
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
<g/>
,	,	kIx,	,
Hermiona	Hermiona	k1gFnSc1	Hermiona
a	a	k8xC	a
Ron	ron	k1gInSc1	ron
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
je	být	k5eAaImIp3nS	být
Zmijozelův	Zmijozelův	k2eAgMnSc1d1	Zmijozelův
dědic	dědic	k1gMnSc1	dědic
a	a	k8xC	a
kdo	kdo	k3yQnSc1	kdo
útočí	útočit	k5eAaImIp3nS	útočit
na	na	k7c4	na
žáky	žák	k1gMnPc4	žák
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
prozatím	prozatím	k6eAd1	prozatím
je	být	k5eAaImIp3nS	být
falešně	falešně	k6eAd1	falešně
obviněn	obvinit	k5eAaPmNgMnS	obvinit
Harry	Harr	k1gMnPc7	Harr
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
on	on	k3xPp3gMnSc1	on
jako	jako	k8xS	jako
jediný	jediný	k2eAgMnSc1d1	jediný
slyší	slyšet	k5eAaImIp3nS	slyšet
šeptavý	šeptavý	k2eAgInSc1d1	šeptavý
hlas	hlas	k1gInSc1	hlas
ze	z	k7c2	z
zdí	zeď	k1gFnPc2	zeď
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
mu	on	k3xPp3gMnSc3	on
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
chce	chtít	k5eAaImIp3nS	chtít
zabít	zabít	k5eAaPmF	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Svěřuje	svěřovat	k5eAaImIp3nS	svěřovat
se	se	k3xPyFc4	se
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
Hermioně	Hermion	k1gInSc6	Hermion
i	i	k9	i
Ronovi	Ronův	k2eAgMnPc1d1	Ronův
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
příliš	příliš	k6eAd1	příliš
nevěří	věřit	k5eNaImIp3nP	věřit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
soubojnickém	soubojnický	k2eAgInSc6d1	soubojnický
klubu	klub	k1gInSc6	klub
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
založil	založit	k5eAaPmAgMnS	založit
profesor	profesor	k1gMnSc1	profesor
obrany	obrana	k1gFnSc2	obrana
proti	proti	k7c3	proti
černé	černý	k2eAgFnSc3d1	černá
magii	magie	k1gFnSc3	magie
Lockhart	Lockhart	k1gInSc4	Lockhart
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
žáci	žák	k1gMnPc1	žák
naučili	naučit	k5eAaPmAgMnP	naučit
základním	základní	k2eAgInSc7d1	základní
kouzlům	kouzlo	k1gNnPc3	kouzlo
obrany	obrana	k1gFnSc2	obrana
a	a	k8xC	a
útoku	útok	k1gInSc2	útok
proti	proti	k7c3	proti
potenciálnímu	potenciální	k2eAgNnSc3d1	potenciální
nebezpečí	nebezpečí	k1gNnSc3	nebezpečí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
na	na	k7c4	na
ně	on	k3xPp3gFnPc4	on
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
dnech	den	k1gInPc6	den
stále	stále	k6eAd1	stále
číhá	číhat	k5eAaImIp3nS	číhat
<g/>
,	,	kIx,	,
Harry	Harra	k1gFnPc1	Harra
před	před	k7c7	před
celou	celý	k2eAgFnSc7d1	celá
školou	škola	k1gFnSc7	škola
mluví	mluvit	k5eAaImIp3nS	mluvit
hadí	hadí	k2eAgFnSc7d1	hadí
řečí	řeč	k1gFnSc7	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
mají	mít	k5eAaImIp3nP	mít
strach	strach	k1gInSc1	strach
a	a	k8xC	a
myslí	myslet	k5eAaImIp3nS	myslet
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
on	on	k3xPp3gMnSc1	on
je	být	k5eAaImIp3nS	být
Zmijozelův	Zmijozelův	k2eAgMnSc1d1	Zmijozelův
dědic	dědic	k1gMnSc1	dědic
a	a	k8xC	a
otevírá	otevírat	k5eAaImIp3nS	otevírat
tajemnou	tajemný	k2eAgFnSc4d1	tajemná
komnatu	komnata	k1gFnSc4	komnata
<g/>
.	.	kIx.	.
</s>
<s>
Hermiona	Hermiona	k1gFnSc1	Hermiona
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
připraví	připravit	k5eAaPmIp3nS	připravit
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
tři	tři	k4xCgMnPc4	tři
o	o	k7c6	o
vánočních	vánoční	k2eAgFnPc6d1	vánoční
prázdninách	prázdniny	k1gFnPc6	prázdniny
mnoholičný	mnoholičný	k2eAgInSc1d1	mnoholičný
lektvar	lektvar	k1gInSc1	lektvar
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	on	k3xPp3gNnSc4	on
přemění	přeměnit	k5eAaPmIp3nS	přeměnit
ve	v	k7c4	v
tři	tři	k4xCgFnPc4	tři
zmijozelské	zmijozelský	k2eAgFnPc4d1	zmijozelská
žáky	žák	k1gMnPc7	žák
<g/>
,	,	kIx,	,
a	a	k8xC	a
potají	potají	k6eAd1	potají
se	se	k3xPyFc4	se
tak	tak	k9	tak
něco	něco	k3yInSc1	něco
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
od	od	k7c2	od
Draca	Drac	k1gInSc2	Drac
Malfoye	Malfoy	k1gInSc2	Malfoy
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
dojde	dojít	k5eAaPmIp3nS	dojít
<g/>
,	,	kIx,	,
Hermiona	Hermiona	k1gFnSc1	Hermiona
se	se	k3xPyFc4	se
omylem	omylem	k6eAd1	omylem
promění	proměnit	k5eAaPmIp3nS	proměnit
v	v	k7c4	v
kočku	kočka	k1gFnSc4	kočka
a	a	k8xC	a
musí	muset	k5eAaImIp3nS	muset
na	na	k7c4	na
ošetřovnu	ošetřovna	k1gFnSc4	ošetřovna
<g/>
,	,	kIx,	,
kluci	kluk	k1gMnPc1	kluk
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
změnu	změna	k1gFnSc4	změna
od	od	k7c2	od
Malfoye	Malfoy	k1gFnSc2	Malfoy
nedozvědí	dozvědět	k5eNaPmIp3nP	dozvědět
nic	nic	k3yNnSc1	nic
<g/>
,	,	kIx,	,
poněvadž	poněvadž	k8xS	poněvadž
on	on	k3xPp3gMnSc1	on
nic	nic	k6eAd1	nic
neví	vědět	k5eNaImIp3nS	vědět
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
koncem	konec	k1gInSc7	konec
školního	školní	k2eAgInSc2d1	školní
roku	rok	k1gInSc2	rok
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
dalšímu	další	k2eAgInSc3d1	další
dvojnásobnému	dvojnásobný	k2eAgInSc3d1	dvojnásobný
útoku	útok	k1gInSc3	útok
<g/>
,	,	kIx,	,
obětí	oběť	k1gFnSc7	oběť
je	být	k5eAaImIp3nS	být
kromě	kromě	k7c2	kromě
jedné	jeden	k4xCgFnSc2	jeden
studentky	studentka	k1gFnSc2	studentka
z	z	k7c2	z
Havraspáru	Havraspár	k1gInSc2	Havraspár
i	i	k9	i
Hermiona	Hermiona	k1gFnSc1	Hermiona
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gInPc1	Harr
s	s	k7c7	s
Ronem	ron	k1gInSc7	ron
ji	on	k3xPp3gFnSc4	on
jdou	jít	k5eAaImIp3nP	jít
na	na	k7c4	na
ošetřovnu	ošetřovna	k1gFnSc4	ošetřovna
navštívit	navštívit	k5eAaPmF	navštívit
<g/>
.	.	kIx.	.
</s>
<s>
Všimnou	všimnout	k5eAaPmIp3nP	všimnout
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
drží	držet	k5eAaImIp3nS	držet
malý	malý	k2eAgInSc1d1	malý
papírek	papírek	k1gInSc1	papírek
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
ho	on	k3xPp3gMnSc4	on
roztáhnou	roztáhnout	k5eAaPmIp3nP	roztáhnout
<g/>
,	,	kIx,	,
dočtou	dočíst	k5eAaPmIp3nP	dočíst
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
pachatelem	pachatel	k1gMnSc7	pachatel
útoků	útok	k1gInPc2	útok
je	být	k5eAaImIp3nS	být
bazilišek	bazilišek	k1gMnSc1	bazilišek
<g/>
,	,	kIx,	,
dávná	dávný	k2eAgFnSc1d1	dávná
nestvůra	nestvůra	k1gFnSc1	nestvůra
<g/>
,	,	kIx,	,
obrovský	obrovský	k2eAgMnSc1d1	obrovský
had	had	k1gMnSc1	had
<g/>
;	;	kIx,	;
na	na	k7c4	na
koho	kdo	k3yInSc4	kdo
upře	upřít	k5eAaPmIp3nS	upřít
své	svůj	k3xOyFgMnPc4	svůj
oči	oko	k1gNnPc4	oko
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
okamžitě	okamžitě	k6eAd1	okamžitě
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
se	se	k3xPyFc4	se
dovtípí	dovtípit	k5eAaPmIp3nP	dovtípit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ufňukanou	ufňukaný	k2eAgFnSc4d1	ufňukaná
Uršulu	Uršula	k1gFnSc4	Uršula
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nyní	nyní	k6eAd1	nyní
straší	strašit	k5eAaImIp3nS	strašit
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
ducha	duch	k1gMnSc2	duch
v	v	k7c6	v
dívčí	dívčí	k2eAgFnSc6d1	dívčí
umývárně	umývárna	k1gFnSc6	umývárna
bezmála	bezmála	k6eAd1	bezmála
už	už	k6eAd1	už
padesát	padesát	k4xCc4	padesát
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
stejně	stejně	k6eAd1	stejně
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
před	před	k7c7	před
kterou	který	k3yQgFnSc7	který
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
poprvé	poprvé	k6eAd1	poprvé
tajemná	tajemný	k2eAgFnSc1d1	tajemná
komnata	komnata	k1gFnSc1	komnata
<g/>
,	,	kIx,	,
nejspíš	nejspíš	k9	nejspíš
zabil	zabít	k5eAaPmAgMnS	zabít
tento	tento	k3xDgMnSc1	tento
tvor	tvor	k1gMnSc1	tvor
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
protože	protože	k8xS	protože
nyní	nyní	k6eAd1	nyní
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
tajemné	tajemný	k2eAgFnSc2d1	tajemná
komnaty	komnata	k1gFnSc2	komnata
unesena	unesen	k2eAgFnSc1d1	unesena
Ginny	Ginna	k1gFnSc2	Ginna
Weasleyová	Weasleyový	k2eAgFnSc1d1	Weasleyová
<g/>
,	,	kIx,	,
Ronova	Ronův	k2eAgFnSc1d1	Ronova
mladší	mladý	k2eAgFnSc1d2	mladší
sestra	sestra	k1gFnSc1	sestra
<g/>
,	,	kIx,	,
dojde	dojít	k5eAaPmIp3nS	dojít
Harry	Harr	k1gMnPc4	Harr
pro	pro	k7c4	pro
Lockharta	Lockhart	k1gMnSc4	Lockhart
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
doufá	doufat	k5eAaImIp3nS	doufat
<g/>
,	,	kIx,	,
jim	on	k3xPp3gFnPc3	on
pomůže	pomoct	k5eAaPmIp3nS	pomoct
přemoci	přemoct	k5eAaPmF	přemoct
baziliška	baziliška	k1gFnSc1	baziliška
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
Ronem	ron	k1gInSc7	ron
zamíří	zamířit	k5eAaPmIp3nP	zamířit
do	do	k7c2	do
umývárny	umývárna	k1gFnSc2	umývárna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Harry	Harr	k1gMnPc4	Harr
najde	najít	k5eAaPmIp3nS	najít
vchod	vchod	k1gInSc1	vchod
do	do	k7c2	do
komnaty	komnata	k1gFnSc2	komnata
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
tři	tři	k4xCgMnPc1	tři
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
spustí	spustit	k5eAaPmIp3nS	spustit
po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
klouzačce	klouzačka	k1gFnSc6	klouzačka
<g/>
.	.	kIx.	.
</s>
<s>
Lockhart	Lockharta	k1gFnPc2	Lockharta
jim	on	k3xPp3gMnPc3	on
dole	dole	k6eAd1	dole
oznámí	oznámit	k5eAaPmIp3nS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nikdy	nikdy	k6eAd1	nikdy
v	v	k7c6	v
životě	život	k1gInSc6	život
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
nic	nic	k3yNnSc4	nic
<g/>
,	,	kIx,	,
než	než	k8xS	než
paměťová	paměťový	k2eAgNnPc4d1	paměťové
kouzla	kouzlo	k1gNnPc4	kouzlo
a	a	k8xC	a
s	s	k7c7	s
Ronovou	Ronův	k2eAgFnSc7d1	Ronova
rozbitou	rozbitý	k2eAgFnSc7d1	rozbitá
hůlkou	hůlka	k1gFnSc7	hůlka
jim	on	k3xPp3gMnPc3	on
chce	chtít	k5eAaImIp3nS	chtít
vymazat	vymazat	k5eAaPmF	vymazat
paměť	paměť	k1gFnSc4	paměť
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ta	ten	k3xDgFnSc1	ten
své	svůj	k3xOyFgNnSc4	svůj
kouzlo	kouzlo	k1gNnSc4	kouzlo
obrátí	obrátit	k5eAaPmIp3nP	obrátit
proti	proti	k7c3	proti
Lockhartovi	Lockhart	k1gMnSc3	Lockhart
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
sesype	sesypat	k5eAaPmIp3nS	sesypat
hora	hora	k1gFnSc1	hora
kamení	kamení	k1gNnSc2	kamení
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yQgFnSc4	který
on	on	k3xPp3gMnSc1	on
a	a	k8xC	a
Ron	ron	k1gInSc4	ron
zůstanou	zůstat	k5eAaPmIp3nP	zůstat
uvězněni	uvězněn	k2eAgMnPc1d1	uvězněn
a	a	k8xC	a
Harry	Harra	k1gFnPc1	Harra
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nP	muset
vydat	vydat	k5eAaPmF	vydat
porazit	porazit	k5eAaPmF	porazit
baziliška	bazilišek	k1gMnSc4	bazilišek
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
komnatě	komnata	k1gFnSc6	komnata
nalezne	naleznout	k5eAaPmIp3nS	naleznout
polomrtvou	polomrtvý	k2eAgFnSc4d1	polomrtvá
Ginny	Ginna	k1gFnPc4	Ginna
a	a	k8xC	a
také	také	k9	také
Toma	Tom	k1gMnSc2	Tom
Rojvola	Rojvola	k1gFnSc1	Rojvola
Raddlea	Raddlea	k1gMnSc1	Raddlea
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
pouhou	pouhý	k2eAgFnSc7d1	pouhá
vzpomínkou	vzpomínka	k1gFnSc7	vzpomínka
studenta	student	k1gMnSc2	student
z	z	k7c2	z
Bradavic	bradavice	k1gFnPc2	bradavice
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
sám	sám	k3xTgInSc1	sám
Voldemort	Voldemort	k1gInSc1	Voldemort
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
Raddle	Raddle	k1gMnSc1	Raddle
donutil	donutit	k5eAaPmAgMnS	donutit
Ginny	Ginna	k1gFnPc4	Ginna
otevírat	otevírat	k5eAaImF	otevírat
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
komnatu	komnata	k1gFnSc4	komnata
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
chce	chtít	k5eAaImIp3nS	chtít
ji	on	k3xPp3gFnSc4	on
i	i	k9	i
Harryho	Harry	k1gMnSc4	Harry
zabít	zabít	k5eAaPmF	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Zavolá	zavolat	k5eAaPmIp3nS	zavolat
baziliška	baziliška	k1gFnSc1	baziliška
<g/>
.	.	kIx.	.
</s>
<s>
Harrymu	Harrym	k1gInSc2	Harrym
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
přiletí	přiletět	k5eAaPmIp3nS	přiletět
Brumbálův	brumbálův	k2eAgMnSc1d1	brumbálův
fénix	fénix	k1gMnSc1	fénix
Fawkes	Fawkes	k1gMnSc1	Fawkes
s	s	k7c7	s
moudrým	moudrý	k2eAgInSc7d1	moudrý
kloboukem	klobouk	k1gInSc7	klobouk
<g/>
,	,	kIx,	,
v	v	k7c6	v
kterém	který	k3yIgInSc6	který
je	být	k5eAaImIp3nS	být
ukryt	ukryt	k2eAgInSc1d1	ukryt
meč	meč	k1gInSc1	meč
Godrika	Godrik	k1gMnSc2	Godrik
Nebelvíra	Nebelvír	k1gMnSc2	Nebelvír
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ním	on	k3xPp3gInSc7	on
Harry	Harra	k1gFnPc4	Harra
zabije	zabít	k5eAaPmIp3nS	zabít
baziliška	baziliška	k1gFnSc1	baziliška
a	a	k8xC	a
poté	poté	k6eAd1	poté
zničí	zničit	k5eAaPmIp3nS	zničit
i	i	k9	i
vzpomínku	vzpomínka	k1gFnSc4	vzpomínka
Toma	Tom	k1gMnSc2	Tom
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
čtyři	čtyři	k4xCgMnPc1	čtyři
se	se	k3xPyFc4	se
zpět	zpět	k6eAd1	zpět
nahoru	nahoru	k6eAd1	nahoru
dostanou	dostat	k5eAaPmIp3nP	dostat
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Fawkese	Fawkese	k1gFnSc2	Fawkese
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
a	a	k8xC	a
Ron	Ron	k1gMnSc1	Ron
dostávají	dostávat	k5eAaImIp3nP	dostávat
vyznamenání	vyznamenání	k1gNnSc4	vyznamenání
za	za	k7c4	za
zvláštní	zvláštní	k2eAgFnPc4d1	zvláštní
služby	služba	k1gFnPc4	služba
škole	škola	k1gFnSc3	škola
a	a	k8xC	a
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
osvobodí	osvobodit	k5eAaPmIp3nS	osvobodit
Dobbyho	Dobby	k1gMnSc4	Dobby
od	od	k7c2	od
rodiny	rodina	k1gFnSc2	rodina
Malfoyových	Malfoyová	k1gFnPc2	Malfoyová
<g/>
,	,	kIx,	,
odjíždí	odjíždět	k5eAaImIp3nS	odjíždět
na	na	k7c4	na
prázdniny	prázdniny	k1gFnPc4	prázdniny
zpět	zpět	k6eAd1	zpět
domů	domů	k6eAd1	domů
<g/>
.	.	kIx.	.
</s>
