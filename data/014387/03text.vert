<s>
Brno-Medlánky	Brno-Medlánka	k1gFnPc1
</s>
<s>
Brno-Medlánky	Brno-Medlánek	k1gInPc1
Socha	socha	k1gFnSc1
sv.	sv.	kA
Jana	Jan	k1gMnSc2
Nepomuckého	Nepomucký	k1gMnSc2
s	s	k7c7
novou	nový	k2eAgFnSc7d1
výstavbou	výstavba	k1gFnSc7
v	v	k7c6
pozadí	pozadí	k1gNnSc6
</s>
<s>
znakvlajka	znakvlajka	k1gFnSc1
Lokalita	lokalita	k1gFnSc1
Status	status	k1gInSc1
</s>
<s>
městská	městský	k2eAgFnSc1d1
část	část	k1gFnSc1
Statutární	statutární	k2eAgNnSc1d1
město	město	k1gNnSc1
</s>
<s>
Brno	Brno	k1gNnSc1
Okres	okres	k1gInSc1
</s>
<s>
Brno-město	Brno-město	k6eAd1
Kraj	kraj	k1gInSc1
</s>
<s>
Jihomoravský	jihomoravský	k2eAgInSc1d1
Historická	historický	k2eAgFnSc1d1
země	zem	k1gFnPc1
</s>
<s>
Morava	Morava	k1gFnSc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
14	#num#	k4
<g/>
′	′	k?
<g/>
30	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
°	°	k?
<g/>
34	#num#	k4
<g/>
′	′	k?
<g/>
26	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Základní	základní	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
5	#num#	k4
898	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Rozloha	rozloha	k1gFnSc1
</s>
<s>
3,51	3,51	k4
km²	km²	k?
Katastrální	katastrální	k2eAgNnSc4d1
území	území	k1gNnSc4
</s>
<s>
Medlánky	Medlánek	k1gInPc4
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
250	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
PSČ	PSČ	kA
</s>
<s>
621	#num#	k4
00	#num#	k4
Počet	počet	k1gInSc1
domů	domů	k6eAd1
</s>
<s>
559	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Počet	počet	k1gInSc1
částí	část	k1gFnPc2
obce	obec	k1gFnSc2
</s>
<s>
1	#num#	k4
Počet	počet	k1gInSc1
k.	k.	k?
ú.	ú.	k?
</s>
<s>
1	#num#	k4
Počet	počet	k1gInSc1
ZSJ	ZSJ	kA
</s>
<s>
5	#num#	k4
Kontakt	kontakt	k1gInSc1
Adresa	adresa	k1gFnSc1
úřadu	úřad	k1gInSc2
MČ	MČ	kA
</s>
<s>
Hudcova	Hudcův	k2eAgFnSc1d1
239	#num#	k4
<g/>
/	/	kIx~
<g/>
7621	#num#	k4
00	#num#	k4
Brno	Brno	k1gNnSc4
podatelna@medlanky.brno.cz	podatelna@medlanky.brno.cz	k1gMnSc1
Starosta	Starosta	k1gMnSc1
</s>
<s>
Michal	Michal	k1gMnSc1
Marek	Marek	k1gMnSc1
(	(	kIx(
<g/>
Občané	občan	k1gMnPc1
pro	pro	k7c4
Medlánky	Medlánek	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
<g/>
:	:	kIx,
medlanky	medlanka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Brno-Medlánky	Brno-Medlánka	k1gFnPc1
</s>
<s>
Další	další	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Kód	kód	k1gInSc4
MČ	MČ	kA
</s>
<s>
551236	#num#	k4
Kód	kód	k1gInSc1
části	část	k1gFnSc2
obce	obec	k1gFnSc2
</s>
<s>
411850	#num#	k4
Kód	kód	k1gInSc1
k.	k.	k?
ú.	ú.	k?
</s>
<s>
611743	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Zdroje	zdroj	k1gInSc2
k	k	k7c3
infoboxu	infobox	k1gInSc3
a	a	k8xC
českým	český	k2eAgNnPc3d1
sídlům	sídlo	k1gNnPc3
<g/>
.	.	kIx.
<g/>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Brno-Medlánky	Brno-Medlánek	k1gMnPc4
je	být	k5eAaImIp3nS
městská	městský	k2eAgFnSc1d1
část	část	k1gFnSc1
na	na	k7c6
severním	severní	k2eAgInSc6d1
okraji	okraj	k1gInSc6
statutárního	statutární	k2eAgNnSc2d1
města	město	k1gNnSc2
Brna	Brno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
tvořena	tvořit	k5eAaImNgFnS
městskou	městský	k2eAgFnSc7d1
čtvrtí	čtvrt	k1gFnSc7
Medlánky	Medlánka	k1gFnSc2
(	(	kIx(
<g/>
německy	německy	k6eAd1
Medlan	Medlany	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
původně	původně	k6eAd1
samostatnou	samostatný	k2eAgFnSc7d1
obcí	obec	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
k	k	k7c3
Brnu	Brno	k1gNnSc3
připojena	připojit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1919	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc4
katastrální	katastrální	k2eAgNnSc1d1
území	území	k1gNnSc1
má	mít	k5eAaImIp3nS
rozlohu	rozloha	k1gFnSc4
3,51	3,51	k4
km²	km²	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samosprávná	samosprávný	k2eAgFnSc1d1
městská	městský	k2eAgFnSc1d1
část	část	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
24	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1990	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žije	žít	k5eAaImIp3nS
zde	zde	k6eAd1
přibližně	přibližně	k6eAd1
5900	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
katastru	katastr	k1gInSc6
čtvrti	čtvrt	k1gFnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
letiště	letiště	k1gNnSc1
Medlánky	Medlánka	k1gFnSc2
<g/>
,	,	kIx,
převážně	převážně	k6eAd1
s	s	k7c7
provozem	provoz	k1gInSc7
kluzáků	kluzák	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
účely	účel	k1gInPc4
senátních	senátní	k2eAgFnPc2d1
voleb	volba	k1gFnPc2
je	být	k5eAaImIp3nS
území	území	k1gNnSc1
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Brno-Medlánky	Brno-Medlánka	k1gFnSc2
zařazeno	zařadit	k5eAaPmNgNnS
do	do	k7c2
volebního	volební	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
číslo	číslo	k1gNnSc1
60	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Medlánkách	Medlánka	k1gFnPc6
se	se	k3xPyFc4
také	také	k9
nachází	nacházet	k5eAaImIp3nS
viniční	viniční	k2eAgFnSc1d1
trať	trať	k1gFnSc1
V	v	k7c6
pustých	pustý	k2eAgFnPc6d1
<g/>
.	.	kIx.
</s>
<s>
Historický	historický	k2eAgInSc1d1
přehled	přehled	k1gInSc1
</s>
<s>
Většina	většina	k1gFnSc1
moderního	moderní	k2eAgInSc2d1
katastru	katastr	k1gInSc2
Medlánek	Medlánka	k1gFnPc2
sice	sice	k8xC
původně	původně	k6eAd1
náležela	náležet	k5eAaImAgFnS
ke	k	k7c3
katastrálnímu	katastrální	k2eAgNnSc3d1
území	území	k1gNnSc3
bývalé	bývalý	k2eAgFnSc2d1
obce	obec	k1gFnSc2
Medlánky	Medlánka	k1gFnSc2
<g/>
,	,	kIx,
okrajové	okrajový	k2eAgFnSc6d1
části	část	k1gFnSc6
moderního	moderní	k2eAgInSc2d1
katastru	katastr	k1gInSc2
však	však	k9
do	do	k7c2
2	#num#	k4
<g/>
.	.	kIx.
poloviny	polovina	k1gFnSc2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
náležely	náležet	k5eAaImAgInP
ke	k	k7c3
katastrálním	katastrální	k2eAgFnPc3d1
územím	území	k1gNnSc7
Komín	Komín	k1gInSc1
a	a	k8xC
Královo	Králův	k2eAgNnSc1d1
Pole	pole	k1gNnSc1
<g/>
,	,	kIx,
naopak	naopak	k6eAd1
některé	některý	k3yIgFnPc1
části	část	k1gFnPc1
původního	původní	k2eAgNnSc2d1
katastrálního	katastrální	k2eAgNnSc2d1
území	území	k1gNnSc2
Medlánky	Medlánka	k1gFnSc2
náležejí	náležet	k5eAaImIp3nP
od	od	k7c2
konce	konec	k1gInSc2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
k	k	k7c3
moderním	moderní	k2eAgFnPc3d1
katastrálním	katastrální	k2eAgFnPc3d1
územím	území	k1gNnSc7
Královo	Králův	k2eAgNnSc1d1
Pole	pole	k1gNnSc4
a	a	k8xC
Řečkovice	Řečkovice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Počátky	počátek	k1gInPc1
osídlení	osídlení	k1gNnSc2
</s>
<s>
K	k	k7c3
jihu	jih	k1gInSc3
otevřený	otevřený	k2eAgInSc1d1
katastr	katastr	k1gInSc1
Medlánek	Medlánek	k1gInSc1
s	s	k7c7
hojností	hojnost	k1gFnSc7
sprašových	sprašový	k2eAgFnPc2d1
půd	půda	k1gFnPc2
tvoří	tvořit	k5eAaImIp3nP
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
dokládají	dokládat	k5eAaImIp3nP
archeologické	archeologický	k2eAgInPc1d1
nálezy	nález	k1gInPc1
<g/>
,	,	kIx,
součást	součást	k1gFnSc1
odedávna	odedávna	k6eAd1
osídlené	osídlený	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
jižní	jižní	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svědčí	svědčit	k5eAaImIp3nS
o	o	k7c6
tom	ten	k3xDgNnSc6
četnost	četnost	k1gFnSc1
nálezů	nález	k1gInPc2
v	v	k7c6
medlánecké	medlánecký	k2eAgFnSc6d1
cihelně	cihelna	k1gFnSc6
<g/>
,	,	kIx,
zejména	zejména	k9
v	v	k7c6
roce	rok	k1gInSc6
1905	#num#	k4
objevená	objevený	k2eAgFnSc1d1
sídlištní	sídlištní	k2eAgFnSc1d1
jáma	jáma	k1gFnSc1
se	s	k7c7
zdobenými	zdobený	k2eAgInPc7d1
zachovalými	zachovalý	k2eAgInPc7d1
zvoncovitými	zvoncovitý	k2eAgInPc7d1
poháry	pohár	k1gInPc7
stejnojmenné	stejnojmenný	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
(	(	kIx(
<g/>
2000	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
letech	let	k1gInPc6
1935	#num#	k4
–	–	k?
1937	#num#	k4
nalezené	nalezený	k2eAgNnSc4d1
pohřebiště	pohřebiště	k1gNnSc4
s	s	k7c7
hroby	hrob	k1gInPc7
typických	typický	k2eAgFnPc2d1
„	„	k?
<g/>
skrčenců	skrčenec	k1gMnPc2
<g/>
“	“	k?
doby	doba	k1gFnSc2
bronzové	bronzový	k2eAgFnSc2d1
–	–	k?
kultury	kultura	k1gFnSc2
unětické	unětický	k2eAgFnSc2d1
(	(	kIx(
<g/>
1900	#num#	k4
–	–	k?
1700	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Také	také	k9
archeologický	archeologický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
ČSAV	ČSAV	kA
při	při	k7c6
průzkumu	průzkum	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
1954	#num#	k4
a	a	k8xC
následně	následně	k6eAd1
plánovaném	plánovaný	k2eAgInSc6d1
záchranném	záchranný	k2eAgInSc6d1
výzkumu	výzkum	k1gInSc6
v	v	k7c6
letech	léto	k1gNnPc6
1964	#num#	k4
a	a	k8xC
1984	#num#	k4
na	na	k7c6
staveništích	staveniště	k1gNnPc6
Meopty	Meopta	k1gFnSc2
a	a	k8xC
Výzkumného	výzkumný	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
energetického	energetický	k2eAgInSc2d1
odkryl	odkrýt	k5eAaPmAgInS
desítky	desítka	k1gFnPc4
základů	základ	k1gInPc2
kůlových	kůlový	k2eAgInPc2d1
domů	dům	k1gInPc2
a	a	k8xC
jámových	jámový	k2eAgInPc2d1
sídlištních	sídlištní	k2eAgInPc2d1
objektů	objekt	k1gInPc2
s	s	k7c7
obilnými	obilný	k2eAgInPc7d1
sklípky	sklípek	k1gInPc7
z	z	k7c2
období	období	k1gNnSc2
kultury	kultura	k1gFnSc2
popelnicových	popelnicový	k2eAgFnPc2d1
polí	pole	k1gFnPc2
(	(	kIx(
<g/>
kultury	kultura	k1gFnSc2
velatické	velatický	k2eAgFnPc1d1
<g/>
,	,	kIx,
1250	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
–	–	k?
<g/>
1000	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
,	,	kIx,
a	a	k8xC
podolské	podolský	k2eAgInPc1d1
<g/>
,	,	kIx,
1000	#num#	k4
<g/>
–	–	k?
<g/>
700	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
a	a	k8xC
ze	z	k7c2
starší	starý	k2eAgFnSc2d2
doby	doba	k1gFnSc2
železné	železný	k2eAgFnSc2d1
(	(	kIx(
<g/>
kultury	kultura	k1gFnSc2
horákovské	horákovský	k2eAgFnPc1d1
<g/>
,	,	kIx,
700	#num#	k4
<g/>
–	–	k?
<g/>
400	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednu	jeden	k4xCgFnSc4
odkrytou	odkrytý	k2eAgFnSc4d1
sídlištní	sídlištní	k2eAgFnSc4d1
jámu	jáma	k1gFnSc4
–	–	k?
obilnici	obilnice	k1gFnSc4
se	s	k7c7
starobylou	starobylý	k2eAgFnSc7d1
keramikou	keramika	k1gFnSc7
–	–	k?
kladou	klást	k5eAaImIp3nP
archeologové	archeolog	k1gMnPc1
do	do	k7c2
8	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
a	a	k8xC
dalších	další	k2eAgMnPc2d1
bezmála	bezmála	k6eAd1
70	#num#	k4
–	–	k?
z	z	k7c2
toho	ten	k3xDgNnSc2
8	#num#	k4
obytných	obytný	k2eAgFnPc2d1
kůlových	kůlový	k2eAgFnPc2d1
<g/>
,	,	kIx,
s	s	k7c7
kamennými	kamenný	k2eAgFnPc7d1
pecemi	pec	k1gFnPc7
v	v	k7c6
rohu	roh	k1gInSc6
–	–	k?
do	do	k7c2
9	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
tj.	tj.	kA
období	období	k1gNnSc1
vzniku	vznik	k1gInSc2
a	a	k8xC
rozmachu	rozmach	k1gInSc2
Velké	velký	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Dějiny	dějiny	k1gFnPc1
následných	následný	k2eAgNnPc2d1
století	století	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yQgNnPc1,k3yRgNnPc1
zatím	zatím	k6eAd1
nejsou	být	k5eNaImIp3nP
sídlištními	sídlištní	k2eAgInPc7d1
ani	ani	k8xC
hrobovými	hrobový	k2eAgInPc7d1
nálezy	nález	k1gInPc7
dokumentována	dokumentovat	k5eAaBmNgFnS
<g/>
,	,	kIx,
nám	my	k3xPp1nPc3
ponechávají	ponechávat	k5eAaImIp3nP
právo	právo	k1gNnSc4
usuzovat	usuzovat	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
zde	zde	k6eAd1
usídlené	usídlený	k2eAgNnSc1d1
společenství	společenství	k1gNnSc1
se	se	k3xPyFc4
dotvářelo	dotvářet	k5eAaImAgNnS
v	v	k7c4
osadu	osada	k1gFnSc4
či	či	k8xC
ves	ves	k1gFnSc4
přemyslovského	přemyslovský	k2eAgInSc2d1
státu	stát	k1gInSc2
–	–	k?
majetek	majetek	k1gInSc1
panovníka	panovník	k1gMnSc2
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
stroze	stroze	k6eAd1
informují	informovat	k5eAaBmIp3nP
historické	historický	k2eAgInPc4d1
prameny	pramen	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
se	se	k3xPyFc4
domníváme	domnívat	k5eAaImIp1nP
<g/>
,	,	kIx,
vznikl	vzniknout	k5eAaPmAgInS
také	také	k9
název	název	k1gInSc1
obce	obec	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obec	obec	k1gFnSc1
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
žila	žít	k5eAaImAgFnS
plně	plně	k6eAd1
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
přírodou	příroda	k1gFnSc7
a	a	k8xC
podle	podle	k7c2
pomalu	pomalu	k6eAd1
tekoucího	tekoucí	k2eAgInSc2d1
potoka	potok	k1gInSc2
si	se	k3xPyFc3
zřejmě	zřejmě	k6eAd1
vzala	vzít	k5eAaPmAgFnS
za	za	k7c4
základ	základ	k1gInSc4
slovanské	slovanský	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
Medl	Medla	k1gFnPc2
(	(	kIx(
<g/>
pomalý	pomalý	k2eAgInSc1d1
<g/>
)	)	kIx)
a	a	k8xC
dotvořila	dotvořit	k5eAaPmAgFnS
jej	on	k3xPp3gMnSc4
do	do	k7c2
tvaru	tvar	k1gInSc2
Medlany	Medlana	k1gFnSc2
(	(	kIx(
<g/>
latinsky	latinsky	k6eAd1
prokazatelně	prokazatelně	k6eAd1
Medlan	Medlana	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Středověká	středověký	k2eAgFnSc1d1
historie	historie	k1gFnSc1
</s>
<s>
Prvním	první	k4xOgInSc7
dochovaným	dochovaný	k2eAgInSc7d1
písemným	písemný	k2eAgInSc7d1
dokladem	doklad	k1gInSc7
<g/>
,	,	kIx,
prokazujícím	prokazující	k2eAgInSc7d1
existenci	existence	k1gFnSc4
obce	obec	k1gFnSc2
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
listina	listina	k1gFnSc1
krále	král	k1gMnSc2
Václava	Václav	k1gMnSc2
I.	I.	kA
ze	z	k7c2
14	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1237	#num#	k4
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
Medlánky	Medlánka	k1gFnPc1
<g/>
,	,	kIx,
zničené	zničený	k2eAgFnPc1d1
jeho	jeho	k3xOp3gNnSc7
vlastním	vlastní	k2eAgNnSc7d1
válečným	válečný	k2eAgNnSc7d1
tažením	tažení	k1gNnSc7
a	a	k8xC
doposud	doposud	k6eAd1
patřící	patřící	k2eAgFnSc6d1
škole	škola	k1gFnSc6
sv.	sv.	kA
Petra	Petr	k1gMnSc2
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
směňuje	směňovat	k5eAaImIp3nS
za	za	k7c4
svůj	svůj	k3xOyFgInSc4
nepoškozený	poškozený	k2eNgInSc4d1
majetek	majetek	k1gInSc4
v	v	k7c6
Bosonohách	Bosonoha	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
roce	rok	k1gInSc6
1237	#num#	k4
historické	historický	k2eAgInPc4d1
prameny	pramen	k1gInPc4
znovu	znovu	k6eAd1
o	o	k7c6
Medlánkách	Medlánka	k1gFnPc6
na	na	k7c4
více	hodně	k6eAd2
než	než	k8xS
100	#num#	k4
let	léto	k1gNnPc2
mlčí	mlčet	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teprve	teprve	k6eAd1
v	v	k7c6
letech	let	k1gInPc6
1344	#num#	k4
<g/>
–	–	k?
<g/>
1380	#num#	k4
se	se	k3xPyFc4
připomínají	připomínat	k5eAaImIp3nP
jako	jako	k9
vlastníci	vlastník	k1gMnPc1
brněnští	brněnský	k2eAgMnPc1d1
měšťané	měšťan	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1381	#num#	k4
do	do	k7c2
1489	#num#	k4
vlastní	vlastní	k2eAgFnSc2d1
Medlánky	Medlánka	k1gFnSc2
vladykové	vladyka	k1gMnPc1
Medlanští	Medlanský	k2eAgMnPc1d1
z	z	k7c2
Medlan	Medlany	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1489	#num#	k4
do	do	k7c2
1559	#num#	k4
pak	pak	k6eAd1
pánové	pán	k1gMnPc1
z	z	k7c2
Pernštejna	Pernštejn	k1gInSc2
–	–	k?
toto	tento	k3xDgNnSc4
70	#num#	k4
let	léto	k1gNnPc2
trvající	trvající	k2eAgNnSc4d1
vlastnictví	vlastnictví	k1gNnSc4
je	být	k5eAaImIp3nS
pro	pro	k7c4
Medlánky	Medlánka	k1gFnPc4
významné	významný	k2eAgFnPc4d1
rokem	rok	k1gInSc7
1532	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
tehdejší	tehdejší	k2eAgMnSc1d1
majitel	majitel	k1gMnSc1
Jan	Jan	k1gMnSc1
z	z	k7c2
Pernštejna	Pernštejn	k1gInSc2
(	(	kIx(
<g/>
byl	být	k5eAaImAgMnS
tehdy	tehdy	k6eAd1
zemským	zemský	k2eAgMnSc7d1
hejtmanem	hejtman	k1gMnSc7
Markrabství	markrabství	k1gNnSc2
moravského	moravský	k2eAgNnSc2d1
<g/>
)	)	kIx)
osvobodil	osvobodit	k5eAaPmAgMnS
medlánecké	medlánecký	k2eAgFnPc4d1
poddané	poddaná	k1gFnPc4
od	od	k7c2
roboty	robota	k1gFnSc2
–	–	k?
tato	tento	k3xDgFnSc1
výsada	výsada	k1gFnSc1
trvala	trvat	k5eAaImAgFnS
zhruba	zhruba	k6eAd1
100	#num#	k4
let	léto	k1gNnPc2
a	a	k8xC
byla	být	k5eAaImAgNnP
zrušena	zrušit	k5eAaPmNgNnP
v	v	k7c6
době	doba	k1gFnSc6
pobělohorské	pobělohorský	k2eAgFnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1559	#num#	k4
do	do	k7c2
1588	#num#	k4
vlastnil	vlastnit	k5eAaImAgMnS
Medlánky	Medlánek	k1gMnPc4
Petr	Petr	k1gMnSc1
Sádovský	Sádovský	k1gMnSc1
ze	z	k7c2
Sloupna	Sloupn	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
odkoupil	odkoupit	k5eAaPmAgMnS
od	od	k7c2
Pernštejnů	Pernštejn	k1gInPc2
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
panstvím	panství	k1gNnSc7
sokolnickým	sokolnický	k2eAgNnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1588	#num#	k4
se	se	k3xPyFc4
novým	nový	k2eAgMnSc7d1
majitelem	majitel	k1gMnSc7
Medlánek	Medlánka	k1gFnPc2
za	za	k7c4
22	#num#	k4
tisíc	tisíc	k4xCgInSc4
zlatých	zlatá	k1gFnPc2
stal	stát	k5eAaPmAgMnS
Hendrych	Hendrych	k1gMnSc1
Pfefferkorn	Pfefferkorn	k1gMnSc1
z	z	k7c2
Ottopachu	Ottopach	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dlouho	dlouho	k6eAd1
se	se	k3xPyFc4
však	však	k9
ze	z	k7c2
svého	svůj	k3xOyFgInSc2
majetku	majetek	k1gInSc2
netěšil	těšit	k5eNaImAgInS
<g/>
,	,	kIx,
neboť	neboť	k8xC
vzápětí	vzápětí	k6eAd1
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zadlužení	zadlužený	k2eAgMnPc1d1
potomci	potomek	k1gMnPc1
prodali	prodat	k5eAaPmAgMnP
v	v	k7c6
roce	rok	k1gInSc6
1590	#num#	k4
Medlánky	Medlánka	k1gFnSc2
panu	pan	k1gMnSc3
Jindřichu	Jindřich	k1gMnSc3
mladšímu	mladý	k2eAgMnSc3d2
<g/>
,	,	kIx,
Purkrabímu	purkrabí	k1gMnSc3
z	z	k7c2
Donína	Donín	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
zde	zde	k6eAd1
však	však	k9
vlastnictví	vlastnictví	k1gNnPc4
netrvalo	trvat	k5eNaImAgNnS
dlouho	dlouho	k6eAd1
a	a	k8xC
roku	rok	k1gInSc2
1596	#num#	k4
se	se	k3xPyFc4
Medlánky	Medlánka	k1gFnPc1
stávají	stávat	k5eAaImIp3nP
majetkem	majetek	k1gInSc7
Bohuslava	Bohuslava	k1gFnSc1
Bořity	Bořit	k1gInPc1
z	z	k7c2
Budče	Budeč	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
pobělohorské	pobělohorský	k2eAgFnSc6d1
době	doba	k1gFnSc6
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1622	#num#	k4
<g/>
,	,	kIx,
prodává	prodávat	k5eAaImIp3nS
Medlánky	Medlánek	k1gMnPc4
pan	pan	k1gMnSc1
Bohuslav	Bohuslav	k1gMnSc1
Bořita	Bořita	k1gMnSc1
z	z	k7c2
Budče	Budeč	k1gFnSc2
paní	paní	k1gFnSc2
Alžbětě	Alžběta	k1gFnSc3
Pergarce	Pergarka	k1gFnSc3
z	z	k7c2
Pergu	Perg	k1gInSc2
<g/>
,	,	kIx,
rozené	rozený	k2eAgFnSc2d1
Kummerové	Kummerová	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
jejímž	jejíž	k3xOyRp3gNnSc6
vlastnictví	vlastnictví	k1gNnSc6
jsou	být	k5eAaImIp3nP
Medlánky	Medlánka	k1gFnPc1
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1650	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
těchto	tento	k3xDgInPc6
letech	let	k1gInPc6
na	na	k7c4
krátkou	krátký	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
drží	držet	k5eAaImIp3nS
Medlánky	Medlánek	k1gMnPc4
Ferdinand	Ferdinand	k1gMnSc1
Zikmund	Zikmund	k1gMnSc1
Sak	sak	k1gInSc4
z	z	k7c2
Bohuňovic	Bohuňovice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
se	se	k3xPyFc4
zadlužil	zadlužit	k5eAaPmAgInS
a	a	k8xC
již	již	k6eAd1
roku	rok	k1gInSc2
1650	#num#	k4
Medlánky	Medlánek	k1gMnPc7
prodává	prodávat	k5eAaImIp3nS
a	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
znovu	znovu	k6eAd1
ocitají	ocitat	k5eAaImIp3nP
v	v	k7c6
držení	držení	k1gNnSc6
rodu	rod	k1gInSc2
Pergů	Perg	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paní	paní	k1gFnSc1
Alžběta	Alžběta	k1gFnSc1
odkázala	odkázat	k5eAaPmAgFnS
však	však	k9
tento	tento	k3xDgInSc4
majetek	majetek	k1gInSc4
své	svůj	k3xOyFgFnSc3
dceři	dcera	k1gFnSc3
Prisce	Priska	k1gFnSc3
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
tou	ten	k3xDgFnSc7
dobou	doba	k1gFnSc7
byla	být	k5eAaImAgFnS
provdána	provdat	k5eAaPmNgFnS
za	za	k7c4
hraběte	hrabě	k1gMnSc4
z	z	k7c2
Magni	Magn	k1gMnPc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
František	František	k1gMnSc1
Magnis	Magnis	k1gFnSc4
udělal	udělat	k5eAaPmAgMnS
v	v	k7c6
období	období	k1gNnSc6
třicetileté	třicetiletý	k2eAgFnSc2d1
války	válka	k1gFnSc2
poměrně	poměrně	k6eAd1
rychlou	rychlý	k2eAgFnSc4d1
kariéru	kariéra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejdříve	dříve	k6eAd3
byl	být	k5eAaImAgInS
rytířem	rytíř	k1gMnSc7
<g/>
,	,	kIx,
svobodným	svobodný	k2eAgMnSc7d1
pánem	pán	k1gMnSc7
<g/>
,	,	kIx,
pak	pak	k6eAd1
hrabětem	hrabě	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Moravě	Morava	k1gFnSc6
zastával	zastávat	k5eAaImAgMnS
úřad	úřad	k1gInSc4
nejvyššího	vysoký	k2eAgMnSc2d3
podkomořího	podkomoří	k1gMnSc2
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1640	#num#	k4
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaImNgMnS,k5eAaBmNgMnS
správcem	správce	k1gMnSc7
země	zem	k1gFnSc2
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
1649	#num#	k4
byl	být	k5eAaImAgInS
zemským	zemský	k2eAgMnSc7d1
sudím	sudí	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1652	#num#	k4
umírá	umírat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
založila	založit	k5eAaPmAgFnS
bezdětná	bezdětný	k2eAgFnSc1d1
vdova	vdova	k1gFnSc1
Jana	Jan	k1gMnSc2
Františka	František	k1gMnSc2
Prisca	Priscus	k1gMnSc2
z	z	k7c2
Magni	Mageň	k1gFnSc3
roku	rok	k1gInSc2
1654	#num#	k4
Nadaci	nadace	k1gFnSc3
Marie	Maria	k1gFnSc2
Školské	školská	k1gFnSc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
Mariaschul	Mariaschul	k1gInSc1
<g/>
)	)	kIx)
pro	pro	k7c4
výchovu	výchova	k1gFnSc4
mladých	mladý	k2eAgFnPc2d1
šlechtičen	šlechtična	k1gFnPc2
a	a	k8xC
měšťanských	měšťanský	k2eAgFnPc2d1
dcer	dcera	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Testamentem	testament	k1gInSc7
z	z	k7c2
29	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1654	#num#	k4
této	tento	k3xDgFnSc3
nadaci	nadace	k1gFnSc3
odkázala	odkázat	k5eAaPmAgFnS
nejen	nejen	k6eAd1
60	#num#	k4
tisíc	tisíc	k4xCgInPc2
rýnských	rýnský	k2eAgFnPc2d1
zlatých	zlatá	k1gFnPc2
<g/>
,	,	kIx,
četné	četný	k2eAgInPc4d1
šperky	šperk	k1gInPc4
a	a	k8xC
klenoty	klenot	k1gInPc4
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
dům	dům	k1gInSc4
na	na	k7c6
rohu	roh	k1gInSc6
Kobližné	kobližný	k2eAgFnSc2d1
ulice	ulice	k1gFnSc2
č.	č.	k?
3	#num#	k4
<g/>
,	,	kIx,
dvě	dva	k4xCgFnPc4
zahrady	zahrada	k1gFnPc4
a	a	k8xC
svůj	svůj	k3xOyFgInSc4
statek	statek	k1gInSc4
Medlánky	Medlánka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
nadace	nadace	k1gFnSc1
pro	pro	k7c4
naše	náš	k3xOp1gMnPc4
předky	předek	k1gMnPc4
i	i	k8xC
pozemkovou	pozemkový	k2eAgFnSc7d1
vrchností	vrchnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
přání	přání	k1gNnPc2
paní	paní	k1gFnSc2
hraběnky	hraběnka	k1gFnSc2
se	se	k3xPyFc4
staly	stát	k5eAaPmAgFnP
vrchními	vrchní	k2eAgFnPc7d1
ředitelkami	ředitelka	k1gFnPc7
nadace	nadace	k1gFnSc2
vždy	vždy	k6eAd1
císařovny	císařovna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ony	onen	k3xDgInPc4
jmenovaly	jmenovat	k5eAaBmAgInP,k5eAaImAgInP
vedoucí	vedoucí	k2eAgFnSc1d1
nadace	nadace	k1gFnSc1
<g/>
,	,	kIx,
byly	být	k5eAaImAgFnP
jimi	on	k3xPp3gMnPc7
dámy	dáma	k1gFnPc1
ze	z	k7c2
šlechtických	šlechtický	k2eAgInPc2d1
rodů	rod	k1gInPc2
–	–	k?
svobodné	svobodný	k2eAgFnPc1d1
paní	paní	k1gFnSc7
hraběnky	hraběnka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Novodobá	novodobý	k2eAgFnSc1d1
historie	historie	k1gFnSc1
</s>
<s>
Nová	nový	k2eAgFnSc1d1
výstavba	výstavba	k1gFnSc1
v	v	k7c6
Medlánkách	Medlánka	k1gFnPc6
</s>
<s>
Počátkem	počátkem	k7c2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byla	být	k5eAaImAgFnS
nadace	nadace	k1gFnSc1
přeměněna	přeměnit	k5eAaPmNgFnS
na	na	k7c4
Světský	světský	k2eAgInSc4d1
zaopatřovací	zaopatřovací	k2eAgInSc4d1
ústav	ústav	k1gInSc4
šlechtičen	šlechtična	k1gFnPc2
a	a	k8xC
v	v	k7c6
průběhu	průběh	k1gInSc6
století	století	k1gNnSc2
doznala	doznat	k5eAaPmAgFnS
řadu	řada	k1gFnSc4
změn	změna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1880	#num#	k4
užívala	užívat	k5eAaImAgFnS
titulu	titul	k1gInSc2
C.	C.	kA
k.	k.	k?
nadace	nadace	k1gFnSc2
šlechtičen	šlechtična	k1gFnPc2
(	(	kIx(
<g/>
C.	C.	kA
k.	k.	k?
Adelige	Adelige	k1gInSc1
Stiftung	Stiftung	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1913	#num#	k4
změnil	změnit	k5eAaPmAgInS
statut	statut	k1gInSc1
nadace	nadace	k1gFnSc2
a	a	k8xC
tato	tento	k3xDgFnSc1
dostala	dostat	k5eAaPmAgFnS
nový	nový	k2eAgInSc4d1
název	název	k1gInSc4
Adelige	Adelig	k1gInSc2
Stiftung	Stiftung	k1gInSc1
Maria	Maria	k1gFnSc1
Schul	schoulit	k5eAaPmRp2nS
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1918	#num#	k4
pak	pak	k6eAd1
<g/>
,	,	kIx,
nesprávným	správný	k2eNgInSc7d1
překladem	překlad	k1gInSc7
<g/>
,	,	kIx,
název	název	k1gInSc4
Dámský	dámský	k2eAgInSc4d1
nadační	nadační	k2eAgInSc4d1
ústav	ústav	k1gInSc4
Marie	Maria	k1gFnSc2
Školské	školská	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Výše	vysoce	k6eAd2
zmíněná	zmíněný	k2eAgFnSc1d1
nadace	nadace	k1gFnSc1
majetkově	majetkově	k6eAd1
přetrvala	přetrvat	k5eAaPmAgFnS
do	do	k7c2
roku	rok	k1gInSc2
1947	#num#	k4
<g/>
/	/	kIx~
<g/>
1949	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
její	její	k3xOp3gInSc4
majetek	majetek	k1gInSc4
v	v	k7c6
rámci	rámec	k1gInSc6
pozemkové	pozemkový	k2eAgFnSc2d1
reformy	reforma	k1gFnSc2
převzalo	převzít	k5eAaPmAgNnS
Ministerstvo	ministerstvo	k1gNnSc1
zemědělství	zemědělství	k1gNnSc2
ČSR	ČSR	kA
a	a	k8xC
oprávněným	oprávněný	k2eAgMnPc3d1
byl	být	k5eAaImAgMnS
z	z	k7c2
nadace	nadace	k1gFnSc2
vyplácen	vyplácen	k2eAgInSc4d1
sociální	sociální	k2eAgInSc4d1
důchod	důchod	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
nastalém	nastalý	k2eAgNnSc6d1
období	období	k1gNnSc6
kolektivizace	kolektivizace	k1gFnSc2
zemědělství	zemědělství	k1gNnSc2
bylo	být	k5eAaImAgNnS
v	v	k7c6
Medlánkách	Medlánka	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
1951	#num#	k4
ustaveno	ustavit	k5eAaPmNgNnS
Jednotné	jednotný	k2eAgNnSc1d1
zemědělské	zemědělský	k2eAgNnSc1d1
družstvo	družstvo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zprvu	zprvu	k6eAd1
je	on	k3xPp3gMnPc4
tvořili	tvořit	k5eAaImAgMnP
bezzemci	bezzemec	k1gMnPc1
a	a	k8xC
kovozemědělci	kovozemědělec	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
vstoupili	vstoupit	k5eAaPmAgMnP
i	i	k9
s	s	k7c7
majetkem	majetek	k1gInSc7
rolníci	rolník	k1gMnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
k	k	k7c3
tomu	ten	k3xDgNnSc3
donutila	donutit	k5eAaPmAgFnS
hospodářská	hospodářský	k2eAgFnSc1d1
úprava	úprava	k1gFnSc1
půdního	půdní	k2eAgInSc2d1
fondu	fond	k1gInSc2
a	a	k8xC
nemožnost	nemožnost	k1gFnSc4
samostatně	samostatně	k6eAd1
hospodařit	hospodařit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
JZD	JZD	kA
v	v	k7c6
Medlánkách	Medlánka	k1gFnPc6
prosperovalo	prosperovat	k5eAaImAgNnS
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1975	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
sloučilo	sloučit	k5eAaPmAgNnS
s	s	k7c7
JZD	JZD	kA
Komín	Komín	k1gInSc1
a	a	k8xC
v	v	k7c6
Medlánkách	Medlánka	k1gFnPc6
zůstal	zůstat	k5eAaPmAgInS
pouze	pouze	k6eAd1
pobočný	pobočný	k2eAgInSc1d1
závod	závod	k1gInSc1
s	s	k7c7
ubývající	ubývající	k2eAgFnSc7d1
rentabilitou	rentabilita	k1gFnSc7
výroby	výroba	k1gFnSc2
zeleniny	zelenina	k1gFnSc2
a	a	k8xC
květinářství	květinářství	k1gNnSc2
<g/>
,	,	kIx,
končící	končící	k2eAgNnSc1d1
do	do	k7c2
ztracena	ztraceno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zůstaly	zůstat	k5eAaPmAgInP
po	po	k7c6
něm	on	k3xPp3gInSc6
chátrající	chátrající	k2eAgFnSc1d1
a	a	k8xC
nevyužité	využitý	k2eNgFnPc1d1
budovy	budova	k1gFnPc1
někdejšího	někdejší	k2eAgInSc2d1
statku	statek	k1gInSc2
s	s	k7c7
přilehlými	přilehlý	k2eAgInPc7d1
pozemky	pozemek	k1gInPc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
probíhá	probíhat	k5eAaImIp3nS
rekonstrukce	rekonstrukce	k1gFnSc1
areálu	areál	k1gInSc2
a	a	k8xC
nová	nový	k2eAgFnSc1d1
výstavba	výstavba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
60	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc4
došlo	dojít	k5eAaPmAgNnS
při	při	k7c6
radikální	radikální	k2eAgFnSc6d1
druhé	druhý	k4xOgFnSc3
katastrální	katastrální	k2eAgFnSc3d1
reformě	reforma	k1gFnSc3
Brna	Brno	k1gNnSc2
k	k	k7c3
velkým	velký	k2eAgFnPc3d1
změnám	změna	k1gFnPc3
katastrální	katastrální	k2eAgFnSc2d1
hranice	hranice	k1gFnSc2
Medlánek	Medlánka	k1gFnPc2
<g/>
,	,	kIx,
při	při	k7c6
níž	jenž	k3xRgFnSc6
Medlánky	Medlánka	k1gFnPc1
ztratily	ztratit	k5eAaPmAgFnP
ve	v	k7c4
prospěch	prospěch	k1gInSc4
Řečkovic	Řečkovice	k1gFnPc2
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
jihovýchodní	jihovýchodní	k2eAgInSc4d1
cíp	cíp	k1gInSc4
původního	původní	k2eAgInSc2d1
katastru	katastr	k1gInSc2
s	s	k7c7
domy	dům	k1gInPc7
na	na	k7c6
východní	východní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
Kuřimské	kuřimský	k2eAgFnSc2d1
ulice	ulice	k1gFnSc2
i	i	k8xC
areál	areál	k1gInSc1
cihelny	cihelna	k1gFnSc2
<g/>
,	,	kIx,
ve	v	k7c4
prospěch	prospěch	k1gInSc4
Králova	Králův	k2eAgNnSc2d1
Pole	pole	k1gNnSc2
přišly	přijít	k5eAaPmAgInP
o	o	k7c4
nejjižnější	jižní	k2eAgFnSc4d3
část	část	k1gFnSc4
původního	původní	k2eAgInSc2d1
katastru	katastr	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
dnes	dnes	k6eAd1
nacházejí	nacházet	k5eAaImIp3nP
vysokoškolské	vysokoškolský	k2eAgFnPc1d1
koleje	kolej	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
tehdy	tehdy	k6eAd1
získaly	získat	k5eAaPmAgInP
lesní	lesní	k2eAgInSc4d1
pozemky	pozemek	k1gInPc4
na	na	k7c6
severovýchodě	severovýchod	k1gInSc6
původního	původní	k2eAgInSc2d1
katastru	katastr	k1gInSc2
Komína	Komín	k1gInSc2
či	či	k8xC
okrajovou	okrajový	k2eAgFnSc4d1
severní	severní	k2eAgFnSc4d1
část	část	k1gFnSc4
původního	původní	k2eAgInSc2d1
katastru	katastr	k1gInSc2
Králova	Králův	k2eAgNnSc2d1
Pole	pole	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Pokud	pokud	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
urbanistický	urbanistický	k2eAgInSc4d1
vývoj	vývoj	k1gInSc4
<g/>
,	,	kIx,
došlo	dojít	k5eAaPmAgNnS
po	po	k7c6
roce	rok	k1gInSc6
1968	#num#	k4
v	v	k7c6
Medlánkách	Medlánka	k1gFnPc6
k	k	k7c3
rozsáhlé	rozsáhlý	k2eAgFnSc3d1
změně	změna	k1gFnSc3
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
díky	díky	k7c3
iniciativě	iniciativa	k1gFnSc3
několika	několik	k4yIc2
mladých	mladý	k2eAgMnPc2d1
nadšenců	nadšenec	k1gMnPc2
(	(	kIx(
<g/>
bratři	bratr	k1gMnPc1
Hlaváčové	Hlaváčové	k2eAgFnSc2d1
<g/>
,	,	kIx,
Ing.	ing.	kA
Sedlák	sedlák	k1gInSc1
<g/>
,	,	kIx,
paní	paní	k1gFnSc1
Pokorná	Pokorná	k1gFnSc1
a	a	k8xC
další	další	k2eAgFnPc1d1
<g/>
)	)	kIx)
zde	zde	k6eAd1
vzniklo	vzniknout	k5eAaPmAgNnS
sídliště	sídliště	k1gNnSc2
Jabloňová	jabloňový	k2eAgFnSc1d1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
se	se	k3xPyFc4
z	z	k7c2
části	část	k1gFnSc2
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
pozemcích	pozemek	k1gInPc6
původně	původně	k6eAd1
náležejících	náležející	k2eAgMnPc2d1
ke	k	k7c3
Královu	Králův	k2eAgNnSc3d1
Poli	pole	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
dostavbě	dostavba	k1gFnSc6
(	(	kIx(
<g/>
návrh	návrh	k1gInSc1
Ing.	ing.	kA
arch	arch	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fuchse	Fuchs	k1gMnSc2
a	a	k8xC
Ing.	ing.	kA
arch	arch	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozehnala	Rozehnal	k1gMnSc4
<g/>
)	)	kIx)
se	se	k3xPyFc4
počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
téměř	téměř	k6eAd1
ztrojnásobil	ztrojnásobit	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
Medlánkách	Medlánka	k1gFnPc6
dostavovalo	dostavovat	k5eAaImAgNnS
nové	nový	k2eAgNnSc1d1
moderně	moderně	k6eAd1
koncipované	koncipovaný	k2eAgNnSc1d1
sídliště	sídliště	k1gNnSc1
V	v	k7c6
Újezdech	Újezd	k1gInPc6
<g/>
,	,	kIx,
budované	budovaný	k2eAgFnSc6d1
v	v	k7c6
rámci	rámec	k1gInSc6
projektů	projekt	k1gInPc2
Kouzelné	kouzelný	k2eAgFnSc2d1
Medlánky	Medlánka	k1gFnSc2
a	a	k8xC
Nové	Nové	k2eAgInPc1d1
Medlánky	Medlánek	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
čtvrti	čtvrt	k1gFnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
jedna	jeden	k4xCgFnSc1
ze	z	k7c2
tří	tři	k4xCgFnPc2
českých	český	k2eAgFnPc2d1
SOS	sos	k1gInSc4
dětských	dětský	k2eAgFnPc2d1
vesniček	vesnička	k1gFnPc2
(	(	kIx(
<g/>
další	další	k2eAgFnPc1d1
jsou	být	k5eAaImIp3nP
v	v	k7c6
Doubí	doubí	k1gNnSc6
u	u	k7c2
Karlových	Karlův	k2eAgInPc2d1
Varů	Vary	k1gInPc2
a	a	k8xC
Chvalčově	Chvalčův	k2eAgFnSc6d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Pamětihodnosti	pamětihodnost	k1gFnPc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
v	v	k7c6
Brně-Medlánkách	Brně-Medlánka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Kříž	Kříž	k1gMnSc1
v	v	k7c6
zámeckém	zámecký	k2eAgInSc6d1
parku	park	k1gInSc6
</s>
<s>
Socha	socha	k1gFnSc1
Sv.	sv.	kA
Jana	Jan	k1gMnSc2
Nepomuckého	Nepomucký	k1gMnSc2
</s>
<s>
Státní	státní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
evidoval	evidovat	k5eAaImAgInS
roku	rok	k1gInSc2
1935	#num#	k4
v	v	k7c6
Medlánkách	Medlánka	k1gFnPc6
jako	jako	k8xS,k8xC
památky	památka	k1gFnSc2
následující	následující	k2eAgInPc1d1
objekty	objekt	k1gInPc1
<g/>
:	:	kIx,
zámek	zámek	k1gInSc1
<g/>
,	,	kIx,
zámecký	zámecký	k2eAgInSc1d1
park	park	k1gInSc1
<g/>
,	,	kIx,
panský	panský	k2eAgInSc1d1
dvůr	dvůr	k1gInSc1
<g/>
,	,	kIx,
sochu	socha	k1gFnSc4
sv.	sv.	kA
Jana	Jan	k1gMnSc2
Nepomuckého	Nepomucký	k1gMnSc2
<g/>
,	,	kIx,
zvonici	zvonice	k1gFnSc4
<g/>
,	,	kIx,
památník	památník	k1gInSc1
padlých	padlý	k1gMnPc2
<g/>
,	,	kIx,
dva	dva	k4xCgInPc4
kříže	kříž	k1gInPc4
a	a	k8xC
předvěké	předvěký	k2eAgNnSc4d1
naleziště	naleziště	k1gNnSc4
v	v	k7c6
cihelně	cihelna	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Zámek	zámek	k1gInSc1
a	a	k8xC
zámecký	zámecký	k2eAgInSc1d1
park	park	k1gInSc1
</s>
<s>
Původně	původně	k6eAd1
přízemní	přízemní	k2eAgFnSc1d1
budova	budova	k1gFnSc1
raně	raně	k6eAd1
barokního	barokní	k2eAgNnSc2d1
založení	založení	k1gNnSc2
v	v	k7c6
užívání	užívání	k1gNnSc6
Nadace	nadace	k1gFnSc2
Marie	Maria	k1gFnSc2
Školské	školská	k1gFnSc2
<g/>
,	,	kIx,
zvednutá	zvednutý	k2eAgFnSc1d1
o	o	k7c4
jedno	jeden	k4xCgNnSc4
patro	patro	k1gNnSc4
v	v	k7c6
pol.	pol.	k?
19	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
Park	park	k1gInSc1
v	v	k7c6
jižním	jižní	k2eAgNnSc6d1
průčelí	průčelí	k1gNnSc6
zámku	zámek	k1gInSc2
po	po	k7c6
vykácení	vykácení	k1gNnSc6
ovocné	ovocný	k2eAgFnSc2d1
zahrady	zahrada	k1gFnSc2
osázen	osázen	k2eAgInSc1d1
okrasným	okrasný	k2eAgNnSc7d1
stromovím	stromoví	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgInSc1d1
barokní	barokní	k2eAgInSc1d1
charakter	charakter	k1gInSc1
parku	park	k1gInSc6
průběhem	průběh	k1gInSc7
času	čas	k1gInSc2
nabyl	nabýt	k5eAaPmAgMnS
charakteru	charakter	k1gInSc3
parku	park	k1gInSc2
anglického	anglický	k2eAgInSc2d1
se	s	k7c7
vzrostlým	vzrostlý	k2eAgNnSc7d1
jehličnatým	jehličnatý	k2eAgNnSc7d1
a	a	k8xC
listnatým	listnatý	k2eAgNnSc7d1
stromovím	stromoví	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
přelomu	přelom	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
zásadní	zásadní	k2eAgFnSc3d1
rekonstrukci	rekonstrukce	k1gFnSc3
parku	park	k1gInSc2
–	–	k?
očištění	očištění	k1gNnSc1
od	od	k7c2
náletových	náletový	k2eAgFnPc2d1
dřevin	dřevina	k1gFnPc2
a	a	k8xC
výsadba	výsadba	k1gFnSc1
dřevin	dřevina	k1gFnPc2
okrasných	okrasný	k2eAgFnPc2d1
<g/>
.	.	kIx.
</s>
<s>
Panský	panský	k2eAgInSc1d1
Dvůr	Dvůr	k1gInSc1
</s>
<s>
Panský	panský	k2eAgInSc1d1
dvůr	dvůr	k1gInSc1
je	být	k5eAaImIp3nS
přízemní	přízemní	k2eAgFnSc1d1
barokní	barokní	k2eAgFnSc1d1
budova	budova	k1gFnSc1
<g/>
,	,	kIx,
ještě	ještě	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1935	#num#	k4
s	s	k7c7
krásnými	krásný	k2eAgFnPc7d1
vyklenutými	vyklenutý	k2eAgFnPc7d1
okenními	okenní	k2eAgFnPc7d1
mřížemi	mříž	k1gFnPc7
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
do	do	k7c2
podoby	podoba	k1gFnSc2
známé	známý	k2eAgFnSc2d1
z	z	k7c2
fotografií	fotografia	k1gFnPc2
dostavěna	dostavěn	k2eAgFnSc1d1
v	v	k7c6
letech	let	k1gInPc6
1765	#num#	k4
–	–	k?
1766	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
započala	započnout	k5eAaPmAgFnS
rekonstrukce	rekonstrukce	k1gFnSc1
dvora	dvůr	k1gInSc2
a	a	k8xC
v	v	k7c6
jeho	jeho	k3xOp3gFnSc6
části	část	k1gFnSc6
<g/>
,	,	kIx,
bývalé	bývalý	k2eAgFnSc6d1
sýpce	sýpka	k1gFnSc6
<g/>
,	,	kIx,
zbudováno	zbudován	k2eAgNnSc1d1
Společenské	společenský	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
Medlánek	Medlánek	k1gInSc1
–	–	k?
SÝPKA	sýpka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupně	postupně	k6eAd1
probíhají	probíhat	k5eAaImIp3nP
rovněž	rovněž	k9
přestavby	přestavba	k1gFnPc1
bočních	boční	k2eAgInPc2d1
traktů	trakt	k1gInPc2
budovy	budova	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Socha	socha	k1gFnSc1
sv.	sv.	kA
Jana	Jan	k1gMnSc2
Nepomuckého	Nepomucký	k1gMnSc2
</s>
<s>
Socha	socha	k1gFnSc1
sv.	sv.	kA
Jana	Jan	k1gMnSc2
Nepomuckého	Nepomucký	k1gMnSc2
je	být	k5eAaImIp3nS
hodnotná	hodnotný	k2eAgFnSc1d1
barokní	barokní	k2eAgFnSc1d1
plastika	plastika	k1gFnSc1
neznámého	známý	k2eNgMnSc2d1
autora	autor	k1gMnSc2
z	z	k7c2
r.	r.	kA
1750	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Zvonice	zvonice	k1gFnSc1
</s>
<s>
Zvonice	zvonice	k1gFnSc1
na	na	k7c6
nároží	nároží	k1gNnSc6
ul	ul	kA
<g/>
.	.	kIx.
Kytnerovy	Kytnerův	k2eAgFnPc1d1
a	a	k8xC
Suchého	Suchého	k2eAgFnSc1d1
postavena	postaven	k2eAgFnSc1d1
nepochybně	pochybně	k6eNd1
ve	v	k7c6
stejné	stejný	k2eAgFnSc6d1
době	doba	k1gFnSc6
jako	jako	k9
socha	socha	k1gFnSc1
sv.	sv.	kA
Jana	Jan	k1gMnSc2
Nepomuckého	Nepomucký	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svědčí	svědčit	k5eAaImIp3nS
o	o	k7c6
tom	ten	k3xDgNnSc6
zvon	zvon	k1gInSc1
odlitý	odlitý	k2eAgInSc1d1
roku	rok	k1gInSc2
1758	#num#	k4
firmou	firma	k1gFnSc7
I.	I.	kA
F.	F.	kA
Klietz	Klietz	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc1d1
nápis	nápis	k1gInSc1
na	na	k7c6
zvonu	zvon	k1gInSc6
Anna	Anna	k1gFnSc1
constantia	constantium	k1gNnSc2
freyn	freyna	k1gFnPc2
miniatin	miniatina	k1gFnPc2
zřejmě	zřejmě	k6eAd1
prokazuje	prokazovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gFnSc7
dárkyní	dárkyně	k1gFnSc7
byla	být	k5eAaImAgFnS
Svobodná	svobodný	k2eAgFnSc1d1
paní	paní	k1gFnSc1
Konstancie	Konstancie	k1gFnSc1
Miniatiová	Miniatiový	k2eAgFnSc1d1
z	z	k7c2
Campoli	Campole	k1gFnSc6
<g/>
,	,	kIx,
roz	roz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žalkovská	Žalkovský	k2eAgFnSc1d1
ze	z	k7c2
Žalkovic	Žalkovice	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
letech	let	k1gInPc6
1749	#num#	k4
–	–	k?
1759	#num#	k4
ředitelkou	ředitelka	k1gFnSc7
Nadace	nadace	k1gFnSc2
Marie	Maria	k1gFnSc2
Školské	školská	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
pro	pro	k7c4
Medlánky	Medlánka	k1gFnPc4
do	do	k7c2
roku	rok	k1gInSc2
1948	#num#	k4
pozemkovou	pozemkový	k2eAgFnSc7d1
vrchností	vrchnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
o	o	k7c6
ní	on	k3xPp3gFnSc6
nejsou	být	k5eNaImIp3nP
žádné	žádný	k3yNgFnPc4
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Kříže	kříž	k1gInPc1
</s>
<s>
Památkáři	památkář	k1gMnPc1
měli	mít	k5eAaImAgMnP
v	v	k7c6
Medlánkách	Medlánka	k1gFnPc6
evidovány	evidovat	k5eAaImNgFnP
dva	dva	k4xCgInPc4
<g/>
:	:	kIx,
železný	železný	k2eAgInSc1d1
na	na	k7c6
Kytnerově	Kytnerův	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
–	–	k?
na	na	k7c6
rozhraní	rozhraní	k1gNnSc6
s	s	k7c7
řečkovickým	řečkovický	k2eAgInSc7d1
katastrem	katastr	k1gInSc7
a	a	k8xC
dřevěný	dřevěný	k2eAgInSc4d1
<g/>
,	,	kIx,
v	v	k7c6
pozdější	pozdní	k2eAgFnSc6d2
době	doba	k1gFnSc6
nahrazený	nahrazený	k2eAgInSc1d1
kamenným	kamenný	k2eAgInSc7d1
<g/>
,	,	kIx,
na	na	k7c6
cestě	cesta	k1gFnSc6
k	k	k7c3
letišti	letiště	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třetí	třetí	k4xOgInSc1
<g/>
,	,	kIx,
v	v	k7c6
popisu	popis	k1gInSc6
památek	památka	k1gFnPc2
nezaznamenaný	zaznamenaný	k2eNgInSc1d1
<g/>
,	,	kIx,
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
Zámeckém	zámecký	k2eAgInSc6d1
parku	park	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Památník	památník	k1gInSc1
odboje	odboj	k1gInSc2
a	a	k8xC
padlým	padlý	k2eAgNnSc7d1
</s>
<s>
Památník	památník	k1gInSc1
odboje	odboj	k1gInSc2
a	a	k8xC
padlým	padlý	k1gMnSc7
byl	být	k5eAaImAgMnS
postaven	postavit	k5eAaPmNgMnS
k	k	k7c3
10	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc2
vzniku	vznik	k1gInSc2
ČSR	ČSR	kA
v	v	k7c6
roce	rok	k1gInSc6
1928	#num#	k4
z	z	k7c2
podnětu	podnět	k1gInSc2
místní	místní	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
Čs	čs	kA
<g/>
.	.	kIx.
obce	obec	k1gFnSc2
legionářské	legionářský	k2eAgFnPc1d1
za	za	k7c4
přispění	přispění	k1gNnSc4
ostatních	ostatní	k2eAgInPc2d1
medláneckých	medlánecký	k2eAgInPc2d1
spolků	spolek	k1gInPc2
a	a	k8xC
občanů	občan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
období	období	k1gNnSc6
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
–	–	k?
po	po	k7c6
příchodu	příchod	k1gInSc6
německých	německý	k2eAgInPc2d1
orgánů	orgán	k1gInPc2
–	–	k?
byl	být	k5eAaImAgMnS
rozebrán	rozebrán	k2eAgMnSc1d1
a	a	k8xC
na	na	k7c6
bezpečném	bezpečný	k2eAgNnSc6d1
místě	místo	k1gNnSc6
uložen	uložit	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
své	svůj	k3xOyFgFnSc6
původní	původní	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
však	však	k9
po	po	k7c6
válce	válka	k1gFnSc6
nebyl	být	k5eNaImAgInS
nalezen	naleznout	k5eAaPmNgInS,k5eAaBmNgInS
a	a	k8xC
proto	proto	k8xC
byl	být	k5eAaImAgMnS
při	při	k7c6
jeho	jeho	k3xOp3gFnSc6
opětovné	opětovný	k2eAgFnSc6d1
instalaci	instalace	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
<g/>
,	,	kIx,
nahrazen	nahradit	k5eAaPmNgInS
památníkem	památník	k1gInSc7
novým	nový	k2eAgInSc7d1
<g/>
.	.	kIx.
</s>
<s>
Předvěké	předvěký	k2eAgNnSc4d1
naleziště	naleziště	k1gNnSc4
v	v	k7c6
cihelně	cihelna	k1gFnSc6
</s>
<s>
Nálezy	nález	k1gInPc1
popsány	popsán	k2eAgInPc1d1
v	v	k7c6
části	část	k1gFnSc6
o	o	k7c6
prehistorii	prehistorie	k1gFnSc6
Medlánek	Medlánka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
změnám	změna	k1gFnPc3
hranic	hranice	k1gFnPc2
katastru	katastr	k1gInSc2
se	se	k3xPyFc4
území	území	k1gNnSc1
dnes	dnes	k6eAd1
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
katastrálním	katastrální	k2eAgNnSc6d1
území	území	k1gNnSc6
Řečkovice	Řečkovice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Zajímavost	zajímavost	k1gFnSc1
</s>
<s>
Jméno	jméno	k1gNnSc1
je	být	k5eAaImIp3nS
zdrobnělinou	zdrobnělina	k1gFnSc7
k	k	k7c3
předpokládanému	předpokládaný	k2eAgNnSc3d1
Medlany	Medlan	k1gMnPc4
–	–	k?
ves	ves	k1gFnSc1
lidí	člověk	k1gMnPc2
bydlících	bydlící	k2eAgMnPc2d1
u	u	k7c2
mdle	mdle	k6eAd1
tekoucí	tekoucí	k2eAgFnSc2d1
vody	voda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
protikladně	protikladně	k6eAd1
svým	svůj	k3xOyFgInSc7
základním	základní	k2eAgInSc7d1
významem	význam	k1gInSc7
k	k	k7c3
názvu	název	k1gInSc3
sousední	sousední	k2eAgFnSc2d1
Bystrce	Bystrc	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Historický	historický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
–	–	k?
1869	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
<g/>
.	.	kIx.
21	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
FLODROVÁ	FLODROVÁ	kA
<g/>
,	,	kIx,
Milena	Milena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
v	v	k7c6
proměnách	proměna	k1gFnPc6
času	čas	k1gInSc2
(	(	kIx(
<g/>
Malá	malý	k2eAgNnPc1d1
zamyšlení	zamyšlení	k1gNnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Šimon	Šimon	k1gMnSc1
Ryšavý	Ryšavý	k1gMnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
179	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86137	#num#	k4
<g/>
-	-	kIx~
<g/>
79	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Medlánky	Medlánka	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
142	#num#	k4
<g/>
-	-	kIx~
<g/>
145	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
FC	FC	kA
Medlánky	Medlánek	k1gInPc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Medlánky	Medlánka	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
</s>
<s>
Drobná	drobný	k2eAgFnSc1d1
sakrální	sakrální	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
v	v	k7c6
Medlánkách	Medlánka	k1gFnPc6
</s>
<s>
Aeroklub	aeroklub	k1gInSc1
Medlánky	Medlánka	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normat	k5eAaPmAgInS,k5eAaImAgInS,k5eAaBmAgInS
<g/>
}	}	kIx)
Statutární	statutární	k2eAgNnSc4d1
město	město	k1gNnSc4
Brno	Brno	k1gNnSc4
Městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
</s>
<s>
Brno-Bohunice	Brno-Bohunice	k1gFnSc1
</s>
<s>
Brno-Bosonohy	Brno-Bosonoh	k1gInPc1
</s>
<s>
Brno-Bystrc	Brno-Bystrc	k6eAd1
</s>
<s>
Brno-Černovice	Brno-Černovice	k1gFnSc1
</s>
<s>
Brno-Chrlice	Brno-Chrlice	k1gFnSc1
</s>
<s>
Brno-Ivanovice	Brno-Ivanovice	k1gFnSc1
</s>
<s>
Brno-Jehnice	Brno-Jehnice	k1gFnSc1
</s>
<s>
Brno-jih	Brno-jih	k1gMnSc1
</s>
<s>
Brno-Jundrov	Brno-Jundrov	k1gInSc1
</s>
<s>
Brno-Kníničky	Brno-Knínička	k1gFnPc1
</s>
<s>
Brno-Kohoutovice	Brno-Kohoutovice	k1gFnSc1
</s>
<s>
Brno-Komín	Brno-Komín	k1gMnSc1
</s>
<s>
Brno-Královo	Brno-Králův	k2eAgNnSc1d1
Pole	pole	k1gNnSc1
</s>
<s>
Brno-Líšeň	Brno-Líšeň	k1gFnSc1
</s>
<s>
Brno-Maloměřice	Brno-Maloměřice	k1gFnPc1
a	a	k8xC
Obřany	Obřana	k1gFnPc1
</s>
<s>
Brno-Medlánky	Brno-Medlánka	k1gFnPc1
</s>
<s>
Brno-Nový	Brno-Nový	k2eAgInSc1d1
Lískovec	Lískovec	k1gInSc1
</s>
<s>
Brno-Ořešín	Brno-Ořešín	k1gMnSc1
</s>
<s>
Brno-Řečkovice	Brno-Řečkovice	k1gFnSc1
a	a	k8xC
Mokrá	mokrý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
</s>
<s>
Brno-sever	Brno-sever	k1gMnSc1
</s>
<s>
Brno-Slatina	Brno-Slatina	k1gFnSc1
</s>
<s>
Brno-Starý	Brno-Starý	k2eAgInSc1d1
Lískovec	Lískovec	k1gInSc1
</s>
<s>
Brno-střed	Brno-střed	k1gMnSc1
</s>
<s>
Brno-Tuřany	Brno-Tuřan	k1gMnPc4
</s>
<s>
Brno-Útěchov	Brno-Útěchov	k1gInSc1
</s>
<s>
Brno-Vinohrady	Brno-Vinohrada	k1gFnPc1
</s>
<s>
Brno-Žabovřesky	Brno-Žabovřesky	k6eAd1
</s>
<s>
Brno-Žebětín	Brno-Žebětín	k1gMnSc1
</s>
<s>
Brno-Židenice	Brno-Židenice	k1gFnSc1
Části	část	k1gFnSc2
obce	obec	k1gFnSc2
akatastrální	akatastrální	k2eAgFnSc4d1
území	území	k1gNnPc2
</s>
<s>
Bohunice	Bohunice	k1gFnPc1
</s>
<s>
Bosonohy	Bosonohy	k?
</s>
<s>
Brněnské	brněnský	k2eAgFnPc4d1
Ivanovice	Ivanovice	k1gFnPc4
</s>
<s>
Brno-město	Brno-město	k6eAd1
(	(	kIx(
<g/>
k.	k.	k?
ú.	ú.	k?
Město	město	k1gNnSc1
Brno	Brno	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Bystrc	Bystrc	k1gFnSc1
</s>
<s>
Černá	černý	k2eAgFnSc1d1
Pole	pole	k1gFnSc1
</s>
<s>
Černovice	Černovice	k1gFnSc1
</s>
<s>
Dolní	dolní	k2eAgFnPc1d1
Heršpice	Heršpice	k1gFnPc1
</s>
<s>
Dvorska	Dvorska	k1gFnSc1
</s>
<s>
Holásky	Holásek	k1gMnPc4
</s>
<s>
Horní	horní	k2eAgFnPc1d1
Heršpice	Heršpice	k1gFnPc1
</s>
<s>
Husovice	Husovice	k1gFnPc1
</s>
<s>
Chrlice	Chrlice	k1gFnPc1
</s>
<s>
Ivanovice	Ivanovice	k1gFnPc1
</s>
<s>
Jehnice	jehnice	k1gFnSc1
</s>
<s>
Jundrov	Jundrov	k1gInSc1
</s>
<s>
Kníničky	Knínička	k1gFnPc1
</s>
<s>
Kohoutovice	Kohoutovice	k1gFnPc1
</s>
<s>
Komárov	Komárov	k1gInSc1
</s>
<s>
Komín	Komín	k1gInSc1
</s>
<s>
Královo	Králův	k2eAgNnSc1d1
Pole	pole	k1gNnSc1
</s>
<s>
Lesná	lesný	k2eAgFnSc1d1
</s>
<s>
Líšeň	Líšeň	k1gFnSc1
</s>
<s>
Maloměřice	Maloměřice	k1gFnPc1
</s>
<s>
Medlánky	Medlánka	k1gFnPc1
</s>
<s>
Mokrá	mokrý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
</s>
<s>
Nový	nový	k2eAgInSc1d1
Lískovec	Lískovec	k1gInSc1
</s>
<s>
Obřany	Obřana	k1gFnPc1
</s>
<s>
Ořešín	Ořešín	k1gMnSc1
</s>
<s>
Pisárky	Pisárka	k1gFnPc1
</s>
<s>
Ponava	Ponava	k1gFnSc1
</s>
<s>
Přízřenice	Přízřenice	k1gFnSc1
</s>
<s>
Řečkovice	Řečkovice	k1gFnSc1
</s>
<s>
Sadová	sadový	k2eAgFnSc1d1
</s>
<s>
Slatina	slatina	k1gFnSc1
</s>
<s>
Soběšice	Soběšice	k1gFnSc1
</s>
<s>
Staré	Staré	k2eAgNnSc1d1
Brno	Brno	k1gNnSc1
</s>
<s>
Starý	starý	k2eAgInSc1d1
Lískovec	Lískovec	k1gInSc1
</s>
<s>
Stránice	Stránice	k1gFnSc1
</s>
<s>
Štýřice	Štýřice	k1gFnSc1
</s>
<s>
Trnitá	trnitý	k2eAgFnSc1d1
</s>
<s>
Tuřany	Tuřana	k1gFnPc1
</s>
<s>
Útěchov	Útěchov	k1gInSc1
(	(	kIx(
<g/>
k.	k.	k?
ú.	ú.	k?
Útěchov	Útěchov	k1gInSc1
u	u	k7c2
Brna	Brno	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Veveří	veveří	k2eAgNnSc4d1
</s>
<s>
Zábrdovice	Zábrdovice	k1gFnPc1
</s>
<s>
Žabovřesky	Žabovřesky	k1gFnPc1
</s>
<s>
Žebětín	Žebětín	k1gMnSc1
</s>
<s>
Židenice	Židenice	k1gFnPc1
Další	další	k2eAgFnPc1d1
čtvrtě	čtvrt	k1gFnPc1
</s>
<s>
Cacovice	Cacovice	k1gFnSc1
</s>
<s>
Černovičky	Černovička	k1gFnPc1
</s>
<s>
Divišova	Divišův	k2eAgFnSc1d1
čtvrť	čtvrť	k1gFnSc1
</s>
<s>
Jabloňová	jabloňový	k2eAgFnSc1d1
</s>
<s>
Juliánov	Juliánov	k1gInSc1
</s>
<s>
Kamechy	Kamech	k1gInPc1
</s>
<s>
Kamenná	kamenný	k2eAgFnSc1d1
kolonie	kolonie	k1gFnSc1
</s>
<s>
Kamenný	kamenný	k2eAgInSc1d1
Vrch	vrch	k1gInSc1
</s>
<s>
Kandie	Kandie	k1gFnSc1
</s>
<s>
Masarykova	Masarykův	k2eAgFnSc1d1
čtvrť	čtvrť	k1gFnSc1
</s>
<s>
Nové	Nové	k2eAgFnPc1d1
Černovice	Černovice	k1gFnPc1
</s>
<s>
Nové	Nové	k2eAgMnPc4d1
Moravany	Moravan	k1gMnPc4
</s>
<s>
Nový	nový	k2eAgInSc1d1
dům	dům	k1gInSc1
</s>
<s>
Písečník	písečník	k1gInSc1
</s>
<s>
Pod	pod	k7c7
vodojemem	vodojem	k1gInSc7
</s>
<s>
Slatinka	Slatinka	k1gFnSc1
</s>
<s>
Štefánikova	Štefánikův	k2eAgFnSc1d1
čtvrť	čtvrť	k1gFnSc1
Historická	historický	k2eAgFnSc1d1
předměstí	předměstí	k1gNnSc4
</s>
<s>
Augustiniánská	augustiniánský	k2eAgFnSc1d1
Ulice	ulice	k1gFnSc1
</s>
<s>
Červená	červený	k2eAgFnSc1d1
Ulice	ulice	k1gFnSc1
</s>
<s>
Dolní	dolní	k2eAgFnPc1d1
a	a	k8xC
Horní	horní	k2eAgInSc1d1
Cejl	Cejl	k1gInSc1
</s>
<s>
Josefov	Josefov	k1gInSc1
</s>
<s>
Kožená	kožený	k2eAgFnSc1d1
</s>
<s>
Křenová	křenový	k2eAgFnSc1d1
</s>
<s>
Křížová	Křížová	k1gFnSc1
</s>
<s>
Lužánky	Lužánka	k1gFnPc1
</s>
<s>
Malá	malý	k2eAgFnSc1d1
Mariacela	Mariacela	k1gFnSc1
</s>
<s>
Malá	malý	k2eAgFnSc1d1
Nová	nový	k2eAgFnSc1d1
Ulice	ulice	k1gFnSc1
</s>
<s>
Na	na	k7c4
Hrázi	hráze	k1gFnSc4
a	a	k8xC
Příkop	příkop	k1gInSc4
</s>
<s>
Náhon	náhon	k1gInSc1
</s>
<s>
Nové	Nové	k2eAgInPc1d1
Sady	sad	k1gInPc1
</s>
<s>
Pekařská	pekařský	k2eAgFnSc1d1
</s>
<s>
Růžový	růžový	k2eAgInSc1d1
</s>
<s>
Silniční	silniční	k2eAgInSc1d1
</s>
<s>
Špilberk	Špilberk	k1gInSc1
</s>
<s>
Švábka	švábka	k1gFnSc1
</s>
<s>
U	u	k7c2
Svaté	svatý	k2eAgFnSc2d1
Anny	Anna	k1gFnSc2
</s>
<s>
Ugartov	Ugartov	k1gInSc1
</s>
<s>
V	v	k7c6
Jirchářích	jirchář	k1gMnPc6
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Nová	nový	k2eAgFnSc1d1
Ulice	ulice	k1gFnSc1
</s>
<s>
Vinohrádky	vinohrádek	k1gInPc1
Zaniklé	zaniklý	k2eAgFnSc2d1
obce	obec	k1gFnSc2
</s>
<s>
Kamenný	kamenný	k2eAgInSc1d1
Mlýn	mlýn	k1gInSc1
</s>
<s>
Kníničky	Knínička	k1gFnPc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Brno	Brno	k1gNnSc1
</s>
