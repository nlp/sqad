<s>
Bor	bor	k1gInSc1	bor
(	(	kIx(	(
<g/>
též	též	k9	též
bór	bór	k1gInSc1	bór
<g/>
;	;	kIx,	;
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
B	B	kA	B
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Borum	Borum	k1gInSc1	Borum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejlehčím	lehký	k2eAgFnPc3d3	nejlehčí
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
prvků	prvek	k1gInPc2	prvek
III	III	kA	III
<g/>
.	.	kIx.	.
hlavní	hlavní	k2eAgFnPc1d1	hlavní
skupiny	skupina	k1gFnPc1	skupina
prvků	prvek	k1gInPc2	prvek
v	v	k7c6	v
periodické	periodický	k2eAgFnSc6d1	periodická
tabulce	tabulka	k1gFnSc6	tabulka
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
