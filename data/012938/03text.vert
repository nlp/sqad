<p>
<s>
Whisky	whisky	k1gFnSc1	whisky
je	být	k5eAaImIp3nS	být
alkoholický	alkoholický	k2eAgInSc4d1	alkoholický
nápoj	nápoj	k1gInSc4	nápoj
destilovaný	destilovaný	k2eAgInSc4d1	destilovaný
z	z	k7c2	z
obilnin	obilnina	k1gFnPc2	obilnina
<g/>
,	,	kIx,	,
zrající	zrající	k2eAgInPc1d1	zrající
v	v	k7c6	v
dřevěných	dřevěný	k2eAgInPc6d1	dřevěný
sudech	sud	k1gInPc6	sud
(	(	kIx(	(
<g/>
většinou	většina	k1gFnSc7	většina
dubových	dubový	k2eAgFnPc2d1	dubová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
znalců	znalec	k1gMnPc2	znalec
nechutnají	chutnat	k5eNaImIp3nP	chutnat
žádné	žádný	k3yNgFnPc1	žádný
dvě	dva	k4xCgFnPc1	dva
značky	značka	k1gFnPc1	značka
whisky	whisky	k1gFnPc2	whisky
stejně	stejně	k6eAd1	stejně
<g/>
.	.	kIx.	.
</s>
<s>
Převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
se	se	k3xPyFc4	se
ale	ale	k9	ale
whisky	whisky	k1gFnSc1	whisky
připravuje	připravovat	k5eAaImIp3nS	připravovat
destilací	destilace	k1gFnSc7	destilace
z	z	k7c2	z
kukuřice	kukuřice	k1gFnSc2	kukuřice
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
že	že	k8xS	že
míchané	míchaný	k2eAgFnPc1d1	míchaná
whisky	whisky	k1gFnPc1	whisky
jsou	být	k5eAaImIp3nP	být
podřadnější	podřadný	k2eAgFnPc1d2	podřadnější
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
whisky	whisky	k1gFnPc2	whisky
na	na	k7c6	na
světě	svět	k1gInSc6	svět
jsou	být	k5eAaImIp3nP	být
směsi	směs	k1gFnPc1	směs
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc1	druh
whisky	whisky	k1gFnSc2	whisky
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
světě	svět	k1gInSc6	svět
existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc1	několik
základních	základní	k2eAgInPc2d1	základní
druhů	druh	k1gInPc2	druh
whisky	whisky	k1gFnSc2	whisky
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
<g/>
,	,	kIx,	,
Walesu	Wales	k1gInSc6	Wales
a	a	k8xC	a
Kanadě	Kanada	k1gFnSc6	Kanada
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
označení	označení	k1gNnSc1	označení
whisky	whisky	k1gFnSc2	whisky
<g/>
,	,	kIx,	,
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
a	a	k8xC	a
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
whiskey	whiske	k2eAgFnPc1d1	whiske
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skotská	skotský	k2eAgFnSc1d1	skotská
whisky	whisky	k1gFnSc1	whisky
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
dvakrát	dvakrát	k6eAd1	dvakrát
destilovaná	destilovaný	k2eAgFnSc1d1	destilovaná
z	z	k7c2	z
ječmenného	ječmenný	k2eAgInSc2d1	ječmenný
sladu	slad	k1gInSc2	slad
<g/>
,	,	kIx,	,
sušeného	sušený	k2eAgInSc2d1	sušený
většinou	většina	k1gFnSc7	většina
kouřem	kouřmo	k1gNnPc2	kouřmo
z	z	k7c2	z
rašeliny	rašelina	k1gFnSc2	rašelina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Irská	irský	k2eAgFnSc1d1	irská
whiskey	whiskey	k1gInPc4	whiskey
<g/>
,	,	kIx,	,
třikrát	třikrát	k6eAd1	třikrát
destilovaná	destilovaný	k2eAgFnSc1d1	destilovaná
a	a	k8xC	a
minimálně	minimálně	k6eAd1	minimálně
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
skladovaná	skladovaný	k2eAgFnSc1d1	skladovaná
v	v	k7c6	v
dubových	dubový	k2eAgInPc6d1	dubový
sudech	sud	k1gInPc6	sud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Americká	americký	k2eAgFnSc1d1	americká
whiskey	whiske	k2eAgInPc1d1	whiske
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
destilovaná	destilovaný	k2eAgFnSc1d1	destilovaná
z	z	k7c2	z
kukuřice	kukuřice	k1gFnSc2	kukuřice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bourbon	bourbon	k1gInSc1	bourbon
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
whisky	whisky	k1gFnSc1	whisky
pocházející	pocházející	k2eAgFnSc1d1	pocházející
ze	z	k7c2	z
státu	stát	k1gInSc2	stát
Kentucky	Kentucka	k1gFnSc2	Kentucka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
nejméně	málo	k6eAd3	málo
51	[number]	k4	51
%	%	kIx~	%
kukuřice	kukuřice	k1gFnSc1	kukuřice
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Jim	on	k3xPp3gInPc3	on
Beam	Beam	k1gInPc3	Beam
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
zrání	zrání	k1gNnSc3	zrání
se	se	k3xPyFc4	se
ukládá	ukládat	k5eAaImIp3nS	ukládat
do	do	k7c2	do
výhradně	výhradně	k6eAd1	výhradně
nových	nový	k2eAgInPc2d1	nový
dubových	dubový	k2eAgInPc2d1	dubový
sudů	sud	k1gInPc2	sud
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
před	před	k7c7	před
použitím	použití	k1gNnSc7	použití
zevnitř	zevnitř	k6eAd1	zevnitř
požahovány	požahován	k2eAgInPc1d1	požahován
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
Bourbonu	bourbon	k1gInSc6	bourbon
dává	dávat	k5eAaImIp3nS	dávat
charakteristickou	charakteristický	k2eAgFnSc4d1	charakteristická
kouřově-dřevitou	kouřověřevitý	k2eAgFnSc4d1	kouřově-dřevitý
chuť	chuť	k1gFnSc4	chuť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Whisky	whisky	k1gFnSc1	whisky
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
amerických	americký	k2eAgInPc2d1	americký
států	stát	k1gInPc2	stát
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
neoznačuje	označovat	k5eNaImIp3nS	označovat
jako	jako	k9	jako
bourbon	bourbon	k1gInSc1	bourbon
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
podle	podle	k7c2	podle
amerických	americký	k2eAgFnPc2d1	americká
potravinářských	potravinářský	k2eAgFnPc2d1	potravinářská
norem	norma	k1gFnPc2	norma
bourbonem	bourbon	k1gInSc7	bourbon
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
nejznámější	známý	k2eAgInSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
Tennessee	Tennessee	k1gInSc1	Tennessee
whiskey	whiskea	k1gFnSc2	whiskea
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Jack	Jack	k1gMnSc1	Jack
Daniel	Daniel	k1gMnSc1	Daniel
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rye	Rye	k?	Rye
whisky	whisky	k1gFnSc1	whisky
<g/>
,	,	kIx,	,
americká	americký	k2eAgFnSc1d1	americká
žitná	žitný	k2eAgFnSc1d1	Žitná
whisky	whisky	k1gFnSc1	whisky
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnSc1d1	obsahující
min	min	kA	min
<g/>
.	.	kIx.	.
51	[number]	k4	51
<g/>
%	%	kIx~	%
žita	žito	k1gNnSc2	žito
</s>
</p>
<p>
<s>
Kanadská	kanadský	k2eAgFnSc1d1	kanadská
whisky	whisky	k1gFnSc1	whisky
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
jemnější	jemný	k2eAgFnSc1d2	jemnější
a	a	k8xC	a
z	z	k7c2	z
více	hodně	k6eAd2	hodně
druhů	druh	k1gInPc2	druh
obilovin	obilovina	k1gFnPc2	obilovina
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
obvykle	obvykle	k6eAd1	obvykle
žito	žit	k2eAgNnSc1d1	žito
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
v	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
množství	množství	k1gNnSc6	množství
<g/>
,	,	kIx,	,
výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
whisky	whisky	k1gFnSc1	whisky
Alberta	Albert	k1gMnSc2	Albert
Premium	Premium	k1gNnSc4	Premium
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
stoprocentní	stoprocentní	k2eAgFnSc4d1	stoprocentní
žitnou	žitná	k1gFnSc4	žitná
<g/>
.	.	kIx.	.
<g/>
U	u	k7c2	u
whisky	whisky	k1gFnSc2	whisky
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
rozeznáváno	rozeznáván	k2eAgNnSc4d1	rozeznáván
stáří	stáří	k1gNnSc4	stáří
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
řečeno	říct	k5eAaPmNgNnS	říct
doba	doba	k1gFnSc1	doba
zrání	zrání	k1gNnSc4	zrání
ve	v	k7c6	v
dřevěných	dřevěný	k2eAgInPc6d1	dřevěný
sudech	sud	k1gInPc6	sud
(	(	kIx(	(
<g/>
v	v	k7c6	v
lahvích	lahev	k1gFnPc6	lahev
totiž	totiž	k9	totiž
již	již	k6eAd1	již
whisky	whisky	k1gFnSc1	whisky
nezraje	zrát	k5eNaImIp3nS	zrát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skotské	skotský	k2eAgFnPc1d1	skotská
whisky	whisky	k1gFnPc1	whisky
mají	mít	k5eAaImIp3nP	mít
ze	z	k7c2	z
zákona	zákon	k1gInSc2	zákon
danou	daný	k2eAgFnSc4d1	daná
minimální	minimální	k2eAgFnSc4d1	minimální
dobu	doba	k1gFnSc4	doba
zrání	zrání	k1gNnSc3	zrání
3	[number]	k4	3
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgFnSc7d1	další
důležitou	důležitý	k2eAgFnSc7d1	důležitá
rozlišovací	rozlišovací	k2eAgFnSc7d1	rozlišovací
vlastností	vlastnost	k1gFnSc7	vlastnost
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
jednodruhovou	jednodruhový	k2eAgFnSc4d1	jednodruhová
(	(	kIx(	(
<g/>
single	singl	k1gInSc5	singl
malt	malta	k1gFnPc2	malta
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
směsnou	směsný	k2eAgFnSc4d1	směsná
whisky	whisky	k1gFnSc4	whisky
(	(	kIx(	(
<g/>
blend	blend	k1gInSc4	blend
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jednodruhové	jednodruhový	k2eAgFnPc1d1	jednodruhová
whisky	whisky	k1gFnPc1	whisky
jsou	být	k5eAaImIp3nP	být
oceňovány	oceňován	k2eAgFnPc1d1	oceňována
znalci	znalec	k1gMnSc3	znalec
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
originální	originální	k2eAgFnSc4d1	originální
chuť	chuť	k1gFnSc4	chuť
<g/>
,	,	kIx,	,
závislou	závislý	k2eAgFnSc4d1	závislá
na	na	k7c6	na
konkrétní	konkrétní	k2eAgFnSc6d1	konkrétní
palírně	palírna	k1gFnSc6	palírna
i	i	k9	i
roku	rok	k1gInSc2	rok
výroby	výroba	k1gFnSc2	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Směsné	směsný	k2eAgInPc1d1	směsný
jsou	být	k5eAaImIp3nP	být
oblíbeny	oblíben	k2eAgInPc1d1	oblíben
spotřebiteli	spotřebitel	k1gMnSc5	spotřebitel
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
díky	díky	k7c3	díky
kontrolovanému	kontrolovaný	k2eAgNnSc3d1	kontrolované
namíchání	namíchání	k1gNnSc3	namíchání
mají	mít	k5eAaImIp3nP	mít
vždy	vždy	k6eAd1	vždy
zaručenu	zaručen	k2eAgFnSc4d1	zaručena
stabilní	stabilní	k2eAgFnSc4d1	stabilní
kvalitu	kvalita	k1gFnSc4	kvalita
a	a	k8xC	a
chuť	chuť	k1gFnSc4	chuť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výroba	výroba	k1gFnSc1	výroba
==	==	k?	==
</s>
</p>
<p>
<s>
Při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
Irské	irský	k2eAgFnSc2d1	irská
whisky	whisky	k1gFnSc2	whisky
single	singl	k1gInSc5	singl
malt	malta	k1gFnPc2	malta
se	se	k3xPyFc4	se
obilí	obilí	k1gNnSc1	obilí
namáčí	namáčet	k5eAaImIp3nS	namáčet
v	v	k7c6	v
pramenité	pramenitý	k2eAgFnSc6d1	pramenitá
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
charakteristiku	charakteristika	k1gFnSc4	charakteristika
destilátu	destilát	k1gInSc2	destilát
(	(	kIx(	(
<g/>
tóny	tón	k1gInPc1	tón
rašeliny	rašelina	k1gFnSc2	rašelina
<g/>
,	,	kIx,	,
tóny	tón	k1gInPc1	tón
vřesu	vřes	k1gInSc2	vřes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zvlhlý	zvlhlý	k2eAgInSc1d1	zvlhlý
ječmen	ječmen	k1gInSc1	ječmen
se	se	k3xPyFc4	se
nechává	nechávat	k5eAaImIp3nS	nechávat
tradičním	tradiční	k2eAgInSc7d1	tradiční
způsobem	způsob	k1gInSc7	způsob
naklíčit	naklíčit	k5eAaPmF	naklíčit
na	na	k7c4	na
tzv.	tzv.	kA	tzv.
malting	malting	k1gInSc4	malting
floor	floora	k1gFnPc2	floora
(	(	kIx(	(
<g/>
humna	humno	k1gNnSc2	humno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
dnes	dnes	k6eAd1	dnes
už	už	k9	už
mnoho	mnoho	k4c1	mnoho
výrobců	výrobce	k1gMnPc2	výrobce
používá	používat	k5eAaImIp3nS	používat
mechanizované	mechanizovaný	k2eAgFnPc4d1	mechanizovaná
linky	linka	k1gFnPc4	linka
na	na	k7c4	na
sladování	sladování	k1gNnSc4	sladování
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
klíčení	klíčení	k1gNnSc2	klíčení
se	se	k3xPyFc4	se
škrob	škrob	k1gInSc1	škrob
v	v	k7c6	v
zrnu	zrno	k1gNnSc6	zrno
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
cukr	cukr	k1gInSc4	cukr
<g/>
,	,	kIx,	,
a	a	k8xC	a
vzniká	vznikat	k5eAaImIp3nS	vznikat
tak	tak	k6eAd1	tak
syrový	syrový	k2eAgInSc1d1	syrový
materiál	materiál	k1gInSc1	materiál
na	na	k7c4	na
kvašení	kvašení	k1gNnSc4	kvašení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyklíčení	vyklíčení	k1gNnSc6	vyklíčení
se	se	k3xPyFc4	se
obilí	obilí	k1gNnSc1	obilí
vysuší	vysušit	k5eAaPmIp3nS	vysušit
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
také	také	k9	také
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
chuť	chuť	k1gFnSc4	chuť
nápoje	nápoj	k1gInSc2	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Vysušené	vysušený	k2eAgNnSc1d1	vysušené
obilí	obilí	k1gNnSc1	obilí
nebo	nebo	k8xC	nebo
slad	slad	k1gInSc1	slad
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
pomele	pomlít	k5eAaPmIp3nS	pomlít
a	a	k8xC	a
zalije	zalít	k5eAaPmIp3nS	zalít
horkou	horký	k2eAgFnSc7d1	horká
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
obilná	obilný	k2eAgFnSc1d1	obilná
tekutina	tekutina	k1gFnSc1	tekutina
se	se	k3xPyFc4	se
přečerpá	přečerpat	k5eAaPmIp3nS	přečerpat
do	do	k7c2	do
nádrže	nádrž	k1gFnSc2	nádrž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
přidají	přidat	k5eAaPmIp3nP	přidat
kvasinky	kvasinka	k1gFnPc1	kvasinka
a	a	k8xC	a
kvasný	kvasný	k2eAgInSc1d1	kvasný
proces	proces	k1gInSc1	proces
může	moct	k5eAaImIp3nS	moct
začít	začít	k5eAaPmF	začít
<g/>
.	.	kIx.	.
</s>
<s>
Vykvašená	vykvašený	k2eAgFnSc1d1	vykvašená
tekutina	tekutina	k1gFnSc1	tekutina
<g/>
,	,	kIx,	,
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
také	také	k9	také
mash	mash	k1gInSc1	mash
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
základem	základ	k1gInSc7	základ
pro	pro	k7c4	pro
destilaci	destilace	k1gFnSc4	destilace
single	singl	k1gInSc5	singl
malt	malta	k1gFnPc2	malta
whisky	whisky	k1gFnSc1	whisky
<g/>
.	.	kIx.	.
<g/>
Skotská	skotský	k2eAgFnSc1d1	skotská
single	singl	k1gInSc5	singl
malt	malta	k1gFnPc2	malta
se	se	k3xPyFc4	se
destiluje	destilovat	k5eAaImIp3nS	destilovat
v	v	k7c6	v
obrovské	obrovský	k2eAgFnSc6d1	obrovská
měděné	měděný	k2eAgFnSc6d1	měděná
nádobě	nádoba	k1gFnSc6	nádoba
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
mash	mash	k1gMnSc1	mash
ohřívá	ohřívat	k5eAaImIp3nS	ohřívat
a	a	k8xC	a
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
destilát	destilát	k1gInSc4	destilát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
před	před	k7c7	před
uložením	uložení	k1gNnSc7	uložení
do	do	k7c2	do
dubových	dubový	k2eAgInPc2d1	dubový
sudů	sud	k1gInPc2	sud
na	na	k7c4	na
minimálně	minimálně	k6eAd1	minimálně
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
předestiluje	předestilovat	k5eAaPmIp3nS	předestilovat
ještě	ještě	k6eAd1	ještě
alespoň	alespoň	k9	alespoň
jednou	jeden	k4xCgFnSc7	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Dubové	Dubové	k2eAgInPc1d1	Dubové
sudy	sud	k1gInPc1	sud
na	na	k7c4	na
dozrávání	dozrávání	k1gNnSc4	dozrávání
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
použity	použít	k5eAaPmNgInP	použít
po	po	k7c6	po
dozrálém	dozrálý	k2eAgInSc6d1	dozrálý
bourbonu	bourbon	k1gInSc6	bourbon
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
sherry	sherry	k1gNnSc1	sherry
<g/>
,	,	kIx,	,
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
i	i	k8xC	i
po	po	k7c6	po
jiných	jiný	k2eAgInPc6d1	jiný
druzích	druh	k1gInPc6	druh
vína	víno	k1gNnSc2	víno
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
do	do	k7c2	do
whisky	whisky	k1gFnSc2	whisky
přináší	přinášet	k5eAaImIp3nS	přinášet
sladkost	sladkost	k1gFnSc4	sladkost
a	a	k8xC	a
charakteristickou	charakteristický	k2eAgFnSc4d1	charakteristická
chuť	chuť	k1gFnSc4	chuť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Název	název	k1gInSc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
whiskey	whiskea	k1gFnSc2	whiskea
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
irské	irský	k2eAgFnSc2d1	irská
gaelštiny	gaelština	k1gFnSc2	gaelština
uisge	uisg	k1gFnSc2	uisg
<g/>
/	/	kIx~	/
<g/>
uisce	uisce	k1gMnSc1	uisce
beatha	beatha	k1gMnSc1	beatha
(	(	kIx(	(
<g/>
[	[	kIx(	[
<g/>
ɪ	ɪ	k?	ɪ
bj	bj	k?	bj
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
voda	voda	k1gFnSc1	voda
života	život	k1gInSc2	život
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
fakt	fakt	k1gInSc1	fakt
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgFnS	používat
údajně	údajně	k6eAd1	údajně
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
léčivé	léčivý	k2eAgInPc4d1	léčivý
účinky	účinek	k1gInPc4	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Tvar	tvar	k1gInSc1	tvar
whisky	whisky	k1gFnSc2	whisky
(	(	kIx(	(
<g/>
plurál	plurál	k1gInSc1	plurál
whiskies	whiskies	k1gInSc1	whiskies
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
pro	pro	k7c4	pro
destiláty	destilát	k1gInPc4	destilát
ze	z	k7c2	z
Skotska	Skotsko	k1gNnSc2	Skotsko
<g/>
,	,	kIx,	,
Walesu	Wales	k1gInSc2	Wales
<g/>
,	,	kIx,	,
Kanady	Kanada	k1gFnSc2	Kanada
a	a	k8xC	a
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
původní	původní	k2eAgInSc1d1	původní
tvar	tvar	k1gInSc1	tvar
whiskey	whiskea	k1gFnSc2	whiskea
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
whisky	whisky	k1gFnSc2	whisky
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
whisky	whisky	k1gFnSc2	whisky
ve	v	k7c4	v
WikislovníkuWhiskipedia	WikislovníkuWhiskipedium	k1gNnPc4	WikislovníkuWhiskipedium
-	-	kIx~	-
The	The	k1gFnSc1	The
Whisky	whisky	k1gFnSc2	whisky
Encyclopedia	Encyclopedium	k1gNnSc2	Encyclopedium
</s>
</p>
<p>
<s>
WhiskyGrotto	WhiskyGrotto	k1gNnSc1	WhiskyGrotto
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
Whisky	whisky	k1gFnSc1	whisky
klub	klub	k1gInSc1	klub
</s>
</p>
<p>
<s>
Český	český	k2eAgMnSc1d1	český
whisky	whisky	k1gFnSc2	whisky
klub	klub	k1gInSc1	klub
"	"	kIx"	"
<g/>
50	[number]	k4	50
<g/>
of	of	k?	of
<g/>
5	[number]	k4	5
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Průvodce	průvodce	k1gMnSc1	průvodce
světem	svět	k1gInSc7	svět
whisky	whisky	k1gFnSc2	whisky
</s>
</p>
<p>
<s>
Single	singl	k1gInSc5	singl
Malt	Malta	k1gFnPc2	Malta
Whisky	whisky	k1gFnSc1	whisky
Club	club	k1gInSc1	club
-	-	kIx~	-
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
</s>
</p>
<p>
<s>
Originální	originální	k2eAgFnSc1d1	originální
whisky	whisky	k1gFnSc1	whisky
club	club	k1gInSc1	club
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
Whiskyonline	Whiskyonlin	k1gInSc5	Whiskyonlin
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
web	web	k1gInSc1	web
o	o	k7c4	o
whisky	whisky	k1gFnSc4	whisky
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Články	článek	k1gInPc1	článek
o	o	k7c4	o
whisky	whisky	k1gFnSc4	whisky
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Blog	Blog	k1gInSc1	Blog
o	o	k7c4	o
whisky	whisky	k1gFnSc4	whisky
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
České	český	k2eAgFnSc2d1	Česká
whisky	whisky	k1gFnSc2	whisky
forum	forum	k1gNnSc1	forum
</s>
</p>
