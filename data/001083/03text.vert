<s>
Andrea	Andrea	k1gFnSc1	Andrea
Pirlo	Pirlo	k1gNnSc4	Pirlo
(	(	kIx(	(
<g/>
*	*	kIx~	*
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
Flero	Flero	k1gNnSc1	Flero
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
italský	italský	k2eAgMnSc1d1	italský
fotbalista	fotbalista	k1gMnSc1	fotbalista
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
hrající	hrající	k2eAgFnSc6d1	hrající
za	za	k7c2	za
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
City	City	k1gFnSc2	City
FC	FC	kA	FC
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
i	i	k8xC	i
za	za	k7c4	za
italskou	italský	k2eAgFnSc4d1	italská
reprezentaci	reprezentace	k1gFnSc4	reprezentace
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
klasický	klasický	k2eAgInSc1d1	klasický
"	"	kIx"	"
<g/>
špílmachr	špílmachr	k1gInSc1	špílmachr
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc7	jeho
hlavní	hlavní	k2eAgFnSc7d1	hlavní
náplní	náplň	k1gFnSc7	náplň
je	být	k5eAaImIp3nS	být
tvořit	tvořit	k5eAaImF	tvořit
hru	hra	k1gFnSc4	hra
a	a	k8xC	a
snažit	snažit	k5eAaImF	snažit
se	se	k3xPyFc4	se
o	o	k7c4	o
překvapivé	překvapivý	k2eAgInPc4d1	překvapivý
pasy	pas	k1gInPc4	pas
(	(	kIx(	(
<g/>
přihrávky	přihrávka	k1gFnPc4	přihrávka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
podaří	podařit	k5eAaPmIp3nS	podařit
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
pro	pro	k7c4	pro
útočníky	útočník	k1gMnPc4	útočník
gólovou	gólový	k2eAgFnSc7d1	gólová
šanci	šance	k1gFnSc4	šance
<g/>
,	,	kIx,	,
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
"	"	kIx"	"
<g/>
squadře	squadra	k1gFnSc6	squadra
azzuře	azzura	k1gFnSc6	azzura
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
=	=	kIx~	=
italský	italský	k2eAgInSc4d1	italský
národní	národní	k2eAgInSc4d1	národní
tým	tým	k1gInSc4	tým
<g/>
)	)	kIx)	)
přezdívku	přezdívka	k1gFnSc4	přezdívka
"	"	kIx"	"
<g/>
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
architetto	architett	k2eAgNnSc1d1	architett
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
architekt	architekt	k1gMnSc1	architekt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Umí	umět	k5eAaImIp3nS	umět
také	také	k9	také
výborně	výborně	k6eAd1	výborně
zahrávat	zahrávat	k5eAaImF	zahrávat
přímé	přímý	k2eAgInPc4d1	přímý
volné	volný	k2eAgInPc4d1	volný
kopy	kop	k1gInPc4	kop
<g/>
.	.	kIx.	.
</s>
<s>
Pirlo	Pirnout	k5eAaPmAgNnS	Pirnout
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
ve	v	k7c6	v
Fleru	Fler	k1gInSc6	Fler
nedaleko	nedaleko	k7c2	nedaleko
Brescie	Brescie	k1gFnSc2	Brescie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začal	začít	k5eAaPmAgMnS	začít
s	s	k7c7	s
mládežnickým	mládežnický	k2eAgInSc7d1	mládežnický
fotbalem	fotbal	k1gInSc7	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
Seniorské	seniorský	k2eAgMnPc4d1	seniorský
premiéry	premiér	k1gMnPc4	premiér
se	se	k3xPyFc4	se
dočkal	dočkat	k5eAaPmAgMnS	dočkat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Brescia	Brescia	k1gFnSc1	Brescia
sestoupila	sestoupit	k5eAaPmAgFnS	sestoupit
z	z	k7c2	z
italské	italský	k2eAgFnSc2d1	italská
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
do	do	k7c2	do
Interu	Inter	k1gInSc2	Inter
Milán	Milán	k1gInSc1	Milán
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tří	tři	k4xCgFnPc2	tři
sezon	sezona	k1gFnPc2	sezona
v	v	k7c6	v
Interu	Intero	k1gNnSc6	Intero
však	však	k9	však
odehrál	odehrát	k5eAaPmAgInS	odehrát
pouze	pouze	k6eAd1	pouze
22	[number]	k4	22
utkání	utkání	k1gNnPc2	utkání
<g/>
,	,	kIx,	,
část	část	k1gFnSc4	část
angažmá	angažmá	k1gNnSc2	angažmá
strávil	strávit	k5eAaPmAgMnS	strávit
na	na	k7c4	na
hostování	hostování	k1gNnSc4	hostování
v	v	k7c6	v
Reggině	Reggina	k1gFnSc6	Reggina
a	a	k8xC	a
Brescii	Brescie	k1gFnSc6	Brescie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
Pirla	Pirlo	k1gNnSc2	Pirlo
koupil	koupit	k5eAaPmAgMnS	koupit
městský	městský	k2eAgMnSc1d1	městský
rival	rival	k1gMnSc1	rival
AC	AC	kA	AC
Milán	Milán	k1gInSc1	Milán
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgMnSc7	který
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
Italský	italský	k2eAgInSc4d1	italský
pohár	pohár	k1gInSc4	pohár
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
i	i	k9	i
ligový	ligový	k2eAgInSc4d1	ligový
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2003	[number]	k4	2003
a	a	k8xC	a
2007	[number]	k4	2007
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
s	s	k7c7	s
AC	AC	kA	AC
Milán	Milán	k1gInSc1	Milán
Ligu	liga	k1gFnSc4	liga
mistrů	mistr	k1gMnPc2	mistr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
ovšem	ovšem	k9	ovšem
Pirlo	Pirlo	k1gNnSc4	Pirlo
opustil	opustit	k5eAaPmAgMnS	opustit
svůj	svůj	k3xOyFgInSc4	svůj
dosavadní	dosavadní	k2eAgInSc4d1	dosavadní
klub	klub	k1gInSc4	klub
AC	AC	kA	AC
Milán	Milán	k1gInSc1	Milán
a	a	k8xC	a
přesunul	přesunout	k5eAaPmAgMnS	přesunout
se	se	k3xPyFc4	se
do	do	k7c2	do
dalšího	další	k2eAgInSc2d1	další
slavného	slavný	k2eAgInSc2d1	slavný
klubu	klub	k1gInSc2	klub
v	v	k7c6	v
Serii	serie	k1gFnSc6	serie
A	a	k9	a
<g/>
,	,	kIx,	,
do	do	k7c2	do
Juventusu	Juventus	k1gInSc2	Juventus
Turín	Turín	k1gInSc1	Turín
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Juventusem	Juventus	k1gInSc7	Juventus
získal	získat	k5eAaPmAgInS	získat
scudetto	scudett	k2eAgNnSc4d1	scudett
(	(	kIx(	(
<g/>
titul	titul	k1gInSc4	titul
v	v	k7c6	v
Serii	serie	k1gFnSc6	serie
A	a	k9	a
<g/>
)	)	kIx)	)
v	v	k7c6	v
sezónách	sezóna	k1gFnPc6	sezóna
2011	[number]	k4	2011
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
/	/	kIx~	/
<g/>
14	[number]	k4	14
a	a	k8xC	a
2014	[number]	k4	2014
<g/>
/	/	kIx~	/
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
z	z	k7c2	z
Inter	Intra	k1gFnPc2	Intra
Milan	Milana	k1gFnPc2	Milana
do	do	k7c2	do
AC	AC	kA	AC
Milan	Milana	k1gFnPc2	Milana
za	za	k7c2	za
18	[number]	k4	18
000	[number]	k4	000
000	[number]	k4	000
eur	euro	k1gNnPc2	euro
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
z	z	k7c2	z
AC	AC	kA	AC
Milan	Milana	k1gFnPc2	Milana
do	do	k7c2	do
Juventus	Juventus	k1gInSc1	Juventus
Turin	Turin	k2eAgInSc1d1	Turin
zadarmo	zadarmo	k6eAd1	zadarmo
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Itálii	Itálie	k1gFnSc6	Itálie
Pirlo	Pirlo	k1gNnSc4	Pirlo
reprezentoval	reprezentovat	k5eAaImAgInS	reprezentovat
již	již	k6eAd1	již
v	v	k7c6	v
mládežnických	mládežnický	k2eAgInPc6d1	mládežnický
výběrech	výběr	k1gInPc6	výběr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
týmu	tým	k1gInSc2	tým
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
hráčů	hráč	k1gMnPc2	hráč
do	do	k7c2	do
21	[number]	k4	21
let	léto	k1gNnPc2	léto
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
proti	proti	k7c3	proti
České	český	k2eAgFnSc3d1	Česká
republice	republika	k1gFnSc3	republika
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
oba	dva	k4xCgInPc4	dva
góly	gól	k1gInPc4	gól
svého	svůj	k3xOyFgInSc2	svůj
týmu	tým	k1gInSc2	tým
a	a	k8xC	a
zařídil	zařídit	k5eAaPmAgMnS	zařídit
tak	tak	k6eAd1	tak
vítězství	vítězství	k1gNnSc4	vítězství
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
také	také	k6eAd1	také
vyhlášen	vyhlásit	k5eAaPmNgMnS	vyhlásit
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
hráčem	hráč	k1gMnSc7	hráč
turnaje	turnaj	k1gInSc2	turnaj
a	a	k8xC	a
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
góly	gól	k1gInPc7	gól
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
i	i	k9	i
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
kanonýrem	kanonýr	k1gMnSc7	kanonýr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Letní	letní	k2eAgFnSc6d1	letní
olympiádě	olympiáda	k1gFnSc6	olympiáda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
získal	získat	k5eAaPmAgMnS	získat
s	s	k7c7	s
Itálií	Itálie	k1gFnSc7	Itálie
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dresu	dres	k1gInSc6	dres
seniorské	seniorský	k2eAgFnSc2d1	seniorská
reprezentace	reprezentace	k1gFnSc2	reprezentace
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2013	[number]	k4	2013
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
Konfederačního	konfederační	k2eAgInSc2d1	konfederační
poháru	pohár	k1gInSc2	pohár
FIFA	FIFA	kA	FIFA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
úvodním	úvodní	k2eAgInSc6d1	úvodní
zápase	zápas	k1gInSc6	zápas
italského	italský	k2eAgNnSc2d1	italské
mužstva	mužstvo	k1gNnSc2	mužstvo
vstřelil	vstřelit	k5eAaPmAgInS	vstřelit
krásný	krásný	k2eAgInSc1d1	krásný
gól	gól	k1gInSc1	gól
z	z	k7c2	z
přímého	přímý	k2eAgInSc2d1	přímý
kopu	kop	k1gInSc2	kop
proti	proti	k7c3	proti
Mexiku	Mexiko	k1gNnSc3	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Itálie	Itálie	k1gFnSc1	Itálie
soupeře	soupeř	k1gMnSc2	soupeř
porazila	porazit	k5eAaPmAgFnS	porazit
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
Pirla	Pirl	k1gMnSc4	Pirl
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgMnS	být
jubilejní	jubilejní	k2eAgMnSc1d1	jubilejní
stý	stý	k4xOgInSc4	stý
zápas	zápas	k1gInSc4	zápas
v	v	k7c6	v
dresu	dres	k1gInSc6	dres
národního	národní	k2eAgInSc2d1	národní
A-týmu	Aým	k1gInSc2	A-tým
<g/>
.	.	kIx.	.
</s>
<s>
Pirlo	Pirnout	k5eAaPmAgNnS	Pirnout
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
Mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
2012	[number]	k4	2012
a	a	k8xC	a
odehrál	odehrát	k5eAaPmAgInS	odehrát
všech	všecek	k3xTgNnPc2	všecek
šest	šest	k4xCc1	šest
utkání	utkání	k1gNnPc2	utkání
Itálie	Itálie	k1gFnSc2	Itálie
na	na	k7c6	na
šampionátu	šampionát	k1gInSc6	šampionát
při	při	k7c6	při
její	její	k3xOp3gFnSc6	její
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
skupině	skupina	k1gFnSc6	skupina
C	C	kA	C
to	ten	k3xDgNnSc1	ten
byly	být	k5eAaImAgInP	být
postupně	postupně	k6eAd1	postupně
zápasy	zápas	k1gInPc1	zápas
se	s	k7c7	s
Španělskem	Španělsko	k1gNnSc7	Španělsko
(	(	kIx(	(
<g/>
remíza	remíza	k1gFnSc1	remíza
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
nahrával	nahrávat	k5eAaImAgInS	nahrávat
na	na	k7c4	na
vedoucí	vedoucí	k2eAgInSc4d1	vedoucí
gól	gól	k1gInSc4	gól
Di	Di	k1gFnSc2	Di
Natalemu	Natalem	k1gInSc2	Natalem
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Chorvatskem	Chorvatsko	k1gNnSc7	Chorvatsko
(	(	kIx(	(
<g/>
rovněž	rovněž	k9	rovněž
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
gól	gól	k1gInSc4	gól
z	z	k7c2	z
přímého	přímý	k2eAgInSc2d1	přímý
kopu	kop	k1gInSc2	kop
<g/>
)	)	kIx)	)
a	a	k8xC	a
Irskem	Irsko	k1gNnSc7	Irsko
(	(	kIx(	(
<g/>
výhra	výhra	k1gFnSc1	výhra
Itálie	Itálie	k1gFnSc2	Itálie
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
přihrával	přihrávat	k5eAaImAgMnS	přihrávat
na	na	k7c4	na
vedoucí	vedoucí	k2eAgInSc4d1	vedoucí
gól	gól	k1gInSc4	gól
Antonio	Antonio	k1gMnSc1	Antonio
Cassanovi	Cassanův	k2eAgMnPc1d1	Cassanův
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
zápas	zápas	k1gInSc4	zápas
s	s	k7c7	s
Anglií	Anglie	k1gFnSc7	Anglie
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgInS	muset
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
až	až	k9	až
v	v	k7c6	v
penaltovém	penaltový	k2eAgInSc6d1	penaltový
rozstřelu	rozstřel	k1gInSc6	rozstřel
<g/>
.	.	kIx.	.
</s>
<s>
Andrea	Andrea	k1gFnSc1	Andrea
zakončil	zakončit	k5eAaPmAgMnS	zakončit
svůj	svůj	k3xOyFgInSc4	svůj
pokus	pokus	k1gInSc4	pokus
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
Antonína	Antonín	k1gMnSc2	Antonín
Panenky	panenka	k1gFnSc2	panenka
a	a	k8xC	a
rozstřel	rozstřel	k1gInSc4	rozstřel
skončil	skončit	k5eAaPmAgMnS	skončit
výsledkem	výsledek	k1gInSc7	výsledek
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
pro	pro	k7c4	pro
Italy	Ital	k1gMnPc4	Ital
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
byl	být	k5eAaImAgMnS	být
u	u	k7c2	u
výhry	výhra	k1gFnSc2	výhra
Itálie	Itálie	k1gFnSc2	Itálie
nad	nad	k7c7	nad
mírně	mírně	k6eAd1	mírně
favorizovaným	favorizovaný	k2eAgNnSc7d1	favorizované
Německem	Německo	k1gNnSc7	Německo
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
musel	muset	k5eAaImAgMnS	muset
se	s	k7c7	s
spoluhráči	spoluhráč	k1gMnPc7	spoluhráč
čelit	čelit	k5eAaImF	čelit
nejlepšímu	dobrý	k2eAgMnSc3d3	nejlepší
týmu	tým	k1gInSc6	tým
turnaje	turnaj	k1gInSc2	turnaj
Španělsku	Španělsko	k1gNnSc3	Španělsko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
nakonec	nakonec	k6eAd1	nakonec
Itálii	Itálie	k1gFnSc4	Itálie
deklasovalo	deklasovat	k5eAaBmAgNnS	deklasovat
poměrem	poměr	k1gInSc7	poměr
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Andrea	Andrea	k1gFnSc1	Andrea
získal	získat	k5eAaPmAgMnS	získat
s	s	k7c7	s
národním	národní	k2eAgInSc7d1	národní
týmem	tým	k1gInSc7	tým
stříbro	stříbro	k1gNnSc1	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Góly	gól	k1gInPc1	gól
Andrey	Andrea	k1gFnSc2	Andrea
Pirla	Pirlo	k1gNnSc2	Pirlo
za	za	k7c4	za
A-mužstvo	Aužstvo	k1gNnSc4	A-mužstvo
Itálie	Itálie	k1gFnSc2	Itálie
6	[number]	k4	6
<g/>
×	×	k?	×
vítěz	vítěz	k1gMnSc1	vítěz
italské	italský	k2eAgFnSc2d1	italská
ligy	liga	k1gFnSc2	liga
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
/	/	kIx~	/
<g/>
14	[number]	k4	14
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
/	/	kIx~	/
<g/>
15	[number]	k4	15
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
×	×	k?	×
vítěz	vítěz	k1gMnSc1	vítěz
italského	italský	k2eAgInSc2d1	italský
poháru	pohár	k1gInSc2	pohár
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
3	[number]	k4	3
<g/>
×	×	k?	×
vítěz	vítěz	k1gMnSc1	vítěz
italského	italský	k2eAgInSc2d1	italský
superpoháru	superpohár	k1gInSc2	superpohár
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
×	×	k?	×
vítěz	vítěz	k1gMnSc1	vítěz
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2002	[number]	k4	2002
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
×	×	k?	×
vítěz	vítěz	k1gMnSc1	vítěz
evropského	evropský	k2eAgInSc2d1	evropský
superpoháru	superpohár	k1gInSc2	superpohár
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
×	×	k?	×
vítěz	vítěz	k1gMnSc1	vítěz
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
klubů	klub	k1gInPc2	klub
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
×	×	k?	×
na	na	k7c6	na
MS	MS	kA	MS
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
2006	[number]	k4	2006
-	-	kIx~	-
zlato	zlato	k1gNnSc1	zlato
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
3	[number]	k4	3
<g/>
×	×	k?	×
na	na	k7c6	na
ME	ME	kA	ME
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
×	×	k?	×
na	na	k7c4	na
ME	ME	kA	ME
21	[number]	k4	21
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
-	-	kIx~	-
zlato	zlato	k1gNnSc1	zlato
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
-	-	kIx~	-
bronz	bronz	k1gInSc4	bronz
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
×	×	k?	×
na	na	k7c6	na
OH	OH	kA	OH
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
2004	[number]	k4	2004
-	-	kIx~	-
bronz	bronz	k1gInSc4	bronz
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
×	×	k?	×
na	na	k7c6	na
konfederačním	konfederační	k2eAgInSc6d1	konfederační
poháru	pohár	k1gInSc6	pohár
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
×	×	k?	×
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
střelec	střelec	k1gMnSc1	střelec
ME	ME	kA	ME
U21	U21	k1gMnSc1	U21
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
-	-	kIx~	-
3	[number]	k4	3
góly	gól	k1gInPc4	gól
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
×	×	k?	×
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
hráč	hráč	k1gMnSc1	hráč
ME	ME	kA	ME
U21	U21	k1gMnSc1	U21
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
All	All	k1gFnSc1	All
Stars	Stars	k1gInSc4	Stars
Team	team	k1gInSc1	team
MS	MS	kA	MS
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
All	All	k1gFnSc1	All
Stars	Stars	k1gInSc4	Stars
Team	team	k1gInSc1	team
FIFPro	FIFPro	k1gNnSc1	FIFPro
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2014	[number]	k4	2014
vyšla	vyjít	k5eAaPmAgFnS	vyjít
jeho	jeho	k3xOp3gFnSc1	jeho
autobiografická	autobiografický	k2eAgFnSc1d1	autobiografická
kniha	kniha	k1gFnSc1	kniha
I	i	k8xC	i
Think	Think	k1gInSc1	Think
<g/>
,	,	kIx,	,
Therefore	Therefor	k1gInSc5	Therefor
I	i	k9	i
play	play	k0	play
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Myslím	myslet	k5eAaImIp1nS	myslet
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
hraji	hrát	k5eAaImIp1nS	hrát
-	-	kIx~	-
parafráze	parafráze	k1gFnSc1	parafráze
známého	známý	k2eAgInSc2d1	známý
výroku	výrok	k1gInSc2	výrok
"	"	kIx"	"
<g/>
Myslím	myslet	k5eAaImIp1nS	myslet
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
jsem	být	k5eAaImIp1nS	být
<g/>
"	"	kIx"	"
francouzského	francouzský	k2eAgMnSc4d1	francouzský
filosofa	filosof	k1gMnSc4	filosof
René	René	k1gMnSc1	René
Descartese	Descartese	k1gFnSc2	Descartese
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
shrnuje	shrnovat	k5eAaImIp3nS	shrnovat
zajímavosti	zajímavost	k1gFnPc4	zajímavost
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Přiznává	přiznávat	k5eAaImIp3nS	přiznávat
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
mj.	mj.	kA	mj.
obrovské	obrovský	k2eAgNnSc1d1	obrovské
zklamání	zklamání	k1gNnSc1	zklamání
po	po	k7c6	po
prohraném	prohraný	k2eAgNnSc6d1	prohrané
finále	finále	k1gNnSc6	finále
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
2004	[number]	k4	2004
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
proti	proti	k7c3	proti
anglickému	anglický	k2eAgMnSc3d1	anglický
klubu	klub	k1gInSc2	klub
Liverpool	Liverpool	k1gInSc1	Liverpool
FC	FC	kA	FC
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
AC	AC	kA	AC
Milán	Milán	k1gInSc1	Milán
ztratil	ztratit	k5eAaPmAgInS	ztratit
po	po	k7c6	po
poločase	poločas	k1gInSc6	poločas
tříbrankové	tříbrankový	k2eAgNnSc1d1	tříbrankové
vedení	vedení	k1gNnSc1	vedení
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
v	v	k7c6	v
penaltovém	penaltový	k2eAgInSc6d1	penaltový
rozstřelu	rozstřel	k1gInSc6	rozstřel
<g/>
.	.	kIx.	.
</s>
