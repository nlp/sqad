<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
brýlový	brýlový	k2eAgMnSc1d1	brýlový
(	(	kIx(	(
<g/>
Spheniscus	Spheniscus	k1gMnSc1	Spheniscus
demersus	demersus	k1gMnSc1	demersus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
také	také	k9	také
nazývaný	nazývaný	k2eAgMnSc1d1	nazývaný
tučňák	tučňák	k1gMnSc1	tučňák
brýlatý	brýlatý	k2eAgMnSc1d1	brýlatý
nebo	nebo	k8xC	nebo
africký	africký	k2eAgInSc1d1	africký
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgInSc1d1	jediný
druh	druh	k1gInSc1	druh
který	který	k3yIgInSc1	který
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
jižní	jižní	k2eAgFnSc2d1	jižní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Evropané	Evropan	k1gMnPc1	Evropan
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
seznámili	seznámit	k5eAaPmAgMnP	seznámit
už	už	k6eAd1	už
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ostatní	ostatní	k2eAgMnPc1d1	ostatní
tučňáci	tučňák	k1gMnPc1	tučňák
není	být	k5eNaImIp3nS	být
schopen	schopit	k5eAaPmNgInS	schopit
letu	let	k1gInSc3	let
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
uzpůsoben	uzpůsobit	k5eAaPmNgMnS	uzpůsobit
k	k	k7c3	k
potápění	potápění	k1gNnSc3	potápění
a	a	k8xC	a
životu	život	k1gInSc3	život
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Zrovna	zrovna	k6eAd1	zrovna
tak	tak	k9	tak
zde	zde	k6eAd1	zde
loví	lovit	k5eAaImIp3nP	lovit
potravu	potrava	k1gFnSc4	potrava
a	a	k8xC	a
živí	živit	k5eAaImIp3nS	živit
se	s	k7c7	s
převážně	převážně	k6eAd1	převážně
menšími	malý	k2eAgInPc7d2	menší
druhy	druh	k1gInPc7	druh
ryb	ryba	k1gFnPc2	ryba
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
ančovičky	ančovička	k1gFnPc4	ančovička
či	či	k8xC	či
sardinky	sardinka	k1gFnPc4	sardinka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dospělý	dospělý	k2eAgMnSc1d1	dospělý
jedinec	jedinec	k1gMnSc1	jedinec
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
výšky	výška	k1gFnSc2	výška
60	[number]	k4	60
<g/>
–	–	k?	–
<g/>
70	[number]	k4	70
cm	cm	kA	cm
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc6	hmotnost
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
4,5	[number]	k4	4,5
kg	kg	kA	kg
<g/>
.	.	kIx.	.
<g/>
Má	mít	k5eAaImIp3nS	mít
jednoduché	jednoduchý	k2eAgNnSc4d1	jednoduché
vakovité	vakovitý	k2eAgNnSc4d1	vakovité
tělo	tělo	k1gNnSc4	tělo
a	a	k8xC	a
tuhá	tuhý	k2eAgNnPc4d1	tuhé
křídla	křídlo	k1gNnPc4	křídlo
zploštěná	zploštěný	k2eAgNnPc4d1	zploštěné
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
ploutve	ploutev	k1gFnSc2	ploutev
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
něho	on	k3xPp3gMnSc4	on
dělá	dělat	k5eAaImIp3nS	dělat
dokonalého	dokonalý	k2eAgMnSc4d1	dokonalý
plavce	plavec	k1gMnSc4	plavec
a	a	k8xC	a
potápěče	potápěč	k1gMnSc4	potápěč
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
ponořit	ponořit	k5eAaPmF	ponořit
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
větší	veliký	k2eAgFnSc2d2	veliký
jak	jak	k8xS	jak
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
nezvykle	zvykle	k6eNd1	zvykle
teplém	teplý	k2eAgNnSc6d1	teplé
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
vybaven	vybaven	k2eAgInSc1d1	vybaven
je	být	k5eAaImIp3nS	být
neopeřenou	opeřený	k2eNgFnSc4d1	neopeřená
kůži	kůže	k1gFnSc4	kůže
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
zpravidla	zpravidla	k6eAd1	zpravidla
růžového	růžový	k2eAgInSc2d1	růžový
odstínu	odstín	k1gInSc2	odstín
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
žlázy	žláza	k1gFnPc4	žláza
obklopené	obklopený	k2eAgFnPc1d1	obklopená
cévami	céva	k1gFnPc7	céva
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
plní	plnit	k5eAaImIp3nP	plnit
termoregulační	termoregulační	k2eAgFnSc4d1	termoregulační
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yRnSc7	co
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
tělní	tělní	k2eAgFnSc1d1	tělní
teplota	teplota	k1gFnSc1	teplota
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
větší	veliký	k2eAgNnSc1d2	veliký
množství	množství	k1gNnSc1	množství
krve	krev	k1gFnSc2	krev
se	se	k3xPyFc4	se
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
žlázách	žláza	k1gFnPc6	žláza
nachází	nacházet	k5eAaImIp3nS	nacházet
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
tak	tak	k9	tak
ochlazena	ochladit	k5eAaPmNgFnS	ochladit
okolním	okolní	k2eAgInSc7d1	okolní
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Rozmnožuje	rozmnožovat	k5eAaImIp3nS	rozmnožovat
se	se	k3xPyFc4	se
i	i	k9	i
dvakrát	dvakrát	k6eAd1	dvakrát
během	během	k7c2	během
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
března	březen	k1gInSc2	březen
do	do	k7c2	do
května	květen	k1gInSc2	květen
(	(	kIx(	(
<g/>
Jižní	jižní	k2eAgFnSc1d1	jižní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
do	do	k7c2	do
prosince	prosinec	k1gInSc2	prosinec
(	(	kIx(	(
<g/>
Namibie	Namibie	k1gFnSc1	Namibie
<g/>
)	)	kIx)	)
položí	položit	k5eAaPmIp3nS	položit
samice	samice	k1gFnSc1	samice
zpravidla	zpravidla	k6eAd1	zpravidla
2	[number]	k4	2
vejce	vejce	k1gNnSc2	vejce
<g/>
,	,	kIx,	,
které	který	k3yIgFnSc3	který
pár	pár	k1gInSc1	pár
společně	společně	k6eAd1	společně
inkubuje	inkubovat	k5eAaImIp3nS	inkubovat
36	[number]	k4	36
až	až	k9	až
41	[number]	k4	41
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
vylíhlá	vylíhlý	k2eAgNnPc4d1	vylíhlé
mláďata	mládě	k1gNnPc4	mládě
rovněž	rovněž	k9	rovněž
pečují	pečovat	k5eAaImIp3nP	pečovat
oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
60	[number]	k4	60
až	až	k9	až
130	[number]	k4	130
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
odchov	odchov	k1gInSc4	odchov
se	se	k3xPyFc4	se
tučňák	tučňák	k1gMnSc1	tučňák
brýlový	brýlový	k2eAgMnSc1d1	brýlový
může	moct	k5eAaImIp3nS	moct
pokusit	pokusit	k5eAaPmF	pokusit
již	již	k6eAd1	již
v	v	k7c6	v
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
roce	rok	k1gInSc6	rok
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
pohlavně	pohlavně	k6eAd1	pohlavně
vyzrálý	vyzrálý	k2eAgInSc1d1	vyzrálý
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
v	v	k7c6	v
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
zranitelné	zranitelný	k2eAgInPc4d1	zranitelný
druhy	druh	k1gInPc4	druh
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jej	on	k3xPp3gInSc4	on
ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
kombinace	kombinace	k1gFnSc1	kombinace
hrozeb	hrozba	k1gFnPc2	hrozba
především	především	k6eAd1	především
lidského	lidský	k2eAgInSc2d1	lidský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgInPc4	který
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
smlouva	smlouva	k1gFnSc1	smlouva
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
africko-euroasijských	africkouroasijský	k2eAgMnPc2d1	africko-euroasijský
vodních	vodní	k2eAgMnPc2d1	vodní
ptáků	pták	k1gMnPc2	pták
(	(	kIx(	(
<g/>
AEWA	AEWA	kA	AEWA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2010	[number]	k4	2010
byl	být	k5eAaImAgInS	být
klasifikován	klasifikovat	k5eAaImNgInS	klasifikovat
jako	jako	k8xC	jako
ohrožený	ohrožený	k2eAgInSc1d1	ohrožený
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
ohrožených	ohrožený	k2eAgInPc6d1	ohrožený
druzích	druh	k1gInPc6	druh
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2013	[number]	k4	2013
byl	být	k5eAaImAgMnS	být
tučňák	tučňák	k1gMnSc1	tučňák
brýlový	brýlový	k2eAgMnSc1d1	brýlový
označen	označen	k2eAgMnSc1d1	označen
jako	jako	k8xC	jako
ohrožený	ohrožený	k2eAgMnSc1d1	ohrožený
na	na	k7c6	na
červeném	červený	k2eAgInSc6d1	červený
seznamu	seznam	k1gInSc6	seznam
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
druhů	druh	k1gInPc2	druh
(	(	kIx(	(
<g/>
IUCN	IUCN	kA	IUCN
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
==	==	k?	==
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
brýlový	brýlový	k2eAgMnSc1d1	brýlový
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
původně	původně	k6eAd1	původně
popsal	popsat	k5eAaPmAgMnS	popsat
švédský	švédský	k2eAgMnSc1d1	švédský
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
Carl	Carl	k1gMnSc1	Carl
Linné	Linné	k1gNnSc4	Linné
během	během	k7c2	během
roku	rok	k1gInSc2	rok
1758	[number]	k4	1758
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
10	[number]	k4	10
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc4	vydání
jeho	jeho	k3xOp3gInSc2	jeho
vědeckého	vědecký	k2eAgInSc2d1	vědecký
systému	systém	k1gInSc2	systém
zvaného	zvaný	k2eAgMnSc2d1	zvaný
Systema	System	k1gMnSc2	System
Naturae	Natura	k1gMnSc2	Natura
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gInSc4	on
na	na	k7c6	na
základě	základ	k1gInSc6	základ
svého	svůj	k3xOyFgNnSc2	svůj
morfologického	morfologický	k2eAgNnSc2d1	morfologické
zkoumání	zkoumání	k1gNnSc2	zkoumání
a	a	k8xC	a
především	především	k9	především
podobnosti	podobnost	k1gFnPc1	podobnost
nozder	nozdra	k1gFnPc2	nozdra
seskupil	seskupit	k5eAaPmAgInS	seskupit
např.	např.	kA	např.
s	s	k7c7	s
albatrosem	albatros	k1gMnSc7	albatros
<g/>
,	,	kIx,	,
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
mu	on	k3xPp3gMnSc3	on
jméno	jméno	k1gNnSc4	jméno
Diomedea	Diomedeus	k1gMnSc4	Diomedeus
demersa	demers	k1gMnSc2	demers
<g/>
.	.	kIx.	.
<g/>
Tento	tento	k3xDgMnSc1	tento
africký	africký	k2eAgMnSc1d1	africký
tučňák	tučňák	k1gMnSc1	tučňák
je	být	k5eAaImIp3nS	být
zařazen	zařadit	k5eAaPmNgMnS	zařadit
do	do	k7c2	do
rodu	rod	k1gInSc2	rod
Spheniscus	Spheniscus	k1gInSc4	Spheniscus
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
ostatní	ostatní	k2eAgMnPc4d1	ostatní
tropické	tropický	k2eAgMnPc4d1	tropický
pruhovité	pruhovitý	k2eAgMnPc4d1	pruhovitý
tučňáky	tučňák	k1gMnPc4	tučňák
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gNnSc1	jeho
nejbližší	blízký	k2eAgMnPc1d3	nejbližší
příbuzní	příbuzný	k1gMnPc1	příbuzný
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
tučňáka	tučňák	k1gMnSc2	tučňák
brýlového	brýlový	k2eAgMnSc4d1	brýlový
žijí	žít	k5eAaImIp3nP	žít
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
Humboldtův	Humboldtův	k2eAgMnSc1d1	Humboldtův
a	a	k8xC	a
tučňák	tučňák	k1gMnSc1	tučňák
magellanský	magellanský	k2eAgMnSc1d1	magellanský
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
a	a	k8xC	a
tučňák	tučňák	k1gMnSc1	tučňák
galapážský	galapážský	k2eAgMnSc1d1	galapážský
kolem	kolem	k7c2	kolem
tichého	tichý	k2eAgInSc2d1	tichý
oceánu	oceán	k1gInSc2	oceán
u	u	k7c2	u
rovníku	rovník	k1gInSc2	rovník
na	na	k7c6	na
Galapážských	galapážský	k2eAgInPc6d1	galapážský
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
mají	mít	k5eAaImIp3nP	mít
podobné	podobný	k2eAgNnSc4d1	podobné
zbarvení	zbarvení	k1gNnSc4	zbarvení
<g/>
,	,	kIx,	,
parametry	parametr	k1gInPc4	parametr
i	i	k8xC	i
biologické	biologický	k2eAgNnSc4d1	biologické
chování	chování	k1gNnSc4	chování
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
třídy	třída	k1gFnSc2	třída
Aves	Avesa	k1gFnPc2	Avesa
(	(	kIx(	(
<g/>
ptáci	pták	k1gMnPc1	pták
<g/>
)	)	kIx)	)
a	a	k8xC	a
řádu	řád	k1gInSc2	řád
Sphenisciformes	Sphenisciformesa	k1gFnPc2	Sphenisciformesa
(	(	kIx(	(
<g/>
tučňáci	tučňák	k1gMnPc1	tučňák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
tučňáků	tučňák	k1gMnPc2	tučňák
Spheniscidae	Spheniscidae	k1gFnSc1	Spheniscidae
(	(	kIx(	(
<g/>
tučňákovití	tučňákovitý	k2eAgMnPc1d1	tučňákovitý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klasifikován	klasifikován	k2eAgInSc1d1	klasifikován
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
Spheniscus	Spheniscus	k1gMnSc1	Spheniscus
demersus	demersus	k1gMnSc1	demersus
<g/>
.	.	kIx.	.
</s>
<s>
Rodové	rodový	k2eAgNnSc1d1	rodové
jméno	jméno	k1gNnSc1	jméno
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
starověkého	starověký	k2eAgNnSc2d1	starověké
řeckého	řecký	k2eAgNnSc2d1	řecké
slova	slovo	k1gNnSc2	slovo
sphen	sphna	k1gFnPc2	sphna
–	–	k?	–
neboli	neboli	k8xC	neboli
"	"	kIx"	"
<g/>
klín	klín	k1gInSc1	klín
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
poukazující	poukazující	k2eAgMnSc1d1	poukazující
na	na	k7c4	na
jejich	jejich	k3xOp3gInSc4	jejich
jednoduchý	jednoduchý	k2eAgInSc4d1	jednoduchý
tvar	tvar	k1gInSc4	tvar
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Druhové	druhový	k2eAgNnSc1d1	druhové
jméno	jméno	k1gNnSc1	jméno
demersus	demersus	k1gInSc1	demersus
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
latinského	latinský	k2eAgInSc2d1	latinský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
ponořit	ponořit	k5eAaPmF	ponořit
se	s	k7c7	s
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
brýlový	brýlový	k2eAgMnSc1d1	brýlový
nevyvádí	vyvádět	k5eNaImIp3nS	vyvádět
mladé	mladý	k2eAgNnSc1d1	mladé
na	na	k7c6	na
sněhu	sníh	k1gInSc6	sníh
a	a	k8xC	a
ledu	led	k1gInSc6	led
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
písčitých	písčitý	k2eAgFnPc6d1	písčitá
nebo	nebo	k8xC	nebo
kamenitých	kamenitý	k2eAgFnPc6d1	kamenitá
plážích	pláž	k1gFnPc6	pláž
v	v	k7c6	v
tropických	tropický	k2eAgFnPc6d1	tropická
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbenými	oblíbený	k2eAgNnPc7d1	oblíbené
místy	místo	k1gNnPc7	místo
pro	pro	k7c4	pro
hnízdění	hnízdění	k1gNnSc4	hnízdění
jsou	být	k5eAaImIp3nP	být
odlehlé	odlehlý	k2eAgInPc1d1	odlehlý
ostrůvky	ostrůvek	k1gInPc1	ostrůvek
nebo	nebo	k8xC	nebo
nepřístupné	přístupný	k2eNgInPc4d1	nepřístupný
zátoky	zátok	k1gInPc4	zátok
obklopeny	obklopen	k2eAgInPc4d1	obklopen
velkými	velký	k2eAgInPc7d1	velký
balvany	balvan	k1gInPc7	balvan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hnízdící	hnízdící	k2eAgFnSc1d1	hnízdící
kolonie	kolonie	k1gFnSc1	kolonie
si	se	k3xPyFc3	se
zakládá	zakládat	k5eAaImIp3nS	zakládat
na	na	k7c6	na
24	[number]	k4	24
ostrůvcích	ostrůvek	k1gInPc6	ostrůvek
u	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
Jižní	jižní	k2eAgFnSc2d1	jižní
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
na	na	k7c6	na
třech	tři	k4xCgNnPc6	tři
pevninských	pevninský	k2eAgNnPc6d1	pevninské
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
u	u	k7c2	u
Hollamsbird	Hollamsbirda	k1gFnPc2	Hollamsbirda
Island	Island	k1gInSc1	Island
v	v	k7c6	v
Namibii	Namibie	k1gFnSc6	Namibie
a	a	k8xC	a
u	u	k7c2	u
Bird	Birda	k1gFnPc2	Birda
Island	Island	k1gInSc1	Island
a	a	k8xC	a
Algoa	Algo	k2eAgFnSc1d1	Algo
Bay	Bay	k1gFnSc1	Bay
v	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Rozsah	rozsah	k1gInSc1	rozsah
výskytu	výskyt	k1gInSc2	výskyt
obrazně	obrazně	k6eAd1	obrazně
obepíná	obepínat	k5eAaImIp3nS	obepínat
prakticky	prakticky	k6eAd1	prakticky
celou	celý	k2eAgFnSc4d1	celá
pobřežní	pobřežní	k2eAgFnSc4d1	pobřežní
část	část	k1gFnSc4	část
jižní	jižní	k2eAgFnSc2d1	jižní
Afriky	Afrika	k1gFnSc2	Afrika
s	s	k7c7	s
někdy	někdy	k6eAd1	někdy
nepatrnou	nepatrný	k2eAgFnSc7d1	nepatrná
nebo	nebo	k8xC	nebo
rozestřenou	rozestřený	k2eAgFnSc7d1	rozestřená
populací	populace	k1gFnSc7	populace
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yInSc7	co
blíže	blízce	k6eAd2	blízce
k	k	k7c3	k
jihu	jih	k1gInSc2	jih
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
zpravidla	zpravidla	k6eAd1	zpravidla
větší	veliký	k2eAgFnSc1d2	veliký
kolonie	kolonie	k1gFnSc1	kolonie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Turisticky	turisticky	k6eAd1	turisticky
známá	známý	k2eAgFnSc1d1	známá
kolonie	kolonie	k1gFnSc1	kolonie
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c4	na
březích	březí	k2eAgInPc2d1	březí
Lambert	Lambert	k1gInSc4	Lambert
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Bay	Bay	k1gFnSc7	Bay
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc1	oblast
ležící	ležící	k2eAgFnSc1d1	ležící
přibližně	přibližně	k6eAd1	přibližně
250	[number]	k4	250
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
Kapského	kapský	k2eAgNnSc2d1	Kapské
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejsevernější	severní	k2eAgFnSc1d3	nejsevernější
kolonie	kolonie	k1gFnSc1	kolonie
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
v	v	k7c6	v
nedávné	dávný	k2eNgFnSc6d1	nedávná
době	doba	k1gFnSc6	doba
úplně	úplně	k6eAd1	úplně
vymizela	vymizet	k5eAaPmAgFnS	vymizet
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k1gNnSc1	další
je	být	k5eAaImIp3nS	být
jižním	jižní	k2eAgInSc7d1	jižní
směrem	směr	k1gInSc7	směr
zhruba	zhruba	k6eAd1	zhruba
50	[number]	k4	50
km	km	kA	km
od	od	k7c2	od
Kapského	kapský	k2eAgNnSc2d1	Kapské
město	město	k1gNnSc1	město
na	na	k7c6	na
plážích	pláž	k1gFnPc6	pláž
"	"	kIx"	"
<g/>
balvanů	balvan	k1gInPc2	balvan
<g/>
"	"	kIx"	"
-	-	kIx~	-
Boulders	Boulders	k1gInSc1	Boulders
Beach	Beach	k1gInSc1	Beach
(	(	kIx(	(
<g/>
2	[number]	k4	2
km	km	kA	km
od	od	k7c2	od
městečka	městečko	k1gNnSc2	městečko
Simon	Simona	k1gFnPc2	Simona
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Town	Towna	k1gFnPc2	Towna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
plážích	pláž	k1gFnPc6	pláž
Boulders	Bouldersa	k1gFnPc2	Bouldersa
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
setkat	setkat	k5eAaPmF	setkat
se	se	k3xPyFc4	se
s	s	k7c7	s
tučňáky	tučňák	k1gMnPc7	tučňák
takřka	takřka	k6eAd1	takřka
tváří	tvářet	k5eAaImIp3nS	tvářet
v	v	k7c4	v
tvář	tvář	k1gFnSc4	tvář
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zde	zde	k6eAd1	zde
neexistuje	existovat	k5eNaImIp3nS	existovat
žádná	žádný	k3yNgFnSc1	žádný
dělící	dělící	k2eAgFnSc1d1	dělící
zóna	zóna	k1gFnSc1	zóna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
jejich	jejich	k3xOp3gFnSc4	jejich
blízkost	blízkost	k1gFnSc4	blízkost
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
prakticky	prakticky	k6eAd1	prakticky
i	i	k9	i
koupat	koupat	k5eAaImF	koupat
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
kolonie	kolonie	k1gFnPc1	kolonie
hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
třeba	třeba	k6eAd1	třeba
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
Stony	ston	k1gInPc4	ston
Point	pointa	k1gFnPc2	pointa
nebo	nebo	k8xC	nebo
Betty	Betty	k1gFnPc2	Betty
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Bay	Bay	k1gMnSc7	Bay
<g/>
.	.	kIx.	.
<g/>
Při	při	k7c6	při
případné	případný	k2eAgFnSc6d1	případná
migraci	migrace	k1gFnSc6	migrace
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
nevyhovujících	vyhovující	k2eNgFnPc2d1	nevyhovující
podmínek	podmínka	k1gFnPc2	podmínka
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
zatoulat	zatoulat	k5eAaPmF	zatoulat
až	až	k9	až
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
Konga	Kongo	k1gNnSc2	Kongo
či	či	k8xC	či
Gabonu	Gabon	k1gInSc2	Gabon
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
víceméně	víceméně	k9	víceméně
výjimečné	výjimečný	k2eAgNnSc1d1	výjimečné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
středně	středně	k6eAd1	středně
velké	velký	k2eAgMnPc4d1	velký
tučňáky	tučňák	k1gMnPc4	tučňák
<g/>
.	.	kIx.	.
</s>
<s>
Dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
60	[number]	k4	60
<g/>
–	–	k?	–
<g/>
70	[number]	k4	70
cm	cm	kA	cm
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc6	hmotnost
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
4,5	[number]	k4	4,5
kg	kg	kA	kg
<g/>
,	,	kIx,	,
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
pohlavní	pohlavní	k2eAgInSc4d1	pohlavní
dimorfismus	dimorfismus	k1gInSc4	dimorfismus
(	(	kIx(	(
<g/>
samice	samice	k1gFnSc1	samice
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
menší	malý	k2eAgFnSc1d2	menší
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výkyvy	výkyv	k1gInPc4	výkyv
v	v	k7c4	v
rozmnožování	rozmnožování	k1gNnSc4	rozmnožování
a	a	k8xC	a
náročnou	náročný	k2eAgFnSc4d1	náročná
výměnu	výměna	k1gFnSc4	výměna
peří	peří	k1gNnSc2	peří
(	(	kIx(	(
<g/>
hladovění	hladovění	k1gNnSc2	hladovění
–	–	k?	–
ztráta	ztráta	k1gFnSc1	ztráta
váhy	váha	k1gFnSc2	váha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dospělý	dospělý	k2eAgMnSc1d1	dospělý
jedinec	jedinec	k1gMnSc1	jedinec
má	mít	k5eAaImIp3nS	mít
černý	černý	k2eAgInSc4d1	černý
obličej	obličej	k1gInSc4	obličej
<g/>
,	,	kIx,	,
čelo	čelo	k1gNnSc4	čelo
a	a	k8xC	a
šíji	šíje	k1gFnSc4	šíje
<g/>
.	.	kIx.	.
</s>
<s>
Obličej	obličej	k1gInSc1	obličej
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
obkroužený	obkroužený	k2eAgInSc1d1	obkroužený
širokým	široký	k2eAgInSc7d1	široký
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
vymezeným	vymezený	k2eAgInSc7d1	vymezený
<g/>
,	,	kIx,	,
bílým	bílý	k2eAgInSc7d1	bílý
pruhem	pruh	k1gInSc7	pruh
sahajícím	sahající	k2eAgInSc7d1	sahající
až	až	k6eAd1	až
k	k	k7c3	k
bíle	bíle	k6eAd1	bíle
hrudi	hruď	k1gFnSc2	hruď
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
kresby	kresba	k1gFnSc2	kresba
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
je	být	k5eAaImIp3nS	být
také	také	k9	také
pojmenován	pojmenován	k2eAgInSc1d1	pojmenován
–	–	k?	–
brýlový	brýlový	k2eAgInSc1d1	brýlový
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
černýma	černý	k2eAgNnPc7d1	černé
očima	oko	k1gNnPc7	oko
má	mít	k5eAaImIp3nS	mít
neopeřenou	opeřený	k2eNgFnSc4d1	neopeřená
a	a	k8xC	a
narůžovělou	narůžovělý	k2eAgFnSc4d1	narůžovělá
kožní	kožní	k2eAgFnSc4d1	kožní
oblast	oblast	k1gFnSc4	oblast
–	–	k?	–
neboli	neboli	k8xC	neboli
žlázy	žláza	k1gFnSc2	žláza
<g/>
,	,	kIx,	,
sloužící	sloužící	k2eAgMnSc1d1	sloužící
k	k	k7c3	k
termoregulaci	termoregulace	k1gFnSc3	termoregulace
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yQnSc7	co
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
tělní	tělní	k2eAgFnSc1d1	tělní
teplota	teplota	k1gFnSc1	teplota
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
větší	veliký	k2eAgNnSc1d2	veliký
množství	množství	k1gNnSc1	množství
krve	krev	k1gFnSc2	krev
se	se	k3xPyFc4	se
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
žlázách	žláza	k1gFnPc6	žláza
nachází	nacházet	k5eAaImIp3nS	nacházet
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
tak	tak	k9	tak
ochlazena	ochladit	k5eAaPmNgFnS	ochladit
okolním	okolní	k2eAgInSc7d1	okolní
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takové	takový	k3xDgFnSc6	takový
situaci	situace	k1gFnSc6	situace
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
růžově	růžově	k6eAd1	růžově
zabarvení	zabarvení	k1gNnSc1	zabarvení
žláz	žláza	k1gFnPc2	žláza
patrnější	patrný	k2eAgFnSc1d2	patrnější
–	–	k?	–
tmavší	tmavý	k2eAgFnSc1d2	tmavší
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hrudi	hruď	k1gFnSc6	hruď
a	a	k8xC	a
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
břicha	břicho	k1gNnSc2	břicho
má	mít	k5eAaImIp3nS	mít
černý	černý	k2eAgInSc1d1	černý
široký	široký	k2eAgInSc1d1	široký
pruh	pruh	k1gInSc1	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Křídla	křídlo	k1gNnPc1	křídlo
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
vnější	vnější	k2eAgFnSc2d1	vnější
strany	strana	k1gFnSc2	strana
černé	černý	k2eAgFnSc2d1	černá
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
v	v	k7c6	v
kontrastu	kontrast	k1gInSc6	kontrast
s	s	k7c7	s
bílou	bílý	k2eAgFnSc7d1	bílá
dolní	dolní	k2eAgFnSc7d1	dolní
částí	část	k1gFnSc7	část
s	s	k7c7	s
proměnlivými	proměnlivý	k2eAgInPc7d1	proměnlivý
černými	černý	k2eAgInPc7d1	černý
fleky	flek	k1gInPc7	flek
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
něco	něco	k3yInSc4	něco
výraznější	výrazný	k2eAgMnSc1d2	výraznější
má	mít	k5eAaImIp3nS	mít
takové	takový	k3xDgInPc4	takový
fleky	flek	k1gInPc4	flek
na	na	k7c6	na
břiše	břicho	k1gNnSc6	břicho
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
skvrnky	skvrnka	k1gFnPc4	skvrnka
má	mít	k5eAaImIp3nS	mít
každý	každý	k3xTgMnSc1	každý
tučňák	tučňák	k1gMnSc1	tučňák
jedinečné	jedinečný	k2eAgInPc4d1	jedinečný
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
lidské	lidský	k2eAgInPc4d1	lidský
otisky	otisk	k1gInPc4	otisk
prstů	prst	k1gInPc2	prst
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
vcelku	vcelku	k6eAd1	vcelku
hluboce	hluboko	k6eAd1	hluboko
posazený	posazený	k2eAgInSc1d1	posazený
silný	silný	k2eAgInSc1d1	silný
zobák	zobák	k1gInSc1	zobák
(	(	kIx(	(
<g/>
samec	samec	k1gMnSc1	samec
o	o	k7c4	o
něco	něco	k3yInSc4	něco
hlouběji	hluboko	k6eAd2	hluboko
než	než	k8xS	než
samice	samice	k1gFnSc1	samice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
černé	černý	k2eAgFnPc1d1	černá
barvy	barva	k1gFnPc1	barva
<g/>
,	,	kIx,	,
se	s	k7c7	s
svislým	svislý	k2eAgInSc7d1	svislý
světlým	světlý	k2eAgInSc7d1	světlý
pruhem	pruh	k1gInSc7	pruh
poblíž	poblíž	k7c2	poblíž
špičky	špička	k1gFnSc2	špička
<g/>
.	.	kIx.	.
</s>
<s>
Pohlaví	pohlaví	k1gNnPc1	pohlaví
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
podobná	podobný	k2eAgNnPc4d1	podobné
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
menší	malý	k2eAgInSc4d2	menší
zobák	zobák	k1gInSc4	zobák
a	a	k8xC	a
celkovou	celkový	k2eAgFnSc4d1	celková
hmotnost	hmotnost	k1gFnSc4	hmotnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
zde	zde	k6eAd1	zde
žádný	žádný	k3yNgInSc1	žádný
významný	významný	k2eAgInSc1d1	významný
rozdíl	rozdíl	k1gInSc1	rozdíl
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
křídla	křídlo	k1gNnSc2	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
přepeřováním	přepeřování	k1gNnSc7	přepeřování
jsou	být	k5eAaImIp3nP	být
tmavé	tmavý	k2eAgFnPc1d1	tmavá
oblasti	oblast	k1gFnPc1	oblast
matnější	matný	k2eAgFnPc1d2	matnější
a	a	k8xC	a
hnědší	hnědý	k2eAgFnPc1d2	hnědší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nedospělý	dospělý	k2eNgMnSc1d1	nedospělý
jedinec	jedinec	k1gMnSc1	jedinec
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
odlišený	odlišený	k2eAgInSc1d1	odlišený
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rozdílně	rozdílně	k6eAd1	rozdílně
definovaný	definovaný	k2eAgInSc1d1	definovaný
vzor	vzor	k1gInSc1	vzor
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
vývoje	vývoj	k1gInSc2	vývoj
tmavě	tmavě	k6eAd1	tmavě
břidlicově	břidlicově	k6eAd1	břidlicově
modrý	modrý	k2eAgInSc1d1	modrý
nahoře	nahoře	k6eAd1	nahoře
(	(	kIx(	(
<g/>
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
až	až	k9	až
k	k	k7c3	k
hrudi	hruď	k1gFnSc3	hruď
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
nemá	mít	k5eNaImIp3nS	mít
výrazný	výrazný	k2eAgInSc1d1	výrazný
vzor	vzor	k1gInSc1	vzor
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
a	a	k8xC	a
pruhy	pruh	k1gInPc7	pruh
na	na	k7c6	na
těle	tělo	k1gNnSc6	tělo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obvykle	obvykle	k6eAd1	obvykle
má	mít	k5eAaImIp3nS	mít
nejasné	jasný	k2eNgFnPc4d1	nejasná
bledé	bledý	k2eAgFnPc4d1	bledá
strany	strana	k1gFnPc4	strana
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
pruh	prouha	k1gFnPc2	prouha
na	na	k7c6	na
hrdle	hrdla	k1gFnSc6	hrdla
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
oblast	oblast	k1gFnSc1	oblast
okolo	okolo	k7c2	okolo
oka	oko	k1gNnSc2	oko
je	být	k5eAaImIp3nS	být
tmavší	tmavý	k2eAgFnSc1d2	tmavší
a	a	k8xC	a
horní	horní	k2eAgFnSc1d1	horní
část	část	k1gFnSc1	část
hrudi	hruď	k1gFnSc2	hruď
a	a	k8xC	a
boky	boka	k1gFnSc2	boka
se	se	k3xPyFc4	se
spojují	spojovat	k5eAaImIp3nP	spojovat
se	s	k7c7	s
špinavě	špinavě	k6eAd1	špinavě
bílým	bílý	k2eAgNnSc7d1	bílé
břichem	břicho	k1gNnSc7	břicho
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
hnědne	hnědnout	k5eAaImIp3nS	hnědnout
s	s	k7c7	s
přibývajícím	přibývající	k2eAgInSc7d1	přibývající
věkem	věk	k1gInSc7	věk
<g/>
.	.	kIx.	.
</s>
<s>
Dospělé	dospělý	k2eAgInPc1d1	dospělý
vzory	vzor	k1gInPc1	vzor
získává	získávat	k5eAaImIp3nS	získávat
v	v	k7c6	v
několika	několik	k4yIc6	několik
fázích	fáze	k1gFnPc6	fáze
<g/>
,	,	kIx,	,
a	a	k8xC	a
zpravidla	zpravidla	k6eAd1	zpravidla
okolo	okolo	k7c2	okolo
druhého	druhý	k4xOgInSc2	druhý
roku	rok	k1gInSc2	rok
již	již	k6eAd1	již
vypadá	vypadat	k5eAaImIp3nS	vypadat
jako	jako	k9	jako
dospělý	dospělý	k2eAgMnSc1d1	dospělý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
hlas	hlas	k1gInSc1	hlas
se	se	k3xPyFc4	se
nápadně	nápadně	k6eAd1	nápadně
podobná	podobný	k2eAgNnPc4d1	podobné
hýkání	hýkání	k1gNnPc4	hýkání
osla	osel	k1gMnSc4	osel
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
slyšitelný	slyšitelný	k2eAgInSc1d1	slyšitelný
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
pronikavý	pronikavý	k2eAgInSc4d1	pronikavý
hlasitý	hlasitý	k2eAgInSc4d1	hlasitý
zvuk	zvuk	k1gInSc4	zvuk
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
přezdívá	přezdívat	k5eAaImIp3nS	přezdívat
"	"	kIx"	"
<g/>
jackass	jackassit	k5eAaPmRp2nS	jackassit
penguin	penguin	k1gInSc1	penguin
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
"	"	kIx"	"
<g/>
hloupý	hloupý	k2eAgMnSc1d1	hloupý
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
oslí	oslí	k2eAgMnSc1d1	oslí
<g/>
"	"	kIx"	"
tučňák	tučňák	k1gMnSc1	tučňák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
charismatický	charismatický	k2eAgMnSc1d1	charismatický
a	a	k8xC	a
turisticky	turisticky	k6eAd1	turisticky
oblíbený	oblíbený	k2eAgInSc4d1	oblíbený
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Lidem	člověk	k1gMnPc3	člověk
se	se	k3xPyFc4	se
dovolí	dovolit	k5eAaPmIp3nS	dovolit
přiblížit	přiblížit	k5eAaPmF	přiblížit
až	až	k9	až
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
tří	tři	k4xCgInPc2	tři
metrů	metr	k1gInPc2	metr
(	(	kIx(	(
<g/>
v	v	k7c4	v
Boulders	Boulders	k1gInSc4	Boulders
Beach	Beach	k1gInSc4	Beach
i	i	k9	i
na	na	k7c4	na
méně	málo	k6eAd2	málo
než	než	k8xS	než
jeden	jeden	k4xCgInSc4	jeden
metr	metr	k1gInSc4	metr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
brýlový	brýlový	k2eAgMnSc1d1	brýlový
se	se	k3xPyFc4	se
dožívá	dožívat	k5eAaImIp3nS	dožívat
10	[number]	k4	10
až	až	k9	až
30	[number]	k4	30
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vyššího	vysoký	k2eAgInSc2d2	vyšší
věku	věk	k1gInSc2	věk
se	se	k3xPyFc4	se
dožije	dožít	k5eAaPmIp3nS	dožít
převážné	převážný	k2eAgNnSc1d1	převážné
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
zřídkakdy	zřídkakdy	k6eAd1	zřídkakdy
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
i	i	k9	i
pouhých	pouhý	k2eAgInPc2d1	pouhý
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
a	a	k8xC	a
lov	lov	k1gInSc1	lov
==	==	k?	==
</s>
</p>
<p>
<s>
Jako	jako	k8xC	jako
všechny	všechen	k3xTgInPc4	všechen
druhy	druh	k1gInPc4	druh
tučňáků	tučňák	k1gMnPc2	tučňák
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
tučňák	tučňák	k1gMnSc1	tučňák
brýlový	brýlový	k2eAgMnSc1d1	brýlový
tělo	tělo	k1gNnSc1	tělo
dokonale	dokonale	k6eAd1	dokonale
přizpůsobeno	přizpůsobit	k5eAaPmNgNnS	přizpůsobit
k	k	k7c3	k
plavání	plavání	k1gNnSc3	plavání
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
potravu	potrava	k1gFnSc4	potrava
získává	získávat	k5eAaImIp3nS	získávat
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
jídelníček	jídelníček	k1gInSc1	jídelníček
tvoří	tvořit	k5eAaImIp3nS	tvořit
hlavně	hlavně	k9	hlavně
menší	malý	k2eAgInPc4d2	menší
druhy	druh	k1gInPc4	druh
ryb	ryba	k1gFnPc2	ryba
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
50	[number]	k4	50
<g/>
–	–	k?	–
<g/>
300	[number]	k4	300
mm	mm	kA	mm
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
je	být	k5eAaImIp3nS	být
sardinka	sardinka	k1gFnSc1	sardinka
africká	africký	k2eAgFnSc1d1	africká
(	(	kIx(	(
<g/>
Sardinops	Sardinops	k1gInSc1	Sardinops
ocellata	ocelle	k1gNnPc1	ocelle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sardinka	sardinka	k1gFnSc1	sardinka
tečkovaná	tečkovaný	k2eAgFnSc1d1	tečkovaná
(	(	kIx(	(
<g/>
Sardinops	Sardinops	k1gInSc1	Sardinops
sagax	sagax	k1gInSc1	sagax
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kranas	kranas	k1gInSc1	kranas
obecný	obecný	k2eAgInSc1d1	obecný
(	(	kIx(	(
<g/>
Trachurus	Trachurus	k1gInSc1	Trachurus
trachurus	trachurus	k1gInSc1	trachurus
<g/>
)	)	kIx)	)
či	či	k8xC	či
etrumeus	etrumeus	k1gMnSc1	etrumeus
velkooký	velkooký	k2eAgMnSc1d1	velkooký
(	(	kIx(	(
<g/>
Etrumeus	Etrumeus	k1gMnSc1	Etrumeus
teres	teres	k1gMnSc1	teres
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
jejich	jejich	k3xOp3gFnSc2	jejich
každodenní	každodenní	k2eAgFnSc2d1	každodenní
stravy	strava	k1gFnSc2	strava
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
široké	široký	k2eAgFnSc2d1	široká
škály	škála	k1gFnSc2	škála
měkkýšů	měkkýš	k1gMnPc2	měkkýš
<g/>
,	,	kIx,	,
hlavonožců	hlavonožec	k1gMnPc2	hlavonožec
a	a	k8xC	a
korýšů	korýš	k1gMnPc2	korýš
<g/>
.	.	kIx.	.
<g/>
Lov	lov	k1gInSc1	lov
je	být	k5eAaImIp3nS	být
uskutečňován	uskutečňován	k2eAgInSc1d1	uskutečňován
v	v	k7c6	v
hloubkách	hloubka	k1gFnPc6	hloubka
nepřesahujících	přesahující	k2eNgFnPc2d1	nepřesahující
20	[number]	k4	20
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
schopni	schopen	k2eAgMnPc1d1	schopen
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
nad	nad	k7c4	nad
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
jsou	být	k5eAaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
plavat	plavat	k5eAaImF	plavat
rychlostí	rychlost	k1gFnSc7	rychlost
až	až	k9	až
16	[number]	k4	16
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
se	se	k3xPyFc4	se
vzdalují	vzdalovat	k5eAaImIp3nP	vzdalovat
maximálně	maximálně	k6eAd1	maximálně
na	na	k7c4	na
25	[number]	k4	25
<g/>
–	–	k?	–
<g/>
70	[number]	k4	70
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nedostatku	nedostatek	k1gInSc2	nedostatek
potravního	potravní	k2eAgInSc2d1	potravní
zdroje	zdroj	k1gInSc2	zdroj
a	a	k8xC	a
jinak	jinak	k6eAd1	jinak
nevyhovujících	vyhovující	k2eNgFnPc2d1	nevyhovující
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přirození	přirozený	k2eAgMnPc1d1	přirozený
predátoři	predátor	k1gMnPc1	predátor
==	==	k?	==
</s>
</p>
<p>
<s>
Především	především	k9	především
volně	volně	k6eAd1	volně
žijící	žijící	k2eAgMnPc1d1	žijící
psi	pes	k1gMnPc1	pes
a	a	k8xC	a
kočky	kočka	k1gFnPc1	kočka
ohrožují	ohrožovat	k5eAaImIp3nP	ohrožovat
mláďata	mládě	k1gNnPc1	mládě
v	v	k7c6	v
norách	nora	k1gFnPc6	nora
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
invazní	invazní	k2eAgInPc1d1	invazní
druhy	druh	k1gInPc1	druh
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
také	také	k9	také
racci	racek	k1gMnPc1	racek
<g/>
,	,	kIx,	,
krysy	krysa	k1gFnPc1	krysa
<g/>
,	,	kIx,	,
promyky	promyk	k1gInPc1	promyk
<g/>
,	,	kIx,	,
ženetky	ženetka	k1gFnPc1	ženetka
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
hadi	had	k1gMnPc1	had
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výjimečném	výjimečný	k2eAgInSc6d1	výjimečný
případě	případ	k1gInSc6	případ
napadl	napadnout	k5eAaPmAgMnS	napadnout
tučňáka	tučňák	k1gMnSc4	tučňák
brýlového	brýlový	k2eAgMnSc4d1	brýlový
i	i	k8xC	i
levhart	levhart	k1gMnSc1	levhart
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
moři	moře	k1gNnSc6	moře
dospělé	dospělí	k1gMnPc4	dospělí
tučňáky	tučňák	k1gMnPc7	tučňák
ohrožují	ohrožovat	k5eAaImIp3nP	ohrožovat
žraloci	žralok	k1gMnPc1	žralok
<g/>
,	,	kIx,	,
kosatky	kosatka	k1gFnPc1	kosatka
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
lachtani	lachtan	k1gMnPc1	lachtan
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
denně	denně	k6eAd1	denně
uloví	ulovit	k5eAaPmIp3nP	ulovit
i	i	k9	i
na	na	k7c4	na
sto	sto	k4xCgNnSc4	sto
kusů	kus	k1gInPc2	kus
tučňáků	tučňák	k1gMnPc2	tučňák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tučňák	tučňák	k1gMnSc1	tučňák
koloniální	koloniální	k2eAgInPc4d1	koloniální
a	a	k8xC	a
monogamní	monogamní	k2eAgInPc4d1	monogamní
<g/>
,	,	kIx,	,
formuje	formovat	k5eAaImIp3nS	formovat
trvalé	trvalý	k2eAgNnSc4d1	trvalé
partnerství	partnerství	k1gNnSc4	partnerství
prakticky	prakticky	k6eAd1	prakticky
na	na	k7c4	na
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
případě	případ	k1gInSc6	případ
úmrtí	úmrtí	k1gNnSc2	úmrtí
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
páru	pár	k1gInSc2	pár
jej	on	k3xPp3gInSc2	on
druhý	druhý	k4xOgMnSc1	druhý
přeživší	přeživší	k2eAgMnSc1d1	přeživší
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozmnožovací	rozmnožovací	k2eAgFnPc1d1	rozmnožovací
aktivity	aktivita	k1gFnPc1	aktivita
nejsou	být	k5eNaImIp3nP	být
fixně	fixně	k6eAd1	fixně
známy	znám	k2eAgFnPc1d1	známa
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nejsou	být	k5eNaImIp3nP	být
limitováni	limitovat	k5eAaBmNgMnP	limitovat
proměnlivou	proměnlivý	k2eAgFnSc7d1	proměnlivá
hojností	hojnost	k1gFnSc7	hojnost
potravy	potrava	k1gFnSc2	potrava
nebo	nebo	k8xC	nebo
patrnější	patrný	k2eAgFnSc7d2	patrnější
změnou	změna	k1gFnSc7	změna
biotopu	biotop	k1gInSc2	biotop
(	(	kIx(	(
<g/>
přírodních	přírodní	k2eAgFnPc2d1	přírodní
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
podmínky	podmínka	k1gFnPc4	podmínka
přívětivě	přívětivě	k6eAd1	přívětivě
<g/>
,	,	kIx,	,
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
i	i	k9	i
dvakrát	dvakrát	k6eAd1	dvakrát
do	do	k7c2	do
roka	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
však	však	k9	však
začínají	začínat	k5eAaImIp3nP	začínat
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
až	až	k8xS	až
srpnu	srpen	k1gInSc6	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Hnízda	hnízdo	k1gNnSc2	hnízdo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
obyčejně	obyčejně	k6eAd1	obyčejně
pod	pod	k7c7	pod
skalami	skála	k1gFnPc7	skála
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vegetaci	vegetace	k1gFnSc6	vegetace
či	či	k8xC	či
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
děrách	děra	k1gFnPc6	děra
v	v	k7c6	v
písku	písek	k1gInSc6	písek
<g/>
,	,	kIx,	,
a	a	k8xC	a
většinou	většinou	k6eAd1	většinou
jsou	být	k5eAaImIp3nP	být
kryta	kryt	k2eAgFnSc1d1	kryta
před	před	k7c7	před
sluncem	slunce	k1gNnSc7	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdní	hnízdní	k2eAgFnPc1d1	hnízdní
nory	nora	k1gFnPc1	nora
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
budovány	budovat	k5eAaImNgFnP	budovat
zpravidla	zpravidla	k6eAd1	zpravidla
koloniálně	koloniálně	k6eAd1	koloniálně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgInPc4	první
dny	den	k1gInPc4	den
v	v	k7c6	v
kolonii	kolonie	k1gFnSc6	kolonie
===	===	k?	===
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
první	první	k4xOgFnSc7	první
se	se	k3xPyFc4	se
ocitají	ocitat	k5eAaImIp3nP	ocitat
na	na	k7c6	na
hnízdišti	hnízdiště	k1gNnSc6	hnízdiště
samci	samec	k1gMnPc1	samec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
si	se	k3xPyFc3	se
vybojují	vybojovat	k5eAaPmIp3nP	vybojovat
nejvhodnější	vhodný	k2eAgNnSc4d3	nejvhodnější
místo	místo	k1gNnSc4	místo
pro	pro	k7c4	pro
hnízdění	hnízdění	k1gNnSc4	hnízdění
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jen	jen	k9	jen
obhájí	obhájit	k5eAaPmIp3nS	obhájit
a	a	k8xC	a
upraví	upravit	k5eAaPmIp3nS	upravit
hnízdo	hnízdo	k1gNnSc1	hnízdo
loňské	loňský	k2eAgNnSc1d1	loňské
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
ostatní	ostatní	k2eAgMnPc1d1	ostatní
tučňáci	tučňák	k1gMnPc1	tučňák
tohoto	tento	k3xDgInSc2	tento
rodu	rod	k1gInSc2	rod
si	se	k3xPyFc3	se
zakládají	zakládat	k5eAaImIp3nP	zakládat
na	na	k7c6	na
dostatečném	dostatečný	k2eAgInSc6d1	dostatečný
stínu	stín	k1gInSc6	stín
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
na	na	k7c4	na
hnízdiště	hnízdiště	k1gNnSc4	hnízdiště
doléhá	doléhat	k5eAaImIp3nS	doléhat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
vyhnuli	vyhnout	k5eAaPmAgMnP	vyhnout
dlouhodobému	dlouhodobý	k2eAgInSc3d1	dlouhodobý
žáru	žár	k1gInSc3	žár
slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
si	se	k3xPyFc3	se
také	také	k9	také
vybudovávají	vybudovávat	k5eAaImIp3nP	vybudovávat
hnízdní	hnízdní	k2eAgFnPc4d1	hnízdní
nory	nora	k1gFnPc4	nora
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
jen	jen	k9	jen
díry	díra	k1gFnSc2	díra
v	v	k7c6	v
písku	písek	k1gInSc6	písek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
až	až	k9	až
metrových	metrový	k2eAgFnPc2d1	metrová
hloubek	hloubka	k1gFnPc2	hloubka
<g/>
.	.	kIx.	.
</s>
<s>
Postupným	postupný	k2eAgNnSc7d1	postupné
opravováním	opravování	k1gNnSc7	opravování
její	její	k3xOp3gFnSc2	její
rozměry	rozměra	k1gFnSc2	rozměra
ještě	ještě	k6eAd1	ještě
rostou	růst	k5eAaImIp3nP	růst
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
takových	takový	k3xDgFnPc2	takový
nor	nora	k1gFnPc2	nora
po	po	k7c6	po
jednom	jeden	k4xCgNnSc6	jeden
hnízdění	hnízdění	k1gNnSc6	hnízdění
rozpadne	rozpadnout	k5eAaPmIp3nS	rozpadnout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
z	z	k7c2	z
nedostatku	nedostatek	k1gInSc2	nedostatek
prorůstající	prorůstající	k2eAgFnSc2d1	prorůstající
vegetace	vegetace	k1gFnSc2	vegetace
nemají	mít	k5eNaImIp3nP	mít
vyšší	vysoký	k2eAgFnSc4d2	vyšší
stabilitu	stabilita	k1gFnSc4	stabilita
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
pevnost	pevnost	k1gFnSc1	pevnost
může	moct	k5eAaImIp3nS	moct
navýšit	navýšit	k5eAaPmF	navýšit
lepkavá	lepkavý	k2eAgFnSc1d1	lepkavá
a	a	k8xC	a
tvrdá	tvrdý	k2eAgFnSc1d1	tvrdá
vrstva	vrstva	k1gFnSc1	vrstva
guana	guaen	k2eAgFnSc1d1	guaen
(	(	kIx(	(
<g/>
vrstva	vrstva	k1gFnSc1	vrstva
trusu	trus	k1gInSc2	trus
tučňáků	tučňák	k1gMnPc2	tučňák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Těžba	těžba	k1gFnSc1	těžba
tohoto	tento	k3xDgInSc2	tento
trusu	trus	k1gInSc2	trus
byla	být	k5eAaImAgFnS	být
pak	pak	k6eAd1	pak
pro	pro	k7c4	pro
tučňáky	tučňák	k1gMnPc4	tučňák
citelná	citelný	k2eAgFnSc1d1	citelná
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
přísně	přísně	k6eAd1	přísně
zakázána	zakázat	k5eAaPmNgFnS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Nory	Nora	k1gFnPc1	Nora
vystýlají	vystýlat	k5eAaImIp3nP	vystýlat
malými	malý	k2eAgFnPc7d1	malá
větvičkami	větvička	k1gFnPc7	větvička
<g/>
,	,	kIx,	,
klacíky	klacík	k1gInPc7	klacík
<g/>
,	,	kIx,	,
kamínky	kamínek	k1gInPc7	kamínek
<g/>
,	,	kIx,	,
šupinami	šupina	k1gFnPc7	šupina
z	z	k7c2	z
ryb	ryba	k1gFnPc2	ryba
nebo	nebo	k8xC	nebo
i	i	k8xC	i
kostmi	kost	k1gFnPc7	kost
z	z	k7c2	z
uhynulých	uhynulý	k2eAgMnPc2d1	uhynulý
tučňáků	tučňák	k1gMnPc2	tučňák
<g/>
,	,	kIx,	,
prakticky	prakticky	k6eAd1	prakticky
tedy	tedy	k9	tedy
veškerým	veškerý	k3xTgInSc7	veškerý
dostupným	dostupný	k2eAgInSc7d1	dostupný
materiálem	materiál	k1gInSc7	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
za	za	k7c4	za
týden	týden	k1gInSc4	týden
se	se	k3xPyFc4	se
na	na	k7c6	na
hnízdišti	hnízdiště	k1gNnSc6	hnízdiště
objeví	objevit	k5eAaPmIp3nP	objevit
i	i	k9	i
samice	samice	k1gFnPc1	samice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
vyhnou	vyhnout	k5eAaPmIp3nP	vyhnout
bojové	bojový	k2eAgFnSc3d1	bojová
náladě	nálada	k1gFnSc3	nálada
samců	samec	k1gMnPc2	samec
hájících	hájící	k2eAgMnPc2d1	hájící
svá	svůj	k3xOyFgNnPc4	svůj
hnízda	hnízdo	k1gNnPc4	hnízdo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Námluvy	námluva	k1gFnSc2	námluva
===	===	k?	===
</s>
</p>
<p>
<s>
Námluvy	námluva	k1gFnPc1	námluva
probíhají	probíhat	k5eAaImIp3nP	probíhat
za	za	k7c2	za
oddaných	oddaný	k2eAgInPc2d1	oddaný
doteků	dotek	k1gInPc2	dotek
a	a	k8xC	a
vzájemných	vzájemný	k2eAgInPc6d1	vzájemný
pozdravech	pozdrav	k1gInPc6	pozdrav
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yRgInPc6	který
se	se	k3xPyFc4	se
třou	třít	k5eAaImIp3nP	třít
hlavou	hlava	k1gFnSc7	hlava
a	a	k8xC	a
krkem	krk	k1gInSc7	krk
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
čistí	čistit	k5eAaImIp3nS	čistit
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
a	a	k8xC	a
krku	krk	k1gInSc6	krk
<g/>
,	,	kIx,	,
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
jiní	jiný	k2eAgMnPc1d1	jiný
tučňáci	tučňák	k1gMnPc1	tučňák
<g/>
,	,	kIx,	,
ozývají	ozývat	k5eAaImIp3nP	ozývat
se	s	k7c7	s
hlubokými	hluboký	k2eAgInPc7d1	hluboký
hrdelními	hrdelní	k2eAgInPc7d1	hrdelní
tóny	tón	k1gInPc7	tón
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
mají	mít	k5eAaImIp3nP	mít
stejný	stejný	k2eAgInSc4d1	stejný
sociální	sociální	k2eAgInSc4d1	sociální
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
tzv.	tzv.	kA	tzv.
uvědomovací	uvědomovací	k2eAgInSc1d1	uvědomovací
hlas	hlas	k1gInSc1	hlas
–	–	k?	–
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
udržovaní	udržovaný	k2eAgMnPc1d1	udržovaný
kontaktu	kontakt	k1gInSc2	kontakt
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgMnPc7d1	jednotlivý
ptáky	pták	k1gMnPc7	pták
na	na	k7c6	na
vodě	voda	k1gFnSc6	voda
i	i	k8xC	i
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
<g/>
.	.	kIx.	.
</s>
<s>
Nejtypičtější	typický	k2eAgFnSc7d3	nejtypičtější
fází	fáze	k1gFnSc7	fáze
toku	tok	k1gInSc2	tok
je	být	k5eAaImIp3nS	být
hlasité	hlasitý	k2eAgNnSc1d1	hlasité
troubení	troubení	k1gNnSc1	troubení
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
stojí	stát	k5eAaImIp3nS	stát
samec	samec	k1gMnSc1	samec
před	před	k7c7	před
samicí	samice	k1gFnSc7	samice
s	s	k7c7	s
otevřeným	otevřený	k2eAgInSc7d1	otevřený
zobákem	zobák	k1gInSc7	zobák
a	a	k8xC	a
nahoru	nahoru	k6eAd1	nahoru
nataženým	natažený	k2eAgInSc7d1	natažený
krkem	krk	k1gInSc7	krk
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
uzavřených	uzavřený	k2eAgInPc2d1	uzavřený
párů	pár	k1gInPc2	pár
troubí	troubit	k5eAaImIp3nP	troubit
oba	dva	k4xCgMnPc1	dva
manželé	manžel	k1gMnPc1	manžel
současně	současně	k6eAd1	současně
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
troubení	troubení	k1gNnSc6	troubení
se	se	k3xPyFc4	se
samec	samec	k1gMnSc1	samec
projevuje	projevovat	k5eAaImIp3nS	projevovat
chvějivými	chvějivý	k2eAgInPc7d1	chvějivý
dotyky	dotyk	k1gInPc7	dotyk
křídel	křídlo	k1gNnPc2	křídlo
<g/>
,	,	kIx,	,
a	a	k8xC	a
následně	následně	k6eAd1	následně
začne	začít	k5eAaPmIp3nS	začít
samici	samice	k1gFnSc4	samice
opětovně	opětovně	k6eAd1	opětovně
probírat	probírat	k5eAaImF	probírat
peří	peří	k1gNnSc4	peří
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
a	a	k8xC	a
krku	krk	k1gInSc6	krk
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
úvodu	úvod	k1gInSc6	úvod
se	se	k3xPyFc4	se
samice	samice	k1gFnSc1	samice
položí	položit	k5eAaPmIp3nS	položit
na	na	k7c4	na
břicho	břicho	k1gNnSc4	břicho
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
samec	samec	k1gMnSc1	samec
vyskočí	vyskočit	k5eAaPmIp3nS	vyskočit
na	na	k7c4	na
její	její	k3xOp3gNnPc4	její
záda	záda	k1gNnPc4	záda
a	a	k8xC	a
proběhne	proběhnout	k5eAaPmIp3nS	proběhnout
kopulace	kopulace	k1gFnSc1	kopulace
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
námluvy	námluva	k1gFnPc1	námluva
neodehrávají	odehrávat	k5eNaImIp3nP	odehrávat
přímo	přímo	k6eAd1	přímo
u	u	k7c2	u
hnízda	hnízdo	k1gNnSc2	hnízdo
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
tučňák	tučňák	k1gMnSc1	tučňák
brýlový	brýlový	k2eAgMnSc1d1	brýlový
ohromit	ohromit	k5eAaPmF	ohromit
samici	samice	k1gFnSc4	samice
zdánlivě	zdánlivě	k6eAd1	zdánlivě
romantickým	romantický	k2eAgInSc7d1	romantický
tanečkem	taneček	k1gInSc7	taneček
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlavu	hlava	k1gFnSc4	hlava
přikrčí	přikrčit	k5eAaPmIp3nS	přikrčit
k	k	k7c3	k
tělu	tělo	k1gNnSc3	tělo
a	a	k8xC	a
obchází	obcházet	k5eAaImIp3nP	obcházet
samici	samice	k1gFnSc4	samice
kolem	kolem	k6eAd1	kolem
dokola	dokola	k6eAd1	dokola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Páření	páření	k1gNnSc2	páření
===	===	k?	===
</s>
</p>
<p>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
akt	akt	k1gInSc1	akt
trvá	trvat	k5eAaImIp3nS	trvat
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
minuty	minuta	k1gFnSc2	minuta
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
jen	jen	k9	jen
několik	několik	k4yIc1	několik
sekund	sekunda	k1gFnPc2	sekunda
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
samcova	samcův	k2eAgFnSc1d1	Samcova
poloha	poloha	k1gFnSc1	poloha
poměrně	poměrně	k6eAd1	poměrně
vrtkavá	vrtkavý	k2eAgFnSc1d1	vrtkavá
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
se	se	k3xPyFc4	se
při	při	k7c6	při
kopulaci	kopulace	k1gFnSc6	kopulace
přikrčí	přikrčit	k5eAaPmIp3nS	přikrčit
nebo	nebo	k8xC	nebo
si	se	k3xPyFc3	se
lehne	lehnout	k5eAaPmIp3nS	lehnout
na	na	k7c4	na
břicho	břicho	k1gNnSc4	břicho
a	a	k8xC	a
samec	samec	k1gInSc1	samec
ji	on	k3xPp3gFnSc4	on
vyskočí	vyskočit	k5eAaPmIp3nS	vyskočit
na	na	k7c4	na
záda	záda	k1gNnPc4	záda
<g/>
.	.	kIx.	.
</s>
<s>
Vyzdvihne	vyzdvihnout	k5eAaPmIp3nS	vyzdvihnout
ocas	ocas	k1gInSc4	ocas
pro	pro	k7c4	pro
lepší	dobrý	k2eAgFnSc4d2	lepší
koordinaci	koordinace	k1gFnSc4	koordinace
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
dostat	dostat	k5eAaPmF	dostat
svou	svůj	k3xOyFgFnSc4	svůj
kloaku	kloaka	k1gFnSc4	kloaka
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
podaří	podařit	k5eAaPmIp3nS	podařit
<g/>
,	,	kIx,	,
předá	předat	k5eAaPmIp3nS	předat
samici	samice	k1gFnSc4	samice
sperma	sperma	k1gNnSc1	sperma
pouhým	pouhý	k2eAgNnSc7d1	pouhé
přitisknutím	přitisknutí	k1gNnSc7	přitisknutí
-	-	kIx~	-
otřením	otření	k1gNnSc7	otření
-	-	kIx~	-
kloaky	kloaka	k1gFnSc2	kloaka
<g/>
.	.	kIx.	.
</s>
<s>
Páření	páření	k1gNnSc1	páření
se	se	k3xPyFc4	se
mnohokrát	mnohokrát	k6eAd1	mnohokrát
opakuje	opakovat	k5eAaImIp3nS	opakovat
–	–	k?	–
u	u	k7c2	u
mladších	mladý	k2eAgInPc2d2	mladší
méně	málo	k6eAd2	málo
zkušených	zkušený	k2eAgInPc2d1	zkušený
párů	pár	k1gInPc2	pár
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
páření	páření	k1gNnSc6	páření
se	se	k3xPyFc4	se
oba	dva	k4xCgMnPc1	dva
ptáci	pták	k1gMnPc1	pták
znovu	znovu	k6eAd1	znovu
intenzivně	intenzivně	k6eAd1	intenzivně
čistí	čistit	k5eAaImIp3nS	čistit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Snesení	snesení	k1gNnSc2	snesení
vajec	vejce	k1gNnPc2	vejce
a	a	k8xC	a
chov	chov	k1gInSc1	chov
mláďat	mládě	k1gNnPc2	mládě
===	===	k?	===
</s>
</p>
<p>
<s>
Nejčastěji	často	k6eAd3	často
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
března	březen	k1gInSc2	březen
do	do	k7c2	do
května	květen	k1gInSc2	květen
(	(	kIx(	(
<g/>
Jižní	jižní	k2eAgFnSc1d1	jižní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
)	)	kIx)	)
či	či	k8xC	či
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
až	až	k8xS	až
prosinci	prosinec	k1gInSc6	prosinec
(	(	kIx(	(
<g/>
Namibie	Namibie	k1gFnSc1	Namibie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
položí	položit	k5eAaPmIp3nS	položit
samice	samice	k1gFnSc1	samice
zpravidla	zpravidla	k6eAd1	zpravidla
dvě	dva	k4xCgFnPc1	dva
bílé	bílý	k2eAgFnPc1d1	bílá
vejce	vejce	k1gNnPc4	vejce
do	do	k7c2	do
podzemních	podzemní	k2eAgFnPc2d1	podzemní
nory	nora	k1gFnSc2	nora
<g/>
,	,	kIx,	,
prohlubně	prohlubeň	k1gFnPc4	prohlubeň
ve	v	k7c6	v
skále	skála	k1gFnSc6	skála
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
do	do	k7c2	do
štěrbiny	štěrbina	k1gFnSc2	štěrbina
mezi	mezi	k7c4	mezi
balvany	balvan	k1gInPc4	balvan
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
vejce	vejce	k1gNnSc1	vejce
bývá	bývat	k5eAaImIp3nS	bývat
větší	veliký	k2eAgNnSc1d2	veliký
než	než	k8xS	než
druhé	druhý	k4xOgNnSc1	druhý
<g/>
,	,	kIx,	,
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
položeny	položen	k2eAgInPc1d1	položen
během	během	k7c2	během
dvou	dva	k4xCgInPc2	dva
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezení	sezení	k1gNnSc6	sezení
na	na	k7c6	na
vejcích	vejce	k1gNnPc6	vejce
se	se	k3xPyFc4	se
samice	samice	k1gFnSc1	samice
střídá	střídat	k5eAaImIp3nS	střídat
s	s	k7c7	s
partnerem	partner	k1gMnSc7	partner
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
asi	asi	k9	asi
36	[number]	k4	36
až	až	k9	až
41	[number]	k4	41
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
a	a	k8xC	a
střídaní	střídaný	k2eAgMnPc1d1	střídaný
probíhá	probíhat	k5eAaImIp3nS	probíhat
zhruba	zhruba	k6eAd1	zhruba
po	po	k7c6	po
třech	tři	k4xCgFnPc6	tři
až	až	k6eAd1	až
čtyřech	čtyři	k4xCgInPc6	čtyři
dnech	den	k1gInPc6	den
(	(	kIx(	(
<g/>
jeden	jeden	k4xCgMnSc1	jeden
vždy	vždy	k6eAd1	vždy
hlídá	hlídat	k5eAaImIp3nS	hlídat
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
loví	lovit	k5eAaImIp3nS	lovit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
výměnou	výměna	k1gFnSc7	výměna
v	v	k7c6	v
noře	nora	k1gFnSc6	nora
se	se	k3xPyFc4	se
oba	dva	k4xCgMnPc1	dva
zdraví	zdravý	k1gMnPc1	zdravý
a	a	k8xC	a
poznávají	poznávat	k5eAaImIp3nP	poznávat
tichými	tichý	k2eAgInPc7d1	tichý
oznamovacími	oznamovací	k2eAgInPc7d1	oznamovací
tóny	tón	k1gInPc7	tón
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c4	o
vylíhlá	vylíhlý	k2eAgNnPc4d1	vylíhlé
mláďata	mládě	k1gNnPc4	mládě
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
starají	starat	k5eAaImIp3nP	starat
oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
<g/>
,	,	kIx,	,
a	a	k8xC	a
střídají	střídat	k5eAaImIp3nP	střídat
se	se	k3xPyFc4	se
po	po	k7c6	po
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
dnech	den	k1gInPc6	den
(	(	kIx(	(
<g/>
jeden	jeden	k4xCgMnSc1	jeden
vždy	vždy	k6eAd1	vždy
hlídá	hlídat	k5eAaImIp3nS	hlídat
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
loví	lovit	k5eAaImIp3nS	lovit
a	a	k8xC	a
přináší	přinášet	k5eAaImIp3nS	přinášet
potravu	potrava	k1gFnSc4	potrava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odrostlejší	odrostlý	k2eAgNnSc1d2	odrostlejší
asi	asi	k9	asi
měsíční	měsíční	k2eAgNnSc1d1	měsíční
mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
shlukují	shlukovat	k5eAaImIp3nP	shlukovat
do	do	k7c2	do
malých	malý	k2eAgFnPc2d1	malá
skupinek	skupinka	k1gFnPc2	skupinka
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
školek	školka	k1gFnPc2	školka
<g/>
,	,	kIx,	,
čítající	čítající	k2eAgFnSc1d1	čítající
jen	jen	k9	jen
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Převážně	převážně	k6eAd1	převážně
jsou	být	k5eAaImIp3nP	být
ale	ale	k8xC	ale
schována	schovat	k5eAaPmNgFnS	schovat
v	v	k7c6	v
norách	nora	k1gFnPc6	nora
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnPc1	rodič
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
loví	lovit	k5eAaImIp3nP	lovit
potravu	potrava	k1gFnSc4	potrava
společně	společně	k6eAd1	společně
<g/>
,	,	kIx,	,
a	a	k8xC	a
mláďata	mládě	k1gNnPc1	mládě
jsou	být	k5eAaImIp3nP	být
hodiny	hodina	k1gFnPc1	hodina
osamocena	osamocen	k2eAgFnSc1d1	osamocena
<g/>
.	.	kIx.	.
</s>
<s>
Kuřata	kuře	k1gNnPc1	kuře
se	se	k3xPyFc4	se
ochmýří	ochmýřit	k5eAaPmIp3nP	ochmýřit
v	v	k7c6	v
60	[number]	k4	60
<g/>
–	–	k?	–
<g/>
130	[number]	k4	130
dnech	den	k1gInPc6	den
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
faktorech	faktor	k1gInPc6	faktor
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
–	–	k?	–
jeho	jeho	k3xOp3gFnPc4	jeho
kvality	kvalita	k1gFnPc4	kvalita
a	a	k8xC	a
dostupnosti	dostupnost	k1gFnPc4	dostupnost
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
dospělými	dospělí	k1gMnPc7	dospělí
opustí	opustit	k5eAaPmIp3nS	opustit
kolonii	kolonie	k1gFnSc4	kolonie
a	a	k8xC	a
pobudou	pobýt	k5eAaPmIp3nP	pobýt
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
mimo	mimo	k7c4	mimo
hnízdiště	hnízdiště	k1gNnSc4	hnízdiště
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věku	věk	k1gInSc6	věk
asi	asi	k9	asi
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
pokusí	pokusit	k5eAaPmIp3nS	pokusit
poprvé	poprvé	k6eAd1	poprvé
zahnízdit	zahnízdit	k5eAaPmF	zahnízdit
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyberou	vybrat	k5eAaPmIp3nP	vybrat
si	se	k3xPyFc3	se
pro	pro	k7c4	pro
hnízdění	hnízdění	k1gNnSc4	hnízdění
zpravidla	zpravidla	k6eAd1	zpravidla
tu	tu	k6eAd1	tu
oblast	oblast	k1gFnSc4	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
sama	sám	k3xTgFnSc1	sám
vychována	vychován	k2eAgFnSc1d1	vychována
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavně	pohlavně	k6eAd1	pohlavně
vyzrálý	vyzrálý	k2eAgInSc1d1	vyzrálý
jsou	být	k5eAaImIp3nP	být
ale	ale	k8xC	ale
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
až	až	k6eAd1	až
čtyřech	čtyři	k4xCgNnPc6	čtyři
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
jsou	být	k5eAaImIp3nP	být
prvotní	prvotní	k2eAgInPc4d1	prvotní
pokusy	pokus	k1gInPc4	pokus
o	o	k7c4	o
snůšku	snůška	k1gFnSc4	snůška
nezdárné	zdárný	k2eNgNnSc1d1	nezdárné
<g/>
.	.	kIx.	.
<g/>
Dospělý	dospělý	k2eAgMnSc1d1	dospělý
se	se	k3xPyFc4	se
po	po	k7c6	po
několika	několik	k4yIc6	několik
týdenním	týdenní	k2eAgInSc6d1	týdenní
pobytu	pobyt	k1gInSc6	pobyt
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
–	–	k?	–
zotatovací	zotatovací	k2eAgFnSc3d1	zotatovací
kůře	kůra	k1gFnSc3	kůra
<g/>
,	,	kIx,	,
vrátí	vrátit	k5eAaPmIp3nS	vrátit
na	na	k7c4	na
břeh	břeh	k1gInSc4	břeh
přepeřit	přepeřit	k5eAaPmF	přepeřit
<g/>
.	.	kIx.	.
</s>
<s>
Pobudou	pobýt	k5eAaPmIp3nP	pobýt
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
a	a	k8xC	a
hladoví	hladovět	k5eAaImIp3nS	hladovět
asi	asi	k9	asi
20	[number]	k4	20
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Náhradní	náhradní	k2eAgFnSc1d1	náhradní
(	(	kIx(	(
<g/>
či	či	k8xC	či
druhé	druhý	k4xOgNnSc4	druhý
<g/>
)	)	kIx)	)
snůšky	snůška	k1gFnPc1	snůška
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
sneseny	snést	k5eAaPmNgInP	snést
asi	asi	k9	asi
o	o	k7c4	o
4	[number]	k4	4
měsíce	měsíc	k1gInSc2	měsíc
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
pakliže	pakliže	k8xS	pakliže
se	se	k3xPyFc4	se
nezmění	změnit	k5eNaPmIp3nP	změnit
podmínky	podmínka	k1gFnPc1	podmínka
a	a	k8xC	a
hojnost	hojnost	k1gFnSc1	hojnost
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
tak	tak	k6eAd1	tak
zahnízdit	zahnízdit	k5eAaPmF	zahnízdit
i	i	k9	i
dvakrát	dvakrát	k6eAd1	dvakrát
do	do	k7c2	do
roka	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Populace	populace	k1gFnSc2	populace
a	a	k8xC	a
ohrožení	ohrožení	k1gNnSc2	ohrožení
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
IUCN	IUCN	kA	IUCN
je	být	k5eAaImIp3nS	být
tučňák	tučňák	k1gMnSc1	tučňák
brýlový	brýlový	k2eAgMnSc1d1	brýlový
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jednak	jednak	k8xC	jednak
zapříčiněno	zapříčiněn	k2eAgNnSc1d1	zapříčiněno
jejich	jejich	k3xOp3gInSc7	jejich
lovem	lov	k1gInSc7	lov
jako	jako	k9	jako
levných	levný	k2eAgFnPc2d1	levná
rybářských	rybářský	k2eAgFnPc2d1	rybářská
návnad	návnada	k1gFnPc2	návnada
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
sběrem	sběr	k1gInSc7	sběr
vajec	vejce	k1gNnPc2	vejce
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc4	který
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
cca	cca	kA	cca
25	[number]	k4	25
léty	léto	k1gNnPc7	léto
si	se	k3xPyFc3	se
koupit	koupit	k5eAaPmF	koupit
v	v	k7c6	v
Kapském	kapský	k2eAgNnSc6d1	Kapské
městě	město	k1gNnSc6	město
ke	k	k7c3	k
snídani	snídaně	k1gFnSc3	snídaně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ještě	ještě	k9	ještě
počátkem	počátkem	k7c2	počátkem
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
žilo	žít	k5eAaImAgNnS	žít
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Afriky	Afrika	k1gFnSc2	Afrika
až	až	k6eAd1	až
milion	milion	k4xCgInSc1	milion
párů	pár	k1gInPc2	pár
<g/>
,	,	kIx,	,
v	v	k7c6	v
80	[number]	k4	80
létech	léto	k1gNnPc6	léto
počty	počet	k1gInPc1	počet
klesly	klesnout	k5eAaPmAgInP	klesnout
na	na	k7c4	na
60	[number]	k4	60
tisíc	tisíc	k4xCgInPc2	tisíc
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
ochraně	ochrana	k1gFnSc3	ochrana
se	se	k3xPyFc4	se
množství	množství	k1gNnSc1	množství
začalo	začít	k5eAaPmAgNnS	začít
opět	opět	k6eAd1	opět
zvolna	zvolna	k6eAd1	zvolna
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
populace	populace	k1gFnSc1	populace
čítá	čítat	k5eAaImIp3nS	čítat
asi	asi	k9	asi
50	[number]	k4	50
-	-	kIx~	-
90	[number]	k4	90
tisíc	tisíc	k4xCgInPc2	tisíc
dospělých	dospělý	k2eAgMnPc2d1	dospělý
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
nedojde	dojít	k5eNaPmIp3nS	dojít
k	k	k7c3	k
patrnějším	patrný	k2eAgFnPc3d2	patrnější
změnám	změna	k1gFnPc3	změna
<g/>
,	,	kIx,	,
odborníci	odborník	k1gMnPc1	odborník
varují	varovat	k5eAaImIp3nP	varovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgMnS	moct
tučňák	tučňák	k1gMnSc1	tučňák
brýlový	brýlový	k2eAgMnSc1d1	brýlový
za	za	k7c4	za
zhruba	zhruba	k6eAd1	zhruba
15	[number]	k4	15
let	léto	k1gNnPc2	léto
vyhynout	vyhynout	k5eAaPmF	vyhynout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgNnSc7d3	veliký
ohrožením	ohrožení	k1gNnSc7	ohrožení
pro	pro	k7c4	pro
kolonie	kolonie	k1gFnPc4	kolonie
tučňáků	tučňák	k1gMnPc2	tučňák
nadměrné	nadměrný	k2eAgNnSc1d1	nadměrné
množství	množství	k1gNnSc1	množství
rybolovů	rybolov	k1gInPc2	rybolov
<g/>
,	,	kIx,	,
zamořování	zamořování	k1gNnSc4	zamořování
okolí	okolí	k1gNnSc2	okolí
pobřeží	pobřeží	k1gNnSc6	pobřeží
odpady	odpad	k1gInPc4	odpad
z	z	k7c2	z
lidské	lidský	k2eAgFnSc2d1	lidská
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
zabírání	zabírání	k1gNnSc1	zabírání
dalších	další	k2eAgFnPc2d1	další
ploch	plocha	k1gFnPc2	plocha
pobřeží	pobřeží	k1gNnSc2	pobřeží
lidmi	člověk	k1gMnPc7	člověk
a	a	k8xC	a
hlavně	hlavně	k6eAd1	hlavně
těžko	těžko	k6eAd1	těžko
zamezitelné	zamezitelný	k2eAgNnSc4d1	zamezitelný
znečisťování	znečisťování	k1gNnSc4	znečisťování
pobřežních	pobřežní	k2eAgFnPc2d1	pobřežní
vod	voda	k1gFnPc2	voda
ropnými	ropný	k2eAgFnPc7d1	ropná
surovinami	surovina	k1gFnPc7	surovina
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
však	však	k9	však
několik	několik	k4yIc4	několik
organizací	organizace	k1gFnPc2	organizace
(	(	kIx(	(
<g/>
např.	např.	kA	např.
SANCCOB	SANCCOB	kA	SANCCOB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
napomáhají	napomáhat	k5eAaImIp3nP	napomáhat
alespoň	alespoň	k9	alespoň
k	k	k7c3	k
nějaké	nějaký	k3yIgFnSc3	nějaký
stabilitě	stabilita	k1gFnSc3	stabilita
jejich	jejich	k3xOp3gNnSc2	jejich
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chov	chov	k1gInSc1	chov
v	v	k7c6	v
ČR	ČR	kA	ČR
==	==	k?	==
</s>
</p>
<p>
<s>
Tučňáky	tučňák	k1gMnPc7	tučňák
brýlové	brýlový	k2eAgFnSc2d1	brýlová
chová	chovat	k5eAaImIp3nS	chovat
Zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
v	v	k7c6	v
Ústí	ústí	k1gNnSc6	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
slavnostně	slavnostně	k6eAd1	slavnostně
představeni	představen	k2eAgMnPc1d1	představen
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
roku	rok	k1gInSc2	rok
2019	[number]	k4	2019
se	se	k3xPyFc4	se
jednomu	jeden	k4xCgNnSc3	jeden
z	z	k7c2	z
párů	pár	k1gInPc2	pár
<g/>
,	,	kIx,	,
samice	samice	k1gFnPc1	samice
Bobině	Bobina	k1gFnSc3	Bobina
(	(	kIx(	(
<g/>
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
originální	originální	k2eAgNnSc1d1	originální
jméno	jméno	k1gNnSc1	jméno
zní	znět	k5eAaImIp3nS	znět
Jamealybillybob	Jamealybillyboba	k1gFnPc2	Jamealybillyboba
<g/>
)	)	kIx)	)
a	a	k8xC	a
samci	samec	k1gMnSc3	samec
Alfiemu	Alfiem	k1gInSc2	Alfiem
<g/>
,	,	kIx,	,
vylíhla	vylíhnout	k5eAaPmAgFnS	vylíhnout
dvě	dva	k4xCgNnPc4	dva
zdravá	zdravý	k2eAgNnPc4d1	zdravé
mláďata	mládě	k1gNnPc4	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
vůbec	vůbec	k9	vůbec
první	první	k4xOgNnPc1	první
mláďata	mládě	k1gNnPc1	mládě
těchto	tento	k3xDgMnPc2	tento
ptáků	pták	k1gMnPc2	pták
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
této	tento	k3xDgFnSc2	tento
zoo	zoo	k1gFnSc2	zoo
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Unie	unie	k1gFnSc2	unie
českých	český	k2eAgFnPc2d1	Česká
a	a	k8xC	a
slovenských	slovenský	k2eAgFnPc2d1	slovenská
zoologických	zoologický	k2eAgFnPc2d1	zoologická
zahrad	zahrada	k1gFnPc2	zahrada
(	(	kIx(	(
<g/>
UCSZOO	UCSZOO	kA	UCSZOO
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
VESELOVSKÝ	Veselovský	k1gMnSc1	Veselovský
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Zvířata	zvíře	k1gNnPc1	zvíře
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
–	–	k?	–
Tučňáci	tučňák	k1gMnPc1	tučňák
<g/>
.	.	kIx.	.
</s>
<s>
Svazek	svazek	k1gInSc1	svazek
10	[number]	k4	10
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Státní	státní	k2eAgNnSc1d1	státní
zemědělské	zemědělský	k2eAgNnSc1d1	zemědělské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
tučňák	tučňák	k1gMnSc1	tučňák
brýlový	brýlový	k2eAgMnSc1d1	brýlový
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
tučňák	tučňák	k1gMnSc1	tučňák
brýlový	brýlový	k2eAgMnSc1d1	brýlový
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
www.biolib.cz	www.biolib.cz	k1gMnSc1	www.biolib.cz
</s>
</p>
<p>
<s>
poutnik	poutnik	k1gInSc1	poutnik
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
Tučňáci	tučňák	k1gMnPc1	tučňák
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
</s>
</p>
<p>
<s>
www.penguinsworld.cz	www.penguinsworld.cz	k1gMnSc1	www.penguinsworld.cz
</s>
</p>
