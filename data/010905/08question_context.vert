<s>
Sopřečský	Sopřečský	k2eAgInSc1d1	Sopřečský
kanál	kanál	k1gInSc1	kanál
má	mít	k5eAaImIp3nS	mít
výše	vysoce	k6eAd2	vysoce
položenou	položený	k2eAgFnSc4d1	položená
hladinu	hladina	k1gFnSc4	hladina
a	a	k8xC	a
rybník	rybník	k1gInSc1	rybník
je	být	k5eAaImIp3nS	být
napájen	napájet	k5eAaImNgInS	napájet
krátkou	krátký	k2eAgFnSc7d1	krátká
odbočkou	odbočka	k1gFnSc7	odbočka
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
přepadového	přepadový	k2eAgNnSc2d1	přepadové
stavidla	stavidlo	k1gNnSc2	stavidlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
svého	svůj	k3xOyFgInSc2	svůj
toku	tok	k1gInSc2	tok
se	se	k3xPyFc4	se
kanál	kanál	k1gInSc1	kanál
vlévá	vlévat	k5eAaImIp3nS	vlévat
do	do	k7c2	do
Sopřečského	Sopřečský	k2eAgInSc2d1	Sopřečský
rybníku	rybník	k1gInSc5	rybník
<g/>
.	.	kIx.	.
</s>
