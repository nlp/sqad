<p>
<s>
Sopřečský	Sopřečský	k2eAgInSc1d1	Sopřečský
kanál	kanál	k1gInSc1	kanál
(	(	kIx(	(
<g/>
též	též	k9	též
Výrovský	výrovský	k2eAgInSc4d1	výrovský
kanál	kanál	k1gInSc4	kanál
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vodní	vodní	k2eAgNnSc1d1	vodní
dílo	dílo	k1gNnSc1	dílo
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Pardubice	Pardubice	k1gInPc1	Pardubice
dnes	dnes	k6eAd1	dnes
sloužící	sloužící	k2eAgMnSc1d1	sloužící
k	k	k7c3	k
napájení	napájení	k1gNnSc3	napájení
rybníků	rybník	k1gInPc2	rybník
Černého	Černý	k1gMnSc2	Černý
Nadýmače	nadýmač	k1gMnSc2	nadýmač
a	a	k8xC	a
Sopřečského	Sopřečský	k2eAgMnSc2d1	Sopřečský
rybníku	rybník	k1gInSc6	rybník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sopřečský	Sopřečský	k2eAgInSc1d1	Sopřečský
kanál	kanál	k1gInSc1	kanál
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
pardubické	pardubický	k2eAgFnSc2d1	pardubická
rybniční	rybniční	k2eAgFnSc2d1	rybniční
soustavy	soustava	k1gFnSc2	soustava
budované	budovaný	k2eAgInPc4d1	budovaný
Pernštejny	Pernštejn	k1gInPc7	Pernštejn
v	v	k7c6	v
šestnáctém	šestnáctý	k4xOgInSc6	šestnáctý
století	století	k1gNnSc6	století
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
odbočnou	odbočný	k2eAgFnSc4d1	odbočná
větev	větev	k1gFnSc4	větev
Opatovického	opatovický	k2eAgInSc2d1	opatovický
kanálu	kanál	k1gInSc2	kanál
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
napájení	napájení	k1gNnSc3	napájení
rybníků	rybník	k1gInPc2	rybník
Černého	Černý	k1gMnSc2	Černý
Nadýmače	nadýmač	k1gMnSc2	nadýmač
a	a	k8xC	a
Sopřečského	Sopřečský	k2eAgMnSc2d1	Sopřečský
rybníku	rybník	k1gInSc6	rybník
<g/>
,	,	kIx,	,
další	další	k2eAgMnSc1d1	další
jím	jíst	k5eAaImIp1nS	jíst
napájené	napájený	k2eAgInPc4d1	napájený
rybníky	rybník	k1gInPc4	rybník
již	již	k6eAd1	již
dnes	dnes	k6eAd1	dnes
neexistují	existovat	k5eNaImIp3nP	existovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vtok	vtok	k1gInSc1	vtok
do	do	k7c2	do
z	z	k7c2	z
Opatovického	opatovický	k2eAgMnSc2d1	opatovický
do	do	k7c2	do
Sopřečského	Sopřečský	k2eAgInSc2d1	Sopřečský
kanálu	kanál	k1gInSc2	kanál
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
u	u	k7c2	u
jezu	jez	k1gInSc2	jez
v	v	k7c6	v
lese	les	k1gInSc6	les
asi	asi	k9	asi
0,5	[number]	k4	0,5
km	km	kA	km
východně	východně	k6eAd1	východně
od	od	k7c2	od
místní	místní	k2eAgFnSc2d1	místní
části	část	k1gFnSc2	část
Výrova	výrův	k2eAgFnSc1d1	výrova
-	-	kIx~	-
místní	místní	k2eAgFnSc6d1	místní
části	část	k1gFnSc6	část
obce	obec	k1gFnSc2	obec
Břehy	břeh	k1gInPc4	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Kanál	kanál	k1gInSc1	kanál
je	být	k5eAaImIp3nS	být
veden	vést	k5eAaImNgInS	vést
lesním	lesní	k2eAgInSc7d1	lesní
porostem	porost	k1gInSc7	porost
převážně	převážně	k6eAd1	převážně
severozápadním	severozápadní	k2eAgInSc7d1	severozápadní
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počáteční	počáteční	k2eAgFnSc6d1	počáteční
části	část	k1gFnSc6	část
jeho	on	k3xPp3gInSc2	on
toku	tok	k1gInSc2	tok
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
přimknuta	přimknout	k5eAaPmNgFnS	přimknout
okružní	okružní	k2eAgFnSc1d1	okružní
turistická	turistický	k2eAgFnSc1d1	turistická
značená	značený	k2eAgFnSc1d1	značená
trasa	trasa	k1gFnSc1	trasa
7259	[number]	k4	7259
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Výrovský	výrovský	k2eAgInSc1d1	výrovský
okruh	okruh	k1gInSc1	okruh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
zprava	zprava	k6eAd1	zprava
obtéká	obtékat	k5eAaImIp3nS	obtékat
rybník	rybník	k1gInSc4	rybník
Černý	Černý	k1gMnSc1	Černý
Nadýmač	nadýmač	k1gMnSc1	nadýmač
<g/>
.	.	kIx.	.
</s>
<s>
Sopřečský	Sopřečský	k2eAgInSc1d1	Sopřečský
kanál	kanál	k1gInSc1	kanál
má	mít	k5eAaImIp3nS	mít
výše	vysoce	k6eAd2	vysoce
položenou	položený	k2eAgFnSc4d1	položená
hladinu	hladina	k1gFnSc4	hladina
a	a	k8xC	a
rybník	rybník	k1gInSc1	rybník
je	být	k5eAaImIp3nS	být
napájen	napájet	k5eAaImNgInS	napájet
krátkou	krátký	k2eAgFnSc7d1	krátká
odbočkou	odbočka	k1gFnSc7	odbočka
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
přepadového	přepadový	k2eAgNnSc2d1	přepadové
stavidla	stavidlo	k1gNnSc2	stavidlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
svého	svůj	k3xOyFgInSc2	svůj
toku	tok	k1gInSc2	tok
se	se	k3xPyFc4	se
kanál	kanál	k1gInSc1	kanál
vlévá	vlévat	k5eAaImIp3nS	vlévat
do	do	k7c2	do
Sopřečského	Sopřečský	k2eAgInSc2d1	Sopřečský
rybníku	rybník	k1gInSc5	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
je	být	k5eAaImIp3nS	být
voda	voda	k1gFnSc1	voda
odváděna	odváděn	k2eAgFnSc1d1	odváděna
pomocí	pomocí	k7c2	pomocí
Sopřečského	Sopřečský	k2eAgInSc2d1	Sopřečský
potoka	potok	k1gInSc2	potok
(	(	kIx(	(
<g/>
též	též	k9	též
Mulda	mulda	k1gFnSc1	mulda
<g/>
)	)	kIx)	)
do	do	k7c2	do
Labe	Labe	k1gNnSc2	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
hrází	hráz	k1gFnSc7	hráz
rybníka	rybník	k1gInSc2	rybník
je	být	k5eAaImIp3nS	být
kanál	kanál	k1gInSc1	kanál
s	s	k7c7	s
potokem	potok	k1gInSc7	potok
spojen	spojit	k5eAaPmNgInS	spojit
suchým	suchý	k2eAgInSc7d1	suchý
příkopem	příkop	k1gInSc7	příkop
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
přímému	přímý	k2eAgNnSc3d1	přímé
převádění	převádění	k1gNnSc3	převádění
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
případě	případ	k1gInSc6	případ
přerušení	přerušení	k1gNnSc2	přerušení
napájení	napájení	k1gNnSc2	napájení
rybníka	rybník	k1gInSc2	rybník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
trase	trasa	k1gFnSc6	trasa
kanálu	kanál	k1gInSc2	kanál
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
dvojice	dvojice	k1gFnSc1	dvojice
akvaduktů	akvadukt	k1gInPc2	akvadukt
<g/>
.	.	kIx.	.
</s>
<s>
Vzdušná	vzdušný	k2eAgFnSc1d1	vzdušná
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
mezi	mezi	k7c7	mezi
vtokem	vtok	k1gInSc7	vtok
a	a	k8xC	a
ústím	ústí	k1gNnSc7	ústí
kanálu	kanál	k1gInSc2	kanál
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
tři	tři	k4xCgInPc4	tři
kilometry	kilometr	k1gInPc4	kilometr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
