<p>
<s>
General	Generat	k5eAaImAgMnS	Generat
Electric	Electric	k1gMnSc1	Electric
Company	Compana	k1gFnSc2	Compana
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
GE	GE	kA	GE
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
NYSE	NYSE	kA	NYSE
<g/>
:	:	kIx,	:
GE	GE	kA	GE
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nadnárodní	nadnárodní	k2eAgInSc1d1	nadnárodní
konglomerát	konglomerát	k1gInSc1	konglomerát
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
americkém	americký	k2eAgNnSc6d1	americké
městě	město	k1gNnSc6	město
Fairfield	Fairfielda	k1gFnPc2	Fairfielda
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Connecticut	Connecticut	k1gInSc1	Connecticut
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1892	[number]	k4	1892
Edisonem	Edison	k1gInSc7	Edison
<g/>
,	,	kIx,	,
Thomsonem	Thomson	k1gInSc7	Thomson
<g/>
,	,	kIx,	,
Houstonem	Houston	k1gInSc7	Houston
a	a	k8xC	a
Coffinem	Coffin	k1gInSc7	Coffin
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
jejím	její	k3xOp3gInSc7	její
prvním	první	k4xOgMnSc7	první
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podnikání	podnikání	k1gNnSc1	podnikání
==	==	k?	==
</s>
</p>
<p>
<s>
GE	GE	kA	GE
vyniká	vynikat	k5eAaImIp3nS	vynikat
zejména	zejména	k9	zejména
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
dopravní	dopravní	k2eAgFnSc2d1	dopravní
technologie	technologie	k1gFnSc2	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
obory	obor	k1gInPc4	obor
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgInPc4	který
se	se	k3xPyFc4	se
soustředí	soustředit	k5eAaPmIp3nS	soustředit
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
finančnictví	finančnictví	k1gNnSc1	finančnictví
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
energetika	energetika	k1gFnSc1	energetika
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
média	médium	k1gNnPc1	médium
a	a	k8xC	a
další	další	k2eAgFnSc1d1	další
<g/>
.	.	kIx.	.
<g/>
Před	před	k7c7	před
ekonomickou	ekonomický	k2eAgFnSc7d1	ekonomická
krizí	krize	k1gFnSc7	krize
se	se	k3xPyFc4	se
její	její	k3xOp3gFnPc1	její
tržby	tržba	k1gFnPc1	tržba
resp.	resp.	kA	resp.
čisté	čistý	k2eAgInPc1d1	čistý
zisky	zisk	k1gInPc1	zisk
vyšplhaly	vyšplhat	k5eAaPmAgInP	vyšplhat
ke	k	k7c3	k
180	[number]	k4	180
resp.	resp.	kA	resp.
22	[number]	k4	22
mld	mld	k?	mld
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
rok	rok	k1gInSc4	rok
2010	[number]	k4	2010
měla	mít	k5eAaImAgFnS	mít
GE	GE	kA	GE
tržby	tržba	k1gFnSc2	tržba
o	o	k7c6	o
výši	výše	k1gFnSc6	výše
150,211	[number]	k4	150,211
mld	mld	k?	mld
a	a	k8xC	a
čistý	čistý	k2eAgInSc4d1	čistý
zisk	zisk	k1gInSc4	zisk
12,163	[number]	k4	12,163
mld	mld	k?	mld
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Média	médium	k1gNnPc1	médium
===	===	k?	===
</s>
</p>
<p>
<s>
Nejznámější	známý	k2eAgFnPc1d3	nejznámější
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gFnPc1	jeho
televizní	televizní	k2eAgFnPc1d1	televizní
společnosti	společnost	k1gFnPc1	společnost
NBC	NBC	kA	NBC
a	a	k8xC	a
Universal	Universal	k1gMnSc1	Universal
Pictures	Pictures	k1gMnSc1	Pictures
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
vlastní	vlastní	k2eAgFnSc2d1	vlastní
společnosti	společnost	k1gFnSc2	společnost
jako	jako	k8xC	jako
Telemundo	Telemundo	k1gNnSc4	Telemundo
<g/>
,	,	kIx,	,
Focus	Focus	k1gMnSc1	Focus
Features	Features	k1gMnSc1	Features
<g/>
,	,	kIx,	,
26	[number]	k4	26
televizních	televizní	k2eAgFnPc2d1	televizní
stanic	stanice	k1gFnPc2	stanice
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kabelovou	kabelový	k2eAgFnSc4d1	kabelová
televizní	televizní	k2eAgFnSc4d1	televizní
síť	síť	k1gFnSc4	síť
MSNBC	MSNBC	kA	MSNBC
<g/>
,	,	kIx,	,
Bravo	bravo	k6eAd1	bravo
a	a	k8xC	a
Syfy	Syf	k2eAgFnPc1d1	Syf
(	(	kIx(	(
<g/>
současně	současně	k6eAd1	současně
vlastní	vlastní	k2eAgFnSc4d1	vlastní
Sci-fi	scii	k1gFnSc4	sci-fi
magazine	magazinout	k5eAaPmIp3nS	magazinout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
GE	GE	kA	GE
taktéž	taktéž	k?	taktéž
vlastní	vlastní	k2eAgInSc4d1	vlastní
80	[number]	k4	80
<g/>
%	%	kIx~	%
podíl	podíl	k1gInSc4	podíl
ve	v	k7c6	v
filmových	filmový	k2eAgNnPc6d1	filmové
studiích	studio	k1gNnPc6	studio
NBC	NBC	kA	NBC
Universal	Universal	k1gFnPc2	Universal
<g/>
.	.	kIx.	.
</s>
<s>
Nebývale	nebývale	k6eAd1	nebývale
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
nezanedbatelnou	zanedbatelný	k2eNgFnSc4d1	nezanedbatelná
část	část	k1gFnSc4	část
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
amerických	americký	k2eAgNnPc2d1	americké
<g/>
)	)	kIx)	)
médií	médium	k1gNnPc2	médium
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgFnSc2	svůj
kritiky	kritika	k1gFnSc2	kritika
–	–	k?	–
ti	ten	k3xDgMnPc1	ten
například	například	k6eAd1	například
podotýkají	podotýkat	k5eAaImIp3nP	podotýkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
těžko	těžko	k6eAd1	těžko
myslitelné	myslitelný	k2eAgNnSc1d1	myslitelné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
např.	např.	kA	např.
zpravodajství	zpravodajství	k1gNnSc2	zpravodajství
NBC	NBC	kA	NBC
přišlo	přijít	k5eAaPmAgNnS	přijít
se	s	k7c7	s
zprávou	zpráva	k1gFnSc7	zpráva
výrazně	výrazně	k6eAd1	výrazně
kritickou	kritický	k2eAgFnSc7d1	kritická
vůči	vůči	k7c3	vůči
GE	GE	kA	GE
nebo	nebo	k8xC	nebo
některému	některý	k3yIgInSc3	některý
jeho	on	k3xPp3gInSc2	on
produktu	produkt	k1gInSc2	produkt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Divize	divize	k1gFnSc2	divize
GE	GE	kA	GE
==	==	k?	==
</s>
</p>
<p>
<s>
GE	GE	kA	GE
Commercial	Commercial	k1gInSc1	Commercial
Finance	finance	k1gFnSc1	finance
</s>
</p>
<p>
<s>
GE	GE	kA	GE
Industrial	Industrial	k1gMnSc1	Industrial
</s>
</p>
<p>
<s>
GE	GE	kA	GE
Infrastructure	Infrastructur	k1gInSc5	Infrastructur
</s>
</p>
<p>
<s>
GE	GE	kA	GE
Aviation	Aviation	k1gInSc1	Aviation
</s>
</p>
<p>
<s>
GE	GE	kA	GE
Transportation	Transportation	k1gInSc1	Transportation
Systems	Systems	k1gInSc1	Systems
</s>
</p>
<p>
<s>
GE	GE	kA	GE
Energy	Energ	k1gInPc4	Energ
</s>
</p>
<p>
<s>
GE	GE	kA	GE
Commercial	Commercial	k1gInSc1	Commercial
Aviation	Aviation	k1gInSc1	Aviation
Services	Services	k1gInSc1	Services
</s>
</p>
<p>
<s>
GE	GE	kA	GE
Energy	Energ	k1gInPc4	Energ
Financial	Financial	k1gMnSc1	Financial
Services	Services	k1gMnSc1	Services
</s>
</p>
<p>
<s>
GE	GE	kA	GE
Oil	Oil	k1gMnSc1	Oil
&	&	k?	&
Gas	Gas	k1gMnSc1	Gas
</s>
</p>
<p>
<s>
GE	GE	kA	GE
Water	Water	k1gMnSc1	Water
&	&	k?	&
Process	Process	k1gInSc1	Process
Technologies	Technologies	k1gInSc1	Technologies
</s>
</p>
<p>
<s>
GE	GE	kA	GE
Capital	Capital	k1gMnSc1	Capital
</s>
</p>
<p>
<s>
GE	GE	kA	GE
Healthcare	Healthcar	k1gMnSc5	Healthcar
</s>
</p>
<p>
<s>
NBC	NBC	kA	NBC
Universal	Universal	k1gFnSc1	Universal
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Jack	Jack	k1gMnSc1	Jack
Welch	Welch	k1gMnSc1	Welch
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
General	General	k1gFnSc2	General
Electric	Electrice	k1gInPc2	Electrice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
