<s>
Next	Next	k2eAgInSc4d1	Next
Computers	Computers	k1gInSc4	Computers
byla	být	k5eAaImAgFnS	být
americká	americký	k2eAgFnSc1d1	americká
počítačová	počítačový	k2eAgFnSc1d1	počítačová
společnost	společnost	k1gFnSc1	společnost
sídlící	sídlící	k2eAgFnSc1d1	sídlící
v	v	k7c4	v
Redwood	Redwood	k1gInSc4	Redwood
City	city	k1gNnSc1	city
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
společnost	společnost	k1gFnSc1	společnost
byla	být	k5eAaImAgFnS	být
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
a	a	k8xC	a
výrobu	výroba	k1gFnSc4	výroba
výkonných	výkonný	k2eAgFnPc2d1	výkonná
pracovních	pracovní	k2eAgFnPc2d1	pracovní
stanic	stanice	k1gFnPc2	stanice
určených	určený	k2eAgFnPc2d1	určená
pro	pro	k7c4	pro
firmy	firma	k1gFnPc4	firma
a	a	k8xC	a
univerzity	univerzita	k1gFnPc4	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
NeXT	NeXT	k?	NeXT
Computers	Computers	k1gInSc1	Computers
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
spoluzakladatelem	spoluzakladatel	k1gMnSc7	spoluzakladatel
Apple	Apple	kA	Apple
Computers	Computersa	k1gFnPc2	Computersa
Stevem	Steve	k1gMnSc7	Steve
Jobsem	Jobs	k1gMnSc7	Jobs
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
na	na	k7c4	na
svoji	svůj	k3xOyFgFnSc4	svůj
funkci	funkce	k1gFnSc4	funkce
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
Apple	Apple	kA	Apple
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
představil	představit	k5eAaPmAgMnS	představit
NeXT	NeXT	k1gMnSc1	NeXT
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
počítač	počítač	k1gInSc4	počítač
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
menší	malý	k2eAgInSc1d2	menší
NeXTstation	NeXTstation	k1gInSc1	NeXTstation
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
počítačů	počítač	k1gInPc2	počítač
NeXT	NeXT	k1gMnPc2	NeXT
prodalo	prodat	k5eAaPmAgNnS	prodat
okolo	okolo	k7c2	okolo
50	[number]	k4	50
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
jeho	jeho	k3xOp3gInSc1	jeho
inovativní	inovativní	k2eAgInSc1d1	inovativní
objektově	objektově	k6eAd1	objektově
orientovaný	orientovaný	k2eAgInSc1d1	orientovaný
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
NeXT	NeXT	k1gFnPc2	NeXT
STEP	step	k1gInSc4	step
a	a	k8xC	a
vývojové	vývojový	k2eAgNnSc4d1	vývojové
prostředí	prostředí	k1gNnPc4	prostředí
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
dobu	doba	k1gFnSc4	doba
velmi	velmi	k6eAd1	velmi
pokrokový	pokrokový	k2eAgInSc1d1	pokrokový
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
založení	založení	k1gNnSc6	založení
firmy	firma	k1gFnSc2	firma
NeXT	NeXT	k1gFnPc2	NeXT
se	se	k3xPyFc4	se
Steve	Steve	k1gMnSc1	Steve
Jobs	Jobsa	k1gFnPc2	Jobsa
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgMnPc7	svůj
spolupracovníky	spolupracovník	k1gMnPc7	spolupracovník
vrátili	vrátit	k5eAaPmAgMnP	vrátit
k	k	k7c3	k
myšlence	myšlenka	k1gFnSc3	myšlenka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
Jobsovi	Jobsův	k2eAgMnPc1d1	Jobsův
ještě	ještě	k9	ještě
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
jeho	on	k3xPp3gNnSc2	on
řízení	řízení	k1gNnSc2	řízení
firmy	firma	k1gFnSc2	firma
Apple	Apple	kA	Apple
nadnesl	nadnést	k5eAaPmAgMnS	nadnést
Paul	Paul	k1gMnSc1	Paul
Berg	Berg	k1gMnSc1	Berg
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
firma	firma	k1gFnSc1	firma
Apple	Apple	kA	Apple
zkonstruovala	zkonstruovat	k5eAaPmAgFnS	zkonstruovat
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
grafickou	grafický	k2eAgFnSc4d1	grafická
pracovní	pracovní	k2eAgFnSc4d1	pracovní
stanici	stanice	k1gFnSc4	stanice
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
například	například	k6eAd1	například
pro	pro	k7c4	pro
provádění	provádění	k1gNnSc4	provádění
simulací	simulace	k1gFnPc2	simulace
chemických	chemický	k2eAgFnPc2d1	chemická
reakcí	reakce	k1gFnPc2	reakce
se	s	k7c7	s
zobrazením	zobrazení	k1gNnSc7	zobrazení
výsledků	výsledek	k1gInPc2	výsledek
na	na	k7c6	na
monitoru	monitor	k1gInSc6	monitor
s	s	k7c7	s
vysokým	vysoký	k2eAgNnSc7d1	vysoké
rozlišením	rozlišení	k1gNnSc7	rozlišení
<g/>
.	.	kIx.	.
</s>
<s>
Berg	Berg	k1gMnSc1	Berg
tuto	tento	k3xDgFnSc4	tento
pracovní	pracovní	k2eAgFnSc4d1	pracovní
stanici	stanice	k1gFnSc4	stanice
označoval	označovat	k5eAaImAgInS	označovat
jménem	jméno	k1gNnSc7	jméno
"	"	kIx"	"
<g/>
3	[number]	k4	3
<g/>
M	M	kA	M
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
písmena	písmeno	k1gNnPc4	písmeno
M	M	kA	M
znamenala	znamenat	k5eAaImAgNnP	znamenat
<g/>
:	:	kIx,	:
jeden	jeden	k4xCgInSc1	jeden
Megabajt	megabajt	k1gInSc1	megabajt
operační	operační	k2eAgFnSc2d1	operační
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
displej	displej	k1gInSc1	displej
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
Miliónem	milión	k4xCgInSc7	milión
pixelů	pixel	k1gInPc2	pixel
(	(	kIx(	(
<g/>
megapixel	megapixel	k1gInSc4	megapixel
<g/>
)	)	kIx)	)
výpočetní	výpočetní	k2eAgFnSc4d1	výpočetní
rychlost	rychlost	k1gFnSc4	rychlost
jeden	jeden	k4xCgInSc4	jeden
MFLOPS	MFLOPS	kA	MFLOPS
<g/>
.	.	kIx.	.
</s>
<s>
Pevný	pevný	k2eAgInSc1d1	pevný
disk	disk	k1gInSc1	disk
prvního	první	k4xOgInSc2	první
počítače	počítač	k1gInSc2	počítač
NeXT	NeXT	k1gFnSc2	NeXT
byl	být	k5eAaImAgInS	být
nabízen	nabízet	k5eAaImNgInS	nabízet
v	v	k7c6	v
několika	několik	k4yIc6	několik
variantách	varianta	k1gFnPc6	varianta
:	:	kIx,	:
40	[number]	k4	40
MB	MB	kA	MB
,	,	kIx,	,
330	[number]	k4	330
MB	MB	kA	MB
nebo	nebo	k8xC	nebo
660	[number]	k4	660
MB	MB	kA	MB
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
zcela	zcela	k6eAd1	zcela
revolučním	revoluční	k2eAgInSc7d1	revoluční
prvkem	prvek	k1gInSc7	prvek
nového	nový	k2eAgMnSc2d1	nový
NeXT	NeXT	k1gMnSc2	NeXT
Computeru	computer	k1gInSc2	computer
byla	být	k5eAaImAgFnS	být
zabudovaná	zabudovaný	k2eAgFnSc1d1	zabudovaná
magnetooptická	magnetooptický	k2eAgFnSc1d1	magnetooptická
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
naprostou	naprostý	k2eAgFnSc7d1	naprostá
novinkou	novinka	k1gFnSc7	novinka
<g/>
.	.	kIx.	.
</s>
<s>
NeXT	NeXT	k?	NeXT
Computer	computer	k1gInSc4	computer
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
provozovat	provozovat	k5eAaImF	provozovat
i	i	k9	i
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
magnetooptickou	magnetooptický	k2eAgFnSc7d1	magnetooptická
jednotkou	jednotka	k1gFnSc7	jednotka
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
konfiguraci	konfigurace	k1gFnSc6	konfigurace
nemohl	moct	k5eNaImAgMnS	moct
MO	MO	kA	MO
disk	disk	k1gInSc1	disk
z	z	k7c2	z
počítače	počítač	k1gInSc2	počítač
vysunovat	vysunovat	k5eAaImF	vysunovat
za	za	k7c2	za
jeho	jeho	k3xOp3gInSc2	jeho
běhu	běh	k1gInSc2	běh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počítači	počítač	k1gInSc6	počítač
byla	být	k5eAaImAgFnS	být
umístěna	umístěn	k2eAgFnSc1d1	umístěna
taktéž	taktéž	k?	taktéž
síťová	síťový	k2eAgFnSc1d1	síťová
karta	karta	k1gFnSc1	karta
pro	pro	k7c4	pro
Ethernet	Ethernet	k1gInSc4	Ethernet
<g/>
.	.	kIx.	.
</s>
<s>
Grafický	grafický	k2eAgInSc4d1	grafický
subsystém	subsystém	k1gInSc4	subsystém
počítače	počítač	k1gInSc2	počítač
NeXT	NeXT	k1gFnSc2	NeXT
Computer	computer	k1gInSc1	computer
byl	být	k5eAaImAgInS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
ovládat	ovládat	k5eAaImF	ovládat
sedmnáctipalcový	sedmnáctipalcový	k2eAgInSc1d1	sedmnáctipalcový
displej	displej	k1gInSc1	displej
tzv.	tzv.	kA	tzv.
MegaPixel	MegaPixel	k1gInSc4	MegaPixel
<g/>
,	,	kIx,	,
zobrazující	zobrazující	k2eAgFnSc4d1	zobrazující
grafiku	grafika	k1gFnSc4	grafika
ve	v	k7c6	v
čtyřech	čtyři	k4xCgInPc6	čtyři
stupních	stupeň	k1gInPc6	stupeň
šedi	šeď	k1gFnSc2	šeď
(	(	kIx(	(
<g/>
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc4	dva
stupně	stupeň	k1gInPc4	stupeň
šedi	šeď	k1gFnSc2	šeď
a	a	k8xC	a
bílá	bílý	k2eAgNnPc1d1	bílé
<g/>
)	)	kIx)	)
v	v	k7c6	v
rozlišení	rozlišení	k1gNnSc6	rozlišení
1120	[number]	k4	1120
<g/>
×	×	k?	×
<g/>
832	[number]	k4	832
pixelů	pixel	k1gInPc2	pixel
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
moduly	modul	k1gInPc1	modul
NeXT	NeXT	k1gFnPc2	NeXT
Computeru	computer	k1gInSc2	computer
byly	být	k5eAaImAgInP	být
kromě	kromě	k7c2	kromě
displeje	displej	k1gInSc2	displej
<g/>
,	,	kIx,	,
klávesnice	klávesnice	k1gFnPc1	klávesnice
a	a	k8xC	a
myši	myš	k1gFnPc1	myš
umístěny	umístit	k5eAaPmNgFnP	umístit
do	do	k7c2	do
čisté	čistý	k2eAgFnSc2d1	čistá
tmavé	tmavý	k2eAgFnSc2d1	tmavá
kostky	kostka	k1gFnSc2	kostka
o	o	k7c6	o
stranách	strana	k1gFnPc6	strana
majících	mající	k2eAgFnPc6d1	mající
délku	délka	k1gFnSc4	délka
přesně	přesně	k6eAd1	přesně
jedné	jeden	k4xCgFnSc2	jeden
stopy	stopa	k1gFnSc2	stopa
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
se	se	k3xPyFc4	se
tomuto	tento	k3xDgInSc3	tento
počítači	počítač	k1gInSc3	počítač
přezdívalo	přezdívat	k5eAaImAgNnS	přezdívat
the	the	k?	the
cube	cube	k1gNnSc1	cube
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
uvedla	uvést	k5eAaPmAgFnS	uvést
společnost	společnost	k1gFnSc1	společnost
NeXT	NeXT	k1gFnSc2	NeXT
na	na	k7c4	na
trh	trh	k1gInSc4	trh
druhou	druhý	k4xOgFnSc4	druhý
generaci	generace	k1gFnSc4	generace
svých	svůj	k3xOyFgFnPc2	svůj
grafických	grafický	k2eAgFnPc2d1	grafická
pracovních	pracovní	k2eAgFnPc2d1	pracovní
stanic	stanice	k1gFnPc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
vylepšený	vylepšený	k2eAgInSc4d1	vylepšený
NeXT	NeXT	k1gFnPc7	NeXT
Computer	computer	k1gInSc1	computer
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
druhé	druhý	k4xOgFnSc6	druhý
verzi	verze	k1gFnSc6	verze
dostal	dostat	k5eAaPmAgMnS	dostat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
přiléhavý	přiléhavý	k2eAgInSc1d1	přiléhavý
název	název	k1gInSc1	název
NeXTcube	NeXTcub	k1gInSc5	NeXTcub
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
počítač	počítač	k1gInSc4	počítač
vybavený	vybavený	k2eAgInSc4d1	vybavený
mikroprocesorem	mikroprocesor	k1gInSc7	mikroprocesor
Motorola	Motorola	kA	Motorola
M	M	kA	M
<g/>
68040	[number]	k4	68040
<g/>
s	s	k7c7	s
s	s	k7c7	s
frekvencí	frekvence	k1gFnSc7	frekvence
25	[number]	k4	25
MHz	Mhz	kA	Mhz
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
kromě	kromě	k7c2	kromě
jednotky	jednotka	k1gFnSc2	jednotka
pro	pro	k7c4	pro
řízení	řízení	k1gNnSc4	řízení
přístupu	přístup	k1gInSc2	přístup
do	do	k7c2	do
operační	operační	k2eAgFnSc2d1	operační
paměti	paměť	k1gFnSc2	paměť
(	(	kIx(	(
<g/>
MMU	MMU	kA	MMU
–	–	k?	–
Memory	Memora	k1gFnSc2	Memora
Management	management	k1gInSc1	management
Unit	Unit	k1gInSc1	Unit
<g/>
)	)	kIx)	)
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
i	i	k9	i
plnohodnotný	plnohodnotný	k2eAgInSc1d1	plnohodnotný
matematický	matematický	k2eAgInSc1d1	matematický
koprocesor	koprocesor	k1gInSc1	koprocesor
(	(	kIx(	(
<g/>
FPU	FPU	kA	FPU
–	–	k?	–
Floating	Floating	k1gInSc1	Floating
Point	pointa	k1gFnPc2	pointa
Unit	Unita	k1gFnPc2	Unita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
kromě	kromě	k7c2	kromě
tohoto	tento	k3xDgInSc2	tento
mikroprocesoru	mikroprocesor	k1gInSc2	mikroprocesor
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
pracovní	pracovní	k2eAgFnSc6d1	pracovní
stanici	stanice	k1gFnSc6	stanice
NeXTcube	NeXTcub	k1gInSc5	NeXTcub
zabudován	zabudovat	k5eAaPmNgInS	zabudovat
i	i	k9	i
druhý	druhý	k4xOgInSc1	druhý
procesor	procesor	k1gInSc1	procesor
–	–	k?	–
konkrétně	konkrétně	k6eAd1	konkrétně
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c6	o
DSP	DSP	kA	DSP
(	(	kIx(	(
<g/>
číslicový	číslicový	k2eAgInSc1d1	číslicový
signálový	signálový	k2eAgInSc1d1	signálový
procesor	procesor	k1gInSc1	procesor
<g/>
)	)	kIx)	)
Motorola	Motorola	kA	Motorola
56001	[number]	k4	56001
s	s	k7c7	s
frekvencí	frekvence	k1gFnSc7	frekvence
25	[number]	k4	25
MHz	Mhz	kA	Mhz
<g/>
.	.	kIx.	.
</s>
<s>
Úlohou	úloha	k1gFnSc7	úloha
tohoto	tento	k3xDgInSc2	tento
DSP	DSP	kA	DSP
bylo	být	k5eAaImAgNnS	být
především	především	k6eAd1	především
řízení	řízení	k1gNnSc1	řízení
zvukového	zvukový	k2eAgInSc2d1	zvukový
subsystému	subsystém	k1gInSc2	subsystém
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
CPU	CPU	kA	CPU
a	a	k8xC	a
DSP	DSP	kA	DSP
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
NeXTcube	NeXTcub	k1gInSc5	NeXTcub
i	i	k9	i
dvanáct	dvanáct	k4xCc1	dvanáct
samostatně	samostatně	k6eAd1	samostatně
pracujících	pracující	k2eAgMnPc2d1	pracující
a	a	k8xC	a
programovatelných	programovatelný	k2eAgMnPc2d1	programovatelný
DMA	dmout	k5eAaImSgInS	dmout
kanálů	kanál	k1gInPc2	kanál
používaných	používaný	k2eAgInPc2d1	používaný
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
operací	operace	k1gFnPc2	operace
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
pro	pro	k7c4	pro
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
datům	datum	k1gNnPc3	datum
na	na	k7c6	na
pevném	pevný	k2eAgInSc6d1	pevný
disku	disk	k1gInSc6	disk
<g/>
,	,	kIx,	,
grafickým	grafický	k2eAgInPc3d1	grafický
přenosům	přenos	k1gInPc3	přenos
<g/>
,	,	kIx,	,
přenosům	přenos	k1gInPc3	přenos
dat	datum	k1gNnPc2	datum
atd.	atd.	kA	atd.
Základní	základní	k2eAgInSc1d1	základní
grafické	grafický	k2eAgFnSc3d1	grafická
schopnosti	schopnost	k1gFnSc3	schopnost
NeXTcube	NeXTcub	k1gInSc5	NeXTcub
zůstaly	zůstat	k5eAaPmAgInP	zůstat
stejné	stejný	k2eAgInPc4d1	stejný
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
u	u	k7c2	u
původního	původní	k2eAgMnSc2d1	původní
NeXT	NeXT	k1gMnSc2	NeXT
Computeru	computer	k1gInSc2	computer
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
grafické	grafický	k2eAgNnSc4d1	grafické
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
prostředí	prostředí	k1gNnSc4	prostředí
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
zobrazovalo	zobrazovat	k5eAaImAgNnS	zobrazovat
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
grafického	grafický	k2eAgInSc2d1	grafický
režimu	režim	k1gInSc2	režim
o	o	k7c6	o
rozlišení	rozlišení	k1gNnSc6	rozlišení
1120	[number]	k4	1120
<g/>
×	×	k?	×
<g/>
832	[number]	k4	832
pixelů	pixel	k1gInPc2	pixel
se	s	k7c7	s
čtyřmi	čtyři	k4xCgInPc7	čtyři
stupni	stupeň	k1gInPc7	stupeň
šedi	šeď	k1gFnSc2	šeď
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
za	za	k7c4	za
přibližně	přibližně	k6eAd1	přibližně
400	[number]	k4	400
USD	USD	kA	USD
dokoupit	dokoupit	k5eAaPmF	dokoupit
přídavnou	přídavný	k2eAgFnSc4d1	přídavná
grafickou	grafický	k2eAgFnSc4d1	grafická
kartu	karta	k1gFnSc4	karta
umožňující	umožňující	k2eAgFnSc4d1	umožňující
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
32	[number]	k4	32
bitovém	bitový	k2eAgInSc6d1	bitový
grafickém	grafický	k2eAgInSc6d1	grafický
režimu	režim	k1gInSc6	režim
true	truat	k5eAaPmIp3nS	truat
color	color	k1gInSc1	color
<g/>
.	.	kIx.	.
</s>
<s>
Počítač	počítač	k1gInSc1	počítač
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
sestavě	sestava	k1gFnSc6	sestava
nainstalováno	nainstalovat	k5eAaPmNgNnS	nainstalovat
8	[number]	k4	8
MB	MB	kA	MB
operační	operační	k2eAgFnSc2d1	operační
paměti	paměť	k1gFnSc2	paměť
a	a	k8xC	a
pevný	pevný	k2eAgInSc4d1	pevný
disk	disk	k1gInSc4	disk
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
340	[number]	k4	340
MB	MB	kA	MB
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
standardního	standardní	k2eAgNnSc2d1	standardní
sériového	sériový	k2eAgNnSc2d1	sériové
rozhraní	rozhraní	k1gNnSc2	rozhraní
a	a	k8xC	a
rozhraní	rozhraní	k1gNnSc2	rozhraní
sítě	síť	k1gFnSc2	síť
Ethernet	Ethernet	k1gMnSc1	Ethernet
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
zadní	zadní	k2eAgFnSc6d1	zadní
straně	strana	k1gFnSc6	strana
NeXTcube	NeXTcub	k1gInSc5	NeXTcub
vyveden	vyveden	k2eAgInSc4d1	vyveden
i	i	k9	i
konektor	konektor	k1gInSc4	konektor
SCSI	SCSI	kA	SCSI
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
použít	použít	k5eAaPmF	použít
mnoha	mnoho	k4c3	mnoho
způsoby	způsob	k1gInPc4	způsob
–	–	k?	–
k	k	k7c3	k
připojení	připojení	k1gNnSc3	připojení
některých	některý	k3yIgInPc2	některý
typů	typ	k1gInPc2	typ
tiskáren	tiskárna	k1gFnPc2	tiskárna
<g/>
,	,	kIx,	,
skenerů	skener	k1gInPc2	skener
<g/>
,	,	kIx,	,
diskových	diskový	k2eAgFnPc2d1	disková
polí	pole	k1gFnPc2	pole
<g/>
,	,	kIx,	,
laboratorních	laboratorní	k2eAgInPc2d1	laboratorní
přístrojů	přístroj	k1gInPc2	přístroj
atd.	atd.	kA	atd.
Kromě	kromě	k7c2	kromě
relativně	relativně	k6eAd1	relativně
drahé	drahý	k2eAgFnSc2d1	drahá
pracovní	pracovní	k2eAgFnSc2d1	pracovní
stanice	stanice	k1gFnSc2	stanice
NeXTcube	NeXTcub	k1gInSc5	NeXTcub
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
všemi	všecek	k3xTgNnPc7	všecek
rozšířeními	rozšíření	k1gNnPc7	rozšíření
stála	stát	k5eAaImAgFnS	stát
okolo	okolo	k7c2	okolo
15	[number]	k4	15
tis	tis	k1gInSc4	tis
<g/>
.	.	kIx.	.
</s>
<s>
USD	USD	kA	USD
<g/>
,	,	kIx,	,
nabízela	nabízet	k5eAaImAgFnS	nabízet
společnost	společnost	k1gFnSc1	společnost
NeXT	NeXT	k1gFnPc2	NeXT
méně	málo	k6eAd2	málo
náročným	náročný	k2eAgMnPc3d1	náročný
zákazníkům	zákazník	k1gMnPc3	zákazník
méně	málo	k6eAd2	málo
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
<g/>
,	,	kIx,	,
nerozšiřitelnou	rozšiřitelný	k2eNgFnSc4d1	nerozšiřitelná
<g/>
,	,	kIx,	,
levnější	levný	k2eAgFnSc4d2	levnější
variantu	varianta	k1gFnSc4	varianta
–	–	k?	–
stanici	stanice	k1gFnSc3	stanice
NeXTstation	NeXTstation	k1gInSc4	NeXTstation
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
varianta	varianta	k1gFnSc1	varianta
NeXTstation	NeXTstation	k1gInSc1	NeXTstation
byla	být	k5eAaImAgFnS	být
výrazně	výrazně	k6eAd1	výrazně
levnější	levný	k2eAgFnSc1d2	levnější
než	než	k8xS	než
NeXTcube	NeXTcub	k1gInSc5	NeXTcub
<g/>
.	.	kIx.	.
</s>
<s>
NeXTstation	NeXTstation	k1gInSc1	NeXTstation
i	i	k9	i
s	s	k7c7	s
monochromaatickým	monochromaatický	k2eAgInSc7d1	monochromaatický
monitorem	monitor	k1gInSc7	monitor
se	se	k3xPyFc4	se
prodával	prodávat	k5eAaImAgInS	prodávat
za	za	k7c4	za
přibližně	přibližně	k6eAd1	přibližně
třetinovou	třetinový	k2eAgFnSc4d1	třetinová
cenu	cena	k1gFnSc4	cena
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
systému	systém	k1gInSc6	systém
nebyly	být	k5eNaImAgFnP	být
nainstalovány	nainstalovat	k5eAaPmNgFnP	nainstalovat
některé	některý	k3yIgFnPc1	některý
aplikace	aplikace	k1gFnPc1	aplikace
<g/>
,	,	kIx,	,
především	především	k9	především
vývojové	vývojový	k2eAgInPc1d1	vývojový
nástroje	nástroj	k1gInPc1	nástroj
(	(	kIx(	(
<g/>
NeXTSTEP	NeXTSTEP	k1gFnSc1	NeXTSTEP
development	development	k1gMnSc1	development
tools	tools	k1gInSc1	tools
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
ceně	cena	k1gFnSc6	cena
dostal	dostat	k5eAaPmAgMnS	dostat
zákazník	zákazník	k1gMnSc1	zákazník
počítač	počítač	k1gInSc4	počítač
s	s	k7c7	s
8	[number]	k4	8
MB	MB	kA	MB
operační	operační	k2eAgFnSc2d1	operační
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
pevným	pevný	k2eAgNnSc7d1	pevné
diskem	disco	k1gNnSc7	disco
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
105	[number]	k4	105
MB	MB	kA	MB
a	a	k8xC	a
procesorem	procesor	k1gInSc7	procesor
i	i	k8xC	i
DSP	DSP	kA	DSP
shodným	shodný	k2eAgInSc7d1	shodný
s	s	k7c7	s
NeXTcube	NeXTcub	k1gInSc5	NeXTcub
(	(	kIx(	(
<g/>
CPU	CPU	kA	CPU
Motorola	Motorola	kA	Motorola
68040	[number]	k4	68040
@	@	kIx~	@
25	[number]	k4	25
MHz	Mhz	kA	Mhz
<g/>
,	,	kIx,	,
DSP	DSP	kA	DSP
Motorola	Motorola	kA	Motorola
56001	[number]	k4	56001
@	@	kIx~	@
25	[number]	k4	25
MHz	Mhz	kA	Mhz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
viditelná	viditelný	k2eAgFnSc1d1	viditelná
změna	změna	k1gFnSc1	změna
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
výměnných	výměnný	k2eAgNnPc2d1	výměnné
paměťových	paměťový	k2eAgNnPc2d1	paměťové
médií	médium	k1gNnPc2	médium
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
velkokapacitního	velkokapacitní	k2eAgNnSc2d1	velkokapacitní
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
drahého	drahý	k2eAgInSc2d1	drahý
magnetooptického	magnetooptický	k2eAgInSc2d1	magnetooptický
disku	disk	k1gInSc2	disk
byla	být	k5eAaImAgFnS	být
v	v	k7c4	v
NeXTstation	NeXTstation	k1gInSc4	NeXTstation
použita	použit	k2eAgFnSc1d1	použita
disketová	disketový	k2eAgFnSc1d1	disketová
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgInPc1d2	pozdější
modely	model	k1gInPc1	model
NeXTstation	NeXTstation	k1gInSc4	NeXTstation
vybaveny	vybavit	k5eAaPmNgInP	vybavit
jednotkami	jednotka	k1gFnPc7	jednotka
CD-ROM	CD-ROM	k1gFnSc7	CD-ROM
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
přenosy	přenos	k1gInPc1	přenos
dat	datum	k1gNnPc2	datum
budou	být	k5eAaImBp3nP	být
probíhat	probíhat	k5eAaImF	probíhat
především	především	k9	především
po	po	k7c6	po
síti	síť	k1gFnSc6	síť
<g/>
.	.	kIx.	.
</s>
