<s>
Radium	radium	k1gNnSc1	radium
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Ra	ra	k0	ra
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Radium	radium	k1gNnSc1	radium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
6	[number]	k4	6
<g/>
.	.	kIx.	.
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
kovů	kov	k1gInPc2	kov
alkalických	alkalický	k2eAgFnPc2d1	alkalická
zemin	zemina	k1gFnPc2	zemina
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
radioaktivní	radioaktivní	k2eAgInSc1d1	radioaktivní
prvek	prvek	k1gInSc1	prvek
vznikající	vznikající	k2eAgInSc1d1	vznikající
v	v	k7c6	v
rozpadových	rozpadový	k2eAgFnPc6d1	rozpadová
řadách	řada	k1gFnPc6	řada
uranu	uran	k1gInSc2	uran
a	a	k8xC	a
thoria	thorium	k1gNnSc2	thorium
<g/>
.	.	kIx.	.
</s>
