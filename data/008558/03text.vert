<p>
<s>
Radium	radium	k1gNnSc1	radium
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Ra	ra	k0	ra
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Radium	radium	k1gNnSc1	radium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
6	[number]	k4	6
<g/>
.	.	kIx.	.
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
kovů	kov	k1gInPc2	kov
alkalických	alkalický	k2eAgFnPc2d1	alkalická
zemin	zemina	k1gFnPc2	zemina
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
radioaktivní	radioaktivní	k2eAgInSc1d1	radioaktivní
prvek	prvek	k1gInSc1	prvek
vznikající	vznikající	k2eAgInSc1d1	vznikající
v	v	k7c6	v
rozpadových	rozpadový	k2eAgFnPc6d1	rozpadová
řadách	řada	k1gFnPc6	řada
uranu	uran	k1gInSc2	uran
a	a	k8xC	a
thoria	thorium	k1gNnSc2	thorium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgFnPc1d1	základní
fyzikálně-chemické	fyzikálněhemický	k2eAgFnPc1d1	fyzikálně-chemická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Radium	radium	k1gNnSc1	radium
je	být	k5eAaImIp3nS	být
mimořádně	mimořádně	k6eAd1	mimořádně
silný	silný	k2eAgInSc1d1	silný
radioaktivní	radioaktivní	k2eAgInSc1d1	radioaktivní
zářič	zářič	k1gInSc1	zářič
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
jako	jako	k9	jako
produkt	produkt	k1gInSc1	produkt
thoriové	thoriový	k2eAgFnSc2d1	thoriová
i	i	k8xC	i
uranové	uranový	k2eAgFnSc2d1	uranová
rozpadové	rozpadový	k2eAgFnSc2d1	rozpadová
řady	řada	k1gFnSc2	řada
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
radioaktivně	radioaktivně	k6eAd1	radioaktivně
přeměňuje	přeměňovat	k5eAaImIp3nS	přeměňovat
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
izotopy	izotop	k1gInPc1	izotop
radia	radio	k1gNnSc2	radio
vyzařují	vyzařovat	k5eAaImIp3nP	vyzařovat
všechny	všechen	k3xTgInPc1	všechen
druhy	druh	k1gInPc1	druh
radioaktivního	radioaktivní	k2eAgNnSc2d1	radioaktivní
záření	záření	k1gNnSc2	záření
–	–	k?	–
paprsky	paprsek	k1gInPc1	paprsek
alfa	alfa	k1gNnSc1	alfa
<g/>
,	,	kIx,	,
beta	beta	k1gNnSc1	beta
i	i	k8xC	i
gama	gama	k1gNnSc1	gama
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
čistém	čistý	k2eAgInSc6d1	čistý
stavu	stav	k1gInSc6	stav
je	být	k5eAaImIp3nS	být
radium	radium	k1gNnSc4	radium
bílý	bílý	k1gMnSc1	bílý
<g/>
,	,	kIx,	,
těžký	těžký	k2eAgMnSc1d1	těžký
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
reaktivní	reaktivní	k2eAgInSc1d1	reaktivní
kov	kov	k1gInSc1	kov
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgFnPc7	svůj
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
velmi	velmi	k6eAd1	velmi
podobá	podobat	k5eAaImIp3nS	podobat
alkalickým	alkalický	k2eAgInPc3d1	alkalický
kovům	kov	k1gInPc3	kov
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejreaktivnější	reaktivní	k2eAgNnSc1d3	reaktivní
z	z	k7c2	z
kovů	kov	k1gInPc2	kov
alkalických	alkalický	k2eAgFnPc2d1	alkalická
zemin	zemina	k1gFnPc2	zemina
a	a	k8xC	a
reaktivitou	reaktivita	k1gFnSc7	reaktivita
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
podobá	podobat	k5eAaImIp3nS	podobat
alkalickým	alkalický	k2eAgInPc3d1	alkalický
kovům	kov	k1gInPc3	kov
<g/>
.	.	kIx.	.
</s>
<s>
Radium	radium	k1gNnSc1	radium
samotné	samotný	k2eAgNnSc1d1	samotné
nevyzařuje	vyzařovat	k5eNaImIp3nS	vyzařovat
žádné	žádný	k3yNgNnSc1	žádný
viditelné	viditelný	k2eAgNnSc1d1	viditelné
světlo	světlo	k1gNnSc1	světlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
částice	částice	k1gFnPc1	částice
produkované	produkovaný	k2eAgFnPc1d1	produkovaná
jeho	jeho	k3xOp3gInSc7	jeho
radioaktivním	radioaktivní	k2eAgInSc7d1	radioaktivní
rozpadem	rozpad	k1gInSc7	rozpad
mohou	moct	k5eAaImIp3nP	moct
excitovat	excitovat	k5eAaBmF	excitovat
atomy	atom	k1gInPc4	atom
jiných	jiný	k2eAgInPc2d1	jiný
prvků	prvek	k1gInPc2	prvek
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Atomy	atom	k1gInPc1	atom
dusíku	dusík	k1gInSc2	dusík
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
tak	tak	k9	tak
emitují	emitovat	k5eAaBmIp3nP	emitovat
modré	modrý	k2eAgNnSc1d1	modré
světlo	světlo	k1gNnSc1	světlo
<g/>
,	,	kIx,	,
sulfid	sulfid	k1gInSc1	sulfid
zinečnatý	zinečnatý	k2eAgInSc1d1	zinečnatý
září	zářit	k5eAaImIp3nS	zářit
různými	různý	k2eAgFnPc7d1	různá
barvami	barva	k1gFnPc7	barva
podle	podle	k7c2	podle
obsaženého	obsažený	k2eAgInSc2d1	obsažený
aktivátoru	aktivátor	k1gInSc2	aktivátor
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
zeleně	zeleně	k6eAd1	zeleně
<g/>
.	.	kIx.	.
</s>
<s>
Reaktivita	reaktivita	k1gFnSc1	reaktivita
radia	radio	k1gNnSc2	radio
je	být	k5eAaImIp3nS	být
natolik	natolik	k6eAd1	natolik
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
že	že	k8xS	že
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
uchováváno	uchovávat	k5eAaImNgNnS	uchovávat
pouze	pouze	k6eAd1	pouze
pod	pod	k7c7	pod
vrstvou	vrstva	k1gFnSc7	vrstva
alifatických	alifatický	k2eAgInPc2d1	alifatický
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
petrolej	petrolej	k1gInSc1	petrolej
či	či	k8xC	či
nafta	nafta	k1gFnSc1	nafta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgInPc7	jenž
nereaguje	reagovat	k5eNaBmIp3nS	reagovat
<g/>
.	.	kIx.	.
</s>
<s>
Soli	sůl	k1gFnPc1	sůl
radia	radio	k1gNnSc2	radio
barví	barvit	k5eAaImIp3nP	barvit
plamen	plamen	k1gInSc4	plamen
sytě	sytě	k6eAd1	sytě
červeně	červeně	k6eAd1	červeně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Radium	radium	k1gNnSc1	radium
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
reaktivní	reaktivní	k2eAgInSc1d1	reaktivní
a	a	k8xC	a
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
pouze	pouze	k6eAd1	pouze
radnaté	radnatý	k2eAgFnPc1d1	radnatý
sloučeniny	sloučenina	k1gFnPc1	sloučenina
Ra	ra	k0	ra
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
Radium	radium	k1gNnSc1	radium
reaguje	reagovat	k5eAaBmIp3nS	reagovat
za	za	k7c4	za
pokojové	pokojový	k2eAgFnPc4d1	pokojová
teploty	teplota	k1gFnPc4	teplota
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
i	i	k8xC	i
kyslíkem	kyslík	k1gInSc7	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
černá	černá	k1gFnSc1	černá
(	(	kIx(	(
<g/>
patrně	patrně	k6eAd1	patrně
tvorbou	tvorba	k1gFnSc7	tvorba
nitridu	nitrid	k1gInSc2	nitrid
<g/>
)	)	kIx)	)
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
vrstvou	vrstva	k1gFnSc7	vrstva
oxidu	oxid	k1gInSc2	oxid
radnatého	radnatý	k2eAgInSc2d1	radnatý
a	a	k8xC	a
peroxidu	peroxid	k1gInSc2	peroxid
<g/>
,	,	kIx,	,
radium	radium	k1gNnSc1	radium
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
schopno	schopen	k2eAgNnSc1d1	schopno
samovolného	samovolný	k2eAgNnSc2d1	samovolné
vznícení	vznícení	k1gNnSc2	vznícení
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
mírně	mírně	k6eAd1	mírně
reaktivnější	reaktivní	k2eAgNnSc1d2	reaktivnější
než	než	k8xS	než
baryum	baryum	k1gNnSc1	baryum
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zahřátí	zahřátí	k1gNnSc6	zahřátí
se	se	k3xPyFc4	se
snadno	snadno	k6eAd1	snadno
slučuje	slučovat	k5eAaImIp3nS	slučovat
s	s	k7c7	s
dusíkem	dusík	k1gInSc7	dusík
na	na	k7c4	na
nitrid	nitrid	k1gInSc4	nitrid
radnatý	radnatý	k2eAgInSc4d1	radnatý
Ra	ra	k0	ra
<g/>
3	[number]	k4	3
<g/>
N	N	kA	N
<g/>
2	[number]	k4	2
a	a	k8xC	a
s	s	k7c7	s
vodíkem	vodík	k1gInSc7	vodík
na	na	k7c4	na
hydrid	hydrid	k1gInSc4	hydrid
radnatý	radnatý	k2eAgInSc4d1	radnatý
RaH	RaH	k1gFnSc7	RaH
<g/>
2	[number]	k4	2
a	a	k8xC	a
i	i	k9	i
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
ostatními	ostatní	k2eAgInPc7d1	ostatní
prvky	prvek	k1gInPc7	prvek
tvoří	tvořit	k5eAaImIp3nP	tvořit
za	za	k7c2	za
vyšších	vysoký	k2eAgFnPc2d2	vyšší
teplot	teplota	k1gFnPc2	teplota
sloučeniny	sloučenina	k1gFnSc2	sloučenina
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
ale	ale	k9	ale
díky	díky	k7c3	díky
vysoké	vysoký	k2eAgFnSc3d1	vysoká
radioaktivitě	radioaktivita	k1gFnSc3	radioaktivita
radia	radio	k1gNnSc2	radio
nemají	mít	k5eNaImIp3nP	mít
téměř	téměř	k6eAd1	téměř
žádný	žádný	k3yNgInSc4	žádný
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historický	historický	k2eAgInSc1d1	historický
vývoj	vývoj	k1gInSc1	vývoj
==	==	k?	==
</s>
</p>
<p>
<s>
Radium	radium	k1gNnSc1	radium
bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
Marií	Maria	k1gFnSc7	Maria
Curie-Skłodowskou	Curie-Skłodowský	k2eAgFnSc7d1	Curie-Skłodowský
<g/>
,	,	kIx,	,
jejím	její	k3xOp3gMnSc7	její
manželem	manžel	k1gMnSc7	manžel
Pierem	pier	k1gInSc7	pier
a	a	k8xC	a
Gustavem	Gustav	k1gMnSc7	Gustav
Bémontem	Bémont	k1gMnSc7	Bémont
v	v	k7c6	v
jáchymovském	jáchymovský	k2eAgInSc6d1	jáchymovský
smolinci	smolinec	k1gInSc6	smolinec
UO	UO	kA	UO
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
pouze	pouze	k6eAd1	pouze
odpad	odpad	k1gInSc4	odpad
při	při	k7c6	při
těžbě	těžba	k1gFnSc6	těžba
galenitu	galenit	k1gInSc2	galenit
PbS	PbS	k1gFnSc2	PbS
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
rudy	ruda	k1gFnSc2	ruda
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
po	po	k7c6	po
mnohaletém	mnohaletý	k2eAgNnSc6d1	mnohaleté
úsilí	úsilí	k1gNnSc6	úsilí
izolovat	izolovat	k5eAaBmF	izolovat
chlorid	chlorid	k1gInSc1	chlorid
radnatý	radnatý	k2eAgInSc1d1	radnatý
RaCl	RaCl	k1gInSc1	RaCl
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
izolaci	izolace	k1gFnSc6	izolace
1	[number]	k4	1
gramu	gram	k1gInSc2	gram
chloridu	chlorid	k1gInSc2	chlorid
radnatého	radnatý	k2eAgInSc2d1	radnatý
spotřebovali	spotřebovat	k5eAaPmAgMnP	spotřebovat
10	[number]	k4	10
tun	tuna	k1gFnPc2	tuna
(	(	kIx(	(
<g/>
10	[number]	k4	10
000	[number]	k4	000
000	[number]	k4	000
gramů	gram	k1gInPc2	gram
<g/>
)	)	kIx)	)
smolince	smolinec	k1gInSc2	smolinec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Objev	objev	k1gInSc1	objev
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c4	na
pozorování	pozorování	k1gNnSc4	pozorování
<g/>
,	,	kIx,	,
že	že	k8xS	že
intenzita	intenzita	k1gFnSc1	intenzita
radioaktivního	radioaktivní	k2eAgNnSc2d1	radioaktivní
záření	záření	k1gNnSc2	záření
je	být	k5eAaImIp3nS	být
úměrná	úměrná	k1gFnSc1	úměrná
obsahu	obsah	k1gInSc2	obsah
uranu	uran	k1gInSc2	uran
v	v	k7c6	v
rudě	ruda	k1gFnSc6	ruda
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Curie-Skłodowská	Curie-Skłodowská	k1gFnSc1	Curie-Skłodowská
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
nerosty	nerost	k1gInPc7	nerost
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgNnPc6	který
je	být	k5eAaImIp3nS	být
radioaktivní	radioaktivní	k2eAgNnSc1d1	radioaktivní
záření	záření	k1gNnSc1	záření
silnější	silný	k2eAgNnSc1d2	silnější
<g/>
,	,	kIx,	,
než	než	k8xS	než
by	by	kYmCp3nS	by
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
množství	množství	k1gNnSc1	množství
záření	záření	k1gNnSc2	záření
uranu	uran	k1gInSc2	uran
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
úsudku	úsudek	k1gInSc2	úsudek
dospěla	dochvít	k5eAaPmAgFnS	dochvít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
musí	muset	k5eAaImIp3nS	muset
existovat	existovat	k5eAaImF	existovat
i	i	k9	i
další	další	k2eAgFnPc4d1	další
složky	složka	k1gFnPc4	složka
uranových	uranový	k2eAgFnPc2d1	uranová
rud	ruda	k1gFnPc2	ruda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
silnější	silný	k2eAgNnSc4d2	silnější
záření	záření	k1gNnSc4	záření
než	než	k8xS	než
samotný	samotný	k2eAgInSc4d1	samotný
uran	uran	k1gInSc4	uran
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
uranových	uranový	k2eAgFnPc2d1	uranová
rud	ruda	k1gFnPc2	ruda
izolovala	izolovat	k5eAaBmAgFnS	izolovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
přes	přes	k7c4	přes
amalgám	amalgám	k1gInSc4	amalgám
nepatrné	nepatrný	k2eAgNnSc4d1	nepatrný
množství	množství	k1gNnSc4	množství
čistého	čistý	k2eAgNnSc2d1	čisté
polonia	polonium	k1gNnSc2	polonium
a	a	k8xC	a
o	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
i	i	k9	i
radium	radium	k1gNnSc1	radium
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
André	André	k1gMnSc7	André
Debiernem	Debiern	k1gInSc7	Debiern
<g/>
.	.	kIx.	.
</s>
<s>
Radium	radium	k1gNnSc1	radium
dostalo	dostat	k5eAaPmAgNnS	dostat
název	název	k1gInSc4	název
z	z	k7c2	z
latinského	latinský	k2eAgMnSc2d1	latinský
radius	radius	k1gInSc4	radius
–	–	k?	–
paprsek	paprsek	k1gInSc1	paprsek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
Curie-Skłodowská	Curie-Skłodowská	k1gFnSc1	Curie-Skłodowská
kvůli	kvůli	k7c3	kvůli
dlouhodobému	dlouhodobý	k2eAgInSc3d1	dlouhodobý
styku	styk	k1gInSc3	styk
s	s	k7c7	s
radioaktivními	radioaktivní	k2eAgInPc7d1	radioaktivní
prvky	prvek	k1gInPc7	prvek
zemřela	zemřít	k5eAaPmAgFnS	zemřít
na	na	k7c6	na
anémii	anémie	k1gFnSc6	anémie
roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
==	==	k?	==
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
všechny	všechen	k3xTgInPc1	všechen
izotopy	izotop	k1gInPc1	izotop
radia	radio	k1gNnSc2	radio
podléhají	podléhat	k5eAaImIp3nP	podléhat
poměrně	poměrně	k6eAd1	poměrně
rychle	rychle	k6eAd1	rychle
dalšímu	další	k2eAgInSc3d1	další
radioaktivnímu	radioaktivní	k2eAgInSc3d1	radioaktivní
rozpadu	rozpad	k1gInSc3	rozpad
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
obsah	obsah	k1gInSc1	obsah
radia	radio	k1gNnSc2	radio
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
velmi	velmi	k6eAd1	velmi
nízký	nízký	k2eAgInSc1d1	nízký
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
lokality	lokalita	k1gFnPc1	lokalita
s	s	k7c7	s
vyšším	vysoký	k2eAgInSc7d2	vyšší
obsahem	obsah	k1gInSc7	obsah
radia	radio	k1gNnSc2	radio
jsou	být	k5eAaImIp3nP	být
přitom	přitom	k6eAd1	přitom
spojeny	spojit	k5eAaPmNgFnP	spojit
se	s	k7c7	s
zvýšeným	zvýšený	k2eAgInSc7d1	zvýšený
výskytem	výskyt	k1gInSc7	výskyt
uranu	uran	k1gInSc2	uran
a	a	k8xC	a
thoria	thorium	k1gNnSc2	thorium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
rudách	ruda	k1gFnPc6	ruda
se	se	k3xPyFc4	se
radium	radium	k1gNnSc1	radium
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
téměř	téměř	k6eAd1	téměř
neměnném	měnný	k2eNgInSc6d1	neměnný
poměru	poměr	k1gInSc6	poměr
radia	radio	k1gNnSc2	radio
<g/>
:	:	kIx,	:
uranu	uran	k1gInSc2	uran
1	[number]	k4	1
:	:	kIx,	:
3	[number]	k4	3
000	[number]	k4	000
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
radia	radio	k1gNnSc2	radio
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
6	[number]	k4	6
ppm	ppm	k?	ppm
(	(	kIx(	(
<g/>
parts	partsa	k1gFnPc2	partsa
per	pero	k1gNnPc2	pero
milion	milion	k4xCgInSc4	milion
=	=	kIx~	=
počet	počet	k1gInSc4	počet
částic	částice	k1gFnPc2	částice
na	na	k7c4	na
1	[number]	k4	1
milion	milion	k4xCgInSc4	milion
částic	částice	k1gFnPc2	částice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
oblasti	oblast	k1gFnSc2	oblast
Jáchymova	Jáchymov	k1gInSc2	Jáchymov
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgFnPc1d1	známá
lokality	lokalita	k1gFnPc1	lokalita
se	s	k7c7	s
zvýšeným	zvýšený	k2eAgInSc7d1	zvýšený
obsahem	obsah	k1gInSc7	obsah
radia	radio	k1gNnSc2	radio
např.	např.	kA	např.
v	v	k7c6	v
Coloradu	Colorado	k1gNnSc6	Colorado
(	(	kIx(	(
<g/>
karnotitové	karnotitový	k2eAgInPc1d1	karnotitový
písky	písek	k1gInPc1	písek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
africkém	africký	k2eAgNnSc6d1	africké
Kongu	Kongo	k1gNnSc6	Kongo
a	a	k8xC	a
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Velkých	velký	k2eAgNnPc2d1	velké
jezer	jezero	k1gNnPc2	jezero
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
minerál	minerál	k1gInSc1	minerál
uranu	uran	k1gInSc2	uran
–	–	k?	–
uraninit	uraninit	k1gInSc1	uraninit
UO2	UO2	k1gFnSc1	UO2
–	–	k?	–
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
průměrně	průměrně	k6eAd1	průměrně
v	v	k7c6	v
1	[number]	k4	1
tuně	tuna	k1gFnSc6	tuna
0,17	[number]	k4	0,17
g	g	kA	g
radia	radio	k1gNnSc2	radio
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
méně	málo	k6eAd2	málo
významným	významný	k2eAgMnPc3d1	významný
minerálům	minerál	k1gInPc3	minerál
uranu	uran	k1gInSc2	uran
a	a	k8xC	a
radia	radio	k1gNnSc2	radio
patří	patřit	k5eAaImIp3nS	patřit
karnotit	karnotit	k1gInSc1	karnotit
K	k	k7c3	k
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
UO	UO	kA	UO
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
VO	VO	k?	VO
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
·	·	k?	·
<g/>
3	[number]	k4	3
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
autunit	autunit	k1gInSc1	autunit
Ca	ca	kA	ca
<g/>
(	(	kIx(	(
<g/>
UO	UO	kA	UO
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
PO	Po	kA	Po
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
·	·	k?	·
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O.	O.	kA	O.
</s>
</p>
<p>
<s>
==	==	k?	==
Izotopy	izotop	k1gInPc1	izotop
radia	radio	k1gNnSc2	radio
a	a	k8xC	a
radonu	radon	k1gInSc2	radon
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
34	[number]	k4	34
izotopů	izotop	k1gInPc2	izotop
radia	radio	k1gNnSc2	radio
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgInPc1	všechen
jsou	být	k5eAaImIp3nP	být
nestabilní	stabilní	k2eNgInPc1d1	nestabilní
a	a	k8xC	a
podléhají	podléhat	k5eAaImIp3nP	podléhat
další	další	k2eAgFnSc3d1	další
radioaktivní	radioaktivní	k2eAgFnSc3d1	radioaktivní
přeměně	přeměna	k1gFnSc3	přeměna
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějšími	významný	k2eAgMnPc7d3	nejvýznamnější
jsou	být	k5eAaImIp3nP	být
izotopy	izotop	k1gInPc1	izotop
226	[number]	k4	226
<g/>
Ra	ra	k0	ra
s	s	k7c7	s
poločasem	poločas	k1gInSc7	poločas
přeměny	přeměna	k1gFnSc2	přeměna
1	[number]	k4	1
600	[number]	k4	600
let	léto	k1gNnPc2	léto
a	a	k8xC	a
228	[number]	k4	228
<g/>
Ra	ra	k0	ra
s	s	k7c7	s
poločasem	poločas	k1gInSc7	poločas
5,75	[number]	k4	5,75
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
radioaktivní	radioaktivní	k2eAgFnSc6d1	radioaktivní
přeměně	přeměna	k1gFnSc6	přeměna
vyzařují	vyzařovat	k5eAaImIp3nP	vyzařovat
atomy	atom	k1gInPc7	atom
radia	radio	k1gNnSc2	radio
intenzivní	intenzivní	k2eAgNnSc1d1	intenzivní
alfa	alfa	k1gNnSc1	alfa
<g/>
,	,	kIx,	,
beta	beta	k1gNnSc1	beta
i	i	k8xC	i
gama	gama	k1gNnSc1	gama
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
však	však	k9	však
značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
prekurzory	prekurzora	k1gFnSc2	prekurzora
dalšího	další	k2eAgInSc2d1	další
nebezpečného	bezpečný	k2eNgInSc2d1	nebezpečný
prvku	prvek	k1gInSc2	prvek
–	–	k?	–
radonu	radon	k1gInSc2	radon
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
plynný	plynný	k2eAgInSc1d1	plynný
prvek	prvek	k1gInSc1	prvek
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
šířit	šířit	k5eAaImF	šířit
vzduchem	vzduch	k1gInSc7	vzduch
a	a	k8xC	a
při	při	k7c6	při
dlouhodobém	dlouhodobý	k2eAgInSc6d1	dlouhodobý
styku	styk	k1gInSc6	styk
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
riziko	riziko	k1gNnSc1	riziko
vzniku	vznik	k1gInSc2	vznik
plicní	plicní	k2eAgFnSc2d1	plicní
rakoviny	rakovina	k1gFnSc2	rakovina
u	u	k7c2	u
exponovaných	exponovaný	k2eAgFnPc2d1	exponovaná
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
obytných	obytný	k2eAgFnPc2d1	obytná
budov	budova	k1gFnPc2	budova
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
se	s	k7c7	s
zvýšeným	zvýšený	k2eAgInSc7d1	zvýšený
výskytem	výskyt	k1gInSc7	výskyt
uranu	uran	k1gInSc2	uran
a	a	k8xC	a
thoria	thorium	k1gNnSc2	thorium
v	v	k7c6	v
geologickém	geologický	k2eAgNnSc6d1	geologické
podloží	podloží	k1gNnSc6	podloží
dobře	dobře	k6eAd1	dobře
izolovat	izolovat	k5eAaBmF	izolovat
základy	základ	k1gInPc4	základ
stavby	stavba	k1gFnSc2	stavba
a	a	k8xC	a
zamezit	zamezit	k5eAaPmF	zamezit
tak	tak	k6eAd1	tak
možnému	možný	k2eAgNnSc3d1	možné
pronikání	pronikání	k1gNnSc3	pronikání
podzemního	podzemní	k2eAgInSc2d1	podzemní
radonu	radon	k1gInSc2	radon
do	do	k7c2	do
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příprava	příprava	k1gFnSc1	příprava
<g/>
,	,	kIx,	,
využití	využití	k1gNnSc2	využití
a	a	k8xC	a
rizika	riziko	k1gNnSc2	riziko
==	==	k?	==
</s>
</p>
<p>
<s>
Elementární	elementární	k2eAgNnSc1d1	elementární
radium	radium	k1gNnSc1	radium
lze	lze	k6eAd1	lze
připravit	připravit	k5eAaPmF	připravit
elektrolytickým	elektrolytický	k2eAgInSc7d1	elektrolytický
rozkladem	rozklad	k1gInSc7	rozklad
chloridu	chlorid	k1gInSc2	chlorid
radnatého	radnatý	k2eAgInSc2d1	radnatý
<g/>
.	.	kIx.	.
</s>
<s>
Průmyslově	průmyslově	k6eAd1	průmyslově
se	se	k3xPyFc4	se
vyrobí	vyrobit	k5eAaPmIp3nS	vyrobit
několik	několik	k4yIc4	několik
gramů	gram	k1gInPc2	gram
radia	radio	k1gNnSc2	radio
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
stačí	stačit	k5eAaBmIp3nS	stačit
pokrýt	pokrýt	k5eAaPmF	pokrýt
celosvětovou	celosvětový	k2eAgFnSc4d1	celosvětová
potřebu	potřeba	k1gFnSc4	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Produkce	produkce	k1gFnSc1	produkce
sloučenin	sloučenina	k1gFnPc2	sloučenina
radia	radio	k1gNnSc2	radio
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
100	[number]	k4	100
gramů	gram	k1gInPc2	gram
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Hlavními	hlavní	k2eAgMnPc7d1	hlavní
dodavateli	dodavatel	k1gMnPc7	dodavatel
rud	ruda	k1gFnPc2	ruda
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
radia	radio	k1gNnSc2	radio
jsou	být	k5eAaImIp3nP	být
Belgie	Belgie	k1gFnSc1	Belgie
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
a	a	k8xC	a
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dřívějších	dřívější	k2eAgFnPc6d1	dřívější
dobách	doba	k1gFnPc6	doba
se	se	k3xPyFc4	se
při	při	k7c6	při
radioterapeutické	radioterapeutický	k2eAgFnSc6d1	Radioterapeutická
léčbě	léčba	k1gFnSc6	léčba
rakovinných	rakovinný	k2eAgInPc2d1	rakovinný
nádorů	nádor	k1gInPc2	nádor
vpravovala	vpravovat	k5eAaImAgFnS	vpravovat
do	do	k7c2	do
nádoru	nádor	k1gInSc2	nádor
malá	malý	k2eAgNnPc1d1	malé
množství	množství	k1gNnPc1	množství
solí	solit	k5eAaImIp3nP	solit
radia	radio	k1gNnPc4	radio
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
uzavřených	uzavřený	k2eAgInPc2d1	uzavřený
zářičů	zářič	k1gInPc2	zářič
tvaru	tvar	k1gInSc2	tvar
jehly	jehla	k1gFnSc2	jehla
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
rakovinné	rakovinný	k2eAgFnPc1d1	rakovinná
buňky	buňka	k1gFnPc1	buňka
jsou	být	k5eAaImIp3nP	být
přednostně	přednostně	k6eAd1	přednostně
likvidovány	likvidovat	k5eAaBmNgFnP	likvidovat
radioaktivním	radioaktivní	k2eAgNnSc7d1	radioaktivní
zářením	záření	k1gNnSc7	záření
<g/>
,	,	kIx,	,
vedl	vést	k5eAaImAgMnS	vést
tento	tento	k3xDgInSc4	tento
postup	postup	k1gInSc4	postup
k	k	k7c3	k
zahubení	zahubení	k1gNnSc3	zahubení
většiny	většina	k1gFnSc2	většina
rakovinou	rakovina	k1gFnSc7	rakovina
napadených	napadený	k2eAgFnPc2d1	napadená
buněk	buňka	k1gFnPc2	buňka
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
léčbu	léčba	k1gFnSc4	léčba
používá	používat	k5eAaImIp3nS	používat
spíše	spíše	k9	spíše
uměle	uměle	k6eAd1	uměle
připravených	připravený	k2eAgInPc2d1	připravený
radioizotopů	radioizotop	k1gInPc2	radioizotop
jako	jako	k9	jako
60	[number]	k4	60
<g/>
Co	co	k9	co
a	a	k9	a
137	[number]	k4	137
<g/>
Cs	Cs	k1gFnPc2	Cs
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
případ	případ	k1gInSc1	případ
smrtelného	smrtelný	k2eAgNnSc2d1	smrtelné
rakovinného	rakovinný	k2eAgNnSc2d1	rakovinné
onemocnění	onemocnění	k1gNnSc2	onemocnění
stovek	stovka	k1gFnPc2	stovka
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
pracovaly	pracovat	k5eAaImAgFnP	pracovat
v	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
vyrábějící	vyrábějící	k2eAgFnSc2d1	vyrábějící
náramkové	náramkový	k2eAgFnSc2d1	náramková
hodinky	hodinka	k1gFnSc2	hodinka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
ciferníky	ciferník	k1gInPc4	ciferník
těchto	tento	k3xDgFnPc2	tento
hodinek	hodinka	k1gFnPc2	hodinka
se	se	k3xPyFc4	se
tenkým	tenký	k2eAgInSc7d1	tenký
štětcem	štětec	k1gInSc7	štětec
nanášelo	nanášet	k5eAaImAgNnS	nanášet
barvivo	barvivo	k1gNnSc1	barvivo
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
radia	radio	k1gNnSc2	radio
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
pak	pak	k6eAd1	pak
ve	v	k7c6	v
tmě	tma	k1gFnSc6	tma
světélkovalo	světélkovat	k5eAaImAgNnS	světélkovat
(	(	kIx(	(
<g/>
luminiscence	luminiscence	k1gFnSc1	luminiscence
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dělnice	dělnice	k1gFnPc1	dělnice
občas	občas	k6eAd1	občas
olízly	olíznout	k5eAaPmAgFnP	olíznout
špičku	špička	k1gFnSc4	špička
štětce	štětec	k1gInSc2	štětec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
udržely	udržet	k5eAaPmAgFnP	udržet
dokonale	dokonale	k6eAd1	dokonale
ostrou	ostrý	k2eAgFnSc4d1	ostrá
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
hrtanu	hrtan	k1gInSc2	hrtan
<g/>
,	,	kIx,	,
štítné	štítný	k2eAgFnPc4d1	štítná
žlázy	žláza	k1gFnPc4	žláza
a	a	k8xC	a
nádory	nádor	k1gInPc4	nádor
v	v	k7c6	v
ústní	ústní	k2eAgFnSc6d1	ústní
dutině	dutina	k1gFnSc6	dutina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sloučeniny	sloučenina	k1gFnPc1	sloučenina
==	==	k?	==
</s>
</p>
<p>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
sloučeniny	sloučenina	k1gFnPc1	sloučenina
radia	radio	k1gNnSc2	radio
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
podobají	podobat	k5eAaImIp3nP	podobat
sloučeninám	sloučenina	k1gFnPc3	sloučenina
barya	baryum	k1gNnSc2	baryum
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
soli	sůl	k1gFnPc4	sůl
radia	radio	k1gNnSc2	radio
jsou	být	k5eAaImIp3nP	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
rozpustnější	rozpustný	k2eAgFnPc1d2	rozpustnější
než	než	k8xS	než
barnaté	barnatý	k2eAgFnPc1d1	barnatý
a	a	k8xC	a
všechny	všechen	k3xTgFnPc1	všechen
soli	sůl	k1gFnPc1	sůl
radia	radio	k1gNnSc2	radio
jsou	být	k5eAaImIp3nP	být
silně	silně	k6eAd1	silně
radioaktivní	radioaktivní	k2eAgInPc1d1	radioaktivní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oxid	oxid	k1gInSc1	oxid
radnatý	radnatý	k2eAgMnSc1d1	radnatý
RaO	RaO	k1gMnSc1	RaO
</s>
</p>
<p>
<s>
Hydroxid	hydroxid	k1gInSc1	hydroxid
radnatý	radnatý	k2eAgInSc1d1	radnatý
Ra	ra	k0	ra
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
</s>
</p>
<p>
<s>
Fluorid	fluorid	k1gInSc1	fluorid
radnatý	radnatý	k2eAgInSc1d1	radnatý
RaF	raf	k0	raf
<g/>
2	[number]	k4	2
</s>
</p>
<p>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
radnatý	radnatý	k2eAgInSc4d1	radnatý
RaCl	RaCl	k1gInSc4	RaCl
<g/>
2	[number]	k4	2
</s>
</p>
<p>
<s>
Bromid	bromid	k1gInSc1	bromid
radnatý	radnatý	k2eAgInSc4d1	radnatý
RaBr	RaBr	k1gInSc4	RaBr
<g/>
2	[number]	k4	2
</s>
</p>
<p>
<s>
Jodid	jodid	k1gInSc4	jodid
radnatý	radnatý	k2eAgInSc4d1	radnatý
RaI	RaI	k1gFnSc7	RaI
<g/>
2	[number]	k4	2
</s>
</p>
<p>
<s>
Nitrid	nitrid	k1gInSc1	nitrid
radnatý	radnatý	k2eAgInSc1d1	radnatý
Ra	ra	k0	ra
<g/>
3	[number]	k4	3
<g/>
N	N	kA	N
<g/>
2	[number]	k4	2
</s>
</p>
<p>
<s>
Dusičnan	dusičnan	k1gInSc1	dusičnan
radnatý	radnatý	k2eAgInSc1d1	radnatý
Ra	ra	k0	ra
<g/>
(	(	kIx(	(
<g/>
NO	no	k9	no
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
</s>
</p>
<p>
<s>
Uhličitan	uhličitan	k1gInSc4	uhličitan
radnatý	radnatý	k2eAgInSc4d1	radnatý
RaCO	RaCO	k1gFnSc7	RaCO
<g/>
3	[number]	k4	3
</s>
</p>
<p>
<s>
Síran	síran	k1gInSc1	síran
radnatý	radnatý	k2eAgInSc1d1	radnatý
RaSO	rasa	k1gFnSc5	rasa
<g/>
4	[number]	k4	4
</s>
</p>
<p>
<s>
==	==	k?	==
Cena	cena	k1gFnSc1	cena
radia	radio	k1gNnSc2	radio
==	==	k?	==
</s>
</p>
<p>
<s>
Lékařské	lékařský	k2eAgNnSc4d1	lékařské
využívání	využívání	k1gNnSc4	využívání
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
obrovskému	obrovský	k2eAgInSc3d1	obrovský
růstu	růst	k1gInSc2	růst
cen	cena	k1gFnPc2	cena
radia	radio	k1gNnSc2	radio
<g/>
.	.	kIx.	.
</s>
<s>
Produkce	produkce	k1gFnSc1	produkce
pouhých	pouhý	k2eAgInPc2d1	pouhý
2	[number]	k4	2
g	g	kA	g
ročně	ročně	k6eAd1	ročně
stačila	stačit	k5eAaBmAgFnS	stačit
před	před	k7c7	před
1	[number]	k4	1
<g/>
.	.	kIx.	.
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
k	k	k7c3	k
prosperitě	prosperita	k1gFnSc3	prosperita
uranových	uranový	k2eAgInPc2d1	uranový
dolů	dol	k1gInPc2	dol
v	v	k7c6	v
Jáchymově	Jáchymov	k1gInSc6	Jáchymov
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nálezu	nález	k1gInSc6	nález
kargonitu	kargonit	k1gInSc2	kargonit
v	v	k7c6	v
Utahu	Utah	k1gInSc6	Utah
se	se	k3xPyFc4	se
světová	světový	k2eAgFnSc1d1	světová
výroba	výroba	k1gFnSc1	výroba
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
na	na	k7c4	na
10	[number]	k4	10
g.	g.	k?	g.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
věnovaly	věnovat	k5eAaImAgFnP	věnovat
americké	americký	k2eAgFnPc1d1	americká
ženy	žena	k1gFnPc1	žena
Marii	Maria	k1gFnSc4	Maria
Curie	curie	k1gNnPc2	curie
1	[number]	k4	1
g	g	kA	g
radia	radio	k1gNnSc2	radio
v	v	k7c6	v
ceně	cena	k1gFnSc6	cena
250	[number]	k4	250
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
byla	být	k5eAaImAgNnP	být
nalezena	nalézt	k5eAaBmNgNnP	nalézt
ložiska	ložisko	k1gNnPc1	ložisko
v	v	k7c6	v
Kongu	Kongo	k1gNnSc6	Kongo
a	a	k8xC	a
výroba	výroba	k1gFnSc1	výroba
stoupla	stoupnout	k5eAaPmAgFnS	stoupnout
během	během	k7c2	během
tří	tři	k4xCgNnPc2	tři
let	léto	k1gNnPc2	léto
na	na	k7c6	na
25	[number]	k4	25
g.	g.	k?	g.
Roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
už	už	k9	už
klesla	klesnout	k5eAaPmAgFnS	klesnout
cena	cena	k1gFnSc1	cena
gramu	gram	k1gInSc2	gram
radia	radio	k1gNnSc2	radio
na	na	k7c4	na
50	[number]	k4	50
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Objev	objev	k1gInSc1	objev
ložisek	ložisko	k1gNnPc2	ložisko
uranu	uran	k1gInSc2	uran
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
pak	pak	k6eAd1	pak
stlačil	stlačit	k5eAaPmAgInS	stlačit
cenu	cena	k1gFnSc4	cena
několikanásobně	několikanásobně	k6eAd1	několikanásobně
níže	nízce	k6eAd2	nízce
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
není	být	k5eNaImIp3nS	být
radium	radium	k1gNnSc1	radium
prakticky	prakticky	k6eAd1	prakticky
užíváno	užíván	k2eAgNnSc1d1	užíváno
a	a	k8xC	a
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
případné	případný	k2eAgFnSc6d1	případná
ceně	cena	k1gFnSc6	cena
lze	lze	k6eAd1	lze
tedy	tedy	k9	tedy
jen	jen	k9	jen
spekulovat	spekulovat	k5eAaImF	spekulovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
<g/>
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
</s>
</p>
<p>
<s>
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
<g/>
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
</s>
</p>
<p>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc4	vydání
1961	[number]	k4	1961
</s>
</p>
<p>
<s>
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwood	k1gInSc1	Greenwood
–	–	k?	–
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc2	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
</s>
</p>
<p>
<s>
VOHLÍDAL	VOHLÍDAL	kA	VOHLÍDAL
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
ŠTULÍK	štulík	k1gInSc1	štulík
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
;	;	kIx,	;
JULÁK	JULÁK	kA	JULÁK
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Chemické	chemický	k2eAgFnPc1d1	chemická
a	a	k8xC	a
analytické	analytický	k2eAgFnPc1d1	analytická
tabulky	tabulka	k1gFnPc1	tabulka
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7169	[number]	k4	7169
<g/>
-	-	kIx~	-
<g/>
855	[number]	k4	855
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Radiové	radiový	k2eAgFnPc1d1	radiová
dívky	dívka	k1gFnPc1	dívka
(	(	kIx(	(
<g/>
Radium	radium	k1gNnSc1	radium
Girls	girl	k1gFnPc2	girl
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
radium	radium	k1gNnSc4	radium
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
radium	radium	k1gNnSc4	radium
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
