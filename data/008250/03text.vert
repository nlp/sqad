<p>
<s>
Adradas	Adradas	k1gInSc1	Adradas
je	být	k5eAaImIp3nS	být
španělská	španělský	k2eAgFnSc1d1	španělská
obec	obec	k1gFnSc1	obec
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Soria	Sorium	k1gNnSc2	Sorium
v	v	k7c6	v
autonomním	autonomní	k2eAgNnSc6d1	autonomní
společenství	společenství	k1gNnSc6	společenství
Kastilie	Kastilie	k1gFnSc2	Kastilie
a	a	k8xC	a
León	Leóna	k1gFnPc2	Leóna
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
1	[number]	k4	1
051	[number]	k4	051
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obec	obec	k1gFnSc1	obec
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
z	z	k7c2	z
Torralby	Torralba	k1gFnSc2	Torralba
do	do	k7c2	do
Sorie	Sorie	k1gFnSc2	Sorie
<g/>
,	,	kIx,	,
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
provincie	provincie	k1gFnSc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgNnSc1d1	místní
nádraží	nádraží	k1gNnSc1	nádraží
bylo	být	k5eAaImAgNnS	být
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1892	[number]	k4	1892
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Adradas	Adradasa	k1gFnPc2	Adradasa
na	na	k7c6	na
španělské	španělský	k2eAgFnSc6d1	španělská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
