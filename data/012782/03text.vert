<p>
<s>
Třešně	třešeň	k1gFnPc1	třešeň
jsou	být	k5eAaImIp3nP	být
druh	druh	k1gInSc4	druh
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
peckoviny	peckovina	k1gFnPc4	peckovina
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
plod	plod	k1gInSc4	plod
některých	některý	k3yIgFnPc2	některý
dřevin	dřevina	k1gFnPc2	dřevina
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
slivoň	slivoň	k1gFnSc1	slivoň
(	(	kIx(	(
<g/>
Prunus	Prunus	k1gInSc1	Prunus
<g/>
)	)	kIx)	)
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
měkký	měkký	k2eAgInSc1d1	měkký
kulovitý	kulovitý	k2eAgInSc1d1	kulovitý
plod	plod	k1gInSc1	plod
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
uvnitř	uvnitř	k6eAd1	uvnitř
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
,	,	kIx,	,
pecku	pecka	k1gFnSc4	pecka
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
ní	on	k3xPp3gFnSc2	on
je	být	k5eAaImIp3nS	být
sladká	sladký	k2eAgFnSc1d1	sladká
dužnina	dužnina	k1gFnSc1	dužnina
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
bílé	bílý	k2eAgFnPc1d1	bílá
až	až	k8xS	až
narůžovělé	narůžovělý	k2eAgFnPc1d1	narůžovělá
barvy	barva	k1gFnPc1	barva
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgInSc1d1	obsahující
vysoký	vysoký	k2eAgInSc1d1	vysoký
podíl	podíl	k1gInSc1	podíl
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
ohraničena	ohraničit	k5eAaPmNgFnS	ohraničit
je	být	k5eAaImIp3nS	být
jedlou	jedlý	k2eAgFnSc7d1	jedlá
slupkou	slupka	k1gFnSc7	slupka
<g/>
.	.	kIx.	.
</s>
<s>
Zralý	zralý	k2eAgInSc1d1	zralý
plod	plod	k1gInSc1	plod
má	mít	k5eAaImIp3nS	mít
slupku	slupka	k1gFnSc4	slupka
červenou	červený	k2eAgFnSc4d1	červená
až	až	k9	až
tmavě	tmavě	k6eAd1	tmavě
rudou	rudý	k2eAgFnSc7d1	rudá
a	a	k8xC	a
měří	měřit	k5eAaImIp3nS	měřit
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
1	[number]	k4	1
až	až	k9	až
2	[number]	k4	2
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Nezralý	zralý	k2eNgInSc1d1	nezralý
plod	plod	k1gInSc1	plod
je	být	k5eAaImIp3nS	být
zelený	zelený	k2eAgInSc1d1	zelený
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Třešně	třešně	k1gFnSc1	třešně
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
kulového	kulový	k2eAgInSc2d1	kulový
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
dvěma	dva	k4xCgFnPc7	dva
nesourodými	sourodý	k2eNgFnPc7d1	nesourodá
částmi	část	k1gFnPc7	část
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgInP	spojit
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vzniká	vznikat	k5eAaImIp3nS	vznikat
slabá	slabý	k2eAgFnSc1d1	slabá
rýha	rýha	k1gFnSc1	rýha
a	a	k8xC	a
ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
ostrý	ostrý	k2eAgInSc1d1	ostrý
výběžek	výběžek	k1gInSc1	výběžek
<g/>
.	.	kIx.	.
</s>
<s>
Dužnina	dužnina	k1gFnSc1	dužnina
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
z	z	k7c2	z
80	[number]	k4	80
%	%	kIx~	%
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
ovocnými	ovocný	k2eAgInPc7d1	ovocný
cukry	cukr	k1gInPc7	cukr
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
bohatým	bohatý	k2eAgInSc7d1	bohatý
zdrojem	zdroj	k1gInSc7	zdroj
jak	jak	k6eAd1	jak
minerálů	minerál	k1gInPc2	minerál
tak	tak	k9	tak
i	i	k9	i
vitamínů	vitamín	k1gInPc2	vitamín
<g/>
.	.	kIx.	.
</s>
<s>
Tmavší	tmavý	k2eAgInPc1d2	tmavší
druhy	druh	k1gInPc1	druh
třešní	třešeň	k1gFnPc2	třešeň
jsou	být	k5eAaImIp3nP	být
bohatým	bohatý	k2eAgInSc7d1	bohatý
zdrojem	zdroj	k1gInSc7	zdroj
hořčíku	hořčík	k1gInSc2	hořčík
<g/>
,	,	kIx,	,
železa	železo	k1gNnSc2	železo
<g/>
,	,	kIx,	,
jódu	jód	k1gInSc2	jód
<g/>
,	,	kIx,	,
fosforu	fosfor	k1gInSc2	fosfor
<g/>
,	,	kIx,	,
zinku	zinek	k1gInSc2	zinek
<g/>
,	,	kIx,	,
vápníku	vápník	k1gInSc2	vápník
<g/>
,	,	kIx,	,
draslíku	draslík	k1gInSc2	draslík
a	a	k8xC	a
křemíku	křemík	k1gInSc2	křemík
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vitamínů	vitamín	k1gInPc2	vitamín
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
ve	v	k7c6	v
významné	významný	k2eAgFnSc6d1	významná
míře	míra	k1gFnSc6	míra
vitamín	vitamín	k1gInSc4	vitamín
A	a	k8xC	a
či	či	k8xC	či
betakaroten	betakaroten	k2eAgInSc1d1	betakaroten
<g/>
,	,	kIx,	,
vitamín	vitamín	k1gInSc1	vitamín
P	P	kA	P
<g/>
,	,	kIx,	,
vitamín	vitamín	k1gInSc1	vitamín
C	C	kA	C
<g/>
,	,	kIx,	,
vitamín	vitamín	k1gInSc1	vitamín
E	E	kA	E
<g/>
,	,	kIx,	,
vitamín	vitamín	k1gInSc1	vitamín
B.	B.	kA	B.
</s>
</p>
<p>
<s>
Třešně	třešeň	k1gFnPc1	třešeň
mají	mít	k5eAaImIp3nP	mít
blahodárný	blahodárný	k2eAgInSc4d1	blahodárný
vliv	vliv	k1gInSc4	vliv
pro	pro	k7c4	pro
lidský	lidský	k2eAgInSc4d1	lidský
organismus	organismus	k1gInSc4	organismus
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
čistit	čistit	k5eAaImF	čistit
krev	krev	k1gFnSc4	krev
<g/>
,	,	kIx,	,
játra	játra	k1gNnPc4	játra
a	a	k8xC	a
ledviny	ledvina	k1gFnPc4	ledvina
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
konzumace	konzumace	k1gFnSc1	konzumace
podporuje	podporovat	k5eAaImIp3nS	podporovat
vyměšování	vyměšování	k1gNnSc4	vyměšování
trávicích	trávicí	k2eAgFnPc2d1	trávicí
šťáv	šťáva	k1gFnPc2	šťáva
a	a	k8xC	a
moči	moč	k1gFnSc2	moč
<g/>
.	.	kIx.	.
</s>
<s>
Novější	nový	k2eAgFnPc1d2	novější
studie	studie	k1gFnPc1	studie
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
konzumace	konzumace	k1gFnSc1	konzumace
třešní	třešeň	k1gFnPc2	třešeň
má	mít	k5eAaImIp3nS	mít
blahodárný	blahodárný	k2eAgInSc4d1	blahodárný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
léčení	léčení	k1gNnSc4	léčení
cukrovky	cukrovka	k1gFnSc2	cukrovka
pomocí	pomocí	k7c2	pomocí
barviva	barvivo	k1gNnSc2	barvivo
antokyan	antokyana	k1gFnPc2	antokyana
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
v	v	k7c6	v
laboratorních	laboratorní	k2eAgInPc6d1	laboratorní
testech	test	k1gInPc6	test
podporovalo	podporovat	k5eAaImAgNnS	podporovat
produkci	produkce	k1gFnSc4	produkce
inzulínu	inzulín	k1gInSc2	inzulín
<g/>
,	,	kIx,	,
a	a	k8xC	a
současně	současně	k6eAd1	současně
fungují	fungovat	k5eAaImIp3nP	fungovat
i	i	k9	i
jako	jako	k9	jako
antioxidanty	antioxidant	k1gInPc1	antioxidant
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
vysokému	vysoký	k2eAgInSc3d1	vysoký
obsahu	obsah	k1gInSc3	obsah
jódu	jód	k1gInSc2	jód
mají	mít	k5eAaImIp3nP	mít
příznivý	příznivý	k2eAgInSc4d1	příznivý
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
problémy	problém	k1gInPc4	problém
se	s	k7c7	s
štítnou	štítný	k2eAgFnSc7d1	štítná
žlázou	žláza	k1gFnSc7	žláza
a	a	k8xC	a
bolestmi	bolest	k1gFnPc7	bolest
páteře	páteř	k1gFnSc2	páteř
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
vysokému	vysoký	k2eAgInSc3d1	vysoký
podílu	podíl	k1gInSc3	podíl
vápníku	vápník	k1gInSc2	vápník
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
dobrým	dobrý	k2eAgInSc7d1	dobrý
zdrojem	zdroj	k1gInSc7	zdroj
pro	pro	k7c4	pro
růst	růst	k1gInSc4	růst
kostí	kost	k1gFnPc2	kost
a	a	k8xC	a
zubů	zub	k1gInPc2	zub
<g/>
.	.	kIx.	.
</s>
<s>
Pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
chránit	chránit	k5eAaImF	chránit
tělo	tělo	k1gNnSc4	tělo
proti	proti	k7c3	proti
různým	různý	k2eAgInPc3d1	různý
druhům	druh	k1gInPc3	druh
zánětů	zánět	k1gInPc2	zánět
<g/>
,	,	kIx,	,
parodontóze	parodontóza	k1gFnSc3	parodontóza
a	a	k8xC	a
artritidě	artritida	k1gFnSc3	artritida
<g/>
.	.	kIx.	.
<g/>
Jsou	být	k5eAaImIp3nP	být
vhodným	vhodný	k2eAgNnSc7d1	vhodné
dietním	dietní	k2eAgNnSc7d1	dietní
jídlem	jídlo	k1gNnSc7	jídlo
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
podporují	podporovat	k5eAaImIp3nP	podporovat
vylučování	vylučování	k1gNnSc4	vylučování
a	a	k8xC	a
tedy	tedy	k8xC	tedy
jsou	být	k5eAaImIp3nP	být
doporučovány	doporučovat	k5eAaImNgFnP	doporučovat
pro	pro	k7c4	pro
hubnutí	hubnutí	k1gNnSc4	hubnutí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Název	název	k1gInSc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
Třešně	třešně	k1gFnSc1	třešně
(	(	kIx(	(
<g/>
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
a	a	k8xC	a
rus	rus	k1gMnSc1	rus
<g/>
.	.	kIx.	.
čerešňa	čerešňa	k1gMnSc1	čerešňa
<g/>
,	,	kIx,	,
něm.	něm.	k?	něm.
Kirsche	kirsch	k1gInPc1	kirsch
<g/>
,	,	kIx,	,
ang	ang	k?	ang
<g/>
.	.	kIx.	.
cherry	cherra	k1gMnSc2	cherra
<g/>
,	,	kIx,	,
fran	fran	k1gNnSc1	fran
<g/>
.	.	kIx.	.
cerise	cerise	k1gFnSc1	cerise
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
praslovanského	praslovanský	k2eAgNnSc2d1	praslovanské
čerš	čerš	k5eAaPmIp2nS	čerš
<g/>
[	[	kIx(	[
<g/>
ň	ň	k?	ň
<g/>
]	]	kIx)	]
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
z	z	k7c2	z
latinského	latinský	k2eAgMnSc2d1	latinský
cerasus	cerasus	k1gMnSc1	cerasus
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
opět	opět	k6eAd1	opět
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
z	z	k7c2	z
íránsko-tureckého	íránskourecký	k2eAgMnSc2d1	íránsko-turecký
keras	keras	k1gInSc4	keras
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
města	město	k1gNnSc2	město
Kérason	Kérasona	k1gFnPc2	Kérasona
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Původní	původní	k2eAgInSc1d1	původní
výskyt	výskyt	k1gInSc1	výskyt
===	===	k?	===
</s>
</p>
<p>
<s>
Třešně	třešeň	k1gFnPc1	třešeň
původně	původně	k6eAd1	původně
rostly	růst	k5eAaImAgFnP	růst
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
Malé	Malé	k2eAgFnSc2d1	Malé
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
dovezl	dovézt	k5eAaPmAgMnS	dovézt
římský	římský	k2eAgMnSc1d1	římský
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
Lucullus	Lucullus	k1gMnSc1	Lucullus
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Na	na	k7c6	na
území	území	k1gNnSc6	území
Česka	Česko	k1gNnSc2	Česko
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gNnSc4	jejich
pěstování	pěstování	k1gNnSc4	pěstování
prvně	prvně	k?	prvně
doloženo	doložen	k2eAgNnSc1d1	doloženo
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pěstování	pěstování	k1gNnSc1	pěstování
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
plodu	plod	k1gInSc2	plod
==	==	k?	==
</s>
</p>
<p>
<s>
Třešně	třešně	k1gFnSc1	třešně
vzniká	vznikat	k5eAaImIp3nS	vznikat
z	z	k7c2	z
bílého	bílý	k2eAgInSc2d1	bílý
až	až	k8xS	až
narůžovělého	narůžovělý	k2eAgInSc2d1	narůžovělý
květu	květ	k1gInSc2	květ
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
po	po	k7c6	po
odkvetení	odkvetení	k1gNnSc6	odkvetení
objevuje	objevovat	k5eAaImIp3nS	objevovat
malá	malý	k2eAgFnSc1d1	malá
zelená	zelený	k2eAgFnSc1d1	zelená
bobule	bobule	k1gFnSc1	bobule
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
postupně	postupně	k6eAd1	postupně
roste	růst	k5eAaImIp3nS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
zrání	zrání	k1gNnSc2	zrání
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Plod	plod	k1gInSc1	plod
začíná	začínat	k5eAaImIp3nS	začínat
pomalu	pomalu	k6eAd1	pomalu
žloutnout	žloutnout	k5eAaImF	žloutnout
a	a	k8xC	a
v	v	k7c6	v
době	doba	k1gFnSc6	doba
uzrálosti	uzrálost	k1gFnSc2	uzrálost
červenat	červenat	k5eAaImF	červenat
až	až	k8xS	až
rudnout	rudnout	k5eAaImF	rudnout
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
zrání	zrání	k1gNnSc2	zrání
jsou	být	k5eAaImIp3nP	být
plody	plod	k1gInPc4	plod
často	často	k6eAd1	často
napadány	napadán	k2eAgMnPc4d1	napadán
ptáky	pták	k1gMnPc4	pták
(	(	kIx(	(
<g/>
špačky	špaček	k1gMnPc7	špaček
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
je	on	k3xPp3gFnPc4	on
konzumují	konzumovat	k5eAaBmIp3nP	konzumovat
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
škůdcem	škůdce	k1gMnSc7	škůdce
je	být	k5eAaImIp3nS	být
hmyz	hmyz	k1gInSc4	hmyz
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
do	do	k7c2	do
třešní	třešeň	k1gFnPc2	třešeň
klade	klást	k5eAaImIp3nS	klást
vajíčka	vajíčko	k1gNnPc4	vajíčko
a	a	k8xC	a
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
červivost	červivost	k1gFnSc4	červivost
plodů	plod	k1gInPc2	plod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Druhy	druh	k1gInPc4	druh
třešní	třešeň	k1gFnPc2	třešeň
===	===	k?	===
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
známy	znám	k2eAgInPc1d1	znám
pouze	pouze	k6eAd1	pouze
sladké	sladký	k2eAgInPc1d1	sladký
druhy	druh	k1gInPc1	druh
asijských	asijský	k2eAgFnPc2d1	asijská
třešní	třešeň	k1gFnPc2	třešeň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
popsáno	popsat	k5eAaPmNgNnS	popsat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
tisíc	tisíc	k4xCgInPc2	tisíc
druhů	druh	k1gInPc2	druh
třešní	třešeň	k1gFnPc2	třešeň
<g/>
,	,	kIx,	,
višní	višeň	k1gFnPc2	višeň
a	a	k8xC	a
sladkovišní	sladkovišeň	k1gFnPc2	sladkovišeň
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
druhová	druhový	k2eAgFnSc1d1	druhová
rozmanitost	rozmanitost	k1gFnSc1	rozmanitost
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
třešně	třešeň	k1gFnPc1	třešeň
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
široké	široký	k2eAgFnSc6d1	široká
škále	škála	k1gFnSc6	škála
barev	barva	k1gFnPc2	barva
od	od	k7c2	od
žluté	žlutý	k2eAgFnSc2d1	žlutá
<g/>
,	,	kIx,	,
červené	červený	k2eAgFnSc2d1	červená
<g/>
,	,	kIx,	,
růžové	růžový	k2eAgFnSc2d1	růžová
až	až	k9	až
po	po	k7c4	po
tmavě	tmavě	k6eAd1	tmavě
rudé	rudý	k2eAgInPc4d1	rudý
až	až	k8xS	až
černočervené	černočervený	k2eAgInPc4d1	černočervený
druhy	druh	k1gInPc4	druh
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
i	i	k9	i
chutí	chuť	k1gFnSc7	chuť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Třešeň	třešeň	k1gFnSc1	třešeň
ptačí	ptačit	k5eAaImIp3nS	ptačit
tmavá	tmavý	k2eAgFnSc1d1	tmavá
====	====	k?	====
</s>
</p>
<p>
<s>
Lidově	lidově	k6eAd1	lidově
zvaná	zvaný	k2eAgFnSc1d1	zvaná
ptáčnice	ptáčnice	k1gFnSc1	ptáčnice
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
třešně	třešeň	k1gFnSc2	třešeň
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgInPc4d1	malý
plody	plod	k1gInPc4	plod
často	často	k6eAd1	často
kyselé	kyselý	k2eAgFnSc3d1	kyselá
chuti	chuť	k1gFnSc3	chuť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Třešeň	třešeň	k1gFnSc1	třešeň
srdcovka	srdcovka	k1gFnSc1	srdcovka
====	====	k?	====
</s>
</p>
<p>
<s>
Oproti	oproti	k7c3	oproti
ostatním	ostatní	k2eAgInPc3d1	ostatní
druhům	druh	k1gInPc3	druh
má	mít	k5eAaImIp3nS	mít
měkkou	měkký	k2eAgFnSc4d1	měkká
a	a	k8xC	a
šťavnatou	šťavnatý	k2eAgFnSc4d1	šťavnatá
dužninu	dužnina	k1gFnSc4	dužnina
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
odrůdy	odrůda	k1gFnPc1	odrůda
jsou	být	k5eAaImIp3nP	být
Rivan	Rivan	k1gInSc4	Rivan
(	(	kIx(	(
<g/>
nejranější	raný	k2eAgFnSc1d3	nejranější
<g/>
,	,	kIx,	,
zraje	zrát	k5eAaImIp3nS	zrát
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
třešňovém	třešňový	k2eAgInSc6d1	třešňový
týdnu	týden	k1gInSc6	týden
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Karešova	Karešův	k2eAgFnSc1d1	Karešova
(	(	kIx(	(
<g/>
původní	původní	k2eAgFnSc1d1	původní
česká	český	k2eAgFnSc1d1	Česká
odrůda	odrůda	k1gFnSc1	odrůda
<g/>
,	,	kIx,	,
nejpěstovanější	pěstovaný	k2eAgFnSc1d3	nejpěstovanější
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kaštánka	kaštánek	k1gMnSc2	kaštánek
(	(	kIx(	(
<g/>
tmavá	tmavý	k2eAgFnSc1d1	tmavá
barva	barva	k1gFnSc1	barva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Třešeň	třešeň	k1gFnSc1	třešeň
chrupka	chrupka	k1gFnSc1	chrupka
====	====	k?	====
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
pevnou	pevný	k2eAgFnSc4d1	pevná
dužninu	dužnina	k1gFnSc4	dužnina
a	a	k8xC	a
červenou	červený	k2eAgFnSc4d1	červená
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
velká	velká	k1gFnSc1	velká
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
2	[number]	k4	2
centimetry	centimetr	k1gInPc4	centimetr
<g/>
.	.	kIx.	.
</s>
<s>
Plody	plod	k1gInPc1	plod
jsou	být	k5eAaImIp3nP	být
středně	středně	k6eAd1	středně
hořkokyselé	hořkokyselý	k2eAgFnPc1d1	hořkokyselá
<g/>
.	.	kIx.	.
</s>
<s>
Dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
na	na	k7c4	na
chrupky	chrupka	k1gFnPc4	chrupka
a	a	k8xC	a
polochrupky	polochrupka	k1gFnPc4	polochrupka
<g/>
.	.	kIx.	.
</s>
<s>
Zrají	zrát	k5eAaImIp3nP	zrát
většinou	většina	k1gFnSc7	většina
od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
třešňového	třešňový	k2eAgInSc2d1	třešňový
týdne	týden	k1gInSc2	týden
do	do	k7c2	do
6	[number]	k4	6
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
napadány	napadán	k2eAgInPc4d1	napadán
vrtulí	vrtule	k1gFnSc7	vrtule
třešňovou	třešňový	k2eAgFnSc7d1	třešňová
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
odrůdy	odrůda	k1gFnPc1	odrůda
jsou	být	k5eAaImIp3nP	být
Kordia	Kordium	k1gNnPc1	Kordium
<g/>
,	,	kIx,	,
Burlat	Burle	k1gNnPc2	Burle
<g/>
,	,	kIx,	,
Granát	granát	k1gInSc1	granát
<g/>
,	,	kIx,	,
Napoleonova	Napoleonův	k2eAgFnSc1d1	Napoleonova
<g/>
,	,	kIx,	,
Sam	Sam	k1gMnSc1	Sam
<g/>
,	,	kIx,	,
Vanda	Vanda	k1gFnSc1	Vanda
<g/>
,	,	kIx,	,
Van	van	k1gInSc1	van
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Mareše	Mareš	k1gMnSc2	Mareš
====	====	k?	====
</s>
</p>
<p>
<s>
Druh	druh	k1gInSc4	druh
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Dalmácie	Dalmácie	k1gFnSc2	Dalmácie
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
typický	typický	k2eAgInSc1d1	typický
pro	pro	k7c4	pro
svojí	svojit	k5eAaImIp3nP	svojit
hořkou	hořký	k2eAgFnSc4d1	hořká
chuť	chuť	k1gFnSc4	chuť
a	a	k8xC	a
malé	malý	k2eAgInPc4d1	malý
červenočerné	červenočerný	k2eAgInPc4d1	červenočerný
plody	plod	k1gInPc4	plod
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
využívány	využívat	k5eAaImNgInP	využívat
pro	pro	k7c4	pro
přípravu	příprava	k1gFnSc4	příprava
alkoholického	alkoholický	k2eAgInSc2d1	alkoholický
nápoje	nápoj	k1gInSc2	nápoj
maraskino	maraskino	k1gNnSc1	maraskino
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Konzumace	konzumace	k1gFnSc2	konzumace
==	==	k?	==
</s>
</p>
<p>
<s>
Třešně	třešně	k1gFnSc1	třešně
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
jedí	jíst	k5eAaImIp3nP	jíst
za	za	k7c2	za
syrova	syrov	k1gInSc2	syrov
<g/>
,	,	kIx,	,
nikterak	nikterak	k6eAd1	nikterak
upravené	upravený	k2eAgFnPc1d1	upravená
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jejich	jejich	k3xOp3gFnSc4	jejich
konzumaci	konzumace	k1gFnSc4	konzumace
stačí	stačit	k5eAaBmIp3nS	stačit
pouze	pouze	k6eAd1	pouze
jejich	jejich	k3xOp3gInSc4	jejich
sběr	sběr	k1gInSc4	sběr
ze	z	k7c2	z
stromu	strom	k1gInSc2	strom
a	a	k8xC	a
následné	následný	k2eAgNnSc4d1	následné
požití	požití	k1gNnSc4	požití
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
třešně	třešeň	k1gFnPc1	třešeň
také	také	k9	také
zavařují	zavařovat	k5eAaImIp3nP	zavařovat
do	do	k7c2	do
kompotů	kompot	k1gInPc2	kompot
<g/>
,	,	kIx,	,
či	či	k8xC	či
se	se	k3xPyFc4	se
zmrazují	zmrazovat	k5eAaImIp3nP	zmrazovat
pro	pro	k7c4	pro
pozdější	pozdní	k2eAgNnSc4d2	pozdější
využití	využití	k1gNnSc4	využití
<g/>
.	.	kIx.	.
</s>
<s>
Dají	dát	k5eAaPmIp3nP	dát
se	se	k3xPyFc4	se
i	i	k9	i
tepelně	tepelně	k6eAd1	tepelně
zpracovávat	zpracovávat	k5eAaImF	zpracovávat
na	na	k7c4	na
pokrmy	pokrm	k1gInPc4	pokrm
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
pečení	pečení	k1gNnSc4	pečení
či	či	k8xC	či
do	do	k7c2	do
marmelád	marmeláda	k1gFnPc2	marmeláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
nejznámějším	známý	k2eAgNnSc7d3	nejznámější
pečivem	pečivo	k1gNnSc7	pečivo
třešňová	třešňový	k2eAgFnSc1d1	třešňová
bublanina	bublanina	k1gFnSc1	bublanina
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
i	i	k9	i
jako	jako	k9	jako
dekorační	dekorační	k2eAgInSc1d1	dekorační
prvek	prvek	k1gInSc1	prvek
u	u	k7c2	u
dortů	dort	k1gInPc2	dort
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
třešně	třešeň	k1gFnPc1	třešeň
podléhají	podléhat	k5eAaImIp3nP	podléhat
rychlému	rychlý	k2eAgNnSc3d1	rychlé
kažení	kažení	k1gNnSc3	kažení
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
jejich	jejich	k3xOp3gFnSc1	jejich
rychlá	rychlý	k2eAgFnSc1d1	rychlá
konzumace	konzumace	k1gFnSc1	konzumace
<g/>
.	.	kIx.	.
</s>
<s>
Případné	případný	k2eAgNnSc1d1	případné
skladování	skladování	k1gNnSc1	skladování
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
v	v	k7c6	v
chladném	chladný	k2eAgNnSc6d1	chladné
a	a	k8xC	a
tmavém	tmavý	k2eAgNnSc6d1	tmavé
prostředí	prostředí	k1gNnSc6	prostředí
a	a	k8xC	a
jen	jen	k9	jen
po	po	k7c4	po
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
<g/>
Podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
ostatní	ostatní	k2eAgFnPc4d1	ostatní
peckoviny	peckovina	k1gFnPc4	peckovina
dochází	docházet	k5eAaImIp3nS	docházet
při	při	k7c6	při
jejich	jejich	k3xOp3gInSc6	jejich
kontaktu	kontakt	k1gInSc6	kontakt
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
k	k	k7c3	k
rychlému	rychlý	k2eAgNnSc3d1	rychlé
kvašení	kvašení	k1gNnSc3	kvašení
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
značně	značně	k6eAd1	značně
nepříjemné	příjemný	k2eNgInPc4d1	nepříjemný
zažívací	zažívací	k2eAgInPc4d1	zažívací
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnSc1	jejich
konzumace	konzumace	k1gFnSc1	konzumace
nespojovala	spojovat	k5eNaImAgFnS	spojovat
s	s	k7c7	s
pitím	pití	k1gNnSc7	pití
vody	voda	k1gFnSc2	voda
či	či	k8xC	či
jiné	jiný	k2eAgFnSc2d1	jiná
tekutiny	tekutina	k1gFnSc2	tekutina
a	a	k8xC	a
po	po	k7c4	po
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
se	se	k3xPyFc4	se
s	s	k7c7	s
pitím	pití	k1gNnSc7	pití
počkalo	počkat	k5eAaPmAgNnS	počkat
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Třešně	třešeň	k1gFnPc1	třešeň
se	se	k3xPyFc4	se
využívají	využívat	k5eAaPmIp3nP	využívat
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
jak	jak	k6eAd1	jak
nealkoholických	alkoholický	k2eNgFnPc2d1	nealkoholická
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Cherry	Cherra	k1gMnSc2	Cherra
Cola	cola	k1gFnSc1	cola
<g/>
,	,	kIx,	,
ovocné	ovocný	k2eAgFnPc1d1	ovocná
šťávy	šťáva	k1gFnPc1	šťáva
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
alkoholických	alkoholický	k2eAgMnPc2d1	alkoholický
nápojů	nápoj	k1gInPc2	nápoj
(	(	kIx(	(
<g/>
pivo	pivo	k1gNnSc1	pivo
<g/>
,	,	kIx,	,
likéry	likér	k1gInPc1	likér
<g/>
)	)	kIx)	)
a	a	k8xC	a
destilátů	destilát	k1gInPc2	destilát
v	v	k7c6	v
širokém	široký	k2eAgNnSc6d1	široké
zastoupení	zastoupení	k1gNnSc6	zastoupení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgNnPc1d1	další
využití	využití	k1gNnPc1	využití
===	===	k?	===
</s>
</p>
<p>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
plody	plod	k1gInPc1	plod
jsou	být	k5eAaImIp3nP	být
využívány	využívat	k5eAaImNgInP	využívat
i	i	k9	i
v	v	k7c6	v
kosmetice	kosmetika	k1gFnSc6	kosmetika
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
mají	mít	k5eAaImIp3nP	mít
schopnost	schopnost	k1gFnSc4	schopnost
dehydrovat	dehydrovat	k5eAaBmF	dehydrovat
buňky	buňka	k1gFnPc4	buňka
a	a	k8xC	a
zpomalovat	zpomalovat	k5eAaImF	zpomalovat
stárnutí	stárnutí	k1gNnSc4	stárnutí
pokožky	pokožka	k1gFnSc2	pokožka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
využívat	využívat	k5eAaPmF	využívat
i	i	k9	i
pecky	pecek	k1gInPc4	pecek
z	z	k7c2	z
třešní	třešeň	k1gFnPc2	třešeň
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
po	po	k7c6	po
vysušení	vysušení	k1gNnSc6	vysušení
a	a	k8xC	a
umístění	umístění	k1gNnSc4	umístění
do	do	k7c2	do
pytlíku	pytlík	k1gInSc2	pytlík
fungují	fungovat	k5eAaImIp3nP	fungovat
jako	jako	k9	jako
absorbent	absorbent	k1gInSc4	absorbent
a	a	k8xC	a
emitor	emitor	k1gInSc4	emitor
tepla	teplo	k1gNnSc2	teplo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odrůdy	odrůda	k1gFnSc2	odrůda
==	==	k?	==
</s>
</p>
<p>
<s>
Rychlice	Rychlice	k1gFnSc1	Rychlice
německá	německý	k2eAgFnSc1d1	německá
</s>
</p>
<p>
<s>
Rivan	Rivan	k1gMnSc1	Rivan
</s>
</p>
<p>
<s>
Burlat	Burlat	k5eAaPmF	Burlat
</s>
</p>
<p>
<s>
Napoleonova	Napoleonův	k2eAgFnSc1d1	Napoleonova
</s>
</p>
<p>
<s>
Stella	Stella	k1gFnSc1	Stella
</s>
</p>
<p>
<s>
Libějovická	Libějovický	k2eAgFnSc1d1	Libějovický
raná	raný	k2eAgFnSc1d1	raná
</s>
</p>
<p>
<s>
Regina	Regina	k1gFnSc1	Regina
</s>
</p>
<p>
<s>
Lapins	Lapins	k6eAd1	Lapins
</s>
</p>
<p>
<s>
Karešova	Karešův	k2eAgFnSc1d1	Karešova
</s>
</p>
<p>
<s>
Kaštánka	kaštánek	k1gMnSc4	kaštánek
</s>
</p>
<p>
<s>
==	==	k?	==
Průměrný	průměrný	k2eAgInSc1d1	průměrný
obsah	obsah	k1gInSc1	obsah
látek	látka	k1gFnPc2	látka
a	a	k8xC	a
minerálů	minerál	k1gInPc2	minerál
==	==	k?	==
</s>
</p>
<p>
<s>
Tabulka	tabulka	k1gFnSc1	tabulka
udává	udávat	k5eAaImIp3nS	udávat
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
průměrný	průměrný	k2eAgInSc1d1	průměrný
obsah	obsah	k1gInSc1	obsah
živin	živina	k1gFnPc2	živina
<g/>
,	,	kIx,	,
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
vitamínů	vitamín	k1gInPc2	vitamín
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
nutričních	nutriční	k2eAgInPc2d1	nutriční
parametrů	parametr	k1gInPc2	parametr
zjištěných	zjištěný	k2eAgInPc2d1	zjištěný
v	v	k7c6	v
syrových	syrový	k2eAgFnPc6d1	syrová
třešních	třešeň	k1gFnPc6	třešeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Třešně	třešeň	k1gFnSc2	třešeň
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Třešně	třešeň	k1gFnPc1	třešeň
jsou	být	k5eAaImIp3nP	být
hojně	hojně	k6eAd1	hojně
rozšířeny	rozšířit	k5eAaPmNgInP	rozšířit
a	a	k8xC	a
konzumovány	konzumovat	k5eAaBmNgInP	konzumovat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
i	i	k9	i
v	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
častém	častý	k2eAgInSc6d1	častý
výskytu	výskyt	k1gInSc6	výskyt
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k9	už
jako	jako	k9	jako
potraviny	potravina	k1gFnPc1	potravina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
pojídána	pojídán	k2eAgFnSc1d1	pojídána
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
jako	jako	k9	jako
častý	častý	k2eAgInSc4d1	častý
námět	námět	k1gInSc4	námět
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Waldemar	Waldemar	k1gMnSc1	Waldemar
Matuška	Matuška	k1gMnSc1	Matuška
je	on	k3xPp3gNnSc4	on
využívá	využívat	k5eAaImIp3nS	využívat
jako	jako	k9	jako
ústřední	ústřední	k2eAgInSc1d1	ústřední
motiv	motiv	k1gInSc1	motiv
svojí	svůj	k3xOyFgFnSc2	svůj
písně	píseň	k1gFnSc2	píseň
Jó	jó	k0	jó
<g/>
,	,	kIx,	,
třešně	třešeň	k1gFnPc1	třešeň
zrály	zrát	k5eAaImAgFnP	zrát
<g/>
,	,	kIx,	,
či	či	k8xC	či
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
v	v	k7c6	v
názvu	název	k1gInSc6	název
filmu	film	k1gInSc6	film
Buldoci	buldok	k1gMnPc1	buldok
a	a	k8xC	a
třešně	třešeň	k1gFnPc1	třešeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Třešně	třešeň	k1gFnPc1	třešeň
jsou	být	k5eAaImIp3nP	být
tradičním	tradiční	k2eAgInSc7d1	tradiční
symbolem	symbol	k1gInSc7	symbol
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	on	k3xPp3gInPc4	on
využívá	využívat	k5eAaPmIp3nS	využívat
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
logu	log	k1gInSc6	log
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ŘÍHA	Říha	k1gMnSc1	Říha
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
ovoce	ovoce	k1gNnSc1	ovoce
:	:	kIx,	:
Díl	díl	k1gInSc1	díl
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Třešně	třešeň	k1gFnPc1	třešeň
<g/>
,	,	kIx,	,
višně	višeň	k1gFnPc1	višeň
<g/>
,	,	kIx,	,
slívy	slíva	k1gFnPc1	slíva
a	a	k8xC	a
švestky	švestka	k1gFnPc1	švestka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ovocnický	ovocnický	k2eAgInSc1d1	ovocnický
spolek	spolek	k1gInSc1	spolek
pro	pro	k7c4	pro
království	království	k1gNnSc4	království
České	český	k2eAgFnSc2d1	Česká
<g/>
,	,	kIx,	,
1915	[number]	k4	1915
<g/>
.	.	kIx.	.
251	[number]	k4	251
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
-	-	kIx~	-
kapitola	kapitola	k1gFnSc1	kapitola
Třešně	třešně	k1gFnSc1	třešně
a	a	k8xC	a
višně	višně	k1gFnSc1	višně
<g/>
,	,	kIx,	,
s.	s.	k?	s.
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
116	[number]	k4	116
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
třešeň	třešeň	k1gFnSc1	třešeň
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
třešně	třešeň	k1gFnSc2	třešeň
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Podrobný	podrobný	k2eAgInSc1d1	podrobný
popis	popis	k1gInSc1	popis
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
třešní	třešeň	k1gFnPc2	třešeň
</s>
</p>
<p>
<s>
Čerstvé	čerstvý	k2eAgFnPc1d1	čerstvá
třešně	třešeň	k1gFnPc1	třešeň
na	na	k7c6	na
vánočním	vánoční	k2eAgInSc6d1	vánoční
stole	stol	k1gInSc6	stol
ohrožují	ohrožovat	k5eAaImIp3nP	ohrožovat
klima	klima	k1gNnSc4	klima
</s>
</p>
