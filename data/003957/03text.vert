<s>
Jihlava	Jihlava	k1gFnSc1	Jihlava
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Iglau	Iglaus	k1gInSc3	Iglaus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
české	český	k2eAgFnPc4d1	Česká
krajské	krajský	k2eAgFnPc4d1	krajská
a	a	k8xC	a
statutární	statutární	k2eAgNnSc1d1	statutární
město	město	k1gNnSc1	město
115	[number]	k4	115
km	km	kA	km
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
položené	položený	k2eAgFnSc2d1	položená
na	na	k7c6	na
někdejší	někdejší	k2eAgFnSc6d1	někdejší
česko-moravské	českooravský	k2eAgFnSc6d1	česko-moravská
zemské	zemský	k2eAgFnSc6d1	zemská
hranici	hranice	k1gFnSc6	hranice
<g/>
,	,	kIx,	,
tvořené	tvořený	k2eAgFnPc1d1	tvořená
zde	zde	k6eAd1	zde
zčásti	zčásti	k6eAd1	zčásti
řekou	řeka	k1gFnSc7	řeka
Jihlavou	Jihlava	k1gFnSc7	Jihlava
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
moravské	moravský	k2eAgNnSc4d1	Moravské
město	město	k1gNnSc4	město
a	a	k8xC	a
i	i	k9	i
dnes	dnes	k6eAd1	dnes
většina	většina	k1gFnSc1	většina
Jihlavy	Jihlava	k1gFnSc2	Jihlava
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
moravské	moravský	k2eAgFnSc6d1	Moravská
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
severozápadní	severozápadní	k2eAgInSc4d1	severozápadní
okraj	okraj	k1gInSc4	okraj
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgNnSc1d1	historické
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
založené	založený	k2eAgNnSc1d1	založené
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jako	jako	k8xS	jako
hornické	hornický	k2eAgNnSc4d1	Hornické
s	s	k7c7	s
těžbou	těžba	k1gFnSc7	těžba
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
městskou	městský	k2eAgFnSc7d1	městská
památkovou	památkový	k2eAgFnSc7d1	památková
rezervací	rezervace	k1gFnSc7	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
Jihlava	Jihlava	k1gFnSc1	Jihlava
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
střediskem	středisko	k1gNnSc7	středisko
Kraje	kraj	k1gInSc2	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
okolo	okolo	k7c2	okolo
50	[number]	k4	50
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc4	název
Jihlava	Jihlava	k1gFnSc1	Jihlava
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Iglau	Iglaa	k1gFnSc4	Iglaa
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
původní	původní	k2eAgFnSc2d1	původní
kupecké	kupecký	k2eAgFnSc2d1	kupecká
osady	osada	k1gFnSc2	osada
u	u	k7c2	u
kostela	kostel	k1gInSc2	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
poblíž	poblíž	k7c2	poblíž
brodu	brod	k1gInSc2	brod
přes	přes	k7c4	přes
stejnojmennou	stejnojmenný	k2eAgFnSc4d1	stejnojmenná
řeku	řeka	k1gFnSc4	řeka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
dala	dát	k5eAaPmAgFnS	dát
osadě	osada	k1gFnSc6	osada
název	název	k1gInSc4	název
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
názvu	název	k1gInSc2	název
řeky	řeka	k1gFnSc2	řeka
však	však	k9	však
není	být	k5eNaImIp3nS	být
jasný	jasný	k2eAgInSc1d1	jasný
<g/>
.	.	kIx.	.
</s>
<s>
Mohli	moct	k5eAaImAgMnP	moct
ji	on	k3xPp3gFnSc4	on
pojmenovat	pojmenovat	k5eAaPmF	pojmenovat
Langobardi	Langobard	k1gMnPc1	Langobard
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
žili	žít	k5eAaImAgMnP	žít
u	u	k7c2	u
jejího	její	k3xOp3gInSc2	její
soutoku	soutok	k1gInSc2	soutok
s	s	k7c7	s
Dyjí	Dyje	k1gFnSc7	Dyje
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Igulaha	Igulaha	k1gFnSc1	Igulaha
(	(	kIx(	(
<g/>
ježový	ježový	k2eAgInSc1d1	ježový
potok	potok	k1gInSc1	potok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenování	pojmenování	k1gNnSc1	pojmenování
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
pocházet	pocházet	k5eAaImF	pocházet
i	i	k9	i
ze	z	k7c2	z
slovanských	slovanský	k2eAgInPc2d1	slovanský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
jehla	jehla	k1gFnSc1	jehla
<g/>
"	"	kIx"	"
označovalo	označovat	k5eAaImAgNnS	označovat
ostré	ostrý	k2eAgNnSc1d1	ostré
kamení	kamení	k1gNnSc1	kamení
v	v	k7c6	v
říčním	říční	k2eAgNnSc6d1	říční
korytu	koryto	k1gNnSc6	koryto
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc4	dějiny
Jihlavy	Jihlava	k1gFnSc2	Jihlava
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
osadě	osada	k1gFnSc6	osada
jménem	jméno	k1gNnSc7	jméno
Jihlava	Jihlava	k1gFnSc1	Jihlava
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1233	[number]	k4	1233
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
biskup	biskup	k1gMnSc1	biskup
Robert	Robert	k1gMnSc1	Robert
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
převod	převod	k1gInSc4	převod
zboží	zboží	k1gNnSc2	zboží
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
figuroval	figurovat	k5eAaImAgInS	figurovat
i	i	k9	i
název	název	k1gInSc1	název
Gyglaua	Gyglau	k1gInSc2	Gyglau
–	–	k?	–
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
)	)	kIx)	)
řádu	řád	k1gInSc2	řád
německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
do	do	k7c2	do
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
želivského	želivský	k2eAgInSc2d1	želivský
kláštera	klášter	k1gInSc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1234	[number]	k4	1234
vyměnil	vyměnit	k5eAaPmAgMnS	vyměnit
markrabě	markrabě	k1gMnSc1	markrabě
Přemysl	Přemysl	k1gMnSc1	Přemysl
a	a	k8xC	a
královna	královna	k1gFnSc1	královna
Konstancie	Konstancie	k1gFnSc2	Konstancie
s	s	k7c7	s
klášterem	klášter	k1gInSc7	klášter
Porta	porta	k1gFnSc1	porta
Coeli	Coele	k1gFnSc4	Coele
v	v	k7c6	v
Předklášteří	Předklášteří	k1gNnSc6	Předklášteří
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
statek	statek	k1gInSc4	statek
Jihlava	Jihlava	k1gFnSc1	Jihlava
s	s	k7c7	s
okolními	okolní	k2eAgFnPc7d1	okolní
vesnicemi	vesnice	k1gFnPc7	vesnice
a	a	k8xC	a
mýtem	mýtus	k1gInSc7	mýtus
za	za	k7c4	za
jiný	jiný	k2eAgInSc4d1	jiný
majetek	majetek	k1gInSc4	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1240	[number]	k4	1240
se	se	k3xPyFc4	se
Jihlava	Jihlava	k1gFnSc1	Jihlava
vrátila	vrátit	k5eAaPmAgFnS	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
držby	držba	k1gFnSc2	držba
Václava	Václav	k1gMnSc2	Václav
I.	I.	kA	I.
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1240	[number]	k4	1240
<g/>
–	–	k?	–
<g/>
1243	[number]	k4	1243
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
horní	horní	k2eAgNnSc1d1	horní
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
nového	nový	k2eAgNnSc2d1	nové
města	město	k1gNnSc2	město
přicházelo	přicházet	k5eAaImAgNnS	přicházet
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
množství	množství	k1gNnSc1	množství
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
ochotných	ochotný	k2eAgMnPc2d1	ochotný
se	se	k3xPyFc4	se
účastnit	účastnit	k5eAaImF	účastnit
na	na	k7c6	na
těžbě	těžba	k1gFnSc6	těžba
a	a	k8xC	a
zpracování	zpracování	k1gNnSc6	zpracování
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1249	[number]	k4	1249
se	se	k3xPyFc4	se
zmiňují	zmiňovat	k5eAaImIp3nP	zmiňovat
mince	mince	k1gFnPc1	mince
ražené	ražený	k2eAgFnPc1d1	ražená
v	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
<g/>
,	,	kIx,	,
jisté	jistý	k2eAgNnSc1d1	jisté
ovšem	ovšem	k9	ovšem
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
zde	zde	k6eAd1	zde
už	už	k6eAd1	už
tak	tak	k6eAd1	tak
brzy	brzy	k6eAd1	brzy
fungovala	fungovat	k5eAaImAgFnS	fungovat
mincovna	mincovna	k1gFnSc1	mincovna
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1253	[number]	k4	1253
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
i	i	k9	i
zakládající	zakládající	k2eAgFnSc1d1	zakládající
listina	listina	k1gFnSc1	listina
Jihlavy	Jihlava	k1gFnSc2	Jihlava
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
město	město	k1gNnSc1	město
charakterizovala	charakterizovat	k5eAaBmAgFnS	charakterizovat
i	i	k9	i
z	z	k7c2	z
právního	právní	k2eAgNnSc2d1	právní
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
,	,	kIx,	,
dodnes	dodnes	k6eAd1	dodnes
se	se	k3xPyFc4	se
však	však	k9	však
nedochovala	dochovat	k5eNaPmAgFnS	dochovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1270	[number]	k4	1270
Jihlava	Jihlava	k1gFnSc1	Jihlava
obdržela	obdržet	k5eAaPmAgFnS	obdržet
od	od	k7c2	od
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
stavební	stavební	k2eAgInSc1d1	stavební
řád	řád	k1gInSc1	řád
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vtiskl	vtisknout	k5eAaPmAgInS	vtisknout
historické	historický	k2eAgFnPc4d1	historická
části	část	k1gFnPc4	část
města	město	k1gNnSc2	město
pravidelný	pravidelný	k2eAgInSc4d1	pravidelný
půdorys	půdorys	k1gInSc4	půdorys
<g/>
,	,	kIx,	,
pravoúhlé	pravoúhlý	k2eAgFnPc1d1	pravoúhlá
sítě	síť	k1gFnPc1	síť
ulic	ulice	k1gFnPc2	ulice
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
náměstím	náměstí	k1gNnSc7	náměstí
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
.	.	kIx.	.
</s>
<s>
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
založena	založen	k2eAgFnSc1d1	založena
za	za	k7c4	za
Václava	Václav	k1gMnSc4	Václav
I.	I.	kA	I.
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
nese	nést	k5eAaImIp3nS	nést
výrazné	výrazný	k2eAgInPc4d1	výrazný
znaky	znak	k1gInPc4	znak
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
přímo	přímo	k6eAd1	přímo
založil	založit	k5eAaPmAgMnS	založit
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
navíc	navíc	k6eAd1	navíc
se	s	k7c7	s
stavebním	stavební	k2eAgInSc7d1	stavební
řádem	řád	k1gInSc7	řád
získalo	získat	k5eAaPmAgNnS	získat
od	od	k7c2	od
Otakara	Otakar	k1gMnSc2	Otakar
privilegium	privilegium	k1gNnSc4	privilegium
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
měšťanům	měšťan	k1gMnPc3	měšťan
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
mohli	moct	k5eAaImAgMnP	moct
sami	sám	k3xTgMnPc1	sám
regulovat	regulovat	k5eAaImF	regulovat
zástavbu	zástavba	k1gFnSc4	zástavba
ve	v	k7c6	v
vnitřních	vnitřní	k2eAgFnPc6d1	vnitřní
částech	část	k1gFnPc6	část
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
také	také	k9	také
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
stavbě	stavba	k1gFnSc3	stavba
opevnění	opevnění	k1gNnSc2	opevnění
s	s	k7c7	s
parkánem	parkán	k1gInSc7	parkán
a	a	k8xC	a
příkopem	příkop	k1gInSc7	příkop
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
listině	listina	k1gFnSc6	listina
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
znovuvýstavbě	znovuvýstavba	k1gFnSc6	znovuvýstavba
zřícených	zřícený	k2eAgFnPc2d1	zřícená
věží	věž	k1gFnPc2	věž
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
nasvědčovalo	nasvědčovat	k5eAaImAgNnS	nasvědčovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nějaké	nějaký	k3yIgNnSc1	nějaký
opevnění	opevnění	k1gNnSc1	opevnění
existovalo	existovat	k5eAaImAgNnS	existovat
již	již	k6eAd1	již
i	i	k9	i
dříve	dříve	k6eAd2	dříve
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jihlavu	Jihlava	k1gFnSc4	Jihlava
si	se	k3xPyFc3	se
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
obzvláště	obzvláště	k6eAd1	obzvláště
hýčkal	hýčkat	k5eAaImAgInS	hýčkat
kvůli	kvůli	k7c3	kvůli
výrazné	výrazný	k2eAgFnSc3d1	výrazná
těžbě	těžba	k1gFnSc3	těžba
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
od	od	k7c2	od
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
velký	velký	k2eAgInSc4d1	velký
nárůst	nárůst	k1gInSc4	nárůst
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1272	[number]	k4	1272
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
král	král	k1gMnSc1	král
–	–	k?	–
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
těžbou	těžba	k1gFnSc7	těžba
stříbra	stříbro	k1gNnSc2	stříbro
–	–	k?	–
jihlavským	jihlavský	k2eAgMnPc3d1	jihlavský
měšťanům	měšťan	k1gMnPc3	měšťan
svolení	svolení	k1gNnSc4	svolení
k	k	k7c3	k
prospektorské	prospektorský	k2eAgFnSc3d1	prospektorská
činnosti	činnost	k1gFnSc3	činnost
inter	intero	k1gNnPc2	intero
Yglauiam	Yglauiam	k1gInSc1	Yglauiam
et	et	k?	et
Vst	Vst	k1gFnSc2	Vst
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
těžko	těžko	k6eAd1	těžko
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
dnes	dnes	k6eAd1	dnes
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgInPc4	jaký
výnosy	výnos	k1gInPc4	výnos
měl	mít	k5eAaImAgMnS	mít
král	král	k1gMnSc1	král
ze	z	k7c2	z
zdejší	zdejší	k2eAgFnSc2d1	zdejší
těžby	těžba	k1gFnSc2	těžba
<g/>
.	.	kIx.	.
</s>
<s>
Údajně	údajně	k6eAd1	údajně
mu	on	k3xPp3gMnSc3	on
patřila	patřit	k5eAaImAgFnS	patřit
osmina	osmina	k1gFnSc1	osmina
-	-	kIx~	-
zvaná	zvaný	k2eAgFnSc1d1	zvaná
urbura	urbura	k1gFnSc1	urbura
–	–	k?	–
z	z	k7c2	z
každé	každý	k3xTgFnSc2	každý
těžby	těžba	k1gFnSc2	těžba
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
těžba	těžba	k1gFnSc1	těžba
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c6	na
Jihlavsku	Jihlavsko	k1gNnSc6	Jihlavsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejen	nejen	k6eAd1	nejen
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
spojována	spojovat	k5eAaImNgFnS	spojovat
s	s	k7c7	s
údajně	údajně	k6eAd1	údajně
famózním	famózní	k2eAgNnSc7d1	famózní
Otakarovým	Otakarův	k2eAgNnSc7d1	Otakarův
bohatstvím	bohatství	k1gNnSc7	bohatství
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
uvádí	uvádět	k5eAaImIp3nS	uvádět
Josef	Josef	k1gMnSc1	Josef
Žemlička	Žemlička	k1gMnSc1	Žemlička
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
věků	věk	k1gInPc2	věk
<g/>
,	,	kIx,	,
z	z	k7c2	z
mozaiky	mozaika	k1gFnSc2	mozaika
dochovaných	dochovaný	k2eAgInPc2d1	dochovaný
střípků	střípek	k1gInPc2	střípek
vědění	vědění	k1gNnPc2	vědění
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
stěží	stěží	k6eAd1	stěží
sestavit	sestavit	k5eAaPmF	sestavit
jasně	jasně	k6eAd1	jasně
čitelný	čitelný	k2eAgInSc1d1	čitelný
obraz	obraz	k1gInSc1	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Víme	vědět	k5eAaImIp1nP	vědět
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
těžba	těžba	k1gFnSc1	těžba
stříbra	stříbro	k1gNnSc2	stříbro
v	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
a	a	k8xC	a
okolí	okolí	k1gNnSc6	okolí
nejživější	živý	k2eAgFnSc4d3	nejživější
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
nebylo	být	k5eNaImAgNnS	být
katolické	katolický	k2eAgNnSc1d1	katolické
město	město	k1gNnSc1	město
Jihlava	Jihlava	k1gFnSc1	Jihlava
husity	husita	k1gMnPc4	husita
nikdy	nikdy	k6eAd1	nikdy
dobyto	dobyt	k2eAgNnSc1d1	dobyto
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1420	[number]	k4	1420
se	se	k3xPyFc4	se
přidalo	přidat	k5eAaPmAgNnS	přidat
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
katolíků	katolík	k1gMnPc2	katolík
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
německou	německý	k2eAgFnSc7d1	německá
národností	národnost	k1gFnSc7	národnost
většiny	většina	k1gFnSc2	většina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
německý	německý	k2eAgInSc1d1	německý
jazykový	jazykový	k2eAgInSc1d1	jazykový
ostrov	ostrov	k1gInSc1	ostrov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1422	[number]	k4	1422
se	se	k3xPyFc4	se
Jihlava	Jihlava	k1gFnSc1	Jihlava
stala	stát	k5eAaPmAgFnS	stát
přímým	přímý	k2eAgMnSc7d1	přímý
svědkem	svědek	k1gMnSc7	svědek
ústupu	ústup	k1gInSc2	ústup
zbídačených	zbídačený	k2eAgNnPc2d1	zbídačené
Zikmundových	Zikmundových	k2eAgNnPc2d1	Zikmundových
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
prchala	prchat	k5eAaImAgNnP	prchat
po	po	k7c6	po
prohrané	prohraný	k2eAgFnSc6d1	prohraná
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Německého	německý	k2eAgInSc2d1	německý
Brodu	Brod	k1gInSc2	Brod
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přímé	přímý	k2eAgNnSc1d1	přímé
ohrožení	ohrožení	k1gNnSc1	ohrožení
husitskými	husitský	k2eAgNnPc7d1	husitské
vojsky	vojsko	k1gNnPc7	vojsko
zažila	zažít	k5eAaPmAgFnS	zažít
Jihlava	Jihlava	k1gFnSc1	Jihlava
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1423	[number]	k4	1423
<g/>
,	,	kIx,	,
1425	[number]	k4	1425
a	a	k8xC	a
1427	[number]	k4	1427
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
načas	načas	k6eAd1	načas
obléhal	obléhat	k5eAaImAgMnS	obléhat
i	i	k9	i
např.	např.	kA	např.
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
či	či	k8xC	či
Jan	Jan	k1gMnSc1	Jan
Roháč	roháč	k1gMnSc1	roháč
z	z	k7c2	z
Dubé	Dubá	k1gFnSc2	Dubá
<g/>
.	.	kIx.	.
</s>
<s>
Pevnost	pevnost	k1gFnSc1	pevnost
však	však	k9	však
vždy	vždy	k6eAd1	vždy
odolala	odolat	k5eAaPmAgFnS	odolat
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
však	však	k9	však
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
velkým	velký	k2eAgFnPc3d1	velká
hospodářským	hospodářský	k2eAgFnPc3d1	hospodářská
ztrátám	ztráta	k1gFnPc3	ztráta
<g/>
,	,	kIx,	,
široké	široký	k2eAgNnSc1d1	široké
okolí	okolí	k1gNnSc1	okolí
města	město	k1gNnSc2	město
bylo	být	k5eAaImAgNnS	být
zpustošeno	zpustošit	k5eAaPmNgNnS	zpustošit
<g/>
,	,	kIx,	,
těžba	těžba	k1gFnSc1	těžba
se	se	k3xPyFc4	se
zastavila	zastavit	k5eAaPmAgFnS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1436	[number]	k4	1436
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
jihlavském	jihlavský	k2eAgNnSc6d1	jihlavské
náměstí	náměstí	k1gNnSc6	náměstí
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Masarykovo	Masarykův	k2eAgNnSc1d1	Masarykovo
<g/>
)	)	kIx)	)
vyhlášena	vyhlášen	k2eAgNnPc4d1	vyhlášeno
Basilejská	basilejský	k2eAgNnPc4d1	Basilejské
kompaktáta	kompaktáta	k1gNnPc4	kompaktáta
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1441	[number]	k4	1441
měl	mít	k5eAaImAgInS	mít
nastat	nastat	k5eAaPmF	nastat
klid	klid	k1gInSc1	klid
uzavřením	uzavření	k1gNnSc7	uzavření
smíru	smír	k1gInSc2	smír
mezi	mezi	k7c7	mezi
Jihlavou	Jihlava	k1gFnSc7	Jihlava
a	a	k8xC	a
Táborem	Tábor	k1gInSc7	Tábor
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Jiřího	Jiří	k1gMnSc2	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
rozhořely	rozhořet	k5eAaPmAgInP	rozhořet
boje	boj	k1gInPc1	boj
<g/>
.	.	kIx.	.
</s>
<s>
Převážně	převážně	k6eAd1	převážně
německá	německý	k2eAgFnSc1d1	německá
Jihlava	Jihlava	k1gFnSc1	Jihlava
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
uznat	uznat	k5eAaPmF	uznat
jeho	jeho	k3xOp3gFnSc4	jeho
korunovaci	korunovace	k1gFnSc4	korunovace
a	a	k8xC	a
volbu	volba	k1gFnSc4	volba
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
ji	on	k3xPp3gFnSc4	on
musel	muset	k5eAaImAgMnS	muset
husitský	husitský	k2eAgMnSc1d1	husitský
král	král	k1gMnSc1	král
dobýt	dobýt	k5eAaPmF	dobýt
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1458	[number]	k4	1458
po	po	k7c6	po
čtyřměsíčním	čtyřměsíční	k2eAgNnSc6d1	čtyřměsíční
obléhání	obléhání	k1gNnSc6	obléhání
<g/>
.	.	kIx.	.
</s>
<s>
Tuhý	tuhý	k2eAgInSc1d1	tuhý
odpor	odpor	k1gInSc1	odpor
byl	být	k5eAaImAgInS	být
způsoben	způsoben	k2eAgInSc1d1	způsoben
zakořeněným	zakořeněný	k2eAgNnSc7d1	zakořeněné
katolictvím	katolictví	k1gNnSc7	katolictví
a	a	k8xC	a
odporem	odpor	k1gInSc7	odpor
proti	proti	k7c3	proti
husitům	husita	k1gMnPc3	husita
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
boje	boj	k1gInPc1	boj
měly	mít	k5eAaImAgInP	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Jihlava	Jihlava	k1gFnSc1	Jihlava
nemohla	moct	k5eNaImAgFnS	moct
dostat	dostat	k5eAaPmF	dostat
z	z	k7c2	z
krize	krize	k1gFnSc2	krize
<g/>
.	.	kIx.	.
</s>
<s>
Oživení	oživení	k1gNnSc1	oživení
nastalo	nastat	k5eAaPmAgNnS	nastat
až	až	k9	až
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Léta	léto	k1gNnSc2	léto
1526	[number]	k4	1526
<g/>
–	–	k?	–
<g/>
1619	[number]	k4	1619
jsou	být	k5eAaImIp3nP	být
roky	rok	k1gInPc1	rok
velkého	velký	k2eAgInSc2d1	velký
rozkvětu	rozkvět	k1gInSc2	rozkvět
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
velkých	velký	k2eAgInPc6d1	velký
požárech	požár	k1gInPc6	požár
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rekonstrukci	rekonstrukce	k1gFnSc3	rekonstrukce
s	s	k7c7	s
výraznými	výrazný	k2eAgInPc7d1	výrazný
renesančními	renesanční	k2eAgInPc7d1	renesanční
rysy	rys	k1gInPc7	rys
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1522	[number]	k4	1522
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
prosadilo	prosadit	k5eAaPmAgNnS	prosadit
luteránství	luteránství	k1gNnSc1	luteránství
<g/>
.	.	kIx.	.
</s>
<s>
Kvetla	kvést	k5eAaImAgFnS	kvést
řemesla	řemeslo	k1gNnPc1	řemeslo
a	a	k8xC	a
obchod	obchod	k1gInSc1	obchod
–	–	k?	–
oborem	obor	k1gInSc7	obor
středoevropského	středoevropský	k2eAgInSc2d1	středoevropský
významu	význam	k1gInSc2	význam
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
soukenictví	soukenictví	k1gNnSc1	soukenictví
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
opětovně	opětovně	k6eAd1	opětovně
k	k	k7c3	k
řadě	řada	k1gFnSc3	řada
průzkumů	průzkum	k1gInPc2	průzkum
<g/>
,	,	kIx,	,
těžaři	těžař	k1gMnPc1	těžař
obnovili	obnovit	k5eAaPmAgMnP	obnovit
řadu	řada	k1gFnSc4	řada
štol	štola	k1gFnPc2	štola
a	a	k8xC	a
zarazili	zarazit	k5eAaPmAgMnP	zarazit
řadu	řada	k1gFnSc4	řada
nových	nový	k2eAgMnPc2d1	nový
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Jihlava	Jihlava	k1gFnSc1	Jihlava
jako	jako	k8xC	jako
jediné	jediný	k2eAgNnSc1d1	jediné
moravské	moravský	k2eAgNnSc1d1	Moravské
město	město	k1gNnSc1	město
účastnila	účastnit	k5eAaImAgFnS	účastnit
prvního	první	k4xOgInSc2	první
stavovského	stavovský	k2eAgInSc2d1	stavovský
odboje	odboj	k1gInSc2	odboj
1546	[number]	k4	1546
<g/>
/	/	kIx~	/
<g/>
47	[number]	k4	47
a	a	k8xC	a
vyvázla	vyváznout	k5eAaPmAgFnS	vyváznout
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
25	[number]	k4	25
000	[number]	k4	000
tolary	tolar	k1gInPc7	tolar
pokuty	pokuta	k1gFnSc2	pokuta
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
dědičnou	dědičný	k2eAgFnSc7d1	dědičná
pivní	pivní	k2eAgFnSc7d1	pivní
daní	daň	k1gFnSc7	daň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1645	[number]	k4	1645
se	se	k3xPyFc4	se
města	město	k1gNnSc2	město
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
Švédové	Švéd	k1gMnPc1	Švéd
<g/>
,	,	kIx,	,
tak	tak	k9	tak
že	že	k8xS	že
podplatily	podplatit	k5eAaPmAgFnP	podplatit
jednu	jeden	k4xCgFnSc4	jeden
ženu	žena	k1gFnSc4	žena
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
jim	on	k3xPp3gMnPc3	on
otevřela	otevřít	k5eAaPmAgFnS	otevřít
dvě	dva	k4xCgFnPc4	dva
brány	brána	k1gFnPc4	brána
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
Lennart	Lennarta	k1gFnPc2	Lennarta
Torstensona	Torstenson	k1gMnSc2	Torstenson
(	(	kIx(	(
<g/>
1603	[number]	k4	1603
<g/>
–	–	k?	–
<g/>
1651	[number]	k4	1651
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc4	město
přestavěli	přestavět	k5eAaPmAgMnP	přestavět
na	na	k7c4	na
barokní	barokní	k2eAgFnSc4d1	barokní
pevnost	pevnost	k1gFnSc4	pevnost
s	s	k7c7	s
předsunutým	předsunutý	k2eAgNnSc7d1	předsunuté
opevněním	opevnění	k1gNnSc7	opevnění
–	–	k?	–
bastionem	bastion	k1gInSc7	bastion
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jejich	jejich	k3xOp3gInSc6	jejich
odchodu	odchod	k1gInSc6	odchod
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1647	[number]	k4	1647
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
ve	v	k7c6	v
zničeném	zničený	k2eAgNnSc6d1	zničené
městě	město	k1gNnSc6	město
pouhých	pouhý	k2eAgInPc2d1	pouhý
1	[number]	k4	1
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc4d1	velký
rozvoj	rozvoj	k1gInSc4	rozvoj
město	město	k1gNnSc1	město
zažilo	zažít	k5eAaPmAgNnS	zažít
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
druhým	druhý	k4xOgInSc7	druhý
největším	veliký	k2eAgMnSc7d3	veliký
producentem	producent	k1gMnSc7	producent
sukna	sukno	k1gNnSc2	sukno
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
habsburské	habsburský	k2eAgFnSc6d1	habsburská
monarchii	monarchie	k1gFnSc6	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
také	také	k9	také
k	k	k7c3	k
bourání	bourání	k1gNnSc3	bourání
částí	část	k1gFnPc2	část
hradeb	hradba	k1gFnPc2	hradba
a	a	k8xC	a
bran	brána	k1gFnPc2	brána
s	s	k7c7	s
úzkými	úzký	k2eAgInPc7d1	úzký
průjezdy	průjezd	k1gInPc7	průjezd
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
zřízen	zřídit	k5eAaPmNgInS	zřídit
krajský	krajský	k2eAgInSc1d1	krajský
soud	soud	k1gInSc1	soud
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1864	[number]	k4	1864
<g/>
–	–	k?	–
<g/>
1928	[number]	k4	1928
byla	být	k5eAaImAgFnS	být
Jihlava	Jihlava	k1gFnSc1	Jihlava
poprvé	poprvé	k6eAd1	poprvé
statutárním	statutární	k2eAgNnSc7d1	statutární
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
Jihlavě	Jihlava	k1gFnSc3	Jihlava
připojena	připojen	k2eAgFnSc1d1	připojena
obec	obec	k1gFnSc1	obec
Dřevěné	dřevěný	k2eAgInPc1d1	dřevěný
mlýny	mlýn	k1gInPc1	mlýn
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
tvořila	tvořit	k5eAaImAgFnS	tvořit
Jihlava	Jihlava	k1gFnSc1	Jihlava
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
okolím	okolí	k1gNnSc7	okolí
druhou	druhý	k4xOgFnSc4	druhý
největší	veliký	k2eAgFnSc4d3	veliký
německou	německý	k2eAgFnSc4d1	německá
jazykovou	jazykový	k2eAgFnSc4d1	jazyková
enklávu	enkláva	k1gFnSc4	enkláva
na	na	k7c6	na
území	území	k1gNnSc6	území
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
obohatil	obohatit	k5eAaPmAgMnS	obohatit
město	město	k1gNnSc4	město
o	o	k7c4	o
díla	dílo	k1gNnPc4	dílo
české	český	k2eAgFnSc2d1	Česká
moderní	moderní	k2eAgFnSc2d1	moderní
architektury	architektura	k1gFnSc2	architektura
její	její	k3xOp3gMnSc1	její
čelní	čelní	k2eAgMnSc1d1	čelní
představitel	představitel	k1gMnSc1	představitel
<g/>
,	,	kIx,	,
mezinárodně	mezinárodně	k6eAd1	mezinárodně
uznávaný	uznávaný	k2eAgMnSc1d1	uznávaný
architekt	architekt	k1gMnSc1	architekt
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Fuchs	Fuchs	k1gMnSc1	Fuchs
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
také	také	k9	také
Jihlavu	Jihlava	k1gFnSc4	Jihlava
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
dnů	den	k1gInPc2	den
po	po	k7c6	po
obsazení	obsazení	k1gNnSc6	obsazení
Němci	Němec	k1gMnPc7	Němec
byla	být	k5eAaImAgFnS	být
vypálena	vypálen	k2eAgFnSc1d1	vypálena
synagoga	synagoga	k1gFnSc1	synagoga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
10	[number]	k4	10
<g/>
.	.	kIx.	.
na	na	k7c4	na
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
byl	být	k5eAaImAgInS	být
partyzány	partyzána	k1gFnSc2	partyzána
poškozen	poškozen	k2eAgInSc1d1	poškozen
most	most	k1gInSc1	most
u	u	k7c2	u
Helenína	Helenín	k1gInSc2	Helenín
a	a	k8xC	a
při	při	k7c6	při
přejezdu	přejezd	k1gInSc6	přejezd
vlaku	vlak	k1gInSc2	vlak
se	se	k3xPyFc4	se
převrátil	převrátit	k5eAaPmAgMnS	převrátit
<g/>
.	.	kIx.	.
</s>
<s>
Akce	akce	k1gFnSc1	akce
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
amatérského	amatérský	k2eAgMnSc2d1	amatérský
historika	historik	k1gMnSc2	historik
Jiřího	Jiří	k1gMnSc2	Jiří
Vybíhala	vybíhat	k5eAaImAgFnS	vybíhat
hodnocena	hodnotit	k5eAaImNgFnS	hodnotit
jako	jako	k9	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
železničních	železniční	k2eAgFnPc2d1	železniční
diverzních	diverzní	k2eAgFnPc2d1	diverzní
akcí	akce	k1gFnPc2	akce
partyzánů	partyzán	k1gMnPc2	partyzán
v	v	k7c6	v
období	období	k1gNnSc6	období
Protektorátu	protektorát	k1gInSc6	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgNnPc4d1	německé
hlášení	hlášení	k1gNnPc4	hlášení
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
smrti	smrt	k1gFnSc6	smrt
65	[number]	k4	65
a	a	k8xC	a
zranění	zranění	k1gNnSc4	zranění
124	[number]	k4	124
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
se	se	k3xPyFc4	se
v	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
normalizaci	normalizace	k1gFnSc3	normalizace
upálil	upálit	k5eAaPmAgMnS	upálit
Evžen	Evžen	k1gMnSc1	Evžen
Plocek	Plocka	k1gFnPc2	Plocka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
u	u	k7c2	u
morového	morový	k2eAgInSc2d1	morový
sloupu	sloup	k1gInSc2	sloup
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
Masarykova	Masarykův	k2eAgNnSc2d1	Masarykovo
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Zrušení	zrušení	k1gNnSc1	zrušení
zemského	zemský	k2eAgNnSc2d1	zemské
uspořádání	uspořádání	k1gNnSc2	uspořádání
a	a	k8xC	a
nové	nový	k2eAgNnSc4d1	nové
krajské	krajský	k2eAgNnSc4d1	krajské
zřízení	zřízení	k1gNnSc4	zřízení
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
přineslo	přinést	k5eAaPmAgNnS	přinést
Jihlavě	Jihlava	k1gFnSc6	Jihlava
status	status	k1gInSc4	status
krajského	krajský	k2eAgNnSc2d1	krajské
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
centra	centrum	k1gNnSc2	centrum
Jihlavského	jihlavský	k2eAgInSc2d1	jihlavský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
správní	správní	k2eAgFnSc7d1	správní
reformou	reforma	k1gFnSc7	reforma
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
však	však	k9	však
byla	být	k5eAaImAgFnS	být
Jihlava	Jihlava	k1gFnSc1	Jihlava
přičleněna	přičleněn	k2eAgFnSc1d1	přičleněna
k	k	k7c3	k
Jihomoravskému	jihomoravský	k2eAgInSc3d1	jihomoravský
kraji	kraj	k1gInSc3	kraj
s	s	k7c7	s
centrem	centrum	k1gNnSc7	centrum
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
zůstala	zůstat	k5eAaPmAgFnS	zůstat
pouze	pouze	k6eAd1	pouze
městem	město	k1gNnSc7	město
okresním	okresní	k2eAgNnSc7d1	okresní
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc6	leden
1951	[number]	k4	1951
se	se	k3xPyFc4	se
Jihlava	Jihlava	k1gFnSc1	Jihlava
dočkala	dočkat	k5eAaPmAgFnS	dočkat
rozšíření	rozšíření	k1gNnSc4	rozšíření
svého	svůj	k3xOyFgNnSc2	svůj
území	území	k1gNnSc2	území
o	o	k7c4	o
Bedřichov	Bedřichov	k1gInSc4	Bedřichov
<g/>
,	,	kIx,	,
Helenín	Helenín	k1gInSc4	Helenín
<g/>
,	,	kIx,	,
Hruškové	Hruškové	k2eAgInPc1d1	Hruškové
Dvory	Dvůr	k1gInPc1	Dvůr
<g/>
,	,	kIx,	,
Staré	Staré	k2eAgInPc1d1	Staré
Hory	hora	k1gFnPc4	hora
a	a	k8xC	a
Pančavu	pančava	k1gFnSc4	pančava
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Bedřichov	Bedřichov	k1gInSc1	Bedřichov
a	a	k8xC	a
Staré	Staré	k2eAgFnPc1d1	Staré
Hory	hora	k1gFnPc1	hora
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Velká	velký	k2eAgFnSc1d1	velká
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
17	[number]	k4	17
<g/>
.	.	kIx.	.
květnu	květen	k1gInSc6	květen
1954	[number]	k4	1954
byly	být	k5eAaImAgInP	být
Hruškové	Hruškové	k2eAgInPc1d1	Hruškové
Dvory	Dvůr	k1gInPc1	Dvůr
od	od	k7c2	od
Jihlavy	Jihlava	k1gFnSc2	Jihlava
odděleny	oddělen	k2eAgFnPc1d1	oddělena
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
Jihlavě	Jihlava	k1gFnSc3	Jihlava
připojena	připojen	k2eAgFnSc1d1	připojena
osada	osada	k1gFnSc1	osada
Sasov	Sasov	k1gInSc1	Sasov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
se	se	k3xPyFc4	se
Jihlava	Jihlava	k1gFnSc1	Jihlava
stala	stát	k5eAaPmAgFnS	stát
okresním	okresní	k2eAgNnSc7d1	okresní
městem	město	k1gNnSc7	město
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
s	s	k7c7	s
krajským	krajský	k2eAgNnSc7d1	krajské
městem	město	k1gNnSc7	město
Brnem	Brno	k1gNnSc7	Brno
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc6	leden
1968	[number]	k4	1968
byla	být	k5eAaImAgFnS	být
Jihlava	Jihlava	k1gFnSc1	Jihlava
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
o	o	k7c4	o
obec	obec	k1gFnSc4	obec
Pávov	Pávovo	k1gNnPc2	Pávovo
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
rozšíření	rozšíření	k1gNnSc1	rozšíření
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpnu	srpen	k1gInSc3	srpen
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
k	k	k7c3	k
Jihlavě	Jihlava	k1gFnSc3	Jihlava
připojeny	připojit	k5eAaPmNgFnP	připojit
Antonínův	Antonínův	k2eAgInSc1d1	Antonínův
Důl	důl	k1gInSc1	důl
<g/>
,	,	kIx,	,
Červený	červený	k2eAgInSc1d1	červený
Kříž	kříž	k1gInSc1	kříž
<g/>
,	,	kIx,	,
Henčov	Henčov	k1gInSc1	Henčov
<g/>
,	,	kIx,	,
Heroltice	Heroltice	k1gFnPc1	Heroltice
<g/>
,	,	kIx,	,
Hruškové	Hruškové	k2eAgInPc1d1	Hruškové
Dvory	Dvůr	k1gInPc1	Dvůr
<g/>
,	,	kIx,	,
Hybrálec	Hybrálec	k1gInSc1	Hybrálec
<g/>
,	,	kIx,	,
Pístov	Pístov	k1gInSc1	Pístov
<g/>
,	,	kIx,	,
Popice	Popice	k1gFnSc1	Popice
<g/>
,	,	kIx,	,
Rančířov	Rančířov	k1gInSc1	Rančířov
<g/>
,	,	kIx,	,
Smrčná	Smrčný	k2eAgFnSc1d1	Smrčná
<g/>
,	,	kIx,	,
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
a	a	k8xC	a
Zborná	zborný	k2eAgFnSc1d1	Zborná
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubnu	duben	k1gInSc3	duben
1980	[number]	k4	1980
byly	být	k5eAaImAgFnP	být
k	k	k7c3	k
Jihlavě	Jihlava	k1gFnSc3	Jihlava
připojeny	připojit	k5eAaPmNgInP	připojit
Horní	horní	k2eAgInPc1d1	horní
Kosov	Kosov	k1gInSc1	Kosov
<g/>
,	,	kIx,	,
Hosov	Hosov	k1gInSc1	Hosov
<g/>
,	,	kIx,	,
Malý	malý	k2eAgInSc1d1	malý
Beranov	Beranov	k1gInSc1	Beranov
a	a	k8xC	a
Kosov	Kosov	k1gInSc1	Kosov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
historickém	historický	k2eAgNnSc6d1	historické
jádru	jádro	k1gNnSc6	jádro
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
městskou	městský	k2eAgFnSc7d1	městská
památkovou	památkový	k2eAgFnSc7d1	památková
rezervací	rezervace	k1gFnSc7	rezervace
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
domy	dům	k1gInPc4	dům
mnoha	mnoho	k4c2	mnoho
historických	historický	k2eAgInPc2d1	historický
slohů	sloh	k1gInPc2	sloh
a	a	k8xC	a
velké	velký	k2eAgInPc1d1	velký
zbytky	zbytek	k1gInPc1	zbytek
městských	městský	k2eAgFnPc2d1	městská
hradeb	hradba	k1gFnPc2	hradba
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
1989	[number]	k4	1989
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zatím	zatím	k6eAd1	zatím
poslednímu	poslední	k2eAgNnSc3d1	poslední
rozšíření	rozšíření	k1gNnSc3	rozšíření
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
byly	být	k5eAaImAgFnP	být
připojeny	připojen	k2eAgInPc1d1	připojen
Rantířov	Rantířov	k1gInSc1	Rantířov
<g/>
,	,	kIx,	,
Měšín	Měšín	k1gInSc1	Měšín
<g/>
,	,	kIx,	,
Cerekvička	Cerekvička	k1gFnSc1	Cerekvička
<g/>
,	,	kIx,	,
Loučky	loučka	k1gFnPc1	loučka
<g/>
,	,	kIx,	,
Vílanec	Vílanec	k1gInSc1	Vílanec
<g/>
,	,	kIx,	,
Čížov	Čížov	k1gInSc1	Čížov
<g/>
,	,	kIx,	,
Rosice	Rosice	k1gFnPc1	Rosice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
1990	[number]	k4	1990
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
řada	řada	k1gFnSc1	řada
částí	část	k1gFnPc2	část
Jihlavy	Jihlava	k1gFnSc2	Jihlava
osamostatnila	osamostatnit	k5eAaPmAgFnS	osamostatnit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
je	být	k5eAaImIp3nS	být
Jihlava	Jihlava	k1gFnSc1	Jihlava
znovu	znovu	k6eAd1	znovu
centrem	centrum	k1gNnSc7	centrum
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
nicméně	nicméně	k8xC	nicméně
z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
staronového	staronový	k2eAgInSc2d1	staronový
názvu	název	k1gInSc2	název
Jihlavský	jihlavský	k2eAgInSc1d1	jihlavský
kraj	kraj	k1gInSc1	kraj
brzy	brzy	k6eAd1	brzy
přejmenoval	přejmenovat	k5eAaPmAgInS	přejmenovat
na	na	k7c4	na
Kraj	kraj	k1gInSc4	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
tak	tak	k9	tak
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Karlovými	Karlův	k2eAgInPc7d1	Karlův
Vary	Vary	k1gInPc7	Vary
mezi	mezi	k7c4	mezi
zdaleka	zdaleka	k6eAd1	zdaleka
nejmenší	malý	k2eAgNnPc4d3	nejmenší
česká	český	k2eAgNnPc4d1	české
krajská	krajský	k2eAgNnPc4d1	krajské
města	město	k1gNnPc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Jihlava	Jihlava	k1gFnSc1	Jihlava
leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
stejnojmenném	stejnojmenný	k2eAgInSc6d1	stejnojmenný
okrese	okres	k1gInSc6	okres
v	v	k7c6	v
Kraji	kraj	k1gInSc6	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
<g/>
,	,	kIx,	,
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vzdáleno	vzdálit	k5eAaPmNgNnS	vzdálit
21	[number]	k4	21
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Havlíčkova	Havlíčkův	k2eAgInSc2d1	Havlíčkův
Brodu	Brod	k1gInSc2	Brod
<g/>
,	,	kIx,	,
27	[number]	k4	27
km	km	kA	km
východně	východně	k6eAd1	východně
od	od	k7c2	od
Pelhřimova	Pelhřimov	k1gInSc2	Pelhřimov
<g/>
,	,	kIx,	,
30	[number]	k4	30
km	km	kA	km
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Třebíče	Třebíč	k1gFnSc2	Třebíč
<g/>
,	,	kIx,	,
115	[number]	k4	115
km	km	kA	km
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
78	[number]	k4	78
km	km	kA	km
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
geologického	geologický	k2eAgNnSc2d1	geologické
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
území	území	k1gNnSc4	území
součástí	součást	k1gFnPc2	součást
Českého	český	k2eAgInSc2d1	český
masivu	masiv	k1gInSc2	masiv
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hornin	hornina	k1gFnPc2	hornina
převažuje	převažovat	k5eAaImIp3nS	převažovat
migmatit	migmatit	k5eAaPmF	migmatit
<g/>
,	,	kIx,	,
sprašová	sprašový	k2eAgFnSc1d1	sprašová
hlína	hlína	k1gFnSc1	hlína
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
Heroltic	Heroltice	k1gFnPc2	Heroltice
a	a	k8xC	a
v	v	k7c6	v
pásu	pás	k1gInSc6	pás
Hosova	Hosovo	k1gNnSc2	Hosovo
<g/>
,	,	kIx,	,
Vysoké	vysoká	k1gFnSc2	vysoká
a	a	k8xC	a
Popic	Popice	k1gFnPc2	Popice
<g/>
,	,	kIx,	,
pararula	pararula	k1gFnSc1	pararula
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Zborné	zborný	k2eAgFnPc1d1	Zborná
<g/>
,	,	kIx,	,
místy	místo	k1gNnPc7	místo
granit	granit	k1gInSc1	granit
a	a	k8xC	a
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
řek	řeka	k1gFnPc2	řeka
hlína	hlína	k1gFnSc1	hlína
<g/>
,	,	kIx,	,
písek	písek	k1gInSc1	písek
a	a	k8xC	a
štěrk	štěrk	k1gInSc1	štěrk
<g/>
.	.	kIx.	.
</s>
<s>
Geomorfologicky	geomorfologicky	k6eAd1	geomorfologicky
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc1	oblast
součástí	součást	k1gFnPc2	součást
Česko-moravské	českooravský	k2eAgFnSc2d1	česko-moravská
subprovincie	subprovincie	k1gFnSc2	subprovincie
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
Hornosázavské	Hornosázavský	k2eAgFnSc2d1	Hornosázavská
pahorkatiny	pahorkatina	k1gFnSc2	pahorkatina
<g/>
,	,	kIx,	,
Křižanovské	Křižanovský	k2eAgFnSc2d1	Křižanovská
vrchoviny	vrchovina	k1gFnSc2	vrchovina
a	a	k8xC	a
Křemešnické	Křemešnický	k2eAgFnSc2d1	Křemešnická
vrchoviny	vrchovina	k1gFnSc2	vrchovina
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc2	jejich
podcelků	podcelek	k1gInPc2	podcelek
Jihlavsko-sázavské	jihlavskoázavský	k2eAgFnSc2d1	jihlavsko-sázavský
brázdy	brázda	k1gFnSc2	brázda
<g/>
,	,	kIx,	,
Brtnické	brtnický	k2eAgFnSc2d1	Brtnická
vrchoviny	vrchovina	k1gFnSc2	vrchovina
a	a	k8xC	a
Humpolecké	humpolecký	k2eAgFnSc2d1	Humpolecká
vrchoviny	vrchovina	k1gFnSc2	vrchovina
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c4	na
vícero	vícero	k1gNnSc4	vícero
geomorfologických	geomorfologický	k2eAgInPc6d1	geomorfologický
okrscích	okrsek	k1gInPc6	okrsek
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Jihlavské	jihlavský	k2eAgFnSc6d1	Jihlavská
kotlině	kotlina	k1gFnSc6	kotlina
<g/>
,	,	kIx,	,
severovýchodní	severovýchodní	k2eAgFnSc4d1	severovýchodní
část	část	k1gFnSc4	část
pak	pak	k6eAd1	pak
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
Štocký	Štocký	k2eAgInSc4d1	Štocký
stupeň	stupeň	k1gInSc4	stupeň
<g/>
,	,	kIx,	,
Dobronínská	Dobronínský	k2eAgFnSc1d1	Dobronínský
pánev	pánev	k1gFnSc1	pánev
a	a	k8xC	a
Beranovský	Beranovský	k2eAgInSc1d1	Beranovský
práh	práh	k1gInSc1	práh
<g/>
.	.	kIx.	.
</s>
<s>
Severozápad	severozápad	k1gInSc1	severozápad
tvoří	tvořit	k5eAaImIp3nS	tvořit
Jeníkovská	Jeníkovský	k2eAgFnSc1d1	Jeníkovská
vrchovina	vrchovina	k1gFnSc1	vrchovina
<g/>
,	,	kIx,	,
jihozápadně	jihozápadně	k6eAd1	jihozápadně
pak	pak	k6eAd1	pak
Kosovská	kosovský	k2eAgFnSc1d1	Kosovská
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
a	a	k8xC	a
Puklická	Puklický	k2eAgFnSc1d1	Puklická
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
činí	činit	k5eAaImIp3nS	činit
525	[number]	k4	525
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
<g/>
,	,	kIx,	,
Popický	Popický	k2eAgInSc1d1	Popický
vrch	vrch	k1gInSc1	vrch
(	(	kIx(	(
<g/>
682	[number]	k4	682
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
známé	známý	k2eAgInPc4d1	známý
vrcholy	vrchol	k1gInPc4	vrchol
patří	patřit	k5eAaImIp3nS	patřit
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
(	(	kIx(	(
<g/>
665	[number]	k4	665
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
místní	místní	k2eAgFnSc2d1	místní
části	část	k1gFnSc2	část
Vysoká	vysoká	k1gFnSc1	vysoká
<g/>
,	,	kIx,	,
Rudný	rudný	k2eAgMnSc1d1	rudný
(	(	kIx(	(
<g/>
613	[number]	k4	613
m	m	kA	m
<g/>
)	)	kIx)	)
ve	v	k7c6	v
Zborné	zborný	k2eAgFnSc6d1	Zborná
<g/>
,	,	kIx,	,
Hůrka	hůrka	k1gFnSc1	hůrka
(	(	kIx(	(
<g/>
622	[number]	k4	622
m	m	kA	m
<g/>
)	)	kIx)	)
u	u	k7c2	u
Vysoké	vysoká	k1gFnSc2	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Nejnižší	nízký	k2eAgInSc1d3	nejnižší
bod	bod	k1gInSc1	bod
v	v	k7c6	v
Heleníně	Helenína	k1gFnSc6	Helenína
u	u	k7c2	u
řeky	řeka	k1gFnSc2	řeka
Jihlavy	Jihlava	k1gFnSc2	Jihlava
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
469	[number]	k4	469
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
převažuje	převažovat	k5eAaImIp3nS	převažovat
kambizem	kambiz	k1gInSc7	kambiz
<g/>
,	,	kIx,	,
okolo	okolo	k7c2	okolo
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
fluvizemě	fluvizemě	k6eAd1	fluvizemě
a	a	k8xC	a
gleje	gleje	k1gFnSc1	gleje
<g/>
,	,	kIx,	,
západně	západně	k6eAd1	západně
od	od	k7c2	od
Vysoké	vysoká	k1gFnSc2	vysoká
<g/>
,	,	kIx,	,
severně	severně	k6eAd1	severně
od	od	k7c2	od
Henčova	Henčův	k2eAgInSc2d1	Henčův
a	a	k8xC	a
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
Heroltic	Heroltice	k1gFnPc2	Heroltice
<g/>
,	,	kIx,	,
Bedřichova	Bedřichův	k2eAgInSc2d1	Bedřichův
<g/>
,	,	kIx,	,
Pávova	pávův	k2eAgInSc2d1	pávův
a	a	k8xC	a
Červeného	Červeného	k2eAgInSc2d1	Červeného
Kříže	kříž	k1gInSc2	kříž
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
pseudogleje	pseudoglet	k5eAaImSgMnS	pseudoglet
<g/>
.	.	kIx.	.
</s>
<s>
Jihlava	Jihlava	k1gFnSc1	Jihlava
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
stejnojmenné	stejnojmenný	k2eAgFnSc6d1	stejnojmenná
řece	řeka	k1gFnSc6	řeka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
Jihlavy	Jihlava	k1gFnSc2	Jihlava
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
vlévají	vlévat	k5eAaImIp3nP	vlévat
Smrčenský	Smrčenský	k2eAgInSc4d1	Smrčenský
potok	potok	k1gInSc4	potok
ve	v	k7c6	v
Starých	Starých	k2eAgFnPc6d1	Starých
Horách	hora	k1gFnPc6	hora
<g/>
,	,	kIx,	,
řeka	řeka	k1gFnSc1	řeka
Jihlávka	Jihlávka	k1gFnSc1	Jihlávka
v	v	k7c4	v
ulici	ulice	k1gFnSc4	ulice
Mlýnská	mlýnský	k2eAgFnSc1d1	Mlýnská
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Hradební	hradební	k2eAgFnSc6d1	hradební
vlévá	vlévat	k5eAaImIp3nS	vlévat
Koželužský	koželužský	k2eAgInSc1d1	koželužský
potok	potok	k1gInSc1	potok
<g/>
.	.	kIx.	.
</s>
<s>
Místními	místní	k2eAgFnPc7d1	místní
částmi	část	k1gFnPc7	část
na	na	k7c6	na
severu	sever	k1gInSc6	sever
území	území	k1gNnSc2	území
města	město	k1gNnSc2	město
–	–	k?	–
Pávov	Pávov	k1gInSc1	Pávov
<g/>
,	,	kIx,	,
Zborná	zborný	k2eAgFnSc1d1	Zborná
<g/>
,	,	kIx,	,
Henčov	Henčov	k1gInSc1	Henčov
<g/>
,	,	kIx,	,
Antonínův	Antonínův	k2eAgInSc1d1	Antonínův
Důl	důl	k1gInSc1	důl
a	a	k8xC	a
Červený	červený	k2eAgInSc1d1	červený
Kříž	kříž	k1gInSc1	kříž
protéká	protékat	k5eAaImIp3nS	protékat
Zlatý	zlatý	k2eAgInSc1d1	zlatý
potok	potok	k1gInSc1	potok
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
města	město	k1gNnSc2	město
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
evropském	evropský	k2eAgNnSc6d1	Evropské
rozvodí	rozvodí	k1gNnSc6	rozvodí
mezi	mezi	k7c7	mezi
úmořím	úmoří	k1gNnSc7	úmoří
Černého	černé	k1gNnSc2	černé
a	a	k8xC	a
Severního	severní	k2eAgNnSc2d1	severní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
soustava	soustava	k1gFnSc1	soustava
rybníků	rybník	k1gInPc2	rybník
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
místních	místní	k2eAgFnPc6d1	místní
částech	část	k1gFnPc6	část
Vysoká	vysoká	k1gFnSc1	vysoká
a	a	k8xC	a
Pístov	Pístov	k1gInSc1	Pístov
na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
toku	tok	k1gInSc6	tok
Koželužského	koželužský	k2eAgInSc2d1	koželužský
potoka	potok	k1gInSc2	potok
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
Lužný	Lužný	k2eAgInSc4d1	Lužný
rybník	rybník	k1gInSc4	rybník
<g/>
,	,	kIx,	,
Silniční	silniční	k2eAgInSc1d1	silniční
rybník	rybník	k1gInSc1	rybník
<g/>
,	,	kIx,	,
Lukáš	Lukáš	k1gMnSc1	Lukáš
<g/>
,	,	kIx,	,
Vodárenský	vodárenský	k2eAgInSc4d1	vodárenský
rybník	rybník	k1gInSc4	rybník
a	a	k8xC	a
Luční	luční	k2eAgInSc4d1	luční
rybník	rybník	k1gInSc4	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Rybníky	rybník	k1gInPc1	rybník
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
výměru	výměra	k1gFnSc4	výměra
od	od	k7c2	od
0,5	[number]	k4	0,5
do	do	k7c2	do
11	[number]	k4	11
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
S.	S.	kA	S.
K.	K.	kA	K.
Neumanna	Neumann	k1gMnSc2	Neumann
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Panský	panský	k2eAgInSc1d1	panský
rybník	rybník	k1gInSc1	rybník
<g/>
,	,	kIx,	,
v	v	k7c6	v
Horním	horní	k2eAgNnSc6d1	horní
Kosově	Kosův	k2eAgNnSc6d1	Kosovo
rybník	rybník	k1gInSc4	rybník
U	u	k7c2	u
Břízy	bříza	k1gFnSc2	bříza
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Smrčenském	Smrčenský	k2eAgInSc6d1	Smrčenský
potoce	potok	k1gInSc6	potok
leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
Starých	Starých	k2eAgFnPc6d1	Starých
Horách	hora	k1gFnPc6	hora
rybník	rybník	k1gInSc4	rybník
Borovinka	borovinka	k1gFnSc1	borovinka
se	s	k7c7	s
sádkami	sádka	k1gFnPc7	sádka
a	a	k8xC	a
Kněžský	kněžský	k2eAgInSc1d1	kněžský
rybník	rybník	k1gInSc1	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
Zbornou	zborný	k2eAgFnSc7d1	Zborná
a	a	k8xC	a
Pávovem	Pávovo	k1gNnSc7	Pávovo
na	na	k7c6	na
Zlatém	zlatý	k2eAgInSc6d1	zlatý
potoce	potok	k1gInSc6	potok
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
několik	několik	k4yIc4	několik
rybníků	rybník	k1gInPc2	rybník
–	–	k?	–
Široký	široký	k2eAgInSc1d1	široký
<g/>
,	,	kIx,	,
Dubový	dubový	k2eAgInSc1d1	dubový
a	a	k8xC	a
Pávovský	Pávovský	k2eAgInSc1d1	Pávovský
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Jihlávce	Jihlávka	k1gFnSc6	Jihlávka
stojí	stát	k5eAaImIp3nS	stát
rybník	rybník	k1gInSc1	rybník
Stará	starý	k2eAgFnSc1d1	stará
plovárna	plovárna	k1gFnSc1	plovárna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Quittovy	Quittův	k2eAgFnSc2d1	Quittova
klimatické	klimatický	k2eAgFnSc2d1	klimatická
klasifikace	klasifikace	k1gFnSc2	klasifikace
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
do	do	k7c2	do
mírně	mírně	k6eAd1	mírně
teplé	teplý	k2eAgFnSc2d1	teplá
oblasti	oblast	k1gFnSc2	oblast
MT	MT	kA	MT
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Atlasu	Atlas	k1gInSc2	Atlas
podnebí	podnebí	k1gNnSc2	podnebí
ČSR	ČSR	kA	ČSR
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
do	do	k7c2	do
okrsku	okrsek	k1gInSc2	okrsek
B5	B5	k1gFnSc2	B5
a	a	k8xC	a
dle	dle	k7c2	dle
Köppenovy	Köppenův	k2eAgFnSc2d1	Köppenova
klasifikace	klasifikace	k1gFnSc2	klasifikace
podnebí	podnebí	k1gNnSc2	podnebí
k	k	k7c3	k
vlhkému	vlhký	k2eAgNnSc3d1	vlhké
kontinentálnímu	kontinentální	k2eAgNnSc3d1	kontinentální
podnebí	podnebí	k1gNnSc3	podnebí
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
oblast	oblast	k1gFnSc4	oblast
MT4	MT4	k1gFnPc2	MT4
jsou	být	k5eAaImIp3nP	být
charakteristická	charakteristický	k2eAgNnPc4d1	charakteristické
krátká	krátký	k2eAgNnPc4d1	krátké
léta	léto	k1gNnPc4	léto
s	s	k7c7	s
mírným	mírný	k2eAgInSc7d1	mírný
<g/>
,	,	kIx,	,
suchým	suchý	k2eAgInSc7d1	suchý
až	až	k6eAd1	až
mírně	mírně	k6eAd1	mírně
suchým	suchý	k2eAgNnSc7d1	suché
<g/>
,	,	kIx,	,
krátkým	krátký	k2eAgNnSc7d1	krátké
přechodným	přechodný	k2eAgNnSc7d1	přechodné
obdobím	období	k1gNnSc7	období
<g/>
,	,	kIx,	,
mírná	mírný	k2eAgNnPc4d1	mírné
jara	jaro	k1gNnPc4	jaro
a	a	k8xC	a
podzimy	podzim	k1gInPc4	podzim
<g/>
.	.	kIx.	.
</s>
<s>
Zimy	zima	k1gFnPc1	zima
jsou	být	k5eAaImIp3nP	být
normálně	normálně	k6eAd1	normálně
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
,	,	kIx,	,
mírně	mírně	k6eAd1	mírně
teplé	teplý	k2eAgInPc1d1	teplý
a	a	k8xC	a
suché	suchý	k2eAgInPc1d1	suchý
<g/>
,	,	kIx,	,
sněhová	sněhový	k2eAgFnSc1d1	sněhová
pokrývka	pokrývka	k1gFnSc1	pokrývka
má	mít	k5eAaImIp3nS	mít
během	během	k7c2	během
nich	on	k3xPp3gMnPc2	on
krátké	krátká	k1gFnSc2	krátká
trvání	trvání	k1gNnSc2	trvání
<g/>
.	.	kIx.	.
</s>
<s>
Platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
průměrné	průměrný	k2eAgFnPc1d1	průměrná
lednové	lednový	k2eAgFnPc1d1	lednová
teploty	teplota	k1gFnPc1	teplota
–	–	k?	–
<g/>
2	[number]	k4	2
až	až	k8xS	až
–	–	k?	–
<g/>
3	[number]	k4	3
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
průměrné	průměrný	k2eAgFnPc1d1	průměrná
červencové	červencový	k2eAgFnPc1d1	červencová
teploty	teplota	k1gFnPc1	teplota
16	[number]	k4	16
až	až	k9	až
17	[number]	k4	17
°	°	k?	°
<g/>
C.	C.	kA	C.
Ve	v	k7c6	v
vegetačním	vegetační	k2eAgNnSc6d1	vegetační
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
350	[number]	k4	350
a	a	k8xC	a
450	[number]	k4	450
mm	mm	kA	mm
<g/>
,	,	kIx,	,
v	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
pak	pak	k9	pak
mezi	mezi	k7c4	mezi
250	[number]	k4	250
a	a	k8xC	a
300	[number]	k4	300
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
činí	činit	k5eAaImIp3nS	činit
6-7	[number]	k4	6-7
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
průměrné	průměrný	k2eAgFnSc2d1	průměrná
srážkový	srážkový	k2eAgInSc4d1	srážkový
úhrn	úhrn	k1gInSc4	úhrn
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
hodnoty	hodnota	k1gFnSc2	hodnota
617	[number]	k4	617
mm	mm	kA	mm
<g/>
/	/	kIx~	/
<g/>
rok	rok	k1gInSc1	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
biogeografického	biogeografický	k2eAgNnSc2d1	Biogeografické
členění	členění	k1gNnSc2	členění
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
k	k	k7c3	k
provincii	provincie	k1gFnSc3	provincie
středoevropských	středoevropský	k2eAgInPc2d1	středoevropský
listnatých	listnatý	k2eAgInPc2d1	listnatý
lesů	les	k1gInPc2	les
<g/>
,	,	kIx,	,
Hercynské	hercynský	k2eAgFnSc6d1	hercynská
podprovincii	podprovincie	k1gFnSc6	podprovincie
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
k	k	k7c3	k
Pelhřimovskému	pelhřimovský	k2eAgNnSc3d1	pelhřimovské
a	a	k8xC	a
východní	východní	k2eAgFnSc1d1	východní
k	k	k7c3	k
Velkomeziříčskému	Velkomeziříčský	k2eAgInSc3d1	Velkomeziříčský
bioregionu	bioregion	k1gInSc3	bioregion
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
ve	v	k7c6	v
fytogeografickém	fytogeografický	k2eAgInSc6d1	fytogeografický
obvodu	obvod	k1gInSc6	obvod
Českomoravského	českomoravský	k2eAgNnSc2d1	Českomoravské
mezofytika	mezofytikum	k1gNnSc2	mezofytikum
<g/>
,	,	kIx,	,
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
67	[number]	k4	67
Českomoravská	českomoravský	k2eAgFnSc1d1	Českomoravská
vrchovina	vrchovina	k1gFnSc1	vrchovina
<g/>
.	.	kIx.	.
</s>
<s>
Lesy	les	k1gInPc1	les
na	na	k7c4	na
území	území	k1gNnSc4	území
Jihlavy	Jihlava	k1gFnSc2	Jihlava
náleží	náležet	k5eAaImIp3nS	náležet
do	do	k7c2	do
přírodní	přírodní	k2eAgFnSc2d1	přírodní
lesní	lesní	k2eAgFnSc2d1	lesní
oblasti	oblast	k1gFnSc2	oblast
16	[number]	k4	16
–	–	k?	–
Českomoravská	českomoravský	k2eAgFnSc1d1	Českomoravská
vrchovina	vrchovina	k1gFnSc1	vrchovina
<g/>
.	.	kIx.	.
</s>
<s>
Jehličnaté	jehličnatý	k2eAgInPc1d1	jehličnatý
lesy	les	k1gInPc1	les
tvoří	tvořit	k5eAaImIp3nP	tvořit
především	především	k9	především
smrková	smrkový	k2eAgFnSc1d1	smrková
monokultura	monokultura	k1gFnSc1	monokultura
<g/>
,	,	kIx,	,
původní	původní	k2eAgMnPc1d1	původní
jedlobukové	jedlobuk	k1gMnPc1	jedlobuk
a	a	k8xC	a
smrkobukové	smrkobukový	k2eAgInPc1d1	smrkobukový
porosty	porost	k1gInPc1	porost
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
u	u	k7c2	u
Vysokého	vysoký	k2eAgInSc2d1	vysoký
kamene	kámen	k1gInSc2	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Potenciálně	potenciálně	k6eAd1	potenciálně
přirozenou	přirozený	k2eAgFnSc7d1	přirozená
vegetací	vegetace	k1gFnSc7	vegetace
je	být	k5eAaImIp3nS	být
bučina	bučina	k1gFnSc1	bučina
s	s	k7c7	s
kyčelnicí	kyčelnice	k1gFnSc7	kyčelnice
devítilistou	devítilistý	k2eAgFnSc7d1	devítilistá
<g/>
,	,	kIx,	,
okrajové	okrajový	k2eAgFnSc3d1	okrajová
části	část	k1gFnSc3	část
okolo	okolo	k7c2	okolo
Heroltic	Heroltice	k1gFnPc2	Heroltice
a	a	k8xC	a
Horní	horní	k2eAgInSc1d1	horní
Kosov	Kosov	k1gInSc1	Kosov
biková	bikový	k2eAgFnSc1d1	biková
bučina	bučina	k1gFnSc1	bučina
<g/>
.	.	kIx.	.
</s>
<s>
Krajinu	Krajina	k1gFnSc4	Krajina
tvoří	tvořit	k5eAaImIp3nP	tvořit
pole	pole	k1gNnPc1	pole
a	a	k8xC	a
louky	louka	k1gFnPc1	louka
s	s	k7c7	s
trvalým	trvalý	k2eAgInSc7d1	trvalý
travním	travní	k2eAgInSc7d1	travní
porostem	porost	k1gInSc7	porost
<g/>
.	.	kIx.	.
</s>
<s>
Podél	podél	k7c2	podél
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
rostou	růst	k5eAaImIp3nP	růst
olšiny	olšina	k1gFnPc1	olšina
(	(	kIx(	(
<g/>
Alnus	Alnus	k1gInSc1	Alnus
<g/>
)	)	kIx)	)
s	s	k7c7	s
bledulemi	bledule	k1gFnPc7	bledule
jarními	jarní	k2eAgFnPc7d1	jarní
(	(	kIx(	(
<g/>
Leucojum	Leucojum	k1gInSc1	Leucojum
vernum	vernum	k1gInSc1	vernum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lesích	les	k1gInPc6	les
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
borůvky	borůvka	k1gFnPc1	borůvka
(	(	kIx(	(
<g/>
Vaccinium	Vaccinium	k1gNnSc1	Vaccinium
myrtillus	myrtillus	k1gInSc1	myrtillus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
violky	violka	k1gFnSc2	violka
lesní	lesní	k2eAgFnSc1d1	lesní
(	(	kIx(	(
<g/>
Viola	Viola	k1gFnSc1	Viola
reichenbachiana	reichenbachiana	k1gFnSc1	reichenbachiana
<g/>
)	)	kIx)	)
či	či	k8xC	či
kapraď	kapraď	k1gFnSc1	kapraď
samec	samec	k1gMnSc1	samec
(	(	kIx(	(
<g/>
Dryopteris	Dryopteris	k1gFnSc1	Dryopteris
filix-mas	filixas	k1gMnSc1	filix-mas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolních	okolní	k2eAgInPc6d1	okolní
lesích	les	k1gInPc6	les
žijí	žít	k5eAaImIp3nP	žít
srnci	srnec	k1gMnPc1	srnec
obecní	obecní	k2eAgMnPc1d1	obecní
(	(	kIx(	(
<g/>
Capreolus	Capreolus	k1gMnSc1	Capreolus
capreolus	capreolus	k1gMnSc1	capreolus
<g/>
)	)	kIx)	)
či	či	k8xC	či
divočáci	divočák	k1gMnPc1	divočák
(	(	kIx(	(
<g/>
Sus	Sus	k1gMnSc1	Sus
scrofa	scrof	k1gMnSc2	scrof
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
šelem	šelma	k1gFnPc2	šelma
pak	pak	k6eAd1	pak
kuny	kuna	k1gFnSc2	kuna
lesní	lesní	k2eAgFnSc2d1	lesní
(	(	kIx(	(
<g/>
Martes	Martesa	k1gFnPc2	Martesa
martes	martes	k1gInSc1	martes
<g/>
)	)	kIx)	)
a	a	k8xC	a
lišky	liška	k1gFnPc1	liška
obecné	obecný	k2eAgFnPc1d1	obecná
(	(	kIx(	(
<g/>
Vulpes	Vulpes	k1gMnSc1	Vulpes
vulpes	vulpes	k1gMnSc1	vulpes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lesích	les	k1gInPc6	les
hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
datli	datel	k1gMnPc1	datel
černí	černý	k2eAgMnPc1d1	černý
(	(	kIx(	(
<g/>
Dryocopus	Dryocopus	k1gMnSc1	Dryocopus
martius	martius	k1gMnSc1	martius
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žluny	žluna	k1gFnPc1	žluna
šedé	šedý	k2eAgFnPc1d1	šedá
(	(	kIx(	(
<g/>
Picus	Picus	k1gMnSc1	Picus
canus	canus	k1gMnSc1	canus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sýkory	sýkora	k1gFnSc2	sýkora
(	(	kIx(	(
<g/>
Parus	Parus	k1gInSc1	Parus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pěnice	pěnice	k1gFnSc1	pěnice
(	(	kIx(	(
<g/>
Sylvia	Sylvia	k1gFnSc1	Sylvia
<g/>
)	)	kIx)	)
či	či	k8xC	či
drozdi	drozd	k1gMnPc1	drozd
zpěvní	zpěvní	k2eAgMnPc1d1	zpěvní
(	(	kIx(	(
<g/>
Turdus	Turdus	k1gMnSc1	Turdus
philomelos	philomelos	k1gMnSc1	philomelos
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
dravých	dravý	k2eAgMnPc2d1	dravý
ptáků	pták	k1gMnPc2	pták
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
káně	káně	k1gFnPc1	káně
lesní	lesní	k2eAgFnSc2d1	lesní
(	(	kIx(	(
<g/>
Buteo	Buteo	k6eAd1	Buteo
buteo	buteo	k6eAd1	buteo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vlhkých	vlhký	k2eAgFnPc6d1	vlhká
loukách	louka	k1gFnPc6	louka
jsou	být	k5eAaImIp3nP	být
zastoupeny	zastoupit	k5eAaPmNgInP	zastoupit
běžné	běžný	k2eAgInPc1d1	běžný
druhy	druh	k1gInPc1	druh
plazů	plaz	k1gMnPc2	plaz
a	a	k8xC	a
obojživelníků	obojživelník	k1gMnPc2	obojživelník
<g/>
,	,	kIx,	,
např.	např.	kA	např.
zmije	zmije	k1gFnSc1	zmije
obecné	obecná	k1gFnSc2	obecná
(	(	kIx(	(
<g/>
Vipera	Vipera	k1gFnSc1	Vipera
berus	berus	k1gMnSc1	berus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ještěrky	ještěrka	k1gFnPc4	ještěrka
živorodé	živorodý	k2eAgFnPc4d1	živorodá
(	(	kIx(	(
<g/>
Zootoca	Zootoc	k2eAgFnSc1d1	Zootoca
vivipara	vivipara	k1gFnSc1	vivipara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
skokani	skokan	k1gMnPc5	skokan
(	(	kIx(	(
<g/>
Rana	Rana	k1gFnSc1	Rana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ropuchy	ropucha	k1gFnSc2	ropucha
obecné	obecný	k2eAgFnSc2d1	obecná
(	(	kIx(	(
<g/>
Bufo	bufa	k1gFnSc5	bufa
bufo	bufa	k1gFnSc5	bufa
<g/>
)	)	kIx)	)
či	či	k8xC	či
čolci	čolek	k1gMnPc1	čolek
(	(	kIx(	(
<g/>
Triturus	Triturus	k1gMnSc1	Triturus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
žijí	žít	k5eAaImIp3nP	žít
typické	typický	k2eAgInPc1d1	typický
druhy	druh	k1gInPc1	druh
ryb	ryba	k1gFnPc2	ryba
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
skokani	skokan	k1gMnPc1	skokan
hnědí	hnědit	k5eAaImIp3nP	hnědit
(	(	kIx(	(
<g/>
Rana	Rana	k1gFnSc1	Rana
temporaria	temporarium	k1gNnSc2	temporarium
<g/>
)	)	kIx)	)
a	a	k8xC	a
mloci	mlok	k1gMnPc1	mlok
skvrnití	skvrnitý	k2eAgMnPc1d1	skvrnitý
(	(	kIx(	(
<g/>
Salamandra	salamandr	k1gMnSc4	salamandr
salamandra	salamandr	k1gMnSc4	salamandr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
rybnících	rybník	k1gInPc6	rybník
sídlí	sídlet	k5eAaImIp3nS	sídlet
kachny	kachna	k1gFnPc4	kachna
divoké	divoký	k2eAgFnPc4d1	divoká
(	(	kIx(	(
<g/>
Anas	Anas	k1gInSc4	Anas
platyrhynchos	platyrhynchosa	k1gFnPc2	platyrhynchosa
<g/>
)	)	kIx)	)
či	či	k8xC	či
lysky	lyska	k1gFnPc1	lyska
černé	černý	k2eAgFnPc1d1	černá
(	(	kIx(	(
<g/>
Fulica	Fulica	k1gMnSc1	Fulica
atra	atra	k1gMnSc1	atra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Evropsky	evropsky	k6eAd1	evropsky
významné	významný	k2eAgFnPc1d1	významná
lokality	lokalita	k1gFnPc1	lokalita
<g/>
:	:	kIx,	:
Lužný	Lužný	k2eAgInSc1d1	Lužný
rybník	rybník	k1gInSc1	rybník
<g/>
,	,	kIx,	,
Šlapanka	Šlapanka	k1gFnSc1	Šlapanka
a	a	k8xC	a
Zlatý	zlatý	k2eAgInSc1d1	zlatý
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Vysoký	vysoký	k2eAgInSc1d1	vysoký
kámen	kámen	k1gInSc1	kámen
u	u	k7c2	u
Smrčné	Smrčný	k2eAgFnSc2d1	Smrčná
a	a	k8xC	a
Zaječí	zaječí	k2eAgFnSc2d1	zaječí
skok	skok	k1gInSc4	skok
<g/>
.	.	kIx.	.
</s>
<s>
Památné	památný	k2eAgInPc1d1	památný
stromy	strom	k1gInPc1	strom
<g/>
:	:	kIx,	:
Buk	buk	k1gInSc1	buk
na	na	k7c6	na
vrchu	vrch	k1gInSc6	vrch
Rudný	rudný	k2eAgInSc1d1	rudný
<g/>
,	,	kIx,	,
Buk	buk	k1gInSc1	buk
u	u	k7c2	u
kostela	kostel	k1gInSc2	kostel
v	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
<g/>
,	,	kIx,	,
Buky	buk	k1gInPc1	buk
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Smetany	smetana	k1gFnSc2	smetana
ve	v	k7c6	v
Smetanových	Smetanových	k2eAgInPc6d1	Smetanových
sadech	sad	k1gInPc6	sad
<g/>
,	,	kIx,	,
Dub	dub	k1gInSc1	dub
letní	letní	k2eAgInSc1d1	letní
u	u	k7c2	u
Henčova	Henčův	k2eAgNnSc2d1	Henčův
<g/>
,	,	kIx,	,
Dub	dub	k1gInSc4	dub
u	u	k7c2	u
Domu	dům	k1gInSc2	dům
zdraví	zdraví	k1gNnSc2	zdraví
<g/>
,	,	kIx,	,
Javor	javor	k1gInSc1	javor
v	v	k7c6	v
Popicích	Popice	k1gFnPc6	Popice
<g/>
,	,	kIx,	,
Jilmy	jilma	k1gFnPc1	jilma
u	u	k7c2	u
Hosova	Hosovo	k1gNnSc2	Hosovo
<g/>
,	,	kIx,	,
Lípa	lípa	k1gFnSc1	lípa
u	u	k7c2	u
Tomášků	Tomášek	k1gMnPc2	Tomášek
ve	v	k7c4	v
Zborné	zborný	k2eAgNnSc4d1	zborný
<g/>
,	,	kIx,	,
Lipové	lipový	k2eAgNnSc4d1	lipové
stromořadí	stromořadí	k1gNnSc4	stromořadí
u	u	k7c2	u
Modety	Modeta	k1gFnSc2	Modeta
<g/>
,	,	kIx,	,
Stromořadí	stromořadí	k1gNnSc1	stromořadí
v	v	k7c6	v
Bedřichově	Bedřichův	k2eAgNnSc6d1	Bedřichovo
<g/>
,	,	kIx,	,
Stromořadí	stromořadí	k1gNnSc3	stromořadí
v	v	k7c6	v
Horním	horní	k2eAgNnSc6d1	horní
Kosově	Kosův	k2eAgNnSc6d1	Kosovo
<g/>
,	,	kIx,	,
Zerav	zerav	k1gInSc4	zerav
obrovský	obrovský	k2eAgInSc4d1	obrovský
v	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnPc2	sčítání
1930	[number]	k4	1930
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c4	v
2	[number]	k4	2
070	[number]	k4	070
domech	dům	k1gInPc6	dům
31	[number]	k4	31
028	[number]	k4	028
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
17	[number]	k4	17
968	[number]	k4	968
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
československé	československý	k2eAgFnSc3d1	Československá
národnosti	národnost	k1gFnSc3	národnost
a	a	k8xC	a
12	[number]	k4	12
093	[number]	k4	093
k	k	k7c3	k
německé	německý	k2eAgFnSc2d1	německá
<g/>
.	.	kIx.	.
</s>
<s>
Žilo	žít	k5eAaImAgNnS	žít
zde	zde	k6eAd1	zde
25	[number]	k4	25
759	[number]	k4	759
římských	římský	k2eAgMnPc2d1	římský
katolíků	katolík	k1gMnPc2	katolík
<g/>
,	,	kIx,	,
1	[number]	k4	1
172	[number]	k4	172
evangelíků	evangelík	k1gMnPc2	evangelík
<g/>
,	,	kIx,	,
1	[number]	k4	1
320	[number]	k4	320
příslušníků	příslušník	k1gMnPc2	příslušník
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
a	a	k8xC	a
1	[number]	k4	1
025	[number]	k4	025
židů	žid	k1gMnPc2	žid
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
trvale	trvale	k6eAd1	trvale
50	[number]	k4	50
075	[number]	k4	075
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
25	[number]	k4	25
866	[number]	k4	866
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
35	[number]	k4	35
495	[number]	k4	495
občanů	občan	k1gMnPc2	občan
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
české	český	k2eAgFnSc3d1	Česká
národnosti	národnost	k1gFnSc3	národnost
<g/>
,	,	kIx,	,
737	[number]	k4	737
k	k	k7c3	k
moravské	moravský	k2eAgFnSc3d1	Moravská
<g/>
,	,	kIx,	,
501	[number]	k4	501
ke	k	k7c3	k
slovenské	slovenský	k2eAgFnSc3d1	slovenská
<g/>
,	,	kIx,	,
183	[number]	k4	183
k	k	k7c3	k
ukrajinské	ukrajinský	k2eAgInPc1d1	ukrajinský
<g/>
,	,	kIx,	,
84	[number]	k4	84
k	k	k7c3	k
vietnamské	vietnamský	k2eAgFnSc3d1	vietnamská
<g/>
,	,	kIx,	,
71	[number]	k4	71
k	k	k7c3	k
německé	německý	k2eAgFnSc3d1	německá
<g/>
,	,	kIx,	,
32	[number]	k4	32
k	k	k7c3	k
romské	romský	k2eAgFnSc3d1	romská
<g/>
,	,	kIx,	,
24	[number]	k4	24
k	k	k7c3	k
polské	polský	k2eAgFnSc3d1	polská
a	a	k8xC	a
3	[number]	k4	3
ke	k	k7c3	k
slezské	slezský	k2eAgFnPc1d1	Slezská
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
jihlavským	jihlavský	k2eAgInSc7d1	jihlavský
obytným	obytný	k2eAgInSc7d1	obytný
celkem	celek	k1gInSc7	celek
je	být	k5eAaImIp3nS	být
sídliště	sídliště	k1gNnSc1	sídliště
Březinova	Březinův	k2eAgNnSc2d1	Březinovo
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Březinky	březinka	k1gFnPc1	březinka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
ležící	ležící	k2eAgFnSc1d1	ležící
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
historického	historický	k2eAgNnSc2d1	historické
centra	centrum	k1gNnSc2	centrum
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
tu	tu	k6eAd1	tu
žilo	žít	k5eAaImAgNnS	žít
10	[number]	k4	10
090	[number]	k4	090
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
o	o	k7c4	o
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
8	[number]	k4	8
964	[number]	k4	964
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
7	[number]	k4	7
622	[number]	k4	622
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
děkanství	děkanství	k1gNnSc2	děkanství
jihlavského	jihlavský	k2eAgNnSc2d1	jihlavské
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
náleží	náležet	k5eAaImIp3nP	náležet
k	k	k7c3	k
brněnské	brněnský	k2eAgFnSc3d1	brněnská
diecézi	diecéze	k1gFnSc3	diecéze
<g/>
.	.	kIx.	.
</s>
<s>
Spadají	spadat	k5eAaImIp3nP	spadat
sem	sem	k6eAd1	sem
dvě	dva	k4xCgFnPc1	dva
zdejší	zdejší	k2eAgFnPc1d1	zdejší
římskokatolické	římskokatolický	k2eAgFnPc1d1	Římskokatolická
farnosti	farnost	k1gFnPc1	farnost
–	–	k?	–
kostela	kostel	k1gInSc2	kostel
Matky	matka	k1gFnSc2	matka
Boží	boží	k2eAgFnSc2d1	boží
a	a	k8xC	a
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Jakuba	Jakub	k1gMnSc2	Jakub
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
cenzu	cenzus	k1gInSc2	cenzus
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
k	k	k7c3	k
víře	víra	k1gFnSc3	víra
hlásí	hlásit	k5eAaImIp3nS	hlásit
10	[number]	k4	10
108	[number]	k4	108
z	z	k7c2	z
50	[number]	k4	50
075	[number]	k4	075
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
3	[number]	k4	3
402	[number]	k4	402
se	se	k3xPyFc4	se
nehlásí	hlásit	k5eNaImIp3nS	hlásit
k	k	k7c3	k
žádné	žádný	k3yNgFnSc3	žádný
církvi	církev	k1gFnSc3	církev
či	či	k8xC	či
náboženské	náboženský	k2eAgFnSc3d1	náboženská
společnosti	společnost	k1gFnSc3	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
6	[number]	k4	6
706	[number]	k4	706
věřících	věřící	k1gMnPc2	věřící
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
uvedli	uvést	k5eAaPmAgMnP	uvést
svou	svůj	k3xOyFgFnSc4	svůj
víru	víra	k1gFnSc4	víra
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
5	[number]	k4	5
151	[number]	k4	151
lidí	člověk	k1gMnPc2	člověk
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
římskokatolické	římskokatolický	k2eAgFnSc3d1	Římskokatolická
církvi	církev	k1gFnSc3	církev
<g/>
,	,	kIx,	,
226	[number]	k4	226
k	k	k7c3	k
Českobratrské	českobratrský	k2eAgFnSc3d1	Českobratrská
církvi	církev	k1gFnSc3	církev
evangelické	evangelický	k2eAgFnSc2d1	evangelická
<g/>
,	,	kIx,	,
138	[number]	k4	138
k	k	k7c3	k
Církvi	církev	k1gFnSc3	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
<g/>
,	,	kIx,	,
81	[number]	k4	81
k	k	k7c3	k
Svědkům	svědek	k1gMnPc3	svědek
Jehovovým	Jehovův	k2eAgNnPc3d1	Jehovovo
a	a	k8xC	a
53	[number]	k4	53
k	k	k7c3	k
Pravoslavné	pravoslavný	k2eAgFnSc3d1	pravoslavná
církvi	církev	k1gFnSc3	církev
<g/>
.	.	kIx.	.
22	[number]	k4	22
162	[number]	k4	162
osob	osoba	k1gFnPc2	osoba
svou	svůj	k3xOyFgFnSc4	svůj
víru	víra	k1gFnSc4	víra
neuvedlo	uvést	k5eNaPmAgNnS	uvést
<g/>
,	,	kIx,	,
17	[number]	k4	17
797	[number]	k4	797
osob	osoba	k1gFnPc2	osoba
bylo	být	k5eAaImAgNnS	být
bez	bez	k7c2	bez
náboženské	náboženský	k2eAgFnSc2d1	náboženská
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
18	[number]	k4	18
katastrálních	katastrální	k2eAgNnPc2d1	katastrální
území	území	k1gNnPc2	území
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yQgInPc6	který
leží	ležet	k5eAaImIp3nS	ležet
16	[number]	k4	16
částí	část	k1gFnPc2	část
a	a	k8xC	a
51	[number]	k4	51
základních	základní	k2eAgFnPc2d1	základní
sídelních	sídelní	k2eAgFnPc2d1	sídelní
jednotek	jednotka	k1gFnPc2	jednotka
(	(	kIx(	(
<g/>
dále	daleko	k6eAd2	daleko
jen	jen	k9	jen
ZSJ	ZSJ	kA	ZSJ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Antonínův	Antonínův	k2eAgInSc1d1	Antonínův
Důl	důl	k1gInSc1	důl
–	–	k?	–
části	část	k1gFnSc2	část
Červený	Červený	k1gMnSc1	Červený
Kříž	Kříž	k1gMnSc1	Kříž
a	a	k8xC	a
Antonínův	Antonínův	k2eAgInSc1d1	Antonínův
Důl	důl	k1gInSc1	důl
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
ZSJ	ZSJ	kA	ZSJ
Antonínův	Antonínův	k2eAgInSc1d1	Antonínův
Důl	důl	k1gInSc1	důl
<g/>
,	,	kIx,	,
Červený	červený	k2eAgInSc1d1	červený
Kříž	kříž	k1gInSc1	kříž
a	a	k8xC	a
Červený	červený	k2eAgInSc1d1	červený
Kříž-průmyslový	Křížrůmyslový	k2eAgInSc1d1	Kříž-průmyslový
obvod	obvod	k1gInSc4	obvod
Bedřichov	Bedřichovo	k1gNnPc2	Bedřichovo
u	u	k7c2	u
Jihlavy	Jihlava	k1gFnSc2	Jihlava
–	–	k?	–
původně	původně	k6eAd1	původně
části	část	k1gFnPc1	část
Bedřichov	Bedřichovo	k1gNnPc2	Bedřichovo
a	a	k8xC	a
Bukovno	Bukovna	k1gFnSc5	Bukovna
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
mezi	mezi	k7c4	mezi
části	část	k1gFnPc4	část
Jihlavu	Jihlava	k1gFnSc4	Jihlava
a	a	k8xC	a
Staré	Staré	k2eAgFnPc1d1	Staré
Hory	hora	k1gFnPc1	hora
<g/>
;	;	kIx,	;
3	[number]	k4	3
ZSJ	ZSJ	kA	ZSJ
<g />
.	.	kIx.	.
</s>
<s>
Bedřichov	Bedřichov	k1gInSc1	Bedřichov
<g/>
,	,	kIx,	,
Lesnov	Lesnov	k1gInSc1	Lesnov
a	a	k8xC	a
Zadní	zadní	k2eAgInSc1d1	zadní
Bedřichov	Bedřichov	k1gInSc1	Bedřichov
Helenín	Helenín	k1gInSc1	Helenín
–	–	k?	–
část	část	k1gFnSc1	část
části	část	k1gFnSc2	část
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
;	;	kIx,	;
2	[number]	k4	2
ZSJ	ZSJ	kA	ZSJ
<g/>
:	:	kIx,	:
Helenín	Helenín	k1gMnSc1	Helenín
a	a	k8xC	a
Na	na	k7c6	na
Člunku	člunek	k1gInSc6	člunek
Henčov	Henčov	k1gInSc1	Henčov
–	–	k?	–
část	část	k1gFnSc1	část
Henčov	Henčov	k1gInSc1	Henčov
Heroltice	Heroltice	k1gFnSc2	Heroltice
u	u	k7c2	u
Jihlavy	Jihlava	k1gFnSc2	Jihlava
–	–	k?	–
část	část	k1gFnSc1	část
Heroltice	Heroltice	k1gFnSc1	Heroltice
<g/>
;	;	kIx,	;
2	[number]	k4	2
ZSJ	ZSJ	kA	ZSJ
<g/>
:	:	kIx,	:
Heroltice	Heroltice	k1gFnSc1	Heroltice
a	a	k8xC	a
U	u	k7c2	u
Karlova	Karlův	k2eAgInSc2d1	Karlův
lesa	les	k1gInSc2	les
Horní	horní	k2eAgInSc1d1	horní
Kosov	Kosov	k1gInSc1	Kosov
–	–	k?	–
část	část	k1gFnSc1	část
Horní	horní	k2eAgFnSc1d1	horní
Kosov	Kosov	k1gInSc4	Kosov
<g/>
<g />
.	.	kIx.	.
</s>
<s>
;	;	kIx,	;
3	[number]	k4	3
ZSJ	ZSJ	kA	ZSJ
<g/>
:	:	kIx,	:
Horní	horní	k2eAgInSc1d1	horní
Kosov	Kosov	k1gInSc1	Kosov
<g/>
,	,	kIx,	,
K	k	k7c3	k
Rantířovu	Rantířův	k2eAgNnSc3d1	Rantířův
<g/>
,	,	kIx,	,
Sídliště	sídliště	k1gNnSc4	sídliště
Horní	horní	k2eAgInSc1d1	horní
Kosov	Kosov	k1gInSc1	Kosov
Hosov	Hosov	k1gInSc1	Hosov
–	–	k?	–
část	část	k1gFnSc1	část
Hosov	Hosovo	k1gNnPc2	Hosovo
Hruškové	Hrušková	k1gFnSc2	Hrušková
Dvory	Dvůr	k1gInPc4	Dvůr
–	–	k?	–
část	část	k1gFnSc1	část
Hruškové	Hrušková	k1gFnSc2	Hrušková
Dvory	Dvůr	k1gInPc1	Dvůr
a	a	k8xC	a
část	část	k1gFnSc1	část
části	část	k1gFnSc2	část
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
;	;	kIx,	;
2	[number]	k4	2
ZSJ	ZSJ	kA	ZSJ
Hruškové	Hruškové	k2eAgInPc1d1	Hruškové
Dvory	Dvůr	k1gInPc1	Dvůr
<g/>
,	,	kIx,	,
Průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
zóna	zóna	k1gFnSc1	zóna
Hruškové	Hrušková	k1gFnSc2	Hrušková
Dvory	Dvůr	k1gInPc4	Dvůr
Jihlava	Jihlava	k1gFnSc1	Jihlava
–	–	k?	–
část	část	k1gFnSc1	část
části	část	k1gFnSc2	část
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
;	;	kIx,	;
22	[number]	k4	22
<g />
.	.	kIx.	.
</s>
<s>
ZSJ	ZSJ	kA	ZSJ
<g/>
:	:	kIx,	:
Heulos	Heulos	k1gMnSc1	Heulos
<g/>
,	,	kIx,	,
Jihlava-historické	Jihlavaistorický	k2eAgNnSc1d1	Jihlava-historický
jádro	jádro	k1gNnSc1	jádro
<g/>
,	,	kIx,	,
Kalvárie	Kalvárie	k1gFnSc1	Kalvárie
<g/>
,	,	kIx,	,
Královský	královský	k2eAgInSc1d1	královský
vršek	vršek	k1gInSc1	vršek
<g/>
,	,	kIx,	,
Na	na	k7c6	na
Dolech	dol	k1gInPc6	dol
<g/>
,	,	kIx,	,
Na	na	k7c4	na
Dolech-jih	Dolechih	k1gInSc4	Dolech-jih
<g/>
,	,	kIx,	,
Na	na	k7c4	na
Dolech-sever	Dolechever	k1gInSc4	Dolech-sever
<g/>
,	,	kIx,	,
Na	na	k7c6	na
Slunci	slunce	k1gNnSc6	slunce
<g/>
,	,	kIx,	,
Na	na	k7c6	na
valech	val	k1gInPc6	val
<g/>
,	,	kIx,	,
Průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
zóna	zóna	k1gFnSc1	zóna
Bedřichov	Bedřichov	k1gInSc1	Bedřichov
<g/>
,	,	kIx,	,
Psychiatrická	psychiatrický	k2eAgFnSc1d1	psychiatrická
léčebna	léčebna	k1gFnSc1	léčebna
<g/>
,	,	kIx,	,
Seifertova	Seifertův	k2eAgFnSc1d1	Seifertova
<g/>
,	,	kIx,	,
Sídliště	sídliště	k1gNnSc1	sídliště
<g />
.	.	kIx.	.
</s>
<s>
Březinova	Březinův	k2eAgFnSc1d1	Březinova
<g/>
,	,	kIx,	,
Třída	třída	k1gFnSc1	třída
Legionářů	legionář	k1gMnPc2	legionář
<g/>
,	,	kIx,	,
U	u	k7c2	u
domu	dům	k1gInSc2	dům
zdraví	zdraví	k1gNnSc2	zdraví
<g/>
,	,	kIx,	,
U	u	k7c2	u
hřbitova	hřbitov	k1gInSc2	hřbitov
<g/>
,	,	kIx,	,
U	u	k7c2	u
mlýna	mlýn	k1gInSc2	mlýn
<g/>
,	,	kIx,	,
U	u	k7c2	u
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
U	u	k7c2	u
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
U	u	k7c2	u
větrníku	větrník	k1gInSc2	větrník
<g/>
,	,	kIx,	,
Za	za	k7c2	za
tratí	trať	k1gFnPc2	trať
<g/>
,	,	kIx,	,
Znojemská	znojemský	k2eAgFnSc1d1	Znojemská
Kosov	Kosov	k1gInSc4	Kosov
u	u	k7c2	u
Jihlavy	Jihlava	k1gFnSc2	Jihlava
–	–	k?	–
část	část	k1gFnSc1	část
Kosov	Kosov	k1gInSc1	Kosov
Pančava	pančava	k1gFnSc1	pančava
–	–	k?	–
část	část	k1gFnSc1	část
části	část	k1gFnSc2	část
Jihlava	Jihlava	k1gFnSc1	Jihlava
Pávov	Pávov	k1gInSc1	Pávov
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
části	část	k1gFnPc1	část
Pávov	Pávovo	k1gNnPc2	Pávovo
a	a	k8xC	a
Antonínův	Antonínův	k2eAgInSc1d1	Antonínův
Důl	důl	k1gInSc1	důl
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
4	[number]	k4	4
ZSJ	ZSJ	kA	ZSJ
<g/>
:	:	kIx,	:
Nový	nový	k2eAgInSc1d1	nový
Pávov	Pávov	k1gInSc1	Pávov
<g/>
,	,	kIx,	,
Pávov	Pávov	k1gInSc1	Pávov
<g/>
,	,	kIx,	,
Průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
zóna	zóna	k1gFnSc1	zóna
Pávov	Pávov	k1gInSc1	Pávov
<g/>
,	,	kIx,	,
Vysoký	vysoký	k2eAgInSc1d1	vysoký
kámen	kámen	k1gInSc4	kámen
Pístov	Pístovo	k1gNnPc2	Pístovo
u	u	k7c2	u
Jihlavy	Jihlava	k1gFnSc2	Jihlava
–	–	k?	–
část	část	k1gFnSc1	část
Pístov	Pístov	k1gInSc1	Pístov
Popice	Popice	k1gFnSc2	Popice
u	u	k7c2	u
Jihlavy	Jihlava	k1gFnSc2	Jihlava
–	–	k?	–
část	část	k1gFnSc1	část
Popice	Popice	k1gFnSc2	Popice
Sasov	Sasov	k1gInSc1	Sasov
–	–	k?	–
část	část	k1gFnSc1	část
Sasov	Sasov	k1gInSc1	Sasov
Staré	Staré	k2eAgFnSc2d1	Staré
Hory	hora	k1gFnSc2	hora
–	–	k?	–
část	část	k1gFnSc1	část
části	část	k1gFnSc2	část
Staré	Staré	k2eAgFnSc2d1	Staré
Hory	hora	k1gFnSc2	hora
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
u	u	k7c2	u
Jihlavy	Jihlava	k1gFnSc2	Jihlava
–	–	k?	–
část	část	k1gFnSc1	část
Vysoká	vysoká	k1gFnSc1	vysoká
Zborná	zborný	k2eAgFnSc1d1	Zborná
–	–	k?	–
část	část	k1gFnSc1	část
Zborná	zborný	k2eAgFnSc1d1	Zborná
Součástí	součást	k1gFnSc7	součást
města	město	k1gNnSc2	město
byly	být	k5eAaImAgInP	být
dříve	dříve	k6eAd2	dříve
i	i	k9	i
dnes	dnes	k6eAd1	dnes
samostatné	samostatný	k2eAgFnPc1d1	samostatná
obce	obec	k1gFnPc1	obec
Cerekvička-Rosice	Cerekvička-Rosice	k1gFnSc1	Cerekvička-Rosice
<g/>
,	,	kIx,	,
Čížov	Čížov	k1gInSc1	Čížov
<g/>
,	,	kIx,	,
Hybrálec	Hybrálec	k1gInSc1	Hybrálec
<g/>
,	,	kIx,	,
Malý	malý	k2eAgInSc1d1	malý
Beranov	Beranov	k1gInSc1	Beranov
<g/>
,	,	kIx,	,
Měšín	Měšín	k1gInSc1	Měšín
<g/>
,	,	kIx,	,
Rančířov	Rančířov	k1gInSc1	Rančířov
<g/>
,	,	kIx,	,
Rantířov	Rantířov	k1gInSc1	Rantířov
<g/>
,	,	kIx,	,
Smrčná	Smrčné	k1gNnPc1	Smrčné
a	a	k8xC	a
Vílanec	Vílanec	k1gInSc1	Vílanec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
leží	ležet	k5eAaImIp3nS	ležet
katastrální	katastrální	k2eAgNnSc4d1	katastrální
území	území	k1gNnSc4	území
Bedřichov	Bedřichovo	k1gNnPc2	Bedřichovo
u	u	k7c2	u
Jihlavy	Jihlava	k1gFnSc2	Jihlava
(	(	kIx(	(
<g/>
nepatrná	patrný	k2eNgFnSc1d1	patrný
část	část	k1gFnSc1	část
katastru	katastr	k1gInSc2	katastr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Helenín	Helenín	k1gInSc1	Helenín
<g/>
,	,	kIx,	,
Henčov	Henčov	k1gInSc1	Henčov
(	(	kIx(	(
<g/>
téměř	téměř	k6eAd1	téměř
celý	celý	k2eAgInSc4d1	celý
katastr	katastr	k1gInSc4	katastr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Horní	horní	k2eAgInSc1d1	horní
Kosov	Kosov	k1gInSc1	Kosov
<g/>
,	,	kIx,	,
Hosov	Hosov	k1gInSc1	Hosov
<g/>
,	,	kIx,	,
Hruškové	Hruškové	k2eAgInPc1d1	Hruškové
Dvory	Dvůr	k1gInPc1	Dvůr
(	(	kIx(	(
<g/>
téměř	téměř	k6eAd1	téměř
celý	celý	k2eAgInSc4d1	celý
katastr	katastr	k1gInSc4	katastr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jihlava	Jihlava	k1gFnSc1	Jihlava
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
většina	většina	k1gFnSc1	většina
katastru	katastr	k1gInSc2	katastr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kosov	Kosov	k1gInSc1	Kosov
u	u	k7c2	u
Jihlavy	Jihlava	k1gFnSc2	Jihlava
<g/>
,	,	kIx,	,
Pančava	pančava	k1gFnSc1	pančava
<g/>
,	,	kIx,	,
Pístov	Pístov	k1gInSc1	Pístov
u	u	k7c2	u
Jihlavy	Jihlava	k1gFnSc2	Jihlava
<g/>
,	,	kIx,	,
Popice	Popice	k1gFnSc2	Popice
u	u	k7c2	u
Jihlavy	Jihlava	k1gFnSc2	Jihlava
<g/>
,	,	kIx,	,
Sasov	Sasov	k1gInSc4	Sasov
<g/>
,	,	kIx,	,
Staré	Staré	k2eAgFnPc1d1	Staré
Hory	hora	k1gFnPc1	hora
(	(	kIx(	(
<g/>
nepatrná	patrný	k2eNgFnSc1d1	patrný
část	část	k1gFnSc1	část
katastru	katastr	k1gInSc2	katastr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
u	u	k7c2	u
Jihlavy	Jihlava	k1gFnSc2	Jihlava
a	a	k8xC	a
Heroltice	Heroltice	k1gFnSc2	Heroltice
u	u	k7c2	u
Jihlavy	Jihlava	k1gFnSc2	Jihlava
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc1	několik
pozemků	pozemek	k1gInPc2	pozemek
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
a	a	k8xC	a
východě	východ	k1gInSc6	východ
katastru	katastr	k1gInSc2	katastr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
pak	pak	k6eAd1	pak
patří	patřit	k5eAaImIp3nS	patřit
katastrální	katastrální	k2eAgNnSc4d1	katastrální
území	území	k1gNnSc4	území
Antonínův	Antonínův	k2eAgInSc1d1	Antonínův
Důl	důl	k1gInSc1	důl
<g/>
,	,	kIx,	,
Bedřichov	Bedřichov	k1gInSc1	Bedřichov
u	u	k7c2	u
Jihlavy	Jihlava	k1gFnSc2	Jihlava
(	(	kIx(	(
<g/>
téměř	téměř	k6eAd1	téměř
celý	celý	k2eAgInSc4d1	celý
katastr	katastr	k1gInSc4	katastr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Heroltice	Heroltice	k1gFnSc1	Heroltice
u	u	k7c2	u
Jihlavy	Jihlava	k1gFnSc2	Jihlava
(	(	kIx(	(
<g/>
téměř	téměř	k6eAd1	téměř
celý	celý	k2eAgInSc4d1	celý
katastr	katastr	k1gInSc4	katastr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hruškové	Hruškové	k2eAgInPc1d1	Hruškové
Dvory	Dvůr	k1gInPc1	Dvůr
(	(	kIx(	(
<g/>
malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
katastru	katastr	k1gInSc2	katastr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pávov	Pávov	k1gInSc1	Pávov
<g/>
,	,	kIx,	,
Staré	Staré	k2eAgFnSc2d1	Staré
Hory	hora	k1gFnSc2	hora
(	(	kIx(	(
<g/>
téměř	téměř	k6eAd1	téměř
celý	celý	k2eAgInSc4d1	celý
katastr	katastr	k1gInSc4	katastr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zborná	zborný	k2eAgFnSc1d1	Zborná
<g/>
,	,	kIx,	,
Jihlava	Jihlava	k1gFnSc1	Jihlava
(	(	kIx(	(
<g/>
menší	malý	k2eAgFnSc1d2	menší
část	část	k1gFnSc1	část
katastru	katastr	k1gInSc2	katastr
především	především	k6eAd1	především
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Jihlavských	jihlavský	k2eAgInPc2d1	jihlavský
dřevařských	dřevařský	k2eAgInPc2d1	dřevařský
závodů	závod	k1gInPc2	závod
<g/>
)	)	kIx)	)
a	a	k8xC	a
Henčov	Henčov	k1gInSc1	Henčov
(	(	kIx(	(
<g/>
nepatrná	nepatrný	k2eAgFnSc1d1	nepatrná
část	část	k1gFnSc1	část
katastru	katastr	k1gInSc2	katastr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Kraj	kraj	k1gInSc1	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
a	a	k8xC	a
Okres	okres	k1gInSc1	okres
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
.	.	kIx.	.
</s>
<s>
Jihlava	Jihlava	k1gFnSc1	Jihlava
je	být	k5eAaImIp3nS	být
krajským	krajský	k2eAgNnSc7d1	krajské
městem	město	k1gNnSc7	město
a	a	k8xC	a
také	také	k9	také
obcí	obec	k1gFnPc2	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
a	a	k8xC	a
pověřeným	pověřený	k2eAgInSc7d1	pověřený
obecním	obecní	k2eAgInSc7d1	obecní
úřadem	úřad	k1gInSc7	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Okres	okres	k1gInSc1	okres
Jihlava	Jihlava	k1gFnSc1	Jihlava
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
123	[number]	k4	123
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
ORP	ORP	kA	ORP
ze	z	k7c2	z
79	[number]	k4	79
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
areálu	areál	k1gInSc6	areál
bývalé	bývalý	k2eAgFnSc2d1	bývalá
nemocnice	nemocnice	k1gFnSc2	nemocnice
stojí	stát	k5eAaImIp3nS	stát
budova	budova	k1gFnSc1	budova
okresního	okresní	k2eAgInSc2d1	okresní
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
okresního	okresní	k2eAgNnSc2d1	okresní
státního	státní	k2eAgNnSc2d1	státní
zastupitelství	zastupitelství	k1gNnSc2	zastupitelství
a	a	k8xC	a
pobočky	pobočka	k1gFnSc2	pobočka
Krajského	krajský	k2eAgInSc2d1	krajský
soudu	soud	k1gInSc2	soud
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
zastoupeny	zastoupit	k5eAaPmNgFnP	zastoupit
hygienická	hygienický	k2eAgFnSc1d1	hygienická
stanice	stanice	k1gFnPc1	stanice
a	a	k8xC	a
Státní	státní	k2eAgInSc1d1	státní
veterinární	veterinární	k2eAgInSc1d1	veterinární
ústav	ústav	k1gInSc1	ústav
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
.	.	kIx.	.
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
Krajské	krajský	k2eAgNnSc4d1	krajské
ředitelství	ředitelství	k1gNnSc4	ředitelství
policie	policie	k1gFnSc2	policie
kraje	kraj	k1gInSc2	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
a	a	k8xC	a
Městská	městský	k2eAgFnSc1d1	městská
policie	policie	k1gFnSc1	policie
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
.	.	kIx.	.
</s>
<s>
Hasičská	hasičský	k2eAgFnSc1d1	hasičská
zbrojnice	zbrojnice	k1gFnSc1	zbrojnice
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Sokolovská	sokolovský	k2eAgFnSc1d1	Sokolovská
a	a	k8xC	a
Krajské	krajský	k2eAgNnSc1d1	krajské
vojenské	vojenský	k2eAgNnSc1d1	vojenské
velitelství	velitelství	k1gNnSc1	velitelství
Jihlava	Jihlava	k1gFnSc1	Jihlava
sídlí	sídlet	k5eAaImIp3nS	sídlet
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Vrchlického	Vrchlický	k1gMnSc2	Vrchlický
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
úřadů	úřad	k1gInPc2	úřad
tu	tu	k6eAd1	tu
sídlí	sídlet	k5eAaImIp3nS	sídlet
Magistrát	magistrát	k1gInSc1	magistrát
města	město	k1gNnSc2	město
Jihlavy	Jihlava	k1gFnSc2	Jihlava
<g/>
,	,	kIx,	,
Krajský	krajský	k2eAgInSc4d1	krajský
úřad	úřad	k1gInSc4	úřad
Kraje	kraj	k1gInSc2	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
a	a	k8xC	a
pobočky	pobočka	k1gFnSc2	pobočka
finančního	finanční	k2eAgInSc2d1	finanční
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
úřadu	úřad	k1gInSc2	úřad
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
katastrálního	katastrální	k2eAgInSc2d1	katastrální
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
České	český	k2eAgFnSc2d1	Česká
správy	správa	k1gFnSc2	správa
sociálního	sociální	k2eAgNnSc2d1	sociální
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
<g/>
,	,	kIx,	,
živnostenského	živnostenský	k2eAgInSc2d1	živnostenský
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
Energetického	energetický	k2eAgInSc2d1	energetický
regulačního	regulační	k2eAgInSc2d1	regulační
úřadu	úřad	k1gInSc2	úřad
a	a	k8xC	a
Regionální	regionální	k2eAgFnSc2d1	regionální
rozvojové	rozvojový	k2eAgFnSc2d1	rozvojová
agentury	agentura	k1gFnSc2	agentura
Vysočina	vysočina	k1gFnSc1	vysočina
<g/>
.	.	kIx.	.
</s>
<s>
Zdravotnické	zdravotnický	k2eAgFnPc4d1	zdravotnická
služby	služba	k1gFnPc4	služba
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
lékaři	lékař	k1gMnPc1	lékař
v	v	k7c6	v
soukromých	soukromý	k2eAgFnPc6d1	soukromá
ordinacích	ordinace	k1gFnPc6	ordinace
či	či	k8xC	či
v	v	k7c6	v
komplexu	komplex	k1gInSc6	komplex
Jihlavské	jihlavský	k2eAgFnSc2d1	Jihlavská
terasy	terasa	k1gFnSc2	terasa
<g/>
,	,	kIx,	,
Nemocnice	nemocnice	k1gFnSc1	nemocnice
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
,	,	kIx,	,
Lékařský	lékařský	k2eAgInSc1d1	lékařský
dům	dům	k1gInSc1	dům
Jihlava	Jihlava	k1gFnSc1	Jihlava
(	(	kIx(	(
<g/>
též	též	k9	též
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xS	jako
Dům	dům	k1gInSc1	dům
zdraví	zdraví	k1gNnSc2	zdraví
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Psychiatrická	psychiatrický	k2eAgFnSc1d1	psychiatrická
nemocnice	nemocnice	k1gFnSc1	nemocnice
Jihlava	Jihlava	k1gFnSc1	Jihlava
a	a	k8xC	a
četné	četný	k2eAgFnPc1d1	četná
lékárny	lékárna	k1gFnPc1	lékárna
<g/>
.	.	kIx.	.
</s>
<s>
Domovy	domov	k1gInPc1	domov
pro	pro	k7c4	pro
seniory	senior	k1gMnPc4	senior
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
Jihlavských	jihlavský	k2eAgFnPc6d1	Jihlavská
terasách	terasa	k1gFnPc6	terasa
a	a	k8xC	a
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
části	část	k1gFnSc6	část
Lesnov	Lesnov	k1gInSc4	Lesnov
<g/>
.	.	kIx.	.
</s>
<s>
Dětský	dětský	k2eAgInSc4d1	dětský
domov	domov	k1gInSc4	domov
se	s	k7c7	s
školou	škola	k1gFnSc7	škola
<g/>
,	,	kIx,	,
základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
školní	školní	k2eAgFnSc1d1	školní
jídelna	jídelna	k1gFnSc1	jídelna
<g/>
,	,	kIx,	,
Jihlava	Jihlava	k1gFnSc1	Jihlava
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
37	[number]	k4	37
dětí	dítě	k1gFnPc2	dítě
zřizuje	zřizovat	k5eAaImIp3nS	zřizovat
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
školství	školství	k1gNnSc2	školství
<g/>
.	.	kIx.	.
</s>
<s>
Ústřední	ústřední	k2eAgInSc1d1	ústřední
hřbitov	hřbitov	k1gInSc1	hřbitov
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Žižkova	Žižkov	k1gInSc2	Žižkov
<g/>
,	,	kIx,	,
menší	malý	k2eAgInSc1d2	menší
Hřbitov	hřbitov	k1gInSc1	hřbitov
Kalvárie	Kalvárie	k1gFnSc2	Kalvárie
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Helenínská	Helenínský	k2eAgFnSc1d1	Helenínský
<g/>
.	.	kIx.	.
</s>
<s>
Služby	služba	k1gFnPc4	služba
města	město	k1gNnSc2	město
Jihlavy	Jihlava	k1gFnSc2	Jihlava
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
části	část	k1gFnSc6	část
Lesnov	Lesnov	k1gInSc1	Lesnov
provozují	provozovat	k5eAaImIp3nP	provozovat
krematorium	krematorium	k1gNnSc4	krematorium
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
představitelů	představitel	k1gMnPc2	představitel
Jihlavy	Jihlava	k1gFnSc2	Jihlava
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
37	[number]	k4	37
<g/>
členné	členný	k2eAgNnSc1d1	členné
zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
<g/>
,	,	kIx,	,
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
11	[number]	k4	11
<g/>
členné	členný	k2eAgFnSc2d1	členná
rady	rada	k1gFnSc2	rada
obce	obec	k1gFnSc2	obec
stojí	stát	k5eAaImIp3nS	stát
primátor	primátor	k1gMnSc1	primátor
Rudolf	Rudolf	k1gMnSc1	Rudolf
Chloupek	Chloupek	k1gMnSc1	Chloupek
<g/>
.	.	kIx.	.
</s>
<s>
Znak	znak	k1gInSc1	znak
Jihlavy	Jihlava	k1gFnSc2	Jihlava
tvoří	tvořit	k5eAaImIp3nS	tvořit
štít	štít	k1gInSc1	štít
dělený	dělený	k2eAgInSc1d1	dělený
na	na	k7c4	na
čtvrtiny	čtvrtina	k1gFnPc4	čtvrtina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgMnSc6	první
a	a	k8xC	a
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
rudém	rudý	k2eAgNnSc6d1	Rudé
poli	pole	k1gNnSc6	pole
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
stříbrný	stříbrný	k2eAgInSc1d1	stříbrný
korunovaný	korunovaný	k2eAgInSc1d1	korunovaný
český	český	k2eAgInSc1d1	český
lev	lev	k1gInSc1	lev
ve	v	k7c6	v
skoku	skok	k1gInSc6	skok
<g/>
,	,	kIx,	,
ve	v	k7c6	v
druhém	druhý	k4xOgMnSc6	druhý
a	a	k8xC	a
třetím	třetí	k4xOgInSc6	třetí
stříbrném	stříbrný	k2eAgNnSc6d1	stříbrné
poli	pole	k1gNnSc6	pole
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
rudá	rudý	k2eAgFnSc1d1	rudá
figura	figura	k1gFnSc1	figura
ježka	ježek	k1gMnSc2	ježek
<g/>
.	.	kIx.	.
</s>
<s>
Ježek	Ježek	k1gMnSc1	Ježek
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
Jihlavou	Jihlava	k1gFnSc7	Jihlava
spojován	spojovat	k5eAaImNgInS	spojovat
od	od	k7c2	od
přelomu	přelom	k1gInSc2	přelom
14	[number]	k4	14
<g/>
.	.	kIx.	.
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ježek	Ježek	k1gMnSc1	Ježek
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
na	na	k7c6	na
štítu	štít	k1gInSc6	štít
dílny	dílna	k1gFnSc2	dílna
jihlavských	jihlavský	k2eAgMnPc2d1	jihlavský
mincmistrů	mincmistr	k1gMnPc2	mincmistr
v	v	k7c6	v
kutnohorském	kutnohorský	k2eAgInSc6d1	kutnohorský
Vlašském	vlašský	k2eAgInSc6d1	vlašský
dvoře	dvůr	k1gInSc6	dvůr
přibližně	přibližně	k6eAd1	přibližně
koncem	koncem	k7c2	koncem
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
na	na	k7c6	na
městské	městský	k2eAgFnSc6d1	městská
pečeti	pečeť	k1gFnSc6	pečeť
nacházel	nacházet	k5eAaImAgMnS	nacházet
jako	jako	k9	jako
symbol	symbol	k1gInSc4	symbol
královského	královský	k2eAgNnSc2d1	královské
města	město	k1gNnSc2	město
korunovaný	korunovaný	k2eAgInSc4d1	korunovaný
dvouocasý	dvouocasý	k2eAgInSc4d1	dvouocasý
lev	lev	k1gInSc4	lev
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zdejším	zdejší	k2eAgInSc6d1	zdejší
Gelnhausenově	Gelnhausenův	k2eAgInSc6d1	Gelnhausenův
kodexu	kodex	k1gInSc6	kodex
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
vyobrazení	vyobrazení	k1gNnSc1	vyobrazení
čtvrceného	čtvrcený	k2eAgInSc2d1	čtvrcený
městského	městský	k2eAgInSc2d1	městský
znaku	znak	k1gInSc2	znak
se	s	k7c7	s
dvěma	dva	k4xCgMnPc7	dva
ježky	ježek	k1gMnPc7	ježek
a	a	k8xC	a
dvěma	dva	k4xCgInPc7	dva
lvy	lev	k1gInPc7	lev
<g/>
.	.	kIx.	.
</s>
<s>
Důvod	důvod	k1gInSc1	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
Jihlavané	Jihlavan	k1gMnPc1	Jihlavan
vzali	vzít	k5eAaPmAgMnP	vzít
symbol	symbol	k1gInSc4	symbol
ježka	ježek	k1gMnSc2	ježek
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
jistý	jistý	k2eAgMnSc1d1	jistý
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
historika	historik	k1gMnSc2	historik
Františka	František	k1gMnSc2	František
Hoffmanna	Hoffmann	k1gMnSc2	Hoffmann
si	se	k3xPyFc3	se
němečtí	německý	k2eAgMnPc1d1	německý
obyvatelé	obyvatel	k1gMnPc1	obyvatel
vykládali	vykládat	k5eAaImAgMnP	vykládat
jméno	jméno	k1gNnSc4	jméno
svého	svůj	k3xOyFgNnSc2	svůj
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
Iglau	Iglaus	k1gInSc2	Iglaus
<g/>
)	)	kIx)	)
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
igel	igel	k1gInSc1	igel
(	(	kIx(	(
<g/>
ježek	ježek	k1gMnSc1	ježek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Symbol	symbol	k1gInSc1	symbol
ježka	ježek	k1gMnSc2	ježek
západního	západní	k2eAgMnSc2d1	západní
využívá	využívat	k5eAaImIp3nS	využívat
i	i	k8xC	i
místní	místní	k2eAgInSc1d1	místní
Pivovar	pivovar	k1gInSc1	pivovar
Jihlava	Jihlava	k1gFnSc1	Jihlava
či	či	k8xC	či
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
FC	FC	kA	FC
Vysočina	vysočina	k1gFnSc1	vysočina
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
.	.	kIx.	.
</s>
<s>
Vlajku	vlajka	k1gFnSc4	vlajka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
městu	město	k1gNnSc3	město
udělena	udělit	k5eAaPmNgFnS	udělit
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
kosmo	kosmo	k?	kosmo
dělený	dělený	k2eAgInSc1d1	dělený
list	list	k1gInSc1	list
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gNnSc6	jehož
horním	horní	k2eAgNnSc6d1	horní
bílém	bílý	k2eAgNnSc6d1	bílé
poli	pole	k1gNnSc6	pole
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
červený	červený	k2eAgMnSc1d1	červený
ježek	ježek	k1gMnSc1	ježek
a	a	k8xC	a
v	v	k7c6	v
dolním	dolní	k2eAgNnSc6d1	dolní
červeném	červený	k2eAgNnSc6d1	červené
poli	pole	k1gNnSc6	pole
český	český	k2eAgMnSc1d1	český
lev	lev	k1gMnSc1	lev
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
má	mít	k5eAaImIp3nS	mít
rozměr	rozměr	k1gInSc4	rozměr
šířky	šířka	k1gFnSc2	šířka
ku	k	k7c3	k
délce	délka	k1gFnSc3	délka
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
pěstovanými	pěstovaný	k2eAgFnPc7d1	pěstovaná
plodinami	plodina	k1gFnPc7	plodina
převažují	převažovat	k5eAaImIp3nP	převažovat
obilniny	obilnina	k1gFnSc2	obilnina
<g/>
,	,	kIx,	,
na	na	k7c4	na
polovinu	polovina	k1gFnSc4	polovina
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
pokleslo	poklesnout	k5eAaPmAgNnS	poklesnout
pěstování	pěstování	k1gNnSc1	pěstování
brambor	brambora	k1gFnPc2	brambora
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
se	se	k3xPyFc4	se
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
produkce	produkce	k1gFnSc1	produkce
řepky	řepka	k1gFnSc2	řepka
olejky	olejka	k1gFnSc2	olejka
<g/>
.	.	kIx.	.
</s>
<s>
Provozy	provoz	k1gInPc1	provoz
rostlinné	rostlinný	k2eAgFnSc2d1	rostlinná
výroby	výroba	k1gFnSc2	výroba
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
především	především	k9	především
v	v	k7c6	v
okrajových	okrajový	k2eAgFnPc6d1	okrajová
částech	část	k1gFnPc6	část
Jihlavy	Jihlava	k1gFnSc2	Jihlava
<g/>
,	,	kIx,	,
v	v	k7c6	v
Herolticích	Heroltice	k1gFnPc6	Heroltice
sídlí	sídlet	k5eAaImIp3nP	sídlet
firmy	firma	k1gFnPc1	firma
EUROFARMS	EUROFARMS	kA	EUROFARMS
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
a	a	k8xC	a
AGROSALES	AGROSALES	kA	AGROSALES
s.	s.	k?	s.
r.	r.	kA	r.
o.	o.	k?	o.
<g/>
,	,	kIx,	,
v	v	k7c4	v
Pístově	pístově	k6eAd1	pístově
Agrodružstvo	Agrodružstvo	k1gNnSc4	Agrodružstvo
Pístov	Pístovo	k1gNnPc2	Pístovo
a	a	k8xC	a
v	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
má	mít	k5eAaImIp3nS	mít
sídlo	sídlo	k1gNnSc4	sídlo
Zemědělské	zemědělský	k2eAgNnSc1d1	zemědělské
zásobování	zásobování	k1gNnSc1	zásobování
a	a	k8xC	a
nákup	nákup	k1gInSc4	nákup
v	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
Těžbou	těžba	k1gFnSc7	těžba
dřeva	dřevo	k1gNnSc2	dřevo
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
společnosti	společnost	k1gFnPc1	společnost
EDELVEJS	EDELVEJS	kA	EDELVEJS
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
a	a	k8xC	a
Lesní	lesní	k2eAgFnSc1d1	lesní
společnost	společnost	k1gFnSc1	společnost
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
živočišné	živočišný	k2eAgFnSc2d1	živočišná
výroby	výroba	k1gFnSc2	výroba
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
zastoupeny	zastoupit	k5eAaPmNgInP	zastoupit
podniky	podnik	k1gInPc7	podnik
Rybářství	rybářství	k1gNnSc1	rybářství
Růžička	růžička	k1gFnSc1	růžička
<g/>
,	,	kIx,	,
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
s	s	k7c7	s
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
chov	chov	k1gInSc4	chov
prasat	prase	k1gNnPc2	prase
Selma	Selma	k1gFnSc1	Selma
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g/>
,	,	kIx,	,
chov	chov	k1gInSc4	chov
drůbeže	drůbež	k1gFnSc2	drůbež
Gallus	Gallus	k1gInSc1	Gallus
Extra	extra	k2eAgInSc1d1	extra
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
a	a	k8xC	a
Biofarma	Biofarma	k1gFnSc1	Biofarma
Sasov	Sasov	k1gInSc1	Sasov
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
průmyslu	průmysl	k1gInSc2	průmysl
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
zastoupeno	zastoupit	k5eAaPmNgNnS	zastoupit
převážně	převážně	k6eAd1	převážně
strojírenství	strojírenství	k1gNnSc2	strojírenství
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
se	s	k7c7	s
specializací	specializace	k1gFnSc7	specializace
na	na	k7c4	na
automobilový	automobilový	k2eAgInSc4d1	automobilový
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
největší	veliký	k2eAgMnPc4d3	veliký
zaměstnavatele	zaměstnavatel	k1gMnPc4	zaměstnavatel
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
BOSCH	Bosch	kA	Bosch
Diesel	diesel	k1gInSc1	diesel
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
vstřikovací	vstřikovací	k2eAgInPc4d1	vstřikovací
systémy	systém	k1gInPc4	systém
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
okolo	okolo	k7c2	okolo
5	[number]	k4	5
000	[number]	k4	000
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
Automotive	Automotiv	k1gInSc5	Automotiv
Lighting	Lighting	k1gInSc4	Lighting
(	(	kIx(	(
<g/>
automobilové	automobilový	k2eAgInPc4d1	automobilový
světlomety	světlomet	k1gInPc4	světlomet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Motorpal	motorpal	k1gInSc1	motorpal
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
(	(	kIx(	(
<g/>
vstřikovací	vstřikovací	k2eAgInPc1d1	vstřikovací
systémy	systém	k1gInPc1	systém
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tesla	Tesla	k1gFnSc1	Tesla
Jihlava	Jihlava	k1gFnSc1	Jihlava
(	(	kIx(	(
<g/>
díly	díl	k1gInPc1	díl
pro	pro	k7c4	pro
automobilový	automobilový	k2eAgInSc4d1	automobilový
průmysl	průmysl	k1gInSc4	průmysl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Moravské	moravský	k2eAgFnSc2d1	Moravská
kovárny	kovárna	k1gFnSc2	kovárna
(	(	kIx(	(
<g/>
zápustkové	zápustkový	k2eAgInPc1d1	zápustkový
výkovky	výkovek	k1gInPc1	výkovek
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jihlavan	Jihlavan	k1gMnSc1	Jihlavan
(	(	kIx(	(
<g/>
letecké	letecký	k2eAgInPc4d1	letecký
přístroje	přístroj	k1gInPc4	přístroj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
strojírenské	strojírenský	k2eAgFnPc4d1	strojírenská
firmy	firma	k1gFnPc4	firma
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
AJAX	AJAX	kA	AJAX
PILNÍKY	pilník	k1gInPc1	pilník
<g/>
,	,	kIx,	,
a.	a.	k?	a.
s.	s.	k?	s.
na	na	k7c4	na
ulici	ulice	k1gFnSc4	ulice
Polenská	Polenský	k2eAgFnSc1d1	Polenská
<g/>
,	,	kIx,	,
FALCON	FALCON	kA	FALCON
v.o.s.	v.o.s.	k?	v.o.s.
vyrábějící	vyrábějící	k2eAgInPc4d1	vyrábějící
ventily	ventil	k1gInPc4	ventil
<g/>
,	,	kIx,	,
Global	globat	k5eAaImAgMnS	globat
<g/>
,	,	kIx,	,
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
s	s	k7c7	s
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
(	(	kIx(	(
<g/>
střešní	střešní	k2eAgInPc1d1	střešní
a	a	k8xC	a
izolační	izolační	k2eAgInPc1d1	izolační
systémy	systém	k1gInPc1	systém
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
SEPOS	SEPOS	kA	SEPOS
(	(	kIx(	(
<g/>
dveře	dveře	k1gFnPc1	dveře
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sklárny	sklárna	k1gFnSc2	sklárna
Bohemia	bohemia	k1gFnSc1	bohemia
Jihlava	Jihlava	k1gFnSc1	Jihlava
a.s.	a.s.	k?	a.s.
a	a	k8xC	a
Plastikov	Plastikov	k1gInSc1	Plastikov
(	(	kIx(	(
<g/>
plastová	plastový	k2eAgNnPc1d1	plastové
okna	okno	k1gNnPc1	okno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stavebnictví	stavebnictví	k1gNnSc1	stavebnictví
je	být	k5eAaImIp3nS	být
zastoupeno	zastoupit	k5eAaPmNgNnS	zastoupit
podniky	podnik	k1gInPc1	podnik
ACO	ACO	kA	ACO
Stavební	stavební	k2eAgInPc1d1	stavební
prvky	prvek	k1gInPc1	prvek
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
s	s	k7c7	s
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
PRIMONT	PRIMONT	kA	PRIMONT
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
<g/>
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
na	na	k7c4	na
Pávově	pávově	k6eAd1	pávově
<g/>
,	,	kIx,	,
POZEMNÍ	pozemní	k2eAgFnPc1d1	pozemní
STAVBY	stavba	k1gFnPc1	stavba
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
,	,	kIx,	,
Rieder	Rieder	k1gInSc1	Rieder
Beton	beton	k1gInSc1	beton
a	a	k8xC	a
PSJ	PSJ	kA	PSJ
<g/>
.	.	kIx.	.
</s>
<s>
Dřevařské	dřevařský	k2eAgFnSc3d1	dřevařská
výrobě	výroba	k1gFnSc3	výroba
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaPmIp3nS	věnovat
firma	firma	k1gFnSc1	firma
Kronospan	Kronospana	k1gFnPc2	Kronospana
<g/>
.	.	kIx.	.
</s>
<s>
Plavky	plavka	k1gFnPc1	plavka
a	a	k8xC	a
sportovní	sportovní	k2eAgNnSc1d1	sportovní
oblečení	oblečení	k1gNnSc1	oblečení
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
podnik	podnik	k1gInSc4	podnik
Modeta	Modeto	k1gNnSc2	Modeto
Style	styl	k1gInSc5	styl
<g/>
,	,	kIx,	,
pracovní	pracovní	k2eAgInPc4d1	pracovní
oděvy	oděv	k1gInPc4	oděv
pak	pak	k6eAd1	pak
firma	firma	k1gFnSc1	firma
Petex	Petex	k1gInSc1	Petex
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
potravinářství	potravinářství	k1gNnSc2	potravinářství
tu	tu	k6eAd1	tu
funguje	fungovat	k5eAaImIp3nS	fungovat
mlékárna	mlékárna	k1gFnSc1	mlékárna
Moravia	Moravia	k1gFnSc1	Moravia
Lacto	Lacto	k1gNnSc1	Lacto
<g/>
,	,	kIx,	,
Pivovar	pivovar	k1gInSc1	pivovar
Jihlava	Jihlava	k1gFnSc1	Jihlava
produkující	produkující	k2eAgNnSc4d1	produkující
pivo	pivo	k1gNnSc4	pivo
Ježek	Ježek	k1gMnSc1	Ježek
a	a	k8xC	a
pekárna	pekárna	k1gFnSc1	pekárna
Lapek	Lapky	k1gFnPc2	Lapky
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
tu	ten	k3xDgFnSc4	ten
sídlila	sídlit	k5eAaImAgFnS	sídlit
společnost	společnost	k1gFnSc1	společnost
Tchibo	Tchiba	k1gFnSc5	Tchiba
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc4	jejíž
káva	káva	k1gFnSc1	káva
–	–	k?	–
Jihlavanka	Jihlavanka	k1gFnSc1	Jihlavanka
–	–	k?	–
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc1	název
podle	podle	k7c2	podle
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
poloze	poloha	k1gFnSc3	poloha
Jihlavy	Jihlava	k1gFnSc2	Jihlava
uprostřed	uprostřed	k7c2	uprostřed
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
nedaleko	nedaleko	k7c2	nedaleko
dálnice	dálnice	k1gFnSc2	dálnice
D1	D1	k1gFnSc2	D1
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
hojně	hojně	k6eAd1	hojně
zastoupeny	zastoupen	k2eAgFnPc4d1	zastoupena
dopravní	dopravní	k2eAgFnPc4d1	dopravní
a	a	k8xC	a
logistické	logistický	k2eAgFnPc4d1	logistická
služby	služba	k1gFnPc4	služba
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
jejichž	jejichž	k3xOyRp3gMnPc4	jejichž
poskytovatele	poskytovatel	k1gMnPc4	poskytovatel
patří	patřit	k5eAaImIp3nS	patřit
Jipocar	Jipocar	k1gInSc1	Jipocar
<g/>
,	,	kIx,	,
GLS	GLS	kA	GLS
<g/>
,	,	kIx,	,
Toptrans	Toptrans	k1gInSc1	Toptrans
<g/>
,	,	kIx,	,
DPD	DPD	kA	DPD
<g/>
,	,	kIx,	,
ICOM	ICOM	kA	ICOM
transport	transport	k1gInSc1	transport
<g/>
,	,	kIx,	,
Geis	Geis	k1gInSc1	Geis
CZ	CZ	kA	CZ
<g/>
,	,	kIx,	,
ČD	ČD	kA	ČD
Cargo	Cargo	k1gNnSc1	Cargo
<g/>
,	,	kIx,	,
DHL	DHL	kA	DHL
Express	express	k1gInSc1	express
Czech	Czech	k1gInSc1	Czech
Republic	Republice	k1gFnPc2	Republice
<g/>
,	,	kIx,	,
Weindel	Weindlo	k1gNnPc2	Weindlo
Logistik	logistik	k1gMnSc1	logistik
Service	Service	k1gFnSc2	Service
ČR	ČR	kA	ČR
a	a	k8xC	a
ESATRANS	ESATRANS	kA	ESATRANS
Jihlava	Jihlava	k1gFnSc1	Jihlava
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
Vývoj	vývoj	k1gInSc1	vývoj
softwaru	software	k1gInSc2	software
nabízí	nabízet	k5eAaImIp3nS	nabízet
společnosti	společnost	k1gFnSc3	společnost
IT	IT	kA	IT
Stormware	Stormwar	k1gMnSc5	Stormwar
a	a	k8xC	a
Gordic	Gordice	k1gInPc2	Gordice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2014	[number]	k4	2014
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
Vědeckotechnický	vědeckotechnický	k2eAgInSc1d1	vědeckotechnický
park	park	k1gInSc1	park
a	a	k8xC	a
Centrum	centrum	k1gNnSc1	centrum
transferu	transfer	k1gInSc2	transfer
technologií	technologie	k1gFnPc2	technologie
Vysočina	vysočina	k1gFnSc1	vysočina
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
množství	množství	k1gNnSc1	množství
supermarketů	supermarket	k1gInPc2	supermarket
a	a	k8xC	a
obchodní	obchodní	k2eAgInPc4d1	obchodní
domy	dům	k1gInPc4	dům
Prior	prior	k1gMnSc1	prior
Jihlava	Jihlava	k1gFnSc1	Jihlava
a	a	k8xC	a
City	city	k1gNnSc1	city
Park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgInPc1d2	menší
obchody	obchod	k1gInPc1	obchod
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
své	svůj	k3xOyFgFnPc4	svůj
pobočky	pobočka	k1gFnPc4	pobočka
má	mít	k5eAaImIp3nS	mít
většina	většina	k1gFnSc1	většina
českých	český	k2eAgFnPc2d1	Česká
pojišťoven	pojišťovna	k1gFnPc2	pojišťovna
a	a	k8xC	a
bank	banka	k1gFnPc2	banka
<g/>
.	.	kIx.	.
</s>
<s>
Právní	právní	k2eAgFnPc4d1	právní
služby	služba	k1gFnPc4	služba
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
nespočet	nespočet	k1gInSc1	nespočet
advokátních	advokátní	k2eAgFnPc2d1	advokátní
kanceláří	kancelář	k1gFnPc2	kancelář
a	a	k8xC	a
notáři	notár	k1gMnPc1	notár
<g/>
.	.	kIx.	.
</s>
<s>
Nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
řada	řada	k1gFnSc1	řada
restauračních	restaurační	k2eAgNnPc2d1	restaurační
zařízení	zařízení	k1gNnPc2	zařízení
např.	např.	kA	např.
Buena	Buena	k1gFnSc1	Buena
Vista	vista	k2eAgFnSc1d1	vista
<g/>
,	,	kIx,	,
restaurace	restaurace	k1gFnSc1	restaurace
Hotelu	hotel	k1gInSc3	hotel
Gustav	Gustav	k1gMnSc1	Gustav
Mahler	Mahler	k1gMnSc1	Mahler
<g/>
,	,	kIx,	,
Pivovarská	pivovarský	k2eAgFnSc1d1	Pivovarská
restaurace	restaurace	k1gFnSc1	restaurace
<g/>
,	,	kIx,	,
Restaurace	restaurace	k1gFnSc1	restaurace
Milenium	milenium	k1gNnSc1	milenium
<g/>
,	,	kIx,	,
Radniční	radniční	k2eAgFnSc1d1	radniční
restaurace	restaurace	k1gFnSc1	restaurace
a	a	k8xC	a
pivovar	pivovar	k1gInSc1	pivovar
<g/>
,	,	kIx,	,
Restaurace	restaurace	k1gFnSc1	restaurace
Tři	tři	k4xCgMnPc1	tři
Knížata	kníže	k1gMnPc1wR	kníže
<g/>
,	,	kIx,	,
Restaurace	restaurace	k1gFnPc1	restaurace
U	u	k7c2	u
Jakuba	Jakub	k1gMnSc2	Jakub
či	či	k8xC	či
Peťanova	Peťanův	k2eAgFnSc1d1	Peťanův
pizza	pizza	k1gFnSc1	pizza
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
menší	malý	k2eAgFnPc4d2	menší
hospody	hospody	k?	hospody
<g/>
,	,	kIx,	,
vinárny	vinárna	k1gFnPc1	vinárna
<g/>
,	,	kIx,	,
kavárny	kavárna	k1gFnPc1	kavárna
<g/>
,	,	kIx,	,
bary	bar	k1gInPc1	bar
a	a	k8xC	a
cukrárny	cukrárna	k1gFnPc1	cukrárna
<g/>
.	.	kIx.	.
</s>
<s>
Rychlé	Rychlé	k2eAgNnSc1d1	Rychlé
občerstvení	občerstvení	k1gNnSc1	občerstvení
je	být	k5eAaImIp3nS	být
zastoupeno	zastoupit	k5eAaPmNgNnS	zastoupit
menšími	malý	k2eAgInPc7d2	menší
provozy	provoz	k1gInPc7	provoz
a	a	k8xC	a
bufety	bufet	k1gInPc7	bufet
a	a	k8xC	a
pobočkami	pobočka	k1gFnPc7	pobočka
řetězců	řetězec	k1gInPc2	řetězec
KFC	KFC	kA	KFC
a	a	k8xC	a
McDonald	McDonald	k1gMnSc1	McDonald
<g/>
'	'	kIx"	'
<g/>
s.	s.	k?	s.
Ubytování	ubytování	k1gNnSc6	ubytování
nabízejí	nabízet	k5eAaImIp3nP	nabízet
hotely	hotel	k1gInPc1	hotel
EA	EA	kA	EA
Business	business	k1gInSc1	business
hotel	hotel	k1gInSc1	hotel
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
,	,	kIx,	,
Grandhotel	grandhotel	k1gInSc1	grandhotel
Jihlava	Jihlava	k1gFnSc1	Jihlava
Garni	Garň	k1gMnSc3	Garň
<g/>
,	,	kIx,	,
Hotel	hotel	k1gInSc1	hotel
Gustav	Gustav	k1gMnSc1	Gustav
Mahler	Mahler	k1gMnSc1	Mahler
<g/>
,	,	kIx,	,
Hotel	hotel	k1gInSc1	hotel
Milenium	milenium	k1gNnSc1	milenium
a	a	k8xC	a
Villa	Villa	k1gFnSc1	Villa
Eden	Eden	k1gInSc1	Eden
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
penzionů	penzion	k1gInPc2	penzion
<g/>
.	.	kIx.	.
</s>
<s>
Jihlava	Jihlava	k1gFnSc1	Jihlava
je	být	k5eAaImIp3nS	být
celostátně	celostátně	k6eAd1	celostátně
až	až	k9	až
mezinárodně	mezinárodně	k6eAd1	mezinárodně
významným	významný	k2eAgInSc7d1	významný
silničním	silniční	k2eAgInSc7d1	silniční
uzlem	uzel	k1gInSc7	uzel
<g/>
,	,	kIx,	,
kříží	křížit	k5eAaImIp3nP	křížit
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
dálnice	dálnice	k1gFnSc1	dálnice
D1	D1	k1gFnSc2	D1
(	(	kIx(	(
<g/>
E	E	kA	E
<g/>
50	[number]	k4	50
<g/>
,	,	kIx,	,
E	E	kA	E
<g/>
65	[number]	k4	65
<g/>
)	)	kIx)	)
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Brno	Brno	k1gNnSc1	Brno
a	a	k8xC	a
dálková	dálkový	k2eAgFnSc1d1	dálková
silnice	silnice	k1gFnSc1	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
38	[number]	k4	38
od	od	k7c2	od
Kolína	Kolín	k1gInSc2	Kolín
<g/>
,	,	kIx,	,
po	po	k7c6	po
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
vedena	vést	k5eAaImNgFnS	vést
evropská	evropský	k2eAgFnSc1d1	Evropská
silnice	silnice	k1gFnSc1	silnice
E59	E59	k1gFnPc2	E59
směr	směr	k1gInSc4	směr
Znojmo	Znojmo	k1gNnSc1	Znojmo
a	a	k8xC	a
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
jihlavského	jihlavský	k2eAgInSc2d1	jihlavský
železničního	železniční	k2eAgInSc2d1	železniční
uzlu	uzel	k1gInSc2	uzel
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
jen	jen	k6eAd1	jen
regionální	regionální	k2eAgFnSc1d1	regionální
<g/>
.	.	kIx.	.
</s>
<s>
Katastrem	katastr	k1gInSc7	katastr
probíhá	probíhat	k5eAaImIp3nS	probíhat
asi	asi	k9	asi
8	[number]	k4	8
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
města	město	k1gNnSc2	město
dálnice	dálnice	k1gFnSc2	dálnice
D1	D1	k1gFnSc2	D1
s	s	k7c7	s
exitem	exit	k1gInSc7	exit
112	[number]	k4	112
<g/>
.	.	kIx.	.
</s>
<s>
Exit	exit	k1gInSc1	exit
s	s	k7c7	s
městem	město	k1gNnSc7	město
spojuje	spojovat	k5eAaImIp3nS	spojovat
čtyřpruhový	čtyřpruhový	k2eAgInSc1d1	čtyřpruhový
přivaděč	přivaděč	k1gInSc1	přivaděč
silnice	silnice	k1gFnSc2	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
38	[number]	k4	38
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vede	vést	k5eAaImIp3nS	vést
ze	z	k7c2	z
Štok	štok	k1gInSc4	štok
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
z	z	k7c2	z
Jihlavy	Jihlava	k1gFnSc2	Jihlava
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Moravské	moravský	k2eAgInPc4d1	moravský
Budějovice	Budějovice	k1gInPc4	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
výhodnému	výhodný	k2eAgNnSc3d1	výhodné
silničnímu	silniční	k2eAgNnSc3d1	silniční
spojení	spojení	k1gNnSc3	spojení
má	mít	k5eAaImIp3nS	mít
Jihlava	Jihlava	k1gFnSc1	Jihlava
množství	množství	k1gNnSc2	množství
autobusových	autobusový	k2eAgInPc2d1	autobusový
spojů	spoj	k1gInPc2	spoj
do	do	k7c2	do
blízkého	blízký	k2eAgNnSc2d1	blízké
okolí	okolí	k1gNnSc2	okolí
i	i	k9	i
do	do	k7c2	do
celé	celý	k2eAgFnSc2d1	celá
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Silnice	silnice	k1gFnSc1	silnice
II	II	kA	II
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
352	[number]	k4	352
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
Pávov	Pávov	k1gInSc1	Pávov
–	–	k?	–
Polná	Polná	k1gFnSc1	Polná
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
405	[number]	k4	405
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
Jihlava	Jihlava	k1gFnSc1	Jihlava
–	–	k?	–
Brtnice	Brtnice	k1gFnSc2	Brtnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
406	[number]	k4	406
ze	z	k7c2	z
silnice	silnice	k1gFnSc2	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
602	[number]	k4	602
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Třešť	třeštit	k5eAaImRp2nS	třeštit
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
<g />
.	.	kIx.	.
</s>
<s>
523	[number]	k4	523
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
Rančířov	Rančířov	k1gInSc1	Rančířov
–	–	k?	–
Jihlava	Jihlava	k1gFnSc1	Jihlava
–	–	k?	–
Větrný	větrný	k2eAgInSc1d1	větrný
Jeníkov	Jeníkov	k1gInSc1	Jeníkov
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
602	[number]	k4	602
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
Velký	velký	k2eAgInSc1d1	velký
Beranov	Beranov	k1gInSc1	Beranov
–	–	k?	–
Jihlava	Jihlava	k1gFnSc1	Jihlava
–	–	k?	–
Vyskytná	Vyskytný	k2eAgFnSc1d1	Vyskytná
Silnice	silnice	k1gFnSc1	silnice
III	III	kA	III
<g/>
.	.	kIx.	.
třídy	třída	k1gFnPc1	třída
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
24	[number]	k4	24
ze	z	k7c2	z
silnice	silnice	k1gFnSc2	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
602	[number]	k4	602
na	na	k7c4	na
Malý	malý	k2eAgInSc4d1	malý
Beranov	Beranov	k1gInSc4	Beranov
III	III	kA	III
<g />
.	.	kIx.	.
</s>
<s>
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
1945	[number]	k4	1945
Jihlava	Jihlava	k1gFnSc1	Jihlava
–	–	k?	–
Rantířov	Rantířov	k1gInSc1	Rantířov
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
3824	[number]	k4	3824
Jihlava	Jihlava	k1gFnSc1	Jihlava
–	–	k?	–
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
38	[number]	k4	38
–	–	k?	–
Hybrálec	Hybrálec	k1gMnSc1	Hybrálec
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
1311	[number]	k4	1311
Smrčná	Smrčný	k2eAgFnSc1d1	Smrčná
–	–	k?	–
Lesnov	Lesnov	k1gInSc1	Lesnov
(	(	kIx(	(
<g/>
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
38	[number]	k4	38
<g/>
)	)	kIx)	)
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
13112	[number]	k4	13112
ze	z	k7c2	z
silnice	silnice	k1gFnSc2	silnice
II	II	kA	II
<g />
.	.	kIx.	.
</s>
<s>
<g/>
/	/	kIx~	/
<g/>
523	[number]	k4	523
na	na	k7c4	na
Plandry	Plandr	k1gInPc4	Plandr
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
4062	[number]	k4	4062
Jihlava	Jihlava	k1gFnSc1	Jihlava
–	–	k?	–
Pístov	Pístov	k1gInSc1	Pístov
(	(	kIx(	(
<g/>
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
38	[number]	k4	38
<g/>
)	)	kIx)	)
–	–	k?	–
Popice	Popice	k1gFnSc2	Popice
–	–	k?	–
Salavice	Salavice	k1gFnSc2	Salavice
Železniční	železniční	k2eAgNnSc1d1	železniční
spojení	spojení	k1gNnSc1	spojení
Jihlava	Jihlava	k1gFnSc1	Jihlava
získala	získat	k5eAaPmAgFnS	získat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1871	[number]	k4	1871
(	(	kIx(	(
<g/>
Rakouská	rakouský	k2eAgFnSc1d1	rakouská
severozápadní	severozápadní	k2eAgFnSc1d1	severozápadní
dráha	dráha	k1gFnSc1	dráha
z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
přes	přes	k7c4	přes
Znojmo	Znojmo	k1gNnSc4	Znojmo
<g/>
,	,	kIx,	,
Kolín	Kolín	k1gInSc1	Kolín
a	a	k8xC	a
Nymburk	Nymburk	k1gInSc1	Nymburk
do	do	k7c2	do
Děčína	Děčín	k1gInSc2	Děčín
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
Jihlava	Jihlava	k1gFnSc1	Jihlava
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
železniční	železniční	k2eAgFnSc6d1	železniční
trati	trať	k1gFnSc6	trať
240	[number]	k4	240
Brno	Brno	k1gNnSc4	Brno
–	–	k?	–
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
napojuje	napojovat	k5eAaImIp3nS	napojovat
na	na	k7c4	na
elektrifikovanou	elektrifikovaný	k2eAgFnSc4d1	elektrifikovaná
trať	trať	k1gFnSc4	trať
225	[number]	k4	225
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
–	–	k?	–
Veselí	veselí	k1gNnSc1	veselí
nad	nad	k7c7	nad
Lužnicí	Lužnice	k1gFnSc7	Lužnice
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
leží	ležet	k5eAaImIp3nS	ležet
také	také	k9	také
druhé	druhý	k4xOgNnSc1	druhý
nádraží	nádraží	k1gNnSc1	nádraží
Jihlava	Jihlava	k1gFnSc1	Jihlava
město	město	k1gNnSc1	město
a	a	k8xC	a
zastávky	zastávka	k1gFnPc1	zastávka
Jihlava-Staré	Jihlava-Starý	k2eAgFnSc2d1	Jihlava-Starý
Hory	hora	k1gFnSc2	hora
a	a	k8xC	a
Jihlava-Bosch	Jihlava-Bosch	k1gMnSc1	Jihlava-Bosch
Diesel	diesel	k1gInSc1	diesel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
zastavují	zastavovat	k5eAaImIp3nP	zastavovat
vnitrostátní	vnitrostátní	k2eAgInSc4d1	vnitrostátní
rychlík	rychlík	k1gInSc4	rychlík
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
Brno	Brno	k1gNnSc1	Brno
–	–	k?	–
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
–	–	k?	–
Plzeň	Plzeň	k1gFnSc1	Plzeň
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
veden	vést	k5eAaImNgInS	vést
jeden	jeden	k4xCgInSc1	jeden
pár	pár	k1gInSc1	pár
přímých	přímý	k2eAgMnPc2d1	přímý
rychlíků	rychlík	k1gInPc2	rychlík
Jihlava	Jihlava	k1gFnSc1	Jihlava
–	–	k?	–
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Letiště	letiště	k1gNnSc1	letiště
Jihlava	Jihlava	k1gFnSc1	Jihlava
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
části	část	k1gFnSc6	část
Henčov	Henčov	k1gInSc4	Henčov
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
veřejné	veřejný	k2eAgNnSc4d1	veřejné
vnitrostátní	vnitrostátní	k2eAgNnSc4d1	vnitrostátní
letiště	letiště	k1gNnSc4	letiště
s	s	k7c7	s
nezpevněným	zpevněný	k2eNgInSc7d1	nezpevněný
povrchem	povrch	k1gInSc7	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
<g/>
,	,	kIx,	,
Trolejbusová	trolejbusový	k2eAgFnSc1d1	trolejbusová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
a	a	k8xC	a
Městská	městský	k2eAgFnSc1d1	městská
autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
tramvaje	tramvaj	k1gFnPc1	tramvaj
vyjely	vyjet	k5eAaPmAgFnP	vyjet
do	do	k7c2	do
jihlavských	jihlavský	k2eAgFnPc2d1	Jihlavská
ulic	ulice	k1gFnPc2	ulice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
<g/>
.	.	kIx.	.
</s>
<s>
Jediná	jediný	k2eAgFnSc1d1	jediná
<g/>
,	,	kIx,	,
2,7	[number]	k4	2,7
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
trať	trať	k1gFnSc1	trať
spojovala	spojovat	k5eAaImAgFnS	spojovat
dnešní	dnešní	k2eAgNnSc4d1	dnešní
Masarykovo	Masarykův	k2eAgNnSc4d1	Masarykovo
náměstí	náměstí	k1gNnSc4	náměstí
s	s	k7c7	s
nádražím	nádraží	k1gNnSc7	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
Provoz	provoz	k1gInSc1	provoz
tramvají	tramvaj	k1gFnPc2	tramvaj
byl	být	k5eAaImAgInS	být
zastaven	zastavit	k5eAaPmNgInS	zastavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
moderní	moderní	k2eAgInPc1d1	moderní
trolejbusy	trolejbus	k1gInPc1	trolejbus
měly	mít	k5eAaImAgInP	mít
nahradit	nahradit	k5eAaPmF	nahradit
zastaralou	zastaralý	k2eAgFnSc4d1	zastaralá
tramvajovou	tramvajový	k2eAgFnSc4d1	tramvajová
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
první	první	k4xOgFnSc1	první
trať	trať	k1gFnSc1	trať
vedla	vést	k5eAaImAgFnS	vést
po	po	k7c6	po
trase	trasa	k1gFnSc6	trasa
té	ten	k3xDgFnSc6	ten
tramvajové	tramvajový	k2eAgFnSc6d1	tramvajová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
trolejbusová	trolejbusový	k2eAgFnSc1d1	trolejbusová
síť	síť	k1gFnSc1	síť
úspěšně	úspěšně	k6eAd1	úspěšně
rozrůstala	rozrůstat	k5eAaImAgFnS	rozrůstat
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
ale	ale	k8xC	ale
přišla	přijít	k5eAaPmAgFnS	přijít
stagnace	stagnace	k1gFnSc1	stagnace
<g/>
.	.	kIx.	.
</s>
<s>
Opětovný	opětovný	k2eAgInSc1d1	opětovný
rozvoj	rozvoj	k1gInSc1	rozvoj
je	být	k5eAaImIp3nS	být
datován	datovat	k5eAaImNgInS	datovat
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Trolejbusové	trolejbusový	k2eAgFnPc1d1	trolejbusová
linky	linka	k1gFnPc1	linka
jsou	být	k5eAaImIp3nP	být
označeny	označen	k2eAgFnPc1d1	označena
písmeny	písmeno	k1gNnPc7	písmeno
(	(	kIx(	(
<g/>
A	A	kA	A
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
,	,	kIx,	,
BI	BI	kA	BI
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
,	,	kIx,	,
E	E	kA	E
a	a	k8xC	a
N	N	kA	N
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
v	v	k7c6	v
jiných	jiný	k2eAgNnPc6d1	jiné
městech	město	k1gNnPc6	město
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
není	být	k5eNaImIp3nS	být
u	u	k7c2	u
povrchové	povrchový	k2eAgFnSc2d1	povrchová
dopravy	doprava	k1gFnSc2	doprava
obvyklé	obvyklý	k2eAgFnSc2d1	obvyklá
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
zřizuje	zřizovat	k5eAaImIp3nS	zřizovat
jako	jako	k8xS	jako
své	svůj	k3xOyFgFnSc2	svůj
příspěvkové	příspěvkový	k2eAgFnSc2d1	příspěvková
organizace	organizace	k1gFnSc2	organizace
jedny	jeden	k4xCgFnPc4	jeden
jesle	jesle	k1gFnPc4	jesle
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
40	[number]	k4	40
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
mateřské	mateřský	k2eAgFnPc4d1	mateřská
školy	škola	k1gFnPc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
Mozaika	mozaika	k1gFnSc1	mozaika
Jihlava	Jihlava	k1gFnSc1	Jihlava
má	mít	k5eAaImIp3nS	mít
18	[number]	k4	18
odloučených	odloučený	k2eAgNnPc2d1	odloučené
pracovišť	pracoviště	k1gNnPc2	pracoviště
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
bylo	být	k5eAaImAgNnS	být
1600	[number]	k4	1600
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
a	a	k8xC	a
Mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
Speciálně	speciálně	k6eAd1	speciálně
pedagogické	pedagogický	k2eAgNnSc1d1	pedagogické
centrum	centrum	k1gNnSc1	centrum
Jihlava	Jihlava	k1gFnSc1	Jihlava
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Demlova	Demlův	k2eAgFnSc1d1	Demlova
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
pro	pro	k7c4	pro
120	[number]	k4	120
dětí	dítě	k1gFnPc2	dítě
s	s	k7c7	s
mentálním	mentální	k2eAgMnSc7d1	mentální
<g/>
,	,	kIx,	,
zrakovým	zrakový	k2eAgMnSc7d1	zrakový
<g/>
,	,	kIx,	,
tělesným	tělesný	k2eAgMnSc7d1	tělesný
<g/>
,	,	kIx,	,
sluchovým	sluchový	k2eAgNnSc7d1	sluchové
a	a	k8xC	a
řečovým	řečový	k2eAgNnSc7d1	řečové
postižením	postižení	k1gNnSc7	postižení
<g/>
.	.	kIx.	.
</s>
<s>
Nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
rovněž	rovněž	k9	rovněž
tři	tři	k4xCgFnPc1	tři
soukromé	soukromý	k2eAgFnPc1d1	soukromá
mateřinky	mateřinka	k1gFnPc1	mateřinka
–	–	k?	–
Mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
Kvítek	kvítek	k1gInSc1	kvítek
<g/>
,	,	kIx,	,
Mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
TINY	Tina	k1gFnSc2	Tina
WORLD	WORLD	kA	WORLD
a	a	k8xC	a
Soukromé	soukromý	k2eAgNnSc1d1	soukromé
centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
Chaloupka	Chaloupka	k1gMnSc1	Chaloupka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
školního	školní	k2eAgInSc2d1	školní
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
<g/>
/	/	kIx~	/
<g/>
2014	[number]	k4	2014
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
soukromá	soukromý	k2eAgFnSc1d1	soukromá
školka	školka	k1gFnSc1	školka
Meruzalka	meruzalka	k1gFnSc1	meruzalka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
zásadami	zásada	k1gFnPc7	zásada
Montessori	Montessor	k1gFnSc2	Montessor
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
město	město	k1gNnSc1	město
zřizuje	zřizovat	k5eAaImIp3nS	zřizovat
10	[number]	k4	10
základních	základní	k2eAgFnPc2d1	základní
škol	škola	k1gFnPc2	škola
(	(	kIx(	(
<g/>
ZŠ	ZŠ	kA	ZŠ
Otokara	Otokar	k1gMnSc2	Otokar
Březiny	Březina	k1gMnSc2	Březina
<g/>
,	,	kIx,	,
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
T.G.	T.G.	k1gMnSc2	T.G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
,	,	kIx,	,
na	na	k7c6	na
ulicích	ulice	k1gFnPc6	ulice
Demlova	Demlův	k2eAgFnSc1d1	Demlova
<g/>
,	,	kIx,	,
E.	E.	kA	E.
Rošického	Rošický	k2eAgInSc2d1	Rošický
<g/>
,	,	kIx,	,
Havlíčkova	Havlíčkův	k2eAgInSc2d1	Havlíčkův
<g/>
,	,	kIx,	,
Kollárova	Kollárův	k2eAgInSc2d1	Kollárův
<g/>
,	,	kIx,	,
Křížová	Křížová	k1gFnSc1	Křížová
<g/>
,	,	kIx,	,
Nad	nad	k7c7	nad
Plovárnou	plovárna	k1gFnSc7	plovárna
<g/>
,	,	kIx,	,
Jungmannova	Jungmannův	k2eAgFnSc1d1	Jungmannova
a	a	k8xC	a
Seifertova	Seifertův	k2eAgFnSc1d1	Seifertova
<g/>
)	)	kIx)	)
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
Základní	základní	k2eAgFnSc4d1	základní
školu	škola	k1gFnSc4	škola
speciální	speciální	k2eAgFnSc4d1	speciální
a	a	k8xC	a
Praktickou	praktický	k2eAgFnSc4d1	praktická
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Funguje	fungovat	k5eAaImIp3nS	fungovat
tu	tu	k6eAd1	tu
rovněž	rovněž	k9	rovněž
základní	základní	k2eAgFnSc1d1	základní
umělecká	umělecký	k2eAgFnSc1d1	umělecká
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
Jihlava	Jihlava	k1gFnSc1	Jihlava
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Svobody	svoboda	k1gFnSc2	svoboda
tu	tu	k6eAd1	tu
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
jejím	její	k3xOp3gMnSc7	její
zřizovatelem	zřizovatel	k1gMnSc7	zřizovatel
je	být	k5eAaImIp3nS	být
biskupství	biskupství	k1gNnSc1	biskupství
brněnské	brněnský	k2eAgNnSc1d1	brněnské
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
sídlí	sídlet	k5eAaImIp3nS	sídlet
6	[number]	k4	6
státních	státní	k2eAgFnPc2d1	státní
středních	střední	k2eAgFnPc2d1	střední
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
Gymnázium	gymnázium	k1gNnSc1	gymnázium
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
,	,	kIx,	,
Střední	střední	k2eAgFnSc1d1	střední
uměleckoprůmyslová	uměleckoprůmyslový	k2eAgFnSc1d1	uměleckoprůmyslová
škola	škola	k1gFnSc1	škola
v	v	k7c6	v
Heleníně	Helenína	k1gFnSc6	Helenína
<g/>
,	,	kIx,	,
Střední	střední	k2eAgFnSc1d1	střední
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
sociální	sociální	k2eAgFnSc1d1	sociální
u	u	k7c2	u
Matky	matka	k1gFnSc2	matka
Boží	boží	k2eAgFnSc2d1	boží
v	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
<g/>
,	,	kIx,	,
Střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
stavební	stavební	k2eAgFnSc1d1	stavební
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
školnímu	školní	k2eAgInSc3d1	školní
roku	rok	k1gInSc3	rok
2014	[number]	k4	2014
<g/>
/	/	kIx~	/
<g/>
2015	[number]	k4	2015
Kraj	kraj	k1gInSc4	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
sloučil	sloučit	k5eAaPmAgInS	sloučit
šest	šest	k4xCc4	šest
středních	střední	k2eAgFnPc2d1	střední
škol	škola	k1gFnPc2	škola
do	do	k7c2	do
dvou	dva	k4xCgNnPc2	dva
vzdělávacích	vzdělávací	k2eAgNnPc2d1	vzdělávací
center	centrum	k1gNnPc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgMnSc7	ten
prvním	první	k4xOgMnSc7	první
je	být	k5eAaImIp3nS	být
Obchodní	obchodní	k2eAgFnSc1d1	obchodní
akademie	akademie	k1gFnSc1	akademie
<g/>
,	,	kIx,	,
Střední	střední	k2eAgFnSc1d1	střední
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
Střední	střední	k2eAgFnSc1d1	střední
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
služeb	služba	k1gFnPc2	služba
a	a	k8xC	a
Jazyková	jazykový	k2eAgFnSc1d1	jazyková
škola	škola	k1gFnSc1	škola
s	s	k7c7	s
právem	právo	k1gNnSc7	právo
státní	státní	k2eAgFnSc2d1	státní
jazykové	jazykový	k2eAgFnSc2d1	jazyková
zkoušky	zkouška	k1gFnSc2	zkouška
Jihlava	Jihlava	k1gFnSc1	Jihlava
(	(	kIx(	(
<g/>
OZŠ	OZŠ	kA	OZŠ
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
)	)	kIx)	)
a	a	k8xC	a
druhé	druhý	k4xOgNnSc1	druhý
vzdělávací	vzdělávací	k2eAgNnSc1d1	vzdělávací
centrum	centrum	k1gNnSc1	centrum
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
<g/>
,	,	kIx,	,
technická	technický	k2eAgFnSc1d1	technická
a	a	k8xC	a
automobilní	automobilní	k2eAgFnSc1d1	automobilní
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
zde	zde	k6eAd1	zde
funguje	fungovat	k5eAaImIp3nS	fungovat
6	[number]	k4	6
soukromých	soukromý	k2eAgFnPc2d1	soukromá
středních	střední	k2eAgFnPc2d1	střední
škol	škola	k1gFnPc2	škola
–	–	k?	–
Manažerská	manažerský	k2eAgFnSc1d1	manažerská
akademie	akademie	k1gFnSc1	akademie
<g/>
,	,	kIx,	,
Soukromá	soukromý	k2eAgFnSc1d1	soukromá
střední	střední	k2eAgFnSc1d1	střední
umělecká	umělecký	k2eAgFnSc1d1	umělecká
škola	škola	k1gFnSc1	škola
grafická	grafický	k2eAgFnSc1d1	grafická
<g/>
,	,	kIx,	,
Soukromé	soukromý	k2eAgNnSc1d1	soukromé
gymnázium	gymnázium	k1gNnSc1	gymnázium
AD	ad	k7c4	ad
FONTES	FONTES	kA	FONTES
<g/>
,	,	kIx,	,
Škola	škola	k1gFnSc1	škola
ekonomiky	ekonomika	k1gFnSc2	ekonomika
a	a	k8xC	a
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
<g/>
,	,	kIx,	,
TRIVIS	TRIVIS	kA	TRIVIS
–	–	k?	–
Střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
veřejnoprávní	veřejnoprávní	k2eAgFnSc1d1	veřejnoprávní
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
,	,	kIx,	,
FARMEKO	FARMEKO	kA	FARMEKO
–	–	k?	–
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
a	a	k8xC	a
Střední	střední	k2eAgFnSc1d1	střední
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Služby	služba	k1gFnPc1	služba
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
i	i	k9	i
čtyři	čtyři	k4xCgFnPc1	čtyři
vyšší	vysoký	k2eAgFnPc1d2	vyšší
odborné	odborný	k2eAgFnPc1d1	odborná
školy	škola	k1gFnPc1	škola
–	–	k?	–
Soukromá	soukromý	k2eAgFnSc1d1	soukromá
vyšší	vysoký	k2eAgFnSc1d2	vyšší
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
sociální	sociální	k2eAgFnSc1d1	sociální
<g/>
,	,	kIx,	,
Soukromá	soukromý	k2eAgFnSc1d1	soukromá
vyšší	vysoký	k2eAgFnSc1d2	vyšší
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
grafická	grafický	k2eAgFnSc1d1	grafická
<g/>
,	,	kIx,	,
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
policejní	policejní	k2eAgFnSc1d1	policejní
škola	škola	k1gFnSc1	škola
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
v	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Tolstého	Tolstý	k2eAgNnSc2d1	Tolstý
stojí	stát	k5eAaImIp3nS	stát
areál	areál	k1gInSc1	areál
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
polytechnické	polytechnický	k2eAgFnSc2d1	polytechnická
Jihlava	Jihlava	k1gFnSc1	Jihlava
(	(	kIx(	(
<g/>
VŠPJ	VŠPJ	kA	VŠPJ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
veřejnou	veřejný	k2eAgFnSc7d1	veřejná
vysokou	vysoký	k2eAgFnSc7d1	vysoká
školou	škola	k1gFnSc7	škola
neuniverzitního	univerzitní	k2eNgInSc2d1	neuniverzitní
typu	typ	k1gInSc2	typ
v	v	k7c6	v
Kraji	kraj	k1gInSc6	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dům	dům	k1gInSc1	dům
Gustava	Gustav	k1gMnSc2	Gustav
Mahlera	Mahler	k1gMnSc2	Mahler
<g/>
.	.	kIx.	.
</s>
<s>
Horácké	horácký	k2eAgNnSc1d1	Horácké
divadlo	divadlo	k1gNnSc1	divadlo
Jihlava	Jihlava	k1gFnSc1	Jihlava
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
a	a	k8xC	a
malou	malý	k2eAgFnSc7d1	malá
scénou	scéna	k1gFnSc7	scéna
<g/>
,	,	kIx,	,
divadelním	divadelní	k2eAgInSc7d1	divadelní
klubem	klub	k1gInSc7	klub
<g/>
,	,	kIx,	,
kavárnou	kavárna	k1gFnSc7	kavárna
a	a	k8xC	a
Jazz	jazz	k1gInSc1	jazz
Clubem	club	k1gInSc7	club
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Komenského	Komenského	k2eAgFnSc6d1	Komenského
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
Na	na	k7c6	na
Kopečku	kopeček	k1gInSc6	kopeček
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
psychiatrické	psychiatrický	k2eAgFnSc2d1	psychiatrická
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
amatérské	amatérský	k2eAgNnSc1d1	amatérské
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prostory	prostora	k1gFnPc1	prostora
slouží	sloužit	k5eAaImIp3nP	sloužit
též	též	k9	též
k	k	k7c3	k
výstavám	výstava	k1gFnPc3	výstava
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
děl	dělo	k1gNnPc2	dělo
či	či	k8xC	či
k	k	k7c3	k
pořádání	pořádání	k1gNnSc3	pořádání
besed	beseda	k1gFnPc2	beseda
a	a	k8xC	a
koncertů	koncert	k1gInPc2	koncert
<g/>
.	.	kIx.	.
</s>
<s>
DIOD	dioda	k1gFnPc2	dioda
–	–	k?	–
Divadlo	divadlo	k1gNnSc4	divadlo
otevřených	otevřený	k2eAgFnPc2d1	otevřená
dveří	dveře	k1gFnPc2	dveře
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
bývalého	bývalý	k2eAgInSc2d1	bývalý
kina	kino	k1gNnPc1	kino
Sokol	Sokol	k1gMnSc1	Sokol
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc7	jeho
provozovatelem	provozovatel	k1gMnSc7	provozovatel
je	být	k5eAaImIp3nS	být
Tělocvičná	tělocvičný	k2eAgFnSc1d1	Tělocvičná
jednota	jednota	k1gFnSc1	jednota
Sokol	Sokol	k1gInSc1	Sokol
v	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
na	na	k7c6	na
programu	program	k1gInSc6	program
má	mít	k5eAaImIp3nS	mít
široké	široký	k2eAgNnSc4d1	široké
spektrum	spektrum	k1gNnSc4	spektrum
uměleckých	umělecký	k2eAgFnPc2d1	umělecká
činností	činnost	k1gFnPc2	činnost
od	od	k7c2	od
divadla	divadlo	k1gNnSc2	divadlo
přes	přes	k7c4	přes
tanec	tanec	k1gInSc4	tanec
po	po	k7c4	po
besedy	beseda	k1gFnPc4	beseda
a	a	k8xC	a
workshopy	workshop	k1gInPc4	workshop
<g/>
.	.	kIx.	.
</s>
<s>
Nezávislé	závislý	k2eNgNnSc1d1	nezávislé
divadelní	divadelní	k2eAgNnSc1d1	divadelní
sdružení	sdružení	k1gNnSc1	sdružení
De	De	k?	De
Facto	Facto	k1gNnSc4	Facto
Mimo	mimo	k6eAd1	mimo
funguje	fungovat	k5eAaImIp3nS	fungovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
se	se	k3xPyFc4	se
řady	řada	k1gFnPc1	řada
divadelních	divadelní	k2eAgMnPc2d1	divadelní
festivalů	festival	k1gInPc2	festival
jak	jak	k8xS	jak
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
T.E.J.	T.E.J.	k1gFnSc2	T.E.J.
<g/>
P.	P.	kA	P.
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
se	se	k3xPyFc4	se
na	na	k7c4	na
pohybové	pohybový	k2eAgNnSc4d1	pohybové
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
,	,	kIx,	,
klauniádu	klauniáda	k1gFnSc4	klauniáda
<g/>
,	,	kIx,	,
cirkusové	cirkusový	k2eAgInPc4d1	cirkusový
výstupy	výstup	k1gInPc4	výstup
a	a	k8xC	a
akrobacii	akrobacie	k1gFnSc4	akrobacie
<g/>
.	.	kIx.	.
</s>
<s>
Nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
hrají	hrát	k5eAaImIp3nP	hrát
BOBO	BOBO	kA	BOBO
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
a	a	k8xC	a
Dětské	dětský	k2eAgNnSc4d1	dětské
karnevalové	karnevalový	k2eAgNnSc4d1	karnevalové
divadlo	divadlo	k1gNnSc4	divadlo
JEŽEK	Ježek	k1gMnSc1	Ježek
založené	založený	k2eAgInPc1d1	založený
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
fungují	fungovat	k5eAaImIp3nP	fungovat
dvě	dva	k4xCgNnPc1	dva
stálá	stálý	k2eAgNnPc1d1	stálé
kina	kino	k1gNnPc1	kino
–	–	k?	–
multikino	multikino	k1gNnSc1	multikino
Cinestar	Cinestar	k1gMnSc1	Cinestar
v	v	k7c6	v
obchodním	obchodní	k2eAgNnSc6d1	obchodní
centru	centrum	k1gNnSc6	centrum
City	City	k1gFnSc2	City
Park	park	k1gInSc4	park
a	a	k8xC	a
kino	kino	k1gNnSc4	kino
Dukla	Dukla	k1gFnSc1	Dukla
s	s	k7c7	s
filmovým	filmový	k2eAgInSc7d1	filmový
klubem	klub	k1gInSc7	klub
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parku	park	k1gInSc6	park
Heulos	Heulosa	k1gFnPc2	Heulosa
stojí	stát	k5eAaImIp3nS	stát
letní	letní	k2eAgNnSc4d1	letní
kino	kino	k1gNnSc4	kino
<g/>
.	.	kIx.	.
</s>
<s>
Hudbě	hudba	k1gFnSc3	hudba
jsou	být	k5eAaImIp3nP	být
zasvěceny	zasvěcen	k2eAgInPc1d1	zasvěcen
menší	malý	k2eAgInPc1d2	menší
tematicky	tematicky	k6eAd1	tematicky
zaměřené	zaměřený	k2eAgInPc1d1	zaměřený
kluby	klub	k1gInPc1	klub
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Mystic	Mystice	k1gFnPc2	Mystice
Club	club	k1gInSc1	club
<g/>
,	,	kIx,	,
Club	club	k1gInSc1	club
Xanadu	Xanad	k1gInSc2	Xanad
či	či	k8xC	či
SOUL	Soul	k1gInSc1	Soul
Music	Musice	k1gInPc2	Musice
Club	club	k1gInSc1	club
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgInPc1d2	veliký
koncerty	koncert	k1gInPc1	koncert
se	se	k3xPyFc4	se
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
na	na	k7c6	na
zimním	zimní	k2eAgInSc6d1	zimní
stadionu	stadion	k1gInSc6	stadion
<g/>
,	,	kIx,	,
v	v	k7c6	v
Domě	dům	k1gInSc6	dům
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
Dělnickém	dělnický	k2eAgInSc6d1	dělnický
domě	dům	k1gInSc6	dům
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
Pěvecké	pěvecký	k2eAgNnSc1d1	pěvecké
sdružení	sdružení	k1gNnSc1	sdružení
Campanula	Campanula	k1gFnSc1	Campanula
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Domě	dům	k1gInSc6	dům
kultury	kultura	k1gFnSc2	kultura
Jihlava	Jihlava	k1gFnSc1	Jihlava
hostují	hostovat	k5eAaImIp3nP	hostovat
svá	svůj	k3xOyFgNnPc4	svůj
představení	představení	k1gNnPc4	představení
různá	různý	k2eAgNnPc4d1	různé
česká	český	k2eAgNnPc4d1	české
divadla	divadlo	k1gNnPc4	divadlo
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
konají	konat	k5eAaImIp3nP	konat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
i	i	k9	i
další	další	k2eAgFnPc1d1	další
společenské	společenský	k2eAgFnPc1d1	společenská
události	událost	k1gFnPc1	událost
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
plesy	ples	k1gInPc4	ples
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
pořádání	pořádání	k1gNnSc4	pořádání
plesů	ples	k1gInPc2	ples
či	či	k8xC	či
kongresových	kongresový	k2eAgNnPc2d1	Kongresové
setkání	setkání	k1gNnPc2	setkání
slouží	sloužit	k5eAaImIp3nS	sloužit
též	též	k9	též
Dělnický	dělnický	k2eAgInSc1d1	dělnický
dům	dům	k1gInSc1	dům
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
.	.	kIx.	.
</s>
<s>
Muzeum	muzeum	k1gNnSc4	muzeum
Vysočiny	vysočina	k1gFnSc2	vysočina
Jihlava	Jihlava	k1gFnSc1	Jihlava
sídlí	sídlet	k5eAaImIp3nS	sídlet
na	na	k7c6	na
Masarykově	Masarykův	k2eAgNnSc6d1	Masarykovo
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Výstavy	výstava	k1gFnPc1	výstava
pořádají	pořádat	k5eAaImIp3nP	pořádat
Oblastní	oblastní	k2eAgFnPc1d1	oblastní
galerie	galerie	k1gFnPc1	galerie
Vysočiny	vysočina	k1gFnSc2	vysočina
<g/>
,	,	kIx,	,
Dům	dům	k1gInSc4	dům
Gustava	Gustav	k1gMnSc2	Gustav
Mahlera	Mahler	k1gMnSc2	Mahler
na	na	k7c4	na
ulici	ulice	k1gFnSc4	ulice
Znojemská	znojemský	k2eAgFnSc1d1	Znojemská
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnPc1	galerie
Horácké	horácký	k2eAgNnSc1d1	Horácké
divadlo	divadlo	k1gNnSc1	divadlo
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
Hotelu	hotel	k1gInSc3	hotel
Gustav	Gustav	k1gMnSc1	Gustav
Mahler	Mahler	k1gMnSc1	Mahler
a	a	k8xC	a
Galerie	galerie	k1gFnSc1	galerie
Jána	Ján	k1gMnSc2	Ján
Šmoka	šmok	k1gMnSc2	šmok
v	v	k7c6	v
ateliérech	ateliér	k1gInPc6	ateliér
soukromé	soukromý	k2eAgFnSc2d1	soukromá
Střední	střední	k2eAgFnSc2d1	střední
umělecké	umělecký	k2eAgFnSc2d1	umělecká
školy	škola	k1gFnSc2	škola
grafické	grafický	k2eAgFnSc2d1	grafická
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
knihovna	knihovna	k1gFnSc1	knihovna
Jihlava	Jihlava	k1gFnSc1	Jihlava
sídlí	sídlet	k5eAaImIp3nS	sídlet
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Hluboká	Hluboká	k1gFnSc1	Hluboká
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
Jihlava	Jihlava	k1gFnSc1	Jihlava
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
areál	areál	k1gInSc1	areál
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
řeky	řeka	k1gFnSc2	řeka
Jihlávky	Jihlávka	k1gFnSc2	Jihlávka
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
místní	místní	k2eAgInPc4d1	místní
tištěné	tištěný	k2eAgInPc4d1	tištěný
sdělovací	sdělovací	k2eAgInPc4d1	sdělovací
prostředky	prostředek	k1gInPc4	prostředek
patří	patřit	k5eAaImIp3nP	patřit
Jihlavské	jihlavský	k2eAgInPc1d1	jihlavský
listy	list	k1gInPc1	list
<g/>
,	,	kIx,	,
Jihlavský	jihlavský	k2eAgInSc1d1	jihlavský
deník	deník	k1gInSc1	deník
a	a	k8xC	a
Noviny	novina	k1gFnPc1	novina
jihlavské	jihlavský	k2eAgFnSc2d1	Jihlavská
radnice	radnice	k1gFnSc2	radnice
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgMnPc1d1	zdejší
posluchači	posluchač	k1gMnPc1	posluchač
mohou	moct	k5eAaImIp3nP	moct
naladit	naladit	k5eAaPmF	naladit
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
Vysočina	vysočina	k1gFnSc1	vysočina
<g/>
,	,	kIx,	,
Hitrádio	Hitrádio	k6eAd1	Hitrádio
Vysočina	vysočina	k1gFnSc1	vysočina
či	či	k8xC	či
Rádio	rádio	k1gNnSc1	rádio
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
tradiční	tradiční	k2eAgFnSc7d1	tradiční
kulturní	kulturní	k2eAgFnSc7d1	kulturní
událostí	událost	k1gFnSc7	událost
patří	patřit	k5eAaImIp3nS	patřit
Jihlavský	jihlavský	k2eAgInSc1d1	jihlavský
havířský	havířský	k2eAgInSc1d1	havířský
průvod	průvod	k1gInSc1	průvod
<g/>
,	,	kIx,	,
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
festival	festival	k1gInSc1	festival
dokumentárních	dokumentární	k2eAgInPc2d1	dokumentární
filmů	film	k1gInPc2	film
Ji	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
<g/>
hlava	hlava	k1gFnSc1	hlava
<g/>
,	,	kIx,	,
Festival	festival	k1gInSc1	festival
Mahler	Mahler	k1gMnSc1	Mahler
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
,	,	kIx,	,
Festival	festival	k1gInSc1	festival
sborového	sborový	k2eAgNnSc2d1	sborové
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
Horácký	horácký	k2eAgInSc1d1	horácký
letecký	letecký	k2eAgInSc1d1	letecký
den	den	k1gInSc1	den
na	na	k7c6	na
henčovském	henčovský	k2eAgNnSc6d1	henčovský
letišti	letiště	k1gNnSc6	letiště
a	a	k8xC	a
jihlavská	jihlavský	k2eAgFnSc1d1	Jihlavská
pouť	pouť	k1gFnSc1	pouť
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Hornická	hornický	k2eAgFnSc1d1	hornická
naučná	naučný	k2eAgFnSc1d1	naučná
stezka	stezka	k1gFnSc1	stezka
(	(	kIx(	(
<g/>
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Naučná	naučný	k2eAgFnSc1d1	naučná
stezka	stezka	k1gFnSc1	stezka
Hudební	hudební	k2eAgFnSc2d1	hudební
osobnosti	osobnost	k1gFnSc2	osobnost
Jihlavy	Jihlava	k1gFnSc2	Jihlava
a	a	k8xC	a
Pivovarská	pivovarský	k2eAgFnSc1d1	Pivovarská
naučná	naučný	k2eAgFnSc1d1	naučná
stezka	stezka	k1gFnSc1	stezka
<g/>
.	.	kIx.	.
</s>
<s>
Hokejový	hokejový	k2eAgInSc1d1	hokejový
klub	klub	k1gInSc1	klub
HC	HC	kA	HC
Dukla	Dukla	k1gFnSc1	Dukla
Jihlava	Jihlava	k1gFnSc1	Jihlava
je	být	k5eAaImIp3nS	být
historicky	historicky	k6eAd1	historicky
nejúspěšnějším	úspěšný	k2eAgInSc7d3	nejúspěšnější
hokejovým	hokejový	k2eAgInSc7d1	hokejový
klubem	klub	k1gInSc7	klub
české	český	k2eAgFnSc2d1	Česká
historie	historie	k1gFnSc2	historie
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
české	český	k2eAgFnSc6d1	Česká
soutěži	soutěž	k1gFnSc6	soutěž
-	-	kIx~	-
extralize	extraliga	k1gFnSc6	extraliga
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
v	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
působil	působit	k5eAaImAgInS	působit
také	také	k9	také
hokejový	hokejový	k2eAgInSc1d1	hokejový
oddíl	oddíl	k1gInSc1	oddíl
pod	pod	k7c7	pod
hlavičkou	hlavička	k1gFnSc7	hlavička
SK	Sk	kA	Sk
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
fotbalové	fotbalový	k2eAgFnSc6d1	fotbalová
lize	liga	k1gFnSc6	liga
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
tým	tým	k1gInSc1	tým
FC	FC	kA	FC
Vysočina	vysočina	k1gFnSc1	vysočina
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
své	svůj	k3xOyFgMnPc4	svůj
soupeře	soupeř	k1gMnSc4	soupeř
hostí	hostit	k5eAaImIp3nS	hostit
na	na	k7c6	na
fotbalovém	fotbalový	k2eAgInSc6d1	fotbalový
stadiónu	stadión	k1gInSc6	stadión
v	v	k7c6	v
Jiráskově	Jiráskův	k2eAgFnSc6d1	Jiráskova
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
SK	Sk	kA	Sk
Toptrans	Toptrans	k1gInSc1	Toptrans
Jihlava	Jihlava	k1gFnSc1	Jihlava
hraje	hrát	k5eAaImIp3nS	hrát
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
hokejbalovou	hokejbalový	k2eAgFnSc4d1	hokejbalová
soutěž	soutěž	k1gFnSc4	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Florbalová	Florbalový	k2eAgFnSc1d1	Florbalová
škola	škola	k1gFnSc1	škola
Jihlava	Jihlava	k1gFnSc1	Jihlava
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc4	několik
týmů	tým	k1gInPc2	tým
–	–	k?	–
ženský	ženský	k2eAgInSc1d1	ženský
hraje	hrát	k5eAaImIp3nS	hrát
extraligu	extraliga	k1gFnSc4	extraliga
a	a	k8xC	a
mužský	mužský	k2eAgInSc4d1	mužský
2	[number]	k4	2
<g/>
.	.	kIx.	.
ligu	liga	k1gFnSc4	liga
(	(	kIx(	(
<g/>
divize	divize	k1gFnSc2	divize
III	III	kA	III
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
tu	tu	k6eAd1	tu
působí	působit	k5eAaImIp3nS	působit
Baseballový	baseballový	k2eAgInSc1d1	baseballový
klub	klub	k1gInSc1	klub
Ježci	Ježek	k1gMnPc1	Ježek
Jihlava	Jihlava	k1gFnSc1	Jihlava
a	a	k8xC	a
Basketbalový	basketbalový	k2eAgInSc1d1	basketbalový
klub	klub	k1gInSc1	klub
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
.	.	kIx.	.
</s>
<s>
Sportovní	sportovní	k2eAgInSc1d1	sportovní
klub	klub	k1gInSc1	klub
Jihlava	Jihlava	k1gFnSc1	Jihlava
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaImIp3nS	věnovat
následujícím	následující	k2eAgInPc3d1	následující
sportům	sport	k1gInPc3	sport
<g/>
:	:	kIx,	:
box	box	k1gInSc1	box
<g/>
,	,	kIx,	,
hokejbal	hokejbal	k1gInSc1	hokejbal
<g/>
,	,	kIx,	,
judo	judo	k1gNnSc1	judo
<g/>
,	,	kIx,	,
kanoistika	kanoistika	k1gFnSc1	kanoistika
<g/>
,	,	kIx,	,
karate	karate	k1gNnSc1	karate
<g/>
,	,	kIx,	,
lyžování	lyžování	k1gNnSc1	lyžování
<g/>
,	,	kIx,	,
malá	malý	k2eAgFnSc1d1	malá
kopaná	kopaná	k1gFnSc1	kopaná
<g/>
,	,	kIx,	,
moderní	moderní	k2eAgFnSc1d1	moderní
gymnastika	gymnastika	k1gFnSc1	gymnastika
<g/>
,	,	kIx,	,
národní	národní	k2eAgFnSc1d1	národní
házená	házená	k1gFnSc1	házená
<g/>
,	,	kIx,	,
paintball	paintball	k1gInSc1	paintball
<g/>
,	,	kIx,	,
sportovní	sportovní	k2eAgFnSc1d1	sportovní
tance	tanec	k1gInPc4	tanec
<g/>
,	,	kIx,	,
stolní	stolní	k2eAgInSc4d1	stolní
tenis	tenis	k1gInSc4	tenis
<g/>
,	,	kIx,	,
volejbal	volejbal	k1gInSc4	volejbal
a	a	k8xC	a
zápas	zápas	k1gInSc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Plavání	plavání	k1gNnSc1	plavání
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaImIp3nS	věnovat
Jihlavský	jihlavský	k2eAgInSc1d1	jihlavský
plavecký	plavecký	k2eAgInSc1d1	plavecký
klub	klub	k1gInSc1	klub
AXIS	AXIS	kA	AXIS
<g/>
,	,	kIx,	,
atletice	atletika	k1gFnSc6	atletika
sdružení	sdružení	k1gNnPc2	sdružení
Atletika	atletika	k1gFnSc1	atletika
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
tu	tu	k6eAd1	tu
působí	působit	k5eAaImIp3nS	působit
Tenisový	tenisový	k2eAgInSc1d1	tenisový
klub	klub	k1gInSc1	klub
ČLTK	ČLTK	kA	ČLTK
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
,	,	kIx,	,
RnR	RnR	k1gFnSc1	RnR
club	club	k1gInSc1	club
Elvis	Elvis	k1gMnSc1	Elvis
Jihlava	Jihlava	k1gFnSc1	Jihlava
TJ	tj	kA	tj
Sokol	Sokol	k1gInSc1	Sokol
Bedřichov	Bedřichov	k1gInSc1	Bedřichov
<g/>
,	,	kIx,	,
Horácký	horácký	k2eAgInSc1d1	horácký
soubor	soubor	k1gInSc1	soubor
písní	píseň	k1gFnPc2	píseň
a	a	k8xC	a
tanců	tanec	k1gInPc2	tanec
Vysočan	Vysočany	k1gInPc2	Vysočany
<g/>
,	,	kIx,	,
Kuželkářský	kuželkářský	k2eAgInSc1d1	kuželkářský
klub	klub	k1gInSc1	klub
PSJ	PSJ	kA	PSJ
Jihlava	Jihlava	k1gFnSc1	Jihlava
a	a	k8xC	a
Aeroklub	aeroklub	k1gInSc1	aeroklub
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaPmIp3nS	věnovat
bezmotorovému	bezmotorový	k2eAgNnSc3d1	bezmotorové
a	a	k8xC	a
motorovému	motorový	k2eAgNnSc3d1	motorové
létání	létání	k1gNnSc3	létání
<g/>
,	,	kIx,	,
parašutismu	parašutismus	k1gInSc2	parašutismus
a	a	k8xC	a
modelářství	modelářství	k1gNnSc2	modelářství
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
hendikepované	hendikepovaný	k2eAgNnSc4d1	hendikepované
tu	ten	k3xDgFnSc4	ten
jsou	být	k5eAaImIp3nP	být
TJ	tj	kA	tj
Neslyšící	slyšící	k2eNgFnSc1d1	neslyšící
Jihlava	Jihlava	k1gFnSc1	Jihlava
a	a	k8xC	a
SK	Sk	kA	Sk
Vodomílek	Vodomílek	k1gInSc1	Vodomílek
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
zde	zde	k6eAd1	zde
sídlí	sídlet	k5eAaImIp3nS	sídlet
jednota	jednota	k1gFnSc1	jednota
Orla	Orel	k1gMnSc2	Orel
<g/>
,	,	kIx,	,
Sokola	Sokol	k1gMnSc2	Sokol
a	a	k8xC	a
několik	několik	k4yIc1	několik
oddílů	oddíl	k1gInPc2	oddíl
Klubu	klub	k1gInSc2	klub
českých	český	k2eAgMnPc2d1	český
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
rekreační	rekreační	k2eAgInPc4d1	rekreační
sporty	sport	k1gInPc4	sport
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
řada	řada	k1gFnSc1	řada
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
koupání	koupání	k1gNnSc3	koupání
slouží	sloužit	k5eAaImIp3nS	sloužit
Aquapark	aquapark	k1gInSc1	aquapark
Vodní	vodní	k2eAgInSc1d1	vodní
ráj	ráj	k1gInSc1	ráj
a	a	k8xC	a
Krytý	krytý	k2eAgInSc1d1	krytý
plavecký	plavecký	k2eAgInSc1d1	plavecký
bazén	bazén	k1gInSc1	bazén
a	a	k8xC	a
sauna	sauna	k1gFnSc1	sauna
E.	E.	kA	E.
Rošického	Rošický	k2eAgInSc2d1	Rošický
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zimním	zimní	k2eAgFnPc3d1	zimní
sportovním	sportovní	k2eAgFnPc3d1	sportovní
radovánkám	radovánka	k1gFnPc3	radovánka
jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgFnP	určit
sjezdovky	sjezdovka	k1gFnPc1	sjezdovka
Čeřínek	čeřínek	k1gInSc4	čeřínek
a	a	k8xC	a
Šacberk	Šacberk	k1gInSc4	Šacberk
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
(	(	kIx(	(
<g/>
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Malý	malý	k2eAgInSc1d1	malý
jednolodní	jednolodní	k2eAgInSc1d1	jednolodní
kostelík	kostelík	k1gInSc1	kostelík
na	na	k7c6	na
Jánském	jánský	k2eAgInSc6d1	jánský
vršku	vršek	k1gInSc6	vršek
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
zasvěcen	zasvěcen	k2eAgInSc1d1	zasvěcen
sv.	sv.	kA	sv.
Janu	Jan	k1gMnSc3	Jan
Křtiteli	křtitel	k1gMnSc3	křtitel
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgInSc1d3	nejstarší
dochovanou	dochovaný	k2eAgFnSc7d1	dochovaná
kamennou	kamenný	k2eAgFnSc7d1	kamenná
stavbou	stavba	k1gFnSc7	stavba
na	na	k7c6	na
Českomoravské	českomoravský	k2eAgFnSc6d1	Českomoravská
vrchovině	vrchovina	k1gFnSc6	vrchovina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
stavba	stavba	k1gFnSc1	stavba
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1200	[number]	k4	1200
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
dnešním	dnešní	k2eAgNnSc6d1	dnešní
pozdně	pozdně	k6eAd1	pozdně
gotickým	gotický	k2eAgNnSc7d1	gotické
kněžištěm	kněžiště	k1gNnSc7	kněžiště
byly	být	k5eAaImAgFnP	být
nalezeny	nalézt	k5eAaBmNgInP	nalézt
základy	základ	k1gInPc1	základ
původního	původní	k2eAgInSc2d1	původní
románského	románský	k2eAgInSc2d1	románský
presbytáře	presbytář	k1gInSc2	presbytář
<g/>
.	.	kIx.	.
</s>
<s>
Románské	románský	k2eAgFnPc1d1	románská
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
i	i	k8xC	i
obvodové	obvodový	k2eAgFnSc2d1	obvodová
zdi	zeď	k1gFnSc2	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jakuba	Jakub	k1gMnSc2	Jakub
Staršího	starší	k1gMnSc2	starší
(	(	kIx(	(
<g/>
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
kostel	kostel	k1gInSc1	kostel
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
patrně	patrně	k6eAd1	patrně
roku	rok	k1gInSc2	rok
1243	[number]	k4	1243
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mohutná	mohutný	k2eAgFnSc1d1	mohutná
gotická	gotický	k2eAgFnSc1d1	gotická
trojlodní	trojlodní	k2eAgFnSc1d1	trojlodní
stavba	stavba	k1gFnSc1	stavba
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
věžemi	věž	k1gFnPc7	věž
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
požáru	požár	k1gInSc6	požár
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1257	[number]	k4	1257
znovu	znovu	k6eAd1	znovu
vysvěcen	vysvětit	k5eAaPmNgInS	vysvětit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
stavbě	stavba	k1gFnSc6	stavba
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
pracovalo	pracovat	k5eAaImAgNnS	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
hudební	hudební	k2eAgFnSc1d1	hudební
kruchta	kruchta	k1gFnSc1	kruchta
<g/>
,	,	kIx,	,
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
jižní	jižní	k2eAgFnSc1d1	jižní
–	–	k?	–
zvonová	zvonový	k2eAgFnSc1d1	zvonová
věž	věž	k1gFnSc1	věž
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
kvůli	kvůli	k7c3	kvůli
statickým	statický	k2eAgFnPc3d1	statická
poruchám	porucha	k1gFnPc3	porucha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1548	[number]	k4	1548
snížena	snížit	k5eAaPmNgFnS	snížit
na	na	k7c4	na
současnou	současný	k2eAgFnSc4d1	současná
výšku	výška	k1gFnSc4	výška
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1500	[number]	k4	1500
snad	snad	k9	snad
přistavěny	přistavět	k5eAaPmNgFnP	přistavět
severní	severní	k2eAgFnPc1d1	severní
kaple	kaple	k1gFnPc1	kaple
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1702	[number]	k4	1702
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
severní	severní	k2eAgFnSc3d1	severní
stěně	stěna	k1gFnSc3	stěna
přistavěna	přistavěn	k2eAgFnSc1d1	přistavěna
barokní	barokní	k2eAgFnSc1d1	barokní
osmiboká	osmiboký	k2eAgFnSc1d1	osmiboká
kaple	kaple	k1gFnSc1	kaple
P.	P.	kA	P.
Marie	Maria	k1gFnSc2	Maria
bolestné	bolestný	k2eAgNnSc4d1	bolestné
a	a	k8xC	a
upraveno	upraven	k2eAgNnSc4d1	upraveno
západní	západní	k2eAgNnSc4d1	západní
průčelí	průčelí	k1gNnSc4	průčelí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1898	[number]	k4	1898
<g/>
–	–	k?	–
<g/>
1906	[number]	k4	1906
byl	být	k5eAaImAgInS	být
kostel	kostel	k1gInSc1	kostel
novogoticky	novogoticky	k6eAd1	novogoticky
upraven	upravit	k5eAaPmNgInS	upravit
a	a	k8xC	a
zvenčí	zvenčí	k6eAd1	zvenčí
dostal	dostat	k5eAaPmAgMnS	dostat
místo	místo	k1gNnSc4	místo
omítky	omítka	k1gFnSc2	omítka
vyspárované	vyspárovaný	k2eAgNnSc4d1	vyspárované
zdivo	zdivo	k1gNnSc4	zdivo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
zařízení	zařízení	k1gNnSc2	zařízení
je	být	k5eAaImIp3nS	být
zvláště	zvláště	k6eAd1	zvláště
cenná	cenný	k2eAgFnSc1d1	cenná
pozlacená	pozlacený	k2eAgFnSc1d1	pozlacená
křtitelnice	křtitelnice	k1gFnSc1	křtitelnice
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1599	[number]	k4	1599
<g/>
,	,	kIx,	,
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
socha	socha	k1gFnSc1	socha
Madony	Madona	k1gFnSc2	Madona
s	s	k7c7	s
dítětem	dítě	k1gNnSc7	dítě
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
1370	[number]	k4	1370
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kamenná	kamenný	k2eAgFnSc1d1	kamenná
socha	socha	k1gFnSc1	socha
sv.	sv.	kA	sv.
Kateřiny	Kateřina	k1gFnSc2	Kateřina
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
1400	[number]	k4	1400
<g/>
)	)	kIx)	)
a	a	k8xC	a
varhany	varhany	k1gInPc1	varhany
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1740	[number]	k4	1740
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
věži	věž	k1gFnSc6	věž
je	být	k5eAaImIp3nS	být
veliký	veliký	k2eAgInSc4d1	veliký
zvon	zvon	k1gInSc4	zvon
(	(	kIx(	(
<g/>
asi	asi	k9	asi
7	[number]	k4	7
400	[number]	k4	400
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ulitý	ulitý	k2eAgMnSc1d1	ulitý
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
pražským	pražský	k2eAgFnPc3d1	Pražská
zvonařem	zvonař	k1gMnSc7	zvonař
Brikcím	Brikce	k1gFnPc3	Brikce
z	z	k7c2	z
Cimperka	Cimperka	k1gFnSc1	Cimperka
roku	rok	k1gInSc2	rok
1564	[number]	k4	1564
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kostel	kostel	k1gInSc1	kostel
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
(	(	kIx(	(
<g/>
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Minoritský	minoritský	k2eAgInSc1d1	minoritský
klášter	klášter	k1gInSc1	klášter
s	s	k7c7	s
kostelem	kostel	k1gInSc7	kostel
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Matky	matka	k1gFnSc2	matka
Boží	boží	k2eAgFnSc2d1	boží
blízko	blízko	k7c2	blízko
brány	brána	k1gFnSc2	brána
byl	být	k5eAaImAgInS	být
patrně	patrně	k6eAd1	patrně
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
ještě	ještě	k9	ještě
před	před	k7c4	před
1240	[number]	k4	1240
<g/>
.	.	kIx.	.
</s>
<s>
Gotické	gotický	k2eAgNnSc1d1	gotické
trojlodí	trojlodí	k1gNnSc1	trojlodí
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
presbytářem	presbytář	k1gInSc7	presbytář
a	a	k8xC	a
křížovými	křížový	k2eAgFnPc7d1	křížová
klenbami	klenba	k1gFnPc7	klenba
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgNnSc3	jenž
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1508	[number]	k4	1508
ještě	ještě	k6eAd1	ještě
přistavěn	přistavěn	k2eAgInSc4d1	přistavěn
polygonální	polygonální	k2eAgInSc4d1	polygonální
závěr	závěr	k1gInSc4	závěr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stěnách	stěna	k1gFnPc6	stěna
se	se	k3xPyFc4	se
zachovaly	zachovat	k5eAaPmAgInP	zachovat
zbytky	zbytek	k1gInPc1	zbytek
fresek	freska	k1gFnPc2	freska
z	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
až	až	k9	až
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
je	být	k5eAaImIp3nS	být
řada	řada	k1gFnSc1	řada
gotických	gotický	k2eAgFnPc2d1	gotická
kamenných	kamenný	k2eAgFnPc2d1	kamenná
soch	socha	k1gFnPc2	socha
a	a	k8xC	a
barokní	barokní	k2eAgNnSc4d1	barokní
dřevěné	dřevěný	k2eAgNnSc4d1	dřevěné
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
přilehlé	přilehlý	k2eAgFnSc2d1	přilehlá
budovy	budova	k1gFnSc2	budova
kláštera	klášter	k1gInSc2	klášter
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
tři	tři	k4xCgNnPc4	tři
křídla	křídlo	k1gNnPc4	křídlo
<g/>
,	,	kIx,	,
v	v	k7c6	v
barokní	barokní	k2eAgFnSc6d1	barokní
době	doba	k1gFnSc6	doba
přestavěná	přestavěný	k2eAgFnSc1d1	přestavěná
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Jihlavská	jihlavský	k2eAgFnSc1d1	Jihlavská
radnice	radnice	k1gFnSc1	radnice
<g/>
.	.	kIx.	.
</s>
<s>
Radnice	radnice	k1gFnSc1	radnice
na	na	k7c6	na
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Masarykově	Masarykův	k2eAgNnSc6d1	Masarykovo
náměstí	náměstí	k1gNnSc6	náměstí
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
spojením	spojení	k1gNnSc7	spojení
několika	několik	k4yIc2	několik
měšťanských	měšťanský	k2eAgInPc2d1	měšťanský
domů	dům	k1gInPc2	dům
s	s	k7c7	s
podloubím	podloubí	k1gNnSc7	podloubí
z	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
zachovaly	zachovat	k5eAaPmAgInP	zachovat
gotické	gotický	k2eAgInPc1d1	gotický
oblouky	oblouk	k1gInPc1	oblouk
do	do	k7c2	do
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
jižním	jižní	k2eAgInSc7d1	jižní
směrem	směr	k1gInSc7	směr
a	a	k8xC	a
velký	velký	k2eAgInSc1d1	velký
sál	sál	k1gInSc1	sál
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
poschodí	poschodí	k1gNnSc6	poschodí
byl	být	k5eAaImAgMnS	být
roku	rok	k1gInSc2	rok
1561	[number]	k4	1561
zaklenut	zaklenout	k5eAaPmNgMnS	zaklenout
na	na	k7c4	na
střední	střední	k2eAgInSc4d1	střední
sloup	sloup	k1gInSc4	sloup
křížovou	křížový	k2eAgFnSc7d1	křížová
klenbou	klenba	k1gFnSc7	klenba
s	s	k7c7	s
cihlovými	cihlový	k2eAgInPc7d1	cihlový
žebry	žebr	k1gInPc7	žebr
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1734	[number]	k4	1734
byla	být	k5eAaImAgFnS	být
dále	daleko	k6eAd2	daleko
rozšířena	rozšířen	k2eAgFnSc1d1	rozšířena
<g/>
,	,	kIx,	,
1786	[number]	k4	1786
zvýšena	zvýšit	k5eAaPmNgFnS	zvýšit
o	o	k7c4	o
další	další	k2eAgNnSc4d1	další
patro	patro	k1gNnSc4	patro
a	a	k8xC	a
opatřena	opatřit	k5eAaPmNgFnS	opatřit
jednotnou	jednotný	k2eAgFnSc7d1	jednotná
fasádou	fasáda	k1gFnSc7	fasáda
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
ní	on	k3xPp3gFnSc2	on
je	být	k5eAaImIp3nS	být
však	však	k9	však
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
zachována	zachován	k2eAgFnSc1d1	zachována
středověká	středověký	k2eAgFnSc1d1	středověká
dispozice	dispozice	k1gFnSc1	dispozice
a	a	k8xC	a
různé	různý	k2eAgInPc1d1	různý
kamenné	kamenný	k2eAgInPc1d1	kamenný
detaily	detail	k1gInPc1	detail
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
neobvyklé	obvyklý	k2eNgNnSc1d1	neobvyklé
množství	množství	k1gNnSc1	množství
dobře	dobře	k6eAd1	dobře
zachovaných	zachovaný	k2eAgInPc2d1	zachovaný
a	a	k8xC	a
cenných	cenný	k2eAgInPc2d1	cenný
měšťanských	měšťanský	k2eAgInPc2d1	měšťanský
domů	dům	k1gInPc2	dům
z	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
s	s	k7c7	s
novější	nový	k2eAgFnSc7d2	novější
fasádou	fasáda	k1gFnSc7	fasáda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dochovaným	dochovaný	k2eAgInSc7d1	dochovaný
historickým	historický	k2eAgInSc7d1	historický
vnitřkem	vnitřek	k1gInSc7	vnitřek
–	–	k?	–
zejména	zejména	k9	zejména
se	s	k7c7	s
charakteristickou	charakteristický	k2eAgFnSc7d1	charakteristická
schodišťovou	schodišťový	k2eAgFnSc7d1	schodišťová
síní	síň	k1gFnSc7	síň
nebo	nebo	k8xC	nebo
mázhauzem	mázhauz	k1gInSc7	mázhauz
v	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
patří	patřit	k5eAaImIp3nS	patřit
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgInPc4	všechen
domy	dům	k1gInPc4	dům
na	na	k7c4	na
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc4d1	další
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
ulicích	ulice	k1gFnPc6	ulice
Brněnská	brněnský	k2eAgFnSc1d1	brněnská
<g/>
,	,	kIx,	,
Havířská	havířský	k2eAgFnSc1d1	Havířská
<g/>
,	,	kIx,	,
Komenského	Komenského	k2eAgFnSc1d1	Komenského
<g/>
,	,	kIx,	,
Matky	matka	k1gFnPc1	matka
Boží	boží	k2eAgFnSc2d1	boží
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
stával	stávat	k5eAaImAgInS	stávat
uprostřed	uprostřed	k7c2	uprostřed
náměstí	náměstí	k1gNnSc2	náměstí
soubor	soubor	k1gInSc1	soubor
historických	historický	k2eAgInPc2d1	historický
středověkých	středověký	k2eAgInPc2d1	středověký
domů	dům	k1gInPc2	dům
Krecl	Krecl	k1gInSc1	Krecl
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
zničen	zničit	k5eAaPmNgInS	zničit
při	při	k7c6	při
výstavbě	výstavba	k1gFnSc6	výstavba
obchodního	obchodní	k2eAgInSc2d1	obchodní
domu	dům	k1gInSc2	dům
Prior	prior	k1gMnSc1	prior
<g/>
.	.	kIx.	.
</s>
<s>
Barokní	barokní	k2eAgInSc1d1	barokní
mariánský	mariánský	k2eAgInSc1d1	mariánský
sloup	sloup	k1gInSc1	sloup
byl	být	k5eAaImAgInS	být
umístěn	umístit	k5eAaPmNgInS	umístit
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
letech	let	k1gInPc6	let
1686	[number]	k4	1686
<g/>
–	–	k?	–
<g/>
1691	[number]	k4	1691
jako	jako	k8xC	jako
výraz	výraz	k1gInSc1	výraz
vděku	vděk	k1gInSc2	vděk
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
městu	město	k1gNnSc3	město
vyhnula	vyhnout	k5eAaPmAgFnS	vyhnout
krutá	krutý	k2eAgFnSc1d1	krutá
morová	morový	k2eAgFnSc1d1	morová
epidemie	epidemie	k1gFnSc1	epidemie
(	(	kIx(	(
<g/>
1679	[number]	k4	1679
<g/>
,	,	kIx,	,
1680	[number]	k4	1680
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
pískovcová	pískovcový	k2eAgFnSc1d1	pískovcová
socha	socha	k1gFnSc1	socha
Neposkvrněné	poskvrněný	k2eNgFnSc2d1	neposkvrněná
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
síni	síň	k1gFnSc6	síň
radnice	radnice	k1gFnSc2	radnice
<g/>
,	,	kIx,	,
na	na	k7c6	na
sloupu	sloup	k1gInSc6	sloup
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gFnSc1	její
kopie	kopie	k1gFnSc1	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Obklopují	obklopovat	k5eAaImIp3nP	obklopovat
ji	on	k3xPp3gFnSc4	on
sochy	socha	k1gFnSc2	socha
sv.	sv.	kA	sv.
Františka	František	k1gMnSc2	František
Xaverského	xaverský	k2eAgMnSc2d1	xaverský
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Josefa	Josef	k1gMnSc2	Josef
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Šebestiána	Šebestián	k1gMnSc4	Šebestián
a	a	k8xC	a
sv.	sv.	kA	sv.
Jakuba	Jakub	k1gMnSc2	Jakub
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
architektonického	architektonický	k2eAgNnSc2d1	architektonické
řešení	řešení	k1gNnSc2	řešení
je	být	k5eAaImIp3nS	být
italský	italský	k2eAgMnSc1d1	italský
stavitel	stavitel	k1gMnSc1	stavitel
jihlavského	jihlavský	k2eAgInSc2d1	jihlavský
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Ignáce	Ignác	k1gMnSc2	Ignác
z	z	k7c2	z
Loyoly	Loyola	k1gFnSc2	Loyola
Jacopo	Jacopa	k1gFnSc5	Jacopa
(	(	kIx(	(
<g/>
Giacomo	Giacoma	k1gFnSc5	Giacoma
<g/>
)	)	kIx)	)
Brascha	Brasch	k1gMnSc4	Brasch
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
(	(	kIx(	(
<g/>
patrně	patrně	k6eAd1	patrně
kameník	kameník	k1gMnSc1	kameník
<g/>
)	)	kIx)	)
Agostino	Agostin	k2eAgNnSc1d1	Agostino
<g/>
,	,	kIx,	,
sochy	socha	k1gFnPc1	socha
jsou	být	k5eAaImIp3nP	být
dílem	díl	k1gInSc7	díl
italského	italský	k2eAgMnSc2d1	italský
barokního	barokní	k2eAgMnSc2d1	barokní
sochaře	sochař	k1gMnSc2	sochař
Antonia	Antonio	k1gMnSc2	Antonio
Laghi	Lagh	k1gFnSc2	Lagh
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
stálo	stát	k5eAaImAgNnS	stát
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
místě	místo	k1gNnSc6	místo
popraviště	popraviště	k1gNnSc2	popraviště
a	a	k8xC	a
pranýř	pranýř	k1gInSc1	pranýř
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Ignáce	Ignác	k1gMnSc2	Ignác
z	z	k7c2	z
Loyoly	Loyola	k1gFnSc2	Loyola
(	(	kIx(	(
<g/>
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jezuitská	jezuitský	k2eAgFnSc1d1	jezuitská
kolej	kolej	k1gFnSc1	kolej
s	s	k7c7	s
kostelem	kostel	k1gInSc7	kostel
sv.	sv.	kA	sv.
Ignáce	Ignác	k1gMnSc2	Ignác
na	na	k7c6	na
Masarykově	Masarykův	k2eAgNnSc6d1	Masarykovo
náměstí	náměstí	k1gNnSc6	náměstí
vedle	vedle	k7c2	vedle
radnice	radnice	k1gFnSc2	radnice
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1625	[number]	k4	1625
a	a	k8xC	a
vybudována	vybudován	k2eAgFnSc1d1	vybudována
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
italského	italský	k2eAgMnSc2d1	italský
architekta	architekt	k1gMnSc2	architekt
Giacomo	Giacoma	k1gFnSc5	Giacoma
Braschy	Brascha	k1gFnPc4	Brascha
v	v	k7c6	v
letech	let	k1gInPc6	let
1680	[number]	k4	1680
<g/>
–	–	k?	–
<g/>
1727	[number]	k4	1727
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
je	být	k5eAaImIp3nS	být
podlouhlá	podlouhlý	k2eAgFnSc1d1	podlouhlá
stavba	stavba	k1gFnSc1	stavba
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
věžemi	věž	k1gFnPc7	věž
<g/>
,	,	kIx,	,
uvnitř	uvnitř	k6eAd1	uvnitř
jednolodní	jednolodní	k2eAgFnSc1d1	jednolodní
s	s	k7c7	s
plochými	plochý	k2eAgFnPc7d1	plochá
bočními	boční	k2eAgFnPc7d1	boční
kaplemi	kaple	k1gFnPc7	kaple
a	a	k8xC	a
oratořemi	oratoř	k1gFnPc7	oratoř
po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
v	v	k7c6	v
typickém	typický	k2eAgInSc6d1	typický
jezuitském	jezuitský	k2eAgInSc6d1	jezuitský
slohu	sloh	k1gInSc6	sloh
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
oltáři	oltář	k1gInSc6	oltář
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
kapli	kaple	k1gFnSc6	kaple
byl	být	k5eAaImAgInS	být
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
Přemyslovský	přemyslovský	k2eAgInSc1d1	přemyslovský
krucifix	krucifix	k1gInSc1	krucifix
z	z	k7c2	z
let	léto	k1gNnPc2	léto
kolem	kolem	k7c2	kolem
1330	[number]	k4	1330
<g/>
,	,	kIx,	,
v	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
P.	P.	kA	P.
Marie	Maria	k1gFnSc2	Maria
je	být	k5eAaImIp3nS	být
gotická	gotický	k2eAgFnSc1d1	gotická
Pieta	pieta	k1gFnSc1	pieta
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
1400	[number]	k4	1400
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
kruchtě	kruchta	k1gFnSc6	kruchta
jsou	být	k5eAaImIp3nP	být
cenné	cenný	k2eAgInPc1d1	cenný
varhany	varhany	k1gInPc1	varhany
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1732	[number]	k4	1732
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kostel	kostel	k1gInSc1	kostel
Povýšení	povýšení	k1gNnSc2	povýšení
svatého	svatý	k2eAgInSc2d1	svatý
Kříže	kříž	k1gInSc2	kříž
(	(	kIx(	(
<g/>
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
dominikánský	dominikánský	k2eAgInSc1d1	dominikánský
kostel	kostel	k1gInSc1	kostel
s	s	k7c7	s
konventem	konvent	k1gInSc7	konvent
v	v	k7c6	v
Křížové	Křížové	k2eAgFnSc6d1	Křížové
ulici	ulice	k1gFnSc6	ulice
(	(	kIx(	(
<g/>
blízko	blízko	k7c2	blízko
Masarykova	Masarykův	k2eAgNnSc2d1	Masarykovo
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
založen	založen	k2eAgInSc1d1	založen
roku	rok	k1gInSc2	rok
1247	[number]	k4	1247
a	a	k8xC	a
dokončen	dokončit	k5eAaPmNgInS	dokončit
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1385	[number]	k4	1385
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1781	[number]	k4	1781
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
zrušeném	zrušený	k2eAgInSc6d1	zrušený
klášteře	klášter	k1gInSc6	klášter
vojenská	vojenský	k2eAgFnSc1d1	vojenská
správa	správa	k1gFnSc1	správa
<g/>
,	,	kIx,	,
nepoužívaný	používaný	k2eNgInSc1d1	nepoužívaný
kostel	kostel	k1gInSc1	kostel
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1947	[number]	k4	1947
<g/>
–	–	k?	–
<g/>
1948	[number]	k4	1948
opraven	opraven	k2eAgMnSc1d1	opraven
a	a	k8xC	a
předán	předán	k2eAgMnSc1d1	předán
CČH	CČH	kA	CČH
(	(	kIx(	(
<g/>
ta	ten	k3xDgFnSc1	ten
jej	on	k3xPp3gMnSc4	on
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
99	[number]	k4	99
let	léto	k1gNnPc2	léto
v	v	k7c6	v
pronájmu	pronájem	k1gInSc6	pronájem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
v	v	k7c6	v
dražbě	dražba	k1gFnSc6	dražba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
koupilo	koupit	k5eAaPmAgNnS	koupit
město	město	k1gNnSc1	město
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
.	.	kIx.	.
</s>
<s>
Klášter	klášter	k1gInSc1	klášter
byl	být	k5eAaImAgInS	být
několikrát	několikrát	k6eAd1	několikrát
přestavěn	přestavěn	k2eAgInSc1d1	přestavěn
a	a	k8xC	a
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
už	už	k6eAd1	už
jen	jen	k9	jen
původní	původní	k2eAgInSc4d1	původní
půdorys	půdorys	k1gInSc4	půdorys
s	s	k7c7	s
ambity	ambit	k1gInPc7	ambit
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
je	být	k5eAaImIp3nS	být
gotické	gotický	k2eAgNnSc4d1	gotické
halové	halový	k2eAgNnSc4d1	halové
trojlodí	trojlodí	k1gNnSc4	trojlodí
s	s	k7c7	s
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
presbytářem	presbytář	k1gInSc7	presbytář
s	s	k7c7	s
barokní	barokní	k2eAgFnSc7d1	barokní
klenbou	klenba	k1gFnSc7	klenba
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lodi	loď	k1gFnSc6	loď
jsou	být	k5eAaImIp3nP	být
dobře	dobře	k6eAd1	dobře
zachované	zachovaný	k2eAgFnPc1d1	zachovaná
gotické	gotický	k2eAgFnPc1d1	gotická
klenby	klenba	k1gFnPc1	klenba
a	a	k8xC	a
v	v	k7c6	v
západním	západní	k2eAgNnSc6d1	západní
průčelí	průčelí	k1gNnSc6	průčelí
je	být	k5eAaImIp3nS	být
bohatý	bohatý	k2eAgInSc1d1	bohatý
gotický	gotický	k2eAgInSc1d1	gotický
portál	portál	k1gInSc1	portál
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
kostele	kostel	k1gInSc6	kostel
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1723	[number]	k4	1723
založena	založen	k2eAgFnSc1d1	založena
Loretánská	loretánský	k2eAgFnSc1d1	Loretánská
kaple	kaple	k1gFnSc1	kaple
<g/>
,	,	kIx,	,
zrušená	zrušený	k2eAgFnSc1d1	zrušená
roku	rok	k1gInSc2	rok
1784	[number]	k4	1784
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Ducha	duch	k1gMnSc2	duch
(	(	kIx(	(
<g/>
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
hřbitovní	hřbitovní	k2eAgFnSc1d1	hřbitovní
kaple	kaple	k1gFnSc1	kaple
v	v	k7c6	v
parku	park	k1gInSc6	park
B.	B.	kA	B.
Smetany	Smetana	k1gMnSc2	Smetana
na	na	k7c6	na
severozápadním	severozápadní	k2eAgInSc6d1	severozápadní
okraji	okraj	k1gInSc6	okraj
historického	historický	k2eAgNnSc2d1	historické
města	město	k1gNnSc2	město
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
roku	rok	k1gInSc2	rok
1661	[number]	k4	1661
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
údaje	údaj	k1gInSc2	údaj
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
1572	[number]	k4	1572
<g/>
)	)	kIx)	)
v	v	k7c6	v
manýristickém	manýristický	k2eAgInSc6d1	manýristický
slohu	sloh	k1gInSc6	sloh
a	a	k8xC	a
romanticky	romanticky	k6eAd1	romanticky
přestavěna	přestavěn	k2eAgFnSc1d1	přestavěna
roku	rok	k1gInSc2	rok
1892	[number]	k4	1892
<g/>
.	.	kIx.	.
</s>
<s>
Palisádové	palisádový	k2eAgFnPc1d1	palisádová
hradby	hradba	k1gFnPc1	hradba
byly	být	k5eAaImAgFnP	být
budovány	budovat	k5eAaImNgFnP	budovat
kolem	kolem	k7c2	kolem
vznikajícího	vznikající	k2eAgNnSc2d1	vznikající
města	město	k1gNnSc2	město
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
založení	založení	k1gNnSc4	založení
před	před	k7c7	před
polovinou	polovina	k1gFnSc7	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
nahrazeny	nahradit	k5eAaPmNgFnP	nahradit
kamennými	kamenný	k2eAgInPc7d1	kamenný
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
zachovaly	zachovat	k5eAaPmAgFnP	zachovat
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
hlavní	hlavní	k2eAgFnSc2d1	hlavní
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
funkce	funkce	k1gFnSc2	funkce
mělo	mít	k5eAaImAgNnS	mít
město	město	k1gNnSc1	město
i	i	k8xC	i
vojenský	vojenský	k2eAgInSc1d1	vojenský
význam	význam	k1gInSc1	význam
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
strategické	strategický	k2eAgFnSc2d1	strategická
polohy	poloha	k1gFnSc2	poloha
na	na	k7c6	na
samotné	samotný	k2eAgFnSc6d1	samotná
západní	západní	k2eAgFnSc6d1	západní
hranici	hranice	k1gFnSc6	hranice
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Několikakilometrové	několikakilometrový	k2eAgNnSc4d1	několikakilometrové
opevnění	opevnění	k1gNnSc4	opevnění
tvořila	tvořit	k5eAaImAgFnS	tvořit
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
hradební	hradební	k2eAgFnSc1d1	hradební
zeď	zeď	k1gFnSc1	zeď
vysoká	vysoký	k2eAgFnSc1d1	vysoká
6	[number]	k4	6
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
parkán	parkán	k1gInSc1	parkán
s	s	k7c7	s
parkánovou	parkánový	k2eAgFnSc7d1	Parkánová
zdí	zeď	k1gFnSc7	zeď
a	a	k8xC	a
příkop	příkop	k1gInSc1	příkop
hluboký	hluboký	k2eAgInSc1d1	hluboký
až	až	k9	až
7	[number]	k4	7
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Hradební	hradební	k2eAgFnSc4d1	hradební
zeď	zeď	k1gFnSc4	zeď
zpevňovaly	zpevňovat	k5eAaImAgFnP	zpevňovat
věže	věž	k1gFnPc1	věž
<g/>
,	,	kIx,	,
v	v	k7c6	v
parkánovém	parkánový	k2eAgInSc6d1	parkánový
pásu	pás	k1gInSc6	pás
bašty	bašta	k1gFnSc2	bašta
<g/>
.	.	kIx.	.
</s>
<s>
Hradby	hradba	k1gFnPc1	hradba
byly	být	k5eAaImAgFnP	být
přerušeny	přerušit	k5eAaPmNgFnP	přerušit
pěti	pět	k4xCc7	pět
branami	brána	k1gFnPc7	brána
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
disponovaly	disponovat	k5eAaBmAgInP	disponovat
mnoha	mnoho	k4c7	mnoho
bezpečnostními	bezpečnostní	k2eAgInPc7d1	bezpečnostní
prvky	prvek	k1gInPc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Severně	severně	k6eAd1	severně
od	od	k7c2	od
brány	brána	k1gFnSc2	brána
Matky	matka	k1gFnSc2	matka
Boží	božit	k5eAaImIp3nS	božit
se	se	k3xPyFc4	se
zachoval	zachovat	k5eAaPmAgInS	zachovat
kus	kus	k1gInSc1	kus
hradby	hradba	k1gFnSc2	hradba
s	s	k7c7	s
příkopem	příkop	k1gInSc7	příkop
a	a	k8xC	a
valem	val	k1gInSc7	val
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postup	k1gInSc7	postup
doby	doba	k1gFnSc2	doba
byla	být	k5eAaImAgFnS	být
pevnostní	pevnostní	k2eAgFnSc1d1	pevnostní
soustava	soustava	k1gFnSc1	soustava
neustále	neustále	k6eAd1	neustále
zdokonalována	zdokonalován	k2eAgFnSc1d1	zdokonalována
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
řada	řada	k1gFnSc1	řada
předsunutých	předsunutý	k2eAgNnPc2d1	předsunuté
stanovišť	stanoviště	k1gNnPc2	stanoviště
a	a	k8xC	a
opevnění	opevnění	k1gNnPc2	opevnění
<g/>
,	,	kIx,	,
za	za	k7c2	za
švédské	švédský	k2eAgFnSc2d1	švédská
okupace	okupace	k1gFnSc2	okupace
1645	[number]	k4	1645
<g/>
–	–	k?	–
<g/>
1647	[number]	k4	1647
byly	být	k5eAaImAgInP	být
před	před	k7c7	před
ní	on	k3xPp3gFnSc7	on
vybudovány	vybudován	k2eAgFnPc4d1	vybudována
mohutné	mohutný	k2eAgFnPc4d1	mohutná
bastiony	bastiona	k1gFnPc4	bastiona
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1755	[number]	k4	1755
byla	být	k5eAaImAgFnS	být
jihlavská	jihlavský	k2eAgFnSc1d1	Jihlavská
pevnost	pevnost	k1gFnSc1	pevnost
zrušena	zrušen	k2eAgFnSc1d1	zrušena
–	–	k?	–
díky	díky	k7c3	díky
vývoji	vývoj	k1gInSc3	vývoj
techniky	technika	k1gFnSc2	technika
byla	být	k5eAaImAgFnS	být
již	již	k9	již
tato	tento	k3xDgFnSc1	tento
koncepce	koncepce	k1gFnSc1	koncepce
opevnění	opevnění	k1gNnSc2	opevnění
překonána	překonán	k2eAgFnSc1d1	překonána
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgFnP	být
bourány	bourán	k2eAgFnPc1d1	bourána
brány	brána	k1gFnPc1	brána
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
bránily	bránit	k5eAaImAgInP	bránit
narůstající	narůstající	k2eAgFnSc4d1	narůstající
dopravě	doprava	k1gFnSc3	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pěti	pět	k4xCc2	pět
bran	brána	k1gFnPc2	brána
zůstala	zůstat	k5eAaPmAgFnS	zůstat
zachována	zachovat	k5eAaPmNgFnS	zachovat
jen	jen	k9	jen
jediná	jediný	k2eAgFnSc1d1	jediná
–	–	k?	–
brána	brána	k1gFnSc1	brána
Matky	matka	k1gFnSc2	matka
Boží	boží	k2eAgFnSc2d1	boží
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
zachovaný	zachovaný	k2eAgInSc1d1	zachovaný
pás	pás	k1gInSc1	pás
hradeb	hradba	k1gFnPc2	hradba
je	být	k5eAaImIp3nS	být
rekonstruován	rekonstruovat	k5eAaBmNgMnS	rekonstruovat
podle	podle	k7c2	podle
barokní	barokní	k2eAgFnSc2d1	barokní
podoby	podoba	k1gFnSc2	podoba
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
včetně	včetně	k7c2	včetně
parkové	parkový	k2eAgFnSc2d1	parková
úpravy	úprava	k1gFnSc2	úprava
parkánu	parkán	k1gInSc2	parkán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Březinových	Březinových	k2eAgInPc6d1	Březinových
sadech	sad	k1gInPc6	sad
stojí	stát	k5eAaImIp3nS	stát
velitelský	velitelský	k2eAgInSc4d1	velitelský
bunkr	bunkr	k1gInSc4	bunkr
ze	z	k7c2	z
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
K	K	kA	K
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Brána	brána	k1gFnSc1	brána
Matky	matka	k1gFnSc2	matka
Boží	boží	k2eAgFnSc2d1	boží
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
typickým	typický	k2eAgInSc7d1	typický
symbolem	symbol	k1gInSc7	symbol
města	město	k1gNnSc2	město
a	a	k8xC	a
jedinou	jediný	k2eAgFnSc7d1	jediná
branou	brána	k1gFnSc7	brána
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
z	z	k7c2	z
pěti	pět	k4xCc2	pět
středověkých	středověký	k2eAgFnPc2d1	středověká
městských	městský	k2eAgFnPc2d1	městská
bran	brána	k1gFnPc2	brána
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
výstavbou	výstavba	k1gFnSc7	výstavba
opevnění	opevnění	k1gNnSc2	opevnění
–	–	k?	–
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
2	[number]	k4	2
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šestnáctém	šestnáctý	k4xOgInSc6	šestnáctý
století	století	k1gNnSc6	století
prošla	projít	k5eAaPmAgFnS	projít
brána	brán	k2eAgFnSc1d1	brána
gotickými	gotický	k2eAgFnPc7d1	gotická
i	i	k8xC	i
renesančními	renesanční	k2eAgFnPc7d1	renesanční
úpravami	úprava	k1gFnPc7	úprava
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Specifická	specifický	k2eAgFnSc1d1	specifická
podoba	podoba	k1gFnSc1	podoba
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
atikou	atika	k1gFnSc7	atika
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
věže	věž	k1gFnSc2	věž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1853	[number]	k4	1853
byla	být	k5eAaImAgFnS	být
brána	brána	k1gFnSc1	brána
renovována	renovovat	k5eAaBmNgFnS	renovovat
a	a	k8xC	a
opatřena	opatřit	k5eAaPmNgFnS	opatřit
hodinami	hodina	k1gFnPc7	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Složité	složitý	k2eAgNnSc1d1	složité
předbraní	předbraní	k1gNnSc1	předbraní
s	s	k7c7	s
obranými	obraný	k2eAgInPc7d1	obraný
systémy	systém	k1gInPc7	systém
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1862	[number]	k4	1862
zbouráno	zbourat	k5eAaPmNgNnS	zbourat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1995	[number]	k4	1995
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
brána	brána	k1gFnSc1	brána
staticky	staticky	k6eAd1	staticky
zajištěna	zajistit	k5eAaPmNgFnS	zajistit
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
poklesu	pokles	k1gInSc2	pokles
základů	základ	k1gInPc2	základ
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
koruna	koruna	k1gFnSc1	koruna
však	však	k9	však
byla	být	k5eAaImAgFnS	být
značně	značně	k6eAd1	značně
porušená	porušený	k2eAgFnSc1d1	porušená
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
kompletní	kompletní	k2eAgFnSc3d1	kompletní
rekonstrukci	rekonstrukce	k1gFnSc3	rekonstrukce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
brána	brána	k1gFnSc1	brána
zpřístupněna	zpřístupněn	k2eAgFnSc1d1	zpřístupněna
veřejnosti	veřejnost	k1gFnSc3	veřejnost
jako	jako	k8xS	jako
vyhlídková	vyhlídkový	k2eAgFnSc1d1	vyhlídková
věž	věž	k1gFnSc1	věž
a	a	k8xC	a
malé	malý	k2eAgNnSc1d1	malé
muzeum	muzeum	k1gNnSc1	muzeum
jihlavského	jihlavský	k2eAgNnSc2d1	jihlavské
podzemí	podzemí	k1gNnSc2	podzemí
a	a	k8xC	a
dolování	dolování	k1gNnSc2	dolování
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
bráně	brána	k1gFnSc6	brána
rovněž	rovněž	k9	rovněž
sídlí	sídlet	k5eAaImIp3nS	sídlet
Jihlavská	jihlavský	k2eAgFnSc1d1	Jihlavská
astronomická	astronomický	k2eAgFnSc1d1	astronomická
společnost	společnost	k1gFnSc1	společnost
a	a	k8xC	a
každé	každý	k3xTgNnSc4	každý
pondělí	pondělí	k1gNnSc4	pondělí
po	po	k7c6	po
setmění	setmění	k1gNnSc6	setmění
zde	zde	k6eAd1	zde
probíhají	probíhat	k5eAaImIp3nP	probíhat
veřejná	veřejný	k2eAgNnPc4d1	veřejné
pozorování	pozorování	k1gNnPc4	pozorování
oblohy	obloha	k1gFnSc2	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Jihlavské	jihlavský	k2eAgFnPc4d1	Jihlavská
katakomby	katakomby	k1gFnPc4	katakomby
<g/>
.	.	kIx.	.
</s>
<s>
Jihlavské	jihlavský	k2eAgNnSc1d1	jihlavské
podzemí	podzemí	k1gNnSc1	podzemí
tvoří	tvořit	k5eAaImIp3nS	tvořit
další	další	k2eAgFnSc4d1	další
významnou	významný	k2eAgFnSc4d1	významná
památku	památka	k1gFnSc4	památka
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
celková	celkový	k2eAgFnSc1d1	celková
rozloha	rozloha	k1gFnSc1	rozloha
je	být	k5eAaImIp3nS	být
50	[number]	k4	50
000	[number]	k4	000
m2	m2	k4	m2
a	a	k8xC	a
délka	délka	k1gFnSc1	délka
přibližně	přibližně	k6eAd1	přibližně
25	[number]	k4	25
km	km	kA	km
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jej	on	k3xPp3gMnSc4	on
činí	činit	k5eAaImIp3nS	činit
druhým	druhý	k4xOgInSc7	druhý
největším	veliký	k2eAgInSc7d3	veliký
podzemním	podzemní	k2eAgInSc7d1	podzemní
labyrintem	labyrint	k1gInSc7	labyrint
v	v	k7c6	v
ČR	ČR	kA	ČR
hned	hned	k6eAd1	hned
po	po	k7c6	po
Znojmu	Znojmo	k1gNnSc6	Znojmo
<g/>
.	.	kIx.	.
</s>
<s>
Chodby	chodba	k1gFnPc1	chodba
jsou	být	k5eAaImIp3nP	být
raženy	razit	k5eAaImNgFnP	razit
ve	v	k7c6	v
skále	skála	k1gFnSc6	skála
pod	pod	k7c7	pod
celým	celý	k2eAgNnSc7d1	celé
městem	město	k1gNnSc7	město
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
až	až	k6eAd1	až
třech	tři	k4xCgNnPc6	tři
podlažích	podlaží	k1gNnPc6	podlaží
pod	pod	k7c7	pod
sebou	se	k3xPyFc7	se
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
od	od	k7c2	od
2	[number]	k4	2
do	do	k7c2	do
14	[number]	k4	14
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Praktická	praktický	k2eAgFnSc1d1	praktická
stavba	stavba	k1gFnSc1	stavba
začala	začít	k5eAaPmAgFnS	začít
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
měšťané	měšťan	k1gMnPc1	měšťan
začali	začít	k5eAaPmAgMnP	začít
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
rozšiřovat	rozšiřovat	k5eAaImF	rozšiřovat
svoje	svůj	k3xOyFgInPc4	svůj
sklepy	sklep	k1gInPc4	sklep
<g/>
.	.	kIx.	.
</s>
<s>
Ražbu	ražba	k1gFnSc4	ražba
prováděli	provádět	k5eAaImAgMnP	provádět
místní	místní	k2eAgMnPc1d1	místní
zkušení	zkušený	k2eAgMnPc1d1	zkušený
havíři	havíř	k1gMnPc1	havíř
<g/>
.	.	kIx.	.
</s>
<s>
Hlubší	hluboký	k2eAgFnPc1d2	hlubší
prostory	prostora	k1gFnPc1	prostora
se	se	k3xPyFc4	se
hloubily	hloubit	k5eAaImAgFnP	hloubit
v	v	k7c6	v
pozdějších	pozdní	k2eAgFnPc6d2	pozdější
dobách	doba	k1gFnPc6	doba
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
sloužila	sloužit	k5eAaImAgFnS	sloužit
část	část	k1gFnSc1	část
chodeb	chodba	k1gFnPc2	chodba
jako	jako	k9	jako
protiletecký	protiletecký	k2eAgInSc4d1	protiletecký
kryt	kryt	k1gInSc4	kryt
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
většina	většina	k1gFnSc1	většina
tunelů	tunel	k1gInPc2	tunel
odborně	odborně	k6eAd1	odborně
zpevněna	zpevněn	k2eAgFnSc1d1	zpevněna
a	a	k8xC	a
veřejnosti	veřejnost	k1gFnSc3	veřejnost
je	být	k5eAaImIp3nS	být
přístupno	přístupen	k2eAgNnSc1d1	přístupno
několik	několik	k4yIc1	několik
set	sto	k4xCgNnPc2	sto
metrů	metr	k1gInPc2	metr
podzemních	podzemní	k2eAgFnPc2d1	podzemní
chodeb	chodba	k1gFnPc2	chodba
<g/>
.	.	kIx.	.
</s>
<s>
Známá	známá	k1gFnSc1	známá
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
svítící	svítící	k2eAgFnSc1d1	svítící
chodba	chodba	k1gFnSc1	chodba
<g/>
,	,	kIx,	,
krátký	krátký	k2eAgInSc1d1	krátký
úsek	úsek	k1gInSc1	úsek
katakomb	katakomby	k1gFnPc2	katakomby
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc4	jehož
stěny	stěna	k1gFnPc4	stěna
ve	v	k7c6	v
tmě	tma	k1gFnSc6	tma
svítí	svítit	k5eAaImIp3nS	svítit
slabě	slabě	k6eAd1	slabě
nazelenalým	nazelenalý	k2eAgNnSc7d1	nazelenalé
světlem	světlo	k1gNnSc7	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Příčiny	příčina	k1gFnPc1	příčina
tohoto	tento	k3xDgInSc2	tento
jevu	jev	k1gInSc2	jev
dosud	dosud	k6eAd1	dosud
nebyly	být	k5eNaImAgFnP	být
uspokojivě	uspokojivě	k6eAd1	uspokojivě
objasněny	objasněn	k2eAgFnPc1d1	objasněna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
svítící	svítící	k2eAgFnSc1d1	svítící
chodba	chodba	k1gFnSc1	chodba
populární	populární	k2eAgFnSc1d1	populární
turistická	turistický	k2eAgFnSc1d1	turistická
atrakce	atrakce	k1gFnSc1	atrakce
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
však	však	k9	však
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
okolí	okolí	k1gNnSc6	okolí
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
paranormální	paranormální	k2eAgInPc1d1	paranormální
jevy	jev	k1gInPc1	jev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
oznámili	oznámit	k5eAaPmAgMnP	oznámit
správci	správce	k1gMnPc1	správce
podzemí	podzemí	k1gNnSc2	podzemí
existenci	existence	k1gFnSc3	existence
druhé	druhý	k4xOgFnSc2	druhý
svítící	svítící	k2eAgFnSc2d1	svítící
chodby	chodba	k1gFnSc2	chodba
pod	pod	k7c7	pod
ulicí	ulice	k1gFnSc7	ulice
Čajkovského	Čajkovský	k2eAgNnSc2d1	Čajkovský
<g/>
.	.	kIx.	.
</s>
<s>
Hraniční	hraniční	k2eAgInPc1d1	hraniční
kameny	kámen	k1gInPc1	kámen
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgInP	být
vystavěny	vystavět	k5eAaPmNgInP	vystavět
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
opticky	opticky	k6eAd1	opticky
oddělovaly	oddělovat	k5eAaImAgFnP	oddělovat
a	a	k8xC	a
znázorňovaly	znázorňovat	k5eAaImAgFnP	znázorňovat
hranice	hranice	k1gFnPc1	hranice
mezi	mezi	k7c7	mezi
Čechami	Čechy	k1gFnPc7	Čechy
a	a	k8xC	a
Moravou	Morava	k1gFnSc7	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Nazývají	nazývat	k5eAaImIp3nP	nazývat
se	se	k3xPyFc4	se
také	také	k9	také
hraničníky	hraničník	k1gInPc1	hraničník
<g/>
.	.	kIx.	.
</s>
<s>
Nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
a	a	k8xC	a
v	v	k7c6	v
těsném	těsný	k2eAgNnSc6d1	těsné
okolí	okolí	k1gNnSc6	okolí
<g/>
,	,	kIx,	,
nevedou	vést	k5eNaImIp3nP	vést
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
žádné	žádný	k3yNgFnSc2	žádný
turistické	turistický	k2eAgFnSc2d1	turistická
značky	značka	k1gFnSc2	značka
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
čtyři	čtyři	k4xCgInPc1	čtyři
zachovalé	zachovalý	k2eAgInPc1d1	zachovalý
čtyřboké	čtyřboký	k2eAgInPc1d1	čtyřboký
jehlany	jehlan	k1gInPc1	jehlan
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
výzdobu	výzdoba	k1gFnSc4	výzdoba
i	i	k8xC	i
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
vybíhají	vybíhat	k5eAaImIp3nP	vybíhat
z	z	k7c2	z
hranolových	hranolový	k2eAgInPc2d1	hranolový
soklů	sokl	k1gInPc2	sokl
<g/>
,	,	kIx,	,
na	na	k7c6	na
protilehlých	protilehlý	k2eAgFnPc6d1	protilehlá
stranách	strana	k1gFnPc6	strana
mají	mít	k5eAaImIp3nP	mít
dva	dva	k4xCgInPc4	dva
výrazné	výrazný	k2eAgInPc4d1	výrazný
reliéfy	reliéf	k1gInPc4	reliéf
heraldických	heraldický	k2eAgInPc2d1	heraldický
znaků	znak	k1gInPc2	znak
Čech	Čechy	k1gFnPc2	Čechy
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
lev	lev	k1gInSc1	lev
<g/>
)	)	kIx)	)
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
(	(	kIx(	(
<g/>
moravská	moravský	k2eAgFnSc1d1	Moravská
orlice	orlice	k1gFnSc1	orlice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1750	[number]	k4	1750
je	být	k5eAaImIp3nS	být
nařídila	nařídit	k5eAaPmAgFnS	nařídit
vystavět	vystavět	k5eAaPmF	vystavět
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
rozřešila	rozřešit	k5eAaPmAgFnS	rozřešit
spory	spor	k1gInPc4	spor
o	o	k7c4	o
přesné	přesný	k2eAgNnSc4d1	přesné
vymezení	vymezení	k1gNnSc4	vymezení
česko-moravské	českooravský	k2eAgFnSc2d1	česko-moravská
hranice	hranice	k1gFnSc2	hranice
severně	severně	k6eAd1	severně
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Jihlavy	Jihlava	k1gFnSc2	Jihlava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jehlanech	jehlan	k1gInPc6	jehlan
jsou	být	k5eAaImIp3nP	být
vyryté	vyrytý	k2eAgInPc1d1	vyrytý
nápisy	nápis	k1gInPc1	nápis
LIMITES	LIMITES	kA	LIMITES
BOHEMIAM	BOHEMIAM	kA	BOHEMIAM
INTER	INTER	kA	INTER
ET	ET	kA	ET
MORAVIAM	MORAVIAM	kA	MORAVIAM
–	–	k?	–
MARIA	Mario	k1gMnSc2	Mario
THERESIA	THERESIA	kA	THERESIA
AUGUSTA	Augusta	k1gMnSc1	Augusta
REGNI	REGNI	kA	REGNI
BOHEMIA	bohemia	k1gFnSc1	bohemia
SCEPTRA	sceptrum	k1gNnSc2	sceptrum
TENENTE	TENENTE	kA	TENENTE
–	–	k?	–
POSITI	POSITI	kA	POSITI
ANNO	Anna	k1gFnSc5	Anna
MDCCL	MDCCL	kA	MDCCL
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
samotné	samotný	k2eAgInPc4d1	samotný
památníky	památník	k1gInPc4	památník
dokončil	dokončit	k5eAaPmAgMnS	dokončit
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1752	[number]	k4	1752
polenský	polenský	k2eAgMnSc1d1	polenský
sochař	sochař	k1gMnSc1	sochař
Václav	Václav	k1gMnSc1	Václav
Viktor	Viktor	k1gMnSc1	Viktor
Morávek	Morávek	k1gMnSc1	Morávek
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
z	z	k7c2	z
hraničníků	hraničník	k1gMnPc2	hraničník
se	se	k3xPyFc4	se
vypíná	vypínat	k5eAaImIp3nS	vypínat
těsně	těsně	k6eAd1	těsně
za	za	k7c7	za
severním	severní	k2eAgInSc7d1	severní
okrajem	okraj	k1gInSc7	okraj
města	město	k1gNnSc2	město
vpravo	vpravo	k6eAd1	vpravo
při	při	k7c6	při
silnici	silnice	k1gFnSc6	silnice
do	do	k7c2	do
Heroltic	Heroltice	k1gFnPc2	Heroltice
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgMnSc1	druhý
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Sokolovské	sokolovský	k2eAgFnSc6d1	Sokolovská
ulici	ulice	k1gFnSc6	ulice
u	u	k7c2	u
zastávky	zastávka	k1gFnSc2	zastávka
Jihlavské	jihlavský	k2eAgNnSc1d1	jihlavské
dřevařské	dřevařský	k2eAgInPc1d1	dřevařský
závody	závod	k1gInPc1	závod
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc1	třetí
kámen	kámen	k1gInSc1	kámen
stojí	stát	k5eAaImIp3nS	stát
u	u	k7c2	u
nákupního	nákupní	k2eAgNnSc2d1	nákupní
centra	centrum	k1gNnSc2	centrum
na	na	k7c6	na
nároží	nároží	k1gNnSc6	nároží
ulic	ulice	k1gFnPc2	ulice
Sokolovská	sokolovský	k2eAgFnSc1d1	Sokolovská
a	a	k8xC	a
Jiřího	Jiří	k1gMnSc4	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
(	(	kIx(	(
<g/>
nedaleká	daleký	k2eNgFnSc1d1	nedaleká
komunikace	komunikace	k1gFnSc1	komunikace
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
U	u	k7c2	u
Hraničníku	hraničník	k1gInSc2	hraničník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1	poslední
je	být	k5eAaImIp3nS	být
umístěn	umístěn	k2eAgInSc1d1	umístěn
na	na	k7c4	na
rozhraní	rozhraní	k1gNnSc4	rozhraní
ulic	ulice	k1gFnPc2	ulice
Polenské	Polenský	k2eAgFnSc2d1	Polenská
a	a	k8xC	a
U	u	k7c2	u
Skály	skála	k1gFnSc2	skála
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
hraniční	hraniční	k2eAgInSc4d1	hraniční
kámen	kámen	k1gInSc4	kámen
u	u	k7c2	u
silnice	silnice	k1gFnSc2	silnice
do	do	k7c2	do
Heroltic	Heroltice	k1gFnPc2	Heroltice
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
zemské	zemský	k2eAgFnSc6d1	zemská
hranici	hranice	k1gFnSc6	hranice
<g/>
;	;	kIx,	;
ostatní	ostatní	k2eAgInPc1d1	ostatní
jsou	být	k5eAaImIp3nP	být
umístěné	umístěný	k2eAgInPc1d1	umístěný
v	v	k7c4	v
různé	různý	k2eAgFnPc4d1	různá
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
odchylky	odchylka	k1gFnPc4	odchylka
od	od	k7c2	od
zemské	zemský	k2eAgFnSc2d1	zemská
hranice	hranice	k1gFnSc2	hranice
jsou	být	k5eAaImIp3nP	být
patrné	patrný	k2eAgFnPc1d1	patrná
i	i	k9	i
ze	z	k7c2	z
zakreslení	zakreslení	k1gNnSc2	zakreslení
těchto	tento	k3xDgInPc2	tento
kamenů	kámen	k1gInPc2	kámen
na	na	k7c6	na
mapách	mapa	k1gFnPc6	mapa
císařských	císařský	k2eAgInPc2d1	císařský
otisků	otisk	k1gInPc2	otisk
stabilního	stabilní	k2eAgInSc2d1	stabilní
katastru	katastr	k1gInSc2	katastr
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1835	[number]	k4	1835
<g/>
.	.	kIx.	.
</s>
<s>
Pomník	pomník	k1gInSc1	pomník
královské	královský	k2eAgFnSc2d1	královská
přísahy	přísaha	k1gFnSc2	přísaha
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1565	[number]	k4	1565
<g/>
,	,	kIx,	,
nechali	nechat	k5eAaPmAgMnP	nechat
jej	on	k3xPp3gNnSc4	on
vybudovat	vybudovat	k5eAaPmF	vybudovat
představitelé	představitel	k1gMnPc1	představitel
města	město	k1gNnSc2	město
jako	jako	k8xC	jako
připomínku	připomínka	k1gFnSc4	připomínka
slavnostní	slavnostní	k2eAgFnSc2d1	slavnostní
přísahy	přísaha	k1gFnSc2	přísaha
rakouského	rakouský	k2eAgMnSc2d1	rakouský
arcivévody	arcivévoda	k1gMnSc2	arcivévoda
a	a	k8xC	a
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
I.	I.	kA	I.
Habsburského	habsburský	k2eAgNnSc2d1	habsburské
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
na	na	k7c6	na
někdejší	někdejší	k2eAgFnSc6d1	někdejší
Císařské	císařský	k2eAgFnSc6d1	císařská
louce	louka	k1gFnSc6	louka
u	u	k7c2	u
Dlouhého	Dlouhého	k2eAgInSc2d1	Dlouhého
mostu	most	k1gInSc2	most
na	na	k7c6	na
česko-moravské	českooravský	k2eAgFnSc6d1	česko-moravská
hranici	hranice	k1gFnSc6	hranice
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1527	[number]	k4	1527
<g/>
.	.	kIx.	.
</s>
<s>
Vysochal	Vysochat	k5eAaImAgMnS	Vysochat
jej	on	k3xPp3gNnSc4	on
nepříliš	příliš	k6eNd1	příliš
známý	známý	k2eAgMnSc1d1	známý
kameník	kameník	k1gMnSc1	kameník
Štěpán	Štěpán	k1gMnSc1	Štěpán
Tettelmayer	Tettelmayer	k1gMnSc1	Tettelmayer
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
tyčí	tyčit	k5eAaImIp3nP	tyčit
nedaleko	nedaleko	k7c2	nedaleko
Pražského	pražský	k2eAgInSc2d1	pražský
mostu	most	k1gInSc2	most
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
Jihlavy	Jihlava	k1gFnSc2	Jihlava
<g/>
.	.	kIx.	.
</s>
<s>
Návrší	návrší	k1gNnSc1	návrší
i	i	k8xC	i
ulice	ulice	k1gFnSc1	ulice
nad	nad	k7c7	nad
řekou	řeka	k1gFnSc7	řeka
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
Královský	královský	k2eAgInSc4d1	královský
vršek	vršek	k1gInSc4	vršek
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
čtyřboký	čtyřboký	k2eAgInSc1d1	čtyřboký
monument	monument	k1gInSc1	monument
s	s	k7c7	s
trojúhelníkovým	trojúhelníkový	k2eAgInSc7d1	trojúhelníkový
štítem	štít	k1gInSc7	štít
<g/>
,	,	kIx,	,
římsou	římsa	k1gFnSc7	římsa
a	a	k8xC	a
pilastry	pilastr	k1gInPc1	pilastr
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
přední	přední	k2eAgFnSc6d1	přední
stěně	stěna	k1gFnSc6	stěna
výklenek	výklenek	k1gInSc4	výklenek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
chrání	chránit	k5eAaImIp3nS	chránit
kovová	kovový	k2eAgFnSc1d1	kovová
mříž	mříž	k1gFnSc1	mříž
a	a	k8xC	a
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
vyplněn	vyplněn	k2eAgMnSc1d1	vyplněn
pozdně	pozdně	k6eAd1	pozdně
renesanční	renesanční	k2eAgFnSc7d1	renesanční
mramorovou	mramorový	k2eAgFnSc7d1	mramorová
deskou	deska	k1gFnSc7	deska
<g/>
,	,	kIx,	,
na	na	k7c6	na
reliéfech	reliéf	k1gInPc6	reliéf
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
najít	najít	k5eAaPmF	najít
říšské	říšský	k2eAgNnSc4d1	říšské
jablko	jablko	k1gNnSc4	jablko
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
korunami	koruna	k1gFnPc7	koruna
na	na	k7c6	na
horní	horní	k2eAgFnSc6d1	horní
římse	římsa	k1gFnSc6	římsa
a	a	k8xC	a
pod	pod	k7c7	pod
ním	on	k3xPp3gMnSc7	on
vykrajovaný	vykrajovaný	k2eAgInSc1d1	vykrajovaný
štít	štít	k1gInSc4	štít
s	s	k7c7	s
českým	český	k2eAgInSc7d1	český
a	a	k8xC	a
uherským	uherský	k2eAgInSc7d1	uherský
erbem	erb	k1gInSc7	erb
a	a	k8xC	a
menším	malý	k2eAgInSc7d2	menší
rakouským	rakouský	k2eAgInSc7d1	rakouský
znakem	znak	k1gInSc7	znak
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
<g/>
.	.	kIx.	.
</s>
<s>
Dole	dole	k6eAd1	dole
stojí	stát	k5eAaImIp3nS	stát
latinský	latinský	k2eAgInSc1d1	latinský
nápis	nápis	k1gInSc1	nápis
<g/>
,	,	kIx,	,
připomínající	připomínající	k2eAgFnSc4d1	připomínající
přísahu	přísaha	k1gFnSc4	přísaha
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Povodně	povodně	k6eAd1	povodně
tuto	tento	k3xDgFnSc4	tento
pamětihodnost	pamětihodnost	k1gFnSc4	pamětihodnost
několikráte	několikráte	k6eAd1	několikráte
poškodily	poškodit	k5eAaPmAgInP	poškodit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
hodnověrnost	hodnověrnost	k1gFnSc4	hodnověrnost
nacházela	nacházet	k5eAaImAgFnS	nacházet
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
záplavovém	záplavový	k2eAgNnSc6d1	záplavové
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Opravy	oprava	k1gFnPc1	oprava
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1640	[number]	k4	1640
a	a	k8xC	a
1830	[number]	k4	1830
<g/>
.	.	kIx.	.
</s>
<s>
Generální	generální	k2eAgFnSc1d1	generální
revize	revize	k1gFnSc1	revize
stavbu	stavba	k1gFnSc4	stavba
postihla	postihnout	k5eAaPmAgFnS	postihnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1859	[number]	k4	1859
<g/>
,	,	kIx,	,
restaurátoři	restaurátor	k1gMnPc1	restaurátor
ji	on	k3xPp3gFnSc4	on
rozebrali	rozebrat	k5eAaPmAgMnP	rozebrat
<g/>
,	,	kIx,	,
vyčistili	vyčistit	k5eAaPmAgMnP	vyčistit
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
sestavili	sestavit	k5eAaPmAgMnP	sestavit
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
nový	nový	k2eAgInSc1d1	nový
<g/>
,	,	kIx,	,
vyšší	vysoký	k2eAgInSc1d2	vyšší
podstavec	podstavec	k1gInSc1	podstavec
z	z	k7c2	z
přitesaných	přitesaný	k2eAgInPc2d1	přitesaný
žulových	žulový	k2eAgInPc2d1	žulový
kamenů	kámen	k1gInPc2	kámen
a	a	k8xC	a
čtyři	čtyři	k4xCgInPc4	čtyři
patníky	patník	k1gInPc4	patník
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
pomník	pomník	k1gInSc1	pomník
chrání	chránit	k5eAaImIp3nS	chránit
před	před	k7c7	před
naplaveninami	naplavenina	k1gFnPc7	naplavenina
<g/>
.	.	kIx.	.
</s>
<s>
Přidali	přidat	k5eAaPmAgMnP	přidat
desku	deska	k1gFnSc4	deska
s	s	k7c7	s
německý	německý	k2eAgInSc1d1	německý
nápisem	nápis	k1gInSc7	nápis
o	o	k7c4	o
renovaci	renovace	k1gFnSc4	renovace
a	a	k8xC	a
doplnění	doplnění	k1gNnSc4	doplnění
pomníku	pomník	k1gInSc2	pomník
<g/>
.	.	kIx.	.
</s>
<s>
Obchodní	obchodní	k2eAgInSc4d1	obchodní
dům	dům	k1gInSc4	dům
Prior	prior	k1gMnSc1	prior
Související	související	k2eAgFnSc2d1	související
informace	informace	k1gFnSc2	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Seznam	seznam	k1gInSc4	seznam
rodáků	rodák	k1gMnPc2	rodák
a	a	k8xC	a
obyvatel	obyvatel	k1gMnPc2	obyvatel
Jihlavy	Jihlava	k1gFnSc2	Jihlava
a	a	k8xC	a
Seznam	seznam	k1gInSc1	seznam
představitelů	představitel	k1gMnPc2	představitel
Jihlavy	Jihlava	k1gFnSc2	Jihlava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
žily	žít	k5eAaImAgFnP	žít
<g/>
,	,	kIx,	,
narodily	narodit	k5eAaPmAgFnP	narodit
se	se	k3xPyFc4	se
či	či	k8xC	či
zemřely	zemřít	k5eAaPmAgFnP	zemřít
mnohé	mnohý	k2eAgFnPc1d1	mnohá
osobnosti	osobnost	k1gFnPc1	osobnost
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
vědy	věda	k1gFnSc2	věda
<g/>
,	,	kIx,	,
sportu	sport	k1gInSc2	sport
či	či	k8xC	či
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Heidenheim	Heidenheim	k1gInSc1	Heidenheim
an	an	k?	an
der	drát	k5eAaImRp2nS	drát
Brenz	Brenz	k1gInSc1	Brenz
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
Purmerend	Purmerenda	k1gFnPc2	Purmerenda
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
Užhorod	Užhorod	k1gInSc1	Užhorod
<g/>
,	,	kIx,	,
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
Wu-chan	Wuhana	k1gFnPc2	Wu-chana
<g/>
,	,	kIx,	,
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
</s>
