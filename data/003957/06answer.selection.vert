<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
osadě	osada	k1gFnSc6	osada
jménem	jméno	k1gNnSc7	jméno
Jihlava	Jihlava	k1gFnSc1	Jihlava
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1233	[number]	k4	1233
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
biskup	biskup	k1gMnSc1	biskup
Robert	Robert	k1gMnSc1	Robert
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
převod	převod	k1gInSc4	převod
zboží	zboží	k1gNnSc2	zboží
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
figuroval	figurovat	k5eAaImAgInS	figurovat
i	i	k9	i
název	název	k1gInSc1	název
Gyglaua	Gyglau	k1gInSc2	Gyglau
–	–	k?	–
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
)	)	kIx)	)
řádu	řád	k1gInSc2	řád
německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
do	do	k7c2	do
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
želivského	želivský	k2eAgInSc2d1	želivský
kláštera	klášter	k1gInSc2	klášter
<g/>
.	.	kIx.	.
</s>
