<p>
<s>
Jasan	jasan	k1gInSc1	jasan
u	u	k7c2	u
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
v	v	k7c6	v
Dubči	Dubč	k1gFnSc6	Dubč
je	být	k5eAaImIp3nS	být
památný	památný	k2eAgInSc4d1	památný
strom	strom	k1gInSc4	strom
v	v	k7c6	v
Dubči	Dubč	k1gFnSc6	Dubč
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jasan	jasan	k1gInSc4	jasan
ztepilý	ztepilý	k2eAgInSc4d1	ztepilý
(	(	kIx(	(
<g/>
Fraxinus	Fraxinus	k1gInSc4	Fraxinus
excelsior	excelsiora	k1gFnPc2	excelsiora
<g/>
)	)	kIx)	)
rostoucí	rostoucí	k2eAgFnSc2d1	rostoucí
na	na	k7c6	na
Lipovém	lipový	k2eAgNnSc6d1	lipové
náměstí	náměstí	k1gNnSc6	náměstí
před	před	k7c7	před
branou	brána	k1gFnSc7	brána
kostela	kostel	k1gInSc2	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Strom	strom	k1gInSc1	strom
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
výšky	výška	k1gFnSc2	výška
28	[number]	k4	28
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
obvod	obvod	k1gInSc1	obvod
kmene	kmen	k1gInSc2	kmen
byl	být	k5eAaImAgInS	být
při	při	k7c6	při
posledním	poslední	k2eAgNnSc6d1	poslední
měření	měření	k1gNnSc6	měření
371	[number]	k4	371
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
.	.	kIx.	.
</s>
<s>
Památným	památný	k2eAgInSc7d1	památný
stromem	strom	k1gInSc7	strom
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
28	[number]	k4	28
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vysazení	vysazení	k1gNnSc1	vysazení
stromu	strom	k1gInSc2	strom
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
kostela	kostel	k1gInSc2	kostel
v	v	k7c6	v
letech	let	k1gInPc6	let
1867	[number]	k4	1867
–	–	k?	–
1868	[number]	k4	1868
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
příležitosti	příležitost	k1gFnSc6	příležitost
byly	být	k5eAaImAgFnP	být
před	před	k7c7	před
kostelem	kostel	k1gInSc7	kostel
vysazeny	vysazen	k2eAgInPc4d1	vysazen
dva	dva	k4xCgInPc4	dva
jasany	jasan	k1gInPc4	jasan
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
stojí	stát	k5eAaImIp3nP	stát
do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
však	však	k9	však
byl	být	k5eAaImAgMnS	být
poražen	porazit	k5eAaPmNgMnS	porazit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
kmen	kmen	k1gInSc1	kmen
pak	pak	k6eAd1	pak
posloužil	posloužit	k5eAaPmAgInS	posloužit
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
barikády	barikáda	k1gFnSc2	barikáda
proti	proti	k7c3	proti
německému	německý	k2eAgNnSc3d1	německé
vojsku	vojsko	k1gNnSc3	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Dochovaný	dochovaný	k2eAgInSc1d1	dochovaný
strom	strom	k1gInSc1	strom
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
laicky	laicky	k6eAd1	laicky
ošetřován	ošetřován	k2eAgMnSc1d1	ošetřován
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
spodní	spodní	k2eAgFnPc1d1	spodní
větve	větev	k1gFnPc1	větev
byly	být	k5eAaImAgFnP	být
neopatrně	opatrně	k6eNd1	opatrně
odstraněny	odstranit	k5eAaPmNgFnP	odstranit
až	až	k9	až
na	na	k7c4	na
kmen	kmen	k1gInSc4	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zásah	zásah	k1gInSc1	zásah
způsobil	způsobit	k5eAaPmAgInS	způsobit
vyzvednutí	vyzvednutí	k1gNnSc4	vyzvednutí
těžiště	těžiště	k1gNnSc2	těžiště
koruny	koruna	k1gFnSc2	koruna
a	a	k8xC	a
nekontrolovatelnou	kontrolovatelný	k2eNgFnSc4d1	nekontrolovatelná
tvorbu	tvorba	k1gFnSc4	tvorba
korunových	korunový	k2eAgInPc2d1	korunový
výmladků	výmladek	k1gInPc2	výmladek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Důvod	důvod	k1gInSc1	důvod
ochrany	ochrana	k1gFnSc2	ochrana
==	==	k?	==
</s>
</p>
<p>
<s>
Strom	strom	k1gInSc1	strom
je	být	k5eAaImIp3nS	být
chráněn	chráněn	k2eAgMnSc1d1	chráněn
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
vzrůst	vzrůst	k1gInSc4	vzrůst
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
krajinná	krajinný	k2eAgFnSc1d1	krajinná
dominanta	dominanta	k1gFnSc1	dominanta
a	a	k8xC	a
součást	součást	k1gFnSc1	součást
kulturní	kulturní	k2eAgFnSc2d1	kulturní
památky	památka	k1gFnSc2	památka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
