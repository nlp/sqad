<s>
Jasan	jasan	k1gInSc1	jasan
u	u	k7c2	u
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
v	v	k7c6	v
Dubči	Dubč	k1gFnSc6	Dubč
je	být	k5eAaImIp3nS	být
památný	památný	k2eAgInSc4d1	památný
strom	strom	k1gInSc4	strom
v	v	k7c6	v
Dubči	Dubč	k1gFnSc6	Dubč
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jasan	jasan	k1gInSc4	jasan
ztepilý	ztepilý	k2eAgInSc4d1	ztepilý
(	(	kIx(	(
<g/>
Fraxinus	Fraxinus	k1gInSc4	Fraxinus
excelsior	excelsiora	k1gFnPc2	excelsiora
<g/>
)	)	kIx)	)
rostoucí	rostoucí	k2eAgFnSc2d1	rostoucí
na	na	k7c6	na
Lipovém	lipový	k2eAgNnSc6d1	lipové
náměstí	náměstí	k1gNnSc6	náměstí
před	před	k7c7	před
branou	brána	k1gFnSc7	brána
kostela	kostel	k1gInSc2	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
<g/>
.	.	kIx.	.
</s>
