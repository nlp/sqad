<p>
<s>
Předehra	předehra	k1gFnSc1	předehra
(	(	kIx(	(
<g/>
ouvertura	ouvertura	k1gFnSc1	ouvertura
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
úvodní	úvodní	k2eAgFnSc4d1	úvodní
instrumentální	instrumentální	k2eAgFnSc4d1	instrumentální
část	část	k1gFnSc4	část
opery	opera	k1gFnSc2	opera
<g/>
,	,	kIx,	,
činohry	činohra	k1gFnSc2	činohra
<g/>
,	,	kIx,	,
hudebního	hudební	k2eAgInSc2d1	hudební
cyklu	cyklus	k1gInSc2	cyklus
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
šlo	jít	k5eAaImAgNnS	jít
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
krátkou	krátký	k2eAgFnSc4d1	krátká
skladbu	skladba	k1gFnSc4	skladba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
jediný	jediný	k2eAgInSc4d1	jediný
účel	účel	k1gInSc4	účel
<g/>
,	,	kIx,	,
ohlásit	ohlásit	k5eAaPmF	ohlásit
začátek	začátek	k1gInSc4	začátek
představení	představení	k1gNnSc2	představení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
obecně	obecně	k6eAd1	obecně
označovala	označovat	k5eAaImAgFnS	označovat
jako	jako	k9	jako
sinfonia	sinfonia	k1gFnSc1	sinfonia
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
první	první	k4xOgFnPc1	první
předehry	předehra	k1gFnPc1	předehra
byly	být	k5eAaImAgFnP	být
zpravidla	zpravidla	k6eAd1	zpravidla
dvoudílné	dvoudílný	k2eAgFnPc1d1	dvoudílná
<g/>
,	,	kIx,	,
při	při	k7c6	při
čemž	což	k3yRnSc6	což
první	první	k4xOgInSc1	první
díl	díl	k1gInSc1	díl
byl	být	k5eAaImAgInS	být
pomalý	pomalý	k2eAgInSc1d1	pomalý
v	v	k7c6	v
sudém	sudý	k2eAgInSc6d1	sudý
taktu	takt	k1gInSc6	takt
a	a	k8xC	a
druhý	druhý	k4xOgInSc4	druhý
díl	díl	k1gInSc4	díl
rychlý	rychlý	k2eAgInSc4d1	rychlý
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
ve	v	k7c6	v
tříčtvrtečním	tříčtvrteční	k2eAgInSc6d1	tříčtvrteční
taktu	takt	k1gInSc6	takt
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
počátku	počátek	k1gInSc2	počátek
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgInP	vyvinout
další	další	k2eAgInPc1d1	další
typy	typ	k1gInPc1	typ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
ouvertura	ouvertura	k1gFnSc1	ouvertura
==	==	k?	==
</s>
</p>
<p>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
ouvertura	ouvertura	k1gFnSc1	ouvertura
byla	být	k5eAaImAgFnS	být
obvyklým	obvyklý	k2eAgInSc7d1	obvyklý
typem	typ	k1gInSc7	typ
barokní	barokní	k2eAgFnSc2d1	barokní
předehry	předehra	k1gFnSc2	předehra
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
třídílná	třídílný	k2eAgFnSc1d1	třídílná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
dosavadní	dosavadní	k2eAgFnSc4d1	dosavadní
předehru	předehra	k1gFnSc4	předehra
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yIgFnSc3	který
byl	být	k5eAaImAgInS	být
připojen	připojit	k5eAaPmNgInS	připojit
ještě	ještě	k9	ještě
třetí	třetí	k4xOgInSc1	třetí
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgInSc1d1	obsahující
návrat	návrat	k1gInSc1	návrat
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
prvního	první	k4xOgInSc2	první
dílu	díl	k1gInSc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
měla	mít	k5eAaImAgFnS	mít
slavnostní	slavnostní	k2eAgFnSc1d1	slavnostní
až	až	k9	až
patetický	patetický	k2eAgInSc4d1	patetický
ráz	ráz	k1gInSc4	ráz
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
část	část	k1gFnSc1	část
měla	mít	k5eAaImAgFnS	mít
často	často	k6eAd1	často
fugovanou	fugovaný	k2eAgFnSc4d1	fugovaný
formu	forma	k1gFnSc4	forma
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1659	[number]	k4	1659
v	v	k7c6	v
baletu	balet	k1gInSc6	balet
Alcidiane	Alcidian	k1gMnSc5	Alcidian
Jeana-Baptista	Jeana-Baptista	k1gMnSc1	Jeana-Baptista
Lully	Lulla	k1gFnSc2	Lulla
uvedeného	uvedený	k2eAgMnSc4d1	uvedený
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Baletní	baletní	k2eAgFnSc1d1	baletní
hudba	hudba	k1gFnSc1	hudba
z	z	k7c2	z
oper	opera	k1gFnPc2	opera
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
sestavovala	sestavovat	k5eAaImAgFnS	sestavovat
do	do	k7c2	do
suit	suita	k1gFnPc2	suita
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
koncertně	koncertně	k6eAd1	koncertně
provozovány	provozovat	k5eAaImNgFnP	provozovat
samostatně	samostatně	k6eAd1	samostatně
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
úvod	úvod	k1gInSc4	úvod
takové	takový	k3xDgFnSc2	takový
suity	suita	k1gFnSc2	suita
se	se	k3xPyFc4	se
hrála	hrát	k5eAaImAgFnS	hrát
operní	operní	k2eAgFnSc1d1	operní
předehra	předehra	k1gFnSc1	předehra
<g/>
.	.	kIx.	.
</s>
<s>
Skladatelé	skladatel	k1gMnPc1	skladatel
pak	pak	k6eAd1	pak
začali	začít	k5eAaPmAgMnP	začít
komponovat	komponovat	k5eAaImF	komponovat
samostatné	samostatný	k2eAgFnSc2d1	samostatná
hudební	hudební	k2eAgFnSc2d1	hudební
suity	suita	k1gFnSc2	suita
bez	bez	k7c2	bez
vztahu	vztah	k1gInSc2	vztah
k	k	k7c3	k
jakékoliv	jakýkoliv	k3yIgFnSc3	jakýkoliv
opeře	opera	k1gFnSc3	opera
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
ty	ten	k3xDgFnPc1	ten
byly	být	k5eAaImAgFnP	být
zahajovány	zahajovat	k5eAaImNgFnP	zahajovat
ouverturou	ouvertura	k1gFnSc7	ouvertura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
stupni	stupeň	k1gInSc6	stupeň
vývoje	vývoj	k1gInSc2	vývoj
pak	pak	k6eAd1	pak
komponovali	komponovat	k5eAaImAgMnP	komponovat
i	i	k9	i
samostatné	samostatný	k2eAgFnPc4d1	samostatná
ouvertury	ouvertura	k1gFnPc4	ouvertura
bez	bez	k7c2	bez
jakéhokoliv	jakýkoliv	k3yIgNnSc2	jakýkoliv
pokračování	pokračování	k1gNnSc2	pokračování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Neapolská	neapolský	k2eAgFnSc1d1	neapolská
ouvertura	ouvertura	k1gFnSc1	ouvertura
==	==	k?	==
</s>
</p>
<p>
<s>
Alessandro	Alessandra	k1gFnSc5	Alessandra
Scarlatti	Scarlatť	k1gFnSc5	Scarlatť
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1696	[number]	k4	1696
zavedl	zavést	k5eAaPmAgMnS	zavést
do	do	k7c2	do
praxe	praxe	k1gFnSc2	praxe
nový	nový	k2eAgInSc4d1	nový
typ	typ	k1gInSc4	typ
předehry	předehra	k1gFnSc2	předehra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
označovala	označovat	k5eAaImAgFnS	označovat
jako	jako	k9	jako
neapolská	neapolský	k2eAgFnSc1d1	neapolská
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
rovněž	rovněž	k9	rovněž
třídilnou	třídilný	k2eAgFnSc4d1	třídilná
formu	forma	k1gFnSc4	forma
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
první	první	k4xOgInSc1	první
díl	díl	k1gInSc1	díl
byl	být	k5eAaImAgInS	být
rychlý	rychlý	k2eAgInSc1d1	rychlý
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc1	druhý
díl	díl	k1gInSc1	díl
volný	volný	k2eAgInSc1d1	volný
<g/>
,	,	kIx,	,
zpěvný	zpěvný	k2eAgInSc1d1	zpěvný
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
koncertním	koncertní	k2eAgInSc7d1	koncertní
nástrojem	nástroj	k1gInSc7	nástroj
a	a	k8xC	a
orchestrálním	orchestrální	k2eAgInSc7d1	orchestrální
doprovodem	doprovod	k1gInSc7	doprovod
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
část	část	k1gFnSc1	část
byla	být	k5eAaImAgFnS	být
opět	opět	k6eAd1	opět
rychlá	rychlý	k2eAgFnSc1d1	rychlá
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
měla	mít	k5eAaImAgFnS	mít
taneční	taneční	k2eAgInSc4d1	taneční
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
sled	sled	k1gInSc1	sled
dílů	díl	k1gInPc2	díl
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
obvyklým	obvyklý	k2eAgFnPc3d1	obvyklá
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
předehry	předehra	k1gFnPc4	předehra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
pro	pro	k7c4	pro
instrumentální	instrumentální	k2eAgFnPc4d1	instrumentální
sonáty	sonáta	k1gFnPc4	sonáta
<g/>
,	,	kIx,	,
koncerty	koncert	k1gInPc4	koncert
a	a	k8xC	a
symfonie	symfonie	k1gFnPc4	symfonie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klasická	klasický	k2eAgFnSc1d1	klasická
ouvertura	ouvertura	k1gFnSc1	ouvertura
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
ouvertura	ouvertura	k1gFnSc1	ouvertura
oprošťuje	oprošťovat	k5eAaImIp3nS	oprošťovat
od	od	k7c2	od
formální	formální	k2eAgFnSc2d1	formální
vázanosti	vázanost	k1gFnSc2	vázanost
a	a	k8xC	a
spíše	spíše	k9	spíše
se	se	k3xPyFc4	se
zajímá	zajímat	k5eAaImIp3nS	zajímat
o	o	k7c4	o
tematickou	tematický	k2eAgFnSc4d1	tematická
vazbu	vazba	k1gFnSc4	vazba
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
operu	opera	k1gFnSc4	opera
<g/>
,	,	kIx,	,
či	či	k8xC	či
alespoň	alespoň	k9	alespoň
na	na	k7c4	na
její	její	k3xOp3gNnSc4	její
první	první	k4xOgNnSc4	první
dějství	dějství	k1gNnSc4	dějství
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
lze	lze	k6eAd1	lze
pozorovat	pozorovat	k5eAaImF	pozorovat
již	již	k9	již
v	v	k7c6	v
operách	opera	k1gFnPc6	opera
Rameauových	Rameauův	k2eAgFnPc6d1	Rameauův
a	a	k8xC	a
Gluckových	Gluckův	k2eAgFnPc6d1	Gluckova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Předehry	předehra	k1gFnSc2	předehra
romantismu	romantismus	k1gInSc2	romantismus
a	a	k8xC	a
současnosti	současnost	k1gFnSc2	současnost
==	==	k?	==
</s>
</p>
<p>
<s>
U	u	k7c2	u
Beethovena	Beethoven	k1gMnSc2	Beethoven
se	se	k3xPyFc4	se
předehra	předehra	k1gFnSc1	předehra
stává	stávat	k5eAaImIp3nS	stávat
samostatným	samostatný	k2eAgNnSc7d1	samostatné
symfonickým	symfonický	k2eAgNnSc7d1	symfonické
dílem	dílo	k1gNnSc7	dílo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
anticipuje	anticipovat	k5eAaBmIp3nS	anticipovat
děj	děj	k1gInSc4	děj
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
romantických	romantický	k2eAgFnPc2d1	romantická
oper	opera	k1gFnPc2	opera
i	i	k8xC	i
předeher	předehra	k1gFnPc2	předehra
komponovaných	komponovaný	k2eAgFnPc2d1	komponovaná
pro	pro	k7c4	pro
činohru	činohra	k1gFnSc4	činohra
<g/>
.	.	kIx.	.
</s>
<s>
Formální	formální	k2eAgFnSc1d1	formální
stavba	stavba	k1gFnSc1	stavba
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
volná	volný	k2eAgFnSc1d1	volná
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
proud	proud	k1gInSc4	proud
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
charakterizovat	charakterizovat	k5eAaBmF	charakterizovat
následující	následující	k2eAgNnSc4d1	následující
představení	představení	k1gNnSc4	představení
<g/>
.	.	kIx.	.
</s>
<s>
Předehra	předehra	k1gFnSc1	předehra
pak	pak	k6eAd1	pak
velice	velice	k6eAd1	velice
často	často	k6eAd1	často
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
děje	děj	k1gInSc2	děj
opery	opera	k1gFnSc2	opera
bez	bez	k7c2	bez
jakéhokoliv	jakýkoliv	k3yIgNnSc2	jakýkoliv
přerušení	přerušení	k1gNnSc2	přerušení
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
Wagnerův	Wagnerův	k2eAgInSc1d1	Wagnerův
Tristan	Tristan	k1gInSc1	Tristan
a	a	k8xC	a
Isolda	Isolda	k1gFnSc1	Isolda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Koncertní	koncertní	k2eAgFnSc2d1	koncertní
předehry	předehra	k1gFnSc2	předehra
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
předeher	předehra	k1gFnPc2	předehra
původně	původně	k6eAd1	původně
vázaných	vázané	k1gNnPc2	vázané
na	na	k7c4	na
nějakou	nějaký	k3yIgFnSc4	nějaký
jinou	jiný	k2eAgFnSc4d1	jiná
hudební	hudební	k2eAgFnSc4d1	hudební
formu	forma	k1gFnSc4	forma
(	(	kIx(	(
<g/>
operu	opera	k1gFnSc4	opera
<g/>
,	,	kIx,	,
oratorium	oratorium	k1gNnSc1	oratorium
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
samostatná	samostatný	k2eAgFnSc1d1	samostatná
hudební	hudební	k2eAgFnSc1d1	hudební
forma	forma	k1gFnSc1	forma
<g/>
.	.	kIx.	.
</s>
<s>
Koncertní	koncertní	k2eAgFnPc1d1	koncertní
předehry	předehra	k1gFnPc1	předehra
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
druhem	druh	k1gInSc7	druh
programní	programní	k2eAgFnSc2d1	programní
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
hudebními	hudební	k2eAgInPc7d1	hudební
prostředky	prostředek	k1gInPc7	prostředek
popisovat	popisovat	k5eAaImF	popisovat
přírodní	přírodní	k2eAgFnPc4d1	přírodní
krásy	krása	k1gFnPc4	krása
<g/>
,	,	kIx,	,
hnutí	hnutí	k1gNnSc4	hnutí
duše	duše	k1gFnSc2	duše
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
i	i	k9	i
nějaký	nějaký	k3yIgInSc4	nějaký
mimohudební	mimohudební	k2eAgInSc4d1	mimohudební
děj	děj	k1gInSc4	děj
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
tedy	tedy	k9	tedy
spíše	spíše	k9	spíše
menší	malý	k2eAgFnPc4d2	menší
symfonické	symfonický	k2eAgFnPc4d1	symfonická
básně	báseň	k1gFnPc4	báseň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Ulrich	Ulrich	k1gMnSc1	Ulrich
Michels	Michelsa	k1gFnPc2	Michelsa
<g/>
:	:	kIx,	:
Encyklopedický	encyklopedický	k2eAgInSc4d1	encyklopedický
atlas	atlas	k1gInSc4	atlas
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
NLN	NLN	kA	NLN
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7106	[number]	k4	80-7106
238-3	[number]	k4	238-3
</s>
</p>
<p>
<s>
Temperley	Temperlea	k1gFnPc1	Temperlea
<g/>
,	,	kIx,	,
Nicholas	Nicholas	k1gMnSc1	Nicholas
<g/>
:	:	kIx,	:
Overture	Overtur	k1gMnSc5	Overtur
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
New	New	k1gFnSc1	New
Grove	Groev	k1gFnSc2	Groev
Dictionary	Dictionara	k1gFnSc2	Dictionara
of	of	k?	of
Music	Music	k1gMnSc1	Music
and	and	k?	and
Musicians	Musicians	k1gInSc1	Musicians
<g/>
,	,	kIx,	,
second	second	k1gInSc1	second
edition	edition	k1gInSc1	edition
<g/>
,	,	kIx,	,
edited	edited	k1gInSc1	edited
by	by	kYmCp3nP	by
Stanley	Stanle	k2eAgFnPc1d1	Stanle
Sadie	Sadie	k1gFnPc1	Sadie
and	and	k?	and
John	John	k1gMnSc1	John
Tyrrell	Tyrrell	k1gMnSc1	Tyrrell
<g/>
,	,	kIx,	,
Macmillan	Macmillan	k1gMnSc1	Macmillan
Publishers	Publishers	k1gInSc1	Publishers
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
2001	[number]	k4	2001
</s>
</p>
<p>
<s>
Riemann	Riemann	k1gInSc1	Riemann
Musik	musika	k1gFnPc2	musika
Lexikon	lexikon	k1gInSc1	lexikon
<g/>
,	,	kIx,	,
Sachteil	Sachteil	k1gInSc1	Sachteil
<g/>
,	,	kIx,	,
Schott	Schott	k1gInSc1	Schott
<g/>
,	,	kIx,	,
Mainz	Mainz	k1gInSc1	Mainz
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
S.	S.	kA	S.
696	[number]	k4	696
<g/>
–	–	k?	–
<g/>
697	[number]	k4	697
</s>
</p>
