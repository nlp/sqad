<s>
Linus	Linus	k1gInSc1
Torvalds	Torvaldsa	k1gFnPc2
</s>
<s>
Linus	Linus	k1gInSc1
Torvalds	Torvalds	k1gInSc1
Linus	Linus	k1gInSc1
Torvalds	Torvalds	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
<g/>
Rodné	rodný	k2eAgInPc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Linus	Linus	k1gInSc1
Benedict	Benedict	k2eAgInSc4d1
Torvalds	Torvalds	k1gInSc4
Narození	narození	k1gNnSc2
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1969	#num#	k4
(	(	kIx(
<g/>
51	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Helsinky	Helsinky	k1gFnPc1
<g/>
,	,	kIx,
Finsko	Finsko	k1gNnSc1
Bydliště	bydliště	k1gNnSc2
</s>
<s>
Dunthorpe	Dunthorpat	k5eAaPmIp3nS
<g/>
,	,	kIx,
Oregon	Oregon	k1gMnSc1
<g/>
,	,	kIx,
USA	USA	kA
Národnost	národnost	k1gFnSc1
</s>
<s>
finští	finský	k2eAgMnPc1d1
Švédové	Švéd	k1gMnPc1
Občanství	občanství	k1gNnSc2
</s>
<s>
americké	americký	k2eAgFnPc1d1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Helsinská	helsinský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
programátor	programátor	k1gMnSc1
Zaměstnavatel	zaměstnavatel	k1gMnSc1
</s>
<s>
Linux	linux	k1gInSc1
Foundation	Foundation	k1gInSc1
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Tove	Tove	k1gInSc1
Torvalds	Torvalds	k1gInSc1
(	(	kIx(
<g/>
narozená	narozený	k2eAgFnSc1d1
Monni	Monň	k1gMnPc7
<g/>
)	)	kIx)
Děti	dítě	k1gFnPc1
</s>
<s>
3	#num#	k4
Rodiče	rodič	k1gMnPc1
</s>
<s>
Nils	Nils	k1gInSc1
Torvalds	Torvalds	k1gInSc1
(	(	kIx(
<g/>
otec	otec	k1gMnSc1
<g/>
)	)	kIx)
<g/>
Anna	Anna	k1gFnSc1
Torvalds	Torvalds	k1gInSc1
(	(	kIx(
<g/>
matka	matka	k1gFnSc1
<g/>
)	)	kIx)
Web	web	k1gInSc1
</s>
<s>
http://torvalds-family.blogspot.com/	http://torvalds-family.blogspot.com/	k?
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Linus	Linus	k1gInSc1
Torvalds	Torvalds	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
</s>
<s>
Linus	k1gMnSc1	k1gInSc1
Benedict	k1gMnSc1	k2eAgInSc4d1
Torvalds	k1gMnSc1	k1gInSc4
(	(	kIx(
<g/>
*	*	kIx~
28	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1969	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
finský	finský	k2eAgMnSc1d1
programátor	programátor	k1gMnSc1
<g/>
,	,	kIx,
známý	známý	k2eAgInSc1d1
především	především	k6eAd1
zahájením	zahájení	k1gNnSc7
vývoje	vývoj	k1gInSc2
jádra	jádro	k1gNnSc2
operačního	operační	k2eAgInSc2d1
systému	systém	k1gInSc2
Linux	Linux	kA
a	a	k8xC
pozdější	pozdní	k2eAgFnSc7d2
koordinací	koordinace	k1gFnSc7
projektu	projekt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
vývoji	vývoj	k1gInSc6
se	se	k3xPyFc4
Linus	Linus	k1gMnSc1
inspiroval	inspirovat	k5eAaBmAgMnS
knihou	kniha	k1gFnSc7
Operating	Operating	k1gInSc4
Systems	Systems	k1gInSc1
<g/>
:	:	kIx,
Design	design	k1gInSc1
and	and	k?
Implementation	Implementation	k1gInSc1
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yRgFnSc6,k3yQgFnSc6,k3yIgFnSc6
Andrew	Andrew	k1gFnSc6
Tanenbaum	Tanenbaum	k1gInSc1
popisuje	popisovat	k5eAaImIp3nS
základní	základní	k2eAgInPc4d1
principy	princip	k1gInPc4
operačního	operační	k2eAgInSc2d1
systému	systém	k1gInSc2
Unix	Unix	k1gInSc1
a	a	k8xC
detailně	detailně	k6eAd1
rozebírá	rozebírat	k5eAaImIp3nS
zdrojový	zdrojový	k2eAgInSc4d1
kód	kód	k1gInSc4
zjednodušeného	zjednodušený	k2eAgInSc2d1
klonu	klon	k1gInSc2
Unixu	Unix	k1gInSc2
nazvaného	nazvaný	k2eAgInSc2d1
MINIX	MINIX	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Linusovým	Linusový	k2eAgInSc7d1
cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
vytvořit	vytvořit	k5eAaPmF
operační	operační	k2eAgInSc4d1
systém	systém	k1gInSc4
unixového	unixový	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
by	by	kYmCp3nS
byl	být	k5eAaImAgInS
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
MINIXu	MINIXus	k1gInSc2
volně	volně	k6eAd1
šiřitelný	šiřitelný	k2eAgInSc1d1
a	a	k8xC
využíval	využívat	k5eAaImAgInS,k5eAaPmAgInS
všech	všecek	k3xTgFnPc2
možností	možnost	k1gFnPc2
procesoru	procesor	k1gInSc2
Intel	Intel	kA
386	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Biografie	biografie	k1gFnSc1
</s>
<s>
Linus	Linus	k1gInSc1
Benedict	Benedict	k2eAgInSc4d1
Torvalds	Torvalds	k1gInSc4
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
v	v	k7c6
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
Finska	Finsko	k1gNnSc2
<g/>
,	,	kIx,
Helsinkách	Helsinky	k1gFnPc6
<g/>
,	,	kIx,
rodičům	rodič	k1gMnPc3
Anně	Anna	k1gFnSc3
a	a	k8xC
Nilsovi	Nils	k1gMnSc3
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
po	po	k7c6
otci	otec	k1gMnSc6
vnukem	vnuk	k1gMnSc7
básníka	básník	k1gMnSc2
<g/>
,	,	kIx,
literárního	literární	k2eAgMnSc4d1
kritika	kritik	k1gMnSc4
a	a	k8xC
novináře	novinář	k1gMnSc4
Oleho	Ole	k1gMnSc4
Torvaldse	Torvalds	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc7
rodina	rodina	k1gFnSc1
pochází	pocházet	k5eAaImIp3nS
ze	z	k7c2
švédsky	švédsky	k6eAd1
mluvící	mluvící	k2eAgFnSc2d1
menšiny	menšina	k1gFnSc2
(	(	kIx(
<g/>
v	v	k7c6
současnosti	současnost	k1gFnSc6
zhruba	zhruba	k6eAd1
5	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
finské	finský	k2eAgFnSc2d1
populace	populace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
pojmenován	pojmenovat	k5eAaPmNgMnS
po	po	k7c6
Linusi	Linuse	k1gFnSc6
Paulingovi	Paulingův	k2eAgMnPc1d1
<g/>
,	,	kIx,
americkém	americký	k2eAgInSc6d1
držiteli	držitel	k1gMnSc3
Nobelovy	Nobelův	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
za	za	k7c4
chemii	chemie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
jeho	jeho	k3xOp3gInSc7
rodiče	rodič	k1gMnPc1
patřili	patřit	k5eAaImAgMnP
v	v	k7c6
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
k	k	k7c3
radikálním	radikální	k2eAgMnPc3d1
studentům	student	k1gMnPc3
Helsinské	helsinský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
(	(	kIx(
<g/>
Helsingin	Helsingin	k1gInSc1
yliopisto	yliopista	k1gMnSc5
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc1
otec	otec	k1gMnSc1
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
poslanec	poslanec	k1gMnSc1
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
za	za	k7c4
Švédskou	švédský	k2eAgFnSc4d1
lidovou	lidový	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
komunistou	komunista	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
strávil	strávit	k5eAaPmAgMnS
v	v	k7c6
polovině	polovina	k1gFnSc6
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
rok	rok	k1gInSc4
studiem	studio	k1gNnSc7
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
Linuse	Linus	k1gInSc6
měl	mít	k5eAaImAgMnS
velký	velký	k2eAgInSc4d1
vliv	vliv	k1gInSc4
dědeček	dědeček	k1gMnSc1
Leo	Leo	k1gMnSc1
Törnqvist	Törnqvist	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
přednášel	přednášet	k5eAaImAgMnS
statistiku	statistika	k1gFnSc4
na	na	k7c6
Helsinské	helsinský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
a	a	k8xC
začátkem	začátkem	k7c2
osmdesátých	osmdesátý	k4xOgNnPc2
let	léto	k1gNnPc2
si	se	k3xPyFc3
koupil	koupit	k5eAaPmAgInS
osmibitový	osmibitový	k2eAgInSc4d1
počítač	počítač	k1gInSc4
Commodore	Commodor	k1gInSc5
VIC-	VIC-	k1gFnPc7
<g/>
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Linus	Linus	k1gMnSc1
se	se	k3xPyFc4
naučil	naučit	k5eAaPmAgMnS
programovat	programovat	k5eAaImF
v	v	k7c6
jazyce	jazyk	k1gInSc6
BASIC	Basic	kA
a	a	k8xC
psal	psát	k5eAaImAgInS
i	i	k9
strojový	strojový	k2eAgInSc1d1
kód	kód	k1gInSc1
pro	pro	k7c4
procesor	procesor	k1gInSc4
6502	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
polovině	polovina	k1gFnSc6
osmdesátých	osmdesátý	k4xOgNnPc2
let	léto	k1gNnPc2
závistivě	závistivě	k6eAd1
pokukoval	pokukovat	k5eAaImAgInS
po	po	k7c6
Commodorech	Commodor	k1gInPc6
64	#num#	k4
svých	svůj	k3xOyFgMnPc2
kamarádů	kamarád	k1gMnPc2
a	a	k8xC
nakonec	nakonec	k6eAd1
si	se	k3xPyFc3
v	v	k7c6
roce	rok	k1gInSc6
1987	#num#	k4
koupil	koupit	k5eAaPmAgInS
počítač	počítač	k1gInSc1
<g/>
,	,	kIx,
Sinclair	Sinclair	k1gInSc1
QL	QL	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
QL	QL	kA
naprogramoval	naprogramovat	k5eAaPmAgMnS
assembler	assembler	k1gInSc4
<g/>
,	,	kIx,
textový	textový	k2eAgInSc4d1
editor	editor	k1gInSc4
a	a	k8xC
ovladač	ovladač	k1gInSc4
pro	pro	k7c4
disketovou	disketový	k2eAgFnSc4d1
mechaniku	mechanika	k1gFnSc4
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
pár	pár	k4xCyI
počítačových	počítačový	k2eAgFnPc2d1
her	hra	k1gFnPc2
(	(	kIx(
<g/>
např.	např.	kA
klon	klon	k1gInSc4
hry	hra	k1gFnSc2
Pac	pac	k1gFnPc2
Man	Man	k1gMnSc1
<g/>
,	,	kIx,
nazvaný	nazvaný	k2eAgMnSc1d1
Cool	Cool	k1gMnSc1
Man	Man	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1988	#num#	k4
Linus	Linus	k1gInSc1
začal	začít	k5eAaPmAgInS
studium	studium	k1gNnSc4
na	na	k7c6
Helsinské	helsinský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1991	#num#	k4
si	se	k3xPyFc3
zakoupil	zakoupit	k5eAaPmAgMnS
na	na	k7c4
splátky	splátka	k1gFnPc4
PC	PC	kA
AT	AT	kA
s	s	k7c7
33	#num#	k4
<g/>
MHz	Mhz	kA
procesorem	procesor	k1gInSc7
Intel	Intel	kA
80386	#num#	k4
a	a	k8xC
4MB	4MB	k4
paměti	paměť	k1gFnSc2
a	a	k8xC
začal	začít	k5eAaPmAgInS
pracovat	pracovat	k5eAaImF
na	na	k7c6
vývoji	vývoj	k1gInSc6
Linuxu	linux	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
graduoval	graduovat	k5eAaBmAgInS
s	s	k7c7
magisterským	magisterský	k2eAgInSc7d1
titulem	titul	k1gInSc7
z	z	k7c2
Informatiky	informatika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svou	svůj	k3xOyFgFnSc4
diplomovou	diplomový	k2eAgFnSc4d1
práci	práce	k1gFnSc4
<g/>
,	,	kIx,
nazvanou	nazvaný	k2eAgFnSc4d1
Linux	Linux	kA
<g/>
:	:	kIx,
A	A	kA
Portable	portable	k1gInSc1
Operating	Operating	k1gInSc1
System	Systo	k1gNnSc7
(	(	kIx(
<g/>
„	„	k?
<g/>
Linux	linux	k1gInSc1
<g/>
:	:	kIx,
přenosný	přenosný	k2eAgInSc1d1
operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
napsal	napsat	k5eAaBmAgInS,k5eAaPmAgInS
v	v	k7c6
Linuxu	linux	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
ženatý	ženatý	k2eAgMnSc1d1
s	s	k7c7
Tove	Tove	k1gFnSc7
Torvaldsovou	Torvaldsův	k2eAgFnSc7d1
<g/>
,	,	kIx,
šestinásobnou	šestinásobný	k2eAgFnSc7d1
mistryní	mistryně	k1gFnSc7
Finska	Finsko	k1gNnSc2
v	v	k7c6
karate	karate	k1gNnSc6
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
poprvé	poprvé	k6eAd1
potkal	potkat	k5eAaPmAgInS
koncem	koncem	k7c2
roku	rok	k1gInSc2
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mají	mít	k5eAaImIp3nP
spolu	spolu	k6eAd1
tři	tři	k4xCgFnPc4
dcery	dcera	k1gFnPc4
–	–	k?
Patricii	Patricie	k1gFnSc3
Mirandu	Mirand	k1gInSc2
(	(	kIx(
<g/>
*	*	kIx~
5	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1996	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Danielu	Daniela	k1gFnSc4
Yolandu	Yoland	k1gInSc2
(	(	kIx(
<g/>
*	*	kIx~
16	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1998	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Celestu	Celesta	k1gFnSc4
Amandu	Amand	k1gInSc2
(	(	kIx(
<g/>
*	*	kIx~
20	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2000	#num#	k4
<g/>
)	)	kIx)
–	–	k?
mají	mít	k5eAaImIp3nP
kočku	kočka	k1gFnSc4
jménem	jméno	k1gNnSc7
Randi	randit	k5eAaImRp2nS
(	(	kIx(
<g/>
zkrácené	zkrácený	k2eAgInPc1d1
Mithrandir	Mithrandir	k1gInSc1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
elfské	elfský	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
pro	pro	k7c4
Gandalfa	Gandalf	k1gMnSc4
<g/>
,	,	kIx,
čaroděje	čaroděj	k1gMnSc4
z	z	k7c2
trilogie	trilogie	k1gFnSc2
Pán	pán	k1gMnSc1
prstenů	prsten	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Linus	Linus	k1gMnSc1
se	se	k3xPyFc4
přestěhoval	přestěhovat	k5eAaPmAgMnS
do	do	k7c2
San	San	k1gFnSc2
José	Josý	k2eAgFnSc2d1
v	v	k7c6
Kalifornii	Kalifornie	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
dlouhou	dlouhý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
s	s	k7c7
rodinou	rodina	k1gFnSc7
žil	žíla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červnu	červen	k1gInSc6
roku	rok	k1gInSc2
2004	#num#	k4
se	se	k3xPyFc4
přestěhovali	přestěhovat	k5eAaPmAgMnP
do	do	k7c2
Lake	Lak	k1gInSc2
Oswego	Oswego	k6eAd1
v	v	k7c6
Oregonu	Oregon	k1gInSc6
a	a	k8xC
nakonec	nakonec	k6eAd1
do	do	k7c2
Portlandu	Portland	k1gInSc2
(	(	kIx(
<g/>
taktéž	taktéž	k?
v	v	k7c6
Oregonu	Oregon	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
to	ten	k3xDgNnSc4
měli	mít	k5eAaImAgMnP
blíž	blízce	k6eAd2
k	k	k7c3
místu	místo	k1gNnSc3
<g/>
,	,	kIx,
kde	kde	k6eAd1
Linus	Linus	k1gInSc1
pracuje	pracovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
února	únor	k1gInSc2
1997	#num#	k4
do	do	k7c2
června	červen	k1gInSc2
2003	#num#	k4
pracoval	pracovat	k5eAaImAgMnS
pro	pro	k7c4
Transmeta	Transmet	k1gMnSc4
Corporation	Corporation	k1gInSc4
a	a	k8xC
nyní	nyní	k6eAd1
pracuje	pracovat	k5eAaImIp3nS
pro	pro	k7c4
Open	Open	k1gInSc4
Source	Sourec	k1gInSc2
Development	Development	k1gInSc1
Labs	Labs	k1gInSc1
<g/>
,	,	kIx,
softwarové	softwarový	k2eAgNnSc1d1
konsorcium	konsorcium	k1gNnSc1
v	v	k7c6
Beavertonu	Beaverton	k1gInSc6
(	(	kIx(
<g/>
Oregon	Oregon	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Oblíbený	oblíbený	k2eAgInSc1d1
maskot	maskot	k1gInSc1
Linuxu	linux	k1gInSc2
</s>
<s>
Jeho	jeho	k3xOp3gInSc7
osobním	osobní	k2eAgInSc7d1
maskotem	maskot	k1gInSc7
je	být	k5eAaImIp3nS
buclatý	buclatý	k2eAgMnSc1d1
tučňák	tučňák	k1gMnSc1
přezdívaný	přezdívaný	k2eAgMnSc1d1
Tux	Tux	k1gMnSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
byl	být	k5eAaImAgMnS
komunitou	komunita	k1gFnSc7
adoptován	adoptován	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
maskot	maskot	k1gInSc1
Linuxu	linux	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Linusův	Linusův	k2eAgInSc1d1
zákon	zákon	k1gInSc1
(	(	kIx(
<g/>
Linus	Linus	k1gInSc1
<g/>
'	'	kIx"
law	law	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pravidlo	pravidlo	k1gNnSc1
inspirované	inspirovaný	k2eAgNnSc1d1
Torvaldsem	Torvalds	k1gInSc7
<g/>
,	,	kIx,
ale	ale	k8xC
formulované	formulovaný	k2eAgFnPc4d1
Ericem	Eriec	k1gInSc7
S.	S.	kA
Raymondem	Raymond	k1gMnSc7
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
článku	článek	k1gInSc6
The	The	k1gMnSc1
Cathedral	Cathedral	k1gMnSc1
and	and	k?
the	the	k?
Bazaar	Bazaar	k1gInSc1
<g/>
,	,	kIx,
zní	znět	k5eAaImIp3nS
<g/>
:	:	kIx,
„	„	k?
<g/>
Je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
dost	dost	k6eAd1
očí	oko	k1gNnPc2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
všechny	všechen	k3xTgFnPc1
chyby	chyba	k1gFnPc1
malé	malý	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
“	“	k?
(	(	kIx(
<g/>
Given	Given	k2eAgInSc1d1
enough	enough	k1gInSc1
eyeballs	eyeballsa	k1gFnPc2
<g/>
,	,	kIx,
all	all	k?
bugs	bugs	k1gInSc1
are	ar	k1gInSc5
shallow	shallow	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Velká	velký	k2eAgFnSc1d1
chyba	chyba	k1gFnSc1
se	se	k3xPyFc4
velmi	velmi	k6eAd1
těžko	těžko	k6eAd1
hledá	hledat	k5eAaImIp3nS
a	a	k8xC
hledá	hledat	k5eAaImIp3nS
<g/>
-li	-li	k?
ji	on	k3xPp3gFnSc4
hodně	hodně	k6eAd1
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
naděje	naděje	k1gFnSc1
(	(	kIx(
<g/>
a	a	k8xC
taky	taky	k6eAd1
už	už	k9
zkušenost	zkušenost	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
že	že	k8xS
žádná	žádný	k3yNgFnSc1
chyba	chyba	k1gFnSc1
nebude	být	k5eNaImBp3nS
velká	velký	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgMnPc1
muži	muž	k1gMnPc1
sdílejí	sdílet	k5eAaImIp3nP
filozofii	filozofie	k1gFnSc4
softwaru	software	k1gInSc2
s	s	k7c7
otevřeným	otevřený	k2eAgInSc7d1
zdrojovým	zdrojový	k2eAgInSc7d1
kódem	kód	k1gInSc7
(	(	kIx(
<g/>
open	open	k1gInSc1
source	sourec	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
byla	být	k5eAaImAgFnS
z	z	k7c2
části	část	k1gFnSc2
(	(	kIx(
<g/>
a	a	k8xC
implicitně	implicitně	k6eAd1
<g/>
)	)	kIx)
založena	založen	k2eAgFnSc1d1
na	na	k7c6
této	tento	k3xDgFnSc6
víře	víra	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Oproti	oproti	k7c3
mnoha	mnoho	k4c3
open	open	k1gMnSc1
source	source	k1gMnSc2
fanatikům	fanatik	k1gMnPc3
se	se	k3xPyFc4
drží	držet	k5eAaImIp3nS
Torvalds	Torvalds	k1gInSc1
zpátky	zpátky	k6eAd1
a	a	k8xC
obvykle	obvykle	k6eAd1
se	se	k3xPyFc4
zdržuje	zdržovat	k5eAaImIp3nS
komentářů	komentář	k1gInPc2
ke	k	k7c3
konkurenčním	konkurenční	k2eAgInPc3d1
produktům	produkt	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bývá	bývat	k5eAaImIp3nS
kritizován	kritizovat	k5eAaImNgMnS
za	za	k7c4
svůj	svůj	k3xOyFgInSc4
neutrální	neutrální	k2eAgInSc4d1
postoj	postoj	k1gInSc4
vůči	vůči	k7c3
projektu	projekt	k1gInSc3
GNU	gnu	k1gNnSc2
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
že	že	k8xS
pracoval	pracovat	k5eAaImAgMnS
na	na	k7c6
proprietárním	proprietární	k2eAgInSc6d1
softwaru	software	k1gInSc6
pro	pro	k7c4
společnost	společnost	k1gFnSc4
Transmeta	Transmeto	k1gNnSc2
a	a	k8xC
že	že	k8xS
používal	používat	k5eAaImAgMnS
a	a	k8xC
hájil	hájit	k5eAaImAgMnS
proprietární	proprietární	k2eAgInSc4d1
BitKeeper	BitKeeper	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navzdory	navzdory	k6eAd1
svému	svůj	k3xOyFgNnSc3
neutrálnímu	neutrální	k2eAgNnSc3d1
založení	založení	k1gNnSc3
však	však	k9
Torvalds	Torvalds	k1gInSc4
vehementně	vehementně	k6eAd1
hájil	hájit	k5eAaImAgMnS
open	open	k1gMnSc1
source	source	k1gMnSc1
a	a	k8xC
svobodný	svobodný	k2eAgInSc1d1
software	software	k1gInSc1
(	(	kIx(
<g/>
free	free	k1gInSc1
software	software	k1gInSc1
<g/>
)	)	kIx)
před	před	k7c7
pomluvami	pomluva	k1gFnPc7
a	a	k8xC
neupřímným	upřímný	k2eNgNnSc7d1
chováním	chování	k1gNnSc7
ze	z	k7c2
strany	strana	k1gFnSc2
prodejců	prodejce	k1gMnPc2
proprietárního	proprietární	k2eAgInSc2d1
softwaru	software	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Spojení	spojení	k1gNnSc1
Linus	Linus	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Linux	Linux	kA
</s>
<s>
Původně	původně	k6eAd1
Linus	Linus	k1gInSc1
používal	používat	k5eAaImAgInS
operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
Minix	Minix	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
později	pozdě	k6eAd2
nahradil	nahradit	k5eAaPmAgMnS
svým	svůj	k3xOyFgInSc7
vlastním	vlastní	k2eAgNnSc7d1
OS	OS	kA
<g/>
,	,	kIx,
nazvaným	nazvaný	k2eAgInSc7d1
Linux	Linux	kA
(	(	kIx(
<g/>
Linusův	Linusův	k2eAgInSc1d1
Minix	Minix	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jméno	jméno	k1gNnSc4
„	„	k?
<g/>
Linux	linux	k1gInSc1
<g/>
“	“	k?
považoval	považovat	k5eAaImAgInS
za	za	k7c4
příliš	příliš	k6eAd1
egoistické	egoistický	k2eAgFnPc4d1
a	a	k8xC
měl	mít	k5eAaImAgMnS
v	v	k7c6
plánu	plán	k1gInSc6
je	být	k5eAaImIp3nS
změnit	změnit	k5eAaPmF
na	na	k7c4
Freax	Freax	k1gInSc4
(	(	kIx(
<g/>
kombinace	kombinace	k1gFnSc1
slov	slovo	k1gNnPc2
„	„	k?
<g/>
free	fre	k1gFnSc2
<g/>
“	“	k?
(	(	kIx(
<g/>
svobodný	svobodný	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
„	„	k?
<g/>
freak	freak	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
výstřední	výstřední	k2eAgInSc4d1
<g/>
,	,	kIx,
mimořádný	mimořádný	k2eAgInSc4d1
<g/>
)	)	kIx)
a	a	k8xC
písmene	písmeno	k1gNnSc2
X	X	kA
jako	jako	k8xC,k8xS
označení	označení	k1gNnSc1
systému	systém	k1gInSc2
unixového	unixový	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Než	než	k8xS
však	však	k9
jméno	jméno	k1gNnSc4
změnil	změnit	k5eAaPmAgMnS
<g/>
,	,	kIx,
přemluvil	přemluvit	k5eAaPmAgMnS
jej	on	k3xPp3gMnSc4
kamarád	kamarád	k1gMnSc1
Ari	Ari	k1gMnSc1
Lemmke	Lemmke	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
jej	on	k3xPp3gMnSc4
nahrál	nahrát	k5eAaBmAgMnS,k5eAaPmAgMnS
na	na	k7c4
síť	síť	k1gFnSc4
a	a	k8xC
umožnil	umožnit	k5eAaPmAgInS
tak	tak	k9
jeho	jeho	k3xOp3gNnSc1
snadné	snadný	k2eAgNnSc1d1
stažení	stažení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ari	Ari	k1gMnSc1
<g/>
,	,	kIx,
nespokojený	spokojený	k2eNgMnSc1d1
s	s	k7c7
názvem	název	k1gInSc7
Freax	Freax	k1gInSc1
<g/>
,	,	kIx,
pojmenoval	pojmenovat	k5eAaPmAgMnS
Linusův	Linusův	k2eAgInSc4d1
adresář	adresář	k1gInSc4
na	na	k7c6
FTP	FTP	kA
serveru	server	k1gInSc2
Linux	Linux	kA
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
srpnu	srpen	k1gInSc6
roku	rok	k1gInSc2
1991	#num#	k4
publikoval	publikovat	k5eAaBmAgInS
Linus	Linus	k1gInSc1
svůj	svůj	k3xOyFgInSc4
výtvor	výtvor	k1gInSc4
v	v	k7c6
diskusní	diskusní	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
comp	comp	k1gMnSc1
<g/>
.	.	kIx.
<g/>
os	osa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
minix	minix	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Současné	současný	k2eAgNnSc1d1
Linuxové	linuxový	k2eAgNnSc1d1
jádro	jádro	k1gNnSc1
obsahuje	obsahovat	k5eAaImIp3nS
jen	jen	k9
něco	něco	k3yInSc4
okolo	okolo	k7c2
2	#num#	k4
%	%	kIx~
kódu	kód	k1gInSc2
napsaného	napsaný	k2eAgInSc2d1
samotným	samotný	k2eAgInSc7d1
Linusem	Linus	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navzdory	navzdory	k6eAd1
relativní	relativní	k2eAgFnSc6d1
míře	míra	k1gFnSc6
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
přínosu	přínos	k1gInSc2
zůstává	zůstávat	k5eAaImIp3nS
Linus	Linus	k1gMnSc1
Torvalds	Torvalds	k1gInSc4
hlavní	hlavní	k2eAgInSc4d1
autoritou	autorita	k1gFnSc7
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yRgInSc4,k3yQgInSc4
kód	kód	k1gInSc4
se	se	k3xPyFc4
do	do	k7c2
jádra	jádro	k1gNnSc2
dostane	dostat	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Správu	správa	k1gFnSc4
verzí	verze	k1gFnSc7
jádra	jádro	k1gNnSc2
Linuxu	linux	k1gInSc2
provádí	provádět	k5eAaImIp3nS
pomocí	pomocí	k7c2
Gitu	Gita	k1gFnSc4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
je	být	k5eAaImIp3nS
také	také	k9
původním	původní	k2eAgMnSc7d1
autorem	autor	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Torvalds	Torvalds	k1gInSc1
má	mít	k5eAaImIp3nS
ve	v	k7c6
zvyku	zvyk	k1gInSc6
nezúčastňovat	zúčastňovat	k5eNaImF
se	se	k3xPyFc4
debat	debata	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
netýkají	týkat	k5eNaImIp3nP
jádra	jádro	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kombinací	kombinace	k1gFnPc2
linuxového	linuxový	k2eAgNnSc2d1
jádra	jádro	k1gNnSc2
a	a	k8xC
softwaru	software	k1gInSc2
vyvíjeného	vyvíjený	k2eAgInSc2d1
mnohými	mnohý	k2eAgFnPc7d1
dalšími	další	k2eAgFnPc7d1
(	(	kIx(
<g/>
převážně	převážně	k6eAd1
pod	pod	k7c7
projektem	projekt	k1gInSc7
GNU	gnu	k1gNnSc2
<g/>
)	)	kIx)
vzniká	vznikat	k5eAaImIp3nS
takzvaná	takzvaný	k2eAgFnSc1d1
Linuxová	linuxový	k2eAgFnSc1d1
distribuce	distribuce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
lidí	člověk	k1gMnPc2
tuto	tento	k3xDgFnSc4
kombinaci	kombinace	k1gFnSc4
označuje	označovat	k5eAaImIp3nS
prostě	prostě	k9
Linux	linux	k1gInSc1
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
někteří	některý	k3yIgMnPc1
<g/>
,	,	kIx,
včetně	včetně	k7c2
Richarda	Richard	k1gMnSc2
Stallmana	Stallman	k1gMnSc2
<g/>
,	,	kIx,
o	o	k7c6
ní	on	k3xPp3gFnSc6
mluví	mluvit	k5eAaImIp3nS
jako	jako	k9
o	o	k7c6
systému	systém	k1gInSc6
„	„	k?
<g/>
GNU	gnu	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Linux	Linux	kA
<g/>
.	.	kIx.
<g/>
“	“	k?
Torvalds	Torvalds	k1gInSc1
zastává	zastávat	k5eAaImIp3nS
názor	názor	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
název	název	k1gInSc1
„	„	k?
<g/>
GNU	gnu	k1gNnSc1
<g/>
/	/	kIx~
<g/>
Linux	linux	k1gInSc1
<g/>
“	“	k?
je	být	k5eAaImIp3nS
oprávněný	oprávněný	k2eAgInSc1d1
pouze	pouze	k6eAd1
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
vytvoříte	vytvořit	k5eAaPmIp2nP
distribuci	distribuce	k1gFnSc4
založenou	založený	k2eAgFnSc4d1
na	na	k7c4
GNU	gnu	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Torvalds	Torvalds	k6eAd1
je	být	k5eAaImIp3nS
vlastníkem	vlastník	k1gMnSc7
obchodní	obchodní	k2eAgFnSc2d1
značky	značka	k1gFnSc2
Linux	Linux	kA
a	a	k8xC
monitoruje	monitorovat	k5eAaImIp3nS
její	její	k3xOp3gNnSc1
použití	použití	k1gNnSc1
(	(	kIx(
<g/>
zneužití	zneužití	k1gNnSc1
<g/>
)	)	kIx)
převážně	převážně	k6eAd1
prostřednictvím	prostřednictvím	k7c2
nekomerční	komerční	k2eNgFnSc2d1
organizace	organizace	k1gFnSc2
Linux	Linux	kA
International	International	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Početná	početný	k2eAgFnSc1d1
a	a	k8xC
vášnivá	vášnivý	k2eAgFnSc1d1
uživatelská	uživatelský	k2eAgFnSc1d1
základna	základna	k1gFnSc1
Linuxu	linux	k1gInSc2
však	však	k9
zneužití	zneužití	k1gNnSc4
obchodní	obchodní	k2eAgFnSc2d1
značky	značka	k1gFnSc2
značně	značně	k6eAd1
ztěžuje	ztěžovat	k5eAaImIp3nS
<g/>
,	,	kIx,
neboť	neboť	k8xC
je	být	k5eAaImIp3nS
rychle	rychle	k6eAd1
odhaleno	odhalen	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Linus	Linus	k1gInSc1
Torvalds	Torvalds	k1gInSc1
na	na	k7c6
Linuxworldu	Linuxworld	k1gInSc6
2000	#num#	k4
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
</s>
<s>
Mnoho	mnoho	k4c1
fanoušků	fanoušek	k1gMnPc2
Linuxu	linux	k1gInSc2
má	mít	k5eAaImIp3nS
tendence	tendence	k1gFnSc1
uctívat	uctívat	k5eAaImF
Linuse	Linuse	k1gFnPc4
jako	jako	k8xS,k8xC
určitý	určitý	k2eAgInSc4d1
druh	druh	k1gInSc4
boha	bůh	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
své	svůj	k3xOyFgFnSc6
knize	kniha	k1gFnSc6
Just	just	k6eAd1
for	forum	k1gNnPc2
Fun	Fun	k1gFnSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
Jen	jen	k9
tak	tak	k6eAd1
pro	pro	k7c4
legraci	legrace	k1gFnSc4
<g/>
“	“	k?
<g/>
)	)	kIx)
si	se	k3xPyFc3
stěžuje	stěžovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
to	ten	k3xDgNnSc1
obtěžuje	obtěžovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
anketě	anketa	k1gFnSc6
Person	persona	k1gFnPc2
of	of	k?
the	the	k?
Century	Centura	k1gFnSc2
(	(	kIx(
<g/>
Osobnost	osobnost	k1gFnSc1
století	století	k1gNnSc2
<g/>
)	)	kIx)
magazínu	magazín	k1gInSc2
Time	Time	k1gNnSc2
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
umístil	umístit	k5eAaPmAgMnS
na	na	k7c6
17	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
se	se	k3xPyFc4
podělil	podělit	k5eAaPmAgMnS
o	o	k7c4
ocenění	ocenění	k1gNnSc4
Takeda	Takeda	k1gMnSc1
Award	Award	k1gMnSc1
za	za	k7c4
společensky	společensky	k6eAd1
<g/>
/	/	kIx~
<g/>
ekonomicky	ekonomicky	k6eAd1
prospěšného	prospěšný	k2eAgMnSc2d1
člověka	člověk	k1gMnSc2
s	s	k7c7
Richardem	Richard	k1gMnSc7
Stallmanem	Stallman	k1gMnSc7
a	a	k8xC
Kenem	Ken	k1gInSc7
Sakamurou	Sakamura	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
byl	být	k5eAaImAgMnS
v	v	k7c6
magazínu	magazín	k1gInSc6
Time	Tim	k1gInSc2
označen	označit	k5eAaPmNgInS
za	za	k7c4
jednoho	jeden	k4xCgMnSc4
z	z	k7c2
nejvlivnějších	vlivný	k2eAgMnPc2d3
lidí	člověk	k1gMnPc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
anketě	anketa	k1gFnSc6
o	o	k7c4
100	#num#	k4
největších	veliký	k2eAgMnPc2d3
Finů	Fin	k1gMnPc2
všech	všecek	k3xTgFnPc2
dob	doba	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
konala	konat	k5eAaImAgFnS
v	v	k7c6
létě	léto	k1gNnSc6
2004	#num#	k4
<g/>
,	,	kIx,
se	se	k3xPyFc4
Torvalds	Torvalds	k1gInSc1
umístil	umístit	k5eAaPmAgInS
na	na	k7c4
16	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
průzkumu	průzkum	k1gInSc6
magazínu	magazín	k1gInSc2
BusinessWeek	BusinessWeek	k6eAd1
objevil	objevit	k5eAaPmAgInS
mezi	mezi	k7c7
nejlepšími	dobrý	k2eAgMnPc7d3
managery	manager	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
IEEE	IEEE	kA
Computer	computer	k1gInSc4
Society	societa	k1gFnSc2
udělila	udělit	k5eAaPmAgFnS
Linusu	Linus	k1gInSc6
Torvaldsovi	Torvaldsův	k2eAgMnPc1d1
ocenění	oceněný	k2eAgMnPc1d1
Computer	computer	k1gInSc1
Pioneer	Pioneer	kA
Award	Award	k1gInSc1
za	za	k7c2
stvoření	stvoření	k1gNnSc2
Linuxu	linux	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ocenění	ocenění	k1gNnSc1
a	a	k8xC
úspěchy	úspěch	k1gInPc1
</s>
<s>
RokOcenění	RokOcenění	k1gNnSc1
</s>
<s>
2014	#num#	k4
</s>
<s>
IEEE	IEEE	kA
Computer	computer	k1gInSc1
Pioneer	Pioneer	kA
Award	Award	k1gInSc1
</s>
<s>
2012	#num#	k4
</s>
<s>
Internet	Internet	k1gInSc1
Hall	Hall	k1gInSc1
of	of	k?
Fame	Fame	k1gInSc1
</s>
<s>
2012	#num#	k4
</s>
<s>
Millennium	millennium	k1gNnSc1
Technology	technolog	k1gMnPc4
Prize	Priz	k1gInSc5
</s>
<s>
2010	#num#	k4
</s>
<s>
C	C	kA
<g/>
&	&	k?
<g/>
C	C	kA
Prize	Prize	k1gFnSc2
</s>
<s>
2008	#num#	k4
</s>
<s>
Hall	Hall	k1gInSc1
of	of	k?
Fellows	Fellows	k1gInSc1
</s>
<s>
2005	#num#	k4
</s>
<s>
Vollum	Vollum	k1gNnSc1
Award	Awarda	k1gFnPc2
</s>
<s>
2001	#num#	k4
</s>
<s>
Takeda	Takeda	k1gMnSc1
Award	Award	k1gMnSc1
</s>
<s>
2000	#num#	k4
</s>
<s>
Lovelace	Lovelace	k1gFnSc1
Medal	Medal	k1gInSc1
</s>
<s>
1998	#num#	k4
</s>
<s>
EFF	EFF	kA
Pioneer	Pioneer	kA
Award	Award	k1gMnSc1
</s>
<s>
1997	#num#	k4
</s>
<s>
Academic	Academic	k1gMnSc1
Honors	Honorsa	k1gFnPc2
</s>
<s>
1996	#num#	k4
</s>
<s>
9793	#num#	k4
Torvalds	Torvalds	k1gInSc1
(	(	kIx(
<g/>
Asteroid	asteroid	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1995	#num#	k4
</s>
<s>
Running	Running	k1gInSc1
Linux	linux	k1gInSc1
on	on	k3xPp3gMnSc1
AlphaStation	AlphaStation	k1gInSc1
</s>
<s>
Osobní	osobní	k2eAgInSc1d1
život	život	k1gInSc1
</s>
<s>
Linus	Linus	k1gInSc1
Torvalds	Torvaldsa	k1gFnPc2
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Tove	Tove	k1gFnSc7
Torvalds	Torvaldsa	k1gFnPc2
(	(	kIx(
<g/>
rozená	rozený	k2eAgFnSc1d1
Monni	Monň	k1gMnPc7
<g/>
)	)	kIx)
–	–	k?
šestinásobná	šestinásobný	k2eAgFnSc1d1
finská	finský	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
mistryně	mistryně	k1gFnSc1
v	v	k7c6
karate	karate	k1gNnSc6
<g/>
,	,	kIx,
se	s	k7c7
kterou	který	k3yIgFnSc7,k3yRgFnSc7,k3yQgFnSc7
se	se	k3xPyFc4
poprvé	poprvé	k6eAd1
setkal	setkat	k5eAaPmAgMnS
na	na	k7c4
podzim	podzim	k1gInSc4
roku	rok	k1gInSc2
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tove	Tove	k1gInSc4
a	a	k8xC
Linus	Linus	k1gInSc4
mají	mít	k5eAaImIp3nP
tři	tři	k4xCgFnPc4
dcery	dcera	k1gFnPc4
<g/>
,	,	kIx,
Patricia	Patricius	k1gMnSc2
Miranda	Mirando	k1gNnSc2
(	(	kIx(
<g/>
narozená	narozený	k2eAgFnSc1d1
1996	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Daniela	Daniela	k1gFnSc1
Yolanda	Yolanda	k1gFnSc1
(	(	kIx(
<g/>
narozená	narozený	k2eAgFnSc1d1
1998	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Celeste	Celest	k1gInSc5
Amanda	Amanda	k1gFnSc1
(	(	kIx(
<g/>
narozená	narozený	k2eAgFnSc1d1
2000	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
se	se	k3xPyFc4
dvě	dva	k4xCgFnPc1
narodily	narodit	k5eAaPmAgFnP
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Torvalds	Torvalds	k1gInSc1
sám	sám	k3xTgMnSc1
sebe	sebe	k3xPyFc4
popisuje	popisovat	k5eAaImIp3nS
jako	jako	k9
„	„	k?
<g/>
úplně	úplně	k6eAd1
náboženského	náboženský	k2eAgMnSc4d1
ateistu	ateista	k1gMnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
občanem	občan	k1gMnSc7
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
</s>
<s>
Po	po	k7c6
Linusovi	Linus	k1gMnSc6
byla	být	k5eAaImAgFnS
pojmenována	pojmenován	k2eAgFnSc1d1
planetka	planetka	k1gFnSc1
(	(	kIx(
<g/>
9793	#num#	k4
<g/>
)	)	kIx)
Torvalds	Torvalds	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
po	po	k7c6
Linuxu	linux	k1gInSc6
(	(	kIx(
<g/>
9885	#num#	k4
<g/>
)	)	kIx)
Linux	Linux	kA
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
zveřejnil	zveřejnit	k5eAaPmAgInS
Linus	Linus	k1gInSc1
Torvalds	Torvalds	k1gInSc1
na	na	k7c6
svém	svůj	k3xOyFgInSc6
účtu	účet	k1gInSc6
Google	Google	k1gFnSc2
<g/>
+	+	kIx~
kritiku	kritika	k1gFnSc4
na	na	k7c4
společnost	společnost	k1gFnSc4
Google	Google	k1gFnSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
emailová	emailový	k2eAgFnSc1d1
aplikace	aplikace	k1gFnSc1
Gmail	Gmaila	k1gFnPc2
začala	začít	k5eAaPmAgFnS
Torvaldsovi	Torvaldsův	k2eAgMnPc1d1
mylně	mylně	k6eAd1
označovat	označovat	k5eAaImF
za	za	k7c4
spam	spam	k1gInSc4
zprávy	zpráva	k1gFnSc2
od	od	k7c2
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
se	s	k7c7
kterými	který	k3yQgMnPc7,k3yIgMnPc7,k3yRgMnPc7
udržuje	udržovat	k5eAaImIp3nS
komunikaci	komunikace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
spam	spam	k1gInSc4
Gmail	Gmail	k1gMnSc1
označil	označit	k5eAaPmAgMnS
také	také	k9
zprávy	zpráva	k1gFnPc4
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yIgInPc6,k3yRgInPc6,k3yQgInPc6
byly	být	k5eAaImAgFnP
požadavky	požadavek	k1gInPc4
pro	pro	k7c4
vložení	vložení	k1gNnSc4
zdrojového	zdrojový	k2eAgInSc2d1
kódu	kód	k1gInSc2
do	do	k7c2
linuxového	linuxový	k2eAgNnSc2d1
jádra	jádro	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
dostává	dostávat	k5eAaImIp3nS
opakovaně	opakovaně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
Google	Google	k1gInSc1
v	v	k7c6
nedávné	dávný	k2eNgFnSc6d1
době	doba	k1gFnSc6
hlásil	hlásit	k5eAaImAgMnS
chybovost	chybovost	k1gFnSc4
pouze	pouze	k6eAd1
na	na	k7c4
0,1	0,1	k4
%	%	kIx~
<g/>
,	,	kIx,
u	u	k7c2
Torvaldse	Torvalds	k1gMnSc2
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
přibližně	přibližně	k6eAd1
30	#num#	k4
%	%	kIx~
chybně	chybně	k6eAd1
označených	označený	k2eAgFnPc2d1
zpráv	zpráva	k1gFnPc2
a	a	k8xC
byl	být	k5eAaImAgMnS
následně	následně	k6eAd1
nucen	nutit	k5eAaImNgMnS
ručně	ručně	k6eAd1
označit	označit	k5eAaPmF
tisíce	tisíc	k4xCgInPc1
zpráv	zpráva	k1gFnPc2
a	a	k8xC
dát	dát	k5eAaPmF
tak	tak	k6eAd1
Googlu	Googla	k1gFnSc4
vědět	vědět	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
o	o	k7c4
spam	spam	k1gInSc4
nejedná	jednat	k5eNaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
HIMANEN	HIMANEN	kA
<g/>
,	,	kIx,
Pekka	Pekka	k1gFnSc1
<g/>
;	;	kIx,
TORVALDS	TORVALDS	kA
<g/>
,	,	kIx,
Linus	Linus	k1gMnSc1
<g/>
;	;	kIx,
CASTELLS	CASTELLS	kA
<g/>
,	,	kIx,
Manuel	Manuel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Hacker	hacker	k1gMnSc1
Ethic	Ethic	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Secker	Secker	k1gMnSc1
&	&	k?
Warburg	Warburg	k1gMnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
436	#num#	k4
<g/>
-	-	kIx~
<g/>
20550	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
TORVALDS	TORVALDS	kA
<g/>
,	,	kIx,
Linus	Linus	k1gMnSc1
<g/>
;	;	kIx,
DIAMOND	DIAMOND	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Just	just	k6eAd1
For	forum	k1gNnPc2
Fun	Fun	k1gFnSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Story	story	k1gFnSc2
of	of	k?
an	an	k?
Accidental	Accidental	k1gMnSc1
Revolutionary	Revolutionara	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
United	United	k1gMnSc1
States	States	k1gMnSc1
<g/>
:	:	kIx,
HarperCollins	HarperCollins	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
662072	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Torvalds	Torvalds	k1gInSc1
a	a	k8xC
Diamond	Diamond	k1gInSc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
51	#num#	k4
<g/>
-	-	kIx~
<g/>
52	#num#	k4
<g/>
↑	↑	k?
Torvalds	Torvaldsa	k1gFnPc2
a	a	k8xC
Diamond	Diamonda	k1gFnPc2
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
86	#num#	k4
<g/>
,	,	kIx,
95	#num#	k4
<g/>
,	,	kIx,
100	#num#	k4
<g/>
↑	↑	k?
The	The	k1gMnSc5
nightmare	nightmar	k1gMnSc5
continues	continues	k1gInSc1
(	(	kIx(
<g/>
rozhovor	rozhovor	k1gInSc1
s	s	k7c7
Linusem	Linus	k1gMnSc7
Torvaldsem	Torvalds	k1gMnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Linux	linux	k1gInSc1
News	News	k1gInSc1
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
3	#num#	k4
<g/>
,	,	kIx,
18	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1992	#num#	k4
<g/>
,	,	kIx,
cit	cit	k1gInSc1
<g/>
.	.	kIx.
"	"	kIx"
<g/>
assemblers	assemblers	k6eAd1
are	ar	k1gInSc5
for	forum	k1gNnPc2
wimps	wimps	k1gInSc1
<g/>
"	"	kIx"
<g/>
↑	↑	k?
BOŘÁNEK	BOŘÁNEK	kA
<g/>
,	,	kIx,
Roman	Roman	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
IEEE	IEEE	kA
ocenila	ocenit	k5eAaPmAgFnS
Linuse	Linuse	k1gFnSc1
Torvaldse	Torvaldse	k1gFnSc1
za	za	k7c2
stvoření	stvoření	k1gNnSc2
Linuxu	linux	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Root	Root	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1212	#num#	k4
<g/>
-	-	kIx~
<g/>
8309	#num#	k4
<g/>
.	.	kIx.
((	((	k?
<g/>
česky	česky	k6eAd1
<g/>
))	))	k?
↑	↑	k?
Linus	Linus	k1gInSc1
Torvalds	Torvalds	k1gInSc1
se	se	k3xPyFc4
opřel	opřít	k5eAaPmAgInS
do	do	k7c2
hloupé	hloupý	k2eAgFnSc2d1
umělé	umělý	k2eAgFnSc2d1
inteligence	inteligence	k1gFnSc2
Gmailu	Gmail	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karel	Karel	k1gMnSc1
Javůrek	Javůrek	k1gMnSc1
-	-	kIx~
živě	živě	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2015-07-22	2015-07-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Linux	linux	k1gInSc1
</s>
<s>
Linusův	Linusův	k2eAgInSc1d1
zákon	zákon	k1gInSc1
</s>
<s>
Git	Gita	k1gFnPc2
</s>
<s>
GNU	gnu	k1gMnSc1
</s>
<s>
Svobodný	svobodný	k2eAgInSc1d1
software	software	k1gInSc1
</s>
<s>
OpenSource	OpenSourka	k1gFnSc3
</s>
<s>
Richard	Richard	k1gMnSc1
Stallman	Stallman	k1gMnSc1
</s>
<s>
Tux	Tux	k?
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Galerie	galerie	k1gFnSc1
Linus	Linus	k1gInSc1
Torvalds	Torvalds	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Linus	Linus	k1gInSc1
Torvalds	Torvalds	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Osoba	osoba	k1gFnSc1
Linus	Linus	k1gInSc1
Torvalds	Torvalds	k1gInSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Domovská	domovský	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
Linuse	Linuse	k1gFnSc1
Torvaldse	Torvaldse	k1gFnSc1
</s>
<s>
The	The	k?
Rampantly	Rampantly	k1gMnSc1
Unofficial	Unofficial	k1gMnSc1
Linus	Linus	k1gMnSc1
Torvalds	Torvaldsa	k1gFnPc2
FAQ	FAQ	kA
</s>
<s>
Leader	leader	k1gMnSc1
of	of	k?
the	the	k?
Free	Free	k1gInSc1
World	World	k1gMnSc1
–	–	k?
How	How	k1gMnSc1
Linus	Linus	k1gMnSc1
Torvalds	Torvalds	k1gInSc4
became	becam	k1gInSc5
the	the	k?
benevolent	benevolent	k1gInSc1
dictator	dictator	k1gInSc1
of	of	k?
Planet	planeta	k1gFnPc2
Linux	Linux	kA
<g/>
,	,	kIx,
the	the	k?
biggest	biggest	k1gInSc1
collaborative	collaborativ	k1gInSc5
project	project	k5eAaPmF
in	in	k?
history	histor	k1gInPc4
(	(	kIx(
<g/>
Wired	Wired	k1gInSc1
News	News	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Benevolent	Benevolent	k1gMnSc1
Dictator	Dictator	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
slightly	slightnout	k5eAaPmAgFnP
skeptical	skepticat	k5eAaPmAgMnS
unauthorized	unauthorized	k1gMnSc1
biography	biographa	k1gFnSc2
and	and	k?
the	the	k?
first	first	k5eAaPmF
ten	ten	k3xDgInSc4
years	years	k1gInSc4
of	of	k?
Linux	linux	k1gInSc1
(	(	kIx(
<g/>
Softpanorama	Softpanorama	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
famous	famous	k1gInSc1
„	„	k?
<g/>
LINUX	linux	k1gInSc1
is	is	k?
obsolete	obsolést	k5eAaPmIp3nS
<g/>
“	“	k?
thread	thread	k1gInSc1
from	from	k1gMnSc1
the	the	k?
comp	comp	k1gMnSc1
<g/>
.	.	kIx.
<g/>
os	osa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
minix	minix	k1gInSc1
newsgroup	newsgroup	k1gInSc1
</s>
<s>
Andrew	Andrew	k?
S.	S.	kA
Tanenbaum	Tanenbaum	k1gInSc1
on	on	k3xPp3gMnSc1
the	the	k?
origins	origins	k1gInSc1
of	of	k?
Linux	Linux	kA
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Rozhovory	rozhovor	k1gInPc1
</s>
<s>
Linux	linux	k1gInSc4
Journal	Journal	k1gFnSc2
Archivováno	archivovat	k5eAaBmNgNnS
26	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2002	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
1994	#num#	k4
</s>
<s>
Linux	linux	k1gInSc1
Journal	Journal	k1gFnSc2
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
1999	#num#	k4
</s>
<s>
Wired	Wired	k1gMnSc1
Magazine	Magazin	k1gInSc5
–	–	k?
červenec	červenec	k1gInSc4
2003	#num#	k4
</s>
<s>
BusinessWeek	BusinessWeek	k1gInSc1
–	–	k?
18	#num#	k4
<g/>
.	.	kIx.
srpen	srpen	k1gInSc1
2004	#num#	k4
</s>
<s>
The	The	k?
Seattle	Seattle	k1gFnSc1
Times	Times	k1gInSc1
–	–	k?
11	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
2004	#num#	k4
</s>
<s>
CNET	CNET	kA
News	News	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
–	–	k?
21	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
2004	#num#	k4
</s>
<s>
Linux	linux	k1gInSc1
Journal	Journal	k1gFnSc2
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2019	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Svobodný	svobodný	k2eAgInSc1d1
software	software	k1gInSc1
|	|	kIx~
Finsko	Finsko	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
34669	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
122924517	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1494	#num#	k4
6962	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
15725	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
85515270	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
15725	#num#	k4
</s>
