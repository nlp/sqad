<s>
Pohotovostní	pohotovostní	k2eAgFnPc1d1	pohotovostní
jednotky	jednotka	k1gFnPc1	jednotka
Einsatzgruppen	Einsatzgruppen	k2eAgInSc4d1	Einsatzgruppen
byly	být	k5eAaImAgInP	být
zřízeny	zřízen	k2eAgMnPc4d1	zřízen
vedoucími	vedoucí	k1gMnPc7	vedoucí
představiteli	představitel	k1gMnPc7	představitel
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
již	již	k6eAd1	již
před	před	k7c7	před
invazí	invaze	k1gFnSc7	invaze
do	do	k7c2	do
Rakouska	Rakousko	k1gNnSc2	Rakousko
a	a	k8xC	a
obsazením	obsazení	k1gNnSc7	obsazení
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obsazení	obsazení	k1gNnSc6	obsazení
Polska	Polsko	k1gNnSc2	Polsko
bylo	být	k5eAaImAgNnS	být
jejich	jejich	k3xOp3gInSc7	jejich
úkolem	úkol	k1gInSc7	úkol
terorizovat	terorizovat	k5eAaImF	terorizovat
místní	místní	k2eAgNnSc4d1	místní
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
a	a	k8xC	a
likvidovat	likvidovat	k5eAaBmF	likvidovat
nežádoucí	žádoucí	k2eNgFnPc4d1	nežádoucí
osoby	osoba	k1gFnPc4	osoba
<g/>
,	,	kIx,	,
v	v	k7c6	v
první	první	k4xOgFnSc6	první
řadě	řada	k1gFnSc6	řada
Židy	Žid	k1gMnPc4	Žid
<g/>
,	,	kIx,	,
polskou	polský	k2eAgFnSc4d1	polská
inteligenci	inteligence	k1gFnSc4	inteligence
a	a	k8xC	a
kněze	kněz	k1gMnPc4	kněz
(	(	kIx(	(
<g/>
operace	operace	k1gFnSc1	operace
Tannenberg	Tannenberg	k1gMnSc1	Tannenberg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
jednotky	jednotka	k1gFnPc1	jednotka
SS	SS	kA	SS
Einsatzgruppen	Einsatzgruppen	k2eAgInSc1d1	Einsatzgruppen
určené	určený	k2eAgInPc4d1	určený
pro	pro	k7c4	pro
činnost	činnost	k1gFnSc4	činnost
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
byl	být	k5eAaImAgInS	být
plánován	plánovat	k5eAaImNgInS	plánovat
německý	německý	k2eAgInSc1d1	německý
útok	útok	k1gInSc1	útok
-	-	kIx~	-
operace	operace	k1gFnSc2	operace
Barbarossa	Barbarossa	k1gMnSc1	Barbarossa
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
úkolem	úkol	k1gInSc7	úkol
byl	být	k5eAaImAgInS	být
jednak	jednak	k8xC	jednak
boj	boj	k1gInSc1	boj
proti	proti	k7c3	proti
partyzánům	partyzán	k1gMnPc3	partyzán
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
likvidace	likvidace	k1gFnSc2	likvidace
skupin	skupina	k1gFnPc2	skupina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
-	-	kIx~	-
v	v	k7c6	v
první	první	k4xOgFnSc6	první
řadě	řada	k1gFnSc6	řada
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
komunistů	komunista	k1gMnPc2	komunista
a	a	k8xC	a
Cikánů	cikán	k1gMnPc2	cikán
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
následovalo	následovat	k5eAaImAgNnS	následovat
vyhlazování	vyhlazování	k1gNnSc1	vyhlazování
slovanského	slovanský	k2eAgNnSc2d1	slovanské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
a	a	k8xC	a
příslušníků	příslušník	k1gMnPc2	příslušník
asijských	asijský	k2eAgFnPc2d1	asijská
menšin	menšina	k1gFnPc2	menšina
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Veliteli	velitel	k1gMnPc7	velitel
těchto	tento	k3xDgFnPc2	tento
zvláštních	zvláštní	k2eAgFnPc2d1	zvláštní
jednotek	jednotka	k1gFnPc2	jednotka
byli	být	k5eAaImAgMnP	být
příslušníci	příslušník	k1gMnPc1	příslušník
tajné	tajný	k2eAgFnSc2d1	tajná
státní	státní	k2eAgFnSc2d1	státní
policie	policie	k1gFnSc2	policie
gestapa	gestapo	k1gNnSc2	gestapo
(	(	kIx(	(
<g/>
Geheime	Geheim	k1gMnSc5	Geheim
Staatspolizei	Staatspolize	k1gMnSc5	Staatspolize
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
SD	SD	kA	SD
(	(	kIx(	(
<g/>
Sicherheitsdienst	Sicherheitsdienst	k1gMnSc1	Sicherheitsdienst
<g/>
)	)	kIx)	)
a	a	k8xC	a
SS	SS	kA	SS
(	(	kIx(	(
<g/>
Schutz-Staffeln	Schutz-Staffeln	k1gMnSc1	Schutz-Staffeln
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mužstvo	mužstvo	k1gNnSc4	mužstvo
tvořili	tvořit	k5eAaImAgMnP	tvořit
příslušníci	příslušník	k1gMnPc1	příslušník
gestapa	gestapo	k1gNnSc2	gestapo
<g/>
,	,	kIx,	,
SD	SD	kA	SD
<g/>
,	,	kIx,	,
SS	SS	kA	SS
<g/>
,	,	kIx,	,
příslušníci	příslušník	k1gMnPc1	příslušník
pořádkové	pořádkový	k2eAgFnSc2d1	pořádková
policie	policie	k1gFnSc2	policie
OrPo	OrPo	k1gNnSc1	OrPo
(	(	kIx(	(
<g/>
Ordnungspolizei	Ordnungspolizei	k1gNnSc1	Ordnungspolizei
<g/>
)	)	kIx)	)
a	a	k8xC	a
zejména	zejména	k9	zejména
příslušníci	příslušník	k1gMnPc1	příslušník
zbraní	zbraň	k1gFnPc2	zbraň
SS	SS	kA	SS
(	(	kIx(	(
<g/>
Waffen-SS	Waffen-SS	k1gMnSc1	Waffen-SS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
SS	SS	kA	SS
Einsatzgruppen	Einsatzgruppen	k2eAgInSc4d1	Einsatzgruppen
vykonávaly	vykonávat	k5eAaImAgInP	vykonávat
ty	ten	k3xDgInPc1	ten
nejvíce	hodně	k6eAd3	hodně
odsuzované	odsuzovaný	k2eAgInPc1d1	odsuzovaný
zločiny	zločin	k1gInPc1	zločin
na	na	k7c6	na
okupovaném	okupovaný	k2eAgNnSc6d1	okupované
území	území	k1gNnSc6	území
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
v	v	k7c6	v
rokli	rokle	k1gFnSc6	rokle
Babí	babí	k2eAgFnSc1d1	babí
Jar	jar	k1gFnSc1	jar
u	u	k7c2	u
Kyjeva	Kyjev	k1gInSc2	Kyjev
zavraždily	zavraždit	k5eAaPmAgFnP	zavraždit
koncem	koncem	k7c2	koncem
září	září	k1gNnSc2	září
přes	přes	k7c4	přes
33	[number]	k4	33
tisíc	tisíc	k4xCgInPc2	tisíc
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
místě	místo	k1gNnSc6	místo
bylo	být	k5eAaImAgNnS	být
celkem	celkem	k6eAd1	celkem
zmasakrováno	zmasakrovat	k5eAaPmNgNnS	zmasakrovat
100	[number]	k4	100
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
tisíc	tisíc	k4xCgInPc2	tisíc
občanů	občan	k1gMnPc2	občan
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
se	se	k3xPyFc4	se
Sonderkommando	Sonderkommanda	k1gFnSc5	Sonderkommanda
1005	[number]	k4	1005
pokusilo	pokusit	k5eAaPmAgNnS	pokusit
odstranit	odstranit	k5eAaPmF	odstranit
důkazy	důkaz	k1gInPc4	důkaz
o	o	k7c6	o
masových	masový	k2eAgFnPc6d1	masová
popravách	poprava	k1gFnPc6	poprava
exhumací	exhumace	k1gFnPc2	exhumace
a	a	k8xC	a
kremací	kremace	k1gFnPc2	kremace
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Einsatzgruppen	Einsatzgruppen	k2eAgInSc4d1	Einsatzgruppen
se	se	k3xPyFc4	se
dělily	dělit	k5eAaImAgInP	dělit
na	na	k7c4	na
další	další	k2eAgFnPc4d1	další
podskupiny	podskupina	k1gFnPc4	podskupina
známé	známý	k2eAgNnSc1d1	známé
jako	jako	k9	jako
Einsatzkommandos	Einsatzkommandos	k1gInSc4	Einsatzkommandos
či	či	k8xC	či
Sonderkommandos	Sonderkommandos	k1gInSc4	Sonderkommandos
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
mohly	moct	k5eAaImAgFnP	moct
požádat	požádat	k5eAaPmF	požádat
jednotky	jednotka	k1gFnPc1	jednotka
Einsatzgruppen	Einsatzgruppen	k2eAgInSc4d1	Einsatzgruppen
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
Wehrmacht	wehrmacht	k1gFnSc2	wehrmacht
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
důležitější	důležitý	k2eAgFnSc1d2	důležitější
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gFnPc4	on
spolupráce	spolupráce	k1gFnPc4	spolupráce
s	s	k7c7	s
místními	místní	k2eAgInPc7d1	místní
policejními	policejní	k2eAgInPc7d1	policejní
sbory	sbor	k1gInPc7	sbor
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
,	,	kIx,	,
v	v	k7c6	v
Lotyšsku	Lotyšsko	k1gNnSc6	Lotyšsko
či	či	k8xC	či
Litvě	Litva	k1gFnSc6	Litva
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jejich	jejich	k3xOp3gMnPc1	jejich
příslušníci	příslušník	k1gMnPc1	příslušník
bývali	bývat	k5eAaImAgMnP	bývat
odměňováni	odměňovat	k5eAaImNgMnP	odměňovat
z	z	k7c2	z
majetku	majetek	k1gInSc2	majetek
ukradeného	ukradený	k2eAgInSc2d1	ukradený
zavražděným	zavražděný	k2eAgFnPc3d1	zavražděná
obětem	oběť	k1gFnPc3	oběť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
byly	být	k5eAaImAgFnP	být
čtyři	čtyři	k4xCgFnPc4	čtyři
Einsatzgruppy	Einsatzgruppa	k1gFnPc4	Einsatzgruppa
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
každou	každý	k3xTgFnSc4	každý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
tvořilo	tvořit	k5eAaImAgNnS	tvořit
500	[number]	k4	500
<g/>
-	-	kIx~	-
<g/>
990	[number]	k4	990
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
sloužilo	sloužit	k5eAaImAgNnS	sloužit
v	v	k7c4	v
Einsatzgruppen	Einsatzgruppen	k2eAgInSc4d1	Einsatzgruppen
zhruba	zhruba	k6eAd1	zhruba
3000	[number]	k4	3000
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Einsatzgruppe	Einsatzgruppat	k5eAaPmIp3nS	Einsatzgruppat
A	a	k9	a
(	(	kIx(	(
<g/>
velitel	velitel	k1gMnSc1	velitel
SS-Standartenführer	SS-Standartenführer	k1gMnSc1	SS-Standartenführer
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Franz	Franz	k1gMnSc1	Franz
Walter	Walter	k1gMnSc1	Walter
Stahlecker	Stahlecker	k1gMnSc1	Stahlecker
<g/>
)	)	kIx)	)
-	-	kIx~	-
Pobaltí	Pobaltí	k1gNnSc1	Pobaltí
<g/>
,	,	kIx,	,
sídlo	sídlo	k1gNnSc1	sídlo
"	"	kIx"	"
<g/>
pevnost	pevnost	k1gFnSc1	pevnost
smrti	smrt	k1gFnSc2	smrt
číslo	číslo	k1gNnSc1	číslo
9	[number]	k4	9
<g/>
"	"	kIx"	"
u	u	k7c2	u
Kaunasu	Kaunas	k1gInSc2	Kaunas
Einsatzgruppe	Einsatzgrupp	k1gInSc5	Einsatzgrupp
B	B	kA	B
(	(	kIx(	(
<g/>
velitel	velitel	k1gMnSc1	velitel
SS-Brigadeführer	SS-Brigadeführer	k1gMnSc1	SS-Brigadeführer
Arthur	Arthur	k1gMnSc1	Arthur
Nebe	nebe	k1gNnSc2	nebe
<g/>
)	)	kIx)	)
-	-	kIx~	-
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
Einsatzgruppe	Einsatzgrupp	k1gInSc5	Einsatzgrupp
C	C	kA	C
(	(	kIx(	(
<g/>
velitel	velitel	k1gMnSc1	velitel
SS-Brigadeführer	SS-Brigadeführer	k1gMnSc1	SS-Brigadeführer
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Otto	Otto	k1gMnSc1	Otto
Rasch	Rasch	k1gMnSc1	Rasch
<g/>
)	)	kIx)	)
-	-	kIx~	-
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
,	,	kIx,	,
sídlo	sídlo	k1gNnSc1	sídlo
Kyjev	Kyjev	k1gInSc1	Kyjev
Einsatzgruppe	Einsatzgrupp	k1gInSc5	Einsatzgrupp
D	D	kA	D
(	(	kIx(	(
<g/>
velitel	velitel	k1gMnSc1	velitel
SS-Standartenführer	SS-Standartenführer	k1gMnSc1	SS-Standartenführer
prof.	prof.	kA	prof.
Otto	Otto	k1gMnSc1	Otto
Ohlendorf	Ohlendorf	k1gMnSc1	Ohlendorf
<g/>
)	)	kIx)	)
-	-	kIx~	-
Besarábie	Besarábie	k1gFnSc1	Besarábie
<g/>
,	,	kIx,	,
Krym	Krym	k1gInSc1	Krym
<g/>
,	,	kIx,	,
Kavkaz	Kavkaz	k1gInSc1	Kavkaz
Způsob	způsob	k1gInSc1	způsob
likvidace	likvidace	k1gFnSc1	likvidace
lidí	člověk	k1gMnPc2	člověk
těmito	tento	k3xDgFnPc7	tento
komandy	komando	k1gNnPc7	komando
byl	být	k5eAaImAgInS	být
nanejvýš	nanejvýš	k6eAd1	nanejvýš
brutální	brutální	k2eAgMnSc1d1	brutální
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
byli	být	k5eAaImAgMnP	být
vyhnáni	vyhnat	k5eAaPmNgMnP	vyhnat
z	z	k7c2	z
domovů	domov	k1gInPc2	domov
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
stávalo	stávat	k5eAaImAgNnS	stávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemohoucí	mohoucí	k2eNgMnPc1d1	nemohoucí
lidé	člověk	k1gMnPc1	člověk
byli	být	k5eAaImAgMnP	být
zastřeleni	zastřelit	k5eAaPmNgMnP	zastřelit
i	i	k9	i
v	v	k7c6	v
posteli	postel	k1gFnSc6	postel
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgMnPc1d1	ostatní
odcházeli	odcházet	k5eAaImAgMnP	odcházet
buď	buď	k8xC	buď
do	do	k7c2	do
transportů	transport	k1gInPc2	transport
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
je	být	k5eAaImIp3nS	být
odvážely	odvážet	k5eAaImAgFnP	odvážet
do	do	k7c2	do
koncentračních	koncentrační	k2eAgInPc2d1	koncentrační
táborů	tábor	k1gInPc2	tábor
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
na	na	k7c4	na
určitá	určitý	k2eAgNnPc4d1	určité
místa	místo	k1gNnPc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
muži	muž	k1gMnPc1	muž
museli	muset	k5eAaImAgMnP	muset
vykopat	vykopat	k5eAaPmF	vykopat
masový	masový	k2eAgInSc1d1	masový
hrob	hrob	k1gInSc1	hrob
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
lidé	člověk	k1gMnPc1	člověk
byli	být	k5eAaImAgMnP	být
postupně	postupně	k6eAd1	postupně
stříleni	střílet	k5eAaImNgMnP	střílet
a	a	k8xC	a
padali	padat	k5eAaImAgMnP	padat
postupně	postupně	k6eAd1	postupně
do	do	k7c2	do
hrobu	hrob	k1gInSc2	hrob
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
exekucí	exekuce	k1gFnSc7	exekuce
se	se	k3xPyFc4	se
mnohdy	mnohdy	k6eAd1	mnohdy
museli	muset	k5eAaImAgMnP	muset
dívat	dívat	k5eAaImF	dívat
na	na	k7c4	na
oběti	oběť	k1gFnPc4	oběť
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
tam	tam	k6eAd1	tam
už	už	k6eAd1	už
ležely	ležet	k5eAaImAgFnP	ležet
<g/>
.	.	kIx.	.
</s>
<s>
Stávalo	stávat	k5eAaImAgNnS	stávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ženy	žena	k1gFnPc1	žena
před	před	k7c7	před
popravou	poprava	k1gFnSc7	poprava
musely	muset	k5eAaImAgFnP	muset
vysvléct	vysvléct	k5eAaPmF	vysvléct
do	do	k7c2	do
naha	naho	k1gNnSc2	naho
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
byly	být	k5eAaImAgFnP	být
před	před	k7c7	před
popravou	poprava	k1gFnSc7	poprava
znásilněny	znásilněn	k2eAgFnPc1d1	znásilněna
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
samy	sám	k3xTgFnPc1	sám
podvolily	podvolit	k5eAaPmAgFnP	podvolit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
zachránily	zachránit	k5eAaPmAgFnP	zachránit
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
většinou	většina	k1gFnSc7	většina
stejně	stejně	k6eAd1	stejně
byly	být	k5eAaImAgInP	být
druhý	druhý	k4xOgInSc1	druhý
den	den	k1gInSc4	den
zastřeleny	zastřelit	k5eAaPmNgInP	zastřelit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
Němci	Němec	k1gMnPc1	Němec
zbavili	zbavit	k5eAaPmAgMnP	zbavit
svědků	svědek	k1gMnPc2	svědek
masakru	masakr	k1gInSc2	masakr
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnPc1d1	další
"	"	kIx"	"
<g/>
kratochvílí	kratochvílit	k5eAaPmIp3nP	kratochvílit
<g/>
"	"	kIx"	"
komand	komando	k1gNnPc2	komando
byly	být	k5eAaImAgFnP	být
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
hony	hon	k1gInPc4	hon
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgInPc6	jenž
byli	být	k5eAaImAgMnP	být
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
schovávali	schovávat	k5eAaImAgMnP	schovávat
v	v	k7c6	v
lese	les	k1gInSc6	les
<g/>
,	,	kIx,	,
stříleni	střílen	k2eAgMnPc1d1	střílen
jako	jako	k8xC	jako
lovná	lovný	k2eAgFnSc1d1	lovná
zvěř	zvěř	k1gFnSc1	zvěř
<g/>
.	.	kIx.	.
</s>
<s>
Podobnou	podobný	k2eAgFnSc7d1	podobná
záležitostí	záležitost	k1gFnSc7	záležitost
bylo	být	k5eAaImAgNnS	být
využívání	využívání	k1gNnSc1	využívání
odsouzených	odsouzená	k1gFnPc2	odsouzená
jako	jako	k8xS	jako
terčů	terč	k1gInPc2	terč
při	při	k7c6	při
cvičných	cvičný	k2eAgFnPc6d1	cvičná
střelbách	střelba	k1gFnPc6	střelba
<g/>
.	.	kIx.	.
</s>
<s>
Brutálním	brutální	k2eAgInSc7d1	brutální
způsobem	způsob	k1gInSc7	způsob
likvidace	likvidace	k1gFnSc2	likvidace
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
nahánění	nahánění	k1gNnSc1	nahánění
lidí	člověk	k1gMnPc2	člověk
na	na	k7c4	na
dopravní	dopravní	k2eAgFnPc4d1	dopravní
lodě	loď	k1gFnPc4	loď
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
potopeny	potopen	k2eAgInPc1d1	potopen
<g/>
.	.	kIx.	.
</s>
<s>
Einsatzgruppy	Einsatzgruppa	k1gFnPc1	Einsatzgruppa
se	se	k3xPyFc4	se
účastnily	účastnit	k5eAaImAgFnP	účastnit
i	i	k9	i
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
vyčišťovacích	vyčišťovací	k2eAgFnPc2d1	vyčišťovací
protipartyzánských	protipartyzánský	k2eAgFnPc2d1	protipartyzánská
akcí	akce	k1gFnPc2	akce
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgInPc6	jenž
bylo	být	k5eAaImAgNnS	být
masakrováno	masakrovat	k5eAaBmNgNnS	masakrovat
civilní	civilní	k2eAgNnSc1d1	civilní
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
vesnic	vesnice	k1gFnPc2	vesnice
v	v	k7c6	v
příslušné	příslušný	k2eAgFnSc6d1	příslušná
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
těchto	tento	k3xDgFnPc6	tento
akcích	akce	k1gFnPc6	akce
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vykonávaly	vykonávat	k5eAaImAgFnP	vykonávat
samostatně	samostatně	k6eAd1	samostatně
i	i	k8xC	i
jednotky	jednotka	k1gFnSc2	jednotka
Waffen	Waffna	k1gFnPc2	Waffna
SS	SS	kA	SS
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
lidé	člověk	k1gMnPc1	člověk
přinuceni	přinucen	k2eAgMnPc1d1	přinucen
odejít	odejít	k5eAaPmF	odejít
do	do	k7c2	do
některé	některý	k3yIgFnSc2	některý
větší	veliký	k2eAgFnSc2d2	veliký
budovy	budova	k1gFnSc2	budova
(	(	kIx(	(
<g/>
např.	např.	kA	např.
do	do	k7c2	do
kostela	kostel	k1gInSc2	kostel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tam	tam	k6eAd1	tam
byli	být	k5eAaImAgMnP	být
buď	buď	k8xC	buď
upáleni	upálit	k5eAaPmNgMnP	upálit
zaživa	zaživa	k6eAd1	zaživa
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zmasakrováni	zmasakrován	k2eAgMnPc1d1	zmasakrován
ručními	ruční	k2eAgInPc7d1	ruční
granáty	granát	k1gInPc7	granát
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
vesnice	vesnice	k1gFnPc4	vesnice
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgMnPc7	jenž
byl	být	k5eAaImAgMnS	být
např.	např.	kA	např.
i	i	k8xC	i
Český	český	k2eAgInSc1d1	český
Malín	Malín	k1gInSc1	Malín
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
žila	žít	k5eAaImAgFnS	žít
česká	český	k2eAgFnSc1d1	Česká
menšina	menšina	k1gFnSc1	menšina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgFnP	být
do	do	k7c2	do
základů	základ	k1gInPc2	základ
vypáleny	vypálen	k2eAgInPc1d1	vypálen
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obětí	oběť	k1gFnPc2	oběť
řádění	řádění	k1gNnSc1	řádění
těchto	tento	k3xDgFnPc2	tento
jednotek	jednotka	k1gFnPc2	jednotka
není	být	k5eNaImIp3nS	být
přesně	přesně	k6eAd1	přesně
znám	znám	k2eAgMnSc1d1	znám
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jen	jen	k9	jen
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
bylo	být	k5eAaImAgNnS	být
těmito	tento	k3xDgFnPc7	tento
komandy	komando	k1gNnPc7	komando
smrti	smrt	k1gFnSc6	smrt
zlikvidováno	zlikvidovat	k5eAaPmNgNnS	zlikvidovat
zhruba	zhruba	k6eAd1	zhruba
2	[number]	k4	2
milióny	milión	k4xCgInPc4	milión
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
asi	asi	k9	asi
900	[number]	k4	900
tisíc	tisíc	k4xCgInPc2	tisíc
Židů	Žid	k1gMnPc2	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
zdroje	zdroj	k1gInPc1	zdroj
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jednotky	jednotka	k1gFnPc1	jednotka
Einsatzgruppen	Einsatzgruppen	k2eAgMnSc1d1	Einsatzgruppen
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
svědomí	svědomí	k1gNnSc6	svědomí
smrt	smrt	k1gFnSc4	smrt
až	až	k6eAd1	až
tří	tři	k4xCgInPc2	tři
miliónů	milión	k4xCgInPc2	milión
sovětských	sovětský	k2eAgMnPc2d1	sovětský
válečných	válečný	k2eAgMnPc2d1	válečný
zajatců	zajatec	k1gMnPc2	zajatec
<g/>
.	.	kIx.	.
</s>
<s>
Odhady	odhad	k1gInPc4	odhad
komplikuje	komplikovat	k5eAaBmIp3nS	komplikovat
i	i	k9	i
ta	ten	k3xDgFnSc1	ten
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
přesně	přesně	k6eAd1	přesně
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
kolik	kolik	k4yRc4	kolik
zločinů	zločin	k1gInPc2	zločin
spadalo	spadat	k5eAaImAgNnS	spadat
pod	pod	k7c4	pod
přímou	přímý	k2eAgFnSc4d1	přímá
kompetenci	kompetence	k1gFnSc4	kompetence
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
skupin	skupina	k1gFnPc2	skupina
Einsatzgruppen	Einsatzgruppen	k2eAgInSc1d1	Einsatzgruppen
<g/>
,	,	kIx,	,
a	a	k8xC	a
kolik	kolik	k9	kolik
jí	on	k3xPp3gFnSc7	on
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
svědomí	svědomí	k1gNnSc6	svědomí
další	další	k2eAgInPc4d1	další
německé	německý	k2eAgInPc4d1	německý
oddíly	oddíl	k1gInPc4	oddíl
(	(	kIx(	(
<g/>
Waffen	Waffen	k1gInSc4	Waffen
SS	SS	kA	SS
<g/>
,	,	kIx,	,
SS-Totenkopfverbände	SS-Totenkopfverbänd	k1gMnSc5	SS-Totenkopfverbänd
<g/>
,	,	kIx,	,
Wehrmacht	wehrmacht	k1gFnPc1	wehrmacht
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc1d1	jiná
speciální	speciální	k2eAgFnPc1d1	speciální
skupiny	skupina	k1gFnPc1	skupina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
Slovenského	slovenský	k2eAgNnSc2d1	slovenské
národního	národní	k2eAgNnSc2d1	národní
povstání	povstání	k1gNnSc2	povstání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
působily	působit	k5eAaImAgFnP	působit
tyto	tento	k3xDgFnPc1	tento
jednotky	jednotka	k1gFnPc1	jednotka
i	i	k9	i
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
potlačování	potlačování	k1gNnSc2	potlačování
povstání	povstání	k1gNnSc2	povstání
se	se	k3xPyFc4	se
účastnily	účastnit	k5eAaImAgFnP	účastnit
masakrů	masakr	k1gInPc2	masakr
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgInPc6	jenž
bylo	být	k5eAaImAgNnS	být
povražděno	povraždit	k5eAaPmNgNnS	povraždit
přes	přes	k7c4	přes
5	[number]	k4	5
tisíc	tisíc	k4xCgInPc2	tisíc
Slováků	Slovák	k1gMnPc2	Slovák
včetně	včetně	k7c2	včetně
starců	stařec	k1gMnPc2	stařec
<g/>
,	,	kIx,	,
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
vypáleno	vypálit	k5eAaPmNgNnS	vypálit
90	[number]	k4	90
slovenských	slovenský	k2eAgFnPc2d1	slovenská
vesnic	vesnice	k1gFnPc2	vesnice
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
bylo	být	k5eAaImAgNnS	být
odkryto	odkrýt	k5eAaPmNgNnS	odkrýt
211	[number]	k4	211
hromadných	hromadný	k2eAgInPc2d1	hromadný
hrobů	hrob	k1gInPc2	hrob
<g/>
.	.	kIx.	.
</s>
<s>
Sonderfahndungsliste	Sonderfahndungslist	k1gMnSc5	Sonderfahndungslist
G.B.	G.B.	k1gMnSc5	G.B.
Kommissarbefehl	Kommissarbefehl	k1gMnSc7	Kommissarbefehl
Následující	následující	k2eAgInPc4d1	následující
zdokumentované	zdokumentovaný	k2eAgInPc4d1	zdokumentovaný
zločiny	zločin	k1gInPc4	zločin
na	na	k7c6	na
obsazených	obsazený	k2eAgNnPc6d1	obsazené
územích	území	k1gNnPc6	území
jsou	být	k5eAaImIp3nP	být
připisovány	připisován	k2eAgFnPc1d1	připisována
jednotkám	jednotka	k1gFnPc3	jednotka
Einsatzgruppen	Einsatzgruppen	k2eAgInSc1d1	Einsatzgruppen
spíše	spíše	k9	spíše
než	než	k8xS	než
Wehrmachtu	wehrmacht	k1gInSc3	wehrmacht
operujícímu	operující	k2eAgInSc3d1	operující
na	na	k7c6	na
frontě	fronta	k1gFnSc6	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Některých	některý	k3yIgFnPc2	některý
protižidovských	protižidovský	k2eAgFnPc2d1	protižidovská
akcí	akce	k1gFnPc2	akce
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
,	,	kIx,	,
v	v	k7c6	v
Litvě	Litva	k1gFnSc6	Litva
a	a	k8xC	a
v	v	k7c6	v
Lotyšsku	Lotyšsko	k1gNnSc6	Lotyšsko
se	se	k3xPyFc4	se
účastnily	účastnit	k5eAaImAgFnP	účastnit
i	i	k9	i
místní	místní	k2eAgFnPc1d1	místní
antisemitské	antisemitský	k2eAgFnPc1d1	antisemitská
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Operace	operace	k1gFnSc1	operace
Tannenberg	Tannenberg	k1gMnSc1	Tannenberg
-	-	kIx~	-
r.	r.	kA	r.
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
vyhlazovací	vyhlazovací	k2eAgFnSc1d1	vyhlazovací
akce	akce	k1gFnSc1	akce
polských	polský	k2eAgMnPc2d1	polský
aktivistů	aktivista	k1gMnPc2	aktivista
<g/>
,	,	kIx,	,
asi	asi	k9	asi
20	[number]	k4	20
000	[number]	k4	000
obětí	oběť	k1gFnPc2	oběť
Intelligenzaktion	Intelligenzaktion	k1gInSc1	Intelligenzaktion
-	-	kIx~	-
r.	r.	kA	r.
1939	[number]	k4	1939
<g/>
-	-	kIx~	-
<g/>
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
vyhlazovací	vyhlazovací	k2eAgFnSc1d1	vyhlazovací
akce	akce	k1gFnSc1	akce
polské	polský	k2eAgFnSc2d1	polská
elity	elita	k1gFnSc2	elita
<g/>
,	,	kIx,	,
asi	asi	k9	asi
60	[number]	k4	60
000	[number]	k4	000
obětí	oběť	k1gFnPc2	oběť
Německá	německý	k2eAgFnSc1d1	německá
AB-Aktion	AB-Aktion	k1gInSc4	AB-Aktion
v	v	k7c4	v
Polsku	Polska	k1gFnSc4	Polska
-	-	kIx~	-
r.	r.	kA	r.
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
zatčeno	zatknout	k5eAaPmNgNnS	zatknout
cca	cca	kA	cca
30	[number]	k4	30
000	[number]	k4	000
<g />
.	.	kIx.	.
</s>
<s>
Poláků	Polák	k1gMnPc2	Polák
<g/>
,	,	kIx,	,
bezprostředních	bezprostřední	k2eAgFnPc2d1	bezprostřední
obětí	oběť	k1gFnPc2	oběť
asi	asi	k9	asi
7	[number]	k4	7
000	[number]	k4	000
Operace	operace	k1gFnSc1	operace
Reinhard	Reinharda	k1gFnPc2	Reinharda
-	-	kIx~	-
r.	r.	kA	r.
1941	[number]	k4	1941
<g/>
-	-	kIx~	-
<g/>
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
vyhlazovací	vyhlazovací	k2eAgInPc4d1	vyhlazovací
tábory	tábor	k1gInPc4	tábor
<g/>
,	,	kIx,	,
asi	asi	k9	asi
1,5	[number]	k4	1,5
milionů	milion	k4xCgInPc2	milion
obětí	oběť	k1gFnPc2	oběť
-	-	kIx~	-
Židů	Žid	k1gMnPc2	Žid
Devátá	devátý	k4xOgFnSc1	devátý
pevnost	pevnost	k1gFnSc1	pevnost
-	-	kIx~	-
r.	r.	kA	r.
1941	[number]	k4	1941
<g/>
-	-	kIx~	-
<g/>
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
židovské	židovský	k2eAgNnSc4d1	Židovské
ghetto	ghetto	k1gNnSc4	ghetto
<g/>
,	,	kIx,	,
asi	asi	k9	asi
30	[number]	k4	30
000	[number]	k4	000
obětí	oběť	k1gFnPc2	oběť
Kaunaský	Kaunaský	k2eAgInSc1d1	Kaunaský
pogrom	pogrom	k1gInSc1	pogrom
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
r.	r.	kA	r.
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
Kaunaský	Kaunaský	k2eAgInSc1d1	Kaunaský
pogrom	pogrom	k1gInSc1	pogrom
<g/>
,	,	kIx,	,
asi	asi	k9	asi
3	[number]	k4	3
000	[number]	k4	000
obětí	oběť	k1gFnPc2	oběť
-	-	kIx~	-
Židů	Žid	k1gMnPc2	Žid
Kaunaský	Kaunaský	k2eAgInSc4d1	Kaunaský
masakr	masakr	k1gInSc4	masakr
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1941	[number]	k4	1941
-	-	kIx~	-
r.	r.	kA	r.
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
masová	masový	k2eAgFnSc1d1	masová
poprava	poprava	k1gFnSc1	poprava
asi	asi	k9	asi
9	[number]	k4	9
000	[number]	k4	000
Židů	Žid	k1gMnPc2	Žid
v	v	k7c6	v
Deváté	devátý	k4xOgFnSc6	devátý
pevnosti	pevnost	k1gFnSc6	pevnost
Masakry	masakr	k1gInPc1	masakr
v	v	k7c6	v
Deváté	devátý	k4xOgFnSc6	devátý
pevnosti	pevnost	k1gFnSc6	pevnost
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1941	[number]	k4	1941
-	-	kIx~	-
r.	r.	kA	r.
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
masová	masový	k2eAgFnSc1d1	masová
poprava	poprava	k1gFnSc1	poprava
<g />
.	.	kIx.	.
</s>
<s>
asi	asi	k9	asi
5	[number]	k4	5
000	[number]	k4	000
Židů	Žid	k1gMnPc2	Žid
v	v	k7c6	v
Deváté	devátý	k4xOgFnSc6	devátý
pevnosti	pevnost	k1gFnSc6	pevnost
Masakr	masakr	k1gInSc1	masakr
v	v	k7c6	v
Ponarech	Ponar	k1gInPc6	Ponar
-	-	kIx~	-
r.	r.	kA	r.
1941	[number]	k4	1941
<g/>
-	-	kIx~	-
<g/>
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
masové	masový	k2eAgFnPc4d1	masová
popravy	poprava	k1gFnPc4	poprava
převážně	převážně	k6eAd1	převážně
polských	polský	k2eAgMnPc2d1	polský
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
Poláků	Polák	k1gMnPc2	Polák
a	a	k8xC	a
Rusů	Rus	k1gMnPc2	Rus
<g/>
,	,	kIx,	,
asi	asi	k9	asi
100	[number]	k4	100
000	[number]	k4	000
obětí	oběť	k1gFnPc2	oběť
Vypálení	vypálení	k1gNnSc2	vypálení
synagog	synagoga	k1gFnPc2	synagoga
v	v	k7c6	v
Rize	Riga	k1gFnSc6	Riga
-	-	kIx~	-
r.	r.	kA	r.
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
akce	akce	k1gFnSc1	akce
proti	proti	k7c3	proti
Židům	Žid	k1gMnPc3	Žid
za	za	k7c2	za
<g />
.	.	kIx.	.
</s>
<s>
nacistické	nacistický	k2eAgFnPc1d1	nacistická
okupace	okupace	k1gFnPc1	okupace
Akce	akce	k1gFnSc2	akce
Dünamünde	Dünamünd	k1gInSc5	Dünamünd
-	-	kIx~	-
r.	r.	kA	r.
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc1	dva
hromadné	hromadný	k2eAgFnPc1d1	hromadná
popravy	poprava	k1gFnPc1	poprava
lotyšských	lotyšský	k2eAgMnPc2d1	lotyšský
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
asi	asi	k9	asi
4	[number]	k4	4
000	[number]	k4	000
obětí	oběť	k1gFnPc2	oběť
Masakry	masakr	k1gInPc1	masakr
v	v	k7c6	v
Jelgavě	Jelgava	k1gFnSc6	Jelgava
-	-	kIx~	-
r.	r.	kA	r.
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
asi	asi	k9	asi
2	[number]	k4	2
000	[number]	k4	000
židovských	židovský	k2eAgFnPc2d1	židovská
obětí	oběť	k1gFnPc2	oběť
Ghetto	ghetto	k1gNnSc4	ghetto
v	v	k7c4	v
Daugavpils	Daugavpils	k1gInSc4	Daugavpils
-	-	kIx~	-
r.	r.	kA	r.
1941	[number]	k4	1941
<g/>
-	-	kIx~	-
<g/>
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
odhady	odhad	k1gInPc1	odhad
počtu	počet	k1gInSc2	počet
obětí	oběť	k1gFnPc2	oběť
okolo	okolo	k7c2	okolo
16	[number]	k4	16
<g />
.	.	kIx.	.
</s>
<s>
000	[number]	k4	000
Židů	Žid	k1gMnPc2	Žid
Masakr	masakr	k1gInSc1	masakr
v	v	k7c6	v
Rumbule	Rumbula	k1gFnSc6	Rumbula
-	-	kIx~	-
r.	r.	kA	r.
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
dvoudenní	dvoudenní	k2eAgFnSc2d1	dvoudenní
masové	masový	k2eAgFnSc2d1	masová
popravy	poprava	k1gFnSc2	poprava
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
asi	asi	k9	asi
25	[number]	k4	25
000	[number]	k4	000
obětí	oběť	k1gFnPc2	oběť
Masakry	masakr	k1gInPc1	masakr
v	v	k7c4	v
Liepā	Liepā	k1gFnSc4	Liepā
-	-	kIx~	-
r.	r.	kA	r.
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
série	série	k1gFnSc1	série
hromadných	hromadný	k2eAgFnPc2d1	hromadná
poprav	poprava	k1gFnPc2	poprava
<g/>
,	,	kIx,	,
asi	asi	k9	asi
5	[number]	k4	5
000	[number]	k4	000
obětí	oběť	k1gFnPc2	oběť
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
Židů	Žid	k1gMnPc2	Žid
Kalevi-Liiva	Kalevi-Liiva	k1gFnSc1	Kalevi-Liiva
-	-	kIx~	-
r.	r.	kA	r.
1941	[number]	k4	1941
<g/>
-	-	kIx~	-
<g/>
1943	[number]	k4	1943
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
hromadné	hromadný	k2eAgFnSc2d1	hromadná
popravy	poprava	k1gFnSc2	poprava
(	(	kIx(	(
<g/>
také	také	k6eAd1	také
československých	československý	k2eAgMnPc2d1	československý
<g/>
)	)	kIx)	)
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
odhad	odhad	k1gInSc1	odhad
3	[number]	k4	3
000	[number]	k4	000
až	až	k9	až
5	[number]	k4	5
000	[number]	k4	000
obětí	oběť	k1gFnPc2	oběť
Ghetto	ghetto	k1gNnSc4	ghetto
Łachwa	Łachwum	k1gNnSc2	Łachwum
-	-	kIx~	-
r.	r.	kA	r.
1941	[number]	k4	1941
<g/>
-	-	kIx~	-
<g/>
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
povstání	povstání	k1gNnSc1	povstání
v	v	k7c6	v
židovském	židovský	k2eAgNnSc6d1	Židovské
ghettu	ghetto	k1gNnSc6	ghetto
<g/>
,	,	kIx,	,
asi	asi	k9	asi
500	[number]	k4	500
obětí	oběť	k1gFnPc2	oběť
Ghetto	ghetto	k1gNnSc4	ghetto
v	v	k7c6	v
Minsku	Minsk	k1gInSc6	Minsk
-	-	kIx~	-
r.	r.	kA	r.
1941	[number]	k4	1941
<g/>
-	-	kIx~	-
<g/>
1943	[number]	k4	1943
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
stotisícové	stotisícový	k2eAgNnSc4d1	stotisícové
židovské	židovský	k2eAgNnSc4d1	Židovské
ghetto	ghetto	k1gNnSc4	ghetto
<g/>
,	,	kIx,	,
asi	asi	k9	asi
20	[number]	k4	20
000	[number]	k4	000
obětí	oběť	k1gFnPc2	oběť
Aféra	aféra	k1gFnSc1	aféra
ve	v	k7c4	v
Slucku	Slucka	k1gFnSc4	Slucka
-	-	kIx~	-
r.	r.	kA	r.
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
dvoudenní	dvoudenní	k2eAgNnSc1d1	dvoudenní
vraždění	vraždění	k1gNnSc1	vraždění
<g/>
,	,	kIx,	,
asi	asi	k9	asi
4	[number]	k4	4
000	[number]	k4	000
převážně	převážně	k6eAd1	převážně
židovských	židovský	k2eAgFnPc2d1	židovská
obětí	oběť	k1gFnPc2	oběť
Masakr	masakr	k1gInSc1	masakr
v	v	k7c6	v
Babím	babí	k2eAgInSc6d1	babí
Jaru	jar	k1gInSc6	jar
-	-	kIx~	-
r.	r.	kA	r.
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
masové	masový	k2eAgNnSc1d1	masové
vyvražďování	vyvražďování	k1gNnSc1	vyvražďování
kyjevských	kyjevský	k2eAgMnPc2d1	kyjevský
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
asi	asi	k9	asi
33	[number]	k4	33
000	[number]	k4	000
obětí	oběť	k1gFnPc2	oběť
Drobickij	Drobickij	k1gFnSc1	Drobickij
Jar	jar	k1gFnSc1	jar
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
r.	r.	kA	r.
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
masový	masový	k2eAgInSc1d1	masový
hrob	hrob	k1gInSc1	hrob
<g/>
,	,	kIx,	,
asi	asi	k9	asi
16	[number]	k4	16
000	[number]	k4	000
obětí	oběť	k1gFnPc2	oběť
Ghetto	ghetto	k1gNnSc1	ghetto
Drohobyč	Drohobyč	k1gMnSc1	Drohobyč
-	-	kIx~	-
r.	r.	kA	r.
1941	[number]	k4	1941
<g/>
-	-	kIx~	-
<g/>
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
židovské	židovský	k2eAgNnSc4d1	Židovské
ghetto	ghetto	k1gNnSc4	ghetto
Masakr	masakr	k1gInSc1	masakr
v	v	k7c6	v
Kamenci	Kamenec	k1gInSc6	Kamenec
Podolském	podolský	k2eAgInSc6d1	podolský
-	-	kIx~	-
r.	r.	kA	r.
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
likvidace	likvidace	k1gFnSc1	likvidace
maďarských	maďarský	k2eAgMnPc2d1	maďarský
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
asi	asi	k9	asi
23	[number]	k4	23
000	[number]	k4	000
obětí	oběť	k1gFnPc2	oběť
Pogromy	pogrom	k1gInPc1	pogrom
ve	v	k7c4	v
Lvově	lvově	k6eAd1	lvově
-	-	kIx~	-
r.	r.	kA	r.
1941	[number]	k4	1941
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
ukrajinskými	ukrajinský	k2eAgMnPc7d1	ukrajinský
nacionalisty	nacionalista	k1gMnPc7	nacionalista
a	a	k8xC	a
Einsatzgruppen	Einsatzgruppen	k2eAgInSc4d1	Einsatzgruppen
zavražděno	zavraždit	k5eAaPmNgNnS	zavraždit
asi	asi	k9	asi
10	[number]	k4	10
000	[number]	k4	000
Židů	Žid	k1gMnPc2	Žid
Ghetto	ghetto	k1gNnSc1	ghetto
v	v	k7c6	v
Mizoči	Mizoč	k1gFnSc6	Mizoč
-	-	kIx~	-
r.	r.	kA	r.
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
postříleno	postřílet	k5eAaPmNgNnS	postřílet
asi	asi	k9	asi
2	[number]	k4	2
000	[number]	k4	000
Židů	Žid	k1gMnPc2	Žid
po	po	k7c6	po
vzpouře	vzpoura	k1gFnSc6	vzpoura
v	v	k7c6	v
Mizoči	Mizoč	k1gFnSc6	Mizoč
<g/>
,	,	kIx,	,
fotografováno	fotografován	k2eAgNnSc4d1	fotografováno
Einsatzgruppen	Einsatzgruppen	k2eAgInSc4d1	Einsatzgruppen
Masakr	masakr	k1gInSc4	masakr
v	v	k7c6	v
Oděse	Oděsa	k1gFnSc6	Oděsa
-	-	kIx~	-
r.	r.	kA	r.
1941	[number]	k4	1941
<g/>
-	-	kIx~	-
<g/>
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
série	série	k1gFnSc1	série
masakrů	masakr	k1gInPc2	masakr
v	v	k7c6	v
Podněstří	Podněstří	k1gFnSc6	Podněstří
<g/>
,	,	kIx,	,
asi	asi	k9	asi
<g />
.	.	kIx.	.
</s>
<s>
30	[number]	k4	30
000	[number]	k4	000
obětí	oběť	k1gFnPc2	oběť
Rokle	rokle	k1gFnSc2	rokle
Petrušino	Petrušin	k2eAgNnSc1d1	Petrušin
-	-	kIx~	-
r.	r.	kA	r.
1941	[number]	k4	1941
<g/>
-	-	kIx~	-
<g/>
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
zastřeleno	zastřelit	k5eAaPmNgNnS	zastřelit
asi	asi	k9	asi
7000	[number]	k4	7000
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
Taganrog	Taganroga	k1gFnPc2	Taganroga
Zmijovskaja	Zmijovskaja	k1gMnSc1	Zmijovskaja
balka	balka	k1gMnSc1	balka
-	-	kIx~	-
r.	r.	kA	r.
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
zavražděno	zavražděn	k2eAgNnSc4d1	zavražděno
27	[number]	k4	27
000	[number]	k4	000
Židů	Žid	k1gMnPc2	Žid
a	a	k8xC	a
sovětských	sovětský	k2eAgMnPc2d1	sovětský
civilistů	civilista	k1gMnPc2	civilista
Richard	Richard	k1gMnSc1	Richard
Rhodes	Rhodes	k1gMnSc1	Rhodes
<g/>
:	:	kIx,	:
Páni	pan	k1gMnPc1	pan
nad	nad	k7c7	nad
smrtí	smrt	k1gFnSc7	smrt
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
303	[number]	k4	303
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7306	[number]	k4	7306
<g/>
-	-	kIx~	-
<g/>
136	[number]	k4	136
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Einsatzgruppen	Einsatzgruppen	k2eAgMnSc1d1	Einsatzgruppen
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
http://www.druhavalkasvetova.euweb.cz/holokaust.htm	[url]	k6eAd1	http://www.druhavalkasvetova.euweb.cz/holokaust.htm
Fašistické	fašistický	k2eAgFnPc4d1	fašistická
represálie	represálie	k1gFnPc4	represálie
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
Fotografie	fotografia	k1gFnSc2	fotografia
dokumentující	dokumentující	k2eAgFnSc2d1	dokumentující
aktivity	aktivita	k1gFnSc2	aktivita
Einsatzgruppen	Einsatzgruppen	k2eAgInSc1d1	Einsatzgruppen
</s>
