<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
jednotky	jednotka	k1gFnPc1	jednotka
SS	SS	kA	SS
Einsatzgruppen	Einsatzgruppen	k2eAgInSc1d1	Einsatzgruppen
určené	určený	k2eAgInPc4d1	určený
pro	pro	k7c4	pro
činnost	činnost	k1gFnSc4	činnost
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
byl	být	k5eAaImAgInS	být
plánován	plánovat	k5eAaImNgInS	plánovat
německý	německý	k2eAgInSc1d1	německý
útok	útok	k1gInSc1	útok
-	-	kIx~	-
operace	operace	k1gFnSc2	operace
Barbarossa	Barbarossa	k1gMnSc1	Barbarossa
<g/>
.	.	kIx.	.
</s>
