<s>
Liparské	Liparský	k2eAgInPc1d1	Liparský
ostrovy	ostrov	k1gInPc1	ostrov
nebo	nebo	k8xC	nebo
též	též	k9	též
Eolské	eolský	k2eAgInPc1d1	eolský
ostrovy	ostrov	k1gInPc1	ostrov
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
<g/>
:	:	kIx,	:
Isole	Isole	k1gFnSc1	Isole
Eolie	Eolie	k1gFnSc1	Eolie
<g/>
)	)	kIx)	)
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Tyrhénském	tyrhénský	k2eAgNnSc6d1	Tyrhénské
moři	moře	k1gNnSc6	moře
u	u	k7c2	u
severního	severní	k2eAgNnSc2d1	severní
pobřeží	pobřeží	k1gNnSc2	pobřeží
Sicílie	Sicílie	k1gFnSc2	Sicílie
v	v	k7c6	v
aktivním	aktivní	k2eAgNnSc6d1	aktivní
vulkanickém	vulkanický	k2eAgNnSc6d1	vulkanické
pásmu	pásmo	k1gNnSc6	pásmo
mezi	mezi	k7c7	mezi
Etnou	Etna	k1gFnSc7	Etna
a	a	k8xC	a
Vesuvem	Vesuv	k1gInSc7	Vesuv
<g/>
.	.	kIx.	.
</s>
