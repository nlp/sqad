<s>
Liparské	Liparský	k2eAgInPc1d1	Liparský
ostrovy	ostrov	k1gInPc1	ostrov
nebo	nebo	k8xC	nebo
též	též	k9	též
Eolské	eolský	k2eAgInPc1d1	eolský
ostrovy	ostrov	k1gInPc1	ostrov
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
<g/>
:	:	kIx,	:
Isole	Isole	k1gFnSc1	Isole
Eolie	Eolie	k1gFnSc1	Eolie
<g/>
)	)	kIx)	)
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Tyrhénském	tyrhénský	k2eAgNnSc6d1	Tyrhénské
moři	moře	k1gNnSc6	moře
u	u	k7c2	u
severního	severní	k2eAgNnSc2d1	severní
pobřeží	pobřeží	k1gNnSc2	pobřeží
Sicílie	Sicílie	k1gFnSc2	Sicílie
v	v	k7c6	v
aktivním	aktivní	k2eAgNnSc6d1	aktivní
vulkanickém	vulkanický	k2eAgNnSc6d1	vulkanické
pásmu	pásmo	k1gNnSc6	pásmo
mezi	mezi	k7c7	mezi
Etnou	Etna	k1gFnSc7	Etna
a	a	k8xC	a
Vesuvem	Vesuv	k1gInSc7	Vesuv
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
sopečné	sopečný	k2eAgInPc1d1	sopečný
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
vrcholky	vrcholek	k1gInPc4	vrcholek
sopek	sopka	k1gFnPc2	sopka
vyčnívající	vyčnívající	k2eAgFnSc1d1	vyčnívající
nad	nad	k7c4	nad
hladinu	hladina	k1gFnSc4	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
italské	italský	k2eAgFnSc2d1	italská
provincie	provincie	k1gFnSc2	provincie
Messina	Messina	k1gFnSc1	Messina
<g/>
,	,	kIx,	,
náležející	náležející	k2eAgMnSc1d1	náležející
k	k	k7c3	k
autonomní	autonomní	k2eAgFnSc3d1	autonomní
oblasti	oblast	k1gFnSc3	oblast
Sicílie	Sicílie	k1gFnSc2	Sicílie
<g/>
.	.	kIx.	.
</s>
<s>
Šest	šest	k4xCc1	šest
ostrovů	ostrov	k1gInPc2	ostrov
-	-	kIx~	-
Vulcano	Vulcana	k1gFnSc5	Vulcana
<g/>
,	,	kIx,	,
Lipari	Lipar	k1gFnSc5	Lipar
<g/>
,	,	kIx,	,
Panarea	Panarea	k1gMnSc1	Panarea
<g/>
,	,	kIx,	,
Stromboli	Strombole	k1gFnSc4	Strombole
<g/>
,	,	kIx,	,
Filicudi	Filicud	k1gMnPc1	Filicud
a	a	k8xC	a
Alicudi	Alicud	k1gMnPc1	Alicud
-	-	kIx~	-
tvoří	tvořit	k5eAaImIp3nS	tvořit
jeden	jeden	k4xCgInSc1	jeden
správní	správní	k2eAgInSc1d1	správní
celek	celek	k1gInSc1	celek
<g/>
,	,	kIx,	,
comune	comun	k1gInSc5	comun
di	di	k?	di
Lipari	Lipar	k1gMnSc3	Lipar
<g/>
.	.	kIx.	.
</s>
<s>
Sedmý	sedmý	k4xOgInSc4	sedmý
ostrov	ostrov	k1gInSc4	ostrov
Salina	salina	k1gFnSc1	salina
s	s	k7c7	s
obcemi	obec	k1gFnPc7	obec
Santa	Sant	k1gMnSc2	Sant
Marina	Marina	k1gFnSc1	Marina
Salina	salina	k1gFnSc1	salina
<g/>
,	,	kIx,	,
Malfa	Malf	k1gMnSc2	Malf
a	a	k8xC	a
Leni	Leni	k?	Leni
je	být	k5eAaImIp3nS	být
samostatný	samostatný	k2eAgInSc1d1	samostatný
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgFnSc4d1	vlastní
správu	správa	k1gFnSc4	správa
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgInPc1d2	menší
ostrůvky	ostrůvek	k1gInPc1	ostrůvek
<g/>
:	:	kIx,	:
Basiluzzo	Basiluzza	k1gFnSc5	Basiluzza
<g/>
,	,	kIx,	,
Dattilo	Dattilo	k1gFnSc5	Dattilo
<g/>
,	,	kIx,	,
Lisca	Lisca	k1gMnSc1	Lisca
Bianca	Bianca	k1gMnSc1	Bianca
<g/>
,	,	kIx,	,
Strombolicchio	Strombolicchio	k6eAd1	Strombolicchio
Liparské	Liparský	k2eAgInPc1d1	Liparský
ostrovy	ostrov	k1gInPc1	ostrov
mají	mít	k5eAaImIp3nP	mít
příjemné	příjemný	k2eAgNnSc4d1	příjemné
středomořské	středomořský	k2eAgNnSc4d1	středomořské
klima	klima	k1gNnSc4	klima
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
pobřeží	pobřeží	k1gNnSc1	pobřeží
je	být	k5eAaImIp3nS	být
členité	členitý	k2eAgNnSc1d1	členité
<g/>
,	,	kIx,	,
plné	plný	k2eAgNnSc1d1	plné
strmých	strmý	k2eAgMnPc2d1	strmý
útesů	útes	k1gInPc2	útes
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
místa	místo	k1gNnPc1	místo
vhodná	vhodný	k2eAgNnPc1d1	vhodné
k	k	k7c3	k
potápění	potápění	k1gNnSc3	potápění
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
málo	málo	k4c1	málo
pláží	pláž	k1gFnPc2	pláž
<g/>
.	.	kIx.	.
</s>
<s>
Krajinný	krajinný	k2eAgInSc4d1	krajinný
ráz	ráz	k1gInSc4	ráz
určují	určovat	k5eAaImIp3nP	určovat
především	především	k9	především
výrazné	výrazný	k2eAgInPc4d1	výrazný
kužele	kužel	k1gInSc2	kužel
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
sopek	sopka	k1gFnPc2	sopka
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
bujná	bujný	k2eAgFnSc1d1	bujná
středomořská	středomořský	k2eAgFnSc1d1	středomořská
vegetace	vegetace	k1gFnSc1	vegetace
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
však	však	k9	však
i	i	k9	i
místa	místo	k1gNnPc1	místo
zcela	zcela	k6eAd1	zcela
holá	holý	k2eAgNnPc1d1	holé
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
světlá	světlý	k2eAgFnSc1d1	světlá
pemza	pemza	k1gFnSc1	pemza
plná	plný	k2eAgFnSc1d1	plná
kousků	kousek	k1gInPc2	kousek
černého	černý	k2eAgInSc2d1	černý
obsidiánu	obsidián	k1gInSc2	obsidián
<g/>
.	.	kIx.	.
</s>
<s>
Bezesporu	bezesporu	k9	bezesporu
největší	veliký	k2eAgFnSc7d3	veliký
přírodní	přírodní	k2eAgFnSc7d1	přírodní
zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
vulkanická	vulkanický	k2eAgFnSc1d1	vulkanická
činnost	činnost	k1gFnSc1	činnost
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
Vulcano	Vulcana	k1gFnSc5	Vulcana
a	a	k8xC	a
Strómboli	Strómbole	k1gFnSc6	Strómbole
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
nejsou	být	k5eNaImIp3nP	být
velké	velký	k2eAgInPc1d1	velký
hotelové	hotelový	k2eAgInPc1d1	hotelový
komplexy	komplex	k1gInPc1	komplex
ani	ani	k8xC	ani
průmyslové	průmyslový	k2eAgInPc1d1	průmyslový
podniky	podnik	k1gInPc1	podnik
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
dolů	dol	k1gInPc2	dol
na	na	k7c4	na
pemzu	pemza	k1gFnSc4	pemza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
stavěné	stavěný	k2eAgInPc1d1	stavěný
penziony	penzion	k1gInPc1	penzion
a	a	k8xC	a
soukromé	soukromý	k2eAgFnPc1d1	soukromá
vily	vila	k1gFnPc1	vila
slouží	sloužit	k5eAaImIp3nP	sloužit
převážně	převážně	k6eAd1	převážně
jako	jako	k9	jako
letní	letní	k2eAgNnPc4d1	letní
sídla	sídlo	k1gNnPc4	sídlo
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
důsledně	důsledně	k6eAd1	důsledně
přidržují	přidržovat	k5eAaImIp3nP	přidržovat
architektonického	architektonický	k2eAgInSc2d1	architektonický
stylu	styl	k1gInSc2	styl
daného	daný	k2eAgInSc2d1	daný
starší	starý	k2eAgNnSc4d2	starší
zástavbou	zástavba	k1gFnSc7	zástavba
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
výstavba	výstavba	k1gFnSc1	výstavba
vede	vést	k5eAaImIp3nS	vést
i	i	k9	i
k	k	k7c3	k
opětovnému	opětovný	k2eAgNnSc3d1	opětovné
osídlování	osídlování	k1gNnSc3	osídlování
odlehlejších	odlehlý	k2eAgFnPc2d2	odlehlejší
částí	část	k1gFnPc2	část
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
v	v	k7c6	v
nedávné	dávný	k2eNgFnSc6d1	nedávná
minulosti	minulost	k1gFnSc6	minulost
opuštěných	opuštěný	k2eAgInPc2d1	opuštěný
místním	místní	k2eAgNnSc7d1	místní
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
stěhujícím	stěhující	k2eAgNnSc7d1	stěhující
se	se	k3xPyFc4	se
do	do	k7c2	do
měst	město	k1gNnPc2	město
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Ostrovy	ostrov	k1gInPc1	ostrov
jsou	být	k5eAaImIp3nP	být
zapsány	zapsat	k5eAaPmNgInP	zapsat
v	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
Světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Ostrovy	ostrov	k1gInPc1	ostrov
byly	být	k5eAaImAgInP	být
osídleny	osídlit	k5eAaPmNgInP	osídlit
koncem	koncem	k7c2	koncem
neolitu	neolit	k1gInSc2	neolit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
zde	zde	k6eAd1	zde
žili	žít	k5eAaImAgMnP	žít
Řekové	Řek	k1gMnPc1	Řek
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
252	[number]	k4	252
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l	l	kA	l
Římané	Říman	k1gMnPc1	Říman
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
zde	zde	k6eAd1	zde
často	často	k6eAd1	často
nacházeli	nacházet	k5eAaImAgMnP	nacházet
útočiště	útočiště	k1gNnSc4	útočiště
piráti	pirát	k1gMnPc1	pirát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Lipari	Lipar	k1gFnSc2	Lipar
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc4	několik
stavebních	stavební	k2eAgFnPc2d1	stavební
památek	památka	k1gFnPc2	památka
z	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
pevnost	pevnost	k1gFnSc1	pevnost
<g/>
,	,	kIx,	,
katedrála	katedrála	k1gFnSc1	katedrála
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vulkanické	vulkanický	k2eAgNnSc4d1	vulkanické
pásmo	pásmo	k1gNnSc4	pásmo
spojující	spojující	k2eAgFnSc4d1	spojující
Etnu	Etna	k1gFnSc4	Etna
s	s	k7c7	s
Vesuvem	Vesuv	k1gInSc7	Vesuv
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
aktivní	aktivní	k2eAgMnSc1d1	aktivní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Tyrhénském	tyrhénský	k2eAgNnSc6d1	Tyrhénské
moři	moře	k1gNnSc6	moře
mezi	mezi	k7c7	mezi
Liparskými	Liparský	k2eAgInPc7d1	Liparský
ostrovy	ostrov	k1gInPc7	ostrov
a	a	k8xC	a
Neapolí	Neapol	k1gFnPc2	Neapol
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
hladinou	hladina	k1gFnSc7	hladina
ukrývají	ukrývat	k5eAaImIp3nP	ukrývat
činné	činný	k2eAgFnPc1d1	činná
sopky	sopka	k1gFnPc1	sopka
nemalých	malý	k2eNgInPc2d1	nemalý
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Stopy	stopa	k1gFnPc1	stopa
vulkanické	vulkanický	k2eAgFnSc2d1	vulkanická
činnosti	činnost	k1gFnSc2	činnost
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
jsou	být	k5eAaImIp3nP	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dva	dva	k4xCgInPc4	dva
miliony	milion	k4xCgInPc4	milion
let	léto	k1gNnPc2	léto
staré	starý	k2eAgFnPc1d1	stará
<g/>
.	.	kIx.	.
</s>
<s>
Stáří	stáří	k1gNnSc1	stáří
samotných	samotný	k2eAgInPc2d1	samotný
ostrovů	ostrov	k1gInPc2	ostrov
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
350	[number]	k4	350
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
nejmladší	mladý	k2eAgFnSc7d3	nejmladší
částí	část	k1gFnSc7	část
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
183	[number]	k4	183
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Vulcanello	Vulcanello	k1gNnSc4	Vulcanello
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
původně	původně	k6eAd1	původně
samostatný	samostatný	k2eAgInSc1d1	samostatný
ostrov	ostrov	k1gInSc1	ostrov
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
spojen	spojit	k5eAaPmNgInS	spojit
proudy	proud	k1gInPc1	proud
lávy	láva	k1gFnSc2	láva
s	s	k7c7	s
ostrovem	ostrov	k1gInSc7	ostrov
Vulcano	Vulcana	k1gFnSc5	Vulcana
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
se	se	k3xPyFc4	se
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
těží	těžet	k5eAaImIp3nS	těžet
pemza	pemza	k1gFnSc1	pemza
a	a	k8xC	a
v	v	k7c6	v
dávné	dávný	k2eAgFnSc6d1	dávná
minulosti	minulost	k1gFnSc6	minulost
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
i	i	k9	i
obsidián	obsidián	k1gInSc1	obsidián
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnSc1	spojení
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
ostrovy	ostrov	k1gInPc7	ostrov
<g/>
,	,	kIx,	,
se	s	k7c7	s
Sicílií	Sicílie	k1gFnSc7	Sicílie
a	a	k8xC	a
s	s	k7c7	s
pevninou	pevnina	k1gFnSc7	pevnina
je	být	k5eAaImIp3nS	být
zajištěno	zajistit	k5eAaPmNgNnS	zajistit
pomocí	pomocí	k7c2	pomocí
trajektů	trajekt	k1gInPc2	trajekt
a	a	k8xC	a
rychlých	rychlý	k2eAgInPc2d1	rychlý
křídlových	křídlový	k2eAgInPc2d1	křídlový
člunů	člun	k1gInPc2	člun
pro	pro	k7c4	pro
osobní	osobní	k2eAgFnSc4d1	osobní
dopravu	doprava	k1gFnSc4	doprava
(	(	kIx(	(
<g/>
Aliscafi	Aliscafe	k1gFnSc4	Aliscafe
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelné	pravidelný	k2eAgFnPc1d1	pravidelná
linky	linka	k1gFnPc1	linka
vyplouvají	vyplouvat	k5eAaImIp3nP	vyplouvat
z	z	k7c2	z
přístavu	přístav	k1gInSc2	přístav
Milazzo	Milazza	k1gFnSc5	Milazza
na	na	k7c6	na
sicilském	sicilský	k2eAgNnSc6d1	sicilské
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Mnohem	mnohem	k6eAd1	mnohem
řidší	řídký	k2eAgNnSc1d2	řidší
je	být	k5eAaImIp3nS	být
spojení	spojení	k1gNnSc1	spojení
s	s	k7c7	s
městy	město	k1gNnPc7	město
Palermo	Palermo	k1gNnSc1	Palermo
na	na	k7c4	na
Sicílii	Sicílie	k1gFnSc4	Sicílie
a	a	k8xC	a
Neapol	Neapol	k1gFnSc4	Neapol
na	na	k7c6	na
Apeninském	apeninský	k2eAgInSc6d1	apeninský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
větších	veliký	k2eAgInPc6d2	veliký
ostrovech	ostrov	k1gInPc6	ostrov
(	(	kIx(	(
<g/>
Lipari	Lipar	k1gFnPc1	Lipar
<g/>
,	,	kIx,	,
Salina	salina	k1gFnSc1	salina
<g/>
,	,	kIx,	,
Vulcano	Vulcana	k1gFnSc5	Vulcana
<g/>
)	)	kIx)	)
funguje	fungovat	k5eAaImIp3nS	fungovat
místní	místní	k2eAgFnSc1d1	místní
autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
na	na	k7c4	na
Alicudi	Alicud	k1gMnPc1	Alicud
a	a	k8xC	a
v	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
Ginostra	Ginostrum	k1gNnSc2	Ginostrum
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Stromboli	Strombole	k1gFnSc4	Strombole
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nejsou	být	k5eNaImIp3nP	být
silnice	silnice	k1gFnPc1	silnice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
schody	schod	k1gInPc4	schod
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
jediným	jediný	k2eAgInSc7d1	jediný
dopravním	dopravní	k2eAgInSc7d1	dopravní
prostředkem	prostředek	k1gInSc7	prostředek
soumaři	soumar	k1gMnPc1	soumar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
turistických	turistický	k2eAgFnPc6d1	turistická
informacích	informace	k1gFnPc6	informace
na	na	k7c6	na
třídě	třída	k1gFnSc6	třída
Vittorio	Vittorio	k6eAd1	Vittorio
Emanuele	Emanuela	k1gFnSc6	Emanuela
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Lipari	Lipar	k1gFnSc2	Lipar
<g/>
,	,	kIx,	,
v	v	k7c6	v
knihkupectvích	knihkupectví	k1gNnPc6	knihkupectví
i	i	k8xC	i
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
trafikách	trafika	k1gFnPc6	trafika
na	na	k7c6	na
Lipari	Lipar	k1gFnSc6	Lipar
a	a	k8xC	a
na	na	k7c6	na
Vulcanu	Vulcan	k1gInSc6	Vulcan
jsou	být	k5eAaImIp3nP	být
běžně	běžně	k6eAd1	běžně
k	k	k7c3	k
dostání	dostání	k1gNnSc3	dostání
podrobné	podrobný	k2eAgFnSc2d1	podrobná
mapy	mapa	k1gFnSc2	mapa
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgFnPc1d1	přesná
informace	informace	k1gFnPc1	informace
o	o	k7c4	o
ubytování	ubytování	k1gNnSc4	ubytování
<g/>
,	,	kIx,	,
autobusové	autobusový	k2eAgFnSc6d1	autobusová
a	a	k8xC	a
lodní	lodní	k2eAgFnSc6d1	lodní
dopravě	doprava	k1gFnSc6	doprava
i	i	k8xC	i
o	o	k7c6	o
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
ostrovech	ostrov	k1gInPc6	ostrov
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
na	na	k7c6	na
oficiálním	oficiální	k2eAgInSc6d1	oficiální
webu	web	k1gInSc6	web
comune	comun	k1gInSc5	comun
di	di	k?	di
Lipari	Lipari	k1gNnPc7	Lipari
a	a	k8xC	a
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
i	i	k9	i
na	na	k7c6	na
četných	četný	k2eAgFnPc6d1	četná
dalších	další	k2eAgFnPc6d1	další
internetových	internetový	k2eAgFnPc6d1	internetová
stránkách	stránka	k1gFnPc6	stránka
<g/>
,	,	kIx,	,
věnovaných	věnovaný	k2eAgFnPc6d1	věnovaná
turismu	turismus	k1gInSc2	turismus
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Turistické	turistický	k2eAgFnPc1d1	turistická
značené	značený	k2eAgFnPc1d1	značená
cesty	cesta	k1gFnPc1	cesta
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
ovšem	ovšem	k9	ovšem
spíše	spíše	k9	spíše
vzácné	vzácný	k2eAgInPc1d1	vzácný
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
ostrovů	ostrov	k1gInPc2	ostrov
Panarea	Panare	k1gInSc2	Panare
a	a	k8xC	a
Stromboli	Strombole	k1gFnSc4	Strombole
<g/>
.	.	kIx.	.
</s>
<s>
Ostrovy	ostrov	k1gInPc1	ostrov
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
příliš	příliš	k6eAd1	příliš
velké	velký	k2eAgFnPc1d1	velká
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
ty	ten	k3xDgFnPc1	ten
největší	veliký	k2eAgFnPc1d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
jako	jako	k9	jako
Lipari	Lipare	k1gFnSc4	Lipare
nebo	nebo	k8xC	nebo
Salinu	salina	k1gFnSc4	salina
<g/>
,	,	kIx,	,
přejde	přejít	k5eAaPmIp3nS	přejít
průměrně	průměrně	k6eAd1	průměrně
zdatný	zdatný	k2eAgMnSc1d1	zdatný
turista	turista	k1gMnSc1	turista
za	za	k7c4	za
jediný	jediný	k2eAgInSc4d1	jediný
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Patrně	patrně	k6eAd1	patrně
nejvýhodnější	výhodný	k2eAgNnSc1d3	nejvýhodnější
místo	místo	k1gNnSc1	místo
pro	pro	k7c4	pro
ubytování	ubytování	k1gNnSc4	ubytování
turisty	turista	k1gMnSc2	turista
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
chce	chtít	k5eAaImIp3nS	chtít
poznávat	poznávat	k5eAaImF	poznávat
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
poloze	poloha	k1gFnSc3	poloha
a	a	k8xC	a
dobrému	dobrý	k2eAgNnSc3d1	dobré
spojení	spojení	k1gNnSc3	spojení
přístav	přístav	k1gInSc1	přístav
Lipari	Lipare	k1gFnSc4	Lipare
na	na	k7c6	na
stejnojmenném	stejnojmenný	k2eAgInSc6d1	stejnojmenný
ostrově	ostrov	k1gInSc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
cestách	cesta	k1gFnPc6	cesta
mezi	mezi	k7c7	mezi
ostrovy	ostrov	k1gInPc7	ostrov
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
mít	mít	k5eAaImF	mít
na	na	k7c6	na
paměti	paměť	k1gFnSc6	paměť
<g/>
,	,	kIx,	,
že	že	k8xS	že
lodní	lodní	k2eAgFnSc1d1	lodní
doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c4	na
počasí	počasí	k1gNnSc4	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
stát	stát	k5eAaPmF	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
člun	člun	k1gInSc1	člun
vynechá	vynechat	k5eAaPmIp3nS	vynechat
některý	některý	k3yIgInSc1	některý
přístav	přístav	k1gInSc1	přístav
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
že	že	k8xS	že
doprava	doprava	k1gFnSc1	doprava
bude	být	k5eAaImBp3nS	být
na	na	k7c4	na
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
třeba	třeba	k6eAd1	třeba
i	i	k9	i
do	do	k7c2	do
druhého	druhý	k4xOgInSc2	druhý
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
přerušena	přerušit	k5eAaPmNgFnS	přerušit
<g/>
.	.	kIx.	.
</s>
<s>
Křídlové	křídlový	k2eAgInPc1d1	křídlový
čluny	člun	k1gInPc1	člun
(	(	kIx(	(
<g/>
Aliscafo	Aliscafo	k6eAd1	Aliscafo
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
zcela	zcela	k6eAd1	zcela
nevhodné	vhodný	k2eNgFnPc1d1	nevhodná
pro	pro	k7c4	pro
osoby	osoba	k1gFnPc4	osoba
trpící	trpící	k2eAgFnPc4d1	trpící
nevolností	nevolnost	k1gFnSc7	nevolnost
v	v	k7c6	v
dopravních	dopravní	k2eAgInPc6d1	dopravní
prostředcích	prostředek	k1gInPc6	prostředek
<g/>
.	.	kIx.	.
</s>
