<s>
Pavel	Pavel	k1gMnSc1	Pavel
Kohout	Kohout	k1gMnSc1	Kohout
(	(	kIx(	(
<g/>
*	*	kIx~	*
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1928	[number]	k4	1928
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
a	a	k8xC	a
rakouský	rakouský	k2eAgMnSc1d1	rakouský
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
představitelů	představitel	k1gMnPc2	představitel
tzv.	tzv.	kA	tzv.
budovatelské	budovatelský	k2eAgFnSc2d1	budovatelská
poezie	poezie	k1gFnSc2	poezie
<g/>
,	,	kIx,	,
prozaik	prozaik	k1gMnSc1	prozaik
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
významný	významný	k2eAgMnSc1d1	významný
spisovatel	spisovatel	k1gMnSc1	spisovatel
samizdatu	samizdat	k1gInSc2	samizdat
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
<g/>
.	.	kIx.	.
</s>
