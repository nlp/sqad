<s>
Pavel	Pavel	k1gMnSc1	Pavel
Kohout	Kohout	k1gMnSc1	Kohout
(	(	kIx(	(
<g/>
*	*	kIx~	*
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1928	[number]	k4	1928
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
a	a	k8xC	a
rakouský	rakouský	k2eAgMnSc1d1	rakouský
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
představitelů	představitel	k1gMnPc2	představitel
tzv.	tzv.	kA	tzv.
budovatelské	budovatelský	k2eAgFnSc2d1	budovatelská
poezie	poezie	k1gFnSc2	poezie
<g/>
,	,	kIx,	,
prozaik	prozaik	k1gMnSc1	prozaik
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
významný	významný	k2eAgMnSc1d1	významný
spisovatel	spisovatel	k1gMnSc1	spisovatel
samizdatu	samizdat	k1gInSc2	samizdat
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
užíval	užívat	k5eAaImAgInS	užívat
své	své	k1gNnSc4	své
vlastní	vlastní	k2eAgFnSc2d1	vlastní
šifry	šifra	k1gFnSc2	šifra
GAL	Gal	k1gMnSc1	Gal
a	a	k8xC	a
veron	veron	k1gMnSc1	veron
<g/>
.	.	kIx.	.
</s>
<s>
Známý	známý	k2eAgInSc1d1	známý
je	být	k5eAaImIp3nS	být
také	také	k9	také
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
prošel	projít	k5eAaPmAgInS	projít
osobnostním	osobnostní	k2eAgInSc7d1	osobnostní
vývojem	vývoj	k1gInSc7	vývoj
<g/>
/	/	kIx~	/
<g/>
přerodem	přerod	k1gInSc7	přerod
od	od	k7c2	od
počáteční	počáteční	k2eAgFnSc2d1	počáteční
bezvýhradné	bezvýhradný	k2eAgFnSc2d1	bezvýhradná
podpory	podpora	k1gFnSc2	podpora
stalinismu	stalinismus	k1gInSc2	stalinismus
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
postupné	postupný	k2eAgNnSc4d1	postupné
vystřízlivění	vystřízlivění	k1gNnSc4	vystřízlivění
až	až	k9	až
po	po	k7c4	po
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
jaru	jar	k1gInSc6	jar
a	a	k8xC	a
nucený	nucený	k2eAgInSc1d1	nucený
odchod	odchod	k1gInSc1	odchod
do	do	k7c2	do
exilu	exil	k1gInSc2	exil
v	v	k7c4	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
patřil	patřit	k5eAaImAgInS	patřit
vždy	vždy	k6eAd1	vždy
mezi	mezi	k7c4	mezi
ty	ten	k3xDgFnPc4	ten
nejaktivnější	aktivní	k2eAgFnPc4d3	nejaktivnější
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
proudu	proud	k1gInSc6	proud
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
třetí	třetí	k4xOgFnSc7	třetí
manželkou	manželka	k1gFnSc7	manželka
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
scenáristka	scenáristka	k1gFnSc1	scenáristka
<g/>
,	,	kIx,	,
spoluautorka	spoluautorka	k1gFnSc1	spoluautorka
jeho	jeho	k3xOp3gInPc2	jeho
scénářů	scénář	k1gInPc2	scénář
<g/>
,	,	kIx,	,
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Jelena	Jelena	k1gFnSc1	Jelena
Mašínová	Mašínová	k1gFnSc1	Mašínová
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
druhého	druhý	k4xOgNnSc2	druhý
manželství	manželství	k1gNnSc2	manželství
s	s	k7c7	s
Annou	Anna	k1gFnSc7	Anna
<g/>
,	,	kIx,	,
asistentkou	asistentka	k1gFnSc7	asistentka
režie	režie	k1gFnSc2	režie
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
spisovatelku	spisovatelka	k1gFnSc4	spisovatelka
Terezu	Tereza	k1gFnSc4	Tereza
Boučkovou	Boučková	k1gFnSc4	Boučková
a	a	k8xC	a
výtvarníka	výtvarník	k1gMnSc4	výtvarník
Ondřeje	Ondřej	k1gMnSc4	Ondřej
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
první	první	k4xOgFnSc7	první
ženou	žena	k1gFnSc7	žena
byla	být	k5eAaImAgFnS	být
herečka	herečka	k1gFnSc1	herečka
Alena	Alena	k1gFnSc1	Alena
Vránová	Vránová	k1gFnSc1	Vránová
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
na	na	k7c4	na
vlastní	vlastní	k2eAgNnSc4d1	vlastní
přání	přání	k1gNnSc4	přání
oženil	oženit	k5eAaPmAgMnS	oženit
v	v	k7c4	v
den	den	k1gInSc4	den
Stalinových	Stalinových	k2eAgFnPc2d1	Stalinových
narozenin	narozeniny	k1gFnPc2	narozeniny
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
maturoval	maturovat	k5eAaBmAgMnS	maturovat
na	na	k7c6	na
reálném	reálný	k2eAgNnSc6d1	reálné
gymnáziu	gymnázium	k1gNnSc6	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
přesvědčeným	přesvědčený	k2eAgMnSc7d1	přesvědčený
komunistou	komunista	k1gMnSc7	komunista
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
členem	člen	k1gMnSc7	člen
ÚV	ÚV	kA	ÚV
ČSM	ČSM	kA	ČSM
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
–	–	k?	–
<g/>
60	[number]	k4	60
<g/>
)	)	kIx)	)
a	a	k8xC	a
členem	člen	k1gMnSc7	člen
ústředního	ústřední	k2eAgInSc2d1	ústřední
výboru	výbor	k1gInSc2	výbor
Svazu	svaz	k1gInSc2	svaz
čs	čs	kA	čs
<g/>
.	.	kIx.	.
spisovatelů	spisovatel	k1gMnPc2	spisovatel
(	(	kIx(	(
<g/>
SČS	SČS	kA	SČS
/	/	kIx~	/
SČSS	SČSS	kA	SČSS
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c4	na
členství	členství	k1gNnSc4	členství
pro	pro	k7c4	pro
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
"	"	kIx"	"
<g/>
kulturněpolitickou	kulturněpolitický	k2eAgFnSc7d1	kulturněpolitická
orientací	orientace	k1gFnSc7	orientace
<g/>
"	"	kIx"	"
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
kulturnímu	kulturní	k2eAgInSc3d1	kulturní
kádru	kádr	k1gInSc3	kádr
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
plynul	plynout	k5eAaImAgInS	plynout
rychlý	rychlý	k2eAgInSc1d1	rychlý
kariérní	kariérní	k2eAgInSc1d1	kariérní
vzestup	vzestup	k1gInSc1	vzestup
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
spoluzaložil	spoluzaložit	k5eAaPmAgInS	spoluzaložit
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
vedl	vést	k5eAaImAgInS	vést
Soubor	soubor	k1gInSc1	soubor
Julia	Julius	k1gMnSc2	Julius
Fučíka	Fučík	k1gMnSc2	Fučík
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
maturitě	maturita	k1gFnSc6	maturita
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
mládežnické	mládežnický	k2eAgFnSc6d1	mládežnická
redakci	redakce	k1gFnSc6	redakce
Čs	čs	kA	čs
<g/>
.	.	kIx.	.
rozhlasu	rozhlas	k1gInSc2	rozhlas
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
–	–	k?	–
<g/>
49	[number]	k4	49
<g/>
)	)	kIx)	)
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
Kynclem	Kyncl	k1gMnSc7	Kyncl
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
jedenadvacetiletý	jedenadvacetiletý	k2eAgMnSc1d1	jedenadvacetiletý
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
kulturního	kulturní	k2eAgMnSc2d1	kulturní
atašé	atašé	k1gMnSc2	atašé
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
byl	být	k5eAaImAgMnS	být
šéfredaktorem	šéfredaktor	k1gMnSc7	šéfredaktor
satirického	satirický	k2eAgInSc2d1	satirický
časopisu	časopis	k1gInSc2	časopis
Dikobraz	dikobraz	k1gMnSc1	dikobraz
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
–	–	k?	–
<g/>
52	[number]	k4	52
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
dokončil	dokončit	k5eAaPmAgInS	dokončit
studium	studium	k1gNnSc4	studium
estetiky	estetika	k1gFnSc2	estetika
a	a	k8xC	a
divadelní	divadelní	k2eAgFnSc2d1	divadelní
vědy	věda	k1gFnSc2	věda
na	na	k7c6	na
Universitě	universita	k1gFnSc6	universita
Karlově	Karlův	k2eAgFnSc6d1	Karlova
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
začal	začít	k5eAaPmAgInS	začít
literárně	literárně	k6eAd1	literárně
tvořit	tvořit	k5eAaImF	tvořit
(	(	kIx(	(
<g/>
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
budovatelského	budovatelský	k2eAgNnSc2d1	budovatelské
nadšení	nadšení	k1gNnSc2	nadšení
–	–	k?	–
dramata	drama	k1gNnPc4	drama
a	a	k8xC	a
poezii	poezie	k1gFnSc4	poezie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
napsal	napsat	k5eAaBmAgInS	napsat
asi	asi	k9	asi
45	[number]	k4	45
divadelních	divadelní	k2eAgFnPc2d1	divadelní
her	hra	k1gFnPc2	hra
–	–	k?	–
aktovek	aktovka	k1gFnPc2	aktovka
a	a	k8xC	a
adaptací	adaptace	k1gFnPc2	adaptace
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
vojenské	vojenský	k2eAgFnSc2d1	vojenská
služby	služba	k1gFnSc2	služba
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
–	–	k?	–
<g/>
54	[number]	k4	54
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
vedoucím	vedoucí	k2eAgMnSc7d1	vedoucí
redaktorem	redaktor	k1gMnSc7	redaktor
kulturní	kulturní	k2eAgFnSc2d1	kulturní
rubriky	rubrika	k1gFnSc2	rubrika
časopisu	časopis	k1gInSc2	časopis
Čs	čs	kA	čs
<g/>
.	.	kIx.	.
voják	voják	k1gMnSc1	voják
a	a	k8xC	a
členem	člen	k1gMnSc7	člen
Armádního	armádní	k2eAgInSc2d1	armádní
uměleckého	umělecký	k2eAgInSc2d1	umělecký
souboru	soubor	k1gInSc2	soubor
Víta	Vít	k1gMnSc2	Vít
Nejedlého	Nejedlý	k1gMnSc2	Nejedlý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
byl	být	k5eAaImAgMnS	být
několik	několik	k4yIc1	několik
měsíců	měsíc	k1gInPc2	měsíc
reportérem	reportér	k1gMnSc7	reportér
a	a	k8xC	a
komentátorem	komentátor	k1gMnSc7	komentátor
vnitropolitické	vnitropolitický	k2eAgFnSc2d1	vnitropolitická
redakce	redakce	k1gFnSc2	redakce
Československé	československý	k2eAgFnSc2d1	Československá
televize	televize	k1gFnSc2	televize
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
spisovatelem	spisovatel	k1gMnSc7	spisovatel
z	z	k7c2	z
povolání	povolání	k1gNnSc2	povolání
<g/>
,	,	kIx,	,
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
různými	různý	k2eAgNnPc7d1	různé
divadly	divadlo	k1gNnPc7	divadlo
–	–	k?	–
mj.	mj.	kA	mj.
byl	být	k5eAaImAgInS	být
dramaturgem	dramaturg	k1gMnSc7	dramaturg
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
na	na	k7c6	na
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
–	–	k?	–
<g/>
66	[number]	k4	66
<g/>
)	)	kIx)	)
–	–	k?	–
a	a	k8xC	a
začínal	začínat	k5eAaImAgMnS	začínat
se	se	k3xPyFc4	se
přiklánět	přiklánět	k5eAaImF	přiklánět
k	k	k7c3	k
reformním	reformní	k2eAgMnPc3d1	reformní
komunistům	komunista	k1gMnPc3	komunista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1967	[number]	k4	1967
na	na	k7c4	na
IV	IV	kA	IV
<g/>
.	.	kIx.	.
sjezdu	sjezd	k1gInSc6	sjezd
SČSS	SČSS	kA	SČSS
demonstrativně	demonstrativně	k6eAd1	demonstrativně
přečetl	přečíst	k5eAaPmAgInS	přečíst
Solženicynův	Solženicynův	k2eAgInSc1d1	Solženicynův
protestní	protestní	k2eAgInSc1d1	protestní
dopis	dopis	k1gInSc1	dopis
sjezdu	sjezd	k1gInSc2	sjezd
Svazu	svaz	k1gInSc2	svaz
sovětských	sovětský	k2eAgMnPc2d1	sovětský
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
dovršil	dovršit	k5eAaPmAgMnS	dovršit
svůj	svůj	k3xOyFgInSc4	svůj
obrat	obrat	k1gInSc4	obrat
v	v	k7c4	v
kritika	kritik	k1gMnSc4	kritik
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
(	(	kIx(	(
<g/>
Ivan	Ivan	k1gMnSc1	Ivan
Klíma	Klíma	k1gMnSc1	Klíma
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
J.	J.	kA	J.
Liehm	Liehm	k1gMnSc1	Liehm
<g/>
,	,	kIx,	,
Ludvík	Ludvík	k1gMnSc1	Ludvík
Vaculík	Vaculík	k1gMnSc1	Vaculík
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
)	)	kIx)	)
podroben	podroben	k2eAgInSc1d1	podroben
stranickému	stranický	k2eAgNnSc3d1	stranické
disciplinárnímu	disciplinární	k2eAgNnSc3d1	disciplinární
řízení	řízení	k1gNnSc3	řízení
<g/>
.	.	kIx.	.
</s>
<s>
Podporoval	podporovat	k5eAaImAgMnS	podporovat
Pražské	pražský	k2eAgNnSc4d1	Pražské
jaro	jaro	k1gNnSc4	jaro
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
předsedou	předseda	k1gMnSc7	předseda
KSČ	KSČ	kA	KSČ
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
pobočce	pobočka	k1gFnSc6	pobočka
SČS	SČS	kA	SČS
a	a	k8xC	a
stal	stát	k5eAaPmAgInS	stát
se	se	k3xPyFc4	se
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejvýraznějších	výrazný	k2eAgFnPc2d3	nejvýraznější
osobností	osobnost	k1gFnPc2	osobnost
tzv.	tzv.	kA	tzv.
pražského	pražský	k2eAgNnSc2d1	Pražské
jara	jaro	k1gNnSc2	jaro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
byl	být	k5eAaImAgMnS	být
z	z	k7c2	z
KSČ	KSČ	kA	KSČ
i	i	k9	i
ze	z	k7c2	z
SČS	SČS	kA	SČS
vyloučen	vyloučit	k5eAaPmNgInS	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
ocitl	ocitnout	k5eAaPmAgMnS	ocitnout
na	na	k7c6	na
indexu	index	k1gInSc6	index
a	a	k8xC	a
jeho	jeho	k3xOp3gNnPc1	jeho
díla	dílo	k1gNnPc1	dílo
byla	být	k5eAaImAgNnP	být
cenzurována	cenzurovat	k5eAaImNgNnP	cenzurovat
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
jeho	jeho	k3xOp3gNnSc4	jeho
nepřátelství	nepřátelství	k1gNnSc4	nepřátelství
k	k	k7c3	k
režimu	režim	k1gInSc3	režim
ještě	ještě	k9	ještě
prohloubilo	prohloubit	k5eAaPmAgNnS	prohloubit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stál	stát	k5eAaImAgMnS	stát
také	také	k9	také
u	u	k7c2	u
zrodu	zrod	k1gInSc2	zrod
Charty	charta	k1gFnSc2	charta
77	[number]	k4	77
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yRnSc4	což
byl	být	k5eAaImAgInS	být
StB	StB	k1gFnSc3	StB
otevřeně	otevřeně	k6eAd1	otevřeně
šikanován	šikanovat	k5eAaImNgInS	šikanovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
povoleno	povolit	k5eAaPmNgNnS	povolit
vyjet	vyjet	k5eAaPmF	vyjet
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
na	na	k7c4	na
pracovní	pracovní	k2eAgInSc4d1	pracovní
pobyt	pobyt	k1gInSc4	pobyt
do	do	k7c2	do
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měl	mít	k5eAaImAgMnS	mít
roční	roční	k2eAgFnSc4d1	roční
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Burgtheatrem	Burgtheatr	k1gInSc7	Burgtheatr
<g/>
.	.	kIx.	.
</s>
<s>
Návrat	návrat	k1gInSc1	návrat
jim	on	k3xPp3gMnPc3	on
ale	ale	k9	ale
byl	být	k5eAaImAgInS	být
znemožněn	znemožnit	k5eAaPmNgMnS	znemožnit
a	a	k8xC	a
oba	dva	k4xCgMnPc1	dva
byli	být	k5eAaImAgMnP	být
zbaveni	zbaven	k2eAgMnPc1d1	zbaven
čs	čs	kA	čs
<g/>
.	.	kIx.	.
občanství	občanství	k1gNnSc4	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Usadili	usadit	k5eAaPmAgMnP	usadit
se	se	k3xPyFc4	se
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
dostali	dostat	k5eAaPmAgMnP	dostat
občanství	občanství	k1gNnSc4	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Žili	žít	k5eAaImAgMnP	žít
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
a	a	k8xC	a
často	často	k6eAd1	často
navštěvovali	navštěvovat	k5eAaImAgMnP	navštěvovat
Prahu	Praha	k1gFnSc4	Praha
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
na	na	k7c6	na
Sázavě	Sázava	k1gFnSc6	Sázava
<g/>
.	.	kIx.	.
</s>
<s>
Verše	verš	k1gInPc4	verš
a	a	k8xC	a
písně	píseň	k1gFnPc4	píseň
<g/>
,	,	kIx,	,
1952	[number]	k4	1952
–	–	k?	–
bás.	bás.	k?	bás.
sbírka	sbírka	k1gFnSc1	sbírka
propagující	propagující	k2eAgFnSc1d1	propagující
socialismus	socialismus	k1gInSc4	socialismus
Tři	tři	k4xCgFnPc4	tři
knihy	kniha	k1gFnPc4	kniha
veršů	verš	k1gInPc2	verš
<g/>
,	,	kIx,	,
1953	[number]	k4	1953
Čas	čas	k1gInSc1	čas
lásky	láska	k1gFnSc2	láska
a	a	k8xC	a
boje	boj	k1gInSc2	boj
<g/>
,	,	kIx,	,
1954	[number]	k4	1954
–	–	k?	–
bás.	bás.	k?	bás.
sbírka	sbírka	k1gFnSc1	sbírka
<g/>
,	,	kIx,	,
budovatelská	budovatelský	k2eAgFnSc1d1	budovatelská
poezie	poezie	k1gFnSc1	poezie
Dobrá	dobrý	k2eAgFnSc1d1	dobrá
píseň	píseň	k1gFnSc1	píseň
<g/>
,	,	kIx,	,
1952	[number]	k4	1952
–	–	k?	–
drama	drama	k1gNnSc1	drama
(	(	kIx(	(
<g/>
veršovaná	veršovaný	k2eAgFnSc1d1	veršovaná
komedie	komedie	k1gFnSc1	komedie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prorežimní	prorežimní	k2eAgFnSc1d1	prorežimní
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c1	mnoho
vtipných	vtipný	k2eAgFnPc2d1	vtipná
scén	scéna	k1gFnPc2	scéna
<g/>
;	;	kIx,	;
muž	muž	k1gMnSc1	muž
jde	jít	k5eAaImIp3nS	jít
místo	místo	k1gNnSc4	místo
svatební	svatební	k2eAgFnSc2d1	svatební
noci	noc	k1gFnSc2	noc
do	do	k7c2	do
ocelárny	ocelárna	k1gFnSc2	ocelárna
<g/>
;	;	kIx,	;
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
velmi	velmi	k6eAd1	velmi
hraná	hraný	k2eAgFnSc1d1	hraná
<g/>
.	.	kIx.	.
</s>
<s>
Zářijové	zářijový	k2eAgFnPc4d1	zářijová
noci	noc	k1gFnPc4	noc
<g/>
,	,	kIx,	,
1955	[number]	k4	1955
–	–	k?	–
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
z	z	k7c2	z
vojenského	vojenský	k2eAgNnSc2d1	vojenské
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
;	;	kIx,	;
už	už	k6eAd1	už
mírný	mírný	k2eAgInSc1d1	mírný
odvrat	odvrat	k1gInSc1	odvrat
od	od	k7c2	od
ideologie	ideologie	k1gFnSc2	ideologie
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
rozpor	rozpor	k1gInSc1	rozpor
teorie	teorie	k1gFnSc2	teorie
a	a	k8xC	a
praxe	praxe	k1gFnSc2	praxe
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Chudáček	chudáček	k1gMnSc1	chudáček
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
–	–	k?	–
drama	drama	k1gFnSc1	drama
<g/>
;	;	kIx,	;
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
před	před	k7c7	před
premiérou	premiéra	k1gFnSc7	premiéra
<g/>
.	.	kIx.	.
</s>
<s>
Sbohem	sbohem	k0	sbohem
<g/>
,	,	kIx,	,
smutku	smutek	k1gInSc2	smutek
<g/>
,	,	kIx,	,
1957	[number]	k4	1957
Taková	takový	k3xDgFnSc1	takový
láska	láska	k1gFnSc1	láska
<g/>
,	,	kIx,	,
1957	[number]	k4	1957
–	–	k?	–
tato	tento	k3xDgFnSc1	tento
hra	hra	k1gFnSc1	hra
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejúspěšnějším	úspěšný	k2eAgNnPc3d3	nejúspěšnější
<g/>
,	,	kIx,	,
hrála	hrát	k5eAaImAgNnP	hrát
se	se	k3xPyFc4	se
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Říkali	říkat	k5eAaImAgMnP	říkat
mi	já	k3xPp1nSc3	já
soudruhu	soudruh	k1gMnSc3	soudruh
<g/>
,	,	kIx,	,
1960	[number]	k4	1960
–	–	k?	–
drama	drama	k1gNnSc4	drama
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
po	po	k7c6	po
sedmé	sedmý	k4xOgFnSc6	sedmý
repríze	repríza	k1gFnSc6	repríza
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
sestra	sestra	k1gFnSc1	sestra
<g/>
,	,	kIx,	,
1960	[number]	k4	1960
Cesta	cesta	k1gFnSc1	cesta
kolem	kolem	k7c2	kolem
světa	svět	k1gInSc2	svět
za	za	k7c4	za
80	[number]	k4	80
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
1962	[number]	k4	1962
–	–	k?	–
divadelní	divadelní	k2eAgFnSc2d1	divadelní
adaptace	adaptace	k1gFnSc2	adaptace
knihy	kniha	k1gFnSc2	kniha
Julese	Julese	k1gFnSc2	Julese
Vernea	Verne	k1gInSc2	Verne
<g/>
.	.	kIx.	.
</s>
<s>
Dvanáct	dvanáct	k4xCc1	dvanáct
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
–	–	k?	–
příběh	příběh	k1gInSc1	příběh
dvanácti	dvanáct	k4xCc2	dvanáct
čerstvých	čerstvý	k2eAgMnPc2d1	čerstvý
absolventů	absolvent	k1gMnPc2	absolvent
AMU	AMU	kA	AMU
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Švejk	Švejk	k1gMnSc1	Švejk
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
–	–	k?	–
dramatizace	dramatizace	k1gFnPc1	dramatizace
částí	část	k1gFnPc2	část
slavného	slavný	k2eAgInSc2d1	slavný
Haškova	Haškův	k2eAgInSc2d1	Haškův
románu	román	k1gInSc2	román
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
s	s	k7c7	s
mloky	mlok	k1gMnPc7	mlok
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
–	–	k?	–
divadelní	divadelní	k2eAgFnSc2d1	divadelní
adaptace	adaptace	k1gFnSc2	adaptace
Čapkovy	Čapkův	k2eAgFnSc2d1	Čapkova
Války	válka	k1gFnSc2	válka
s	s	k7c7	s
mloky	mlok	k1gMnPc7	mlok
<g/>
.	.	kIx.	.
</s>
<s>
Řazeno	řadit	k5eAaImNgNnS	řadit
do	do	k7c2	do
SF	SF	kA	SF
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
na	na	k7c4	na
Biskaj	Biskaj	k1gFnSc4	Biskaj
<g/>
,	,	kIx,	,
1965	[number]	k4	1965
August	August	k1gMnSc1	August
August	August	k1gMnSc1	August
<g/>
,	,	kIx,	,
august	august	k1gMnSc1	august
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
–	–	k?	–
cirkusové	cirkusový	k2eAgNnSc4d1	cirkusové
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
je	být	k5eAaImIp3nS	být
smutný	smutný	k2eAgMnSc1d1	smutný
klaun	klaun	k1gMnSc1	klaun
August	August	k1gMnSc1	August
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
má	mít	k5eAaImIp3nS	mít
sen	sen	k1gInSc4	sen
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
drezírovat	drezírovat	k5eAaImF	drezírovat
8	[number]	k4	8
bílých	bílý	k2eAgMnPc2d1	bílý
lipicánů	lipicán	k1gMnPc2	lipicán
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
proto	proto	k8xC	proto
pustí	pustit	k5eAaPmIp3nS	pustit
tygry	tygr	k1gMnPc4	tygr
(	(	kIx(	(
<g/>
=	=	kIx~	=
komunistické	komunistický	k2eAgNnSc1d1	komunistické
hledisko	hledisko	k1gNnSc1	hledisko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Briefe	Brief	k1gMnSc5	Brief
über	über	k1gMnSc1	über
die	die	k?	die
Grenze	Grenze	k1gFnSc2	Grenze
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
Nenávist	nenávist	k1gFnSc1	nenávist
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
Aksál	Aksála	k1gFnPc2	Aksála
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
Válka	válka	k1gFnSc1	válka
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
poschodí	poschodí	k1gNnSc6	poschodí
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
–	–	k?	–
absurdní	absurdní	k2eAgNnSc4d1	absurdní
drama	drama	k1gNnSc4	drama
<g/>
.	.	kIx.	.
</s>
<s>
Pech	Pech	k1gMnSc1	Pech
pod	pod	k7c7	pod
střechou	střecha	k1gFnSc7	střecha
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
Ubohý	ubohý	k2eAgMnSc1d1	ubohý
vrah	vrah	k1gMnSc1	vrah
<g/>
:	:	kIx,	:
Na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
povídky	povídka	k1gFnSc2	povídka
Rozum	rozum	k1gInSc1	rozum
Leonida	Leonid	k1gMnSc2	Leonid
Andrejeva	Andrejev	k1gMnSc2	Andrejev
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
–	–	k?	–
divadelní	divadelní	k2eAgFnSc2d1	divadelní
adaptace	adaptace	k1gFnSc2	adaptace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
premiéru	premiéra	k1gFnSc4	premiéra
na	na	k7c6	na
Broadwayi	Broadwayi	k1gNnSc6	Broadwayi
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
–	–	k?	–
divadelní	divadelní	k2eAgFnSc2d1	divadelní
adaptace	adaptace	k1gFnSc2	adaptace
Kafkova	Kafkův	k2eAgInSc2d1	Kafkův
románu	román	k1gInSc2	román
(	(	kIx(	(
<g/>
spoluautor	spoluautor	k1gMnSc1	spoluautor
Ivan	Ivan	k1gMnSc1	Ivan
Klíma	Klíma	k1gMnSc1	Klíma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Požár	požár	k1gInSc1	požár
v	v	k7c6	v
suterénu	suterén	k1gInSc6	suterén
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
absurdní	absurdní	k2eAgNnSc4d1	absurdní
drama	drama	k1gNnSc4	drama
Život	život	k1gInSc1	život
v	v	k7c6	v
tichém	tichý	k2eAgInSc6d1	tichý
domě	dům	k1gInSc6	dům
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
absurdní	absurdní	k2eAgNnSc4d1	absurdní
drama	drama	k1gNnSc4	drama
Ruleta	ruleta	k1gFnSc1	ruleta
<g/>
:	:	kIx,	:
Na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
povídky	povídka	k1gFnSc2	povídka
Tma	tma	k6eAd1	tma
Leonida	Leonid	k1gMnSc2	Leonid
Andrejeva	Andrejev	k1gMnSc2	Andrejev
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
–	–	k?	–
divadelní	divadelní	k2eAgFnSc2d1	divadelní
adaptace	adaptace	k1gFnSc2	adaptace
<g/>
.	.	kIx.	.
</s>
<s>
Atest	atest	k1gInSc1	atest
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
–	–	k?	–
jednoaktovka	jednoaktovka	k1gFnSc1	jednoaktovka
<g/>
,	,	kIx,	,
hl.	hl.	k?	hl.
<g/>
p.	p.	k?	p.
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Vaněk	Vaněk	k1gMnSc1	Vaněk
postižený	postižený	k2eAgInSc4d1	postižený
režimem	režim	k1gInSc7	režim
chce	chtít	k5eAaImIp3nS	chtít
pro	pro	k7c4	pro
svého	svůj	k3xOyFgMnSc4	svůj
psa	pes	k1gMnSc4	pes
atest	atest	k1gInSc4	atest
a	a	k8xC	a
přihlásit	přihlásit	k5eAaPmF	přihlásit
ho	on	k3xPp3gInSc4	on
do	do	k7c2	do
chovatelství	chovatelství	k1gNnSc2	chovatelství
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ale	ale	k8xC	ale
nelze	lze	k6eNd1	lze
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
majitel	majitel	k1gMnSc1	majitel
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
špatný	špatný	k2eAgInSc4d1	špatný
kádrový	kádrový	k2eAgInSc4d1	kádrový
posudek	posudek	k1gInSc4	posudek
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
zápasí	zápasit	k5eAaImIp3nS	zápasit
s	s	k7c7	s
anděly	anděl	k1gMnPc7	anděl
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
–	–	k?	–
jednoaktovka	jednoaktovka	k1gFnSc1	jednoaktovka
o	o	k7c6	o
životě	život	k1gInSc6	život
Vlasty	Vlasta	k1gFnSc2	Vlasta
Chramostové	Chramostová	k1gFnSc2	Chramostová
<g/>
.	.	kIx.	.
</s>
<s>
Marast	marast	k1gInSc1	marast
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
Ecce	Ecce	k1gFnSc1	Ecce
Constantia	Constantia	k1gFnSc1	Constantia
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
–	–	k?	–
"	"	kIx"	"
<g/>
přímý	přímý	k2eAgInSc1d1	přímý
přenos	přenos	k1gInSc1	přenos
<g/>
"	"	kIx"	"
z	z	k7c2	z
koncilu	koncil	k1gInSc2	koncil
v	v	k7c6	v
Kostnici	Kostnice	k1gFnSc6	Kostnice
<g/>
.	.	kIx.	.
</s>
<s>
Kyanid	kyanid	k1gInSc1	kyanid
o	o	k7c4	o
páté	pátá	k1gFnPc4	pátá
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
Šest	šest	k4xCc1	šest
a	a	k8xC	a
sex	sex	k1gInSc4	sex
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
–	–	k?	–
sedm	sedm	k4xCc1	sedm
jednoaktových	jednoaktový	k2eAgNnPc2d1	jednoaktové
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Nuly	nula	k1gFnPc1	nula
<g/>
,	,	kIx,	,
prem	prem	k1gInSc1	prem
<g/>
.	.	kIx.	.
2000	[number]	k4	2000
Malá	malý	k2eAgFnSc1d1	malá
hudba	hudba	k1gFnSc1	hudba
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
–	–	k?	–
napsáno	napsat	k5eAaBmNgNnS	napsat
německy	německy	k6eAd1	německy
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Eine	Ein	k1gInSc2	Ein
kleine	kleinout	k5eAaPmIp3nS	kleinout
machtmusik	machtmusik	k1gInSc1	machtmusik
k	k	k7c3	k
mozartovskému	mozartovský	k2eAgInSc3d1	mozartovský
jubilejnímu	jubilejní	k2eAgInSc3d1	jubilejní
roku	rok	k1gInSc3	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
černém	černé	k1gNnSc6	černé
a	a	k8xC	a
bílém	bílé	k1gNnSc6	bílé
<g/>
,	,	kIx,	,
1950	[number]	k4	1950
–	–	k?	–
šest	šest	k4xCc1	šest
povídek	povídka	k1gFnPc2	povídka
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
podávajících	podávající	k2eAgInPc2d1	podávající
pohádkové	pohádkový	k2eAgInPc4d1	pohádkový
motivy	motiv	k1gInPc4	motiv
v	v	k7c6	v
komunisticko-budovatelském	komunistickoudovatelský	k2eAgMnSc6d1	komunisticko-budovatelský
duchu	duch	k1gMnSc6	duch
<g/>
:	:	kIx,	:
O	o	k7c6	o
černém	černé	k1gNnSc6	černé
a	a	k8xC	a
bílém	bílé	k1gNnSc6	bílé
(	(	kIx(	(
<g/>
o	o	k7c6	o
rasismu	rasismus	k1gInSc6	rasismus
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
O	o	k7c6	o
hloupém	hloupý	k2eAgMnSc6d1	hloupý
Honzovi	Honz	k1gMnSc6	Honz
(	(	kIx(	(
<g/>
Honza	Honza	k1gMnSc1	Honza
přestoupí	přestoupit	k5eAaPmIp3nS	přestoupit
od	od	k7c2	od
kulaka	kulak	k1gMnSc2	kulak
do	do	k7c2	do
JZD	JZD	kA	JZD
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
O	o	k7c6	o
třinácti	třináct	k4xCc6	třináct
rudých	rudý	k2eAgFnPc6d1	rudá
růžích	růž	k1gFnPc6	růž
(	(	kIx(	(
<g/>
dívky	dívka	k1gFnPc1	dívka
se	se	k3xPyFc4	se
obětují	obětovat	k5eAaBmIp3nP	obětovat
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
Francovi	Franc	k1gMnSc3	Franc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
O	o	k7c6	o
pionýrském	pionýrský	k2eAgInSc6d1	pionýrský
šátku	šátek	k1gInSc6	šátek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
plakal	plakat	k5eAaImAgInS	plakat
<g/>
,	,	kIx,	,
O	o	k7c6	o
srdci	srdce	k1gNnSc6	srdce
uralského	uralský	k2eAgMnSc2d1	uralský
chlapce	chlapec	k1gMnSc2	chlapec
(	(	kIx(	(
<g/>
ruský	ruský	k2eAgMnSc1d1	ruský
Ivan	Ivan	k1gMnSc1	Ivan
obětuje	obětovat	k5eAaBmIp3nS	obětovat
svá	svůj	k3xOyFgNnPc4	svůj
srdce	srdce	k1gNnPc4	srdce
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
osvobodit	osvobodit	k5eAaPmF	osvobodit
Rusko	Rusko	k1gNnSc4	Rusko
i	i	k8xC	i
Prahu	Praha	k1gFnSc4	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
čtyřech	čtyři	k4xCgMnPc6	čtyři
chlapcích	chlapec	k1gMnPc6	chlapec
a	a	k8xC	a
soudruhu	soudruh	k1gMnSc6	soudruh
Gottwaldovi	Gottwald	k1gMnSc6	Gottwald
(	(	kIx(	(
<g/>
dobrý	dobrý	k2eAgMnSc1d1	dobrý
muž	muž	k1gMnSc1	muž
s	s	k7c7	s
dýmkou	dýmka	k1gFnSc7	dýmka
doporučí	doporučit	k5eAaPmIp3nS	doporučit
chlapcům	chlapec	k1gMnPc3	chlapec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
místo	místo	k1gNnSc4	místo
u	u	k7c2	u
cirkusu	cirkus	k1gInSc2	cirkus
hledali	hledat	k5eAaImAgMnP	hledat
práci	práce	k1gFnSc4	práce
mezi	mezi	k7c4	mezi
dělníky	dělník	k1gMnPc4	dělník
<g/>
)	)	kIx)	)
Z	z	k7c2	z
deníku	deník	k1gInSc2	deník
kontrarevolucionáře	kontrarevolucionář	k1gMnSc2	kontrarevolucionář
aneb	aneb	k?	aneb
Životy	život	k1gInPc1	život
od	od	k7c2	od
tanku	tank	k1gInSc2	tank
k	k	k7c3	k
tanku	tank	k1gInSc2	tank
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
–	–	k?	–
prozaické	prozaický	k2eAgNnSc1d1	prozaické
dílo	dílo	k1gNnSc1	dílo
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc1	první
české	český	k2eAgNnSc1d1	české
vydání	vydání	k1gNnSc1	vydání
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
shrnutí	shrnutí	k1gNnSc4	shrnutí
životních	životní	k2eAgInPc2d1	životní
osudů	osud	k1gInPc2	osud
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
kniha	kniha	k1gFnSc1	kniha
o	o	k7c6	o
kauze	kauza	k1gFnSc6	kauza
Adam	Adam	k1gMnSc1	Adam
Juráček	Juráček	k1gMnSc1	Juráček
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
tělocviku	tělocvik	k1gInSc2	tělocvik
a	a	k8xC	a
kreslení	kreslení	k1gNnSc2	kreslení
na	na	k7c6	na
Pedagogické	pedagogický	k2eAgFnSc6d1	pedagogická
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
K.	K.	kA	K.
<g/>
,	,	kIx,	,
kontra	kontra	k2eAgMnSc1d1	kontra
Sir	sir	k1gMnSc1	sir
Isaac	Isaac	k1gFnSc4	Isaac
Newton	Newton	k1gMnSc1	Newton
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
fyziky	fyzika	k1gFnSc2	fyzika
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Cambridge	Cambridge	k1gFnSc1	Cambridge
–	–	k?	–
podle	podle	k7c2	podle
dobových	dobový	k2eAgInPc2d1	dobový
materiálů	materiál	k1gInPc2	materiál
rekonstruoval	rekonstruovat	k5eAaBmAgMnS	rekonstruovat
a	a	k8xC	a
nejzajímavějšími	zajímavý	k2eAgInPc7d3	nejzajímavější
dokumenty	dokument	k1gInPc7	dokument
doplnil	doplnit	k5eAaPmAgMnS	doplnit
P.	P.	kA	P.
K.	K.	kA	K.
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
v	v	k7c6	v
samizdatu	samizdat	k1gInSc6	samizdat
–	–	k?	–
groteskní	groteskní	k2eAgInSc1d1	groteskní
román	román	k1gInSc1	román
<g />
.	.	kIx.	.
</s>
<s>
psaný	psaný	k2eAgMnSc1d1	psaný
formou	forma	k1gFnSc7	forma
koláže	koláž	k1gFnSc2	koláž
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
zápisy	zápis	k1gInPc1	zápis
<g/>
,	,	kIx,	,
protokoly	protokol	k1gInPc1	protokol
<g/>
,	,	kIx,	,
prohlášení	prohlášení	k1gNnSc1	prohlášení
aj.	aj.	kA	aj.
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
je	být	k5eAaImIp3nS	být
Adam	Adam	k1gMnSc1	Adam
Juráček	Juráček	k1gMnSc1	Juráček
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
chce	chtít	k5eAaImIp3nS	chtít
okouzlit	okouzlit	k5eAaPmF	okouzlit
krásnou	krásný	k2eAgFnSc4d1	krásná
Kateřinu	Kateřina	k1gFnSc4	Kateřina
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
se	se	k3xPyFc4	se
cvičil	cvičit	k5eAaImAgInS	cvičit
v	v	k7c6	v
soustřeďování	soustřeďování	k1gNnSc6	soustřeďování
myšlenek	myšlenka	k1gFnPc2	myšlenka
a	a	k8xC	a
naučil	naučit	k5eAaPmAgMnS	naučit
se	se	k3xPyFc4	se
chodit	chodit	k5eAaImF	chodit
po	po	k7c6	po
stropě	strop	k1gInSc6	strop
<g/>
,	,	kIx,	,
stranický	stranický	k2eAgInSc1d1	stranický
aparát	aparát	k1gInSc1	aparát
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc4	ten
snaží	snažit	k5eAaImIp3nS	snažit
zatajit	zatajit	k5eAaPmF	zatajit
a	a	k8xC	a
zakázat	zakázat	k5eAaPmF	zakázat
<g/>
,	,	kIx,	,
úřady	úřad	k1gInPc1	úřad
ho	on	k3xPp3gMnSc4	on
obviní	obvinit	k5eAaPmIp3nP	obvinit
z	z	k7c2	z
trestného	trestný	k2eAgInSc2d1	trestný
činu	čin	k1gInSc2	čin
a	a	k8xC	a
Adam	Adam	k1gMnSc1	Adam
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
zblázní	zbláznit	k5eAaPmIp3nS	zbláznit
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
se	se	k3xPyFc4	se
vysmívá	vysmívat	k5eAaImIp3nS	vysmívat
komunistickým	komunistický	k2eAgFnPc3d1	komunistická
praktikám	praktika	k1gFnPc3	praktika
<g/>
.	.	kIx.	.
</s>
<s>
Katyně	Katyně	k1gFnSc1	Katyně
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
–	–	k?	–
román	román	k1gInSc1	román
o	o	k7c6	o
dospívající	dospívající	k2eAgFnSc6d1	dospívající
dívce	dívka	k1gFnSc6	dívka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nedostane	dostat	k5eNaPmIp3nS	dostat
na	na	k7c4	na
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
začne	začít	k5eAaPmIp3nS	začít
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
speciální	speciální	k2eAgInSc4d1	speciální
humanitní	humanitní	k2eAgInSc4d1	humanitní
obor	obor	k1gInSc4	obor
s	s	k7c7	s
maturitou	maturita	k1gFnSc7	maturita
–	–	k?	–
školu	škola	k1gFnSc4	škola
pro	pro	k7c4	pro
katy	kat	k1gMnPc4	kat
<g/>
.	.	kIx.	.
</s>
<s>
Náplní	náplň	k1gFnSc7	náplň
studia	studio	k1gNnSc2	studio
jsou	být	k5eAaImIp3nP	být
popisy	popis	k1gInPc1	popis
mučení	mučení	k1gNnSc1	mučení
<g/>
,	,	kIx,	,
historie	historie	k1gFnSc1	historie
popravování	popravování	k1gNnSc2	popravování
aj.	aj.	kA	aj.
<g/>
;	;	kIx,	;
humoristický	humoristický	k2eAgInSc1d1	humoristický
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
lidské	lidský	k2eAgFnPc4d1	lidská
krutosti	krutost	k1gFnPc4	krutost
a	a	k8xC	a
praktiky	praktika	k1gFnPc4	praktika
totalitní	totalitní	k2eAgFnSc2d1	totalitní
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
také	také	k9	také
nebezpečnou	bezpečný	k2eNgFnSc4d1	nebezpečná
schopnost	schopnost	k1gFnSc4	schopnost
lidí	člověk	k1gMnPc2	člověk
smířit	smířit	k5eAaPmF	smířit
se	se	k3xPyFc4	se
s	s	k7c7	s
absurdní	absurdní	k2eAgFnSc7d1	absurdní
situací	situace	k1gFnSc7	situace
<g/>
;	;	kIx,	;
hodně	hodně	k6eAd1	hodně
oceňován	oceňovat	k5eAaImNgMnS	oceňovat
a	a	k8xC	a
vydán	vydat	k5eAaPmNgInS	vydat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Nápady	nápad	k1gInPc1	nápad
svaté	svatý	k2eAgFnSc2d1	svatá
Kláry	Klára	k1gFnSc2	Klára
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
(	(	kIx(	(
<g/>
první	první	k4xOgNnPc4	první
vydání	vydání	k1gNnPc4	vydání
u	u	k7c2	u
Sixty-Eight	Sixty-Eighta	k1gFnPc2	Sixty-Eighta
Publishers	Publishersa	k1gFnPc2	Publishersa
<g/>
)	)	kIx)	)
–	–	k?	–
bizarně	bizarně	k6eAd1	bizarně
alegorický	alegorický	k2eAgInSc4d1	alegorický
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
satiricky	satiricky	k6eAd1	satiricky
pohlížející	pohlížející	k2eAgInPc1d1	pohlížející
na	na	k7c4	na
totalitní	totalitní	k2eAgInPc4d1	totalitní
úřady	úřad	k1gInPc4	úřad
<g/>
;	;	kIx,	;
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
postava	postava	k1gFnSc1	postava
je	být	k5eAaImIp3nS	být
žákyně	žákyně	k1gFnSc1	žákyně
8	[number]	k4	8
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
obdařená	obdařený	k2eAgFnSc1d1	obdařená
schopností	schopnost	k1gFnSc7	schopnost
jasnovidectví	jasnovidectví	k1gNnSc4	jasnovidectví
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
zfilmován	zfilmovat	k5eAaPmNgMnS	zfilmovat
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
režiséry	režisér	k1gMnPc7	režisér
Ari	Ari	k1gMnSc7	Ari
Folmanem	Folman	k1gMnSc7	Folman
a	a	k8xC	a
Ori	Ori	k1gMnSc7	Ori
Sivanem	Sivan	k1gMnSc7	Sivan
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Svatá	svatý	k2eAgFnSc1d1	svatá
Klára	Klára	k1gFnSc1	Klára
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
:	:	kIx,	:
ק	ק	k?	ק
ה	ה	k?	ה
<g/>
,	,	kIx,	,
Klara	Klara	k1gMnSc1	Klara
ha-kedoša	haedoša	k1gMnSc1	ha-kedoša
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
zakopán	zakopán	k2eAgMnSc1d1	zakopán
pes	pes	k1gMnSc1	pes
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
–	–	k?	–
memoárový	memoárový	k2eAgInSc4d1	memoárový
román	román	k1gInSc4	román
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
autor	autor	k1gMnSc1	autor
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
vlastních	vlastní	k2eAgFnPc2d1	vlastní
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
praktiky	praktika	k1gFnPc4	praktika
StB	StB	k1gFnPc7	StB
v	v	k7c6	v
době	doba	k1gFnSc6	doba
normalizace	normalizace	k1gFnSc2	normalizace
<g/>
;	;	kIx,	;
okolnosti	okolnost	k1gFnPc1	okolnost
kolem	kolem	k7c2	kolem
vzniku	vznik	k1gInSc2	vznik
Charty	charta	k1gFnSc2	charta
77	[number]	k4	77
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
podle	podle	k7c2	podle
jezevčíka	jezevčík	k1gMnSc2	jezevčík
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
Kohoutovým	Kohoutův	k2eAgInSc7d1	Kohoutův
právě	právě	k6eAd1	právě
StB	StB	k1gMnPc4	StB
otrávila	otrávit	k5eAaPmAgFnS	otrávit
<g/>
.	.	kIx.	.
</s>
<s>
Hodina	hodina	k1gFnSc1	hodina
tance	tanec	k1gInSc2	tanec
a	a	k8xC	a
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
–	–	k?	–
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
představující	představující	k2eAgInSc1d1	představující
v	v	k7c6	v
Kohoutově	Kohoutův	k2eAgFnSc6d1	Kohoutova
tvorbě	tvorba	k1gFnSc6	tvorba
zcela	zcela	k6eAd1	zcela
nové	nový	k2eAgNnSc1d1	nové
téma	téma	k1gNnSc1	téma
–	–	k?	–
téma	téma	k1gNnSc1	téma
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
;	;	kIx,	;
příběh	příběh	k1gInSc1	příběh
z	z	k7c2	z
terezínské	terezínský	k2eAgFnSc2d1	Terezínská
Malé	Malé	k2eAgFnSc2d1	Malé
pevnosti	pevnost	k1gFnSc2	pevnost
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zároveň	zároveň	k6eAd1	zároveň
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
může	moct	k5eAaImIp3nS	moct
u	u	k7c2	u
mladých	mladý	k2eAgMnPc2d1	mladý
lidí	člověk	k1gMnPc2	člověk
způsobit	způsobit	k5eAaPmF	způsobit
vymývání	vymývání	k1gNnSc4	vymývání
mozků	mozek	k1gInPc2	mozek
<g/>
;	;	kIx,	;
mladá	mladý	k2eAgFnSc1d1	mladá
<g />
.	.	kIx.	.
</s>
<s>
dívka	dívka	k1gFnSc1	dívka
Kristina	Kristina	k1gFnSc1	Kristina
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
dcerou	dcera	k1gFnSc7	dcera
velitele	velitel	k1gMnSc2	velitel
tábora	tábor	k1gInSc2	tábor
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
ze	z	k7c2	z
školy	škola	k1gFnSc2	škola
za	za	k7c7	za
otcem	otec	k1gMnSc7	otec
do	do	k7c2	do
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
do	do	k7c2	do
mladého	mladý	k2eAgMnSc2d1	mladý
příslušníka	příslušník	k1gMnSc2	příslušník
SS	SS	kA	SS
–	–	k?	–
ona	onen	k3xDgFnSc1	onen
je	být	k5eAaImIp3nS	být
naivní	naivní	k2eAgFnSc1d1	naivní
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
je	být	k5eAaImIp3nS	být
uvědomělý	uvědomělý	k2eAgMnSc1d1	uvědomělý
mladý	mladý	k2eAgMnSc1d1	mladý
árijec	árijec	k1gMnSc1	árijec
<g/>
;	;	kIx,	;
připojují	připojovat	k5eAaImIp3nP	připojovat
se	se	k3xPyFc4	se
osudy	osud	k1gInPc7	osud
židů	žid	k1gMnPc2	žid
(	(	kIx(	(
<g/>
otec	otec	k1gMnSc1	otec
jí	jíst	k5eAaImIp3nS	jíst
přivede	přivést	k5eAaPmIp3nS	přivést
mladou	mladý	k2eAgFnSc4d1	mladá
židovskou	židovská	k1gFnSc4	židovská
<g />
.	.	kIx.	.
</s>
<s>
baletku	baletka	k1gFnSc4	baletka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
učila	učít	k5eAaPmAgFnS	učít
tančit	tančit	k5eAaImF	tančit
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
dívka	dívka	k1gFnSc1	dívka
stále	stále	k6eAd1	stále
víc	hodně	k6eAd2	hodně
chápe	chápat	k5eAaImIp3nS	chápat
poměry	poměr	k1gInPc4	poměr
v	v	k7c6	v
táboře	tábor	k1gInSc6	tábor
<g/>
;	;	kIx,	;
onu	onen	k3xDgFnSc4	onen
mladou	mladý	k2eAgFnSc4d1	mladá
židovku	židovka	k1gFnSc4	židovka
znásilní	znásilnit	k5eAaPmIp3nS	znásilnit
Kristinin	Kristinin	k2eAgInSc1d1	Kristinin
vyhlédnutý	vyhlédnutý	k2eAgInSc1d1	vyhlédnutý
esesák	esesák	k?	esesák
a	a	k8xC	a
nechá	nechat	k5eAaPmIp3nS	nechat
ji	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
utopila	utopit	k5eAaPmAgFnS	utopit
(	(	kIx(	(
<g/>
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
se	se	k3xPyFc4	se
Kristina	Kristina	k1gFnSc1	Kristina
nedozví	dozvědět	k5eNaPmIp3nS	dozvědět
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
příběh	příběh	k1gInSc1	příběh
končí	končit	k5eAaImIp3nS	končit
osvobozením	osvobození	k1gNnSc7	osvobození
tábora	tábor	k1gInSc2	tábor
<g/>
;	;	kIx,	;
zfilmován	zfilmován	k2eAgMnSc1d1	zfilmován
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
Viktorem	Viktor	k1gMnSc7	Viktor
Polesným	polesný	k1gMnSc7	polesný
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
velkých	velký	k2eAgFnPc2d1	velká
prázdnin	prázdniny	k1gFnPc2	prázdniny
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
–	–	k?	–
rozsáhlé	rozsáhlý	k2eAgNnSc4d1	rozsáhlé
zpracování	zpracování	k1gNnSc4	zpracování
látky	látka	k1gFnSc2	látka
české	český	k2eAgFnSc2d1	Česká
emigrace	emigrace	k1gFnSc2	emigrace
<g/>
;	;	kIx,	;
natočeno	natočen	k2eAgNnSc1d1	natočeno
Miloslavem	Miloslav	k1gMnSc7	Miloslav
Lutherem	Luther	k1gMnSc7	Luther
jako	jako	k8xS	jako
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sněžím	sněžit	k5eAaImIp1nS	sněžit
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
–	–	k?	–
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
aktuální	aktuální	k2eAgFnSc7d1	aktuální
politickou	politický	k2eAgFnSc7d1	politická
situací	situace	k1gFnSc7	situace
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
;	;	kIx,	;
téma	téma	k1gNnSc4	téma
lustrací	lustrace	k1gFnPc2	lustrace
<g/>
.	.	kIx.	.
</s>
<s>
Klaun	klaun	k1gMnSc1	klaun
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
–	–	k?	–
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Michalem	Michal	k1gMnSc7	Michal
Pavlíčkem	Pavlíček	k1gMnSc7	Pavlíček
znovu	znovu	k6eAd1	znovu
zpracované	zpracovaný	k2eAgNnSc4d1	zpracované
téma	téma	k1gNnSc4	téma
hry	hra	k1gFnSc2	hra
August	August	k1gMnSc1	August
<g/>
,	,	kIx,	,
August	August	k1gMnSc1	August
<g/>
,	,	kIx,	,
August	August	k1gMnSc1	August
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
hodina	hodina	k1gFnSc1	hodina
vrahů	vrah	k1gMnPc2	vrah
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
–	–	k?	–
román	román	k1gInSc1	román
zpracovávající	zpracovávající	k2eAgMnSc1d1	zpracovávající
poslední	poslední	k2eAgInPc4d1	poslední
týdny	týden	k1gInPc4	týden
okupace	okupace	k1gFnSc2	okupace
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
detektivního	detektivní	k2eAgInSc2d1	detektivní
thrilleru	thriller	k1gInSc2	thriller
<g/>
.	.	kIx.	.
</s>
<s>
Pat	pat	k1gInSc1	pat
aneb	aneb	k?	aneb
Hra	hra	k1gFnSc1	hra
králů	král	k1gMnPc2	král
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
Ta	ten	k3xDgFnSc1	ten
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
vlna	vlna	k1gFnSc1	vlna
za	za	k7c7	za
kýlem	kýl	k1gInSc7	kýl
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
Smyčka	smyčka	k1gFnSc1	smyčka
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
O	O	kA	O
ničem	ničema	k1gFnPc2	ničema
a	a	k8xC	a
o	o	k7c6	o
všem	všecek	k3xTgNnSc6	všecek
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
Cizinec	cizinec	k1gMnSc1	cizinec
a	a	k8xC	a
Krásná	krásný	k2eAgFnSc1d1	krásná
paní	paní	k1gFnSc1	paní
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
Můj	můj	k3xOp1gInSc1	můj
život	život	k1gInSc4	život
s	s	k7c7	s
Hitlerem	Hitler	k1gMnSc7	Hitler
<g/>
,	,	kIx,	,
Stalinem	Stalin	k1gMnSc7	Stalin
a	a	k8xC	a
Havlem	Havel	k1gMnSc7	Havel
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
Tango	tango	k1gNnSc1	tango
Mortale	Mortal	k1gMnSc5	Mortal
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
1965	[number]	k4	1965
–	–	k?	–
Svatba	svatba	k1gFnSc1	svatba
s	s	k7c7	s
podmínkou	podmínka	k1gFnSc7	podmínka
1965	[number]	k4	1965
–	–	k?	–
7	[number]	k4	7
zabitých	zabitý	k2eAgFnPc2d1	zabitá
1983	[number]	k4	1983
–	–	k?	–
Ucho	ucho	k1gNnSc1	ucho
</s>
