<s>
Tlak	tlak	k1gInSc1	tlak
je	být	k5eAaImIp3nS	být
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
veličina	veličina	k1gFnSc1	veličina
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
označovaná	označovaný	k2eAgFnSc1d1	označovaná
symbolem	symbol	k1gInSc7	symbol
p	p	k?	p
nebo	nebo	k8xC	nebo
P	P	kA	P
(	(	kIx(	(
<g/>
z	z	k7c2	z
latinského	latinský	k2eAgMnSc2d1	latinský
pressura	pressur	k1gMnSc2	pressur
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyjadřující	vyjadřující	k2eAgInSc1d1	vyjadřující
poměr	poměr	k1gInSc1	poměr
velikosti	velikost	k1gFnSc2	velikost
síly	síla	k1gFnSc2	síla
F	F	kA	F
<g/>
,	,	kIx,	,
působící	působící	k2eAgMnSc1d1	působící
kolmo	kolmo	k6eAd1	kolmo
na	na	k7c4	na
rovinnou	rovinný	k2eAgFnSc4d1	rovinná
plochu	plocha	k1gFnSc4	plocha
a	a	k8xC	a
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
spojitě	spojitě	k6eAd1	spojitě
rozloženou	rozložený	k2eAgFnSc7d1	rozložená
po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
ploše	plocha	k1gFnSc6	plocha
<g/>
,	,	kIx,	,
a	a	k8xC	a
obsahu	obsah	k1gInSc2	obsah
této	tento	k3xDgFnSc2	tento
plochy	plocha	k1gFnSc2	plocha
S	s	k7c7	s
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
F	F	kA	F
S	s	k7c7	s
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
S	s	k7c7	s
<g/>
}}}	}}}	k?	}}}
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
síla	síla	k1gFnSc1	síla
F	F	kA	F
rozložena	rozložen	k2eAgFnSc1d1	rozložena
na	na	k7c6	na
dané	daný	k2eAgFnSc6d1	daná
ploše	plocha	k1gFnSc6	plocha
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
veličinu	veličina	k1gFnSc4	veličina
p	p	k?	p
<g/>
,	,	kIx,	,
danou	daný	k2eAgFnSc4d1	daná
předchozím	předchozí	k2eAgInSc7d1	předchozí
vzorcem	vzorec	k1gInSc7	vzorec
<g/>
,	,	kIx,	,
nazýváme	nazývat	k5eAaImIp1nP	nazývat
střední	střední	k2eAgInSc4d1	střední
tlak	tlak	k1gInSc4	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Místním	místní	k2eAgInSc7d1	místní
tlakem	tlak	k1gInSc7	tlak
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
tlakem	tlak	k1gInSc7	tlak
působícím	působící	k2eAgInSc7d1	působící
v	v	k7c6	v
nějakém	nějaký	k3yIgInSc6	nějaký
bodě	bod	k1gInSc6	bod
uvažované	uvažovaný	k2eAgFnSc2d1	uvažovaná
plochy	plocha	k1gFnSc2	plocha
rozumíme	rozumět	k5eAaImIp1nP	rozumět
diferenciální	diferenciální	k2eAgInSc4d1	diferenciální
podíl	podíl	k1gInSc4	podíl
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
F	F	kA	F
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
F	F	kA	F
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
S	s	k7c7	s
<g/>
}}}	}}}	k?	}}}
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
vnější	vnější	k2eAgFnSc1d1	vnější
síla	síla	k1gFnSc1	síla
působí	působit	k5eAaImIp3nS	působit
pouze	pouze	k6eAd1	pouze
kolmo	kolmo	k6eAd1	kolmo
(	(	kIx(	(
<g/>
neexistuje	existovat	k5eNaImIp3nS	existovat
žádná	žádný	k3yNgFnSc1	žádný
tečná	tečný	k2eAgFnSc1d1	tečná
složka	složka	k1gFnSc1	složka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tlak	tlak	k1gInSc1	tlak
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xC	jako
prostý	prostý	k2eAgInSc1d1	prostý
(	(	kIx(	(
<g/>
čistý	čistý	k2eAgInSc1d1	čistý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zobecněnou	zobecněný	k2eAgFnSc4d1	zobecněná
definici	definice	k1gFnSc4	definice
tlaku	tlak	k1gInSc2	tlak
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
síly	síla	k1gFnPc4	síla
působící	působící	k2eAgInSc1d1	působící
libovolným	libovolný	k2eAgInSc7d1	libovolný
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
obecně	obecně	k6eAd1	obecně
nerovinnou	rovinný	k2eNgFnSc4d1	rovinný
plochu	plocha	k1gFnSc4	plocha
<g/>
)	)	kIx)	)
můžeme	moct	k5eAaImIp1nP	moct
ve	v	k7c6	v
vektorovém	vektorový	k2eAgInSc6d1	vektorový
tvaru	tvar	k1gInSc6	tvar
zapsat	zapsat	k5eAaPmF	zapsat
rovnicí	rovnice	k1gFnSc7	rovnice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
F	F	kA	F
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
p	p	k?	p
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
s	s	k7c7	s
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
p	p	k?	p
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
S	s	k7c7	s
<g/>
}	}	kIx)	}
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
F	F	kA	F
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
s	s	k7c7	s
<g/>
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
složka	složka	k1gFnSc1	složka
vektoru	vektor	k1gInSc2	vektor
síly	síla	k1gFnSc2	síla
a	a	k8xC	a
kolmá	kolmý	k2eAgFnSc1d1	kolmá
k	k	k7c3	k
elementu	element	k1gInSc3	element
plochy	plocha	k1gFnSc2	plocha
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
S	s	k7c7	s
<g/>
}	}	kIx)	}
}	}	kIx)	}
na	na	k7c4	na
který	který	k3yQgInSc4	který
působí	působit	k5eAaImIp3nS	působit
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
směr	směr	k1gInSc1	směr
vektoru	vektor	k1gInSc2	vektor
popisujícího	popisující	k2eAgInSc2d1	popisující
element	element	k1gInSc1	element
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
S	s	k7c7	s
<g/>
}	}	kIx)	}
}	}	kIx)	}
má	mít	k5eAaImIp3nS	mít
směr	směr	k1gInSc4	směr
normály	normála	k1gFnSc2	normála
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
plošce	ploška	k1gFnSc3	ploška
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
existuje	existovat	k5eAaImIp3nS	existovat
reálná	reálný	k2eAgFnSc1d1	reálná
tečná	tečný	k2eAgFnSc1d1	tečná
složka	složka	k1gFnSc1	složka
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
však	však	k9	však
pojem	pojem	k1gInSc1	pojem
tlaku	tlak	k1gInSc2	tlak
zpravidla	zpravidla	k6eAd1	zpravidla
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
obecnější	obecní	k2eAgFnSc7d2	obecní
tenzorovou	tenzorový	k2eAgFnSc7d1	tenzorová
veličinou	veličina	k1gFnSc7	veličina
mechanické	mechanický	k2eAgNnSc4d1	mechanické
napětí	napětí	k1gNnSc4	napětí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stlačitelných	stlačitelný	k2eAgFnPc6d1	stlačitelná
látkách	látka	k1gFnPc6	látka
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
tlak	tlak	k1gInSc1	tlak
deformaci	deformace	k1gFnSc4	deformace
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
působení	působení	k1gNnSc6	působení
tlakové	tlakový	k2eAgFnSc2d1	tlaková
síly	síla	k1gFnSc2	síla
na	na	k7c4	na
pevné	pevný	k2eAgNnSc4d1	pevné
těleso	těleso	k1gNnSc4	těleso
se	se	k3xPyFc4	se
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
tah	tah	k1gInSc1	tah
a	a	k8xC	a
tlak	tlak	k1gInSc1	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Tahová	tahový	k2eAgFnSc1d1	tahová
síla	síla	k1gFnSc1	síla
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
roztahování	roztahování	k1gNnSc4	roztahování
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
tlaková	tlakový	k2eAgFnSc1d1	tlaková
jeho	jeho	k3xOp3gNnSc4	jeho
stlačování	stlačování	k1gNnSc4	stlačování
<g/>
.	.	kIx.	.
</s>
<s>
Tahové	tahový	k2eAgFnPc1d1	tahová
a	a	k8xC	a
tlakové	tlakový	k2eAgFnPc1d1	tlaková
síly	síla	k1gFnPc1	síla
se	se	k3xPyFc4	se
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
pouze	pouze	k6eAd1	pouze
směrem	směr	k1gInSc7	směr
působení	působení	k1gNnSc2	působení
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
způsobené	způsobený	k2eAgFnPc1d1	způsobená
deformace	deformace	k1gFnPc1	deformace
jsou	být	k5eAaImIp3nP	být
obdobné	obdobný	k2eAgFnPc1d1	obdobná
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
s	s	k7c7	s
opačnými	opačný	k2eAgNnPc7d1	opačné
znaménky	znaménko	k1gNnPc7	znaménko
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
tlak	tlak	k1gInSc4	tlak
v	v	k7c6	v
nějaké	nějaký	k3yIgFnSc6	nějaký
uzavřené	uzavřený	k2eAgFnSc6d1	uzavřená
nádobě	nádoba	k1gFnSc6	nádoba
větší	veliký	k2eAgInSc1d2	veliký
než	než	k8xS	než
tlak	tlak	k1gInSc1	tlak
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
okolí	okolí	k1gNnSc6	okolí
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nádobě	nádoba	k1gFnSc6	nádoba
přetlak	přetlak	k1gInSc4	přetlak
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
naopak	naopak	k6eAd1	naopak
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
tlak	tlak	k1gInSc4	tlak
nižší	nízký	k2eAgFnPc1d2	nižší
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nádobě	nádoba	k1gFnSc6	nádoba
podtlak	podtlak	k1gInSc4	podtlak
<g/>
.	.	kIx.	.
</s>
<s>
Přetlaku	přetlak	k1gInSc2	přetlak
a	a	k8xC	a
podtlaku	podtlak	k1gInSc2	podtlak
lze	lze	k6eAd1	lze
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
přesunem	přesun	k1gInSc7	přesun
části	část	k1gFnSc2	část
hmoty	hmota	k1gFnSc2	hmota
do	do	k7c2	do
nebo	nebo	k8xC	nebo
z	z	k7c2	z
uzavřené	uzavřený	k2eAgFnSc2d1	uzavřená
nádoby	nádoba	k1gFnSc2	nádoba
<g/>
,	,	kIx,	,
změnou	změna	k1gFnSc7	změna
její	její	k3xOp3gFnSc2	její
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
velikosti	velikost	k1gFnSc2	velikost
nebo	nebo	k8xC	nebo
teploty	teplota	k1gFnPc1	teplota
jejího	její	k3xOp3gInSc2	její
obsahu	obsah	k1gInSc2	obsah
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
přesun	přesun	k1gInSc4	přesun
části	část	k1gFnSc2	část
obsahu	obsah	k1gInSc2	obsah
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
pumpy	pumpa	k1gFnPc1	pumpa
<g/>
.	.	kIx.	.
</s>
<s>
Sací	sací	k2eAgFnSc1d1	sací
pumpa	pumpa	k1gFnSc1	pumpa
pro	pro	k7c4	pro
odčerpání	odčerpání	k1gNnSc4	odčerpání
plynů	plyn	k1gInPc2	plyn
z	z	k7c2	z
uzavřené	uzavřený	k2eAgFnSc2d1	uzavřená
nádoby	nádoba	k1gFnSc2	nádoba
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
vývěva	vývěva	k1gFnSc1	vývěva
<g/>
.	.	kIx.	.
</s>
<s>
Tlak	tlak	k1gInSc1	tlak
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
let	let	k1gInSc1	let
vyjadřoval	vyjadřovat	k5eAaImAgInS	vyjadřovat
velkým	velký	k2eAgInSc7d1	velký
počtem	počet	k1gInSc7	počet
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
platná	platný	k2eAgFnSc1d1	platná
jednotka	jednotka	k1gFnSc1	jednotka
odvozená	odvozený	k2eAgFnSc1d1	odvozená
z	z	k7c2	z
jednotek	jednotka	k1gFnPc2	jednotka
SI	si	k1gNnSc2	si
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
pascal	pascal	k1gInSc1	pascal
<g/>
.	.	kIx.	.
</s>
<s>
Pascal	pascal	k1gInSc1	pascal
je	být	k5eAaImIp3nS	být
tlak	tlak	k1gInSc4	tlak
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
síla	síla	k1gFnSc1	síla
jednoho	jeden	k4xCgInSc2	jeden
newtonu	newton	k1gInSc2	newton
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
rozložená	rozložený	k2eAgFnSc1d1	rozložená
na	na	k7c6	na
rovinné	rovinný	k2eAgFnSc6d1	rovinná
ploše	plocha	k1gFnSc6	plocha
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
jednoho	jeden	k4xCgInSc2	jeden
čtverečního	čtvereční	k2eAgInSc2d1	čtvereční
metru	metr	k1gInSc2	metr
<g/>
,	,	kIx,	,
kolmé	kolmý	k2eAgInPc4d1	kolmý
ke	k	k7c3	k
směru	směr	k1gInSc3	směr
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
praktické	praktický	k2eAgNnSc4d1	praktické
použití	použití	k1gNnSc4	použití
v	v	k7c6	v
technické	technický	k2eAgFnSc6d1	technická
praxi	praxe	k1gFnSc6	praxe
je	být	k5eAaImIp3nS	být
jednotka	jednotka	k1gFnSc1	jednotka
1	[number]	k4	1
Pa	Pa	kA	Pa
velmi	velmi	k6eAd1	velmi
malá	malý	k2eAgFnSc1d1	malá
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
používáme	používat	k5eAaImIp1nP	používat
jednotky	jednotka	k1gFnPc4	jednotka
násobné	násobný	k2eAgFnPc4d1	násobná
kPa	kPa	k?	kPa
<g/>
,	,	kIx,	,
MPa	MPa	k1gFnSc1	MPa
<g/>
.	.	kIx.	.
</s>
<s>
Tlak	tlak	k1gInSc1	tlak
v	v	k7c6	v
plynech	plyn	k1gInPc6	plyn
je	být	k5eAaImIp3nS	být
vyvoláván	vyvolávat	k5eAaImNgInS	vyvolávat
tepelným	tepelný	k2eAgInSc7d1	tepelný
pohybem	pohyb	k1gInSc7	pohyb
částic	částice	k1gFnPc2	částice
plynu	plynout	k5eAaImIp1nS	plynout
(	(	kIx(	(
<g/>
atomů	atom	k1gInPc2	atom
nebo	nebo	k8xC	nebo
molekul	molekula	k1gFnPc2	molekula
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
nárazy	náraz	k1gInPc7	náraz
těchto	tento	k3xDgFnPc2	tento
částic	částice	k1gFnPc2	částice
na	na	k7c4	na
stěny	stěna	k1gFnPc4	stěna
nádoby	nádoba	k1gFnSc2	nádoba
se	se	k3xPyFc4	se
projevují	projevovat	k5eAaImIp3nP	projevovat
tlakem	tlak	k1gInSc7	tlak
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
působícím	působící	k2eAgNnSc6d1	působící
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
tomu	ten	k3xDgMnSc3	ten
je	být	k5eAaImIp3nS	být
i	i	k9	i
v	v	k7c6	v
kapalinách	kapalina	k1gFnPc6	kapalina
<g/>
.	.	kIx.	.
</s>
<s>
Tlak	tlak	k1gInSc1	tlak
působí	působit	k5eAaImIp3nS	působit
i	i	k9	i
v	v	k7c6	v
pevných	pevný	k2eAgInPc6d1	pevný
tělesech	těleso	k1gNnPc6	těleso
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
přenáší	přenášet	k5eAaImIp3nS	přenášet
interakcí	interakce	k1gFnSc7	interakce
mezi	mezi	k7c7	mezi
částicemi	částice	k1gFnPc7	částice
pevně	pevně	k6eAd1	pevně
vázanými	vázané	k1gNnPc7	vázané
v	v	k7c6	v
krystalové	krystalový	k2eAgFnSc6d1	krystalová
nebo	nebo	k8xC	nebo
pseudokrystalové	pseudokrystal	k1gMnPc1	pseudokrystal
struktuře	struktura	k1gFnSc3	struktura
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Tlaková	tlakový	k2eAgFnSc1d1	tlaková
síla	síla	k1gFnSc1	síla
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
třetího	třetí	k4xOgInSc2	třetí
pohybového	pohybový	k2eAgInSc2d1	pohybový
zákona	zákon	k1gInSc2	zákon
působí	působit	k5eAaImIp3nS	působit
kapalina	kapalina	k1gFnSc1	kapalina
proti	proti	k7c3	proti
působící	působící	k2eAgFnSc3d1	působící
síle	síla	k1gFnSc3	síla
stejně	stejně	k6eAd1	stejně
velkou	velký	k2eAgFnSc7d1	velká
reakcí	reakce	k1gFnSc7	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Kapalina	kapalina	k1gFnSc1	kapalina
tedy	tedy	k9	tedy
působí	působit	k5eAaImIp3nS	působit
tlakovou	tlakový	k2eAgFnSc7d1	tlaková
silou	síla	k1gFnSc7	síla
kolmou	kolmý	k2eAgFnSc7d1	kolmá
k	k	k7c3	k
ploše	plocha	k1gFnSc3	plocha
<g/>
,	,	kIx,	,
na	na	k7c4	na
niž	jenž	k3xRgFnSc4	jenž
síla	síla	k1gFnSc1	síla
působí	působit	k5eAaImIp3nS	působit
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
síla	síla	k1gFnSc1	síla
má	mít	k5eAaImIp3nS	mít
velikost	velikost	k1gFnSc4	velikost
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
F	F	kA	F
=	=	kIx~	=
p	p	k?	p
S	s	k7c7	s
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F	F	kA	F
<g/>
=	=	kIx~	=
<g/>
pS	ps	k0	ps
<g/>
}	}	kIx)	}
:	:	kIx,	:
Působení	působení	k1gNnSc1	působení
tlakové	tlakový	k2eAgFnSc2d1	tlaková
síly	síla	k1gFnSc2	síla
lze	lze	k6eAd1	lze
demonstrovat	demonstrovat	k5eAaBmF	demonstrovat
na	na	k7c6	na
principu	princip	k1gInSc6	princip
hydraulického	hydraulický	k2eAgInSc2d1	hydraulický
lisu	lis	k1gInSc2	lis
<g/>
.	.	kIx.	.
</s>
<s>
Tlaková	tlakový	k2eAgFnSc1d1	tlaková
síla	síla	k1gFnSc1	síla
působí	působit	k5eAaImIp3nS	působit
vždy	vždy	k6eAd1	vždy
kolmo	kolmo	k6eAd1	kolmo
na	na	k7c4	na
plochu	plocha	k1gFnSc4	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vodorovné	vodorovný	k2eAgNnSc4d1	vodorovné
dno	dno	k1gNnSc4	dno
působí	působit	k5eAaImIp3nS	působit
svislá	svislý	k2eAgFnSc1d1	svislá
tlaková	tlakový	k2eAgFnSc1d1	tlaková
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
na	na	k7c4	na
svislé	svislý	k2eAgFnPc4d1	svislá
stěny	stěna	k1gFnPc4	stěna
působí	působit	k5eAaImIp3nS	působit
vodorovná	vodorovný	k2eAgFnSc1d1	vodorovná
tlaková	tlakový	k2eAgFnSc1d1	tlaková
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
na	na	k7c4	na
šikmé	šikmý	k2eAgFnPc4d1	šikmá
stěny	stěna	k1gFnPc4	stěna
působí	působit	k5eAaImIp3nS	působit
tlaková	tlakový	k2eAgFnSc1d1	tlaková
síla	síla	k1gFnSc1	síla
kolmá	kolmý	k2eAgFnSc1d1	kolmá
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
stěně	stěna	k1gFnSc3	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Působení	působení	k1gNnSc1	působení
tlaku	tlak	k1gInSc2	tlak
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
v	v	k7c6	v
pevných	pevný	k2eAgNnPc6d1	pevné
tělesech	těleso	k1gNnPc6	těleso
deformace	deformace	k1gFnSc1	deformace
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vzájemnou	vzájemný	k2eAgFnSc4d1	vzájemná
souvislost	souvislost	k1gFnSc4	souvislost
mezi	mezi	k7c7	mezi
působícím	působící	k2eAgInSc7d1	působící
tlakem	tlak	k1gInSc7	tlak
a	a	k8xC	a
vzniklou	vzniklý	k2eAgFnSc7d1	vzniklá
deformací	deformace	k1gFnSc7	deformace
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
pružnost	pružnost	k1gFnSc1	pružnost
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
působení	působení	k1gNnSc6	působení
malého	malý	k2eAgInSc2d1	malý
tlaku	tlak	k1gInSc2	tlak
platí	platit	k5eAaImIp3nS	platit
Hookův	Hookův	k2eAgInSc1d1	Hookův
zákon	zákon	k1gInSc1	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Čím	čí	k3xOyRgNnSc7	čí
je	být	k5eAaImIp3nS	být
síla	síla	k1gFnSc1	síla
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
větší	veliký	k2eAgFnSc1d2	veliký
je	být	k5eAaImIp3nS	být
i	i	k9	i
tlak	tlak	k1gInSc1	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Čím	čí	k3xOyRgNnSc7	čí
větší	veliký	k2eAgFnSc1d2	veliký
plocha	plocha	k1gFnSc1	plocha
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
menší	malý	k2eAgFnSc1d2	menší
tlak	tlak	k1gInSc1	tlak
(	(	kIx(	(
<g/>
při	při	k7c6	při
stejné	stejný	k2eAgFnSc6d1	stejná
síle	síla	k1gFnSc6	síla
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Parciální	parciální	k2eAgInSc4d1	parciální
tlak	tlak	k1gInSc4	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Parciální	parciální	k2eAgInSc1d1	parciální
tlak	tlak	k1gInSc1	tlak
plynu	plyn	k1gInSc2	plyn
ve	v	k7c6	v
směsi	směs	k1gFnSc6	směs
je	být	k5eAaImIp3nS	být
tlak	tlak	k1gInSc1	tlak
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
tento	tento	k3xDgInSc1	tento
plyn	plyn	k1gInSc1	plyn
vykazoval	vykazovat	k5eAaImAgInS	vykazovat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
objemu	objem	k1gInSc6	objem
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Molární	molární	k2eAgInSc1d1	molární
podíl	podíl	k1gInSc1	podíl
jednotlivé	jednotlivý	k2eAgFnSc2d1	jednotlivá
složky	složka	k1gFnSc2	složka
ve	v	k7c6	v
směsi	směs	k1gFnSc6	směs
plynů	plyn	k1gInPc2	plyn
(	(	kIx(	(
<g/>
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
stavové	stavový	k2eAgFnSc2d1	stavová
rovnice	rovnice	k1gFnSc2	rovnice
ideálního	ideální	k2eAgInSc2d1	ideální
plynu	plyn	k1gInSc2	plyn
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vyjádřen	vyjádřit	k5eAaPmNgInS	vyjádřit
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
⋅	⋅	k?	⋅
R	R	kA	R
T	T	kA	T
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p_	p_	k?	p_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
RT	RT	kA	RT
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
V	V	kA	V
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
i	i	k9	i
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
⋅	⋅	k?	⋅
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p_	p_	k?	p_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
p	p	k?	p
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
i	i	k9	i
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
kde	kde	k9	kde
pi	pi	k0	pi
je	být	k5eAaImIp3nS	být
parciální	parciální	k2eAgInSc1d1	parciální
tlak	tlak	k1gInSc1	tlak
<g/>
,	,	kIx,	,
R	R	kA	R
je	být	k5eAaImIp3nS	být
univerzální	univerzální	k2eAgFnSc1d1	univerzální
plynová	plynový	k2eAgFnSc1d1	plynová
konstanta	konstanta	k1gFnSc1	konstanta
(	(	kIx(	(
<g/>
8,314	[number]	k4	8,314
J	J	kA	J
<g/>
/	/	kIx~	/
<g/>
K.	K.	kA	K.
<g/>
mol	mol	k1gInSc1	mol
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
xi	xi	k?	xi
je	být	k5eAaImIp3nS	být
molární	molární	k2eAgInSc4d1	molární
zlomek	zlomek	k1gInSc4	zlomek
<g/>
.	.	kIx.	.
</s>
<s>
Rozměr	rozměr	k1gInSc1	rozměr
této	tento	k3xDgFnSc2	tento
veličiny	veličina	k1gFnSc2	veličina
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
SI	se	k3xPyFc3	se
i	i	k8xC	i
soustavě	soustava	k1gFnSc3	soustava
CGS	CGS	kA	CGS
je	být	k5eAaImIp3nS	být
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
i	i	k8xC	i
m	m	kA	m
:	:	kIx,	:
p	p	k?	p
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
dim	dim	k?	dim
<g/>
}	}	kIx)	}
p	p	k?	p
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
L	L	kA	L
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
T	T	kA	T
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
jednotkou	jednotka	k1gFnSc7	jednotka
tlaku	tlak	k1gInSc2	tlak
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
SI	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
pascal	pascal	k1gInSc1	pascal
(	(	kIx(	(
<g/>
Pa	Pa	kA	Pa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tlak	tlak	k1gInSc1	tlak
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
síla	síla	k1gFnSc1	síla
1	[number]	k4	1
newtonu	newton	k1gInSc2	newton
(	(	kIx(	(
<g/>
1	[number]	k4	1
N	N	kA	N
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
a	a	k8xC	a
spojitě	spojitě	k6eAd1	spojitě
rozložená	rozložený	k2eAgFnSc1d1	rozložená
a	a	k8xC	a
působící	působící	k2eAgMnSc1d1	působící
kolmo	kolmo	k6eAd1	kolmo
na	na	k7c4	na
plochu	plocha	k1gFnSc4	plocha
o	o	k7c6	o
obsahu	obsah	k1gInSc6	obsah
1	[number]	k4	1
čtverečního	čtvereční	k2eAgInSc2d1	čtvereční
metru	metr	k1gInSc2	metr
(	(	kIx(	(
<g/>
1	[number]	k4	1
m	m	kA	m
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
technické	technický	k2eAgFnSc6d1	technická
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
zejména	zejména	k9	zejména
násobky	násobek	k1gInPc4	násobek
kilopascal	kilopascat	k5eAaPmAgInS	kilopascat
(	(	kIx(	(
<g/>
kPa	kPa	k?	kPa
<g/>
,	,	kIx,	,
tj	tj	kA	tj
103	[number]	k4	103
Pa	Pa	kA	Pa
<g/>
)	)	kIx)	)
a	a	k8xC	a
megapascal	megapascat	k5eAaPmAgMnS	megapascat
(	(	kIx(	(
<g/>
MPa	MPa	k1gMnSc7	MPa
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
106	[number]	k4	106
Pa	Pa	kA	Pa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
hlubinné	hlubinný	k2eAgFnSc2d1	hlubinná
geologie	geologie	k1gFnSc2	geologie
a	a	k8xC	a
geofyziky	geofyzika	k1gFnSc2	geofyzika
i	i	k9	i
gigapascal	gigapascat	k5eAaPmAgInS	gigapascat
(	(	kIx(	(
<g/>
GPa	GPa	k1gFnSc4	GPa
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
109	[number]	k4	109
Pa	Pa	kA	Pa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
meteorologii	meteorologie	k1gFnSc6	meteorologie
je	být	k5eAaImIp3nS	být
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
uvádět	uvádět	k5eAaImF	uvádět
tlak	tlak	k1gInSc4	tlak
vzduchu	vzduch	k1gInSc2	vzduch
v	v	k7c6	v
jednotkách	jednotka	k1gFnPc6	jednotka
hektopascal	hektopascal	k1gInSc1	hektopascal
(	(	kIx(	(
<g/>
hPa	hPa	k?	hPa
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
100	[number]	k4	100
Pa	Pa	kA	Pa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
normální	normální	k2eAgInSc1d1	normální
tlak	tlak	k1gInSc1	tlak
atmosféry	atmosféra	k1gFnSc2	atmosféra
je	být	k5eAaImIp3nS	být
blízký	blízký	k2eAgInSc1d1	blízký
tisícinásobku	tisícinásobek	k1gInSc6	tisícinásobek
této	tento	k3xDgFnSc2	tento
jednotky	jednotka	k1gFnSc2	jednotka
(	(	kIx(	(
<g/>
přesně	přesně	k6eAd1	přesně
1013,25	[number]	k4	1013,25
hPa	hPa	k?	hPa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vakuové	vakuový	k2eAgFnSc6d1	vakuová
technice	technika	k1gFnSc6	technika
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
menší	malý	k2eAgFnSc1d2	menší
jednotka	jednotka	k1gFnSc1	jednotka
jako	jako	k8xS	jako
milipascal	milipascat	k5eAaPmAgInS	milipascat
(	(	kIx(	(
<g/>
mPa	mPa	k?	mPa
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
3	[number]	k4	3
Pa	Pa	kA	Pa
<g/>
)	)	kIx)	)
a	a	k8xC	a
mikropascal	mikropascat	k5eAaPmAgMnS	mikropascat
(	(	kIx(	(
<g/>
μ	μ	k?	μ
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
6	[number]	k4	6
Pa	Pa	kA	Pa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
technické	technický	k2eAgFnSc6d1	technická
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
používaly	používat	k5eAaImAgFnP	používat
i	i	k9	i
jiné	jiný	k2eAgFnPc1d1	jiná
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
bar	bar	k1gInSc1	bar
(	(	kIx(	(
<g/>
bar	bar	k1gInSc1	bar
<g/>
)	)	kIx)	)
a	a	k8xC	a
technická	technický	k2eAgFnSc1d1	technická
atmosféra	atmosféra	k1gFnSc1	atmosféra
(	(	kIx(	(
<g/>
at	at	k?	at
<g/>
)	)	kIx)	)
rovná	rovnat	k5eAaImIp3nS	rovnat
kilopondu	kilopond	k1gInSc2	kilopond
na	na	k7c4	na
čtverečný	čtverečný	k2eAgInSc4d1	čtverečný
centimetr	centimetr	k1gInSc4	centimetr
(	(	kIx(	(
<g/>
kp	kp	k?	kp
<g/>
/	/	kIx~	/
<g/>
cm	cm	kA	cm
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
a	a	k8xC	a
termodynamice	termodynamika	k1gFnSc6	termodynamika
se	se	k3xPyFc4	se
užívala	užívat	k5eAaImAgFnS	užívat
tzv.	tzv.	kA	tzv.
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
atmosféra	atmosféra	k1gFnSc1	atmosféra
(	(	kIx(	(
<g/>
atm	atm	k?	atm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odvozená	odvozený	k2eAgFnSc1d1	odvozená
od	od	k7c2	od
normálního	normální	k2eAgInSc2d1	normální
tlaku	tlak	k1gInSc2	tlak
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
meteorologii	meteorologie	k1gFnSc6	meteorologie
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
používání	používání	k1gNnSc1	používání
jednotky	jednotka	k1gFnSc2	jednotka
torr	torr	k1gInSc1	torr
(	(	kIx(	(
<g/>
Torr	torr	k1gInSc1	torr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
nazývané	nazývaný	k2eAgInPc1d1	nazývaný
milimetr	milimetr	k1gInSc4	milimetr
sloupce	sloupec	k1gInSc2	sloupec
rtuti	rtuť	k1gFnSc2	rtuť
(	(	kIx(	(
<g/>
mmHg	mmHg	k1gInSc1	mmHg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
anglosaské	anglosaský	k2eAgFnSc6d1	anglosaská
oblasti	oblast	k1gFnSc6	oblast
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
jednotkou	jednotka	k1gFnSc7	jednotka
libra	libra	k1gFnSc1	libra
síly	síla	k1gFnSc2	síla
na	na	k7c4	na
čtverečný	čtverečný	k2eAgInSc4d1	čtverečný
palec	palec	k1gInSc4	palec
(	(	kIx(	(
<g/>
psi	pes	k1gMnPc1	pes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
připojené	připojený	k2eAgFnSc6d1	připojená
tabulce	tabulka	k1gFnSc6	tabulka
jsou	být	k5eAaImIp3nP	být
převodní	převodní	k2eAgInPc1d1	převodní
vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
nejběžnějšími	běžný	k2eAgFnPc7d3	nejběžnější
jednotkami	jednotka	k1gFnPc7	jednotka
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
Symboly	symbol	k1gInPc1	symbol
<g/>
,	,	kIx,	,
uvedené	uvedený	k2eAgInPc1d1	uvedený
před	před	k7c7	před
převodními	převodní	k2eAgInPc7d1	převodní
koeficienty	koeficient	k1gInPc7	koeficient
znamenají	znamenat	k5eAaImIp3nP	znamenat
<g/>
:	:	kIx,	:
≡	≡	k?	≡
koeficient	koeficient	k1gInSc1	koeficient
je	být	k5eAaImIp3nS	být
takto	takto	k6eAd1	takto
definován	definovat	k5eAaBmNgInS	definovat
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
absolutně	absolutně	k6eAd1	absolutně
přesný	přesný	k2eAgMnSc1d1	přesný
<g/>
;	;	kIx,	;
=	=	kIx~	=
koeficient	koeficient	k1gInSc1	koeficient
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
definice	definice	k1gFnSc2	definice
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
konstanty	konstanta	k1gFnSc2	konstanta
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
hodnoty	hodnota	k1gFnPc1	hodnota
normálního	normální	k2eAgInSc2d1	normální
atmosférického	atmosférický	k2eAgInSc2d1	atmosférický
tlaku	tlak	k1gInSc2	tlak
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
≈	≈	k?	≈
koeficient	koeficient	k1gInSc1	koeficient
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
výpočtem	výpočet	k1gInSc7	výpočet
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zaokrouhlený	zaokrouhlený	k2eAgMnSc1d1	zaokrouhlený
<g/>
.	.	kIx.	.
</s>
<s>
Tlak	tlak	k1gInSc1	tlak
se	se	k3xPyFc4	se
měří	měřit	k5eAaImIp3nS	měřit
manometrem	manometr	k1gInSc7	manometr
(	(	kIx(	(
<g/>
tlakoměrem	tlakoměr	k1gInSc7	tlakoměr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Měření	měření	k1gNnSc2	měření
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Atmosférický	atmosférický	k2eAgInSc1d1	atmosférický
tlak	tlak	k1gInSc1	tlak
Krevní	krevní	k2eAgInSc4d1	krevní
tlak	tlak	k1gInSc4	tlak
Mechanické	mechanický	k2eAgNnSc4d1	mechanické
napětí	napětí	k1gNnSc4	napětí
Osmotický	osmotický	k2eAgInSc1d1	osmotický
tlak	tlak	k1gInSc1	tlak
Pascalův	Pascalův	k2eAgInSc1d1	Pascalův
zákon	zákon	k1gInSc4	zákon
Hydraulický	hydraulický	k2eAgInSc4d1	hydraulický
lis	lis	k1gInSc4	lis
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
tlak	tlak	k1gInSc1	tlak
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
tlak	tlak	k1gInSc1	tlak
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
