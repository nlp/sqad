<s>
Kinetická	kinetický	k2eAgFnSc1d1	kinetická
energie	energie	k1gFnSc1	energie
částic	částice	k1gFnPc2	částice
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
potenciální	potenciální	k2eAgFnSc1d1	potenciální
energie	energie	k1gFnSc1	energie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
přitažlivým	přitažlivý	k2eAgFnPc3d1	přitažlivá
silám	síla	k1gFnPc3	síla
<g/>
.	.	kIx.	.
</s>
