<p>
<s>
Plyn	plyn	k1gInSc1	plyn
neboli	neboli	k8xC	neboli
plynná	plynný	k2eAgFnSc1d1	plynná
látka	látka	k1gFnSc1	látka
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
ze	z	k7c2	z
skupenství	skupenství	k1gNnSc2	skupenství
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
jsou	být	k5eAaImIp3nP	být
částice	částice	k1gFnPc1	částice
relativně	relativně	k6eAd1	relativně
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
sebe	se	k3xPyFc2	se
<g/>
,	,	kIx,	,
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
se	se	k3xPyFc4	se
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
objemu	objem	k1gInSc6	objem
a	a	k8xC	a
nepůsobí	působit	k5eNaImIp3nS	působit
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
přitažlivou	přitažlivý	k2eAgFnSc7d1	přitažlivá
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chemických	chemický	k2eAgFnPc6d1	chemická
rovnicích	rovnice	k1gFnPc6	rovnice
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
písmenem	písmeno	k1gNnSc7	písmeno
g	g	kA	g
(	(	kIx(	(
<g/>
gas	gas	k?	gas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kinetická	kinetický	k2eAgFnSc1d1	kinetická
energie	energie	k1gFnSc1	energie
částic	částice	k1gFnPc2	částice
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
potenciální	potenciální	k2eAgFnSc1d1	potenciální
energie	energie	k1gFnSc1	energie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
přitažlivým	přitažlivý	k2eAgFnPc3d1	přitažlivá
silám	síla	k1gFnPc3	síla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgInSc2	ten
se	se	k3xPyFc4	se
částice	částice	k1gFnSc1	částice
po	po	k7c6	po
vzájemné	vzájemný	k2eAgFnSc6d1	vzájemná
srážce	srážka	k1gFnSc6	srážka
rychle	rychle	k6eAd1	rychle
vymaní	vymanit	k5eAaPmIp3nS	vymanit
z	z	k7c2	z
dosahu	dosah	k1gInSc2	dosah
přitažlivých	přitažlivý	k2eAgFnPc2d1	přitažlivá
sil	síla	k1gFnPc2	síla
a	a	k8xC	a
v	v	k7c6	v
objemu	objem	k1gInSc6	objem
látky	látka	k1gFnSc2	látka
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
téměř	téměř	k6eAd1	téměř
volně	volně	k6eAd1	volně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzájemné	vzájemný	k2eAgFnPc1d1	vzájemná
vazby	vazba	k1gFnPc1	vazba
mezi	mezi	k7c7	mezi
částicemi	částice	k1gFnPc7	částice
lze	lze	k6eAd1	lze
tedy	tedy	k9	tedy
téměř	téměř	k6eAd1	téměř
zanedbat	zanedbat	k5eAaPmF	zanedbat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
řídkých	řídký	k2eAgInPc2d1	řídký
plynů	plyn	k1gInPc2	plyn
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgFnPc4d1	možná
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
částice	částice	k1gFnPc4	částice
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
volné	volný	k2eAgNnSc4d1	volné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc4	vlastnost
plynů	plyn	k1gInPc2	plyn
==	==	k?	==
</s>
</p>
<p>
<s>
plynná	plynný	k2eAgNnPc1d1	plynné
tělesa	těleso	k1gNnPc1	těleso
nemají	mít	k5eNaImIp3nP	mít
svůj	svůj	k3xOyFgInSc4	svůj
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gInSc1	jejich
tvar	tvar	k1gInSc1	tvar
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
tvaru	tvar	k1gInSc3	tvar
nádoby	nádoba	k1gFnSc2	nádoba
</s>
</p>
<p>
<s>
plynná	plynný	k2eAgNnPc1d1	plynné
tělesa	těleso	k1gNnPc1	těleso
nemají	mít	k5eNaImIp3nP	mít
vlastní	vlastní	k2eAgInSc4d1	vlastní
objem	objem	k1gInSc4	objem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vyplňují	vyplňovat	k5eAaImIp3nP	vyplňovat
vždy	vždy	k6eAd1	vždy
celý	celý	k2eAgInSc4d1	celý
objem	objem	k1gInSc4	objem
nádoby	nádoba	k1gFnSc2	nádoba
</s>
</p>
<p>
<s>
plynná	plynný	k2eAgNnPc1d1	plynné
tělesa	těleso	k1gNnPc1	těleso
nemají	mít	k5eNaImIp3nP	mít
volný	volný	k2eAgInSc4d1	volný
povrch	povrch	k1gInSc4	povrch
(	(	kIx(	(
<g/>
hladinu	hladina	k1gFnSc4	hladina
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
plyny	plyn	k1gInPc1	plyn
jsou	být	k5eAaImIp3nP	být
stlačitelné	stlačitelný	k2eAgInPc1d1	stlačitelný
</s>
</p>
<p>
<s>
plyny	plyn	k1gInPc1	plyn
vedou	vést	k5eAaImIp3nP	vést
elektrický	elektrický	k2eAgInSc4d1	elektrický
proud	proud	k1gInSc4	proud
jen	jen	k9	jen
za	za	k7c2	za
určitých	určitý	k2eAgFnPc2d1	určitá
speciálních	speciální	k2eAgFnPc2d1	speciální
podmínek	podmínka	k1gFnPc2	podmínka
</s>
</p>
<p>
<s>
teplo	teplo	k1gNnSc1	teplo
se	se	k3xPyFc4	se
v	v	k7c6	v
plynech	plyn	k1gInPc6	plyn
může	moct	k5eAaImIp3nS	moct
šířit	šířit	k5eAaImF	šířit
prouděním	proudění	k1gNnSc7	proudění
</s>
</p>
<p>
<s>
celkový	celkový	k2eAgInSc1d1	celkový
tlak	tlak	k1gInSc1	tlak
plynu	plyn	k1gInSc2	plyn
ve	v	k7c6	v
směsi	směs	k1gFnSc6	směs
vzájemně	vzájemně	k6eAd1	vzájemně
chemicky	chemicky	k6eAd1	chemicky
neinteragujících	interagující	k2eNgInPc2d1	interagující
plynů	plyn	k1gInPc2	plyn
je	být	k5eAaImIp3nS	být
dán	dát	k5eAaPmNgInS	dát
Daltonovým	Daltonový	k2eAgInSc7d1	Daltonový
zákonemVýše	zákonemVýše	k1gFnSc2	zákonemVýše
zmíněná	zmíněný	k2eAgNnPc1d1	zmíněné
pravidla	pravidlo	k1gNnPc1	pravidlo
platí	platit	k5eAaImIp3nP	platit
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
zanedbáme	zanedbat	k5eAaPmIp1nP	zanedbat
gravitaci	gravitace	k1gFnSc4	gravitace
(	(	kIx(	(
<g/>
což	což	k9	což
pro	pro	k7c4	pro
pokusy	pokus	k1gInPc4	pokus
v	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
měřítku	měřítko	k1gNnSc6	měřítko
lze	lze	k6eAd1	lze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Plyn	plyn	k1gInSc1	plyn
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
také	také	k6eAd1	také
držen	držet	k5eAaImNgMnS	držet
pohromadě	pohromadě	k6eAd1	pohromadě
gravitací	gravitace	k1gFnSc7	gravitace
<g/>
,	,	kIx,	,
a	a	k8xC	a
tvořit	tvořit	k5eAaImF	tvořit
tak	tak	k6eAd1	tak
atmosféru	atmosféra	k1gFnSc4	atmosféra
planety	planeta	k1gFnSc2	planeta
nebo	nebo	k8xC	nebo
planetu	planeta	k1gFnSc4	planeta
samou	samý	k3xTgFnSc4	samý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Objem	objem	k1gInSc1	objem
plynu	plyn	k1gInSc2	plyn
===	===	k?	===
</s>
</p>
<p>
<s>
Objem	objem	k1gInSc1	objem
jednoho	jeden	k4xCgInSc2	jeden
molu	mol	k1gInSc2	mol
plynu	plyn	k1gInSc2	plyn
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
podmínek	podmínka	k1gFnPc2	podmínka
(	(	kIx(	(
<g/>
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
101	[number]	k4	101
325	[number]	k4	325
Pa	Pa	kA	Pa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
0,022414	[number]	k4	0,022414
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
22,414	[number]	k4	22,414
litru	litr	k1gInSc2	litr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
objemu	objem	k1gInSc6	objem
je	být	k5eAaImIp3nS	být
obsaženo	obsáhnout	k5eAaPmNgNnS	obsáhnout
6,022	[number]	k4	6,022
<g/>
×	×	k?	×
<g/>
1023	[number]	k4	1023
částic	částice	k1gFnPc2	částice
(	(	kIx(	(
<g/>
atomů	atom	k1gInPc2	atom
nebo	nebo	k8xC	nebo
molekul	molekula	k1gFnPc2	molekula
<g/>
)	)	kIx)	)
látky	látka	k1gFnSc2	látka
–	–	k?	–
tzv.	tzv.	kA	tzv.
Avogadrova	Avogadrův	k2eAgFnSc1d1	Avogadrova
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
podmínek	podmínka	k1gFnPc2	podmínka
podle	podle	k7c2	podle
IUPAC	IUPAC	kA	IUPAC
(	(	kIx(	(
<g/>
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
100	[number]	k4	100
000	[number]	k4	000
Pa	Pa	kA	Pa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
objem	objem	k1gInSc4	objem
1	[number]	k4	1
molu	mol	k1gInSc2	mol
přibližně	přibližně	k6eAd1	přibližně
22,71	[number]	k4	22,71
litru	litr	k1gInSc2	litr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ideální	ideální	k2eAgInSc1d1	ideální
plyn	plyn	k1gInSc1	plyn
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
zjednodušené	zjednodušený	k2eAgNnSc4d1	zjednodušené
zkoumání	zkoumání	k1gNnSc4	zkoumání
vlastností	vlastnost	k1gFnPc2	vlastnost
plynů	plyn	k1gInPc2	plyn
se	se	k3xPyFc4	se
zavádí	zavádět	k5eAaImIp3nS	zavádět
pojem	pojem	k1gInSc1	pojem
ideální	ideální	k2eAgInSc1d1	ideální
(	(	kIx(	(
<g/>
dokonalý	dokonalý	k2eAgInSc1d1	dokonalý
<g/>
)	)	kIx)	)
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
dokonale	dokonale	k6eAd1	dokonale
stlačitelný	stlačitelný	k2eAgInSc1d1	stlačitelný
plyn	plyn	k1gInSc1	plyn
bez	bez	k7c2	bez
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
tření	tření	k1gNnSc2	tření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Skutečný	skutečný	k2eAgInSc1d1	skutečný
plyn	plyn	k1gInSc1	plyn
==	==	k?	==
</s>
</p>
<p>
<s>
Skutečný	skutečný	k2eAgInSc1d1	skutečný
(	(	kIx(	(
<g/>
reálný	reálný	k2eAgInSc1d1	reálný
<g/>
)	)	kIx)	)
plyn	plyn	k1gInSc1	plyn
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ideálního	ideální	k2eAgInSc2d1	ideální
plynu	plyn	k1gInSc2	plyn
také	také	k9	také
viskozitu	viskozita	k1gFnSc4	viskozita
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
tření	tření	k1gNnSc4	tření
<g/>
)	)	kIx)	)
a	a	k8xC	a
nedá	dát	k5eNaPmIp3nS	dát
se	se	k3xPyFc4	se
dokonale	dokonale	k6eAd1	dokonale
stlačit	stlačit	k5eAaPmF	stlačit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
zkoumání	zkoumání	k1gNnSc6	zkoumání
plynu	plyn	k1gInSc2	plyn
jakožto	jakožto	k8xS	jakožto
souboru	soubor	k1gInSc2	soubor
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
částic	částice	k1gFnPc2	částice
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
kinetická	kinetický	k2eAgFnSc1d1	kinetická
teorie	teorie	k1gFnSc1	teorie
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
aplikací	aplikace	k1gFnSc7	aplikace
získáme	získat	k5eAaPmIp1nP	získat
kinetickou	kinetický	k2eAgFnSc4d1	kinetická
teorii	teorie	k1gFnSc4	teorie
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mez	mezit	k5eAaImRp2nS	mezit
výbušnosti	výbušnost	k1gFnSc2	výbušnost
==	==	k?	==
</s>
</p>
<p>
<s>
Spodní	spodní	k2eAgFnSc1d1	spodní
mez	mez	k1gFnSc1	mez
výbušnosti	výbušnost	k1gFnSc2	výbušnost
je	být	k5eAaImIp3nS	být
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
koncentrace	koncentrace	k1gFnSc1	koncentrace
hořlavého	hořlavý	k2eAgInSc2d1	hořlavý
plynu	plyn	k1gInSc2	plyn
ve	v	k7c6	v
směsi	směs	k1gFnSc6	směs
se	s	k7c7	s
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
už	už	k6eAd1	už
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
dostatek	dostatek	k1gInSc4	dostatek
kyslíku	kyslík	k1gInSc2	kyslík
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
směs	směs	k1gFnSc1	směs
schopná	schopný	k2eAgFnSc1d1	schopná
po	po	k7c6	po
dodání	dodání	k1gNnSc6	dodání
iniciační	iniciační	k2eAgFnSc2d1	iniciační
energie	energie	k1gFnSc2	energie
(	(	kIx(	(
<g/>
např.	např.	kA	např.
jiskrou	jiskra	k1gFnSc7	jiskra
<g/>
,	,	kIx,	,
plamenem	plamen	k1gInSc7	plamen
<g/>
,	,	kIx,	,
tlakem	tlak	k1gInSc7	tlak
<g/>
)	)	kIx)	)
hořet	hořet	k5eAaImF	hořet
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
deflagrovat	deflagrovat	k5eAaBmF	deflagrovat
či	či	k8xC	či
detonovat	detonovat	k5eAaBmF	detonovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Horní	horní	k2eAgFnSc1d1	horní
mez	mez	k1gFnSc1	mez
výbušnosti	výbušnost	k1gFnSc2	výbušnost
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
koncentrace	koncentrace	k1gFnSc1	koncentrace
hořlavého	hořlavý	k2eAgInSc2d1	hořlavý
plynu	plyn	k1gInSc2	plyn
ve	v	k7c6	v
směsi	směs	k1gFnSc6	směs
se	s	k7c7	s
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
dostatek	dostatek	k1gInSc4	dostatek
kyslíku	kyslík	k1gInSc2	kyslík
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
směs	směs	k1gFnSc1	směs
schopná	schopný	k2eAgFnSc1d1	schopná
po	po	k7c6	po
dodání	dodání	k1gNnSc6	dodání
iniciační	iniciační	k2eAgFnSc2d1	iniciační
energie	energie	k1gFnSc2	energie
hořet	hořet	k5eAaImF	hořet
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
deflagrovat	deflagrovat	k5eAaBmF	deflagrovat
či	či	k8xC	či
detonovat	detonovat	k5eAaBmF	detonovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Například	například	k6eAd1	například
u	u	k7c2	u
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
je	být	k5eAaImIp3nS	být
spodní	spodní	k2eAgFnSc1d1	spodní
mez	mez	k1gFnSc1	mez
výbušnosti	výbušnost	k1gFnSc2	výbušnost
4,3	[number]	k4	4,3
<g/>
%	%	kIx~	%
a	a	k8xC	a
horní	horní	k2eAgFnSc1d1	horní
15	[number]	k4	15
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
koncentrace	koncentrace	k1gFnSc1	koncentrace
pro	pro	k7c4	pro
výbuch	výbuch	k1gInSc4	výbuch
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgFnPc7	tento
hodnotami	hodnota	k1gFnPc7	hodnota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgMnPc4d1	související
===	===	k?	===
</s>
</p>
<p>
<s>
Látka	látka	k1gFnSc1	látka
</s>
</p>
<p>
<s>
Skupenství	skupenství	k1gNnSc1	skupenství
</s>
</p>
<p>
<s>
Tekutina	tekutina	k1gFnSc1	tekutina
</s>
</p>
<p>
<s>
Kapalina	kapalina	k1gFnSc1	kapalina
</s>
</p>
<p>
<s>
Pevná	pevný	k2eAgFnSc1d1	pevná
látka	látka	k1gFnSc1	látka
</s>
</p>
<p>
<s>
Aeromechanika	aeromechanika	k1gFnSc1	aeromechanika
</s>
</p>
<p>
<s>
Svítiplyn	svítiplyn	k1gInSc1	svítiplyn
</s>
</p>
<p>
<s>
Zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
</s>
</p>
<p>
<s>
Plynovod	plynovod	k1gInSc1	plynovod
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
plyn	plyn	k1gInSc1	plyn
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Plyn	plyn	k1gInSc1	plyn
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
plyn	plyn	k1gInSc1	plyn
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
