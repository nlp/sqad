<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc4
Islandu	Island	k1gInSc2
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
Islandu	Island	k1gInSc2
v	v	k7c6
polovině	polovina	k1gFnSc6
roku	rok	k1gInSc2
2015	#num#	k4
činil	činit	k5eAaImAgMnS
odhadem	odhad	k1gInSc7
330	#num#	k4
610	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
93	#num#	k4
%	%	kIx~
obyvatel	obyvatel	k1gMnPc2
Islandu	Island	k1gInSc2
jsou	být	k5eAaImIp3nP
Islanďané	Islanďan	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
Islanďanů	Islanďan	k1gMnPc2
jsou	být	k5eAaImIp3nP
potomci	potomek	k1gMnPc1
norských	norský	k2eAgMnPc2d1
osadníků	osadník	k1gMnPc2
a	a	k8xC
keltů	kelta	k1gMnPc2
z	z	k7c2
Irska	Irsko	k1gNnSc2
a	a	k8xC
Skotska	Skotsko	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc4,k3yRgNnSc4,k3yIgNnSc4
sem	sem	k6eAd1
byli	být	k5eAaImAgMnP
dovezeni	dovézt	k5eAaPmNgMnP
jako	jako	k9
otroci	otrok	k1gMnPc1
v	v	k7c6
době	doba	k1gFnSc6
osídlení	osídlení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moderní	moderní	k2eAgFnPc4d1
analýzy	analýza	k1gFnPc4
DNA	dno	k1gNnSc2
naznačují	naznačovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
asi	asi	k9
66	#num#	k4
procent	procento	k1gNnPc2
mužské	mužský	k2eAgFnSc2d1
populace	populace	k1gFnSc2
v	v	k7c6
době	doba	k1gFnSc6
osídlování	osídlování	k1gNnSc2
měla	mít	k5eAaImAgFnS
nordický	nordický	k2eAgInSc4d1
původ	původ	k1gInSc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
ženská	ženský	k2eAgFnSc1d1
populace	populace	k1gFnSc1
byla	být	k5eAaImAgFnS
z	z	k7c2
šedesáti	šedesát	k4xCc2
procent	procento	k1gNnPc2
keltská	keltský	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Dnešní	dnešní	k2eAgFnSc1d1
islandská	islandský	k2eAgFnSc1d1
populace	populace	k1gFnSc1
je	být	k5eAaImIp3nS
pozoruhodně	pozoruhodně	k6eAd1
homogenní	homogenní	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
statistik	statistika	k1gFnPc2
islandské	islandský	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
žije	žít	k5eAaImIp3nS
99	#num#	k4
%	%	kIx~
populace	populace	k1gFnSc2
v	v	k7c6
městských	městský	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
(	(	kIx(
<g/>
místa	místo	k1gNnPc4
<g/>
,	,	kIx,
která	který	k3yRgNnPc1,k3yQgNnPc1,k3yIgNnPc1
mají	mít	k5eAaImIp3nP
více	hodně	k6eAd2
než	než	k8xS
200	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
)	)	kIx)
a	a	k8xC
60	#num#	k4
%	%	kIx~
žije	žít	k5eAaImIp3nS
v	v	k7c6
oblasti	oblast	k1gFnSc6
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
severogermánských	severogermánský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
má	mít	k5eAaImIp3nS
islandština	islandština	k1gFnSc1
nejblíže	blízce	k6eAd3
ke	k	k7c3
staré	starý	k2eAgFnSc3d1
severštině	severština	k1gFnSc3
a	a	k8xC
zůstala	zůstat	k5eAaPmAgFnS
relativně	relativně	k6eAd1
nezměněná	změněný	k2eNgFnSc1d1
od	od	k7c2
dvanáctého	dvanáctý	k4xOgNnSc2
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Všichni	všechen	k3xTgMnPc1
žijící	žijící	k2eAgMnPc1d1
Islanďané	Islanďan	k1gMnPc1
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
všichni	všechen	k3xTgMnPc1
cizinci	cizinec	k1gMnPc1
s	s	k7c7
trvalým	trvalý	k2eAgInSc7d1
pobytem	pobyt	k1gInSc7
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
osobní	osobní	k2eAgNnSc4d1
identifikační	identifikační	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
(	(	kIx(
<g/>
kennitala	kennitat	k5eAaImAgFnS,k5eAaPmAgFnS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgNnSc4,k3yRgNnSc4,k3yQgNnSc4
je	být	k5eAaImIp3nS
identifikuje	identifikovat	k5eAaBmIp3nS
v	v	k7c6
národním	národní	k2eAgInSc6d1
registru	registr	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kompletnost	kompletnost	k1gFnSc1
tohoto	tento	k3xDgInSc2
registru	registr	k1gInSc2
eliminuje	eliminovat	k5eAaBmIp3nS
potřebu	potřeba	k1gFnSc4
sčítání	sčítání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
Reykjavíku	Reykjavík	k1gInSc2
a	a	k8xC
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
okolí	okolí	k1gNnSc2
žije	žít	k5eAaImIp3nS
více	hodně	k6eAd2
než	než	k8xS
polovina	polovina	k1gFnSc1
Islanďanů	Islanďan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Island	Island	k1gInSc1
je	být	k5eAaImIp3nS
nejřidčeji	řídce	k6eAd3
osídlená	osídlený	k2eAgFnSc1d1
evropská	evropský	k2eAgFnSc1d1
země	země	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnSc2
venkovské	venkovský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
navštěvuje	navštěvovat	k5eAaImIp3nS
ve	v	k7c6
všech	všecek	k3xTgFnPc6
třídách	třída	k1gFnPc6
méně	málo	k6eAd2
než	než	k8xS
sto	sto	k4xCgNnSc1
žáků	žák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Demografická	demografický	k2eAgFnSc1d1
statistika	statistika	k1gFnSc1
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Konkrétní	konkrétní	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
:	:	kIx,
WP	WP	kA
<g/>
:	:	kIx,
<g/>
TYP	typ	k1gInSc1
<g/>
,	,	kIx,
čárka	čárka	k1gFnSc1
v	v	k7c6
číslech	číslo	k1gNnPc6
(	(	kIx(
<g/>
3	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
)	)	kIx)
má	mít	k5eAaImIp3nS
v	v	k7c4
cs	cs	k?
význam	význam	k1gInSc4
desetinné	desetinný	k2eAgFnSc2d1
</s>
<s>
Demografická	demografický	k2eAgFnSc1d1
statistika	statistika	k1gFnSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Populace	populace	k1gFnSc1
(	(	kIx(
<g/>
x	x	k?
1000	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Živě	živě	k6eAd1
narození	narozený	k2eAgMnPc1d1
</s>
<s>
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
Přirozená	přirozený	k2eAgFnSc1d1
změna	změna	k1gFnSc1
</s>
<s>
Hrubá	hrubý	k2eAgFnSc1d1
míra	míra	k1gFnSc1
porodnosti	porodnost	k1gFnSc2
(	(	kIx(
<g/>
na	na	k7c4
1000	#num#	k4
obyv	obyva	k1gFnPc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Hrubá	hrubý	k2eAgFnSc1d1
míra	míra	k1gFnSc1
úmrtnosti	úmrtnost	k1gFnSc2
(	(	kIx(
<g/>
na	na	k7c4
1000	#num#	k4
obyv	obyva	k1gFnPc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Přirozená	přirozený	k2eAgFnSc1d1
změna	změna	k1gFnSc1
(	(	kIx(
<g/>
na	na	k7c4
1000	#num#	k4
obyv	obyva	k1gFnPc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Plodnost	plodnost	k1gFnSc1
</s>
<s>
1900	#num#	k4
</s>
<s>
78	#num#	k4
</s>
<s>
2,237	2,237	k4
</s>
<s>
1,545	1,545	k4
</s>
<s>
692	#num#	k4
</s>
<s>
28,6	28,6	k4
</s>
<s>
19,8	19,8	k4
</s>
<s>
8,9	8,9	k4
</s>
<s>
3,93	3,93	k4
</s>
<s>
1901	#num#	k4
</s>
<s>
78	#num#	k4
</s>
<s>
2,179	2,179	k4
</s>
<s>
1,155	1,155	k4
</s>
<s>
1,024	1,024	k4
</s>
<s>
27,8	27,8	k4
</s>
<s>
14,7	14,7	k4
</s>
<s>
13,1	13,1	k4
</s>
<s>
3,86	3,86	k4
</s>
<s>
1902	#num#	k4
</s>
<s>
79	#num#	k4
</s>
<s>
2,220	2,220	k4
</s>
<s>
1,262	1,262	k4
</s>
<s>
958	#num#	k4
</s>
<s>
28,1	28,1	k4
</s>
<s>
16,0	16,0	k4
</s>
<s>
12,1	12,1	k4
</s>
<s>
3,95	3,95	k4
</s>
<s>
1903	#num#	k4
</s>
<s>
79	#num#	k4
</s>
<s>
2,244	2,244	k4
</s>
<s>
1,324	1,324	k4
</s>
<s>
920	#num#	k4
</s>
<s>
28,3	28,3	k4
</s>
<s>
16,7	16,7	k4
</s>
<s>
11,6	11,6	k4
</s>
<s>
4,00	4,00	k4
</s>
<s>
1904	#num#	k4
</s>
<s>
80	#num#	k4
</s>
<s>
2,293	2,293	k4
</s>
<s>
1,242	1,242	k4
</s>
<s>
1,051	1,051	k4
</s>
<s>
28,7	28,7	k4
</s>
<s>
15,5	15,5	k4
</s>
<s>
13,1	13,1	k4
</s>
<s>
4,09	4,09	k4
</s>
<s>
1905	#num#	k4
</s>
<s>
81	#num#	k4
</s>
<s>
2,271	2,271	k4
</s>
<s>
1,435	1,435	k4
</s>
<s>
836	#num#	k4
</s>
<s>
28,1	28,1	k4
</s>
<s>
17,8	17,8	k4
</s>
<s>
10,4	10,4	k4
</s>
<s>
4,02	4,02	k4
</s>
<s>
1906	#num#	k4
</s>
<s>
82	#num#	k4
</s>
<s>
2,346	2,346	k4
</s>
<s>
1,193	1,193	k4
</s>
<s>
1,153	1,153	k4
</s>
<s>
28,8	28,8	k4
</s>
<s>
14,6	14,6	k4
</s>
<s>
14,1	14,1	k4
</s>
<s>
4,14	4,14	k4
</s>
<s>
1907	#num#	k4
</s>
<s>
83	#num#	k4
</s>
<s>
2,304	2,304	k4
</s>
<s>
1,396	1,396	k4
</s>
<s>
908	#num#	k4
</s>
<s>
27,9	27,9	k4
</s>
<s>
16,9	16,9	k4
</s>
<s>
11,0	11,0	k4
</s>
<s>
4,04	4,04	k4
</s>
<s>
1908	#num#	k4
</s>
<s>
83	#num#	k4
</s>
<s>
2,270	2,270	k4
</s>
<s>
1,594	1,594	k4
</s>
<s>
676	#num#	k4
</s>
<s>
27,3	27,3	k4
</s>
<s>
19,1	19,1	k4
</s>
<s>
8,1	8,1	k4
</s>
<s>
4,01	4,01	k4
</s>
<s>
1909	#num#	k4
</s>
<s>
84	#num#	k4
</s>
<s>
2,283	2,283	k4
</s>
<s>
1,263	1,263	k4
</s>
<s>
1,020	1,020	k4
</s>
<s>
27,2	27,2	k4
</s>
<s>
15,0	15,0	k4
</s>
<s>
12,1	12,1	k4
</s>
<s>
4,00	4,00	k4
</s>
<s>
1910	#num#	k4
</s>
<s>
85	#num#	k4
</s>
<s>
2,171	2,171	k4
</s>
<s>
1,304	1,304	k4
</s>
<s>
867	#num#	k4
</s>
<s>
25,6	25,6	k4
</s>
<s>
15,4	15,4	k4
</s>
<s>
10,2	10,2	k4
</s>
<s>
3,79	3,79	k4
</s>
<s>
1911	#num#	k4
</s>
<s>
85	#num#	k4
</s>
<s>
2,205	2,205	k4
</s>
<s>
1,152	1,152	k4
</s>
<s>
1,053	1,053	k4
</s>
<s>
25,8	25,8	k4
</s>
<s>
13,5	13,5	k4
</s>
<s>
12,3	12,3	k4
</s>
<s>
3,80	3,80	k4
</s>
<s>
1912	#num#	k4
</s>
<s>
86	#num#	k4
</s>
<s>
2,234	2,234	k4
</s>
<s>
1,171	1,171	k4
</s>
<s>
1,063	1,063	k4
</s>
<s>
26,0	26,0	k4
</s>
<s>
13,6	13,6	k4
</s>
<s>
12,4	12,4	k4
</s>
<s>
3,82	3,82	k4
</s>
<s>
1913	#num#	k4
</s>
<s>
87	#num#	k4
</s>
<s>
2,216	2,216	k4
</s>
<s>
1,060	1,060	k4
</s>
<s>
1,156	1,156	k4
</s>
<s>
25,6	25,6	k4
</s>
<s>
12,2	12,2	k4
</s>
<s>
13,3	13,3	k4
</s>
<s>
3,76	3,76	k4
</s>
<s>
1914	#num#	k4
</s>
<s>
88	#num#	k4
</s>
<s>
2,338	2,338	k4
</s>
<s>
1,428	1,428	k4
</s>
<s>
910	#num#	k4
</s>
<s>
26,7	26,7	k4
</s>
<s>
16,3	16,3	k4
</s>
<s>
10,4	10,4	k4
</s>
<s>
3,90	3,90	k4
</s>
<s>
1915	#num#	k4
</s>
<s>
89	#num#	k4
</s>
<s>
2,446	2,446	k4
</s>
<s>
1,376	1,376	k4
</s>
<s>
1,070	1,070	k4
</s>
<s>
27,6	27,6	k4
</s>
<s>
15,5	15,5	k4
</s>
<s>
12,1	12,1	k4
</s>
<s>
4,00	4,00	k4
</s>
<s>
1916	#num#	k4
</s>
<s>
89	#num#	k4
</s>
<s>
2,377	2,377	k4
</s>
<s>
1,322	1,322	k4
</s>
<s>
1,055	1,055	k4
</s>
<s>
26,6	26,6	k4
</s>
<s>
14,8	14,8	k4
</s>
<s>
11,8	11,8	k4
</s>
<s>
3,81	3,81	k4
</s>
<s>
1917	#num#	k4
</s>
<s>
91	#num#	k4
</s>
<s>
2,427	2,427	k4
</s>
<s>
1,111	1,111	k4
</s>
<s>
1,316	1,316	k4
</s>
<s>
26,8	26,8	k4
</s>
<s>
12,3	12,3	k4
</s>
<s>
14,5	14,5	k4
</s>
<s>
3,82	3,82	k4
</s>
<s>
1918	#num#	k4
</s>
<s>
92	#num#	k4
</s>
<s>
2,441	2,441	k4
</s>
<s>
1,518	1,518	k4
</s>
<s>
923	#num#	k4
</s>
<s>
26,6	26,6	k4
</s>
<s>
16,6	16,6	k4
</s>
<s>
10,1	10,1	k4
</s>
<s>
3,78	3,78	k4
</s>
<s>
1919	#num#	k4
</s>
<s>
92	#num#	k4
</s>
<s>
2,342	2,342	k4
</s>
<s>
1,169	1,169	k4
</s>
<s>
1,173	1,173	k4
</s>
<s>
25,4	25,4	k4
</s>
<s>
12,7	12,7	k4
</s>
<s>
12,7	12,7	k4
</s>
<s>
3,62	3,62	k4
</s>
<s>
1920	#num#	k4
</s>
<s>
94	#num#	k4
</s>
<s>
2,627	2,627	k4
</s>
<s>
1,360	1,360	k4
</s>
<s>
1,267	1,267	k4
</s>
<s>
28,1	28,1	k4
</s>
<s>
14,5	14,5	k4
</s>
<s>
13,5	13,5	k4
</s>
<s>
3,96	3,96	k4
</s>
<s>
1921	#num#	k4
</s>
<s>
95	#num#	k4
</s>
<s>
2,601	2,601	k4
</s>
<s>
1,478	1,478	k4
</s>
<s>
1,123	1,123	k4
</s>
<s>
27,4	27,4	k4
</s>
<s>
15,6	15,6	k4
</s>
<s>
11,8	11,8	k4
</s>
<s>
3,87	3,87	k4
</s>
<s>
1922	#num#	k4
</s>
<s>
96	#num#	k4
</s>
<s>
2,546	2,546	k4
</s>
<s>
1,280	1,280	k4
</s>
<s>
1,266	1,266	k4
</s>
<s>
26,6	26,6	k4
</s>
<s>
13,4	13,4	k4
</s>
<s>
13,2	13,2	k4
</s>
<s>
3,72	3,72	k4
</s>
<s>
1923	#num#	k4
</s>
<s>
97	#num#	k4
</s>
<s>
2,612	2,612	k4
</s>
<s>
1,287	1,287	k4
</s>
<s>
1,325	1,325	k4
</s>
<s>
26,9	26,9	k4
</s>
<s>
13,3	13,3	k4
</s>
<s>
13,7	13,7	k4
</s>
<s>
3,77	3,77	k4
</s>
<s>
1924	#num#	k4
</s>
<s>
98	#num#	k4
</s>
<s>
2,525	2,525	k4
</s>
<s>
1,462	1,462	k4
</s>
<s>
1,063	1,063	k4
</s>
<s>
25,7	25,7	k4
</s>
<s>
14,9	14,9	k4
</s>
<s>
10,8	10,8	k4
</s>
<s>
3,62	3,62	k4
</s>
<s>
1925	#num#	k4
</s>
<s>
99	#num#	k4
</s>
<s>
2,554	2,554	k4
</s>
<s>
1,229	1,229	k4
</s>
<s>
1,325	1,325	k4
</s>
<s>
25,7	25,7	k4
</s>
<s>
12,4	12,4	k4
</s>
<s>
13,3	13,3	k4
</s>
<s>
3,59	3,59	k4
</s>
<s>
1926	#num#	k4
</s>
<s>
101	#num#	k4
</s>
<s>
2,676	2,676	k4
</s>
<s>
1,121	1,121	k4
</s>
<s>
1,555	1,555	k4
</s>
<s>
26,5	26,5	k4
</s>
<s>
11,1	11,1	k4
</s>
<s>
15,4	15,4	k4
</s>
<s>
3,71	3,71	k4
</s>
<s>
1927	#num#	k4
</s>
<s>
103	#num#	k4
</s>
<s>
2,642	2,642	k4
</s>
<s>
1,282	1,282	k4
</s>
<s>
1,360	1,360	k4
</s>
<s>
25,8	25,8	k4
</s>
<s>
12,5	12,5	k4
</s>
<s>
13,3	13,3	k4
</s>
<s>
3,59	3,59	k4
</s>
<s>
1928	#num#	k4
</s>
<s>
104	#num#	k4
</s>
<s>
2,542	2,542	k4
</s>
<s>
1,124	1,124	k4
</s>
<s>
1,418	1,418	k4
</s>
<s>
24,4	24,4	k4
</s>
<s>
10,8	10,8	k4
</s>
<s>
13,6	13,6	k4
</s>
<s>
3,40	3,40	k4
</s>
<s>
1929	#num#	k4
</s>
<s>
106	#num#	k4
</s>
<s>
2,644	2,644	k4
</s>
<s>
1,237	1,237	k4
</s>
<s>
1,407	1,407	k4
</s>
<s>
25,0	25,0	k4
</s>
<s>
11,7	11,7	k4
</s>
<s>
13,3	13,3	k4
</s>
<s>
3,47	3,47	k4
</s>
<s>
1930	#num#	k4
</s>
<s>
107	#num#	k4
</s>
<s>
2,808	2,808	k4
</s>
<s>
1,248	1,248	k4
</s>
<s>
1,560	1,560	k4
</s>
<s>
26,1	26,1	k4
</s>
<s>
11,6	11,6	k4
</s>
<s>
14,5	14,5	k4
</s>
<s>
3,59	3,59	k4
</s>
<s>
1931	#num#	k4
</s>
<s>
109	#num#	k4
</s>
<s>
2,804	2,804	k4
</s>
<s>
1,277	1,277	k4
</s>
<s>
1,527	1,527	k4
</s>
<s>
25,7	25,7	k4
</s>
<s>
11,7	11,7	k4
</s>
<s>
14,0	14,0	k4
</s>
<s>
3,53	3,53	k4
</s>
<s>
1932	#num#	k4
</s>
<s>
111	#num#	k4
</s>
<s>
2,696	2,696	k4
</s>
<s>
1,191	1,191	k4
</s>
<s>
1,505	1,505	k4
</s>
<s>
24,4	24,4	k4
</s>
<s>
10,8	10,8	k4
</s>
<s>
13,6	13,6	k4
</s>
<s>
3,31	3,31	k4
</s>
<s>
1933	#num#	k4
</s>
<s>
112	#num#	k4
</s>
<s>
2,531	2,531	k4
</s>
<s>
1,159	1,159	k4
</s>
<s>
1,372	1,372	k4
</s>
<s>
22,5	22,5	k4
</s>
<s>
10,3	10,3	k4
</s>
<s>
12,2	12,2	k4
</s>
<s>
3,07	3,07	k4
</s>
<s>
1934	#num#	k4
</s>
<s>
114	#num#	k4
</s>
<s>
2,597	2,597	k4
</s>
<s>
1,181	1,181	k4
</s>
<s>
1,416	1,416	k4
</s>
<s>
22,8	22,8	k4
</s>
<s>
10,4	10,4	k4
</s>
<s>
12,4	12,4	k4
</s>
<s>
3,10	3,10	k4
</s>
<s>
1935	#num#	k4
</s>
<s>
115	#num#	k4
</s>
<s>
2,551	2,551	k4
</s>
<s>
1,402	1,402	k4
</s>
<s>
1,149	1,149	k4
</s>
<s>
22,1	22,1	k4
</s>
<s>
12,2	12,2	k4
</s>
<s>
10,0	10,0	k4
</s>
<s>
2,99	2,99	k4
</s>
<s>
1936	#num#	k4
</s>
<s>
116	#num#	k4
</s>
<s>
2,557	2,557	k4
</s>
<s>
1,253	1,253	k4
</s>
<s>
1,304	1,304	k4
</s>
<s>
22,0	22,0	k4
</s>
<s>
10,8	10,8	k4
</s>
<s>
11,2	11,2	k4
</s>
<s>
2,98	2,98	k4
</s>
<s>
1937	#num#	k4
</s>
<s>
117	#num#	k4
</s>
<s>
2,397	2,397	k4
</s>
<s>
1,317	1,317	k4
</s>
<s>
1,080	1,080	k4
</s>
<s>
20,4	20,4	k4
</s>
<s>
11,2	11,2	k4
</s>
<s>
9,2	9,2	k4
</s>
<s>
2,78	2,78	k4
</s>
<s>
1938	#num#	k4
</s>
<s>
118	#num#	k4
</s>
<s>
2,374	2,374	k4
</s>
<s>
1,207	1,207	k4
</s>
<s>
1,167	1,167	k4
</s>
<s>
20,1	20,1	k4
</s>
<s>
10,2	10,2	k4
</s>
<s>
9,9	9,9	k4
</s>
<s>
2,71	2,71	k4
</s>
<s>
1939	#num#	k4
</s>
<s>
120	#num#	k4
</s>
<s>
2,363	2,363	k4
</s>
<s>
1,160	1,160	k4
</s>
<s>
1,203	1,203	k4
</s>
<s>
19,8	19,8	k4
</s>
<s>
9,7	9,7	k4
</s>
<s>
10,1	10,1	k4
</s>
<s>
2,68	2,68	k4
</s>
<s>
1940	#num#	k4
</s>
<s>
121	#num#	k4
</s>
<s>
2,480	2,480	k4
</s>
<s>
1,200	1,200	k4
</s>
<s>
1,280	1,280	k4
</s>
<s>
20,5	20,5	k4
</s>
<s>
9,9	9,9	k4
</s>
<s>
10,6	10,6	k4
</s>
<s>
2,75	2,75	k4
</s>
<s>
1941	#num#	k4
</s>
<s>
122	#num#	k4
</s>
<s>
2,634	2,634	k4
</s>
<s>
1,352	1,352	k4
</s>
<s>
1,282	1,282	k4
</s>
<s>
21,6	21,6	k4
</s>
<s>
11,1	11,1	k4
</s>
<s>
10,5	10,5	k4
</s>
<s>
2,91	2,91	k4
</s>
<s>
1942	#num#	k4
</s>
<s>
123	#num#	k4
</s>
<s>
3,005	3,005	k4
</s>
<s>
1,293	1,293	k4
</s>
<s>
1,712	1,712	k4
</s>
<s>
24,4	24,4	k4
</s>
<s>
10,5	10,5	k4
</s>
<s>
13,9	13,9	k4
</s>
<s>
3,26	3,26	k4
</s>
<s>
1943	#num#	k4
</s>
<s>
125	#num#	k4
</s>
<s>
3,173	3,173	k4
</s>
<s>
1,268	1,268	k4
</s>
<s>
1,905	1,905	k4
</s>
<s>
25,4	25,4	k4
</s>
<s>
10,1	10,1	k4
</s>
<s>
15,2	15,2	k4
</s>
<s>
3,36	3,36	k4
</s>
<s>
1944	#num#	k4
</s>
<s>
127	#num#	k4
</s>
<s>
3,213	3,213	k4
</s>
<s>
1,218	1,218	k4
</s>
<s>
1,995	1,995	k4
</s>
<s>
25,3	25,3	k4
</s>
<s>
9,6	9,6	k4
</s>
<s>
15,7	15,7	k4
</s>
<s>
3,34	3,34	k4
</s>
<s>
1945	#num#	k4
</s>
<s>
129	#num#	k4
</s>
<s>
3,434	3,434	k4
</s>
<s>
1,179	1,179	k4
</s>
<s>
2,255	2,255	k4
</s>
<s>
26,6	26,6	k4
</s>
<s>
9,1	9,1	k4
</s>
<s>
17,5	17,5	k4
</s>
<s>
3,55	3,55	k4
</s>
<s>
1946	#num#	k4
</s>
<s>
132	#num#	k4
</s>
<s>
3,434	3,434	k4
</s>
<s>
1,121	1,121	k4
</s>
<s>
2,313	2,313	k4
</s>
<s>
26,1	26,1	k4
</s>
<s>
8,5	8,5	k4
</s>
<s>
17,6	17,6	k4
</s>
<s>
3,47	3,47	k4
</s>
<s>
1947	#num#	k4
</s>
<s>
134	#num#	k4
</s>
<s>
3,703	3,703	k4
</s>
<s>
1,162	1,162	k4
</s>
<s>
2,541	2,541	k4
</s>
<s>
27,6	27,6	k4
</s>
<s>
8,6	8,6	k4
</s>
<s>
18,9	18,9	k4
</s>
<s>
3,67	3,67	k4
</s>
<s>
1948	#num#	k4
</s>
<s>
137	#num#	k4
</s>
<s>
3,821	3,821	k4
</s>
<s>
1,114	1,114	k4
</s>
<s>
2,707	2,707	k4
</s>
<s>
27,8	27,8	k4
</s>
<s>
8,1	8,1	k4
</s>
<s>
19,7	19,7	k4
</s>
<s>
3,72	3,72	k4
</s>
<s>
1949	#num#	k4
</s>
<s>
140	#num#	k4
</s>
<s>
3,884	3,884	k4
</s>
<s>
1,106	1,106	k4
</s>
<s>
2,778	2,778	k4
</s>
<s>
27,8	27,8	k4
</s>
<s>
7,9	7,9	k4
</s>
<s>
19,9	19,9	k4
</s>
<s>
3,73	3,73	k4
</s>
<s>
1950	#num#	k4
</s>
<s>
143	#num#	k4
</s>
<s>
4,093	4,093	k4
</s>
<s>
1,122	1,122	k4
</s>
<s>
2,971	2,971	k4
</s>
<s>
28,7	28,7	k4
</s>
<s>
7,9	7,9	k4
</s>
<s>
20,8	20,8	k4
</s>
<s>
3,86	3,86	k4
</s>
<s>
1951	#num#	k4
</s>
<s>
145	#num#	k4
</s>
<s>
3,999	3,999	k4
</s>
<s>
1,145	1,145	k4
</s>
<s>
2,854	2,854	k4
</s>
<s>
27,5	27,5	k4
</s>
<s>
7,9	7,9	k4
</s>
<s>
19,6	19,6	k4
</s>
<s>
3,72	3,72	k4
</s>
<s>
1952	#num#	k4
</s>
<s>
148	#num#	k4
</s>
<s>
4,075	4,075	k4
</s>
<s>
1,082	1,082	k4
</s>
<s>
2,993	2,993	k4
</s>
<s>
27,5	27,5	k4
</s>
<s>
7,3	7,3	k4
</s>
<s>
20,2	20,2	k4
</s>
<s>
3,79	3,79	k4
</s>
<s>
1953	#num#	k4
</s>
<s>
151	#num#	k4
</s>
<s>
4,254	4,254	k4
</s>
<s>
1,118	1,118	k4
</s>
<s>
3,136	3,136	k4
</s>
<s>
28,1	28,1	k4
</s>
<s>
7,4	7,4	k4
</s>
<s>
20,7	20,7	k4
</s>
<s>
3,94	3,94	k4
</s>
<s>
1954	#num#	k4
</s>
<s>
154	#num#	k4
</s>
<s>
4,281	4,281	k4
</s>
<s>
1,064	1,064	k4
</s>
<s>
3,217	3,217	k4
</s>
<s>
27,7	27,7	k4
</s>
<s>
6,9	6,9	k4
</s>
<s>
20,8	20,8	k4
</s>
<s>
3,91	3,91	k4
</s>
<s>
1955	#num#	k4
</s>
<s>
158	#num#	k4
</s>
<s>
4,505	4,505	k4
</s>
<s>
1,099	1,099	k4
</s>
<s>
3,406	3,406	k4
</s>
<s>
28,5	28,5	k4
</s>
<s>
7,0	7,0	k4
</s>
<s>
21,6	21,6	k4
</s>
<s>
4,07	4,07	k4
</s>
<s>
1956	#num#	k4
</s>
<s>
161	#num#	k4
</s>
<s>
4,603	4,603	k4
</s>
<s>
1,153	1,153	k4
</s>
<s>
3,450	3,450	k4
</s>
<s>
28,5	28,5	k4
</s>
<s>
7,1	7,1	k4
</s>
<s>
21,4	21,4	k4
</s>
<s>
4,14	4,14	k4
</s>
<s>
1957	#num#	k4
</s>
<s>
165	#num#	k4
</s>
<s>
4,725	4,725	k4
</s>
<s>
1,157	1,157	k4
</s>
<s>
3,568	3,568	k4
</s>
<s>
28,6	28,6	k4
</s>
<s>
7,0	7,0	k4
</s>
<s>
21,6	21,6	k4
</s>
<s>
4,20	4,20	k4
</s>
<s>
1958	#num#	k4
</s>
<s>
168	#num#	k4
</s>
<s>
4,641	4,641	k4
</s>
<s>
1,165	1,165	k4
</s>
<s>
3,476	3,476	k4
</s>
<s>
27,5	27,5	k4
</s>
<s>
6,9	6,9	k4
</s>
<s>
20,6	20,6	k4
</s>
<s>
4,09	4,09	k4
</s>
<s>
1959	#num#	k4
</s>
<s>
172	#num#	k4
</s>
<s>
4,837	4,837	k4
</s>
<s>
1,242	1,242	k4
</s>
<s>
3,595	3,595	k4
</s>
<s>
28,1	28,1	k4
</s>
<s>
7,2	7,2	k4
</s>
<s>
20,9	20,9	k4
</s>
<s>
4,24	4,24	k4
</s>
<s>
1960	#num#	k4
</s>
<s>
176	#num#	k4
</s>
<s>
4,916	4,916	k4
</s>
<s>
1,167	1,167	k4
</s>
<s>
3,749	3,749	k4
</s>
<s>
28,0	28,0	k4
</s>
<s>
6,6	6,6	k4
</s>
<s>
21,3	21,3	k4
</s>
<s>
4,29	4,29	k4
</s>
<s>
1961	#num#	k4
</s>
<s>
179	#num#	k4
</s>
<s>
4,563	4,563	k4
</s>
<s>
1,248	1,248	k4
</s>
<s>
3,315	3,315	k4
</s>
<s>
25,5	25,5	k4
</s>
<s>
7,0	7,0	k4
</s>
<s>
18,5	18,5	k4
</s>
<s>
3,88	3,88	k4
</s>
<s>
1962	#num#	k4
</s>
<s>
182	#num#	k4
</s>
<s>
4,711	4,711	k4
</s>
<s>
1,237	1,237	k4
</s>
<s>
3,474	3,474	k4
</s>
<s>
25,9	25,9	k4
</s>
<s>
6,8	6,8	k4
</s>
<s>
19,1	19,1	k4
</s>
<s>
3,98	3,98	k4
</s>
<s>
1963	#num#	k4
</s>
<s>
186	#num#	k4
</s>
<s>
4,820	4,820	k4
</s>
<s>
1,327	1,327	k4
</s>
<s>
3,493	3,493	k4
</s>
<s>
26,0	26,0	k4
</s>
<s>
7,2	7,2	k4
</s>
<s>
18,8	18,8	k4
</s>
<s>
4,02	4,02	k4
</s>
<s>
1964	#num#	k4
</s>
<s>
189	#num#	k4
</s>
<s>
4,787	4,787	k4
</s>
<s>
1,315	1,315	k4
</s>
<s>
3,472	3,472	k4
</s>
<s>
25,3	25,3	k4
</s>
<s>
7,0	7,0	k4
</s>
<s>
18,4	18,4	k4
</s>
<s>
3,87	3,87	k4
</s>
<s>
1965	#num#	k4
</s>
<s>
192	#num#	k4
</s>
<s>
4,721	4,721	k4
</s>
<s>
1,291	1,291	k4
</s>
<s>
3,430	3,430	k4
</s>
<s>
24,6	24,6	k4
</s>
<s>
6,7	6,7	k4
</s>
<s>
17,8	17,8	k4
</s>
<s>
3,73	3,73	k4
</s>
<s>
1966	#num#	k4
</s>
<s>
196	#num#	k4
</s>
<s>
4,692	4,692	k4
</s>
<s>
1,391	1,391	k4
</s>
<s>
3,301	3,301	k4
</s>
<s>
24,0	24,0	k4
</s>
<s>
7,1	7,1	k4
</s>
<s>
16,9	16,9	k4
</s>
<s>
3,60	3,60	k4
</s>
<s>
1967	#num#	k4
</s>
<s>
199	#num#	k4
</s>
<s>
4,404	4,404	k4
</s>
<s>
1,385	1,385	k4
</s>
<s>
3,019	3,019	k4
</s>
<s>
22,2	22,2	k4
</s>
<s>
7,0	7,0	k4
</s>
<s>
15,2	15,2	k4
</s>
<s>
3,28	3,28	k4
</s>
<s>
1968	#num#	k4
</s>
<s>
201	#num#	k4
</s>
<s>
4,227	4,227	k4
</s>
<s>
1,390	1,390	k4
</s>
<s>
2,837	2,837	k4
</s>
<s>
21,0	21,0	k4
</s>
<s>
6,9	6,9	k4
</s>
<s>
14,1	14,1	k4
</s>
<s>
3,06	3,06	k4
</s>
<s>
1969	#num#	k4
</s>
<s>
203	#num#	k4
</s>
<s>
4,218	4,218	k4
</s>
<s>
1,451	1,451	k4
</s>
<s>
2,767	2,767	k4
</s>
<s>
20,8	20,8	k4
</s>
<s>
7,2	7,2	k4
</s>
<s>
13,6	13,6	k4
</s>
<s>
2,99	2,99	k4
</s>
<s>
1970	#num#	k4
</s>
<s>
204	#num#	k4
</s>
<s>
4,023	4,023	k4
</s>
<s>
1,457	1,457	k4
</s>
<s>
2,566	2,566	k4
</s>
<s>
19,7	19,7	k4
</s>
<s>
7,1	7,1	k4
</s>
<s>
12,6	12,6	k4
</s>
<s>
2,79	2,79	k4
</s>
<s>
1971	#num#	k4
</s>
<s>
206	#num#	k4
</s>
<s>
4,277	4,277	k4
</s>
<s>
1,501	1,501	k4
</s>
<s>
2,776	2,776	k4
</s>
<s>
20,8	20,8	k4
</s>
<s>
7,3	7,3	k4
</s>
<s>
13,5	13,5	k4
</s>
<s>
2,86	2,86	k4
</s>
<s>
1972	#num#	k4
</s>
<s>
209	#num#	k4
</s>
<s>
4,676	4,676	k4
</s>
<s>
1,447	1,447	k4
</s>
<s>
3,229	3,229	k4
</s>
<s>
22,3	22,3	k4
</s>
<s>
6,9	6,9	k4
</s>
<s>
15,4	15,4	k4
</s>
<s>
3,09	3,09	k4
</s>
<s>
1973	#num#	k4
</s>
<s>
212	#num#	k4
</s>
<s>
4,598	4,598	k4
</s>
<s>
1,475	1,475	k4
</s>
<s>
3,123	3,123	k4
</s>
<s>
21,7	21,7	k4
</s>
<s>
6,9	6,9	k4
</s>
<s>
14,7	14,7	k4
</s>
<s>
2,95	2,95	k4
</s>
<s>
1974	#num#	k4
</s>
<s>
215	#num#	k4
</s>
<s>
4,276	4,276	k4
</s>
<s>
1,495	1,495	k4
</s>
<s>
2,781	2,781	k4
</s>
<s>
19,9	19,9	k4
</s>
<s>
6,9	6,9	k4
</s>
<s>
12,9	12,9	k4
</s>
<s>
2,66	2,66	k4
</s>
<s>
1975	#num#	k4
</s>
<s>
218	#num#	k4
</s>
<s>
4,384	4,384	k4
</s>
<s>
1,412	1,412	k4
</s>
<s>
2,972	2,972	k4
</s>
<s>
20,1	20,1	k4
</s>
<s>
6,5	6,5	k4
</s>
<s>
13,6	13,6	k4
</s>
<s>
2,71	2,71	k4
</s>
<s>
1976	#num#	k4
</s>
<s>
220	#num#	k4
</s>
<s>
4,291	4,291	k4
</s>
<s>
1,343	1,343	k4
</s>
<s>
2,948	2,948	k4
</s>
<s>
19,5	19,5	k4
</s>
<s>
6,1	6,1	k4
</s>
<s>
13,4	13,4	k4
</s>
<s>
2,53	2,53	k4
</s>
<s>
1977	#num#	k4
</s>
<s>
222	#num#	k4
</s>
<s>
3,996	3,996	k4
</s>
<s>
1,435	1,435	k4
</s>
<s>
2,561	2,561	k4
</s>
<s>
18,0	18,0	k4
</s>
<s>
6,5	6,5	k4
</s>
<s>
11,5	11,5	k4
</s>
<s>
2,32	2,32	k4
</s>
<s>
1978	#num#	k4
</s>
<s>
224	#num#	k4
</s>
<s>
4,162	4,162	k4
</s>
<s>
1,421	1,421	k4
</s>
<s>
2,741	2,741	k4
</s>
<s>
18,6	18,6	k4
</s>
<s>
6,4	6,4	k4
</s>
<s>
12,3	12,3	k4
</s>
<s>
2,36	2,36	k4
</s>
<s>
1979	#num#	k4
</s>
<s>
226	#num#	k4
</s>
<s>
4,475	4,475	k4
</s>
<s>
1,482	1,482	k4
</s>
<s>
2,993	2,993	k4
</s>
<s>
19,8	19,8	k4
</s>
<s>
6,6	6,6	k4
</s>
<s>
13,3	13,3	k4
</s>
<s>
2,49	2,49	k4
</s>
<s>
1980	#num#	k4
</s>
<s>
228	#num#	k4
</s>
<s>
4,528	4,528	k4
</s>
<s>
1,538	1,538	k4
</s>
<s>
2,990	2,990	k4
</s>
<s>
19,8	19,8	k4
</s>
<s>
6,7	6,7	k4
</s>
<s>
13,1	13,1	k4
</s>
<s>
2,49	2,49	k4
</s>
<s>
1981	#num#	k4
</s>
<s>
231	#num#	k4
</s>
<s>
4,345	4,345	k4
</s>
<s>
1,656	1,656	k4
</s>
<s>
2,689	2,689	k4
</s>
<s>
18,8	18,8	k4
</s>
<s>
7,2	7,2	k4
</s>
<s>
11,7	11,7	k4
</s>
<s>
2,33	2,33	k4
</s>
<s>
1982	#num#	k4
</s>
<s>
234	#num#	k4
</s>
<s>
4,337	4,337	k4
</s>
<s>
1,583	1,583	k4
</s>
<s>
2,754	2,754	k4
</s>
<s>
18,5	18,5	k4
</s>
<s>
6,8	6,8	k4
</s>
<s>
11,8	11,8	k4
</s>
<s>
2,26	2,26	k4
</s>
<s>
1983	#num#	k4
</s>
<s>
237	#num#	k4
</s>
<s>
4,371	4,371	k4
</s>
<s>
1,653	1,653	k4
</s>
<s>
2,718	2,718	k4
</s>
<s>
18,4	18,4	k4
</s>
<s>
7,0	7,0	k4
</s>
<s>
11,5	11,5	k4
</s>
<s>
2,24	2,24	k4
</s>
<s>
1984	#num#	k4
</s>
<s>
240	#num#	k4
</s>
<s>
4,113	4,113	k4
</s>
<s>
1,584	1,584	k4
</s>
<s>
2,529	2,529	k4
</s>
<s>
17,2	17,2	k4
</s>
<s>
6,6	6,6	k4
</s>
<s>
10,6	10,6	k4
</s>
<s>
2,08	2,08	k4
</s>
<s>
1985	#num#	k4
</s>
<s>
241	#num#	k4
</s>
<s>
3,856	3,856	k4
</s>
<s>
1,652	1,652	k4
</s>
<s>
2,204	2,204	k4
</s>
<s>
16,0	16,0	k4
</s>
<s>
6,8	6,8	k4
</s>
<s>
9,1	9,1	k4
</s>
<s>
1,93	1,93	k4
</s>
<s>
1986	#num#	k4
</s>
<s>
243	#num#	k4
</s>
<s>
3,881	3,881	k4
</s>
<s>
1,598	1,598	k4
</s>
<s>
2,283	2,283	k4
</s>
<s>
16,0	16,0	k4
</s>
<s>
6,6	6,6	k4
</s>
<s>
9,4	9,4	k4
</s>
<s>
1,92	1,92	k4
</s>
<s>
1987	#num#	k4
</s>
<s>
246	#num#	k4
</s>
<s>
4,193	4,193	k4
</s>
<s>
1,724	1,724	k4
</s>
<s>
2,469	2,469	k4
</s>
<s>
17,0	17,0	k4
</s>
<s>
7,0	7,0	k4
</s>
<s>
10,0	10,0	k4
</s>
<s>
2,05	2,05	k4
</s>
<s>
1988	#num#	k4
</s>
<s>
250	#num#	k4
</s>
<s>
4,673	4,673	k4
</s>
<s>
1,818	1,818	k4
</s>
<s>
2,855	2,855	k4
</s>
<s>
18,7	18,7	k4
</s>
<s>
7,3	7,3	k4
</s>
<s>
11,4	11,4	k4
</s>
<s>
2,20	2,20	k4
</s>
<s>
1989	#num#	k4
</s>
<s>
253	#num#	k4
</s>
<s>
4,560	4,560	k4
</s>
<s>
1,716	1,716	k4
</s>
<s>
2,844	2,844	k4
</s>
<s>
18,0	18,0	k4
</s>
<s>
6,8	6,8	k4
</s>
<s>
11,3	11,3	k4
</s>
<s>
2,17	2,17	k4
</s>
<s>
1990	#num#	k4
</s>
<s>
255	#num#	k4
</s>
<s>
4,768	4,768	k4
</s>
<s>
1,704	1,704	k4
</s>
<s>
3,064	3,064	k4
</s>
<s>
18,7	18,7	k4
</s>
<s>
6,7	6,7	k4
</s>
<s>
12,0	12,0	k4
</s>
<s>
2,30	2,30	k4
</s>
<s>
1991	#num#	k4
</s>
<s>
258	#num#	k4
</s>
<s>
4,533	4,533	k4
</s>
<s>
1,796	1,796	k4
</s>
<s>
2,737	2,737	k4
</s>
<s>
17,6	17,6	k4
</s>
<s>
7,0	7,0	k4
</s>
<s>
10,6	10,6	k4
</s>
<s>
2,19	2,19	k4
</s>
<s>
1992	#num#	k4
</s>
<s>
261	#num#	k4
</s>
<s>
4,609	4,609	k4
</s>
<s>
1,719	1,719	k4
</s>
<s>
2,890	2,890	k4
</s>
<s>
17,7	17,7	k4
</s>
<s>
6,6	6,6	k4
</s>
<s>
11,1	11,1	k4
</s>
<s>
2,21	2,21	k4
</s>
<s>
1993	#num#	k4
</s>
<s>
264	#num#	k4
</s>
<s>
4,623	4,623	k4
</s>
<s>
1,753	1,753	k4
</s>
<s>
2,870	2,870	k4
</s>
<s>
17,5	17,5	k4
</s>
<s>
6,6	6,6	k4
</s>
<s>
10,9	10,9	k4
</s>
<s>
2,22	2,22	k4
</s>
<s>
1994	#num#	k4
</s>
<s>
266	#num#	k4
</s>
<s>
4,442	4,442	k4
</s>
<s>
1,717	1,717	k4
</s>
<s>
2,725	2,725	k4
</s>
<s>
16,7	16,7	k4
</s>
<s>
6,5	6,5	k4
</s>
<s>
10,2	10,2	k4
</s>
<s>
2,14	2,14	k4
</s>
<s>
1995	#num#	k4
</s>
<s>
267	#num#	k4
</s>
<s>
4,280	4,280	k4
</s>
<s>
1,923	1,923	k4
</s>
<s>
2,357	2,357	k4
</s>
<s>
16,0	16,0	k4
</s>
<s>
7,2	7,2	k4
</s>
<s>
8,8	8,8	k4
</s>
<s>
2,08	2,08	k4
</s>
<s>
1996	#num#	k4
</s>
<s>
269	#num#	k4
</s>
<s>
4,329	4,329	k4
</s>
<s>
1,879	1,879	k4
</s>
<s>
2,450	2,450	k4
</s>
<s>
16,1	16,1	k4
</s>
<s>
7,0	7,0	k4
</s>
<s>
9,1	9,1	k4
</s>
<s>
2,13	2,13	k4
</s>
<s>
1997	#num#	k4
</s>
<s>
271	#num#	k4
</s>
<s>
4,151	4,151	k4
</s>
<s>
1,843	1,843	k4
</s>
<s>
2,308	2,308	k4
</s>
<s>
15,3	15,3	k4
</s>
<s>
6,8	6,8	k4
</s>
<s>
8,5	8,5	k4
</s>
<s>
2,04	2,04	k4
</s>
<s>
1998	#num#	k4
</s>
<s>
274	#num#	k4
</s>
<s>
4,178	4,178	k4
</s>
<s>
1,821	1,821	k4
</s>
<s>
2,357	2,357	k4
</s>
<s>
15,2	15,2	k4
</s>
<s>
6,6	6,6	k4
</s>
<s>
8,6	8,6	k4
</s>
<s>
2,04	2,04	k4
</s>
<s>
1999	#num#	k4
</s>
<s>
277	#num#	k4
</s>
<s>
4,100	4,100	k4
</s>
<s>
1,901	1,901	k4
</s>
<s>
2,199	2,199	k4
</s>
<s>
14,8	14,8	k4
</s>
<s>
6,9	6,9	k4
</s>
<s>
7,9	7,9	k4
</s>
<s>
1,99	1,99	k4
</s>
<s>
2000	#num#	k4
</s>
<s>
281	#num#	k4
</s>
<s>
4,315	4,315	k4
</s>
<s>
1,823	1,823	k4
</s>
<s>
2,492	2,492	k4
</s>
<s>
15,3	15,3	k4
</s>
<s>
6,5	6,5	k4
</s>
<s>
8,9	8,9	k4
</s>
<s>
2,06	2,06	k4
</s>
<s>
2001	#num#	k4
</s>
<s>
285	#num#	k4
</s>
<s>
4,091	4,091	k4
</s>
<s>
1,725	1,725	k4
</s>
<s>
2,366	2,366	k4
</s>
<s>
14,4	14,4	k4
</s>
<s>
6,1	6,1	k4
</s>
<s>
8,3	8,3	k4
</s>
<s>
1,95	1,95	k4
</s>
<s>
2002	#num#	k4
</s>
<s>
288	#num#	k4
</s>
<s>
4,049	4,049	k4
</s>
<s>
1,821	1,821	k4
</s>
<s>
2,228	2,228	k4
</s>
<s>
14,1	14,1	k4
</s>
<s>
6,3	6,3	k4
</s>
<s>
7,7	7,7	k4
</s>
<s>
1,94	1,94	k4
</s>
<s>
2003	#num#	k4
</s>
<s>
290	#num#	k4
</s>
<s>
4,143	4,143	k4
</s>
<s>
1,827	1,827	k4
</s>
<s>
2,316	2,316	k4
</s>
<s>
14,3	14,3	k4
</s>
<s>
6,3	6,3	k4
</s>
<s>
8,0	8,0	k4
</s>
<s>
1,99	1,99	k4
</s>
<s>
2004	#num#	k4
</s>
<s>
292	#num#	k4
</s>
<s>
4,234	4,234	k4
</s>
<s>
1,824	1,824	k4
</s>
<s>
2,410	2,410	k4
</s>
<s>
14,5	14,5	k4
</s>
<s>
6,2	6,2	k4
</s>
<s>
8,3	8,3	k4
</s>
<s>
2,04	2,04	k4
</s>
<s>
2005	#num#	k4
</s>
<s>
297	#num#	k4
</s>
<s>
4,280	4,280	k4
</s>
<s>
1,838	1,838	k4
</s>
<s>
2,442	2,442	k4
</s>
<s>
14,4	14,4	k4
</s>
<s>
6,2	6,2	k4
</s>
<s>
8,2	8,2	k4
</s>
<s>
2,05	2,05	k4
</s>
<s>
2006	#num#	k4
</s>
<s>
304	#num#	k4
</s>
<s>
4,415	4,415	k4
</s>
<s>
1,903	1,903	k4
</s>
<s>
2,512	2,512	k4
</s>
<s>
14,5	14,5	k4
</s>
<s>
6,3	6,3	k4
</s>
<s>
8,3	8,3	k4
</s>
<s>
2,07	2,07	k4
</s>
<s>
2007	#num#	k4
</s>
<s>
312	#num#	k4
</s>
<s>
4,560	4,560	k4
</s>
<s>
1,943	1,943	k4
</s>
<s>
2,617	2,617	k4
</s>
<s>
14,6	14,6	k4
</s>
<s>
6,2	6,2	k4
</s>
<s>
8,4	8,4	k4
</s>
<s>
2,09	2,09	k4
</s>
<s>
2008	#num#	k4
</s>
<s>
317	#num#	k4
</s>
<s>
4,835	4,835	k4
</s>
<s>
2,186	2,186	k4
</s>
<s>
2,244	2,244	k4
</s>
<s>
14,2	14,2	k4
</s>
<s>
6,2	6,2	k4
</s>
<s>
7,0	7,0	k4
</s>
<s>
2,14	2,14	k4
</s>
<s>
2009	#num#	k4
</s>
<s>
318	#num#	k4
</s>
<s>
5,026	5,026	k4
</s>
<s>
2,002	2,002	k4
</s>
<s>
3,024	3,024	k4
</s>
<s>
15,7	15,7	k4
</s>
<s>
6,3	6,3	k4
</s>
<s>
9,4	9,4	k4
</s>
<s>
2,22	2,22	k4
</s>
<s>
2010	#num#	k4
</s>
<s>
318	#num#	k4
</s>
<s>
4,907	4,907	k4
</s>
<s>
2,020	2,020	k4
</s>
<s>
2,887	2,887	k4
</s>
<s>
15,5	15,5	k4
</s>
<s>
6,4	6,4	k4
</s>
<s>
9,1	9,1	k4
</s>
<s>
2,20	2,20	k4
</s>
<s>
2011	#num#	k4
</s>
<s>
319	#num#	k4
</s>
<s>
4,492	4,492	k4
</s>
<s>
1,985	1,985	k4
</s>
<s>
2,511	2,511	k4
</s>
<s>
14,1	14,1	k4
</s>
<s>
6,2	6,2	k4
</s>
<s>
7,9	7,9	k4
</s>
<s>
2,02	2,02	k4
</s>
<s>
2012	#num#	k4
</s>
<s>
321	#num#	k4
</s>
<s>
4,533	4,533	k4
<g/>
1,960	1,960	k4
<g/>
2,573	2,573	k4
</s>
<s>
14,26	14,26	k4
<g/>
,18	,18	k4
<g/>
,1	,1	k4
</s>
<s>
2,04	2,04	k4
</s>
<s>
2013	#num#	k4
</s>
<s>
324	#num#	k4
</s>
<s>
4,326	4,326	k4
<g/>
2,150	2,150	k4
<g/>
2,180	2,180	k4
</s>
<s>
13,46	13,46	k4
<g/>
,66	,66	k4
<g/>
,7	,7	k4
</s>
<s>
1,93	1,93	k4
</s>
<s>
2014	#num#	k4
</s>
<s>
329	#num#	k4
</s>
<s>
4,375	4,375	k4
<g/>
2,100	2,100	k4
<g/>
2,275	2,275	k4
</s>
<s>
13,56	13,56	k4
<g/>
,47	,47	k4
<g/>
,1	,1	k4
</s>
<s>
1,93	1,93	k4
</s>
<s>
Data	datum	k1gNnPc1
z	z	k7c2
CIA	CIA	kA
World	World	k1gInSc1
Factbook	Factbook	k1gInSc1
</s>
<s>
Věková	věkový	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
</s>
<s>
0	#num#	k4
<g/>
–	–	k?
<g/>
14	#num#	k4
let	léto	k1gNnPc2
<g/>
:	:	kIx,
</s>
<s>
19,8	19,8	k4
%	%	kIx~
</s>
<s>
15	#num#	k4
<g/>
–	–	k?
<g/>
24	#num#	k4
let	léto	k1gNnPc2
<g/>
:	:	kIx,
</s>
<s>
14,6	14,6	k4
%	%	kIx~
</s>
<s>
25-54	25-54	k4
let	let	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
40,9	40,9	k4
%	%	kIx~
</s>
<s>
55-64	55-64	k4
let	let	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
11,4	11,4	k4
%	%	kIx~
</s>
<s>
65	#num#	k4
let	let	k1gInSc4
a	a	k8xC
víc	hodně	k6eAd2
<g/>
:	:	kIx,
</s>
<s>
13,2	13,2	k4
%	%	kIx~
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
,	,	kIx,
odhad	odhad	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Naděje	naděje	k1gFnPc1
dožití	dožití	k1gNnSc1
</s>
<s>
celkem	celkem	k6eAd1
<g/>
:	:	kIx,
</s>
<s>
81,11	81,11	k4
let	léto	k1gNnPc2
</s>
<s>
muži	muž	k1gMnPc1
<g/>
:	:	kIx,
</s>
<s>
78,89	78,89	k4
let	léto	k1gNnPc2
</s>
<s>
ženy	žena	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
83,42	83,42	k4
let	léto	k1gNnPc2
</s>
<s>
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
,	,	kIx,
odhad	odhad	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Náboženství	náboženství	k1gNnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
<g/>
:	:	kIx,
luteráni	luterán	k1gMnPc1
87,1	87,1	k4
%	%	kIx~
<g/>
,	,	kIx,
protestanti	protestant	k1gMnPc1
4,1	4,1	k4
%	%	kIx~
<g/>
,	,	kIx,
římští	římský	k2eAgMnPc1d1
katolíci	katolík	k1gMnPc1
1,7	1,7	k4
%	%	kIx~
a	a	k8xC
ostatní	ostatní	k2eAgMnSc1d1
7,1	7,1	k4
<g/>
%	%	kIx~
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Demographics	Demographicsa	k1gFnPc2
of	of	k?
Iceland	Icelanda	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
People	People	k1gFnSc1
and	and	k?
Society	societa	k1gFnSc2
<g/>
:	:	kIx,
Ukraine	Ukrain	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CIA	CIA	kA
World	World	k1gInSc1
Factbook	Factbook	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
mtDNA	mtDNA	k?
and	and	k?
the	the	k?
Islands	Islands	k1gInSc1
of	of	k?
the	the	k?
North	North	k1gInSc1
Atlantic	Atlantice	k1gFnPc2
<g/>
:	:	kIx,
Estimating	Estimating	k1gInSc1
the	the	k?
Proportions	Proportions	k1gInSc1
of	of	k?
Norse	Norse	k1gFnSc1
and	and	k?
Gaelic	Gaelice	k1gFnPc2
Ancestry	Ancestra	k1gFnSc2
<g/>
,	,	kIx,
Agnar	Agnar	k1gMnSc1
Helgason	Helgason	k1gMnSc1
<g/>
,	,	kIx,
Eileen	Eileen	k1gInSc1
Hickey	Hickea	k1gFnSc2
<g/>
,	,	kIx,
Sara	Sarum	k1gNnSc2
Goodacre	Goodacr	k1gInSc5
<g/>
,	,	kIx,
Vidar	Vidar	k1gMnSc1
Bosnes	Bosnes	k1gMnSc1
<g/>
,	,	kIx,
Ka	Ka	k1gMnSc1
<g/>
´	´	k?
<g/>
ri	ri	k?
Stefa	Stef	k1gMnSc2
<g/>
´	´	k?
<g/>
nsson	nsson	k1gInSc1
<g/>
,	,	kIx,
Ryk	ryk	k1gInSc1
Ward	Ward	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
Bryan	Bryan	k1gMnSc1
Sykes	Sykes	k1gMnSc1
<g/>
,	,	kIx,
Am	Am	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
J.	J.	kA
Hum	hum	k0wR
<g/>
.	.	kIx.
</s>
<s desamb="1">
Genet	Genet	k1gInSc1
<g/>
.	.	kIx.
68	#num#	k4
<g/>
:	:	kIx,
<g/>
723	#num#	k4
<g/>
–	–	k?
<g/>
737	#num#	k4
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
,	,	kIx,
http://www.journals.uchicago.edu/AJHG/journal/issues/v68n3/002146/002146.web.pdf	http://www.journals.uchicago.edu/AJHG/journal/issues/v68n3/002146/002146.web.pdf	k1gInSc1
and	and	k?
mtDNA	mtDNA	k?
and	and	k?
the	the	k?
Origin	Origin	k1gInSc1
of	of	k?
the	the	k?
Icelanders	Icelanders	k1gInSc1
<g/>
:	:	kIx,
Deciphering	Deciphering	k1gInSc1
Signals	Signals	k1gInSc1
of	of	k?
Recent	Recent	k1gInSc1
Population	Population	k1gInSc4
History	Histor	k1gInPc7
<g/>
,	,	kIx,
Agnar	Agnar	k1gMnSc1
Helgason	Helgason	k1gMnSc1
<g/>
,	,	kIx,
Sigrún	Sigrún	k1gMnSc1
Sigurð	Sigurð	k1gMnSc1
<g/>
,	,	kIx,
Jeffrey	Jeffrea	k1gFnPc1
R.	R.	kA
Gulcher	Gulchra	k1gFnPc2
<g/>
,	,	kIx,
Ryk	ryk	k1gInSc1
Ward	Ward	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
Kári	Kári	k1gNnSc2
Stefánsson	Stefánssona	k1gFnPc2
<g/>
,	,	kIx,
Am	Am	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
J.	J.	kA
Hum	hum	k0wR
<g/>
.	.	kIx.
</s>
<s desamb="1">
Genet	Genet	k1gInSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
66	#num#	k4
<g/>
:	:	kIx,
<g/>
999	#num#	k4
<g/>
-	-	kIx~
<g/>
1016	#num#	k4
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
,	,	kIx,
http://www.journals.uchicago.edu/AJHG/journal/issues/v66n3/991226/991226.html	http://www.journals.uchicago.edu/AJHG/journal/issues/v66n3/991226/991226.htmnout	k5eAaPmAgInS
<g/>
↑	↑	k?
Statistics	Statistics	k1gInSc1
Iceland	Iceland	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Obyvatelstvo	obyvatelstvo	k1gNnSc1
Evropy	Evropa	k1gFnSc2
Země	zem	k1gFnSc2
</s>
<s>
Albánie	Albánie	k1gFnSc1
</s>
<s>
Andorra	Andorra	k1gFnSc1
</s>
<s>
Arménie	Arménie	k1gFnSc1
</s>
<s>
Ázerbájdžán	Ázerbájdžán	k1gInSc1
</s>
<s>
Belgie	Belgie	k1gFnSc1
</s>
<s>
Bělorusko	Bělorusko	k1gNnSc1
</s>
<s>
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
Dánsko	Dánsko	k1gNnSc1
</s>
<s>
Estonsko	Estonsko	k1gNnSc1
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
Gruzie	Gruzie	k1gFnSc1
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1
</s>
<s>
Irsko	Irsko	k1gNnSc1
</s>
<s>
Island	Island	k1gInSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
Kazachstán	Kazachstán	k1gInSc1
</s>
<s>
Kypr	Kypr	k1gInSc1
</s>
<s>
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1
</s>
<s>
Litva	Litva	k1gFnSc1
</s>
<s>
Lotyšsko	Lotyšsko	k1gNnSc1
</s>
<s>
Lucembursko	Lucembursko	k1gNnSc1
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
Malta	Malta	k1gFnSc1
</s>
<s>
Moldavsko	Moldavsko	k1gNnSc1
</s>
<s>
Monako	Monako	k1gNnSc1
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
</s>
<s>
Norsko	Norsko	k1gNnSc1
</s>
<s>
Polsko	Polsko	k1gNnSc1
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Řecko	Řecko	k1gNnSc1
</s>
<s>
San	San	k?
Marino	Marina	k1gFnSc5
</s>
<s>
Severní	severní	k2eAgFnSc1d1
Makedonie	Makedonie	k1gFnSc1
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
Slovinsko	Slovinsko	k1gNnSc1
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Srbsko	Srbsko	k1gNnSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
Turecko	Turecko	k1gNnSc1
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1
</s>
<s>
Vatikán	Vatikán	k1gInSc1
Teritoria	teritorium	k1gNnSc2
<g/>
,	,	kIx,
koloniea	koloniea	k6eAd1
zámořská	zámořský	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Akrotiri	Akrotiri	k6eAd1
a	a	k8xC
Dekelia	Dekelia	k1gFnSc1
(	(	kIx(
<g/>
GB	GB	kA
<g/>
)	)	kIx)
</s>
<s>
Athos	Athos	k1gMnSc1
(	(	kIx(
<g/>
GR	GR	kA
<g/>
)	)	kIx)
</s>
<s>
Alandy	Alanda	k1gFnPc1
(	(	kIx(
<g/>
FI	fi	k0
<g/>
)	)	kIx)
</s>
<s>
Faerské	Faerský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
(	(	kIx(
<g/>
DK	DK	kA
<g/>
)	)	kIx)
</s>
<s>
Gibraltar	Gibraltar	k1gInSc1
(	(	kIx(
<g/>
GB	GB	kA
<g/>
)	)	kIx)
</s>
<s>
Guernsey	Guernsea	k1gFnPc1
(	(	kIx(
<g/>
GB	GB	kA
<g/>
)	)	kIx)
</s>
<s>
Jan	Jan	k1gMnSc1
Mayen	Mayna	k1gFnPc2
(	(	kIx(
<g/>
NO	no	k9
<g/>
)	)	kIx)
</s>
<s>
Jersey	Jersea	k1gFnPc1
(	(	kIx(
<g/>
GB	GB	kA
<g/>
)	)	kIx)
</s>
<s>
Man	Man	k1gMnSc1
(	(	kIx(
<g/>
GB	GB	kA
<g/>
)	)	kIx)
</s>
<s>
Špicberky	Špicberky	k1gFnPc1
(	(	kIx(
<g/>
NO	no	k9
<g/>
)	)	kIx)
Území	území	k1gNnSc1
se	s	k7c7
sporným	sporný	k2eAgNnSc7d1
postavením	postavení	k1gNnSc7
</s>
<s>
Kosovo	Kosův	k2eAgNnSc1d1
</s>
<s>
Arcach	Arcach	k1gMnSc1
</s>
<s>
Podněstří	Podněstřit	k5eAaPmIp3nS
</s>
<s>
Severní	severní	k2eAgInSc1d1
Kypr	Kypr	k1gInSc1
</s>
<s>
Abcházie	Abcházie	k1gFnSc1
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Osetie	Osetie	k1gFnSc1
</s>
