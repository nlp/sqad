Většina	většina	k1gFnSc1
Islanďanů	Islanďan	k1gMnPc2
jsou	být	k5eAaImIp3nP
potomci	potomek	k1gMnPc1
norských	norský	k2eAgMnPc2d1
osadníků	osadník	k1gMnPc2
a	a	k8xC
keltů	kelta	k1gMnPc2
z	z	k7c2
Irska	Irsko	k1gNnSc2
a	a	k8xC
Skotska	Skotsko	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc4,k3yRgNnSc4,k3yIgNnSc4
sem	sem	k6eAd1
byli	být	k5eAaImAgMnP
dovezeni	dovézt	k5eAaPmNgMnP
jako	jako	k9
otroci	otrok	k1gMnPc1
v	v	k7c6
době	doba	k1gFnSc6
osídlení	osídlení	k1gNnSc2
<g/>
.	.	kIx.
