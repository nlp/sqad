<s>
Filmový	filmový	k2eAgInSc1d1	filmový
festival	festival	k1gInSc1	festival
v	v	k7c6	v
Cannes	Cannes	k1gNnSc6	Cannes
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
:	:	kIx,	:
le	le	k?	le
Festival	festival	k1gInSc1	festival
de	de	k?	de
Cannes	Cannes	k1gNnSc2	Cannes
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejstarší	starý	k2eAgInPc4d3	nejstarší
a	a	k8xC	a
nejprestižnější	prestižní	k2eAgInPc4d3	nejprestižnější
filmové	filmový	k2eAgInPc4d1	filmový
festivaly	festival	k1gInPc4	festival
vedle	vedle	k7c2	vedle
festivalů	festival	k1gInPc2	festival
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
nebo	nebo	k8xC	nebo
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
