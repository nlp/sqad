<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Zagorová	Zagorová	k1gFnSc1	Zagorová
(	(	kIx(	(
<g/>
*	*	kIx~	*
6	[number]	k4	6
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1946	[number]	k4	1946
Petřkovice	Petřkovice	k1gFnPc1	Petřkovice
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
,	,	kIx,	,
textařka	textařka	k1gFnSc1	textařka
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
moderátorka	moderátorka	k1gFnSc1	moderátorka
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nejznámějším	známý	k2eAgFnPc3d3	nejznámější
českým	český	k2eAgFnPc3d1	Česká
a	a	k8xC	a
slovenským	slovenský	k2eAgFnPc3d1	slovenská
zpěvačkám	zpěvačka	k1gFnPc3	zpěvačka
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
pěvecké	pěvecký	k2eAgFnSc2d1	pěvecká
kariéry	kariéra	k1gFnSc2	kariéra
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
druhou	druhý	k4xOgFnSc7	druhý
nejúspěšnější	úspěšný	k2eAgFnSc7d3	nejúspěšnější
držitelkou	držitelka	k1gFnSc7	držitelka
ocenění	ocenění	k1gNnSc2	ocenění
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
slavík	slavík	k1gMnSc1	slavík
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
(	(	kIx(	(
<g/>
po	po	k7c6	po
Lucii	Lucie	k1gFnSc6	Lucie
Bílé	bílý	k2eAgFnSc6d1	bílá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
když	když	k8xS	když
získala	získat	k5eAaPmAgFnS	získat
devět	devět	k4xCc4	devět
Slavíků	slavík	k1gInPc2	slavík
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1977	[number]	k4	1977
až	až	k9	až
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
Nazpívala	nazpívat	k5eAaPmAgFnS	nazpívat
přes	přes	k7c4	přes
895	[number]	k4	895
písní	píseň	k1gFnPc2	píseň
a	a	k8xC	a
za	za	k7c4	za
hudbu	hudba	k1gFnSc4	hudba
a	a	k8xC	a
zpěv	zpěv	k1gInSc4	zpěv
získala	získat	k5eAaPmAgFnS	získat
mnoho	mnoho	k4c4	mnoho
prestižních	prestižní	k2eAgFnPc2d1	prestižní
cen	cena	k1gFnPc2	cena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Soukromý	soukromý	k2eAgInSc4d1	soukromý
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Zagorová	Zagorová	k1gFnSc1	Zagorová
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
a	a	k8xC	a
vyrůstala	vyrůstat	k5eAaImAgFnS	vyrůstat
na	na	k7c6	na
Ostravsku	Ostravsko	k1gNnSc6	Ostravsko
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
Josef	Josef	k1gMnSc1	Josef
Zagora	Zagor	k1gMnSc4	Zagor
byl	být	k5eAaImAgMnS	být
stavebním	stavební	k2eAgMnSc7d1	stavební
inženýrem	inženýr	k1gMnSc7	inženýr
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
Edeltruda	Edeltruda	k1gFnSc1	Edeltruda
učitelkou	učitelka	k1gFnSc7	učitelka
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
manželem	manžel	k1gMnSc7	manžel
Hany	Hana	k1gFnSc2	Hana
Zagorové	Zagorová	k1gFnSc2	Zagorová
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1986	[number]	k4	1986
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
tanečník	tanečník	k1gMnSc1	tanečník
a	a	k8xC	a
baletní	baletní	k2eAgMnSc1d1	baletní
mistr	mistr	k1gMnSc1	mistr
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Harapes	Harapes	k1gMnSc1	Harapes
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozvodu	rozvod	k1gInSc6	rozvod
s	s	k7c7	s
Harapesem	Harapes	k1gMnSc7	Harapes
si	se	k3xPyFc3	se
vzala	vzít	k5eAaPmAgFnS	vzít
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1992	[number]	k4	1992
slovenského	slovenský	k2eAgInSc2d1	slovenský
tenoristu	tenorista	k1gMnSc4	tenorista
Štefana	Štefan	k1gMnSc4	Štefan
Margitu	Margita	k1gMnSc4	Margita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Profesní	profesní	k2eAgFnSc1d1	profesní
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
60	[number]	k4	60
<g/>
.	.	kIx.	.
léta	léto	k1gNnPc4	léto
–	–	k?	–
začátky	začátek	k1gInPc4	začátek
===	===	k?	===
</s>
</p>
<p>
<s>
Poprvé	poprvé	k6eAd1	poprvé
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
pěvecky	pěvecky	k6eAd1	pěvecky
upozornila	upozornit	k5eAaPmAgFnS	upozornit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
v	v	k7c6	v
ostravské	ostravský	k2eAgFnSc6d1	Ostravská
soutěži	soutěž	k1gFnSc6	soutěž
Hledáme	hledat	k5eAaImIp1nP	hledat
nové	nový	k2eAgInPc4d1	nový
talenty	talent	k1gInPc4	talent
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ji	on	k3xPp3gFnSc4	on
doprovázel	doprovázet	k5eAaImAgInS	doprovázet
Orchestr	orchestr	k1gInSc1	orchestr
Gustava	Gustav	k1gMnSc2	Gustav
Broma	Brom	k1gMnSc2	Brom
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1964	[number]	k4	1964
<g/>
–	–	k?	–
<g/>
1968	[number]	k4	1968
studovala	studovat	k5eAaImAgFnS	studovat
a	a	k8xC	a
následně	následně	k6eAd1	následně
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
herectví	herectví	k1gNnSc4	herectví
na	na	k7c6	na
brněnském	brněnský	k2eAgInSc6d1	brněnský
JAMU	jam	k1gInSc6	jam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
vydala	vydat	k5eAaPmAgFnS	vydat
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
pěvecký	pěvecký	k2eAgInSc4d1	pěvecký
singl	singl	k1gInSc4	singl
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
Svatej	svatat	k5eAaImRp2nS	svatat
kluk	kluk	k1gMnSc1	kluk
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
častěji	často	k6eAd2	často
objevovat	objevovat	k5eAaImF	objevovat
na	na	k7c6	na
televizní	televizní	k2eAgFnSc6d1	televizní
obrazovce	obrazovka	k1gFnSc6	obrazovka
<g/>
.	.	kIx.	.
</s>
<s>
Účinkovala	účinkovat	k5eAaImAgFnS	účinkovat
se	se	k3xPyFc4	se
skupinou	skupina	k1gFnSc7	skupina
Flamingo	Flamingo	k6eAd1	Flamingo
a	a	k8xC	a
natáčela	natáčet	k5eAaImAgFnS	natáčet
v	v	k7c6	v
ostravském	ostravský	k2eAgInSc6d1	ostravský
rozhlase	rozhlas	k1gInSc6	rozhlas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
ujal	ujmout	k5eAaPmAgInS	ujmout
termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
beatový	beatový	k2eAgInSc1d1	beatový
šanson	šanson	k1gInSc1	šanson
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
poukazoval	poukazovat	k5eAaImAgInS	poukazovat
na	na	k7c4	na
přesvědčivost	přesvědčivost	k1gFnSc4	přesvědčivost
jí	jíst	k5eAaImIp3nS	jíst
podaného	podaný	k2eAgInSc2d1	podaný
textu	text	k1gInSc2	text
písně	píseň	k1gFnSc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgInSc6	ten
jí	jíst	k5eAaImIp3nS	jíst
pomáhala	pomáhat	k5eAaImAgFnS	pomáhat
i	i	k9	i
její	její	k3xOp3gFnSc1	její
herecká	herecký	k2eAgFnSc1d1	herecká
průprava	průprava	k1gFnSc1	průprava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dále	daleko	k6eAd2	daleko
účinkovala	účinkovat	k5eAaImAgFnS	účinkovat
v	v	k7c6	v
souboru	soubor	k1gInSc6	soubor
Sodoma-Gomora	Sodoma-Gomora	k1gFnSc1	Sodoma-Gomora
(	(	kIx(	(
<g/>
Zagorová	Zagorová	k1gFnSc1	Zagorová
<g/>
,	,	kIx,	,
Štědroň	Štědroň	k1gFnSc1	Štědroň
<g/>
,	,	kIx,	,
Sodoma	Sodoma	k1gFnSc1	Sodoma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Začala	začít	k5eAaPmAgFnS	začít
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
s	s	k7c7	s
orchestrem	orchestr	k1gInSc7	orchestr
Václava	Václav	k1gMnSc2	Václav
Zahradníka	Zahradník	k1gMnSc2	Zahradník
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgInSc7	který
natočila	natočit	k5eAaBmAgFnS	natočit
své	své	k1gNnSc4	své
první	první	k4xOgFnSc2	první
LP	LP	kA	LP
Bludička	bludička	k1gFnSc1	bludička
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nesoucí	nesoucí	k2eAgMnSc1d1	nesoucí
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
hitů	hit	k1gInPc2	hit
Bludička	bludička	k1gFnSc1	bludička
Julie	Julie	k1gFnSc1	Julie
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
účinkovala	účinkovat	k5eAaImAgFnS	účinkovat
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
divadle	divadlo	k1gNnSc6	divadlo
Apollo	Apollo	k1gMnSc1	Apollo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
70	[number]	k4	70
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
přesídlení	přesídlení	k1gNnSc6	přesídlení
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
začala	začít	k5eAaPmAgFnS	začít
natáčet	natáčet	k5eAaImF	natáčet
s	s	k7c7	s
Tanečním	taneční	k2eAgInSc7d1	taneční
orchestrem	orchestr	k1gInSc7	orchestr
Československého	československý	k2eAgInSc2d1	československý
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
např.	např.	kA	např.
písně	píseň	k1gFnPc4	píseň
Tisíc	tisíc	k4xCgInSc1	tisíc
nových	nový	k2eAgNnPc2d1	nové
jmen	jméno	k1gNnPc2	jméno
<g/>
,	,	kIx,	,
Já	já	k3xPp1nSc1	já
jsem	být	k5eAaImIp1nS	být
tvá	tvůj	k3xOp2gFnSc1	tvůj
neznámá	neznámá	k1gFnSc1	neznámá
<g/>
,	,	kIx,	,
Náš	náš	k3xOp1gInSc1	náš
dům	dům	k1gInSc1	dům
zní	znět	k5eAaImIp3nS	znět
smíchem	smích	k1gInSc7	smích
nebo	nebo	k8xC	nebo
třeba	třeba	k6eAd1	třeba
baladická	baladický	k2eAgFnSc1d1	baladická
píseň	píseň	k1gFnSc1	píseň
s	s	k7c7	s
citovým	citový	k2eAgInSc7d1	citový
projevem	projev	k1gInSc7	projev
On	on	k3xPp3gMnSc1	on
je	být	k5eAaImIp3nS	být
někdo	někdo	k3yInSc1	někdo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1972	[number]	k4	1972
až	až	k9	až
1974	[number]	k4	1974
hostovala	hostovat	k5eAaImAgFnS	hostovat
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
Semafor	Semafor	k1gInSc1	Semafor
v	v	k7c6	v
Suchého	Suchého	k2eAgNnSc6d1	Suchého
parodickém	parodický	k2eAgNnSc6d1	parodické
představení	představení	k1gNnSc6	představení
Kytice	kytice	k1gFnSc1	kytice
(	(	kIx(	(
<g/>
zpracovaném	zpracovaný	k2eAgNnSc6d1	zpracované
velmi	velmi	k6eAd1	velmi
volně	volně	k6eAd1	volně
na	na	k7c4	na
námět	námět	k1gInSc4	námět
Karla	Karel	k1gMnSc2	Karel
Jaromíra	Jaromír	k1gMnSc2	Jaromír
Erbena	Erben	k1gMnSc2	Erben
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
v	v	k7c6	v
roli	role	k1gFnSc6	role
Bludičky	bludička	k1gFnSc2	bludička
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
obliba	obliba	k1gFnSc1	obliba
u	u	k7c2	u
posluchačů	posluchač	k1gMnPc2	posluchač
narůstala	narůstat	k5eAaImAgFnS	narůstat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
odrazilo	odrazit	k5eAaPmAgNnS	odrazit
i	i	k9	i
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
umístění	umístění	k1gNnSc6	umístění
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
posluchačské	posluchačský	k2eAgFnSc2d1	posluchačská
oblíbenosti	oblíbenost	k1gFnSc2	oblíbenost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
r.	r.	kA	r.
1974	[number]	k4	1974
a	a	k8xC	a
1975	[number]	k4	1975
získala	získat	k5eAaPmAgFnS	získat
bronzového	bronzový	k2eAgMnSc4d1	bronzový
slavíka	slavík	k1gMnSc4	slavík
<g/>
,	,	kIx,	,
následoval	následovat	k5eAaImAgInS	následovat
stříbrný	stříbrný	k2eAgMnSc1d1	stříbrný
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
získala	získat	k5eAaPmAgFnS	získat
devětkrát	devětkrát	k6eAd1	devětkrát
po	po	k7c6	po
sobě	se	k3xPyFc3	se
Zlatého	zlatý	k2eAgMnSc4d1	zlatý
slavíka	slavík	k1gMnSc4	slavík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
podepsala	podepsat	k5eAaPmAgFnS	podepsat
Antichartu	anticharta	k1gFnSc4	anticharta
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
objevovat	objevovat	k5eAaImF	objevovat
s	s	k7c7	s
vlastní	vlastní	k2eAgFnSc7d1	vlastní
doprovodnou	doprovodný	k2eAgFnSc7d1	doprovodná
kapelou	kapela	k1gFnSc7	kapela
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vedl	vést	k5eAaImAgMnS	vést
Karel	Karel	k1gMnSc1	Karel
Vágner	Vágner	k1gMnSc1	Vágner
<g/>
.	.	kIx.	.
</s>
<s>
Vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
s	s	k7c7	s
kolegou	kolega	k1gMnSc7	kolega
Michalem	Michal	k1gMnSc7	Michal
Prokopem	Prokop	k1gMnSc7	Prokop
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
Petr	Petr	k1gMnSc1	Petr
Rezek	Rezek	k1gMnSc1	Rezek
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Petrem	Petr	k1gMnSc7	Petr
Rezkem	Rezek	k1gMnSc7	Rezek
natočila	natočit	k5eAaBmAgFnS	natočit
několik	několik	k4yIc4	několik
úspěšných	úspěšný	k2eAgInPc2d1	úspěšný
duetů	duet	k1gInPc2	duet
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgMnPc4d1	jiný
<g/>
:	:	kIx,	:
Asi	asi	k9	asi
<g/>
,	,	kIx,	,
asi	asi	k9	asi
nebo	nebo	k8xC	nebo
Duhová	duhový	k2eAgFnSc1d1	Duhová
víla	víla	k1gFnSc1	víla
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
spolupráce	spolupráce	k1gFnSc1	spolupráce
trvala	trvat	k5eAaImAgFnS	trvat
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
koncertovala	koncertovat	k5eAaImAgFnS	koncertovat
po	po	k7c6	po
ČSSR	ČSSR	kA	ČSSR
společně	společně	k6eAd1	společně
s	s	k7c7	s
italským	italský	k2eAgMnSc7d1	italský
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Drupim	Drupima	k1gFnPc2	Drupima
<g/>
.	.	kIx.	.
</s>
<s>
Natáčí	natáčet	k5eAaImIp3nS	natáčet
své	svůj	k3xOyFgInPc4	svůj
pořady	pořad	k1gInPc4	pořad
s	s	k7c7	s
vlastním	vlastní	k2eAgInSc7d1	vlastní
scénářem	scénář	k1gInSc7	scénář
<g/>
:	:	kIx,	:
Zaváté	zavátý	k2eAgFnPc4d1	zavátá
studánky	studánka	k1gFnPc4	studánka
<g/>
,	,	kIx,	,
Prázdniny	prázdniny	k1gFnPc1	prázdniny
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
80	[number]	k4	80
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
pravidelně	pravidelně	k6eAd1	pravidelně
natáčela	natáčet	k5eAaImAgFnS	natáčet
svůj	svůj	k3xOyFgInSc4	svůj
hudební	hudební	k2eAgInSc4d1	hudební
TV	TV	kA	TV
pořad	pořad	k1gInSc1	pořad
s	s	k7c7	s
názvem	název	k1gInSc7	název
Dluhy	dluh	k1gInPc4	dluh
Hany	Hana	k1gFnSc2	Hana
Zagorové	Zagorová	k1gFnSc3	Zagorová
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
koncertovala	koncertovat	k5eAaImAgFnS	koncertovat
<g/>
,	,	kIx,	,
během	během	k7c2	během
roku	rok	k1gInSc2	rok
odehrála	odehrát	k5eAaPmAgFnS	odehrát
až	až	k9	až
360	[number]	k4	360
koncertů	koncert	k1gInPc2	koncert
doma	doma	k6eAd1	doma
i	i	k9	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
se	se	k3xPyFc4	se
vracela	vracet	k5eAaImAgFnS	vracet
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgFnSc7d1	populární
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
či	či	k8xC	či
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Nahrávala	nahrávat	k5eAaImAgFnS	nahrávat
desky	deska	k1gFnPc4	deska
pro	pro	k7c4	pro
domácí	domácí	k2eAgMnPc4d1	domácí
příznivce	příznivec	k1gMnPc4	příznivec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
pro	pro	k7c4	pro
zahraniční	zahraniční	k2eAgFnPc4d1	zahraniční
firmy	firma	k1gFnPc4	firma
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Natáčela	natáčet	k5eAaImAgFnS	natáčet
pořady	pořad	k1gInPc4	pořad
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Egypt	Egypt	k1gInSc1	Egypt
<g/>
,	,	kIx,	,
Malta	Malta	k1gFnSc1	Malta
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
)	)	kIx)	)
a	a	k8xC	a
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
kolegů	kolega	k1gMnPc2	kolega
(	(	kIx(	(
<g/>
polská	polský	k2eAgFnSc1d1	polská
skupina	skupina	k1gFnSc1	skupina
Vox	Vox	k1gFnSc1	Vox
<g/>
,	,	kIx,	,
polská	polský	k2eAgFnSc1d1	polská
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Irena	Irena	k1gFnSc1	Irena
Jarocka	Jarocka	k1gFnSc1	Jarocka
<g/>
,	,	kIx,	,
rakouská	rakouský	k2eAgFnSc1d1	rakouská
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Goldie	Goldie	k1gFnSc1	Goldie
Ens	Ens	k1gFnSc1	Ens
<g/>
,	,	kIx,	,
Belgičan	Belgičan	k1gMnSc1	Belgičan
Julius	Julius	k1gMnSc1	Julius
Zegers	Zegersa	k1gFnPc2	Zegersa
<g/>
,	,	kIx,	,
Buena	Bueen	k2eAgFnSc1d1	Buena
Ventura	Ventura	kA	Ventura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
rolí	role	k1gFnPc2	role
ve	v	k7c6	v
filmovém	filmový	k2eAgInSc6d1	filmový
muzikálu	muzikál	k1gInSc6	muzikál
Trhák	trhák	k1gInSc1	trhák
režiséra	režisér	k1gMnSc2	režisér
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Podskalského	podskalský	k2eAgMnSc2d1	podskalský
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
filmu	film	k1gInSc2	film
pochází	pocházet	k5eAaImIp3nS	pocházet
duet	duet	k1gInSc1	duet
s	s	k7c7	s
Jurajem	Juraj	k1gInSc7	Juraj
Kukurou	Kukura	k1gFnSc7	Kukura
Lásko	láska	k1gFnSc5	láska
<g/>
,	,	kIx,	,
amore	amor	k1gMnSc5	amor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
natočila	natočit	k5eAaBmAgFnS	natočit
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
německou	německý	k2eAgFnSc7d1	německá
televizí	televize	k1gFnSc7	televize
muzikálovou	muzikálový	k2eAgFnSc4d1	muzikálová
pohádku	pohádka	k1gFnSc4	pohádka
Peter	Peter	k1gMnSc1	Peter
Pan	Pan	k1gMnSc1	Pan
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrála	hrát	k5eAaImAgFnS	hrát
roli	role	k1gFnSc4	role
matky	matka	k1gFnSc2	matka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
1980	[number]	k4	1980
až	až	k8xS	až
1986	[number]	k4	1986
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
s	s	k7c7	s
tanečním	taneční	k2eAgNnSc7d1	taneční
a	a	k8xC	a
pěveckým	pěvecký	k2eAgNnSc7d1	pěvecké
duem	duo	k1gNnSc7	duo
Petra	Petr	k1gMnSc2	Petr
Kotvalda	Kotvald	k1gMnSc2	Kotvald
a	a	k8xC	a
Stanislava	Stanislav	k1gMnSc2	Stanislav
Hložka	Hložek	k1gMnSc2	Hložek
(	(	kIx(	(
<g/>
se	s	k7c7	s
Stanislavem	Stanislav	k1gMnSc7	Stanislav
Hložkem	Hložek	k1gMnSc7	Hložek
až	až	k8xS	až
do	do	k7c2	do
r.	r.	kA	r.
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
stáje	stáj	k1gFnSc2	stáj
Karla	Karel	k1gMnSc2	Karel
Vágnera	Vágner	k1gMnSc2	Vágner
r.	r.	kA	r.
1981	[number]	k4	1981
odchází	odcházet	k5eAaImIp3nS	odcházet
Petr	Petr	k1gMnSc1	Petr
Rezek	Rezek	k1gMnSc1	Rezek
<g/>
.	.	kIx.	.
</s>
<s>
Rokem	rok	k1gInSc7	rok
1986	[number]	k4	1986
počíná	počínat	k5eAaImIp3nS	počínat
spolupráce	spolupráce	k1gFnSc1	spolupráce
orchestru	orchestr	k1gInSc2	orchestr
K.	K.	kA	K.
Vágnera	Vágner	k1gMnSc2	Vágner
a	a	k8xC	a
Hany	Hana	k1gFnSc2	Hana
Zagorové	Zagorová	k1gFnSc2	Zagorová
s	s	k7c7	s
dvojicí	dvojice	k1gFnSc7	dvojice
Linda	Linda	k1gFnSc1	Linda
Finková	Finková	k1gFnSc1	Finková
a	a	k8xC	a
Pavel	Pavel	k1gMnSc1	Pavel
Noha	noha	k1gFnSc1	noha
<g/>
.	.	kIx.	.
</s>
<s>
Pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
Vágnerem	Vágner	k1gMnSc7	Vágner
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
byl	být	k5eAaImAgMnS	být
nejen	nejen	k6eAd1	nejen
kapelníkem	kapelník	k1gMnSc7	kapelník
a	a	k8xC	a
manažerem	manažer	k1gMnSc7	manažer
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
skladatelem	skladatel	k1gMnSc7	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
dílny	dílna	k1gFnSc2	dílna
pochází	pocházet	k5eAaImIp3nS	pocházet
např.	např.	kA	např.
Počítadlo	počítadlo	k1gNnSc4	počítadlo
lásky	láska	k1gFnSc2	láska
nebo	nebo	k8xC	nebo
Usnul	usnout	k5eAaPmAgMnS	usnout
nám	my	k3xPp1nPc3	my
<g/>
,	,	kIx,	,
spí	spát	k5eAaImIp3nP	spát
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
doprovodu	doprovod	k1gInSc2	doprovod
orchestru	orchestr	k1gInSc2	orchestr
Karla	Karel	k1gMnSc2	Karel
Vágnera	Vágner	k1gMnSc2	Vágner
se	se	k3xPyFc4	se
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
těší	těšit	k5eAaImIp3nS	těšit
enormní	enormní	k2eAgFnSc3d1	enormní
slávě	sláva	k1gFnSc3	sláva
<g/>
,	,	kIx,	,
počátkem	počátkem	k7c2	počátkem
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
čítá	čítat	k5eAaImIp3nS	čítat
její	její	k3xOp3gInSc4	její
počet	počet	k1gInSc4	počet
hlasů	hlas	k1gInPc2	hlas
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Zlatý	zlatý	k2eAgInSc1d1	zlatý
slavík	slavík	k1gInSc1	slavík
takový	takový	k3xDgInSc4	takový
počet	počet	k1gInSc4	počet
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
počet	počet	k1gInSc1	počet
hlasů	hlas	k1gInPc2	hlas
všech	všecek	k3xTgFnPc2	všecek
ostatních	ostatní	k2eAgFnPc2d1	ostatní
zpěvaček	zpěvačka	k1gFnPc2	zpěvačka
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
</s>
<s>
Zagorová	Zagorová	k1gFnSc1	Zagorová
natáčí	natáčet	k5eAaImIp3nS	natáčet
LP	LP	kA	LP
desky	deska	k1gFnPc4	deska
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
kvalitním	kvalitní	k2eAgInSc7d1	kvalitní
repertoárem	repertoár	k1gInSc7	repertoár
<g/>
,	,	kIx,	,
jen	jen	k9	jen
namátkou	namátkou	k6eAd1	namátkou
LP	LP	kA	LP
Světlo	světlo	k1gNnSc1	světlo
a	a	k8xC	a
Stín	stín	k1gInSc1	stín
<g/>
,	,	kIx,	,
Co	co	k8xS	co
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
<g/>
,	,	kIx,	,
Sítě	síť	k1gFnPc4	síť
kroků	krok	k1gInPc2	krok
tvých	tvůj	k3xOp2gInPc2	tvůj
či	či	k8xC	či
Živá	živý	k2eAgFnSc1d1	živá
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Skladby	skladba	k1gFnPc1	skladba
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
psali	psát	k5eAaImAgMnP	psát
známí	známý	k1gMnPc1	známý
osvědčení	osvědčený	k2eAgMnPc1d1	osvědčený
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
začínající	začínající	k2eAgMnPc1d1	začínající
skladatelé	skladatel	k1gMnPc1	skladatel
–	–	k?	–
Pavel	Pavel	k1gMnSc1	Pavel
Žák	Žák	k1gMnSc1	Žák
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Hapka	Hapka	k1gMnSc1	Hapka
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
Parma	Parma	k1gFnSc1	Parma
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Zmožek	Zmožek	k1gInSc1	Zmožek
<g/>
,	,	kIx,	,
Vítězslav	Vítězslav	k1gMnSc1	Vítězslav
Hádl	Hádl	k1gMnSc1	Hádl
či	či	k8xC	či
Lída	Lída	k1gFnSc1	Lída
Nopová	nopový	k2eAgFnSc1d1	Nopová
i	i	k9	i
textaři	textař	k1gMnSc3	textař
–	–	k?	–
Pavel	Pavel	k1gMnSc1	Pavel
Žák	Žák	k1gMnSc1	Žák
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Krůta	krůta	k1gFnSc1	krůta
<g/>
,	,	kIx,	,
Jiřina	Jiřina	k1gFnSc1	Jiřina
Fikejzová	Fikejzová	k1gFnSc1	Fikejzová
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Borovec	Borovec	k1gMnSc1	Borovec
či	či	k8xC	či
Vladimír	Vladimír	k1gMnSc1	Vladimír
Čort	Čort	k1gMnSc1	Čort
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
interpretka	interpretka	k1gFnSc1	interpretka
rovněž	rovněž	k9	rovněž
přispívá	přispívat	k5eAaImIp3nS	přispívat
svými	svůj	k3xOyFgMnPc7	svůj
texty	text	k1gInPc7	text
–	–	k?	–
například	například	k6eAd1	například
Málokdo	málokdo	k3yInSc1	málokdo
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
To	ten	k3xDgNnSc1	ten
by	by	kYmCp3nS	by
nebylo	být	k5eNaImAgNnS	být
fér	fér	k2eAgNnSc1d1	fér
<g/>
,	,	kIx,	,
Sláva	Sláva	k1gMnSc1	Sláva
je	být	k5eAaImIp3nS	být
bál	bát	k5eAaImAgMnS	bát
nebo	nebo	k8xC	nebo
Černý	Černý	k1gMnSc1	Černý
páv	páv	k1gMnSc1	páv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
se	se	k3xPyFc4	se
po	po	k7c6	po
sérií	série	k1gFnSc7	série
zlatých	zlatý	k2eAgInPc2d1	zlatý
slavíků	slavík	k1gInPc2	slavík
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c6	na
bronzové	bronzový	k2eAgFnSc6d1	bronzová
příčce	příčka	k1gFnSc6	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
Zagorová	Zagorová	k1gFnSc1	Zagorová
to	ten	k3xDgNnSc4	ten
nebere	brát	k5eNaImIp3nS	brát
jako	jako	k9	jako
prohru	prohra	k1gFnSc4	prohra
<g/>
,	,	kIx,	,
v	v	k7c6	v
budoucích	budoucí	k2eAgInPc6d1	budoucí
letech	let	k1gInPc6	let
se	se	k3xPyFc4	se
těší	těšit	k5eAaImIp3nS	těšit
stálé	stálý	k2eAgFnSc3d1	stálá
oblibě	obliba	k1gFnSc3	obliba
publika	publikum	k1gNnSc2	publikum
a	a	k8xC	a
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
se	se	k3xPyFc4	se
umisťuje	umisťovat	k5eAaImIp3nS	umisťovat
pravidelně	pravidelně	k6eAd1	pravidelně
v	v	k7c6	v
první	první	k4xOgFnSc6	první
desítce	desítka	k1gFnSc6	desítka
<g/>
:	:	kIx,	:
1989	[number]	k4	1989
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
–	–	k?	–
7	[number]	k4	7
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
–	–	k?	–
10	[number]	k4	10
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
–	–	k?	–
10	[number]	k4	10
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Natočila	natočit	k5eAaBmAgFnS	natočit
šansonově	šansonově	k6eAd1	šansonově
laděnou	laděný	k2eAgFnSc4d1	laděná
desku	deska	k1gFnSc4	deska
s	s	k7c7	s
texty	text	k1gInPc7	text
Michala	Michal	k1gMnSc2	Michal
Horáčka	Horáček	k1gMnSc2	Horáček
s	s	k7c7	s
názvem	název	k1gInSc7	název
Živá	živý	k2eAgFnSc1d1	živá
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podobném	podobný	k2eAgMnSc6d1	podobný
duchu	duch	k1gMnSc6	duch
se	se	k3xPyFc4	se
neslo	nést	k5eAaImAgNnS	nést
i	i	k9	i
následující	následující	k2eAgNnSc1d1	následující
album	album	k1gNnSc1	album
Dnes	dnes	k6eAd1	dnes
nejsem	být	k5eNaImIp1nS	být
doma	doma	k6eAd1	doma
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
vydáno	vydat	k5eAaPmNgNnS	vydat
až	až	k6eAd1	až
r.	r.	kA	r.
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Směrem	směr	k1gInSc7	směr
šansonu	šanson	k1gInSc2	šanson
se	se	k3xPyFc4	se
ubírá	ubírat	k5eAaImIp3nS	ubírat
i	i	k9	i
její	její	k3xOp3gFnSc1	její
další	další	k2eAgFnSc1d1	další
tvorba	tvorba	k1gFnSc1	tvorba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1989	[number]	k4	1989
podepsala	podepsat	k5eAaPmAgFnS	podepsat
petici	petice	k1gFnSc4	petice
Několik	několik	k4yIc1	několik
vět	věta	k1gFnPc2	věta
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
hnutí	hnutí	k1gNnPc2	hnutí
Charta	charta	k1gFnSc1	charta
77	[number]	k4	77
požadovalo	požadovat	k5eAaImAgNnS	požadovat
propuštění	propuštění	k1gNnSc1	propuštění
politických	politický	k2eAgMnPc2d1	politický
vězňů	vězeň	k1gMnPc2	vězeň
a	a	k8xC	a
svobodu	svoboda	k1gFnSc4	svoboda
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
jí	jíst	k5eAaImIp3nS	jíst
zastavena	zastaven	k2eAgFnSc1d1	zastavena
činnost	činnost	k1gFnSc1	činnost
a	a	k8xC	a
až	až	k9	až
do	do	k7c2	do
pádu	pád	k1gInSc2	pád
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
nesměla	smět	k5eNaImAgFnS	smět
veřejně	veřejně	k6eAd1	veřejně
vystupovat	vystupovat	k5eAaImF	vystupovat
<g/>
.	.	kIx.	.
</s>
<s>
Odolala	odolat	k5eAaPmAgFnS	odolat
tlaku	tlak	k1gInSc2	tlak
svůj	svůj	k3xOyFgInSc4	svůj
podpis	podpis	k1gInSc4	podpis
odvolat	odvolat	k5eAaPmF	odvolat
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
ho	on	k3xPp3gNnSc4	on
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nebylo	být	k5eNaImAgNnS	být
tam	tam	k6eAd1	tam
nic	nic	k3yNnSc1	nic
<g/>
,	,	kIx,	,
s	s	k7c7	s
čím	co	k3yInSc7	co
by	by	kYmCp3nS	by
slušný	slušný	k2eAgMnSc1d1	slušný
člověk	člověk	k1gMnSc1	člověk
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Znovu	znovu	k6eAd1	znovu
zpívala	zpívat	k5eAaImAgFnS	zpívat
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1989	[number]	k4	1989
společně	společně	k6eAd1	společně
s	s	k7c7	s
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Hutkou	Hutka	k1gMnSc7	Hutka
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
hymnu	hymna	k1gFnSc4	hymna
na	na	k7c6	na
Václavském	václavský	k2eAgNnSc6d1	Václavské
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
na	na	k7c6	na
Letné	Letná	k1gFnSc6	Letná
s	s	k7c7	s
Martou	Marta	k1gFnSc7	Marta
Kubišovou	Kubišová	k1gFnSc7	Kubišová
a	a	k8xC	a
Jiřím	Jiří	k1gMnSc7	Jiří
Suchým	Suchý	k1gMnSc7	Suchý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
a	a	k8xC	a
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
nazpívala	nazpívat	k5eAaPmAgFnS	nazpívat
znělky	znělka	k1gFnPc4	znělka
pro	pro	k7c4	pro
dětské	dětský	k2eAgInPc4d1	dětský
seriály	seriál	k1gInPc4	seriál
(	(	kIx(	(
<g/>
Šmoulové	šmoula	k1gMnPc1	šmoula
a	a	k8xC	a
Tom	Tom	k1gMnSc1	Tom
a	a	k8xC	a
Jerry	Jerra	k1gFnPc1	Jerra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
90	[number]	k4	90
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
vydala	vydat	k5eAaPmAgFnS	vydat
šansonové	šansonový	k2eAgNnSc4d1	šansonové
album	album	k1gNnSc4	album
Rozhovor	rozhovor	k1gInSc1	rozhovor
v	v	k7c6	v
tichu	ticho	k1gNnSc6	ticho
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Vágner	Vágner	k1gMnSc1	Vágner
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jejím	její	k3xOp3gMnSc7	její
producentem	producent	k1gMnSc7	producent
a	a	k8xC	a
jeho	jeho	k3xOp3gNnPc2	jeho
vydavatelství	vydavatelství	k1gNnPc2	vydavatelství
Multisonic	Multisonice	k1gFnPc2	Multisonice
vydává	vydávat	k5eAaPmIp3nS	vydávat
nadále	nadále	k6eAd1	nadále
její	její	k3xOp3gFnSc1	její
řadová	řadový	k2eAgFnSc1d1	řadová
alba	alba	k1gFnSc1	alba
místo	místo	k7c2	místo
firmy	firma	k1gFnSc2	firma
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Počátkem	počátkem	k7c2	počátkem
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
s	s	k7c7	s
programem	program	k1gInSc7	program
pro	pro	k7c4	pro
krajany	krajan	k1gMnPc4	krajan
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Doma	doma	k6eAd1	doma
připravila	připravit	k5eAaPmAgFnS	připravit
zcela	zcela	k6eAd1	zcela
nový	nový	k2eAgInSc4d1	nový
koncertní	koncertní	k2eAgInSc4d1	koncertní
program	program	k1gInSc4	program
s	s	k7c7	s
názvem	název	k1gInSc7	název
Noční	noční	k2eAgInPc4d1	noční
telefony	telefon	k1gInPc4	telefon
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
tvořily	tvořit	k5eAaImAgFnP	tvořit
převážně	převážně	k6eAd1	převážně
šansonově	šansonově	k6eAd1	šansonově
laděné	laděný	k2eAgFnPc4d1	laděná
skladby	skladba	k1gFnPc4	skladba
z	z	k7c2	z
posledních	poslední	k2eAgNnPc2d1	poslední
alb	album	k1gNnPc2	album
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
programem	program	k1gInSc7	program
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
až	až	k9	až
do	do	k7c2	do
r.	r.	kA	r.
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
omezuje	omezovat	k5eAaImIp3nS	omezovat
svoji	svůj	k3xOyFgFnSc4	svůj
profesní	profesní	k2eAgFnSc4d1	profesní
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
vlastních	vlastní	k2eAgNnPc2d1	vlastní
slov	slovo	k1gNnPc2	slovo
"	"	kIx"	"
<g/>
měla	mít	k5eAaImAgFnS	mít
pocit	pocit	k1gInSc4	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
co	co	k3yRnSc4	co
měla	mít	k5eAaImAgFnS	mít
odzpívat	odzpívat	k5eAaPmF	odzpívat
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
odzpívala	odzpívat	k5eAaPmAgFnS	odzpívat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Věnuje	věnovat	k5eAaPmIp3nS	věnovat
se	se	k3xPyFc4	se
svému	svůj	k3xOyFgMnSc3	svůj
manželovi	manžel	k1gMnSc3	manžel
<g/>
,	,	kIx,	,
Štefanu	Štefan	k1gMnSc3	Štefan
Margitovi	Margita	k1gMnSc3	Margita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
píše	psát	k5eAaImIp3nS	psát
scénáře	scénář	k1gInPc4	scénář
některých	některý	k3yIgMnPc2	některý
televizních	televizní	k2eAgMnPc2d1	televizní
pořadů	pořad	k1gInPc2	pořad
a	a	k8xC	a
vydává	vydávat	k5eAaImIp3nS	vydávat
kompilace	kompilace	k1gFnSc1	kompilace
svých	svůj	k3xOyFgFnPc2	svůj
starších	starý	k2eAgFnPc2d2	starší
písní	píseň	k1gFnPc2	píseň
a	a	k8xC	a
LP	LP	kA	LP
desek	deska	k1gFnPc2	deska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1993	[number]	k4	1993
až	až	k8xS	až
1998	[number]	k4	1998
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
na	na	k7c6	na
TV	TV	kA	TV
obrazovkách	obrazovka	k1gFnPc6	obrazovka
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
pořadem	pořad	k1gInSc7	pořad
Když	když	k8xS	když
nemůžu	nemůžu	k?	nemůžu
spát	spát	k5eAaImF	spát
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
pořad	pořad	k1gInSc4	pořad
natáčí	natáčet	k5eAaImIp3nP	natáčet
nové	nový	k2eAgFnPc1d1	nová
písničky	písnička	k1gFnPc1	písnička
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
duety	duet	k1gInPc1	duet
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
hosty	host	k1gMnPc7	host
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
těchto	tento	k3xDgFnPc2	tento
písní	píseň	k1gFnPc2	píseň
je	být	k5eAaImIp3nS	být
Michal	Michal	k1gMnSc1	Michal
David	David	k1gMnSc1	David
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Znovu	znovu	k6eAd1	znovu
začíná	začínat	k5eAaImIp3nS	začínat
koncertovat	koncertovat	k5eAaImF	koncertovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
mají	mít	k5eAaImIp3nP	mít
koncerty	koncert	k1gInPc1	koncert
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
plán	plán	k1gInSc4	plán
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
ji	on	k3xPp3gFnSc4	on
doprovázela	doprovázet	k5eAaImAgFnS	doprovázet
skupina	skupina	k1gFnSc1	skupina
Miloše	Miloš	k1gMnSc2	Miloš
Nopa	nopa	k1gFnSc1	nopa
<g/>
.	.	kIx.	.
</s>
<s>
Obnovuje	obnovovat	k5eAaImIp3nS	obnovovat
příležitostnou	příležitostný	k2eAgFnSc4d1	příležitostná
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
Petrem	Petr	k1gMnSc7	Petr
Kotvaldem	Kotvald	k1gMnSc7	Kotvald
a	a	k8xC	a
Stanislavem	Stanislav	k1gMnSc7	Stanislav
Hložkem	Hložek	k1gMnSc7	Hložek
<g/>
,	,	kIx,	,
pravidelně	pravidelně	k6eAd1	pravidelně
koncertuje	koncertovat	k5eAaImIp3nS	koncertovat
s	s	k7c7	s
Petrem	Petr	k1gMnSc7	Petr
Rezkem	Rezek	k1gMnSc7	Rezek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Natáčí	natáčet	k5eAaImIp3nP	natáčet
také	také	k9	také
dvě	dva	k4xCgNnPc1	dva
alba	album	k1gNnPc1	album
s	s	k7c7	s
manželem	manžel	k1gMnSc7	manžel
Štefanem	Štefan	k1gMnSc7	Štefan
Margitou	Margita	k1gMnSc7	Margita
s	s	k7c7	s
názvy	název	k1gInPc7	název
Ave	ave	k1gNnSc1	ave
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ave	ave	k1gNnSc1	ave
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
natáčí	natáčet	k5eAaImIp3nS	natáčet
po	po	k7c6	po
čtyřech	čtyři	k4xCgNnPc6	čtyři
letech	léto	k1gNnPc6	léto
novinkové	novinkový	k2eAgNnSc1d1	novinkové
album	album	k1gNnSc1	album
Já	já	k3xPp1nSc1	já
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
první	první	k4xOgFnPc1	první
písně	píseň	k1gFnPc1	píseň
od	od	k7c2	od
jejího	její	k3xOp3gMnSc2	její
nového	nový	k2eAgMnSc2d1	nový
spolupracovníka	spolupracovník	k1gMnSc2	spolupracovník
<g/>
,	,	kIx,	,
autora	autor	k1gMnSc2	autor
<g/>
,	,	kIx,	,
Jiřího	Jiří	k1gMnSc2	Jiří
Březíka	Březík	k1gMnSc2	Březík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
===	===	k?	===
</s>
</p>
<p>
<s>
Koncertně	koncertně	k6eAd1	koncertně
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Miloše	Miloš	k1gMnSc2	Miloš
Nopa	nopa	k1gFnSc1	nopa
<g/>
,	,	kIx,	,
od	od	k7c2	od
r.	r.	kA	r.
2010	[number]	k4	2010
trvá	trvat	k5eAaImIp3nS	trvat
spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
Jiřím	Jiří	k1gMnSc7	Jiří
Dvořákem	Dvořák	k1gMnSc7	Dvořák
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
skupinou	skupina	k1gFnSc7	skupina
Boom	boom	k1gInSc4	boom
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
Band	band	k1gInSc1	band
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
Petrem	Petr	k1gMnSc7	Petr
Rezkem	Rezek	k1gMnSc7	Rezek
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelně	pravidelně	k6eAd1	pravidelně
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
před	před	k7c7	před
Vánoci	Vánoce	k1gFnPc7	Vánoce
<g/>
,	,	kIx,	,
pořádá	pořádat	k5eAaImIp3nS	pořádat
koncert	koncert	k1gInSc1	koncert
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
Lucerně	lucerna	k1gFnSc6	lucerna
se	s	k7c7	s
speciálním	speciální	k2eAgInSc7d1	speciální
programem	program	k1gInSc7	program
a	a	k8xC	a
hosty	host	k1gMnPc7	host
<g/>
.	.	kIx.	.
</s>
<s>
Koncertuje	koncertovat	k5eAaImIp3nS	koncertovat
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
vrací	vracet	k5eAaImIp3nS	vracet
i	i	k9	i
na	na	k7c4	na
Slovensko	Slovensko	k1gNnSc4	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Natáčí	natáčet	k5eAaImIp3nP	natáčet
další	další	k2eAgNnPc4d1	další
novinková	novinkový	k2eAgNnPc4d1	novinkové
alba	album	k1gNnPc4	album
a	a	k8xC	a
singly	singl	k1gInPc4	singl
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
vydány	vydat	k5eAaPmNgFnP	vydat
také	také	k9	také
kompilace	kompilace	k1gFnPc1	kompilace
starších	starý	k2eAgFnPc2d2	starší
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
za	za	k7c4	za
prodej	prodej	k1gInSc4	prodej
svých	svůj	k3xOyFgNnPc2	svůj
alb	album	k1gNnPc2	album
a	a	k8xC	a
kompilací	kompilace	k1gFnPc2	kompilace
získává	získávat	k5eAaImIp3nS	získávat
zlaté	zlatý	k2eAgFnPc4d1	zlatá
a	a	k8xC	a
platinové	platinový	k2eAgFnPc4d1	platinová
desky	deska	k1gFnPc4	deska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
vydává	vydávat	k5eAaPmIp3nS	vydávat
album	album	k1gNnSc4	album
Hanka	Hanka	k1gFnSc1	Hanka
<g/>
,	,	kIx,	,
získává	získávat	k5eAaImIp3nS	získávat
zlatou	zlatý	k2eAgFnSc4d1	zlatá
desku	deska	k1gFnSc4	deska
<g/>
,	,	kIx,	,
dostává	dostávat	k5eAaImIp3nS	dostávat
se	se	k3xPyFc4	se
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
hitparád	hitparáda	k1gFnPc2	hitparáda
a	a	k8xC	a
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Český	český	k2eAgInSc1d1	český
Slavík	slavík	k1gInSc1	slavík
2003	[number]	k4	2003
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c6	na
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2000	[number]	k4	2000
až	až	k9	až
2004	[number]	k4	2004
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
vysílala	vysílat	k5eAaImAgFnS	vysílat
zábavný	zábavný	k2eAgInSc4d1	zábavný
pořad	pořad	k1gInSc4	pořad
Hogo-Fogo	Hogo-Fogo	k1gMnSc1	Hogo-Fogo
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
společně	společně	k6eAd1	společně
s	s	k7c7	s
manželem	manžel	k1gMnSc7	manžel
moderovala	moderovat	k5eAaBmAgFnS	moderovat
a	a	k8xC	a
podílela	podílet	k5eAaImAgFnS	podílet
se	se	k3xPyFc4	se
i	i	k9	i
na	na	k7c4	na
psaní	psaní	k1gNnSc4	psaní
scénáře	scénář	k1gInSc2	scénář
<g/>
.	.	kIx.	.
</s>
<s>
Pořad	pořad	k1gInSc1	pořad
se	se	k3xPyFc4	se
těšil	těšit	k5eAaImAgInS	těšit
veliké	veliký	k2eAgFnSc3d1	veliká
oblibě	obliba	k1gFnSc3	obliba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Píše	psát	k5eAaImIp3nS	psát
scénáře	scénář	k1gInPc4	scénář
a	a	k8xC	a
písňové	písňový	k2eAgInPc4d1	písňový
texty	text	k1gInPc4	text
<g/>
,	,	kIx,	,
věnuje	věnovat	k5eAaPmIp3nS	věnovat
se	se	k3xPyFc4	se
literární	literární	k2eAgFnSc3d1	literární
tvorbě	tvorba	k1gFnSc3	tvorba
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
vydává	vydávat	k5eAaPmIp3nS	vydávat
svou	svůj	k3xOyFgFnSc4	svůj
knihu	kniha	k1gFnSc4	kniha
básnických	básnický	k2eAgFnPc2d1	básnická
sbírek	sbírka	k1gFnPc2	sbírka
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Milostně	milostně	k6eAd1	milostně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
r.	r.	kA	r.
2007	[number]	k4	2007
poprvé	poprvé	k6eAd1	poprvé
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
postavu	postava	k1gFnSc4	postava
bláznivé	bláznivý	k2eAgFnSc2d1	bláznivá
prostitutky	prostitutka	k1gFnSc2	prostitutka
v	v	k7c6	v
muzikálu	muzikál	k1gInSc6	muzikál
Vašo	Vašo	k1gMnSc1	Vašo
Patejdla	Patejdla	k1gMnSc1	Patejdla
Jack	Jack	k1gMnSc1	Jack
Rozparovač	rozparovač	k1gMnSc1	rozparovač
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
Kalich	kalich	k1gInSc4	kalich
<g/>
,	,	kIx,	,
a	a	k8xC	a
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
Davidově	Davidův	k2eAgFnSc6d1	Davidova
Moně	Mona	k1gFnSc6	Mona
Lise	lis	k1gInSc6	lis
<g/>
,	,	kIx,	,
matku	matka	k1gFnSc4	matka
hlavní	hlavní	k2eAgFnSc2d1	hlavní
hrdinky	hrdinka	k1gFnSc2	hrdinka
<g/>
.	.	kIx.	.
</s>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
Kalich	kalich	k1gInSc1	kalich
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
domovskou	domovský	k2eAgFnSc7d1	domovská
scénou	scéna	k1gFnSc7	scéna
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
a	a	k8xC	a
Supraphon	supraphon	k1gInSc1	supraphon
vydali	vydat	k5eAaPmAgMnP	vydat
životopisnou	životopisný	k2eAgFnSc4d1	životopisná
knihu	kniha	k1gFnSc4	kniha
s	s	k7c7	s
DVD	DVD	kA	DVD
a	a	k8xC	a
CD	CD	kA	CD
nesoucí	nesoucí	k2eAgInSc1d1	nesoucí
název	název	k1gInSc1	název
Legenda	legenda	k1gFnSc1	legenda
–	–	k?	–
Hana	Hana	k1gFnSc1	Hana
Zagorová	Zagorová	k1gFnSc1	Zagorová
–	–	k?	–
Málokdo	málokdo	k3yInSc1	málokdo
ví	vědět	k5eAaImIp3nS	vědět
<g/>
.	.	kIx.	.
</s>
<s>
Uspořádala	uspořádat	k5eAaPmAgFnS	uspořádat
koncert	koncert	k1gInSc4	koncert
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
Lucerně	lucerna	k1gFnSc6	lucerna
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgInSc2	který
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
odvysílala	odvysílat	k5eAaPmAgFnS	odvysílat
záznam	záznam	k1gInSc4	záznam
(	(	kIx(	(
<g/>
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
na	na	k7c6	na
CD	CD	kA	CD
a	a	k8xC	a
DVD	DVD	kA	DVD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
na	na	k7c6	na
řadě	řada	k1gFnSc6	řada
charitativních	charitativní	k2eAgFnPc2d1	charitativní
akcí	akce	k1gFnPc2	akce
a	a	k8xC	a
také	také	k9	také
se	se	k3xPyFc4	se
účastnila	účastnit	k5eAaImAgFnS	účastnit
jejich	jejich	k3xOp3gFnSc1	jejich
organizace	organizace	k1gFnSc1	organizace
a	a	k8xC	a
produkce	produkce	k1gFnSc1	produkce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
získává	získávat	k5eAaImIp3nS	získávat
Diamantovou	diamantový	k2eAgFnSc4d1	Diamantová
desku	deska	k1gFnSc4	deska
Supraphonu	supraphon	k1gInSc2	supraphon
za	za	k7c4	za
10,5	[number]	k4	10,5
milionu	milion	k4xCgInSc2	milion
prodaných	prodaný	k2eAgInPc2d1	prodaný
nosičů	nosič	k1gInPc2	nosič
<g/>
,	,	kIx,	,
vydává	vydávat	k5eAaImIp3nS	vydávat
dlouho	dlouho	k6eAd1	dlouho
očekávané	očekávaný	k2eAgNnSc1d1	očekávané
novinkové	novinkový	k2eAgNnSc1d1	novinkové
album	album	k1gNnSc1	album
Vyznání	vyznání	k1gNnSc2	vyznání
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
své	svůj	k3xOyFgFnPc4	svůj
kvality	kvalita	k1gFnPc4	kvalita
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
prodává	prodávat	k5eAaImIp3nS	prodávat
a	a	k8xC	a
zpěvačce	zpěvačka	k1gFnSc3	zpěvačka
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
omladilo	omladit	k5eAaPmAgNnS	omladit
publikum	publikum	k1gNnSc1	publikum
za	za	k7c4	za
zmíněné	zmíněný	k2eAgNnSc4d1	zmíněné
album	album	k1gNnSc4	album
získala	získat	k5eAaPmAgFnS	získat
Zlatou	zlatý	k2eAgFnSc4d1	zlatá
desku	deska	k1gFnSc4	deska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2016	[number]	k4	2016
slavila	slavit	k5eAaImAgFnS	slavit
své	svůj	k3xOyFgFnPc4	svůj
sedmdesáté	sedmdesátý	k4xOgFnPc4	sedmdesátý
narozeniny	narozeniny	k1gFnPc4	narozeniny
koncertem	koncert	k1gInSc7	koncert
ve	v	k7c6	v
vyprodané	vyprodaný	k2eAgFnSc6d1	vyprodaná
Lucerně	lucerna	k1gFnSc6	lucerna
<g/>
,	,	kIx,	,
záznam	záznam	k1gInSc1	záznam
z	z	k7c2	z
koncertu	koncert	k1gInSc2	koncert
zaznamenává	zaznamenávat	k5eAaImIp3nS	zaznamenávat
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
<g/>
,	,	kIx,	,
na	na	k7c6	na
koncertě	koncert	k1gInSc6	koncert
křtí	křtít	k5eAaImIp3nS	křtít
novinkové	novinkový	k2eAgNnSc1d1	novinkové
album	album	k1gNnSc1	album
O	o	k7c6	o
Lásce	láska	k1gFnSc6	láska
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
první	první	k4xOgInSc4	první
týden	týden	k1gInSc4	týden
v	v	k7c6	v
prodeji	prodej	k1gInSc6	prodej
se	se	k3xPyFc4	se
album	album	k1gNnSc1	album
stává	stávat	k5eAaImIp3nS	stávat
nejprodávanějším	prodávaný	k2eAgNnSc6d3	nejprodávanější
pro	pro	k7c4	pro
daný	daný	k2eAgInSc4d1	daný
týden	týden	k1gInSc4	týden
<g/>
,	,	kIx,	,
získalo	získat	k5eAaPmAgNnS	získat
status	status	k1gInSc4	status
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
deska	deska	k1gFnSc1	deska
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roy	roy	k?	roy
později	pozdě	k6eAd2	pozdě
s	s	k7c7	s
albem	album	k1gNnSc7	album
Já	já	k3xPp1nSc1	já
nemám	mít	k5eNaImIp1nS	mít
strach	strach	k1gInSc4	strach
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Se	s	k7c7	s
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
hovorový	hovorový	k2eAgInSc1d1	hovorový
termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
..	..	k?	..
<g/>
nedělej	dělat	k5eNaImRp2nS	dělat
Zagorku	Zagorka	k1gFnSc4	Zagorka
<g/>
..	..	k?	..
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
I	i	k9	i
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejoblíbenější	oblíbený	k2eAgFnPc4d3	nejoblíbenější
a	a	k8xC	a
nejprodávanější	prodávaný	k2eAgFnPc4d3	nejprodávanější
zpěvačky	zpěvačka	k1gFnPc4	zpěvačka
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yQnSc6	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
například	například	k6eAd1	například
stálé	stálý	k2eAgNnSc4d1	stálé
umístění	umístění	k1gNnSc4	umístění
na	na	k7c6	na
předních	přední	k2eAgFnPc6d1	přední
příčkách	příčka	k1gFnPc6	příčka
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
<g/>
.	.	kIx.	.
</s>
<s>
Zagorová	Zagorová	k1gFnSc1	Zagorová
je	být	k5eAaImIp3nS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
své	svůj	k3xOyFgFnSc2	svůj
generace	generace	k1gFnSc2	generace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
po	po	k7c4	po
celou	celá	k1gFnSc4	celá
svou	svůj	k3xOyFgFnSc4	svůj
dobu	doba	k1gFnSc4	doba
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
,	,	kIx,	,
od	od	k7c2	od
r.	r.	kA	r.
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
drží	držet	k5eAaImIp3nS	držet
v	v	k7c6	v
první	první	k4xOgFnSc6	první
desítce	desítka	k1gFnSc6	desítka
žebříčku	žebříček	k1gInSc2	žebříček
oblíbenosti	oblíbenost	k1gFnSc2	oblíbenost
ankety	anketa	k1gFnSc2	anketa
Český	český	k2eAgMnSc1d1	český
/	/	kIx~	/
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
slaví	slavit	k5eAaImIp3nS	slavit
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejprodávanější	prodávaný	k2eAgNnSc1d3	nejprodávanější
tuzemskou	tuzemský	k2eAgFnSc7d1	tuzemská
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
byla	být	k5eAaImAgFnS	být
akademiky	akademik	k1gMnPc4	akademik
uvedena	uveden	k2eAgFnSc1d1	uvedena
do	do	k7c2	do
Síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
2014	[number]	k4	2014
v	v	k7c6	v
hudebních	hudební	k2eAgFnPc6d1	hudební
cenách	cena	k1gFnPc6	cena
Anděl	Anděla	k1gFnPc2	Anděla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výrazné	výrazný	k2eAgFnPc1d1	výrazná
nahrávky	nahrávka	k1gFnPc1	nahrávka
podle	podle	k7c2	podle
období	období	k1gNnSc2	období
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
60	[number]	k4	60
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
===	===	k?	===
</s>
</p>
<p>
<s>
Hany	Hana	k1gFnPc1	Hana
<g/>
,	,	kIx,	,
Svatej	svatat	k5eAaImRp2nS	svatat
kluk	kluk	k1gMnSc1	kluk
<g/>
,	,	kIx,	,
Poslední	poslední	k2eAgInSc1d1	poslední
šantán	šantán	k1gInSc1	šantán
<g/>
,	,	kIx,	,
Obraz	obraz	k1gInSc1	obraz
smutný	smutný	k2eAgInSc1d1	smutný
slečny	slečna	k1gFnSc2	slečna
<g/>
,	,	kIx,	,
Mrtvá	mrtvý	k2eAgFnSc1d1	mrtvá
láska	láska	k1gFnSc1	láska
<g/>
,	,	kIx,	,
Bludička	bludička	k1gFnSc1	bludička
Julie	Julie	k1gFnSc1	Julie
<g/>
,	,	kIx,	,
Tisíc	tisíc	k4xCgInSc1	tisíc
nových	nový	k2eAgNnPc2d1	nové
jmen	jméno	k1gNnPc2	jméno
<g/>
,	,	kIx,	,
Tanečnice	tanečnice	k1gFnSc1	tanečnice
<g/>
,	,	kIx,	,
Prý	prý	k9	prý
jsem	být	k5eAaImIp1nS	být
zhýralá	zhýralý	k2eAgFnSc1d1	zhýralá
<g/>
,	,	kIx,	,
Černý	Černý	k1gMnSc1	Černý
pasažér	pasažér	k1gMnSc1	pasažér
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
===	===	k?	===
70	[number]	k4	70
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
===	===	k?	===
</s>
</p>
<p>
<s>
Gvendolína	Gvendolína	k1gFnSc1	Gvendolína
<g/>
,	,	kIx,	,
Nemám	mít	k5eNaImIp1nS	mít
ráda	rád	k2eAgFnSc1d1	ráda
<g/>
,	,	kIx,	,
Já	já	k3xPp1nSc1	já
jsem	být	k5eAaImIp1nS	být
tvá	tvůj	k3xOp2gNnPc4	tvůj
neznámá	známý	k2eNgNnPc4d1	neznámé
<g/>
,	,	kIx,	,
<g/>
Koho	kdo	k3yQnSc4	kdo
tlačí	tlačit	k5eAaImIp3nS	tlačit
můra	můra	k1gFnSc1	můra
<g/>
,	,	kIx,	,
Fata	fatum	k1gNnPc1	fatum
morgana	morgan	k1gMnSc2	morgan
<g/>
,	,	kIx,	,
Sliby-Chyby	Sliby-Chyba	k1gMnSc2	Sliby-Chyba
<g/>
,	,	kIx,	,
Náš	náš	k3xOp1gInSc1	náš
dům	dům	k1gInSc1	dům
zní	znět	k5eAaImIp3nS	znět
smíchem	smích	k1gInSc7	smích
<g/>
,	,	kIx,	,
Já	já	k3xPp1nSc1	já
se	se	k3xPyFc4	se
vznáším	vznášet	k5eAaImIp1nS	vznášet
<g/>
,	,	kIx,	,
On	on	k3xPp3gMnSc1	on
je	být	k5eAaImIp3nS	být
někdo	někdo	k3yInSc1	někdo
<g/>
,	,	kIx,	,
já	já	k3xPp1nSc1	já
znám	znát	k5eAaImIp1nS	znát
ten	ten	k3xDgInSc4	ten
balzám	balzám	k1gInSc4	balzám
<g/>
,	,	kIx,	,
kluk	kluk	k1gMnSc1	kluk
na	na	k7c4	na
kterého	který	k3yIgMnSc4	který
<g />
.	.	kIx.	.
</s>
<s>
můžu	můžu	k?	můžu
dát	dát	k5eAaPmF	dát
<g/>
,	,	kIx,	,
Cesta	cesta	k1gFnSc1	cesta
ke	k	k7c3	k
štěstí	štěstí	k1gNnSc3	štěstí
<g/>
,	,	kIx,	,
Studánko	studánka	k1gFnSc5	studánka
stříbrná	stříbrná	k1gFnSc1	stříbrná
<g/>
,	,	kIx,	,
<g/>
Breviář	breviář	k1gInSc1	breviář
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
Kamarád	kamarád	k1gMnSc1	kamarád
<g/>
,	,	kIx,	,
Zima	zima	k1gFnSc1	zima
<g/>
,	,	kIx,	,
<g/>
zima	zima	k1gFnSc1	zima
<g/>
,	,	kIx,	,
<g/>
zima	zima	k1gFnSc1	zima
<g/>
,	,	kIx,	,
Dávám	dávat	k5eAaImIp1nS	dávat
kabát	kabát	k1gInSc4	kabát
na	na	k7c4	na
věšák	věšák	k1gInSc4	věšák
<g/>
,	,	kIx,	,
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
přítel	přítel	k1gMnSc1	přítel
můj	můj	k1gMnSc1	můj
<g/>
,	,	kIx,	,
Louka	louka	k1gFnSc1	louka
vábí	vábit	k5eAaImIp3nS	vábit
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Maluj	malovat	k5eAaImRp2nS	malovat
zase	zase	k9	zase
obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
<g/>
Můj	můj	k3xOp1gInSc1	můj
sen	sen	k1gInSc1	sen
je	být	k5eAaImIp3nS	být
touha	touha	k1gFnSc1	touha
žít	žít	k5eAaImF	žít
<g/>
,	,	kIx,	,
Žízeň	žízeň	k1gFnSc4	žízeň
<g/>
,	,	kIx,	,
Málokdo	málokdo	k3yInSc1	málokdo
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
Telegram	telegram	k1gInSc4	telegram
<g/>
,	,	kIx,	,
<g/>
Tak	tak	k9	tak
to	ten	k3xDgNnSc1	ten
jsem	být	k5eAaImIp1nS	být
já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
Sám	sám	k3xTgMnSc1	sám
jsi	být	k5eAaImIp2nS	být
šel	jít	k5eAaImAgMnS	jít
<g/>
,	,	kIx,	,
Proč	proč	k6eAd1	proč
ti	ty	k3xPp2nSc3	ty
na	na	k7c6	na
tom	ten	k3xDgMnSc6	ten
tak	tak	k6eAd1	tak
záleží	záležet	k5eAaImIp3nS	záležet
<g/>
,	,	kIx,	,
Píseň	píseň	k1gFnSc1	píseň
labutí	labutí	k2eAgFnSc1d1	labutí
<g/>
,	,	kIx,	,
Kosmický	kosmický	k2eAgInSc1d1	kosmický
sen	sen	k1gInSc1	sen
<g/>
,	,	kIx,	,
Náskok	náskok	k1gInSc1	náskok
</s>
</p>
<p>
<s>
Duety	duet	k1gInPc1	duet
<g/>
:	:	kIx,	:
s	s	k7c7	s
Petrem	Petr	k1gMnSc7	Petr
Rezkem	Rezek	k1gMnSc7	Rezek
<g/>
:	:	kIx,	:
Asi	asi	k9	asi
<g/>
,	,	kIx,	,
asi	asi	k9	asi
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Duhová	duhový	k2eAgFnSc1d1	Duhová
víla	víla	k1gFnSc1	víla
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dotazník	dotazník	k1gInSc1	dotazník
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ta	ten	k3xDgFnSc1	ten
pusa	pusa	k1gFnSc1	pusa
je	být	k5eAaImIp3nS	být
Tvá	tvůj	k3xOp2gFnSc1	tvůj
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
s	s	k7c7	s
italským	italský	k2eAgMnSc7d1	italský
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Drupim	Drupim	k1gMnSc1	Drupim
<g/>
:	:	kIx,	:
Setkání	setkání	k1gNnSc1	setkání
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
na	na	k7c6	na
SP	SP	kA	SP
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
80	[number]	k4	80
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
===	===	k?	===
</s>
</p>
<p>
<s>
Rýmy	rým	k1gInPc1	rým
<g/>
,	,	kIx,	,
Perný	perný	k2eAgInSc1d1	perný
den	den	k1gInSc1	den
<g/>
,	,	kIx,	,
Opona	opona	k1gFnSc1	opona
<g/>
,	,	kIx,	,
Líto	líto	k6eAd1	líto
je	být	k5eAaImIp3nS	být
mi	já	k3xPp1nSc3	já
líto	líto	k6eAd1	líto
<g/>
,	,	kIx,	,
Zásnuby	zásnuba	k1gFnPc1	zásnuba
<g/>
,	,	kIx,	,
Tam	tam	k6eAd1	tam
pod	pod	k7c7	pod
naší	náš	k3xOp1gFnSc7	náš
strání	stráň	k1gFnSc7	stráň
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Benjamín	Benjamín	k1gMnSc1	Benjamín
<g/>
,	,	kIx,	,
Usnul	usnout	k5eAaPmAgMnS	usnout
nám	my	k3xPp1nPc3	my
spí	spát	k5eAaImIp3nS	spát
<g/>
,	,	kIx,	,
Mimořádná	mimořádný	k2eAgFnSc1d1	mimořádná
linka	linka	k1gFnSc1	linka
<g/>
,	,	kIx,	,
Sloky	sloka	k1gFnPc1	sloka
trochu	trochu	k6eAd1	trochu
smutné	smutný	k2eAgFnPc1d1	smutná
lásky	láska	k1gFnPc1	láska
<g/>
,	,	kIx,	,
Nápad	nápad	k1gInSc1	nápad
<g/>
,	,	kIx,	,
Biograf	biograf	k1gInSc1	biograf
láska	láska	k1gFnSc1	láska
<g/>
,	,	kIx,	,
Žízeň	žízeň	k1gFnSc1	žízeň
po	po	k7c6	po
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
<g/>
Počítadlo	počítadlo	k1gNnSc4	počítadlo
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
Láska	láska	k1gFnSc1	láska
je	být	k5eAaImIp3nS	být
počasí	počasí	k1gNnSc4	počasí
<g/>
,	,	kIx,	,
Proč	proč	k6eAd1	proč
nejsi	být	k5eNaImIp2nS	být
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
Vím	vědět	k5eAaImIp1nS	vědět
málo	málo	k1gNnSc1	málo
<g/>
,	,	kIx,	,
Křižovatka	křižovatka	k1gFnSc1	křižovatka
6	[number]	k4	6
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Co	co	k3yQnSc1	co
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
<g/>
,	,	kIx,	,
<g/>
Dneska	Dneska	k?	Dneska
už	už	k6eAd1	už
to	ten	k3xDgNnSc1	ten
vím	vědět	k5eAaImIp1nS	vědět
<g/>
,	,	kIx,	,
Maják	maják	k1gInSc1	maják
<g/>
,	,	kIx,	,
Řekni	říct	k5eAaPmRp2nS	říct
třikrát	třikrát	k6eAd1	třikrát
lásko	láska	k1gFnSc5	láska
<g/>
,	,	kIx,	,
Už	už	k6eAd1	už
dlouho	dlouho	k6eAd1	dlouho
se	se	k3xPyFc4	se
mi	já	k3xPp1nSc3	já
zdáš	zdát	k5eAaImIp2nS	zdát
<g/>
,	,	kIx,	,
Mys	mys	k1gInSc1	mys
dobrých	dobrý	k2eAgFnPc2d1	dobrá
nadějí	naděje	k1gFnPc2	naděje
<g/>
,	,	kIx,	,
Nešlap	šlapat	k5eNaImRp2nS	šlapat
nelámej	lámat	k5eNaImRp2nS	lámat
<g/>
,	,	kIx,	,
<g/>
Noční	noční	k2eAgInSc1d1	noční
dopis	dopis	k1gInSc1	dopis
<g/>
,	,	kIx,	,
Sláva	Sláva	k1gFnSc1	Sláva
je	být	k5eAaImIp3nS	být
bál	bál	k1gInSc1	bál
<g/>
,	,	kIx,	,
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
svítím	svítit	k5eAaImIp1nS	svítit
<g/>
,	,	kIx,	,
Sítě	síť	k1gFnPc1	síť
kroků	krok	k1gInPc2	krok
tvých	tvůj	k3xOp2gInPc2	tvůj
<g/>
,	,	kIx,	,
Letní	letní	k2eAgMnPc1d1	letní
kluci	kluk	k1gMnPc1	kluk
<g/>
,	,	kIx,	,
Džínovej	Džínovej	k?	Džínovej
kluk	kluk	k1gMnSc1	kluk
<g/>
,	,	kIx,	,
Náhlá	náhlý	k2eAgNnPc4d1	náhlé
loučení	loučení	k1gNnPc4	loučení
<g/>
,	,	kIx,	,
Láska	láska	k1gFnSc1	láska
na	na	k7c4	na
inzerát	inzerát	k1gInSc4	inzerát
<g/>
,	,	kIx,	,
Já	já	k3xPp1nSc1	já
chtěla	chtít	k5eAaImAgFnS	chtít
jsem	být	k5eAaImIp1nS	být
ti	ten	k3xDgMnPc1	ten
báseň	báseň	k1gFnSc4	báseň
psát	psát	k5eAaImF	psát
<g/>
,	,	kIx,	,
Dobré	dobrý	k2eAgNnSc1d1	dobré
jitro	jitro	k1gNnSc1	jitro
<g/>
,	,	kIx,	,
Už	už	k6eAd1	už
se	se	k3xPyFc4	se
mi	já	k3xPp1nSc3	já
nechce	chtít	k5eNaImIp3nS	chtít
jít	jít	k5eAaImF	jít
dál	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
Tu	tu	k6eAd1	tu
<g/>
,	,	kIx,	,
<g/>
<g />
.	.	kIx.	.
</s>
<s>
tu	tu	k6eAd1	tu
<g/>
,	,	kIx,	,
Naše	náš	k3xOp1gFnSc1	náš
láska	láska	k1gFnSc1	láska
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
L	L	kA	L
<g/>
,	,	kIx,	,
Kousek	kousek	k6eAd1	kousek
cesty	cesta	k1gFnPc4	cesta
s	s	k7c7	s
tebou	ty	k3xPp2nSc7	ty
<g/>
,	,	kIx,	,
Zlato	zlato	k1gNnSc1	zlato
<g/>
,	,	kIx,	,
Srážka	srážka	k1gFnSc1	srážka
s	s	k7c7	s
láskou	láska	k1gFnSc7	láska
<g/>
,	,	kIx,	,
Skleněné	skleněný	k2eAgInPc1d1	skleněný
sny	sen	k1gInPc1	sen
<g/>
,	,	kIx,	,
Mám	mít	k5eAaImIp1nS	mít
plán	plán	k1gInSc1	plán
<g/>
,	,	kIx,	,
Živá	živý	k2eAgFnSc1d1	živá
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
Zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
<g/>
,	,	kIx,	,
Modrá	modrý	k2eAgFnSc1d1	modrá
čajovna	čajovna	k1gFnSc1	čajovna
<g/>
,	,	kIx,	,
Rybičko	rybička	k1gFnSc5	rybička
zlatá	zlatý	k2eAgNnPc4d1	Zlaté
<g/>
,	,	kIx,	,
přeju	přát	k5eAaImIp1nS	přát
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
Měsíc	měsíc	k1gInSc1	měsíc
je	být	k5eAaImIp3nS	být
den	den	k1gInSc4	den
ode	ode	k7c2	ode
dne	den	k1gInSc2	den
krásnější	krásný	k2eAgFnSc2d2	krásnější
</s>
</p>
<p>
<s>
Duety	duet	k1gInPc1	duet
<g/>
:	:	kIx,	:
Dávné	dávný	k2eAgFnPc1d1	dávná
lásky	láska	k1gFnPc1	láska
<g/>
,	,	kIx,	,
Setkání	setkání	k1gNnSc1	setkání
<g/>
,	,	kIx,	,
Hej	hej	k0	hej
<g/>
,	,	kIx,	,
mistře	mistr	k1gMnSc5	mistr
basů	bas	k1gInPc2	bas
<g/>
,	,	kIx,	,
Jen	jen	k9	jen
pár	pár	k4xCyI	pár
dnů	den	k1gInPc2	den
</s>
</p>
<p>
<s>
S	s	k7c7	s
Hložkem	hložek	k1gInSc7	hložek
a	a	k8xC	a
Kotvaldem	Kotvald	k1gInSc7	Kotvald
<g/>
:	:	kIx,	:
Diskohrátky	Diskohrátka	k1gFnPc1	Diskohrátka
<g/>
,	,	kIx,	,
Polibek	polibek	k1gInSc1	polibek
<g/>
,	,	kIx,	,
Černý	Černý	k1gMnSc1	Černý
páv	páv	k1gMnSc1	páv
<g/>
,	,	kIx,	,
Máj	máj	k1gFnSc1	máj
je	být	k5eAaImIp3nS	být
máj	máj	k1gInSc4	máj
<g/>
,	,	kIx,	,
Jinak	jinak	k6eAd1	jinak
to	ten	k3xDgNnSc1	ten
nejde	jít	k5eNaImIp3nS	jít
<g/>
,	,	kIx,	,
Kostky	kostka	k1gFnPc1	kostka
jsou	být	k5eAaImIp3nP	být
vrženy	vržen	k2eAgFnPc1d1	vržena
<g/>
,	,	kIx,	,
Spěchám	spěchat	k5eAaImIp1nS	spěchat
<g/>
,	,	kIx,	,
Pro	pro	k7c4	pro
dva	dva	k4xCgInPc4	dva
tři	tři	k4xCgInPc4	tři
úsměvy	úsměv	k1gInPc4	úsměv
<g/>
,	,	kIx,	,
Auťák	Auťák	k?	Auťák
na	na	k7c4	na
lásku	láska	k1gFnSc4	láska
<g/>
,	,	kIx,	,
Můj	můj	k3xOp1gInSc1	můj
čas	čas	k1gInSc1	čas
</s>
</p>
<p>
<s>
===	===	k?	===
90	[number]	k4	90
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
===	===	k?	===
</s>
</p>
<p>
<s>
Beránek	Beránek	k1gMnSc1	Beránek
<g/>
,	,	kIx,	,
Andělé	anděl	k1gMnPc1	anděl
<g/>
,	,	kIx,	,
Dnes	dnes	k6eAd1	dnes
nejsem	být	k5eNaImIp1nS	být
doma	doma	k6eAd1	doma
<g/>
,	,	kIx,	,
Skanzen	skanzen	k1gInSc4	skanzen
bídy	bída	k1gFnSc2	bída
<g/>
,	,	kIx,	,
Čas	čas	k1gInSc4	čas
odejít	odejít	k5eAaPmF	odejít
<g/>
,	,	kIx,	,
Rozhovor	rozhovor	k1gInSc1	rozhovor
v	v	k7c6	v
tichu	ticho	k1gNnSc6	ticho
<g/>
,	,	kIx,	,
Merilyn	Merilyn	k1gNnSc1	Merilyn
<g/>
,	,	kIx,	,
Moře	moře	k1gNnSc1	moře
samoty	samota	k1gFnSc2	samota
<g/>
,	,	kIx,	,
Žena	žena	k1gFnSc1	žena
za	za	k7c7	za
Tvými	tvůj	k3xOp2gNnPc7	tvůj
zády	záda	k1gNnPc7	záda
<g/>
,	,	kIx,	,
Jako	jako	k8xS	jako
starý	starý	k2eAgInSc1d1	starý
strom	strom	k1gInSc1	strom
<g/>
,	,	kIx,	,
Když	když	k8xS	když
nemůžu	nemůžu	k?	nemůžu
spát	spát	k5eAaImF	spát
<g/>
,	,	kIx,	,
Kdybych	kdyby	kYmCp1nS	kdyby
byla	být	k5eAaImAgFnS	být
bůh	bůh	k1gMnSc1	bůh
<g/>
,	,	kIx,	,
J	J	kA	J
<g/>
́	́	k?	́
<g/>
tame	tame	k1gInSc1	tame
<g/>
,	,	kIx,	,
Když	když	k8xS	když
tě	ty	k3xPp2nSc4	ty
ztrácím	ztrácet	k5eAaImIp1nS	ztrácet
<g/>
,	,	kIx,	,
Na	na	k7c4	na
co	co	k3yInSc4	co
já	já	k3xPp1nSc1	já
tě	ty	k3xPp2nSc4	ty
mám	mít	k5eAaImIp1nS	mít
</s>
</p>
<p>
<s>
Duety	duet	k1gInPc1	duet
<g/>
:	:	kIx,	:
Ave	ave	k1gNnSc1	ave
Maria	Maria	k1gFnSc1	Maria
<g/>
,	,	kIx,	,
Svítíš	svítit	k5eAaImIp2nS	svítit
mi	já	k3xPp1nSc3	já
v	v	k7c6	v
tmách	tma	k1gFnPc6	tma
<g/>
,	,	kIx,	,
Mží	mžít	k5eAaImIp3nP	mžít
<g/>
,	,	kIx,	,
Den	den	k1gInSc4	den
jako	jako	k8xC	jako
obrázek	obrázek	k1gInSc4	obrázek
<g/>
,	,	kIx,	,
Přijď	přijít	k5eAaPmRp2nS	přijít
aspoň	aspoň	k9	aspoň
na	na	k7c4	na
chvíli	chvíle	k1gFnSc4	chvíle
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
z	z	k7c2	z
přelomu	přelom	k1gInSc2	přelom
80	[number]	k4	80
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
<g/>
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
:	:	kIx,	:
Šmoulí	Šmoule	k1gFnPc2	Šmoule
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
Tom	Tom	k1gMnSc1	Tom
a	a	k8xC	a
Jerry	Jerra	k1gFnPc1	Jerra
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Písničky	písnička	k1gFnPc1	písnička
pro	pro	k7c4	pro
Barbie	Barbie	k1gFnPc4	Barbie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Želvy	želva	k1gFnPc1	želva
Ninja	Ninjum	k1gNnSc2	Ninjum
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
===	===	k?	===
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
===	===	k?	===
</s>
</p>
<p>
<s>
Za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
chvíli	chvíle	k1gFnSc4	chvíle
s	s	k7c7	s
tebou	ty	k3xPp2nSc7	ty
platím	platit	k5eAaImIp1nS	platit
<g/>
,	,	kIx,	,
Je	být	k5eAaImIp3nS	být
naprosto	naprosto	k6eAd1	naprosto
nezbytné	nezbytný	k2eAgNnSc1d1	nezbytný
<g/>
,	,	kIx,	,
Zůstaň	zůstat	k5eAaPmRp2nS	zůstat
tady	tady	k6eAd1	tady
léto	léto	k1gNnSc4	léto
<g/>
,	,	kIx,	,
Ode	ode	k7c2	ode
zdi	zeď	k1gFnSc2	zeď
ke	k	k7c3	k
zdi	zeď	k1gFnSc3	zeď
<g/>
,	,	kIx,	,
Pokušení	pokušení	k1gNnSc3	pokušení
<g/>
,	,	kIx,	,
Navěky	navěky	k6eAd1	navěky
zůstane	zůstat	k5eAaPmIp3nS	zůstat
pouze	pouze	k6eAd1	pouze
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
Co	co	k3yInSc1	co
jsi	být	k5eAaImIp2nS	být
zač	zač	k6eAd1	zač
<g/>
,	,	kIx,	,
Lásko	láska	k1gFnSc5	láska
vítej	vítat	k5eAaImRp2nS	vítat
<g/>
,	,	kIx,	,
Smůlu	smůla	k1gFnSc4	smůla
máš	mít	k5eAaImIp2nS	mít
<g/>
,	,	kIx,	,
Tam	tam	k6eAd1	tam
kde	kde	k6eAd1	kde
něco	něco	k3yInSc1	něco
končí	končit	k5eAaImIp3nS	končit
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Adieu	adieu	k0	adieu
<g/>
,	,	kIx,	,
S	s	k7c7	s
láskou	láska	k1gFnSc7	láska
je	být	k5eAaImIp3nS	být
svět	svět	k1gInSc1	svět
akorát	akorát	k6eAd1	akorát
<g/>
,	,	kIx,	,
Zloděj	zloděj	k1gMnSc1	zloděj
duší	duše	k1gFnPc2	duše
<g/>
,	,	kIx,	,
Z	z	k7c2	z
nebe	nebe	k1gNnSc2	nebe
pláče	plakat	k5eAaImIp3nS	plakat
déšť	déšť	k1gInSc1	déšť
<g/>
,	,	kIx,	,
Zítra	zítra	k6eAd1	zítra
se	se	k3xPyFc4	se
zvedne	zvednout	k5eAaPmIp3nS	zvednout
vítr	vítr	k1gInSc1	vítr
<g/>
,	,	kIx,	,
Zvon	zvon	k1gInSc1	zvon
vánoční	vánoční	k2eAgInSc1d1	vánoční
<g/>
,	,	kIx,	,
Rande	rande	k1gNnSc1	rande
u	u	k7c2	u
zdi	zeď	k1gFnSc2	zeď
nářků	nářek	k1gInPc2	nářek
<g/>
,	,	kIx,	,
S	s	k7c7	s
tebou	ty	k3xPp2nSc7	ty
<g/>
,	,	kIx,	,
Bleriot	Bleriot	k1gMnSc1	Bleriot
<g/>
,	,	kIx,	,
Můj	můj	k3xOp1gMnSc1	můj
chlap	chlap	k1gMnSc1	chlap
<g/>
,	,	kIx,	,
Už	už	k6eAd1	už
nemám	mít	k5eNaImIp1nS	mít
křídla	křídlo	k1gNnPc4	křídlo
tvá	tvůj	k3xOp2gFnSc1	tvůj
<g/>
,	,	kIx,	,
Je	být	k5eAaImIp3nS	být
tady	tady	k6eAd1	tady
podzim	podzim	k1gInSc4	podzim
<g/>
,	,	kIx,	,
O	o	k7c6	o
Lásce	láska	k1gFnSc6	láska
<g/>
,	,	kIx,	,
Hraj	hrát	k5eAaImRp2nS	hrát
muziko	muzika	k1gFnSc5	muzika
<g/>
,	,	kIx,	,
Já	já	k3xPp1nSc1	já
nemám	mít	k5eNaImIp1nS	mít
strach	strach	k1gInSc4	strach
<g/>
,	,	kIx,	,
Není	být	k5eNaImIp3nS	být
taková	takový	k3xDgFnSc1	takový
</s>
</p>
<p>
<s>
==	==	k?	==
Ceny	cena	k1gFnPc1	cena
a	a	k8xC	a
uznání	uznání	k1gNnSc1	uznání
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Tuzemské	tuzemský	k2eAgMnPc4d1	tuzemský
===	===	k?	===
</s>
</p>
<p>
<s>
1963	[number]	k4	1963
–	–	k?	–
Hledáme	hledat	k5eAaImIp1nP	hledat
nové	nový	k2eAgInPc4d1	nový
talenty	talent	k1gInPc4	talent
–	–	k?	–
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
1965	[number]	k4	1965
–	–	k?	–
Dvanáct	dvanáct	k4xCc4	dvanáct
na	na	k7c6	na
houpačce	houpačka	k1gFnSc6	houpačka
<g/>
(	(	kIx(	(
<g/>
hitparáda	hitparáda	k1gFnSc1	hitparáda
Československého	československý	k2eAgInSc2d1	československý
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
)	)	kIx)	)
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
z	z	k7c2	z
12	[number]	k4	12
<g/>
.	.	kIx.	.
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
Písnička	písnička	k1gFnSc1	písnička
v	v	k7c6	v
bílém	bílé	k1gNnSc6	bílé
</s>
</p>
<p>
<s>
1968	[number]	k4	1968
–	–	k?	–
Třináct	třináct	k4xCc4	třináct
na	na	k7c6	na
houpačce	houpačka	k1gFnSc6	houpačka
–	–	k?	–
12	[number]	k4	12
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
z	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
Svatej	svatat	k5eAaImRp2nS	svatat
kluk	kluk	k1gMnSc1	kluk
</s>
</p>
<p>
<s>
1969	[number]	k4	1969
–	–	k?	–
Moravský	moravský	k2eAgMnSc1d1	moravský
vrabec	vrabec	k1gMnSc1	vrabec
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
pro	pro	k7c4	pro
nejpopulárnějšího	populární	k2eAgMnSc4d3	nejpopulárnější
zpěváka	zpěvák	k1gMnSc4	zpěvák
Moravy	Morava	k1gFnSc2	Morava
1968	[number]	k4	1968
</s>
</p>
<p>
<s>
1969	[number]	k4	1969
–	–	k?	–
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
slavík	slavík	k1gMnSc1	slavík
1968	[number]	k4	1968
–	–	k?	–
5	[number]	k4	5
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
1970	[number]	k4	1970
–	–	k?	–
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
slavík	slavík	k1gMnSc1	slavík
1969	[number]	k4	1969
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
1970	[number]	k4	1970
–	–	k?	–
Moravský	moravský	k2eAgMnSc1d1	moravský
vrabec	vrabec	k1gMnSc1	vrabec
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
pro	pro	k7c4	pro
nejpopulárnějšího	populární	k2eAgMnSc4d3	nejpopulárnější
zpěváka	zpěvák	k1gMnSc4	zpěvák
Moravy	Morava	k1gFnSc2	Morava
1969	[number]	k4	1969
</s>
</p>
<p>
<s>
1970	[number]	k4	1970
–	–	k?	–
Žebřík	žebřík	k1gInSc4	žebřík
Mladé	mladý	k2eAgFnSc2d1	mladá
fronty	fronta	k1gFnSc2	fronta
<g/>
(	(	kIx(	(
<g/>
hitparáda	hitparáda	k1gFnSc1	hitparáda
Mladé	mladý	k2eAgFnSc2d1	mladá
fronty	fronta	k1gFnSc2	fronta
<g/>
)	)	kIx)	)
I0	I0	k1gFnSc1	I0
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
z	z	k7c2	z
30	[number]	k4	30
<g/>
.	.	kIx.	.
–	–	k?	–
Koho	kdo	k3yQnSc4	kdo
tlačí	tlačit	k5eAaImIp3nS	tlačit
múra	múra	k1gFnSc1	múra
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
z	z	k7c2	z
30	[number]	k4	30
<g/>
.	.	kIx.	.
–	–	k?	–
Já	já	k3xPp1nSc1	já
jsem	být	k5eAaImIp1nS	být
tvá	tvůj	k3xOp2gNnPc4	tvůj
neznámá	známý	k2eNgNnPc4d1	neznámé
</s>
</p>
<p>
<s>
1970	[number]	k4	1970
–	–	k?	–
Žebřík	žebřík	k1gInSc4	žebřík
Mladé	mladý	k2eAgFnSc2d1	mladá
fronty	fronta	k1gFnSc2	fronta
II	II	kA	II
–	–	k?	–
7	[number]	k4	7
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
z	z	k7c2	z
30	[number]	k4	30
<g/>
.	.	kIx.	.
–	–	k?	–
Já	já	k3xPp1nSc1	já
jsem	být	k5eAaImIp1nS	být
tvá	tvůj	k3xOp2gFnSc1	tvůj
neznámá	známý	k2eNgFnSc1d1	neznámá
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
z	z	k7c2	z
30	[number]	k4	30
<g/>
.	.	kIx.	.
–	–	k?	–
Koho	kdo	k3yQnSc4	kdo
tlačí	tlačit	k5eAaImIp3nS	tlačit
múra	múra	k6eAd1	múra
</s>
</p>
<p>
<s>
1.6	[number]	k4	1.6
<g/>
.1971	.1971	k4	.1971
–	–	k?	–
Žebřík	žebřík	k1gInSc4	žebřík
Mladé	mladý	k2eAgFnSc2d1	mladá
fronty	fronta	k1gFnSc2	fronta
–	–	k?	–
28	[number]	k4	28
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
z	z	k7c2	z
30	[number]	k4	30
<g/>
.	.	kIx.	.
–	–	k?	–
Gvendolína	Gvendolína	k1gFnSc1	Gvendolína
</s>
</p>
<p>
<s>
1971	[number]	k4	1971
–	–	k?	–
šestá	šestý	k4xOgFnSc1	šestý
nejprodávanější	prodávaný	k2eAgFnSc1d3	nejprodávanější
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
roku	rok	k1gInSc2	rok
na	na	k7c6	na
značce	značka	k1gFnSc6	značka
Supraphon	supraphon	k1gInSc1	supraphon
</s>
</p>
<p>
<s>
1972	[number]	k4	1972
–	–	k?	–
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
slavík	slavík	k1gMnSc1	slavík
1971	[number]	k4	1971
–	–	k?	–
5	[number]	k4	5
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
1973	[number]	k4	1973
–	–	k?	–
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
slavík	slavík	k1gMnSc1	slavík
1972	[number]	k4	1972
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
1973	[number]	k4	1973
–	–	k?	–
2	[number]	k4	2
<g/>
.	.	kIx.	.
cena	cena	k1gFnSc1	cena
Supraphonu	supraphon	k1gInSc2	supraphon
1972	[number]	k4	1972
(	(	kIx(	(
<g/>
druhá	druhý	k4xOgFnSc1	druhý
nejprodávanější	prodávaný	k2eAgFnSc1d3	nejprodávanější
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
na	na	k7c6	na
značce	značka	k1gFnSc6	značka
Supraphon	supraphon	k1gInSc1	supraphon
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1973	[number]	k4	1973
–	–	k?	–
šestá	šestý	k4xOgFnSc1	šestý
nejprodávanější	prodávaný	k2eAgFnSc1d3	nejprodávanější
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
roku	rok	k1gInSc2	rok
na	na	k7c6	na
značce	značka	k1gFnSc6	značka
Supraphon	supraphon	k1gInSc1	supraphon
</s>
</p>
<p>
<s>
1974	[number]	k4	1974
–	–	k?	–
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
slavík	slavík	k1gMnSc1	slavík
1973	[number]	k4	1973
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
1974	[number]	k4	1974
–	–	k?	–
sedmá	sedmý	k4xOgFnSc1	sedmý
nejprodávanější	prodávaný	k2eAgFnSc1d3	nejprodávanější
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
roku	rok	k1gInSc2	rok
na	na	k7c6	na
značce	značka	k1gFnSc6	značka
Supraphon	supraphon	k1gInSc1	supraphon
</s>
</p>
<p>
<s>
1975	[number]	k4	1975
–	–	k?	–
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
slavík	slavík	k1gMnSc1	slavík
1974	[number]	k4	1974
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
1975	[number]	k4	1975
–	–	k?	–
čtvrtá	čtvrtá	k1gFnSc1	čtvrtá
nejprodávanější	prodávaný	k2eAgFnSc1d3	nejprodávanější
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
roku	rok	k1gInSc2	rok
na	na	k7c6	na
značce	značka	k1gFnSc6	značka
Supraphon	supraphon	k1gInSc1	supraphon
</s>
</p>
<p>
<s>
1976	[number]	k4	1976
–	–	k?	–
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
slavík	slavík	k1gMnSc1	slavík
1975	[number]	k4	1975
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
1977	[number]	k4	1977
–	–	k?	–
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
slavík	slavík	k1gMnSc1	slavík
1976	[number]	k4	1976
–	–	k?	–
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
1977	[number]	k4	1977
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
cena	cena	k1gFnSc1	cena
Supraphonu	supraphon	k1gInSc2	supraphon
1976	[number]	k4	1976
(	(	kIx(	(
<g/>
třetí	třetí	k4xOgFnSc1	třetí
nejprodávanější	prodávaný	k2eAgFnSc1d3	nejprodávanější
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
na	na	k7c6	na
značce	značka	k1gFnSc6	značka
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1977	[number]	k4	1977
–	–	k?	–
1985	[number]	k4	1985
–	–	k?	–
Zlatý	zlatý	k2eAgInSc1d1	zlatý
slavík	slavík	k1gInSc1	slavík
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
1977	[number]	k4	1977
–	–	k?	–
SP	SP	kA	SP
Duhová	duhový	k2eAgFnSc1d1	Duhová
víla	víla	k1gFnSc1	víla
–	–	k?	–
nejprodávanější	prodávaný	k2eAgFnSc1d3	nejprodávanější
deska	deska	k1gFnSc1	deska
roku	rok	k1gInSc2	rok
</s>
</p>
<p>
<s>
1978	[number]	k4	1978
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
cena	cena	k1gFnSc1	cena
Supraphonu	supraphon	k1gInSc2	supraphon
1977	[number]	k4	1977
(	(	kIx(	(
<g/>
nejprodávanější	prodávaný	k2eAgFnSc1d3	nejprodávanější
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
na	na	k7c6	na
značce	značka	k1gFnSc6	značka
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1979	[number]	k4	1979
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
cena	cena	k1gFnSc1	cena
Supraphonu	supraphon	k1gInSc2	supraphon
1978	[number]	k4	1978
(	(	kIx(	(
<g/>
nejprodávanější	prodávaný	k2eAgFnSc1d3	nejprodávanější
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
na	na	k7c6	na
značce	značka	k1gFnSc6	značka
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1980	[number]	k4	1980
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
cena	cena	k1gFnSc1	cena
Supraphonu	supraphon	k1gInSc2	supraphon
1979	[number]	k4	1979
(	(	kIx(	(
<g/>
nejprodávanější	prodávaný	k2eAgFnSc1d3	nejprodávanější
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
na	na	k7c6	na
značce	značka	k1gFnSc6	značka
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1980	[number]	k4	1980
srpen	srpen	k1gInSc4	srpen
–	–	k?	–
Nejprodávanější	prodávaný	k2eAgInPc4d3	nejprodávanější
singly	singl	k1gInPc4	singl
Supraphonu	supraphon	k1gInSc2	supraphon
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
s	s	k7c7	s
písní	píseň	k1gFnPc2	píseň
Setkání	setkání	k1gNnSc1	setkání
</s>
</p>
<p>
<s>
1981	[number]	k4	1981
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
cena	cena	k1gFnSc1	cena
Supraphonu	supraphon	k1gInSc2	supraphon
1980	[number]	k4	1980
(	(	kIx(	(
<g/>
nejprodávanější	prodávaný	k2eAgFnSc1d3	nejprodávanější
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
na	na	k7c6	na
značce	značka	k1gFnSc6	značka
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1982	[number]	k4	1982
<g/>
–	–	k?	–
<g/>
1983	[number]	k4	1983
–	–	k?	–
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
slavík	slavík	k1gMnSc1	slavík
–	–	k?	–
absolutní	absolutní	k2eAgFnSc1d1	absolutní
vítězka	vítězka	k1gFnSc1	vítězka
</s>
</p>
<p>
<s>
1982	[number]	k4	1982
<g/>
–	–	k?	–
<g/>
1983	[number]	k4	1983
–	–	k?	–
Sedmikráska	sedmikráska	k1gFnSc1	sedmikráska
Sedmičky	sedmička	k1gFnSc2	sedmička
</s>
</p>
<p>
<s>
1982	[number]	k4	1982
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
cena	cena	k1gFnSc1	cena
Supraphonu	supraphon	k1gInSc2	supraphon
1981	[number]	k4	1981
<g/>
(	(	kIx(	(
<g/>
nejprodávanější	prodávaný	k2eAgFnSc1d3	nejprodávanější
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
na	na	k7c6	na
značce	značka	k1gFnSc6	značka
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1983	[number]	k4	1983
–	–	k?	–
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
deska	deska	k1gFnSc1	deska
Supraphonu	supraphon	k1gInSc2	supraphon
za	za	k7c4	za
1	[number]	k4	1
000	[number]	k4	000
000	[number]	k4	000
prodaných	prodaný	k2eAgInPc2d1	prodaný
LP	LP	kA	LP
desek	deska	k1gFnPc2	deska
</s>
</p>
<p>
<s>
1983	[number]	k4	1983
–	–	k?	–
zasloužilá	zasloužilý	k2eAgFnSc1d1	zasloužilá
umělkyně	umělkyně	k1gFnSc1	umělkyně
</s>
</p>
<p>
<s>
1983	[number]	k4	1983
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
cena	cena	k1gFnSc1	cena
Supraphonu	supraphon	k1gInSc2	supraphon
1982	[number]	k4	1982
(	(	kIx(	(
<g/>
nejprodávanější	prodávaný	k2eAgFnSc1d3	nejprodávanější
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
na	na	k7c6	na
značce	značka	k1gFnSc6	značka
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
–	–	k?	–
1985	[number]	k4	1985
–	–	k?	–
Televizní	televizní	k2eAgFnSc1d1	televizní
rolnička	rolnička	k1gFnSc1	rolnička
–	–	k?	–
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
(	(	kIx(	(
<g/>
po	po	k7c6	po
Karlu	Karel	k1gMnSc6	Karel
Gottovi	Gott	k1gMnSc6	Gott
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
cena	cena	k1gFnSc1	cena
Supraphonu	supraphon	k1gInSc2	supraphon
1983	[number]	k4	1983
(	(	kIx(	(
<g/>
nejprodávanější	prodávaný	k2eAgFnSc1d3	nejprodávanější
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
na	na	k7c6	na
značce	značka	k1gFnSc6	značka
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
cena	cena	k1gFnSc1	cena
Supraphonu	supraphon	k1gInSc2	supraphon
1984	[number]	k4	1984
(	(	kIx(	(
<g/>
nejprodávanější	prodávaný	k2eAgFnSc1d3	nejprodávanější
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
na	na	k7c6	na
značce	značka	k1gFnSc6	značka
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
–	–	k?	–
Diskoslavík	Diskoslavík	k1gInSc1	Diskoslavík
1984	[number]	k4	1984
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
říjen	říjen	k1gInSc1	říjen
–	–	k?	–
20	[number]	k4	20
nej	nej	k?	nej
SP	SP	kA	SP
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
Letní	letní	k2eAgMnPc1d1	letní
kluci	kluk	k1gMnPc1	kluk
</s>
</p>
<p>
<s>
1986	[number]	k4	1986
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
cena	cena	k1gFnSc1	cena
Supraphonu	supraphon	k1gInSc2	supraphon
1985	[number]	k4	1985
(	(	kIx(	(
<g/>
nejprodávanější	prodávaný	k2eAgFnSc1d3	nejprodávanější
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
na	na	k7c6	na
značce	značka	k1gFnSc6	značka
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1986	[number]	k4	1986
–	–	k?	–
Televizní	televizní	k2eAgFnSc1d1	televizní
rolnička	rolnička	k1gFnSc1	rolnička
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
1986	[number]	k4	1986
–	–	k?	–
Čestné	čestný	k2eAgNnSc4d1	čestné
uznání	uznání	k1gNnSc4	uznání
Pragokoncertu	pragokoncert	k1gInSc2	pragokoncert
</s>
</p>
<p>
<s>
1986	[number]	k4	1986
–	–	k?	–
Pop	pop	k1gInSc1	pop
'	'	kIx"	'
<g/>
86	[number]	k4	86
–	–	k?	–
anketa	anketa	k1gFnSc1	anketa
časopisu	časopis	k1gInSc2	časopis
Populár	populár	k1gInSc1	populár
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
1987	[number]	k4	1987
–	–	k?	–
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
slavík	slavík	k1gMnSc1	slavík
1986	[number]	k4	1986
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
1987	[number]	k4	1987
–	–	k?	–
Televizní	televizní	k2eAgFnSc1d1	televizní
rolnička	rolnička	k1gFnSc1	rolnička
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
1988	[number]	k4	1988
–	–	k?	–
Diskoslavík	Diskoslavík	k1gInSc1	Diskoslavík
1987	[number]	k4	1987
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
1988	[number]	k4	1988
únor	únor	k1gInSc4	únor
–	–	k?	–
SP	SP	kA	SP
desky	deska	k1gFnSc2	deska
10	[number]	k4	10
nej	nej	k?	nej
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
Mám	mít	k5eAaImIp1nS	mít
plán	plán	k1gInSc4	plán
</s>
</p>
<p>
<s>
1988	[number]	k4	1988
–	–	k?	–
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
slavík	slavík	k1gMnSc1	slavík
1987	[number]	k4	1987
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
1988	[number]	k4	1988
–	–	k?	–
Televizní	televizní	k2eAgFnSc1d1	televizní
rolnička	rolnička	k1gFnSc1	rolnička
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
1989	[number]	k4	1989
duben	duben	k1gInSc1	duben
–	–	k?	–
SP	SP	kA	SP
desky	deska	k1gFnSc2	deska
10	[number]	k4	10
nej	nej	k?	nej
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
Šmoulová	Šmoulový	k2eAgFnSc1d1	Šmoulový
země	země	k1gFnSc1	země
</s>
</p>
<p>
<s>
1989	[number]	k4	1989
–	–	k?	–
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
slavík	slavík	k1gMnSc1	slavík
1988	[number]	k4	1988
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
1990	[number]	k4	1990
–	–	k?	–
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
slavík	slavík	k1gMnSc1	slavík
1989	[number]	k4	1989
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
1991	[number]	k4	1991
–	–	k?	–
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
slavík	slavík	k1gMnSc1	slavík
1990	[number]	k4	1990
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
<g/>
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
1991	[number]	k4	1991
–	–	k?	–
Telerezonance	Telerezonanec	k1gInSc2	Telerezonanec
–	–	k?	–
televizní	televizní	k2eAgFnSc1d1	televizní
písničková	písničkový	k2eAgFnSc1d1	písničková
soutež	soutež	k1gFnSc1	soutež
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
–	–	k?	–
Rozhovor	rozhovor	k1gInSc1	rozhovor
v	v	k7c6	v
tichu	ticho	k1gNnSc6	ticho
</s>
</p>
<p>
<s>
1991	[number]	k4	1991
–	–	k?	–
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
slavík	slavík	k1gMnSc1	slavík
–	–	k?	–
tabulka	tabulka	k1gFnSc1	tabulka
úspěšnosti	úspěšnost	k1gFnSc2	úspěšnost
v	v	k7c6	v
letech	let	k1gInPc6	let
1962	[number]	k4	1962
<g/>
–	–	k?	–
<g/>
1991	[number]	k4	1991
–	–	k?	–
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
K.	K.	kA	K.
Gott	Gott	k1gInSc4	Gott
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
1992	[number]	k4	1992
–	–	k?	–
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
slavík	slavík	k1gMnSc1	slavík
1991	[number]	k4	1991
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
1992	[number]	k4	1992
–	–	k?	–
TýTý	TýTý	k1gFnPc2	TýTý
1991	[number]	k4	1991
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
1992	[number]	k4	1992
–	–	k?	–
Telerezonance	Telerezonanec	k1gInSc2	Telerezonanec
–	–	k?	–
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
1993	[number]	k4	1993
–	–	k?	–
TýTý	TýTý	k1gFnPc2	TýTý
1992	[number]	k4	1992
–	–	k?	–
7	[number]	k4	7
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
1994	[number]	k4	1994
–	–	k?	–
TýTý	TýTý	k1gFnPc2	TýTý
1993	[number]	k4	1993
–	–	k?	–
7	[number]	k4	7
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
1995	[number]	k4	1995
–	–	k?	–
TýTý	TýTý	k1gFnPc2	TýTý
1994	[number]	k4	1994
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
–	–	k?	–
Super	super	k2eAgInSc1d1	super
slavík	slavík	k1gInSc1	slavík
(	(	kIx(	(
<g/>
obnovena	obnoven	k2eAgFnSc1d1	obnovena
anketa	anketa	k1gFnSc1	anketa
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
–	–	k?	–
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
–	–	k?	–
TýTý	TýTý	k1gFnPc2	TýTý
1995	[number]	k4	1995
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
1997	[number]	k4	1997
–	–	k?	–
TýTý	TýTý	k1gFnPc2	TýTý
1996	[number]	k4	1996
–	–	k?	–
7	[number]	k4	7
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
1997	[number]	k4	1997
–	–	k?	–
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
–	–	k?	–
10	[number]	k4	10
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
1997	[number]	k4	1997
únor	únor	k1gInSc1	únor
–	–	k?	–
...	...	k?	...
<g/>
než	než	k8xS	než
to	ten	k3xDgNnSc4	ten
zapomenu	zapomnět	k5eAaImIp1nS	zapomnět
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
2	[number]	k4	2
<g/>
.	.	kIx.	.
nejprodávanější	prodávaný	k2eAgFnSc7d3	nejprodávanější
knihou	kniha	k1gFnSc7	kniha
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
–	–	k?	–
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
deska	deska	k1gFnSc1	deska
za	za	k7c4	za
25	[number]	k4	25
tisíc	tisíc	k4xCgInSc4	tisíc
prodaných	prodaný	k2eAgInPc2d1	prodaný
nosičů	nosič	k1gInPc2	nosič
alba	album	k1gNnSc2	album
Maluj	malovat	k5eAaImRp2nS	malovat
zase	zase	k9	zase
obrázky	obrázek	k1gInPc4	obrázek
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
–	–	k?	–
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
deska	deska	k1gFnSc1	deska
za	za	k7c4	za
25	[number]	k4	25
tisíc	tisíc	k4xCgInSc4	tisíc
prodaných	prodaný	k2eAgInPc2d1	prodaný
nosičů	nosič	k1gInPc2	nosič
alba	album	k1gNnSc2	album
Ave	ave	k1gNnSc2	ave
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
–	–	k?	–
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
–	–	k?	–
11	[number]	k4	11
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
–	–	k?	–
TýTý	TýTý	k1gFnPc2	TýTý
1997	[number]	k4	1997
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
–	–	k?	–
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
–	–	k?	–
10	[number]	k4	10
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
–	–	k?	–
TýTý	TýTý	k1gFnPc2	TýTý
1998	[number]	k4	1998
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
–	–	k?	–
osmá	osmý	k4xOgFnSc1	osmý
nejprodávanější	prodávaný	k2eAgFnSc1d3	nejprodávanější
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
roku	rok	k1gInSc2	rok
ČR	ČR	kA	ČR
dle	dle	k7c2	dle
IFPI	IFPI	kA	IFPI
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
–	–	k?	–
TýTý	TýTý	k1gFnPc2	TýTý
1999	[number]	k4	1999
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
–	–	k?	–
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
–	–	k?	–
11	[number]	k4	11
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
–	–	k?	–
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
deska	deska	k1gFnSc1	deska
za	za	k7c4	za
15	[number]	k4	15
tisíc	tisíc	k4xCgInSc4	tisíc
prodaných	prodaný	k2eAgInPc2d1	prodaný
nosičů	nosič	k1gInPc2	nosič
alba	alba	k1gFnSc1	alba
Hanka	Hanka	k1gFnSc1	Hanka
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
–	–	k?	–
TýTý	TýTý	k1gFnPc2	TýTý
2000	[number]	k4	2000
–	–	k?	–
7	[number]	k4	7
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
–	–	k?	–
druhá	druhý	k4xOgFnSc1	druhý
nejprodávanější	prodávaný	k2eAgFnSc1d3	nejprodávanější
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
roku	rok	k1gInSc2	rok
dle	dle	k7c2	dle
IFPI	IFPI	kA	IFPI
–	–	k?	–
CD	CD	kA	CD
Hanka	Hanka	k1gFnSc1	Hanka
třetí	třetí	k4xOgFnSc2	třetí
nejprodávanější	prodávaný	k2eAgFnSc2d3	nejprodávanější
CD	CD	kA	CD
roku	rok	k1gInSc2	rok
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
–	–	k?	–
Platinová	platinový	k2eAgFnSc1d1	platinová
deska	deska	k1gFnSc1	deska
za	za	k7c4	za
55	[number]	k4	55
tisíc	tisíc	k4xCgInSc4	tisíc
prodaných	prodaný	k2eAgInPc2d1	prodaný
nosičů	nosič	k1gInPc2	nosič
alba	album	k1gNnSc2	album
Maluj	malovat	k5eAaImRp2nS	malovat
zase	zase	k9	zase
obrázky	obrázek	k1gInPc4	obrázek
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
–	–	k?	–
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
–	–	k?	–
Je	být	k5eAaImIp3nS	být
naprosto	naprosto	k6eAd1	naprosto
nezbytné	nezbytný	k2eAgFnSc2d1	nezbytná
–	–	k?	–
5	[number]	k4	5
<g/>
.	.	kIx.	.
nejúspešnější	úspešný	k2eAgFnSc1d3	úspešný
píseň	píseň	k1gFnSc1	píseň
roku	rok	k1gInSc2	rok
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
leden	leden	k1gInSc1	leden
–	–	k?	–
Je	být	k5eAaImIp3nS	být
naprosto	naprosto	k6eAd1	naprosto
nezbytné	zbytný	k2eNgNnSc1d1	nezbytné
boduje	bodovat	k5eAaImIp3nS	bodovat
9	[number]	k4	9
týdnů	týden	k1gInPc2	týden
ve	v	k7c4	v
Formule	formule	k1gFnPc4	formule
Pop	pop	k1gInSc4	pop
<g/>
,	,	kIx,	,
po	po	k7c6	po
2	[number]	k4	2
týdnu	týden	k1gInSc6	týden
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
–	–	k?	–
s	s	k7c7	s
manželem	manžel	k1gMnSc7	manžel
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
"	"	kIx"	"
<g/>
Zlatý	zlatý	k1gInSc1	zlatý
pár	pár	k4xCyI	pár
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
"	"	kIx"	"
–	–	k?	–
anketa	anketa	k1gFnSc1	anketa
časopisu	časopis	k1gInSc2	časopis
Šťastný	šťastný	k2eAgInSc1d1	šťastný
Jim	on	k3xPp3gMnPc3	on
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
–	–	k?	–
TýTý	TýTý	k1gFnPc2	TýTý
2001	[number]	k4	2001
–	–	k?	–
7	[number]	k4	7
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
červenec	červenec	k1gInSc1	červenec
–	–	k?	–
Hitparáda	hitparáda	k1gFnSc1	hitparáda
Formule	formule	k1gFnSc1	formule
Pop	pop	k1gInSc1	pop
ČRo	ČRo	k1gFnSc1	ČRo
<g/>
–	–	k?	–
Zůstaň	zůstat	k5eAaPmRp2nS	zůstat
tady	tady	k6eAd1	tady
léto	léto	k1gNnSc4	léto
10	[number]	k4	10
týdnů	týden	k1gInPc2	týden
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
–	–	k?	–
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
–	–	k?	–
7	[number]	k4	7
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
–	–	k?	–
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
a	a	k8xC	a
Dvouplatinová	dvouplatinový	k2eAgFnSc1d1	dvouplatinový
deska	deska	k1gFnSc1	deska
za	za	k7c4	za
55	[number]	k4	55
tisíc	tisíc	k4xCgInSc4	tisíc
prodaných	prodaný	k2eAgInPc2d1	prodaný
nosičů	nosič	k1gInPc2	nosič
alba	album	k1gNnSc2	album
Maluj	malovat	k5eAaImRp2nS	malovat
zase	zase	k9	zase
obrázky	obrázek	k1gInPc4	obrázek
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
–	–	k?	–
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
deska	deska	k1gFnSc1	deska
za	za	k7c4	za
14	[number]	k4	14
tisíc	tisíc	k4xCgInSc4	tisíc
prodaných	prodaný	k2eAgInPc2d1	prodaný
nosičů	nosič	k1gInPc2	nosič
alba	album	k1gNnSc2	album
Největší	veliký	k2eAgInPc4d3	veliký
hity	hit	k1gInPc4	hit
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
–	–	k?	–
TýTý	TýTý	k1gFnPc2	TýTý
2002	[number]	k4	2002
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
–	–	k?	–
Platinová	platinový	k2eAgFnSc1d1	platinová
deska	deska	k1gFnSc1	deska
za	za	k7c4	za
31	[number]	k4	31
tisíc	tisíc	k4xCgInSc4	tisíc
prodaných	prodaný	k2eAgInPc2d1	prodaný
nosičů	nosič	k1gInPc2	nosič
alba	album	k1gNnSc2	album
Ave	ave	k1gNnSc2	ave
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
–	–	k?	–
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
–	–	k?	–
čtvrtá	čtvrtá	k1gFnSc1	čtvrtá
nejprodávanější	prodávaný	k2eAgFnSc1d3	nejprodávanější
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
roku	rok	k1gInSc2	rok
dle	dle	k7c2	dle
IFPI	IFPI	kA	IFPI
s	s	k7c7	s
albem	album	k1gNnSc7	album
Největší	veliký	k2eAgFnSc2d3	veliký
hity	hit	k1gInPc7	hit
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
–	–	k?	–
Platinová	platinový	k2eAgFnSc1d1	platinová
deska	deska	k1gFnSc1	deska
za	za	k7c4	za
20	[number]	k4	20
tisíc	tisíc	k4xCgInSc4	tisíc
prodaných	prodaný	k2eAgInPc2d1	prodaný
nosičů	nosič	k1gInPc2	nosič
alba	album	k1gNnSc2	album
Největší	veliký	k2eAgInPc4d3	veliký
hity	hit	k1gInPc4	hit
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
–	–	k?	–
TýTý	TýTý	k1gFnPc2	TýTý
2003	[number]	k4	2003
–	–	k?	–
5	[number]	k4	5
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
leden	leden	k1gInSc1	leden
–	–	k?	–
Hitparáda	hitparáda	k1gFnSc1	hitparáda
Formule	formule	k1gFnSc1	formule
Pop	pop	k1gInSc1	pop
–	–	k?	–
písnička	písnička	k1gFnSc1	písnička
Navěky	navěky	k6eAd1	navěky
zůstane	zůstat	k5eAaPmIp3nS	zůstat
čas	čas	k1gInSc1	čas
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
v	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
kole	kolo	k1gNnSc6	kolo
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
pozici	pozice	k1gFnSc6	pozice
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
–	–	k?	–
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
–	–	k?	–
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
–	–	k?	–
TýTý	TýTý	k1gFnPc2	TýTý
2004	[number]	k4	2004
–	–	k?	–
5	[number]	k4	5
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
–	–	k?	–
Platinová	platinový	k2eAgFnSc1d1	platinová
deska	deska	k1gFnSc1	deska
za	za	k7c4	za
prodej	prodej	k1gInSc4	prodej
2DVD	[number]	k4	2DVD
Cesta	cesta	k1gFnSc1	cesta
ke	k	k7c3	k
štěstí	štěstí	k1gNnSc3	štěstí
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
–	–	k?	–
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
deska	deska	k1gFnSc1	deska
za	za	k7c4	za
více	hodně	k6eAd2	hodně
než	než	k8xS	než
10	[number]	k4	10
tisíc	tisíc	k4xCgInSc4	tisíc
prodaných	prodaný	k2eAgInPc2d1	prodaný
nosičů	nosič	k1gInPc2	nosič
4CD	[number]	k4	4CD
S	s	k7c7	s
úctou	úcta	k1gFnSc7	úcta
<g/>
/	/	kIx~	/
<g/>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
kolekce	kolekce	k1gFnSc1	kolekce
–	–	k?	–
Hana	Hana	k1gFnSc1	Hana
Zagorová	Zagorová	k1gFnSc1	Zagorová
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
jako	jako	k8xC	jako
jediná	jediný	k2eAgFnSc1d1	jediná
a	a	k8xC	a
první	první	k4xOgFnSc1	první
dostala	dostat	k5eAaPmAgFnS	dostat
zlatou	zlatý	k2eAgFnSc4d1	zlatá
desku	deska	k1gFnSc4	deska
za	za	k7c4	za
prodej	prodej	k1gInSc4	prodej
alba	album	k1gNnSc2	album
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
–	–	k?	–
TýTý	TýTý	k1gFnPc2	TýTý
2005	[number]	k4	2005
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
–	–	k?	–
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
–	–	k?	–
10	[number]	k4	10
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
–	–	k?	–
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
–	–	k?	–
10	[number]	k4	10
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
–	–	k?	–
TýTý	TýTý	k1gFnPc2	TýTý
2006	[number]	k4	2006
–	–	k?	–
5	[number]	k4	5
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
–	–	k?	–
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
deska	deska	k1gFnSc1	deska
za	za	k7c4	za
prodej	prodej	k1gInSc4	prodej
DVD	DVD	kA	DVD
Vzpomínání	vzpomínání	k1gNnSc1	vzpomínání
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
–	–	k?	–
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
–	–	k?	–
Hitparáda	hitparáda	k1gFnSc1	hitparáda
Česká	český	k2eAgFnSc1d1	Česká
dvanáctka	dvanáctka	k1gFnSc1	dvanáctka
–	–	k?	–
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
Česká	český	k2eAgFnSc1d1	Česká
12	[number]	k4	12
za	za	k7c4	za
píseň	píseň	k1gFnSc4	píseň
Adieu	adieu	k0	adieu
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
–	–	k?	–
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
–	–	k?	–
11	[number]	k4	11
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
–	–	k?	–
Lady	lady	k1gFnPc2	lady
PRO	pro	k7c4	pro
2009	[number]	k4	2009
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
–	–	k?	–
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
–	–	k?	–
10	[number]	k4	10
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
–	–	k?	–
Cena	cena	k1gFnSc1	cena
nočního	noční	k2eAgInSc2d1	noční
proudu	proud	k1gInSc2	proud
(	(	kIx(	(
<g/>
ČRo	ČRo	k1gFnSc1	ČRo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
–	–	k?	–
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
–	–	k?	–
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
deska	deska	k1gFnSc1	deska
za	za	k7c4	za
prodej	prodej	k1gInSc4	prodej
3000	[number]	k4	3000
nosičú	nosičú	k?	nosičú
2	[number]	k4	2
<g/>
DVD	DVD	kA	DVD
<g/>
/	/	kIx~	/
<g/>
CD	CD	kA	CD
Lucerna	lucerna	k1gFnSc1	lucerna
2010	[number]	k4	2010
(	(	kIx(	(
<g/>
live	liv	k1gInSc2	liv
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
–	–	k?	–
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
–	–	k?	–
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
do	do	k7c2	do
Síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
(	(	kIx(	(
<g/>
Předávání	předávání	k1gNnPc2	předávání
cen	cena	k1gFnPc2	cena
vítězům	vítěz	k1gMnPc3	vítěz
ankety	anketa	k1gFnSc2	anketa
popularity	popularita	k1gFnSc2	popularita
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
už	už	k9	už
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
vyhlašuje	vyhlašovat	k5eAaImIp3nS	vyhlašovat
časopis	časopis	k1gInSc4	časopis
Rytmus	rytmus	k1gInSc1	rytmus
života	život	k1gInSc2	život
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
–	–	k?	–
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
–	–	k?	–
10	[number]	k4	10
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
–	–	k?	–
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
–	–	k?	–
Diamantová	diamantový	k2eAgFnSc1d1	Diamantová
deska	deska	k1gFnSc1	deska
Supraphonu	supraphon	k1gInSc2	supraphon
za	za	k7c4	za
prodej	prodej	k1gInSc4	prodej
10	[number]	k4	10
120	[number]	k4	120
000	[number]	k4	000
nosičů	nosič	k1gInPc2	nosič
/	/	kIx~	/
nejprodávanější	prodávaný	k2eAgFnSc1d3	nejprodávanější
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
–	–	k?	–
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
deska	deska	k1gFnSc1	deska
za	za	k7c4	za
prodej	prodej	k1gInSc4	prodej
alba	album	k1gNnSc2	album
Vyznání	vyznání	k1gNnSc2	vyznání
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
–	–	k?	–
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
2015	[number]	k4	2015
–	–	k?	–
Hudební	hudební	k2eAgFnSc2d1	hudební
ceny	cena	k1gFnSc2	cena
Anděl	Anděla	k1gFnPc2	Anděla
2014	[number]	k4	2014
–	–	k?	–
Síň	síň	k1gFnSc1	síň
slávy	sláva	k1gFnSc2	sláva
</s>
</p>
<p>
<s>
2015	[number]	k4	2015
–	–	k?	–
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
2016	[number]	k4	2016
–	–	k?	–
Senior	senior	k1gMnSc1	senior
Prix	Prix	k1gInSc4	Prix
za	za	k7c4	za
celoživotní	celoživotní	k2eAgNnSc4d1	celoživotní
mistrovství	mistrovství	k1gNnSc4	mistrovství
–	–	k?	–
Nadace	nadace	k1gFnSc1	nadace
Život	život	k1gInSc1	život
umělce	umělec	k1gMnPc4	umělec
</s>
</p>
<p>
<s>
2016	[number]	k4	2016
–	–	k?	–
Čestné	čestný	k2eAgNnSc4d1	čestné
občanství	občanství	k1gNnSc4	občanství
Prahy	Praha	k1gFnSc2	Praha
4	[number]	k4	4
</s>
</p>
<p>
<s>
2016	[number]	k4	2016
–	–	k?	–
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
2016	[number]	k4	2016
–	–	k?	–
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
deska	deska	k1gFnSc1	deska
za	za	k7c4	za
prodej	prodej	k1gInSc4	prodej
alba	album	k1gNnSc2	album
O	o	k7c6	o
Lásce	láska	k1gFnSc6	láska
</s>
</p>
<p>
<s>
2017	[number]	k4	2017
–	–	k?	–
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
a	a	k8xC	a
stříbrná	stříbrný	k2eAgFnSc1d1	stříbrná
medaile	medaile	k1gFnSc1	medaile
–	–	k?	–
Česká	český	k2eAgFnSc1d1	Česká
mincovna	mincovna	k1gFnSc1	mincovna
–	–	k?	–
série	série	k1gFnSc2	série
Slavíci	Slavík	k1gMnPc1	Slavík
ve	v	k7c6	v
zlatě	zlato	k1gNnSc6	zlato
</s>
</p>
<p>
<s>
2017	[number]	k4	2017
–	–	k?	–
Radio	radio	k1gNnSc4	radio
Vlna	vlna	k1gFnSc1	vlna
–	–	k?	–
anketa	anketa	k1gFnSc1	anketa
TOP	topit	k5eAaImRp2nS	topit
1000	[number]	k4	1000
hitov	hitov	k1gInSc4	hitov
–	–	k?	–
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
Maluj	malovat	k5eAaImRp2nS	malovat
zase	zase	k9	zase
obrázky	obrázek	k1gInPc4	obrázek
</s>
</p>
<p>
<s>
2017	[number]	k4	2017
–	–	k?	–
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
–	–	k?	–
10	[number]	k4	10
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
2018	[number]	k4	2018
-	-	kIx~	-
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
deska	deska	k1gFnSc1	deska
za	za	k7c4	za
prodej	prodej	k1gInSc4	prodej
alba	album	k1gNnSc2	album
Já	já	k3xPp1nSc1	já
nemám	mít	k5eNaImIp1nS	mít
strach	strach	k1gInSc4	strach
</s>
</p>
<p>
<s>
===	===	k?	===
Zahraniční	zahraniční	k2eAgMnPc1d1	zahraniční
===	===	k?	===
</s>
</p>
<p>
<s>
1969	[number]	k4	1969
–	–	k?	–
Belgie	Belgie	k1gFnSc1	Belgie
–	–	k?	–
Cena	cena	k1gFnSc1	cena
obecenstva	obecenstvo	k1gNnSc2	obecenstvo
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Knokke	Knokke	k1gFnSc6	Knokke
</s>
</p>
<p>
<s>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
–	–	k?	–
0	[number]	k4	0
<g/>
9.03	[number]	k4	9.03
<g/>
.1970	.1970	k4	.1970
–	–	k?	–
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
–	–	k?	–
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
<g/>
)	)	kIx)	)
Účast	účast	k1gFnSc1	účast
na	na	k7c6	na
písňovém	písňový	k2eAgInSc6d1	písňový
festivalu	festival	k1gInSc6	festival
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
jelen	jelen	k1gMnSc1	jelen
Brašov	Brašov	k1gInSc1	Brašov
<g/>
/	/	kIx~	/
<g/>
Cerbul	Cerbul	k1gInSc1	Cerbul
de	de	k?	de
aur	aura	k1gFnPc2	aura
1970	[number]	k4	1970
Brasov	Brasov	k1gInSc1	Brasov
–	–	k?	–
písnička	písnička	k1gFnSc1	písnička
Bludička	bludička	k1gFnSc1	bludička
Julie	Julie	k1gFnSc1	Julie
</s>
</p>
<p>
<s>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
–	–	k?	–
22.11	[number]	k4	22.11
<g/>
.1970	.1970	k4	.1970
–	–	k?	–
Kuba	Kuba	k1gFnSc1	Kuba
–	–	k?	–
Varadero	Varadero	k1gNnSc1	Varadero
'	'	kIx"	'
<g/>
70	[number]	k4	70
Festival	festival	k1gInSc1	festival
internacional	internacionat	k5eAaPmAgInS	internacionat
de	de	k?	de
la	la	k1gNnPc1	la
cancion	cancion	k1gInSc1	cancion
popular	popular	k1gInSc4	popular
(	(	kIx(	(
<g/>
tři	tři	k4xCgNnPc4	tři
vystoupení	vystoupení	k1gNnPc4	vystoupení
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1971	[number]	k4	1971
–	–	k?	–
Belgie	Belgie	k1gFnSc1	Belgie
–	–	k?	–
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
u	u	k7c2	u
poroty	porota	k1gFnSc2	porota
na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
festivalu	festival	k1gInSc6	festival
TV	TV	kA	TV
společností	společnost	k1gFnPc2	společnost
v	v	k7c6	v
Knokke	Knokke	k1gFnSc6	Knokke
</s>
</p>
<p>
<s>
1973	[number]	k4	1973
–	–	k?	–
Kuba	Kuba	k1gFnSc1	Kuba
–	–	k?	–
Cena	cena	k1gFnSc1	cena
Světového	světový	k2eAgInSc2d1	světový
festivalu	festival	k1gInSc2	festival
</s>
</p>
<p>
<s>
1975	[number]	k4	1975
–	–	k?	–
Německo	Německo	k1gNnSc1	Německo
<g/>
/	/	kIx~	/
<g/>
Drážďany	Drážďany	k1gInPc4	Drážďany
–	–	k?	–
účast	účast	k1gFnSc1	účast
na	na	k7c6	na
písňovém	písňový	k2eAgInSc6d1	písňový
festivalu	festival	k1gInSc6	festival
–	–	k?	–
půlhodinový	půlhodinový	k2eAgInSc1d1	půlhodinový
recitál	recitál	k1gInSc1	recitál
</s>
</p>
<p>
<s>
1975	[number]	k4	1975
–	–	k?	–
Německo	Německo	k1gNnSc1	Německo
–	–	k?	–
písničkový	písničkový	k2eAgInSc1d1	písničkový
pořad	pořad	k1gInSc1	pořad
TV	TV	kA	TV
NDR	NDR	kA	NDR
"	"	kIx"	"
<g/>
TV	TV	kA	TV
Schlager	Schlager	k1gInSc1	Schlager
studio	studio	k1gNnSc1	studio
<g/>
"	"	kIx"	"
–	–	k?	–
vítězství	vítězství	k1gNnSc2	vítězství
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
Ich	Ich	k1gMnSc2	Ich
gehe	geh	k1gMnSc2	geh
nur	nur	k?	nur
in	in	k?	in
Hosen	Hosen	k1gInSc1	Hosen
+	+	kIx~	+
postup	postup	k1gInSc1	postup
do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
kola	kolo	k1gNnSc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Následne	Následnout	k5eAaPmIp3nS	Následnout
z	z	k7c2	z
něhož	jenž	k3xRgMnSc2	jenž
šla	jít	k5eAaImAgFnS	jít
do	do	k7c2	do
celoroční	celoroční	k2eAgFnSc2d1	celoroční
přehlídky	přehlídka	k1gFnSc2	přehlídka
nejúspěšnejších	úspěšný	k2eAgFnPc2d3	nejúspěšnější
skladeb	skladba	k1gFnPc2	skladba
"	"	kIx"	"
<g/>
Einmal	Einmal	k1gInSc1	Einmal
im	im	k?	im
Jahr	Jahr	k1gInSc1	Jahr
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1978	[number]	k4	1978
–	–	k?	–
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
–	–	k?	–
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
Orfeus	Orfeus	k1gMnSc1	Orfeus
</s>
</p>
<p>
<s>
1978	[number]	k4	1978
–	–	k?	–
Slovensko	Slovensko	k1gNnSc4	Slovensko
–	–	k?	–
Bratislavská	bratislavský	k2eAgFnSc1d1	Bratislavská
lýra	lýra	k1gFnSc1	lýra
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
semifinále	semifinále	k1gNnSc1	semifinále
–	–	k?	–
Pojď	jít	k5eAaImRp2nS	jít
kdo	kdo	k3yInSc1	kdo
máš	mít	k5eAaImIp2nS	mít
mě	já	k3xPp1nSc4	já
rád	rád	k6eAd1	rád
(	(	kIx(	(
<g/>
Hana	Hana	k1gFnSc1	Hana
soutěžila	soutěžit	k5eAaImAgFnS	soutěžit
+	+	kIx~	+
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
programem	program	k1gInSc7	program
a	a	k8xC	a
hostem	host	k1gMnSc7	host
P.	P.	kA	P.
Rezkem	Rezek	k1gMnSc7	Rezek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1978	[number]	k4	1978
–	–	k?	–
Polsko	Polsko	k1gNnSc1	Polsko
–	–	k?	–
Cena	cena	k1gFnSc1	cena
polského	polský	k2eAgInSc2d1	polský
rozhlasu	rozhlas	k1gInSc2	rozhlas
a	a	k8xC	a
televize	televize	k1gFnSc1	televize
<g/>
,	,	kIx,	,
Intertalent	Intertalent	k1gInSc1	Intertalent
78	[number]	k4	78
<g/>
,	,	kIx,	,
Festival	festival	k1gInSc1	festival
Sopoty	sopot	k1gInPc5	sopot
78	[number]	k4	78
</s>
</p>
<p>
<s>
1983	[number]	k4	1983
–	–	k?	–
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
–	–	k?	–
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
Orfeus	Orfeus	k1gMnSc1	Orfeus
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
–	–	k?	–
Polsko	Polsko	k1gNnSc1	Polsko
–	–	k?	–
Host	host	k1gMnSc1	host
hudebního	hudební	k2eAgInSc2d1	hudební
festivalu	festival	k1gInSc2	festival
(	(	kIx(	(
<g/>
triumfální	triumfální	k2eAgInSc1d1	triumfální
úspěch	úspěch	k1gInSc1	úspěch
<g/>
,	,	kIx,	,
program	program	k1gInSc1	program
musel	muset	k5eAaImAgInS	muset
být	být	k5eAaImF	být
přerušen	přerušit	k5eAaPmNgInS	přerušit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
publikum	publikum	k1gNnSc1	publikum
uklidnilo	uklidnit	k5eAaPmAgNnS	uklidnit
po	po	k7c6	po
dvacetiminutovém	dvacetiminutový	k2eAgNnSc6d1	dvacetiminutové
vystoupení	vystoupení	k1gNnSc6	vystoupení
Hanky	Hanka	k1gFnSc2	Hanka
<g/>
;	;	kIx,	;
festivalu	festival	k1gInSc2	festival
se	se	k3xPyFc4	se
např.	např.	kA	např.
účastnila	účastnit	k5eAaImAgFnS	účastnit
i	i	k9	i
Tina	Tina	k1gFnSc1	Tina
Turner	turner	k1gMnSc1	turner
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
–	–	k?	–
Polsko	Polsko	k1gNnSc1	Polsko
–	–	k?	–
Katowice	Katowic	k1gMnSc2	Katowic
</s>
</p>
<p>
<s>
1988	[number]	k4	1988
–	–	k?	–
Polsko	Polsko	k1gNnSc1	Polsko
–	–	k?	–
Cena	cena	k1gFnSc1	cena
Krysztalowy	Krysztalowa	k1gFnSc2	Krysztalowa
Kamerton	Kamerton	k1gInSc1	Kamerton
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
–	–	k?	–
Slovensko	Slovensko	k1gNnSc4	Slovensko
–	–	k?	–
Legendy	legenda	k1gFnPc4	legenda
popu	pop	k1gInSc2	pop
</s>
</p>
<p>
<s>
==	==	k?	==
Film	film	k1gInSc1	film
==	==	k?	==
</s>
</p>
<p>
<s>
Josefína	Josefína	k1gFnSc1	Josefína
</s>
</p>
<p>
<s>
Trhák	trhák	k1gInSc1	trhák
</s>
</p>
<p>
<s>
Loupežnická	loupežnický	k2eAgFnSc1d1	loupežnická
legenda	legenda	k1gFnSc1	legenda
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Pan	Pan	k1gMnSc1	Pan
–	–	k?	–
česko-německý	českoěmecký	k2eAgInSc1d1	česko-německý
filmový	filmový	k2eAgInSc1d1	filmový
muzikál	muzikál	k1gInSc1	muzikál
</s>
</p>
<p>
<s>
Hrubeš	Hrubeš	k1gMnSc1	Hrubeš
a	a	k8xC	a
Mareš	Mareš	k1gMnSc1	Mareš
jsou	být	k5eAaImIp3nP	být
kamarádi	kamarád	k1gMnPc1	kamarád
do	do	k7c2	do
deště	dešť	k1gInSc2	dešť
</s>
</p>
<p>
<s>
==	==	k?	==
Televize	televize	k1gFnSc1	televize
==	==	k?	==
</s>
</p>
<p>
<s>
Písničky	písnička	k1gFnPc1	písnička
pro	pro	k7c4	pro
Hanku	Hanka	k1gFnSc4	Hanka
–	–	k?	–
1968	[number]	k4	1968
a	a	k8xC	a
1969	[number]	k4	1969
</s>
</p>
<p>
<s>
Zpívá	zpívat	k5eAaImIp3nS	zpívat
Hana	Hana	k1gFnSc1	Hana
Zagorová	Zagorová	k1gFnSc1	Zagorová
–	–	k?	–
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
a	a	k8xC	a
1978	[number]	k4	1978
</s>
</p>
<p>
<s>
Zaváté	zavátý	k2eAgFnSc2d1	zavátá
studánky	studánka	k1gFnSc2	studánka
–	–	k?	–
1976	[number]	k4	1976
</s>
</p>
<p>
<s>
Báječné	báječný	k2eAgFnPc1d1	báječná
ženy	žena	k1gFnPc1	žena
na	na	k7c6	na
létajících	létající	k2eAgInPc6d1	létající
strojích	stroj	k1gInPc6	stroj
–	–	k?	–
1978	[number]	k4	1978
–	–	k?	–
Televizní	televizní	k2eAgInSc4d1	televizní
muzikál	muzikál	k1gInSc4	muzikál
</s>
</p>
<p>
<s>
Prázdniny	prázdniny	k1gFnPc4	prázdniny
–	–	k?	–
1978	[number]	k4	1978
</s>
</p>
<p>
<s>
Zájezd	zájezd	k1gInSc1	zájezd
–	–	k?	–
1979	[number]	k4	1979
–	–	k?	–
televizní	televizní	k2eAgInSc4d1	televizní
recitál	recitál	k1gInSc4	recitál
Hany	Hana	k1gFnSc2	Hana
Zagorové	Zagorové	k2eAgMnSc2d1	Zagorové
a	a	k8xC	a
italského	italský	k2eAgMnSc2d1	italský
zpěváka	zpěvák	k1gMnSc2	zpěvák
Drupiho	Drupi	k1gMnSc2	Drupi
</s>
</p>
<p>
<s>
Dluhy	dluh	k1gInPc1	dluh
Hany	Hana	k1gFnSc2	Hana
Zagorové	Zagorová	k1gFnSc2	Zagorová
–	–	k?	–
osmidílná	osmidílný	k2eAgFnSc1d1	osmidílná
série	série	k1gFnSc1	série
<g/>
,	,	kIx,	,
skládající	skládající	k2eAgInPc4d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
hudebních	hudební	k2eAgNnPc2d1	hudební
vystoupení	vystoupení	k1gNnPc2	vystoupení
Hany	Hana	k1gFnSc2	Hana
a	a	k8xC	a
jejích	její	k3xOp3gMnPc2	její
hostů	host	k1gMnPc2	host
nebo	nebo	k8xC	nebo
spolupracovníků	spolupracovník	k1gMnPc2	spolupracovník
(	(	kIx(	(
<g/>
natáčená	natáčený	k2eAgFnSc1d1	natáčená
v	v	k7c6	v
létech	léto	k1gNnPc6	léto
1980	[number]	k4	1980
<g/>
–	–	k?	–
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Výtah	výtah	k1gInSc1	výtah
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
Sen	sena	k1gFnPc2	sena
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
–	–	k?	–
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Když	když	k8xS	když
nemůžu	nemůžu	k?	nemůžu
spát	spát	k5eAaImF	spát
–	–	k?	–
noční	noční	k2eAgInSc4d1	noční
pořad	pořad	k1gInSc4	pořad
s	s	k7c7	s
odbornými	odborný	k2eAgMnPc7d1	odborný
hosty	host	k1gMnPc7	host
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
a	a	k8xC	a
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hogo	Hogo	k1gMnSc1	Hogo
Fogo	Fogo	k1gMnSc1	Fogo
–	–	k?	–
zábavný	zábavný	k2eAgInSc4d1	zábavný
pořad	pořad	k1gInSc4	pořad
na	na	k7c6	na
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
Hana	Hana	k1gFnSc1	Hana
Zagorová	Zagorová	k1gFnSc1	Zagorová
uváděla	uvádět	k5eAaImAgFnS	uvádět
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
manželem	manžel	k1gMnSc7	manžel
Štefanem	Štefan	k1gMnSc7	Štefan
Margitou	Margita	k1gMnSc7	Margita
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kam	kam	k6eAd1	kam
zmizel	zmizet	k5eAaPmAgInS	zmizet
ten	ten	k3xDgInSc1	ten
starý	starý	k2eAgInSc1d1	starý
song	song	k1gInSc1	song
-	-	kIx~	-
Hana	Hana	k1gFnSc1	Hana
Zagorová	Zagorová	k1gFnSc1	Zagorová
–	–	k?	–
2005	[number]	k4	2005
</s>
</p>
<p>
<s>
13	[number]	k4	13
<g/>
.	.	kIx.	.
<g/>
komnata	komnata	k1gFnSc1	komnata
Hany	Hana	k1gFnSc2	Hana
Zagorové	Zagorová	k1gFnSc2	Zagorová
–	–	k?	–
2006	[number]	k4	2006
</s>
</p>
<p>
<s>
Cesta	cesta	k1gFnSc1	cesta
ke	k	k7c3	k
štěstí	štěstí	k1gNnSc3	štěstí
–	–	k?	–
narozeninový	narozeninový	k2eAgInSc1d1	narozeninový
medailon	medailon	k1gInSc1	medailon
–	–	k?	–
2006	[number]	k4	2006
</s>
</p>
<p>
<s>
Top	topit	k5eAaImRp2nS	topit
Star	Star	kA	Star
-	-	kIx~	-
Skutečné	skutečný	k2eAgInPc1d1	skutečný
příběhy	příběh	k1gInPc1	příběh
hvězd	hvězda	k1gFnPc2	hvězda
–	–	k?	–
dokumentární	dokumentární	k2eAgInSc1d1	dokumentární
cyklus	cyklus	k1gInSc1	cyklus
věnovaný	věnovaný	k2eAgInSc1d1	věnovaný
Haně	Hana	k1gFnSc3	Hana
Zagorové	Zagorová	k1gFnSc2	Zagorová
–	–	k?	–
2010	[number]	k4	2010
</s>
</p>
<p>
<s>
Zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
tvého	tvůj	k3xOp2gInSc2	tvůj
života	život	k1gInSc2	život
-	-	kIx~	-
Hana	Hana	k1gFnSc1	Hana
Zagorová	Zagorová	k1gFnSc1	Zagorová
–	–	k?	–
2012	[number]	k4	2012
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Řadová	řadový	k2eAgNnPc4d1	řadové
alba	album	k1gNnPc4	album
===	===	k?	===
</s>
</p>
<p>
<s>
1971	[number]	k4	1971
Bludička	bludička	k1gFnSc1	bludička
–	–	k?	–
Supraphon	supraphon	k1gInSc1	supraphon
</s>
</p>
<p>
<s>
1976	[number]	k4	1976
Cesta	cesta	k1gFnSc1	cesta
ke	k	k7c3	k
štěstí	štěstí	k1gNnSc3	štěstí
–	–	k?	–
Supraphon	supraphon	k1gInSc1	supraphon
</s>
</p>
<p>
<s>
1979	[number]	k4	1979
Breviary	Breviar	k1gInPc1	Breviar
Of	Of	k1gMnSc1	Of
Love	lov	k1gInSc5	lov
–	–	k?	–
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
/	/	kIx~	/
<g/>
Artia	Artia	k1gFnSc1	Artia
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Určeno	určit	k5eAaPmNgNnS	určit
pro	pro	k7c4	pro
export	export	k1gInSc4	export
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1979	[number]	k4	1979
...	...	k?	...
<g/>
tobě	ty	k3xPp2nSc3	ty
<g/>
,	,	kIx,	,
tebe	ty	k3xPp2nSc4	ty
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
–	–	k?	–
Supraphon	supraphon	k1gInSc1	supraphon
</s>
</p>
<p>
<s>
1980	[number]	k4	1980
Oheň	oheň	k1gInSc1	oheň
v	v	k7c6	v
duši	duše	k1gFnSc6	duše
mé	můj	k3xOp1gNnSc1	můj
–	–	k?	–
Supraphon	supraphon	k1gInSc1	supraphon
</s>
</p>
<p>
<s>
1981	[number]	k4	1981
Střípky	střípek	k1gInPc7	střípek
–	–	k?	–
Supraphon	supraphon	k1gInSc1	supraphon
</s>
</p>
<p>
<s>
1982	[number]	k4	1982
Světlo	světlo	k1gNnSc1	světlo
a	a	k8xC	a
stín	stín	k1gInSc1	stín
–	–	k?	–
Supraphon	supraphon	k1gInSc1	supraphon
</s>
</p>
<p>
<s>
1983	[number]	k4	1983
Mimořádná	mimořádný	k2eAgFnSc1d1	mimořádná
linka	linka	k1gFnSc1	linka
–	–	k?	–
Supraphon	supraphon	k1gInSc1	supraphon
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
Co	co	k8xS	co
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
–	–	k?	–
Supraphon	supraphon	k1gInSc1	supraphon
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
Sítě	síť	k1gFnSc2	síť
kroků	krok	k1gInPc2	krok
Tvých	tvůj	k3xOp2gFnPc2	tvůj
–	–	k?	–
Supraphon	supraphon	k1gInSc1	supraphon
</s>
</p>
<p>
<s>
1986	[number]	k4	1986
Náhlá	náhlý	k2eAgNnPc4d1	náhlé
loučení	loučení	k1gNnSc4	loučení
–	–	k?	–
Supraphon	supraphon	k1gInSc1	supraphon
</s>
</p>
<p>
<s>
1988	[number]	k4	1988
Živá	živý	k2eAgFnSc1d1	živá
voda	voda	k1gFnSc1	voda
–	–	k?	–
Supraphon	supraphon	k1gInSc1	supraphon
</s>
</p>
<p>
<s>
1990	[number]	k4	1990
Dnes	dnes	k6eAd1	dnes
nejsem	být	k5eNaImIp1nS	být
doma	doma	k6eAd1	doma
–	–	k?	–
Supraphon	supraphon	k1gInSc4	supraphon
</s>
</p>
<p>
<s>
1991	[number]	k4	1991
Rozhovor	rozhovor	k1gInSc1	rozhovor
v	v	k7c6	v
tichu	ticho	k1gNnSc6	ticho
–	–	k?	–
Multisonic	Multisonice	k1gFnPc2	Multisonice
</s>
</p>
<p>
<s>
1994	[number]	k4	1994
Když	když	k8xS	když
nemůžu	nemůžu	k?	nemůžu
spát	spát	k5eAaImF	spát
–	–	k?	–
Tommü	Tommü	k1gFnSc4	Tommü
records	records	k6eAd1	records
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
Já	já	k3xPp1nSc1	já
<g/>
?	?	kIx.	?
</s>
<s>
–	–	k?	–
Multisonic	Multisonice	k1gFnPc2	Multisonice
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
Hanka	Hanka	k1gFnSc1	Hanka
–	–	k?	–
Multisonic	Multisonice	k1gFnPc2	Multisonice
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
Navěky	navěky	k6eAd1	navěky
zůstane	zůstat	k5eAaPmIp3nS	zůstat
čas	čas	k1gInSc4	čas
–	–	k?	–
Multisonic	Multisonice	k1gFnPc2	Multisonice
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
Zloděj	zloděj	k1gMnSc1	zloděj
duší	duše	k1gFnPc2	duše
–	–	k?	–
Multisonic	Multisonice	k1gFnPc2	Multisonice
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
Vyznání	vyznání	k1gNnSc4	vyznání
–	–	k?	–
Supraphon	supraphon	k1gInSc1	supraphon
</s>
</p>
<p>
<s>
2016	[number]	k4	2016
O	o	k7c6	o
Lásce	láska	k1gFnSc6	láska
–	–	k?	–
Supraphon	supraphon	k1gInSc1	supraphon
</s>
</p>
<p>
<s>
2018	[number]	k4	2018
Já	já	k3xPp1nSc1	já
nemám	mít	k5eNaImIp1nS	mít
strach	strach	k1gInSc4	strach
–	–	k?	–
Supraphon	supraphon	k1gInSc4	supraphon
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
Jinak	jinak	k6eAd1	jinak
to	ten	k3xDgNnSc1	ten
nejde	jít	k5eNaImIp3nS	jít
–	–	k?	–
Hana	Hana	k1gFnSc1	Hana
Zagorová	Zagorová	k1gFnSc1	Zagorová
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Kotvald	Kotvald	k1gMnSc1	Kotvald
a	a	k8xC	a
Stanislav	Stanislav	k1gMnSc1	Stanislav
Hložek	Hložek	k1gMnSc1	Hložek
–	–	k?	–
Supraphon	supraphon	k1gInSc1	supraphon
</s>
</p>
<p>
<s>
1987	[number]	k4	1987
Dluhy	dluh	k1gInPc4	dluh
Hany	Hana	k1gFnSc2	Hana
Zagorové	Zagorová	k1gFnSc3	Zagorová
–	–	k?	–
Hana	Hana	k1gFnSc1	Hana
Zagorová	Zagorová	k1gFnSc1	Zagorová
a	a	k8xC	a
hosté	host	k1gMnPc1	host
–	–	k?	–
Supraphon	supraphon	k1gInSc1	supraphon
</s>
</p>
<p>
<s>
1994	[number]	k4	1994
Ave	ave	k1gNnSc4	ave
–	–	k?	–
Hana	Hana	k1gFnSc1	Hana
Zagorová	Zagorová	k1gFnSc1	Zagorová
a	a	k8xC	a
Štefan	Štefan	k1gMnSc1	Štefan
Margita	Margitum	k1gNnSc2	Margitum
–	–	k?	–
Multisonic	Multisonice	k1gFnPc2	Multisonice
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
Ave	ave	k1gNnSc1	ave
2	[number]	k4	2
–	–	k?	–
Hana	Hana	k1gFnSc1	Hana
Zagorová	Zagorová	k1gFnSc1	Zagorová
a	a	k8xC	a
Štefan	Štefan	k1gMnSc1	Štefan
Margita	Margitum	k1gNnSc2	Margitum
–	–	k?	–
Multisonic	Multisonice	k1gFnPc2	Multisonice
</s>
</p>
<p>
<s>
===	===	k?	===
Výběry	výběr	k1gInPc4	výběr
===	===	k?	===
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
Lávky	lávka	k1gFnPc1	lávka
(	(	kIx(	(
<g/>
2	[number]	k4	2
LP	LP	kA	LP
<g/>
)	)	kIx)	)
–	–	k?	–
Supraphon	supraphon	k1gInSc1	supraphon
</s>
</p>
<p>
<s>
1995	[number]	k4	1995
Gvendolína	Gvendolína	k1gFnSc1	Gvendolína
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
–	–	k?	–
<g/>
71	[number]	k4	71
<g/>
)	)	kIx)	)
Bonton	bonton	k1gInSc1	bonton
</s>
</p>
<p>
<s>
1995	[number]	k4	1995
Maluj	malovat	k5eAaImRp2nS	malovat
zase	zase	k9	zase
obrázky	obrázek	k1gInPc1	obrázek
/	/	kIx~	/
To	ten	k3xDgNnSc1	ten
nej	nej	k?	nej
<g/>
...	...	k?	...
(	(	kIx(	(
<g/>
kompilace	kompilace	k1gFnSc1	kompilace
nově	nově	k6eAd1	nově
nazpívaných	nazpívaný	k2eAgInPc2d1	nazpívaný
hitů	hit	k1gInPc2	hit
<g/>
)	)	kIx)	)
–	–	k?	–
Multisonic	Multisonice	k1gFnPc2	Multisonice
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
Maluj	malovat	k5eAaImRp2nS	malovat
zase	zase	k9	zase
obrázky	obrázek	k1gInPc4	obrázek
2	[number]	k4	2
(	(	kIx(	(
<g/>
kompilace	kompilace	k1gFnPc4	kompilace
nově	nově	k6eAd1	nově
nazpívaných	nazpívaný	k2eAgInPc2d1	nazpívaný
hitů	hit	k1gInPc2	hit
<g/>
)	)	kIx)	)
–	–	k?	–
Multisonic	Multisonice	k1gFnPc2	Multisonice
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
Jinak	jinak	k6eAd1	jinak
to	ten	k3xDgNnSc1	ten
nejde	jít	k5eNaImIp3nS	jít
Bonton	bonton	k1gInSc1	bonton
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Duety	duet	k1gInPc1	duet
s	s	k7c7	s
Petrem	Petr	k1gMnSc7	Petr
Kotvaldem	Kotvald	k1gMnSc7	Kotvald
a	a	k8xC	a
Stanislavem	Stanislav	k1gMnSc7	Stanislav
Hložkem	Hložek	k1gMnSc7	Hložek
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
totožné	totožný	k2eAgNnSc1d1	totožné
se	s	k7c7	s
stejnojmenným	stejnojmenný	k2eAgNnSc7d1	stejnojmenné
LP	LP	kA	LP
z	z	k7c2	z
r.	r.	kA	r.
1985	[number]	k4	1985
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
Modrá	modrý	k2eAgFnSc1d1	modrá
Čajovna	čajovna	k1gFnSc1	čajovna
–	–	k?	–
Multisonic	Multisonice	k1gFnPc2	Multisonice
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Výběr	výběr	k1gInSc1	výběr
z	z	k7c2	z
písniček	písnička	k1gFnPc2	písnička
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
pro	pro	k7c4	pro
Hanu	Hana	k1gFnSc4	Hana
Zagorovou	Zagorová	k1gFnSc4	Zagorová
napsal	napsat	k5eAaPmAgMnS	napsat
Karel	Karel	k1gMnSc1	Karel
Vágner	Vágner	k1gMnSc1	Vágner
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
Hana	Hana	k1gFnSc1	Hana
Zagorová	Zagorová	k1gFnSc1	Zagorová
–	–	k?	–
Největší	veliký	k2eAgInPc4d3	veliký
hity	hit	k1gInPc4	hit
–	–	k?	–
(	(	kIx(	(
<g/>
2	[number]	k4	2
CD	CD	kA	CD
<g/>
)	)	kIx)	)
–	–	k?	–
Sony	Sony	kA	Sony
Music	Music	k1gMnSc1	Music
Bonton	bonton	k1gInSc1	bonton
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
S	s	k7c7	s
úctou	úcta	k1gFnSc7	úcta
–	–	k?	–
(	(	kIx(	(
<g/>
4	[number]	k4	4
CD	CD	kA	CD
<g/>
)	)	kIx)	)
–	–	k?	–
Supraphon	supraphon	k1gInSc1	supraphon
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
Perly	perla	k1gFnPc1	perla
(	(	kIx(	(
<g/>
2	[number]	k4	2
CD	CD	kA	CD
<g/>
)	)	kIx)	)
–	–	k?	–
Multisonic	Multisonice	k1gFnPc2	Multisonice
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
Můj	můj	k1gMnSc1	můj
čas	čas	k1gInSc1	čas
–	–	k?	–
Multisonic	Multisonice	k1gFnPc2	Multisonice
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Výběr	výběr	k1gInSc1	výběr
z	z	k7c2	z
duetů	duet	k1gInPc2	duet
s	s	k7c7	s
Petrem	Petr	k1gMnSc7	Petr
Kotvaldem	Kotvald	k1gMnSc7	Kotvald
a	a	k8xC	a
Stanislavem	Stanislav	k1gMnSc7	Stanislav
Hložkem	Hložek	k1gMnSc7	Hložek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
Dávné	dávný	k2eAgFnPc4d1	dávná
lásky	láska	k1gFnPc4	láska
–	–	k?	–
Supraphon	supraphon	k1gInSc1	supraphon
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Výběr	výběr	k1gInSc1	výběr
duetů	duet	k1gInPc2	duet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
Černý	Černý	k1gMnSc1	Černý
páv	páv	k1gMnSc1	páv
–	–	k?	–
Supraphon	supraphon	k1gInSc1	supraphon
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Výběr	výběr	k1gInSc1	výběr
písní	píseň	k1gFnPc2	píseň
s	s	k7c7	s
texty	text	k1gInPc7	text
Hany	Hana	k1gFnSc2	Hana
Zagorové	Zagorová	k1gFnSc2	Zagorová
<g/>
,	,	kIx,	,
album	album	k1gNnSc4	album
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
dvě	dva	k4xCgFnPc1	dva
novinky	novinka	k1gFnPc1	novinka
<g/>
:	:	kIx,	:
Z	z	k7c2	z
nebe	nebe	k1gNnSc2	nebe
pláče	plakat	k5eAaImIp3nS	plakat
déšť	déšť	k1gInSc1	déšť
/	/	kIx~	/
Bodegita	Bodegita	k1gFnSc1	Bodegita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
Duety	duet	k1gInPc4	duet
se	s	k7c7	s
slavnými	slavný	k2eAgMnPc7d1	slavný
muži	muž	k1gMnPc7	muž
–	–	k?	–
Multisonic	Multisonice	k1gFnPc2	Multisonice
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Výběr	výběr	k1gInSc1	výběr
duetů	duet	k1gInPc2	duet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
Zítra	zítra	k6eAd1	zítra
se	se	k3xPyFc4	se
zvedne	zvednout	k5eAaPmIp3nS	zvednout
vítr	vítr	k1gInSc1	vítr
–	–	k?	–
(	(	kIx(	(
<g/>
3	[number]	k4	3
CD	CD	kA	CD
+	+	kIx~	+
dvě	dva	k4xCgFnPc1	dva
novinkové	novinkový	k2eAgFnPc1d1	novinková
písně	píseň	k1gFnPc1	píseň
<g/>
)	)	kIx)	)
–	–	k?	–
Supraphon	supraphon	k1gInSc1	supraphon
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
Spolu	spolu	k6eAd1	spolu
–	–	k?	–
Hana	Hana	k1gFnSc1	Hana
Zagorová	Zagorová	k1gFnSc1	Zagorová
a	a	k8xC	a
Petr	Petr	k1gMnSc1	Petr
Rezek	Rezek	k1gMnSc1	Rezek
<g/>
,	,	kIx,	,
hity	hit	k1gInPc1	hit
–	–	k?	–
Multisonic	Multisonice	k1gFnPc2	Multisonice
</s>
</p>
<p>
<s>
===	===	k?	===
DVD	DVD	kA	DVD
===	===	k?	===
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
Hej	hej	k6eAd1	hej
<g/>
,	,	kIx,	,
mistře	mistr	k1gMnSc5	mistr
basů	bas	k1gInPc2	bas
-	-	kIx~	-
Lucerna	lucerna	k1gFnSc1	lucerna
1999	[number]	k4	1999
–	–	k?	–
Multisonic	Multisonice	k1gFnPc2	Multisonice
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
Cesta	cesta	k1gFnSc1	cesta
ke	k	k7c3	k
štěstí	štěstí	k1gNnSc3	štěstí
–	–	k?	–
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
,	,	kIx,	,
2DVD	[number]	k4	2DVD
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
Vzpomínaní	vzpomínaný	k2eAgMnPc5d1	vzpomínaný
–	–	k?	–
Supraphon	supraphon	k1gInSc1	supraphon
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
Lucerna	lucerna	k1gFnSc1	lucerna
2010	[number]	k4	2010
–	–	k?	–
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
<g/>
,	,	kIx,	,
2DVD	[number]	k4	2DVD
+	+	kIx~	+
1CD	[number]	k4	1CD
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
Hogo	Hogo	k1gMnSc1	Hogo
fogo	fogo	k1gMnSc1	fogo
<g/>
/	/	kIx~	/
<g/>
nachytávky	nachytávka	k1gFnPc1	nachytávka
a	a	k8xC	a
duety	duet	k1gInPc1	duet
–	–	k?	–
EMI	EMI	kA	EMI
</s>
</p>
<p>
<s>
2016	[number]	k4	2016
Hana	Hana	k1gFnSc1	Hana
Zagorová	Zagorová	k1gFnSc1	Zagorová
70	[number]	k4	70
–	–	k?	–
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
<g/>
,	,	kIx,	,
DVD	DVD	kA	DVD
+	+	kIx~	+
CD	CD	kA	CD
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Janoušek	Janoušek	k1gMnSc1	Janoušek
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Křesťan	Křesťan	k1gMnSc1	Křesťan
<g/>
:	:	kIx,	:
Hvězdy	hvězda	k1gFnPc1	hvězda
z	z	k7c2	z
masa	maso	k1gNnSc2	maso
a	a	k8xC	a
kostí	kost	k1gFnPc2	kost
<g/>
,	,	kIx,	,
Práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1979	[number]	k4	1979
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
Formáčková	Formáčková	k1gFnSc1	Formáčková
<g/>
:	:	kIx,	:
Dopisy	dopis	k1gInPc1	dopis
Heleně	Helena	k1gFnSc3	Helena
<g/>
,	,	kIx,	,
aneb	aneb	k?	aneb
Adresát	adresát	k1gMnSc1	adresát
<g/>
:	:	kIx,	:
Helena	Helena	k1gFnSc1	Helena
Růžičková	Růžičková	k1gFnSc1	Růžičková
<g/>
,	,	kIx,	,
Formát	formát	k1gInSc1	formát
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-86718-26-3	[number]	k4	80-86718-26-3
</s>
</p>
<p>
<s>
Kdo	kdo	k3yInSc1	kdo
je	být	k5eAaImIp3nS	být
kdo	kdo	k3yRnSc1	kdo
:	:	kIx,	:
91	[number]	k4	91
<g/>
/	/	kIx~	/
<g/>
92	[number]	k4	92
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
federální	federální	k2eAgInPc4d1	federální
orgány	orgán	k1gInPc4	orgán
ČSFR	ČSFR	kA	ČSFR
<g/>
.	.	kIx.	.
</s>
<s>
Díl	díl	k1gInSc1	díl
2	[number]	k4	2
<g/>
,	,	kIx,	,
N	N	kA	N
<g/>
–	–	k?	–
<g/>
Ž.	Ž.	kA	Ž.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Kdo	kdo	k3yQnSc1	kdo
je	být	k5eAaImIp3nS	být
kdo	kdo	k3yQnSc1	kdo
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
637	[number]	k4	637
<g/>
–	–	k?	–
<g/>
1298	[number]	k4	1298
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
901103	[number]	k4	901103
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
1115	[number]	k4	1115
<g/>
–	–	k?	–
<g/>
1116	[number]	k4	1116
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kdo	kdo	k3yInSc1	kdo
je	být	k5eAaImIp3nS	být
kdo	kdo	k3yInSc1	kdo
=	=	kIx~	=
Who	Who	k1gMnPc1	Who
is	is	k?	is
who	who	k?	who
:	:	kIx,	:
osobnosti	osobnost	k1gFnPc4	osobnost
české	český	k2eAgFnSc2d1	Česká
současnosti	současnost	k1gFnSc2	současnost
:	:	kIx,	:
5000	[number]	k4	5000
životopisů	životopis	k1gInPc2	životopis
/	/	kIx~	/
(	(	kIx(	(
<g/>
Michael	Michael	k1gMnSc1	Michael
Třeštík	Třeštík	k1gMnSc1	Třeštík
editor	editor	k1gMnSc1	editor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Agentura	agentura	k1gFnSc1	agentura
Kdo	kdo	k3yInSc1	kdo
je	být	k5eAaImIp3nS	být
kdo	kdo	k3yRnSc1	kdo
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
775	[number]	k4	775
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902586	[number]	k4	902586
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
751	[number]	k4	751
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Osobnosti	osobnost	k1gFnPc1	osobnost
-	-	kIx~	-
Česko	Česko	k1gNnSc1	Česko
:	:	kIx,	:
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ottovo	Ottův	k2eAgNnSc1d1	Ottovo
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
823	[number]	k4	823
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7360	[number]	k4	7360
<g/>
-	-	kIx~	-
<g/>
796	[number]	k4	796
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
802	[number]	k4	802
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TOMEŠ	Tomeš	k1gMnSc1	Tomeš
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
XX	XX	kA	XX
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
:	:	kIx,	:
III	III	kA	III
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
:	:	kIx,	:
Q	Q	kA	Q
<g/>
–	–	k?	–
<g/>
Ž.	Ž.	kA	Ž.
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
;	;	kIx,	;
Petr	Petr	k1gMnSc1	Petr
Meissner	Meissner	k1gMnSc1	Meissner
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
587	[number]	k4	587
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
247	[number]	k4	247
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
537	[number]	k4	537
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Zagorová	Zagorový	k2eAgFnSc5d1	Zagorová
<g/>
,	,	kIx,	,
Michaela	Michael	k1gMnSc4	Michael
Zindelová	Zindelový	k2eAgNnPc4d1	Zindelový
<g/>
:	:	kIx,	:
Než	než	k8xS	než
to	ten	k3xDgNnSc4	ten
zapomenu	zapomnět	k5eAaImIp1nS	zapomnět
<g/>
...	...	k?	...
<g/>
,	,	kIx,	,
Cesty	cesta	k1gFnPc4	cesta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7181-150-5	[number]	k4	80-7181-150-5
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Zagorová	Zagorový	k2eAgFnSc5d1	Zagorová
<g/>
,	,	kIx,	,
Michaela	Michael	k1gMnSc4	Michael
Zindelová	Zindelový	k2eAgFnSc5d1	Zindelový
<g/>
:	:	kIx,	:
Hana	Hana	k1gFnSc1	Hana
Zagorová	Zagorový	k2eAgFnSc5d1	Zagorová
<g/>
...	...	k?	...
Dřív	dříve	k6eAd2	dříve
než	než	k8xS	než
to	ten	k3xDgNnSc4	ten
zapomenu	zapomnět	k5eAaImIp1nS	zapomnět
<g/>
,	,	kIx,	,
Ikar	Ikara	k1gFnPc2	Ikara
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-249-0441-1	[number]	k4	80-249-0441-1
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Zagorová	Zagorová	k1gFnSc1	Zagorová
<g/>
:	:	kIx,	:
Milostně	milostně	k6eAd1	milostně
<g/>
,	,	kIx,	,
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-242-1696-5	[number]	k4	80-242-1696-5
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Zagorová	Zagorová	k1gFnSc1	Zagorová
a	a	k8xC	a
Scarlett	Scarlett	k1gInSc1	Scarlett
Wilková	Wilková	k1gFnSc1	Wilková
–	–	k?	–
Málokdo	málokdo	k3yInSc1	málokdo
ví	vědět	k5eAaImIp3nS	vědět
2010	[number]	k4	2010
(	(	kIx(	(
<g/>
příloha	příloha	k1gFnSc1	příloha
DVD	DVD	kA	DVD
a	a	k8xC	a
CD	CD	kA	CD
<g/>
)	)	kIx)	)
/	/	kIx~	/
Česká	český	k2eAgFnSc1d1	Česká
Televize	televize	k1gFnSc1	televize
a	a	k8xC	a
SUPRAPHON	supraphon	k1gInSc1	supraphon
/	/	kIx~	/
</s>
</p>
<p>
<s>
Mirosla	Mirosnout	k5eAaPmAgFnS	Mirosnout
Graclík	Graclík	k1gInSc4	Graclík
a	a	k8xC	a
Václav	Václav	k1gMnSc1	Václav
Nekvapil	Nekvapil	k1gMnSc1	Nekvapil
<g/>
:	:	kIx,	:
Hana	Hana	k1gFnSc1	Hana
Zagorová	Zagorová	k1gFnSc1	Zagorová
<g/>
...	...	k?	...
<g/>
Zagorka	Zagorka	k1gFnSc1	Zagorka
<g/>
,	,	kIx,	,
MV	MV	kA	MV
knihy	kniha	k1gFnPc1	kniha
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-87003-49-7	[number]	k4	978-80-87003-49-7
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Hana	Hana	k1gFnSc1	Hana
Zagorová	Zagorový	k2eAgFnSc1d1	Zagorová
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Neoficiální	oficiální	k2eNgFnPc1d1	neoficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
13	[number]	k4	13
<g/>
.	.	kIx.	.
komnata	komnata	k1gFnSc1	komnata
Hany	Hana	k1gFnSc2	Hana
Zagorové	Zagorová	k1gFnSc2	Zagorová
–	–	k?	–
dokument	dokument	k1gInSc1	dokument
ČT	ČT	kA	ČT
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc4	možnost
on-line	onin	k1gInSc5	on-lin
přehrání	přehrání	k1gNnSc3	přehrání
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Zagorová	Zagorový	k2eAgFnSc1d1	Zagorová
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Zagorová	Zagorový	k2eAgFnSc1d1	Zagorová
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Zagorová	Zagorový	k2eAgFnSc1d1	Zagorová
ve	v	k7c6	v
Filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Zagorová	Zagorový	k2eAgFnSc1d1	Zagorová
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Zagorová	Zagorový	k2eAgFnSc1d1	Zagorová
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Impulsy	impuls	k1gInPc4	impuls
Václava	Václav	k1gMnSc4	Václav
Moravce	Moravec	k1gMnSc2	Moravec
</s>
</p>
