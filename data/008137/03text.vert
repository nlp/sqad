<s>
Federální	federální	k2eAgInSc1d1	federální
úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Federal	Federal	k1gMnSc1	Federal
Bureau	Bureaus	k1gInSc2	Bureaus
of	of	k?	of
Investigation	Investigation	k1gInSc1	Investigation
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
FBI	FBI	kA	FBI
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vyšetřovací	vyšetřovací	k2eAgInSc1d1	vyšetřovací
orgán	orgán	k1gInSc1	orgán
amerického	americký	k2eAgNnSc2d1	americké
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
působí	působit	k5eAaImIp3nS	působit
jak	jak	k6eAd1	jak
jako	jako	k8xC	jako
federální	federální	k2eAgInSc1d1	federální
vyšetřovací	vyšetřovací	k2eAgInSc1d1	vyšetřovací
úřad	úřad	k1gInSc1	úřad
trestné	trestný	k2eAgFnSc2d1	trestná
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
jako	jako	k8xS	jako
kontrarozvědná	kontrarozvědný	k2eAgFnSc1d1	kontrarozvědná
služba	služba	k1gFnSc1	služba
<g/>
.	.	kIx.	.
</s>
<s>
Ředitel	ředitel	k1gMnSc1	ředitel
FBI	FBI	kA	FBI
je	být	k5eAaImIp3nS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
prezidentem	prezident	k1gMnSc7	prezident
USA	USA	kA	USA
a	a	k8xC	a
potvrzen	potvrdit	k5eAaPmNgInS	potvrdit
Senátem	senát	k1gInSc7	senát
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Počátek	počátek	k1gInSc1	počátek
FBI	FBI	kA	FBI
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
na	na	k7c4	na
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1908	[number]	k4	1908
<g/>
.	.	kIx.	.
</s>
<s>
FBI	FBI	kA	FBI
vyšetřuje	vyšetřovat	k5eAaImIp3nS	vyšetřovat
zločiny	zločin	k1gInPc4	zločin
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
buď	buď	k8xC	buď
páchány	páchán	k2eAgFnPc1d1	páchána
v	v	k7c6	v
několika	několik	k4yIc6	několik
státech	stát	k1gInPc6	stát
federace	federace	k1gFnSc2	federace
najednou	najednou	k9	najednou
nebo	nebo	k8xC	nebo
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
zvlášť	zvlášť	k6eAd1	zvlášť
závažné	závažný	k2eAgFnPc1d1	závažná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
seznam	seznam	k1gInSc1	seznam
zločinů	zločin	k1gInPc2	zločin
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
se	se	k3xPyFc4	se
FBI	FBI	kA	FBI
zabývá	zabývat	k5eAaImIp3nS	zabývat
<g/>
,	,	kIx,	,
činí	činit	k5eAaImIp3nS	činit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
250	[number]	k4	250
položek	položka	k1gFnPc2	položka
<g/>
.	.	kIx.	.
</s>
<s>
Mottem	motto	k1gNnSc7	motto
FBI	FBI	kA	FBI
je	být	k5eAaImIp3nS	být
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
Fidelity	Fidelita	k1gFnPc1	Fidelita
<g/>
,	,	kIx,	,
Bravery	Braver	k1gInPc1	Braver
<g/>
,	,	kIx,	,
Integrity	integrita	k1gFnPc1	integrita
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Věrnost	věrnost	k1gFnSc1	věrnost
<g/>
,	,	kIx,	,
statečnost	statečnost	k1gFnSc1	statečnost
<g/>
,	,	kIx,	,
sounáležitost	sounáležitost	k1gFnSc1	sounáležitost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Federální	federální	k2eAgMnPc1d1	federální
agenti	agent	k1gMnPc1	agent
FBI	FBI	kA	FBI
používají	používat	k5eAaImIp3nP	používat
pistole	pistol	k1gFnSc2	pistol
Glock	Glocka	k1gFnPc2	Glocka
22	[number]	k4	22
<g/>
,	,	kIx,	,
23	[number]	k4	23
a	a	k8xC	a
Sig	Sig	k1gMnSc1	Sig
Sauer	Sauer	k1gMnSc1	Sauer
P228	P228	k1gMnSc1	P228
v	v	k7c6	v
ráži	ráže	k1gFnSc6	ráže
.40	.40	k4	.40
S	s	k7c7	s
<g/>
&	&	k?	&
<g/>
W.	W.	kA	W.
Americký	americký	k2eAgMnSc1d1	americký
ministr	ministr	k1gMnSc1	ministr
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
Charles	Charles	k1gMnSc1	Charles
Joseph	Joseph	k1gMnSc1	Joseph
Bonaparte	bonapart	k1gInSc5	bonapart
(	(	kIx(	(
<g/>
prasynovec	prasynovec	k1gMnSc1	prasynovec
Napoleona	Napoleon	k1gMnSc2	Napoleon
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
vyvzdoroval	vyvzdorovat	k5eAaPmAgMnS	vyvzdorovat
na	na	k7c6	na
senátu	senát	k1gInSc6	senát
skupinu	skupina	k1gFnSc4	skupina
čtyřiatřiceti	čtyřiatřicet	k4xCc2	čtyřiatřicet
vyšetřovatelů	vyšetřovatel	k1gMnPc2	vyšetřovatel
a	a	k8xC	a
úředníků	úředník	k1gMnPc2	úředník
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
skromná	skromný	k2eAgFnSc1d1	skromná
kancelář	kancelář	k1gFnSc1	kancelář
rostla	růst	k5eAaImAgFnS	růst
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
přibývalo	přibývat	k5eAaImAgNnS	přibývat
kriminality	kriminalita	k1gFnSc2	kriminalita
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kterou	který	k3yIgFnSc4	který
si	se	k3xPyFc3	se
již	již	k6eAd1	již
nemohli	moct	k5eNaImAgMnP	moct
poradit	poradit	k5eAaPmF	poradit
místní	místní	k2eAgMnPc1d1	místní
ochránci	ochránce	k1gMnPc1	ochránce
práva	právo	k1gNnSc2	právo
(	(	kIx(	(
<g/>
šerifové	šerif	k1gMnPc1	šerif
<g/>
,	,	kIx,	,
policie	policie	k1gFnPc1	policie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
skutečně	skutečně	k6eAd1	skutečně
velkým	velký	k2eAgInSc7d1	velký
úkolem	úkol	k1gInSc7	úkol
agentů	agens	k1gInPc2	agens
FBI	FBI	kA	FBI
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
výnosu	výnos	k1gInSc2	výnos
prezidenta	prezident	k1gMnSc2	prezident
Theodora	Theodor	k1gMnSc2	Theodor
Roosevelta	Roosevelt	k1gMnSc2	Roosevelt
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
stíhání	stíhání	k1gNnSc2	stíhání
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
bílého	bílý	k2eAgNnSc2d1	bílé
otroctví	otroctví	k1gNnSc2	otroctví
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
organizované	organizovaný	k2eAgFnSc2d1	organizovaná
prostituce	prostituce	k1gFnSc2	prostituce
<g/>
.	.	kIx.	.
</s>
<s>
Celonárodní	celonárodní	k2eAgNnSc1d1	celonárodní
povědomí	povědomí	k1gNnSc1	povědomí
a	a	k8xC	a
zvýšení	zvýšení	k1gNnSc1	zvýšení
vlivu	vliv	k1gInSc2	vliv
FBI	FBI	kA	FBI
zajistil	zajistit	k5eAaPmAgMnS	zajistit
hlavně	hlavně	k6eAd1	hlavně
kontroverzní	kontroverzní	k2eAgMnSc1d1	kontroverzní
John	John	k1gMnSc1	John
Edgar	Edgar	k1gMnSc1	Edgar
Hoover	Hoover	k1gMnSc1	Hoover
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
devětadvaceti	devětadvacet	k4xCc6	devětadvacet
<g/>
,	,	kIx,	,
ředitelem	ředitel	k1gMnSc7	ředitel
kanceláře	kancelář	k1gFnSc2	kancelář
a	a	k8xC	a
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
funkci	funkce	k1gFnSc6	funkce
vydržel	vydržet	k5eAaPmAgMnS	vydržet
až	až	k9	až
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
</s>
<s>
Novináře	novinář	k1gMnPc4	novinář
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mu	on	k3xPp3gMnSc3	on
nešli	jít	k5eNaImAgMnP	jít
na	na	k7c4	na
ruku	ruka	k1gFnSc4	ruka
<g/>
,	,	kIx,	,
sice	sice	k8xC	sice
nazýval	nazývat	k5eAaImAgInS	nazývat
šakaly	šakal	k1gMnPc4	šakal
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
propagace	propagace	k1gFnSc1	propagace
FBI	FBI	kA	FBI
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gInSc7	jeho
velkým	velký	k2eAgInSc7d1	velký
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
jeho	jeho	k3xOp3gNnSc7	jeho
vedením	vedení	k1gNnSc7	vedení
však	však	k8xC	však
FBI	FBI	kA	FBI
často	často	k6eAd1	často
používala	používat	k5eAaImAgFnS	používat
nezákonné	zákonný	k2eNgFnPc4d1	nezákonná
metody	metoda	k1gFnPc4	metoda
a	a	k8xC	a
zastrašování	zastrašování	k1gNnPc4	zastrašování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
federální	federální	k2eAgMnPc1d1	federální
agenti	agent	k1gMnPc1	agent
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
zastřelili	zastřelit	k5eAaPmAgMnP	zastřelit
známého	známý	k2eAgMnSc4d1	známý
bankovního	bankovní	k2eAgMnSc4d1	bankovní
lupiče	lupič	k1gMnSc4	lupič
Johna	John	k1gMnSc4	John
Dillingera	Dillinger	k1gMnSc4	Dillinger
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
hodnoceno	hodnotit	k5eAaImNgNnS	hodnotit
velmi	velmi	k6eAd1	velmi
kladně	kladně	k6eAd1	kladně
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
ještě	ještě	k9	ještě
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
Hoover	Hoover	k1gMnSc1	Hoover
veřejně	veřejně	k6eAd1	veřejně
popíral	popírat	k5eAaImAgMnS	popírat
existenci	existence	k1gFnSc4	existence
mafie	mafie	k1gFnSc2	mafie
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
ignorování	ignorování	k1gNnSc6	ignorování
organizovaného	organizovaný	k2eAgInSc2d1	organizovaný
zločinu	zločin	k1gInSc2	zločin
pak	pak	k6eAd1	pak
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
spekulacím	spekulace	k1gFnPc3	spekulace
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
napojení	napojení	k1gNnSc6	napojení
na	na	k7c4	na
něj	on	k3xPp3gNnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Edgar	Edgar	k1gMnSc1	Edgar
Hoover	Hoover	k1gMnSc1	Hoover
však	však	k9	však
jako	jako	k9	jako
antikomunista	antikomunista	k1gMnSc1	antikomunista
monitoroval	monitorovat	k5eAaImAgMnS	monitorovat
také	také	k9	také
aktivity	aktivita	k1gFnPc4	aktivita
komunistů	komunista	k1gMnPc2	komunista
<g/>
,	,	kIx,	,
organizací	organizace	k1gFnPc2	organizace
odmítajících	odmítající	k2eAgFnPc2d1	odmítající
vietnamskou	vietnamský	k2eAgFnSc4d1	vietnamská
válku	válka	k1gFnSc4	válka
či	či	k8xC	či
skupin	skupina	k1gFnPc2	skupina
bojujících	bojující	k2eAgFnPc2d1	bojující
za	za	k7c4	za
občanskou	občanský	k2eAgFnSc4d1	občanská
rovnoprávnost	rovnoprávnost	k1gFnSc4	rovnoprávnost
černochů	černoch	k1gMnPc2	černoch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
Studené	Studené	k2eAgFnSc2d1	Studené
války	válka	k1gFnSc2	válka
dopadla	dopadnout	k5eAaPmAgFnS	dopadnout
FBI	FBI	kA	FBI
mnoho	mnoho	k6eAd1	mnoho
pro	pro	k7c4	pro
Sověty	Sověty	k1gInPc4	Sověty
pracujících	pracující	k2eAgMnPc2d1	pracující
špiónů	špión	k1gMnPc2	špión
-	-	kIx~	-
např.	např.	kA	např.
postupně	postupně	k6eAd1	postupně
Rudolpha	Rudolpha	k1gFnSc1	Rudolpha
Abela	Abel	k1gMnSc2	Abel
<g/>
,	,	kIx,	,
Johna	John	k1gMnSc2	John
Walkera	Walker	k1gMnSc2	Walker
a	a	k8xC	a
Roberta	Roberta	k1gFnSc1	Roberta
Hanssena	Hanssen	k2eAgFnSc1d1	Hanssena
či	či	k8xC	či
zatkla	zatknout	k5eAaPmAgFnS	zatknout
z	z	k7c2	z
neamerické	americký	k2eNgFnSc2d1	neamerická
činnosti	činnost	k1gFnSc2	činnost
obviňovaného	obviňovaný	k2eAgMnSc4d1	obviňovaný
Guse	Gus	k1gMnSc4	Gus
Halla	Hall	k1gMnSc4	Hall
<g/>
.	.	kIx.	.
</s>
<s>
Nešťastný	šťastný	k2eNgInSc1d1	nešťastný
konec	konec	k1gInSc1	konec
"	"	kIx"	"
<g/>
dvojčat	dvojče	k1gNnPc2	dvojče
<g/>
"	"	kIx"	"
a	a	k8xC	a
události	událost	k1gFnPc4	událost
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2001	[number]	k4	2001
ale	ale	k8xC	ale
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
slabost	slabost	k1gFnSc4	slabost
FBI	FBI	kA	FBI
a	a	k8xC	a
neschopnost	neschopnost	k1gFnSc1	neschopnost
spolupráce	spolupráce	k1gFnSc2	spolupráce
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
vládními	vládní	k2eAgFnPc7d1	vládní
agenturami	agentura	k1gFnPc7	agentura
<g/>
.	.	kIx.	.
</s>
<s>
Komise	komise	k1gFnPc1	komise
vyšetřující	vyšetřující	k2eAgFnPc1d1	vyšetřující
útoky	útok	k1gInPc4	útok
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
2001	[number]	k4	2001
došla	dojít	k5eAaPmAgFnS	dojít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gInPc3	on
měla	mít	k5eAaImAgFnS	mít
FBI	FBI	kA	FBI
společně	společně	k6eAd1	společně
s	s	k7c7	s
CIA	CIA	kA	CIA
zabránit	zabránit	k5eAaPmF	zabránit
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
úřadů	úřad	k1gInPc2	úřad
své	svůj	k3xOyFgFnSc6	svůj
zemi	zem	k1gFnSc6	zem
podle	podle	k7c2	podle
závěrečné	závěrečný	k2eAgFnSc2d1	závěrečná
zprávy	zpráva	k1gFnSc2	zpráva
komise	komise	k1gFnSc2	komise
"	"	kIx"	"
<g/>
neposloužil	posloužit	k5eNaPmAgMnS	posloužit
dobře	dobře	k6eAd1	dobře
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Chránit	chránit	k5eAaImF	chránit
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
před	před	k7c7	před
teroristickými	teroristický	k2eAgInPc7d1	teroristický
útoky	útok	k1gInPc7	útok
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
také	také	k9	také
válka	válka	k1gFnSc1	válka
proti	proti	k7c3	proti
terorismu	terorismus	k1gInSc3	terorismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chránit	chránit	k5eAaImF	chránit
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
před	před	k7c7	před
cizími	cizí	k2eAgFnPc7d1	cizí
výzvědnými	výzvědný	k2eAgFnPc7d1	výzvědná
službami	služba	k1gFnPc7	služba
a	a	k8xC	a
špiony	špion	k1gMnPc7	špion
<g/>
.	.	kIx.	.
</s>
<s>
Chránit	chránit	k5eAaImF	chránit
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
před	před	k7c7	před
počítačovými	počítačový	k2eAgInPc7d1	počítačový
zločiny	zločin	k1gInPc7	zločin
a	a	k8xC	a
zločiny	zločin	k1gInPc7	zločin
používající	používající	k2eAgInPc4d1	používající
high-tech	higho	k1gNnPc6	high-to
technologie	technologie	k1gFnSc2	technologie
(	(	kIx(	(
<g/>
kybernetickými	kybernetický	k2eAgInPc7d1	kybernetický
útoky	útok	k1gInPc7	útok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bojovat	bojovat	k5eAaImF	bojovat
proti	proti	k7c3	proti
korupci	korupce	k1gFnSc3	korupce
ve	v	k7c6	v
státní	státní	k2eAgFnSc6d1	státní
správě	správa	k1gFnSc6	správa
<g/>
.	.	kIx.	.
</s>
<s>
Chránit	chránit	k5eAaImF	chránit
občanská	občanský	k2eAgNnPc4d1	občanské
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Bojovat	bojovat	k5eAaImF	bojovat
proti	proti	k7c3	proti
zločinným	zločinný	k2eAgFnPc3d1	zločinná
organizacím	organizace	k1gFnPc3	organizace
a	a	k8xC	a
společnostem	společnost	k1gFnPc3	společnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
místním	místní	k2eAgFnPc3d1	místní
<g/>
,	,	kIx,	,
tak	tak	k9	tak
mezinárodním	mezinárodnět	k5eAaPmIp1nS	mezinárodnět
(	(	kIx(	(
<g/>
organizovanému	organizovaný	k2eAgInSc3d1	organizovaný
zločinu	zločin	k1gInSc3	zločin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bojovat	bojovat	k5eAaImF	bojovat
proti	proti	k7c3	proti
závažné	závažný	k2eAgFnSc3d1	závažná
kriminalitě	kriminalita	k1gFnSc3	kriminalita
spáchané	spáchaný	k2eAgMnPc4d1	spáchaný
pracovníky	pracovník	k1gMnPc4	pracovník
v	v	k7c6	v
administrativě	administrativa	k1gFnSc6	administrativa
<g/>
.	.	kIx.	.
</s>
<s>
Bojovat	bojovat	k5eAaImF	bojovat
proti	proti	k7c3	proti
závažným	závažný	k2eAgInPc3d1	závažný
násilným	násilný	k2eAgInPc3d1	násilný
zločinům	zločin	k1gInPc3	zločin
<g/>
.	.	kIx.	.
</s>
<s>
Podporovat	podporovat	k5eAaImF	podporovat
ostatní	ostatní	k2eAgMnPc4d1	ostatní
partnery	partner	k1gMnPc4	partner
ve	v	k7c6	v
výše	vysoce	k6eAd2	vysoce
uvedených	uvedený	k2eAgFnPc6d1	uvedená
záležitostech	záležitost	k1gFnPc6	záležitost
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
na	na	k7c4	na
místní	místní	k2eAgNnSc4d1	místní
<g/>
,	,	kIx,	,
státní	státní	k2eAgNnSc4d1	státní
a	a	k8xC	a
federální	federální	k2eAgNnSc4d1	federální
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
na	na	k7c6	na
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Vylepšovat	vylepšovat	k5eAaImF	vylepšovat
technologie	technologie	k1gFnPc4	technologie
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
úspěšnějšího	úspěšný	k2eAgNnSc2d2	úspěšnější
plnění	plnění	k1gNnSc2	plnění
úkolů	úkol	k1gInPc2	úkol
FBI	FBI	kA	FBI
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Centrála	centrála	k1gFnSc1	centrála
FBI	FBI	kA	FBI
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
J.	J.	kA	J.
Edgar	Edgar	k1gMnSc1	Edgar
Hoover	Hoover	k1gMnSc1	Hoover
Building	Building	k1gInSc4	Building
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
<g/>
,	,	kIx,	,
D.C.	D.C.	k1gFnSc5	D.C.
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
celých	celý	k2eAgInPc6d1	celý
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
400	[number]	k4	400
menších	malý	k2eAgFnPc2d2	menší
poboček	pobočka	k1gFnPc2	pobočka
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
pobočky	pobočka	k1gFnPc1	pobočka
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Česka	Česko	k1gNnSc2	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Pražská	pražský	k2eAgFnSc1d1	Pražská
pobočka	pobočka	k1gFnSc1	pobočka
oficiálně	oficiálně	k6eAd1	oficiálně
funguje	fungovat	k5eAaImIp3nS	fungovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
práci	práce	k1gFnSc6	práce
FBI	FBI	kA	FBI
bylo	být	k5eAaImAgNnS	být
natočeno	natočit	k5eAaBmNgNnS	natočit
také	také	k9	také
řada	řada	k1gFnSc1	řada
hraných	hraný	k2eAgInPc2d1	hraný
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
kriminální	kriminální	k2eAgFnSc2d1	kriminální
nebo	nebo	k8xC	nebo
detektivní	detektivní	k2eAgFnSc2d1	detektivní
povahy	povaha	k1gFnSc2	povaha
<g/>
.	.	kIx.	.
</s>
<s>
FBI	FBI	kA	FBI
v	v	k7c6	v
sukních	sukně	k1gFnPc6	sukně
<g/>
,	,	kIx,	,
americká	americký	k2eAgFnSc1d1	americká
filmová	filmový	k2eAgFnSc1d1	filmová
komedie	komedie	k1gFnSc1	komedie
o	o	k7c6	o
studiu	studio	k1gNnSc6	studio
žen	žena	k1gFnPc2	žena
na	na	k7c4	na
Akademii	akademie	k1gFnSc4	akademie
FBI	FBI	kA	FBI
Mlčení	mlčení	k1gNnSc1	mlčení
jehňátek	jehňátko	k1gNnPc2	jehňátko
<g/>
,	,	kIx,	,
kriminální	kriminální	k2eAgInSc1d1	kriminální
psychothiller	psychothiller	k1gInSc1	psychothiller
o	o	k7c4	o
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
sériových	sériový	k2eAgFnPc2d1	sériová
vražd	vražda	k1gFnPc2	vražda
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
zde	zde	k6eAd1	zde
hraje	hrát	k5eAaImIp3nS	hrát
posluchačka	posluchačka	k1gFnSc1	posluchačka
Akademie	akademie	k1gFnSc2	akademie
FBI	FBI	kA	FBI
Městečko	městečko	k1gNnSc1	městečko
Twin	Twin	k1gNnSc1	Twin
Peaks	Peaks	k1gInSc4	Peaks
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc4d1	americký
dramatický	dramatický	k2eAgInSc4d1	dramatický
seriál	seriál	k1gInSc4	seriál
o	o	k7c4	o
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
brutální	brutální	k2eAgFnSc2d1	brutální
vraždy	vražda	k1gFnSc2	vražda
populární	populární	k2eAgFnSc2d1	populární
mladé	mladý	k2eAgFnSc2d1	mladá
dívky	dívka	k1gFnSc2	dívka
a	a	k8xC	a
středoškolské	středoškolský	k2eAgFnSc2d1	středoškolská
královny	královna	k1gFnSc2	královna
<g/>
,	,	kIx,	,
Laury	Laura	k1gFnSc2	Laura
Palmerové	Palmerová	k1gFnSc2	Palmerová
(	(	kIx(	(
<g/>
Sheryl	Sheryl	k1gInSc1	Sheryl
Lee	Lea	k1gFnSc3	Lea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejž	jenž	k3xRgMnSc4	jenž
vede	vést	k5eAaImIp3nS	vést
speciální	speciální	k2eAgMnSc1d1	speciální
agent	agent	k1gMnSc1	agent
FBI	FBI	kA	FBI
Dale	Dale	k1gFnSc1	Dale
Cooper	Cooper	k1gMnSc1	Cooper
(	(	kIx(	(
<g/>
Kyle	Kyle	k1gFnSc1	Kyle
MacLachlan	MacLachlan	k1gMnSc1	MacLachlan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
FBI	FBI	kA	FBI
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
kriminální	kriminální	k2eAgInSc1d1	kriminální
a	a	k8xC	a
komediální	komediální	k2eAgInSc1d1	komediální
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
J.	J.	kA	J.
<g/>
Edgar	Edgar	k1gMnSc1	Edgar
<g/>
,	,	kIx,	,
životopisný	životopisný	k2eAgMnSc1d1	životopisný
<g/>
/	/	kIx~	/
<g/>
historický	historický	k2eAgInSc1d1	historický
<g/>
/	/	kIx~	/
<g/>
dráma	dráma	k1gFnSc1	dráma
<g/>
/	/	kIx~	/
<g/>
krimi	krimi	k1gNnSc1	krimi
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
</s>
