<s>
Federální	federální	k2eAgInSc1d1	federální
úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Federal	Federal	k1gMnSc1	Federal
Bureau	Bureaus	k1gInSc2	Bureaus
of	of	k?	of
Investigation	Investigation	k1gInSc1	Investigation
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
FBI	FBI	kA	FBI
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vyšetřovací	vyšetřovací	k2eAgInSc1d1	vyšetřovací
orgán	orgán	k1gInSc1	orgán
amerického	americký	k2eAgNnSc2d1	americké
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
působí	působit	k5eAaImIp3nS	působit
jak	jak	k6eAd1	jak
jako	jako	k8xC	jako
federální	federální	k2eAgInSc1d1	federální
vyšetřovací	vyšetřovací	k2eAgInSc1d1	vyšetřovací
úřad	úřad	k1gInSc1	úřad
trestné	trestný	k2eAgFnSc2d1	trestná
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
jako	jako	k8xS	jako
kontrarozvědná	kontrarozvědný	k2eAgFnSc1d1	kontrarozvědná
služba	služba	k1gFnSc1	služba
<g/>
.	.	kIx.	.
</s>
