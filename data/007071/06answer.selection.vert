<s>
Dnešní	dnešní	k2eAgNnPc1d1	dnešní
horolezecká	horolezecký	k2eAgNnPc1d1	horolezecké
lana	lano	k1gNnPc1	lano
jsou	být	k5eAaImIp3nP	být
zhotovena	zhotovit	k5eAaPmNgNnP	zhotovit
z	z	k7c2	z
polyamidu	polyamid	k1gInSc6	polyamid
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
výborné	výborný	k2eAgInPc4d1	výborný
pevnostní	pevnostní	k2eAgInPc4d1	pevnostní
parametry	parametr	k1gInPc4	parametr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavně	hlavně	k9	hlavně
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
jediný	jediný	k2eAgInSc1d1	jediný
schopen	schopen	k2eAgInSc1d1	schopen
efektivně	efektivně	k6eAd1	efektivně
tlumit	tlumit	k5eAaImF	tlumit
rázové	rázový	k2eAgFnPc4d1	rázová
síly	síla	k1gFnPc4	síla
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
při	při	k7c6	při
zachycení	zachycení	k1gNnSc6	zachycení
pádu	pád	k1gInSc2	pád
lezce	lezec	k1gMnSc2	lezec
<g/>
.	.	kIx.	.
</s>
