<s>
Horolezecké	horolezecký	k2eAgNnSc1d1	horolezecké
lano	lano	k1gNnSc1	lano
slouží	sloužit	k5eAaImIp3nS	sloužit
lezci	lezec	k1gMnSc3	lezec
k	k	k7c3	k
jištění	jištění	k1gNnSc3	jištění
při	při	k7c6	při
výstupu	výstup	k1gInSc6	výstup
v	v	k7c6	v
těžkém	těžký	k2eAgInSc6d1	těžký
horském	horský	k2eAgInSc6d1	horský
terénu	terén	k1gInSc6	terén
<g/>
,	,	kIx,	,
při	při	k7c6	při
sportovním	sportovní	k2eAgNnSc6d1	sportovní
lezení	lezení	k1gNnSc6	lezení
na	na	k7c6	na
cvičných	cvičný	k2eAgFnPc6d1	cvičná
stěnách	stěna	k1gFnPc6	stěna
<g/>
,	,	kIx,	,
lezení	lezení	k1gNnSc3	lezení
na	na	k7c6	na
ledu	led	k1gInSc6	led
a	a	k8xC	a
pro	pro	k7c4	pro
další	další	k2eAgFnPc4d1	další
outdoorové	outdoorový	k2eAgFnPc4d1	outdoorová
aktivity	aktivita	k1gFnPc4	aktivita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
chránit	chránit	k5eAaImF	chránit
osoby	osoba	k1gFnPc4	osoba
proti	proti	k7c3	proti
pádu	pád	k1gInSc3	pád
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
využívána	využívat	k5eAaPmNgNnP	využívat
i	i	k9	i
pro	pro	k7c4	pro
slanění	slanění	k1gNnSc4	slanění
(	(	kIx(	(
<g/>
sestup	sestup	k1gInSc1	sestup
pomocí	pomocí	k7c2	pomocí
lana	lano	k1gNnSc2	lano
<g/>
)	)	kIx)	)
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nelze	lze	k6eNd1	lze
použít	použít	k5eAaPmF	použít
klasický	klasický	k2eAgInSc4d1	klasický
sestup	sestup	k1gInSc4	sestup
z	z	k7c2	z
výšky	výška	k1gFnSc2	výška
–	–	k?	–
např.	např.	kA	např.
skalní	skalní	k2eAgInPc1d1	skalní
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
jinou	jiný	k2eAgFnSc7d1	jiná
<g/>
)	)	kIx)	)
stěnou	stěna	k1gFnSc7	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgNnPc1d1	dnešní
horolezecká	horolezecký	k2eAgNnPc1d1	horolezecké
lana	lano	k1gNnPc1	lano
jsou	být	k5eAaImIp3nP	být
zhotovena	zhotovit	k5eAaPmNgNnP	zhotovit
z	z	k7c2	z
polyamidu	polyamid	k1gInSc6	polyamid
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
výborné	výborný	k2eAgInPc4d1	výborný
pevnostní	pevnostní	k2eAgInPc4d1	pevnostní
parametry	parametr	k1gInPc4	parametr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavně	hlavně	k9	hlavně
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
jediný	jediný	k2eAgInSc1d1	jediný
schopen	schopen	k2eAgInSc1d1	schopen
efektivně	efektivně	k6eAd1	efektivně
tlumit	tlumit	k5eAaImF	tlumit
rázové	rázový	k2eAgFnPc4d1	rázová
síly	síla	k1gFnPc4	síla
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
při	při	k7c6	při
zachycení	zachycení	k1gNnSc6	zachycení
pádu	pád	k1gInSc2	pád
lezce	lezec	k1gMnSc2	lezec
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
dynamická	dynamický	k2eAgNnPc1d1	dynamické
lana	lano	k1gNnPc1	lano
jsou	být	k5eAaImIp3nP	být
konstruována	konstruován	k2eAgNnPc1d1	konstruováno
jako	jako	k8xC	jako
pletená	pletený	k2eAgFnSc1d1	pletená
s	s	k7c7	s
jádrem	jádro	k1gNnSc7	jádro
a	a	k8xC	a
opletem	opletem	k?	opletem
(	(	kIx(	(
<g/>
systém	systém	k1gInSc1	systém
Kernmantel	Kernmantel	k1gInSc1	Kernmantel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obdobnou	obdobný	k2eAgFnSc4d1	obdobná
konstrukci	konstrukce	k1gFnSc4	konstrukce
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
statické	statický	k2eAgNnSc1d1	statické
lano	lano	k1gNnSc1	lano
(	(	kIx(	(
<g/>
lano	lano	k1gNnSc1	lano
s	s	k7c7	s
nízkou	nízký	k2eAgFnSc7d1	nízká
průtažností	průtažnost	k1gFnSc7	průtažnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
má	mít	k5eAaImIp3nS	mít
při	při	k7c6	při
zatížení	zatížení	k1gNnSc6	zatížení
menší	malý	k2eAgFnSc4d2	menší
průtažnost	průtažnost	k1gFnSc4	průtažnost
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
výrazně	výrazně	k6eAd1	výrazně
menší	malý	k2eAgFnSc1d2	menší
schopnost	schopnost	k1gFnSc1	schopnost
tlumení	tlumení	k1gNnSc2	tlumení
rázových	rázový	k2eAgFnPc2d1	rázová
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Statické	statický	k2eAgNnSc1d1	statické
lano	lano	k1gNnSc1	lano
najde	najít	k5eAaPmIp3nS	najít
uplatnění	uplatnění	k1gNnSc4	uplatnění
při	při	k7c6	při
budování	budování	k1gNnSc6	budování
lanovek	lanovka	k1gFnPc2	lanovka
a	a	k8xC	a
statických	statický	k2eAgNnPc2d1	statické
jištění	jištění	k1gNnPc2	jištění
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
také	také	k9	také
jako	jako	k9	jako
lano	lano	k1gNnSc1	lano
pracovní	pracovní	k2eAgNnSc1d1	pracovní
<g/>
.	.	kIx.	.
</s>
<s>
Dělení	dělení	k1gNnSc1	dělení
dle	dle	k7c2	dle
Franka	Frank	k1gMnSc2	Frank
a	a	k8xC	a
Kubálka	Kubálek	k1gMnSc2	Kubálek
<g/>
:	:	kIx,	:
dynamická	dynamický	k2eAgNnPc4d1	dynamické
lana	lano	k1gNnPc4	lano
–	–	k?	–
jejich	jejich	k3xOp3gInSc7	jejich
stěžejním	stěžejní	k2eAgInSc7d1	stěžejní
faktorem	faktor	k1gInSc7	faktor
je	být	k5eAaImIp3nS	být
průtažnost	průtažnost	k1gFnSc1	průtažnost
(	(	kIx(	(
<g/>
dynamické	dynamický	k2eAgNnSc1d1	dynamické
prodloužení	prodloužení	k1gNnSc1	prodloužení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
bývá	bývat	k5eAaImIp3nS	bývat
v	v	k7c6	v
rozmezích	rozmezí	k1gNnPc6	rozmezí
8	[number]	k4	8
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
použití	použití	k1gNnSc1	použití
je	být	k5eAaImIp3nS	být
především	především	k9	především
v	v	k7c6	v
horolezectví	horolezectví	k1gNnSc6	horolezectví
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
lezení	lezení	k1gNnSc4	lezení
"	"	kIx"	"
<g/>
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
<g/>
"	"	kIx"	"
jedinou	jediný	k2eAgFnSc7d1	jediná
možností	možnost	k1gFnSc7	možnost
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
pružnost	pružnost	k1gFnSc1	pružnost
totiž	totiž	k9	totiž
postupně	postupně	k6eAd1	postupně
pohlcuje	pohlcovat	k5eAaImIp3nS	pohlcovat
kinetickou	kinetický	k2eAgFnSc4d1	kinetická
energii	energie	k1gFnSc4	energie
horolezce	horolezec	k1gMnSc2	horolezec
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
rizikům	riziko	k1gNnPc3	riziko
způsobeným	způsobený	k2eAgNnSc7d1	způsobené
velkým	velký	k2eAgNnSc7d1	velké
přetížením	přetížení	k1gNnSc7	přetížení
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
zachycení	zachycení	k1gNnSc2	zachycení
pádu	pád	k1gInSc2	pád
<g/>
.	.	kIx.	.
</s>
<s>
Těmito	tento	k3xDgInPc7	tento
riziky	riziko	k1gNnPc7	riziko
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vážná	vážný	k2eAgNnPc4d1	vážné
vnitřní	vnitřní	k2eAgNnPc4d1	vnitřní
zranění	zranění	k1gNnPc4	zranění
<g/>
,	,	kIx,	,
poškození	poškození	k1gNnSc4	poškození
páteře	páteř	k1gFnSc2	páteř
při	při	k7c6	při
uvázání	uvázání	k1gNnSc6	uvázání
lana	lano	k1gNnSc2	lano
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
sedací	sedací	k2eAgInSc4d1	sedací
úvazek	úvazek	k1gInSc4	úvazek
nebo	nebo	k8xC	nebo
selhání	selhání	k1gNnSc4	selhání
některých	některý	k3yIgFnPc2	některý
částí	část	k1gFnPc2	část
jistícího	jistící	k2eAgInSc2d1	jistící
systému	systém	k1gInSc2	systém
(	(	kIx(	(
<g/>
štand	štanda	k1gFnPc2	štanda
<g/>
,	,	kIx,	,
postupové	postupový	k2eAgNnSc1d1	postupové
jištění	jištění	k1gNnSc1	jištění
<g/>
,	,	kIx,	,
úvazek	úvazek	k1gInSc1	úvazek
<g/>
,	,	kIx,	,
lano	lano	k1gNnSc1	lano
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
průtažnosti	průtažnost	k1gFnSc2	průtažnost
je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
údajem	údaj	k1gInSc7	údaj
dynamického	dynamický	k2eAgNnSc2d1	dynamické
lana	lano	k1gNnSc2	lano
počet	počet	k1gInSc1	počet
normovaných	normovaný	k2eAgInPc2d1	normovaný
pádů	pád	k1gInPc2	pád
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
lano	lano	k1gNnSc1	lano
vydržet	vydržet	k5eAaPmF	vydržet
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
udávány	udávat	k5eAaImNgFnP	udávat
výrobcem	výrobce	k1gMnSc7	výrobce
<g/>
.	.	kIx.	.
</s>
<s>
Normy	Norma	k1gFnPc1	Norma
dynamických	dynamický	k2eAgNnPc2d1	dynamické
lan	lano	k1gNnPc2	lano
<g/>
:	:	kIx,	:
EN	EN	kA	EN
892	[number]	k4	892
<g/>
,	,	kIx,	,
ČSN	ČSN	kA	ČSN
EN	EN	kA	EN
892	[number]	k4	892
<g/>
,	,	kIx,	,
UIAA	UIAA	kA	UIAA
101	[number]	k4	101
<g/>
.	.	kIx.	.
statická	statický	k2eAgNnPc4d1	statické
lana	lano	k1gNnPc4	lano
–	–	k?	–
mají	mít	k5eAaImIp3nP	mít
výrazně	výrazně	k6eAd1	výrazně
nižší	nízký	k2eAgFnSc4d2	nižší
průtažnost	průtažnost	k1gFnSc4	průtažnost
než	než	k8xS	než
dynamická	dynamický	k2eAgNnPc4d1	dynamické
lana	lano	k1gNnPc4	lano
(	(	kIx(	(
<g/>
do	do	k7c2	do
5	[number]	k4	5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
hlavně	hlavně	k6eAd1	hlavně
jako	jako	k9	jako
pracovní	pracovní	k2eAgNnPc4d1	pracovní
lana	lano	k1gNnPc4	lano
<g/>
,	,	kIx,	,
užití	užití	k1gNnSc4	užití
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
ve	v	k7c6	v
speleologii	speleologie	k1gFnSc6	speleologie
a	a	k8xC	a
v	v	k7c6	v
záchranářství	záchranářství	k1gNnSc6	záchranářství
<g/>
,	,	kIx,	,
v	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pružení	pružení	k1gNnSc1	pružení
lana	lano	k1gNnSc2	lano
není	být	k5eNaImIp3nS	být
žádoucí	žádoucí	k2eAgFnSc1d1	žádoucí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horolezectví	horolezectví	k1gNnSc6	horolezectví
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
zřídka	zřídka	k6eAd1	zřídka
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
na	na	k7c4	na
vytahování	vytahování	k1gNnSc4	vytahování
zásob	zásoba	k1gFnPc2	zásoba
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
levnější	levný	k2eAgNnPc4d2	levnější
než	než	k8xS	než
dynamická	dynamický	k2eAgNnPc4d1	dynamické
lana	lano	k1gNnPc4	lano
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
statického	statický	k2eAgNnSc2d1	statické
lana	lano	k1gNnSc2	lano
při	při	k7c6	při
jištění	jištění	k1gNnSc6	jištění
prvolezce	prvolezec	k1gMnSc2	prvolezec
je	být	k5eAaImIp3nS	být
smrtelně	smrtelně	k6eAd1	smrtelně
nebezpečné	bezpečný	k2eNgNnSc1d1	nebezpečné
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
jiné	jiný	k2eAgInPc4d1	jiný
druhy	druh	k1gInPc4	druh
lan	lano	k1gNnPc2	lano
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
statodynamické	statodynamický	k2eAgFnPc1d1	statodynamický
<g/>
,	,	kIx,	,
superstatické	superstatický	k2eAgFnPc1d1	superstatický
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
lana	lano	k1gNnPc4	lano
specializovaná	specializovaný	k2eAgNnPc4d1	specializované
na	na	k7c6	na
různé	různý	k2eAgFnSc6d1	různá
konkrétní	konkrétní	k2eAgFnSc6d1	konkrétní
činnosti	činnost	k1gFnSc6	činnost
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
speleologie	speleologie	k1gFnSc1	speleologie
<g/>
,	,	kIx,	,
canyoning	canyoning	k1gInSc1	canyoning
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
lano	lano	k1gNnSc1	lano
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
single	singl	k1gInSc5	singl
rope	rope	k1gNnPc7	rope
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
symbolem	symbol	k1gInSc7	symbol
jedničky	jednička	k1gFnSc2	jednička
v	v	k7c6	v
kroužku	kroužek	k1gInSc6	kroužek
<g/>
.	.	kIx.	.
</s>
<s>
Průměr	průměr	k1gInSc1	průměr
bývá	bývat	k5eAaImIp3nS	bývat
mezi	mezi	k7c7	mezi
9	[number]	k4	9
<g/>
–	–	k?	–
<g/>
11	[number]	k4	11
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
především	především	k9	především
v	v	k7c6	v
bezpečnějším	bezpečný	k2eAgInSc6d2	bezpečnější
terénu	terén	k1gInSc6	terén
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nehrozí	hrozit	k5eNaImIp3nS	hrozit
poškození	poškození	k1gNnSc3	poškození
lana	lano	k1gNnSc2	lano
například	například	k6eAd1	například
pádem	pád	k1gInSc7	pád
kamene	kámen	k1gInSc2	kámen
nebo	nebo	k8xC	nebo
přeříznutí	přeříznutí	k1gNnSc2	přeříznutí
přes	přes	k7c4	přes
ostrou	ostrý	k2eAgFnSc4d1	ostrá
skalní	skalní	k2eAgFnSc4d1	skalní
hranu	hrana	k1gFnSc4	hrana
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
na	na	k7c6	na
umělých	umělý	k2eAgFnPc6d1	umělá
stěnách	stěna	k1gFnPc6	stěna
a	a	k8xC	a
sportovních	sportovní	k2eAgFnPc6d1	sportovní
lezeckých	lezecký	k2eAgFnPc6d1	lezecká
lokalitách	lokalita	k1gFnPc6	lokalita
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
norem	norma	k1gFnPc2	norma
UIAA	UIAA	kA	UIAA
převzatých	převzatý	k2eAgFnPc2d1	převzatá
i	i	k8xC	i
do	do	k7c2	do
našeho	náš	k3xOp1gInSc2	náš
normového	normový	k2eAgInSc2d1	normový
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
lano	lano	k1gNnSc1	lano
testuje	testovat	k5eAaImIp3nS	testovat
pádem	pád	k1gInSc7	pád
80	[number]	k4	80
Kg	kg	kA	kg
závaží	závaží	k1gNnSc1	závaží
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
pramene	pramen	k1gInSc2	pramen
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
vydržet	vydržet	k5eAaPmF	vydržet
minimálně	minimálně	k6eAd1	minimálně
5	[number]	k4	5
normovaných	normovaný	k2eAgInPc2d1	normovaný
pádů	pád	k1gInPc2	pád
<g/>
,	,	kIx,	,
síla	síla	k1gFnSc1	síla
při	při	k7c6	při
zachycení	zachycení	k1gNnSc6	zachycení
pádu	pád	k1gInSc2	pád
je	být	k5eAaImIp3nS	být
maximálně	maximálně	k6eAd1	maximálně
12	[number]	k4	12
kN	kN	k?	kN
<g/>
.	.	kIx.	.
</s>
<s>
Poloviční	poloviční	k2eAgNnPc4d1	poloviční
lana	lano	k1gNnPc4	lano
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
half	halfa	k1gFnPc2	halfa
rope	rope	k6eAd1	rope
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
také	také	k9	také
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
půlky	půlka	k1gFnSc2	půlka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Označují	označovat	k5eAaImIp3nP	označovat
se	s	k7c7	s
symbolem	symbol	k1gInSc7	symbol
1⁄	1⁄	k?	1⁄
v	v	k7c6	v
kroužku	kroužek	k1gInSc6	kroužek
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
průměr	průměr	k1gInSc1	průměr
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgInSc1d2	menší
než	než	k8xS	než
u	u	k7c2	u
jednoduchých	jednoduchý	k2eAgNnPc2d1	jednoduché
lan	lano	k1gNnPc2	lano
(	(	kIx(	(
<g/>
bývá	bývat	k5eAaImIp3nS	bývat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
kolem	kolem	k7c2	kolem
7,5	[number]	k4	7,5
<g/>
–	–	k?	–
<g/>
9	[number]	k4	9
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poloviční	poloviční	k2eAgNnPc1d1	poloviční
lana	lano	k1gNnPc1	lano
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
vždy	vždy	k6eAd1	vždy
ve	v	k7c6	v
dvojici	dvojice	k1gFnSc6	dvojice
a	a	k8xC	a
postupové	postupový	k2eAgNnSc1d1	postupové
jištění	jištění	k1gNnSc1	jištění
se	se	k3xPyFc4	se
zakládá	zakládat	k5eAaImIp3nS	zakládat
střídavě	střídavě	k6eAd1	střídavě
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jištění	jištění	k1gNnSc1	jištění
založí	založit	k5eAaPmIp3nS	založit
nejdříve	dříve	k6eAd3	dříve
pro	pro	k7c4	pro
jedno	jeden	k4xCgNnSc4	jeden
lano	lano	k1gNnSc4	lano
a	a	k8xC	a
v	v	k7c6	v
při	pře	k1gFnSc6	pře
další	další	k2eAgFnSc2d1	další
příležitosti	příležitost	k1gFnSc2	příležitost
k	k	k7c3	k
zajištění	zajištění	k1gNnSc3	zajištění
se	se	k3xPyFc4	se
do	do	k7c2	do
jištění	jištění	k1gNnSc2	jištění
"	"	kIx"	"
<g/>
cvakne	cvaknout	k5eAaPmIp3nS	cvaknout
<g/>
"	"	kIx"	"
lano	lano	k1gNnSc1	lano
druhé	druhý	k4xOgFnSc6	druhý
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
eliminuje	eliminovat	k5eAaBmIp3nS	eliminovat
tření	tření	k1gNnSc1	tření
lan	lano	k1gNnPc2	lano
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
cesta	cesta	k1gFnSc1	cesta
kličkuje	kličkovat	k5eAaImIp3nS	kličkovat
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
jedno	jeden	k4xCgNnSc1	jeden
lano	lano	k1gNnSc1	lano
cvaká	cvakat	k5eAaImIp3nS	cvakat
do	do	k7c2	do
levých	levý	k2eAgNnPc2d1	levé
jištění	jištění	k1gNnPc2	jištění
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgFnSc2	druhý
do	do	k7c2	do
pravých	pravá	k1gFnPc2	pravá
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
do	do	k7c2	do
karabiny	karabina	k1gFnSc2	karabina
postupového	postupový	k2eAgNnSc2d1	postupové
jištění	jištění	k1gNnSc2	jištění
nevkládají	vkládat	k5eNaImIp3nP	vkládat
oba	dva	k4xCgInPc4	dva
prameny	pramen	k1gInPc4	pramen
lan	lano	k1gNnPc2	lano
naráz	naráz	k6eAd1	naráz
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejsou	být	k5eNaImIp3nP	být
lana	lano	k1gNnPc1	lano
vedena	vést	k5eAaImNgNnP	vést
stále	stále	k6eAd1	stále
paralelně	paralelně	k6eAd1	paralelně
a	a	k8xC	a
při	při	k7c6	při
pádu	pád	k1gInSc6	pád
se	se	k3xPyFc4	se
každé	každý	k3xTgNnSc1	každý
napíná	napínat	k5eAaImIp3nS	napínat
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
mohla	moct	k5eAaImAgFnS	moct
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
vzájemným	vzájemný	k2eAgNnSc7d1	vzájemné
třením	tření	k1gNnSc7	tření
pod	pod	k7c7	pod
zatížením	zatížení	k1gNnSc7	zatížení
přepálit	přepálit	k5eAaPmF	přepálit
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zapnout	zapnout	k5eAaPmF	zapnout
obě	dva	k4xCgNnPc4	dva
poloviční	poloviční	k2eAgNnPc4d1	poloviční
lana	lano	k1gNnPc4	lano
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
jisticího	jisticí	k2eAgInSc2d1	jisticí
bodu	bod	k1gInSc2	bod
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
použít	použít	k5eAaPmF	použít
dvou	dva	k4xCgFnPc6	dva
samostatných	samostatný	k2eAgFnPc2d1	samostatná
karabin	karabina	k1gFnPc2	karabina
<g/>
.	.	kIx.	.
</s>
<s>
Dynamické	dynamický	k2eAgNnSc4d1	dynamické
prodloužení	prodloužení	k1gNnSc4	prodloužení
polovičního	poloviční	k2eAgNnSc2d1	poloviční
lana	lano	k1gNnSc2	lano
není	být	k5eNaImIp3nS	být
dimenzované	dimenzovaný	k2eAgNnSc1d1	dimenzované
na	na	k7c4	na
pád	pád	k1gInSc4	pád
člověka	člověk	k1gMnSc2	člověk
do	do	k7c2	do
jediného	jediný	k2eAgInSc2d1	jediný
pramenu	pramen	k1gInSc2	pramen
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
střídavém	střídavý	k2eAgNnSc6d1	střídavé
jištění	jištění	k1gNnSc6	jištění
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
rovněž	rovněž	k9	rovněž
dbát	dbát	k5eAaImF	dbát
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
prameny	pramen	k1gInPc1	pramen
lan	lano	k1gNnPc2	lano
nekřížily	křížit	k5eNaImAgInP	křížit
<g/>
,	,	kIx,	,
mohlo	moct	k5eAaImAgNnS	moct
by	by	kYmCp3nS	by
dojít	dojít	k5eAaPmF	dojít
při	při	k7c6	při
pádu	pád	k1gInSc6	pád
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
přepálení	přepálení	k1gNnSc3	přepálení
<g/>
.	.	kIx.	.
</s>
<s>
Jištění	jištění	k1gNnSc1	jištění
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
oddělenými	oddělený	k2eAgInPc7d1	oddělený
polovičními	poloviční	k2eAgInPc7d1	poloviční
uzly	uzel	k1gInPc7	uzel
na	na	k7c6	na
dvou	dva	k4xCgFnPc6	dva
karabinách	karabina	k1gFnPc6	karabina
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pomocí	pomocí	k7c2	pomocí
jisticí	jisticí	k2eAgFnSc2d1	jisticí
pomůcky	pomůcka	k1gFnSc2	pomůcka
umožňující	umožňující	k2eAgNnSc4d1	umožňující
vložení	vložení	k1gNnSc4	vložení
dvou	dva	k4xCgFnPc2	dva
nezávislých	závislý	k2eNgFnPc2d1	nezávislá
lan	lano	k1gNnPc2	lano
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
polovičních	poloviční	k2eAgNnPc2d1	poloviční
lan	lano	k1gNnPc2	lano
předchází	předcházet	k5eAaImIp3nS	předcházet
nehodám	nehoda	k1gFnPc3	nehoda
způsobeným	způsobený	k2eAgNnSc7d1	způsobené
přeříznutím	přeříznutí	k1gNnSc7	přeříznutí
lan	lano	k1gNnPc2	lano
na	na	k7c6	na
ostrých	ostrý	k2eAgFnPc6d1	ostrá
skalních	skalní	k2eAgFnPc6d1	skalní
hranách	hrana	k1gFnPc6	hrana
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gNnSc7	jejich
přeseknutím	přeseknutí	k1gNnSc7	přeseknutí
padajícím	padající	k2eAgNnSc7d1	padající
kamením	kamení	k1gNnSc7	kamení
<g/>
,	,	kIx,	,
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
horských	horský	k2eAgFnPc6d1	horská
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Poloviční	poloviční	k2eAgNnSc1d1	poloviční
lano	lano	k1gNnSc1	lano
se	se	k3xPyFc4	se
testuje	testovat	k5eAaImIp3nS	testovat
pádem	pád	k1gInSc7	pád
závaží	závaží	k1gNnPc2	závaží
55	[number]	k4	55
Kg	kg	kA	kg
do	do	k7c2	do
jediného	jediný	k2eAgInSc2d1	jediný
pramene	pramen	k1gInSc2	pramen
<g/>
,	,	kIx,	,
pádová	pádový	k2eAgFnSc1d1	pádová
síla	síla	k1gFnSc1	síla
nesmí	smět	k5eNaImIp3nS	smět
překročit	překročit	k5eAaPmF	překročit
8	[number]	k4	8
kN	kN	k?	kN
<g/>
,	,	kIx,	,
lano	lano	k1gNnSc1	lano
musí	muset	k5eAaImIp3nS	muset
vydržet	vydržet	k5eAaPmF	vydržet
nejméně	málo	k6eAd3	málo
5	[number]	k4	5
normovaných	normovaný	k2eAgInPc2d1	normovaný
pádů	pád	k1gInPc2	pád
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
lezení	lezení	k1gNnSc6	lezení
tříčlenného	tříčlenný	k2eAgNnSc2d1	tříčlenné
družstva	družstvo	k1gNnSc2	družstvo
lze	lze	k6eAd1	lze
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
polovičních	poloviční	k2eAgNnPc2d1	poloviční
lan	lano	k1gNnPc2	lano
použít	použít	k5eAaPmF	použít
taktiku	taktika	k1gFnSc4	taktika
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
leze	lézt	k5eAaImIp3nS	lézt
prvolezec	prvolezec	k1gMnSc1	prvolezec
navázan	navázan	k1gMnSc1	navázan
na	na	k7c6	na
obou	dva	k4xCgNnPc6	dva
lanech	lano	k1gNnPc6	lano
<g/>
,	,	kIx,	,
druholezci	druholezce	k1gMnPc1	druholezce
jsou	být	k5eAaImIp3nP	být
každý	každý	k3xTgInSc4	každý
na	na	k7c6	na
jednom	jeden	k4xCgNnSc6	jeden
laně	lano	k1gNnSc6	lano
<g/>
.	.	kIx.	.
</s>
<s>
Prvolezec	prvolezec	k1gMnSc1	prvolezec
pak	pak	k6eAd1	pak
dobírá	dobírat	k5eAaImIp3nS	dobírat
oba	dva	k4xCgMnPc4	dva
druholezce	druholezka	k1gFnSc6	druholezka
současně	současně	k6eAd1	současně
a	a	k8xC	a
postup	postup	k1gInSc4	postup
trojice	trojice	k1gFnSc2	trojice
takto	takto	k6eAd1	takto
není	být	k5eNaImIp3nS	být
pomalejší	pomalý	k2eAgMnSc1d2	pomalejší
<g/>
,	,	kIx,	,
než	než	k8xS	než
u	u	k7c2	u
dvoučlenného	dvoučlenný	k2eAgNnSc2d1	dvoučlenné
družstva	družstvo	k1gNnSc2	družstvo
<g/>
.	.	kIx.	.
</s>
<s>
Dvojitá	dvojitý	k2eAgNnPc4d1	dvojité
lana	lano	k1gNnPc4	lano
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
twin	twin	k1gInSc1	twin
rope	rop	k1gInSc2	rop
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
také	také	k9	také
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
dvojčata	dvojče	k1gNnPc1	dvojče
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Průměr	průměr	k1gInSc1	průměr
mívají	mívat	k5eAaImIp3nP	mívat
kolem	kolem	k7c2	kolem
8	[number]	k4	8
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Označují	označovat	k5eAaImIp3nP	označovat
se	s	k7c7	s
spojenými	spojený	k2eAgInPc7d1	spojený
"	"	kIx"	"
<g/>
prstýnky	prstýnek	k1gInPc7	prstýnek
<g/>
"	"	kIx"	"
v	v	k7c6	v
kroužku	kroužek	k1gInSc6	kroužek
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
výhradně	výhradně	k6eAd1	výhradně
ve	v	k7c6	v
dvojici	dvojice	k1gFnSc6	dvojice
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
jediné	jediný	k2eAgNnSc4d1	jediné
lano	lano	k1gNnSc4	lano
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
polovičních	poloviční	k2eAgNnPc2d1	poloviční
lan	lano	k1gNnPc2	lano
<g/>
,	,	kIx,	,
do	do	k7c2	do
karabin	karabina	k1gFnPc2	karabina
postupového	postupový	k2eAgNnSc2d1	postupové
jištění	jištění	k1gNnSc2	jištění
vkládají	vkládat	k5eAaImIp3nP	vkládat
oba	dva	k4xCgInPc1	dva
prameny	pramen	k1gInPc1	pramen
zároveň	zároveň	k6eAd1	zároveň
<g/>
.	.	kIx.	.
</s>
<s>
Střídavě	střídavě	k6eAd1	střídavě
být	být	k5eAaImF	být
zakládána	zakládat	k5eAaImNgFnS	zakládat
nesmí	smět	k5eNaImIp3nS	smět
<g/>
.	.	kIx.	.
</s>
<s>
Dvojčata	dvojče	k1gNnPc1	dvojče
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
poloviční	poloviční	k2eAgNnPc4d1	poloviční
lana	lano	k1gNnPc4	lano
v	v	k7c6	v
méně	málo	k6eAd2	málo
bezpečném	bezpečný	k2eAgInSc6d1	bezpečný
terénu	terén	k1gInSc6	terén
<g/>
.	.	kIx.	.
</s>
<s>
Dvojitá	dvojitý	k2eAgNnPc1d1	dvojité
lana	lano	k1gNnPc1	lano
se	se	k3xPyFc4	se
testují	testovat	k5eAaImIp3nP	testovat
pádem	pád	k1gInSc7	pád
závaží	závaží	k1gNnPc2	závaží
80	[number]	k4	80
Kg	kg	kA	kg
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
pramenů	pramen	k1gInPc2	pramen
lana	lano	k1gNnSc2	lano
<g/>
,	,	kIx,	,
pádová	pádový	k2eAgFnSc1d1	pádová
síla	síla	k1gFnSc1	síla
nesmí	smět	k5eNaImIp3nS	smět
překročit	překročit	k5eAaPmF	překročit
12	[number]	k4	12
kN	kN	k?	kN
<g/>
,	,	kIx,	,
soustava	soustava	k1gFnSc1	soustava
dvojčat	dvojče	k1gNnPc2	dvojče
musí	muset	k5eAaImIp3nS	muset
vydržet	vydržet	k5eAaPmF	vydržet
minimálně	minimálně	k6eAd1	minimálně
12	[number]	k4	12
normovaných	normovaný	k2eAgInPc2d1	normovaný
pádů	pád	k1gInPc2	pád
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
vyšší	vysoký	k2eAgFnSc2d2	vyšší
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
mají	mít	k5eAaImIp3nP	mít
poloviční	poloviční	k2eAgNnPc4d1	poloviční
a	a	k8xC	a
dvojitá	dvojitý	k2eAgNnPc4d1	dvojité
lana	lano	k1gNnPc4	lano
výhodu	výhod	k1gInSc2	výhod
dvojnásobné	dvojnásobný	k2eAgFnSc2d1	dvojnásobná
délky	délka	k1gFnSc2	délka
při	při	k7c6	při
slaňování	slaňování	k1gNnSc6	slaňování
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
účel	účel	k1gInSc4	účel
se	se	k3xPyFc4	se
lana	lano	k1gNnPc1	lano
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
konci	konec	k1gInSc6	konec
svazují	svazovat	k5eAaImIp3nP	svazovat
tzv.	tzv.	kA	tzv.
vůdcovským	vůdcovský	k2eAgInSc7d1	vůdcovský
uzlem	uzel	k1gInSc7	uzel
<g/>
.	.	kIx.	.
uvedené	uvedený	k2eAgInPc1d1	uvedený
průměry	průměr	k1gInPc1	průměr
lan	lano	k1gNnPc2	lano
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
orientační	orientační	k2eAgFnPc1d1	orientační
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
rychlému	rychlý	k2eAgInSc3d1	rychlý
vývoji	vývoj	k1gInSc3	vývoj
materiálu	materiál	k1gInSc2	materiál
se	s	k7c7	s
průměry	průměr	k1gInPc7	průměr
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
druhů	druh	k1gInPc2	druh
lan	lano	k1gNnPc2	lano
neustále	neustále	k6eAd1	neustále
snižují	snižovat	k5eAaImIp3nP	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
většina	většina	k1gFnSc1	většina
polovičních	poloviční	k2eAgNnPc2d1	poloviční
lan	lano	k1gNnPc2	lano
zároveň	zároveň	k6eAd1	zároveň
certifikována	certifikovat	k5eAaImNgFnS	certifikovat
jako	jako	k9	jako
dvojčata	dvojče	k1gNnPc1	dvojče
<g/>
,	,	kIx,	,
vzájemný	vzájemný	k2eAgInSc1d1	vzájemný
rozdíl	rozdíl	k1gInSc1	rozdíl
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
smývá	smývat	k5eAaImIp3nS	smývat
<g/>
.	.	kIx.	.
</s>
<s>
Existující	existující	k2eAgNnSc1d1	existující
dokonce	dokonce	k9	dokonce
lana	lano	k1gNnPc4	lano
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
jsou	být	k5eAaImIp3nP	být
certifikována	certifikovat	k5eAaImNgNnP	certifikovat
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
všemi	všecek	k3xTgFnPc7	všecek
třemi	tři	k4xCgInPc7	tři
způsoby	způsob	k1gInPc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
umělých	umělý	k2eAgFnPc6d1	umělá
stěnách	stěna	k1gFnPc6	stěna
obvykle	obvykle	k6eAd1	obvykle
stačí	stačit	k5eAaBmIp3nS	stačit
délka	délka	k1gFnSc1	délka
30	[number]	k4	30
až	až	k9	až
40	[number]	k4	40
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skalním	skalní	k2eAgInSc6d1	skalní
a	a	k8xC	a
horském	horský	k2eAgInSc6d1	horský
terénu	terén	k1gInSc6	terén
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
potřeba	potřeba	k6eAd1	potřeba
alespoň	alespoň	k9	alespoň
50	[number]	k4	50
a	a	k8xC	a
víc	hodně	k6eAd2	hodně
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Horské	Horské	k2eAgFnPc1d1	Horské
a	a	k8xC	a
vícedélkové	vícedélkový	k2eAgFnPc1d1	vícedélkový
cesty	cesta	k1gFnPc1	cesta
se	s	k7c7	s
zajištěnými	zajištěný	k2eAgInPc7d1	zajištěný
štandy	štanda	k1gFnPc4	štanda
bývají	bývat	k5eAaImIp3nP	bývat
zpravidla	zpravidla	k6eAd1	zpravidla
dimenzovány	dimenzovat	k5eAaBmNgInP	dimenzovat
na	na	k7c4	na
určitou	určitý	k2eAgFnSc4d1	určitá
délku	délka	k1gFnSc4	délka
lana	lano	k1gNnSc2	lano
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
60	[number]	k4	60
m.	m.	k?	m.
Horolezecká	horolezecký	k2eAgNnPc4d1	horolezecké
lana	lano	k1gNnPc4	lano
bývají	bývat	k5eAaImIp3nP	bývat
vystavována	vystavovat	k5eAaImNgFnS	vystavovat
náročným	náročný	k2eAgFnPc3d1	náročná
podmínkám	podmínka	k1gFnPc3	podmínka
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
sníh	sníh	k1gInSc1	sníh
<g/>
,	,	kIx,	,
led	led	k1gInSc1	led
nebo	nebo	k8xC	nebo
písek	písek	k1gInSc1	písek
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
postupné	postupný	k2eAgNnSc4d1	postupné
mechanické	mechanický	k2eAgNnSc4d1	mechanické
a	a	k8xC	a
chemické	chemický	k2eAgNnSc4d1	chemické
opotřebení	opotřebení	k1gNnSc4	opotřebení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimních	zimní	k2eAgFnPc6d1	zimní
podmínkách	podmínka	k1gFnPc6	podmínka
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
,	,	kIx,	,
že	že	k8xS	že
lano	lano	k1gNnSc1	lano
zvlhne	zvlhnout	k5eAaPmIp3nS	zvlhnout
a	a	k8xC	a
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
laně	lano	k1gNnSc6	lano
následně	následně	k6eAd1	následně
zmrzne	zmrznout	k5eAaPmIp3nS	zmrznout
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
ke	k	k7c3	k
zhoršení	zhoršení	k1gNnSc3	zhoršení
vlastností	vlastnost	k1gFnPc2	vlastnost
materiálu	materiál	k1gInSc2	materiál
a	a	k8xC	a
horší	zlý	k2eAgFnSc4d2	horší
manipulaci	manipulace	k1gFnSc4	manipulace
s	s	k7c7	s
lanem	lano	k1gNnSc7	lano
<g/>
.	.	kIx.	.
</s>
<s>
Výrobci	výrobce	k1gMnPc1	výrobce
proto	proto	k8xC	proto
nabízejí	nabízet	k5eAaImIp3nP	nabízet
různé	různý	k2eAgFnPc4d1	různá
úpravy	úprava	k1gFnPc4	úprava
povrchu	povrch	k1gInSc2	povrch
a	a	k8xC	a
impregnace	impregnace	k1gFnSc2	impregnace
zabraňující	zabraňující	k2eAgNnSc4d1	zabraňující
navlhnutí	navlhnutí	k1gNnSc4	navlhnutí
nebo	nebo	k8xC	nebo
vniku	vnik	k1gInSc2	vnik
nečistot	nečistota	k1gFnPc2	nečistota
skrz	skrz	k6eAd1	skrz
oplet	oplít	k5eAaPmNgInS	oplít
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
lana	lano	k1gNnPc4	lano
vyráběná	vyráběný	k2eAgNnPc4d1	vyráběné
speciálně	speciálně	k6eAd1	speciálně
pro	pro	k7c4	pro
lezení	lezení	k1gNnSc4	lezení
na	na	k7c6	na
umělých	umělý	k2eAgFnPc6d1	umělá
stěnách	stěna	k1gFnPc6	stěna
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
indoor	indoor	k1gInSc1	indoor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Horolezecká	horolezecký	k2eAgNnPc1d1	horolezecké
lana	lano	k1gNnPc1	lano
byla	být	k5eAaImAgNnP	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
(	(	kIx(	(
<g/>
cca	cca	kA	cca
do	do	k7c2	do
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
vyráběna	vyrábět	k5eAaImNgFnS	vyrábět
z	z	k7c2	z
přírodních	přírodní	k2eAgInPc2d1	přírodní
materiálů	materiál	k1gInPc2	materiál
(	(	kIx(	(
<g/>
např.	např.	kA	např.
konopí	konopí	k1gNnSc1	konopí
<g/>
,	,	kIx,	,
sisal	sisal	k1gInSc1	sisal
<g/>
,	,	kIx,	,
bavlna	bavlna	k1gFnSc1	bavlna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
lana	lano	k1gNnPc1	lano
měla	mít	k5eAaImAgNnP	mít
výrazně	výrazně	k6eAd1	výrazně
menší	malý	k2eAgFnSc4d2	menší
dynamickou	dynamický	k2eAgFnSc4d1	dynamická
pevnost	pevnost	k1gFnSc4	pevnost
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
náchylná	náchylný	k2eAgFnSc1d1	náchylná
k	k	k7c3	k
hnilobě	hniloba	k1gFnSc3	hniloba
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
průmyslového	průmyslový	k2eAgNnSc2d1	průmyslové
zavedení	zavedení	k1gNnSc2	zavedení
polyamidových	polyamidový	k2eAgNnPc2d1	polyamidové
vláken	vlákno	k1gNnPc2	vlákno
se	se	k3xPyFc4	se
již	již	k6eAd1	již
tyto	tento	k3xDgInPc1	tento
materiály	materiál	k1gInPc1	materiál
neužívají	užívat	k5eNaImIp3nP	užívat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zlepšení	zlepšení	k1gNnSc3	zlepšení
vlastností	vlastnost	k1gFnPc2	vlastnost
horolezeckých	horolezecký	k2eAgNnPc2d1	horolezecké
lan	lano	k1gNnPc2	lano
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
různé	různý	k2eAgFnPc4d1	různá
chemické	chemický	k2eAgFnPc4d1	chemická
úpravy	úprava	k1gFnPc4	úprava
jejich	jejich	k3xOp3gNnPc2	jejich
vláken	vlákno	k1gNnPc2	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Hlavně	hlavně	k9	hlavně
jsou	být	k5eAaImIp3nP	být
chráněna	chránit	k5eAaImNgFnS	chránit
proti	proti	k7c3	proti
vlhkosti	vlhkost	k1gFnSc3	vlhkost
<g/>
,	,	kIx,	,
oděru	oděr	k1gInSc3	oděr
a	a	k8xC	a
nečistotám	nečistota	k1gFnPc3	nečistota
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Pádový	pádový	k2eAgInSc4d1	pádový
faktor	faktor	k1gInSc4	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Horolezecká	horolezecký	k2eAgNnPc4d1	horolezecké
lana	lano	k1gNnPc4	lano
se	se	k3xPyFc4	se
zkouší	zkoušet	k5eAaImIp3nS	zkoušet
absolvováním	absolvování	k1gNnSc7	absolvování
normových	normový	k2eAgInPc2d1	normový
pádů	pád	k1gInPc2	pád
na	na	k7c6	na
zkušebním	zkušební	k2eAgInSc6d1	zkušební
pádostroji	pádostroj	k1gInSc6	pádostroj
při	při	k7c6	při
předepsaném	předepsaný	k2eAgInSc6d1	předepsaný
pádovém	pádový	k2eAgInSc6d1	pádový
faktoru	faktor	k1gInSc6	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
zkoušené	zkoušený	k2eAgNnSc1d1	zkoušené
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
lano	lano	k1gNnSc1	lano
musí	muset	k5eAaImIp3nS	muset
bez	bez	k7c2	bez
přetržení	přetržení	k1gNnSc2	přetržení
absolvovat	absolvovat	k5eAaPmF	absolvovat
5	[number]	k4	5
normových	normový	k2eAgInPc2d1	normový
pádů	pád	k1gInPc2	pád
se	s	k7c7	s
zkušebním	zkušební	k2eAgNnSc7d1	zkušební
závažím	závaží	k1gNnSc7	závaží
z	z	k7c2	z
pevné	pevný	k2eAgFnSc2d1	pevná
hmoty	hmota	k1gFnSc2	hmota
(	(	kIx(	(
<g/>
ocel	ocel	k1gFnSc1	ocel
<g/>
,	,	kIx,	,
beton	beton	k1gInSc1	beton
<g/>
)	)	kIx)	)
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
80	[number]	k4	80
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dynamických	dynamický	k2eAgNnPc2d1	dynamické
horolezeckých	horolezecký	k2eAgNnPc2d1	horolezecké
lan	lano	k1gNnPc2	lano
není	být	k5eNaImIp3nS	být
udávaná	udávaný	k2eAgFnSc1d1	udávaná
statická	statický	k2eAgFnSc1d1	statická
pevnost	pevnost	k1gFnSc1	pevnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
udávána	udáván	k2eAgFnSc1d1	udávána
pevnost	pevnost	k1gFnSc1	pevnost
dynamická	dynamický	k2eAgFnSc1d1	dynamická
–	–	k?	–
počet	počet	k1gInSc4	počet
zachycených	zachycený	k2eAgInPc2d1	zachycený
normových	normový	k2eAgInPc2d1	normový
pádů	pád	k1gInPc2	pád
<g/>
.	.	kIx.	.
statické	statický	k2eAgNnSc4d1	statické
lano	lano	k1gNnSc4	lano
textilní	textilní	k2eAgNnSc1d1	textilní
lano	lano	k1gNnSc1	lano
horolezectví	horolezectví	k1gNnSc2	horolezectví
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
horolezecké	horolezecký	k2eAgFnSc2d1	horolezecká
lano	lano	k1gNnSc4	lano
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
www.horolezeckametodika.cz	www.horolezeckametodika.cz	k1gInSc1	www.horolezeckametodika.cz
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
Mokrá	mokrý	k2eAgNnPc4d1	mokré
a	a	k8xC	a
zmrzlá	zmrzlý	k2eAgNnPc4d1	zmrzlé
lana	lano	k1gNnPc4	lano
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
Statická	statický	k2eAgNnPc1d1	statické
lana	lano	k1gNnPc1	lano
<g/>
:	:	kIx,	:
vliv	vliv	k1gInSc1	vliv
vody	voda	k1gFnSc2	voda
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
Kontrola	kontrola	k1gFnSc1	kontrola
lan	lano	k1gNnPc2	lano
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
</s>
