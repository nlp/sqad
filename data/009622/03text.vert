<p>
<s>
Protestantismus	protestantismus	k1gInSc1	protestantismus
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
směrů	směr	k1gInPc2	směr
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
,	,	kIx,	,
vycházející	vycházející	k2eAgFnSc1d1	vycházející
z	z	k7c2	z
náboženských	náboženský	k2eAgFnPc2d1	náboženská
reformních	reformní	k2eAgFnPc2d1	reformní
hnutí	hnutí	k1gNnPc1	hnutí
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
pozdního	pozdní	k2eAgInSc2d1	pozdní
středověku	středověk	k1gInSc2	středověk
a	a	k8xC	a
raného	raný	k2eAgInSc2d1	raný
novověku	novověk	k1gInSc2	novověk
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
z	z	k7c2	z
reformace	reformace	k1gFnSc2	reformace
<g/>
,	,	kIx,	,
zahájené	zahájený	k2eAgFnSc3d1	zahájená
vystoupením	vystoupení	k1gNnSc7	vystoupení
Martina	Martin	k2eAgNnSc2d1	Martino
Luthera	Luthero	k1gNnSc2	Luthero
proti	proti	k7c3	proti
prodeji	prodej	k1gInSc3	prodej
odpustků	odpustek	k1gInPc2	odpustek
roku	rok	k1gInSc2	rok
1517	[number]	k4	1517
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označený	k2eAgMnPc1d1	označený
protestanti	protestant	k1gMnPc1	protestant
bylo	být	k5eAaImAgNnS	být
původně	původně	k6eAd1	původně
politické	politický	k2eAgNnSc1d1	politické
a	a	k8xC	a
vztahovalo	vztahovat	k5eAaImAgNnS	vztahovat
se	se	k3xPyFc4	se
na	na	k7c4	na
luteránská	luteránský	k2eAgNnPc4d1	luteránské
knížata	kníže	k1gNnPc4	kníže
a	a	k8xC	a
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
na	na	k7c6	na
říšském	říšský	k2eAgInSc6d1	říšský
sněmu	sněm	k1gInSc6	sněm
ve	v	k7c6	v
Špýru	Špýr	k1gInSc6	Špýr
roku	rok	k1gInSc2	rok
1529	[number]	k4	1529
protestovala	protestovat	k5eAaBmAgFnS	protestovat
proti	proti	k7c3	proti
zákazu	zákaz	k1gInSc3	zákaz
reformace	reformace	k1gFnSc2	reformace
ve	v	k7c6	v
Svaté	svatý	k2eAgFnSc6d1	svatá
říši	říš	k1gFnSc6	říš
římské	římský	k2eAgFnSc6d1	římská
<g/>
.	.	kIx.	.
</s>
<s>
Počínaje	počínaje	k7c7	počínaje
vestfálským	vestfálský	k2eAgInSc7d1	vestfálský
mírem	mír	k1gInSc7	mír
r.	r.	kA	r.
1648	[number]	k4	1648
se	se	k3xPyFc4	se
označení	označení	k1gNnSc2	označení
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
na	na	k7c4	na
všechny	všechen	k3xTgMnPc4	všechen
křesťany	křesťan	k1gMnPc4	křesťan
vyznávající	vyznávající	k2eAgFnSc2d1	vyznávající
reformační	reformační	k2eAgFnSc2d1	reformační
a	a	k8xC	a
podobné	podobný	k2eAgFnSc2d1	podobná
nauky	nauka	k1gFnSc2	nauka
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgNnSc1d1	jiné
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
protestanty	protestant	k1gMnPc4	protestant
je	být	k5eAaImIp3nS	být
evangelíci	evangelík	k1gMnPc1	evangelík
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
k	k	k7c3	k
jejich	jejich	k3xOp3gInSc3	jejich
důrazu	důraz	k1gInSc3	důraz
na	na	k7c6	na
bibli	bible	k1gFnSc6	bible
<g/>
,	,	kIx,	,
evangelium	evangelium	k1gNnSc4	evangelium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
Protestantismus	protestantismus	k1gInSc1	protestantismus
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
během	během	k7c2	během
světové	světový	k2eAgFnSc2d1	světová
reformace	reformace	k1gFnSc2	reformace
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
krize	krize	k1gFnSc2	krize
západní	západní	k2eAgFnSc2d1	západní
církve	církev	k1gFnSc2	církev
se	se	k3xPyFc4	se
však	však	k9	však
projevovala	projevovat	k5eAaImAgFnS	projevovat
již	již	k6eAd1	již
na	na	k7c6	na
konci	konec	k1gInSc6	konec
středověku	středověk	k1gInSc6	středověk
řadou	řada	k1gFnSc7	řada
reformních	reformní	k2eAgNnPc2d1	reformní
hnutí	hnutí	k1gNnPc2	hnutí
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yIgFnPc4	který
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
užívá	užívat	k5eAaImIp3nS	užívat
termín	termín	k1gInSc4	termín
první	první	k4xOgFnSc2	první
reformace	reformace	k1gFnSc2	reformace
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
české	český	k2eAgNnSc4d1	české
husitství	husitství	k1gNnSc4	husitství
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
první	první	k4xOgFnSc2	první
reformace	reformace	k1gFnSc2	reformace
dodnes	dodnes	k6eAd1	dodnes
bez	bez	k7c2	bez
přerušení	přerušení	k1gNnSc2	přerušení
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
hnutí	hnutí	k1gNnSc4	hnutí
valdenských	valdenští	k1gMnPc2	valdenští
a	a	k8xC	a
Jednota	jednota	k1gFnSc1	jednota
bratrská	bratrský	k2eAgFnSc1d1	bratrská
<g/>
.	.	kIx.	.
</s>
<s>
Půdu	půda	k1gFnSc4	půda
reformaci	reformace	k1gFnSc4	reformace
připravovali	připravovat	k5eAaImAgMnP	připravovat
i	i	k9	i
mystici	mystik	k1gMnPc1	mystik
typu	typ	k1gInSc2	typ
Mistra	mistr	k1gMnSc2	mistr
Eckharta	Eckhart	k1gMnSc2	Eckhart
a	a	k8xC	a
humanisté	humanista	k1gMnPc1	humanista
jako	jako	k9	jako
Erasmus	Erasmus	k1gMnSc1	Erasmus
Rotterdamský	rotterdamský	k2eAgMnSc1d1	rotterdamský
<g/>
,	,	kIx,	,
na	na	k7c4	na
jejichž	jejichž	k3xOyRp3gInPc4	jejichž
náboženské	náboženský	k2eAgInPc4d1	náboženský
a	a	k8xC	a
intelektuální	intelektuální	k2eAgInPc4d1	intelektuální
podněty	podnět	k1gInPc4	podnět
mohli	moct	k5eAaImAgMnP	moct
reformátoři	reformátor	k1gMnPc1	reformátor
navázat	navázat	k5eAaPmF	navázat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Počátek	počátek	k1gInSc1	počátek
vlastní	vlastní	k2eAgFnSc2d1	vlastní
reformace	reformace	k1gFnSc2	reformace
se	se	k3xPyFc4	se
klade	klást	k5eAaImIp3nS	klást
na	na	k7c4	na
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1517	[number]	k4	1517
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Martin	Martin	k1gMnSc1	Martin
Luther	Luthra	k1gFnPc2	Luthra
předložil	předložit	k5eAaPmAgMnS	předložit
svých	svůj	k3xOyFgFnPc2	svůj
95	[number]	k4	95
tezí	teze	k1gFnPc2	teze
<g/>
,	,	kIx,	,
kritiku	kritika	k1gFnSc4	kritika
zlořádů	zlořád	k1gInPc2	zlořád
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
především	především	k9	především
zneužívání	zneužívání	k1gNnSc1	zneužívání
odpustků	odpustek	k1gInPc2	odpustek
<g/>
.	.	kIx.	.
</s>
<s>
Lutherem	Luther	k1gInSc7	Luther
založené	založený	k2eAgNnSc1d1	založené
hnutí	hnutí	k1gNnSc1	hnutí
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
šířilo	šířit	k5eAaImAgNnS	šířit
zprvu	zprvu	k6eAd1	zprvu
po	po	k7c6	po
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
poté	poté	k6eAd1	poté
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
západní	západní	k2eAgFnSc6d1	západní
a	a	k8xC	a
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
kritiky	kritika	k1gFnSc2	kritika
stávajícího	stávající	k2eAgNnSc2d1	stávající
zneužívání	zneužívání	k1gNnSc2	zneužívání
náboženské	náboženský	k2eAgFnSc2d1	náboženská
moci	moc	k1gFnSc2	moc
nabízelo	nabízet	k5eAaImAgNnS	nabízet
i	i	k9	i
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
náboženský	náboženský	k2eAgInSc4d1	náboženský
obsah	obsah	k1gInSc4	obsah
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
bohoslužbu	bohoslužba	k1gFnSc4	bohoslužba
v	v	k7c6	v
lidovém	lidový	k2eAgInSc6d1	lidový
jazyce	jazyk	k1gInSc6	jazyk
namísto	namísto	k7c2	namísto
dosavadní	dosavadní	k2eAgFnSc2d1	dosavadní
latiny	latina	k1gFnSc2	latina
<g/>
,	,	kIx,	,
prostotu	prostota	k1gFnSc4	prostota
a	a	k8xC	a
soustředění	soustředění	k1gNnSc4	soustředění
na	na	k7c4	na
jádro	jádro	k1gNnSc4	jádro
křesťanského	křesťanský	k2eAgNnSc2d1	křesťanské
poselství	poselství	k1gNnSc2	poselství
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
mu	on	k3xPp3gMnSc3	on
reformátoři	reformátor	k1gMnPc1	reformátor
rozuměli	rozumět	k5eAaImAgMnP	rozumět
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
úspěch	úspěch	k1gInSc4	úspěch
reformace	reformace	k1gFnSc2	reformace
měla	mít	k5eAaImAgFnS	mít
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
i	i	k8xC	i
podpora	podpora	k1gFnSc1	podpora
řady	řada	k1gFnSc2	řada
světských	světský	k2eAgMnPc2d1	světský
vládců	vládce	k1gMnPc2	vládce
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
někteří	některý	k3yIgMnPc1	některý
–	–	k?	–
jako	jako	k8xS	jako
například	například	k6eAd1	například
anglický	anglický	k2eAgMnSc1d1	anglický
král	král	k1gMnSc1	král
Jindřich	Jindřich	k1gMnSc1	Jindřich
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
</s>
<s>
Tudor	tudor	k1gInSc1	tudor
–	–	k?	–
vedle	vedle	k7c2	vedle
očisty	očista	k1gFnSc2	očista
církve	církev	k1gFnSc2	církev
vítali	vítat	k5eAaImAgMnP	vítat
i	i	k9	i
možnost	možnost	k1gFnSc4	možnost
zabavovat	zabavovat	k5eAaImF	zabavovat
církevní	církevní	k2eAgInSc4d1	církevní
majetek	majetek	k1gInSc4	majetek
a	a	k8xC	a
osvobodit	osvobodit	k5eAaPmF	osvobodit
se	se	k3xPyFc4	se
od	od	k7c2	od
vlivu	vliv	k1gInSc2	vliv
papežství	papežství	k1gNnSc2	papežství
<g/>
.	.	kIx.	.
</s>
<s>
Protest	protest	k1gInSc1	protest
reformovaných	reformovaný	k2eAgNnPc2d1	reformované
knížat	kníže	k1gNnPc2	kníže
a	a	k8xC	a
měst	město	k1gNnPc2	město
na	na	k7c6	na
říšském	říšský	k2eAgInSc6d1	říšský
sněmu	sněm	k1gInSc6	sněm
ve	v	k7c6	v
Špýru	Špýr	k1gInSc6	Špýr
roku	rok	k1gInSc2	rok
1529	[number]	k4	1529
také	také	k9	také
dal	dát	k5eAaPmAgMnS	dát
novému	nový	k2eAgNnSc3d1	nové
hnutí	hnutí	k1gNnSc3	hnutí
jméno	jméno	k1gNnSc1	jméno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Protestantismus	protestantismus	k1gInSc1	protestantismus
sice	sice	k8xC	sice
nese	nést	k5eAaImIp3nS	nést
inovativní	inovativní	k2eAgFnPc4d1	inovativní
myšlenky	myšlenka	k1gFnPc4	myšlenka
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
teologie	teologie	k1gFnSc2	teologie
<g/>
,	,	kIx,	,
organizace	organizace	k1gFnSc2	organizace
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
procesu	proces	k1gInSc2	proces
bohoslužeb	bohoslužba	k1gFnPc2	bohoslužba
a	a	k8xC	a
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
tradic	tradice	k1gFnPc2	tradice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
byly	být	k5eAaImAgFnP	být
reformy	reforma	k1gFnPc1	reforma
většinou	většinou	k6eAd1	většinou
prováděny	provádět	k5eAaImNgInP	provádět
velmi	velmi	k6eAd1	velmi
pozvolna	pozvolna	k6eAd1	pozvolna
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
provádí	provádět	k5eAaImIp3nS	provádět
vlastní	vlastní	k2eAgFnPc4d1	vlastní
reformy	reforma	k1gFnPc4	reforma
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgFnPc7	jenž
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
na	na	k7c4	na
kritiku	kritika	k1gFnSc4	kritika
reformátorů	reformátor	k1gMnPc2	reformátor
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
konečném	konečný	k2eAgInSc6d1	konečný
důsledku	důsledek	k1gInSc6	důsledek
týkají	týkat	k5eAaImIp3nP	týkat
všech	všecek	k3xTgMnPc2	všecek
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
protestantů	protestant	k1gMnPc2	protestant
<g/>
.	.	kIx.	.
</s>
<s>
Reformátoři	reformátor	k1gMnPc1	reformátor
se	se	k3xPyFc4	se
eventuálně	eventuálně	k6eAd1	eventuálně
setkali	setkat	k5eAaPmAgMnP	setkat
s	s	k7c7	s
problémem	problém	k1gInSc7	problém
<g/>
:	:	kIx,	:
počáteční	počáteční	k2eAgNnSc1d1	počáteční
nadšení	nadšení	k1gNnSc1	nadšení
lidí	člověk	k1gMnPc2	člověk
se	se	k3xPyFc4	se
vytratilo	vytratit	k5eAaPmAgNnS	vytratit
<g/>
,	,	kIx,	,
přijímali	přijímat	k5eAaImAgMnP	přijímat
inovace	inovace	k1gFnPc4	inovace
především	především	k9	především
chladně	chladně	k6eAd1	chladně
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
i	i	k9	i
nesouhlasili	souhlasit	k5eNaImAgMnP	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
Vyvstala	vyvstat	k5eAaPmAgFnS	vyvstat
tak	tak	k9	tak
nutnost	nutnost	k1gFnSc1	nutnost
plynulé	plynulý	k2eAgFnPc1d1	plynulá
změny	změna	k1gFnPc1	změna
<g/>
,	,	kIx,	,
přechod	přechod	k1gInSc1	přechod
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
považovali	považovat	k5eAaImAgMnP	považovat
evangelisté	evangelista	k1gMnPc1	evangelista
za	za	k7c4	za
ideální	ideální	k2eAgNnSc4d1	ideální
nakonec	nakonec	k6eAd1	nakonec
trval	trvat	k5eAaImAgMnS	trvat
skoro	skoro	k6eAd1	skoro
100	[number]	k4	100
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Reformace	reformace	k1gFnSc1	reformace
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
neprosadila	prosadit	k5eNaPmAgFnS	prosadit
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
papežství	papežství	k1gNnSc2	papežství
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgInS	postavit
vlivný	vlivný	k2eAgInSc1d1	vlivný
rod	rod	k1gInSc1	rod
Habsburků	Habsburk	k1gMnPc2	Habsburk
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
panovníci	panovník	k1gMnPc1	panovník
jižní	jižní	k2eAgFnSc2d1	jižní
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
sama	sám	k3xTgFnSc1	sám
římská	římský	k2eAgFnSc1d1	římská
církev	církev	k1gFnSc1	církev
prošla	projít	k5eAaPmAgFnS	projít
reformou	reforma	k1gFnSc7	reforma
v	v	k7c6	v
takzvané	takzvaný	k2eAgFnSc6d1	takzvaná
protireformaci	protireformace	k1gFnSc6	protireformace
<g/>
.	.	kIx.	.
</s>
<s>
Třicetiletá	třicetiletý	k2eAgFnSc1d1	třicetiletá
válka	válka	k1gFnSc1	válka
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
konflikty	konflikt	k1gInPc1	konflikt
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
přinesly	přinést	k5eAaPmAgInP	přinést
jisté	jistý	k2eAgInPc1d1	jistý
územní	územní	k2eAgInPc1d1	územní
posuny	posun	k1gInPc1	posun
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
české	český	k2eAgFnSc2d1	Česká
země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
po	po	k7c6	po
potlačení	potlačení	k1gNnSc6	potlačení
stavovského	stavovský	k2eAgNnSc2d1	Stavovské
povstání	povstání	k1gNnSc2	povstání
rekatolizovaly	rekatolizovat	k5eAaBmAgFnP	rekatolizovat
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
západní	západní	k2eAgFnSc1d1	západní
a	a	k8xC	a
střední	střední	k2eAgFnSc1d1	střední
Evropa	Evropa	k1gFnSc1	Evropa
zůstala	zůstat	k5eAaPmAgFnS	zůstat
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
země	zem	k1gFnPc4	zem
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
románská	románský	k2eAgNnPc1d1	románské
a	a	k8xC	a
západoslovanská	západoslovanský	k2eAgNnPc1d1	západoslovanské
území	území	k1gNnPc1	území
<g/>
)	)	kIx)	)
a	a	k8xC	a
protestantské	protestantský	k2eAgNnSc1d1	protestantské
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
germánská	germánský	k2eAgNnPc1d1	germánské
území	území	k1gNnPc1	území
<g/>
)	)	kIx)	)
a	a	k8xC	a
nábožensky	nábožensky	k6eAd1	nábožensky
rozdělena	rozdělen	k2eAgFnSc1d1	rozdělena
zůstala	zůstat	k5eAaPmAgFnS	zůstat
i	i	k9	i
samotná	samotný	k2eAgFnSc1d1	samotná
Svatá	svatý	k2eAgFnSc1d1	svatá
říše	říše	k1gFnSc1	říše
římská	římský	k2eAgFnSc1d1	římská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
centralizovaného	centralizovaný	k2eAgInSc2d1	centralizovaný
římského	římský	k2eAgInSc2d1	římský
katolicismu	katolicismus	k1gInSc2	katolicismus
byly	být	k5eAaImAgFnP	být
reformované	reformovaný	k2eAgFnPc1d1	reformovaná
církve	církev	k1gFnPc1	církev
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
regionálně	regionálně	k6eAd1	regionálně
i	i	k9	i
věroučně	věroučně	k6eAd1	věroučně
roztříštěné	roztříštěný	k2eAgFnPc1d1	roztříštěná
<g/>
.	.	kIx.	.
</s>
<s>
Luteránství	luteránství	k1gNnSc1	luteránství
<g/>
,	,	kIx,	,
navazující	navazující	k2eAgInPc1d1	navazující
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
Luthera	Luthero	k1gNnPc4	Luthero
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
prosadilo	prosadit	k5eAaPmAgNnS	prosadit
především	především	k9	především
v	v	k7c6	v
severním	severní	k2eAgNnSc6d1	severní
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
<g/>
.	.	kIx.	.
</s>
<s>
Radikálnější	radikální	k2eAgInSc1d2	radikálnější
kalvinismus	kalvinismus	k1gInSc1	kalvinismus
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
syntéze	syntéza	k1gFnSc6	syntéza
učení	učení	k1gNnSc2	učení
Jana	Jan	k1gMnSc4	Jan
Kalvína	Kalvín	k1gMnSc4	Kalvín
a	a	k8xC	a
Ulricha	Ulrich	k1gMnSc4	Ulrich
Zwingliho	Zwingli	k1gMnSc4	Zwingli
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgMnS	prosadit
zejména	zejména	k9	zejména
na	na	k7c6	na
části	část	k1gFnSc6	část
frankofonního	frankofonní	k2eAgNnSc2d1	frankofonní
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
anglosaského	anglosaský	k2eAgInSc2d1	anglosaský
světa	svět	k1gInSc2	svět
a	a	k8xC	a
Nizozemí	Nizozemí	k1gNnSc2	Nizozemí
<g/>
.	.	kIx.	.
</s>
<s>
Anglikánství	anglikánství	k1gNnSc1	anglikánství
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
reformačního	reformační	k2eAgNnSc2d1	reformační
působení	působení	k1gNnSc2	působení
Jindřicha	Jindřich	k1gMnSc2	Jindřich
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
a	a	k8xC	a
jím	jíst	k5eAaImIp1nS	jíst
dosazeného	dosazený	k2eAgMnSc4d1	dosazený
arcibiskupa	arcibiskup	k1gMnSc4	arcibiskup
Thomase	Thomas	k1gMnSc4	Thomas
Cranmera	Cranmer	k1gMnSc4	Cranmer
na	na	k7c6	na
Britských	britský	k2eAgInPc6d1	britský
ostrovech	ostrov	k1gInPc6	ostrov
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
podnes	podnes	k6eAd1	podnes
státní	státní	k2eAgFnSc7d1	státní
církví	církev	k1gFnSc7	církev
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
těchto	tento	k3xDgInPc2	tento
hlavních	hlavní	k2eAgInPc2d1	hlavní
směrů	směr	k1gInPc2	směr
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
reformace	reformace	k1gFnSc2	reformace
již	již	k6eAd1	již
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
další	další	k2eAgFnSc2d1	další
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
novokřtěnci	novokřtěnec	k1gMnPc1	novokřtěnec
<g/>
,	,	kIx,	,
na	na	k7c4	na
něž	jenž	k3xRgInPc4	jenž
navazují	navazovat	k5eAaImIp3nP	navazovat
dnešní	dnešní	k2eAgMnPc1d1	dnešní
baptisté	baptista	k1gMnPc1	baptista
<g/>
,	,	kIx,	,
odmítající	odmítající	k2eAgInSc1d1	odmítající
křest	křest	k1gInSc1	křest
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
předchůdci	předchůdce	k1gMnPc7	předchůdce
dnešních	dnešní	k2eAgMnPc2d1	dnešní
unitářů	unitář	k1gMnPc2	unitář
<g/>
,	,	kIx,	,
popírající	popírající	k2eAgFnSc4d1	popírající
nauku	nauka	k1gFnSc4	nauka
o	o	k7c6	o
boží	boží	k2eAgFnSc6d1	boží
Trojici	trojice	k1gFnSc6	trojice
<g/>
.	.	kIx.	.
</s>
<s>
Novokřtěnci	novokřtěnec	k1gMnPc1	novokřtěnec
vytvářeli	vytvářet	k5eAaImAgMnP	vytvářet
vlastní	vlastní	k2eAgFnPc4d1	vlastní
uzavřené	uzavřený	k2eAgFnPc4d1	uzavřená
demokratické	demokratický	k2eAgFnPc4d1	demokratická
komunity	komunita	k1gFnPc4	komunita
<g/>
,	,	kIx,	,
pojila	pojit	k5eAaImAgFnS	pojit
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
víra	víra	k1gFnSc1	víra
v	v	k7c4	v
konec	konec	k1gInSc4	konec
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
cokoliv	cokoliv	k3yInSc4	cokoliv
světského	světský	k2eAgMnSc4d1	světský
bylo	být	k5eAaImAgNnS	být
označeno	označit	k5eAaPmNgNnS	označit
za	za	k7c4	za
špatné	špatný	k2eAgNnSc4d1	špatné
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
byli	být	k5eAaImAgMnP	být
církví	církev	k1gFnSc7	církev
tolerováni	tolerován	k2eAgMnPc1d1	tolerován
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
ale	ale	k8xC	ale
byli	být	k5eAaImAgMnP	být
prohlášeni	prohlásit	k5eAaPmNgMnP	prohlásit
za	za	k7c4	za
kacíře	kacíř	k1gMnPc4	kacíř
a	a	k8xC	a
začalo	začít	k5eAaPmAgNnS	začít
jejich	jejich	k3xOp3gNnSc1	jejich
brutální	brutální	k2eAgNnSc1d1	brutální
stíhání	stíhání	k1gNnSc1	stíhání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zejména	zejména	k9	zejména
v	v	k7c6	v
anglosaských	anglosaský	k2eAgFnPc6d1	anglosaská
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
z	z	k7c2	z
protestantských	protestantský	k2eAgFnPc2d1	protestantská
církví	církev	k1gFnPc2	církev
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
stoletích	století	k1gNnPc6	století
dále	daleko	k6eAd2	daleko
vydělovaly	vydělovat	k5eAaImAgInP	vydělovat
nové	nový	k2eAgInPc1d1	nový
směry	směr	k1gInPc1	směr
<g/>
,	,	kIx,	,
sekty	sekta	k1gFnPc1	sekta
<g/>
,	,	kIx,	,
církve	církev	k1gFnPc1	církev
a	a	k8xC	a
hnutí	hnutí	k1gNnPc1	hnutí
<g/>
,	,	kIx,	,
u	u	k7c2	u
jejichž	jejichž	k3xOyRp3gInPc2	jejichž
kořenů	kořen	k1gInPc2	kořen
často	často	k6eAd1	často
stál	stát	k5eAaImAgInS	stát
puritanismus	puritanismus	k1gInSc1	puritanismus
a	a	k8xC	a
probuzenecké	probuzenecký	k2eAgNnSc1d1	probuzenecké
hnutí	hnutí	k1gNnSc1	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Sem	sem	k6eAd1	sem
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
metodismus	metodismus	k1gInSc4	metodismus
<g/>
,	,	kIx,	,
letniční	letniční	k2eAgNnPc1d1	letniční
hnutí	hnutí	k1gNnPc1	hnutí
(	(	kIx(	(
<g/>
pentekostalismus	pentekostalismus	k1gInSc1	pentekostalismus
<g/>
)	)	kIx)	)
či	či	k8xC	či
evangelikalismus	evangelikalismus	k1gInSc1	evangelikalismus
(	(	kIx(	(
<g/>
protestantský	protestantský	k2eAgInSc1d1	protestantský
fundamentalismus	fundamentalismus	k1gInSc1	fundamentalismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgInP	vzniknout
ovšem	ovšem	k9	ovšem
i	i	k9	i
směry	směr	k1gInPc1	směr
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
tradičního	tradiční	k2eAgNnSc2d1	tradiční
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nejsou	být	k5eNaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
ostatními	ostatní	k2eAgFnPc7d1	ostatní
církvemi	církev	k1gFnPc7	církev
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
křesťanské	křesťanský	k2eAgNnSc4d1	křesťanské
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
mormonismus	mormonismus	k1gInSc1	mormonismus
a	a	k8xC	a
Svědkové	svědek	k1gMnPc1	svědek
Jehovovi	Jehovův	k2eAgMnPc1d1	Jehovův
<g/>
.	.	kIx.	.
</s>
<s>
Vznikaly	vznikat	k5eAaImAgFnP	vznikat
i	i	k9	i
menší	malý	k2eAgFnPc1d2	menší
<g/>
,	,	kIx,	,
okrajové	okrajový	k2eAgFnPc1d1	okrajová
náboženské	náboženský	k2eAgFnPc1d1	náboženská
skupiny	skupina	k1gFnPc1	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
doby	doba	k1gFnSc2	doba
vzestupu	vzestup	k1gInSc2	vzestup
protestanství	protestanství	k1gNnSc2	protestanství
zapadá	zapadat	k5eAaPmIp3nS	zapadat
počátek	počátek	k1gInSc4	počátek
tzv.	tzv.	kA	tzv.
nadkonfesního	nadkonfesní	k2eAgNnSc2d1	nadkonfesní
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
,	,	kIx,	,
přesvědčení	přesvědčení	k1gNnSc2	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemají	mít	k5eNaImIp3nP	mít
existovat	existovat	k5eAaImF	existovat
žádné	žádný	k3yNgFnPc4	žádný
církevní	církevní	k2eAgFnPc4d1	církevní
instituce	instituce	k1gFnPc4	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c4	o
globální	globální	k2eAgFnSc4d1	globální
církev	církev	k1gFnSc4	církev
bez	bez	k7c2	bez
jakékoliv	jakýkoliv	k3yIgFnSc2	jakýkoliv
vnější	vnější	k2eAgFnSc2d1	vnější
organizace	organizace	k1gFnSc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Přívrženci	přívrženec	k1gMnPc1	přívrženec
těchto	tento	k3xDgFnPc2	tento
myšlenek	myšlenka	k1gFnPc2	myšlenka
často	často	k6eAd1	často
neuznávají	uznávat	k5eNaImIp3nP	uznávat
světskou	světský	k2eAgFnSc4d1	světská
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
často	často	k6eAd1	často
obsaženy	obsažen	k2eAgInPc1d1	obsažen
anarchistické	anarchistický	k2eAgInPc1d1	anarchistický
prvky	prvek	k1gInPc1	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
jiné	jiný	k2eAgInPc4d1	jiný
protestantské	protestantský	k2eAgInPc4d1	protestantský
odnože	odnož	k1gInPc4	odnož
<g/>
,	,	kIx,	,
i	i	k8xC	i
zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
individuální	individuální	k2eAgInSc4d1	individuální
spiritualismus	spiritualismus	k1gInSc4	spiritualismus
<g/>
,	,	kIx,	,
víra	víra	k1gFnSc1	víra
je	být	k5eAaImIp3nS	být
soukromou	soukromý	k2eAgFnSc7d1	soukromá
záležitostí	záležitost	k1gFnSc7	záležitost
každého	každý	k3xTgMnSc2	každý
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
zastánce	zastánce	k1gMnSc4	zastánce
nadkonfesního	nadkonfesní	k2eAgNnSc2d1	nadkonfesní
křesťanství	křesťanství	k1gNnSc2	křesťanství
patřil	patřit	k5eAaImAgMnS	patřit
například	například	k6eAd1	například
lékař	lékař	k1gMnSc1	lékař
Paracelsus	Paracelsus	k1gMnSc1	Paracelsus
<g/>
,	,	kIx,	,
či	či	k8xC	či
teologové	teolog	k1gMnPc1	teolog
Valentin	Valentin	k1gMnSc1	Valentin
Weigel	Weigel	k1gMnSc1	Weigel
a	a	k8xC	a
Jakob	Jakob	k1gMnSc1	Jakob
Böhme	Böhm	k1gInSc5	Böhm
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
učení	učení	k1gNnSc1	učení
se	se	k3xPyFc4	se
ale	ale	k9	ale
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgFnPc2d1	jiná
protestantských	protestantský	k2eAgFnPc2d1	protestantská
odnoží	odnož	k1gFnPc2	odnož
příliš	příliš	k6eAd1	příliš
nešířilo	šířit	k5eNaImAgNnS	šířit
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgMnPc7d1	jednotlivý
zastánci	zastánce	k1gMnPc7	zastánce
panovaly	panovat	k5eAaImAgFnP	panovat
kolosální	kolosální	k2eAgInPc4d1	kolosální
rozdíly	rozdíl	k1gInPc4	rozdíl
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
jiných	jiný	k2eAgFnPc2d1	jiná
sekt	sekta	k1gFnPc2	sekta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Učení	učení	k1gNnSc2	učení
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
protestantismus	protestantismus	k1gInSc4	protestantismus
je	být	k5eAaImIp3nS	být
typický	typický	k2eAgInSc4d1	typický
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
Písmo	písmo	k1gNnSc4	písmo
jako	jako	k8xS	jako
zdroj	zdroj	k1gInSc4	zdroj
zjevené	zjevený	k2eAgFnSc2d1	zjevená
Pravdy	pravda	k1gFnSc2	pravda
<g/>
,	,	kIx,	,
vůči	vůči	k7c3	vůči
katolictví	katolictví	k1gNnSc3	katolictví
se	se	k3xPyFc4	se
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
mj.	mj.	kA	mj.
popřením	popření	k1gNnSc7	popření
církevní	církevní	k2eAgFnSc2d1	církevní
tradice	tradice	k1gFnSc2	tradice
jakožto	jakožto	k8xS	jakožto
dalšího	další	k2eAgInSc2d1	další
zdroje	zdroj	k1gInSc2	zdroj
zjevení	zjevení	k1gNnSc1	zjevení
rovnocenného	rovnocenný	k2eAgNnSc2d1	rovnocenné
Písmu	písmo	k1gNnSc6	písmo
(	(	kIx(	(
<g/>
Lutherova	Lutherův	k2eAgFnSc1d1	Lutherova
zásada	zásada	k1gFnSc1	zásada
sola	sola	k1gFnSc1	sola
scriptura	scriptura	k1gFnSc1	scriptura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
bodů	bod	k1gInPc2	bod
reformace	reformace	k1gFnSc2	reformace
bylo	být	k5eAaImAgNnS	být
vyloučit	vyloučit	k5eAaPmF	vyloučit
elementy	element	k1gInPc4	element
"	"	kIx"	"
<g/>
magičnosti	magičnost	k1gFnPc4	magičnost
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
z	z	k7c2	z
církevních	církevní	k2eAgInPc2d1	církevní
obřadů	obřad	k1gInPc2	obřad
a	a	k8xC	a
víry	víra	k1gFnSc2	víra
<g/>
,	,	kIx,	,
likvidace	likvidace	k1gFnSc1	likvidace
všech	všecek	k3xTgInPc2	všecek
elementů	element	k1gInPc2	element
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
přejala	přejmout	k5eAaPmAgFnS	přejmout
od	od	k7c2	od
starších	starý	k2eAgNnPc2d2	starší
náboženství	náboženství	k1gNnPc2	náboženství
v	v	k7c4	v
období	období	k1gNnSc4	období
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
<g/>
.	.	kIx.	.
</s>
<s>
Rituály	rituál	k1gInPc1	rituál
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
zasahovaly	zasahovat	k5eAaImAgFnP	zasahovat
kromě	kromě	k7c2	kromě
sakrálního	sakrální	k2eAgInSc2d1	sakrální
prostoru	prostor	k1gInSc2	prostor
(	(	kIx(	(
<g/>
např.	např.	kA	např.
kostel	kostel	k1gInSc1	kostel
<g/>
)	)	kIx)	)
i	i	k9	i
do	do	k7c2	do
běžného	běžný	k2eAgInSc2d1	běžný
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
čemuž	což	k3yRnSc3	což
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
protestantských	protestantský	k2eAgNnPc2d1	protestantské
odnoží	odnoží	k1gNnPc2	odnoží
snaží	snažit	k5eAaImIp3nS	snažit
zamezit	zamezit	k5eAaPmF	zamezit
<g/>
.	.	kIx.	.
</s>
<s>
Protestanté	protestant	k1gMnPc1	protestant
se	se	k3xPyFc4	se
odvracejí	odvracet	k5eAaImIp3nP	odvracet
od	od	k7c2	od
kultu	kult	k1gInSc2	kult
svatých	svatá	k1gFnPc2	svatá
<g/>
,	,	kIx,	,
nepřikládají	přikládat	k5eNaImIp3nP	přikládat
jim	on	k3xPp3gMnPc3	on
zdaleka	zdaleka	k6eAd1	zdaleka
takovou	takový	k3xDgFnSc4	takový
váhu	váha	k1gFnSc4	váha
jako	jako	k9	jako
katolíci	katolík	k1gMnPc1	katolík
<g/>
.	.	kIx.	.
</s>
<s>
Obzvlášť	obzvlášť	k6eAd1	obzvlášť
viditelný	viditelný	k2eAgInSc1d1	viditelný
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
rozdíl	rozdíl	k1gInSc1	rozdíl
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
protestanských	protestanský	k2eAgFnPc6d1	protestanská
církvích	církev	k1gFnPc6	církev
zcela	zcela	k6eAd1	zcela
upozaděna	upozaděn	k2eAgFnSc1d1	upozaděna
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
protestantů	protestant	k1gMnPc2	protestant
je	být	k5eAaImIp3nS	být
bohoslužba	bohoslužba	k1gFnSc1	bohoslužba
orientovaná	orientovaný	k2eAgFnSc1d1	orientovaná
na	na	k7c4	na
náboženské	náboženský	k2eAgNnSc4d1	náboženské
vědomí	vědomí	k1gNnSc4	vědomí
<g/>
,	,	kIx,	,
vede	vést	k5eAaImIp3nS	vést
se	se	k3xPyFc4	se
v	v	k7c6	v
národních	národní	k2eAgInPc6d1	národní
jazycích	jazyk	k1gInPc6	jazyk
<g/>
,	,	kIx,	,
společné	společný	k2eAgFnPc1d1	společná
modlitby	modlitba	k1gFnPc1	modlitba
a	a	k8xC	a
zpěvy	zpěv	k1gInPc1	zpěv
mají	mít	k5eAaImIp3nP	mít
vést	vést	k5eAaImF	vést
ke	k	k7c3	k
sjednocování	sjednocování	k1gNnSc3	sjednocování
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
rozdíl	rozdíl	k1gInSc1	rozdíl
s	s	k7c7	s
katolíky	katolík	k1gMnPc7	katolík
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zádušní	zádušní	k2eAgFnSc6d1	zádušní
bohoslužbě	bohoslužba	k1gFnSc6	bohoslužba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
katolické	katolický	k2eAgFnSc6d1	katolická
verzi	verze	k1gFnSc6	verze
se	se	k3xPyFc4	se
prosí	prosit	k5eAaImIp3nS	prosit
u	u	k7c2	u
Boha	bůh	k1gMnSc2	bůh
za	za	k7c4	za
spásu	spása	k1gFnSc4	spása
zemřelého	zemřelý	k1gMnSc2	zemřelý
<g/>
,	,	kIx,	,
v	v	k7c6	v
protestantské	protestantský	k2eAgFnSc6d1	protestantská
si	se	k3xPyFc3	se
lidé	člověk	k1gMnPc1	člověk
pouze	pouze	k6eAd1	pouze
připomínají	připomínat	k5eAaImIp3nP	připomínat
jeho	jeho	k3xOp3gInPc4	jeho
skutky	skutek	k1gInPc4	skutek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Církve	církev	k1gFnSc2	církev
==	==	k?	==
</s>
</p>
<p>
<s>
Protestantské	protestantský	k2eAgFnSc2d1	protestantská
církve	církev	k1gFnSc2	církev
je	být	k5eAaImIp3nS	být
souhrnné	souhrnný	k2eAgNnSc1d1	souhrnné
označení	označení	k1gNnSc1	označení
církví	církev	k1gFnPc2	církev
vzešlých	vzešlý	k2eAgFnPc2d1	vzešlá
z	z	k7c2	z
reformace	reformace	k1gFnSc2	reformace
a	a	k8xC	a
z	z	k7c2	z
následných	následný	k2eAgNnPc2d1	následné
duchovních	duchovní	k2eAgNnPc2d1	duchovní
hnutí	hnutí	k1gNnPc2	hnutí
uvnitř	uvnitř	k7c2	uvnitř
těchto	tento	k3xDgFnPc2	tento
církví	církev	k1gFnPc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Církve	církev	k1gFnPc1	církev
čerpající	čerpající	k2eAgFnPc1d1	čerpající
primárně	primárně	k6eAd1	primárně
z	z	k7c2	z
Lutherova	Lutherův	k2eAgInSc2d1	Lutherův
odkazu	odkaz	k1gInSc2	odkaz
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
luteráni	luterán	k1gMnPc1	luterán
(	(	kIx(	(
<g/>
Augsburské	augsburský	k2eAgNnSc1d1	Augsburské
vyznání	vyznání	k1gNnSc1	vyznání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protestanté	protestant	k1gMnPc1	protestant
navazující	navazující	k2eAgFnSc2d1	navazující
zejména	zejména	k9	zejména
na	na	k7c4	na
odkaz	odkaz	k1gInSc4	odkaz
Jana	Jan	k1gMnSc2	Jan
Kalvína	Kalvín	k1gMnSc2	Kalvín
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
kalvinisté	kalvinista	k1gMnPc1	kalvinista
nebo	nebo	k8xC	nebo
reformovaní	reformovaný	k2eAgMnPc1d1	reformovaný
(	(	kIx(	(
<g/>
Helvetské	helvetský	k2eAgNnSc1d1	Helvetské
vyznání	vyznání	k1gNnSc1	vyznání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
protestantismu	protestantismus	k1gInSc3	protestantismus
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
i	i	k9	i
anglikánská	anglikánský	k2eAgFnSc1d1	anglikánská
církev	církev	k1gFnSc1	církev
a	a	k8xC	a
menší	malý	k2eAgFnPc1d2	menší
evangelikální	evangelikální	k2eAgFnPc1d1	evangelikální
církve	církev	k1gFnPc1	církev
<g/>
,	,	kIx,	,
např.	např.	kA	např.
adventisté	adventista	k1gMnPc1	adventista
<g/>
,	,	kIx,	,
baptisté	baptista	k1gMnPc1	baptista
<g/>
,	,	kIx,	,
metodisté	metodista	k1gMnPc1	metodista
<g/>
,	,	kIx,	,
presbyteriáni	presbyterián	k1gMnPc1	presbyterián
<g/>
,	,	kIx,	,
puritáni	puritán	k1gMnPc1	puritán
<g/>
,	,	kIx,	,
mennonité	mennonitý	k2eAgFnPc1d1	mennonitý
<g/>
,	,	kIx,	,
letniční	letniční	k2eAgFnSc1d1	letniční
a	a	k8xC	a
podstatná	podstatný	k2eAgFnSc1d1	podstatná
část	část	k1gFnSc1	část
charismatiků	charismatik	k1gMnPc2	charismatik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Protestanté	protestant	k1gMnPc1	protestant
je	být	k5eAaImIp3nS	být
společný	společný	k2eAgInSc4d1	společný
název	název	k1gInSc4	název
pro	pro	k7c4	pro
členy	člen	k1gMnPc4	člen
těchto	tento	k3xDgFnPc2	tento
církví	církev	k1gFnPc2	církev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Někdy	někdy	k6eAd1	někdy
bývá	bývat	k5eAaImIp3nS	bývat
pojem	pojem	k1gInSc1	pojem
protestanté	protestant	k1gMnPc1	protestant
ztotožňován	ztotožňovat	k5eAaImNgMnS	ztotožňovat
s	s	k7c7	s
pojmem	pojem	k1gInSc7	pojem
evangelíci	evangelík	k1gMnPc1	evangelík
<g/>
;	;	kIx,	;
většinově	většinově	k6eAd1	většinově
je	být	k5eAaImIp3nS	být
však	však	k9	však
pojem	pojem	k1gInSc1	pojem
evangelíci	evangelík	k1gMnPc1	evangelík
chápán	chápat	k5eAaImNgMnS	chápat
úžeji	úzko	k6eAd2	úzko
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vyhrazen	vyhradit	k5eAaPmNgInS	vyhradit
jen	jen	k9	jen
pro	pro	k7c4	pro
nejstarší	starý	k2eAgInPc4d3	nejstarší
protestantské	protestantský	k2eAgInPc4d1	protestantský
proudy	proud	k1gInPc4	proud
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
pro	pro	k7c4	pro
luterány	luterán	k1gMnPc4	luterán
<g/>
,	,	kIx,	,
kalvinisty	kalvinista	k1gMnPc4	kalvinista
a	a	k8xC	a
případně	případně	k6eAd1	případně
metodisty	metodista	k1gMnPc7	metodista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Protestantské	protestantský	k2eAgFnSc2d1	protestantská
církve	církev	k1gFnSc2	církev
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
==	==	k?	==
</s>
</p>
<p>
<s>
Českobratrská	českobratrský	k2eAgFnSc1d1	Českobratrská
církev	církev	k1gFnSc1	církev
evangelická	evangelický	k2eAgFnSc1d1	evangelická
–	–	k?	–
největší	veliký	k2eAgFnSc1d3	veliký
česká	český	k2eAgFnSc1d1	Česká
protestantská	protestantský	k2eAgFnSc1d1	protestantská
církev	církev	k1gFnSc1	církev
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
sloučením	sloučení	k1gNnSc7	sloučení
luteránské	luteránský	k2eAgMnPc4d1	luteránský
(	(	kIx(	(
<g/>
augsburské	augsburský	k2eAgMnPc4d1	augsburský
<g/>
)	)	kIx)	)
a	a	k8xC	a
kalvínské	kalvínský	k2eAgFnPc1d1	kalvínská
(	(	kIx(	(
<g/>
reformované	reformovaný	k2eAgFnPc1d1	reformovaná
<g/>
,	,	kIx,	,
helvétské	helvétský	k2eAgFnPc1d1	helvétská
<g/>
)	)	kIx)	)
církve	církev	k1gFnPc1	církev
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgFnSc4d1	vlastní
fakultu	fakulta	k1gFnSc4	fakulta
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
Karlově	Karlův	k2eAgFnSc6d1	Karlova
–	–	k?	–
Evangelickou	evangelický	k2eAgFnSc4d1	evangelická
teologickou	teologický	k2eAgFnSc4d1	teologická
fakultu	fakulta	k1gFnSc4	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednota	jednota	k1gFnSc1	jednota
bratrská	bratrský	k2eAgFnSc1d1	bratrská
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
též	též	k9	též
Moravští	moravský	k2eAgMnPc1d1	moravský
bratří	bratr	k1gMnPc1	bratr
nebo	nebo	k8xC	nebo
Unitas	Unitas	k1gMnSc1	Unitas
fratrum	fratrum	k1gNnSc1	fratrum
–	–	k?	–
nejstarší	starý	k2eAgFnSc1d3	nejstarší
česká	český	k2eAgFnSc1d1	Česká
protestantská	protestantský	k2eAgFnSc1d1	protestantská
církev	církev	k1gFnSc1	církev
<g/>
;	;	kIx,	;
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1457	[number]	k4	1457
(	(	kIx(	(
<g/>
Kunvald	Kunvald	k1gInSc1	Kunvald
u	u	k7c2	u
Žamberka	Žamberka	k1gFnSc1	Žamberka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
celosvětově	celosvětově	k6eAd1	celosvětově
obnovena	obnovit	k5eAaPmNgNnP	obnovit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1727	[number]	k4	1727
hrabětem	hrabě	k1gMnSc7	hrabě
Zinzendorfem	Zinzendorf	k1gMnSc7	Zinzendorf
a	a	k8xC	a
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1880	[number]	k4	1880
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Evangelická	evangelický	k2eAgFnSc1d1	evangelická
bratrská	bratrský	k2eAgFnSc1d1	bratrská
církev	církev	k1gFnSc1	církev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Luteránské	luteránský	k2eAgFnSc2d1	luteránská
církve	církev	k1gFnSc2	církev
===	===	k?	===
</s>
</p>
<p>
<s>
Slezská	slezský	k2eAgFnSc1d1	Slezská
církev	církev	k1gFnSc1	církev
evangelická	evangelický	k2eAgFnSc1d1	evangelická
augsburského	augsburský	k2eAgNnSc2d1	Augsburské
vyznání	vyznání	k1gNnPc2	vyznání
</s>
</p>
<p>
<s>
Luterská	luterský	k2eAgFnSc1d1	luterská
evangelická	evangelický	k2eAgFnSc1d1	evangelická
církev	církev	k1gFnSc1	církev
a.	a.	k?	a.
v.	v.	k?	v.
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
</s>
</p>
<p>
<s>
Evangelická	evangelický	k2eAgFnSc1d1	evangelická
církev	církev	k1gFnSc1	církev
augsburského	augsburský	k2eAgNnSc2d1	Augsburské
vyznání	vyznání	k1gNnSc2	vyznání
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
</s>
</p>
<p>
<s>
===	===	k?	===
Původem	původ	k1gInSc7	původ
anglosaské	anglosaský	k2eAgFnSc2d1	anglosaská
církve	církev	k1gFnSc2	církev
===	===	k?	===
</s>
</p>
<p>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
církve	církev	k1gFnPc4	církev
původem	původ	k1gInSc7	původ
vzniklé	vzniklý	k2eAgNnSc4d1	vzniklé
jako	jako	k8xC	jako
puritánské	puritánský	k2eAgNnSc4d1	puritánské
disidentské	disidentský	k2eAgNnSc4d1	disidentské
hnutí	hnutí	k1gNnSc4	hnutí
v	v	k7c6	v
anglikánské	anglikánský	k2eAgFnSc6d1	anglikánská
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
v	v	k7c6	v
USA	USA	kA	USA
zvané	zvaný	k2eAgFnSc6d1	zvaná
episkopální	episkopální	k2eAgFnSc6d1	episkopální
<g/>
)	)	kIx)	)
církvi	církev	k1gFnSc3	církev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Církev	církev	k1gFnSc1	církev
bratrská	bratrský	k2eAgFnSc1d1	bratrská
–	–	k?	–
reformovaná	reformovaný	k2eAgFnSc1d1	reformovaná
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
spojením	spojení	k1gNnSc7	spojení
domácího	domácí	k2eAgNnSc2d1	domácí
probuzeneckého	probuzenecký	k2eAgNnSc2d1	probuzenecké
hnutí	hnutí	k1gNnSc2	hnutí
a	a	k8xC	a
misijní	misijní	k2eAgFnSc2d1	misijní
činnosti	činnost	k1gFnSc2	činnost
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
misionářů	misionář	k1gMnPc2	misionář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bratrská	bratrský	k2eAgFnSc1d1	bratrská
jednota	jednota	k1gFnSc1	jednota
baptistů	baptista	k1gMnPc2	baptista
–	–	k?	–
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c6	na
novokřtěnce	novokřtěnka	k1gFnSc6	novokřtěnka
<g/>
;	;	kIx,	;
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1611	[number]	k4	1611
<g/>
,	,	kIx,	,
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Evangelická	evangelický	k2eAgFnSc1d1	evangelická
církev	církev	k1gFnSc1	církev
metodistická	metodistický	k2eAgFnSc1d1	metodistická
–	–	k?	–
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1739	[number]	k4	1739
<g/>
,	,	kIx,	,
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
po	po	k7c4	po
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Armáda	armáda	k1gFnSc1	armáda
spásy	spása	k1gFnSc2	spása
–	–	k?	–
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
z	z	k7c2	z
metodistické	metodistický	k2eAgFnSc2d1	metodistická
církve	církev	k1gFnSc2	církev
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1865	[number]	k4	1865
<g/>
,	,	kIx,	,
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
po	po	k7c4	po
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Církev	církev	k1gFnSc1	církev
adventistů	adventista	k1gMnPc2	adventista
sedmého	sedmý	k4xOgInSc2	sedmý
dne	den	k1gInSc2	den
–	–	k?	–
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1863	[number]	k4	1863
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
její	její	k3xOp3gInPc1	její
počátky	počátek	k1gInPc1	počátek
se	se	k3xPyFc4	se
datují	datovat	k5eAaImIp3nP	datovat
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1830	[number]	k4	1830
<g/>
;	;	kIx,	;
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc1	první
literatura	literatura	k1gFnSc1	literatura
vydaná	vydaný	k2eAgFnSc1d1	vydaná
česky	česky	k6eAd1	česky
roku	rok	k1gInSc2	rok
1893	[number]	k4	1893
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
sbor	sbor	k1gInSc4	sbor
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Křesťanské	křesťanský	k2eAgInPc1d1	křesťanský
sbory	sbor	k1gInPc1	sbor
(	(	kIx(	(
<g/>
plymouthští	plymouthštit	k5eAaPmIp3nP	plymouthštit
bratří	bratr	k1gMnPc1	bratr
<g/>
)	)	kIx)	)
–	–	k?	–
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1831	[number]	k4	1831
<g/>
,	,	kIx,	,
na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Apoštolská	apoštolský	k2eAgFnSc1d1	apoštolská
církev	církev	k1gFnSc1	církev
–	–	k?	–
letniční	letniční	k2eAgNnSc4d1	letniční
hnutí	hnutí	k1gNnSc4	hnutí
<g/>
,	,	kIx,	,
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
i	i	k8xC	i
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Křesťanské	křesťanský	k2eAgNnSc1d1	křesťanské
společenství	společenství	k1gNnSc1	společenství
–	–	k?	–
charismatické	charismatický	k2eAgNnSc4d1	charismatické
hnutí	hnutí	k1gNnSc4	hnutí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
oddělilo	oddělit	k5eAaPmAgNnS	oddělit
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
od	od	k7c2	od
českobratrské	českobratrský	k2eAgFnSc2d1	Českobratrská
evangelické	evangelický	k2eAgFnSc2d1	evangelická
církve	církev	k1gFnSc2	církev
příklonem	příklon	k1gInSc7	příklon
k	k	k7c3	k
některým	některý	k3yIgFnPc3	některý
letničním	letniční	k2eAgFnPc3d1	letniční
myšlenkám	myšlenka	k1gFnPc3	myšlenka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
církev	církev	k1gFnSc1	církev
Cesta	cesta	k1gFnSc1	cesta
života	život	k1gInSc2	život
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
<g/>
,	,	kIx,	,
státem	stát	k1gInSc7	stát
neregistrovaná	registrovaný	k2eNgFnSc1d1	neregistrovaná
<g/>
,	,	kIx,	,
charizmatická	charizmatický	k2eAgFnSc1d1	charizmatická
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
působící	působící	k2eAgInSc1d1	působící
v	v	k7c6	v
Jižních	jižní	k2eAgFnPc6d1	jižní
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
;	;	kIx,	;
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
odštěpením	odštěpení	k1gNnSc7	odštěpení
od	od	k7c2	od
Církve	církev	k1gFnSc2	církev
bratrské	bratrský	k2eAgFnSc2d1	bratrská
</s>
</p>
<p>
<s>
===	===	k?	===
Ostatní	ostatní	k2eAgFnPc1d1	ostatní
církve	církev	k1gFnPc1	církev
hlavního	hlavní	k2eAgInSc2d1	hlavní
proudu	proud	k1gInSc2	proud
křesťanství	křesťanství	k1gNnSc2	křesťanství
===	===	k?	===
</s>
</p>
<p>
<s>
Církev	církev	k1gFnSc1	církev
československá	československý	k2eAgFnSc1d1	Československá
husitská	husitský	k2eAgFnSc1d1	husitská
–	–	k?	–
považuje	považovat	k5eAaImIp3nS	považovat
se	se	k3xPyFc4	se
za	za	k7c4	za
dědičku	dědička	k1gFnSc4	dědička
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc2	první
<g/>
)	)	kIx)	)
České	český	k2eAgFnSc2d1	Česká
reformace	reformace	k1gFnSc2	reformace
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
odtržením	odtržení	k1gNnSc7	odtržení
od	od	k7c2	od
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgFnSc4d1	vlastní
fakultu	fakulta	k1gFnSc4	fakulta
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
Karlově	Karlův	k2eAgFnSc6d1	Karlova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Náboženské	náboženský	k2eAgFnPc1d1	náboženská
společnosti	společnost	k1gFnPc1	společnost
s	s	k7c7	s
reformačními	reformační	k2eAgInPc7d1	reformační
kořeny	kořen	k1gInPc7	kořen
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
obecně	obecně	k6eAd1	obecně
řazeny	řadit	k5eAaImNgFnP	řadit
mezi	mezi	k7c4	mezi
křesťanské	křesťanský	k2eAgFnPc4d1	křesťanská
církve	církev	k1gFnPc4	církev
===	===	k?	===
</s>
</p>
<p>
<s>
I	i	k9	i
když	když	k8xS	když
tyto	tento	k3xDgFnPc1	tento
náboženské	náboženský	k2eAgFnPc1d1	náboženská
společnosti	společnost	k1gFnPc1	společnost
svou	svůj	k3xOyFgFnSc4	svůj
víru	víra	k1gFnSc4	víra
od	od	k7c2	od
křesťanství	křesťanství	k1gNnSc2	křesťanství
a	a	k8xC	a
reformace	reformace	k1gFnSc2	reformace
odvozují	odvozovat	k5eAaImIp3nP	odvozovat
<g/>
,	,	kIx,	,
nauky	nauka	k1gFnPc4	nauka
těchto	tento	k3xDgFnPc2	tento
náboženských	náboženský	k2eAgFnPc2d1	náboženská
společností	společnost	k1gFnPc2	společnost
se	se	k3xPyFc4	se
podstatně	podstatně	k6eAd1	podstatně
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
některých	některý	k3yIgInPc2	některý
bodů	bod	k1gInPc2	bod
Apoštolského	apoštolský	k2eAgNnSc2d1	apoštolské
vyznání	vyznání	k1gNnSc2	vyznání
víry	víra	k1gFnSc2	víra
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
tradičními	tradiční	k2eAgFnPc7d1	tradiční
církvemi	církev	k1gFnPc7	církev
uznáváno	uznávat	k5eAaImNgNnS	uznávat
jako	jako	k8xS	jako
základní	základní	k2eAgFnSc2d1	základní
definice	definice	k1gFnSc2	definice
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
těchto	tento	k3xDgFnPc2	tento
náboženských	náboženský	k2eAgFnPc2d1	náboženská
společností	společnost	k1gFnPc2	společnost
proto	proto	k8xC	proto
nejsou	být	k5eNaImIp3nP	být
tradičními	tradiční	k2eAgFnPc7d1	tradiční
církvemi	církev	k1gFnPc7	církev
počítáni	počítat	k5eAaImNgMnP	počítat
mezi	mezi	k7c7	mezi
křesťany	křesťan	k1gMnPc7	křesťan
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
se	se	k3xPyFc4	se
za	za	k7c4	za
ně	on	k3xPp3gInPc4	on
sami	sám	k3xTgMnPc1	sám
považují	považovat	k5eAaImIp3nP	považovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Novoapoštolská	Novoapoštolský	k2eAgFnSc1d1	Novoapoštolská
církev	církev	k1gFnSc1	církev
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1863	[number]	k4	1863
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Církev	církev	k1gFnSc1	církev
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
Svatých	svatý	k1gMnPc2	svatý
posledních	poslední	k2eAgInPc2d1	poslední
dnů	den	k1gInPc2	den
(	(	kIx(	(
<g/>
mormoni	mormon	k1gMnPc1	mormon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1830	[number]	k4	1830
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Náboženská	náboženský	k2eAgFnSc1d1	náboženská
společnost	společnost	k1gFnSc1	společnost
Svědkové	svědek	k1gMnPc5	svědek
Jehovovi	Jehova	k1gMnSc3	Jehova
</s>
</p>
<p>
<s>
Náboženská	náboženský	k2eAgFnSc1d1	náboženská
společnost	společnost	k1gFnSc1	společnost
českých	český	k2eAgMnPc2d1	český
unitářů	unitář	k1gMnPc2	unitář
</s>
</p>
<p>
<s>
===	===	k?	===
Představitelé	představitel	k1gMnPc1	představitel
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
protestantského	protestantský	k2eAgNnSc2d1	protestantské
vyznání	vyznání	k1gNnSc2	vyznání
===	===	k?	===
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
1378	[number]	k4	1378
<g/>
-	-	kIx~	-
<g/>
1419	[number]	k4	1419
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
(	(	kIx(	(
<g/>
1458	[number]	k4	1458
<g/>
-	-	kIx~	-
<g/>
1471	[number]	k4	1471
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Fridrich	Fridrich	k1gMnSc1	Fridrich
Falcký	falcký	k2eAgMnSc1d1	falcký
(	(	kIx(	(
<g/>
1619	[number]	k4	1619
<g/>
-	-	kIx~	-
<g/>
1620	[number]	k4	1620
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigu	k1gFnSc2	Garrigu
Masaryk	Masaryk	k1gMnSc1	Masaryk
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
-	-	kIx~	-
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
-	-	kIx~	-
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Ariánství	ariánství	k1gNnSc1	ariánství
</s>
</p>
<p>
<s>
Protestantská	protestantský	k2eAgFnSc1d1	protestantská
etika	etika	k1gFnSc1	etika
a	a	k8xC	a
duch	duch	k1gMnSc1	duch
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
</s>
</p>
