<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Monaka	Monako	k1gNnSc2	Monako
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
vodorovné	vodorovný	k2eAgInPc4d1	vodorovný
pruhy	pruh	k1gInPc4	pruh
<g/>
,	,	kIx,	,
červený	červený	k2eAgInSc1d1	červený
a	a	k8xC	a
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
barvy	barva	k1gFnPc1	barva
jsou	být	k5eAaImIp3nP	být
odvozené	odvozený	k2eAgInPc4d1	odvozený
z	z	k7c2	z
barev	barva	k1gFnPc2	barva
znaku	znak	k1gInSc2	znak
knížat	kníže	k1gMnPc2wR	kníže
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Grimaldi	Grimald	k1gMnPc1	Grimald
<g/>
,	,	kIx,	,
známých	známý	k2eAgFnPc2d1	známá
už	už	k6eAd1	už
roku	rok	k1gInSc2	rok
1339	[number]	k4	1339
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
roku	rok	k1gInSc2	rok
1815	[number]	k4	1815
<g/>
,	,	kIx,	,
definitivně	definitivně	k6eAd1	definitivně
schválená	schválený	k2eAgFnSc1d1	schválená
roku	rok	k1gInSc2	rok
1881	[number]	k4	1881
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
svým	svůj	k3xOyFgInSc7	svůj
vzhledem	vzhled	k1gInSc7	vzhled
totožná	totožný	k2eAgFnSc1d1	totožná
s	s	k7c7	s
vlajkou	vlajka	k1gFnSc7	vlajka
indonéskou	indonéský	k2eAgFnSc7d1	Indonéská
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
jejímuž	jejíž	k3xOyRp3gNnSc3	jejíž
zavedení	zavedení	k1gNnSc3	zavedení
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
Monako	Monako	k1gNnSc1	Monako
protestovalo	protestovat	k5eAaBmAgNnS	protestovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Rerefence	Rerefenka	k1gFnSc6	Rerefenka
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Vlajka	vlajka	k1gFnSc1	vlajka
Monaka	Monako	k1gNnPc4	Monako
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Monaka	Monako	k1gNnSc2	Monako
</s>
</p>
<p>
<s>
Monacká	monacký	k2eAgFnSc1d1	monacká
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Monacká	monacký	k2eAgFnSc1d1	monacká
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
