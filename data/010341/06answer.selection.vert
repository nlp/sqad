<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Monaka	Monako	k1gNnSc2	Monako
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
vodorovné	vodorovný	k2eAgInPc4d1	vodorovný
pruhy	pruh	k1gInPc4	pruh
<g/>
,	,	kIx,	,
červený	červený	k2eAgInSc1d1	červený
a	a	k8xC	a
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
barvy	barva	k1gFnPc1	barva
jsou	být	k5eAaImIp3nP	být
odvozené	odvozený	k2eAgInPc4d1	odvozený
z	z	k7c2	z
barev	barva	k1gFnPc2	barva
znaku	znak	k1gInSc2	znak
knížat	kníže	k1gMnPc2wR	kníže
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Grimaldi	Grimald	k1gMnPc1	Grimald
<g/>
,	,	kIx,	,
známých	známý	k2eAgFnPc2d1	známá
už	už	k6eAd1	už
roku	rok	k1gInSc2	rok
1339	[number]	k4	1339
<g/>
.	.	kIx.	.
</s>
