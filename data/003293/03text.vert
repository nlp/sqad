<s>
Iron	iron	k1gInSc1	iron
Maiden	Maidna	k1gFnPc2	Maidna
je	být	k5eAaImIp3nS	být
anglická	anglický	k2eAgFnSc1d1	anglická
heavymetalová	heavymetalový	k2eAgFnSc1d1	heavymetalová
skupina	skupina	k1gFnSc1	skupina
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
baskytaristou	baskytarista	k1gMnSc7	baskytarista
Stevem	Steve	k1gMnSc7	Steve
Harrisem	Harris	k1gInSc7	Harris
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
legendární	legendární	k2eAgFnSc1d1	legendární
nejen	nejen	k6eAd1	nejen
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
hudbě	hudba	k1gFnSc3	hudba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
maskotovi	maskotův	k2eAgMnPc1d1	maskotův
Eddiemu	Eddiema	k1gFnSc4	Eddiema
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
na	na	k7c6	na
obalech	obal	k1gInPc6	obal
všech	všecek	k3xTgNnPc2	všecek
alb	album	k1gNnPc2	album
a	a	k8xC	a
singlů	singl	k1gInPc2	singl
kapely	kapela	k1gFnSc2	kapela
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
prvního	první	k4xOgInSc2	první
singlu	singl	k1gInSc2	singl
"	"	kIx"	"
<g/>
Running	Running	k1gInSc1	Running
Free	Fre	k1gInSc2	Fre
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2010	[number]	k4	2010
prodali	prodat	k5eAaPmAgMnP	prodat
přes	přes	k7c4	přes
95	[number]	k4	95
milionů	milion	k4xCgInPc2	milion
alb	alba	k1gFnPc2	alba
a	a	k8xC	a
dostali	dostat	k5eAaPmAgMnP	dostat
jednu	jeden	k4xCgFnSc4	jeden
cenu	cena	k1gFnSc4	cena
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
.	.	kIx.	.
</s>
<s>
Získali	získat	k5eAaPmAgMnP	získat
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgFnPc2d1	další
cen	cena	k1gFnPc2	cena
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yIgInPc4	který
patří	patřit	k5eAaImIp3nS	patřit
Ivor	Ivor	k1gInSc1	Ivor
Novello	Novello	k1gNnSc1	Novello
Awards	Awards	k1gInSc1	Awards
<g/>
,	,	kIx,	,
Juno	Juno	k1gFnSc1	Juno
Award	Award	k1gMnSc1	Award
či	či	k8xC	či
Brit	Brit	k1gMnSc1	Brit
Awards	Awardsa	k1gFnPc2	Awardsa
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
skupina	skupina	k1gFnSc1	skupina
vydala	vydat	k5eAaPmAgFnS	vydat
celkem	celkem	k6eAd1	celkem
šestnáct	šestnáct	k4xCc4	šestnáct
studiových	studiový	k2eAgNnPc2d1	studiové
alb	album	k1gNnPc2	album
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yQgMnPc6	který
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
obdobích	období	k1gNnPc6	období
zpívali	zpívat	k5eAaImAgMnP	zpívat
tři	tři	k4xCgMnPc1	tři
zpěváci	zpěvák	k1gMnPc1	zpěvák
−	−	k?	−
Paul	Paul	k1gMnSc1	Paul
Di	Di	k1gMnSc1	Di
<g/>
'	'	kIx"	'
<g/>
Anno	Anna	k1gFnSc5	Anna
(	(	kIx(	(
<g/>
dvě	dva	k4xCgNnPc4	dva
alba	album	k1gNnPc4	album
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bruce	Bruce	k1gMnSc1	Bruce
Dickinson	Dickinson	k1gInSc1	Dickinson
(	(	kIx(	(
<g/>
dvanáct	dvanáct	k4xCc4	dvanáct
alb	album	k1gNnPc2	album
<g/>
)	)	kIx)	)
a	a	k8xC	a
Blaze	blaze	k6eAd1	blaze
Bayley	Bayle	k2eAgInPc1d1	Bayle
(	(	kIx(	(
<g/>
dvě	dva	k4xCgNnPc4	dva
alba	album	k1gNnPc4	album
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgMnSc7d1	jediný
stálým	stálý	k2eAgMnSc7d1	stálý
členem	člen	k1gMnSc7	člen
skupiny	skupina	k1gFnSc2	skupina
zůstal	zůstat	k5eAaPmAgMnS	zůstat
Harris	Harris	k1gInSc4	Harris
<g/>
,	,	kIx,	,
na	na	k7c6	na
všech	všecek	k3xTgNnPc6	všecek
albech	album	k1gNnPc6	album
se	se	k3xPyFc4	se
však	však	k9	však
podílel	podílet	k5eAaImAgMnS	podílet
i	i	k9	i
kytarista	kytarista	k1gMnSc1	kytarista
Dave	Dav	k1gInSc5	Dav
Murray	Murray	k1gInPc5	Murray
<g/>
.	.	kIx.	.
</s>
<s>
Nejdelší	dlouhý	k2eAgNnSc1d3	nejdelší
období	období	k1gNnSc1	období
skupina	skupina	k1gFnSc1	skupina
odehrála	odehrát	k5eAaPmAgFnS	odehrát
s	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
kytaristy	kytarista	k1gMnPc7	kytarista
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
Adrian	Adrian	k1gMnSc1	Adrian
Smith	Smith	k1gMnSc1	Smith
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
hrají	hrát	k5eAaImIp3nP	hrát
tři	tři	k4xCgFnPc4	tři
−	−	k?	−
Murray	Murraa	k1gFnPc4	Murraa
<g/>
,	,	kIx,	,
Smith	Smith	k1gMnSc1	Smith
a	a	k8xC	a
Janick	Janick	k1gMnSc1	Janick
Gers	Gers	k1gInSc1	Gers
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
Smithe	Smithe	k1gInSc1	Smithe
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
nahradil	nahradit	k5eAaPmAgMnS	nahradit
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
a	a	k8xC	a
pilířem	pilíř	k1gInSc7	pilíř
pro	pro	k7c4	pro
Iron	iron	k1gInSc4	iron
Maiden	Maidna	k1gFnPc2	Maidna
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gMnSc1	její
baskytarista	baskytarista	k1gMnSc1	baskytarista
Steve	Steve	k1gMnSc1	Steve
Harris	Harris	k1gFnSc1	Harris
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mladých	mladý	k2eAgNnPc6d1	mladé
letech	léto	k1gNnPc6	léto
obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
skupiny	skupina	k1gFnSc2	skupina
jako	jako	k8xC	jako
Genesis	Genesis	k1gFnSc1	Genesis
nebo	nebo	k8xC	nebo
Jethro	Jethra	k1gFnSc5	Jethra
Tull	Tulla	k1gFnPc2	Tulla
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	on	k3xPp3gInSc4	on
první	první	k4xOgInSc4	první
nástroj	nástroj	k1gInSc4	nástroj
byla	být	k5eAaImAgFnS	být
akustická	akustický	k2eAgFnSc1d1	akustická
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
si	se	k3xPyFc3	se
však	však	k9	však
pořídil	pořídit	k5eAaPmAgMnS	pořídit
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
baskytaru	baskytara	k1gFnSc4	baskytara
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
skupinu	skupina	k1gFnSc4	skupina
s	s	k7c7	s
názvem	název	k1gInSc7	název
Influence	influence	k1gFnSc2	influence
založil	založit	k5eAaPmAgInS	založit
počátkem	počátkem	k7c2	počátkem
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
kamarádem	kamarád	k1gMnSc7	kamarád
<g/>
,	,	kIx,	,
kytaristou	kytarista	k1gMnSc7	kytarista
Davem	Dav	k1gInSc7	Dav
Smithem	Smith	k1gInSc7	Smith
<g/>
.	.	kIx.	.
</s>
<s>
Sestavu	sestava	k1gFnSc4	sestava
doplnil	doplnit	k5eAaPmAgMnS	doplnit
zpěvák	zpěvák	k1gMnSc1	zpěvák
Bob	Bob	k1gMnSc1	Bob
Verschoile	Verschoila	k1gFnSc6	Verschoila
<g/>
,	,	kIx,	,
bubeník	bubeník	k1gMnSc1	bubeník
Paul	Paul	k1gMnSc1	Paul
Sears	Sears	k1gInSc1	Sears
a	a	k8xC	a
zpěvák	zpěvák	k1gMnSc1	zpěvák
Tim	Tim	k?	Tim
<g/>
.	.	kIx.	.
</s>
<s>
Hráli	hrát	k5eAaImAgMnP	hrát
například	například	k6eAd1	například
převzaté	převzatý	k2eAgFnPc4d1	převzatá
skladby	skladba	k1gFnPc4	skladba
od	od	k7c2	od
Free	Fre	k1gFnSc2	Fre
a	a	k8xC	a
The	The	k1gFnSc2	The
Who	Who	k1gFnSc2	Who
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
vlastní	vlastní	k2eAgMnSc1d1	vlastní
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Influence	influence	k1gFnSc2	influence
odehráli	odehrát	k5eAaPmAgMnP	odehrát
pouhý	pouhý	k2eAgInSc4d1	pouhý
jeden	jeden	k4xCgInSc4	jeden
koncert	koncert	k1gInSc4	koncert
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
jej	on	k3xPp3gMnSc4	on
změnili	změnit	k5eAaPmAgMnP	změnit
na	na	k7c4	na
Gypsy	gyps	k1gInPc4	gyps
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Kiss	Kissa	k1gFnPc2	Kissa
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
však	však	k9	však
po	po	k7c6	po
několika	několik	k4yIc6	několik
dalších	další	k2eAgInPc6d1	další
koncertech	koncert	k1gInPc6	koncert
úplně	úplně	k6eAd1	úplně
vytratila	vytratit	k5eAaPmAgFnS	vytratit
<g/>
.	.	kIx.	.
</s>
<s>
Harris	Harris	k1gInSc1	Harris
následně	následně	k6eAd1	následně
přešel	přejít	k5eAaPmAgInS	přejít
do	do	k7c2	do
již	již	k6eAd1	již
zavedené	zavedený	k2eAgFnSc2d1	zavedená
kapely	kapela	k1gFnSc2	kapela
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
Smiler	Smiler	k1gInSc4	Smiler
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
koncertech	koncert	k1gInPc6	koncert
vyměnili	vyměnit	k5eAaPmAgMnP	vyměnit
stávajícího	stávající	k2eAgMnSc4d1	stávající
bubeníka	bubeník	k1gMnSc4	bubeník
za	za	k7c4	za
nového	nový	k2eAgMnSc4d1	nový
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jím	jíst	k5eAaImIp1nS	jíst
Doug	Doug	k1gInSc4	Doug
Sampson	Sampsona	k1gFnPc2	Sampsona
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
sehnat	sehnat	k5eAaPmF	sehnat
sólového	sólový	k2eAgMnSc4d1	sólový
zpěváka	zpěvák	k1gMnSc4	zpěvák
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnSc7	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Dennis	Dennis	k1gFnSc7	Dennis
Wilcock	Wilcocka	k1gFnPc2	Wilcocka
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Harris	Harris	k1gInSc1	Harris
se	s	k7c7	s
Sampsonem	Sampson	k1gInSc7	Sampson
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
odešli	odejít	k5eAaPmAgMnP	odejít
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
si	se	k3xPyFc3	se
Harris	Harris	k1gInSc1	Harris
chtěl	chtít	k5eAaImAgInS	chtít
založit	založit	k5eAaPmF	založit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
skupinu	skupina	k1gFnSc4	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
chtěl	chtít	k5eAaImAgMnS	chtít
vzít	vzít	k5eAaPmF	vzít
i	i	k9	i
Sampsona	Sampson	k1gMnSc4	Sampson
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
však	však	k9	však
již	již	k6eAd1	již
hrál	hrát	k5eAaImAgMnS	hrát
s	s	k7c7	s
někým	někdo	k3yInSc7	někdo
jiným	jiný	k2eAgNnSc7d1	jiné
<g/>
.	.	kIx.	.
</s>
<s>
Volba	volba	k1gFnSc1	volba
nakonec	nakonec	k6eAd1	nakonec
padla	padnout	k5eAaImAgFnS	padnout
na	na	k7c4	na
Rona	Ron	k1gMnSc4	Ron
Matthewse	Matthews	k1gMnSc4	Matthews
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
Iron	iron	k1gInSc1	iron
Maiden	Maidna	k1gFnPc2	Maidna
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
železná	železný	k2eAgFnSc1d1	železná
panna	panna	k1gFnSc1	panna
<g/>
,	,	kIx,	,
mučicí	mučicí	k2eAgInSc1d1	mučicí
nástroj	nástroj	k1gInSc1	nástroj
<g/>
)	)	kIx)	)
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c4	na
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
v	v	k7c6	v
sestavě	sestava	k1gFnSc6	sestava
Harris	Harris	k1gFnSc1	Harris
(	(	kIx(	(
<g/>
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Matthews	Matthews	k1gInSc1	Matthews
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgMnSc1d1	bicí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dave	Dav	k1gInSc5	Dav
Sullivan	Sullivan	k1gMnSc1	Sullivan
(	(	kIx(	(
<g/>
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Day	Day	k1gMnSc1	Day
(	(	kIx(	(
<g/>
zpěv	zpěv	k1gInSc1	zpěv
<g/>
)	)	kIx)	)
a	a	k8xC	a
Terry	Terr	k1gInPc1	Terr
Rance	ranec	k1gInSc2	ranec
(	(	kIx(	(
<g/>
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Harrise	Harrise	k1gFnSc2	Harrise
její	její	k3xOp3gInSc1	její
název	název	k1gInSc1	název
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
podle	podle	k7c2	podle
filmu	film	k1gInSc2	film
Muž	muž	k1gMnSc1	muž
se	s	k7c7	s
železnou	železný	k2eAgFnSc7d1	železná
maskou	maska	k1gFnSc7	maska
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Man	Man	k1gMnSc1	Man
in	in	k?	in
the	the	k?	the
Iron	iron	k1gInSc1	iron
Mask	Mask	k1gInSc1	Mask
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
právě	právě	k6eAd1	právě
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
hospodských	hospodský	k2eAgInPc6d1	hospodský
koncertech	koncert	k1gInPc6	koncert
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
odešel	odejít	k5eAaPmAgMnS	odejít
zpěvák	zpěvák	k1gMnSc1	zpěvák
Paul	Paul	k1gMnSc1	Paul
Day	Day	k1gMnSc1	Day
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
náhrada	náhrada	k1gFnSc1	náhrada
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
Dennis	Dennis	k1gInSc1	Dennis
Wilcock	Wilcocko	k1gNnPc2	Wilcocko
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgInSc7	který
již	již	k9	již
Harris	Harris	k1gInSc1	Harris
dříve	dříve	k6eAd2	dříve
hrál	hrát	k5eAaImAgInS	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
začínali	začínat	k5eAaImAgMnP	začínat
dělat	dělat	k5eAaImF	dělat
problémy	problém	k1gInPc4	problém
kytaristé	kytarista	k1gMnPc1	kytarista
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
neuměli	umět	k5eNaImAgMnP	umět
hrát	hrát	k5eAaImF	hrát
sóla	sólo	k1gNnPc4	sólo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc4	ten
vyřešil	vyřešit	k5eAaPmAgMnS	vyřešit
Wilcockův	Wilcockův	k2eAgMnSc1d1	Wilcockův
kamarád	kamarád	k1gMnSc1	kamarád
Dave	Dav	k1gInSc5	Dav
Murray	Murray	k1gInPc1	Murray
<g/>
,	,	kIx,	,
oba	dva	k4xCgMnPc1	dva
dosavadní	dosavadní	k2eAgMnPc1d1	dosavadní
kytaristé	kytarista	k1gMnPc1	kytarista
však	však	k9	však
odešli	odejít	k5eAaPmAgMnP	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
před	před	k7c4	před
Iron	iron	k1gInSc4	iron
Maiden	Maidna	k1gFnPc2	Maidna
se	se	k3xPyFc4	se
Murray	Murraa	k1gFnSc2	Murraa
znal	znát	k5eAaImAgMnS	znát
s	s	k7c7	s
Adrianem	Adrian	k1gMnSc7	Adrian
Smithem	Smith	k1gInSc7	Smith
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
hrál	hrát	k5eAaImAgMnS	hrát
rovněž	rovněž	k9	rovněž
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
první	první	k4xOgFnSc6	první
skupině	skupina	k1gFnSc6	skupina
pojmenované	pojmenovaný	k2eAgFnSc2d1	pojmenovaná
Stone	ston	k1gInSc5	ston
Free	Free	k1gFnSc6	Free
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
společně	společně	k6eAd1	společně
působili	působit	k5eAaImAgMnP	působit
ještě	ještě	k9	ještě
v	v	k7c6	v
několika	několik	k4yIc6	několik
dalších	další	k2eAgFnPc6d1	další
skupinách	skupina	k1gFnPc6	skupina
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgFnPc4	který
vydržel	vydržet	k5eAaPmAgMnS	vydržet
déle	dlouho	k6eAd2	dlouho
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
Smithe	Smith	k1gFnSc2	Smith
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
The	The	k1gFnSc7	The
Secret	Secreta	k1gFnPc2	Secreta
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
rovněž	rovněž	k9	rovněž
první	první	k4xOgFnSc1	první
kapela	kapela	k1gFnSc1	kapela
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kterou	který	k3yRgFnSc4	který
nahrál	nahrát	k5eAaBmAgMnS	nahrát
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
nahrávku	nahrávka	k1gFnSc4	nahrávka
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
ho	on	k3xPp3gInSc4	on
Wilcock	Wilcock	k1gInSc4	Wilcock
s	s	k7c7	s
Harrisem	Harris	k1gInSc7	Harris
přijali	přijmout	k5eAaPmAgMnP	přijmout
do	do	k7c2	do
Iron	iron	k1gInSc4	iron
Maiden	Maidna	k1gFnPc2	Maidna
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
odehrála	odehrát	k5eAaPmAgFnS	odehrát
nějaké	nějaký	k3yIgInPc4	nějaký
koncerty	koncert	k1gInPc4	koncert
jen	jen	k9	jen
ve	v	k7c6	v
čtyřech	čtyři	k4xCgNnPc6	čtyři
(	(	kIx(	(
<g/>
Harris	Harris	k1gFnPc1	Harris
<g/>
,	,	kIx,	,
Murray	Murraa	k1gFnPc1	Murraa
<g/>
,	,	kIx,	,
Wilcock	Wilcock	k1gInSc1	Wilcock
a	a	k8xC	a
Matthews	Matthews	k1gInSc1	Matthews
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
však	však	k9	však
přijala	přijmout	k5eAaPmAgFnS	přijmout
druhého	druhý	k4xOgMnSc4	druhý
kytaristu	kytarista	k1gMnSc4	kytarista
jménem	jméno	k1gNnSc7	jméno
Bob	Bob	k1gMnSc1	Bob
Sawyer	Sawyer	k1gMnSc1	Sawyer
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
neshodách	neshoda	k1gFnPc6	neshoda
se	s	k7c7	s
Sawyerem	Sawyer	k1gInSc7	Sawyer
a	a	k8xC	a
Wilcockem	Wilcock	k1gInSc7	Wilcock
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
na	na	k7c4	na
čas	čas	k1gInSc4	čas
odešel	odejít	k5eAaPmAgMnS	odejít
Dave	Dav	k1gInSc5	Dav
Murray	Murraa	k1gFnPc5	Murraa
<g/>
.	.	kIx.	.
</s>
<s>
Murray	Murray	k1gInPc1	Murray
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
přidal	přidat	k5eAaPmAgMnS	přidat
ke	k	k7c3	k
Smithově	Smithův	k2eAgFnSc3d1	Smithova
skupině	skupina	k1gFnSc3	skupina
Urchin	Urchin	k1gInSc4	Urchin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Maiden	Maidna	k1gFnPc2	Maidna
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nezůstal	zůstat	k5eNaPmAgMnS	zůstat
ani	ani	k8xC	ani
jeden	jeden	k4xCgMnSc1	jeden
kytarista	kytarista	k1gMnSc1	kytarista
a	a	k8xC	a
Harris	Harris	k1gFnSc1	Harris
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
vůdčí	vůdčí	k2eAgFnSc1d1	vůdčí
osobnost	osobnost	k1gFnSc1	osobnost
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
shánět	shánět	k5eAaImF	shánět
klávesistu	klávesista	k1gMnSc4	klávesista
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
však	však	k9	však
s	s	k7c7	s
klávesistou	klávesista	k1gMnSc7	klávesista
Tonym	Tony	k1gMnSc7	Tony
Moorem	Moor	k1gMnSc7	Moor
odehrála	odehrát	k5eAaPmAgFnS	odehrát
jeden	jeden	k4xCgInSc4	jeden
pouhý	pouhý	k2eAgInSc4d1	pouhý
koncert	koncert	k1gInSc4	koncert
a	a	k8xC	a
Harris	Harris	k1gInSc1	Harris
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
pokračovat	pokračovat	k5eAaImF	pokračovat
opět	opět	k6eAd1	opět
bez	bez	k7c2	bez
kláves	klávesa	k1gFnPc2	klávesa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
koncertě	koncert	k1gInSc6	koncert
nehrál	hrát	k5eNaImAgMnS	hrát
ani	ani	k8xC	ani
stálý	stálý	k2eAgMnSc1d1	stálý
bubeník	bubeník	k1gMnSc1	bubeník
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
toho	ten	k3xDgMnSc4	ten
nahradil	nahradit	k5eAaPmAgInS	nahradit
Barry	Barra	k1gFnSc2	Barra
Purkins	Purkins	k1gInSc1	Purkins
(	(	kIx(	(
<g/>
známější	známý	k2eAgFnSc1d2	známější
jako	jako	k8xC	jako
Thunderstick	Thunderstick	k1gInSc1	Thunderstick
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
kytaristy	kytarista	k1gMnPc7	kytarista
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgInS	postavit
Terry	Terra	k1gFnSc2	Terra
Wrapram	Wrapram	k1gInSc1	Wrapram
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
Thunderstick	Thunderstick	k1gMnSc1	Thunderstick
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
nevydržel	vydržet	k5eNaPmAgMnS	vydržet
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
jeden	jeden	k4xCgInSc4	jeden
koncert	koncert	k1gInSc4	koncert
<g/>
;	;	kIx,	;
nahradil	nahradit	k5eAaPmAgMnS	nahradit
ho	on	k3xPp3gNnSc4	on
Harrisův	Harrisův	k2eAgMnSc1d1	Harrisův
starý	starý	k2eAgMnSc1d1	starý
známý	známý	k2eAgMnSc1d1	známý
Doug	Doug	k1gMnSc1	Doug
Sampson	Sampson	k1gMnSc1	Sampson
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
další	další	k2eAgMnPc1d1	další
ze	z	k7c2	z
sestavy	sestava	k1gFnSc2	sestava
odešel	odejít	k5eAaPmAgMnS	odejít
zpěvák	zpěvák	k1gMnSc1	zpěvák
Dennis	Dennis	k1gFnSc2	Dennis
Wilcock	Wilcock	k1gMnSc1	Wilcock
<g/>
,	,	kIx,	,
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
kytaristy	kytarista	k1gMnSc2	kytarista
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
Murray	Murraa	k1gFnSc2	Murraa
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
zkoušeli	zkoušet	k5eAaImAgMnP	zkoušet
v	v	k7c6	v
triu	trio	k1gNnSc6	trio
Harris	Harris	k1gFnPc2	Harris
<g/>
,	,	kIx,	,
Sampson	Sampsona	k1gFnPc2	Sampsona
a	a	k8xC	a
Murray	Murraa	k1gFnSc2	Murraa
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
členech	člen	k1gInPc6	člen
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
pouze	pouze	k6eAd1	pouze
zkoušeli	zkoušet	k5eAaImAgMnP	zkoušet
až	až	k9	až
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1978	[number]	k4	1978
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
přišel	přijít	k5eAaPmAgMnS	přijít
zpěvák	zpěvák	k1gMnSc1	zpěvák
Paul	Paul	k1gMnSc1	Paul
Di	Di	k1gMnSc1	Di
<g/>
'	'	kIx"	'
<g/>
Anno	Anna	k1gFnSc5	Anna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
skupina	skupina	k1gFnSc1	skupina
začala	začít	k5eAaPmAgFnS	začít
používat	používat	k5eAaImF	používat
různé	různý	k2eAgInPc4d1	různý
kouřové	kouřový	k2eAgInPc4d1	kouřový
a	a	k8xC	a
světelné	světelný	k2eAgInPc4d1	světelný
efekty	efekt	k1gInPc4	efekt
<g/>
,	,	kIx,	,
a	a	k8xC	a
přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c6	v
stejném	stejný	k2eAgNnSc6d1	stejné
období	období	k1gNnSc6	období
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
maskot	maskot	k1gInSc1	maskot
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
Eddie	Eddius	k1gMnSc5	Eddius
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
prosincový	prosincový	k2eAgInSc1d1	prosincový
den	den	k1gInSc1	den
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
Iron	iron	k1gInSc4	iron
Maiden	Maidno	k1gNnPc2	Maidno
získali	získat	k5eAaPmAgMnP	získat
možnost	možnost	k1gFnSc4	možnost
nahrát	nahrát	k5eAaBmF	nahrát
své	svůj	k3xOyFgInPc4	svůj
první	první	k4xOgMnSc1	první
demo	demo	k2eAgMnPc2d1	demo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sestavě	sestava	k1gFnSc6	sestava
Harris	Harris	k1gFnSc2	Harris
<g/>
,	,	kIx,	,
Murray	Murraa	k1gFnSc2	Murraa
<g/>
,	,	kIx,	,
Di	Di	k1gFnSc2	Di
<g/>
'	'	kIx"	'
<g/>
Anno	Anna	k1gFnSc5	Anna
a	a	k8xC	a
Sampson	Sampson	k1gNnSc4	Sampson
nahráli	nahrát	k5eAaBmAgMnP	nahrát
celkem	celkem	k6eAd1	celkem
čtyři	čtyři	k4xCgFnPc4	čtyři
skladby	skladba	k1gFnPc4	skladba
−	−	k?	−
"	"	kIx"	"
<g/>
Iron	iron	k1gInSc1	iron
Maiden	Maidna	k1gFnPc2	Maidna
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Prowler	Prowler	k1gInSc1	Prowler
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Invasion	Invasion	k1gInSc1	Invasion
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Strange	Strange	k1gFnSc1	Strange
World	World	k1gMnSc1	World
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nahrávky	nahrávka	k1gFnPc4	nahrávka
poskytli	poskytnout	k5eAaPmAgMnP	poskytnout
DJ	DJ	kA	DJ
Nealu	Neal	k1gMnSc3	Neal
Kayovi	Kaya	k1gMnSc3	Kaya
<g/>
,	,	kIx,	,
vlastníkovi	vlastník	k1gMnSc3	vlastník
klubu	klub	k1gInSc2	klub
Soundhouse	Soundhouse	k1gFnPc1	Soundhouse
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
manažerem	manažer	k1gMnSc7	manažer
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Rod	rod	k1gInSc1	rod
Smallwood	Smallwood	k1gInSc1	Smallwood
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
hrála	hrát	k5eAaImAgFnS	hrát
poměrně	poměrně	k6eAd1	poměrně
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
s	s	k7c7	s
jedním	jeden	k4xCgMnSc7	jeden
kytaristou	kytarista	k1gMnSc7	kytarista
<g/>
,	,	kIx,	,
počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
se	se	k3xPyFc4	se
jím	on	k3xPp3gMnSc7	on
stal	stát	k5eAaPmAgMnS	stát
Paul	Paul	k1gMnSc1	Paul
Cairns	Cairnsa	k1gFnPc2	Cairnsa
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
však	však	k9	však
vydržel	vydržet	k5eAaPmAgMnS	vydržet
přibližně	přibližně	k6eAd1	přibližně
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
a	a	k8xC	a
pak	pak	k6eAd1	pak
jej	on	k3xPp3gMnSc4	on
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Paul	Paul	k1gMnSc1	Paul
Todd	Todd	k1gMnSc1	Todd
(	(	kIx(	(
<g/>
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
skupinou	skupina	k1gFnSc7	skupina
odehrál	odehrát	k5eAaPmAgInS	odehrát
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc1	jeden
koncert	koncert	k1gInSc1	koncert
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1979	[number]	k4	1979
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
přišel	přijít	k5eAaPmAgMnS	přijít
Tony	Tony	k1gMnSc1	Tony
Parsons	Parsons	k1gInSc4	Parsons
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zde	zde	k6eAd1	zde
vydržel	vydržet	k5eAaPmAgMnS	vydržet
přibližně	přibližně	k6eAd1	přibližně
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
a	a	k8xC	a
odešel	odejít	k5eAaPmAgMnS	odejít
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
podepsáním	podepsání	k1gNnSc7	podepsání
smlouvy	smlouva	k1gFnSc2	smlouva
s	s	k7c7	s
EMI	EMI	kA	EMI
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
podepsáním	podepsání	k1gNnSc7	podepsání
smlouvy	smlouva	k1gFnSc2	smlouva
s	s	k7c7	s
EMI	EMI	kA	EMI
Records	Recordsa	k1gFnPc2	Recordsa
si	se	k3xPyFc3	se
skupina	skupina	k1gFnSc1	skupina
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1979	[number]	k4	1979
u	u	k7c2	u
fiktivního	fiktivní	k2eAgNnSc2d1	fiktivní
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
Rock	rock	k1gInSc1	rock
Hard	Hard	k1gInSc1	Hard
Records	Records	k1gInSc4	Records
(	(	kIx(	(
<g/>
vlastním	vlastní	k2eAgInSc7d1	vlastní
nákladem	náklad	k1gInSc7	náklad
<g/>
)	)	kIx)	)
vydala	vydat	k5eAaPmAgFnS	vydat
EP	EP	kA	EP
pojmenované	pojmenovaný	k2eAgFnSc2d1	pojmenovaná
The	The	k1gFnSc2	The
Soundhouse	Soundhouse	k1gFnSc2	Soundhouse
Tapes	Tapesa	k1gFnPc2	Tapesa
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
špatné	špatný	k2eAgFnSc3d1	špatná
technické	technický	k2eAgFnSc3d1	technická
kvalitě	kvalita	k1gFnSc3	kvalita
nahrávky	nahrávka	k1gFnSc2	nahrávka
neobsahovaly	obsahovat	k5eNaImAgInP	obsahovat
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
Strange	Strange	k1gFnSc1	Strange
World	World	k1gMnSc1	World
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
prosincový	prosincový	k2eAgInSc4d1	prosincový
den	den	k1gInSc4	den
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
se	se	k3xPyFc4	se
členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
Iron	iron	k1gInSc1	iron
Maiden	Maidna	k1gFnPc2	Maidna
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Roda	Rodus	k1gMnSc2	Rodus
Smallwooda	Smallwood	k1gMnSc2	Smallwood
sešli	sejít	k5eAaPmAgMnP	sejít
se	s	k7c7	s
zástupci	zástupce	k1gMnPc7	zástupce
společnosti	společnost	k1gFnSc2	společnost
EMI	EMI	kA	EMI
a	a	k8xC	a
podepsali	podepsat	k5eAaPmAgMnP	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nahrání	nahrání	k1gNnSc4	nahrání
alba	album	k1gNnSc2	album
Harris	Harris	k1gFnSc2	Harris
chtěl	chtít	k5eAaImAgMnS	chtít
sehnat	sehnat	k5eAaPmF	sehnat
ještě	ještě	k9	ještě
druhého	druhý	k4xOgMnSc4	druhý
kytaristu	kytarista	k1gMnSc4	kytarista
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
prvnímu	první	k4xOgMnSc3	první
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
místo	místo	k1gNnSc1	místo
nabídnuto	nabídnut	k2eAgNnSc1d1	nabídnuto
Adrianu	Adriana	k1gFnSc4	Adriana
Smithovi	Smith	k1gMnSc3	Smith
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
s	s	k7c7	s
Murrayem	Murray	k1gInSc7	Murray
hrál	hrát	k5eAaImAgMnS	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
to	ten	k3xDgNnSc4	ten
však	však	k9	však
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
i	i	k9	i
jeho	jeho	k3xOp3gFnSc1	jeho
kapela	kapela	k1gFnSc1	kapela
Urchin	Urchina	k1gFnPc2	Urchina
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
získala	získat	k5eAaPmAgFnS	získat
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
volba	volba	k1gFnSc1	volba
padla	padnout	k5eAaImAgFnS	padnout
na	na	k7c6	na
Dennise	Dennis	k1gInSc6	Dennis
Strattona	Strattona	k1gFnSc1	Strattona
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
rovněž	rovněž	k9	rovněž
odešel	odejít	k5eAaPmAgMnS	odejít
bubeník	bubeník	k1gMnSc1	bubeník
Sampson	Sampson	k1gMnSc1	Sampson
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1979	[number]	k4	1979
ho	on	k3xPp3gMnSc4	on
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Strattonův	Strattonův	k2eAgMnSc1d1	Strattonův
přítel	přítel	k1gMnSc1	přítel
Clive	Cliev	k1gFnSc2	Cliev
Burr	Burr	k1gMnSc1	Burr
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
skupina	skupina	k1gFnSc1	skupina
začala	začít	k5eAaPmAgFnS	začít
nahrávat	nahrávat	k5eAaImF	nahrávat
své	svůj	k3xOyFgInPc4	svůj
historicky	historicky	k6eAd1	historicky
první	první	k4xOgNnSc4	první
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
skladby	skladba	k1gFnPc4	skladba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
již	již	k6eAd1	již
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
hráli	hrát	k5eAaImAgMnP	hrát
na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
neúspěšných	úspěšný	k2eNgFnPc6d1	neúspěšná
zkouškách	zkouška	k1gFnPc6	zkouška
se	s	k7c7	s
dvěma	dva	k4xCgMnPc7	dva
různými	různý	k2eAgMnPc7d1	různý
producenty	producent	k1gMnPc7	producent
nakonec	nakonec	k6eAd1	nakonec
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
Will	Will	k1gMnSc1	Will
Malone	Malon	k1gMnSc5	Malon
<g/>
.	.	kIx.	.
</s>
<s>
Vydání	vydání	k1gNnSc1	vydání
eponymního	eponymní	k2eAgNnSc2d1	eponymní
alba	album	k1gNnSc2	album
Iron	iron	k1gInSc1	iron
Maiden	Maidna	k1gFnPc2	Maidna
předcházel	předcházet	k5eAaImAgInS	předcházet
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Running	Running	k1gInSc1	Running
Free	Fre	k1gInSc2	Fre
<g/>
"	"	kIx"	"
se	s	k7c7	s
skladbou	skladba	k1gFnSc7	skladba
"	"	kIx"	"
<g/>
Burning	Burning	k1gInSc1	Burning
Ambition	Ambition	k1gInSc1	Ambition
<g/>
"	"	kIx"	"
na	na	k7c6	na
B-straně	Btrana	k1gFnSc6	B-strana
<g/>
.	.	kIx.	.
</s>
<s>
Singl	singl	k1gInSc1	singl
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
a	a	k8xC	a
album	album	k1gNnSc1	album
až	až	k6eAd1	až
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obalu	obal	k1gInSc6	obal
singlu	singl	k1gInSc2	singl
se	se	k3xPyFc4	se
nenachází	nacházet	k5eNaImIp3nS	nacházet
maskot	maskot	k1gInSc1	maskot
Eddie	Eddie	k1gFnSc2	Eddie
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
však	však	k9	však
je	být	k5eAaImIp3nS	být
Derek	Derek	k6eAd1	Derek
Riggs	Riggs	k1gInSc4	Riggs
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
Eddieho	Eddie	k1gMnSc2	Eddie
na	na	k7c4	na
pozdější	pozdní	k2eAgNnSc4d2	pozdější
alba	album	k1gNnPc4	album
připravoval	připravovat	k5eAaImAgInS	připravovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1980	[number]	k4	1980
Iron	iron	k1gInSc4	iron
Maiden	Maidno	k1gNnPc2	Maidno
rozjeli	rozjet	k5eAaPmAgMnP	rozjet
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
několika	několik	k4yIc7	několik
dalšími	další	k2eAgMnPc7d1	další
turné	turné	k1gNnSc1	turné
nazvané	nazvaný	k2eAgNnSc1d1	nazvané
Metal	metal	k1gInSc4	metal
for	forum	k1gNnPc2	forum
Muthas	Muthas	k1gMnSc1	Muthas
Tour	Tour	k1gMnSc1	Tour
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
doprovodné	doprovodný	k2eAgNnSc4d1	doprovodné
turné	turné	k1gNnSc4	turné
ke	k	k7c3	k
kompilaci	kompilace	k1gFnSc3	kompilace
Metal	metat	k5eAaImAgMnS	metat
for	forum	k1gNnPc2	forum
Muthas	Muthas	k1gMnSc1	Muthas
Neala	Nealo	k1gNnSc2	Nealo
Kaye	Kaye	k1gNnSc2	Kaye
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
vydáním	vydání	k1gNnSc7	vydání
prvního	první	k4xOgMnSc2	první
alba	album	k1gNnSc2	album
skupina	skupina	k1gFnSc1	skupina
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1980	[number]	k4	1980
dělala	dělat	k5eAaImAgFnS	dělat
předkapelu	předkapela	k1gFnSc4	předkapela
skupině	skupina	k1gFnSc3	skupina
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
v	v	k7c6	v
britské	britský	k2eAgFnSc6d1	britská
části	část	k1gFnSc6	část
při	při	k7c6	při
jejich	jejich	k3xOp3gNnSc6	jejich
turné	turné	k1gNnSc6	turné
British	British	k1gMnSc1	British
Steel	Steel	k1gMnSc1	Steel
Tour	Tour	k1gMnSc1	Tour
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
konalo	konat	k5eAaImAgNnS	konat
několik	několik	k4yIc1	několik
posledních	poslední	k2eAgInPc2d1	poslední
koncertů	koncert	k1gInPc2	koncert
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
turné	turné	k1gNnSc2	turné
Metal	metal	k1gInSc1	metal
for	forum	k1gNnPc2	forum
Muthas	Muthasa	k1gFnPc2	Muthasa
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Iron	iron	k1gInSc1	iron
Maiden	Maidno	k1gNnPc2	Maidno
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
a	a	k8xC	a
ihned	ihned	k6eAd1	ihned
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
na	na	k7c4	na
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
místo	místo	k1gNnSc4	místo
hitparády	hitparáda	k1gFnSc2	hitparáda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
května	květen	k1gInSc2	květen
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
1980	[number]	k4	1980
pak	pak	k8xC	pak
Iron	iron	k1gInSc4	iron
Maiden	Maidno	k1gNnPc2	Maidno
odehráli	odehrát	k5eAaPmAgMnP	odehrát
společné	společný	k2eAgNnSc4d1	společné
turné	turné	k1gNnSc4	turné
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Praying	Praying	k1gInSc4	Praying
Mantis	mantisa	k1gFnPc2	mantisa
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
srpna	srpen	k1gInSc2	srpen
pak	pak	k6eAd1	pak
své	svůj	k3xOyFgInPc4	svůj
vůbec	vůbec	k9	vůbec
první	první	k4xOgNnSc4	první
evropské	evropský	k2eAgNnSc4d1	Evropské
turné	turné	k1gNnSc4	turné
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jako	jako	k9	jako
předkapela	předkapela	k1gFnSc1	předkapela
americké	americký	k2eAgFnSc2d1	americká
skupině	skupina	k1gFnSc3	skupina
Kiss	Kiss	k1gInSc4	Kiss
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
během	během	k7c2	během
turné	turné	k1gNnSc2	turné
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
problémy	problém	k1gInPc1	problém
mezi	mezi	k7c7	mezi
Strattonem	Stratton	k1gInSc7	Stratton
a	a	k8xC	a
ostatními	ostatní	k2eAgMnPc7d1	ostatní
členy	člen	k1gMnPc7	člen
zhoršovat	zhoršovat	k5eAaImF	zhoršovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
ukončení	ukončení	k1gNnSc6	ukončení
byl	být	k5eAaImAgInS	být
Stratton	Stratton	k1gInSc1	Stratton
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
propuštěn	propuštěn	k2eAgInSc4d1	propuštěn
a	a	k8xC	a
jako	jako	k8xS	jako
náhrada	náhrada	k1gFnSc1	náhrada
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
přišel	přijít	k5eAaPmAgMnS	přijít
Adrian	Adrian	k1gMnSc1	Adrian
Smith	Smith	k1gMnSc1	Smith
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
do	do	k7c2	do
prosince	prosinec	k1gInSc2	prosinec
1980	[number]	k4	1980
skupina	skupina	k1gFnSc1	skupina
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
turné	turné	k1gNnSc4	turné
s	s	k7c7	s
novým	nový	k2eAgMnSc7d1	nový
kytaristou	kytarista	k1gMnSc7	kytarista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
pak	pak	k6eAd1	pak
zahájila	zahájit	k5eAaPmAgFnS	zahájit
nahrávání	nahrávání	k1gNnSc4	nahrávání
své	své	k1gNnSc4	své
druhé	druhý	k4xOgFnSc2	druhý
desky	deska	k1gFnSc2	deska
v	v	k7c6	v
londýnském	londýnský	k2eAgNnSc6d1	Londýnské
studiu	studio	k1gNnSc6	studio
Battery	Batter	k1gMnPc4	Batter
Studios	Studios	k?	Studios
<g/>
.	.	kIx.	.
</s>
<s>
Produkce	produkce	k1gFnSc1	produkce
se	se	k3xPyFc4	se
tentokrát	tentokrát	k6eAd1	tentokrát
ujal	ujmout	k5eAaPmAgMnS	ujmout
Martin	Martin	k1gMnSc1	Martin
Birch	Birch	k1gMnSc1	Birch
<g/>
.	.	kIx.	.
</s>
<s>
Nahrávání	nahrávání	k1gNnSc1	nahrávání
skončilo	skončit	k5eAaPmAgNnS	skončit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1981	[number]	k4	1981
a	a	k8xC	a
výsledek	výsledek	k1gInSc1	výsledek
dostal	dostat	k5eAaPmAgInS	dostat
název	název	k1gInSc4	název
Killers	Killersa	k1gFnPc2	Killersa
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
většinou	většinou	k6eAd1	většinou
skladby	skladba	k1gFnSc2	skladba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
hráli	hrát	k5eAaImAgMnP	hrát
na	na	k7c6	na
svých	svůj	k3xOyFgInPc6	svůj
koncertech	koncert	k1gInPc6	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
9	[number]	k4	9
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
britském	britský	k2eAgInSc6d1	britský
žebříčku	žebříček	k1gInSc6	žebříček
se	se	k3xPyFc4	se
tentokrát	tentokrát	k6eAd1	tentokrát
umístilo	umístit	k5eAaPmAgNnS	umístit
až	až	k9	až
na	na	k7c6	na
dvanáctém	dvanáctý	k4xOgInSc6	dvanáctý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1981	[number]	k4	1981
skupina	skupina	k1gFnSc1	skupina
zahájila	zahájit	k5eAaPmAgFnS	zahájit
světové	světový	k2eAgNnSc4d1	světové
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
začínající	začínající	k2eAgMnPc1d1	začínající
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Ipswich	Ipswicha	k1gFnPc2	Ipswicha
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
následně	následně	k6eAd1	následně
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
přes	přes	k7c4	přes
Francii	Francie	k1gFnSc4	Francie
<g/>
,	,	kIx,	,
Itálii	Itálie	k1gFnSc4	Itálie
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc4	Švýcarsko
<g/>
,	,	kIx,	,
Belgii	Belgie	k1gFnSc4	Belgie
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
a	a	k8xC	a
Německo	Německo	k1gNnSc1	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
se	se	k3xPyFc4	se
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
začalo	začít	k5eAaPmAgNnS	začít
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
napětí	napětí	k1gNnSc4	napětí
kvůli	kvůli	k7c3	kvůli
Paulu	Paul	k1gMnSc3	Paul
Di	Di	k1gMnSc3	Di
<g/>
'	'	kIx"	'
<g/>
Annovi	Anna	k1gMnSc3	Anna
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
několik	několik	k4yIc4	několik
zrušených	zrušený	k2eAgInPc2d1	zrušený
koncertů	koncert	k1gInPc2	koncert
turné	turné	k1gNnSc2	turné
dál	daleko	k6eAd2	daleko
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
Austrálii	Austrálie	k1gFnSc6	Austrálie
a	a	k8xC	a
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
většině	většina	k1gFnSc6	většina
amerických	americký	k2eAgInPc2d1	americký
koncertů	koncert	k1gInPc2	koncert
hráli	hrát	k5eAaImAgMnP	hrát
jako	jako	k8xS	jako
předkapela	předkapela	k1gFnSc1	předkapela
skupině	skupina	k1gFnSc3	skupina
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
neshodách	neshoda	k1gFnPc6	neshoda
s	s	k7c7	s
Paulem	Paul	k1gMnSc7	Paul
Di	Di	k1gMnSc7	Di
<g/>
'	'	kIx"	'
<g/>
Annem	Ann	k1gInSc7	Ann
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
přišel	přijít	k5eAaPmAgMnS	přijít
zpěvák	zpěvák	k1gMnSc1	zpěvák
skupiny	skupina	k1gFnSc2	skupina
Samson	Samson	k1gMnSc1	Samson
jménem	jméno	k1gNnSc7	jméno
Bruce	Bruce	k1gMnSc1	Bruce
Dickinson	Dickinson	k1gMnSc1	Dickinson
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
později	pozdě	k6eAd2	pozdě
vysloužil	vysloužit	k5eAaPmAgMnS	vysloužit
přezdívku	přezdívka	k1gFnSc4	přezdívka
the	the	k?	the
anti	ant	k1gFnSc2	ant
air-raid	airaida	k1gFnPc2	air-raida
siren	siren	k1gInSc1	siren
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
přeložit	přeložit	k5eAaPmF	přeložit
jako	jako	k8xC	jako
siréna	siréna	k1gFnSc1	siréna
při	při	k7c6	při
leteckém	letecký	k2eAgInSc6d1	letecký
náletu	nálet	k1gInSc6	nálet
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
přišla	přijít	k5eAaPmAgFnS	přijít
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ledna	leden	k1gInSc2	leden
do	do	k7c2	do
února	únor	k1gInSc2	únor
1982	[number]	k4	1982
nahrávali	nahrávat	k5eAaImAgMnP	nahrávat
své	svůj	k3xOyFgNnSc4	svůj
třetí	třetí	k4xOgNnSc4	třetí
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
The	The	k1gMnSc1	The
Number	Number	k1gMnSc1	Number
of	of	k?	of
the	the	k?	the
Beast	Beast	k1gInSc1	Beast
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
pak	pak	k6eAd1	pak
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
a	a	k8xC	a
okamžitě	okamžitě	k6eAd1	okamžitě
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
albového	albový	k2eAgInSc2d1	albový
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
Producentem	producent	k1gMnSc7	producent
byl	být	k5eAaImAgMnS	být
opět	opět	k6eAd1	opět
Birch	Birch	k1gMnSc1	Birch
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
skupina	skupina	k1gFnSc1	skupina
zahájila	zahájit	k5eAaPmAgFnS	zahájit
další	další	k2eAgNnSc4d1	další
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
pak	pak	k6eAd1	pak
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
přes	přes	k7c4	přes
Francii	Francie	k1gFnSc4	Francie
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc4	Španělsko
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc4	Švýcarsko
<g/>
,	,	kIx,	,
Belgii	Belgie	k1gFnSc4	Belgie
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
a	a	k8xC	a
následně	následně	k6eAd1	následně
pak	pak	k6eAd1	pak
i	i	k9	i
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Kanadu	Kanada	k1gFnSc4	Kanada
<g/>
,	,	kIx,	,
Austrálii	Austrálie	k1gFnSc4	Austrálie
a	a	k8xC	a
Japonsko	Japonsko	k1gNnSc4	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
několika	několik	k4yIc6	několik
koncertech	koncert	k1gInPc6	koncert
skupina	skupina	k1gFnSc1	skupina
dělala	dělat	k5eAaImAgFnS	dělat
předkapelu	předkapela	k1gFnSc4	předkapela
Scorpions	Scorpionsa	k1gFnPc2	Scorpionsa
<g/>
,	,	kIx,	,
Rainbow	Rainbow	k1gMnSc1	Rainbow
<g/>
,	,	kIx,	,
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
a	a	k8xC	a
38	[number]	k4	38
Special	Special	k1gMnSc1	Special
<g/>
,	,	kIx,	,
většinu	většina	k1gFnSc4	většina
však	však	k9	však
odehrála	odehrát	k5eAaPmAgFnS	odehrát
jako	jako	k9	jako
vlastní	vlastní	k2eAgInPc4d1	vlastní
koncerty	koncert	k1gInPc4	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
turné	turné	k1gNnSc2	turné
se	se	k3xPyFc4	se
začínaly	začínat	k5eAaImAgInP	začínat
tvořit	tvořit	k5eAaImF	tvořit
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
Clivem	Cliv	k1gMnSc7	Cliv
Burrem	Burr	k1gMnSc7	Burr
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
náhrada	náhrada	k1gFnSc1	náhrada
přišel	přijít	k5eAaPmAgInS	přijít
Nicko	nicka	k1gFnSc5	nicka
McBrain	McBrain	k1gInSc4	McBrain
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1983	[number]	k4	1983
skupina	skupina	k1gFnSc1	skupina
zahájila	zahájit	k5eAaPmAgFnS	zahájit
práce	práce	k1gFnPc4	práce
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
čtvrtém	čtvrtý	k4xOgNnSc6	čtvrtý
studiovém	studiový	k2eAgNnSc6d1	studiové
albu	album	k1gNnSc6	album
<g/>
;	;	kIx,	;
prvním	první	k4xOgMnPc3	první
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
nevzniklo	vzniknout	k5eNaPmAgNnS	vzniknout
v	v	k7c6	v
domovské	domovský	k2eAgFnSc6d1	domovská
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Nahrávalo	nahrávat	k5eAaImAgNnS	nahrávat
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
Compass	Compassa	k1gFnPc2	Compassa
Point	pointa	k1gFnPc2	pointa
Studios	Studios	k?	Studios
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Nassau	Nassaus	k1gInSc2	Nassaus
na	na	k7c6	na
Bahamách	Bahamy	k1gFnPc6	Bahamy
<g/>
.	.	kIx.	.
</s>
<s>
Produkce	produkce	k1gFnSc1	produkce
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
ujal	ujmout	k5eAaPmAgInS	ujmout
Birch	Birch	k1gInSc1	Birch
a	a	k8xC	a
album	album	k1gNnSc1	album
nazvané	nazvaný	k2eAgNnSc1d1	nazvané
Piece	pieca	k1gFnSc3	pieca
of	of	k?	of
Mind	Mind	k1gInSc4	Mind
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1983	[number]	k4	1983
a	a	k8xC	a
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
se	se	k3xPyFc4	se
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c6	na
třetím	třetí	k4xOgInSc6	třetí
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
albu	album	k1gNnSc3	album
vyšly	vyjít	k5eAaPmAgInP	vyjít
dva	dva	k4xCgInPc1	dva
singly	singl	k1gInPc1	singl
−	−	k?	−
"	"	kIx"	"
<g/>
Flight	Flight	k1gMnSc1	Flight
of	of	k?	of
Icarus	Icarus	k1gMnSc1	Icarus
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Trooper	Trooper	k1gMnSc1	Trooper
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
The	The	k1gMnSc1	The
Number	Number	k1gMnSc1	Number
of	of	k?	of
the	the	k?	the
Beast	Beast	k1gInSc4	Beast
bylo	být	k5eAaImAgNnS	být
oceněno	ocenit	k5eAaPmNgNnS	ocenit
platinovou	platinový	k2eAgFnSc7d1	platinová
deskou	deska	k1gFnSc7	deska
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
podporu	podpora	k1gFnSc4	podpora
začalo	začít	k5eAaPmAgNnS	začít
2	[number]	k4	2
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
a	a	k8xC	a
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
až	až	k6eAd1	až
do	do	k7c2	do
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
evropských	evropský	k2eAgInPc2d1	evropský
koncertů	koncert	k1gInPc2	koncert
odehráli	odehrát	k5eAaPmAgMnP	odehrát
i	i	k8xC	i
své	svůj	k3xOyFgNnSc4	svůj
vůbec	vůbec	k9	vůbec
první	první	k4xOgNnSc4	první
vlastní	vlastní	k2eAgNnSc4d1	vlastní
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgNnSc6d1	stejné
studiu	studio	k1gNnSc6	studio
jako	jako	k8xS	jako
Piece	pieca	k1gFnSc6	pieca
of	of	k?	of
Mind	Minda	k1gFnPc2	Minda
nahráli	nahrát	k5eAaPmAgMnP	nahrát
Iron	iron	k1gInSc4	iron
Maiden	Maidna	k1gFnPc2	Maidna
i	i	k8xC	i
své	svůj	k3xOyFgNnSc4	svůj
další	další	k2eAgNnSc4d1	další
album	album	k1gNnSc4	album
−	−	k?	−
Powerslave	Powerslav	k1gMnSc5	Powerslav
<g/>
.	.	kIx.	.
</s>
<s>
Nahrávalo	nahrávat	k5eAaImAgNnS	nahrávat
se	se	k3xPyFc4	se
od	od	k7c2	od
února	únor	k1gInSc2	únor
do	do	k7c2	do
června	červen	k1gInSc2	červen
1984	[number]	k4	1984
a	a	k8xC	a
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c4	v
září	září	k1gNnSc4	září
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejznámějším	známý	k2eAgFnPc3d3	nejznámější
skladbám	skladba	k1gFnPc3	skladba
z	z	k7c2	z
alba	album	k1gNnSc2	album
patří	patřit	k5eAaImIp3nP	patřit
singly	singl	k1gInPc1	singl
"	"	kIx"	"
<g/>
Aces	Aces	k1gInSc1	Aces
High	High	k1gInSc1	High
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
2	[number]	k4	2
Minutes	Minutes	k1gInSc1	Minutes
to	ten	k3xDgNnSc1	ten
Midnight	Midnight	k2eAgMnSc1d1	Midnight
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
další	další	k2eAgFnSc1d1	další
známou	známý	k2eAgFnSc7d1	známá
písní	píseň	k1gFnSc7	píseň
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
třináctiminutový	třináctiminutový	k2eAgInSc4d1	třináctiminutový
epos	epos	k1gInSc4	epos
"	"	kIx"	"
<g/>
Rhyme	Rhym	k1gMnSc5	Rhym
of	of	k?	of
the	the	k?	the
Ancient	Ancient	k1gMnSc1	Ancient
Mariner	Mariner	k1gMnSc1	Mariner
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
složen	složit	k5eAaPmNgInS	složit
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
básně	báseň	k1gFnSc2	báseň
anglického	anglický	k2eAgMnSc2d1	anglický
básníka	básník	k1gMnSc2	básník
Samuela	Samuel	k1gMnSc2	Samuel
Taylora	Taylor	k1gMnSc2	Taylor
Coleridge	Coleridg	k1gMnSc2	Coleridg
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
jeho	jeho	k3xOp3gNnSc7	jeho
vydáním	vydání	k1gNnSc7	vydání
skupina	skupina	k1gFnSc1	skupina
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1984	[number]	k4	1984
zahájila	zahájit	k5eAaPmAgFnS	zahájit
své	svůj	k3xOyFgNnSc4	svůj
doposud	doposud	k6eAd1	doposud
největší	veliký	k2eAgNnSc4d3	veliký
turné	turné	k1gNnSc4	turné
nazvané	nazvaný	k2eAgInPc1d1	nazvaný
World	World	k1gInSc1	World
Slavery	Slavera	k1gFnSc2	Slavera
Tour	Toura	k1gFnPc2	Toura
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgNnSc6	jenž
odehrála	odehrát	k5eAaPmAgFnS	odehrát
přibližně	přibližně	k6eAd1	přibližně
190	[number]	k4	190
koncertů	koncert	k1gInPc2	koncert
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
koncertů	koncert	k1gInPc2	koncert
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
Rock	rock	k1gInSc1	rock
in	in	k?	in
Rio	Rio	k1gFnSc2	Rio
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hráli	hrát	k5eAaImAgMnP	hrát
pro	pro	k7c4	pro
přibližně	přibližně	k6eAd1	přibližně
250	[number]	k4	250
000	[number]	k4	000
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
skončilo	skončit	k5eAaPmAgNnS	skončit
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1985	[number]	k4	1985
a	a	k8xC	a
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
při	při	k7c6	při
něm	on	k3xPp3gMnSc6	on
první	první	k4xOgNnSc4	první
oficiální	oficiální	k2eAgNnSc4d1	oficiální
koncertní	koncertní	k2eAgNnSc4d1	koncertní
album	album	k1gNnSc4	album
skupiny	skupina	k1gFnSc2	skupina
−	−	k?	−
Live	Liv	k1gFnSc2	Liv
After	After	k1gMnSc1	After
Death	Death	k1gMnSc1	Death
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
skupina	skupina	k1gFnSc1	skupina
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
na	na	k7c6	na
italském	italský	k2eAgInSc6d1	italský
hororu	horor	k1gInSc6	horor
Phenomena	Phenomen	k2eAgFnSc1d1	Phenomena
režiséra	režisér	k1gMnSc2	režisér
Daria	Daria	k1gFnSc1	Daria
Argenta	argentum	k1gNnSc2	argentum
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgNnSc3	jenž
nahrála	nahrát	k5eAaBmAgFnS	nahrát
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Flash	Flash	k1gInSc1	Flash
of	of	k?	of
the	the	k?	the
Blade	Blad	k1gInSc5	Blad
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc1	svůj
šesté	šestý	k4xOgNnSc1	šestý
studiové	studiový	k2eAgNnSc1d1	studiové
album	album	k1gNnSc1	album
nazvané	nazvaný	k2eAgNnSc1d1	nazvané
Somewhere	Somewher	k1gInSc5	Somewher
in	in	k?	in
Time	Time	k1gNnSc1	Time
bylo	být	k5eAaImAgNnS	být
netradičně	tradičně	k6eNd1	tradičně
nahráváno	nahrávat	k5eAaImNgNnS	nahrávat
ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
studiích	studio	k1gNnPc6	studio
−	−	k?	−
v	v	k7c4	v
Compass	Compass	k1gInSc4	Compass
Point	pointa	k1gFnPc2	pointa
a	a	k8xC	a
v	v	k7c6	v
Wisseloord	Wisseloordo	k1gNnPc2	Wisseloordo
Studios	Studios	k?	Studios
v	v	k7c6	v
nizozemském	nizozemský	k2eAgInSc6d1	nizozemský
Hilversumu	Hilversum	k1gInSc6	Hilversum
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1986	[number]	k4	1986
a	a	k8xC	a
v	v	k7c6	v
britském	britský	k2eAgInSc6d1	britský
žebříčku	žebříček	k1gInSc6	žebříček
stanulo	stanout	k5eAaPmAgNnS	stanout
opět	opět	k6eAd1	opět
na	na	k7c6	na
třetím	třetí	k4xOgInSc6	třetí
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
září	září	k1gNnSc2	září
1986	[number]	k4	1986
do	do	k7c2	do
května	květen	k1gInSc2	květen
1987	[number]	k4	1987
skupina	skupina	k1gFnSc1	skupina
jela	jet	k5eAaImAgFnS	jet
další	další	k2eAgNnSc4d1	další
velké	velký	k2eAgNnSc4d1	velké
turné	turné	k1gNnSc4	turné
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
aktuálního	aktuální	k2eAgNnSc2d1	aktuální
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
února	únor	k1gInSc2	únor
do	do	k7c2	do
března	březen	k1gInSc2	březen
1988	[number]	k4	1988
skupina	skupina	k1gFnSc1	skupina
nahrávala	nahrávat	k5eAaImAgFnS	nahrávat
své	svůj	k3xOyFgNnSc4	svůj
sedmé	sedmý	k4xOgNnSc4	sedmý
album	album	k1gNnSc4	album
příznačně	příznačně	k6eAd1	příznačně
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
Seventh	Seventh	k1gMnSc1	Seventh
Son	son	k1gInSc1	son
of	of	k?	of
a	a	k8xC	a
Seventh	Seventh	k1gMnSc1	Seventh
Son	son	k1gInSc1	son
<g/>
.	.	kIx.	.
</s>
<s>
Nahrávání	nahrávání	k1gNnSc1	nahrávání
tentokrát	tentokrát	k6eAd1	tentokrát
probíhalo	probíhat	k5eAaImAgNnS	probíhat
v	v	k7c6	v
mnichovském	mnichovský	k2eAgNnSc6d1	mnichovské
studiu	studio	k1gNnSc6	studio
Musicland	Musiclanda	k1gFnPc2	Musiclanda
Studios	Studios	k?	Studios
a	a	k8xC	a
album	album	k1gNnSc1	album
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
pojato	pojmout	k5eAaPmNgNnS	pojmout
jako	jako	k8xS	jako
koncepční	koncepční	k2eAgFnPc1d1	koncepční
<g/>
,	,	kIx,	,
vzešly	vzejít	k5eAaPmAgFnP	vzejít
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
čtyři	čtyři	k4xCgInPc4	čtyři
singly	singl	k1gInPc4	singl
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Can	Can	k1gFnSc1	Can
I	i	k9	i
Play	play	k0	play
with	with	k1gMnSc1	with
Madness	Madness	k1gInSc1	Madness
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Evil	Evil	k1gMnSc1	Evil
That	That	k1gMnSc1	That
Men	Men	k1gFnSc1	Men
Do	do	k7c2	do
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Clairvoyant	Clairvoyant	k1gMnSc1	Clairvoyant
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Infinite	Infinit	k1gInSc5	Infinit
Dreams	Dreamsa	k1gFnPc2	Dreamsa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc1	všechen
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
v	v	k7c6	v
první	první	k4xOgFnSc6	první
desítce	desítka	k1gFnSc6	desítka
hitparády	hitparáda	k1gFnPc1	hitparáda
singlů	singl	k1gInPc2	singl
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
album	album	k1gNnSc1	album
pak	pak	k6eAd1	pak
napodobilo	napodobit	k5eAaPmAgNnS	napodobit
legendární	legendární	k2eAgFnSc4d1	legendární
The	The	k1gFnSc4	The
Number	Numbra	k1gFnPc2	Numbra
of	of	k?	of
the	the	k?	the
Beast	Beast	k1gFnSc4	Beast
a	a	k8xC	a
také	také	k9	také
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
žebříčku	žebříček	k1gInSc2	žebříček
prodejnosti	prodejnost	k1gFnSc2	prodejnost
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
prosince	prosinec	k1gInSc2	prosinec
1988	[number]	k4	1988
pak	pak	k6eAd1	pak
odehráli	odehrát	k5eAaPmAgMnP	odehrát
necelou	celý	k2eNgFnSc4d1	necelá
stovku	stovka	k1gFnSc4	stovka
koncertů	koncert	k1gInPc2	koncert
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
podpory	podpora	k1gFnSc2	podpora
turné	turné	k1gNnSc2	turné
k	k	k7c3	k
aktuálnímu	aktuální	k2eAgNnSc3d1	aktuální
albu	album	k1gNnSc3	album
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
vystoupení	vystoupení	k1gNnSc4	vystoupení
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
Monsters	Monsters	k1gInSc1	Monsters
of	of	k?	of
Rock	rock	k1gInSc1	rock
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Castle	Castle	k1gFnSc2	Castle
Donington	Donington	k1gInSc1	Donington
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
měla	mít	k5eAaImAgFnS	mít
rovněž	rovněž	k9	rovněž
naplánováno	naplánován	k2eAgNnSc1d1	naplánováno
odehrát	odehrát	k5eAaPmF	odehrát
jeden	jeden	k4xCgInSc4	jeden
koncert	koncert	k1gInSc4	koncert
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
;	;	kIx,	;
mělo	mít	k5eAaImAgNnS	mít
se	se	k3xPyFc4	se
tak	tak	k9	tak
stát	stát	k5eAaImF	stát
na	na	k7c6	na
Stadionu	stadion	k1gInSc6	stadion
na	na	k7c6	na
Letné	Letná	k1gFnSc6	Letná
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
Koncert	koncert	k1gInSc1	koncert
byl	být	k5eAaImAgInS	být
však	však	k9	však
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
vůbec	vůbec	k9	vůbec
o	o	k7c4	o
první	první	k4xOgNnSc4	první
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgInSc6	který
skupina	skupina	k1gFnSc1	skupina
využila	využít	k5eAaPmAgFnS	využít
doprovodného	doprovodný	k2eAgMnSc4d1	doprovodný
klávesistu	klávesista	k1gMnSc4	klávesista
<g/>
;	;	kIx,	;
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jím	on	k3xPp3gMnSc7	on
Harrisův	Harrisův	k2eAgMnSc1d1	Harrisův
baskytarový	baskytarový	k2eAgMnSc1d1	baskytarový
technik	technik	k1gMnSc1	technik
Michael	Michael	k1gMnSc1	Michael
Kenney	Kennea	k1gFnSc2	Kennea
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
skupina	skupina	k1gFnSc1	skupina
spolu	spolu	k6eAd1	spolu
neodehrála	odehrát	k5eNaPmAgFnS	odehrát
ani	ani	k9	ani
jeden	jeden	k4xCgInSc4	jeden
koncert	koncert	k1gInSc4	koncert
<g/>
,	,	kIx,	,
namísto	namísto	k7c2	namísto
toho	ten	k3xDgNnSc2	ten
Harris	Harris	k1gInSc1	Harris
pracoval	pracovat	k5eAaImAgInS	pracovat
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
na	na	k7c6	na
koncertním	koncertní	k2eAgNnSc6d1	koncertní
albu	album	k1gNnSc6	album
a	a	k8xC	a
video	video	k1gNnSc4	video
záznamu	záznam	k1gInSc2	záznam
nazvaném	nazvaný	k2eAgNnSc6d1	nazvané
Maiden	Maidna	k1gFnPc2	Maidna
England	Englando	k1gNnPc2	Englando
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
pak	pak	k6eAd1	pak
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Smith	Smith	k1gMnSc1	Smith
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
vydat	vydat	k5eAaPmF	vydat
vlastní	vlastní	k2eAgNnSc4d1	vlastní
album	album	k1gNnSc4	album
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
Silver	Silver	k1gInSc4	Silver
and	and	k?	and
Gold	Gold	k1gInSc1	Gold
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
pod	pod	k7c7	pod
hlavičkou	hlavička	k1gFnSc7	hlavička
skupiny	skupina	k1gFnSc2	skupina
A.S.	A.S.	k1gFnSc2	A.S.
<g/>
A.	A.	kA	A.
<g/>
P.	P.	kA	P.
(	(	kIx(	(
<g/>
Adrian	Adrian	k1gMnSc1	Adrian
Smith	Smith	k1gMnSc1	Smith
and	and	k?	and
Project	Project	k1gMnSc1	Project
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
sólové	sólový	k2eAgNnSc4d1	sólové
album	album	k1gNnSc4	album
rovněž	rovněž	k9	rovněž
nahrál	nahrát	k5eAaBmAgMnS	nahrát
Dickinson	Dickinson	k1gMnSc1	Dickinson
<g/>
.	.	kIx.	.
</s>
<s>
Dostalo	dostat	k5eAaPmAgNnS	dostat
název	název	k1gInSc4	název
Tattooed	Tattooed	k1gInSc1	Tattooed
Millionaire	Millionair	k1gInSc5	Millionair
a	a	k8xC	a
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
zde	zde	k6eAd1	zde
hrál	hrát	k5eAaImAgInS	hrát
Janick	Janick	k1gInSc1	Janick
Gers	Gers	k1gInSc1	Gers
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
Fishem	Fish	k1gInSc7	Fish
z	z	k7c2	z
Marillion	Marillion	k1gInSc4	Marillion
a	a	k8xC	a
Ianem	Ianus	k1gMnSc7	Ianus
Gillanem	Gillan	k1gMnSc7	Gillan
z	z	k7c2	z
Deep	Deep	k1gMnSc1	Deep
Purple	Purple	k1gMnSc2	Purple
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
nahráváním	nahrávání	k1gNnSc7	nahrávání
osmého	osmý	k4xOgNnSc2	osmý
alba	album	k1gNnSc2	album
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
odešel	odejít	k5eAaPmAgMnS	odejít
Adrian	Adrian	k1gMnSc1	Adrian
Smith	Smith	k1gMnSc1	Smith
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
náhrada	náhrada	k1gFnSc1	náhrada
přišel	přijít	k5eAaPmAgInS	přijít
právě	právě	k6eAd1	právě
Janick	Janick	k1gInSc1	Janick
Gers	Gersa	k1gFnPc2	Gersa
<g/>
.	.	kIx.	.
</s>
<s>
Nahrávalo	nahrávat	k5eAaImAgNnS	nahrávat
se	se	k3xPyFc4	se
od	od	k7c2	od
června	červen	k1gInSc2	červen
do	do	k7c2	do
září	září	k1gNnSc2	září
a	a	k8xC	a
produkce	produkce	k1gFnSc2	produkce
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
ujal	ujmout	k5eAaPmAgMnS	ujmout
Martin	Martin	k1gMnSc1	Martin
Birch	Birch	k1gMnSc1	Birch
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
dostalo	dostat	k5eAaPmAgNnS	dostat
název	název	k1gInSc4	název
No	no	k9	no
Prayer	Prayer	k1gMnSc1	Prayer
for	forum	k1gNnPc2	forum
the	the	k?	the
Dying	Dying	k1gMnSc1	Dying
a	a	k8xC	a
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1990	[number]	k4	1990
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
britském	britský	k2eAgInSc6d1	britský
žebříčku	žebříček	k1gInSc6	žebříček
prodejnosti	prodejnost	k1gFnSc2	prodejnost
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
na	na	k7c4	na
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Albu	alba	k1gFnSc4	alba
se	se	k3xPyFc4	se
však	však	k9	však
moc	moc	k6eAd1	moc
velký	velký	k2eAgInSc4d1	velký
úspěch	úspěch	k1gInSc4	úspěch
nedostavil	dostavit	k5eNaPmAgMnS	dostavit
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgInSc4d2	veliký
úspěch	úspěch	k1gInSc4	úspěch
než	než	k8xS	než
samotné	samotný	k2eAgNnSc4d1	samotné
album	album	k1gNnSc4	album
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Bring	Bring	k1gMnSc1	Bring
Your	Your	k1gMnSc1	Your
Daughter	Daughter	k1gMnSc1	Daughter
<g/>
...	...	k?	...
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Slaughter	Slaughtrum	k1gNnPc2	Slaughtrum
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
vyhoupl	vyhoupnout	k5eAaPmAgMnS	vyhoupnout
na	na	k7c4	na
post	post	k1gInSc4	post
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
udržel	udržet	k5eAaPmAgInS	udržet
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
píseň	píseň	k1gFnSc4	píseň
napsal	napsat	k5eAaBmAgMnS	napsat
Bruce	Bruce	k1gMnSc1	Bruce
Dickinson	Dickinson	k1gMnSc1	Dickinson
a	a	k8xC	a
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
verzi	verze	k1gFnSc6	verze
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
na	na	k7c6	na
soundtracku	soundtrack	k1gInSc6	soundtrack
k	k	k7c3	k
filmu	film	k1gInSc3	film
Noční	noční	k2eAgFnSc1d1	noční
můra	můra	k1gFnSc1	můra
v	v	k7c4	v
Elm	Elm	k1gMnSc4	Elm
Street	Street	k1gInSc4	Street
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
alba	album	k1gNnSc2	album
začalo	začít	k5eAaPmAgNnS	začít
19	[number]	k4	19
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1990	[number]	k4	1990
a	a	k8xC	a
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
až	až	k9	až
do	do	k7c2	do
září	září	k1gNnSc2	září
roku	rok	k1gInSc2	rok
následujícího	následující	k2eAgInSc2d1	následující
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
turné	turné	k1gNnSc6	turné
skupina	skupina	k1gFnSc1	skupina
zahájila	zahájit	k5eAaPmAgFnS	zahájit
práce	práce	k1gFnPc4	práce
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
novém	nový	k2eAgNnSc6d1	nové
albu	album	k1gNnSc6	album
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
to	ten	k3xDgNnSc1	ten
poslední	poslední	k2eAgNnSc1d1	poslední
<g/>
,	,	kIx,	,
produkované	produkovaný	k2eAgNnSc1d1	produkované
Martinem	Martin	k1gMnSc7	Martin
Birchem	Birch	k1gMnSc7	Birch
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nahrávalo	nahrávat	k5eAaImAgNnS	nahrávat
v	v	k7c6	v
Harrisově	Harrisův	k2eAgNnSc6d1	Harrisovo
vlastním	vlastní	k2eAgNnSc6d1	vlastní
studiu	studio	k1gNnSc6	studio
Barnyard	Barnyard	k1gMnSc1	Barnyard
Studios	Studios	k?	Studios
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Birchem	Birch	k1gInSc7	Birch
se	se	k3xPyFc4	se
tentokrát	tentokrát	k6eAd1	tentokrát
na	na	k7c4	na
produkci	produkce	k1gFnSc4	produkce
podílel	podílet	k5eAaImAgInS	podílet
i	i	k9	i
sám	sám	k3xTgInSc1	sám
Harris	Harris	k1gInSc1	Harris
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Fear	Feara	k1gFnPc2	Feara
of	of	k?	of
the	the	k?	the
Dark	Darka	k1gFnPc2	Darka
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
známou	známý	k2eAgFnSc7d1	známá
písní	píseň	k1gFnSc7	píseň
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
alba	album	k1gNnSc2	album
je	být	k5eAaImIp3nS	být
stejnojmenná	stejnojmenný	k2eAgFnSc1d1	stejnojmenná
skladba	skladba	k1gFnSc1	skladba
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
kapela	kapela	k1gFnSc1	kapela
často	často	k6eAd1	často
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c6	na
svých	svůj	k3xOyFgInPc6	svůj
koncertech	koncert	k1gInPc6	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Úspěch	úspěch	k1gInSc1	úspěch
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
singl	singl	k1gInSc4	singl
"	"	kIx"	"
<g/>
Be	Be	k1gMnSc1	Be
Quick	Quick	k1gMnSc1	Quick
or	or	k?	or
Be	Be	k1gMnSc1	Be
Dead	Dead	k1gMnSc1	Dead
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c4	na
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
hitparádě	hitparáda	k1gFnSc6	hitparáda
singlů	singl	k1gInPc2	singl
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
podporu	podpora	k1gFnSc4	podpora
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
a	a	k8xC	a
skončilo	skončit	k5eAaPmAgNnS	skončit
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
stejného	stejný	k2eAgInSc2d1	stejný
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
vystoupení	vystoupení	k1gNnSc1	vystoupení
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
opět	opět	k6eAd1	opět
v	v	k7c6	v
Doningtonu	Donington	k1gInSc6	Donington
−	−	k?	−
jako	jako	k8xC	jako
host	host	k1gMnSc1	host
se	se	k3xPyFc4	se
při	při	k7c6	při
přídavku	přídavek	k1gInSc6	přídavek
představil	představit	k5eAaPmAgMnS	představit
Adrian	Adrian	k1gMnSc1	Adrian
Smith	Smith	k1gMnSc1	Smith
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
března	březen	k1gInSc2	březen
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
1993	[number]	k4	1993
odehráli	odehrát	k5eAaPmAgMnP	odehrát
další	další	k2eAgNnSc4d1	další
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
při	při	k7c6	při
něm	on	k3xPp3gMnSc6	on
však	však	k8xC	však
postupně	postupně	k6eAd1	postupně
začal	začít	k5eAaPmAgMnS	začít
dělat	dělat	k5eAaImF	dělat
problémy	problém	k1gInPc4	problém
i	i	k8xC	i
Dickinson	Dickinson	k1gNnSc4	Dickinson
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
první	první	k4xOgNnSc4	první
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterého	který	k3yQgMnSc4	který
skupina	skupina	k1gFnSc1	skupina
zahrála	zahrát	k5eAaPmAgFnS	zahrát
i	i	k9	i
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
;	;	kIx,	;
v	v	k7c6	v
ostravském	ostravský	k2eAgInSc6d1	ostravský
Paláci	palác	k1gInSc6	palác
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
sportu	sport	k1gInSc2	sport
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
turné	turné	k1gNnSc2	turné
vyšly	vyjít	k5eAaPmAgFnP	vyjít
celkem	celkem	k6eAd1	celkem
dvě	dva	k4xCgNnPc4	dva
koncertní	koncertní	k2eAgNnPc4d1	koncertní
alba	album	k1gNnPc4	album
−	−	k?	−
A	a	k8xC	a
Real	Real	k1gInSc1	Real
Live	Liv	k1gFnSc2	Liv
One	One	k1gFnSc2	One
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
a	a	k8xC	a
A	a	k9	a
Real	Real	k1gInSc1	Real
Dead	Dead	k1gMnSc1	Dead
One	One	k1gMnSc1	One
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
poté	poté	k6eAd1	poté
vyšla	vyjít	k5eAaPmAgFnS	vyjít
jako	jako	k9	jako
jedno	jeden	k4xCgNnSc4	jeden
album	album	k1gNnSc4	album
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
A	a	k9	a
Real	Real	k1gInSc4	Real
Live	Live	k1gFnPc2	Live
Dead	Dead	k1gMnSc1	Dead
One	One	k1gMnSc1	One
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1993	[number]	k4	1993
ještě	ještě	k6eAd1	ještě
vyšel	vyjít	k5eAaPmAgInS	vyjít
další	další	k2eAgInSc1d1	další
koncertní	koncertní	k2eAgInSc1d1	koncertní
záznam	záznam	k1gInSc1	záznam
Live	Liv	k1gFnSc2	Liv
at	at	k?	at
Donington	Donington	k1gInSc1	Donington
<g/>
.	.	kIx.	.
</s>
<s>
Dickinson	Dickinson	k1gMnSc1	Dickinson
svůj	svůj	k3xOyFgInSc4	svůj
odchod	odchod	k1gInSc4	odchod
oznámil	oznámit	k5eAaPmAgMnS	oznámit
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
skupinou	skupina	k1gFnSc7	skupina
však	však	k9	však
ještě	ještě	k6eAd1	ještě
odehrál	odehrát	k5eAaPmAgMnS	odehrát
celé	celý	k2eAgNnSc4d1	celé
turné	turné	k1gNnSc4	turné
až	až	k9	až
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
konkurzu	konkurz	k1gInSc6	konkurz
na	na	k7c4	na
uvolněné	uvolněný	k2eAgNnSc4d1	uvolněné
místo	místo	k1gNnSc4	místo
se	se	k3xPyFc4	se
členům	člen	k1gMnPc3	člen
nejvíce	hodně	k6eAd3	hodně
líbil	líbit	k5eAaImAgMnS	líbit
Blaze	blaze	k6eAd1	blaze
Bayley	Baylea	k1gFnSc2	Baylea
<g/>
,	,	kIx,	,
frontman	frontman	k1gMnSc1	frontman
skupiny	skupina	k1gFnSc2	skupina
Wolfsbane	Wolfsban	k1gInSc5	Wolfsban
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
nástup	nástup	k1gInSc1	nástup
byl	být	k5eAaImAgInS	být
oznámen	oznámit	k5eAaPmNgInS	oznámit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1994	[number]	k4	1994
a	a	k8xC	a
hned	hned	k6eAd1	hned
ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
měsíc	měsíc	k1gInSc4	měsíc
zahájili	zahájit	k5eAaPmAgMnP	zahájit
práce	práce	k1gFnPc4	práce
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
novém	nový	k2eAgNnSc6d1	nové
albu	album	k1gNnSc6	album
<g/>
.	.	kIx.	.
</s>
<s>
Nahrávání	nahrávání	k1gNnSc1	nahrávání
trvalo	trvat	k5eAaImAgNnS	trvat
s	s	k7c7	s
přestávkami	přestávka	k1gFnPc7	přestávka
více	hodně	k6eAd2	hodně
než	než	k8xS	než
rok	rok	k1gInSc4	rok
a	a	k8xC	a
výsledek	výsledek	k1gInSc4	výsledek
s	s	k7c7	s
názvem	název	k1gInSc7	název
The	The	k1gMnPc1	The
X	X	kA	X
Factor	Factor	k1gMnSc1	Factor
spatřil	spatřit	k5eAaPmAgMnS	spatřit
světlo	světlo	k1gNnSc4	světlo
světa	svět	k1gInSc2	svět
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Albu	alba	k1gFnSc4	alba
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
velmi	velmi	k6eAd1	velmi
špatná	špatný	k2eAgFnSc1d1	špatná
kritika	kritika	k1gFnSc1	kritika
a	a	k8xC	a
oproti	oproti	k7c3	oproti
předchozím	předchozí	k2eAgNnPc3d1	předchozí
albům	album	k1gNnPc3	album
působilo	působit	k5eAaImAgNnS	působit
spíš	spíš	k9	spíš
jako	jako	k9	jako
propadák	propadák	k1gInSc1	propadák
<g/>
.	.	kIx.	.
</s>
<s>
Skalní	skalní	k2eAgMnPc1d1	skalní
fanoušci	fanoušek	k1gMnPc1	fanoušek
kapely	kapela	k1gFnSc2	kapela
si	se	k3xPyFc3	se
nemohli	moct	k5eNaImAgMnP	moct
zvyknout	zvyknout	k5eAaPmF	zvyknout
na	na	k7c4	na
nového	nový	k2eAgMnSc4d1	nový
zpěváka	zpěvák	k1gMnSc4	zpěvák
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ne	ne	k9	ne
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
Bayley	Bayley	k1gInPc4	Bayley
špatný	špatný	k2eAgInSc1d1	špatný
<g/>
,	,	kIx,	,
spíš	spíš	k9	spíš
byl	být	k5eAaImAgInS	být
jiný	jiný	k2eAgInSc1d1	jiný
než	než	k8xS	než
Dickinson	Dickinson	k1gInSc1	Dickinson
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterého	který	k3yRgMnSc4	který
byli	být	k5eAaImAgMnP	být
zvyklí	zvyklý	k2eAgMnPc1d1	zvyklý
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
albu	album	k1gNnSc3	album
vyšly	vyjít	k5eAaPmAgInP	vyjít
dva	dva	k4xCgInPc1	dva
doprovodné	doprovodný	k2eAgInPc1d1	doprovodný
singly	singl	k1gInPc1	singl
−	−	k?	−
"	"	kIx"	"
<g/>
Man	Man	k1gMnSc1	Man
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Edge	Edge	k1gNnSc1	Edge
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Lord	lord	k1gMnSc1	lord
of	of	k?	of
the	the	k?	the
Flies	Flies	k1gInSc1	Flies
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
září	září	k1gNnSc2	září
1995	[number]	k4	1995
do	do	k7c2	do
září	září	k1gNnSc2	září
1996	[number]	k4	1996
skupina	skupina	k1gFnSc1	skupina
koncertovala	koncertovat	k5eAaImAgFnS	koncertovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
turné	turné	k1gNnSc2	turné
The	The	k1gFnSc2	The
X	X	kA	X
Factour	Factour	k1gMnSc1	Factour
opět	opět	k6eAd1	opět
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
podruhé	podruhé	k6eAd1	podruhé
zahrála	zahrát	k5eAaPmAgFnS	zahrát
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
;	;	kIx,	;
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
v	v	k7c6	v
pražském	pražský	k2eAgInSc6d1	pražský
Průmyslovém	průmyslový	k2eAgInSc6d1	průmyslový
paláci	palác	k1gInSc6	palác
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
turné	turné	k1gNnSc2	turné
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
opět	opět	k6eAd1	opět
umlčela	umlčet	k5eAaPmAgFnS	umlčet
z	z	k7c2	z
koncertování	koncertování	k1gNnSc2	koncertování
<g/>
,	,	kIx,	,
namísto	namísto	k7c2	namísto
toho	ten	k3xDgNnSc2	ten
však	však	k9	však
zahájila	zahájit	k5eAaPmAgFnS	zahájit
práce	práce	k1gFnPc4	práce
na	na	k7c6	na
albu	album	k1gNnSc6	album
Virtual	Virtual	k1gMnSc1	Virtual
XI	XI	kA	XI
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
až	až	k9	až
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
zaostalo	zaostat	k5eAaPmAgNnS	zaostat
i	i	k9	i
za	za	k7c2	za
The	The	k1gMnSc2	The
X	X	kA	X
Factor	Factor	k1gInSc1	Factor
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgFnPc1d1	známá
písně	píseň	k1gFnPc1	píseň
"	"	kIx"	"
<g/>
Futureal	Futureal	k1gInSc1	Futureal
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Clansman	Clansman	k1gMnSc1	Clansman
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
prosince	prosinec	k1gInSc2	prosinec
pak	pak	k6eAd1	pak
následovalo	následovat	k5eAaImAgNnS	následovat
další	další	k2eAgNnSc1d1	další
turné	turné	k1gNnSc1	turné
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
odehrálo	odehrát	k5eAaPmAgNnS	odehrát
třetí	třetí	k4xOgNnSc1	třetí
vystoupení	vystoupení	k1gNnSc1	vystoupení
skupiny	skupina	k1gFnSc2	skupina
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Tentokrát	tentokrát	k6eAd1	tentokrát
opět	opět	k6eAd1	opět
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
však	však	k9	však
v	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
sportovní	sportovní	k2eAgFnSc6d1	sportovní
hale	hala	k1gFnSc6	hala
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
turné	turné	k1gNnSc6	turné
začal	začít	k5eAaPmAgInS	začít
zlobit	zlobit	k5eAaImF	zlobit
Bayleyův	Bayleyův	k2eAgInSc1d1	Bayleyův
hlas	hlas	k1gInSc1	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
koncertů	koncert	k1gInPc2	koncert
muselo	muset	k5eAaImAgNnS	muset
být	být	k5eAaImF	být
dokonce	dokonce	k9	dokonce
zrušeno	zrušen	k2eAgNnSc1d1	zrušeno
a	a	k8xC	a
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
ukončení	ukončení	k1gNnSc6	ukončení
byl	být	k5eAaImAgInS	být
Bayley	Bayle	k1gMnPc4	Bayle
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
vyhozen	vyhozen	k2eAgMnSc1d1	vyhozen
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
Blaze	blaze	k6eAd1	blaze
Bayleyho	Bayley	k1gMnSc4	Bayley
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
myšlenka	myšlenka	k1gFnSc1	myšlenka
opět	opět	k6eAd1	opět
oslovit	oslovit	k5eAaPmF	oslovit
Bruce	Bruce	k1gMnSc4	Bruce
Dickinsona	Dickinson	k1gMnSc4	Dickinson
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
přišel	přijít	k5eAaPmAgMnS	přijít
i	i	k9	i
Adrian	Adrian	k1gMnSc1	Adrian
Smith	Smith	k1gMnSc1	Smith
a	a	k8xC	a
skupina	skupina	k1gFnSc1	skupina
poprvé	poprvé	k6eAd1	poprvé
doopravdy	doopravdy	k6eAd1	doopravdy
začala	začít	k5eAaPmAgFnS	začít
fungovat	fungovat	k5eAaImF	fungovat
s	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
kytarami	kytara	k1gFnPc7	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
července	červenec	k1gInSc2	červenec
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
1999	[number]	k4	1999
skupina	skupina	k1gFnSc1	skupina
odehrála	odehrát	k5eAaPmAgFnS	odehrát
kratší	krátký	k2eAgNnSc4d2	kratší
turné	turné	k1gNnSc4	turné
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
celá	celý	k2eAgFnSc1d1	celá
šestičlenná	šestičlenný	k2eAgFnSc1d1	šestičlenná
sestava	sestava	k1gFnSc1	sestava
i	i	k9	i
s	s	k7c7	s
novým	nový	k2eAgMnSc7d1	nový
producentem	producent	k1gMnSc7	producent
Kevinem	Kevin	k1gMnSc7	Kevin
Shirleyem	Shirley	k1gMnSc7	Shirley
začala	začít	k5eAaPmAgFnS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
novém	nový	k2eAgNnSc6d1	nové
albu	album	k1gNnSc6	album
nazvaném	nazvaný	k2eAgNnSc6d1	nazvané
Brave	brav	k1gInSc5	brav
New	New	k1gFnPc7	New
World	Worldo	k1gNnPc2	Worldo
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2000	[number]	k4	2000
a	a	k8xC	a
umístilo	umístit	k5eAaPmAgNnS	umístit
se	se	k3xPyFc4	se
na	na	k7c6	na
sedmém	sedmý	k4xOgNnSc6	sedmý
místě	místo	k1gNnSc6	místo
britského	britský	k2eAgInSc2d1	britský
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
Iron	iron	k1gInSc4	iron
Maiden	Maidno	k1gNnPc2	Maidno
odstartovali	odstartovat	k5eAaPmAgMnP	odstartovat
další	další	k2eAgNnSc4d1	další
velké	velký	k2eAgNnSc4d1	velké
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterého	který	k3yQgInSc2	který
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
Paegas	Paegas	kA	Paegas
aréně	aréna	k1gFnSc6	aréna
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
pražské	pražský	k2eAgNnSc4d1	Pražské
vystoupení	vystoupení	k1gNnSc4	vystoupení
skupina	skupina	k1gFnSc1	skupina
během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
turné	turné	k1gNnSc2	turné
hrála	hrát	k5eAaImAgNnP	hrát
i	i	k9	i
na	na	k7c6	na
dalším	další	k2eAgInSc6d1	další
ročníku	ročník	k1gInSc6	ročník
festivalu	festival	k1gInSc2	festival
Rock	rock	k1gInSc1	rock
in	in	k?	in
Rio	Rio	k1gFnSc1	Rio
v	v	k7c6	v
brazilském	brazilský	k2eAgMnSc6d1	brazilský
Rio	Rio	k1gMnSc6	Rio
de	de	k?	de
Janeiru	Janeir	k1gMnSc3	Janeir
<g/>
.	.	kIx.	.
</s>
<s>
Záznam	záznam	k1gInSc1	záznam
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
koncertu	koncert	k1gInSc2	koncert
vyšel	vyjít	k5eAaPmAgInS	vyjít
jako	jako	k9	jako
živé	živý	k2eAgNnSc4d1	živé
album	album	k1gNnSc4	album
a	a	k8xC	a
DVD	DVD	kA	DVD
Rock	rock	k1gInSc1	rock
in	in	k?	in
Rio	Rio	k1gFnSc2	Rio
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
vystoupení	vystoupení	k1gNnSc4	vystoupení
opět	opět	k6eAd1	opět
přišlo	přijít	k5eAaPmAgNnS	přijít
přes	přes	k7c4	přes
250	[number]	k4	250
000	[number]	k4	000
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
alba	album	k1gNnSc2	album
Brave	brav	k1gInSc5	brav
New	New	k1gMnPc4	New
World	World	k1gInSc1	World
skončilo	skončit	k5eAaPmAgNnS	skončit
až	až	k9	až
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2002	[number]	k4	2002
a	a	k8xC	a
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
skupina	skupina	k1gFnSc1	skupina
rovněž	rovněž	k9	rovněž
získala	získat	k5eAaPmAgFnS	získat
ocenění	ocenění	k1gNnSc4	ocenění
Ivor	Ivor	k1gInSc1	Ivor
Novello	Novello	k1gNnSc1	Novello
Awards	Awards	k1gInSc1	Awards
<g/>
.	.	kIx.	.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
období	období	k1gNnSc1	období
klidu	klid	k1gInSc2	klid
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
ho	on	k3xPp3gMnSc4	on
však	však	k9	však
skupina	skupina	k1gFnSc1	skupina
přerušila	přerušit	k5eAaPmAgFnS	přerušit
nahráváním	nahrávání	k1gNnSc7	nahrávání
alba	album	k1gNnPc4	album
Dance	Danka	k1gFnSc6	Danka
of	of	k?	of
Death	Deatha	k1gFnPc2	Deatha
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
pak	pak	k6eAd1	pak
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
září	září	k1gNnSc6	září
<g/>
.	.	kIx.	.
</s>
<s>
Dostalo	dostat	k5eAaPmAgNnS	dostat
se	se	k3xPyFc4	se
na	na	k7c4	na
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
žebříčku	žebříček	k1gInSc2	žebříček
prodejnosti	prodejnost	k1gFnSc2	prodejnost
a	a	k8xC	a
společně	společně	k6eAd1	společně
s	s	k7c7	s
albem	album	k1gNnSc7	album
Brave	brav	k1gInSc5	brav
New	New	k1gFnSc3	New
World	Worlda	k1gFnPc2	Worlda
bylo	být	k5eAaImAgNnS	být
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
nejlepším	dobrý	k2eAgNnSc7d3	nejlepší
metalovým	metalový	k2eAgNnSc7d1	metalové
albem	album	k1gNnSc7	album
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
(	(	kIx(	(
<g/>
Brave	brav	k1gInSc5	brav
New	New	k1gFnPc4	New
World	World	k1gInSc4	World
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
většinou	většina	k1gFnSc7	většina
písně	píseň	k1gFnSc2	píseň
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
délka	délka	k1gFnSc1	délka
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
pět	pět	k4xCc4	pět
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
dvě	dva	k4xCgFnPc1	dva
písně	píseň	k1gFnPc1	píseň
jsou	být	k5eAaImIp3nP	být
kratší	krátký	k2eAgFnPc1d2	kratší
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
dvě	dva	k4xCgFnPc4	dva
vyšly	vyjít	k5eAaPmAgInP	vyjít
jako	jako	k9	jako
singly	singl	k1gInPc1	singl
−	−	k?	−
"	"	kIx"	"
<g/>
Wildest	Wildest	k1gFnSc1	Wildest
Dreams	Dreamsa	k1gFnPc2	Dreamsa
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Rainmaker	Rainmaker	k1gInSc1	Rainmaker
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
vydáním	vydání	k1gNnSc7	vydání
alba	album	k1gNnSc2	album
skupina	skupina	k1gFnSc1	skupina
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
turné	turné	k1gNnSc4	turné
Give	Giv	k1gFnSc2	Giv
Me	Me	k1gMnSc1	Me
Ed	Ed	k1gMnSc1	Ed
<g/>
...	...	k?	...
'	'	kIx"	'
<g/>
Til	til	k1gInSc1	til
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
Dead	Dead	k1gMnSc1	Dead
Tour	Tour	k1gMnSc1	Tour
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
vydání	vydání	k1gNnSc6	vydání
pak	pak	k6eAd1	pak
přibližně	přibližně	k6eAd1	přibližně
stejně	stejně	k6eAd1	stejně
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
turné	turné	k1gNnSc4	turné
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
podporu	podpora	k1gFnSc4	podpora
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
obou	dva	k4xCgMnPc2	dva
skupina	skupina	k1gFnSc1	skupina
hrála	hrát	k5eAaImAgFnS	hrát
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
;	;	kIx,	;
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
a	a	k8xC	a
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
skončilo	skončit	k5eAaPmAgNnS	skončit
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
května	květen	k1gInSc2	květen
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
2005	[number]	k4	2005
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
turné	turné	k1gNnSc3	turné
Eddie	Eddie	k1gFnSc2	Eddie
Rips	rips	k1gInSc1	rips
Up	Up	k1gMnSc1	Up
the	the	k?	the
World	World	k1gMnSc1	World
Tour	Tour	k1gMnSc1	Tour
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zahájeno	zahájit	k5eAaPmNgNnS	zahájit
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
T-Mobile	T-Mobila	k1gFnSc6	T-Mobila
Aréně	aréna	k1gFnSc6	aréna
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
turné	turné	k1gNnSc1	turné
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
několik	několik	k4yIc1	několik
vystoupení	vystoupení	k1gNnPc2	vystoupení
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
putovního	putovní	k2eAgInSc2d1	putovní
festivalu	festival	k1gInSc2	festival
Ozzfest	Ozzfest	k1gFnSc1	Ozzfest
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
<g/>
,	,	kIx,	,
během	během	k7c2	během
vystoupení	vystoupení	k1gNnSc2	vystoupení
v	v	k7c6	v
San	San	k1gMnSc6	San
Bernardinu	bernardin	k1gMnSc6	bernardin
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
napadení	napadení	k1gNnSc3	napadení
kapely	kapela	k1gFnSc2	kapela
některými	některý	k3yIgMnPc7	některý
fanoušky	fanoušek	k1gMnPc7	fanoušek
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
házeli	házet	k5eAaImAgMnP	házet
nejrůznější	různý	k2eAgInPc4d3	nejrůznější
předměty	předmět	k1gInPc4	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
kapela	kapela	k1gFnSc1	kapela
zrušila	zrušit	k5eAaPmAgFnS	zrušit
své	svůj	k3xOyFgInPc4	svůj
zbývající	zbývající	k2eAgInPc4d1	zbývající
koncerty	koncert	k1gInPc4	koncert
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
festivalu	festival	k1gInSc6	festival
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
skupina	skupina	k1gFnSc1	skupina
zakončila	zakončit	k5eAaPmAgFnS	zakončit
charitativním	charitativní	k2eAgNnSc7d1	charitativní
vystoupením	vystoupení	k1gNnSc7	vystoupení
pro	pro	k7c4	pro
bývalého	bývalý	k2eAgMnSc4d1	bývalý
bubeníka	bubeník	k1gMnSc4	bubeník
skupiny	skupina	k1gFnSc2	skupina
Clivea	Cliveus	k1gMnSc4	Cliveus
Burra	Burr	k1gMnSc4	Burr
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
trpěl	trpět	k5eAaImAgMnS	trpět
roztroušenou	roztroušený	k2eAgFnSc7d1	roztroušená
sklerózou	skleróza	k1gFnSc7	skleróza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2005	[number]	k4	2005
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
živé	živý	k2eAgNnSc1d1	živé
album	album	k1gNnSc1	album
Death	Deatha	k1gFnPc2	Deatha
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Road	Roado	k1gNnPc2	Roado
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
zachycen	zachycen	k2eAgInSc1d1	zachycen
záznam	záznam	k1gInSc1	záznam
koncertu	koncert	k1gInSc2	koncert
v	v	k7c6	v
Dortmundu	Dortmund	k1gInSc6	Dortmund
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
součástí	součást	k1gFnSc7	součást
turné	turné	k1gNnSc2	turné
k	k	k7c3	k
albu	album	k1gNnSc3	album
Dance	Danka	k1gFnSc3	Danka
of	of	k?	of
Death	Death	k1gInSc1	Death
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
března	březen	k1gInSc2	březen
do	do	k7c2	do
dubna	duben	k1gInSc2	duben
2006	[number]	k4	2006
Iron	iron	k1gInSc4	iron
Maiden	Maidno	k1gNnPc2	Maidno
nahráli	nahrát	k5eAaBmAgMnP	nahrát
své	své	k1gNnSc4	své
<g/>
,	,	kIx,	,
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
čtrnácté	čtrnáctý	k4xOgFnSc6	čtrnáctý
<g/>
,	,	kIx,	,
album	album	k1gNnSc1	album
nazvané	nazvaný	k2eAgNnSc1d1	nazvané
A	a	k9	a
Matter	Matter	k1gInSc4	Matter
of	of	k?	of
Life	Life	k1gInSc1	Life
and	and	k?	and
Death	Death	k1gInSc1	Death
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
a	a	k8xC	a
produkce	produkce	k1gFnSc1	produkce
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
ujali	ujmout	k5eAaPmAgMnP	ujmout
Shirley	Shirley	k1gInPc4	Shirley
s	s	k7c7	s
Harrisem	Harris	k1gInSc7	Harris
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
samotným	samotný	k2eAgInSc7d1	samotný
počinem	počin	k1gInSc7	počin
vyšel	vyjít	k5eAaPmAgInS	vyjít
první	první	k4xOgInSc1	první
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Reincarnation	Reincarnation	k1gInSc1	Reincarnation
of	of	k?	of
Benjamin	Benjamin	k1gMnSc1	Benjamin
Breeg	Breeg	k1gMnSc1	Breeg
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
"	"	kIx"	"
<g/>
Different	Different	k1gMnSc1	Different
World	World	k1gMnSc1	World
<g/>
"	"	kIx"	"
následoval	následovat	k5eAaImAgInS	následovat
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
nahráli	nahrát	k5eAaPmAgMnP	nahrát
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
Abbey	Abbea	k1gFnSc2	Abbea
Road	Roada	k1gFnPc2	Roada
živé	živý	k2eAgFnSc2d1	živá
vystoupení	vystoupení	k1gNnSc4	vystoupení
pro	pro	k7c4	pro
pořad	pořad	k1gInSc4	pořad
Live	Live	k1gNnSc2	Live
from	fro	k1gNnSc7	fro
Abbey	Abbea	k1gFnSc2	Abbea
Road	Roada	k1gFnPc2	Roada
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
před	před	k7c7	před
tím	ten	k3xDgInSc7	ten
Iron	iron	k1gInSc1	iron
Maiden	Maidna	k1gFnPc2	Maidna
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
manažerem	manažer	k1gMnSc7	manažer
Rodem	rod	k1gInSc7	rod
Smallwoodem	Smallwood	k1gInSc7	Smallwood
ohlásili	ohlásit	k5eAaPmAgMnP	ohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
27	[number]	k4	27
letech	léto	k1gNnPc6	léto
končí	končit	k5eAaImIp3nS	končit
se	se	k3xPyFc4	se
spoluprací	spolupráce	k1gFnSc7	spolupráce
se	s	k7c7	s
Sanctuary	Sanctuar	k1gMnPc7	Sanctuar
Music	Music	k1gMnSc1	Music
<g/>
.	.	kIx.	.
</s>
<s>
Založili	založit	k5eAaPmAgMnP	založit
společnost	společnost	k1gFnSc1	společnost
Phantom	Phantom	k1gInSc1	Phantom
Music	Music	k1gMnSc1	Music
Management	management	k1gInSc1	management
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
nenastaly	nastat	k5eNaPmAgInP	nastat
žádné	žádný	k3yNgFnPc4	žádný
významné	významný	k2eAgFnPc4d1	významná
změny	změna	k1gFnPc4	změna
<g/>
.	.	kIx.	.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
turné	turné	k1gNnSc1	turné
po	po	k7c6	po
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
nejprve	nejprve	k6eAd1	nejprve
odehráli	odehrát	k5eAaPmAgMnP	odehrát
všech	všecek	k3xTgFnPc2	všecek
deset	deset	k4xCc1	deset
skladeb	skladba	k1gFnPc2	skladba
z	z	k7c2	z
aktuálního	aktuální	k2eAgNnSc2d1	aktuální
alba	album	k1gNnSc2	album
v	v	k7c6	v
nezměněném	změněný	k2eNgNnSc6d1	nezměněné
pořadí	pořadí	k1gNnSc6	pořadí
a	a	k8xC	a
poté	poté	k6eAd1	poté
pět	pět	k4xCc4	pět
klasických	klasický	k2eAgFnPc2d1	klasická
skladeb	skladba	k1gFnPc2	skladba
z	z	k7c2	z
jiných	jiný	k2eAgNnPc2d1	jiné
alb	album	k1gNnPc2	album
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tohoto	tento	k3xDgNnSc2	tento
turné	turné	k1gNnSc2	turné
skupina	skupina	k1gFnSc1	skupina
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2007	[number]	k4	2007
odehrála	odehrát	k5eAaPmAgFnS	odehrát
svůj	svůj	k3xOyFgInSc4	svůj
druhý	druhý	k4xOgInSc4	druhý
koncert	koncert	k1gInSc4	koncert
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
vystoupení	vystoupení	k1gNnSc1	vystoupení
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
Download	Download	k1gInSc1	Download
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
natáčeno	natáčet	k5eAaImNgNnS	natáčet
a	a	k8xC	a
vysíláno	vysílat	k5eAaImNgNnS	vysílat
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
koncertu	koncert	k1gInSc2	koncert
Bruce	Bruce	k1gMnSc1	Bruce
Dickinson	Dickinson	k1gMnSc1	Dickinson
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
záznam	záznam	k1gInSc1	záznam
bude	být	k5eAaImBp3nS	být
použit	použít	k5eAaPmNgInS	použít
pro	pro	k7c4	pro
živé	živý	k2eAgNnSc4d1	živé
DVD	DVD	kA	DVD
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2007	[number]	k4	2007
turné	turné	k1gNnSc1	turné
skončilo	skončit	k5eAaPmAgNnS	skončit
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgInSc4d1	poslední
koncert	koncert	k1gInSc4	koncert
byl	být	k5eAaImAgMnS	být
v	v	k7c4	v
Londýnské	londýnský	k2eAgInPc4d1	londýnský
Brixton	Brixton	k1gInSc4	Brixton
Academy	Academa	k1gFnSc2	Academa
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
nadaci	nadace	k1gFnSc4	nadace
Cliva	Cliv	k1gMnSc2	Cliv
Burra	Burr	k1gMnSc2	Burr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2008	[number]	k4	2008
skupina	skupina	k1gFnSc1	skupina
zahájila	zahájit	k5eAaPmAgFnS	zahájit
velké	velký	k2eAgNnSc4d1	velké
turné	turné	k1gNnSc4	turné
Somewhere	Somewher	k1gInSc5	Somewher
Back	Backo	k1gNnPc2	Backo
in	in	k?	in
Time	Tim	k1gFnSc2	Tim
World	World	k1gMnSc1	World
Tour	Tour	k1gMnSc1	Tour
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
zaměřovalo	zaměřovat	k5eAaImAgNnS	zaměřovat
na	na	k7c4	na
slavné	slavný	k2eAgNnSc4d1	slavné
období	období	k1gNnSc4	období
skupiny	skupina	k1gFnSc2	skupina
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
na	na	k7c4	na
album	album	k1gNnSc4	album
Powerslave	Powerslav	k1gMnSc5	Powerslav
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
celkové	celkový	k2eAgNnSc4d1	celkové
ladění	ladění	k1gNnSc4	ladění
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
egyptské	egyptský	k2eAgInPc4d1	egyptský
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
na	na	k7c6	na
albu	album	k1gNnSc6	album
a	a	k8xC	a
tehdejších	tehdejší	k2eAgNnPc6d1	tehdejší
vystoupeních	vystoupení	k1gNnPc6	vystoupení
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
indické	indický	k2eAgFnSc6d1	indická
Bombaji	Bombaj	k1gFnSc6	Bombaj
a	a	k8xC	a
také	také	k9	také
poprvé	poprvé	k6eAd1	poprvé
navštívili	navštívit	k5eAaPmAgMnP	navštívit
Kostariku	Kostarika	k1gFnSc4	Kostarika
a	a	k8xC	a
Kolumbii	Kolumbie	k1gFnSc4	Kolumbie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2008	[number]	k4	2008
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
Eden	Eden	k1gInSc1	Eden
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
začátkem	začátek	k1gInSc7	začátek
tohoto	tento	k3xDgNnSc2	tento
turné	turné	k1gNnSc2	turné
také	také	k9	také
souvisí	souviset	k5eAaImIp3nS	souviset
vydání	vydání	k1gNnSc1	vydání
živého	živý	k2eAgInSc2d1	živý
dvou-DVD	dvou-DVD	k?	dvou-DVD
se	s	k7c7	s
záznamy	záznam	k1gInPc7	záznam
Live	Liv	k1gFnSc2	Liv
After	After	k1gMnSc1	After
Death	Death	k1gMnSc1	Death
a	a	k8xC	a
Maiden	Maidno	k1gNnPc2	Maidno
England	Englanda	k1gFnPc2	Englanda
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
turné	turné	k1gNnSc6	turné
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
dokumentární	dokumentární	k2eAgInSc4d1	dokumentární
film	film	k1gInSc4	film
Iron	iron	k1gInSc1	iron
Maiden	Maidna	k1gFnPc2	Maidna
<g/>
:	:	kIx,	:
Flight	Flight	k1gInSc1	Flight
666	[number]	k4	666
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
turné	turné	k1gNnSc2	turné
skupina	skupina	k1gFnSc1	skupina
cestovala	cestovat	k5eAaImAgFnS	cestovat
speciálně	speciálně	k6eAd1	speciálně
upraveným	upravený	k2eAgInSc7d1	upravený
Boeingem	boeing	k1gInSc7	boeing
757	[number]	k4	757
zvaným	zvaný	k2eAgNnSc7d1	zvané
"	"	kIx"	"
<g/>
Ed	Ed	k1gMnSc1	Ed
Force	force	k1gFnSc2	force
One	One	k1gMnSc1	One
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
pilotoval	pilotovat	k5eAaImAgMnS	pilotovat
zpěvák	zpěvák	k1gMnSc1	zpěvák
skupiny	skupina	k1gFnSc2	skupina
Bruce	Bruce	k1gMnSc1	Bruce
Dickinson	Dickinson	k1gMnSc1	Dickinson
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ledna	leden	k1gInSc2	leden
do	do	k7c2	do
března	březen	k1gInSc2	březen
2010	[number]	k4	2010
skupina	skupina	k1gFnSc1	skupina
pracovala	pracovat	k5eAaImAgFnS	pracovat
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
patnáctém	patnáctý	k4xOgNnSc6	patnáctý
studiovém	studiový	k2eAgNnSc6d1	studiové
albu	album	k1gNnSc6	album
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
dostal	dostat	k5eAaPmAgInS	dostat
název	název	k1gInSc4	název
The	The	k1gMnPc2	The
Final	Final	k1gMnSc1	Final
Frontier	Frontier	k1gMnSc1	Frontier
a	a	k8xC	a
vyšel	vyjít	k5eAaPmAgMnS	vyjít
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Produkce	produkce	k1gFnSc1	produkce
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
ujali	ujmout	k5eAaPmAgMnP	ujmout
Shirley	Shirley	k1gInPc4	Shirley
a	a	k8xC	a
Harris	Harris	k1gFnPc4	Harris
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
2011	[number]	k4	2011
skupina	skupina	k1gFnSc1	skupina
odehrála	odehrát	k5eAaPmAgFnS	odehrát
přibližně	přibližně	k6eAd1	přibližně
sto	sto	k4xCgNnSc1	sto
koncertů	koncert	k1gInPc2	koncert
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
The	The	k1gMnSc1	The
Final	Final	k1gMnSc1	Final
Frontier	Frontier	k1gMnSc1	Frontier
World	World	k1gMnSc1	World
Tour	Tour	k1gMnSc1	Tour
<g/>
.	.	kIx.	.
</s>
<s>
Tentokrát	tentokrát	k6eAd1	tentokrát
skupina	skupina	k1gFnSc1	skupina
odehrála	odehrát	k5eAaPmAgFnS	odehrát
i	i	k9	i
několik	několik	k4yIc4	několik
koncertů	koncert	k1gInPc2	koncert
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
putovního	putovní	k2eAgInSc2d1	putovní
festivalu	festival	k1gInSc2	festival
Sonisphere	Sonispher	k1gInSc5	Sonispher
<g/>
;	;	kIx,	;
jedno	jeden	k4xCgNnSc1	jeden
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
na	na	k7c6	na
Holešovickém	holešovický	k2eAgNnSc6d1	holešovické
výstavišti	výstaviště	k1gNnSc6	výstaviště
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2012	[number]	k4	2012
vyšel	vyjít	k5eAaPmAgInS	vyjít
záznam	záznam	k1gInSc1	záznam
z	z	k7c2	z
koncertu	koncert	k1gInSc2	koncert
nazvaný	nazvaný	k2eAgMnSc1d1	nazvaný
En	En	k1gMnSc1	En
Vivo	vivo	k6eAd1	vivo
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
stejného	stejný	k2eAgInSc2d1	stejný
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
vydala	vydat	k5eAaPmAgFnS	vydat
na	na	k7c4	na
další	další	k2eAgNnSc4d1	další
světové	světový	k2eAgNnSc4d1	světové
turné	turné	k1gNnSc4	turné
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
Maiden	Maidna	k1gFnPc2	Maidna
England	England	k1gInSc1	England
World	World	k1gMnSc1	World
Tour	Tour	k1gMnSc1	Tour
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
2013	[number]	k4	2013
<g/>
;	;	kIx,	;
jeden	jeden	k4xCgInSc1	jeden
koncert	koncert	k1gInSc1	koncert
z	z	k7c2	z
turné	turné	k1gNnSc2	turné
odehráli	odehrát	k5eAaPmAgMnP	odehrát
i	i	k9	i
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
29	[number]	k4	29
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2013	[number]	k4	2013
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
aréně	aréna	k1gFnSc6	aréna
Eden	Eden	k1gInSc4	Eden
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2013	[number]	k4	2013
zemřel	zemřít	k5eAaPmAgMnS	zemřít
bubeník	bubeník	k1gMnSc1	bubeník
Clive	Cliev	k1gFnSc2	Cliev
Burr	Burr	k1gMnSc1	Burr
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
skupinou	skupina	k1gFnSc7	skupina
nahrál	nahrát	k5eAaPmAgMnS	nahrát
první	první	k4xOgNnPc4	první
tři	tři	k4xCgNnPc4	tři
alba	album	k1gNnPc4	album
<g/>
,	,	kIx,	,
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
56	[number]	k4	56
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
vydal	vydat	k5eAaPmAgMnS	vydat
Smith	Smith	k1gMnSc1	Smith
společně	společně	k6eAd1	společně
s	s	k7c7	s
projektem	projekt	k1gInSc7	projekt
Primal	Primal	k1gInSc1	Primal
Rock	rock	k1gInSc1	rock
Rebellion	Rebellion	k?	Rebellion
album	album	k1gNnSc4	album
Awoken	Awokno	k1gNnPc2	Awokno
Broken	Brokna	k1gFnPc2	Brokna
a	a	k8xC	a
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
vydal	vydat	k5eAaPmAgMnS	vydat
první	první	k4xOgNnSc4	první
sólové	sólový	k2eAgNnSc4d1	sólové
album	album	k1gNnSc4	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
British	British	k1gMnSc1	British
Lion	Lion	k1gMnSc1	Lion
i	i	k8xC	i
Steve	Steve	k1gMnSc1	Steve
Harris	Harris	k1gFnSc2	Harris
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2012	[number]	k4	2012
Dickinson	Dickinsona	k1gFnPc2	Dickinsona
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
skupina	skupina	k1gFnSc1	skupina
chystá	chystat	k5eAaImIp3nS	chystat
na	na	k7c4	na
rok	rok	k1gInSc4	rok
2014	[number]	k4	2014
nové	nový	k2eAgNnSc4d1	nové
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
byla	být	k5eAaImAgFnS	být
Dickinsonovi	Dickinsonův	k2eAgMnPc1d1	Dickinsonův
diagnostikována	diagnostikován	k2eAgFnSc1d1	diagnostikována
rakovina	rakovina	k1gFnSc1	rakovina
a	a	k8xC	a
přestože	přestože	k8xS	přestože
měla	mít	k5eAaImAgFnS	mít
skupina	skupina	k1gFnSc1	skupina
kompletně	kompletně	k6eAd1	kompletně
dokončené	dokončený	k2eAgNnSc4d1	dokončené
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc4	její
členové	člen	k1gMnPc1	člen
prohlásili	prohlásit	k5eAaPmAgMnP	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
vydáním	vydání	k1gNnSc7	vydání
počkají	počkat	k5eAaPmIp3nP	počkat
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
bude	být	k5eAaImBp3nS	být
frontman	frontman	k1gMnSc1	frontman
zdráv	zdráv	k2eAgMnSc1d1	zdráv
a	a	k8xC	a
vyrazí	vyrazit	k5eAaPmIp3nS	vyrazit
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
na	na	k7c4	na
koncertní	koncertní	k2eAgNnSc4d1	koncertní
turné	turné	k1gNnSc4	turné
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
nakonec	nakonec	k6eAd1	nakonec
dostalo	dostat	k5eAaPmAgNnS	dostat
název	název	k1gInSc4	název
The	The	k1gMnPc2	The
Book	Book	k1gMnSc1	Book
of	of	k?	of
Souls	Souls	k1gInSc1	Souls
a	a	k8xC	a
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
září	září	k1gNnSc6	září
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Nahrávka	nahrávka	k1gFnSc1	nahrávka
je	být	k5eAaImIp3nS	být
nejdelším	dlouhý	k2eAgNnSc7d3	nejdelší
studiovým	studiový	k2eAgNnSc7d1	studiové
albem	album	k1gNnSc7	album
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
album	album	k1gNnSc4	album
navázalo	navázat	k5eAaPmAgNnS	navázat
celosvětové	celosvětový	k2eAgNnSc4d1	celosvětové
turné	turné	k1gNnSc4	turné
The	The	k1gFnSc2	The
Book	Book	k1gMnSc1	Book
of	of	k?	of
Souls	Souls	k1gInSc1	Souls
World	World	k1gMnSc1	World
Tour	Tour	k1gMnSc1	Tour
čítající	čítající	k2eAgMnSc1d1	čítající
více	hodně	k6eAd2	hodně
než	než	k8xS	než
sto	sto	k4xCgNnSc4	sto
vystoupení	vystoupení	k1gNnSc4	vystoupení
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc4	seznam
členů	člen	k1gMnPc2	člen
Iron	iron	k1gInSc1	iron
Maiden	Maidna	k1gFnPc2	Maidna
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
sestava	sestava	k1gFnSc1	sestava
Bruce	Bruce	k1gMnSc1	Bruce
Dickinson	Dickinson	k1gMnSc1	Dickinson
–	–	k?	–
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
–	–	k?	–
<g/>
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
–	–	k?	–
<g/>
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
Steve	Steve	k1gMnSc1	Steve
Harris	Harris	k1gFnSc2	Harris
–	–	k?	–
baskytara	baskytara	k1gFnSc1	baskytara
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
–	–	k?	–
<g/>
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
Dave	Dav	k1gInSc5	Dav
Murray	Murra	k1gMnPc4	Murra
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
<g/>
–	–	k?	–
<g/>
dosud	dosud	k6eAd1	dosud
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Adrian	Adrian	k1gMnSc1	Adrian
Smith	Smith	k1gMnSc1	Smith
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
–	–	k?	–
<g/>
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
Janick	Janick	k1gInSc1	Janick
Gers	Gers	k1gInSc1	Gers
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
Nicko	nicka	k1gFnSc5	nicka
McBrain	McBrain	k2eAgInSc4d1	McBrain
–	–	k?	–
bicí	bicí	k2eAgNnSc1d1	bicí
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
–	–	k?	–
<g/>
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
a	a	k8xC	a
Michael	Michael	k1gMnSc1	Michael
Kenney	Kennea	k1gFnSc2	Kennea
–	–	k?	–
klávesy	klávesa	k1gFnSc2	klávesa
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
–	–	k?	–
<g/>
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
–	–	k?	–
pouze	pouze	k6eAd1	pouze
koncertní	koncertní	k2eAgMnSc1d1	koncertní
hudebník	hudebník	k1gMnSc1	hudebník
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Diskografie	diskografie	k1gFnSc1	diskografie
Iron	iron	k1gInSc1	iron
Maiden	Maidno	k1gNnPc2	Maidno
<g/>
.	.	kIx.	.
</s>
<s>
Studiová	studiový	k2eAgNnPc1d1	studiové
alba	album	k1gNnPc1	album
Iron	iron	k1gInSc1	iron
Maiden	Maidna	k1gFnPc2	Maidna
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
Killers	Killersa	k1gFnPc2	Killersa
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Number	Number	k1gMnSc1	Number
of	of	k?	of
the	the	k?	the
Beast	Beast	k1gInSc1	Beast
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
Piece	pieca	k1gFnSc3	pieca
of	of	k?	of
Mind	Mind	k1gInSc1	Mind
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
Powerslave	Powerslav	k1gMnSc5	Powerslav
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
Somewhere	Somewher	k1gInSc5	Somewher
in	in	k?	in
Time	Time	k1gNnPc1	Time
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
Seventh	Seventh	k1gInSc1	Seventh
Son	son	k1gInSc1	son
of	of	k?	of
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
Seventh	Seventh	k1gMnSc1	Seventh
Son	son	k1gInSc1	son
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
No	no	k9	no
Prayer	Prayer	k1gMnSc1	Prayer
for	forum	k1gNnPc2	forum
the	the	k?	the
Dying	Dying	k1gMnSc1	Dying
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Fear	Fear	k1gInSc1	Fear
of	of	k?	of
the	the	k?	the
Dark	Dark	k1gInSc1	Dark
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
The	The	k1gFnSc2	The
X	X	kA	X
Factor	Factor	k1gMnSc1	Factor
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Virtual	Virtual	k1gInSc1	Virtual
XI	XI	kA	XI
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Brave	brav	k1gInSc5	brav
New	New	k1gMnSc7	New
World	World	k1gMnSc1	World
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Dance	Danka	k1gFnSc3	Danka
of	of	k?	of
Death	Death	k1gInSc1	Death
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
A	a	k8xC	a
Matter	Matter	k1gInSc1	Matter
of	of	k?	of
Life	Life	k1gInSc1	Life
and	and	k?	and
Death	Death	k1gInSc1	Death
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Final	Final	k1gMnSc1	Final
Frontier	Frontier	k1gMnSc1	Frontier
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Book	Book	k1gMnSc1	Book
of	of	k?	of
Souls	Souls	k1gInSc1	Souls
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
</s>
