<p>
<s>
Kishu-Inu	Kishu-Inout	k5eAaPmIp1nS	Kishu-Inout
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
Kishu	Kish	k1gMnSc6	Kish
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
Kishu	Kisha	k1gFnSc4	Kisha
Ken	Ken	k1gFnSc2	Ken
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
psí	psí	k2eAgNnSc1d1	psí
plemeno	plemeno	k1gNnSc1	plemeno
<g/>
,	,	kIx,	,
vyšlechtěné	vyšlechtěný	k2eAgNnSc1d1	vyšlechtěné
před	před	k7c7	před
tisíci	tisíc	k4xCgInPc7	tisíc
lety	léto	k1gNnPc7	léto
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
pro	pro	k7c4	pro
lov	lov	k1gInSc4	lov
jelenů	jelen	k1gMnPc2	jelen
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
po	po	k7c6	po
regionu	region	k1gInSc6	region
Kišu	Kišus	k1gInSc2	Kišus
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Kii	Kii	k1gMnSc2	Kii
<g/>
,	,	kIx,	,
nacházejícího	nacházející	k2eAgMnSc2d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Honšú	Honšú	k1gFnSc2	Honšú
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
středně	středně	k6eAd1	středně
velké	velký	k2eAgNnSc4d1	velké
psí	psí	k2eAgNnSc4d1	psí
plemeno	plemeno	k1gNnSc4	plemeno
typu	typ	k1gInSc2	typ
špice	špice	k1gFnSc2	špice
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
uznáno	uznat	k5eAaPmNgNnS	uznat
Mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
kynologickou	kynologický	k2eAgFnSc7d1	kynologická
federací	federace	k1gFnSc7	federace
(	(	kIx(	(
<g/>
FCI	FCI	kA	FCI
<g/>
)	)	kIx)	)
a	a	k8xC	a
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
i	i	k9	i
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzhled	vzhled	k1gInSc4	vzhled
==	==	k?	==
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
středně	středně	k6eAd1	středně
velké	velký	k2eAgNnSc4d1	velké
psí	psí	k2eAgNnSc4d1	psí
plemeno	plemeno	k1gNnSc4	plemeno
lehké	lehký	k2eAgNnSc4d1	lehké
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dobře	dobře	k6eAd1	dobře
osvalené	osvalený	k2eAgFnSc2d1	osvalená
konstrukce	konstrukce	k1gFnSc2	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Srst	srst	k1gFnSc1	srst
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
celé	celý	k2eAgNnSc4d1	celé
tělo	tělo	k1gNnSc4	tělo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
krátká	krátký	k2eAgFnSc1d1	krátká
<g/>
,	,	kIx,	,
na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
středně	středně	k6eAd1	středně
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
přiléhavá	přiléhavý	k2eAgFnSc1d1	přiléhavá
a	a	k8xC	a
hustá	hustý	k2eAgFnSc1d1	hustá
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
dotek	dotek	k1gInSc4	dotek
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
hladká	hladký	k2eAgFnSc1d1	hladká
a	a	k8xC	a
jemná	jemný	k2eAgFnSc1d1	jemná
a	a	k8xC	a
tradiční	tradiční	k2eAgNnSc1d1	tradiční
zbarvení	zbarvení	k1gNnSc1	zbarvení
je	být	k5eAaImIp3nS	být
červené	červený	k2eAgNnSc1d1	červené
nebo	nebo	k8xC	nebo
sezamové	sezamový	k2eAgNnSc1d1	sezamové
<g/>
,	,	kIx,	,
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
ale	ale	k9	ale
i	i	k9	i
bílé	bílý	k2eAgFnPc1d1	bílá
či	či	k8xC	či
rezavé	rezavý	k2eAgFnPc1d1	rezavá
<g/>
.	.	kIx.	.
</s>
<s>
Ideální	ideální	k2eAgFnSc1d1	ideální
výška	výška	k1gFnSc1	výška
v	v	k7c6	v
kohoutku	kohoutek	k1gInSc6	kohoutek
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
feny	fena	k1gFnSc2	fena
49	[number]	k4	49
a	a	k8xC	a
u	u	k7c2	u
psa	pes	k1gMnSc2	pes
52	[number]	k4	52
cm	cm	kA	cm
<g/>
,	,	kIx,	,
s	s	k7c7	s
tolerancí	tolerance	k1gFnSc7	tolerance
+	+	kIx~	+
<g/>
-	-	kIx~	-
3	[number]	k4	3
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlava	hlava	k1gFnSc1	hlava
je	být	k5eAaImIp3nS	být
přiměřeně	přiměřeně	k6eAd1	přiměřeně
velká	velký	k2eAgFnSc1d1	velká
<g/>
,	,	kIx,	,
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
mozkovny	mozkovna	k1gFnSc2	mozkovna
ku	k	k7c3	k
čumáku	čumák	k1gInSc3	čumák
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Uši	ucho	k1gNnPc1	ucho
jsou	být	k5eAaImIp3nP	být
středně	středně	k6eAd1	středně
velké	velký	k2eAgFnPc1d1	velká
<g/>
,	,	kIx,	,
trojúhelníkovité	trojúhelníkovitý	k2eAgFnPc1d1	trojúhelníkovitá
<g/>
,	,	kIx,	,
natočené	natočený	k2eAgFnPc1d1	natočená
vždy	vždy	k6eAd1	vždy
dopředu	dopředu	k6eAd1	dopředu
<g/>
.	.	kIx.	.
</s>
<s>
Oči	oko	k1gNnPc1	oko
jsou	být	k5eAaImIp3nP	být
spíše	spíše	k9	spíše
mandlového	mandlový	k2eAgInSc2d1	mandlový
tvaru	tvar	k1gInSc2	tvar
v	v	k7c6	v
barvě	barva	k1gFnSc6	barva
oříšků	oříšek	k1gInPc2	oříšek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bez	bez	k7c2	bez
viditelného	viditelný	k2eAgNnSc2d1	viditelné
bělma	bělmo	k1gNnSc2	bělmo
<g/>
.	.	kIx.	.
</s>
<s>
Nosní	nosní	k2eAgFnSc1d1	nosní
houba	houba	k1gFnSc1	houba
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
bílých	bílý	k2eAgFnPc6d1	bílá
variantách	varianta	k1gFnPc6	varianta
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
růžová	růžový	k2eAgFnSc1d1	růžová
či	či	k8xC	či
hnědá	hnědý	k2eAgFnSc1d1	hnědá
<g/>
.	.	kIx.	.
</s>
<s>
Skus	skus	k1gInSc1	skus
nůžkový	nůžkový	k2eAgInSc1d1	nůžkový
<g/>
,	,	kIx,	,
se	s	k7c7	s
silnými	silný	k2eAgInPc7d1	silný
zuby	zub	k1gInPc7	zub
<g/>
.	.	kIx.	.
</s>
<s>
Krk	krk	k1gInSc1	krk
je	být	k5eAaImIp3nS	být
silný	silný	k2eAgInSc1d1	silný
a	a	k8xC	a
široký	široký	k2eAgInSc1d1	široký
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
krátký	krátký	k2eAgInSc4d1	krátký
a	a	k8xC	a
klenutý	klenutý	k2eAgInSc4d1	klenutý
<g/>
.	.	kIx.	.
</s>
<s>
Srst	srst	k1gFnSc1	srst
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
tvoří	tvořit	k5eAaImIp3nP	tvořit
límec	límec	k1gInSc4	límec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
napojuje	napojovat	k5eAaImIp3nS	napojovat
na	na	k7c4	na
hřbet	hřbet	k1gInSc4	hřbet
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nepatrná	nepatrný	k2eAgFnSc1d1	nepatrná
prohlubeň	prohlubeň	k1gFnSc1	prohlubeň
<g/>
.	.	kIx.	.
</s>
<s>
Hřbet	hřbet	k1gInSc1	hřbet
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
<g/>
,	,	kIx,	,
široký	široký	k2eAgInSc1d1	široký
<g/>
,	,	kIx,	,
hustě	hustě	k6eAd1	hustě
porostlý	porostlý	k2eAgInSc1d1	porostlý
srstí	srst	k1gFnSc7	srst
<g/>
,	,	kIx,	,
zakončen	zakončen	k2eAgInSc1d1	zakončen
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
ocasem	ocas	k1gInSc7	ocas
šavlovitého	šavlovitý	k2eAgInSc2d1	šavlovitý
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
tvoří	tvořit	k5eAaImIp3nS	tvořit
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
srst	srst	k1gFnSc1	srst
prapor	prapor	k1gInSc1	prapor
<g/>
.	.	kIx.	.
</s>
<s>
Nohy	noha	k1gFnPc1	noha
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
,	,	kIx,	,
svalnaté	svalnatý	k2eAgFnPc1d1	svalnatá
<g/>
,	,	kIx,	,
zakončené	zakončený	k2eAgFnPc1d1	zakončená
kulatými	kulatý	k2eAgFnPc7d1	kulatá
tlapkami	tlapka	k1gFnPc7	tlapka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Podobná	podobný	k2eAgNnPc1d1	podobné
plemena	plemeno	k1gNnPc1	plemeno
===	===	k?	===
</s>
</p>
<p>
<s>
Kishu-Inu	Kishu-Ina	k1gFnSc4	Kishu-Ina
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
japonské	japonský	k2eAgNnSc1d1	Japonské
psí	psí	k2eAgNnSc1d1	psí
plemeno	plemeno	k1gNnSc1	plemeno
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
si	se	k3xPyFc3	se
lze	lze	k6eAd1	lze
splést	splést	k5eAaPmF	splést
hned	hned	k6eAd1	hned
s	s	k7c7	s
několika	několik	k4yIc7	několik
tamějšími	tamější	k2eAgMnPc7d1	tamější
špici	špic	k1gMnPc7	špic
<g/>
:	:	kIx,	:
Hokkaido-Ken	Hokkaido-Kna	k1gFnPc2	Hokkaido-Kna
<g/>
,	,	kIx,	,
Šikoku	Šikok	k1gInSc2	Šikok
Inu	inu	k9	inu
a	a	k8xC	a
Kai	Kai	k1gFnSc1	Kai
Inu	inu	k9	inu
<g/>
.	.	kIx.	.
</s>
<s>
Hokkaido-Ken	Hokkaido-Ken	k1gInSc1	Hokkaido-Ken
je	být	k5eAaImIp3nS	být
plemeno	plemeno	k1gNnSc4	plemeno
nižšího	nízký	k2eAgInSc2d2	nižší
vzrůstu	vzrůst	k1gInSc2	vzrůst
<g/>
,	,	kIx,	,
mohutnější	mohutný	k2eAgFnSc2d2	mohutnější
konstituce	konstituce	k1gFnSc2	konstituce
a	a	k8xC	a
s	s	k7c7	s
mírně	mírně	k6eAd1	mírně
delší	dlouhý	k2eAgFnSc7d2	delší
srstí	srst	k1gFnSc7	srst
<g/>
.	.	kIx.	.
</s>
<s>
Liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
i	i	k9	i
povolená	povolený	k2eAgNnPc1d1	povolené
zbarvení	zbarvení	k1gNnPc1	zbarvení
<g/>
.	.	kIx.	.
</s>
<s>
Šikoku	Šikok	k1gInSc3	Šikok
Inu	inu	k9	inu
je	být	k5eAaImIp3nS	být
málo	málo	k6eAd1	málo
známé	známý	k2eAgNnSc1d1	známé
psí	psí	k2eAgNnSc1d1	psí
plemeno	plemeno	k1gNnSc1	plemeno
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
Kishu-Inu	Kishu-In	k1gInSc2	Kishu-In
liší	lišit	k5eAaImIp3nS	lišit
hlavně	hlavně	k9	hlavně
zbarvením	zbarvení	k1gNnSc7	zbarvení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
vlkošedé	vlkošedý	k2eAgNnSc1d1	vlkošedé
se	s	k7c7	s
světlou	světlý	k2eAgFnSc7d1	světlá
maskou	maska	k1gFnSc7	maska
(	(	kIx(	(
<g/>
zvanou	zvaný	k2eAgFnSc7d1	zvaná
Urajiro	Urajira	k1gMnSc5	Urajira
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
psí	psí	k2eAgNnSc1d1	psí
plemeno	plemeno	k1gNnSc1	plemeno
stejně	stejně	k6eAd1	stejně
velké	velký	k2eAgNnSc1d1	velké
i	i	k8xC	i
těžké	těžký	k2eAgNnSc1d1	těžké
<g/>
.	.	kIx.	.
</s>
<s>
Kai	Kai	k?	Kai
Inu	inu	k9	inu
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
neznámé	známý	k2eNgNnSc4d1	neznámé
japonské	japonský	k2eAgNnSc4d1	Japonské
psí	psí	k2eAgNnSc4d1	psí
plemeno	plemeno	k1gNnSc4	plemeno
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc4	ten
je	být	k5eAaImIp3nS	být
má	mít	k5eAaImIp3nS	mít
ale	ale	k9	ale
žíhané	žíhaný	k2eAgNnSc4d1	žíhané
zbarvení	zbarvení	k1gNnSc4	zbarvení
a	a	k8xC	a
kratší	krátký	k2eAgFnSc4d2	kratší
srst	srst	k1gFnSc4	srst
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Povaha	povaha	k1gFnSc1	povaha
==	==	k?	==
</s>
</p>
<p>
<s>
Kishu-Inu	Kishu-Ina	k1gFnSc4	Kishu-Ina
je	být	k5eAaImIp3nS	být
loajální	loajální	k2eAgMnSc1d1	loajální
<g/>
,	,	kIx,	,
oddaný	oddaný	k2eAgMnSc1d1	oddaný
a	a	k8xC	a
energický	energický	k2eAgMnSc1d1	energický
pes	pes	k1gMnSc1	pes
s	s	k7c7	s
bystrým	bystrý	k2eAgInSc7d1	bystrý
výrazem	výraz	k1gInSc7	výraz
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
bystré	bystrý	k2eAgNnSc4d1	bystré
<g/>
,	,	kIx,	,
inteligentní	inteligentní	k2eAgNnSc4d1	inteligentní
a	a	k8xC	a
učenlivé	učenlivý	k2eAgNnSc4d1	učenlivé
plemeno	plemeno	k1gNnSc4	plemeno
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
výcvik	výcvik	k1gInSc1	výcvik
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
snadný	snadný	k2eAgInSc1d1	snadný
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
psi	pes	k1gMnPc1	pes
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
samostatní	samostatný	k2eAgMnPc1d1	samostatný
a	a	k8xC	a
pokud	pokud	k8xS	pokud
ve	v	k7c6	v
svém	svůj	k3xOyFgMnSc6	svůj
majiteli	majitel	k1gMnSc6	majitel
nevidí	vidět	k5eNaImIp3nS	vidět
dostatečného	dostatečný	k2eAgMnSc4d1	dostatečný
vůdce	vůdce	k1gMnSc4	vůdce
smečky	smečka	k1gFnSc2	smečka
<g/>
,	,	kIx,	,
jednoduše	jednoduše	k6eAd1	jednoduše
ho	on	k3xPp3gMnSc4	on
ignorují	ignorovat	k5eAaImIp3nP	ignorovat
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
sami	sám	k3xTgMnPc1	sám
snaží	snažit	k5eAaImIp3nP	snažit
dostat	dostat	k5eAaPmF	dostat
na	na	k7c4	na
jeho	jeho	k3xOp3gInSc4	jeho
post	post	k1gInSc4	post
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
dominantního	dominantní	k2eAgMnSc4d1	dominantní
psa	pes	k1gMnSc4	pes
<g/>
.	.	kIx.	.
</s>
<s>
Kishu-Inu	Kishu-Ina	k1gFnSc4	Kishu-Ina
je	být	k5eAaImIp3nS	být
tichý	tichý	k2eAgInSc1d1	tichý
<g/>
,	,	kIx,	,
málokdy	málokdy	k6eAd1	málokdy
štěká	štěkat	k5eAaImIp3nS	štěkat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
rodinný	rodinný	k2eAgMnSc1d1	rodinný
pes	pes	k1gMnSc1	pes
<g/>
,	,	kIx,	,
mající	mající	k2eAgFnSc4d1	mající
odvahu	odvaha	k1gFnSc4	odvaha
ke	k	k7c3	k
chránění	chránění	k1gNnSc3	chránění
své	svůj	k3xOyFgFnSc2	svůj
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
loví	lovit	k5eAaImIp3nP	lovit
menší	malý	k2eAgNnPc1d2	menší
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dobré	dobrý	k2eAgFnSc6d1	dobrá
socializaci	socializace	k1gFnSc6	socializace
vychází	vycházet	k5eAaImIp3nS	vycházet
dobře	dobře	k6eAd1	dobře
s	s	k7c7	s
jinými	jiný	k2eAgMnPc7d1	jiný
psy	pes	k1gMnPc7	pes
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
brát	brát	k5eAaImF	brát
na	na	k7c6	na
mysli	mysl	k1gFnSc6	mysl
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
dominantní	dominantní	k2eAgNnSc4d1	dominantní
psí	psí	k2eAgNnSc4d1	psí
plemeno	plemeno	k1gNnSc4	plemeno
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
si	se	k3xPyFc3	se
nic	nic	k3yNnSc1	nic
nenechá	nechat	k5eNaPmIp3nS	nechat
líbit	líbit	k5eAaImF	líbit
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jinými	jiný	k2eAgNnPc7d1	jiné
zvířaty	zvíře	k1gNnPc7	zvíře
dobře	dobře	k6eAd1	dobře
nevychází	vycházet	k5eNaImIp3nS	vycházet
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gInPc3	jeho
loveckým	lovecký	k2eAgInPc3d1	lovecký
pudům	pud	k1gInPc3	pud
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
naplno	naplno	k6eAd1	naplno
pracují	pracovat	k5eAaImIp3nP	pracovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nároky	nárok	k1gInPc1	nárok
==	==	k?	==
</s>
</p>
<p>
<s>
Kishu-Inu	Kishu-Ina	k1gFnSc4	Kishu-Ina
nemá	mít	k5eNaImIp3nS	mít
příliš	příliš	k6eAd1	příliš
velké	velký	k2eAgInPc4d1	velký
nároky	nárok	k1gInPc4	nárok
<g/>
:	:	kIx,	:
jeho	jeho	k3xOp3gFnSc1	jeho
srst	srst	k1gFnSc1	srst
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
alespoň	alespoň	k9	alespoň
1	[number]	k4	1
<g/>
x	x	k?	x
týdne	týden	k1gInSc2	týden
vyčesat	vyčesat	k5eAaPmF	vyčesat
<g/>
,	,	kIx,	,
drápky	drápek	k1gInPc1	drápek
zastřihnout	zastřihnout	k5eAaPmF	zastřihnout
jen	jen	k6eAd1	jen
v	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
ani	ani	k8xC	ani
speciální	speciální	k2eAgNnSc1d1	speciální
krmení	krmení	k1gNnSc1	krmení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
línání	línání	k1gNnSc2	línání
se	se	k3xPyFc4	se
majitel	majitel	k1gMnSc1	majitel
musí	muset	k5eAaImIp3nS	muset
srst	srst	k1gFnSc4	srst
věnovat	věnovat	k5eAaPmF	věnovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
obvykle	obvykle	k6eAd1	obvykle
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
každodenního	každodenní	k2eAgNnSc2d1	každodenní
vyčesávání	vyčesávání	k1gNnSc2	vyčesávání
<g/>
.	.	kIx.	.
</s>
<s>
Dobré	dobrý	k2eAgNnSc1d1	dobré
je	být	k5eAaImIp3nS	být
opakovat	opakovat	k5eAaImF	opakovat
naučené	naučený	k2eAgInPc4d1	naučený
cviky	cvik	k1gInPc4	cvik
nebo	nebo	k8xC	nebo
psa	pes	k1gMnSc4	pes
brát	brát	k5eAaImF	brát
do	do	k7c2	do
společnosti	společnost	k1gFnSc2	společnost
jiných	jiný	k2eAgMnPc2d1	jiný
psů	pes	k1gMnPc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
Kishu-Inu	Kishu-Ina	k1gFnSc4	Kishu-Ina
trpí	trpět	k5eAaImIp3nP	trpět
entropií	entropie	k1gFnPc2	entropie
–	–	k?	–
problémy	problém	k1gInPc1	problém
s	s	k7c7	s
očními	oční	k2eAgNnPc7d1	oční
víčky	víčko	k1gNnPc7	víčko
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
psí	psí	k2eAgNnSc4d1	psí
plemeno	plemeno	k1gNnSc4	plemeno
<g/>
,	,	kIx,	,
kterému	který	k3yIgNnSc3	který
nevadí	vadit	k5eNaImIp3nS	vadit
celoroční	celoroční	k2eAgInSc1d1	celoroční
pobyt	pobyt	k1gInSc1	pobyt
venku	venku	k6eAd1	venku
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
samotu	samota	k1gFnSc4	samota
špatně	špatně	k6eAd1	špatně
snáší	snášet	k5eAaImIp3nS	snášet
a	a	k8xC	a
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
kontakt	kontakt	k1gInSc4	kontakt
s	s	k7c7	s
majitelem	majitel	k1gMnSc7	majitel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kishu-Inu	Kishu-In	k1gMnSc6	Kishu-In
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Pes	pes	k1gMnSc1	pes
plemene	plemeno	k1gNnSc2	plemeno
kishu-inu	kishunout	k5eAaPmIp1nS	kishu-inout
Aoi	Aoi	k1gFnSc1	Aoi
Rjú	Rjú	k1gFnSc1	Rjú
(	(	kIx(	(
<g/>
あ	あ	k?	あ
-	-	kIx~	-
Modrý	modrý	k2eAgInSc1d1	modrý
drak	drak	k1gInSc1	drak
<g/>
)	)	kIx)	)
hrál	hrát	k5eAaImAgInS	hrát
v	v	k7c6	v
několika	několik	k4yIc6	několik
seriálech	seriál	k1gInPc6	seriál
a	a	k8xC	a
filmech	film	k1gInPc6	film
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
nejznámější	známý	k2eAgMnSc1d3	nejznámější
jsou	být	k5eAaImIp3nP	být
seriály	seriál	k1gInPc1	seriál
Goro	Goro	k1gNnSc1	Goro
–	–	k?	–
bílý	bílý	k1gMnSc1	bílý
pes	pes	k1gMnSc1	pes
a	a	k8xC	a
Ohnivý	ohnivý	k2eAgMnSc1d1	ohnivý
pes	pes	k1gMnSc1	pes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Kishu	Kish	k1gInSc2	Kish
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
