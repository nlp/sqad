<p>
<s>
Německá	německý	k2eAgFnSc1d1	německá
demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
NDR	NDR	kA	NDR
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
východní	východní	k2eAgNnSc4d1	východní
Německo	Německo	k1gNnSc4	Německo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
německy	německy	k6eAd1	německy
mluvících	mluvící	k2eAgFnPc6d1	mluvící
zemích	zem	k1gFnPc6	zem
i	i	k8xC	i
jinde	jinde	k6eAd1	jinde
běžně	běžně	k6eAd1	běžně
DDR	DDR	kA	DDR
<g/>
,	,	kIx,	,
zkratka	zkratka	k1gFnSc1	zkratka
pro	pro	k7c4	pro
Deutsche	Deutsche	k1gNnSc4	Deutsche
Demokratische	Demokratisch	k1gFnSc2	Demokratisch
Republik	republika	k1gFnPc2	republika
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1949	[number]	k4	1949
lidově	lidově	k6eAd1	lidově
demokratickým	demokratický	k2eAgInSc7d1	demokratický
státem	stát	k1gInSc7	stát
na	na	k7c6	na
území	území	k1gNnSc6	území
sovětské	sovětský	k2eAgFnSc2d1	sovětská
okupační	okupační	k2eAgFnSc2d1	okupační
zóny	zóna	k1gFnSc2	zóna
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
východního	východní	k2eAgInSc2d1	východní
sektoru	sektor	k1gInSc2	sektor
Berlína	Berlín	k1gInSc2	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1990	[number]	k4	1990
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
Spolkové	spolkový	k2eAgFnSc2d1	spolková
republiky	republika	k1gFnSc2	republika
Německo	Německo	k1gNnSc1	Německo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
převzaly	převzít	k5eAaPmAgFnP	převzít
čtyři	čtyři	k4xCgFnPc1	čtyři
vítězné	vítězný	k2eAgFnPc1d1	vítězná
mocnosti	mocnost	k1gFnPc1	mocnost
svrchovanou	svrchovaný	k2eAgFnSc4d1	svrchovaná
moc	moc	k1gFnSc4	moc
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
činnost	činnost	k1gFnSc4	činnost
zahájila	zahájit	k5eAaPmAgFnS	zahájit
Spojenecká	spojenecký	k2eAgFnSc1d1	spojenecká
kontrolní	kontrolní	k2eAgFnSc1d1	kontrolní
rada	rada	k1gFnSc1	rada
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
měsíce	měsíc	k1gInSc2	měsíc
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
vojenská	vojenský	k2eAgFnSc1d1	vojenská
správa	správa	k1gFnSc1	správa
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
(	(	kIx(	(
<g/>
Sowjetische	Sowjetische	k1gInSc1	Sowjetische
Militäradministration	Militäradministration	k1gInSc1	Militäradministration
in	in	k?	in
Deutschland	Deutschland	k1gInSc1	Deutschland
-	-	kIx~	-
SMAD	SMAD	kA	SMAD
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
mocenský	mocenský	k2eAgInSc1d1	mocenský
orgán	orgán	k1gInSc1	orgán
v	v	k7c6	v
sovětské	sovětský	k2eAgFnSc6d1	sovětská
okupační	okupační	k2eAgFnSc6d1	okupační
zóně	zóna	k1gFnSc6	zóna
<g/>
,	,	kIx,	,
o	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
později	pozdě	k6eAd2	pozdě
byly	být	k5eAaImAgFnP	být
zřízeny	zřídit	k5eAaPmNgFnP	zřídit
správy	správa	k1gFnPc1	správa
SMAD	SMAD	kA	SMAD
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
a	a	k8xC	a
srpnu	srpen	k1gInSc6	srpen
1945	[number]	k4	1945
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
postupimská	postupimský	k2eAgFnSc1d1	Postupimská
konference	konference	k1gFnSc1	konference
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nejvyšší	vysoký	k2eAgMnPc1d3	nejvyšší
představitelé	představitel	k1gMnPc1	představitel
spojeneckých	spojenecký	k2eAgFnPc2d1	spojenecká
velmocí	velmoc	k1gFnPc2	velmoc
podepsali	podepsat	k5eAaPmAgMnP	podepsat
dohodu	dohoda	k1gFnSc4	dohoda
obsahující	obsahující	k2eAgFnSc2d1	obsahující
ustanovení	ustanovení	k1gNnSc4	ustanovení
o	o	k7c4	o
denacifikaci	denacifikace	k1gFnSc4	denacifikace
a	a	k8xC	a
demilitarizaci	demilitarizace	k1gFnSc4	demilitarizace
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
o	o	k7c6	o
rozdělení	rozdělení	k1gNnSc6	rozdělení
jeho	on	k3xPp3gNnSc2	on
území	území	k1gNnSc2	území
do	do	k7c2	do
čtyř	čtyři	k4xCgFnPc2	čtyři
okupačních	okupační	k2eAgFnPc2d1	okupační
zón	zóna	k1gFnPc2	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
června	červen	k1gInSc2	červen
a	a	k8xC	a
července	červenec	k1gInSc2	červenec
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
v	v	k7c6	v
sovětské	sovětský	k2eAgFnSc6d1	sovětská
okupační	okupační	k2eAgFnSc6d1	okupační
zóně	zóna	k1gFnSc6	zóna
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
Sociálně	sociálně	k6eAd1	sociálně
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
Křesťansko-demokratická	křesťanskoemokratický	k2eAgFnSc1d1	křesťansko-demokratická
unie	unie	k1gFnSc1	unie
a	a	k8xC	a
Liberálně	liberálně	k6eAd1	liberálně
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
na	na	k7c6	na
základě	základ	k1gInSc6	základ
rozkazu	rozkaz	k1gInSc2	rozkaz
SMAD	SMAD	kA	SMAD
byly	být	k5eAaImAgFnP	být
vytvořeny	vytvořit	k5eAaPmNgFnP	vytvořit
německé	německý	k2eAgFnPc1d1	německá
ústřední	ústřední	k2eAgFnPc1d1	ústřední
správy	správa	k1gFnPc1	správa
<g/>
.	.	kIx.	.
</s>
<s>
Sloužily	sloužit	k5eAaImAgInP	sloužit
jako	jako	k9	jako
pomocné	pomocný	k2eAgInPc1d1	pomocný
orgány	orgán	k1gInPc1	orgán
SMAD	SMAD	kA	SMAD
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
resortech	resort	k1gInPc6	resort
správy	správa	k1gFnSc2	správa
a	a	k8xC	a
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
byly	být	k5eAaImAgFnP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
zárodek	zárodek	k1gInSc4	zárodek
budoucí	budoucí	k2eAgFnSc2d1	budoucí
německé	německý	k2eAgFnSc2d1	německá
ústřední	ústřední	k2eAgFnSc2d1	ústřední
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc4d3	veliký
vliv	vliv	k1gInSc4	vliv
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
získali	získat	k5eAaPmAgMnP	získat
komunisté	komunista	k1gMnPc1	komunista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
sovětské	sovětský	k2eAgFnSc6d1	sovětská
okupační	okupační	k2eAgFnSc6d1	okupační
zóně	zóna	k1gFnSc6	zóna
provedena	proveden	k2eAgFnSc1d1	provedena
pozemková	pozemkový	k2eAgFnSc1d1	pozemková
reforma	reforma	k1gFnSc1	reforma
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
znamenala	znamenat	k5eAaImAgFnS	znamenat
převod	převod	k1gInSc4	převod
2,5	[number]	k4	2,5
milionu	milion	k4xCgInSc2	milion
hektarů	hektar	k1gInPc2	hektar
půdy	půda	k1gFnSc2	půda
z	z	k7c2	z
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
7	[number]	k4	7
tisíc	tisíc	k4xCgInPc2	tisíc
velkostatkářů	velkostatkář	k1gMnPc2	velkostatkář
do	do	k7c2	do
půdního	půdní	k2eAgInSc2d1	půdní
fondu	fond	k1gInSc2	fond
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
něho	on	k3xPp3gMnSc2	on
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
přidělovalo	přidělovat	k5eAaImAgNnS	přidělovat
bezzemkům	bezzemek	k1gMnPc3	bezzemek
a	a	k8xC	a
malým	malý	k2eAgMnPc3d1	malý
rolníkům	rolník	k1gMnPc3	rolník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1946	[number]	k4	1946
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
sloučení	sloučení	k1gNnSc3	sloučení
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
KPD	KPD	kA	KPD
<g/>
)	)	kIx)	)
a	a	k8xC	a
Sociálně	sociálně	k6eAd1	sociálně
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
SPD	SPD	kA	SPD
<g/>
)	)	kIx)	)
v	v	k7c6	v
sovětské	sovětský	k2eAgFnSc6d1	sovětská
okupační	okupační	k2eAgFnSc6d1	okupační
zóně	zóna	k1gFnSc6	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc7	jejich
spojením	spojení	k1gNnSc7	spojení
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Sjednocená	sjednocený	k2eAgFnSc1d1	sjednocená
socialistická	socialistický	k2eAgFnSc1d1	socialistická
strana	strana	k1gFnSc1	strana
Německa	Německo	k1gNnSc2	Německo
(	(	kIx(	(
<g/>
SED	sed	k1gInSc1	sed
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
obecní	obecní	k2eAgFnPc1d1	obecní
volby	volba	k1gFnPc1	volba
v	v	k7c6	v
sovětské	sovětský	k2eAgFnSc6d1	sovětská
okupační	okupační	k2eAgFnSc6d1	okupační
zóně	zóna	k1gFnSc6	zóna
<g/>
,	,	kIx,	,
nejvíce	hodně	k6eAd3	hodně
hlasů	hlas	k1gInPc2	hlas
všude	všude	k6eAd1	všude
získala	získat	k5eAaPmAgFnS	získat
SED	sed	k1gInSc4	sed
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1946	[number]	k4	1946
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
volby	volba	k1gFnPc1	volba
do	do	k7c2	do
krajských	krajský	k2eAgInPc2d1	krajský
a	a	k8xC	a
zemských	zemský	k2eAgInPc2d1	zemský
sněmů	sněm	k1gInPc2	sněm
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
si	se	k3xPyFc3	se
SED	sed	k1gInSc1	sed
udržela	udržet	k5eAaPmAgFnS	udržet
své	své	k1gNnSc4	své
postavení	postavení	k1gNnSc4	postavení
nejsilnější	silný	k2eAgFnSc2d3	nejsilnější
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
obecním	obecní	k2eAgFnPc3d1	obecní
volbám	volba	k1gFnPc3	volba
hlasy	hlas	k1gInPc7	hlas
spíše	spíše	k9	spíše
ztrácela	ztrácet	k5eAaImAgFnS	ztrácet
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
křesťanští	křesťanský	k2eAgMnPc1d1	křesťanský
a	a	k8xC	a
liberální	liberální	k2eAgMnPc1d1	liberální
demokraté	demokrat	k1gMnPc1	demokrat
naopak	naopak	k6eAd1	naopak
zaznamenali	zaznamenat	k5eAaPmAgMnP	zaznamenat
přírůstek	přírůstek	k1gInSc4	přírůstek
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
volby	volba	k1gFnPc1	volba
také	také	k9	také
ve	v	k7c6	v
Velkém	velký	k2eAgInSc6d1	velký
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Jednotná	jednotný	k2eAgFnSc1d1	jednotná
socialistická	socialistický	k2eAgFnSc1d1	socialistická
strana	strana	k1gFnSc1	strana
(	(	kIx(	(
<g/>
SED	sed	k1gInSc1	sed
<g/>
)	)	kIx)	)
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
výrazného	výrazný	k2eAgNnSc2d1	výrazné
vítězství	vítězství	k1gNnSc2	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
zvolené	zvolený	k2eAgInPc1d1	zvolený
zemské	zemský	k2eAgInPc1d1	zemský
sněmy	sněm	k1gInPc1	sněm
ustavily	ustavit	k5eAaPmAgInP	ustavit
zemské	zemský	k2eAgFnPc4d1	zemská
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jejich	jejich	k3xOp3gMnPc4	jejich
ministry	ministr	k1gMnPc4	ministr
měla	mít	k5eAaImAgFnS	mít
naprosto	naprosto	k6eAd1	naprosto
nejsilnější	silný	k2eAgNnSc4d3	nejsilnější
zastoupení	zastoupení	k1gNnSc4	zastoupení
SED	sed	k1gInSc4	sed
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
konference	konference	k1gFnSc1	konference
ministerských	ministerský	k2eAgMnPc2d1	ministerský
předsedů	předseda	k1gMnPc2	předseda
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
zemí	zem	k1gFnPc2	zem
z	z	k7c2	z
celého	celý	k2eAgNnSc2d1	celé
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
určitém	určitý	k2eAgNnSc6d1	určité
váhání	váhání	k1gNnSc6	váhání
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
i	i	k9	i
premiéři	premiér	k1gMnPc1	premiér
zemí	zem	k1gFnPc2	zem
sovětského	sovětský	k2eAgMnSc2d1	sovětský
okupační	okupační	k2eAgMnPc1d1	okupační
zóny	zóna	k1gFnSc2	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Jednání	jednání	k1gNnSc1	jednání
však	však	k9	však
nevedla	vést	k5eNaImAgFnS	vést
k	k	k7c3	k
žádným	žádný	k3yNgInPc3	žádný
konkrétnějším	konkrétní	k2eAgInPc3d2	konkrétnější
výsledkům	výsledek	k1gInPc3	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1947	[number]	k4	1947
se	se	k3xPyFc4	se
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
konal	konat	k5eAaImAgInS	konat
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Sjezd	sjezd	k1gInSc1	sjezd
SED	sed	k1gInSc1	sed
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
jako	jako	k8xS	jako
hlavní	hlavní	k2eAgInSc4d1	hlavní
úkol	úkol	k1gInSc4	úkol
strany	strana	k1gFnSc2	strana
označil	označit	k5eAaPmAgMnS	označit
boj	boj	k1gInSc4	boj
za	za	k7c4	za
jednotu	jednota	k1gFnSc4	jednota
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1948	[number]	k4	1948
sovětský	sovětský	k2eAgMnSc1d1	sovětský
zástupce	zástupce	k1gMnSc1	zástupce
opustil	opustit	k5eAaPmAgMnS	opustit
Spojeneckou	spojenecký	k2eAgFnSc4d1	spojenecká
kontrolní	kontrolní	k2eAgFnSc4d1	kontrolní
radu	rada	k1gFnSc4	rada
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
přenesla	přenést	k5eAaPmAgFnS	přenést
definitivně	definitivně	k6eAd1	definitivně
i	i	k9	i
na	na	k7c4	na
německé	německý	k2eAgNnSc4d1	německé
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
okupačních	okupační	k2eAgFnPc6d1	okupační
zónách	zóna	k1gFnPc6	zóna
uskutečněna	uskutečnit	k5eAaPmNgFnS	uskutečnit
měnová	měnový	k2eAgFnSc1d1	měnová
reforma	reforma	k1gFnSc1	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
SSSR	SSSR	kA	SSSR
reagoval	reagovat	k5eAaBmAgMnS	reagovat
měnovou	měnový	k2eAgFnSc7d1	měnová
reformou	reforma	k1gFnSc7	reforma
také	také	k9	také
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
zóně	zóna	k1gFnSc6	zóna
a	a	k8xC	a
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
23	[number]	k4	23
<g/>
.	.	kIx.	.
na	na	k7c4	na
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
zahájil	zahájit	k5eAaPmAgMnS	zahájit
blokádu	blokáda	k1gFnSc4	blokáda
Západního	západní	k2eAgInSc2d1	západní
Berlína	Berlín	k1gInSc2	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
pak	pak	k6eAd1	pak
začal	začít	k5eAaPmAgInS	začít
fungovat	fungovat	k5eAaImF	fungovat
letecký	letecký	k2eAgInSc1d1	letecký
most	most	k1gInSc1	most
zásobující	zásobující	k2eAgFnSc4d1	zásobující
izolovanou	izolovaný	k2eAgFnSc4d1	izolovaná
část	část	k1gFnSc4	část
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
bylo	být	k5eAaImAgNnS	být
sídlo	sídlo	k1gNnSc1	sídlo
berlínské	berlínský	k2eAgFnSc2d1	Berlínská
městské	městský	k2eAgFnSc2d1	městská
rady	rada	k1gFnSc2	rada
po	po	k7c6	po
komunistických	komunistický	k2eAgFnPc6d1	komunistická
demonstracích	demonstrace	k1gFnPc6	demonstrace
přeneseno	přenést	k5eAaPmNgNnS	přenést
do	do	k7c2	do
Západního	západní	k2eAgInSc2d1	západní
Berlína	Berlín	k1gInSc2	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
později	pozdě	k6eAd2	pozdě
shromáždění	shromáždění	k1gNnSc1	shromáždění
berlínské	berlínský	k2eAgNnSc1d1	berlínské
SED	sed	k1gInSc4	sed
prohlásilo	prohlásit	k5eAaPmAgNnS	prohlásit
berlínský	berlínský	k2eAgInSc1d1	berlínský
magistrát	magistrát	k1gInSc1	magistrát
za	za	k7c4	za
sesazený	sesazený	k2eAgInSc4d1	sesazený
a	a	k8xC	a
dosadilo	dosadit	k5eAaPmAgNnS	dosadit
magistrát	magistrát	k1gInSc4	magistrát
nový	nový	k2eAgInSc4d1	nový
<g/>
,	,	kIx,	,
uznaný	uznaný	k2eAgInSc4d1	uznaný
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
SMAD	SMAD	kA	SMAD
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
svobodně	svobodně	k6eAd1	svobodně
zvolený	zvolený	k2eAgInSc1d1	zvolený
magistrát	magistrát	k1gInSc1	magistrát
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
přesídlit	přesídlit	k5eAaPmF	přesídlit
do	do	k7c2	do
Západního	západní	k2eAgInSc2d1	západní
Berlína	Berlín	k1gInSc2	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgFnPc1d1	Nové
volby	volba	k1gFnPc1	volba
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
mohly	moct	k5eAaImAgFnP	moct
konat	konat	k5eAaImF	konat
pouze	pouze	k6eAd1	pouze
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
SMED	SMED	kA	SMED
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
sektoru	sektor	k1gInSc6	sektor
zakázal	zakázat	k5eAaPmAgMnS	zakázat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1949	[number]	k4	1949
byla	být	k5eAaImAgFnS	být
schválena	schválit	k5eAaPmNgFnS	schválit
ústava	ústava	k1gFnSc1	ústava
sovětské	sovětský	k2eAgFnSc2d1	sovětská
okupační	okupační	k2eAgFnSc2d1	okupační
zóny	zóna	k1gFnSc2	zóna
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Německé	německý	k2eAgFnSc2d1	německá
demokratické	demokratický	k2eAgFnSc2d1	demokratická
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
Německá	německý	k2eAgFnSc1d1	německá
spolková	spolkový	k2eAgFnSc1d1	spolková
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
reakce	reakce	k1gFnPc1	reakce
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
čin	čin	k1gInSc4	čin
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
se	s	k7c7	s
svolením	svolení	k1gNnSc7	svolení
sovětského	sovětský	k2eAgNnSc2d1	sovětské
vedení	vedení	k1gNnSc2	vedení
Německá	německý	k2eAgFnSc1d1	německá
demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
první	první	k4xOgFnSc6	první
prezidentem	prezident	k1gMnSc7	prezident
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Pieck	Pieck	k1gMnSc1	Pieck
<g/>
,	,	kIx,	,
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
Otto	Otto	k1gMnSc1	Otto
Grotewohl	Grotewohl	k1gMnSc1	Grotewohl
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
Národní	národní	k2eAgFnSc1d1	národní
fronta	fronta	k1gFnSc1	fronta
demokratického	demokratický	k2eAgNnSc2d1	demokratické
Německa	Německo	k1gNnSc2	Německo
sdružující	sdružující	k2eAgNnSc1d1	sdružující
všechny	všechen	k3xTgFnPc4	všechen
strany	strana	k1gFnPc4	strana
a	a	k8xC	a
masové	masový	k2eAgFnPc4d1	masová
organizace	organizace	k1gFnPc4	organizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1950	[number]	k4	1950
se	se	k3xPyFc4	se
v	v	k7c6	v
NDR	NDR	kA	NDR
konaly	konat	k5eAaImAgFnP	konat
volby	volba	k1gFnPc4	volba
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
nesvobodné	svobodný	k2eNgFnPc1d1	nesvobodná
<g/>
,	,	kIx,	,
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k9	jako
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
socialistických	socialistický	k2eAgInPc6d1	socialistický
státech	stát	k1gInPc6	stát
se	se	k3xPyFc4	se
volila	volit	k5eAaImAgFnS	volit
jednotná	jednotný	k2eAgFnSc1d1	jednotná
kandidátka	kandidátka	k1gFnSc1	kandidátka
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
niž	jenž	k3xRgFnSc4	jenž
podle	podle	k7c2	podle
oficiálních	oficiální	k2eAgInPc2d1	oficiální
výsledků	výsledek	k1gInPc2	výsledek
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
12	[number]	k4	12
088	[number]	k4	088
745	[number]	k4	745
oprávněných	oprávněný	k2eAgMnPc2d1	oprávněný
voličů	volič	k1gMnPc2	volič
(	(	kIx(	(
<g/>
99,6	[number]	k4	99,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hlasů	hlas	k1gInPc2	hlas
proti	proti	k7c3	proti
a	a	k8xC	a
neplatných	platný	k2eNgInPc2d1	neplatný
hlasů	hlas	k1gInPc2	hlas
bylo	být	k5eAaImAgNnS	být
51	[number]	k4	51
187	[number]	k4	187
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
představovalo	představovat	k5eAaImAgNnS	představovat
0,4	[number]	k4	0,4
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
voleb	volba	k1gFnPc2	volba
vzešla	vzejít	k5eAaPmAgFnS	vzejít
nová	nový	k2eAgFnSc1d1	nová
vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
výrazně	výrazně	k6eAd1	výrazně
obměněny	obměněn	k2eAgInPc1d1	obměněn
byly	být	k5eAaImAgInP	být
také	také	k9	také
vlády	vláda	k1gFnSc2	vláda
zemské	zemský	k2eAgFnSc2d1	zemská
<g/>
.	.	kIx.	.
</s>
<s>
Voliči	volič	k1gMnPc1	volič
mohli	moct	k5eAaImAgMnP	moct
hlasovat	hlasovat	k5eAaImF	hlasovat
buď	buď	k8xC	buď
pro	pro	k7c4	pro
jednotnou	jednotný	k2eAgFnSc4d1	jednotná
kandidátku	kandidátka	k1gFnSc4	kandidátka
nebo	nebo	k8xC	nebo
hlasovací	hlasovací	k2eAgInSc4d1	hlasovací
lístek	lístek	k1gInSc4	lístek
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
ale	ale	k8xC	ale
bez	bez	k7c2	bez
jakéhokoliv	jakýkoliv	k3yIgNnSc2	jakýkoliv
utajení	utajení	k1gNnSc2	utajení
<g/>
,	,	kIx,	,
přeškrtnout	přeškrtnout	k5eAaPmF	přeškrtnout
<g/>
,	,	kIx,	,
čehož	což	k3yQnSc2	což
důsledkem	důsledek	k1gInSc7	důsledek
bylo	být	k5eAaImAgNnS	být
vystavení	vystavení	k1gNnSc1	vystavení
perzekuce	perzekuce	k1gFnSc2	perzekuce
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
režimu	režim	k1gInSc2	režim
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
ztráty	ztráta	k1gFnSc2	ztráta
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
<g/>
,	,	kIx,	,
vyhození	vyhození	k1gNnSc2	vyhození
ze	z	k7c2	z
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
sledování	sledování	k1gNnSc4	sledování
státní	státní	k2eAgFnSc7d1	státní
bezpečností	bezpečnost	k1gFnSc7	bezpečnost
(	(	kIx(	(
<g/>
Stasi	stase	k1gFnSc6	stase
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
J.	J.	kA	J.
V.	V.	kA	V.
Stalina	Stalin	k1gMnSc2	Stalin
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
a	a	k8xC	a
nástupu	nástup	k1gInSc2	nástup
nové	nový	k2eAgFnSc2d1	nová
garnitury	garnitura	k1gFnSc2	garnitura
ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
SSSR	SSSR	kA	SSSR
bylo	být	k5eAaImAgNnS	být
očekáváno	očekáván	k2eAgNnSc1d1	očekáváno
celkové	celkový	k2eAgNnSc1d1	celkové
uvolnění	uvolnění	k1gNnSc1	uvolnění
<g/>
.	.	kIx.	.
</s>
<s>
Tužby	tužba	k1gFnPc1	tužba
lidu	lid	k1gInSc2	lid
se	se	k3xPyFc4	se
však	však	k9	však
nenaplňovaly	naplňovat	k5eNaImAgFnP	naplňovat
a	a	k8xC	a
pod	pod	k7c7	pod
počáteční	počáteční	k2eAgFnSc7d1	počáteční
záminkou	záminka	k1gFnSc7	záminka
protestu	protest	k1gInSc2	protest
berlínských	berlínský	k2eAgMnPc2d1	berlínský
dělníků	dělník	k1gMnPc2	dělník
na	na	k7c4	na
zvyšování	zvyšování	k1gNnSc4	zvyšování
výroby	výroba	k1gFnSc2	výroba
se	s	k7c7	s
16	[number]	k4	16
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1953	[number]	k4	1953
zvedla	zvednout	k5eAaPmAgFnS	zvednout
vlna	vlna	k1gFnSc1	vlna
celostátních	celostátní	k2eAgFnPc2d1	celostátní
demonstrací	demonstrace	k1gFnPc2	demonstrace
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
to	ten	k3xDgNnSc4	ten
tak	tak	k9	tak
silné	silný	k2eAgInPc1d1	silný
protesty	protest	k1gInPc1	protest
<g/>
,	,	kIx,	,
že	že	k8xS	že
okupační	okupační	k2eAgFnSc1d1	okupační
správa	správa	k1gFnSc1	správa
musela	muset	k5eAaImAgFnS	muset
na	na	k7c4	na
demonstranty	demonstrant	k1gMnPc4	demonstrant
poslat	poslat	k5eAaPmF	poslat
okupační	okupační	k2eAgFnSc2d1	okupační
síly	síla	k1gFnSc2	síla
dislokované	dislokovaný	k2eAgFnSc2d1	dislokovaná
na	na	k7c6	na
území	území	k1gNnSc6	území
NDR	NDR	kA	NDR
<g/>
.	.	kIx.	.
<g/>
Rok	rok	k1gInSc1	rok
1956	[number]	k4	1956
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
NDR	NDR	kA	NDR
zlomový	zlomový	k2eAgMnSc1d1	zlomový
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
byla	být	k5eAaImAgFnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
založena	založit	k5eAaPmNgFnS	založit
Národní	národní	k2eAgFnSc1d1	národní
lidová	lidový	k2eAgFnSc1d1	lidová
armáda	armáda	k1gFnSc1	armáda
NDR	NDR	kA	NDR
(	(	kIx(	(
<g/>
NVA	NVA	kA	NVA
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
reakce	reakce	k1gFnSc1	reakce
na	na	k7c4	na
založení	založení	k1gNnSc4	založení
a	a	k8xC	a
vstup	vstup	k1gInSc4	vstup
Bundeswehru	bundeswehr	k1gInSc2	bundeswehr
do	do	k7c2	do
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgNnP	být
NDR	NDR	kA	NDR
postavena	postaven	k2eAgFnSc1d1	postavena
před	před	k7c4	před
problém	problém	k1gInSc4	problém
s	s	k7c7	s
emigrací	emigrace	k1gFnSc7	emigrace
přes	přes	k7c4	přes
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Berlín	Berlín	k1gInSc1	Berlín
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
bylo	být	k5eAaImAgNnS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
-	-	kIx~	-
východní	východní	k2eAgInPc4d1	východní
a	a	k8xC	a
západní	západní	k2eAgInPc4d1	západní
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
státu	stát	k1gInSc2	stát
se	se	k3xPyFc4	se
na	na	k7c4	na
nátlak	nátlak	k1gInSc4	nátlak
z	z	k7c2	z
ostatních	ostatní	k2eAgInPc2d1	ostatní
socialistických	socialistický	k2eAgInPc2d1	socialistický
států	stát	k1gInPc2	stát
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
uzavřít	uzavřít	k5eAaPmF	uzavřít
hranice	hranice	k1gFnPc4	hranice
Berlína	Berlín	k1gInSc2	Berlín
neproniknutelnou	proniknutelný	k2eNgFnSc7d1	neproniknutelná
zdí	zeď	k1gFnSc7	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Američané	Američan	k1gMnPc1	Američan
s	s	k7c7	s
postavením	postavení	k1gNnSc7	postavení
Berlínské	berlínský	k2eAgFnSc2d1	Berlínská
zdi	zeď	k1gFnSc2	zeď
počítali	počítat	k5eAaImAgMnP	počítat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepředpokládali	předpokládat	k5eNaImAgMnP	předpokládat
tak	tak	k6eAd1	tak
rychlou	rychlý	k2eAgFnSc4d1	rychlá
akci	akce	k1gFnSc4	akce
vedení	vedení	k1gNnSc2	vedení
NDR	NDR	kA	NDR
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
ráno	ráno	k6eAd1	ráno
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1961	[number]	k4	1961
obsadila	obsadit	k5eAaPmAgFnS	obsadit
Volkspolizei	Volkspolizei	k1gNnSc4	Volkspolizei
a	a	k8xC	a
NVA	NVA	kA	NVA
hranice	hranice	k1gFnPc4	hranice
mezi	mezi	k7c7	mezi
Západním	západní	k2eAgInSc7d1	západní
a	a	k8xC	a
východním	východní	k2eAgInSc7d1	východní
Berlínem	Berlín	k1gInSc7	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
k	k	k7c3	k
takovým	takový	k3xDgFnPc3	takový
opatřením	opatření	k1gNnSc7	opatření
bylo	být	k5eAaImAgNnS	být
zahájení	zahájení	k1gNnSc1	zahájení
stavby	stavba	k1gFnSc2	stavba
Berlínské	berlínský	k2eAgFnSc2d1	Berlínská
zdi	zeď	k1gFnSc2	zeď
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
měla	mít	k5eAaImAgFnS	mít
zastavit	zastavit	k5eAaPmF	zastavit
migraci	migrace	k1gFnSc3	migrace
obyvatel	obyvatel	k1gMnSc1	obyvatel
NDR	NDR	kA	NDR
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hospodářské	hospodářský	k2eAgFnPc1d1	hospodářská
těžkosti	těžkost	k1gFnPc1	těžkost
socialistického	socialistický	k2eAgInSc2d1	socialistický
tábora	tábor	k1gInSc2	tábor
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1960	[number]	k4	1960
až	až	k6eAd1	až
1963	[number]	k4	1963
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
zavedení	zavedení	k1gNnSc3	zavedení
revoluční	revoluční	k2eAgFnSc2d1	revoluční
Nové	Nová	k1gFnSc2	Nová
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přinesla	přinést	k5eAaPmAgFnS	přinést
výrazné	výrazný	k2eAgNnSc4d1	výrazné
pozvednutí	pozvednutí	k1gNnSc4	pozvednutí
životní	životní	k2eAgFnSc2d1	životní
úrovně	úroveň	k1gFnSc2	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
demokratizací	demokratizace	k1gFnSc7	demokratizace
systému	systém	k1gInSc2	systém
v	v	k7c6	v
ČSSR	ČSSR	kA	ČSSR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
nastala	nastat	k5eAaPmAgFnS	nastat
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
komunistických	komunistický	k2eAgFnPc2d1	komunistická
stran	strana	k1gFnPc2	strana
obava	obava	k1gFnSc1	obava
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
snahy	snaha	k1gFnPc1	snaha
nerozšířily	rozšířit	k5eNaPmAgFnP	rozšířit
i	i	k9	i
do	do	k7c2	do
jejich	jejich	k3xOp3gFnPc2	jejich
republik	republika	k1gFnPc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1968	[number]	k4	1968
poslalo	poslat	k5eAaPmAgNnS	poslat
vedení	vedení	k1gNnSc1	vedení
SED	sed	k1gInSc1	sed
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
čtyř	čtyři	k4xCgFnPc2	čtyři
komunistických	komunistický	k2eAgFnPc2d1	komunistická
stran	strana	k1gFnPc2	strana
varovný	varovný	k2eAgInSc4d1	varovný
tzv.	tzv.	kA	tzv.
Varšavský	varšavský	k2eAgInSc4d1	varšavský
dopis	dopis	k1gInSc4	dopis
československému	československý	k2eAgNnSc3d1	Československé
vedení	vedení	k1gNnSc3	vedení
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
varování	varování	k1gNnSc4	varování
ale	ale	k8xC	ale
změny	změna	k1gFnPc1	změna
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c6	o
vojenské	vojenský	k2eAgFnSc6d1	vojenská
intervenci	intervence	k1gFnSc6	intervence
<g/>
.	.	kIx.	.
</s>
<s>
Vpádu	vpád	k1gInSc3	vpád
do	do	k7c2	do
ČSSR	ČSSR	kA	ČSSR
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
pět	pět	k4xCc1	pět
zemí	zem	k1gFnPc2	zem
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
také	také	k9	také
NDR	NDR	kA	NDR
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
měsících	měsíc	k1gInPc6	měsíc
pobytu	pobyt	k1gInSc2	pobyt
se	se	k3xPyFc4	se
vojska	vojsko	k1gNnPc1	vojsko
čtyř	čtyři	k4xCgInPc2	čtyři
zemí	zem	k1gFnPc2	zem
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
vojska	vojsko	k1gNnSc2	vojsko
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
byl	být	k5eAaImAgInS	být
odvolán	odvolat	k5eAaPmNgMnS	odvolat
dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
generální	generální	k2eAgMnSc1d1	generální
tajemník	tajemník	k1gMnSc1	tajemník
SED	sed	k1gInSc4	sed
Walter	Walter	k1gMnSc1	Walter
Ulbricht	Ulbricht	k1gMnSc1	Ulbricht
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
vysoký	vysoký	k2eAgInSc4d1	vysoký
věk	věk	k1gInSc4	věk
a	a	k8xC	a
obstarožní	obstarožní	k2eAgInPc4d1	obstarožní
názory	názor	k1gInPc4	názor
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
Erich	Erich	k1gMnSc1	Erich
Honecker	Honecker	k1gMnSc1	Honecker
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
vyznačovala	vyznačovat	k5eAaImAgFnS	vyznačovat
jak	jak	k6eAd1	jak
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
politickou	politický	k2eAgFnSc7d1	politická
stagnací	stagnace	k1gFnSc7	stagnace
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
stagnace	stagnace	k1gFnSc1	stagnace
uvrhla	uvrhnout	k5eAaPmAgFnS	uvrhnout
NDR	NDR	kA	NDR
do	do	k7c2	do
hluboké	hluboký	k2eAgFnSc2d1	hluboká
krize	krize	k1gFnSc2	krize
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Krizi	krize	k1gFnSc4	krize
v	v	k7c6	v
NDR	NDR	kA	NDR
se	se	k3xPyFc4	se
vedení	vedení	k1gNnSc1	vedení
SED	sed	k1gInSc4	sed
neodvážilo	odvážit	k5eNaPmAgNnS	odvážit
řešit	řešit	k5eAaImF	řešit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ještě	ještě	k6eAd1	ještě
odmítalo	odmítat	k5eAaImAgNnS	odmítat
perestrojku	perestrojka	k1gFnSc4	perestrojka
a	a	k8xC	a
glastnost	glastnost	k1gFnSc4	glastnost
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
jednání	jednání	k1gNnSc1	jednání
vyústilo	vyústit	k5eAaPmAgNnS	vyústit
v	v	k7c4	v
otevřené	otevřený	k2eAgFnPc4d1	otevřená
masové	masový	k2eAgFnPc4d1	masová
demonstrace	demonstrace	k1gFnPc4	demonstrace
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
a	a	k8xC	a
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
násilně	násilně	k6eAd1	násilně
potlačeny	potlačen	k2eAgFnPc1d1	potlačena
<g/>
.	.	kIx.	.
</s>
<s>
Takovéto	takovýto	k3xDgNnSc1	takovýto
jednání	jednání	k1gNnSc1	jednání
pobouřilo	pobouřit	k5eAaPmAgNnS	pobouřit
občany	občan	k1gMnPc4	občan
NDR	NDR	kA	NDR
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
celostátních	celostátní	k2eAgFnPc2d1	celostátní
demonstrací	demonstrace	k1gFnPc2	demonstrace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NDR	NDR	kA	NDR
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
diskusi	diskuse	k1gFnSc6	diskuse
o	o	k7c6	o
dopingových	dopingový	k2eAgInPc6d1	dopingový
skandálech	skandál	k1gInPc6	skandál
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Německá	německý	k2eAgFnSc1d1	německá
dopingová	dopingový	k2eAgFnSc1d1	dopingová
republika	republika	k1gFnSc1	republika
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Zánik	zánik	k1gInSc1	zánik
NDR	NDR	kA	NDR
==	==	k?	==
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1990	[number]	k4	1990
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
sjednocení	sjednocení	k1gNnSc3	sjednocení
se	s	k7c7	s
Spolkovou	spolkový	k2eAgFnSc7d1	spolková
republikou	republika	k1gFnSc7	republika
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
byly	být	k5eAaImAgFnP	být
země	zem	k1gFnPc1	zem
obnoveny	obnoven	k2eAgFnPc1d1	obnovena
a	a	k8xC	a
Východní	východní	k2eAgInSc1d1	východní
Berlín	Berlín	k1gInSc1	Berlín
byl	být	k5eAaImAgInS	být
sjednocen	sjednotit	k5eAaPmNgInS	sjednotit
se	s	k7c7	s
Západním	západní	k2eAgInSc7d1	západní
Berlínem	Berlín	k1gInSc7	Berlín
do	do	k7c2	do
jednotné	jednotný	k2eAgFnSc2d1	jednotná
spolkové	spolkový	k2eAgFnSc2d1	spolková
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Nedošlo	dojít	k5eNaPmAgNnS	dojít
však	však	k9	však
k	k	k7c3	k
obnově	obnova	k1gFnSc3	obnova
historických	historický	k2eAgFnPc2d1	historická
hranic	hranice	k1gFnPc2	hranice
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
mapa	mapa	k1gFnSc1	mapa
spolkových	spolkový	k2eAgFnPc2d1	spolková
zemí	zem	k1gFnPc2	zem
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1990	[number]	k4	1990
<g/>
-	-	kIx~	-
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klasifikace	klasifikace	k1gFnSc2	klasifikace
NDR	NDR	kA	NDR
==	==	k?	==
</s>
</p>
<p>
<s>
Dobová	dobový	k2eAgFnSc1d1	dobová
literatura	literatura	k1gFnSc1	literatura
v	v	k7c6	v
tehdejším	tehdejší	k2eAgNnSc6d1	tehdejší
Československu	Československo	k1gNnSc6	Československo
psala	psát	k5eAaImAgFnS	psát
o	o	k7c4	o
NDR	NDR	kA	NDR
například	například	k6eAd1	například
jako	jako	k8xC	jako
o	o	k7c6	o
"	"	kIx"	"
<g/>
demokratickém	demokratický	k2eAgInSc6d1	demokratický
státu	stát	k1gInSc6	stát
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
reprezentantem	reprezentant	k1gMnSc7	reprezentant
německého	německý	k2eAgInSc2d1	německý
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
budujícího	budující	k2eAgMnSc2d1	budující
demokratický	demokratický	k2eAgInSc4d1	demokratický
<g/>
,	,	kIx,	,
mírumilovný	mírumilovný	k2eAgInSc4d1	mírumilovný
a	a	k8xC	a
socialistický	socialistický	k2eAgInSc4d1	socialistický
stát	stát	k1gInSc4	stát
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Administrativní	administrativní	k2eAgNnSc4d1	administrativní
členění	členění	k1gNnSc4	členění
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1949	[number]	k4	1949
do	do	k7c2	do
22	[number]	k4	22
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1952	[number]	k4	1952
formálně	formálně	k6eAd1	formálně
federace	federace	k1gFnSc1	federace
složená	složený	k2eAgFnSc1d1	složená
ze	z	k7c2	z
6	[number]	k4	6
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
Länder	Länder	k1gInSc1	Länder
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Berlín	Berlín	k1gInSc1	Berlín
<g/>
,	,	kIx,	,
Braniborsko	Braniborsko	k1gNnSc1	Braniborsko
<g/>
,	,	kIx,	,
Durynsko	Durynsko	k1gNnSc1	Durynsko
<g/>
,	,	kIx,	,
Meklenbursko	Meklenbursko	k1gNnSc1	Meklenbursko
<g/>
,	,	kIx,	,
Sasko	Sasko	k1gNnSc1	Sasko
<g/>
,	,	kIx,	,
Sasko-Anhaltsko	Sasko-Anhaltsko	k1gNnSc1	Sasko-Anhaltsko
</s>
</p>
<p>
<s>
Postupně	postupně	k6eAd1	postupně
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
čím	co	k3yRnSc7	co
dál	daleko	k6eAd2	daleko
silnější	silný	k2eAgFnSc4d2	silnější
centralizaci	centralizace	k1gFnSc4	centralizace
a	a	k8xC	a
zákonem	zákon	k1gInSc7	zákon
"	"	kIx"	"
<g/>
Gesetz	Gesetz	k1gInSc1	Gesetz
über	über	k1gInSc1	über
die	die	k?	die
weitere	weiter	k1gInSc5	weiter
Demokratisierung	Demokratisierung	k1gMnSc1	Demokratisierung
des	des	k1gNnSc2	des
Aufbaus	Aufbaus	k1gMnSc1	Aufbaus
und	und	k?	und
der	drát	k5eAaImRp2nS	drát
Arbeitsweise	Arbeitsweis	k1gInSc5	Arbeitsweis
der	drát	k5eAaImRp2nS	drát
staatlichen	staatlichna	k1gFnPc2	staatlichna
Organe	Organ	k1gMnSc5	Organ
in	in	k?	in
den	den	k1gInSc4	den
Ländern	Länderna	k1gFnPc2	Länderna
der	drát	k5eAaImRp2nS	drát
Deutschen	Deutschno	k1gNnPc2	Deutschno
Demokratischen	Demokratischna	k1gFnPc2	Demokratischna
Republik	republika	k1gFnPc2	republika
<g/>
"	"	kIx"	"
z	z	k7c2	z
23	[number]	k4	23
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1952	[number]	k4	1952
(	(	kIx(	(
<g/>
který	který	k3yQgInSc1	který
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
okamžitě	okamžitě	k6eAd1	okamžitě
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
<g/>
)	)	kIx)	)
pak	pak	k6eAd1	pak
dochází	docházet	k5eAaImIp3nS	docházet
na	na	k7c6	na
území	území	k1gNnSc6	území
NDR	NDR	kA	NDR
k	k	k7c3	k
definitivnímu	definitivní	k2eAgNnSc3d1	definitivní
odstranění	odstranění	k1gNnSc3	odstranění
posledních	poslední	k2eAgInPc2d1	poslední
zbytků	zbytek	k1gInPc2	zbytek
federalismu	federalismus	k1gInSc2	federalismus
a	a	k8xC	a
přeměně	přeměna	k1gFnSc3	přeměna
NDR	NDR	kA	NDR
v	v	k7c4	v
centralistický	centralistický	k2eAgInSc4d1	centralistický
stát	stát	k1gInSc4	stát
a	a	k8xC	a
k	k	k7c3	k
faktickému	faktický	k2eAgNnSc3d1	faktické
zrušení	zrušení	k1gNnSc3	zrušení
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1952	[number]	k4	1952
se	se	k3xPyFc4	se
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
dosavadních	dosavadní	k2eAgFnPc6d1	dosavadní
zemích	zem	k1gFnPc6	zem
NDR	NDR	kA	NDR
konaly	konat	k5eAaImAgFnP	konat
poslední	poslední	k2eAgFnPc4d1	poslední
schůze	schůze	k1gFnPc4	schůze
zemských	zemský	k2eAgInPc2d1	zemský
sněmů	sněm	k1gInPc2	sněm
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
měly	mít	k5eAaImAgFnP	mít
nyní	nyní	k6eAd1	nyní
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
odhlasovat	odhlasovat	k5eAaPmF	odhlasovat
zákony	zákon	k1gInPc4	zákon
stanovující	stanovující	k2eAgFnSc2d1	stanovující
nové	nový	k2eAgFnSc2d1	nová
rozdělení	rozdělení	k1gNnSc4	rozdělení
na	na	k7c4	na
kraje	kraj	k1gInPc4	kraj
a	a	k8xC	a
okresy	okres	k1gInPc4	okres
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
již	již	k6eAd1	již
nerespektovaly	respektovat	k5eNaImAgInP	respektovat
stávající	stávající	k2eAgFnSc1d1	stávající
hranice	hranice	k1gFnSc1	hranice
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
až	až	k9	až
na	na	k7c4	na
výjimky	výjimek	k1gInPc4	výjimek
historické	historický	k2eAgInPc4d1	historický
(	(	kIx(	(
<g/>
ještě	ještě	k9	ještě
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
Vídeňského	vídeňský	k2eAgInSc2d1	vídeňský
kongresu	kongres	k1gInSc2	kongres
<g/>
;	;	kIx,	;
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
tehdejších	tehdejší	k2eAgFnPc2d1	tehdejší
meklenbursko-braniborských	meklenburskoraniborský	k2eAgFnPc2d1	meklenbursko-braniborský
hranic	hranice	k1gFnPc2	hranice
staré	starý	k2eAgFnSc2d1	stará
řadu	řada	k1gFnSc4	řada
staletí	staletí	k1gNnSc2	staletí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NDR	NDR	kA	NDR
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
členila	členit	k5eAaImAgFnS	členit
na	na	k7c4	na
15	[number]	k4	15
krajů	kraj	k1gInPc2	kraj
(	(	kIx(	(
<g/>
Bezirke	Bezirke	k1gInSc1	Bezirke
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Berlín	Berlín	k1gInSc1	Berlín
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
NDR	NDR	kA	NDR
<g/>
,	,	kIx,	,
Drážďany	Drážďany	k1gInPc1	Drážďany
<g/>
,	,	kIx,	,
Karl-Marx-Stadt	Karl-Marx-Stadt	k1gInSc1	Karl-Marx-Stadt
<g/>
,	,	kIx,	,
Lipsko	Lipsko	k1gNnSc1	Lipsko
<g/>
,	,	kIx,	,
Gera	Gera	k1gFnSc1	Gera
<g/>
,	,	kIx,	,
Erfurt	Erfurt	k1gInSc1	Erfurt
<g/>
,	,	kIx,	,
Suhl	Suhl	k1gInSc1	Suhl
<g/>
,	,	kIx,	,
Halle	Halle	k1gInSc1	Halle
<g/>
,	,	kIx,	,
Magdeburg	Magdeburg	k1gInSc1	Magdeburg
<g/>
,	,	kIx,	,
Chotěbuz	Chotěbuz	k1gFnSc1	Chotěbuz
<g/>
,	,	kIx,	,
Postupim	Postupim	k1gFnSc1	Postupim
<g/>
,	,	kIx,	,
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
nad	nad	k7c7	nad
Odrou	Odra	k1gFnSc7	Odra
<g/>
,	,	kIx,	,
Neubrandenburg	Neubrandenburg	k1gInSc1	Neubrandenburg
<g/>
,	,	kIx,	,
Schwerin	Schwerin	k1gInSc1	Schwerin
<g/>
,	,	kIx,	,
Rostock	Rostock	k1gInSc1	Rostock
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
členily	členit	k5eAaImAgFnP	členit
na	na	k7c4	na
okresy	okres	k1gInPc4	okres
(	(	kIx(	(
<g/>
Kreise	Kreise	k1gFnPc4	Kreise
<g/>
)	)	kIx)	)
a	a	k8xC	a
městské	městský	k2eAgInPc1d1	městský
okresy	okres	k1gInPc1	okres
(	(	kIx(	(
<g/>
Stadtkreise	Stadtkreise	k1gFnSc1	Stadtkreise
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
z	z	k7c2	z
19	[number]	k4	19
milionů	milion	k4xCgInPc2	milion
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
na	na	k7c4	na
16	[number]	k4	16
milionů	milion	k4xCgInPc2	milion
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
především	především	k6eAd1	především
houfným	houfný	k2eAgInSc7d1	houfný
útěkem	útěk	k1gInSc7	útěk
obyvatel	obyvatel	k1gMnPc2	obyvatel
do	do	k7c2	do
Západního	západní	k2eAgNnSc2d1	západní
Německa	Německo	k1gNnSc2	Německo
-	-	kIx~	-
celkem	celkem	k6eAd1	celkem
jedna	jeden	k4xCgFnSc1	jeden
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
obyvatel	obyvatel	k1gMnPc2	obyvatel
emigrovala	emigrovat	k5eAaBmAgFnS	emigrovat
přes	přes	k7c4	přes
západní	západní	k2eAgInSc4d1	západní
Berlín	Berlín	k1gInSc4	Berlín
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
postavena	postaven	k2eAgFnSc1d1	postavena
Berlínská	berlínský	k2eAgFnSc1d1	Berlínská
zeď	zeď	k1gFnSc1	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
NDR	NDR	kA	NDR
trpěla	trpět	k5eAaImAgFnS	trpět
nízkou	nízký	k2eAgFnSc7d1	nízká
porodností	porodnost	k1gFnSc7	porodnost
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ostře	ostro	k6eAd1	ostro
kontrastovalo	kontrastovat	k5eAaImAgNnS	kontrastovat
např.	např.	kA	např.
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
populace	populace	k1gFnSc1	populace
se	se	k3xPyFc4	se
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
z	z	k7c2	z
24	[number]	k4	24
milionů	milion	k4xCgInPc2	milion
(	(	kIx(	(
<g/>
o	o	k7c6	o
málo	málo	k6eAd1	málo
více	hodně	k6eAd2	hodně
než	než	k8xS	než
NDR	NDR	kA	NDR
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
na	na	k7c4	na
38	[number]	k4	38
milionů	milion	k4xCgInPc2	milion
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvojnásobek	dvojnásobek	k1gInSc1	dvojnásobek
populace	populace	k1gFnSc2	populace
NDR	NDR	kA	NDR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Východoněmečtí	východoněmecký	k2eAgMnPc1d1	východoněmecký
politici	politik	k1gMnPc1	politik
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Seznam	seznam	k1gInSc1	seznam
nejvyšších	vysoký	k2eAgMnPc2d3	nejvyšší
představitelů	představitel	k1gMnPc2	představitel
NDR	NDR	kA	NDR
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Galerie	galerie	k1gFnSc1	galerie
generálních	generální	k2eAgInPc2d1	generální
tajemníků	tajemník	k1gInPc2	tajemník
SED	sed	k1gInSc1	sed
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Nefjodov	Nefjodov	k1gInSc1	Nefjodov
V.	V.	kA	V.
V.	V.	kA	V.
SED	sed	k1gInSc1	sed
a	a	k8xC	a
kultura	kultura	k1gFnSc1	kultura
NDR	NDR	kA	NDR
<g/>
.	.	kIx.	.
</s>
<s>
Historická	historický	k2eAgFnSc1d1	historická
monografie	monografie	k1gFnSc1	monografie
<g/>
.	.	kIx.	.
</s>
<s>
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Východoněmecké	východoněmecký	k2eAgNnSc1d1	východoněmecké
povstání	povstání	k1gNnSc1	povstání
</s>
</p>
<p>
<s>
Stasi	stase	k1gFnSc4	stase
</s>
</p>
<p>
<s>
Berlínská	berlínský	k2eAgFnSc1d1	Berlínská
zeď	zeď	k1gFnSc1	zeď
</s>
</p>
<p>
<s>
Checkpoint	Checkpoint	k1gMnSc1	Checkpoint
Charlie	Charlie	k1gMnSc1	Charlie
</s>
</p>
<p>
<s>
Památník	památník	k1gInSc1	památník
Berlín-Hohenschönhausen	Berlín-Hohenschönhausna	k1gFnPc2	Berlín-Hohenschönhausna
</s>
</p>
<p>
<s>
Karl-Marx-Stadt	Karl-Marx-Stadt	k1gInSc1	Karl-Marx-Stadt
</s>
</p>
<p>
<s>
Trabant	trabant	k1gMnSc1	trabant
</s>
</p>
<p>
<s>
Ostalgie	Ostalgie	k1gFnSc1	Ostalgie
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Německá	německý	k2eAgFnSc1d1	německá
demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Mapa	mapa	k1gFnSc1	mapa
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
Länder	Länder	k1gInSc1	Länder
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
federalistická	federalistický	k2eAgFnSc1d1	federalistická
ústava	ústava	k1gFnSc1	ústava
NDR	NDR	kA	NDR
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
opět	opět	k6eAd1	opět
federalistická	federalistický	k2eAgFnSc1d1	federalistická
ústava	ústava	k1gFnSc1	ústava
NDR	NDR	kA	NDR
z	z	k7c2	z
dubna	duben	k1gInSc2	duben
1990	[number]	k4	1990
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Povstání	povstání	k1gNnSc1	povstání
dělníků	dělník	k1gMnPc2	dělník
v	v	k7c6	v
NDR	NDR	kA	NDR
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
červen	červen	k1gInSc1	červen
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
17	[number]	k4	17
<g/>
.	.	kIx.	.
</s>
<s>
Juni	Juni	k6eAd1	Juni
1953	[number]	k4	1953
-	-	kIx~	-
17	[number]	k4	17
<g/>
.	.	kIx.	.
červen	červen	k1gInSc1	červen
1953	[number]	k4	1953
<g/>
.	.	kIx.	.
</s>
<s>
Bibliographical	Bibliographicat	k5eAaPmAgMnS	Bibliographicat
database	database	k6eAd1	database
of	of	k?	of
the	the	k?	the
international	internationat	k5eAaImAgMnS	internationat
literature	literatur	k1gInSc5	literatur
</s>
</p>
