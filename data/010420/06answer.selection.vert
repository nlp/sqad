<s>
Německá	německý	k2eAgFnSc1d1	německá
demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
NDR	NDR	kA	NDR
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
východní	východní	k2eAgNnSc4d1	východní
Německo	Německo	k1gNnSc4	Německo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
německy	německy	k6eAd1	německy
mluvících	mluvící	k2eAgFnPc6d1	mluvící
zemích	zem	k1gFnPc6	zem
i	i	k8xC	i
jinde	jinde	k6eAd1	jinde
běžně	běžně	k6eAd1	běžně
DDR	DDR	kA	DDR
<g/>
,	,	kIx,	,
zkratka	zkratka	k1gFnSc1	zkratka
pro	pro	k7c4	pro
Deutsche	Deutsche	k1gNnSc4	Deutsche
Demokratische	Demokratisch	k1gFnSc2	Demokratisch
Republik	republika	k1gFnPc2	republika
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1949	[number]	k4	1949
lidově	lidově	k6eAd1	lidově
demokratickým	demokratický	k2eAgInSc7d1	demokratický
státem	stát	k1gInSc7	stát
na	na	k7c6	na
území	území	k1gNnSc6	území
sovětské	sovětský	k2eAgFnSc2d1	sovětská
okupační	okupační	k2eAgFnSc2d1	okupační
zóny	zóna	k1gFnSc2	zóna
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
východního	východní	k2eAgInSc2d1	východní
sektoru	sektor	k1gInSc2	sektor
Berlína	Berlín	k1gInSc2	Berlín
<g/>
.	.	kIx.	.
</s>
