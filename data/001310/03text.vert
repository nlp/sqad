<s>
Cín	cín	k1gInSc1	cín
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Sn	Sn	k1gFnSc2	Sn
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Stannum	Stannum	k1gInSc1	Stannum
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
kovy	kov	k1gInPc7	kov
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
lidstvu	lidstvo	k1gNnSc3	lidstvo
již	již	k9	již
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
především	především	k6eAd1	především
jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
slitiny	slitina	k1gFnSc2	slitina
zvané	zvaný	k2eAgFnSc2d1	zvaná
bronz	bronz	k1gInSc4	bronz
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
nízký	nízký	k2eAgInSc1d1	nízký
bod	bod	k1gInSc1	bod
tání	tání	k1gNnSc2	tání
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
kujný	kujný	k2eAgInSc1d1	kujný
a	a	k8xC	a
odolný	odolný	k2eAgInSc1d1	odolný
vůči	vůči	k7c3	vůči
korozi	koroze	k1gFnSc3	koroze
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nP	nacházet
využití	využití	k1gNnSc4	využití
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
slitin	slitina	k1gFnPc2	slitina
(	(	kIx(	(
<g/>
bronz	bronz	k1gInSc4	bronz
<g/>
,	,	kIx,	,
pájky	pájka	k1gFnPc1	pájka
<g/>
,	,	kIx,	,
ložiskový	ložiskový	k2eAgInSc1d1	ložiskový
kov	kov	k1gInSc1	kov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
potravinářství	potravinářství	k1gNnSc6	potravinářství
při	při	k7c6	při
dlouhodobém	dlouhodobý	k2eAgNnSc6d1	dlouhodobé
uchovávání	uchovávání	k1gNnSc6	uchovávání
potravin	potravina	k1gFnPc2	potravina
(	(	kIx(	(
<g/>
pocínování	pocínování	k1gNnPc1	pocínování
konzerv	konzerva	k1gFnPc2	konzerva
<g/>
,	,	kIx,	,
cínové	cínový	k2eAgFnPc4d1	cínová
fólie	fólie	k1gFnPc4	fólie
<g/>
)	)	kIx)	)
a	a	k8xC	a
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
uměleckých	umělecký	k2eAgInPc2d1	umělecký
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Nízkotavitelný	Nízkotavitelný	k2eAgInSc1d1	Nízkotavitelný
kov	kov	k1gInSc1	kov
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc1d1	používaný
člověkem	člověk	k1gMnSc7	člověk
již	již	k6eAd1	již
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
<g/>
.	.	kIx.	.
</s>
<s>
Cín	cín	k1gInSc1	cín
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
normálním	normální	k2eAgNnSc6d1	normální
prostředí	prostředí	k1gNnSc6	prostředí
značně	značně	k6eAd1	značně
odolný	odolný	k2eAgMnSc1d1	odolný
proti	proti	k7c3	proti
korozi	koroze	k1gFnSc3	koroze
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
zdravotně	zdravotně	k6eAd1	zdravotně
prakticky	prakticky	k6eAd1	prakticky
nezávadný	závadný	k2eNgInSc1d1	nezávadný
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
stříbrobílý	stříbrobílý	k2eAgInSc1d1	stříbrobílý
lesklý	lesklý	k2eAgInSc1d1	lesklý
kov	kov	k1gInSc1	kov
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
tvrdý	tvrdý	k2eAgMnSc1d1	tvrdý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
tažný	tažný	k2eAgInSc1d1	tažný
<g/>
.	.	kIx.	.
</s>
<s>
Takže	takže	k9	takže
jej	on	k3xPp3gMnSc4	on
lze	lze	k6eAd1	lze
válcovat	válcovat	k5eAaImF	válcovat
na	na	k7c4	na
velmi	velmi	k6eAd1	velmi
tenké	tenký	k2eAgFnPc4d1	tenká
fólie	fólie	k1gFnPc4	fólie
(	(	kIx(	(
<g/>
obalový	obalový	k2eAgInSc4d1	obalový
materiál	materiál	k1gInSc4	materiál
staniol	staniol	k1gInSc1	staniol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
mocenství	mocenství	k1gNnSc6	mocenství
<g/>
:	:	kIx,	:
Sn	Sn	k1gMnSc1	Sn
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
a	a	k8xC	a
Sn	Sn	k1gMnSc1	Sn
<g/>
+	+	kIx~	+
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Cín	cín	k1gInSc1	cín
je	být	k5eAaImIp3nS	být
vůči	vůči	k7c3	vůči
vzduchu	vzduch	k1gInSc3	vzduch
i	i	k8xC	i
vodě	voda	k1gFnSc3	voda
za	za	k7c2	za
normální	normální	k2eAgFnSc2d1	normální
teploty	teplota	k1gFnSc2	teplota
stálý	stálý	k2eAgInSc1d1	stálý
<g/>
.	.	kIx.	.
</s>
<s>
Vůči	vůči	k7c3	vůči
působení	působení	k1gNnSc3	působení
silných	silný	k2eAgFnPc2d1	silná
minerálních	minerální	k2eAgFnPc2d1	minerální
kyselin	kyselina	k1gFnPc2	kyselina
není	být	k5eNaImIp3nS	být
cín	cín	k1gInSc1	cín
příliš	příliš	k6eAd1	příliš
odolný	odolný	k2eAgInSc1d1	odolný
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
ochotně	ochotně	k6eAd1	ochotně
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
především	především	k9	především
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
chlorovodíkové	chlorovodíkový	k2eAgFnSc6d1	chlorovodíková
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
i	i	k8xC	i
malých	malý	k2eAgFnPc2d1	malá
množství	množství	k1gNnSc4	množství
oxidačních	oxidační	k2eAgNnPc2d1	oxidační
činidel	činidlo	k1gNnPc2	činidlo
(	(	kIx(	(
<g/>
HNO	HNO	kA	HNO
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
silně	silně	k6eAd1	silně
alkalických	alkalický	k2eAgInPc6d1	alkalický
roztocích	roztok	k1gInPc6	roztok
se	se	k3xPyFc4	se
kovový	kovový	k2eAgInSc1d1	kovový
cín	cín	k1gInSc1	cín
poměrně	poměrně	k6eAd1	poměrně
rychle	rychle	k6eAd1	rychle
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
za	za	k7c2	za
vzniků	vznik	k1gInPc2	vznik
ciničitanového	ciničitanový	k2eAgInSc2d1	ciničitanový
aniontu	anion	k1gInSc2	anion
[	[	kIx(	[
<g/>
SnO	SnO	k1gFnSc1	SnO
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Cín	cín	k1gInSc1	cín
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
amfoterní	amfoterní	k2eAgNnSc1d1	amfoterní
<g/>
.	.	kIx.	.
</s>
<s>
Kovový	kovový	k2eAgInSc1d1	kovový
cín	cín	k1gInSc1	cín
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
alotropních	alotropní	k2eAgFnPc6d1	alotropní
modifikacích	modifikace	k1gFnPc6	modifikace
<g/>
:	:	kIx,	:
šedý	šedý	k2eAgInSc4d1	šedý
α	α	k?	α
<g/>
,	,	kIx,	,
krystalizující	krystalizující	k2eAgFnSc1d1	krystalizující
v	v	k7c6	v
kubické	kubický	k2eAgFnSc6d1	kubická
soustavě	soustava	k1gFnSc6	soustava
<g/>
,	,	kIx,	,
bílý	bílý	k2eAgInSc1d1	bílý
β	β	k?	β
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
tetragonální	tetragonální	k2eAgFnSc6d1	tetragonální
krystalické	krystalický	k2eAgFnSc6d1	krystalická
soustavě	soustava	k1gFnSc6	soustava
<g/>
,	,	kIx,	,
a	a	k8xC	a
γ	γ	k?	γ
krystalizující	krystalizující	k2eAgNnSc1d1	krystalizující
v	v	k7c6	v
kosočtverečné	kosočtverečný	k2eAgFnSc6d1	kosočtverečná
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Přechod	přechod	k1gInSc1	přechod
mezi	mezi	k7c7	mezi
formou	forma	k1gFnSc7	forma
bílého	bílý	k2eAgInSc2d1	bílý
a	a	k8xC	a
šedého	šedý	k2eAgInSc2d1	šedý
cínu	cín	k1gInSc2	cín
nastává	nastávat	k5eAaImIp3nS	nastávat
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
13,2	[number]	k4	13,2
°	°	k?	°
<g/>
C.	C.	kA	C.
Jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
cínové	cínový	k2eAgInPc1d1	cínový
předměty	předmět	k1gInPc1	předmět
(	(	kIx(	(
<g/>
nádoby	nádoba	k1gFnPc1	nádoba
<g/>
,	,	kIx,	,
sošky	soška	k1gFnPc1	soška
<g/>
)	)	kIx)	)
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
vystaveny	vystavit	k5eAaPmNgInP	vystavit
takto	takto	k6eAd1	takto
nízkým	nízký	k2eAgFnPc3d1	nízká
teplotám	teplota	k1gFnPc3	teplota
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
přechodu	přechod	k1gInSc3	přechod
původně	původně	k6eAd1	původně
bílého	bílý	k2eAgInSc2d1	bílý
cínu	cín	k1gInSc2	cín
na	na	k7c4	na
šedou	šedý	k2eAgFnSc4d1	šedá
modifikaci	modifikace	k1gFnSc4	modifikace
a	a	k8xC	a
předmět	předmět	k1gInSc1	předmět
se	se	k3xPyFc4	se
rozpadne	rozpadnout	k5eAaPmIp3nS	rozpadnout
na	na	k7c4	na
prach	prach	k1gInSc4	prach
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
je	být	k5eAaImIp3nS	být
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xC	jako
cínový	cínový	k2eAgInSc1d1	cínový
mor	mor	k1gInSc1	mor
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
znám	znám	k2eAgInSc1d1	znám
již	již	k6eAd1	již
od	od	k7c2	od
středověku	středověk	k1gInSc2	středověk
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
přes	přes	k7c4	přes
zimu	zima	k1gFnSc4	zima
teploty	teplota	k1gFnSc2	teplota
v	v	k7c6	v
hradních	hradní	k2eAgFnPc6d1	hradní
místnostech	místnost	k1gFnPc6	místnost
mohly	moct	k5eAaImAgFnP	moct
klesnout	klesnout	k5eAaPmF	klesnout
pod	pod	k7c4	pod
uvedenou	uvedený	k2eAgFnSc4d1	uvedená
hodnotu	hodnota	k1gFnSc4	hodnota
a	a	k8xC	a
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zničení	zničení	k1gNnSc3	zničení
cínových	cínový	k2eAgFnPc2d1	cínová
nádob	nádoba	k1gFnPc2	nádoba
<g/>
.	.	kIx.	.
γ	γ	k?	γ
vzniká	vznikat	k5eAaImIp3nS	vznikat
z	z	k7c2	z
bílého	bílý	k2eAgNnSc2d1	bílé
β	β	k?	β
až	až	k6eAd1	až
při	při	k7c6	při
160	[number]	k4	160
°	°	k?	°
<g/>
C.	C.	kA	C.
Za	za	k7c2	za
extrémně	extrémně	k6eAd1	extrémně
nízkých	nízký	k2eAgFnPc2d1	nízká
teplot	teplota	k1gFnPc2	teplota
pod	pod	k7c4	pod
3,72	[number]	k4	3,72
K	K	kA	K
je	být	k5eAaImIp3nS	být
cín	cín	k1gInSc1	cín
supravodičem	supravodič	k1gInSc7	supravodič
I.	I.	kA	I.
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
je	být	k5eAaImIp3nS	být
cín	cín	k1gInSc1	cín
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
poměrně	poměrně	k6eAd1	poměrně
vzácným	vzácný	k2eAgInSc7d1	vzácný
prvkem	prvek	k1gInSc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
obsah	obsah	k1gInSc1	obsah
činí	činit	k5eAaImIp3nS	činit
pouze	pouze	k6eAd1	pouze
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
ppm	ppm	k?	ppm
(	(	kIx(	(
<g/>
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
činí	činit	k5eAaImIp3nS	činit
jeho	jeho	k3xOp3gFnSc1	jeho
koncentrace	koncentrace	k1gFnSc1	koncentrace
pouze	pouze	k6eAd1	pouze
3	[number]	k4	3
mikrogramy	mikrogram	k1gInPc4	mikrogram
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
litru	litr	k1gInSc6	litr
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
cínu	cín	k1gInSc2	cín
přibližně	přibližně	k6eAd1	přibližně
30	[number]	k4	30
miliard	miliarda	k4xCgFnPc2	miliarda
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
cínovou	cínový	k2eAgFnSc7d1	cínová
rudou	ruda	k1gFnSc7	ruda
je	být	k5eAaImIp3nS	být
kasiterit	kasiterit	k1gInSc1	kasiterit
neboli	neboli	k8xC	neboli
cínovec	cínovec	k1gInSc1	cínovec
<g/>
,	,	kIx,	,
chemicky	chemicky	k6eAd1	chemicky
oxid	oxid	k1gInSc4	oxid
cíničitý	cíničitý	k2eAgInSc4d1	cíničitý
SnO	SnO	k1gFnSc7	SnO
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
78,62	[number]	k4	78,62
%	%	kIx~	%
cínu	cín	k1gInSc2	cín
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzácná	vzácný	k2eAgFnSc1d1	vzácná
cínová	cínový	k2eAgFnSc1d1	cínová
ruda	ruda	k1gFnSc1	ruda
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
stannin	stannin	k2eAgMnSc1d1	stannin
Cu	Cu	k1gMnSc1	Cu
<g/>
2	[number]	k4	2
<g/>
S.	S.	kA	S.
<g/>
FeS	fes	k1gNnSc2	fes
<g/>
.	.	kIx.	.
<g/>
SnS	SnS	k1gFnSc2	SnS
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Cínovec	Cínovec	k1gInSc1	Cínovec
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
cínovcových	cínovcový	k2eAgFnPc6d1	cínovcový
žilách	žíla	k1gFnPc6	žíla
a	a	k8xC	a
pegmatitech	pegmatit	k1gInPc6	pegmatit
<g/>
,	,	kIx,	,
hromadí	hromadit	k5eAaImIp3nP	hromadit
se	se	k3xPyFc4	se
v	v	k7c6	v
náplavech	náplav	k1gInPc6	náplav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
bohatá	bohatý	k2eAgNnPc1d1	bohaté
naleziště	naleziště	k1gNnPc1	naleziště
cínových	cínový	k2eAgFnPc2d1	cínová
rud	ruda	k1gFnPc2	ruda
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Cínovec	Cínovec	k1gInSc1	Cínovec
<g/>
,	,	kIx,	,
Horní	horní	k2eAgFnSc1d1	horní
Krupka	krupka	k1gFnSc1	krupka
<g/>
,	,	kIx,	,
Horní	horní	k2eAgInSc1d1	horní
Slavkov	Slavkov	k1gInSc1	Slavkov
<g/>
.	.	kIx.	.
</s>
<s>
Světová	světový	k2eAgNnPc1d1	světové
ložiska	ložisko	k1gNnPc1	ložisko
jsou	být	k5eAaImIp3nP	být
především	především	k9	především
v	v	k7c6	v
Barmě	Barma	k1gFnSc6	Barma
<g/>
,	,	kIx,	,
Indonésii	Indonésie	k1gFnSc6	Indonésie
<g/>
,	,	kIx,	,
Malajsii	Malajsie	k1gFnSc6	Malajsie
<g/>
,	,	kIx,	,
Bolívii	Bolívie	k1gFnSc6	Bolívie
<g/>
,	,	kIx,	,
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
,	,	kIx,	,
Rusku	Rusko	k1gNnSc6	Rusko
(	(	kIx(	(
<g/>
Jakutsko	Jakutsko	k1gNnSc1	Jakutsko
a	a	k8xC	a
Čukotka	Čukotka	k1gFnSc1	Čukotka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nigérii	Nigérie	k1gFnSc4	Nigérie
a	a	k8xC	a
Austrálii	Austrálie	k1gFnSc4	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
méně	málo	k6eAd2	málo
známými	známý	k2eAgFnPc7d1	známá
rudami	ruda	k1gFnPc7	ruda
cínu	cín	k1gInSc2	cín
jsou	být	k5eAaImIp3nP	být
franckeit	franckeit	k5eAaImF	franckeit
Pb	Pb	k1gFnPc4	Pb
<g/>
5	[number]	k4	5
<g/>
Sn	Sn	k1gFnSc2	Sn
<g/>
3	[number]	k4	3
<g/>
Sb	sb	kA	sb
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
14	[number]	k4	14
<g/>
,	,	kIx,	,
stanin	stanin	k1gInSc1	stanin
Cu	Cu	k1gFnSc2	Cu
<g/>
2	[number]	k4	2
<g/>
FeSnS	FeSnS	k1gFnSc1	FeSnS
<g/>
4	[number]	k4	4
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
Cu	Cu	k1gFnSc1	Cu
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
•	•	k?	•
<g/>
FeS	fes	k1gNnPc7	fes
<g/>
•	•	k?	•
<g/>
SnS	SnS	k1gMnSc1	SnS
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
a	a	k8xC	a
cylindrit	cylindrit	k1gInSc1	cylindrit
Pb	Pb	k1gFnSc2	Pb
<g/>
3	[number]	k4	3
<g/>
Sn	Sn	k1gFnSc1	Sn
<g/>
4	[number]	k4	4
<g/>
FeSb	FeSb	k1gInSc1	FeSb
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
kovového	kovový	k2eAgInSc2d1	kovový
cínu	cín	k1gInSc2	cín
z	z	k7c2	z
rudy	ruda	k1gFnSc2	ruda
je	být	k5eAaImIp3nS	být
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
žárovou	žárový	k2eAgFnSc4d1	Žárová
redukci	redukce	k1gFnSc4	redukce
uhlím	uhlí	k1gNnSc7	uhlí
v	v	k7c6	v
šachtových	šachtový	k2eAgFnPc6d1	šachtová
nebo	nebo	k8xC	nebo
plamenných	plamenný	k2eAgFnPc6d1	plamenná
pecích	pec	k1gFnPc6	pec
<g/>
:	:	kIx,	:
SnO	SnO	k1gFnSc1	SnO
<g/>
2	[number]	k4	2
+	+	kIx~	+
2	[number]	k4	2
C	C	kA	C
→	→	k?	→
Sn	Sn	k1gMnSc1	Sn
+	+	kIx~	+
2	[number]	k4	2
CO	co	k8xS	co
Ve	v	k7c6	v
strusce	struska	k1gFnSc6	struska
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
pochodu	pochod	k1gInSc6	pochod
je	být	k5eAaImIp3nS	být
ještě	ještě	k9	ještě
stále	stále	k6eAd1	stále
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
cínu	cín	k1gInSc2	cín
a	a	k8xC	a
lze	lze	k6eAd1	lze
jej	on	k3xPp3gInSc4	on
získat	získat	k5eAaPmF	získat
redukčním	redukční	k2eAgInSc7d1	redukční
pochodem	pochod	k1gInSc7	pochod
(	(	kIx(	(
<g/>
tavením	tavení	k1gNnSc7	tavení
strusky	struska	k1gFnSc2	struska
v	v	k7c6	v
plamenných	plamenný	k2eAgFnPc6d1	plamenná
pecích	pec	k1gFnPc6	pec
s	s	k7c7	s
oxidem	oxid	k1gInSc7	oxid
vápenatým	vápenatý	k2eAgInSc7d1	vápenatý
a	a	k8xC	a
uhlím	uhlí	k1gNnSc7	uhlí
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
srážecím	srážecí	k2eAgInSc7d1	srážecí
pochodem	pochod	k1gInSc7	pochod
(	(	kIx(	(
<g/>
tavením	tavení	k1gNnSc7	tavení
se	s	k7c7	s
železnými	železný	k2eAgInPc7d1	železný
odpadky	odpadek	k1gInPc7	odpadek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
SnSiO	SnSiO	k?	SnSiO
<g/>
3	[number]	k4	3
+	+	kIx~	+
CaO	CaO	k1gMnSc1	CaO
+	+	kIx~	+
C	C	kA	C
→	→	k?	→
Sn	Sn	k1gFnSc1	Sn
+	+	kIx~	+
CaSiO	CaSiO	k1gFnSc1	CaSiO
<g/>
3	[number]	k4	3
+	+	kIx~	+
CO	co	k8xS	co
redukční	redukční	k2eAgInSc1d1	redukční
pochod	pochod	k1gInSc1	pochod
SnSiO	SnSiO	k1gFnSc1	SnSiO
<g/>
3	[number]	k4	3
+	+	kIx~	+
Fe	Fe	k1gMnPc2	Fe
→	→	k?	→
FeSiO	FeSiO	k1gFnSc7	FeSiO
<g/>
3	[number]	k4	3
+	+	kIx~	+
Sn	Sn	k1gFnSc7	Sn
srážecí	srážecí	k2eAgInSc4d1	srážecí
pochod	pochod	k1gInSc4	pochod
K	k	k7c3	k
opětovné	opětovný	k2eAgFnSc3d1	opětovná
regeneraci	regenerace	k1gFnSc3	regenerace
pocínovaných	pocínovaný	k2eAgInPc2d1	pocínovaný
předmětů	předmět	k1gInPc2	předmět
lze	lze	k6eAd1	lze
využít	využít	k5eAaPmF	využít
elektrolýzu	elektrolýza	k1gFnSc4	elektrolýza
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
cín	cín	k1gInSc1	cín
z	z	k7c2	z
pocínovaných	pocínovaný	k2eAgInPc2d1	pocínovaný
kovových	kovový	k2eAgInPc2d1	kovový
povrchů	povrch	k1gInPc2	povrch
zpětně	zpětně	k6eAd1	zpětně
získává	získávat	k5eAaImIp3nS	získávat
působením	působení	k1gNnSc7	působení
plynného	plynný	k2eAgInSc2d1	plynný
chloru	chlor	k1gInSc2	chlor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bibli	bible	k1gFnSc6	bible
i	i	k8xC	i
v	v	k7c6	v
nejstarších	starý	k2eAgInPc6d3	nejstarší
spisech	spis	k1gInPc6	spis
Řeků	Řek	k1gMnPc2	Řek
a	a	k8xC	a
Římanů	Říman	k1gMnPc2	Říman
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
pojmy	pojem	k1gInPc1	pojem
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mnozí	mnohý	k2eAgMnPc1d1	mnohý
badatelé	badatel	k1gMnPc1	badatel
vztahují	vztahovat	k5eAaImIp3nP	vztahovat
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
cínu	cín	k1gInSc2	cín
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
nijak	nijak	k6eAd1	nijak
vyloučeno	vyloučit	k5eAaPmNgNnS	vyloučit
<g/>
,	,	kIx,	,
že	že	k8xS	že
cín	cín	k1gInSc1	cín
byl	být	k5eAaImAgInS	být
znám	znám	k2eAgInSc1d1	znám
velmi	velmi	k6eAd1	velmi
dávno	dávno	k6eAd1	dávno
<g/>
,	,	kIx,	,
především	především	k9	především
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
poměrně	poměrně	k6eAd1	poměrně
snadnou	snadný	k2eAgFnSc4d1	snadná
výrobu	výroba	k1gFnSc4	výroba
z	z	k7c2	z
rud	ruda	k1gFnPc2	ruda
<g/>
.	.	kIx.	.
</s>
<s>
Herodotos	Herodotos	k1gMnSc1	Herodotos
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
cínových	cínový	k2eAgInPc6d1	cínový
ostrovech	ostrov	k1gInPc6	ostrov
a	a	k8xC	a
Plinius	Plinius	k1gMnSc1	Plinius
starší	starší	k1gMnSc1	starší
píše	psát	k5eAaImIp3nS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
cín	cín	k1gInSc1	cín
je	být	k5eAaImIp3nS	být
dražší	drahý	k2eAgMnSc1d2	dražší
než	než	k8xS	než
olovo	olovo	k1gNnSc1	olovo
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
dováží	dovážet	k5eAaImIp3nP	dovážet
z	z	k7c2	z
cínových	cínový	k2eAgInPc2d1	cínový
ostrovů	ostrov	k1gInPc2	ostrov
v	v	k7c6	v
Atlantském	atlantský	k2eAgInSc6d1	atlantský
oceánu	oceán	k1gInSc6	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
cínové	cínový	k2eAgInPc4d1	cínový
ostrovy	ostrov	k1gInPc4	ostrov
se	se	k3xPyFc4	se
pokládají	pokládat	k5eAaImIp3nP	pokládat
ostrovy	ostrov	k1gInPc1	ostrov
Scillské	Scillský	k2eAgInPc1d1	Scillský
u	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
jižní	jižní	k2eAgFnSc2d1	jižní
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
Féničané	Féničan	k1gMnPc1	Féničan
cín	cín	k1gInSc4	cín
vyváželi	vyvážet	k5eAaImAgMnP	vyvážet
do	do	k7c2	do
Východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
Západní	západní	k2eAgFnSc2d1	západní
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
poskytovalo	poskytovat	k5eAaImAgNnS	poskytovat
Féničanům	Féničan	k1gMnPc3	Féničan
dostatek	dostatek	k1gInSc1	dostatek
cínu	cín	k1gInSc2	cín
Španělsko	Španělsko	k1gNnSc4	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Těžba	těžba	k1gFnSc1	těžba
cínové	cínový	k2eAgFnSc2d1	cínová
rudy	ruda	k1gFnSc2	ruda
na	na	k7c6	na
Pyrenejském	pyrenejský	k2eAgInSc6d1	pyrenejský
poloostrově	poloostrov	k1gInSc6	poloostrov
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Maurové	Maurové	k?	Maurové
usazovali	usazovat	k5eAaImAgMnP	usazovat
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
byla	být	k5eAaImAgFnS	být
nejdůležitější	důležitý	k2eAgNnSc4d3	nejdůležitější
naleziště	naleziště	k1gNnSc4	naleziště
cínovce	cínovec	k1gInSc2	cínovec
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
v	v	k7c6	v
Cornwallu	Cornwall	k1gInSc6	Cornwall
a	a	k8xC	a
Devonu	devon	k1gInSc6	devon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Krušných	krušný	k2eAgFnPc6d1	krušná
horách	hora	k1gFnPc6	hora
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
proslavily	proslavit	k5eAaPmAgFnP	proslavit
bohatstvím	bohatství	k1gNnSc7	bohatství
cínové	cínový	k2eAgFnSc2d1	cínová
rudy	ruda	k1gFnSc2	ruda
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
s	s	k7c7	s
rýžováním	rýžování	k1gNnSc7	rýžování
cínu	cín	k1gInSc2	cín
nejpozději	pozdě	k6eAd3	pozdě
v	v	k7c6	v
době	doba	k1gFnSc6	doba
laténské	laténský	k2eAgFnSc6d1	laténská
<g/>
.	.	kIx.	.
</s>
<s>
Keltové	Kelt	k1gMnPc1	Kelt
těžili	těžit	k5eAaImAgMnP	těžit
náplavy	náplav	k1gInPc4	náplav
s	s	k7c7	s
kasiteritem	kasiterit	k1gInSc7	kasiterit
pomocí	pomoc	k1gFnPc2	pomoc
uměle	uměle	k6eAd1	uměle
vytvořených	vytvořený	k2eAgInPc2d1	vytvořený
kanálů	kanál	k1gInPc2	kanál
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
pod	pod	k7c7	pod
Cínovcem	Cínovec	k1gInSc7	Cínovec
a	a	k8xC	a
horou	hora	k1gFnSc7	hora
Pramenáč	Pramenáč	k1gMnSc1	Pramenáč
<g/>
.	.	kIx.	.
</s>
<s>
Obce	obec	k1gFnPc1	obec
Střelná	střelný	k2eAgFnSc1d1	střelná
<g/>
,	,	kIx,	,
Mstišov	Mstišov	k1gInSc1	Mstišov
a	a	k8xC	a
Košťany	Košťan	k1gMnPc4	Košťan
si	se	k3xPyFc3	se
památku	památka	k1gFnSc4	památka
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
rýžování	rýžování	k1gNnSc2	rýžování
nesou	nést	k5eAaImIp3nP	nést
dodnes	dodnes	k6eAd1	dodnes
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
názvech	název	k1gInPc6	název
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
obcích	obec	k1gFnPc6	obec
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
i	i	k9	i
nádrže	nádrž	k1gFnPc1	nádrž
na	na	k7c4	na
tavení	tavení	k1gNnSc4	tavení
kasiteritu	kasiterit	k1gInSc2	kasiterit
se	s	k7c7	s
struskou	struska	k1gFnSc7	struska
(	(	kIx(	(
<g/>
hubeltrog	hubeltrog	k1gInSc1	hubeltrog
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
používány	používat	k5eAaImNgFnP	používat
jako	jako	k9	jako
nádrže	nádrž	k1gFnPc1	nádrž
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
(	(	kIx(	(
<g/>
Mstišov	Mstišov	k1gInSc4	Mstišov
a	a	k8xC	a
Běhánky	Běhánek	k1gInPc4	Běhánek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlubinným	hlubinný	k2eAgNnSc7d1	hlubinné
dobýváním	dobývání	k1gNnSc7	dobývání
cínu	cín	k1gInSc2	cín
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
Krupce	krupka	k1gFnSc6	krupka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1200	[number]	k4	1200
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgFnP	být
založeny	založit	k5eAaPmNgFnP	založit
hutě	huť	k1gFnPc1	huť
v	v	k7c6	v
Cínovci	Cínovec	k1gInSc6	Cínovec
a	a	k8xC	a
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
ve	v	k7c6	v
Slavkově	Slavkov	k1gInSc6	Slavkov
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
hutě	huť	k1gFnPc1	huť
a	a	k8xC	a
doly	dol	k1gInPc1	dol
byly	být	k5eAaImAgInP	být
zničeny	zničit	k5eAaPmNgInP	zničit
za	za	k7c4	za
třicetileté	třicetiletý	k2eAgFnPc4d1	třicetiletá
války	válka	k1gFnPc4	válka
a	a	k8xC	a
poté	poté	k6eAd1	poté
nebyly	být	k5eNaImAgInP	být
dlouho	dlouho	k6eAd1	dlouho
obnoveny	obnovit	k5eAaPmNgInP	obnovit
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
funguje	fungovat	k5eAaImIp3nS	fungovat
pouze	pouze	k6eAd1	pouze
jedna	jeden	k4xCgFnSc1	jeden
huť	huť	k1gFnSc1	huť
<g/>
.	.	kIx.	.
</s>
<s>
Cínovec	Cínovec	k1gInSc1	Cínovec
se	se	k3xPyFc4	se
rozemele	rozemlít	k5eAaPmIp3nS	rozemlít
<g/>
,	,	kIx,	,
plaví	plavit	k5eAaImIp3nS	plavit
a	a	k8xC	a
potom	potom	k6eAd1	potom
se	se	k3xPyFc4	se
praží	pražit	k5eAaImIp3nS	pražit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
odstranily	odstranit	k5eAaPmAgFnP	odstranit
síra	síra	k1gFnSc1	síra
a	a	k8xC	a
arsen	arsen	k1gInSc1	arsen
<g/>
.	.	kIx.	.
</s>
<s>
Propírání	propírání	k1gNnSc1	propírání
i	i	k8xC	i
pražení	pražení	k1gNnSc1	pražení
rudy	ruda	k1gFnSc2	ruda
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
několikrát	několikrát	k6eAd1	několikrát
opakuje	opakovat	k5eAaImIp3nS	opakovat
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgInSc7	ten
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
obohatí	obohatit	k5eAaPmIp3nS	obohatit
cínová	cínový	k2eAgFnSc1d1	cínová
ruda	ruda	k1gFnSc1	ruda
na	na	k7c4	na
obsah	obsah	k1gInSc4	obsah
až	až	k9	až
70	[number]	k4	70
%	%	kIx~	%
cínu	cín	k1gInSc2	cín
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
cínovce	cínovec	k1gInSc2	cínovec
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
kov	kov	k1gInSc1	kov
redukcí	redukce	k1gFnPc2	redukce
uhlím	uhlí	k1gNnSc7	uhlí
v	v	k7c6	v
peci	pec	k1gFnSc6	pec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
v	v	k7c6	v
Krupce	krupka	k1gFnSc6	krupka
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
jedna	jeden	k4xCgFnSc1	jeden
šachtová	šachtový	k2eAgFnSc1d1	šachtová
pec	pec	k1gFnSc1	pec
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
plní	plnit	k5eAaImIp3nS	plnit
střídavě	střídavě	k6eAd1	střídavě
dřevěným	dřevěný	k2eAgNnSc7d1	dřevěné
uhlím	uhlí	k1gNnSc7	uhlí
a	a	k8xC	a
rudou	ruda	k1gFnSc7	ruda
<g/>
.	.	kIx.	.
</s>
<s>
Obdržený	obdržený	k2eAgInSc1d1	obdržený
cín	cín	k1gInSc1	cín
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
čistý	čistý	k2eAgInSc1d1	čistý
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
rafinuje	rafinovat	k5eAaImIp3nS	rafinovat
<g/>
.	.	kIx.	.
</s>
<s>
Rafinace	rafinace	k1gFnSc1	rafinace
cínu	cín	k1gInSc2	cín
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
na	na	k7c6	na
nakloněné	nakloněný	k2eAgFnSc6d1	nakloněná
nístěji	nístěj	k1gFnSc6	nístěj
<g/>
,	,	kIx,	,
pokryté	pokrytý	k2eAgNnSc1d1	pokryté
žhavým	žhavý	k2eAgNnSc7d1	žhavé
uhlím	uhlí	k1gNnSc7	uhlí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
uhlí	uhlí	k1gNnSc6	uhlí
se	se	k3xPyFc4	se
vlije	vlít	k5eAaPmIp3nS	vlít
v	v	k7c6	v
šachtové	šachtový	k2eAgFnSc6d1	šachtová
peci	pec	k1gFnSc6	pec
získaný	získaný	k2eAgInSc1d1	získaný
cín	cín	k1gInSc1	cín
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
stéká	stékat	k5eAaImIp3nS	stékat
po	po	k7c6	po
žhavém	žhavý	k2eAgNnSc6d1	žhavé
uhlí	uhlí	k1gNnSc6	uhlí
a	a	k8xC	a
zanechává	zanechávat	k5eAaImIp3nS	zanechávat
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
nečistoty	nečistota	k1gFnPc1	nečistota
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnSc1	práce
s	s	k7c7	s
naléváním	nalévání	k1gNnSc7	nalévání
cínu	cín	k1gInSc2	cín
na	na	k7c4	na
uhlí	uhlí	k1gNnSc4	uhlí
se	se	k3xPyFc4	se
opakuje	opakovat	k5eAaImIp3nS	opakovat
tolikrát	tolikrát	k6eAd1	tolikrát
<g/>
,	,	kIx,	,
až	až	k8xS	až
cín	cín	k1gInSc1	cín
na	na	k7c4	na
uhlí	uhlí	k1gNnSc4	uhlí
nezanechává	zanechávat	k5eNaImIp3nS	zanechávat
zbytky	zbytek	k1gInPc7	zbytek
při	při	k7c6	při
odtékání	odtékání	k1gNnSc6	odtékání
<g/>
.	.	kIx.	.
</s>
<s>
Vyčištěný	vyčištěný	k2eAgInSc1d1	vyčištěný
cín	cín	k1gInSc1	cín
se	se	k3xPyFc4	se
lije	lít	k5eAaImIp3nS	lít
do	do	k7c2	do
kadlubů	kadlub	k1gInPc2	kadlub
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
cínu	cín	k1gInSc3	cín
značný	značný	k2eAgInSc1d1	značný
nedostatek	nedostatek	k1gInSc1	nedostatek
<g/>
,	,	kIx,	,
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
cín	cín	k1gInSc1	cín
i	i	k9	i
ze	z	k7c2	z
zbytků	zbytek	k1gInPc2	zbytek
a	a	k8xC	a
odřezků	odřezek	k1gInPc2	odřezek
bílého	bílý	k2eAgInSc2d1	bílý
plechu	plech	k1gInSc2	plech
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
se	se	k3xPyFc4	se
použije	použít	k5eAaPmIp3nS	použít
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Elektrolytem	elektrolyt	k1gInSc7	elektrolyt
je	být	k5eAaImIp3nS	být
roztok	roztok	k1gInSc1	roztok
hydroxidu	hydroxid	k1gInSc2	hydroxid
sodného	sodný	k2eAgMnSc2d1	sodný
NaOH	NaOH	k1gMnSc2	NaOH
<g/>
,	,	kIx,	,
anodu	anoda	k1gFnSc4	anoda
tvoří	tvořit	k5eAaImIp3nP	tvořit
drátěný	drátěný	k2eAgInSc4d1	drátěný
košík	košík	k1gInSc4	košík
naplněný	naplněný	k2eAgInSc4d1	naplněný
odpadky	odpadek	k1gInPc7	odpadek
bílého	bílý	k2eAgInSc2d1	bílý
plechu	plech	k1gInSc2	plech
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
železné	železný	k2eAgFnSc6d1	železná
katodě	katoda	k1gFnSc6	katoda
se	se	k3xPyFc4	se
usazuje	usazovat	k5eAaImIp3nS	usazovat
houbovitý	houbovitý	k2eAgInSc1d1	houbovitý
cín	cín	k1gInSc1	cín
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
vybírá	vybírat	k5eAaImIp3nS	vybírat
a	a	k8xC	a
taví	tavit	k5eAaImIp3nS	tavit
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
velmi	velmi	k6eAd1	velmi
čistého	čistý	k2eAgInSc2d1	čistý
kovu	kov	k1gInSc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Železné	železný	k2eAgInPc1d1	železný
odpadky	odpadek	k1gInPc1	odpadek
po	po	k7c6	po
odstranění	odstranění	k1gNnSc6	odstranění
cínu	cín	k1gInSc2	cín
se	se	k3xPyFc4	se
použijí	použít	k5eAaPmIp3nP	použít
jako	jako	k9	jako
šrot	šrot	k1gInSc4	šrot
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
oceli	ocel	k1gFnSc2	ocel
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
zpracováním	zpracování	k1gNnSc7	zpracování
cínu	cín	k1gInSc2	cín
litím	lití	k1gNnSc7	lití
do	do	k7c2	do
různých	různý	k2eAgFnPc2d1	různá
forem	forma	k1gFnPc2	forma
se	se	k3xPyFc4	se
setkáváme	setkávat	k5eAaImIp1nP	setkávat
již	již	k6eAd1	již
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
3	[number]	k4	3
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
I	i	k9	i
v	v	k7c6	v
antice	antika	k1gFnSc6	antika
byla	být	k5eAaImAgFnS	být
výroba	výroba	k1gFnSc1	výroba
cínových	cínový	k2eAgInPc2d1	cínový
předmětů	předmět	k1gInPc2	předmět
vysoce	vysoce	k6eAd1	vysoce
ceněna	ceněn	k2eAgFnSc1d1	ceněna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
cínařství	cínařství	k1gNnSc1	cínařství
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
vrcholu	vrchol	k1gInSc3	vrchol
v	v	k7c6	v
evropském	evropský	k2eAgInSc6d1	evropský
středověku	středověk	k1gInSc6	středověk
<g/>
,	,	kIx,	,
renesanci	renesance	k1gFnSc6	renesance
a	a	k8xC	a
baroku	baroko	k1gNnSc6	baroko
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgInPc7d1	hlavní
výrobky	výrobek	k1gInPc7	výrobek
z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
jsou	být	k5eAaImIp3nP	být
užitkové	užitkový	k2eAgFnPc1d1	užitková
a	a	k8xC	a
liturgické	liturgický	k2eAgFnPc1d1	liturgická
nádoby	nádoba	k1gFnPc1	nádoba
<g/>
,	,	kIx,	,
svícny	svícen	k1gInPc1	svícen
<g/>
,	,	kIx,	,
křtitelnice	křtitelnice	k1gFnPc1	křtitelnice
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
těchto	tento	k3xDgInPc2	tento
výrobků	výrobek	k1gInPc2	výrobek
byl	být	k5eAaImAgInS	být
zdoben	zdoben	k2eAgInSc1d1	zdoben
reliéfy	reliéf	k1gInPc7	reliéf
<g/>
,	,	kIx,	,
rytím	rytí	k1gNnSc7	rytí
<g/>
,	,	kIx,	,
cizelováním	cizelování	k1gNnSc7	cizelování
a	a	k8xC	a
leptáním	leptání	k1gNnSc7	leptání
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
objevením	objevení	k1gNnSc7	objevení
porcelánu	porcelán	k1gInSc2	porcelán
byl	být	k5eAaImAgInS	být
cín	cín	k1gInSc4	cín
důležitým	důležitý	k2eAgInSc7d1	důležitý
materiálem	materiál	k1gInSc7	materiál
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
talířů	talíř	k1gInPc2	talíř
<g/>
,	,	kIx,	,
konví	konev	k1gFnPc2	konev
a	a	k8xC	a
číší	číš	k1gFnPc2	číš
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byly	být	k5eAaImAgFnP	být
z	z	k7c2	z
cínu	cín	k1gInSc2	cín
odlévány	odléván	k2eAgInPc4d1	odléván
i	i	k8xC	i
drobné	drobný	k2eAgFnPc4d1	drobná
hračky	hračka	k1gFnPc4	hračka
(	(	kIx(	(
<g/>
cínoví	cínový	k2eAgMnPc1d1	cínový
vojáčci	vojáček	k1gMnPc1	vojáček
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sošky	soška	k1gFnSc2	soška
<g/>
,	,	kIx,	,
pamětní	pamětní	k2eAgFnSc2d1	pamětní
medaile	medaile	k1gFnSc2	medaile
apod.	apod.	kA	apod.
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
těžiště	těžiště	k1gNnSc1	těžiště
využití	využití	k1gNnSc2	využití
kovového	kovový	k2eAgInSc2d1	kovový
cínu	cín	k1gInSc2	cín
v	v	k7c6	v
potravinářství	potravinářství	k1gNnSc6	potravinářství
<g/>
.	.	kIx.	.
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
odolnost	odolnost	k1gFnSc1	odolnost
cínu	cín	k1gInSc2	cín
proti	proti	k7c3	proti
korozi	koroze	k1gFnSc3	koroze
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc7	jeho
zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
nezávadnost	nezávadnost	k1gFnSc1	nezávadnost
ho	on	k3xPp3gNnSc4	on
určují	určovat	k5eAaImIp3nP	určovat
jako	jako	k9	jako
ideální	ideální	k2eAgInSc4d1	ideální
materiál	materiál	k1gInSc4	materiál
pro	pro	k7c4	pro
styk	styk	k1gInSc4	styk
s	s	k7c7	s
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
uchovávanými	uchovávaný	k2eAgFnPc7d1	uchovávaná
potravinami	potravina	k1gFnPc7	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
cena	cena	k1gFnSc1	cena
samotného	samotný	k2eAgInSc2d1	samotný
cínu	cín	k1gInSc2	cín
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
plech	plech	k1gInSc1	plech
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
konzerv	konzerva	k1gFnPc2	konzerva
obvykle	obvykle	k6eAd1	obvykle
ze	z	k7c2	z
slitin	slitina	k1gFnPc2	slitina
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
cínem	cín	k1gInSc7	cín
v	v	k7c6	v
tenké	tenký	k2eAgFnSc6d1	tenká
vrstvě	vrstva	k1gFnSc6	vrstva
je	být	k5eAaImIp3nS	být
pokrýván	pokrýván	k2eAgInSc4d1	pokrýván
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
povrch	povrch	k1gInSc4	povrch
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
kontaktu	kontakt	k1gInSc6	kontakt
s	s	k7c7	s
potravinami	potravina	k1gFnPc7	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
se	se	k3xPyFc4	se
upravují	upravovat	k5eAaImIp3nP	upravovat
i	i	k9	i
kovové	kovový	k2eAgInPc1d1	kovový
povrchy	povrch	k1gInPc1	povrch
zařízení	zařízení	k1gNnSc2	zařízení
pro	pro	k7c4	pro
potravinářský	potravinářský	k2eAgInSc4d1	potravinářský
průmysl	průmysl	k1gInSc4	průmysl
-	-	kIx~	-
trubky	trubka	k1gFnPc1	trubka
<g/>
,	,	kIx,	,
kotle	kotel	k1gInPc1	kotel
<g/>
,	,	kIx,	,
reaktory	reaktor	k1gInPc1	reaktor
<g/>
...	...	k?	...
Z	z	k7c2	z
cínu	cín	k1gInSc2	cín
lze	lze	k6eAd1	lze
také	také	k9	také
vyválcovat	vyválcovat	k5eAaPmF	vyválcovat
tenké	tenký	k2eAgFnPc4d1	tenká
fólie	fólie	k1gFnPc4	fólie
(	(	kIx(	(
<g/>
staniol	staniol	k1gInSc1	staniol
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
při	při	k7c6	při
ochraně	ochrana	k1gFnSc6	ochrana
potravin	potravina	k1gFnPc2	potravina
nebo	nebo	k8xC	nebo
jiných	jiný	k2eAgInPc2d1	jiný
předmětů	předmět	k1gInPc2	předmět
před	před	k7c7	před
korozí	koroze	k1gFnSc7	koroze
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
ale	ale	k8xC	ale
vytlačovány	vytlačován	k2eAgInPc1d1	vytlačován
hliníkovou	hliníkový	k2eAgFnSc7d1	hliníková
fólií	fólie	k1gFnSc7	fólie
-	-	kIx~	-
alobalem	alobal	k1gInSc7	alobal
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
znatelně	znatelně	k6eAd1	znatelně
levnější	levný	k2eAgMnSc1d2	levnější
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
stejné	stejný	k2eAgFnPc4d1	stejná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sklářském	sklářský	k2eAgInSc6d1	sklářský
průmyslu	průmysl	k1gInSc6	průmysl
je	být	k5eAaImIp3nS	být
lití	lití	k1gNnSc1	lití
skleněných	skleněný	k2eAgFnPc2d1	skleněná
tabulí	tabule	k1gFnPc2	tabule
na	na	k7c4	na
roztavený	roztavený	k2eAgInSc4d1	roztavený
cín	cín	k1gInSc4	cín
ideální	ideální	k2eAgFnSc7d1	ideální
metodou	metoda	k1gFnSc7	metoda
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
dokonale	dokonale	k6eAd1	dokonale
rovných	rovný	k2eAgFnPc2d1	rovná
skleněných	skleněný	k2eAgFnPc2d1	skleněná
ploch	plocha	k1gFnPc2	plocha
o	o	k7c6	o
značně	značně	k6eAd1	značně
velkých	velký	k2eAgInPc6d1	velký
rozměrech	rozměr	k1gInPc6	rozměr
(	(	kIx(	(
<g/>
výkladní	výkladní	k2eAgFnPc4d1	výkladní
skříně	skříň	k1gFnPc4	skříň
<g/>
,	,	kIx,	,
okna	okno	k1gNnPc1	okno
moderních	moderní	k2eAgFnPc2d1	moderní
výškových	výškový	k2eAgFnPc2d1	výšková
budov	budova	k1gFnPc2	budova
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Elementární	elementární	k2eAgInSc1d1	elementární
cín	cín	k1gInSc1	cín
získává	získávat	k5eAaImIp3nS	získávat
supravodivé	supravodivý	k2eAgFnPc4d1	supravodivá
vlastnosti	vlastnost	k1gFnPc4	vlastnost
při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
pod	pod	k7c4	pod
3,72	[number]	k4	3,72
K.	K.	kA	K.
Dosažení	dosažení	k1gNnSc4	dosažení
této	tento	k3xDgFnSc2	tento
teploty	teplota	k1gFnSc2	teplota
není	být	k5eNaImIp3nS	být
technicky	technicky	k6eAd1	technicky
příliš	příliš	k6eAd1	příliš
obtížné	obtížný	k2eAgNnSc1d1	obtížné
a	a	k8xC	a
proto	proto	k8xC	proto
cínové	cínový	k2eAgInPc1d1	cínový
krystaly	krystal	k1gInPc1	krystal
sloužily	sloužit	k5eAaImAgInP	sloužit
jako	jako	k9	jako
první	první	k4xOgInSc4	první
materiál	materiál	k1gInSc4	materiál
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
jevů	jev	k1gInPc2	jev
supravodivosti	supravodivost	k1gFnSc2	supravodivost
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Meissnerův-Ochsenfeldův	Meissnerův-Ochsenfeldův	k2eAgInSc1d1	Meissnerův-Ochsenfeldův
jev	jev	k1gInSc1	jev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
čistého	čistý	k2eAgInSc2d1	čistý
cínu	cín	k1gInSc2	cín
je	být	k5eAaImIp3nS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
měkký	měkký	k2eAgInSc1d1	měkký
a	a	k8xC	a
nehodí	hodit	k5eNaPmIp3nS	hodit
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
nástrojů	nástroj	k1gInPc2	nástroj
nebo	nebo	k8xC	nebo
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
objev	objev	k1gInSc1	objev
slitin	slitina	k1gFnPc2	slitina
cínu	cín	k1gInSc2	cín
s	s	k7c7	s
mědí	měď	k1gFnSc7	měď
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
kovy	kov	k1gInPc7	kov
umožnil	umožnit	k5eAaPmAgInS	umožnit
rozkvět	rozkvět	k1gInSc1	rozkvět
starověké	starověký	k2eAgFnSc2d1	starověká
metalurgie	metalurgie	k1gFnSc2	metalurgie
a	a	k8xC	a
přispěl	přispět	k5eAaPmAgInS	přispět
k	k	k7c3	k
rychlejšímu	rychlý	k2eAgInSc3d2	rychlejší
vývoji	vývoj	k1gInSc3	vývoj
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Cínový	cínový	k2eAgInSc4d1	cínový
bronz	bronz	k1gInSc4	bronz
je	být	k5eAaImIp3nS	být
slitina	slitina	k1gFnSc1	slitina
mědi	měď	k1gFnSc2	měď
a	a	k8xC	a
cínu	cín	k1gInSc2	cín
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
cínu	cín	k1gInSc2	cín
do	do	k7c2	do
33	[number]	k4	33
%	%	kIx~	%
<g/>
,	,	kIx,	,
běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
obsah	obsah	k1gInSc1	obsah
cínu	cín	k1gInSc2	cín
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
pouze	pouze	k6eAd1	pouze
okolo	okolo	k7c2	okolo
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Cín	cín	k1gInSc4	cín
podstatným	podstatný	k2eAgInSc7d1	podstatný
způsobem	způsob	k1gInSc7	způsob
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
pevnost	pevnost	k1gFnSc1	pevnost
a	a	k8xC	a
tvrdost	tvrdost	k1gFnSc1	tvrdost
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
má	mít	k5eAaImIp3nS	mít
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
slitina	slitina	k1gFnSc1	slitina
nižší	nízký	k2eAgFnSc1d2	nižší
bod	bod	k1gInSc4	bod
tání	tání	k1gNnSc1	tání
než	než	k8xS	než
samotná	samotný	k2eAgFnSc1d1	samotná
měď	měď	k1gFnSc1	měď
a	a	k8xC	a
snáze	snadno	k6eAd2	snadno
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
litím	lití	k1gNnSc7	lití
<g/>
.	.	kIx.	.
</s>
<s>
Použije	použít	k5eAaPmIp3nS	použít
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
pojem	pojem	k1gInSc1	pojem
bronz	bronz	k1gInSc1	bronz
sám	sám	k3xTgInSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tím	ten	k3xDgNnSc7	ten
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
kontextu	kontext	k1gInSc6	kontext
míněn	míněn	k2eAgInSc4d1	míněn
právě	právě	k6eAd1	právě
cínový	cínový	k2eAgInSc4d1	cínový
bronz	bronz	k1gInSc4	bronz
<g/>
.	.	kIx.	.
</s>
<s>
Bronz	bronz	k1gInSc1	bronz
jako	jako	k8xC	jako
slitina	slitina	k1gFnSc1	slitina
cínu	cín	k1gInSc2	cín
s	s	k7c7	s
mědí	měď	k1gFnSc7	měď
dala	dát	k5eAaPmAgFnS	dát
jménu	jméno	k1gNnSc3	jméno
celé	celý	k2eAgFnSc2d1	celá
epoše	epocha	k1gFnSc6	epocha
lidských	lidský	k2eAgFnPc2d1	lidská
dějin	dějiny	k1gFnPc2	dějiny
-	-	kIx~	-
doba	doba	k1gFnSc1	doba
bronzová	bronzový	k2eAgFnSc1d1	bronzová
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
významu	význam	k1gInSc6	význam
<g/>
,	,	kIx,	,
jaký	jaký	k3yQgMnSc1	jaký
je	být	k5eAaImIp3nS	být
člověkem	člověk	k1gMnSc7	člověk
bronzu	bronz	k1gInSc2	bronz
připisován	připisován	k2eAgMnSc1d1	připisován
svědčí	svědčit	k5eAaImIp3nS	svědčit
např.	např.	kA	např.
i	i	k8xC	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
nějaké	nějaký	k3yIgFnSc6	nějaký
sportovní	sportovní	k2eAgFnSc6d1	sportovní
disciplíně	disciplína	k1gFnSc6	disciplína
se	se	k3xPyFc4	se
uděluje	udělovat	k5eAaImIp3nS	udělovat
bronzová	bronzový	k2eAgFnSc1d1	bronzová
medaile	medaile	k1gFnSc1	medaile
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
po	po	k7c4	po
zlaté	zlatý	k2eAgFnPc4d1	zlatá
a	a	k8xC	a
stříbrné	stříbrný	k2eAgFnPc4d1	stříbrná
<g/>
.	.	kIx.	.
</s>
<s>
Bronzy	bronz	k1gInPc1	bronz
mají	mít	k5eAaImIp3nP	mít
mnoho	mnoho	k4c4	mnoho
využití	využití	k1gNnPc2	využití
i	i	k9	i
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Bronz	bronz	k1gInSc1	bronz
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
velkou	velký	k2eAgFnSc4d1	velká
odolnost	odolnost	k1gFnSc4	odolnost
při	při	k7c6	při
styku	styk	k1gInSc6	styk
s	s	k7c7	s
mořskou	mořský	k2eAgFnSc7d1	mořská
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
důležité	důležitý	k2eAgFnPc1d1	důležitá
součásti	součást	k1gFnPc1	součást
průmyslových	průmyslový	k2eAgFnPc2d1	průmyslová
aparatur	aparatura	k1gFnPc2	aparatura
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
trvale	trvale	k6eAd1	trvale
vystaveny	vystavit	k5eAaPmNgInP	vystavit
jejímu	její	k3xOp3gNnSc3	její
působení	působení	k1gNnSc4	působení
(	(	kIx(	(
<g/>
potrubí	potrubí	k1gNnSc4	potrubí
a	a	k8xC	a
ventily	ventil	k1gInPc4	ventil
pro	pro	k7c4	pro
její	její	k3xOp3gInSc4	její
rozvod	rozvod	k1gInSc4	rozvod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bronz	bronz	k1gInSc1	bronz
(	(	kIx(	(
<g/>
zvonovina	zvonovina	k1gFnSc1	zvonovina
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
pokládán	pokládat	k5eAaImNgInS	pokládat
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
slitinu	slitina	k1gFnSc4	slitina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
zvonů	zvon	k1gInPc2	zvon
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
odlévány	odléván	k2eAgFnPc1d1	odlévána
různé	různý	k2eAgFnPc1d1	různá
sochy	socha	k1gFnPc1	socha
a	a	k8xC	a
kovové	kovový	k2eAgFnPc1d1	kovová
plastiky	plastika	k1gFnPc1	plastika
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
prakticky	prakticky	k6eAd1	prakticky
neomezeně	omezeně	k6eNd1	omezeně
vzdoruje	vzdorovat	k5eAaImIp3nS	vzdorovat
vlivům	vliv	k1gInPc3	vliv
počasí	počasí	k1gNnSc2	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
významné	významný	k2eAgInPc1d1	významný
a	a	k8xC	a
ceněné	ceněný	k2eAgInPc1d1	ceněný
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
čínské	čínský	k2eAgInPc4d1	čínský
bronzové	bronzový	k2eAgInPc4d1	bronzový
umělecké	umělecký	k2eAgInPc4d1	umělecký
předměty	předmět	k1gInPc4	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Fosforové	fosforový	k2eAgInPc1d1	fosforový
bronzy	bronz	k1gInPc1	bronz
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
navíc	navíc	k6eAd1	navíc
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
%	%	kIx~	%
fosforu	fosfor	k1gInSc2	fosfor
a	a	k8xC	a
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
ještě	ještě	k9	ještě
zvýšenou	zvýšený	k2eAgFnSc7d1	zvýšená
tvrdostí	tvrdost	k1gFnSc7	tvrdost
<g/>
,	,	kIx,	,
mechanickou	mechanický	k2eAgFnSc7d1	mechanická
a	a	k8xC	a
chemickou	chemický	k2eAgFnSc7d1	chemická
odolností	odolnost	k1gFnSc7	odolnost
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšením	zvýšení	k1gNnSc7	zvýšení
obsahu	obsah	k1gInSc2	obsah
cínu	cín	k1gInSc2	cín
v	v	k7c6	v
bronzu	bronz	k1gInSc6	bronz
se	se	k3xPyFc4	se
získá	získat	k5eAaPmIp3nS	získat
jeho	jeho	k3xOp3gFnSc1	jeho
speciální	speciální	k2eAgFnSc1d1	speciální
odrůda	odrůda	k1gFnSc1	odrůda
<g/>
,	,	kIx,	,
slitina	slitina	k1gFnSc1	slitina
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
dělovina	dělovina	k1gFnSc1	dělovina
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
slitina	slitina	k1gFnSc1	slitina
sloužila	sloužit	k5eAaImAgFnS	sloužit
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
novověku	novověk	k1gInSc6	novověk
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
těžkých	těžký	k2eAgFnPc2d1	těžká
palných	palný	k2eAgFnPc2d1	palná
zbraní	zbraň	k1gFnPc2	zbraň
a	a	k8xC	a
nebylo	být	k5eNaImAgNnS	být
proto	proto	k8xC	proto
výjimkou	výjimka	k1gFnSc7	výjimka
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
kostelní	kostelní	k2eAgInPc1d1	kostelní
zvony	zvon	k1gInPc1	zvon
přetavovaly	přetavovat	k5eAaImAgInP	přetavovat
na	na	k7c4	na
děla	dělo	k1gNnPc4	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
typem	typ	k1gInSc7	typ
slitin	slitina	k1gFnPc2	slitina
obsahujících	obsahující	k2eAgFnPc2d1	obsahující
měď	měď	k1gFnSc4	měď
je	být	k5eAaImIp3nS	být
ložiskový	ložiskový	k2eAgInSc1d1	ložiskový
kov	kov	k1gInSc1	kov
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
slitinu	slitina	k1gFnSc4	slitina
s	s	k7c7	s
přibližným	přibližný	k2eAgNnSc7d1	přibližné
složením	složení	k1gNnSc7	složení
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
90	[number]	k4	90
%	%	kIx~	%
cínu	cín	k1gInSc2	cín
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
navíc	navíc	k6eAd1	navíc
měď	měď	k1gFnSc1	měď
<g/>
,	,	kIx,	,
olovo	olovo	k1gNnSc1	olovo
a	a	k8xC	a
antimon	antimon	k1gInSc1	antimon
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	se	k3xPyFc4	se
především	především	k9	především
vysokou	vysoký	k2eAgFnSc7d1	vysoká
odolností	odolnost	k1gFnSc7	odolnost
proti	proti	k7c3	proti
otěru	otěr	k1gInSc3	otěr
i	i	k9	i
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
měkké	měkký	k2eAgFnPc1d1	měkká
-	-	kIx~	-
slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
kluzných	kluzný	k2eAgNnPc2d1	kluzné
ložisek	ložisko	k1gNnPc2	ložisko
pro	pro	k7c4	pro
automobilový	automobilový	k2eAgInSc4d1	automobilový
průmysl	průmysl	k1gInSc4	průmysl
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
aplikace	aplikace	k1gFnPc4	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Anglický	anglický	k2eAgInSc1d1	anglický
cín	cín	k1gInSc1	cín
(	(	kIx(	(
<g/>
pewter	pewter	k1gInSc1	pewter
<g/>
)	)	kIx)	)
-	-	kIx~	-
slitina	slitina	k1gFnSc1	slitina
s	s	k7c7	s
mědí	měď	k1gFnSc7	měď
a	a	k8xC	a
antimonem	antimon	k1gInSc7	antimon
nebo	nebo	k8xC	nebo
olovem	olovo	k1gNnSc7	olovo
<g/>
,	,	kIx,	,
obsah	obsah	k1gInSc1	obsah
cínu	cín	k1gInSc2	cín
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
90	[number]	k4	90
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Užívá	užívat	k5eAaImIp3nS	užívat
se	se	k3xPyFc4	se
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
levnějších	levný	k2eAgInPc2d2	levnější
šperků	šperk	k1gInPc2	šperk
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc4d1	významná
skupinu	skupina	k1gFnSc4	skupina
slitin	slitina	k1gFnPc2	slitina
cínu	cín	k1gInSc2	cín
představují	představovat	k5eAaImIp3nP	představovat
pájky	pájka	k1gFnPc1	pájka
<g/>
.	.	kIx.	.
</s>
<s>
Cín	cín	k1gInSc1	cín
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
především	především	k6eAd1	především
pájek	pájka	k1gFnPc2	pájka
pro	pro	k7c4	pro
tzv.	tzv.	kA	tzv.
měkké	měkký	k2eAgNnSc1d1	měkké
pájení	pájení	k1gNnSc1	pájení
<g/>
.	.	kIx.	.
</s>
<s>
Měkké	měkký	k2eAgFnPc1d1	měkká
pájky	pájka	k1gFnPc1	pájka
(	(	kIx(	(
<g/>
s	s	k7c7	s
teplotou	teplota	k1gFnSc7	teplota
tání	tání	k1gNnSc2	tání
nižší	nízký	k2eAgFnSc2d2	nižší
než	než	k8xS	než
400	[number]	k4	400
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
slitiny	slitina	k1gFnPc1	slitina
kovů	kov	k1gInPc2	kov
jako	jako	k8xS	jako
Pb	Pb	k1gFnPc2	Pb
<g/>
,	,	kIx,	,
Sn	Sn	k1gFnPc2	Sn
<g/>
,	,	kIx,	,
Cd	cd	kA	cd
<g/>
,	,	kIx,	,
Zn	zn	kA	zn
<g/>
,	,	kIx,	,
Ag	Ag	k1gMnSc1	Ag
aj.	aj.	kA	aj.
Tyto	tento	k3xDgFnPc1	tento
pájky	pájka	k1gFnPc1	pájka
jsou	být	k5eAaImIp3nP	být
využívány	využívat	k5eAaPmNgFnP	využívat
pro	pro	k7c4	pro
instalatérské	instalatérský	k2eAgFnPc4d1	instalatérská
práce	práce	k1gFnPc4	práce
(	(	kIx(	(
<g/>
pájení	pájení	k1gNnSc1	pájení
trubek	trubka	k1gFnPc2	trubka
pro	pro	k7c4	pro
rozvody	rozvod	k1gInPc4	rozvod
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
topných	topný	k2eAgNnPc2d1	topné
médií	médium	k1gNnPc2	médium
<g/>
,	,	kIx,	,
chladicích	chladicí	k2eAgNnPc2d1	chladicí
médií	médium	k1gNnPc2	médium
v	v	k7c6	v
klimatizacích	klimatizace	k1gFnPc6	klimatizace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
klempířské	klempířský	k2eAgFnPc4d1	klempířská
práce	práce	k1gFnPc4	práce
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
spojování	spojování	k1gNnSc1	spojování
okapů	okap	k1gInPc2	okap
<g/>
,	,	kIx,	,
střešních	střešní	k2eAgFnPc2d1	střešní
krytin	krytina	k1gFnPc2	krytina
a	a	k8xC	a
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
měřítku	měřítko	k1gNnSc6	měřítko
v	v	k7c6	v
elektrotechnice	elektrotechnika	k1gFnSc6	elektrotechnika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
měkké	měkký	k2eAgFnPc4d1	měkká
pájky	pájka	k1gFnPc4	pájka
norma	norma	k1gFnSc1	norma
<g/>
:	:	kIx,	:
ČSN	ČSN	kA	ČSN
EN	EN	kA	EN
ISO	ISO	kA	ISO
9453	[number]	k4	9453
Slitiny	slitina	k1gFnSc2	slitina
pro	pro	k7c4	pro
měkké	měkký	k2eAgNnSc4d1	měkké
pájení	pájení	k1gNnSc4	pájení
-	-	kIx~	-
Chemické	chemický	k2eAgNnSc4d1	chemické
složení	složení	k1gNnSc4	složení
a	a	k8xC	a
tvary	tvar	k1gInPc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
některé	některý	k3yIgFnPc4	některý
aplikace	aplikace	k1gFnPc4	aplikace
se	se	k3xPyFc4	se
odlévají	odlévat	k5eAaImIp3nP	odlévat
komplikovanější	komplikovaný	k2eAgFnPc1d2	komplikovanější
směsi	směs	k1gFnPc1	směs
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
slitiny	slitina	k1gFnPc4	slitina
cínu	cín	k1gInSc2	cín
<g/>
,	,	kIx,	,
olova	olovo	k1gNnSc2	olovo
<g/>
,	,	kIx,	,
kadmia	kadmium	k1gNnSc2	kadmium
a	a	k8xC	a
antimonu	antimon	k1gInSc2	antimon
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
bodu	bod	k1gInSc2	bod
tání	tání	k1gNnSc2	tání
<g/>
,	,	kIx,	,
zvýšení	zvýšení	k1gNnSc2	zvýšení
pevnosti	pevnost	k1gFnSc2	pevnost
a	a	k8xC	a
vodivosti	vodivost	k1gFnSc2	vodivost
spoje	spoj	k1gInSc2	spoj
se	se	k3xPyFc4	se
často	často	k6eAd1	často
leguje	legovat	k5eAaImIp3nS	legovat
do	do	k7c2	do
slitiny	slitina	k1gFnSc2	slitina
i	i	k9	i
stříbro	stříbro	k1gNnSc4	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
<g/>
,	,	kIx,	,
vyžadující	vyžadující	k2eAgFnSc4d1	vyžadující
zvlášť	zvlášť	k6eAd1	zvlášť
velkou	velký	k2eAgFnSc4d1	velká
tvrdost	tvrdost	k1gFnSc4	tvrdost
spoje	spoj	k1gInSc2	spoj
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
přidává	přidávat	k5eAaImIp3nS	přidávat
i	i	k9	i
fosfor	fosfor	k1gInSc4	fosfor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
však	však	k9	však
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
křehkost	křehkost	k1gFnSc4	křehkost
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
pájení	pájení	k1gNnSc4	pájení
v	v	k7c6	v
elektrotechnice	elektrotechnika	k1gFnSc6	elektrotechnika
a	a	k8xC	a
elektronice	elektronika	k1gFnSc6	elektronika
se	se	k3xPyFc4	se
dlouho	dlouho	k6eAd1	dlouho
používala	používat	k5eAaImAgFnS	používat
eutektická	eutektický	k2eAgFnSc1d1	eutektická
slitina	slitina	k1gFnSc1	slitina
s	s	k7c7	s
37	[number]	k4	37
%	%	kIx~	%
olova	olovo	k1gNnSc2	olovo
a	a	k8xC	a
63	[number]	k4	63
%	%	kIx~	%
cínu	cín	k1gInSc2	cín
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
teplota	teplota	k1gFnSc1	teplota
tání	tání	k1gNnSc2	tání
je	být	k5eAaImIp3nS	být
183	[number]	k4	183
°	°	k?	°
<g/>
C.	C.	kA	C.
Výhodou	výhoda	k1gFnSc7	výhoda
eutektické	eutektický	k2eAgFnSc2d1	eutektická
slitiny	slitina	k1gFnSc2	slitina
je	být	k5eAaImIp3nS	být
hlavně	hlavně	k9	hlavně
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tuhne	tuhnout	k5eAaImIp3nS	tuhnout
bez	bez	k7c2	bez
přechodových	přechodový	k2eAgFnPc2d1	přechodová
fází	fáze	k1gFnPc2	fáze
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
dříve	dříve	k6eAd2	dříve
používané	používaný	k2eAgFnPc1d1	používaná
slitiny	slitina	k1gFnPc1	slitina
s	s	k7c7	s
olovem	olovo	k1gNnSc7	olovo
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
směrnice	směrnice	k1gFnSc2	směrnice
RoHS	RoHS	k1gFnSc2	RoHS
od	od	k7c2	od
r.	r.	kA	r.
2006	[number]	k4	2006
zakázány	zakázán	k2eAgInPc1d1	zakázán
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
proto	proto	k8xC	proto
nahrazeny	nahradit	k5eAaPmNgFnP	nahradit
slitinami	slitina	k1gFnPc7	slitina
Sn	Sn	k1gFnSc7	Sn
s	s	k7c7	s
Cd	cd	kA	cd
<g/>
,	,	kIx,	,
Zn	zn	kA	zn
<g/>
,	,	kIx,	,
Ni	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
Bi	Bi	k1gMnSc4	Bi
<g/>
,	,	kIx,	,
In	In	k1gMnSc4	In
<g/>
,	,	kIx,	,
aj.	aj.	kA	aj.
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pájka	pájka	k1gFnSc1	pájka
220	[number]	k4	220
<g/>
,	,	kIx,	,
s	s	k7c7	s
Cd	cd	kA	cd
<g/>
82	[number]	k4	82
<g/>
Zn	zn	kA	zn
<g/>
16	[number]	k4	16
<g/>
Ag	Ag	k1gFnSc1	Ag
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnPc1	jejich
vlastnosti	vlastnost	k1gFnPc1	vlastnost
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
olovnatými	olovnatý	k2eAgMnPc7d1	olovnatý
méně	málo	k6eAd2	málo
výhodné	výhodný	k2eAgNnSc1d1	výhodné
<g/>
,	,	kIx,	,
např.	např.	kA	např.
mají	mít	k5eAaImIp3nP	mít
horší	zlý	k2eAgFnSc4d2	horší
smáčivost	smáčivost	k1gFnSc4	smáčivost
a	a	k8xC	a
vyšší	vysoký	k2eAgFnSc4d2	vyšší
teplotu	teplota	k1gFnSc4	teplota
tavení	tavení	k1gNnSc2	tavení
<g/>
.	.	kIx.	.
</s>
<s>
Pájky	pájka	k1gFnPc1	pájka
pro	pro	k7c4	pro
elektrotechniku	elektrotechnika	k1gFnSc4	elektrotechnika
jsou	být	k5eAaImIp3nP	být
dodávány	dodávat	k5eAaImNgFnP	dodávat
v	v	k7c6	v
nejrůznějších	různý	k2eAgFnPc6d3	nejrůznější
formách	forma	k1gFnPc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ruční	ruční	k2eAgNnSc4d1	ruční
pájení	pájení	k1gNnSc4	pájení
v	v	k7c6	v
elektronice	elektronika	k1gFnSc6	elektronika
se	se	k3xPyFc4	se
dodávají	dodávat	k5eAaImIp3nP	dodávat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
"	"	kIx"	"
<g/>
dutého	dutý	k2eAgMnSc2d1	dutý
<g/>
"	"	kIx"	"
drátu	drát	k1gInSc2	drát
(	(	kIx(	(
<g/>
trubičky	trubička	k1gFnSc2	trubička
<g/>
)	)	kIx)	)
vyplněné	vyplněný	k2eAgFnSc2d1	vyplněná
pastovitým	pastovitý	k2eAgNnSc7d1	pastovité
tavidlem	tavidlo	k1gNnSc7	tavidlo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
pájení	pájení	k1gNnSc3	pájení
SMD	SMD	kA	SMD
součástek	součástka	k1gFnPc2	součástka
(	(	kIx(	(
<g/>
Surface	Surface	k1gFnSc1	Surface
Mounting	Mounting	k1gInSc1	Mounting
Devices	Devices	k1gInSc1	Devices
-	-	kIx~	-
bezvývodové	bezvývodový	k2eAgFnSc2d1	bezvývodový
součástky	součástka	k1gFnSc2	součástka
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
pájka	pájka	k1gFnSc1	pájka
v	v	k7c6	v
pastovité	pastovitý	k2eAgFnSc6d1	pastovitá
formě	forma	k1gFnSc6	forma
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
miniaturní	miniaturní	k2eAgFnPc1d1	miniaturní
kuličky	kulička	k1gFnPc1	kulička
pájky	pájka	k1gFnSc2	pájka
smísené	smísený	k2eAgInPc1d1	smísený
s	s	k7c7	s
pastovitým	pastovitý	k2eAgNnSc7d1	pastovité
tavidlem	tavidlo	k1gNnSc7	tavidlo
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
vsázka	vsázka	k1gFnSc1	vsázka
do	do	k7c2	do
zařízení	zařízení	k1gNnSc2	zařízení
pro	pro	k7c4	pro
hromadné	hromadný	k2eAgNnSc4d1	hromadné
pájení	pájení	k1gNnSc4	pájení
(	(	kIx(	(
<g/>
cínová	cínový	k2eAgFnSc1d1	cínová
vlna	vlna	k1gFnSc1	vlna
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
užívají	užívat	k5eAaImIp3nP	užívat
vakuově	vakuově	k6eAd1	vakuově
odlévané	odlévaný	k2eAgInPc1d1	odlévaný
slitky	slitek	k1gInPc1	slitek
(	(	kIx(	(
<g/>
tyče	tyč	k1gFnPc1	tyč
<g/>
)	)	kIx)	)
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
půl	půl	k1xP	půl
až	až	k8xS	až
několika	několik	k4yIc2	několik
kilogramů	kilogram	k1gInPc2	kilogram
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
využitím	využití	k1gNnSc7	využití
cínu	cín	k1gInSc2	cín
je	být	k5eAaImIp3nS	být
varhanní	varhanní	k2eAgInSc1d1	varhanní
kov	kov	k1gInSc1	kov
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
varhanní	varhanní	k2eAgFnPc1d1	varhanní
píšťaly	píšťala	k1gFnPc1	píšťala
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
slitina	slitina	k1gFnSc1	slitina
cínu	cín	k1gInSc2	cín
a	a	k8xC	a
olova	olovo	k1gNnSc2	olovo
<g/>
,	,	kIx,	,
obsah	obsah	k1gInSc1	obsah
cínu	cín	k1gInSc2	cín
určuje	určovat	k5eAaImIp3nS	určovat
kvalitu	kvalita	k1gFnSc4	kvalita
píšťaly	píšťala	k1gFnSc2	píšťala
a	a	k8xC	a
její	její	k3xOp3gInSc4	její
zvuk	zvuk	k1gInSc4	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yRnSc7	co
více	hodně	k6eAd2	hodně
cínu	cín	k1gInSc2	cín
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
větší	veliký	k2eAgFnSc1d2	veliký
má	mít	k5eAaImIp3nS	mít
pak	pak	k6eAd1	pak
výsledný	výsledný	k2eAgInSc1d1	výsledný
zvuk	zvuk	k1gInSc1	zvuk
lesk	lesk	k1gInSc4	lesk
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
kompromis	kompromis	k1gInSc4	kompromis
mezi	mezi	k7c7	mezi
výrobními	výrobní	k2eAgInPc7d1	výrobní
náklady	náklad	k1gInPc7	náklad
a	a	k8xC	a
požadovaným	požadovaný	k2eAgInSc7d1	požadovaný
zvukem	zvuk	k1gInSc7	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1	poslední
ze	z	k7c2	z
známých	známý	k2eAgFnPc2d1	známá
slitin	slitina	k1gFnPc2	slitina
je	být	k5eAaImIp3nS	být
kov	kov	k1gInSc1	kov
Britania	Britanium	k1gNnSc2	Britanium
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
jídelních	jídelní	k2eAgInPc2d1	jídelní
příborů	příbor	k1gInPc2	příbor
a	a	k8xC	a
nádobí	nádobí	k1gNnSc2	nádobí
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
slitina	slitina	k1gFnSc1	slitina
90	[number]	k4	90
%	%	kIx~	%
cínu	cín	k1gInSc2	cín
<g/>
,	,	kIx,	,
8	[number]	k4	8
%	%	kIx~	%
antimonu	antimon	k1gInSc2	antimon
a	a	k8xC	a
2	[number]	k4	2
%	%	kIx~	%
mědi	měď	k1gFnSc2	měď
<g/>
.	.	kIx.	.
</s>
<s>
Cín	cín	k1gInSc1	cín
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
s	s	k7c7	s
většinou	většina	k1gFnSc7	většina
minerálních	minerální	k2eAgFnPc2d1	minerální
kyselin	kyselina	k1gFnPc2	kyselina
dvě	dva	k4xCgFnPc1	dva
řady	řada	k1gFnPc1	řada
solí	sůl	k1gFnPc2	sůl
s	s	k7c7	s
mocenstvím	mocenství	k1gNnSc7	mocenství
Sn	Sn	k1gFnSc2	Sn
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
a	a	k8xC	a
Sn	Sn	k1gMnSc1	Sn
<g/>
+	+	kIx~	+
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodných	vodný	k2eAgInPc6d1	vodný
roztocích	roztok	k1gInPc6	roztok
jsou	být	k5eAaImIp3nP	být
ionty	ion	k1gInPc1	ion
o	o	k7c6	o
uvedeném	uvedený	k2eAgNnSc6d1	uvedené
mocenství	mocenství	k1gNnSc6	mocenství
stálé	stálý	k2eAgFnSc2d1	stálá
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
velkého	velký	k2eAgInSc2d1	velký
nadbytku	nadbytek	k1gInSc2	nadbytek
kyselin	kyselina	k1gFnPc2	kyselina
(	(	kIx(	(
<g/>
především	především	k9	především
HCl	HCl	k1gFnSc1	HCl
stabilizuje	stabilizovat	k5eAaBmIp3nS	stabilizovat
ion	ion	k1gInSc4	ion
Sn	Sn	k1gFnSc2	Sn
<g/>
+	+	kIx~	+
<g/>
4	[number]	k4	4
tvorbou	tvorba	k1gFnSc7	tvorba
silných	silný	k2eAgInPc2d1	silný
chlorokomplexů	chlorokomplex	k1gInPc2	chlorokomplex
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roztoky	roztoka	k1gFnPc1	roztoka
cínatých	cínatý	k2eAgFnPc2d1	cínatý
solí	sůl	k1gFnPc2	sůl
Sn	Sn	k1gFnSc2	Sn
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
se	se	k3xPyFc4	se
ve	v	k7c6	v
styku	styk	k1gInSc6	styk
se	s	k7c7	s
vzdušným	vzdušný	k2eAgInSc7d1	vzdušný
kyslíkem	kyslík	k1gInSc7	kyslík
pozvolna	pozvolna	k6eAd1	pozvolna
oxidují	oxidovat	k5eAaBmIp3nP	oxidovat
na	na	k7c4	na
soli	sůl	k1gFnPc4	sůl
cíničité	cíničitý	k2eAgFnPc4d1	cíničitý
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc4d1	hlavní
praktické	praktický	k2eAgNnSc4d1	praktické
uplatnění	uplatnění	k1gNnSc4	uplatnění
nalézají	nalézat	k5eAaImIp3nP	nalézat
roztoky	roztok	k1gInPc1	roztok
cínatých	cínatý	k2eAgFnPc2d1	cínatý
solí	sůl	k1gFnPc2	sůl
jako	jako	k9	jako
redukční	redukční	k2eAgNnPc4d1	redukční
činidla	činidlo	k1gNnPc4	činidlo
střední	střední	k2eAgFnSc2d1	střední
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
nasazovány	nasazován	k2eAgInPc4d1	nasazován
jak	jak	k6eAd1	jak
v	v	k7c6	v
organické	organický	k2eAgFnSc6d1	organická
tak	tak	k8xS	tak
anorganické	anorganický	k2eAgFnSc6d1	anorganická
syntéze	syntéza	k1gFnSc6	syntéza
i	i	k8xC	i
v	v	k7c6	v
analytické	analytický	k2eAgFnSc6d1	analytická
chemii	chemie	k1gFnSc6	chemie
v	v	k7c6	v
reduktometrických	reduktometrický	k2eAgFnPc6d1	reduktometrický
titracích	titrace	k1gFnPc6	titrace
nebo	nebo	k8xC	nebo
při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
těkavých	těkavý	k2eAgInPc2d1	těkavý
hydridů	hydrid	k1gInPc2	hydrid
arsenu	arsen	k1gInSc2	arsen
nebo	nebo	k8xC	nebo
antimonu	antimon	k1gInSc2	antimon
<g/>
.	.	kIx.	.
</s>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
cínatý	cínatý	k2eAgInSc4d1	cínatý
SnCl	SnCl	k1gInSc4	SnCl
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
mastně	mastně	k6eAd1	mastně
lesklá	lesklý	k2eAgFnSc1d1	lesklá
<g/>
,	,	kIx,	,
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
silné	silný	k2eAgNnSc1d1	silné
redukční	redukční	k2eAgNnSc1d1	redukční
činidlo	činidlo	k1gNnSc1	činidlo
<g/>
.	.	kIx.	.
</s>
<s>
Nejlépe	dobře	k6eAd3	dobře
se	se	k3xPyFc4	se
připraví	připravit	k5eAaPmIp3nS	připravit
vedením	vedení	k1gNnSc7	vedení
par	para	k1gFnPc2	para
chlorovodíku	chlorovodík	k1gInSc2	chlorovodík
přes	přes	k7c4	přes
rozžhavený	rozžhavený	k2eAgInSc4d1	rozžhavený
cín	cín	k1gInSc4	cín
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
cínatý	cínatý	k2eAgInSc1d1	cínatý
SnO	SnO	k1gFnSc4	SnO
je	být	k5eAaImIp3nS	být
tmavý	tmavý	k2eAgInSc1d1	tmavý
prášek	prášek	k1gInSc1	prášek
<g/>
,	,	kIx,	,
nerozpustný	rozpustný	k2eNgInSc1d1	nerozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
reakcí	reakce	k1gFnSc7	reakce
hydroxidu	hydroxid	k1gInSc2	hydroxid
cínatého	cínatý	k2eAgInSc2d1	cínatý
s	s	k7c7	s
cínatými	cínatý	k2eAgFnPc7d1	cínatý
nebo	nebo	k8xC	nebo
zásaditými	zásaditý	k2eAgFnPc7d1	zásaditá
látkami	látka	k1gFnPc7	látka
<g/>
.	.	kIx.	.
</s>
<s>
Sulfid	sulfid	k1gInSc1	sulfid
cínatý	cínatý	k2eAgInSc1d1	cínatý
SnS	SnS	k1gFnSc4	SnS
je	být	k5eAaImIp3nS	být
tmavě	tmavě	k6eAd1	tmavě
hnědá	hnědý	k2eAgFnSc1d1	hnědá
<g/>
,	,	kIx,	,
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
přímým	přímý	k2eAgNnSc7d1	přímé
slučováním	slučování	k1gNnSc7	slučování
cínu	cín	k1gInSc2	cín
se	s	k7c7	s
sírou	síra	k1gFnSc7	síra
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
z	z	k7c2	z
roztoků	roztok	k1gInPc2	roztok
cínatých	cínatý	k2eAgFnPc2d1	cínatý
solí	sůl	k1gFnPc2	sůl
po	po	k7c4	po
přidání	přidání	k1gNnSc4	přidání
roztoku	roztok	k1gInSc2	roztok
sirovodíku	sirovodík	k1gInSc2	sirovodík
<g/>
.	.	kIx.	.
</s>
<s>
Dusičnan	dusičnan	k1gInSc1	dusičnan
cínatý	cínatý	k2eAgInSc1d1	cínatý
Sn	Sn	k1gMnSc7	Sn
<g/>
(	(	kIx(	(
<g/>
NO	no	k9	no
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
hygroskopická	hygroskopický	k2eAgFnSc1d1	hygroskopická
<g/>
,	,	kIx,	,
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
snadno	snadno	k6eAd1	snadno
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
působením	působení	k1gNnSc7	působení
zředěné	zředěný	k2eAgFnSc2d1	zředěná
kyseliny	kyselina	k1gFnSc2	kyselina
dusičné	dusičný	k2eAgFnSc2d1	dusičná
na	na	k7c4	na
cín	cín	k1gInSc4	cín
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vzniká	vznikat	k5eAaImIp3nS	vznikat
vedle	vedle	k7c2	vedle
dusičnanu	dusičnan	k1gInSc2	dusičnan
cínatého	cínatý	k2eAgMnSc2d1	cínatý
i	i	k8xC	i
dusičnan	dusičnan	k1gInSc1	dusičnan
cíničitý	cíničitý	k2eAgInSc1d1	cíničitý
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ho	on	k3xPp3gInSc4	on
lze	lze	k6eAd1	lze
připravit	připravit	k5eAaPmF	připravit
reakcí	reakce	k1gFnSc7	reakce
oxidu	oxid	k1gInSc2	oxid
cínatého	cínatý	k2eAgInSc2d1	cínatý
nebo	nebo	k8xC	nebo
hydroxidu	hydroxid	k1gInSc2	hydroxid
cínatého	cínatý	k2eAgInSc2d1	cínatý
se	se	k3xPyFc4	se
zředěnou	zředěný	k2eAgFnSc7d1	zředěná
kyselinou	kyselina	k1gFnSc7	kyselina
dusičnou	dusičný	k2eAgFnSc7d1	dusičná
<g/>
.	.	kIx.	.
hydroxid	hydroxid	k1gInSc1	hydroxid
cínatý	cínatý	k2eAgInSc1d1	cínatý
Sn	Sn	k1gMnSc7	Sn
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
amfoterní	amfoterní	k2eAgFnSc1d1	amfoterní
bílá	bílý	k2eAgFnSc1d1	bílá
sraženina	sraženina	k1gFnSc1	sraženina
<g/>
.	.	kIx.	.
</s>
<s>
Cínovodík	Cínovodík	k1gInSc1	Cínovodík
SnH	SnH	k1gFnSc2	SnH
<g/>
4	[number]	k4	4
neboli	neboli	k8xC	neboli
stannan	stannan	k1gMnSc1	stannan
je	být	k5eAaImIp3nS	být
bezbarvá	bezbarvý	k2eAgNnPc1d1	bezbarvé
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
jedovatá	jedovatý	k2eAgFnSc1d1	jedovatá
plynná	plynný	k2eAgFnSc1d1	plynná
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
lehce	lehko	k6eAd1	lehko
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
při	při	k7c6	při
rozkladu	rozklad	k1gInSc6	rozklad
slitiny	slitina	k1gFnSc2	slitina
hořčíku	hořčík	k1gInSc2	hořčík
a	a	k8xC	a
cínu	cín	k1gInSc2	cín
čtyřnormální	čtyřnormální	k2eAgFnSc7d1	čtyřnormální
kyselinou	kyselina	k1gFnSc7	kyselina
chlorovodíkovou	chlorovodíkový	k2eAgFnSc7d1	chlorovodíková
<g/>
.	.	kIx.	.
</s>
<s>
Fluorid	fluorid	k1gInSc4	fluorid
cíničitý	cíničitý	k2eAgInSc4d1	cíničitý
SnF	SnF	k1gFnSc7	SnF
<g/>
4	[number]	k4	4
je	být	k5eAaImIp3nS	být
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
lehce	lehko	k6eAd1	lehko
tvoří	tvořit	k5eAaImIp3nS	tvořit
komplexní	komplexní	k2eAgFnPc4d1	komplexní
sloučeniny	sloučenina	k1gFnPc4	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
reakcí	reakce	k1gFnSc7	reakce
chloridu	chlorid	k1gInSc2	chlorid
cíničitého	cíničitý	k2eAgInSc2d1	cíničitý
s	s	k7c7	s
bezvodým	bezvodý	k2eAgInSc7d1	bezvodý
fluorovodíkem	fluorovodík	k1gInSc7	fluorovodík
<g/>
.	.	kIx.	.
</s>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
cíničitý	cíničitý	k2eAgInSc4d1	cíničitý
SnCl	SnCl	k1gInSc4	SnCl
<g/>
4	[number]	k4	4
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
bezvodém	bezvodý	k2eAgInSc6d1	bezvodý
stavu	stav	k1gInSc6	stav
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
<g/>
,	,	kIx,	,
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
dýmající	dýmající	k2eAgFnSc1d1	dýmající
kapalina	kapalina	k1gFnSc1	kapalina
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vodného	vodný	k2eAgInSc2d1	vodný
roztoku	roztok	k1gInSc2	roztok
krystalizuje	krystalizovat	k5eAaImIp3nS	krystalizovat
jako	jako	k9	jako
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
snadno	snadno	k6eAd1	snadno
rozplývá	rozplývat	k5eAaImIp3nS	rozplývat
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
a	a	k8xC	a
nejčastěji	často	k6eAd3	často
krystalizuje	krystalizovat	k5eAaImIp3nS	krystalizovat
jako	jako	k9	jako
pentahydrát	pentahydrát	k1gInSc1	pentahydrát
<g/>
.	.	kIx.	.
</s>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
cíničitý	cíničitý	k2eAgInSc1d1	cíničitý
snadno	snadno	k6eAd1	snadno
tvoří	tvořit	k5eAaImIp3nP	tvořit
komplexní	komplexní	k2eAgFnPc1d1	komplexní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
reakcí	reakce	k1gFnSc7	reakce
cínu	cín	k1gInSc2	cín
s	s	k7c7	s
chlorem	chlor	k1gInSc7	chlor
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaImIp3nS	využívat
se	se	k3xPyFc4	se
v	v	k7c4	v
barvířství	barvířství	k1gNnSc4	barvířství
jako	jako	k8xC	jako
mořidlo	mořidlo	k1gNnSc4	mořidlo
<g/>
.	.	kIx.	.
</s>
<s>
Bromid	bromid	k1gInSc1	bromid
cíničitý	cíničitý	k2eAgInSc4d1	cíničitý
SnBr	SnBr	k1gInSc4	SnBr
<g/>
4	[number]	k4	4
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
pokojové	pokojový	k2eAgFnPc4d1	pokojová
teploty	teplota	k1gFnPc4	teplota
sněhobílá	sněhobílý	k2eAgFnSc1d1	sněhobílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
snadno	snadno	k6eAd1	snadno
tvoří	tvořit	k5eAaImIp3nP	tvořit
komplexní	komplexní	k2eAgFnPc1d1	komplexní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
slučování	slučování	k1gNnSc1	slučování
cínu	cín	k1gInSc2	cín
s	s	k7c7	s
bromem	brom	k1gInSc7	brom
<g/>
.	.	kIx.	.
</s>
<s>
Jodid	jodid	k1gInSc1	jodid
cíničitý	cíničitý	k2eAgInSc1d1	cíničitý
SnI	snít	k5eAaImRp2nS	snít
<g/>
4	[number]	k4	4
je	být	k5eAaImIp3nS	být
žlutá	žlutý	k2eAgFnSc1d1	žlutá
až	až	k8xS	až
žlutohnědá	žlutohnědý	k2eAgFnSc1d1	žlutohnědá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
vodě	voda	k1gFnSc3	voda
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c4	na
jodid	jodid	k1gInSc4	jodid
cínatý	cínatý	k2eAgInSc4d1	cínatý
a	a	k8xC	a
jod	jod	k1gInSc4	jod
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
reakcí	reakce	k1gFnSc7	reakce
cínu	cín	k1gInSc2	cín
s	s	k7c7	s
jodem	jod	k1gInSc7	jod
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc4	oxid
cíničitý	cíničitý	k2eAgInSc4d1	cíničitý
SnO	SnO	k1gFnSc7	SnO
<g/>
2	[number]	k4	2
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
nerost	nerost	k1gInSc4	nerost
cínovec	cínovec	k1gInSc1	cínovec
(	(	kIx(	(
<g/>
kasiterit	kasiterit	k1gInSc1	kasiterit
<g/>
)	)	kIx)	)
hnědočerné	hnědočerný	k2eAgFnSc2d1	hnědočerná
<g/>
,	,	kIx,	,
šedé	šedý	k2eAgFnSc2d1	šedá
nebo	nebo	k8xC	nebo
žluté	žlutý	k2eAgFnSc2d1	žlutá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc7d1	základní
surovinou	surovina	k1gFnSc7	surovina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
cínu	cín	k1gInSc2	cín
<g/>
.	.	kIx.	.
</s>
<s>
Nemá	mít	k5eNaImIp3nS	mít
kapalné	kapalný	k2eAgNnSc1d1	kapalné
skupenství	skupenství	k1gNnSc1	skupenství
<g/>
,	,	kIx,	,
při	při	k7c6	při
1800	[number]	k4	1800
°	°	k?	°
<g/>
C	C	kA	C
sublimuje	sublimovat	k5eAaBmIp3nS	sublimovat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
je	být	k5eAaImIp3nS	být
nerozpustný	rozpustný	k2eNgInSc1d1	nerozpustný
a	a	k8xC	a
ani	ani	k8xC	ani
s	s	k7c7	s
kyselinami	kyselina	k1gFnPc7	kyselina
a	a	k8xC	a
roztoky	roztoka	k1gFnSc2	roztoka
hydroxidů	hydroxid	k1gInPc2	hydroxid
nijak	nijak	k6eAd1	nijak
výrazně	výrazně	k6eAd1	výrazně
nereaguje	reagovat	k5eNaBmIp3nS	reagovat
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
bílých	bílý	k2eAgInPc2d1	bílý
smaltů	smalt	k1gInPc2	smalt
<g/>
,	,	kIx,	,
glazur	glazura	k1gFnPc2	glazura
a	a	k8xC	a
leštících	leštící	k2eAgInPc2d1	leštící
prášků	prášek	k1gInPc2	prášek
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
antistatické	antistatický	k2eAgInPc4d1	antistatický
účinky	účinek	k1gInPc4	účinek
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
aplikuje	aplikovat	k5eAaBmIp3nS	aplikovat
na	na	k7c4	na
povrchy	povrch	k1gInPc4	povrch
skel	sklo	k1gNnPc2	sklo
(	(	kIx(	(
<g/>
vzniká	vznikat	k5eAaImIp3nS	vznikat
mléčné	mléčný	k2eAgNnSc1d1	mléčné
sklo	sklo	k1gNnSc1	sklo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
spalováním	spalování	k1gNnSc7	spalování
cínu	cín	k1gInSc2	cín
v	v	k7c6	v
proudu	proud	k1gInSc6	proud
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Sulfid	sulfid	k1gInSc4	sulfid
cíničitý	cíničitý	k2eAgInSc4d1	cíničitý
SnS	SnS	k1gFnSc7	SnS
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
zlatožlutá	zlatožlutý	k2eAgFnSc1d1	zlatožlutá
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
obchodní	obchodní	k2eAgInSc4d1	obchodní
název	název	k1gInSc4	název
musivní	musivní	k2eAgNnSc4d1	musivní
zlato	zlato	k1gNnSc4	zlato
-	-	kIx~	-
tj.	tj.	kA	tj.
zlato	zlato	k1gNnSc4	zlato
pro	pro	k7c4	pro
mosaikové	mosaikový	k2eAgFnPc4d1	mosaiková
práce	práce	k1gFnPc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
několik	několik	k4yIc1	několik
komplexů	komplex	k1gInPc2	komplex
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
působením	působení	k1gNnSc7	působení
sulfanu	sulfan	k1gInSc2	sulfan
na	na	k7c4	na
roztoky	roztoka	k1gFnPc4	roztoka
cíničitých	cíničitý	k2eAgFnPc2d1	cíničitý
sloučenin	sloučenina	k1gFnPc2	sloučenina
nebo	nebo	k8xC	nebo
zahříváním	zahřívání	k1gNnSc7	zahřívání
cínu	cín	k1gInSc2	cín
se	s	k7c7	s
sírou	síra	k1gFnSc7	síra
a	a	k8xC	a
salmiakem	salmiak	k1gInSc7	salmiak
<g/>
.	.	kIx.	.
</s>
<s>
COTTON	COTTON	kA	COTTON
<g/>
,	,	kIx,	,
F.	F.	kA	F.
A.	A.	kA	A.
-	-	kIx~	-
WILKINSON	WILKINSON	kA	WILKINSON
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
:	:	kIx,	:
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k2eAgMnPc4d1	pokročilý
<g/>
.	.	kIx.	.
</s>
<s>
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
HOLZBECHER	HOLZBECHER	kA	HOLZBECHER
<g/>
,	,	kIx,	,
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
.	.	kIx.	.
</s>
<s>
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1961	[number]	k4	1961
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwooda	k1gFnPc2	Greenwooda
-	-	kIx~	-
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc1	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
MAJER	Majer	k1gMnSc1	Majer
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Těžba	těžba	k1gFnSc1	těžba
cínu	cín	k1gInSc2	cín
ve	v	k7c6	v
Slavkovském	slavkovský	k2eAgInSc6d1	slavkovský
lese	les	k1gInSc6	les
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Národní	národní	k2eAgNnSc1d1	národní
technické	technický	k2eAgNnSc1d1	technické
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
227	[number]	k4	227
s.	s.	k?	s.
RUŽA	RUŽA	kA	RUŽA
<g/>
,	,	kIx,	,
V.	V.	kA	V.
<g/>
:	:	kIx,	:
Pájení	pájení	k1gNnSc1	pájení
<g/>
.	.	kIx.	.
</s>
<s>
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
ABEL	ABEL	kA	ABEL
<g/>
,	,	kIx,	,
M.	M.	kA	M.
-	-	kIx~	-
CIMBUREK	CIMBUREK	kA	CIMBUREK
<g/>
,	,	kIx,	,
V.	V.	kA	V.
<g/>
:	:	kIx,	:
Bezolovnaté	bezolovnatý	k2eAgNnSc1d1	bezolovnaté
pájení	pájení	k1gNnSc1	pájení
v	v	k7c6	v
legislativě	legislativa	k1gFnSc6	legislativa
i	i	k8xC	i
praxi	praxe	k1gFnSc6	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Abetec	Abetec	k1gMnSc1	Abetec
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
903597	[number]	k4	903597
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
ČSN	ČSN	kA	ČSN
EN	EN	kA	EN
ISO	ISO	kA	ISO
9453	[number]	k4	9453
Slitiny	slitina	k1gFnSc2	slitina
pro	pro	k7c4	pro
měkké	měkký	k2eAgNnSc4d1	měkké
pájení	pájení	k1gNnSc4	pájení
-	-	kIx~	-
Chemické	chemický	k2eAgNnSc4d1	chemické
složení	složení	k1gNnSc4	složení
a	a	k8xC	a
tvary	tvar	k1gInPc4	tvar
cínový	cínový	k2eAgInSc4d1	cínový
mor	mor	k1gInSc4	mor
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
cín	cín	k1gInSc1	cín
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
cín	cín	k1gInSc1	cín
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
