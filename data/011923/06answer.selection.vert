<s>
Noha	noha	k1gFnSc1	noha
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
končetin	končetina	k1gFnPc2	končetina
<g/>
,	,	kIx,	,
nese	nést	k5eAaImIp3nS	nést
váhu	váha	k1gFnSc4	váha
celého	celý	k2eAgNnSc2d1	celé
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
udržovat	udržovat	k5eAaImF	udržovat
vzpřímený	vzpřímený	k2eAgInSc4d1	vzpřímený
postoj	postoj	k1gInSc4	postoj
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
pohyb	pohyb	k1gInSc1	pohyb
<g/>
.	.	kIx.	.
</s>
