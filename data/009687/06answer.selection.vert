<s>
Ganymed	Ganymed	k1gMnSc1	Ganymed
(	(	kIx(	(
<g/>
též	též	k9	též
Ganymedes	Ganymedes	k1gMnSc1	Ganymedes
nebo	nebo	k8xC	nebo
z	z	k7c2	z
angl.	angl.	k?	angl.
Ganymede	Ganymed	k1gMnSc5	Ganymed
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc4d3	veliký
Jupiterův	Jupiterův	k2eAgInSc4d1	Jupiterův
měsíc	měsíc	k1gInSc4	měsíc
a	a	k8xC	a
současně	současně	k6eAd1	současně
i	i	k9	i
největší	veliký	k2eAgInSc4d3	veliký
měsíc	měsíc	k1gInSc4	měsíc
ve	v	k7c6	v
Sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
(	(	kIx(	(
<g/>
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
Titanem	titan	k1gInSc7	titan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
