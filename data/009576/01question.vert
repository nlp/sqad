<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
optickému	optický	k2eAgNnSc3d1	optické
zařízení	zařízení	k1gNnSc3	zařízení
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
zachytit	zachytit	k5eAaPmF	zachytit
obrazy	obraz	k1gInPc4	obraz
pro	pro	k7c4	pro
kinematografii	kinematografie	k1gFnSc4	kinematografie
<g/>
?	?	kIx.	?
</s>
