<p>
<s>
Filmová	filmový	k2eAgFnSc1d1	filmová
kamera	kamera	k1gFnSc1	kamera
je	být	k5eAaImIp3nS	být
technické	technický	k2eAgNnSc4d1	technické
optické	optický	k2eAgNnSc4d1	optické
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
zachytit	zachytit	k5eAaPmF	zachytit
obrazy	obraz	k1gInPc4	obraz
pro	pro	k7c4	pro
kinematografii	kinematografie	k1gFnSc4	kinematografie
<g/>
,	,	kIx,	,
televizi	televize	k1gFnSc4	televize
apod.	apod.	kA	apod.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
vlastně	vlastně	k9	vlastně
o	o	k7c4	o
velký	velký	k2eAgInSc4d1	velký
speciální	speciální	k2eAgInSc4d1	speciální
fotoaparát	fotoaparát	k1gInSc4	fotoaparát
<g/>
,	,	kIx,	,
upravený	upravený	k2eAgInSc1d1	upravený
pro	pro	k7c4	pro
kontinuální	kontinuální	k2eAgNnPc4d1	kontinuální
fotografování	fotografování	k1gNnPc4	fotografování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Principem	princip	k1gInSc7	princip
záznamů	záznam	k1gInPc2	záznam
obrazu	obraz	k1gInSc2	obraz
filmovou	filmový	k2eAgFnSc7d1	filmová
kamerou	kamera	k1gFnSc7	kamera
je	on	k3xPp3gNnPc4	on
kontinuální	kontinuální	k2eAgNnPc4d1	kontinuální
exponování	exponování	k1gNnPc4	exponování
obrazů	obraz	k1gInPc2	obraz
na	na	k7c4	na
filmový	filmový	k2eAgInSc4d1	filmový
pás	pás	k1gInSc4	pás
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
časové	časový	k2eAgInPc4d1	časový
intervaly	interval	k1gInPc4	interval
mezi	mezi	k7c7	mezi
</s>
</p>
<p>
<s>
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
snímky	snímek	k1gInPc7	snímek
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
kratší	krátký	k2eAgFnSc1d2	kratší
jako	jako	k8xS	jako
hranice	hranice	k1gFnSc1	hranice
rozpoznatelnosti	rozpoznatelnost	k1gFnSc2	rozpoznatelnost
lidského	lidský	k2eAgNnSc2d1	lidské
oka	oko	k1gNnSc2	oko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
není	být	k5eNaImIp3nS	být
schopno	schopen	k2eAgNnSc1d1	schopno
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
vlivem	vliv	k1gInSc7	vliv
setrvačnosti	setrvačnost	k1gFnSc2	setrvačnost
lidského	lidský	k2eAgNnSc2d1	lidské
oka	oko	k1gNnSc2	oko
<g/>
)	)	kIx)	)
rozlišit	rozlišit	k5eAaPmF	rozlišit
dva	dva	k4xCgMnPc1	dva
po	po	k7c4	po
sobě	se	k3xPyFc3	se
následující	následující	k2eAgInPc4d1	následující
obrazy	obraz	k1gInPc4	obraz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
následují	následovat	k5eAaImIp3nP	následovat
za	za	k7c7	za
sebou	se	k3xPyFc7	se
dva	dva	k4xCgInPc4	dva
obrazy	obraz	k1gInPc4	obraz
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
za	za	k7c4	za
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
16	[number]	k4	16
sekundy	sekunda	k1gFnSc2	sekunda
lidské	lidský	k2eAgNnSc1d1	lidské
oko	oko	k1gNnSc1	oko
tento	tento	k3xDgInSc4	tento
fakt	fakt	k1gInSc4	fakt
není	být	k5eNaImIp3nS	být
schopno	schopen	k2eAgNnSc1d1	schopno
zachytit	zachytit	k5eAaPmF	zachytit
a	a	k8xC	a
lidský	lidský	k2eAgInSc4d1	lidský
mozek	mozek	k1gInSc4	mozek
vnímat	vnímat	k5eAaImF	vnímat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
promítání	promítání	k1gNnSc6	promítání
(	(	kIx(	(
<g/>
ve	v	k7c6	v
filmové	filmový	k2eAgFnSc6d1	filmová
promítačce	promítačka	k1gFnSc6	promítačka
<g/>
)	)	kIx)	)
takto	takto	k6eAd1	takto
nasnímaných	nasnímaný	k2eAgInPc2d1	nasnímaný
obrazů	obraz	k1gInPc2	obraz
stejnou	stejný	k2eAgFnSc4d1	stejná
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
,	,	kIx,	,
jakou	jaký	k3yIgFnSc4	jaký
byl	být	k5eAaImAgInS	být
původní	původní	k2eAgInSc1d1	původní
</s>
</p>
<p>
<s>
obraz	obraz	k1gInSc1	obraz
nasnímán	nasnímat	k5eAaPmNgInS	nasnímat
filmovou	filmový	k2eAgFnSc7d1	filmová
kamerou	kamera	k1gFnSc7	kamera
(	(	kIx(	(
<g/>
typicky	typicky	k6eAd1	typicky
24	[number]	k4	24
krát	krát	k6eAd1	krát
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lidské	lidský	k2eAgNnSc1d1	lidské
oko	oko	k1gNnSc1	oko
vnímá	vnímat	k5eAaImIp3nS	vnímat
obraz	obraz	k1gInSc4	obraz
jako	jako	k8xC	jako
kontinuálně	kontinuálně	k6eAd1	kontinuálně
se	se	k3xPyFc4	se
pohybující	pohybující	k2eAgFnPc1d1	pohybující
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
obrazy	obraz	k1gInPc1	obraz
se	se	k3xPyFc4	se
v	v	k7c6	v
lidském	lidský	k2eAgInSc6d1	lidský
mozku	mozek	k1gInSc6	mozek
slévají	slévat	k5eAaImIp3nP	slévat
dohromady	dohromady	k6eAd1	dohromady
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Konstrukce	konstrukce	k1gFnSc2	konstrukce
kamery	kamera	k1gFnSc2	kamera
==	==	k?	==
</s>
</p>
<p>
<s>
Kamera	kamera	k1gFnSc1	kamera
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
principu	princip	k1gInSc2	princip
běžného	běžný	k2eAgInSc2d1	běžný
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
známému	známý	k1gMnSc3	známý
z	z	k7c2	z
klasické	klasický	k2eAgFnSc2d1	klasická
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kameře	kamera	k1gFnSc6	kamera
tedy	tedy	k9	tedy
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
filmový	filmový	k2eAgInSc4d1	filmový
pás	pás	k1gInSc4	pás
(	(	kIx(	(
<g/>
kinofilm	kinofilm	k1gInSc1	kinofilm
<g/>
)	)	kIx)	)
přístroj	přístroj	k1gInSc1	přístroj
pořizuje	pořizovat	k5eAaImIp3nS	pořizovat
v	v	k7c6	v
pravidelných	pravidelný	k2eAgInPc6d1	pravidelný
časových	časový	k2eAgInPc6d1	časový
intervalech	interval	k1gInPc6	interval
jednu	jeden	k4xCgFnSc4	jeden
fotografii	fotografia	k1gFnSc4	fotografia
za	za	k7c7	za
druhou	druhý	k4xOgFnSc7	druhý
(	(	kIx(	(
<g/>
typicky	typicky	k6eAd1	typicky
tedy	tedy	k8xC	tedy
24	[number]	k4	24
fotografií	fotografia	k1gFnPc2	fotografia
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obdobně	obdobně	k6eAd1	obdobně
jako	jako	k9	jako
klasický	klasický	k2eAgInSc4d1	klasický
fotoaparát	fotoaparát	k1gInSc4	fotoaparát
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
filmová	filmový	k2eAgFnSc1d1	filmová
kamera	kamera	k1gFnSc1	kamera
objektiv	objektiv	k1gInSc4	objektiv
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
převádí	převádět	k5eAaImIp3nS	převádět
dopadající	dopadající	k2eAgNnSc4d1	dopadající
venkovní	venkovní	k2eAgNnSc4d1	venkovní
světlo	světlo	k1gNnSc4	světlo
na	na	k7c4	na
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
filmový	filmový	k2eAgInSc4d1	filmový
pás	pás	k1gInSc4	pás
<g/>
.	.	kIx.	.
</s>
<s>
Filmová	filmový	k2eAgFnSc1d1	filmová
kamera	kamera	k1gFnSc1	kamera
je	být	k5eAaImIp3nS	být
také	také	k9	také
vybavena	vybavit	k5eAaPmNgFnS	vybavit
závěrkou	závěrka	k1gFnSc7	závěrka
určenou	určený	k2eAgFnSc7d1	určená
k	k	k7c3	k
regulaci	regulace	k1gFnSc3	regulace
clony	clona	k1gFnSc2	clona
a	a	k8xC	a
vlastní	vlastní	k2eAgFnSc7d1	vlastní
clonkou	clonka	k1gFnSc7	clonka
zajišťující	zajišťující	k2eAgFnSc2d1	zajišťující
expozice	expozice	k1gFnSc2	expozice
snímků	snímek	k1gInPc2	snímek
v	v	k7c4	v
daný	daný	k2eAgInSc4d1	daný
přesný	přesný	k2eAgInSc4d1	přesný
časový	časový	k2eAgInSc4d1	časový
okamžik	okamžik	k1gInSc4	okamžik
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
klasického	klasický	k2eAgInSc2d1	klasický
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
se	se	k3xPyFc4	se
film	film	k1gInSc1	film
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
kameře	kamera	k1gFnSc6	kamera
prakticky	prakticky	k6eAd1	prakticky
pravidelně	pravidelně	k6eAd1	pravidelně
krokově	krokově	k6eAd1	krokově
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
přesným	přesný	k2eAgInSc7d1	přesný
konstantním	konstantní	k2eAgInSc7d1	konstantní
intervalem	interval	k1gInSc7	interval
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
pohyby	pohyb	k1gInPc1	pohyb
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
pohyb	pohyb	k1gInSc1	pohyb
závěrky	závěrka	k1gFnSc2	závěrka
i	i	k8xC	i
pohyb	pohyb	k1gInSc1	pohyb
filmu	film	k1gInSc2	film
zde	zde	k6eAd1	zde
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
velmi	velmi	k6eAd1	velmi
přesně	přesně	k6eAd1	přesně
vzájemně	vzájemně	k6eAd1	vzájemně
sesynchronizovány	sesynchronizován	k2eAgFnPc1d1	sesynchronizován
<g/>
.	.	kIx.	.
</s>
<s>
Důležitá	důležitý	k2eAgFnSc1d1	důležitá
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
i	i	k9	i
naprosto	naprosto	k6eAd1	naprosto
přesná	přesný	k2eAgFnSc1d1	přesná
poloha	poloha	k1gFnSc1	poloha
filmového	filmový	k2eAgInSc2d1	filmový
pásu	pás	k1gInSc2	pás
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
kameře	kamera	k1gFnSc6	kamera
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
pomocí	pomocí	k7c2	pomocí
důmyslného	důmyslný	k2eAgInSc2d1	důmyslný
mechanického	mechanický	k2eAgInSc2d1	mechanický
systému	systém	k1gInSc2	systém
vodících	vodící	k2eAgFnPc2d1	vodící
lišt	lišta	k1gFnPc2	lišta
a	a	k8xC	a
přítlačných	přítlačný	k2eAgFnPc2d1	přítlačná
součástek	součástka	k1gFnPc2	součástka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aby	aby	kYmCp3nS	aby
nebyl	být	k5eNaImAgInS	být
výsledný	výsledný	k2eAgInSc1d1	výsledný
obraz	obraz	k1gInSc1	obraz
rozmazaný	rozmazaný	k2eAgInSc1d1	rozmazaný
nebo	nebo	k8xC	nebo
roztřesený	roztřesený	k2eAgInSc1d1	roztřesený
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
expozice	expozice	k1gFnSc2	expozice
filmu	film	k1gInSc2	film
(	(	kIx(	(
<g/>
otevřené	otevřený	k2eAgFnSc2d1	otevřená
závěrky	závěrka	k1gFnSc2	závěrka
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nesmí	smět	k5eNaImIp3nS	smět
filmový	filmový	k2eAgInSc1d1	filmový
pás	pás	k1gInSc1	pás
nijak	nijak	k6eAd1	nijak
pohybovat	pohybovat	k5eAaImF	pohybovat
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
v	v	k7c6	v
naprostém	naprostý	k2eAgInSc6d1	naprostý
klidu	klid	k1gInSc6	klid
a	a	k8xC	a
musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
nacházet	nacházet	k5eAaImF	nacházet
na	na	k7c6	na
přesném	přesný	k2eAgNnSc6d1	přesné
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
uzavření	uzavření	k1gNnSc6	uzavření
závěrky	závěrka	k1gFnSc2	závěrka
mechanizmus	mechanizmus	k1gInSc4	mechanizmus
kamery	kamera	k1gFnSc2	kamera
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
velice	velice	k6eAd1	velice
přesně	přesně	k6eAd1	přesně
(	(	kIx(	(
<g/>
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
malý	malý	k2eAgInSc4d1	malý
zlomek	zlomek	k1gInSc4	zlomek
sekundy	sekunda	k1gFnSc2	sekunda
<g/>
)	)	kIx)	)
posune	posunout	k5eAaPmIp3nS	posunout
filmový	filmový	k2eAgInSc1d1	filmový
pás	pás	k1gInSc1	pás
o	o	k7c4	o
jedno	jeden	k4xCgNnSc4	jeden
políčko	políčko	k1gNnSc4	políčko
dále	daleko	k6eAd2	daleko
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
kroku	krok	k1gInSc6	krok
exponován	exponován	k2eAgInSc4d1	exponován
další	další	k2eAgInSc4d1	další
snímek	snímek	k1gInSc4	snímek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pohyb	pohyb	k1gInSc1	pohyb
filmu	film	k1gInSc2	film
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
velice	velice	k6eAd1	velice
přesný	přesný	k2eAgInSc1d1	přesný
i	i	k8xC	i
rychlý	rychlý	k2eAgInSc1d1	rychlý
(	(	kIx(	(
<g/>
časově	časově	k6eAd1	časově
i	i	k9	i
polohově	polohově	k6eAd1	polohově
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
toho	ten	k3xDgMnSc4	ten
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
klasický	klasický	k2eAgInSc1d1	klasický
filmový	filmový	k2eAgInSc1d1	filmový
pás	pás	k1gInSc1	pás
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
po	po	k7c6	po
celé	celá	k1gFnSc6	celá
své	svůj	k3xOyFgFnSc6	svůj
délce	délka	k1gFnSc6	délka
perforován	perforován	k2eAgMnSc1d1	perforován
<g/>
.	.	kIx.	.
</s>
<s>
Mechanizmus	mechanizmus	k1gInSc1	mechanizmus
kamery	kamera	k1gFnSc2	kamera
pak	pak	k6eAd1	pak
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
pohyblivých	pohyblivý	k2eAgFnPc2d1	pohyblivá
součástí	součást	k1gFnPc2	součást
zapadajících	zapadající	k2eAgFnPc2d1	zapadající
do	do	k7c2	do
perforace	perforace	k1gFnSc2	perforace
pásu	pás	k1gInSc2	pás
(	(	kIx(	(
<g/>
kolíčky	kolíček	k1gInPc4	kolíček
<g/>
,	,	kIx,	,
drápky	drápek	k1gInPc4	drápek
<g/>
)	)	kIx)	)
postrkuje	postrkovat	k5eAaImIp3nS	postrkovat
filmový	filmový	k2eAgInSc1d1	filmový
pás	pás	k1gInSc1	pás
kupředu	kupředu	k6eAd1	kupředu
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
mechanizmus	mechanizmus	k1gInSc1	mechanizmus
kamery	kamera	k1gFnSc2	kamera
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
poháněn	poháněn	k2eAgInSc1d1	poháněn
jedním	jeden	k4xCgInSc7	jeden
přesným	přesný	k2eAgInSc7d1	přesný
elektromotorkem	elektromotorek	k1gInSc7	elektromotorek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zde	zde	k6eAd1	zde
pohání	pohánět	k5eAaImIp3nS	pohánět
všechny	všechen	k3xTgInPc1	všechen
pohyblivé	pohyblivý	k2eAgInPc1d1	pohyblivý
mechanizmy	mechanizmus	k1gInPc1	mechanizmus
kamery	kamera	k1gFnSc2	kamera
včetně	včetně	k7c2	včetně
závěrky	závěrka	k1gFnSc2	závěrka
<g/>
.	.	kIx.	.
</s>
<s>
Poháněcí	poháněcí	k2eAgInSc1d1	poháněcí
elektromotor	elektromotor	k1gInSc1	elektromotor
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
velice	velice	k6eAd1	velice
přesně	přesně	k6eAd1	přesně
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
celý	celý	k2eAgInSc4d1	celý
mechanizmus	mechanizmus	k1gInSc4	mechanizmus
kamery	kamera	k1gFnSc2	kamera
musí	muset	k5eAaImIp3nS	muset
pracovat	pracovat	k5eAaImF	pracovat
pod	pod	k7c4	pod
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
s	s	k7c7	s
přesnou	přesný	k2eAgFnSc7d1	přesná
konstantní	konstantní	k2eAgFnSc7d1	konstantní
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Profesionální	profesionální	k2eAgFnPc1d1	profesionální
i	i	k8xC	i
amatérské	amatérský	k2eAgFnPc1d1	amatérská
kamery	kamera	k1gFnPc1	kamera
bývají	bývat	k5eAaImIp3nP	bývat
vybaveny	vybaven	k2eAgFnPc1d1	vybavena
regulací	regulace	k1gFnSc7	regulace
počtu	počet	k1gInSc2	počet
snímků	snímek	k1gInPc2	snímek
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
od	od	k7c2	od
1	[number]	k4	1
do	do	k7c2	do
50	[number]	k4	50
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
lze	lze	k6eAd1	lze
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
takto	takto	k6eAd1	takto
vybavené	vybavený	k2eAgFnSc2d1	vybavená
kamery	kamera	k1gFnSc2	kamera
obraz	obraz	k1gInSc1	obraz
při	při	k7c6	při
promítání	promítání	k1gNnSc6	promítání
zrychlovat	zrychlovat	k5eAaImF	zrychlovat
nebo	nebo	k8xC	nebo
zpomalovat	zpomalovat	k5eAaImF	zpomalovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pohon	pohon	k1gInSc1	pohon
kamery	kamera	k1gFnSc2	kamera
==	==	k?	==
</s>
</p>
<p>
<s>
Pohon	pohon	k1gInSc1	pohon
kamery	kamera	k1gFnSc2	kamera
obstarává	obstarávat	k5eAaImIp3nS	obstarávat
elektromotorek	elektromotorek	k1gInSc1	elektromotorek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dávných	dávný	k2eAgFnPc6d1	dávná
dobách	doba	k1gFnPc6	doba
filmové	filmový	k2eAgFnSc2d1	filmová
prehistorie	prehistorie	k1gFnSc2	prehistorie
byly	být	k5eAaImAgFnP	být
první	první	k4xOgFnPc1	první
filmové	filmový	k2eAgFnPc1d1	filmová
kamery	kamera	k1gFnPc1	kamera
poháněny	pohánět	k5eAaImNgFnP	pohánět
pouze	pouze	k6eAd1	pouze
ručně	ručně	k6eAd1	ručně
kameramanem	kameraman	k1gMnSc7	kameraman
pomocí	pomocí	k7c2	pomocí
kliky	klika	k1gFnSc2	klika
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgFnPc1d2	pozdější
menší	malý	k2eAgFnPc1d2	menší
kamery	kamera	k1gFnPc1	kamera
měly	mít	k5eAaImAgFnP	mít
pohon	pohon	k1gInSc4	pohon
na	na	k7c4	na
pero	pero	k1gNnSc4	pero
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
natahovalo	natahovat	k5eAaImAgNnS	natahovat
ručně	ručně	k6eAd1	ručně
-	-	kIx~	-
například	například	k6eAd1	například
československé	československý	k2eAgFnSc2d1	Československá
amatérské	amatérský	k2eAgFnSc2d1	amatérská
kamery	kamera	k1gFnSc2	kamera
značky	značka	k1gFnSc2	značka
Admira	Admir	k1gInSc2	Admir
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filmový	filmový	k2eAgInSc1d1	filmový
pás	pás	k1gInSc1	pás
==	==	k?	==
</s>
</p>
<p>
<s>
Klasický	klasický	k2eAgInSc1d1	klasický
35	[number]	k4	35
mm	mm	kA	mm
kinofilm	kinofilm	k1gInSc1	kinofilm
se	se	k3xPyFc4	se
do	do	k7c2	do
kamery	kamera	k1gFnSc2	kamera
původně	původně	k6eAd1	původně
vkládal	vkládat	k5eAaImAgMnS	vkládat
z	z	k7c2	z
kazet	kazeta	k1gFnPc2	kazeta
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
byl	být	k5eAaImAgInS	být
mechanicky	mechanicky	k6eAd1	mechanicky
namotán	namotat	k5eAaPmNgInS	namotat
na	na	k7c6	na
kovové	kovový	k2eAgFnSc6d1	kovová
cívce	cívka	k1gFnSc6	cívka
<g/>
.	.	kIx.	.
</s>
<s>
Kazeta	kazeta	k1gFnSc1	kazeta
se	se	k3xPyFc4	se
připevnila	připevnit	k5eAaPmAgFnS	připevnit
na	na	k7c4	na
kameru	kamera	k1gFnSc4	kamera
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgInSc4d1	filmový
pás	pás	k1gInSc4	pás
do	do	k7c2	do
kamery	kamera	k1gFnSc2	kamera
vstupoval	vstupovat	k5eAaImAgInS	vstupovat
vstupním	vstupní	k2eAgInSc7d1	vstupní
otvorem	otvor	k1gInSc7	otvor
<g/>
.	.	kIx.	.
</s>
<s>
Exponovaný	exponovaný	k2eAgInSc1d1	exponovaný
film	film	k1gInSc1	film
ze	z	k7c2	z
navíjel	navíjet	k5eAaImAgMnS	navíjet
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
cívku	cívka	k1gFnSc4	cívka
v	v	k7c6	v
jiné	jiná	k1gFnSc6	jiná
(	(	kIx(	(
<g/>
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
prázdné	prázdný	k2eAgNnSc1d1	prázdné
<g/>
)	)	kIx)	)
kazetě	kazeta	k1gFnSc6	kazeta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Současnost	současnost	k1gFnSc4	současnost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
nástupu	nástup	k1gInSc3	nástup
elektroniky	elektronika	k1gFnSc2	elektronika
a	a	k8xC	a
digitální	digitální	k2eAgFnSc2d1	digitální
techniky	technika	k1gFnSc2	technika
prakticky	prakticky	k6eAd1	prakticky
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
oblastech	oblast	k1gFnPc6	oblast
lidského	lidský	k2eAgInSc2d1	lidský
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
tyto	tento	k3xDgInPc1	tento
trendy	trend	k1gInPc1	trend
neminuly	minout	k5eNaImAgInP	minout
ani	ani	k9	ani
klasické	klasický	k2eAgFnPc1d1	klasická
filmové	filmový	k2eAgFnPc1d1	filmová
kamery	kamera	k1gFnPc1	kamera
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnPc1d1	moderní
profesionální	profesionální	k2eAgFnPc1d1	profesionální
kamery	kamera	k1gFnPc1	kamera
mají	mít	k5eAaImIp3nP	mít
zcela	zcela	k6eAd1	zcela
běžně	běžně	k6eAd1	běžně
paralelní	paralelní	k2eAgInSc4d1	paralelní
výstup	výstup	k1gInSc4	výstup
pro	pro	k7c4	pro
digitální	digitální	k2eAgInSc4d1	digitální
videozáznam	videozáznam	k1gInSc4	videozáznam
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
také	také	k9	také
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
prakticky	prakticky	k6eAd1	prakticky
okamžitou	okamžitý	k2eAgFnSc4d1	okamžitá
kontrolu	kontrola	k1gFnSc4	kontrola
právě	právě	k6eAd1	právě
nasnímané	nasnímaný	k2eAgFnPc1d1	nasnímaná
sekvence	sekvence	k1gFnPc1	sekvence
pomocí	pomocí	k7c2	pomocí
náhledového	náhledový	k2eAgInSc2d1	náhledový
monitoru	monitor	k1gInSc2	monitor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
profesionální	profesionální	k2eAgFnPc1d1	profesionální
kamery	kamera	k1gFnPc1	kamera
jsou	být	k5eAaImIp3nP	být
vybaveny	vybaven	k2eAgInPc1d1	vybaven
špičkovými	špičkový	k2eAgInPc7d1	špičkový
výměnnými	výměnný	k2eAgInPc7d1	výměnný
objektivy	objektiv	k1gInPc7	objektiv
</s>
</p>
<p>
<s>
případně	případně	k6eAd1	případně
transfokátory	transfokátor	k1gInPc1	transfokátor
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgNnSc1d1	vlastní
tělo	tělo	k1gNnSc1	tělo
kamery	kamera	k1gFnSc2	kamera
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
z	z	k7c2	z
odlehčených	odlehčený	k2eAgInPc2d1	odlehčený
kompozitních	kompozitní	k2eAgInPc2d1	kompozitní
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
což	což	k3yRnSc1	což
radikálně	radikálně	k6eAd1	radikálně
snižuje	snižovat	k5eAaImIp3nS	snižovat
celkovou	celkový	k2eAgFnSc4d1	celková
hmotnost	hmotnost	k1gFnSc4	hmotnost
</s>
</p>
<p>
<s>
přístroje	přístroj	k1gInPc1	přístroj
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
míry	míra	k1gFnSc2	míra
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
kamery	kamera	k1gFnSc2	kamera
nosit	nosit	k5eAaImF	nosit
i	i	k9	i
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
(	(	kIx(	(
<g/>
nepoužívat	používat	k5eNaImF	používat
stativy	stativ	k1gInPc4	stativ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příslušenství	příslušenství	k1gNnSc1	příslušenství
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Optické	optický	k2eAgMnPc4d1	optický
===	===	k?	===
</s>
</p>
<p>
<s>
kompendium	kompendium	k1gNnSc1	kompendium
(	(	kIx(	(
<g/>
mattebox	mattebox	k1gInSc1	mattebox
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
objektivy	objektiv	k1gInPc1	objektiv
<g/>
,	,	kIx,	,
transfokátory	transfokátor	k1gInPc1	transfokátor
</s>
</p>
<p>
<s>
předsádkové	předsádkový	k2eAgInPc1d1	předsádkový
filmy	film	k1gInPc1	film
</s>
</p>
<p>
<s>
kamerové	kamerový	k2eAgFnPc1d1	kamerová
očnice	očnice	k1gFnPc1	očnice
</s>
</p>
<p>
<s>
ostřící	ostřící	k2eAgNnSc4d1	ostřící
kolečko	kolečko	k1gNnSc4	kolečko
</s>
</p>
<p>
<s>
režisérský	režisérský	k2eAgInSc1d1	režisérský
hledáček	hledáček	k1gInSc1	hledáček
</s>
</p>
<p>
<s>
===	===	k?	===
Ostatní	ostatní	k2eAgNnPc4d1	ostatní
===	===	k?	===
</s>
</p>
<p>
<s>
kamerové	kamerový	k2eAgFnPc1d1	kamerová
baterie	baterie	k1gFnPc1	baterie
</s>
</p>
<p>
<s>
montážní	montážní	k2eAgInPc1d1	montážní
úchyty	úchyt	k1gInPc1	úchyt
na	na	k7c4	na
stativ	stativ	k1gInSc4	stativ
a	a	k8xC	a
stativové	stativový	k2eAgFnPc4d1	stativová
hlavy	hlava	k1gFnPc4	hlava
</s>
</p>
<p>
<s>
náhledové	náhledový	k2eAgInPc1d1	náhledový
monitory	monitor	k1gInPc1	monitor
</s>
</p>
<p>
<s>
pláštěnky	pláštěnka	k1gFnPc1	pláštěnka
a	a	k8xC	a
ochranné	ochranný	k2eAgFnPc1d1	ochranná
clony	clona	k1gFnPc1	clona
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
filmový	filmový	k2eAgInSc1d1	filmový
pás	pás	k1gInSc1	pás
</s>
</p>
<p>
<s>
filmová	filmový	k2eAgFnSc1d1	filmová
promítačka	promítačka	k1gFnSc1	promítačka
-	-	kIx~	-
přesněji	přesně	k6eAd2	přesně
promítací	promítací	k2eAgInSc1d1	promítací
stroj	stroj	k1gInSc1	stroj
</s>
</p>
<p>
<s>
televizní	televizní	k2eAgFnSc1d1	televizní
kamera	kamera	k1gFnSc1	kamera
</s>
</p>
<p>
<s>
filmový	filmový	k2eAgInSc1d1	filmový
jeřáb	jeřáb	k1gInSc1	jeřáb
</s>
</p>
<p>
<s>
fotografická	fotografický	k2eAgFnSc1d1	fotografická
kamera	kamera	k1gFnSc1	kamera
</s>
</p>
<p>
<s>
triková	trikový	k2eAgFnSc1d1	triková
kamera	kamera	k1gFnSc1	kamera
</s>
</p>
<p>
<s>
časosběrná	časosběrný	k2eAgFnSc1d1	časosběrná
kamera	kamera	k1gFnSc1	kamera
</s>
</p>
<p>
<s>
==	==	k?	==
Zdroje	zdroj	k1gInPc1	zdroj
==	==	k?	==
</s>
</p>
<p>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
:	:	kIx,	:
Filmová	filmový	k2eAgFnSc1d1	filmová
kamera	kamera	k1gFnSc1	kamera
</s>
</p>
<p>
<s>
Filmové	filmový	k2eAgFnPc1d1	filmová
kamery	kamera	k1gFnPc1	kamera
a	a	k8xC	a
řemeslo	řemeslo	k1gNnSc1	řemeslo
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
