<s>
Filmová	filmový	k2eAgFnSc1d1	filmová
kamera	kamera	k1gFnSc1	kamera
je	být	k5eAaImIp3nS	být
technické	technický	k2eAgNnSc4d1	technické
optické	optický	k2eAgNnSc4d1	optické
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
zachytit	zachytit	k5eAaPmF	zachytit
obrazy	obraz	k1gInPc4	obraz
pro	pro	k7c4	pro
kinematografii	kinematografie	k1gFnSc4	kinematografie
<g/>
,	,	kIx,	,
televizi	televize	k1gFnSc4	televize
apod.	apod.	kA	apod.
</s>
