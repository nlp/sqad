<s>
Hra	hra	k1gFnSc1	hra
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
podobě	podoba	k1gFnSc6	podoba
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jejího	její	k3xOp3gMnSc4	její
předchůdce	předchůdce	k1gMnSc4	předchůdce
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
již	již	k6eAd1	již
ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc1	tisíciletí
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
<g/>
.	.	kIx.	.
</s>
