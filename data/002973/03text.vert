<s>
Kudlanka	kudlanka	k1gFnSc1	kudlanka
nábožná	nábožný	k2eAgFnSc1d1	nábožná
(	(	kIx(	(
<g/>
Mantis	mantisa	k1gFnPc2	mantisa
religiosa	religiosa	k1gFnSc1	religiosa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgMnSc1d1	jediný
český	český	k2eAgMnSc1d1	český
zástupce	zástupce	k1gMnSc1	zástupce
řádu	řád	k1gInSc2	řád
kudlanek	kudlanka	k1gFnPc2	kudlanka
(	(	kIx(	(
<g/>
Mantodea	Mantodea	k1gFnSc1	Mantodea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kudlanka	kudlanka	k1gFnSc1	kudlanka
nábožná	nábožný	k2eAgFnSc1d1	nábožná
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
palearktické	palearktický	k2eAgFnSc6d1	palearktická
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
hojná	hojný	k2eAgFnSc1d1	hojná
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
suchých	suchý	k2eAgFnPc6d1	suchá
stepních	stepní	k2eAgFnPc6d1	stepní
lokalitách	lokalita	k1gFnPc6	lokalita
jsou	být	k5eAaImIp3nP	být
hojné	hojný	k2eAgFnPc1d1	hojná
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
nebyly	být	k5eNaImAgFnP	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
kudlanky	kudlanka	k1gFnSc2	kudlanka
příliš	příliš	k6eAd1	příliš
rozšířené	rozšířený	k2eAgFnPc4d1	rozšířená
<g/>
,	,	kIx,	,
vyskytovaly	vyskytovat	k5eAaImAgInP	vyskytovat
se	se	k3xPyFc4	se
hojně	hojně	k6eAd1	hojně
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
Jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
(	(	kIx(	(
<g/>
NP	NP	kA	NP
Podyjí	Podyjí	k1gNnSc4	Podyjí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
oteplování	oteplování	k1gNnSc2	oteplování
podnebí	podnebí	k1gNnSc2	podnebí
se	se	k3xPyFc4	se
rozšiřují	rozšiřovat	k5eAaImIp3nP	rozšiřovat
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Viděny	viděn	k2eAgInPc1d1	viděn
byly	být	k5eAaImAgInP	být
dokonce	dokonce	k9	dokonce
i	i	k9	i
na	na	k7c6	na
Ostravsku	Ostravsko	k1gNnSc6	Ostravsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
září	září	k1gNnSc6	září
2008	[number]	k4	2008
a	a	k8xC	a
říjnu	říjen	k1gInSc3	říjen
2015	[number]	k4	2015
v	v	k7c6	v
podhůří	podhůří	k1gNnSc6	podhůří
Jeseníků	Jeseník	k1gInPc2	Jeseník
(	(	kIx(	(
<g/>
Šternberk	Šternberk	k1gInSc1	Šternberk
<g/>
)	)	kIx)	)
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2011	[number]	k4	2011
v	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
Na	na	k7c6	na
Plachtě	plachta	k1gFnSc6	plachta
u	u	k7c2	u
Hradce	Hradec	k1gInSc2	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
..	..	k?	..
Mapování	mapování	k1gNnSc1	mapování
výskytu	výskyt	k1gInSc2	výskyt
kudlanky	kudlanka	k1gFnSc2	kudlanka
nábožné	nábožný	k2eAgFnSc2d1	nábožná
v	v	k7c6	v
ČR	ČR	kA	ČR
probíhá	probíhat	k5eAaImIp3nS	probíhat
na	na	k7c6	na
serveru	server	k1gInSc6	server
Biolib	Biolib	k1gInSc1	Biolib
<g/>
.	.	kIx.	.
</s>
<s>
Kudlanka	kudlanka	k1gFnSc1	kudlanka
nábožná	nábožný	k2eAgFnSc1d1	nábožná
je	být	k5eAaImIp3nS	být
zbarvena	zbarven	k2eAgFnSc1d1	zbarvena
světle	světle	k6eAd1	světle
zeleně	zeleně	k6eAd1	zeleně
<g/>
,	,	kIx,	,
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
hnědě	hnědě	k6eAd1	hnědě
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomuto	tento	k3xDgNnSc3	tento
zbarvení	zbarvení	k1gNnSc3	zbarvení
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
může	moct	k5eAaImIp3nS	moct
predátor	predátor	k1gMnSc1	predátor
i	i	k8xC	i
vášnivý	vášnivý	k2eAgMnSc1d1	vášnivý
entomolog	entomolog	k1gMnSc1	entomolog
snadno	snadno	k6eAd1	snadno
přehlédnout	přehlédnout	k5eAaPmF	přehlédnout
<g/>
.	.	kIx.	.
</s>
<s>
Pronotum	Pronotum	k1gNnSc1	Pronotum
(	(	kIx(	(
<g/>
první	první	k4xOgInSc4	první
hrudní	hrudní	k2eAgInSc4d1	hrudní
článek	článek	k1gInSc4	článek
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
prodloužen	prodloužen	k2eAgMnSc1d1	prodloužen
<g/>
,	,	kIx,	,
hlava	hlava	k1gFnSc1	hlava
je	být	k5eAaImIp3nS	být
trojúhelníková	trojúhelníkový	k2eAgFnSc1d1	trojúhelníková
s	s	k7c7	s
velkýma	velký	k2eAgNnPc7d1	velké
očima	oko	k1gNnPc7	oko
(	(	kIx(	(
<g/>
typické	typický	k2eAgNnSc1d1	typické
pro	pro	k7c4	pro
predátory	predátor	k1gMnPc4	predátor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
zaujme	zaujmout	k5eAaPmIp3nS	zaujmout
přední	přední	k2eAgInSc1d1	přední
pár	pár	k4xCyI	pár
končetin	končetina	k1gFnPc2	končetina
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
uzpůsoben	uzpůsobit	k5eAaPmNgMnS	uzpůsobit
k	k	k7c3	k
uchvacování	uchvacování	k1gNnSc3	uchvacování
kořisti	kořist	k1gFnSc2	kořist
-	-	kIx~	-
loupeživé	loupeživý	k2eAgFnSc2d1	loupeživá
končetiny	končetina	k1gFnSc2	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Fungují	fungovat	k5eAaImIp3nP	fungovat
jako	jako	k8xC	jako
střenka	střenka	k1gFnSc1	střenka
zavíracího	zavírací	k2eAgInSc2d1	zavírací
nože	nůž	k1gInSc2	nůž
<g/>
,	,	kIx,	,
kořist	kořist	k1gFnSc4	kořist
je	být	k5eAaImIp3nS	být
bleskovým	bleskový	k2eAgNnSc7d1	bleskové
sklapnutím	sklapnutí	k1gNnSc7	sklapnutí
přivřena	přivřít	k5eAaPmNgFnS	přivřít
mezi	mezi	k7c4	mezi
ozubenou	ozubený	k2eAgFnSc4d1	ozubená
holeň	holeň	k1gFnSc4	holeň
(	(	kIx(	(
<g/>
tibia	tibia	k1gFnSc1	tibia
<g/>
)	)	kIx)	)
a	a	k8xC	a
chodidlo	chodidlo	k1gNnSc1	chodidlo
(	(	kIx(	(
<g/>
tarsus	tarsus	k1gInSc1	tarsus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stehně	stehno	k1gNnSc6	stehno
(	(	kIx(	(
<g/>
femur	femur	k1gMnSc1	femur
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
černé	černý	k2eAgFnPc1d1	černá
aposematické	aposematický	k2eAgFnPc1d1	aposematický
skvrny	skvrna	k1gFnPc1	skvrna
sloužící	sloužící	k2eAgFnPc1d1	sloužící
k	k	k7c3	k
zastrašení	zastrašení	k1gNnSc3	zastrašení
predátorů	predátor	k1gMnPc2	predátor
(	(	kIx(	(
<g/>
připomínají	připomínat	k5eAaImIp3nP	připomínat
oči	oko	k1gNnPc4	oko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přední	přední	k2eAgMnPc1d1	přední
pár	pár	k4xCyI	pár
křídel	křídlo	k1gNnPc2	křídlo
je	být	k5eAaImIp3nS	být
kožovitý	kožovitý	k2eAgInSc1d1	kožovitý
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
u	u	k7c2	u
švábů	šváb	k1gMnPc2	šváb
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zadní	zadní	k2eAgInSc1d1	zadní
je	být	k5eAaImIp3nS	být
krytý	krytý	k2eAgInSc1d1	krytý
pod	pod	k7c7	pod
nimi	on	k3xPp3gMnPc7	on
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
blanitý	blanitý	k2eAgMnSc1d1	blanitý
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
kudlanek	kudlanka	k1gFnPc2	kudlanka
létají	létat	k5eAaImIp3nP	létat
hlavně	hlavně	k9	hlavně
samci	samec	k1gMnPc1	samec
-	-	kIx~	-
jsou	být	k5eAaImIp3nP	být
menší	malý	k2eAgFnPc1d2	menší
-	-	kIx~	-
a	a	k8xC	a
i	i	k9	i
ti	ten	k3xDgMnPc1	ten
jen	jen	k9	jen
neradi	nerad	k2eAgMnPc1d1	nerad
<g/>
.	.	kIx.	.
</s>
<s>
Samička	samička	k1gFnSc1	samička
dokáže	dokázat	k5eAaPmIp3nS	dokázat
přeletět	přeletět	k5eAaPmF	přeletět
jen	jen	k9	jen
pár	pár	k4xCyI	pár
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
ní	on	k3xPp3gFnSc3	on
sameček	sameček	k1gMnSc1	sameček
až	až	k9	až
pár	pár	k4xCyI	pár
desítek	desítka	k1gFnPc2	desítka
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
především	především	k9	především
velikostí	velikost	k1gFnSc7	velikost
a	a	k8xC	a
hmotností	hmotnost	k1gFnSc7	hmotnost
samičky	samička	k1gFnSc2	samička
<g/>
.	.	kIx.	.
</s>
<s>
Kudlanky	kudlanka	k1gFnPc4	kudlanka
jsou	být	k5eAaImIp3nP	být
dravci	dravec	k1gMnPc1	dravec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
dobře	dobře	k6eAd1	dobře
využívají	využívat	k5eAaPmIp3nP	využívat
maskování	maskování	k1gNnSc4	maskování
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
maskované	maskovaný	k2eAgFnPc1d1	maskovaná
a	a	k8xC	a
dokáží	dokázat	k5eAaPmIp3nP	dokázat
dlouho	dlouho	k6eAd1	dlouho
číhat	číhat	k5eAaImF	číhat
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
kořist	kořist	k1gFnSc4	kořist
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
drobný	drobný	k2eAgInSc4d1	drobný
hmyz	hmyz	k1gInSc4	hmyz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
vystřelí	vystřelit	k5eAaPmIp3nS	vystřelit
své	svůj	k3xOyFgFnPc4	svůj
loupeživé	loupeživý	k2eAgFnPc4d1	loupeživá
nohy	noha	k1gFnPc4	noha
<g/>
,	,	kIx,	,
zachytí	zachytit	k5eAaPmIp3nP	zachytit
kořist	kořist	k1gFnSc4	kořist
a	a	k8xC	a
zaživa	zaživa	k6eAd1	zaživa
ji	on	k3xPp3gFnSc4	on
sežerou	sežrat	k5eAaPmIp3nP	sežrat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
kudlanek	kudlanka	k1gFnPc2	kudlanka
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
manželský	manželský	k2eAgInSc4d1	manželský
kanibalismus	kanibalismus	k1gInSc4	kanibalismus
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
samička	samička	k1gFnSc1	samička
po	po	k7c6	po
páření	páření	k1gNnSc6	páření
sežere	sežrat	k5eAaPmIp3nS	sežrat
samečka	sameček	k1gMnSc4	sameček
<g/>
.	.	kIx.	.
</s>
<s>
Děje	dít	k5eAaImIp3nS	dít
se	se	k3xPyFc4	se
tak	tak	k9	tak
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
době	doba	k1gFnSc6	doba
nedostatku	nedostatek	k1gInSc2	nedostatek
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
v	v	k7c6	v
pokusech	pokus	k1gInPc6	pokus
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
jevu	jev	k1gInSc3	jev
došlo	dojít	k5eAaPmAgNnS	dojít
zřídkakdy	zřídkakdy	k6eAd1	zřídkakdy
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
řídící	řídící	k2eAgNnSc1d1	řídící
rozmnožování	rozmnožování	k1gNnSc1	rozmnožování
je	být	k5eAaImIp3nS	být
podřízené	podřízený	k1gMnPc4	podřízený
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
když	když	k8xS	když
samička	samička	k1gFnSc1	samička
začne	začít	k5eAaPmIp3nS	začít
žrát	žrát	k5eAaImF	žrát
samečka	sameček	k1gMnSc4	sameček
od	od	k7c2	od
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
kopulace	kopulace	k1gFnSc1	kopulace
neustává	ustávat	k5eNaImIp3nS	ustávat
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
probíhá	probíhat	k5eAaImIp3nS	probíhat
ještě	ještě	k6eAd1	ještě
vytrvaleji	vytrvale	k6eAd2	vytrvale
<g/>
.	.	kIx.	.
</s>
<s>
Vajíčka	vajíčko	k1gNnPc1	vajíčko
jsou	být	k5eAaImIp3nP	být
ukryta	ukryt	k2eAgNnPc1d1	ukryto
v	v	k7c6	v
ootéce	ootéka	k1gFnSc6	ootéka
<g/>
,	,	kIx,	,
tuhém	tuhý	k2eAgInSc6d1	tuhý
obalu	obal	k1gInSc6	obal
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
přezimuje	přezimovat	k5eAaBmIp3nS	přezimovat
<g/>
.	.	kIx.	.
</s>
<s>
Kudlanka	kudlanka	k1gFnSc1	kudlanka
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
Hemimetabola	Hemimetabola	k1gFnSc1	Hemimetabola
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
hmyz	hmyz	k1gInSc1	hmyz
s	s	k7c7	s
proměnou	proměna	k1gFnSc7	proměna
nedokonalou	dokonalý	k2eNgFnSc7d1	nedokonalá
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yIgInSc2	který
neexistuje	existovat	k5eNaImIp3nS	existovat
stádium	stádium	k1gNnSc1	stádium
kukly	kukla	k1gFnSc2	kukla
(	(	kIx(	(
<g/>
pupa	pupa	k1gFnSc1	pupa
<g/>
)	)	kIx)	)
a	a	k8xC	a
larva	larva	k1gFnSc1	larva
(	(	kIx(	(
<g/>
nymfa	nymfa	k1gFnSc1	nymfa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
podobná	podobný	k2eAgFnSc1d1	podobná
dospělci	dospělec	k1gMnPc7	dospělec
(	(	kIx(	(
<g/>
imagu	imaga	k1gFnSc4	imaga
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
nemá	mít	k5eNaImIp3nS	mít
vyvinutá	vyvinutý	k2eAgNnPc4d1	vyvinuté
křídla	křídlo	k1gNnPc4	křídlo
a	a	k8xC	a
rozmnožovací	rozmnožovací	k2eAgInPc4d1	rozmnožovací
orgány	orgán	k1gInPc4	orgán
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
dospějí	dochvít	k5eAaPmIp3nP	dochvít
po	po	k7c6	po
posledním	poslední	k2eAgNnSc6d1	poslední
svlečení	svlečení	k1gNnSc6	svlečení
(	(	kIx(	(
<g/>
ekdyzi	ekdyze	k1gFnSc6	ekdyze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kudlanka	kudlanka	k1gFnSc1	kudlanka
nábožná	nábožný	k2eAgFnSc1d1	nábožná
je	být	k5eAaImIp3nS	být
výjimečná	výjimečný	k2eAgFnSc1d1	výjimečná
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jako	jako	k9	jako
jediný	jediný	k2eAgInSc1d1	jediný
druh	druh	k1gInSc1	druh
kudlanek	kudlanka	k1gFnPc2	kudlanka
dokáže	dokázat	k5eAaPmIp3nS	dokázat
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
hnula	hnout	k5eAaPmAgFnS	hnout
tělem	tělo	k1gNnSc7	tělo
<g/>
,	,	kIx,	,
otočit	otočit	k5eAaPmF	otočit
hlavu	hlava	k1gFnSc4	hlava
o	o	k7c4	o
360	[number]	k4	360
stupňů	stupeň	k1gInPc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Samička	samička	k1gFnSc1	samička
se	se	k3xPyFc4	se
dožívá	dožívat	k5eAaImIp3nS	dožívat
7-8	[number]	k4	7-8
měsíců	měsíc	k1gInPc2	měsíc
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
sameček	sameček	k1gMnSc1	sameček
pouze	pouze	k6eAd1	pouze
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Kudlanka	kudlanka	k1gFnSc1	kudlanka
je	být	k5eAaImIp3nS	být
také	také	k9	také
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
druhů	druh	k1gInPc2	druh
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dokáže	dokázat	k5eAaPmIp3nS	dokázat
vnímat	vnímat	k5eAaImF	vnímat
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Křídla	křídlo	k1gNnPc1	křídlo
používají	používat	k5eAaImIp3nP	používat
kudlanky	kudlanka	k1gFnPc1	kudlanka
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
ohrožení	ohrožení	k1gNnSc6	ohrožení
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	on	k3xPp3gNnPc4	on
pronásleduje	pronásledovat	k5eAaImIp3nS	pronásledovat
predátor	predátor	k1gMnSc1	predátor
se	s	k7c7	s
kterým	který	k3yIgInSc7	který
nechtějí	chtít	k5eNaImIp3nP	chtít
bojovat	bojovat	k5eAaImF	bojovat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
drobní	drobný	k2eAgMnPc1d1	drobný
hlodavci	hlodavec	k1gMnPc1	hlodavec
<g/>
,	,	kIx,	,
kočky	kočka	k1gFnPc1	kočka
<g/>
,	,	kIx,	,
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
při	při	k7c6	při
tzv.	tzv.	kA	tzv.
dálkovém	dálkový	k2eAgNnSc6d1	dálkové
cestování	cestování	k1gNnSc6	cestování
za	za	k7c7	za
potravou	potrava	k1gFnSc7	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Samička	samička	k1gFnSc1	samička
má	mít	k5eAaImIp3nS	mít
bříško	bříško	k1gNnSc4	bříško
složené	složený	k2eAgFnSc2d1	složená
ze	z	k7c2	z
šesti	šest	k4xCc2	šest
článků	článek	k1gInPc2	článek
<g/>
,	,	kIx,	,
sameček	sameček	k1gMnSc1	sameček
má	mít	k5eAaImIp3nS	mít
článků	článek	k1gInPc2	článek
osm	osm	k4xCc1	osm
<g/>
.	.	kIx.	.
</s>
<s>
Kudlanka	kudlanka	k1gFnSc1	kudlanka
vypouští	vypouštět	k5eAaImIp3nS	vypouštět
na	na	k7c4	na
tělo	tělo	k1gNnSc4	tělo
oběti	oběť	k1gFnSc2	oběť
sliny	slina	k1gFnSc2	slina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
lokálně	lokálně	k6eAd1	lokálně
umrtví	umrtvit	k5eAaPmIp3nP	umrtvit
nervovou	nervový	k2eAgFnSc4d1	nervová
soustavu	soustava	k1gFnSc4	soustava
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
kyselina	kyselina	k1gFnSc1	kyselina
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
obsažená	obsažený	k2eAgFnSc1d1	obsažená
naleptá	naleptat	k5eAaPmIp3nS	naleptat
potravu	potrava	k1gFnSc4	potrava
do	do	k7c2	do
jakési	jakýsi	k3yIgFnSc2	jakýsi
kaše	kaše	k1gFnSc2	kaše
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
pak	pak	k6eAd1	pak
kudlanka	kudlanka	k1gFnSc1	kudlanka
konzumuje	konzumovat	k5eAaBmIp3nS	konzumovat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
oběť	oběť	k1gFnSc1	oběť
při	při	k7c6	při
požírání	požírání	k1gNnSc6	požírání
hýbe	hýbat	k5eAaImIp3nS	hýbat
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
jí	on	k3xPp3gFnSc3	on
již	již	k6eAd1	již
chybí	chybět	k5eAaImIp3nS	chybět
i	i	k9	i
větší	veliký	k2eAgFnSc4d2	veliký
část	část	k1gFnSc4	část
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Samečků	sameček	k1gMnPc2	sameček
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
podstatně	podstatně	k6eAd1	podstatně
méně	málo	k6eAd2	málo
než	než	k8xS	než
samiček	samička	k1gFnPc2	samička
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kudlanka	kudlanka	k1gFnSc1	kudlanka
nábožná	nábožný	k2eAgFnSc1d1	nábožná
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
Kudlanka	kudlanka	k1gFnSc1	kudlanka
nábožná	nábožný	k2eAgFnSc1d1	nábožná
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
