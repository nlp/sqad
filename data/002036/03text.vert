<s>
Metallica	Metallica	k1gFnSc1	Metallica
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
metalová	metalový	k2eAgFnSc1d1	metalová
skupina	skupina	k1gFnSc1	skupina
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
první	první	k4xOgNnPc4	první
alba	album	k1gNnPc4	album
Metallicy	Metallica	k1gFnSc2	Metallica
(	(	kIx(	(
<g/>
DAWE	DAWE	kA	DAWE
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
typická	typický	k2eAgNnPc1d1	typické
rychlejší	rychlý	k2eAgNnPc1d2	rychlejší
tempa	tempo	k1gNnPc1	tempo
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
agresivní	agresivní	k2eAgMnSc1d1	agresivní
a	a	k8xC	a
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
stylu	styl	k1gInSc6	styl
i	i	k9	i
hudebně	hudebně	k6eAd1	hudebně
náročnější	náročný	k2eAgFnPc4d2	náročnější
kompozice	kompozice	k1gFnPc4	kompozice
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
je	být	k5eAaImIp3nS	být
zařadila	zařadit	k5eAaPmAgFnS	zařadit
společně	společně	k6eAd1	společně
se	s	k7c7	s
skupinami	skupina	k1gFnPc7	skupina
Megadeth	Megadetha	k1gFnPc2	Megadetha
<g/>
,	,	kIx,	,
Slayer	Slayra	k1gFnPc2	Slayra
a	a	k8xC	a
Anthrax	Anthrax	k1gInSc1	Anthrax
do	do	k7c2	do
"	"	kIx"	"
<g/>
Velké	velký	k2eAgFnSc2d1	velká
čtyřky	čtyřka	k1gFnSc2	čtyřka
<g/>
"	"	kIx"	"
thrash	thrash	k1gInSc1	thrash
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
dala	dát	k5eAaPmAgFnS	dát
dohromady	dohromady	k6eAd1	dohromady
na	na	k7c6	na
základě	základ	k1gInSc6	základ
inzerátu	inzerát	k1gInSc2	inzerát
v	v	k7c6	v
losangeleských	losangeleský	k2eAgFnPc6d1	losangeleská
novinách	novina	k1gFnPc6	novina
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
podal	podat	k5eAaPmAgInS	podat
bubeník	bubeník	k1gMnSc1	bubeník
Lars	Larsa	k1gFnPc2	Larsa
Ulrich	Ulrich	k1gMnSc1	Ulrich
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
hraje	hrát	k5eAaImIp3nS	hrát
sólový	sólový	k2eAgMnSc1d1	sólový
kytarista	kytarista	k1gMnSc1	kytarista
Kirk	Kirk	k1gMnSc1	Kirk
Hammett	Hammett	k1gMnSc1	Hammett
(	(	kIx(	(
<g/>
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
přidal	přidat	k5eAaPmAgMnS	přidat
ke	k	k7c3	k
skupině	skupina	k1gFnSc3	skupina
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
basový	basový	k2eAgMnSc1d1	basový
kytarista	kytarista	k1gMnSc1	kytarista
Robert	Robert	k1gMnSc1	Robert
Trujillo	Trujillo	k1gNnSc4	Trujillo
(	(	kIx(	(
<g/>
členem	člen	k1gInSc7	člen
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
zakládajícím	zakládající	k2eAgMnSc7d1	zakládající
bubeníkem	bubeník	k1gMnSc7	bubeník
Larsem	Lars	k1gMnSc7	Lars
Ulrichem	Ulrich	k1gMnSc7	Ulrich
a	a	k8xC	a
pak	pak	k9	pak
zpěvákem	zpěvák	k1gMnSc7	zpěvák
a	a	k8xC	a
doprovodným	doprovodný	k2eAgMnSc7d1	doprovodný
kytaristou	kytarista	k1gMnSc7	kytarista
Jamesem	James	k1gMnSc7	James
Hetfieldem	Hetfield	k1gMnSc7	Hetfield
<g/>
.	.	kIx.	.
</s>
<s>
Předchozí	předchozí	k2eAgMnPc1d1	předchozí
členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
byli	být	k5eAaImAgMnP	být
sólový	sólový	k2eAgMnSc1d1	sólový
kytarista	kytarista	k1gMnSc1	kytarista
Dave	Dav	k1gInSc5	Dav
Mustaine	Mustain	k1gInSc5	Mustain
(	(	kIx(	(
<g/>
který	který	k3yRgMnSc1	který
odešel	odejít	k5eAaPmAgMnS	odejít
a	a	k8xC	a
založil	založit	k5eAaPmAgMnS	založit
skupinu	skupina	k1gFnSc4	skupina
Megadeth	Megadetha	k1gFnPc2	Megadetha
<g/>
)	)	kIx)	)
a	a	k8xC	a
basisté	basista	k1gMnPc1	basista
Ron	Ron	k1gMnSc1	Ron
McGovney	McGovnea	k1gFnPc4	McGovnea
<g/>
,	,	kIx,	,
Cliff	Cliff	k1gInSc1	Cliff
Burton	Burton	k1gInSc1	Burton
a	a	k8xC	a
Jason	Jason	k1gMnSc1	Jason
Newsted	Newsted	k1gMnSc1	Newsted
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
také	také	k9	také
dlouho	dlouho	k6eAd1	dlouho
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
s	s	k7c7	s
producentem	producent	k1gMnSc7	producent
Bobem	Bob	k1gMnSc7	Bob
Rockem	rock	k1gInSc7	rock
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
produkoval	produkovat	k5eAaImAgMnS	produkovat
všechna	všechen	k3xTgNnPc4	všechen
alba	album	k1gNnPc4	album
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
a	a	k8xC	a
pomáhal	pomáhat	k5eAaImAgInS	pomáhat
také	také	k9	také
jako	jako	k9	jako
dočasný	dočasný	k2eAgMnSc1d1	dočasný
baskytarista	baskytarista	k1gMnSc1	baskytarista
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
Jasona	Jason	k1gMnSc2	Jason
Newsteda	Newsted	k1gMnSc2	Newsted
do	do	k7c2	do
příchodu	příchod	k1gInSc2	příchod
Trujilla	Trujillo	k1gNnSc2	Trujillo
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
si	se	k3xPyFc3	se
dokázala	dokázat	k5eAaPmAgFnS	dokázat
postupně	postupně	k6eAd1	postupně
získávat	získávat	k5eAaImF	získávat
stále	stále	k6eAd1	stále
větší	veliký	k2eAgNnSc4d2	veliký
a	a	k8xC	a
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
fanoušků	fanoušek	k1gMnPc2	fanoušek
v	v	k7c6	v
undergroundu	underground	k1gInSc6	underground
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
přízeň	přízeň	k1gFnSc4	přízeň
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
kritici	kritik	k1gMnPc1	kritik
považují	považovat	k5eAaImIp3nP	považovat
album	album	k1gNnSc4	album
Master	master	k1gMnSc1	master
of	of	k?	of
Puppets	Puppets	k1gInSc1	Puppets
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
za	za	k7c4	za
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgNnPc2d3	nejvýznamnější
metalových	metalový	k2eAgNnPc2d1	metalové
alb	album	k1gNnPc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
výrazný	výrazný	k2eAgInSc4d1	výrazný
komerční	komerční	k2eAgInSc4d1	komerční
úspěch	úspěch	k1gInSc4	úspěch
s	s	k7c7	s
pátým	pátý	k4xOgNnSc7	pátý
albem	album	k1gNnSc7	album
Metallica	Metallic	k1gInSc2	Metallic
(	(	kIx(	(
<g/>
známým	známý	k2eAgInPc3d1	známý
také	také	k9	také
jako	jako	k9	jako
Black	Black	k1gInSc1	Black
Album	album	k1gNnSc1	album
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vydaném	vydaný	k2eAgInSc6d1	vydaný
roku	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
debutovalo	debutovat	k5eAaBmAgNnS	debutovat
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc1	billboard
200	[number]	k4	200
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
albem	album	k1gNnSc7	album
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
počala	počnout	k5eAaPmAgFnS	počnout
ubírat	ubírat	k5eAaImF	ubírat
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
střednímu	střední	k2eAgInSc3d1	střední
proudu	proud	k1gInSc3	proud
<g/>
.	.	kIx.	.
</s>
<s>
Celosvětového	celosvětový	k2eAgInSc2d1	celosvětový
ohlasu	ohlas	k1gInSc2	ohlas
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
singly	singl	k1gInPc1	singl
"	"	kIx"	"
<g/>
Fade	Fade	k1gFnSc1	Fade
to	ten	k3xDgNnSc4	ten
Black	Black	k1gMnSc1	Black
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Master	master	k1gMnSc1	master
of	of	k?	of
Puppets	Puppets	k1gInSc1	Puppets
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Welcome	Welcom	k1gInSc5	Welcom
Home	Home	k1gNnSc1	Home
(	(	kIx(	(
<g/>
Sanitarium	Sanitarium	k1gNnSc1	Sanitarium
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
One	One	k1gFnSc1	One
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Enter	Enter	k1gMnSc1	Enter
Sandman	Sandman	k1gMnSc1	Sandman
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Unforgiven	Unforgiven	k2eAgMnSc1d1	Unforgiven
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Nothing	Nothing	k1gInSc4	Nothing
Else	Elsa	k1gFnSc3	Elsa
Matters	Matters	k1gInSc1	Matters
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Fuel	Fuel	k1gInSc1	Fuel
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Whiskey	Whiskea	k1gFnSc2	Whiskea
in	in	k?	in
the	the	k?	the
Jar	jar	k1gInSc1	jar
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
Metallica	Metallic	k1gInSc2	Metallic
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
několika	několik	k4yIc7	několik
dalšími	další	k2eAgMnPc7d1	další
umělci	umělec	k1gMnPc7	umělec
<g/>
,	,	kIx,	,
zažalovala	zažalovat	k5eAaPmAgFnS	zažalovat
společnost	společnost	k1gFnSc1	společnost
Napster	Napstra	k1gFnPc2	Napstra
za	za	k7c4	za
sdílení	sdílení	k1gNnSc4	sdílení
materiálu	materiál	k1gInSc2	materiál
chráněného	chráněný	k2eAgNnSc2d1	chráněné
autorskými	autorský	k2eAgNnPc7d1	autorské
právy	právo	k1gNnPc7	právo
bez	bez	k7c2	bez
souhlasu	souhlas	k1gInSc2	souhlas
členů	člen	k1gMnPc2	člen
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Spor	spor	k1gInSc1	spor
se	se	k3xPyFc4	se
skončil	skončit	k5eAaPmAgInS	skončit
dohodou	dohoda	k1gFnSc7	dohoda
a	a	k8xC	a
z	z	k7c2	z
Napsteru	Napster	k1gInSc2	Napster
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
placená	placený	k2eAgFnSc1d1	placená
služba	služba	k1gFnSc1	služba
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
St.	st.	kA	st.
Anger	Angra	k1gFnPc2	Angra
<g/>
,	,	kIx,	,
vydané	vydaný	k2eAgFnPc4d1	vydaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
navzdory	navzdory	k7c3	navzdory
debutu	debut	k1gInSc3	debut
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc1	billboard
200	[number]	k4	200
<g/>
,	,	kIx,	,
zklamalo	zklamat	k5eAaPmAgNnS	zklamat
některé	některý	k3yIgMnPc4	některý
fanoušky	fanoušek	k1gMnPc4	fanoušek
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
chyběla	chybět	k5eAaImAgNnP	chybět
kytarová	kytarový	k2eAgNnPc1d1	kytarové
sóla	sólo	k1gNnPc1	sólo
a	a	k8xC	a
vadily	vadit	k5eAaImAgFnP	vadit
"	"	kIx"	"
<g/>
plechové	plechový	k2eAgFnPc1d1	plechová
<g/>
"	"	kIx"	"
bicí	bicí	k2eAgFnPc1d1	bicí
<g/>
.	.	kIx.	.
</s>
<s>
Dokument	dokument	k1gInSc1	dokument
s	s	k7c7	s
názvem	název	k1gInSc7	název
Some	Some	k1gFnSc1	Some
Kind	Kind	k1gMnSc1	Kind
of	of	k?	of
Monster	monstrum	k1gNnPc2	monstrum
přiblížil	přiblížit	k5eAaPmAgInS	přiblížit
fanouškům	fanoušek	k1gMnPc3	fanoušek
proces	proces	k1gInSc4	proces
nahrávání	nahrávání	k1gNnSc4	nahrávání
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Metallica	Metallica	k1gFnSc1	Metallica
doposud	doposud	k6eAd1	doposud
vydala	vydat	k5eAaPmAgFnS	vydat
deset	deset	k4xCc4	deset
studiových	studiový	k2eAgFnPc2d1	studiová
alb	alba	k1gFnPc2	alba
<g/>
,	,	kIx,	,
čtyři	čtyři	k4xCgNnPc4	čtyři
živá	živý	k2eAgNnPc4d1	živé
alba	album	k1gNnPc4	album
<g/>
,	,	kIx,	,
šest	šest	k4xCc1	šest
EP	EP	kA	EP
<g/>
,	,	kIx,	,
dvacet	dvacet	k4xCc4	dvacet
pět	pět	k4xCc4	pět
videoklipů	videoklip	k1gInPc2	videoklip
a	a	k8xC	a
šestačtyřicet	šestačtyřicet	k4xCc4	šestačtyřicet
singlů	singl	k1gInPc2	singl
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
devět	devět	k4xCc4	devět
cen	cena	k1gFnPc2	cena
Grammy	Gramma	k1gFnSc2	Gramma
a	a	k8xC	a
jako	jako	k9	jako
jediné	jediný	k2eAgFnSc3d1	jediná
kapele	kapela	k1gFnSc3	kapela
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
podařilo	podařit	k5eAaPmAgNnS	podařit
bodovat	bodovat	k5eAaImF	bodovat
v	v	k7c6	v
albovém	albový	k2eAgNnSc6d1	albové
žebříčku	žebříčko	k1gNnSc6	žebříčko
Billboard	billboard	k1gInSc1	billboard
200	[number]	k4	200
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
s	s	k7c7	s
pěti	pět	k4xCc7	pět
po	po	k7c6	po
sobě	se	k3xPyFc3	se
následujícími	následující	k2eAgNnPc7d1	následující
alby	album	k1gNnPc7	album
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Metallica	Metallic	k1gInSc2	Metallic
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterého	který	k3yRgMnSc4	který
se	se	k3xPyFc4	se
v	v	k7c6	v
USA	USA	kA	USA
prodalo	prodat	k5eAaPmAgNnS	prodat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
15	[number]	k4	15
miliónů	milión	k4xCgInPc2	milión
kopií	kopie	k1gFnPc2	kopie
(	(	kIx(	(
<g/>
22	[number]	k4	22
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
celosvětově	celosvětově	k6eAd1	celosvětově
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
25	[number]	k4	25
<g/>
.	.	kIx.	.
nejprodávanějším	prodávaný	k2eAgNnSc7d3	nejprodávanější
albem	album	k1gNnSc7	album
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
jejich	jejich	k3xOp3gNnSc4	jejich
poslední	poslední	k2eAgNnSc4d1	poslední
album	album	k1gNnSc4	album
Death	Deatha	k1gFnPc2	Deatha
Magnetic	Magnetice	k1gFnPc2	Magnetice
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
prodaných	prodaný	k2eAgFnPc2d1	prodaná
nahrávek	nahrávka	k1gFnPc2	nahrávka
odhadoval	odhadovat	k5eAaImAgInS	odhadovat
na	na	k7c4	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
miliónů	milión	k4xCgInPc2	milión
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
září	září	k1gNnSc3	září
2008	[number]	k4	2008
byla	být	k5eAaImAgFnS	být
Metallica	Metallica	k1gFnSc1	Metallica
pátým	pátý	k4xOgNnSc7	pátý
nejlépe	dobře	k6eAd3	dobře
se	s	k7c7	s
prodávajícím	prodávající	k2eAgMnSc7d1	prodávající
hudebním	hudební	k2eAgMnSc7d1	hudební
interpretem	interpret	k1gMnSc7	interpret
<g/>
,	,	kIx,	,
od	od	k7c2	od
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začal	začít	k5eAaPmAgInS	začít
informační	informační	k2eAgInSc1d1	informační
systém	systém	k1gInSc1	systém
SoundScan	SoundScana	k1gFnPc2	SoundScana
sledovat	sledovat	k5eAaImF	sledovat
prodejnost	prodejnost	k1gFnSc4	prodejnost
nahrávek	nahrávka	k1gFnPc2	nahrávka
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
sledované	sledovaný	k2eAgNnSc4d1	sledované
období	období	k1gNnSc4	období
prodala	prodat	k5eAaPmAgFnS	prodat
Metallica	Metallica	k1gFnSc1	Metallica
jenom	jenom	k9	jenom
v	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
50	[number]	k4	50
111	[number]	k4	111
000	[number]	k4	000
alb	album	k1gNnPc2	album
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
byla	být	k5eAaImAgFnS	být
skupina	skupina	k1gFnSc1	skupina
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
Rock	rock	k1gInSc1	rock
and	and	k?	and
Roll	Roll	k1gInSc1	Roll
Hall	Hall	k1gInSc1	Hall
of	of	k?	of
Fame	Fame	k1gInSc1	Fame
<g/>
.	.	kIx.	.
</s>
<s>
Úvodní	úvodní	k2eAgNnSc4d1	úvodní
slovo	slovo	k1gNnSc4	slovo
ceremoniálu	ceremoniál	k1gInSc2	ceremoniál
přednesl	přednést	k5eAaPmAgMnS	přednést
Flea	Flea	k1gMnSc1	Flea
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
skupina	skupina	k1gFnSc1	skupina
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
novém	nový	k2eAgNnSc6d1	nové
albu	album	k1gNnSc6	album
<g/>
.	.	kIx.	.
</s>
<s>
Fanoušci	Fanoušek	k1gMnPc1	Fanoušek
se	se	k3xPyFc4	se
konečně	konečně	k6eAd1	konečně
po	po	k7c6	po
osmi	osm	k4xCc6	osm
letech	léto	k1gNnPc6	léto
čekání	čekání	k1gNnSc2	čekání
dočkají	dočkat	k5eAaPmIp3nP	dočkat
nového	nový	k2eAgNnSc2d1	nové
alba	album	k1gNnSc2	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
Hardwired	Hardwired	k1gInSc1	Hardwired
<g/>
...	...	k?	...
<g/>
to	ten	k3xDgNnSc1	ten
Self-Destruct	Self-Destruct	k2eAgMnSc1d1	Self-Destruct
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
má	mít	k5eAaImIp3nS	mít
datum	datum	k1gNnSc4	datum
vydání	vydání	k1gNnSc2	vydání
na	na	k7c4	na
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Metallica	Metallica	k1gFnSc1	Metallica
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
,	,	kIx,	,
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
,	,	kIx,	,
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
půlce	půlka	k1gFnSc6	půlka
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
bubeník	bubeník	k1gMnSc1	bubeník
Lars	Larsa	k1gFnPc2	Larsa
Ulrich	Ulrich	k1gMnSc1	Ulrich
napsal	napsat	k5eAaPmAgMnS	napsat
inzerát	inzerát	k1gInSc4	inzerát
do	do	k7c2	do
Los	los	k1gInSc4	los
Angelských	Angelský	k2eAgFnPc2d1	Angelská
novin	novina	k1gFnPc2	novina
The	The	k1gMnSc1	The
Recycler	Recycler	k1gMnSc1	Recycler
<g/>
:	:	kIx,	:
Bubeník	Bubeník	k1gMnSc1	Bubeník
hledá	hledat	k5eAaImIp3nS	hledat
ostatní	ostatní	k2eAgMnPc4d1	ostatní
metalové	metalový	k2eAgMnPc4d1	metalový
hráče	hráč	k1gMnPc4	hráč
<g/>
,	,	kIx,	,
abychom	aby	kYmCp1nP	aby
mohli	moct	k5eAaImAgMnP	moct
jamovat	jamovat	k5eAaPmF	jamovat
na	na	k7c4	na
hudbu	hudba	k1gFnSc4	hudba
skupin	skupina	k1gFnPc2	skupina
Tygers	Tygersa	k1gFnPc2	Tygersa
of	of	k?	of
Pan	Pan	k1gMnSc1	Pan
Tang	tango	k1gNnPc2	tango
<g/>
,	,	kIx,	,
Diamond	Diamond	k1gMnSc1	Diamond
Head	Head	k1gMnSc1	Head
a	a	k8xC	a
Iron	iron	k1gInSc1	iron
Maiden	Maidna	k1gFnPc2	Maidna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
inzerát	inzerát	k1gInSc4	inzerát
poté	poté	k6eAd1	poté
odpověděli	odpovědět	k5eAaPmAgMnP	odpovědět
kytarista	kytarista	k1gMnSc1	kytarista
James	James	k1gMnSc1	James
Hetfield	Hetfield	k1gMnSc1	Hetfield
a	a	k8xC	a
Hugh	Hugh	k1gMnSc1	Hugh
Tanner	Tanner	k1gMnSc1	Tanner
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Leather	Leathra	k1gFnPc2	Leathra
Charm	charm	k1gInSc1	charm
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
nevytvořila	vytvořit	k5eNaPmAgFnS	vytvořit
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
Ulrich	Ulrich	k1gMnSc1	Ulrich
už	už	k6eAd1	už
požádal	požádat	k5eAaPmAgMnS	požádat
zakladatele	zakladatel	k1gMnSc2	zakladatel
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
Metal	metat	k5eAaImAgMnS	metat
Blade	Blad	k1gInSc5	Blad
Records	Recordsa	k1gFnPc2	Recordsa
Briana	Brian	k1gMnSc4	Brian
Slagela	Slagel	k1gMnSc4	Slagel
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
by	by	kYmCp3nS	by
nemohl	moct	k5eNaImAgMnS	moct
nahrát	nahrát	k5eAaBmF	nahrát
skladbu	skladba	k1gFnSc4	skladba
pro	pro	k7c4	pro
novou	nový	k2eAgFnSc4d1	nová
komplaci	komplace	k1gFnSc4	komplace
vydavatelství	vydavatelství	k1gNnSc3	vydavatelství
zvanou	zvaný	k2eAgFnSc4d1	zvaná
Metal	metal	k1gInSc1	metal
Massacre	Massacr	k1gInSc5	Massacr
<g/>
.	.	kIx.	.
</s>
<s>
Slagel	Slagel	k1gMnSc1	Slagel
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
a	a	k8xC	a
Ulrich	Ulrich	k1gMnSc1	Ulrich
převědčil	převědčit	k5eAaPmAgMnS	převědčit
Hetfielda	Hetfield	k1gMnSc4	Hetfield
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zpíval	zpívat	k5eAaImAgMnS	zpívat
a	a	k8xC	a
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
rytmickou	rytmický	k2eAgFnSc4d1	rytmická
kytaru	kytara	k1gFnSc4	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
tedy	tedy	k9	tedy
byla	být	k5eAaImAgFnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
řjnu	řjnus	k1gInSc6	řjnus
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
pět	pět	k4xCc4	pět
měsíců	měsíc	k1gInPc2	měsíc
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
Ulrich	Ulrich	k1gMnSc1	Ulrich
a	a	k8xC	a
Hetfield	Hetfield	k1gMnSc1	Hetfield
poprvé	poprvé	k6eAd1	poprvé
setkali	setkat	k5eAaPmAgMnP	setkat
<g/>
.	.	kIx.	.
</s>
<s>
Ulrichův	Ulrichův	k2eAgMnSc1d1	Ulrichův
přítel	přítel	k1gMnSc1	přítel
Ron	Ron	k1gMnSc1	Ron
Quintana	Quintan	k1gMnSc2	Quintan
potom	potom	k6eAd1	potom
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
jméno	jméno	k1gNnSc4	jméno
Metallica	Metallicum	k1gNnSc2	Metallicum
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gInPc7	jeho
návrhy	návrh	k1gInPc7	návrh
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
i	i	k9	i
název	název	k1gInSc1	název
MetalMania	MetalManium	k1gNnSc2	MetalManium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ulrich	Ulrich	k1gMnSc1	Ulrich
nakonec	nakonec	k6eAd1	nakonec
použil	použít	k5eAaPmAgMnS	použít
jméno	jméno	k1gNnSc4	jméno
Metallica	Metallicum	k1gNnSc2	Metallicum
a	a	k8xC	a
pak	pak	k6eAd1	pak
dal	dát	k5eAaPmAgMnS	dát
druhý	druhý	k4xOgInSc4	druhý
inzerát	inzerát	k1gInSc4	inzerát
do	do	k7c2	do
novin	novina	k1gFnPc2	novina
The	The	k1gMnSc1	The
Recycler	Recycler	k1gMnSc1	Recycler
<g/>
,	,	kIx,	,
a	a	k8xC	a
do	do	k7c2	do
něho	on	k3xPp3gMnSc2	on
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hledá	hledat	k5eAaImIp3nS	hledat
sólového	sólový	k2eAgMnSc4d1	sólový
kytaristu	kytarista	k1gMnSc4	kytarista
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
Dave	Dav	k1gInSc5	Dav
Mustaine	Mustain	k1gMnSc5	Mustain
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
Hetfield	Hetfield	k1gMnSc1	Hetfield
a	a	k8xC	a
Ulrich	Ulrich	k1gMnSc1	Ulrich
viděli	vidět	k5eAaImAgMnP	vidět
jeho	jeho	k3xOp3gNnSc4	jeho
drahé	drahý	k2eAgNnSc4d1	drahé
vybavení	vybavení	k1gNnSc4	vybavení
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
začleněn	začlenit	k5eAaPmNgInS	začlenit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
pak	pak	k6eAd1	pak
Metallica	Metallicum	k1gNnSc2	Metallicum
nahrála	nahrát	k5eAaPmAgFnS	nahrát
první	první	k4xOgFnSc4	první
vlastní	vlastní	k2eAgFnSc4d1	vlastní
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
Hit	hit	k1gInSc1	hit
the	the	k?	the
Lights	Lights	k1gInSc1	Lights
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
kompilaci	kompilace	k1gFnSc4	kompilace
Metal	metat	k5eAaImAgMnS	metat
Massacre	Massacr	k1gInSc5	Massacr
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
skupina	skupina	k1gFnSc1	skupina
uvedena	uvést	k5eAaPmNgFnS	uvést
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Mettallica	Mettallica	k1gMnSc1	Mettallica
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hetfield	Hetfield	k1gMnSc1	Hetfield
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
basovou	basový	k2eAgFnSc4d1	basová
kytaru	kytara	k1gFnSc4	kytara
a	a	k8xC	a
Lloyd	Lloyd	k1gInSc4	Lloyd
Grant	grant	k1gInSc1	grant
hrál	hrát	k5eAaImAgInS	hrát
kytarové	kytarový	k2eAgNnSc4d1	kytarové
sólo	sólo	k1gNnSc4	sólo
<g/>
.	.	kIx.	.
</s>
<s>
Kompilace	kompilace	k1gFnSc1	kompilace
vyšla	vyjít	k5eAaPmAgFnS	vyjít
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
byli	být	k5eAaImAgMnP	být
členové	člen	k1gMnPc1	člen
rozhněváni	rozhněván	k2eAgMnPc1d1	rozhněván
chybou	chyba	k1gFnSc7	chyba
na	na	k7c6	na
obalu	obal	k1gInSc6	obal
<g/>
,	,	kIx,	,
Metallica	Metallica	k1gFnSc1	Metallica
se	se	k3xPyFc4	se
dokázala	dokázat	k5eAaPmAgFnS	dokázat
touto	tento	k3xDgFnSc7	tento
skladbou	skladba	k1gFnSc7	skladba
dostatečně	dostatečně	k6eAd1	dostatečně
proslavit	proslavit	k5eAaPmF	proslavit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
si	se	k3xPyFc3	se
mohla	moct	k5eAaImAgFnS	moct
poprvé	poprvé	k6eAd1	poprvé
naživo	naživo	k1gNnSc4	naživo
zahrát	zahrát	k5eAaPmF	zahrát
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1982	[number]	k4	1982
v	v	k7c4	v
Radio	radio	k1gNnSc4	radio
City	City	k1gFnSc2	City
v	v	k7c6	v
Anaheimu	Anaheim	k1gInSc6	Anaheim
v	v	k7c6	v
Californii	Californie	k1gFnSc6	Californie
s	s	k7c7	s
nově	nově	k6eAd1	nově
začleněným	začleněný	k2eAgMnSc7d1	začleněný
baskytaristou	baskytarista	k1gMnSc7	baskytarista
Ronem	ron	k1gInSc7	ron
McGovneym	McGovneym	k1gInSc1	McGovneym
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
skutečný	skutečný	k2eAgInSc1d1	skutečný
koncert	koncert	k1gInSc1	koncert
kapely	kapela	k1gFnSc2	kapela
(	(	kIx(	(
<g/>
sice	sice	k8xC	sice
zahráli	zahrát	k5eAaPmAgMnP	zahrát
již	již	k6eAd1	již
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
dříve	dříve	k6eAd2	dříve
v	v	k7c6	v
Anaheimu	Anaheim	k1gInSc6	Anaheim
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
návštěvě	návštěva	k1gFnSc3	návštěva
pouze	pouze	k6eAd1	pouze
asi	asi	k9	asi
dvaceti	dvacet	k4xCc2	dvacet
lidí	člověk	k1gMnPc2	člověk
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
spíše	spíše	k9	spíše
o	o	k7c4	o
večírek	večírek	k1gInSc4	večírek
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
až	až	k9	až
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1982	[number]	k4	1982
v	v	k7c6	v
Hollywoodském	hollywoodský	k2eAgInSc6d1	hollywoodský
klubu	klub	k1gInSc6	klub
Whisky	whisky	k1gFnSc2	whisky
a	a	k8xC	a
Go	Go	k1gFnSc2	Go
Go	Go	k1gFnSc2	Go
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
měli	mít	k5eAaImAgMnP	mít
pouze	pouze	k6eAd1	pouze
předskakovat	předskakovat	k5eAaImF	předskakovat
britským	britský	k2eAgMnPc3d1	britský
Saxon	Saxon	k1gInSc4	Saxon
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
hráli	hrát	k5eAaImAgMnP	hrát
celý	celý	k2eAgInSc4d1	celý
večer	večer	k1gInSc4	večer
<g/>
.	.	kIx.	.
</s>
<s>
Metallica	Metallica	k1gFnSc1	Metallica
pak	pak	k6eAd1	pak
nahrála	nahrát	k5eAaBmAgFnS	nahrát
první	první	k4xOgFnPc4	první
demo	demo	k2eAgFnPc4d1	demo
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
se	se	k3xPyFc4	se
jmenovalo	jmenovat	k5eAaImAgNnS	jmenovat
Power	Power	k1gInSc4	Power
Metal	metal	k1gInSc4	metal
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
Quitanovy	Quitanův	k2eAgFnSc2d1	Quitanův
vizitky	vizitka	k1gFnSc2	vizitka
ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
ale	ale	k8xC	ale
Ulrich	Ulrich	k1gMnSc1	Ulrich
a	a	k8xC	a
Hetfield	Hetfielda	k1gFnPc2	Hetfielda
navštívili	navštívit	k5eAaPmAgMnP	navštívit
vystoupení	vystoupení	k1gNnSc4	vystoupení
skupiny	skupina	k1gFnSc2	skupina
Trauma	trauma	k1gNnSc1	trauma
v	v	k7c6	v
nočním	noční	k2eAgInSc6d1	noční
klubu	klub	k1gInSc6	klub
Whisky	whisky	k1gFnSc2	whisky
a	a	k8xC	a
Go	Go	k1gMnSc1	Go
Go	Go	k1gMnSc1	Go
(	(	kIx(	(
<g/>
který	který	k3yIgMnSc1	který
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
západním	západní	k2eAgInSc6d1	západní
Hollywoodu	Hollywood	k1gInSc6	Hollywood
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
hrál	hrát	k5eAaImAgMnS	hrát
baskytarista	baskytarista	k1gMnSc1	baskytarista
Cliff	Cliff	k1gMnSc1	Cliff
Burton	Burton	k1gInSc4	Burton
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
dva	dva	k4xCgMnPc1	dva
byli	být	k5eAaImAgMnP	být
ohromeni	ohromit	k5eAaPmNgMnP	ohromit
Burtonovým	Burtonový	k2eAgNnSc7d1	Burtonový
používáním	používání	k1gNnSc7	používání
wah-wah	wahaha	k1gFnPc2	wah-waha
pedálu	pedál	k1gInSc2	pedál
a	a	k8xC	a
požádali	požádat	k5eAaPmAgMnP	požádat
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
nepřidal	přidat	k5eNaPmAgMnS	přidat
k	k	k7c3	k
Metallice	Metallika	k1gFnSc3	Metallika
<g/>
.	.	kIx.	.
</s>
<s>
Hetfield	Hetfield	k6eAd1	Hetfield
a	a	k8xC	a
Mustaine	Mustain	k1gInSc5	Mustain
pak	pak	k6eAd1	pak
chtěli	chtít	k5eAaImAgMnP	chtít
dostat	dostat	k5eAaPmF	dostat
McGovneyho	McGovney	k1gMnSc4	McGovney
pryč	pryč	k6eAd1	pryč
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
mysleli	myslet	k5eAaImAgMnP	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
ničím	nic	k3yNnSc7	nic
nepřispěl	přispět	k5eNaPmAgMnS	přispět
<g/>
,	,	kIx,	,
jenom	jenom	k9	jenom
poslouchal	poslouchat	k5eAaImAgMnS	poslouchat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
Burton	Burton	k1gInSc4	Burton
nabídku	nabídka	k1gFnSc4	nabídka
původně	původně	k6eAd1	původně
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
,	,	kIx,	,
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
přijal	přijmout	k5eAaPmAgMnS	přijmout
s	s	k7c7	s
podmínkou	podmínka	k1gFnSc7	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
přesune	přesunout	k5eAaPmIp3nS	přesunout
do	do	k7c2	do
El	Ela	k1gFnPc2	Ela
Cerrita	Cerrita	k1gFnSc1	Cerrita
v	v	k7c4	v
San	San	k1gFnSc4	San
Franciscu	Franciscus	k1gInSc2	Franciscus
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
vystoupení	vystoupení	k1gNnSc1	vystoupení
Metallicy	Metallica	k1gFnSc2	Metallica
s	s	k7c7	s
Burtonem	Burton	k1gInSc7	Burton
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
nočním	noční	k2eAgInSc6d1	noční
klubu	klub	k1gInSc6	klub
The	The	k1gFnSc2	The
Stone	ston	k1gInSc5	ston
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
nahrávka	nahrávka	k1gFnSc1	nahrávka
s	s	k7c7	s
Burtonem	Burton	k1gInSc7	Burton
bylo	být	k5eAaImAgNnS	být
demo	demo	k2eAgFnSc4d1	demo
Megaforce	Megaforka	k1gFnSc3	Megaforka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
Metallica	Metallica	k6eAd1	Metallica
byla	být	k5eAaImAgFnS	být
připravena	připraven	k2eAgFnSc1d1	připravena
nahrát	nahrát	k5eAaPmF	nahrát
první	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
Metal	metat	k5eAaImAgMnS	metat
Blade	Blad	k1gInSc5	Blad
nebyl	být	k5eNaImAgInS	být
schopný	schopný	k2eAgInSc1d1	schopný
pokrýt	pokrýt	k5eAaPmF	pokrýt
dodatečné	dodatečný	k2eAgFnPc4d1	dodatečná
finance	finance	k1gFnPc4	finance
<g/>
,	,	kIx,	,
skupina	skupina	k1gFnSc1	skupina
začala	začít	k5eAaPmAgFnS	začít
hledat	hledat	k5eAaImF	hledat
další	další	k2eAgFnPc4d1	další
možnosti	možnost	k1gFnPc4	možnost
<g/>
.	.	kIx.	.
</s>
<s>
Koncertní	koncertní	k2eAgMnSc1d1	koncertní
promotér	promotér	k1gMnSc1	promotér
Johny	John	k1gMnPc4	John
"	"	kIx"	"
<g/>
Z	z	k7c2	z
<g/>
"	"	kIx"	"
Zazula	Zazul	k1gMnSc2	Zazul
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
slyšel	slyšet	k5eAaImAgMnS	slyšet
demo	demo	k2eAgMnSc1d1	demo
No	no	k9	no
Life	Life	k1gNnSc1	Life
'	'	kIx"	'
<g/>
til	til	k1gInSc1	til
Leather	Leathra	k1gFnPc2	Leathra
<g/>
,	,	kIx,	,
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
sjedná	sjednat	k5eAaPmIp3nS	sjednat
dohodu	dohoda	k1gFnSc4	dohoda
mezi	mezi	k7c7	mezi
Metallicou	Metallica	k1gFnSc7	Metallica
a	a	k8xC	a
Newyorskými	newyorský	k2eAgFnPc7d1	newyorská
nahrávacími	nahrávací	k2eAgFnPc7d1	nahrávací
společnostmi	společnost	k1gFnPc7	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
společnosti	společnost	k1gFnPc1	společnost
nemají	mít	k5eNaImIp3nP	mít
zájem	zájem	k1gInSc4	zájem
nahrávat	nahrávat	k5eAaImF	nahrávat
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
Zazula	Zazula	k1gFnSc1	Zazula
půjčil	půjčit	k5eAaPmAgMnS	půjčit
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pokryl	pokrýt	k5eAaPmAgInS	pokrýt
náklady	náklad	k1gInPc4	náklad
na	na	k7c4	na
nahrávání	nahrávání	k1gNnSc4	nahrávání
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
podepsat	podepsat	k5eAaPmF	podepsat
Metallicu	Metallic	k2eAgFnSc4d1	Metallica
smlouvu	smlouva	k1gFnSc4	smlouva
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gNnPc4	jeho
vlastní	vlastní	k2eAgNnPc4d1	vlastní
vydavatelství	vydavatelství	k1gNnPc4	vydavatelství
Megaforce	Megaforka	k1gFnSc3	Megaforka
Records	Records	k1gInSc4	Records
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
natáčení	natáčení	k1gNnSc2	natáčení
prvního	první	k4xOgNnSc2	první
alba	album	k1gNnSc2	album
Kill	Killa	k1gFnPc2	Killa
'	'	kIx"	'
<g/>
Em	Ema	k1gFnPc2	Ema
All	All	k1gFnSc2	All
jela	jet	k5eAaImAgFnS	jet
kapela	kapela	k1gFnSc1	kapela
do	do	k7c2	do
Rochesteru	Rochester	k1gInSc2	Rochester
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
Hetfield	Hetfield	k1gMnSc1	Hetfield
<g/>
,	,	kIx,	,
Ulrich	Ulrich	k1gMnSc1	Ulrich
a	a	k8xC	a
Burton	Burton	k1gInSc4	Burton
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
vyloučit	vyloučit	k5eAaPmF	vyloučit
Mustaina	Mustain	k1gMnSc4	Mustain
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gNnSc4	jeho
problémy	problém	k1gInPc1	problém
s	s	k7c7	s
alkoholem	alkohol	k1gInSc7	alkohol
<g/>
,	,	kIx,	,
drogami	droga	k1gFnPc7	droga
a	a	k8xC	a
násilnickým	násilnický	k2eAgNnSc7d1	násilnické
chováním	chování	k1gNnSc7	chování
<g/>
,	,	kIx,	,
během	během	k7c2	během
sešlosti	sešlost	k1gFnSc2	sešlost
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
Mustaina	Mustaina	k1gMnSc1	Mustaina
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Kirk	Kirk	k1gMnSc1	Kirk
Hammett	Hammett	k1gMnSc1	Hammett
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
Exodus	Exodus	k1gInSc1	Exodus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
členem	člen	k1gInSc7	člen
Metallicy	Metallica	k1gFnSc2	Metallica
ještě	ještě	k6eAd1	ještě
ten	ten	k3xDgInSc4	ten
samý	samý	k3xTgInSc4	samý
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
vystoupení	vystoupení	k1gNnSc1	vystoupení
s	s	k7c7	s
Hammetem	Hammet	k1gInSc7	Hammet
bylo	být	k5eAaImAgNnS	být
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1983	[number]	k4	1983
v	v	k7c6	v
nočním	noční	k2eAgInSc6d1	noční
klubu	klub	k1gInSc6	klub
The	The	k1gFnSc2	The
Showplace	Showplace	k1gFnSc2	Showplace
v	v	k7c6	v
Doveru	Dover	k1gInSc6	Dover
v	v	k7c6	v
New	New	k1gFnSc6	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
mělo	mít	k5eAaImAgNnS	mít
jmenovat	jmenovat	k5eAaBmF	jmenovat
Metal	metal	k1gInSc4	metal
Up	Up	k1gMnSc1	Up
Your	Your	k1gMnSc1	Your
Ass	Ass	k1gMnSc1	Ass
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
sporu	spor	k1gInSc3	spor
mezi	mezi	k7c7	mezi
vydavateli	vydavatel	k1gMnPc7	vydavatel
a	a	k8xC	a
distributory	distributor	k1gMnPc7	distributor
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
odmítali	odmítat	k5eAaImAgMnP	odmítat
vydat	vydat	k5eAaPmF	vydat
album	album	k1gNnSc4	album
s	s	k7c7	s
tímhle	tenhle	k3xDgInSc7	tenhle
názevem	název	k1gInSc7	název
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
album	album	k1gNnSc1	album
přejemováno	přejemovat	k5eAaImNgNnS	přejemovat
na	na	k7c4	na
Kill	Kill	k1gMnSc1	Kill
'	'	kIx"	'
<g/>
Em	Ema	k1gFnPc2	Ema
All	All	k1gFnSc2	All
a	a	k8xC	a
album	album	k1gNnSc1	album
tak	tak	k6eAd1	tak
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1983	[number]	k4	1983
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
klasiku	klasika	k1gFnSc4	klasika
žánru	žánr	k1gInSc2	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
firmou	firma	k1gFnSc7	firma
Megaforce	Megaforka	k1gFnSc3	Megaforka
Records	Records	k1gInSc4	Records
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
firmou	firma	k1gFnSc7	firma
Music	Musice	k1gFnPc2	Musice
for	forum	k1gNnPc2	forum
Nations	Nationsa	k1gFnPc2	Nationsa
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
umístilo	umístit	k5eAaPmAgNnS	umístit
se	se	k3xPyFc4	se
na	na	k7c6	na
120	[number]	k4	120
místě	místo	k1gNnSc6	místo
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc1	billboard
200	[number]	k4	200
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
a	a	k8xC	a
ačkoliv	ačkoliv	k8xS	ačkoliv
album	album	k1gNnSc1	album
nebylo	být	k5eNaImAgNnS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
finančním	finanční	k2eAgInSc7d1	finanční
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
,	,	kIx,	,
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
Metallice	Metallika	k1gFnSc3	Metallika
získat	získat	k5eAaPmF	získat
základnu	základna	k1gFnSc4	základna
posluchačů	posluchač	k1gMnPc2	posluchač
v	v	k7c6	v
undergroundové	undergroundový	k2eAgFnSc6d1	undergroundová
metalové	metalový	k2eAgFnSc6d1	metalová
scéně	scéna	k1gFnSc6	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Metallica	Metallica	k6eAd1	Metallica
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
svého	své	k1gNnSc2	své
prvního	první	k4xOgNnSc2	první
alba	album	k1gNnSc2	album
vyjela	vyjet	k5eAaPmAgFnS	vyjet
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
turné	turné	k1gNnSc4	turné
(	(	kIx(	(
<g/>
s	s	k7c7	s
britskou	britský	k2eAgFnSc7d1	britská
kapelou	kapela	k1gFnSc7	kapela
Raven	Ravna	k1gFnPc2	Ravna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
podpořila	podpořit	k5eAaPmAgFnS	podpořit
vydání	vydání	k1gNnSc2	vydání
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1984	[number]	k4	1984
předskakovala	předskakovat	k5eAaImAgFnS	předskakovat
skupině	skupina	k1gFnSc3	skupina
Venom	Venom	k1gInSc4	Venom
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
Seven	Seven	k2eAgInSc1d1	Seven
Dates	Dates	k1gInSc1	Dates
of	of	k?	of
Hell	Hell	k1gInSc1	Hell
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
skupina	skupina	k1gFnSc1	skupina
hrála	hrát	k5eAaImAgFnS	hrát
před	před	k7c7	před
sedmi	sedm	k4xCc7	sedm
tisíci	tisíc	k4xCgInPc7	tisíc
lidmi	člověk	k1gMnPc7	člověk
na	na	k7c6	na
Aardschokovém	Aardschokový	k2eAgInSc6d1	Aardschokový
Festivalu	festival	k1gInSc6	festival
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Zwolle	Zwolle	k1gFnSc2	Zwolle
v	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
<g/>
.	.	kIx.	.
</s>
<s>
Mustaine	Mustainout	k5eAaPmIp3nS	Mustainout
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
založil	založit	k5eAaPmAgMnS	založit
skupinu	skupina	k1gFnSc4	skupina
Megadeth	Megadeth	k1gMnSc1	Megadeth
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
v	v	k7c6	v
rozhovorech	rozhovor	k1gInPc6	rozhovor
vyjadřoval	vyjadřovat	k5eAaImAgMnS	vyjadřovat
svoji	svůj	k3xOyFgFnSc4	svůj
nelibost	nelibost	k1gFnSc4	nelibost
vůči	vůči	k7c3	vůči
Hammetovi	Hammet	k1gMnSc3	Hammet
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
říkal	říkat	k5eAaImAgMnS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
Hammet	Hammet	k1gInSc4	Hammet
"	"	kIx"	"
<g/>
ukradl	ukradnout	k5eAaPmAgMnS	ukradnout
job	job	k?	job
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Mustaine	Mustainout	k5eAaPmIp3nS	Mustainout
byl	být	k5eAaImAgMnS	být
"	"	kIx"	"
<g/>
nasrán	nasrán	k2eAgMnSc1d1	nasrán
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Hammet	Hammet	k1gInSc1	Hammet
stal	stát	k5eAaPmAgInS	stát
známý	známý	k2eAgMnSc1d1	známý
díky	díky	k7c3	díky
hraním	hraň	k1gFnPc3	hraň
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
napsal	napsat	k5eAaPmAgMnS	napsat
on	on	k3xPp3gMnSc1	on
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
s	s	k7c7	s
Metal	metal	k1gInSc1	metal
Force	force	k1gFnPc1	force
<g/>
,	,	kIx,	,
Mustaine	Mustain	k1gInSc5	Mustain
o	o	k7c6	o
Hammetovi	Hammet	k1gMnSc6	Hammet
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
opravdu	opravdu	k6eAd1	opravdu
legrační	legrační	k2eAgNnSc1d1	legrační
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
Kirk	Kirk	k1gMnSc1	Kirk
Hammet	Hammet	k1gMnSc1	Hammet
ukradl	ukradnout	k5eAaPmAgMnS	ukradnout
všechna	všechen	k3xTgNnPc4	všechen
má	mít	k5eAaImIp3nS	mít
sóla	sólo	k1gNnPc1	sólo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jsem	být	k5eAaImIp1nS	být
nahrál	nahrát	k5eAaBmAgInS	nahrát
v	v	k7c6	v
No	no	k9	no
Life	Life	k1gNnSc2	Life
'	'	kIx"	'
<g/>
til	til	k1gInSc1	til
Leather	Leathra	k1gFnPc2	Leathra
a	a	k8xC	a
vy	vy	k3xPp2nPc1	vy
jste	být	k5eAaImIp2nP	být
ho	on	k3xPp3gInSc4	on
ještě	ještě	k6eAd1	ještě
zvolili	zvolit	k5eAaPmAgMnP	zvolit
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
kytaristou	kytarista	k1gMnSc7	kytarista
časopisu	časopis	k1gInSc2	časopis
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Ns	Ns	k1gFnSc6	Ns
první	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
skupiny	skupina	k1gFnSc2	skupina
Megadeth	Megadetha	k1gFnPc2	Megadetha
Killing	Killing	k1gInSc1	Killing
Is	Is	k1gMnSc1	Is
My	my	k3xPp1nPc1	my
Business	business	k1gInSc4	business
<g/>
...	...	k?	...
And	Anda	k1gFnPc2	Anda
Business	business	k1gInSc1	business
Is	Is	k1gMnSc1	Is
Good	Good	k1gMnSc1	Good
<g/>
!	!	kIx.	!
</s>
<s>
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
Mustaine	Mustain	k1gInSc5	Mustain
zařadil	zařadit	k5eAaPmAgInS	zařadit
skladbu	skladba	k1gFnSc4	skladba
Mechanix	Mechanix	k1gInSc1	Mechanix
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
Metallica	Metallica	k1gFnSc1	Metallica
upravila	upravit	k5eAaPmAgFnS	upravit
a	a	k8xC	a
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
na	na	k7c6	na
The	The	k1gFnSc6	The
Four	Foura	k1gFnPc2	Foura
Horsemen	Horsemen	k1gInSc1	Horsemen
(	(	kIx(	(
<g/>
z	z	k7c2	z
alba	album	k1gNnSc2	album
Kill	Killa	k1gFnPc2	Killa
'	'	kIx"	'
<g/>
Em	Ema	k1gFnPc2	Ema
All	All	k1gFnPc2	All
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mustaine	Mustainout	k5eAaPmIp3nS	Mustainout
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc4	ten
udělal	udělat	k5eAaPmAgMnS	udělat
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
naštval	naštvat	k5eAaBmAgMnS	naštvat
Metallicu	Metallica	k1gFnSc4	Metallica
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
zase	zase	k9	zase
oponovala	oponovat	k5eAaImAgFnS	oponovat
a	a	k8xC	a
označila	označit	k5eAaPmAgFnS	označit
Mustaina	Mustaina	k1gFnSc1	Mustaina
za	za	k7c4	za
alkoholika	alkoholik	k1gMnSc4	alkoholik
a	a	k8xC	a
za	za	k7c4	za
muzikanta	muzikant	k1gMnSc4	muzikant
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ani	ani	k9	ani
neumí	umět	k5eNaImIp3nS	umět
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Metallica	Metallica	k1gFnSc1	Metallica
nahrála	nahrát	k5eAaBmAgFnS	nahrát
druhé	druhý	k4xOgNnSc1	druhý
studiové	studiový	k2eAgNnSc1d1	studiové
album	album	k1gNnSc1	album
<g/>
,	,	kIx,	,
Ride	Ride	k1gInSc1	Ride
the	the	k?	the
Lightning	Lightning	k1gInSc1	Lightning
<g/>
,	,	kIx,	,
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
Sweet	Sweeta	k1gFnPc2	Sweeta
Silence	silenka	k1gFnSc6	silenka
v	v	k7c6	v
Kodani	Kodaň	k1gFnSc6	Kodaň
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
<g/>
.	.	kIx.	.
</s>
<s>
Producentem	producent	k1gMnSc7	producent
tentokrát	tentokrát	k6eAd1	tentokrát
byl	být	k5eAaImAgMnS	být
Flemming	Flemming	k1gInSc4	Flemming
Rasmussen	Rasmussen	k2eAgInSc4d1	Rasmussen
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1984	[number]	k4	1984
a	a	k8xC	a
umístilo	umístit	k5eAaPmAgNnS	umístit
se	se	k3xPyFc4	se
na	na	k7c6	na
stém	stý	k4xOgNnSc6	stý
místě	místo	k1gNnSc6	místo
v	v	k7c4	v
Billboard	billboard	k1gInSc4	billboard
200	[number]	k4	200
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgMnSc1d1	zvláštní
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
tiskárně	tiskárna	k1gFnSc6	tiskárna
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
omylem	omylem	k6eAd1	omylem
vytiskli	vytisknout	k5eAaPmAgMnP	vytisknout
zelené	zelený	k2eAgInPc4d1	zelený
obaly	obal	k1gInPc4	obal
k	k	k7c3	k
albu	album	k1gNnSc3	album
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
staly	stát	k5eAaPmAgFnP	stát
vysoce	vysoce	k6eAd1	vysoce
ceněnou	ceněný	k2eAgFnSc7d1	ceněná
sběratelskou	sběratelský	k2eAgFnSc7d1	sběratelská
raritou	rarita	k1gFnSc7	rarita
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1	ostatní
skladby	skladba	k1gFnPc1	skladba
z	z	k7c2	z
alba	album	k1gNnSc2	album
jsou	být	k5eAaImIp3nP	být
Fight	Fight	k2eAgInSc4d1	Fight
Fire	Fire	k1gInSc4	Fire
With	With	k1gMnSc1	With
Fire	Fir	k1gMnSc2	Fir
(	(	kIx(	(
<g/>
tématem	téma	k1gNnSc7	téma
písně	píseň	k1gFnSc2	píseň
je	být	k5eAaImIp3nS	být
nukleární	nukleární	k2eAgFnSc1d1	nukleární
válka	válka	k1gFnSc1	válka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
For	forum	k1gNnPc2	forum
Whom	Whom	k1gInSc1	Whom
the	the	k?	the
Bell	bell	k1gInSc1	bell
Tolls	Tolls	k1gInSc1	Tolls
<g/>
,	,	kIx,	,
Fade	Fade	k1gInSc1	Fade
to	ten	k3xDgNnSc1	ten
Black	Black	k1gMnSc1	Black
<g/>
,	,	kIx,	,
Trapped	Trapped	k1gMnSc1	Trapped
Under	Under	k1gMnSc1	Under
Ice	Ice	k1gMnSc1	Ice
<g/>
,	,	kIx,	,
Escape	Escap	k1gMnSc5	Escap
(	(	kIx(	(
<g/>
pokus	pokus	k1gInSc4	pokus
kapely	kapela	k1gFnSc2	kapela
o	o	k7c4	o
rádiový	rádiový	k2eAgInSc4d1	rádiový
hit	hit	k1gInSc4	hit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Creeping	Creeping	k1gInSc1	Creeping
Death	Death	k1gInSc1	Death
(	(	kIx(	(
<g/>
skladba	skladba	k1gFnSc1	skladba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
biblický	biblický	k2eAgInSc4d1	biblický
příběh	příběh	k1gInSc4	příběh
o	o	k7c6	o
židovském	židovský	k2eAgInSc6d1	židovský
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
otroctví	otroctví	k1gNnSc2	otroctví
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
,	,	kIx,	,
zaměřující	zaměřující	k2eAgFnSc4d1	zaměřující
se	se	k3xPyFc4	se
především	především	k9	především
na	na	k7c4	na
mor	mor	k1gInSc4	mor
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Egypťany	Egypťan	k1gMnPc4	Egypťan
<g/>
)	)	kIx)	)
a	a	k8xC	a
instrumentální	instrumentální	k2eAgFnSc1d1	instrumentální
skladba	skladba	k1gFnSc1	skladba
The	The	k1gFnSc2	The
Call	Calla	k1gFnPc2	Calla
of	of	k?	of
Ktulu	Ktul	k1gInSc2	Ktul
<g/>
.	.	kIx.	.
</s>
<s>
Mustaine	Mustainout	k5eAaPmIp3nS	Mustainout
je	být	k5eAaImIp3nS	být
označen	označit	k5eAaPmNgMnS	označit
jako	jako	k9	jako
spoluautor	spoluautor	k1gMnSc1	spoluautor
skladeb	skladba	k1gFnPc2	skladba
Ride	Ride	k1gInSc4	Ride
the	the	k?	the
Lightning	Lightning	k1gInSc1	Lightning
a	a	k8xC	a
The	The	k1gFnSc1	The
Call	Calla	k1gFnPc2	Calla
of	of	k?	of
Ktulu	Ktul	k1gInSc2	Ktul
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
po	po	k7c6	po
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
koncertů	koncert	k1gInPc2	koncert
<g/>
,	,	kIx,	,
někdo	někdo	k3yInSc1	někdo
vykradl	vykrást	k5eAaPmAgMnS	vykrást
náklaďák	náklaďák	k1gInSc4	náklaďák
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
hudební	hudební	k2eAgFnSc7d1	hudební
výbavou	výbava	k1gFnSc7	výbava
<g/>
,	,	kIx,	,
členům	člen	k1gMnPc3	člen
skupiny	skupina	k1gFnPc1	skupina
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
jen	jen	k9	jen
ty	ten	k3xDgFnPc1	ten
kytary	kytara	k1gFnPc1	kytara
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
měli	mít	k5eAaImAgMnP	mít
s	s	k7c7	s
sebou	se	k3xPyFc7	se
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
zážitku	zážitek	k1gInSc2	zážitek
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
první	první	k4xOgFnSc1	první
balada	balada	k1gFnSc1	balada
Metallicy	Metallica	k1gFnSc2	Metallica
Fade	Fade	k1gFnSc1	Fade
to	ten	k3xDgNnSc4	ten
Black	Black	k1gInSc1	Black
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
má	mít	k5eAaImIp3nS	mít
velice	velice	k6eAd1	velice
pesimistický	pesimistický	k2eAgInSc1d1	pesimistický
text	text	k1gInSc1	text
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ztratil	ztratit	k5eAaPmAgMnS	ztratit
jsem	být	k5eAaImIp1nS	být
chuť	chuť	k1gFnSc4	chuť
do	do	k7c2	do
života	život	k1gInSc2	život
<g/>
...	...	k?	...
nic	nic	k6eAd1	nic
mi	já	k3xPp1nSc3	já
nezůstalo	zůstat	k5eNaPmAgNnS	zůstat
<g/>
,	,	kIx,	,
potřebuji	potřebovat	k5eAaImIp1nS	potřebovat
konec	konec	k1gInSc4	konec
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
se	se	k3xPyFc4	se
osvobodil	osvobodit	k5eAaPmAgInS	osvobodit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
Metallica	Metallica	k1gFnSc1	Metallica
vydala	vydat	k5eAaPmAgFnS	vydat
na	na	k7c4	na
evropské	evropský	k2eAgNnSc4d1	Evropské
turné	turné	k1gNnSc4	turné
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k8xS	co
ředitel	ředitel	k1gMnSc1	ředitel
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
Elektra	Elektr	k1gMnSc2	Elektr
Records	Recordsa	k1gFnPc2	Recordsa
Michael	Michael	k1gMnSc1	Michael
Alago	Alago	k1gMnSc1	Alago
a	a	k8xC	a
spoluzakladatel	spoluzakladatel	k1gMnSc1	spoluzakladatel
firmy	firma	k1gFnSc2	firma
Q-Prime	Q-Prim	k1gInSc5	Q-Prim
Management	management	k1gInSc1	management
Cliff	Cliff	k1gMnSc1	Cliff
Burnstein	Burnsteina	k1gFnPc2	Burnsteina
viděli	vidět	k5eAaImAgMnP	vidět
koncert	koncert	k1gInSc4	koncert
Metallicy	Metallica	k1gFnSc2	Metallica
<g/>
,	,	kIx,	,
nabídli	nabídnout	k5eAaPmAgMnP	nabídnout
ji	on	k3xPp3gFnSc4	on
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
skupinu	skupina	k1gFnSc4	skupina
výhodná	výhodný	k2eAgFnSc1d1	výhodná
a	a	k8xC	a
tak	tak	k6eAd1	tak
ji	on	k3xPp3gFnSc4	on
12	[number]	k4	12
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
podepsala	podepsat	k5eAaPmAgFnS	podepsat
<g/>
.	.	kIx.	.
</s>
<s>
Úspěch	úspěch	k1gInSc1	úspěch
Metallicy	Metallica	k1gFnSc2	Metallica
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
zvýrazněn	zvýraznit	k5eAaPmNgInS	zvýraznit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
British	British	k1gMnSc1	British
label	label	k1gMnSc1	label
Music	Music	k1gMnSc1	Music
for	forum	k1gNnPc2	forum
Nations	Nations	k1gInSc1	Nations
vydal	vydat	k5eAaPmAgInS	vydat
limitovanou	limitovaný	k2eAgFnSc4d1	limitovaná
edici	edice	k1gFnSc4	edice
singlu	singl	k1gInSc2	singl
Creeping	Creeping	k1gInSc1	Creeping
Death	Death	k1gInSc1	Death
<g/>
,	,	kIx,	,
kterého	který	k3yRgNnSc2	který
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
40	[number]	k4	40
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
skladeb	skladba	k1gFnPc2	skladba
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
na	na	k7c6	na
znovuvydaném	znovuvydaný	k2eAgNnSc6d1	znovuvydaný
albu	album	k1gNnSc6	album
Kill	Killa	k1gFnPc2	Killa
'	'	kIx"	'
<g/>
Em	Ema	k1gFnPc2	Ema
All	All	k1gFnPc2	All
(	(	kIx(	(
<g/>
vydaném	vydaný	k2eAgInSc6d1	vydaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
Elektrou	Elektrý	k2eAgFnSc7d1	Elektrý
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnPc1d1	obsahující
předělávky	předělávka	k1gFnPc1	předělávka
skladeb	skladba	k1gFnPc2	skladba
Am	Am	k1gFnPc2	Am
I	i	k8xC	i
Evil	Evila	k1gFnPc2	Evila
<g/>
?	?	kIx.	?
</s>
<s>
od	od	k7c2	od
Diamond	Diamonda	k1gFnPc2	Diamonda
Head	Heada	k1gFnPc2	Heada
a	a	k8xC	a
Blitzkrieg	Blitzkriega	k1gFnPc2	Blitzkriega
od	od	k7c2	od
skupiny	skupina	k1gFnSc2	skupina
Blitzkrieg	Blitzkrieg	k1gInSc1	Blitzkrieg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Metallica	Metallica	k1gFnSc1	Metallica
poté	poté	k6eAd1	poté
vyjela	vyjet	k5eAaPmAgFnS	vyjet
na	na	k7c4	na
první	první	k4xOgNnSc4	první
velké	velký	k2eAgNnSc4d1	velké
evropské	evropský	k2eAgNnSc4d1	Evropské
turné	turné	k1gNnSc4	turné
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Tank	tank	k1gInSc4	tank
s	s	k7c7	s
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
návštěvou	návštěva	k1gFnSc7	návštěva
1300	[number]	k4	1300
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
USA	USA	kA	USA
skupina	skupina	k1gFnSc1	skupina
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
turné	turné	k1gNnSc4	turné
se	s	k7c7	s
skupinami	skupina	k1gFnPc7	skupina
W.	W.	kA	W.
<g/>
A.S.	A.S.	k1gFnPc2	A.S.
<g/>
P.	P.	kA	P.
a	a	k8xC	a
Armored	Armored	k1gMnSc1	Armored
Saint	Saint	k1gMnSc1	Saint
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1984	[number]	k4	1984
skupina	skupina	k1gFnSc1	skupina
hrála	hrát	k5eAaImAgFnS	hrát
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
Monsters	Monsters	k1gInSc1	Monsters
of	of	k?	of
Rock	rock	k1gInSc1	rock
v	v	k7c4	v
Donnington	Donnington	k1gInSc4	Donnington
Parku	park	k1gInSc2	park
vedle	vedle	k7c2	vedle
takových	takový	k3xDgFnPc2	takový
kapel	kapela	k1gFnPc2	kapela
jako	jako	k8xC	jako
Ratt	Ratta	k1gFnPc2	Ratta
a	a	k8xC	a
Bon	bona	k1gFnPc2	bona
Jovi	Jov	k1gFnSc2	Jov
před	před	k7c7	před
70	[number]	k4	70
000	[number]	k4	000
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gMnSc1	James
Hettfield	Hettfield	k1gMnSc1	Hettfield
na	na	k7c6	na
koncertě	koncert	k1gInSc6	koncert
přednesl	přednést	k5eAaPmAgMnS	přednést
slavnou	slavný	k2eAgFnSc4d1	slavná
řeč	řeč	k1gFnSc4	řeč
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
"	"	kIx"	"
<g/>
Metallica	Metallica	k1gFnSc1	Metallica
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
kapely	kapela	k1gFnPc1	kapela
tohoto	tento	k3xDgInSc2	tento
festivalu	festival	k1gInSc2	festival
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgFnSc1d1	malá
ukázka	ukázka	k1gFnSc1	ukázka
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jestli	jestli	k8xS	jestli
jste	být	k5eAaImIp2nP	být
přišli	přijít	k5eAaPmAgMnP	přijít
<g/>
,	,	kIx,	,
abyste	aby	kYmCp2nP	aby
viděli	vidět	k5eAaImAgMnP	vidět
barevný	barevný	k2eAgInSc4d1	barevný
hadry	hadr	k1gInPc4	hadr
ze	z	k7c2	z
spandexu	spandex	k1gInSc2	spandex
a	a	k8xC	a
make-up	makep	k1gInSc4	make-up
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jiný	jiný	k2eAgInSc1d1	jiný
sračky	sračka	k1gMnPc7	sračka
a	a	k8xC	a
čekáte	čekat	k5eAaImIp2nP	čekat
na	na	k7c4	na
slova	slovo	k1gNnSc2	slovo
rock	rock	k1gInSc4	rock
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
roll	rolla	k1gFnPc2	rolla
<g/>
,	,	kIx,	,
baby	baba	k1gFnPc1	baba
v	v	k7c4	v
každý	každý	k3xTgInSc4	každý
písničce	písnička	k1gFnSc3	písnička
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
my	my	k3xPp1nPc1	my
nejsme	být	k5eNaImIp1nP	být
vaší	váš	k3xOp2gFnSc7	váš
kapelou	kapela	k1gFnSc7	kapela
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vystoupení	vystoupení	k1gNnSc6	vystoupení
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
Metallicy	Metallica	k1gFnSc2	Metallica
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
zlepšila	zlepšit	k5eAaPmAgFnS	zlepšit
a	a	k8xC	a
prodej	prodej	k1gInSc1	prodej
desek	deska	k1gFnPc2	deska
se	se	k3xPyFc4	se
zdvojnásobil	zdvojnásobit	k5eAaPmAgInS	zdvojnásobit
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
skupina	skupina	k1gFnSc1	skupina
hrála	hrát	k5eAaImAgFnS	hrát
před	před	k7c7	před
obecenstvem	obecenstvo	k1gNnSc7	obecenstvo
60	[number]	k4	60
tisíc	tisíc	k4xCgInPc2	tisíc
posluchačů	posluchač	k1gMnPc2	posluchač
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
Days	Daysa	k1gFnPc2	Daysa
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Green	Green	k2eAgMnSc1d1	Green
v	v	k7c6	v
Oaklandu	Oakland	k1gInSc6	Oakland
v	v	k7c6	v
Californii	Californie	k1gFnSc6	Californie
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgNnSc1	třetí
album	album	k1gNnSc1	album
Metallicy	Metallica	k1gFnSc2	Metallica
<g/>
,	,	kIx,	,
Master	master	k1gMnSc1	master
of	of	k?	of
Puppets	Puppets	k1gInSc1	Puppets
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
nahráno	nahrát	k5eAaPmNgNnS	nahrát
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
Sweet	Sweet	k1gInSc4	Sweet
Silence	silenka	k1gFnSc3	silenka
a	a	k8xC	a
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
pracovali	pracovat	k5eAaImAgMnP	pracovat
od	od	k7c2	od
léta	léto	k1gNnSc2	léto
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
natáčení	natáčení	k1gNnSc1	natáčení
trvalo	trvat	k5eAaImAgNnS	trvat
od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
do	do	k7c2	do
27	[number]	k4	27
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
,	,	kIx,	,
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
album	album	k1gNnSc1	album
mixováno	mixovat	k5eAaImNgNnS	mixovat
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
29	[number]	k4	29
<g/>
.	.	kIx.	.
příčky	příčka	k1gFnSc2	příčka
v	v	k7c6	v
Billboardu	billboard	k1gInSc6	billboard
200	[number]	k4	200
a	a	k8xC	a
strávilo	strávit	k5eAaPmAgNnS	strávit
tam	tam	k6eAd1	tam
72	[number]	k4	72
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Master	master	k1gMnSc1	master
of	of	k?	of
Puppets	Puppetsa	k1gFnPc2	Puppetsa
bylo	být	k5eAaImAgNnS	být
první	první	k4xOgNnSc1	první
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc2	který
Metallice	Metallice	k1gFnSc2	Metallice
získalo	získat	k5eAaPmAgNnS	získat
zlaté	zlatý	k2eAgNnSc1d1	Zlaté
album	album	k1gNnSc1	album
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
4	[number]	k4	4
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1986	[number]	k4	1986
a	a	k8xC	a
poté	poté	k6eAd1	poté
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
bylo	být	k5eAaImAgNnS	být
šestkrát	šestkrát	k6eAd1	šestkrát
označeno	označit	k5eAaPmNgNnS	označit
za	za	k7c4	za
platinové	platinový	k2eAgNnSc4d1	platinové
<g/>
.	.	kIx.	.
</s>
<s>
Steve	Steve	k1gMnSc1	Steve
Huey	Huea	k1gFnSc2	Huea
z	z	k7c2	z
Allmusic	Allmusice	k1gInPc2	Allmusice
označil	označit	k5eAaPmAgMnS	označit
album	album	k1gNnSc4	album
za	za	k7c4	za
"	"	kIx"	"
<g/>
největší	veliký	k2eAgInSc4d3	veliký
úspěch	úspěch	k1gInSc4	úspěch
skupiny	skupina	k1gFnSc2	skupina
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Titulní	titulní	k2eAgFnSc1d1	titulní
skladba	skladba	k1gFnSc1	skladba
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
kokainu	kokain	k1gInSc6	kokain
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
velmi	velmi	k6eAd1	velmi
oblíbený	oblíbený	k2eAgMnSc1d1	oblíbený
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
alba	album	k1gNnSc2	album
se	se	k3xPyFc4	se
Metallica	Metallica	k1gFnSc1	Metallica
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ozzym	Ozzym	k1gInSc1	Ozzym
Osbournem	Osbourn	k1gInSc7	Osbourn
vydala	vydat	k5eAaPmAgFnS	vydat
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
celých	celý	k2eAgInPc6d1	celý
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
Amerických	americký	k2eAgInPc6d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Hetfield	Hetfield	k6eAd1	Hetfield
si	se	k3xPyFc3	se
při	při	k7c6	při
něm	on	k3xPp3gInSc6	on
zlomil	zlomit	k5eAaPmAgMnS	zlomit
zápěstí	zápěstí	k1gNnSc4	zápěstí
a	a	k8xC	a
proto	proto	k8xC	proto
pouze	pouze	k6eAd1	pouze
zpíval	zpívat	k5eAaImAgMnS	zpívat
(	(	kIx(	(
<g/>
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
za	za	k7c4	za
něho	on	k3xPp3gMnSc4	on
hrál	hrát	k5eAaImAgMnS	hrát
John	John	k1gMnSc1	John
Marshall	Marshall	k1gMnSc1	Marshall
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ráno	ráno	k6eAd1	ráno
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
během	během	k7c2	během
Evropské	evropský	k2eAgFnSc2d1	Evropská
části	část	k1gFnSc2	část
turné	turné	k1gNnSc2	turné
Damage	Damag	k1gMnSc2	Damag
Inc	Inc	k1gMnSc2	Inc
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jela	jet	k5eAaImAgFnS	jet
skupina	skupina	k1gFnSc1	skupina
autobusem	autobus	k1gInSc7	autobus
ze	z	k7c2	z
Stockholmu	Stockholm	k1gInSc2	Stockholm
do	do	k7c2	do
Kodaně	Kodaň	k1gFnSc2	Kodaň
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Dörarpu	Dörarp	k1gInSc2	Dörarp
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
však	však	k9	však
řidič	řidič	k1gMnSc1	řidič
autobusu	autobus	k1gInSc2	autobus
ztratil	ztratit	k5eAaPmAgMnS	ztratit
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
vozem	vůz	k1gInSc7	vůz
a	a	k8xC	a
dostal	dostat	k5eAaPmAgMnS	dostat
smyk	smyk	k1gInSc4	smyk
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
poté	poté	k6eAd1	poté
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
převrácení	převrácení	k1gNnSc1	převrácení
autobusu	autobus	k1gInSc2	autobus
<g/>
.	.	kIx.	.
</s>
<s>
Hetfield	Hetfield	k1gInSc1	Hetfield
měl	mít	k5eAaImAgInS	mít
několik	několik	k4yIc4	několik
odřenin	odřenina	k1gFnPc2	odřenina
<g/>
,	,	kIx,	,
Ulrich	Ulrich	k1gMnSc1	Ulrich
si	se	k3xPyFc3	se
zlomil	zlomit	k5eAaPmAgMnS	zlomit
prst	prst	k1gInSc4	prst
u	u	k7c2	u
nohy	noha	k1gFnSc2	noha
<g/>
,	,	kIx,	,
Hammett	Hammett	k1gInSc1	Hammett
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
chvíli	chvíle	k1gFnSc4	chvíle
v	v	k7c6	v
bezvědomí	bezvědomí	k1gNnSc6	bezvědomí
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
neviděl	vidět	k5eNaImAgMnS	vidět
na	na	k7c4	na
jedno	jeden	k4xCgNnSc4	jeden
oko	oko	k1gNnSc4	oko
<g/>
.	.	kIx.	.
</s>
<s>
Burton	Burton	k1gInSc1	Burton
byl	být	k5eAaImAgInS	být
při	při	k7c6	při
smyku	smyk	k1gInSc6	smyk
vyhozen	vyhodit	k5eAaPmNgInS	vyhodit
z	z	k7c2	z
horního	horní	k2eAgNnSc2d1	horní
lůžka	lůžko	k1gNnSc2	lůžko
oknem	okno	k1gNnSc7	okno
<g/>
,	,	kIx,	,
autobus	autobus	k1gInSc1	autobus
na	na	k7c4	na
něho	on	k3xPp3gMnSc4	on
záhy	záhy	k6eAd1	záhy
převrátil	převrátit	k5eAaPmAgMnS	převrátit
<g/>
.	.	kIx.	.
</s>
<s>
Jenom	jenom	k9	jenom
nohy	noha	k1gFnPc1	noha
mu	on	k3xPp3gNnSc3	on
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
vidět	vidět	k5eAaImF	vidět
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
mrtvý	mrtvý	k2eAgMnSc1d1	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Kirk	Kirk	k1gMnSc1	Kirk
mohl	moct	k5eAaImAgMnS	moct
mluvit	mluvit	k5eAaImF	mluvit
o	o	k7c4	o
štěstí	štěstí	k1gNnSc4	štěstí
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
"	"	kIx"	"
<g/>
lůžko	lůžko	k1gNnSc1	lůžko
smrti	smrt	k1gFnSc2	smrt
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
Burton	Burton	k1gInSc4	Burton
ležel	ležet	k5eAaImAgInS	ležet
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
původně	původně	k6eAd1	původně
jeho	jeho	k3xOp3gNnSc1	jeho
<g/>
.	.	kIx.	.
</s>
<s>
Pohřeb	pohřeb	k1gInSc1	pohřeb
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
v	v	k7c6	v
San	San	k1gFnSc6	San
Francisku	Francisek	k1gInSc2	Francisek
<g/>
.	.	kIx.	.
</s>
<s>
Cliffovi	Cliff	k1gMnSc3	Cliff
na	na	k7c4	na
rozloučenou	rozloučená	k1gFnSc4	rozloučená
hrála	hrát	k5eAaImAgFnS	hrát
jeho	jeho	k3xOp3gFnSc1	jeho
skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
Orion	orion	k1gInSc1	orion
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Burtonova	Burtonův	k2eAgFnSc1d1	Burtonova
smrt	smrt	k1gFnSc1	smrt
nechala	nechat	k5eAaPmAgFnS	nechat
budoucnost	budoucnost	k1gFnSc4	budoucnost
skupiny	skupina	k1gFnSc2	skupina
v	v	k7c6	v
nejistotě	nejistota	k1gFnSc6	nejistota
<g/>
.	.	kIx.	.
</s>
<s>
Zbývající	zbývající	k2eAgMnPc1d1	zbývající
členové	člen	k1gMnPc1	člen
se	se	k3xPyFc4	se
rozohodli	rozohodnout	k5eAaPmAgMnP	rozohodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
Burton	Burton	k1gInSc4	Burton
chtěl	chtít	k5eAaImAgMnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
a	a	k8xC	a
s	s	k7c7	s
požehnáním	požehnání	k1gNnSc7	požehnání
Burtonovy	Burtonův	k2eAgFnSc2d1	Burtonova
rodiny	rodina	k1gFnSc2	rodina
hledali	hledat	k5eAaImAgMnP	hledat
náhradu	náhrada	k1gFnSc4	náhrada
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
čtyřicet	čtyřicet	k4xCc1	čtyřicet
lidí	člověk	k1gMnPc2	člověk
zkoušelo	zkoušet	k5eAaImAgNnS	zkoušet
získat	získat	k5eAaPmF	získat
místo	místo	k7c2	místo
baskytaristy	baskytarista	k1gMnSc2	baskytarista
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Lese	lese	k1gFnSc2	lese
Claypoola	Claypoola	k1gFnSc1	Claypoola
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Primus	primus	k1gMnSc1	primus
<g/>
,	,	kIx,	,
Troye	Troy	k1gMnSc2	Troy
Gregoryho	Gregory	k1gMnSc2	Gregory
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Prong	Pronga	k1gFnPc2	Pronga
a	a	k8xC	a
frontmana	frontman	k1gMnSc2	frontman
kapely	kapela	k1gFnSc2	kapela
Flotsam	Flotsam	k1gInSc1	Flotsam
and	and	k?	and
Jetsam	Jetsam	k1gInSc1	Jetsam
<g/>
,	,	kIx,	,
Jasona	Jasona	k1gFnSc1	Jasona
Newsteda	Newsted	k1gMnSc2	Newsted
<g/>
.	.	kIx.	.
</s>
<s>
Newsted	Newsted	k1gMnSc1	Newsted
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
úplně	úplně	k6eAd1	úplně
všechny	všechen	k3xTgFnPc4	všechen
skladby	skladba	k1gFnPc4	skladba
a	a	k8xC	a
po	po	k7c6	po
konkurzu	konkurz	k1gInSc6	konkurz
ho	on	k3xPp3gMnSc4	on
skupina	skupina	k1gFnSc1	skupina
pozvala	pozvat	k5eAaPmAgFnS	pozvat
do	do	k7c2	do
klubu	klub	k1gInSc2	klub
Tommy	Tomma	k1gFnSc2	Tomma
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Joynt	Joynta	k1gFnPc2	Joynta
v	v	k7c6	v
San	San	k1gFnSc6	San
Franciscu	Franciscus	k1gInSc2	Franciscus
<g/>
.	.	kIx.	.
</s>
<s>
Hetfield	Hetfield	k1gMnSc1	Hetfield
<g/>
,	,	kIx,	,
Ulrich	Ulrich	k1gMnSc1	Ulrich
a	a	k8xC	a
Hammett	Hammett	k1gMnSc1	Hammett
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Newsted	Newsted	k1gInSc1	Newsted
je	být	k5eAaImIp3nS	být
ten	ten	k3xDgInSc1	ten
pravý	pravý	k2eAgInSc1d1	pravý
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
má	mít	k5eAaImIp3nS	mít
nahradit	nahradit	k5eAaPmF	nahradit
Burtona	Burtona	k1gFnSc1	Burtona
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
vystoupení	vystoupení	k1gNnSc1	vystoupení
s	s	k7c7	s
Newstedem	Newsted	k1gMnSc7	Newsted
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Country	country	k2eAgInSc6d1	country
Clubu	club	k1gInSc6	club
v	v	k7c6	v
Rededě	Rededa	k1gFnSc6	Rededa
<g/>
,	,	kIx,	,
v	v	k7c6	v
Californii	Californie	k1gFnSc6	Californie
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
Newsted	Newsted	k1gMnSc1	Newsted
přidal	přidat	k5eAaPmAgMnS	přidat
ke	k	k7c3	k
skupině	skupina	k1gFnSc3	skupina
<g/>
,	,	kIx,	,
skupina	skupina	k1gFnSc1	skupina
dokončila	dokončit	k5eAaPmAgFnS	dokončit
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
turné	turné	k1gNnSc2	turné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1987	[number]	k4	1987
si	se	k3xPyFc3	se
Hetfield	Hetfield	k1gMnSc1	Hetfield
podruhé	podruhé	k6eAd1	podruhé
zlomil	zlomit	k5eAaPmAgMnS	zlomit
zápěstí	zápěstí	k1gNnSc4	zápěstí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
1987	[number]	k4	1987
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
"	"	kIx"	"
<g/>
zašila	zašít	k5eAaPmAgFnS	zašít
<g/>
"	"	kIx"	"
v	v	k7c6	v
garáži	garáž	k1gFnSc6	garáž
Larse	Larse	k1gFnSc2	Larse
v	v	k7c4	v
East	East	k1gInSc4	East
Bay	Bay	k1gFnSc2	Bay
v	v	k7c6	v
San	San	k1gFnSc6	San
Franciscu	Franciscus	k1gInSc2	Franciscus
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začaly	začít	k5eAaPmAgFnP	začít
zkoušky	zkouška	k1gFnPc1	zkouška
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgInPc6	který
hráli	hrát	k5eAaImAgMnP	hrát
covery	cover	k1gInPc1	cover
skupin	skupina	k1gFnPc2	skupina
NWOBHM	NWOBHM	kA	NWOBHM
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
se	se	k3xPyFc4	se
Metallica	Metallica	k1gFnSc1	Metallica
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
studia	studio	k1gNnSc2	studio
A.	A.	kA	A.
<g/>
M.	M.	kA	M.
Conway	Conwaa	k1gFnSc2	Conwaa
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nahráli	nahrát	k5eAaBmAgMnP	nahrát
některé	některý	k3yIgFnPc4	některý
skladby	skladba	k1gFnPc4	skladba
z	z	k7c2	z
garážových	garážový	k2eAgFnPc2d1	garážová
zkoušek	zkouška	k1gFnPc2	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
šest	šest	k4xCc4	šest
dnů	den	k1gInPc2	den
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vzešlo	vzejít	k5eAaPmAgNnS	vzejít
minialbum	minialbum	k1gNnSc1	minialbum
The	The	k1gFnSc2	The
$	$	kIx~	$
<g/>
5.98	[number]	k4	5.98
E.	E.	kA	E.
<g/>
P.	P.	kA	P.
Garage	Garage	k1gFnSc1	Garage
Days	Days	k1gInSc1	Days
Re-Revisited	Re-Revisited	k1gInSc1	Re-Revisited
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
nahráno	nahrát	k5eAaPmNgNnS	nahrát
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
zkouška	zkouška	k1gFnSc1	zkouška
nově	nově	k6eAd1	nově
sestaveného	sestavený	k2eAgNnSc2d1	sestavené
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
zkouška	zkouška	k1gFnSc1	zkouška
pro	pro	k7c4	pro
Newsteda	Newsted	k1gMnSc4	Newsted
a	a	k8xC	a
zmírnění	zmírnění	k1gNnSc4	zmírnění
smutku	smutek	k1gInSc2	smutek
a	a	k8xC	a
stresu	stres	k1gInSc2	stres
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Burtona	Burtona	k1gFnSc1	Burtona
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
byla	být	k5eAaImAgFnS	být
premiéra	premiéra	k1gFnSc1	premiéra
této	tento	k3xDgFnSc2	tento
desky	deska	k1gFnSc2	deska
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ale	ale	k9	ale
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
už	už	k9	už
nebyla	být	k5eNaImAgFnS	být
běžně	běžně	k6eAd1	běžně
v	v	k7c6	v
prodeji	prodej	k1gInSc6	prodej
(	(	kIx(	(
<g/>
znovu	znovu	k6eAd1	znovu
ale	ale	k8xC	ale
vyšla	vyjít	k5eAaPmAgFnS	vyjít
jako	jako	k9	jako
bonus	bonus	k1gInSc4	bonus
pozdější	pozdní	k2eAgFnSc2d2	pozdější
desky	deska	k1gFnSc2	deska
"	"	kIx"	"
<g/>
Garage	Garage	k1gFnSc1	Garage
Inc	Inc	k1gFnSc1	Inc
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Video	video	k1gNnSc1	video
nazvané	nazvaný	k2eAgNnSc1d1	nazvané
Cliff	Cliff	k1gInSc4	Cliff
'	'	kIx"	'
<g/>
Em	Ema	k1gFnPc2	Ema
All	All	k1gMnPc2	All
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
připomínalo	připomínat	k5eAaImAgNnS	připomínat
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
Burtona	Burtona	k1gFnSc1	Burtona
v	v	k7c4	v
Metallice	Metallice	k1gFnPc4	Metallice
<g/>
.	.	kIx.	.
</s>
<s>
Video	video	k1gNnSc1	video
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
basových	basový	k2eAgNnPc2d1	basové
sól	sólo	k1gNnPc2	sólo
<g/>
,	,	kIx,	,
domácích	domácí	k2eAgNnPc2d1	domácí
videí	video	k1gNnPc2	video
a	a	k8xC	a
fotek	fotka	k1gFnPc2	fotka
<g/>
.	.	kIx.	.
...	...	k?	...
<g/>
And	Anda	k1gFnPc2	Anda
Justice	justice	k1gFnSc2	justice
for	forum	k1gNnPc2	forum
All	All	k1gMnPc2	All
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
jejího	její	k3xOp3gInSc2	její
názvu	název	k1gInSc2	název
můžeme	moct	k5eAaImIp1nP	moct
hledat	hledat	k5eAaImF	hledat
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
slovech	slovo	k1gNnPc6	slovo
americké	americký	k2eAgFnSc2d1	americká
přísahy	přísaha	k1gFnSc2	přísaha
vlajce	vlajka	k1gFnSc3	vlajka
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
I	i	k9	i
pledge	pledge	k1gFnSc1	pledge
all	all	k?	all
allegiance	allegiance	k1gFnSc2	allegiance
to	ten	k3xDgNnSc1	ten
the	the	k?	the
flag	flag	k1gInSc1	flag
of	of	k?	of
the	the	k?	the
United	United	k1gMnSc1	United
States	States	k1gMnSc1	States
of	of	k?	of
America	America	k1gMnSc1	America
and	and	k?	and
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Republic	Republice	k1gFnPc2	Republice
for	forum	k1gNnPc2	forum
which	which	k1gInSc1	which
it	it	k?	it
stands	stands	k1gInSc1	stands
<g/>
,	,	kIx,	,
one	one	k?	one
nation	nation	k1gInSc1	nation
<g/>
,	,	kIx,	,
under	under	k1gMnSc1	under
God	God	k1gMnSc1	God
<g/>
,	,	kIx,	,
individual	individual	k1gMnSc1	individual
<g/>
,	,	kIx,	,
with	with	k1gMnSc1	with
liberty	libert	k1gMnPc4	libert
and	and	k?	and
justice	justice	k1gFnSc1	justice
for	forum	k1gNnPc2	forum
all	all	k?	all
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Přísahám	přísahat	k5eAaImIp1nS	přísahat
úplnou	úplný	k2eAgFnSc4d1	úplná
věrnost	věrnost	k1gFnSc4	věrnost
vlajce	vlajka	k1gFnSc3	vlajka
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
a	a	k8xC	a
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
za	za	k7c4	za
niž	jenž	k3xRgFnSc4	jenž
vlaje	vlát	k5eAaImIp3nS	vlát
<g/>
,	,	kIx,	,
národu	národ	k1gInSc6	národ
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
Boží	božit	k5eAaImIp3nS	božit
<g/>
,	,	kIx,	,
nezávislému	závislý	k2eNgInSc3d1	nezávislý
<g/>
,	,	kIx,	,
se	s	k7c7	s
svobodou	svoboda	k1gFnSc7	svoboda
a	a	k8xC	a
spravedlností	spravedlnost	k1gFnPc2	spravedlnost
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
komerečním	komereční	k2eAgInSc7d1	komereční
trhákem	trhák	k1gInSc7	trhák
<g/>
,	,	kIx,	,
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
na	na	k7c4	na
šesté	šestý	k4xOgNnSc4	šestý
místo	místo	k1gNnSc4	místo
v	v	k7c4	v
Billboard	billboard	k1gInSc4	billboard
200	[number]	k4	200
a	a	k8xC	a
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
prvním	první	k4xOgMnPc3	první
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
první	první	k4xOgFnSc6	první
desítce	desítka	k1gFnSc6	desítka
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
stalo	stát	k5eAaPmAgNnS	stát
platinové	platinový	k2eAgNnSc1d1	platinové
devět	devět	k4xCc4	devět
týdnů	týden	k1gInPc2	týden
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Metallica	Metallica	k1gFnSc1	Metallica
tak	tak	k6eAd1	tak
strhla	strhnout	k5eAaPmAgFnS	strhnout
z	z	k7c2	z
výsluní	výsluní	k1gNnSc2	výsluní
tehdy	tehdy	k6eAd1	tehdy
módní	módní	k2eAgMnPc1d1	módní
hairmetalové	hairmetal	k1gMnPc1	hairmetal
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Newstedova	Newstedův	k2eAgFnSc1d1	Newstedův
basová	basový	k2eAgFnSc1d1	basová
linka	linka	k1gFnSc1	linka
byla	být	k5eAaImAgFnS	být
úmyslně	úmyslně	k6eAd1	úmyslně
utlumená	utlumený	k2eAgFnSc1d1	utlumená
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
nápady	nápad	k1gInPc1	nápad
byly	být	k5eAaImAgInP	být
ignorovány	ignorovat	k5eAaImNgInP	ignorovat
(	(	kIx(	(
<g/>
dostal	dostat	k5eAaPmAgMnS	dostat
ale	ale	k9	ale
zásluhy	zásluha	k1gFnPc4	zásluha
za	za	k7c4	za
část	část	k1gFnSc4	část
písně	píseň	k1gFnSc2	píseň
Blackened	Blackened	k1gInSc1	Blackened
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k6eAd1	také
bylo	být	k5eAaImAgNnS	být
několik	několik	k4yIc1	několik
stížností	stížnost	k1gFnPc2	stížnost
na	na	k7c4	na
produkci	produkce	k1gFnSc4	produkce
<g/>
:	:	kIx,	:
např.	např.	kA	např.
Steve	Steve	k1gMnSc1	Steve
Huey	Huea	k1gMnSc2	Huea
z	z	k7c2	z
Allmusic	Allmusice	k1gInPc2	Allmusice
si	se	k3xPyFc3	se
všiml	všimnout	k5eAaPmAgMnS	všimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ulrichovy	Ulrichův	k2eAgInPc1d1	Ulrichův
bicí	bicí	k2eAgInPc1d1	bicí
jsou	být	k5eAaImIp3nP	být
více	hodně	k6eAd2	hodně
cvakající	cvakající	k2eAgFnPc1d1	cvakající
než	než	k8xS	než
bušící	bušící	k2eAgFnPc1d1	bušící
a	a	k8xC	a
že	že	k8xS	že
kytary	kytara	k1gFnPc1	kytara
málo	málo	k6eAd1	málo
zní	znět	k5eAaImIp3nP	znět
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc2	turné
k	k	k7c3	k
propagaci	propagace	k1gFnSc3	propagace
alba	album	k1gNnSc2	album
se	se	k3xPyFc4	se
jmenovalo	jmenovat	k5eAaBmAgNnS	jmenovat
The	The	k1gFnSc4	The
Damaged	Damaged	k1gInSc4	Damaged
Justice	justice	k1gFnSc2	justice
tour	toura	k1gFnPc2	toura
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
Metallica	Metallica	k1gFnSc1	Metallica
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
a	a	k8xC	a
roku	rok	k1gInSc2	rok
následujícího	následující	k2eAgInSc2d1	následující
hrála	hrát	k5eAaImAgFnS	hrát
hodně	hodně	k6eAd1	hodně
koncertů	koncert	k1gInPc2	koncert
a	a	k8xC	a
zanedbávala	zanedbávat	k5eAaImAgFnS	zanedbávat
nahrávání	nahrávání	k1gNnSc4	nahrávání
<g/>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
album	album	k1gNnSc1	album
až	až	k9	až
za	za	k7c4	za
rok	rok	k1gInSc4	rok
6	[number]	k4	6
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
předešlých	předešlý	k2eAgFnPc2d1	předešlá
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
zvukově	zvukově	k6eAd1	zvukově
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
určitým	určitý	k2eAgInSc7d1	určitý
posunem	posun	k1gInSc7	posun
i	i	k9	i
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
nominováno	nominovat	k5eAaBmNgNnS	nominovat
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
balladě	balladě	k6eAd1	balladě
One	One	k1gMnPc3	One
byl	být	k5eAaImAgInS	být
natočen	natočen	k2eAgInSc4d1	natočen
první	první	k4xOgInSc4	první
videoklip	videoklip	k1gInSc4	videoklip
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Metallica	Metallica	k6eAd1	Metallica
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vydala	vydat	k5eAaPmAgFnS	vydat
na	na	k7c4	na
velké	velký	k2eAgNnSc4d1	velké
turné	turné	k1gNnSc4	turné
Damage	Damag	k1gFnSc2	Damag
Justice	justice	k1gFnSc2	justice
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
začalo	začít	k5eAaPmAgNnS	začít
už	už	k6eAd1	už
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
a	a	k8xC	a
skončilo	skončit	k5eAaPmAgNnS	skončit
8	[number]	k4	8
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1989	[number]	k4	1989
v	v	k7c6	v
Sao	Sao	k1gMnSc6	Sao
Paulu	Paul	k1gMnSc6	Paul
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
prací	práce	k1gFnSc7	práce
na	na	k7c6	na
novém	nový	k2eAgNnSc6d1	nové
albu	album	k1gNnSc6	album
začala	začít	k5eAaPmAgFnS	začít
kapela	kapela	k1gFnSc1	kapela
hledat	hledat	k5eAaImF	hledat
nového	nový	k2eAgMnSc4d1	nový
producenta	producent	k1gMnSc4	producent
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Rasmussen	Rasmussen	k1gInSc1	Rasmussen
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
názoru	názor	k1gInSc2	názor
členů	člen	k1gInPc2	člen
"	"	kIx"	"
<g/>
moc	moc	k6eAd1	moc
panovačný	panovačný	k2eAgInSc1d1	panovačný
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gInSc4	on
Bob	bob	k1gInSc4	bob
Rock	rock	k1gInSc1	rock
<g/>
.	.	kIx.	.
</s>
<s>
Nahrávání	nahrávání	k1gNnSc1	nahrávání
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1990	[number]	k4	1990
a	a	k8xC	a
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Premiéra	premiéra	k1gFnSc1	premiéra
však	však	k9	však
byla	být	k5eAaImAgFnS	být
několikrát	několikrát	k6eAd1	několikrát
odložena	odložit	k5eAaPmNgFnS	odložit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nahrávání	nahrávání	k1gNnSc1	nahrávání
skončilo	skončit	k5eAaPmAgNnS	skončit
až	až	k9	až
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
i	i	k9	i
několikahodinové	několikahodinový	k2eAgNnSc1d1	několikahodinové
video	video	k1gNnSc1	video
ze	z	k7c2	z
zákulisí	zákulisí	k1gNnSc2	zákulisí
studia	studio	k1gNnSc2	studio
(	(	kIx(	(
<g/>
A	a	k9	a
Year	Year	k1gInSc1	Year
and	and	k?	and
a	a	k8xC	a
Half	halfa	k1gFnPc2	halfa
in	in	k?	in
the	the	k?	the
Life	Life	k1gInSc1	Life
of	of	k?	of
Metallica	Metallic	k1gInSc2	Metallic
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Premiéra	premiéra	k1gFnSc1	premiéra
samotné	samotný	k2eAgFnSc2d1	samotná
desky	deska	k1gFnSc2	deska
byla	být	k5eAaImAgFnS	být
až	až	k9	až
12	[number]	k4	12
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
dostalo	dostat	k5eAaPmAgNnS	dostat
jednoduchý	jednoduchý	k2eAgInSc4d1	jednoduchý
název	název	k1gInSc4	název
Metallica	Metallic	k1gInSc2	Metallic
<g/>
,	,	kIx,	,
říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
také	také	k9	také
Black	Black	k1gInSc1	Black
Album	album	k1gNnSc1	album
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
nejznámějším	známý	k2eAgMnSc7d3	nejznámější
a	a	k8xC	a
mělo	mít	k5eAaImAgNnS	mít
také	také	k9	také
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
prodejnost	prodejnost	k1gFnSc4	prodejnost
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
jeho	jeho	k3xOp3gFnPc1	jeho
skladby	skladba	k1gFnPc1	skladba
už	už	k6eAd1	už
byly	být	k5eAaImAgFnP	být
přijatelné	přijatelný	k2eAgInPc1d1	přijatelný
i	i	k9	i
pro	pro	k7c4	pro
mainstreamová	mainstreamový	k2eAgNnPc4d1	mainstreamové
média	médium	k1gNnPc4	médium
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
jejich	jejich	k3xOp3gFnSc4	jejich
popularita	popularita	k1gFnSc1	popularita
dále	daleko	k6eAd2	daleko
rostla	růst	k5eAaImAgFnS	růst
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1999	[number]	k4	1999
Metallica	Metallica	k1gFnSc1	Metallica
získala	získat	k5eAaPmAgFnS	získat
za	za	k7c4	za
11	[number]	k4	11
miliónů	milión	k4xCgInPc2	milión
prodaných	prodaný	k2eAgInPc2d1	prodaný
nosičů	nosič	k1gInPc2	nosič
Black	Blacka	k1gFnPc2	Blacka
Album	album	k1gNnSc4	album
ocenění	ocenění	k1gNnSc2	ocenění
Diamond	Diamonda	k1gFnPc2	Diamonda
Award	Awarda	k1gFnPc2	Awarda
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
stálo	stát	k5eAaImAgNnS	stát
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgNnPc2d3	nejznámější
alb	album	k1gNnPc2	album
v	v	k7c6	v
rockové	rockový	k2eAgFnSc6d1	rocková
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
se	se	k3xPyFc4	se
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
stala	stát	k5eAaPmAgFnS	stát
komerčně	komerčně	k6eAd1	komerčně
úspěšnější	úspěšný	k2eAgFnSc1d2	úspěšnější
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
i	i	k9	i
na	na	k7c4	na
zvětšování	zvětšování	k1gNnSc4	zvětšování
koncertního	koncertní	k2eAgNnSc2d1	koncertní
pódia	pódium	k1gNnSc2	pódium
a	a	k8xC	a
přidávání	přidávání	k1gNnSc2	přidávání
dalších	další	k2eAgInPc2d1	další
efektů	efekt	k1gInPc2	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Metallica	Metallica	k6eAd1	Metallica
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vydala	vydat	k5eAaPmAgFnS	vydat
na	na	k7c6	na
turné	turné	k1gNnSc6	turné
s	s	k7c7	s
Guns	Gunsa	k1gFnPc2	Gunsa
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
koncertě	koncert	k1gInSc6	koncert
v	v	k7c6	v
Montrealu	Montreal	k1gInSc6	Montreal
ohňostroj	ohňostroj	k1gInSc1	ohňostroj
vybuchl	vybuchnout	k5eAaPmAgInS	vybuchnout
v	v	k7c4	v
nepravou	pravý	k2eNgFnSc4d1	nepravá
chvíli	chvíle	k1gFnSc4	chvíle
a	a	k8xC	a
silně	silně	k6eAd1	silně
popálil	popálit	k5eAaPmAgMnS	popálit
Jamese	Jamesa	k1gFnSc3	Jamesa
<g/>
.	.	kIx.	.
</s>
<s>
Metallica	Metallica	k1gFnSc1	Metallica
tak	tak	k6eAd1	tak
zrušila	zrušit	k5eAaPmAgFnS	zrušit
několik	několik	k4yIc4	několik
vystoupení	vystoupení	k1gNnPc2	vystoupení
a	a	k8xC	a
po	po	k7c4	po
zbytek	zbytek	k1gInSc4	zbytek
turné	turné	k1gNnSc2	turné
James	Jamesa	k1gFnPc2	Jamesa
jenom	jenom	k9	jenom
zpíval	zpívat	k5eAaImAgMnS	zpívat
<g/>
;	;	kIx,	;
znovu	znovu	k6eAd1	znovu
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
"	"	kIx"	"
<g/>
zaskočil	zaskočit	k5eAaPmAgMnS	zaskočit
<g/>
"	"	kIx"	"
John	John	k1gMnSc1	John
Marshal	Marshal	k1gMnSc1	Marshal
<g/>
,	,	kIx,	,
tak	tak	k9	tak
jako	jako	k9	jako
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
když	když	k8xS	když
měl	mít	k5eAaImAgInS	mít
James	James	k1gInSc1	James
několikrát	několikrát	k6eAd1	několikrát
zlomenou	zlomený	k2eAgFnSc4d1	zlomená
ruku	ruka	k1gFnSc4	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krátké	krátký	k2eAgFnSc6d1	krátká
pauze	pauza	k1gFnSc6	pauza
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1992	[number]	k4	1992
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
vydává	vydávat	k5eAaPmIp3nS	vydávat
na	na	k7c4	na
evropskou	evropský	k2eAgFnSc4d1	Evropská
Wherever	Wherever	k1gMnSc1	Wherever
I	I	kA	I
May	May	k1gMnSc1	May
Roam	Roam	k1gMnSc1	Roam
Tour	Tour	k1gMnSc1	Tour
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
prosince	prosinec	k1gInSc2	prosinec
koncertovala	koncertovat	k5eAaImAgFnS	koncertovat
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
"	"	kIx"	"
<g/>
šňůře	šňůra	k1gFnSc6	šňůra
<g/>
"	"	kIx"	"
koncertů	koncert	k1gInPc2	koncert
si	se	k3xPyFc3	se
členové	člen	k1gMnPc1	člen
dopřáli	dopřát	k5eAaPmAgMnP	dopřát
měsíční	měsíční	k2eAgNnSc4d1	měsíční
volno	volno	k1gNnSc4	volno
<g/>
.	.	kIx.	.
</s>
<s>
Věnovali	věnovat	k5eAaImAgMnP	věnovat
se	se	k3xPyFc4	se
hlavně	hlavně	k6eAd1	hlavně
rodině	rodina	k1gFnSc3	rodina
a	a	k8xC	a
svým	svůj	k3xOyFgInPc3	svůj
zájmům	zájem	k1gInPc3	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Kirk	Kirk	k1gInSc1	Kirk
započal	započnout	k5eAaPmAgInS	započnout
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
časopisem	časopis	k1gInSc7	časopis
Guitar	Guitar	k1gMnSc1	Guitar
World	World	k1gMnSc1	World
<g/>
.	.	kIx.	.
</s>
<s>
Prostřednictvím	prostřednictvím	k7c2	prostřednictvím
tohoto	tento	k3xDgInSc2	tento
časopisu	časopis	k1gInSc2	časopis
dával	dávat	k5eAaImAgInS	dávat
kurzy	kurz	k1gInPc4	kurz
hry	hra	k1gFnSc2	hra
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
měsíci	měsíc	k1gInSc6	měsíc
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
série	série	k1gFnSc1	série
koncertů	koncert	k1gInPc2	koncert
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
na	na	k7c6	na
stadiónu	stadión	k1gInSc6	stadión
FC	FC	kA	FC
Boby	bob	k1gInPc1	bob
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
skončila	skončit	k5eAaPmAgFnS	skončit
šňůra	šňůra	k1gFnSc1	šňůra
koncertů	koncert	k1gInPc2	koncert
propagujících	propagující	k2eAgInPc2d1	propagující
"	"	kIx"	"
<g/>
Černé	černé	k1gNnSc1	černé
Album	album	k1gNnSc1	album
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
vychází	vycházet	k5eAaImIp3nS	vycházet
boxset	boxset	k5eAaImF	boxset
Live	Live	k1gInSc1	Live
Shit	Shit	k2eAgInSc1d1	Shit
<g/>
:	:	kIx,	:
Binge	Binge	k1gInSc1	Binge
&	&	k?	&
Purge	Purg	k1gInSc2	Purg
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc1	první
živé	živý	k2eAgNnSc1d1	živé
album	album	k1gNnSc1	album
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
James	Jamesa	k1gFnPc2	Jamesa
a	a	k8xC	a
Lars	Larsa	k1gFnPc2	Larsa
začali	začít	k5eAaPmAgMnP	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
novém	nový	k2eAgNnSc6d1	nové
albu	album	k1gNnSc6	album
<g/>
;	;	kIx,	;
i	i	k9	i
když	když	k8xS	když
předem	předem	k6eAd1	předem
věděli	vědět	k5eAaImAgMnP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebude	být	k5eNaImBp3nS	být
jako	jako	k9	jako
předchozí	předchozí	k2eAgMnSc1d1	předchozí
<g/>
.	.	kIx.	.
</s>
<s>
Lars	Lars	k6eAd1	Lars
chtěl	chtít	k5eAaImAgMnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
kapela	kapela	k1gFnSc1	kapela
"	"	kIx"	"
<g/>
stárla	stárnout	k5eAaImAgFnS	stárnout
s	s	k7c7	s
hrdostí	hrdost	k1gFnSc7	hrdost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
udělala	udělat	k5eAaPmAgFnS	udělat
Metallica	Metallica	k1gFnSc1	Metallica
na	na	k7c4	na
metalovou	metalový	k2eAgFnSc4d1	metalová
skupinu	skupina	k1gFnSc4	skupina
revoluční	revoluční	k2eAgInSc4d1	revoluční
počin	počin	k1gInSc4	počin
<g/>
:	:	kIx,	:
všichni	všechen	k3xTgMnPc1	všechen
si	se	k3xPyFc3	se
ostříhali	ostříhat	k5eAaPmAgMnP	ostříhat
vlasy	vlas	k1gInPc4	vlas
(	(	kIx(	(
<g/>
Newsted	Newsted	k1gMnSc1	Newsted
tak	tak	k6eAd1	tak
učinil	učinit	k5eAaPmAgMnS	učinit
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skladby	skladba	k1gFnPc1	skladba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tentokrát	tentokrát	k6eAd1	tentokrát
nesly	nést	k5eAaImAgFnP	nést
místy	místy	k6eAd1	místy
až	až	k6eAd1	až
bluesový	bluesový	k2eAgMnSc1d1	bluesový
<g/>
,	,	kIx,	,
countryový	countryový	k2eAgMnSc1d1	countryový
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
hard	hard	k6eAd1	hard
rockový	rockový	k2eAgInSc4d1	rockový
feeling	feeling	k1gInSc4	feeling
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
hotovy	hotov	k2eAgInPc1d1	hotov
už	už	k6eAd1	už
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
celé	celý	k2eAgNnSc1d1	celé
nahrávání	nahrávání	k1gNnSc1	nahrávání
trvalo	trvat	k5eAaImAgNnS	trvat
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Load	Loada	k1gFnPc2	Loada
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
u	u	k7c2	u
Polygramu	Polygram	k1gInSc2	Polygram
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
nahrávalo	nahrávat	k5eAaImAgNnS	nahrávat
se	se	k3xPyFc4	se
v	v	k7c6	v
The	The	k1gFnSc6	The
Plant	planta	k1gFnPc2	planta
Studios	Studios	k?	Studios
v	v	k7c6	v
Sausalitu	Sausalit	k1gInSc6	Sausalit
<g/>
,	,	kIx,	,
nedaleko	nedaleko	k7c2	nedaleko
San	San	k1gMnSc2	San
Francisca	Franciscus	k1gMnSc2	Franciscus
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Boba	Bob	k1gMnSc2	Bob
Rocka	Rocek	k1gMnSc2	Rocek
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
práci	práce	k1gFnSc6	práce
na	na	k7c6	na
albu	album	k1gNnSc6	album
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
něco	něco	k6eAd1	něco
kolem	kolem	k7c2	kolem
třiceti	třicet	k4xCc2	třicet
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
na	na	k7c4	na
Load	Load	k1gInSc4	Load
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
dostalo	dostat	k5eAaPmAgNnS	dostat
jen	jen	k9	jen
čtrnáct	čtrnáct	k4xCc1	čtrnáct
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
bylo	být	k5eAaImAgNnS	být
nahráno	nahrát	k5eAaBmNgNnS	nahrát
pokračování	pokračování	k1gNnSc1	pokračování
Loadu	Load	k1gInSc2	Load
<g/>
:	:	kIx,	:
ReLoad	ReLoad	k1gInSc1	ReLoad
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
opět	opět	k6eAd1	opět
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
The	The	k1gFnSc2	The
Plant	planta	k1gFnPc2	planta
mezi	mezi	k7c7	mezi
květnem	květno	k1gNnSc7	květno
1995	[number]	k4	1995
a	a	k8xC	a
únorem	únor	k1gInSc7	únor
1996	[number]	k4	1996
a	a	k8xC	a
od	od	k7c2	od
července	červenec	k1gInSc2	červenec
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Mixovalo	mixovat	k5eAaImAgNnS	mixovat
se	se	k3xPyFc4	se
tamtéž	tamtéž	k6eAd1	tamtéž
v	v	k7c6	v
září	září	k1gNnSc6	září
a	a	k8xC	a
říjnu	říjen	k1gInSc6	říjen
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Kapele	kapela	k1gFnSc3	kapela
na	na	k7c6	na
desce	deska	k1gFnSc6	deska
hostovala	hostovat	k5eAaImAgFnS	hostovat
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Marianne	Mariann	k1gInSc5	Mariann
Faithfull	Faithfull	k1gMnSc1	Faithfull
(	(	kIx(	(
<g/>
v	v	k7c6	v
písni	píseň	k1gFnSc6	píseň
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Memory	Memora	k1gFnSc2	Memora
Remains	Remainsa	k1gFnPc2	Remainsa
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
propagující	propagující	k2eAgFnSc2d1	propagující
tato	tento	k3xDgNnPc4	tento
dvě	dva	k4xCgNnPc4	dva
alba	album	k1gNnPc4	album
trvalo	trvat	k5eAaImAgNnS	trvat
až	až	k9	až
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
Load	Load	k1gInSc4	Load
a	a	k8xC	a
ReLoad	ReLoad	k1gInSc4	ReLoad
jsou	být	k5eAaImIp3nP	být
znatelné	znatelný	k2eAgInPc1d1	znatelný
rozdíly	rozdíl	k1gInPc1	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Load	Load	k6eAd1	Load
má	mít	k5eAaImIp3nS	mít
fotografii	fotografia	k1gFnSc4	fotografia
obalu	obal	k1gInSc2	obal
z	z	k7c2	z
autorova	autorův	k2eAgNnSc2d1	autorovo
spermatu	sperma	k1gNnSc2	sperma
a	a	k8xC	a
kravské	kravský	k2eAgFnSc2d1	kravská
krve	krev	k1gFnSc2	krev
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
jemnější	jemný	k2eAgInSc4d2	jemnější
zvuk	zvuk	k1gInSc4	zvuk
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ReLoad	ReLoad	k1gInSc1	ReLoad
má	mít	k5eAaImIp3nS	mít
fotografii	fotografia	k1gFnSc4	fotografia
obalu	obal	k1gInSc2	obal
z	z	k7c2	z
autorovy	autorův	k2eAgFnSc2d1	autorova
moči	moč	k1gFnSc2	moč
a	a	k8xC	a
kravské	kravský	k2eAgFnSc2d1	kravská
krve	krev	k1gFnSc2	krev
a	a	k8xC	a
texty	text	k1gInPc1	text
a	a	k8xC	a
hudba	hudba	k1gFnSc1	hudba
jsou	být	k5eAaImIp3nP	být
o	o	k7c4	o
poznání	poznání	k1gNnSc4	poznání
temnější	temný	k2eAgFnSc2d2	temnější
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
den	den	k1gInSc4	den
po	po	k7c6	po
koncertě	koncert	k1gInSc6	koncert
v	v	k7c6	v
San	San	k1gFnSc6	San
Diegu	Dieg	k1gInSc2	Dieg
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Metallica	Metallica	k1gFnSc1	Metallica
znovu	znovu	k6eAd1	znovu
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nahrála	nahrát	k5eAaPmAgFnS	nahrát
nové	nový	k2eAgFnPc4d1	nová
písně	píseň	k1gFnPc4	píseň
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
další	další	k2eAgNnSc4d1	další
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Garage	Garage	k1gFnSc1	Garage
Inc	Inc	k1gFnSc2	Inc
<g/>
.	.	kIx.	.
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1998	[number]	k4	1998
jako	jako	k8xS	jako
dvojCD	dvojCD	k?	dvojCD
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
všechny	všechen	k3xTgInPc4	všechen
dosud	dosud	k6eAd1	dosud
nahrané	nahraný	k2eAgInPc4d1	nahraný
cover	cover	k1gInSc4	cover
songy	song	k1gInPc4	song
Metallicy	Metallic	k2eAgInPc4d1	Metallic
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
11	[number]	k4	11
nových	nový	k2eAgMnPc2d1	nový
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1998	[number]	k4	1998
byly	být	k5eAaImAgFnP	být
vydány	vydat	k5eAaPmNgFnP	vydat
i	i	k9	i
videokazety	videokazeta	k1gFnPc1	videokazeta
<g/>
,	,	kIx,	,
dokumentující	dokumentující	k2eAgInPc1d1	dokumentující
koncerty	koncert	k1gInPc1	koncert
ve	v	k7c6	v
Fort	Fort	k?	Fort
Worth	Worth	k1gMnSc1	Worth
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
a	a	k8xC	a
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Režisérem	režisér	k1gMnSc7	režisér
byl	být	k5eAaImAgInS	být
Wayne	Wayn	k1gInSc5	Wayn
Isham	Isham	k1gInSc4	Isham
<g/>
.	.	kIx.	.
</s>
<s>
Koncert	koncert	k1gInSc1	koncert
<g/>
,	,	kIx,	,
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
Cunning	Cunning	k1gInSc1	Cunning
Stunts	Stunts	k1gInSc1	Stunts
byl	být	k5eAaImAgInS	být
rovněž	rovněž	k9	rovněž
vydán	vydán	k2eAgInSc1d1	vydán
na	na	k7c6	na
dvou	dva	k4xCgInPc6	dva
discích	disk	k1gInPc6	disk
DVD	DVD	kA	DVD
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
prvním	první	k4xOgMnSc7	první
DVD	DVD	kA	DVD
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1999	[number]	k4	1999
kapelu	kapela	k1gFnSc4	kapela
kontaktoval	kontaktovat	k5eAaImAgInS	kontaktovat
dirigent	dirigent	k1gMnSc1	dirigent
a	a	k8xC	a
skladatel	skladatel	k1gMnSc1	skladatel
filmové	filmový	k2eAgFnSc2d1	filmová
hudby	hudba	k1gFnSc2	hudba
Michael	Michael	k1gMnSc1	Michael
Kamen	kamna	k1gNnPc2	kamna
<g/>
.	.	kIx.	.
</s>
<s>
Navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
by	by	kYmCp3nS	by
Metallica	Metallica	k1gFnSc1	Metallica
nechtěla	chtít	k5eNaImAgFnS	chtít
nahrát	nahrát	k5eAaBmF	nahrát
pár	pár	k4xCyI	pár
koncertů	koncert	k1gInPc2	koncert
se	s	k7c7	s
San	San	k1gMnSc1	San
Franciským	Franciský	k2eAgInSc7d1	Franciský
symfonickým	symfonický	k2eAgInSc7d1	symfonický
orchestrem	orchestr	k1gInSc7	orchestr
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
a	a	k8xC	a
22	[number]	k4	22
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1999	[number]	k4	1999
se	se	k3xPyFc4	se
v	v	k7c4	v
Berkeley	Berkele	k2eAgFnPc4d1	Berkele
Community	Communita	k1gFnPc4	Communita
Theatre	Theatr	k1gInSc5	Theatr
v	v	k7c6	v
San	San	k1gFnPc2	San
Franciscu	Franciscus	k1gInSc2	Franciscus
uskutečnily	uskutečnit	k5eAaPmAgInP	uskutečnit
dva	dva	k4xCgInPc1	dva
velmi	velmi	k6eAd1	velmi
neobyčejné	obyčejný	k2eNgInPc1d1	neobyčejný
koncerty	koncert	k1gInPc1	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
zazněly	zaznět	k5eAaImAgFnP	zaznět
skladby	skladba	k1gFnPc1	skladba
ze	z	k7c2	z
všech	všecek	k3xTgNnPc2	všecek
alb	album	k1gNnPc2	album
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
alba	album	k1gNnPc4	album
Kill	Killa	k1gFnPc2	Killa
'	'	kIx"	'
<g/>
Em	Ema	k1gFnPc2	Ema
All	All	k1gMnPc2	All
<g/>
.	.	kIx.	.
</s>
<s>
Metallica	Metallica	k1gFnSc1	Metallica
vydala	vydat	k5eAaPmAgFnS	vydat
záznam	záznam	k1gInSc4	záznam
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
koncertů	koncert	k1gInPc2	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc1	název
S	s	k7c7	s
<g/>
&	&	k?	&
<g/>
M.	M.	kA	M.
Vyšlo	vyjít	k5eAaPmAgNnS	vyjít
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1999	[number]	k4	1999
na	na	k7c6	na
dvojcédéčku	dvojcédéček	k1gInSc6	dvojcédéček
<g/>
.	.	kIx.	.
</s>
<s>
Koncerty	koncert	k1gInPc4	koncert
natáčel	natáčet	k5eAaImAgInS	natáčet
i	i	k9	i
tým	tým	k1gInSc1	tým
režiséra	režisér	k1gMnSc2	režisér
Wayneho	Wayne	k1gMnSc2	Wayne
Ishama	Isham	k1gMnSc2	Isham
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
koncert	koncert	k1gInSc1	koncert
dostupný	dostupný	k2eAgInSc1d1	dostupný
také	také	k9	také
na	na	k7c6	na
videokazetě	videokazeta	k1gFnSc6	videokazeta
a	a	k8xC	a
DVD	DVD	kA	DVD
<g/>
.	.	kIx.	.
</s>
<s>
Metallica	Metallica	k6eAd1	Metallica
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
vydání	vydání	k1gNnSc2	vydání
alba	album	k1gNnSc2	album
odehrála	odehrát	k5eAaPmAgFnS	odehrát
ještě	ještě	k6eAd1	ještě
další	další	k2eAgInPc4d1	další
dva	dva	k4xCgInPc4	dva
koncerty	koncert	k1gInPc4	koncert
s	s	k7c7	s
orchestry	orchestr	k1gInPc7	orchestr
-	-	kIx~	-
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
a	a	k8xC	a
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
dvě	dva	k4xCgFnPc4	dva
nové	nový	k2eAgFnPc4d1	nová
skladby	skladba	k1gFnPc4	skladba
-	-	kIx~	-
pomalou	pomalý	k2eAgFnSc4d1	pomalá
"	"	kIx"	"
<g/>
-	-	kIx~	-
Human	Human	k1gInSc1	Human
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
čti	číst	k5eAaImRp2nS	číst
majnus	majnus	k1gMnSc1	majnus
hjumn	hjumn	k1gMnSc1	hjumn
<g/>
)	)	kIx)	)
a	a	k8xC	a
No	no	k9	no
Leaf	Leaf	k1gMnSc1	Leaf
Clover	Clover	k1gMnSc1	Clover
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
typickou	typický	k2eAgFnSc7d1	typická
metalovou	metalový	k2eAgFnSc7d1	metalová
baladou	balada	k1gFnSc7	balada
<g/>
;	;	kIx,	;
jemné	jemný	k2eAgFnPc4d1	jemná
pasáže	pasáž	k1gFnPc4	pasáž
se	se	k3xPyFc4	se
střídají	střídat	k5eAaImIp3nP	střídat
s	s	k7c7	s
tvrdšími	tvrdý	k2eAgMnPc7d2	tvrdší
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
albem	album	k1gNnSc7	album
Metallica	Metallic	k1gInSc2	Metallic
překonala	překonat	k5eAaPmAgFnS	překonat
další	další	k2eAgFnPc4d1	další
bariéry	bariéra	k1gFnPc4	bariéra
a	a	k8xC	a
původní	původní	k2eAgFnPc4d1	původní
představy	představa	k1gFnPc4	představa
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
je	být	k5eAaImIp3nS	být
a	a	k8xC	a
co	co	k3yRnSc1	co
není	být	k5eNaImIp3nS	být
Metallica	Metallica	k1gFnSc1	Metallica
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
Metallica	Metallica	k1gFnSc1	Metallica
nahrála	nahrát	k5eAaPmAgFnS	nahrát
poprvé	poprvé	k6eAd1	poprvé
skladbu	skladba	k1gFnSc4	skladba
pro	pro	k7c4	pro
soundtrack	soundtrack	k1gInSc4	soundtrack
-	-	kIx~	-
"	"	kIx"	"
<g/>
I	i	k9	i
Disappear	Disappear	k1gInSc1	Disappear
<g/>
"	"	kIx"	"
-	-	kIx~	-
k	k	k7c3	k
filmu	film	k1gInSc3	film
Mission	Mission	k1gInSc1	Mission
<g/>
:	:	kIx,	:
<g/>
Imposible	Imposible	k1gFnSc1	Imposible
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Právníci	právník	k1gMnPc1	právník
kapely	kapela	k1gFnSc2	kapela
rozpoutali	rozpoutat	k5eAaPmAgMnP	rozpoutat
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
soudních	soudní	k2eAgInPc2d1	soudní
procesů	proces	k1gInPc2	proces
v	v	k7c6	v
hudební	hudební	k2eAgFnSc6d1	hudební
historii	historie	k1gFnSc6	historie
-	-	kIx~	-
kvůli	kvůli	k7c3	kvůli
zpřístupňování	zpřístupňování	k1gNnSc3	zpřístupňování
muziky	muzika	k1gFnSc2	muzika
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
ve	v	k7c6	v
formátu	formát	k1gInSc6	formát
MP3	MP3	k1gFnSc2	MP3
zdarma	zdarma	k6eAd1	zdarma
proti	proti	k7c3	proti
firmě	firma	k1gFnSc3	firma
Napster	Napstra	k1gFnPc2	Napstra
<g/>
.	.	kIx.	.
</s>
<s>
Udělali	udělat	k5eAaPmAgMnP	udělat
si	se	k3xPyFc3	se
tak	tak	k6eAd1	tak
mnoho	mnoho	k4c1	mnoho
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
bránili	bránit	k5eAaImAgMnP	bránit
tím	ten	k3xDgNnSc7	ten
autorská	autorský	k2eAgNnPc1d1	autorské
práva	právo	k1gNnPc1	právo
nejen	nejen	k6eAd1	nejen
svých	svůj	k3xOyFgFnPc2	svůj
písní	píseň	k1gFnPc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Lars	Lars	k1gInSc1	Lars
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
stál	stát	k5eAaImAgInS	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
tohoto	tento	k3xDgInSc2	tento
sporu	spor	k1gInSc2	spor
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
soud	soud	k1gInSc1	soud
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
Metallica	Metallic	k1gInSc2	Metallic
hrála	hrát	k5eAaImAgFnS	hrát
na	na	k7c6	na
13	[number]	k4	13
koncertech	koncert	k1gInPc6	koncert
v	v	k7c4	v
USA	USA	kA	USA
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Summer	Summra	k1gFnPc2	Summra
Sanitarium	Sanitarium	k1gNnSc1	Sanitarium
Tour	Tour	k1gMnSc1	Tour
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
Metallicy	Metallica	k1gMnSc2	Metallica
nedělo	dít	k5eNaBmAgNnS	dít
nic	nic	k3yNnSc1	nic
zajímavého	zajímavý	k2eAgNnSc2d1	zajímavé
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
pravá	pravý	k2eAgFnSc1d1	pravá
senzace	senzace	k1gFnSc1	senzace
teprve	teprve	k6eAd1	teprve
měla	mít	k5eAaImAgFnS	mít
přijít	přijít	k5eAaPmF	přijít
s	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
nového	nový	k2eAgNnSc2d1	nové
milénia	milénium	k1gNnSc2	milénium
<g/>
.	.	kIx.	.
</s>
<s>
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2001	[number]	k4	2001
z	z	k7c2	z
Metallicy	Metallica	k1gFnSc2	Metallica
odchází	odcházet	k5eAaImIp3nS	odcházet
Jason	Jason	k1gMnSc1	Jason
Newsted	Newsted	k1gMnSc1	Newsted
-	-	kIx~	-
jako	jako	k8xS	jako
důvod	důvod	k1gInSc4	důvod
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
hraní	hraní	k1gNnSc4	hraní
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
miluje	milovat	k5eAaImIp3nS	milovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
odrazilo	odrazit	k5eAaPmAgNnS	odrazit
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
zdraví	zdraví	k1gNnSc6	zdraví
jak	jak	k8xC	jak
fyzickém	fyzický	k2eAgInSc6d1	fyzický
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
psychickém	psychický	k2eAgInSc6d1	psychický
<g/>
.	.	kIx.	.
</s>
<s>
Metallica	Metallica	k1gFnSc1	Metallica
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
znovu	znovu	k6eAd1	znovu
vešla	vejít	k5eAaPmAgNnP	vejít
do	do	k7c2	do
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
postu	posta	k1gFnSc4	posta
baskytaristy	baskytarista	k1gMnSc2	baskytarista
zaměstnali	zaměstnat	k5eAaPmAgMnP	zaměstnat
producenta	producent	k1gMnSc4	producent
Boba	Bob	k1gMnSc4	Bob
Rocka	Rocek	k1gMnSc4	Rocek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
července	červenec	k1gInSc2	červenec
se	se	k3xPyFc4	se
Hetfield	Hetfield	k1gInSc1	Hetfield
ocitl	ocitnout	k5eAaPmAgInS	ocitnout
na	na	k7c6	na
odvykací	odvykací	k2eAgFnSc6d1	odvykací
kůře	kůra	k1gFnSc6	kůra
ze	z	k7c2	z
závislosti	závislost	k1gFnSc2	závislost
na	na	k7c6	na
alkoholu	alkohol	k1gInSc6	alkohol
a	a	k8xC	a
jiných	jiný	k2eAgFnPc6d1	jiná
látkách	látka	k1gFnPc6	látka
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
,	,	kIx,	,
po	po	k7c6	po
půl	půl	k1xP	půl
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
Hetfield	Hetfield	k1gInSc1	Hetfield
konečně	konečně	k6eAd1	konečně
ukončil	ukončit	k5eAaPmAgInS	ukončit
svoje	svůj	k3xOyFgNnSc4	svůj
léčení	léčení	k1gNnSc4	léčení
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
přinesl	přinést	k5eAaPmAgInS	přinést
informaci	informace	k1gFnSc4	informace
<g/>
,	,	kIx,	,
že	že	k8xS	že
režisérské	režisérský	k2eAgNnSc1d1	režisérské
duo	duo	k1gNnSc1	duo
Joe	Joe	k1gMnSc1	Joe
Berlinger	Berlinger	k1gMnSc1	Berlinger
a	a	k8xC	a
Bruce	Bruce	k1gMnSc1	Bruce
Sinofsky	Sinofsky	k1gMnSc1	Sinofsky
pracují	pracovat	k5eAaImIp3nP	pracovat
na	na	k7c6	na
dokumentárním	dokumentární	k2eAgInSc6d1	dokumentární
filmu	film	k1gInSc6	film
o	o	k7c6	o
Metallice	Metallika	k1gFnSc6	Metallika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2002	[number]	k4	2002
oficiální	oficiální	k2eAgInSc4d1	oficiální
fanclub	fanclub	k1gInSc4	fanclub
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Metallica	Metallica	k1gFnSc1	Metallica
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
prosince	prosinec	k1gInSc2	prosinec
začala	začít	k5eAaPmAgFnS	začít
Metallica	Metallica	k1gFnSc1	Metallica
hledat	hledat	k5eAaImF	hledat
nového	nový	k2eAgMnSc4d1	nový
baskytaristu	baskytarista	k1gMnSc4	baskytarista
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
vydal	vydat	k5eAaPmAgMnS	vydat
na	na	k7c4	na
koncertní	koncertní	k2eAgFnSc4d1	koncertní
šňůru	šňůra	k1gFnSc4	šňůra
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
jméno	jméno	k1gNnSc1	jméno
ale	ale	k8xC	ale
nepadlo	padnout	k5eNaPmAgNnS	padnout
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
kandidáty	kandidát	k1gMnPc7	kandidát
byli	být	k5eAaImAgMnP	být
třeba	třeba	k6eAd1	třeba
<g/>
:	:	kIx,	:
Mike	Mike	k1gFnSc1	Mike
Inez	Inez	k1gMnSc1	Inez
(	(	kIx(	(
<g/>
Alice	Alice	k1gFnSc1	Alice
in	in	k?	in
Chains	Chainsa	k1gFnPc2	Chainsa
<g/>
,	,	kIx,	,
Ozzy	Ozza	k1gFnSc2	Ozza
Osbourne	Osbourn	k1gInSc5	Osbourn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Joey	Joea	k1gMnSc2	Joea
Vera	Verus	k1gMnSc2	Verus
(	(	kIx(	(
<g/>
Armored	Armored	k1gMnSc1	Armored
Saint	Saint	k1gMnSc1	Saint
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
ledna	leden	k1gInSc2	leden
byli	být	k5eAaImAgMnP	být
dopsáni	dopsat	k5eAaPmNgMnP	dopsat
Rob	robit	k5eAaImRp2nS	robit
Trujillo	Trujillo	k1gNnSc4	Trujillo
(	(	kIx(	(
<g/>
ex-	ex-	k?	ex-
Suicidal	Suicidal	k1gMnSc1	Suicidal
Tendancies	Tendancies	k1gMnSc1	Tendancies
<g/>
,	,	kIx,	,
ex-	ex-	k?	ex-
Ozzy	Ozza	k1gFnSc2	Ozza
Osbourne	Osbourn	k1gInSc5	Osbourn
<g/>
)	)	kIx)	)
a	a	k8xC	a
Eric	Eric	k1gFnSc1	Eric
Forrest	Forrest	k1gFnSc1	Forrest
(	(	kIx(	(
<g/>
ex-	ex-	k?	ex-
VoiVod	VoiVod	k1gInSc1	VoiVod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejnovější	nový	k2eAgFnPc1d3	nejnovější
informace	informace	k1gFnPc1	informace
říkaly	říkat	k5eAaImAgFnP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nové	nový	k2eAgNnSc1d1	nové
album	album	k1gNnSc1	album
St.	st.	kA	st.
Anger	Angra	k1gFnPc2	Angra
má	mít	k5eAaImIp3nS	mít
objevit	objevit	k5eAaPmF	objevit
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
A	a	k9	a
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
poslední	poslední	k2eAgFnPc1d1	poslední
nahrávky	nahrávka	k1gFnPc1	nahrávka
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
nejvíc	hodně	k6eAd3	hodně
se	se	k3xPyFc4	se
spekulovalo	spekulovat	k5eAaImAgNnS	spekulovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
novým	nový	k2eAgMnSc7d1	nový
basistou	basista	k1gMnSc7	basista
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
stát	stát	k5eAaPmF	stát
Robert	Robert	k1gMnSc1	Robert
Trujillo	Trujillo	k1gNnSc4	Trujillo
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
to	ten	k3xDgNnSc1	ten
však	však	k9	však
nebylo	být	k5eNaImAgNnS	být
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
<g/>
,	,	kIx,	,
24	[number]	k4	24
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Robert	Robert	k1gMnSc1	Robert
připojil	připojit	k5eAaPmAgMnS	připojit
k	k	k7c3	k
Metallice	Metallika	k1gFnSc3	Metallika
<g/>
.	.	kIx.	.
</s>
<s>
St.	st.	kA	st.
<g/>
Anger	Angero	k1gNnPc2	Angero
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
<s>
Skalní	skalní	k2eAgMnPc1d1	skalní
fanoušci	fanoušek	k1gMnPc1	fanoušek
byli	být	k5eAaImAgMnP	být
zděšeni	zděsit	k5eAaPmNgMnP	zděsit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
albu	album	k1gNnSc6	album
nejsou	být	k5eNaImIp3nP	být
slyšet	slyšet	k5eAaImF	slyšet
sólové	sólový	k2eAgFnPc1d1	sólová
kytary	kytara	k1gFnPc1	kytara
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
pro	pro	k7c4	pro
Hammetta	Hammett	k1gMnSc4	Hammett
tak	tak	k6eAd1	tak
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
<g/>
.	.	kIx.	.
</s>
<s>
Joe	Joe	k?	Joe
Berlinger	Berlinger	k1gMnSc1	Berlinger
i	i	k8xC	i
Bruce	Bruce	k1gMnSc1	Bruce
Sinofsky	Sinofsky	k1gMnSc1	Sinofsky
jsou	být	k5eAaImIp3nP	být
tvůrci	tvůrce	k1gMnPc7	tvůrce
dokumentu	dokument	k1gInSc2	dokument
o	o	k7c6	o
Metallice	Metallika	k1gFnSc6	Metallika
-	-	kIx~	-
Some	Some	k1gFnSc1	Some
Kind	Kind	k1gInSc1	Kind
Of	Of	k1gFnSc4	Of
Monster	monstrum	k1gNnPc2	monstrum
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
a	a	k8xC	a
půl	půl	k1xP	půl
hodiny	hodina	k1gFnSc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
výřez	výřez	k1gInSc1	výřez
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
dnů	den	k1gInPc2	den
každodenního	každodenní	k2eAgInSc2d1	každodenní
života	život	k1gInSc2	život
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
2001	[number]	k4	2001
a	a	k8xC	a
2003	[number]	k4	2003
bylo	být	k5eAaImAgNnS	být
nahráno	nahrát	k5eAaBmNgNnS	nahrát
tisíc	tisíc	k4xCgInSc4	tisíc
hodin	hodina	k1gFnPc2	hodina
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Rozpočet	rozpočet	k1gInSc1	rozpočet
filmu	film	k1gInSc2	film
je	být	k5eAaImIp3nS	být
okolo	okolo	k7c2	okolo
1,5	[number]	k4	1,5
miliónu	milión	k4xCgInSc2	milión
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Premiéra	premiéra	k1gFnSc1	premiéra
filmu	film	k1gInSc2	film
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2004	[number]	k4	2004
na	na	k7c6	na
filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
v	v	k7c4	v
Park	park	k1gInSc4	park
City	City	k1gFnSc2	City
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2004	[number]	k4	2004
skončila	skončit	k5eAaPmAgFnS	skončit
jeden	jeden	k4xCgInSc4	jeden
a	a	k8xC	a
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
trvající	trvající	k2eAgFnSc1d1	trvající
koncertní	koncertní	k2eAgFnSc1d1	koncertní
šňůra	šňůra	k1gFnSc1	šňůra
180	[number]	k4	180
vystoupení	vystoupení	k1gNnSc2	vystoupení
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
Rolling	Rolling	k1gInSc1	Rolling
Stone	ston	k1gInSc5	ston
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
Jamese	Jamesa	k1gFnSc3	Jamesa
Hetfielda	Hetfielda	k1gMnSc1	Hetfielda
mužem	muž	k1gMnSc7	muž
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gMnSc1	James
Hetfield	Hetfield	k1gMnSc1	Hetfield
dostal	dostat	k5eAaPmAgMnS	dostat
12	[number]	k4	12
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2006	[number]	k4	2006
speciální	speciální	k2eAgFnSc4d1	speciální
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
Cenu	cena	k1gFnSc4	cena
Stévie	Stévie	k1gFnSc2	Stévie
Ray	Ray	k1gMnSc2	Ray
Vaughana	Vaughan	k1gMnSc2	Vaughan
a	a	k8xC	a
James	James	k1gMnSc1	James
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
díky	díky	k7c3	díky
podpoře	podpora	k1gFnSc3	podpora
fondu	fond	k1gInSc2	fond
MusiCares	MusiCaresa	k1gFnPc2	MusiCaresa
MAP	mapa	k1gFnPc2	mapa
a	a	k8xC	a
díky	dík	k1gInPc4	dík
vytrvalé	vytrvalý	k2eAgNnSc1d1	vytrvalé
pomoci	pomoct	k5eAaPmF	pomoct
závislým	závislý	k2eAgMnPc3d1	závislý
hudebníkům	hudebník	k1gMnPc3	hudebník
s	s	k7c7	s
odvykací	odvykací	k2eAgFnSc7d1	odvykací
léčbou	léčba	k1gFnSc7	léčba
<g/>
.	.	kIx.	.
</s>
<s>
Metallica	Metallica	k6eAd1	Metallica
se	se	k3xPyFc4	se
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
vydala	vydat	k5eAaPmAgFnS	vydat
na	na	k7c6	na
miniturné	miniturný	k2eAgFnSc6d1	miniturný
Escape	Escap	k1gInSc5	Escap
from	froma	k1gFnPc2	froma
the	the	k?	the
studio	studio	k1gNnSc4	studio
'	'	kIx"	'
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
začalo	začít	k5eAaPmAgNnS	začít
na	na	k7c6	na
obrovském	obrovský	k2eAgInSc6d1	obrovský
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
Rock	rock	k1gInSc1	rock
am	am	k?	am
Ring	ring	k1gInSc1	ring
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
koncert	koncert	k1gInSc1	koncert
byl	být	k5eAaImAgInS	být
vysílán	vysílat	k5eAaImNgInS	vysílat
v	v	k7c6	v
přímém	přímý	k2eAgInSc6d1	přímý
přenosu	přenos	k1gInSc6	přenos
na	na	k7c6	na
MTV	MTV	kA	MTV
<g/>
.	.	kIx.	.
</s>
<s>
Fanoušci	Fanoušek	k1gMnPc1	Fanoušek
Metallicy	Metallica	k1gFnSc2	Metallica
byli	být	k5eAaImAgMnP	být
tímto	tento	k3xDgInSc7	tento
turné	turné	k1gNnSc4	turné
naprosto	naprosto	k6eAd1	naprosto
nadšeni	nadchnout	k5eAaPmNgMnP	nadchnout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
skupina	skupina	k1gFnSc1	skupina
zahrála	zahrát	k5eAaPmAgFnS	zahrát
na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
celé	celá	k1gFnSc2	celá
legendární	legendární	k2eAgNnSc4d1	legendární
album	album	k1gNnSc4	album
Master	master	k1gMnSc1	master
of	of	k?	of
Puppets	Puppets	k1gInSc1	Puppets
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
<g/>
,	,	kIx,	,
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
turné	turné	k1gNnSc2	turné
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Metallica	Metallica	k1gFnSc1	Metallica
vrátila	vrátit	k5eAaPmAgFnS	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
svého	své	k1gNnSc2	své
studia	studio	k1gNnSc2	studio
HQ	HQ	kA	HQ
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
práce	práce	k1gFnPc4	práce
na	na	k7c6	na
novém	nový	k2eAgNnSc6d1	nové
albu	album	k1gNnSc6	album
Death	Deatha	k1gFnPc2	Deatha
Magnetic	Magnetice	k1gFnPc2	Magnetice
<g/>
.	.	kIx.	.
</s>
<s>
Producentem	producent	k1gMnSc7	producent
alba	album	k1gNnSc2	album
je	být	k5eAaImIp3nS	být
Rick	Rick	k1gInSc1	Rick
Rubin	Rubin	k2eAgInSc1d1	Rubin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2008	[number]	k4	2008
Metallica	Metallica	k1gFnSc1	Metallica
spustila	spustit	k5eAaPmAgFnS	spustit
promo	promo	k6eAd1	promo
stránku	stránka	k1gFnSc4	stránka
MissionMetallica	MissionMetallic	k1gInSc2	MissionMetallic
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
postupně	postupně	k6eAd1	postupně
zveřejňovány	zveřejňován	k2eAgFnPc1d1	zveřejňována
videa	video	k1gNnSc2	video
a	a	k8xC	a
fotografie	fotografia	k1gFnSc2	fotografia
z	z	k7c2	z
natáčení	natáčení	k1gNnSc2	natáčení
<g/>
.	.	kIx.	.
</s>
<s>
Fanoušci	Fanoušek	k1gMnPc1	Fanoušek
si	se	k3xPyFc3	se
zde	zde	k6eAd1	zde
mohou	moct	k5eAaImIp3nP	moct
album	album	k1gNnSc4	album
také	také	k9	také
zakoupit	zakoupit	k5eAaPmF	zakoupit
<g/>
.	.	kIx.	.
</s>
<s>
Death	Death	k1gInSc1	Death
Magnetic	Magnetice	k1gFnPc2	Magnetice
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
12	[number]	k4	12
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
EP	EP	kA	EP
Beyond	Beyonda	k1gFnPc2	Beyonda
Magnetic	Magnetice	k1gFnPc2	Magnetice
<g/>
,	,	kIx,	,
s	s	k7c7	s
několika	několik	k4yIc7	několik
skladbami	skladba	k1gFnPc7	skladba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
nedostaly	dostat	k5eNaPmAgFnP	dostat
na	na	k7c4	na
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2011	[number]	k4	2011
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
zcela	zcela	k6eAd1	zcela
nové	nový	k2eAgNnSc1d1	nové
album	album	k1gNnSc1	album
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
nese	nést	k5eAaImIp3nS	nést
jméno	jméno	k1gNnSc4	jméno
Lulu	lula	k1gFnSc4	lula
<g/>
.	.	kIx.	.
</s>
<s>
Metallica	Metallica	k6eAd1	Metallica
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
se	se	k3xPyFc4	se
slavným	slavný	k2eAgMnSc7d1	slavný
hudebníkem	hudebník	k1gMnSc7	hudebník
Lou	Lou	k1gMnSc7	Lou
Reedem	Reed	k1gMnSc7	Reed
<g/>
,	,	kIx,	,
album	album	k1gNnSc1	album
má	mít	k5eAaImIp3nS	mít
celkem	celkem	k6eAd1	celkem
10	[number]	k4	10
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
dočkalo	dočkat	k5eAaPmAgNnS	dočkat
velmi	velmi	k6eAd1	velmi
negativních	negativní	k2eAgFnPc2d1	negativní
recenzí	recenze	k1gFnPc2	recenze
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
alba	album	k1gNnSc2	album
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
téměř	téměř	k6eAd1	téměř
90	[number]	k4	90
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
===	===	k?	===
<g/>
''	''	k?	''
<g/>
'	'	kIx"	'
<g/>
Metallica	Metallica	k1gFnSc1	Metallica
<g/>
:	:	kIx,	:
Through	Through	k1gInSc1	Through
the	the	k?	the
Never	Never	k1gInSc1	Never
a	a	k8xC	a
Hardwired	Hardwired	k1gInSc1	Hardwired
<g/>
...	...	k?	...
to	ten	k3xDgNnSc4	ten
Self-Destruct	Self-Destruct	k2eAgInSc1d1	Self-Destruct
<g/>
:	:	kIx,	:
Throught	Throught	k1gInSc1	Throught
the	the	k?	the
Never	Never	k1gInSc1	Never
je	být	k5eAaImIp3nS	být
film	film	k1gInSc4	film
o	o	k7c6	o
skupině	skupina	k1gFnSc6	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
zfilmovaný	zfilmovaný	k2eAgInSc1d1	zfilmovaný
koncert	koncert	k1gInSc1	koncert
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
naposledy	naposledy	k6eAd1	naposledy
kapela	kapela	k1gFnSc1	kapela
podívala	podívat	k5eAaPmAgFnS	podívat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
na	na	k7c4	na
festival	festival	k1gInSc4	festival
Aerodrome	aerodrom	k1gInSc5	aerodrom
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Fanoušci	Fanoušek	k1gMnPc1	Fanoušek
kapely	kapela	k1gFnSc2	kapela
si	se	k3xPyFc3	se
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
mohli	moct	k5eAaImAgMnP	moct
vybrat	vybrat	k5eAaPmF	vybrat
skladby	skladba	k1gFnPc4	skladba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
chtějí	chtít	k5eAaImIp3nP	chtít
zahrát	zahrát	k5eAaPmF	zahrát
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
velmi	velmi	k6eAd1	velmi
krátce	krátce	k6eAd1	krátce
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
potom	potom	k6eAd1	potom
kapela	kapela	k1gFnSc1	kapela
odjela	odjet	k5eAaPmAgFnS	odjet
na	na	k7c4	na
další	další	k2eAgNnSc4d1	další
turné	turné	k1gNnSc4	turné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
tohoto	tento	k3xDgInSc2	tento
roku	rok	k1gInSc2	rok
uveřejnila	uveřejnit	k5eAaPmAgFnS	uveřejnit
datum	datum	k1gNnSc4	datum
vydání	vydání	k1gNnSc2	vydání
nového	nový	k2eAgNnSc2d1	nové
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc1	název
Hardwired	Hardwired	k1gInSc4	Hardwired
<g/>
...	...	k?	...
to	ten	k3xDgNnSc1	ten
Self-Destruct	Self-Destruct	k2eAgMnSc1d1	Self-Destruct
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
vyjde	vyjít	k5eAaPmIp3nS	vyjít
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
mezitím	mezitím	k6eAd1	mezitím
vydala	vydat	k5eAaPmAgFnS	vydat
už	už	k9	už
dvě	dva	k4xCgFnPc1	dva
skladby	skladba	k1gFnPc1	skladba
z	z	k7c2	z
nového	nový	k2eAgNnSc2d1	nové
alba	album	k1gNnSc2	album
fanoušci	fanoušek	k1gMnPc1	fanoušek
první	první	k4xOgFnSc4	první
skladbu	skladba	k1gFnSc4	skladba
z	z	k7c2	z
názvem	název	k1gInSc7	název
Hardwired	Hardwired	k1gInSc1	Hardwired
hodnotí	hodnotit	k5eAaImIp3nS	hodnotit
velmi	velmi	k6eAd1	velmi
pozitivně	pozitivně	k6eAd1	pozitivně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
druhou	druhý	k4xOgFnSc4	druhý
kladbu	kladba	k1gFnSc4	kladba
z	z	k7c2	z
názvem	název	k1gInSc7	název
Month	Month	k1gMnSc1	Month
into	into	k1gMnSc1	into
Flame	Flam	k1gInSc5	Flam
už	už	k6eAd1	už
zase	zase	k9	zase
tak	tak	k9	tak
ne	ne	k9	ne
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
přesto	přesto	k8xC	přesto
má	mít	k5eAaImIp3nS	mít
skladba	skladba	k1gFnSc1	skladba
velký	velký	k2eAgInSc4d1	velký
potenciál	potenciál	k1gInSc4	potenciál
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Diskografie	diskografie	k1gFnSc2	diskografie
Metallicy	Metallica	k1gFnSc2	Metallica
<g/>
.	.	kIx.	.
</s>
<s>
Spawn	Spawn	k1gInSc1	Spawn
<g/>
:	:	kIx,	:
For	forum	k1gNnPc2	forum
Whom	Whoma	k1gFnPc2	Whoma
the	the	k?	the
Bell	bell	k1gInSc1	bell
Tolls	Tollsa	k1gFnPc2	Tollsa
Mission	Mission	k1gInSc1	Mission
Impossible	Impossible	k1gMnSc1	Impossible
II	II	kA	II
<g/>
:	:	kIx,	:
I	I	kA	I
Disappear	Disappear	k1gInSc1	Disappear
Zombieland	Zombieland	k1gInSc1	Zombieland
<g/>
:	:	kIx,	:
For	forum	k1gNnPc2	forum
Whom	Whoma	k1gFnPc2	Whoma
the	the	k?	the
Bell	bell	k1gInSc1	bell
Tolls	Tolls	k1gInSc1	Tolls
1982	[number]	k4	1982
-	-	kIx~	-
No	no	k9	no
Life	Life	k1gNnSc6	Life
́	́	k?	́
<g/>
til	til	k1gInSc1	til
Leather	Leathra	k1gFnPc2	Leathra
Metallica	Metallic	k1gInSc2	Metallic
získala	získat	k5eAaPmAgFnS	získat
8	[number]	k4	8
ocenění	ocenění	k1gNnSc4	ocenění
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
:	:	kIx,	:
1990	[number]	k4	1990
-	-	kIx~	-
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
metalová	metalový	k2eAgFnSc1d1	metalová
skladba	skladba	k1gFnSc1	skladba
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
One	One	k1gFnSc1	One
<g/>
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
And	Anda	k1gFnPc2	Anda
Justice	justice	k1gFnSc1	justice
for	forum	k1gNnPc2	forum
All	All	k1gFnSc2	All
<g/>
)	)	kIx)	)
1991	[number]	k4	1991
-	-	kIx~	-
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
metalová	metalový	k2eAgFnSc1d1	metalová
skladba	skladba	k1gFnSc1	skladba
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Stone	ston	k1gInSc5	ston
Cold	Cold	k1gInSc4	Cold
Crazy	Craza	k1gFnPc4	Craza
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Garage	Garage	k1gInSc1	Garage
Days	Days	k1gInSc1	Days
Re-Revisited	Re-Revisited	k1gInSc1	Re-Revisited
<g/>
)	)	kIx)	)
1992	[number]	k4	1992
-	-	kIx~	-
Nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
metalové	metalový	k2eAgNnSc1d1	metalové
album	album	k1gNnSc1	album
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Metallica	Metallica	k1gFnSc1	Metallica
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Black	Black	k1gInSc1	Black
Album	album	k1gNnSc1	album
<g/>
)	)	kIx)	)
1999	[number]	k4	1999
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
metalová	metalový	k2eAgFnSc1d1	metalová
skladba	skladba	k1gFnSc1	skladba
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Better	Better	k1gMnSc1	Better
Than	Than	k1gMnSc1	Than
You	You	k1gMnSc1	You
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ReLoad	ReLoad	k1gInSc1	ReLoad
<g/>
)	)	kIx)	)
2000	[number]	k4	2000
-	-	kIx~	-
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
hard	hard	k1gInSc4	hard
rocková	rockový	k2eAgFnSc1d1	rocková
skladba	skladba	k1gFnSc1	skladba
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Whiskey	Whiskea	k1gFnPc1	Whiskea
in	in	k?	in
the	the	k?	the
Jar	jar	k1gInSc1	jar
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Garage	Garage	k1gFnSc1	Garage
Inc	Inc	k1gFnSc1	Inc
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
2001	[number]	k4	2001
-	-	kIx~	-
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
rocková	rockový	k2eAgFnSc1d1	rocková
instrumentální	instrumentální	k2eAgFnSc1d1	instrumentální
skladba	skladba	k1gFnSc1	skladba
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
Call	Call	k1gInSc4	Call
Of	Of	k1gFnSc2	Of
Ktulu	Ktul	k1gInSc2	Ktul
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
S	s	k7c7	s
<g/>
&	&	k?	&
<g/>
M	M	kA	M
<g/>
)	)	kIx)	)
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Michaelem	Michael	k1gMnSc7	Michael
Kamenem	kámen	k1gInSc7	kámen
a	a	k8xC	a
Symfonií	symfonie	k1gFnSc7	symfonie
San	San	k1gFnSc2	San
Francisca	Francisc	k1gInSc2	Francisc
2004	[number]	k4	2004
-	-	kIx~	-
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
metalová	metalový	k2eAgFnSc1d1	metalová
skladba	skladba	k1gFnSc1	skladba
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
St.	st.	kA	st.
Anger	Anger	k1gMnSc1	Anger
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
St.	st.	kA	st.
Anger	Anger	k1gMnSc1	Anger
<g/>
)	)	kIx)	)
2008	[number]	k4	2008
-	-	kIx~	-
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
metalová	metalový	k2eAgFnSc1d1	metalová
skladba	skladba	k1gFnSc1	skladba
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
My	my	k3xPp1nPc1	my
Apocalypse	Apocalyps	k1gMnPc4	Apocalyps
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Death	Death	k1gInSc1	Death
Magnetic	Magnetice	k1gFnPc2	Magnetice
<g/>
)	)	kIx)	)
MTV	MTV	kA	MTV
Video	video	k1gNnSc1	video
Music	Musice	k1gInPc2	Musice
Awards	Awardsa	k1gFnPc2	Awardsa
<g/>
:	:	kIx,	:
1992	[number]	k4	1992
-	-	kIx~	-
Nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
metalové	metalový	k2eAgNnSc1d1	metalové
video	video	k1gNnSc1	video
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Enter	Enter	k1gMnSc1	Enter
Sandman	Sandman	k1gMnSc1	Sandman
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Black	Black	k1gInSc1	Black
Album	album	k1gNnSc1	album
<g/>
)	)	kIx)	)
1996	[number]	k4	1996
-	-	kIx~	-
Nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
metalové	metalový	k2eAgNnSc1d1	metalové
video	video	k1gNnSc1	video
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Until	Until	k1gInSc1	Until
it	it	k?	it
Sleeps	Sleeps	k1gInSc1	Sleeps
<g/>
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Load	Load	k1gMnSc1	Load
<g/>
)	)	kIx)	)
American	American	k1gMnSc1	American
Music	Music	k1gMnSc1	Music
Award	Award	k1gMnSc1	Award
<g/>
:	:	kIx,	:
1996	[number]	k4	1996
-	-	kIx~	-
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
metalový	metalový	k2eAgMnSc1d1	metalový
<g/>
/	/	kIx~	/
<g/>
hard	hard	k6eAd1	hard
rockový	rockový	k2eAgMnSc1d1	rockový
umělec	umělec	k1gMnSc1	umělec
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Metallica	Metallica	k1gMnSc1	Metallica
<g/>
"	"	kIx"	"
1996	[number]	k4	1996
-	-	kIx~	-
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
metalová	metalový	k2eAgFnSc1d1	metalová
skladba	skladba	k1gFnSc1	skladba
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Until	Until	k1gInSc1	Until
It	It	k1gFnSc2	It
Sleeps	Sleepsa	k1gFnPc2	Sleepsa
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Load	Load	k1gInSc1	Load
<g/>
)	)	kIx)	)
Billboard	billboard	k1gInSc1	billboard
Music	Music	k1gMnSc1	Music
Award	Award	k1gMnSc1	Award
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
1997	[number]	k4	1997
-	-	kIx~	-
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
Rock	rock	k1gInSc4	rock
and	and	k?	and
rollový	rollový	k2eAgMnSc1d1	rollový
umělec	umělec	k1gMnSc1	umělec
<g/>
:	:	kIx,	:
Metallica	Metallica	k1gFnSc1	Metallica
1999	[number]	k4	1999
-	-	kIx~	-
Catalog	Catalog	k1gMnSc1	Catalog
Artist	Artist	k1gMnSc1	Artist
of	of	k?	of
the	the	k?	the
Year	Year	k1gInSc1	Year
<g/>
:	:	kIx,	:
Metallica	Metallica	k1gFnSc1	Metallica
1999	[number]	k4	1999
-	-	kIx~	-
Catalog	Catalog	k1gMnSc1	Catalog
Album	album	k1gNnSc1	album
of	of	k?	of
the	the	k?	the
Year	Year	k1gInSc1	Year
<g/>
:	:	kIx,	:
Metallica	Metallica	k1gFnSc1	Metallica
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Black	Black	k1gInSc1	Black
Album	album	k1gNnSc1	album
<g/>
)	)	kIx)	)
Governor	Governor	k1gInSc1	Governor
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Award	Awarda	k1gFnPc2	Awarda
<g/>
:	:	kIx,	:
2004	[number]	k4	2004
-	-	kIx~	-
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
skupina	skupina	k1gFnSc1	skupina
<g/>
:	:	kIx,	:
Metallica	Metallica	k1gMnSc1	Metallica
Kerrang	Kerrang	k1gMnSc1	Kerrang
<g/>
!	!	kIx.	!
</s>
<s>
2003	[number]	k4	2003
-	-	kIx~	-
Síň	síň	k1gFnSc1	síň
slávy	sláva	k1gFnSc2	sláva
(	(	kIx(	(
<g/>
Hall	Hall	k1gInSc1	Hall
of	of	k?	of
Fame	Fame	k1gInSc1	Fame
<g/>
)	)	kIx)	)
2003	[number]	k4	2003
-	-	kIx~	-
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
skupina	skupina	k1gFnSc1	skupina
-	-	kIx~	-
Metallica	Metallica	k1gFnSc1	Metallica
2004	[number]	k4	2004
-	-	kIx~	-
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
skupina	skupina	k1gFnSc1	skupina
na	na	k7c6	na
světě	svět	k1gInSc6	svět
-	-	kIx~	-
Metallica	Metallica	k1gFnSc1	Metallica
2008	[number]	k4	2008
-	-	kIx~	-
Inspiration	Inspiration	k1gInSc1	Inspiration
Award	Award	k1gMnSc1	Award
Winner	Winner	k1gMnSc1	Winner
-	-	kIx~	-
Metallica	Metallica	k1gFnSc1	Metallica
2009	[number]	k4	2009
-	-	kIx~	-
Nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
album	album	k1gNnSc1	album
-	-	kIx~	-
Death	Death	k1gInSc1	Death
Magnetic	Magnetice	k1gFnPc2	Magnetice
Bammies	Bammiesa	k1gFnPc2	Bammiesa
<g/>
:	:	kIx,	:
1997	[number]	k4	1997
<g/>
:	:	kIx,	:
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
bubeník	bubeník	k1gMnSc1	bubeník
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
Lars	Lars	k6eAd1	Lars
Ulrich	Ulrich	k1gMnSc1	Ulrich
1997	[number]	k4	1997
<g/>
:	:	kIx,	:
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
Hard	Hard	k1gMnSc1	Hard
Rockové	rockový	k2eAgNnSc4d1	rockové
album	album	k1gNnSc4	album
<g/>
:	:	kIx,	:
ReLoad	ReLoad	k1gInSc4	ReLoad
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Metallica	Metallic	k1gInSc2	Metallic
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Metallica	Metallic	k1gInSc2	Metallic
<g/>
.4	.4	k4	.4
<g/>
fan	fana	k1gFnPc2	fana
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
Český	český	k2eAgInSc1d1	český
fanklub	fanklub	k1gInSc1	fanklub
kapely	kapela	k1gFnSc2	kapela
Metallica	Metallic	k1gInSc2	Metallic
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Metallica	Metallica	k1gFnSc1	Metallica
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
-	-	kIx~	-
oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
HMB	HMB	kA	HMB
-	-	kIx~	-
Metallica	Metallica	k1gMnSc1	Metallica
v	v	k7c6	v
databázi	databáze	k1gFnSc6	databáze
Hard	Hard	k1gMnSc1	Hard
Music	Music	k1gMnSc1	Music
Base	basa	k1gFnSc3	basa
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
