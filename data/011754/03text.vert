<p>
<s>
Metric	Metrice	k1gFnPc2	Metrice
je	být	k5eAaImIp3nS	být
kanadská	kanadský	k2eAgFnSc1d1	kanadská
rocková	rockový	k2eAgFnSc1d1	rocková
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
v	v	k7c6	v
kanadském	kanadský	k2eAgNnSc6d1	kanadské
Torontu	Toronto	k1gNnSc6	Toronto
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
jsou	být	k5eAaImIp3nP	být
Emily	Emil	k1gMnPc4	Emil
Haines	Hainesa	k1gFnPc2	Hainesa
(	(	kIx(	(
<g/>
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
syntezátory	syntezátor	k1gInPc1	syntezátor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
Shaw	Shaw	k1gMnSc1	Shaw
(	(	kIx(	(
<g/>
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
theremin	theremin	k1gInSc1	theremin
<g/>
,	,	kIx,	,
syntezátory	syntezátor	k1gInPc1	syntezátor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Joshua	Joshua	k1gMnSc1	Joshua
Winstead	Winstead	k1gInSc1	Winstead
(	(	kIx(	(
<g/>
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
)	)	kIx)	)
a	a	k8xC	a
Joules	Joules	k1gMnSc1	Joules
Scott-Key	Scott-Kea	k1gFnSc2	Scott-Kea
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgFnSc1d1	bicí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
Old	Olda	k1gFnPc2	Olda
World	Worlda	k1gFnPc2	Worlda
Underground	underground	k1gInSc1	underground
<g/>
,	,	kIx,	,
Where	Wher	k1gInSc5	Wher
Are	ar	k1gInSc5	ar
You	You	k1gFnPc2	You
Now	Now	k1gMnPc4	Now
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Live	Live	k1gFnSc1	Live
It	It	k1gFnSc1	It
Out	Out	k1gFnSc1	Out
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Grow	Grow	k?	Grow
Up	Up	k1gFnSc1	Up
and	and	k?	and
Blow	Blow	k1gMnSc2	Blow
Away	Awaa	k1gMnSc2	Awaa
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Fantasies	Fantasies	k1gInSc1	Fantasies
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Synthetica	Synthetica	k1gFnSc1	Synthetica
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pagans	Pagans	k1gInSc1	Pagans
in	in	k?	in
Vegas	Vegas	k1gInSc1	Vegas
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Art	Art	k?	Art
of	of	k?	of
Doubt	Doubt	k1gInSc1	Doubt
(	(	kIx(	(
<g/>
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Metric	Metrice	k1gFnPc2	Metrice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
web	web	k1gInSc1	web
</s>
</p>
<p>
<s>
Metric	Metric	k1gMnSc1	Metric
na	na	k7c4	na
SoundCloud	SoundCloud	k1gInSc4	SoundCloud
</s>
</p>
