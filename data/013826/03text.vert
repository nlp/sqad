<s>
Prachatice	Prachatice	k1gFnPc1
I	i	k9
</s>
<s>
Prachatice	Prachatice	k1gFnPc1
I	i	k8xC
Pohled	pohled	k1gInSc1
na	na	k7c4
historické	historický	k2eAgNnSc4d1
centrum	centrum	k1gNnSc4
Prachatic	Prachatice	k1gFnPc2
ze	z	k7c2
Žižkovy	Žižkov	k1gInPc1
skalkyLokalita	skalkyLokalita	k1gFnSc1
Charakter	charakter	k1gInSc1
</s>
<s>
část	část	k1gFnSc1
města	město	k1gNnSc2
Obec	obec	k1gFnSc1
</s>
<s>
Prachatice	Prachatice	k1gFnPc1
Okres	okres	k1gInSc1
</s>
<s>
Prachatice	Prachatice	k1gFnPc1
Kraj	kraj	k7c2
</s>
<s>
Jihočeský	jihočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
Historická	historický	k2eAgFnSc1d1
země	země	k1gFnSc1
</s>
<s>
Čechy	Čechy	k1gFnPc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
0	#num#	k4
<g/>
′	′	k?
<g/>
47	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
13	#num#	k4
<g/>
°	°	k?
<g/>
59	#num#	k4
<g/>
′	′	k?
<g/>
55	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Základní	základní	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
682	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Katastrální	katastrální	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Prachatice	Prachatice	k1gFnPc1
(	(	kIx(
<g/>
20,33	20,33	k4
km²	km²	k?
<g/>
)	)	kIx)
PSČ	PSČ	kA
</s>
<s>
383	#num#	k4
01	#num#	k4
Počet	počet	k1gInSc1
domů	domů	k6eAd1
</s>
<s>
149	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Prachatice	Prachatice	k1gFnPc1
I	i	k9
</s>
<s>
Další	další	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Kód	kód	k1gInSc4
části	část	k1gFnPc1
obce	obec	k1gFnSc2
</s>
<s>
404837	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Zdroje	zdroj	k1gInSc2
k	k	k7c3
infoboxu	infobox	k1gInSc3
a	a	k8xC
českým	český	k2eAgNnPc3d1
sídlům	sídlo	k1gNnPc3
<g/>
.	.	kIx.
<g/>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Prachatice	Prachatice	k1gFnPc1
I	i	k9
jsou	být	k5eAaImIp3nP
část	část	k1gFnSc4
okresního	okresní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prachatice	Prachatice	k1gFnPc1
<g/>
,	,	kIx,
představující	představující	k2eAgFnSc1d1
jeho	jeho	k3xOp3gNnSc4
původní	původní	k2eAgNnSc4d1
středověké	středověký	k2eAgNnSc4d1
jádro	jádro	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
evidováno	evidovat	k5eAaImNgNnS
182	#num#	k4
adres	adresa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
zde	zde	k6eAd1
trvale	trvale	k6eAd1
žilo	žít	k5eAaImAgNnP
682	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Prachatice	Prachatice	k1gFnPc1
I	i	k9
leží	ležet	k5eAaImIp3nP
v	v	k7c6
katastrálním	katastrální	k2eAgNnSc6d1
území	území	k1gNnSc6
Prachatice	Prachatice	k1gFnPc4
o	o	k7c6
výměře	výměra	k1gFnSc6
20,33	20,33	k4
km	km	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Historický	historický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
–	–	k?
1869	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
<g/>
.	.	kIx.
21	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Ministerstvo	ministerstvo	k1gNnSc1
vnitra	vnitro	k1gNnSc2
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Adresy	adresa	k1gFnSc2
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2009-10-10	2009-10-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Statistický	statistický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
900	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
250	#num#	k4
<g/>
-	-	kIx~
<g/>
2394	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
199	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Územně	územně	k6eAd1
identifikační	identifikační	k2eAgInSc4d1
registr	registr	k1gInSc4
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Územně	územně	k6eAd1
identifikační	identifikační	k2eAgInSc1d1
registr	registr	k1gInSc1
ČR	ČR	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1999-01-01	1999-01-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Katastrální	katastrální	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
katastru	katastr	k1gInSc6
Prachatice	Prachatice	k1gFnPc1
na	na	k7c6
webu	web	k1gInSc6
ČÚZK	ČÚZK	kA
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Město	město	k1gNnSc1
Prachatice	Prachatice	k1gFnPc1
</s>
<s>
k.	k.	k?
ú.	ú.	k?
Prachatice	Prachatice	k1gFnPc1
</s>
<s>
(	(	kIx(
<g/>
Prachatice	Prachatice	k1gFnPc1
I	i	k8xC
•	•	k?
</s>
<s>
Prachatice	Prachatice	k1gFnPc1
II	II	kA
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
k.	k.	k?
ú.	ú.	k?
Staré	Staré	k2eAgFnPc1d1
Prachatice	Prachatice	k1gFnPc1
</s>
<s>
(	(	kIx(
<g/>
Staré	Staré	k2eAgFnPc1d1
Prachatice	Prachatice	k1gFnPc1
•	•	k?
</s>
<s>
Ostrov	ostrov	k1gInSc1
•	•	k?
</s>
<s>
Městská	městský	k2eAgFnSc1d1
Lhotka	Lhotka	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Libínské	Libínský	k2eAgNnSc1d1
Sedlo	sedlo	k1gNnSc1
•	•	k?
</s>
<s>
Perlovice	Perlovice	k1gFnSc1
•	•	k?
</s>
<s>
Volovice	volovice	k1gFnSc1
•	•	k?
</s>
<s>
Stádla	Stádnout	k5eAaPmAgFnS
•	•	k?
</s>
<s>
k.	k.	k?
ú.	ú.	k?
Kahov	Kahov	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
Kahov	Kahov	k1gInSc1
•	•	k?
</s>
<s>
Podolí	Podolí	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Oseky	Osek	k1gInPc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Geografie	geografie	k1gFnSc1
</s>
