<p>
<s>
Formální	formální	k2eAgFnSc1d1	formální
gramatika	gramatika	k1gFnSc1	gramatika
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
označuje	označovat	k5eAaImIp3nS	označovat
strukturu	struktura	k1gFnSc4	struktura
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
popisuje	popisovat	k5eAaImIp3nS	popisovat
formální	formální	k2eAgInSc4d1	formální
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenování	pojmenování	k1gNnSc1	pojmenování
je	být	k5eAaImIp3nS	být
zvoleno	zvolit	k5eAaPmNgNnS	zvolit
kvůli	kvůli	k7c3	kvůli
podobnosti	podobnost	k1gFnSc3	podobnost
s	s	k7c7	s
gramatikami	gramatika	k1gFnPc7	gramatika
používanými	používaný	k2eAgFnPc7d1	používaná
v	v	k7c6	v
přirozených	přirozený	k2eAgInPc6d1	přirozený
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gramatika	gramatika	k1gFnSc1	gramatika
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
množiny	množina	k1gFnSc2	množina
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
kterých	který	k3yRgInPc2	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
každé	každý	k3xTgNnSc1	každý
slovo	slovo	k1gNnSc1	slovo
předepsaným	předepsaný	k2eAgInSc7d1	předepsaný
způsobem	způsob	k1gInSc7	způsob
vygenerováno	vygenerovat	k5eAaPmNgNnS	vygenerovat
z	z	k7c2	z
předem	předem	k6eAd1	předem
daného	daný	k2eAgInSc2d1	daný
počátečního	počáteční	k2eAgInSc2d1	počáteční
symbolu	symbol	k1gInSc2	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Generování	generování	k1gNnSc1	generování
probíhá	probíhat	k5eAaImIp3nS	probíhat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
vezmeme	vzít	k5eAaPmIp1nP	vzít
počáteční	počáteční	k2eAgInSc4d1	počáteční
symbol	symbol	k1gInSc4	symbol
<g/>
,	,	kIx,	,
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
aplikujeme	aplikovat	k5eAaBmIp1nP	aplikovat
kterékoli	kterýkoli	k3yIgNnSc4	kterýkoli
z	z	k7c2	z
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
,	,	kIx,	,
na	na	k7c4	na
získaný	získaný	k2eAgInSc4d1	získaný
řetězec	řetězec	k1gInSc4	řetězec
opět	opět	k6eAd1	opět
aplikujeme	aplikovat	k5eAaBmIp1nP	aplikovat
kterékoli	kterýkoli	k3yIgNnSc4	kterýkoli
z	z	k7c2	z
pravidel	pravidlo	k1gNnPc2	pravidlo
atd.	atd.	kA	atd.
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nevygenerujeme	vygenerovat	k5eNaPmIp1nP	vygenerovat
požadované	požadovaný	k2eAgNnSc4d1	požadované
slovo	slovo	k1gNnSc4	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
slovo	slovo	k1gNnSc4	slovo
nejvýše	vysoce	k6eAd3	vysoce
jeden	jeden	k4xCgInSc4	jeden
postup	postup	k1gInSc4	postup
generování	generování	k1gNnSc4	generování
<g/>
,	,	kIx,	,
gramatika	gramatika	k1gFnSc1	gramatika
je	být	k5eAaImIp3nS	být
jednoznačná	jednoznačný	k2eAgFnSc1d1	jednoznačná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mějme	mít	k5eAaImRp1nP	mít
například	například	k6eAd1	například
abecedu	abeceda	k1gFnSc4	abeceda
obsahující	obsahující	k2eAgInPc4d1	obsahující
symboly	symbol	k1gInPc4	symbol
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
'	'	kIx"	'
a	a	k8xC	a
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
b	b	k?	b
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
'	'	kIx"	'
<g/>
,	,	kIx,	,
počáteční	počáteční	k2eAgInSc1d1	počáteční
symbol	symbol	k1gInSc1	symbol
je	být	k5eAaImIp3nS	být
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
'	'	kIx"	'
a	a	k8xC	a
pravidla	pravidlo	k1gNnPc1	pravidlo
jsou	být	k5eAaImIp3nP	být
definována	definovat	k5eAaBmNgNnP	definovat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
⟶	⟶	k?	⟶
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
\	\	kIx~	\
<g/>
longrightarrow	longrightarrow	k?	longrightarrow
aSb	aSb	k?	aSb
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
⟶	⟶	k?	⟶
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
\	\	kIx~	\
<g/>
longrightarrow	longrightarrow	k?	longrightarrow
ba	ba	k9	ba
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
začneme	začít	k5eAaPmIp1nP	začít
symbolem	symbol	k1gInSc7	symbol
"	"	kIx"	"
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
"	"	kIx"	"
a	a	k8xC	a
vybereme	vybrat	k5eAaPmIp1nP	vybrat
pravidlo	pravidlo	k1gNnSc1	pravidlo
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
budeme	být	k5eAaImBp1nP	být
aplikovat	aplikovat	k5eAaBmF	aplikovat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
vybereme	vybrat	k5eAaPmIp1nP	vybrat
1	[number]	k4	1
<g/>
,	,	kIx,	,
nahradíme	nahradit	k5eAaPmIp1nP	nahradit
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
'	'	kIx"	'
řetězcem	řetězec	k1gInSc7	řetězec
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
aSb	aSb	k?	aSb
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
'	'	kIx"	'
a	a	k8xC	a
obdržíme	obdržet	k5eAaPmIp1nP	obdržet
tak	tak	k6eAd1	tak
"	"	kIx"	"
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
aSb	aSb	k?	aSb
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Znovuzvolením	znovuzvolení	k1gNnSc7	znovuzvolení
1	[number]	k4	1
<g/>
.	.	kIx.	.
pravidla	pravidlo	k1gNnSc2	pravidlo
nahradíme	nahradit	k5eAaPmIp1nP	nahradit
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
'	'	kIx"	'
opět	opět	k6eAd1	opět
řetězcem	řetězec	k1gInSc7	řetězec
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
aSb	aSb	k?	aSb
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
'	'	kIx"	'
a	a	k8xC	a
obdržíme	obdržet	k5eAaPmIp1nP	obdržet
"	"	kIx"	"
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
aaSbb	aaSbba	k1gFnPc2	aaSbba
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
proces	proces	k1gInSc4	proces
můžeme	moct	k5eAaImIp1nP	moct
opakovat	opakovat	k5eAaImF	opakovat
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nejsou	být	k5eNaImIp3nP	být
všechny	všechen	k3xTgInPc4	všechen
symboly	symbol	k1gInPc4	symbol
našeho	náš	k3xOp1gNnSc2	náš
slova	slovo	k1gNnSc2	slovo
z	z	k7c2	z
abecedy	abeceda	k1gFnSc2	abeceda
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
'	'	kIx"	'
a	a	k8xC	a
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
b	b	k?	b
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
'	'	kIx"	'
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Abychom	aby	kYmCp1nP	aby
tedy	tedy	k9	tedy
vygenerovali	vygenerovat	k5eAaPmAgMnP	vygenerovat
slovo	slovo	k1gNnSc4	slovo
<g/>
,	,	kIx,	,
musíme	muset	k5eAaImIp1nP	muset
zvolit	zvolit	k5eAaPmF	zvolit
2	[number]	k4	2
<g/>
.	.	kIx.	.
pravidlo	pravidlo	k1gNnSc1	pravidlo
a	a	k8xC	a
přepsat	přepsat	k5eAaPmF	přepsat
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
'	'	kIx"	'
na	na	k7c4	na
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
ba	ba	k9	ba
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
obdržíme	obdržet	k5eAaPmIp1nP	obdržet
"	"	kIx"	"
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
aababb	aababb	k1gMnSc1	aababb
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
"	"	kIx"	"
a	a	k8xC	a
jsme	být	k5eAaImIp1nP	být
hotovi	hotov	k2eAgMnPc1d1	hotov
<g/>
.	.	kIx.	.
</s>
<s>
Jazykem	jazyk	k1gInSc7	jazyk
gramatiky	gramatika	k1gFnSc2	gramatika
jsou	být	k5eAaImIp3nP	být
všechna	všechen	k3xTgNnPc4	všechen
slova	slovo	k1gNnPc4	slovo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
dokážeme	dokázat	k5eAaPmIp1nP	dokázat
vygenerovat	vygenerovat	k5eAaPmF	vygenerovat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
</s>
</p>
<p>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
<s>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
ba	ba	k9	ba
<g/>
,	,	kIx,	,
<g/>
abab	abab	k1gMnSc1	abab
<g/>
,	,	kIx,	,
<g/>
aababb	aababb	k1gMnSc1	aababb
<g/>
,	,	kIx,	,
<g/>
aaababbb	aaababbb	k1gMnSc1	aaababbb
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
\	\	kIx~	\
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
</p>
<p>
<s>
Znaky	znak	k1gInPc1	znak
z	z	k7c2	z
abecedy	abeceda	k1gFnSc2	abeceda
(	(	kIx(	(
<g/>
v	v	k7c6	v
našem	náš	k3xOp1gInSc6	náš
případě	případ	k1gInSc6	případ
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
'	'	kIx"	'
a	a	k8xC	a
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
b	b	k?	b
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
'	'	kIx"	'
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
terminály	terminál	k1gInPc1	terminál
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgInPc1d1	ostatní
znaky	znak	k1gInPc1	znak
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
)	)	kIx)	)
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
neterminály	neterminála	k1gFnPc1	neterminála
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Formální	formální	k2eAgFnPc1d1	formální
definice	definice	k1gFnPc1	definice
==	==	k?	==
</s>
</p>
<p>
<s>
Gramatika	gramatika	k1gFnSc1	gramatika
G	G	kA	G
je	být	k5eAaImIp3nS	být
čtveřice	čtveřice	k1gFnSc1	čtveřice
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Σ	Σ	k?	Σ
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
P	P	kA	P
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
N	n	k0	n
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
Sigma	sigma	k1gNnSc2	sigma
,	,	kIx,	,
<g/>
P	P	kA	P
<g/>
,	,	kIx,	,
<g/>
S	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
kde	kde	k6eAd1	kde
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
N	N	kA	N
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
konečná	konečný	k2eAgFnSc1d1	konečná
množina	množina	k1gFnSc1	množina
neterminálních	terminální	k2eNgInPc2d1	neterminální
symbolů	symbol	k1gInPc2	symbol
(	(	kIx(	(
<g/>
neterminálů	neterminál	k1gInPc2	neterminál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Σ	Σ	k?	Σ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Sigma	sigma	k1gNnPc3	sigma
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
konečná	konečný	k2eAgFnSc1d1	konečná
množina	množina	k1gFnSc1	množina
terminálních	terminální	k2eAgInPc2d1	terminální
symbolů	symbol	k1gInPc2	symbol
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádný	žádný	k3yNgInSc1	žádný
symbol	symbol	k1gInSc1	symbol
nepatří	patřit	k5eNaImIp3nS	patřit
do	do	k7c2	do
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
N	N	kA	N
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Σ	Σ	k?	Σ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Sigma	sigma	k1gNnPc3	sigma
}	}	kIx)	}
</s>
</p>
<p>
<s>
zároveň	zároveň	k6eAd1	zároveň
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
značí	značit	k5eAaImIp3nS	značit
řeckým	řecký	k2eAgNnSc7d1	řecké
písmenkem	písmenko	k1gNnSc7	písmenko
sigma	sigma	k1gNnSc2	sigma
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
P	P	kA	P
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
P	P	kA	P
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
konečná	konečný	k2eAgFnSc1d1	konečná
množina	množina	k1gFnSc1	množina
odvozovacích	odvozovací	k2eAgNnPc2d1	odvozovací
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc1	každý
pravidlo	pravidlo	k1gNnSc1	pravidlo
je	být	k5eAaImIp3nS	být
tvaru	tvar	k1gInSc3	tvar
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
Σ	Σ	k?	Σ
</s>
</p>
<p>
<s>
∪	∪	k?	∪
</s>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
∗	∗	k?	∗
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
Σ	Σ	k?	Σ
</s>
</p>
<p>
<s>
∪	∪	k?	∪
</s>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
∗	∗	k?	∗
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⟶	⟶	k?	⟶
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
Σ	Σ	k?	Σ
</s>
</p>
<p>
<s>
∪	∪	k?	∪
</s>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
∗	∗	k?	∗
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
Sigma	sigma	k1gNnSc1	sigma
\	\	kIx~	\
<g/>
cup	cup	k1gInSc1	cup
N	N	kA	N
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
*	*	kIx~	*
<g/>
}	}	kIx)	}
<g/>
N	N	kA	N
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
Sigma	sigma	k1gNnSc1	sigma
\	\	kIx~	\
<g/>
cup	cup	k1gInSc1	cup
N	N	kA	N
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
*	*	kIx~	*
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
longrightarrow	longrightarrow	k?	longrightarrow
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
Sigma	sigma	k1gNnSc1	sigma
\	\	kIx~	\
<g/>
cup	cup	k1gInSc1	cup
N	N	kA	N
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
*	*	kIx~	*
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
prvek	prvek	k1gInSc1	prvek
z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
N	N	kA	N
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
počáteční	počáteční	k2eAgInSc1d1	počáteční
symbol	symbol	k1gInSc1	symbol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Konvence	konvence	k1gFnSc2	konvence
==	==	k?	==
</s>
</p>
<p>
<s>
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
terminály	terminál	k1gInPc4	terminál
značíme	značit	k5eAaImIp1nP	značit
–	–	k?	–
a	a	k8xC	a
<g/>
,	,	kIx,	,
b	b	k?	b
<g/>
,	,	kIx,	,
c	c	k0	c
<g/>
,	,	kIx,	,
...	...	k?	...
</s>
</p>
<p>
<s>
řetězce	řetězec	k1gInPc4	řetězec
terminálů	terminál	k1gInPc2	terminál
značíme	značit	k5eAaImIp1nP	značit
–	–	k?	–
u	u	k7c2	u
<g/>
,	,	kIx,	,
v	v	k7c6	v
<g/>
,	,	kIx,	,
w	w	k?	w
<g/>
,	,	kIx,	,
...	...	k?	...
</s>
</p>
<p>
<s>
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
neterminály	neterminál	k1gInPc1	neterminál
–	–	k?	–
A	A	kA	A
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
,	,	kIx,	,
C	C	kA	C
...	...	k?	...
X	X	kA	X
<g/>
,	,	kIx,	,
Y	Y	kA	Y
<g/>
,	,	kIx,	,
Z	z	k7c2	z
</s>
</p>
<p>
<s>
řetězce	řetězec	k1gInPc1	řetězec
neterminálů	neterminál	k1gInPc2	neterminál
a	a	k8xC	a
terminálů	terminál	k1gInPc2	terminál
–	–	k?	–
α	α	k?	α
<g/>
,	,	kIx,	,
β	β	k?	β
<g/>
,	,	kIx,	,
γ	γ	k?	γ
<g/>
,	,	kIx,	,
...	...	k?	...
</s>
</p>
<p>
<s>
prázdný	prázdný	k2eAgInSc4d1	prázdný
řetězec	řetězec	k1gInSc4	řetězec
značíme	značit	k5eAaImIp1nP	značit
symbolem	symbol	k1gInSc7	symbol
e	e	k0	e
nebo	nebo	k8xC	nebo
také	také	k9	také
ε	ε	k?	ε
</s>
</p>
<p>
<s>
==	==	k?	==
Chomského	Chomský	k2eAgInSc2d1	Chomský
hierarchie	hierarchie	k1gFnSc1	hierarchie
==	==	k?	==
</s>
</p>
<p>
<s>
Chomského	Chomský	k2eAgInSc2d1	Chomský
hierarchie	hierarchie	k1gFnSc1	hierarchie
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
čtyři	čtyři	k4xCgInPc4	čtyři
typy	typ	k1gInPc4	typ
gramatik	gramatika	k1gFnPc2	gramatika
podle	podle	k7c2	podle
tvaru	tvar	k1gInSc2	tvar
přepisovacích	přepisovací	k2eAgNnPc2d1	přepisovací
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
množina	množina	k1gFnSc1	množina
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
P	P	kA	P
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
P	P	kA	P
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Typ	typ	k1gInSc1	typ
0	[number]	k4	0
–	–	k?	–
Všechny	všechen	k3xTgFnPc1	všechen
formální	formální	k2eAgFnPc1d1	formální
gramatiky	gramatika	k1gFnPc1	gramatika
(	(	kIx(	(
<g/>
neomezené	omezený	k2eNgFnPc1d1	neomezená
gramatiky	gramatika	k1gFnPc1	gramatika
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Typ	typ	k1gInSc1	typ
1	[number]	k4	1
–	–	k?	–
Kontextové	kontextový	k2eAgFnSc2d1	kontextová
gramatiky	gramatika	k1gFnSc2	gramatika
</s>
</p>
<p>
<s>
Typ	typ	k1gInSc1	typ
2	[number]	k4	2
–	–	k?	–
Bezkontextové	bezkontextový	k2eAgFnSc2d1	bezkontextová
gramatiky	gramatika	k1gFnSc2	gramatika
</s>
</p>
<p>
<s>
Typ	typ	k1gInSc1	typ
3	[number]	k4	3
–	–	k?	–
Regulární	regulární	k2eAgMnPc4d1	regulární
gramatikyPlatí	gramatikyPlatit	k5eAaPmIp3nS	gramatikyPlatit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jazyky	jazyk	k1gInPc1	jazyk
generované	generovaný	k2eAgInPc1d1	generovaný
gramatikami	gramatika	k1gFnPc7	gramatika
typu	typ	k1gInSc2	typ
3	[number]	k4	3
jsou	být	k5eAaImIp3nP	být
podmnožinou	podmnožina	k1gFnSc7	podmnožina
jazyků	jazyk	k1gInPc2	jazyk
generovaných	generovaný	k2eAgInPc2d1	generovaný
gramatikami	gramatika	k1gFnPc7	gramatika
typu	typ	k1gInSc2	typ
2	[number]	k4	2
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Formální	formální	k2eAgInSc1d1	formální
jazyk	jazyk	k1gInSc1	jazyk
</s>
</p>
<p>
<s>
Noam	Noam	k1gMnSc1	Noam
Chomsky	Chomsky	k1gMnSc1	Chomsky
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
