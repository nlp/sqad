<p>
<s>
A	a	k9	a
cappella	cappella	k6eAd1	cappella
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
označuje	označovat	k5eAaImIp3nS	označovat
vokální	vokální	k2eAgFnSc4d1	vokální
hudbu	hudba	k1gFnSc4	hudba
nebo	nebo	k8xC	nebo
zpěv	zpěv	k1gInSc4	zpěv
bez	bez	k7c2	bez
doprovodu	doprovod	k1gInSc2	doprovod
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
cappella	cappella	k6eAd1	cappella
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
italštině	italština	k1gFnSc6	italština
jako	jako	k8xC	jako
v	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
tomu	ten	k3xDgNnSc3	ten
tak	tak	k9	tak
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
a	a	k8xC	a
vrcholném	vrcholný	k2eAgInSc6d1	vrcholný
středověku	středověk	k1gInSc6	středověk
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
používat	používat	k5eAaImF	používat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
církevní	církevní	k2eAgFnSc2d1	církevní
hudby	hudba	k1gFnSc2	hudba
hudební	hudební	k2eAgInPc4d1	hudební
nástroje	nástroj	k1gInPc4	nástroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
A	a	k9	a
cappella	cappella	k6eAd1	cappella
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
používána	používat	k5eAaImNgFnS	používat
v	v	k7c6	v
církevní	církevní	k2eAgFnSc6d1	církevní
hudbě	hudba	k1gFnSc6	hudba
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
mezi	mezi	k7c7	mezi
černošskými	černošský	k2eAgMnPc7d1	černošský
věřícími	věřící	k2eAgMnPc7d1	věřící
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Angel	angel	k1gMnSc1	angel
City	City	k1gFnSc2	City
Chorale	Choral	k1gMnSc5	Choral
</s>
</p>
<p>
<s>
4TET	[number]	k4	4TET
</s>
</p>
<p>
<s>
DNA	DNA	kA	DNA
-	-	kIx~	-
Dej	dát	k5eAaPmRp2nS	dát
Nám	my	k3xPp1nPc3	my
Akord	akord	k1gInSc4	akord
</s>
</p>
<p>
<s>
The	The	k?	The
Real	Real	k1gInSc1	Real
Group	Group	k1gInSc1	Group
</s>
</p>
<p>
<s>
Bobby	Bobba	k1gFnPc1	Bobba
McFerrin	McFerrin	k1gInSc1	McFerrin
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Ševčík	Ševčík	k1gMnSc1	Ševčík
</s>
</p>
<p>
<s>
Van	van	k1gInSc1	van
Canto	canto	k1gNnSc1	canto
</s>
</p>
<p>
<s>
Viva	Viva	k1gMnSc1	Viva
Vox	Vox	k1gMnSc1	Vox
Choir	Choir	k1gMnSc1	Choir
</s>
</p>
<p>
<s>
Smooth	Smooth	k1gMnSc1	Smooth
McGroove	McGroov	k1gInSc5	McGroov
<g/>
/	/	kIx~	/
<g/>
Max	Max	k1gMnSc1	Max
Gleason	Gleason	k1gMnSc1	Gleason
</s>
</p>
