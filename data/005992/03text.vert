<s>
Hněv	hněv	k1gInSc4	hněv
je	být	k5eAaImIp3nS	být
silná	silný	k2eAgFnSc1d1	silná
afektivní	afektivní	k2eAgFnSc1d1	afektivní
reakce	reakce	k1gFnSc1	reakce
na	na	k7c4	na
překážku	překážka	k1gFnSc4	překážka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
staví	stavit	k5eAaPmIp3nS	stavit
do	do	k7c2	do
cesty	cesta	k1gFnSc2	cesta
při	při	k7c6	při
dosahování	dosahování	k1gNnSc6	dosahování
nějakého	nějaký	k3yIgInSc2	nějaký
cíle	cíl	k1gInSc2	cíl
nebo	nebo	k8xC	nebo
brání	bránit	k5eAaImIp3nS	bránit
rozvíjení	rozvíjení	k1gNnSc4	rozvíjení
jednání	jednání	k1gNnSc2	jednání
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
emoci	emoce	k1gFnSc4	emoce
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
původním	původní	k2eAgInSc7d1	původní
smyslem	smysl	k1gInSc7	smysl
bylo	být	k5eAaImAgNnS	být
připravit	připravit	k5eAaPmF	připravit
jedince	jedinec	k1gMnPc4	jedinec
na	na	k7c4	na
útok	útok	k1gInSc4	útok
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
intenzity	intenzita	k1gFnSc2	intenzita
se	se	k3xPyFc4	se
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
rozzlobenost	rozzlobenost	k1gFnSc1	rozzlobenost
neboli	neboli	k8xC	neboli
zloba	zloba	k1gFnSc1	zloba
(	(	kIx(	(
<g/>
nejslabší	slabý	k2eAgFnSc1d3	nejslabší
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hněv	hněv	k1gInSc4	hněv
<g/>
,	,	kIx,	,
zlost	zlost	k1gFnSc4	zlost
a	a	k8xC	a
vztek	vztek	k1gInSc4	vztek
(	(	kIx(	(
<g/>
nejsilnější	silný	k2eAgFnSc1d3	nejsilnější
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Projevy	projev	k1gInPc1	projev
hněvu	hněv	k1gInSc2	hněv
zpravidla	zpravidla	k6eAd1	zpravidla
trvají	trvat	k5eAaImIp3nP	trvat
krátce	krátce	k6eAd1	krátce
<g/>
,	,	kIx,	,
dlouhodobějším	dlouhodobý	k2eAgInSc7d2	dlouhodobější
projevem	projev	k1gInSc7	projev
je	být	k5eAaImIp3nS	být
hostilita	hostilita	k1gFnSc1	hostilita
(	(	kIx(	(
<g/>
nepřátelství	nepřátelství	k1gNnSc1	nepřátelství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
hněv	hněv	k1gInSc1	hněv
je	být	k5eAaImIp3nS	být
primárně	primárně	k6eAd1	primárně
násilnou	násilný	k2eAgFnSc7d1	násilná
reakcí	reakce	k1gFnSc7	reakce
<g/>
,	,	kIx,	,
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
ho	on	k3xPp3gNnSc4	on
lidé	člověk	k1gMnPc1	člověk
pokud	pokud	k8xS	pokud
možno	možno	k6eAd1	možno
brzdit	brzdit	k5eAaImF	brzdit
<g/>
,	,	kIx,	,
emočními	emoční	k2eAgInPc7d1	emoční
výjevy	výjev	k1gInPc7	výjev
(	(	kIx(	(
<g/>
např.	např.	kA	např.
zčervenáním	zčervenání	k1gNnSc7	zčervenání
<g/>
,	,	kIx,	,
roněním	ronění	k1gNnSc7	ronění
slz	slza	k1gFnPc2	slza
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
velké	velký	k2eAgFnSc6d1	velká
zlosti	zlost	k1gFnSc6	zlost
se	se	k3xPyFc4	se
místo	místo	k7c2	místo
fyzického	fyzický	k2eAgNnSc2d1	fyzické
napadení	napadení	k1gNnSc2	napadení
uchylují	uchylovat	k5eAaImIp3nP	uchylovat
spíše	spíše	k9	spíše
k	k	k7c3	k
urážkám	urážka	k1gFnPc3	urážka
<g/>
,	,	kIx,	,
výhrůžkám	výhrůžka	k1gFnPc3	výhrůžka
<g/>
.	.	kIx.	.
</s>
<s>
Projevy	projev	k1gInPc1	projev
hněvu	hněv	k1gInSc2	hněv
nejčastěji	často	k6eAd3	často
narušují	narušovat	k5eAaImIp3nP	narušovat
vztahy	vztah	k1gInPc4	vztah
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
mohou	moct	k5eAaImIp3nP	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
izolaci	izolace	k1gFnSc3	izolace
a	a	k8xC	a
bolestné	bolestný	k2eAgFnSc3d1	bolestná
osamělosti	osamělost	k1gFnSc3	osamělost
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
projevy	projev	k1gInPc1	projev
hněvu	hněv	k1gInSc2	hněv
mají	mít	k5eAaImIp3nP	mít
často	často	k6eAd1	často
svůj	svůj	k3xOyFgInSc4	svůj
důvod	důvod	k1gInSc4	důvod
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
přehnané	přehnaný	k2eAgFnSc3d1	přehnaná
a	a	k8xC	a
nepřiměřené	přiměřený	k2eNgFnSc3d1	nepřiměřená
situaci	situace	k1gFnSc3	situace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	on	k3xPp3gFnPc4	on
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
náboženského	náboženský	k2eAgNnSc2d1	náboženské
bývá	bývat	k5eAaImIp3nS	bývat
odsuzován	odsuzován	k2eAgInSc1d1	odsuzován
jako	jako	k8xS	jako
hřích	hřích	k1gInSc1	hřích
<g/>
,	,	kIx,	,
např.	např.	kA	např.
katolická	katolický	k2eAgFnSc1d1	katolická
nauka	nauka	k1gFnSc1	nauka
jej	on	k3xPp3gMnSc4	on
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
hlavních	hlavní	k2eAgInPc2d1	hlavní
hříchů	hřích	k1gInPc2	hřích
<g/>
.	.	kIx.	.
</s>
<s>
Vlastnost	vlastnost	k1gFnSc1	vlastnost
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
člověk	člověk	k1gMnSc1	člověk
snadno	snadno	k6eAd1	snadno
podléhá	podléhat	k5eAaImIp3nS	podléhat
vzteku	vztek	k1gInSc3	vztek
<g/>
,	,	kIx,	,
označujeme	označovat	k5eAaImIp1nP	označovat
jako	jako	k9	jako
prchlivost	prchlivost	k1gFnSc4	prchlivost
<g/>
.	.	kIx.	.
</s>
<s>
Fyziologickými	fyziologický	k2eAgInPc7d1	fyziologický
projevy	projev	k1gInPc7	projev
hněvu	hněv	k1gInSc2	hněv
jsou	být	k5eAaImIp3nP	být
některé	některý	k3yIgFnPc1	některý
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
tělesné	tělesný	k2eAgFnSc6d1	tělesná
činnosti	činnost	k1gFnSc6	činnost
<g/>
,	,	kIx,	,
směřující	směřující	k2eAgInSc4d1	směřující
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
fyzické	fyzický	k2eAgFnSc2d1	fyzická
síly	síla	k1gFnSc2	síla
organismu	organismus	k1gInSc2	organismus
–	–	k?	–
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
se	se	k3xPyFc4	se
srdeční	srdeční	k2eAgInSc1d1	srdeční
tep	tep	k1gInSc1	tep
<g/>
,	,	kIx,	,
stoupá	stoupat	k5eAaImIp3nS	stoupat
krevní	krevní	k2eAgInSc1d1	krevní
tlak	tlak	k1gInSc1	tlak
<g/>
,	,	kIx,	,
člověk	člověk	k1gMnSc1	člověk
zčervená	zčervenat	k5eAaPmIp3nS	zčervenat
<g/>
.	.	kIx.	.
</s>
<s>
Zlost	zlost	k1gFnSc4	zlost
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
některé	některý	k3yIgFnPc4	některý
drogy	droga	k1gFnPc4	droga
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
alkohol	alkohol	k1gInSc1	alkohol
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
stimulanty	stimulant	k1gInPc4	stimulant
.	.	kIx.	.
</s>
<s>
Opakem	opak	k1gInSc7	opak
hněvu	hněv	k1gInSc2	hněv
je	být	k5eAaImIp3nS	být
mírumilovnost	mírumilovnost	k1gFnSc4	mírumilovnost
<g/>
.	.	kIx.	.
</s>
