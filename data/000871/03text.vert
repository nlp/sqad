<s>
Sarah	Sarah	k1gFnSc1	Sarah
Bernhardt	Bernhardta	k1gFnPc2	Bernhardta
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Henriette-Marie-Sarah	Henriette-Marie-Sarah	k1gInSc4	Henriette-Marie-Sarah
Bernardt	Bernardt	k2eAgMnSc1d1	Bernardt
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1844	[number]	k4	1844
Paříž	Paříž	k1gFnSc1	Paříž
-	-	kIx~	-
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1923	[number]	k4	1923
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
francouzská	francouzský	k2eAgFnSc1d1	francouzská
herečka	herečka	k1gFnSc1	herečka
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
divadelních	divadelní	k2eAgFnPc2d1	divadelní
osobností	osobnost	k1gFnPc2	osobnost
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	s	k7c7	s
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1844	[number]	k4	1844
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
nejstarší	starý	k2eAgFnSc7d3	nejstarší
a	a	k8xC	a
nemanželskou	manželský	k2eNgFnSc7d1	nemanželská
dcerou	dcera	k1gFnSc7	dcera
Judith-Julie	Judith-Julie	k1gFnSc2	Judith-Julie
Bernardtové	Bernardtová	k1gFnSc2	Bernardtová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
sama	sám	k3xTgMnSc4	sám
byla	být	k5eAaImAgFnS	být
dcerou	dcera	k1gFnSc7	dcera
holandského	holandský	k2eAgMnSc4d1	holandský
varietního	varietní	k2eAgMnSc4d1	varietní
podnikatele	podnikatel	k1gMnSc4	podnikatel
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
možná	možná	k9	možná
také	také	k9	také
podvodníka	podvodník	k1gMnSc4	podvodník
<g/>
)	)	kIx)	)
jménem	jméno	k1gNnSc7	jméno
Moritz	Moritz	k1gMnSc1	Moritz
Baruch	Baruch	k1gMnSc1	Baruch
Bernardt	Bernardt	k1gMnSc1	Bernardt
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
povětšinou	povětšinou	k6eAd1	povětšinou
nemajetná	majetný	k2eNgFnSc1d1	nemajetná
(	(	kIx(	(
<g/>
sans	sans	k6eAd1	sans
le	le	k?	le
sou	sou	k1gInSc1	sou
<g/>
)	)	kIx)	)
a	a	k8xC	a
působila	působit	k5eAaImAgFnS	působit
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
jako	jako	k8xC	jako
kurtizána	kurtizána	k1gFnSc1	kurtizána
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
"	"	kIx"	"
<g/>
Youle	Youle	k1gNnSc7	Youle
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Sarah	Sarah	k1gFnSc1	Sarah
Bernhardt	Bernhardta	k1gFnPc2	Bernhardta
se	se	k3xPyFc4	se
ale	ale	k9	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
sama	sám	k3xTgFnSc1	sám
bez	bez	k7c2	bez
patřičného	patřičný	k2eAgNnSc2d1	patřičné
oprávnění	oprávnění	k1gNnSc2	oprávnění
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
za	za	k7c4	za
dceru	dcera	k1gFnSc4	dcera
páru	pár	k1gInSc2	pár
Judith	Juditha	k1gFnPc2	Juditha
van	van	k1gInSc1	van
Hard	Hard	k1gMnSc1	Hard
a	a	k8xC	a
Édouard	Édouard	k1gMnSc1	Édouard
Bernardt	Bernardt	k1gMnSc1	Bernardt
z	z	k7c2	z
přístavního	přístavní	k2eAgNnSc2d1	přístavní
města	město	k1gNnSc2	město
Le	Le	k1gMnSc5	Le
Havre	Havr	k1gMnSc5	Havr
<g/>
.	.	kIx.	.
</s>
<s>
Chtěla	chtít	k5eAaImAgFnS	chtít
pomocí	pomocí	k7c2	pomocí
tohoto	tento	k3xDgInSc2	tento
postupu	postup	k1gInSc2	postup
získat	získat	k5eAaPmF	získat
francouzské	francouzský	k2eAgNnSc4d1	francouzské
občanství	občanství	k1gNnSc4	občanství
a	a	k8xC	a
oprávnění	oprávnění	k1gNnSc4	oprávnění
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
obdržela	obdržet	k5eAaPmAgFnS	obdržet
Řád	řád	k1gInSc4	řád
Čestné	čestný	k2eAgFnSc2d1	čestná
legie	legie	k1gFnSc2	legie
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
také	také	k9	také
podařilo	podařit	k5eAaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
přání	přání	k1gNnSc4	přání
svého	své	k1gNnSc2	své
(	(	kIx(	(
<g/>
nám	my	k3xPp1nPc3	my
neznámého	známý	k2eNgMnSc2d1	neznámý
<g/>
)	)	kIx)	)
otce	otec	k1gMnSc2	otec
byla	být	k5eAaImAgFnS	být
Sarah	Sarah	k1gFnSc1	Sarah
Bernhardtová	Bernhardtová	k1gFnSc1	Bernhardtová
pokřtěna	pokřtěn	k2eAgFnSc1d1	pokřtěna
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
byla	být	k5eAaImAgFnS	být
svěřena	svěřit	k5eAaPmNgFnS	svěřit
do	do	k7c2	do
pěstounské	pěstounský	k2eAgFnSc2d1	pěstounská
péče	péče	k1gFnSc2	péče
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
osmi	osm	k4xCc6	osm
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1852	[number]	k4	1852
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
odeslána	odeslat	k5eAaPmNgFnS	odeslat
do	do	k7c2	do
pensionátu	pensionát	k1gInSc2	pensionát
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1854	[number]	k4	1854
navštěvovala	navštěvovat	k5eAaImAgFnS	navštěvovat
klášterní	klášterní	k2eAgFnSc4d1	klášterní
školu	škola	k1gFnSc4	škola
ve	v	k7c6	v
Versailles	Versailles	k1gFnSc6	Versailles
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1858	[number]	k4	1858
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
vévoda	vévoda	k1gMnSc1	vévoda
de	de	k?	de
Morny	morna	k1gFnSc2	morna
(	(	kIx(	(
<g/>
Duc	duc	k0	duc
de	de	k?	de
Morny	morna	k1gFnPc1	morna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nevlastní	vlastnit	k5eNaImIp3nS	vlastnit
bratr	bratr	k1gMnSc1	bratr
Napoleona	Napoleon	k1gMnSc2	Napoleon
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
milenec	milenec	k1gMnSc1	milenec
její	její	k3xOp3gFnSc2	její
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
vymohl	vymoct	k5eAaPmAgInS	vymoct
výuku	výuka	k1gFnSc4	výuka
herectví	herectví	k1gNnSc2	herectví
na	na	k7c6	na
divadle	divadlo	k1gNnSc6	divadlo
Comédie-Française	Comédie-Française	k1gFnSc2	Comédie-Française
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čtyřech	čtyři	k4xCgInPc6	čtyři
letech	let	k1gInPc6	let
studia	studio	k1gNnSc2	studio
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
1862	[number]	k4	1862
<g/>
)	)	kIx)	)
debutovala	debutovat	k5eAaBmAgFnS	debutovat
v	v	k7c6	v
titulní	titulní	k2eAgFnSc6d1	titulní
roli	role	k1gFnSc6	role
Racinova	Racinův	k2eAgNnSc2d1	Racinův
dramatu	drama	k1gNnSc2	drama
Ifigenie	Ifigenie	k1gFnSc2	Ifigenie
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
roli	role	k1gFnSc6	role
používala	používat	k5eAaImAgFnS	používat
své	svůj	k3xOyFgNnSc4	svůj
umělecké	umělecký	k2eAgNnSc4d1	umělecké
jméno	jméno	k1gNnSc4	jméno
Sarah	Sarah	k1gFnSc2	Sarah
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
kariéra	kariéra	k1gFnSc1	kariéra
byla	být	k5eAaImAgFnS	být
však	však	k9	však
po	po	k7c6	po
několika	několik	k4yIc6	několik
měsících	měsíc	k1gInPc6	měsíc
na	na	k7c4	na
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
přerušena	přerušen	k2eAgFnSc1d1	přerušena
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
divadla	divadlo	k1gNnSc2	divadlo
propuštěna	propustit	k5eAaPmNgFnS	propustit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
poprala	poprat	k5eAaPmAgFnS	poprat
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
kolegyní	kolegyně	k1gFnSc7	kolegyně
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
hrála	hrát	k5eAaImAgFnS	hrát
zpravidla	zpravidla	k6eAd1	zpravidla
vedlejší	vedlejší	k2eAgFnPc4d1	vedlejší
role	role	k1gFnPc4	role
na	na	k7c6	na
malých	malý	k2eAgFnPc6d1	malá
scénách	scéna	k1gFnPc6	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1864	[number]	k4	1864
porodila	porodit	k5eAaPmAgFnS	porodit
syna	syn	k1gMnSc4	syn
pokřtěného	pokřtěný	k2eAgMnSc4d1	pokřtěný
jako	jako	k8xC	jako
Maurice	Maurika	k1gFnSc6	Maurika
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
otcem	otec	k1gMnSc7	otec
byl	být	k5eAaImAgMnS	být
belgický	belgický	k2eAgMnSc1d1	belgický
kníže	kníže	k1gMnSc1	kníže
Henri	Henr	k1gFnSc2	Henr
de	de	k?	de
Ligne	Lign	k1gMnSc5	Lign
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
zájem	zájem	k1gInSc4	zájem
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
oženit	oženit	k5eAaPmF	oženit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
tomuto	tento	k3xDgInSc3	tento
kroku	krok	k1gInSc3	krok
postavila	postavit	k5eAaPmAgFnS	postavit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1868	[number]	k4	1868
Sarah	Sarah	k1gFnSc1	Sarah
Bernhardtová	Bernhardtová	k1gFnSc1	Bernhardtová
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
v	v	k7c6	v
pařížském	pařížský	k2eAgNnSc6d1	pařížské
divadle	divadlo	k1gNnSc6	divadlo
Odéon	Odéona	k1gFnPc2	Odéona
v	v	k7c6	v
Lucemburských	lucemburský	k2eAgFnPc6d1	Lucemburská
zahradách	zahrada	k1gFnPc6	zahrada
(	(	kIx(	(
<g/>
Jardin	Jardin	k2eAgInSc4d1	Jardin
de	de	k?	de
Luxembourg	Luxembourg	k1gInSc4	Luxembourg
<g/>
)	)	kIx)	)
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
Kean	Kean	k1gInSc1	Kean
od	od	k7c2	od
Alexandra	Alexandr	k1gMnSc2	Alexandr
Dumase	Dumasa	k1gFnSc6	Dumasa
staršího	starší	k1gMnSc2	starší
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prusko-francouzské	pruskorancouzský	k2eAgFnSc6d1	prusko-francouzská
válce	válka	k1gFnSc6	válka
(	(	kIx(	(
<g/>
1870	[number]	k4	1870
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgNnP	být
uzavřena	uzavřen	k2eAgNnPc1d1	uzavřeno
divadla	divadlo	k1gNnPc1	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
pečovala	pečovat	k5eAaImAgFnS	pečovat
Sarah	Sarah	k1gFnSc7	Sarah
Bernhardtová	Bernhardtový	k2eAgFnSc1d1	Bernhardtová
o	o	k7c6	o
raněné	raněný	k2eAgFnSc6d1	raněná
a	a	k8xC	a
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
mohla	moct	k5eAaImAgFnS	moct
vrátit	vrátit	k5eAaPmF	vrátit
do	do	k7c2	do
Comédie-Française	Comédie-Française	k1gFnSc2	Comédie-Française
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
začíná	začínat	k5eAaImIp3nS	začínat
její	její	k3xOp3gInSc4	její
vzestup	vzestup	k1gInSc4	vzestup
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
je	být	k5eAaImIp3nS	být
pokládána	pokládán	k2eAgFnSc1d1	pokládána
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
div	diva	k1gFnPc2	diva
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
Francii	Francie	k1gFnSc4	Francie
byla	být	k5eAaImAgFnS	být
kritikou	kritika	k1gFnSc7	kritika
oslavována	oslavován	k2eAgFnSc1d1	oslavována
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
la	la	k1gNnSc1	la
voix	voix	k1gInSc1	voix
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
or	or	k?	or
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
zlatý	zlatý	k2eAgInSc1d1	zlatý
hlas	hlas	k1gInSc1	hlas
<g/>
)	)	kIx)	)
či	či	k8xC	či
"	"	kIx"	"
<g/>
la	la	k1gNnSc1	la
divine	divinout	k5eAaPmIp3nS	divinout
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
božská	božská	k1gFnSc1	božská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1882	[number]	k4	1882
se	se	k3xPyFc4	se
vdala	vdát	k5eAaPmAgFnS	vdát
za	za	k7c4	za
Jaquese	Jaquese	k1gFnPc4	Jaquese
Damalu	Damal	k1gMnSc3	Damal
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
působil	působit	k5eAaImAgMnS	působit
jako	jako	k8xC	jako
atašé	atašé	k1gMnSc1	atašé
řeckého	řecký	k2eAgNnSc2d1	řecké
vyslanectví	vyslanectví	k1gNnSc2	vyslanectví
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Jaques	Jaques	k1gInSc1	Jaques
Damalu	Damal	k1gInSc2	Damal
se	se	k3xPyFc4	se
také	také	k9	také
věnoval	věnovat	k5eAaPmAgMnS	věnovat
herectví	herectví	k1gNnSc4	herectví
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
spolu	spolu	k6eAd1	spolu
otevřeli	otevřít	k5eAaPmAgMnP	otevřít
vlastní	vlastní	k2eAgNnSc4d1	vlastní
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
,	,	kIx,	,
ředitelem	ředitel	k1gMnSc7	ředitel
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
její	její	k3xOp3gMnSc1	její
syn	syn	k1gMnSc1	syn
Maurice	Maurika	k1gFnSc6	Maurika
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
divadlo	divadlo	k1gNnSc1	divadlo
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
dostalo	dostat	k5eAaPmAgNnS	dostat
do	do	k7c2	do
finančních	finanční	k2eAgFnPc2d1	finanční
potíží	potíž	k1gFnPc2	potíž
a	a	k8xC	a
poté	poté	k6eAd1	poté
zbankrotovalo	zbankrotovat	k5eAaPmAgNnS	zbankrotovat
<g/>
,	,	kIx,	,
za	za	k7c4	za
příčinu	příčina	k1gFnSc4	příčina
bývá	bývat	k5eAaImIp3nS	bývat
uváděn	uváděn	k2eAgInSc1d1	uváděn
jednak	jednak	k8xC	jednak
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
Demala	Demala	k1gFnSc1	Demala
svým	svůj	k3xOyFgMnSc7	svůj
výrazným	výrazný	k2eAgInSc7d1	výrazný
řeckým	řecký	k2eAgInSc7d1	řecký
akcentem	akcent	k1gInSc7	akcent
vzbuzoval	vzbuzovat	k5eAaImAgInS	vzbuzovat
u	u	k7c2	u
diváků	divák	k1gMnPc2	divák
smích	smích	k1gInSc4	smích
<g/>
.	.	kIx.	.
</s>
<s>
Pravdou	pravda	k1gFnSc7	pravda
ovšem	ovšem	k9	ovšem
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
Demala	Demala	k1gFnSc1	Demala
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
závislý	závislý	k2eAgMnSc1d1	závislý
na	na	k7c6	na
morfiu	morfium	k1gNnSc6	morfium
a	a	k8xC	a
hazardních	hazardní	k2eAgFnPc6d1	hazardní
hrách	hra	k1gFnPc6	hra
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
vášně	vášeň	k1gFnPc4	vášeň
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
brzy	brzy	k6eAd1	brzy
financovat	financovat	k5eAaBmF	financovat
právě	právě	k9	právě
ze	z	k7c2	z
zisků	zisk	k1gInPc2	zisk
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Damalova	Damalův	k2eAgFnSc1d1	Damalův
závislost	závislost	k1gFnSc1	závislost
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1882	[number]	k4	1882
žili	žít	k5eAaImAgMnP	žít
odděleně	odděleně	k6eAd1	odděleně
<g/>
.	.	kIx.	.
</s>
<s>
Damala	Damal	k1gMnSc4	Damal
pak	pak	k6eAd1	pak
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1889	[number]	k4	1889
<g/>
.	.	kIx.	.
</s>
<s>
Krach	krach	k1gInSc1	krach
divadla	divadlo	k1gNnSc2	divadlo
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
znamenal	znamenat	k5eAaImAgInS	znamenat
velkou	velký	k2eAgFnSc4d1	velká
finanční	finanční	k2eAgFnSc4d1	finanční
ztrátu	ztráta	k1gFnSc4	ztráta
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vyrovnávala	vyrovnávat	k5eAaImAgFnS	vyrovnávat
jen	jen	k9	jen
díky	díky	k7c3	díky
zahraničnímu	zahraniční	k2eAgNnSc3d1	zahraniční
turné	turné	k1gNnSc3	turné
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
utrpěla	utrpět	k5eAaPmAgFnS	utrpět
úraz	úraz	k1gInSc4	úraz
kolena	koleno	k1gNnSc2	koleno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
tohoto	tento	k3xDgInSc2	tento
úrazu	úraz	k1gInSc2	úraz
musela	muset	k5eAaImAgFnS	muset
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
podstoupit	podstoupit	k5eAaPmF	podstoupit
amputaci	amputace	k1gFnSc4	amputace
pravé	pravý	k2eAgFnSc2d1	pravá
dolní	dolní	k2eAgFnSc2d1	dolní
končetiny	končetina	k1gFnSc2	končetina
pod	pod	k7c7	pod
kyčelním	kyčelní	k2eAgInSc7d1	kyčelní
kloubem	kloub	k1gInSc7	kloub
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
herecky	herecky	k6eAd1	herecky
angažovala	angažovat	k5eAaBmAgFnS	angažovat
<g/>
,	,	kIx,	,
během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
hrála	hrát	k5eAaImAgFnS	hrát
ve	v	k7c6	v
stanech	stan	k1gInPc6	stan
<g/>
,	,	kIx,	,
stodolách	stodola	k1gFnPc6	stodola
a	a	k8xC	a
lazaretech	lazaret	k1gInPc6	lazaret
na	na	k7c6	na
improvizovaných	improvizovaný	k2eAgNnPc6d1	improvizované
jevištích	jeviště	k1gNnPc6	jeviště
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc1	její
zásluhy	zásluha	k1gFnPc1	zásluha
byly	být	k5eAaImAgFnP	být
oceněny	ocenit	k5eAaPmNgFnP	ocenit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
jmenována	jmenovat	k5eAaImNgFnS	jmenovat
profesorkou	profesorka	k1gFnSc7	profesorka
na	na	k7c6	na
pařížské	pařížský	k2eAgFnSc6d1	Pařížská
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
byla	být	k5eAaImAgFnS	být
členkou	členka	k1gFnSc7	členka
Čestné	čestný	k2eAgFnSc2d1	čestná
legie	legie	k1gFnSc2	legie
<g/>
.	.	kIx.	.
</s>
<s>
Sarah	Sarah	k1gFnSc1	Sarah
Bernhardtová	Bernhardtová	k1gFnSc1	Bernhardtová
zemřela	zemřít	k5eAaPmAgFnS	zemřít
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1923	[number]	k4	1923
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
na	na	k7c4	na
selhání	selhání	k1gNnSc4	selhání
ledvin	ledvina	k1gFnPc2	ledvina
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
pochována	pochovat	k5eAaPmNgFnS	pochovat
na	na	k7c6	na
tamním	tamní	k2eAgInSc6d1	tamní
hřbitově	hřbitov	k1gInSc6	hřbitov
Pè	Pè	k1gFnSc2	Pè
Lachaise	Lachaise	k1gFnSc2	Lachaise
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
hereckého	herecký	k2eAgNnSc2d1	herecké
umění	umění	k1gNnSc2	umění
proslula	proslout	k5eAaPmAgFnS	proslout
Sarah	Sarah	k1gFnSc1	Sarah
Bernhardtová	Bernhardtový	k2eAgFnSc1d1	Bernhardtová
i	i	k9	i
jako	jako	k8xC	jako
excentrická	excentrický	k2eAgFnSc1d1	excentrická
a	a	k8xC	a
náladová	náladový	k2eAgFnSc1d1	náladová
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
milenců	milenec	k1gMnPc2	milenec
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byli	být	k5eAaImAgMnP	být
například	například	k6eAd1	například
Charles	Charles	k1gMnSc1	Charles
Haas	Haasa	k1gFnPc2	Haasa
<g/>
,	,	kIx,	,
Mounet-Sully	Mounet-Sulla	k1gFnSc2	Mounet-Sulla
či	či	k8xC	či
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
Gustave	Gustav	k1gMnSc5	Gustav
Doré	Dorý	k2eAgNnSc5d1	Dorý
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
bytě	byt	k1gInSc6	byt
chovala	chovat	k5eAaImAgFnS	chovat
řadu	řada	k1gFnSc4	řada
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgNnPc7	jenž
kromě	kromě	k7c2	kromě
psů	pes	k1gMnPc2	pes
a	a	k8xC	a
koček	kočka	k1gFnPc2	kočka
byla	být	k5eAaImAgNnP	být
i	i	k8xC	i
exotická	exotický	k2eAgNnPc1d1	exotické
zvířata	zvíře	k1gNnPc1	zvíře
jako	jako	k8xS	jako
papoušek	papoušek	k1gMnSc1	papoušek
<g/>
,	,	kIx,	,
opice	opice	k1gFnSc1	opice
<g/>
,	,	kIx,	,
gepard	gepard	k1gMnSc1	gepard
<g/>
,	,	kIx,	,
chameleon	chameleon	k1gMnSc1	chameleon
<g/>
,	,	kIx,	,
had	had	k1gMnSc1	had
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
lev	lev	k1gMnSc1	lev
<g/>
.	.	kIx.	.
</s>
<s>
Sarah	Sarah	k1gFnSc1	Sarah
Bernhardtová	Bernhardtová	k1gFnSc1	Bernhardtová
byla	být	k5eAaImAgFnS	být
mnohostranná	mnohostranný	k2eAgFnSc1d1	mnohostranná
<g/>
,	,	kIx,	,
talentovaná	talentovaný	k2eAgFnSc1d1	talentovaná
herečka	herečka	k1gFnSc1	herečka
<g/>
,	,	kIx,	,
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
úspěchu	úspěch	k1gInSc3	úspěch
jak	jak	k8xS	jak
v	v	k7c6	v
klasickém	klasický	k2eAgInSc6d1	klasický
repertoáru	repertoár	k1gInSc6	repertoár
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
moderních	moderní	k2eAgFnPc6d1	moderní
hrách	hra	k1gFnPc6	hra
<g/>
.	.	kIx.	.
</s>
<s>
Proslula	proslout	k5eAaPmAgFnS	proslout
krásným	krásný	k2eAgInSc7d1	krásný
hlasem	hlas	k1gInSc7	hlas
<g/>
,	,	kIx,	,
pohybovou	pohybový	k2eAgFnSc7d1	pohybová
elegancí	elegance	k1gFnSc7	elegance
a	a	k8xC	a
temperamentem	temperament	k1gInSc7	temperament
<g/>
,	,	kIx,	,
tyto	tento	k3xDgFnPc4	tento
vlastnosti	vlastnost	k1gFnPc4	vlastnost
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
dělaly	dělat	k5eAaImAgFnP	dělat
ideální	ideální	k2eAgFnSc4d1	ideální
herečku	herečka	k1gFnSc4	herečka
v	v	k7c6	v
období	období	k1gNnSc6	období
romantismu	romantismus	k1gInSc2	romantismus
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
si	se	k3xPyFc3	se
potrpělo	potrpět	k5eAaImAgNnS	potrpět
na	na	k7c4	na
přehnané	přehnaný	k2eAgFnPc4d1	přehnaná
deklamace	deklamace	k1gFnPc4	deklamace
a	a	k8xC	a
velká	velký	k2eAgNnPc4d1	velké
rozmáchnutá	rozmáchnutý	k2eAgNnPc4d1	rozmáchnutý
gesta	gesto	k1gNnPc4	gesto
<g/>
.	.	kIx.	.
</s>
<s>
Největšího	veliký	k2eAgInSc2d3	veliký
úspěchu	úspěch	k1gInSc2	úspěch
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
ve	v	k7c6	v
francouzských	francouzský	k2eAgFnPc6d1	francouzská
moderních	moderní	k2eAgFnPc6d1	moderní
hrách	hra	k1gFnPc6	hra
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
nelze	lze	k6eNd1	lze
opomíjet	opomíjet	k5eAaImF	opomíjet
i	i	k9	i
její	její	k3xOp3gNnSc4	její
úspěšné	úspěšný	k2eAgNnSc4d1	úspěšné
ztvárnění	ztvárnění	k1gNnSc4	ztvárnění
nefrancouzského	francouzský	k2eNgNnSc2d1	nefrancouzské
dramatu	drama	k1gNnSc2	drama
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
dokázala	dokázat	k5eAaPmAgNnP	dokázat
ztvárnit	ztvárnit	k5eAaPmF	ztvárnit
i	i	k9	i
řadu	řada	k1gFnSc4	řada
mužských	mužský	k2eAgFnPc2d1	mužská
rolí	role	k1gFnPc2	role
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
nejznámější	známý	k2eAgMnSc1d3	nejznámější
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
její	její	k3xOp3gNnSc1	její
vystoupení	vystoupení	k1gNnSc1	vystoupení
v	v	k7c6	v
roli	role	k1gFnSc6	role
Hamleta	Hamlet	k1gMnSc2	Hamlet
od	od	k7c2	od
Williama	William	k1gMnSc2	William
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
(	(	kIx(	(
<g/>
1899	[number]	k4	1899
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
aiglon	aiglon	k1gInSc1	aiglon
(	(	kIx(	(
<g/>
Orlík	Orlík	k1gInSc1	Orlík
<g/>
)	)	kIx)	)
od	od	k7c2	od
Edmonda	Edmond	k1gMnSc2	Edmond
Rostanda	Rostanda	k1gFnSc1	Rostanda
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
roli	role	k1gFnSc4	role
vévody	vévoda	k1gMnSc2	vévoda
Zákupského	Zákupský	k2eAgMnSc2d1	Zákupský
<g/>
,	,	kIx,	,
v	v	k7c6	v
Mussetově	Mussetův	k2eAgFnSc6d1	Mussetův
hře	hra	k1gFnSc6	hra
Lorenzaccio	Lorenzaccio	k6eAd1	Lorenzaccio
pak	pak	k6eAd1	pak
titulní	titulní	k2eAgFnSc4d1	titulní
roli	role	k1gFnSc4	role
Lorenza	Lorenza	k?	Lorenza
Medicejského	Medicejský	k2eAgMnSc2d1	Medicejský
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
úspěch	úspěch	k1gInSc1	úspěch
měla	mít	k5eAaImAgFnS	mít
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
Fedra	Fedr	k1gInSc2	Fedr
a	a	k8xC	a
Hyppolit	Hyppolit	k1gInSc1	Hyppolit
od	od	k7c2	od
Jana	Jan	k1gMnSc2	Jan
Racina	Racin	k1gMnSc2	Racin
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrála	hrát	k5eAaImAgFnS	hrát
Fedru	Fedra	k1gFnSc4	Fedra
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k1gNnSc1	další
výrazné	výrazný	k2eAgFnSc2d1	výrazná
role	role	k1gFnSc2	role
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
v	v	k7c6	v
hrách	hra	k1gFnPc6	hra
Victora	Victor	k1gMnSc2	Victor
Huga	Hugo	k1gMnSc2	Hugo
Ruy	Ruy	k1gMnSc2	Ruy
Blas	Blas	k1gInSc1	Blas
a	a	k8xC	a
Hernani	Hernan	k1gMnPc1	Hernan
<g/>
.	.	kIx.	.
</s>
<s>
Ústřední	ústřední	k2eAgFnSc7d1	ústřední
hrou	hra	k1gFnSc7	hra
jejího	její	k3xOp3gInSc2	její
života	život	k1gInSc2	život
byla	být	k5eAaImAgFnS	být
Dáma	dáma	k1gFnSc1	dáma
s	s	k7c7	s
kameliemi	kamelie	k1gFnPc7	kamelie
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
napsal	napsat	k5eAaBmAgMnS	napsat
Alexandre	Alexandr	k1gInSc5	Alexandr
Dumas	Dumas	k1gMnSc1	Dumas
mladší	mladý	k2eAgMnSc1d2	mladší
<g/>
,	,	kIx,	,
tento	tento	k3xDgMnSc1	tento
příběh	příběh	k1gInSc4	příběh
odpovídal	odpovídat	k5eAaImAgMnS	odpovídat
nejen	nejen	k6eAd1	nejen
vkusu	vkus	k1gInSc2	vkus
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
publika	publikum	k1gNnSc2	publikum
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
jejímu	její	k3xOp3gInSc3	její
temperamentu	temperament	k1gInSc3	temperament
a	a	k8xC	a
Sarah	Sarah	k1gFnSc1	Sarah
Bernhardtová	Bernhardtová	k1gFnSc1	Bernhardtová
hrála	hrát	k5eAaImAgFnS	hrát
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
divadelní	divadelní	k2eAgFnSc6d1	divadelní
hře	hra	k1gFnSc6	hra
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1880	[number]	k4	1880
až	až	k9	až
do	do	k7c2	do
vysokého	vysoký	k2eAgInSc2d1	vysoký
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
hrála	hrát	k5eAaImAgFnS	hrát
i	i	k9	i
ve	v	k7c6	v
filmu	film	k1gInSc6	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
(	(	kIx(	(
<g/>
La	la	k1gNnSc6	la
Dame	Dam	k1gInSc2	Dam
aux	aux	k?	aux
camélias	camélias	k1gMnSc1	camélias
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jí	on	k3xPp3gFnSc7	on
bylo	být	k5eAaImAgNnS	být
již	již	k9	již
šedesát	šedesát	k4xCc4	šedesát
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
Sarah	Sarah	k1gFnSc1	Sarah
Bernhardtová	Bernhardtová	k1gFnSc1	Bernhardtová
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
(	(	kIx(	(
<g/>
Le	Le	k1gFnSc1	Le
Duel	duel	k1gInSc1	duel
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Hamlet	Hamlet	k1gMnSc1	Hamlet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
filmu	film	k1gInSc6	film
zaujala	zaujmout	k5eAaPmAgFnS	zaujmout
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
umění	umění	k1gNnSc3	umění
negativní	negativní	k2eAgInSc4d1	negativní
postoj	postoj	k1gInSc4	postoj
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
však	však	k9	však
ještě	ještě	k6eAd1	ještě
hrála	hrát	k5eAaImAgFnS	hrát
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Tosca	Tosca	k1gFnSc1	Tosca
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Lásky	láska	k1gFnSc2	láska
královny	královna	k1gFnSc2	královna
Alžběty	Alžběta	k1gFnSc2	Alžběta
<g/>
.	.	kIx.	.
</s>
<s>
Sarah	Sarah	k1gFnSc1	Sarah
Bernhardtová	Bernhardtová	k1gFnSc1	Bernhardtová
mj.	mj.	kA	mj.
vedla	vést	k5eAaImAgFnS	vést
několik	několik	k4yIc4	několik
divadel	divadlo	k1gNnPc2	divadlo
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgNnPc6	který
také	také	k6eAd1	také
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgMnPc1d3	nejvýznamnější
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
Theatre	Theatr	k1gInSc5	Theatr
des	des	k1gNnPc4	des
Nations	Nationsa	k1gFnPc2	Nationsa
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
na	na	k7c6	na
Theatre	Theatr	k1gInSc5	Theatr
Sarah	Sarah	k1gFnSc7	Sarah
Bernhardt	Bernhardta	k1gFnPc2	Bernhardta
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
si	se	k3xPyFc3	se
toto	tento	k3xDgNnSc1	tento
divadlo	divadlo	k1gNnSc1	divadlo
podrželo	podržet	k5eAaPmAgNnS	podržet
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
součástí	součást	k1gFnSc7	součást
umělecké	umělecký	k2eAgFnSc2d1	umělecká
činnosti	činnost	k1gFnSc2	činnost
Sarah	Sarah	k1gFnSc1	Sarah
Bernhardtové	Bernhardtová	k1gFnSc2	Bernhardtová
byla	být	k5eAaImAgFnS	být
její	její	k3xOp3gNnSc4	její
vystupování	vystupování	k1gNnSc4	vystupování
v	v	k7c6	v
cizině	cizina	k1gFnSc6	cizina
<g/>
,	,	kIx,	,
na	na	k7c6	na
která	který	k3yRgFnSc1	který
jezdila	jezdit	k5eAaImAgFnS	jezdit
s	s	k7c7	s
vlastní	vlastní	k2eAgFnSc7d1	vlastní
hereckou	herecký	k2eAgFnSc7d1	herecká
společností	společnost	k1gFnSc7	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1879	[number]	k4	1879
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1880	[number]	k4	1880
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
šestiměsíční	šestiměsíční	k2eAgNnSc4d1	šestiměsíční
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
navštívila	navštívit	k5eAaPmAgFnS	navštívit
51	[number]	k4	51
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1881	[number]	k4	1881
a	a	k8xC	a
následně	následně	k6eAd1	následně
pak	pak	k6eAd1	pak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1882	[number]	k4	1882
(	(	kIx(	(
<g/>
z	z	k7c2	z
finančních	finanční	k2eAgInPc2d1	finanční
důvodů	důvod	k1gInPc2	důvod
<g/>
)	)	kIx)	)
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
velké	velký	k2eAgNnSc4d1	velké
evropské	evropský	k2eAgNnSc4d1	Evropské
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
navštívila	navštívit	k5eAaPmAgFnS	navštívit
Rusko	Rusko	k1gNnSc4	Rusko
<g/>
,	,	kIx,	,
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
,	,	kIx,	,
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
,	,	kIx,	,
Belgii	Belgie	k1gFnSc6	Belgie
a	a	k8xC	a
Nizozemí	Nizozemí	k1gNnSc6	Nizozemí
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1886	[number]	k4	1886
a	a	k8xC	a
1889	[number]	k4	1889
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
několikrát	několikrát	k6eAd1	několikrát
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1891	[number]	k4	1891
až	až	k9	až
1893	[number]	k4	1893
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
světové	světový	k2eAgNnSc4d1	světové
turné	turné	k1gNnSc4	turné
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
odmítala	odmítat	k5eAaImAgFnS	odmítat
vystupovat	vystupovat	k5eAaImF	vystupovat
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Sarah	Sarah	k1gFnSc1	Sarah
Bernhardtová	Bernhardtová	k1gFnSc1	Bernhardtová
byla	být	k5eAaImAgFnS	být
mimo	mimo	k7c4	mimo
divadelní	divadelní	k2eAgNnSc4d1	divadelní
působení	působení	k1gNnSc4	působení
i	i	k9	i
literárně	literárně	k6eAd1	literárně
činná	činný	k2eAgFnSc1d1	činná
<g/>
,	,	kIx,	,
psala	psát	k5eAaImAgFnS	psát
lehké	lehký	k2eAgFnPc4d1	lehká
divadelní	divadelní	k2eAgFnPc4d1	divadelní
hry	hra	k1gFnPc4	hra
a	a	k8xC	a
romány	román	k1gInPc4	román
<g/>
,	,	kIx,	,
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
činnosti	činnost	k1gFnSc6	činnost
však	však	k9	však
nebyla	být	k5eNaImAgFnS	být
příliš	příliš	k6eAd1	příliš
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
,	,	kIx,	,
její	její	k3xOp3gNnSc4	její
nejúspěšnější	úspěšný	k2eAgNnSc4d3	nejúspěšnější
dílo	dílo	k1gNnSc4	dílo
jsou	být	k5eAaImIp3nP	být
její	její	k3xOp3gInPc1	její
memoáry	memoáry	k1gInPc1	memoáry
Můj	můj	k3xOp1gInSc4	můj
dvojí	dvojí	k4xRgInSc4	dvojí
život	život	k1gInSc4	život
(	(	kIx(	(
<g/>
Ma	Ma	k1gFnSc4	Ma
double	double	k2eAgNnPc2d1	double
vie	vie	k?	vie
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
<g/>
.	.	kIx.	.
</s>
<s>
Vyjma	vyjma	k7c2	vyjma
vlastní	vlastní	k2eAgFnSc2d1	vlastní
tvorby	tvorba	k1gFnSc2	tvorba
přeložila	přeložit	k5eAaPmAgFnS	přeložit
několik	několik	k4yIc4	několik
divadelních	divadelní	k2eAgFnPc2d1	divadelní
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
literatury	literatura	k1gFnPc4	literatura
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
věnovala	věnovat	k5eAaImAgFnS	věnovat
malbě	malba	k1gFnSc6	malba
a	a	k8xC	a
sochařství	sochařství	k1gNnSc6	sochařství
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1894	[number]	k4	1894
pověřila	pověřit	k5eAaPmAgFnS	pověřit
Sarah	Sarah	k1gFnSc1	Sarah
Bernhardtová	Bernhardtová	k1gFnSc1	Bernhardtová
Alfonse	Alfons	k1gMnSc4	Alfons
Muchu	Mucha	k1gMnSc4	Mucha
vytvořením	vytvoření	k1gNnSc7	vytvoření
návrhu	návrh	k1gInSc2	návrh
na	na	k7c4	na
plakát	plakát	k1gInSc4	plakát
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výsledkem	výsledek	k1gInSc7	výsledek
byla	být	k5eAaImAgFnS	být
spokojena	spokojen	k2eAgFnSc1d1	spokojena
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
pověřovala	pověřovat	k5eAaImAgFnS	pověřovat
Muchu	Mucha	k1gMnSc4	Mucha
dalšími	další	k2eAgFnPc7d1	další
zakázkami	zakázka	k1gFnPc7	zakázka
na	na	k7c4	na
plakáty	plakát	k1gInPc4	plakát
<g/>
,	,	kIx,	,
kulisy	kulisa	k1gFnPc4	kulisa
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1896	[number]	k4	1896
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
Mucha	Mucha	k1gMnSc1	Mucha
na	na	k7c6	na
plakátu	plakát	k1gInSc6	plakát
Sarah	Sarah	k1gFnSc2	Sarah
Bernhardtovou	Bernhardtová	k1gFnSc4	Bernhardtová
jako	jako	k8xS	jako
Dámu	dáma	k1gFnSc4	dáma
s	s	k7c7	s
kaméliemi	kamélie	k1gFnPc7	kamélie
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
vrcholů	vrchol	k1gInPc2	vrchol
secesní	secesní	k2eAgFnSc2d1	secesní
grafiky	grafika	k1gFnSc2	grafika
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
další	další	k2eAgInPc1d1	další
Muchovy	Muchův	k2eAgInPc1d1	Muchův
plakáty	plakát	k1gInPc1	plakát
ke	k	k7c3	k
hrám	hra	k1gFnPc3	hra
Lorenzaccio	Lorenzaccio	k6eAd1	Lorenzaccio
nebo	nebo	k8xC	nebo
Médea	Médea	k1gFnSc1	Médea
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
život	život	k1gInSc1	život
byl	být	k5eAaImAgInS	být
ztvárněn	ztvárnit	k5eAaPmNgInS	ztvárnit
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Neuvěřitelná	uvěřitelný	k2eNgFnSc1d1	neuvěřitelná
Sarah	Sarah	k1gFnSc1	Sarah
(	(	kIx(	(
<g/>
Incredible	Incredible	k1gFnSc1	Incredible
Sarah	Sarah	k1gFnSc1	Sarah
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
s	s	k7c7	s
Glendou	Glenda	k1gFnSc7	Glenda
Jacksonovou	Jacksonový	k2eAgFnSc7d1	Jacksonová
v	v	k7c6	v
titulní	titulní	k2eAgFnSc6d1	titulní
roli	role	k1gFnSc6	role
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
život	život	k1gInSc1	život
inspiroval	inspirovat	k5eAaBmAgInS	inspirovat
mnoho	mnoho	k4c4	mnoho
umělců	umělec	k1gMnPc2	umělec
a	a	k8xC	a
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
,	,	kIx,	,
nejznámější	známý	k2eAgMnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
Marcel	Marcel	k1gMnSc1	Marcel
Proust	Proust	k1gMnSc1	Proust
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
její	její	k3xOp3gFnSc1	její
postava	postava	k1gFnSc1	postava
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
k	k	k7c3	k
postavě	postava	k1gFnSc3	postava
herečky	herečka	k1gFnSc2	herečka
jménem	jméno	k1gNnSc7	jméno
La	la	k1gNnSc4	la
Berma	berma	k1gFnSc1	berma
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
díle	dílo	k1gNnSc6	dílo
Hledání	hledání	k1gNnSc4	hledání
ztraceného	ztracený	k2eAgInSc2d1	ztracený
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgNnSc1d1	populární
portrétování	portrétování	k1gNnSc1	portrétování
celebrit	celebrita	k1gFnPc2	celebrita
<g/>
,	,	kIx,	,
platil	platit	k5eAaImAgMnS	platit
americký	americký	k2eAgMnSc1d1	americký
fotograf	fotograf	k1gMnSc1	fotograf
Napoleon	Napoleon	k1gMnSc1	Napoleon
Sarony	Sarona	k1gFnSc2	Sarona
údajně	údajně	k6eAd1	údajně
Bernhardtové	Bernhardtové	k2eAgInPc2d1	Bernhardtové
1	[number]	k4	1
500	[number]	k4	500
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gNnSc3	on
pózovala	pózovat	k5eAaImAgFnS	pózovat
před	před	k7c7	před
fotoaparátem	fotoaparát	k1gInSc7	fotoaparát
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
svou	svůj	k3xOyFgFnSc7	svůj
hodnotou	hodnota	k1gFnSc7	hodnota
dnešním	dnešní	k2eAgInSc6d1	dnešní
20	[number]	k4	20
000	[number]	k4	000
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
