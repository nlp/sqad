<s>
Sarah	Sarah	k1gFnSc1	Sarah
Bernhardt	Bernhardta	k1gFnPc2	Bernhardta
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Henriette-Marie-Sarah	Henriette-Marie-Sarah	k1gInSc4	Henriette-Marie-Sarah
Bernardt	Bernardt	k2eAgMnSc1d1	Bernardt
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1844	[number]	k4	1844
Paříž	Paříž	k1gFnSc1	Paříž
-	-	kIx~	-
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1923	[number]	k4	1923
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
francouzská	francouzský	k2eAgFnSc1d1	francouzská
herečka	herečka	k1gFnSc1	herečka
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
divadelních	divadelní	k2eAgFnPc2d1	divadelní
osobností	osobnost	k1gFnPc2	osobnost
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
