<s>
Největšího	veliký	k2eAgInSc2d3	veliký
vzrůstu	vzrůst	k1gInSc2	vzrůst
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
přes	přes	k7c4	přes
čtyři	čtyři	k4xCgInPc4	čtyři
a	a	k8xC	a
půl	půl	k1xP	půl
stopy	stopa	k1gFnSc2	stopa
(	(	kIx(	(
<g/>
140	[number]	k4	140
cm	cm	kA	cm
<g/>
)	)	kIx)	)
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
hobiti	hobit	k1gMnPc1	hobit
Smělmír	Smělmíra	k1gFnPc2	Smělmíra
Brandorád	Brandoráda	k1gFnPc2	Brandoráda
a	a	k8xC	a
Peregrin	Peregrin	k1gInSc1	Peregrin
Bral	brát	k5eAaImAgInS	brát
díky	díky	k7c3	díky
kouzelným	kouzelný	k2eAgInPc3d1	kouzelný
nápojům	nápoj	k1gInPc3	nápoj
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgNnPc7	jenž
je	být	k5eAaImIp3nS	být
pohostil	pohostit	k5eAaPmAgInS	pohostit
ent	ent	k?	ent
Stromovous	Stromovous	k1gInSc1	Stromovous
<g/>
.	.	kIx.	.
</s>
