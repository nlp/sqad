<s>
Yamaha	yamaha	k1gFnSc1
XSR	XSR	kA
700	#num#	k4
</s>
<s>
Yamaha	yamaha	k1gFnSc1
XSR	XSR	kA
700	#num#	k4
Yamaha	yamaha	k1gFnSc1
XSR	XSR	kA
700	#num#	k4
v	v	k7c6
limitovaném	limitovaný	k2eAgNnSc6d1
barevném	barevný	k2eAgNnSc6d1
provedení	provedení	k1gNnSc6
60	#num#	k4
<g/>
th	th	k?
AnniversaryVýrobce	AnniversaryVýrobce	k1gMnSc1
</s>
<s>
MBK	MBK	kA
Industrie	industrie	k1gFnPc1
<g/>
,	,	kIx,
Rouvroy	Rouvroy	k1gInPc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
Mateřská	mateřský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
</s>
<s>
Yamaha	yamaha	k1gFnSc1
Výroba	výroba	k1gFnSc1
</s>
<s>
2016	#num#	k4
-	-	kIx~
dosud	dosud	k6eAd1
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Yamaha	yamaha	k1gFnSc1
XS	XS	kA
650	#num#	k4
Třída	třída	k1gFnSc1
</s>
<s>
naked	naked	k1gInSc1
bike	bikat	k5eAaPmIp3nS
Motor	motor	k1gInSc1
</s>
<s>
čtyřdobý	čtyřdobý	k2eAgMnSc1d1
<g/>
,	,	kIx,
kapalinou	kapalina	k1gFnSc7
chlazený	chlazený	k2eAgInSc1d1
osmiventilový	osmiventilový	k2eAgInSc1d1
řadový	řadový	k2eAgInSc1d1
dvouválec	dvouválec	k1gInSc1
DOHC	DOHC	kA
s	s	k7c7
obsahem	obsah	k1gInSc7
689	#num#	k4
cm³	cm³	k?
Vrtání	vrtání	k1gNnSc1
</s>
<s>
80,0	80,0	k4
mm	mm	kA
Zdvih	zdvih	k1gInSc1
</s>
<s>
68,6	68,6	k4
mm	mm	kA
Kompresní	kompresní	k2eAgInSc4d1
poměr	poměr	k1gInSc4
</s>
<s>
11,5	11,5	k4
:	:	kIx,
1	#num#	k4
Plnění	plnění	k1gNnPc2
</s>
<s>
Vstřikování	vstřikování	k1gNnSc1
1WS1	1WS1	k4
10	#num#	k4
Výkon	výkon	k1gInSc1
</s>
<s>
55,0	55,0	k4
kW	kW	kA
(	(	kIx(
<g/>
74,8	74,8	k4
HP	HP	kA
<g/>
)	)	kIx)
(	(	kIx(
<g/>
při	při	k7c6
9000	#num#	k4
ot	ot	k1gMnSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
min	mina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Krouticí	krouticí	k2eAgInSc1d1
moment	moment	k1gInSc1
</s>
<s>
68	#num#	k4
Nm	Nm	k1gFnSc1
(	(	kIx(
<g/>
při	při	k7c6
6500	#num#	k4
ot	ot	k1gMnSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
min	mina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Převodovka	převodovka	k1gFnSc1
</s>
<s>
šestistupňová	šestistupňový	k2eAgFnSc1d1
<g/>
,	,	kIx,
sekundární	sekundární	k2eAgInSc1d1
převod	převod	k1gInSc1
řetězem	řetěz	k1gInSc7
Brzdy	brzda	k1gFnSc2
</s>
<s>
přední	přední	k2eAgFnSc1d1
dvoukotoučová	dvoukotoučový	k2eAgFnSc1d1
(	(	kIx(
<g/>
čtyřpístkový	čtyřpístkový	k2eAgInSc4d1
třmen	třmen	k1gInSc4
<g/>
)	)	kIx)
<g/>
;	;	kIx,
zadní	zadní	k2eAgFnSc1d1
jednokotoučová	jednokotoučový	k2eAgFnSc1d1
(	(	kIx(
<g/>
dvoupístkový	dvoupístkový	k2eAgInSc1d1
třmen	třmen	k1gInSc1
<g/>
)	)	kIx)
Pneumatiky	pneumatika	k1gFnPc1
</s>
<s>
přední	přední	k2eAgMnSc1d1
120	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
ZR	ZR	kA
<g/>
17	#num#	k4
<g/>
;	;	kIx,
zadní	zadní	k2eAgFnSc2d1
180	#num#	k4
<g/>
/	/	kIx~
<g/>
55	#num#	k4
ZR17	ZR17	k1gFnSc2
Úhel	úhel	k1gInSc4
hlavy	hlava	k1gFnSc2
řízení	řízení	k1gNnSc2
</s>
<s>
25,0	25,0	k4
<g/>
°	°	k?
Závlek	závlek	k1gInSc4
předního	přední	k2eAgNnSc2d1
kola	kolo	k1gNnSc2
</s>
<s>
90	#num#	k4
mm	mm	kA
Rozvor	rozvora	k1gFnPc2
</s>
<s>
1405	#num#	k4
mm	mm	kA
Rozměry	rozměra	k1gFnSc2
</s>
<s>
2075	#num#	k4
mm	mm	kA
(	(	kIx(
<g/>
l	l	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
820	#num#	k4
mm	mm	kA
(	(	kIx(
<g/>
w	w	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1130	#num#	k4
mm	mm	kA
(	(	kIx(
<g/>
h	h	k?
<g/>
)	)	kIx)
Výška	výška	k1gFnSc1
sedadla	sedadlo	k1gNnSc2
</s>
<s>
815	#num#	k4
mm	mm	kA
Hmotnost	hmotnost	k1gFnSc4
</s>
<s>
186	#num#	k4
kg	kg	kA
(	(	kIx(
<g/>
pohotovostní	pohotovostní	k2eAgFnSc4d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
172	#num#	k4
kg	kg	kA
(	(	kIx(
<g/>
užitečná	užitečný	k2eAgFnSc1d1
<g/>
)	)	kIx)
Kapacita	kapacita	k1gFnSc1
nádrže	nádrž	k1gFnSc2
</s>
<s>
14	#num#	k4
l	l	kA
Související	související	k2eAgFnSc2d1
</s>
<s>
Yamaha	yamaha	k1gFnSc1
XSR	XSR	kA
900	#num#	k4
Podobné	podobný	k2eAgInPc1d1
</s>
<s>
Ducati	ducat	k5eAaImF
Scrambler	Scrambler	k1gInSc4
800	#num#	k4
</s>
<s>
Yamaha	yamaha	k1gFnSc1
XSR	XSR	kA
700	#num#	k4
je	být	k5eAaImIp3nS
motocykl	motocykl	k1gInSc1
firmy	firma	k1gFnSc2
Yamaha	Yamaha	kA
střední	střední	k2eAgFnSc2d1
váhy	váha	k1gFnSc2
<g/>
,	,	kIx,
kategorie	kategorie	k1gFnSc1
naked	naked	k1gMnSc1
bike	bike	k1gInSc1
<g/>
,	,	kIx,
vyráběný	vyráběný	k2eAgInSc1d1
od	od	k7c2
roku	rok	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Koncepce	koncepce	k1gFnSc1
</s>
<s>
V	v	k7c6
základním	základní	k2eAgInSc6d1
konceptu	koncept	k1gInSc6
jej	on	k3xPp3gNnSc4
navrhl	navrhnout	k5eAaPmAgMnS
japonský	japonský	k2eAgMnSc1d1
úpravce	úpravce	k1gMnSc1
motocyklů	motocykl	k1gInPc2
Shinya	Shinya	k1gMnSc1
Kimura	Kimur	k1gMnSc2
přestavbou	přestavba	k1gFnSc7
motocyklu	motocykl	k1gInSc2
Yamaha	yamaha	k1gFnSc1
MT-07	MT-07	k1gFnSc1
a	a	k8xC
dále	daleko	k6eAd2
byl	být	k5eAaImAgInS
vyvíjen	vyvíjet	k5eAaImNgInS
týmem	tým	k1gInSc7
designérů	designér	k1gMnPc2
Yamaha	Yamaha	kA
v	v	k7c6
italské	italský	k2eAgFnSc6d1
Monze	Monza	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Motocykl	motocykl	k1gInSc1
je	být	k5eAaImIp3nS
navržen	navrhnout	k5eAaPmNgInS
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
kombinoval	kombinovat	k5eAaImAgInS
klasický	klasický	k2eAgInSc1d1
design	design	k1gInSc1
<g/>
,	,	kIx,
vycházející	vycházející	k2eAgInSc1d1
z	z	k7c2
modelu	model	k1gInSc2
Yamaha	yamaha	k1gFnSc1
XS	XS	kA
650	#num#	k4
z	z	k7c2
roku	rok	k1gInSc2
1976	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
moderní	moderní	k2eAgFnSc1d1
technologie	technologie	k1gFnSc1
současného	současný	k2eAgInSc2d1
modelu	model	k1gInSc2
Yamaha	yamaha	k1gFnSc1
MT-	MT-	k1gFnSc1
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Motor	motor	k1gInSc1
<g/>
,	,	kIx,
podvozek	podvozek	k1gInSc1
a	a	k8xC
brzdový	brzdový	k2eAgInSc1d1
systém	systém	k1gInSc1
zůstaly	zůstat	k5eAaPmAgInP
s	s	k7c7
tímto	tento	k3xDgInSc7
typem	typ	k1gInSc7
shodné	shodný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výrazněji	výrazně	k6eAd2
se	se	k3xPyFc4
však	však	k9
změnila	změnit	k5eAaPmAgFnS
geometrie	geometrie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyšší	vysoký	k2eAgInSc4d2
a	a	k8xC
vzpřímenější	vzpřímený	k2eAgInSc4d2
posed	posed	k1gInSc4
<g/>
,	,	kIx,
stupačky	stupačka	k1gFnPc4
řidiče	řidič	k1gInSc2
zhruba	zhruba	k6eAd1
v	v	k7c6
úrovni	úroveň	k1gFnSc6
předního	přední	k2eAgInSc2d1
okraje	okraj	k1gInSc2
sedla	sedlo	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
širší	široký	k2eAgNnPc1d2
řídítka	řídítka	k1gNnPc1
přibližují	přibližovat	k5eAaImIp3nP
model	model	k1gInSc4
XSR	XSR	kA
700	#num#	k4
scramblerům	scrambler	k1gMnPc3
<g/>
;	;	kIx,
avšak	avšak	k8xC
měkký	měkký	k2eAgInSc1d1
podvozek	podvozek	k1gInSc1
a	a	k8xC
dodávané	dodávaný	k2eAgFnSc2d1
pneumatiky	pneumatika	k1gFnSc2
jej	on	k3xPp3gMnSc4
předurčují	předurčovat	k5eAaImIp3nP
spíše	spíše	k9
jen	jen	k9
pro	pro	k7c4
silniční	silniční	k2eAgNnSc4d1
využití	využití	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koncepčně	koncepčně	k6eAd1
srovnatelný	srovnatelný	k2eAgInSc1d1
je	být	k5eAaImIp3nS
motocykl	motocykl	k1gInSc1
Ducati	ducat	k5eAaImF
Scrambler	Scrambler	k1gInSc4
800	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Pohon	pohon	k1gInSc1
</s>
<s>
Čtyřdobý	čtyřdobý	k2eAgMnSc1d1
<g/>
,	,	kIx,
kapalinou	kapalina	k1gFnSc7
chlazený	chlazený	k2eAgInSc1d1
osmiventilový	osmiventilový	k2eAgInSc1d1
řadový	řadový	k2eAgInSc1d1
dvouválec	dvouválec	k1gInSc1
DOHC	DOHC	kA
s	s	k7c7
obsahem	obsah	k1gInSc7
689	#num#	k4
cm³	cm³	k?
má	mít	k5eAaImIp3nS
nominální	nominální	k2eAgInSc1d1
výkon	výkon	k1gInSc1
55	#num#	k4
kW	kW	kA
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
umožňuje	umožňovat	k5eAaImIp3nS
motocyklu	motocykl	k1gInSc2
dosáhnout	dosáhnout	k5eAaPmF
maximální	maximální	k2eAgInSc1d1
rychlosti	rychlost	k1gFnSc3
až	až	k9
200	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
Díky	díky	k7c3
přesazení	přesazení	k1gNnSc3
ojničních	ojniční	k2eAgInPc2d1
čepů	čep	k1gInPc2
o	o	k7c4
270	#num#	k4
<g/>
°	°	k?
(	(	kIx(
<g/>
Crossplane	Crossplan	k1gMnSc5
<g/>
)	)	kIx)
připomíná	připomínat	k5eAaImIp3nS
charakter	charakter	k1gInSc4
motoru	motor	k1gInSc2
spíše	spíše	k9
V-Twin	V-Twin	k2eAgInSc4d1
než	než	k8xS
klasický	klasický	k2eAgInSc4d1
řadový	řadový	k2eAgInSc4d1
motor	motor	k1gInSc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
projevuje	projevovat	k5eAaImIp3nS
i	i	k9
charakteristickým	charakteristický	k2eAgInSc7d1
zvukem	zvuk	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vibrace	vibrace	k1gFnSc1
<g/>
,	,	kIx,
způsobené	způsobený	k2eAgNnSc1d1
nepravidelným	pravidelný	k2eNgInSc7d1
pohybem	pohyb	k1gInSc7
pístů	píst	k1gInPc2
motoru	motor	k1gInSc2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
sníženy	snížit	k5eAaPmNgInP
vyvážením	vyvážení	k1gNnSc7
hřídele	hřídel	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Převodovka	převodovka	k1gFnSc1
je	být	k5eAaImIp3nS
šestistupňová	šestistupňový	k2eAgFnSc1d1
s	s	k7c7
trvalým	trvalý	k2eAgInSc7d1
záběrem	záběr	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spotřeba	spotřeba	k1gFnSc1
činí	činit	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
5,1	5,1	k4
l	l	kA
<g/>
/	/	kIx~
<g/>
100	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vozidla	vozidlo	k1gNnPc1
vyrobená	vyrobený	k2eAgNnPc1d1
v	v	k7c6
letech	léto	k1gNnPc6
2016	#num#	k4
-	-	kIx~
2020	#num#	k4
vyhovují	vyhovovat	k5eAaImIp3nP
emisní	emisní	k2eAgFnSc4d1
normě	norma	k1gFnSc6
Euro	euro	k1gNnSc1
4	#num#	k4
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
2021	#num#	k4
pak	pak	k6eAd1
normě	norma	k1gFnSc6
Euro	euro	k1gNnSc1
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Šasi	šasi	k1gNnSc1
</s>
<s>
Rám	rám	k1gInSc1
typu	typ	k1gInSc2
Diamond	Diamonda	k1gFnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
z	z	k7c2
ocelových	ocelový	k2eAgFnPc2d1
trubek	trubka	k1gFnPc2
je	být	k5eAaImIp3nS
dvoudílný	dvoudílný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zadní	zadní	k2eAgInSc1d1
díl	díl	k1gInSc1
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
MT-	MT-	k1gFnSc2
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
,	,	kIx,
přišroubován	přišroubován	k2eAgInSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
vyměněn	vyměnit	k5eAaPmNgInS
v	v	k7c6
rámci	rámec	k1gInSc6
individuálních	individuální	k2eAgFnPc2d1
úprav	úprava	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Podvozek	podvozek	k1gInSc1
tvoří	tvořit	k5eAaImIp3nS
klasická	klasický	k2eAgFnSc1d1
teleskopická	teleskopický	k2eAgFnSc1d1
přední	přední	k2eAgFnSc1d1
vidlice	vidlice	k1gFnSc1
s	s	k7c7
úhlem	úhel	k1gInSc7
hlavy	hlava	k1gFnSc2
řízení	řízení	k1gNnSc2
25,0	25,0	k4
<g/>
°	°	k?
a	a	k8xC
se	se	k3xPyFc4
zdvihem	zdvih	k1gInSc7
130	#num#	k4
mm	mm	kA
bez	bez	k7c2
možnosti	možnost	k1gFnSc2
nastavení	nastavení	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
zadní	zadní	k2eAgFnSc1d1
kyvná	kyvný	k2eAgFnSc1d1
vidlice	vidlice	k1gFnSc1
s	s	k7c7
centrálním	centrální	k2eAgInSc7d1
tlumičem	tlumič	k1gInSc7
(	(	kIx(
<g/>
Monocross	Monocross	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
s	s	k7c7
nastavitelným	nastavitelný	k2eAgNnSc7d1
předpětím	předpětí	k1gNnSc7
pružiny	pružina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdvih	zdvih	k1gInSc1
na	na	k7c6
zadním	zadní	k2eAgNnSc6d1
kole	kolo	k1gNnSc6
je	být	k5eAaImIp3nS
rovněž	rovněž	k9
130	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tlumič	tlumič	k1gInSc1
je	být	k5eAaImIp3nS
umístěn	umístit	k5eAaPmNgInS
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
těžiště	těžiště	k1gNnSc2
vozidla	vozidlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Litá	litý	k2eAgFnSc1d1
hliníková	hliníkový	k2eAgFnSc1d1
10	#num#	k4
<g/>
-paprsková	-paprskový	k2eAgFnSc1d1
kola	kola	k1gFnSc1
o	o	k7c6
průměru	průměr	k1gInSc6
17	#num#	k4
<g/>
"	"	kIx"
jsou	být	k5eAaImIp3nP
opatřena	opatřit	k5eAaPmNgFnS
bezdušovými	bezdušový	k2eAgFnPc7d1
pneumatikami	pneumatika	k1gFnPc7
Pirelli	Pirell	k1gMnSc3
Phantom	Phantom	k1gInSc1
o	o	k7c6
rozměrech	rozměr	k1gInPc6
120	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
vpředu	vpředu	k6eAd1
a	a	k8xC
180	#num#	k4
<g/>
/	/	kIx~
<g/>
55	#num#	k4
vzadu	vzadu	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Přední	přední	k2eAgFnSc1d1
brzda	brzda	k1gFnSc1
sestává	sestávat	k5eAaImIp3nS
ze	z	k7c2
dvou	dva	k4xCgInPc2
kotoučů	kotouč	k1gInPc2
o	o	k7c6
průměru	průměr	k1gInSc6
282	#num#	k4
mm	mm	kA
se	s	k7c7
čtyřpístkovými	čtyřpístkův	k2eAgInPc7d1
třmeny	třmen	k1gInPc7
<g/>
,	,	kIx,
zadní	zadní	k2eAgFnPc1d1
brzda	brzda	k1gFnSc1
z	z	k7c2
jednoho	jeden	k4xCgInSc2
kotouče	kotouč	k1gInSc2
o	o	k7c6
průměru	průměr	k1gInSc6
245	#num#	k4
mm	mm	kA
s	s	k7c7
dvoupístkovým	dvoupístkův	k2eAgInSc7d1
třmenem	třmen	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
legislativních	legislativní	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
jsou	být	k5eAaImIp3nP
brzdy	brzda	k1gFnPc1
vybaveny	vybavit	k5eAaPmNgFnP
standardním	standardní	k2eAgInSc6d1
systémem	systém	k1gInSc7
ABS	ABS	kA
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
však	však	k9
nelze	lze	k6eNd1
vypnout	vypnout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgNnPc4d1
specifika	specifikon	k1gNnPc4
</s>
<s>
Řídítka	řídítka	k1gNnPc1
o	o	k7c6
šířce	šířka	k1gFnSc6
760	#num#	k4
mm	mm	kA
jsou	být	k5eAaImIp3nP
mírně	mírně	k6eAd1
sklopená	sklopený	k2eAgFnSc1d1
dozadu	dozadu	k6eAd1
a	a	k8xC
umožňují	umožňovat	k5eAaImIp3nP
relativně	relativně	k6eAd1
vzpřímený	vzpřímený	k2eAgInSc4d1
posed	posed	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
vyšší	vysoký	k2eAgInPc1d2
<g/>
,	,	kIx,
než	než	k8xS
je	být	k5eAaImIp3nS
obvyklé	obvyklý	k2eAgNnSc1d1
u	u	k7c2
naked	naked	k1gInSc4
biků	bik	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
nižší	nízký	k2eAgFnSc1d2
než	než	k8xS
u	u	k7c2
cestovních	cestovní	k2eAgFnPc2d1
endur	endura	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Boční	boční	k2eAgFnPc1d1
kryty	kryt	k2eAgFnPc1d1
palivové	palivový	k2eAgFnPc1d1
nádrže	nádrž	k1gFnPc1
jsou	být	k5eAaImIp3nP
z	z	k7c2
hliníku	hliník	k1gInSc2
<g/>
,	,	kIx,
ručně	ručně	k6eAd1
tepané	tepaný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přední	přední	k2eAgInSc1d1
světlomet	světlomet	k1gInSc1
je	být	k5eAaImIp3nS
kulatý	kulatý	k2eAgInSc1d1
<g/>
,	,	kIx,
velmi	velmi	k6eAd1
výrazný	výrazný	k2eAgInSc1d1
<g/>
,	,	kIx,
opatřený	opatřený	k2eAgInSc1d1
halogenovou	halogenový	k2eAgFnSc7d1
žárovkou	žárovka	k1gFnSc7
a	a	k8xC
skleněnou	skleněný	k2eAgFnSc7d1
parabolou	parabola	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zadní	zadní	k2eAgFnPc1d1
koncové	koncový	k2eAgFnPc1d1
a	a	k8xC
brzdové	brzdový	k2eAgNnSc1d1
světlo	světlo	k1gNnSc1
<g/>
,	,	kIx,
rovněž	rovněž	k9
v	v	k7c6
retro	retro	k1gNnSc6
designu	design	k1gInSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
však	však	k9
tvořené	tvořený	k2eAgNnSc1d1
LED	led	k1gInSc4
diodami	dioda	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Motocykl	motocykl	k1gInSc1
je	být	k5eAaImIp3nS
vybaven	vybavit	k5eAaPmNgInS
multifunkční	multifunkční	k2eAgFnSc7d1
palubní	palubní	k2eAgFnSc7d1
deskou	deska	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
podobu	podoba	k1gFnSc4
klasického	klasický	k2eAgInSc2d1
kulatého	kulatý	k2eAgInSc2d1
"	"	kIx"
<g/>
budíku	budík	k1gInSc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
však	však	k9
plně	plně	k6eAd1
digitální	digitální	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahuje	obsahovat	k5eAaImIp3nS
rychloměr	rychloměr	k1gInSc1
<g/>
,	,	kIx,
otáčkoměr	otáčkoměr	k1gInSc1
<g/>
,	,	kIx,
palivoměr	palivoměr	k1gInSc1
<g/>
,	,	kIx,
ukazatel	ukazatel	k1gInSc1
zařazené	zařazený	k2eAgFnSc2d1
rychlosti	rychlost	k1gFnSc2
a	a	k8xC
univerzální	univerzální	k2eAgInSc4d1
displej	displej	k1gInSc4
<g/>
,	,	kIx,
zobrazující	zobrazující	k2eAgInSc4d1
(	(	kIx(
<g/>
volitelně	volitelně	k6eAd1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
počítadla	počítadlo	k1gNnSc2
kilometrů	kilometr	k1gInPc2
(	(	kIx(
<g/>
celkové	celkový	k2eAgNnSc4d1
<g/>
,	,	kIx,
2	#num#	k4
<g/>
×	×	k?
denní	denní	k2eAgInSc1d1
a	a	k8xC
nájezd	nájezd	k1gInSc1
kilometrů	kilometr	k1gInPc2
na	na	k7c4
palivovou	palivový	k2eAgFnSc4d1
rezervu	rezerva	k1gFnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
aktuální	aktuální	k2eAgFnSc4d1
spotřebu	spotřeba	k1gFnSc4
paliva	palivo	k1gNnSc2
<g/>
,	,	kIx,
průměrnou	průměrný	k2eAgFnSc4d1
spotřebu	spotřeba	k1gFnSc4
paliva	palivo	k1gNnSc2
<g/>
,	,	kIx,
teplotu	teplota	k1gFnSc4
chladicí	chladicí	k2eAgFnSc2d1
kapaliny	kapalina	k1gFnSc2
<g/>
,	,	kIx,
okolní	okolní	k2eAgFnSc4d1
teplotu	teplota	k1gFnSc4
nebo	nebo	k8xC
hodiny	hodina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Díky	díky	k7c3
své	svůj	k3xOyFgFnSc3
nízké	nízký	k2eAgFnSc3d1
hmotnosti	hmotnost	k1gFnSc3
<g/>
,	,	kIx,
malému	malý	k2eAgInSc3d1
rozvoru	rozvor	k1gInSc3
a	a	k8xC
vyššímu	vysoký	k2eAgInSc3d2
posedu	posed	k1gInSc3
je	být	k5eAaImIp3nS
motocykl	motocykl	k1gInSc1
velmi	velmi	k6eAd1
snadno	snadno	k6eAd1
ovladatelný	ovladatelný	k2eAgInSc1d1
v	v	k7c6
nízkých	nízký	k2eAgFnPc6d1
rychlostech	rychlost	k1gFnPc6
<g/>
,	,	kIx,
takže	takže	k8xS
je	být	k5eAaImIp3nS
vhodný	vhodný	k2eAgMnSc1d1
do	do	k7c2
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hodí	hodit	k5eAaImIp3nS,k5eAaPmIp3nS
se	se	k3xPyFc4
rovněž	rovněž	k9
pro	pro	k7c4
začínající	začínající	k2eAgMnPc4d1
jezdce	jezdec	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
motocyklu	motocykl	k1gInSc3
je	být	k5eAaImIp3nS
výrobcem	výrobce	k1gMnSc7
nabízena	nabízen	k2eAgFnSc1d1
kolekce	kolekce	k1gFnSc1
čtyřiceti	čtyřicet	k4xCc2
originálních	originální	k2eAgFnPc2d1
komponent	komponenta	k1gFnPc2
a	a	k8xC
doplňků	doplněk	k1gInPc2
pro	pro	k7c4
individuální	individuální	k2eAgFnPc4d1
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Yamaha	yamaha	k1gFnSc1
XSR	XSR	kA
700	#num#	k4
na	na	k7c6
německé	německý	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Yamaha	yamaha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
XSR700	XSR700	k1gMnSc1
Owner	Owner	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Manual	Manual	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Nařízení	nařízení	k1gNnSc1
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
a	a	k8xC
Rady	rada	k1gFnSc2
(	(	kIx(
<g/>
EU	EU	kA
<g/>
)	)	kIx)
č.	č.	k?
168	#num#	k4
<g/>
/	/	kIx~
<g/>
2013	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Motocykl	motocykl	k1gInSc1
online	onlinout	k5eAaPmIp3nS
<g/>
:	:	kIx,
Test	test	k1gInSc1
</s>
<s>
MotoRoute	MotoRout	k1gMnSc5
<g/>
:	:	kIx,
Recenze	recenze	k1gFnSc1
</s>
<s>
Silniční	silniční	k2eAgFnPc1d1
motorky	motorka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
Test	test	k1gInSc1
</s>
<s>
Motorkáři	motorkář	k1gMnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
Redakční	redakční	k2eAgInSc1d1
test	test	k1gInSc1
</s>
