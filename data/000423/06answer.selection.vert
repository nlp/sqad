<s>
Byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1892	[number]	k4	1892
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
jako	jako	k8xC	jako
cyklistický	cyklistický	k2eAgInSc1d1	cyklistický
odbor	odbor	k1gInSc1	odbor
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
vzešlý	vzešlý	k2eAgInSc1d1	vzešlý
z	z	k7c2	z
pražského	pražský	k2eAgNnSc2d1	Pražské
vlasteneckého	vlastenecký	k2eAgNnSc2d1	vlastenecké
studentského	studentský	k2eAgNnSc2d1	studentské
sdružení	sdružení	k1gNnSc2	sdružení
Literární	literární	k2eAgMnSc1d1	literární
a	a	k8xC	a
řečnický	řečnický	k2eAgInSc1d1	řečnický
spolek	spolek	k1gInSc1	spolek
Slavia	Slavia	k1gFnSc1	Slavia
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
ve	v	k7c6	v
Vodičkově	Vodičkův	k2eAgFnSc6d1	Vodičkova
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
