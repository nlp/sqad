<s>
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Ludwig	Ludwig	k1gMnSc1	Ludwig
Abeken	Abeken	k2eAgMnSc1d1	Abeken
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1813	[number]	k4	1813
<g/>
,	,	kIx,	,
Osnabrück	Osnabrück	k1gInSc1	Osnabrück
–	–	k?	–
29	[number]	k4	29
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1843	[number]	k4	1843
<g/>
,	,	kIx,	,
Mnichov	Mnichov	k1gInSc1	Mnichov
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
klasický	klasický	k2eAgMnSc1d1	klasický
archeolog	archeolog	k1gMnSc1	archeolog
<g/>
.	.	kIx.	.
</s>
<s>
Abeken	Abeken	k1gInSc4	Abeken
byl	být	k5eAaImAgMnS	být
syn	syn	k1gMnSc1	syn
Bernharda	Bernhard	k1gMnSc2	Bernhard
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Abekena	Abeken	k1gMnSc2	Abeken
a	a	k8xC	a
bratr	bratr	k1gMnSc1	bratr
politického	politický	k2eAgMnSc2d1	politický
spisovatele	spisovatel	k1gMnSc2	spisovatel
Hermanna	Hermann	k1gMnSc2	Hermann
Abekena	Abeken	k1gMnSc2	Abeken
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1833	[number]	k4	1833
teologii	teologie	k1gFnSc4	teologie
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
,	,	kIx,	,
přešel	přejít	k5eAaPmAgInS	přejít
však	však	k9	však
k	k	k7c3	k
archeologii	archeologie	k1gFnSc3	archeologie
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Eduarda	Eduard	k1gMnSc2	Eduard
Gerharda	Gerhard	k1gMnSc2	Gerhard
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1836	[number]	k4	1836
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
studiu	studio	k1gNnSc3	studio
starého	starý	k2eAgNnSc2d1	staré
osídlení	osídlení	k1gNnSc2	osídlení
Itálie	Itálie	k1gFnSc1	Itálie
(	(	kIx(	(
<g/>
Etruskové	Etrusk	k1gMnPc1	Etrusk
<g/>
,	,	kIx,	,
Umbrijci	Umbrijce	k1gMnPc1	Umbrijce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
byly	být	k5eAaImAgInP	být
publikovány	publikován	k2eAgInPc1d1	publikován
v	v	k7c6	v
posmrtně	posmrtně	k6eAd1	posmrtně
vydaném	vydaný	k2eAgNnSc6d1	vydané
díle	dílo	k1gNnSc6	dílo
Mittelitalien	Mittelitalien	k2eAgInSc4d1	Mittelitalien
vor	vor	k1gInSc4	vor
den	den	k1gInSc4	den
Zeiten	Zeitno	k1gNnPc2	Zeitno
römischer	römischra	k1gFnPc2	römischra
Herrschaft	Herrschaft	k2eAgInSc1d1	Herrschaft
nach	nach	k1gInSc1	nach
seinen	seinen	k2eAgInSc1d1	seinen
Denkmalen	Denkmalen	k2eAgInSc1d1	Denkmalen
dargestellt	dargestellt	k1gInSc1	dargestellt
(	(	kIx(	(
<g/>
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
1843	[number]	k4	1843
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
De	De	k?	De
μ	μ	k?	μ
apud	apuda	k1gFnPc2	apuda
Platonem	Plato	k1gMnSc7	Plato
et	et	k?	et
Aristotelem	Aristoteles	k1gMnSc7	Aristoteles
notione	notion	k1gInSc5	notion
dissertatio	dissertatio	k1gMnSc1	dissertatio
<g/>
.	.	kIx.	.
</s>
<s>
Scripsit	Scripsit	k1gInSc1	Scripsit
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Guilelmus	Guilelmus	k1gInSc1	Guilelmus
Abeken	Abeken	k2eAgInSc1d1	Abeken
<g/>
.	.	kIx.	.
</s>
<s>
Prostat	prostata	k1gFnPc2	prostata
apud	apud	k1gMnSc1	apud
Dieterich	Dieterich	k1gMnSc1	Dieterich
<g/>
,	,	kIx,	,
Gottingae	Gottingae	k1gFnSc1	Gottingae
1836	[number]	k4	1836
<g/>
.	.	kIx.	.
pdf	pdf	k?	pdf
na	na	k7c4	na
google	google	k6eAd1	google
Mittelitalien	Mittelitalien	k2eAgInSc4d1	Mittelitalien
vor	vor	k1gInSc4	vor
den	den	k1gInSc4	den
Zeiten	Zeitno	k1gNnPc2	Zeitno
römischer	römischra	k1gFnPc2	römischra
Herrschaft	Herrschafta	k1gFnPc2	Herrschafta
<g/>
.	.	kIx.	.
</s>
<s>
Nach	nach	k1gInSc1	nach
seinen	seinen	k1gInSc1	seinen
Denkmalen	Denkmalen	k2eAgInSc1d1	Denkmalen
<g/>
.	.	kIx.	.
</s>
<s>
Cotta	Cotta	k1gFnSc1	Cotta
<g/>
,	,	kIx,	,
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
und	und	k?	und
Tübingen	Tübingen	k1gInSc1	Tübingen
1843	[number]	k4	1843
<g/>
.	.	kIx.	.
pdf	pdf	k?	pdf
na	na	k7c6	na
google	googl	k1gInSc6	googl
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Wilhelm	Wilhelma	k1gFnPc2	Wilhelma
Abeken	Abekno	k1gNnPc2	Abekno
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Karl	Karl	k1gMnSc1	Karl
Felix	Felix	k1gMnSc1	Felix
Halm	halma	k1gFnPc2	halma
<g/>
:	:	kIx,	:
Abeken	Abeken	k1gInSc1	Abeken
<g/>
,	,	kIx,	,
Bernhard	Bernhard	k1gMnSc1	Bernhard
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
.	.	kIx.	.
v	v	k7c6	v
<g/>
:	:	kIx,	:
Allgemeine	Allgemein	k1gMnSc5	Allgemein
Deutsche	Deutschus	k1gMnSc5	Deutschus
Biographie	Biographius	k1gMnSc5	Biographius
(	(	kIx(	(
<g/>
ADB	ADB	kA	ADB
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
svazek	svazek	k1gInSc1	svazek
1	[number]	k4	1
<g/>
,	,	kIx,	,
Duncker	Duncker	k1gInSc1	Duncker
&	&	k?	&
Humblot	Humblot	k1gInSc1	Humblot
<g/>
,	,	kIx,	,
Lipsko	Lipsko	k1gNnSc1	Lipsko
1875	[number]	k4	1875
<g/>
,	,	kIx,	,
S.	S.	kA	S.
8	[number]	k4	8
f.	f.	k?	f.
Darin	Darina	k1gFnPc2	Darina
Kurzbeitrag	Kurzbeitrag	k1gMnSc1	Kurzbeitrag
zu	zu	k?	zu
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Ludwig	Ludwig	k1gMnSc1	Ludwig
Abeken	Abekno	k1gNnPc2	Abekno
<g/>
.	.	kIx.	.
</s>
