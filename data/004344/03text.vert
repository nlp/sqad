<s>
Světáci	Světák	k1gMnPc1	Světák
je	on	k3xPp3gNnSc4	on
český	český	k2eAgInSc1d1	český
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Podskalského	podskalský	k2eAgMnSc2d1	podskalský
natočený	natočený	k2eAgInSc4d1	natočený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
situační	situační	k2eAgFnSc4d1	situační
komedii	komedie	k1gFnSc4	komedie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
mj.	mj.	kA	mj.
objevují	objevovat	k5eAaImIp3nP	objevovat
ostré	ostrý	k2eAgFnPc4d1	ostrá
narážky	narážka	k1gFnPc4	narážka
na	na	k7c4	na
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
politickou	politický	k2eAgFnSc4d1	politická
scénu	scéna	k1gFnSc4	scéna
<g/>
.	.	kIx.	.
</s>
<s>
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Podskalský	podskalský	k2eAgInSc4d1	podskalský
námět	námět	k1gInSc4	námět
a	a	k8xC	a
scénář	scénář	k1gInSc4	scénář
<g/>
:	:	kIx,	:
Vratislav	Vratislav	k1gMnSc1	Vratislav
Blažek	Blažek	k1gMnSc1	Blažek
kamera	kamera	k1gFnSc1	kamera
<g/>
:	:	kIx,	:
František	František	k1gMnSc1	František
Valert	Valert	k1gMnSc1	Valert
hudba	hudba	k1gFnSc1	hudba
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
Evžen	Evžen	k1gMnSc1	Evžen
Illín	Illín	k1gMnSc1	Illín
<g/>
,	,	kIx,	,
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Hála	Hála	k1gMnSc1	Hála
kostýmy	kostým	k1gInPc7	kostým
<g/>
:	:	kIx,	:
Irena	Irena	k1gFnSc1	Irena
Greifová	Greifový	k2eAgFnSc1d1	Greifová
střih	střih	k1gInSc1	střih
<g/>
:	:	kIx,	:
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Stehlík	Stehlík	k1gMnSc1	Stehlík
zvuk	zvuk	k1gInSc1	zvuk
<g/>
:	:	kIx,	:
Josef	Josef	k1gMnSc1	Josef
Vlček	Vlček	k1gMnSc1	Vlček
vedoucí	vedoucí	k1gMnSc1	vedoucí
produkce	produkce	k1gFnSc2	produkce
<g/>
:	:	kIx,	:
Josef	Josef	k1gMnSc1	Josef
Ouzký	Ouzký	k2eAgMnSc1d1	Ouzký
výtvarník	výtvarník	k1gMnSc1	výtvarník
<g/>
:	:	kIx,	:
Boris	Boris	k1gMnSc1	Boris
Moravec	Moravec	k1gMnSc1	Moravec
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
Pavlík	Pavlík	k1gMnSc1	Pavlík
zpěv	zpěv	k1gInSc1	zpěv
<g/>
:	:	kIx,	:
Waldemar	Waldemar	k1gMnSc1	Waldemar
Matuška	Matuška	k1gMnSc1	Matuška
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
vtipný	vtipný	k2eAgInSc4d1	vtipný
a	a	k8xC	a
rozverný	rozverný	k2eAgInSc4d1	rozverný
příběh	příběh	k1gInSc4	příběh
tří	tři	k4xCgMnPc2	tři
venkovských	venkovský	k2eAgMnPc2d1	venkovský
zedníků-fasádníků	zedníkůasádník	k1gMnPc2	zedníků-fasádník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
jednou	jeden	k4xCgFnSc7	jeden
dostanou	dostat	k5eAaPmIp3nP	dostat
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
prací	práce	k1gFnSc7	práce
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
tři	tři	k4xCgMnPc1	tři
dělníci	dělník	k1gMnPc1	dělník
<g/>
,	,	kIx,	,
chlapi	chlap	k1gMnPc1	chlap
od	od	k7c2	od
stavebního	stavební	k2eAgNnSc2d1	stavební
řemesla	řemeslo	k1gNnSc2	řemeslo
si	se	k3xPyFc3	se
usmyslí	usmyslet	k5eAaPmIp3nS	usmyslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
užijí	užít	k5eAaPmIp3nP	užít
radovánek	radovánka	k1gFnPc2	radovánka
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
jim	on	k3xPp3gMnPc3	on
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
československé	československý	k2eAgNnSc4d1	Československé
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
alespoň	alespoň	k9	alespoň
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
noc	noc	k1gFnSc4	noc
budou	být	k5eAaImBp3nP	být
vystupovat	vystupovat	k5eAaImF	vystupovat
jako	jako	k9	jako
skuteční	skutečný	k2eAgMnPc1d1	skutečný
životem	život	k1gInSc7	život
protřelí	protřelý	k2eAgMnPc1d1	protřelý
světáci	světák	k1gMnPc1	světák
<g/>
.	.	kIx.	.
</s>
<s>
Vyrazí	vyrazit	k5eAaPmIp3nS	vyrazit
proto	proto	k8xC	proto
do	do	k7c2	do
"	"	kIx"	"
<g/>
lepší	dobrý	k2eAgFnSc2d2	lepší
<g/>
"	"	kIx"	"
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
seznámí	seznámit	k5eAaPmIp3nP	seznámit
se	se	k3xPyFc4	se
s	s	k7c7	s
jim	on	k3xPp3gFnPc3	on
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
nepodobnou	podobný	k2eNgFnSc7d1	nepodobná
trojicí	trojice	k1gFnSc7	trojice
lehkých	lehký	k2eAgFnPc2d1	lehká
dámiček	dámička	k1gFnPc2	dámička
(	(	kIx(	(
<g/>
které	který	k3yRgFnPc1	který
ve	v	k7c6	v
filmu	film	k1gInSc6	film
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
trojice	trojice	k1gFnSc1	trojice
kamarádek	kamarádka	k1gFnPc2	kamarádka
a	a	k8xC	a
kolegyň	kolegyně	k1gFnPc2	kolegyně
z	z	k7c2	z
Vinohradského	vinohradský	k2eAgNnSc2d1	Vinohradské
divadla	divadlo	k1gNnSc2	divadlo
Jiřina	Jiřina	k1gFnSc1	Jiřina
Bohdalová-Jiřina	Bohdalová-Jiřina	k1gFnSc1	Bohdalová-Jiřina
Jirásková-Iva	Jirásková-Iva	k1gFnSc1	Jirásková-Iva
Janžurová	Janžurový	k2eAgFnSc1d1	Janžurová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
trojici	trojice	k1gFnSc4	trojice
dam	dáma	k1gFnPc2	dáma
pak	pak	k6eAd1	pak
doplnila	doplnit	k5eAaPmAgFnS	doplnit
jejich	jejich	k3xOp3gFnSc1	jejich
starší	starý	k2eAgFnSc1d2	starší
kuplířka	kuplířka	k1gFnSc1	kuplířka
Trčková	Trčková	k1gFnSc1	Trčková
(	(	kIx(	(
<g/>
Jiřina	Jiřina	k1gFnSc1	Jiřina
Šejbalová	Šejbalová	k1gFnSc1	Šejbalová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Brodský	Brodský	k1gMnSc1	Brodský
(	(	kIx(	(
<g/>
Gustáv	Gustáva	k1gFnPc2	Gustáva
Prouza	Prouza	k1gFnSc1	Prouza
<g/>
)	)	kIx)	)
Jiří	Jiří	k1gMnSc1	Jiří
Sovák	Sovák	k1gMnSc1	Sovák
(	(	kIx(	(
<g/>
Antonín	Antonín	k1gMnSc1	Antonín
Skopec	Skopec	k1gMnSc1	Skopec
<g/>
)	)	kIx)	)
Jan	Jan	k1gMnSc1	Jan
Libíček	Libíček	k1gMnSc1	Libíček
(	(	kIx(	(
<g/>
Petrtýl	Petrtýl	k1gMnSc1	Petrtýl
<g/>
)	)	kIx)	)
Jiřina	Jiřina	k1gFnSc1	Jiřina
Bohdalová	Bohdalová	k1gFnSc1	Bohdalová
(	(	kIx(	(
<g/>
Božka	Božka	k1gFnSc1	Božka
<g/>
)	)	kIx)	)
Jiřina	Jiřina	k1gFnSc1	Jiřina
Jirásková	Jirásková	k1gFnSc1	Jirásková
(	(	kIx(	(
<g/>
Marcela	Marcela	k1gFnSc1	Marcela
<g/>
)	)	kIx)	)
Iva	Iva	k1gFnSc1	Iva
Janžurová	Janžurový	k2eAgFnSc1d1	Janžurová
(	(	kIx(	(
<g/>
Zuzana	Zuzana	k1gFnSc1	Zuzana
<g/>
)	)	kIx)	)
Oldřich	Oldřich	k1gMnSc1	Oldřich
Nový	Nový	k1gMnSc1	Nový
(	(	kIx(	(
<g/>
Dvorský	Dvorský	k1gMnSc1	Dvorský
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Jiřina	Jiřina	k1gFnSc1	Jiřina
Šejbalová	Šejbalová	k1gFnSc1	Šejbalová
(	(	kIx(	(
<g/>
Trčková	Trčková	k1gFnSc1	Trčková
<g/>
)	)	kIx)	)
Ilja	Ilja	k1gMnSc1	Ilja
Prachař	Prachař	k1gMnSc1	Prachař
(	(	kIx(	(
<g/>
kapitán	kapitán	k1gMnSc1	kapitán
Veřejné	veřejný	k2eAgFnSc2d1	veřejná
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
)	)	kIx)	)
Vladimír	Vladimír	k1gMnSc1	Vladimír
Menšík	Menšík	k1gMnSc1	Menšík
(	(	kIx(	(
<g/>
Novák	Novák	k1gMnSc1	Novák
<g/>
)	)	kIx)	)
Otto	Otto	k1gMnSc1	Otto
Šimánek	Šimánek	k1gMnSc1	Šimánek
(	(	kIx(	(
<g/>
muž	muž	k1gMnSc1	muž
<g/>
)	)	kIx)	)
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Moučka	Moučka	k1gMnSc1	Moučka
(	(	kIx(	(
<g/>
Hovorka	Hovorka	k1gMnSc1	Hovorka
<g/>
)	)	kIx)	)
Jiří	Jiří	k1gMnSc1	Jiří
Lír	lír	k1gInSc1	lír
(	(	kIx(	(
<g/>
prodavač	prodavač	k1gMnSc1	prodavač
u	u	k7c2	u
Adama	Adam	k1gMnSc2	Adam
<g/>
)	)	kIx)	)
Viktor	Viktor	k1gMnSc1	Viktor
Maurer	Maurer	k1gMnSc1	Maurer
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
vrchní	vrchní	k2eAgMnSc1d1	vrchní
<g/>
)	)	kIx)	)
Ladislav	Ladislav	k1gMnSc1	Ladislav
Křiváček	křiváček	k1gInSc1	křiváček
(	(	kIx(	(
<g/>
praporčík	praporčík	k1gMnSc1	praporčík
Veřejné	veřejný	k2eAgFnSc2d1	veřejná
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
)	)	kIx)	)
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Štercl	Štercl	k1gMnSc1	Štercl
(	(	kIx(	(
<g/>
kabaretní	kabaretní	k2eAgMnSc1d1	kabaretní
komik	komik	k1gMnSc1	komik
<g/>
)	)	kIx)	)
Settleři	Settler	k1gMnPc1	Settler
(	(	kIx(	(
<g/>
sami	sám	k3xTgMnPc1	sám
sebe	sebe	k3xPyFc4	sebe
<g/>
)	)	kIx)	)
Darja	Darja	k1gFnSc1	Darja
Hajská	Hajská	k1gFnSc1	Hajská
(	(	kIx(	(
<g/>
Bártová	Bártová	k1gFnSc1	Bártová
<g/>
)	)	kIx)	)
Josef	Josef	k1gMnSc1	Josef
Bek	bek	k1gMnSc1	bek
(	(	kIx(	(
<g/>
elegán	elegán	k1gMnSc1	elegán
ve	v	k7c6	v
slamáku	slamák	k1gInSc6	slamák
<g/>
)	)	kIx)	)
Karel	Karel	k1gMnSc1	Karel
Hála	Hála	k1gMnSc1	Hála
(	(	kIx(	(
<g/>
pianista	pianista	k1gMnSc1	pianista
<g/>
)	)	kIx)	)
Karel	Karel	k1gMnSc1	Karel
Hovorka	Hovorka	k1gMnSc1	Hovorka
(	(	kIx(	(
<g/>
stavební	stavební	k2eAgMnSc1d1	stavební
mistr	mistr	k1gMnSc1	mistr
<g/>
)	)	kIx)	)
Helena	Helena	k1gFnSc1	Helena
Růžičková	Růžičková	k1gFnSc1	Růžičková
(	(	kIx(	(
<g/>
návštěvnice	návštěvnice	k1gFnSc1	návštěvnice
v	v	k7c6	v
recepci	recepce	k1gFnSc6	recepce
<g/>
)	)	kIx)	)
Divadelní	divadelní	k2eAgFnSc1d1	divadelní
společnost	společnost	k1gFnSc1	společnost
Háta	Háta	k1gFnSc1	Háta
Olgy	Olga	k1gFnSc2	Olga
Želenské	Želenský	k2eAgFnSc2d1	Želenská
v	v	k7c6	v
Branickém	branický	k2eAgNnSc6d1	Branické
divadle	divadlo	k1gNnSc6	divadlo
uvedlo	uvést	k5eAaPmAgNnS	uvést
divadelní	divadelní	k2eAgFnSc4d1	divadelní
adaptaci	adaptace	k1gFnSc4	adaptace
této	tento	k3xDgFnSc2	tento
hudební	hudební	k2eAgFnSc2d1	hudební
komedie	komedie	k1gFnSc2	komedie
<g/>
.	.	kIx.	.
</s>
<s>
Režisérem	režisér	k1gMnSc7	režisér
nové	nový	k2eAgFnSc2d1	nová
divadelní	divadelní	k2eAgFnSc2d1	divadelní
úpravy	úprava	k1gFnSc2	úprava
je	být	k5eAaImIp3nS	být
Lumír	Lumír	k1gMnSc1	Lumír
Olšovský	Olšovský	k1gMnSc1	Olšovský
<g/>
.	.	kIx.	.
</s>
<s>
Účinkují	účinkovat	k5eAaImIp3nP	účinkovat
Mahulena	Mahulena	k1gFnSc1	Mahulena
Bočanová	Bočanová	k1gFnSc1	Bočanová
<g/>
,	,	kIx,	,
Adéla	Adéla	k1gFnSc1	Adéla
Gondíková	Gondíková	k1gFnSc1	Gondíková
<g/>
,	,	kIx,	,
Vlasta	Vlasta	k1gFnSc1	Vlasta
Žehrová	Žehrová	k1gFnSc1	Žehrová
<g/>
,	,	kIx,	,
Olga	Olga	k1gFnSc1	Olga
Želenská	Želenský	k2eAgFnSc1d1	Želenská
<g/>
,	,	kIx,	,
Monika	Monika	k1gFnSc1	Monika
Absolonová	Absolonová	k1gFnSc1	Absolonová
<g/>
,	,	kIx,	,
Ivana	Ivana	k1gFnSc1	Ivana
Andrlová	Andrlová	k1gFnSc1	Andrlová
anebo	anebo	k8xC	anebo
Jana	Jana	k1gFnSc1	Jana
Zenáhlíková	Zenáhlíkový	k2eAgFnSc1d1	Zenáhlíkový
v	v	k7c6	v
rolích	role	k1gFnPc6	role
lehkých	lehký	k2eAgFnPc2d1	lehká
dam	dáma	k1gFnPc2	dáma
<g/>
,	,	kIx,	,
Miriam	Miriam	k1gFnSc1	Miriam
Kantorková	Kantorková	k1gFnSc1	Kantorková
nebo	nebo	k8xC	nebo
Vlasta	Vlasta	k1gFnSc1	Vlasta
Peterková	Peterková	k1gFnSc1	Peterková
v	v	k7c6	v
roli	role	k1gFnSc6	role
paní	paní	k1gFnSc2	paní
Trčkové	Trčkové	k2eAgFnSc2d1	Trčkové
<g/>
,	,	kIx,	,
Dalibor	Dalibor	k1gMnSc1	Dalibor
Gondík	Gondík	k1gMnSc1	Gondík
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Zounar	Zounar	k1gMnSc1	Zounar
<g/>
,	,	kIx,	,
Zbyšek	Zbyšek	k1gMnSc1	Zbyšek
Pantůček	Pantůček	k1gMnSc1	Pantůček
<g/>
,	,	kIx,	,
Aleš	Aleš	k1gMnSc1	Aleš
Háma	Háma	k1gMnSc1	Háma
<g/>
,	,	kIx,	,
Filip	Filip	k1gMnSc1	Filip
Tomsa	Tomsa	k1gFnSc1	Tomsa
<g/>
,	,	kIx,	,
Lumír	Lumír	k1gMnSc1	Lumír
Olšovský	Olšovský	k1gMnSc1	Olšovský
v	v	k7c6	v
rolích	role	k1gFnPc6	role
fasádníků	fasádník	k1gMnPc2	fasádník
a	a	k8xC	a
Martin	Martin	k1gMnSc1	Martin
Sobotka	Sobotka	k1gMnSc1	Sobotka
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Vítek	Vítek	k1gMnSc1	Vítek
a	a	k8xC	a
Lukáš	Lukáš	k1gMnSc1	Lukáš
Pečenka	pečenka	k1gFnSc1	pečenka
v	v	k7c6	v
ostatních	ostatní	k2eAgFnPc6d1	ostatní
rolích	role	k1gFnPc6	role
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
zvláštní	zvláštní	k2eAgMnSc1d1	zvláštní
hosté	host	k1gMnPc1	host
účinkují	účinkovat	k5eAaImIp3nP	účinkovat
v	v	k7c6	v
roli	role	k1gFnSc6	role
emeritního	emeritní	k2eAgMnSc2d1	emeritní
profesora	profesor	k1gMnSc2	profesor
Jan	Jan	k1gMnSc1	Jan
Přeučil	přeučit	k5eAaPmAgMnS	přeučit
nebo	nebo	k8xC	nebo
Josef	Josef	k1gMnSc1	Josef
Oplt	Oplt	k1gMnSc1	Oplt
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostní	slavnostní	k2eAgFnSc1d1	slavnostní
premiéra	premiéra	k1gFnSc1	premiéra
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2011	[number]	k4	2011
v	v	k7c6	v
Branickém	branický	k2eAgNnSc6d1	Branické
divadle	divadlo	k1gNnSc6	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
uvedlo	uvést	k5eAaPmAgNnS	uvést
Světáky	Světák	k1gMnPc7	Světák
také	také	k9	také
československé	československý	k2eAgFnSc2d1	Československá
Nové	Nová	k1gFnSc2	Nová
Divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
Jany	Jana	k1gFnSc2	Jana
Fabiánové	Fabiánová	k1gFnSc2	Fabiánová
<g/>
.	.	kIx.	.
</s>
