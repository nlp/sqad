<p>
<s>
Realismus	realismus	k1gInSc1	realismus
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
skutečný	skutečný	k2eAgInSc1d1	skutečný
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
výtvarná	výtvarný	k2eAgFnSc1d1	výtvarná
forma	forma	k1gFnSc1	forma
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
dívá	dívat	k5eAaImIp3nS	dívat
na	na	k7c4	na
skutečnost	skutečnost	k1gFnSc4	skutečnost
objektivně	objektivně	k6eAd1	objektivně
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
iluzí	iluze	k1gFnPc2	iluze
a	a	k8xC	a
fantazie	fantazie	k1gFnSc2	fantazie
<g/>
.	.	kIx.	.
</s>
<s>
Sílící	sílící	k2eAgNnSc1d1	sílící
materialistické	materialistický	k2eAgNnSc1d1	materialistické
pojetí	pojetí	k1gNnSc1	pojetí
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
ve	v	k7c6	v
výtvarném	výtvarný	k2eAgNnSc6d1	výtvarné
umění	umění	k1gNnSc6	umění
snahou	snaha	k1gFnSc7	snaha
o	o	k7c4	o
zachycení	zachycení	k1gNnSc4	zachycení
dokonalé	dokonalý	k2eAgFnSc2d1	dokonalá
fyzické	fyzický	k2eAgFnSc2d1	fyzická
reality	realita	k1gFnSc2	realita
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
jde	jít	k5eAaImIp3nS	jít
až	až	k9	až
o	o	k7c6	o
dokonalé	dokonalý	k2eAgFnSc6d1	dokonalá
naturalistické	naturalistický	k2eAgFnSc6d1	naturalistická
kopii	kopie	k1gFnSc6	kopie
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
chtěli	chtít	k5eAaImAgMnP	chtít
lidé	člověk	k1gMnPc1	člověk
zachytit	zachytit	k5eAaPmF	zachytit
skutečnost	skutečnost	k1gFnSc4	skutečnost
pravdivě	pravdivě	k6eAd1	pravdivě
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
idealizace	idealizace	k1gFnSc2	idealizace
<g/>
,	,	kIx,	,
měli	mít	k5eAaImAgMnP	mít
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
všední	všední	k2eAgFnSc4d1	všední
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
Umění	umění	k1gNnSc1	umění
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
soustředilo	soustředit	k5eAaPmAgNnS	soustředit
na	na	k7c4	na
život	život	k1gInSc4	život
bohatých	bohatý	k2eAgMnPc2d1	bohatý
a	a	k8xC	a
spokojených	spokojený	k2eAgMnPc2d1	spokojený
lidí	člověk	k1gMnPc2	člověk
nebo	nebo	k8xC	nebo
historických	historický	k2eAgFnPc2d1	historická
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vystřídal	vystřídat	k5eAaPmAgInS	vystřídat
romantismus	romantismus	k1gInSc1	romantismus
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
do	do	k7c2	do
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
se	se	k3xPyFc4	se
staví	stavit	k5eAaBmIp3nS	stavit
rozum	rozum	k1gInSc1	rozum
<g/>
.	.	kIx.	.
</s>
<s>
Označuje	označovat	k5eAaImIp3nS	označovat
různé	různý	k2eAgInPc4d1	různý
směry	směr	k1gInPc4	směr
<g/>
,	,	kIx,	,
názory	názor	k1gInPc4	názor
a	a	k8xC	a
postoje	postoj	k1gInPc4	postoj
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výtvarném	výtvarný	k2eAgNnSc6d1	výtvarné
umění	umění	k1gNnSc6	umění
znamená	znamenat	k5eAaImIp3nS	znamenat
realismus	realismus	k1gInSc4	realismus
snahu	snaha	k1gFnSc4	snaha
o	o	k7c4	o
věrné	věrný	k2eAgNnSc4d1	věrné
a	a	k8xC	a
detailní	detailní	k2eAgNnSc4d1	detailní
zachycení	zachycení	k1gNnSc4	zachycení
předmětu	předmět	k1gInSc2	předmět
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
jej	on	k3xPp3gMnSc4	on
divák	divák	k1gMnSc1	divák
vidí	vidět	k5eAaImIp3nS	vidět
<g/>
.	.	kIx.	.
</s>
<s>
Objevovaly	objevovat	k5eAaImAgInP	objevovat
se	se	k3xPyFc4	se
nové	nový	k2eAgInPc1d1	nový
stavební	stavební	k2eAgInPc1d1	stavební
materiály	materiál	k1gInPc1	materiál
<g/>
,	,	kIx,	,
např.	např.	kA	např.
železo	železo	k1gNnSc4	železo
a	a	k8xC	a
sklo	sklo	k1gNnSc4	sklo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Realismus	realismus	k1gInSc4	realismus
v	v	k7c6	v
sochařství	sochařství	k1gNnSc6	sochařství
==	==	k?	==
</s>
</p>
<p>
<s>
Realismus	realismus	k1gInSc1	realismus
v	v	k7c6	v
sochařství	sochařství	k1gNnSc6	sochařství
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
druhého	druhý	k4xOgNnSc2	druhý
císařství	císařství	k1gNnPc2	císařství
<g/>
.	.	kIx.	.
</s>
<s>
Nejslavnějším	slavný	k2eAgMnSc7d3	nejslavnější
sochařem	sochař	k1gMnSc7	sochař
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
byl	být	k5eAaImAgInS	být
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
Carpeaux	Carpeaux	k1gInSc1	Carpeaux
(	(	kIx(	(
<g/>
1827	[number]	k4	1827
<g/>
-	-	kIx~	-
<g/>
1875	[number]	k4	1875
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc4	jeho
díla	dílo	k1gNnPc4	dílo
zdobí	zdobit	k5eAaImIp3nS	zdobit
budovu	budova	k1gFnSc4	budova
Louvru	Louvre	k1gInSc2	Louvre
(	(	kIx(	(
<g/>
Flora	Flora	k1gFnSc1	Flora
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pařížské	pařížský	k2eAgFnSc2d1	Pařížská
opery	opera	k1gFnSc2	opera
(	(	kIx(	(
<g/>
Tanec	tanec	k1gInSc1	tanec
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
fontánu	fontána	k1gFnSc4	fontána
Observatoře	observatoř	k1gFnSc2	observatoř
(	(	kIx(	(
<g/>
Fontána	fontána	k1gFnSc1	fontána
Čtyř	čtyři	k4xCgFnPc2	čtyři
světových	světový	k2eAgFnPc2d1	světová
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Carpeaux	Carpeaux	k1gInSc1	Carpeaux
byl	být	k5eAaImAgInS	být
i	i	k9	i
malířem	malíř	k1gMnSc7	malíř
<g/>
,	,	kIx,	,
jeho	on	k3xPp3gNnSc2	on
obrazy	obraz	k1gInPc7	obraz
plné	plný	k2eAgFnSc6d1	plná
světelných	světelný	k2eAgInPc2d1	světelný
efektů	efekt	k1gInPc2	efekt
působí	působit	k5eAaImIp3nS	působit
až	až	k9	až
impresionisticky	impresionisticky	k6eAd1	impresionisticky
(	(	kIx(	(
<g/>
Maškarní	maškarní	k2eAgInSc1d1	maškarní
ples	ples	k1gInSc1	ples
v	v	k7c6	v
Tuileriích	Tuilerie	k1gFnPc6	Tuilerie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejtypičtějším	typický	k2eAgMnSc7d3	nejtypičtější
realistou	realista	k1gMnSc7	realista
v	v	k7c6	v
sochařství	sochařství	k1gNnSc6	sochařství
byl	být	k5eAaImAgInS	být
Aimé-Jules	Aimé-Jules	k1gInSc1	Aimé-Jules
Dalou	Dalá	k1gFnSc4	Dalá
(	(	kIx(	(
<g/>
1838	[number]	k4	1838
<g/>
-	-	kIx~	-
<g/>
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
alegorické	alegorický	k2eAgNnSc1d1	alegorické
sousoší	sousoší	k1gNnSc1	sousoší
Triumf	triumf	k1gInSc4	triumf
Republiky	republika	k1gFnSc2	republika
na	na	k7c6	na
Place	plac	k1gInSc6	plac
de	de	k?	de
la	la	k1gNnSc2	la
Nation	Nation	k1gInSc1	Nation
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
)	)	kIx)	)
Realistické	realistický	k2eAgInPc1d1	realistický
začátky	začátek	k1gInPc1	začátek
lze	lze	k6eAd1	lze
vidět	vidět	k5eAaImF	vidět
i	i	k9	i
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Augusta	August	k1gMnSc2	August
Rodina	rodina	k1gFnSc1	rodina
(	(	kIx(	(
<g/>
1840	[number]	k4	1840
<g/>
-	-	kIx~	-
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dokonalá	dokonalý	k2eAgFnSc1d1	dokonalá
forma	forma	k1gFnSc1	forma
jeho	jeho	k3xOp3gNnPc2	jeho
děl	dělo	k1gNnPc2	dělo
vedla	vést	k5eAaImAgFnS	vést
kritiky	kritik	k1gMnPc4	kritik
k	k	k7c3	k
tvrzení	tvrzení	k1gNnSc3	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnPc1	jeho
sochy	socha	k1gFnPc1	socha
jsou	být	k5eAaImIp3nP	být
sádrovými	sádrový	k2eAgFnPc7d1	sádrová
odlitky	odlitek	k1gInPc1	odlitek
skutečných	skutečný	k2eAgMnPc2d1	skutečný
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
Kovový	kovový	k2eAgInSc1d1	kovový
věk	věk	k1gInSc1	věk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sochařská	sochařský	k2eAgFnSc1d1	sochařská
činnost	činnost	k1gFnSc1	činnost
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
také	také	k9	také
malbu	malba	k1gFnSc4	malba
Edgara	Edgar	k1gMnSc4	Edgar
Degase	Degasa	k1gFnSc6	Degasa
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
postavy	postava	k1gFnPc1	postava
baletek	baletka	k1gFnPc2	baletka
oblečené	oblečený	k2eAgFnPc4d1	oblečená
ve	v	k7c6	v
skutečných	skutečný	k2eAgInPc6d1	skutečný
šatech	šat	k1gInPc6	šat
jako	jako	k9	jako
by	by	kYmCp3nP	by
vystoupily	vystoupit	k5eAaPmAgInP	vystoupit
z	z	k7c2	z
obrazů	obraz	k1gInPc2	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
i	i	k9	i
sochy	socha	k1gFnPc1	socha
a	a	k8xC	a
reliéfy	reliéf	k1gInPc1	reliéf
Paula	Paul	k1gMnSc2	Paul
Gauguina	Gauguin	k1gMnSc2	Gauguin
vycházející	vycházející	k2eAgFnPc1d1	vycházející
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
zážitků	zážitek	k1gInPc2	zážitek
na	na	k7c6	na
Tahiti	Tahiti	k1gNnSc6	Tahiti
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Realismus	realismus	k1gInSc4	realismus
v	v	k7c6	v
malířství	malířství	k1gNnSc6	malířství
==	==	k?	==
</s>
</p>
<p>
<s>
Předvoj	předvoj	k1gInSc1	předvoj
realistické	realistický	k2eAgFnSc2d1	realistická
malby	malba	k1gFnSc2	malba
je	být	k5eAaImIp3nS	být
představován	představovat	k5eAaImNgInS	představovat
obrazem	obraz	k1gInSc7	obraz
Prám	prám	k1gInSc1	prám
Medusy	Medusa	k1gFnSc2	Medusa
(	(	kIx(	(
<g/>
Vor	vor	k1gInSc1	vor
Medusy	Medusa	k1gFnSc2	Medusa
<g/>
)	)	kIx)	)
od	od	k7c2	od
Théodora	Théodor	k1gMnSc2	Théodor
Géricaulta	Géricault	k1gMnSc2	Géricault
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
líčí	líčit	k5eAaImIp3nS	líčit
dramatickou	dramatický	k2eAgFnSc4d1	dramatická
událost	událost	k1gFnSc4	událost
podle	podle	k7c2	podle
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
po	po	k7c6	po
ztroskotání	ztroskotání	k1gNnSc6	ztroskotání
lodi	loď	k1gFnSc2	loď
zachránilo	zachránit	k5eAaPmAgNnS	zachránit
pouze	pouze	k6eAd1	pouze
deset	deset	k4xCc1	deset
námořníků	námořník	k1gMnPc2	námořník
z	z	k7c2	z
původních	původní	k2eAgNnPc2d1	původní
400	[number]	k4	400
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
vzniká	vznikat	k5eAaImIp3nS	vznikat
tzv.	tzv.	kA	tzv.
Barbizonská	Barbizonský	k2eAgFnSc1d1	Barbizonská
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
vznik	vznik	k1gInSc4	vznik
realistického	realistický	k2eAgNnSc2d1	realistické
krajinářství	krajinářství	k1gNnSc2	krajinářství
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
patří	patřit	k5eAaImIp3nS	patřit
zejména	zejména	k9	zejména
Théodore	Théodor	k1gInSc5	Théodor
Rousseau	Rousseau	k1gMnSc1	Rousseau
<g/>
,	,	kIx,	,
Jean-Francois	Jean-Francois	k1gFnSc1	Jean-Francois
Millet	Millet	k1gInSc1	Millet
a	a	k8xC	a
Jules	Jules	k1gInSc1	Jules
Dupré	Duprý	k2eAgFnSc2d1	Duprý
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
pravdivost	pravdivost	k1gFnSc4	pravdivost
obrazu	obraz	k1gInSc2	obraz
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc4	jejich
až	až	k9	až
kultovní	kultovní	k2eAgNnSc1d1	kultovní
uctívání	uctívání	k1gNnSc1	uctívání
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
venkovského	venkovský	k2eAgInSc2d1	venkovský
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
základním	základní	k2eAgInSc7d1	základní
kamenem	kámen	k1gInSc7	kámen
přicházejícího	přicházející	k2eAgInSc2d1	přicházející
realismu	realismus	k1gInSc2	realismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1855	[number]	k4	1855
uspořádal	uspořádat	k5eAaPmAgMnS	uspořádat
francouzský	francouzský	k2eAgMnSc1d1	francouzský
malíř	malíř	k1gMnSc1	malíř
Gustave	Gustav	k1gMnSc5	Gustav
Courbet	Courbet	k1gInSc4	Courbet
výstavu	výstava	k1gFnSc4	výstava
svých	svůj	k3xOyFgInPc2	svůj
obrazů	obraz	k1gInPc2	obraz
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
nazval	nazvat	k5eAaBmAgMnS	nazvat
provokativně	provokativně	k6eAd1	provokativně
Le	Le	k1gMnSc1	Le
realisme	realismus	k1gInSc5	realismus
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
název	název	k1gInSc4	název
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
označením	označení	k1gNnSc7	označení
celého	celý	k2eAgInSc2d1	celý
uměleckého	umělecký	k2eAgInSc2d1	umělecký
směru	směr	k1gInSc2	směr
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc4	jehož
zásady	zásada	k1gFnPc4	zásada
Courbet	Courbet	k1gInSc1	Courbet
propagoval	propagovat	k5eAaImAgInS	propagovat
i	i	k9	i
teoreticky	teoreticky	k6eAd1	teoreticky
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
obrazy	obraz	k1gInPc1	obraz
vyvolávaly	vyvolávat	k5eAaImAgInP	vyvolávat
pohoršení	pohoršení	k1gNnSc4	pohoršení
(	(	kIx(	(
<g/>
Štěrkaři	štěrkař	k1gMnSc3	štěrkař
<g/>
,	,	kIx,	,
Pohřeb	pohřeb	k1gInSc1	pohřeb
v	v	k7c4	v
Ornans	Ornans	k1gInSc4	Ornans
<g/>
,	,	kIx,	,
Atelier	atelier	k1gNnSc4	atelier
<g/>
,	,	kIx,	,
Dobrý	dobrý	k2eAgInSc4d1	dobrý
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
pane	pan	k1gMnSc5	pan
Courbete	Courbete	k1gMnSc5	Courbete
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vliv	vliv	k1gInSc1	vliv
francouzských	francouzský	k2eAgMnPc2d1	francouzský
realistů	realista	k1gMnPc2	realista
podnítil	podnítit	k5eAaPmAgInS	podnítit
rozvoj	rozvoj	k1gInSc4	rozvoj
realistické	realistický	k2eAgFnSc2d1	realistická
malby	malba	k1gFnSc2	malba
i	i	k9	i
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
osobností	osobnost	k1gFnSc7	osobnost
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgMnS	být
Adolf	Adolf	k1gMnSc1	Adolf
von	von	k1gInSc4	von
Menzel	Menzel	k1gFnSc2	Menzel
(	(	kIx(	(
<g/>
1815	[number]	k4	1815
-	-	kIx~	-
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velké	velký	k2eAgFnSc3d1	velká
oblibě	obliba	k1gFnSc3	obliba
se	se	k3xPyFc4	se
těšil	těšit	k5eAaImAgMnS	těšit
realismus	realismus	k1gInSc4	realismus
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
skutečně	skutečně	k6eAd1	skutečně
národním	národní	k2eAgInSc7d1	národní
slohem	sloh	k1gInSc7	sloh
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějšími	známý	k2eAgMnPc7d3	nejznámější
malíři	malíř	k1gMnPc7	malíř
byli	být	k5eAaImAgMnP	být
Ilja	Ilja	k1gMnSc1	Ilja
Jefimovič	Jefimovič	k1gMnSc1	Jefimovič
Repin	Repin	k1gMnSc1	Repin
(	(	kIx(	(
<g/>
Burlaci	Burlace	k1gFnSc6	Burlace
na	na	k7c6	na
Volze	Volha	k1gFnSc6	Volha
<g/>
)	)	kIx)	)
a	a	k8xC	a
Isaak	Isaak	k1gMnSc1	Isaak
Iljič	Iljič	k1gMnSc1	Iljič
Levitan	Levitan	k1gInSc4	Levitan
<g/>
.	.	kIx.	.
</s>
</p>
