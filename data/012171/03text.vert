<p>
<s>
Čamara	čamara	k1gFnSc1	čamara
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
pánského	pánský	k2eAgInSc2d1	pánský
kabátu	kabát	k1gInSc2	kabát
<g/>
.	.	kIx.	.
</s>
<s>
Černá	černý	k2eAgFnSc1d1	černá
čamara	čamara	k1gFnSc1	čamara
se	se	k3xPyFc4	se
nosila	nosit	k5eAaImAgFnS	nosit
při	při	k7c6	při
slavnostních	slavnostní	k2eAgFnPc6d1	slavnostní
událostech	událost	k1gFnPc6	událost
<g/>
,	,	kIx,	,
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
hlavně	hlavně	k9	hlavně
v	v	k7c4	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jako	jako	k8xC	jako
symbol	symbol	k1gInSc1	symbol
příslušnosti	příslušnost	k1gFnSc2	příslušnost
ke	k	k7c3	k
slovanstvu	slovanstvo	k1gNnSc3	slovanstvo
a	a	k8xC	a
vlastenectví	vlastenectví	k1gNnSc3	vlastenectví
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
čamary	čamara	k1gFnSc2	čamara
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
oděvu	oděv	k1gInSc2	oděv
(	(	kIx(	(
<g/>
czamara	czamara	k1gFnSc1	czamara
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzhled	vzhled	k1gInSc4	vzhled
==	==	k?	==
</s>
</p>
<p>
<s>
Čamara	čamara	k1gFnSc1	čamara
má	mít	k5eAaImIp3nS	mít
černou	černý	k2eAgFnSc4d1	černá
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
stojatý	stojatý	k2eAgInSc4d1	stojatý
límec	límec	k1gInSc4	límec
a	a	k8xC	a
zapíná	zapínat	k5eAaImIp3nS	zapínat
se	se	k3xPyFc4	se
řadou	řada	k1gFnSc7	řada
knoflíků	knoflík	k1gInPc2	knoflík
a	a	k8xC	a
šňůrek	šňůrka	k1gFnPc2	šňůrka
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
kabát	kabát	k1gInSc1	kabát
je	být	k5eAaImIp3nS	být
delšího	dlouhý	k2eAgInSc2d2	delší
střihu	střih	k1gInSc2	střih
<g/>
.	.	kIx.	.
</s>
</p>
