<s>
Garcia	Garcia	k1gFnSc1	Garcia
Jofre	Jofr	k1gInSc5	Jofr
de	de	k?	de
Loaisa	Loais	k1gMnSc2	Loais
(	(	kIx(	(
<g/>
1485	[number]	k4	1485
Španělsko	Španělsko	k1gNnSc1	Španělsko
-	-	kIx~	-
30	[number]	k4	30
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1526	[number]	k4	1526
Tichý	tichý	k2eAgInSc1d1	tichý
oceán	oceán	k1gInSc1	oceán
<g/>
)	)	kIx)	)
také	také	k9	také
Loaysa	Loaysa	k1gFnSc1	Loaysa
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
španělský	španělský	k2eAgMnSc1d1	španělský
mořeplavec	mořeplavec	k1gMnSc1	mořeplavec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
roku	rok	k1gInSc2	rok
1525	[number]	k4	1525
byl	být	k5eAaImAgInS	být
vyslán	vyslat	k5eAaPmNgInS	vyslat
se	s	k7c7	s
sedmi	sedm	k4xCc7	sedm
loděmi	loď	k1gFnPc7	loď
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
opakoval	opakovat	k5eAaImAgInS	opakovat
plavbu	plavba	k1gFnSc4	plavba
Fernanda	Fernando	k1gNnSc2	Fernando
Magellana	Magellana	k1gFnSc1	Magellana
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
vydal	vydat	k5eAaPmAgMnS	vydat
také	také	k9	také
Juan	Juan	k1gMnSc1	Juan
Sebastián	Sebastián	k1gMnSc1	Sebastián
Elcano	Elcana	k1gFnSc5	Elcana
<g/>
,	,	kIx,	,
mořeplavec	mořeplavec	k1gMnSc1	mořeplavec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1522	[number]	k4	1522
dokončil	dokončit	k5eAaPmAgMnS	dokončit
plavbu	plavba	k1gFnSc4	plavba
kolem	kolem	k7c2	kolem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tentokrát	tentokrát	k6eAd1	tentokrát
Elcano	Elcana	k1gFnSc5	Elcana
zastával	zastávat	k5eAaImAgInS	zastávat
funkci	funkce	k1gFnSc4	funkce
hlavního	hlavní	k2eAgMnSc2d1	hlavní
navigátora	navigátor	k1gMnSc2	navigátor
<g/>
.	.	kIx.	.
</s>
<s>
Loaisa	Loaisa	k1gFnSc1	Loaisa
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
západním	západní	k2eAgInSc7d1	západní
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
proplutí	proplutí	k1gNnSc6	proplutí
Magellanova	Magellanův	k2eAgInSc2d1	Magellanův
průlivu	průliv	k1gInSc2	průliv
bylo	být	k5eAaImAgNnS	být
loďstvo	loďstvo	k1gNnSc1	loďstvo
v	v	k7c6	v
bouři	bouř	k1gFnSc6	bouř
rozptýleno	rozptýlit	k5eAaPmNgNnS	rozptýlit
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
lodě	loď	k1gFnPc1	loď
ztroskotaly	ztroskotat	k5eAaPmAgFnP	ztroskotat
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
loď	loď	k1gFnSc1	loď
plula	plout	k5eAaImAgFnS	plout
na	na	k7c4	na
sever	sever	k1gInSc4	sever
do	do	k7c2	do
Mexika	Mexiko	k1gNnSc2	Mexiko
podél	podél	k7c2	podél
Jihoamerického	jihoamerický	k2eAgNnSc2d1	jihoamerické
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
byl	být	k5eAaImAgInS	být
určen	určen	k2eAgInSc1d1	určen
rozsah	rozsah	k1gInSc1	rozsah
tohoto	tento	k3xDgInSc2	tento
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
,	,	kIx,	,
o	o	k7c6	o
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
domnívali	domnívat	k5eAaImAgMnP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
mnohem	mnohem	k6eAd1	mnohem
víc	hodně	k6eAd2	hodně
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Plavbu	plavba	k1gFnSc4	plavba
přes	přes	k7c4	přes
Tichý	tichý	k2eAgInSc4d1	tichý
oceán	oceán	k1gInSc4	oceán
dokončila	dokončit	k5eAaPmAgFnS	dokončit
jen	jen	k9	jen
Loaisova	Loaisův	k2eAgFnSc1d1	Loaisův
loď	loď	k1gFnSc1	loď
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
objevila	objevit	k5eAaPmAgFnS	objevit
ostrov	ostrov	k1gInSc4	ostrov
Taona	Taon	k1gInSc2	Taon
v	v	k7c6	v
Marshallových	Marshallův	k2eAgInPc6d1	Marshallův
ostrovech	ostrov	k1gInPc6	ostrov
a	a	k8xC	a
přes	přes	k7c4	přes
Mariany	Marian	k1gMnPc4	Marian
(	(	kIx(	(
<g/>
Landrony	Landron	k1gMnPc7	Landron
<g/>
)	)	kIx)	)
a	a	k8xC	a
Mindanao	Mindanao	k6eAd1	Mindanao
se	s	k7c7	s
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1527	[number]	k4	1527
dostala	dostat	k5eAaPmAgFnS	dostat
k	k	k7c3	k
ostrovu	ostrov	k1gInSc3	ostrov
Tidore	Tidor	k1gInSc5	Tidor
na	na	k7c6	na
Molukách	Moluky	k1gFnPc6	Moluky
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Loaisa	Loais	k1gMnSc2	Loais
však	však	k9	však
už	už	k6eAd1	už
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
nebyl	být	k5eNaImAgMnS	být
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
překročení	překročení	k1gNnSc6	překročení
rovníku	rovník	k1gInSc2	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
posádky	posádka	k1gFnSc2	posádka
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
Tidore	Tidor	k1gInSc5	Tidor
zajat	zajat	k2eAgMnSc1d1	zajat
Portugalci	Portugalec	k1gMnPc1	Portugalec
<g/>
.	.	kIx.	.
</s>
<s>
Vysvobodil	vysvobodit	k5eAaPmAgMnS	vysvobodit
je	být	k5eAaImIp3nS	být
až	až	k9	až
španělský	španělský	k2eAgMnSc1d1	španělský
mořeplavec	mořeplavec	k1gMnSc1	mořeplavec
a	a	k8xC	a
bratranec	bratranec	k1gMnSc1	bratranec
Hernána	Hernán	k2eAgFnSc1d1	Hernána
Cortése	Cortés	k1gInSc5	Cortés
Álvaro	Álvara	k1gFnSc5	Álvara
de	de	k?	de
Saavedra	Saavedr	k1gMnSc4	Saavedr
Céron	Céron	k1gInSc1	Céron
<g/>
.	.	kIx.	.
</s>
<s>
Geografický	geografický	k2eAgInSc1d1	geografický
přínos	přínos	k1gInSc1	přínos
samotného	samotný	k2eAgMnSc2d1	samotný
Loaisy	Loais	k1gInPc7	Loais
nebyl	být	k5eNaImAgInS	být
velký	velký	k2eAgInSc1d1	velký
<g/>
,	,	kIx,	,
významná	významný	k2eAgFnSc1d1	významná
byla	být	k5eAaImAgFnS	být
plavba	plavba	k1gFnSc1	plavba
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
lodí	loď	k1gFnPc2	loď
podél	podél	k7c2	podél
západního	západní	k2eAgNnSc2d1	západní
pobřeží	pobřeží	k1gNnSc2	pobřeží
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Hrbek	Hrbek	k1gMnSc1	Hrbek
<g/>
,	,	kIx,	,
ABC	ABC	kA	ABC
cestovatelů	cestovatel	k1gMnPc2	cestovatel
<g/>
,	,	kIx,	,
mořeplavců	mořeplavec	k1gMnPc2	mořeplavec
<g/>
,	,	kIx,	,
objevitelů	objevitel	k1gMnPc2	objevitel
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Panorama	panorama	k1gNnSc1	panorama
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
285	[number]	k4	285
s.	s.	k?	s.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
Historie	historie	k1gFnSc1	historie
Filipín	Filipíny	k1gFnPc2	Filipíny
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
<g/>
)	)	kIx)	)
<g/>
Španělští	španělský	k2eAgMnPc1d1	španělský
mořeplavci	mořeplavec	k1gMnPc1	mořeplavec
na	na	k7c6	na
Filipínách	Filipíny	k1gFnPc6	Filipíny
</s>
