<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
v	v	k7c6
dubnu	duben	k1gInSc6
1926	#num#	k4
uhodil	uhodit	k5eAaPmAgMnS
do	do	k7c2
hlavy	hlava	k1gFnSc2
jedenáctiletého	jedenáctiletý	k2eAgMnSc2d1
žáka	žák	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
pak	pak	k6eAd1
upadl	upadnout	k5eAaPmAgMnS
do	do	k7c2
bezvědomí	bezvědomí	k1gNnSc2
<g/>
,	,	kIx,
podal	podat	k5eAaPmAgInS
Wittgenstein	Wittgenstein	k1gInSc1
u	u	k7c2
okresního	okresní	k2eAgMnSc2d1
školního	školní	k2eAgMnSc2d1
inspektora	inspektor	k1gMnSc2
žádost	žádost	k1gFnSc4
o	o	k7c4
propuštění	propuštění	k1gNnSc4
dříve	dříve	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
proti	proti	k7c3
němu	on	k3xPp3gInSc3
mohly	moct	k5eAaImAgFnP
být	být	k5eAaImF
podniknuty	podniknout	k5eAaPmNgInP
oficiální	oficiální	k2eAgInPc1d1
kroky	krok	k1gInPc1
(	(	kIx(
<g/>
žákův	žákův	k2eAgMnSc1d1
otčím	otčím	k1gMnSc1
žádal	žádat	k5eAaImAgMnS
jeho	jeho	k3xOp3gNnSc4
uvěznění	uvěznění	k1gNnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>