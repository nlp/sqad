<s>
Ludwig	Ludwig	k1gMnSc1
Wittgenstein	Wittgenstein	k1gMnSc1
</s>
<s>
Ludwig	Ludwig	k1gMnSc1
Wittgenstein	Wittgenstein	k1gMnSc1
</s>
<s>
Ludwig	Ludwig	k1gMnSc1
Wittgenstein	Wittgenstein	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1930	#num#	k4
Region	region	k1gInSc1
</s>
<s>
Západní	západní	k2eAgFnSc1d1
filosofie	filosofie	k1gFnSc1
Období	období	k1gNnSc1
</s>
<s>
filosofie	filosofie	k1gFnSc1
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
Narození	narození	k1gNnPc1
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1889	#num#	k4
Vídeň	Vídeň	k1gFnSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1951	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
62	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Cambridge	Cambridge	k1gFnSc1
Škola	škola	k1gFnSc1
<g/>
/	/	kIx~
<g/>
tradice	tradice	k1gFnSc1
</s>
<s>
analytická	analytický	k2eAgFnSc1d1
filosofie	filosofie	k1gFnSc1
Oblasti	oblast	k1gFnSc2
zájmu	zájem	k1gInSc2
</s>
<s>
logika	logika	k1gFnSc1
<g/>
,	,	kIx,
metafyzika	metafyzika	k1gFnSc1
<g/>
,	,	kIx,
filosofie	filosofie	k1gFnSc1
jazyka	jazyk	k1gInSc2
<g/>
,	,	kIx,
filosofie	filosofie	k1gFnSc2
matematiky	matematika	k1gFnSc2
<g/>
,	,	kIx,
filosofie	filosofie	k1gFnSc2
mysli	mysl	k1gFnSc2
<g/>
,	,	kIx,
epistemologie	epistemologie	k1gFnSc2
Význačné	význačný	k2eAgFnSc2d1
ideje	idea	k1gFnSc2
</s>
<s>
obrázková	obrázkový	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
jazyka	jazyk	k1gInSc2
<g/>
,	,	kIx,
pravdivostní	pravdivostní	k2eAgFnSc1d1
funkce	funkce	k1gFnSc1
<g/>
,	,	kIx,
souhrn	souhrn	k1gInSc1
stavů	stav	k1gInPc2
věcí	věc	k1gFnPc2
<g/>
,	,	kIx,
logická	logický	k2eAgFnSc1d1
nutnost	nutnost	k1gFnSc1
<g/>
,	,	kIx,
používaný	používaný	k2eAgInSc1d1
význam	význam	k1gInSc1
<g/>
,	,	kIx,
jazykové	jazykový	k2eAgFnPc1d1
hry	hra	k1gFnPc1
<g/>
,	,	kIx,
argument	argument	k1gInSc1
proti	proti	k7c3
soukromému	soukromý	k2eAgInSc3d1
jazyku	jazyk	k1gInSc3
<g/>
,	,	kIx,
„	„	k?
<g/>
rodinná	rodinný	k2eAgFnSc1d1
podobnost	podobnost	k1gFnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
následování	následování	k1gNnSc4
pravidla	pravidlo	k1gNnSc2
<g/>
,	,	kIx,
životní	životní	k2eAgFnSc2d1
formy	forma	k1gFnSc2
<g/>
,	,	kIx,
wittgensteinovský	wittgensteinovský	k2eAgInSc1d1
fideismus	fideismus	k1gInSc1
<g/>
,	,	kIx,
antirealismus	antirealismus	k1gInSc1
<g/>
,	,	kIx,
filosofie	filosofie	k1gFnSc1
obyčejného	obyčejný	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
<g/>
,	,	kIx,
sémantický	sémantický	k2eAgInSc1d1
externalismus	externalismus	k1gInSc1
<g/>
,	,	kIx,
kritika	kritika	k1gFnSc1
teorie	teorie	k1gFnSc2
množin	množina	k1gFnPc2
Významná	významný	k2eAgFnSc1d1
díla	dílo	k1gNnPc4
</s>
<s>
Tractatus	Tractatus	k1gMnSc1
logico-philosophicus	logico-philosophicus	k1gMnSc1
<g/>
,	,	kIx,
Filosofická	filosofický	k2eAgNnPc4d1
zkoumání	zkoumání	k1gNnPc4
<g/>
,	,	kIx,
„	„	k?
<g/>
Modrá	modrý	k2eAgFnSc1d1
a	a	k8xC
hnědá	hnědý	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
O	o	k7c6
jistotě	jistota	k1gFnSc6
<g/>
,	,	kIx,
Rozličné	rozličný	k2eAgFnPc4d1
poznámky	poznámka	k1gFnPc4
Vlivy	vliv	k1gInPc1
</s>
<s>
Gottlob	Gottloba	k1gFnPc2
Frege	Frege	k1gNnSc2
<g/>
,	,	kIx,
Johann	Johann	k1gMnSc1
Wolfgang	Wolfgang	k1gMnSc1
von	von	k1gInSc4
Goethe	Goethe	k1gNnSc2
<g/>
,	,	kIx,
Sø	Sø	k1gFnPc2
Kierkegaard	Kierkegaarda	k1gFnPc2
<g/>
,	,	kIx,
G.	G.	kA
E.	E.	kA
Moore	Moor	k1gInSc5
<g/>
,	,	kIx,
Oswald	Oswald	k1gMnSc1
Spengler	Spengler	k1gMnSc1
<g/>
,	,	kIx,
Ludwig	Ludwig	k1gMnSc1
Boltzmann	Boltzmann	k1gMnSc1
<g/>
,	,	kIx,
Karl	Karl	k1gMnSc1
Kraus	Kraus	k1gMnSc1
<g/>
,	,	kIx,
Adolf	Adolf	k1gMnSc1
Loos	Loosa	k1gFnPc2
<g/>
,	,	kIx,
Piero	Piero	k1gNnSc4
Sraffa	Sraff	k1gMnSc2
<g/>
,	,	kIx,
Otto	Otto	k1gMnSc1
Weininger	Weininger	k1gMnSc1
<g/>
,	,	kIx,
Bertrand	Bertrand	k1gInSc1
Russell	Russell	k1gInSc1
<g/>
,	,	kIx,
Arthur	Arthur	k1gMnSc1
Schopenhauer	Schopenhauer	k1gMnSc1
<g/>
,	,	kIx,
Baruch	Baruch	k1gMnSc1
Spinoza	Spinoza	k1gFnSc1
<g/>
,	,	kIx,
Lev	Lev	k1gMnSc1
Nikolajevič	Nikolajevič	k1gMnSc1
Tolstoj	Tolstoj	k1gMnSc1
<g/>
,	,	kIx,
Heinrich	Heinrich	k1gMnSc1
Rudolf	Rudolf	k1gMnSc1
Hertz	hertz	k1gInSc4
Vliv	vliv	k1gInSc4
na	na	k7c4
</s>
<s>
Bertrand	Bertrand	k1gInSc1
Russell	Russell	k1gInSc1
<g/>
,	,	kIx,
George	George	k1gFnSc1
Edward	Edward	k1gMnSc1
Moore	Moor	k1gInSc5
<g/>
,	,	kIx,
Frank	Frank	k1gMnSc1
Ramsey	Ramsea	k1gFnSc2
<g/>
,	,	kIx,
Vídeňský	vídeňský	k2eAgInSc1d1
kroužek	kroužek	k1gInSc1
<g/>
,	,	kIx,
Rudolf	Rudolf	k1gMnSc1
Carnap	Carnap	k1gMnSc1
<g/>
,	,	kIx,
Alan	Alan	k1gMnSc1
Turing	Turing	k1gInSc1
<g/>
,	,	kIx,
Elizabeth	Elizabeth	k1gFnSc1
Anscombe	Anscomb	k1gInSc5
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
Geach	Geach	k1gMnSc1
<g/>
,	,	kIx,
Anthony	Anthona	k1gFnPc1
Kenny	Kenna	k1gFnPc1
<g/>
,	,	kIx,
Barry	Barra	k1gFnPc1
Stroud	Stroud	k1gMnSc1
<g/>
,	,	kIx,
Gilbert	Gilbert	k1gMnSc1
Ryle	Ryl	k1gFnSc2
<g/>
,	,	kIx,
Saul	Saul	k1gMnSc1
Kripke	Kripk	k1gFnSc2
<g/>
,	,	kIx,
John	John	k1gMnSc1
Searle	Searle	k1gFnSc2
<g/>
,	,	kIx,
John	John	k1gMnSc1
McDowell	McDowell	k1gMnSc1
<g/>
,	,	kIx,
Hans	Hans	k1gMnSc1
Sluga	Sluga	k1gFnSc1
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
Hacker	hacker	k1gMnSc1
<g/>
,	,	kIx,
Stephen	Stephen	k2eAgInSc1d1
Edelston	Edelston	k1gInSc1
Toulmin	Toulmin	k2eAgInSc1d1
<g/>
,	,	kIx,
Hannah	Hannah	k1gInSc1
Ginsborg	Ginsborg	k1gInSc1
Podpis	podpis	k1gInSc4
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Příbuzenstvo	příbuzenstvo	k1gNnSc1
</s>
<s>
bratranec	bratranec	k1gMnSc1
</s>
<s>
Friedrich	Friedrich	k1gMnSc1
August	August	k1gMnSc1
von	von	k1gInSc4
Hayek	Hayek	k1gInSc1
</s>
<s>
bratr	bratr	k1gMnSc1
</s>
<s>
Paul	Paul	k1gMnSc1
Wittgenstein	Wittgenstein	k1gMnSc1
</s>
<s>
Ludwig	Ludwig	k1gMnSc1
Josef	Josef	k1gMnSc1
Johann	Johann	k1gMnSc1
Wittgenstein	Wittgenstein	k1gMnSc1
[	[	kIx(
<g/>
vitg	vitg	k1gMnSc1
<g/>
(	(	kIx(
<g/>
e	e	k0
<g/>
)	)	kIx)
<g/>
nštajn	nštajn	k1gInSc4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
26	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1889	#num#	k4
Vídeň	Vídeň	k1gFnSc1
–	–	k?
29	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1951	#num#	k4
Cambridge	Cambridge	k1gFnSc2
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
nejvlivnějších	vlivný	k2eAgMnPc2d3
filosofů	filosof	k1gMnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bývá	bývat	k5eAaImIp3nS
spojován	spojovat	k5eAaImNgInS
především	především	k6eAd1
s	s	k7c7
analytickou	analytický	k2eAgFnSc7d1
filosofií	filosofie	k1gFnSc7
a	a	k8xC
filosofií	filosofie	k1gFnSc7
jazyka	jazyk	k1gInSc2
<g/>
,	,	kIx,
ovlivnil	ovlivnit	k5eAaPmAgInS
však	však	k9
i	i	k9
logické	logický	k2eAgMnPc4d1
pozitivisty	pozitivista	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
myšlení	myšlení	k1gNnSc1
pravděpodobně	pravděpodobně	k6eAd1
nejvíce	hodně	k6eAd3,k6eAd1
ovlivnili	ovlivnit	k5eAaPmAgMnP
Otto	Otto	k1gMnSc1
Weininger	Weininger	k1gMnSc1
<g/>
,	,	kIx,
Arthur	Arthur	k1gMnSc1
Schopenhauer	Schopenhauer	k1gMnSc1
<g/>
,	,	kIx,
Bertrand	Bertrand	k1gInSc1
Russell	Russella	k1gFnPc2
a	a	k8xC
Gottlob	Gottloba	k1gFnPc2
Frege	Freg	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Ludwig	Ludwig	k1gMnSc1
Josef	Josef	k1gMnSc1
Johann	Johann	k1gMnSc1
Wittgenstein	Wittgenstein	k1gMnSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
26	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1889	#num#	k4
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
jako	jako	k9
nejmladší	mladý	k2eAgInSc4d3
z	z	k7c2
osmi	osm	k4xCc2
dětí	dítě	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
rodina	rodina	k1gFnSc1
byla	být	k5eAaImAgFnS
velmi	velmi	k6eAd1
bohatá	bohatý	k2eAgFnSc1d1
<g/>
,	,	kIx,
otec	otec	k1gMnSc1
Karl	Karl	k1gMnSc1
Wittgenstein	Wittgenstein	k1gMnSc1
patřil	patřit	k5eAaImAgMnS
k	k	k7c3
nejúspěšnějším	úspěšný	k2eAgMnPc3d3
podnikatelům	podnikatel	k1gMnPc3
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgMnPc1
Karlovi	Karlův	k2eAgMnPc1d1
rodiče	rodič	k1gMnPc1
byli	být	k5eAaImAgMnP
Židé	Žid	k1gMnPc1
<g/>
,	,	kIx,
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
otec	otec	k1gMnSc1
přijal	přijmout	k5eAaPmAgMnS
jméno	jméno	k1gNnSc4
Christian	Christian	k1gMnSc1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
tak	tak	k6eAd1
distancoval	distancovat	k5eAaBmAgMnS
od	od	k7c2
svého	svůj	k3xOyFgInSc2
židovského	židovský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc1
matka	matka	k1gFnSc1
před	před	k7c7
sňatkem	sňatek	k1gInSc7
konvertovala	konvertovat	k5eAaBmAgFnS
k	k	k7c3
protestantismu	protestantismus	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Ludwigova	Ludwigův	k2eAgFnSc1d1
matka	matka	k1gFnSc1
<g/>
,	,	kIx,
Leopoldina	Leopoldina	k1gFnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
známá	známý	k2eAgFnSc1d1
„	„	k?
<g/>
Poldi	Poldi	k1gFnSc1
<g/>
“	“	k?
z	z	k7c2
názvu	název	k1gInSc2
a	a	k8xC
emblému	emblém	k1gInSc2
kladenských	kladenský	k2eAgFnPc2d1
železáren	železárna	k1gFnPc2
(	(	kIx(
<g/>
Huť	huť	k1gFnSc1
Poldi	Poldi	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gInPc4
otec	otec	k1gMnSc1
byl	být	k5eAaImAgMnS
český	český	k2eAgMnSc1d1
Žid	Žid	k1gMnSc1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
její	její	k3xOp3gFnSc1
matka	matka	k1gFnSc1
byla	být	k5eAaImAgFnS
rakousko-slovinská	rakousko-slovinský	k2eAgFnSc1d1
katolička	katolička	k1gFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Do	do	k7c2
školy	škola	k1gFnSc2
chodil	chodit	k5eAaImAgMnS
Ludwig	Ludwig	k1gMnSc1
Wittgenstein	Wittgenstein	k1gMnSc1
v	v	k7c6
Linci	Linec	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
stejnou	stejný	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
navštěvoval	navštěvovat	k5eAaImAgMnS
tuto	tento	k3xDgFnSc4
školu	škola	k1gFnSc4
i	i	k9
Adolf	Adolf	k1gMnSc1
Hitler	Hitler	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
přesto	přesto	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
byl	být	k5eAaImAgInS
o	o	k7c4
šest	šest	k4xCc4
dnů	den	k1gInPc2
starší	starý	k2eAgMnSc1d2
<g/>
,	,	kIx,
chodil	chodit	k5eAaImAgInS
o	o	k7c4
dvě	dva	k4xCgFnPc4
třídy	třída	k1gFnPc4
níž	nízce	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reálně	reálně	k6eAd1
se	se	k3xPyFc4
tak	tak	k6eAd1
mohl	moct	k5eAaImAgInS
Wittgenstein	Wittgenstein	k1gInSc1
setkat	setkat	k5eAaPmF
s	s	k7c7
Hitlerem	Hitler	k1gMnSc7
pouze	pouze	k6eAd1
v	v	k7c6
jednom	jeden	k4xCgInSc6
školním	školní	k2eAgInSc6d1
roce	rok	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Později	pozdě	k6eAd2
studoval	studovat	k5eAaImAgMnS
Ludwig	Ludwig	k1gMnSc1
strojnictví	strojnictví	k1gNnSc2
v	v	k7c6
Berlíně	Berlín	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc1
zájem	zájem	k1gInSc1
se	se	k3xPyFc4
časem	časem	k6eAd1
rozšířil	rozšířit	k5eAaPmAgInS
na	na	k7c4
matematiku	matematika	k1gFnSc4
<g/>
,	,	kIx,
přes	přes	k7c4
niž	jenž	k3xRgFnSc4
se	se	k3xPyFc4
dostal	dostat	k5eAaPmAgMnS
k	k	k7c3
logice	logika	k1gFnSc3
a	a	k8xC
filozofii	filozofie	k1gFnSc3
matematiky	matematika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
radu	rada	k1gFnSc4
Gottloba	Gottloba	k1gFnSc1
Fregeho	Frege	k1gMnSc2
absolvoval	absolvovat	k5eAaPmAgInS
několik	několik	k4yIc4
trimestrů	trimestr	k1gInPc2
filozofie	filozofie	k1gFnSc2
u	u	k7c2
Bertranda	Bertrando	k1gNnSc2
Russella	Russell	k1gMnSc2
v	v	k7c6
anglické	anglický	k2eAgFnSc6d1
Cambridgi	Cambridge	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vypuknutí	vypuknutí	k1gNnSc6
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
se	se	k3xPyFc4
přihlásil	přihlásit	k5eAaPmAgMnS
do	do	k7c2
rakouské	rakouský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
jako	jako	k8xS,k8xC
dobrovolník	dobrovolník	k1gMnSc1
a	a	k8xC
v	v	k7c6
jejím	její	k3xOp3gInSc6
průběhu	průběh	k1gInSc6
získal	získat	k5eAaPmAgMnS
na	na	k7c6
východní	východní	k2eAgFnSc6d1
frontě	fronta	k1gFnSc6
několik	několik	k4yIc4
medailí	medaile	k1gFnPc2
za	za	k7c4
statečnost	statečnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
válce	válka	k1gFnSc6
pokračoval	pokračovat	k5eAaImAgInS
v	v	k7c6
bádání	bádání	k1gNnSc6
a	a	k8xC
roku	rok	k1gInSc6
1921	#num#	k4
vydal	vydat	k5eAaPmAgInS
s	s	k7c7
Russellovou	Russellův	k2eAgFnSc7d1
pomocí	pomoc	k1gFnSc7
Logisch-philosophische	Logisch-philosophisch	k1gInSc2
Abhandlung	Abhandlung	k1gInSc1
(	(	kIx(
<g/>
později	pozdě	k6eAd2
známé	známý	k2eAgNnSc1d1
jako	jako	k8xS,k8xC
Tractatus	Tractatus	k1gMnSc1
Logico-Philosophicus	Logico-Philosophicus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jediné	jediný	k2eAgNnSc1d1
dílo	dílo	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
vyšlo	vyjít	k5eAaPmAgNnS
za	za	k7c2
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
života	život	k1gInSc2
(	(	kIx(
<g/>
Traktát	traktát	k1gInSc1
byl	být	k5eAaImAgInS
napsán	napsat	k5eAaPmNgInS,k5eAaBmNgInS
již	již	k6eAd1
roku	rok	k1gInSc2
1918	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
z	z	k7c2
důvodů	důvod	k1gInPc2
válečných	válečný	k2eAgInPc2d1
a	a	k8xC
následně	následně	k6eAd1
kvůli	kvůli	k7c3
problémům	problém	k1gInPc3
při	při	k7c6
shánění	shánění	k1gNnSc6
nakladatele	nakladatel	k1gMnSc2
se	se	k3xPyFc4
vydání	vydání	k1gNnSc1
o	o	k7c4
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
zdrželo	zdržet	k5eAaPmAgNnS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
něm	on	k3xPp3gNnSc6
<g/>
,	,	kIx,
podle	podle	k7c2
svého	svůj	k3xOyFgInSc2
názoru	názor	k1gInSc2
<g/>
,	,	kIx,
vyřešil	vyřešit	k5eAaPmAgMnS
všechny	všechen	k3xTgInPc4
problémy	problém	k1gInPc4
filosofie	filosofie	k1gFnSc2
a	a	k8xC
stáhl	stáhnout	k5eAaPmAgInS
se	se	k3xPyFc4
do	do	k7c2
ústraní	ústraní	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Několik	několik	k4yIc4
let	léto	k1gNnPc2
působil	působit	k5eAaImAgMnS
jako	jako	k9
učitel	učitel	k1gMnSc1
na	na	k7c6
několika	několik	k4yIc6
venkovských	venkovský	k2eAgFnPc6d1
základních	základní	k2eAgFnPc6d1
školách	škola	k1gFnPc6
(	(	kIx(
<g/>
v	v	k7c6
Trattenbachu	Trattenbach	k1gInSc6
<g/>
,	,	kIx,
Puchbergu	Puchberg	k1gInSc6
am	am	k?
Schneeberg	Schneeberg	k1gInSc1
<g/>
,	,	kIx,
Otterthalu	Otterthala	k1gFnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
mezi	mezi	k7c7
ním	on	k3xPp3gMnSc7
a	a	k8xC
rodiči	rodič	k1gMnPc7
žáků	žák	k1gMnPc2
však	však	k9
panovalo	panovat	k5eAaImAgNnS
napětí	napětí	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
v	v	k7c6
dubnu	duben	k1gInSc6
1926	#num#	k4
uhodil	uhodit	k5eAaPmAgMnS
do	do	k7c2
hlavy	hlava	k1gFnSc2
jedenáctiletého	jedenáctiletý	k2eAgMnSc2d1
žáka	žák	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
pak	pak	k6eAd1
upadl	upadnout	k5eAaPmAgMnS
do	do	k7c2
bezvědomí	bezvědomí	k1gNnSc2
<g/>
,	,	kIx,
podal	podat	k5eAaPmAgInS
Wittgenstein	Wittgenstein	k1gInSc1
u	u	k7c2
okresního	okresní	k2eAgMnSc2d1
školního	školní	k2eAgMnSc2d1
inspektora	inspektor	k1gMnSc2
žádost	žádost	k1gFnSc4
o	o	k7c4
propuštění	propuštění	k1gNnSc4
dříve	dříve	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
proti	proti	k7c3
němu	on	k3xPp3gInSc3
mohly	moct	k5eAaImAgFnP
být	být	k5eAaImF
podniknuty	podniknout	k5eAaPmNgInP
oficiální	oficiální	k2eAgInPc1d1
kroky	krok	k1gInPc1
(	(	kIx(
<g/>
žákův	žákův	k2eAgMnSc1d1
otčím	otčím	k1gMnSc1
žádal	žádat	k5eAaImAgMnS
jeho	jeho	k3xOp3gNnSc4
uvěznění	uvěznění	k1gNnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cítil	cítit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
selhal	selhat	k5eAaPmAgMnS
jako	jako	k9
učitel	učitel	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
několik	několik	k4yIc4
měsíců	měsíc	k1gInPc2
pak	pak	k6eAd1
pracoval	pracovat	k5eAaImAgMnS
jako	jako	k8xC,k8xS
pomocný	pomocný	k2eAgMnSc1d1
zahradník	zahradník	k1gMnSc1
v	v	k7c6
klášteře	klášter	k1gInSc6
v	v	k7c6
Hütteldorfu	Hütteldorf	k1gInSc6
u	u	k7c2
Vídně	Vídeň	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
bydlel	bydlet	k5eAaImAgMnS
v	v	k7c6
zahradníkově	zahradníkův	k2eAgFnSc6d1
kůlně	kůlna	k1gFnSc6
na	na	k7c6
nářadí	nářadí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
také	také	k9
zvažoval	zvažovat	k5eAaImAgMnS
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
nikoli	nikoli	k9
poprvé	poprvé	k6eAd1
<g/>
,	,	kIx,
zda	zda	k8xS
má	mít	k5eAaImIp3nS
vstoupit	vstoupit	k5eAaPmF
do	do	k7c2
kláštera	klášter	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
rozhovoru	rozhovor	k1gInSc2
na	na	k7c4
toto	tento	k3xDgNnSc4
téma	téma	k1gNnSc4
mu	on	k3xPp3gMnSc3
tamní	tamní	k2eAgMnSc1d1
opat	opat	k1gMnSc1
poradil	poradit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
klášterní	klášterní	k2eAgInSc1d1
život	život	k1gInSc1
asi	asi	k9
není	být	k5eNaImIp3nS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yRnSc4,k3yInSc4
hledal	hledat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1929	#num#	k4
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
do	do	k7c2
Cambridge	Cambridge	k1gFnSc2
na	na	k7c4
Trinity	Trinit	k2eAgFnPc4d1
College	Colleg	k1gFnPc4
k	k	k7c3
filosofické	filosofický	k2eAgFnSc3d1
práci	práce	k1gFnSc3
a	a	k8xC
výuce	výuka	k1gFnSc3
studentů	student	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
v	v	k7c6
přátelské	přátelský	k2eAgFnSc6d1
atmoféře	atmoféra	k1gFnSc6
před	před	k7c7
komisí	komise	k1gFnSc7
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
předsedal	předsedat	k5eAaImAgMnS
Russell	Russell	k1gMnSc1
<g/>
,	,	kIx,
obhájil	obhájit	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
Tractatus	Tractatus	k1gInSc4
jako	jako	k8xS,k8xC
disertační	disertační	k2eAgFnSc4d1
práci	práce	k1gFnSc4
a	a	k8xC
získal	získat	k5eAaPmAgInS
titul	titul	k1gInSc4
PhD	PhD	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1939	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
Cambridgi	Cambridge	k1gFnSc6
stal	stát	k5eAaPmAgMnS
profesorem	profesor	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
pracoval	pracovat	k5eAaImAgMnS
v	v	k7c6
nemocnici	nemocnice	k1gFnSc6
a	a	k8xC
jako	jako	k8xC,k8xS
technický	technický	k2eAgMnSc1d1
pracovník	pracovník	k1gMnSc1
v	v	k7c6
Newcastlu	Newcastl	k1gInSc6
<g/>
,	,	kIx,
po	po	k7c6
jejím	její	k3xOp3gInSc6
konci	konec	k1gInSc6
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
na	na	k7c4
univerzitu	univerzita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1947	#num#	k4
rezignoval	rezignovat	k5eAaBmAgInS
na	na	k7c4
akademickou	akademický	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
a	a	k8xC
plně	plně	k6eAd1
se	se	k3xPyFc4
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
psaní	psaní	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
1949	#num#	k4
vznikla	vzniknout	k5eAaPmAgFnS
většina	většina	k1gFnSc1
materiálu	materiál	k1gInSc2
posmrtně	posmrtně	k6eAd1
vydaného	vydaný	k2eAgMnSc4d1
jako	jako	k9
Philosophische	Philosophische	k1gFnSc1
Untersuchungen	Untersuchungen	k1gInSc1
(	(	kIx(
<g/>
Filosofická	filosofický	k2eAgNnPc1d1
zkoumání	zkoumání	k1gNnPc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
toto	tento	k3xDgNnSc1
dílo	dílo	k1gNnSc1
je	být	k5eAaImIp3nS
pokládáno	pokládat	k5eAaImNgNnS
za	za	k7c4
Wittgensteinovu	Wittgensteinův	k2eAgFnSc4d1
nejzávažnější	závažný	k2eAgFnSc4d3
práci	práce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posledních	poslední	k2eAgNnPc2d1
několik	několik	k4yIc4
let	léto	k1gNnPc2
trávil	trávit	k5eAaImAgMnS
usilovnou	usilovný	k2eAgFnSc7d1
prací	práce	k1gFnSc7
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
,	,	kIx,
Oxfordu	Oxford	k1gInSc6
a	a	k8xC
v	v	k7c6
Cambridgi	Cambridge	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
v	v	k7c6
dubnu	duben	k1gInSc6
1951	#num#	k4
zemřel	zemřít	k5eAaPmAgMnS
na	na	k7c4
rakovinu	rakovina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Poznámkový	poznámkový	k2eAgInSc1d1
sešit	sešit	k1gInSc1
Ludwiga	Ludwig	k1gMnSc2
Wittgensteina	Wittgenstein	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
1914	#num#	k4
</s>
<s>
Tractatus	Tractatus	k1gMnSc1
logico-philosophicus	logico-philosophicus	k1gMnSc1
je	být	k5eAaImIp3nS
jediné	jediný	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
vydané	vydaný	k2eAgNnSc4d1
během	během	k7c2
W.	W.	kA
života	život	k1gInSc2
<g/>
;	;	kIx,
sám	sám	k3xTgInSc1
Wittgenstein	Wittgenstein	k1gInSc1
v	v	k7c6
předmluvě	předmluva	k1gFnSc6
napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
hlavních	hlavní	k2eAgFnPc2d1
dvou	dva	k4xCgFnPc2
částí	část	k1gFnPc2
–	–	k?
za	za	k7c4
prvé	prvý	k4xOgNnSc4
<g/>
:	:	kIx,
myšlenek	myšlenka	k1gFnPc2
v	v	k7c6
něm	on	k3xPp3gInSc6
vyjádřených	vyjádřený	k2eAgInPc2d1
<g/>
,	,	kIx,
za	za	k7c4
druhé	druhý	k4xOgNnSc4
<g/>
:	:	kIx,
ukázáním	ukázání	k1gNnSc7
jak	jak	k8xC,k8xS
málo	málo	k6eAd1
se	se	k3xPyFc4
dosáhne	dosáhnout	k5eAaPmIp3nS
řešením	řešení	k1gNnSc7
podobných	podobný	k2eAgInPc2d1
problémů	problém	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
z	z	k7c2
nejdůležitějších	důležitý	k2eAgFnPc2d3
myšlenek	myšlenka	k1gFnPc2
je	být	k5eAaImIp3nS
vyjádřena	vyjádřit	k5eAaPmNgFnS
v	v	k7c6
oddílu	oddíl	k1gInSc6
4.003	4.003	k4
–	–	k?
„	„	k?
<g/>
Většina	většina	k1gFnSc1
vět	věta	k1gFnPc2
a	a	k8xC
otázek	otázka	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
byly	být	k5eAaImAgFnP
napsány	napsat	k5eAaBmNgFnP,k5eAaPmNgFnP
o	o	k7c6
filosofických	filosofický	k2eAgFnPc6d1
věcech	věc	k1gFnPc6
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
nepravdivá	pravdivý	k2eNgFnSc1d1
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
nesmyslná	smyslný	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nemůžeme	moct	k5eNaImIp1nP
proto	proto	k8xC
vůbec	vůbec	k9
takové	takový	k3xDgFnPc4
otázky	otázka	k1gFnPc4
zodpovědět	zodpovědět	k5eAaPmF
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
jen	jen	k9
prokázat	prokázat	k5eAaPmF
jejich	jejich	k3xOp3gFnSc4
nesmyslnost	nesmyslnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
otázek	otázka	k1gFnPc2
a	a	k8xC
vět	věta	k1gFnPc2
filosofů	filosof	k1gMnPc2
spočívá	spočívat	k5eAaImIp3nS
na	na	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
nerozumíme	rozumět	k5eNaImIp1nP
naší	náš	k3xOp1gFnSc3
jazykové	jazykový	k2eAgFnSc3d1
logice	logika	k1gFnSc3
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
takový	takový	k3xDgInSc1
druh	druh	k1gInSc1
otázek	otázka	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
zda	zda	k8xS
je	být	k5eAaImIp3nS
dobro	dobro	k1gNnSc4
více	hodně	k6eAd2
<g/>
,	,	kIx,
či	či	k8xC
méně	málo	k6eAd2
identické	identický	k2eAgFnPc1d1
než	než	k8xS
krásno	krásno	k1gNnSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
A	a	k9
nelze	lze	k6eNd1
se	se	k3xPyFc4
divit	divit	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
nejhlubší	hluboký	k2eAgInPc1d3
problémy	problém	k1gInPc1
vlastně	vlastně	k9
žádné	žádný	k3yNgInPc1
problémy	problém	k1gInPc1
nejsou	být	k5eNaImIp3nP
<g/>
.	.	kIx.
<g/>
“	“	k?
a	a	k8xC
4.003	4.003	k4
<g/>
1	#num#	k4
–	–	k?
„	„	k?
<g/>
Celá	celý	k2eAgFnSc1d1
filosofie	filosofie	k1gFnSc1
je	být	k5eAaImIp3nS
‚	‚	k?
<g/>
kritikou	kritika	k1gFnSc7
jazyka	jazyk	k1gInSc2
<g/>
‘	‘	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Russelovi	Russelův	k2eAgMnPc1d1
vděčíme	vděčit	k5eAaImIp1nP
za	za	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
ukázal	ukázat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
zdánlivá	zdánlivý	k2eAgFnSc1d1
logická	logický	k2eAgFnSc1d1
forma	forma	k1gFnSc1
věty	věta	k1gFnSc2
nemusí	muset	k5eNaImIp3nS
být	být	k5eAaImF
její	její	k3xOp3gFnSc7
skutečnou	skutečný	k2eAgFnSc7d1
formou	forma	k1gFnSc7
<g/>
.	.	kIx.
<g/>
“	“	k?
Z	z	k7c2
dalších	další	k2eAgInPc2d1
zajímavých	zajímavý	k2eAgInPc2d1
výroků	výrok	k1gInPc2
stojí	stát	k5eAaImIp3nS
za	za	k7c4
uvedení	uvedení	k1gNnSc4
ještě	ještě	k9
minimálně	minimálně	k6eAd1
oddíl	oddíl	k1gInSc1
5.6	5.6	k4
–	–	k?
„	„	k?
<g/>
Hranice	hranice	k1gFnSc1
mého	můj	k3xOp1gInSc2
jazyka	jazyk	k1gInSc2
znamenají	znamenat	k5eAaImIp3nP
hranice	hranice	k1gFnPc4
mého	můj	k3xOp1gInSc2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
<g/>
“	“	k?
a	a	k8xC
samozřejmě	samozřejmě	k6eAd1
poslední	poslední	k2eAgInSc4d1
oddíl	oddíl	k1gInSc4
7	#num#	k4
tvořený	tvořený	k2eAgInSc4d1
jen	jen	k8xS
jednou	jeden	k4xCgFnSc7
větou	věta	k1gFnSc7
<g/>
:	:	kIx,
„	„	k?
<g/>
O	o	k7c6
čem	co	k3yInSc6,k3yQnSc6,k3yRnSc6
nelze	lze	k6eNd1
mluvit	mluvit	k5eAaImF
<g/>
,	,	kIx,
o	o	k7c6
tom	ten	k3xDgNnSc6
se	se	k3xPyFc4
musí	muset	k5eAaImIp3nS
mlčet	mlčet	k5eAaImF
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Filosofická	filosofický	k2eAgNnPc4d1
zkoumání	zkoumání	k1gNnSc4
–	–	k?
zde	zde	k6eAd1
zavádí	zavádět	k5eAaImIp3nS
pojem	pojem	k1gInSc1
řečové	řečový	k2eAgFnSc2d1
hry	hra	k1gFnSc2
a	a	k8xC
rozvíjí	rozvíjet	k5eAaImIp3nS
myšlenku	myšlenka	k1gFnSc4
filosofie	filosofie	k1gFnSc2
jako	jako	k8xC,k8xS
objasňování	objasňování	k1gNnSc2
problémů	problém	k1gInPc2
jazyka	jazyk	k1gInSc2
(	(	kIx(
<g/>
„	„	k?
<g/>
Význam	význam	k1gInSc1
nějakého	nějaký	k3yIgNnSc2
slova	slovo	k1gNnSc2
je	být	k5eAaImIp3nS
způsob	způsob	k1gInSc1
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
použití	použití	k1gNnSc2
v	v	k7c6
řeči	řeč	k1gFnSc6
<g/>
.	.	kIx.
<g/>
“	“	k?
§	§	k?
43	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řečové	řečový	k2eAgFnPc4d1
hry	hra	k1gFnPc4
je	být	k5eAaImIp3nS
Wittgensteinův	Wittgensteinův	k2eAgInSc1d1
pojem	pojem	k1gInSc1
<g/>
,	,	kIx,
zdůrazňující	zdůrazňující	k2eAgFnSc4d1
aktuální	aktuální	k2eAgFnSc4d1
stránku	stránka	k1gFnSc4
řeči	řeč	k1gFnSc2
a	a	k8xC
mluvení	mluvení	k1gNnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
se	se	k3xPyFc4
řídí	řídit	k5eAaImIp3nP
různými	různý	k2eAgNnPc7d1
pravidly	pravidlo	k1gNnPc7
například	například	k6eAd1
v	v	k7c6
rodině	rodina	k1gFnSc6
<g/>
,	,	kIx,
ve	v	k7c6
vědeckém	vědecký	k2eAgNnSc6d1
užití	užití	k1gNnSc6
apod.	apod.	kA
Každá	každý	k3xTgFnSc1
promluva	promluva	k1gFnSc1
sleduje	sledovat	k5eAaImIp3nS
nějaký	nějaký	k3yIgInSc4
cíl	cíl	k1gInSc4
<g/>
,	,	kIx,
nestačí	stačit	k5eNaBmIp3nS
tedy	tedy	k9
zkoumat	zkoumat	k5eAaImF
jen	jen	k9
její	její	k3xOp3gFnSc7
logickou	logický	k2eAgFnSc7d1
stavbu	stavba	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Modrá	modrý	k2eAgFnSc1d1
a	a	k8xC
Hnědá	hnědý	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
–	–	k?
výbor	výbor	k1gInSc1
z	z	k7c2
přednášek	přednáška	k1gFnPc2
v	v	k7c4
Cambridge	Cambridge	k1gFnPc4
<g/>
,	,	kIx,
asi	asi	k9
nejpřístupnější	přístupný	k2eAgNnSc4d3
vyjádření	vyjádření	k1gNnSc4
Wittgensteinových	Wittgensteinův	k2eAgFnPc2d1
myšlenek	myšlenka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
K	k	k7c3
čemu	co	k3yRnSc3,k3yInSc3,k3yQnSc3
jsou	být	k5eAaImIp3nP
definice	definice	k1gFnSc1
<g/>
,	,	kIx,
když	když	k8xS
stejně	stejně	k6eAd1
vedou	vést	k5eAaImIp3nP
jen	jen	k9
k	k	k7c3
dalším	další	k2eAgInPc3d1
nedefinovaným	definovaný	k2eNgInPc3d1
pojmům	pojem	k1gInPc3
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
(	(	kIx(
<g/>
Blue	Blue	k1gFnSc1
book	book	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
26	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Frazerův	Frazerův	k2eAgInSc4d1
výklad	výklad	k1gInSc4
magických	magický	k2eAgInPc2d1
a	a	k8xC
náboženských	náboženský	k2eAgInPc2d1
názorů	názor	k1gInPc2
lidstva	lidstvo	k1gNnSc2
není	být	k5eNaImIp3nS
uspokojivý	uspokojivý	k2eAgInSc1d1
<g/>
;	;	kIx,
představuje	představovat	k5eAaImIp3nS
je	on	k3xPp3gNnSc4
totiž	totiž	k9
jako	jako	k8xS,k8xC
omyly	omyl	k1gInPc1
<g/>
.	.	kIx.
<g/>
“	“	k?
(	(	kIx(
<g/>
Blue	Blue	k1gFnSc1
book	book	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
119	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
On	on	k3xPp3gMnSc1
Certainty	Certaint	k1gInPc1
–	–	k?
práce	práce	k1gFnSc2
z	z	k7c2
posledních	poslední	k2eAgNnPc2d1
let	léto	k1gNnPc2
(	(	kIx(
<g/>
z	z	k7c2
velké	velký	k2eAgFnSc2d1
části	část	k1gFnSc2
odpověď	odpověď	k1gFnSc1
na	na	k7c4
práci	práce	k1gFnSc4
G.	G.	kA
E.	E.	kA
Moora	Moora	k1gMnSc1
In	In	k1gMnSc1
Defense	defense	k1gFnSc2
of	of	k?
Common	Common	k1gInSc1
Sense	Sense	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Poznání	poznání	k1gNnSc1
je	být	k5eAaImIp3nS
nakonec	nakonec	k6eAd1
založeno	založit	k5eAaPmNgNnS
na	na	k7c4
uznání	uznání	k1gNnSc4
<g/>
.	.	kIx.
<g/>
“	“	k?
(	(	kIx(
<g/>
On	on	k3xPp3gMnSc1
certainty	certainta	k1gFnSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
378	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Rozličné	rozličný	k2eAgFnPc1d1
poznámky	poznámka	k1gFnPc1
(	(	kIx(
<g/>
Vermischte	Vermischt	k1gInSc5
Bemerkungen	Bemerkungen	k1gInSc4
<g/>
)	)	kIx)
–	–	k?
poznámky	poznámka	k1gFnPc4
z	z	k7c2
pozůstalosti	pozůstalost	k1gFnSc2
(	(	kIx(
<g/>
sestavil	sestavit	k5eAaPmAgMnS
Georg	Georg	k1gMnSc1
Henrik	Henrik	k1gMnSc1
von	von	k1gInSc4
Wright	Wright	k2eAgInSc4d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Člověk	člověk	k1gMnSc1
se	se	k3xPyFc4
musí	muset	k5eAaImIp3nS
probudit	probudit	k5eAaPmF
k	k	k7c3
údivu	údiv	k1gInSc3
–	–	k?
a	a	k8xC
národy	národ	k1gInPc1
možná	možná	k9
také	také	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Věda	věda	k1gFnSc1
je	být	k5eAaImIp3nS
způsob	způsob	k1gInSc4
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
je	být	k5eAaImIp3nS
zase	zase	k9
poslat	poslat	k5eAaPmF
spát	spát	k5eAaImF
<g/>
.	.	kIx.
<g/>
“	“	k?
(	(	kIx(
<g/>
Culture	Cultur	k1gMnSc5
and	and	k?
value	value	k1gNnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
České	český	k2eAgInPc1d1
překlady	překlad	k1gInPc1
</s>
<s>
Tractatus	Tractatus	k1gMnSc1
logico-philosophicus	logico-philosophicus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přeložil	přeložit	k5eAaPmAgMnS
Jiří	Jiří	k1gMnSc1
Fiala	Fiala	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Institut	institut	k1gInSc1
pro	pro	k7c4
středoevropskou	středoevropský	k2eAgFnSc4d1
kulturu	kultura	k1gFnSc4
a	a	k8xC
politiku	politika	k1gFnSc4
a	a	k8xC
Svoboda-Libertas	Svoboda-Libertas	k1gInSc4
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Tractatus	Tractatus	k1gMnSc1
logico-philosophicus	logico-philosophicus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přeložil	přeložit	k5eAaPmAgMnS
Petr	Petr	k1gMnSc1
Glombíček	Glombíček	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
OIKOYMENH	OIKOYMENH	kA
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Filosofická	filosofický	k2eAgNnPc1d1
zkoumání	zkoumání	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přeložil	přeložit	k5eAaPmAgMnS
Jiří	Jiří	k1gMnSc1
Pechar	Pechar	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Filosofický	filosofický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Filosofická	filosofický	k2eAgNnPc1d1
zkoumání	zkoumání	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přeložil	přeložit	k5eAaPmAgMnS
Jiří	Jiří	k1gMnSc1
Pechar	Pechar	k1gMnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
upravené	upravený	k2eAgNnSc1d1
vydání	vydání	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Filosofia	Filosofia	k1gFnSc1
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Rozličné	rozličný	k2eAgFnPc1d1
poznámky	poznámka	k1gFnPc1
<g/>
:	:	kIx,
výbor	výbor	k1gInSc1
z	z	k7c2
pozůstalosti	pozůstalost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přeložil	přeložit	k5eAaPmAgMnS
Marek	Marek	k1gMnSc1
Nekula	Nekula	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
a	a	k8xC
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Předběžné	předběžný	k2eAgFnPc1d1
studie	studie	k1gFnPc1
k	k	k7c3
Filosofickým	filosofický	k2eAgNnPc3d1
zkoumáním	zkoumání	k1gNnPc3
obecně	obecně	k6eAd1
známé	známý	k2eAgFnPc1d1
jako	jako	k8xS,k8xC
Modrá	modrý	k2eAgFnSc1d1
a	a	k8xC
Hnědá	hnědý	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přeložil	přeložit	k5eAaPmAgMnS
Petr	Petr	k1gMnSc1
Glombíček	Glombíček	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Filosofia	Filosofia	k1gFnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Modrá	modrý	k2eAgFnSc1d1
a	a	k8xC
Hnědá	hnědý	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přeložil	přeložit	k5eAaPmAgMnS
Petr	Petr	k1gMnSc1
Glombíček	Glombíček	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Filosofia	Filosofia	k1gFnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Poznámky	poznámka	k1gFnPc1
o	o	k7c6
barvách	barva	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přeložil	přeložit	k5eAaPmAgMnS
Ondřej	Ondřej	k1gMnSc1
Beran	Beran	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Filosofia	Filosofia	k1gFnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
O	o	k7c6
jistotě	jistota	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přeložil	přeložit	k5eAaPmAgMnS
Vlastimil	Vlastimil	k1gMnSc1
Zátka	zátka	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
SOKOL	Sokol	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malá	malý	k2eAgFnSc1d1
filosofie	filosofie	k1gFnSc1
člověka	člověk	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovník	slovník	k1gInSc4
filosofických	filosofický	k2eAgInPc2d1
pojmů	pojem	k1gInPc2
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2007	#num#	k4
ISBN	ISBN	kA
978-80-7021-884-6	978-80-7021-884-6	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
H.	H.	kA
J.	J.	kA
Störig	Störiga	k1gFnPc2
<g/>
,	,	kIx,
Malé	Malé	k2eAgFnPc1d1
dějiny	dějiny	k1gFnPc1
filosofie	filosofie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Zvon	zvon	k1gInSc1
1996	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
468	#num#	k4
<g/>
nn	nn	k?
<g/>
..	..	k?
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Ludwig	Ludwig	k1gMnSc1
Wittgenstein	Wittgenstein	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Osoba	osoba	k1gFnSc1
Ludwig	Ludwig	k1gMnSc1
Wittgenstein	Wittgenstein	k1gMnSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Ludwig	Ludwig	k1gMnSc1
Wittgenstein	Wittgenstein	k1gMnSc1
</s>
<s>
Časopis	časopis	k1gInSc1
Reflex	reflex	k1gInSc1
<g/>
:	:	kIx,
článek	článek	k1gInSc1
Ludwig	Ludwig	k1gMnSc1
Wittgenstein	Wittgenstein	k1gMnSc1
z	z	k7c2
čísla	číslo	k1gNnSc2
17	#num#	k4
<g/>
/	/	kIx~
<g/>
2006	#num#	k4
</s>
<s>
Danica	Danica	k6eAd1
Slouková	Sloukový	k2eAgFnSc1d1
<g/>
:	:	kIx,
L.	L.	kA
Wittgenstein	Wittgenstein	k1gMnSc1
–	–	k?
Metoda	metoda	k1gFnSc1
s	s	k7c7
„	„	k?
<g/>
vyloučením	vyloučení	k1gNnSc7
problémů	problém	k1gInPc2
(	(	kIx(
<g/>
záhad	záhada	k1gFnPc2
<g/>
)	)	kIx)
<g/>
“	“	k?
versus	versus	k7c1
metoda	metoda	k1gFnSc1
jako	jako	k8xS,k8xC
„	„	k?
<g/>
terapie	terapie	k1gFnSc1
zapletenosti	zapletenost	k1gFnSc2
<g/>
“	“	k?
zvané	zvaný	k2eAgNnSc1d1
problém	problém	k1gInSc1
</s>
<s>
Ladislav	Ladislav	k1gMnSc1
Čumba	Čumba	k1gMnSc1
<g/>
:	:	kIx,
Wittgensteinova	Wittgensteinův	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
faktů	fakt	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Tractatus	Tractatus	k1gMnSc1
logico-philosophicus	logico-philosophicus	k1gMnSc1
—	—	k?
původní	původní	k2eAgFnSc1d1
německá	německý	k2eAgFnSc1d1
verze	verze	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Philosophische	Philosophische	k1gFnSc1
Unterschungen	Unterschungen	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Tractatus	Tractatus	k1gInSc1
logico-philosophicus	logico-philosophicus	k1gInSc1
—	—	k?
anglický	anglický	k2eAgInSc1d1
překlad	překlad	k1gInSc1
(	(	kIx(
<g/>
projekt	projekt	k1gInSc1
Gutenberg	Gutenberg	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Blue	Blue	k1gFnSc1
book	book	k1gInSc1
–	–	k?
na	na	k7c4
Geocities	Geocities	k1gInSc4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
19990009220	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118634313	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2099	#num#	k4
9171	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79032058	#num#	k4
|	|	kIx~
ULAN	ULAN	kA
<g/>
:	:	kIx,
500270292	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
24609378	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79032058	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Filosofie	filosofie	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
</s>
