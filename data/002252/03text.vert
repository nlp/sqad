<s>
Finská	finský	k2eAgFnSc1d1	finská
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
zastarale	zastarale	k6eAd1	zastarale
Čuchonsko	Čuchonsko	k1gNnSc1	Čuchonsko
<g/>
,	,	kIx,	,
finsky	finsky	k6eAd1	finsky
Suomen	Suomen	k2eAgMnSc1d1	Suomen
Tasavalta	Tasavalta	k1gMnSc1	Tasavalta
<g/>
,	,	kIx,	,
švédsky	švédsky	k6eAd1	švédsky
Republiken	Republiken	k2eAgInSc1d1	Republiken
Finland	Finland	k1gInSc1	Finland
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
severská	severský	k2eAgFnSc1d1	severská
země	země	k1gFnSc1	země
v	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
omývá	omývat	k5eAaImIp3nS	omývat
Baltské	baltský	k2eAgNnSc1d1	Baltské
moře	moře	k1gNnSc1	moře
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
<g/>
,	,	kIx,	,
Finský	finský	k2eAgInSc1d1	finský
záliv	záliv	k1gInSc1	záliv
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
a	a	k8xC	a
Botnický	botnický	k2eAgInSc1d1	botnický
záliv	záliv	k1gInSc1	záliv
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
<s>
Finsko	Finsko	k1gNnSc1	Finsko
sousedí	sousedit	k5eAaImIp3nS	sousedit
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
Švédskem	Švédsko	k1gNnSc7	Švédsko
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
a	a	k8xC	a
Norskem	Norsko	k1gNnSc7	Norsko
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
má	mít	k5eAaImIp3nS	mít
společnou	společný	k2eAgFnSc4d1	společná
hranici	hranice	k1gFnSc4	hranice
navíc	navíc	k6eAd1	navíc
s	s	k7c7	s
Estonskem	Estonsko	k1gNnSc7	Estonsko
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c4	pod
finskou	finský	k2eAgFnSc4d1	finská
suverenitu	suverenita	k1gFnSc4	suverenita
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
souostroví	souostroví	k1gNnSc1	souostroví
Å	Å	k?	Å
na	na	k7c4	na
jihozápad	jihozápad	k1gInSc4	jihozápad
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
však	však	k9	však
má	mít	k5eAaImIp3nS	mít
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
autonomii	autonomie	k1gFnSc4	autonomie
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Finska	Finsko	k1gNnSc2	Finsko
<g/>
.	.	kIx.	.
</s>
<s>
Archeologické	archeologický	k2eAgInPc1d1	archeologický
nálezy	nález	k1gInPc1	nález
prokazují	prokazovat	k5eAaImIp3nP	prokazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dnes	dnes	k6eAd1	dnes
náleží	náležet	k5eAaImIp3nS	náležet
Finsku	Finsko	k1gNnSc3	Finsko
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
osídlena	osídlit	k5eAaPmNgFnS	osídlit
již	již	k6eAd1	již
v	v	k7c6	v
osmém	osmý	k4xOgInSc6	osmý
tisíciletí	tisíciletí	k1gNnSc6	tisíciletí
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
během	během	k7c2	během
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
stáhl	stáhnout	k5eAaPmAgInS	stáhnout
ledovec	ledovec	k1gInSc1	ledovec
po	po	k7c6	po
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
ledové	ledový	k2eAgFnPc1d1	ledová
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
první	první	k4xOgMnPc1	první
obyvatelé	obyvatel	k1gMnPc1	obyvatel
byli	být	k5eAaImAgMnP	být
především	především	k9	především
lovci	lovec	k1gMnPc1	lovec
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
5000	[number]	k4	5000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
výraznějšímu	výrazný	k2eAgNnSc3d2	výraznější
oteplení	oteplení	k1gNnSc3	oteplení
a	a	k8xC	a
zvlhčení	zvlhčení	k1gNnSc3	zvlhčení
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
sídlišť	sídliště	k1gNnPc2	sídliště
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
objevilo	objevit	k5eAaPmAgNnS	objevit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
kulturou	kultura	k1gFnSc7	kultura
hřebenové	hřebenový	k2eAgFnSc2d1	hřebenová
keramiky	keramika	k1gFnSc2	keramika
hrnčířství	hrnčířství	k1gNnSc2	hrnčířství
<g/>
.	.	kIx.	.
</s>
<s>
Existenci	existence	k1gFnSc4	existence
čilého	čilý	k2eAgInSc2d1	čilý
výměnného	výměnný	k2eAgInSc2d1	výměnný
systému	systém	k1gInSc2	systém
dokládají	dokládat	k5eAaImIp3nP	dokládat
nálezy	nález	k1gInPc1	nález
osinku	osinek	k1gInSc2	osinek
a	a	k8xC	a
mastku	mastek	k1gInSc2	mastek
z	z	k7c2	z
východního	východní	k2eAgNnSc2d1	východní
Finska	Finsko	k1gNnSc2	Finsko
<g/>
,	,	kIx,	,
křemene	křemen	k1gInSc2	křemen
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
a	a	k8xC	a
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
dlát	dláto	k1gNnPc2	dláto
od	od	k7c2	od
Oněžského	oněžský	k2eAgNnSc2d1	Oněžské
jezera	jezero	k1gNnSc2	jezero
a	a	k8xC	a
hrotů	hrot	k1gInPc2	hrot
oštěpů	oštěp	k1gInPc2	oštěp
ze	z	k7c2	z
severní	severní	k2eAgFnSc2d1	severní
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
soudí	soudit	k5eAaImIp3nS	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mluvčí	mluvčí	k1gMnSc1	mluvčí
ugrofinského	ugrofinský	k2eAgInSc2d1	ugrofinský
jazyka	jazyk	k1gInSc2	jazyk
přišli	přijít	k5eAaPmAgMnP	přijít
do	do	k7c2	do
Finska	Finsko	k1gNnSc2	Finsko
během	během	k7c2	během
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
již	již	k9	již
mezi	mezi	k7c7	mezi
prvními	první	k4xOgMnPc7	první
mezolitickými	mezolitický	k2eAgMnPc7d1	mezolitický
osadníky	osadník	k1gMnPc7	osadník
<g/>
.	.	kIx.	.
</s>
<s>
Příchod	příchod	k1gInSc1	příchod
kultury	kultura	k1gFnSc2	kultura
šňůrové	šňůrový	k2eAgFnSc2d1	šňůrová
keramiky	keramika	k1gFnSc2	keramika
(	(	kIx(	(
<g/>
také	také	k9	také
kultura	kultura	k1gFnSc1	kultura
sekeromlatů	sekeromlat	k1gInPc2	sekeromlat
<g/>
)	)	kIx)	)
do	do	k7c2	do
jižního	jižní	k2eAgNnSc2d1	jižní
Finska	Finsko	k1gNnSc2	Finsko
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
3200	[number]	k4	3200
až	až	k9	až
2500	[number]	k4	2500
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
počátek	počátek	k1gInSc4	počátek
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
lov	lov	k1gInSc1	lov
a	a	k8xC	a
rybolov	rybolov	k1gInSc1	rybolov
zůstal	zůstat	k5eAaPmAgInS	zůstat
důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
hospodaření	hospodaření	k1gNnSc2	hospodaření
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
v	v	k7c6	v
severních	severní	k2eAgFnPc6d1	severní
a	a	k8xC	a
východních	východní	k2eAgFnPc6d1	východní
částech	část	k1gFnPc6	část
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
i	i	k9	i
nadále	nadále	k6eAd1	nadále
vyskytovala	vyskytovat	k5eAaImAgFnS	vyskytovat
kultura	kultura	k1gFnSc1	kultura
hřebenové	hřebenový	k2eAgFnSc2d1	hřebenová
keramiky	keramika	k1gFnSc2	keramika
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
známky	známka	k1gFnPc1	známka
zemědělství	zemědělství	k1gNnSc2	zemědělství
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
období	období	k1gNnSc2	období
asi	asi	k9	asi
2300	[number]	k4	2300
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Výrazná	výrazný	k2eAgFnSc1d1	výrazná
změna	změna	k1gFnSc1	změna
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
napovídá	napovídat	k5eAaBmIp3nS	napovídat
příchodu	příchod	k1gInSc3	příchod
nového	nový	k2eAgNnSc2d1	nové
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
z	z	k7c2	z
Pobaltí	Pobaltí	k1gNnSc2	Pobaltí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
vyšší	vysoký	k2eAgFnSc7d2	vyšší
vrstvou	vrstva	k1gFnSc7	vrstva
a	a	k8xC	a
zřejmě	zřejmě	k6eAd1	zřejmě
i	i	k9	i
významně	významně	k6eAd1	významně
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
místní	místní	k2eAgInSc4d1	místní
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
bronzová	bronzový	k2eAgFnSc1d1	bronzová
(	(	kIx(	(
<g/>
1500	[number]	k4	1500
-	-	kIx~	-
500	[number]	k4	500
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
a	a	k8xC	a
doba	doba	k1gFnSc1	doba
železná	železný	k2eAgFnSc1d1	železná
(	(	kIx(	(
<g/>
500	[number]	k4	500
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
-	-	kIx~	-
1200	[number]	k4	1200
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
rozsáhlých	rozsáhlý	k2eAgInPc2d1	rozsáhlý
kontaktů	kontakt	k1gInPc2	kontakt
se	s	k7c7	s
Skandinávií	Skandinávie	k1gFnSc7	Skandinávie
<g/>
,	,	kIx,	,
severním	severní	k2eAgNnSc7d1	severní
Ruskem	Rusko	k1gNnSc7	Rusko
a	a	k8xC	a
Pobaltím	Pobaltí	k1gNnSc7	Pobaltí
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
500	[number]	k4	500
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
výrazně	výrazně	k6eAd1	výrazně
ochladilo	ochladit	k5eAaPmAgNnS	ochladit
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
značnému	značný	k2eAgInSc3d1	značný
úbytku	úbytek	k1gInSc3	úbytek
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Opětovný	opětovný	k2eAgInSc1d1	opětovný
rozmach	rozmach	k1gInSc1	rozmach
přišel	přijít	k5eAaPmAgInS	přijít
až	až	k9	až
s	s	k7c7	s
římskou	římský	k2eAgFnSc7d1	římská
dobou	doba	k1gFnSc7	doba
železnou	železný	k2eAgFnSc7d1	železná
po	po	k7c6	po
roce	rok	k1gInSc6	rok
50	[number]	k4	50
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
dříve	dříve	k6eAd2	dříve
spojována	spojovat	k5eAaImNgFnS	spojovat
s	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
Finů	Fin	k1gMnPc2	Fin
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
podporují	podporovat	k5eAaImIp3nP	podporovat
i	i	k9	i
lingvistické	lingvistický	k2eAgFnPc1d1	lingvistická
teorie	teorie	k1gFnPc1	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Novější	nový	k2eAgInPc1d2	novější
archeologické	archeologický	k2eAgInPc1d1	archeologický
nálezy	nález	k1gInPc1	nález
ale	ale	k8xC	ale
tuto	tento	k3xDgFnSc4	tento
hypotézu	hypotéza	k1gFnSc4	hypotéza
nijak	nijak	k6eAd1	nijak
nepotvrzují	potvrzovat	k5eNaImIp3nP	potvrzovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
doby	doba	k1gFnSc2	doba
železné	železný	k2eAgFnSc2d1	železná
žilo	žít	k5eAaImAgNnS	žít
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
asi	asi	k9	asi
50	[number]	k4	50
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
sedmisetletá	sedmisetletý	k2eAgFnSc1d1	sedmisetletá
příslušnost	příslušnost	k1gFnSc1	příslušnost
Finska	Finsko	k1gNnSc2	Finsko
k	k	k7c3	k
Švédskému	švédský	k2eAgNnSc3d1	švédské
království	království	k1gNnSc3	království
je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
spojována	spojovat	k5eAaImNgFnS	spojovat
s	s	k7c7	s
rokem	rok	k1gInSc7	rok
1154	[number]	k4	1154
a	a	k8xC	a
údajným	údajný	k2eAgInSc7d1	údajný
příchodem	příchod	k1gInSc7	příchod
křesťanství	křesťanství	k1gNnSc2	křesťanství
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
švédským	švédský	k2eAgMnSc7d1	švédský
králem	král	k1gMnSc7	král
Erikem	Erik	k1gMnSc7	Erik
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
archeologických	archeologický	k2eAgInPc2d1	archeologický
záznamů	záznam	k1gInPc2	záznam
ale	ale	k8xC	ale
bylo	být	k5eAaImAgNnS	být
mezi	mezi	k7c7	mezi
finskými	finský	k2eAgMnPc7d1	finský
pohany	pohan	k1gMnPc7	pohan
mnoho	mnoho	k6eAd1	mnoho
křesťanů	křesťan	k1gMnPc2	křesťan
již	již	k9	již
předtím	předtím	k6eAd1	předtím
<g/>
.	.	kIx.	.
</s>
<s>
Švédština	švédština	k1gFnSc1	švédština
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
dominantním	dominantní	k2eAgInSc7d1	dominantní
jazykem	jazyk	k1gInSc7	jazyk
administrativy	administrativa	k1gFnSc2	administrativa
a	a	k8xC	a
výuky	výuka	k1gFnSc2	výuka
<g/>
.	.	kIx.	.
</s>
<s>
Finština	finština	k1gFnSc1	finština
zůstala	zůstat	k5eAaPmAgFnS	zůstat
jazykem	jazyk	k1gInSc7	jazyk
obyčejných	obyčejný	k2eAgMnPc2d1	obyčejný
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
tiskla	tisknout	k5eAaImAgFnS	tisknout
náboženská	náboženský	k2eAgFnSc1d1	náboženská
literatura	literatura	k1gFnSc1	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1697	[number]	k4	1697
během	během	k7c2	během
malé	malý	k2eAgFnSc2d1	malá
doby	doba	k1gFnSc2	doba
ledové	ledový	k2eAgFnSc2d1	ledová
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
Finsko	Finsko	k1gNnSc4	Finsko
ničivý	ničivý	k2eAgInSc1d1	ničivý
hladomor	hladomor	k1gInSc1	hladomor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zahubil	zahubit	k5eAaPmAgInS	zahubit
až	až	k6eAd1	až
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1808	[number]	k4	1808
<g/>
-	-	kIx~	-
<g/>
1809	[number]	k4	1809
Finsko	Finsko	k1gNnSc4	Finsko
dobyly	dobýt	k5eAaPmAgFnP	dobýt
armády	armáda	k1gFnPc1	armáda
ruského	ruský	k2eAgMnSc2d1	ruský
cara	car	k1gMnSc2	car
Alexandra	Alexandr	k1gMnSc2	Alexandr
I.	I.	kA	I.
<g/>
.	.	kIx.	.
</s>
<s>
Ruský	ruský	k2eAgMnSc1d1	ruský
car	car	k1gMnSc1	car
<g/>
,	,	kIx,	,
vědom	vědom	k2eAgMnSc1d1	vědom
si	se	k3xPyFc3	se
vazeb	vazba	k1gFnPc2	vazba
na	na	k7c4	na
Švédsko	Švédsko	k1gNnSc4	Švédsko
a	a	k8xC	a
odlišné	odlišný	k2eAgFnSc2d1	odlišná
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
ustanovil	ustanovit	k5eAaPmAgInS	ustanovit
Finsko	Finsko	k1gNnSc4	Finsko
velkoknížectvím	velkoknížectví	k1gNnPc3	velkoknížectví
v	v	k7c6	v
personální	personální	k2eAgFnSc6d1	personální
unii	unie	k1gFnSc6	unie
s	s	k7c7	s
carským	carský	k2eAgNnSc7d1	carské
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nP	aby
ruština	ruština	k1gFnSc1	ruština
nahradila	nahradit	k5eAaPmAgFnS	nahradit
švédštinu	švédština	k1gFnSc4	švédština
a	a	k8xC	a
také	také	k9	také
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
zpřetrhaly	zpřetrhat	k5eAaPmAgFnP	zpřetrhat
citové	citový	k2eAgFnPc1d1	citová
vazby	vazba	k1gFnPc1	vazba
se	s	k7c7	s
Švédskem	Švédsko	k1gNnSc7	Švédsko
<g/>
,	,	kIx,	,
carský	carský	k2eAgInSc1d1	carský
dvůr	dvůr	k1gInSc1	dvůr
a	a	k8xC	a
finská	finský	k2eAgFnSc1d1	finská
vláda	vláda	k1gFnSc1	vláda
začaly	začít	k5eAaPmAgFnP	začít
prosazovat	prosazovat	k5eAaImF	prosazovat
finštinu	finština	k1gFnSc4	finština
<g/>
.	.	kIx.	.
</s>
<s>
Přidalo	přidat	k5eAaPmAgNnS	přidat
se	se	k3xPyFc4	se
také	také	k9	také
silné	silný	k2eAgNnSc1d1	silné
nacionalistické	nacionalistický	k2eAgNnSc1d1	nacionalistické
obrozenecké	obrozenecký	k2eAgNnSc1d1	obrozenecké
hnutí	hnutí	k1gNnSc1	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Milníkem	milník	k1gInSc7	milník
dalšího	další	k2eAgInSc2d1	další
vývoje	vývoj	k1gInSc2	vývoj
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
vydání	vydání	k1gNnSc1	vydání
národního	národní	k2eAgInSc2d1	národní
eposu	epos	k1gInSc2	epos
Kalevala	Kalevala	k1gFnSc2	Kalevala
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
vyšel	vyjít	k5eAaPmAgMnS	vyjít
poprvé	poprvé	k6eAd1	poprvé
roku	rok	k1gInSc2	rok
1833	[number]	k4	1833
jako	jako	k8xC	jako
tzv.	tzv.	kA	tzv.
Pra-Kalevala	Pra-Kalevala	k1gFnSc7	Pra-Kalevala
(	(	kIx(	(
<g/>
finsky	finsky	k6eAd1	finsky
Alku	alka	k1gFnSc4	alka
Kalevala	Kalevala	k1gFnSc2	Kalevala
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
výsledná	výsledný	k2eAgFnSc1d1	výsledná
podoba	podoba	k1gFnSc1	podoba
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1849	[number]	k4	1849
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
důležitým	důležitý	k2eAgInSc7d1	důležitý
výsledkem	výsledek	k1gInSc7	výsledek
obrozeneckého	obrozenecký	k2eAgNnSc2d1	obrozenecké
hnutí	hnutí	k1gNnSc2	hnutí
bylo	být	k5eAaImAgNnS	být
získání	získání	k1gNnSc4	získání
rovnoprávnosti	rovnoprávnost	k1gFnSc2	rovnoprávnost
finštiny	finština	k1gFnSc2	finština
se	s	k7c7	s
švédštinou	švédština	k1gFnSc7	švédština
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1892	[number]	k4	1892
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
VŘSR	VŘSR	kA	VŘSR
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
Finsko	Finsko	k1gNnSc1	Finsko
samostatnost	samostatnost	k1gFnSc1	samostatnost
<g/>
.	.	kIx.	.
</s>
<s>
Bolševické	bolševický	k2eAgNnSc1d1	bolševické
Rusko	Rusko	k1gNnSc1	Rusko
ji	on	k3xPp3gFnSc4	on
uznalo	uznat	k5eAaPmAgNnS	uznat
během	během	k7c2	během
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Rusko-finské	ruskoinský	k2eAgInPc1d1	rusko-finský
vztahy	vztah	k1gInPc1	vztah
však	však	k9	však
zkomplikovaly	zkomplikovat	k5eAaPmAgInP	zkomplikovat
občanské	občanský	k2eAgInPc1d1	občanský
války	válek	k1gInPc1	válek
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
a	a	k8xC	a
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
<g/>
,	,	kIx,	,
expedice	expedice	k1gFnSc1	expedice
finských	finský	k2eAgMnPc2d1	finský
nacionalistických	nacionalistický	k2eAgMnPc2d1	nacionalistický
aktivistů	aktivista	k1gMnPc2	aktivista
do	do	k7c2	do
Karélie	Karélie	k1gFnSc2	Karélie
a	a	k8xC	a
do	do	k7c2	do
Aunusu	Aunus	k1gInSc2	Aunus
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
intervenční	intervenční	k2eAgFnPc1d1	intervenční
jednotky	jednotka	k1gFnPc1	jednotka
ruských	ruský	k2eAgMnPc2d1	ruský
bolševiků	bolševik	k1gMnPc2	bolševik
vysílané	vysílaný	k2eAgFnPc4d1	vysílaná
zejména	zejména	k9	zejména
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
Viipuri	Viipur	k1gFnSc2	Viipur
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
Finskem	Finsko	k1gNnSc7	Finsko
a	a	k8xC	a
Ruskem	Rusko	k1gNnSc7	Rusko
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
stvrzena	stvrdit	k5eAaPmNgFnS	stvrdit
Tartskou	Tartský	k2eAgFnSc7d1	Tartský
dohodou	dohoda	k1gFnSc7	dohoda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
Sociální	sociální	k2eAgInPc1d1	sociální
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
vládnoucí	vládnoucí	k2eAgFnSc7d1	vládnoucí
a	a	k8xC	a
pracující	pracující	k2eAgFnSc7d1	pracující
třídou	třída	k1gFnSc7	třída
byly	být	k5eAaImAgFnP	být
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
porovnatelných	porovnatelný	k2eAgFnPc6d1	porovnatelná
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
existovala	existovat	k5eAaImAgFnS	existovat
zřejmá	zřejmý	k2eAgFnSc1d1	zřejmá
jazyková	jazykový	k2eAgFnSc1d1	jazyková
bariéra	bariéra	k1gFnSc1	bariéra
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
se	se	k3xPyFc4	se
během	během	k7c2	během
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
univerzitně	univerzitně	k6eAd1	univerzitně
vzdělaná	vzdělaný	k2eAgFnSc1d1	vzdělaná
meritokracie	meritokracie	k1gFnSc1	meritokracie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
cítila	cítit	k5eAaImAgFnS	cítit
být	být	k5eAaImF	být
pravou	pravý	k2eAgFnSc7d1	pravá
reprezentací	reprezentace	k1gFnSc7	reprezentace
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mluvila	mluvit	k5eAaImAgFnS	mluvit
stejnou	stejný	k2eAgFnSc7d1	stejná
řečí	řeč	k1gFnSc7	řeč
a	a	k8xC	a
protože	protože	k8xS	protože
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
z	z	k7c2	z
jejich	jejich	k3xOp3gMnPc2	jejich
předků	předek	k1gMnPc2	předek
skutečně	skutečně	k6eAd1	skutečně
byla	být	k5eAaImAgFnS	být
chudými	chudý	k2eAgMnPc7d1	chudý
rolníky	rolník	k1gMnPc7	rolník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
země	země	k1gFnSc1	země
prošla	projít	k5eAaPmAgFnS	projít
krátkou	krátká	k1gFnSc4	krátká
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
trpkou	trpký	k2eAgFnSc7d1	trpká
zkušeností	zkušenost	k1gFnSc7	zkušenost
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zabarvila	zabarvit	k5eAaPmAgFnS	zabarvit
domácí	domácí	k2eAgFnSc4d1	domácí
politiku	politika	k1gFnSc4	politika
na	na	k7c4	na
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
stáli	stát	k5eAaImAgMnP	stát
vzdělaná	vzdělaný	k2eAgFnSc1d1	vzdělaná
třída	třída	k1gFnSc1	třída
<g/>
,	,	kIx,	,
podporovaná	podporovaný	k2eAgFnSc1d1	podporovaná
početnou	početný	k2eAgFnSc7d1	početná
skupinou	skupina	k1gFnSc7	skupina
malých	malý	k2eAgMnPc2d1	malý
farmářů	farmář	k1gMnPc2	farmář
a	a	k8xC	a
císařským	císařský	k2eAgNnSc7d1	císařské
Německem	Německo	k1gNnSc7	Německo
<g/>
,	,	kIx,	,
a	a	k8xC	a
nemajetní	majetný	k2eNgMnPc1d1	nemajetný
venkované	venkovan	k1gMnPc1	venkovan
a	a	k8xC	a
dělníci	dělník	k1gMnPc1	dělník
z	z	k7c2	z
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
si	se	k3xPyFc3	se
připadali	připadat	k5eAaPmAgMnP	připadat
bez	bez	k7c2	bez
politického	politický	k2eAgInSc2d1	politický
vlivu	vliv	k1gInSc2	vliv
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
měli	mít	k5eAaImAgMnP	mít
všeobecné	všeobecný	k2eAgNnSc4d1	všeobecné
hlasovací	hlasovací	k2eAgNnSc4d1	hlasovací
právo	právo	k1gNnSc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
Finsko	Finsko	k1gNnSc1	Finsko
dvakrát	dvakrát	k6eAd1	dvakrát
bojovalo	bojovat	k5eAaImAgNnS	bojovat
se	s	k7c7	s
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
Zimní	zimní	k2eAgFnSc6d1	zimní
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1939	[number]	k4	1939
až	až	k9	až
1940	[number]	k4	1940
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
v	v	k7c6	v
Pokračovací	pokračovací	k2eAgFnSc6d1	pokračovací
válce	válka	k1gFnSc6	válka
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Finské	finský	k2eAgFnPc1d1	finská
jednotky	jednotka	k1gFnPc1	jednotka
se	se	k3xPyFc4	se
také	také	k9	také
zúčastnily	zúčastnit	k5eAaPmAgFnP	zúčastnit
obležení	obležení	k1gNnSc4	obležení
Leningradu	Leningrad	k1gInSc2	Leningrad
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
válku	válka	k1gFnSc4	válka
navazovala	navazovat	k5eAaImAgFnS	navazovat
Laponská	laponský	k2eAgFnSc1d1	laponská
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1944	[number]	k4	1944
až	až	k9	až
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Finsko	Finsko	k1gNnSc1	Finsko
přinutilo	přinutit	k5eAaPmAgNnS	přinutit
Německo	Německo	k1gNnSc1	Německo
k	k	k7c3	k
odchodu	odchod	k1gInSc3	odchod
ze	z	k7c2	z
severního	severní	k2eAgNnSc2d1	severní
Finska	Finsko	k1gNnSc2	Finsko
<g/>
.	.	kIx.	.
</s>
<s>
Dohody	dohoda	k1gFnPc1	dohoda
se	s	k7c7	s
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1947	[number]	k4	1947
a	a	k8xC	a
1948	[number]	k4	1948
obsahovaly	obsahovat	k5eAaImAgInP	obsahovat
závazky	závazek	k1gInPc1	závazek
a	a	k8xC	a
omezení	omezení	k1gNnSc4	omezení
Finska	Finsko	k1gNnSc2	Finsko
vůči	vůči	k7c3	vůči
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
a	a	k8xC	a
také	také	k9	také
další	další	k2eAgInPc4d1	další
územní	územní	k2eAgInPc4d1	územní
ústupky	ústupek	k1gInPc4	ústupek
(	(	kIx(	(
<g/>
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
Moskevskou	moskevský	k2eAgFnSc7d1	Moskevská
mírovou	mírový	k2eAgFnSc7d1	mírová
dohodou	dohoda	k1gFnSc7	dohoda
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
Finsko	Finsko	k1gNnSc1	Finsko
ocitlo	ocitnout	k5eAaPmAgNnS	ocitnout
v	v	k7c6	v
šedé	šedý	k2eAgFnSc6d1	šedá
zóně	zóna	k1gFnSc6	zóna
mezi	mezi	k7c7	mezi
Západem	západ	k1gInSc7	západ
a	a	k8xC	a
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
YYA	YYA	kA	YYA
dohoda	dohoda	k1gFnSc1	dohoda
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
ze	z	k7c2	z
slov	slovo	k1gNnPc2	slovo
Ystävyys-	Ystävyys-	k1gFnSc2	Ystävyys-
<g/>
,	,	kIx,	,
yhteistyö-	yhteistyö-	k?	yhteistyö-
ja	ja	k?	ja
avunantosopimus	avunantosopimus	k1gInSc1	avunantosopimus
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
Smlouva	smlouva	k1gFnSc1	smlouva
o	o	k7c6	o
přátelství	přátelství	k1gNnSc6	přátelství
<g/>
,	,	kIx,	,
spolupráci	spolupráce	k1gFnSc6	spolupráce
a	a	k8xC	a
vzájemné	vzájemný	k2eAgFnSc3d1	vzájemná
pomoci	pomoc	k1gFnSc3	pomoc
<g/>
)	)	kIx)	)
dávala	dávat	k5eAaImAgFnS	dávat
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
možnost	možnost	k1gFnSc1	možnost
vlivu	vliv	k1gInSc6	vliv
na	na	k7c4	na
finskou	finský	k2eAgFnSc4d1	finská
domácí	domácí	k2eAgFnSc4d1	domácí
politiku	politika	k1gFnSc4	politika
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
politiků	politik	k1gMnPc2	politik
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
prezident	prezident	k1gMnSc1	prezident
Kekkonen	Kekkonen	k2eAgMnSc1d1	Kekkonen
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
až	až	k9	až
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
využívalo	využívat	k5eAaPmAgNnS	využívat
jejich	jejich	k3xOp3gInPc2	jejich
vztahů	vztah	k1gInPc2	vztah
s	s	k7c7	s
Moskvou	Moskva	k1gFnSc7	Moskva
k	k	k7c3	k
řešení	řešení	k1gNnSc3	řešení
vnitrostranických	vnitrostranický	k2eAgInPc2d1	vnitrostranický
sporů	spor	k1gInPc2	spor
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
měl	mít	k5eAaImAgInS	mít
ještě	ještě	k6eAd1	ještě
větší	veliký	k2eAgInSc4d2	veliký
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Jiní	jiný	k2eAgMnPc1d1	jiný
lidé	člověk	k1gMnPc1	člověk
umíněně	umíněně	k6eAd1	umíněně
pracovali	pracovat	k5eAaImAgMnP	pracovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
čelili	čelit	k5eAaImAgMnP	čelit
komunistům	komunista	k1gMnPc3	komunista
<g/>
.	.	kIx.	.
</s>
<s>
Rozpad	rozpad	k1gInSc1	rozpad
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
Finsko	Finsko	k1gNnSc4	Finsko
překvapil	překvapit	k5eAaPmAgInS	překvapit
a	a	k8xC	a
způsobil	způsobit	k5eAaPmAgInS	způsobit
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
krizi	krize	k1gFnSc4	krize
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Finsko	Finsko	k1gNnSc1	Finsko
mohlo	moct	k5eAaImAgNnS	moct
nabrat	nabrat	k5eAaPmF	nabrat
nový	nový	k2eAgInSc4d1	nový
kurz	kurz	k1gInSc4	kurz
<g/>
.	.	kIx.	.
</s>
<s>
Finsko	Finsko	k1gNnSc1	Finsko
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
a	a	k8xC	a
eurozóny	eurozóna	k1gFnSc2	eurozóna
(	(	kIx(	(
<g/>
od	od	k7c2	od
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikoli	nikoli	k9	nikoli
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
sousední	sousední	k2eAgNnSc4d1	sousední
Švédsko	Švédsko	k1gNnSc4	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Finsko	Finsko	k1gNnSc1	Finsko
v	v	k7c6	v
EU	EU	kA	EU
podporuje	podporovat	k5eAaImIp3nS	podporovat
federalismus	federalismus	k1gInSc4	federalismus
oproti	oproti	k7c3	oproti
ostatním	ostatní	k2eAgFnPc3d1	ostatní
severským	severský	k2eAgFnPc3d1	severská
zemím	zem	k1gFnPc3	zem
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
převážně	převážně	k6eAd1	převážně
podporují	podporovat	k5eAaImIp3nP	podporovat
konfederalismus	konfederalismus	k1gInSc4	konfederalismus
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Geografie	geografie	k1gFnSc2	geografie
Finska	Finsko	k1gNnSc2	Finsko
a	a	k8xC	a
Národní	národní	k2eAgInPc1d1	národní
parky	park	k1gInPc1	park
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
<g/>
.	.	kIx.	.
</s>
<s>
Finsko	Finsko	k1gNnSc1	Finsko
je	být	k5eAaImIp3nS	být
zemí	zem	k1gFnSc7	zem
tisíců	tisíc	k4xCgInPc2	tisíc
jezer	jezero	k1gNnPc2	jezero
a	a	k8xC	a
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
necelých	celý	k2eNgInPc2d1	necelý
10	[number]	k4	10
%	%	kIx~	%
jeho	jeho	k3xOp3gFnPc1	jeho
plochy	plocha	k1gFnPc1	plocha
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Přesněji	přesně	k6eAd2	přesně
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
187	[number]	k4	187
888	[number]	k4	888
jezer	jezero	k1gNnPc2	jezero
větších	veliký	k2eAgFnPc2d2	veliký
než	než	k8xS	než
500	[number]	k4	500
m2	m2	k4	m2
a	a	k8xC	a
179	[number]	k4	179
584	[number]	k4	584
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
jen	jen	k9	jen
80	[number]	k4	80
897	[number]	k4	897
leží	ležet	k5eAaImIp3nP	ležet
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Jezero	jezero	k1gNnSc1	jezero
Saimaa	Saima	k1gInSc2	Saima
je	být	k5eAaImIp3nS	být
čtvrté	čtvrtá	k1gFnPc4	čtvrtá
největší	veliký	k2eAgFnPc4d3	veliký
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Finská	finský	k2eAgFnSc1d1	finská
krajina	krajina	k1gFnSc1	krajina
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
rovinatá	rovinatý	k2eAgFnSc1d1	rovinatá
s	s	k7c7	s
nízkými	nízký	k2eAgInPc7d1	nízký
kopci	kopec	k1gInPc7	kopec
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
severozápadní	severozápadní	k2eAgInSc1d1	severozápadní
cíp	cíp	k1gInSc1	cíp
Laponska	Laponsko	k1gNnSc2	Laponsko
<g/>
,	,	kIx,	,
u	u	k7c2	u
hranice	hranice	k1gFnSc2	hranice
s	s	k7c7	s
Norskem	Norsko	k1gNnSc7	Norsko
a	a	k8xC	a
Švédskem	Švédsko	k1gNnSc7	Švédsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Finska	Finsko	k1gNnSc2	Finsko
Halti	Halti	k1gNnSc2	Halti
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
1328	[number]	k4	1328
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
i	i	k8xC	i
ostatních	ostatní	k2eAgFnPc2d1	ostatní
14	[number]	k4	14
finských	finský	k2eAgFnPc2d1	finská
tisícovek	tisícovka	k1gFnPc2	tisícovka
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
mnoha	mnoho	k4c2	mnoho
jezer	jezero	k1gNnPc2	jezero
krajině	krajina	k1gFnSc6	krajina
dominují	dominovat	k5eAaImIp3nP	dominovat
rozsáhlé	rozsáhlý	k2eAgInPc1d1	rozsáhlý
severské	severský	k2eAgInPc1d1	severský
lesy	les	k1gInPc1	les
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
asi	asi	k9	asi
76	[number]	k4	76
%	%	kIx~	%
souše	souš	k1gFnSc2	souš
a	a	k8xC	a
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
zvětšují	zvětšovat	k5eAaImIp3nP	zvětšovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
obdělávaná	obdělávaný	k2eAgFnSc1d1	obdělávaná
půda	půda	k1gFnSc1	půda
<g/>
.	.	kIx.	.
</s>
<s>
Převážná	převážný	k2eAgFnSc1d1	převážná
část	část	k1gFnSc1	část
ostrovů	ostrov	k1gInPc2	ostrov
leží	ležet	k5eAaImIp3nS	ležet
jihozápadě	jihozápad	k1gInSc6	jihozápad
(	(	kIx(	(
<g/>
souostroví	souostroví	k1gNnSc2	souostroví
Å	Å	k?	Å
<g/>
)	)	kIx)	)
a	a	k8xC	a
podél	podél	k7c2	podél
jižního	jižní	k2eAgNnSc2d1	jižní
pobřeží	pobřeží	k1gNnSc2	pobřeží
Finska	Finsko	k1gNnSc2	Finsko
ve	v	k7c6	v
Finském	finský	k2eAgInSc6d1	finský
zálivu	záliv	k1gInSc6	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Finsko	Finsko	k1gNnSc1	Finsko
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
zemí	zem	k1gFnPc2	zem
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
poslední	poslední	k2eAgFnSc2d1	poslední
doby	doba	k1gFnSc2	doba
ledové	ledový	k2eAgFnSc2d1	ledová
se	se	k3xPyFc4	se
země	zem	k1gFnSc2	zem
izostaticky	izostaticky	k6eAd1	izostaticky
zvedá	zvedat	k5eAaImIp3nS	zvedat
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
až	až	k9	až
o	o	k7c4	o
8	[number]	k4	8
mm	mm	kA	mm
a	a	k8xC	a
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
se	se	k3xPyFc4	se
tak	tak	k9	tak
zhruba	zhruba	k6eAd1	zhruba
o	o	k7c4	o
7	[number]	k4	7
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
jižního	jižní	k2eAgNnSc2d1	jižní
Finska	Finsko	k1gNnSc2	Finsko
je	být	k5eAaImIp3nS	být
severské	severský	k2eAgNnSc4d1	severské
mírné	mírný	k2eAgNnSc4d1	mírné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severním	severní	k2eAgNnSc6d1	severní
Finsku	Finsko	k1gNnSc6	Finsko
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
v	v	k7c6	v
Laponsku	Laponsko	k1gNnSc6	Laponsko
převažuje	převažovat	k5eAaImIp3nS	převažovat
subarktické	subarktický	k2eAgNnSc1d1	subarktické
podnebí	podnebí	k1gNnSc1	podnebí
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
studenými	studený	k2eAgInPc7d1	studený
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
krutými	krutý	k2eAgFnPc7d1	krutá
zimami	zima	k1gFnPc7	zima
a	a	k8xC	a
relativně	relativně	k6eAd1	relativně
teplými	teplý	k2eAgNnPc7d1	teplé
léty	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Počasí	počasí	k1gNnSc1	počasí
významně	významně	k6eAd1	významně
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
jak	jak	k6eAd1	jak
Golfský	golfský	k2eAgInSc1d1	golfský
proud	proud	k1gInSc1	proud
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
vnitrozemské	vnitrozemský	k2eAgNnSc1d1	vnitrozemské
podnebí	podnebí	k1gNnSc1	podnebí
západní	západní	k2eAgFnSc2d1	západní
Sibiře	Sibiř	k1gFnSc2	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
Teploty	teplota	k1gFnPc1	teplota
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
,	,	kIx,	,
měnit	měnit	k5eAaImF	měnit
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
teplota	teplota	k1gFnSc1	teplota
změní	změnit	k5eAaPmIp3nS	změnit
i	i	k9	i
o	o	k7c4	o
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
°	°	k?	°
<g/>
C	C	kA	C
během	během	k7c2	během
jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
je	být	k5eAaImIp3nS	být
většina	většina	k1gFnSc1	většina
území	území	k1gNnSc2	území
Finska	Finsko	k1gNnSc2	Finsko
pod	pod	k7c7	pod
sněhem	sníh	k1gInSc7	sníh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
Finsku	Finsko	k1gNnSc6	Finsko
je	být	k5eAaImIp3nS	být
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
zimy	zima	k1gFnSc2	zima
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
cm	cm	kA	cm
sněhu	sníh	k1gInSc2	sníh
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
postupně	postupně	k6eAd1	postupně
připadává	připadávat	k5eAaImIp3nS	připadávat
<g/>
.	.	kIx.	.
</s>
<s>
Jezera	jezero	k1gNnPc1	jezero
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
zamrzají	zamrzat	k5eAaImIp3nP	zamrzat
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
<g/>
,	,	kIx,	,
větší	veliký	k2eAgNnPc4d2	veliký
jezera	jezero	k1gNnPc4	jezero
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
<g/>
.	.	kIx.	.
</s>
<s>
Rozmrzají	rozmrzat	k5eAaImIp3nP	rozmrzat
pak	pak	k6eAd1	pak
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
<g/>
.	.	kIx.	.
</s>
<s>
Zamrzají	zamrzat	k5eAaImIp3nP	zamrzat
i	i	k9	i
okolní	okolní	k2eAgNnSc4d1	okolní
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
finského	finský	k2eAgNnSc2d1	finské
území	území	k1gNnSc2	území
leží	ležet	k5eAaImIp3nS	ležet
za	za	k7c7	za
polárním	polární	k2eAgInSc7d1	polární
kruhem	kruh	k1gInSc7	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
dá	dát	k5eAaPmIp3nS	dát
zažít	zažít	k5eAaPmF	zažít
půlnoční	půlnoční	k2eAgNnSc4d1	půlnoční
slunce	slunce	k1gNnSc4	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yQnSc7	co
více	hodně	k6eAd2	hodně
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
,	,	kIx,	,
tím	ten	k3xDgInSc7	ten
je	být	k5eAaImIp3nS	být
víc	hodně	k6eAd2	hodně
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
konci	konec	k1gInSc6	konec
Finska	Finsko	k1gNnSc2	Finsko
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
Slunce	slunce	k1gNnSc2	slunce
nezapadá	zapadat	k5eNaImIp3nS	zapadat
po	po	k7c4	po
73	[number]	k4	73
dnů	den	k1gInPc2	den
a	a	k8xC	a
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
nevyjde	vyjít	k5eNaPmIp3nS	vyjít
51	[number]	k4	51
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
Finsku	Finsko	k1gNnSc6	Finsko
pak	pak	k6eAd1	pak
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
sice	sice	k8xC	sice
Slunce	slunce	k1gNnSc4	slunce
zapadá	zapadat	k5eAaImIp3nS	zapadat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nesetmí	setmět	k5eNaPmIp3nP	setmět
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
okolo	okolo	k7c2	okolo
letního	letní	k2eAgInSc2d1	letní
slunovratu	slunovrat	k1gInSc2	slunovrat
jsou	být	k5eAaImIp3nP	být
noci	noc	k1gFnPc1	noc
takzvaně	takzvaně	k6eAd1	takzvaně
bílé	bílý	k2eAgFnPc1d1	bílá
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
zimního	zimní	k2eAgInSc2d1	zimní
slunovratu	slunovrat	k1gInSc2	slunovrat
pak	pak	k6eAd1	pak
zde	zde	k6eAd1	zde
jsou	být	k5eAaImIp3nP	být
nejkratší	krátký	k2eAgInPc4d3	nejkratší
dny	den	k1gInPc4	den
asi	asi	k9	asi
pět	pět	k4xCc1	pět
až	až	k9	až
šest	šest	k4xCc1	šest
hodin	hodina	k1gFnPc2	hodina
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
Finska	Finsko	k1gNnSc2	Finsko
<g/>
.	.	kIx.	.
</s>
<s>
Finsko	Finsko	k1gNnSc1	Finsko
je	být	k5eAaImIp3nS	být
parlamentní	parlamentní	k2eAgFnSc1d1	parlamentní
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
výkonné	výkonný	k2eAgFnSc2d1	výkonná
moci	moc	k1gFnSc2	moc
náleží	náležet	k5eAaImIp3nS	náležet
vládě	vláda	k1gFnSc3	vláda
(	(	kIx(	(
<g/>
Státní	státní	k2eAgFnSc1d1	státní
rada	rada	k1gFnSc1	rada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vede	vést	k5eAaImIp3nS	vést
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
vybraný	vybraný	k2eAgMnSc1d1	vybraný
parlamentem	parlament	k1gInSc7	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgFnSc1d1	státní
rada	rada	k1gFnSc1	rada
je	být	k5eAaImIp3nS	být
tvořená	tvořený	k2eAgFnSc1d1	tvořená
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
ministry	ministr	k1gMnPc7	ministr
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
ministerstev	ministerstvo	k1gNnPc2	ministerstvo
centrální	centrální	k2eAgFnSc2d1	centrální
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
také	také	k9	také
Kancléřem	kancléř	k1gMnSc7	kancléř
pro	pro	k7c4	pro
spravedlnost	spravedlnost	k1gFnSc4	spravedlnost
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
ochráncem	ochránce	k1gMnSc7	ochránce
práv	právo	k1gNnPc2	právo
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
a	a	k8xC	a
dohlíží	dohlížet	k5eAaImIp3nS	dohlížet
na	na	k7c4	na
zákonnost	zákonnost	k1gFnSc4	zákonnost
jednání	jednání	k1gNnSc2	jednání
veřejných	veřejný	k2eAgInPc2d1	veřejný
činitelů	činitel	k1gInPc2	činitel
a	a	k8xC	a
dodržování	dodržování	k1gNnSc4	dodržování
základních	základní	k2eAgNnPc2d1	základní
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
finské	finský	k2eAgFnSc2d1	finská
Ústavy	ústava	k1gFnSc2	ústava
je	být	k5eAaImIp3nS	být
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
zákonodárným	zákonodárný	k2eAgInSc7d1	zákonodárný
sborem	sbor	k1gInSc7	sbor
jednokomorový	jednokomorový	k2eAgInSc4d1	jednokomorový
parlament	parlament	k1gInSc4	parlament
(	(	kIx(	(
<g/>
finsky	finsky	k6eAd1	finsky
Eduskunta	Eduskunt	k1gMnSc2	Eduskunt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
může	moct	k5eAaImIp3nS	moct
měnit	měnit	k5eAaImF	měnit
Ústavu	ústav	k1gInSc3	ústav
<g/>
,	,	kIx,	,
odvolávat	odvolávat	k5eAaImF	odvolávat
předsedu	předseda	k1gMnSc4	předseda
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
přehlasovat	přehlasovat	k5eAaBmF	přehlasovat
prezidentské	prezidentský	k2eAgNnSc4d1	prezidentské
veto	veto	k1gNnSc4	veto
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
zákony	zákon	k1gInPc1	zákon
nejsou	být	k5eNaImIp3nP	být
předmětem	předmět	k1gInSc7	předmět
soudního	soudní	k2eAgInSc2d1	soudní
přezkumu	přezkum	k1gInSc2	přezkum
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
je	on	k3xPp3gFnPc4	on
Státní	státní	k2eAgFnSc1d1	státní
rada	rada	k1gFnSc1	rada
nebo	nebo	k8xC	nebo
některý	některý	k3yIgMnSc1	některý
z	z	k7c2	z
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Poslanci	poslanec	k1gMnPc1	poslanec
jsou	být	k5eAaImIp3nP	být
voleni	volit	k5eAaImNgMnP	volit
na	na	k7c4	na
čtyřleté	čtyřletý	k2eAgFnPc4d1	čtyřletá
období	období	k1gNnSc6	období
poměrným	poměrný	k2eAgInSc7d1	poměrný
systémem	systém	k1gInSc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
Volič	volič	k1gMnSc1	volič
může	moct	k5eAaImIp3nS	moct
při	při	k7c6	při
volbách	volba	k1gFnPc6	volba
dávat	dávat	k5eAaImF	dávat
preferenční	preferenční	k2eAgFnSc1d1	preferenční
hlasy	hlas	k1gInPc7	hlas
jednotlivým	jednotlivý	k2eAgMnPc3d1	jednotlivý
kandidátům	kandidát	k1gMnPc3	kandidát
<g/>
.	.	kIx.	.
</s>
<s>
Soudní	soudní	k2eAgInSc1d1	soudní
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
mezi	mezi	k7c7	mezi
soudy	soud	k1gInPc7	soud
s	s	k7c7	s
běžnou	běžný	k2eAgFnSc7d1	běžná
občanskou	občanský	k2eAgFnSc7d1	občanská
a	a	k8xC	a
trestní	trestní	k2eAgFnSc7d1	trestní
soudní	soudní	k2eAgFnSc7d1	soudní
pravomocí	pravomoc	k1gFnSc7	pravomoc
a	a	k8xC	a
administrativní	administrativní	k2eAgInPc1d1	administrativní
soudy	soud	k1gInPc1	soud
řešící	řešící	k2eAgInPc1d1	řešící
spory	spor	k1gInPc1	spor
mezi	mezi	k7c7	mezi
jednotlivci	jednotlivec	k1gMnPc7	jednotlivec
a	a	k8xC	a
administrativními	administrativní	k2eAgInPc7d1	administrativní
orgány	orgán	k1gInPc7	orgán
státu	stát	k1gInSc2	stát
a	a	k8xC	a
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc4	jejich
soudní	soudní	k2eAgFnSc4d1	soudní
pravomoc	pravomoc	k1gFnSc4	pravomoc
vystihuje	vystihovat	k5eAaImIp3nS	vystihovat
následující	následující	k2eAgInSc1d1	následující
příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
Rodiče	rodič	k1gMnPc1	rodič
nespokojení	spokojený	k2eNgMnPc1d1	nespokojený
s	s	k7c7	s
umístěním	umístění	k1gNnSc7	umístění
jejich	jejich	k3xOp3gNnSc2	jejich
dítěte	dítě	k1gNnSc2	dítě
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
odvolat	odvolat	k5eAaPmF	odvolat
proti	proti	k7c3	proti
školskému	školský	k2eAgInSc3d1	školský
výboru	výbor	k1gInSc3	výbor
k	k	k7c3	k
administrativnímu	administrativní	k2eAgInSc3d1	administrativní
soudu	soud	k1gInSc3	soud
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
umístění	umístění	k1gNnSc1	umístění
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
je	být	k5eAaImIp3nS	být
administrativní	administrativní	k2eAgNnSc4d1	administrativní
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
<g/>
.	.	kIx.	.
</s>
<s>
Finské	finský	k2eAgNnSc1d1	finské
právo	právo	k1gNnSc1	právo
je	být	k5eAaImIp3nS	být
kodifikované	kodifikovaný	k2eAgNnSc1d1	kodifikované
a	a	k8xC	a
soudní	soudní	k2eAgInSc1d1	soudní
systém	systém	k1gInSc1	systém
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
místních	místní	k2eAgInPc2d1	místní
soudů	soud	k1gInPc2	soud
<g/>
,	,	kIx,	,
oblastních	oblastní	k2eAgInPc2d1	oblastní
odvolacích	odvolací	k2eAgInPc2d1	odvolací
soudů	soud	k1gInPc2	soud
a	a	k8xC	a
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Administrativní	administrativní	k2eAgFnSc1d1	administrativní
větev	větev	k1gFnSc1	větev
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
administrativních	administrativní	k2eAgInPc2d1	administrativní
soudů	soud	k1gInPc2	soud
a	a	k8xC	a
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
administrativního	administrativní	k2eAgInSc2d1	administrativní
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Administrativní	administrativní	k2eAgInSc1d1	administrativní
proces	proces	k1gInSc1	proces
je	být	k5eAaImIp3nS	být
populárnější	populární	k2eAgMnSc1d2	populárnější
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
levnější	levný	k2eAgMnSc1d2	levnější
a	a	k8xC	a
představuje	představovat	k5eAaImIp3nS	představovat
pro	pro	k7c4	pro
stěžujícího	stěžující	k2eAgMnSc2d1	stěžující
menší	malý	k2eAgInSc4d2	menší
finanční	finanční	k2eAgInSc4d1	finanční
risk	risk	k1gInSc4	risk
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc1	několik
zvláštních	zvláštní	k2eAgInPc2d1	zvláštní
soudů	soud	k1gInPc2	soud
pro	pro	k7c4	pro
některé	některý	k3yIgFnPc4	některý
oblasti	oblast	k1gFnPc4	oblast
administrativy	administrativa	k1gFnSc2	administrativa
<g/>
.	.	kIx.	.
</s>
<s>
Parlament	parlament	k1gInSc1	parlament
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začalo	začít	k5eAaPmAgNnS	začít
platit	platit	k5eAaImF	platit
všeobecné	všeobecný	k2eAgNnSc4d1	všeobecné
hlasovací	hlasovací	k2eAgNnSc4d1	hlasovací
právo	právo	k1gNnSc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnPc1d3	veliký
strany	strana	k1gFnPc1	strana
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
zastoupené	zastoupený	k2eAgInPc1d1	zastoupený
jsou	být	k5eAaImIp3nP	být
Strana	strana	k1gFnSc1	strana
národní	národní	k2eAgFnSc2d1	národní
koalice	koalice	k1gFnSc2	koalice
(	(	kIx(	(
<g/>
pravicová	pravicový	k2eAgFnSc1d1	pravicová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Finská	finský	k2eAgFnSc1d1	finská
strana	strana	k1gFnSc1	strana
středu	střed	k1gInSc2	střed
(	(	kIx(	(
<g/>
agrárníci	agrárník	k1gMnPc1	agrárník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Finská	finský	k2eAgFnSc1d1	finská
strana	strana	k1gFnSc1	strana
sociálně	sociálně	k6eAd1	sociálně
demokratická	demokratický	k2eAgFnSc1d1	demokratická
Levá	levý	k2eAgFnSc1d1	levá
aliance	aliance	k1gFnSc1	aliance
(	(	kIx(	(
<g/>
komunisté	komunista	k1gMnPc1	komunista
<g/>
)	)	kIx)	)
Je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
poznamenat	poznamenat	k5eAaPmF	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Finsko	Finsko	k1gNnSc1	Finsko
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
během	během	k7c2	během
Studené	Studené	k2eAgFnSc2d1	Studené
války	válka	k1gFnSc2	válka
demokratické	demokratický	k2eAgFnSc2d1	demokratická
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
politická	politický	k2eAgFnSc1d1	politická
atmosféra	atmosféra	k1gFnSc1	atmosféra
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
ovlivněná	ovlivněný	k2eAgNnPc4d1	ovlivněné
sousedícím	sousedící	k2eAgInSc7d1	sousedící
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Poměry	poměr	k1gInPc1	poměr
sil	síla	k1gFnPc2	síla
stran	strana	k1gFnPc2	strana
se	se	k3xPyFc4	se
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
mění	měnit	k5eAaImIp3nS	měnit
jen	jen	k9	jen
mírně	mírně	k6eAd1	mírně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
některé	některý	k3yIgInPc1	některý
dlouhodobé	dlouhodobý	k2eAgInPc1d1	dlouhodobý
trendy	trend	k1gInPc1	trend
jsou	být	k5eAaImIp3nP	být
viditelné	viditelný	k2eAgFnPc1d1	viditelná
<g/>
.	.	kIx.	.
</s>
<s>
Finská	finský	k2eAgFnSc1d1	finská
Ústava	ústava	k1gFnSc1	ústava
a	a	k8xC	a
její	její	k3xOp3gNnSc1	její
místo	místo	k1gNnSc1	místo
v	v	k7c6	v
soudním	soudní	k2eAgInSc6d1	soudní
systému	systém	k1gInSc6	systém
jsou	být	k5eAaImIp3nP	být
neobvyklé	obvyklý	k2eNgFnPc1d1	neobvyklá
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
neexistuje	existovat	k5eNaImIp3nS	existovat
žádný	žádný	k3yNgInSc1	žádný
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
a	a	k8xC	a
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
nemá	mít	k5eNaImIp3nS	mít
výslovné	výslovný	k2eAgNnSc1d1	výslovné
právo	právo	k1gNnSc1	právo
prohlásit	prohlásit	k5eAaPmF	prohlásit
nějaký	nějaký	k3yIgInSc4	nějaký
zákon	zákon	k1gInSc4	zákon
za	za	k7c4	za
neústavní	ústavní	k2eNgNnSc4d1	neústavní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
principu	princip	k1gInSc6	princip
<g/>
,	,	kIx,	,
ústavnost	ústavnost	k1gFnSc1	ústavnost
zákonů	zákon	k1gInPc2	zákon
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
ověřena	ověřen	k2eAgFnSc1d1	ověřena
hlasováním	hlasování	k1gNnSc7	hlasování
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
Ústavní	ústavní	k2eAgFnSc1d1	ústavní
komise	komise	k1gFnSc1	komise
posuzuje	posuzovat	k5eAaImIp3nS	posuzovat
nejasné	jasný	k2eNgInPc4d1	nejasný
zákony	zákon	k1gInPc4	zákon
a	a	k8xC	a
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
změny	změna	k1gFnPc4	změna
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
Ústavní	ústavní	k2eAgFnSc2d1	ústavní
komise	komise	k1gFnSc2	komise
plní	plnit	k5eAaImIp3nP	plnit
povinnosti	povinnost	k1gFnPc1	povinnost
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Finskou	finský	k2eAgFnSc7d1	finská
specialitou	specialita	k1gFnSc7	specialita
je	být	k5eAaImIp3nS	být
možnost	možnost	k1gFnSc1	možnost
přijímat	přijímat	k5eAaImF	přijímat
v	v	k7c6	v
obyčejných	obyčejný	k2eAgInPc6d1	obyčejný
zákonech	zákon	k1gInPc6	zákon
výjimky	výjimka	k1gFnSc2	výjimka
z	z	k7c2	z
Ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
přijaté	přijatý	k2eAgMnPc4d1	přijatý
stejným	stejný	k2eAgInSc7d1	stejný
postupem	postup	k1gInSc7	postup
jako	jako	k8xS	jako
Ústavní	ústavní	k2eAgInPc1d1	ústavní
dodatky	dodatek	k1gInPc1	dodatek
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zákon	zákon	k1gInSc4	zákon
o	o	k7c6	o
stavu	stav	k1gInSc6	stav
pohotovosti	pohotovost	k1gFnSc2	pohotovost
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dává	dávat	k5eAaImIp3nS	dávat
Státní	státní	k2eAgFnSc3d1	státní
radě	rada	k1gFnSc3	rada
výjimečná	výjimečný	k2eAgFnSc1d1	výjimečná
práva	práv	k2eAgFnSc1d1	práva
v	v	k7c6	v
případě	případ	k1gInSc6	případ
národního	národní	k2eAgNnSc2d1	národní
ohrožení	ohrožení	k1gNnSc2	ohrožení
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
tato	tento	k3xDgNnPc1	tento
práva	právo	k1gNnPc1	právo
hrubě	hrubě	k6eAd1	hrubě
porušují	porušovat	k5eAaImIp3nP	porušovat
základní	základní	k2eAgNnPc4d1	základní
ústavní	ústavní	k2eAgNnPc4d1	ústavní
práva	právo	k1gNnPc4	právo
<g/>
,	,	kIx,	,
zákon	zákon	k1gInSc1	zákon
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
jako	jako	k8xC	jako
ústavní	ústavní	k2eAgInSc4d1	ústavní
dodatek	dodatek	k1gInSc4	dodatek
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
jako	jako	k8xC	jako
běžný	běžný	k2eAgInSc1d1	běžný
zákon	zákon	k1gInSc1	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
finské	finský	k2eAgInPc1d1	finský
soudy	soud	k1gInPc1	soud
mají	mít	k5eAaImIp3nP	mít
povinnost	povinnost	k1gFnSc4	povinnost
dát	dát	k5eAaPmF	dát
přednost	přednost	k1gFnSc4	přednost
Ústavě	ústava	k1gFnSc3	ústava
před	před	k7c7	před
obyčejným	obyčejný	k2eAgInSc7d1	obyčejný
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
pojistka	pojistka	k1gFnSc1	pojistka
však	však	k9	však
zatím	zatím	k6eAd1	zatím
nikdy	nikdy	k6eAd1	nikdy
nebyla	být	k5eNaImAgFnS	být
využita	využít	k5eAaPmNgFnS	využít
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediný	k2eAgFnPc1d1	jediná
další	další	k2eAgFnPc1d1	další
evropské	evropský	k2eAgFnPc1d1	Evropská
země	zem	k1gFnPc1	zem
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nemají	mít	k5eNaImIp3nP	mít
Ústavní	ústavní	k2eAgInSc4d1	ústavní
soud	soud	k1gInSc4	soud
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
a	a	k8xC	a
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
(	(	kIx(	(
<g/>
které	který	k3yQgNnSc1	který
nemá	mít	k5eNaImIp3nS	mít
psanou	psaný	k2eAgFnSc4d1	psaná
Ústavu	ústava	k1gFnSc4	ústava
vůbec	vůbec	k9	vůbec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
Finska	Finsko	k1gNnSc2	Finsko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
je	být	k5eAaImIp3nS	být
zaveden	zaveden	k2eAgInSc1d1	zaveden
několikastupňový	několikastupňový	k2eAgInSc1d1	několikastupňový
systém	systém	k1gInSc1	systém
administrativního	administrativní	k2eAgNnSc2d1	administrativní
dělení	dělení	k1gNnSc2	dělení
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Nejnižším	nízký	k2eAgInSc7d3	nejnižší
celkem	celek	k1gInSc7	celek
je	být	k5eAaImIp3nS	být
kunta	kunta	k?	kunta
(	(	kIx(	(
<g/>
obec	obec	k1gFnSc1	obec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
další	další	k2eAgFnSc7d1	další
úrovní	úroveň	k1gFnSc7	úroveň
je	být	k5eAaImIp3nS	být
seutukunta	seutukunta	k1gFnSc1	seutukunta
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyšší	vysoký	k2eAgFnSc1d2	vyšší
stupněm	stupeň	k1gInSc7	stupeň
je	být	k5eAaImIp3nS	být
maakunta	maakunta	k1gFnSc1	maakunta
(	(	kIx(	(
<g/>
provincie	provincie	k1gFnSc1	provincie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
provinciemi	provincie	k1gFnPc7	provincie
figurují	figurovat	k5eAaImIp3nP	figurovat
následně	následně	k6eAd1	následně
úřady	úřad	k1gInPc1	úřad
aluehallintovirasto	aluehallintovirasto	k6eAd1	aluehallintovirasto
(	(	kIx(	(
<g/>
oblastní	oblastní	k2eAgInSc1d1	oblastní
správní	správní	k2eAgInSc1d1	správní
úřad	úřad	k1gInSc1	úřad
<g/>
)	)	kIx)	)
a	a	k8xC	a
elinkeino-	elinkeino-	k?	elinkeino-
<g/>
,	,	kIx,	,
liikenne-	liikenne-	k?	liikenne-
ja	ja	k?	ja
ympäristökeskus	ympäristökeskus	k1gInSc1	ympäristökeskus
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
ely-keskus	elyeskus	k1gInSc1	ely-keskus
<g/>
;	;	kIx,	;
centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
,	,	kIx,	,
dopravu	doprava	k1gFnSc4	doprava
a	a	k8xC	a
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
dva	dva	k4xCgInPc1	dva
úřady	úřad	k1gInPc1	úřad
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2010	[number]	k4	2010
kdy	kdy	k6eAd1	kdy
nahradily	nahradit	k5eAaPmAgInP	nahradit
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
používaný	používaný	k2eAgInSc1d1	používaný
systém	systém	k1gInSc1	systém
dělení	dělení	k1gNnSc2	dělení
území	území	k1gNnSc2	území
Finska	Finsko	k1gNnSc2	Finsko
do	do	k7c2	do
6	[number]	k4	6
krajů	kraj	k1gInPc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
320	[number]	k4	320
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
70	[number]	k4	70
okresů	okres	k1gInPc2	okres
a	a	k8xC	a
19	[number]	k4	19
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
Å	Å	k?	Å
mají	mít	k5eAaImIp3nP	mít
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
autonomii	autonomie	k1gFnSc4	autonomie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
dohod	dohoda	k1gFnPc2	dohoda
a	a	k8xC	a
finských	finský	k2eAgInPc2d1	finský
zákonů	zákon	k1gInPc2	zákon
regionální	regionální	k2eAgInSc1d1	regionální
å	å	k?	å
vláda	vláda	k1gFnSc1	vláda
řeší	řešit	k5eAaImIp3nS	řešit
některé	některý	k3yIgFnPc4	některý
záležitosti	záležitost	k1gFnPc4	záležitost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jinak	jinak	k6eAd1	jinak
přísluší	příslušet	k5eAaImIp3nP	příslušet
do	do	k7c2	do
pravomocí	pravomoc	k1gFnPc2	pravomoc
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
Å	Å	k?	Å
požívají	požívat	k5eAaImIp3nP	požívat
také	také	k9	také
mnohých	mnohý	k2eAgFnPc2d1	mnohá
výjimek	výjimka	k1gFnPc2	výjimka
ze	z	k7c2	z
zákonodárství	zákonodárství	k1gNnSc2	zákonodárství
Evropské	evropský	k2eAgFnSc2d1	Evropská
Unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
patrné	patrný	k2eAgNnSc1d1	patrné
dělení	dělení	k1gNnSc1	dělení
na	na	k7c4	na
regiony	region	k1gInPc4	region
vzniklé	vzniklý	k2eAgNnSc1d1	vzniklé
postupnou	postupný	k2eAgFnSc7d1	postupná
kolonizací	kolonizace	k1gFnSc7	kolonizace
Finska	Finsko	k1gNnSc2	Finsko
<g/>
.	.	kIx.	.
</s>
<s>
Dialekty	dialekt	k1gInPc4	dialekt
<g/>
,	,	kIx,	,
folklór	folklór	k1gInSc4	folklór
<g/>
,	,	kIx,	,
zvyky	zvyk	k1gInPc4	zvyk
a	a	k8xC	a
pocity	pocit	k1gInPc4	pocit
lidí	člověk	k1gMnPc2	člověk
k	k	k7c3	k
příslušnosti	příslušnost	k1gFnSc3	příslušnost
jsou	být	k5eAaImIp3nP	být
spojeny	spojen	k2eAgInPc1d1	spojen
s	s	k7c7	s
historickými	historický	k2eAgFnPc7d1	historická
provinciemi	provincie	k1gFnPc7	provincie
Finska	Finsko	k1gNnSc2	Finsko
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
přesídlení	přesídlení	k1gNnSc3	přesídlení
420	[number]	k4	420
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
Karélie	Karélie	k1gFnSc2	Karélie
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
urbanizace	urbanizace	k1gFnSc2	urbanizace
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
mnoho	mnoho	k4c4	mnoho
rozdílů	rozdíl	k1gInPc2	rozdíl
zjemnilo	zjemnit	k5eAaPmAgNnS	zjemnit
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Finska	Finsko	k1gNnSc2	Finsko
<g/>
.	.	kIx.	.
</s>
<s>
Finsko	Finsko	k1gNnSc1	Finsko
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
vysoce	vysoce	k6eAd1	vysoce
industrializované	industrializovaný	k2eAgFnPc4d1	industrializovaná
ekonomiky	ekonomika	k1gFnPc4	ekonomika
s	s	k7c7	s
volným	volný	k2eAgInSc7d1	volný
trhem	trh	k1gInSc7	trh
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
vyprodukuje	vyprodukovat	k5eAaPmIp3nS	vyprodukovat
zhruba	zhruba	k6eAd1	zhruba
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
nebo	nebo	k8xC	nebo
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Životní	životní	k2eAgInSc1d1	životní
standard	standard	k1gInSc1	standard
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
vysoký	vysoký	k2eAgMnSc1d1	vysoký
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
klíčové	klíčový	k2eAgInPc4d1	klíčový
ekonomické	ekonomický	k2eAgInPc4d1	ekonomický
sektory	sektor	k1gInPc4	sektor
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
výroba	výroba	k1gFnSc1	výroba
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
zpracování	zpracování	k1gNnSc4	zpracování
dřeva	dřevo	k1gNnSc2	dřevo
a	a	k8xC	a
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
strojírenství	strojírenství	k1gNnSc2	strojírenství
a	a	k8xC	a
odvětví	odvětví	k1gNnSc2	odvětví
telekomunikací	telekomunikace	k1gFnPc2	telekomunikace
(	(	kIx(	(
<g/>
jmenujme	jmenovat	k5eAaImRp1nP	jmenovat
firmu	firma	k1gFnSc4	firma
Nokia	Nokia	kA	Nokia
<g/>
)	)	kIx)	)
a	a	k8xC	a
elektroniky	elektronika	k1gFnSc2	elektronika
<g/>
.	.	kIx.	.
</s>
<s>
Důležitost	důležitost	k1gFnSc1	důležitost
obchodu	obchod	k1gInSc2	obchod
podtrhuje	podtrhovat	k5eAaImIp3nS	podtrhovat
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vyváží	vyvážit	k5eAaPmIp3nS	vyvážit
téměř	téměř	k6eAd1	téměř
třetina	třetina	k1gFnSc1	třetina
z	z	k7c2	z
HDP	HDP	kA	HDP
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
na	na	k7c6	na
dřevo	dřevo	k1gNnSc1	dřevo
a	a	k8xC	a
několik	několik	k4yIc1	několik
nerostů	nerost	k1gInPc2	nerost
závisí	záviset	k5eAaImIp3nS	záviset
hospodářství	hospodářství	k1gNnSc1	hospodářství
Finska	Finsko	k1gNnSc2	Finsko
na	na	k7c6	na
dovozu	dovoz	k1gInSc6	dovoz
surovin	surovina	k1gFnPc2	surovina
<g/>
,	,	kIx,	,
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
také	také	k9	také
některých	některý	k3yIgFnPc2	některý
součástí	součást	k1gFnPc2	součást
vyráběného	vyráběný	k2eAgNnSc2d1	vyráběné
zboží	zboží	k1gNnSc2	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělství	zemědělství	k1gNnSc1	zemědělství
je	být	k5eAaImIp3nS	být
soběstačné	soběstačný	k2eAgNnSc1d1	soběstačné
v	v	k7c6	v
základních	základní	k2eAgInPc6d1	základní
produktech	produkt	k1gInPc6	produkt
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
ho	on	k3xPp3gMnSc4	on
omezuje	omezovat	k5eAaImIp3nS	omezovat
podnebí	podnebí	k1gNnSc1	podnebí
<g/>
.	.	kIx.	.
</s>
<s>
Značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
venkovského	venkovský	k2eAgNnSc2d1	venkovské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
lesnictví	lesnictví	k1gNnSc6	lesnictví
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
důležité	důležitý	k2eAgInPc4d1	důležitý
exportní	exportní	k2eAgInPc4d1	exportní
příjmové	příjmový	k2eAgInPc4d1	příjmový
zdroje	zdroj	k1gInPc4	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
růst	růst	k1gInSc1	růst
Finska	Finsko	k1gNnSc2	Finsko
se	se	k3xPyFc4	se
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
zvětšující	zvětšující	k2eAgFnSc1d1	zvětšující
integrace	integrace	k1gFnSc1	integrace
se	s	k7c7	s
západní	západní	k2eAgFnSc7d1	západní
Evropou	Evropa	k1gFnSc7	Evropa
-	-	kIx~	-
Finsko	Finsko	k1gNnSc1	Finsko
bylo	být	k5eAaImAgNnS	být
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
11	[number]	k4	11
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
přijaly	přijmout	k5eAaPmAgInP	přijmout
euro	euro	k1gNnSc1	euro
-	-	kIx~	-
bude	být	k5eAaImBp3nS	být
po	po	k7c4	po
příští	příští	k2eAgNnPc4d1	příští
léta	léto	k1gNnPc4	léto
finské	finský	k2eAgFnSc2d1	finská
ekonomice	ekonomika	k1gFnSc3	ekonomika
dominovat	dominovat	k5eAaImF	dominovat
<g/>
.	.	kIx.	.
</s>
<s>
Průmysl	průmysl	k1gInSc1	průmysl
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
ústupu	ústup	k1gInSc6	ústup
kvůli	kvůli	k7c3	kvůli
slábnoucí	slábnoucí	k2eAgFnSc3d1	slábnoucí
Nokii	Nokie	k1gFnSc3	Nokie
a	a	k8xC	a
snižující	snižující	k2eAgFnSc3d1	snižující
se	se	k3xPyFc4	se
poptávce	poptávka	k1gFnSc3	poptávka
po	po	k7c6	po
dřevě	dřevo	k1gNnSc6	dřevo
-	-	kIx~	-
dřevozpracující	dřevozpracující	k2eAgInSc1d1	dřevozpracující
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
kolabuje	kolabovat	k5eAaImIp3nS	kolabovat
<g/>
.	.	kIx.	.
</s>
<s>
Finská	finský	k2eAgFnSc1d1	finská
vláda	vláda	k1gFnSc1	vláda
to	ten	k3xDgNnSc4	ten
vysvětlila	vysvětlit	k5eAaPmAgFnS	vysvětlit
čím	čí	k3xOyRgNnSc7	čí
dál	daleko	k6eAd2	daleko
silnějším	silný	k2eAgNnSc7d2	silnější
postavením	postavení	k1gNnSc7	postavení
firmy	firma	k1gFnSc2	firma
Apple	Apple	kA	Apple
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
Finska	Finsko	k1gNnSc2	Finsko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgInPc1	dva
úřední	úřední	k2eAgInPc1d1	úřední
jazyky	jazyk	k1gInPc1	jazyk
<g/>
:	:	kIx,	:
finština	finština	k1gFnSc1	finština
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
mluví	mluvit	k5eAaImIp3nS	mluvit
92	[number]	k4	92
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
a	a	k8xC	a
švédština	švédština	k1gFnSc1	švédština
<g/>
,	,	kIx,	,
mateřský	mateřský	k2eAgInSc1d1	mateřský
jazyk	jazyk	k1gInSc1	jazyk
pro	pro	k7c4	pro
5,5	[number]	k4	5,5
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Etničtí	etnický	k2eAgMnPc1d1	etnický
Finové	Fin	k1gMnPc1	Fin
a	a	k8xC	a
finští	finský	k2eAgMnPc1d1	finský
Švédové	Švéd	k1gMnPc1	Švéd
tvoří	tvořit	k5eAaImIp3nP	tvořit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
obecně	obecně	k6eAd1	obecně
přijímá	přijímat	k5eAaImIp3nS	přijímat
<g/>
,	,	kIx,	,
společný	společný	k2eAgInSc1d1	společný
národ	národ	k1gInSc1	národ
<g/>
.	.	kIx.	.
</s>
<s>
Finští	finský	k2eAgMnPc1d1	finský
Švédové	Švéd	k1gMnPc1	Švéd
žijí	žít	k5eAaImIp3nP	žít
především	především	k9	především
v	v	k7c6	v
pobřežních	pobřežní	k2eAgFnPc6d1	pobřežní
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
mezi	mezi	k7c7	mezi
etnickými	etnický	k2eAgMnPc7d1	etnický
Finy	Fin	k1gMnPc7	Fin
a	a	k8xC	a
finskými	finský	k2eAgMnPc7d1	finský
Švédy	Švéd	k1gMnPc7	Švéd
jemný	jemný	k2eAgInSc1d1	jemný
kulturní	kulturní	k2eAgInSc4d1	kulturní
rozdíl	rozdíl	k1gInSc4	rozdíl
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
etničtí	etnický	k2eAgMnPc1d1	etnický
Finové	Fin	k1gMnPc1	Fin
jsou	být	k5eAaImIp3nP	být
spíše	spíše	k9	spíše
zaměřeni	zaměřen	k2eAgMnPc1d1	zaměřen
na	na	k7c4	na
jezera	jezero	k1gNnPc4	jezero
a	a	k8xC	a
les	les	k1gInSc4	les
a	a	k8xC	a
finským	finský	k2eAgMnPc3d1	finský
Švédům	Švéd	k1gMnPc3	Švéd
patří	patřit	k5eAaImIp3nS	patřit
spíše	spíše	k9	spíše
pobřežní	pobřežní	k2eAgFnSc1d1	pobřežní
kultura	kultura	k1gFnSc1	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
tato	tento	k3xDgFnSc1	tento
diferenciace	diferenciace	k1gFnSc1	diferenciace
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
malá	malý	k2eAgFnSc1d1	malá
a	a	k8xC	a
obecně	obecně	k6eAd1	obecně
menší	malý	k2eAgInPc1d2	menší
než	než	k8xS	než
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
kulturami	kultura	k1gFnPc7	kultura
východního	východní	k2eAgMnSc2d1	východní
a	a	k8xC	a
západního	západní	k2eAgNnSc2d1	západní
Finska	Finsko	k1gNnSc2	Finsko
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
menšinovými	menšinový	k2eAgMnPc7d1	menšinový
jazyky	jazyk	k1gMnPc7	jazyk
jsou	být	k5eAaImIp3nP	být
ruština	ruština	k1gFnSc1	ruština
(	(	kIx(	(
<g/>
1,22	[number]	k4	1,22
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
estonština	estonština	k1gFnSc1	estonština
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Laponsku	Laponsko	k1gNnSc6	Laponsko
žije	žít	k5eAaImIp3nS	žít
asi	asi	k9	asi
6500	[number]	k4	6500
Sámů	Sámo	k1gMnPc2	Sámo
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
též	též	k9	též
Laponců	Laponec	k1gMnPc2	Laponec
<g/>
)	)	kIx)	)
s	s	k7c7	s
vlastním	vlastní	k2eAgInSc7d1	vlastní
ugrofinským	ugrofinský	k2eAgInSc7d1	ugrofinský
jazykem	jazyk	k1gInSc7	jazyk
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
sámských	sámský	k2eAgInPc2d1	sámský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Těmi	ten	k3xDgFnPc7	ten
mluví	mluvit	k5eAaImIp3nS	mluvit
asi	asi	k9	asi
3000	[number]	k4	3000
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Převažující	převažující	k2eAgNnSc1d1	převažující
náboženství	náboženství	k1gNnSc1	náboženství
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
je	být	k5eAaImIp3nS	být
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
Finů	Fin	k1gMnPc2	Fin
(	(	kIx(	(
<g/>
76	[number]	k4	76
%	%	kIx~	%
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
k	k	k7c3	k
Finské	finský	k2eAgFnSc3d1	finská
evangelické	evangelický	k2eAgFnSc3d1	evangelická
luteránské	luteránský	k2eAgFnSc3d1	luteránská
církvi	církev	k1gFnSc3	církev
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
%	%	kIx~	%
menšina	menšina	k1gFnSc1	menšina
potom	potom	k6eAd1	potom
k	k	k7c3	k
Finské	finský	k2eAgFnSc3d1	finská
pravoslavné	pravoslavný	k2eAgFnSc3d1	pravoslavná
církvi	církev	k1gFnSc3	církev
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
věřících	věřící	k1gMnPc2	věřící
tvoří	tvořit	k5eAaImIp3nP	tvořit
poměrně	poměrně	k6eAd1	poměrně
malé	malý	k2eAgFnPc1d1	malá
skupiny	skupina	k1gFnPc1	skupina
protestantů	protestant	k1gMnPc2	protestant
<g/>
,	,	kIx,	,
římští	římský	k2eAgMnPc1d1	římský
katolíci	katolík	k1gMnPc1	katolík
<g/>
,	,	kIx,	,
muslimové	muslim	k1gMnPc1	muslim
a	a	k8xC	a
židé	žid	k1gMnPc1	žid
<g/>
.	.	kIx.	.
21	[number]	k4	21
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
.	.	kIx.	.
</s>
<s>
Nutno	nutno	k6eAd1	nutno
zdůraznit	zdůraznit	k5eAaPmF	zdůraznit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c7	mezi
aktivně	aktivně	k6eAd1	aktivně
věřící	věřící	k1gFnSc7	věřící
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
lidé	člověk	k1gMnPc1	člověk
platí	platit	k5eAaImIp3nP	platit
církevní	církevní	k2eAgFnSc4d1	církevní
daň	daň	k1gFnSc4	daň
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
náleží	náležet	k5eAaImIp3nS	náležet
k	k	k7c3	k
některé	některý	k3yIgFnSc3	některý
z	z	k7c2	z
církví	církev	k1gFnPc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
Zimní	zimní	k2eAgInPc4d1	zimní
válce	válec	k1gInPc4	válec
(	(	kIx(	(
<g/>
potvrzené	potvrzený	k2eAgFnPc4d1	potvrzená
výsledkem	výsledek	k1gInSc7	výsledek
Pokračovací	pokračovací	k2eAgFnSc2d1	pokračovací
války	válka	k1gFnSc2	válka
<g/>
)	)	kIx)	)
muselo	muset	k5eAaImAgNnS	muset
být	být	k5eAaImF	být
12	[number]	k4	12
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
Finska	Finsko	k1gNnSc2	Finsko
přesídleno	přesídlen	k2eAgNnSc1d1	přesídlen
<g/>
.	.	kIx.	.
</s>
<s>
Válečné	válečný	k2eAgFnPc4d1	válečná
reparace	reparace	k1gFnPc4	reparace
<g/>
,	,	kIx,	,
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
a	a	k8xC	a
nejistota	nejistota	k1gFnSc1	nejistota
v	v	k7c6	v
šancích	šance	k1gFnPc6	šance
zůstat	zůstat	k5eAaPmF	zůstat
suverénním	suverénní	k2eAgInSc7d1	suverénní
a	a	k8xC	a
nezávislým	závislý	k2eNgInSc7d1	nezávislý
státem	stát	k1gInSc7	stát
na	na	k7c6	na
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
vedla	vést	k5eAaImAgFnS	vést
ke	k	k7c3	k
značné	značný	k2eAgFnSc3d1	značná
emigraci	emigrace	k1gFnSc3	emigrace
slábnoucí	slábnoucí	k2eAgFnSc1d1	slábnoucí
až	až	k6eAd1	až
v	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
emigrovalo	emigrovat	k5eAaBmAgNnS	emigrovat
zhruba	zhruba	k6eAd1	zhruba
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
Finů	Fin	k1gMnPc2	Fin
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
do	do	k7c2	do
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
polovina	polovina	k1gFnSc1	polovina
emigrantů	emigrant	k1gMnPc2	emigrant
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
vrátila	vrátit	k5eAaPmAgFnS	vrátit
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
Finsko	Finsko	k1gNnSc1	Finsko
přijímá	přijímat	k5eAaImIp3nS	přijímat
uprchlíky	uprchlík	k1gMnPc4	uprchlík
a	a	k8xC	a
imigranty	imigrant	k1gMnPc4	imigrant
v	v	k7c6	v
měřítku	měřítko	k1gNnSc6	měřítko
porovnatelném	porovnatelný	k2eAgNnSc6d1	porovnatelné
se	s	k7c7	s
skandinávskými	skandinávský	k2eAgFnPc7d1	skandinávská
zeměmi	zem	k1gFnPc7	zem
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
jejich	jejich	k3xOp3gInSc1	jejich
celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
mnoho	mnoho	k6eAd1	mnoho
menší	malý	k2eAgFnPc4d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Značný	značný	k2eAgInSc4d1	značný
podíl	podíl	k1gInSc4	podíl
imigrantů	imigrant	k1gMnPc2	imigrant
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
bývalého	bývalý	k2eAgInSc2d1	bývalý
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
etnické	etnický	k2eAgFnPc4d1	etnická
Finy	Fina	k1gFnPc4	Fina
<g/>
.	.	kIx.	.
</s>
<s>
Nejpočetnější	početní	k2eAgFnPc1d3	nejpočetnější
přistěhovalecké	přistěhovalecký	k2eAgFnPc1d1	přistěhovalecká
skupiny	skupina	k1gFnPc1	skupina
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
jsou	být	k5eAaImIp3nP	být
Švédové	Švéd	k1gMnPc1	Švéd
<g/>
,	,	kIx,	,
Rusové	Rus	k1gMnPc1	Rus
<g/>
,	,	kIx,	,
Estonci	Estonec	k1gMnPc1	Estonec
a	a	k8xC	a
Somálci	Somálec	k1gMnPc1	Somálec
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1990-2013	[number]	k4	1990-2013
se	se	k3xPyFc4	se
podíl	podíl	k1gInSc1	podíl
rusů	rus	k1gMnPc2	rus
zvětšil	zvětšit	k5eAaPmAgInS	zvětšit
z	z	k7c2	z
0,08	[number]	k4	0,08
%	%	kIx~	%
na	na	k7c4	na
1,22	[number]	k4	1,22
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
mluví	mluvit	k5eAaImIp3nS	mluvit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvaceti	dvacet	k4xCc7	dvacet
většími	veliký	k2eAgInPc7d2	veliký
jazyky	jazyk	k1gInPc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Finská	finský	k2eAgFnSc1d1	finská
populace	populace	k1gFnSc1	populace
byla	být	k5eAaImAgFnS	být
vždy	vždy	k6eAd1	vždy
koncentrovaná	koncentrovaný	k2eAgFnSc1d1	koncentrovaná
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
urbanizace	urbanizace	k1gFnPc1	urbanizace
ve	v	k7c4	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
ještě	ještě	k6eAd1	ještě
zvýraznila	zvýraznit	k5eAaPmAgFnS	zvýraznit
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
a	a	k8xC	a
nejdůležitější	důležitý	k2eAgNnPc1d3	nejdůležitější
města	město	k1gNnPc1	město
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
jsou	být	k5eAaImIp3nP	být
Helsinky	Helsinky	k1gFnPc1	Helsinky
<g/>
,	,	kIx,	,
Tampere	Tamper	k1gMnSc5	Tamper
<g/>
,	,	kIx,	,
Turku	turek	k1gInSc6	turek
a	a	k8xC	a
Oulu	Oulus	k1gInSc6	Oulus
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Oulu	Oula	k1gFnSc4	Oula
je	být	k5eAaImIp3nS	být
jediné	jediný	k2eAgNnSc1d1	jediné
město	město	k1gNnSc1	město
v	v	k7c6	v
severním	severní	k2eAgNnSc6d1	severní
Finsku	Finsko	k1gNnSc6	Finsko
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
000	[number]	k4	000
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Stará	starý	k2eAgFnSc1d1	stará
finská	finský	k2eAgFnSc1d1	finská
kultura	kultura	k1gFnSc1	kultura
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
původního	původní	k2eAgNnSc2d1	původní
finského	finský	k2eAgNnSc2d1	finské
pohanství	pohanství	k1gNnSc2	pohanství
<g/>
.	.	kIx.	.
</s>
<s>
Vánoční	vánoční	k2eAgInPc4d1	vánoční
dárky	dárek	k1gInPc4	dárek
dětem	dítě	k1gFnPc3	dítě
nosí	nosit	k5eAaImIp3nS	nosit
Joulupukki	Joulupukki	k1gNnSc1	Joulupukki
<g/>
,	,	kIx,	,
místní	místní	k2eAgFnSc1d1	místní
varianta	varianta	k1gFnSc1	varianta
Santy	Santa	k1gFnSc2	Santa
Clause	Clause	k1gFnSc2	Clause
<g/>
.	.	kIx.	.
</s>
<s>
Národním	národní	k2eAgInSc7d1	národní
finským	finský	k2eAgInSc7d1	finský
eposem	epos	k1gInSc7	epos
je	být	k5eAaImIp3nS	být
Kalevala	Kalevala	k1gFnSc1	Kalevala
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
spisovatelů	spisovatel	k1gMnPc2	spisovatel
jmenujme	jmenovat	k5eAaBmRp1nP	jmenovat
Miku	Mik	k1gMnSc3	Mik
Waltariho	Waltari	k1gMnSc4	Waltari
a	a	k8xC	a
Tove	Tov	k1gMnSc4	Tov
Janssonovou	Janssonová	k1gFnSc4	Janssonová
<g/>
,	,	kIx,	,
autorku	autorka	k1gFnSc4	autorka
oblíbených	oblíbený	k2eAgFnPc2d1	oblíbená
pohádek	pohádka	k1gFnPc2	pohádka
o	o	k7c6	o
Mumíncích	Mumínek	k1gInPc6	Mumínek
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
oblíbeným	oblíbený	k2eAgMnSc7d1	oblíbený
je	být	k5eAaImIp3nS	být
sport	sport	k1gInSc4	sport
<g/>
,	,	kIx,	,
úspěšnými	úspěšný	k2eAgMnPc7d1	úspěšný
sportovci	sportovec	k1gMnPc1	sportovec
byli	být	k5eAaImAgMnP	být
či	či	k8xC	či
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
Paavo	Paavo	k1gNnSc4	Paavo
Nurmi	Nur	k1gFnPc7	Nur
<g/>
,	,	kIx,	,
Matti	Matti	k1gNnSc7	Matti
Nykänen	Nykänna	k1gFnPc2	Nykänna
<g/>
,	,	kIx,	,
Teemu	Teem	k1gInSc2	Teem
Selänne	Selänn	k1gInSc5	Selänn
<g/>
,	,	kIx,	,
Mika	Mik	k1gMnSc2	Mik
Häkkinen	Häkkinno	k1gNnPc2	Häkkinno
<g/>
,	,	kIx,	,
Kimi	Kim	k1gMnPc1	Kim
Räikkönen	Räikkönno	k1gNnPc2	Räikkönno
či	či	k8xC	či
Juho	Juho	k6eAd1	Juho
Hänninen	Hänninen	k2eAgInSc1d1	Hänninen
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
životu	život	k1gInSc3	život
každého	každý	k3xTgMnSc4	každý
Fina	Fin	k1gMnSc4	Fin
neodmyslitelně	odmyslitelně	k6eNd1	odmyslitelně
patří	patřit	k5eAaImIp3nS	patřit
sauna	sauna	k1gFnSc1	sauna
<g/>
,	,	kIx,	,
náleží	náležet	k5eAaImIp3nS	náležet
zde	zde	k6eAd1	zde
ke	k	k7c3	k
standardnímu	standardní	k2eAgNnSc3d1	standardní
vybavení	vybavení	k1gNnSc3	vybavení
domácnosti	domácnost	k1gFnSc2	domácnost
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
největšího	veliký	k2eAgMnSc2d3	veliký
architekta	architekt	k1gMnSc2	architekt
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
Alvar	Alvar	k1gInSc1	Alvar
Aalto	Aalto	k1gNnSc1	Aalto
<g/>
.	.	kIx.	.
</s>
<s>
Finská	finský	k2eAgFnSc1d1	finská
kuchyně	kuchyně	k1gFnSc1	kuchyně
vyniká	vynikat	k5eAaImIp3nS	vynikat
střídmostí	střídmost	k1gFnSc7	střídmost
a	a	k8xC	a
jednoduchostí	jednoduchost	k1gFnSc7	jednoduchost
<g/>
.	.	kIx.	.
</s>
<s>
Hodně	hodně	k6eAd1	hodně
se	se	k3xPyFc4	se
konzumují	konzumovat	k5eAaBmIp3nP	konzumovat
ryby	ryba	k1gFnPc1	ryba
a	a	k8xC	a
mléčné	mléčný	k2eAgInPc1d1	mléčný
výrobky	výrobek	k1gInPc1	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
místní	místní	k2eAgFnSc4d1	místní
specialitu	specialita	k1gFnSc4	specialita
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
například	například	k6eAd1	například
sobí	sobí	k2eAgNnSc1d1	sobí
<g/>
,	,	kIx,	,
medvědí	medvědí	k2eAgNnSc1d1	medvědí
nebo	nebo	k8xC	nebo
losí	losí	k2eAgNnSc1d1	losí
maso	maso	k1gNnSc1	maso
<g/>
.	.	kIx.	.
</s>
<s>
Finským	finský	k2eAgInSc7d1	finský
národním	národní	k2eAgInSc7d1	národní
hudebním	hudební	k2eAgInSc7d1	hudební
nástrojem	nástroj	k1gInSc7	nástroj
je	být	k5eAaImIp3nS	být
strunné	strunný	k2eAgNnSc1d1	strunné
kantele	kantele	k6eAd1	kantele
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
finskými	finský	k2eAgMnPc7d1	finský
skladateli	skladatel	k1gMnPc7	skladatel
vynikal	vynikat	k5eAaImAgMnS	vynikat
zejména	zejména	k9	zejména
Jean	Jean	k1gMnSc1	Jean
Sibelius	Sibelius	k1gMnSc1	Sibelius
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
oblíbené	oblíbený	k2eAgFnPc1d1	oblíbená
jsou	být	k5eAaImIp3nP	být
rockové	rockový	k2eAgFnPc1d1	rocková
a	a	k8xC	a
metalové	metalový	k2eAgFnPc1d1	metalová
kapely	kapela	k1gFnPc1	kapela
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Sonata	Sonat	k2eAgFnSc1d1	Sonata
Arctica	Arctica	k1gFnSc1	Arctica
<g/>
,	,	kIx,	,
Sunrise	Sunrise	k1gFnSc1	Sunrise
Avenue	avenue	k1gFnSc1	avenue
<g/>
,	,	kIx,	,
Stratovarius	Stratovarius	k1gMnSc1	Stratovarius
<g/>
,	,	kIx,	,
Nightwish	Nightwish	k1gMnSc1	Nightwish
<g/>
,	,	kIx,	,
HIM	HIM	kA	HIM
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Rasmus	Rasmus	k1gMnSc1	Rasmus
<g/>
,	,	kIx,	,
Apocalyptica	Apocalyptica	k1gMnSc1	Apocalyptica
<g/>
,	,	kIx,	,
Children	Childrna	k1gFnPc2	Childrna
of	of	k?	of
Bodom	Bodom	k1gInSc1	Bodom
<g/>
,	,	kIx,	,
Waltari	Waltari	k1gNnSc1	Waltari
<g/>
,	,	kIx,	,
Turmion	Turmion	k1gInSc1	Turmion
Kätilöt	Kätilöta	k1gFnPc2	Kätilöta
<g/>
,	,	kIx,	,
Lordi	lord	k1gMnPc1	lord
<g/>
,	,	kIx,	,
Korpiklaani	Korpiklaan	k1gMnPc1	Korpiklaan
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Známou	známý	k2eAgFnSc7d1	známá
sólovou	sólový	k2eAgFnSc7d1	sólová
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
je	být	k5eAaImIp3nS	být
Tarja	Tarja	k1gFnSc1	Tarja
Turunen	Turunna	k1gFnPc2	Turunna
a	a	k8xC	a
ze	z	k7c2	z
zpěváků	zpěvák	k1gMnPc2	zpěvák
například	například	k6eAd1	například
Timo	Timo	k6eAd1	Timo
Kotipelto	Kotipelt	k2eAgNnSc1d1	Kotipelt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
finská	finský	k2eAgFnSc1d1	finská
hard	hard	k6eAd1	hard
rocková	rockový	k2eAgFnSc1d1	rocková
<g/>
/	/	kIx~	/
<g/>
heavy	heava	k1gFnSc2	heava
metalová	metalový	k2eAgFnSc1d1	metalová
skupina	skupina	k1gFnSc1	skupina
Lordi	lord	k1gMnPc1	lord
Velkou	velký	k2eAgFnSc4d1	velká
cenu	cena	k1gFnSc4	cena
Eurovize	Eurovize	k1gFnSc2	Eurovize
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
Hard	Hard	k1gInSc4	Hard
Rock	rock	k1gInSc1	rock
Hallelujah	Hallelujah	k1gInSc1	Hallelujah
<g/>
.	.	kIx.	.
</s>
<s>
Známí	známý	k1gMnPc1	známý
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
například	například	k6eAd1	například
finská	finský	k2eAgFnSc1d1	finská
folková	folkový	k2eAgFnSc1d1	folková
skupina	skupina	k1gFnSc1	skupina
Värttinä	Värttinä	k1gFnSc2	Värttinä
<g/>
,	,	kIx,	,
rockoví	rockový	k2eAgMnPc1d1	rockový
Leningrad	Leningrad	k1gInSc4	Leningrad
Cowboys	Cowboysa	k1gFnPc2	Cowboysa
a	a	k8xC	a
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
velmi	velmi	k6eAd1	velmi
úspěšní	úspěšný	k2eAgMnPc1d1	úspěšný
Hurriganes	Hurriganes	k1gInSc4	Hurriganes
<g/>
.	.	kIx.	.
</s>
<s>
Finská	finský	k2eAgFnSc1d1	finská
metalová	metalový	k2eAgFnSc1d1	metalová
scéna	scéna	k1gFnSc1	scéna
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k8xC	jako
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
oficiální	oficiální	k2eAgInPc1d1	oficiální
svátky	svátek	k1gInPc1	svátek
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
jsou	být	k5eAaImIp3nP	být
ustanoveny	ustanovit	k5eAaPmNgFnP	ustanovit
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Svátky	svátek	k1gInPc1	svátek
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
křesťanské	křesťanský	k2eAgNnSc4d1	křesťanské
a	a	k8xC	a
sekulární	sekulární	k2eAgNnSc4d1	sekulární
(	(	kIx(	(
<g/>
světské	světský	k2eAgNnSc4d1	světské
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejdůležitější	důležitý	k2eAgInPc4d3	nejdůležitější
křesťanské	křesťanský	k2eAgInPc4d1	křesťanský
svátky	svátek	k1gInPc4	svátek
patří	patřit	k5eAaImIp3nP	patřit
Vánoce	Vánoce	k1gFnPc1	Vánoce
<g/>
,	,	kIx,	,
Tří	tři	k4xCgNnPc2	tři
králů	král	k1gMnPc2	král
<g/>
,	,	kIx,	,
Velikonoce	Velikonoce	k1gFnPc4	Velikonoce
<g/>
,	,	kIx,	,
Nanebevstoupení	nanebevstoupení	k1gNnSc4	nanebevstoupení
Páně	páně	k2eAgFnSc2d1	páně
<g/>
,	,	kIx,	,
Svatodušní	svatodušní	k2eAgFnSc2d1	svatodušní
neděle	neděle	k1gFnSc2	neděle
a	a	k8xC	a
slavnost	slavnost	k1gFnSc1	slavnost
Všech	všecek	k3xTgFnPc2	všecek
svatých	svatá	k1gFnPc2	svatá
<g/>
.	.	kIx.	.
</s>
<s>
Sekulárními	sekulární	k2eAgInPc7d1	sekulární
svátky	svátek	k1gInPc7	svátek
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
Nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
máj	máj	k1gFnSc1	máj
(	(	kIx(	(
<g/>
finsky	finsky	k6eAd1	finsky
Vappu	Vapp	k1gInSc3	Vapp
<g/>
)	)	kIx)	)
a	a	k8xC	a
letní	letní	k2eAgInSc4d1	letní
slunovrat	slunovrat	k1gInSc4	slunovrat
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
oficiální	oficiální	k2eAgInPc4d1	oficiální
svátky	svátek	k1gInPc4	svátek
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
navíc	navíc	k6eAd1	navíc
všechny	všechen	k3xTgFnPc4	všechen
neděle	neděle	k1gFnPc4	neděle
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
pracovní	pracovní	k2eAgInSc1d1	pracovní
týden	týden	k1gInSc1	týden
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
zkrátil	zkrátit	k5eAaPmAgMnS	zkrátit
na	na	k7c4	na
40	[number]	k4	40
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
ze	z	k7c2	z
sobot	sobota	k1gFnPc2	sobota
se	se	k3xPyFc4	se
prakticky	prakticky	k6eAd1	prakticky
staly	stát	k5eAaPmAgFnP	stát
také	také	k9	také
svátky	svátek	k1gInPc4	svátek
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
ne	ne	k9	ne
oficiální	oficiální	k2eAgFnSc1d1	oficiální
<g/>
.	.	kIx.	.
</s>
<s>
Velikonoční	velikonoční	k2eAgFnSc1d1	velikonoční
neděle	neděle	k1gFnSc1	neděle
a	a	k8xC	a
slavnost	slavnost	k1gFnSc1	slavnost
Všech	všecek	k3xTgFnPc2	všecek
svatých	svatá	k1gFnPc2	svatá
jsou	být	k5eAaImIp3nP	být
neděle	neděle	k1gFnSc2	neděle
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
předchází	předcházet	k5eAaImIp3nS	předcházet
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
druh	druh	k1gInSc1	druh
sobot	sobota	k1gFnPc2	sobota
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
musely	muset	k5eAaImAgInP	muset
mít	mít	k5eAaImF	mít
obchody	obchod	k1gInPc1	obchod
ze	z	k7c2	z
zákona	zákon	k1gInSc2	zákon
v	v	k7c4	v
neděli	neděle	k1gFnSc4	neděle
zavřeno	zavřít	k5eAaPmNgNnS	zavřít
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
byly	být	k5eAaImAgInP	být
letní	letní	k2eAgInPc1d1	letní
měsíce	měsíc	k1gInPc1	měsíc
(	(	kIx(	(
<g/>
od	od	k7c2	od
května	květen	k1gInSc2	květen
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
<g/>
)	)	kIx)	)
a	a	k8xC	a
předvánoční	předvánoční	k2eAgFnSc1d1	předvánoční
sezóna	sezóna	k1gFnSc1	sezóna
(	(	kIx(	(
<g/>
listopad	listopad	k1gInSc1	listopad
a	a	k8xC	a
prosinec	prosinec	k1gInSc1	prosinec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
zákon	zákon	k1gInSc1	zákon
reguluje	regulovat	k5eAaImIp3nS	regulovat
otevírací	otevírací	k2eAgFnSc4d1	otevírací
dobu	doba	k1gFnSc4	doba
obchodů	obchod	k1gInPc2	obchod
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
400	[number]	k4	400
m	m	kA	m
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
obchody	obchod	k1gInPc1	obchod
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
v	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
od	od	k7c2	od
7,00	[number]	k4	7,00
do	do	k7c2	do
18,00	[number]	k4	18,00
a	a	k8xC	a
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
od	od	k7c2	od
12,00	[number]	k4	12,00
do	do	k7c2	do
18,00	[number]	k4	18,00
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
ostatní	ostatní	k2eAgInPc4d1	ostatní
dny	den	k1gInPc4	den
jsou	být	k5eAaImIp3nP	být
otevřeny	otevřít	k5eAaPmNgFnP	otevřít
mezi	mezi	k7c4	mezi
7,00	[number]	k4	7,00
a	a	k8xC	a
21,00	[number]	k4	21,00
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
oficiální	oficiální	k2eAgInPc1d1	oficiální
svátky	svátek	k1gInPc1	svátek
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc1	tento
obchody	obchod	k1gInPc1	obchod
zavřené	zavřený	k2eAgInPc1d1	zavřený
<g/>
.	.	kIx.	.
</s>
<s>
Obchody	obchod	k1gInPc1	obchod
s	s	k7c7	s
méně	málo	k6eAd2	málo
než	než	k8xS	než
400	[number]	k4	400
m	m	kA	m
<g/>
2	[number]	k4	2
plochy	plocha	k1gFnPc1	plocha
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
kromě	kromě	k7c2	kromě
oficiálních	oficiální	k2eAgInPc2d1	oficiální
svátků	svátek	k1gInPc2	svátek
a	a	k8xC	a
některých	některý	k3yIgFnPc2	některý
nedělí	neděle	k1gFnPc2	neděle
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
třeba	třeba	k6eAd1	třeba
Den	den	k1gInSc4	den
Matek	matka	k1gFnPc2	matka
(	(	kIx(	(
<g/>
druhá	druhý	k4xOgFnSc1	druhý
neděle	neděle	k1gFnSc1	neděle
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Den	den	k1gInSc1	den
otců	otec	k1gMnPc2	otec
(	(	kIx(	(
<g/>
druhá	druhý	k4xOgFnSc1	druhý
neděle	neděle	k1gFnSc1	neděle
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
jejich	jejich	k3xOp3gFnSc4	jejich
otevírací	otevírací	k2eAgFnSc4d1	otevírací
dobu	doba	k1gFnSc4	doba
ale	ale	k8xC	ale
nijak	nijak	k6eAd1	nijak
nereguluje	regulovat	k5eNaImIp3nS	regulovat
<g/>
.	.	kIx.	.
</s>
<s>
Datový	datový	k2eAgInSc1d1	datový
formát	formát	k1gInSc1	formát
<g/>
:	:	kIx,	:
DD	DD	kA	DD
<g/>
.	.	kIx.	.
<g/>
MM	mm	kA	mm
<g/>
.	.	kIx.	.
<g/>
YYYY	YYYY	kA	YYYY
(	(	kIx(	(
<g/>
nepoužívají	používat	k5eNaImIp3nP	používat
se	se	k3xPyFc4	se
nuly	nula	k1gFnSc2	nula
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
<g/>
)	)	kIx)	)
Napětí	napětí	k1gNnSc1	napětí
<g/>
:	:	kIx,	:
220-240	[number]	k4	220-240
V	V	kA	V
<g/>
,	,	kIx,	,
50	[number]	k4	50
Hz	Hz	kA	Hz
<g/>
;	;	kIx,	;
Zásuvky	zásuvka	k1gFnSc2	zásuvka
<g/>
:	:	kIx,	:
<g/>
typ	typ	k1gInSc1	typ
Schuko	Schuko	k1gNnSc4	Schuko
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
označení	označení	k1gNnSc2	označení
F	F	kA	F
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stejné	stejný	k2eAgFnPc1d1	stejná
jako	jako	k8xC	jako
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
většině	většina	k1gFnSc6	většina
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
české	český	k2eAgInPc1d1	český
spotřebiče	spotřebič	k1gInPc1	spotřebič
s	s	k7c7	s
francouzským	francouzský	k2eAgInSc7d1	francouzský
typem	typ	k1gInSc7	typ
vidlice	vidlice	k1gFnSc2	vidlice
E	E	kA	E
<g/>
/	/	kIx~	/
<g/>
F	F	kA	F
-	-	kIx~	-
EuroSchuko	EuroSchuko	k1gNnSc4	EuroSchuko
lze	lze	k6eAd1	lze
používat	používat	k5eAaImF	používat
normálně	normálně	k6eAd1	normálně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
PSČ	PSČ	kA	PSČ
<g/>
:	:	kIx,	:
5	[number]	k4	5
číslic	číslice	k1gFnPc2	číslice
<g/>
.	.	kIx.	.
</s>
<s>
Mobilní	mobilní	k2eAgFnPc1d1	mobilní
sítě	síť	k1gFnPc1	síť
a	a	k8xC	a
technologie	technologie	k1gFnPc1	technologie
<g/>
:	:	kIx,	:
GSM	GSM	kA	GSM
900	[number]	k4	900
<g/>
,	,	kIx,	,
GSM	GSM	kA	GSM
1800	[number]	k4	1800
<g/>
;	;	kIx,	;
GSM	GSM	kA	GSM
<g/>
/	/	kIx~	/
<g/>
GPRS	GPRS	kA	GPRS
<g/>
/	/	kIx~	/
<g/>
EDGE	EDGE	kA	EDGE
<g/>
/	/	kIx~	/
<g/>
UMTS	UMTS	kA	UMTS
<g/>
/	/	kIx~	/
<g/>
LTE	LTE	kA	LTE
Měrná	měrný	k2eAgFnSc1d1	měrná
soustava	soustava	k1gFnSc1	soustava
<g/>
:	:	kIx,	:
SI	si	k1gNnSc1	si
(	(	kIx(	(
<g/>
metrická	metrický	k2eAgFnSc1d1	metrická
<g/>
)	)	kIx)	)
IMD	IMD	kA	IMD
International	International	k1gFnSc1	International
<g/>
,	,	kIx,	,
Žebříček	žebříček	k1gInSc1	žebříček
konkurenceschopnosti	konkurenceschopnost	k1gFnSc2	konkurenceschopnost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
8	[number]	k4	8
<g/>
.	.	kIx.	.
ze	z	k7c2	z
60	[number]	k4	60
ekonomik	ekonomika	k1gFnPc2	ekonomika
(	(	kIx(	(
<g/>
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
regionů	region	k1gInPc2	region
<g/>
)	)	kIx)	)
OECD	OECD	kA	OECD
<g/>
,	,	kIx,	,
Žebříček	žebříček	k1gInSc1	žebříček
připravenosti	připravenost	k1gFnSc2	připravenost
studentů	student	k1gMnPc2	student
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
ze	z	k7c2	z
41	[number]	k4	41
zemí	zem	k1gFnPc2	zem
Reportéři	reportér	k1gMnPc1	reportér
bez	bez	k7c2	bez
hranic	hranice	k1gFnPc2	hranice
<g/>
,	,	kIx,	,
Žebříček	žebříček	k1gInSc1	žebříček
svobody	svoboda	k1gFnSc2	svoboda
tisku	tisk	k1gInSc2	tisk
(	(	kIx(	(
<g/>
říjen	říjen	k1gInSc1	říjen
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
<g />
.	.	kIx.	.
</s>
<s>
ze	z	k7c2	z
166	[number]	k4	166
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Islandem	Island	k1gInSc7	Island
<g/>
,	,	kIx,	,
Nizozemím	Nizozemí	k1gNnSc7	Nizozemí
a	a	k8xC	a
Norskem	Norsko	k1gNnSc7	Norsko
<g/>
)	)	kIx)	)
Save	Save	k1gNnSc1	Save
the	the	k?	the
Children	Childrno	k1gNnPc2	Childrno
<g/>
,	,	kIx,	,
Stav	stav	k1gInSc1	stav
matek	matka	k1gFnPc2	matka
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
:	:	kIx,	:
2	[number]	k4	2
<g/>
.	.	kIx.	.
z	z	k7c2	z
119	[number]	k4	119
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Dánskem	Dánsko	k1gNnSc7	Dánsko
<g/>
)	)	kIx)	)
Transparency	Transparenc	k2eAgFnPc1d1	Transparenc
International	International	k1gFnPc1	International
<g/>
,	,	kIx,	,
Index	index	k1gInSc1	index
korupce	korupce	k1gFnSc2	korupce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
ze	z	k7c2	z
146	[number]	k4	146
zemí	zem	k1gFnPc2	zem
UNDP	UNDP	kA	UNDP
<g/>
:	:	kIx,	:
Zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
lidském	lidský	k2eAgInSc6d1	lidský
vývoji	vývoj	k1gInSc6	vývoj
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
.	.	kIx.	.
ze	z	k7c2	z
177	[number]	k4	177
zemí	zem	k1gFnPc2	zem
Světové	světový	k2eAgNnSc4d1	světové
ekonomické	ekonomický	k2eAgNnSc4d1	ekonomické
fórum	fórum	k1gNnSc4	fórum
<g/>
,	,	kIx,	,
Zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
konkurenceschopnosti	konkurenceschopnost	k1gFnSc6	konkurenceschopnost
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
2004	[number]	k4	2004
<g/>
-	-	kIx~	-
<g/>
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
ze	z	k7c2	z
104	[number]	k4	104
zemí	zem	k1gFnPc2	zem
</s>
