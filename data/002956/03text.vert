<s>
Sebastian	Sebastian	k1gMnSc1	Sebastian
je	být	k5eAaImIp3nS	být
mužské	mužský	k2eAgNnSc4d1	mužské
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
řeckého	řecký	k2eAgInSc2d1	řecký
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
z	z	k7c2	z
latinského	latinský	k2eAgNnSc2d1	latinské
jména	jméno	k1gNnSc2	jméno
Sebastianus	Sebastianus	k1gInSc1	Sebastianus
a	a	k8xC	a
vykládá	vykládat	k5eAaImIp3nS	vykládat
se	se	k3xPyFc4	se
jako	jako	k9	jako
"	"	kIx"	"
<g/>
sebastský	sebastský	k2eAgMnSc1d1	sebastský
<g/>
,	,	kIx,	,
obyvatel	obyvatel	k1gMnSc1	obyvatel
města	město	k1gNnSc2	město
Sebasty	Sebast	k1gInPc4	Sebast
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
původní	původní	k2eAgInSc1d1	původní
význam	význam	k1gInSc1	význam
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
slova	slovo	k1gNnSc2	slovo
σ	σ	k?	σ
(	(	kIx(	(
<g/>
sebastos	sebastos	k1gMnSc1	sebastos
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
vznešený	vznešený	k2eAgMnSc1d1	vznešený
<g/>
,	,	kIx,	,
ctihodný	ctihodný	k2eAgMnSc1d1	ctihodný
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
σ	σ	k?	σ
(	(	kIx(	(
<g/>
sebas	sebas	k1gMnSc1	sebas
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
hrůza	hrůza	k1gFnSc1	hrůza
<g/>
,	,	kIx,	,
úcta	úcta	k1gFnSc1	úcta
<g/>
,	,	kIx,	,
strach	strach	k1gInSc1	strach
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
varianty	varianta	k1gFnPc1	varianta
jména	jméno	k1gNnSc2	jméno
jsou	být	k5eAaImIp3nP	být
Sebastian	Sebastian	k1gMnSc1	Sebastian
<g/>
,	,	kIx,	,
Sebastián	Sebastián	k1gMnSc1	Sebastián
<g/>
,	,	kIx,	,
počeštěně	počeštěně	k6eAd1	počeštěně
Šebestián	Šebestián	k1gMnSc1	Šebestián
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
staršího	starý	k2eAgInSc2d2	starší
kalendáře	kalendář	k1gInSc2	kalendář
má	mít	k5eAaImIp3nS	mít
svátek	svátek	k1gInSc1	svátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
.	.	kIx.	.
</s>
<s>
Slovensky	slovensky	k6eAd1	slovensky
<g/>
:	:	kIx,	:
Šebastián	Šebastián	k1gMnSc1	Šebastián
nebo	nebo	k8xC	nebo
Sebastián	Sebastián	k1gMnSc1	Sebastián
Polsky	Polska	k1gFnSc2	Polska
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
<g/>
,	,	kIx,	,
rumunsky	rumunsky	k6eAd1	rumunsky
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
<g/>
:	:	kIx,	:
Sebastian	Sebastian	k1gMnSc1	Sebastian
Francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
:	:	kIx,	:
Sébastien	Sébastien	k1gInSc1	Sébastien
Italsky	italsky	k6eAd1	italsky
<g/>
:	:	kIx,	:
Sebastiano	Sebastiana	k1gFnSc5	Sebastiana
Španělsky	španělsky	k6eAd1	španělsky
<g/>
:	:	kIx,	:
Sebastián	Sebastián	k1gMnSc1	Sebastián
Rusky	Ruska	k1gFnSc2	Ruska
<g/>
:	:	kIx,	:
Sevastjan	Sevastjan	k1gInSc1	Sevastjan
Srbsky	srbsky	k6eAd1	srbsky
<g/>
:	:	kIx,	:
Sevastijan	Sevastijan	k1gInSc1	Sevastijan
Maďarsky	maďarsky	k6eAd1	maďarsky
<g/>
:	:	kIx,	:
Sebestyén	Sebestyén	k1gInSc1	Sebestyén
svatý	svatý	k1gMnSc1	svatý
Šebestián	Šebestián	k1gMnSc1	Šebestián
Šebestián	Šebestián	k1gMnSc1	Šebestián
-	-	kIx~	-
probošt	probošt	k1gMnSc1	probošt
litoměřické	litoměřický	k2eAgFnSc2d1	Litoměřická
kapituly	kapitula	k1gFnSc2	kapitula
<g />
.	.	kIx.	.
</s>
<s>
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Johann	Johann	k1gMnSc1	Johann
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bach	Bach	k1gMnSc1	Bach
-	-	kIx~	-
německý	německý	k2eAgMnSc1d1	německý
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
virtuóz	virtuóza	k1gFnPc2	virtuóza
hry	hra	k1gFnSc2	hra
na	na	k7c4	na
klávesové	klávesový	k2eAgInPc4d1	klávesový
nástroje	nástroj	k1gInPc4	nástroj
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bach	Bach	k1gMnSc1	Bach
-	-	kIx~	-
kanadský	kanadský	k2eAgMnSc1d1	kanadský
heavy	heava	k1gFnPc4	heava
metalový	metalový	k2eAgMnSc1d1	metalový
zpěvák	zpěvák	k1gMnSc1	zpěvák
Adam	Adam	k1gMnSc1	Adam
Sebastian	Sebastian	k1gMnSc1	Sebastian
Helcelet	Helcelet	k1gInSc1	Helcelet
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
atlet	atlet	k1gMnSc1	atlet
Šebestián	Šebestián	k1gMnSc1	Šebestián
Hněvkovský	Hněvkovský	k2eAgMnSc1d1	Hněvkovský
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
národní	národní	k2eAgMnSc1d1	národní
buditel	buditel	k1gMnSc1	buditel
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
Sebastian	Sebastian	k1gMnSc1	Sebastian
Koch	Koch	k1gMnSc1	Koch
-	-	kIx~	-
německý	německý	k2eAgMnSc1d1	německý
herec	herec	k1gMnSc1	herec
Sébastien	Sébastina	k1gFnPc2	Sébastina
Loeb	Loeb	k1gMnSc1	Loeb
-	-	kIx~	-
Francouz	Francouz	k1gMnSc1	Francouz
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
vícenásobný	vícenásobný	k2eAgMnSc1d1	vícenásobný
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
v	v	k7c6	v
rallye	rallye	k1gFnSc6	rallye
Sebastian	Sebastian	k1gMnSc1	Sebastian
Pawlowski	Pawlowsk	k1gFnSc2	Pawlowsk
-	-	kIx~	-
podnikatel	podnikatel	k1gMnSc1	podnikatel
švýcarského	švýcarský	k2eAgInSc2d1	švýcarský
původu	původ	k1gInSc2	původ
Sebastian	Sebastian	k1gMnSc1	Sebastian
Vettel	Vettel	k1gMnSc1	Vettel
-	-	kIx~	-
německý	německý	k2eAgMnSc1d1	německý
pilot	pilot	k1gMnSc1	pilot
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
Sebastián	Sebastián	k1gMnSc1	Sebastián
I.	I.	kA	I.
Portugalský	portugalský	k2eAgMnSc1d1	portugalský
-	-	kIx~	-
portugalský	portugalský	k2eAgMnSc1d1	portugalský
král	král	k1gMnSc1	král
Sebastian	Sebastian	k1gMnSc1	Sebastian
Stan	stan	k1gInSc1	stan
-	-	kIx~	-
Americký	americký	k2eAgMnSc1d1	americký
herec	herec	k1gMnSc1	herec
Jakub	Jakub	k1gMnSc1	Jakub
Šebesta	Šebesta	k1gMnSc1	Šebesta
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
státní	státní	k2eAgMnSc1d1	státní
úředník	úředník	k1gMnSc1	úředník
Ondřej	Ondřej	k1gMnSc1	Ondřej
Šebesta	Šebesta	k1gMnSc1	Šebesta
(	(	kIx(	(
<g/>
1680	[number]	k4	1680
<g/>
-	-	kIx~	-
<g/>
1715	[number]	k4	1715
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slezský	slezský	k2eAgMnSc1d1	slezský
zbojník	zbojník	k1gMnSc1	zbojník
Pavel	Pavel	k1gMnSc1	Pavel
Jáchym	Jáchym	k1gMnSc1	Jáchym
Šebesta	Šebesta	k1gMnSc1	Šebesta
<g/>
,	,	kIx,	,
česko-rakouský	českoakouský	k2eAgMnSc1d1	česko-rakouský
misionář	misionář	k1gMnSc1	misionář
<g/>
,	,	kIx,	,
antropolog	antropolog	k1gMnSc1	antropolog
a	a	k8xC	a
lingvista	lingvista	k1gMnSc1	lingvista
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Šebestiána	Šebestián	k1gMnSc2	Šebestián
Hora	hora	k1gFnSc1	hora
Svatého	svatý	k2eAgMnSc2d1	svatý
Šebestiána	Šebestián	k1gMnSc2	Šebestián
Hora	hora	k1gFnSc1	hora
svatého	svatý	k2eAgMnSc2d1	svatý
Šebestiána	Šebestián	k1gMnSc2	Šebestián
(	(	kIx(	(
<g/>
Chile	Chile	k1gNnSc2	Chile
<g/>
)	)	kIx)	)
Šebesta	Šebesta	k1gMnSc1	Šebesta
</s>
