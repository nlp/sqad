<p>
<s>
Ohňová	ohňový	k2eAgFnSc1d1	ohňová
země	země	k1gFnSc1	země
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
<g/>
:	:	kIx,	:
Tierra	Tierra	k1gMnSc1	Tierra
del	del	k?	del
Fuego	Fuego	k1gMnSc1	Fuego
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
souostroví	souostroví	k1gNnSc4	souostroví
u	u	k7c2	u
jižního	jižní	k2eAgNnSc2d1	jižní
pobřeží	pobřeží	k1gNnSc2	pobřeží
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
také	také	k9	také
jeho	jeho	k3xOp3gInSc1	jeho
hlavní	hlavní	k2eAgInSc1d1	hlavní
ostrov	ostrov	k1gInSc1	ostrov
(	(	kIx(	(
<g/>
Isla	Isla	k1gFnSc1	Isla
Grande	grand	k1gMnSc5	grand
de	de	k?	de
Tierra	Tierra	k1gMnSc1	Tierra
del	del	k?	del
Fuego	Fuego	k1gMnSc1	Fuego
<g/>
)	)	kIx)	)
u	u	k7c2	u
jižního	jižní	k2eAgInSc2d1	jižní
cípu	cíp	k1gInSc2	cíp
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
Magalhã	Magalhã	k1gMnSc7	Magalhã
průlivem	průliv	k1gInSc7	průliv
(	(	kIx(	(
<g/>
ten	ten	k3xDgInSc1	ten
ji	on	k3xPp3gFnSc4	on
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
od	od	k7c2	od
pevniny	pevnina	k1gFnSc2	pevnina
<g/>
)	)	kIx)	)
a	a	k8xC	a
Drakeovým	Drakeův	k2eAgInSc7d1	Drakeův
průlivem	průliv	k1gInSc7	průliv
<g/>
.	.	kIx.	.
</s>
<s>
Souostroví	souostroví	k1gNnSc1	souostroví
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
hlavního	hlavní	k2eAgInSc2d1	hlavní
ostrova	ostrov	k1gInSc2	ostrov
Isla	Isla	k1gMnSc1	Isla
Grande	grand	k1gMnSc5	grand
de	de	k?	de
Tierra	Tierra	k1gMnSc1	Tierra
del	del	k?	del
Fuego	Fuego	k1gMnSc1	Fuego
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
mezi	mezi	k7c7	mezi
Chile	Chile	k1gNnSc7	Chile
a	a	k8xC	a
Argentinou	Argentina	k1gFnSc7	Argentina
<g/>
,	,	kIx,	,
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
48.100	[number]	k4	48.100
km2	km2	k4	km2
<g/>
,	,	kIx,	,
a	a	k8xC	a
skupinou	skupina	k1gFnSc7	skupina
menších	malý	k2eAgInPc2d2	menší
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
mysu	mys	k1gInSc2	mys
Horn.	Horn.	k1gFnSc2	Horn.
</s>
</p>
<p>
<s>
Ohňovou	ohňový	k2eAgFnSc4d1	ohňová
zemi	zem	k1gFnSc4	zem
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1520	[number]	k4	1520
Fernã	Fernã	k1gMnSc1	Fernã
de	de	k?	de
Magalhã	Magalhã	k1gMnSc1	Magalhã
a	a	k8xC	a
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
ji	on	k3xPp3gFnSc4	on
podle	podle	k7c2	podle
ohňů	oheň	k1gInPc2	oheň
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
uviděl	uvidět	k5eAaPmAgInS	uvidět
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1881	[number]	k4	1881
je	být	k5eAaImIp3nS	být
Ohňová	ohňový	k2eAgFnSc1d1	ohňová
země	země	k1gFnSc1	země
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
mezi	mezi	k7c4	mezi
Argentinu	Argentina	k1gFnSc4	Argentina
a	a	k8xC	a
Chile	Chile	k1gNnSc4	Chile
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgInPc1d1	základní
údaje	údaj	k1gInPc1	údaj
==	==	k?	==
</s>
</p>
<p>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
<g/>
:	:	kIx,	:
71	[number]	k4	71
500	[number]	k4	500
km2	km2	k4	km2
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
73	[number]	k4	73
646	[number]	k4	646
km2	km2	k4	km2
</s>
</p>
<p>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
:	:	kIx,	:
v	v	k7c6	v
argentinské	argentinský	k2eAgFnSc6d1	Argentinská
části	část	k1gFnSc6	část
asi	asi	k9	asi
100	[number]	k4	100
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
v	v	k7c6	v
chilské	chilský	k2eAgFnSc6d1	chilská
části	část	k1gFnSc6	část
asi	asi	k9	asi
7	[number]	k4	7
500	[number]	k4	500
</s>
</p>
<p>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
<g/>
:	:	kIx,	:
oceánické	oceánický	k2eAgFnSc2d1	oceánická
<g/>
,	,	kIx,	,
chladné	chladný	k2eAgFnSc2d1	chladná
</s>
</p>
<p>
<s>
Krajina	Krajina	k1gFnSc1	Krajina
<g/>
:	:	kIx,	:
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
jehličnaté	jehličnatý	k2eAgInPc1d1	jehličnatý
lesy	les	k1gInPc1	les
And	Anda	k1gFnPc2	Anda
(	(	kIx(	(
<g/>
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Cordillera	Cordiller	k1gMnSc2	Cordiller
Darwin	Darwin	k1gMnSc1	Darwin
2469	[number]	k4	2469
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
severních	severní	k2eAgFnPc6d1	severní
částech	část	k1gFnPc6	část
stepi	step	k1gFnSc2	step
</s>
</p>
<p>
<s>
==	==	k?	==
Přehled	přehled	k1gInSc4	přehled
ostrovů	ostrov	k1gInPc2	ostrov
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Souostroví	souostroví	k1gNnSc1	souostroví
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
hlavního	hlavní	k2eAgInSc2d1	hlavní
ostrova	ostrov	k1gInSc2	ostrov
(	(	kIx(	(
<g/>
Isla	Isla	k1gFnSc1	Isla
Grande	grand	k1gMnSc5	grand
de	de	k?	de
Tierra	Tierra	k1gMnSc1	Tierra
del	del	k?	del
Fuego	Fuego	k1gMnSc1	Fuego
<g/>
)	)	kIx)	)
s	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
48	[number]	k4	48
100	[number]	k4	100
km2	km2	k4	km2
<g/>
,	,	kIx,	,
a	a	k8xC	a
skupinou	skupina	k1gFnSc7	skupina
menších	malý	k2eAgInPc2d2	menší
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
ostrov	ostrov	k1gInSc1	ostrov
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
mezi	mezi	k7c4	mezi
dvě	dva	k4xCgFnPc4	dva
země	zem	k1gFnPc4	zem
<g/>
:	:	kIx,	:
18	[number]	k4	18
507,3	[number]	k4	507,3
km2	km2	k4	km2
(	(	kIx(	(
<g/>
38,57	[number]	k4	38,57
%	%	kIx~	%
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
Argentině	Argentina	k1gFnSc3	Argentina
<g/>
,	,	kIx,	,
29	[number]	k4	29
484,7	[number]	k4	484,7
km2	km2	k4	km2
(	(	kIx(	(
<g/>
61,43	[number]	k4	61,43
%	%	kIx~	%
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
území	území	k1gNnSc3	území
Chile	Chile	k1gNnSc2	Chile
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
území	území	k1gNnSc3	území
Chile	Chile	k1gNnSc2	Chile
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Polovina	polovina	k1gFnSc1	polovina
ostrova	ostrov	k1gInSc2	ostrov
Isla	Islum	k1gNnSc2	Islum
Grande	grand	k1gMnSc5	grand
de	de	k?	de
Tierra	Tierr	k1gMnSc2	Tierr
del	del	k?	del
Fuego	Fuego	k1gNnSc1	Fuego
a	a	k8xC	a
ostrovy	ostrov	k1gInPc1	ostrov
na	na	k7c4	na
západ	západ	k1gInSc4	západ
a	a	k8xC	a
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
Punta	punto	k1gNnSc2	punto
Arenas	Arenasa	k1gFnPc2	Arenasa
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
chilské	chilský	k2eAgNnSc1d1	Chilské
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
Porvenir	Porvenir	k1gInSc4	Porvenir
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
území	území	k1gNnSc3	území
Argentiny	Argentina	k1gFnSc2	Argentina
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Východní	východní	k2eAgFnSc1d1	východní
část	část	k1gFnSc1	část
souostroví	souostroví	k1gNnSc2	souostroví
<g/>
,	,	kIx,	,
ostrovy	ostrov	k1gInPc1	ostrov
v	v	k7c6	v
jižním	jižní	k2eAgInSc6d1	jižní
Atlantiku	Atlantik	k1gInSc6	Atlantik
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
základní	základní	k2eAgInSc1d1	základní
kapitál	kapitál	k1gInSc1	kapitál
tvoří	tvořit	k5eAaImIp3nS	tvořit
Ushuaia	Ushuaia	k1gFnSc1	Ushuaia
<g/>
,	,	kIx,	,
největším	veliký	k2eAgInSc7d3	veliký
město	město	k1gNnSc4	město
souostroví	souostroví	k1gNnSc3	souostroví
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
důležitým	důležitý	k2eAgNnSc7d1	důležité
městem	město	k1gNnSc7	město
v	v	k7c6	v
regionu	region	k1gInSc6	region
je	být	k5eAaImIp3nS	být
Rio	Rio	k1gFnSc1	Rio
Grande	grand	k1gMnSc5	grand
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podnebí	podnebí	k1gNnSc2	podnebí
==	==	k?	==
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
má	mít	k5eAaImIp3nS	mít
oceánské	oceánský	k2eAgNnSc1d1	oceánské
podnebí	podnebí	k1gNnSc1	podnebí
<g/>
,	,	kIx,	,
s	s	k7c7	s
krátkými	krátké	k1gNnPc7	krátké
<g/>
,	,	kIx,	,
chladnými	chladný	k2eAgNnPc7d1	chladné
léty	léto	k1gNnPc7	léto
a	a	k8xC	a
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
<g/>
,	,	kIx,	,
vlhké	vlhký	k2eAgInPc1d1	vlhký
<g/>
,	,	kIx,	,
středně	středně	k6eAd1	středně
mírné	mírný	k2eAgFnSc2d1	mírná
zimy	zima	k1gFnSc2	zima
<g/>
:	:	kIx,	:
průměr	průměr	k1gInSc4	průměr
srážek	srážka	k1gFnPc2	srážka
3000	[number]	k4	3000
mm	mm	kA	mm
za	za	k7c4	za
rok	rok	k1gInSc4	rok
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
srážky	srážka	k1gFnPc1	srážka
rychle	rychle	k6eAd1	rychle
klesájí	klesájet	k5eAaPmIp3nP	klesájet
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
straně	strana	k1gFnSc6	strana
souostroví	souostroví	k1gNnSc2	souostroví
<g/>
.	.	kIx.	.
</s>
<s>
Teploty	teplota	k1gFnPc1	teplota
jsou	být	k5eAaImIp3nP	být
stabilní	stabilní	k2eAgFnPc1d1	stabilní
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
:	:	kIx,	:
v	v	k7c6	v
městě	město	k1gNnSc6	město
Ushuaia	Ushuaium	k1gNnSc2	Ushuaium
jen	jen	k9	jen
těžko	těžko	k6eAd1	těžko
překonávají	překonávat	k5eAaImIp3nP	překonávat
9	[number]	k4	9
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
a	a	k8xC	a
průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
je	být	k5eAaImIp3nS	být
0	[number]	k4	0
°	°	k?	°
<g/>
C.	C.	kA	C.
Sněžení	sněžení	k1gNnSc1	sněžení
může	moct	k5eAaImIp3nS	moct
nastat	nastat	k5eAaPmF	nastat
i	i	k9	i
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Chladná	chladný	k2eAgNnPc1d1	chladné
a	a	k8xC	a
vlhká	vlhký	k2eAgNnPc1d1	vlhké
léta	léto	k1gNnPc1	léto
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
zachovávat	zachovávat	k5eAaImF	zachovávat
staré	starý	k2eAgInPc1d1	starý
ledovce	ledovec	k1gInPc1	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Nejjižnější	jižní	k2eAgInPc1d3	nejjižnější
ostrovy	ostrov	k1gInPc1	ostrov
pod	pod	k7c7	pod
Antarktidou	Antarktida	k1gFnSc7	Antarktida
mají	mít	k5eAaImIp3nP	mít
prostředí	prostředí	k1gNnSc4	prostředí
typické	typický	k2eAgNnSc4d1	typické
pro	pro	k7c4	pro
tundry	tundra	k1gFnPc4	tundra
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
neumožňuje	umožňovat	k5eNaImIp3nS	umožňovat
růst	růst	k1gInSc4	růst
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
oblasti	oblast	k1gFnPc1	oblast
mají	mít	k5eAaImIp3nP	mít
polární	polární	k2eAgNnSc4d1	polární
klima	klima	k1gNnSc4	klima
<g/>
.	.	kIx.	.
</s>
<s>
Oblasti	oblast	k1gFnPc1	oblast
na	na	k7c6	na
světě	svět	k1gInSc6	svět
s	s	k7c7	s
podobným	podobný	k2eAgNnSc7d1	podobné
podnebím	podnebí	k1gNnSc7	podnebí
jako	jako	k8xC	jako
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
straně	strana	k1gFnSc6	strana
Ohňové	ohňový	k2eAgFnSc2d1	ohňová
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Aleutské	aleutský	k2eAgInPc1d1	aleutský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
Island	Island	k1gInSc1	Island
<g/>
,	,	kIx,	,
Aljašský	aljašský	k2eAgInSc1d1	aljašský
poloostrov	poloostrov	k1gInSc1	poloostrov
a	a	k8xC	a
Faerské	Faerský	k2eAgInPc1d1	Faerský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původní	původní	k2eAgMnPc1d1	původní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
==	==	k?	==
</s>
</p>
<p>
<s>
Původními	původní	k2eAgMnPc7d1	původní
obyvateli	obyvatel	k1gMnPc7	obyvatel
byli	být	k5eAaImAgMnP	být
indiáni	indián	k1gMnPc1	indián
z	z	k7c2	z
kmenů	kmen	k1gInPc2	kmen
Alakaluf	Alakaluf	k1gInSc1	Alakaluf
<g/>
,	,	kIx,	,
Selknam	Selknam	k1gInSc1	Selknam
(	(	kIx(	(
<g/>
Ona	onen	k3xDgFnSc1	onen
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jaghan	Jaghan	k1gInSc4	Jaghan
<g/>
.	.	kIx.	.
</s>
<s>
Selknamové	Selknamový	k2eAgFnPc1d1	Selknamový
žijící	žijící	k2eAgFnPc1d1	žijící
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
byli	být	k5eAaImAgMnP	být
kočovní	kočovní	k2eAgMnPc1d1	kočovní
lovci	lovec	k1gMnPc1	lovec
divokých	divoký	k2eAgFnPc2d1	divoká
lam	lama	k1gFnPc2	lama
guanako	guanako	k6eAd1	guanako
a	a	k8xC	a
kožešinové	kožešinový	k2eAgFnSc2d1	kožešinová
zvěře	zvěř	k1gFnSc2	zvěř
<g/>
,	,	kIx,	,
Jaghanové	Jaghan	k1gMnPc1	Jaghan
a	a	k8xC	a
Alakalufové	Alakaluf	k1gMnPc1	Alakaluf
sídlili	sídlit	k5eAaImAgMnP	sídlit
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
,	,	kIx,	,
zhotovovali	zhotovovat	k5eAaImAgMnP	zhotovovat
čluny	člun	k1gInPc4	člun
z	z	k7c2	z
kůry	kůra	k1gFnSc2	kůra
a	a	k8xC	a
živili	živit	k5eAaImAgMnP	živit
se	s	k7c7	s
sběrem	sběr	k1gInSc7	sběr
měkkýšů	měkkýš	k1gMnPc2	měkkýš
<g/>
,	,	kIx,	,
rybolovem	rybolov	k1gInSc7	rybolov
a	a	k8xC	a
lovem	lov	k1gInSc7	lov
mořských	mořský	k2eAgMnPc2d1	mořský
savců	savec	k1gMnPc2	savec
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Darwina	Darwin	k1gMnSc2	Darwin
praktikovali	praktikovat	k5eAaImAgMnP	praktikovat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
hladu	hlad	k1gInSc2	hlad
kanibalismus	kanibalismus	k1gInSc4	kanibalismus
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byli	být	k5eAaImAgMnP	být
zdejší	zdejší	k2eAgMnPc1d1	zdejší
indiáni	indián	k1gMnPc1	indián
postiženi	postižen	k2eAgMnPc1d1	postižen
epidemiemi	epidemie	k1gFnPc7	epidemie
a	a	k8xC	a
vystaveni	vystavit	k5eAaPmNgMnP	vystavit
tvrdému	tvrdé	k1gNnSc3	tvrdé
pronásledování	pronásledování	k1gNnSc2	pronásledování
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
zlatokopů	zlatokop	k1gMnPc2	zlatokop
a	a	k8xC	a
farmářů	farmář	k1gMnPc2	farmář
<g/>
,	,	kIx,	,
nejhůř	zle	k6eAd3	zle
byli	být	k5eAaImAgMnP	být
postiženi	postihnout	k5eAaPmNgMnP	postihnout
Selknamové	Selknamový	k2eAgMnPc4d1	Selknamový
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
přímo	přímo	k6eAd1	přímo
vybíjeni	vybíjen	k2eAgMnPc1d1	vybíjen
<g/>
.	.	kIx.	.
</s>
<s>
Jaghanové	Jaghan	k1gMnPc1	Jaghan
a	a	k8xC	a
Alakalufové	Alakaluf	k1gMnPc1	Alakaluf
byli	být	k5eAaImAgMnP	být
postiženi	postihnout	k5eAaPmNgMnP	postihnout
spíše	spíše	k9	spíše
epidemiemi	epidemie	k1gFnPc7	epidemie
neštovic	neštovice	k1gFnPc2	neštovice
<g/>
,	,	kIx,	,
chřipky	chřipka	k1gFnSc2	chřipka
a	a	k8xC	a
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Ohňové	ohňový	k2eAgFnSc6d1	ohňová
zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
přilehlé	přilehlý	k2eAgFnSc3d1	přilehlá
chilské	chilský	k2eAgFnSc3d1	chilská
pevnině	pevnina	k1gFnSc3	pevnina
přežívá	přežívat	k5eAaImIp3nS	přežívat
asi	asi	k9	asi
2600	[number]	k4	2600
Alakalufů	Alakaluf	k1gInPc2	Alakaluf
<g/>
,	,	kIx,	,
1700	[number]	k4	1700
Jaghanů	Jaghan	k1gInPc2	Jaghan
a	a	k8xC	a
necelých	celý	k2eNgInPc2d1	necelý
500	[number]	k4	500
Selknamů	Selknam	k1gInPc2	Selknam
<g/>
,	,	kIx,	,
většinu	většina	k1gFnSc4	většina
míšenců	míšenec	k1gMnPc2	míšenec
<g/>
,	,	kIx,	,
hovořících	hovořící	k2eAgMnPc2d1	hovořící
pouze	pouze	k6eAd1	pouze
španělsky	španělsky	k6eAd1	španělsky
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
část	část	k1gFnSc1	část
Alakalufů	Alakaluf	k1gMnPc2	Alakaluf
ještě	ještě	k6eAd1	ještě
mluví	mluvit	k5eAaImIp3nS	mluvit
svým	svůj	k3xOyFgInSc7	svůj
původním	původní	k2eAgInSc7d1	původní
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Ohňová	ohňový	k2eAgFnSc1d1	ohňová
zem	zem	k1gFnSc1	zem
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
Tierra	Tierra	k1gMnSc1	Tierra
del	del	k?	del
Fuego	Fuego	k1gMnSc1	Fuego
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ohňová	ohňový	k2eAgFnSc1d1	ohňová
země	země	k1gFnSc1	země
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
