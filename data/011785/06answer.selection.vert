<s>
Ohňovou	ohňový	k2eAgFnSc4d1
zemi	zem	k1gFnSc4
objevil	objevit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1520	[number]	k4
Fernã	Fernã	k1gMnSc1
de	de	k?
Magalhã	Magalhã	k1gMnSc1
a	a	k8xC
pojmenoval	pojmenovat	k5eAaPmAgMnS
ji	on	k3xPp3gFnSc4
podle	podle	k7c2
ohňů	oheň	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgMnPc4
uviděl	uvidět	k5eAaPmAgInS
v	v	k7c6
noci	noc	k1gFnSc6
na	na	k7c6
ostrovech	ostrov	k1gInPc6
<g/>
.	.	kIx.
</s>