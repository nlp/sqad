<s>
Var	var	k1gInSc1	var
je	být	k5eAaImIp3nS	být
skupenská	skupenský	k2eAgFnSc1d1	skupenská
přeměna	přeměna	k1gFnSc1	přeměna
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
kapalina	kapalina	k1gFnSc1	kapalina
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
plyn	plyn	k1gInSc4	plyn
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
svém	své	k1gNnSc6	své
objemu	objem	k1gInSc2	objem
(	(	kIx(	(
<g/>
nejenom	nejenom	k6eAd1	nejenom
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
jako	jako	k8xS	jako
při	při	k7c6	při
vypařování	vypařování	k1gNnSc6	vypařování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
varu	var	k1gInSc3	var
dochází	docházet	k5eAaImIp3nS	docházet
při	při	k7c6	při
zahřátí	zahřátí	k1gNnSc6	zahřátí
kapaliny	kapalina	k1gFnSc2	kapalina
na	na	k7c4	na
teplotu	teplota	k1gFnSc4	teplota
varu	var	k1gInSc2	var
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
varu	var	k1gInSc2	var
je	být	k5eAaImIp3nS	být
různá	různý	k2eAgFnSc1d1	různá
pro	pro	k7c4	pro
různé	různý	k2eAgFnPc4d1	různá
kapaliny	kapalina	k1gFnPc4	kapalina
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
velikost	velikost	k1gFnSc1	velikost
závisí	záviset	k5eAaImIp3nS	záviset
také	také	k9	také
na	na	k7c6	na
tlaku	tlak	k1gInSc6	tlak
nad	nad	k7c7	nad
kapalinou	kapalina	k1gFnSc7	kapalina
(	(	kIx(	(
<g/>
s	s	k7c7	s
rostoucím	rostoucí	k2eAgInSc7d1	rostoucí
tlakem	tlak	k1gInSc7	tlak
teplota	teplota	k1gFnSc1	teplota
varu	var	k1gInSc2	var
stoupá	stoupat	k5eAaImIp3nS	stoupat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
má	mít	k5eAaImIp3nS	mít
vliv	vliv	k1gInSc4	vliv
i	i	k9	i
na	na	k7c4	na
průběh	průběh	k1gInSc4	průběh
varu	var	k1gInSc2	var
(	(	kIx(	(
<g/>
vzniku	vznik	k1gInSc2	vznik
bublin	bublina	k1gFnPc2	bublina
páry	pára	k1gFnSc2	pára
<g/>
)	)	kIx)	)
v	v	k7c6	v
nádobách	nádoba	k1gFnPc6	nádoba
s	s	k7c7	s
nezanedbatelnou	zanedbatelný	k2eNgFnSc7d1	nezanedbatelná
výškou	výška	k1gFnSc7	výška
<g/>
.	.	kIx.	.
</s>
<s>
Teplo	teplo	k1gNnSc1	teplo
potřebné	potřebný	k2eAgNnSc1d1	potřebné
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
skupenství	skupenství	k1gNnSc1	skupenství
při	při	k7c6	při
varu	var	k1gInSc6	var
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
skupenské	skupenský	k2eAgNnSc4d1	skupenské
teplo	teplo	k1gNnSc4	teplo
varu	var	k1gInSc2	var
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
potravin	potravina	k1gFnPc2	potravina
var	var	k1gInSc4	var
představuje	představovat	k5eAaImIp3nS	představovat
vkládání	vkládání	k1gNnSc1	vkládání
příslušné	příslušný	k2eAgFnSc2d1	příslušná
potraviny	potravina	k1gFnSc2	potravina
do	do	k7c2	do
vařící	vařící	k2eAgFnSc2d1	vařící
vody	voda	k1gFnSc2	voda
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgFnSc2d1	jiná
kapaliny	kapalina	k1gFnSc2	kapalina
založené	založený	k2eAgFnSc2d1	založená
na	na	k7c6	na
vodě	voda	k1gFnSc6	voda
(	(	kIx(	(
<g/>
např.	např.	kA	např.
mléko	mléko	k1gNnSc1	mléko
<g/>
,	,	kIx,	,
vývar	vývar	k1gInSc1	vývar
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Převaření	převaření	k1gNnSc1	převaření
a	a	k8xC	a
následné	následný	k2eAgNnSc1d1	následné
ochlazení	ochlazení	k1gNnSc1	ochlazení
vody	voda	k1gFnSc2	voda
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
účinný	účinný	k2eAgInSc1d1	účinný
prostředek	prostředek	k1gInSc1	prostředek
dezinfekce	dezinfekce	k1gFnSc2	dezinfekce
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Vařící	vařící	k2eAgFnSc1d1	vařící
voda	voda	k1gFnSc1	voda
během	během	k7c2	během
několika	několik	k4yIc2	několik
minut	minuta	k1gFnPc2	minuta
usmrtí	usmrtit	k5eAaPmIp3nS	usmrtit
většinu	většina	k1gFnSc4	většina
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Vypařování	vypařování	k1gNnSc4	vypařování
Utajený	utajený	k2eAgInSc1d1	utajený
var	var	k1gInSc1	var
Pasterizace	pasterizace	k1gFnSc2	pasterizace
Destilace	destilace	k1gFnSc2	destilace
Skupenství	skupenství	k1gNnSc2	skupenství
Pára	pára	k1gFnSc1	pára
Papinův	Papinův	k2eAgInSc1d1	Papinův
hrnec	hrnec	k1gInSc4	hrnec
Kapalnění	kapalnění	k1gNnPc2	kapalnění
Skupenské	skupenský	k2eAgNnSc1d1	skupenské
teplo	teplo	k1gNnSc1	teplo
varu	var	k1gInSc2	var
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
var	var	k1gInSc1	var
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
var	var	k1gInSc1	var
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
