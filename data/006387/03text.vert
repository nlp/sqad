<s>
Bulat	bulat	k5eAaImF	bulat
Šalvovič	Šalvovič	k1gInSc4	Šalvovič
Okudžava	Okudžava	k1gFnSc1	Okudžava
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
Б	Б	k?	Б
Ш	Ш	k?	Ш
О	О	k?	О
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
,	,	kIx,	,
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
–	–	k?	–
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
prozaik	prozaik	k1gMnSc1	prozaik
a	a	k8xC	a
šansoniér	šansoniér	k1gMnSc1	šansoniér
arménsko-abchazsko-gruzínského	arménskobchazskoruzínský	k2eAgInSc2d1	arménsko-abchazsko-gruzínský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
několika	několik	k4yIc6	několik
filmech	film	k1gInPc6	film
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
scenárista	scenárista	k1gMnSc1	scenárista
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
autor	autor	k1gMnSc1	autor
filmové	filmový	k2eAgFnSc2d1	filmová
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
komunistických	komunistický	k2eAgMnPc2d1	komunistický
funkcionářů	funkcionář	k1gMnPc2	funkcionář
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
přišli	přijít	k5eAaPmAgMnP	přijít
studovat	studovat	k5eAaImF	studovat
na	na	k7c4	na
Komunistickou	komunistický	k2eAgFnSc4d1	komunistická
akademii	akademie	k1gFnSc4	akademie
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Arbat	Arbat	k1gInSc4	Arbat
<g/>
.	.	kIx.	.
</s>
<s>
Dětství	dětství	k1gNnSc4	dětství
strávil	strávit	k5eAaPmAgInS	strávit
střídavě	střídavě	k6eAd1	střídavě
v	v	k7c6	v
Tbilisi	Tbilisi	k1gNnSc6	Tbilisi
a	a	k8xC	a
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
byli	být	k5eAaImAgMnP	být
rodiče	rodič	k1gMnPc1	rodič
zatčeni	zatčen	k2eAgMnPc1d1	zatčen
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
stalinských	stalinský	k2eAgFnPc2d1	stalinská
čistek	čistka	k1gFnPc2	čistka
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1937	[number]	k4	1937
po	po	k7c6	po
vykonstruovaném	vykonstruovaný	k2eAgInSc6d1	vykonstruovaný
procesu	proces	k1gInSc6	proces
zastřelen	zastřelen	k2eAgInSc4d1	zastřelen
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
uvězněna	uvěznit	k5eAaPmNgFnS	uvěznit
v	v	k7c6	v
koncentračním	koncentrační	k2eAgInSc6d1	koncentrační
táboře	tábor	k1gInSc6	tábor
"	"	kIx"	"
<g/>
Karlag	Karlag	k1gMnSc1	Karlag
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
К	К	k?	К
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Karagandská	Karagandský	k2eAgFnSc1d1	Karagandský
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
Kazachstán	Kazachstán	k1gInSc1	Kazachstán
<g/>
.	.	kIx.	.
</s>
<s>
Bulat	bulat	k5eAaImF	bulat
žil	žít	k5eAaImAgMnS	žít
s	s	k7c7	s
babičkou	babička	k1gFnSc7	babička
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
se	se	k3xPyFc4	se
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
18	[number]	k4	18
letech	léto	k1gNnPc6	léto
do	do	k7c2	do
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
jako	jako	k8xS	jako
dobrovolník	dobrovolník	k1gMnSc1	dobrovolník
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc4	druhý
světovou	světový	k2eAgFnSc4d1	světová
válku	válka	k1gFnSc4	válka
strávil	strávit	k5eAaPmAgMnS	strávit
na	na	k7c6	na
frontě	fronta	k1gFnSc6	fronta
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
výrazně	výrazně	k6eAd1	výrazně
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
jeho	jeho	k3xOp3gFnSc4	jeho
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
několikrát	několikrát	k6eAd1	několikrát
raněn	ranit	k5eAaPmNgMnS	ranit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
studoval	studovat	k5eAaImAgMnS	studovat
filologii	filologie	k1gFnSc4	filologie
v	v	k7c6	v
Tbilisi	Tbilisi	k1gNnSc6	Tbilisi
<g/>
,	,	kIx,	,
studia	studio	k1gNnSc2	studio
dokončil	dokončit	k5eAaPmAgInS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1950	[number]	k4	1950
<g/>
–	–	k?	–
<g/>
1954	[number]	k4	1954
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
učitel	učitel	k1gMnSc1	učitel
v	v	k7c6	v
obcích	obec	k1gFnPc6	obec
Šamordinovo	Šamordinův	k2eAgNnSc1d1	Šamordinův
(	(	kIx(	(
<g/>
Ш	Ш	k?	Ш
<g/>
)	)	kIx)	)
a	a	k8xC	a
Vysokiniči	Vysokinič	k1gMnPc1	Vysokinič
(	(	kIx(	(
<g/>
В	В	k?	В
<g/>
)	)	kIx)	)
v	v	k7c6	v
Kalužské	Kalužský	k2eAgFnSc6d1	Kalužská
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1954	[number]	k4	1954
<g/>
–	–	k?	–
<g/>
1956	[number]	k4	1956
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
redaktor	redaktor	k1gMnSc1	redaktor
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
Molodaja	Molodaja	k1gMnSc1	Molodaja
gvardija	gvardija	k1gMnSc1	gvardija
(	(	kIx(	(
<g/>
М	М	k?	М
г	г	k?	г
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
redigoval	redigovat	k5eAaImAgMnS	redigovat
poezii	poezie	k1gFnSc4	poezie
v	v	k7c6	v
časopisu	časopis	k1gInSc6	časopis
Litěraturnaja	Litěraturnaj	k2eAgFnSc1d1	Litěraturnaja
gazeta	gazeta	k1gFnSc1	gazeta
(	(	kIx(	(
<g/>
Л	Л	k?	Л
г	г	k?	г
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
začal	začít	k5eAaPmAgInS	začít
psát	psát	k5eAaImF	psát
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
také	také	k9	také
veřejně	veřejně	k6eAd1	veřejně
zpívat	zpívat	k5eAaImF	zpívat
<g/>
..	..	k?	..
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
po	po	k7c6	po
XX	XX	kA	XX
<g/>
.	.	kIx.	.
sjezdu	sjezd	k1gInSc6	sjezd
KSSS	KSSS	kA	KSSS
a	a	k8xC	a
po	po	k7c6	po
rehabilitaci	rehabilitace	k1gFnSc6	rehabilitace
rodičů	rodič	k1gMnPc2	rodič
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Proslavily	proslavit	k5eAaPmAgFnP	proslavit
ho	on	k3xPp3gMnSc4	on
především	především	k6eAd1	především
jeho	jeho	k3xOp3gFnPc4	jeho
lyrické	lyrický	k2eAgFnPc4d1	lyrická
písně	píseň	k1gFnPc4	píseň
<g/>
,	,	kIx,	,
mimo	mimo	k6eAd1	mimo
nich	on	k3xPp3gMnPc2	on
však	však	k9	však
psal	psát	k5eAaImAgMnS	psát
také	také	k6eAd1	také
historické	historický	k2eAgInPc4d1	historický
romány	román	k1gInPc4	román
<g/>
,	,	kIx,	,
novely	novela	k1gFnPc4	novela
<g/>
,	,	kIx,	,
básnické	básnický	k2eAgFnPc4d1	básnická
sbírky	sbírka	k1gFnPc4	sbírka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
časopisech	časopis	k1gInPc6	časopis
publikoval	publikovat	k5eAaBmAgMnS	publikovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
básnickou	básnický	k2eAgFnSc4d1	básnická
sbírku	sbírka	k1gFnSc4	sbírka
Lyrika	lyrika	k1gFnSc1	lyrika
(	(	kIx(	(
<g/>
Л	Л	k?	Л
<g/>
)	)	kIx)	)
vydal	vydat	k5eAaPmAgInS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
v	v	k7c6	v
Kaluze	Kaluha	k1gFnSc6	Kaluha
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
sbírka	sbírka	k1gFnSc1	sbírka
Ostrovy	ostrov	k1gInPc1	ostrov
mu	on	k3xPp3gMnSc3	on
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
prozaickým	prozaický	k2eAgInSc7d1	prozaický
debutem	debut	k1gInSc7	debut
byla	být	k5eAaImAgFnS	být
povídka	povídka	k1gFnSc1	povídka
Ahoj	ahoj	k0	ahoj
študente	študente	k?	študente
(	(	kIx(	(
<g/>
Б	Б	k?	Б
з	з	k?	з
<g/>
,	,	kIx,	,
ш	ш	k?	ш
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
její	její	k3xOp3gNnSc4	její
vydání	vydání	k1gNnSc4	vydání
se	se	k3xPyFc4	se
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
Konstantin	Konstantin	k1gMnSc1	Konstantin
Paustovskij	Paustovskij	k1gMnSc1	Paustovskij
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgFnPc1d1	významná
autobiografické	autobiografický	k2eAgFnPc1d1	autobiografická
povídky	povídka	k1gFnPc1	povídka
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Nás	my	k3xPp1nPc4	my
vzbudilo	vzbudit	k5eAaPmAgNnS	vzbudit
ráno	ráno	k6eAd1	ráno
již	již	k6eAd1	již
chladné	chladný	k2eAgNnSc1d1	chladné
<g/>
...	...	k?	...
(	(	kIx(	(
<g/>
Litěraturnaja	Litěraturnaj	k2eAgFnSc1d1	Litěraturnaja
gazeta	gazeta	k1gFnSc1	gazeta
17	[number]	k4	17
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
o	o	k7c6	o
odchodu	odchod	k1gInSc6	odchod
mladičkých	mladičký	k2eAgMnPc2d1	mladičký
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
do	do	k7c2	do
války	válka	k1gFnSc2	válka
<g/>
;	;	kIx,	;
Soukromý	soukromý	k2eAgInSc1d1	soukromý
život	život	k1gInSc1	život
Alexandra	Alexandr	k1gMnSc2	Alexandr
Sergejeviče	Sergejevič	k1gMnSc2	Sergejevič
Puškina	Puškin	k1gMnSc2	Puškin
aneb	aneb	k?	aneb
použití	použití	k1gNnSc1	použití
nominativu	nominativ	k1gInSc2	nominativ
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Michaila	Michail	k1gMnSc2	Michail
Jurieviče	Jurievič	k1gMnSc2	Jurievič
Lermontova	Lermontův	k2eAgMnSc2d1	Lermontův
(	(	kIx(	(
<g/>
Litěraturnaja	Litěraturnaja	k1gFnSc1	Litěraturnaja
gazeta	gazeta	k1gFnSc1	gazeta
47	[number]	k4	47
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
sám	sám	k3xTgMnSc1	sám
autor	autor	k1gMnSc1	autor
hodnotí	hodnotit	k5eAaImIp3nS	hodnotit
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
smutná	smutný	k2eAgFnSc1d1	smutná
povídka	povídka	k1gFnSc1	povídka
o	o	k7c6	o
mých	můj	k3xOp1gInPc6	můj
smutných	smutný	k2eAgInPc6d1	smutný
učitelských	učitelský	k2eAgInPc6d1	učitelský
koncích	konec	k1gInPc6	konec
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
Dílčí	dílčí	k2eAgInPc4d1	dílčí
neúspěchy	neúspěch	k1gInPc4	neúspěch
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
řadě	řada	k1gFnSc6	řada
úspěchů	úspěch	k1gInPc2	úspěch
(	(	kIx(	(
<g/>
Družba	družba	k1gFnSc1	družba
narodov	narodov	k1gInSc1	narodov
1	[number]	k4	1
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
groteska	groteska	k1gFnSc1	groteska
ze	z	k7c2	z
života	život	k1gInSc2	život
mladého	mladý	k2eAgMnSc2d1	mladý
redaktora	redaktor	k1gMnSc2	redaktor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
do	do	k7c2	do
Svazu	svaz	k1gInSc2	svaz
spisovatelů	spisovatel	k1gMnPc2	spisovatel
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
dostalo	dostat	k5eAaPmAgNnS	dostat
prvního	první	k4xOgNnSc2	první
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
ocenění	ocenění	k1gNnSc2	ocenění
<g/>
:	:	kIx,	:
obdržel	obdržet	k5eAaPmAgMnS	obdržet
hlavní	hlavní	k2eAgFnSc4d1	hlavní
cenu	cena	k1gFnSc4	cena
na	na	k7c6	na
básnickém	básnický	k2eAgInSc6d1	básnický
festivalu	festival	k1gInSc6	festival
Stružské	Stružský	k2eAgInPc4d1	Stružský
večery	večer	k1gInPc4	večer
v	v	k7c6	v
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
za	za	k7c4	za
písničku	písnička	k1gFnSc4	písnička
Cínovej	Cínovej	k?	Cínovej
vojášek	vojášek	k1gMnSc1	vojášek
mýho	mýho	k?	mýho
syna	syn	k1gMnSc4	syn
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
popularitu	popularita	k1gFnSc4	popularita
získal	získat	k5eAaPmAgInS	získat
především	především	k6eAd1	především
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
vydal	vydat	k5eAaPmAgInS	vydat
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
historický	historický	k2eAgInSc4d1	historický
román	román	k1gInSc4	román
Nebohý	nebohý	k2eAgInSc4d1	nebohý
Avrosimov	Avrosimov	k1gInSc4	Avrosimov
<g/>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
časopisecky	časopisecky	k6eAd1	časopisecky
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Б	Б	k?	Б
А	А	k?	А
(	(	kIx(	(
<g/>
Družba	družba	k1gFnSc1	družba
narodov	narodov	k1gInSc1	narodov
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc1	první
knižní	knižní	k2eAgNnSc1d1	knižní
vydání	vydání	k1gNnSc1	vydání
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Doušek	doušek	k1gInSc1	doušek
svobody	svoboda	k1gFnSc2	svoboda
<g/>
,	,	kIx,	,
Г	Г	k?	Г
с	с	k?	с
<g/>
,	,	kIx,	,
Politizdat	Politizdat	k1gMnSc1	Politizdat
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Okudžava	Okudžava	k1gFnSc1	Okudžava
nebyl	být	k5eNaImAgInS	být
příliš	příliš	k6eAd1	příliš
dobrý	dobrý	k2eAgMnSc1d1	dobrý
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnPc1	jeho
písně	píseň	k1gFnPc1	píseň
s	s	k7c7	s
vlastním	vlastní	k2eAgInSc7d1	vlastní
doprovodem	doprovod	k1gInSc7	doprovod
na	na	k7c4	na
sedmistrunnou	sedmistrunný	k2eAgFnSc4d1	sedmistrunná
kytaru	kytara	k1gFnSc4	kytara
staly	stát	k5eAaPmAgFnP	stát
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgFnPc1d1	populární
<g/>
.	.	kIx.	.
</s>
<s>
Patřil	patřit	k5eAaImAgInS	patřit
ke	k	k7c3	k
skupině	skupina	k1gFnSc3	skupina
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
stavěla	stavět	k5eAaImAgFnS	stavět
kriticky	kriticky	k6eAd1	kriticky
k	k	k7c3	k
socialistickému	socialistický	k2eAgInSc3d1	socialistický
režimu	režim	k1gInSc3	režim
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
neusilovali	usilovat	k5eNaImAgMnP	usilovat
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
odstranění	odstranění	k1gNnSc6	odstranění
<g/>
,	,	kIx,	,
usilovali	usilovat	k5eAaImAgMnP	usilovat
o	o	k7c4	o
zlepšení	zlepšení	k1gNnSc4	zlepšení
podmínek	podmínka	k1gFnPc2	podmínka
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
socialismu	socialismus	k1gInSc2	socialismus
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
během	během	k7c2	během
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
USA	USA	kA	USA
se	se	k3xPyFc4	se
u	u	k7c2	u
něj	on	k3xPp3gNnSc2	on
projevila	projevit	k5eAaPmAgFnS	projevit
srdeční	srdeční	k2eAgFnSc1d1	srdeční
choroba	choroba	k1gFnSc1	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Musel	muset	k5eAaImAgMnS	muset
absolvovat	absolvovat	k5eAaPmF	absolvovat
operaci	operace	k1gFnSc4	operace
srdce	srdce	k1gNnSc2	srdce
a	a	k8xC	a
po	po	k7c6	po
uzdravení	uzdravení	k1gNnSc6	uzdravení
již	již	k6eAd1	již
veřejně	veřejně	k6eAd1	veřejně
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
<g/>
.	.	kIx.	.
</s>
<s>
Ahoj	ahoj	k0	ahoj
<g/>
,	,	kIx,	,
študente	študente	k?	študente
–	–	k?	–
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
<g/>
:	:	kIx,	:
Anna	Anna	k1gFnSc1	Anna
Nováková	Nováková	k1gFnSc1	Nováková
Nebohý	nebohý	k2eAgInSc4d1	nebohý
Avrosimov	Avrosimov	k1gInSc4	Avrosimov
–	–	k?	–
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Ludmila	Ludmila	k1gFnSc1	Ludmila
Dušková	Dušková	k1gFnSc1	Dušková
Šipovova	Šipovův	k2eAgNnSc2d1	Šipovův
dobrodružství	dobrodružství	k1gNnSc2	dobrodružství
aneb	aneb	k?	aneb
Starodávná	starodávný	k2eAgFnSc1d1	starodávná
fraška	fraška	k1gFnSc1	fraška
–	–	k?	–
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Ludmila	Ludmila	k1gFnSc1	Ludmila
Dušková	Dušková	k1gFnSc1	Dušková
Putování	putování	k1gNnSc2	putování
diletantů	diletant	k1gMnPc2	diletant
:	:	kIx,	:
ze	z	k7c2	z
<g />
.	.	kIx.	.
</s>
<s>
zápisků	zápisek	k1gInPc2	zápisek
poručíka	poručík	k1gMnSc4	poručík
ve	v	k7c6	v
výslužbě	výslužba	k1gFnSc6	výslužba
Amirana	Amiran	k1gMnSc2	Amiran
Amilachvariho	Amilachvari	k1gMnSc2	Amilachvari
–	–	k?	–
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Ludmila	Ludmila	k1gFnSc1	Ludmila
Dušková	Dušková	k1gFnSc1	Dušková
Dostaveníčko	dostaveníčko	k1gNnSc1	dostaveníčko
s	s	k7c7	s
Bonapartem	bonapart	k1gInSc7	bonapart
–	–	k?	–
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Ludmila	Ludmila	k1gFnSc1	Ludmila
Dušková	Dušková	k1gFnSc1	Dušková
Zrušené	zrušený	k2eAgNnSc4d1	zrušené
divadlo	divadlo	k1gNnSc4	divadlo
–	–	k?	–
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Libor	Libor	k1gMnSc1	Libor
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7217-253-0	[number]	k4	80-7217-253-0
Lyrika	lyrika	k1gFnSc1	lyrika
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
1956	[number]	k4	1956
Ostrovy	ostrov	k1gInPc7	ostrov
–	–	k?	–
1959	[number]	k4	1959
Cesta	cesta	k1gFnSc1	cesta
na	na	k7c4	na
Tinatin	Tinatina	k1gFnPc2	Tinatina
–	–	k?	–
1964	[number]	k4	1964
Veselý	Veselý	k1gMnSc1	Veselý
bubeníček	bubeníček	k1gMnSc1	bubeníček
(	(	kIx(	(
<g/>
В	В	k?	В
б	б	k?	б
<g/>
)	)	kIx)	)
–	–	k?	–
1964	[number]	k4	1964
–	–	k?	–
řada	řada	k1gFnSc1	řada
textů	text	k1gInPc2	text
byla	být	k5eAaImAgFnS	být
zhudebněna	zhudebněn	k2eAgFnSc1d1	zhudebněna
Velkomyslný	velkomyslný	k2eAgInSc4d1	velkomyslný
březen	březen	k1gInSc4	březen
(	(	kIx(	(
<g/>
М	М	k?	М
в	в	k?	в
<g/>
)	)	kIx)	)
–	–	k?	–
1967	[number]	k4	1967
Arbate	Arbat	k1gMnSc5	Arbat
<g/>
,	,	kIx,	,
můj	můj	k1gMnSc5	můj
Arbate	Arbat	k1gMnSc5	Arbat
(	(	kIx(	(
<g/>
А	А	k?	А
<g/>
,	,	kIx,	,
м	м	k?	м
А	А	k?	А
<g/>
)	)	kIx)	)
–	–	k?	–
1976	[number]	k4	1976
–	–	k?	–
řada	řada	k1gFnSc1	řada
textů	text	k1gInPc2	text
byla	být	k5eAaImAgFnS	být
zhudebněna	zhudebněn	k2eAgFnSc1d1	zhudebněna
Blíží	blížit	k5eAaImIp3nS	blížit
se	se	k3xPyFc4	se
k	k	k7c3	k
nám	my	k3xPp1nPc3	my
fronta	fronta	k1gFnSc1	fronta
–	–	k?	–
1965	[number]	k4	1965
OKUDŽAVA	OKUDŽAVA	kA	OKUDŽAVA
<g/>
,	,	kIx,	,
Bulat	bulat	k5eAaImF	bulat
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
naděje	naděje	k1gFnSc1	naděje
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Ludmila	Ludmila	k1gFnSc1	Ludmila
Dušková	Dušková	k1gFnSc1	Dušková
<g/>
;	;	kIx,	;
překlad	překlad	k1gInSc1	překlad
Václav	Václav	k1gMnSc1	Václav
Daněk	Daněk	k1gMnSc1	Daněk
(	(	kIx(	(
<g/>
básně	báseň	k1gFnPc1	báseň
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ludmila	Ludmila	k1gFnSc1	Ludmila
Dušková	Dušková	k1gFnSc1	Dušková
(	(	kIx(	(
<g/>
povídky	povídka	k1gFnPc1	povídka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
OKUDŽAVA	OKUDŽAVA	kA	OKUDŽAVA
<g/>
,	,	kIx,	,
Bulat	bulat	k5eAaImF	bulat
<g/>
.	.	kIx.	.
</s>
<s>
Půlnoční	půlnoční	k2eAgInSc1d1	půlnoční
trolejbus	trolejbus	k1gInSc1	trolejbus
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Petr	Petr	k1gMnSc1	Petr
Kovařík	Kovařík	k1gMnSc1	Kovařík
<g/>
;	;	kIx,	;
překlad	překlad	k1gInSc1	překlad
Petr	Petr	k1gMnSc1	Petr
Kovařík	Kovařík	k1gMnSc1	Kovařík
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Československý	československý	k2eAgMnSc1d1	československý
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Klub	klub	k1gInSc1	klub
přátel	přítel	k1gMnPc2	přítel
poezie	poezie	k1gFnSc2	poezie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Koně	kůň	k1gMnSc4	kůň
k	k	k7c3	k
nezkrocení	nezkrocení	k1gNnSc3	nezkrocení
-	-	kIx~	-
Bulat	bulat	k5eAaImF	bulat
Okudžava	Okudžava	k1gFnSc1	Okudžava
a	a	k8xC	a
Vladimír	Vladimír	k1gMnSc1	Vladimír
Vysockij	Vysockij	k1gMnSc1	Vysockij
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Václav	Václav	k1gMnSc1	Václav
Daněk	Daněk	k1gMnSc1	Daněk
<g/>
;	;	kIx,	;
překlad	překlad	k1gInSc1	překlad
Václav	Václav	k1gMnSc1	Václav
Daněk	Daněk	k1gMnSc1	Daněk
(	(	kIx(	(
<g/>
Bulat	bulat	k5eAaImF	bulat
Okudžava	Okudžava	k1gFnSc1	Okudžava
<g/>
)	)	kIx)	)
a	a	k8xC	a
Milan	Milan	k1gMnSc1	Milan
Dvořák	Dvořák	k1gMnSc1	Dvořák
(	(	kIx(	(
<g/>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Vysockij	Vysockij	k1gMnSc1	Vysockij
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Svaz	svaz	k1gInSc1	svaz
Hudebníků	hudebník	k1gMnPc2	hudebník
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Edice	edice	k1gFnSc1	edice
Kruhu	kruh	k1gInSc2	kruh
přátel	přítel	k1gMnPc2	přítel
mladé	mladý	k2eAgFnSc2d1	mladá
hudby	hudba	k1gFnSc2	hudba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1965	[number]	k4	1965
Věrnosť	Věrnosť	k1gFnPc2	Věrnosť
(	(	kIx(	(
<g/>
В	В	k?	В
<g/>
)	)	kIx)	)
1967	[number]	k4	1967
Žeňa	Žeňum	k1gNnSc2	Žeňum
<g/>
,	,	kIx,	,
Ženěčka	Ženěčko	k1gNnSc2	Ženěčko
a	a	k8xC	a
"	"	kIx"	"
<g/>
kaťuše	kaťuše	k1gFnSc1	kaťuše
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Ж	Ж	k?	Ж
<g/>
,	,	kIx,	,
Ж	Ж	k?	Ж
и	и	k?	и
"	"	kIx"	"
<g/>
к	к	k?	к
<g/>
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
1964	[number]	k4	1964
Je	být	k5eAaImIp3nS	být
mi	já	k3xPp1nSc3	já
20	[number]	k4	20
let	léto	k1gNnPc2	léto
1967	[number]	k4	1967
Žeňa	Žeňum	k1gNnSc2	Žeňum
<g/>
,	,	kIx,	,
Ženěčka	Ženěčko	k1gNnSc2	Ženěčko
a	a	k8xC	a
"	"	kIx"	"
<g/>
kaťuše	kaťuše	k1gFnSc1	kaťuše
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Vladimír	Vladimír	k1gMnSc1	Vladimír
Motyl	Motyl	k1gInSc1	Motyl
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
sám	sám	k3xTgMnSc1	sám
hraje	hrát	k5eAaImIp3nS	hrát
písničku	písnička	k1gFnSc4	písnička
Píseň	píseň	k1gFnSc1	píseň
o	o	k7c6	o
kapkách	kapka	k1gFnPc6	kapka
dánskýho	dánskýho	k?	dánskýho
krále	král	k1gMnSc2	král
(	(	kIx(	(
<g/>
К	К	k?	К
Д	Д	k?	Д
к	к	k?	к
<g/>
)	)	kIx)	)
1975	[number]	k4	1975
Hvězda	Hvězda	k1gMnSc1	Hvězda
kouzelného	kouzelný	k2eAgNnSc2d1	kouzelné
štěstí	štěstí	k1gNnSc2	štěstí
1970	[number]	k4	1970
Běloruské	běloruský	k2eAgNnSc1d1	běloruské
nádraží	nádraží	k1gNnSc1	nádraží
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Vladimír	Vladimír	k1gMnSc1	Vladimír
Motyl	Motyl	k1gInSc1	Motyl
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
film	film	k1gInSc4	film
napsal	napsat	k5eAaBmAgMnS	napsat
písničku	písnička	k1gFnSc4	písnička
Ať	ať	k9	ať
to	ten	k3xDgNnSc1	ten
stojí	stát	k5eAaImIp3nS	stát
cokoliv	cokoliv	k3yInSc1	cokoliv
(	(	kIx(	(
<g/>
М	М	k?	М
з	з	k?	з
ц	ц	k?	ц
н	н	k?	н
п	п	k?	п
<g/>
,	,	kIx,	,
český	český	k2eAgInSc1d1	český
překlad	překlad	k1gInSc1	překlad
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Desátý	desátý	k4xOgInSc4	desátý
prapor	prapor	k1gInSc4	prapor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
tak	tak	k6eAd1	tak
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
široké	široký	k2eAgFnPc4d1	široká
popularity	popularita	k1gFnPc4	popularita
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
rusku	rusk	k1gInSc6	rusk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Československo	Československo	k1gNnSc4	Československo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
zde	zde	k6eAd1	zde
veřejně	veřejně	k6eAd1	veřejně
nevystoupil	vystoupit	k5eNaPmAgMnS	vystoupit
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
Československý	československý	k2eAgInSc4d1	československý
rozhlas	rozhlas	k1gInSc4	rozhlas
ale	ale	k8xC	ale
nahrál	nahrát	k5eAaPmAgMnS	nahrát
písničku	písnička	k1gFnSc4	písnička
Balada	balada	k1gFnSc1	balada
o	o	k7c4	o
starým	starý	k2eAgMnPc3d1	starý
králi	král	k1gMnSc6	král
<g/>
.	.	kIx.	.
</s>
<s>
Okudžavovi	Okudžavův	k2eAgMnPc1d1	Okudžavův
čeští	český	k2eAgMnPc1d1	český
překladatelé	překladatel	k1gMnPc1	překladatel
jsou	být	k5eAaImIp3nP	být
Václav	Václav	k1gMnSc1	Václav
Daněk	Daněk	k1gMnSc1	Daněk
<g/>
,	,	kIx,	,
Ludmila	Ludmila	k1gFnSc1	Ludmila
Dušková	Dušková	k1gFnSc1	Dušková
<g/>
,	,	kIx,	,
Libor	Libor	k1gMnSc1	Libor
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Honzík	Honzík	k1gMnSc1	Honzík
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Kovařík	Kovařík	k1gMnSc1	Kovařík
<g/>
,	,	kIx,	,
Jaromír	Jaromír	k1gMnSc1	Jaromír
Nohavica	Nohavica	k1gMnSc1	Nohavica
<g/>
,	,	kIx,	,
Anna	Anna	k1gFnSc1	Anna
Nováková	Nováková	k1gFnSc1	Nováková
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Vondrák	Vondrák	k1gMnSc1	Vondrák
<g/>
.	.	kIx.	.
</s>
<s>
Okudžavovy	Okudžavův	k2eAgFnPc4d1	Okudžavova
písničky	písnička	k1gFnPc4	písnička
zpívali	zpívat	k5eAaImAgMnP	zpívat
a	a	k8xC	a
zpívají	zpívat	k5eAaImIp3nP	zpívat
<g/>
:	:	kIx,	:
Jaromír	Jaromír	k1gMnSc1	Jaromír
Nohavica	Nohavica	k1gMnSc1	Nohavica
Alfred	Alfred	k1gMnSc1	Alfred
Strejček	Strejček	k1gMnSc1	Strejček
(	(	kIx(	(
<g/>
československý	československý	k2eAgInSc1d1	československý
rozhlas	rozhlas	k1gInSc1	rozhlas
1978	[number]	k4	1978
<g/>
;	;	kIx,	;
Divadlo	divadlo	k1gNnSc1	divadlo
Viola	Viola	k1gFnSc1	Viola
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jiřím	Jiří	k1gMnSc7	Jiří
Šlupkou	Šlupkou	k?	Šlupkou
Svěrákem	Svěrák	k1gMnSc7	Svěrák
<g/>
)	)	kIx)	)
Milan	Milan	k1gMnSc1	Milan
Dvořák	Dvořák	k1gMnSc1	Dvořák
a	a	k8xC	a
Věra	Věra	k1gFnSc1	Věra
Slunéčková	Slunéčková	k1gFnSc1	Slunéčková
Jiří	Jiří	k1gMnSc1	Jiří
Vondrák	Vondrák	k1gMnSc1	Vondrák
Hana	Hana	k1gFnSc1	Hana
Hegerová	Hegerová	k1gFnSc1	Hegerová
–	–	k?	–
píseň	píseň	k1gFnSc1	píseň
Váňa	Váňa	k1gMnSc1	Váňa
(	(	kIx(	(
<g/>
В	В	k?	В
М	М	k?	М
<g/>
,	,	kIx,	,
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Ondřej	Ondřej	k1gMnSc1	Ondřej
Suchý	Suchý	k1gMnSc1	Suchý
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
napsal	napsat	k5eAaPmAgMnS	napsat
Vladimír	Vladimír	k1gMnSc1	Vladimír
Merta	Merta	k1gMnSc1	Merta
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Na	na	k7c4	na
dluh	dluh	k1gInSc4	dluh
Bulatu	Bulat	k1gMnSc3	Bulat
Okudžavovi	Okudžava	k1gMnSc3	Okudžava
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
nahrána	nahrát	k5eAaPmNgFnS	nahrát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
na	na	k7c6	na
LP	LP	kA	LP
desce	deska	k1gFnSc6	deska
Vladimír	Vladimír	k1gMnSc1	Vladimír
Merta	Merta	k1gMnSc1	Merta
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
hrál	hrát	k5eAaImAgMnS	hrát
Okudžava	Okudžava	k1gFnSc1	Okudžava
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
:	:	kIx,	:
27	[number]	k4	27
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1995	[number]	k4	1995
koncertoval	koncertovat	k5eAaImAgMnS	koncertovat
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
Husa	husa	k1gFnSc1	husa
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1995	[number]	k4	1995
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
Divadle	divadlo	k1gNnSc6	divadlo
Komedie	komedie	k1gFnSc2	komedie
<g/>
.	.	kIx.	.
</s>
<s>
Brněnský	brněnský	k2eAgInSc1d1	brněnský
koncert	koncert	k1gInSc1	koncert
uváděl	uvádět	k5eAaImAgMnS	uvádět
Jiří	Jiří	k1gMnSc1	Jiří
Vondrák	Vondrák	k1gMnSc1	Vondrák
<g/>
,	,	kIx,	,
pražský	pražský	k2eAgMnSc1d1	pražský
pak	pak	k6eAd1	pak
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
koncerty	koncert	k1gInPc1	koncert
byly	být	k5eAaImAgFnP	být
posledním	poslední	k2eAgNnSc7d1	poslední
vystoupením	vystoupení	k1gNnSc7	vystoupení
Bulata	Bulat	k1gMnSc2	Bulat
Okudžavy	Okudžava	k1gFnSc2	Okudžava
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
<g/>
.	.	kIx.	.
</s>
