<s>
Divadélko	divadélko	k1gNnSc1	divadélko
ve	v	k7c6	v
Smetanově	Smetanův	k2eAgNnSc6d1	Smetanovo
muzeu	muzeum	k1gNnSc6	muzeum
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
divadlo	divadlo	k1gNnSc1	divadlo
vzniklé	vzniklý	k2eAgNnSc1d1	vzniklé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
z	z	k7c2	z
části	část	k1gFnSc2	část
souboru	soubor	k1gInSc2	soubor
Divadélka	divadélko	k1gNnSc2	divadélko
pro	pro	k7c4	pro
99	[number]	k4	99
<g/>
.	.	kIx.	.
</s>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
existovalo	existovat	k5eAaImAgNnS	existovat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Soubor	soubor	k1gInSc1	soubor
divadla	divadlo	k1gNnSc2	divadlo
byl	být	k5eAaImAgInS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
především	především	k6eAd1	především
konzervatoristy	konzervatorista	k1gMnPc4	konzervatorista
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
hráli	hrát	k5eAaImAgMnP	hrát
v	v	k7c6	v
letech	let	k1gInPc6	let
1940	[number]	k4	1940
<g/>
–	–	k?	–
<g/>
1941	[number]	k4	1941
v	v	k7c6	v
Divadélku	divadélko	k1gNnSc6	divadélko
pro	pro	k7c4	pro
99	[number]	k4	99
v	v	k7c6	v
souboru	soubor	k1gInSc6	soubor
vedeném	vedený	k2eAgInSc6d1	vedený
Jindřichem	Jindřich	k1gMnSc7	Jindřich
Honzlem	Honzl	k1gMnSc7	Honzl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
1941	[number]	k4	1941
bylo	být	k5eAaImAgNnS	být
působení	působení	k1gNnSc1	působení
divadélka	divadélko	k1gNnSc2	divadélko
v	v	k7c6	v
salónu	salón	k1gInSc6	salón
U	u	k7c2	u
Topičů	topič	k1gInPc2	topič
nuceně	nuceně	k6eAd1	nuceně
ukončeno	ukončit	k5eAaPmNgNnS	ukončit
výpovědí	výpověď	k1gFnSc7	výpověď
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
F.	F.	kA	F.
Borový	borový	k2eAgMnSc1d1	borový
a	a	k8xC	a
část	část	k1gFnSc1	část
souboru	soubor	k1gInSc2	soubor
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
Smetanova	Smetanův	k2eAgNnSc2d1	Smetanovo
muzea	muzeum	k1gNnSc2	muzeum
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
od	od	k7c2	od
podzimu	podzim	k1gInSc2	podzim
1941	[number]	k4	1941
soubor	soubor	k1gInSc1	soubor
vystupoval	vystupovat	k5eAaImAgInS	vystupovat
v	v	k7c6	v
koncertní	koncertní	k2eAgFnSc6d1	koncertní
síni	síň	k1gFnSc6	síň
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
okolo	okolo	k7c2	okolo
150	[number]	k4	150
míst	místo	k1gNnPc2	místo
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Divadélko	divadélko	k1gNnSc1	divadélko
ve	v	k7c6	v
Smetanově	Smetanův	k2eAgNnSc6d1	Smetanovo
muzeu	muzeum	k1gNnSc6	muzeum
,	,	kIx,	,
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
představení	představení	k1gNnSc1	představení
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1941	[number]	k4	1941
<g/>
.	.	kIx.	.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
Honzl	Honzl	k1gMnSc1	Honzl
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
ještě	ještě	k6eAd1	ještě
chvíli	chvíle	k1gFnSc4	chvíle
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
a	a	k8xC	a
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
kvůli	kvůli	k7c3	kvůli
útokům	útok	k1gInPc3	útok
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
fašistického	fašistický	k2eAgInSc2d1	fašistický
tisku	tisk	k1gInSc2	tisk
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
Vlajky	vlajka	k1gFnSc2	vlajka
a	a	k8xC	a
Arijského	arijský	k2eAgInSc2d1	arijský
boje	boj	k1gInSc2	boj
<g/>
,	,	kIx,	,
odejít	odejít	k5eAaPmF	odejít
do	do	k7c2	do
ústraní	ústraní	k1gNnSc2	ústraní
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
Honzlova	Honzlův	k2eAgNnSc2d1	Honzlův
přání	přání	k1gNnSc2	přání
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
vedení	vedení	k1gNnSc2	vedení
souboru	soubor	k1gInSc2	soubor
nejprve	nejprve	k6eAd1	nejprve
Felix	Felix	k1gMnSc1	Felix
le	le	k?	le
Breux	Breux	k1gInSc1	Breux
a	a	k8xC	a
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
odchodu	odchod	k1gInSc6	odchod
do	do	k7c2	do
Nezávislého	závislý	k2eNgNnSc2d1	nezávislé
divadla	divadlo	k1gNnSc2	divadlo
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1942	[number]	k4	1942
vedl	vést	k5eAaImAgMnS	vést
divadlo	divadlo	k1gNnSc4	divadlo
Antonín	Antonín	k1gMnSc1	Antonín
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
do	do	k7c2	do
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souboru	soubor	k1gInSc6	soubor
Divadélka	divadélko	k1gNnSc2	divadélko
ve	v	k7c6	v
Smetanově	Smetanův	k2eAgNnSc6d1	Smetanovo
muzeu	muzeum	k1gNnSc6	muzeum
vystupovali	vystupovat	k5eAaImAgMnP	vystupovat
např.	např.	kA	např.
Antonie	Antonie	k1gFnSc1	Antonie
Hegerlíková	Hegerlíková	k1gFnSc1	Hegerlíková
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Houska	houska	k1gFnSc1	houska
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Stránský	Stránský	k1gMnSc1	Stránský
(	(	kIx(	(
<g/>
od	od	k7c2	od
podzimu	podzim	k1gInSc2	podzim
1941	[number]	k4	1941
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
)	)	kIx)	)
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
Motlová	Motlová	k1gFnSc1	Motlová
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
Zvoníček	Zvoníček	k1gMnSc1	Zvoníček
<g/>
,	,	kIx,	,
Jaromír	Jaromír	k1gMnSc1	Jaromír
Spal	spát	k5eAaImAgMnS	spát
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Pehr	Pehr	k1gMnSc1	Pehr
<g/>
,	,	kIx,	,
Felix	Felix	k1gMnSc1	Felix
le	le	k?	le
Breux	Breux	k1gInSc1	Breux
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Seydler	Seydler	k1gMnSc1	Seydler
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
Vyskočil	Vyskočil	k1gMnSc1	Vyskočil
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
.	.	kIx.	.
</s>
<s>
Režisérem	režisér	k1gMnSc7	režisér
byl	být	k5eAaImAgMnS	být
Antonín	Antonín	k1gMnSc1	Antonín
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
současně	současně	k6eAd1	současně
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
Intimním	intimní	k2eAgNnSc6d1	intimní
divadle	divadlo	k1gNnSc6	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
častým	častý	k2eAgInPc3d1	častý
návštěvníků	návštěvník	k1gMnPc2	návštěvník
divadla	divadlo	k1gNnSc2	divadlo
pařili	pařit	k5eAaImAgMnP	pařit
mj.	mj.	kA	mj.
Jindřich	Jindřich	k1gMnSc1	Jindřich
Plachta	plachta	k1gFnSc1	plachta
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Drda	Drda	k1gMnSc1	Drda
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Řezáč	Řezáč	k1gMnSc1	Řezáč
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Konrád	Konrád	k1gMnSc1	Konrád
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Teige	Teig	k1gFnSc2	Teig
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Frejka	Frejka	k1gFnSc1	Frejka
<g/>
.	.	kIx.	.
</s>
<s>
Divadélko	divadélko	k1gNnSc1	divadélko
působilo	působit	k5eAaImAgNnS	působit
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
ve	v	k7c6	v
Smetanově	Smetanův	k2eAgNnSc6d1	Smetanovo
muzeu	muzeum	k1gNnSc6	muzeum
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
krátce	krátce	k6eAd1	krátce
také	také	k9	také
ve	v	k7c6	v
Stálém	stálý	k2eAgNnSc6d1	stálé
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Unitarii	Unitarie	k1gFnSc6	Unitarie
v	v	k7c6	v
Karlově	Karlův	k2eAgFnSc6d1	Karlova
ulice	ulice	k1gFnPc4	ulice
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
zde	zde	k6eAd1	zde
sídlil	sídlit	k5eAaImAgInS	sídlit
DISK	disk	k1gInSc1	disk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
hrál	hrát	k5eAaImAgInS	hrát
soubor	soubor	k1gInSc1	soubor
v	v	k7c6	v
Nezávislém	závislý	k2eNgNnSc6d1	nezávislé
divadle	divadlo	k1gNnSc6	divadlo
na	na	k7c6	na
Václavském	václavský	k2eAgNnSc6d1	Václavské
náměstí	náměstí	k1gNnSc6	náměstí
a	a	k8xC	a
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1943	[number]	k4	1943
<g/>
/	/	kIx~	/
<g/>
1944	[number]	k4	1944
v	v	k7c6	v
Intimním	intimní	k2eAgNnSc6d1	intimní
divadle	divadlo	k1gNnSc6	divadlo
pod	pod	k7c7	pod
hlavičkou	hlavička	k1gFnSc7	hlavička
"	"	kIx"	"
<g/>
Studio	studio	k1gNnSc1	studio
Intimního	intimní	k2eAgNnSc2d1	intimní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
názorových	názorový	k2eAgFnPc6d1	názorová
neshodách	neshoda	k1gFnPc6	neshoda
s	s	k7c7	s
ředitelem	ředitel	k1gMnSc7	ředitel
Intimního	intimní	k2eAgNnSc2d1	intimní
divadla	divadlo	k1gNnSc2	divadlo
Jaromírem	Jaromír	k1gMnSc7	Jaromír
Dohnalem	Dohnal	k1gMnSc7	Dohnal
se	se	k3xPyFc4	se
soubor	soubor	k1gInSc1	soubor
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
znovu	znovu	k6eAd1	znovu
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Smetanova	Smetanův	k2eAgNnSc2d1	Smetanovo
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgNnSc7d1	poslední
představením	představení	k1gNnSc7	představení
Divadélka	divadélko	k1gNnSc2	divadélko
ve	v	k7c4	v
Smetanově	smetanově	k6eAd1	smetanově
divadlo	divadlo	k1gNnSc4	divadlo
v	v	k7c6	v
době	doba	k1gFnSc6	doba
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
ilegální	ilegální	k2eAgNnSc1d1	ilegální
nastudování	nastudování	k1gNnSc1	nastudování
hry	hra	k1gFnPc1	hra
Král	Král	k1gMnSc1	Král
Ubu	Ubu	k1gMnSc1	Ubu
<g/>
,	,	kIx,	,
uvedené	uvedený	k2eAgFnPc4d1	uvedená
pro	pro	k7c4	pro
pozvané	pozvaný	k2eAgMnPc4d1	pozvaný
hosty	host	k1gMnPc4	host
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1944	[number]	k4	1944
ve	v	k7c6	v
Smetanově	Smetanův	k2eAgNnSc6d1	Smetanovo
muzeu	muzeum	k1gNnSc6	muzeum
a	a	k8xC	a
věnované	věnovaný	k2eAgNnSc1d1	věnované
J.	J.	kA	J.
Honzlovi	Honzlův	k2eAgMnPc1d1	Honzlův
k	k	k7c3	k
jeho	jeho	k3xOp3gFnPc3	jeho
50	[number]	k4	50
<g/>
.	.	kIx.	.
narozeninám	narozeniny	k1gFnPc3	narozeniny
<g/>
.	.	kIx.	.
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
inscenaci	inscenace	k1gFnSc6	inscenace
účinkoval	účinkovat	k5eAaImAgMnS	účinkovat
jako	jako	k9	jako
host	host	k1gMnSc1	host
Jindřich	Jindřich	k1gMnSc1	Jindřich
Plachta	plachta	k1gFnSc1	plachta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
osvobození	osvobození	k1gNnSc6	osvobození
soubor	soubor	k1gInSc4	soubor
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
krátce	krátce	k6eAd1	krátce
jako	jako	k9	jako
Soubor	soubor	k1gInSc1	soubor
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Honzla	Honzla	k1gMnSc2	Honzla
a	a	k8xC	a
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgInS	být
převeden	převést	k5eAaPmNgInS	převést
do	do	k7c2	do
Studia	studio	k1gNnSc2	studio
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
1941	[number]	k4	1941
J.	J.	kA	J.
Honzl	Honzl	k1gFnPc2	Honzl
<g/>
:	:	kIx,	:
Dvě	dva	k4xCgFnPc1	dva
lásky	láska	k1gFnPc1	láska
Mikoláše	Mikoláš	k1gMnSc2	Mikoláš
Alše	Aleš	k1gMnSc2	Aleš
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Jindřich	Jindřich	k1gMnSc1	Jindřich
Honzl	Honzl	k1gMnSc1	Honzl
1942	[number]	k4	1942
F.	F.	kA	F.
Mach	macha	k1gFnPc2	macha
<g/>
:	:	kIx,	:
Balada	balada	k1gFnSc1	balada
o	o	k7c6	o
stínu	stín	k1gInSc6	stín
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Antonín	Antonín	k1gMnSc1	Antonín
Dvořák	Dvořák	k1gMnSc1	Dvořák
1942	[number]	k4	1942
V.	V.	kA	V.
K.	K.	kA	K.
Klicpera	Klicper	k1gMnSc2	Klicper
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
Hadrián	Hadrián	k1gMnSc1	Hadrián
z	z	k7c2	z
Římsů	říms	k1gInPc2	říms
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Antonín	Antonín	k1gMnSc1	Antonín
Dvořák	Dvořák	k1gMnSc1	Dvořák
1942	[number]	k4	1942
J.	J.	kA	J.
N.	N.	kA	N.
Nestroy	Nestroa	k1gMnSc2	Nestroa
<g/>
,	,	kIx,	,
J.	J.	kA	J.
K.	K.	kA	K.
Tyl	Tyl	k1gMnSc1	Tyl
<g/>
:	:	kIx,	:
Enšpígl	Enšpígl	k1gMnSc1	Enšpígl
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Antonín	Antonín	k1gMnSc1	Antonín
Dvořák	Dvořák	k1gMnSc1	Dvořák
(	(	kIx(	(
<g/>
v	v	k7c4	v
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
roli	role	k1gFnSc4	role
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Stránský	Stránský	k1gMnSc1	Stránský
<g/>
)	)	kIx)	)
1943	[number]	k4	1943
Sofokles	Sofokles	k1gMnSc1	Sofokles
<g/>
:	:	kIx,	:
Slídiči	slídič	k1gMnPc1	slídič
(	(	kIx(	(
<g/>
Zrození	zrození	k1gNnSc1	zrození
komedie	komedie	k1gFnSc2	komedie
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Antonín	Antonín	k1gMnSc1	Antonín
Dvořák	Dvořák	k1gMnSc1	Dvořák
1943	[number]	k4	1943
Viktor	Viktor	k1gMnSc1	Viktor
Dyk	Dyk	k?	Dyk
<g/>
:	:	kIx,	:
Veliký	veliký	k2eAgMnSc1d1	veliký
mág	mág	k1gMnSc1	mág
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Antonín	Antonín	k1gMnSc1	Antonín
Dvořák	Dvořák	k1gMnSc1	Dvořák
(	(	kIx(	(
<g/>
hráno	hrát	k5eAaImNgNnS	hrát
pod	pod	k7c7	pod
hlavičkou	hlavička	k1gFnSc7	hlavička
Studia	studio	k1gNnSc2	studio
Intimního	intimní	k2eAgNnSc2d1	intimní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
)	)	kIx)	)
1943	[number]	k4	1943
J.	J.	kA	J.
W.	W.	kA	W.
Goethe	Goethe	k1gFnSc1	Goethe
<g/>
:	:	kIx,	:
Urfaust	Urfaust	k1gFnSc1	Urfaust
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Antonín	Antonín	k1gMnSc1	Antonín
Dvořák	Dvořák	k1gMnSc1	Dvořák
1943	[number]	k4	1943
Lev	lev	k1gInSc1	lev
Blatný	blatný	k2eAgInSc1d1	blatný
<g/>
:	:	kIx,	:
Smrt	smrt	k1gFnSc1	smrt
na	na	k7c4	na
prodej	prodej	k1gInSc4	prodej
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Antonín	Antonín	k1gMnSc1	Antonín
<g />
.	.	kIx.	.
</s>
<s>
Dvořák	Dvořák	k1gMnSc1	Dvořák
1943	[number]	k4	1943
A.	A.	kA	A.
Dumas	Dumasa	k1gFnPc2	Dumasa
<g/>
:	:	kIx,	:
Dáma	dáma	k1gFnSc1	dáma
s	s	k7c7	s
kameliemi	kamelie	k1gFnPc7	kamelie
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Antonín	Antonín	k1gMnSc1	Antonín
Dvořák	Dvořák	k1gMnSc1	Dvořák
(	(	kIx(	(
<g/>
hráno	hrát	k5eAaImNgNnS	hrát
pod	pod	k7c7	pod
hlavičkou	hlavička	k1gFnSc7	hlavička
Studia	studio	k1gNnSc2	studio
Intimního	intimní	k2eAgNnSc2d1	intimní
divadla	divadlo	k1gNnSc2	divadlo
)	)	kIx)	)
1944	[number]	k4	1944
J.	J.	kA	J.
Pokorný	Pokorný	k1gMnSc1	Pokorný
<g/>
:	:	kIx,	:
Píseň	píseň	k1gFnSc1	píseň
otroků	otrok	k1gMnPc2	otrok
a	a	k8xC	a
bídy	bída	k1gFnSc2	bída
(	(	kIx(	(
<g/>
montáž	montáž	k1gFnSc1	montáž
ze	z	k7c2	z
starořecké	starořecký	k2eAgFnSc2d1	starořecká
lyriky	lyrika	k1gFnSc2	lyrika
<g/>
)	)	kIx)	)
–	–	k?	–
cenzurou	cenzura	k1gFnSc7	cenzura
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
1944	[number]	k4	1944
A.	A.	kA	A.
Jarry	Jarra	k1gFnSc2	Jarra
<g/>
:	:	kIx,	:
<g/>
Král	Král	k1gMnSc1	Král
Ubu	Ubu	k1gMnSc1	Ubu
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Antonín	Antonín	k1gMnSc1	Antonín
Dvořák	Dvořák	k1gMnSc1	Dvořák
(	(	kIx(	(
<g/>
nastudování	nastudování	k1gNnSc1	nastudování
věnováno	věnovat	k5eAaPmNgNnS	věnovat
J.	J.	kA	J.
Honzlovi	Honzlův	k2eAgMnPc1d1	Honzlův
k	k	k7c3	k
jeho	jeho	k3xOp3gFnPc3	jeho
50	[number]	k4	50
<g/>
.	.	kIx.	.
narozeninám	narozeniny	k1gFnPc3	narozeniny
<g/>
)	)	kIx)	)
</s>
