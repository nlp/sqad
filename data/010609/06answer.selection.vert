<s>
Histologie	histologie	k1gFnSc1	histologie
(	(	kIx(	(
<g/>
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
<g/>
:	:	kIx,	:
histos	histos	k1gInSc1	histos
=	=	kIx~	=
tkáň	tkáň	k1gFnSc1	tkáň
<g/>
,	,	kIx,	,
logos	logos	k1gInSc1	logos
=	=	kIx~	=
nauka	nauka	k1gFnSc1	nauka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vědní	vědní	k2eAgFnSc1d1	vědní
disciplína	disciplína	k1gFnSc1	disciplína
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
studiem	studio	k1gNnSc7	studio
mikroskopické	mikroskopický	k2eAgFnSc2d1	mikroskopická
struktury	struktura	k1gFnSc2	struktura
živočišných	živočišný	k2eAgFnPc2d1	živočišná
tkání	tkáň	k1gFnPc2	tkáň
(	(	kIx(	(
<g/>
rostlinné	rostlinný	k2eAgFnSc2d1	rostlinná
tkáně	tkáň	k1gFnSc2	tkáň
–	–	k?	–
pletiva	pletivo	k1gNnSc2	pletivo
–	–	k?	–
jsou	být	k5eAaImIp3nP	být
také	také	k6eAd1	také
zkoumány	zkoumat	k5eAaImNgInP	zkoumat
histologicky	histologicky	k6eAd1	histologicky
<g/>
)	)	kIx)	)
a	a	k8xC	a
orgánů	orgán	k1gInPc2	orgán
mnohobuněčných	mnohobuněčný	k2eAgInPc2d1	mnohobuněčný
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
