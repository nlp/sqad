<p>
<s>
Histologie	histologie	k1gFnSc1	histologie
(	(	kIx(	(
<g/>
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
<g/>
:	:	kIx,	:
histos	histos	k1gInSc1	histos
=	=	kIx~	=
tkáň	tkáň	k1gFnSc1	tkáň
<g/>
,	,	kIx,	,
logos	logos	k1gInSc1	logos
=	=	kIx~	=
nauka	nauka	k1gFnSc1	nauka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vědní	vědní	k2eAgFnSc1d1	vědní
disciplína	disciplína	k1gFnSc1	disciplína
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
studiem	studio	k1gNnSc7	studio
mikroskopické	mikroskopický	k2eAgFnSc2d1	mikroskopická
struktury	struktura	k1gFnSc2	struktura
živočišných	živočišný	k2eAgFnPc2d1	živočišná
tkání	tkáň	k1gFnPc2	tkáň
(	(	kIx(	(
<g/>
rostlinné	rostlinný	k2eAgFnSc2d1	rostlinná
tkáně	tkáň	k1gFnSc2	tkáň
–	–	k?	–
pletiva	pletivo	k1gNnSc2	pletivo
–	–	k?	–
jsou	být	k5eAaImIp3nP	být
také	také	k6eAd1	také
zkoumány	zkoumat	k5eAaImNgInP	zkoumat
histologicky	histologicky	k6eAd1	histologicky
<g/>
)	)	kIx)	)
a	a	k8xC	a
orgánů	orgán	k1gInPc2	orgán
mnohobuněčných	mnohobuněčný	k2eAgInPc2d1	mnohobuněčný
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
předmětem	předmět	k1gInSc7	předmět
studia	studio	k1gNnSc2	studio
na	na	k7c6	na
přírodovědeckých	přírodovědecký	k2eAgFnPc6d1	Přírodovědecká
a	a	k8xC	a
lékařských	lékařský	k2eAgFnPc6d1	lékařská
fakultách	fakulta	k1gFnPc6	fakulta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zařazení	zařazení	k1gNnSc1	zařazení
a	a	k8xC	a
rozdělení	rozdělení	k1gNnSc1	rozdělení
histologie	histologie	k1gFnSc2	histologie
==	==	k?	==
</s>
</p>
<p>
<s>
biologie	biologie	k1gFnSc1	biologie
</s>
</p>
<p>
<s>
morfologie	morfologie	k1gFnSc1	morfologie
</s>
</p>
<p>
<s>
histologie	histologie	k1gFnSc1	histologie
</s>
</p>
<p>
<s>
cytologie	cytologie	k1gFnSc1	cytologie
–	–	k?	–
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
buňce	buňka	k1gFnSc6	buňka
</s>
</p>
<p>
<s>
obecná	obecný	k2eAgFnSc1d1	obecná
histologie	histologie	k1gFnSc1	histologie
–	–	k?	–
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
tkáních	tkáň	k1gFnPc6	tkáň
</s>
</p>
<p>
<s>
speciální	speciální	k2eAgFnSc1d1	speciální
histologie	histologie	k1gFnSc1	histologie
(	(	kIx(	(
<g/>
mikroskopická	mikroskopický	k2eAgFnSc1d1	mikroskopická
anatomie	anatomie	k1gFnSc1	anatomie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
histotechnologieZ	histotechnologieZ	k?	histotechnologieZ
praktických	praktický	k2eAgInPc2d1	praktický
důvodů	důvod	k1gInPc2	důvod
se	se	k3xPyFc4	se
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
na	na	k7c4	na
obecnou	obecný	k2eAgFnSc4d1	obecná
a	a	k8xC	a
speciální	speciální	k2eAgFnSc4d1	speciální
histologii	histologie	k1gFnSc4	histologie
<g/>
.	.	kIx.	.
</s>
<s>
Obecná	obecný	k2eAgFnSc1d1	obecná
histologie	histologie	k1gFnSc1	histologie
je	být	k5eAaImIp3nS	být
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
struktuře	struktura	k1gFnSc6	struktura
základních	základní	k2eAgInPc2d1	základní
typů	typ	k1gInPc2	typ
tkání	tkáň	k1gFnPc2	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
přiřazována	přiřazován	k2eAgMnSc4d1	přiřazován
rovněž	rovněž	k9	rovněž
cytologie	cytologie	k1gFnSc1	cytologie
–	–	k?	–
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
buňce	buňka	k1gFnSc6	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgFnSc1d1	speciální
histologie	histologie	k1gFnSc1	histologie
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
také	také	k9	také
mikroskopická	mikroskopický	k2eAgFnSc1d1	mikroskopická
anatomie	anatomie	k1gFnSc1	anatomie
je	být	k5eAaImIp3nS	být
věda	věda	k1gFnSc1	věda
studující	studující	k2eAgFnSc4d1	studující
mikroskopickou	mikroskopický	k2eAgFnSc4d1	mikroskopická
stavbu	stavba	k1gFnSc4	stavba
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
orgánů	orgán	k1gInPc2	orgán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příbuzné	příbuzný	k2eAgInPc1d1	příbuzný
a	a	k8xC	a
související	související	k2eAgInPc1d1	související
obory	obor	k1gInPc1	obor
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
buněčná	buněčný	k2eAgFnSc1d1	buněčná
biologie	biologie	k1gFnSc1	biologie
–	–	k?	–
studuje	studovat	k5eAaImIp3nS	studovat
struktury	struktura	k1gFnPc1	struktura
menší	malý	k2eAgFnPc1d2	menší
než	než	k8xS	než
buňka	buňka	k1gFnSc1	buňka
<g/>
,	,	kIx,	,
hlavně	hlavně	k6eAd1	hlavně
organely	organela	k1gFnSc2	organela
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
</s>
</p>
<p>
<s>
histopatologie	histopatologie	k1gFnSc1	histopatologie
–	–	k?	–
mikroskopická	mikroskopický	k2eAgFnSc1d1	mikroskopická
struktura	struktura	k1gFnSc1	struktura
nemocných	mocný	k2eNgFnPc2d1	mocný
tkání	tkáň	k1gFnPc2	tkáň
a	a	k8xC	a
orgánů	orgán	k1gInPc2	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
nástrojem	nástroj	k1gInSc7	nástroj
patologické	patologický	k2eAgFnSc2d1	patologická
anatomie	anatomie	k1gFnSc2	anatomie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
histochemie	histochemie	k1gFnSc1	histochemie
–	–	k?	–
chemické	chemický	k2eAgInPc4d1	chemický
pochody	pochod	k1gInPc4	pochod
probíhající	probíhající	k2eAgInSc4d1	probíhající
v	v	k7c6	v
tkáních	tkáň	k1gFnPc6	tkáň
</s>
</p>
<p>
<s>
imunohistochemie	imunohistochemie	k1gFnSc1	imunohistochemie
–	–	k?	–
využívá	využívat	k5eAaPmIp3nS	využívat
antigeny	antigen	k1gInPc4	antigen
a	a	k8xC	a
protilátky	protilátka	k1gFnPc4	protilátka
na	na	k7c4	na
bližší	blízký	k2eAgNnSc4d2	bližší
poznání	poznání	k1gNnSc4	poznání
struktur	struktura	k1gFnPc2	struktura
</s>
</p>
<p>
<s>
anatomie	anatomie	k1gFnSc1	anatomie
–	–	k?	–
struktura	struktura	k1gFnSc1	struktura
orgánů	orgán	k1gInPc2	orgán
a	a	k8xC	a
orgánových	orgánový	k2eAgFnPc2d1	orgánová
soustav	soustava	k1gFnPc2	soustava
</s>
</p>
<p>
<s>
morfologie	morfologie	k1gFnSc1	morfologie
–	–	k?	–
studuje	studovat	k5eAaImIp3nS	studovat
celé	celý	k2eAgInPc4d1	celý
organismy	organismus	k1gInPc4	organismus
</s>
</p>
<p>
<s>
==	==	k?	==
Histologické	histologický	k2eAgFnSc2d1	histologická
techniky	technika	k1gFnSc2	technika
==	==	k?	==
</s>
</p>
<p>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
histologie	histologie	k1gFnSc2	histologie
by	by	kYmCp3nS	by
nebyl	být	k5eNaImAgInS	být
možný	možný	k2eAgInSc1d1	možný
bez	bez	k7c2	bez
vynálezu	vynález	k1gInSc2	vynález
světelného	světelný	k2eAgInSc2d1	světelný
mikroskopu	mikroskop	k1gInSc2	mikroskop
<g/>
;	;	kIx,	;
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
získávání	získávání	k1gNnSc1	získávání
dalších	další	k2eAgInPc2d1	další
poznatků	poznatek	k1gInPc2	poznatek
umožněno	umožnit	k5eAaPmNgNnS	umožnit
díky	díky	k7c3	díky
mikroskopům	mikroskop	k1gInPc3	mikroskop
elektronovým	elektronový	k2eAgInPc3d1	elektronový
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
histologie	histologie	k1gFnSc2	histologie
muselo	muset	k5eAaImAgNnS	muset
dojít	dojít	k5eAaPmF	dojít
i	i	k9	i
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
a	a	k8xC	a
zdokonalování	zdokonalování	k1gNnSc1	zdokonalování
mnoha	mnoho	k4c2	mnoho
technik	technika	k1gFnPc2	technika
fixace	fixace	k1gFnSc2	fixace
a	a	k8xC	a
barvení	barvení	k1gNnSc2	barvení
vzorků	vzorek	k1gInPc2	vzorek
<g/>
.	.	kIx.	.
</s>
<s>
Živočišné	živočišný	k2eAgFnPc1d1	živočišná
tkáně	tkáň	k1gFnPc1	tkáň
totiž	totiž	k9	totiž
nelze	lze	k6eNd1	lze
zkoumat	zkoumat	k5eAaImF	zkoumat
v	v	k7c6	v
nativním	nativní	k2eAgInSc6d1	nativní
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
rozkladné	rozkladný	k2eAgInPc4d1	rozkladný
procesy	proces	k1gInPc4	proces
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
začínají	začínat	k5eAaImIp3nP	začínat
prakticky	prakticky	k6eAd1	prakticky
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
buňky	buňka	k1gFnSc2	buňka
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
nenávratně	návratně	k6eNd1	návratně
ničí	ničit	k5eAaImIp3nS	ničit
původní	původní	k2eAgFnSc4d1	původní
strukturu	struktura	k1gFnSc4	struktura
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Odborníci	odborník	k1gMnPc1	odborník
trénovaní	trénovaný	k2eAgMnPc1d1	trénovaný
v	v	k7c6	v
histologických	histologický	k2eAgFnPc6d1	histologická
technikách	technika	k1gFnPc6	technika
<g/>
,	,	kIx,	,
řezání	řezání	k1gNnSc6	řezání
a	a	k8xC	a
barvení	barvení	k1gNnSc6	barvení
tkání	tkáň	k1gFnPc2	tkáň
jsou	být	k5eAaImIp3nP	být
histologičtí	histologický	k2eAgMnPc1d1	histologický
technologové	technolog	k1gMnPc1	technolog
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Histologic	Histologic	k1gMnSc1	Histologic
Technicians	Techniciansa	k1gFnPc2	Techniciansa
<g/>
,	,	kIx,	,
HT	HT	kA	HT
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
oborem	obor	k1gInSc7	obor
je	být	k5eAaImIp3nS	být
histotechnologie	histotechnologie	k1gFnSc1	histotechnologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Histologický	histologický	k2eAgInSc4d1	histologický
artefakt	artefakt	k1gInSc4	artefakt
je	být	k5eAaImIp3nS	být
struktura	struktura	k1gFnSc1	struktura
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
chybí	chybit	k5eAaPmIp3nS	chybit
v	v	k7c6	v
živých	živý	k2eAgFnPc6d1	živá
tkáních	tkáň	k1gFnPc6	tkáň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
objevila	objevit	k5eAaPmAgFnS	objevit
se	se	k3xPyFc4	se
během	během	k7c2	během
přípravy	příprava	k1gFnSc2	příprava
histologického	histologický	k2eAgInSc2d1	histologický
preparátu	preparát	k1gInSc2	preparát
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
během	během	k7c2	během
barvení	barvení	k1gNnSc2	barvení
<g/>
,	,	kIx,	,
montování	montování	k1gNnSc1	montování
v	v	k7c6	v
entellanu	entellan	k1gInSc6	entellan
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
Histologický	histologický	k2eAgMnSc1d1	histologický
technolog	technolog	k1gMnSc1	technolog
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
snaží	snažit	k5eAaImIp3nP	snažit
vznik	vznik	k1gInSc4	vznik
artefaktů	artefakt	k1gInPc2	artefakt
minimalizovat	minimalizovat	k5eAaBmF	minimalizovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
našla	najít	k5eAaPmAgFnS	najít
histologie	histologie	k1gFnSc1	histologie
své	svůj	k3xOyFgNnSc4	svůj
využití	využití	k1gNnSc4	využití
při	při	k7c6	při
zkoumání	zkoumání	k1gNnSc6	zkoumání
patologických	patologický	k2eAgFnPc2d1	patologická
změn	změna	k1gFnPc2	změna
tkáně	tkáň	k1gFnSc2	tkáň
(	(	kIx(	(
<g/>
zhoubné	zhoubný	k2eAgNnSc1d1	zhoubné
bujení	bujení	k1gNnSc1	bujení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
histochemických	histochemický	k2eAgFnPc2d1	histochemická
technik	technika	k1gFnPc2	technika
lze	lze	k6eAd1	lze
prokázat	prokázat	k5eAaPmF	prokázat
přítomnost	přítomnost	k1gFnSc4	přítomnost
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
enzymů	enzym	k1gInPc2	enzym
<g/>
)	)	kIx)	)
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
a	a	k8xC	a
tkáních	tkáň	k1gFnPc6	tkáň
a	a	k8xC	a
lépe	dobře	k6eAd2	dobře
tak	tak	k6eAd1	tak
porozumět	porozumět	k5eAaPmF	porozumět
chemickým	chemický	k2eAgFnPc3d1	chemická
procesů	proces	k1gInPc2	proces
v	v	k7c6	v
živém	živý	k2eAgInSc6d1	živý
organismu	organismus	k1gInSc6	organismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
buňka	buňka	k1gFnSc1	buňka
</s>
</p>
<p>
<s>
pletivo	pletivo	k1gNnSc1	pletivo
</s>
</p>
<p>
<s>
tkáň	tkáň	k1gFnSc1	tkáň
</s>
</p>
<p>
<s>
barvení	barvení	k1gNnSc1	barvení
(	(	kIx(	(
<g/>
biologie	biologie	k1gFnSc1	biologie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
apoptóza	apoptóza	k1gFnSc1	apoptóza
</s>
</p>
<p>
<s>
biopsie	biopsie	k1gFnSc1	biopsie
</s>
</p>
<p>
<s>
mikroskopické	mikroskopický	k2eAgInPc1d1	mikroskopický
preparáty	preparát	k1gInPc1	preparát
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Histologie	histologie	k1gFnSc2	histologie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
===	===	k?	===
Atlasy	Atlas	k1gInPc4	Atlas
===	===	k?	===
</s>
</p>
<p>
<s>
Atlas	Atlas	k1gInSc1	Atlas
histologických	histologický	k2eAgFnPc2d1	histologická
technik	technika	k1gFnPc2	technika
</s>
</p>
<p>
<s>
Histologický	histologický	k2eAgInSc4d1	histologický
atlas	atlas	k1gInSc4	atlas
MedAtlas	MedAtlasa	k1gFnPc2	MedAtlasa
</s>
</p>
<p>
<s>
Atlas	Atlas	k1gInSc1	Atlas
histologie	histologie	k1gFnSc2	histologie
LF	LF	kA	LF
UP	UP	kA	UP
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Histology	histolog	k1gMnPc4	histolog
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Internet	Internet	k1gInSc1	Internet
Atlas	Atlas	k1gInSc1	Atlas
of	of	k?	of
Histology	histolog	k1gMnPc7	histolog
</s>
</p>
<p>
<s>
===	===	k?	===
Ústavy	ústava	k1gFnSc2	ústava
===	===	k?	===
</s>
</p>
<p>
<s>
Ústav	ústav	k1gInSc1	ústav
histologie	histologie	k1gFnSc2	histologie
a	a	k8xC	a
embryologie	embryologie	k1gFnSc2	embryologie
LF	LF	kA	LF
UP	UP	kA	UP
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
</s>
</p>
<p>
<s>
CBO	CBO	kA	CBO
-	-	kIx~	-
Oddělení	oddělení	k1gNnSc4	oddělení
histologie	histologie	k1gFnSc2	histologie
a	a	k8xC	a
embryologie	embryologie	k1gFnSc1	embryologie
3	[number]	k4	3
<g/>
.	.	kIx.	.
lékařské	lékařský	k2eAgFnSc2d1	lékařská
fakulty	fakulta	k1gFnSc2	fakulta
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
Ústav	ústav	k1gInSc1	ústav
histologie	histologie	k1gFnSc2	histologie
a	a	k8xC	a
embryologie	embryologie	k1gFnSc2	embryologie
(	(	kIx(	(
<g/>
Lékařská	lékařský	k2eAgFnSc1d1	lékařská
fakulta	fakulta	k1gFnSc1	fakulta
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
,	,	kIx,	,
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ústav	ústav	k1gInSc1	ústav
anatomie	anatomie	k1gFnSc2	anatomie
<g/>
,	,	kIx,	,
histologie	histologie	k1gFnSc2	histologie
a	a	k8xC	a
embryologie	embryologie	k1gFnSc2	embryologie
Fakulty	fakulta	k1gFnSc2	fakulta
veterinárního	veterinární	k2eAgNnSc2d1	veterinární
lékařství	lékařství	k1gNnSc2	lékařství
VFU	VFU	kA	VFU
Brno	Brno	k1gNnSc1	Brno
</s>
</p>
<p>
<s>
Ústav	ústav	k1gInSc1	ústav
histologie	histologie	k1gFnSc2	histologie
a	a	k8xC	a
embryologie	embryologie	k1gFnSc2	embryologie
MU	MU	kA	MU
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
-	-	kIx~	-
Lékařská	lékařský	k2eAgFnSc1d1	lékařská
fakulta	fakulta	k1gFnSc1	fakulta
</s>
</p>
<p>
<s>
===	===	k?	===
Ostatní	ostatní	k2eAgNnPc4d1	ostatní
===	===	k?	===
</s>
</p>
<p>
<s>
Histologie	histologie	k1gFnSc1	histologie
močového	močový	k2eAgInSc2d1	močový
měchýře	měchýř	k1gInSc2	měchýř
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
společnost	společnost	k1gFnSc1	společnost
histologických	histologický	k2eAgMnPc2d1	histologický
laborantů	laborant	k1gMnPc2	laborant
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnSc1	kategorie
<g/>
:	:	kIx,	:
<g/>
Histologie	histologie	k1gFnSc1	histologie
na	na	k7c6	na
Medik	medik	k1gMnSc1	medik
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Imunohistologické	Imunohistologický	k2eAgFnPc4d1	Imunohistologický
metody	metoda	k1gFnPc4	metoda
</s>
</p>
