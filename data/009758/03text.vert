<p>
<s>
Toxicita	toxicita	k1gFnSc1	toxicita
je	být	k5eAaImIp3nS	být
vlastnost	vlastnost	k1gFnSc4	vlastnost
chemických	chemický	k2eAgFnPc2d1	chemická
sloučenin	sloučenina	k1gFnPc2	sloučenina
spočívající	spočívající	k2eAgFnSc1d1	spočívající
ve	v	k7c6	v
vyvolání	vyvolání	k1gNnSc6	vyvolání
otravy	otrava	k1gFnSc2	otrava
osob	osoba	k1gFnPc2	osoba
nebo	nebo	k8xC	nebo
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
látku	látka	k1gFnSc4	látka
požily	požít	k5eAaPmAgInP	požít
<g/>
,	,	kIx,	,
vdechly	vdechnout	k5eAaPmAgInP	vdechnout
nebo	nebo	k8xC	nebo
vstřebaly	vstřebat	k5eAaPmAgInP	vstřebat
kůží	kůže	k1gFnSc7	kůže
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
všechny	všechen	k3xTgFnPc1	všechen
chemické	chemický	k2eAgFnPc1d1	chemická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
při	pře	k1gFnSc4	pře
užití	užití	k1gNnSc2	užití
příliš	příliš	k6eAd1	příliš
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
toxické	toxický	k2eAgNnSc1d1	toxické
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
i	i	k9	i
požití	požití	k1gNnSc4	požití
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
vody	voda	k1gFnSc2	voda
může	moct	k5eAaImIp3nS	moct
rozvrátit	rozvrátit	k5eAaPmF	rozvrátit
homeostázu	homeostáza	k1gFnSc4	homeostáza
a	a	k8xC	a
způsobit	způsobit	k5eAaPmF	způsobit
člověku	člověk	k1gMnSc3	člověk
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
prakticky	prakticky	k6eAd1	prakticky
všechny	všechen	k3xTgInPc1	všechen
syntetické	syntetický	k2eAgInPc1d1	syntetický
léky	lék	k1gInPc1	lék
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
větším	veliký	k2eAgNnSc6d2	veliký
množství	množství	k1gNnSc6	množství
toxické	toxický	k2eAgInPc1d1	toxický
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
čím	čí	k3xOyRgNnSc7	čí
jsou	být	k5eAaImIp3nP	být
účinnější	účinný	k2eAgFnSc4d2	účinnější
<g/>
,	,	kIx,	,
tím	ten	k3xDgInSc7	ten
jsou	být	k5eAaImIp3nP	být
toxičtější	toxický	k2eAgFnPc1d2	toxičtější
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Měření	měření	k1gNnPc1	měření
==	==	k?	==
</s>
</p>
<p>
<s>
Aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
toxicitu	toxicita	k1gFnSc4	toxicita
měřit	měřit	k5eAaImF	měřit
a	a	k8xC	a
srovnávat	srovnávat	k5eAaImF	srovnávat
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
stupnice	stupnice	k1gFnSc1	stupnice
označovaná	označovaný	k2eAgFnSc1d1	označovaná
zkratkou	zkratka	k1gFnSc7	zkratka
LD	LD	kA	LD
(	(	kIx(	(
<g/>
z	z	k7c2	z
anglického	anglický	k2eAgNnSc2d1	anglické
lethal	lethat	k5eAaImAgInS	lethat
dose	dose	k1gFnSc7	dose
–	–	k?	–
smrtná	smrtný	k2eAgFnSc1d1	Smrtná
dávka	dávka	k1gFnSc1	dávka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
variantou	varianta	k1gFnSc7	varianta
LD	LD	kA	LD
<g/>
50	[number]	k4	50
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
označení	označení	k1gNnSc4	označení
dávky	dávka	k1gFnSc2	dávka
<g/>
,	,	kIx,	,
po	po	k7c6	po
které	který	k3yIgFnSc6	který
uhynulo	uhynout	k5eAaPmAgNnS	uhynout
50	[number]	k4	50
ze	z	k7c2	z
100	[number]	k4	100
laboratorních	laboratorní	k2eAgMnPc2d1	laboratorní
potkanů	potkan	k1gMnPc2	potkan
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
byla	být	k5eAaImAgFnS	být
látka	látka	k1gFnSc1	látka
podána	podat	k5eAaPmNgFnS	podat
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
(	(	kIx(	(
<g/>
především	především	k9	především
orálně	orálně	k6eAd1	orálně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
varianta	varianta	k1gFnSc1	varianta
LD90	LD90	k1gMnPc2	LD90
pro	pro	k7c4	pro
úhyn	úhyn	k1gInSc4	úhyn
90	[number]	k4	90
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jinou	jiný	k2eAgFnSc7d1	jiná
stupnicí	stupnice	k1gFnSc7	stupnice
<g/>
,	,	kIx,	,
užívanou	užívaný	k2eAgFnSc4d1	užívaná
pro	pro	k7c4	pro
plynné	plynný	k2eAgFnPc4d1	plynná
sloučeniny	sloučenina	k1gFnPc4	sloučenina
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
smrtná	smrtný	k2eAgFnSc1d1	Smrtná
koncentrace	koncentrace	k1gFnSc1	koncentrace
ve	v	k7c6	v
vdechovaném	vdechovaný	k2eAgInSc6d1	vdechovaný
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
,	,	kIx,	,
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
LC	LC	kA	LC
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Toxicita	toxicita	k1gFnSc1	toxicita
je	být	k5eAaImIp3nS	být
buď	buď	k8xC	buď
akutní	akutní	k2eAgFnSc7d1	akutní
(	(	kIx(	(
<g/>
po	po	k7c6	po
jednorázové	jednorázový	k2eAgFnSc6d1	jednorázová
aplikaci	aplikace	k1gFnSc6	aplikace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
chronická	chronický	k2eAgFnSc1d1	chronická
(	(	kIx(	(
<g/>
po	po	k7c6	po
opakované	opakovaný	k2eAgFnSc6d1	opakovaná
aplikaci	aplikace	k1gFnSc6	aplikace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
udávání	udávání	k1gNnSc6	udávání
hodnot	hodnota	k1gFnPc2	hodnota
LD	LD	kA	LD
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
dávkou	dávka	k1gFnSc7	dávka
na	na	k7c4	na
kg	kg	kA	kg
a	a	k8xC	a
koncentrací	koncentrace	k1gFnSc7	koncentrace
zkoumané	zkoumaný	k2eAgFnSc2d1	zkoumaná
sloučeniny	sloučenina	k1gFnSc2	sloučenina
uvést	uvést	k5eAaPmF	uvést
i	i	k9	i
způsob	způsob	k1gInSc4	způsob
aplikace	aplikace	k1gFnSc2	aplikace
<g/>
,	,	kIx,	,
např.	např.	kA	např.
perorální	perorální	k2eAgFnSc1d1	perorální
(	(	kIx(	(
<g/>
ústy	ústa	k1gNnPc7	ústa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
perkutánní	perkutánní	k2eAgFnSc7d1	perkutánní
(	(	kIx(	(
<g/>
kůží	kůže	k1gFnSc7	kůže
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
inhalační	inhalační	k2eAgFnSc1d1	inhalační
(	(	kIx(	(
<g/>
vdechováním	vdechování	k1gNnSc7	vdechování
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
parenterální	parenterální	k2eAgFnSc1d1	parenterální
(	(	kIx(	(
<g/>
mimostřevní	mimostřevní	k2eAgFnSc1d1	mimostřevní
<g/>
,	,	kIx,	,
např.	např.	kA	např.
nitrožilní	nitrožilní	k2eAgFnSc1d1	nitrožilní
<g/>
,	,	kIx,	,
nitrosvalový	nitrosvalový	k2eAgInSc1d1	nitrosvalový
<g/>
,	,	kIx,	,
podkožní	podkožní	k2eAgInSc1d1	podkožní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rektální	rektální	k2eAgMnSc1d1	rektální
(	(	kIx(	(
<g/>
konečníkový	konečníkový	k2eAgMnSc1d1	konečníkový
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
uvést	uvést	k5eAaPmF	uvést
druh	druh	k1gInSc4	druh
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc4	jeho
pohlaví	pohlaví	k1gNnSc4	pohlaví
<g/>
,	,	kIx,	,
stáří	stáří	k1gNnSc4	stáří
a	a	k8xC	a
dobu	doba	k1gFnSc4	doba
pozorování	pozorování	k1gNnSc2	pozorování
zvířete	zvíře	k1gNnSc2	zvíře
po	po	k7c6	po
aplikaci	aplikace	k1gFnSc6	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
chronické	chronický	k2eAgFnSc2d1	chronická
toxicity	toxicita	k1gFnSc2	toxicita
je	být	k5eAaImIp3nS	být
krom	krom	k7c2	krom
uvedených	uvedený	k2eAgInPc2d1	uvedený
údajů	údaj	k1gInPc2	údaj
nutné	nutný	k2eAgNnSc1d1	nutné
uvádět	uvádět	k5eAaImF	uvádět
i	i	k9	i
intervaly	interval	k1gInPc1	interval
aplikací	aplikace	k1gFnPc2	aplikace
a	a	k8xC	a
dobu	doba	k1gFnSc4	doba
celého	celý	k2eAgInSc2d1	celý
pokusu	pokus	k1gInSc2	pokus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozdělení	rozdělení	k1gNnSc1	rozdělení
látek	látka	k1gFnPc2	látka
dle	dle	k7c2	dle
toxicity	toxicita	k1gFnSc2	toxicita
==	==	k?	==
</s>
</p>
<p>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
několik	několik	k4yIc1	několik
stupnic	stupnice	k1gFnPc2	stupnice
dle	dle	k7c2	dle
organizmu	organizmus	k1gInSc2	organizmus
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yIgInSc4	který
má	mít	k5eAaImIp3nS	mít
látka	látka	k1gFnSc1	látka
působit	působit	k5eAaImF	působit
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
látky	látka	k1gFnPc1	látka
dělí	dělit	k5eAaImIp3nP	dělit
dle	dle	k7c2	dle
působení	působení	k1gNnSc2	působení
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
a	a	k8xC	a
na	na	k7c4	na
včely	včela	k1gFnPc4	včela
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
toxicity	toxicita	k1gFnSc2	toxicita
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
obalech	obal	k1gInPc6	obal
používaných	používaný	k2eAgFnPc2d1	používaná
látek	látka	k1gFnPc2	látka
uváděny	uvádět	k5eAaImNgFnP	uvádět
značky	značka	k1gFnPc1	značka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
T	T	kA	T
<g/>
+	+	kIx~	+
–	–	k?	–
látka	látka	k1gFnSc1	látka
vysoce	vysoce	k6eAd1	vysoce
toxická	toxický	k2eAgFnSc1d1	toxická
</s>
</p>
<p>
<s>
T	T	kA	T
–	–	k?	–
látka	látka	k1gFnSc1	látka
toxická	toxický	k2eAgFnSc1d1	toxická
</s>
</p>
<p>
<s>
Xn	Xn	k?	Xn
–	–	k?	–
látka	látka	k1gFnSc1	látka
zdraví	zdravit	k5eAaImIp3nS	zdravit
škodlivá	škodlivý	k2eAgFnSc1d1	škodlivá
</s>
</p>
<p>
<s>
Xi	Xi	k?	Xi
–	–	k?	–
látka	látka	k1gFnSc1	látka
dráždivá	dráždivý	k2eAgFnSc1d1	dráždivá
</s>
</p>
<p>
<s>
C	C	kA	C
–	–	k?	–
látka	látka	k1gFnSc1	látka
žíravá	žíravý	k2eAgFnSc1d1	žíravá
</s>
</p>
<p>
<s>
bez	bez	k7c2	bez
značky	značka	k1gFnSc2	značka
–	–	k?	–
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
u	u	k7c2	u
níž	jenž	k3xRgFnSc2	jenž
není	být	k5eNaImIp3nS	být
toxicita	toxicita	k1gFnSc1	toxicita
udána	udán	k2eAgFnSc1d1	udána
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Není	být	k5eNaImIp3nS	být
totéž	týž	k3xTgNnSc1	týž
jako	jako	k9	jako
látka	látka	k1gFnSc1	látka
netoxická	toxický	k2eNgFnSc1d1	netoxická
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Příklady	příklad	k1gInPc1	příklad
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
nejznámějších	známý	k2eAgInPc2d3	nejznámější
jedů	jed	k1gInPc2	jed
<g/>
:	:	kIx,	:
arsenik	arsenik	k1gInSc1	arsenik
má	mít	k5eAaImIp3nS	mít
LD50	LD50	k1gFnSc4	LD50
=	=	kIx~	=
20	[number]	k4	20
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
hmotnosti	hmotnost	k1gFnSc2	hmotnost
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
kyanovodík	kyanovodík	k1gInSc1	kyanovodík
</s>
</p>
<p>
<s>
má	můj	k3xOp1gFnSc1	můj
LD50	LD50	k1gFnSc1	LD50
=	=	kIx~	=
1,5	[number]	k4	1,5
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
a	a	k8xC	a
LD50	LD50	k1gMnSc1	LD50
THC	THC	kA	THC
=	=	kIx~	=
1259	[number]	k4	1259
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
jed	jed	k1gInSc1	jed
</s>
</p>
<p>
<s>
otrava	otrava	k1gFnSc1	otrava
</s>
</p>
<p>
<s>
toxikologie	toxikologie	k1gFnSc1	toxikologie
</s>
</p>
