<s>
Near	Near	k1gMnSc1
Field	Field	k1gMnSc1
Communication	Communication	k1gInSc4
</s>
<s>
Mobilní	mobilní	k2eAgInSc4d1
telefon	telefon	k1gInSc4
Nokia	Nokia	kA
vybavený	vybavený	k2eAgInSc4d1
NFC	NFC	kA
technologií	technologie	k1gFnPc2
</s>
<s>
Near	Near	k1gInSc1
field	field	k1gInSc1
communication	communication	k1gInSc1
(	(	kIx(
<g/>
NFC	NFC	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
modulární	modulární	k2eAgFnSc1d1
technologie	technologie	k1gFnSc1
rádiové	rádiový	k2eAgFnSc2d1
bezdrátové	bezdrátový	k2eAgFnSc2d1
komunikace	komunikace	k1gFnSc2
mezi	mezi	k7c7
elektronickými	elektronický	k2eAgNnPc7d1
zařízeními	zařízení	k1gNnPc7
na	na	k7c4
velmi	velmi	k6eAd1
krátkou	krátký	k2eAgFnSc4d1
vzdálenost	vzdálenost	k1gFnSc4
(	(	kIx(
<g/>
do	do	k7c2
4	#num#	k4
cm	cm	kA
<g/>
)	)	kIx)
s	s	k7c7
přiblížením	přiblížení	k1gNnSc7
přístrojů	přístroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
architekturu	architektura	k1gFnSc4
definuje	definovat	k5eAaBmIp3nS
sada	sada	k1gFnSc1
standardů	standard	k1gInPc2
ISO	ISO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současné	současný	k2eAgInPc1d1
a	a	k8xC
předpokládané	předpokládaný	k2eAgNnSc1d1
využití	využití	k1gNnSc1
této	tento	k3xDgFnSc2
technologie	technologie	k1gFnSc2
je	být	k5eAaImIp3nS
především	především	k9
ve	v	k7c6
výměně	výměna	k1gFnSc6
klíčových	klíčový	k2eAgNnPc2d1
dat	datum	k1gNnPc2
při	při	k7c6
bezkontaktních	bezkontaktní	k2eAgFnPc6d1
finančních	finanční	k2eAgFnPc6d1
transakcích	transakce	k1gFnPc6
a	a	k8xC
ve	v	k7c6
zjednodušené	zjednodušený	k2eAgFnSc6d1
konfiguraci	konfigurace	k1gFnSc6
spojení	spojení	k1gNnSc6
rádiových	rádiový	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
např.	např.	kA
Wi-Fi	Wi-Fi	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
S	s	k7c7
využitím	využití	k1gNnSc7
této	tento	k3xDgFnSc2
technologie	technologie	k1gFnSc2
se	se	k3xPyFc4
počítá	počítat	k5eAaImIp3nS
ve	v	k7c6
vzájemné	vzájemný	k2eAgFnSc6d1
komunikaci	komunikace	k1gFnSc6
jak	jak	k8xC,k8xS
dvou	dva	k4xCgInPc2
aktivních	aktivní	k2eAgInPc2d1
přístrojů	přístroj	k1gInPc2
(	(	kIx(
<g/>
např.	např.	kA
příslušně	příslušně	k6eAd1
vybavených	vybavený	k2eAgInPc2d1
mobilních	mobilní	k2eAgInPc2d1
telefonů	telefon	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tak	tak	k6eAd1
aktivních	aktivní	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
s	s	k7c7
pasivními	pasivní	k2eAgNnPc7d1
zařízeními	zařízení	k1gNnPc7
(	(	kIx(
<g/>
s	s	k7c7
tzv.	tzv.	kA
tagem	tag	k1gInSc7
<g/>
,	,	kIx,
pasivním	pasivní	k2eAgInSc7d1
nenapájeným	napájený	k2eNgInSc7d1
NFC	NFC	kA
zařízením	zařízení	k1gNnSc7
<g/>
)	)	kIx)
jako	jako	k8xS,k8xC
čtečka	čtečka	k1gFnSc1
s	s	k7c7
bezkontaktní	bezkontaktní	k2eAgFnSc7d1
platební	platební	k2eAgFnSc7d1
kartou	karta	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Technologie	technologie	k1gFnSc1
NFC	NFC	kA
je	být	k5eAaImIp3nS
popisována	popisován	k2eAgFnSc1d1
standardy	standard	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
zahrnují	zahrnovat	k5eAaImIp3nP
několik	několik	k4yIc4
komunikačních	komunikační	k2eAgInPc2d1
protokolů	protokol	k1gInPc2
a	a	k8xC
formátů	formát	k1gInPc2
popisujících	popisující	k2eAgInPc2d1
přenášená	přenášený	k2eAgNnPc4d1
data	datum	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
založena	založit	k5eAaPmNgFnS
na	na	k7c6
standardech	standard	k1gInPc6
RFID	RFID	kA
zahrnující	zahrnující	k2eAgInSc4d1
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
14443	#num#	k4
a	a	k8xC
FeliCa	FeliCa	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Tyto	tento	k3xDgInPc1
standardy	standard	k1gInPc1
jsou	být	k5eAaImIp3nP
součástí	součást	k1gFnSc7
normy	norma	k1gFnSc2
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
18092	#num#	k4
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
definovaném	definovaný	k2eAgNnSc6d1
neziskovou	ziskový	k2eNgFnSc7d1
organizací	organizace	k1gFnSc7
NFC	NFC	kA
Forum	forum	k1gNnSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
firmami	firma	k1gFnPc7
Nokia	Nokia	kA
<g/>
,	,	kIx,
Philips	Philips	kA
a	a	k8xC
Sony	Sony	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
organizace	organizace	k1gFnSc1
čítá	čítat	k5eAaImIp3nS
přes	přes	k7c4
170	#num#	k4
členů	člen	k1gInPc2
<g/>
,	,	kIx,
prosazuje	prosazovat	k5eAaImIp3nS
NFC	NFC	kA
a	a	k8xC
certifikuje	certifikovat	k5eAaImIp3nS
zařízení	zařízení	k1gNnSc1
na	na	k7c4
shodu	shoda	k1gFnSc4
s	s	k7c7
uvedenou	uvedený	k2eAgFnSc7d1
normou	norma	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Případy	případ	k1gInPc1
užití	užití	k1gNnSc2
</s>
<s>
Technologie	technologie	k1gFnSc1
NFC	NFC	kA
umožňuje	umožňovat	k5eAaImIp3nS
také	také	k9
oboucestnou	oboucestný	k2eAgFnSc4d1
komunikaci	komunikace	k1gFnSc4
mezi	mezi	k7c7
koncovými	koncový	k2eAgNnPc7d1
zařízeními	zařízení	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předchozí	předchozí	k2eAgInPc1d1
systémy	systém	k1gInPc1
postavené	postavený	k2eAgInPc1d1
na	na	k7c6
pouhém	pouhý	k2eAgNnSc6d1
čtení	čtení	k1gNnSc6
bezkontaktních	bezkontaktní	k2eAgInPc2d1
čipů	čip	k1gInPc2
RFID	RFID	kA
umožňovaly	umožňovat	k5eAaImAgInP
pouze	pouze	k6eAd1
jednocestnou	jednocestný	k2eAgFnSc4d1
komunikaci	komunikace	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Nenapájené	napájený	k2eNgFnSc6d1
NFC	NFC	kA
„	„	k?
<g/>
tagy	tag	k1gInPc1
<g/>
“	“	k?
(	(	kIx(
<g/>
bezkontaktní	bezkontaktní	k2eAgFnSc2d1
platební	platební	k2eAgFnSc2d1
karty	karta	k1gFnSc2
a	a	k8xC
identifikační	identifikační	k2eAgInPc1d1
prvky	prvek	k1gInPc1
<g/>
,	,	kIx,
zvané	zvaný	k2eAgInPc1d1
"	"	kIx"
<g/>
čipy	čip	k1gInPc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
např.	např.	kA
pro	pro	k7c4
kontrolu	kontrola	k1gFnSc4
vstupu	vstup	k1gInSc2
<g/>
)	)	kIx)
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
čtené	čtený	k2eAgInPc1d1
i	i	k9
aktivními	aktivní	k2eAgFnPc7d1
NFC	NFC	kA
zařízeními	zařízení	k1gNnPc7
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
takže	takže	k8xS
systém	systém	k1gInSc1
NFC	NFC	kA
může	moct	k5eAaImIp3nS
postupně	postupně	k6eAd1
nahradit	nahradit	k5eAaPmF
tyto	tento	k3xDgInPc4
jednostranné	jednostranný	k2eAgInPc4d1
systémy	systém	k1gInPc4
a	a	k8xC
také	také	k9
souběžný	souběžný	k2eAgInSc4d1
provoz	provoz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1
výhoda	výhoda	k1gFnSc1
oproti	oproti	k7c3
dosavadní	dosavadní	k2eAgFnSc3d1
bezkontaktní	bezkontaktní	k2eAgFnSc3d1
identifikaci	identifikace	k1gFnSc3
<g/>
,	,	kIx,
zejména	zejména	k9
pomocí	pomocí	k7c2
RFID	RFID	kA
(	(	kIx(
<g/>
platební	platební	k2eAgFnSc2d1
karty	karta	k1gFnSc2
<g/>
,	,	kIx,
kontroly	kontrola	k1gFnSc2
vstupu	vstup	k1gInSc2
<g/>
,	,	kIx,
ochrana	ochrana	k1gFnSc1
navázání	navázání	k1gNnSc2
komunikace	komunikace	k1gFnSc1
<g/>
)	)	kIx)
spočívá	spočívat	k5eAaImIp3nS
v	v	k7c6
</s>
<s>
odstranění	odstranění	k1gNnSc1
nutnosti	nutnost	k1gFnSc2
zvláštních	zvláštní	k2eAgInPc2d1
identifikačních	identifikační	k2eAgInPc2d1
prvků	prvek	k1gInPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
karty	karta	k1gFnPc1
<g/>
,	,	kIx,
klíčenky	klíčenka	k1gFnPc1
a	a	k8xC
podobně	podobně	k6eAd1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
může	moct	k5eAaImIp3nS
nahradit	nahradit	k5eAaPmF
jediný	jediný	k2eAgInSc1d1
<g/>
,	,	kIx,
společný	společný	k2eAgInSc1d1
<g/>
,	,	kIx,
</s>
<s>
propojení	propojení	k1gNnSc1
s	s	k7c7
komunikací	komunikace	k1gFnSc7
bezdrátovými	bezdrátový	k2eAgInPc7d1
systémy	systém	k1gInPc7
na	na	k7c4
delší	dlouhý	k2eAgFnSc4d2
vzdálenost	vzdálenost	k1gFnSc4
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
GSM	GSM	kA
(	(	kIx(
<g/>
a	a	k8xC
navazující	navazující	k2eAgInPc1d1
standardy	standard	k1gInPc1
digitální	digitální	k2eAgFnSc2d1
komunikace	komunikace	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Wi-Fi	Wi-Fi	k1gNnPc2
a	a	k8xC
Bluetooth	Bluetootha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Platební	platební	k2eAgInPc1d1
systémy	systém	k1gInPc1
</s>
<s>
Pro	pro	k7c4
platební	platební	k2eAgInPc4d1
systémy	systém	k1gInPc4
využívající	využívající	k2eAgFnSc2d1
debetní	debetní	k2eAgFnSc2d1
či	či	k8xC
kreditní	kreditní	k2eAgFnSc2d1
karty	karta	k1gFnSc2
a	a	k8xC
čipové	čipový	k2eAgFnSc2d1
karty	karta	k1gFnSc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
Smartcards	Smartcards	k1gInSc1
<g/>
)	)	kIx)
může	moct	k5eAaImIp3nS
znamenat	znamenat	k5eAaImF
použití	použití	k1gNnSc4
technologie	technologie	k1gFnSc2
NFC	NFC	kA
alternativu	alternativa	k1gFnSc4
či	či	k8xC
kompletní	kompletní	k2eAgFnSc4d1
náhradu	náhrada	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
příklad	příklad	k1gInSc4
můžeme	moct	k5eAaImIp1nP
uvést	uvést	k5eAaPmF
Google	Google	k1gNnSc2
Wallet	Wallet	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
umožňuje	umožňovat	k5eAaImIp3nS
zákazníkům	zákazník	k1gMnPc3
uložit	uložit	k5eAaPmF
si	se	k3xPyFc3
informace	informace	k1gFnPc4
o	o	k7c6
platební	platební	k2eAgFnSc6d1
kartě	karta	k1gFnSc6
resp.	resp.	kA
perspektivně	perspektivně	k6eAd1
o	o	k7c6
více	hodně	k6eAd2
platebních	platební	k2eAgFnPc6d1
kartách	karta	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
při	při	k7c6
jakékoli	jakýkoli	k3yIgFnSc6
platbě	platba	k1gFnSc6
u	u	k7c2
terminálu	terminál	k1gInSc2
MasterCard	MasterCarda	k1gFnPc2
PayPass	PayPassa	k1gFnPc2
mohou	moct	k5eAaImIp3nP
využít	využít	k5eAaPmF
svůj	svůj	k3xOyFgInSc4
mobilní	mobilní	k2eAgInSc4d1
telefon	telefon	k1gInSc4
podporující	podporující	k2eAgInSc4d1
NFC	NFC	kA
pro	pro	k7c4
platební	platební	k2eAgFnPc4d1
transakce	transakce	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Německo	Německo	k1gNnSc1
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Rakousko	Rakousko	k1gNnSc1
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Itálie	Itálie	k1gFnSc1
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
již	již	k6eAd1
vyzkoušely	vyzkoušet	k5eAaPmAgFnP
a	a	k8xC
zavedly	zavést	k5eAaPmAgFnP
NFC	NFC	kA
jako	jako	k8xS,k8xC
způsob	způsob	k1gInSc1
prodeje	prodej	k1gInSc2
jízdenek	jízdenka	k1gFnPc2
pro	pro	k7c4
veřejnou	veřejný	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čína	Čína	k1gFnSc1
již	již	k6eAd1
běžně	běžně	k6eAd1
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
NFC	NFC	kA
v	v	k7c6
autobusech	autobus	k1gInPc6
veřejné	veřejný	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
a	a	k8xC
Indie	Indie	k1gFnSc2
zavádí	zavádět	k5eAaImIp3nS
pokladny	pokladna	k1gFnSc2
podporující	podporující	k2eAgFnSc2d1
NFC	NFC	kA
transakce	transakce	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kontakty	kontakt	k1gInPc4
lidí	člověk	k1gMnPc2
s	s	k7c7
bluetooth	bluetooth	k1gInSc4
a	a	k8xC
Wi-Fi	Wi-Fe	k1gFnSc4
připojení	připojení	k1gNnSc2
</s>
<s>
Další	další	k2eAgFnSc7d1
možností	možnost	k1gFnSc7
využití	využití	k1gNnSc2
technologie	technologie	k1gFnSc2
NFC	NFC	kA
jsou	být	k5eAaImIp3nP
situace	situace	k1gFnSc1
<g/>
,	,	kIx,
při	při	k7c6
níž	jenž	k3xRgFnSc6
se	se	k3xPyFc4
setkávají	setkávat	k5eAaImIp3nP
skupiny	skupina	k1gFnPc1
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
těchto	tento	k3xDgFnPc6
situacích	situace	k1gFnPc6
může	moct	k5eAaImIp3nS
technologie	technologie	k1gFnSc1
NFC	NFC	kA
zjednodušit	zjednodušit	k5eAaPmF
sdílení	sdílení	k1gNnSc4
kontaktů	kontakt	k1gInPc2
<g/>
,	,	kIx,
fotografií	fotografia	k1gFnPc2
<g/>
,	,	kIx,
videí	video	k1gNnPc2
nebo	nebo	k8xC
souborů	soubor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Díky	díky	k7c3
velmi	velmi	k6eAd1
jednoduché	jednoduchý	k2eAgFnSc3d1
a	a	k8xC
rychlé	rychlý	k2eAgFnSc3d1
konfiguraci	konfigurace	k1gFnSc3
NFC	NFC	kA
<g/>
,	,	kIx,
byť	byť	k8xS
o	o	k7c6
nízkých	nízký	k2eAgFnPc6d1
přenosových	přenosový	k2eAgFnPc6d1
rychlostech	rychlost	k1gFnPc6
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
se	se	k3xPyFc4
využít	využít	k5eAaPmF
k	k	k7c3
navázání	navázání	k1gNnSc3
a	a	k8xC
konfiguraci	konfigurace	k1gFnSc3
ostatních	ostatní	k2eAgMnPc2d1
<g/>
,	,	kIx,
složitějších	složitý	k2eAgInPc2d2
bezdrátových	bezdrátový	k2eAgInPc2d1
připojení	připojení	k1gNnSc4
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
párování	párování	k1gNnSc4
zařízení	zařízení	k1gNnSc2
pro	pro	k7c4
připojení	připojení	k1gNnSc4
Bluetooth	Bluetootha	k1gFnPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
pro	pro	k7c4
párování	párování	k1gNnSc4
užije	užít	k5eAaPmIp3nS
technologie	technologie	k1gFnSc1
NFC	NFC	kA
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
pro	pro	k7c4
přenos	přenos	k1gInSc4
dat	datum	k1gNnPc2
bude	být	k5eAaImBp3nS
následně	následně	k6eAd1
použita	použit	k2eAgFnSc1d1
technologie	technologie	k1gFnSc1
Bluetooth	Bluetootha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobně	podobně	k6eAd1
také	také	k9
konfigurace	konfigurace	k1gFnSc1
připojení	připojení	k1gNnSc2
Wi-Fi	Wi-F	k1gFnSc2
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
pro	pro	k7c4
tyto	tento	k3xDgInPc4
případy	případ	k1gInPc4
zde	zde	k6eAd1
existuje	existovat	k5eAaImIp3nS
technologie	technologie	k1gFnSc1
Wi-Fi	Wi-F	k1gInSc3
Protected	Protected	k1gMnSc1
Setup	Setup	k1gMnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
umožňuje	umožňovat	k5eAaImIp3nS
také	také	k9
velmi	velmi	k6eAd1
jednoduchou	jednoduchý	k2eAgFnSc4d1
konfiguraci	konfigurace	k1gFnSc4
Wi-Fi	Wi-Fi	k1gNnSc2
připojení	připojení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
NFC	NFC	kA
Forum	forum	k1gNnSc1
má	mít	k5eAaImIp3nS
zájem	zájem	k1gInSc4
o	o	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
potenciální	potenciální	k2eAgMnSc1d1
NFC	NFC	kA
zařízení	zařízení	k1gNnPc2
užívala	užívat	k5eAaImAgFnS
jako	jako	k9
elektronické	elektronický	k2eAgFnPc4d1
identifikační	identifikační	k2eAgFnPc4d1
karty	karta	k1gFnPc4
a	a	k8xC
klíčenky	klíčenka	k1gFnPc4
<g/>
,	,	kIx,
běžně	běžně	k6eAd1
nazývané	nazývaný	k2eAgNnSc4d1
„	„	k?
<g/>
čip	čip	k1gInSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
dosud	dosud	k6eAd1
používané	používaný	k2eAgInPc1d1
pro	pro	k7c4
kontrolu	kontrola	k1gFnSc4
vstupu	vstup	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
NFC	NFC	kA
podporuje	podporovat	k5eAaImIp3nS
šifrování	šifrování	k1gNnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
její	její	k3xOp3gNnSc1
použití	použití	k1gNnSc1
výhodnější	výhodný	k2eAgNnSc1d2
než	než	k8xS
méně	málo	k6eAd2
zabezpečené	zabezpečený	k2eAgInPc4d1
systémy	systém	k1gInPc4
RFID	RFID	kA
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Počátky	počátek	k1gInPc1
technologie	technologie	k1gFnSc2
NFC	NFC	kA
se	se	k3xPyFc4
datují	datovat	k5eAaImIp3nP
do	do	k7c2
doby	doba	k1gFnSc2
vzniku	vznik	k1gInSc2
technologie	technologie	k1gFnSc2
RFID	RFID	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
RFID	RFID	kA
umožňuje	umožňovat	k5eAaImIp3nS
čtečce	čtečka	k1gFnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
vysílala	vysílat	k5eAaImAgFnS
rádiové	rádiový	k2eAgFnPc4d1
vlny	vlna	k1gFnPc4
k	k	k7c3
pasivnímu	pasivní	k2eAgInSc3d1
<g/>
,	,	kIx,
elektronickému	elektronický	k2eAgInSc3d1
tagu	tag	k1gInSc3
<g/>
,	,	kIx,
pro	pro	k7c4
identifikaci	identifikace	k1gFnSc4
<g/>
,	,	kIx,
autentizaci	autentizace	k1gFnSc4
a	a	k8xC
sledování	sledování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
1983	#num#	k4
–	–	k?
první	první	k4xOgInSc4
patent	patent	k1gInSc4
byl	být	k5eAaImAgMnS
asociován	asociován	k2eAgMnSc1d1
se	s	k7c7
zkratkou	zkratka	k1gFnSc7
RFID	RFID	kA
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
přidělen	přidělit	k5eAaPmNgInS
Charlesu	Charlesu	k?
Waltonovi	Walton	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s>
2004	#num#	k4
–	–	k?
firmy	firma	k1gFnSc2
Nokia	Nokia	kA
<g/>
,	,	kIx,
Philips	Philips	kA
a	a	k8xC
Sony	Sony	kA
založily	založit	k5eAaPmAgFnP
neziskovou	ziskový	k2eNgFnSc4d1
organizaci	organizace	k1gFnSc4
NFC	NFC	kA
Forum	forum	k1gNnSc1
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2006	#num#	k4
–	–	k?
Byly	být	k5eAaImAgFnP
vytvořeny	vytvořen	k2eAgFnPc1d1
počáteční	počáteční	k2eAgFnPc1d1
specifikace	specifikace	k1gFnPc1
pro	pro	k7c4
NFC	NFC	kA
tagy	tag	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2006	#num#	k4
–	–	k?
Byly	být	k5eAaImAgFnP
vytvořeny	vytvořen	k2eAgFnPc4d1
specifikace	specifikace	k1gFnPc4
k	k	k7c3
záznamům	záznam	k1gInPc3
„	„	k?
<g/>
SmartPoster	SmartPoster	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2006	#num#	k4
–	–	k?
Prvním	první	k4xOgInSc7
telefonem	telefon	k1gInSc7
podporujícím	podporující	k2eAgInSc7d1
NFC	NFC	kA
byla	být	k5eAaImAgFnS
Nokia	Nokia	kA
6131	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2009	#num#	k4
–	–	k?
V	v	k7c6
lednu	leden	k1gInSc6
roku	rok	k1gInSc2
2009	#num#	k4
byly	být	k5eAaImAgFnP
NFC	NFC	kA
fórem	fór	k1gInSc7
vytvořeny	vytvořen	k2eAgInPc4d1
standardy	standard	k1gInPc4
pro	pro	k7c4
přenos	přenos	k1gInSc4
kontaktů	kontakt	k1gInPc2
<g/>
,	,	kIx,
URL	URL	kA
<g/>
,	,	kIx,
iniciaci	iniciace	k1gFnSc4
Bluetooth	Bluetootha	k1gFnPc2
a	a	k8xC
další	další	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2010	#num#	k4
–	–	k?
Samsung	Samsung	kA
Nexus	nexus	k1gInSc1
S	s	k7c7
<g/>
:	:	kIx,
prezentován	prezentovat	k5eAaBmNgInS
první	první	k4xOgInSc1
Android	android	k1gInSc1
telefon	telefon	k1gInSc1
podporující	podporující	k2eAgFnSc2d1
NFC	NFC	kA
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2011	#num#	k4
–	–	k?
Relace	relace	k1gFnSc2
„	„	k?
<g/>
How	How	k1gFnSc2
to	ten	k3xDgNnSc1
NFC	NFC	kA
<g/>
“	“	k?
na	na	k7c4
Google	Google	k1gNnSc4
I	I	kA
<g/>
/	/	kIx~
<g/>
O	O	kA
demonstruje	demonstrovat	k5eAaBmIp3nS
NFC	NFC	kA
k	k	k7c3
zahajování	zahajování	k1gNnSc3
her	hra	k1gFnPc2
a	a	k8xC
sdílení	sdílení	k1gNnSc4
kontaktů	kontakt	k1gInPc2
<g/>
,	,	kIx,
URL	URL	kA
<g/>
,	,	kIx,
aplikací	aplikace	k1gFnPc2
<g/>
,	,	kIx,
videí	video	k1gNnPc2
atd.	atd.	kA
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2011	#num#	k4
–	–	k?
Podpora	podpora	k1gFnSc1
NFC	NFC	kA
se	se	k3xPyFc4
stává	stávat	k5eAaImIp3nS
součástí	součást	k1gFnSc7
operačního	operační	k2eAgInSc2d1
systému	systém	k1gInSc2
Symbian	Symbiana	k1gFnPc2
ve	v	k7c6
verzi	verze	k1gFnSc6
Symbian	Symbiana	k1gFnPc2
Anna	Anna	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2011	#num#	k4
–	–	k?
RIM	RIM	kA
2011	#num#	k4
je	být	k5eAaImIp3nS
první	první	k4xOgFnSc7
firmou	firma	k1gFnSc7
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gNnSc4
zařízení	zařízení	k1gNnSc4
jsou	být	k5eAaImIp3nP
firmou	firma	k1gFnSc7
MasterCard	MasterCarda	k1gFnPc2
WorldWide	WorldWid	k1gInSc5
certifikovány	certifikovat	k5eAaImNgInP
pro	pro	k7c4
funkci	funkce	k1gFnSc4
MasterCard	MasterCarda	k1gFnPc2
Paypass	Paypassa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2012	#num#	k4
–	–	k?
V	v	k7c6
březnu	březen	k1gInSc6
roku	rok	k1gInSc2
2012	#num#	k4
řetězec	řetězec	k1gInSc1
britských	britský	k2eAgFnPc2d1
restaurací	restaurace	k1gFnPc2
EAT	EAT	kA
a	a	k8xC
firma	firma	k1gFnSc1
Everything	Everything	k1gInSc1
Everywhere	Everywher	k1gInSc5
(	(	kIx(
<g/>
partner	partner	k1gMnSc1
mobilního	mobilní	k2eAgMnSc2d1
operátora	operátor	k1gMnSc2
Orange	Orang	k1gInSc2
Mobile	mobile	k1gNnPc2
<g/>
)	)	kIx)
vytvořili	vytvořit	k5eAaPmAgMnP
první	první	k4xOgFnSc4
celonárodní	celonárodní	k2eAgFnSc4d1
kampaň	kampaň	k1gFnSc4
k	k	k7c3
NFC	NFC	kA
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
Británii	Británie	k1gFnSc6
pomocí	pomocí	k7c2
SmartPosterů	SmartPoster	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
2012	#num#	k4
–	–	k?
Sony	Sony	kA
uvádí	uvádět	k5eAaImIp3nS
„	„	k?
<g/>
Smart	Smart	k1gInSc1
Tags	Tags	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
používají	používat	k5eAaImIp3nP
technologii	technologie	k1gFnSc4
NFC	NFC	kA
pro	pro	k7c4
změnu	změna	k1gFnSc4
režimů	režim	k1gInPc2
a	a	k8xC
profilů	profil	k1gInPc2
na	na	k7c6
smartphonech	smartphon	k1gInPc6
Sony	Sony	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
Tato	tento	k3xDgFnSc1
technologie	technologie	k1gFnSc1
byla	být	k5eAaImAgFnS
již	již	k6eAd1
použita	použít	k5eAaPmNgFnS
u	u	k7c2
zařízení	zařízení	k1gNnSc2
Sony	Sony	kA
Xperia	Xperium	k1gNnSc2
P	P	kA
<g/>
,	,	kIx,
vydaném	vydaný	k2eAgNnSc6d1
v	v	k7c6
tomto	tento	k3xDgInSc6
roce	rok	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
2013	#num#	k4
-	-	kIx~
Samsung	Samsung	kA
a	a	k8xC
Visa	viso	k1gNnSc2
oznámili	oznámit	k5eAaPmAgMnP
partnerství	partnerství	k1gNnSc4
pro	pro	k7c4
rozvoj	rozvoj	k1gInSc4
plateb	platba	k1gFnPc2
mobilním	mobilní	k2eAgInSc7d1
telefonem	telefon	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
2013	#num#	k4
-	-	kIx~
Vědci	vědec	k1gMnPc1
z	z	k7c2
IBM	IBM	kA
v	v	k7c6
Curychu	Curych	k1gInSc6
ve	v	k7c6
snaze	snaha	k1gFnSc6
zamezit	zamezit	k5eAaPmF
podvodům	podvod	k1gInPc3
a	a	k8xC
narušení	narušení	k1gNnSc3
bezpečnosti	bezpečnost	k1gFnSc2
přichází	přicházet	k5eAaImIp3nS
s	s	k7c7
novou	nový	k2eAgFnSc7d1
technologií	technologie	k1gFnSc7
mobilní	mobilní	k2eAgFnSc2d1
autentizace	autentizace	k1gFnSc2
založené	založený	k2eAgFnSc2d1
na	na	k7c4
technologii	technologie	k1gFnSc4
NFC	NFC	kA
<g/>
.	.	kIx.
</s>
<s>
2014	#num#	k4
-	-	kIx~
Apple	Apple	kA
představil	představit	k5eAaPmAgInS
Apple	Apple	kA
Pay	Pay	k1gFnSc2
pro	pro	k7c4
placení	placení	k1gNnSc4
plateb	platba	k1gFnPc2
mobilním	mobilní	k2eAgInSc7d1
telefonem	telefon	k1gInSc7
na	na	k7c6
základě	základ	k1gInSc6
technologie	technologie	k1gFnSc2
NFC	NFC	kA
na	na	k7c6
jejich	jejich	k3xOp3gNnPc6
nových	nový	k2eAgNnPc6d1
zařízeních	zařízení	k1gNnPc6
iPhone	iPhon	k1gInSc5
6	#num#	k4
a	a	k8xC
iPhone	iPhon	k1gInSc5
6	#num#	k4
<g/>
+	+	kIx~
<g/>
.	.	kIx.
</s>
<s>
Technické	technický	k2eAgFnPc1d1
specifikace	specifikace	k1gFnPc1
</s>
<s>
NFC	NFC	kA
je	být	k5eAaImIp3nS
sada	sada	k1gFnSc1
bezdrátových	bezdrátový	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
krátkého	krátký	k2eAgInSc2d1
dosahu	dosah	k1gInSc2
<g/>
,	,	kIx,
pracující	pracující	k2eAgMnSc1d1
obvykle	obvykle	k6eAd1
na	na	k7c6
vzdálenostech	vzdálenost	k1gFnPc6
do	do	k7c2
4	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
nejnižší	nízký	k2eAgFnSc6d3
vrstvě	vrstva	k1gFnSc6
je	být	k5eAaImIp3nS
NFC	NFC	kA
definováno	definovat	k5eAaBmNgNnS
skupinou	skupina	k1gFnSc7
standardů	standard	k1gInPc2
bezkontaktních	bezkontaktní	k2eAgFnPc2d1
karet	kareta	k1gFnPc2
<g/>
,	,	kIx,
mezi	mezi	k7c4
které	který	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
patří	patřit	k5eAaImIp3nP
standardy	standard	k1gInPc1
bezkontaktních	bezkontaktní	k2eAgFnPc2d1
čipových	čipový	k2eAgFnPc2d1
karet	kareta	k1gFnPc2
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
14443	#num#	k4
<g/>
,	,	kIx,
JIS	JIS	kA
X	X	kA
6319	#num#	k4
pod	pod	k7c7
názvem	název	k1gInSc7
FeliCa	FeliC	k1gInSc2
a	a	k8xC
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
15693	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
dva	dva	k4xCgInPc1
zmíněné	zmíněný	k2eAgInPc1d1
standardy	standard	k1gInPc1
(	(	kIx(
<g/>
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
14443	#num#	k4
a	a	k8xC
JIS	JIS	kA
X	X	kA
6319	#num#	k4
<g/>
)	)	kIx)
operují	operovat	k5eAaImIp3nP
na	na	k7c6
frekvenci	frekvence	k1gFnSc6
13,56	13,56	k4
MHz	Mhz	kA
na	na	k7c4
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
rádiovém	rádiový	k2eAgNnSc6d1
rozhraní	rozhraní	k1gNnSc6
a	a	k8xC
s	s	k7c7
obvyklými	obvyklý	k2eAgFnPc7d1
přenosovými	přenosový	k2eAgFnPc7d1
rychlostmi	rychlost	k1gFnPc7
od	od	k7c2
106	#num#	k4
kbit	kbitum	k1gNnPc2
<g/>
/	/	kIx~
<g/>
s	s	k7c7
do	do	k7c2
424	#num#	k4
kbit	kbita	k1gFnPc2
<g/>
/	/	kIx~
<g/>
s.	s.	k?
Výjimkou	výjimka	k1gFnSc7
je	být	k5eAaImIp3nS
standard	standard	k1gInSc1
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
15693	#num#	k4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
použitelná	použitelný	k2eAgFnSc1d1
vzdálenost	vzdálenost	k1gFnSc1
dosahuje	dosahovat	k5eAaImIp3nS
oproti	oproti	k7c3
dvěma	dva	k4xCgInPc3
předchozím	předchozí	k2eAgInPc3d1
standardům	standard	k1gInPc3
vzdálenostem	vzdálenost	k1gFnPc3
výrazně	výrazně	k6eAd1
větším	veliký	k2eAgFnPc3d2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
až	až	k9
do	do	k7c2
vzdálenosti	vzdálenost	k1gFnSc2
1,5	1,5	k4
metru	metr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
touto	tento	k3xDgFnSc7
vzdáleností	vzdálenost	k1gFnSc7
však	však	k9
musíme	muset	k5eAaImIp1nP
počítat	počítat	k5eAaImF
s	s	k7c7
razantním	razantní	k2eAgInSc7d1
poklesem	pokles	k1gInSc7
přenosových	přenosový	k2eAgFnPc2d1
rychlostí	rychlost	k1gFnPc2
<g/>
,	,	kIx,
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c6
rychlosti	rychlost	k1gFnSc6
do	do	k7c2
26	#num#	k4
kbit	kbita	k1gFnPc2
<g/>
/	/	kIx~
<g/>
s.	s.	k?
Rozšíření	rozšíření	k1gNnSc1
standardu	standard	k1gInSc2
pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
technologie	technologie	k1gFnSc2
NFC	NFC	kA
je	být	k5eAaImIp3nS
specifikováno	specifikovat	k5eAaBmNgNnS
pomocí	pomocí	k7c2
standardů	standard	k1gInPc2
NFCIP	NFCIP	kA
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
rozšiřují	rozšiřovat	k5eAaImIp3nP
standard	standard	k1gInSc4
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
14443	#num#	k4
o	o	k7c4
další	další	k1gNnSc4
technické	technický	k2eAgFnSc2d1
specifikace	specifikace	k1gFnSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
definuje	definovat	k5eAaBmIp3nS
komunikaci	komunikace	k1gFnSc4
mezi	mezi	k7c7
dvěma	dva	k4xCgInPc7
NFC	NFC	kA
zařízeními	zařízení	k1gNnPc7
<g/>
,	,	kIx,
a	a	k8xC
je	být	k5eAaImIp3nS
znám	znám	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
18092	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Fyzická	fyzický	k2eAgFnSc1d1
a	a	k8xC
linková	linkový	k2eAgFnSc1d1
vrstva	vrstva	k1gFnSc1
NFC	NFC	kA
</s>
<s>
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
14443	#num#	k4
</s>
<s>
Bezkontaktní	bezkontaktní	k2eAgFnPc1d1
čipové	čipový	k2eAgFnPc1d1
karty	karta	k1gFnPc1
<g/>
,	,	kIx,
na	na	k7c6
nichž	jenž	k3xRgInPc6
je	být	k5eAaImIp3nS
založena	založen	k2eAgFnSc1d1
technologie	technologie	k1gFnSc1
NFC	NFC	kA
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
popsány	popsán	k2eAgFnPc1d1
standardem	standard	k1gInSc7
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
14443	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veškeré	veškerý	k3xTgInPc4
NFC	NFC	kA
transakce	transakce	k1gFnSc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
libovolné	libovolný	k2eAgInPc4d1
přenosy	přenos	k1gInPc4
dat	datum	k1gNnPc2
mezi	mezi	k7c7
účastníky	účastník	k1gMnPc7
přenosu	přenos	k1gInSc2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
realizovány	realizovat	k5eAaBmNgFnP
pomocí	pomocí	k7c2
přenosu	přenos	k1gInSc2
energie	energie	k1gFnSc2
skrze	skrze	k?
elektromagnetickou	elektromagnetický	k2eAgFnSc4d1
indukci	indukce	k1gFnSc4
mezi	mezi	k7c7
dvěma	dva	k4xCgFnPc7
smyčkovými	smyčkový	k2eAgFnPc7d1
anténami	anténa	k1gFnPc7
čipové	čipový	k2eAgFnSc2d1
karty	karta	k1gFnSc2
a	a	k8xC
NFC	NFC	kA
čtečky	čtečka	k1gFnSc2
(	(	kIx(
<g/>
mezi	mezi	k7c4
stěžejní	stěžejní	k2eAgFnPc4d1
komponenty	komponenta	k1gFnPc4
NFC	NFC	kA
čtečky	čtečka	k1gFnSc2
patří	patřit	k5eAaImIp3nS
mikrokontrolér	mikrokontrolér	k1gInSc1
a	a	k8xC
magnetická	magnetický	k2eAgFnSc1d1
smyčková	smyčkový	k2eAgFnSc1d1
anténa	anténa	k1gFnSc1
pracující	pracující	k2eAgFnSc1d1
na	na	k7c6
frekvenci	frekvence	k1gFnSc6
13,56	13,56	k4
MHz	Mhz	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
tohoto	tento	k3xDgInSc2
standardu	standard	k1gInSc2
se	se	k3xPyFc4
běžně	běžně	k6eAd1
užívají	užívat	k5eAaImIp3nP
pojmy	pojem	k1gInPc1
PICC	PICC	kA
(	(	kIx(
<g/>
Proximity	Proximita	k1gFnSc2
Integrated	Integrated	k1gMnSc1
Circuit	Circuit	k1gMnSc1
Card	Card	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
PCD	PCD	kA
(	(	kIx(
<g/>
Proximity	Proximita	k1gFnSc2
Coupling	Coupling	k1gInSc1
Device	device	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
představují	představovat	k5eAaImIp3nP
zmíněné	zmíněný	k2eAgFnPc4d1
komponenty	komponenta	k1gFnPc4
v	v	k7c6
rámci	rámec	k1gInSc6
NFC	NFC	kA
transakce	transakce	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
PICC	PICC	kA
ve	v	k7c6
formě	forma	k1gFnSc6
čipové	čipový	k2eAgFnSc2d1
karty	karta	k1gFnSc2
(	(	kIx(
<g/>
obsahující	obsahující	k2eAgInSc4d1
čip	čip	k1gInSc4
a	a	k8xC
smyčkovou	smyčkový	k2eAgFnSc4d1
anténu	anténa	k1gFnSc4
<g/>
)	)	kIx)
a	a	k8xC
PCD	PCD	kA
ve	v	k7c6
formě	forma	k1gFnSc6
NFC	NFC	kA
čtečky	čtečka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společně	společně	k6eAd1
se	s	k7c7
standardem	standard	k1gInSc7
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
14443	#num#	k4
existují	existovat	k5eAaImIp3nP
i	i	k9
některé	některý	k3yIgInPc4
vzájemně	vzájemně	k6eAd1
kompatibilní	kompatibilní	k2eAgInPc4d1
standardy	standard	k1gInPc4
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgMnPc2
nejznámější	známý	k2eAgMnPc4d3
jsou	být	k5eAaImIp3nP
MIFARE	MIFARE	kA
<g/>
,	,	kIx,
Calypso	Calypsa	k1gFnSc5
a	a	k8xC
FeliCa	FeliCum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Čipové	čipový	k2eAgFnPc1d1
karty	karta	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
pro	pro	k7c4
tyto	tento	k3xDgFnPc4
podmínky	podmínka	k1gFnPc4
používají	používat	k5eAaImIp3nP
<g/>
,	,	kIx,
mohou	moct	k5eAaImIp3nP
nabývat	nabývat	k5eAaImF
rozměrů	rozměr	k1gInPc2
definovanými	definovaný	k2eAgFnPc7d1
standardem	standard	k1gInSc7
pro	pro	k7c4
rozměry	rozměr	k1gInPc4
identifikačních	identifikační	k2eAgFnPc2d1
karet	kareta	k1gFnPc2
a	a	k8xC
jejich	jejich	k3xOp3gFnPc3
fyzickým	fyzický	k2eAgFnPc3d1
vlastnostem	vlastnost	k1gFnPc3
<g/>
,	,	kIx,
standardem	standard	k1gInSc7
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
7810	#num#	k4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
nejpoužívanějším	používaný	k2eAgInSc7d3
rozměrem	rozměr	k1gInSc7
je	být	k5eAaImIp3nS
formát	formát	k1gInSc1
ID-	ID-	k1gFnSc2
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozměry	rozměra	k1gFnPc1
tohoto	tento	k3xDgInSc2
formátu	formát	k1gInSc2
používá	používat	k5eAaImIp3nS
většina	většina	k1gFnSc1
identifikačních	identifikační	k2eAgFnPc2d1
a	a	k8xC
platebních	platební	k2eAgFnPc2d1
karet	kareta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Standard	standard	k1gInSc1
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
14443	#num#	k4
<g/>
,	,	kIx,
na	na	k7c6
němž	jenž	k3xRgInSc6
staví	stavit	k5eAaBmIp3nS,k5eAaPmIp3nS,k5eAaImIp3nS
bezkontaktní	bezkontaktní	k2eAgFnPc4d1
karty	karta	k1gFnPc4
<g/>
,	,	kIx,
definuje	definovat	k5eAaBmIp3nS
především	především	k6eAd1
základní	základní	k2eAgInPc4d1
elementy	element	k1gInPc4
v	v	k7c4
komunikaci	komunikace	k1gFnSc4
<g/>
,	,	kIx,
základní	základní	k2eAgInPc4d1
požadavky	požadavek	k1gInPc4
<g/>
,	,	kIx,
fyzické	fyzický	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
<g/>
,	,	kIx,
maximální	maximální	k2eAgInPc4d1
vysílací	vysílací	k2eAgInPc4d1
výkony	výkon	k1gInPc4
a	a	k8xC
protokoly	protokol	k1gInPc4
pro	pro	k7c4
iniciaci	iniciace	k1gFnSc4
komunikace	komunikace	k1gFnSc2
<g/>
,	,	kIx,
antikolizní	antikolizní	k2eAgInPc1d1
protokoly	protokol	k1gInPc1
a	a	k8xC
přenosové	přenosový	k2eAgInPc1d1
protokoly	protokol	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgInSc4d1
shrnutí	shrnutí	k1gNnSc1
tohoto	tento	k3xDgInSc2
standardu	standard	k1gInSc2
je	být	k5eAaImIp3nS
v	v	k7c6
níže	nízce	k6eAd2
uvedené	uvedený	k2eAgFnSc6d1
tabulce	tabulka	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
popisuje	popisovat	k5eAaImIp3nS
jednotlivé	jednotlivý	k2eAgFnPc4d1
části	část	k1gFnPc4
tohoto	tento	k3xDgInSc2
standardu	standard	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Část	část	k1gFnSc1
standardu	standard	k1gInSc2
</s>
<s>
Popis	popis	k1gInSc1
standardu	standard	k1gInSc2
</s>
<s>
Část	část	k1gFnSc1
1	#num#	k4
-	-	kIx~
Fyzikální	fyzikální	k2eAgFnSc2d1
charakteristiky	charakteristika	k1gFnSc2
bezkontaktních	bezkontaktní	k2eAgFnPc2d1
čipových	čipový	k2eAgFnPc2d1
karet	kareta	k1gFnPc2
</s>
<s>
Definuje	definovat	k5eAaBmIp3nS
fyzikální	fyzikální	k2eAgFnPc4d1
charakteristiky	charakteristika	k1gFnPc4
bezkontaktních	bezkontaktní	k2eAgFnPc2d1
čipových	čipový	k2eAgFnPc2d1
karet	kareta	k1gFnPc2
a	a	k8xC
jejich	jejich	k3xOp3gInPc1
požadavky	požadavek	k1gInPc1
na	na	k7c4
ně	on	k3xPp3gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Část	část	k1gFnSc1
2	#num#	k4
-	-	kIx~
Vysílací	vysílací	k2eAgInPc1d1
výkony	výkon	k1gInPc1
a	a	k8xC
signálové	signálový	k2eAgNnSc1d1
rozhraní	rozhraní	k1gNnSc1
</s>
<s>
Definuje	definovat	k5eAaBmIp3nS
vysílací	vysílací	k2eAgInPc4d1
výkony	výkon	k1gInPc4
<g/>
,	,	kIx,
způsob	způsob	k1gInSc4
napájení	napájení	k1gNnSc2
čipu	čip	k1gInSc2
z	z	k7c2
radiofrekvenčního	radiofrekvenční	k2eAgNnSc2d1
magnetického	magnetický	k2eAgNnSc2d1
pole	pole	k1gNnSc2
<g/>
,	,	kIx,
signalizační	signalizační	k2eAgNnSc1d1
rozhraní	rozhraní	k1gNnSc1
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gNnPc1
signalizační	signalizační	k2eAgNnPc1d1
schémata	schéma	k1gNnPc1
a	a	k8xC
typy	typa	k1gFnPc1
modulací	modulace	k1gFnSc7
k	k	k7c3
oběma	dva	k4xCgInPc3
typům	typ	k1gInPc3
PICC	PICC	kA
(	(	kIx(
<g/>
pro	pro	k7c4
oba	dva	k4xCgInPc4
směry	směr	k1gInPc4
komunikace	komunikace	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
jak	jak	k6eAd1
pro	pro	k7c4
aktivní	aktivní	k2eAgInSc4d1
a	a	k8xC
pasivní	pasivní	k2eAgInSc4d1
typ	typ	k1gInSc4
PICC	PICC	kA
<g/>
)	)	kIx)
</s>
<s>
Část	část	k1gFnSc1
3	#num#	k4
-	-	kIx~
Inicializační	inicializační	k2eAgInPc1d1
a	a	k8xC
antikolizní	antikolizní	k2eAgInPc1d1
protokoly	protokol	k1gInPc1
</s>
<s>
Definuje	definovat	k5eAaBmIp3nS
inicializační	inicializační	k2eAgMnSc1d1
a	a	k8xC
antikolizní	antikolizní	k2eAgInPc1d1
protokoly	protokol	k1gInPc1
pro	pro	k7c4
oba	dva	k4xCgInPc4
typy	typ	k1gInPc4
PICC	PICC	kA
<g/>
,	,	kIx,
společně	společně	k6eAd1
s	s	k7c7
antikolizními	antikolizní	k2eAgInPc7d1
příkazy	příkaz	k1gInPc7
<g/>
,	,	kIx,
odpověďmi	odpověď	k1gFnPc7
<g/>
,	,	kIx,
datovými	datový	k2eAgInPc7d1
rámci	rámec	k1gInPc7
a	a	k8xC
časováním	časování	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Část	část	k1gFnSc1
4	#num#	k4
-	-	kIx~
Protokoly	protokol	k1gInPc1
pro	pro	k7c4
přenos	přenos	k1gInSc4
</s>
<s>
Určuje	určovat	k5eAaImIp3nS
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
protokoly	protokol	k1gInPc1
jsou	být	k5eAaImIp3nP
určeny	určit	k5eAaPmNgInP
pro	pro	k7c4
vysokoúrovňový	vysokoúrovňový	k2eAgInSc4d1
přenos	přenos	k1gInSc4
dat	datum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgInPc4
tyto	tento	k3xDgInPc4
protokoly	protokol	k1gInPc4
v	v	k7c6
rámci	rámec	k1gInSc6
této	tento	k3xDgFnSc2
části	část	k1gFnSc2
standardu	standard	k1gInSc2
jsou	být	k5eAaImIp3nP
volitelné	volitelný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Standard	standard	k1gInSc1
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
14443	#num#	k4
definuje	definovat	k5eAaBmIp3nS
dva	dva	k4xCgInPc1
typy	typ	k1gInPc1
komunikačních	komunikační	k2eAgNnPc2d1
rozhraní	rozhraní	k1gNnPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
typ	typ	k1gInSc4
A	a	k9
a	a	k8xC
typ	typ	k1gInSc4
B.	B.	kA
Jako	jako	k8xC,k8xS
dodatek	dodatek	k1gInSc1
k	k	k7c3
nim	on	k3xPp3gMnPc3
existuje	existovat	k5eAaImIp3nS
rozhraní	rozhraní	k1gNnSc4
typu	typ	k1gInSc2
F	F	kA
z	z	k7c2
Japonského	japonský	k2eAgInSc2d1
standardu	standard	k1gInSc2
JIS	JIS	kA
X	X	kA
6319	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Rozhraní	rozhraní	k1gNnSc1
typu	typ	k1gInSc2
A	a	k9
používá	používat	k5eAaImIp3nS
ve	v	k7c6
směru	směr	k1gInSc6
čtečky	čtečka	k1gFnSc2
(	(	kIx(
<g/>
PCD	PCD	kA
<g/>
)	)	kIx)
ke	k	k7c3
kartě	karta	k1gFnSc3
(	(	kIx(
<g/>
PICC	PICC	kA
<g/>
)	)	kIx)
ASK	ASK	kA
modulaci	modulace	k1gFnSc4
se	s	k7c7
100	#num#	k4
<g/>
%	%	kIx~
hloubkou	hloubka	k1gFnSc7
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gNnPc1
data	datum	k1gNnPc1
jsou	být	k5eAaImIp3nP
zakódována	zakódován	k2eAgNnPc1d1
pomocí	pomocí	k7c2
modifikovaného	modifikovaný	k2eAgNnSc2d1
Millerova	Millerův	k2eAgNnSc2d1
kódování	kódování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
opačném	opačný	k2eAgInSc6d1
směru	směr	k1gInSc6
komunikace	komunikace	k1gFnSc2
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
OOK	OOK	kA
modulace	modulace	k1gFnSc1
s	s	k7c7
daty	datum	k1gNnPc7
zakódovanými	zakódovaný	k2eAgNnPc7d1
pomocí	pomocí	k7c2
Manchester	Manchester	k1gInSc1
kódování	kódování	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
typu	typ	k1gInSc6
rozhraní	rozhraní	k1gNnSc2
se	se	k3xPyFc4
pole	pole	k1gFnSc1
vypíná	vypínat	k5eAaImIp3nS
po	po	k7c4
dobu	doba	k1gFnSc4
krátkých	krátký	k2eAgInPc2d1
intervalů	interval	k1gInPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
čtečka	čtečka	k1gFnSc1
přenáší	přenášet	k5eAaImIp3nS
data	datum	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Rozhraní	rozhraní	k1gNnSc1
typu	typ	k1gInSc2
B	B	kA
používá	používat	k5eAaImIp3nS
ve	v	k7c6
směru	směr	k1gInSc6
čtečky	čtečka	k1gFnSc2
(	(	kIx(
<g/>
PCD	PCD	kA
<g/>
)	)	kIx)
ke	k	k7c3
kartě	karta	k1gFnSc3
(	(	kIx(
<g/>
PICC	PICC	kA
<g/>
)	)	kIx)
ASK	ASK	kA
modulaci	modulace	k1gFnSc4
s	s	k7c7
10	#num#	k4
<g/>
%	%	kIx~
hloubkou	hloubka	k1gFnSc7
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gNnPc1
data	datum	k1gNnPc1
jsou	být	k5eAaImIp3nP
zakódována	zakódovat	k5eAaPmNgNnP
pomocí	pomocí	k7c2
kódování	kódování	k1gNnPc2
NRZ-	NRZ-	k1gMnPc2
<g/>
L.	L.	kA
Komunikace	komunikace	k1gFnSc1
v	v	k7c6
opačném	opačný	k2eAgInSc6d1
směru	směr	k1gInSc6
používá	používat	k5eAaImIp3nS
BPSK	BPSK	kA
modulaci	modulace	k1gFnSc4
s	s	k7c7
daty	datum	k1gNnPc7
zakódovanými	zakódovaný	k2eAgNnPc7d1
pomocí	pomocí	k7c2
NRZ-	NRZ-	k1gMnPc2
<g/>
L.	L.	kA
</s>
<s>
NFCIP	NFCIP	kA
</s>
<s>
Rozšířením	rozšíření	k1gNnSc7
standardu	standard	k1gInSc2
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
14443	#num#	k4
je	být	k5eAaImIp3nS
standard	standard	k1gInSc4
NFCIP-	NFCIP-	k1gFnPc2
<g/>
1	#num#	k4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
z	z	k7c2
něj	on	k3xPp3gInSc2
vychází	vycházet	k5eAaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
rovněž	rovněž	k9
jej	on	k3xPp3gMnSc4
rozšiřuje	rozšiřovat	k5eAaImIp3nS
o	o	k7c4
komunikační	komunikační	k2eAgInPc4d1
režimy	režim	k1gInPc4
zařízení	zařízení	k1gNnPc2
<g/>
,	,	kIx,
transportní	transportní	k2eAgInPc1d1
protokoly	protokol	k1gInPc1
a	a	k8xC
protokoly	protokol	k1gInPc1
pro	pro	k7c4
přenos	přenos	k1gInSc4
dat	datum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komunikační	komunikační	k2eAgInPc1d1
režimy	režim	k1gInPc1
jsou	být	k5eAaImIp3nP
rozděleny	rozdělit	k5eAaPmNgInP
na	na	k7c4
pasivní	pasivní	k2eAgInSc4d1
a	a	k8xC
aktivní	aktivní	k2eAgInSc4d1
režim	režim	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
standard	standard	k1gInSc4
je	být	k5eAaImIp3nS
podmnožinou	podmnožina	k1gFnSc7
skupiny	skupina	k1gFnSc2
standardů	standard	k1gInPc2
NFCIP	NFCIP	kA
<g/>
,	,	kIx,
do	do	k7c2
kterého	který	k3yQgNnSc2,k3yIgNnSc2,k3yRgNnSc2
rovněž	rovněž	k9
patří	patřit	k5eAaImIp3nS
i	i	k9
standard	standard	k1gInSc1
NFCIP-	NFCIP-	k1gMnPc2
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
pasivním	pasivní	k2eAgInSc6d1
komunikačním	komunikační	k2eAgInSc6d1
režimu	režim	k1gInSc6
zařízení	zařízení	k1gNnPc2
iniciující	iniciující	k2eAgFnSc4d1
komunikaci	komunikace	k1gFnSc4
poskytuje	poskytovat	k5eAaImIp3nS
nosné	nosný	k2eAgNnSc1d1
pole	pole	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
je	být	k5eAaImIp3nS
cílovým	cílový	k2eAgNnSc7d1
zařízením	zařízení	k1gNnSc7
modulováno	modulován	k2eAgNnSc1d1
(	(	kIx(
<g/>
čímž	což	k3yRnSc7,k3yQnSc7
tímto	tento	k3xDgInSc7
odpovídá	odpovídat	k5eAaImIp3nS
iniciátorovi	iniciátor	k1gMnSc6
komunikace	komunikace	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
režimu	režim	k1gInSc6
může	moct	k5eAaImIp3nS
cílové	cílový	k2eAgNnSc4d1
zařízení	zařízení	k1gNnSc4
získat	získat	k5eAaPmF
energii	energie	k1gFnSc4
z	z	k7c2
elektromagnetického	elektromagnetický	k2eAgNnSc2d1
pole	pole	k1gNnSc2
iniciujícího	iniciující	k2eAgNnSc2d1
zařízení	zařízení	k1gNnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
pak	pak	k6eAd1
činí	činit	k5eAaImIp3nS
z	z	k7c2
cílového	cílový	k2eAgNnSc2d1
zařízení	zařízení	k1gNnSc2
<g/>
,	,	kIx,
tzv.	tzv.	kA
transpondér	transpondér	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
aktivním	aktivní	k2eAgInSc6d1
komunikačním	komunikační	k2eAgInSc6d1
režimu	režim	k1gInSc6
iniciátor	iniciátor	k1gInSc4
a	a	k8xC
cílové	cílový	k2eAgNnSc4d1
zařízení	zařízení	k1gNnSc4
komunikují	komunikovat	k5eAaImIp3nP
alternativně	alternativně	k6eAd1
mezi	mezi	k7c7
sebou	se	k3xPyFc7
generováním	generování	k1gNnSc7
vlastních	vlastní	k2eAgFnPc2d1
magnetických	magnetický	k2eAgFnPc2d1
polí	pole	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zařízení	zařízení	k1gNnSc1
deaktivuje	deaktivovat	k5eAaImIp3nS
své	svůj	k3xOyFgNnSc4
pole	pole	k1gNnSc4
zatímco	zatímco	k8xS
čeká	čekat	k5eAaImIp3nS
na	na	k7c4
data	datum	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
režimu	režim	k1gInSc6
jsou	být	k5eAaImIp3nP
obě	dva	k4xCgNnPc1
zařízení	zařízení	k1gNnPc1
napájena	napájet	k5eAaImNgFnS
vlastními	vlastní	k2eAgInPc7d1
zdroji	zdroj	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Komunikace	komunikace	k1gFnSc1
skrze	skrze	k?
radiofrekvenční	radiofrekvenční	k2eAgNnSc4d1
rozhraní	rozhraní	k1gNnSc4
v	v	k7c6
aktivním	aktivní	k2eAgInSc6d1
a	a	k8xC
pasivním	pasivní	k2eAgInSc6d1
režimu	režim	k1gInSc6
musí	muset	k5eAaImIp3nP
definovat	definovat	k5eAaBmF
modulační	modulační	k2eAgNnPc1d1
schémata	schéma	k1gNnPc1
a	a	k8xC
přenosové	přenosový	k2eAgFnPc1d1
rychlosti	rychlost	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dodatečně	dodatečně	k6eAd1
lze	lze	k6eAd1
také	také	k9
zahrnout	zahrnout	k5eAaPmF
počátek	počátek	k1gInSc4
komunikace	komunikace	k1gFnSc2
<g/>
,	,	kIx,
její	její	k3xOp3gInSc4
konec	konec	k1gInSc4
<g/>
,	,	kIx,
bitovou	bitový	k2eAgFnSc4d1
reprezentaci	reprezentace	k1gFnSc4
<g/>
,	,	kIx,
detekci	detekce	k1gFnSc3
rámců	rámec	k1gInPc2
a	a	k8xC
detekci	detekce	k1gFnSc3
chyb	chyba	k1gFnPc2
<g/>
,	,	kIx,
detekci	detekce	k1gFnSc4
zařízení	zařízení	k1gNnSc2
<g/>
,	,	kIx,
výběr	výběr	k1gInSc1
parametrů	parametr	k1gInPc2
a	a	k8xC
protokolů	protokol	k1gInPc2
<g/>
,	,	kIx,
výměnu	výměna	k1gFnSc4
dat	datum	k1gNnPc2
a	a	k8xC
deselekci	deselekce	k1gFnSc3
NFCIP-1	NFCIP-1	k1gFnPc2
zařízení	zařízení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Všechna	všechen	k3xTgNnPc1
zařízení	zařízení	k1gNnPc1
podporující	podporující	k2eAgFnSc1d1
NFCIP-1	NFCIP-1	k1gFnSc1
musí	muset	k5eAaImIp3nS
operovat	operovat	k5eAaImF
na	na	k7c6
přenosových	přenosový	k2eAgFnPc6d1
rychlostech	rychlost	k1gFnPc6
106	#num#	k4
<g/>
,	,	kIx,
212	#num#	k4
nebo	nebo	k8xC
424	#num#	k4
kbit	kbita	k1gFnPc2
<g/>
/	/	kIx~
<g/>
s.	s.	k?
Mezi	mezi	k7c7
těmito	tento	k3xDgFnPc7
rychlostmi	rychlost	k1gFnPc7
se	se	k3xPyFc4
zařízení	zařízení	k1gNnPc1
mohou	moct	k5eAaImIp3nP
přepínat	přepínat	k5eAaImF
či	či	k8xC
si	se	k3xPyFc3
stávající	stávající	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
ponechat	ponechat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
jedné	jeden	k4xCgFnSc2
transakce	transakce	k1gFnSc2
od	od	k7c2
iniciátora	iniciátor	k1gMnSc2
akce	akce	k1gFnSc2
k	k	k7c3
cíli	cíl	k1gInSc3
není	být	k5eNaImIp3nS
zapotřebí	zapotřebí	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
přenosová	přenosový	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
byla	být	k5eAaImAgFnS
fixní	fixní	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Změna	změna	k1gFnSc1
rychlosti	rychlost	k1gFnSc2
v	v	k7c6
rámci	rámec	k1gInSc6
jedné	jeden	k4xCgFnSc2
transakce	transakce	k1gFnSc2
je	být	k5eAaImIp3nS
jednoduše	jednoduše	k6eAd1
realizovatelná	realizovatelný	k2eAgFnSc1d1
pomocí	pomocí	k7c2
změny	změna	k1gFnSc2
parametru	parametr	k1gInSc2
procedury	procedura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
rychlosti	rychlost	k1gFnSc2
však	však	k9
nelze	lze	k6eNd1
měnit	měnit	k5eAaImF
režim	režim	k1gInSc4
v	v	k7c6
rámci	rámec	k1gInSc6
jedné	jeden	k4xCgFnSc2
transakce	transakce	k1gFnSc2
(	(	kIx(
<g/>
z	z	k7c2
aktivního	aktivní	k2eAgMnSc2d1
na	na	k7c4
pasivní	pasivní	k2eAgFnSc4d1
nebo	nebo	k8xC
naopak	naopak	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
NFCIP-2	NFCIP-2	k4
je	být	k5eAaImIp3nS
rozšířením	rozšíření	k1gNnSc7
standardu	standard	k1gInSc2
NFCIP-	NFCIP-	k1gFnSc2
<g/>
1	#num#	k4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
specifikován	specifikovat	k5eAaBmNgInS
ve	v	k7c6
standardech	standard	k1gInPc6
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
21481	#num#	k4
<g/>
,	,	kIx,
ECMA	ECMA	kA
352	#num#	k4
a	a	k8xC
ETSI	ETSI	kA
TS	ts	k0
102	#num#	k4
312	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
standard	standard	k1gInSc4
specifikuje	specifikovat	k5eAaBmIp3nS
mechanismy	mechanismus	k1gInPc4
výběru	výběr	k1gInSc2
správného	správný	k2eAgInSc2d1
komunikačního	komunikační	k2eAgInSc2d1
režimu	režim	k1gInSc2
a	a	k8xC
je	být	k5eAaImIp3nS
navržen	navrhnout	k5eAaPmNgInS
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
nenarušoval	narušovat	k5eNaImAgMnS
jakoukoliv	jakýkoliv	k3yIgFnSc4
implementaci	implementace	k1gFnSc4
komunikace	komunikace	k1gFnSc2
na	na	k7c4
frekvenci	frekvence	k1gFnSc4
13,56	13,56	k4
MHz	Mhz	kA
<g/>
,	,	kIx,
tj.	tj.	kA
je	být	k5eAaImIp3nS
navržen	navrhnout	k5eAaPmNgInS
s	s	k7c7
ohledem	ohled	k1gInSc7
na	na	k7c4
existující	existující	k2eAgFnPc4d1
bezkontaktní	bezkontaktní	k2eAgFnPc4d1
implementace	implementace	k1gFnPc4
postavené	postavený	k2eAgFnPc4d1
na	na	k7c6
RFID	RFID	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zařízení	zařízení	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
implementuje	implementovat	k5eAaImIp3nS
tento	tento	k3xDgInSc1
standard	standard	k1gInSc1
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nS
rovněž	rovněž	k9
implementovat	implementovat	k5eAaImF
funkce	funkce	k1gFnPc4
zařízení	zařízení	k1gNnPc2
definovaných	definovaný	k2eAgNnPc2d1
ve	v	k7c6
všech	všecek	k3xTgInPc6
zmíněných	zmíněný	k2eAgInPc6d1
standardech	standard	k1gInPc6
-	-	kIx~
a	a	k8xC
to	ten	k3xDgNnSc1
funkce	funkce	k1gFnSc1
všech	všecek	k3xTgInPc2
druhů	druh	k1gInPc2
zařízení	zařízení	k1gNnSc2
blízkých	blízký	k2eAgFnPc2d1
a	a	k8xC
z	z	k7c2
blízkého	blízký	k2eAgNnSc2d1
okolí	okolí	k1gNnSc2
(	(	kIx(
<g/>
standardy	standard	k1gInPc7
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
14443	#num#	k4
a	a	k8xC
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
15936	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgNnSc7
se	se	k3xPyFc4
zaručí	zaručit	k5eAaPmIp3nS
zpětná	zpětný	k2eAgFnSc1d1
kompatibilita	kompatibilita	k1gFnSc1
všech	všecek	k3xTgNnPc2
zařízení	zařízení	k1gNnPc2
s	s	k7c7
již	již	k6eAd1
existujícími	existující	k2eAgInPc7d1
bezkontaktními	bezkontaktní	k2eAgInPc7d1
systémy	systém	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Klasifikace	klasifikace	k1gFnSc1
zařízení	zařízení	k1gNnSc2
</s>
<s>
Vzhledem	vzhledem	k7c3
k	k	k7c3
široké	široký	k2eAgFnSc3d1
škále	škála	k1gFnSc3
standardů	standard	k1gInPc2
definujících	definující	k2eAgInPc2d1
technologii	technologie	k1gFnSc4
NFC	NFC	kA
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
rozlišit	rozlišit	k5eAaPmF
zařízení	zařízení	k1gNnSc4
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
klíčových	klíčový	k2eAgInPc6d1
parametrech	parametr	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Prvním	první	k4xOgInSc7
parametrem	parametr	k1gInSc7
<g/>
,	,	kIx,
na	na	k7c6
jehož	jehož	k3xOyRp3gInSc6
základě	základ	k1gInSc6
dělíme	dělit	k5eAaImIp1nP
typy	typ	k1gInPc1
zařízení	zařízení	k1gNnSc2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
napájecí	napájecí	k2eAgInPc4d1
požadavky	požadavek	k1gInPc4
<g/>
:	:	kIx,
</s>
<s>
Zařízení	zařízení	k1gNnSc1
s	s	k7c7
vlastním	vlastní	k2eAgNnSc7d1
napájením	napájení	k1gNnSc7
označujeme	označovat	k5eAaImIp1nP
jako	jako	k9
aktivní	aktivní	k2eAgNnPc4d1
zařízení	zařízení	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Zařízení	zařízení	k1gNnSc4
bez	bez	k7c2
vlastního	vlastní	k2eAgNnSc2d1
napájení	napájení	k1gNnSc2
označujeme	označovat	k5eAaImIp1nP
jako	jako	k9
pasivní	pasivní	k2eAgNnPc4d1
zařízení	zařízení	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgNnPc1
zařízení	zařízení	k1gNnPc4
jsou	být	k5eAaImIp3nP
v	v	k7c6
komunikaci	komunikace	k1gFnSc6
napájena	napájet	k5eAaImNgFnS
aktivním	aktivní	k2eAgNnSc7d1
zařízením	zařízení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Druhým	druhý	k4xOgInSc7
parametrem	parametr	k1gInSc7
<g/>
,	,	kIx,
na	na	k7c6
jehož	jehož	k3xOyRp3gInSc6
základě	základ	k1gInSc6
dělíme	dělit	k5eAaImIp1nP
typy	typ	k1gInPc1
zařízení	zařízení	k1gNnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
rozlišení	rozlišení	k1gNnSc1
účastníků	účastník	k1gMnPc2
komunikace	komunikace	k1gFnSc2
na	na	k7c6
<g/>
:	:	kIx,
</s>
<s>
iniciátora	iniciátor	k1gMnSc4
komunikace	komunikace	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
potřebuje	potřebovat	k5eAaImIp3nS
vlastní	vlastní	k2eAgNnPc4d1
napájení	napájení	k1gNnPc4
pro	pro	k7c4
iniciaci	iniciace	k1gFnSc4
komunikace	komunikace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pasivní	pasivní	k2eAgInSc4d1
zařízení	zařízení	k1gNnSc1
se	se	k3xPyFc4
nemůže	moct	k5eNaImIp3nS
stát	stát	k5eAaPmF,k5eAaImF
iniciátorem	iniciátor	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
cíl	cíl	k1gInSc1
komunikace	komunikace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgNnPc1
zařízení	zařízení	k1gNnPc4
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
pasivní	pasivní	k2eAgMnPc1d1
i	i	k8xC
aktivní	aktivní	k2eAgMnPc1d1
(	(	kIx(
<g/>
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
použitém	použitý	k2eAgInSc6d1
režimu	režim	k1gInSc6
přenosu	přenos	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Režimy	režim	k1gInPc1
přenosu	přenos	k1gInSc2
</s>
<s>
Reader	Reader	k1gInSc1
<g/>
/	/	kIx~
<g/>
Writer	Writer	k1gInSc1
režim	režim	k1gInSc1
</s>
<s>
Specifickými	specifický	k2eAgInPc7d1
režimy	režim	k1gInPc7
pro	pro	k7c4
čtení	čtení	k1gNnSc4
či	či	k8xC
zápis	zápis	k1gInSc4
dat	datum	k1gNnPc2
NFC	NFC	kA
čteček	čtečka	k1gFnPc2
z	z	k7c2
tagů	tag	k1gInPc2
jsou	být	k5eAaImIp3nP
režimy	režim	k1gInPc1
reader	readra	k1gFnPc2
<g/>
/	/	kIx~
<g/>
writer	writra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc1
režimy	režim	k1gInPc1
vyhovují	vyhovovat	k5eAaImIp3nP
rádiovému	rádiový	k2eAgNnSc3d1
rozhraní	rozhraní	k1gNnSc4
standardů	standard	k1gInPc2
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
14443	#num#	k4
typu	typ	k1gInSc2
A	A	kA
<g/>
,	,	kIx,
B	B	kA
a	a	k8xC
schémat	schéma	k1gNnPc2
FeliCa	FeliCum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
režimu	režim	k1gInSc6
není	být	k5eNaImIp3nS
vyžadována	vyžadován	k2eAgFnSc1d1
vysoká	vysoký	k2eAgFnSc1d1
bezpečnost	bezpečnost	k1gFnSc1
vzhledem	vzhled	k1gInSc7
k	k	k7c3
povaze	povaha	k1gFnSc3
komunikace	komunikace	k1gFnSc2
<g/>
,	,	kIx,
tj.	tj.	kA
není	být	k5eNaImIp3nS
zde	zde	k6eAd1
zapotřebí	zapotřebí	k6eAd1
mít	mít	k5eAaImF
bezpečné	bezpečný	k2eAgNnSc4d1
<g/>
,	,	kIx,
zašifrované	zašifrovaný	k2eAgNnSc4d1
úložiště	úložiště	k1gNnSc4
dat	datum	k1gNnPc2
<g/>
,	,	kIx,
tzv.	tzv.	kA
Secure	Secur	k1gMnSc5
element	element	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celý	celý	k2eAgInSc1d1
proces	proces	k1gInSc1
komunikace	komunikace	k1gFnSc2
spočívá	spočívat	k5eAaImIp3nS
pouze	pouze	k6eAd1
v	v	k7c6
zápisu	zápis	k1gInSc6
nebo	nebo	k8xC
čtení	čtení	k1gNnSc6
dat	datum	k1gNnPc2
z	z	k7c2
<g/>
/	/	kIx~
<g/>
do	do	k7c2
pasivního	pasivní	k2eAgInSc2d1
čipu	čip	k1gInSc2
<g/>
,	,	kIx,
tzv.	tzv.	kA
NFC	NFC	kA
tagu	tag	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
NFC	NFC	kA
tag	tag	k1gInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
obou	dva	k4xCgInPc6
případech	případ	k1gInPc6
napájen	napájen	k2eAgInSc1d1
elektromagnetickým	elektromagnetický	k2eAgNnSc7d1
polem	pole	k1gNnSc7
iniciátora	iniciátor	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maximální	maximální	k2eAgFnSc1d1
přenosová	přenosový	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
v	v	k7c6
režimu	režim	k1gInSc6
pro	pro	k7c4
zápis	zápis	k1gInSc4
je	být	k5eAaImIp3nS
106	#num#	k4
kbit	kbita	k1gFnPc2
<g/>
/	/	kIx~
<g/>
s.	s.	k?
</s>
<s>
Příkazy	příkaz	k1gInPc1
a	a	k8xC
instrukce	instrukce	k1gFnSc1
k	k	k7c3
řízení	řízení	k1gNnSc3
tagů	tag	k1gInPc2
NFC	NFC	kA
zařízeními	zařízení	k1gNnPc7
jsou	být	k5eAaImIp3nP
realizovány	realizovat	k5eAaBmNgInP
pomocí	pomocí	k7c2
datového	datový	k2eAgInSc2d1
formátu	formát	k1gInSc2
NDEF	NDEF	kA
a	a	k8xC
parametrů	parametr	k1gInPc2
RTD	RTD	kA
k	k	k7c3
definici	definice	k1gFnSc3
obsahu	obsah	k1gInSc2
NDEF	NDEF	kA
záznamů	záznam	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Použití	použití	k1gNnSc3
datového	datový	k2eAgInSc2d1
formátu	formát	k1gInSc2
NDEF	NDEF	kA
však	však	k9
není	být	k5eNaImIp3nS
pro	pro	k7c4
aplikace	aplikace	k1gFnPc4
vyžadováno	vyžadován	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Peer-to-peer	Peer-to-peer	k1gInSc1
režim	režim	k1gInSc1
</s>
<s>
Rozhraní	rozhraní	k1gNnSc1
komunikačního	komunikační	k2eAgNnSc2d1
peer-to-peer	peer-to-peer	k1gInSc4
režimu	režim	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
umožňuje	umožňovat	k5eAaImIp3nS
obousměrnou	obousměrný	k2eAgFnSc4d1
komunikaci	komunikace	k1gFnSc4
mezi	mezi	k7c7
NFC	NFC	kA
zařízeními	zařízení	k1gNnPc7
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
definováno	definovat	k5eAaBmNgNnS
v	v	k7c6
rámci	rámec	k1gInSc6
standardu	standard	k1gInSc2
NFCIP-	NFCIP-	k1gFnSc2
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
režim	režim	k1gInSc1
je	být	k5eAaImIp3nS
určen	určit	k5eAaPmNgInS
především	především	k9
pro	pro	k7c4
vzájemnou	vzájemný	k2eAgFnSc4d1
výměnu	výměna	k1gFnSc4
dat	datum	k1gNnPc2
<g/>
,	,	kIx,
kontaktů	kontakt	k1gInPc2
či	či	k8xC
textových	textový	k2eAgFnPc2d1
zpráv	zpráva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
tohoto	tento	k3xDgInSc2
režimu	režim	k1gInSc2
se	se	k3xPyFc4
předpokládá	předpokládat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
veškerá	veškerý	k3xTgNnPc1
zařízení	zařízení	k1gNnPc1
budou	být	k5eAaImBp3nP
v	v	k7c6
aktivním	aktivní	k2eAgInSc6d1
režimu	režim	k1gInSc6
během	během	k7c2
komunikace	komunikace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maximální	maximální	k2eAgFnPc1d1
přenosové	přenosový	k2eAgFnPc1d1
rychlosti	rychlost	k1gFnPc1
dosahují	dosahovat	k5eAaImIp3nP
424	#num#	k4
kbit	kbitum	k1gNnPc2
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
,	,	kIx,
komunikace	komunikace	k1gFnPc4
mezi	mezi	k7c7
zařízeními	zařízení	k1gNnPc7
probíhá	probíhat	k5eAaImIp3nS
v	v	k7c6
half-duplexním	half-duplexní	k2eAgInSc6d1
kanálu	kanál	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Standard	standard	k1gInSc1
NFCIP-1	NFCIP-1	k1gFnSc2
poskytuje	poskytovat	k5eAaImIp3nS
na	na	k7c4
základní	základní	k2eAgFnPc4d1
funkce	funkce	k1gFnPc4
linkové	linkový	k2eAgFnSc2d1
vrstvy	vrstva	k1gFnSc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
opravy	oprava	k1gFnPc1
chyb	chyba	k1gFnPc2
<g/>
,	,	kIx,
potvrzování	potvrzování	k1gNnSc1
rámců	rámec	k1gInPc2
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gNnSc1
řazení	řazení	k1gNnSc1
a	a	k8xC
další	další	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
LLCP	LLCP	kA
protokol	protokol	k1gInSc1
rozšiřuje	rozšiřovat	k5eAaImIp3nS
základní	základní	k2eAgFnSc4d1
funkcionalitu	funkcionalita	k1gFnSc4
standardu	standard	k1gInSc2
NFCIP-1	NFCIP-1	k1gFnSc2
o	o	k7c4
další	další	k2eAgFnPc4d1
důležité	důležitý	k2eAgFnPc4d1
služby	služba	k1gFnPc4
-	-	kIx~
spojově	spojově	k6eAd1
orientovaný	orientovaný	k2eAgInSc1d1
transport	transport	k1gInSc1
rámců	rámec	k1gInPc2
<g/>
,	,	kIx,
nespojově	spojově	k6eNd1
orientovaný	orientovaný	k2eAgInSc1d1
transport	transport	k1gInSc1
(	(	kIx(
<g/>
nepotvrzovaný	potvrzovaný	k2eNgMnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
řízení	řízení	k1gNnSc1
stavu	stav	k1gInSc2
linky	linka	k1gFnSc2
<g/>
,	,	kIx,
asynchronní	asynchronní	k2eAgFnSc4d1
vyvažovanou	vyvažovaný	k2eAgFnSc4d1
komunikaci	komunikace	k1gFnSc4
a	a	k8xC
multiplexaci	multiplexace	k1gFnSc4
protokolů	protokol	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Card	Card	k1gInSc1
emulation	emulation	k1gInSc1
režim	režim	k1gInSc4
</s>
<s>
Režim	režim	k1gInSc1
card	card	k6eAd1
emulation	emulation	k1gInSc1
umožňuje	umožňovat	k5eAaImIp3nS
mobilním	mobilní	k2eAgInPc3d1
telefonům	telefon	k1gInPc3
(	(	kIx(
<g/>
nebo	nebo	k8xC
jiným	jiný	k2eAgMnSc7d1
NFC	NFC	kA
zařízením	zařízení	k1gNnPc3
<g/>
)	)	kIx)
chovat	chovat	k5eAaImF
se	se	k3xPyFc4
jako	jako	k9
NFC	NFC	kA
čipová	čipový	k2eAgFnSc1d1
karta	karta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mobilní	mobilní	k2eAgInSc4d1
telefon	telefon	k1gInSc4
(	(	kIx(
<g/>
nebo	nebo	k8xC
jiné	jiný	k2eAgNnSc1d1
NFC	NFC	kA
aktivní	aktivní	k2eAgNnSc1d1
zařízení	zařízení	k1gNnSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
v	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
chová	chovat	k5eAaImIp3nS
jako	jako	k9
pasivní	pasivní	k2eAgInSc4d1
NFC	NFC	kA
čip	čip	k1gInSc4
standardu	standard	k1gInSc2
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
14443	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jakmile	jakmile	k8xS
dojde	dojít	k5eAaPmIp3nS
ke	k	k7c3
kontaktu	kontakt	k1gInSc3
NFC	NFC	kA
čtečky	čtečka	k1gFnSc2
s	s	k7c7
tímto	tento	k3xDgInSc7
čipem	čip	k1gInSc7
v	v	k7c6
telefonu	telefon	k1gInSc6
<g/>
,	,	kIx,
tak	tak	k6eAd1
komunikaci	komunikace	k1gFnSc4
iniciuje	iniciovat	k5eAaBmIp3nS
NFC	NFC	kA
čtečka	čtečka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
běžný	běžný	k2eAgInSc1d1
případ	případ	k1gInSc1
užití	užití	k1gNnSc2
je	být	k5eAaImIp3nS
v	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
chování	chování	k1gNnSc2
telefonu	telefon	k1gInSc2
jako	jako	k8xC,k8xS
sms	sms	kA
jízdenka	jízdenka	k1gFnSc1
<g/>
,	,	kIx,
vstupenka	vstupenka	k1gFnSc1
nebo	nebo	k8xC
libovolná	libovolný	k2eAgFnSc1d1
forma	forma	k1gFnSc1
autentizačního	autentizační	k2eAgInSc2d1
faktoru	faktor	k1gInSc2
ve	v	k7c6
formě	forma	k1gFnSc6
pasivního	pasivní	k2eAgInSc2d1
čipu	čip	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
NFC	NFC	kA
tagy	tag	k1gInPc4
</s>
<s>
NFC	NFC	kA
tagy	tag	k1gInPc1
obsahují	obsahovat	k5eAaImIp3nP
data	datum	k1gNnPc4
a	a	k8xC
jsou	být	k5eAaImIp3nP
typicky	typicky	k6eAd1
pouze	pouze	k6eAd1
pro	pro	k7c4
čtení	čtení	k1gNnSc4
<g/>
,	,	kIx,
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
ale	ale	k9
i	i	k9
přepisovatelnými	přepisovatelný	k2eAgInPc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
výrobci	výrobce	k1gMnPc1
je	on	k3xPp3gMnPc4
mohou	moct	k5eAaImIp3nP
šifrovat	šifrovat	k5eAaBmF
<g/>
,	,	kIx,
nebo	nebo	k8xC
používat	používat	k5eAaImF
specifikace	specifikace	k1gFnPc4
poskytnuté	poskytnutý	k2eAgFnPc4d1
NFC	NFC	kA
fórem	fórum	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tagy	tag	k1gInPc7
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
bezpečným	bezpečný	k2eAgNnSc7d1
úložištěm	úložiště	k1gNnSc7
soukromých	soukromý	k2eAgNnPc2d1
dat	datum	k1gNnPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
informace	informace	k1gFnPc4
o	o	k7c6
debetních	debetní	k2eAgFnPc6d1
a	a	k8xC
kreditních	kreditní	k2eAgFnPc6d1
kartách	karta	k1gFnPc6
<g/>
,	,	kIx,
PIN	pin	k1gInSc4
kódy	kód	k1gInPc7
<g/>
,	,	kIx,
kontakty	kontakt	k1gInPc1
a	a	k8xC
další	další	k2eAgInPc1d1
typy	typ	k1gInPc1
důvěrných	důvěrný	k2eAgNnPc2d1
dat	datum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vhodným	vhodný	k2eAgNnSc7d1
použitím	použití	k1gNnSc7
NFC	NFC	kA
tagů	tag	k1gInPc2
je	být	k5eAaImIp3nS
také	také	k9
NFC	NFC	kA
smartposters	smartposters	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
NFC	NFC	kA
forum	forum	k1gNnSc1
definuje	definovat	k5eAaBmIp3nS
4	#num#	k4
typy	typa	k1gFnSc2
tagů	tag	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
poskytují	poskytovat	k5eAaImIp3nP
různé	různý	k2eAgFnPc4d1
přenosové	přenosový	k2eAgFnPc4d1
rychlosti	rychlost	k1gFnPc4
a	a	k8xC
schopnosti	schopnost	k1gFnPc4
ve	v	k7c6
vztahu	vztah	k1gInSc6
k	k	k7c3
jejich	jejich	k3xOp3gFnSc3
konfigurovatelnosti	konfigurovatelnost	k1gFnSc3
<g/>
,	,	kIx,
bezpečnosti	bezpečnost	k1gFnSc6
<g/>
,	,	kIx,
velikosti	velikost	k1gFnSc6
paměti	paměť	k1gFnSc6
a	a	k8xC
kvalitě	kvalita	k1gFnSc6
čipu	čip	k1gInSc2
proti	proti	k7c3
zápisům	zápis	k1gInPc3
<g/>
.	.	kIx.
</s>
<s>
Níže	nízce	k6eAd2
uvedená	uvedený	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
uvádí	uvádět	k5eAaImIp3nS
obecný	obecný	k2eAgInSc4d1
přehled	přehled	k1gInSc4
jednotlivých	jednotlivý	k2eAgInPc2d1
typů	typ	k1gInPc2
tagů	tag	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Typ	typ	k1gInSc1
1	#num#	k4
</s>
<s>
Typ	typ	k1gInSc1
2	#num#	k4
</s>
<s>
Typ	typ	k1gInSc1
3	#num#	k4
</s>
<s>
Typ	typ	k1gInSc1
4	#num#	k4
</s>
<s>
Založeno	založen	k2eAgNnSc1d1
na	na	k7c6
standardu	standard	k1gInSc6
</s>
<s>
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
14443	#num#	k4
Typ	typ	k1gInSc4
A	a	k9
</s>
<s>
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
14443	#num#	k4
Typ	typ	k1gInSc4
A	a	k9
</s>
<s>
FeliCa	FeliCa	k6eAd1
</s>
<s>
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
14443	#num#	k4
Typ	typ	k1gInSc4
A	A	kA
<g/>
,	,	kIx,
typ	typ	k1gInSc1
B	B	kA
</s>
<s>
Název	název	k1gInSc1
čipu	čip	k1gInSc2
</s>
<s>
Topaz	topaz	k1gInSc1
</s>
<s>
MIFARE	MIFARE	kA
</s>
<s>
FeliCa	FeliCa	k6eAd1
</s>
<s>
DESFire	DESFir	k1gMnSc5
<g/>
,	,	kIx,
SmartMX-JCOP	SmartMX-JCOP	k1gMnSc5
</s>
<s>
Velikost	velikost	k1gFnSc1
paměti	paměť	k1gFnSc2
</s>
<s>
do	do	k7c2
1	#num#	k4
kB	kb	kA
</s>
<s>
do	do	k7c2
2	#num#	k4
kB	kb	kA
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
do	do	k7c2
1	#num#	k4
MB	MB	kA
</s>
<s>
do	do	k7c2
64	#num#	k4
kB	kb	kA
</s>
<s>
Přenosová	přenosový	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
</s>
<s>
106	#num#	k4
kbit	kbit	k1gInSc1
<g/>
/	/	kIx~
<g/>
s	s	k7c7
</s>
<s>
106	#num#	k4
kbit	kbit	k1gInSc1
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
212	#num#	k4
kbit	kbit	k1gInSc1
<g/>
/	/	kIx~
<g/>
s	s	k7c7
</s>
<s>
424	#num#	k4
kbit	kbit	k1gInSc1
<g/>
/	/	kIx~
<g/>
s	s	k7c7
</s>
<s>
Zabezpečení	zabezpečení	k1gNnSc1
</s>
<s>
16	#num#	k4
nebo	nebo	k8xC
32	#num#	k4
<g/>
bitový	bitový	k2eAgInSc4d1
digitální	digitální	k2eAgInSc4d1
podpis	podpis	k1gInSc4
</s>
<s>
nezabezpečeno	zabezpečen	k2eNgNnSc1d1
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
16	#num#	k4
nebo	nebo	k8xC
32	#num#	k4
<g/>
bitový	bitový	k2eAgInSc4d1
digitální	digitální	k2eAgInSc4d1
podpis	podpis	k1gInSc4
</s>
<s>
volitelně	volitelně	k6eAd1
</s>
<s>
Cena	cena	k1gFnSc1
</s>
<s>
nízká	nízký	k2eAgFnSc1d1
</s>
<s>
nízká	nízký	k2eAgFnSc1d1
</s>
<s>
vysoká	vysoký	k2eAgFnSc1d1
</s>
<s>
průměrná	průměrný	k2eAgFnSc1d1
až	až	k8xS
vysoká	vysoký	k2eAgFnSc1d1
</s>
<s>
Poskytované	poskytovaný	k2eAgNnSc1d1
výrobci	výrobce	k1gMnPc7
</s>
<s>
Innovision	Innovision	k1gInSc1
Research	Research	k1gInSc1
and	and	k?
Technology	technolog	k1gMnPc4
</s>
<s>
Philips	Philips	kA
<g/>
/	/	kIx~
<g/>
NXP	NXP	kA
</s>
<s>
Sony	Sony	kA
</s>
<s>
různí	různý	k2eAgMnPc1d1
výrobci	výrobce	k1gMnPc1
</s>
<s>
Případy	případ	k1gInPc1
užití	užití	k1gNnSc2
</s>
<s>
Jednoúčelové	jednoúčelový	k2eAgInPc1d1
tagy	tag	k1gInPc1
</s>
<s>
Jednoúčelové	jednoúčelový	k2eAgInPc1d1
tagy	tag	k1gInPc1
</s>
<s>
Flexibilní	flexibilní	k2eAgInPc1d1
tagy	tag	k1gInPc1
s	s	k7c7
širokými	široký	k2eAgFnPc7d1
možnostmi	možnost	k1gFnPc7
užití	užití	k1gNnSc2
</s>
<s>
Flexibilní	flexibilní	k2eAgInPc1d1
tagy	tag	k1gInPc1
s	s	k7c7
širokými	široký	k2eAgFnPc7d1
možnostmi	možnost	k1gFnPc7
užití	užití	k1gNnSc2
</s>
<s>
Kapacity	kapacita	k1gFnPc1
bezkontaktních	bezkontaktní	k2eAgInPc2d1
čipů	čip	k1gInPc2
</s>
<s>
Paměťové	paměťový	k2eAgFnPc1d1
kapacity	kapacita	k1gFnPc1
běžně	běžně	k6eAd1
dostupných	dostupný	k2eAgInPc2d1
bezkontaktních	bezkontaktní	k2eAgInPc2d1
čipů	čip	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
MIFARE	MIFARE	kA
Ultralight	Ultralight	k1gMnSc1
-	-	kIx~
46	#num#	k4
bajtů	bajt	k1gInPc2
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
ICODE	ICODE	kA
SLI	SLI	kA
/	/	kIx~
ICODE	ICODE	kA
SLIX	SLIX	kA
-	-	kIx~
106	#num#	k4
bajtů	bajt	k1gInPc2
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
NTAG	NTAG	kA
<g/>
203	#num#	k4
-	-	kIx~
137	#num#	k4
bajtů	bajt	k1gInPc2
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
MIFARE	MIFARE	kA
Ultralight	Ultralighta	k1gFnPc2
C	C	kA
-	-	kIx~
137	#num#	k4
bajtů	bajt	k1gInPc2
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
ICODE	ICODE	kA
SLI-S	SLI-S	k1gMnSc1
/	/	kIx~
ICODE	ICODE	kA
SLIX-S	SLIX-S	k1gFnSc6
-	-	kIx~
154	#num#	k4
bajtů	bajt	k1gInPc2
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
MIFARE	MIFARE	kA
Classic	Classic	k1gMnSc1
1	#num#	k4
<g/>
k	k	k7c3
-	-	kIx~
716	#num#	k4
bajtů	bajt	k1gInPc2
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
MIFARE	MIFARE	kA
DESFire	DESFir	k1gInSc5
EV1	EV1	k1gFnSc6
2	#num#	k4
<g/>
k	k	k7c3
-	-	kIx~
2046	#num#	k4
bajtů	bajt	k1gInPc2
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
MIFARE	MIFARE	kA
Classic	Classic	k1gMnSc1
4	#num#	k4
<g/>
k	k	k7c3
-	-	kIx~
3356	#num#	k4
bajtů	bajt	k1gInPc2
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
MIFARE	MIFARE	kA
DESFire	DESFir	k1gInSc5
EV1	EV1	k1gFnSc6
4	#num#	k4
<g/>
k	k	k7c3
-	-	kIx~
4094	#num#	k4
bajtů	bajt	k1gInPc2
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
MIFARE	MIFARE	kA
DESFire	DESFir	k1gInSc5
EV1	EV1	k1gFnSc6
8	#num#	k4
<g/>
k	k	k7c3
-	-	kIx~
7678	#num#	k4
bajtů	bajt	k1gInPc2
<g/>
]	]	kIx)
</s>
<s>
Specifikace	specifikace	k1gFnSc1
NFC	NFC	kA
a	a	k8xC
Bluetooth	Bluetooth	k1gMnSc1
</s>
<s>
NFC	NFC	kA
</s>
<s>
Bluetooth	Bluetooth	k1gInSc1
V	v	k7c6
<g/>
2.1	2.1	k4
</s>
<s>
Bluetooth	Bluetooth	k1gInSc1
V	v	k7c6
<g/>
4.0	4.0	k4
(	(	kIx(
<g/>
nízká	nízký	k2eAgFnSc1d1
spotřeba	spotřeba	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Kompatibilita	kompatibilita	k1gFnSc1
s	s	k7c7
pasivním	pasivní	k2eAgNnSc7d1
RFID	RFID	kA
</s>
<s>
ano	ano	k9
(	(	kIx(
<g/>
ISO	ISO	kA
18000	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
ne	ne	k9
(	(	kIx(
<g/>
pouze	pouze	k6eAd1
aktivně	aktivně	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
ne	ne	k9
(	(	kIx(
<g/>
pouze	pouze	k6eAd1
aktivně	aktivně	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Tvůrce	tvůrce	k1gMnSc1
standardu	standard	k1gInSc2
</s>
<s>
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
</s>
<s>
Bluetooth	Bluetooth	k1gMnSc1
SIG	SIG	kA
</s>
<s>
Bluetooth	Bluetooth	k1gMnSc1
SIG	SIG	kA
</s>
<s>
Norma	Norma	k1gFnSc1
</s>
<s>
ISO	ISO	kA
13157	#num#	k4
</s>
<s>
IEEE	IEEE	kA
802.15	802.15	k4
<g/>
.1	.1	k4
</s>
<s>
IEEE	IEEE	kA
802.15	802.15	k4
<g/>
.1	.1	k4
</s>
<s>
Typ	typ	k1gInSc1
sítě	síť	k1gFnSc2
</s>
<s>
Point-to-point	Point-to-point	k1gMnSc1
P2P	P2P	k1gMnSc1
</s>
<s>
WPAN	WPAN	kA
</s>
<s>
WPAN	WPAN	kA
</s>
<s>
Kryptografie	kryptografie	k1gFnSc1
</s>
<s>
ne	ne	k9
s	s	k7c7
RFID	RFID	kA
</s>
<s>
možná	možná	k9
</s>
<s>
možná	možná	k9
</s>
<s>
Dosah	dosah	k1gInSc1
</s>
<s>
<	<	kIx(
0,2	0,2	k4
m	m	kA
</s>
<s>
~	~	kIx~
<g/>
10	#num#	k4
m	m	kA
(	(	kIx(
<g/>
třída	třída	k1gFnSc1
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
~	~	kIx~
<g/>
100	#num#	k4
m	m	kA
(	(	kIx(
<g/>
třída	třída	k1gFnSc1
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Frekvence	frekvence	k1gFnSc1
</s>
<s>
13,56	13,56	k4
MHz	Mhz	kA
</s>
<s>
2,4	2,4	k4
<g/>
-	-	kIx~
<g/>
2,5	2,5	k4
GHz	GHz	k1gFnPc2
</s>
<s>
2,4	2,4	k4
<g/>
-	-	kIx~
<g/>
2,5	2,5	k4
GHz	GHz	k1gFnPc2
</s>
<s>
Rychlost	rychlost	k1gFnSc1
přenosu	přenos	k1gInSc2
</s>
<s>
424	#num#	k4
kbit	kbit	k1gInSc1
<g/>
/	/	kIx~
<g/>
s	s	k7c7
</s>
<s>
2,1	2,1	k4
Mbit	Mbit	k1gInSc1
<g/>
/	/	kIx~
<g/>
s	s	k7c7
</s>
<s>
~	~	kIx~
<g/>
200	#num#	k4
kbit	kbitum	k1gNnPc2
<g/>
/	/	kIx~
<g/>
s	s	k7c7
</s>
<s>
Čas	čas	k1gInSc1
pro	pro	k7c4
sestavení	sestavení	k1gNnSc4
přenosu	přenos	k1gInSc2
</s>
<s>
<	<	kIx(
0,1	0,1	k4
s	s	k7c7
</s>
<s>
<	<	kIx(
6	#num#	k4
s	s	k7c7
</s>
<s>
<	<	kIx(
1	#num#	k4
s	s	k7c7
</s>
<s>
Spotřeba	spotřeba	k1gFnSc1
energie	energie	k1gFnSc2
</s>
<s>
<	<	kIx(
15	#num#	k4
mA	mA	k?
(	(	kIx(
<g/>
čtení	čtení	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
závislé	závislý	k2eAgNnSc1d1
na	na	k7c6
třídě	třída	k1gFnSc6
</s>
<s>
<	<	kIx(
15	#num#	k4
mA	mA	k?
(	(	kIx(
<g/>
střed	střed	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
NFC	NFC	kA
a	a	k8xC
Bluetooth	Bluetooth	k1gMnSc1
jsou	být	k5eAaImIp3nP
rádiové	rádiový	k2eAgFnPc4d1
technologie	technologie	k1gFnPc4
krátkého	krátký	k2eAgInSc2d1
dosahu	dosah	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
technických	technický	k2eAgInPc2d1
detailů	detail	k1gInPc2
uvedených	uvedený	k2eAgInPc2d1
níže	nízce	k6eAd2
NFC	NFC	kA
operuje	operovat	k5eAaImIp3nS
na	na	k7c6
nižších	nízký	k2eAgFnPc6d2
přenosových	přenosový	k2eAgFnPc6d1
rychlostech	rychlost	k1gFnPc6
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
potřebuje	potřebovat	k5eAaImIp3nS
ke	k	k7c3
svému	svůj	k3xOyFgInSc3
provozu	provoz	k1gInSc3
výrazně	výrazně	k6eAd1
méně	málo	k6eAd2
energie	energie	k1gFnSc2
a	a	k8xC
nepotřebuje	potřebovat	k5eNaImIp3nS
párování	párování	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Konfigurace	konfigurace	k1gFnSc1
NFC	NFC	kA
je	být	k5eAaImIp3nS
výrazně	výrazně	k6eAd1
rychlejší	rychlý	k2eAgFnSc1d2
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
se	s	k7c7
standardním	standardní	k2eAgNnSc7d1
Bluetooth	Bluetooth	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
nikoliv	nikoliv	k9
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
Bluetooth	Bluetooth	k1gMnSc1
low	low	k?
energy	energ	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Namísto	namísto	k7c2
manuální	manuální	k2eAgFnSc2d1
konfigurace	konfigurace	k1gFnSc2
pro	pro	k7c4
identifikace	identifikace	k1gFnPc4
zařízení	zařízení	k1gNnPc2
je	být	k5eAaImIp3nS
spojení	spojení	k1gNnSc1
mezi	mezi	k7c7
dvěma	dva	k4xCgNnPc7
zařízeními	zařízení	k1gNnPc7
provedeno	provést	k5eAaPmNgNnS
automaticky	automaticky	k6eAd1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
za	za	k7c4
méně	málo	k6eAd2
než	než	k8xS
1	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
sekundy	sekunda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maximální	maximální	k2eAgFnSc2d1
přenosové	přenosový	k2eAgFnSc2d1
rychlosti	rychlost	k1gFnSc2
NFC	NFC	kA
(	(	kIx(
<g/>
424	#num#	k4
kbit	kbitum	k1gNnPc2
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
nižší	nízký	k2eAgFnPc1d2
<g/>
,	,	kIx,
než	než	k8xS
u	u	k7c2
standardu	standard	k1gInSc2
Bluetooth	Bluetootha	k1gFnPc2
V	v	k7c6
<g/>
2.1	2.1	k4
(	(	kIx(
<g/>
2.1	2.1	k4
Mbit	Mbitum	k1gNnPc2
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maximální	maximální	k2eAgInSc4d1
dosah	dosah	k1gInSc4
menší	malý	k2eAgInSc4d2
než	než	k8xS
20	#num#	k4
cm	cm	kA
u	u	k7c2
této	tento	k3xDgFnSc2
technologie	technologie	k1gFnSc2
výrazně	výrazně	k6eAd1
redukuje	redukovat	k5eAaBmIp3nS
možnosti	možnost	k1gFnPc4
okolních	okolní	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
zachytávat	zachytávat	k5eAaImF
provoz	provoz	k1gInSc4
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
ve	v	k7c6
velmi	velmi	k6eAd1
blízkém	blízký	k2eAgInSc6d1
dosahu	dosah	k1gInSc6
zařízení	zařízení	k1gNnSc2
<g/>
,	,	kIx,
u	u	k7c2
nějž	jenž	k3xRgNnSc2
chcete	chtít	k5eAaImIp2nP
odchytávat	odchytávat	k5eAaImF
provoz	provoz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
Bluetooth	Bluetooth	k1gInSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
NFC	NFC	kA
kompatibilní	kompatibilní	k2eAgInSc1d1
s	s	k7c7
existující	existující	k2eAgFnSc7d1
pasivní	pasivní	k2eAgFnSc7d1
RFID	RFID	kA
infrastrukturou	infrastruktura	k1gFnSc7
(	(	kIx(
<g/>
13.56	13.56	k4
MHz	Mhz	kA
<g/>
,	,	kIx,
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
18000	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Požadavky	požadavek	k1gInPc1
na	na	k7c4
napájení	napájení	k1gNnSc4
jsou	být	k5eAaImIp3nP
výrazně	výrazně	k6eAd1
nižší	nízký	k2eAgFnPc1d2
<g/>
,	,	kIx,
nebo	nebo	k8xC
podobné	podobný	k2eAgNnSc1d1
s	s	k7c7
Bluetooth	Bluetootha	k1gFnPc2
V	v	k7c6
<g/>
4.0	4.0	k4
low	low	k?
energy	energ	k1gInPc1
protokolem	protokol	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
některých	některý	k3yIgInPc6
případech	případ	k1gInPc6
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
ale	ale	k9
požadavky	požadavek	k1gInPc4
na	na	k7c4
napájení	napájení	k1gNnSc4
zařízení	zařízení	k1gNnSc2
vyšší	vysoký	k2eAgNnSc1d2
<g/>
,	,	kIx,
než	než	k8xS
u	u	k7c2
Bluetooth	Bluetootha	k1gFnPc2
V	v	k7c4
<g/>
4.0	4.0	k4
Low	Low	k1gFnSc7
Energy	Energ	k1gInPc1
protokolu	protokol	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
v	v	k7c6
případech	případ	k1gInPc6
komunikace	komunikace	k1gFnSc2
s	s	k7c7
pasivním	pasivní	k2eAgNnSc7d1
<g/>
,	,	kIx,
nenapájeným	napájený	k2eNgNnSc7d1
zařízením	zařízení	k1gNnSc7
(	(	kIx(
<g/>
vypnutý	vypnutý	k2eAgInSc4d1
telefon	telefon	k1gInSc4
<g/>
,	,	kIx,
bezkontaktní	bezkontaktní	k2eAgFnPc1d1
čipové	čipový	k2eAgFnPc1d1
karty	karta	k1gFnPc1
<g/>
,	,	kIx,
smart	smart	k1gInSc1
postery	poster	k1gInPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
je	být	k5eAaImIp3nS
potřeba	potřeba	k6eAd1
napájet	napájet	k5eAaImF
ze	z	k7c2
zařízení	zařízení	k1gNnSc2
iniciujícího	iniciující	k2eAgNnSc2d1
komunikaci	komunikace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Normy	Norma	k1gFnPc1
NFC	NFC	kA
a	a	k8xC
jejich	jejich	k3xOp3gMnPc7
tvůrci	tvůrce	k1gMnPc7
</s>
<s>
Normy	Norma	k1gFnPc1
</s>
<s>
NFC	NFC	kA
bylo	být	k5eAaImAgNnS
schváleno	schválit	k5eAaPmNgNnS
jako	jako	k9
norma	norma	k1gFnSc1
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
8	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
roku	rok	k1gInSc2
2003	#num#	k4
a	a	k8xC
později	pozdě	k6eAd2
jako	jako	k9
norma	norma	k1gFnSc1
ECMA	ECMA	kA
<g/>
.	.	kIx.
</s>
<s>
NFC	NFC	kA
je	být	k5eAaImIp3nS
otevřená	otevřený	k2eAgFnSc1d1
platforma	platforma	k1gFnSc1
technologií	technologie	k1gFnPc2
standardizovaná	standardizovaný	k2eAgFnSc1d1
v	v	k7c6
ECMA-340	ECMA-340	k1gFnSc6
a	a	k8xC
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
18092	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
normy	norma	k1gFnPc1
specifikují	specifikovat	k5eAaBmIp3nP
modulační	modulační	k2eAgNnPc1d1
schémata	schéma	k1gNnPc1
<g/>
,	,	kIx,
kódování	kódování	k1gNnSc1
<g/>
,	,	kIx,
přenosové	přenosový	k2eAgFnPc1d1
rychlosti	rychlost	k1gFnPc1
<g/>
,	,	kIx,
formáty	formát	k1gInPc1
rámců	rámec	k1gInPc2
na	na	k7c6
rozhraní	rozhraní	k1gNnSc6
NFC	NFC	kA
zařízení	zařízení	k1gNnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
i	i	k9
inicializační	inicializační	k2eAgNnPc1d1
schémata	schéma	k1gNnPc1
a	a	k8xC
podmínky	podmínka	k1gFnPc1
vyžadované	vyžadovaný	k2eAgFnPc1d1
pro	pro	k7c4
řízení	řízení	k1gNnSc4
kolizí	kolize	k1gFnPc2
během	během	k7c2
inicializace	inicializace	k1gFnSc2
během	během	k7c2
obou	dva	k4xCgFnPc2
<g/>
,	,	kIx,
pasivních	pasivní	k2eAgInPc2d1
a	a	k8xC
aktivních	aktivní	k2eAgInPc2d1
režimů	režim	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimoto	mimoto	k6eAd1
také	také	k9
definuje	definovat	k5eAaBmIp3nS
transportní	transportní	k2eAgInSc4d1
protokol	protokol	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
aktivaci	aktivace	k1gFnSc4
protokolu	protokol	k1gInSc2
a	a	k8xC
metody	metoda	k1gFnSc2
pro	pro	k7c4
přenos	přenos	k1gInSc4
dat	datum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Rádiové	rádiový	k2eAgNnSc1d1
rozhraní	rozhraní	k1gNnSc1
pro	pro	k7c4
NFC	NFC	kA
je	být	k5eAaImIp3nS
definováno	definovat	k5eAaBmNgNnS
ve	v	k7c6
dvou	dva	k4xCgFnPc6
normách	norma	k1gFnPc6
<g/>
:	:	kIx,
</s>
<s>
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
18092	#num#	k4
/	/	kIx~
ECMA-	ECMA-	k1gFnSc1
<g/>
340	#num#	k4
<g/>
:	:	kIx,
Near	Near	k1gInSc1
Field	Field	k1gInSc1
Communication	Communication	k1gInSc1
Interface	interface	k1gInSc1
and	and	k?
Protocol-	Protocol-	k1gFnSc2
<g/>
1	#num#	k4
(	(	kIx(
<g/>
NFCIP-	NFCIP-	k1gFnSc1
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
21481	#num#	k4
/	/	kIx~
ECMA-	ECMA-	k1gFnSc1
<g/>
352	#num#	k4
<g/>
:	:	kIx,
Near	Near	k1gInSc1
Field	Field	k1gInSc1
Communication	Communication	k1gInSc1
Interface	interface	k1gInSc1
and	and	k?
Protocol-	Protocol-	k1gFnSc2
<g/>
2	#num#	k4
(	(	kIx(
<g/>
NFCIP-	NFCIP-	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Systém	systém	k1gInSc1
NFC	NFC	kA
obsahuje	obsahovat	k5eAaImIp3nS
řadu	řada	k1gFnSc4
stávajících	stávající	k2eAgFnPc2d1
norem	norma	k1gFnPc2
(	(	kIx(
<g/>
standardů	standard	k1gInPc2
<g/>
)	)	kIx)
včetně	včetně	k7c2
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
14443	#num#	k4
obou	dva	k4xCgInPc2
typů	typ	k1gInPc2
(	(	kIx(
<g/>
typ	typ	k1gInSc1
A	A	kA
a	a	k8xC
typ	typ	k1gInSc1
B	B	kA
<g/>
,	,	kIx,
a	a	k8xC
FeliCa	FeliCa	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Telefony	telefon	k1gInPc1
podporující	podporující	k2eAgInPc1d1
NFC	NFC	kA
pracují	pracovat	k5eAaImIp3nP
minimálně	minimálně	k6eAd1
s	s	k7c7
existujícími	existující	k2eAgFnPc7d1
čtečkami	čtečka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obzvláště	obzvláště	k6eAd1
v	v	k7c6
režimu	režim	k1gInSc6
"	"	kIx"
<g/>
card	card	k1gInSc1
emulation	emulation	k1gInSc1
<g/>
"	"	kIx"
by	by	kYmCp3nS
mělo	mít	k5eAaImAgNnS
NFC	NFC	kA
zařízení	zařízený	k2eAgMnPc1d1
vysílat	vysílat	k5eAaImF
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
alespoň	alespoň	k9
své	svůj	k3xOyFgNnSc1
unikátní	unikátní	k2eAgNnSc1d1
identifikační	identifikační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
do	do	k7c2
čtečky	čtečka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kromě	kromě	k7c2
toho	ten	k3xDgNnSc2
definovalo	definovat	k5eAaBmAgNnS
NFC	NFC	kA
Forum	forum	k1gNnSc4
datový	datový	k2eAgInSc1d1
formát	formát	k1gInSc1
pod	pod	k7c7
názvem	název	k1gInSc7
NFC	NFC	kA
Data	datum	k1gNnSc2
Exchange	Exchange	k1gNnSc1
Format	Format	k1gInSc1
(	(	kIx(
<g/>
NDEF	NDEF	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
použit	použít	k5eAaPmNgInS
k	k	k7c3
ukládání	ukládání	k1gNnSc3
a	a	k8xC
transportu	transport	k1gInSc3
různých	různý	k2eAgInPc2d1
typů	typ	k1gInPc2
objektů	objekt	k1gInPc2
<g/>
,	,	kIx,
od	od	k7c2
MIME-type	MIME-typ	k1gInSc5
objektů	objekt	k1gInPc2
po	po	k7c4
ultra	ultra	k2eAgInPc4d1
krátké	krátký	k2eAgInPc4d1
RTD-dokumenty	RTD-dokument	k1gInPc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
URLs	URLs	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
NFC	NFC	kA
Forum	forum	k1gNnSc1
dodalo	dodat	k5eAaPmAgNnS
specifikaci	specifikace	k1gFnSc4
protokolu	protokol	k1gInSc2
Simple	Simple	k1gMnPc2
NDEF	NDEF	kA
Exchange	Exchange	k1gInSc4
Protocol	Protocola	k1gFnPc2
pro	pro	k7c4
umožnění	umožnění	k1gNnSc4
příjmu	příjem	k1gInSc2
a	a	k8xC
vysílání	vysílání	k1gNnSc4
zpráv	zpráva	k1gFnPc2
mezi	mezi	k7c7
dvěma	dva	k4xCgInPc7
aktivními	aktivní	k2eAgInPc7d1
NFC	NFC	kA
zařízeními	zařízení	k1gNnPc7
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
GSMA	GSMA	kA
</s>
<s>
Asociace	asociace	k1gFnSc1
světového	světový	k2eAgInSc2d1
systému	systém	k1gInSc2
pro	pro	k7c4
mobilní	mobilní	k2eAgFnSc4d1
komunikaci	komunikace	k1gFnSc4
(	(	kIx(
<g/>
Global	globat	k5eAaImAgInS
System	Systo	k1gNnSc7
for	forum	k1gNnPc2
Mobile	mobile	k1gNnPc7
Communications	Communications	k1gInSc1
<g/>
,	,	kIx,
GSMA	GSMA	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
globální	globální	k2eAgFnSc7d1
oborovou	oborový	k2eAgFnSc7d1
organizací	organizace	k1gFnSc7
zahrnující	zahrnující	k2eAgInSc4d1
téměř	téměř	k6eAd1
800	#num#	k4
mobilních	mobilní	k2eAgInPc2d1
operátorů	operátor	k1gInPc2
a	a	k8xC
přes	přes	k7c4
200	#num#	k4
firem	firma	k1gFnPc2
poskytujících	poskytující	k2eAgFnPc2d1
produkty	produkt	k1gInPc4
a	a	k8xC
služby	služba	k1gFnSc2
ve	v	k7c6
219	#num#	k4
zemích	zem	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k4c1
členů	člen	k1gMnPc2
této	tento	k3xDgFnSc2
asociace	asociace	k1gFnSc2
vyzkoušelo	vyzkoušet	k5eAaPmAgNnS
technologii	technologie	k1gFnSc3
NFC	NFC	kA
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
zkušenosti	zkušenost	k1gFnSc2
jim	on	k3xPp3gMnPc3
umožnily	umožnit	k5eAaPmAgInP
komerční	komerční	k2eAgFnSc4d1
spuštění	spuštění	k1gNnSc6
služeb	služba	k1gFnPc2
založených	založený	k2eAgFnPc2d1
na	na	k7c6
NFC	NFC	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
GSMA	GSMA	kA
je	být	k5eAaImIp3nS
zapojeno	zapojit	k5eAaPmNgNnS
v	v	k7c6
několika	několik	k4yIc6
iniciativách	iniciativa	k1gFnPc6
souvisejících	související	k2eAgFnPc2d1
s	s	k7c7
NFC	NFC	kA
<g/>
:	:	kIx,
</s>
<s>
Vývoj	vývoj	k1gInSc1
standardů	standard	k1gInPc2
<g/>
,	,	kIx,
certifikací	certifikace	k1gFnPc2
a	a	k8xC
jejich	jejich	k3xOp3gNnPc4
testování	testování	k1gNnPc4
k	k	k7c3
zajištění	zajištění	k1gNnSc3
vzájemné	vzájemný	k2eAgFnSc2d1
součinnosti	součinnost	k1gFnSc2
NFC	NFC	kA
služeb	služba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Iniciativa	iniciativa	k1gFnSc1
Pay-Buy-Mobile	Pay-Buy-Mobila	k1gFnSc3
definuje	definovat	k5eAaBmIp3nS
obecný	obecný	k2eAgInSc1d1
přístup	přístup	k1gInSc1
NFC	NFC	kA
technologie	technologie	k1gFnPc1
k	k	k7c3
mobilním	mobilní	k2eAgNnPc3d1
zařízením	zařízení	k1gNnPc3
s	s	k7c7
platebními	platební	k2eAgInPc7d1
a	a	k8xC
bezkontaktními	bezkontaktní	k2eAgInPc7d1
systémy	systém	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
dvou	dva	k4xCgNnPc6
letech	léto	k1gNnPc6
diskusí	diskuse	k1gFnSc7
<g/>
,	,	kIx,
17	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2010	#num#	k4
<g/>
,	,	kIx,
operátoři	operátor	k1gMnPc1
AT	AT	kA
<g/>
&	&	k?
<g/>
T	T	kA
<g/>
,	,	kIx,
Verizon	Verizon	k1gNnSc1
a	a	k8xC
T-Mobile	T-Mobila	k1gFnSc6
spustili	spustit	k5eAaPmAgMnP
společný	společný	k2eAgInSc4d1
podnik	podnik	k1gInSc4
k	k	k7c3
vývoji	vývoj	k1gInSc3
jediné	jediný	k2eAgFnSc2d1
platformy	platforma	k1gFnSc2
založené	založený	k2eAgFnSc2d1
na	na	k7c6
specifikacích	specifikace	k1gFnPc6
NFC	NFC	kA
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
užita	užít	k5eAaPmNgFnS
jejich	jejich	k3xOp3gNnPc3
zákazníky	zákazník	k1gMnPc4
k	k	k7c3
realizaci	realizace	k1gFnSc3
mobilních	mobilní	k2eAgFnPc2d1
plateb	platba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	nový	k2eAgInSc1d1
podnik	podnik	k1gInSc1
<g/>
,	,	kIx,
pod	pod	k7c7
názvem	název	k1gInSc7
ISIS	Isis	k1gFnSc1
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
navržen	navrhnout	k5eAaPmNgInS
k	k	k7c3
rychlému	rychlý	k2eAgNnSc3d1
a	a	k8xC
plošnému	plošný	k2eAgNnSc3d1
nasazení	nasazení	k1gNnSc3
NFC	NFC	kA
technologie	technologie	k1gFnPc1
k	k	k7c3
realizaci	realizace	k1gFnSc3
plateb	platba	k1gFnPc2
za	za	k7c4
jízdné	jízdné	k1gNnSc4
u	u	k7c2
dopravců	dopravce	k1gMnPc2
v	v	k7c6
celých	celý	k2eAgInPc6d1
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Funkcionalita	funkcionalita	k1gFnSc1
by	by	kYmCp3nS
v	v	k7c6
této	tento	k3xDgFnSc6
implementaci	implementace	k1gFnSc6
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
shodná	shodný	k2eAgFnSc1d1
s	s	k7c7
platebními	platební	k2eAgFnPc7d1
<g/>
,	,	kIx,
bezkontaktními	bezkontaktní	k2eAgFnPc7d1
kartami	karta	k1gFnPc7
použitými	použitý	k2eAgFnPc7d1
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
StoLPaN	StoLPaN	k?
</s>
<s>
StoLPaN	StoLPaN	k?
(	(	kIx(
<g/>
'	'	kIx"
<g/>
Store	Stor	k1gMnSc5
Logistics	Logisticsa	k1gFnPc2
and	and	k?
Payment	Payment	k1gMnSc1
with	with	k1gMnSc1
NFC	NFC	kA
<g/>
'	'	kIx"
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
celoevropské	celoevropský	k2eAgNnSc1d1
konsorcium	konsorcium	k1gNnSc1
podporované	podporovaný	k2eAgNnSc1d1
Evropskou	evropský	k2eAgFnSc7d1
komisí	komise	k1gFnSc7
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
konkrétně	konkrétně	k6eAd1
programem	program	k1gInSc7
Technika	technikum	k1gNnSc2
informační	informační	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
IST	IST	kA
(	(	kIx(
<g/>
Information	Information	k1gInSc1
Society	societa	k1gFnSc2
Technologies	Technologiesa	k1gFnPc2
<g/>
)	)	kIx)
nadnárodního	nadnárodní	k2eAgInSc2d1
projektu	projekt	k1gInSc2
FP6	FP6	k1gFnSc2
a	a	k8xC
zkoumá	zkoumat	k5eAaImIp3nS
dosud	dosud	k6eAd1
nevyužitý	využitý	k2eNgInSc1d1
potenciál	potenciál	k1gInSc1
této	tento	k3xDgFnSc2
technologie	technologie	k1gFnSc2
ve	v	k7c6
vztahu	vztah	k1gInSc6
k	k	k7c3
mobilní	mobilní	k2eAgFnSc3d1
komunikaci	komunikace	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
NFC	NFC	kA
Forum	forum	k1gNnSc1
</s>
<s>
NFC	NFC	kA
Forum	forum	k1gNnSc1
je	být	k5eAaImIp3nS
nezisková	ziskový	k2eNgFnSc1d1
organizace	organizace	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
za	za	k7c7
účelem	účel	k1gInSc7
zkonsolidovat	zkonsolidovat	k5eAaPmF
rádiové	rádiový	k2eAgFnPc4d1
technologie	technologie	k1gFnPc4
krátkého	krátký	k2eAgInSc2d1
dosahu	dosah	k1gInSc2
do	do	k7c2
jednoho	jeden	k4xCgInSc2
funkčního	funkční	k2eAgInSc2d1
celku	celek	k1gInSc2
<g/>
,	,	kIx,
NFC	NFC	kA
<g/>
,	,	kIx,
se	s	k7c7
záměrem	záměr	k1gInSc7
protlačit	protlačit	k5eAaPmF
tuto	tento	k3xDgFnSc4
technologii	technologie	k1gFnSc4
na	na	k7c4
veškeré	veškerý	k3xTgInPc4
možné	možný	k2eAgInPc4d1
trhy	trh	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c6
uskupení	uskupení	k1gNnSc6
<g/>
,	,	kIx,
které	který	k3yQgNnSc4,k3yIgNnSc4,k3yRgNnSc4
specifikuje	specifikovat	k5eAaBmIp3nS
NFC	NFC	kA
standardy	standard	k1gInPc4
postavené	postavený	k2eAgInPc4d1
na	na	k7c4
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
standardech	standard	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
uskupení	uskupení	k1gNnSc6
jsou	být	k5eAaImIp3nP
zahrnuty	zahrnut	k2eAgFnPc1d1
firmy	firma	k1gFnPc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
udávají	udávat	k5eAaImIp3nP
směr	směr	k1gInSc4
vývoje	vývoj	k1gInSc2
v	v	k7c6
mobilních	mobilní	k2eAgFnPc6d1
<g/>
,	,	kIx,
informačních	informační	k2eAgFnPc6d1
a	a	k8xC
platebních	platební	k2eAgFnPc6d1
technologiích	technologie	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
založeno	založit	k5eAaPmNgNnS
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
firmami	firma	k1gFnPc7
<g/>
,	,	kIx,
mezi	mezi	k7c7
nimiž	jenž	k3xRgMnPc7
byla	být	k5eAaImAgFnS
firma	firma	k1gFnSc1
Nokia	Nokia	kA
<g/>
,	,	kIx,
Philips	Philips	kA
a	a	k8xC
Sony	Sony	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
postupem	postup	k1gInSc7
času	čas	k1gInSc2
počet	počet	k1gInSc1
členů	člen	k1gInPc2
NFC	NFC	kA
fóra	fórum	k1gNnPc4
roste	růst	k5eAaImIp3nS
a	a	k8xC
nyní	nyní	k6eAd1
již	již	k6eAd1
čítá	čítat	k5eAaImIp3nS
přes	přes	k7c4
170	#num#	k4
členů	člen	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
výrobce	výrobce	k1gMnSc4
hardwaru	hardware	k1gInSc2
<g/>
,	,	kIx,
softwaru	software	k1gInSc2
a	a	k8xC
finanční	finanční	k2eAgFnSc2d1
instituce	instituce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
hlavní	hlavní	k2eAgInPc4d1
záměry	záměr	k1gInPc4
organizace	organizace	k1gFnSc2
NFC	NFC	kA
Forum	forum	k1gNnSc1
patří	patřit	k5eAaImIp3nS
<g/>
:	:	kIx,
</s>
<s>
Vývoj	vývoj	k1gInSc1
NFC	NFC	kA
standardů	standard	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
jsou	být	k5eAaImIp3nP
navrhovány	navrhovat	k5eAaImNgInP
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
neporušovaly	porušovat	k5eNaImAgFnP
architekturu	architektura	k1gFnSc4
NFC	NFC	kA
a	a	k8xC
definují	definovat	k5eAaBmIp3nP
parametry	parametr	k1gInPc4
pro	pro	k7c4
vzájemnou	vzájemný	k2eAgFnSc4d1
kooperaci	kooperace	k1gFnSc4
protokolů	protokol	k1gInPc2
a	a	k8xC
zařízení	zařízení	k1gNnPc2
<g/>
,	,	kIx,
</s>
<s>
kontrola	kontrola	k1gFnSc1
<g/>
,	,	kIx,
zdali	zdali	k8xS
zařízení	zařízení	k1gNnSc1
NFC	NFC	kA
fungují	fungovat	k5eAaImIp3nP
podle	podle	k7c2
specifikací	specifikace	k1gFnPc2
NFC	NFC	kA
fóra	fórum	k1gNnPc4
<g/>
,	,	kIx,
</s>
<s>
rozvoj	rozvoj	k1gInSc1
produktů	produkt	k1gInPc2
pro	pro	k7c4
podporu	podpora	k1gFnSc4
NFC	NFC	kA
<g/>
,	,	kIx,
</s>
<s>
rozšiřování	rozšiřování	k1gNnSc3
povědomí	povědomí	k1gNnSc2
o	o	k7c4
NFC	NFC	kA
mezi	mezi	k7c7
firmami	firma	k1gFnPc7
a	a	k8xC
zákazníky	zákazník	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
Java	Java	k1gFnSc1
Community	Communita	k1gFnSc2
Process	Processa	k1gFnPc2
</s>
<s>
Java	Java	k1gMnSc1
Community	Communita	k1gFnSc2
Process	Process	k1gInSc1
(	(	kIx(
<g/>
JCP	JCP	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
proces	proces	k1gInSc1
<g/>
,	,	kIx,
program	program	k1gInSc1
či	či	k8xC
mechanismus	mechanismus	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
zašťituje	zašťitovat	k5eAaImIp3nS,k5eAaPmIp3nS,k5eAaBmIp3nS
vývoj	vývoj	k1gInSc4
standardů	standard	k1gInPc2
<g/>
,	,	kIx,
specifikací	specifikace	k1gFnPc2
a	a	k8xC
nových	nový	k2eAgInPc2d1
API	API	kA
pro	pro	k7c4
platformu	platforma	k1gFnSc4
jazyka	jazyk	k1gInSc2
Java	Jav	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Účastnit	účastnit	k5eAaImF
se	se	k3xPyFc4
vývoje	vývoj	k1gInSc2
a	a	k8xC
poskytování	poskytování	k1gNnSc2
zpětné	zpětný	k2eAgFnSc2d1
vazby	vazba	k1gFnSc2
pro	pro	k7c4
požadavky	požadavek	k1gInPc4
na	na	k7c4
změnu	změna	k1gFnSc4
Java	Jav	k1gInSc2
specifikací	specifikace	k1gFnPc2
(	(	kIx(
<g/>
Java	Java	k1gFnSc1
Specification	Specification	k1gInSc1
Requests	Requests	k1gInSc1
<g/>
,	,	kIx,
zkratka	zkratka	k1gFnSc1
JSR	JSR	kA
<g/>
)	)	kIx)
může	moct	k5eAaImIp3nS
kdokoliv	kdokoliv	k3yInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
JCP	JCP	kA
tímto	tento	k3xDgInSc7
způsobem	způsob	k1gInSc7
přijalo	přijmout	k5eAaPmAgNnS
dvě	dva	k4xCgNnPc1
důležitá	důležitý	k2eAgFnSc1d1
API	API	kA
specifikace	specifikace	k1gFnSc1
pro	pro	k7c4
mobilní	mobilní	k2eAgInPc4d1
telefony	telefon	k1gInPc4
podporující	podporující	k2eAgFnSc4d1
NFC	NFC	kA
technologii	technologie	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
konkrétně	konkrétně	k6eAd1
Contactless	Contactless	k1gInSc1
Communications	Communicationsa	k1gFnPc2
API	API	kA
(	(	kIx(
<g/>
JSR	JSR	kA
<g/>
257	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Secure	Secur	k1gMnSc5
and	and	k?
Trust	trust	k1gInSc1
Service	Service	k1gFnSc1
API	API	kA
(	(	kIx(
<g/>
JSR	JSR	kA
<g/>
177	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Alternativní	alternativní	k2eAgFnSc1d1
implementace	implementace	k1gFnSc1
NFC	NFC	kA
</s>
<s>
Absence	absence	k1gFnSc1
vestavěných	vestavěný	k2eAgInPc2d1
NFC	NFC	kA
čipů	čip	k1gInPc2
v	v	k7c6
mobilních	mobilní	k2eAgInPc6d1
telefonech	telefon	k1gInPc6
nebrání	bránit	k5eNaImIp3nS
ve	v	k7c6
využití	využití	k1gNnSc6
NFC	NFC	kA
technologie	technologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
tyto	tento	k3xDgInPc4
případy	případ	k1gInPc4
byly	být	k5eAaImAgFnP
vyvinuty	vyvinout	k5eAaPmNgFnP
karty	karta	k1gFnPc1
microSD	microSD	k?
a	a	k8xC
UICC	UICC	kA
SIM	SIM	kA
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
obsahují	obsahovat	k5eAaImIp3nP
bezkontaktní	bezkontaktní	k2eAgInSc4d1
smartcard	smartcard	k1gInSc4
čipy	čip	k1gInPc1
s	s	k7c7
rozhraním	rozhraní	k1gNnSc7
podle	podle	k7c2
normy	norma	k1gFnSc2
ISO14443	ISO14443	k1gFnSc2
s	s	k7c7
vestavěnou	vestavěný	k2eAgFnSc7d1
anténou	anténa	k1gFnSc7
nebo	nebo	k8xC
bez	bez	k7c2
ní	on	k3xPp3gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc4
implementace	implementace	k1gFnPc1
v	v	k7c6
podobě	podoba	k1gFnSc6
SIM	sima	k1gFnPc2
karet	kareta	k1gFnPc2
a	a	k8xC
karet	kareta	k1gFnPc2
microSD	microSD	k?
umožňují	umožňovat	k5eAaImIp3nP
nasazení	nasazení	k1gNnSc2
této	tento	k3xDgFnSc2
technologie	technologie	k1gFnSc2
na	na	k7c4
současná	současný	k2eAgNnPc4d1
zařízení	zařízení	k1gNnPc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
umožňuje	umožňovat	k5eAaImIp3nS
realizaci	realizace	k1gFnSc3
NFC	NFC	kA
služeb	služba	k1gFnPc2
i	i	k8xC
na	na	k7c6
přístrojích	přístroj	k1gInPc6
<g/>
,	,	kIx,
u	u	k7c2
nichž	jenž	k3xRgMnPc2
s	s	k7c7
tímto	tento	k3xDgNnSc7
využitím	využití	k1gNnSc7
nebylo	být	k5eNaImAgNnS
počítáno	počítán	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výhodou	výhoda	k1gFnSc7
tohoto	tento	k3xDgInSc2
přístupu	přístup	k1gInSc2
je	být	k5eAaImIp3nS
jak	jak	k8xS,k8xC
zlevnění	zlevnění	k1gNnSc4
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
zkrácení	zkrácení	k1gNnSc1
doby	doba	k1gFnSc2
globálního	globální	k2eAgNnSc2d1
nasazení	nasazení	k1gNnSc2
bezkontaktních	bezkontaktní	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
na	na	k7c4
stávající	stávající	k2eAgInSc4d1
trh	trh	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Ostatní	ostatní	k2eAgMnPc1d1
spolutvůrci	spolutvůrce	k1gMnPc1
norem	norma	k1gFnPc2
NFC	NFC	kA
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1
standardizační	standardizační	k2eAgInPc1d1
subjekty	subjekt	k1gInPc1
a	a	k8xC
projekty	projekt	k1gInPc1
zainteresované	zainteresovaný	k2eAgInPc1d1
ve	v	k7c6
vývoji	vývoj	k1gInSc6
NFC	NFC	kA
zahrnují	zahrnovat	k5eAaImIp3nP
<g/>
:	:	kIx,
</s>
<s>
ETSI	ETSI	kA
/	/	kIx~
SCP	SCP	kA
(	(	kIx(
<g/>
Smart	Smart	k1gInSc1
Card	Card	k1gInSc1
Platform	Platform	k1gInSc4
<g/>
)	)	kIx)
ke	k	k7c3
specifikaci	specifikace	k1gFnSc3
rozhraní	rozhraní	k1gNnSc2
mezi	mezi	k7c7
SIM	SIM	kA
kartou	karta	k1gFnSc7
a	a	k8xC
čipovou	čipový	k2eAgFnSc7d1
sadou	sada	k1gFnSc7
NFC	NFC	kA
<g/>
.	.	kIx.
</s>
<s>
GlobalPlatform	GlobalPlatform	k1gInSc1
ke	k	k7c3
specifikaci	specifikace	k1gFnSc3
multi-aplikační	multi-aplikační	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
secure	secur	k1gMnSc5
elementu	element	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
EMVCo	EMVCo	k6eAd1
pro	pro	k7c4
zajištění	zajištění	k1gNnSc4
interoperability	interoperabilita	k1gFnSc2
čipových	čipový	k2eAgFnPc2d1
platebních	platební	k2eAgFnPc2d1
karet	kareta	k1gFnPc2
s	s	k7c7
NFC	NFC	kA
zařízeními	zařízení	k1gNnPc7
<g/>
,	,	kIx,
testování	testování	k1gNnSc1
a	a	k8xC
schvalování	schvalování	k1gNnSc1
nových	nový	k2eAgInPc2d1
platebních	platební	k2eAgInPc2d1
NFC	NFC	kA
technologií	technologie	k1gFnPc2
vůči	vůči	k7c3
EMV	EMV	kA
standardům	standard	k1gInPc3
<g/>
.	.	kIx.
</s>
<s>
Bezpečnostní	bezpečnostní	k2eAgInPc1d1
aspekty	aspekt	k1gInPc1
</s>
<s>
Ačkoliv	ačkoliv	k8xS
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
krátký	krátký	k2eAgInSc1d1
dosah	dosah	k1gInSc1
NFC	NFC	kA
technologie	technologie	k1gFnSc2
brán	brát	k5eAaImNgInS
za	za	k7c4
jeden	jeden	k4xCgInSc4
z	z	k7c2
bezpečnostních	bezpečnostní	k2eAgInPc2d1
aspektů	aspekt	k1gInPc2
<g/>
,	,	kIx,
tak	tak	k9
NFC	NFC	kA
samotné	samotný	k2eAgNnSc1d1
nezabezpečuje	zabezpečovat	k5eNaImIp3nS
komunikaci	komunikace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
Ernst	Ernst	k1gMnSc1
Haselsteiner	Haselsteiner	k1gMnSc1
a	a	k8xC
Klemens	Klemensa	k1gFnPc2
Breitfuß	Breitfuß	k1gMnPc1
popsali	popsat	k5eAaPmAgMnP
různé	různý	k2eAgInPc4d1
typy	typ	k1gInPc4
útoků	útok	k1gInPc2
a	a	k8xC
ukázali	ukázat	k5eAaPmAgMnP
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
využít	využít	k5eAaPmF
odolnost	odolnost	k1gFnSc4
NFC	NFC	kA
vůči	vůči	k7c3
útokům	útok	k1gInPc3
typu	typ	k1gInSc2
Man-in-the-middle	Man-in-the-middle	k1gMnPc4
k	k	k7c3
získání	získání	k1gNnSc3
specifického	specifický	k2eAgInSc2d1
klíče	klíč	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
Tato	tento	k3xDgFnSc1
technika	technika	k1gFnSc1
není	být	k5eNaImIp3nS
součástí	součást	k1gFnSc7
ISO	ISO	kA
standardu	standard	k1gInSc2
<g/>
,	,	kIx,
NFC	NFC	kA
nenabízí	nabízet	k5eNaImIp3nP
nějakou	nějaký	k3yIgFnSc4
ochranu	ochrana	k1gFnSc4
proti	proti	k7c3
odposlechu	odposlech	k1gInSc3
a	a	k8xC
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
proto	proto	k8xC
zranitelné	zranitelný	k2eAgInPc4d1
vůči	vůči	k7c3
modifikaci	modifikace	k1gFnSc3
dat	datum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Aplikace	aplikace	k1gFnSc1
využívající	využívající	k2eAgFnSc2d1
NFC	NFC	kA
proto	proto	k8xC
musí	muset	k5eAaImIp3nP
použít	použít	k5eAaPmF
kryptografické	kryptografický	k2eAgInPc1d1
protokoly	protokol	k1gInPc1
vyšších	vysoký	k2eAgFnPc2d2
vrstev	vrstva	k1gFnPc2
(	(	kIx(
<g/>
např.	např.	kA
SSL	SSL	kA
<g/>
)	)	kIx)
k	k	k7c3
vytvoření	vytvoření	k1gNnSc3
zabezpečeného	zabezpečený	k2eAgInSc2d1
kanálu	kanál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajištění	zajištění	k1gNnSc4
bezpečnosti	bezpečnost	k1gFnSc2
přenášených	přenášený	k2eAgNnPc2d1
dat	datum	k1gNnPc2
pomocí	pomocí	k7c2
technologie	technologie	k1gFnSc2
NFC	NFC	kA
proto	proto	k8xC
vyžaduje	vyžadovat	k5eAaImIp3nS
spolupráci	spolupráce	k1gFnSc4
na	na	k7c6
více	hodně	k6eAd2
úrovních	úroveň	k1gFnPc6
<g/>
:	:	kIx,
</s>
<s>
výrobci	výrobce	k1gMnPc1
hardware	hardware	k1gInSc4
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
budou	být	k5eAaImBp3nP
chtít	chtít	k5eAaImF
zabezpečit	zabezpečit	k5eAaPmF
NFC	NFC	kA
zařízení	zařízení	k1gNnSc4
silnou	silný	k2eAgFnSc7d1
kryptografií	kryptografie	k1gFnSc7
a	a	k8xC
autentizačními	autentizační	k2eAgInPc7d1
protokoly	protokol	k1gInPc7
<g/>
;	;	kIx,
</s>
<s>
zákazníci	zákazník	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
budou	být	k5eAaImBp3nP
chtít	chtít	k5eAaImF
zabezpečit	zabezpečit	k5eAaPmF
jejich	jejich	k3xOp3gNnSc2
zařízení	zařízení	k1gNnSc2
a	a	k8xC
data	datum	k1gNnSc2
různými	různý	k2eAgInPc7d1
typy	typ	k1gInPc7
zámků	zámek	k1gInPc2
<g/>
,	,	kIx,
hesly	heslo	k1gNnPc7
či	či	k8xC
antiviry	antivir	k1gInPc7
<g/>
;	;	kIx,
</s>
<s>
výrobci	výrobce	k1gMnPc1
softwaru	software	k1gInSc2
a	a	k8xC
subjekty	subjekt	k1gInPc4
poskytující	poskytující	k2eAgFnSc2d1
bezkontaktní	bezkontaktní	k2eAgFnSc2d1
transakce	transakce	k1gFnSc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
budou	být	k5eAaImBp3nP
chtít	chtít	k5eAaImF
zabezpečit	zabezpečit	k5eAaPmF
své	svůj	k3xOyFgInPc4
systémy	systém	k1gInPc4
proti	proti	k7c3
spywaru	spywar	k1gInSc3
a	a	k8xC
malwaru	malwar	k1gInSc3
před	před	k7c7
nákazou	nákaza	k1gFnSc7
systémů	systém	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Útoky	útok	k1gInPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
různého	různý	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
:	:	kIx,
</s>
<s>
Odposlech	odposlech	k1gInSc1
-	-	kIx~
Pomocí	pomocí	k7c2
antén	anténa	k1gFnPc2
můžeme	moct	k5eAaImIp1nP
odposlechnout	odposlechnout	k5eAaPmF
radiofrekvenční	radiofrekvenční	k2eAgInSc4d1
signál	signál	k1gInSc4
vysílaný	vysílaný	k2eAgInSc4d1
zařízeními	zařízení	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzdálenost	vzdálenost	k1gFnSc1
<g/>
,	,	kIx,
z	z	k7c2
níž	jenž	k3xRgFnSc2
je	být	k5eAaImIp3nS
schopen	schopen	k2eAgMnSc1d1
útočník	útočník	k1gMnSc1
odposlechnout	odposlechnout	k5eAaPmF
signál	signál	k1gInSc4
<g/>
,	,	kIx,
závisí	záviset	k5eAaImIp3nS
na	na	k7c6
několika	několik	k4yIc6
parametrech	parametr	k1gInPc6
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
především	především	k9
na	na	k7c6
použitém	použitý	k2eAgInSc6d1
komunikačním	komunikační	k2eAgInSc6d1
režimu	režim	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
u	u	k7c2
pasivních	pasivní	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
negenerují	generovat	k5eNaImIp3nP
své	svůj	k3xOyFgNnSc4
elektromagnetické	elektromagnetický	k2eAgNnSc4d1
pole	pole	k1gNnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
odposlech	odposlech	k1gInSc1
výrazně	výrazně	k6eAd1
náročnější	náročný	k2eAgMnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
je	být	k5eAaImIp3nS
tomu	ten	k3xDgNnSc3
u	u	k7c2
aktivních	aktivní	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
odposlech	odposlech	k1gInSc4
můžeme	moct	k5eAaImIp1nP
realizovat	realizovat	k5eAaBmF
i	i	k9
ze	z	k7c2
vzdálenosti	vzdálenost	k1gFnSc2
několika	několik	k4yIc2
metrů	metr	k1gMnPc2
(	(	kIx(
<g/>
sic	sic	k6eAd1
jde	jít	k5eAaImIp3nS
o	o	k7c6
opravdu	opravdu	k6eAd1
krátké	krátký	k2eAgFnSc6d1
vzdálenosti	vzdálenost	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Modifikace	modifikace	k1gFnSc1
dat	datum	k1gNnPc2
-	-	kIx~
Je	být	k5eAaImIp3nS
relativně	relativně	k6eAd1
jednoduché	jednoduchý	k2eAgNnSc1d1
narušovat	narušovat	k5eAaImF
přenášená	přenášený	k2eAgNnPc4d1
data	datum	k1gNnPc4
pomocí	pomocí	k7c2
RFID	RFID	kA
rušičky	rušička	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neexistuje	existovat	k5eNaImIp3nS
zatím	zatím	k6eAd1
žádná	žádný	k3yNgFnSc1
možnost	možnost	k1gFnSc1
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
zabránit	zabránit	k5eAaPmF
takovému	takový	k3xDgInSc3
typu	typ	k1gInSc3
útoku	útok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Detekce	detekce	k1gFnSc1
takového	takový	k3xDgInSc2
útoku	útok	k1gInSc2
je	být	k5eAaImIp3nS
ale	ale	k9
možná	možný	k2eAgFnSc1d1
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
NFC	NFC	kA
zařízení	zařízení	k1gNnPc2
během	během	k7c2
přenosu	přenos	k1gInSc2
kontrolují	kontrolovat	k5eAaImIp3nP
své	svůj	k3xOyFgNnSc4
okolní	okolní	k2eAgNnSc4d1
elektromagnetické	elektromagnetický	k2eAgNnSc4d1
pole	pole	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výrazně	výrazně	k6eAd1
obtížnější	obtížný	k2eAgFnSc1d2
úlohou	úloha	k1gFnSc7
je	být	k5eAaImIp3nS
modifikace	modifikace	k1gFnPc4
dat	datum	k1gNnPc2
takovým	takový	k3xDgInSc7
způsobem	způsob	k1gInSc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
zdála	zdát	k5eAaImAgNnP
veškerá	veškerý	k3xTgNnPc1
komunikace	komunikace	k1gFnPc4
uživatelům	uživatel	k1gMnPc3
jako	jako	k8xC,k8xS
nenarušená	narušený	k2eNgNnPc1d1
<g/>
,	,	kIx,
validní	validní	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
modifikaci	modifikace	k1gFnSc3
přenášených	přenášený	k2eAgNnPc2d1
dat	datum	k1gNnPc2
musí	muset	k5eAaImIp3nS
útočník	útočník	k1gMnSc1
modifikovat	modifikovat	k5eAaBmF
jednotlivé	jednotlivý	k2eAgInPc4d1
bity	bit	k1gInPc4
radiofrekvenčního	radiofrekvenční	k2eAgInSc2d1
signálu	signál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proveditelnost	proveditelnost	k1gFnSc1
takového	takový	k3xDgInSc2
útoku	útok	k1gInSc2
(	(	kIx(
<g/>
t.j.	t.j.	k?
<g/>
,	,	kIx,
jestliže	jestliže	k8xS
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
změnit	změnit	k5eAaPmF
hodnotu	hodnota	k1gFnSc4
bitu	bit	k1gInSc2
signálu	signál	k1gInSc2
z	z	k7c2
0	#num#	k4
na	na	k7c4
1	#num#	k4
nebo	nebo	k8xC
naopak	naopak	k6eAd1
<g/>
)	)	kIx)
se	se	k3xPyFc4
vztahuje	vztahovat	k5eAaImIp3nS
k	k	k7c3
hloubce	hloubka	k1gFnSc3
amplitudové	amplitudový	k2eAgFnSc2d1
modulace	modulace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jestliže	jestliže	k8xS
jsou	být	k5eAaImIp3nP
data	datum	k1gNnPc1
přenesena	přenést	k5eAaPmNgFnS
modifikovaným	modifikovaný	k2eAgNnSc7d1
Millerovým	Millerův	k2eAgNnSc7d1
kódováním	kódování	k1gNnSc7
a	a	k8xC
hloubka	hloubka	k1gFnSc1
modulace	modulace	k1gFnSc1
byla	být	k5eAaImAgFnS
100	#num#	k4
<g/>
%	%	kIx~
<g/>
,	,	kIx,
pak	pak	k6eAd1
pouze	pouze	k6eAd1
některé	některý	k3yIgInPc1
bity	bit	k1gInPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
modifikovány	modifikován	k2eAgInPc4d1
<g/>
.	.	kIx.
100	#num#	k4
<g/>
%	%	kIx~
hloubka	hloubka	k1gFnSc1
modulace	modulace	k1gFnSc1
nám	my	k3xPp1nPc3
umožňuje	umožňovat	k5eAaImIp3nS
eliminovat	eliminovat	k5eAaBmF
pauzy	pauza	k1gFnPc4
v	v	k7c6
radiofrekvenčním	radiofrekvenční	k2eAgInSc6d1
signálu	signál	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
neumožňuje	umožňovat	k5eNaImIp3nS
generovat	generovat	k5eAaImF
pauzy	pauza	k1gFnPc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
pauzy	pauza	k1gFnPc1
nebyly	být	k5eNaImAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
pouze	pouze	k6eAd1
bity	bit	k2eAgFnPc1d1
1	#num#	k4
následované	následovaný	k2eAgFnPc1d1
bitem	bit	k1gInSc7
1	#num#	k4
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
změněny	změněn	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přenosem	přenos	k1gInSc7
dat	datum	k1gNnPc2
zakódovaných	zakódovaný	k2eAgFnPc2d1
pomocí	pomoc	k1gFnPc2
kódování	kódování	k1gNnSc2
Manchester	Manchester	k1gInSc1
s	s	k7c7
hloubkou	hloubka	k1gFnSc7
modulace	modulace	k1gFnSc2
10	#num#	k4
%	%	kIx~
umožňujeme	umožňovat	k5eAaImIp1nP
útočníkovi	útočník	k1gMnSc3
modifikovat	modifikovat	k5eAaBmF
data	datum	k1gNnPc1
ve	v	k7c6
všech	všecek	k3xTgInPc6
bitech	bit	k1gInPc6
signálu	signál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Přepojovaný	přepojovaný	k2eAgInSc1d1
útok	útok	k1gInSc1
-	-	kIx~
Přepojované	přepojovaný	k2eAgInPc1d1
útoky	útok	k1gInPc1
jsou	být	k5eAaImIp3nP
možné	možný	k2eAgInPc1d1
i	i	k9
na	na	k7c4
NFC	NFC	kA
zařízeních	zařízení	k1gNnPc6
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
tato	tento	k3xDgFnSc1
technologie	technologie	k1gFnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
protokoly	protokol	k1gInPc4
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
14443	#num#	k4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
jsou	být	k5eAaImIp3nP
na	na	k7c4
tyto	tento	k3xDgInPc4
útoky	útok	k1gInPc4
náchylné	náchylný	k2eAgInPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
typu	typ	k1gInSc6
útoku	útok	k1gInSc2
musí	muset	k5eAaImIp3nS
útočník	útočník	k1gMnSc1
přeposílat	přeposílat	k5eAaImF
požadavky	požadavek	k1gInPc4
čtečky	čtečka	k1gFnSc2
k	k	k7c3
oběti	oběť	k1gFnSc3
a	a	k8xC
poté	poté	k6eAd1
vracet	vracet	k5eAaImF
tyto	tento	k3xDgFnPc4
odpovědi	odpověď	k1gFnPc4
v	v	k7c6
reálném	reálný	k2eAgInSc6d1
čase	čas	k1gInSc6
zpět	zpět	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mohl	moct	k5eAaImAgInS
úspěšně	úspěšně	k6eAd1
předstírat	předstírat	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
čipová	čipový	k2eAgFnSc1d1
Smart	Smarta	k1gFnPc2
karta	karta	k1gFnSc1
oběti	oběť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
útoky	útok	k1gInPc4
jsou	být	k5eAaImIp3nP
podobné	podobný	k2eAgNnSc1d1
útokům	útok	k1gInPc3
Man-in-the-Middle	Man-in-the-Middle	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ukázkové	ukázkový	k2eAgInPc4d1
zdrojové	zdrojový	k2eAgInPc4d1
kódy	kód	k1gInPc4
knihovny	knihovna	k1gFnPc1
libnfc	libnfc	k6eAd1
demonstrují	demonstrovat	k5eAaBmIp3nP
přepojovaný	přepojovaný	k2eAgInSc4d1
útok	útok	k1gInSc4
s	s	k7c7
použitím	použití	k1gNnSc7
dvou	dva	k4xCgInPc2
běžně	běžně	k6eAd1
dostupných	dostupný	k2eAgInPc2d1
NFC	NFC	kA
zařízení	zařízení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
způsob	způsob	k1gInSc1
útoku	útok	k1gInSc2
byl	být	k5eAaImAgInS
také	také	k9
předveden	předveden	k2eAgInSc1d1
na	na	k7c6
dvou	dva	k4xCgInPc6
běžných	běžný	k2eAgInPc6d1
telefonech	telefon	k1gInPc6
podporujících	podporující	k2eAgInPc2d1
NFC	NFC	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ztráta	ztráta	k1gFnSc1
majetku	majetek	k1gInSc2
-	-	kIx~
Ztráta	ztráta	k1gFnSc1
NFC	NFC	kA
RFID	RFID	kA
karty	karta	k1gFnPc1
umožní	umožnit	k5eAaPmIp3nP
nálezci	nálezce	k1gMnPc1
pracovat	pracovat	k5eAaImF
s	s	k7c7
telefonem	telefon	k1gInSc7
obvykle	obvykle	k6eAd1
jako	jako	k8xC,k8xS
s	s	k7c7
jednofaktorovou	jednofaktorový	k2eAgFnSc7d1
autentizační	autentizační	k2eAgFnSc7d1
entitou	entita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mobilní	mobilní	k2eAgInPc1d1
telefony	telefon	k1gInPc1
chráněné	chráněný	k2eAgInPc1d1
PIN	pin	k1gInSc4
kódem	kód	k1gInSc7
jsou	být	k5eAaImIp3nP
zařízení	zařízení	k1gNnSc4
s	s	k7c7
jednofaktorovou	jednofaktorový	k2eAgFnSc7d1
autentizací	autentizace	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Možnost	možnost	k1gFnSc4
jak	jak	k8xC,k8xS
zabránit	zabránit	k5eAaPmF
zneužití	zneužití	k1gNnSc4
dat	datum	k1gNnPc2
při	při	k7c6
ztrátě	ztráta	k1gFnSc6
zařízení	zařízení	k1gNnSc2
je	být	k5eAaImIp3nS
možnost	možnost	k1gFnSc1
rozšířit	rozšířit	k5eAaPmF
tento	tento	k3xDgInSc4
typ	typ	k1gInSc4
zabezpečení	zabezpečení	k1gNnSc2
o	o	k7c4
další	další	k1gNnSc4
nezávislý	závislý	k2eNgInSc1d1
bezdrátový	bezdrátový	k2eAgInSc1d1
autentizační	autentizační	k2eAgInSc1d1
faktor	faktor	k1gInSc1
<g/>
,	,	kIx,
tj.	tj.	kA
další	další	k2eAgInSc1d1
typ	typ	k1gInSc1
autentizace	autentizace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přerušení	přerušení	k1gNnSc1
spojení	spojení	k1gNnSc2
-	-	kIx~
Otevřené	otevřený	k2eAgNnSc4d1
spojení	spojení	k1gNnSc4
k	k	k7c3
zabezpečeným	zabezpečený	k2eAgFnPc3d1
funkcím	funkce	k1gFnPc3
NFC	NFC	kA
<g/>
,	,	kIx,
nebo	nebo	k8xC
jejich	jejich	k3xOp3gNnPc3
datům	datum	k1gNnPc3
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
chráněno	chránit	k5eAaImNgNnS
intervalem	interval	k1gInSc7
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
kanál	kanál	k1gInSc1
se	se	k3xPyFc4
uzavírá	uzavírat	k5eAaPmIp3nS,k5eAaImIp3nS
tehdy	tehdy	k6eAd1
<g/>
,	,	kIx,
jestliže	jestliže	k8xS
na	na	k7c6
něm	on	k3xPp3gInSc6
není	být	k5eNaImIp3nS
aktivita	aktivita	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Útoky	útok	k1gInPc4
však	však	k8xC
mohou	moct	k5eAaImIp3nP
nastat	nastat	k5eAaPmF
v	v	k7c6
případech	případ	k1gInPc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
zařízení	zařízení	k1gNnSc4
<g/>
,	,	kIx,
opouštějící	opouštějící	k2eAgInSc4d1
kanál	kanál	k1gInSc4
<g/>
,	,	kIx,
jej	on	k3xPp3gMnSc4
neuzavře	uzavřít	k5eNaPmIp3nS
a	a	k8xC
tak	tak	k6eAd1
potenciální	potenciální	k2eAgMnSc1d1
útočník	útočník	k1gMnSc1
může	moct	k5eAaImIp3nS
navázat	navázat	k5eAaPmF
z	z	k7c2
původního	původní	k2eAgNnSc2d1
umístění	umístění	k1gNnSc2
zařízení	zařízení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc1d1
autentizační	autentizační	k2eAgInSc1d1
faktor	faktor	k1gInSc1
by	by	kYmCp3nS
takovým	takový	k3xDgInPc3
případům	případ	k1gInPc3
mohl	moct	k5eAaImAgMnS
zabránit	zabránit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
RFID	RFID	kA
</s>
<s>
NDEF	NDEF	kA
</s>
<s>
Bluetooth	Bluetooth	k1gMnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Near_Field_Communication	Near_Field_Communication	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
What	What	k1gInSc1
is	is	k?
NFC	NFC	kA
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NFC	NFC	kA
Forum	forum	k1gNnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
Nikhila	Nikhil	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
NFC	NFC	kA
—	—	k?
future	futur	k1gMnSc5
of	of	k?
wireless	wireless	k1gInSc1
communication	communication	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gadgetronica	Gadgetronic	k1gInSc2
<g/>
,	,	kIx,
26	#num#	k4
October	October	k1gInSc1
2011	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Technical	Technical	k1gFnSc1
Specifications	Specifications	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NFC	NFC	kA
Forum	forum	k1gNnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
18092	#num#	k4
<g/>
:	:	kIx,
<g/>
2004	#num#	k4
Information	Information	k1gInSc1
technology	technolog	k1gMnPc4
--	--	k?
Telecommunications	Telecommunications	k1gInSc1
and	and	k?
information	information	k1gInSc1
exchange	exchangat	k5eAaPmIp3nS
between	between	k2eAgInSc1d1
systems	systems	k1gInSc1
--	--	k?
Near	Near	k1gInSc1
Field	Field	k1gMnSc1
Communication	Communication	k1gInSc1
--	--	k?
Interface	interface	k1gInSc1
and	and	k?
Protocol	Protocol	k1gInSc1
(	(	kIx(
<g/>
NFCIP-	NFCIP-	k1gFnSc1
<g/>
1	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISO	ISO	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
About	About	k1gInSc1
the	the	k?
Forum	forum	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NFC	NFC	kA
Forum	forum	k1gNnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
NOSOWITZ	NOSOWITZ	kA
<g/>
,	,	kIx,
Dan	Dan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Everything	Everything	k1gInSc1
You	You	k1gFnSc2
Need	Needa	k1gFnPc2
to	ten	k3xDgNnSc1
Know	Know	k1gMnSc1
About	About	k1gMnSc1
Near	Near	k1gMnSc1
Field	Field	k1gMnSc1
Communication	Communication	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Popular	Popular	k1gInSc1
Science	Science	k1gFnSc2
<g/>
,	,	kIx,
1	#num#	k4
March	March	k1gInSc1
2011	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Google	Google	k1gInSc1
Wallet	Wallet	k1gInSc1
—	—	k?
where	wher	k1gInSc5
it	it	k?
works	works	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Google	Google	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Current	Current	k1gInSc1
participating	participating	k1gInSc4
retailers	retailers	k6eAd1
include	include	k6eAd1
<g/>
:	:	kIx,
Macy	Macy	k1gInPc4
<g/>
'	'	kIx"
<g/>
s	s	k7c7
<g/>
,	,	kIx,
American	American	k1gInSc4
Eagle	Eagle	k1gFnSc2
<g/>
,	,	kIx,
and	and	k?
Subway	Subwaa	k1gFnSc2
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Germany	German	k1gInPc4
<g/>
:	:	kIx,
Transit	transit	k1gInSc1
Officials	Officialsa	k1gFnPc2
Enable	Enable	k1gFnSc2
Users	Usersa	k1gFnPc2
to	ten	k3xDgNnSc1
Tap	Tap	k1gFnPc1
or	or	k?
Scan	Scan	k1gMnSc1
in	in	k?
New	New	k1gFnSc1
Trial	trial	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
February	Februara	k1gFnSc2
11	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Austria	Austrium	k1gNnSc2
<g/>
:	:	kIx,
'	'	kIx"
<g/>
Rollout	Rollout	k1gFnSc1
<g/>
'	'	kIx"
Uses	Uses	k1gInSc1
NFC	NFC	kA
Reader	Reader	k1gInSc1
Mode	modus	k1gInSc5
To	to	k9
Sell	Sellum	k1gNnPc2
Tickets	Ticketsa	k1gFnPc2
and	and	k?
Snacks	Snacks	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
March	March	k1gInSc1
1	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Italy	Ital	k1gMnPc7
<g/>
:	:	kIx,
Telecom	Telecom	k1gInSc1
Italia	Italia	k1gFnSc1
and	and	k?
ATM	ATM	kA
to	ten	k3xDgNnSc4
launch	launch	k1gMnSc1
NFC	NFC	kA
ticketing	ticketing	k1gInSc1
service	service	k1gFnSc1
in	in	k?
Milan	Milan	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
April	April	k1gInSc1
24	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
India	indium	k1gNnSc2
<g/>
:	:	kIx,
NFC	NFC	kA
used	used	k1gMnSc1
for	forum	k1gNnPc2
ticketing	ticketing	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
June	jun	k1gMnSc5
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
NFC	NFC	kA
as	as	k9
Technology	technolog	k1gMnPc4
Enabler	Enabler	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NFC	NFC	kA
Forum	forum	k1gNnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
kia	kia	k?
<g/>
,	,	kIx,
Philips	Philips	kA
and	and	k?
Sony	Sony	kA
established	established	k1gInSc1
the	the	k?
Near	Near	k1gInSc1
Field	Field	k1gMnSc1
Communication	Communication	k1gInSc1
(	(	kIx(
<g/>
NFC	NFC	kA
<g/>
)	)	kIx)
Forum	forum	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NFC	NFC	kA
Forum	forum	k1gNnSc1
<g/>
,	,	kIx,
18	#num#	k4
Mar	Mar	k1gFnSc1
2004	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
NFC	NFC	kA
Forum	forum	k1gNnSc1
Unveils	Unveils	k1gInSc1
Technology	technolog	k1gMnPc4
Architecture	Architectur	k1gMnSc5
And	Anda	k1gFnPc2
Announces	Announces	k1gInSc1
Initial	Initial	k1gMnSc1
Specifications	Specificationsa	k1gFnPc2
And	Anda	k1gFnPc2
Mandatory	Mandator	k1gMnPc4
Tag	tag	k1gInSc4
Format	Format	k1gInSc1
Support	support	k1gInSc1
<g/>
.	.	kIx.
www.nfc-forum.org	www.nfc-forum.org	k1gInSc1
<g/>
.	.	kIx.
05	#num#	k4
Jun	jun	k1gMnSc1
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
NFC	NFC	kA
Forum	forum	k1gNnSc1
Publishes	Publishes	k1gMnSc1
Specification	Specification	k1gInSc1
For	forum	k1gNnPc2
"	"	kIx"
<g/>
SmartPoster	SmartPoster	k1gInSc1
<g/>
"	"	kIx"
Records	Records	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
5	#num#	k4
October	October	k1gInSc1
2006	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Nokia	Nokia	kA
6131	#num#	k4
NFC	NFC	kA
<g/>
.	.	kIx.
www.phonearena.com	www.phonearena.com	k1gInSc1
<g/>
.	.	kIx.
7	#num#	k4
Jan	Jan	k1gMnSc1
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
14	#num#	k4
June	jun	k1gMnSc5
2011	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
NFC	NFC	kA
Forum	forum	k1gNnSc1
Announces	Announces	k1gMnSc1
Two	Two	k1gMnSc2
New	New	k1gFnSc2
Specifications	Specificationsa	k1gFnPc2
to	ten	k3xDgNnSc1
Foster	Foster	k1gMnSc1
Device	device	k1gInSc2
Interoperability	interoperabilita	k1gFnSc2
and	and	k?
Peer-to-Peer	Peer-to-Peer	k1gInSc1
Device	device	k1gInSc1
Communication	Communication	k1gInSc1
<g/>
.	.	kIx.
www.nfc-forum.org	www.nfc-forum.org	k1gInSc1
<g/>
.	.	kIx.
19	#num#	k4
May	May	k1gMnSc1
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Video	video	k1gNnSc1
<g/>
:	:	kIx,
Google	Google	k1gInSc1
CEO	CEO	kA
talks	talks	k1gInSc1
Android	android	k1gInSc1
<g/>
,	,	kIx,
Gingerbread	Gingerbread	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
Chrome	chromat	k5eAaImIp3nS
OS	OS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Computerworld	Computerworld	k1gInSc1
<g/>
.	.	kIx.
16	#num#	k4
November	November	k1gInSc1
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Gingerbread	Gingerbread	k1gInSc1
feature	featur	k1gMnSc5
<g/>
:	:	kIx,
Near	Near	k1gMnSc1
Field	Field	k1gMnSc1
Communication	Communication	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Android	android	k1gInSc1
Central	Central	k1gFnSc2
<g/>
.	.	kIx.
21	#num#	k4
Dec	Dec	k1gMnPc2
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
15	#num#	k4
June	jun	k1gMnSc5
2011	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
PELLY	PELLY	kA
<g/>
,	,	kIx,
Nick	Nick	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
How	How	k1gFnSc1
to	ten	k3xDgNnSc4
NFC	NFC	kA
<g/>
.	.	kIx.
www.google.com	www.google.com	k1gInSc1
<g/>
.	.	kIx.
10	#num#	k4
May	May	k1gMnSc1
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
14	#num#	k4
June	jun	k1gMnSc5
2011	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
CLARK	CLARK	kA
<g/>
,	,	kIx,
Sarah	Sarah	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nokia	Nokia	kA
releases	releases	k1gMnSc1
Symbian	Symbian	k1gMnSc1
Anna	Anna	k1gFnSc1
NFC	NFC	kA
update	update	k1gInSc1
<g/>
.	.	kIx.
www.nfcworld.com	www.nfcworld.com	k1gInSc1
<g/>
.	.	kIx.
18	#num#	k4
August	August	k1gMnSc1
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
31	#num#	k4
August	August	k1gMnSc1
2011	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
RIM	RIM	kA
Scores	Scores	k1gMnSc1
MasterCard	MasterCard	k1gMnSc1
NFC	NFC	kA
Certification	Certification	k1gInSc1
<g/>
.	.	kIx.
mobilemarketingmagazine	mobilemarketingmagazinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
MACMANUS	MACMANUS	kA
<g/>
,	,	kIx,
Christopher	Christophra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sony	Sony	kA
<g/>
'	'	kIx"
<g/>
s	s	k7c7
SmartTags	SmartTags	k1gInSc1
could	coulda	k1gFnPc2
change	change	k1gFnPc1
phone	phonout	k5eAaImIp3nS,k5eAaPmIp3nS
habits	habits	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
CNET	CNET	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CBS	CBS	kA
Interactive	Interactiv	k1gInSc5
<g/>
,	,	kIx,
2012-01-16	2012-01-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Ecma	Ecma	k1gMnSc1
International	International	k1gMnSc1
<g/>
:	:	kIx,
Standard	standard	k1gInSc1
ECMA-	ECMA-	k1gFnSc2
<g/>
340	#num#	k4
<g/>
,	,	kIx,
Near	Near	k1gInSc1
Field	Field	k1gInSc1
Communication	Communication	k1gInSc1
Interface	interface	k1gInSc1
and	and	k?
Protocol	Protocol	k1gInSc1
(	(	kIx(
<g/>
NFCIP-	NFCIP-	k1gFnSc1
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
December	December	k1gInSc1
2004	#num#	k4
<g/>
↑	↑	k?
Ecma	Ecma	k1gMnSc1
International	International	k1gMnSc1
<g/>
:	:	kIx,
Standard	standard	k1gInSc1
ECMA-	ECMA-	k1gFnSc2
<g/>
352	#num#	k4
<g/>
,	,	kIx,
Near	Near	k1gInSc1
Field	Field	k1gInSc1
Communication	Communication	k1gInSc1
Interface	interface	k1gInSc1
and	and	k?
Protocol	Protocol	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
–	–	k?
<g/>
2	#num#	k4
(	(	kIx(
<g/>
NFCIP-	NFCIP-	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
December	December	k1gInSc1
2003	#num#	k4
<g/>
↑	↑	k?
NFC-forum	NFC-forum	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
.	.	kIx.
www.nfc-forum.org	www.nfc-forum.org	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Electronista	Electronista	k1gMnSc1
Article	Article	k1gFnSc2
<g/>
:	:	kIx,
New	New	k1gFnSc2
NFC	NFC	kA
spec	spéct	k5eAaPmRp2nSwK
lets	lets	k1gInSc1
two	two	k?
phones	phones	k1gInSc1
swap	swap	k1gInSc1
messages	messages	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
12	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
říjen	říjen	k1gInSc1
20111	#num#	k4
2	#num#	k4
World	Worlda	k1gFnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
leading	leading	k1gInSc1
mobile	mobile	k1gNnSc4
operators	operators	k6eAd1
announce	announko	k6eAd1
commitment	commitment	k1gMnSc1
to	ten	k3xDgNnSc1
NFC	NFC	kA
technology	technolog	k1gMnPc4
<g/>
,	,	kIx,
tiskové	tiskový	k2eAgNnSc1d1
prohlášení	prohlášení	k1gNnSc1
GSMA	GSMA	kA
<g/>
,	,	kIx,
firemní	firemní	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
<g/>
,	,	kIx,
21	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc4
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archivováno	archivován	k2eAgNnSc4d1
25	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
GSM	GSM	kA
Asociace	asociace	k1gFnSc2
se	se	k3xPyFc4
zaměřuje	zaměřovat	k5eAaImIp3nS
na	na	k7c4
platby	platba	k1gFnSc2
mobilním	mobilní	k2eAgInSc7d1
telefonem	telefon	k1gInSc7
na	na	k7c6
pokladnách	pokladna	k1gFnPc6
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
Archivováno	archivován	k2eAgNnSc1d1
5	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
GSM	GSM	kA
Association	Association	k1gInSc1
<g/>
,	,	kIx,
13	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc1
2007	#num#	k4
<g/>
↑	↑	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Momentum	Momentum	k1gNnSc1
Builds	Buildsa	k1gFnPc2
Around	Arounda	k1gFnPc2
GSMA	GSMA	kA
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Pay-Buy	Pay-Bu	k1gMnPc7
Mobile	mobile	k1gNnSc2
Project	Project	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
28	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
GSM	GSM	kA
Association	Association	k1gInSc1
<g/>
,	,	kIx,
25	#num#	k4
April	April	k1gInSc1
2007	#num#	k4
<g/>
↑	↑	k?
Ernst	Ernst	k1gMnSc1
Haselsteiner	Haselsteiner	k1gMnSc1
<g/>
,	,	kIx,
Klemens	Klemens	k1gInSc1
Breitfuß	Breitfuß	k1gFnSc1
<g/>
:	:	kIx,
Security	Securit	k1gInPc1
in	in	k?
near	near	k1gInSc1
field	field	k1gMnSc1
communication	communication	k1gInSc1
(	(	kIx(
<g/>
NFC	NFC	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Philips	Philips	kA
Semiconductors	Semiconductors	k1gInSc1
<g/>
,	,	kIx,
Printed	Printed	k1gMnSc1
handout	handout	k1gMnSc1
of	of	k?
Workshop	workshop	k1gInSc1
on	on	k3xPp3gMnSc1
RFID	RFID	kA
Security	Securit	k1gInPc4
RFIDSec	RFIDSec	k1gInSc1
0	#num#	k4
<g/>
6	#num#	k4
<g/>
,	,	kIx,
July	Jula	k1gFnPc1
2006	#num#	k4
<g/>
↑	↑	k?
Lishoy	Lishoa	k1gFnSc2
Francis	Francis	k1gFnSc2
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
<g/>
:	:	kIx,
<g/>
Practical	Practical	k1gMnSc1
Relay	Relaa	k1gFnSc2
Attack	Attack	k1gMnSc1
on	on	k3xPp3gMnSc1
Contactless	Contactless	k1gInSc1
Transactions	Transactions	k1gInSc4
by	by	k9
Using	Using	k1gInSc4
NFC	NFC	kA
Mobile	mobile	k1gNnSc1
Phones	Phones	k1gMnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Zahraniční	zahraniční	k2eAgInPc1d1
zdroje	zdroj	k1gInPc1
</s>
<s>
Near	Near	k1gInSc1
Field	Field	k1gInSc1
Communication	Communication	k1gInSc1
(	(	kIx(
<g/>
NFC	NFC	kA
<g/>
)	)	kIx)
Technology	technolog	k1gMnPc4
and	and	k?
Measurements	Measurements	k1gInSc1
</s>
<s>
ORTIZ	ORTIZ	kA
<g/>
,	,	kIx,
C.	C.	kA
Enrique	Enrique	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
An	An	k1gFnPc2
Introduction	Introduction	k1gInSc4
to	ten	k3xDgNnSc1
Near-Field	Near-Fieldo	k1gNnPc2
Communication	Communication	k1gInSc1
and	and	k?
the	the	k?
Contactless	Contactless	k1gInSc1
Communication	Communication	k1gInSc1
API	API	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sun	suna	k1gFnPc2
<g/>
,	,	kIx,
2006-06	2006-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
COSKUN	COSKUN	kA
<g/>
,	,	kIx,
Vedat	Vedat	k1gInSc1
<g/>
;	;	kIx,
OK	oko	k1gNnPc2
<g/>
,	,	kIx,
Kerem	Kerem	k1gInSc1
<g/>
;	;	kIx,
OZDENIZCI	OZDENIZCI	kA
<g/>
,	,	kIx,
Busra	Busro	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Near	Near	k1gMnSc1
field	field	k1gMnSc1
communication	communication	k1gInSc1
:	:	kIx,
from	from	k6eAd1
theory	theor	k1gInPc4
to	ten	k3xDgNnSc1
practice	practice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc6
Atrium	atrium	k1gNnSc1
<g/>
,	,	kIx,
Southern	Southern	k1gNnSc1
Gate	Gate	k1gInSc1
<g/>
,	,	kIx,
Chichester	Chichester	k1gInSc1
<g/>
,	,	kIx,
West	West	k2eAgInSc1d1
Sussex	Sussex	k1gInSc1
<g/>
,	,	kIx,
PO19	PO19	k1gFnSc1
8	#num#	k4
<g/>
SQ	SQ	kA
<g/>
,	,	kIx,
United	United	k1gInSc1
Kingdom	Kingdom	k1gInSc1
<g/>
:	:	kIx,
John	John	k1gMnSc1
Wiley	Wilea	k1gFnSc2
&	&	k?
Sons	Sons	k1gInSc1
Ltd	ltd	kA
<g/>
,	,	kIx,
Publication	Publication	k1gInSc1
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
797	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
119	#num#	k4
<g/>
-	-	kIx~
<g/>
97109	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Near	Near	k1gInSc1
Field	Field	k1gInSc1
Communication	Communication	k1gInSc1
(	(	kIx(
<g/>
NFC	NFC	kA
<g/>
)	)	kIx)
Technology	technolog	k1gMnPc4
and	and	k?
Measurements	Measurements	k1gInSc4
</s>
<s>
ORTIZ	ORTIZ	kA
<g/>
,	,	kIx,
C.	C.	kA
Enrique	Enrique	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
An	An	k1gFnPc2
Introduction	Introduction	k1gInSc4
to	ten	k3xDgNnSc1
Near-Field	Near-Fieldo	k1gNnPc2
Communication	Communication	k1gInSc1
and	and	k?
the	the	k?
Contactless	Contactless	k1gInSc1
Communication	Communication	k1gInSc1
API	API	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sun	suna	k1gFnPc2
<g/>
,	,	kIx,
2006-06	2006-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
COSKUN	COSKUN	kA
<g/>
,	,	kIx,
Vedat	Vedat	k1gInSc1
<g/>
;	;	kIx,
OK	oko	k1gNnPc2
<g/>
,	,	kIx,
Kerem	Kerem	k1gInSc1
<g/>
;	;	kIx,
OZDENIZCI	OZDENIZCI	kA
<g/>
,	,	kIx,
Busra	Busro	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Near	Near	k1gMnSc1
field	field	k1gMnSc1
communication	communication	k1gInSc1
:	:	kIx,
from	from	k6eAd1
theory	theor	k1gInPc4
to	ten	k3xDgNnSc1
practice	practice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc6
Atrium	atrium	k1gNnSc1
<g/>
,	,	kIx,
Southern	Southern	k1gNnSc1
Gate	Gate	k1gInSc1
<g/>
,	,	kIx,
Chichester	Chichester	k1gInSc1
<g/>
,	,	kIx,
West	West	k2eAgInSc1d1
Sussex	Sussex	k1gInSc1
<g/>
,	,	kIx,
PO19	PO19	k1gFnSc1
8	#num#	k4
<g/>
SQ	SQ	kA
<g/>
,	,	kIx,
United	United	k1gInSc1
Kingdom	Kingdom	k1gInSc1
<g/>
:	:	kIx,
John	John	k1gMnSc1
Wiley	Wilea	k1gFnSc2
&	&	k?
Sons	Sons	k1gInSc1
Ltd	ltd	kA
<g/>
,	,	kIx,
Publication	Publication	k1gInSc1
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
797	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
119	#num#	k4
<g/>
-	-	kIx~
<g/>
97109	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
NFC	NFC	kA
technology	technolog	k1gMnPc7
</s>
<s>
About	About	k1gMnSc1
NFC	NFC	kA
Antennas	Antennas	k1gMnSc1
</s>
<s>
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
18092	#num#	k4
<g/>
:	:	kIx,
<g/>
2004	#num#	k4
</s>
<s>
Mobile	mobile	k1gNnSc1
phones	phonesa	k1gFnPc2
hope	hopat	k5eAaPmIp3nS
to	ten	k3xDgNnSc4
be	be	k?
'	'	kIx"
<g/>
smart	smart	k1gInSc1
wallet	wallet	k1gInSc1
<g/>
'	'	kIx"
</s>
<s>
Preparing	Preparing	k1gInSc1
for	forum	k1gNnPc2
the	the	k?
NFC	NFC	kA
revolution	revolution	k1gInSc1
</s>
<s>
Future	Futur	k1gMnSc5
of	of	k?
Near	Near	k1gMnSc1
Field	Field	k1gMnSc1
</s>
<s>
Near	Near	k1gMnSc1
Field	Field	k1gMnSc1
Communications	Communicationsa	k1gFnPc2
in	in	k?
the	the	k?
security	securita	k1gFnSc2
industry	industra	k1gFnSc2
—	—	k?
Access	Access	k1gInSc1
Control	Control	k1gInSc1
with	witha	k1gFnPc2
mobile	mobile	k1gNnSc1
phones	phones	k1gMnSc1
</s>
<s>
A	a	k9
day	day	k?
at	at	k?
MIT	MIT	kA
</s>
<s>
About	About	k1gMnSc1
NFC	NFC	kA
Antennas	Antennas	k1gMnSc1
</s>
<s>
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
18092	#num#	k4
<g/>
:	:	kIx,
<g/>
2004	#num#	k4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
7678305-4	7678305-4	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
2009007845	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
2009007845	#num#	k4
</s>
