<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Kanady	Kanada	k1gFnSc2	Kanada
(	(	kIx(	(
<g/>
označovaná	označovaný	k2eAgFnSc1d1	označovaná
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
vlajka	vlajka	k1gFnSc1	vlajka
javorového	javorový	k2eAgInSc2d1	javorový
listu	list	k1gInSc2	list
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
červená	červený	k2eAgFnSc1d1	červená
vlajka	vlajka	k1gFnSc1	vlajka
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
výšky	výška	k1gFnSc2	výška
a	a	k8xC	a
délky	délka	k1gFnSc2	délka
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
uprostřed	uprostřed	k6eAd1	uprostřed
svislý	svislý	k2eAgInSc1d1	svislý
bílý	bílý	k2eAgInSc1d1	bílý
pruh	pruh	k1gInSc1	pruh
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
stylizovaným	stylizovaný	k2eAgInSc7d1	stylizovaný
jedenácticípým	jedenácticípý	k2eAgInSc7d1	jedenácticípý
javorovým	javorový	k2eAgInSc7d1	javorový
listem	list	k1gInSc7	list
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
podoba	podoba	k1gFnSc1	podoba
kanadské	kanadský	k2eAgFnSc2d1	kanadská
národní	národní	k2eAgFnSc2d1	národní
vlajky	vlajka	k1gFnSc2	vlajka
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
kanadským	kanadský	k2eAgInSc7d1	kanadský
parlamentem	parlament	k1gInSc7	parlament
dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1964	[number]	k4	1964
a	a	k8xC	a
uzákoněna	uzákoněn	k2eAgFnSc1d1	uzákoněna
královnou	královna	k1gFnSc7	královna
Alžbětou	Alžběta	k1gFnSc7	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
<g/>
Barvy	barva	k1gFnPc1	barva
vlajky	vlajka	k1gFnPc1	vlajka
jsou	být	k5eAaImIp3nP	být
červená	červený	k2eAgFnSc1d1	červená
a	a	k8xC	a
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
jsou	být	k5eAaImIp3nP	být
národními	národní	k2eAgFnPc7d1	národní
barvami	barva	k1gFnPc7	barva
Kanady	Kanada	k1gFnSc2	Kanada
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jimi	on	k3xPp3gFnPc7	on
byly	být	k5eAaImAgFnP	být
prohlášeny	prohlásit	k5eAaPmNgInP	prohlásit
králem	král	k1gMnSc7	král
Jiřím	Jiří	k1gMnSc7	Jiří
V.	V.	kA	V.
na	na	k7c6	na
doporučení	doporučení	k1gNnSc6	doporučení
kanadské	kanadský	k2eAgFnSc2d1	kanadská
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Barvy	barva	k1gFnPc1	barva
jsou	být	k5eAaImIp3nP	být
definovány	definovat	k5eAaBmNgFnP	definovat
kanadskou	kanadský	k2eAgFnSc7d1	kanadská
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
<g/>
Ačkoliv	ačkoliv	k8xS	ačkoliv
javorový	javorový	k2eAgInSc1d1	javorový
list	list	k1gInSc1	list
nebyl	být	k5eNaImAgInS	být
do	do	k7c2	do
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
podoby	podoba	k1gFnSc2	podoba
kanadské	kanadský	k2eAgFnSc2d1	kanadská
vlajky	vlajka	k1gFnSc2	vlajka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
oficiálním	oficiální	k2eAgInSc7d1	oficiální
znakem	znak	k1gInSc7	znak
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
historickým	historický	k2eAgInSc7d1	historický
symbolem	symbol	k1gInSc7	symbol
Kanady	Kanada	k1gFnSc2	Kanada
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
užíván	užívat	k5eAaImNgInS	užívat
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
jako	jako	k8xS	jako
dekorace	dekorace	k1gFnPc1	dekorace
při	při	k7c6	při
kanadské	kanadský	k2eAgFnSc6d1	kanadská
návštěvě	návštěva	k1gFnSc6	návštěva
prince	princ	k1gMnSc2	princ
z	z	k7c2	z
Walesu	Wales	k1gInSc2	Wales
(	(	kIx(	(
<g/>
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
Eduard	Eduard	k1gMnSc1	Eduard
VII	VII	kA	VII
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
jedenácti	jedenáct	k4xCc2	jedenáct
cípů	cíp	k1gInPc2	cíp
javorového	javorový	k2eAgInSc2d1	javorový
listu	list	k1gInSc2	list
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
jim	on	k3xPp3gMnPc3	on
přikládán	přikládat	k5eAaImNgInS	přikládat
žádný	žádný	k3yNgInSc1	žádný
specifický	specifický	k2eAgInSc1d1	specifický
význam	význam	k1gInSc1	význam
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
postranní	postranní	k2eAgInPc1d1	postranní
pruhy	pruh	k1gInPc1	pruh
symbolizují	symbolizovat	k5eAaImIp3nP	symbolizovat
oceány	oceán	k1gInPc4	oceán
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
Kanadu	Kanada	k1gFnSc4	Kanada
omývají	omývat	k5eAaImIp3nP	omývat
-	-	kIx~	-
tedy	tedy	k8xC	tedy
oceán	oceán	k1gInSc1	oceán
Atlantský	atlantský	k2eAgInSc1d1	atlantský
a	a	k8xC	a
oceán	oceán	k1gInSc1	oceán
Tichý	tichý	k2eAgMnSc1d1	tichý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Vlajky	vlajka	k1gFnPc4	vlajka
kanadských	kanadský	k2eAgInPc2d1	kanadský
států	stát	k1gInPc2	stát
a	a	k8xC	a
teritorií	teritorium	k1gNnPc2	teritorium
==	==	k?	==
</s>
</p>
<p>
<s>
Kanada	Kanada	k1gFnSc1	Kanada
je	být	k5eAaImIp3nS	být
federativní	federativní	k2eAgInSc1d1	federativní
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
10	[number]	k4	10
provincií	provincie	k1gFnPc2	provincie
a	a	k8xC	a
3	[number]	k4	3
spolkových	spolkový	k2eAgNnPc2d1	Spolkové
teritorií	teritorium	k1gNnPc2	teritorium
<g/>
.	.	kIx.	.
</s>
<s>
Provincie	provincie	k1gFnPc1	provincie
mají	mít	k5eAaImIp3nP	mít
vysoký	vysoký	k2eAgInSc4d1	vysoký
stupeň	stupeň	k1gInSc4	stupeň
autonomie	autonomie	k1gFnSc2	autonomie
<g/>
,	,	kIx,	,
teritoria	teritorium	k1gNnPc4	teritorium
poněkud	poněkud	k6eAd1	poněkud
menší	malý	k2eAgFnPc1d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
části	část	k1gFnPc1	část
mají	mít	k5eAaImIp3nP	mít
své	svůj	k3xOyFgFnPc4	svůj
vlajky	vlajka	k1gFnPc4	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Kanady	Kanada	k1gFnSc2	Kanada
</s>
</p>
<p>
<s>
Kanadská	kanadský	k2eAgFnSc1d1	kanadská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Kanady	Kanada	k1gFnSc2	Kanada
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kanadská	kanadský	k2eAgFnSc1d1	kanadská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Department	department	k1gInSc1	department
of	of	k?	of
Canadian	Canadian	k1gInSc1	Canadian
Heritage	Heritag	k1gMnSc2	Heritag
<g/>
,	,	kIx,	,
The	The	k1gMnSc2	The
National	National	k1gMnSc2	National
Flag	flag	k1gInSc1	flag
of	of	k?	of
Canada	Canada	k1gFnSc1	Canada
</s>
</p>
