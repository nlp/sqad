<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Kanady	Kanada	k1gFnSc2	Kanada
(	(	kIx(	(
<g/>
označovaná	označovaný	k2eAgFnSc1d1	označovaná
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
vlajka	vlajka	k1gFnSc1	vlajka
javorového	javorový	k2eAgInSc2d1	javorový
listu	list	k1gInSc2	list
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
červená	červený	k2eAgFnSc1d1	červená
vlajka	vlajka	k1gFnSc1	vlajka
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
výšky	výška	k1gFnSc2	výška
a	a	k8xC	a
délky	délka	k1gFnSc2	délka
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
uprostřed	uprostřed	k6eAd1	uprostřed
svislý	svislý	k2eAgInSc1d1	svislý
bílý	bílý	k2eAgInSc1d1	bílý
pruh	pruh	k1gInSc1	pruh
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
stylizovaným	stylizovaný	k2eAgInSc7d1	stylizovaný
jedenácticípým	jedenácticípý	k2eAgInSc7d1	jedenácticípý
javorovým	javorový	k2eAgInSc7d1	javorový
listem	list	k1gInSc7	list
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
.	.	kIx.	.
</s>
