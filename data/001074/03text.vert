<s>
Libye	Libye	k1gFnSc1	Libye
je	být	k5eAaImIp3nS	být
arabský	arabský	k2eAgInSc1d1	arabský
stát	stát	k1gInSc1	stát
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
u	u	k7c2	u
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Jejími	její	k3xOp3gMnPc7	její
sousedy	soused	k1gMnPc7	soused
jsou	být	k5eAaImIp3nP	být
Egypt	Egypt	k1gInSc1	Egypt
<g/>
,	,	kIx,	,
Súdán	Súdán	k1gInSc1	Súdán
<g/>
,	,	kIx,	,
Čad	Čad	k1gInSc1	Čad
<g/>
,	,	kIx,	,
Niger	Niger	k1gInSc1	Niger
<g/>
,	,	kIx,	,
Alžírsko	Alžírsko	k1gNnSc1	Alžírsko
a	a	k8xC	a
Tunisko	Tunisko	k1gNnSc1	Tunisko
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
státu	stát	k1gInSc2	stát
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
ve	v	k7c6	v
starořeckém	starořecký	k2eAgInSc6d1	starořecký
mýtu	mýtus	k1gInSc6	mýtus
o	o	k7c6	o
Libyi	Libye	k1gFnSc6	Libye
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
zde	zde	k6eAd1	zde
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
mezi	mezi	k7c7	mezi
stoupenci	stoupenec	k1gMnPc7	stoupenec
Kaddáfího	Kaddáfí	k1gMnSc2	Kaddáfí
džamáhíríje	džamáhíríj	k1gMnSc2	džamáhíríj
a	a	k8xC	a
povstalci	povstalec	k1gMnPc7	povstalec
zastupovanými	zastupovaný	k2eAgMnPc7d1	zastupovaný
Národní	národní	k2eAgFnSc7d1	národní
přechodnou	přechodný	k2eAgFnSc7d1	přechodná
radou	rada	k1gFnSc7	rada
a	a	k8xC	a
podporovanými	podporovaný	k2eAgInPc7d1	podporovaný
NATO	nato	k6eAd1	nato
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
skončila	skončit	k5eAaPmAgFnS	skončit
vítězstvím	vítězství	k1gNnSc7	vítězství
povstalců	povstalec	k1gMnPc2	povstalec
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Leptis	Leptis	k1gFnSc2	Leptis
Magna	Magn	k1gMnSc2	Magn
patřilo	patřit	k5eAaImAgNnS	patřit
k	k	k7c3	k
významným	významný	k2eAgInPc3d1	významný
obchodním	obchodní	k2eAgInPc3d1	obchodní
centrům	centr	k1gInPc3	centr
Římského	římský	k2eAgNnSc2d1	římské
impéria	impérium	k1gNnSc2	impérium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
oblast	oblast	k1gFnSc4	oblast
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Libye	Libye	k1gFnSc2	Libye
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Arabové	Arab	k1gMnPc1	Arab
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1551	[number]	k4	1551
Osmanští	osmanský	k2eAgMnPc1d1	osmanský
Turci	Turek	k1gMnPc1	Turek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1911	[number]	k4	1911
<g/>
-	-	kIx~	-
<g/>
1943	[number]	k4	1943
byla	být	k5eAaImAgFnS	být
Libye	Libye	k1gFnSc2	Libye
italskou	italský	k2eAgFnSc7d1	italská
kolonií	kolonie	k1gFnSc7	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Itálie	Itálie	k1gFnSc1	Itálie
tuto	tento	k3xDgFnSc4	tento
bývalou	bývalý	k2eAgFnSc4d1	bývalá
osmanskou	osmanský	k2eAgFnSc4d1	Osmanská
provincii	provincie	k1gFnSc4	provincie
získala	získat	k5eAaPmAgFnS	získat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Smlouvy	smlouva	k1gFnPc1	smlouva
z	z	k7c2	z
Lausanne	Lausanne	k1gNnSc2	Lausanne
po	po	k7c6	po
vyhrané	vyhraný	k2eAgFnSc6d1	vyhraná
italsko-turecké	italskourecký	k2eAgFnSc6d1	italsko-turecký
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byli	být	k5eAaImAgMnP	být
Turci	Turek	k1gMnPc1	Turek
<g/>
,	,	kIx,	,
tlačeni	tlačen	k2eAgMnPc1d1	tlačen
hrozícím	hrozící	k2eAgInSc7d1	hrozící
konfliktem	konflikt	k1gInSc7	konflikt
s	s	k7c7	s
balkánskými	balkánský	k2eAgInPc7d1	balkánský
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
donuceni	donutit	k5eAaPmNgMnP	donutit
podepsat	podepsat	k5eAaPmF	podepsat
s	s	k7c7	s
Itálií	Itálie	k1gFnSc7	Itálie
mírovou	mírový	k2eAgFnSc4d1	mírová
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
této	tento	k3xDgFnSc2	tento
smlouvy	smlouva	k1gFnSc2	smlouva
vyklidili	vyklidit	k5eAaPmAgMnP	vyklidit
Tripolsko	Tripolsko	k1gNnSc4	Tripolsko
a	a	k8xC	a
Kyrenaiku	Kyrenaika	k1gFnSc4	Kyrenaika
a	a	k8xC	a
předali	předat	k5eAaPmAgMnP	předat
ji	on	k3xPp3gFnSc4	on
pod	pod	k7c4	pod
správu	správa	k1gFnSc4	správa
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Kyrenaiku	Kyrenaika	k1gFnSc4	Kyrenaika
se	se	k3xPyFc4	se
však	však	k9	však
Italům	Ital	k1gMnPc3	Ital
nepodařilo	podařit	k5eNaPmAgNnS	podařit
zcela	zcela	k6eAd1	zcela
obsadit	obsadit	k5eAaPmF	obsadit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
požívala	požívat	k5eAaImAgFnS	požívat
značné	značný	k2eAgFnPc4d1	značná
autonomie	autonomie	k1gFnPc4	autonomie
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
italské	italský	k2eAgFnSc2d1	italská
koloniální	koloniální	k2eAgFnSc2d1	koloniální
říše	říš	k1gFnSc2	říš
(	(	kIx(	(
<g/>
vlastní	vlastní	k2eAgInSc1d1	vlastní
parlament	parlament	k1gInSc1	parlament
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
po	po	k7c6	po
další	další	k2eAgFnSc6d1	další
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1922	[number]	k4	1922
<g/>
-	-	kIx~	-
<g/>
1928	[number]	k4	1928
byla	být	k5eAaImAgFnS	být
Kyrenaika	Kyrenaika	k1gFnSc1	Kyrenaika
zcela	zcela	k6eAd1	zcela
podrobena	podrobit	k5eAaPmNgFnS	podrobit
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
spojena	spojen	k2eAgFnSc1d1	spojena
s	s	k7c7	s
Tripolskem	Tripolsko	k1gNnSc7	Tripolsko
a	a	k8xC	a
Fezzánem	Fezzán	k1gInSc7	Fezzán
<g/>
.	.	kIx.	.
</s>
<s>
Itálie	Itálie	k1gFnSc1	Itálie
musela	muset	k5eAaImAgFnS	muset
poté	poté	k6eAd1	poté
čelit	čelit	k5eAaImF	čelit
tuhému	tuhý	k2eAgInSc3d1	tuhý
odporu	odpor	k1gInSc3	odpor
beduínských	beduínský	k2eAgMnPc2d1	beduínský
povstalců	povstalec	k1gMnPc2	povstalec
<g/>
,	,	kIx,	,
zesílenému	zesílený	k2eAgInSc3d1	zesílený
v	v	k7c6	v
průběhu	průběh	k1gInSc2	průběh
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byli	být	k5eAaImAgMnP	být
zásobováni	zásobován	k2eAgMnPc1d1	zásobován
dodávkami	dodávka	k1gFnPc7	dodávka
zbraní	zbraň	k1gFnPc2	zbraň
od	od	k7c2	od
Centrálních	centrální	k2eAgFnPc2d1	centrální
mocností	mocnost	k1gFnPc2	mocnost
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
odpor	odpor	k1gInSc1	odpor
byl	být	k5eAaImAgInS	být
zlomen	zlomen	k2eAgInSc1d1	zlomen
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
dobytím	dobytí	k1gNnSc7	dobytí
oázy	oáza	k1gFnSc2	oáza
Kufra	Kufr	k1gInSc2	Kufr
<g/>
.	.	kIx.	.
</s>
<s>
Italská	italský	k2eAgFnSc1d1	italská
vláda	vláda	k1gFnSc1	vláda
poskytovala	poskytovat	k5eAaImAgFnS	poskytovat
kolonistům	kolonista	k1gMnPc3	kolonista
velké	velký	k2eAgFnSc2d1	velká
subvence	subvence	k1gFnSc2	subvence
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
jich	on	k3xPp3gMnPc2	on
v	v	k7c6	v
Libyi	Libye	k1gFnSc6	Libye
žilo	žít	k5eAaImAgNnS	žít
přibližně	přibližně	k6eAd1	přibližně
110	[number]	k4	110
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
se	se	k3xPyFc4	se
pobřežní	pobřežní	k1gMnSc1	pobřežní
části	část	k1gFnSc2	část
Kyrenaiky	Kyrenaika	k1gFnSc2	Kyrenaika
i	i	k8xC	i
Tripolska	Tripolsko	k1gNnPc1	Tripolsko
staly	stát	k5eAaPmAgFnP	stát
integrální	integrální	k2eAgFnSc7d1	integrální
součástí	součást	k1gFnSc7	součást
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
provincie	provincie	k1gFnSc1	provincie
Libyjská	libyjský	k2eAgFnSc1d1	Libyjská
Sahara	Sahara	k1gFnSc1	Sahara
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc4	území
Libye	Libye	k1gFnSc2	Libye
svědkem	svědek	k1gMnSc7	svědek
rozsáhlých	rozsáhlý	k2eAgFnPc2d1	rozsáhlá
válečných	válečný	k2eAgFnPc2d1	válečná
operací	operace	k1gFnPc2	operace
<g/>
.	.	kIx.	.
</s>
<s>
Válčící	válčící	k2eAgFnPc1d1	válčící
strany	strana	k1gFnPc1	strana
(	(	kIx(	(
<g/>
Němci	Němec	k1gMnPc1	Němec
<g/>
,	,	kIx,	,
Italové	Ital	k1gMnPc1	Ital
a	a	k8xC	a
Britové	Brit	k1gMnPc1	Brit
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
střídali	střídat	k5eAaImAgMnP	střídat
v	v	k7c6	v
kontrole	kontrola	k1gFnSc6	kontrola
nad	nad	k7c7	nad
touto	tento	k3xDgFnSc7	tento
oblastí	oblast	k1gFnSc7	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
se	se	k3xPyFc4	se
Britům	Brit	k1gMnPc3	Brit
podařilo	podařit	k5eAaPmAgNnS	podařit
definitivně	definitivně	k6eAd1	definitivně
vytlačit	vytlačit	k5eAaPmF	vytlačit
italsko-německé	italskoěmecký	k2eAgFnPc4d1	italsko-německý
jednotky	jednotka	k1gFnPc4	jednotka
z	z	k7c2	z
Libye	Libye	k1gFnSc2	Libye
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
zbytek	zbytek	k1gInSc4	zbytek
války	válka	k1gFnSc2	válka
a	a	k8xC	a
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
byly	být	k5eAaImAgFnP	být
Tripolsko	Tripolsko	k1gNnSc4	Tripolsko
a	a	k8xC	a
Kyrenaika	Kyrenaika	k1gFnSc1	Kyrenaika
spravovány	spravován	k2eAgInPc1d1	spravován
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
bývalé	bývalý	k2eAgInPc1d1	bývalý
Italské	italský	k2eAgInPc1d1	italský
severní	severní	k2eAgInPc1d1	severní
afriky	afrik	k1gInPc1	afrik
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
historické	historický	k2eAgNnSc4d1	historické
regiony	region	k1gInPc1	region
Tripolsko	Tripolsko	k1gNnSc1	Tripolsko
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
Britů	Brit	k1gMnPc2	Brit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kyrenaika	Kyrenaika	k1gFnSc1	Kyrenaika
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
Britů	Brit	k1gMnPc2	Brit
<g/>
)	)	kIx)	)
a	a	k8xC	a
Fazzā	Fazzā	k1gMnSc1	Fazzā
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
Francouzů	Francouz	k1gMnPc2	Francouz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
k	k	k7c3	k
rozsáhlým	rozsáhlý	k2eAgMnPc3d1	rozsáhlý
nepokojům	nepokoj	k1gInPc3	nepokoj
<g/>
,	,	kIx,	,
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
OSN	OSN	kA	OSN
o	o	k7c6	o
plném	plný	k2eAgNnSc6d1	plné
osamostatnění	osamostatnění	k1gNnSc6	osamostatnění
Libye	Libye	k1gFnSc2	Libye
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
bylo	být	k5eAaImAgNnS	být
naplněno	naplnit	k5eAaPmNgNnS	naplnit
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
spojením	spojení	k1gNnSc7	spojení
Fezzánu	Fezzán	k2eAgFnSc4d1	Fezzán
<g/>
,	,	kIx,	,
Tripolska	Tripolsko	k1gNnSc2	Tripolsko
a	a	k8xC	a
Kyrenaiky	Kyrenaika	k1gFnSc2	Kyrenaika
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
nezávislý	závislý	k2eNgInSc1d1	nezávislý
federativní	federativní	k2eAgInSc1d1	federativní
stát	stát	k1gInSc1	stát
Libye	Libye	k1gFnSc2	Libye
jako	jako	k8xC	jako
Libyjské	libyjský	k2eAgNnSc1d1	Libyjské
království	království	k1gNnSc1	království
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
krále	král	k1gMnSc2	král
Idrise	Idrise	k1gFnSc2	Idrise
I.	I.	kA	I.
emíra	emír	k1gMnSc2	emír
Kyrenaiky	Kyrenaika	k1gFnSc2	Kyrenaika
a	a	k8xC	a
Tripolska	Tripolsko	k1gNnSc2	Tripolsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
byl	být	k5eAaImAgMnS	být
za	za	k7c2	za
autoritativní	autoritativní	k2eAgFnSc2d1	autoritativní
vlády	vláda	k1gFnSc2	vláda
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
federativní	federativní	k2eAgNnSc1d1	federativní
členění	členění	k1gNnSc1	členění
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
vojenský	vojenský	k2eAgInSc1d1	vojenský
puč	puč	k1gInSc1	puč
vedený	vedený	k2eAgInSc1d1	vedený
skupinou	skupina	k1gFnSc7	skupina
plukovníků	plukovník	k1gMnPc2	plukovník
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Muammara	Muammar	k1gMnSc2	Muammar
Kaddáfího	Kaddáfí	k1gMnSc2	Kaddáfí
<g/>
.	.	kIx.	.
</s>
<s>
Libye	Libye	k1gFnSc1	Libye
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
postupně	postupně	k6eAd1	postupně
začala	začít	k5eAaPmAgFnS	začít
usilovat	usilovat	k5eAaImF	usilovat
o	o	k7c4	o
pozici	pozice	k1gFnSc4	pozice
regionální	regionální	k2eAgFnSc2d1	regionální
mocnosti	mocnost	k1gFnSc2	mocnost
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
země	zem	k1gFnSc2	zem
byly	být	k5eAaImAgFnP	být
vykázány	vykázán	k2eAgFnPc1d1	vykázána
americké	americký	k2eAgFnPc1d1	americká
a	a	k8xC	a
britské	britský	k2eAgFnPc1d1	britská
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
,	,	kIx,	,
znárodněny	znárodněn	k2eAgFnPc1d1	znárodněna
zahraniční	zahraniční	k2eAgFnPc4d1	zahraniční
ropné	ropný	k2eAgFnPc4d1	ropná
společnosti	společnost	k1gFnPc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Muammar	Muammar	k1gMnSc1	Muammar
Kaddáfí	Kaddáfí	k1gMnSc1	Kaddáfí
začal	začít	k5eAaPmAgMnS	začít
podporovat	podporovat	k5eAaImF	podporovat
povstalecké	povstalecký	k2eAgFnPc4d1	povstalecká
skupiny	skupina	k1gFnPc4	skupina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
Afriky	Afrika	k1gFnSc2	Afrika
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
i	i	k9	i
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
)	)	kIx)	)
ohrožovaly	ohrožovat	k5eAaImAgFnP	ohrožovat
tamní	tamní	k2eAgFnPc1d1	tamní
vlády	vláda	k1gFnPc1	vláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
navíc	navíc	k6eAd1	navíc
Libye	Libye	k1gFnSc1	Libye
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
tlak	tlak	k1gInSc4	tlak
a	a	k8xC	a
následně	následně	k6eAd1	následně
i	i	k9	i
vojensky	vojensky	k6eAd1	vojensky
intervenovala	intervenovat	k5eAaImAgFnS	intervenovat
v	v	k7c6	v
sousedním	sousední	k2eAgInSc6d1	sousední
Čadu	Čad	k1gInSc6	Čad
<g/>
,	,	kIx,	,
bývalé	bývalý	k2eAgFnSc3d1	bývalá
francouzské	francouzský	k2eAgFnSc3d1	francouzská
kolonii	kolonie	k1gFnSc3	kolonie
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
na	na	k7c4	na
což	což	k3yQnSc4	což
Francie	Francie	k1gFnSc1	Francie
odpověděla	odpovědět	k5eAaPmAgFnS	odpovědět
vojenským	vojenský	k2eAgInSc7d1	vojenský
zásahem	zásah	k1gInSc7	zásah
<g/>
.	.	kIx.	.
</s>
<s>
Francouzské	francouzský	k2eAgFnPc1d1	francouzská
jednotky	jednotka	k1gFnPc1	jednotka
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
přímo	přímo	k6eAd1	přímo
střetly	střetnout	k5eAaPmAgFnP	střetnout
s	s	k7c7	s
Libyjci	Libyjec	k1gMnPc7	Libyjec
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
operace	operace	k1gFnSc2	operace
Épervier	Épervira	k1gFnPc2	Épervira
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
zničila	zničit	k5eAaPmAgFnS	zničit
libyjské	libyjský	k2eAgNnSc4d1	Libyjské
radarové	radarový	k2eAgNnSc4d1	radarové
zařízení	zařízení	k1gNnSc4	zařízení
a	a	k8xC	a
pomohla	pomoct	k5eAaPmAgFnS	pomoct
čadskému	čadský	k2eAgMnSc3d1	čadský
prezidentovi	prezident	k1gMnSc3	prezident
Habrému	Habrý	k2eAgInSc3d1	Habrý
dobýt	dobýt	k5eAaPmF	dobýt
okupovaný	okupovaný	k2eAgInSc4d1	okupovaný
sever	sever	k1gInSc4	sever
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1972	[number]	k4	1972
Kaddáfí	Kaddáfí	k1gMnSc1	Kaddáfí
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kterýkoliv	kterýkoliv	k3yIgMnSc1	kterýkoliv
Arab	Arab	k1gMnSc1	Arab
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
přeje	přát	k5eAaImIp3nS	přát
se	se	k3xPyFc4	se
dobrovolně	dobrovolně	k6eAd1	dobrovolně
přidat	přidat	k5eAaPmF	přidat
k	k	k7c3	k
palestinským	palestinský	k2eAgFnPc3d1	palestinská
ozbrojeným	ozbrojený	k2eAgFnPc3d1	ozbrojená
skupinám	skupina	k1gFnPc3	skupina
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
může	moct	k5eAaImIp3nS	moct
zaregistrovat	zaregistrovat	k5eAaPmF	zaregistrovat
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
na	na	k7c6	na
kterémkoliv	kterýkoliv	k3yIgNnSc6	kterýkoliv
libyjském	libyjský	k2eAgNnSc6d1	Libyjské
velvyslanectví	velvyslanectví	k1gNnSc6	velvyslanectví
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
mu	on	k3xPp3gMnSc3	on
zajistí	zajistit	k5eAaPmIp3nS	zajistit
potřebný	potřebný	k2eAgInSc1d1	potřebný
bojový	bojový	k2eAgInSc1d1	bojový
výcvik	výcvik	k1gInSc1	výcvik
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
přislíbil	přislíbit	k5eAaPmAgInS	přislíbit
finanční	finanční	k2eAgFnSc4d1	finanční
pomoc	pomoc	k1gFnSc4	pomoc
pro	pro	k7c4	pro
útoky	útok	k1gInPc4	útok
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
založil	založit	k5eAaPmAgMnS	založit
Islámskou	islámský	k2eAgFnSc4d1	islámská
legii	legie	k1gFnSc4	legie
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
-	-	kIx~	-
<g/>
87	[number]	k4	87
<g/>
)	)	kIx)	)
-	-	kIx~	-
žoldnéřskou	žoldnéřský	k2eAgFnSc4d1	žoldnéřská
paravojenskou	paravojenský	k2eAgFnSc4d1	paravojenská
skupinu	skupina	k1gFnSc4	skupina
<g/>
,	,	kIx,	,
mající	mající	k2eAgInSc4d1	mající
napomoci	napomoct	k5eAaPmF	napomoct
vytvořit	vytvořit	k5eAaPmF	vytvořit
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Sahelu	Sahela	k1gFnSc4	Sahela
jeden	jeden	k4xCgInSc1	jeden
velký	velký	k2eAgInSc1d1	velký
islámský	islámský	k2eAgInSc1d1	islámský
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Libye	Libye	k1gFnSc1	Libye
hrála	hrát	k5eAaImAgFnS	hrát
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
roli	role	k1gFnSc4	role
při	při	k7c6	při
podpoře	podpora	k1gFnSc6	podpora
ropného	ropný	k2eAgNnSc2d1	ropné
embarga	embargo	k1gNnSc2	embargo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
jako	jako	k8xS	jako
politické	politický	k2eAgFnPc1d1	politická
zbraně	zbraň	k1gFnPc1	zbraň
v	v	k7c6	v
naději	naděje	k1gFnSc6	naděje
<g/>
,	,	kIx,	,
že	že	k8xS	že
cena	cena	k1gFnSc1	cena
ropy	ropa	k1gFnSc2	ropa
vzroste	vzrůst	k5eAaPmIp3nS	vzrůst
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
přinutí	přinutit	k5eAaPmIp3nS	přinutit
Západ	západ	k1gInSc1	západ
zastavit	zastavit	k5eAaPmF	zastavit
podporu	podpora	k1gFnSc4	podpora
Izraele	Izrael	k1gInSc2	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Opakovaně	opakovaně	k6eAd1	opakovaně
byly	být	k5eAaImAgFnP	být
zasílány	zasílán	k2eAgFnPc4d1	zasílána
zbraně	zbraň	k1gFnPc4	zbraň
různým	různý	k2eAgFnPc3d1	různá
teroristickým	teroristický	k2eAgFnPc3d1	teroristická
a	a	k8xC	a
národně-osvobozeneckým	národněsvobozenecký	k2eAgFnPc3d1	národně-osvobozenecký
skupinám	skupina	k1gFnPc3	skupina
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
zbraně	zbraň	k1gFnPc1	zbraň
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
bomby	bomba	k1gFnPc4	bomba
působící	působící	k2eAgFnSc2d1	působící
křeče	křeč	k1gFnSc2	křeč
a	a	k8xC	a
lámající	lámající	k2eAgFnPc1d1	lámající
ducha	duch	k1gMnSc2	duch
Británie	Británie	k1gFnSc2	Británie
jsou	být	k5eAaImIp3nP	být
bombami	bomba	k1gFnPc7	bomba
libyjského	libyjský	k2eAgInSc2d1	libyjský
lidu	lid	k1gInSc2	lid
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
zachytilo	zachytit	k5eAaPmAgNnS	zachytit
irské	irský	k2eAgNnSc1d1	irské
námořnictvo	námořnictvo	k1gNnSc1	námořnictvo
loď	loď	k1gFnSc1	loď
Claudia	Claudia	k1gFnSc1	Claudia
dopravující	dopravující	k2eAgFnPc4d1	dopravující
sovětské	sovětský	k2eAgFnPc4d1	sovětská
zbraně	zbraň	k1gFnPc4	zbraň
z	z	k7c2	z
Libye	Libye	k1gFnSc2	Libye
do	do	k7c2	do
Severního	severní	k2eAgNnSc2d1	severní
Irska	Irsko	k1gNnSc2	Irsko
pro	pro	k7c4	pro
Prozatímní	prozatímní	k2eAgFnSc4d1	prozatímní
irskou	irský	k2eAgFnSc4d1	irská
republikánskou	republikánský	k2eAgFnSc4d1	republikánská
armádu	armáda	k1gFnSc4	armáda
(	(	kIx(	(
<g/>
PIRA	PIRA	kA	PIRA
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
o	o	k7c4	o
14	[number]	k4	14
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
francouzské	francouzský	k2eAgInPc1d1	francouzský
úřady	úřad	k1gInPc1	úřad
zastavily	zastavit	k5eAaPmAgInP	zastavit
loď	loď	k1gFnSc4	loď
MV	MV	kA	MV
Eksund	Eksunda	k1gFnPc2	Eksunda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dovážela	dovážet	k5eAaImAgFnS	dovážet
přes	přes	k7c4	přes
120	[number]	k4	120
tun	tuna	k1gFnPc2	tuna
libyjských	libyjský	k2eAgFnPc2d1	Libyjská
zbraní	zbraň	k1gFnPc2	zbraň
teroristickým	teroristický	k2eAgFnPc3d1	teroristická
skupinám	skupina	k1gFnPc3	skupina
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
IRA	Ir	k1gMnSc4	Ir
a	a	k8xC	a
PIRA	PIRA	kA	PIRA
<g/>
.	.	kIx.	.
</s>
<s>
Kaddáfí	Kaddáfí	k1gMnSc1	Kaddáfí
byl	být	k5eAaImAgMnS	být
těsným	těsný	k2eAgMnSc7d1	těsný
stoupencem	stoupenec	k1gMnSc7	stoupenec
ugandského	ugandský	k2eAgMnSc2d1	ugandský
diktátora	diktátor	k1gMnSc2	diktátor
Idiho	Idi	k1gMnSc2	Idi
Amina	Amin	k1gMnSc2	Amin
<g/>
.	.	kIx.	.
</s>
<s>
Libyjské	libyjský	k2eAgFnPc1d1	Libyjská
jednotky	jednotka	k1gFnPc1	jednotka
byly	být	k5eAaImAgFnP	být
poslány	poslat	k5eAaPmNgInP	poslat
do	do	k7c2	do
Aminovy	Aminovy	k?	Aminovy
války	válka	k1gFnSc2	válka
proti	proti	k7c3	proti
Tanzánii	Tanzánie	k1gFnSc3	Tanzánie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
padlo	padnout	k5eAaPmAgNnS	padnout
na	na	k7c4	na
600	[number]	k4	600
libyjských	libyjský	k2eAgMnPc2d1	libyjský
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Kaddáfí	Kaddáfí	k1gMnSc1	Kaddáfí
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
také	také	k6eAd1	také
středoafrickému	středoafrický	k2eAgMnSc3d1	středoafrický
diktátorovi	diktátor	k1gMnSc3	diktátor
Jean-Bédel	Jean-Bédlo	k1gNnPc2	Jean-Bédlo
Bokassovi	Bokassův	k2eAgMnPc5d1	Bokassův
<g/>
.	.	kIx.	.
</s>
<s>
Podporoval	podporovat	k5eAaImAgMnS	podporovat
i	i	k9	i
etiopského	etiopský	k2eAgMnSc4d1	etiopský
levicového	levicový	k2eAgMnSc4d1	levicový
diktátora	diktátor	k1gMnSc4	diktátor
Haile	Hail	k1gMnSc4	Hail
Mariama	Mariama	k?	Mariama
Mengistua	Mengistuus	k1gMnSc4	Mengistuus
<g/>
;	;	kIx,	;
dále	daleko	k6eAd2	daleko
cvičil	cvičit	k5eAaImAgInS	cvičit
a	a	k8xC	a
podporoval	podporovat	k5eAaImAgInS	podporovat
Charlese	Charles	k1gMnSc4	Charles
Taylora	Taylor	k1gMnSc4	Taylor
<g/>
,	,	kIx,	,
prezidenta	prezident	k1gMnSc4	prezident
Libérie	Libérie	k1gFnSc2	Libérie
<g/>
,	,	kIx,	,
obviněného	obviněný	k1gMnSc2	obviněný
za	za	k7c4	za
válečné	válečný	k2eAgInPc4d1	válečný
zločiny	zločin	k1gInPc4	zločin
a	a	k8xC	a
zločiny	zločin	k1gInPc4	zločin
proti	proti	k7c3	proti
lidskosti	lidskost	k1gFnSc3	lidskost
v	v	k7c6	v
Sierra	Sierra	k1gFnSc1	Sierra
Leone	Leo	k1gMnSc5	Leo
<g/>
.	.	kIx.	.
</s>
<s>
Libye	Libye	k1gFnSc1	Libye
měla	mít	k5eAaImAgFnS	mít
rovněž	rovněž	k9	rovněž
úzké	úzký	k2eAgInPc4d1	úzký
kontakty	kontakt	k1gInPc4	kontakt
s	s	k7c7	s
režimem	režim	k1gInSc7	režim
Slobodana	Slobodan	k1gMnSc2	Slobodan
Miloševiče	Miloševič	k1gMnSc2	Miloševič
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1984	[number]	k4	1984
protestovali	protestovat	k5eAaBmAgMnP	protestovat
libyjští	libyjský	k2eAgMnPc1d1	libyjský
uprchlíci	uprchlík	k1gMnPc1	uprchlík
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
proti	proti	k7c3	proti
popravě	poprava	k1gFnSc6	poprava
dvou	dva	k4xCgMnPc2	dva
disidentů	disident	k1gMnPc2	disident
<g/>
.	.	kIx.	.
</s>
<s>
Libyjští	libyjský	k2eAgMnPc1d1	libyjský
diplomaté	diplomat	k1gMnPc1	diplomat
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
zastřelili	zastřelit	k5eAaPmAgMnP	zastřelit
11	[number]	k4	11
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
britskou	britský	k2eAgFnSc4d1	britská
policistku	policistka	k1gFnSc4	policistka
<g/>
.	.	kIx.	.
</s>
<s>
Incident	incident	k1gInSc1	incident
k	k	k7c3	k
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
ukončení	ukončení	k1gNnSc3	ukončení
diplomatických	diplomatický	k2eAgInPc2d1	diplomatický
vztahů	vztah	k1gInPc2	vztah
mezi	mezi	k7c7	mezi
Spojeným	spojený	k2eAgNnSc7d1	spojené
královstvím	království	k1gNnSc7	království
a	a	k8xC	a
Libyí	Libye	k1gFnSc7	Libye
na	na	k7c6	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
později	pozdě	k6eAd2	pozdě
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
Kaddáfí	Kaddáfí	k1gFnSc4	Kaddáfí
<g/>
,	,	kIx,	,
že	že	k8xS	že
očekává	očekávat	k5eAaImIp3nS	očekávat
od	od	k7c2	od
svých	svůj	k3xOyFgMnPc2	svůj
agentů	agent	k1gMnPc2	agent
likvidaci	likvidace	k1gFnSc4	likvidace
uprchlých	uprchlý	k2eAgMnPc2d1	uprchlý
disidentů	disident	k1gMnPc2	disident
i	i	k8xC	i
během	během	k7c2	během
jejich	jejich	k3xOp3gFnSc2	jejich
pouti	pouť	k1gFnSc2	pouť
do	do	k7c2	do
Mekky	Mekka	k1gFnSc2	Mekka
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
Libyjské	libyjský	k2eAgNnSc1d1	Libyjské
spiknutí	spiknutí	k1gNnSc1	spiknutí
bylo	být	k5eAaImAgNnS	být
skutečně	skutečně	k6eAd1	skutečně
v	v	k7c6	v
Saúdské	saúdský	k2eAgFnSc6d1	Saúdská
Arábii	Arábie	k1gFnSc6	Arábie
odhaleno	odhalen	k2eAgNnSc1d1	odhaleno
a	a	k8xC	a
zneškodněno	zneškodněn	k2eAgNnSc1d1	zneškodněno
saúdskoarabskou	saúdskoarabský	k2eAgFnSc7d1	saúdskoarabská
policií	policie	k1gFnSc7	policie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
prosincových	prosincový	k2eAgInPc6d1	prosincový
bombových	bombový	k2eAgInPc6d1	bombový
útocích	útok	k1gInPc6	útok
na	na	k7c4	na
letiště	letiště	k1gNnSc4	letiště
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
a	a	k8xC	a
Vídni	Vídeň	k1gFnSc6	Vídeň
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yIgInPc6	který
bylo	být	k5eAaImAgNnS	být
19	[number]	k4	19
lidí	člověk	k1gMnPc2	člověk
zabito	zabít	k5eAaPmNgNnS	zabít
a	a	k8xC	a
kolem	kolem	k7c2	kolem
140	[number]	k4	140
zraněno	zraněn	k2eAgNnSc4d1	zraněno
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
Kaddáfí	Kaddáfí	k2eAgMnSc1d1	Kaddáfí
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
podpoře	podpora	k1gFnSc6	podpora
RAF	raf	k0	raf
<g/>
,	,	kIx,	,
Rudých	rudý	k2eAgFnPc2d1	rudá
brigád	brigáda	k1gFnPc2	brigáda
a	a	k8xC	a
IRA	Ir	k1gMnSc2	Ir
tak	tak	k9	tak
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
západoevropské	západoevropský	k2eAgFnPc1d1	západoevropská
země	zem	k1gFnPc1	zem
budou	být	k5eAaImBp3nP	být
podporovat	podporovat	k5eAaImF	podporovat
protikaddáfíovsky	protikaddáfíovsky	k6eAd1	protikaddáfíovsky
orientované	orientovaný	k2eAgMnPc4d1	orientovaný
Libyjce	Libyjec	k1gMnPc4	Libyjec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
oznámila	oznámit	k5eAaPmAgFnS	oznámit
libyjská	libyjský	k2eAgFnSc1d1	Libyjská
státní	státní	k2eAgFnSc1d1	státní
televize	televize	k1gFnSc1	televize
<g/>
,	,	kIx,	,
že	že	k8xS	že
Libye	Libye	k1gFnSc1	Libye
cvičí	cvičit	k5eAaImIp3nP	cvičit
sebevražedné	sebevražedný	k2eAgFnPc4d1	sebevražedná
jednotky	jednotka	k1gFnPc4	jednotka
k	k	k7c3	k
útokům	útok	k1gInPc3	útok
proti	proti	k7c3	proti
evropským	evropský	k2eAgInPc3d1	evropský
a	a	k8xC	a
americkým	americký	k2eAgInPc3d1	americký
zájmům	zájem	k1gInPc3	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Libyjští	libyjský	k2eAgMnPc1d1	libyjský
agenti	agent	k1gMnPc1	agent
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1986	[number]	k4	1986
provedli	provést	k5eAaPmAgMnP	provést
bombový	bombový	k2eAgInSc4d1	bombový
útok	útok	k1gInSc4	útok
na	na	k7c4	na
diskotéku	diskotéka	k1gFnSc4	diskotéka
La	la	k1gNnSc2	la
Belle	bell	k1gInSc5	bell
v	v	k7c6	v
západním	západní	k2eAgInSc6d1	západní
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgNnSc6	který
byli	být	k5eAaImAgMnP	být
3	[number]	k4	3
lidé	člověk	k1gMnPc1	člověk
zabiti	zabít	k5eAaPmNgMnP	zabít
a	a	k8xC	a
229	[number]	k4	229
zraněno	zranit	k5eAaPmNgNnS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sjednocení	sjednocení	k1gNnSc6	sjednocení
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
otevření	otevření	k1gNnSc2	otevření
archívu	archív	k1gInSc3	archív
Stasi	stase	k1gFnSc4	stase
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
teroristická	teroristický	k2eAgFnSc1d1	teroristická
akce	akce	k1gFnSc1	akce
byla	být	k5eAaImAgFnS	být
nařízena	nařídit	k5eAaPmNgFnS	nařídit
z	z	k7c2	z
Tripolisu	Tripolis	k1gInSc2	Tripolis
<g/>
,	,	kIx,	,
provedena	provést	k5eAaPmNgFnS	provést
za	za	k7c4	za
spoluúčasti	spoluúčast	k1gFnSc3	spoluúčast
Stasi	stase	k1gFnSc3	stase
a	a	k8xC	a
souhlasu	souhlas	k1gInSc3	souhlas
sovětských	sovětský	k2eAgMnPc2d1	sovětský
poradců	poradce	k1gMnPc2	poradce
v	v	k7c6	v
NDR	NDR	kA	NDR
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1986	[number]	k4	1986
USA	USA	kA	USA
provedly	provést	k5eAaPmAgInP	provést
odvetnou	odvetný	k2eAgFnSc4d1	odvetná
operaci	operace	k1gFnSc4	operace
El	Ela	k1gFnPc2	Ela
Dorado	Dorada	k1gFnSc5	Dorada
Canyon	Canyon	k1gNnSc4	Canyon
<g/>
.	.	kIx.	.
</s>
<s>
Protivzdušná	protivzdušný	k2eAgFnSc1d1	protivzdušná
obrana	obrana	k1gFnSc1	obrana
<g/>
,	,	kIx,	,
3	[number]	k4	3
vojenské	vojenský	k2eAgFnSc2d1	vojenská
základny	základna	k1gFnSc2	základna
a	a	k8xC	a
2	[number]	k4	2
vojenská	vojenský	k2eAgNnPc4d1	vojenské
letiště	letiště	k1gNnSc4	letiště
v	v	k7c6	v
Tripolisu	Tripolis	k1gInSc6	Tripolis
a	a	k8xC	a
Benghází	Bengháze	k1gFnSc7	Bengháze
byly	být	k5eAaImAgFnP	být
bombardovány	bombardovat	k5eAaImNgFnP	bombardovat
<g/>
.	.	kIx.	.
</s>
<s>
Zabít	zabít	k5eAaPmF	zabít
Kaddáfího	Kaddáfí	k1gMnSc2	Kaddáfí
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
život	život	k1gInSc4	život
přišlo	přijít	k5eAaPmAgNnS	přijít
několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
armádních	armádní	k2eAgMnPc2d1	armádní
důstojníků	důstojník	k1gMnPc2	důstojník
<g/>
.	.	kIx.	.
</s>
<s>
Kaddáfí	Kaddáfí	k1gMnSc1	Kaddáfí
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
unikátního	unikátní	k2eAgNnSc2d1	unikátní
vojenského	vojenský	k2eAgNnSc2d1	vojenské
vítězství	vítězství	k1gNnSc2	vítězství
nad	nad	k7c7	nad
USA	USA	kA	USA
a	a	k8xC	a
přejmenoval	přejmenovat	k5eAaPmAgInS	přejmenovat
Libyi	Libye	k1gFnSc4	Libye
na	na	k7c4	na
Velkou	velký	k2eAgFnSc4d1	velká
libyjskou	libyjský	k2eAgFnSc4d1	Libyjská
arabskou	arabský	k2eAgFnSc4d1	arabská
lidovou	lidový	k2eAgFnSc4d1	lidová
socialistickou	socialistický	k2eAgFnSc4d1	socialistická
džamáhíríji	džamáhíríje	k1gFnSc4	džamáhíríje
<g/>
.	.	kIx.	.
</s>
<s>
Kaddáfí	Kaddáfí	k1gMnSc1	Kaddáfí
stimuloval	stimulovat	k5eAaImAgMnS	stimulovat
několik	několik	k4yIc4	několik
islámských	islámský	k2eAgFnPc2d1	islámská
a	a	k8xC	a
komunistických	komunistický	k2eAgFnPc2d1	komunistická
teroristických	teroristický	k2eAgFnPc2d1	teroristická
skupin	skupina	k1gFnPc2	skupina
až	až	k9	až
na	na	k7c6	na
Filipínách	Filipíny	k1gFnPc6	Filipíny
<g/>
;	;	kIx,	;
také	také	k9	také
dotoval	dotovat	k5eAaBmAgMnS	dotovat
paravojenské	paravojenský	k2eAgFnPc4d1	paravojenská
skupiny	skupina	k1gFnPc4	skupina
v	v	k7c6	v
Oceánii	Oceánie	k1gFnSc6	Oceánie
<g/>
.	.	kIx.	.
</s>
<s>
Pokusil	pokusit	k5eAaPmAgMnS	pokusit
se	se	k3xPyFc4	se
radikalizovat	radikalizovat	k5eAaBmF	radikalizovat
i	i	k9	i
novozélandské	novozélandský	k2eAgFnPc4d1	novozélandská
Maory	Maora	k1gFnPc4	Maora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
podplácel	podplácet	k5eAaImAgMnS	podplácet
obchodní	obchodní	k2eAgInPc4d1	obchodní
svazy	svaz	k1gInPc4	svaz
a	a	k8xC	a
některé	některý	k3yIgMnPc4	některý
politiky	politik	k1gMnPc4	politik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1987	[number]	k4	1987
pak	pak	k6eAd1	pak
Austrálie	Austrálie	k1gFnSc1	Austrálie
vypověděla	vypovědět	k5eAaPmAgFnS	vypovědět
diplomaty	diplomat	k1gInPc4	diplomat
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
aktivitám	aktivita	k1gFnPc3	aktivita
v	v	k7c6	v
Oceánii	Oceánie	k1gFnSc6	Oceánie
přerušila	přerušit	k5eAaPmAgFnS	přerušit
s	s	k7c7	s
Libyí	Libye	k1gFnSc7	Libye
styky	styk	k1gInPc1	styk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
dva	dva	k4xCgMnPc1	dva
libyjští	libyjský	k2eAgMnPc1d1	libyjský
tajní	tajný	k2eAgMnPc1d1	tajný
agenti	agent	k1gMnPc1	agent
byli	být	k5eAaImAgMnP	být
státními	státní	k2eAgFnPc7d1	státní
zastupitelstvími	zastupitelství	k1gNnPc7	zastupitelství
USA	USA	kA	USA
a	a	k8xC	a
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
obviněni	obvinit	k5eAaPmNgMnP	obvinit
z	z	k7c2	z
účasti	účast	k1gFnSc2	účast
na	na	k7c4	na
explozi	exploze	k1gFnSc4	exploze
dopravního	dopravní	k2eAgNnSc2d1	dopravní
letadla	letadlo	k1gNnSc2	letadlo
letu	let	k1gInSc2	let
Pan	Pan	k1gMnSc1	Pan
Am	Am	k1gMnSc1	Am
103	[number]	k4	103
<g/>
.	.	kIx.	.
</s>
<s>
Šest	šest	k4xCc1	šest
dalších	další	k2eAgMnPc2d1	další
Libyjců	Libyjec	k1gMnPc2	Libyjec
bylo	být	k5eAaImAgNnS	být
souzeno	soudit	k5eAaImNgNnS	soudit
v	v	k7c6	v
nepřítomnosti	nepřítomnost	k1gFnSc6	nepřítomnost
za	za	k7c4	za
bombový	bombový	k2eAgInSc4d1	bombový
útok	útok	k1gInSc4	útok
letu	let	k1gInSc2	let
UTA	UTA	kA	UTA
772	[number]	k4	772
nad	nad	k7c7	nad
Čadem	Čad	k1gInSc7	Čad
a	a	k8xC	a
Nigerem	Niger	k1gInSc7	Niger
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
požadovala	požadovat	k5eAaImAgFnS	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Libye	Libye	k1gFnSc1	Libye
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
s	s	k7c7	s
vyšetřovateli	vyšetřovatel	k1gMnPc7	vyšetřovatel
letů	let	k1gInPc2	let
UTA	UTA	kA	UTA
772	[number]	k4	772
a	a	k8xC	a
Pan	Pan	k1gMnSc1	Pan
Am	Am	k1gFnSc2	Am
103	[number]	k4	103
<g/>
,	,	kIx,	,
zaplatila	zaplatit	k5eAaPmAgFnS	zaplatit
odškodné	odškodné	k1gNnSc4	odškodné
rodinám	rodina	k1gFnPc3	rodina
obětí	oběť	k1gFnPc2	oběť
a	a	k8xC	a
zastavila	zastavit	k5eAaPmAgFnS	zastavit
veškerou	veškerý	k3xTgFnSc4	veškerý
podporu	podpora	k1gFnSc4	podpora
terorismu	terorismus	k1gInSc2	terorismus
<g/>
.	.	kIx.	.
</s>
<s>
Odmítnutí	odmítnutí	k1gNnSc1	odmítnutí
Libye	Libye	k1gFnSc2	Libye
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
vydání	vydání	k1gNnSc3	vydání
rezoluce	rezoluce	k1gFnSc2	rezoluce
rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
748	[number]	k4	748
dne	den	k1gInSc2	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1992	[number]	k4	1992
o	o	k7c6	o
uvalení	uvalení	k1gNnSc6	uvalení
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
sankcí	sankce	k1gFnPc2	sankce
vůči	vůči	k7c3	vůči
Libyi	Libye	k1gFnSc3	Libye
<g/>
.	.	kIx.	.
</s>
<s>
Opětné	opětný	k2eAgNnSc1d1	opětné
odmítnutí	odmítnutí	k1gNnSc1	odmítnutí
Libyí	Libye	k1gFnPc2	Libye
pak	pak	k6eAd1	pak
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
uvalení	uvalení	k1gNnSc3	uvalení
dalších	další	k2eAgFnPc2d1	další
sankcí	sankce	k1gFnPc2	sankce
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
než	než	k8xS	než
10	[number]	k4	10
let	léto	k1gNnPc2	léto
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
byly	být	k5eAaImAgFnP	být
uvaleny	uvalen	k2eAgFnPc1d1	uvalena
sankce	sankce	k1gFnPc1	sankce
<g/>
,	,	kIx,	,
Libye	Libye	k1gFnSc1	Libye
začala	začít	k5eAaPmAgFnS	začít
svoji	svůj	k3xOyFgFnSc4	svůj
politiku	politika	k1gFnSc4	politika
vůči	vůči	k7c3	vůči
Západu	západ	k1gInSc3	západ
dramaticky	dramaticky	k6eAd1	dramaticky
měnit	měnit	k5eAaImF	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
režimu	režim	k1gInSc2	režim
Saddáma	Saddám	k1gMnSc2	Saddám
Husajna	Husajn	k1gMnSc2	Husajn
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Kaddáfí	Kaddáfí	k1gMnSc1	Kaddáfí
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
zastavit	zastavit	k5eAaPmF	zastavit
projekt	projekt	k1gInSc4	projekt
zbraní	zbraň	k1gFnPc2	zbraň
hromadného	hromadný	k2eAgNnSc2d1	hromadné
ničení	ničení	k1gNnSc2	ničení
a	a	k8xC	a
zaplatil	zaplatit	k5eAaPmAgInS	zaplatit
téměř	téměř	k6eAd1	téměř
3	[number]	k4	3
miliardy	miliarda	k4xCgFnSc2	miliarda
eur	euro	k1gNnPc2	euro
na	na	k7c6	na
odškodnění	odškodnění	k1gNnSc6	odškodnění
rodin	rodina	k1gFnPc2	rodina
obětí	oběť	k1gFnPc2	oběť
letů	let	k1gInPc2	let
Pan	Pan	k1gMnSc1	Pan
Am	Am	k1gMnSc1	Am
103	[number]	k4	103
a	a	k8xC	a
UTA	UTA	kA	UTA
772	[number]	k4	772
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
čin	čin	k1gInSc1	čin
mnohé	mnohý	k2eAgFnSc2d1	mnohá
západní	západní	k2eAgFnSc2d1	západní
země	zem	k1gFnSc2	zem
přivítaly	přivítat	k5eAaPmAgFnP	přivítat
a	a	k8xC	a
pohlížely	pohlížet	k5eAaImAgFnP	pohlížet
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
jako	jako	k8xS	jako
na	na	k7c4	na
významný	významný	k2eAgInSc4d1	významný
krok	krok	k1gInSc4	krok
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
Libye	Libye	k1gFnSc2	Libye
do	do	k7c2	do
mezinárodních	mezinárodní	k2eAgNnPc2d1	mezinárodní
společenstev	společenstvo	k1gNnPc2	společenstvo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
se	se	k3xPyFc4	se
země	země	k1gFnSc1	země
pokoušela	pokoušet	k5eAaImAgFnS	pokoušet
normalizovat	normalizovat	k5eAaBmF	normalizovat
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
EU	EU	kA	EU
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
prezident	prezident	k1gMnSc1	prezident
George	Georg	k1gInSc2	Georg
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
sankce	sankce	k1gFnSc2	sankce
vůči	vůči	k7c3	vůči
Libyi	Libye	k1gFnSc3	Libye
ukončil	ukončit	k5eAaPmAgMnS	ukončit
a	a	k8xC	a
současně	současně	k6eAd1	současně
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
navázal	navázat	k5eAaPmAgMnS	navázat
mezinárodní	mezinárodní	k2eAgInPc4d1	mezinárodní
styky	styk	k1gInPc4	styk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2004	[number]	k4	2004
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
první	první	k4xOgFnSc1	první
oficiální	oficiální	k2eAgFnSc1d1	oficiální
návštěva	návštěva	k1gFnSc1	návštěva
Libye	Libye	k1gFnSc1	Libye
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
delegací	delegace	k1gFnSc7	delegace
amerického	americký	k2eAgInSc2d1	americký
Kongresu	kongres	k1gInSc2	kongres
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Libyi	Libye	k1gFnSc6	Libye
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
a	a	k8xC	a
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
intervence	intervence	k1gFnSc1	intervence
v	v	k7c6	v
Libyi	Libye	k1gFnSc6	Libye
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
až	až	k9	až
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
probíhaly	probíhat	k5eAaImAgFnP	probíhat
nebo	nebo	k8xC	nebo
probíhají	probíhat	k5eAaImIp3nP	probíhat
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
arabských	arabský	k2eAgInPc2d1	arabský
států	stát	k1gInPc2	stát
protesty	protest	k1gInPc1	protest
<g/>
,	,	kIx,	,
nepokoje	nepokoj	k1gInPc1	nepokoj
<g/>
,	,	kIx,	,
povstání	povstání	k1gNnPc1	povstání
a	a	k8xC	a
revoluce	revoluce	k1gFnPc1	revoluce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
nazývány	nazývat	k5eAaImNgFnP	nazývat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
arabské	arabský	k2eAgNnSc1d1	arabské
jaro	jaro	k1gNnSc1	jaro
<g/>
"	"	kIx"	"
Během	během	k7c2	během
vlny	vlna	k1gFnSc2	vlna
úspěšných	úspěšný	k2eAgFnPc2d1	úspěšná
revolucí	revoluce	k1gFnPc2	revoluce
v	v	k7c6	v
Tunisku	Tunisko	k1gNnSc6	Tunisko
a	a	k8xC	a
Egyptě	Egypt	k1gInSc6	Egypt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byly	být	k5eAaImAgFnP	být
svrženy	svrhnout	k5eAaPmNgInP	svrhnout
staré	starý	k2eAgInPc1d1	starý
autoritativní	autoritativní	k2eAgInPc1d1	autoritativní
režimy	režim	k1gInPc1	režim
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2011	[number]	k4	2011
propuklo	propuknout	k5eAaPmAgNnS	propuknout
v	v	k7c6	v
Libyi	Libye	k1gFnSc6	Libye
také	také	k9	také
povstání	povstání	k1gNnSc1	povstání
namířené	namířený	k2eAgNnSc1d1	namířené
proti	proti	k7c3	proti
režimu	režim	k1gInSc3	režim
Muammara	Muammar	k1gMnSc2	Muammar
Kaddáfího	Kaddáfí	k1gMnSc2	Kaddáfí
<g/>
.	.	kIx.	.
</s>
<s>
Protesty	protest	k1gInPc1	protest
nejprve	nejprve	k6eAd1	nejprve
zachvátily	zachvátit	k5eAaPmAgInP	zachvátit
města	město	k1gNnPc4	město
v	v	k7c6	v
Kyrenaice	Kyrenaika	k1gFnSc6	Kyrenaika
zejména	zejména	k9	zejména
v	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
největším	veliký	k2eAgNnSc6d3	veliký
libyjském	libyjský	k2eAgNnSc6d1	Libyjské
městě	město	k1gNnSc6	město
Benghází	Bengháze	k1gFnPc2	Bengháze
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
zde	zde	k6eAd1	zde
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
střetům	střet	k1gInPc3	střet
protestujících	protestující	k2eAgFnPc2d1	protestující
s	s	k7c7	s
vojskem	vojsko	k1gNnSc7	vojsko
a	a	k8xC	a
k	k	k7c3	k
pouličním	pouliční	k2eAgFnPc3d1	pouliční
bitvám	bitva	k1gFnPc3	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
nepokoje	nepokoj	k1gInPc1	nepokoj
přenesly	přenést	k5eAaPmAgInP	přenést
i	i	k9	i
do	do	k7c2	do
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Tripolisu	Tripolis	k1gInSc2	Tripolis
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2011	[number]	k4	2011
začala	začít	k5eAaPmAgFnS	začít
armáda	armáda	k1gFnSc1	armáda
letecky	letecky	k6eAd1	letecky
bombardovat	bombardovat	k5eAaImF	bombardovat
protestující	protestující	k2eAgInPc4d1	protestující
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
francouzská	francouzský	k2eAgFnSc1d1	francouzská
bojová	bojový	k2eAgNnPc1d1	bojové
letadla	letadlo	k1gNnPc1	letadlo
do	do	k7c2	do
vzdušného	vzdušný	k2eAgInSc2d1	vzdušný
prostoru	prostor	k1gInSc2	prostor
Libye	Libye	k1gFnSc2	Libye
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
připojila	připojit	k5eAaPmAgFnS	připojit
letadla	letadlo	k1gNnSc2	letadlo
nebo	nebo	k8xC	nebo
technika	technika	k1gFnSc1	technika
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
,	,	kIx,	,
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
Kanady	Kanada	k1gFnSc2	Kanada
i	i	k8xC	i
některých	některý	k3yIgFnPc2	některý
arabských	arabský	k2eAgFnPc2d1	arabská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
státy	stát	k1gInPc1	stát
podpořily	podpořit	k5eAaPmAgInP	podpořit
povstalce	povstalec	k1gMnSc2	povstalec
a	a	k8xC	a
vynutily	vynutit	k5eAaPmAgFnP	vynutit
dodržování	dodržování	k1gNnSc4	dodržování
rezoluce	rezoluce	k1gFnSc2	rezoluce
číslo	číslo	k1gNnSc1	číslo
1973	[number]	k4	1973
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
o	o	k7c6	o
bezletové	bezletový	k2eAgFnSc6d1	bezletová
zóně	zóna	k1gFnSc6	zóna
nad	nad	k7c7	nad
Libyí	Libye	k1gFnSc7	Libye
<g/>
.	.	kIx.	.
</s>
<s>
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
intervence	intervence	k1gFnSc1	intervence
výrazně	výrazně	k6eAd1	výrazně
změnila	změnit	k5eAaPmAgFnS	změnit
strategickou	strategický	k2eAgFnSc4d1	strategická
situaci	situace	k1gFnSc4	situace
na	na	k7c6	na
bojišti	bojiště	k1gNnSc6	bojiště
<g/>
.	.	kIx.	.
</s>
<s>
Kaddáfího	Kaddáfí	k1gMnSc4	Kaddáfí
síly	síla	k1gFnSc2	síla
přišly	přijít	k5eAaPmAgInP	přijít
částečně	částečně	k6eAd1	částečně
o	o	k7c4	o
výhody	výhoda	k1gFnPc4	výhoda
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
používáním	používání	k1gNnSc7	používání
těžké	těžký	k2eAgFnSc2d1	těžká
bojové	bojový	k2eAgFnSc2d1	bojová
techniky	technika	k1gFnSc2	technika
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
letadel	letadlo	k1gNnPc2	letadlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
tanků	tank	k1gInPc2	tank
<g/>
,	,	kIx,	,
dělostřelectva	dělostřelectvo	k1gNnSc2	dělostřelectvo
a	a	k8xC	a
podpůrných	podpůrný	k2eAgFnPc2d1	podpůrná
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
vystaveny	vystavit	k5eAaPmNgFnP	vystavit
útokům	útok	k1gInPc3	útok
letadel	letadlo	k1gNnPc2	letadlo
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
povstalcům	povstalec	k1gMnPc3	povstalec
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
přeskupit	přeskupit	k5eAaPmF	přeskupit
své	svůj	k3xOyFgNnSc4	svůj
sily	sít	k5eAaImAgFnP	sít
<g/>
.	.	kIx.	.
</s>
<s>
Podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
též	též	k9	též
navázat	navázat	k5eAaPmF	navázat
první	první	k4xOgInPc4	první
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
styky	styk	k1gInPc4	styk
a	a	k8xC	a
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Francie	Francie	k1gFnSc1	Francie
vyzbrojit	vyzbrojit	k5eAaPmF	vyzbrojit
své	svůj	k3xOyFgFnPc4	svůj
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Kaddáfí	Kaddáfit	k5eAaImIp3nS	Kaddáfit
se	se	k3xPyFc4	se
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
ocitl	ocitnout	k5eAaPmAgInS	ocitnout
v	v	k7c6	v
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
izolaci	izolace	k1gFnSc6	izolace
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
vydán	vydán	k2eAgInSc1d1	vydán
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
zatykač	zatykač	k1gInSc1	zatykač
a	a	k8xC	a
jeho	jeho	k3xOp3gNnPc1	jeho
zahraniční	zahraniční	k2eAgNnPc1d1	zahraniční
konta	konto	k1gNnPc1	konto
byla	být	k5eAaImAgNnP	být
zmrazena	zmrazit	k5eAaPmNgNnP	zmrazit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2011	[number]	k4	2011
začala	začít	k5eAaPmAgFnS	začít
koordinovaná	koordinovaný	k2eAgFnSc1d1	koordinovaná
ofenziva	ofenziva	k1gFnSc1	ofenziva
povstalců	povstalec	k1gMnPc2	povstalec
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
srpna	srpen	k1gInSc2	srpen
2011	[number]	k4	2011
se	se	k3xPyFc4	se
povstalcům	povstalec	k1gMnPc3	povstalec
podařilo	podařit	k5eAaPmAgNnS	podařit
dobýt	dobýt	k5eAaPmF	dobýt
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgNnSc4d1	celé
území	území	k1gNnSc4	území
Libye	Libye	k1gFnSc2	Libye
a	a	k8xC	a
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2011	[number]	k4	2011
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Kaddáfího	Kaddáfí	k1gMnSc2	Kaddáfí
sídlo	sídlo	k1gNnSc1	sídlo
Báb	Báb	k1gMnSc1	Báb
al-Azízíju	al-Azízíju	k5eAaPmIp1nS	al-Azízíju
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
však	však	k9	však
podařilo	podařit	k5eAaPmAgNnS	podařit
nalézt	nalézt	k5eAaPmF	nalézt
samotného	samotný	k2eAgMnSc4d1	samotný
Kaddáfího	Kaddáfí	k1gMnSc4	Kaddáfí
<g/>
.	.	kIx.	.
</s>
<s>
Libyjská	libyjský	k2eAgFnSc1d1	Libyjská
Dočasná	dočasný	k2eAgFnSc1d1	dočasná
národní	národní	k2eAgFnSc1d1	národní
přechodná	přechodný	k2eAgFnSc1d1	přechodná
rada	rada	k1gFnSc1	rada
již	již	k6eAd1	již
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
přesídlí	přesídlet	k5eAaPmIp3nS	přesídlet
do	do	k7c2	do
Tripolisu	Tripolis	k1gInSc2	Tripolis
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
října	říjen	k1gInSc2	říjen
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
potyčkám	potyčka	k1gFnPc3	potyčka
mezi	mezi	k7c7	mezi
povstaleckými	povstalecký	k2eAgFnPc7d1	povstalecká
brigádami	brigáda	k1gFnPc7	brigáda
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
byl	být	k5eAaImAgMnS	být
Kaddáfí	Kaddáfí	k1gFnSc4	Kaddáfí
povstalci	povstalec	k1gMnSc3	povstalec
dopaden	dopaden	k2eAgMnSc1d1	dopaden
<g/>
,	,	kIx,	,
lynčován	lynčován	k2eAgMnSc1d1	lynčován
a	a	k8xC	a
následně	následně	k6eAd1	následně
zastřelen	zastřelit	k5eAaPmNgMnS	zastřelit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následných	následný	k2eAgInPc6d1	následný
průzkumech	průzkum	k1gInPc6	průzkum
veřejného	veřejný	k2eAgNnSc2d1	veřejné
mínění	mínění	k1gNnSc2	mínění
vyjádřilo	vyjádřit	k5eAaPmAgNnS	vyjádřit
podporu	podpora	k1gFnSc4	podpora
zásahu	zásah	k1gInSc2	zásah
NATO	NATO	kA	NATO
75	[number]	k4	75
<g/>
%	%	kIx~	%
Libyjců	Libyjec	k1gMnPc2	Libyjec
dle	dle	k7c2	dle
Gallupova	Gallupův	k2eAgInSc2d1	Gallupův
průzkumu	průzkum	k1gInSc2	průzkum
<g/>
,	,	kIx,	,
85	[number]	k4	85
<g/>
%	%	kIx~	%
Libyjců	Libyjec	k1gMnPc2	Libyjec
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
vyjádřilo	vyjádřit	k5eAaPmAgNnS	vyjádřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
země	země	k1gFnSc1	země
"	"	kIx"	"
<g/>
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
správné	správný	k2eAgFnSc6d1	správná
cestě	cesta	k1gFnSc6	cesta
<g/>
"	"	kIx"	"
v	v	k7c6	v
průzkumu	průzkum	k1gInSc6	průzkum
ORB	ORB	kA	ORB
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2012	[number]	k4	2012
se	se	k3xPyFc4	se
v	v	k7c6	v
Libyi	Libye	k1gFnSc6	Libye
konaly	konat	k5eAaImAgFnP	konat
volby	volba	k1gFnPc1	volba
do	do	k7c2	do
přechodného	přechodný	k2eAgInSc2d1	přechodný
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
čítá	čítat	k5eAaImIp3nS	čítat
200	[number]	k4	200
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
80	[number]	k4	80
bylo	být	k5eAaImAgNnS	být
voleno	volit	k5eAaImNgNnS	volit
proporčně	proporčně	k6eAd1	proporčně
dle	dle	k7c2	dle
stranických	stranický	k2eAgFnPc2d1	stranická
kandidátních	kandidátní	k2eAgFnPc2d1	kandidátní
listin	listina	k1gFnPc2	listina
<g/>
,	,	kIx,	,
zbylých	zbylý	k2eAgNnPc2d1	zbylé
120	[number]	k4	120
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
kandidátů	kandidát	k1gMnPc2	kandidát
bylo	být	k5eAaImAgNnS	být
volených	volený	k2eAgMnPc2d1	volený
systémem	systém	k1gInSc7	systém
relativní	relativní	k2eAgFnSc2d1	relativní
většiny	většina	k1gFnSc2	většina
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
západu	západ	k1gInSc2	západ
země	zem	k1gFnSc2	zem
pocházelo	pocházet	k5eAaImAgNnS	pocházet
100	[number]	k4	100
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
,	,	kIx,	,
z	z	k7c2	z
východu	východ	k1gInSc2	východ
60	[number]	k4	60
a	a	k8xC	a
jihu	jih	k1gInSc3	jih
40	[number]	k4	40
<g/>
.	.	kIx.	.
</s>
<s>
Regionální	regionální	k2eAgNnSc1d1	regionální
rozložení	rozložení	k1gNnSc1	rozložení
mandátů	mandát	k1gInPc2	mandát
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
protestům	protest	k1gInPc3	protest
na	na	k7c6	na
východě	východ	k1gInSc6	východ
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Některým	některý	k3yIgMnPc3	některý
místním	místní	k2eAgMnPc3d1	místní
obyvatelům	obyvatel	k1gMnPc3	obyvatel
totiž	totiž	k9	totiž
připadal	připadat	k5eAaImAgInS	připadat
počet	počet	k1gInSc1	počet
jejich	jejich	k3xOp3gMnPc2	jejich
poslanců	poslanec	k1gMnPc2	poslanec
jako	jako	k9	jako
nedostatečný	dostatečný	k2eNgInSc1d1	nedostatečný
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
sporům	spor	k1gInPc3	spor
o	o	k7c6	o
počtu	počet	k1gInSc6	počet
delegátů	delegát	k1gMnPc2	delegát
z	z	k7c2	z
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
částí	část	k1gFnPc2	část
Libye	Libye	k1gFnSc2	Libye
v	v	k7c6	v
kongresu	kongres	k1gInSc6	kongres
zároveň	zároveň	k6eAd1	zároveň
Dočasná	dočasný	k2eAgFnSc1d1	dočasná
národní	národní	k2eAgFnSc1d1	národní
přechodná	přechodný	k2eAgFnSc1d1	přechodná
rada	rada	k1gFnSc1	rada
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
volbami	volba	k1gFnPc7	volba
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
samotnou	samotný	k2eAgFnSc4d1	samotná
ústavu	ústava	k1gFnSc4	ústava
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
nezávislý	závislý	k2eNgInSc1d1	nezávislý
<g/>
,	,	kIx,	,
přímo	přímo	k6eAd1	přímo
volený	volený	k2eAgInSc1d1	volený
orgán	orgán	k1gInSc1	orgán
s	s	k7c7	s
rovným	rovný	k2eAgNnSc7d1	rovné
zastoupením	zastoupení	k1gNnSc7	zastoupení
účastníků	účastník	k1gMnPc2	účastník
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
regionů	region	k1gInPc2	region
Libye	Libye	k1gFnSc2	Libye
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
Kyrenaiky	Kyrenaika	k1gFnSc2	Kyrenaika
<g/>
,	,	kIx,	,
Fezzánu	Fezzán	k2eAgFnSc4d1	Fezzán
a	a	k8xC	a
Tripolska	Tripolsko	k1gNnSc2	Tripolsko
<g/>
.	.	kIx.	.
</s>
<s>
Pozorovatelé	pozorovatel	k1gMnPc1	pozorovatel
EU	EU	kA	EU
a	a	k8xC	a
OSN	OSN	kA	OSN
označili	označit	k5eAaPmAgMnP	označit
průběh	průběh	k1gInSc4	průběh
voleb	volba	k1gFnPc2	volba
za	za	k7c4	za
úspěšný	úspěšný	k2eAgInSc4d1	úspěšný
<g/>
.	.	kIx.	.
</s>
<s>
Moc	moc	k6eAd1	moc
byla	být	k5eAaImAgFnS	být
předána	předán	k2eAgFnSc1d1	předána
parlamentu	parlament	k1gInSc2	parlament
8	[number]	k4	8
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomicky	ekonomicky	k6eAd1	ekonomicky
země	zem	k1gFnPc1	zem
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
navázala	navázat	k5eAaPmAgFnS	navázat
na	na	k7c4	na
předchozí	předchozí	k2eAgFnSc4d1	předchozí
úroveň	úroveň	k1gFnSc4	úroveň
vývozu	vývoz	k1gInSc2	vývoz
ropy	ropa	k1gFnSc2	ropa
s	s	k7c7	s
1,5	[number]	k4	1,5
milionem	milion	k4xCgInSc7	milion
vyvezených	vyvezený	k2eAgInPc2d1	vyvezený
barelů	barel	k1gInPc2	barel
vůči	vůči	k7c3	vůči
1,6	[number]	k4	1,6
milionům	milion	k4xCgInPc3	milion
před	před	k7c7	před
konfliktem	konflikt	k1gInSc7	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
centrální	centrální	k2eAgFnSc1d1	centrální
vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
však	však	k9	však
vyznačovala	vyznačovat	k5eAaImAgFnS	vyznačovat
slabostí	slabost	k1gFnSc7	slabost
a	a	k8xC	a
nedařilo	dařit	k5eNaImAgNnS	dařit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
prosazovat	prosazovat	k5eAaImF	prosazovat
svou	svůj	k3xOyFgFnSc4	svůj
moc	moc	k1gFnSc4	moc
vůči	vůči	k7c3	vůči
ozbrojeným	ozbrojený	k2eAgFnPc3d1	ozbrojená
milicím	milice	k1gFnPc3	milice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
probíhaly	probíhat	k5eAaImAgInP	probíhat
ozbrojené	ozbrojený	k2eAgInPc1d1	ozbrojený
střety	střet	k1gInPc1	střet
a	a	k8xC	a
násilí	násilí	k1gNnSc4	násilí
zaměřené	zaměřený	k2eAgNnSc4d1	zaměřené
především	především	k9	především
proti	proti	k7c3	proti
státním	státní	k2eAgMnPc3d1	státní
orgánům	orgán	k1gInPc3	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
byla	být	k5eAaImAgFnS	být
Libye	Libye	k1gFnSc1	Libye
řízena	řídit	k5eAaImNgFnS	řídit
Všeobecným	všeobecný	k2eAgInSc7d1	všeobecný
národním	národní	k2eAgInSc7d1	národní
kongresem	kongres	k1gInSc7	kongres
(	(	kIx(	(
<g/>
VNK	VNK	kA	VNK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
islamisté	islamista	k1gMnPc1	islamista
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2013	[number]	k4	2013
Núrí	Núrí	k1gMnSc1	Núrí
abú	abú	k1gMnSc1	abú
Sahmajn	Sahmajn	k1gMnSc1	Sahmajn
zvolen	zvolit	k5eAaPmNgMnS	zvolit
předsedou	předseda	k1gMnSc7	předseda
VNK	VNK	kA	VNK
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
zneužil	zneužít	k5eAaPmAgMnS	zneužít
své	svůj	k3xOyFgFnPc4	svůj
pravomoci	pravomoc	k1gFnPc4	pravomoc
k	k	k7c3	k
potlačení	potlačení	k1gNnSc3	potlačení
diskuse	diskuse	k1gFnSc2	diskuse
a	a	k8xC	a
jednání	jednání	k1gNnSc2	jednání
kongresu	kongres	k1gInSc2	kongres
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2014	[number]	k4	2014
VNK	VNK	kA	VNK
nedostál	dostát	k5eNaPmAgMnS	dostát
své	svůj	k3xOyFgFnPc4	svůj
povinnosti	povinnost	k1gFnPc4	povinnost
odstoupit	odstoupit	k5eAaPmF	odstoupit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
svého	svůj	k3xOyFgInSc2	svůj
mandátu	mandát	k1gInSc2	mandát
a	a	k8xC	a
jednostranně	jednostranně	k6eAd1	jednostranně
si	se	k3xPyFc3	se
tak	tak	k6eAd1	tak
uzurpoval	uzurpovat	k5eAaBmAgMnS	uzurpovat
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2014	[number]	k4	2014
nařídil	nařídit	k5eAaPmAgMnS	nařídit
generál	generál	k1gMnSc1	generál
Chalífa	chalífa	k1gMnSc1	chalífa
Haftar	Haftar	k1gInSc4	Haftar
rozpuštění	rozpuštění	k1gNnSc2	rozpuštění
kongresu	kongres	k1gInSc2	kongres
a	a	k8xC	a
požadoval	požadovat	k5eAaImAgMnS	požadovat
sestavení	sestavení	k1gNnSc4	sestavení
komise	komise	k1gFnSc2	komise
úřednické	úřednický	k2eAgFnSc2d1	úřednická
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
dohlížela	dohlížet	k5eAaImAgFnS	dohlížet
na	na	k7c4	na
nové	nový	k2eAgFnPc4d1	nová
volby	volba	k1gFnPc4	volba
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
požadavky	požadavek	k1gInPc1	požadavek
však	však	k9	však
byly	být	k5eAaImAgInP	být
ignorovány	ignorovat	k5eAaImNgInP	ignorovat
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Libyi	Libye	k1gFnSc6	Libye
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2014	[number]	k4	2014
síly	síla	k1gFnSc2	síla
věrné	věrný	k2eAgFnSc2d1	věrná
generálu	generál	k1gMnSc3	generál
Haftarovi	Haftarův	k2eAgMnPc1d1	Haftarův
zahájily	zahájit	k5eAaPmAgInP	zahájit
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
pozemní	pozemní	k2eAgFnSc4d1	pozemní
a	a	k8xC	a
vzdušnou	vzdušný	k2eAgFnSc4d1	vzdušná
ofenzivu	ofenziva	k1gFnSc4	ofenziva
proti	proti	k7c3	proti
islamistům	islamista	k1gMnPc3	islamista
v	v	k7c6	v
Benghází	Bengháze	k1gFnSc7	Bengháze
<g/>
,	,	kIx,	,
přezdívanou	přezdívaný	k2eAgFnSc7d1	přezdívaná
Operace	operace	k1gFnPc4	operace
důstojnost	důstojnost	k1gFnSc4	důstojnost
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
ع	ع	k?	ع
ا	ا	k?	ا
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
Haftar	Haftar	k1gInSc1	Haftar
pokusil	pokusit	k5eAaPmAgInS	pokusit
rozpustit	rozpustit	k5eAaPmF	rozpustit
Všeobecný	všeobecný	k2eAgInSc1d1	všeobecný
národní	národní	k2eAgInSc1d1	národní
kongres	kongres	k1gInSc1	kongres
v	v	k7c6	v
Tripolisu	Tripolis	k1gInSc6	Tripolis
<g/>
.	.	kIx.	.
</s>
<s>
Konflikt	konflikt	k1gInSc1	konflikt
zabránil	zabránit	k5eAaPmAgMnS	zabránit
VNK	VNK	kA	VNK
zablokovat	zablokovat	k5eAaPmF	zablokovat
nové	nový	k2eAgFnPc4d1	nová
volby	volba	k1gFnPc4	volba
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
volby	volba	k1gFnPc1	volba
měly	mít	k5eAaImAgFnP	mít
za	za	k7c4	za
důsledek	důsledek	k1gInSc4	důsledek
vytvoření	vytvoření	k1gNnSc4	vytvoření
Rady	rada	k1gFnSc2	rada
poslanců	poslanec	k1gMnPc2	poslanec
namísto	namísto	k7c2	namísto
VNK	VNK	kA	VNK
a	a	k8xC	a
vyústily	vyústit	k5eAaPmAgFnP	vyústit
v	v	k7c4	v
drtivou	drtivý	k2eAgFnSc4d1	drtivá
porážku	porážka	k1gFnSc4	porážka
islamistů	islamista	k1gMnPc2	islamista
<g/>
.	.	kIx.	.
</s>
<s>
Konflikt	konflikt	k1gInSc1	konflikt
eskaloval	eskalovat	k5eAaImAgInS	eskalovat
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
islamisté	islamista	k1gMnPc1	islamista
<g/>
,	,	kIx,	,
reagující	reagující	k2eAgMnSc1d1	reagující
na	na	k7c4	na
drtivou	drtivý	k2eAgFnSc4d1	drtivá
porážku	porážka	k1gFnSc4	porážka
islamistických	islamistický	k2eAgMnPc2d1	islamistický
politiků	politik	k1gMnPc2	politik
<g/>
,	,	kIx,	,
zahájili	zahájit	k5eAaPmAgMnP	zahájit
Operaci	operace	k1gFnSc4	operace
Libyjský	libyjský	k2eAgInSc1d1	libyjský
úsvit	úsvit	k1gInSc1	úsvit
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
dobýt	dobýt	k5eAaPmF	dobýt
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
letiště	letiště	k1gNnSc4	letiště
v	v	k7c6	v
Tripolisu	Tripolis	k1gInSc6	Tripolis
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
41	[number]	k4	41
dnech	den	k1gInPc6	den
bojů	boj	k1gInPc2	boj
islamisté	islamista	k1gMnPc1	islamista
dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
letiště	letiště	k1gNnSc2	letiště
dobyli	dobýt	k5eAaPmAgMnP	dobýt
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2014	[number]	k4	2014
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
probíhajících	probíhající	k2eAgInPc2d1	probíhající
bojů	boj	k1gInPc2	boj
evakuována	evakuován	k2eAgFnSc1d1	evakuována
česká	český	k2eAgFnSc1d1	Česká
ambasáda	ambasáda	k1gFnSc1	ambasáda
v	v	k7c6	v
Libyi	Libye	k1gFnSc6	Libye
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Libye	Libye	k1gFnSc2	Libye
<g/>
.	.	kIx.	.
</s>
<s>
Libye	Libye	k1gFnSc1	Libye
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c4	na
1	[number]	k4	1
759	[number]	k4	759
540	[number]	k4	540
kilometrech	kilometr	k1gInPc6	kilometr
čtverečních	čtvereční	k2eAgMnPc2d1	čtvereční
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
činí	činit	k5eAaImIp3nS	činit
17	[number]	k4	17
<g/>
.	.	kIx.	.
nejrozlehlejší	rozlehlý	k2eAgFnSc6d3	nejrozlehlejší
zemi	zem	k1gFnSc6	zem
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
rozlohou	rozloha	k1gFnSc7	rozloha
zhruba	zhruba	k6eAd1	zhruba
třikráte	třikráte	k6eAd1	třikráte
větší	veliký	k2eAgFnSc1d2	veliký
nežli	nežli	k8xS	nežli
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
a	a	k8xC	a
jen	jen	k9	jen
o	o	k7c4	o
něco	něco	k3yInSc4	něco
menší	malý	k2eAgFnSc1d2	menší
nežli	nežli	k8xS	nežli
Indonésie	Indonésie	k1gFnSc1	Indonésie
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
poloha	poloha	k1gFnSc1	poloha
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mezi	mezi	k7c7	mezi
19	[number]	k4	19
<g/>
°	°	k?	°
a	a	k8xC	a
34	[number]	k4	34
<g/>
°	°	k?	°
severní	severní	k2eAgFnSc2d1	severní
šířky	šířka	k1gFnSc2	šířka
<g/>
,	,	kIx,	,
9	[number]	k4	9
<g/>
°	°	k?	°
a	a	k8xC	a
26	[number]	k4	26
<g/>
°	°	k?	°
východní	východní	k2eAgFnSc2d1	východní
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
západu	západ	k1gInSc2	západ
sdílí	sdílet	k5eAaImIp3nS	sdílet
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
Tuniskem	Tunisko	k1gNnSc7	Tunisko
a	a	k8xC	a
Alžírem	Alžír	k1gInSc7	Alžír
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
tvoří	tvořit	k5eAaImIp3nS	tvořit
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
hranici	hranice	k1gFnSc4	hranice
středozemní	středozemní	k2eAgNnSc1d1	středozemní
moře	moře	k1gNnSc1	moře
<g/>
,	,	kIx,	,
východní	východní	k2eAgFnSc1d1	východní
hranice	hranice	k1gFnSc1	hranice
sdílí	sdílet	k5eAaImIp3nS	sdílet
s	s	k7c7	s
Egyptem	Egypt	k1gInSc7	Egypt
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
pak	pak	k6eAd1	pak
sousedí	sousedit	k5eAaImIp3nS	sousedit
se	s	k7c7	s
Súdánem	Súdán	k1gInSc7	Súdán
<g/>
,	,	kIx,	,
Nigerem	Niger	k1gInSc7	Niger
a	a	k8xC	a
Čadem	Čad	k1gInSc7	Čad
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
severní	severní	k2eAgNnSc1d1	severní
pobřeží	pobřeží	k1gNnSc1	pobřeží
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
1770	[number]	k4	1770
kilometrů	kilometr	k1gInPc2	kilometr
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
nejdelším	dlouhý	k2eAgNnSc7d3	nejdelší
pobřežím	pobřeží	k1gNnSc7	pobřeží
kteréhokoliv	kterýkoliv	k3yIgInSc2	kterýkoliv
státu	stát	k1gInSc2	stát
Středomoří	středomoří	k1gNnSc2	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
území	území	k1gNnSc2	území
zabírá	zabírat	k5eAaImIp3nS	zabírat
Libyjská	libyjský	k2eAgFnSc1d1	Libyjská
poušť	poušť	k1gFnSc1	poušť
<g/>
,	,	kIx,	,
podnebí	podnebí	k1gNnSc1	podnebí
je	být	k5eAaImIp3nS	být
povětšinou	povětšinou	k6eAd1	povětšinou
suché	suchý	k2eAgFnSc2d1	suchá
<g/>
,	,	kIx,	,
pouštní	pouštní	k2eAgFnSc2d1	pouštní
<g/>
,	,	kIx,	,
pobřežní	pobřežní	k2eAgFnSc2d1	pobřežní
oblasti	oblast	k1gFnSc2	oblast
pak	pak	k6eAd1	pak
mají	mít	k5eAaImIp3nP	mít
mírnější	mírný	k2eAgNnSc4d2	mírnější
středomořské	středomořský	k2eAgNnSc4d1	středomořské
klima	klima	k1gNnSc4	klima
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
objevovat	objevovat	k5eAaImF	objevovat
písečné	písečný	k2eAgFnPc4d1	písečná
bouře	bouř	k1gFnPc4	bouř
<g/>
,	,	kIx,	,
na	na	k7c6	na
území	území	k1gNnSc6	území
Libye	Libye	k1gFnSc2	Libye
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
i	i	k9	i
pouštní	pouštní	k2eAgFnPc4d1	pouštní
oázy	oáza	k1gFnPc4	oáza
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
získání	získání	k1gNnSc6	získání
nezávislosti	nezávislost	k1gFnSc2	nezávislost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
byla	být	k5eAaImAgFnS	být
Libye	Libye	k1gFnSc1	Libye
federací	federace	k1gFnPc2	federace
3	[number]	k4	3
historických	historický	k2eAgFnPc2d1	historická
zemí	zem	k1gFnPc2	zem
<g/>
:	:	kIx,	:
Tripolsko	Tripolsko	k1gNnSc1	Tripolsko
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
<g/>
,	,	kIx,	,
Kyrenaika	Kyrenaika	k1gFnSc1	Kyrenaika
na	na	k7c6	na
východě	východ	k1gInSc6	východ
a	a	k8xC	a
Fezzán	Fezzán	k2eAgMnSc1d1	Fezzán
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Federativní	federativní	k2eAgNnSc1d1	federativní
uspořádání	uspořádání	k1gNnSc1	uspořádání
bylo	být	k5eAaImAgNnS	být
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
Libye	Libye	k1gFnSc1	Libye
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
22	[number]	k4	22
okresů	okres	k1gInPc2	okres
(	(	kIx(	(
<g/>
arabský	arabský	k2eAgInSc1d1	arabský
výraz	výraz	k1gInSc1	výraz
shabiyah	shabiyaha	k1gFnPc2	shabiyaha
<g/>
,	,	kIx,	,
ش	ش	k?	ش
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
poloze	poloha	k1gFnSc3	poloha
v	v	k7c6	v
subtropickém	subtropický	k2eAgInSc6d1	subtropický
pásu	pás	k1gInSc6	pás
<g/>
,	,	kIx,	,
suchému	suchý	k2eAgNnSc3d1	suché
podnebí	podnebí	k1gNnSc3	podnebí
a	a	k8xC	a
množství	množství	k1gNnSc4	množství
pouští	poušť	k1gFnPc2	poušť
má	mít	k5eAaImIp3nS	mít
Libye	Libye	k1gFnSc1	Libye
minimum	minimum	k1gNnSc1	minimum
půdy	půda	k1gFnSc2	půda
vhodné	vhodný	k2eAgFnSc2d1	vhodná
pro	pro	k7c4	pro
zemědělství	zemědělství	k1gNnSc4	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
hospodářství	hospodářství	k1gNnSc2	hospodářství
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
orientována	orientovat	k5eAaBmNgFnS	orientovat
k	k	k7c3	k
těžbě	těžba	k1gFnSc3	těžba
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Libye	Libye	k1gFnSc1	Libye
má	mít	k5eAaImIp3nS	mít
zájem	zájem	k1gInSc4	zájem
na	na	k7c6	na
přílivu	příliv	k1gInSc6	příliv
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
investic	investice	k1gFnPc2	investice
do	do	k7c2	do
ropného	ropný	k2eAgInSc2d1	ropný
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
vinou	vina	k1gFnSc7	vina
desetiletí	desetiletí	k1gNnPc2	desetiletí
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
sankcí	sankce	k1gFnPc2	sankce
potýká	potýkat	k5eAaImIp3nS	potýkat
s	s	k7c7	s
celkovou	celkový	k2eAgFnSc7d1	celková
technickou	technický	k2eAgFnSc7d1	technická
zaostalostí	zaostalost	k1gFnSc7	zaostalost
(	(	kIx(	(
<g/>
v	v	k7c4	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
odhadovalo	odhadovat	k5eAaImAgNnS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
obnově	obnova	k1gFnSc3	obnova
ropného	ropný	k2eAgInSc2d1	ropný
průmyslu	průmysl	k1gInSc2	průmysl
třeba	třeba	k6eAd1	třeba
30	[number]	k4	30
miliard	miliarda	k4xCgFnPc2	miliarda
USD	USD	kA	USD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nedávné	dávný	k2eNgFnSc6d1	nedávná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
země	země	k1gFnSc1	země
více	hodně	k6eAd2	hodně
snažila	snažit	k5eAaImAgFnS	snažit
angažovat	angažovat	k5eAaBmF	angažovat
na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
poli	pole	k1gNnSc6	pole
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
členstvím	členství	k1gNnSc7	členství
ve	v	k7c6	v
Světové	světový	k2eAgFnSc6d1	světová
obchodní	obchodní	k2eAgFnSc6d1	obchodní
organizaci	organizace	k1gFnSc6	organizace
(	(	kIx(	(
<g/>
WTO	WTO	kA	WTO
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
Muammar	Muammar	k1gMnSc1	Muammar
Kaddáfí	Kaddáfí	k1gMnSc1	Kaddáfí
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
od	od	k7c2	od
snah	snaha	k1gFnPc2	snaha
vedoucích	vedoucí	k1gMnPc2	vedoucí
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
ZHN	ZHN	kA	ZHN
a	a	k8xC	a
OSN	OSN	kA	OSN
zareagovala	zareagovat	k5eAaPmAgFnS	zareagovat
uvolněním	uvolnění	k1gNnSc7	uvolnění
ekonomických	ekonomický	k2eAgFnPc2d1	ekonomická
sankcí	sankce	k1gFnPc2	sankce
(	(	kIx(	(
<g/>
prosinec	prosinec	k1gInSc1	prosinec
2003	[number]	k4	2003
až	až	k9	až
duben	duben	k1gInSc1	duben
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Libye	Libye	k1gFnSc1	Libye
v	v	k7c6	v
nedávné	dávný	k2eNgFnSc6d1	nedávná
době	doba	k1gFnSc6	doba
diverzifikovala	diverzifikovat	k5eAaImAgFnS	diverzifikovat
své	svůj	k3xOyFgNnSc4	svůj
hospodářství	hospodářství	k1gNnSc4	hospodářství
a	a	k8xC	a
snažila	snažit	k5eAaImAgFnS	snažit
se	se	k3xPyFc4	se
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
některých	některý	k3yIgFnPc2	některý
bohatých	bohatý	k2eAgFnPc2d1	bohatá
ropných	ropný	k2eAgFnPc2d1	ropná
zemí	zem	k1gFnPc2	zem
v	v	k7c6	v
Perském	perský	k2eAgInSc6d1	perský
zálivu	záliv	k1gInSc6	záliv
rozvinout	rozvinout	k5eAaPmF	rozvinout
sektor	sektor	k1gInSc4	sektor
služeb	služba	k1gFnPc2	služba
(	(	kIx(	(
<g/>
potenciál	potenciál	k1gInSc1	potenciál
je	být	k5eAaImIp3nS	být
viděn	vidět	k5eAaImNgInS	vidět
zejména	zejména	k9	zejména
v	v	k7c6	v
cestovním	cestovní	k2eAgInSc6d1	cestovní
ruchu	ruch	k1gInSc6	ruch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
například	například	k6eAd1	například
země	země	k1gFnSc1	země
začala	začít	k5eAaPmAgFnS	začít
modernizovat	modernizovat	k5eAaBmF	modernizovat
svou	svůj	k3xOyFgFnSc4	svůj
leteckou	letecký	k2eAgFnSc4d1	letecká
společnost	společnost	k1gFnSc4	společnost
Afrikíja	Afrikíjum	k1gNnSc2	Afrikíjum
Airways	Airwaysa	k1gFnPc2	Airwaysa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
ambici	ambice	k1gFnSc4	ambice
stát	stát	k5eAaPmF	stát
se	s	k7c7	s
hlavním	hlavní	k2eAgMnSc7d1	hlavní
leteckým	letecký	k2eAgMnSc7d1	letecký
přepravcem	přepravce	k1gMnSc7	přepravce
mezi	mezi	k7c7	mezi
Evropou	Evropa	k1gFnSc7	Evropa
a	a	k8xC	a
Afrikou	Afrika	k1gFnSc7	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
další	další	k2eAgFnPc1d1	další
charakteristiky	charakteristika	k1gFnPc1	charakteristika
<g/>
:	:	kIx,	:
91	[number]	k4	91
%	%	kIx~	%
plochy	plocha	k1gFnSc2	plocha
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
neproduktivní	produktivní	k2eNgFnSc1d1	neproduktivní
půda	půda	k1gFnSc1	půda
<g/>
,	,	kIx,	,
95	[number]	k4	95
%	%	kIx~	%
tvoří	tvořit	k5eAaImIp3nS	tvořit
poušť	poušť	k1gFnSc1	poušť
nebo	nebo	k8xC	nebo
polopoušť	polopoušť	k1gFnSc1	polopoušť
obdělávané	obdělávaný	k2eAgFnSc2d1	obdělávaná
půdy	půda	k1gFnSc2	půda
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
1,5	[number]	k4	1,5
%	%	kIx~	%
75	[number]	k4	75
%	%	kIx~	%
potravin	potravina	k1gFnPc2	potravina
se	s	k7c7	s
(	(	kIx(	(
<g/>
vlivem	vliv	k1gInSc7	vliv
nedostatku	nedostatek	k1gInSc2	nedostatek
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
půdy	půda	k1gFnSc2	půda
<g/>
)	)	kIx)	)
musí	muset	k5eAaImIp3nP	muset
dovážet	dovážet	k5eAaImF	dovážet
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
plodiny	plodina	k1gFnPc4	plodina
vyprodukované	vyprodukovaný	k2eAgFnPc4d1	vyprodukovaná
v	v	k7c6	v
Libyi	Libye	k1gFnSc6	Libye
patří	patřit	k5eAaImIp3nS	patřit
ječmen	ječmen	k1gInSc1	ječmen
<g/>
,	,	kIx,	,
olivy	oliva	k1gFnPc1	oliva
<g/>
,	,	kIx,	,
datle	datle	k1gFnPc1	datle
<g/>
,	,	kIx,	,
fíky	fík	k1gInPc1	fík
<g/>
,	,	kIx,	,
agrumy	agrum	k1gInPc1	agrum
<g/>
,	,	kIx,	,
vinná	vinný	k2eAgFnSc1d1	vinná
réva	réva	k1gFnSc1	réva
<g/>
,	,	kIx,	,
rajčata	rajče	k1gNnPc1	rajče
<g/>
,	,	kIx,	,
tabák	tabák	k1gInSc1	tabák
a	a	k8xC	a
halfa	halfa	k1gFnSc1	halfa
80	[number]	k4	80
%	%	kIx~	%
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
produkce	produkce	k1gFnSc2	produkce
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
pobřeží	pobřeží	k1gNnSc2	pobřeží
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
30	[number]	k4	30
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
95	[number]	k4	95
%	%	kIx~	%
vývozu	vývoz	k1gInSc2	vývoz
tvoří	tvořit	k5eAaImIp3nS	tvořit
<g />
.	.	kIx.	.
</s>
<s>
ropa	ropa	k1gFnSc1	ropa
(	(	kIx(	(
<g/>
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
dokonce	dokonce	k9	dokonce
98	[number]	k4	98
%	%	kIx~	%
<g/>
)	)	kIx)	)
20	[number]	k4	20
%	%	kIx~	%
HDP	HDP	kA	HDP
tvoří	tvořit	k5eAaImIp3nS	tvořit
průmysl	průmysl	k1gInSc1	průmysl
nesouvisející	související	k2eNgInSc1d1	nesouvisející
s	s	k7c7	s
těžbou	těžba	k1gFnSc7	těžba
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
nejvíce	nejvíce	k6eAd1	nejvíce
asi	asi	k9	asi
stavebnictví	stavebnictví	k1gNnSc1	stavebnictví
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
<g/>
)	)	kIx)	)
nejvýznamnější	významný	k2eAgMnPc1d3	nejvýznamnější
obchodní	obchodní	k2eAgMnPc1d1	obchodní
partneři	partner	k1gMnPc1	partner
Libye	Libye	k1gFnSc2	Libye
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
a	a	k8xC	a
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
.	.	kIx.	.
</s>
<s>
Libye	Libye	k1gFnSc1	Libye
vytěží	vytěžit	k5eAaPmIp3nS	vytěžit
denně	denně	k6eAd1	denně
zhruba	zhruba	k6eAd1	zhruba
1,6	[number]	k4	1,6
milionu	milion	k4xCgInSc2	milion
barelů	barel	k1gInPc2	barel
ropy	ropa	k1gFnSc2	ropa
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
jde	jít	k5eAaImIp3nS	jít
1,1	[number]	k4	1,1
milionu	milion	k4xCgInSc2	milion
na	na	k7c4	na
vývoz	vývoz	k1gInSc4	vývoz
a	a	k8xC	a
většinu	většina	k1gFnSc4	většina
ropy	ropa	k1gFnSc2	ropa
odebírá	odebírat	k5eAaImIp3nS	odebírat
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
OPEC	opéct	k5eAaPmRp2nSwK	opéct
je	být	k5eAaImIp3nS	být
co	co	k3yInSc4	co
do	do	k7c2	do
vývozu	vývoz	k1gInSc2	vývoz
ropy	ropa	k1gFnSc2	ropa
12	[number]	k4	12
<g/>
.	.	kIx.	.
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
12	[number]	k4	12
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
má	mít	k5eAaImIp3nS	mít
rovněž	rovněž	k9	rovněž
zásoby	zásoba	k1gFnPc4	zásoba
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
potvrzený	potvrzený	k2eAgInSc1d1	potvrzený
rozsah	rozsah	k1gInSc1	rozsah
činí	činit	k5eAaImIp3nS	činit
53	[number]	k4	53
bilionů	bilion	k4xCgInPc2	bilion
kubických	kubický	k2eAgFnPc2d1	kubická
stop	stopa	k1gFnPc2	stopa
(	(	kIx(	(
<g/>
0,8	[number]	k4	0,8
<g/>
%	%	kIx~	%
celkových	celkový	k2eAgFnPc2d1	celková
světových	světový	k2eAgFnPc2d1	světová
zásob	zásoba	k1gFnPc2	zásoba
plynu	plyn	k1gInSc2	plyn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Libye	Libye	k1gFnSc1	Libye
produkuje	produkovat	k5eAaImIp3nS	produkovat
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
cca	cca	kA	cca
12	[number]	k4	12
miliard	miliarda	k4xCgFnPc2	miliarda
kubických	kubický	k2eAgInPc2d1	kubický
metrů	metr	k1gInPc2	metr
plynu	plyn	k1gInSc2	plyn
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
je	být	k5eAaImIp3nS	být
osm	osm	k4xCc4	osm
miliard	miliarda	k4xCgFnPc2	miliarda
exportováno	exportován	k2eAgNnSc4d1	exportováno
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
podmořským	podmořský	k2eAgInSc7d1	podmořský
plynovodem	plynovod	k1gInSc7	plynovod
Greenstream	Greenstream	k1gInSc1	Greenstream
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemí	zem	k1gFnPc2	zem
však	však	k9	však
neexistují	existovat	k5eNaImIp3nP	existovat
plynové	plynový	k2eAgInPc1d1	plynový
rozvody	rozvod	k1gInPc1	rozvod
a	a	k8xC	a
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
se	se	k3xPyFc4	se
spoléhá	spoléhat	k5eAaImIp3nS	spoléhat
na	na	k7c4	na
propan-butan	propanutan	k1gInSc4	propan-butan
v	v	k7c6	v
lahvích	lahev	k1gFnPc6	lahev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
první	první	k4xOgInSc1	první
rezervoár	rezervoár	k1gInSc1	rezervoár
"	"	kIx"	"
<g/>
fosilní	fosilní	k2eAgFnPc1d1	fosilní
vody	voda	k1gFnPc1	voda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
od	od	k7c2	od
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
pěti	pět	k4xCc6	pět
podložích	podloží	k1gNnPc6	podloží
libyjské	libyjský	k2eAgFnSc2d1	Libyjská
pouště	poušť	k1gFnSc2	poušť
objeveny	objevit	k5eAaPmNgInP	objevit
další	další	k2eAgFnSc3d1	další
významné	významný	k2eAgFnSc3d1	významná
zvodně	zvodna	k1gFnSc3	zvodna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
byla	být	k5eAaImAgFnS	být
provedena	proveden	k2eAgFnSc1d1	provedena
studie	studie	k1gFnSc1	studie
proveditelnosti	proveditelnost	k1gFnSc2	proveditelnost
projektu	projekt	k1gInSc2	projekt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
tuto	tento	k3xDgFnSc4	tento
vodu	voda	k1gFnSc4	voda
pomocí	pomocí	k7c2	pomocí
podzemního	podzemní	k2eAgNnSc2d1	podzemní
potrubí	potrubí	k1gNnSc2	potrubí
a	a	k8xC	a
sítě	síť	k1gFnSc2	síť
čerpadel	čerpadlo	k1gNnPc2	čerpadlo
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
obydlených	obydlený	k2eAgFnPc2d1	obydlená
oblastí	oblast	k1gFnPc2	oblast
u	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
projekt	projekt	k1gInSc1	projekt
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgMnSc1d1	zvaný
The	The	k1gMnSc1	The
Great	Great	k2eAgMnSc1d1	Great
Man-Made	Man-Mad	k1gInSc5	Man-Mad
River	Rivero	k1gNnPc2	Rivero
(	(	kIx(	(
<g/>
Velká	velký	k2eAgFnSc1d1	velká
člověkem	člověk	k1gMnSc7	člověk
vybudovaná	vybudovaný	k2eAgFnSc1d1	vybudovaná
řeka	řeka	k1gFnSc1	řeka
nebo	nebo	k8xC	nebo
Velká	velký	k2eAgFnSc1d1	velká
umělá	umělý	k2eAgFnSc1d1	umělá
řeka	řeka	k1gFnSc1	řeka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1983	[number]	k4	1983
a	a	k8xC	a
rozrostl	rozrůst	k5eAaPmAgInS	rozrůst
se	se	k3xPyFc4	se
do	do	k7c2	do
pěti	pět	k4xCc2	pět
fází	fáze	k1gFnPc2	fáze
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
realizace	realizace	k1gFnSc1	realizace
má	mít	k5eAaImIp3nS	mít
trvat	trvat	k5eAaImF	trvat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
půl	půl	k1xP	půl
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
fáze	fáze	k1gFnSc1	fáze
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
dokončení	dokončení	k1gNnSc4	dokončení
té	ten	k3xDgFnSc2	ten
poslední	poslední	k2eAgFnSc2d1	poslední
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
za	za	k7c4	za
25	[number]	k4	25
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Podnik	podnik	k1gInSc1	podnik
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
stát	stát	k1gInSc1	stát
25	[number]	k4	25
mld	mld	k?	mld
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
stávající	stávající	k2eAgFnSc2d1	stávající
fáze	fáze	k1gFnSc2	fáze
rozšíření	rozšíření	k1gNnSc2	rozšíření
zásobovat	zásobovat	k5eAaImF	zásobovat
zemi	zem	k1gFnSc4	zem
3,68	[number]	k4	3,68
miliony	milion	k4xCgInPc4	milion
m3	m3	k4	m3
vody	voda	k1gFnSc2	voda
denně	denně	k6eAd1	denně
s	s	k7c7	s
cenou	cena	k1gFnSc7	cena
35	[number]	k4	35
centů	cent	k1gInPc2	cent
za	za	k7c4	za
m3	m3	k4	m3
<g/>
.	.	kIx.	.
</s>
<s>
Zásoby	zásoba	k1gFnPc1	zásoba
fosilní	fosilní	k2eAgFnSc2d1	fosilní
vody	voda	k1gFnSc2	voda
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
spotřebě	spotřeba	k1gFnSc6	spotřeba
vydržet	vydržet	k5eAaPmF	vydržet
tisíce	tisíc	k4xCgInPc1	tisíc
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
kapacitě	kapacita	k1gFnSc3	kapacita
má	mít	k5eAaImIp3nS	mít
potenciál	potenciál	k1gInSc1	potenciál
umožnit	umožnit	k5eAaPmF	umožnit
obdělávání	obdělávání	k1gNnSc4	obdělávání
155,000	[number]	k4	155,000
<g/>
ha	ha	kA	ha
dosud	dosud	k6eAd1	dosud
neúrodné	úrodný	k2eNgFnSc2d1	neúrodná
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
zaručit	zaručit	k5eAaPmF	zaručit
všem	všecek	k3xTgMnPc3	všecek
Libyjcům	Libyjec	k1gMnPc3	Libyjec
hojnost	hojnost	k1gFnSc4	hojnost
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
větší	veliký	k2eAgNnSc1d2	veliký
samozásobitelství	samozásobitelství	k1gNnSc1	samozásobitelství
a	a	k8xC	a
menší	malý	k2eAgFnSc1d2	menší
závislost	závislost	k1gFnSc1	závislost
na	na	k7c6	na
dovozu	dovoz	k1gInSc6	dovoz
potravin	potravina	k1gFnPc2	potravina
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
spolupracovalo	spolupracovat	k5eAaImAgNnS	spolupracovat
mnoho	mnoho	k4c1	mnoho
firem	firma	k1gFnPc2	firma
z	z	k7c2	z
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
<g/>
,	,	kIx,	,
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
fosilní	fosilní	k2eAgFnSc2d1	fosilní
vody	voda	k1gFnSc2	voda
využívá	využívat	k5eAaImIp3nS	využívat
Libye	Libye	k1gFnSc1	Libye
také	také	k9	také
odsolování	odsolování	k1gNnSc4	odsolování
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
takto	takto	k6eAd1	takto
získává	získávat	k5eAaImIp3nS	získávat
přes	přes	k7c4	přes
1,8	[number]	k4	1,8
milionu	milion	k4xCgInSc2	milion
m3	m3	k4	m3
sladké	sladký	k2eAgFnSc2d1	sladká
vody	voda	k1gFnSc2	voda
za	za	k7c4	za
den	den	k1gInSc4	den
při	při	k7c6	při
provozních	provozní	k2eAgInPc6d1	provozní
nákladech	náklad	k1gInPc6	náklad
které	který	k3yQgNnSc1	který
činí	činit	k5eAaImIp3nS	činit
93,5	[number]	k4	93,5
milionu	milion	k4xCgInSc2	milion
USD	USD	kA	USD
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
plánuje	plánovat	k5eAaImIp3nS	plánovat
navýšení	navýšení	k1gNnSc2	navýšení
tohoto	tento	k3xDgNnSc2	tento
množství	množství	k1gNnSc2	množství
až	až	k9	až
na	na	k7c4	na
4,2	[number]	k4	4,2
milionu	milion	k4xCgInSc2	milion
m3	m3	k4	m3
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
při	při	k7c6	při
provozních	provozní	k2eAgInPc6d1	provozní
nákladech	náklad	k1gInPc6	náklad
351	[number]	k4	351
milionů	milion	k4xCgInPc2	milion
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
5,3	[number]	k4	5,3
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
6,5	[number]	k4	6,5
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
V	v	k7c6	v
Libyi	Libye	k1gFnSc6	Libye
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
140	[number]	k4	140
kmenů	kmen	k1gInPc2	kmen
a	a	k8xC	a
klanů	klan	k1gInPc2	klan
<g/>
.	.	kIx.	.
</s>
