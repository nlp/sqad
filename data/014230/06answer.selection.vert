<s>
Strach	strach	k1gInSc1
<g/>
,	,	kIx,
nejistota	nejistota	k1gFnSc1
<g/>
,	,	kIx,
pochyby	pochyba	k1gFnPc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Fear	Fear	k1gInSc1
<g/>
,	,	kIx,
Uncertainty	Uncertaint	k1gInPc1
<g/>
,	,	kIx,
Doubt	Doubt	k1gInSc1
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
FUD	FUD	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
marketingová	marketingový	k2eAgFnSc1d1
strategie	strategie	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
rozšiřuje	rozšiřovat	k5eAaImIp3nS
záporné	záporný	k2eAgFnPc4d1
<g/>
,	,	kIx,
nejasné	jasný	k2eNgFnPc4d1
či	či	k8xC
nepřesné	přesný	k2eNgFnPc4d1
informace	informace	k1gFnPc4
o	o	k7c6
konkurenčním	konkurenční	k2eAgInSc6d1
výrobku	výrobek	k1gInSc6
<g/>
.	.	kIx.
</s>