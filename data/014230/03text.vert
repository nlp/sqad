<s>
Strach	strach	k1gInSc1
<g/>
,	,	kIx,
nejistota	nejistota	k1gFnSc1
<g/>
,	,	kIx,
pochyby	pochyba	k1gFnPc1
</s>
<s>
Strach	strach	k1gInSc1
<g/>
,	,	kIx,
nejistota	nejistota	k1gFnSc1
<g/>
,	,	kIx,
pochyby	pochyba	k1gFnPc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Fear	Fear	k1gInSc1
<g/>
,	,	kIx,
Uncertainty	Uncertaint	k1gInPc1
<g/>
,	,	kIx,
Doubt	Doubt	k1gInSc1
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
FUD	FUD	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
marketingová	marketingový	k2eAgFnSc1d1
strategie	strategie	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
rozšiřuje	rozšiřovat	k5eAaImIp3nS
záporné	záporný	k2eAgFnPc4d1
<g/>
,	,	kIx,
nejasné	jasný	k2eNgFnPc4d1
či	či	k8xC
nepřesné	přesný	k2eNgFnPc4d1
informace	informace	k1gFnPc4
o	o	k7c6
konkurenčním	konkurenční	k2eAgInSc6d1
výrobku	výrobek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
FUD	FUD	kA
se	se	k3xPyFc4
původně	původně	k6eAd1
označovaly	označovat	k5eAaImAgFnP
dezinformační	dezinformační	k2eAgFnPc4d1
taktiky	taktika	k1gFnPc4
používané	používaný	k2eAgFnPc4d1
výrobci	výrobce	k1gMnPc7
počítačového	počítačový	k2eAgMnSc4d1
hardware	hardware	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
později	pozdě	k6eAd2
se	se	k3xPyFc4
pojem	pojem	k1gInSc1
rozšířil	rozšířit	k5eAaPmAgInS
i	i	k9
do	do	k7c2
jiných	jiný	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
a	a	k8xC
dnes	dnes	k6eAd1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
i	i	k9
ve	v	k7c6
spojení	spojení	k1gNnSc6
s	s	k7c7
politikou	politika	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obecněji	obecně	k6eAd2
značí	značit	k5eAaImIp3nS
FUD	FUD	kA
také	také	k9
souhrn	souhrn	k1gInSc4
rozšířených	rozšířený	k2eAgInPc2d1
mylných	mylný	k2eAgInPc2d1
dojmů	dojem	k1gInPc2
o	o	k7c6
nějaké	nějaký	k3yIgFnSc6
záležitosti	záležitost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Vznik	vznik	k1gInSc1
pojmu	pojem	k1gInSc2
FUD	FUD	kA
je	být	k5eAaImIp3nS
spojen	spojen	k2eAgInSc1d1
s	s	k7c7
americkou	americký	k2eAgFnSc7d1
počítačovou	počítačový	k2eAgFnSc7d1
firmou	firma	k1gFnSc7
IBM	IBM	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
je	být	k5eAaImIp3nS
však	však	k9
z	z	k7c2
šíření	šíření	k1gNnSc2
dezinformací	dezinformace	k1gFnSc7
o	o	k7c4
konkurenci	konkurence	k1gFnSc4
obviňováno	obviňovat	k5eAaImNgNnS
mnoho	mnoho	k4c1
dalších	další	k2eAgFnPc2d1
velkých	velký	k2eAgFnPc2d1
firem	firma	k1gFnPc2
nejen	nejen	k6eAd1
z	z	k7c2
počítačového	počítačový	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Tuto	tento	k3xDgFnSc4
taktiku	taktika	k1gFnSc4
poprvé	poprvé	k6eAd1
popsal	popsat	k5eAaPmAgMnS
americký	americký	k2eAgMnSc1d1
počítačový	počítačový	k2eAgMnSc1d1
architekt	architekt	k1gMnSc1
Gene	gen	k1gInSc5
Amdahl	Amdahl	k1gMnPc3
po	po	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
opustil	opustit	k5eAaPmAgInS
IBM	IBM	kA
a	a	k8xC
založil	založit	k5eAaPmAgInS
vlastní	vlastní	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řekl	říct	k5eAaPmAgMnS
tehdy	tehdy	k6eAd1
<g/>
:	:	kIx,
„	„	k?
<g/>
FUD	FUD	kA
je	být	k5eAaImIp3nS
strach	strach	k1gInSc1
<g/>
,	,	kIx,
nejistota	nejistota	k1gFnSc1
a	a	k8xC
pochybnost	pochybnost	k1gFnSc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
obchodníci	obchodník	k1gMnPc1
od	od	k7c2
IBM	IBM	kA
vnucují	vnucovat	k5eAaImIp3nP
potenciálním	potenciální	k2eAgMnPc3d1
zákazníkům	zákazník	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
by	by	kYmCp3nP
mohli	moct	k5eAaImAgMnP
uvažovat	uvažovat	k5eAaImF
o	o	k7c6
produktech	produkt	k1gInPc6
Amdahl	Amdahl	k1gFnPc2
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
O	o	k7c6
vzniku	vznik	k1gInSc6
FUD	FUD	kA
pojednává	pojednávat	k5eAaImIp3nS
také	také	k9
Eric	Eric	k1gInSc1
S.	S.	kA
Raymond	Raymond	k1gInSc1
<g/>
,	,	kIx,
autor	autor	k1gMnSc1
knihy	kniha	k1gFnSc2
Katedrála	katedrála	k1gFnSc1
a	a	k8xC
tržiště	tržiště	k1gNnSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Cílem	cíl	k1gInSc7
samozřejmě	samozřejmě	k6eAd1
bylo	být	k5eAaImAgNnS
přesvědčit	přesvědčit	k5eAaPmF
kupující	kupující	k2eAgMnSc1d1
používat	používat	k5eAaImF
vybavení	vybavení	k1gNnSc4
od	od	k7c2
IBM	IBM	kA
místo	místo	k7c2
toho	ten	k3xDgNnSc2
konkurenčního	konkurenční	k2eAgNnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
zjevný	zjevný	k2eAgInSc1d1
nátlak	nátlak	k1gInSc1
byl	být	k5eAaImAgInS
obvykle	obvykle	k6eAd1
prováděn	provádět	k5eAaImNgInS
za	za	k7c2
pomoci	pomoc	k1gFnSc2
příslibů	příslib	k1gInPc2
<g/>
,	,	kIx,
že	že	k8xS
těm	ten	k3xDgMnPc3
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
zůstanou	zůstat	k5eAaPmIp3nP
věrní	věrný	k2eAgMnPc1d1
IBM	IBM	kA
<g/>
,	,	kIx,
se	se	k3xPyFc4
dobře	dobře	k6eAd1
povede	povést	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
ty	ten	k3xDgMnPc4
<g/>
,	,	kIx,
kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
si	se	k3xPyFc3
pořídí	pořídit	k5eAaPmIp3nS
konkurenční	konkurenční	k2eAgNnSc4d1
zařízení	zařízení	k1gNnSc4
<g/>
,	,	kIx,
čeká	čekat	k5eAaImIp3nS
černá	černý	k2eAgFnSc1d1
budoucnost	budoucnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
se	se	k3xPyFc4
pojem	pojem	k1gInSc1
rozšířil	rozšířit	k5eAaPmAgInS
jako	jako	k9
označení	označení	k1gNnSc4
pro	pro	k7c4
jakýkoliv	jakýkoliv	k3yIgInSc4
druh	druh	k1gInSc4
dezinformací	dezinformace	k1gFnPc2
používaných	používaný	k2eAgInPc2d1
jako	jako	k8xS,k8xC
zbraň	zbraň	k1gFnSc4
v	v	k7c6
konkurenčním	konkurenční	k2eAgInSc6d1
boji	boj	k1gInSc6
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
Odpůrci	odpůrce	k1gMnPc1
velkých	velký	k2eAgFnPc2d1
počítačových	počítačový	k2eAgFnPc2d1
společností	společnost	k1gFnPc2
zastávají	zastávat	k5eAaImIp3nP
názor	názor	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
šíření	šíření	k1gNnSc4
strachu	strach	k1gInSc2
<g/>
,	,	kIx,
nejistoty	nejistota	k1gFnSc2
a	a	k8xC
pochybností	pochybnost	k1gFnPc2
je	být	k5eAaImIp3nS
běžná	běžný	k2eAgFnSc1d1
marketingová	marketingový	k2eAgFnSc1d1
technika	technika	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
tyto	tento	k3xDgFnPc4
společnosti	společnost	k1gFnPc1
vědomě	vědomě	k6eAd1
používají	používat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šíření	šíření	k1gNnSc1
informací	informace	k1gFnPc2
o	o	k7c6
údajných	údajný	k2eAgInPc6d1
nedostatcích	nedostatek	k1gInPc6
méně	málo	k6eAd2
známých	známý	k2eAgInPc2d1
výrobků	výrobek	k1gInPc2
umožňuje	umožňovat	k5eAaImIp3nS
zavedeným	zavedený	k2eAgFnPc3d1
společnostem	společnost	k1gFnPc3
odrazovat	odrazovat	k5eAaImF
zákazníky	zákazník	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
rozhodují	rozhodovat	k5eAaImIp3nP
mezi	mezi	k7c7
jejich	jejich	k3xOp3gFnSc7
a	a	k8xC
konkurenčním	konkurenční	k2eAgInSc7d1
výrobkem	výrobek	k1gInSc7
<g/>
,	,	kIx,
aniž	aniž	k8xC,k8xS
by	by	kYmCp3nS
došlo	dojít	k5eAaPmAgNnS
na	na	k7c6
srovnání	srovnání	k1gNnSc6
skutečných	skutečný	k2eAgFnPc2d1
technických	technický	k2eAgFnPc2d1
vlastností	vlastnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Názornou	názorný	k2eAgFnSc7d1
ukázkou	ukázka	k1gFnSc7
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
například	například	k6eAd1
tradiční	tradiční	k2eAgFnSc1d1
poučka	poučka	k1gFnSc1
nákupčích	nákupčí	k1gMnPc2
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
za	za	k7c4
nákup	nákup	k1gInSc4
výrobků	výrobek	k1gInPc2
IBM	IBM	kA
ještě	ještě	k9
nikoho	nikdo	k3yNnSc4
nevyhodili	vyhodit	k5eNaPmAgMnP
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Halloween	Halloween	k2eAgInSc1d1
documents	documents	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1998	#num#	k4
(	(	kIx(
<g/>
vnitřní	vnitřní	k2eAgInPc4d1
dokumenty	dokument	k1gInPc4
Microsoftu	Microsoft	k1gInSc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc1
existenci	existence	k1gFnSc4
sami	sám	k3xTgMnPc1
potvrdili	potvrdit	k5eAaPmAgMnP
<g/>
)	)	kIx)
používají	používat	k5eAaImIp3nP
termín	termín	k1gInSc4
FUD	FUD	kA
k	k	k7c3
popsání	popsání	k1gNnSc3
potenciální	potenciální	k2eAgFnSc2d1
taktiky	taktika	k1gFnSc2
proti	proti	k7c3
open	open	k1gInSc1
source	source	k1gMnSc4
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
jsou	být	k5eAaImIp3nP
dlouhodobě	dlouhodobě	k6eAd1
důvěryhodní	důvěryhodný	k2eAgMnPc1d1
–	–	k?
proto	proto	k8xC
taktika	taktika	k1gFnSc1
FUD	FUD	kA
nemůže	moct	k5eNaImIp3nS
být	být	k5eAaImF
použita	použít	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
zkratek	zkratka	k1gFnPc2
v	v	k7c6
online	onlinout	k5eAaPmIp3nS
diskusích	diskuse	k1gFnPc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
What	What	k1gInSc1
is	is	k?
FUD	FUD	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Archivováno	archivovat	k5eAaBmNgNnS
16	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
</s>
<s>
Microsoft	Microsoft	kA
tvrdě	tvrdě	k6eAd1
útočí	útočit	k5eAaImIp3nS
na	na	k7c4
Linux	linux	k1gInSc4
v	v	k7c6
rámci	rámec	k1gInSc6
kampaně	kampaň	k1gFnSc2
za	za	k7c4
Windows	Windows	kA
7	#num#	k4
<g/>
,	,	kIx,
DSL	DSL	kA
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
