<p>
<s>
Barva	barva	k1gFnSc1	barva
diamantů	diamant	k1gInPc2	diamant
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
velmi	velmi	k6eAd1	velmi
různá	různý	k2eAgNnPc1d1	různé
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
diamant	diamant	k1gInSc1	diamant
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
za	za	k7c2	za
dokonalých	dokonalý	k2eAgFnPc2d1	dokonalá
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
bezbarvý	bezbarvý	k2eAgMnSc1d1	bezbarvý
<g/>
.	.	kIx.	.
</s>
<s>
Bezbarvost	bezbarvost	k1gFnSc1	bezbarvost
u	u	k7c2	u
diamantů	diamant	k1gInPc2	diamant
je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
ceněna	ceněn	k2eAgFnSc1d1	ceněna
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
drahokamů	drahokam	k1gInPc2	drahokam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
nežádoucí	žádoucí	k2eNgFnSc1d1	nežádoucí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
škálou	škála	k1gFnSc7	škála
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
bezbarvých	bezbarvý	k2eAgInPc2d1	bezbarvý
diamantů	diamant	k1gInPc2	diamant
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
lehce	lehko	k6eAd1	lehko
zažloutlé	zažloutlý	k2eAgFnPc4d1	zažloutlá
<g/>
,	,	kIx,	,
až	až	k9	až
k	k	k7c3	k
oranžovým	oranžový	k2eAgFnPc3d1	oranžová
<g/>
,	,	kIx,	,
růžovým	růžový	k2eAgFnPc3d1	růžová
<g/>
,	,	kIx,	,
fialovým	fialový	k2eAgFnPc3d1	fialová
<g/>
,	,	kIx,	,
modrým	modrý	k2eAgFnPc3d1	modrá
nebo	nebo	k8xC	nebo
i	i	k9	i
extrémně	extrémně	k6eAd1	extrémně
vzácným	vzácný	k2eAgInPc3d1	vzácný
červeným	červený	k2eAgInPc3d1	červený
kamenům	kámen	k1gInPc3	kámen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
zabarvení	zabarvení	k1gNnSc2	zabarvení
==	==	k?	==
</s>
</p>
<p>
<s>
Barva	barva	k1gFnSc1	barva
diamantu	diamant	k1gInSc2	diamant
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
složení	složení	k1gNnSc4	složení
<g/>
.	.	kIx.	.
</s>
<s>
Chemicky	chemicky	k6eAd1	chemicky
naprosto	naprosto	k6eAd1	naprosto
čistý	čistý	k2eAgInSc1d1	čistý
diamant	diamant	k1gInSc1	diamant
je	být	k5eAaImIp3nS	být
bezbarvý	bezbarvý	k2eAgInSc1d1	bezbarvý
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
barvy	barva	k1gFnPc1	barva
diamantu	diamant	k1gInSc2	diamant
jsou	být	k5eAaImIp3nP	být
závislé	závislý	k2eAgInPc1d1	závislý
na	na	k7c6	na
stopovém	stopový	k2eAgNnSc6d1	stopové
množství	množství	k1gNnSc6	množství
ostatních	ostatní	k2eAgInPc2d1	ostatní
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
diamant	diamant	k1gInSc1	diamant
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
ve	v	k7c6	v
šperkařství	šperkařství	k1gNnSc6	šperkařství
často	často	k6eAd1	často
používané	používaný	k2eAgInPc1d1	používaný
<g/>
,	,	kIx,	,
lehce	lehko	k6eAd1	lehko
nažloutlé	nažloutlý	k2eAgInPc1d1	nažloutlý
diamanty	diamant	k1gInPc1	diamant
Kapské	kapský	k2eAgFnSc2d1	kapská
série	série	k1gFnSc2	série
<g/>
,	,	kIx,	,
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
příměsi	příměs	k1gFnPc1	příměs
dusíku	dusík	k1gInSc2	dusík
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
barvu	barva	k1gFnSc4	barva
diamantu	diamant	k1gInSc2	diamant
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
vliv	vliv	k1gInSc1	vliv
deformace	deformace	k1gFnSc2	deformace
krystalové	krystalový	k2eAgFnSc2d1	krystalová
mřížky	mřížka	k1gFnSc2	mřížka
(	(	kIx(	(
<g/>
abstrakce	abstrakce	k1gFnSc1	abstrakce
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
popisuje	popisovat	k5eAaImIp3nS	popisovat
polohu	poloha	k1gFnSc4	poloha
částic	částice	k1gFnPc2	částice
v	v	k7c6	v
krystalu	krystal	k1gInSc6	krystal
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Určování	určování	k1gNnSc4	určování
barvy	barva	k1gFnPc1	barva
==	==	k?	==
</s>
</p>
<p>
<s>
Barva	barva	k1gFnSc1	barva
diamantu	diamant	k1gInSc2	diamant
se	se	k3xPyFc4	se
posuzuje	posuzovat	k5eAaImIp3nS	posuzovat
dle	dle	k7c2	dle
barevné	barevný	k2eAgFnSc2d1	barevná
stupnice	stupnice	k1gFnSc2	stupnice
GIA	GIA	kA	GIA
(	(	kIx(	(
<g/>
Gemological	Gemological	k1gMnSc1	Gemological
Institute	institut	k1gInSc5	institut
of	of	k?	of
America	Americum	k1gNnPc1	Americum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
diamanty	diamant	k1gInPc4	diamant
dělí	dělit	k5eAaImIp3nS	dělit
dle	dle	k7c2	dle
barev	barva	k1gFnPc2	barva
za	za	k7c7	za
pomocí	pomoc	k1gFnSc7	pomoc
písmen	písmeno	k1gNnPc2	písmeno
abecedy	abeceda	k1gFnSc2	abeceda
od	od	k7c2	od
D	D	kA	D
po	po	k7c6	po
Z.	Z.	kA	Z.
Písmeno	písmeno	k1gNnSc1	písmeno
D	D	kA	D
označuje	označovat	k5eAaImIp3nS	označovat
naprosto	naprosto	k6eAd1	naprosto
bezbarvé	bezbarvý	k2eAgInPc4d1	bezbarvý
diamanty	diamant	k1gInPc4	diamant
(	(	kIx(	(
<g/>
prvotřídně	prvotřídně	k6eAd1	prvotřídně
bílé	bílý	k2eAgFnPc1d1	bílá
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yInSc7	co
větší	veliký	k2eAgMnSc1d2	veliký
písmeno	písmeno	k1gNnSc4	písmeno
v	v	k7c6	v
abecedě	abeceda	k1gFnSc6	abeceda
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
více	hodně	k6eAd2	hodně
je	být	k5eAaImIp3nS	být
kámen	kámen	k1gInSc1	kámen
zabarven	zabarven	k2eAgInSc1d1	zabarven
<g/>
.	.	kIx.	.
</s>
<s>
Diamanty	diamant	k1gInPc1	diamant
s	s	k7c7	s
písmeny	písmeno	k1gNnPc7	písmeno
M-O	M-O	k1gMnPc2	M-O
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
nažloutlé	nažloutlý	k2eAgFnPc1d1	nažloutlá
až	až	k8xS	až
žluté	žlutý	k2eAgFnPc1d1	žlutá
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yInSc7	co
je	být	k5eAaImIp3nS	být
diamant	diamant	k1gInSc1	diamant
bezbarvější	bezbarvý	k2eAgInSc1d2	bezbarvý
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
i	i	k9	i
dražší	drahý	k2eAgMnSc1d2	dražší
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
fancy	fanc	k2eAgInPc1d1	fanc
diamanty	diamant	k1gInPc1	diamant
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
sytě	sytě	k6eAd1	sytě
barevné	barevný	k2eAgFnPc1d1	barevná
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
ty	ten	k3xDgMnPc4	ten
se	se	k3xPyFc4	se
stupnice	stupnice	k1gFnSc1	stupnice
GIA	GIA	kA	GIA
nevztahuje	vztahovat	k5eNaImIp3nS	vztahovat
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc7	svůj
cenou	cena	k1gFnSc7	cena
mohou	moct	k5eAaImIp3nP	moct
překonat	překonat	k5eAaPmF	překonat
i	i	k9	i
dokonale	dokonale	k6eAd1	dokonale
bezbarvé	bezbarvý	k2eAgInPc4d1	bezbarvý
diamaty	diamat	k1gInPc4	diamat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Barva	barva	k1gFnSc1	barva
se	se	k3xPyFc4	se
posuzuje	posuzovat	k5eAaImIp3nS	posuzovat
pomocí	pomocí	k7c2	pomocí
porovnávací	porovnávací	k2eAgFnSc2d1	porovnávací
sady	sada	k1gFnSc2	sada
kamenů	kámen	k1gInPc2	kámen
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
master	master	k1gMnSc1	master
stones	stones	k1gMnSc1	stones
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pod	pod	k7c7	pod
lampou	lampa	k1gFnSc7	lampa
imitující	imitující	k2eAgNnSc1d1	imitující
denní	denní	k2eAgNnSc1d1	denní
světlo	světlo	k1gNnSc1	světlo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
určení	určení	k1gNnSc3	určení
barvy	barva	k1gFnSc2	barva
kamene	kámen	k1gInSc2	kámen
se	se	k3xPyFc4	se
také	také	k9	také
používá	používat	k5eAaImIp3nS	používat
elektronický	elektronický	k2eAgInSc4d1	elektronický
kolorimetr	kolorimetr	k1gInSc4	kolorimetr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
popisující	popisující	k2eAgFnSc1d1	popisující
vše	všechen	k3xTgNnSc4	všechen
o	o	k7c6	o
diamantech	diamant	k1gInPc6	diamant
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
jejich	jejich	k3xOp3gFnSc2	jejich
barvy	barva	k1gFnSc2	barva
</s>
</p>
<p>
<s>
http://www.brilianty.cz/barva-diamantu-p34.html	[url]	k1gMnSc1	http://www.brilianty.cz/barva-diamantu-p34.html
</s>
</p>
<p>
<s>
Hodnocení	hodnocení	k1gNnSc1	hodnocení
barev	barva	k1gFnPc2	barva
diamantu	diamant	k1gInSc2	diamant
na	na	k7c6	na
webu	web	k1gInSc6	web
VVDiamonds	VVDiamondsa	k1gFnPc2	VVDiamondsa
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hodnocení	hodnocení	k1gNnSc1	hodnocení
(	(	kIx(	(
<g/>
fancy	fanca	k1gFnPc1	fanca
<g/>
)	)	kIx)	)
barevných	barevný	k2eAgInPc2d1	barevný
diamantů	diamant	k1gInPc2	diamant
na	na	k7c6	na
webu	web	k1gInSc6	web
VVDiamonds	VVDiamondsa	k1gFnPc2	VVDiamondsa
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
GIA	GIA	kA	GIA
-	-	kIx~	-
Přehledná	přehledný	k2eAgFnSc1d1	přehledná
tabulka	tabulka	k1gFnSc1	tabulka
hodnocení	hodnocení	k1gNnSc2	hodnocení
barev	barva	k1gFnPc2	barva
diamatu	diamat	k1gInSc2	diamat
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
