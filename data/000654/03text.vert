<s>
Svatava	Svatava	k1gFnSc1	Svatava
Polská	polský	k2eAgFnSc1d1	polská
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1046-1048	[number]	k4	1046-1048
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1126	[number]	k4	1126
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
třetí	třetí	k4xOgFnSc7	třetí
manželkou	manželka	k1gFnSc7	manželka
Vratislava	Vratislav	k1gMnSc2	Vratislav
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1085	[number]	k4	1085
první	první	k4xOgFnSc7	první
českou	český	k2eAgFnSc7d1	Česká
královnou	královna	k1gFnSc7	královna
<g/>
.	.	kIx.	.
</s>
<s>
Svatava	Svatava	k1gFnSc1	Svatava
byla	být	k5eAaImAgFnS	být
dcerou	dcera	k1gFnSc7	dcera
polského	polský	k2eAgNnSc2d1	polské
knížete	kníže	k1gNnSc2wR	kníže
Kazimíra	Kazimír	k1gMnSc2	Kazimír
I.	I.	kA	I.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
choti	choť	k1gFnPc1	choť
Dobroněgy	Dobroněga	k1gFnSc2	Dobroněga
Kyjevské	kyjevský	k2eAgFnPc1d1	Kyjevská
<g/>
,	,	kIx,	,
dcery	dcera	k1gFnPc1	dcera
Vladimíra	Vladimír	k1gMnSc2	Vladimír
I.	I.	kA	I.
Kyjevského	kyjevský	k2eAgMnSc2d1	kyjevský
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
prababičce	prababička	k1gFnSc6	prababička
Doubravce	Doubravka	k1gFnSc6	Doubravka
byla	být	k5eAaImAgFnS	být
spřízněna	spříznit	k5eAaPmNgFnS	spříznit
s	s	k7c7	s
Přemyslovci	Přemyslovec	k1gMnPc7	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Jejími	její	k3xOp3gMnPc7	její
bratry	bratr	k1gMnPc7	bratr
byli	být	k5eAaImAgMnP	být
polští	polský	k2eAgMnPc1d1	polský
panovníci	panovník	k1gMnPc1	panovník
Boleslav	Boleslav	k1gMnSc1	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Smělý	Smělý	k1gMnSc1	Smělý
a	a	k8xC	a
Vladislav	Vladislav	k1gMnSc1	Vladislav
I.	I.	kA	I.
Heřman	Heřman	k1gMnSc1	Heřman
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
sňatku	sňatek	k1gInSc3	sňatek
Svatavy	Svatava	k1gFnSc2	Svatava
s	s	k7c7	s
Vratislavem	Vratislav	k1gMnSc7	Vratislav
došlo	dojít	k5eAaPmAgNnS	dojít
zřejmě	zřejmě	k6eAd1	zřejmě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1062	[number]	k4	1062
<g/>
,	,	kIx,	,
rok	rok	k1gInSc4	rok
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
předchozí	předchozí	k2eAgFnSc2d1	předchozí
kněžny	kněžna	k1gFnSc2	kněžna
<g/>
,	,	kIx,	,
Vratislavovy	Vratislavův	k2eAgFnSc2d1	Vratislavova
druhé	druhý	k4xOgFnSc2	druhý
manželky	manželka	k1gFnSc2	manželka
Adléty	Adléta	k1gFnSc2	Adléta
Uherské	uherský	k2eAgFnSc2d1	uherská
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
první	první	k4xOgFnSc1	první
manželka	manželka	k1gFnSc1	manželka
zemřela	zemřít	k5eAaPmAgFnS	zemřít
při	při	k7c6	při
předčasném	předčasný	k2eAgInSc6d1	předčasný
porodu	porod	k1gInSc6	porod
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
ji	on	k3xPp3gFnSc4	on
Spytihněv	Spytihněv	k1gFnSc4	Spytihněv
II	II	kA	II
<g/>
.	.	kIx.	.
věznil	věznit	k5eAaImAgMnS	věznit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
německých	německý	k2eAgMnPc2d1	německý
historiků	historik	k1gMnPc2	historik
bylo	být	k5eAaImAgNnS	být
Svatavě	Svatava	k1gFnSc6	Svatava
v	v	k7c6	v
době	doba	k1gFnSc6	doba
sňatku	sňatek	k1gInSc2	sňatek
asi	asi	k9	asi
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Oswalda	Oswaldo	k1gNnSc2	Oswaldo
Balzera	Balzera	k1gFnSc1	Balzera
19	[number]	k4	19
až	až	k9	až
22	[number]	k4	22
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
ke	k	k7c3	k
svatbě	svatba	k1gFnSc3	svatba
bylo	být	k5eAaImAgNnS	být
zachování	zachování	k1gNnSc1	zachování
neutrality	neutralita	k1gFnSc2	neutralita
Čech	Čechy	k1gFnPc2	Čechy
v	v	k7c6	v
polsko-německém	polskoěmecký	k2eAgInSc6d1	polsko-německý
konfliktu	konflikt	k1gInSc6	konflikt
a	a	k8xC	a
Vratislav	Vratislav	k1gMnSc1	Vratislav
tak	tak	k6eAd1	tak
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
přátelské	přátelský	k2eAgInPc4d1	přátelský
vztahy	vztah	k1gInPc4	vztah
se	s	k7c7	s
Svataviným	Svatavin	k2eAgMnSc7d1	Svatavin
bratrem	bratr	k1gMnSc7	bratr
<g/>
,	,	kIx,	,
polským	polský	k2eAgMnSc7d1	polský
knížetem	kníže	k1gMnSc7	kníže
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
králem	král	k1gMnSc7	král
<g/>
)	)	kIx)	)
Boleslavem	Boleslav	k1gMnSc7	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Smělým	Smělý	k1gMnPc3	Smělý
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgNnSc7	který
ale	ale	k8xC	ale
Vratislav	Vratislav	k1gFnSc1	Vratislav
později	pozdě	k6eAd2	pozdě
bojoval	bojovat	k5eAaImAgInS	bojovat
o	o	k7c4	o
česko-polské	českoolský	k2eAgFnPc4d1	česko-polská
hranice	hranice	k1gFnPc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Vratislavovým	Vratislavův	k2eAgInPc3d1	Vratislavův
čtyřem	čtyři	k4xCgInPc3	čtyři
dětem	dítě	k1gFnPc3	dítě
Svatava	Svatava	k1gFnSc1	Svatava
přidala	přidat	k5eAaPmAgFnS	přidat
Boleslava	Boleslav	k1gMnSc4	Boleslav
<g/>
,	,	kIx,	,
Bořivoje	Bořivoj	k1gMnSc4	Bořivoj
<g/>
,	,	kIx,	,
Vladislava	Vladislav	k1gMnSc4	Vladislav
<g/>
,	,	kIx,	,
Soběslava	Soběslav	k1gMnSc4	Soběslav
a	a	k8xC	a
Juditu	Judita	k1gFnSc4	Judita
<g/>
.	.	kIx.	.
</s>
<s>
Nejmladší	mladý	k2eAgMnSc1d3	nejmladší
potomek	potomek	k1gMnSc1	potomek
<g/>
,	,	kIx,	,
Soběslav	Soběslav	k1gMnSc1	Soběslav
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
zřejmě	zřejmě	k6eAd1	zřejmě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1075	[number]	k4	1075
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
už	už	k6eAd1	už
byla	být	k5eAaImAgFnS	být
Svatava	Svatava	k1gFnSc1	Svatava
zralou	zralý	k2eAgFnSc4d1	zralá
ženou	žena	k1gFnSc7	žena
a	a	k8xC	a
matkou	matka	k1gFnSc7	matka
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
snad	snad	k9	snad
měla	mít	k5eAaImAgFnS	mít
k	k	k7c3	k
nejmladšímu	mladý	k2eAgMnSc3d3	nejmladší
synovi	syn	k1gMnSc3	syn
poněkud	poněkud	k6eAd1	poněkud
jiný	jiný	k2eAgInSc4d1	jiný
vztah	vztah	k1gInSc4	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgMnPc1	tři
z	z	k7c2	z
jejích	její	k3xOp3gMnPc2	její
synů	syn	k1gMnPc2	syn
-	-	kIx~	-
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
<g/>
,	,	kIx,	,
Vladislav	Vladislav	k1gMnSc1	Vladislav
a	a	k8xC	a
Soběslav	Soběslav	k1gMnSc1	Soběslav
-	-	kIx~	-
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
knížaty	kníže	k1gMnPc7wR	kníže
v	v	k7c6	v
neklidných	klidný	k2eNgInPc6d1	neklidný
letech	let	k1gInPc6	let
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
jejich	jejich	k3xOp3gMnSc2	jejich
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
dceru	dcera	k1gFnSc4	dcera
Juditu	Judita	k1gFnSc4	Judita
její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
provdal	provdat	k5eAaPmAgMnS	provdat
za	za	k7c4	za
svého	svůj	k3xOyFgMnSc4	svůj
přítele	přítel	k1gMnSc4	přítel
a	a	k8xC	a
rádce	rádce	k1gMnPc4	rádce
Wiprechta	Wiprecht	k1gMnSc4	Wiprecht
Grojčského	Grojčský	k2eAgMnSc4d1	Grojčský
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
manželce	manželka	k1gFnSc6	manželka
knížete	kníže	k1gMnSc2	kníže
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
nezachovalo	zachovat	k5eNaPmAgNnS	zachovat
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
příliš	příliš	k6eAd1	příliš
zpráv	zpráva	k1gFnPc2	zpráva
<g/>
,	,	kIx,	,
snad	snad	k9	snad
jen	jen	k9	jen
ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
podporovala	podporovat	k5eAaImAgFnS	podporovat
vyšehradskou	vyšehradský	k2eAgFnSc4d1	Vyšehradská
kapitulu	kapitula	k1gFnSc4	kapitula
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1085	[number]	k4	1085
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
dvorský	dvorský	k2eAgInSc1d1	dvorský
sjezd	sjezd	k1gInSc1	sjezd
v	v	k7c6	v
Mohuči	Mohuč	k1gFnSc6	Mohuč
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
významně	významně	k6eAd1	významně
zapsal	zapsat	k5eAaPmAgInS	zapsat
do	do	k7c2	do
českých	český	k2eAgFnPc2d1	Česká
dějin	dějiny	k1gFnPc2	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Vratislav	Vratislav	k1gMnSc1	Vratislav
II	II	kA	II
<g/>
.	.	kIx.	.
obdržel	obdržet	k5eAaPmAgInS	obdržet
od	od	k7c2	od
císaře	císař	k1gMnSc4	císař
Jindřicha	Jindřich	k1gMnSc2	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
za	za	k7c4	za
své	svůj	k3xOyFgFnPc4	svůj
věrné	věrný	k2eAgFnPc4d1	věrná
služby	služba	k1gFnPc4	služba
královskou	královský	k2eAgFnSc4d1	královská
korunu	koruna	k1gFnSc4	koruna
(	(	kIx(	(
<g/>
ovšem	ovšem	k9	ovšem
jen	jen	k9	jen
nedědičnou	dědičný	k2eNgFnSc4d1	nedědičná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
zbaven	zbavit	k5eAaPmNgMnS	zbavit
poplatků	poplatek	k1gInPc2	poplatek
a	a	k8xC	a
povinován	povinován	k2eAgInSc1d1	povinován
účastí	účast	k1gFnSc7	účast
českých	český	k2eAgMnPc2d1	český
vládců	vládce	k1gMnPc2	vládce
s	s	k7c7	s
družinou	družina	k1gFnSc7	družina
na	na	k7c6	na
korunovačních	korunovační	k2eAgFnPc6d1	korunovační
cestách	cesta	k1gFnPc6	cesta
římskoněmeckých	římskoněmecký	k2eAgMnPc2d1	římskoněmecký
panovníků	panovník	k1gMnPc2	panovník
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
uvolňovaly	uvolňovat	k5eAaImAgFnP	uvolňovat
vazby	vazba	k1gFnPc4	vazba
českých	český	k2eAgMnPc2d1	český
a	a	k8xC	a
německých	německý	k2eAgMnPc2d1	německý
panovníků	panovník	k1gMnPc2	panovník
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1085	[number]	k4	1085
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
trevírský	trevírský	k2eAgMnSc1d1	trevírský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Egilbert	Egilbert	k1gMnSc1	Egilbert
Vratislava	Vratislava	k1gFnSc1	Vratislava
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
manželku	manželka	k1gFnSc4	manželka
korunoval	korunovat	k5eAaBmAgInS	korunovat
králem	král	k1gMnSc7	král
a	a	k8xC	a
královnou	královna	k1gFnSc7	královna
<g/>
,	,	kIx,	,
při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
příležitosti	příležitost	k1gFnSc6	příležitost
je	být	k5eAaImIp3nS	být
uváděn	uvádět	k5eAaImNgMnS	uvádět
u	u	k7c2	u
Vratislava	Vratislav	k1gMnSc2	Vratislav
i	i	k8xC	i
titul	titul	k1gInSc1	titul
krále	král	k1gMnSc2	král
polského	polský	k2eAgMnSc2d1	polský
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
korunovace	korunovace	k1gFnSc2	korunovace
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
Kodex	kodex	k1gInSc1	kodex
vyšehradský	vyšehradský	k2eAgInSc1d1	vyšehradský
<g/>
.	.	kIx.	.
</s>
<s>
Vratislav	Vratislav	k1gMnSc1	Vratislav
si	se	k3xPyFc3	se
užíval	užívat	k5eAaImAgMnS	užívat
titulu	titul	k1gInSc2	titul
krále	král	k1gMnSc2	král
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1092	[number]	k4	1092
<g/>
.	.	kIx.	.
</s>
<s>
Svatava	Svatava	k1gFnSc1	Svatava
tak	tak	k6eAd1	tak
ovdověla	ovdovět	k5eAaPmAgFnS	ovdovět
a	a	k8xC	a
přihlížela	přihlížet	k5eAaImAgFnS	přihlížet
poté	poté	k6eAd1	poté
bojům	boj	k1gInPc3	boj
mezi	mezi	k7c4	mezi
členy	člen	k1gInPc4	člen
přemyslovského	přemyslovský	k2eAgInSc2d1	přemyslovský
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1111	[number]	k4	1111
sehrála	sehrát	k5eAaPmAgFnS	sehrát
roli	role	k1gFnSc4	role
při	při	k7c6	při
jednání	jednání	k1gNnSc6	jednání
Vladislava	Vladislav	k1gMnSc4	Vladislav
I.	I.	kA	I.
s	s	k7c7	s
Boleslavem	Boleslav	k1gMnSc7	Boleslav
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Křivoústým	křivoústý	k2eAgInSc7d1	křivoústý
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
králově	králův	k2eAgFnSc6d1	králova
smrti	smrt	k1gFnSc6	smrt
Svatava	Svatava	k1gFnSc1	Svatava
také	také	k6eAd1	také
usmiřovala	usmiřovat	k5eAaImAgFnS	usmiřovat
své	svůj	k3xOyFgMnPc4	svůj
nejmladší	mladý	k2eAgMnPc4d3	nejmladší
syny	syn	k1gMnPc4	syn
Soběslava	Soběslav	k1gMnSc4	Soběslav
a	a	k8xC	a
Vladislava	Vladislav	k1gMnSc4	Vladislav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1125	[number]	k4	1125
umírající	umírající	k1gMnSc1	umírající
Vladislav	Vladislav	k1gMnSc1	Vladislav
I.	I.	kA	I.
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
jako	jako	k9	jako
svého	svůj	k3xOyFgMnSc4	svůj
nástupce	nástupce	k1gMnSc4	nástupce
Otu	Ota	k1gMnSc4	Ota
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Olomouckého	olomoucký	k2eAgMnSc4d1	olomoucký
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
nástupnictví	nástupnictví	k1gNnSc4	nástupnictví
podporovala	podporovat	k5eAaImAgFnS	podporovat
také	také	k9	také
Vladislavova	Vladislavův	k2eAgFnSc1d1	Vladislavova
manželka	manželka	k1gFnSc1	manželka
Richenza	Richenza	k1gFnSc1	Richenza
z	z	k7c2	z
Bergu	Berg	k1gInSc2	Berg
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
zásah	zásah	k1gInSc1	zásah
královny	královna	k1gFnSc2	královna
matky	matka	k1gFnSc2	matka
jeho	jeho	k3xOp3gNnPc2	jeho
rozhodnutí	rozhodnutí	k1gNnPc2	rozhodnutí
změnil	změnit	k5eAaPmAgInS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
velkém	velký	k2eAgInSc6d1	velký
nátlaku	nátlak	k1gInSc6	nátlak
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgInSc3	jenž
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgInS	připojit
i	i	k9	i
Svatavin	Svatavin	k2eAgMnSc1d1	Svatavin
přítel	přítel	k1gMnSc1	přítel
biskup	biskup	k1gMnSc1	biskup
Ota	Ota	k1gMnSc1	Ota
Bamberský	bamberský	k2eAgMnSc1d1	bamberský
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
umírající	umírající	k2eAgMnSc1d1	umírající
kníže	kníže	k1gMnSc1	kníže
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
smířil	smířit	k5eAaPmAgMnS	smířit
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
velice	velice	k6eAd1	velice
nerad	nerad	k2eAgMnSc1d1	nerad
<g/>
.	.	kIx.	.
</s>
<s>
Svatava	Svatava	k1gFnSc1	Svatava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
30	[number]	k4	30
let	léto	k1gNnPc2	léto
vdovou	vdova	k1gFnSc7	vdova
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
zažila	zažít	k5eAaPmAgFnS	zažít
vítězství	vítězství	k1gNnSc4	vítězství
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
Soběslava	Soběslav	k1gMnSc2	Soběslav
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Chlumce	Chlumec	k1gInSc2	Chlumec
a	a	k8xC	a
zemřela	zemřít	k5eAaPmAgFnS	zemřít
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1126	[number]	k4	1126
<g/>
.	.	kIx.	.
</s>
<s>
Pohřbená	pohřbený	k2eAgFnSc1d1	pohřbená
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
na	na	k7c6	na
Vyšehradě	Vyšehrad	k1gInSc6	Vyšehrad
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc4	Petr
a	a	k8xC	a
Pavla	Pavel	k1gMnSc4	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pěti	pět	k4xCc2	pět
známých	známý	k2eAgFnPc2d1	známá
dětí	dítě	k1gFnPc2	dítě
Vratislava	Vratislav	k1gMnSc2	Vratislav
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
Svatavy	Svatava	k1gFnPc1	Svatava
se	se	k3xPyFc4	se
všechny	všechen	k3xTgFnPc1	všechen
dožily	dožít	k5eAaPmAgFnP	dožít
dospělosti	dospělost	k1gFnPc4	dospělost
<g/>
.	.	kIx.	.
</s>
<s>
Boleslav	Boleslav	k1gMnSc1	Boleslav
(	(	kIx(	(
<g/>
†	†	k?	†
1091	[number]	k4	1091
<g/>
)	)	kIx)	)
,	,	kIx,	,
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
údělník	údělník	k1gMnSc1	údělník
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1064	[number]	k4	1064
<g/>
?	?	kIx.	?
</s>
<s>
-	-	kIx~	-
1124	[number]	k4	1124
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
∞	∞	k?	∞
1100	[number]	k4	1100
Helbirga	Helbirga	k1gFnSc1	Helbirga
Babenberská	babenberský	k2eAgFnSc1d1	Babenberská
Vladislav	Vladislav	k1gMnSc1	Vladislav
I.	I.	kA	I.
(	(	kIx(	(
<g/>
†	†	k?	†
1125	[number]	k4	1125
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
∞	∞	k?	∞
1111	[number]	k4	1111
<g/>
?	?	kIx.	?
</s>
<s>
Richenza	Richenza	k1gFnSc1	Richenza
z	z	k7c2	z
Bergu	Berg	k1gInSc2	Berg
Soběslav	Soběslav	k1gMnSc1	Soběslav
I.	I.	kA	I.
(	(	kIx(	(
<g/>
1075	[number]	k4	1075
<g/>
?	?	kIx.	?
</s>
<s>
-	-	kIx~	-
1140	[number]	k4	1140
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
∞	∞	k?	∞
1123	[number]	k4	1123
Adleyta	Adleyta	k1gFnSc1	Adleyta
Arpádovna	Arpádovna	k1gFnSc1	Arpádovna
Judita	Judita	k1gFnSc1	Judita
Grojčská	Grojčský	k2eAgFnSc1d1	Grojčský
(	(	kIx(	(
<g/>
1066	[number]	k4	1066
<g/>
?	?	kIx.	?
</s>
<s>
-	-	kIx~	-
1108	[number]	k4	1108
<g/>
)	)	kIx)	)
∞	∞	k?	∞
1086	[number]	k4	1086
Wiprecht	Wiprecht	k1gInSc1	Wiprecht
Grojčský	Grojčský	k2eAgInSc1d1	Grojčský
BLÁHOVÁ	Bláhová	k1gFnSc1	Bláhová
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
<g/>
;	;	kIx,	;
FROLÍK	FROLÍK	kA	FROLÍK
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
;	;	kIx,	;
PROFANTOVÁ	PROFANTOVÁ	kA	PROFANTOVÁ
<g/>
,	,	kIx,	,
Naďa	Naďa	k1gFnSc1	Naďa
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
dějiny	dějiny	k1gFnPc1	dějiny
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
I.	I.	kA	I.
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1197	[number]	k4	1197
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
800	[number]	k4	800
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
265	[number]	k4	265
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
NOVOTNÝ	Novotný	k1gMnSc1	Novotný
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
dějiny	dějiny	k1gFnPc1	dějiny
I.	I.	kA	I.
<g/>
/	/	kIx~	/
<g/>
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Břetislava	Břetislav	k1gMnSc2	Břetislav
I.	I.	kA	I.
do	do	k7c2	do
Přemysla	Přemysl	k1gMnSc2	Přemysl
I.	I.	kA	I.
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Laichter	Laichter	k1gMnSc1	Laichter
<g/>
,	,	kIx,	,
1913	[number]	k4	1913
<g/>
.	.	kIx.	.
1214	[number]	k4	1214
s.	s.	k?	s.
VANÍČEK	Vaníček	k1gMnSc1	Vaníček
<g/>
,	,	kIx,	,
Vratislav	Vratislav	k1gMnSc1	Vratislav
<g/>
.	.	kIx.	.
</s>
<s>
Vratislav	Vratislav	k1gMnSc1	Vratislav
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
I.	I.	kA	I.
<g/>
)	)	kIx)	)
První	první	k4xOgMnSc1	první
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
269	[number]	k4	269
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7021	[number]	k4	7021
<g/>
-	-	kIx~	-
<g/>
655	[number]	k4	655
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
KAREŠOVÁ	Karešová	k1gFnSc1	Karešová
<g/>
,	,	kIx,	,
Z.	Z.	kA	Z.
<g/>
;	;	kIx,	;
PRAŽÁK	Pražák	k1gMnSc1	Pražák
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Královny	královna	k1gFnSc2	královna
a	a	k8xC	a
kněžny	kněžna	k1gFnSc2	kněžna
české	český	k2eAgFnSc2d1	Česká
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
X-Egem	X-Eg	k1gInSc7	X-Eg
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Svatava	Svatava	k1gFnSc1	Svatava
Polská	polský	k2eAgFnSc1d1	polská
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Královna	královna	k1gFnSc1	královna
Svatava	Svatava	k1gFnSc1	Svatava
a	a	k8xC	a
ženy	žena	k1gFnPc1	žena
kolem	kolem	k7c2	kolem
ní	on	k3xPp3gFnSc2	on
</s>
