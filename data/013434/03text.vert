<p>
<s>
Podorlický	podorlický	k2eAgInSc1d1	podorlický
skanzen	skanzen	k1gInSc1	skanzen
Krňovice	Krňovice	k1gFnSc2	Krňovice
je	být	k5eAaImIp3nS	být
skanzenem	skanzen	k1gInSc7	skanzen
s	s	k7c7	s
ukázkami	ukázka	k1gFnPc7	ukázka
lidové	lidový	k2eAgFnSc2d1	lidová
architektury	architektura	k1gFnSc2	architektura
Královéhradecka	Královéhradecko	k1gNnSc2	Královéhradecko
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
části	část	k1gFnSc2	část
obce	obec	k1gFnSc2	obec
Krňovice	Krňovice	k1gFnSc2	Krňovice
nedaleko	nedaleko	k7c2	nedaleko
Třebechovic	Třebechovice	k1gFnPc2	Třebechovice
pod	pod	k7c7	pod
Orebem	Oreb	k1gInSc7	Oreb
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
projekt	projekt	k1gInSc4	projekt
neziskové	ziskový	k2eNgFnSc2d1	nezisková
organizace	organizace	k1gFnSc2	organizace
a	a	k8xC	a
soukromé	soukromý	k2eAgNnSc4d1	soukromé
–	–	k?	–
rodinné	rodinný	k2eAgFnSc2d1	rodinná
firmy	firma	k1gFnSc2	firma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
skanzenu	skanzen	k1gInSc2	skanzen
==	==	k?	==
</s>
</p>
<p>
<s>
Podorlický	podorlický	k2eAgInSc1d1	podorlický
skanzen	skanzen	k1gInSc1	skanzen
Krňovice	Krňovice	k1gFnSc2	Krňovice
je	být	k5eAaImIp3nS	být
jediné	jediný	k2eAgNnSc1d1	jediné
muzeum	muzeum	k1gNnSc1	muzeum
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
v	v	k7c6	v
Královéhradeckém	královéhradecký	k2eAgInSc6d1	královéhradecký
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Soustřeďuje	soustřeďovat	k5eAaImIp3nS	soustřeďovat
architektonické	architektonický	k2eAgFnPc4d1	architektonická
a	a	k8xC	a
technické	technický	k2eAgFnPc4d1	technická
památky	památka	k1gFnPc4	památka
regionu	region	k1gInSc2	region
Královéhradecka	Královéhradecko	k1gNnSc2	Královéhradecko
<g/>
,	,	kIx,	,
podhůří	podhůří	k1gNnSc1	podhůří
Orlických	orlický	k2eAgFnPc2d1	Orlická
hor	hora	k1gFnPc2	hora
a	a	k8xC	a
Podkrkonoší	Podkrkonoší	k1gNnSc6	Podkrkonoší
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nepřekrývá	překrývat	k5eNaImIp3nS	překrývat
se	s	k7c7	s
žádným	žádný	k3yNgMnSc7	žádný
z	z	k7c2	z
dnes	dnes	k6eAd1	dnes
existujících	existující	k2eAgInPc2d1	existující
skanzenů	skanzen	k1gInPc2	skanzen
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
naopak	naopak	k6eAd1	naopak
jejich	jejich	k3xOp3gFnSc4	jejich
síť	síť	k1gFnSc4	síť
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Realizace	realizace	k1gFnSc1	realizace
je	být	k5eAaImIp3nS	být
dílem	dílo	k1gNnSc7	dílo
nadšenců	nadšenec	k1gMnPc2	nadšenec
a	a	k8xC	a
fandů	fandů	k?	fandů
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
do	do	k7c2	do
projektu	projekt	k1gInSc2	projekt
investují	investovat	k5eAaBmIp3nP	investovat
svůj	svůj	k3xOyFgInSc4	svůj
volný	volný	k2eAgInSc4d1	volný
čas	čas	k1gInSc4	čas
a	a	k8xC	a
elán	elán	k1gInSc4	elán
<g/>
.	.	kIx.	.
</s>
<s>
Finančně	finančně	k6eAd1	finančně
je	být	k5eAaImIp3nS	být
areál	areál	k1gInSc1	areál
dílem	díl	k1gInSc7	díl
soukromé	soukromý	k2eAgFnSc2d1	soukromá
firmy	firma	k1gFnSc2	firma
Dřevozpracující	dřevozpracující	k2eAgNnSc1d1	dřevozpracující
družstvo	družstvo	k1gNnSc1	družstvo
(	(	kIx(	(
<g/>
dřevostavby	dřevostavba	k1gFnPc1	dřevostavba
a	a	k8xC	a
opravy	oprava	k1gFnPc1	oprava
památek	památka	k1gFnPc2	památka
<g/>
)	)	kIx)	)
a	a	k8xC	a
neziskové	ziskový	k2eNgFnSc2d1	nezisková
organizace	organizace	k1gFnSc2	organizace
Český	český	k2eAgInSc1d1	český
svaz	svaz	k1gInSc1	svaz
ochránců	ochránce	k1gMnPc2	ochránce
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
základní	základní	k2eAgFnSc2d1	základní
organizace	organizace	k1gFnSc2	organizace
Orlice	Orlice	k1gFnSc2	Orlice
<g/>
.	.	kIx.	.
</s>
<s>
Naše	náš	k3xOp1gNnSc4	náš
poděkování	poděkování	k1gNnSc4	poděkování
si	se	k3xPyFc3	se
zaslouží	zasloužit	k5eAaPmIp3nP	zasloužit
i	i	k9	i
další	další	k2eAgMnPc1d1	další
mecenáši	mecenáš	k1gMnPc1	mecenáš
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
finančně	finančně	k6eAd1	finančně
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
s	s	k7c7	s
realizací	realizace	k1gFnSc7	realizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výstavbu	výstavba	k1gFnSc4	výstavba
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Dřevozpracující	dřevozpracující	k2eAgNnSc1d1	dřevozpracující
družstvo	družstvo	k1gNnSc1	družstvo
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
svaz	svaz	k1gInSc1	svaz
ochránců	ochránce	k1gMnPc2	ochránce
přírody	příroda	k1gFnSc2	příroda
–	–	k?	–
ZO	ZO	kA	ZO
Orlice	Orlice	k1gNnSc1	Orlice
</s>
<s>
První	První	k4xOgInPc1	První
záměry	záměr	k1gInPc1	záměr
vzniku	vznik	k1gInSc2	vznik
skanzenu	skanzen	k1gInSc2	skanzen
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
mapoval	mapovat	k5eAaImAgInS	mapovat
architekturu	architektura	k1gFnSc4	architektura
regionu	region	k1gInSc2	region
Královéhradecka	Královéhradecko	k1gNnSc2	Královéhradecko
a	a	k8xC	a
podhůří	podhůří	k1gNnSc2	podhůří
Orlických	orlický	k2eAgFnPc2d1	Orlická
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Areál	areál	k1gInSc1	areál
měl	mít	k5eAaImAgInS	mít
původně	původně	k6eAd1	původně
vzniknout	vzniknout	k5eAaPmF	vzniknout
v	v	k7c6	v
nedaleké	daleký	k2eNgFnSc6d1	nedaleká
obci	obec	k1gFnSc6	obec
Bělečko	Bělečko	k1gNnSc1	Bělečko
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
negativnímu	negativní	k2eAgInSc3d1	negativní
postoji	postoj	k1gInSc3	postoj
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
obecního	obecní	k2eAgInSc2d1	obecní
úřadu	úřad	k1gInSc2	úřad
(	(	kIx(	(
<g/>
zejména	zejména	k6eAd1	zejména
starosty	starosta	k1gMnSc2	starosta
<g/>
)	)	kIx)	)
v	v	k7c6	v
Býšti	Býšť	k1gFnSc6	Býšť
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
i	i	k9	i
Krajského	krajský	k2eAgInSc2d1	krajský
úřadu	úřad	k1gInSc2	úřad
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgFnPc4d1	nutná
záměry	záměra	k1gFnPc4	záměra
přehodnotit	přehodnotit	k5eAaPmF	přehodnotit
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
již	již	k6eAd1	již
byly	být	k5eAaImAgInP	být
vykoupeny	vykoupen	k2eAgInPc1d1	vykoupen
pozemky	pozemek	k1gInPc1	pozemek
pro	pro	k7c4	pro
výstavbu	výstavba	k1gFnSc4	výstavba
a	a	k8xC	a
hotová	hotový	k2eAgFnSc1d1	hotová
architektonická	architektonický	k2eAgFnSc1d1	architektonická
studie	studie	k1gFnSc1	studie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
začalo	začít	k5eAaPmAgNnS	začít
znovu	znovu	k6eAd1	znovu
hledáním	hledání	k1gNnSc7	hledání
vhodného	vhodný	k2eAgNnSc2d1	vhodné
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
nalezeno	naleznout	k5eAaPmNgNnS	naleznout
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Krňovice	Krňovice	k1gFnSc2	Krňovice
<g/>
.	.	kIx.	.
</s>
<s>
Záměr	záměr	k1gInSc1	záměr
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
jak	jak	k8xS	jak
původními	původní	k2eAgMnPc7d1	původní
majiteli	majitel	k1gMnPc7	majitel
pozemků	pozemek	k1gInPc2	pozemek
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
Městským	městský	k2eAgInSc7d1	městský
úřadem	úřad	k1gInSc7	úřad
v	v	k7c6	v
Třebechovicích	Třebechovice	k1gFnPc6	Třebechovice
pod	pod	k7c7	pod
Orebem	Oreb	k1gInSc7	Oreb
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
rychlá	rychlý	k2eAgFnSc1d1	rychlá
dílčí	dílčí	k2eAgFnSc1d1	dílčí
změna	změna	k1gFnSc1	změna
územního	územní	k2eAgInSc2d1	územní
plánu	plán	k1gInSc2	plán
a	a	k8xC	a
další	další	k2eAgInSc4d1	další
kolotoč	kolotoč	k1gInSc4	kolotoč
nekonečných	konečný	k2eNgNnPc2d1	nekonečné
úředních	úřední	k2eAgNnPc2d1	úřední
jednání	jednání	k1gNnPc2	jednání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
bylo	být	k5eAaImAgNnS	být
vydáno	vydán	k2eAgNnSc4d1	vydáno
stavební	stavební	k2eAgNnSc4d1	stavební
povolení	povolení	k1gNnSc4	povolení
pro	pro	k7c4	pro
první	první	k4xOgFnSc4	první
etapu	etapa	k1gFnSc4	etapa
(	(	kIx(	(
<g/>
vstupní	vstupní	k2eAgFnSc1d1	vstupní
část	část	k1gFnSc1	část
skanzenu	skanzen	k1gInSc2	skanzen
<g/>
)	)	kIx)	)
a	a	k8xC	a
započaly	započnout	k5eAaPmAgFnP	započnout
práce	práce	k1gFnPc1	práce
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
realizaci	realizace	k1gFnSc4	realizace
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostní	slavnostní	k2eAgNnSc1d1	slavnostní
veřejné	veřejný	k2eAgNnSc1d1	veřejné
zahájení	zahájení	k1gNnSc1	zahájení
prací	práce	k1gFnPc2	práce
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
4	[number]	k4	4
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
2002	[number]	k4	2002
při	při	k7c6	při
postavením	postavení	k1gNnSc7	postavení
dřevěné	dřevěný	k2eAgFnSc2d1	dřevěná
zvoničky	zvonička	k1gFnSc2	zvonička
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
skanzenu	skanzen	k1gInSc2	skanzen
neprobíhala	probíhat	k5eNaImAgFnS	probíhat
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
za	za	k7c7	za
zavřenými	zavřený	k2eAgFnPc7d1	zavřená
dveřmi	dveře	k1gFnPc7	dveře
a	a	k8xC	a
zájemci	zájemce	k1gMnPc1	zájemce
si	se	k3xPyFc3	se
mohli	moct	k5eAaImAgMnP	moct
prohlédnout	prohlédnout	k5eAaPmF	prohlédnout
již	již	k6eAd1	již
rozestavěné	rozestavěný	k2eAgFnPc4d1	rozestavěná
stavby	stavba	k1gFnPc4	stavba
i	i	k8xC	i
vznikající	vznikající	k2eAgFnPc4d1	vznikající
expozice	expozice	k1gFnPc4	expozice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
pak	pak	k6eAd1	pak
postupně	postupně	k6eAd1	postupně
vznikaly	vznikat	k5eAaImAgFnP	vznikat
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
stavby	stavba	k1gFnPc1	stavba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
lze	lze	k6eAd1	lze
dnes	dnes	k6eAd1	dnes
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
cesty	cesta	k1gFnPc1	cesta
<g/>
,	,	kIx,	,
oplocení	oplocení	k1gNnPc1	oplocení
a	a	k8xC	a
další	další	k2eAgFnSc1d1	další
doprovodná	doprovodný	k2eAgFnSc1d1	doprovodná
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stavby	stavba	k1gFnSc2	stavba
ve	v	k7c6	v
skanzenu	skanzen	k1gInSc6	skanzen
==	==	k?	==
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
–	–	k?	–
zvonička	zvonička	k1gFnSc1	zvonička
z	z	k7c2	z
Orlických	orlický	k2eAgFnPc2d1	Orlická
hor	hora	k1gFnPc2	hora
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
–	–	k?	–
roubená	roubený	k2eAgFnSc1d1	roubená
stodola	stodola	k1gFnSc1	stodola
z	z	k7c2	z
Ledců	Ledce	k1gMnPc2	Ledce
(	(	kIx(	(
<g/>
depozitář	depozitář	k1gMnSc1	depozitář
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
–	–	k?	–
demontáž	demontáž	k1gFnSc1	demontáž
mlýna	mlýn	k1gInSc2	mlýn
z	z	k7c2	z
Bělče	Bělč	k1gFnSc2	Bělč
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
č.	č.	k?	č.
<g/>
p.	p.	k?	p.
30	[number]	k4	30
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
–	–	k?	–
polabský	polabský	k2eAgInSc1d1	polabský
statek	statek	k1gInSc1	statek
(	(	kIx(	(
<g/>
správní	správní	k2eAgInSc1d1	správní
objekt	objekt	k1gInSc1	objekt
skanzenu	skanzen	k1gInSc2	skanzen
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
–	–	k?	–
2004	[number]	k4	2004
–	–	k?	–
transfer	transfer	k1gInSc1	transfer
špýcharu	špýchar	k1gInSc2	špýchar
ze	z	k7c2	z
Semechnic	Semechnice	k1gFnPc2	Semechnice
(	(	kIx(	(
<g/>
expozice	expozice	k1gFnSc1	expozice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
–	–	k?	–
stodola	stodola	k1gFnSc1	stodola
z	z	k7c2	z
Klášterce	Klášterec	k1gInSc2	Klášterec
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
(	(	kIx(	(
<g/>
restaurátorská	restaurátorský	k2eAgFnSc1d1	restaurátorská
dílna	dílna	k1gFnSc1	dílna
a	a	k8xC	a
zázemí	zázemí	k1gNnSc1	zázemí
skanzenu	skanzen	k1gInSc2	skanzen
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
–	–	k?	–
2006	[number]	k4	2006
–	–	k?	–
kočárovna	kočárovna	k1gFnSc1	kočárovna
(	(	kIx(	(
<g/>
expozice	expozice	k1gFnSc1	expozice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
–	–	k?	–
2008	[number]	k4	2008
–	–	k?	–
hospoda	hospoda	k?	hospoda
"	"	kIx"	"
<g/>
Na	na	k7c6	na
špici	špice	k1gFnSc6	špice
<g/>
"	"	kIx"	"
–	–	k?	–
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
stavby	stavba	k1gFnSc2	stavba
(	(	kIx(	(
<g/>
ekocentrum	ekocentrum	k1gNnSc1	ekocentrum
ČSOP	ČSOP	kA	ČSOP
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
–	–	k?	–
2010	[number]	k4	2010
–	–	k?	–
transfer	transfer	k1gInSc1	transfer
školy	škola	k1gFnSc2	škola
ze	z	k7c2	z
Všestar	Všestara	k1gFnPc2	Všestara
(	(	kIx(	(
<g/>
expozice	expozice	k1gFnSc1	expozice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
–	–	k?	–
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
sušárny	sušárna	k1gFnSc2	sušárna
ze	z	k7c2	z
Semechnic	Semechnice	k1gFnPc2	Semechnice
(	(	kIx(	(
<g/>
expozice	expozice	k1gFnSc1	expozice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
–	–	k?	–
transfer	transfer	k1gInSc1	transfer
špýcharu	špýchar	k1gInSc2	špýchar
z	z	k7c2	z
Prasku	prask	k1gInSc2	prask
(	(	kIx(	(
<g/>
expozice	expozice	k1gFnSc1	expozice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
–	–	k?	–
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
kapličky	kaplička	k1gFnSc2	kaplička
z	z	k7c2	z
Humburk	Humburk	k1gInSc4	Humburk
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
–	–	k?	–
2010	[number]	k4	2010
–	–	k?	–
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
kovárny	kovárna	k1gFnSc2	kovárna
(	(	kIx(	(
<g/>
expozice	expozice	k1gFnSc1	expozice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
–	–	k?	–
2011	[number]	k4	2011
–	–	k?	–
obnova	obnova	k1gFnSc1	obnova
roubeného	roubený	k2eAgInSc2d1	roubený
vodního	vodní	k2eAgInSc2d1	vodní
mlýna	mlýn	k1gInSc2	mlýn
Běleč	Běleč	k1gMnSc1	Běleč
n.	n.	k?	n.
Orlicí	Orlice	k1gFnSc7	Orlice
č.	č.	k?	č.
<g/>
p.	p.	k?	p.
30	[number]	k4	30
–	–	k?	–
budova	budova	k1gFnSc1	budova
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
–	–	k?	–
roubená	roubený	k2eAgFnSc1d1	roubená
stodola	stodola	k1gFnSc1	stodola
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
–	–	k?	–
dosud	dosud	k6eAd1	dosud
–	–	k?	–
obnova	obnova	k1gFnSc1	obnova
interiéru	interiér	k1gInSc2	interiér
a	a	k8xC	a
mlecí	mlecí	k2eAgFnSc2d1	mlecí
technologie	technologie	k1gFnSc2	technologie
–	–	k?	–
mlýn	mlýn	k1gInSc1	mlýn
z	z	k7c2	z
Bělče	Bělč	k1gFnSc2	Bělč
n.	n.	k?	n.
O.	O.	kA	O.
č.	č.	k?	č.
<g/>
p.	p.	k?	p.
30	[number]	k4	30
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
–	–	k?	–
zahájení	zahájení	k1gNnSc2	zahájení
prací	práce	k1gFnPc2	práce
na	na	k7c6	na
dvorcovém	dvorcový	k2eAgInSc6d1	dvorcový
statku	statek	k1gInSc6	statek
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
skanzenu	skanzen	k1gInSc2	skanzen
</s>
</p>
<p>
<s>
2016	[number]	k4	2016
–	–	k?	–
mlýn	mlýn	k1gInSc1	mlýn
z	z	k7c2	z
Bělče	Bělč	k1gFnSc2	Bělč
n.	n.	k?	n.
O.	O.	kA	O.
č.	č.	k?	č.
<g/>
p.	p.	k?	p.
30	[number]	k4	30
uveden	uvést	k5eAaPmNgInS	uvést
do	do	k7c2	do
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
ukončení	ukončení	k1gNnSc1	ukončení
výstavby	výstavba	k1gFnSc2	výstavba
dvorcového	dvorcový	k2eAgInSc2d1	dvorcový
statku	statek	k1gInSc2	statek
</s>
</p>
<p>
<s>
2017	[number]	k4	2017
–	–	k?	–
zahájení	zahájení	k1gNnSc4	zahájení
výstavby	výstavba	k1gFnSc2	výstavba
katru	katr	k1gInSc2	katr
–	–	k?	–
součást	součást	k1gFnSc1	součást
truhlářské	truhlářský	k2eAgFnSc6d1	truhlářská
expoziceV	expoziceV	k?	expoziceV
muzeu	muzeum	k1gNnSc6	muzeum
lidových	lidový	k2eAgFnPc2d1	lidová
staveb	stavba	k1gFnPc2	stavba
jsou	být	k5eAaImIp3nP	být
pravidelně	pravidelně	k6eAd1	pravidelně
pořádány	pořádán	k2eAgInPc1d1	pořádán
programy	program	k1gInPc1	program
zaměřené	zaměřený	k2eAgInPc1d1	zaměřený
na	na	k7c4	na
historickou	historický	k2eAgFnSc4d1	historická
techniku	technika	k1gFnSc4	technika
a	a	k8xC	a
řemesla	řemeslo	k1gNnPc4	řemeslo
–	–	k?	–
tematické	tematický	k2eAgInPc4d1	tematický
dny	den	k1gInPc4	den
a	a	k8xC	a
víkendy	víkend	k1gInPc4	víkend
<g/>
.	.	kIx.	.
</s>
<s>
Aktuální	aktuální	k2eAgInSc1d1	aktuální
kalendář	kalendář	k1gInSc1	kalendář
akcí	akce	k1gFnPc2	akce
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
na	na	k7c6	na
oficiálních	oficiální	k2eAgFnPc6d1	oficiální
stránkách	stránka	k1gFnPc6	stránka
skanzenu	skanzen	k1gInSc2	skanzen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
SKANZEN	skanzen	k1gInSc4	skanzen
KRŇOVICE	KRŇOVICE	kA	KRŇOVICE
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
www	www	k?	www
stránky	stránka	k1gFnPc4	stránka
skanzenu	skanzen	k1gInSc2	skanzen
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
:	:	kIx,	:
Podorlický	podorlický	k2eAgInSc1d1	podorlický
skanzen	skanzen	k1gInSc1	skanzen
Krňovice	Krňovice	k1gFnSc1	Krňovice
-	-	kIx~	-
Dřevozpracující	dřevozpracující	k2eAgNnSc1d1	dřevozpracující
družstvo	družstvo	k1gNnSc1	družstvo
<g/>
,	,	kIx,	,
1.1	[number]	k4	1.1
<g/>
.2012	.2012	k4	.2012
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2014	[number]	k4	2014
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
25	[number]	k4	25
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
Podorlického	podorlický	k2eAgInSc2d1	podorlický
skanzenu	skanzen	k1gInSc2	skanzen
</s>
</p>
<p>
<s>
Kalendář	kalendář	k1gInSc1	kalendář
akcí	akce	k1gFnPc2	akce
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Podorlický	podorlický	k2eAgInSc4d1	podorlický
skanzen	skanzen	k1gInSc4	skanzen
Krňovice	Krňovice	k1gFnSc2	Krňovice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
