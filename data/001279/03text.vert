<s>
Chlor	chlor	k1gInSc1	chlor
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Cl	Cl	k1gFnSc2	Cl
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
chlorum	chlorum	k1gNnSc1	chlorum
(	(	kIx(	(
<g/>
starořecky	starořecky	k6eAd1	starořecky
χ	χ	k?	χ
<g/>
,	,	kIx,	,
chlóros	chlórosa	k1gFnPc2	chlórosa
-	-	kIx~	-
"	"	kIx"	"
<g/>
zelený	zelený	k2eAgInSc1d1	zelený
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
toxický	toxický	k2eAgInSc1d1	toxický
<g/>
,	,	kIx,	,
světle	světle	k6eAd1	světle
zelený	zelený	k2eAgInSc1d1	zelený
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc1	druhý
člen	člen	k1gInSc1	člen
řady	řada	k1gFnSc2	řada
halogenů	halogen	k1gInPc2	halogen
<g/>
.	.	kIx.	.
</s>
<s>
Chlor	chlor	k1gInSc1	chlor
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
reaktivní	reaktivní	k2eAgInSc1d1	reaktivní
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
ochotně	ochotně	k6eAd1	ochotně
slučuje	slučovat	k5eAaImIp3nS	slučovat
s	s	k7c7	s
většinou	většina	k1gFnSc7	většina
prvků	prvek	k1gInPc2	prvek
periodické	periodický	k2eAgFnSc2d1	periodická
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
objeven	objeven	k2eAgInSc1d1	objeven
roku	rok	k1gInSc2	rok
1774	[number]	k4	1774
Carlem	Carl	k1gMnSc7	Carl
Wilhelmem	Wilhelm	k1gMnSc7	Wilhelm
Scheelem	Scheel	k1gMnSc7	Scheel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dnešní	dnešní	k2eAgNnSc1d1	dnešní
pojmenování	pojmenování	k1gNnSc1	pojmenování
mu	on	k3xPp3gMnSc3	on
dal	dát	k5eAaPmAgMnS	dát
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1810	[number]	k4	1810
anglický	anglický	k2eAgMnSc1d1	anglický
chemik	chemik	k1gMnSc1	chemik
sir	sir	k1gMnSc1	sir
Humphry	Humphra	k1gFnSc2	Humphra
Davy	Dav	k1gInPc4	Dav
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
chlor	chlor	k1gInSc1	chlor
přítomen	přítomen	k2eAgInSc1d1	přítomen
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
rozpuštěna	rozpuštěn	k2eAgFnSc1d1	rozpuštěna
v	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
některých	některý	k3yIgNnPc2	některý
vnitrozemských	vnitrozemský	k2eAgNnPc2d1	vnitrozemské
jezer	jezero	k1gNnPc2	jezero
(	(	kIx(	(
<g/>
Mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
moře	moře	k1gNnSc1	moře
<g/>
,	,	kIx,	,
Velké	velký	k2eAgNnSc1d1	velké
solné	solný	k2eAgNnSc1d1	solné
jezero	jezero	k1gNnSc1	jezero
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
minerálů	minerál	k1gInPc2	minerál
je	být	k5eAaImIp3nS	být
nejznámější	známý	k2eAgInSc1d3	nejznámější
chlorid	chlorid	k1gInSc1	chlorid
sodný	sodný	k2eAgInSc1d1	sodný
(	(	kIx(	(
<g/>
NaCl	NaCl	k1gInSc1	NaCl
<g/>
)	)	kIx)	)
neboli	neboli	k8xC	neboli
kuchyňská	kuchyňský	k2eAgFnSc1d1	kuchyňská
sůl	sůl	k1gFnSc1	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgNnPc1d1	velké
ložiska	ložisko	k1gNnPc1	ložisko
chloridu	chlorid	k1gInSc2	chlorid
sodného	sodný	k2eAgInSc2d1	sodný
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
např.	např.	kA	např.
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
a	a	k8xC	a
USA	USA	kA	USA
a	a	k8xC	a
geologicky	geologicky	k6eAd1	geologicky
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jako	jako	k9	jako
pozůstatek	pozůstatek	k1gInSc4	pozůstatek
po	po	k7c4	po
odpaření	odpaření	k1gNnPc4	odpaření
slaných	slaný	k2eAgNnPc2d1	slané
vnitrozemských	vnitrozemský	k2eAgNnPc2d1	vnitrozemské
jezer	jezero	k1gNnPc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Viz	vidět	k5eAaImRp2nS	vidět
také	také	k9	také
minerál	minerál	k1gInSc4	minerál
halit	halit	k5eAaImF	halit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
je	být	k5eAaImIp3nS	být
chlor	chlor	k1gInSc1	chlor
20	[number]	k4	20
<g/>
.	.	kIx.	.
nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
prvkem	prvek	k1gInSc7	prvek
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
přítomen	přítomen	k2eAgMnSc1d1	přítomen
v	v	k7c6	v
koncentraci	koncentrace	k1gFnSc6	koncentrace
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
1900	[number]	k4	1900
ppm	ppm	k?	ppm
(	(	kIx(	(
<g/>
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
tvoří	tvořit	k5eAaImIp3nP	tvořit
chloridové	chloridový	k2eAgInPc4d1	chloridový
ionty	ion	k1gInPc4	ion
nejvíce	nejvíce	k6eAd1	nejvíce
zastoupený	zastoupený	k2eAgInSc1d1	zastoupený
anion	anion	k1gInSc1	anion
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
koncentrace	koncentrace	k1gFnSc1	koncentrace
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
19	[number]	k4	19
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
l.	l.	k?	l.
Přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
zhruba	zhruba	k6eAd1	zhruba
10	[number]	k4	10
<g/>
krát	krát	k6eAd1	krát
méně	málo	k6eAd2	málo
chloru	chlor	k1gInSc2	chlor
<g/>
,	,	kIx,	,
než	než	k8xS	než
by	by	kYmCp3nS	by
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
jiný	jiný	k2eAgMnSc1d1	jiný
planetám	planeta	k1gFnPc3	planeta
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mohlo	moct	k5eAaImAgNnS	moct
napomoci	napomoct	k5eAaPmF	napomoct
rozšíření	rozšíření	k1gNnSc1	rozšíření
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Zastoupení	zastoupení	k1gNnSc1	zastoupení
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
vyššímu	vysoký	k2eAgNnSc3d2	vyšší
atomovému	atomový	k2eAgNnSc3d1	atomové
číslu	číslo	k1gNnSc3	číslo
chloru	chlor	k1gInSc2	chlor
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
1	[number]	k4	1
atom	atom	k1gInSc4	atom
chloru	chlor	k1gInSc2	chlor
připadá	připadat	k5eAaImIp3nS	připadat
přes	přes	k7c4	přes
17	[number]	k4	17
milionů	milion	k4xCgInPc2	milion
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Chlor	chlor	k1gInSc1	chlor
je	být	k5eAaImIp3nS	být
mikrobiogenním	mikrobiogenní	k2eAgInSc7d1	mikrobiogenní
prvkem	prvek	k1gInSc7	prvek
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Přijímají	přijímat	k5eAaImIp3nP	přijímat
ho	on	k3xPp3gNnSc4	on
z	z	k7c2	z
půdy	půda	k1gFnSc2	půda
z	z	k7c2	z
vodného	vodné	k1gNnSc2	vodné
roztoku	roztok	k1gInSc2	roztok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rostlině	rostlina	k1gFnSc6	rostlina
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
pohyblivý	pohyblivý	k2eAgMnSc1d1	pohyblivý
<g/>
.	.	kIx.	.
</s>
<s>
Pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
kompenzovat	kompenzovat	k5eAaBmF	kompenzovat
kladný	kladný	k2eAgInSc4d1	kladný
náboj	náboj	k1gInSc4	náboj
draslíku	draslík	k1gInSc2	draslík
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vápníkem	vápník	k1gInSc7	vápník
stabilizuje	stabilizovat	k5eAaBmIp3nS	stabilizovat
komplex	komplex	k1gInSc1	komplex
rozkládající	rozkládající	k2eAgInSc1d1	rozkládající
vodu	voda	k1gFnSc4	voda
v	v	k7c6	v
primární	primární	k2eAgFnSc6d1	primární
fázi	fáze	k1gFnSc6	fáze
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
draslíkem	draslík	k1gInSc7	draslík
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
otvírání	otvírání	k1gNnSc6	otvírání
a	a	k8xC	a
zavírání	zavírání	k1gNnSc6	zavírání
průduchů	průduch	k1gInPc2	průduch
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
se	se	k3xPyFc4	se
chlor	chlor	k1gInSc1	chlor
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
mocenství	mocenství	k1gNnSc6	mocenství
Cl	Cl	k1gFnSc2	Cl
<g/>
-	-	kIx~	-
<g/>
I	i	k9	i
<g/>
,	,	kIx,	,
ClI	clít	k5eAaImRp2nS	clít
<g/>
,	,	kIx,	,
ClIII	ClIII	k1gMnSc4	ClIII
<g/>
,	,	kIx,	,
ClIV	ClIV	k1gMnSc4	ClIV
<g/>
,	,	kIx,	,
ClV	ClV	k1gMnSc4	ClV
a	a	k8xC	a
ClVII	ClVII	k1gMnSc4	ClVII
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
mocenství	mocenství	k1gNnSc6	mocenství
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
chlor	chlor	k1gInSc1	chlor
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
<g/>
,	,	kIx,	,
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
i	i	k9	i
příslušnou	příslušný	k2eAgFnSc4d1	příslušná
kyselinu	kyselina	k1gFnSc4	kyselina
<g/>
.	.	kIx.	.
jedinou	jediný	k2eAgFnSc7d1	jediná
bezkyslíkatou	bezkyslíkatý	k2eAgFnSc7d1	bezkyslíkatá
kyselinou	kyselina	k1gFnSc7	kyselina
je	být	k5eAaImIp3nS	být
kyselina	kyselina	k1gFnSc1	kyselina
chlorovodíková	chlorovodíkový	k2eAgFnSc1d1	chlorovodíková
(	(	kIx(	(
<g/>
HCl	HCl	k1gFnSc1	HCl
<g/>
)	)	kIx)	)
s	s	k7c7	s
chlorem	chlor	k1gInSc7	chlor
záporně	záporně	k6eAd1	záporně
jednomocným	jednomocný	k2eAgMnSc7d1	jednomocný
Cl	Cl	k1gMnSc7	Cl
<g/>
-	-	kIx~	-
<g/>
I	i	k9	i
kyselina	kyselina	k1gFnSc1	kyselina
chlorná	chlorný	k2eAgFnSc1d1	chlorná
HClO	HClO	k1gFnSc1	HClO
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
valenci	valence	k1gFnSc4	valence
ClI	clít	k5eAaImRp2nS	clít
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
velmi	velmi	k6eAd1	velmi
slabé	slabý	k2eAgFnPc4d1	slabá
kyseliny	kyselina	k1gFnPc4	kyselina
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
mocenství	mocenství	k1gNnSc6	mocenství
ClIII	ClIII	k1gFnSc2	ClIII
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
kyselina	kyselina	k1gFnSc1	kyselina
chloritá	chloritat	k5eAaPmIp3nS	chloritat
HClO	HClO	k1gFnSc4	HClO
<g/>
2	[number]	k4	2
v	v	k7c6	v
mocenství	mocenství	k1gNnSc6	mocenství
ClIV	ClIV	k1gFnSc2	ClIV
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
oxid	oxid	k1gInSc1	oxid
chloričitý	chloričitý	k2eAgInSc1d1	chloričitý
ClO	clo	k1gNnSc4	clo
<g/>
2	[number]	k4	2
používaný	používaný	k2eAgInSc1d1	používaný
k	k	k7c3	k
průmyslovému	průmyslový	k2eAgNnSc3d1	průmyslové
čištění	čištění	k1gNnSc3	čištění
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
také	také	k9	také
ve	v	k7c6	v
zdraví	zdraví	k1gNnSc6	zdraví
škodlivém	škodlivý	k2eAgNnSc6d1	škodlivé
přípravku	přípravek	k1gInSc2	přípravek
MMS	MMS	kA	MMS
(	(	kIx(	(
<g/>
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
označován	označovat	k5eAaImNgInS	označovat
i	i	k8xC	i
jako	jako	k8xC	jako
CDS	CDS	kA	CDS
<g/>
)	)	kIx)	)
kyselina	kyselina	k1gFnSc1	kyselina
chlorečná	chlorečný	k2eAgFnSc1d1	chlorečný
HClO	HClO	k1gFnSc1	HClO
<g/>
3	[number]	k4	3
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
valenci	valence	k1gFnSc4	valence
ClV	ClV	k1gMnSc2	ClV
velmi	velmi	k6eAd1	velmi
silná	silný	k2eAgFnSc1d1	silná
kyselina	kyselina	k1gFnSc1	kyselina
chloristá	chloristý	k2eAgFnSc1d1	chloristá
HClO	HClO	k1gFnSc1	HClO
<g/>
4	[number]	k4	4
přísluší	příslušet	k5eAaImIp3nS	příslušet
sedmimocnému	sedmimocný	k2eAgInSc3d1	sedmimocný
chloru	chlor	k1gInSc3	chlor
ClVII	ClVII	k1gFnSc2	ClVII
Všechny	všechen	k3xTgFnPc1	všechen
uvedené	uvedený	k2eAgFnPc1d1	uvedená
kyseliny	kyselina	k1gFnPc1	kyselina
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
soli	sůl	k1gFnPc4	sůl
s	s	k7c7	s
elektropozitivními	elektropozitivní	k2eAgInPc7d1	elektropozitivní
prvky	prvek	k1gInPc7	prvek
a	a	k8xC	a
především	především	k6eAd1	především
v	v	k7c6	v
případě	případ	k1gInSc6	případ
kyseliny	kyselina	k1gFnSc2	kyselina
chlorné	chlorný	k2eAgFnPc1d1	chlorná
<g/>
,	,	kIx,	,
chlorité	chloritý	k2eAgFnPc1d1	chloritý
a	a	k8xC	a
chlorečné	chlorečný	k2eAgFnPc1d1	chlorečný
mají	mít	k5eAaImIp3nP	mít
tyto	tento	k3xDgFnPc1	tento
soli	sůl	k1gFnPc1	sůl
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgInSc4d2	veliký
praktický	praktický	k2eAgInSc4d1	praktický
význam	význam	k1gInSc4	význam
než	než	k8xS	než
příslušné	příslušný	k2eAgFnSc2d1	příslušná
kyseliny	kyselina	k1gFnSc2	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Organické	organický	k2eAgFnPc1d1	organická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
chloru	chlor	k1gInSc2	chlor
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
výjimečné	výjimečný	k2eAgNnSc1d1	výjimečné
a	a	k8xC	a
naprostá	naprostý	k2eAgFnSc1d1	naprostá
většina	většina	k1gFnSc1	většina
organických	organický	k2eAgFnPc2d1	organická
molekul	molekula	k1gFnPc2	molekula
obsahujících	obsahující	k2eAgFnPc2d1	obsahující
atom	atom	k1gInSc4	atom
chloru	chlor	k1gInSc2	chlor
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgMnPc7	jenž
se	se	k3xPyFc4	se
kolem	kolem	k6eAd1	kolem
sebe	sebe	k3xPyFc4	sebe
setkáváme	setkávat	k5eAaImIp1nP	setkávat
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
chemickou	chemický	k2eAgFnSc7d1	chemická
syntézou	syntéza	k1gFnSc7	syntéza
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nejpoužívanějších	používaný	k2eAgMnPc2d3	nejpoužívanější
můžeme	moct	k5eAaImIp1nP	moct
jmenovat	jmenovat	k5eAaBmF	jmenovat
chloroform	chloroform	k1gInSc4	chloroform
<g/>
,	,	kIx,	,
tetrachlormethan	tetrachlormethan	k1gInSc4	tetrachlormethan
<g/>
,	,	kIx,	,
PVC	PVC	kA	PVC
<g/>
,	,	kIx,	,
DDT	DDT	kA	DDT
<g/>
,	,	kIx,	,
freony	freon	k1gInPc7	freon
atd.	atd.	kA	atd.
Související	související	k2eAgFnSc1d1	související
informace	informace	k1gFnSc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Hydroxid	hydroxid	k1gInSc1	hydroxid
sodný	sodný	k2eAgInSc1d1	sodný
<g/>
#	#	kIx~	#
<g/>
Průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
výroba	výroba	k1gFnSc1	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Průmyslově	průmyslově	k6eAd1	průmyslově
vyráběný	vyráběný	k2eAgInSc1d1	vyráběný
plynný	plynný	k2eAgInSc1d1	plynný
chlor	chlor	k1gInSc1	chlor
vzniká	vznikat	k5eAaImIp3nS	vznikat
jako	jako	k9	jako
vedlejší	vedlejší	k2eAgInSc1d1	vedlejší
produkt	produkt	k1gInSc1	produkt
výroby	výroba	k1gFnSc2	výroba
hydroxidu	hydroxid	k1gInSc2	hydroxid
sodného	sodný	k2eAgNnSc2d1	sodné
nebo	nebo	k8xC	nebo
draselného	draselný	k2eAgNnSc2d1	draselné
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
vodného	vodné	k1gNnSc2	vodné
roztoku	roztok	k1gInSc2	roztok
chloridu	chlorid	k1gInSc2	chlorid
sodného	sodný	k2eAgInSc2d1	sodný
nebo	nebo	k8xC	nebo
draselného	draselný	k2eAgInSc2d1	draselný
-	-	kIx~	-
solanky	solanka	k1gFnPc4	solanka
(	(	kIx(	(
<g/>
dalším	další	k2eAgInSc7d1	další
vedlejším	vedlejší	k2eAgInSc7d1	vedlejší
produktem	produkt	k1gInSc7	produkt
je	být	k5eAaImIp3nS	být
vodík	vodík	k1gInSc1	vodík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgInSc1d2	starší
způsob	způsob	k1gInSc1	způsob
výroby	výroba	k1gFnSc2	výroba
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
katodu	katoda	k1gFnSc4	katoda
kovovou	kovový	k2eAgFnSc4d1	kovová
kapalnou	kapalný	k2eAgFnSc4d1	kapalná
rtuť	rtuť	k1gFnSc4	rtuť
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
ke	k	k7c3	k
kontaminaci	kontaminace	k1gFnSc3	kontaminace
výrobních	výrobní	k2eAgInPc2d1	výrobní
závodů	závod	k1gInPc2	závod
i	i	k8xC	i
okolního	okolní	k2eAgNnSc2d1	okolní
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Spolana	Spolana	k1gFnSc1	Spolana
Neratovice	Neratovice	k1gFnSc1	Neratovice
<g/>
,	,	kIx,	,
Spolchemie	Spolchemie	k1gFnSc1	Spolchemie
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Modernějšími	moderní	k2eAgInPc7d2	modernější
způsoby	způsob	k1gInPc7	způsob
přípravy	příprava	k1gFnSc2	příprava
chloru	chlor	k1gInSc2	chlor
jsou	být	k5eAaImIp3nP	být
membránová	membránový	k2eAgFnSc1d1	membránová
nebo	nebo	k8xC	nebo
diafragmová	diafragmový	k2eAgFnSc1d1	diafragmový
elektrolýza	elektrolýza	k1gFnSc1	elektrolýza
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
diafragmové	diafragmový	k2eAgFnSc6d1	diafragmový
elektrolýze	elektrolýza	k1gFnSc6	elektrolýza
je	být	k5eAaImIp3nS	být
katodový	katodový	k2eAgInSc1d1	katodový
a	a	k8xC	a
anodový	anodový	k2eAgInSc1d1	anodový
prostor	prostor	k1gInSc1	prostor
oddělen	oddělit	k5eAaPmNgInS	oddělit
polopropustnou	polopropustný	k2eAgFnSc7d1	polopropustná
membránou	membrána	k1gFnSc7	membrána
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
zabránit	zabránit	k5eAaPmF	zabránit
vedlejším	vedlejší	k2eAgFnPc3d1	vedlejší
reakcím	reakce	k1gFnPc3	reakce
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
nízké	nízký	k2eAgFnSc3d1	nízká
účinnosti	účinnost	k1gFnSc3	účinnost
je	být	k5eAaImIp3nS	být
však	však	k9	však
tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
historií	historie	k1gFnPc2	historie
(	(	kIx(	(
<g/>
malá	malý	k2eAgFnSc1d1	malá
čistota	čistota	k1gFnSc1	čistota
vznikajícího	vznikající	k2eAgInSc2d1	vznikající
hydroxidu	hydroxid	k1gInSc2	hydroxid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
přípravě	příprava	k1gFnSc3	příprava
užívá	užívat	k5eAaImIp3nS	užívat
způsobu	způsob	k1gInSc2	způsob
membránového	membránový	k2eAgInSc2d1	membránový
<g/>
,	,	kIx,	,
fungujícího	fungující	k2eAgInSc2d1	fungující
na	na	k7c6	na
podobném	podobný	k2eAgInSc6d1	podobný
principu	princip	k1gInSc6	princip
jako	jako	k9	jako
diafragmové	diafragmový	k2eAgNnSc1d1	diafragmový
provedení	provedení	k1gNnSc1	provedení
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
účinností	účinnost	k1gFnSc7	účinnost
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
diafragmový	diafragmový	k2eAgInSc4d1	diafragmový
i	i	k8xC	i
membránový	membránový	k2eAgInSc4d1	membránový
způsob	způsob	k1gInSc4	způsob
jsou	být	k5eAaImIp3nP	být
společné	společný	k2eAgFnPc4d1	společná
užívané	užívaný	k2eAgFnPc4d1	užívaná
elektrody	elektroda	k1gFnPc4	elektroda
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
anoda	anoda	k1gFnSc1	anoda
(	(	kIx(	(
<g/>
rozměrově	rozměrově	k6eAd1	rozměrově
stálá	stálý	k2eAgFnSc1d1	stálá
<g/>
)	)	kIx)	)
z	z	k7c2	z
titanu	titan	k1gInSc2	titan
pokrytého	pokrytý	k2eAgInSc2d1	pokrytý
tenkou	tenký	k2eAgFnSc7d1	tenká
vrstvou	vrstva	k1gFnSc7	vrstva
oxidu	oxid	k1gInSc2	oxid
titaničitého	titaničitý	k2eAgNnSc2d1	titaničitý
a	a	k8xC	a
katoda	katoda	k1gFnSc1	katoda
ze	z	k7c2	z
železa	železo	k1gNnSc2	železo
či	či	k8xC	či
niklu	nikl	k1gInSc2	nikl
<g/>
.	.	kIx.	.
</s>
<s>
Tlakové	tlakový	k2eAgFnPc1d1	tlaková
kovové	kovový	k2eAgFnPc1d1	kovová
lahve	lahev	k1gFnPc1	lahev
s	s	k7c7	s
plynným	plynný	k2eAgInSc7d1	plynný
chlorem	chlor	k1gInSc7	chlor
jsou	být	k5eAaImIp3nP	být
označeny	označit	k5eAaPmNgInP	označit
žlutým	žlutý	k2eAgInSc7d1	žlutý
pruhem	pruh	k1gInSc7	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
práci	práce	k1gFnSc6	práce
s	s	k7c7	s
plynným	plynný	k2eAgInSc7d1	plynný
chlorem	chlor	k1gInSc7	chlor
je	být	k5eAaImIp3nS	být
nezbytné	zbytný	k2eNgNnSc1d1	nezbytné
zachovávat	zachovávat	k5eAaImF	zachovávat
přísná	přísný	k2eAgNnPc4d1	přísné
bezpečnostní	bezpečnostní	k2eAgNnPc4d1	bezpečnostní
opatření	opatření	k1gNnPc4	opatření
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
silně	silně	k6eAd1	silně
toxický	toxický	k2eAgInSc1d1	toxický
a	a	k8xC	a
z	z	k7c2	z
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
provozů	provoz	k1gInPc2	provoz
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
řada	řada	k1gFnSc1	řada
havárií	havárie	k1gFnPc2	havárie
se	s	k7c7	s
smrtelnými	smrtelný	k2eAgInPc7d1	smrtelný
následky	následek	k1gInPc7	následek
<g/>
.	.	kIx.	.
</s>
<s>
Plynný	plynný	k2eAgInSc1d1	plynný
chlor	chlor	k1gInSc1	chlor
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
jako	jako	k8xS	jako
první	první	k4xOgFnSc1	první
prakticky	prakticky	k6eAd1	prakticky
použitá	použitý	k2eAgFnSc1d1	použitá
chemická	chemický	k2eAgFnSc1d1	chemická
bojová	bojový	k2eAgFnSc1d1	bojová
látka	látka	k1gFnSc1	látka
již	již	k6eAd1	již
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
<g/>
.	.	kIx.	.
</s>
<s>
Elementární	elementární	k2eAgInSc1d1	elementární
chlor	chlor	k1gInSc1	chlor
se	se	k3xPyFc4	se
prakticky	prakticky	k6eAd1	prakticky
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
desinfekci	desinfekce	k1gFnSc3	desinfekce
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
i	i	k9	i
v	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
koncentracích	koncentrace	k1gFnPc6	koncentrace
hubí	hubit	k5eAaImIp3nP	hubit
bakterie	bakterie	k1gFnPc1	bakterie
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
nadbytek	nadbytek	k1gInSc1	nadbytek
lze	lze	k6eAd1	lze
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
snadno	snadno	k6eAd1	snadno
odstranit	odstranit	k5eAaPmF	odstranit
pouhým	pouhý	k2eAgNnSc7d1	pouhé
probubláním	probublání	k1gNnSc7	probublání
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc4d1	další
uplatnění	uplatnění	k1gNnSc4	uplatnění
nachází	nacházet	k5eAaImIp3nS	nacházet
chlor	chlor	k1gInSc1	chlor
v	v	k7c6	v
papírenském	papírenský	k2eAgInSc6d1	papírenský
a	a	k8xC	a
textilním	textilní	k2eAgInSc6d1	textilní
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
bělení	bělení	k1gNnSc3	bělení
surovin	surovina	k1gFnPc2	surovina
<g/>
.	.	kIx.	.
</s>
<s>
Chlorovodík	chlorovodík	k1gInSc1	chlorovodík
HCl	HCl	k1gFnSc2	HCl
je	být	k5eAaImIp3nS	být
bezbarvý	bezbarvý	k2eAgInSc1d1	bezbarvý
plyn	plyn	k1gInSc1	plyn
o	o	k7c6	o
bodu	bod	k1gInSc6	bod
varu	var	k1gInSc2	var
53	[number]	k4	53
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
po	po	k7c6	po
rozpuštění	rozpuštění	k1gNnSc6	rozpuštění
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
reaguje	reagovat	k5eAaBmIp3nS	reagovat
silně	silně	k6eAd1	silně
kysele	kysele	k6eAd1	kysele
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xS	jako
kyselina	kyselina	k1gFnSc1	kyselina
chlorovodíková	chlorovodíkový	k2eAgFnSc1d1	chlorovodíková
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
působením	působení	k1gNnSc7	působení
kyseliny	kyselina	k1gFnSc2	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
na	na	k7c4	na
chlorid	chlorid	k1gInSc4	chlorid
sodný	sodný	k2eAgInSc4d1	sodný
nebo	nebo	k8xC	nebo
častěji	často	k6eAd2	často
přímou	přímý	k2eAgFnSc7d1	přímá
reakcí	reakce	k1gFnSc7	reakce
plynného	plynný	k2eAgInSc2d1	plynný
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
chloru	chlor	k1gInSc2	chlor
<g/>
.	.	kIx.	.
</s>
<s>
Značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
chlorovodíku	chlorovodík	k1gInSc2	chlorovodík
vzniká	vznikat	k5eAaImIp3nS	vznikat
jako	jako	k9	jako
odpadní	odpadní	k2eAgInSc4d1	odpadní
produkt	produkt	k1gInSc4	produkt
při	při	k7c6	při
chloraci	chlorace	k1gFnSc6	chlorace
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
<g/>
.	.	kIx.	.
</s>
<s>
Komerčně	komerčně	k6eAd1	komerčně
se	se	k3xPyFc4	se
dodává	dodávat	k5eAaImIp3nS	dodávat
v	v	k7c6	v
roztocích	roztok	k1gInPc6	roztok
o	o	k7c6	o
koncentraci	koncentrace	k1gFnSc6	koncentrace
kolem	kolem	k7c2	kolem
35	[number]	k4	35
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Směs	směs	k1gFnSc1	směs
kyseliny	kyselina	k1gFnSc2	kyselina
chlorovodíkové	chlorovodíkový	k2eAgFnSc2d1	chlorovodíková
a	a	k8xC	a
dusičné	dusičný	k2eAgFnSc2d1	dusičná
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
lučavka	lučavka	k1gFnSc1	lučavka
královská	královský	k2eAgFnSc1d1	královská
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
směs	směs	k1gFnSc1	směs
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
i	i	k9	i
velmi	velmi	k6eAd1	velmi
odolné	odolný	k2eAgInPc4d1	odolný
drahé	drahý	k2eAgInPc4d1	drahý
kovy	kov	k1gInPc4	kov
jako	jako	k8xC	jako
zlato	zlato	k1gNnSc4	zlato
nebo	nebo	k8xC	nebo
platinu	platina	k1gFnSc4	platina
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
jevu	jev	k1gInSc3	jev
dochází	docházet	k5eAaImIp3nS	docházet
kombinací	kombinace	k1gFnPc2	kombinace
velmi	velmi	k6eAd1	velmi
silných	silný	k2eAgFnPc2d1	silná
oxidačních	oxidační	k2eAgFnPc2d1	oxidační
vlastností	vlastnost	k1gFnPc2	vlastnost
kyseliny	kyselina	k1gFnSc2	kyselina
dusičné	dusičný	k2eAgFnSc2d1	dusičná
a	a	k8xC	a
komplexotvorných	komplexotvorný	k2eAgFnPc2d1	komplexotvorný
vlastností	vlastnost	k1gFnPc2	vlastnost
chloridového	chloridový	k2eAgInSc2d1	chloridový
iontu	ion	k1gInSc2	ion
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
stabilní	stabilní	k2eAgInPc4d1	stabilní
komplexní	komplexní	k2eAgInPc4d1	komplexní
ionty	ion	k1gInPc4	ion
[	[	kIx(	[
<g/>
AuCl	AuCl	k1gInSc1	AuCl
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
<g/>
-	-	kIx~	-
a	a	k8xC	a
[	[	kIx(	[
<g/>
PtCl	PtCl	k1gInSc1	PtCl
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
.	.	kIx.	.
</s>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
sodný	sodný	k2eAgInSc1d1	sodný
NaCl	NaCl	k1gInSc1	NaCl
je	být	k5eAaImIp3nS	být
nezbytnou	nezbytný	k2eAgFnSc7d1	nezbytná
složkou	složka	k1gFnSc7	složka
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
obsažen	obsáhnout	k5eAaPmNgInS	obsáhnout
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
i	i	k8xC	i
krevní	krevní	k2eAgFnSc6d1	krevní
plasmě	plasma	k1gFnSc6	plasma
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
příjem	příjem	k1gInSc1	příjem
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
je	být	k5eAaImIp3nS	být
však	však	k9	však
třeba	třeba	k6eAd1	třeba
udržovat	udržovat	k5eAaImF	udržovat
v	v	k7c6	v
rozumných	rozumný	k2eAgFnPc6d1	rozumná
mezích	mez	k1gFnPc6	mez
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gInSc1	jeho
přebytek	přebytek	k1gInSc1	přebytek
působí	působit	k5eAaImIp3nS	působit
nepříznivě	příznivě	k6eNd1	příznivě
na	na	k7c4	na
kardiovaskulární	kardiovaskulární	k2eAgInSc4d1	kardiovaskulární
systém	systém	k1gInSc4	systém
a	a	k8xC	a
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
riziko	riziko	k1gNnSc4	riziko
infarktu	infarkt	k1gInSc2	infarkt
myokardu	myokard	k1gInSc2	myokard
<g/>
.	.	kIx.	.
</s>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
sodný	sodný	k2eAgInSc1d1	sodný
a	a	k8xC	a
chlorid	chlorid	k1gInSc1	chlorid
vápenatý	vápenatý	k2eAgInSc4d1	vápenatý
CaCl	CaCl	k1gInSc4	CaCl
<g/>
2	[number]	k4	2
se	se	k3xPyFc4	se
v	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
období	období	k1gNnSc6	období
rozprašují	rozprašovat	k5eAaImIp3nP	rozprašovat
na	na	k7c4	na
vozovky	vozovka	k1gFnPc4	vozovka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
odstranily	odstranit	k5eAaPmAgFnP	odstranit
námrazu	námraza	k1gFnSc4	námraza
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaPmIp3nS	využívat
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
faktu	fakt	k1gInSc2	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
roztoky	roztok	k1gInPc1	roztok
těchto	tento	k3xDgFnPc2	tento
látek	látka	k1gFnPc2	látka
mrznou	mrznout	k5eAaImIp3nP	mrznout
až	až	k9	až
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
pod	pod	k7c7	pod
-	-	kIx~	-
20	[number]	k4	20
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
při	při	k7c6	při
kontaktu	kontakt	k1gInSc6	kontakt
ledu	led	k1gInSc2	led
s	s	k7c7	s
uvedenými	uvedený	k2eAgInPc7d1	uvedený
chloridy	chlorid	k1gInPc7	chlorid
led	led	k1gInSc1	led
taje	taj	k1gInSc2	taj
a	a	k8xC	a
vznikají	vznikat	k5eAaImIp3nP	vznikat
nasycené	nasycený	k2eAgInPc1d1	nasycený
chloridové	chloridový	k2eAgInPc1d1	chloridový
roztoky	roztok	k1gInPc1	roztok
<g/>
.	.	kIx.	.
</s>
<s>
Problémem	problém	k1gInSc7	problém
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
korozivní	korozivní	k2eAgNnSc4d1	korozivní
působení	působení	k1gNnSc4	působení
těchto	tento	k3xDgInPc2	tento
roztoků	roztok	k1gInPc2	roztok
na	na	k7c4	na
kovové	kovový	k2eAgFnPc4d1	kovová
součásti	součást	k1gFnPc4	součást
automobilů	automobil	k1gInPc2	automobil
i	i	k8xC	i
zatížení	zatížení	k1gNnSc4	zatížení
půdy	půda	k1gFnSc2	půda
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
komunikací	komunikace	k1gFnPc2	komunikace
vysokými	vysoký	k2eAgFnPc7d1	vysoká
koncentracemi	koncentrace	k1gFnPc7	koncentrace
solí	sůl	k1gFnPc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
chloridů	chlorid	k1gInPc2	chlorid
je	být	k5eAaImIp3nS	být
výborně	výborně	k6eAd1	výborně
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nS	tvořit
chlorid	chlorid	k1gInSc1	chlorid
stříbrný	stříbrný	k2eAgInSc1d1	stříbrný
AgCl	AgCl	k1gInSc1	AgCl
a	a	k8xC	a
chlorid	chlorid	k1gInSc1	chlorid
rtuťný	rtuťný	k2eAgInSc1d1	rtuťný
neboli	neboli	k8xC	neboli
kalomel	kalomel	k1gInSc1	kalomel
Hg	Hg	k1gFnSc2	Hg
<g/>
2	[number]	k4	2
<g/>
Cl	Cl	k1gMnPc2	Cl
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Nerozpustnosti	Nerozpustnost	k1gFnPc1	Nerozpustnost
AgCl	AgCla	k1gFnPc2	AgCla
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
využít	využít	k5eAaPmF	využít
k	k	k7c3	k
oddělení	oddělení	k1gNnSc3	oddělení
stříbra	stříbro	k1gNnSc2	stříbro
z	z	k7c2	z
roztoků	roztok	k1gInPc2	roztok
dalších	další	k2eAgInPc2d1	další
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
kovové	kovový	k2eAgNnSc4d1	kovové
stříbro	stříbro	k1gNnSc4	stříbro
lze	lze	k6eAd1	lze
z	z	k7c2	z
filtrátu	filtrát	k1gInSc2	filtrát
AgCl	AgCl	k1gInSc4	AgCl
poměrně	poměrně	k6eAd1	poměrně
jednoduše	jednoduše	k6eAd1	jednoduše
získat	získat	k5eAaPmF	získat
pyrometalurgicky	pyrometalurgicky	k6eAd1	pyrometalurgicky
<g/>
.	.	kIx.	.
</s>
<s>
Vysoce	vysoce	k6eAd1	vysoce
čistý	čistý	k2eAgInSc1d1	čistý
chlorid	chlorid	k1gInSc1	chlorid
stříbrný	stříbrný	k1gInSc1	stříbrný
je	být	k5eAaImIp3nS	být
důležitou	důležitý	k2eAgFnSc7d1	důležitá
složkou	složka	k1gFnSc7	složka
fotografických	fotografický	k2eAgInPc2d1	fotografický
materiálů	materiál	k1gInPc2	materiál
(	(	kIx(	(
<g/>
filmy	film	k1gInPc1	film
<g/>
,	,	kIx,	,
fotopapíry	fotopapír	k1gInPc1	fotopapír
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kalomel	kalomel	k1gInSc1	kalomel
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
svoji	svůj	k3xOyFgFnSc4	svůj
toxicitu	toxicita	k1gFnSc4	toxicita
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
analytické	analytický	k2eAgFnSc6d1	analytická
chemii	chemie	k1gFnSc6	chemie
jako	jako	k8xC	jako
redukční	redukční	k2eAgNnSc1d1	redukční
činidlo	činidlo	k1gNnSc1	činidlo
pří	přít	k5eAaImIp3nS	přít
vážkovém	vážkový	k2eAgNnSc6d1	vážkové
stanovení	stanovení	k1gNnSc6	stanovení
platinových	platinový	k2eAgInPc2d1	platinový
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
chlorná	chlorný	k2eAgFnSc1d1	chlorná
HClO	HClO	k1gFnSc1	HClO
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
slabá	slabý	k2eAgFnSc1d1	slabá
kyselina	kyselina	k1gFnSc1	kyselina
a	a	k8xC	a
chemicky	chemicky	k6eAd1	chemicky
značně	značně	k6eAd1	značně
nestálá	stálý	k2eNgFnSc1d1	nestálá
sloučenina	sloučenina	k1gFnSc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgInSc4d2	veliký
význam	význam	k1gInSc4	význam
mají	mít	k5eAaImIp3nP	mít
její	její	k3xOp3gFnPc4	její
soli	sůl	k1gFnPc1	sůl
chlornany	chlornan	k1gInPc4	chlornan
<g/>
.	.	kIx.	.
</s>
<s>
Chlornan	chlornan	k1gInSc1	chlornan
sodný	sodný	k2eAgInSc1d1	sodný
NaClO	NaClO	k1gFnSc4	NaClO
je	být	k5eAaImIp3nS	být
sloučenina	sloučenina	k1gFnSc1	sloučenina
s	s	k7c7	s
oxidačními	oxidační	k2eAgMnPc7d1	oxidační
a	a	k8xC	a
především	především	k9	především
dezinfekčními	dezinfekční	k2eAgInPc7d1	dezinfekční
účinky	účinek	k1gInPc7	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
zaváděním	zavádění	k1gNnSc7	zavádění
plynného	plynný	k2eAgInSc2d1	plynný
chloru	chlor	k1gInSc2	chlor
do	do	k7c2	do
roztoku	roztok	k1gInSc2	roztok
hydroxidu	hydroxid	k1gInSc2	hydroxid
sodného	sodný	k2eAgInSc2d1	sodný
<g/>
,	,	kIx,	,
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
chlornanového	chlornanový	k2eAgInSc2d1	chlornanový
a	a	k8xC	a
chloridového	chloridový	k2eAgInSc2d1	chloridový
iontu	ion	k1gInSc2	ion
<g/>
.	.	kIx.	.
</s>
<s>
Reakce	reakce	k1gFnSc1	reakce
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
disproporcionace	disproporcionace	k1gFnSc1	disproporcionace
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
l	l	kA	l
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
2	[number]	k4	2
:	:	kIx,	:
N	N	kA	N
a	a	k8xC	a
O	o	k7c6	o
H	H	kA	H
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
N	N	kA	N
a	a	k8xC	a
C	C	kA	C
l	l	kA	l
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
N	N	kA	N
a	a	k8xC	a
C	C	kA	C
l	l	kA	l
O	o	k7c4	o
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c4	o
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
Cl	Cl	k1gFnSc1	Cl
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
NaOH	NaOH	k1gFnSc1	NaOH
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
NaCl	NaCl	k1gInSc1	NaCl
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
NaClO	NaClO	k1gFnSc1	NaClO
<g/>
}	}	kIx)	}
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
H	H	kA	H
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
O	O	kA	O
<g/>
}	}	kIx)	}
}	}	kIx)	}
:	:	kIx,	:
Vyrobený	vyrobený	k2eAgInSc1d1	vyrobený
chlornan	chlornan	k1gInSc1	chlornan
sodný	sodný	k2eAgInSc1d1	sodný
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
distribuován	distribuovat	k5eAaBmNgInS	distribuovat
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
používán	používat	k5eAaImNgInS	používat
přímo	přímo	k6eAd1	přímo
jako	jako	k8xS	jako
roztok	roztok	k1gInSc1	roztok
<g/>
.	.	kIx.	.
</s>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
chlorečná	chlorečný	k2eAgFnSc1d1	chlorečný
HClO	HClO	k1gFnSc1	HClO
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
nestálá	stálý	k2eNgFnSc1d1	nestálá
kapalina	kapalina	k1gFnSc1	kapalina
se	s	k7c7	s
silně	silně	k6eAd1	silně
oxidačními	oxidační	k2eAgInPc7d1	oxidační
účinky	účinek	k1gInPc7	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
solí	sůl	k1gFnPc2	sůl
jsou	být	k5eAaImIp3nP	být
nejznámější	známý	k2eAgInSc1d3	nejznámější
chlorečnan	chlorečnan	k1gInSc1	chlorečnan
sodný	sodný	k2eAgInSc1d1	sodný
a	a	k8xC	a
draselný	draselný	k2eAgInSc1d1	draselný
<g/>
,	,	kIx,	,
základní	základní	k2eAgFnSc2d1	základní
složky	složka	k1gFnSc2	složka
"	"	kIx"	"
<g/>
Travexu	Travex	k1gInSc2	Travex
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
přípravku	přípravka	k1gFnSc4	přípravka
pro	pro	k7c4	pro
hubení	hubení	k1gNnSc4	hubení
plevelů	plevel	k1gInPc2	plevel
a	a	k8xC	a
trávy	tráva	k1gFnSc2	tráva
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
tyto	tento	k3xDgInPc1	tento
chlorečnany	chlorečnan	k1gInPc1	chlorečnan
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
velmi	velmi	k6eAd1	velmi
silnými	silný	k2eAgFnPc7d1	silná
oxidačními	oxidační	k2eAgFnPc7d1	oxidační
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
a	a	k8xC	a
ve	v	k7c6	v
směsi	směs	k1gFnSc6	směs
s	s	k7c7	s
organickými	organický	k2eAgFnPc7d1	organická
látkami	látka	k1gFnPc7	látka
(	(	kIx(	(
<g/>
cukr	cukr	k1gInSc1	cukr
<g/>
)	)	kIx)	)
velmi	velmi	k6eAd1	velmi
snadno	snadno	k6eAd1	snadno
a	a	k8xC	a
silně	silně	k6eAd1	silně
explodují	explodovat	k5eAaBmIp3nP	explodovat
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
především	především	k6eAd1	především
mladších	mladý	k2eAgMnPc2d2	mladší
amatérských	amatérský	k2eAgMnPc2d1	amatérský
pyrotechniků	pyrotechnik	k1gMnPc2	pyrotechnik
byla	být	k5eAaImAgFnS	být
při	při	k7c6	při
experimentech	experiment	k1gInPc6	experiment
s	s	k7c7	s
těmito	tento	k3xDgFnPc7	tento
směsmi	směs	k1gFnPc7	směs
vážně	vážně	k6eAd1	vážně
zraněna	zranit	k5eAaPmNgFnS	zranit
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Kyselina	kyselina	k1gFnSc1	kyselina
chloristá	chloristý	k2eAgFnSc1d1	chloristá
HClO	HClO	k1gFnSc1	HClO
<g/>
4	[number]	k4	4
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejsilnějších	silný	k2eAgFnPc2d3	nejsilnější
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
elektrochemickou	elektrochemický	k2eAgFnSc7d1	elektrochemická
oxidací	oxidace	k1gFnSc7	oxidace
chlorečnanů	chlorečnan	k1gInPc2	chlorečnan
a	a	k8xC	a
komerčně	komerčně	k6eAd1	komerčně
se	se	k3xPyFc4	se
distribuuje	distribuovat	k5eAaBmIp3nS	distribuovat
jako	jako	k9	jako
vodný	vodný	k2eAgInSc1d1	vodný
roztok	roztok	k1gInSc1	roztok
o	o	k7c6	o
koncentraci	koncentrace	k1gFnSc6	koncentrace
přibližně	přibližně	k6eAd1	přibližně
70	[number]	k4	70
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
prakticky	prakticky	k6eAd1	prakticky
neomezeně	omezeně	k6eNd1	omezeně
stálý	stálý	k2eAgInSc1d1	stálý
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zředěných	zředěný	k2eAgInPc6d1	zředěný
roztocích	roztok	k1gInPc6	roztok
se	se	k3xPyFc4	se
kyselina	kyselina	k1gFnSc1	kyselina
chloristá	chloristý	k2eAgFnSc1d1	chloristá
chová	chovat	k5eAaImIp3nS	chovat
pouze	pouze	k6eAd1	pouze
jako	jako	k8xS	jako
velmi	velmi	k6eAd1	velmi
silná	silný	k2eAgFnSc1d1	silná
kyselina	kyselina	k1gFnSc1	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
koncentrovaném	koncentrovaný	k2eAgInSc6d1	koncentrovaný
stavu	stav	k1gInSc6	stav
a	a	k8xC	a
za	za	k7c4	za
horka	horko	k1gNnPc4	horko
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
mimořádně	mimořádně	k6eAd1	mimořádně
silným	silný	k2eAgInSc7d1	silný
oxidačním	oxidační	k2eAgInSc7d1	oxidační
prostředkem	prostředek	k1gInSc7	prostředek
<g/>
,	,	kIx,	,
využívaným	využívaný	k2eAgMnSc7d1	využívaný
např.	např.	kA	např.
k	k	k7c3	k
rozkladům	rozklad	k1gInPc3	rozklad
stálých	stálý	k2eAgInPc2d1	stálý
organických	organický	k2eAgInPc2d1	organický
polymerů	polymer	k1gInPc2	polymer
(	(	kIx(	(
<g/>
polystyren	polystyren	k1gInSc1	polystyren
<g/>
,	,	kIx,	,
PVC	PVC	kA	PVC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
se	se	k3xPyFc4	se
však	však	k9	však
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
uvedené	uvedený	k2eAgFnPc4d1	uvedená
reakce	reakce	k1gFnPc4	reakce
prováděl	provádět	k5eAaImAgInS	provádět
pouze	pouze	k6eAd1	pouze
zkušený	zkušený	k2eAgMnSc1d1	zkušený
analytický	analytický	k2eAgMnSc1d1	analytický
chemik	chemik	k1gMnSc1	chemik
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
při	při	k7c6	při
nich	on	k3xPp3gFnPc6	on
hrozí	hrozit	k5eAaImIp3nS	hrozit
nebezpečí	nebezpečí	k1gNnSc3	nebezpečí
exploze	exploze	k1gFnSc2	exploze
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
organické	organický	k2eAgFnPc1d1	organická
látky	látka	k1gFnPc1	látka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
dřevěné	dřevěný	k2eAgFnPc4d1	dřevěná
součásti	součást	k1gFnPc4	součást
laboratorních	laboratorní	k2eAgFnPc2d1	laboratorní
digestoří	digestoř	k1gFnPc2	digestoř
<g/>
)	)	kIx)	)
mohou	moct	k5eAaImIp3nP	moct
po	po	k7c6	po
dlouhodobém	dlouhodobý	k2eAgInSc6d1	dlouhodobý
styku	styk	k1gInSc6	styk
s	s	k7c7	s
parami	para	k1gFnPc7	para
kyseliny	kyselina	k1gFnSc2	kyselina
chloristé	chloristý	k2eAgFnSc2d1	chloristá
nebo	nebo	k8xC	nebo
oxidu	oxid	k1gInSc2	oxid
chloristého	chloristý	k2eAgNnSc2d1	chloristý
samovolně	samovolně	k6eAd1	samovolně
explodovat	explodovat	k5eAaBmF	explodovat
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
většina	většina	k1gFnSc1	většina
solí	sůl	k1gFnPc2	sůl
kyseliny	kyselina	k1gFnSc2	kyselina
chloristé	chloristý	k2eAgFnSc2d1	chloristá
<g/>
,	,	kIx,	,
chloristanů	chloristan	k1gInPc2	chloristan
jsou	být	k5eAaImIp3nP	být
sloučeniny	sloučenina	k1gFnPc1	sloučenina
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpustné	rozpustný	k2eAgNnSc1d1	rozpustné
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nP	tvořit
chloristan	chloristan	k1gInSc4	chloristan
draselný	draselný	k2eAgInSc4d1	draselný
KClO	KClO	k1gFnSc7	KClO
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
rubidný	rubidný	k2eAgMnSc1d1	rubidný
RbClO	RbClO	k1gMnSc1	RbClO
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
cesný	cesný	k2eAgMnSc1d1	cesný
CsClO	CsClO	k1gMnSc1	CsClO
<g/>
4	[number]	k4	4
a	a	k8xC	a
amonný	amonný	k2eAgMnSc1d1	amonný
(	(	kIx(	(
<g/>
NH	NH	kA	NH
<g/>
4	[number]	k4	4
)	)	kIx)	)
<g/>
ClO	clo	k1gNnSc1	clo
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
analytické	analytický	k2eAgFnSc6d1	analytická
chemii	chemie	k1gFnSc6	chemie
slouží	sloužit	k5eAaImIp3nS	sloužit
tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
k	k	k7c3	k
důkazu	důkaz	k1gInSc3	důkaz
nebo	nebo	k8xC	nebo
separaci	separace	k1gFnSc3	separace
uvedených	uvedený	k2eAgInPc2d1	uvedený
kationtů	kation	k1gInPc2	kation
<g/>
.	.	kIx.	.
</s>
<s>
Chloristan	chloristan	k1gInSc1	chloristan
draselný	draselný	k2eAgInSc1d1	draselný
a	a	k8xC	a
amonný	amonný	k2eAgMnSc1d1	amonný
se	se	k3xPyFc4	se
jsou	být	k5eAaImIp3nP	být
složkou	složka	k1gFnSc7	složka
různých	různý	k2eAgFnPc2d1	různá
pyrotechnických	pyrotechnický	k2eAgFnPc2d1	pyrotechnická
směsí	směs	k1gFnPc2	směs
<g/>
.	.	kIx.	.
</s>
<s>
Chloristan	chloristan	k1gInSc1	chloristan
amonný	amonný	k2eAgInSc1d1	amonný
je	být	k5eAaImIp3nS	být
základem	základ	k1gInSc7	základ
pevného	pevný	k2eAgNnSc2d1	pevné
paliva	palivo	k1gNnSc2	palivo
raketových	raketový	k2eAgInPc2d1	raketový
motorů	motor	k1gInPc2	motor
<g/>
,	,	kIx,	,
využívá	využívat	k5eAaPmIp3nS	využívat
jej	on	k3xPp3gMnSc4	on
např.	např.	kA	např.
i	i	k8xC	i
americký	americký	k2eAgInSc1d1	americký
raketoplán	raketoplán	k1gInSc1	raketoplán
Space	Space	k1gFnSc2	Space
Shuttle	Shuttle	k1gFnSc2	Shuttle
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
chlorovaných	chlorovaný	k2eAgInPc2d1	chlorovaný
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
milionů	milion	k4xCgInPc2	milion
tun	tuna	k1gFnPc2	tuna
nejrůznějších	různý	k2eAgFnPc2d3	nejrůznější
sloučenin	sloučenina	k1gFnPc2	sloučenina
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
představuje	představovat	k5eAaImIp3nS	představovat
řada	řada	k1gFnSc1	řada
těchto	tento	k3xDgInPc2	tento
produktů	produkt	k1gInPc2	produkt
velkou	velký	k2eAgFnSc4d1	velká
ekologickou	ekologický	k2eAgFnSc4d1	ekologická
zátěž	zátěž	k1gFnSc4	zátěž
pro	pro	k7c4	pro
naši	náš	k3xOp1gFnSc4	náš
planetu	planeta	k1gFnSc4	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
sloučenin	sloučenina	k1gFnPc2	sloučenina
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
svého	svůj	k3xOyFgInSc2	svůj
objevu	objev	k1gInSc2	objev
pokládána	pokládán	k2eAgFnSc1d1	pokládána
za	za	k7c4	za
prakticky	prakticky	k6eAd1	prakticky
neškodné	škodný	k2eNgFnPc4d1	neškodná
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
mnohaletém	mnohaletý	k2eAgNnSc6d1	mnohaleté
masovém	masový	k2eAgNnSc6d1	masové
používání	používání	k1gNnSc6	používání
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gNnSc1	jejich
dlouhodobé	dlouhodobý	k2eAgNnSc1d1	dlouhodobé
působení	působení	k1gNnSc1	působení
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
katastrofální	katastrofální	k2eAgInPc4d1	katastrofální
následky	následek	k1gInPc4	následek
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
příkladů	příklad	k1gInPc2	příklad
je	být	k5eAaImIp3nS	být
vliv	vliv	k1gInSc1	vliv
chlorofluorovaných	chlorofluorovaný	k2eAgInPc2d1	chlorofluorovaný
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
-	-	kIx~	-
freonů	freon	k1gInPc2	freon
na	na	k7c4	na
ozonovou	ozonový	k2eAgFnSc4d1	ozonová
vrstvu	vrstva	k1gFnSc4	vrstva
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
podrobněji	podrobně	k6eAd2	podrobně
diskutován	diskutovat	k5eAaImNgInS	diskutovat
v	v	k7c6	v
kapitole	kapitola	k1gFnSc6	kapitola
o	o	k7c6	o
fluoru	fluor	k1gInSc6	fluor
<g/>
.	.	kIx.	.
</s>
<s>
Nejjednoduššími	jednoduchý	k2eAgInPc7d3	nejjednodušší
významnými	významný	k2eAgInPc7d1	významný
chlorovanými	chlorovaný	k2eAgInPc7d1	chlorovaný
uhlovodíky	uhlovodík	k1gInPc7	uhlovodík
jsou	být	k5eAaImIp3nP	být
trichlormetan	trichlormetan	k1gInSc4	trichlormetan
neboli	neboli	k8xC	neboli
chloroform	chloroform	k1gInSc4	chloroform
CHCl	CHCl	k1gInSc1	CHCl
<g/>
3	[number]	k4	3
a	a	k8xC	a
tetrachlormetan	tetrachlormetan	k1gInSc1	tetrachlormetan
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
tetrachlor	tetrachlor	k1gInSc1	tetrachlor
CCl	CCl	k1gFnSc2	CCl
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
sloučeniny	sloučenina	k1gFnPc1	sloučenina
jsou	být	k5eAaImIp3nP	být
těžké	těžký	k2eAgFnPc1d1	těžká
kapaliny	kapalina	k1gFnPc1	kapalina
s	s	k7c7	s
relativně	relativně	k6eAd1	relativně
nízkým	nízký	k2eAgInSc7d1	nízký
bodem	bod	k1gInSc7	bod
varu	var	k1gInSc2	var
(	(	kIx(	(
<g/>
CHCl	CHCl	k1gInSc1	CHCl
<g/>
3	[number]	k4	3
62	[number]	k4	62
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
CCl	CCl	k1gFnSc1	CCl
<g/>
4	[number]	k4	4
77	[number]	k4	77
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
jsou	být	k5eAaImIp3nP	být
nehořlavé	hořlavý	k2eNgFnPc1d1	nehořlavá
a	a	k8xC	a
při	při	k7c6	při
vdechování	vdechování	k1gNnSc6	vdechování
nejsou	být	k5eNaImIp3nP	být
toxické	toxický	k2eAgFnPc1d1	toxická
<g/>
.	.	kIx.	.
</s>
<s>
Chloroform	chloroform	k1gInSc1	chloroform
má	mít	k5eAaImIp3nS	mít
navíc	navíc	k6eAd1	navíc
narkotizační	narkotizační	k2eAgInPc4d1	narkotizační
účinky	účinek	k1gInPc4	účinek
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
omezeném	omezený	k2eAgNnSc6d1	omezené
množství	množství	k1gNnSc6	množství
používán	používat	k5eAaImNgInS	používat
i	i	k9	i
v	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
pro	pro	k7c4	pro
uspávání	uspávání	k1gNnSc4	uspávání
pacientů	pacient	k1gMnPc2	pacient
při	při	k7c6	při
operacích	operace	k1gFnPc6	operace
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
kapaliny	kapalina	k1gFnPc1	kapalina
se	se	k3xPyFc4	se
prakticky	prakticky	k6eAd1	prakticky
nemísí	mísit	k5eNaImIp3nP	mísit
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
výbornými	výborný	k2eAgNnPc7d1	výborné
rozpouštědly	rozpouštědlo	k1gNnPc7	rozpouštědlo
nepolárních	polární	k2eNgFnPc2d1	nepolární
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
a	a	k8xC	a
tuků	tuk	k1gInPc2	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
několika	několik	k4yIc7	několik
lety	let	k1gInPc7	let
prakticky	prakticky	k6eAd1	prakticky
všechny	všechen	k3xTgFnPc1	všechen
čistírny	čistírna	k1gFnPc1	čistírna
oděvů	oděv	k1gInPc2	oděv
praly	prát	k5eAaImAgInP	prát
zboží	zboží	k1gNnSc4	zboží
svých	svůj	k3xOyFgMnPc2	svůj
zákazníků	zákazník	k1gMnPc2	zákazník
ve	v	k7c6	v
směsi	směs	k1gFnSc6	směs
těchto	tento	k3xDgFnPc2	tento
kapalin	kapalina	k1gFnPc2	kapalina
<g/>
.	.	kIx.	.
</s>
<s>
Organické	organický	k2eAgNnSc1d1	organické
znečištění	znečištění	k1gNnSc1	znečištění
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
efektivně	efektivně	k6eAd1	efektivně
odstraněno	odstranit	k5eAaPmNgNnS	odstranit
a	a	k8xC	a
díky	díky	k7c3	díky
nízkému	nízký	k2eAgInSc3d1	nízký
bodu	bod	k1gInSc3	bod
varu	var	k1gInSc2	var
čisticí	čisticí	k2eAgFnSc2d1	čisticí
směsi	směs	k1gFnSc2	směs
byly	být	k5eAaImAgInP	být
její	její	k3xOp3gInPc1	její
zbytky	zbytek	k1gInPc1	zbytek
z	z	k7c2	z
oděvů	oděv	k1gInPc2	oděv
snadno	snadno	k6eAd1	snadno
odstraněny	odstranit	k5eAaPmNgInP	odstranit
zahřátím	zahřátí	k1gNnSc7	zahřátí
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
barev	barva	k1gFnPc2	barva
na	na	k7c4	na
kovy	kov	k1gInPc4	kov
i	i	k8xC	i
dřevo	dřevo	k1gNnSc4	dřevo
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
rozpouštědlo	rozpouštědlo	k1gNnSc4	rozpouštědlo
pigmentů	pigment	k1gInPc2	pigment
a	a	k8xC	a
ochranných	ochranný	k2eAgFnPc2d1	ochranná
látek	látka	k1gFnPc2	látka
právě	právě	k9	právě
chloroform	chloroform	k1gInSc4	chloroform
či	či	k8xC	či
tetrachlor	tetrachlor	k1gInSc4	tetrachlor
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
nehořlavosti	nehořlavost	k1gFnSc3	nehořlavost
terachlormetanu	terachlormetan	k1gInSc2	terachlormetan
sloužila	sloužit	k5eAaImAgFnS	sloužit
tato	tento	k3xDgFnSc1	tento
sloučenina	sloučenina	k1gFnSc1	sloučenina
jako	jako	k8xS	jako
náplň	náplň	k1gFnSc1	náplň
některých	některý	k3yIgInPc2	některý
hasicích	hasicí	k2eAgInPc2d1	hasicí
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
látky	látka	k1gFnPc1	látka
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
v	v	k7c6	v
omezeném	omezený	k2eAgNnSc6d1	omezené
měřítku	měřítko	k1gNnSc6	měřítko
používají	používat	k5eAaImIp3nP	používat
pro	pro	k7c4	pro
extrakci	extrakce	k1gFnSc4	extrakce
tuků	tuk	k1gInPc2	tuk
z	z	k7c2	z
biologického	biologický	k2eAgInSc2d1	biologický
materiálu	materiál	k1gInSc2	materiál
i	i	k9	i
pro	pro	k7c4	pro
separace	separace	k1gFnPc4	separace
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
ze	z	k7c2	z
směsí	směs	k1gFnPc2	směs
extrakcí	extrakce	k1gFnPc2	extrakce
typu	typ	k1gInSc2	typ
kapalina-kapalina	kapalinaapalin	k2eAgFnSc1d1	kapalina-kapalin
<g/>
.	.	kIx.	.
</s>
<s>
Podrobné	podrobný	k2eAgFnPc1d1	podrobná
dlouhodobé	dlouhodobý	k2eAgFnPc1d1	dlouhodobá
klinické	klinický	k2eAgFnPc1d1	klinická
studie	studie	k1gFnPc1	studie
však	však	k9	však
bohužel	bohužel	k9	bohužel
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
trvalý	trvalý	k2eAgInSc4d1	trvalý
dlouhodobý	dlouhodobý	k2eAgInSc4d1	dlouhodobý
styk	styk	k1gInSc4	styk
organizmu	organizmus	k1gInSc2	organizmus
s	s	k7c7	s
těmito	tento	k3xDgNnPc7	tento
rozpouštědly	rozpouštědlo	k1gNnPc7	rozpouštědlo
značně	značně	k6eAd1	značně
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
riziko	riziko	k1gNnSc1	riziko
vzniku	vznik	k1gInSc2	vznik
rakovinného	rakovinný	k2eAgNnSc2d1	rakovinné
bujení	bujení	k1gNnSc2	bujení
a	a	k8xC	a
obě	dva	k4xCgFnPc1	dva
sloučeniny	sloučenina	k1gFnPc1	sloučenina
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgFnP	dostat
do	do	k7c2	do
seznamu	seznam	k1gInSc2	seznam
karcinogenních	karcinogenní	k2eAgFnPc2d1	karcinogenní
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
této	tento	k3xDgFnSc2	tento
skutečnosti	skutečnost	k1gFnSc2	skutečnost
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
velmi	velmi	k6eAd1	velmi
silně	silně	k6eAd1	silně
omezena	omezit	k5eAaPmNgFnS	omezit
jak	jak	k8xS	jak
produkce	produkce	k1gFnSc1	produkce
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
praktické	praktický	k2eAgNnSc4d1	praktické
využití	využití	k1gNnSc4	využití
těchto	tento	k3xDgFnPc2	tento
organických	organický	k2eAgFnPc2d1	organická
rozpouštědel	rozpouštědlo	k1gNnPc2	rozpouštědlo
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc1d1	podobný
osud	osud	k1gInSc1	osud
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
skupina	skupina	k1gFnSc1	skupina
organických	organický	k2eAgInPc2d1	organický
chlorovaných	chlorovaný	k2eAgInPc2d1	chlorovaný
aromatických	aromatický	k2eAgInPc2d1	aromatický
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
polychlorované	polychlorovaný	k2eAgInPc1d1	polychlorovaný
bifenyly	bifenyl	k1gInPc1	bifenyl
neboli	neboli	k8xC	neboli
PCB	PCB	kA	PCB
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
celkem	celkem	k6eAd1	celkem
208	[number]	k4	208
kongenerů	kongener	k1gInPc2	kongener
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
počtem	počet	k1gInSc7	počet
atomů	atom	k1gInPc2	atom
chloru	chlor	k1gInSc2	chlor
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc7	jejich
vzájemnou	vzájemný	k2eAgFnSc7d1	vzájemná
polohou	poloha	k1gFnSc7	poloha
na	na	k7c6	na
bifenylovém	bifenylový	k2eAgInSc6d1	bifenylový
skeletu	skelet	k1gInSc6	skelet
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
výševroucí	výševroucí	k2eAgFnPc1d1	výševroucí
nehořlavé	hořlavý	k2eNgFnPc1d1	nehořlavá
a	a	k8xC	a
chemicky	chemicky	k6eAd1	chemicky
jsou	být	k5eAaImIp3nP	být
mimořádně	mimořádně	k6eAd1	mimořádně
stálé	stálý	k2eAgFnPc1d1	stálá
kapaliny	kapalina	k1gFnPc1	kapalina
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
výbornými	výborný	k2eAgNnPc7d1	výborné
rozpouštědly	rozpouštědlo	k1gNnPc7	rozpouštědlo
nepolárních	polární	k2eNgFnPc2d1	nepolární
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
a	a	k8xC	a
tuků	tuk	k1gInPc2	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
uplatnění	uplatnění	k1gNnSc1	uplatnění
nalezly	naleznout	k5eAaPmAgFnP	naleznout
PCB	PCB	kA	PCB
jako	jako	k8xC	jako
náplně	náplň	k1gFnPc1	náplň
transformátorů	transformátor	k1gInPc2	transformátor
<g/>
,	,	kIx,	,
kondenzátorů	kondenzátor	k1gInPc2	kondenzátor
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
elektrických	elektrický	k2eAgNnPc2d1	elektrické
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
byly	být	k5eAaImAgFnP	být
užívány	užíván	k2eAgFnPc1d1	užívána
jako	jako	k8xS	jako
hydraulické	hydraulický	k2eAgFnPc1d1	hydraulická
kapaliny	kapalina	k1gFnPc1	kapalina
<g/>
,	,	kIx,	,
součásti	součást	k1gFnPc1	součást
tiskových	tiskový	k2eAgFnPc2d1	tisková
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
nátěrových	nátěrový	k2eAgFnPc2d1	nátěrová
hmot	hmota	k1gFnPc2	hmota
<g/>
,	,	kIx,	,
tmelů	tmel	k1gInPc2	tmel
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
výrobků	výrobek	k1gInPc2	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
PCB	PCB	kA	PCB
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
počátku	počátek	k1gInSc2	počátek
jejich	jejich	k3xOp3gFnSc2	jejich
výroby	výroba	k1gFnSc2	výroba
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
zcela	zcela	k6eAd1	zcela
neškodné	škodný	k2eNgFnPc4d1	neškodná
látky	látka	k1gFnPc4	látka
a	a	k8xC	a
na	na	k7c4	na
jejich	jejich	k3xOp3gInSc4	jejich
únik	únik	k1gInSc4	únik
do	do	k7c2	do
prostředí	prostředí	k1gNnSc2	prostředí
nebyl	být	k5eNaImAgInS	být
brán	brát	k5eAaImNgInS	brát
ohled	ohled	k1gInSc1	ohled
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
desetiletích	desetiletí	k1gNnPc6	desetiletí
jejich	jejich	k3xOp3gNnSc2	jejich
užívání	užívání	k1gNnSc2	užívání
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
PCB	PCB	kA	PCB
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
nerozkládají	rozkládat	k5eNaImIp3nP	rozkládat
(	(	kIx(	(
<g/>
jsou	být	k5eAaImIp3nP	být
perzistentní	perzistentní	k2eAgFnPc1d1	perzistentní
<g/>
,	,	kIx,	,
hromadí	hromadit	k5eAaImIp3nP	hromadit
se	se	k3xPyFc4	se
v	v	k7c6	v
tělech	tělo	k1gNnPc6	tělo
živočichů	živočich	k1gMnPc2	živočich
a	a	k8xC	a
šíří	šířit	k5eAaImIp3nS	šířit
se	se	k3xPyFc4	se
v	v	k7c6	v
potravních	potravní	k2eAgInPc6d1	potravní
řetězcích	řetězec	k1gInPc6	řetězec
(	(	kIx(	(
<g/>
jsou	být	k5eAaImIp3nP	být
bioakumulativní	bioakumulativní	k2eAgMnPc1d1	bioakumulativní
<g/>
)	)	kIx)	)
a	a	k8xC	a
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
ve	v	k7c6	v
stopových	stopový	k2eAgNnPc6d1	stopové
množstvích	množství	k1gNnPc6	množství
nepříznivé	příznivý	k2eNgInPc1d1	nepříznivý
účinky	účinek	k1gInPc1	účinek
na	na	k7c4	na
živé	živý	k2eAgInPc4d1	živý
organismy	organismus	k1gInPc4	organismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgFnS	být
jejich	jejich	k3xOp3gFnSc1	jejich
výroba	výroba	k1gFnSc1	výroba
zastavena	zastaven	k2eAgFnSc1d1	zastavena
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
užívání	užívání	k1gNnSc1	užívání
omezeno	omezit	k5eAaPmNgNnS	omezit
Stockholmskou	stockholmský	k2eAgFnSc7d1	Stockholmská
úmluvou	úmluva	k1gFnSc7	úmluva
<g/>
.	.	kIx.	.
</s>
<s>
DDT	DDT	kA	DDT
je	být	k5eAaImIp3nS	být
zkratka	zkratka	k1gFnSc1	zkratka
sloučeniny	sloučenina	k1gFnSc2	sloučenina
dichlordifenyltrichlorethan	dichlordifenyltrichlorethana	k1gFnPc2	dichlordifenyltrichlorethana
nebo	nebo	k8xC	nebo
1,1	[number]	k4	1,1
<g/>
,1	,1	k4	,1
<g/>
-trichlor-	richlor-	k?	-trichlor-
<g/>
2,2	[number]	k4	2,2
<g/>
-bis	is	k1gInSc1	-bis
<g/>
(	(	kIx(	(
<g/>
p-chlorofenyl	phlorofenyl	k1gInSc1	p-chlorofenyl
<g/>
)	)	kIx)	)
<g/>
ethan	ethan	k1gInSc1	ethan
(	(	kIx(	(
<g/>
ClC	ClC	k1gFnSc1	ClC
<g/>
6	[number]	k4	6
<g/>
H	H	kA	H
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
CH	Ch	kA	Ch
<g/>
(	(	kIx(	(
<g/>
CCl	CCl	k1gFnSc1	CCl
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgMnPc2d3	nejstarší
a	a	k8xC	a
nejznámějších	známý	k2eAgMnPc2d3	nejznámější
insekticidů	insekticid	k1gInPc2	insekticid
<g/>
.	.	kIx.	.
</s>
<s>
DDT	DDT	kA	DDT
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
syntetizován	syntetizovat	k5eAaImNgInS	syntetizovat
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1874	[number]	k4	1874
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
insekticidní	insekticidní	k2eAgInPc1d1	insekticidní
účinky	účinek	k1gInPc1	účinek
však	však	k9	však
objevil	objevit	k5eAaPmAgMnS	objevit
až	až	k9	až
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
chemik	chemik	k1gMnSc1	chemik
Paul	Paul	k1gMnSc1	Paul
Hermann	Hermann	k1gMnSc1	Hermann
Müller	Müller	k1gMnSc1	Müller
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
DDT	DDT	kA	DDT
používáno	používat	k5eAaImNgNnS	používat
v	v	k7c6	v
masovém	masový	k2eAgNnSc6d1	masové
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sloužilo	sloužit	k5eAaImAgNnS	sloužit
jako	jako	k9	jako
pro	pro	k7c4	pro
přípravek	přípravek	k1gInSc4	přípravek
k	k	k7c3	k
hubení	hubení	k1gNnSc3	hubení
škodlivého	škodlivý	k2eAgInSc2d1	škodlivý
hmyzu	hmyz	k1gInSc2	hmyz
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
k	k	k7c3	k
likvidaci	likvidace	k1gFnSc3	likvidace
komárů	komár	k1gMnPc2	komár
a	a	k8xC	a
moskytů	moskyt	k1gMnPc2	moskyt
v	v	k7c6	v
tropických	tropický	k2eAgFnPc6d1	tropická
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
perzistentní	perzistentní	k2eAgFnSc4d1	perzistentní
látku	látka	k1gFnSc4	látka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
hromadí	hromadit	k5eAaImIp3nS	hromadit
v	v	k7c6	v
tělech	tělo	k1gNnPc6	tělo
živočichů	živočich	k1gMnPc2	živočich
i	i	k8xC	i
člověka	člověk	k1gMnSc2	člověk
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
bioakumulativní	bioakumulativní	k2eAgMnSc1d1	bioakumulativní
<g/>
)	)	kIx)	)
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
v	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
dávkách	dávka	k1gFnPc6	dávka
nepříznivé	příznivý	k2eNgInPc1d1	nepříznivý
účinky	účinek	k1gInPc1	účinek
na	na	k7c4	na
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc1	jeho
výroba	výroba	k1gFnSc1	výroba
a	a	k8xC	a
použití	použití	k1gNnSc1	použití
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
zakázáno	zakázán	k2eAgNnSc1d1	zakázáno
a	a	k8xC	a
celosvětově	celosvětově	k6eAd1	celosvětově
je	být	k5eAaImIp3nS	být
použití	použití	k1gNnSc1	použití
omezeno	omezit	k5eAaPmNgNnS	omezit
Stockholmskou	stockholmský	k2eAgFnSc7d1	Stockholmská
úmluvou	úmluva	k1gFnSc7	úmluva
o	o	k7c6	o
perzistentních	perzistentní	k2eAgFnPc6d1	perzistentní
organických	organický	k2eAgFnPc6d1	organická
látkách	látka	k1gFnPc6	látka
<g/>
.	.	kIx.	.
</s>
<s>
Polymer	polymer	k1gInSc1	polymer
polyvinylchlorid	polyvinylchlorid	k1gInSc1	polyvinylchlorid
<g/>
,	,	kIx,	,
PVC	PVC	kA	PVC
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
dodnes	dodnes	k6eAd1	dodnes
k	k	k7c3	k
nejrozšířenějším	rozšířený	k2eAgFnPc3d3	nejrozšířenější
průmyslově	průmyslově	k6eAd1	průmyslově
vyráběným	vyráběný	k2eAgFnPc3d1	vyráběná
plastickým	plastický	k2eAgFnPc3d1	plastická
hmotám	hmota	k1gFnPc3	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
polymerací	polymerace	k1gFnSc7	polymerace
nenasyceného	nasycený	k2eNgInSc2d1	nenasycený
chlorovaného	chlorovaný	k2eAgInSc2d1	chlorovaný
uhlovodíku	uhlovodík	k1gInSc2	uhlovodík
vinylchloridu	vinylchlorid	k1gInSc2	vinylchlorid
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
H	H	kA	H
<g/>
3	[number]	k4	3
<g/>
Cl	Cl	k1gFnPc1	Cl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
produkt	produkt	k1gInSc1	produkt
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
barví	barvit	k5eAaImIp3nS	barvit
a	a	k8xC	a
plní	plnit	k5eAaImIp3nS	plnit
různými	různý	k2eAgNnPc7d1	různé
aditivy	aditivum	k1gNnPc7	aditivum
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
na	na	k7c4	na
trh	trh	k1gInSc4	trh
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
velmi	velmi	k6eAd1	velmi
široká	široký	k2eAgFnSc1d1	široká
škála	škála	k1gFnSc1	škála
plastů	plast	k1gInPc2	plast
s	s	k7c7	s
rozdílnými	rozdílný	k2eAgInPc7d1	rozdílný
fyzikálním	fyzikální	k2eAgFnPc3d1	fyzikální
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
vyráběné	vyráběný	k2eAgInPc1d1	vyráběný
druhy	druh	k1gInPc1	druh
PVC	PVC	kA	PVC
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
dobrou	dobrý	k2eAgFnSc7d1	dobrá
chemickou	chemický	k2eAgFnSc7d1	chemická
odolností	odolnost	k1gFnSc7	odolnost
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
špatně	špatně	k6eAd1	špatně
hořlavé	hořlavý	k2eAgInPc4d1	hořlavý
a	a	k8xC	a
tepelně	tepelně	k6eAd1	tepelně
dobře	dobře	k6eAd1	dobře
odolné	odolný	k2eAgFnPc1d1	odolná
<g/>
.	.	kIx.	.
</s>
<s>
Vnášením	vnášení	k1gNnSc7	vnášení
různých	různý	k2eAgInPc2d1	různý
pigmentů	pigment	k1gInPc2	pigment
do	do	k7c2	do
hmoty	hmota	k1gFnSc2	hmota
plastu	plast	k1gInSc2	plast
lze	lze	k6eAd1	lze
připravit	připravit	k5eAaPmF	připravit
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
paletu	paleta	k1gFnSc4	paleta
barevných	barevný	k2eAgInPc2d1	barevný
odstínů	odstín	k1gInPc2	odstín
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
plastu	plast	k1gInSc2	plast
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
snadno	snadno	k6eAd1	snadno
omyvatelný	omyvatelný	k2eAgInSc4d1	omyvatelný
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
důvodů	důvod	k1gInPc2	důvod
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
hojně	hojně	k6eAd1	hojně
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
podlahových	podlahový	k2eAgFnPc2d1	podlahová
krytin	krytina	k1gFnPc2	krytina
a	a	k8xC	a
bytových	bytový	k2eAgFnPc2d1	bytová
textilií	textilie	k1gFnPc2	textilie
<g/>
.	.	kIx.	.
</s>
<s>
Povrchy	povrch	k1gInPc1	povrch
domácích	domácí	k2eAgInPc2d1	domácí
zahradních	zahradní	k2eAgInPc2d1	zahradní
bazénů	bazén	k1gInPc2	bazén
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
pokryty	pokrýt	k5eAaPmNgInP	pokrýt
vrstvou	vrstva	k1gFnSc7	vrstva
PVC	PVC	kA	PVC
<g/>
,	,	kIx,	,
z	z	k7c2	z
PVC	PVC	kA	PVC
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
armatury	armatura	k1gFnPc1	armatura
pro	pro	k7c4	pro
rozvody	rozvod	k1gInPc4	rozvod
pitných	pitný	k2eAgFnPc2d1	pitná
i	i	k8xC	i
odpadních	odpadní	k2eAgFnPc2d1	odpadní
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc4d1	další
využití	využití	k1gNnSc4	využití
nalézá	nalézat	k5eAaImIp3nS	nalézat
PVC	PVC	kA	PVC
v	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
jako	jako	k8xS	jako
inertní	inertní	k2eAgFnSc1d1	inertní
součást	součást	k1gFnSc1	součást
různých	různý	k2eAgInPc2d1	různý
aparátů	aparát	k1gInPc2	aparát
(	(	kIx(	(
<g/>
hadičky	hadička	k1gFnPc1	hadička
<g/>
,	,	kIx,	,
trubičky	trubička	k1gFnPc1	trubička
<g/>
,	,	kIx,	,
zásobníky	zásobník	k1gInPc1	zásobník
tekutin	tekutina	k1gFnPc2	tekutina
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
např.	např.	kA	např.
vyráběny	vyrábět	k5eAaImNgInP	vyrábět
i	i	k8xC	i
hračky	hračka	k1gFnPc1	hračka
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
s	s	k7c7	s
PVC	PVC	kA	PVC
nastává	nastávat	k5eAaImIp3nS	nastávat
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
jeho	on	k3xPp3gNnSc2	on
hoření	hoření	k1gNnSc2	hoření
nebo	nebo	k8xC	nebo
tepelného	tepelný	k2eAgInSc2d1	tepelný
rozkladu	rozklad	k1gInSc2	rozklad
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
okolností	okolnost	k1gFnPc2	okolnost
téměř	téměř	k6eAd1	téměř
nehořlavé	hořlavý	k2eNgFnPc1d1	nehořlavá
<g/>
,	,	kIx,	,
za	za	k7c2	za
vysokých	vysoký	k2eAgFnPc2d1	vysoká
teplot	teplota	k1gFnPc2	teplota
(	(	kIx(	(
<g/>
např.	např.	kA	např.
při	při	k7c6	při
požáru	požár	k1gInSc6	požár
bytu	byt	k1gInSc2	byt
<g/>
)	)	kIx)	)
začíná	začínat	k5eAaImIp3nS	začínat
tepelně	tepelně	k6eAd1	tepelně
degradovat	degradovat	k5eAaBmF	degradovat
za	za	k7c4	za
uvolňování	uvolňování	k1gNnSc4	uvolňování
širokého	široký	k2eAgNnSc2d1	široké
spektra	spektrum	k1gNnSc2	spektrum
vesměs	vesměs	k6eAd1	vesměs
velmi	velmi	k6eAd1	velmi
toxických	toxický	k2eAgFnPc2d1	toxická
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
(	(	kIx(	(
<g/>
nejznámější	známý	k2eAgFnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
patrně	patrně	k6eAd1	patrně
fosgen	fosgen	k1gInSc4	fosgen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
při	při	k7c6	při
řízeném	řízený	k2eAgNnSc6d1	řízené
spalování	spalování	k1gNnSc6	spalování
se	se	k3xPyFc4	se
do	do	k7c2	do
plynných	plynný	k2eAgFnPc2d1	plynná
spalin	spaliny	k1gFnPc2	spaliny
dostávají	dostávat	k5eAaImIp3nP	dostávat
atomy	atom	k1gInPc1	atom
chloru	chlor	k1gInSc2	chlor
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
působí	působit	k5eAaImIp3nP	působit
silně	silně	k6eAd1	silně
korozivně	korozivně	k6eAd1	korozivně
na	na	k7c4	na
spalovací	spalovací	k2eAgFnPc4d1	spalovací
aparatury	aparatura	k1gFnPc4	aparatura
díky	díky	k7c3	díky
vznikajícímu	vznikající	k2eAgInSc3d1	vznikající
chlorovodíku	chlorovodík	k1gInSc3	chlorovodík
<g/>
.	.	kIx.	.
</s>
<s>
Reakcí	reakce	k1gFnPc2	reakce
chloru	chlor	k1gInSc2	chlor
s	s	k7c7	s
aromatickými	aromatický	k2eAgInPc7d1	aromatický
uhlovodíky	uhlovodík	k1gInPc7	uhlovodík
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
zvláště	zvláště	k6eAd1	zvláště
nebezpečných	bezpečný	k2eNgInPc2d1	nebezpečný
dioxinů	dioxin	k1gInPc2	dioxin
<g/>
.	.	kIx.	.
</s>
<s>
Dioxiny	dioxin	k1gInPc1	dioxin
představují	představovat	k5eAaImIp3nP	představovat
ekologicky	ekologicky	k6eAd1	ekologicky
nejproblematičtější	problematický	k2eAgFnPc1d3	nejproblematičtější
organické	organický	k2eAgFnPc1d1	organická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
chloru	chlor	k1gInSc2	chlor
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
termínem	termín	k1gInSc7	termín
jsou	být	k5eAaImIp3nP	být
označovány	označován	k2eAgFnPc1d1	označována
dvě	dva	k4xCgFnPc1	dva
skupiny	skupina	k1gFnPc1	skupina
látek	látka	k1gFnPc2	látka
<g/>
:	:	kIx,	:
Polychlorované	polychlorovaný	k2eAgInPc1d1	polychlorovaný
dibenzo-para-dioxiny	dibenzoaraioxin	k1gInPc1	dibenzo-para-dioxin
a	a	k8xC	a
dibenzofurany	dibenzofuran	k1gInPc1	dibenzofuran
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
základní	základní	k2eAgInSc4d1	základní
skelet	skelet	k1gInSc4	skelet
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
připojené	připojený	k2eAgInPc1d1	připojený
obrázky	obrázek	k1gInPc1	obrázek
<g/>
.	.	kIx.	.
</s>
<s>
Záměnou	záměna	k1gFnSc7	záměna
jednoho	jeden	k4xCgInSc2	jeden
nebo	nebo	k8xC	nebo
několika	několik	k4yIc2	několik
vodíkových	vodíkový	k2eAgInPc2d1	vodíkový
atomů	atom	k1gInPc2	atom
za	za	k7c7	za
atomy	atom	k1gInPc7	atom
chloru	chlor	k1gInSc2	chlor
vznikají	vznikat	k5eAaImIp3nP	vznikat
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
označovány	označovat	k5eAaImNgInP	označovat
souhrnným	souhrnný	k2eAgInSc7d1	souhrnný
názvem	název	k1gInSc7	název
dioxiny	dioxin	k1gInPc4	dioxin
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
látky	látka	k1gFnPc1	látka
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
jak	jak	k6eAd1	jak
značnou	značný	k2eAgFnSc7d1	značná
přímou	přímý	k2eAgFnSc7d1	přímá
toxicitou	toxicita	k1gFnSc7	toxicita
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
i	i	k9	i
silně	silně	k6eAd1	silně
mutagenní	mutagenní	k2eAgNnPc1d1	mutagenní
i	i	k8xC	i
karcinogenní	karcinogenní	k2eAgNnPc1d1	karcinogenní
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
sloučeniny	sloučenina	k1gFnPc1	sloučenina
se	se	k3xPyFc4	se
průmyslově	průmyslově	k6eAd1	průmyslově
nevyrábějí	vyrábět	k5eNaImIp3nP	vyrábět
<g/>
,	,	kIx,	,
vznikají	vznikat	k5eAaImIp3nP	vznikat
jako	jako	k9	jako
nechtěné	chtěný	k2eNgFnPc1d1	nechtěná
příměsi	příměs	k1gFnPc1	příměs
při	při	k7c6	při
chemické	chemický	k2eAgFnSc6d1	chemická
syntéze	syntéza	k1gFnSc6	syntéza
jiných	jiný	k2eAgFnPc2d1	jiná
aromatických	aromatický	k2eAgFnPc2d1	aromatická
chlorovaných	chlorovaný	k2eAgFnPc2d1	chlorovaná
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
uváděným	uváděný	k2eAgInSc7d1	uváděný
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
defoliant	defoliant	k1gMnSc1	defoliant
Agent	agent	k1gMnSc1	agent
Orange	Orange	k1gInSc4	Orange
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
Vietnamské	vietnamský	k2eAgFnSc6d1	vietnamská
válce	válka	k1gFnSc6	válka
masově	masově	k6eAd1	masově
rozprašován	rozprašovat	k5eAaImNgMnS	rozprašovat
americkou	americký	k2eAgFnSc7d1	americká
armádou	armáda	k1gFnSc7	armáda
na	na	k7c4	na
tropické	tropický	k2eAgInPc4d1	tropický
pralesy	prales	k1gInPc4	prales
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zbavit	zbavit	k5eAaPmF	zbavit
vegetaci	vegetace	k1gFnSc3	vegetace
listů	list	k1gInPc2	list
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
dioxiny	dioxin	k1gInPc1	dioxin
nejsou	být	k5eNaImIp3nP	být
originálně	originálně	k6eAd1	originálně
složkou	složka	k1gFnSc7	složka
této	tento	k3xDgFnSc2	tento
směsi	směs	k1gFnSc2	směs
<g/>
,	,	kIx,	,
vyskytovaly	vyskytovat	k5eAaImAgInP	vyskytovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
v	v	k7c6	v
relativně	relativně	k6eAd1	relativně
značném	značný	k2eAgNnSc6d1	značné
množství	množství	k1gNnSc6	množství
díky	díky	k7c3	díky
nedokonalému	dokonalý	k2eNgNnSc3d1	nedokonalé
čištění	čištění	k1gNnSc3	čištění
základních	základní	k2eAgFnPc2d1	základní
složek	složka	k1gFnPc2	složka
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
půda	půda	k1gFnSc1	půda
natolik	natolik	k6eAd1	natolik
zatížena	zatížit	k5eAaPmNgFnS	zatížit
těmito	tento	k3xDgFnPc7	tento
látkami	látka	k1gFnPc7	látka
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
nelze	lze	k6eNd1	lze
zemědělsky	zemědělsky	k6eAd1	zemědělsky
využívat	využívat	k5eAaPmF	využívat
<g/>
.	.	kIx.	.
</s>
<s>
Daleko	daleko	k6eAd1	daleko
tragičtější	tragický	k2eAgFnPc1d2	tragičtější
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
tisíce	tisíc	k4xCgInPc4	tisíc
případů	případ	k1gInPc2	případ
mrtvě	mrtvě	k6eAd1	mrtvě
narozených	narozený	k2eAgFnPc2d1	narozená
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
dětí	dítě	k1gFnPc2	dítě
narozených	narozený	k2eAgFnPc2d1	narozená
s	s	k7c7	s
hrůznými	hrůzný	k2eAgFnPc7d1	hrůzná
deformitami	deformita	k1gFnPc7	deformita
<g/>
,	,	kIx,	,
extrémní	extrémní	k2eAgInSc1d1	extrémní
nárůst	nárůst	k1gInSc1	nárůst
případů	případ	k1gInPc2	případ
onemocnění	onemocnění	k1gNnPc2	onemocnění
rakovinou	rakovina	k1gFnSc7	rakovina
v	v	k7c6	v
postižených	postižený	k2eAgFnPc6d1	postižená
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Problémy	problém	k1gInPc1	problém
se	se	k3xPyFc4	se
nevyhnuly	vyhnout	k5eNaPmAgInP	vyhnout
ani	ani	k8xC	ani
americkým	americký	k2eAgMnPc3d1	americký
vojákům	voják	k1gMnPc3	voják
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
veteránů	veterán	k1gMnPc2	veterán
Vietnamské	vietnamský	k2eAgFnSc2d1	vietnamská
války	válka	k1gFnSc2	válka
vykazovala	vykazovat	k5eAaImAgFnS	vykazovat
podobné	podobný	k2eAgInPc4d1	podobný
syndromy	syndrom	k1gInPc4	syndrom
jako	jako	k8xS	jako
vietnamští	vietnamský	k2eAgMnPc1d1	vietnamský
domorodci	domorodec	k1gMnPc1	domorodec
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
je	být	k5eAaImIp3nS	být
dáváno	dávat	k5eAaImNgNnS	dávat
do	do	k7c2	do
souvislosti	souvislost	k1gFnSc2	souvislost
s	s	k7c7	s
dlouhodobým	dlouhodobý	k2eAgInSc7d1	dlouhodobý
kontaktem	kontakt	k1gInSc7	kontakt
postižených	postižený	k2eAgFnPc2d1	postižená
osob	osoba	k1gFnPc2	osoba
s	s	k7c7	s
dioxiny	dioxin	k1gInPc7	dioxin
obsaženými	obsažený	k2eAgInPc7d1	obsažený
v	v	k7c6	v
Agent	agent	k1gMnSc1	agent
Orange	Orang	k1gMnSc4	Orang
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
složek	složka	k1gFnPc2	složka
Agent	agent	k1gMnSc1	agent
Orange	Orang	k1gFnSc2	Orang
(	(	kIx(	(
<g/>
2,4	[number]	k4	2,4
<g/>
,5	,5	k4	,5
<g/>
-T	-T	k?	-T
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
i	i	k9	i
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
v	v	k7c6	v
chemičce	chemička	k1gFnSc6	chemička
Spolana	Spolana	k1gFnSc1	Spolana
Neratovice	Neratovice	k1gFnSc1	Neratovice
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
krátké	krátký	k2eAgFnSc2d1	krátká
doby	doba	k1gFnSc2	doba
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
kontaminace	kontaminace	k1gFnSc1	kontaminace
dioxiny	dioxin	k1gInPc4	dioxin
vážně	vážně	k6eAd1	vážně
onemocněly	onemocnět	k5eAaPmAgFnP	onemocnět
desítky	desítka	k1gFnPc1	desítka
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
Spolany	Spolana	k1gFnSc2	Spolana
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
podíleli	podílet	k5eAaImAgMnP	podílet
na	na	k7c6	na
výrobě	výroba	k1gFnSc6	výroba
pesticidu	pesticid	k1gInSc2	pesticid
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodně	rozhodně	k6eAd1	rozhodně
nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
zdrojem	zdroj	k1gInSc7	zdroj
dioxinů	dioxin	k1gInPc2	dioxin
v	v	k7c6	v
životním	životní	k2eAgNnSc6d1	životní
prostředí	prostředí	k1gNnSc6	prostředí
je	být	k5eAaImIp3nS	být
spalování	spalování	k1gNnSc1	spalování
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
komunálního	komunální	k2eAgInSc2d1	komunální
odpadu	odpad	k1gInSc2	odpad
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
především	především	k9	především
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
aromatické	aromatický	k2eAgNnSc1d1	aromatické
benzenové	benzenový	k2eAgNnSc1d1	benzenové
jádro	jádro	k1gNnSc1	jádro
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
stálé	stálý	k2eAgNnSc1d1	stálé
a	a	k8xC	a
při	při	k7c6	při
spalování	spalování	k1gNnSc6	spalování
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
přednostní	přednostní	k2eAgFnSc3d1	přednostní
oxidaci	oxidace	k1gFnSc3	oxidace
běžných	běžný	k2eAgInPc2d1	běžný
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
<g/>
.	.	kIx.	.
</s>
<s>
Benzenový	benzenový	k2eAgInSc1d1	benzenový
skelet	skelet	k1gInSc1	skelet
přitom	přitom	k6eAd1	přitom
často	často	k6eAd1	často
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
nedotčen	dotčen	k2eNgInSc1d1	nedotčen
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc2	jeho
reakcí	reakce	k1gFnPc2	reakce
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
a	a	k8xC	a
chlorem	chlor	k1gInSc7	chlor
vznikají	vznikat	k5eAaImIp3nP	vznikat
dioxiny	dioxin	k1gInPc4	dioxin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
spalovny	spalovna	k1gFnPc1	spalovna
komunálního	komunální	k2eAgInSc2d1	komunální
odpadu	odpad	k1gInSc2	odpad
konstruovány	konstruovat	k5eAaImNgInP	konstruovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
v	v	k7c6	v
plynných	plynný	k2eAgFnPc6d1	plynná
emisích	emise	k1gFnPc6	emise
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
destrukci	destrukce	k1gFnSc3	destrukce
valné	valný	k2eAgFnSc2d1	valná
většiny	většina	k1gFnSc2	většina
vznikajících	vznikající	k2eAgInPc2d1	vznikající
dioxinů	dioxin	k1gInPc2	dioxin
<g/>
.	.	kIx.	.
</s>
<s>
Dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
se	se	k3xPyFc4	se
toho	ten	k3xDgNnSc2	ten
převážně	převážně	k6eAd1	převážně
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
spaliny	spaliny	k1gFnPc1	spaliny
z	z	k7c2	z
primárního	primární	k2eAgNnSc2d1	primární
spalování	spalování	k1gNnSc2	spalování
odpadu	odpad	k1gInSc2	odpad
vstupují	vstupovat	k5eAaImIp3nP	vstupovat
do	do	k7c2	do
druhého	druhý	k4xOgInSc2	druhý
spalovacího	spalovací	k2eAgInSc2d1	spalovací
stupně	stupeň	k1gInSc2	stupeň
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
zvýšena	zvýšit	k5eAaPmNgFnS	zvýšit
teplota	teplota	k1gFnSc1	teplota
<g/>
'	'	kIx"	'
na	na	k7c4	na
1	[number]	k4	1
500	[number]	k4	500
°	°	k?	°
<g/>
C	C	kA	C
i	i	k9	i
více	hodně	k6eAd2	hodně
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Kombinací	kombinace	k1gFnSc7	kombinace
vysoké	vysoký	k2eAgFnSc2d1	vysoká
teploty	teplota	k1gFnSc2	teplota
spalování	spalování	k1gNnSc2	spalování
a	a	k8xC	a
prodloužené	prodloužený	k2eAgFnSc2d1	prodloužená
doby	doba	k1gFnSc2	doba
pobytu	pobyt	k1gInSc2	pobyt
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
o	o	k7c6	o
extrémně	extrémně	k6eAd1	extrémně
vysoké	vysoký	k2eAgFnSc6d1	vysoká
teplotě	teplota	k1gFnSc6	teplota
lze	lze	k6eAd1	lze
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
prakticky	prakticky	k6eAd1	prakticky
kompletní	kompletní	k2eAgFnSc1d1	kompletní
oxidace	oxidace	k1gFnSc1	oxidace
přítomných	přítomný	k2eAgInPc2d1	přítomný
dioxinů	dioxin	k1gInPc2	dioxin
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zajímavost	zajímavost	k1gFnSc4	zajímavost
lze	lze	k6eAd1	lze
uvést	uvést	k5eAaPmF	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
pražská	pražský	k2eAgFnSc1d1	Pražská
spalovna	spalovna	k1gFnSc1	spalovna
komunálního	komunální	k2eAgInSc2d1	komunální
odpadu	odpad	k1gInSc2	odpad
je	být	k5eAaImIp3nS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
ještě	ještě	k9	ještě
dalším	další	k2eAgInSc7d1	další
<g/>
,	,	kIx,	,
třetím	třetí	k4xOgInSc7	třetí
spalovacím	spalovací	k2eAgInSc7d1	spalovací
stupněm	stupeň	k1gInSc7	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
spalovny	spalovna	k1gFnPc1	spalovna
jsou	být	k5eAaImIp3nP	být
navíc	navíc	k6eAd1	navíc
ze	z	k7c2	z
zákona	zákon	k1gInSc2	zákon
pravidelně	pravidelně	k6eAd1	pravidelně
kontrolovány	kontrolován	k2eAgFnPc1d1	kontrolována
a	a	k8xC	a
obsah	obsah	k1gInSc1	obsah
dioxinů	dioxin	k1gInPc2	dioxin
ve	v	k7c6	v
spalinách	spaliny	k1gFnPc6	spaliny
je	být	k5eAaImIp3nS	být
monitorován	monitorován	k2eAgInSc1d1	monitorován
<g/>
.	.	kIx.	.
</s>
<s>
Měření	měření	k1gNnPc1	měření
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
prováděna	provádět	k5eAaImNgFnS	provádět
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
18	[number]	k4	18
hodin	hodina	k1gFnPc2	hodina
dvakrát	dvakrát	k6eAd1	dvakrát
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Dioxiny	dioxin	k1gInPc1	dioxin
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
velkým	velký	k2eAgInSc7d1	velký
problémem	problém	k1gInSc7	problém
v	v	k7c6	v
popílcích	popílek	k1gInPc6	popílek
a	a	k8xC	a
dalších	další	k2eAgInPc6d1	další
odpadech	odpad	k1gInPc6	odpad
z	z	k7c2	z
čištění	čištění	k1gNnSc2	čištění
spalin	spaliny	k1gFnPc2	spaliny
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vliv	vliv	k1gInSc1	vliv
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
kongenerů	kongener	k1gInPc2	kongener
dioxinů	dioxin	k1gInPc2	dioxin
na	na	k7c4	na
živé	živý	k2eAgInPc4d1	živý
organizmy	organizmus	k1gInPc4	organizmus
se	se	k3xPyFc4	se
podstatně	podstatně	k6eAd1	podstatně
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zjednodušení	zjednodušení	k1gNnSc4	zjednodušení
byl	být	k5eAaImAgMnS	být
vybrán	vybrat	k5eAaPmNgMnS	vybrat
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
-	-	kIx~	-
2,3	[number]	k4	2,3
<g/>
,7	,7	k4	,7
<g/>
,8	,8	k4	,8
<g/>
-tetrachlorodibenzen-p-dioxin	etrachlorodibenzenioxina	k1gFnPc2	-tetrachlorodibenzen-p-dioxina
(	(	kIx(	(
<g/>
obrázek	obrázek	k1gInSc1	obrázek
<g/>
)	)	kIx)	)
a	a	k8xC	a
při	při	k7c6	při
uvádění	uvádění	k1gNnSc6	uvádění
koncentrace	koncentrace	k1gFnSc2	koncentrace
dioxinů	dioxin	k1gInPc2	dioxin
v	v	k7c6	v
analytickém	analytický	k2eAgInSc6d1	analytický
vzorku	vzorek	k1gInSc6	vzorek
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
obsahy	obsah	k1gInPc1	obsah
všech	všecek	k3xTgInPc2	všecek
přítomných	přítomný	k2eAgInPc2d1	přítomný
dioxinů	dioxin	k1gInPc2	dioxin
vztahovány	vztahován	k2eAgInPc4d1	vztahován
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
sloučenině	sloučenina	k1gFnSc3	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
<g/>
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
<g/>
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s>
<g/>
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1961	[number]	k4	1961
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwooda	k1gFnPc2	Greenwooda
-	-	kIx~	-
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc1	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
chlor	chlor	k1gInSc1	chlor
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
chlor	chlor	k1gInSc1	chlor
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Chemický	chemický	k2eAgInSc1d1	chemický
vzdělávací	vzdělávací	k2eAgInSc1d1	vzdělávací
portál	portál	k1gInSc1	portál
Miroslav	Miroslav	k1gMnSc1	Miroslav
Šuta	šuta	k1gFnSc1	šuta
<g/>
:	:	kIx,	:
Chemické	chemický	k2eAgFnPc1d1	chemická
látky	látka	k1gFnPc1	látka
v	v	k7c6	v
životním	životní	k2eAgNnSc6d1	životní
prostředí	prostředí	k1gNnSc6	prostředí
a	a	k8xC	a
zdraví	zdraví	k1gNnSc6	zdraví
<g/>
,	,	kIx,	,
Ekologický	ekologický	k2eAgInSc1d1	ekologický
institut	institut	k1gInSc1	institut
Veronica	Veronica	k1gFnSc1	Veronica
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-87308-00-4	[number]	k4	978-80-87308-00-4
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Chlorine	Chlorin	k1gInSc5	Chlorin
-	-	kIx~	-
National	National	k1gMnSc1	National
Institute	institut	k1gInSc5	institut
for	forum	k1gNnPc2	forum
Occupational	Occupational	k1gFnPc7	Occupational
Safety	Safeta	k1gFnSc2	Safeta
and	and	k?	and
Health	Health	k1gMnSc1	Health
Agency	Agenca	k1gFnSc2	Agenca
for	forum	k1gNnPc2	forum
Toxic	Toxic	k1gMnSc1	Toxic
Substances	Substances	k1gMnSc1	Substances
and	and	k?	and
Disease	Diseas	k1gInSc6	Diseas
Registry	registr	k1gInPc1	registr
<g/>
:	:	kIx,	:
Chlorine	Chlorin	k1gInSc5	Chlorin
Electrolytic	Electrolytice	k1gFnPc2	Electrolytice
production	production	k1gInSc1	production
Production	Production	k1gInSc1	Production
and	and	k?	and
liquefaction	liquefaction	k1gInSc1	liquefaction
of	of	k?	of
chlorine	chlorin	k1gInSc5	chlorin
Chlorine	Chlorin	k1gInSc5	Chlorin
Production	Production	k1gInSc1	Production
Using	Using	k1gInSc4	Using
Mercury	Mercura	k1gFnSc2	Mercura
<g/>
,	,	kIx,	,
Environmental	Environmental	k1gMnSc1	Environmental
Considerations	Considerationsa	k1gFnPc2	Considerationsa
and	and	k?	and
Alternatives	Alternatives	k1gMnSc1	Alternatives
</s>
