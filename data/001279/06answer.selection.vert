<s>
Chlor	chlor	k1gInSc1	chlor
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Cl	Cl	k1gFnSc2	Cl
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
chlorum	chlorum	k1gNnSc1	chlorum
(	(	kIx(	(
<g/>
starořecky	starořecky	k6eAd1	starořecky
χ	χ	k?	χ
<g/>
,	,	kIx,	,
chlóros	chlórosa	k1gFnPc2	chlórosa
-	-	kIx~	-
"	"	kIx"	"
<g/>
zelený	zelený	k2eAgInSc1d1	zelený
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
toxický	toxický	k2eAgInSc1d1	toxický
<g/>
,	,	kIx,	,
světle	světle	k6eAd1	světle
zelený	zelený	k2eAgInSc1d1	zelený
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc1	druhý
člen	člen	k1gInSc1	člen
řady	řada	k1gFnSc2	řada
halogenů	halogen	k1gInPc2	halogen
<g/>
.	.	kIx.	.
</s>
