<s>
Jaký	jaký	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
název	název	k1gInSc1
nese	nést	k5eAaImIp3nS
plně	plně	k6eAd1
autonomní	autonomní	k2eAgInSc1d1
vrtulník	vrtulník	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
provedl	provést	k5eAaPmAgInS
první	první	k4xOgInPc1
let	léto	k1gNnPc2
na	na	k7c6
cizím	cizí	k2eAgNnSc6d1
vesmírném	vesmírný	k2eAgNnSc6d1
tělese	těleso	k1gNnSc6
<g/>
?	?	kIx.
</s>