<s>
První	první	k4xOgInSc1
let	let	k1gInSc1
se	se	k3xPyFc4
uskutečnil	uskutečnit	k5eAaPmAgInS
19	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2021	#num#	k4
od	od	k7c2
12	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
vědci	vědec	k1gMnPc1
začali	začít	k5eAaPmAgMnP
přijímat	přijímat	k5eAaImF
první	první	k4xOgNnPc1
data	datum	k1gNnPc1
od	od	k7c2
vrtulníku	vrtulník	k1gInSc2
Ingenuity	Ingenuita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyrobila	vyrobit	k5eAaPmAgFnS
jej	on	k3xPp3gInSc4
společnost	společnost	k1gFnSc1
Jet	jet	k5eAaImF
Propulsion	Propulsion	k1gInSc4
Laboratory	Laboratory	k1gInSc1
pro	pro	k7c4
NASA	NASA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Jde	jít	k5eAaImIp3nS
o	o	k7c4
první	první	k4xOgInSc4
létající	létající	k2eAgInSc4d1
stroj	stroj	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
bude	být	k5eAaImBp3nS
létat	létat	k5eAaImF
na	na	k7c6
jiné	jiný	k2eAgFnSc6d1
planetě	planeta	k1gFnSc6
<g/>
,	,	kIx,
než	než	k8xS
je	být	k5eAaImIp3nS
Země	země	k1gFnSc1
<g/>
.	.	kIx.
</s>