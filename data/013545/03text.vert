<s>
Ingenuity	Ingenuita	k1gFnPc1
(	(	kIx(
<g/>
vrtulník	vrtulník	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
probíhající	probíhající	k2eAgFnSc7d1
vesmírnou	vesmírný	k2eAgFnSc7d1
misí	mise	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Informace	informace	k1gFnPc1
zde	zde	k6eAd1
uvedené	uvedený	k2eAgInPc4d1
se	s	k7c7
vzhledem	vzhled	k1gInSc7
k	k	k7c3
neustálému	neustálý	k2eAgInSc3d1
vývoji	vývoj	k1gInSc3
mohou	moct	k5eAaImIp3nP
průběžně	průběžně	k6eAd1
měnit	měnit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
je	být	k5eAaImIp3nS
se	s	k7c7
zvýšenou	zvýšený	k2eAgFnSc7d1
péčí	péče	k1gFnSc7
aktualizovat	aktualizovat	k5eAaBmF
a	a	k8xC
doplňovat	doplňovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Ingenuity	Ingenuit	k2eAgInPc1d1
Vrtulník	vrtulník	k1gInSc1
Ingenuity	Ingenuita	k1gFnSc2
na	na	k7c6
povrchu	povrch	k1gInSc6
Marsu	Mars	k1gInSc2
(	(	kIx(
<g/>
umělecká	umělecký	k2eAgFnSc1d1
představa	představa	k1gFnSc1
<g/>
)	)	kIx)
Provozovatel	provozovatel	k1gMnSc1
</s>
<s>
NASA	NASA	kA
Výrobce	výrobce	k1gMnSc1
</s>
<s>
Jet	jet	k2eAgInSc1d1
Propulsion	Propulsion	k1gInSc1
Laboratory	Laborator	k1gInPc1
Program	program	k1gInSc1
</s>
<s>
Mars	Mars	k1gInSc1
2020	#num#	k4
Hmotnost	hmotnost	k1gFnSc4
</s>
<s>
1,8	1,8	k4
kg	kg	kA
Délka	délka	k1gFnSc1
</s>
<s>
14	#num#	k4
cm	cm	kA
Šířka	šířka	k1gFnSc1
</s>
<s>
14	#num#	k4
cm	cm	kA
Výška	výška	k1gFnSc1
</s>
<s>
0,49	0,49	k4
m	m	kA
Teleskop	teleskop	k1gInSc1
Průměr	průměr	k1gInSc1
</s>
<s>
1,2	1,2	k4
m	m	kA
Přístroje	přístroj	k1gInSc2
Nese	nést	k5eAaImIp3nS
přístroje	přístroj	k1gInPc4
</s>
<s>
inerciální	inerciální	k2eAgFnSc1d1
měřící	měřící	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
<g/>
,	,	kIx,
lidar	lidar	k1gInSc1
a	a	k8xC
NavCam	NavCam	k1gInSc1
Oficiální	oficiální	k2eAgInSc4d1
web	web	k1gInSc4
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
vrtulník	vrtulník	k1gInSc1
Ingenuity	Ingenuita	k1gFnSc2
</s>
<s>
Ingenuity	Ingenuity	k1gFnPc1
je	být	k5eAaImIp3nS
malý	malý	k2eAgInSc1d1
plně	plně	k6eAd1
autonomní	autonomní	k2eAgInSc1d1
vrtulník	vrtulník	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
rámci	rámec	k1gInSc6
mise	mise	k1gFnSc1
Mars	Mars	k1gInSc1
2020	#num#	k4
dopraven	dopravna	k1gFnPc2
na	na	k7c4
planetu	planeta	k1gFnSc4
Mars	Mars	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyrobila	vyrobit	k5eAaPmAgFnS
jej	on	k3xPp3gInSc4
společnost	společnost	k1gFnSc1
Jet	jet	k5eAaImF
Propulsion	Propulsion	k1gInSc4
Laboratory	Laboratory	k1gInSc1
pro	pro	k7c4
NASA	NASA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Jde	jít	k5eAaImIp3nS
o	o	k7c4
první	první	k4xOgInSc4
létající	létající	k2eAgInSc4d1
stroj	stroj	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
bude	být	k5eAaImBp3nS
létat	létat	k5eAaImF
na	na	k7c6
jiné	jiný	k2eAgFnSc6d1
planetě	planeta	k1gFnSc6
<g/>
,	,	kIx,
než	než	k8xS
je	být	k5eAaImIp3nS
Země	země	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dron	Dron	k1gInSc1
byl	být	k5eAaImAgInS
zkonstruován	zkonstruovat	k5eAaPmNgInS
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byl	být	k5eAaImAgMnS
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
nejlehčí	lehký	k2eAgMnSc1d3
a	a	k8xC
zároveň	zároveň	k6eAd1
výkonný	výkonný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Problémem	problém	k1gInSc7
bylo	být	k5eAaImAgNnS
zkonstruovat	zkonstruovat	k5eAaPmF
zařízení	zařízení	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
bude	být	k5eAaImBp3nS
moct	moct	k5eAaImF
létat	létat	k5eAaImF
ve	v	k7c6
100	#num#	k4
<g/>
×	×	k?
řidší	řídký	k2eAgFnSc6d2
atmosféře	atmosféra	k1gFnSc6
<g/>
,	,	kIx,
než	než	k8xS
je	být	k5eAaImIp3nS
ta	ten	k3xDgFnSc1
zemská	zemský	k2eAgFnSc1d1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Ingenuity	Ingenuita	k1gFnPc1
pohání	pohánět	k5eAaImIp3nP
dva	dva	k4xCgInPc4
protiběžné	protiběžný	k2eAgInPc4d1
dvoulisté	dvoulistý	k2eAgInPc4d1
rotory	rotor	k1gInPc4
s	s	k7c7
lopatkami	lopatka	k1gFnPc7
z	z	k7c2
uhlíkových	uhlíkový	k2eAgFnPc2d1
vláken	vlákna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Délka	délka	k1gFnSc1
rotorů	rotor	k1gInPc2
je	být	k5eAaImIp3nS
1,2	1,2	k4
metru	metr	k1gInSc2
<g/>
,	,	kIx,
provozní	provozní	k2eAgFnSc2d1
otáčky	otáčka	k1gFnSc2
2400	#num#	k4
ot	ot	k1gMnSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
min	mina	k1gFnPc2
<g/>
.	.	kIx.
Výkon	výkon	k1gInSc1
motoru	motor	k1gInSc2
je	být	k5eAaImIp3nS
350	#num#	k4
W.	W.	kA
Je	být	k5eAaImIp3nS
napájený	napájený	k2eAgInSc1d1
systémem	systém	k1gInSc7
šesti	šest	k4xCc2
lithium-iontových	lithium-iontův	k2eAgFnPc2d1
baterií	baterie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgInPc1
se	se	k3xPyFc4
budou	být	k5eAaImBp3nP
denně	denně	k6eAd1
dobíjet	dobíjet	k5eAaImF
solárními	solární	k2eAgInPc7d1
články	článek	k1gInPc7
na	na	k7c6
povrchu	povrch	k1gInSc6
listů	list	k1gInPc2
rotorů	rotor	k1gInPc2
<g/>
,	,	kIx,
částečně	částečně	k6eAd1
se	se	k3xPyFc4
bude	být	k5eAaImBp3nS
nabíjet	nabíjet	k5eAaImF
i	i	k9
z	z	k7c2
vozítka	vozítko	k1gNnSc2
Perseverance	perseverance	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
vysoký	vysoký	k2eAgMnSc1d1
49	#num#	k4
centimetrů	centimetr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trup	trup	k1gInSc1
vrtulníku	vrtulník	k1gInSc2
je	být	k5eAaImIp3nS
14	#num#	k4
<g/>
×	×	k?
<g/>
14	#num#	k4
<g/>
×	×	k?
<g/>
14	#num#	k4
cm	cm	kA
velký	velký	k2eAgMnSc1d1
a	a	k8xC
NASA	NASA	kA
jej	on	k3xPp3gInSc4
přirovnává	přirovnávat	k5eAaImIp3nS
k	k	k7c3
velikosti	velikost	k1gFnSc3
softballového	softballový	k2eAgInSc2d1
míčku	míček	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Vrtulník	vrtulník	k1gInSc1
je	být	k5eAaImIp3nS
až	až	k9
do	do	k7c2
startu	start	k1gInSc2
skrytý	skrytý	k2eAgInSc4d1
v	v	k7c6
trupu	trup	k1gInSc6
vozítka	vozítko	k1gNnSc2
Perseverance	perseverance	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
až	až	k9
ve	v	k7c6
chvíli	chvíle	k1gFnSc6
startu	start	k1gInSc2
je	být	k5eAaImIp3nS
vypuštěn	vypustit	k5eAaPmNgInS
na	na	k7c4
povrch	povrch	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
22	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2021	#num#	k4
NASA	NASA	kA
oznámila	oznámit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
helikoptéra	helikoptéra	k1gFnSc1
přistání	přistání	k1gNnSc2
přežila	přežít	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Plánovaný	plánovaný	k2eAgInSc4d1
první	první	k4xOgInSc4
let	let	k1gInSc4
by	by	kYmCp3nS
měl	mít	k5eAaImAgMnS
proběhnout	proběhnout	k5eAaPmF
asi	asi	k9
60	#num#	k4
dní	den	k1gInPc2
po	po	k7c4
přistání	přistání	k1gNnSc4
vozítka	vozítko	k1gNnSc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
v	v	k7c6
dubnu	duben	k1gInSc6
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vozítko	vozítko	k1gNnSc1
na	na	k7c6
sobě	sebe	k3xPyFc6
má	mít	k5eAaImIp3nS
speciální	speciální	k2eAgNnPc4d1
komunikační	komunikační	k2eAgNnPc4d1
zařízení	zařízení	k1gNnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
data	datum	k1gNnPc1
z	z	k7c2
vrtulníku	vrtulník	k1gInSc2
jsou	být	k5eAaImIp3nP
zpracovávána	zpracováván	k2eAgFnSc1d1
stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
data	datum	k1gNnPc4
z	z	k7c2
ostatních	ostatní	k2eAgInPc2d1
vědeckých	vědecký	k2eAgInPc2d1
přístrojů	přístroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Helikoptéra	helikoptéra	k1gFnSc1
je	být	k5eAaImIp3nS
určená	určený	k2eAgFnSc1d1
pro	pro	k7c4
2	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
minuty	minuta	k1gFnSc2
provozu	provoz	k1gInSc2
denně	denně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
lety	let	k1gInPc4
bylo	být	k5eAaImAgNnS
vyhrazeno	vyhradit	k5eAaPmNgNnS
okno	okno	k1gNnSc4
30	#num#	k4
dní	den	k1gInPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
při	při	k7c6
němž	jenž	k3xRgNnSc6
by	by	kYmCp3nS
se	se	k3xPyFc4
mělo	mít	k5eAaImAgNnS
zvládnout	zvládnout	k5eAaPmF
5	#num#	k4
letů	let	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
13	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2020	#num#	k4
NASA	NASA	kA
oznámila	oznámit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
Ingenuity	Ingenuita	k1gFnSc2
bylo	být	k5eAaImAgNnS
poprvé	poprvé	k6eAd1
zapnuto	zapnout	k5eAaPmNgNnS
ve	v	k7c6
vesmíru	vesmír	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
4	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2021	#num#	k4
pak	pak	k6eAd1
vrtulník	vrtulník	k1gInSc1
opustil	opustit	k5eAaPmAgInS
své	svůj	k3xOyFgNnSc4
místo	místo	k1gNnSc4
na	na	k7c6
břiše	břicho	k1gNnSc6
vozidla	vozidlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vozítko	vozítko	k1gNnSc1
následně	následně	k6eAd1
poodjelo	poodjet	k5eAaPmAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
6	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2021	#num#	k4
bylo	být	k5eAaImAgNnS
oznámeno	oznámit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
vrtulník	vrtulník	k1gInSc1
úspěšně	úspěšně	k6eAd1
přečkal	přečkat	k5eAaPmAgInS
první	první	k4xOgFnSc4
noc	noc	k1gFnSc4
mimo	mimo	k7c4
vozítko	vozítko	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
NASA	NASA	kA
6	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
také	také	k9
oznámila	oznámit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
vrtulník	vrtulník	k1gInSc1
pořídil	pořídit	k5eAaPmAgInS
první	první	k4xOgFnSc4
barevnou	barevný	k2eAgFnSc4d1
fotografii	fotografia	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
9	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
vrtulník	vrtulník	k1gInSc1
poprvé	poprvé	k6eAd1
roztočil	roztočit	k5eAaPmAgInS
vrtule	vrtule	k1gFnSc2
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
však	však	k9
na	na	k7c4
50	#num#	k4
otáček	otáčka	k1gFnPc2
za	za	k7c4
minutu	minuta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rover	rover	k1gMnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
asi	asi	k9
60	#num#	k4
metrů	metr	k1gInPc2
od	od	k7c2
vrtulníku	vrtulník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgInSc1
let	let	k1gInSc1
měl	mít	k5eAaImAgInS
proběhnout	proběhnout	k5eAaPmF
14	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
<g/>
,	,	kIx,
protože	protože	k8xS
byl	být	k5eAaImAgInS
odložen	odložit	k5eAaPmNgInS
z	z	k7c2
11	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvodem	důvod	k1gInSc7
jsou	být	k5eAaImIp3nP
potíže	potíž	k1gFnPc4
ve	v	k7c6
vysokých	vysoký	k2eAgFnPc6d1
otáčkách	otáčka	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
První	první	k4xOgInSc1
let	let	k1gInSc1
se	se	k3xPyFc4
uskutečnil	uskutečnit	k5eAaPmAgInS
19	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2021	#num#	k4
od	od	k7c2
12	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
vědci	vědec	k1gMnPc1
začali	začít	k5eAaPmAgMnP
přijímat	přijímat	k5eAaImF
první	první	k4xOgNnPc1
data	datum	k1gNnPc1
od	od	k7c2
vrtulníku	vrtulník	k1gInSc2
Ingenuity	Ingenuita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Let	let	k1gInSc1
trval	trvat	k5eAaImAgInS
39,1	39,1	k4
sekundy	sekunda	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Technologie	technologie	k1gFnSc1
</s>
<s>
Většina	většina	k1gFnSc1
technologií	technologie	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
jsou	být	k5eAaImIp3nP
použity	použít	k5eAaPmNgFnP
na	na	k7c6
vrtulníku	vrtulník	k1gInSc6
jsou	být	k5eAaImIp3nP
volně	volně	k6eAd1
dostupné	dostupný	k2eAgFnPc1d1
a	a	k8xC
volně	volně	k6eAd1
se	se	k3xPyFc4
prodávají	prodávat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Software	software	k1gInSc1
nebyl	být	k5eNaImAgInS
vytvořen	vytvořit	k5eAaPmNgInS
speciálně	speciálně	k6eAd1
na	na	k7c4
míru	míra	k1gFnSc4
vrtulníku	vrtulník	k1gInSc2
<g/>
,	,	kIx,
protože	protože	k8xS
vše	všechen	k3xTgNnSc1
běží	běžet	k5eAaImIp3nS
na	na	k7c6
operačním	operační	k2eAgInSc6d1
systému	systém	k1gInSc6
Linux	Linux	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
Tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
Mars	Mars	k1gInSc1
stal	stát	k5eAaPmAgInS
druhou	druhý	k4xOgFnSc7
planetou	planeta	k1gFnSc7
Sluneční	sluneční	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
má	mít	k5eAaImIp3nS
Linux	linux	k1gInSc4
větší	veliký	k2eAgNnSc4d2
zastoupení	zastoupení	k1gNnSc4
než	než	k8xS
Windows	Windows	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Řídící	řídící	k2eAgInSc1d1
a	a	k8xC
stabilizační	stabilizační	k2eAgInSc1d1
systém	systém	k1gInSc1
byl	být	k5eAaImAgInS
vyvinut	vyvinout	k5eAaPmNgInS
pro	pro	k7c4
minisatelity	minisatelita	k1gFnPc4
CubeSats	CubeSatsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Vrtulník	vrtulník	k1gInSc1
je	být	k5eAaImIp3nS
naplněný	naplněný	k2eAgInSc1d1
CO	co	k8xS
<g/>
2	#num#	k4
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
by	by	kYmCp3nP
měl	mít	k5eAaImAgInS
vrtulník	vrtulník	k1gInSc4
snáze	snadno	k6eAd2
odolávat	odolávat	k5eAaImF
mrazu	mráz	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Hardware	hardware	k1gInSc1
je	být	k5eAaImIp3nS
však	však	k9
z	z	k7c2
většiny	většina	k1gFnSc2
z	z	k7c2
veřejně	veřejně	k6eAd1
dostupných	dostupný	k2eAgInPc2d1
produktů	produkt	k1gInPc2
-	-	kIx~
například	například	k6eAd1
procesorová	procesorový	k2eAgFnSc1d1
deska	deska	k1gFnSc1
je	být	k5eAaImIp3nS
tvořena	tvořit	k5eAaImNgFnS
28	#num#	k4
nanometrovým	nanometrový	k2eAgInSc7d1
procesorem	procesor	k1gInSc7
Snapdragon	Snapdragon	k1gInSc4
801	#num#	k4
z	z	k7c2
roku	rok	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
procesor	procesor	k1gInSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
v	v	k7c6
mobilních	mobilní	k2eAgInPc6d1
telefonech	telefon	k1gInPc6
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
přes	přes	k7c4
to	ten	k3xDgNnSc4
má	mít	k5eAaImIp3nS
výpočetní	výpočetní	k2eAgInSc1d1
systém	systém	k1gInSc1
vrtulníku	vrtulník	k1gInSc2
vyšší	vysoký	k2eAgInSc1d2
výkon	výkon	k1gInSc1
než	než	k8xS
vozítko	vozítko	k1gNnSc1
Perseverance	perseverance	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Naváděcí	naváděcí	k2eAgFnSc2d1
smyčky	smyčka	k1gFnSc2
běží	běžet	k5eAaImIp3nS
na	na	k7c4
frekvenci	frekvence	k1gFnSc4
500	#num#	k4
Hz	Hz	kA
<g/>
,	,	kIx,
snímky	snímek	k1gInPc1
jsou	být	k5eAaImIp3nP
pak	pak	k6eAd1
pořizovány	pořizovat	k5eAaImNgInP
kamerou	kamera	k1gFnSc7
o	o	k7c6
frekvenci	frekvence	k1gFnSc6
30	#num#	k4
Hz	Hz	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrtulník	vrtulník	k1gInSc1
má	mít	k5eAaImIp3nS
laserový	laserový	k2eAgInSc1d1
výškoměr	výškoměr	k1gInSc1
od	od	k7c2
společnosti	společnost	k1gFnSc2
SparkFun	SparkFun	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
umístěn	umístit	k5eAaPmNgInS
na	na	k7c6
spodu	spod	k1gInSc6
vrtulníku	vrtulník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrtulník	vrtulník	k1gInSc1
má	mít	k5eAaImIp3nS
také	také	k9
sklonoměr	sklonoměr	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
stará	starat	k5eAaImIp3nS
o	o	k7c4
hladký	hladký	k2eAgInSc4d1
průběh	průběh	k1gInSc4
startu	start	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalšími	další	k1gNnPc7
zařízeními	zařízení	k1gNnPc7
jsou	být	k5eAaImIp3nP
gyroskopy	gyroskop	k1gInPc1
a	a	k8xC
akcelerometry	akcelerometr	k1gInPc1
jsou	být	k5eAaImIp3nP
součástí	součást	k1gFnSc7
komplexního	komplexní	k2eAgInSc2d1
systému	systém	k1gInSc2
SoC	soc	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrtulník	vrtulník	k1gInSc1
snímkuje	snímkovat	k5eAaImIp3nS
povrch	povrch	k1gInSc4
díky	díky	k7c3
13	#num#	k4
megapixelovému	megapixelový	k2eAgInSc3d1
barevnému	barevný	k2eAgInSc3d1
fotoaparátu	fotoaparát	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vypuštění	vypuštění	k1gNnSc1
vrtulníku	vrtulník	k1gInSc2
a	a	k8xC
průběh	průběh	k1gInSc4
letu	let	k1gInSc2
</s>
<s>
Samotný	samotný	k2eAgInSc1d1
proces	proces	k1gInSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
dvou	dva	k4xCgFnPc2
částí	část	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
Vypuštění	vypuštění	k1gNnSc1
vrtulníku	vrtulník	k1gInSc2
a	a	k8xC
oddělení	oddělení	k1gNnSc2
od	od	k7c2
vozítka	vozítko	k1gNnSc2
</s>
<s>
Odpadne	odpadnout	k5eAaPmIp3nS
plastový	plastový	k2eAgInSc1d1
kryt	kryt	k1gInSc1
vrtulníku	vrtulník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Vrtulník	vrtulník	k1gInSc1
je	být	k5eAaImIp3nS
oddělen	oddělit	k5eAaPmNgInS
od	od	k7c2
vozítka	vozítko	k1gNnSc2
a	a	k8xC
postaven	postavit	k5eAaPmNgMnS
na	na	k7c4
povrch	povrch	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Vozítko	vozítko	k1gNnSc1
poodjede	poodjet	k5eAaPmIp3nS
do	do	k7c2
dostatečné	dostatečný	k2eAgFnSc2d1
vzdálenosti	vzdálenost	k1gFnSc2
(	(	kIx(
<g/>
cca	cca	kA
<g/>
.	.	kIx.
60	#num#	k4
metrů	metr	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Příprava	příprava	k1gFnSc1
na	na	k7c4
odhození	odhození	k1gNnSc4
plastového	plastový	k2eAgInSc2d1
krytu	kryt	k1gInSc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Odhození	odhození	k1gNnSc1
plastového	plastový	k2eAgInSc2d1
krytu	kryt	k1gInSc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Vrtulník	vrtulník	k1gInSc1
připevněný	připevněný	k2eAgInSc1d1
na	na	k7c6
břiše	břich	k1gInSc6
vozítka	vozítko	k1gNnSc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Uvolňování	uvolňování	k1gNnSc1
vrtulníku	vrtulník	k1gInSc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Uvolňování	uvolňování	k1gNnSc1
vrtulníku	vrtulník	k1gInSc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Uvolněný	uvolněný	k2eAgInSc1d1
vrtulník	vrtulník	k1gInSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Vrtulník	vrtulník	k1gInSc1
Ingenuity	Ingenuita	k1gFnSc2
několik	několik	k4yIc4
metrů	metr	k1gInPc2
od	od	k7c2
vozítka	vozítko	k1gNnSc2
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Let	let	k1gInSc1
vrtulníku	vrtulník	k1gInSc2
v	v	k7c6
atmosféře	atmosféra	k1gFnSc6
</s>
<s>
Vrtulník	vrtulník	k1gInSc1
vzletí	vzletět	k5eAaPmIp3nS
-	-	kIx~
dosáhne	dosáhnout	k5eAaPmIp3nS
plánované	plánovaný	k2eAgFnSc2d1
letové	letový	k2eAgFnSc2d1
výšky	výška	k1gFnSc2
a	a	k8xC
poté	poté	k6eAd1
bude	být	k5eAaImBp3nS
pokračovat	pokračovat	k5eAaImF
po	po	k7c6
trase	trasa	k1gFnSc6
mise	mise	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vrtulník	vrtulník	k1gInSc1
poletí	letět	k5eAaImIp3nS,k5eAaPmIp3nS
automaticky	automaticky	k6eAd1
po	po	k7c6
naplánované	naplánovaný	k2eAgFnSc6d1
trase	trasa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Orientace	orientace	k1gFnSc1
je	být	k5eAaImIp3nS
zajišťována	zajišťovat	k5eAaImNgFnS
díky	díky	k7c3
přesné	přesný	k2eAgFnSc3d1
analýze	analýza	k1gFnSc3
kamenů	kámen	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
byly	být	k5eAaImAgFnP
pozemní	pozemní	k2eAgInPc1d1
kontrolou	kontrola	k1gFnSc7
označeny	označen	k2eAgFnPc1d1
jako	jako	k8xS,k8xC
kontroalní	kontroalný	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Vrtulník	vrtulník	k1gInSc1
přistane	přistat	k5eAaPmIp3nS
na	na	k7c6
vytipovaném	vytipovaný	k2eAgNnSc6d1
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
bylo	být	k5eAaImAgNnS
zhodnoceno	zhodnocen	k2eAgNnSc1d1
jako	jako	k8xS,k8xC
bezpečné	bezpečný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Ingenuity	Ingenuita	k1gFnPc1
-	-	kIx~
umělecká	umělecký	k2eAgFnSc1d1
ilustrace	ilustrace	k1gFnSc1
na	na	k7c6
povrchu	povrch	k1gInSc6
Marsu	Mars	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Pohled	pohled	k1gInSc1
ze	z	k7c2
spodu	spod	k1gInSc2
<g/>
,	,	kIx,
lze	lze	k6eAd1
vidět	vidět	k5eAaImF
kameru	kamera	k1gFnSc4
a	a	k8xC
další	další	k2eAgNnSc4d1
zařízení	zařízení	k1gNnSc4
vrtulníku	vrtulník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Harmonogram	harmonogram	k1gInSc1
letů	let	k1gInPc2
</s>
<s>
Helikoptéra	helikoptéra	k1gFnSc1
Ingenuity	Ingenuita	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
pozadí	pozadí	k1gNnSc6
rover	rover	k1gMnSc1
Persevernace	Persevernace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Tým	tým	k1gInSc1
Ingenuity	Ingenuita	k1gFnSc2
dostal	dostat	k5eAaPmAgInS
30	#num#	k4
dní	den	k1gInPc2
na	na	k7c4
provedení	provedení	k1gNnSc4
všech	všecek	k3xTgInPc2
pokusů	pokus	k1gInPc2
<g/>
,	,	kIx,
poté	poté	k6eAd1
bude	být	k5eAaImBp3nS
muset	muset	k5eAaImF
Persevernace	Persevernace	k1gFnSc1
pokračovat	pokračovat	k5eAaImF
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
hlavní	hlavní	k2eAgFnSc6d1
misi	mise	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plán	plán	k1gInSc1
je	být	k5eAaImIp3nS
2	#num#	k4
-	-	kIx~
3	#num#	k4
minuty	minuta	k1gFnPc4
letu	let	k1gInSc2
denně	denně	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
letů	let	k1gInPc2
by	by	kYmCp3nS
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
provedeno	provést	k5eAaPmNgNnS
pouze	pouze	k6eAd1
pět	pět	k4xCc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tým	tým	k1gInSc4
však	však	k9
připouští	připouštět	k5eAaImIp3nS
i	i	k9
více	hodně	k6eAd2
letů	let	k1gInPc2
<g/>
,	,	kIx,
než	než	k8xS
je	být	k5eAaImIp3nS
plánováno	plánovat	k5eAaImNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
tři	tři	k4xCgInPc1
lety	let	k1gInPc1
jsou	být	k5eAaImIp3nP
dle	dle	k7c2
vědců	vědec	k1gMnPc2
minimum	minimum	k1gNnSc4
nutné	nutný	k2eAgNnSc4d1
pro	pro	k7c4
otestování	otestování	k1gNnSc4
všech	všecek	k3xTgFnPc2
technologií	technologie	k1gFnPc2
<g/>
,	,	kIx,
vše	všechen	k3xTgNnSc1
poté	poté	k6eAd1
je	být	k5eAaImIp3nS
bonus	bonus	k1gInSc4
a	a	k8xC
byly	být	k5eAaImAgFnP
by	by	kYmCp3nP
testovány	testován	k2eAgFnPc1d1
jiné	jiný	k2eAgFnPc1d1
<g/>
,	,	kIx,
například	například	k6eAd1
nebezpečné	bezpečný	k2eNgInPc1d1
<g/>
,	,	kIx,
věci	věc	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
První	první	k4xOgFnSc1
zpráva	zpráva	k1gFnSc1
o	o	k7c6
stavu	stav	k1gInSc6
vrtulníku	vrtulník	k1gInSc2
přišla	přijít	k5eAaPmAgFnS
19	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2021	#num#	k4
v	v	k7c4
15	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
PST	pst	k0
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
zprávě	zpráva	k1gFnSc6
stálo	stát	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
baterie	baterie	k1gFnPc1
i	i	k8xC
topná	topný	k2eAgNnPc1d1
tělesa	těleso	k1gNnPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
pořádku	pořádek	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
První	první	k4xOgInSc1
barevný	barevný	k2eAgInSc1d1
snímek	snímek	k1gInSc1
pořízený	pořízený	k2eAgInSc1d1
vrtulníkem	vrtulník	k1gInSc7
Ingenuity	Ingenuita	k1gFnSc2
</s>
<s>
Vývoj	vývoj	k1gInSc1
</s>
<s>
Popis	popis	k1gInSc1
helikoptéry	helikoptéra	k1gFnSc2
Ineguity	Ineguita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
vrtulníku	vrtulník	k1gInSc2
začal	začít	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
v	v	k7c6
laboratořích	laboratoř	k1gFnPc6
JPL	JPL	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
Během	během	k7c2
šesti	šest	k4xCc2
let	léto	k1gNnPc2
výzkumu	výzkum	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
zakončen	zakončit	k5eAaPmNgInS
úspěšným	úspěšný	k2eAgInSc7d1
testem	test	k1gInSc7
v	v	k7c6
atmosféře	atmosféra	k1gFnSc6
podobné	podobný	k2eAgFnPc1d1
té	ten	k3xDgFnSc3
marsovské	marsovský	k2eAgFnSc3d1
<g/>
,	,	kIx,
ukázali	ukázat	k5eAaPmAgMnP
odborníci	odborník	k1gMnPc1
<g/>
,	,	kIx,
že	že	k8xS
lze	lze	k6eAd1
udělat	udělat	k5eAaPmF
stroj	stroj	k1gInSc1
tak	tak	k6eAd1
lehký	lehký	k2eAgInSc1d1
a	a	k8xC
zároveň	zároveň	k6eAd1
výkonný	výkonný	k2eAgMnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
dokáže	dokázat	k5eAaPmIp3nS
udělat	udělat	k5eAaPmF
dostatečný	dostatečný	k2eAgInSc4d1
vztlak	vztlak	k1gInSc4
v	v	k7c6
atmosféře	atmosféra	k1gFnSc6
Marsu	Mars	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
byly	být	k5eAaImAgInP
vytvořeny	vytvořen	k2eAgInPc1d1
první	první	k4xOgInPc1
prototypy	prototyp	k1gInPc1
vrtulí	vrtule	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
by	by	kYmCp3nP
dodaly	dodat	k5eAaPmAgFnP
dostatečný	dostatečný	k2eAgInSc4d1
vztlak	vztlak	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
březnu	březen	k1gInSc6
2014	#num#	k4
proběhl	proběhnout	k5eAaPmAgInS
první	první	k4xOgInSc1
test	test	k1gInSc1
1	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
rotorů	rotor	k1gInPc2
vrtulníku	vrtulník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
prosinci	prosinec	k1gInSc6
2014	#num#	k4
letoun	letoun	k1gInSc1
udělal	udělat	k5eAaPmAgInS
první	první	k4xOgInSc4
nekontrolovaný	kontrolovaný	k2eNgInSc4d1
let	let	k1gInSc4
(	(	kIx(
<g/>
takzvaný	takzvaný	k2eAgInSc4d1
skok	skok	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
proběhl	proběhnout	k5eAaPmAgInS
první	první	k4xOgInSc4
kontrolovaný	kontrolovaný	k2eAgInSc4d1
let	let	k1gInSc4
<g/>
,	,	kIx,
ovšem	ovšem	k9
ne	ne	k9
v	v	k7c6
marsovské	marsovský	k2eAgFnSc6d1
atmosféře	atmosféra	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Testování	testování	k1gNnPc1
na	na	k7c6
Zemi	zem	k1gFnSc6
</s>
<s>
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
rekord	rekord	k1gInSc1
vrtulníku	vrtulník	k1gInSc2
v	v	k7c6
zemské	zemský	k2eAgFnSc6d1
atmosféře	atmosféra	k1gFnSc6
je	být	k5eAaImIp3nS
12,2	12,2	k4
km	km	kA
a	a	k8xC
tlak	tlak	k1gInSc1
v	v	k7c6
marsovské	marsovský	k2eAgFnSc6d1
atmosféře	atmosféra	k1gFnSc6
v	v	k7c6
letové	letový	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
Ingenuity	Ingenuita	k1gFnSc2
odpovídá	odpovídat	k5eAaImIp3nS
30,5	30,5	k4
km	km	kA
nad	nad	k7c7
povrchem	povrch	k1gInSc7
v	v	k7c6
zemské	zemský	k2eAgFnSc6d1
atmosféře	atmosféra	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
Testování	testování	k1gNnSc4
vrtulníku	vrtulník	k1gInSc2
tedy	tedy	k8xC
probíhalo	probíhat	k5eAaImAgNnS
ve	v	k7c6
speciální	speciální	k2eAgFnSc6d1
komoře	komora	k1gFnSc6
s	s	k7c7
řidším	řídký	k2eAgInSc7d2
vzduchem	vzduch	k1gInSc7
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
vrtulník	vrtulník	k1gInSc1
byl	být	k5eAaImAgInS
na	na	k7c6
laně	lano	k1gNnSc6
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
má	mít	k5eAaImIp3nS
simulovat	simulovat	k5eAaImF
menší	malý	k2eAgFnSc4d2
gravitaci	gravitace	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1
testování	testování	k1gNnSc1
probíhalo	probíhat	k5eAaImAgNnS
ve	v	k7c6
vakuové	vakuový	k2eAgFnSc6d1
komoře	komora	k1gFnSc6
Space	Space	k1gMnSc1
Simulator	Simulator	k1gMnSc1
o	o	k7c6
průměru	průměr	k1gInSc6
7,62	7,62	k4
metru	metr	k1gInSc2
<g/>
.	.	kIx.
tato	tento	k3xDgFnSc1
komora	komora	k1gFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
Pasadeně	Pasadena	k1gFnSc6
v	v	k7c6
Kalifornii	Kalifornie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
Testy	test	k1gInPc1
probíhaly	probíhat	k5eAaImAgInP
i	i	k9
v	v	k7c6
areálu	areál	k1gInSc6
Lockheed-Martin	Lockheed-Martina	k1gFnPc2
v	v	k7c6
Denveru	Denver	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tým	tým	k1gInSc1
Ingenuity	Ingenuita	k1gFnSc2
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Timothy	Timotha	k1gFnPc1
Canham	Canham	k1gInSc1
-	-	kIx~
vedoucí	vedoucí	k1gFnSc1
provozu	provoz	k1gInSc2
vrtulníku	vrtulník	k1gInSc2
</s>
<s>
Jennifer	Jennifer	k1gInSc1
Shatts	Shatts	k1gInSc1
-	-	kIx~
systémový	systémový	k2eAgMnSc1d1
inženýr	inženýr	k1gMnSc1
</s>
<s>
Hå	Hå	k1gMnSc1
Fjæ	Fjæ	k1gMnSc1
Grip	Grip	k1gMnSc1
-	-	kIx~
hlavní	hlavní	k2eAgMnSc1d1
pilot	pilot	k1gMnSc1
</s>
<s>
Theodora	Theodora	k1gFnSc1
Tzanetose	Tzanetosa	k1gFnSc6
-	-	kIx~
taktické	taktický	k2eAgNnSc1d1
vedení	vedení	k1gNnSc1
</s>
<s>
Vandi	Vandit	k5eAaPmRp2nS
Verma	Verma	k1gFnSc1
-	-	kIx~
hlavní	hlavní	k2eAgMnSc1d1
inženýr	inženýr	k1gMnSc1
robotických	robotický	k2eAgFnPc2d1
operací	operace	k1gFnPc2
</s>
<s>
Mimi	Mimi	k1gFnSc1
Aung	Aung	k1gMnSc1
-	-	kIx~
vedoucí	vedoucí	k1gMnSc1
projektu	projekt	k1gInSc2
</s>
<s>
Cíle	cíl	k1gInPc1
</s>
<s>
Cílem	cíl	k1gInSc7
mise	mise	k1gFnSc2
je	být	k5eAaImIp3nS
demonstrovat	demonstrovat	k5eAaBmF
technologický	technologický	k2eAgInSc4d1
pokrok	pokrok	k1gInSc4
a	a	k8xC
zároveň	zároveň	k6eAd1
otestovat	otestovat	k5eAaPmF
technologie	technologie	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
by	by	kYmCp3nP
byly	být	k5eAaImAgFnP
nutné	nutný	k2eAgFnPc1d1
pro	pro	k7c4
případné	případný	k2eAgNnSc4d1
cestování	cestování	k1gNnSc4
lidí	člověk	k1gMnPc2
nebo	nebo	k8xC
strojů	stroj	k1gInPc2
po	po	k7c6
Marsu	Mars	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
budoucna	budoucno	k1gNnSc2
by	by	kYmCp3nS
měl	mít	k5eAaImAgInS
obdobný	obdobný	k2eAgInSc1d1
vrtulník	vrtulník	k1gInSc1
mít	mít	k5eAaImF
dolet	dolet	k1gInSc4
i	i	k9
přes	přes	k7c4
600	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrtulník	vrtulník	k1gInSc1
také	také	k6eAd1
bude	být	k5eAaImBp3nS
zkoumat	zkoumat	k5eAaImF
místa	místo	k1gNnPc4
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yRgNnPc1,k3yQgNnPc1
jsou	být	k5eAaImIp3nP
pro	pro	k7c4
vozítko	vozítko	k1gNnSc4
nedostupná	dostupný	k2eNgNnPc1d1
pro	pro	k7c4
náročný	náročný	k2eAgInSc4d1
terén	terén	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Minimum	minimum	k1gNnSc1
letů	let	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
musí	muset	k5eAaImIp3nP
vrtulník	vrtulník	k1gInSc4
absolvovat	absolvovat	k5eAaPmF
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
pět	pět	k4xCc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Lety	let	k1gInPc1
</s>
<s>
To	ten	k3xDgNnSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
bude	být	k5eAaImBp3nS
proveden	provést	k5eAaPmNgInS
první	první	k4xOgInSc1
let	let	k1gInSc1
<g/>
,	,	kIx,
NASA	NASA	kA
oznámil	oznámit	k5eAaPmAgMnS
na	na	k7c4
online	onlin	k1gInSc5
briefingu	briefing	k1gInSc2
pro	pro	k7c4
média	médium	k1gNnPc4
v	v	k7c6
úterý	úterý	k1gNnSc6
23	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
v	v	k7c6
13	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
EDT	EDT	kA
(	(	kIx(
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
PDT	PDT	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sledovat	sledovat	k5eAaImF
ho	on	k3xPp3gNnSc4
šlo	jít	k5eAaImAgNnS
buď	buď	k8xC
přes	přes	k7c4
NASA	NASA	kA
TV	TV	kA
<g/>
,	,	kIx,
YouTube	YouTub	k1gInSc5
nebo	nebo	k8xC
další	další	k2eAgFnPc4d1
sociální	sociální	k2eAgFnPc4d1
sítě	síť	k1gFnPc4
NASA	NASA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veřejnost	veřejnost	k1gFnSc1
mohla	moct	k5eAaImAgFnS
klást	klást	k5eAaImF
dotazy	dotaz	k1gInPc4
přes	přes	k7c4
sociální	sociální	k2eAgFnPc4d1
sítě	síť	k1gFnPc4
pod	pod	k7c7
hastagem	hastago	k1gNnSc7
#	#	kIx~
<g/>
MarsHelicopter	MarsHelicopter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
tomto	tento	k3xDgInSc6
briefingu	briefing	k1gInSc6
bylo	být	k5eAaImAgNnS
oznámeno	oznámit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
první	první	k4xOgNnSc1
let	léto	k1gNnPc2
proběhne	proběhnout	k5eAaPmIp3nS
začátkem	začátek	k1gInSc7
dubna	duben	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přehled	přehled	k1gInSc1
letů	let	k1gInPc2
</s>
<s>
Číslo	číslo	k1gNnSc1
letu	let	k1gInSc2
</s>
<s>
Datum	datum	k1gNnSc1
letu	let	k1gInSc2
</s>
<s>
Cíle	cíl	k1gInPc1
</s>
<s>
Úspúěšnost	Úspúěšnost	k1gFnSc1
</s>
<s>
Délka	délka	k1gFnSc1
trvání	trvání	k1gNnSc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2021	#num#	k4
</s>
<s>
Vyzkoušet	vyzkoušet	k5eAaPmF
použité	použitý	k2eAgFnPc4d1
technologie	technologie	k1gFnPc4
a	a	k8xC
pořídit	pořídit	k5eAaPmF
snímky	snímek	k1gInPc4
povrchu	povrch	k1gInSc2
a	a	k8xC
vozítka	vozítko	k1gNnSc2
Perseverance	perseverance	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
úspěšně	úspěšně	k6eAd1
přistál	přistát	k5eAaImAgInS,k5eAaPmAgInS
</s>
<s>
39,1	39,1	k4
sekundy	sekunda	k1gFnPc4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
duben	duben	k1gInSc1
2021	#num#	k4
</s>
<s>
Pořídit	pořídit	k5eAaPmF
snímky	snímek	k1gInPc4
povrchu	povrch	k1gInSc2
a	a	k8xC
zmapovat	zmapovat	k5eAaPmF
terén	terén	k1gInSc4
v	v	k7c6
okolí	okolí	k1gNnSc6
vozítka	vozítko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
duben	duben	k1gInSc1
2021	#num#	k4
</s>
<s>
Bude	být	k5eAaImBp3nS
oznámeno	oznámen	k2eAgNnSc1d1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
duben	duben	k1gInSc1
2021	#num#	k4
</s>
<s>
Bude	být	k5eAaImBp3nS
oznámeno	oznámen	k2eAgNnSc1d1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
duben	duben	k1gInSc1
2021	#num#	k4
</s>
<s>
Bude	být	k5eAaImBp3nS
oznámeno	oznámen	k2eAgNnSc1d1
</s>
<s>
Místo	místo	k7c2
působení	působení	k1gNnSc2
</s>
<s>
Vrtulník	vrtulník	k1gInSc1
Ingenuity	Ingenuita	k1gFnSc2
bude	být	k5eAaImBp3nS
létat	létat	k5eAaImF
v	v	k7c6
kráteru	kráter	k1gInSc6
Jezero	jezero	k1gNnSc1
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
vozítko	vozítko	k1gNnSc4
Perseverance	perseverance	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
teplota	teplota	k1gFnSc1
v	v	k7c6
kráteru	kráter	k1gInSc6
klesá	klesat	k5eAaImIp3nS
v	v	k7c6
noci	noc	k1gFnSc6
až	až	k9
na	na	k7c4
-90	-90	k4
°	°	k?
<g/>
C	C	kA
bude	být	k5eAaImBp3nS
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
energie	energie	k1gFnSc2
vyloženo	vyložit	k5eAaPmNgNnS
na	na	k7c4
ohřev	ohřev	k1gInSc4
<g/>
,	,	kIx,
proto	proto	k8xC
dosah	dosah	k1gInSc1
ani	ani	k8xC
dolet	dolet	k1gInSc1
vrtulníku	vrtulník	k1gInSc2
není	být	k5eNaImIp3nS
příliš	příliš	k6eAd1
velký	velký	k2eAgInSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
Vrtulník	vrtulník	k1gInSc1
se	se	k3xPyFc4
bude	být	k5eAaImBp3nS
soustředit	soustředit	k5eAaPmF
na	na	k7c4
zkoumání	zkoumání	k1gNnSc4
okolí	okolí	k1gNnSc2
vozítka	vozítko	k1gNnSc2
a	a	k8xC
také	také	k9
jeho	jeho	k3xOp3gFnPc1
následné	následný	k2eAgFnPc1d1
cesty	cesta	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tým	tým	k1gInSc1
na	na	k7c4
Zemi	zem	k1gFnSc4
vytvořil	vytvořit	k5eAaPmAgInS
přesné	přesný	k2eAgInPc4d1
plány	plán	k1gInPc4
letů	let	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Název	název	k1gInSc1
Ingenuity	Ingenuita	k1gFnSc2
byl	být	k5eAaImAgInS
navržen	navrhnout	k5eAaPmNgInS
Alezi	Aleze	k1gFnSc3
Rupani	Rupan	k1gMnPc1
<g/>
,	,	kIx,
studentkou	studentka	k1gFnSc7
střední	střední	k2eAgFnSc2d1
školy	škola	k1gFnSc2
v	v	k7c6
Alabamě	Alabama	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
rámci	rámec	k1gInSc6
akce	akce	k1gFnSc2
Name	Nam	k1gFnSc2
the	the	k?
Rover	rover	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
Původně	původně	k6eAd1
mělo	mít	k5eAaImAgNnS
tento	tento	k3xDgInSc4
název	název	k1gInSc4
nést	nést	k5eAaImF
vozítko	vozítko	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
však	však	k9
nakonec	nakonec	k6eAd1
bylo	být	k5eAaImAgNnS
pojmenováno	pojmenovat	k5eAaPmNgNnS
Perseverance	perseverance	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tým	tým	k1gInSc1
NASA	NASA	kA
a	a	k8xC
JPL	JPL	kA
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
však	však	k9
za	za	k7c4
název	název	k1gInSc4
zodpovídal	zodpovídat	k5eAaImAgInS,k5eAaPmAgInS
<g/>
,	,	kIx,
však	však	k9
rozhodl	rozhodnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
název	název	k1gInSc1
Ingenuity	Ingenuita	k1gFnSc2
(	(	kIx(
<g/>
česky	česky	k6eAd1
vynalézavost	vynalézavost	k1gFnSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
hodí	hodit	k5eAaImIp3nS,k5eAaPmIp3nS
na	na	k7c4
velice	velice	k6eAd1
odvážný	odvážný	k2eAgInSc4d1
koncept	koncept	k1gInSc4
vrtulníku	vrtulník	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Do	do	k7c2
března	březen	k1gInSc2
2020	#num#	k4
byl	být	k5eAaImAgInS
oficiálně	oficiálně	k6eAd1
používán	používat	k5eAaImNgInS
název	název	k1gInSc1
JPL	JPL	kA
Mars	Mars	k1gInSc1
helicpoter	helicpoter	k1gInSc1
scout	scout	k1gInSc1
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2020	#num#	k4
JPL	JPL	kA
oznámilo	oznámit	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
bude	být	k5eAaImBp3nS
používán	používat	k5eAaImNgInS
název	název	k1gInSc1
Mars	Mars	k1gInSc1
Helicpoter	Helicpoter	k1gInSc1
<g/>
.	.	kIx.
29	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2020	#num#	k4
pak	pak	k8xC
JPL	JPL	kA
na	na	k7c6
tiskové	tiskový	k2eAgFnSc6d1
konferenci	konference	k1gFnSc6
uvedlo	uvést	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
komise	komise	k1gFnSc1
vybrala	vybrat	k5eAaPmAgFnS
název	název	k1gInSc4
pro	pro	k7c4
vrtulníček	vrtulníček	k1gInSc4
Ingenuity	Ingenuita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vrtulníku	vrtulník	k1gInSc2
Ingenuity	Ingenuita	k1gMnPc4
je	být	k5eAaImIp3nS
také	také	k9
přezdíváno	přezdíván	k2eAgNnSc4d1
Ginny	Ginna	k1gFnPc4
<g/>
,	,	kIx,
tato	tento	k3xDgFnSc1
přezdívka	přezdívka	k1gFnSc1
však	však	k9
byla	být	k5eAaImAgFnS
využívána	využívat	k5eAaPmNgFnS,k5eAaImNgFnS
hlavně	hlavně	k9
před	před	k7c7
konečným	konečný	k2eAgNnSc7d1
pojmenováním	pojmenování	k1gNnSc7
Ingenuity	Ingenuita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Technické	technický	k2eAgFnPc1d1
specifikace	specifikace	k1gFnPc1
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1
<g/>
:	:	kIx,
1,8	1,8	k4
kg	kg	kA
na	na	k7c4
Zemi	zem	k1gFnSc4
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výška	výška	k1gFnSc1
<g/>
:	:	kIx,
49	#num#	k4
cm	cm	kA
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dolet	dolet	k1gInSc1
<g/>
:	:	kIx,
300	#num#	k4
metrů	metr	k1gInPc2
</s>
<s>
Výška	výška	k1gFnSc1
letu	let	k1gInSc2
(	(	kIx(
<g/>
dostup	dostup	k1gInSc1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
3	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
metrů	metr	k1gInPc2
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1
doba	doba	k1gFnSc1
letu	let	k1gInSc2
<g/>
:	:	kIx,
90	#num#	k4
sekund	sekunda	k1gFnPc2
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
horizontální	horizontální	k2eAgFnSc1d1
<g/>
:	:	kIx,
10	#num#	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
(	(	kIx(
<g/>
36	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
)	)	kIx)
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
svislá	svislý	k2eAgFnSc1d1
<g/>
:	:	kIx,
3	#num#	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
(	(	kIx(
<g/>
10,8	10,8	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
)	)	kIx)
</s>
<s>
Maximální	maximální	k2eAgInSc1d1
dosah	dosah	k1gInSc1
rádiového	rádiový	k2eAgNnSc2d1
spojení	spojení	k1gNnSc2
vrtulníku	vrtulník	k1gInSc2
<g/>
:	:	kIx,
1000	#num#	k4
m	m	kA
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Velikost	velikost	k1gFnSc1
trupu	trup	k1gInSc2
<g/>
:	:	kIx,
14	#num#	k4
cm	cm	kA
x	x	k?
14	#num#	k4
cm	cm	kA
x	x	k?
14	#num#	k4
cm	cm	kA
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výkon	výkon	k1gInSc1
<g/>
:	:	kIx,
cca	cca	kA
<g/>
.	.	kIx.
350	#num#	k4
W	W	kA
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Baterie	baterie	k1gFnPc1
<g/>
:	:	kIx,
6	#num#	k4
<g/>
x	x	k?
Li-ion	Li-ion	k1gInSc1
baterie	baterie	k1gFnSc1
Sony	Sony	kA
220	#num#	k4
<g/>
W	W	kA
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Umělecká	umělecký	k2eAgFnSc1d1
představa	představa	k1gFnSc1
vrtulníku	vrtulník	k1gInSc2
na	na	k7c6
Marsu	Mars	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
pozadí	pozadí	k1gNnSc6
rover	rover	k1gMnSc1
Perseverance	perseverance	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Helikoptéra	helikoptéra	k1gFnSc1
Ingenuity	Ingenuita	k1gFnSc2
před	před	k7c7
umístěním	umístění	k1gNnSc7
do	do	k7c2
roveru	rover	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s>
Detail	detail	k1gInSc1
rotorů	rotor	k1gInPc2
vrtulníku	vrtulník	k1gInSc2
Ingenuity	Ingenuita	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Vrtulník	vrtulník	k1gInSc1
Ingenuity	Ingenuita	k1gFnSc2
při	při	k7c6
montáži	montáž	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Vrtulník	vrtulník	k1gInSc1
Ingenuity	Ingenuita	k1gFnSc2
při	při	k7c6
montáži	montáž	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Autorka	autorka	k1gFnSc1
návrhu	návrh	k1gInSc2
jména	jméno	k1gNnSc2
Alezi	Aleh	k1gMnPc1
Rupani	Rupan	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Logo	logo	k1gNnSc1
vrtulníku	vrtulník	k1gInSc2
Ingenuity	Ingenuita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Helikoptéra	helikoptéra	k1gFnSc1
Ingenuity	Ingenuita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
KONIG	KONIG	kA
<g/>
,	,	kIx,
Witold	Witold	k1gMnSc1
J.	J.	kA
F.	F.	kA
Generation	Generation	k1gInSc1
of	of	k?
Mars	Mars	k1gInSc1
Helicopter	Helicopter	k1gInSc1
Rotor	rotor	k1gInSc1
Model	model	k1gInSc1
for	forum	k1gNnPc2
Comprehensive	Comprehensiev	k1gFnSc2
Analyses	Analyses	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
MARS	Mars	k1gMnSc1
<g/>
.	.	kIx.
<g/>
NASA	NASA	kA
<g/>
.	.	kIx.
<g/>
GOV	GOV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mars	Mars	k1gInSc1
Helicopter	Helicopter	k1gInSc4
<g/>
.	.	kIx.
mars	mars	k1gInSc1
<g/>
.	.	kIx.
<g/>
nasa	nasa	k1gFnSc1
<g/>
.	.	kIx.
<g/>
gov	gov	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
LANDAU	LANDAU	kA
<g/>
,	,	kIx,
By	by	k9
Elizabeth	Elizabeth	k1gFnSc1
<g/>
.	.	kIx.
10	#num#	k4
Things	Things	k1gInSc1
<g/>
:	:	kIx,
Mars	Mars	k1gMnSc1
Helicopter	Helicopter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
NASA	NASA	kA
Solar	Solara	k1gFnPc2
System	Syst	k1gInSc7
Exploration	Exploration	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
FEBRUARY	FEBRUARY	kA
2021	#num#	k4
<g/>
,	,	kIx,
Chelsea	Chelsea	k1gFnSc1
Gohd	Gohd	k1gInSc1
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
NASA	NASA	kA
is	is	k?
about	about	k1gInSc1
to	ten	k3xDgNnSc4
land	land	k6eAd1
a	a	k8xC
helicopter	helicopter	k1gInSc4
on	on	k3xPp3gMnSc1
Mars	Mars	k1gMnSc1
for	forum	k1gNnPc2
the	the	k?
first	first	k1gMnSc1
time	timat	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Space	Space	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
JPL	JPL	kA
<g/>
:	:	kIx,
Oficiální	oficiální	k2eAgFnSc1d1
prezentace	prezentace	k1gFnSc1
na	na	k7c4
Youtube	Youtub	k1gInSc5
(	(	kIx(
<g/>
NASA	NASA	kA
<g/>
’	’	k?
<g/>
s	s	k7c7
Ingenuity	Ingenuita	k1gMnPc7
Mars	Mars	k1gMnSc1
Helicopter	Helicopter	k1gMnSc1
<g/>
:	:	kIx,
Attempting	Attempting	k1gInSc1
the	the	k?
First	First	k1gInSc1
Powered	Powered	k1gInSc1
Flight	Flight	k1gInSc4
on	on	k3xPp3gMnSc1
Mars	Mars	k1gMnSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
1	#num#	k4
2	#num#	k4
Full	Fulla	k1gFnPc2
Page	Page	k1gFnPc2
Reload	Reload	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
IEEE	IEEE	kA
Spectrum	Spectrum	k1gNnSc1
<g/>
:	:	kIx,
Technology	technolog	k1gMnPc4
<g/>
,	,	kIx,
Engineering	Engineering	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
Science	Science	k1gFnSc1
News	News	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ATKINSON	ATKINSON	kA
<g/>
,	,	kIx,
Ian	Ian	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
NASASpaceFlight	NASASpaceFlight	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2021-03-10	2021-03-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
GAURAV	GAURAV	kA
<g/>
,	,	kIx,
Kunal	Kunal	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hinudstan	Hinudstan	k1gInSc1
Times	Times	k1gInSc4
<g/>
.	.	kIx.
www.hindustantimes.com	www.hindustantimes.com	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2021-04-04	2021-04-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
S.	S.	kA
<g/>
R.	R.	kA
<g/>
O	O	kA
<g/>
,	,	kIx,
24	#num#	k4
<g/>
net	net	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Helikoptéra	helikoptéra	k1gFnSc1
Ingenuity	Ingenuita	k1gFnSc2
se	se	k3xPyFc4
odpojila	odpojit	k5eAaPmAgFnS
od	od	k7c2
roveru	rover	k1gMnSc6
Perseverance	perseverance	k1gFnSc2
<g/>
.	.	kIx.
fZone	fZonout	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ITBiz	ITBiz	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2021-04-06	2021-04-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
TELEVIZE	televize	k1gFnSc1
<g/>
,	,	kIx,
Česká	český	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Helikoptéra	helikoptéra	k1gFnSc1
Ingenuity	Ingenuita	k1gFnSc2
zvládla	zvládnout	k5eAaPmAgFnS
první	první	k4xOgFnSc4
samostatnou	samostatný	k2eAgFnSc4d1
noc	noc	k1gFnSc4
na	na	k7c6
Marsu	Mars	k1gInSc6
<g/>
,	,	kIx,
oznámila	oznámit	k5eAaPmAgFnS
NASA	NASA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
-	-	kIx~
Nejdůvěryhodnější	důvěryhodný	k2eAgInSc1d3
zpravodajský	zpravodajský	k2eAgInSc1d1
web	web	k1gInSc1
v	v	k7c6
ČR	ČR	kA
-	-	kIx~
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Helikoptéra	helikoptéra	k1gFnSc1
Ingenuity	Ingenuita	k1gFnSc2
přečkala	přečkat	k5eAaPmAgFnS
první	první	k4xOgFnSc1
noc	noc	k1gFnSc1
na	na	k7c6
Marsu	Mars	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
chladem	chlad	k1gInSc7
ji	on	k3xPp3gFnSc4
chránila	chránit	k5eAaImAgFnS
izolace	izolace	k1gFnSc1
a	a	k8xC
energie	energie	k1gFnSc1
v	v	k7c6
bateriích	baterie	k1gFnPc6
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Mars	Mars	k1gInSc1
Helicopter	Helicoptra	k1gFnPc2
Ingenuity	Ingenuita	k1gFnSc2
snaps	snaps	k6eAd1
1	#num#	k4
<g/>
st	st	kA
color	color	k1gInSc1
photo	photo	k1gNnSc1
on	on	k3xPp3gMnSc1
Red	Red	k1gMnSc1
Planet	planeta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Space	Space	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
NASA	NASA	kA
odložila	odložit	k5eAaPmAgFnS
let	let	k1gInSc4
vrtulníku	vrtulník	k1gInSc2
Ingenuity	Ingenuita	k1gFnSc2
na	na	k7c6
Marsu	Mars	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odstartuje	odstartovat	k5eAaPmIp3nS
nejdříve	dříve	k6eAd3
ve	v	k7c6
středu	střed	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT	ČT	kA
24	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
TELEVIZE	televize	k1gFnSc1
<g/>
,	,	kIx,
Česká	český	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrtulníček	vrtulníček	k1gInSc1
Ingenuity	Ingenuita	k1gFnSc2
na	na	k7c6
Marsu	Mars	k1gInSc6
vzlétl	vzlétnout	k5eAaPmAgMnS
i	i	k8xC
přistál	přistát	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
NASA	NASA	kA
slaví	slavit	k5eAaImIp3nS
úspěch	úspěch	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
-	-	kIx~
Nejdůvěryhodnější	důvěryhodný	k2eAgInSc1d3
zpravodajský	zpravodajský	k2eAgInSc1d1
web	web	k1gInSc1
v	v	k7c6
ČR	ČR	kA
-	-	kIx~
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Historický	historický	k2eAgInSc1d1
průlom	průlom	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Helikoptéra	helikoptéra	k1gFnSc1
Ingenuity	Ingenuita	k1gFnSc2
na	na	k7c6
Marsu	Mars	k1gInSc6
vzletěla	vzletět	k5eAaPmAgFnS
a	a	k8xC
úspěšně	úspěšně	k6eAd1
přistála	přistát	k5eAaImAgFnS,k5eAaPmAgFnS
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
VIDEO	video	k1gNnSc1
<g/>
:	:	kIx,
Helikoptéra	helikoptéra	k1gFnSc1
Ingenuity	Ingenuita	k1gFnSc2
přepisuje	přepisovat	k5eAaImIp3nS
dějiny	dějiny	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
NASA	NASA	kA
testuje	testovat	k5eAaImIp3nS
první	první	k4xOgInSc1
řízený	řízený	k2eAgInSc1d1
motorický	motorický	k2eAgInSc1d1
let	let	k1gInSc1
na	na	k7c6
Marsu	Mars	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radiožurnál	radiožurnál	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2021-04-19	2021-04-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
APRIL	APRIL	kA
2021	#num#	k4
<g/>
,	,	kIx,
Mike	Mike	k1gFnSc1
Wall	Wall	k1gInSc1
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
NASA	NASA	kA
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Mars	Mars	k1gMnSc1
helicopter	helicopter	k1gMnSc1
Ingenuity	Ingenuita	k1gFnSc2
takes	takes	k1gMnSc1
off	off	k?
on	on	k3xPp3gMnSc1
historic	historic	k1gMnSc1
1	#num#	k4
<g/>
st	st	kA
powered	powered	k1gMnSc1
flight	flight	k1gMnSc1
on	on	k3xPp3gMnSc1
another	anothra	k1gFnPc2
world	worldo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Space	Space	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
NASA	NASA	kA
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Ingenuity	Ingenuita	k1gMnPc7
helicopter	helicopter	k1gMnSc1
makes	makes	k1gMnSc1
historic	historic	k1gMnSc1
first	first	k1gMnSc1
flight	flight	k5eAaPmF
on	on	k3xPp3gMnSc1
Mars	Mars	k1gMnSc1
<g/>
.	.	kIx.
www.abc.net.au	www.abc.net.au	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2021-04-19	2021-04-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
NORTHON	NORTHON	kA
<g/>
,	,	kIx,
Karen	Karen	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
NASA	NASA	kA
<g/>
’	’	k?
<g/>
s	s	k7c7
Ingenuity	Ingenuita	k1gMnPc7
Mars	Mars	k1gMnSc1
Helicopter	Helicopter	k1gMnSc1
Succeeds	Succeedsa	k1gFnPc2
in	in	k?
Historic	Historic	k1gMnSc1
First	First	k1gMnSc1
Flight	Flight	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
NASA	NASA	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2021-04-19	2021-04-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
STEPHEN	STEPHEN	kA
<g/>
,	,	kIx,
Bijan	bijan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Linux	linux	k1gInSc1
has	hasit	k5eAaImRp2nS
made	made	k1gInSc1
it	it	k?
to	ten	k3xDgNnSc4
Mars	Mars	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Verge	Verge	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2021-02-19	2021-02-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
JEŽEK	Ježek	k1gMnSc1
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrtulníček	vrtulníček	k1gInSc1
Ingenuity	Ingenuita	k1gFnSc2
na	na	k7c6
Marsu	Mars	k1gInSc6
pohání	pohánět	k5eAaImIp3nS
Linux	linux	k1gInSc1
<g/>
,	,	kIx,
dláždí	dláždit	k5eAaImIp3nS
cestu	cesta	k1gFnSc4
jako	jako	k9
kdysi	kdysi	k6eAd1
Pathfinder	Pathfinder	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Root	Root	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
CNN	CNN	kA
<g/>
,	,	kIx,
Ashley	Ashlea	k1gFnSc2
Strickland	Stricklanda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ingenuity	Ingenuita	k1gFnSc2
helicopter	helicoptrum	k1gNnPc2
phones	phones	k1gMnSc1
home	home	k1gNnSc2
from	from	k1gMnSc1
Mars	Mars	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
CNN	CNN	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
Full	Fulla	k1gFnPc2
Page	Page	k1gFnPc2
Reload	Reload	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
IEEE	IEEE	kA
Spectrum	Spectrum	k1gNnSc1
<g/>
:	:	kIx,
Technology	technolog	k1gMnPc4
<g/>
,	,	kIx,
Engineering	Engineering	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
Science	Science	k1gFnSc1
News	News	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
HTTPS://JPL.NASA.GOV	HTTPS://JPL.NASA.GOV	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
NASA	NASA	kA
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Mars	Mars	k1gInSc1
Helicopter	Helicopter	k1gInSc1
Testing	Testing	k1gInSc1
Enters	Enters	k1gInSc1
Final	Final	k1gInSc4
Phase	Phase	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
NASA	NASA	kA
Jet	jet	k2eAgInSc1d1
Propulsion	Propulsion	k1gInSc1
Laboratory	Laborator	k1gInPc1
(	(	kIx(
<g/>
JPL	JPL	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HTTPS://JPL.NASA.GOV	HTTPS://JPL.NASA.GOV	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
NASA	NASA	kA
<g/>
’	’	k?
<g/>
s	s	k7c7
Mars	Mars	k1gMnSc1
Helicopter	Helicopter	k1gMnSc1
Reports	Reportsa	k1gFnPc2
In	In	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
NASA	NASA	kA
Jet	jet	k2eAgInSc1d1
Propulsion	Propulsion	k1gInSc1
Laboratory	Laborator	k1gInPc1
(	(	kIx(
<g/>
JPL	JPL	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
NORTHON	NORTHON	kA
<g/>
,	,	kIx,
Karen	Karen	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mars	Mars	k1gMnSc1
Helicopter	Helicopter	k1gMnSc1
to	ten	k3xDgNnSc4
Fly	Fly	k1gMnSc1
on	on	k3xPp3gMnSc1
NASA	NASA	kA
<g/>
’	’	k?
<g/>
s	s	k7c7
Next	Next	k1gMnSc1
Red	Red	k1gMnSc1
Planet	planeta	k1gFnPc2
Rover	rover	k1gMnSc1
Mission	Mission	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
NASA	NASA	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-05-11	2018-05-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
GREICIUS	GREICIUS	kA
<g/>
,	,	kIx,
Tony	Tony	k1gMnSc1
<g/>
.	.	kIx.
6	#num#	k4
Things	Thingsa	k1gFnPc2
to	ten	k3xDgNnSc4
Know	Know	k1gFnSc7
About	About	k1gMnSc1
NASA	NASA	kA
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Ingenuity	Ingenuita	k1gMnPc7
Mars	Mars	k1gMnSc1
Helicopter	Helicopter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
NASA	NASA	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-07-14	2020-07-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
GRUSH	GRUSH	kA
<g/>
,	,	kIx,
Loren	Lorna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
NASA	NASA	kA
is	is	k?
sending	sending	k1gInSc1
a	a	k8xC
helicopter	helicopter	k1gInSc1
to	ten	k3xDgNnSc4
Mars	Mars	k1gInSc1
to	ten	k3xDgNnSc4
get	get	k?
a	a	k8xC
bird	bird	k1gInSc1
<g/>
’	’	k?
<g/>
s-eye	s-eye	k1gInSc1
view	view	k?
of	of	k?
the	the	k?
planet	planeta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Verge	Verge	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-05-11	2018-05-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Veitasium	Veitasium	k1gNnSc1
-	-	kIx~
YouTube	YouTub	k1gInSc5
-	-	kIx~
První	první	k4xOgInSc1
let	let	k1gInSc1
na	na	k7c6
jiné	jiný	k2eAgFnSc6d1
planetě	planeta	k1gFnSc6
<g/>
!	!	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
překlad	překlad	k1gInSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
↑	↑	k?
HTTPS://JPL.NASA.GOV	HTTPS://JPL.NASA.GOV	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
NASA	NASA	kA
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Mars	Mars	k1gMnSc1
Helicopter	Helicopter	k1gMnSc1
Completes	Completes	k1gMnSc1
Flight	Flight	k2eAgInSc4d1
Tests	Tests	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
NASA	NASA	kA
Jet	jet	k2eAgInSc1d1
Propulsion	Propulsion	k1gInSc1
Laboratory	Laborator	k1gInPc1
(	(	kIx(
<g/>
JPL	JPL	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MARS	Mars	k1gMnSc1
<g/>
.	.	kIx.
<g/>
NASA	NASA	kA
<g/>
.	.	kIx.
<g/>
GOV	GOV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Meet	Meet	k2eAgInSc1d1
the	the	k?
Martians	Martians	k1gInSc1
<g/>
.	.	kIx.
mars	mars	k1gInSc1
<g/>
.	.	kIx.
<g/>
nasa	nasa	k1gFnSc1
<g/>
.	.	kIx.
<g/>
gov	gov	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
TELEVIZE	televize	k1gFnSc1
<g/>
,	,	kIx,
Česká	český	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrtulníček	vrtulníček	k1gInSc1
Ingenuity	Ingenuita	k1gFnSc2
na	na	k7c6
Marsu	Mars	k1gInSc6
vzlétne	vzlétnout	k5eAaPmIp3nS
na	na	k7c6
začátku	začátek	k1gInSc6
dubna	duben	k1gInSc2
<g/>
,	,	kIx,
oznámila	oznámit	k5eAaPmAgFnS
NASA	NASA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
-	-	kIx~
Nejdůvěryhodnější	důvěryhodný	k2eAgInSc1d3
zpravodajský	zpravodajský	k2eAgInSc1d1
web	web	k1gInSc1
v	v	k7c6
ČR	ČR	kA
-	-	kIx~
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Nasa	Nasa	k1gFnSc1
to	ten	k3xDgNnSc4
fly	fly	k?
Ingenuity	Ingenuita	k1gFnSc2
Mars	Mars	k1gMnSc1
helicopter	helicopter	k1gMnSc1
in	in	k?
early	earl	k1gMnPc4
April	April	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
News	Newsa	k1gFnPc2
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MARCH	MARCH	kA
2021	#num#	k4
<g/>
,	,	kIx,
Mike	Mike	k1gFnSc1
Wall	Wall	k1gInSc1
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mars	Mars	k1gInSc1
helicopter	helicopter	k1gInSc4
Ingenuity	Ingenuita	k1gFnSc2
gets	gets	k6eAd1
1	#num#	k4
<g/>
st	st	kA
taste	tasit	k5eAaPmRp2nP
of	of	k?
Red	Red	k1gFnSc3
Planet	planeta	k1gFnPc2
air	air	k?
(	(	kIx(
<g/>
video	video	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Space	Space	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
CNN	CNN	kA
<g/>
,	,	kIx,
Ashley	Ashlea	k1gFnSc2
Strickland	Stricklanda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ingenuity	Ingenuit	k1gInPc7
Mars	Mars	k1gMnSc1
helicopter	helicopter	k1gMnSc1
prepares	prepares	k1gMnSc1
for	forum	k1gNnPc2
the	the	k?
first	first	k1gMnSc1
flight	flight	k1gMnSc1
on	on	k3xPp3gMnSc1
another	anothra	k1gFnPc2
planet	planeta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
CNN	CNN	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historický	historický	k2eAgInSc1d1
milník	milník	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
NASA	NASA	kA
stanovila	stanovit	k5eAaPmAgFnS
datum	datum	k1gNnSc4
letu	let	k1gInSc2
helikoptéry	helikoptéra	k1gFnSc2
Ingenuity	Ingenuita	k1gFnSc2
na	na	k7c4
Mars	Mars	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Everything	Everything	k1gInSc1
to	ten	k3xDgNnSc1
know	know	k?
about	about	k1gInSc1
NASA	NASA	kA
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Mars	Mars	k1gInSc1
Ingenuity	Ingenuita	k1gFnSc2
helicopter	helicopter	k1gMnSc1
—	—	k?
the	the	k?
first	first	k1gFnSc1
to	ten	k3xDgNnSc4
fly	fly	k?
on	on	k3xPp3gMnSc1
another	anothra	k1gFnPc2
planet	planeta	k1gFnPc2
<g/>
.	.	kIx.
www.cbsnews.com	www.cbsnews.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
When	When	k1gMnSc1
will	will	k1gMnSc1
the	the	k?
Ingenuity	Ingenuita	k1gFnSc2
Mars	Mars	k1gMnSc1
helicopter	helicopter	k1gMnSc1
fly	fly	k?
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
Science	Science	k1gFnSc1
Focus	Focus	k1gInSc1
Magazine	Magazin	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
NASA	NASA	kA
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Mars	Mars	k1gInSc1
2020	#num#	k4
helicopter	helicoptra	k1gFnPc2
is	is	k?
named	named	k1gMnSc1
'	'	kIx"
<g/>
Ingenuity	Ingenuita	k1gMnSc2
<g/>
'	'	kIx"
by	by	kYmCp3nS
Alabama	Alabama	k1gFnSc1
high	high	k1gInSc4
school	school	k1gInSc1
teen	teen	k1gInSc1
-	-	kIx~
Sports	Sports	k1gInSc1
News	News	k1gInSc1
,	,	kIx,
Firstpost	Firstpost	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Firstpost	Firstpost	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-05-01	2020-05-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
NASA	NASA	kA
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Ingenuity	Ingenuita	k1gMnPc7
Mars	Mars	k1gMnSc1
Helicopter	Helicopter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
American	American	k1gMnSc1
Scientist	Scientist	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-10-07	2020-10-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Keck	Kecka	k1gFnPc2
-	-	kIx~
Mars	Mars	k1gMnSc1
Helicopter	Helicopter	k1gMnSc1
Scout	Scout	k1gMnSc1
<g/>
;	;	kIx,
přednáška	přednáška	k1gFnSc1
na	na	k7c4
Youtube	Youtub	k1gInSc5
-	-	kIx~
MiMi	Mimi	k1gFnSc1
Aung	Aung	k1gMnSc1
(	(	kIx(
<g/>
https://www.youtube.com/watch?v=w3y7iJEe7uM&	https://www.youtube.com/watch?v=w3y7iJEe7uM&	k1gMnSc1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
KLOTZ	KLOTZ	kA
<g/>
,	,	kIx,
Irene	Iren	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
NASA	NASA	kA
<g/>
’	’	k?
<g/>
s	s	k7c7
Ingenuity	Ingenuit	k1gInPc7
<g/>
—	—	k?
<g/>
the	the	k?
First	First	k1gMnSc1
Ever	Ever	k1gMnSc1
Off-World	Off-World	k1gMnSc1
Helicopter	Helicopter	k1gMnSc1
<g/>
—	—	k?
<g/>
Is	Is	k1gMnSc4
Set	set	k1gInSc1
for	forum	k1gNnPc2
a	a	k8xC
‘	‘	k?
<g/>
Wright	Wright	k1gInSc1
Brothers	Brothers	k1gInSc1
Moment	moment	k1gInSc1
<g/>
’	’	k?
on	on	k3xPp3gMnSc1
Mars	Mars	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Scientific	Scientific	k1gMnSc1
American	American	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Mars	Mars	k1gInSc1
</s>
<s>
Perseverance	perseverance	k1gFnSc1
</s>
<s>
NASA	NASA	kA
</s>
<s>
Jet	jet	k2eAgInSc1d1
Propulsion	Propulsion	k1gInSc1
Laboratory	Laborator	k1gInPc1
</s>
<s>
Vrtulník	vrtulník	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Ingenuity	Ingenuita	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Ingenuity	Ingenuita	k1gMnSc2
Mars	Mars	k1gMnSc1
Helicopter	Helicopter	k1gMnSc1
-	-	kIx~
Landing	Landing	k1gInSc1
Press	Press	k1gInSc1
Kit	kit	k1gInSc4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Kosmonautika	kosmonautika	k1gFnSc1
|	|	kIx~
Letectví	letectví	k1gNnSc1
</s>
