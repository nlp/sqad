<s>
Lední	lední	k2eAgInSc1d1	lední
hokej	hokej	k1gInSc1	hokej
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c4	na
3	[number]	k4	3
třetiny	třetina	k1gFnSc2	třetina
po	po	k7c6	po
20	[number]	k4	20
minutách	minuta	k1gFnPc6	minuta
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
tedy	tedy	k9	tedy
60	[number]	k4	60
minut	minuta	k1gFnPc2	minuta
čistého	čistý	k2eAgInSc2d1	čistý
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
čas	čas	k1gInSc1	čas
se	se	k3xPyFc4	se
při	při	k7c6	při
přerušení	přerušení	k1gNnSc6	přerušení
zastavuje	zastavovat	k5eAaImIp3nS	zastavovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
