<p>
<s>
Tavoliere	Tavolirat	k5eAaPmIp3nS	Tavolirat
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
nížina	nížina	k1gFnSc1	nížina
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
v	v	k7c6	v
Apulii	Apulie	k1gFnSc6	Apulie
<g/>
,	,	kIx,	,
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Foggia	Foggium	k1gNnSc2	Foggium
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
více	hodně	k6eAd2	hodně
než	než	k8xS	než
3	[number]	k4	3
000	[number]	k4	000
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
složena	složit	k5eAaPmNgFnS	složit
z	z	k7c2	z
mořských	mořský	k2eAgInPc2d1	mořský
pliocenních	pliocenní	k2eAgInPc2d1	pliocenní
a	a	k8xC	a
čtvrtohorních	čtvrtohorní	k2eAgInPc2d1	čtvrtohorní
nánosů	nános	k1gInPc2	nános
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejrozlehlejší	rozlehlý	k2eAgFnSc4d3	nejrozlehlejší
nížinu	nížina	k1gFnSc4	nížina
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
po	po	k7c6	po
Pádské	pádský	k2eAgFnSc6d1	Pádská
nížině	nížina	k1gFnSc6	nížina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
a	a	k8xC	a
podnebí	podnebí	k1gNnSc2	podnebí
==	==	k?	==
</s>
</p>
<p>
<s>
Oblast	oblast	k1gFnSc1	oblast
je	být	k5eAaImIp3nS	být
ohraničena	ohraničit	k5eAaPmNgFnS	ohraničit
řekou	řeka	k1gFnSc7	řeka
Fortore	Fortor	k1gMnSc5	Fortor
na	na	k7c6	na
severu	sever	k1gInSc2	sever
a	a	k8xC	a
řekou	řeka	k1gFnSc7	řeka
Ofanto	Ofanto	k1gNnSc4	Ofanto
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
leží	ležet	k5eAaImIp3nS	ležet
pohoří	pohoří	k1gNnSc1	pohoří
Monte	Mont	k1gInSc5	Mont
del	del	k?	del
Daunia	Daunium	k1gNnSc2	Daunium
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
kampánských	kampánský	k2eAgFnPc2d1	kampánský
Subapenin	Subapenina	k1gFnPc2	Subapenina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
tvoří	tvořit	k5eAaImIp3nS	tvořit
hranici	hranice	k1gFnSc4	hranice
poloostrov	poloostrov	k1gInSc1	poloostrov
Gargano	Gargana	k1gFnSc5	Gargana
a	a	k8xC	a
Jaderské	jaderský	k2eAgNnSc4d1	Jaderské
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
Tavoliere	Tavolirat	k5eAaPmIp3nS	Tavolirat
je	být	k5eAaImIp3nS	být
nejsušším	suchý	k2eAgNnSc7d3	nejsušší
územím	území	k1gNnSc7	území
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Srážek	srážka	k1gFnPc2	srážka
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
než	než	k8xS	než
300	[number]	k4	300
mm	mm	kA	mm
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Oblast	oblast	k1gFnSc1	oblast
je	být	k5eAaImIp3nS	být
využívána	využívat	k5eAaImNgFnS	využívat
především	především	k9	především
k	k	k7c3	k
zemědělské	zemědělský	k2eAgFnSc3d1	zemědělská
produkci	produkce	k1gFnSc3	produkce
<g/>
.	.	kIx.	.
</s>
<s>
Pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
pšenice	pšenice	k1gFnSc2	pšenice
<g/>
,	,	kIx,	,
červená	červený	k2eAgFnSc1d1	červená
řepa	řepa	k1gFnSc1	řepa
<g/>
,	,	kIx,	,
rajčata	rajče	k1gNnPc1	rajče
<g/>
,	,	kIx,	,
olivy	oliva	k1gFnPc1	oliva
a	a	k8xC	a
vinná	vinný	k2eAgFnSc1d1	vinná
réva	réva	k1gFnSc1	réva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
