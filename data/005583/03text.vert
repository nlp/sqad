<s>
Neurotoxin	Neurotoxin	k1gInSc1	Neurotoxin
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
jedu	jed	k1gInSc2	jed
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
negativně	negativně	k6eAd1	negativně
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
nervový	nervový	k2eAgInSc4d1	nervový
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
často	často	k6eAd1	často
smrtelně	smrtelně	k6eAd1	smrtelně
nebezpečné	bezpečný	k2eNgInPc1d1	nebezpečný
jedy	jed	k1gInPc1	jed
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
využívají	využívat	k5eAaImIp3nP	využívat
některé	některý	k3yIgInPc1	některý
organismy	organismus	k1gInPc1	organismus
k	k	k7c3	k
obraně	obrana	k1gFnSc3	obrana
nebo	nebo	k8xC	nebo
útoku	útok	k1gInSc3	útok
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
bakteriemi	bakterie	k1gFnPc7	bakterie
má	mít	k5eAaImIp3nS	mít
velice	velice	k6eAd1	velice
silný	silný	k2eAgInSc1d1	silný
neurotoxin	neurotoxin	k1gInSc1	neurotoxin
Clostridium	Clostridium	k1gNnSc1	Clostridium
botulinum	botulinum	k1gInSc1	botulinum
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
může	moct	k5eAaImIp3nS	moct
při	při	k7c6	při
nesprávné	správný	k2eNgFnSc3d1	nesprávná
výrobě	výroba	k1gFnSc3	výroba
nebo	nebo	k8xC	nebo
přípravě	příprava	k1gFnSc3	příprava
osídlit	osídlit	k5eAaPmF	osídlit
konzervy	konzerva	k1gFnPc4	konzerva
nebo	nebo	k8xC	nebo
domácí	domácí	k2eAgFnPc4d1	domácí
zavařeniny	zavařenina	k1gFnPc4	zavařenina
a	a	k8xC	a
vytvářet	vytvářet	k5eAaImF	vytvářet
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
botulotoxin	botulotoxin	k1gInSc1	botulotoxin
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
i	i	k9	i
v	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
množství	množství	k1gNnSc6	množství
smrtící	smrtící	k2eAgMnSc1d1	smrtící
(	(	kIx(	(
<g/>
smrt	smrt	k1gFnSc4	smrt
způsobí	způsobit	k5eAaPmIp3nS	způsobit
rychle	rychle	k6eAd1	rychle
ochrnutím	ochrnutí	k1gNnSc7	ochrnutí
dýchacích	dýchací	k2eAgInPc2d1	dýchací
svalů	sval	k1gInPc2	sval
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
jedu	jed	k1gInSc3	jed
botulotoxinu	botulotoxin	k1gInSc2	botulotoxin
existuje	existovat	k5eAaImIp3nS	existovat
serum	serum	k1gInSc1	serum
antibotulinum	antibotulinum	k1gInSc1	antibotulinum
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
pavouky	pavouk	k1gMnPc7	pavouk
má	mít	k5eAaImIp3nS	mít
neurotoxin	urotoxin	k2eNgInSc1d1	neurotoxin
např.	např.	kA	např.
Atrax	Atrax	k1gInSc1	Atrax
robustus	robustus	k1gInSc1	robustus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
vzdáleným	vzdálený	k2eAgMnPc3d1	vzdálený
příbuzným	příbuzný	k1gMnPc3	příbuzný
běžně	běžně	k6eAd1	běžně
chovaných	chovaný	k2eAgMnPc2d1	chovaný
sklípkanů	sklípkan	k1gMnPc2	sklípkan
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
u	u	k7c2	u
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
však	však	k9	však
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
několik	několik	k4yIc1	několik
rodů	rod	k1gInPc2	rod
disponujících	disponující	k2eAgInPc2d1	disponující
neurotoxinem	neurotoxino	k1gNnSc7	neurotoxino
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
slabším	slabý	k2eAgMnPc3d2	slabší
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebezpečným	bezpečný	k2eNgMnSc7d1	nebezpečný
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
např.	např.	kA	např.
o	o	k7c4	o
rod	rod	k1gInSc4	rod
Poecilotheria	Poecilotherium	k1gNnSc2	Poecilotherium
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
jed	jed	k1gInSc1	jed
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
ochrnutí	ochrnutí	k1gNnSc4	ochrnutí
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
svalů	sval	k1gInPc2	sval
<g/>
.	.	kIx.	.
</s>
<s>
Žádný	žádný	k3yNgMnSc1	žádný
z	z	k7c2	z
pravých	pravý	k2eAgMnPc2d1	pravý
sklípkanů	sklípkan	k1gMnPc2	sklípkan
smrt	smrt	k1gFnSc4	smrt
člověka	člověk	k1gMnSc2	člověk
nikdy	nikdy	k6eAd1	nikdy
nezpůsobil	způsobit	k5eNaPmAgInS	způsobit
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
druhů	druh	k1gInPc2	druh
štírů	štír	k1gMnPc2	štír
se	se	k3xPyFc4	se
také	také	k9	také
pyšní	pyšnit	k5eAaImIp3nS	pyšnit
neurotoxickým	urotoxický	k2eNgInSc7d1	neurotoxický
jedem	jed	k1gInSc7	jed
<g/>
.	.	kIx.	.
</s>
<s>
Nejnebezpečnějším	bezpečný	k2eNgMnSc7d3	nejnebezpečnější
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jsou	být	k5eAaImIp3nP	být
Androctonus	Androctonus	k1gInSc4	Androctonus
australis	australis	k1gFnSc2	australis
a	a	k8xC	a
Leiurus	Leiurus	k1gMnSc1	Leiurus
quinquestriatus	quinquestriatus	k1gMnSc1	quinquestriatus
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
silný	silný	k2eAgInSc1d1	silný
jed	jed	k1gInSc1	jed
může	moct	k5eAaImIp3nS	moct
dokonce	dokonce	k9	dokonce
zabít	zabít	k5eAaPmF	zabít
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Neurotoxiny	Neurotoxina	k1gFnPc4	Neurotoxina
využívá	využívat	k5eAaImIp3nS	využívat
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
plazů	plaz	k1gInPc2	plaz
například	například	k6eAd1	například
kobra	kobra	k1gFnSc1	kobra
královská	královský	k2eAgFnSc1d1	královská
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
rybami	ryba	k1gFnPc7	ryba
jej	on	k3xPp3gMnSc4	on
využívá	využívat	k5eAaPmIp3nS	využívat
například	například	k6eAd1	například
čtverzubci	čtverzubce	k1gMnPc1	čtverzubce
<g/>
,	,	kIx,	,
známí	známý	k2eAgMnPc1d1	známý
z	z	k7c2	z
japonské	japonský	k2eAgFnSc2d1	japonská
kuchyně	kuchyně	k1gFnSc2	kuchyně
jako	jako	k8xS	jako
fugu	fuga	k1gFnSc4	fuga
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
neurotoxin	urotoxin	k2eNgInSc4d1	neurotoxin
tetrodotoxin	tetrodotoxin	k1gInSc4	tetrodotoxin
a	a	k8xC	a
smrt	smrt	k1gFnSc4	smrt
nastává	nastávat	k5eAaImIp3nS	nastávat
při	při	k7c6	při
paralýze	paralýza	k1gFnSc6	paralýza
dýchacích	dýchací	k2eAgInPc2d1	dýchací
svalů	sval	k1gInPc2	sval
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
obojživelníky	obojživelník	k1gMnPc7	obojživelník
využívají	využívat	k5eAaImIp3nP	využívat
smrtelný	smrtelný	k2eAgInSc1d1	smrtelný
neurotoxin	neurotoxin	k1gInSc1	neurotoxin
žáby	žába	k1gFnSc2	žába
pralesničky	pralesnička	k1gFnSc2	pralesnička
(	(	kIx(	(
<g/>
Dendrobatidae	Dendrobatidae	k1gInSc1	Dendrobatidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
žáby	žába	k1gFnPc1	žába
se	se	k3xPyFc4	se
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
stávají	stávat	k5eAaImIp3nP	stávat
neškodnými	škodný	k2eNgNnPc7d1	neškodné
<g/>
,	,	kIx,	,
svůj	svůj	k3xOyFgInSc4	svůj
jed	jed	k1gInSc4	jed
totiž	totiž	k9	totiž
získávají	získávat	k5eAaImIp3nP	získávat
z	z	k7c2	z
mravenců	mravenec	k1gMnPc2	mravenec
<g/>
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
neonikotinoidy	neonikotinoida	k1gFnSc2	neonikotinoida
"	"	kIx"	"
<g/>
Opioidy	Opioida	k1gFnSc2	Opioida
indukovaná	indukovaný	k2eAgFnSc1d1	indukovaná
toxicita	toxicita	k1gFnSc1	toxicita
nastává	nastávat	k5eAaImIp3nS	nastávat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
látky	látka	k1gFnPc1	látka
při	při	k7c6	při
svém	svůj	k3xOyFgNnSc6	svůj
odbourávání	odbourávání	k1gNnSc6	odbourávání
v	v	k7c6	v
játrech	játra	k1gNnPc6	játra
uvolňují	uvolňovat	k5eAaImIp3nP	uvolňovat
toxické	toxický	k2eAgInPc1d1	toxický
metabolity	metabolit	k1gInPc1	metabolit
<g/>
.	.	kIx.	.
</s>
<s>
Neurotoxicita	Neurotoxicita	k1gFnSc1	Neurotoxicita
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vyvolána	vyvolat	k5eAaPmNgFnS	vyvolat
oxidací	oxidace	k1gFnSc7	oxidace
dvojné	dvojný	k2eAgFnSc2d1	dvojná
vazby	vazba	k1gFnSc2	vazba
v	v	k7c6	v
chemické	chemický	k2eAgFnSc6d1	chemická
struktuře	struktura	k1gFnSc6	struktura
opioidu	opioid	k1gInSc2	opioid
a	a	k8xC	a
na	na	k7c6	na
základě	základ	k1gInSc6	základ
toho	ten	k3xDgNnSc2	ten
mohou	moct	k5eAaImIp3nP	moct
vznikat	vznikat	k5eAaImF	vznikat
volné	volný	k2eAgInPc1d1	volný
radikály	radikál	k1gInPc1	radikál
poškozující	poškozující	k2eAgFnSc4d1	poškozující
tkáň	tkáň	k1gFnSc4	tkáň
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Odmašťování	odmašťování	k1gNnSc1	odmašťování
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
čištění	čištění	k1gNnSc2	čištění
oděvů	oděv	k1gInPc2	oděv
<g/>
,	,	kIx,	,
součást	součást	k1gFnSc4	součást
ředidel	ředidlo	k1gNnPc2	ředidlo
[[	[[	k?	[[
<g/>
1	[number]	k4	1
<g/>
]]	]]	k?	]]
-	-	kIx~	-
etylbenzen	etylbenzen	k2eAgInSc1d1	etylbenzen
-	-	kIx~	-
sirouhlík	sirouhlík	k1gInSc1	sirouhlík
-	-	kIx~	-
thujon	thujon	k1gInSc1	thujon
-	-	kIx~	-
methylrtuť	methylrtuť	k1gFnSc1	methylrtuť
-	-	kIx~	-
diethylrtuť	diethylrtuť	k1gFnSc1	diethylrtuť
-	-	kIx~	-
dimethylrtuť	dimethylrtuť	k1gFnSc1	dimethylrtuť
</s>
