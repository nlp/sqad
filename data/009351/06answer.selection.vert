<s>
České	český	k2eAgNnSc1d1	české
vysoké	vysoký	k2eAgNnSc1d1	vysoké
učení	učení	k1gNnSc1	učení
technické	technický	k2eAgNnSc1d1	technické
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
ČVUT	ČVUT	kA	ČVUT
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
největším	veliký	k2eAgFnPc3d3	veliký
a	a	k8xC	a
nejstarším	starý	k2eAgFnPc3d3	nejstarší
technickým	technický	k2eAgFnPc3d1	technická
vysokým	vysoký	k2eAgFnPc3d1	vysoká
školám	škola	k1gFnPc3	škola
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
