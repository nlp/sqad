<p>
<s>
České	český	k2eAgNnSc1d1	české
vysoké	vysoký	k2eAgNnSc1d1	vysoké
učení	učení	k1gNnSc1	učení
technické	technický	k2eAgNnSc1d1	technické
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
ČVUT	ČVUT	kA	ČVUT
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
největším	veliký	k2eAgFnPc3d3	veliký
a	a	k8xC	a
nejstarším	starý	k2eAgFnPc3d3	nejstarší
technickým	technický	k2eAgFnPc3d1	technická
vysokým	vysoký	k2eAgFnPc3d1	vysoká
školám	škola	k1gFnPc3	škola
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
bylo	být	k5eAaImAgNnS	být
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
nejlepší	dobrý	k2eAgFnSc7d3	nejlepší
technickou	technický	k2eAgFnSc7d1	technická
univerzitou	univerzita	k1gFnSc7	univerzita
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
má	mít	k5eAaImIp3nS	mít
ČVUT	ČVUT	kA	ČVUT
osm	osm	k4xCc4	osm
fakult	fakulta	k1gFnPc2	fakulta
a	a	k8xC	a
studuje	studovat	k5eAaImIp3nS	studovat
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
přes	přes	k7c4	přes
19	[number]	k4	19
tisíc	tisíc	k4xCgInPc2	tisíc
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
akademickém	akademický	k2eAgInSc6d1	akademický
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
<g/>
/	/	kIx~	/
<g/>
18	[number]	k4	18
nabízelo	nabízet	k5eAaImAgNnS	nabízet
ČVUT	ČVUT	kA	ČVUT
svým	svůj	k3xOyFgInSc7	svůj
studentům	student	k1gMnPc3	student
128	[number]	k4	128
studijních	studijní	k2eAgInPc2d1	studijní
programů	program	k1gInPc2	program
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
jazyce	jazyk	k1gInSc6	jazyk
a	a	k8xC	a
87	[number]	k4	87
programů	program	k1gInPc2	program
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Počátky	počátek	k1gInPc4	počátek
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1705	[number]	k4	1705
požádal	požádat	k5eAaPmAgMnS	požádat
Christian	Christian	k1gMnSc1	Christian
Josef	Josef	k1gMnSc1	Josef
Willenberg	Willenberg	k1gMnSc1	Willenberg
císaře	císař	k1gMnSc2	císař
Leopolda	Leopold	k1gMnSc2	Leopold
I.	I.	kA	I.
o	o	k7c4	o
souhlas	souhlas	k1gInSc4	souhlas
vyučovat	vyučovat	k5eAaImF	vyučovat
"	"	kIx"	"
<g/>
ingenieurský	ingenieurský	k2eAgInSc4d1	ingenieurský
kunst	kunst	k1gInSc4	kunst
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
císařův	císařův	k2eAgMnSc1d1	císařův
syn	syn	k1gMnSc1	syn
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ho	on	k3xPp3gMnSc4	on
na	na	k7c6	na
trůnu	trůn	k1gInSc6	trůn
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1707	[number]	k4	1707
reskriptem	reskript	k1gInSc7	reskript
nařídil	nařídit	k5eAaPmAgMnS	nařídit
českým	český	k2eAgInPc3d1	český
stavům	stav	k1gInPc3	stav
zajistit	zajistit	k5eAaPmF	zajistit
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
inženýrskou	inženýrský	k2eAgFnSc4d1	inženýrská
výuku	výuka	k1gFnSc4	výuka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
důvodů	důvod	k1gInPc2	důvod
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
;	;	kIx,	;
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1716	[number]	k4	1716
tedy	tedy	k8xC	tedy
Willenberg	Willenberg	k1gInSc1	Willenberg
svou	svůj	k3xOyFgFnSc4	svůj
žádost	žádost	k1gFnSc4	žádost
zopakoval	zopakovat	k5eAaPmAgMnS	zopakovat
(	(	kIx(	(
<g/>
českým	český	k2eAgInPc3d1	český
stavům	stav	k1gInPc3	stav
i	i	k9	i
císaři	císař	k1gMnSc3	císař
Karlu	Karel	k1gMnSc3	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
po	po	k7c6	po
císařské	císařský	k2eAgFnSc6d1	císařská
urgenci	urgence	k1gFnSc6	urgence
byla	být	k5eAaImAgFnS	být
konečně	konečně	k6eAd1	konečně
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1717	[number]	k4	1717
dekretem	dekret	k1gInSc7	dekret
českých	český	k2eAgInPc2d1	český
stavů	stav	k1gInPc2	stav
ustanovena	ustanoven	k2eAgFnSc1d1	ustanovena
Willenbergova	Willenbergův	k2eAgFnSc1d1	Willenbergova
profesura	profesura	k1gFnSc1	profesura
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
inženýrská	inženýrský	k2eAgFnSc1d1	inženýrská
profesura	profesura	k1gFnSc1	profesura
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1718	[number]	k4	1718
pak	pak	k6eAd1	pak
začala	začít	k5eAaPmAgFnS	začít
výuka	výuka	k1gFnSc1	výuka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
učil	učít	k5eAaPmAgInS	učít
Willenberg	Willenberg	k1gInSc1	Willenberg
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
bytě	byt	k1gInSc6	byt
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
Mostecké	mostecký	k2eAgFnSc6d1	Mostecká
ulici	ulice	k1gFnSc6	ulice
na	na	k7c6	na
Malé	Malé	k2eAgFnSc6d1	Malé
Straně	strana	k1gFnSc6	strana
pouhých	pouhý	k2eAgMnPc2d1	pouhý
dvanáct	dvanáct	k4xCc1	dvanáct
studentů	student	k1gMnPc2	student
–	–	k?	–
šest	šest	k4xCc1	šest
svobodných	svobodný	k2eAgMnPc2d1	svobodný
pánů	pan	k1gMnPc2	pan
<g/>
,	,	kIx,	,
čtyři	čtyři	k4xCgMnPc4	čtyři
rytíře	rytíř	k1gMnPc4	rytíř
a	a	k8xC	a
dva	dva	k4xCgMnPc4	dva
měšťany	měšťan	k1gMnPc4	měšťan
<g/>
,	,	kIx,	,
každoročně	každoročně	k6eAd1	každoročně
jmenované	jmenovaný	k2eAgFnPc1d1	jmenovaná
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
zemských	zemský	k2eAgInPc2d1	zemský
stavů	stav	k1gInPc2	stav
–	–	k?	–
postupně	postupně	k6eAd1	postupně
však	však	k8xC	však
studentů	student	k1gMnPc2	student
přibývalo	přibývat	k5eAaImAgNnS	přibývat
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1779	[number]	k4	1779
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
kolem	kolem	k6eAd1	kolem
200	[number]	k4	200
<g/>
)	)	kIx)	)
a	a	k8xC	a
výuka	výuka	k1gFnSc1	výuka
se	se	k3xPyFc4	se
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
vhodnějších	vhodný	k2eAgFnPc2d2	vhodnější
prostor	prostora	k1gFnPc2	prostora
na	na	k7c4	na
Staré	Staré	k2eAgNnSc4d1	Staré
Město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
byla	být	k5eAaImAgFnS	být
výuka	výuka	k1gFnSc1	výuka
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
zejména	zejména	k9	zejména
na	na	k7c6	na
vojenství	vojenství	k1gNnSc6	vojenství
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
opevňování	opevňování	k1gNnSc2	opevňování
se	se	k3xPyFc4	se
vyučovalo	vyučovat	k5eAaImAgNnS	vyučovat
zeměměřictví	zeměměřictví	k1gNnSc1	zeměměřictví
<g/>
,	,	kIx,	,
kartografie	kartografie	k1gFnSc1	kartografie
<g/>
,	,	kIx,	,
odvodňování	odvodňování	k1gNnSc1	odvodňování
či	či	k8xC	či
mechanismy	mechanismus	k1gInPc1	mechanismus
zvedání	zvedání	k1gNnSc2	zvedání
těžkých	těžký	k2eAgNnPc2d1	těžké
břemen	břemeno	k1gNnPc2	břemeno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
císařových	císařův	k2eAgFnPc2d1	císařova
motivací	motivace	k1gFnPc2	motivace
pro	pro	k7c4	pro
zavedení	zavedení	k1gNnSc4	zavedení
výuky	výuka	k1gFnSc2	výuka
<g/>
.	.	kIx.	.
</s>
<s>
Výuka	výuka	k1gFnSc1	výuka
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
roce	rok	k1gInSc6	rok
trvala	trvat	k5eAaImAgFnS	trvat
jednu	jeden	k4xCgFnSc4	jeden
hodinu	hodina	k1gFnSc4	hodina
denně	denně	k6eAd1	denně
<g/>
,	,	kIx,	,
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
roce	rok	k1gInSc6	rok
již	již	k6eAd1	již
dvě	dva	k4xCgNnPc1	dva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nástupcem	nástupce	k1gMnSc7	nástupce
prof.	prof.	kA	prof.
Willenberga	Willenberga	k1gFnSc1	Willenberga
byl	být	k5eAaImAgMnS	být
Jan	Jan	k1gMnSc1	Jan
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Schor	Schor	k1gMnSc1	Schor
<g/>
,	,	kIx,	,
stavitel	stavitel	k1gMnSc1	stavitel
vodních	vodní	k2eAgFnPc2d1	vodní
staveb	stavba	k1gFnPc2	stavba
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
Vltavy	Vltava	k1gFnSc2	Vltava
a	a	k8xC	a
autor	autor	k1gMnSc1	autor
na	na	k7c6	na
škole	škola	k1gFnSc6	škola
využívané	využívaný	k2eAgFnPc1d1	využívaná
učebnice	učebnice	k1gFnPc1	učebnice
matematiky	matematika	k1gFnSc2	matematika
<g/>
;	;	kIx,	;
za	za	k7c4	za
jeho	jeho	k3xOp3gNnSc4	jeho
vedení	vedení	k1gNnSc4	vedení
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
vyučovat	vyučovat	k5eAaImF	vyučovat
optika	optika	k1gFnSc1	optika
<g/>
,	,	kIx,	,
perspektiva	perspektiva	k1gFnSc1	perspektiva
<g/>
,	,	kIx,	,
technické	technický	k2eAgNnSc1d1	technické
kreslení	kreslení	k1gNnSc1	kreslení
a	a	k8xC	a
geografie	geografie	k1gFnSc1	geografie
<g/>
.	.	kIx.	.
</s>
<s>
Třetím	třetí	k4xOgMnSc7	třetí
profesorem	profesor	k1gMnSc7	profesor
byl	být	k5eAaImAgMnS	být
František	František	k1gMnSc1	František
Antonín	Antonín	k1gMnSc1	Antonín
Herget	Herget	k1gMnSc1	Herget
<g/>
,	,	kIx,	,
vrchní	vrchní	k2eAgMnSc1d1	vrchní
zemský	zemský	k2eAgMnSc1d1	zemský
stavební	stavební	k2eAgMnSc1d1	stavební
ředitel	ředitel	k1gMnSc1	ředitel
a	a	k8xC	a
významný	významný	k2eAgMnSc1d1	významný
zeměměřič	zeměměřič	k1gMnSc1	zeměměřič
<g/>
;	;	kIx,	;
tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
již	již	k6eAd1	již
škola	škola	k1gFnSc1	škola
zaměřovala	zaměřovat	k5eAaImAgFnS	zaměřovat
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
civilní	civilní	k2eAgNnSc4d1	civilní
inženýrství	inženýrství	k1gNnSc4	inženýrství
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
stavební	stavební	k2eAgFnSc1d1	stavební
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1776	[number]	k4	1776
dala	dát	k5eAaPmAgFnS	dát
Hergetovi	Hergetův	k2eAgMnPc1d1	Hergetův
svolení	svolení	k1gNnSc4	svolení
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
k	k	k7c3	k
využívání	využívání	k1gNnSc3	využívání
Klementina	Klementinum	k1gNnSc2	Klementinum
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
výuky	výuka	k1gFnSc2	výuka
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1786	[number]	k4	1786
se	se	k3xPyFc4	se
Stavovská	stavovský	k2eAgFnSc1d1	stavovská
inženýrská	inženýrský	k2eAgFnSc1d1	inženýrská
škola	škola	k1gFnSc1	škola
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
budovy	budova	k1gFnSc2	budova
bývalého	bývalý	k2eAgInSc2d1	bývalý
jezuitského	jezuitský	k2eAgInSc2d1	jezuitský
Svatováclavského	svatováclavský	k2eAgInSc2d1	svatováclavský
semináře	seminář	k1gInSc2	seminář
v	v	k7c6	v
Dominikánské	dominikánský	k2eAgFnSc6d1	Dominikánská
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc6d1	dnešní
Husově	Husův	k2eAgFnSc6d1	Husova
<g/>
)	)	kIx)	)
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1787	[number]	k4	1787
byla	být	k5eAaImAgFnS	být
škola	škola	k1gFnSc1	škola
na	na	k7c6	na
základě	základ	k1gInSc6	základ
dekretu	dekret	k1gInSc2	dekret
císaře	císař	k1gMnSc2	císař
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
pražskou	pražský	k2eAgFnSc7d1	Pražská
univerzitou	univerzita	k1gFnSc7	univerzita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Polytechnika	polytechnika	k1gFnSc1	polytechnika
–	–	k?	–
německá	německý	k2eAgFnSc1d1	německá
a	a	k8xC	a
česká	český	k2eAgFnSc1d1	Česká
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1798	[number]	k4	1798
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
Gerstner	Gerstner	k1gMnSc1	Gerstner
koncept	koncept	k1gInSc4	koncept
přetvoření	přetvoření	k1gNnSc2	přetvoření
inženýrské	inženýrský	k2eAgFnSc2d1	inženýrská
stavovské	stavovský	k2eAgFnSc2d1	stavovská
školy	škola	k1gFnSc2	škola
na	na	k7c4	na
polytechniku	polytechnika	k1gFnSc4	polytechnika
<g/>
.	.	kIx.	.
</s>
<s>
Inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
jej	on	k3xPp3gNnSc4	on
nedlouho	dlouho	k6eNd1	dlouho
předtím	předtím	k6eAd1	předtím
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
pařížská	pařížský	k2eAgFnSc1d1	Pařížská
École	École	k1gFnSc1	École
polytechnique	polytechniquat	k5eAaPmIp3nS	polytechniquat
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
návrh	návrh	k1gInSc1	návrh
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1803	[number]	k4	1803
císař	císař	k1gMnSc1	císař
František	František	k1gMnSc1	František
I.	I.	kA	I.
schválil	schválit	k5eAaPmAgMnS	schválit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
10	[number]	k4	10
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1806	[number]	k4	1806
nový	nový	k2eAgInSc1d1	nový
Königliche	Königliche	k1gInSc1	Königliche
böhmische	böhmische	k6eAd1	böhmische
ständische	ständischat	k5eAaPmIp3nS	ständischat
technische	technische	k6eAd1	technische
Lehranstalt	Lehranstalt	k2eAgInSc1d1	Lehranstalt
zu	zu	k?	zu
Prag	Prag	k1gInSc1	Prag
(	(	kIx(	(
<g/>
Královské	královský	k2eAgNnSc1d1	královské
české	český	k2eAgNnSc1d1	české
stavovské	stavovský	k2eAgNnSc1d1	Stavovské
technické	technický	k2eAgNnSc1d1	technické
učiliště	učiliště	k1gNnSc1	učiliště
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
zahájilo	zahájit	k5eAaPmAgNnS	zahájit
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
pražská	pražský	k2eAgFnSc1d1	Pražská
škola	škola	k1gFnSc1	škola
stala	stát	k5eAaPmAgFnS	stát
jedinou	jediný	k2eAgFnSc7d1	jediná
institucí	instituce	k1gFnSc7	instituce
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
Rakouském	rakouský	k2eAgNnSc6d1	rakouské
císařství	císařství	k1gNnSc6	císařství
i	i	k8xC	i
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
jelikož	jelikož	k8xS	jelikož
inženýrská	inženýrský	k2eAgFnSc1d1	inženýrská
akademie	akademie	k1gFnSc1	akademie
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
již	již	k6eAd1	již
předtím	předtím	k6eAd1	předtím
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Škola	škola	k1gFnSc1	škola
však	však	k9	však
stále	stále	k6eAd1	stále
oficiálně	oficiálně	k6eAd1	oficiálně
patřila	patřit	k5eAaImAgFnS	patřit
pod	pod	k7c4	pod
pražskou	pražský	k2eAgFnSc4d1	Pražská
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
,	,	kIx,	,
osamostatnila	osamostatnit	k5eAaPmAgFnS	osamostatnit
se	se	k3xPyFc4	se
až	až	k9	až
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1815	[number]	k4	1815
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1863	[number]	k4	1863
schválil	schválit	k5eAaPmAgMnS	schválit
císař	císař	k1gMnSc1	císař
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
Organický	organický	k2eAgInSc4d1	organický
statut	statut	k1gInSc4	statut
Polytechnického	polytechnický	k2eAgInSc2d1	polytechnický
ústavu	ústav	k1gInSc2	ústav
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
reforma	reforma	k1gFnSc1	reforma
polytechniky	polytechnika	k1gFnSc2	polytechnika
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
které	který	k3yIgFnSc2	který
se	se	k3xPyFc4	se
představitelem	představitel	k1gMnSc7	představitel
školy	škola	k1gFnSc2	škola
stal	stát	k5eAaPmAgMnS	stát
volený	volený	k2eAgMnSc1d1	volený
rektor	rektor	k1gMnSc1	rektor
<g/>
.	.	kIx.	.
</s>
<s>
Studenti	student	k1gMnPc1	student
byli	být	k5eAaImAgMnP	být
rozděleni	rozdělit	k5eAaPmNgMnP	rozdělit
do	do	k7c2	do
čtyř	čtyři	k4xCgInPc2	čtyři
odborů	odbor	k1gInPc2	odbor
<g/>
:	:	kIx,	:
pozemní	pozemní	k2eAgNnSc1d1	pozemní
stavitelství	stavitelství	k1gNnSc1	stavitelství
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgNnSc1d1	vodní
a	a	k8xC	a
silniční	silniční	k2eAgNnSc1d1	silniční
stavitelství	stavitelství	k1gNnSc1	stavitelství
<g/>
,	,	kIx,	,
strojnictví	strojnictví	k1gNnSc1	strojnictví
a	a	k8xC	a
technická	technický	k2eAgFnSc1d1	technická
lučba	lučba	k?	lučba
(	(	kIx(	(
<g/>
chemie	chemie	k1gFnSc2	chemie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
rovnoprávný	rovnoprávný	k2eAgInSc4d1	rovnoprávný
vyučovací	vyučovací	k2eAgInSc4d1	vyučovací
jazyk	jazyk	k1gInSc4	jazyk
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
němčině	němčina	k1gFnSc3	němčina
uznána	uznán	k2eAgFnSc1d1	uznána
čeština	čeština	k1gFnSc1	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Rozpory	rozpor	k1gInPc1	rozpor
mezi	mezi	k7c7	mezi
českou	český	k2eAgFnSc7d1	Česká
a	a	k8xC	a
německou	německý	k2eAgFnSc7d1	německá
částí	část	k1gFnSc7	část
profesorstva	profesorstvo	k1gNnSc2	profesorstvo
však	však	k9	však
necelých	celý	k2eNgInPc2d1	necelý
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
poté	poté	k6eAd1	poté
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
rozdělení	rozdělení	k1gNnSc3	rozdělení
na	na	k7c4	na
český	český	k2eAgInSc4d1	český
a	a	k8xC	a
německý	německý	k2eAgInSc4d1	německý
ústav	ústav	k1gInSc4	ústav
(	(	kIx(	(
<g/>
k	k	k7c3	k
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubnu	duben	k1gInSc3	duben
1869	[number]	k4	1869
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Český	český	k2eAgInSc4d1	český
polytechnický	polytechnický	k2eAgInSc4d1	polytechnický
ústav	ústav	k1gInSc4	ústav
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1874	[number]	k4	1874
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
nově	nově	k6eAd1	nově
postavené	postavený	k2eAgFnSc2d1	postavená
budovy	budova	k1gFnSc2	budova
na	na	k7c6	na
Karlově	Karlův	k2eAgNnSc6d1	Karlovo
náměstí	náměstí	k1gNnSc6	náměstí
(	(	kIx(	(
<g/>
architekt	architekt	k1gMnSc1	architekt
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Ignác	Ignác	k1gMnSc1	Ignác
Ullmann	Ullmann	k1gMnSc1	Ullmann
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1875	[number]	k4	1875
byly	být	k5eAaImAgFnP	být
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
zemské	zemský	k2eAgInPc1d1	zemský
ústavy	ústav	k1gInPc1	ústav
zestátněny	zestátněn	k2eAgInPc1d1	zestátněn
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
říšského	říšský	k2eAgInSc2d1	říšský
zákona	zákon	k1gInSc2	zákon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1878	[number]	k4	1878
směli	smět	k5eAaImAgMnP	smět
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
složili	složit	k5eAaPmAgMnP	složit
na	na	k7c6	na
technice	technika	k1gFnSc6	technika
dvě	dva	k4xCgFnPc4	dva
státní	státní	k2eAgFnPc4d1	státní
zkoušky	zkouška	k1gFnPc4	zkouška
<g/>
,	,	kIx,	,
používat	používat	k5eAaImF	používat
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k6eAd1	ještě
stavovské	stavovský	k2eAgNnSc4d1	Stavovské
označení	označení	k1gNnSc4	označení
inženýr	inženýr	k1gMnSc1	inženýr
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1879	[number]	k4	1879
bylo	být	k5eAaImAgNnS	být
škole	škola	k1gFnSc6	škola
dovoleno	dovolit	k5eAaPmNgNnS	dovolit
nazývat	nazývat	k5eAaImF	nazývat
se	se	k3xPyFc4	se
vysokou	vysoký	k2eAgFnSc7d1	vysoká
školou	škola	k1gFnSc7	škola
technickou	technický	k2eAgFnSc7d1	technická
(	(	kIx(	(
<g/>
Česká	český	k2eAgFnSc1d1	Česká
škola	škola	k1gFnSc1	škola
technická	technický	k2eAgFnSc1d1	technická
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
získala	získat	k5eAaPmAgFnS	získat
škola	škola	k1gFnSc1	škola
říšským	říšský	k2eAgInSc7d1	říšský
zákonem	zákon	k1gInSc7	zákon
z	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
právo	právo	k1gNnSc4	právo
udělovat	udělovat	k5eAaImF	udělovat
doktoráty	doktorát	k1gInPc4	doktorát
technických	technický	k2eAgFnPc2d1	technická
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
vydal	vydat	k5eAaPmAgInS	vydat
profesorský	profesorský	k2eAgInSc1d1	profesorský
sbor	sbor	k1gInSc1	sbor
doporučení	doporučení	k1gNnSc2	doporučení
přijímat	přijímat	k5eAaImF	přijímat
ženy	žena	k1gFnPc4	žena
jako	jako	k8xC	jako
řádné	řádný	k2eAgFnPc4d1	řádná
posluchačky	posluchačka	k1gFnPc4	posluchačka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
škola	škola	k1gFnSc1	škola
využívala	využívat	k5eAaImAgFnS	využívat
rozmachu	rozmach	k1gInSc3	rozmach
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
měla	mít	k5eAaImAgFnS	mít
přes	přes	k7c4	přes
tři	tři	k4xCgInPc4	tři
tisíce	tisíc	k4xCgInPc4	tisíc
posluchačů	posluchač	k1gMnPc2	posluchač
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
přednášel	přednášet	k5eAaImAgMnS	přednášet
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
na	na	k7c6	na
Karlově	Karlův	k2eAgNnSc6d1	Karlovo
náměstí	náměstí	k1gNnSc6	náměstí
i	i	k9	i
profesor	profesor	k1gMnSc1	profesor
T.	T.	kA	T.
G.	G.	kA	G.
Masaryk	Masaryk	k1gMnSc1	Masaryk
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
přednáška	přednáška	k1gFnSc1	přednáška
vyšla	vyjít	k5eAaPmAgFnS	vyjít
i	i	k9	i
knižně	knižně	k6eAd1	knižně
(	(	kIx(	(
<g/>
Student	student	k1gMnSc1	student
a	a	k8xC	a
politika	politika	k1gFnSc1	politika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nebyl	být	k5eNaImAgInS	být
to	ten	k3xDgNnSc1	ten
však	však	k9	však
zdaleka	zdaleka	k6eAd1	zdaleka
Masarykův	Masarykův	k2eAgInSc4d1	Masarykův
první	první	k4xOgInSc4	první
kontakt	kontakt	k1gInSc4	kontakt
se	s	k7c7	s
školou	škola	k1gFnSc7	škola
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
tam	tam	k6eAd1	tam
přednášel	přednášet	k5eAaImAgMnS	přednášet
již	již	k6eAd1	již
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
příchodu	příchod	k1gInSc6	příchod
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
uvádí	uvádět	k5eAaImIp3nS	uvádět
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Jan	Jan	k1gMnSc1	Jan
Herben	Herben	k2eAgMnSc1d1	Herben
dne	den	k1gInSc2	den
"	"	kIx"	"
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1882	[number]	k4	1882
přednášel	přednášet	k5eAaImAgMnS	přednášet
v	v	k7c6	v
sále	sál	k1gInSc6	sál
české	český	k2eAgFnSc2d1	Česká
techniky	technika	k1gFnSc2	technika
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
<g/>
:	:	kIx,	:
Blaise	Blaise	k1gFnSc1	Blaise
Pascal	pascal	k1gInSc1	pascal
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
život	život	k1gInSc1	život
a	a	k8xC	a
filosofie	filosofie	k1gFnSc1	filosofie
s	s	k7c7	s
převažující	převažující	k2eAgFnSc7d1	převažující
otázkou	otázka	k1gFnSc7	otázka
náboženskou	náboženský	k2eAgFnSc7d1	náboženská
<g/>
.	.	kIx.	.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
nesčetně	sčetně	k6eNd1	sčetně
přednášek	přednáška	k1gFnPc2	přednáška
mimo	mimo	k7c4	mimo
síně	síň	k1gFnPc4	síň
universitní	universitní	k2eAgFnPc4d1	universitní
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
České	český	k2eAgFnSc2d1	Česká
vysoké	vysoká	k1gFnSc2	vysoká
učení	učení	k1gNnSc2	učení
technické	technický	k2eAgNnSc1d1	technické
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
vytvoření	vytvoření	k1gNnSc6	vytvoření
ČSR	ČSR	kA	ČSR
byla	být	k5eAaImAgFnS	být
škola	škola	k1gFnSc1	škola
výnosem	výnos	k1gInSc7	výnos
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
školství	školství	k1gNnSc2	školství
a	a	k8xC	a
národní	národní	k2eAgFnSc2d1	národní
osvěty	osvěta	k1gFnSc2	osvěta
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1920	[number]	k4	1920
přeorganizována	přeorganizován	k2eAgFnSc1d1	přeorganizována
–	–	k?	–
název	název	k1gInSc1	název
se	se	k3xPyFc4	se
změnil	změnit	k5eAaPmAgInS	změnit
na	na	k7c4	na
České	český	k2eAgNnSc4d1	české
vysoké	vysoký	k2eAgNnSc4d1	vysoké
učení	učení	k1gNnSc4	učení
technické	technický	k2eAgNnSc4d1	technické
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc4	místo
odborů	odbor	k1gInPc2	odbor
řízených	řízený	k2eAgInPc2d1	řízený
přednosty	přednosta	k1gMnPc7	přednosta
byly	být	k5eAaImAgFnP	být
zavedeny	zaveden	k2eAgFnPc4d1	zavedena
vysoké	vysoký	k2eAgFnPc4d1	vysoká
školy	škola	k1gFnPc4	škola
řízené	řízený	k2eAgFnPc4d1	řízená
děkany	děkan	k1gMnPc7	děkan
<g/>
,	,	kIx,	,
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
akademického	akademický	k2eAgInSc2d1	akademický
senátu	senát	k1gInSc2	senát
stál	stát	k5eAaImAgMnS	stát
rektor	rektor	k1gMnSc1	rektor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tehdy	tehdy	k6eAd1	tehdy
ČVUT	ČVUT	kA	ČVUT
tvořilo	tvořit	k5eAaImAgNnS	tvořit
svazek	svazek	k1gInSc4	svazek
sedmi	sedm	k4xCc2	sedm
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
inženýrského	inženýrský	k2eAgNnSc2d1	inženýrské
stavitelství	stavitelství	k1gNnSc2	stavitelství
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
kulturního	kulturní	k2eAgNnSc2d1	kulturní
inženýrství	inženýrství	k1gNnSc2	inženýrství
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
architektury	architektura	k1gFnPc1	architektura
a	a	k8xC	a
pozemního	pozemní	k2eAgNnSc2d1	pozemní
stavitelství	stavitelství	k1gNnSc2	stavitelství
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
strojního	strojní	k2eAgMnSc2d1	strojní
a	a	k8xC	a
elektrotechnického	elektrotechnický	k2eAgNnSc2d1	elektrotechnické
inženýrství	inženýrství	k1gNnSc2	inženýrství
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
zemědělského	zemědělský	k2eAgMnSc2d1	zemědělský
a	a	k8xC	a
lesního	lesní	k2eAgNnSc2d1	lesní
inženýrství	inženýrství	k1gNnSc2	inženýrství
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
chemicko-technologického	chemickoechnologický	k2eAgNnSc2d1	chemicko-technologické
inženýrství	inženýrství	k1gNnSc2	inženýrství
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
speciálních	speciální	k2eAgFnPc2d1	speciální
nauk	nauka	k1gFnPc2	nauka
<g/>
.	.	kIx.	.
<g/>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
kulturního	kulturní	k2eAgNnSc2d1	kulturní
inženýrství	inženýrství	k1gNnSc2	inženýrství
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
začleněna	začlenit	k5eAaPmNgFnS	začlenit
pod	pod	k7c4	pod
Vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
inženýrského	inženýrský	k2eAgNnSc2d1	inženýrské
stavitelství	stavitelství	k1gNnSc2	stavitelství
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
se	s	k7c7	s
plnohodnotnou	plnohodnotný	k2eAgFnSc7d1	plnohodnotná
součástí	součást	k1gFnSc7	součást
ČVUT	ČVUT	kA	ČVUT
stala	stát	k5eAaPmAgFnS	stát
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
obchodní	obchodní	k2eAgFnSc1d1	obchodní
<g/>
,	,	kIx,	,
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
pouze	pouze	k6eAd1	pouze
tříletá	tříletý	k2eAgFnSc1d1	tříletá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
založil	založit	k5eAaPmAgMnS	založit
František	František	k1gMnSc1	František
Klokner	Klokner	k1gMnSc1	Klokner
při	při	k7c6	při
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
inženýrského	inženýrský	k2eAgNnSc2d1	inženýrské
stavitelství	stavitelství	k1gNnSc2	stavitelství
Výzkumný	výzkumný	k2eAgMnSc1d1	výzkumný
a	a	k8xC	a
zkušební	zkušební	k2eAgInSc1d1	zkušební
ústav	ústav	k1gInSc1	ústav
hmot	hmota	k1gFnPc2	hmota
a	a	k8xC	a
konstrukcí	konstrukce	k1gFnPc2	konstrukce
stavebních	stavební	k2eAgFnPc2d1	stavební
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
I	i	k9	i
ČVUT	ČVUT	kA	ČVUT
postihlo	postihnout	k5eAaPmAgNnS	postihnout
násilné	násilný	k2eAgNnSc1d1	násilné
uzavření	uzavření	k1gNnSc1	uzavření
českých	český	k2eAgFnPc2d1	Česká
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
za	za	k7c2	za
nacistické	nacistický	k2eAgFnSc2d1	nacistická
okupace	okupace	k1gFnSc2	okupace
<g/>
,	,	kIx,	,
od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1939	[number]	k4	1939
do	do	k7c2	do
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
němu	on	k3xPp3gNnSc3	on
zůstala	zůstat	k5eAaPmAgFnS	zůstat
nedokončena	dokončen	k2eNgFnSc1d1	nedokončena
i	i	k8xC	i
výstavba	výstavba	k1gFnSc1	výstavba
budov	budova	k1gFnPc2	budova
ČVUT	ČVUT	kA	ČVUT
v	v	k7c6	v
Dejvicích	Dejvice	k1gFnPc6	Dejvice
<g/>
,	,	kIx,	,
započatá	započatý	k2eAgFnSc1d1	započatá
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
technika	technika	k1gFnSc1	technika
byla	být	k5eAaImAgFnS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
Benešovým	Benešův	k2eAgInSc7d1	Benešův
dekretem	dekret	k1gInSc7	dekret
123	[number]	k4	123
<g/>
/	/	kIx~	/
<g/>
1945	[number]	k4	1945
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
ze	z	k7c2	z
dne	den	k1gInSc2	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
50	[number]	k4	50
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
===	===	k?	===
</s>
</p>
<p>
<s>
Rok	rok	k1gInSc1	rok
1948	[number]	k4	1948
znamenal	znamenat	k5eAaImAgInS	znamenat
pro	pro	k7c4	pro
stovky	stovka	k1gFnPc4	stovka
studentů	student	k1gMnPc2	student
vyloučení	vyloučení	k1gNnSc2	vyloučení
ze	z	k7c2	z
studií	studio	k1gNnPc2	studio
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
byla	být	k5eAaImAgFnS	být
také	také	k9	také
zrušena	zrušen	k2eAgFnSc1d1	zrušena
vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
obchodní	obchodní	k2eAgFnSc1d1	obchodní
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
největší	veliký	k2eAgFnSc1d3	veliký
fakulta	fakulta	k1gFnSc1	fakulta
ČVUT	ČVUT	kA	ČVUT
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
věd	věda	k1gFnPc2	věda
hospodářských	hospodářský	k2eAgFnPc2d1	hospodářská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
ukončením	ukončení	k1gNnSc7	ukončení
výuky	výuka	k1gFnSc2	výuka
na	na	k7c4	na
VŠO	VŠO	kA	VŠO
(	(	kIx(	(
<g/>
Vysoké	vysoký	k2eAgFnSc3d1	vysoká
škole	škola	k1gFnSc3	škola
věd	věda	k1gFnPc2	věda
hospodářských	hospodářský	k2eAgInPc2d1	hospodářský
<g/>
)	)	kIx)	)
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
byla	být	k5eAaImAgFnS	být
zřízena	zřídit	k5eAaPmNgFnS	zřídit
na	na	k7c6	na
ČVUT	ČVUT	kA	ČVUT
Fakulta	fakulta	k1gFnSc1	fakulta
ekonomicko-inženýrská	ekonomickonženýrský	k2eAgFnSc1d1	ekonomicko-inženýrská
(	(	kIx(	(
<g/>
FEI	FEI	kA	FEI
–	–	k?	–
zrušena	zrušen	k2eAgFnSc1d1	zrušena
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
let	léto	k1gNnPc2	léto
1949	[number]	k4	1949
<g/>
–	–	k?	–
<g/>
1960	[number]	k4	1960
byla	být	k5eAaImAgFnS	být
univerzita	univerzita	k1gFnSc1	univerzita
přeorganizována	přeorganizovat	k5eAaPmNgFnS	přeorganizovat
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgNnP	vzniknout
celoškolská	celoškolský	k2eAgNnPc1d1	celoškolské
pracoviště	pracoviště	k1gNnPc1	pracoviště
jako	jako	k8xS	jako
např.	např.	kA	např.
katedry	katedra	k1gFnSc2	katedra
vojenské	vojenský	k2eAgFnSc2d1	vojenská
<g/>
,	,	kIx,	,
marxismu-leninismu	marxismueninismus	k1gInSc2	marxismu-leninismus
<g/>
,	,	kIx,	,
branné	branný	k2eAgFnSc2d1	Branná
výchovy	výchova	k1gFnSc2	výchova
(	(	kIx(	(
<g/>
zrušeny	zrušit	k5eAaPmNgFnP	zrušit
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
byly	být	k5eAaImAgInP	být
z	z	k7c2	z
ČVUT	ČVUT	kA	ČVUT
vyděleny	vydělen	k2eAgFnPc1d1	vydělena
Vysoká	vysoká	k1gFnSc1	vysoká
škola	škola	k1gFnSc1	škola
chemicko-technologická	chemickoechnologický	k2eAgFnSc1d1	chemicko-technologická
a	a	k8xC	a
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
byl	být	k5eAaImAgInS	být
škole	škola	k1gFnSc3	škola
udělen	udělen	k2eAgInSc1d1	udělen
Řád	řád	k1gInSc1	řád
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
60	[number]	k4	60
<g/>
.	.	kIx.	.
až	až	k9	až
90	[number]	k4	90
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
sestávalo	sestávat	k5eAaImAgNnS	sestávat
ČVUT	ČVUT	kA	ČVUT
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
fakult	fakulta	k1gFnPc2	fakulta
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
stavební	stavební	k2eAgFnSc1d1	stavební
(	(	kIx(	(
<g/>
architektura	architektura	k1gFnSc1	architektura
<g/>
,	,	kIx,	,
stavební	stavební	k2eAgNnSc1d1	stavební
inženýrství	inženýrství	k1gNnSc1	inženýrství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
strojní	strojní	k2eAgNnSc1d1	strojní
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
elektrotechnická	elektrotechnický	k2eAgFnSc1d1	elektrotechnická
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
jaderná	jaderný	k2eAgFnSc1d1	jaderná
a	a	k8xC	a
fyzikálně	fyzikálně	k6eAd1	fyzikálně
inženýrská	inženýrský	k2eAgFnSc1d1	inženýrská
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
byla	být	k5eAaImAgFnS	být
zřízena	zřízen	k2eAgFnSc1d1	zřízena
Fakulta	fakulta	k1gFnSc1	fakulta
architektury	architektura	k1gFnSc2	architektura
(	(	kIx(	(
<g/>
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
část	část	k1gFnSc4	část
stavební	stavební	k2eAgFnSc2d1	stavební
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
pak	pak	k6eAd1	pak
Fakulta	fakulta	k1gFnSc1	fakulta
dopravní	dopravní	k2eAgFnSc1d1	dopravní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
bylo	být	k5eAaImAgNnS	být
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
detašované	detašovaný	k2eAgNnSc1d1	detašované
pracoviště	pracoviště	k1gNnSc1	pracoviště
FD	FD	kA	FD
a	a	k8xC	a
jaderné	jaderný	k2eAgFnSc2d1	jaderná
a	a	k8xC	a
FJFI	FJFI	kA	FJFI
v	v	k7c6	v
Děčíně	Děčín	k1gInSc6	Děčín
<g/>
.	.	kIx.	.
</s>
<s>
Ústav	ústav	k1gInSc1	ústav
biomedicínského	biomedicínský	k2eAgNnSc2d1	biomedicínské
inženýrství	inženýrství	k1gNnSc2	inženýrství
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
k	k	k7c3	k
27	[number]	k4	27
<g/>
.	.	kIx.	.
květnu	květen	k1gInSc3	květen
2005	[number]	k4	2005
přetransformoval	přetransformovat	k5eAaPmAgInS	přetransformovat
na	na	k7c4	na
Fakultu	fakulta	k1gFnSc4	fakulta
biomedicínského	biomedicínský	k2eAgNnSc2d1	biomedicínské
inženýrství	inženýrství	k1gNnSc2	inženýrství
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Kladně	Kladno	k1gNnSc6	Kladno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Fakulta	fakulta	k1gFnSc1	fakulta
informačních	informační	k2eAgFnPc2d1	informační
technologií	technologie	k1gFnPc2	technologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Organizace	organizace	k1gFnSc2	organizace
==	==	k?	==
</s>
</p>
<p>
<s>
ČVUT	ČVUT	kA	ČVUT
dnes	dnes	k6eAd1	dnes
má	mít	k5eAaImIp3nS	mít
osm	osm	k4xCc1	osm
fakult	fakulta	k1gFnPc2	fakulta
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
podle	podle	k7c2	podle
oborů	obor	k1gInPc2	obor
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
katedry	katedra	k1gFnPc4	katedra
<g/>
/	/	kIx~	/
<g/>
ústavy	ústav	k1gInPc4	ústav
<g/>
.	.	kIx.	.
</s>
<s>
Výuku	výuka	k1gFnSc4	výuka
také	také	k9	také
nabízí	nabízet	k5eAaImIp3nS	nabízet
pět	pět	k4xCc4	pět
vysokoškolských	vysokoškolský	k2eAgInPc2d1	vysokoškolský
ústavů	ústav	k1gInPc2	ústav
<g/>
,	,	kIx,	,
univerzita	univerzita	k1gFnSc1	univerzita
také	také	k9	také
dále	daleko	k6eAd2	daleko
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
celoškolská	celoškolský	k2eAgNnPc4d1	celoškolské
účelová	účelový	k2eAgNnPc4d1	účelové
zařízení	zařízení	k1gNnPc4	zařízení
a	a	k8xC	a
specializovaná	specializovaný	k2eAgNnPc4d1	specializované
pracoviště	pracoviště	k1gNnPc4	pracoviště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fakulty	fakulta	k1gFnSc2	fakulta
===	===	k?	===
</s>
</p>
<p>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
stavební	stavební	k2eAgFnSc2d1	stavební
</s>
</p>
<p>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
strojní	strojní	k2eAgFnSc2d1	strojní
</s>
</p>
<p>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
elektrotechnická	elektrotechnický	k2eAgFnSc1d1	elektrotechnická
</s>
</p>
<p>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
jaderná	jaderný	k2eAgFnSc1d1	jaderná
a	a	k8xC	a
fyzikálně	fyzikálně	k6eAd1	fyzikálně
inženýrská	inženýrský	k2eAgFnSc1d1	inženýrská
</s>
</p>
<p>
<s>
Detašované	detašovaný	k2eAgNnSc1d1	detašované
pracoviště	pracoviště	k1gNnSc1	pracoviště
Děčín	Děčín	k1gInSc1	Děčín
</s>
</p>
<p>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
architektury	architektura	k1gFnSc2	architektura
</s>
</p>
<p>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
dopravní	dopravní	k2eAgFnSc2d1	dopravní
</s>
</p>
<p>
<s>
Detašované	detašovaný	k2eAgNnSc1d1	detašované
pracoviště	pracoviště	k1gNnSc1	pracoviště
Děčín	Děčín	k1gInSc1	Děčín
</s>
</p>
<p>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
biomedicínského	biomedicínský	k2eAgNnSc2d1	biomedicínské
inženýrství	inženýrství	k1gNnSc2	inženýrství
</s>
</p>
<p>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
informačních	informační	k2eAgFnPc2d1	informační
technologií	technologie	k1gFnPc2	technologie
</s>
</p>
<p>
<s>
===	===	k?	===
Vysokoškolské	vysokoškolský	k2eAgInPc1d1	vysokoškolský
ústavy	ústav	k1gInPc1	ústav
===	===	k?	===
</s>
</p>
<p>
<s>
Masarykův	Masarykův	k2eAgInSc4d1	Masarykův
ústav	ústav	k1gInSc4	ústav
vyšších	vysoký	k2eAgFnPc2d2	vyšší
studií	studie	k1gFnPc2	studie
</s>
</p>
<p>
<s>
Kloknerův	Kloknerův	k2eAgInSc1d1	Kloknerův
ústav	ústav	k1gInSc1	ústav
</s>
</p>
<p>
<s>
Univerzitní	univerzitní	k2eAgNnSc1d1	univerzitní
centrum	centrum	k1gNnSc1	centrum
energeticky	energeticky	k6eAd1	energeticky
efektivních	efektivní	k2eAgFnPc2d1	efektivní
budov	budova	k1gFnPc2	budova
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
institut	institut	k1gInSc1	institut
informatiky	informatika	k1gFnSc2	informatika
<g/>
,	,	kIx,	,
robotiky	robotika	k1gFnSc2	robotika
a	a	k8xC	a
kybernetiky	kybernetika	k1gFnSc2	kybernetika
</s>
</p>
<p>
<s>
Ústav	ústav	k1gInSc1	ústav
tělesné	tělesný	k2eAgFnSc2d1	tělesná
výchovy	výchova	k1gFnSc2	výchova
a	a	k8xC	a
sportu	sport	k1gInSc2	sport
</s>
</p>
<p>
<s>
===	===	k?	===
Ostatní	ostatní	k2eAgFnPc4d1	ostatní
součásti	součást	k1gFnPc4	součást
ČVUT	ČVUT	kA	ČVUT
===	===	k?	===
</s>
</p>
<p>
<s>
Výpočetní	výpočetní	k2eAgNnSc1d1	výpočetní
a	a	k8xC	a
informační	informační	k2eAgNnSc1d1	informační
centrum	centrum	k1gNnSc1	centrum
(	(	kIx(	(
<g/>
VIC	VIC	kA	VIC
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Inovacentrum	Inovacentrum	k1gNnSc1	Inovacentrum
-	-	kIx~	-
centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
transfer	transfer	k1gInSc4	transfer
technologií	technologie	k1gFnPc2	technologie
a	a	k8xC	a
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
průmyslem	průmysl	k1gInSc7	průmysl
ČVUT	ČVUT	kA	ČVUT
</s>
</p>
<p>
<s>
Ústav	ústav	k1gInSc1	ústav
technické	technický	k2eAgFnSc2d1	technická
a	a	k8xC	a
experimentální	experimentální	k2eAgFnSc2d1	experimentální
fyziky	fyzika	k1gFnSc2	fyzika
ČVUT	ČVUT	kA	ČVUT
(	(	kIx(	(
<g/>
ÚTEF	ÚTEF	kA	ÚTEF
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ústřední	ústřední	k2eAgFnSc1d1	ústřední
knihovna	knihovna	k1gFnSc1	knihovna
ČVUT	ČVUT	kA	ČVUT
(	(	kIx(	(
<g/>
ÚK	ÚK	kA	ÚK
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2014	[number]	k4	2014
bylo	být	k5eAaImAgNnS	být
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
nové	nový	k2eAgNnSc1d1	nové
Informační	informační	k2eAgNnSc1d1	informační
centrum	centrum	k1gNnSc1	centrum
ČVUT	ČVUT	kA	ČVUT
v	v	k7c6	v
objektu	objekt	k1gInSc6	objekt
AIR	AIR	kA	AIR
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
studenti	student	k1gMnPc1	student
Fakulty	fakulta	k1gFnSc2	fakulta
architektury	architektura	k1gFnSc2	architektura
ČVUT	ČVUT	kA	ČVUT
pro	pro	k7c4	pro
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
studentskou	studentský	k2eAgFnSc4d1	studentská
soutěž	soutěž	k1gFnSc4	soutěž
Solar	Solar	k1gMnSc1	Solar
Decathlon	Decathlon	k1gInSc4	Decathlon
2013	[number]	k4	2013
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Účelová	účelový	k2eAgNnPc1d1	účelové
zařízení	zařízení	k1gNnPc1	zařízení
===	===	k?	===
</s>
</p>
<p>
<s>
Rektorát	rektorát	k1gInSc1	rektorát
ČVUT	ČVUT	kA	ČVUT
</s>
</p>
<p>
<s>
Správa	správa	k1gFnSc1	správa
účelových	účelový	k2eAgNnPc2d1	účelové
zařízení	zařízení	k1gNnPc2	zařízení
(	(	kIx(	(
<g/>
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
studentské	studentský	k2eAgFnPc4d1	studentská
koleje	kolej	k1gFnPc4	kolej
<g/>
,	,	kIx,	,
menzy	menza	k1gFnPc4	menza
<g/>
,	,	kIx,	,
správu	správa	k1gFnSc4	správa
Betlémské	betlémský	k2eAgFnSc2d1	Betlémská
kaple	kaple	k1gFnSc2	kaple
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
technika	technika	k1gFnSc1	technika
–	–	k?	–
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
ČVUT	ČVUT	kA	ČVUT
</s>
</p>
<p>
<s>
==	==	k?	==
Fakulty	fakulta	k1gFnPc4	fakulta
a	a	k8xC	a
VŠ	vš	k0	vš
ústavy	ústav	k1gInPc1	ústav
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Fakulta	fakulta	k1gFnSc1	fakulta
stavební	stavební	k2eAgFnSc1d1	stavební
===	===	k?	===
</s>
</p>
<p>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
stavební	stavební	k2eAgFnSc1d1	stavební
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
osmi	osm	k4xCc2	osm
fakult	fakulta	k1gFnPc2	fakulta
Českého	český	k2eAgNnSc2d1	české
vysokého	vysoký	k2eAgNnSc2d1	vysoké
učení	učení	k1gNnSc2	učení
technického	technický	k2eAgNnSc2d1	technické
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pevným	pevný	k2eAgInSc7d1	pevný
základem	základ	k1gInSc7	základ
univerzity	univerzita	k1gFnPc4	univerzita
od	od	k7c2	od
jejího	její	k3xOp3gNnSc2	její
založení	založení	k1gNnSc2	založení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1707	[number]	k4	1707
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nejstarším	starý	k2eAgInSc7d3	nejstarší
samostatným	samostatný	k2eAgInSc7d1	samostatný
oborem	obor	k1gInSc7	obor
vznikajících	vznikající	k2eAgFnPc2d1	vznikající
polytechnických	polytechnický	k2eAgFnPc2d1	polytechnická
institucí	instituce	k1gFnPc2	instituce
bylo	být	k5eAaImAgNnS	být
právě	právě	k6eAd1	právě
stavitelské	stavitelský	k2eAgNnSc1d1	stavitelské
a	a	k8xC	a
kartografické	kartografický	k2eAgNnSc1d1	kartografické
umění	umění	k1gNnSc1	umění
<g/>
.	.	kIx.	.
</s>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
historicky	historicky	k6eAd1	historicky
nejstarší	starý	k2eAgFnSc1d3	nejstarší
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
počtem	počet	k1gInSc7	počet
studentů	student	k1gMnPc2	student
i	i	k8xC	i
dalšími	další	k2eAgMnPc7d1	další
parametry	parametr	k1gInPc4	parametr
svého	svůj	k3xOyFgInSc2	svůj
výkonu	výkon	k1gInSc2	výkon
k	k	k7c3	k
největším	veliký	k2eAgFnPc3d3	veliký
fakultám	fakulta	k1gFnPc3	fakulta
ČVUT	ČVUT	kA	ČVUT
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
je	být	k5eAaImIp3nS	být
moderní	moderní	k2eAgFnSc7d1	moderní
vysokoškolskou	vysokoškolský	k2eAgFnSc7d1	vysokoškolská
institucí	instituce	k1gFnSc7	instituce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
neustále	neustále	k6eAd1	neustále
zdokonaluje	zdokonalovat	k5eAaImIp3nS	zdokonalovat
nabídku	nabídka	k1gFnSc4	nabídka
studijních	studijní	k2eAgInPc2d1	studijní
oborů	obor	k1gInPc2	obor
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc4	jejich
skladbu	skladba	k1gFnSc4	skladba
i	i	k8xC	i
obsah	obsah	k1gInSc4	obsah
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Studenti	student	k1gMnPc1	student
mají	mít	k5eAaImIp3nP	mít
řadu	řada	k1gFnSc4	řada
příležitostí	příležitost	k1gFnPc2	příležitost
absolvovat	absolvovat	k5eAaPmF	absolvovat
část	část	k1gFnSc4	část
studia	studio	k1gNnSc2	studio
svého	svůj	k3xOyFgNnSc2	svůj
studia	studio	k1gNnSc2	studio
také	také	k9	také
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
si	se	k3xPyFc3	se
dává	dávat	k5eAaImIp3nS	dávat
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
držet	držet	k5eAaImF	držet
vysokou	vysoký	k2eAgFnSc4d1	vysoká
úroveň	úroveň	k1gFnSc4	úroveň
kvality	kvalita	k1gFnSc2	kvalita
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
studijních	studijní	k2eAgInPc6d1	studijní
oborech	obor	k1gInPc6	obor
garantuje	garantovat	k5eAaBmIp3nS	garantovat
propojení	propojení	k1gNnSc1	propojení
na	na	k7c4	na
praxi	praxe	k1gFnSc4	praxe
a	a	k8xC	a
nové	nový	k2eAgInPc4d1	nový
poznatky	poznatek	k1gInPc4	poznatek
z	z	k7c2	z
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Absolventi	absolvent	k1gMnPc1	absolvent
se	se	k3xPyFc4	se
profilují	profilovat	k5eAaImIp3nP	profilovat
jako	jako	k8xC	jako
široká	široký	k2eAgFnSc1d1	široká
řada	řada	k1gFnSc1	řada
odborníků	odborník	k1gMnPc2	odborník
ve	v	k7c6	v
stavebnictví	stavebnictví	k1gNnSc6	stavebnictví
s	s	k7c7	s
uplatněním	uplatnění	k1gNnSc7	uplatnění
napříč	napříč	k7c7	napříč
nejrůznějšími	různý	k2eAgFnPc7d3	nejrůznější
oblastmi	oblast	k1gFnPc7	oblast
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
doprava	doprava	k1gFnSc1	doprava
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgNnSc1d1	vodní
a	a	k8xC	a
lesní	lesní	k2eAgNnSc1d1	lesní
hospodářství	hospodářství	k1gNnSc1	hospodářství
<g/>
,	,	kIx,	,
energetika	energetika	k1gFnSc1	energetika
<g/>
,	,	kIx,	,
životní	životní	k2eAgNnSc1d1	životní
prostředí	prostředí	k1gNnSc1	prostředí
a	a	k8xC	a
hygiena	hygiena	k1gFnSc1	hygiena
<g/>
,	,	kIx,	,
zdravotnictví	zdravotnictví	k1gNnSc1	zdravotnictví
<g/>
,	,	kIx,	,
zemědělství	zemědělství	k1gNnSc1	zemědělství
či	či	k8xC	či
průmysl	průmysl	k1gInSc1	průmysl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fakulta	fakulta	k1gFnSc1	fakulta
strojní	strojní	k2eAgFnSc1d1	strojní
===	===	k?	===
</s>
</p>
<p>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
strojní	strojní	k2eAgFnSc2d1	strojní
ČVUT	ČVUT	kA	ČVUT
(	(	kIx(	(
<g/>
FS	FS	kA	FS
ČVUT	ČVUT	kA	ČVUT
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc7d3	nejstarší
strojní	strojní	k2eAgFnSc7d1	strojní
fakultou	fakulta	k1gFnSc7	fakulta
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
vznik	vznik	k1gInSc1	vznik
je	být	k5eAaImIp3nS	být
datován	datovat	k5eAaImNgInS	datovat
rokem	rok	k1gInSc7	rok
1864	[number]	k4	1864
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
fakultě	fakulta	k1gFnSc6	fakulta
studuje	studovat	k5eAaImIp3nS	studovat
přes	přes	k7c4	přes
3	[number]	k4	3
100	[number]	k4	100
studentů	student	k1gMnPc2	student
ve	v	k7c6	v
3	[number]	k4	3
studijních	studijní	k2eAgInPc6d1	studijní
programech	program	k1gInPc6	program
bakalářského	bakalářský	k2eAgInSc2d1	bakalářský
<g/>
,	,	kIx,	,
5	[number]	k4	5
programech	program	k1gInPc6	program
navazujícího	navazující	k2eAgNnSc2d1	navazující
magisterského	magisterský	k2eAgNnSc2d1	magisterské
studia	studio	k1gNnSc2	studio
a	a	k8xC	a
v	v	k7c6	v
doktorském	doktorský	k2eAgNnSc6d1	doktorské
studiu	studio	k1gNnSc6	studio
<g/>
.	.	kIx.	.
</s>
<s>
Studenti	student	k1gMnPc1	student
si	se	k3xPyFc3	se
mohou	moct	k5eAaImIp3nP	moct
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
uvedených	uvedený	k2eAgInPc2d1	uvedený
studijních	studijní	k2eAgInPc2d1	studijní
programů	program	k1gInPc2	program
vybírat	vybírat	k5eAaImF	vybírat
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
řady	řada	k1gFnSc2	řada
oborů	obor	k1gInPc2	obor
od	od	k7c2	od
aplikované	aplikovaný	k2eAgFnSc2d1	aplikovaná
mechaniky	mechanika	k1gFnSc2	mechanika
po	po	k7c4	po
techniku	technika	k1gFnSc4	technika
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
má	mít	k5eAaImIp3nS	mít
fakulta	fakulta	k1gFnSc1	fakulta
18	[number]	k4	18
ústavů	ústav	k1gInPc2	ústav
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
kateder	katedra	k1gFnPc2	katedra
<g/>
)	)	kIx)	)
a	a	k8xC	a
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
530	[number]	k4	530
kvalitních	kvalitní	k2eAgMnPc2d1	kvalitní
absolventů	absolvent	k1gMnPc2	absolvent
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
nemají	mít	k5eNaImIp3nP	mít
problém	problém	k1gInSc4	problém
uplatnit	uplatnit	k5eAaPmF	uplatnit
se	se	k3xPyFc4	se
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
i	i	k9	i
na	na	k7c6	na
činnosti	činnost	k1gFnSc6	činnost
dalších	další	k2eAgNnPc2d1	další
výzkumných	výzkumný	k2eAgNnPc2d1	výzkumné
center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
Inženýrskou	inženýrský	k2eAgFnSc7d1	inženýrská
akademií	akademie	k1gFnSc7	akademie
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
Asociací	asociace	k1gFnPc2	asociace
výzkumných	výzkumný	k2eAgFnPc2d1	výzkumná
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
Svazem	svaz	k1gInSc7	svaz
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
Svazem	svaz	k1gInSc7	svaz
výrobců	výrobce	k1gMnPc2	výrobce
strojírenské	strojírenský	k2eAgFnSc2d1	strojírenská
techniky	technika	k1gFnSc2	technika
<g/>
,	,	kIx,	,
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
velkých	velká	k1gFnPc2	velká
<g/>
,	,	kIx,	,
středních	střední	k2eAgFnPc2d1	střední
i	i	k8xC	i
malých	malý	k2eAgFnPc2d1	malá
průmyslových	průmyslový	k2eAgFnPc2d1	průmyslová
firem	firma	k1gFnPc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
fakulty	fakulta	k1gFnSc2	fakulta
je	být	k5eAaImIp3nS	být
špičkové	špičkový	k2eAgNnSc1d1	špičkové
pedagogické	pedagogický	k2eAgNnSc1d1	pedagogické
a	a	k8xC	a
vědecké	vědecký	k2eAgNnSc1d1	vědecké
pracoviště	pracoviště	k1gNnSc4	pracoviště
uznávané	uznávaný	k2eAgNnSc4d1	uznávané
doma	doma	k6eAd1	doma
i	i	k8xC	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
úspěchů	úspěch	k1gInPc2	úspěch
Fakulty	fakulta	k1gFnSc2	fakulta
strojní	strojní	k2eAgFnSc1d1	strojní
ČVUT	ČVUT	kA	ČVUT
její	její	k3xOp3gInSc4	její
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
objemu	objem	k1gInSc6	objem
aplikovaného	aplikovaný	k2eAgInSc2d1	aplikovaný
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
===	===	k?	===
Fakulta	fakulta	k1gFnSc1	fakulta
elektrotechnická	elektrotechnický	k2eAgFnSc1d1	elektrotechnická
===	===	k?	===
</s>
</p>
<p>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
elektrotechnická	elektrotechnický	k2eAgFnSc1d1	elektrotechnická
je	být	k5eAaImIp3nS	být
druhou	druhý	k4xOgFnSc7	druhý
největší	veliký	k2eAgFnSc7d3	veliký
fakultou	fakulta	k1gFnSc7	fakulta
ČVUT	ČVUT	kA	ČVUT
<g/>
.	.	kIx.	.
</s>
<s>
Tradici	tradice	k1gFnSc4	tradice
zde	zde	k6eAd1	zde
má	mít	k5eAaImIp3nS	mít
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
výuka	výuka	k1gFnSc1	výuka
informatiky	informatika	k1gFnPc1	informatika
<g/>
,	,	kIx,	,
Hospodářské	hospodářský	k2eAgFnPc1d1	hospodářská
noviny	novina	k1gFnPc1	novina
ji	on	k3xPp3gFnSc4	on
označily	označit	k5eAaPmAgInP	označit
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
českou	český	k2eAgFnSc4d1	Česká
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
<g/>
Studium	studium	k1gNnSc1	studium
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
bakalářském	bakalářský	k2eAgNnSc6d1	bakalářské
<g/>
,	,	kIx,	,
navazujících	navazující	k2eAgInPc2d1	navazující
magisterských	magisterský	k2eAgInPc2d1	magisterský
a	a	k8xC	a
v	v	k7c6	v
doktorském	doktorský	k2eAgInSc6d1	doktorský
stupni	stupeň	k1gInSc6	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
fakultě	fakulta	k1gFnSc6	fakulta
lze	lze	k6eAd1	lze
studovat	studovat	k5eAaImF	studovat
programy	program	k1gInPc4	program
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Elektrotechnika	elektrotechnika	k1gFnSc1	elektrotechnika
<g/>
,	,	kIx,	,
energetika	energetika	k1gFnSc1	energetika
a	a	k8xC	a
management	management	k1gInSc1	management
</s>
</p>
<p>
<s>
Elektronika	elektronika	k1gFnSc1	elektronika
a	a	k8xC	a
komunikace	komunikace	k1gFnSc1	komunikace
</s>
</p>
<p>
<s>
Elektrotechnika	elektrotechnika	k1gFnSc1	elektrotechnika
<g/>
,	,	kIx,	,
elektronika	elektronika	k1gFnSc1	elektronika
a	a	k8xC	a
komunikační	komunikační	k2eAgFnSc1d1	komunikační
technika	technika	k1gFnSc1	technika
</s>
</p>
<p>
<s>
Kybernetika	kybernetika	k1gFnSc1	kybernetika
a	a	k8xC	a
robotika	robotika	k1gFnSc1	robotika
</s>
</p>
<p>
<s>
Otevřená	otevřený	k2eAgFnSc1d1	otevřená
informatika	informatika	k1gFnSc1	informatika
</s>
</p>
<p>
<s>
Otevřené	otevřený	k2eAgInPc1d1	otevřený
elektronické	elektronický	k2eAgInPc1d1	elektronický
systémy	systém	k1gInPc1	systém
</s>
</p>
<p>
<s>
Softwarové	softwarový	k2eAgNnSc1d1	softwarové
inženýrství	inženýrství	k1gNnSc1	inženýrství
a	a	k8xC	a
technologie	technologie	k1gFnSc1	technologie
</s>
</p>
<p>
<s>
Inteligentní	inteligentní	k2eAgFnPc1d1	inteligentní
budovy	budova	k1gFnPc1	budova
</s>
</p>
<p>
<s>
Biomedicínské	Biomedicínský	k2eAgNnSc1d1	Biomedicínské
inženýrství	inženýrství	k1gNnSc1	inženýrství
a	a	k8xC	a
informatika	informatika	k1gFnSc1	informatika
</s>
</p>
<p>
<s>
Elektrotechnika	elektrotechnika	k1gFnSc1	elektrotechnika
a	a	k8xC	a
informatika	informatika	k1gFnSc1	informatika
</s>
</p>
<p>
<s>
Lékařská	lékařský	k2eAgFnSc1d1	lékařská
elektronika	elektronika	k1gFnSc1	elektronika
a	a	k8xC	a
bioinformatikaVelká	bioinformatikaVelký	k2eAgFnSc1d1	bioinformatikaVelký
část	část	k1gFnSc1	část
předmětů	předmět	k1gInPc2	předmět
je	být	k5eAaImIp3nS	být
vyučována	vyučovat	k5eAaImNgFnS	vyučovat
i	i	k9	i
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
se	se	k3xPyFc4	se
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
řadí	řadit	k5eAaImIp3nS	řadit
do	do	k7c2	do
přední	přední	k2eAgFnSc2d1	přední
desítky	desítka	k1gFnSc2	desítka
výzkumných	výzkumný	k2eAgFnPc2d1	výzkumná
institucí	instituce	k1gFnPc2	instituce
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
produkuje	produkovat	k5eAaImIp3nS	produkovat
přibližně	přibližně	k6eAd1	přibližně
30	[number]	k4	30
%	%	kIx~	%
výzkumných	výzkumný	k2eAgInPc2d1	výzkumný
výsledků	výsledek	k1gInPc2	výsledek
celého	celý	k2eAgInSc2d1	celý
ČVUT	ČVUT	kA	ČVUT
<g/>
.	.	kIx.	.
</s>
<s>
Spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
se	s	k7c7	s
špičkovými	špičkový	k2eAgFnPc7d1	špičková
světovými	světový	k2eAgFnPc7d1	světová
univerzitami	univerzita	k1gFnPc7	univerzita
a	a	k8xC	a
výzkumnými	výzkumný	k2eAgInPc7d1	výzkumný
ústavy	ústav	k1gInPc7	ústav
i	i	k8xC	i
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
významných	významný	k2eAgFnPc2d1	významná
průmyslových	průmyslový	k2eAgFnPc2d1	průmyslová
<g/>
,	,	kIx,	,
energetických	energetický	k2eAgFnPc2d1	energetická
a	a	k8xC	a
telekomunikačních	telekomunikační	k2eAgFnPc2d1	telekomunikační
společností	společnost	k1gFnPc2	společnost
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Škoda	škoda	k6eAd1	škoda
Auto	auto	k1gNnSc1	auto
<g/>
,	,	kIx,	,
Porsche	Porsche	k1gNnSc1	Porsche
<g/>
,	,	kIx,	,
T-Mobile	T-Mobila	k1gFnSc6	T-Mobila
či	či	k8xC	či
Samsung	Samsung	kA	Samsung
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Elektrofakulta	Elektrofakulta	k1gFnSc1	Elektrofakulta
je	být	k5eAaImIp3nS	být
domovem	domov	k1gInSc7	domov
mnoha	mnoho	k4c2	mnoho
zajímavých	zajímavý	k2eAgInPc2d1	zajímavý
projektů	projekt	k1gInPc2	projekt
–	–	k?	–
například	například	k6eAd1	například
leteckého	letecký	k2eAgInSc2d1	letecký
simulátoru	simulátor	k1gInSc2	simulátor
či	či	k8xC	či
aplikace	aplikace	k1gFnSc2	aplikace
cykloplánovač	cykloplánovač	k1gInSc1	cykloplánovač
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
studentů	student	k1gMnPc2	student
se	se	k3xPyFc4	se
také	také	k9	také
již	již	k6eAd1	již
osmým	osmý	k4xOgInSc7	osmý
rokem	rok	k1gInSc7	rok
úspěšně	úspěšně	k6eAd1	úspěšně
účastní	účastnit	k5eAaImIp3nP	účastnit
celosvětové	celosvětový	k2eAgFnPc4d1	celosvětová
konstrukční	konstrukční	k2eAgFnPc4d1	konstrukční
a	a	k8xC	a
závodní	závodní	k2eAgFnPc4d1	závodní
soutěže	soutěž	k1gFnPc4	soutěž
univerzitních	univerzitní	k2eAgMnPc2d1	univerzitní
týmů	tým	k1gInPc2	tým
Formule	formule	k1gFnSc2	formule
Student	student	k1gMnSc1	student
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
formulí	formule	k1gFnPc2	formule
s	s	k7c7	s
elektrickým	elektrický	k2eAgInSc7d1	elektrický
pohonem	pohon	k1gInSc7	pohon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fakulta	fakulta	k1gFnSc1	fakulta
jaderná	jaderný	k2eAgFnSc1d1	jaderná
a	a	k8xC	a
fyzikálně	fyzikálně	k6eAd1	fyzikálně
inženýrská	inženýrský	k2eAgFnSc1d1	inženýrská
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Fakultě	fakulta	k1gFnSc6	fakulta
jaderné	jaderný	k2eAgFnSc6d1	jaderná
a	a	k8xC	a
fyzikálně	fyzikálně	k6eAd1	fyzikálně
inženýrské	inženýrský	k2eAgInPc1d1	inženýrský
(	(	kIx(	(
<g/>
FJFI	FJFI	kA	FJFI
<g/>
)	)	kIx)	)
studuje	studovat	k5eAaImIp3nS	studovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
<g/>
/	/	kIx~	/
<g/>
18	[number]	k4	18
796	[number]	k4	796
studentů	student	k1gMnPc2	student
v	v	k7c6	v
16	[number]	k4	16
studijních	studijní	k2eAgInPc6d1	studijní
programech	program	k1gInPc6	program
bakalářského	bakalářský	k2eAgMnSc2d1	bakalářský
a	a	k8xC	a
16	[number]	k4	16
programech	program	k1gInPc6	program
magisterského	magisterský	k2eAgNnSc2d1	magisterské
studia	studio	k1gNnSc2	studio
a	a	k8xC	a
v	v	k7c6	v
doktorském	doktorský	k2eAgNnSc6d1	doktorské
studiu	studio	k1gNnSc6	studio
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
má	mít	k5eAaImIp3nS	mít
fakulta	fakulta	k1gFnSc1	fakulta
10	[number]	k4	10
kateder	katedra	k1gFnPc2	katedra
a	a	k8xC	a
jejími	její	k3xOp3gFnPc7	její
zdmi	zeď	k1gFnPc7	zeď
prošlo	projít	k5eAaPmAgNnS	projít
již	již	k9	již
5	[number]	k4	5
232	[number]	k4	232
kvalitních	kvalitní	k2eAgMnPc2d1	kvalitní
a	a	k8xC	a
specializovaných	specializovaný	k2eAgMnPc2d1	specializovaný
absolventů	absolvent	k1gMnPc2	absolvent
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgMnSc7d1	současný
děkanem	děkan	k1gMnSc7	děkan
je	být	k5eAaImIp3nS	být
prof.	prof.	kA	prof.
Igor	Igor	k1gMnSc1	Igor
Jex	Jex	k1gMnSc1	Jex
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
založená	založený	k2eAgFnSc1d1	založená
původně	původně	k6eAd1	původně
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
čs	čs	kA	čs
<g/>
.	.	kIx.	.
jaderného	jaderný	k2eAgInSc2d1	jaderný
programu	program	k1gInSc2	program
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
postupně	postupně	k6eAd1	postupně
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
svou	svůj	k3xOyFgFnSc4	svůj
působnost	působnost	k1gFnSc4	působnost
na	na	k7c4	na
široké	široký	k2eAgNnSc4d1	široké
spektrum	spektrum	k1gNnSc4	spektrum
matematických	matematický	k2eAgInPc2d1	matematický
<g/>
,	,	kIx,	,
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
a	a	k8xC	a
chemických	chemický	k2eAgInPc2d1	chemický
oborů	obor	k1gInPc2	obor
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
60	[number]	k4	60
let	let	k1gInSc4	let
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
si	se	k3xPyFc3	se
vydobyla	vydobýt	k5eAaPmAgFnS	vydobýt
pověst	pověst	k1gFnSc4	pověst
náročné	náročný	k2eAgFnSc2d1	náročná
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
vzdělání	vzdělání	k1gNnSc4	vzdělání
vysoké	vysoký	k2eAgFnSc2d1	vysoká
úrovně	úroveň	k1gFnSc2	úroveň
s	s	k7c7	s
hlubokým	hluboký	k2eAgInSc7d1	hluboký
matematicko-fyzikálním	matematickoyzikální	k2eAgInSc7d1	matematicko-fyzikální
základem	základ	k1gInSc7	základ
a	a	k8xC	a
individuálním	individuální	k2eAgInSc7d1	individuální
přístupem	přístup	k1gInSc7	přístup
ke	k	k7c3	k
studentům	student	k1gMnPc3	student
<g/>
.	.	kIx.	.
</s>
<s>
Studenti	student	k1gMnPc1	student
se	se	k3xPyFc4	se
aktivně	aktivně	k6eAd1	aktivně
podílejí	podílet	k5eAaImIp3nP	podílet
na	na	k7c4	na
práci	práce	k1gFnSc4	práce
kateder	katedra	k1gFnPc2	katedra
a	a	k8xC	a
vědeckých	vědecký	k2eAgInPc2d1	vědecký
týmů	tým	k1gInPc2	tým
<g/>
,	,	kIx,	,
naučí	naučit	k5eAaPmIp3nS	naučit
se	se	k3xPyFc4	se
nejméně	málo	k6eAd3	málo
dva	dva	k4xCgInPc4	dva
světové	světový	k2eAgInPc4d1	světový
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
důvěrně	důvěrně	k6eAd1	důvěrně
se	se	k3xPyFc4	se
sžijí	sžít	k5eAaPmIp3nP	sžít
s	s	k7c7	s
výpočetní	výpočetní	k2eAgFnSc7d1	výpočetní
technikou	technika	k1gFnSc7	technika
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
orientace	orientace	k1gFnPc4	orientace
v	v	k7c6	v
mezioborové	mezioborový	k2eAgFnSc6d1	mezioborová
problematice	problematika	k1gFnSc6	problematika
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
absolvuje	absolvovat	k5eAaPmIp3nS	absolvovat
studijní	studijní	k2eAgInPc4d1	studijní
pobyty	pobyt	k1gInPc4	pobyt
na	na	k7c6	na
zahraničních	zahraniční	k2eAgFnPc6d1	zahraniční
univerzitách	univerzita	k1gFnPc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Absolventi	absolvent	k1gMnPc1	absolvent
jsou	být	k5eAaImIp3nP	být
všestranně	všestranně	k6eAd1	všestranně
připraveni	připravit	k5eAaPmNgMnP	připravit
a	a	k8xC	a
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
vysoce	vysoce	k6eAd1	vysoce
úspěšní	úspěšný	k2eAgMnPc1d1	úspěšný
bez	bez	k7c2	bez
problému	problém	k1gInSc2	problém
uplatnit	uplatnit	k5eAaPmF	uplatnit
se	se	k3xPyFc4	se
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
se	se	k3xPyFc4	se
také	také	k9	také
snaží	snažit	k5eAaImIp3nS	snažit
maximálně	maximálně	k6eAd1	maximálně
vyjít	vyjít	k5eAaPmF	vyjít
vstříc	vstříc	k6eAd1	vstříc
handicapovaným	handicapovaný	k2eAgMnPc3d1	handicapovaný
studentům	student	k1gMnPc3	student
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
budova	budova	k1gFnSc1	budova
je	být	k5eAaImIp3nS	být
bezbariérová	bezbariérový	k2eAgFnSc1d1	bezbariérová
a	a	k8xC	a
díky	díky	k7c3	díky
svému	svůj	k3xOyFgNnSc3	svůj
technickému	technický	k2eAgNnSc3d1	technické
vybavení	vybavení	k1gNnSc3	vybavení
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
studium	studium	k1gNnSc4	studium
i	i	k9	i
zrakově	zrakově	k6eAd1	zrakově
postiženým	postižený	k2eAgMnPc3d1	postižený
<g/>
.	.	kIx.	.
</s>
<s>
Funguje	fungovat	k5eAaImIp3nS	fungovat
zde	zde	k6eAd1	zde
Středisko	středisko	k1gNnSc1	středisko
pro	pro	k7c4	pro
podporu	podpora	k1gFnSc4	podpora
studentů	student	k1gMnPc2	student
se	s	k7c7	s
specifickými	specifický	k2eAgFnPc7d1	specifická
potřebami	potřeba	k1gFnPc7	potřeba
ČVUT	ČVUT	kA	ČVUT
ELSA	Elsa	k1gFnSc1	Elsa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
handicapovaní	handicapovaný	k2eAgMnPc1d1	handicapovaný
získávají	získávat	k5eAaImIp3nP	získávat
nejen	nejen	k6eAd1	nejen
technickou	technický	k2eAgFnSc4d1	technická
podporu	podpora	k1gFnSc4	podpora
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
fakulta	fakulta	k1gFnSc1	fakulta
snaží	snažit	k5eAaImIp3nS	snažit
podporovat	podporovat	k5eAaImF	podporovat
nové	nový	k2eAgInPc4d1	nový
trendy	trend	k1gInPc4	trend
ve	v	k7c6	v
výzkumu	výzkum	k1gInSc6	výzkum
a	a	k8xC	a
konkurovat	konkurovat	k5eAaImF	konkurovat
zahraničním	zahraniční	k2eAgFnPc3d1	zahraniční
univerzitám	univerzita	k1gFnPc3	univerzita
ve	v	k7c6	v
vědeckých	vědecký	k2eAgInPc6d1	vědecký
projektech	projekt	k1gInPc6	projekt
<g/>
,	,	kIx,	,
čehož	což	k3yQnSc2	což
jsou	být	k5eAaImIp3nP	být
ostatně	ostatně	k6eAd1	ostatně
důsledkem	důsledek	k1gInSc7	důsledek
nejen	nejen	k6eAd1	nejen
kvalitní	kvalitní	k2eAgFnPc4d1	kvalitní
vysoce	vysoce	k6eAd1	vysoce
impaktované	impaktovaný	k2eAgFnPc4d1	impaktovaná
publikace	publikace	k1gFnPc4	publikace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
mezinárodních	mezinárodní	k2eAgNnPc2d1	mezinárodní
ocenění	ocenění	k1gNnPc2	ocenění
pro	pro	k7c4	pro
pracovníky	pracovník	k1gMnPc4	pracovník
nebo	nebo	k8xC	nebo
studenty	student	k1gMnPc4	student
FJFI	FJFI	kA	FJFI
<g/>
.	.	kIx.	.
</s>
<s>
Samozřejmostí	samozřejmost	k1gFnSc7	samozřejmost
je	být	k5eAaImIp3nS	být
spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
mezinárodními	mezinárodní	k2eAgFnPc7d1	mezinárodní
institucemi	instituce	k1gFnPc7	instituce
a	a	k8xC	a
velkými	velký	k2eAgFnPc7d1	velká
firmami	firma	k1gFnPc7	firma
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
ČEZ	ČEZ	kA	ČEZ
fakulta	fakulta	k1gFnSc1	fakulta
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
provoz	provoz	k1gInSc4	provoz
a	a	k8xC	a
organizuje	organizovat	k5eAaBmIp3nS	organizovat
využití	využití	k1gNnSc1	využití
školního	školní	k2eAgInSc2d1	školní
jaderného	jaderný	k2eAgInSc2d1	jaderný
reaktoru	reaktor	k1gInSc2	reaktor
VR-1	VR-1	k1gMnSc1	VR-1
VRABEC	Vrabec	k1gMnSc1	Vrabec
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
unikátní	unikátní	k2eAgNnPc4d1	unikátní
zařízení	zařízení	k1gNnPc4	zařízení
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
rezortu	rezort	k1gInSc6	rezort
školství	školství	k1gNnSc2	školství
<g/>
.	.	kIx.	.
</s>
<s>
Výuky	výuka	k1gFnPc1	výuka
na	na	k7c6	na
lehkovodním	lehkovodní	k2eAgInSc6d1	lehkovodní
reaktoru	reaktor	k1gInSc6	reaktor
bazénového	bazénový	k2eAgInSc2d1	bazénový
typu	typ	k1gInSc2	typ
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
kmenových	kmenový	k2eAgMnPc2d1	kmenový
posluchačů	posluchač	k1gMnPc2	posluchač
FJFI	FJFI	kA	FJFI
v	v	k7c6	v
různé	různý	k2eAgFnSc6d1	různá
míře	míra	k1gFnSc6	míra
účastní	účastnit	k5eAaImIp3nP	účastnit
i	i	k9	i
studenti	student	k1gMnPc1	student
zhruba	zhruba	k6eAd1	zhruba
12	[number]	k4	12
fakult	fakulta	k1gFnPc2	fakulta
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
studentů	student	k1gMnPc2	student
ze	z	k7c2	z
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
exkurze	exkurze	k1gFnPc4	exkurze
s	s	k7c7	s
ukázkou	ukázka	k1gFnSc7	ukázka
provozu	provoz	k1gInSc2	provoz
jezdí	jezdit	k5eAaImIp3nS	jezdit
také	také	k9	také
stále	stále	k6eAd1	stále
větší	veliký	k2eAgInSc4d2	veliký
počet	počet	k1gInSc4	počet
středních	střední	k2eAgFnPc2d1	střední
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Dne	den	k1gInSc2	den
na	na	k7c6	na
Jaderce	Jaderka	k1gFnSc6	Jaderka
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
štěpného	štěpný	k2eAgInSc2d1	štěpný
reaktoru	reaktor	k1gInSc2	reaktor
fakulta	fakulta	k1gFnSc1	fakulta
spravuje	spravovat	k5eAaImIp3nS	spravovat
unikátní	unikátní	k2eAgInSc4d1	unikátní
pokusný	pokusný	k2eAgInSc4d1	pokusný
reaktor	reaktor	k1gInSc4	reaktor
pro	pro	k7c4	pro
zvládnutí	zvládnutí	k1gNnSc4	zvládnutí
řízené	řízený	k2eAgFnSc2d1	řízená
termojaderné	termojaderný	k2eAgFnSc2d1	termojaderná
fúze	fúze	k1gFnSc2	fúze
<g/>
,	,	kIx,	,
tokamak	tokamak	k1gMnSc1	tokamak
GOLEM	Golem	k1gMnSc1	Golem
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
získala	získat	k5eAaPmAgFnS	získat
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pod	pod	k7c7	pod
záštitou	záštita	k1gFnSc7	záštita
fakulty	fakulta	k1gFnSc2	fakulta
funguje	fungovat	k5eAaImIp3nS	fungovat
Studentská	studentský	k2eAgFnSc1d1	studentská
unie	unie	k1gFnSc1	unie
při	při	k7c6	při
FJFI	FJFI	kA	FJFI
ČVUT	ČVUT	kA	ČVUT
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
organizací	organizace	k1gFnSc7	organizace
akcí	akce	k1gFnPc2	akce
jako	jako	k8xS	jako
Noc	noc	k1gFnSc4	noc
na	na	k7c6	na
Jaderce	Jaderka	k1gFnSc6	Jaderka
<g/>
,	,	kIx,	,
reprezentační	reprezentační	k2eAgInSc1d1	reprezentační
ples	ples	k1gInSc1	ples
Všejaderná	Všejaderný	k2eAgFnSc1d1	Všejaderná
fúze	fúze	k1gFnSc1	fúze
či	či	k8xC	či
představení	představení	k1gNnSc1	představení
fakultního	fakultní	k2eAgInSc2d1	fakultní
divadelního	divadelní	k2eAgInSc2d1	divadelní
spolku	spolek	k1gInSc2	spolek
Nucleus	Nucleus	k1gInSc1	Nucleus
pečuje	pečovat	k5eAaImIp3nS	pečovat
o	o	k7c4	o
kulturní	kulturní	k2eAgInSc4d1	kulturní
život	život	k1gInSc4	život
studentstva	studentstvo	k1gNnSc2	studentstvo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fakulta	fakulta	k1gFnSc1	fakulta
architektury	architektura	k1gFnSc2	architektura
===	===	k?	===
</s>
</p>
<p>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
architektury	architektura	k1gFnSc2	architektura
(	(	kIx(	(
<g/>
FA	fa	kA	fa
<g/>
)	)	kIx)	)
ČVUT	ČVUT	kA	ČVUT
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc7d3	veliký
institucí	instituce	k1gFnSc7	instituce
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
vysokoškolské	vysokoškolský	k2eAgNnSc4d1	vysokoškolské
vzdělání	vzdělání	k1gNnSc4	vzdělání
ve	v	k7c6	v
studijních	studijní	k2eAgInPc6d1	studijní
programech	program	k1gInPc6	program
Architektura	architektura	k1gFnSc1	architektura
a	a	k8xC	a
urbanismus	urbanismus	k1gInSc1	urbanismus
<g/>
,	,	kIx,	,
Krajinářská	krajinářský	k2eAgFnSc1d1	krajinářská
architektura	architektura	k1gFnSc1	architektura
a	a	k8xC	a
Design	design	k1gInSc1	design
<g/>
.	.	kIx.	.
</s>
<s>
Fakultu	fakulta	k1gFnSc4	fakulta
sídlící	sídlící	k2eAgFnSc4d1	sídlící
v	v	k7c6	v
Nové	Nové	k2eAgFnSc6d1	Nové
budově	budova	k1gFnSc6	budova
ČVUT	ČVUT	kA	ČVUT
v	v	k7c6	v
akademickém	akademický	k2eAgInSc6d1	akademický
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
/	/	kIx~	/
<g/>
16	[number]	k4	16
studovalo	studovat	k5eAaImAgNnS	studovat
926	[number]	k4	926
studentů	student	k1gMnPc2	student
v	v	k7c6	v
bakalářském	bakalářský	k2eAgInSc6d1	bakalářský
cyklu	cyklus	k1gInSc6	cyklus
<g/>
,	,	kIx,	,
v	v	k7c6	v
navazujícím	navazující	k2eAgNnSc6d1	navazující
magisterském	magisterský	k2eAgNnSc6d1	magisterské
studiu	studio	k1gNnSc6	studio
630	[number]	k4	630
studentů	student	k1gMnPc2	student
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
90	[number]	k4	90
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
doktorském	doktorský	k2eAgInSc6d1	doktorský
programu	program	k1gInSc6	program
170	[number]	k4	170
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výuka	výuka	k1gFnSc1	výuka
architektury	architektura	k1gFnSc2	architektura
na	na	k7c6	na
Českém	český	k2eAgNnSc6d1	české
vysokém	vysoký	k2eAgNnSc6d1	vysoké
učení	učení	k1gNnSc6	učení
technickém	technický	k2eAgNnSc6d1	technické
má	mít	k5eAaImIp3nS	mít
hlubokou	hluboký	k2eAgFnSc4d1	hluboká
tradici	tradice	k1gFnSc4	tradice
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
základy	základ	k1gInPc1	základ
v	v	k7c6	v
samých	samý	k3xTgInPc6	samý
počátcích	počátek	k1gInPc6	počátek
existence	existence	k1gFnSc2	existence
Stavovské	stavovský	k2eAgFnSc2d1	stavovská
inženýrské	inženýrský	k2eAgFnSc2d1	inženýrská
školy	škola	k1gFnSc2	škola
(	(	kIx(	(
<g/>
1707	[number]	k4	1707
<g/>
)	)	kIx)	)
a	a	k8xC	a
pražské	pražský	k2eAgFnSc2d1	Pražská
polytechniky	polytechnika	k1gFnSc2	polytechnika
(	(	kIx(	(
<g/>
1803	[number]	k4	1803
-	-	kIx~	-
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
na	na	k7c6	na
výuce	výuka	k1gFnSc6	výuka
podílela	podílet	k5eAaImAgFnS	podílet
řada	řada	k1gFnSc1	řada
vynikajících	vynikající	k2eAgMnPc2d1	vynikající
českých	český	k2eAgMnPc2d1	český
architektů	architekt	k1gMnPc2	architekt
jako	jako	k8xS	jako
např.	např.	kA	např.
J.	J.	kA	J.
Fischer	Fischer	k1gMnSc1	Fischer
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Zítek	Zítek	k1gMnSc1	Zítek
a	a	k8xC	a
Josef	Josef	k1gMnSc1	Josef
Schulz	Schulz	k1gMnSc1	Schulz
nebo	nebo	k8xC	nebo
Antonín	Antonín	k1gMnSc1	Antonín
Balšánek	balšánek	k1gMnSc1	balšánek
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Fanta	Fanta	k1gMnSc1	Fanta
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Koula	Koula	k1gMnSc1	Koula
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
byla	být	k5eAaImAgFnS	být
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
architektury	architektura	k1gFnSc2	architektura
a	a	k8xC	a
pozemního	pozemní	k2eAgNnSc2d1	pozemní
stavitelství	stavitelství	k1gNnSc2	stavitelství
(	(	kIx(	(
<g/>
od	od	k7c2	od
1950	[number]	k4	1950
Fakulta	fakulta	k1gFnSc1	fakulta
<g/>
)	)	kIx)	)
jednou	jednou	k6eAd1	jednou
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
ČVUT	ČVUT	kA	ČVUT
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
sloučením	sloučení	k1gNnSc7	sloučení
několika	několik	k4yIc2	několik
fakult	fakulta	k1gFnPc2	fakulta
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Fakulta	fakulta	k1gFnSc1	fakulta
stavební	stavební	k2eAgFnSc1d1	stavební
<g/>
.	.	kIx.	.
</s>
<s>
Samostatná	samostatný	k2eAgFnSc1d1	samostatná
existence	existence	k1gFnSc1	existence
Fakulty	fakulta	k1gFnSc2	fakulta
architektury	architektura	k1gFnSc2	architektura
se	se	k3xPyFc4	se
obnovila	obnovit	k5eAaPmAgFnS	obnovit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
a	a	k8xC	a
studijní	studijní	k2eAgInSc4d1	studijní
směr	směr	k1gInSc4	směr
Pozemní	pozemní	k2eAgNnSc1d1	pozemní
stavitelství	stavitelství	k1gNnSc1	stavitelství
zůstal	zůstat	k5eAaPmAgInS	zůstat
součástí	součást	k1gFnSc7	součást
velké	velký	k2eAgFnSc2d1	velká
Fakulty	fakulta	k1gFnSc2	fakulta
stavební	stavební	k2eAgFnSc2d1	stavební
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
"	"	kIx"	"
<g/>
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
<g/>
"	"	kIx"	"
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
výrazným	výrazný	k2eAgFnPc3d1	výrazná
změnám	změna	k1gFnPc3	změna
v	v	k7c6	v
organizaci	organizace	k1gFnSc6	organizace
i	i	k8xC	i
struktuře	struktura	k1gFnSc6	struktura
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Dosavadní	dosavadní	k2eAgInSc1d1	dosavadní
klasický	klasický	k2eAgInSc1d1	klasický
sytém	sytý	k2eAgInSc6d1	sytý
typologicky	typologicky	k6eAd1	typologicky
rozdělených	rozdělený	k2eAgFnPc2d1	rozdělená
kateder	katedra	k1gFnPc2	katedra
nahradil	nahradit	k5eAaPmAgMnS	nahradit
systém	systém	k1gInSc4	systém
volnější	volný	k2eAgInSc4d2	volnější
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc4d1	založený
na	na	k7c6	na
kombinaci	kombinace	k1gFnSc6	kombinace
znalostní	znalostní	k2eAgFnSc2d1	znalostní
výuky	výuka	k1gFnSc2	výuka
s	s	k7c7	s
tvorbou	tvorba	k1gFnSc7	tvorba
ve	v	k7c6	v
"	"	kIx"	"
<g/>
vertikálním	vertikální	k2eAgInSc6d1	vertikální
ateliéru	ateliér	k1gInSc6	ateliér
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
vedle	vedle	k6eAd1	vedle
sebe	sebe	k3xPyFc4	sebe
pracují	pracovat	k5eAaImIp3nP	pracovat
na	na	k7c6	na
různých	různý	k2eAgInPc6d1	různý
typech	typ	k1gInPc6	typ
projektů	projekt	k1gInPc2	projekt
studenti	student	k1gMnPc1	student
druhého	druhý	k4xOgMnSc4	druhý
až	až	k6eAd1	až
pátého	pátý	k4xOgInSc2	pátý
ročníku	ročník	k1gInSc2	ročník
<g/>
.	.	kIx.	.
</s>
<s>
Ateliéry	ateliér	k1gInPc4	ateliér
vedou	vést	k5eAaImIp3nP	vést
zkušení	zkušený	k2eAgMnPc1d1	zkušený
praktici	praktik	k1gMnPc1	praktik
<g/>
,	,	kIx,	,
přední	přední	k2eAgMnPc1d1	přední
architekti	architekt	k1gMnPc1	architekt
<g/>
,	,	kIx,	,
urbanisté	urbanista	k1gMnPc1	urbanista
a	a	k8xC	a
designéři	designér	k1gMnPc1	designér
z	z	k7c2	z
ČR	ČR	kA	ČR
i	i	k8xC	i
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
/	/	kIx~	/
<g/>
16	[number]	k4	16
probíhala	probíhat	k5eAaImAgFnS	probíhat
na	na	k7c6	na
FA	fa	k1gNnSc6	fa
ČVUT	ČVUT	kA	ČVUT
výuka	výuka	k1gFnSc1	výuka
ve	v	k7c6	v
44	[number]	k4	44
vertikálních	vertikální	k2eAgInPc6d1	vertikální
atelierech	atelier	k1gInPc6	atelier
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Projekty	projekt	k1gInPc1	projekt
studentů	student	k1gMnPc2	student
Fakulty	fakulta	k1gFnSc2	fakulta
architektury	architektura	k1gFnSc2	architektura
nezůstávají	zůstávat	k5eNaImIp3nP	zůstávat
jen	jen	k9	jen
na	na	k7c6	na
papíře	papír	k1gInSc6	papír
<g/>
.	.	kIx.	.
</s>
<s>
Energeticky	energeticky	k6eAd1	energeticky
soběstačný	soběstačný	k2eAgInSc4d1	soběstačný
dům	dům	k1gInSc4	dům
AIR	AIR	kA	AIR
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgInSc7	který
se	se	k3xPyFc4	se
tým	tým	k1gInSc1	tým
studentů	student	k1gMnPc2	student
ČVUT	ČVUT	kA	ČVUT
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
soutěže	soutěž	k1gFnSc2	soutěž
Solar	Solar	k1gMnSc1	Solar
Decathlon	Decathlon	k1gInSc1	Decathlon
pořádané	pořádaný	k2eAgNnSc1d1	pořádané
Ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
energetiky	energetika	k1gFnSc2	energetika
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
Department	department	k1gInSc1	department
of	of	k?	of
Energy	Energ	k1gInPc1	Energ
–	–	k?	–
DOE	DOE	kA	DOE
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
informační	informační	k2eAgNnSc1d1	informační
centrum	centrum	k1gNnSc1	centrum
ČVUT	ČVUT	kA	ČVUT
v	v	k7c6	v
dejvickém	dejvický	k2eAgInSc6d1	dejvický
kampusu	kampus	k1gInSc6	kampus
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
Laboratoř	laboratoř	k1gFnSc1	laboratoř
ticha	ticho	k1gNnSc2	ticho
<g/>
,	,	kIx,	,
audiovizuální	audiovizuální	k2eAgFnSc4d1	audiovizuální
instalaci	instalace	k1gFnSc4	instalace
<g/>
,	,	kIx,	,
navrženou	navržený	k2eAgFnSc4d1	navržená
pro	pro	k7c4	pro
český	český	k2eAgInSc4d1	český
pavilon	pavilon	k1gInSc4	pavilon
Světové	světový	k2eAgFnSc2d1	světová
výstavy	výstava	k1gFnSc2	výstava
EXPO	Expo	k1gNnSc1	Expo
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
navštívit	navštívit	k5eAaPmF	navštívit
v	v	k7c4	v
Národní	národní	k2eAgNnSc4d1	národní
zemědělské	zemědělský	k2eAgNnSc4d1	zemědělské
muzeum	muzeum	k1gNnSc4	muzeum
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fakulta	fakulta	k1gFnSc1	fakulta
dopravní	dopravní	k2eAgFnSc1d1	dopravní
===	===	k?	===
</s>
</p>
<p>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
má	mít	k5eAaImIp3nS	mít
unikátní	unikátní	k2eAgInSc4d1	unikátní
systém	systém	k1gInSc4	systém
projektově	projektově	k6eAd1	projektově
orientované	orientovaný	k2eAgFnSc2d1	orientovaná
výuky	výuka	k1gFnSc2	výuka
<g/>
,	,	kIx,	,
studenti	student	k1gMnPc1	student
tak	tak	k6eAd1	tak
od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
semestru	semestr	k1gInSc2	semestr
pracují	pracovat	k5eAaImIp3nP	pracovat
na	na	k7c6	na
konkrétních	konkrétní	k2eAgInPc6d1	konkrétní
projektech	projekt	k1gInPc6	projekt
úzce	úzko	k6eAd1	úzko
spjatých	spjatý	k2eAgMnPc2d1	spjatý
s	s	k7c7	s
praxí	praxe	k1gFnSc7	praxe
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
této	tento	k3xDgFnSc2	tento
výuky	výuka	k1gFnSc2	výuka
jsou	být	k5eAaImIp3nP	být
řešena	řešen	k2eAgNnPc4d1	řešeno
aktuální	aktuální	k2eAgNnPc4d1	aktuální
témata	téma	k1gNnPc4	téma
a	a	k8xC	a
problematiky	problematika	k1gFnPc4	problematika
z	z	k7c2	z
oboru	obor	k1gInSc2	obor
dopravy	doprava	k1gFnSc2	doprava
a	a	k8xC	a
telekomunikací	telekomunikace	k1gFnPc2	telekomunikace
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
unikátní	unikátní	k2eAgInSc1d1	unikátní
systém	systém	k1gInSc1	systém
studia	studio	k1gNnSc2	studio
dává	dávat	k5eAaImIp3nS	dávat
studentům	student	k1gMnPc3	student
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
vybrat	vybrat	k5eAaPmF	vybrat
si	se	k3xPyFc3	se
svou	svůj	k3xOyFgFnSc7	svůj
specializací	specializace	k1gFnSc7	specializace
až	až	k9	až
po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
1,5	[number]	k4	1,5
roku	rok	k1gInSc2	rok
všeobecného	všeobecný	k2eAgNnSc2d1	všeobecné
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
na	na	k7c6	na
fakultě	fakulta	k1gFnSc6	fakulta
studuje	studovat	k5eAaImIp3nS	studovat
1012	[number]	k4	1012
studentů	student	k1gMnPc2	student
ať	ať	k8xC	ať
už	už	k6eAd1	už
v	v	k7c6	v
bakalářském	bakalářský	k2eAgMnSc6d1	bakalářský
<g/>
,	,	kIx,	,
magisterském	magisterský	k2eAgInSc6d1	magisterský
či	či	k8xC	či
doktorském	doktorský	k2eAgInSc6d1	doktorský
studijním	studijní	k2eAgInSc6d1	studijní
programu	program	k1gInSc6	program
<g/>
.	.	kIx.	.
</s>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
je	být	k5eAaImIp3nS	být
lokalizována	lokalizovat	k5eAaBmNgFnS	lokalizovat
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
detašované	detašovaný	k2eAgNnSc4d1	detašované
pracoviště	pracoviště	k1gNnSc4	pracoviště
v	v	k7c6	v
Děčíně	Děčín	k1gInSc6	Děčín
s	s	k7c7	s
plnohodnotnou	plnohodnotný	k2eAgFnSc7d1	plnohodnotná
výukou	výuka	k1gFnSc7	výuka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
Fakulta	fakulta	k1gFnSc1	fakulta
dopravní	dopravní	k2eAgFnSc1d1	dopravní
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
nejmladších	mladý	k2eAgInPc2d3	nejmladší
na	na	k7c6	na
ČVUT	ČVUT	kA	ČVUT
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nP	patřit
její	její	k3xOp3gMnPc1	její
absolventi	absolvent	k1gMnPc1	absolvent
k	k	k7c3	k
těm	ten	k3xDgNnPc3	ten
nejžádanějším	žádaný	k2eAgNnPc3d3	nejžádanější
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
práce	práce	k1gFnSc2	práce
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
spojených	spojený	k2eAgInPc2d1	spojený
s	s	k7c7	s
dopravou	doprava	k1gFnSc7	doprava
<g/>
,	,	kIx,	,
telematikou	telematika	k1gFnSc7	telematika
a	a	k8xC	a
telekomunikacemi	telekomunikace	k1gFnPc7	telekomunikace
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
25	[number]	k4	25
<g/>
letou	letý	k2eAgFnSc4d1	letá
existenci	existence	k1gFnSc4	existence
vybudovala	vybudovat	k5eAaPmAgFnS	vybudovat
mnoho	mnoho	k6eAd1	mnoho
unikátních	unikátní	k2eAgNnPc2d1	unikátní
vědecko-výzkumných	vědeckoýzkumný	k2eAgNnPc2d1	vědecko-výzkumné
pracovišť	pracoviště	k1gNnPc2	pracoviště
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgMnPc6	který
projektová	projektový	k2eAgFnSc1d1	projektová
výuka	výuka	k1gFnSc1	výuka
probíhá	probíhat	k5eAaImIp3nS	probíhat
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterým	který	k3yQgInPc3	který
je	být	k5eAaImIp3nS	být
úzce	úzko	k6eAd1	úzko
spjata	spjat	k2eAgFnSc1d1	spjata
s	s	k7c7	s
praxí	praxe	k1gFnSc7	praxe
a	a	k8xC	a
podílí	podílet	k5eAaImIp3nS	podílet
se	se	k3xPyFc4	se
na	na	k7c4	na
řešení	řešení	k1gNnSc4	řešení
řady	řada	k1gFnSc2	řada
grantových	grantový	k2eAgInPc2d1	grantový
i	i	k8xC	i
komerčních	komerční	k2eAgInPc2d1	komerční
projektů	projekt	k1gInPc2	projekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Například	například	k6eAd1	například
na	na	k7c6	na
modelu	model	k1gInSc6	model
v	v	k7c6	v
Dopravním	dopravní	k2eAgInSc6d1	dopravní
sále	sál	k1gInSc6	sál
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
bez	bez	k7c2	bez
rizika	riziko	k1gNnSc2	riziko
negativních	negativní	k2eAgInPc2d1	negativní
dopadů	dopad	k1gInPc2	dopad
na	na	k7c4	na
skutečný	skutečný	k2eAgInSc4d1	skutečný
provoz	provoz	k1gInSc4	provoz
nacvičovat	nacvičovat	k5eAaImF	nacvičovat
technologii	technologie	k1gFnSc4	technologie
řízení	řízení	k1gNnSc2	řízení
železniční	železniční	k2eAgFnSc2d1	železniční
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
řešení	řešení	k1gNnSc1	řešení
mimořádných	mimořádný	k2eAgFnPc2d1	mimořádná
situací	situace	k1gFnPc2	situace
<g/>
,	,	kIx,	,
poruchových	poruchový	k2eAgInPc2d1	poruchový
stavů	stav	k1gInPc2	stav
apod.	apod.	kA	apod.
Dalším	další	k2eAgInSc7d1	další
z	z	k7c2	z
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
studentských	studentský	k2eAgInPc2d1	studentský
projektů	projekt	k1gInPc2	projekt
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
projekt	projekt	k1gInSc1	projekt
MOTOSTUDENT	MOTOSTUDENT	kA	MOTOSTUDENT
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
studenti	student	k1gMnPc1	student
vyvíjí	vyvíjet	k5eAaImIp3nP	vyvíjet
motocykl	motocykl	k1gInSc4	motocykl
vlastní	vlastní	k2eAgFnSc2d1	vlastní
konstrukce	konstrukce	k1gFnSc2	konstrukce
a	a	k8xC	a
zúčastní	zúčastnit	k5eAaPmIp3nS	zúčastnit
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
klání	klání	k1gNnSc2	klání
studentských	studentský	k2eAgInPc2d1	studentský
týmů	tým	k1gInPc2	tým
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
spolupráce	spolupráce	k1gFnSc2	spolupráce
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
studia	studio	k1gNnSc2	studio
je	být	k5eAaImIp3nS	být
fakulta	fakulta	k1gFnSc1	fakulta
unikátní	unikátní	k2eAgFnSc1d1	unikátní
především	především	k6eAd1	především
svými	svůj	k3xOyFgFnPc7	svůj
dual-degree	dualegree	k1gFnPc7	dual-degree
studijními	studijní	k2eAgInPc7d1	studijní
programy	program	k1gInPc7	program
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k6eAd1	už
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
evropskými	evropský	k2eAgFnPc7d1	Evropská
univerzitami	univerzita	k1gFnPc7	univerzita
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
s	s	k7c7	s
americkou	americký	k2eAgFnSc7d1	americká
univerzitou	univerzita	k1gFnSc7	univerzita
UTEP	UTEP	kA	UTEP
ve	v	k7c6	v
studijním	studijní	k2eAgInSc6d1	studijní
programu	program	k1gInSc6	program
SMART	SMART	kA	SMART
CITIES	CITIES	kA	CITIES
<g/>
.	.	kIx.	.
</s>
<s>
Absolventi	absolvent	k1gMnPc1	absolvent
těchto	tento	k3xDgFnPc2	tento
dual-degree	dualegree	k1gFnPc2	dual-degree
studijních	studijní	k2eAgInPc2d1	studijní
programů	program	k1gInPc2	program
získávají	získávat	k5eAaImIp3nP	získávat
závěrečné	závěrečný	k2eAgInPc1d1	závěrečný
akademické	akademický	k2eAgInPc1d1	akademický
tituly	titul	k1gInPc1	titul
z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fakulta	fakulta	k1gFnSc1	fakulta
biomedicínského	biomedicínský	k2eAgNnSc2d1	biomedicínské
inženýrství	inženýrství	k1gNnSc2	inženýrství
===	===	k?	===
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
veřejnou	veřejný	k2eAgFnSc7d1	veřejná
vysokou	vysoký	k2eAgFnSc7d1	vysoká
školou	škola	k1gFnSc7	škola
ve	v	k7c6	v
Středočeském	středočeský	k2eAgInSc6d1	středočeský
kraji	kraj	k1gInSc6	kraj
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Kladně	Kladno	k1gNnSc6	Kladno
a	a	k8xC	a
druhou	druhý	k4xOgFnSc7	druhý
nejmladší	mladý	k2eAgFnSc7d3	nejmladší
fakultou	fakulta	k1gFnSc7	fakulta
Českého	český	k2eAgNnSc2d1	české
vysokého	vysoký	k2eAgNnSc2d1	vysoké
učení	učení	k1gNnSc2	učení
technického	technický	k2eAgNnSc2d1	technické
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Složení	složení	k1gNnSc1	složení
studijních	studijní	k2eAgInPc2d1	studijní
oborů	obor	k1gInPc2	obor
<g/>
,	,	kIx,	,
charakter	charakter	k1gInSc4	charakter
výuky	výuka	k1gFnSc2	výuka
i	i	k8xC	i
zaměření	zaměření	k1gNnSc2	zaměření
vědecké	vědecký	k2eAgFnSc2d1	vědecká
a	a	k8xC	a
výzkumné	výzkumný	k2eAgFnSc2d1	výzkumná
činnosti	činnost	k1gFnSc2	činnost
vycházejí	vycházet	k5eAaImIp3nP	vycházet
z	z	k7c2	z
interdisciplinárního	interdisciplinární	k2eAgInSc2d1	interdisciplinární
charakteru	charakter	k1gInSc2	charakter
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
zahrnujícího	zahrnující	k2eAgNnSc2d1	zahrnující
spojení	spojení	k1gNnSc2	spojení
techniky	technika	k1gFnSc2	technika
<g/>
,	,	kIx,	,
biologie	biologie	k1gFnSc2	biologie
i	i	k8xC	i
medicíny	medicína	k1gFnSc2	medicína
<g/>
,	,	kIx,	,
s	s	k7c7	s
důrazem	důraz	k1gInSc7	důraz
na	na	k7c4	na
praktickou	praktický	k2eAgFnSc4d1	praktická
výuku	výuka	k1gFnSc4	výuka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
na	na	k7c6	na
FBMI	FBMI	kA	FBMI
studuje	studovat	k5eAaImIp3nS	studovat
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
formách	forma	k1gFnPc6	forma
studia	studio	k1gNnSc2	studio
(	(	kIx(	(
<g/>
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
oborech	obor	k1gInPc6	obor
také	také	k9	také
v	v	k7c6	v
jazycích	jazyk	k1gInPc6	jazyk
anglickém	anglický	k2eAgNnSc6d1	anglické
a	a	k8xC	a
ruském	ruský	k2eAgMnSc6d1	ruský
<g/>
)	)	kIx)	)
téměř	téměř	k6eAd1	téměř
2	[number]	k4	2
000	[number]	k4	000
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
nabízí	nabízet	k5eAaImIp3nS	nabízet
9	[number]	k4	9
bakalářských	bakalářský	k2eAgInPc2d1	bakalářský
studijních	studijní	k2eAgInPc2d1	studijní
oborů	obor	k1gInPc2	obor
<g/>
,	,	kIx,	,
4	[number]	k4	4
navazující	navazující	k2eAgFnPc1d1	navazující
magisterské	magisterský	k2eAgFnPc1d1	magisterská
a	a	k8xC	a
2	[number]	k4	2
doktorské	doktorský	k2eAgInPc4d1	doktorský
studijní	studijní	k2eAgInPc4d1	studijní
obory	obor	k1gInPc4	obor
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnPc1	její
absolventi	absolvent	k1gMnPc1	absolvent
mají	mít	k5eAaImIp3nP	mít
velmi	velmi	k6eAd1	velmi
dobré	dobrý	k2eAgNnSc4d1	dobré
uplatnění	uplatnění	k1gNnSc4	uplatnění
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
třináct	třináct	k4xCc4	třináct
let	léto	k1gNnPc2	léto
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
vychovala	vychovat	k5eAaPmAgFnS	vychovat
téměř	téměř	k6eAd1	téměř
1	[number]	k4	1
600	[number]	k4	600
absolventů	absolvent	k1gMnPc2	absolvent
bakalářského	bakalářský	k2eAgNnSc2d1	bakalářské
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
1	[number]	k4	1
000	[number]	k4	000
absolventů	absolvent	k1gMnPc2	absolvent
magisterského	magisterský	k2eAgNnSc2d1	magisterské
studia	studio	k1gNnSc2	studio
a	a	k8xC	a
27	[number]	k4	27
absolventů	absolvent	k1gMnPc2	absolvent
doktorského	doktorský	k2eAgNnSc2d1	doktorské
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
<g/>
FBMI	FBMI	kA	FBMI
disponuje	disponovat	k5eAaBmIp3nS	disponovat
nejmodernějším	moderní	k2eAgNnSc7d3	nejmodernější
laboratorním	laboratorní	k2eAgNnSc7d1	laboratorní
vybavením	vybavení	k1gNnSc7	vybavení
pro	pro	k7c4	pro
experimentální	experimentální	k2eAgFnSc4d1	experimentální
výuku	výuka	k1gFnSc4	výuka
a	a	k8xC	a
výzkum	výzkum	k1gInSc4	výzkum
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
čítající	čítající	k2eAgMnSc1d1	čítající
na	na	k7c4	na
30	[number]	k4	30
specializovaných	specializovaný	k2eAgFnPc2d1	specializovaná
laboratoří	laboratoř	k1gFnPc2	laboratoř
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
těch	ten	k3xDgFnPc2	ten
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
simulují	simulovat	k5eAaImIp3nP	simulovat
prostředí	prostředí	k1gNnSc4	prostředí
vybraných	vybraný	k2eAgNnPc2d1	vybrané
oddělení	oddělení	k1gNnPc2	oddělení
urgentní	urgentní	k2eAgFnSc2d1	urgentní
medicíny	medicína	k1gFnSc2	medicína
a	a	k8xC	a
intenzivní	intenzivní	k2eAgFnSc2d1	intenzivní
péče	péče	k1gFnSc2	péče
v	v	k7c6	v
nemocnicích	nemocnice	k1gFnPc6	nemocnice
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
lze	lze	k6eAd1	lze
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
ČR	ČR	kA	ČR
považovat	považovat	k5eAaImF	považovat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
biomedicínského	biomedicínský	k2eAgNnSc2d1	biomedicínské
inženýrství	inženýrství	k1gNnSc2	inženýrství
za	za	k7c4	za
unikátní	unikátní	k2eAgNnPc4d1	unikátní
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
unikátním	unikátní	k2eAgFnPc3d1	unikátní
oblastem	oblast	k1gFnPc3	oblast
vědy	věda	k1gFnSc2	věda
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
výzkum	výzkum	k1gInSc1	výzkum
nanotechnologií	nanotechnologie	k1gFnPc2	nanotechnologie
pro	pro	k7c4	pro
zdravotnictví	zdravotnictví	k1gNnSc4	zdravotnictví
a	a	k8xC	a
senzorika	senzorikum	k1gNnPc4	senzorikum
pro	pro	k7c4	pro
biomedicínu	biomedicína	k1gFnSc4	biomedicína
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
oblast	oblast	k1gFnSc1	oblast
biometrických	biometrický	k2eAgInPc2d1	biometrický
systémů	systém	k1gInPc2	systém
spojená	spojený	k2eAgFnSc1d1	spojená
se	s	k7c7	s
snímáním	snímání	k1gNnSc7	snímání
<g/>
,	,	kIx,	,
zpracováním	zpracování	k1gNnSc7	zpracování
<g/>
,	,	kIx,	,
přenosem	přenos	k1gInSc7	přenos
<g/>
,	,	kIx,	,
archivací	archivace	k1gFnSc7	archivace
biologických	biologický	k2eAgInPc2d1	biologický
signálů	signál	k1gInPc2	signál
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgNnSc4d1	nacházející
uplatnění	uplatnění	k1gNnSc4	uplatnění
také	také	k9	také
v	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
katastrof	katastrofa	k1gFnPc2	katastrofa
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
výzkum	výzkum	k1gInSc1	výzkum
interakce	interakce	k1gFnSc1	interakce
XUV	XUV	kA	XUV
záření	záření	k1gNnSc1	záření
s	s	k7c7	s
biologickými	biologický	k2eAgInPc7d1	biologický
objekty	objekt	k1gInPc7	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Unikátní	unikátní	k2eAgInPc4d1	unikátní
výsledky	výsledek	k1gInPc4	výsledek
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
fakulta	fakulta	k1gFnSc1	fakulta
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
nekonvenční	konvenční	k2eNgFnSc2d1	nekonvenční
umělé	umělý	k2eAgFnSc2d1	umělá
plicní	plicní	k2eAgFnSc2d1	plicní
ventilace	ventilace	k1gFnSc2	ventilace
<g/>
,	,	kIx,	,
ve	v	k7c6	v
výzkumu	výzkum	k1gInSc6	výzkum
kvantifikace	kvantifikace	k1gFnSc2	kvantifikace
rehabilitačního	rehabilitační	k2eAgInSc2d1	rehabilitační
procesu	proces	k1gInSc2	proces
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vyhodnocování	vyhodnocování	k1gNnSc6	vyhodnocování
polohy	poloha	k1gFnSc2	poloha
těla	tělo	k1gNnSc2	tělo
i	i	k8xC	i
končetin	končetina	k1gFnPc2	končetina
a	a	k8xC	a
očí	oko	k1gNnPc2	oko
<g/>
.	.	kIx.	.
</s>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
zavádí	zavádět	k5eAaImIp3nS	zavádět
metody	metoda	k1gFnPc4	metoda
Helth	Helth	k1gInSc1	Helth
Technology	technolog	k1gMnPc4	technolog
Assessment	Assessment	k1gInSc1	Assessment
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Škola	škola	k1gFnSc1	škola
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
širokou	široký	k2eAgFnSc4d1	široká
výukovou	výukový	k2eAgFnSc4d1	výuková
základnu	základna	k1gFnSc4	základna
pro	pro	k7c4	pro
budoucí	budoucí	k2eAgMnPc4d1	budoucí
biomedicínské	biomedicínský	k2eAgMnPc4d1	biomedicínský
techniky	technik	k1gMnPc4	technik
a	a	k8xC	a
biomedicínské	biomedicínský	k2eAgMnPc4d1	biomedicínský
inženýry	inženýr	k1gMnPc4	inženýr
zahrnující	zahrnující	k2eAgInSc1d1	zahrnující
kompletní	kompletní	k2eAgInSc1d1	kompletní
unikátní	unikátní	k2eAgInSc1d1	unikátní
soubor	soubor	k1gInSc1	soubor
testerů	tester	k1gInPc2	tester
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
zdravotnických	zdravotnický	k2eAgInPc2d1	zdravotnický
elektrických	elektrický	k2eAgInPc2d1	elektrický
přístrojů	přístroj	k1gInPc2	přístroj
od	od	k7c2	od
předních	přední	k2eAgMnPc2d1	přední
světových	světový	k2eAgMnPc2d1	světový
výrobců	výrobce	k1gMnPc2	výrobce
<g/>
,	,	kIx,	,
analyzátorů	analyzátor	k1gInPc2	analyzátor
a	a	k8xC	a
simulátorů	simulátor	k1gInPc2	simulátor
vybraných	vybraný	k2eAgInPc2d1	vybraný
fyziologických	fyziologický	k2eAgInPc2d1	fyziologický
subsystémů	subsystém	k1gInPc2	subsystém
<g/>
,	,	kIx,	,
či	či	k8xC	či
parametrů	parametr	k1gInPc2	parametr
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
též	též	k9	též
unikátním	unikátní	k2eAgInSc7d1	unikátní
reálným	reálný	k2eAgInSc7d1	reálný
modelem	model	k1gInSc7	model
řídicího	řídicí	k2eAgInSc2d1	řídicí
a	a	k8xC	a
monitorovacího	monitorovací	k2eAgInSc2d1	monitorovací
systému	systém	k1gInSc2	systém
MEDICS	MEDICS	kA	MEDICS
pro	pro	k7c4	pro
elektrické	elektrický	k2eAgInPc4d1	elektrický
rozvody	rozvod	k1gInPc4	rozvod
v	v	k7c6	v
nemocničních	nemocniční	k2eAgNnPc6d1	nemocniční
zařízeních	zařízení	k1gNnPc6	zařízení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
zázemí	zázemí	k1gNnSc1	zázemí
pro	pro	k7c4	pro
pacientskou	pacientský	k2eAgFnSc4d1	pacientská
simulaci	simulace	k1gFnSc4	simulace
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
nejmodernějších	moderní	k2eAgInPc2d3	nejmodernější
prostředků	prostředek	k1gInPc2	prostředek
zdravotnické	zdravotnický	k2eAgFnSc2d1	zdravotnická
techniky	technika	k1gFnSc2	technika
a	a	k8xC	a
pro	pro	k7c4	pro
výuku	výuka	k1gFnSc4	výuka
biomedicínské	biomedicínský	k2eAgFnSc2d1	biomedicínská
informatiky	informatika	k1gFnSc2	informatika
včetně	včetně	k7c2	včetně
možnosti	možnost	k1gFnSc2	možnost
si	se	k3xPyFc3	se
vyzkoušet	vyzkoušet	k5eAaPmF	vyzkoušet
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
rolích	role	k1gFnPc6	role
nejčastěji	často	k6eAd3	často
používané	používaný	k2eAgInPc4d1	používaný
nemocniční	nemocniční	k2eAgInPc4d1	nemocniční
informační	informační	k2eAgInPc4d1	informační
systémy	systém	k1gInPc4	systém
v	v	k7c6	v
nemocničních	nemocniční	k2eAgNnPc6d1	nemocniční
zařízeních	zařízení	k1gNnPc6	zařízení
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fakulta	fakulta	k1gFnSc1	fakulta
informačních	informační	k2eAgFnPc2d1	informační
technologií	technologie	k1gFnPc2	technologie
===	===	k?	===
</s>
</p>
<p>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
informačních	informační	k2eAgFnPc2d1	informační
technologií	technologie	k1gFnPc2	technologie
je	být	k5eAaImIp3nS	být
nejmladší	mladý	k2eAgFnSc7d3	nejmladší
fakultou	fakulta	k1gFnSc7	fakulta
ČVUT	ČVUT	kA	ČVUT
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2009	[number]	k4	2009
a	a	k8xC	a
sídlí	sídlet	k5eAaImIp3nS	sídlet
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Fakultou	fakulta	k1gFnSc7	fakulta
architektury	architektura	k1gFnSc2	architektura
v	v	k7c6	v
Nové	Nové	k2eAgFnSc6d1	Nové
budově	budova	k1gFnSc6	budova
ČVUT	ČVUT	kA	ČVUT
<g/>
.	.	kIx.	.
</s>
<s>
Škola	škola	k1gFnSc1	škola
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
celkem	celkem	k6eAd1	celkem
119	[number]	k4	119
akademiků	akademik	k1gMnPc2	akademik
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
7	[number]	k4	7
profesorů	profesor	k1gMnPc2	profesor
<g/>
,	,	kIx,	,
21	[number]	k4	21
docentů	docent	k1gMnPc2	docent
a	a	k8xC	a
91	[number]	k4	91
odborných	odborný	k2eAgMnPc2d1	odborný
asistentů	asistent	k1gMnPc2	asistent
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
akademickém	akademický	k2eAgInSc6d1	akademický
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
<g/>
/	/	kIx~	/
<g/>
2018	[number]	k4	2018
studuje	studovat	k5eAaImIp3nS	studovat
na	na	k7c6	na
fakultě	fakulta	k1gFnSc6	fakulta
1642	[number]	k4	1642
studentů	student	k1gMnPc2	student
ve	v	k7c6	v
3	[number]	k4	3
studijních	studijní	k2eAgInPc6d1	studijní
programech	program	k1gInPc6	program
a	a	k8xC	a
16	[number]	k4	16
studijních	studijní	k2eAgInPc6d1	studijní
oborech	obor	k1gInPc6	obor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Škola	škola	k1gFnSc1	škola
má	mít	k5eAaImIp3nS	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
mnoho	mnoho	k4c4	mnoho
moderních	moderní	k2eAgFnPc2d1	moderní
laboratoří	laboratoř	k1gFnPc2	laboratoř
a	a	k8xC	a
odborných	odborný	k2eAgNnPc2d1	odborné
pracovišť	pracoviště	k1gNnPc2	pracoviště
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
laboratoř	laboratoř	k1gFnSc4	laboratoř
etického	etický	k2eAgNnSc2d1	etické
hackování	hackování	k1gNnSc2	hackování
<g/>
,	,	kIx,	,
laboratoř	laboratoř	k1gFnSc1	laboratoř
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
aspektů	aspekt	k1gInPc2	aspekt
čipových	čipový	k2eAgFnPc2d1	čipová
karet	kareta	k1gFnPc2	kareta
<g/>
,	,	kIx,	,
laboratoř	laboratoř	k1gFnSc1	laboratoř
3D	[number]	k4	3D
tisku	tisk	k1gInSc2	tisk
<g/>
,	,	kIx,	,
síťová	síťový	k2eAgFnSc1d1	síťová
multimediální	multimediální	k2eAgFnSc1d1	multimediální
laboratoř	laboratoř	k1gFnSc1	laboratoř
SAGElab	SAGElab	k1gMnSc1	SAGElab
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
Každoročně	každoročně	k6eAd1	každoročně
škola	škola	k1gFnSc1	škola
pořádá	pořádat	k5eAaImIp3nS	pořádat
významné	významný	k2eAgFnPc4d1	významná
konference	konference	k1gFnPc4	konference
<g/>
,	,	kIx,	,
např.	např.	kA	např.
LinuxDays	LinuxDays	k1gInSc1	LinuxDays
(	(	kIx(	(
<g/>
mezinárodní	mezinárodní	k2eAgFnPc1d1	mezinárodní
komunitní	komunitní	k2eAgFnPc1d1	komunitní
konference	konference	k1gFnPc1	konference
týkající	týkající	k2eAgFnPc1d1	týkající
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
svobodného	svobodný	k2eAgInSc2d1	svobodný
softwaru	software	k1gInSc2	software
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
P2D2	P2D2	k1gFnSc1	P2D2
(	(	kIx(	(
<g/>
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
vývojářská	vývojářský	k2eAgFnSc1d1	vývojářská
konference	konference	k1gFnSc1	konference
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
relačním	relační	k2eAgInSc7d1	relační
a	a	k8xC	a
open-source	openourka	k1gFnSc6	open-sourka
databázovým	databázový	k2eAgInSc7d1	databázový
strojem	stroj	k1gInSc7	stroj
PostgreSQL	PostgreSQL	k1gFnPc2	PostgreSQL
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Prague	Pragu	k1gInSc2	Pragu
Stringology	Stringolog	k1gMnPc4	Stringolog
Conference	Conferenec	k1gInSc2	Conferenec
(	(	kIx(	(
<g/>
vědecká	vědecký	k2eAgFnSc1d1	vědecká
<g />
.	.	kIx.	.
</s>
<s>
konference	konference	k1gFnPc1	konference
z	z	k7c2	z
oblastí	oblast	k1gFnPc2	oblast
stringologie	stringologie	k1gFnSc2	stringologie
<g/>
,	,	kIx,	,
překladačů	překladač	k1gInPc2	překladač
<g/>
,	,	kIx,	,
komprese	komprese	k1gFnSc2	komprese
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
arbologie	arbologie	k1gFnSc2	arbologie
a	a	k8xC	a
konečných	konečný	k2eAgInPc2d1	konečný
automatů	automat	k1gInPc2	automat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
LawFIT	LawFIT	k1gFnSc1	LawFIT
(	(	kIx(	(
<g/>
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	s	k7c7	s
právem	právo	k1gNnSc7	právo
v	v	k7c6	v
IT	IT	kA	IT
oblasti	oblast	k1gFnSc6	oblast
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ARCS	ARCS	kA	ARCS
(	(	kIx(	(
<g/>
nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
evropská	evropský	k2eAgFnSc1d1	Evropská
vědecká	vědecký	k2eAgFnSc1d1	vědecká
konference	konference	k1gFnSc1	konference
o	o	k7c6	o
počítačových	počítačový	k2eAgFnPc6d1	počítačová
architekturách	architektura	k1gFnPc6	architektura
a	a	k8xC	a
operačních	operační	k2eAgInPc6d1	operační
systémech	systém	k1gInPc6	systém
<g/>
)	)	kIx)	)
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Masarykův	Masarykův	k2eAgInSc4d1	Masarykův
ústav	ústav	k1gInSc4	ústav
vyšších	vysoký	k2eAgFnPc2d2	vyšší
studií	studie	k1gFnPc2	studie
===	===	k?	===
</s>
</p>
<p>
<s>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
oblastmi	oblast	k1gFnPc7	oblast
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
Masarykova	Masarykův	k2eAgInSc2d1	Masarykův
ústavu	ústav	k1gInSc2	ústav
vyšších	vysoký	k2eAgFnPc2d2	vyšší
studií	studie	k1gFnPc2	studie
jsou	být	k5eAaImIp3nP	být
ekonomické	ekonomický	k2eAgFnPc1d1	ekonomická
a	a	k8xC	a
manažerské	manažerský	k2eAgNnSc1d1	manažerské
vzdělávání	vzdělávání	k1gNnSc1	vzdělávání
<g/>
,	,	kIx,	,
inženýrská	inženýrský	k2eAgFnSc1d1	inženýrská
pedagogika	pedagogika	k1gFnSc1	pedagogika
a	a	k8xC	a
jazyková	jazykový	k2eAgFnSc1d1	jazyková
příprava	příprava	k1gFnSc1	příprava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
se	se	k3xPyFc4	se
MÚVS	MÚVS	kA	MÚVS
snaží	snažit	k5eAaImIp3nP	snažit
obsáhnout	obsáhnout	k5eAaPmF	obsáhnout
kompletní	kompletní	k2eAgNnSc4d1	kompletní
portfolio	portfolio	k1gNnSc4	portfolio
vysokoškolských	vysokoškolský	k2eAgInPc2d1	vysokoškolský
vzdělávacích	vzdělávací	k2eAgInPc2d1	vzdělávací
programů	program	k1gInPc2	program
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
propojují	propojovat	k5eAaImIp3nP	propojovat
technické	technický	k2eAgInPc4d1	technický
předměty	předmět	k1gInPc4	předmět
s	s	k7c7	s
předměty	předmět	k1gInPc7	předmět
humanitními	humanitní	k2eAgInPc7d1	humanitní
a	a	k8xC	a
ekonomickými	ekonomický	k2eAgInPc7d1	ekonomický
a	a	k8xC	a
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
tak	tak	k6eAd1	tak
široké	široký	k2eAgNnSc4d1	široké
spektrum	spektrum	k1gNnSc4	spektrum
technických	technický	k2eAgInPc2d1	technický
oborů	obor	k1gInPc2	obor
na	na	k7c6	na
ČVUT	ČVUT	kA	ČVUT
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Aktuálně	aktuálně	k6eAd1	aktuálně
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
/	/	kIx~	/
<g/>
18	[number]	k4	18
<g/>
)	)	kIx)	)
zde	zde	k6eAd1	zde
studuje	studovat	k5eAaImIp3nS	studovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1300	[number]	k4	1300
studentů	student	k1gMnPc2	student
v	v	k7c6	v
bakalářských	bakalářský	k2eAgInPc6d1	bakalářský
<g/>
,	,	kIx,	,
navazujících	navazující	k2eAgInPc6d1	navazující
magisterských	magisterský	k2eAgInPc6d1	magisterský
a	a	k8xC	a
doktorských	doktorský	k2eAgInPc6d1	doktorský
studijních	studijní	k2eAgInPc6d1	studijní
programech	program	k1gInPc6	program
<g/>
.	.	kIx.	.
</s>
<s>
Paralelně	paralelně	k6eAd1	paralelně
MÚVS	MÚVS	kA	MÚVS
nabízí	nabízet	k5eAaImIp3nS	nabízet
výukové	výukový	k2eAgInPc4d1	výukový
programy	program	k1gInPc4	program
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
celoživotního	celoživotní	k2eAgNnSc2d1	celoživotní
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
<g/>
,	,	kIx,	,
určené	určený	k2eAgInPc4d1	určený
zejména	zejména	k9	zejména
absolventům	absolvent	k1gMnPc3	absolvent
českých	český	k2eAgFnPc2d1	Česká
a	a	k8xC	a
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
realizované	realizovaný	k2eAgFnSc2d1	realizovaná
formou	forma	k1gFnSc7	forma
studia	studio	k1gNnSc2	studio
při	při	k7c6	při
zaměstnání	zaměstnání	k1gNnSc6	zaměstnání
či	či	k8xC	či
krátkodobých	krátkodobý	k2eAgInPc2d1	krátkodobý
kurzů	kurz	k1gInPc2	kurz
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
pedagogické	pedagogický	k2eAgFnSc2d1	pedagogická
činnosti	činnost	k1gFnSc2	činnost
se	se	k3xPyFc4	se
akademičtí	akademický	k2eAgMnPc1d1	akademický
pracovníci	pracovník	k1gMnPc1	pracovník
ústavu	ústav	k1gInSc2	ústav
věnují	věnovat	k5eAaPmIp3nP	věnovat
základnímu	základní	k2eAgMnSc3d1	základní
a	a	k8xC	a
aplikovanému	aplikovaný	k2eAgInSc3d1	aplikovaný
výzkumu	výzkum	k1gInSc3	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
především	především	k9	především
o	o	k7c4	o
výzkum	výzkum	k1gInSc4	výzkum
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
řízení	řízení	k1gNnSc2	řízení
podniku	podnik	k1gInSc2	podnik
a	a	k8xC	a
financí	finance	k1gFnPc2	finance
podniku	podnik	k1gInSc2	podnik
<g/>
,	,	kIx,	,
ekonomie	ekonomie	k1gFnSc2	ekonomie
<g/>
,	,	kIx,	,
rozhodování	rozhodování	k1gNnSc2	rozhodování
a	a	k8xC	a
strategického	strategický	k2eAgNnSc2d1	strategické
řízení	řízení	k1gNnSc2	řízení
<g/>
,	,	kIx,	,
marketingu	marketing	k1gInSc2	marketing
<g/>
,	,	kIx,	,
strategické	strategický	k2eAgNnSc4d1	strategické
prostorové	prostorový	k2eAgNnSc4d1	prostorové
plánování	plánování	k1gNnSc4	plánování
<g/>
,	,	kIx,	,
rozvoj	rozvoj	k1gInSc4	rozvoj
měst	město	k1gNnPc2	město
a	a	k8xC	a
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
řízení	řízení	k1gNnSc1	řízení
ve	v	k7c6	v
veřejné	veřejný	k2eAgFnSc6d1	veřejná
správě	správa	k1gFnSc6	správa
<g/>
,	,	kIx,	,
psychologie	psychologie	k1gFnSc1	psychologie
<g/>
,	,	kIx,	,
sociologie	sociologie	k1gFnSc1	sociologie
a	a	k8xC	a
pedagogiky	pedagogika	k1gFnSc2	pedagogika
<g/>
,	,	kIx,	,
historie	historie	k1gFnSc2	historie
techniky	technika	k1gFnSc2	technika
a	a	k8xC	a
průmyslového	průmyslový	k2eAgNnSc2d1	průmyslové
dědíctví	dědíctví	k1gNnSc2	dědíctví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kloknerův	Kloknerův	k2eAgInSc1d1	Kloknerův
ústav	ústav	k1gInSc1	ústav
===	===	k?	===
</s>
</p>
<p>
<s>
Kloknerův	Kloknerův	k2eAgInSc1d1	Kloknerův
ústav	ústav	k1gInSc1	ústav
je	být	k5eAaImIp3nS	být
samostatným	samostatný	k2eAgNnSc7d1	samostatné
pracovištěm	pracoviště	k1gNnSc7	pracoviště
Českého	český	k2eAgNnSc2d1	české
vysokého	vysoký	k2eAgNnSc2d1	vysoké
učení	učení	k1gNnSc2	učení
technického	technický	k2eAgNnSc2d1	technické
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
ČVUT	ČVUT	kA	ČVUT
profesorem	profesor	k1gMnSc7	profesor
Františkem	František	k1gMnSc7	František
Kloknerem	Klokner	k1gMnSc7	Klokner
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Výzkumný	výzkumný	k2eAgInSc1d1	výzkumný
a	a	k8xC	a
zkušební	zkušební	k2eAgInSc1d1	zkušební
ústav	ústav	k1gInSc1	ústav
hmot	hmota	k1gFnPc2	hmota
a	a	k8xC	a
konstrukcí	konstrukce	k1gFnPc2	konstrukce
stavebních	stavební	k2eAgFnPc2d1	stavební
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Činnost	činnost	k1gFnSc1	činnost
a	a	k8xC	a
výsledky	výsledek	k1gInPc1	výsledek
dosažené	dosažený	k2eAgInPc1d1	dosažený
v	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
období	období	k1gNnSc6	období
prokazují	prokazovat	k5eAaImIp3nP	prokazovat
významné	významný	k2eAgNnSc4d1	významné
postavení	postavení	k1gNnSc4	postavení
ústavu	ústav	k1gInSc2	ústav
jak	jak	k8xS	jak
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
zejména	zejména	k9	zejména
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
diagnostiky	diagnostika	k1gFnSc2	diagnostika
<g/>
,	,	kIx,	,
monitorování	monitorování	k1gNnSc1	monitorování
a	a	k8xC	a
hodnocení	hodnocení	k1gNnSc1	hodnocení
konstrukcí	konstrukce	k1gFnPc2	konstrukce
nebo	nebo	k8xC	nebo
výzkumu	výzkum	k1gInSc2	výzkum
nových	nový	k2eAgInPc2d1	nový
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Ústav	ústav	k1gInSc1	ústav
má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgNnPc4	čtyři
odborná	odborný	k2eAgNnPc4d1	odborné
oddělení	oddělení	k1gNnPc4	oddělení
(	(	kIx(	(
<g/>
oddělení	oddělení	k1gNnSc3	oddělení
spolehlivosti	spolehlivost	k1gFnSc2	spolehlivost
<g/>
,	,	kIx,	,
stavebních	stavební	k2eAgInPc2d1	stavební
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
mechaniky	mechanika	k1gFnPc1	mechanika
a	a	k8xC	a
experimentální	experimentální	k2eAgFnPc1d1	experimentální
<g/>
)	)	kIx)	)
a	a	k8xC	a
akreditovanou	akreditovaný	k2eAgFnSc4d1	akreditovaná
laboratoř	laboratoř	k1gFnSc4	laboratoř
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
je	být	k5eAaImIp3nS	být
soudně-znaleckým	soudněnalecký	k2eAgNnSc7d1	soudně-znalecký
pracovištěm	pracoviště	k1gNnSc7	pracoviště
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
stavebnictví	stavebnictví	k1gNnSc2	stavebnictví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Univerzitní	univerzitní	k2eAgNnSc1d1	univerzitní
centrum	centrum	k1gNnSc1	centrum
energeticky	energeticky	k6eAd1	energeticky
efektivních	efektivní	k2eAgFnPc2d1	efektivní
budov	budova	k1gFnPc2	budova
===	===	k?	===
</s>
</p>
<p>
<s>
UCEEB	UCEEB	kA	UCEEB
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
jako	jako	k8xS	jako
samostatný	samostatný	k2eAgInSc4d1	samostatný
vysokoškolský	vysokoškolský	k2eAgInSc4d1	vysokoškolský
ústav	ústav	k1gInSc4	ústav
ČVUT	ČVUT	kA	ČVUT
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
za	za	k7c2	za
podpory	podpora	k1gFnSc2	podpora
Evropského	evropský	k2eAgInSc2d1	evropský
fondu	fond	k1gInSc2	fond
pro	pro	k7c4	pro
regionální	regionální	k2eAgInSc4d1	regionální
rozvoj	rozvoj	k1gInSc4	rozvoj
a	a	k8xC	a
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
v	v	k7c6	v
Buštěhradě	Buštěhrad	k1gInSc6	Buštěhrad
byla	být	k5eAaImAgFnS	být
slavnostně	slavnostně	k6eAd1	slavnostně
otevřena	otevřen	k2eAgFnSc1d1	otevřena
15	[number]	k4	15
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
centra	centrum	k1gNnSc2	centrum
se	se	k3xPyFc4	se
podílely	podílet	k5eAaImAgFnP	podílet
celkem	celkem	k6eAd1	celkem
čtyři	čtyři	k4xCgFnPc4	čtyři
fakulty	fakulta	k1gFnPc4	fakulta
–	–	k?	–
stavební	stavební	k2eAgFnSc1d1	stavební
<g/>
,	,	kIx,	,
strojní	strojní	k2eAgFnSc1d1	strojní
<g/>
,	,	kIx,	,
elektrotechnická	elektrotechnický	k2eAgFnSc1d1	elektrotechnická
a	a	k8xC	a
biomedicíny	biomedicína	k1gFnPc1	biomedicína
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
sestavily	sestavit	k5eAaPmAgInP	sestavit
pět	pět	k4xCc4	pět
výzkumných	výzkumný	k2eAgInPc2d1	výzkumný
programů	program	k1gInPc2	program
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
jejichž	jejichž	k3xOyRp3gNnPc4	jejichž
zajištění	zajištění	k1gNnPc4	zajištění
se	s	k7c7	s
ČVUT	ČVUT	kA	ČVUT
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
účelové	účelový	k2eAgInPc4d1	účelový
finanční	finanční	k2eAgInPc4d1	finanční
prostředky	prostředek	k1gInPc4	prostředek
z	z	k7c2	z
operačního	operační	k2eAgInSc2d1	operační
programu	program	k1gInSc2	program
Věda	věda	k1gFnSc1	věda
a	a	k8xC	a
výzkum	výzkum	k1gInSc1	výzkum
pro	pro	k7c4	pro
inovace	inovace	k1gFnPc4	inovace
<g/>
,	,	kIx,	,
řízeným	řízený	k2eAgMnSc7d1	řízený
MŠMT	MŠMT	kA	MŠMT
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
UCEEB	UCEEB	kA	UCEEB
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
zjednodušení	zjednodušení	k1gNnSc1	zjednodušení
spolupráce	spolupráce	k1gFnSc2	spolupráce
univerzity	univerzita	k1gFnSc2	univerzita
s	s	k7c7	s
průmyslem	průmysl	k1gInSc7	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Přesněji	přesně	k6eAd2	přesně
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
se	se	k3xPyFc4	se
na	na	k7c6	na
komercializaci	komercializace	k1gFnSc6	komercializace
výsledků	výsledek	k1gInPc2	výsledek
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
,	,	kIx,	,
transfer	transfer	k1gInSc1	transfer
technologií	technologie	k1gFnPc2	technologie
<g/>
,	,	kIx,	,
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
průmyslem	průmysl	k1gInSc7	průmysl
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
synergické	synergický	k2eAgFnSc2d1	synergická
inovační	inovační	k2eAgFnSc2d1	inovační
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
přidanou	přidaný	k2eAgFnSc7d1	přidaná
hodnotou	hodnota	k1gFnSc7	hodnota
je	být	k5eAaImIp3nS	být
schopnost	schopnost	k1gFnSc1	schopnost
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
<g/>
,	,	kIx,	,
inovovat	inovovat	k5eAaBmF	inovovat
a	a	k8xC	a
přicházet	přicházet	k5eAaImF	přicházet
s	s	k7c7	s
originálními	originální	k2eAgNnPc7d1	originální
řešeními	řešení	k1gNnPc7	řešení
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
trvale	trvale	k6eAd1	trvale
udržitelných	udržitelný	k2eAgFnPc2d1	udržitelná
budov	budova	k1gFnPc2	budova
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
jejich	jejich	k3xOp3gInSc6	jejich
životním	životní	k2eAgInSc6d1	životní
cyklu	cyklus	k1gInSc6	cyklus
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
zdravého	zdravý	k2eAgNnSc2d1	zdravé
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
úsporu	úspora	k1gFnSc4	úspora
investičních	investiční	k2eAgInPc2d1	investiční
a	a	k8xC	a
provozních	provozní	k2eAgInPc2d1	provozní
nákladů	náklad	k1gInPc2	náklad
i	i	k8xC	i
energií	energie	k1gFnPc2	energie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
má	mít	k5eAaImIp3nS	mít
praktickou	praktický	k2eAgFnSc4d1	praktická
integraci	integrace	k1gFnSc4	integrace
know-how	knowow	k?	know-how
vědců	vědec	k1gMnPc2	vědec
a	a	k8xC	a
inženýrů	inženýr	k1gMnPc2	inženýr
převzatých	převzatý	k2eAgMnPc2d1	převzatý
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
fakult	fakulta	k1gFnPc2	fakulta
ČVUT	ČVUT	kA	ČVUT
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
elektrotechniky	elektrotechnika	k1gFnSc2	elektrotechnika
<g/>
,	,	kIx,	,
stavebního	stavební	k2eAgInSc2d1	stavební
<g/>
,	,	kIx,	,
biomedicínského	biomedicínský	k2eAgMnSc2d1	biomedicínský
a	a	k8xC	a
strojního	strojní	k2eAgNnSc2d1	strojní
inženýrství	inženýrství	k1gNnSc2	inženýrství
<g/>
.	.	kIx.	.
</s>
<s>
Špičkové	Špičkové	k2eAgNnSc4d1	Špičkové
technologické	technologický	k2eAgNnSc4d1	Technologické
vybavení	vybavení	k1gNnSc4	vybavení
výzkumné	výzkumný	k2eAgFnSc2d1	výzkumná
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
a	a	k8xC	a
rozmanitost	rozmanitost	k1gFnSc4	rozmanitost
laboratorního	laboratorní	k2eAgNnSc2d1	laboratorní
zařízení	zařízení	k1gNnSc2	zařízení
nabízí	nabízet	k5eAaImIp3nS	nabízet
místním	místní	k2eAgMnPc3d1	místní
i	i	k8xC	i
zahraničním	zahraniční	k2eAgMnPc3d1	zahraniční
studentům	student	k1gMnPc3	student
možnost	možnost	k1gFnSc4	možnost
využívat	využívat	k5eAaImF	využívat
laboratoře	laboratoř	k1gFnPc4	laboratoř
i	i	k8xC	i
zařízení	zařízení	k1gNnPc4	zařízení
pro	pro	k7c4	pro
studentské	studentský	k2eAgFnPc4d1	studentská
závěrečné	závěrečný	k2eAgFnPc4d1	závěrečná
práce	práce	k1gFnPc4	práce
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
přednostně	přednostně	k6eAd1	přednostně
navrhovány	navrhován	k2eAgInPc1d1	navrhován
pro	pro	k7c4	pro
zefektivnění	zefektivnění	k1gNnSc4	zefektivnění
technologií	technologie	k1gFnPc2	technologie
používaných	používaný	k2eAgFnPc2d1	používaná
komerčně	komerčně	k6eAd1	komerčně
pro	pro	k7c4	pro
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
priorit	priorita	k1gFnPc2	priorita
univerzitního	univerzitní	k2eAgNnSc2d1	univerzitní
centra	centrum	k1gNnSc2	centrum
je	být	k5eAaImIp3nS	být
propojovat	propojovat	k5eAaImF	propojovat
vědu	věda	k1gFnSc4	věda
s	s	k7c7	s
průmyslem	průmysl	k1gInSc7	průmysl
a	a	k8xC	a
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
na	na	k7c6	na
významných	významný	k2eAgInPc6d1	významný
evropských	evropský	k2eAgInPc6d1	evropský
a	a	k8xC	a
světových	světový	k2eAgInPc6d1	světový
projektech	projekt	k1gInPc6	projekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Český	český	k2eAgInSc1d1	český
institut	institut	k1gInSc1	institut
informatiky	informatika	k1gFnSc2	informatika
<g/>
,	,	kIx,	,
robotiky	robotika	k1gFnSc2	robotika
a	a	k8xC	a
kybernetiky	kybernetika	k1gFnSc2	kybernetika
===	===	k?	===
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
institut	institut	k1gInSc1	institut
informatiky	informatika	k1gFnSc2	informatika
<g/>
,	,	kIx,	,
robotiky	robotika	k1gFnSc2	robotika
a	a	k8xC	a
kybernetiky	kybernetika	k1gFnSc2	kybernetika
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Czech	Czech	k1gMnSc1	Czech
Institute	institut	k1gInSc5	institut
of	of	k?	of
Informatics	Informatics	k1gInSc1	Informatics
<g/>
,	,	kIx,	,
Robotics	Robotics	k1gInSc1	Robotics
<g/>
,	,	kIx,	,
and	and	k?	and
Cybernetics	Cybernetics	k1gInSc1	Cybernetics
<g/>
,	,	kIx,	,
zkratkou	zkratka	k1gFnSc7	zkratka
CIIRC	CIIRC	kA	CIIRC
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Českého	český	k2eAgNnSc2d1	české
vysokého	vysoký	k2eAgNnSc2d1	vysoké
učení	učení	k1gNnSc2	učení
technického	technický	k2eAgNnSc2d1	technické
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
založen	založit	k5eAaPmNgInS	založit
byl	být	k5eAaImAgInS	být
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
posláním	poslání	k1gNnSc7	poslání
je	být	k5eAaImIp3nS	být
být	být	k5eAaImF	být
mezinárodně	mezinárodně	k6eAd1	mezinárodně
uznávaným	uznávaný	k2eAgNnSc7d1	uznávané
výzkumným	výzkumný	k2eAgNnSc7d1	výzkumné
pracovištěm	pracoviště	k1gNnSc7	pracoviště
<g/>
,	,	kIx,	,
podílejícím	podílející	k2eAgMnSc7d1	podílející
se	se	k3xPyFc4	se
na	na	k7c6	na
výuce	výuka	k1gFnSc6	výuka
studentů	student	k1gMnPc2	student
a	a	k8xC	a
místem	místo	k1gNnSc7	místo
pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
technologií	technologie	k1gFnPc2	technologie
do	do	k7c2	do
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2013	[number]	k4	2013
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
dejvickém	dejvický	k2eAgInSc6d1	dejvický
areálu	areál	k1gInSc6	areál
ČVUT	ČVUT	kA	ČVUT
výstavba	výstavba	k1gFnSc1	výstavba
dvou	dva	k4xCgFnPc2	dva
nových	nový	k2eAgFnPc2d1	nová
budov	budova	k1gFnPc2	budova
určených	určený	k2eAgInPc2d1	určený
pro	pro	k7c4	pro
CIIRC	CIIRC	kA	CIIRC
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
dokončena	dokončen	k2eAgFnSc1d1	dokončena
a	a	k8xC	a
budovy	budova	k1gFnPc1	budova
osídleny	osídlit	k5eAaPmNgFnP	osídlit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
léta	léto	k1gNnSc2	léto
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
CIIRC	CIIRC	kA	CIIRC
bude	být	k5eAaImBp3nS	být
sídlit	sídlit	k5eAaImF	sídlit
v	v	k7c6	v
nově	nově	k6eAd1	nově
zrekonstruované	zrekonstruovaný	k2eAgFnSc6d1	zrekonstruovaná
budově	budova	k1gFnSc6	budova
v	v	k7c6	v
pražských	pražský	k2eAgFnPc6d1	Pražská
Dejvicích	Dejvice	k1gFnPc6	Dejvice
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
dříve	dříve	k6eAd2	dříve
byla	být	k5eAaImAgFnS	být
Technická	technický	k2eAgFnSc1d1	technická
menza	menza	k1gFnSc1	menza
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
českých	český	k2eAgFnPc2d1	Česká
prodejen	prodejna	k1gFnPc2	prodejna
Billa	Bill	k1gMnSc2	Bill
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CIIRC	CIIRC	kA	CIIRC
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
osmi	osm	k4xCc2	osm
výzkumných	výzkumný	k2eAgNnPc2d1	výzkumné
oddělení	oddělení	k1gNnPc2	oddělení
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgNnPc7	jenž
jsou	být	k5eAaImIp3nP	být
CYPHY	CYPHY	kA	CYPHY
<g/>
:	:	kIx,	:
Kyberneticko-fyzikální	kybernetickoyzikální	k2eAgInPc1d1	kyberneticko-fyzikální
systémy	systém	k1gInPc1	systém
<g/>
,	,	kIx,	,
INTSYS	INTSYS	kA	INTSYS
<g/>
:	:	kIx,	:
Inteligentní	inteligentní	k2eAgInPc1d1	inteligentní
systémy	systém	k1gInPc1	systém
<g/>
,	,	kIx,	,
IIG	IIG	kA	IIG
<g/>
:	:	kIx,	:
Průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
informatika	informatika	k1gFnSc1	informatika
<g/>
,	,	kIx,	,
RMP	RMP	kA	RMP
<g/>
:	:	kIx,	:
Robotika	robotika	k1gFnSc1	robotika
a	a	k8xC	a
strojové	strojový	k2eAgNnSc1d1	strojové
vnímání	vnímání	k1gNnSc1	vnímání
<g/>
,	,	kIx,	,
IPA	IPA	kA	IPA
<g/>
:	:	kIx,	:
Průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
výroba	výroba	k1gFnSc1	výroba
a	a	k8xC	a
automatizace	automatizace	k1gFnSc1	automatizace
<g/>
,	,	kIx,	,
COGSYS	COGSYS	kA	COGSYS
<g/>
:	:	kIx,	:
Kognitivní	kognitivní	k2eAgInPc1d1	kognitivní
systémy	systém	k1gInPc1	systém
a	a	k8xC	a
neurovědy	neurověda	k1gFnPc1	neurověda
<g/>
,	,	kIx,	,
BEAT	beat	k1gInSc1	beat
<g/>
:	:	kIx,	:
Biomedicína	Biomedicína	k1gFnSc1	Biomedicína
a	a	k8xC	a
asistivní	asistivní	k2eAgFnSc1d1	asistivní
technologie	technologie	k1gFnSc1	technologie
a	a	k8xC	a
PLAT	plat	k1gInSc1	plat
<g/>
:	:	kIx,	:
Vědecké	vědecký	k2eAgNnSc1d1	vědecké
řízení	řízení	k1gNnSc1	řízení
platforem	platforma	k1gFnPc2	platforma
<g/>
.	.	kIx.	.
</s>
<s>
Výzkumná	výzkumný	k2eAgNnPc1d1	výzkumné
oddělení	oddělení	k1gNnPc1	oddělení
jsou	být	k5eAaImIp3nP	být
dále	daleko	k6eAd2	daleko
členěná	členěný	k2eAgNnPc1d1	členěné
na	na	k7c4	na
výzkumné	výzkumný	k2eAgFnPc4d1	výzkumná
skupiny	skupina	k1gFnPc4	skupina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
CIIRC	CIIRC	kA	CIIRC
stojí	stát	k5eAaImIp3nS	stát
Vladimír	Vladimír	k1gMnSc1	Vladimír
Mařík	Mařík	k1gMnSc1	Mařík
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
stavby	stavba	k1gFnSc2	stavba
budovy	budova	k1gFnSc2	budova
se	se	k3xPyFc4	se
CIIRC	CIIRC	kA	CIIRC
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
v	v	k7c6	v
provizorních	provizorní	k2eAgInPc6d1	provizorní
prostorech	prostor	k1gInPc6	prostor
<g/>
,	,	kIx,	,
také	také	k9	také
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
fakultami	fakulta	k1gFnPc7	fakulta
<g/>
,	,	kIx,	,
součástmi	součást	k1gFnPc7	součást
ČVUT	ČVUT	kA	ČVUT
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
institucemi	instituce	k1gFnPc7	instituce
<g/>
.	.	kIx.	.
</s>
<s>
CIIRC	CIIRC	kA	CIIRC
postupně	postupně	k6eAd1	postupně
získává	získávat	k5eAaImIp3nS	získávat
vlastní	vlastní	k2eAgInPc4d1	vlastní
výzkumné	výzkumný	k2eAgInPc4d1	výzkumný
projekty	projekt	k1gInPc4	projekt
<g/>
,	,	kIx,	,
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
počet	počet	k1gInSc1	počet
výzkumných	výzkumný	k2eAgMnPc2d1	výzkumný
pracovníků	pracovník	k1gMnPc2	pracovník
a	a	k8xC	a
svůj	svůj	k3xOyFgInSc4	svůj
výzkumný	výzkumný	k2eAgInSc4d1	výzkumný
výkon	výkon	k1gInSc4	výkon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ústav	ústav	k1gInSc1	ústav
tělesné	tělesný	k2eAgFnSc2d1	tělesná
výchovy	výchova	k1gFnSc2	výchova
a	a	k8xC	a
sportu	sport	k1gInSc2	sport
===	===	k?	===
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
Ústavu	ústav	k1gInSc2	ústav
tělesné	tělesný	k2eAgFnSc2d1	tělesná
výchovy	výchova	k1gFnSc2	výchova
a	a	k8xC	a
sportu	sport	k1gInSc2	sport
ČVUT	ČVUT	kA	ČVUT
ÚTVS	ÚTVS	kA	ÚTVS
je	být	k5eAaImIp3nS	být
zlepšení	zlepšení	k1gNnSc4	zlepšení
kvality	kvalita	k1gFnSc2	kvalita
životního	životní	k2eAgInSc2d1	životní
stylu	styl	k1gInSc2	styl
co	co	k9	co
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
počtu	počet	k1gInSc2	počet
studentů	student	k1gMnPc2	student
a	a	k8xC	a
akademických	akademický	k2eAgMnPc2d1	akademický
pracovníků	pracovník	k1gMnPc2	pracovník
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
pravidelně	pravidelně	k6eAd1	pravidelně
prováděných	prováděný	k2eAgFnPc2d1	prováděná
sportovních	sportovní	k2eAgFnPc2d1	sportovní
aktivit	aktivita	k1gFnPc2	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
spravuje	spravovat	k5eAaImIp3nS	spravovat
a	a	k8xC	a
provozuje	provozovat	k5eAaImIp3nS	provozovat
tělovýchovná	tělovýchovný	k2eAgNnPc4d1	tělovýchovné
zařízení	zařízení	k1gNnPc4	zařízení
ČVUT	ČVUT	kA	ČVUT
<g/>
:	:	kIx,	:
sportovní	sportovní	k2eAgFnPc4d1	sportovní
haly	hala	k1gFnPc4	hala
<g/>
,	,	kIx,	,
víceúčelové	víceúčelový	k2eAgFnPc4d1	víceúčelová
tělocvičny	tělocvična	k1gFnPc4	tělocvična
<g/>
,	,	kIx,	,
posilovny	posilovna	k1gFnPc4	posilovna
<g/>
,	,	kIx,	,
lezecké	lezecký	k2eAgFnPc4d1	lezecká
stěny	stěna	k1gFnPc4	stěna
<g/>
,	,	kIx,	,
lukostřelecké	lukostřelecký	k2eAgFnPc4d1	lukostřelecká
střelnice	střelnice	k1gFnPc4	střelnice
<g/>
,	,	kIx,	,
tenisové	tenisový	k2eAgInPc4d1	tenisový
kurty	kurt	k1gInPc4	kurt
atp.	atp.	kA	atp.
</s>
</p>
<p>
<s>
Ústav	ústav	k1gInSc1	ústav
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
semestrální	semestrální	k2eAgFnSc4d1	semestrální
výuku	výuka	k1gFnSc4	výuka
TV	TV	kA	TV
na	na	k7c6	na
fakultách	fakulta	k1gFnPc6	fakulta
ČVUT	ČVUT	kA	ČVUT
<g/>
,	,	kIx,	,
organizaci	organizace	k1gFnSc4	organizace
zimních	zimní	k2eAgInPc2d1	zimní
a	a	k8xC	a
letních	letní	k2eAgInPc2d1	letní
výcvikových	výcvikový	k2eAgInPc2d1	výcvikový
kurzů	kurz	k1gInPc2	kurz
v	v	k7c6	v
ČR	ČR	kA	ČR
i	i	k8xC	i
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
jednorázové	jednorázový	k2eAgFnPc4d1	jednorázová
sportovní	sportovní	k2eAgFnPc4d1	sportovní
akce	akce	k1gFnPc4	akce
pro	pro	k7c4	pro
studenty	student	k1gMnPc4	student
a	a	k8xC	a
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
sportovní	sportovní	k2eAgFnSc4d1	sportovní
reprezentaci	reprezentace	k1gFnSc4	reprezentace
ČVUT	ČVUT	kA	ČVUT
na	na	k7c6	na
sportovních	sportovní	k2eAgInPc6d1	sportovní
přeborech	přebor	k1gInPc6	přebor
VŠ	vš	k0	vš
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
Českých	český	k2eAgFnPc6d1	Česká
akademických	akademický	k2eAgFnPc6d1	akademická
hrách	hra	k1gFnPc6	hra
(	(	kIx(	(
<g/>
ČAH	ČAH	kA	ČAH
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mezinárodních	mezinárodní	k2eAgFnPc6d1	mezinárodní
sportovních	sportovní	k2eAgFnPc6d1	sportovní
akcích	akce	k1gFnPc6	akce
<g/>
.	.	kIx.	.
</s>
<s>
Týdně	týdně	k6eAd1	týdně
učí	učit	k5eAaImIp3nS	učit
na	na	k7c4	na
9	[number]	k4	9
000	[number]	k4	000
studentů	student	k1gMnPc2	student
s	s	k7c7	s
nabídkou	nabídka	k1gFnSc7	nabídka
přibližně	přibližně	k6eAd1	přibližně
40	[number]	k4	40
různých	různý	k2eAgFnPc2d1	různá
pohybových	pohybový	k2eAgFnPc2d1	pohybová
aktivit	aktivita	k1gFnPc2	aktivita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
spolupráce	spolupráce	k1gFnSc1	spolupráce
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
se	se	k3xPyFc4	se
ČVUT	ČVUT	kA	ČVUT
umístilo	umístit	k5eAaPmAgNnS	umístit
v	v	k7c6	v
hodnocení	hodnocení	k1gNnSc6	hodnocení
QS	QS	kA	QS
World	World	k1gMnSc1	World
University	universita	k1gFnSc2	universita
Rankings	Rankings	k1gInSc1	Rankings
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
tisíce	tisíc	k4xCgInPc4	tisíc
světových	světový	k2eAgFnPc2d1	světová
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
univerzit	univerzita	k1gFnPc2	univerzita
na	na	k7c4	na
501	[number]	k4	501
<g/>
.	.	kIx.	.
–	–	k?	–
550	[number]	k4	550
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
"	"	kIx"	"
<g/>
Civil	civil	k1gMnSc1	civil
and	and	k?	and
Structural	Structural	k1gFnSc1	Structural
Engineering	Engineering	k1gInSc1	Engineering
<g/>
"	"	kIx"	"
bylo	být	k5eAaImAgNnS	být
ČVUT	ČVUT	kA	ČVUT
hodnoceno	hodnotit	k5eAaImNgNnS	hodnotit
na	na	k7c4	na
51	[number]	k4	51
<g/>
.	.	kIx.	.
–	–	k?	–
100	[number]	k4	100
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
"	"	kIx"	"
<g/>
Mechanical	Mechanical	k1gFnSc1	Mechanical
Engineering	Engineering	k1gInSc1	Engineering
<g/>
"	"	kIx"	"
na	na	k7c4	na
101	[number]	k4	101
<g/>
.	.	kIx.	.
–	–	k?	–
150	[number]	k4	150
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
"	"	kIx"	"
<g/>
Computer	computer	k1gInSc1	computer
Science	Science	k1gFnSc2	Science
and	and	k?	and
<g />
.	.	kIx.	.
</s>
<s>
Information	Information	k1gInSc1	Information
Systems	Systems	k1gInSc1	Systems
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Electrical	Electrical	k1gFnSc1	Electrical
Engineering	Engineering	k1gInSc1	Engineering
<g/>
"	"	kIx"	"
na	na	k7c4	na
151	[number]	k4	151
<g/>
.	.	kIx.	.
–	–	k?	–
200	[number]	k4	200
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
"	"	kIx"	"
<g/>
Computer	computer	k1gInSc1	computer
Science	Science	k1gFnSc2	Science
and	and	k?	and
Information	Information	k1gInSc1	Information
Systems	Systems	k1gInSc1	Systems
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Electrical	Electrical	k1gFnSc1	Electrical
Engineering	Engineering	k1gInSc1	Engineering
<g/>
"	"	kIx"	"
na	na	k7c4	na
151	[number]	k4	151
<g/>
.	.	kIx.	.
–	–	k?	–
200	[number]	k4	200
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
i	i	k9	i
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
"	"	kIx"	"
<g/>
Mathematics	Mathematicsa	k1gFnPc2	Mathematicsa
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Physics	Physics	k1gInSc1	Physics
and	and	k?	and
Astronomy	astronom	k1gMnPc7	astronom
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
České	český	k2eAgNnSc1d1	české
vysoké	vysoký	k2eAgNnSc1d1	vysoké
učení	učení	k1gNnSc1	učení
technické	technický	k2eAgNnSc1d1	technické
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
má	mít	k5eAaImIp3nS	mít
uzavřenu	uzavřen	k2eAgFnSc4d1	uzavřena
řadu	řada	k1gFnSc4	řada
smluv	smlouva	k1gFnPc2	smlouva
<g/>
,	,	kIx,	,
buď	buď	k8xC	buď
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
mezivládních	mezivládní	k2eAgFnPc2d1	mezivládní
dohod	dohoda	k1gFnPc2	dohoda
<g/>
,	,	kIx,	,
či	či	k8xC	či
na	na	k7c6	na
principu	princip	k1gInSc6	princip
meziuniverzitní	meziuniverzitní	k2eAgFnSc2d1	meziuniverzitní
výměny	výměna	k1gFnSc2	výměna
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zapojeno	zapojit	k5eAaPmNgNnS	zapojit
do	do	k7c2	do
celoevropských	celoevropský	k2eAgInPc2d1	celoevropský
studijních	studijní	k2eAgInPc2d1	studijní
programů	program	k1gInPc2	program
Erasmus	Erasmus	k1gMnSc1	Erasmus
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
ATHENS	ATHENS	kA	ATHENS
<g/>
,	,	kIx,	,
Erasmus	Erasmus	k1gMnSc1	Erasmus
Mundus	Mundus	k1gMnSc1	Mundus
a	a	k8xC	a
řady	řada	k1gFnPc1	řada
programů	program	k1gInPc2	program
vědecko-výzkumných	vědeckoýzkumný	k2eAgInPc2d1	vědecko-výzkumný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Studium	studium	k1gNnSc1	studium
a	a	k8xC	a
praxe	praxe	k1gFnSc1	praxe
studentů	student	k1gMnPc2	student
ČVUT	ČVUT	kA	ČVUT
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
===	===	k?	===
</s>
</p>
<p>
<s>
ČVUT	ČVUT	kA	ČVUT
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
uzavírat	uzavírat	k5eAaImF	uzavírat
smlouvy	smlouva	k1gFnPc4	smlouva
o	o	k7c6	o
spolupráci	spolupráce	k1gFnSc6	spolupráce
především	především	k6eAd1	především
s	s	k7c7	s
univerzitami	univerzita	k1gFnPc7	univerzita
z	z	k7c2	z
první	první	k4xOgFnSc2	první
pětistovky	pětistovka	k1gFnSc2	pětistovka
mezinárodně	mezinárodně	k6eAd1	mezinárodně
uznávaného	uznávaný	k2eAgInSc2d1	uznávaný
žebříčku	žebříček	k1gInSc2	žebříček
QS	QS	kA	QS
World	World	k1gMnSc1	World
University	universita	k1gFnSc2	universita
Rankings	Rankings	k1gInSc1	Rankings
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
má	mít	k5eAaImIp3nS	mít
platné	platný	k2eAgFnPc4d1	platná
dvoustranné	dvoustranný	k2eAgFnPc4d1	dvoustranná
dohody	dohoda	k1gFnPc4	dohoda
s	s	k7c7	s
484	[number]	k4	484
zahraničními	zahraniční	k2eAgFnPc7d1	zahraniční
institucemi	instituce	k1gFnPc7	instituce
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
89	[number]	k4	89
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
prestižní	prestižní	k2eAgFnSc2d1	prestižní
skupiny	skupina	k1gFnSc2	skupina
univerzit	univerzita	k1gFnPc2	univerzita
do	do	k7c2	do
stého	stý	k4xOgNnSc2	stý
místa	místo	k1gNnSc2	místo
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
(	(	kIx(	(
<g/>
Nanyang	Nanyang	k1gInSc1	Nanyang
Technological	Technological	k1gFnSc2	Technological
University	universita	k1gFnSc2	universita
<g/>
,	,	kIx,	,
Purdue	Purdu	k1gFnSc2	Purdu
University	universita	k1gFnSc2	universita
<g/>
,	,	kIx,	,
Politecnico	Politecnico	k6eAd1	Politecnico
di	di	k?	di
Milano	Milana	k1gFnSc5	Milana
<g/>
,	,	kIx,	,
Katolická	katolický	k2eAgFnSc1d1	katolická
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Lovani	Lovaň	k1gFnSc6	Lovaň
<g/>
,	,	kIx,	,
TU	tu	k6eAd1	tu
Mnichov	Mnichov	k1gInSc1	Mnichov
<g/>
,	,	kIx,	,
TU	tu	k6eAd1	tu
Delft	Delft	k1gInSc1	Delft
<g/>
,	,	kIx,	,
TU	tu	k6eAd1	tu
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
KTH	KTH	kA	KTH
Stockholm	Stockholm	k1gInSc1	Stockholm
<g/>
,	,	kIx,	,
National	National	k1gMnSc1	National
University	universita	k1gFnSc2	universita
of	of	k?	of
Singapore	Singapor	k1gInSc5	Singapor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
ČVUT	ČVUT	kA	ČVUT
podařilo	podařit	k5eAaPmAgNnS	podařit
navázat	navázat	k5eAaPmF	navázat
kontakty	kontakt	k1gInPc4	kontakt
a	a	k8xC	a
následně	následně	k6eAd1	následně
uzavřít	uzavřít	k5eAaPmF	uzavřít
bilaterální	bilaterální	k2eAgFnSc2d1	bilaterální
dohody	dohoda	k1gFnSc2	dohoda
například	například	k6eAd1	například
s	s	k7c7	s
izraelským	izraelský	k2eAgInSc7d1	izraelský
TECHNIONEM	TECHNIONEM	kA	TECHNIONEM
(	(	kIx(	(
<g/>
Israel	Israel	k1gMnSc1	Israel
Institute	institut	k1gInSc5	institut
of	of	k?	of
Technology	technolog	k1gMnPc7	technolog
<g/>
)	)	kIx)	)
z	z	k7c2	z
Haify	Haifa	k1gFnSc2	Haifa
<g/>
,	,	kIx,	,
s	s	k7c7	s
jihoafrickou	jihoafrický	k2eAgFnSc7d1	Jihoafrická
Stellenbosch	Stellenbosch	k1gInSc1	Stellenbosch
University	universita	k1gFnPc1	universita
<g/>
,	,	kIx,	,
s	s	k7c7	s
australskými	australský	k2eAgInPc7d1	australský
University	universita	k1gFnSc2	universita
of	of	k?	of
South	South	k1gInSc1	South
Australia	Australius	k1gMnSc2	Australius
v	v	k7c6	v
Adelaide	Adelaid	k1gInSc5	Adelaid
a	a	k8xC	a
Queensland	Queensland	k1gInSc1	Queensland
University	universita	k1gFnSc2	universita
of	of	k?	of
Technology	technolog	k1gMnPc4	technolog
v	v	k7c6	v
Brisbane	Brisban	k1gMnSc5	Brisban
<g/>
,	,	kIx,	,
s	s	k7c7	s
korejskými	korejský	k2eAgMnPc7d1	korejský
Kyungpook	Kyungpook	k1gInSc4	Kyungpook
National	National	k1gFnPc1	National
University	universita	k1gFnSc2	universita
v	v	k7c6	v
Tegu	Tegus	k1gInSc6	Tegus
a	a	k8xC	a
se	se	k3xPyFc4	se
Sungkyunkwan	Sungkyunkwan	k1gInSc1	Sungkyunkwan
University	universita	k1gFnSc2	universita
v	v	k7c6	v
Soulu	Soul	k1gInSc6	Soul
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
bilaterálních	bilaterální	k2eAgFnPc2d1	bilaterální
smluv	smlouva	k1gFnPc2	smlouva
probíhají	probíhat	k5eAaImIp3nP	probíhat
výměny	výměna	k1gFnPc1	výměna
zejména	zejména	k9	zejména
s	s	k7c7	s
mimoevropskými	mimoevropský	k2eAgFnPc7d1	mimoevropská
univerzitami	univerzita	k1gFnPc7	univerzita
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
<g/>
,	,	kIx,	,
Střední	střední	k2eAgFnSc6d1	střední
i	i	k8xC	i
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
zájem	zájem	k1gInSc1	zájem
studentů	student	k1gMnPc2	student
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
země	zem	k1gFnPc4	zem
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
,	,	kIx,	,
Singapur	Singapur	k1gInSc1	Singapur
<g/>
,	,	kIx,	,
USA	USA	kA	USA
a	a	k8xC	a
Japonsko	Japonsko	k1gNnSc1	Japonsko
(	(	kIx(	(
<g/>
seřazeno	seřazen	k2eAgNnSc1d1	seřazeno
od	od	k7c2	od
Zemí	zem	k1gFnPc2	zem
o	o	k7c4	o
které	který	k3yRgMnPc4	který
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
zájem	zájem	k1gInSc1	zájem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
vyjíždějí	vyjíždět	k5eAaImIp3nP	vyjíždět
studenti	student	k1gMnPc1	student
rovněž	rovněž	k9	rovněž
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
atraktivních	atraktivní	k2eAgFnPc2d1	atraktivní
destinací	destinace	k1gFnPc2	destinace
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
Hong	Hong	k1gMnSc1	Hong
Kong	Kongo	k1gNnPc2	Kongo
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
Indonésie	Indonésie	k1gFnSc1	Indonésie
<g/>
,	,	kIx,	,
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
<g/>
,	,	kIx,	,
Kostarika	Kostarika	k1gFnSc1	Kostarika
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
<g/>
,	,	kIx,	,
Peru	Peru	k1gNnSc1	Peru
<g/>
,	,	kIx,	,
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
Taiwan	Taiwana	k1gFnPc2	Taiwana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
programu	program	k1gInSc2	program
Erasmus	Erasmus	k1gMnSc1	Erasmus
směřuje	směřovat	k5eAaImIp3nS	směřovat
přes	přes	k7c4	přes
50	[number]	k4	50
%	%	kIx~	%
studentů	student	k1gMnPc2	student
na	na	k7c4	na
studijní	studijní	k2eAgInPc4d1	studijní
pobyty	pobyt	k1gInPc4	pobyt
do	do	k7c2	do
4	[number]	k4	4
zemí	zem	k1gFnPc2	zem
–	–	k?	–
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Španělska	Španělsko	k1gNnSc2	Španělsko
(	(	kIx(	(
<g/>
odkud	odkud	k6eAd1	odkud
také	také	k9	také
do	do	k7c2	do
Česka	Česko	k1gNnSc2	Česko
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
nejvíce	nejvíce	k6eAd1	nejvíce
studentů	student	k1gMnPc2	student
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
kolem	kolem	k7c2	kolem
700	[number]	k4	700
studentů	student	k1gMnPc2	student
ČVUT	ČVUT	kA	ČVUT
pobývá	pobývat	k5eAaImIp3nS	pobývat
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
semestry	semestr	k1gInPc4	semestr
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
na	na	k7c6	na
partnerských	partnerský	k2eAgFnPc6d1	partnerská
univerzitách	univerzita	k1gFnPc6	univerzita
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
====	====	k?	====
Dvojí	dvojí	k4xRgInSc1	dvojí
diplom	diplom	k1gInSc1	diplom
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
má	mít	k5eAaImIp3nS	mít
ČVUT	ČVUT	kA	ČVUT
21	[number]	k4	21
platných	platný	k2eAgFnPc2d1	platná
dohod	dohoda	k1gFnPc2	dohoda
o	o	k7c6	o
společných	společný	k2eAgInPc6d1	společný
diplomech	diplom	k1gInPc6	diplom
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
na	na	k7c6	na
Fakultě	fakulta	k1gFnSc6	fakulta
stavební	stavební	k2eAgFnSc6d1	stavební
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
studenti	student	k1gMnPc1	student
mohou	moct	k5eAaImIp3nP	moct
přihlásit	přihlásit	k5eAaPmF	přihlásit
do	do	k7c2	do
pěti	pět	k4xCc2	pět
společných	společný	k2eAgInPc2d1	společný
studijních	studijní	k2eAgInPc2d1	studijní
programů	program	k1gInPc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
čtyři	čtyři	k4xCgInPc4	čtyři
programy	program	k1gInPc4	program
na	na	k7c4	na
získání	získání	k1gNnSc4	získání
společného	společný	k2eAgInSc2d1	společný
diplomu	diplom	k1gInSc2	diplom
nabízí	nabízet	k5eAaImIp3nS	nabízet
Fakulta	fakulta	k1gFnSc1	fakulta
elektrotechnická	elektrotechnický	k2eAgFnSc1d1	elektrotechnická
<g/>
,	,	kIx,	,
po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
programech	program	k1gInPc6	program
Fakulta	fakulta	k1gFnSc1	fakulta
dopravní	dopravní	k2eAgFnSc1d1	dopravní
a	a	k8xC	a
Fakulta	fakulta	k1gFnSc1	fakulta
strojní	strojní	k2eAgFnSc1d1	strojní
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc4	jeden
program	program	k1gInSc4	program
získala	získat	k5eAaPmAgFnS	získat
Fakulta	fakulta	k1gFnSc1	fakulta
biomedicínského	biomedicínský	k2eAgNnSc2d1	biomedicínské
inženýrství	inženýrství	k1gNnSc2	inženýrství
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
partnerských	partnerský	k2eAgFnPc2d1	partnerská
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
univerzit	univerzita	k1gFnPc2	univerzita
jmenujme	jmenovat	k5eAaBmRp1nP	jmenovat
alespoň	alespoň	k9	alespoň
ty	ten	k3xDgMnPc4	ten
nejvýznamnější	významný	k2eAgMnPc4d3	nejvýznamnější
<g/>
:	:	kIx,	:
Padovská	padovský	k2eAgFnSc5d1	Padovská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Katalánská	katalánský	k2eAgNnPc4d1	katalánské
polytechnika	polytechnikum	k1gNnPc4	polytechnikum
<g/>
,	,	kIx,	,
École	École	k1gFnPc4	École
Nationale	Nationale	k1gFnSc2	Nationale
des	des	k1gNnSc2	des
Ponts	Pontsa	k1gFnPc2	Pontsa
et	et	k?	et
Chaussées	Chausséesa	k1gFnPc2	Chausséesa
<g/>
,	,	kIx,	,
École	Écol	k1gMnSc5	Écol
Centrale	Central	k1gMnSc5	Central
de	de	k?	de
Nantes	Nantesa	k1gFnPc2	Nantesa
<g/>
,	,	kIx,	,
RWTH	RWTH	kA	RWTH
Aachen	Aachen	k1gInSc4	Aachen
<g/>
,	,	kIx,	,
TU	tu	k6eAd1	tu
Mnichov	Mnichov	k1gInSc4	Mnichov
<g/>
,	,	kIx,	,
Trinity	Trinit	k2eAgFnPc4d1	Trinita
College	Colleg	k1gFnPc4	Colleg
Dublin	Dublin	k1gInSc4	Dublin
<g/>
,	,	kIx,	,
Universita	universita	k1gFnSc1	universita
v	v	k7c6	v
Coimbře	Coimbra	k1gFnSc6	Coimbra
<g/>
,	,	kIx,	,
Tomská	Tomský	k2eAgNnPc4d1	Tomský
polytechnická	polytechnický	k2eAgNnPc4d1	polytechnické
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
University	universita	k1gFnPc4	universita
of	of	k?	of
Linköping	Linköping	k1gInSc4	Linköping
<g/>
,	,	kIx,	,
University	universita	k1gFnSc2	universita
of	of	k?	of
Texas	Texas	kA	Texas
at	at	k?	at
El	Ela	k1gFnPc2	Ela
Paso	Paso	k6eAd1	Paso
<g/>
,	,	kIx,	,
National	National	k1gFnSc3	National
Tsing	Tsinga	k1gFnPc2	Tsinga
Hua	Hua	k1gFnSc2	Hua
University	universita	k1gFnSc2	universita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zahraniční	zahraniční	k2eAgMnPc1d1	zahraniční
studenti	student	k1gMnPc1	student
===	===	k?	===
</s>
</p>
<p>
<s>
České	český	k2eAgNnSc1d1	české
vysoké	vysoký	k2eAgNnSc1d1	vysoké
učení	učení	k1gNnSc1	učení
technické	technický	k2eAgNnSc1d1	technické
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
nabízí	nabízet	k5eAaImIp3nS	nabízet
69	[number]	k4	69
studijních	studijní	k2eAgInPc2d1	studijní
programů	program	k1gInPc2	program
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
a	a	k8xC	a
ruském	ruský	k2eAgInSc6d1	ruský
jazyce	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
bakalářském	bakalářský	k2eAgNnSc6d1	bakalářské
<g/>
,	,	kIx,	,
magisterském	magisterský	k2eAgNnSc6d1	magisterské
i	i	k8xC	i
doktorském	doktorský	k2eAgNnSc6d1	doktorské
studiu	studio	k1gNnSc6	studio
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
svůj	svůj	k3xOyFgInSc1	svůj
akademický	akademický	k2eAgInSc1d1	akademický
život	život	k1gInSc1	život
na	na	k7c6	na
ČVUT	ČVUT	kA	ČVUT
tráví	trávit	k5eAaImIp3nP	trávit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
3500	[number]	k4	3500
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
studentů	student	k1gMnPc2	student
ze	z	k7c2	z
117	[number]	k4	117
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
kolem	kolem	k6eAd1	kolem
750	[number]	k4	750
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
jako	jako	k9	jako
studenti	student	k1gMnPc1	student
výměnných	výměnný	k2eAgInPc2d1	výměnný
programů	program	k1gInPc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
statistik	statistika	k1gFnPc2	statistika
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgMnSc1	každý
šestý	šestý	k4xOgMnSc1	šestý
student	student	k1gMnSc1	student
ČVUT	ČVUT	kA	ČVUT
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výraznou	výrazný	k2eAgFnSc7d1	výrazná
měrou	míra	k1gFnSc7wR	míra
narostl	narůst	k5eAaPmAgInS	narůst
počet	počet	k1gInSc1	počet
studentů	student	k1gMnPc2	student
samoplátců	samoplátce	k1gMnPc2	samoplátce
<g/>
,	,	kIx,	,
za	za	k7c4	za
poslední	poslední	k2eAgInPc4d1	poslední
roky	rok	k1gInPc4	rok
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
navýšení	navýšení	k1gNnSc3	navýšení
počtu	počet	k1gInSc2	počet
těchto	tento	k3xDgMnPc2	tento
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
na	na	k7c4	na
448	[number]	k4	448
<g/>
.	.	kIx.	.
</s>
<s>
Pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
více	hodně	k6eAd2	hodně
než	než	k8xS	než
40	[number]	k4	40
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgNnSc4d3	nejvyšší
zastupení	zastupení	k1gNnSc4	zastupení
mají	mít	k5eAaImIp3nP	mít
studenti	student	k1gMnPc1	student
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
(	(	kIx(	(
<g/>
107	[number]	k4	107
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kazachstánu	Kazachstán	k1gInSc2	Kazachstán
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ruska	Rusko	k1gNnSc2	Rusko
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
studuje	studovat	k5eAaImIp3nS	studovat
momentálně	momentálně	k6eAd1	momentálně
na	na	k7c6	na
Fakultě	fakulta	k1gFnSc6	fakulta
strojní	strojní	k2eAgNnSc1d1	strojní
(	(	kIx(	(
<g/>
181	[number]	k4	181
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
studijních	studijní	k2eAgFnPc6d1	studijní
skupinách	skupina	k1gFnPc6	skupina
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
přispívá	přispívat	k5eAaImIp3nS	přispívat
k	k	k7c3	k
individuálnímu	individuální	k2eAgInSc3d1	individuální
přístupu	přístup	k1gInSc3	přístup
ke	k	k7c3	k
studentům	student	k1gMnPc3	student
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2012	[number]	k4	2012
ČVUT	ČVUT	kA	ČVUT
spustilo	spustit	k5eAaPmAgNnS	spustit
web	web	k1gInSc4	web
studyatctu	studyatct	k1gInSc2	studyatct
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
uchazeči	uchazeč	k1gMnPc1	uchazeč
najdou	najít	k5eAaPmIp3nP	najít
kompletní	kompletní	k2eAgFnPc4d1	kompletní
informace	informace	k1gFnPc4	informace
<g/>
.	.	kIx.	.
</s>
<s>
Zájemci	zájemce	k1gMnPc1	zájemce
o	o	k7c4	o
studium	studium	k1gNnSc4	studium
na	na	k7c6	na
ČVUT	ČVUT	kA	ČVUT
je	být	k5eAaImIp3nS	být
poskytnuta	poskytnut	k2eAgFnSc1d1	poskytnuta
veškerá	veškerý	k3xTgFnSc1	veškerý
péče	péče	k1gFnSc1	péče
od	od	k7c2	od
prvních	první	k4xOgInPc2	první
kroků	krok	k1gInPc2	krok
nejen	nejen	k6eAd1	nejen
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
studijních	studijní	k2eAgFnPc2d1	studijní
informací	informace	k1gFnPc2	informace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
problémů	problém	k1gInPc2	problém
souvisejících	související	k2eAgInPc2d1	související
se	s	k7c7	s
životem	život	k1gInSc7	život
v	v	k7c6	v
cizí	cizí	k2eAgFnSc6d1	cizí
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
vždy	vždy	k6eAd1	vždy
aktuálních	aktuální	k2eAgFnPc2d1	aktuální
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
možnostech	možnost	k1gFnPc6	možnost
studia	studio	k1gNnSc2	studio
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
jazyce	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
procesu	proces	k1gInSc2	proces
výběrového	výběrový	k2eAgNnSc2d1	výběrové
konání	konání	k1gNnSc2	konání
<g/>
,	,	kIx,	,
získaní	získaný	k2eAgMnPc1d1	získaný
víz	vízo	k1gNnPc2	vízo
<g/>
,	,	kIx,	,
nostrifikace	nostrifikace	k1gFnSc1	nostrifikace
předešlého	předešlý	k2eAgNnSc2d1	předešlé
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
ubytování	ubytování	k1gNnSc2	ubytování
<g/>
,	,	kIx,	,
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
životních	životní	k2eAgInPc6d1	životní
nákladech	náklad	k1gInPc6	náklad
<g/>
,	,	kIx,	,
až	až	k9	až
po	po	k7c4	po
kulturní	kulturní	k2eAgInSc4d1	kulturní
život	život	k1gInSc4	život
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
možnosti	možnost	k1gFnSc6	možnost
cestování	cestování	k1gNnSc2	cestování
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
dotazy	dotaz	k1gInPc7	dotaz
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
obrátit	obrátit	k5eAaPmF	obrátit
na	na	k7c4	na
tzv.	tzv.	kA	tzv.
Student	student	k1gMnSc1	student
Advisora	Advisor	k1gMnSc2	Advisor
na	na	k7c4	na
e-mail	eail	k1gInSc4	e-mail
<g/>
,	,	kIx,	,
Skype	Skyp	k1gInSc5	Skyp
nebo	nebo	k8xC	nebo
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
prakticky	prakticky	k6eAd1	prakticky
nonstop	nonstop	k6eAd1	nonstop
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
pozitivní	pozitivní	k2eAgFnPc4d1	pozitivní
zkušenosti	zkušenost	k1gFnPc4	zkušenost
se	s	k7c7	s
získáváním	získávání	k1gNnSc7	získávání
zahraničních	zahraniční	k2eAgInPc2d1	zahraniční
studentů-samoplátců	studentůamoplátec	k1gInPc2	studentů-samoplátec
navazuje	navazovat	k5eAaImIp3nS	navazovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
projekt	projekt	k1gInSc1	projekt
Study	stud	k1gInPc1	stud
in	in	k?	in
Prague	Pragu	k1gInSc2	Pragu
<g/>
,	,	kIx,	,
koordinovaný	koordinovaný	k2eAgMnSc1d1	koordinovaný
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
ČVUT	ČVUT	kA	ČVUT
<g/>
,	,	kIx,	,
sdružující	sdružující	k2eAgFnSc1d1	sdružující
pět	pět	k4xCc1	pět
významných	významný	k2eAgFnPc2d1	významná
pražských	pražský	k2eAgFnPc2d1	Pražská
veřejných	veřejný	k2eAgFnPc2d1	veřejná
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
ČVUT	ČVUT	kA	ČVUT
jsou	být	k5eAaImIp3nP	být
členy	člen	k1gMnPc7	člen
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
<g/>
,	,	kIx,	,
VŠCHT	VŠCHT	kA	VŠCHT
<g/>
,	,	kIx,	,
VŠE	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
a	a	k8xC	a
ČZU	ČZU	kA	ČZU
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
představit	představit	k5eAaPmF	představit
Prahu	Praha	k1gFnSc4	Praha
jako	jako	k8xC	jako
atraktivní	atraktivní	k2eAgNnSc4d1	atraktivní
místo	místo	k1gNnSc4	místo
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
<g/>
,	,	kIx,	,
nabízející	nabízející	k2eAgFnSc4d1	nabízející
úplnou	úplný	k2eAgFnSc4d1	úplná
škálu	škála	k1gFnSc4	škála
studijních	studijní	k2eAgInPc2d1	studijní
programů	program	k1gInPc2	program
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
a	a	k8xC	a
pestrý	pestrý	k2eAgInSc1d1	pestrý
studentský	studentský	k2eAgInSc1d1	studentský
život	život	k1gInSc1	život
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
projekt	projekt	k1gInSc1	projekt
přispívá	přispívat	k5eAaImIp3nS	přispívat
k	k	k7c3	k
dalšímů	dalším	k1gMnPc2	dalším
nárůstu	nárůst	k1gInSc2	nárůst
studentů-samoplátců	studentůamoplátec	k1gMnPc2	studentů-samoplátec
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
zapojených	zapojený	k2eAgFnPc6d1	zapojená
univerzitách	univerzita	k1gFnPc6	univerzita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
při	při	k7c6	při
péči	péče	k1gFnSc6	péče
o	o	k7c4	o
zahraniční	zahraniční	k2eAgMnPc4d1	zahraniční
studenty	student	k1gMnPc4	student
sehrává	sehrávat	k5eAaImIp3nS	sehrávat
na	na	k7c6	na
škole	škola	k1gFnSc6	škola
působící	působící	k2eAgFnSc2d1	působící
International	International	k1gFnSc2	International
Student	student	k1gMnSc1	student
Club	club	k1gInSc1	club
(	(	kIx(	(
<g/>
ISC	ISC	kA	ISC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
například	například	k6eAd1	například
organizuje	organizovat	k5eAaBmIp3nS	organizovat
tzv.	tzv.	kA	tzv.
buddy	budda	k1gFnSc2	budda
program	program	k1gInSc4	program
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
čeští	český	k2eAgMnPc1d1	český
studenti	student	k1gMnPc1	student
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
studentům	student	k1gMnPc3	student
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
překonávat	překonávat	k5eAaImF	překonávat
počáteční	počáteční	k2eAgInSc4d1	počáteční
kulturní	kulturní	k2eAgInSc4d1	kulturní
rozdíly	rozdíl	k1gInPc4	rozdíl
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
pestré	pestrý	k2eAgFnSc2d1	pestrá
nabídky	nabídka	k1gFnSc2	nabídka
mimoškolních	mimoškolní	k2eAgFnPc2d1	mimoškolní
aktivit	aktivita	k1gFnPc2	aktivita
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
prezentace	prezentace	k1gFnSc2	prezentace
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
jazykové	jazykový	k2eAgInPc4d1	jazykový
kurzy	kurz	k1gInPc4	kurz
typu	typ	k1gInSc2	typ
student	student	k1gMnSc1	student
učí	učit	k5eAaImIp3nS	učit
studenty	student	k1gMnPc4	student
<g/>
,	,	kIx,	,
exkurze	exkurze	k1gFnSc1	exkurze
<g/>
,	,	kIx,	,
sport	sport	k1gInSc1	sport
a	a	k8xC	a
výlety	výlet	k1gInPc1	výlet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
českým	český	k2eAgMnPc3d1	český
studentům	student	k1gMnPc3	student
ke	k	k7c3	k
zvyšování	zvyšování	k1gNnSc3	zvyšování
jazykových	jazykový	k2eAgFnPc2d1	jazyková
znalostí	znalost	k1gFnPc2	znalost
a	a	k8xC	a
získávání	získávání	k1gNnSc1	získávání
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
přátel	přítel	k1gMnPc2	přítel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
dobré	dobrý	k2eAgFnSc2d1	dobrá
práce	práce	k1gFnSc2	práce
se	s	k7c7	s
zahraničními	zahraniční	k2eAgMnPc7d1	zahraniční
studenty	student	k1gMnPc7	student
je	být	k5eAaImIp3nS	být
i	i	k9	i
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
většina	většina	k1gFnSc1	většina
dotázaných	dotázaný	k2eAgMnPc2d1	dotázaný
studentů	student	k1gMnPc2	student
samoplátců	samoplátce	k1gMnPc2	samoplátce
(	(	kIx(	(
<g/>
80	[number]	k4	80
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
označuje	označovat	k5eAaImIp3nS	označovat
ČVUT	ČVUT	kA	ČVUT
jako	jako	k8xS	jako
první	první	k4xOgFnSc4	první
alternativu	alternativa	k1gFnSc4	alternativa
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
výběru	výběr	k1gInSc6	výběr
univerzitního	univerzitní	k2eAgNnSc2d1	univerzitní
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ubytování	ubytování	k1gNnSc4	ubytování
a	a	k8xC	a
stravování	stravování	k1gNnSc4	stravování
studentů	student	k1gMnPc2	student
==	==	k?	==
</s>
</p>
<p>
<s>
Škola	škola	k1gFnSc1	škola
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgNnSc4d1	vlastní
stravovací	stravovací	k2eAgNnSc4d1	stravovací
zařízení	zařízení	k1gNnSc4	zařízení
(	(	kIx(	(
<g/>
menzy	menza	k1gFnSc2	menza
<g/>
)	)	kIx)	)
i	i	k9	i
ubytovací	ubytovací	k2eAgNnPc1d1	ubytovací
zařízení	zařízení	k1gNnPc1	zařízení
(	(	kIx(	(
<g/>
koleje	kolej	k1gFnSc2	kolej
<g/>
)	)	kIx)	)
–	–	k?	–
celkem	celkem	k6eAd1	celkem
8	[number]	k4	8
000	[number]	k4	000
lůžek	lůžko	k1gNnPc2	lůžko
<g/>
.	.	kIx.	.
</s>
<s>
Menzy	menza	k1gFnPc1	menza
a	a	k8xC	a
koleje	kolej	k1gFnPc1	kolej
<g/>
,	,	kIx,	,
i	i	k9	i
včetně	včetně	k7c2	včetně
sportovišť	sportoviště	k1gNnPc2	sportoviště
<g/>
,	,	kIx,	,
spravuje	spravovat	k5eAaImIp3nS	spravovat
Správa	správa	k1gFnSc1	správa
účelových	účelový	k2eAgNnPc2d1	účelové
zařízení	zařízení	k1gNnPc2	zařízení
(	(	kIx(	(
<g/>
SÚZ	SÚZ	kA	SÚZ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Koleje	kolej	k1gFnSc2	kolej
ČVUT	ČVUT	kA	ČVUT
===	===	k?	===
</s>
</p>
<p>
<s>
Strahov	Strahov	k1gInSc1	Strahov
(	(	kIx(	(
<g/>
Břevnov	Břevnov	k1gInSc1	Břevnov
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Podolí	Podolí	k1gNnSc1	Podolí
</s>
</p>
<p>
<s>
Bubeneč	Bubeneč	k1gFnSc1	Bubeneč
</s>
</p>
<p>
<s>
Dejvická	dejvický	k2eAgFnSc1d1	Dejvická
(	(	kIx(	(
<g/>
Dejvice	Dejvice	k1gFnPc1	Dejvice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Orlík	Orlík	k1gInSc1	Orlík
(	(	kIx(	(
<g/>
Bubeneč	Bubeneč	k1gInSc1	Bubeneč
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hlávkova	Hlávkův	k2eAgFnSc1d1	Hlávkova
kolej	kolej	k1gFnSc1	kolej
(	(	kIx(	(
<g/>
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sinkuleho	Sinkuleze	k6eAd1	Sinkuleze
(	(	kIx(	(
<g/>
Dejvice	Dejvice	k1gFnPc1	Dejvice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
kolej	kolej	k1gFnSc1	kolej
(	(	kIx(	(
<g/>
Dejvice	Dejvice	k1gFnPc1	Dejvice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
====	====	k?	====
Další	další	k2eAgInPc1d1	další
ubytovací	ubytovací	k2eAgInPc1d1	ubytovací
provozy	provoz	k1gInPc1	provoz
====	====	k?	====
</s>
</p>
<p>
<s>
Novoměstský	novoměstský	k2eAgInSc1d1	novoměstský
hotel	hotel	k1gInSc1	hotel
(	(	kIx(	(
<g/>
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
<g/>
;	;	kIx,	;
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Menzy	menza	k1gFnSc2	menza
ČVUT	ČVUT	kA	ČVUT
===	===	k?	===
</s>
</p>
<p>
<s>
Studentský	studentský	k2eAgInSc4d1	studentský
dům	dům	k1gInSc4	dům
(	(	kIx(	(
<g/>
kampus	kampus	k1gInSc4	kampus
Dejvice	Dejvice	k1gFnPc4	Dejvice
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Strahovská	strahovský	k2eAgFnSc1d1	Strahovská
(	(	kIx(	(
<g/>
na	na	k7c6	na
kolejích	kolej	k1gFnPc6	kolej
Strahov	Strahov	k1gInSc1	Strahov
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Podolská	podolský	k2eAgFnSc1d1	Podolská
(	(	kIx(	(
<g/>
na	na	k7c6	na
kolejích	kolej	k1gFnPc6	kolej
v	v	k7c6	v
Podolí	Podolí	k1gNnSc6	Podolí
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
kolej	kolej	k1gFnSc1	kolej
(	(	kIx(	(
<g/>
Dejvice	Dejvice	k1gFnPc1	Dejvice
<g/>
,	,	kIx,	,
Thákurova	Thákurův	k2eAgFnSc1d1	Thákurova
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Výdejna	výdejna	k1gFnSc1	výdejna
Karlovo	Karlův	k2eAgNnSc1d1	Karlovo
náměstí	náměstí	k1gNnSc1	náměstí
(	(	kIx(	(
<g/>
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Menza	menza	k1gFnSc1	menza
Kladno	Kladno	k1gNnSc4	Kladno
(	(	kIx(	(
<g/>
na	na	k7c6	na
Fakultě	fakulta	k1gFnSc6	fakulta
biomedicínského	biomedicínský	k2eAgNnSc2d1	biomedicínské
inženýrství	inženýrství	k1gNnSc2	inženýrství
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Výdejna	výdejna	k1gFnSc1	výdejna
Horská	horský	k2eAgFnSc1d1	horská
(	(	kIx(	(
<g/>
na	na	k7c6	na
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
na	na	k7c6	na
Albertově	Albertův	k2eAgNnSc6d1	Albertovo
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
7	[number]	k4	7
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Pizzerie	pizzerie	k1gFnSc1	pizzerie
La	la	k1gNnSc2	la
Fontanella	Fontanello	k1gNnSc2	Fontanello
(	(	kIx(	(
<g/>
kampus	kampus	k1gInSc4	kampus
Dejvice	Dejvice	k1gFnPc4	Dejvice
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
8	[number]	k4	8
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
====	====	k?	====
Jídelníček	jídelníček	k1gInSc4	jídelníček
Menz	menza	k1gFnPc2	menza
ČVUT	ČVUT	kA	ČVUT
====	====	k?	====
</s>
</p>
<p>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
jídelny	jídelna	k1gFnPc1	jídelna
nabízejí	nabízet	k5eAaImIp3nP	nabízet
denně	denně	k6eAd1	denně
široký	široký	k2eAgInSc4d1	široký
výběr	výběr	k1gInSc4	výběr
pokrmů	pokrm	k1gInPc2	pokrm
<g/>
.	.	kIx.	.
<g/>
Ty	ty	k3xPp2nSc1	ty
lze	lze	k6eAd1	lze
vyhledat	vyhledat	k5eAaPmF	vyhledat
na	na	k7c6	na
následujícím	následující	k2eAgInSc6d1	následující
odkazu	odkaz	k1gInSc6	odkaz
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgInPc4d1	další
stravovací	stravovací	k2eAgInPc4d1	stravovací
provozy	provoz	k1gInPc4	provoz
===	===	k?	===
</s>
</p>
<p>
<s>
MEGA	mega	k1gNnSc1	mega
Buf	bufa	k1gFnPc2	bufa
Fat	fatum	k1gNnPc2	fatum
(	(	kIx(	(
<g/>
Fakulta	fakulta	k1gFnSc1	fakulta
stavební	stavební	k2eAgFnSc1d1	stavební
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Snack	Snack	k1gInSc1	Snack
Bar	bar	k1gInSc1	bar
(	(	kIx(	(
<g/>
budova	budova	k1gFnSc1	budova
SÚZ	SÚZ	kA	SÚZ
<g/>
,	,	kIx,	,
kolej	kolej	k1gFnSc1	kolej
Strahov	Strahov	k1gInSc1	Strahov
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ArchiCafé	ArchiCafé	k1gNnSc1	ArchiCafé
(	(	kIx(	(
<g/>
budova	budova	k1gFnSc1	budova
FIT	fit	k2eAgFnSc1d1	fit
<g/>
,	,	kIx,	,
FA	fa	k1gNnSc1	fa
<g/>
,	,	kIx,	,
Dejvice	Dejvice	k1gFnPc1	Dejvice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Studentské	studentský	k2eAgFnPc1d1	studentská
organizace	organizace	k1gFnPc1	organizace
==	==	k?	==
</s>
</p>
<p>
<s>
Studentské	studentský	k2eAgFnPc1d1	studentská
organizace	organizace	k1gFnPc1	organizace
jsou	být	k5eAaImIp3nP	být
nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
vysokoškolského	vysokoškolský	k2eAgInSc2d1	vysokoškolský
studentského	studentský	k2eAgInSc2d1	studentský
života	život	k1gInSc2	život
a	a	k8xC	a
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
ně	on	k3xPp3gInPc4	on
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
studenty	student	k1gMnPc7	student
rok	rok	k1gInSc4	rok
od	od	k7c2	od
roku	rok	k1gInSc2	rok
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
Aktivity	aktivita	k1gFnPc1	aktivita
studentských	studentský	k2eAgFnPc2d1	studentská
organizací	organizace	k1gFnPc2	organizace
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
rozmanité	rozmanitý	k2eAgFnPc1d1	rozmanitá
<g/>
,	,	kIx,	,
od	od	k7c2	od
kulturních	kulturní	k2eAgInPc2d1	kulturní
a	a	k8xC	a
sportovních	sportovní	k2eAgInPc2d1	sportovní
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
odborné	odborný	k2eAgInPc4d1	odborný
až	až	k9	až
po	po	k7c4	po
mezinárodní	mezinárodní	k2eAgFnPc4d1	mezinárodní
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
studentských	studentský	k2eAgFnPc2d1	studentská
organizací	organizace	k1gFnPc2	organizace
a	a	k8xC	a
klubů	klub	k1gInPc2	klub
vykonávají	vykonávat	k5eAaImIp3nP	vykonávat
mnohdy	mnohdy	k6eAd1	mnohdy
vysoce	vysoce	k6eAd1	vysoce
kvalifikované	kvalifikovaný	k2eAgFnPc1d1	kvalifikovaná
činnosti	činnost	k1gFnPc1	činnost
spojené	spojený	k2eAgFnPc1d1	spojená
s	s	k7c7	s
jejich	jejich	k3xOp3gInSc7	jejich
provozem	provoz	k1gInSc7	provoz
<g/>
,	,	kIx,	,
či	či	k8xC	či
s	s	k7c7	s
jejich	jejich	k3xOp3gFnSc7	jejich
vlastní	vlastní	k2eAgFnSc7d1	vlastní
náplní	náplň	k1gFnSc7	náplň
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zpravidla	zpravidla	k6eAd1	zpravidla
bez	bez	k7c2	bez
nároků	nárok	k1gInPc2	nárok
na	na	k7c4	na
honorář	honorář	k1gInSc4	honorář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Studentská	studentský	k2eAgFnSc1d1	studentská
unie	unie	k1gFnSc1	unie
ČVUT	ČVUT	kA	ČVUT
===	===	k?	===
</s>
</p>
<p>
<s>
Studentská	studentský	k2eAgFnSc1d1	studentská
unie	unie	k1gFnSc1	unie
ČVUT	ČVUT	kA	ČVUT
(	(	kIx(	(
<g/>
dále	daleko	k6eAd2	daleko
jen	jen	k9	jen
SU	SU	k?	SU
ČVUT	ČVUT	kA	ČVUT
<g/>
)	)	kIx)	)
vznikala	vznikat	k5eAaImAgFnS	vznikat
z	z	k7c2	z
iniciativy	iniciativa	k1gFnSc2	iniciativa
samotných	samotný	k2eAgMnPc2d1	samotný
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
z	z	k7c2	z
Akademického	akademický	k2eAgInSc2d1	akademický
senátu	senát	k1gInSc2	senát
ČVUT	ČVUT	kA	ČVUT
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
místopředsedy	místopředseda	k1gMnSc2	místopředseda
AS	as	k1gNnSc2	as
ČVUT	ČVUT	kA	ČVUT
za	za	k7c7	za
studenty	student	k1gMnPc7	student
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Kuby	Kuba	k1gMnSc2	Kuba
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
řady	řada	k1gFnSc2	řada
dalších	další	k2eAgMnPc2d1	další
studentů	student	k1gMnPc2	student
postupně	postupně	k6eAd1	postupně
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
centrálou	centrála	k1gFnSc7	centrála
<g/>
,	,	kIx,	,
kontrolním	kontrolní	k2eAgInSc7d1	kontrolní
výborem	výbor	k1gInSc7	výbor
<g/>
,	,	kIx,	,
kolejními	kolejní	k2eAgInPc7d1	kolejní
<g/>
,	,	kIx,	,
zájmovými	zájmový	k2eAgInPc7d1	zájmový
a	a	k8xC	a
externími	externí	k2eAgInPc7d1	externí
kluby	klub	k1gInPc7	klub
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodování	rozhodování	k1gNnSc1	rozhodování
o	o	k7c6	o
budoucnosti	budoucnost	k1gFnSc6	budoucnost
SU	SU	k?	SU
ČVUT	ČVUT	kA	ČVUT
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
starosti	starost	k1gFnSc6	starost
parlament	parlament	k1gInSc1	parlament
Studentské	studentský	k2eAgFnSc2d1	studentská
unie	unie	k1gFnSc2	unie
ČVUT	ČVUT	kA	ČVUT
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
patnácti	patnáct	k4xCc7	patnáct
delegáty	delegát	k1gMnPc4	delegát
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
klubů	klub	k1gInPc2	klub
<g/>
,	,	kIx,	,
delegátem	delegát	k1gMnSc7	delegát
za	za	k7c4	za
Akademický	akademický	k2eAgInSc4d1	akademický
senát	senát	k1gInSc4	senát
ČVUT	ČVUT	kA	ČVUT
a	a	k8xC	a
presidentem	president	k1gMnSc7	president
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
SU	SU	k?	SU
ČVUT	ČVUT	kA	ČVUT
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
studenty	student	k1gMnPc4	student
před	před	k7c7	před
univerzitou	univerzita	k1gFnSc7	univerzita
a	a	k8xC	a
Správou	správa	k1gFnSc7	správa
účelových	účelový	k2eAgNnPc2d1	účelové
zařízení	zařízení	k1gNnPc2	zařízení
ČVUT	ČVUT	kA	ČVUT
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
organizuje	organizovat	k5eAaBmIp3nS	organizovat
společenské	společenský	k2eAgFnPc4d1	společenská
<g/>
,	,	kIx,	,
kulturní	kulturní	k2eAgFnPc4d1	kulturní
a	a	k8xC	a
sportovní	sportovní	k2eAgFnPc4d1	sportovní
akce	akce	k1gFnPc4	akce
a	a	k8xC	a
podílí	podílet	k5eAaImIp3nS	podílet
se	se	k3xPyFc4	se
na	na	k7c4	na
reprezentaci	reprezentace	k1gFnSc4	reprezentace
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
veřejných	veřejný	k2eAgFnPc2d1	veřejná
akcí	akce	k1gFnPc2	akce
<g/>
.	.	kIx.	.
</s>
<s>
Bližší	blízký	k2eAgFnPc4d2	bližší
informace	informace	k1gFnPc4	informace
o	o	k7c4	o
SU	SU	k?	SU
ČVUT	ČVUT	kA	ČVUT
naleznete	naleznout	k5eAaPmIp2nP	naleznout
na	na	k7c6	na
webu	web	k1gInSc6	web
www.su.cvut.cz	www.su.cvut.cz	k1gInSc1	www.su.cvut.cz
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
nás	my	k3xPp1nPc4	my
můžete	moct	k5eAaImIp2nP	moct
sledovat	sledovat	k5eAaImF	sledovat
na	na	k7c6	na
facebooku	facebook	k1gInSc6	facebook
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Studentská	studentský	k2eAgFnSc1d1	studentská
unie	unie	k1gFnSc1	unie
ČVUT	ČVUT	kA	ČVUT
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Informovanost	informovanost	k1gFnSc1	informovanost
</s>
</p>
<p>
<s>
Průvodce	průvodce	k1gMnSc1	průvodce
prváka	prvák	k1gMnSc2	prvák
po	po	k7c6	po
ČVUT	ČVUT	kA	ČVUT
-	-	kIx~	-
studentův	studentův	k2eAgMnSc1d1	studentův
průvodce	průvodce	k1gMnSc1	průvodce
životem	život	k1gInSc7	život
na	na	k7c4	na
ČVUT	ČVUT	kA	ČVUT
(	(	kIx(	(
<g/>
v	v	k7c6	v
elektronické	elektronický	k2eAgFnSc6d1	elektronická
i	i	k8xC	i
tištěné	tištěný	k2eAgFnSc6d1	tištěná
podobě	podoba	k1gFnSc6	podoba
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Akce	akce	k1gFnSc1	akce
prvák	prvák	k1gMnSc1	prvák
-	-	kIx~	-
součást	součást	k1gFnSc1	součást
Ahoj	ahoj	k0	ahoj
techniko	technika	k1gFnSc5	technika
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
usnadnění	usnadnění	k1gNnSc4	usnadnění
studia	studio	k1gNnSc2	studio
na	na	k7c4	na
vysoké	vysoký	k2eAgFnPc4d1	vysoká
školy	škola	k1gFnPc4	škola
pro	pro	k7c4	pro
prváky	prvák	k1gInPc4	prvák
(	(	kIx(	(
<g/>
web	web	k1gInSc4	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
webový	webový	k2eAgInSc1d1	webový
portál	portál	k1gInSc1	portál
Student	student	k1gMnSc1	student
<g/>
.	.	kIx.	.
<g/>
cvut	cvut	k1gMnSc1	cvut
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
podporu	podpora	k1gFnSc4	podpora
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
pobavení	pobavení	k1gNnSc4	pobavení
studentů	student	k1gMnPc2	student
a	a	k8xC	a
informuje	informovat	k5eAaBmIp3nS	informovat
o	o	k7c6	o
všech	všecek	k3xTgFnPc6	všecek
akcích	akce	k1gFnPc6	akce
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
na	na	k7c4	na
ČVUT	ČVUT	kA	ČVUT
pořádají	pořádat	k5eAaImIp3nP	pořádat
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zábavu	zábava	k1gFnSc4	zábava
</s>
</p>
<p>
<s>
Strahov	Strahov	k1gInSc1	Strahov
Open	Open	k1gInSc1	Open
Air	Air	k1gFnSc1	Air
-	-	kIx~	-
součást	součást	k1gFnSc1	součást
Ahoj	ahoj	k0	ahoj
techniko	technika	k1gFnSc5	technika
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgInSc1d1	hudební
festival	festival	k1gInSc1	festival
pod	pod	k7c7	pod
širým	širý	k2eAgNnSc7d1	širé
nebem	nebe	k1gNnSc7	nebe
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Studentský	studentský	k2eAgInSc1d1	studentský
ples	ples	k1gInSc1	ples
-	-	kIx~	-
maškarní	maškarní	k2eAgInSc1d1	maškarní
ples	ples	k1gInSc1	ples
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
koleje	kolej	k1gFnSc2	kolej
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
bloky	blok	k1gInPc4	blok
-	-	kIx~	-
hudební	hudební	k2eAgInSc1d1	hudební
festival	festival	k1gInSc1	festival
a	a	k8xC	a
největší	veliký	k2eAgFnSc1d3	veliký
pravidelná	pravidelný	k2eAgFnSc1d1	pravidelná
akce	akce	k1gFnSc1	akce
pořádaná	pořádaný	k2eAgFnSc1d1	pořádaná
na	na	k7c6	na
Praze	Praha	k1gFnSc6	Praha
4	[number]	k4	4
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
MayDej	MayDej	k1gInSc1	MayDej
-	-	kIx~	-
sportovně	sportovně	k6eAd1	sportovně
společenský	společenský	k2eAgInSc1d1	společenský
den	den	k1gInSc1	den
plný	plný	k2eAgInSc1d1	plný
zábavy	zábava	k1gFnSc2	zábava
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vánoční	vánoční	k2eAgInSc1d1	vánoční
večírek	večírek	k1gInSc1	večírek
-	-	kIx~	-
společenská	společenský	k2eAgFnSc1d1	společenská
akce	akce	k1gFnSc1	akce
pořádaná	pořádaný	k2eAgFnSc1d1	pořádaná
před	před	k7c7	před
vánočními	vánoční	k2eAgInPc7d1	vánoční
svátky	svátek	k1gInPc7	svátek
</s>
</p>
<p>
<s>
Volný	volný	k2eAgInSc1d1	volný
čas	čas	k1gInSc1	čas
</s>
</p>
<p>
<s>
Čajovna	čajovna	k1gFnSc1	čajovna
-	-	kIx~	-
příjemné	příjemný	k2eAgNnSc1d1	příjemné
posezení	posezení	k1gNnSc1	posezení
a	a	k8xC	a
široká	široký	k2eAgFnSc1d1	široká
nabídka	nabídka	k1gFnSc1	nabídka
nejrůznějších	různý	k2eAgInPc2d3	nejrůznější
druhů	druh	k1gInPc2	druh
čajů	čaj	k1gInPc2	čaj
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Klub	klub	k1gInSc1	klub
STOlních	stolní	k2eAgFnPc2d1	stolní
Her	hra	k1gFnPc2	hra
(	(	kIx(	(
<g/>
STOH	stoh	k1gInSc1	stoh
<g/>
)	)	kIx)	)
-	-	kIx~	-
hráčské	hráčský	k2eAgInPc1d1	hráčský
večery	večer	k1gInPc1	večer
pro	pro	k7c4	pro
každého	každý	k3xTgMnSc4	každý
a	a	k8xC	a
turnaje	turnaj	k1gInPc4	turnaj
o	o	k7c4	o
zajímavé	zajímavý	k2eAgFnPc4d1	zajímavá
ceny	cena	k1gFnPc4	cena
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Křesťani	Křesťan	k1gMnPc1	Křesťan
-	-	kIx~	-
podpora	podpora	k1gFnSc1	podpora
duchovního	duchovní	k2eAgInSc2d1	duchovní
a	a	k8xC	a
osobního	osobní	k2eAgInSc2d1	osobní
rozvoje	rozvoj	k1gInSc2	rozvoj
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
GALIBI	GALIBI	kA	GALIBI
-	-	kIx~	-
vysokoškolsky	vysokoškolsky	k6eAd1	vysokoškolsky
gay-lesbický	gayesbický	k2eAgInSc1d1	gay-lesbický
spolek	spolek	k1gInSc1	spolek
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
posilovny	posilovna	k1gFnPc1	posilovna
a	a	k8xC	a
sportoviště	sportoviště	k1gNnSc1	sportoviště
-	-	kIx~	-
na	na	k7c6	na
kolejích	kolej	k1gFnPc6	kolej
</s>
</p>
<p>
<s>
Vzdělávání	vzdělávání	k1gNnSc1	vzdělávání
</s>
</p>
<p>
<s>
Cisco	Cisco	k6eAd1	Cisco
Networking	Networking	k1gInSc1	Networking
Academy	Academa	k1gFnSc2	Academa
-	-	kIx~	-
čtyř	čtyři	k4xCgMnPc2	čtyři
semestrální	semestrální	k2eAgInSc1d1	semestrální
vzdělávací	vzdělávací	k2eAgInSc1d1	vzdělávací
program	program	k1gInSc1	program
zaměřený	zaměřený	k2eAgInSc1d1	zaměřený
na	na	k7c4	na
síťové	síťový	k2eAgFnPc4d1	síťová
technologie	technologie	k1gFnPc4	technologie
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Středisko	středisko	k1gNnSc1	středisko
UNIXových	unixový	k2eAgFnPc2d1	unixová
Technologií	technologie	k1gFnPc2	technologie
(	(	kIx(	(
<g/>
SUT	sout	k5eAaImNgInS	sout
<g/>
)	)	kIx)	)
-	-	kIx~	-
semináře	seminář	k1gInPc1	seminář
o	o	k7c6	o
operačním	operační	k2eAgInSc6d1	operační
systému	systém	k1gInSc6	systém
GNU	gnu	k1gMnSc1	gnu
<g/>
/	/	kIx~	/
<g/>
Linux	Linux	kA	Linux
a	a	k8xC	a
svobodném	svobodný	k2eAgInSc6d1	svobodný
softwaru	software	k1gInSc6	software
jako	jako	k8xS	jako
takovém	takový	k3xDgInSc6	takový
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
InstallFest	InstallFest	k1gFnSc1	InstallFest
-	-	kIx~	-
akce	akce	k1gFnSc1	akce
pro	pro	k7c4	pro
zájemce	zájemce	k1gMnPc4	zájemce
o	o	k7c4	o
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
GNU	gnu	k1gNnSc2	gnu
<g/>
/	/	kIx~	/
<g/>
Linux	linux	k1gInSc1	linux
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Podporu	podpora	k1gFnSc4	podpora
</s>
</p>
<p>
<s>
Buddy	Buddy	k6eAd1	Buddy
program	program	k1gInSc1	program
-	-	kIx~	-
přivítání	přivítání	k1gNnSc1	přivítání
a	a	k8xC	a
péče	péče	k1gFnSc1	péče
o	o	k7c4	o
zahraniční	zahraniční	k2eAgMnPc4d1	zahraniční
studenty	student	k1gMnPc4	student
<g/>
,	,	kIx,	,
doprovázeno	doprovázen	k2eAgNnSc1d1	doprovázeno
celou	celý	k2eAgFnSc7d1	celá
řadou	řada	k1gFnSc7	řada
akcí	akce	k1gFnPc2	akce
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
koleje	kolej	k1gFnPc1	kolej
-	-	kIx~	-
jednání	jednání	k1gNnSc1	jednání
s	s	k7c7	s
vedením	vedení	k1gNnSc7	vedení
Správy	správa	k1gFnSc2	správa
účelových	účelový	k2eAgNnPc2d1	účelové
zařízení	zařízení	k1gNnPc2	zařízení
ČVUT	ČVUT	kA	ČVUT
</s>
</p>
<p>
<s>
tiskárny	tiskárna	k1gFnPc1	tiskárna
</s>
</p>
<p>
<s>
síťové	síťový	k2eAgFnPc1d1	síťová
služby	služba	k1gFnPc1	služba
</s>
</p>
<p>
<s>
studovny	studovna	k1gFnPc1	studovna
<g/>
,	,	kIx,	,
společenské	společenský	k2eAgInPc1d1	společenský
místnostiVeletrh	místnostiVeletrh	k1gInSc4	místnostiVeletrh
iKariéra	iKariéra	k1gFnSc1	iKariéra
-	-	kIx~	-
veletrh	veletrh	k1gInSc1	veletrh
pro	pro	k7c4	pro
studenty	student	k1gMnPc4	student
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
mají	mít	k5eAaImIp3nP	mít
možnost	možnost	k1gFnSc4	možnost
potkat	potkat	k5eAaPmF	potkat
se	se	k3xPyFc4	se
s	s	k7c7	s
firmami	firma	k1gFnPc7	firma
(	(	kIx(	(
<g/>
nabídky	nabídka	k1gFnPc1	nabídka
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
brigád	brigáda	k1gFnPc2	brigáda
<g/>
,	,	kIx,	,
diplomových	diplomový	k2eAgFnPc2d1	Diplomová
či	či	k8xC	či
bakalářských	bakalářský	k2eAgFnPc2d1	Bakalářská
prací	práce	k1gFnPc2	práce
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Kluby	klub	k1gInPc1	klub
Studentské	studentský	k2eAgFnSc2d1	studentská
unie	unie	k1gFnSc2	unie
ČVUT	ČVUT	kA	ČVUT
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Kolejní	kolejní	k2eAgInPc1d1	kolejní
kluby	klub	k1gInPc1	klub
====	====	k?	====
</s>
</p>
<p>
<s>
Starají	starat	k5eAaImIp3nP	starat
se	se	k3xPyFc4	se
o	o	k7c4	o
blaho	blaho	k1gNnSc4	blaho
studentů	student	k1gMnPc2	student
ubytovaných	ubytovaný	k2eAgMnPc2d1	ubytovaný
na	na	k7c6	na
koleji	kolej	k1gFnSc6	kolej
<g/>
,	,	kIx,	,
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
svým	svůj	k3xOyFgMnPc3	svůj
členům	člen	k1gMnPc3	člen
připojení	připojení	k1gNnSc4	připojení
k	k	k7c3	k
internetu	internet	k1gInSc3	internet
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
služby	služba	k1gFnPc4	služba
jako	jako	k8xC	jako
tisk	tisk	k1gInSc4	tisk
dokumentů	dokument	k1gInPc2	dokument
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgFnSc4d1	vlastní
emailovou	emailový	k2eAgFnSc4d1	emailová
schránku	schránka	k1gFnSc4	schránka
<g/>
,	,	kIx,	,
webový	webový	k2eAgInSc4d1	webový
prostor	prostor	k1gInSc4	prostor
<g/>
,	,	kIx,	,
digitální	digitální	k2eAgFnSc4d1	digitální
televizi	televize	k1gFnSc4	televize
po	po	k7c6	po
síti	síť	k1gFnSc6	síť
jiné	jiný	k2eAgInPc4d1	jiný
projekty	projekt	k1gInPc4	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Komunikují	komunikovat	k5eAaImIp3nP	komunikovat
s	s	k7c7	s
vedením	vedení	k1gNnSc7	vedení
koleje	kolej	k1gFnSc2	kolej
<g/>
,	,	kIx,	,
spravují	spravovat	k5eAaImIp3nP	spravovat
a	a	k8xC	a
půjčují	půjčovat	k5eAaImIp3nP	půjčovat
zájmové	zájmový	k2eAgFnPc4d1	zájmová
místnosti	místnost	k1gFnPc4	místnost
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
pořádají	pořádat	k5eAaImIp3nP	pořádat
také	také	k9	také
různé	různý	k2eAgFnPc4d1	různá
společenské	společenský	k2eAgFnPc4d1	společenská
<g/>
,	,	kIx,	,
sportovní	sportovní	k2eAgFnPc4d1	sportovní
a	a	k8xC	a
vzdělávací	vzdělávací	k2eAgFnPc4d1	vzdělávací
akce	akce	k1gFnPc4	akce
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
všechny	všechen	k3xTgInPc4	všechen
lze	lze	k6eAd1	lze
jmenovat	jmenovat	k5eAaImF	jmenovat
ten	ten	k3xDgInSc4	ten
největší	veliký	k2eAgInSc4d3	veliký
<g/>
,	,	kIx,	,
strahovský	strahovský	k2eAgInSc4d1	strahovský
Silicon	Silicon	kA	Silicon
Hill	Hill	k1gInSc1	Hill
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Zájmové	zájmový	k2eAgInPc1d1	zájmový
kluby	klub	k1gInPc1	klub
====	====	k?	====
</s>
</p>
<p>
<s>
Studenti	student	k1gMnPc1	student
ČVUT	ČVUT	kA	ČVUT
mají	mít	k5eAaImIp3nP	mít
své	svůj	k3xOyFgInPc4	svůj
rozličné	rozličný	k2eAgInPc4d1	rozličný
zájmy	zájem	k1gInPc4	zájem
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
technická	technický	k2eAgFnSc1d1	technická
duše	duše	k1gFnSc1	duše
si	se	k3xPyFc3	se
čas	čas	k1gInSc4	čas
od	od	k7c2	od
času	čas	k1gInSc2	čas
také	také	k9	také
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
povyrazit	povyrazit	k5eAaPmF	povyrazit
<g/>
.	.	kIx.	.
</s>
<s>
Zájmové	zájmový	k2eAgInPc1d1	zájmový
kluby	klub	k1gInPc1	klub
přináší	přinášet	k5eAaImIp3nS	přinášet
do	do	k7c2	do
života	život	k1gInSc2	život
studentů	student	k1gMnPc2	student
zábavu	zábav	k1gInSc2	zábav
<g/>
,	,	kIx,	,
poučení	poučení	k1gNnSc1	poučení
i	i	k8xC	i
oddech	oddech	k1gInSc1	oddech
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
sport	sport	k1gInSc1	sport
<g/>
.	.	kIx.	.
</s>
<s>
Nabízejí	nabízet	k5eAaImIp3nP	nabízet
jak	jak	k8xS	jak
příjemné	příjemný	k2eAgNnSc4d1	příjemné
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
tak	tak	k9	tak
možnost	možnost	k1gFnSc4	možnost
otevřené	otevřený	k2eAgFnSc2d1	otevřená
diskuse	diskuse	k1gFnSc2	diskuse
na	na	k7c4	na
různá	různý	k2eAgNnPc4d1	různé
témata	téma	k1gNnPc4	téma
nebo	nebo	k8xC	nebo
možnost	možnost	k1gFnSc4	možnost
poměřit	poměřit	k5eAaPmF	poměřit
síly	síla	k1gFnPc4	síla
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
hrách	hra	k1gFnPc6	hra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Zahraniční	zahraniční	k2eAgInPc1d1	zahraniční
kluby	klub	k1gInPc1	klub
====	====	k?	====
</s>
</p>
<p>
<s>
Kluby	klub	k1gInPc1	klub
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
typu	typ	k1gInSc2	typ
se	se	k3xPyFc4	se
starají	starat	k5eAaImIp3nP	starat
o	o	k7c4	o
zahraniční	zahraniční	k2eAgMnPc4d1	zahraniční
studenty	student	k1gMnPc4	student
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
přijedou	přijet	k5eAaPmIp3nP	přijet
na	na	k7c6	na
ČVUT	ČVUT	kA	ČVUT
a	a	k8xC	a
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
jim	on	k3xPp3gMnPc3	on
překonávat	překonávat	k5eAaImF	překonávat
počáteční	počáteční	k2eAgFnPc1d1	počáteční
obtíže	obtíž	k1gFnPc1	obtíž
v	v	k7c6	v
neznámém	známý	k2eNgNnSc6d1	neznámé
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
nabízejí	nabízet	k5eAaImIp3nP	nabízet
studentům	student	k1gMnPc3	student
vycestovat	vycestovat	k5eAaPmF	vycestovat
na	na	k7c6	na
praxi	praxe	k1gFnSc6	praxe
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
podívat	podívat	k5eAaPmF	podívat
se	se	k3xPyFc4	se
do	do	k7c2	do
světa	svět	k1gInSc2	svět
a	a	k8xC	a
něco	něco	k3yInSc1	něco
se	se	k3xPyFc4	se
přiučit	přiučit	k5eAaPmF	přiučit
<g/>
.	.	kIx.	.
</s>
<s>
Aktivity	aktivita	k1gFnPc1	aktivita
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
klubů	klub	k1gInPc2	klub
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
podstatně	podstatně	k6eAd1	podstatně
širší	široký	k2eAgInSc4d2	širší
a	a	k8xC	a
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
pořádání	pořádání	k1gNnSc4	pořádání
různých	různý	k2eAgFnPc2d1	různá
akcí	akce	k1gFnPc2	akce
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
dalšího	další	k2eAgNnSc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Zahraniční	zahraniční	k2eAgInPc1d1	zahraniční
kluby	klub	k1gInPc1	klub
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
IAESTE	IAESTE	kA	IAESTE
<g/>
,	,	kIx,	,
International	International	k1gFnSc1	International
Student	student	k1gMnSc1	student
Club	club	k1gInSc1	club
ČVUT	ČVUT	kA	ČVUT
a	a	k8xC	a
BEST	BEST	kA	BEST
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hudební	hudební	k2eAgFnSc1d1	hudební
kultura	kultura	k1gFnSc1	kultura
na	na	k7c4	na
ČVUT	ČVUT	kA	ČVUT
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Akademický	akademický	k2eAgInSc1d1	akademický
orchestr	orchestr	k1gInSc1	orchestr
ČVUT	ČVUT	kA	ČVUT
===	===	k?	===
</s>
</p>
<p>
<s>
Vystoupení	vystoupení	k1gNnSc1	vystoupení
britského	britský	k2eAgInSc2d1	britský
orchestru	orchestr	k1gInSc2	orchestr
a	a	k8xC	a
sboru	sbor	k1gInSc2	sbor
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Cambridge	Cambridge	k1gFnSc1	Cambridge
orchestra	orchestra	k1gFnSc1	orchestra
and	and	k?	and
chorus	chorus	k1gInSc1	chorus
<g/>
"	"	kIx"	"
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2006	[number]	k4	2006
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
koncertů	koncert	k1gInPc2	koncert
konaných	konaný	k2eAgInPc2d1	konaný
k	k	k7c3	k
300	[number]	k4	300
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
vzniku	vznik	k1gInSc2	vznik
ČVUT	ČVUT	kA	ČVUT
podnítilo	podnítit	k5eAaPmAgNnS	podnítit
prorektora	prorektor	k1gMnSc2	prorektor
ČVUT	ČVUT	kA	ČVUT
prof.	prof.	kA	prof.
Vejražku	Vejražku	k?	Vejražku
k	k	k7c3	k
předložení	předložení	k1gNnSc3	předložení
návrhu	návrh	k1gInSc2	návrh
opatření	opatření	k1gNnPc2	opatření
vedoucích	vedoucí	k1gMnPc2	vedoucí
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
orchestru	orchestr	k1gInSc2	orchestr
<g/>
.	.	kIx.	.
</s>
<s>
Dirigentská	dirigentský	k2eAgFnSc1d1	dirigentská
taktovka	taktovka	k1gFnSc1	taktovka
byla	být	k5eAaImAgFnS	být
svěřena	svěřen	k2eAgFnSc1d1	svěřena
Janu	Jan	k1gMnSc3	Jan
Šrámkovi	Šrámek	k1gMnSc3	Šrámek
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
úkolem	úkol	k1gInSc7	úkol
budovat	budovat	k5eAaImF	budovat
reprezentační	reprezentační	k2eAgNnSc4d1	reprezentační
těleso	těleso	k1gNnSc4	těleso
ČVUT	ČVUT	kA	ČVUT
<g/>
.	.	kIx.	.
</s>
<s>
Zakrátko	zakrátko	k6eAd1	zakrátko
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
malý	malý	k2eAgInSc1d1	malý
smyčcový	smyčcový	k2eAgInSc1d1	smyčcový
ansámbl	ansámbl	k1gInSc1	ansámbl
a	a	k8xC	a
flétnový	flétnový	k2eAgInSc1d1	flétnový
soubor	soubor	k1gInSc1	soubor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
působí	působit	k5eAaImIp3nS	působit
i	i	k9	i
jako	jako	k9	jako
samostatné	samostatný	k2eAgNnSc4d1	samostatné
hudební	hudební	k2eAgNnSc4d1	hudební
těleso	těleso	k1gNnSc4	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Orchestr	orchestr	k1gInSc1	orchestr
ČVUT	ČVUT	kA	ČVUT
je	být	k5eAaImIp3nS	být
jedinečné	jedinečný	k2eAgNnSc4d1	jedinečné
hudební	hudební	k2eAgNnSc4d1	hudební
univerzitní	univerzitní	k2eAgNnSc4d1	univerzitní
těleso	těleso	k1gNnSc4	těleso
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
propojuje	propojovat	k5eAaImIp3nS	propojovat
profesionální	profesionální	k2eAgMnPc4d1	profesionální
mladé	mladý	k2eAgMnPc4d1	mladý
umělce	umělec	k1gMnPc4	umělec
<g/>
,	,	kIx,	,
s	s	k7c7	s
neprofesionálními	profesionální	k2eNgMnPc7d1	neprofesionální
hudebníky	hudebník	k1gMnPc7	hudebník
studující	studující	k2eAgFnSc2d1	studující
vysokoškolské	vysokoškolský	k2eAgFnSc2d1	vysokoškolská
obory	obora	k1gFnSc2	obora
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	k9	aby
jejich	jejich	k3xOp3gInPc1	jejich
koncerty	koncert	k1gInPc1	koncert
byly	být	k5eAaImAgInP	být
co	co	k9	co
nejzajímavější	zajímavý	k2eAgMnSc1d3	nejzajímavější
jak	jak	k8xS	jak
pro	pro	k7c4	pro
publikum	publikum	k1gNnSc4	publikum
<g/>
,	,	kIx,	,
tak	tak	k9	tak
pro	pro	k7c4	pro
samotné	samotný	k2eAgMnPc4d1	samotný
členy	člen	k1gMnPc4	člen
orchestru	orchestr	k1gInSc2	orchestr
<g/>
,	,	kIx,	,
zvou	zvát	k5eAaImIp3nP	zvát
ke	k	k7c3	k
spolupráci	spolupráce	k1gFnSc6	spolupráce
vynikající	vynikající	k2eAgMnPc4d1	vynikající
sólisty	sólista	k1gMnPc4	sólista
a	a	k8xC	a
renomované	renomovaný	k2eAgMnPc4d1	renomovaný
hudebníky	hudebník	k1gMnPc4	hudebník
české	český	k2eAgFnSc2d1	Česká
hudební	hudební	k2eAgFnSc2d1	hudební
scény	scéna	k1gFnSc2	scéna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stálým	stálý	k2eAgNnSc7d1	stálé
působištěm	působiště	k1gNnSc7	působiště
orchestru	orchestr	k1gInSc2	orchestr
je	být	k5eAaImIp3nS	být
pražská	pražský	k2eAgFnSc1d1	Pražská
Betlémská	betlémský	k2eAgFnSc1d1	Betlémská
kaple	kaple	k1gFnSc1	kaple
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pořádá	pořádat	k5eAaImIp3nS	pořádat
své	svůj	k3xOyFgInPc4	svůj
pravidelné	pravidelný	k2eAgInPc4d1	pravidelný
koncerty	koncert	k1gInPc4	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Orchestru	orchestr	k1gInSc2	orchestr
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
pozvání	pozvání	k1gNnSc4	pozvání
i	i	k9	i
k	k	k7c3	k
vystoupením	vystoupení	k1gNnSc7	vystoupení
mimo	mimo	k7c4	mimo
akademickou	akademický	k2eAgFnSc4d1	akademická
obec	obec	k1gFnSc4	obec
ČVUT	ČVUT	kA	ČVUT
<g/>
,	,	kIx,	,
např.	např.	kA	např.
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2008	[number]	k4	2008
v	v	k7c6	v
Senátu	senát	k1gInSc6	senát
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
v	v	k7c6	v
září	září	k1gNnSc6	září
2009	[number]	k4	2009
ve	v	k7c6	v
Španělském	španělský	k2eAgInSc6d1	španělský
sále	sál	k1gInSc6	sál
Pražského	pražský	k2eAgInSc2d1	pražský
Hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2013	[number]	k4	2013
soubor	soubor	k1gInSc1	soubor
vystoupil	vystoupit	k5eAaPmAgInS	vystoupit
také	také	k9	také
na	na	k7c4	na
slavné	slavný	k2eAgFnPc4d1	slavná
University	universita	k1gFnPc4	universita
of	of	k?	of
Cambridge	Cambridge	k1gFnPc4	Cambridge
k	k	k7c3	k
příležitosti	příležitost	k1gFnSc3	příležitost
konání	konání	k1gNnSc2	konání
akce	akce	k1gFnSc2	akce
Jewels	Jewelsa	k1gFnPc2	Jewelsa
of	of	k?	of
Czech	Czech	k1gMnSc1	Czech
Music	Music	k1gMnSc1	Music
v	v	k7c6	v
Emmanuel	Emmanuel	k1gMnSc1	Emmanuel
United	United	k1gMnSc1	United
Reformed	Reformed	k1gMnSc1	Reformed
Church	Church	k1gMnSc1	Church
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pěvecký	pěvecký	k2eAgInSc1d1	pěvecký
sbor	sbor	k1gInSc1	sbor
===	===	k?	===
</s>
</p>
<p>
<s>
Smíšený	smíšený	k2eAgInSc1d1	smíšený
Pěvecký	pěvecký	k2eAgInSc1d1	pěvecký
sbor	sbor	k1gInSc1	sbor
ČVUT	ČVUT	kA	ČVUT
je	být	k5eAaImIp3nS	být
uměleckým	umělecký	k2eAgNnSc7d1	umělecké
sdružením	sdružení	k1gNnSc7	sdružení
s	s	k7c7	s
tradicí	tradice	k1gFnSc7	tradice
sahající	sahající	k2eAgInSc4d1	sahající
až	až	k6eAd1	až
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
skoro	skoro	k6eAd1	skoro
sto	sto	k4xCgNnSc4	sto
členy	člen	k1gMnPc7	člen
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
největším	veliký	k2eAgNnPc3d3	veliký
pražským	pražský	k2eAgNnPc3d1	Pražské
hudebním	hudební	k2eAgNnPc3d1	hudební
tělesům	těleso	k1gNnPc3	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
absolventů	absolvent	k1gMnPc2	absolvent
a	a	k8xC	a
profesorů	profesor	k1gMnPc2	profesor
ČVUT	ČVUT	kA	ČVUT
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
řadách	řada	k1gFnPc6	řada
i	i	k8xC	i
další	další	k2eAgMnPc1d1	další
zájemci	zájemce	k1gMnPc1	zájemce
o	o	k7c4	o
sborový	sborový	k2eAgInSc4d1	sborový
zpěv	zpěv	k1gInSc4	zpěv
nejrůznějších	různý	k2eAgNnPc2d3	nejrůznější
povolání	povolání	k1gNnPc2	povolání
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
amatérský	amatérský	k2eAgInSc4d1	amatérský
sbor	sbor	k1gInSc4	sbor
<g/>
,	,	kIx,	,
vystřídalo	vystřídat	k5eAaPmAgNnS	vystřídat
se	se	k3xPyFc4	se
během	během	k7c2	během
historie	historie	k1gFnSc2	historie
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
vedení	vedení	k1gNnSc1	vedení
mnoho	mnoho	k6eAd1	mnoho
vynikajících	vynikající	k2eAgMnPc2d1	vynikající
profesionálních	profesionální	k2eAgMnPc2d1	profesionální
hudebníků	hudebník	k1gMnPc2	hudebník
<g/>
.	.	kIx.	.
</s>
<s>
Repertoár	repertoár	k1gInSc1	repertoár
sboru	sbor	k1gInSc2	sbor
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
vážnou	vážný	k2eAgFnSc4d1	vážná
hudbu	hudba	k1gFnSc4	hudba
od	od	k7c2	od
renesance	renesance	k1gFnSc2	renesance
až	až	k9	až
po	po	k7c4	po
současnost	současnost	k1gFnSc4	současnost
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
pozornost	pozornost	k1gFnSc1	pozornost
je	být	k5eAaImIp3nS	být
věnována	věnovat	k5eAaImNgFnS	věnovat
hudbě	hudba	k1gFnSc3	hudba
českých	český	k2eAgMnPc2d1	český
autorů	autor	k1gMnPc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
sbor	sbor	k1gInSc1	sbor
nebrání	bránit	k5eNaImIp3nS	bránit
i	i	k9	i
jiným	jiný	k2eAgInPc3d1	jiný
hudebním	hudební	k2eAgInPc3d1	hudební
žánrům	žánr	k1gInPc3	žánr
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
úpravy	úprava	k1gFnPc4	úprava
lidových	lidový	k2eAgFnPc2d1	lidová
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
spirituály	spirituál	k1gInPc1	spirituál
<g/>
,	,	kIx,	,
či	či	k8xC	či
aranže	aranže	k1gFnSc1	aranže
rockové	rockový	k2eAgFnSc2d1	rocková
a	a	k8xC	a
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Pěvecký	pěvecký	k2eAgInSc1d1	pěvecký
sbor	sbor	k1gInSc1	sbor
ČVUT	ČVUT	kA	ČVUT
pravidelně	pravidelně	k6eAd1	pravidelně
účinkuje	účinkovat	k5eAaImIp3nS	účinkovat
na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
a	a	k8xC	a
hudebních	hudební	k2eAgInPc6d1	hudební
festivalech	festival	k1gInPc6	festival
jak	jak	k8xS	jak
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
profesionálními	profesionální	k2eAgMnPc7d1	profesionální
sólisty	sólista	k1gMnPc7	sólista
a	a	k8xC	a
hudebními	hudební	k2eAgNnPc7d1	hudební
tělesy	těleso	k1gNnPc7	těleso
(	(	kIx(	(
<g/>
Symfonický	symfonický	k2eAgInSc4d1	symfonický
orchestr	orchestr	k1gInSc4	orchestr
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
,	,	kIx,	,
Severočeská	severočeský	k2eAgFnSc1d1	Severočeská
filharmonie	filharmonie	k1gFnSc1	filharmonie
Teplice	Teplice	k1gFnPc1	Teplice
<g/>
,	,	kIx,	,
Hudba	hudba	k1gFnSc1	hudba
Hradní	hradní	k2eAgFnSc2d1	hradní
stráže	stráž	k1gFnSc2	stráž
a	a	k8xC	a
policie	policie	k1gFnSc2	policie
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
s	s	k7c7	s
nimiž	jenž	k3xRgInPc7	jenž
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
na	na	k7c6	na
předních	přední	k2eAgNnPc6d1	přední
pražských	pražský	k2eAgNnPc6d1	Pražské
koncertních	koncertní	k2eAgNnPc6d1	koncertní
pódiích	pódium	k1gNnPc6	pódium
(	(	kIx(	(
<g/>
Dvořákova	Dvořákův	k2eAgFnSc1d1	Dvořákova
síň	síň	k1gFnSc1	síň
Rudolfina	Rudolfin	k2eAgFnSc1d1	Rudolfina
<g/>
,	,	kIx,	,
Smetanova	Smetanův	k2eAgFnSc1d1	Smetanova
síň	síň	k1gFnSc1	síň
Obecního	obecní	k2eAgInSc2d1	obecní
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
Sál	sál	k1gInSc4	sál
paláce	palác	k1gInSc2	palác
Žofín	Žofín	k1gInSc1	Žofín
<g/>
,	,	kIx,	,
Španělský	španělský	k2eAgInSc1d1	španělský
sál	sál	k1gInSc1	sál
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
Katedrála	katedrála	k1gFnSc1	katedrála
sv.	sv.	kA	sv.
Víta	Víta	k1gFnSc1	Víta
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sbor	sbor	k1gInSc1	sbor
již	již	k6eAd1	již
několikrát	několikrát	k6eAd1	několikrát
natáčel	natáčet	k5eAaImAgMnS	natáčet
pro	pro	k7c4	pro
Českou	český	k2eAgFnSc4d1	Česká
televizi	televize	k1gFnSc4	televize
a	a	k8xC	a
bývá	bývat	k5eAaImIp3nS	bývat
zván	zvát	k5eAaImNgInS	zvát
Kanceláří	kancelář	k1gFnSc7	kancelář
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
k	k	k7c3	k
uměleckému	umělecký	k2eAgNnSc3d1	umělecké
vystupování	vystupování	k1gNnSc3	vystupování
při	při	k7c6	při
slavnostních	slavnostní	k2eAgFnPc6d1	slavnostní
příležitostech	příležitost	k1gFnPc6	příležitost
a	a	k8xC	a
výročích	výročí	k1gNnPc6	výročí
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
<g/>
Sbormistrem	sbormistr	k1gMnSc7	sbormistr
tělesa	těleso	k1gNnSc2	těleso
je	být	k5eAaImIp3nS	být
Jan	Jan	k1gMnSc1	Jan
Steyer	Steyer	k1gMnSc1	Steyer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Symfonický	symfonický	k2eAgInSc1d1	symfonický
a	a	k8xC	a
Komorní	komorní	k2eAgInSc1d1	komorní
orchestr	orchestr	k1gInSc1	orchestr
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
Symfonický	symfonický	k2eAgInSc1d1	symfonický
orchestr	orchestr	k1gInSc1	orchestr
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
pak	pak	k6eAd1	pak
Komorní	komorní	k2eAgInSc1d1	komorní
orchestr	orchestr	k1gInSc1	orchestr
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
tělesa	těleso	k1gNnPc1	těleso
vznikla	vzniknout	k5eAaPmAgNnP	vzniknout
jako	jako	k9	jako
orchestry	orchestr	k1gInPc1	orchestr
SÚDOP	SÚDOP	kA	SÚDOP
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
cca	cca	kA	cca
tří	tři	k4xCgInPc2	tři
let	léto	k1gNnPc2	léto
působila	působit	k5eAaImAgFnS	působit
jako	jako	k9	jako
orchestry	orchestra	k1gFnPc1	orchestra
ČSD	ČSD	kA	ČSD
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
orchestry	orchestra	k1gFnPc4	orchestra
ČVUT	ČVUT	kA	ČVUT
<g/>
.	.	kIx.	.
</s>
<s>
Byť	byť	k8xS	byť
orchestry	orchestr	k1gInPc1	orchestr
původně	původně	k6eAd1	původně
sdružovaly	sdružovat	k5eAaImAgInP	sdružovat
hráče	hráč	k1gMnPc4	hráč
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
SÚDOPu	SÚDOPa	k1gFnSc4	SÚDOPa
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
do	do	k7c2	do
orchestru	orchestr	k1gInSc2	orchestr
přihlásit	přihlásit	k5eAaPmF	přihlásit
i	i	k9	i
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nejsou	být	k5eNaImIp3nP	být
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
firmou	firma	k1gFnSc7	firma
spojeni	spojit	k5eAaPmNgMnP	spojit
a	a	k8xC	a
ani	ani	k8xC	ani
nemusejí	muset	k5eNaImIp3nP	muset
být	být	k5eAaImF	být
studenty	student	k1gMnPc4	student
či	či	k8xC	či
absolventy	absolvent	k1gMnPc4	absolvent
ČVUT	ČVUT	kA	ČVUT
<g/>
.	.	kIx.	.
<g/>
Orchestry	orchestr	k1gInPc1	orchestr
jako	jako	k8xS	jako
dirigent	dirigent	k1gMnSc1	dirigent
vedl	vést	k5eAaImAgMnS	vést
Ivo	Ivo	k1gMnSc1	Ivo
Kraupner	Kraupner	k1gMnSc1	Kraupner
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
vystřídán	vystřídat	k5eAaPmNgInS	vystřídat
Janem	Jan	k1gMnSc7	Jan
Štrosem	Štros	k1gMnSc7	Štros
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
čele	čelo	k1gNnSc6	čelo
Lukáš	Lukáš	k1gMnSc1	Lukáš
Kovařík	Kovařík	k1gMnSc1	Kovařík
<g/>
.	.	kIx.	.
</s>
<s>
Tělesa	těleso	k1gNnPc1	těleso
vystupovala	vystupovat	k5eAaImAgNnP	vystupovat
jak	jak	k6eAd1	jak
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
v	v	k7c6	v
Miličíně	Miličína	k1gFnSc6	Miličína
či	či	k8xC	či
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
Betlémské	betlémský	k2eAgFnSc6d1	Betlémská
kapli	kaple	k1gFnSc6	kaple
<g/>
,	,	kIx,	,
Smetanově	Smetanův	k2eAgFnSc6d1	Smetanova
síni	síň	k1gFnSc6	síň
Obecního	obecní	k2eAgInSc2d1	obecní
domu	dům	k1gInSc2	dům
nebo	nebo	k8xC	nebo
Dvořákově	Dvořákův	k2eAgFnSc3d1	Dvořákova
síni	síň	k1gFnSc3	síň
Rudolfina	Rudolfinum	k1gNnSc2	Rudolfinum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
(	(	kIx(	(
<g/>
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
nebo	nebo	k8xC	nebo
Vatikán	Vatikán	k1gInSc1	Vatikán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
orchestry	orchestr	k1gInPc7	orchestr
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
též	též	k9	též
i	i	k9	i
významní	významný	k2eAgMnPc1d1	významný
hudebníci	hudebník	k1gMnPc1	hudebník
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
houslista	houslista	k1gMnSc1	houslista
Čeněk	Čeněk	k1gMnSc1	Čeněk
Pavlík	Pavlík	k1gMnSc1	Pavlík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Řídící	řídící	k2eAgInPc1d1	řídící
a	a	k8xC	a
správní	správní	k2eAgInPc1d1	správní
orgány	orgán	k1gInPc1	orgán
==	==	k?	==
</s>
</p>
<p>
<s>
Vrcholným	vrcholný	k2eAgMnSc7d1	vrcholný
představitelem	představitel	k1gMnSc7	představitel
a	a	k8xC	a
reprezentantem	reprezentant	k1gMnSc7	reprezentant
ČVUT	ČVUT	kA	ČVUT
navenek	navenek	k6eAd1	navenek
je	být	k5eAaImIp3nS	být
rektor	rektor	k1gMnSc1	rektor
jmenovaný	jmenovaný	k2eAgMnSc1d1	jmenovaný
prezidentem	prezident	k1gMnSc7	prezident
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rektor	rektor	k1gMnSc1	rektor
má	mít	k5eAaImIp3nS	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
jako	jako	k8xS	jako
poradní	poradní	k2eAgInPc1d1	poradní
orgány	orgán	k1gInPc1	orgán
kolegium	kolegium	k1gNnSc1	kolegium
rektora	rektor	k1gMnSc2	rektor
(	(	kIx(	(
<g/>
prorektoři	prorektor	k1gMnPc1	prorektor
<g/>
,	,	kIx,	,
děkani	děkan	k1gMnPc1	děkan
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
fakult	fakulta	k1gFnPc2	fakulta
<g/>
,	,	kIx,	,
kvestor	kvestor	k1gMnSc1	kvestor
<g/>
,	,	kIx,	,
kancléř	kancléř	k1gMnSc1	kancléř
a	a	k8xC	a
předseda	předseda	k1gMnSc1	předseda
AS	as	k1gNnSc2	as
ČVUT	ČVUT	kA	ČVUT
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
grémium	grémium	k1gNnSc1	grémium
rektora	rektor	k1gMnSc2	rektor
(	(	kIx(	(
<g/>
členové	člen	k1gMnPc1	člen
kolegia	kolegium	k1gNnSc2	kolegium
<g/>
,	,	kIx,	,
ředitelé	ředitel	k1gMnPc1	ředitel
součástí	součást	k1gFnPc2	součást
ČVUT	ČVUT	kA	ČVUT
a	a	k8xC	a
místopředseda	místopředseda	k1gMnSc1	místopředseda
AS	as	k1gNnSc2	as
ČVUT	ČVUT	kA	ČVUT
za	za	k7c7	za
studenty	student	k1gMnPc7	student
<g/>
)	)	kIx)	)
a	a	k8xC	a
vedení	vedení	k1gNnSc1	vedení
ČVUT	ČVUT	kA	ČVUT
(	(	kIx(	(
<g/>
rektor	rektor	k1gMnSc1	rektor
<g/>
,	,	kIx,	,
prorektoři	prorektor	k1gMnPc1	prorektor
<g/>
,	,	kIx,	,
kvestor	kvestor	k1gMnSc1	kvestor
<g/>
,	,	kIx,	,
kancléř	kancléř	k1gMnSc1	kancléř
a	a	k8xC	a
předseda	předseda	k1gMnSc1	předseda
AS	as	k1gNnSc2	as
ČVUT	ČVUT	kA	ČVUT
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prorektoři	prorektor	k1gMnPc1	prorektor
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
rektora	rektor	k1gMnSc4	rektor
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jim	on	k3xPp3gMnPc3	on
rektor	rektor	k1gMnSc1	rektor
určí	určit	k5eAaPmIp3nS	určit
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
zahraniční	zahraniční	k2eAgInPc4d1	zahraniční
vztahy	vztah	k1gInPc4	vztah
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
a	a	k8xC	a
výzkumnou	výzkumný	k2eAgFnSc4d1	výzkumná
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
a	a	k8xC	a
studentské	studentský	k2eAgFnPc4d1	studentská
záležitosti	záležitost	k1gFnPc4	záležitost
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
a	a	k8xC	a
pro	pro	k7c4	pro
informační	informační	k2eAgInSc4d1	informační
systém	systém	k1gInSc4	systém
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
výkon	výkon	k1gInSc4	výkon
své	svůj	k3xOyFgFnSc2	svůj
činnosti	činnost	k1gFnSc2	činnost
rektor	rektor	k1gMnSc1	rektor
zodpovídá	zodpovídat	k5eAaPmIp3nS	zodpovídat
akademickému	akademický	k2eAgInSc3d1	akademický
senátu	senát	k1gInSc3	senát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
samosprávným	samosprávný	k2eAgMnSc7d1	samosprávný
zastupitelským	zastupitelský	k2eAgInSc7d1	zastupitelský
orgánem	orgán	k1gInSc7	orgán
<g/>
,	,	kIx,	,
voleným	volený	k2eAgInSc7d1	volený
akademickou	akademický	k2eAgFnSc7d1	akademická
obcí	obec	k1gFnSc7	obec
ČVUT	ČVUT	kA	ČVUT
na	na	k7c4	na
tříleté	tříletý	k2eAgNnSc4d1	tříleté
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
senátu	senát	k1gInSc6	senát
je	být	k5eAaImIp3nS	být
každá	každý	k3xTgFnSc1	každý
fakulta	fakulta	k1gFnSc1	fakulta
zastoupena	zastoupit	k5eAaPmNgFnS	zastoupit
třemi	tři	k4xCgMnPc7	tři
akademickými	akademický	k2eAgMnPc7d1	akademický
pracovníky	pracovník	k1gMnPc7	pracovník
a	a	k8xC	a
dvěma	dva	k4xCgMnPc7	dva
studenty	student	k1gMnPc7	student
<g/>
.	.	kIx.	.
</s>
<s>
Rektorát	rektorát	k1gInSc1	rektorát
<g/>
,	,	kIx,	,
vysokoškolské	vysokoškolský	k2eAgInPc1d1	vysokoškolský
ústavy	ústav	k1gInPc1	ústav
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
součásti	součást	k1gFnPc1	součást
ČVUT	ČVUT	kA	ČVUT
jsou	být	k5eAaImIp3nP	být
dohromady	dohromady	k6eAd1	dohromady
zastoupeny	zastoupit	k5eAaPmNgFnP	zastoupit
třemi	tři	k4xCgMnPc7	tři
akademickými	akademický	k2eAgMnPc7d1	akademický
pracovníky	pracovník	k1gMnPc7	pracovník
a	a	k8xC	a
dvěma	dva	k4xCgMnPc7	dva
studenty	student	k1gMnPc7	student
<g/>
.	.	kIx.	.
</s>
<s>
Senát	senát	k1gInSc1	senát
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
schvaluje	schvalovat	k5eAaImIp3nS	schvalovat
vnitřní	vnitřní	k2eAgInPc4d1	vnitřní
předpisy	předpis	k1gInPc4	předpis
<g/>
,	,	kIx,	,
rozpočet	rozpočet	k1gInSc1	rozpočet
předložený	předložený	k2eAgInSc1d1	předložený
rektorem	rektor	k1gMnSc7	rektor
<g/>
,	,	kIx,	,
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
hospodaření	hospodaření	k1gNnSc1	hospodaření
a	a	k8xC	a
schvaluje	schvalovat	k5eAaImIp3nS	schvalovat
výroční	výroční	k2eAgFnSc4d1	výroční
zprávu	zpráva	k1gFnSc4	zpráva
a	a	k8xC	a
dlouhodobý	dlouhodobý	k2eAgInSc4d1	dlouhodobý
záměr	záměr	k1gInSc4	záměr
<g/>
.	.	kIx.	.
</s>
<s>
Řízení	řízení	k1gNnSc1	řízení
hospodaření	hospodaření	k1gNnSc2	hospodaření
a	a	k8xC	a
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
správu	správa	k1gFnSc4	správa
školy	škola	k1gFnSc2	škola
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
starosti	starost	k1gFnSc6	starost
kvestor	kvestor	k1gMnSc1	kvestor
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobý	dlouhodobý	k2eAgInSc1d1	dlouhodobý
záměr	záměr	k1gInSc1	záměr
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
studijní	studijní	k2eAgInPc4d1	studijní
programy	program	k1gInPc4	program
projednává	projednávat	k5eAaImIp3nS	projednávat
a	a	k8xC	a
schvaluje	schvalovat	k5eAaImIp3nS	schvalovat
vědecká	vědecký	k2eAgFnSc1d1	vědecká
rada	rada	k1gFnSc1	rada
ČVUT	ČVUT	kA	ČVUT
<g/>
,	,	kIx,	,
jejímiž	jejíž	k3xOyRp3gMnPc7	jejíž
členy	člen	k1gMnPc7	člen
jsou	být	k5eAaImIp3nP	být
významní	významný	k2eAgMnPc1d1	významný
představitelé	představitel	k1gMnPc1	představitel
oborů	obor	k1gInPc2	obor
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgFnPc6	který
škola	škola	k1gFnSc1	škola
působí	působit	k5eAaImIp3nS	působit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
činnost	činnost	k1gFnSc4	činnost
univerzity	univerzita	k1gFnSc2	univerzita
dohlíží	dohlížet	k5eAaImIp3nS	dohlížet
správní	správní	k2eAgFnSc1d1	správní
rada	rada	k1gFnSc1	rada
ČVUT	ČVUT	kA	ČVUT
(	(	kIx(	(
<g/>
jmenovaná	jmenovaný	k2eAgFnSc1d1	jmenovaná
ministrem	ministr	k1gMnSc7	ministr
školství	školství	k1gNnSc2	školství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
hlavně	hlavně	k9	hlavně
rozpočet	rozpočet	k1gInSc4	rozpočet
či	či	k8xC	či
dlouhodobý	dlouhodobý	k2eAgInSc4d1	dlouhodobý
záměr	záměr	k1gInSc4	záměr
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
fakulta	fakulta	k1gFnSc1	fakulta
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
děkana	děkan	k1gMnSc2	děkan
jako	jako	k8xS	jako
nejvyššího	vysoký	k2eAgMnSc2d3	nejvyšší
představitele	představitel	k1gMnSc2	představitel
<g/>
,	,	kIx,	,
akademický	akademický	k2eAgInSc1d1	akademický
senát	senát	k1gInSc1	senát
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
orgán	orgán	k1gInSc1	orgán
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
děkana	děkan	k1gMnSc4	děkan
volí	volit	k5eAaImIp3nS	volit
<g/>
,	,	kIx,	,
a	a	k8xC	a
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
radu	rada	k1gFnSc4	rada
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
spravuje	spravovat	k5eAaImIp3nS	spravovat
studijní	studijní	k2eAgInPc4d1	studijní
plány	plán	k1gInPc4	plán
<g/>
.	.	kIx.	.
</s>
<s>
Poradním	poradní	k2eAgInSc7d1	poradní
orgánem	orgán	k1gInSc7	orgán
děkana	děkan	k1gMnSc2	děkan
jsou	být	k5eAaImIp3nP	být
kolegium	kolegium	k1gNnSc4	kolegium
děkana	děkan	k1gMnSc2	děkan
a	a	k8xC	a
grémium	grémium	k1gNnSc1	grémium
děkana	děkan	k1gMnSc2	děkan
<g/>
.	.	kIx.	.
</s>
<s>
Svými	svůj	k3xOyFgInPc7	svůj
zástupci	zástupce	k1gMnPc1	zástupce
pro	pro	k7c4	pro
některé	některý	k3yIgFnPc4	některý
činnosti	činnost	k1gFnPc4	činnost
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
děkan	děkan	k1gMnSc1	děkan
proděkany	proděkan	k1gMnPc4	proděkan
a	a	k8xC	a
svého	svůj	k3xOyFgMnSc2	svůj
tajemníka	tajemník	k1gMnSc2	tajemník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Každá	každý	k3xTgFnSc1	každý
fakulta	fakulta	k1gFnSc1	fakulta
má	mít	k5eAaImIp3nS	mít
svou	svůj	k3xOyFgFnSc4	svůj
disciplinární	disciplinární	k2eAgFnSc4d1	disciplinární
komisi	komise	k1gFnSc4	komise
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
projednává	projednávat	k5eAaImIp3nS	projednávat
přestupky	přestupek	k1gInPc4	přestupek
studentů	student	k1gMnPc2	student
zapsaných	zapsaný	k2eAgMnPc2d1	zapsaný
na	na	k7c6	na
dané	daný	k2eAgFnSc6d1	daná
fakultě	fakulta	k1gFnSc6	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Přestupky	přestupek	k1gInPc4	přestupek
studentů	student	k1gMnPc2	student
ČVUT	ČVUT	kA	ČVUT
zapsaných	zapsaný	k2eAgNnPc2d1	zapsané
na	na	k7c6	na
mimofakultních	mimofakultní	k2eAgInPc6d1	mimofakultní
vysokoškolských	vysokoškolský	k2eAgInPc6d1	vysokoškolský
ústavech	ústav	k1gInPc6	ústav
projednává	projednávat	k5eAaImIp3nS	projednávat
disciplinární	disciplinární	k2eAgFnSc1d1	disciplinární
komise	komise	k1gFnSc1	komise
ČVUT	ČVUT	kA	ČVUT
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Členové	člen	k1gMnPc1	člen
akademické	akademický	k2eAgFnSc2d1	akademická
obce	obec	k1gFnSc2	obec
ČVUT	ČVUT	kA	ČVUT
a	a	k8xC	a
ostatní	ostatní	k2eAgMnPc1d1	ostatní
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nP	řídit
etickými	etický	k2eAgInPc7d1	etický
principy	princip	k1gInPc7	princip
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
vztahů	vztah	k1gInPc2	vztah
na	na	k7c6	na
ČVUT	ČVUT	kA	ČVUT
i	i	k8xC	i
vztahů	vztah	k1gInPc2	vztah
vůči	vůči	k7c3	vůči
společnosti	společnost	k1gFnSc3	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Souhrn	souhrn	k1gInSc1	souhrn
těchto	tento	k3xDgInPc2	tento
základních	základní	k2eAgInPc2d1	základní
principů	princip	k1gInPc2	princip
je	být	k5eAaImIp3nS	být
zakotven	zakotvit	k5eAaPmNgInS	zakotvit
v	v	k7c6	v
Etickém	etický	k2eAgInSc6d1	etický
kodexu	kodex	k1gInSc6	kodex
ČVUT	ČVUT	kA	ČVUT
<g/>
.	.	kIx.	.
</s>
<s>
Strategické	strategický	k2eAgFnPc4d1	strategická
vize	vize	k1gFnPc4	vize
a	a	k8xC	a
mise	mise	k1gFnPc4	mise
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
ve	v	k7c6	v
Strategii	strategie	k1gFnSc6	strategie
ČVUT	ČVUT	kA	ČVUT
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Významné	významný	k2eAgFnPc1d1	významná
osobnosti	osobnost	k1gFnPc1	osobnost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
historii	historie	k1gFnSc6	historie
univerzity	univerzita	k1gFnSc2	univerzita
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
působilo	působit	k5eAaImAgNnS	působit
mnoho	mnoho	k4c1	mnoho
významných	významný	k2eAgFnPc2d1	významná
osobností	osobnost	k1gFnPc2	osobnost
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
Gerstner	Gerstner	k1gMnSc1	Gerstner
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
projektu	projekt	k1gInSc2	projekt
koněspřežní	koněspřežní	k2eAgFnSc2d1	koněspřežní
železnice	železnice	k1gFnSc2	železnice
z	z	k7c2	z
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
do	do	k7c2	do
Lince	Linec	k1gInSc2	Linec
</s>
</p>
<p>
<s>
Christian	Christian	k1gMnSc1	Christian
Doppler	Doppler	k1gMnSc1	Doppler
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
matematiky	matematika	k1gFnSc2	matematika
a	a	k8xC	a
praktické	praktický	k2eAgFnSc2d1	praktická
geometrie	geometrie	k1gFnSc2	geometrie
</s>
</p>
<p>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Puluj	Puluj	k1gMnSc1	Puluj
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
lékařské	lékařský	k2eAgFnSc2d1	lékařská
radiologie	radiologie	k1gFnSc2	radiologie
</s>
</p>
<p>
<s>
Gustav	Gustav	k1gMnSc1	Gustav
Schmidt	Schmidt	k1gMnSc1	Schmidt
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
für	für	k?	für
Maschinenbau	Maschinenbaa	k1gMnSc4	Maschinenbaa
(	(	kIx(	(
<g/>
strojírenství	strojírenství	k1gNnSc2	strojírenství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rektor	rektor	k1gMnSc1	rektor
1868	[number]	k4	1868
</s>
</p>
<p>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Frič	Frič	k1gMnSc1	Frič
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
<g/>
,	,	kIx,	,
paleontolog	paleontolog	k1gMnSc1	paleontolog
<g/>
,	,	kIx,	,
zoolog	zoolog	k1gMnSc1	zoolog
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Kořistka	Kořistka	k1gFnSc1	Kořistka
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
<g/>
,	,	kIx,	,
rektor	rektor	k1gMnSc1	rektor
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Krejčí	Krejčí	k1gMnSc1	Krejčí
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
geologie	geologie	k1gFnSc2	geologie
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Křižík	Křižík	k1gMnSc1	Křižík
<g/>
,	,	kIx,	,
absolvent	absolvent	k1gMnSc1	absolvent
ČVUT	ČVUT	kA	ČVUT
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Zítek	Zítek	k1gMnSc1	Zítek
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
architektury	architektura	k1gFnSc2	architektura
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Zvoníček	Zvoníček	k1gMnSc1	Zvoníček
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
teorie	teorie	k1gFnSc2	teorie
a	a	k8xC	a
návrhu	návrh	k1gInSc2	návrh
parních	parní	k2eAgInPc2d1	parní
strojů	stroj	k1gInPc2	stroj
a	a	k8xC	a
kompresorů	kompresor	k1gInPc2	kompresor
<g/>
,	,	kIx,	,
rektor	rektor	k1gMnSc1	rektor
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Běhounek	běhounek	k1gMnSc1	běhounek
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
fyziky	fyzika	k1gFnSc2	fyzika
</s>
</p>
<p>
<s>
Simon	Simon	k1gMnSc1	Simon
Wiesenthal	Wiesenthal	k1gMnSc1	Wiesenthal
<g/>
,	,	kIx,	,
absolvent	absolvent	k1gMnSc1	absolvent
ČVUT	ČVUT	kA	ČVUT
</s>
</p>
<p>
<s>
Emil	Emil	k1gMnSc1	Emil
Votoček	Votočka	k1gFnPc2	Votočka
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
chemie	chemie	k1gFnSc2	chemie
</s>
</p>
<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Lukeš	Lukeš	k1gMnSc1	Lukeš
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
organické	organický	k2eAgFnSc2d1	organická
chemie	chemie	k1gFnSc2	chemie
</s>
</p>
<p>
<s>
Vladimir	Vladimir	k1gMnSc1	Vladimir
Prelog	Prelog	k1gMnSc1	Prelog
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
–	–	k?	–
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
absolvent	absolvent	k1gMnSc1	absolvent
ČVUT	ČVUT	kA	ČVUT
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
ETH	ETH	kA	ETH
a	a	k8xC	a
držitel	držitel	k1gMnSc1	držitel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1975	[number]	k4	1975
</s>
</p>
<p>
<s>
Heinrich	Heinrich	k1gMnSc1	Heinrich
Mandel	mandel	k1gInSc1	mandel
<g/>
,	,	kIx,	,
absolvent	absolvent	k1gMnSc1	absolvent
ČVUT	ČVUT	kA	ČVUT
</s>
</p>
<p>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Engel	Engel	k1gMnSc1	Engel
<g/>
,	,	kIx,	,
absolvent	absolvent	k1gMnSc1	absolvent
ČVUT	ČVUT	kA	ČVUT
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
a	a	k8xC	a
urbanista	urbanista	k1gMnSc1	urbanista
</s>
</p>
<p>
<s>
Otakar	Otakar	k1gMnSc1	Otakar
Husák	Husák	k1gMnSc1	Husák
<g/>
,	,	kIx,	,
absolvent	absolvent	k1gMnSc1	absolvent
ČVUT	ČVUT	kA	ČVUT
<g/>
,	,	kIx,	,
chemik	chemik	k1gMnSc1	chemik
<g/>
,	,	kIx,	,
generál	generál	k1gMnSc1	generál
<g/>
,	,	kIx,	,
československý	československý	k2eAgMnSc1d1	československý
legionář	legionář	k1gMnSc1	legionář
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
a	a	k8xC	a
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
bojovník	bojovník	k1gMnSc1	bojovník
od	od	k7c2	od
Zborova	Zborov	k1gInSc2	Zborov
a	a	k8xC	a
Terronu	Terron	k1gInSc2	Terron
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
vojenské	vojenský	k2eAgFnSc2d1	vojenská
kanceláře	kancelář	k1gFnSc2	kancelář
presidenta	president	k1gMnSc4	president
Masaryka	Masaryk	k1gMnSc4	Masaryk
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
obrany	obrana	k1gFnSc2	obrana
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
ředitel	ředitel	k1gMnSc1	ředitel
továrny	továrna	k1gFnSc2	továrna
Explosia	Explosia	k1gFnSc1	Explosia
Semtín	Semtín	k1gMnSc1	Semtín
<g/>
,	,	kIx,	,
vězeň	vězeň	k1gMnSc1	vězeň
koncentračních	koncentrační	k2eAgMnPc2d1	koncentrační
táborů	tábor	k1gInPc2	tábor
Dachau	Dachaus	k1gInSc2	Dachaus
a	a	k8xC	a
Buchenwald	Buchenwald	k1gInSc1	Buchenwald
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
Synthesia	Synthesius	k1gMnSc2	Synthesius
Semtín	Semtín	k1gMnSc1	Semtín
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politický	politický	k2eAgMnSc1d1	politický
vězeň	vězeň	k1gMnSc1	vězeň
(	(	kIx(	(
<g/>
Pankrác	Pankrác	k1gMnSc1	Pankrác
<g/>
,	,	kIx,	,
Mírov	Mírov	k1gInSc1	Mírov
1950	[number]	k4	1950
<g/>
–	–	k?	–
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
-	-	kIx~	-
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
,	,	kIx,	,
studoval	studovat	k5eAaImAgMnS	studovat
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
fakultu	fakulta	k1gFnSc4	fakulta
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1955	[number]	k4	1955
až	až	k9	až
1957	[number]	k4	1957
-	-	kIx~	-
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
esejista	esejista	k1gMnSc1	esejista
<g/>
,	,	kIx,	,
disident	disident	k1gMnSc1	disident
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
mluvčí	mluvčí	k1gMnSc1	mluvčí
Charty	charta	k1gFnSc2	charta
77	[number]	k4	77
<g/>
,	,	kIx,	,
kritik	kritik	k1gMnSc1	kritik
i	i	k8xC	i
vězeň	vězeň	k1gMnSc1	vězeň
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
poslední	poslední	k2eAgMnSc1d1	poslední
prezident	prezident	k1gMnSc1	prezident
Československa	Československo	k1gNnSc2	Československo
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
a	a	k8xC	a
první	první	k4xOgMnSc1	první
prezident	prezident	k1gMnSc1	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
-	-	kIx~	-
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
VELFLÍK	VELFLÍK	kA	VELFLÍK
<g/>
,	,	kIx,	,
Albert	Albert	k1gMnSc1	Albert
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
technického	technický	k2eAgNnSc2d1	technické
učení	učení	k1gNnSc2	učení
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
s	s	k7c7	s
dějinným	dějinný	k2eAgInSc7d1	dějinný
přehledem	přehled	k1gInSc7	přehled
nejstarších	starý	k2eAgFnPc2d3	nejstarší
inženýrských	inženýrský	k2eAgFnPc2d1	inženýrská
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
akademií	akademie	k1gFnPc2	akademie
a	a	k8xC	a
ústavů	ústav	k1gInPc2	ústav
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgNnPc6	jenž
bylo	být	k5eAaImAgNnS	být
vědám	věda	k1gFnPc3	věda
inženýrským	inženýrský	k2eAgFnPc3d1	inženýrská
nejdříve	dříve	k6eAd3	dříve
vyučováno	vyučován	k2eAgNnSc1d1	vyučováno
:	:	kIx,	:
Pamětní	pamětní	k2eAgInSc4d1	pamětní
spis	spis	k1gInSc4	spis
na	na	k7c4	na
oslavu	oslava	k1gFnSc4	oslava
založení	založení	k1gNnSc2	založení
inženýrské	inženýrský	k2eAgFnSc2d1	inženýrská
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
před	před	k7c7	před
200	[number]	k4	200
lety	léto	k1gNnPc7	léto
a	a	k8xC	a
stoletého	stoletý	k2eAgNnSc2d1	stoleté
trvání	trvání	k1gNnSc2	trvání
polytechnického	polytechnický	k2eAgInSc2d1	polytechnický
ústavu	ústav	k1gInSc2	ústav
Pražského	pražský	k2eAgInSc2d1	pražský
;	;	kIx,	;
Díl	díl	k1gInSc4	díl
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
prvního	první	k4xOgNnSc2	první
vyučování	vyučování	k1gNnSc2	vyučování
inženýrským	inženýrský	k2eAgFnPc3d1	inženýrská
vědám	věda	k1gFnPc3	věda
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
1686	[number]	k4	1686
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
zakončení	zakončení	k1gNnSc2	zakončení
utraquistického	utraquistický	k2eAgNnSc2d1	utraquistické
období	období	k1gNnSc2	období
polytechnického	polytechnický	k2eAgInSc2d1	polytechnický
ústavu	ústav	k1gInSc2	ústav
v	v	k7c6	v
Království	království	k1gNnSc6	království
českém	český	k2eAgNnSc6d1	české
roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
technická	technický	k2eAgFnSc1d1	technická
<g/>
,	,	kIx,	,
1906	[number]	k4	1906
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VELFLÍK	VELFLÍK	kA	VELFLÍK
<g/>
,	,	kIx,	,
Albert	Albert	k1gMnSc1	Albert
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
<g/>
;	;	kIx,	;
KOLÁŘ	Kolář	k1gMnSc1	Kolář
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
technického	technický	k2eAgNnSc2d1	technické
učení	učení	k1gNnSc2	učení
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
s	s	k7c7	s
dějinným	dějinný	k2eAgInSc7d1	dějinný
přehledem	přehled	k1gInSc7	přehled
nejstarších	starý	k2eAgFnPc2d3	nejstarší
inženýrských	inženýrský	k2eAgFnPc2d1	inženýrská
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
akademií	akademie	k1gFnPc2	akademie
a	a	k8xC	a
ústavů	ústav	k1gInPc2	ústav
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgNnPc6	jenž
bylo	být	k5eAaImAgNnS	být
vědám	věda	k1gFnPc3	věda
inženýrským	inženýrský	k2eAgFnPc3d1	inženýrská
nejdříve	dříve	k6eAd3	dříve
vyučováno	vyučován	k2eAgNnSc1d1	vyučováno
:	:	kIx,	:
Pamětní	pamětní	k2eAgInSc4d1	pamětní
spis	spis	k1gInSc4	spis
na	na	k7c4	na
oslavu	oslava	k1gFnSc4	oslava
založení	založení	k1gNnSc2	založení
stavovské	stavovský	k2eAgFnSc2d1	stavovská
inženýrské	inženýrský	k2eAgFnSc2d1	inženýrská
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
před	před	k7c7	před
200	[number]	k4	200
lety	léto	k1gNnPc7	léto
a	a	k8xC	a
stoletého	stoletý	k2eAgNnSc2d1	stoleté
trvání	trvání	k1gNnSc2	trvání
polytechnického	polytechnický	k2eAgInSc2d1	polytechnický
ústavu	ústav	k1gInSc2	ústav
Pražského	pražský	k2eAgInSc2d1	pražský
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
matice	matice	k1gFnSc1	matice
technická	technický	k2eAgFnSc1d1	technická
<g/>
,	,	kIx,	,
1925	[number]	k4	1925
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Reprezentační	reprezentační	k2eAgFnSc1d1	reprezentační
brožura	brožura	k1gFnSc1	brožura
ČVUT	ČVUT	kA	ČVUT
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
České	český	k2eAgFnSc2d1	Česká
vysoké	vysoká	k1gFnSc2	vysoká
učení	učení	k1gNnSc1	učení
technické	technický	k2eAgNnSc1d1	technické
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Reskript	reskript	k1gInSc1	reskript
císaře	císař	k1gMnSc2	císař
Josefa	Josef	k1gMnSc2	Josef
I.	I.	kA	I.
českým	český	k2eAgMnPc3d1	český
stavům	stav	k1gInPc3	stav
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
ČVUT	ČVUT	kA	ČVUT
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
www.cvut.cz	www.cvut.cz	k1gInSc1	www.cvut.cz
Domácí	domácí	k2eAgFnSc2d1	domácí
stránky	stránka	k1gFnSc2	stránka
ČVUT	ČVUT	kA	ČVUT
</s>
</p>
<p>
<s>
Ústřední	ústřední	k2eAgFnSc1d1	ústřední
knihovna	knihovna	k1gFnSc1	knihovna
ČVUT	ČVUT	kA	ČVUT
</s>
</p>
<p>
<s>
Správa	správa	k1gFnSc1	správa
účelových	účelový	k2eAgNnPc2d1	účelové
zařízení	zařízení	k1gNnPc2	zařízení
-	-	kIx~	-
koleje	kolej	k1gFnPc1	kolej
<g/>
,	,	kIx,	,
menzy	menza	k1gFnPc1	menza
-	-	kIx~	-
www.suz.cvut.cz	www.suz.cvut.cz	k1gInSc1	www.suz.cvut.cz
</s>
</p>
<p>
<s>
Stručná	stručný	k2eAgFnSc1d1	stručná
historie	historie	k1gFnSc1	historie
ČVUT	ČVUT	kA	ČVUT
v	v	k7c6	v
datech	datum	k1gNnPc6	datum
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
univerzity	univerzita	k1gFnSc2	univerzita
</s>
</p>
<p>
<s>
Milada	Milada	k1gFnSc1	Milada
Sekyrková	sekyrkový	k2eAgFnSc1d1	Sekyrková
<g/>
:	:	kIx,	:
Rozdělení	rozdělení	k1gNnSc1	rozdělení
polytechniky	polytechnika	k1gFnSc2	polytechnika
–	–	k?	–
precedens	precedens	k1gNnSc1	precedens
pro	pro	k7c4	pro
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
?	?	kIx.	?
</s>
<s>
in	in	k?	in
Práce	práce	k1gFnSc1	práce
z	z	k7c2	z
dějin	dějiny	k1gFnPc2	dějiny
vědy	věda	k1gFnSc2	věda
<g/>
,	,	kIx,	,
svazek	svazek	k1gInSc4	svazek
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
Výzkumné	výzkumný	k2eAgNnSc1d1	výzkumné
centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
dějiny	dějiny	k1gFnPc4	dějiny
vědy	věda	k1gFnSc2	věda
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7285-027-X	[number]	k4	80-7285-027-X
</s>
</p>
<p>
<s>
Univerzitní	univerzitní	k2eAgNnSc1d1	univerzitní
centrum	centrum	k1gNnSc1	centrum
energeticky	energeticky	k6eAd1	energeticky
efektivních	efektivní	k2eAgFnPc2d1	efektivní
budov	budova	k1gFnPc2	budova
</s>
</p>
<p>
<s>
QS	QS	kA	QS
World	World	k1gInSc4	World
University	universita	k1gFnSc2	universita
RankingsNejprestižnější	RankingsNejprestižný	k2eAgInSc4d2	RankingsNejprestižný
žebříček	žebříček	k1gInSc4	žebříček
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
</s>
</p>
<p>
<s>
www.StudyAtCTU.com	www.StudyAtCTU.com	k1gInSc1	www.StudyAtCTU.com
Portál	portál	k1gInSc1	portál
pro	pro	k7c4	pro
zahraniční	zahraniční	k2eAgMnPc4d1	zahraniční
studenty	student	k1gMnPc4	student
-	-	kIx~	-
konkrétně	konkrétně	k6eAd1	konkrétně
zaměřený	zaměřený	k2eAgInSc1d1	zaměřený
na	na	k7c6	na
ČVUT	ČVUT	kA	ČVUT
</s>
</p>
<p>
<s>
www.StudyInPrague.com	www.StudyInPrague.com	k1gInSc1	www.StudyInPrague.com
Portál	portál	k1gInSc1	portál
pro	pro	k7c4	pro
zahraniční	zahraniční	k2eAgMnPc4d1	zahraniční
studenty	student	k1gMnPc4	student
</s>
</p>
<p>
<s>
Studentská	studentský	k2eAgFnSc1d1	studentská
unie	unie	k1gFnSc1	unie
ČVUT	ČVUT	kA	ČVUT
</s>
</p>
<p>
<s>
Portál	portál	k1gInSc1	portál
Student	student	k1gMnSc1	student
<g/>
.	.	kIx.	.
<g/>
cvut	cvut	k1gMnSc1	cvut
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
International	Internationat	k5eAaImAgMnS	Internationat
Student	student	k1gMnSc1	student
Club	club	k1gInSc4	club
</s>
</p>
<p>
<s>
IAESTE	IAESTE	kA	IAESTE
</s>
</p>
<p>
<s>
Studuj	studovat	k5eAaImRp2nS	studovat
na	na	k7c4	na
CVUT	CVUT	kA	CVUT
</s>
</p>
<p>
<s>
Studentské	studentský	k2eAgFnPc1d1	studentská
formule	formule	k1gFnPc1	formule
-	-	kIx~	-
CTU	CTU	kA	CTU
CarTech	CarTech	k1gInSc1	CarTech
</s>
</p>
