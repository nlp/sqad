<s desamb="1">
Stejnojmenná	stejnojmenný	k2eAgFnSc1d1
fyzikální	fyzikální	k2eAgFnSc1d1
veličina	veličina	k1gFnSc1
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
označovaná	označovaný	k2eAgFnSc1d1
I	i	k9
<g/>
,	,	kIx,
a	a	k8xC
její	její	k3xOp3gFnSc1
jednotka	jednotka	k1gFnSc1
je	být	k5eAaImIp3nS
ampér	ampér	k1gInSc1
(	(	kIx(
<g/>
A	A	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vyjadřuje	vyjadřovat	k5eAaImIp3nS
množství	množství	k1gNnSc1
elektrického	elektrický	k2eAgInSc2d1
náboje	náboj	k1gInSc2
prošlého	prošlý	k2eAgMnSc4d1
za	za	k7c4
jednotku	jednotka	k1gFnSc4
času	čas	k1gInSc2
daným	daný	k2eAgInSc7d1
průřezem	průřez	k1gInSc7
<g/>
.	.	kIx.
</s>