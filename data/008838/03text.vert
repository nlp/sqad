<p>
<s>
Palladium	palladium	k1gNnSc1	palladium
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Pd	Pd	k1gFnSc2	Pd
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Palladium	palladium	k1gNnSc1	palladium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
drahý	drahý	k2eAgInSc1d1	drahý
kov	kov	k1gInSc1	kov
šedivě	šedivě	k6eAd1	šedivě
bílé	bílý	k2eAgFnSc2d1	bílá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
drahých	drahý	k2eAgInPc2d1	drahý
kovů	kov	k1gInPc2	kov
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
největší	veliký	k2eAgFnSc7d3	veliký
reaktivitou	reaktivita	k1gFnSc7	reaktivita
<g/>
.	.	kIx.	.
</s>
<s>
Nalézá	nalézat	k5eAaImIp3nS	nalézat
uplatnění	uplatnění	k1gNnSc4	uplatnění
především	především	k9	především
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
katalyzátorů	katalyzátor	k1gInPc2	katalyzátor
a	a	k8xC	a
jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
slitin	slitina	k1gFnPc2	slitina
pro	pro	k7c4	pro
dentální	dentální	k2eAgNnSc4d1	dentální
a	a	k8xC	a
šperkařské	šperkařský	k2eAgNnSc4d1	šperkařské
využití	využití	k1gNnSc4	využití
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgFnPc1d1	základní
fyzikálně-chemické	fyzikálněhemický	k2eAgFnPc1d1	fyzikálně-chemická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Palladium	palladium	k1gNnSc1	palladium
izoloval	izolovat	k5eAaBmAgInS	izolovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1803	[number]	k4	1803
anglický	anglický	k2eAgMnSc1d1	anglický
chemik	chemik	k1gMnSc1	chemik
William	William	k1gInSc4	William
Hyde	Hyd	k1gInSc2	Hyd
Wollaston	Wollaston	k1gInSc1	Wollaston
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1804	[number]	k4	1804
jej	on	k3xPp3gMnSc4	on
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
podle	podle	k7c2	podle
planetky	planetka	k1gFnSc2	planetka
Pallas	Pallas	k1gMnSc1	Pallas
objevené	objevený	k2eAgFnSc2d1	objevená
roku	rok	k1gInSc2	rok
1802	[number]	k4	1802
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
ušlechtilý	ušlechtilý	k2eAgInSc4d1	ušlechtilý
<g/>
,	,	kIx,	,
odolný	odolný	k2eAgInSc4d1	odolný
<g/>
,	,	kIx,	,
kujný	kujný	k2eAgInSc4d1	kujný
a	a	k8xC	a
tažný	tažný	k2eAgInSc4d1	tažný
kov	kov	k1gInSc4	kov
<g/>
,	,	kIx,	,
elektricky	elektricky	k6eAd1	elektricky
i	i	k9	i
tepelně	tepelně	k6eAd1	tepelně
středně	středně	k6eAd1	středně
dobře	dobře	k6eAd1	dobře
vodivý	vodivý	k2eAgInSc1d1	vodivý
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
rhodiem	rhodium	k1gNnSc7	rhodium
a	a	k8xC	a
rutheniem	ruthenium	k1gNnSc7	ruthenium
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
triády	triáda	k1gFnSc2	triáda
lehkých	lehký	k2eAgInPc2d1	lehký
platinových	platinový	k2eAgInPc2d1	platinový
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
zejména	zejména	k9	zejména
ryzí	ryzí	k2eAgNnSc1d1	ryzí
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
ve	v	k7c6	v
směsi	směs	k1gFnSc6	směs
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
drahými	drahý	k2eAgInPc7d1	drahý
kovy	kov	k1gInPc7	kov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Odhad	odhad	k1gInSc1	odhad
průměrného	průměrný	k2eAgInSc2d1	průměrný
obsahu	obsah	k1gInSc2	obsah
palladia	palladion	k1gNnSc2	palladion
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
činí	činit	k5eAaImIp3nS	činit
přibližně	přibližně	k6eAd1	přibližně
0,0075	[number]	k4	0,0075
<g/>
–	–	k?	–
<g/>
0,01	[number]	k4	0,01
ppm	ppm	k?	ppm
(	(	kIx(	(
<g/>
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Údaje	údaj	k1gInPc1	údaj
o	o	k7c4	o
koncentraci	koncentrace	k1gFnSc4	koncentrace
v	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
nejsou	být	k5eNaImIp3nP	být
dostupné	dostupný	k2eAgFnPc1d1	dostupná
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tato	tento	k3xDgFnSc1	tento
hodnota	hodnota	k1gFnSc1	hodnota
leží	ležet	k5eAaImIp3nS	ležet
pod	pod	k7c7	pod
mezí	mez	k1gFnSc7	mez
detekce	detekce	k1gFnSc2	detekce
i	i	k8xC	i
těch	ten	k3xDgFnPc2	ten
nejcitlivějších	citlivý	k2eAgFnPc2d3	nejcitlivější
analytických	analytický	k2eAgFnPc2d1	analytická
technik	technika	k1gFnPc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládaný	předpokládaný	k2eAgInSc1d1	předpokládaný
výskyt	výskyt	k1gInSc1	výskyt
palladia	palladion	k1gNnSc2	palladion
v	v	k7c6	v
okolním	okolní	k2eAgInSc6d1	okolní
vesmíru	vesmír	k1gInSc6	vesmír
je	být	k5eAaImIp3nS	být
uváděn	uvádět	k5eAaImNgInS	uvádět
přibližně	přibližně	k6eAd1	přibližně
jako	jako	k8xS	jako
1	[number]	k4	1
atom	atom	k1gInSc1	atom
Pd	Pd	k1gFnSc2	Pd
na	na	k7c4	na
30	[number]	k4	30
miliard	miliarda	k4xCgFnPc2	miliarda
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
v	v	k7c6	v
lučavce	lučavka	k1gFnSc6	lučavka
královské	královský	k2eAgFnSc6d1	královská
i	i	k8xC	i
koncentrované	koncentrovaný	k2eAgFnSc6d1	koncentrovaná
kyselině	kyselina	k1gFnSc6	kyselina
dusičné	dusičný	k2eAgFnSc6d1	dusičná
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavá	zajímavý	k2eAgFnSc1d1	zajímavá
je	být	k5eAaImIp3nS	být
schopnost	schopnost	k1gFnSc1	schopnost
palladia	palladion	k1gNnSc2	palladion
pohlcovat	pohlcovat	k5eAaImF	pohlcovat
značné	značný	k2eAgInPc4d1	značný
objemy	objem	k1gInPc4	objem
plynného	plynný	k2eAgInSc2d1	plynný
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Palladium	palladium	k1gNnSc1	palladium
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
také	také	k9	také
dobré	dobrý	k2eAgFnPc4d1	dobrá
katalytické	katalytický	k2eAgFnPc4d1	katalytická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
jako	jako	k9	jako
kovové	kovový	k2eAgInPc1d1	kovový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Zásadního	zásadní	k2eAgNnSc2d1	zásadní
využití	využití	k1gNnSc2	využití
doznává	doznávat	k5eAaImIp3nS	doznávat
palladium	palladium	k1gNnSc4	palladium
v	v	k7c6	v
chemickém	chemický	k2eAgInSc6d1	chemický
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nejrůznějších	různý	k2eAgFnPc6d3	nejrůznější
podobách	podoba	k1gFnPc6	podoba
používáno	používán	k2eAgNnSc1d1	používáno
jako	jako	k8xS	jako
velmi	velmi	k6eAd1	velmi
účinný	účinný	k2eAgInSc1d1	účinný
katalyzátor	katalyzátor	k1gInSc1	katalyzátor
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
organických	organický	k2eAgFnPc2d1	organická
syntéz	syntéza	k1gFnPc2	syntéza
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
při	při	k7c6	při
hydrogenacích	hydrogenace	k1gFnPc6	hydrogenace
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
molekuly	molekula	k1gFnSc2	molekula
zaváděn	zaváděn	k2eAgInSc4d1	zaváděn
atom	atom	k1gInSc4	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
platinou	platina	k1gFnSc7	platina
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
i	i	k9	i
v	v	k7c6	v
katalyzátorech	katalyzátor	k1gInPc6	katalyzátor
výfukových	výfukový	k2eAgInPc2d1	výfukový
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
odstranění	odstranění	k1gNnSc3	odstranění
nežádoucích	žádoucí	k2eNgFnPc2d1	nežádoucí
látek	látka	k1gFnPc2	látka
jako	jako	k8xS	jako
nespálených	spálený	k2eNgInPc2d1	nespálený
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
a	a	k8xC	a
oxidu	oxid	k1gInSc2	oxid
uhelnatého	uhelnatý	k2eAgMnSc2d1	uhelnatý
z	z	k7c2	z
výfukových	výfukový	k2eAgInPc2d1	výfukový
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
omezené	omezený	k2eAgFnSc6d1	omezená
míře	míra	k1gFnSc6	míra
se	se	k3xPyFc4	se
palladium	palladium	k1gNnSc1	palladium
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
slitin	slitina	k1gFnPc2	slitina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
šperků	šperk	k1gInPc2	šperk
<g/>
,	,	kIx,	,
především	především	k9	především
tzv.	tzv.	kA	tzv.
bílého	bílý	k2eAgNnSc2d1	bílé
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
náhrada	náhrada	k1gFnSc1	náhrada
levnějšího	levný	k2eAgNnSc2d2	levnější
avšak	avšak	k8xC	avšak
potenciálně	potenciálně	k6eAd1	potenciálně
toxického	toxický	k2eAgInSc2d1	toxický
niklu	nikl	k1gInSc2	nikl
<g/>
.	.	kIx.	.
</s>
<s>
Poměrně	poměrně	k6eAd1	poměrně
rozšířené	rozšířený	k2eAgFnPc1d1	rozšířená
jsou	být	k5eAaImIp3nP	být
dentální	dentální	k2eAgFnPc1d1	dentální
slitiny	slitina	k1gFnPc1	slitina
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
palladia	palladion	k1gNnSc2	palladion
a	a	k8xC	a
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
náhrada	náhrada	k1gFnSc1	náhrada
dražších	drahý	k2eAgFnPc2d2	dražší
slitin	slitina	k1gFnPc2	slitina
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
zlata	zlato	k1gNnSc2	zlato
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Palladium	palladium	k1gNnSc1	palladium
se	se	k3xPyFc4	se
také	také	k9	také
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
ne	ne	k9	ne
tak	tak	k9	tak
často	často	k6eAd1	často
<g/>
)	)	kIx)	)
obchoduje	obchodovat	k5eAaImIp3nS	obchodovat
jako	jako	k9	jako
investiční	investiční	k2eAgInSc1d1	investiční
kov	kov	k1gInSc1	kov
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
zlato	zlato	k1gNnSc4	zlato
nebo	nebo	k8xC	nebo
stříbro	stříbro	k1gNnSc4	stříbro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mineralogie	mineralogie	k1gFnSc2	mineralogie
==	==	k?	==
</s>
</p>
<p>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
ryzí	ryzí	k2eAgMnSc1d1	ryzí
a	a	k8xC	a
bývá	bývat	k5eAaImIp3nS	bývat
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
procentu	procent	k1gInSc6	procent
vždy	vždy	k6eAd1	vždy
přítomno	přítomen	k2eAgNnSc1d1	přítomno
v	v	k7c6	v
horninách	hornina	k1gFnPc6	hornina
obsahujících	obsahující	k2eAgFnPc6d1	obsahující
platinu	platina	k1gFnSc4	platina
a	a	k8xC	a
rhodium	rhodium	k1gNnSc4	rhodium
<g/>
.	.	kIx.	.
</s>
<s>
Suverénně	suverénně	k6eAd1	suverénně
největším	veliký	k2eAgMnSc7d3	veliký
dodavatelem	dodavatel	k1gMnSc7	dodavatel
palladia	palladion	k1gNnSc2	palladion
na	na	k7c4	na
světový	světový	k2eAgInSc4d1	světový
trh	trh	k1gInSc4	trh
je	být	k5eAaImIp3nS	být
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
těží	těžet	k5eAaImIp3nS	těžet
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
44	[number]	k4	44
%	%	kIx~	%
světové	světový	k2eAgFnSc2d1	světová
produkce	produkce	k1gFnSc2	produkce
palladia	palladion	k1gNnSc2	palladion
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
následuje	následovat	k5eAaImIp3nS	následovat
Jižní	jižní	k2eAgFnSc1d1	jižní
Afrika	Afrika	k1gFnSc1	Afrika
se	s	k7c7	s
40	[number]	k4	40
%	%	kIx~	%
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
s	s	k7c7	s
6	[number]	k4	6
%	%	kIx~	%
a	a	k8xC	a
USA	USA	kA	USA
s	s	k7c7	s
5	[number]	k4	5
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jediné	jediný	k2eAgMnPc4d1	jediný
velké	velký	k2eAgMnPc4d1	velký
producenty	producent	k1gMnPc4	producent
tohoto	tento	k3xDgInSc2	tento
prvkuPoměrně	prvkuPoměrně	k6eAd1	prvkuPoměrně
často	často	k6eAd1	často
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
palladium	palladium	k1gNnSc4	palladium
v	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
procentu	procento	k1gNnSc6	procento
rudy	ruda	k1gFnSc2	ruda
niklu	nikl	k1gInSc2	nikl
a	a	k8xC	a
mědi	měď	k1gFnSc2	měď
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
metalurgickém	metalurgický	k2eAgNnSc6d1	metalurgické
zpracování	zpracování	k1gNnSc6	zpracování
těchto	tento	k3xDgFnPc2	tento
rud	ruda	k1gFnPc2	ruda
také	také	k6eAd1	také
získáváno	získáván	k2eAgNnSc1d1	získáváno
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc4	několik
ložisek	ložisko	k1gNnPc2	ložisko
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
je	být	k5eAaImIp3nS	být
vytěžováno	vytěžován	k2eAgNnSc1d1	vytěžováno
především	především	k9	především
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Platidlo	platidlo	k1gNnSc1	platidlo
===	===	k?	===
</s>
</p>
<p>
<s>
Palladium	palladium	k1gNnSc1	palladium
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
drahým	drahý	k2eAgInSc7d1	drahý
kovem	kov	k1gInSc7	kov
<g/>
,	,	kIx,	,
kujným	kujný	k2eAgMnSc7d1	kujný
<g/>
,	,	kIx,	,
vhodným	vhodný	k2eAgNnSc7d1	vhodné
pro	pro	k7c4	pro
ražbu	ražba	k1gFnSc4	ražba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
ražbu	ražba	k1gFnSc4	ražba
mincí	mince	k1gFnPc2	mince
nebyl	být	k5eNaImAgMnS	být
a	a	k8xC	a
ani	ani	k8xC	ani
není	být	k5eNaImIp3nS	být
moc	moc	k6eAd1	moc
oblíbený	oblíbený	k2eAgInSc1d1	oblíbený
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
spíše	spíše	k9	spíše
šedou	šedý	k2eAgFnSc4d1	šedá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
platinové	platinový	k2eAgFnPc1d1	platinová
a	a	k8xC	a
palladiové	palladiový	k2eAgFnPc1d1	palladiový
mince	mince	k1gFnPc1	mince
spíše	spíše	k9	spíše
jako	jako	k9	jako
sběratelské	sběratelský	k2eAgNnSc1d1	sběratelské
<g/>
,	,	kIx,	,
či	či	k8xC	či
investiční	investiční	k2eAgFnSc1d1	investiční
mince	mince	k1gFnSc1	mince
<g/>
.	.	kIx.	.
</s>
<s>
Mince	mince	k1gFnSc1	mince
z	z	k7c2	z
palladia	palladion	k1gNnSc2	palladion
se	se	k3xPyFc4	se
razí	razit	k5eAaImIp3nS	razit
spíše	spíše	k9	spíše
v	v	k7c6	v
malých	malý	k2eAgInPc6d1	malý
rozměrech	rozměr	k1gInPc6	rozměr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
XPD	XPD	kA	XPD
je	být	k5eAaImIp3nS	být
kód	kód	k1gInSc4	kód
1	[number]	k4	1
trojské	trojský	k2eAgFnPc4d1	Trojská
unce	unce	k1gFnPc4	unce
palladia	palladion	k1gNnSc2	palladion
jako	jako	k8xC	jako
platidla	platidlo	k1gNnSc2	platidlo
podle	podle	k7c2	podle
standardu	standard	k1gInSc2	standard
ISO	ISO	kA	ISO
4217	[number]	k4	4217
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Drahé	drahý	k2eAgInPc1d1	drahý
kovy	kov	k1gInPc1	kov
</s>
</p>
<p>
<s>
Platina	platina	k1gFnSc1	platina
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
<g/>
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
</s>
</p>
<p>
<s>
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
<g/>
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
</s>
</p>
<p>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc4	vydání
1961	[number]	k4	1961
</s>
</p>
<p>
<s>
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwood	k1gInSc1	Greenwood
–	–	k?	–
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc2	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
palladium	palladium	k1gNnSc4	palladium
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
palladium	palladium	k1gNnSc4	palladium
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
