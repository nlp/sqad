<s>
Z	z	k7c2	z
výše	výše	k1gFnSc2	výše
uvedeného	uvedený	k2eAgNnSc2d1	uvedené
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
unschooling	unschooling	k1gInSc4	unschooling
legálně	legálně	k6eAd1	legálně
provozovat	provozovat	k5eAaImF	provozovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
dítě	dítě	k1gNnSc1	dítě
nemá	mít	k5eNaImIp3nS	mít
svobodu	svoboda	k1gFnSc4	svoboda
volit	volit	k5eAaImF	volit
si	se	k3xPyFc3	se
co	co	k9	co
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
<g/>
,	,	kIx,	,
s	s	k7c7	s
kým	kdo	k3yInSc7	kdo
a	a	k8xC	a
jakým	jaký	k3yQgInSc7	jaký
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
učit	učit	k5eAaImF	učit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
musí	muset	k5eAaImIp3nS	muset
plnit	plnit	k5eAaImF	plnit
požadavky	požadavek	k1gInPc4	požadavek
rámcového	rámcový	k2eAgInSc2d1	rámcový
vzdělávacího	vzdělávací	k2eAgInSc2d1	vzdělávací
programu	program	k1gInSc2	program
MŠMT	MŠMT	kA	MŠMT
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
potažmo	potažmo	k6eAd1	potažmo
školního	školní	k2eAgInSc2d1	školní
vzdělávacího	vzdělávací	k2eAgInSc2d1	vzdělávací
programu	program	k1gInSc2	program
konkrétní	konkrétní	k2eAgFnSc2d1	konkrétní
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
