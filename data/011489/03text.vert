<p>
<s>
Irská	irský	k2eAgFnSc1d1	irská
republikánská	republikánský	k2eAgFnSc1d1	republikánská
armáda	armáda	k1gFnSc1	armáda
(	(	kIx(	(
<g/>
IRA	Ir	k1gMnSc2	Ir
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
irsky	irsky	k6eAd1	irsky
<g/>
:	:	kIx,	:
Óglaigh	Óglaigh	k1gInSc1	Óglaigh
na	na	k7c4	na
hÉireann	hÉireann	k1gInSc4	hÉireann
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
irská	irský	k2eAgFnSc1d1	irská
republikánská	republikánský	k2eAgFnSc1d1	republikánská
vojenská	vojenský	k2eAgFnSc1d1	vojenská
organizace	organizace	k1gFnSc1	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInPc1	její
kořeny	kořen	k1gInPc1	kořen
sahají	sahat	k5eAaImIp3nP	sahat
k	k	k7c3	k
organizaci	organizace	k1gFnSc4	organizace
Irští	irský	k2eAgMnPc1d1	irský
dobrovolníci	dobrovolník	k1gMnPc1	dobrovolník
<g/>
,	,	kIx,	,
založené	založený	k2eAgFnSc2d1	založená
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
na	na	k7c6	na
svědomí	svědomí	k1gNnSc6	svědomí
Velikonoční	velikonoční	k2eAgNnPc1d1	velikonoční
povstání	povstání	k1gNnPc1	povstání
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1916	[number]	k4	1916
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
byla	být	k5eAaImAgFnS	být
zvoleným	zvolený	k2eAgNnSc7d1	zvolené
shromážděním	shromáždění	k1gNnSc7	shromáždění
(	(	kIx(	(
<g/>
Dáil	Dáil	k1gMnSc1	Dáil
Éireann	Éireann	k1gMnSc1	Éireann
<g/>
)	)	kIx)	)
během	během	k7c2	během
povstání	povstání	k1gNnSc2	povstání
formálně	formálně	k6eAd1	formálně
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
Irská	irský	k2eAgFnSc1d1	irská
republika	republika	k1gFnSc1	republika
a	a	k8xC	a
Irští	irský	k2eAgMnPc1d1	irský
dobrovolníci	dobrovolník	k1gMnPc1	dobrovolník
její	její	k3xOp3gNnSc4	její
legitimní	legitimní	k2eAgNnSc4d1	legitimní
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
</s>
<s>
IRA	Ir	k1gMnSc4	Ir
pak	pak	k6eAd1	pak
vedla	vést	k5eAaImAgFnS	vést
partyzánský	partyzánský	k2eAgInSc4d1	partyzánský
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
britské	britský	k2eAgFnSc3d1	britská
nadvládě	nadvláda	k1gFnSc3	nadvláda
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
během	během	k7c2	během
irské	irský	k2eAgFnSc2d1	irská
války	válka	k1gFnSc2	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
–	–	k?	–
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podepsání	podepsání	k1gNnSc1	podepsání
anglo-irské	anglorský	k2eAgFnSc2d1	anglo-irská
dohody	dohoda	k1gFnSc2	dohoda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ukončila	ukončit	k5eAaPmAgFnS	ukončit
válku	válka	k1gFnSc4	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
,	,	kIx,	,
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
uvnitř	uvnitř	k7c2	uvnitř
IRA	Ir	k1gMnSc2	Ir
rozkol	rozkol	k1gInSc4	rozkol
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
s	s	k7c7	s
dohodou	dohoda	k1gFnSc7	dohoda
souhlasili	souhlasit	k5eAaImAgMnP	souhlasit
<g/>
,	,	kIx,	,
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Michaela	Michael	k1gMnSc2	Michael
Collinse	Collinse	k1gFnSc2	Collinse
jádro	jádro	k1gNnSc4	jádro
Irské	irský	k2eAgFnSc2d1	irská
národní	národní	k2eAgFnSc2d1	národní
armády	armáda	k1gFnSc2	armáda
(	(	kIx(	(
<g/>
INA	INA	kA	INA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
IRA	Ir	k1gMnSc2	Ir
však	však	k9	však
byla	být	k5eAaImAgFnS	být
proti	proti	k7c3	proti
dohodě	dohoda	k1gFnSc3	dohoda
a	a	k8xC	a
chtěla	chtít	k5eAaImAgFnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nezávislost	nezávislost	k1gFnSc4	nezávislost
získal	získat	k5eAaPmAgMnS	získat
celý	celý	k2eAgInSc4d1	celý
ostrov	ostrov	k1gInSc4	ostrov
<g/>
,	,	kIx,	,
ne	ne	k9	ne
jen	jen	k9	jen
jeho	jeho	k3xOp3gFnSc4	jeho
část	část	k1gFnSc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
názorové	názorový	k2eAgFnPc1d1	názorová
neshody	neshoda	k1gFnPc1	neshoda
vyústily	vyústit	k5eAaPmAgFnP	vyústit
v	v	k7c4	v
občanskou	občanský	k2eAgFnSc4d1	občanská
válku	válka	k1gFnSc4	válka
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
–	–	k?	–
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
nakonec	nakonec	k6eAd1	nakonec
skončila	skončit	k5eAaPmAgFnS	skončit
pro	pro	k7c4	pro
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
proti	proti	k7c3	proti
dohodě	dohoda	k1gFnSc3	dohoda
(	(	kIx(	(
<g/>
anti-treaty	antireata	k1gFnSc2	anti-treata
IRA	Ir	k1gMnSc2	Ir
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neúspěchem	neúspěch	k1gInSc7	neúspěch
–	–	k?	–
ostrov	ostrov	k1gInSc1	ostrov
zůstal	zůstat	k5eAaPmAgInS	zůstat
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
svůj	svůj	k3xOyFgInSc4	svůj
boj	boj	k1gInSc4	boj
nevzdali	vzdát	k5eNaPmAgMnP	vzdát
a	a	k8xC	a
dál	daleko	k6eAd2	daleko
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
násilné	násilný	k2eAgFnSc6d1	násilná
kampani	kampaň	k1gFnSc6	kampaň
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
usilovali	usilovat	k5eAaImAgMnP	usilovat
o	o	k7c4	o
sjednocení	sjednocení	k1gNnSc4	sjednocení
celé	celý	k2eAgFnSc2d1	celá
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Počátky	počátek	k1gInPc4	počátek
==	==	k?	==
</s>
</p>
<p>
<s>
Irská	irský	k2eAgFnSc1d1	irská
inklinace	inklinace	k1gFnSc1	inklinace
k	k	k7c3	k
ideám	idea	k1gFnPc3	idea
republiky	republika	k1gFnSc2	republika
měla	mít	k5eAaImAgFnS	mít
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
historii	historie	k1gFnSc4	historie
–	–	k?	–
od	od	k7c2	od
organizace	organizace	k1gFnSc2	organizace
United	United	k1gInSc1	United
Irishmen	Irishmen	k2eAgInSc1d1	Irishmen
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
stála	stát	k5eAaImAgFnS	stát
za	za	k7c7	za
irským	irský	k2eAgNnSc7d1	irské
povstáním	povstání	k1gNnSc7	povstání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1798	[number]	k4	1798
a	a	k8xC	a
1803	[number]	k4	1803
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
"	"	kIx"	"
<g/>
povstání	povstání	k1gNnSc4	povstání
mladých	mladý	k2eAgMnPc2d1	mladý
Irů	Ir	k1gMnPc2	Ir
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Young	Young	k1gMnSc1	Young
Irelander	Irelander	k1gMnSc1	Irelander
Rebellion	Rebellion	k?	Rebellion
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
až	až	k6eAd1	až
po	po	k7c6	po
povstání	povstání	k1gNnSc6	povstání
pod	pod	k7c7	pod
taktovkou	taktovka	k1gFnSc7	taktovka
Irského	irský	k2eAgNnSc2d1	irské
republikánského	republikánský	k2eAgNnSc2d1	republikánské
bratrstva	bratrstvo	k1gNnSc2	bratrstvo
(	(	kIx(	(
<g/>
Irish	Irish	k1gInSc1	Irish
Republican	Republican	k1gMnSc1	Republican
Brotherhood	Brotherhood	k1gInSc1	Brotherhood
<g/>
/	/	kIx~	/
IRB	IRB	kA	IRB
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1867	[number]	k4	1867
<g/>
.	.	kIx.	.
</s>
<s>
Metody	metoda	k1gFnPc1	metoda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
IRA	Ir	k1gMnSc2	Ir
používala	používat	k5eAaImAgFnS	používat
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
smyslu	smysl	k1gInSc6	smysl
inspirovány	inspirován	k2eAgFnPc1d1	inspirována
i	i	k8xC	i
militantními	militantní	k2eAgInPc7d1	militantní
irskými	irský	k2eAgInPc7d1	irský
agrárními	agrární	k2eAgInPc7d1	agrární
spolky	spolek	k1gInPc7	spolek
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
činnost	činnost	k1gFnSc1	činnost
probíhala	probíhat	k5eAaImAgFnS	probíhat
v	v	k7c6	v
utajení	utajení	k1gNnSc6	utajení
<g/>
.	.	kIx.	.
</s>
<s>
Patřili	patřit	k5eAaImAgMnP	patřit
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
např.	např.	kA	např.
Defenders	Defenders	k1gInSc1	Defenders
<g/>
,	,	kIx,	,
Ribbonmen	Ribbonmen	k1gInSc1	Ribbonmen
a	a	k8xC	a
stoupenci	stoupenec	k1gMnPc1	stoupenec
Irish	Irish	k1gMnSc1	Irish
Land	Land	k1gMnSc1	Land
League	League	k1gFnSc1	League
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zkratka	zkratka	k1gFnSc1	zkratka
IRA	Ir	k1gMnSc2	Ir
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
použita	použít	k5eAaPmNgFnS	použít
organizací	organizace	k1gFnSc7	organizace
IRB	IRB	kA	IRB
(	(	kIx(	(
<g/>
známou	známý	k2eAgFnSc4d1	známá
také	také	k6eAd1	také
pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
Fenian	Fenian	k1gInSc1	Fenian
Brotherhood	Brotherhood	k1gInSc1	Brotherhood
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
Irská	irský	k2eAgFnSc1d1	irská
republikánská	republikánský	k2eAgFnSc1d1	republikánská
armáda	armáda	k1gFnSc1	armáda
se	se	k3xPyFc4	se
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
skládala	skládat	k5eAaImAgFnS	skládat
z	z	k7c2	z
polovojenských	polovojenský	k2eAgFnPc2d1	polovojenská
jednotek	jednotka	k1gFnPc2	jednotka
amerických	americký	k2eAgInPc2d1	americký
Feniánů	Fenián	k1gInPc2	Fenián
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
organizovány	organizovat	k5eAaBmNgInP	organizovat
do	do	k7c2	do
regimentů	regiment	k1gInPc2	regiment
<g/>
.	.	kIx.	.
</s>
<s>
Feniánští	Feniánský	k2eAgMnPc1d1	Feniánský
vojáci	voják	k1gMnPc1	voják
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
bojovali	bojovat	k5eAaImAgMnP	bojovat
pod	pod	k7c7	pod
vlajkou	vlajka	k1gFnSc7	vlajka
IRA	Ir	k1gMnSc2	Ir
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
účastnili	účastnit	k5eAaImAgMnP	účastnit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
i	i	k9	i
boje	boj	k1gInPc4	boj
o	o	k7c4	o
Ridgeway	Ridgeway	k1gInPc4	Ridgeway
(	(	kIx(	(
<g/>
Battle	Battle	k1gFnSc1	Battle
of	of	k?	of
Ridgeway	Ridgewaa	k1gFnSc2	Ridgewaa
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1866	[number]	k4	1866
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Irská	irský	k2eAgFnSc1d1	irská
republikánská	republikánský	k2eAgFnSc1d1	republikánská
armáda	armáda	k1gFnSc1	armáda
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jej	on	k3xPp3gMnSc4	on
známe	znát	k5eAaImIp1nP	znát
dnes	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
použit	použít	k5eAaPmNgInS	použít
ve	v	k7c6	v
dvacátých	dvacátý	k4xOgNnPc6	dvacátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
pro	pro	k7c4	pro
odbojné	odbojný	k2eAgFnPc4d1	odbojná
jednotky	jednotka	k1gFnPc4	jednotka
Irish	Irisha	k1gFnPc2	Irisha
Volunteers	Volunteers	k1gInSc1	Volunteers
a	a	k8xC	a
Irish	Irish	k1gMnSc1	Irish
Citizen	Citizen	kA	Citizen
Army	Arma	k1gFnPc1	Arma
během	během	k7c2	během
Velikonočního	velikonoční	k2eAgNnSc2d1	velikonoční
povstání	povstání	k1gNnSc2	povstání
(	(	kIx(	(
<g/>
Easter	Easter	k1gInSc1	Easter
Rising	Rising	k1gInSc1	Rising
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
označení	označení	k1gNnSc1	označení
použito	použít	k5eAaPmNgNnS	použít
pro	pro	k7c4	pro
ty	ten	k3xDgFnPc4	ten
z	z	k7c2	z
Volunteers	Volunteersa	k1gFnPc2	Volunteersa
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1919	[number]	k4	1919
až	až	k9	až
1921	[number]	k4	1921
vedli	vést	k5eAaImAgMnP	vést
partyzánský	partyzánský	k2eAgInSc4d1	partyzánský
boj	boj	k1gInSc4	boj
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
podporovali	podporovat	k5eAaImAgMnP	podporovat
vyhlášení	vyhlášení	k1gNnPc4	vyhlášení
Irské	irský	k2eAgFnSc2d1	irská
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pozadí	pozadí	k1gNnSc1	pozadí
–	–	k?	–
Home	Hom	k1gInSc2	Hom
Rule	rula	k1gFnSc3	rula
a	a	k8xC	a
Volunteers	Volunteersa	k1gFnPc2	Volunteersa
==	==	k?	==
</s>
</p>
<p>
<s>
Politické	politický	k2eAgNnSc1d1	politické
násilí	násilí	k1gNnSc1	násilí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1916	[number]	k4	1916
až	až	k9	až
1923	[number]	k4	1923
bylo	být	k5eAaImAgNnS	být
jen	jen	k6eAd1	jen
vyústěním	vyústění	k1gNnSc7	vyústění
nesourodých	sourodý	k2eNgInPc2d1	nesourodý
nároků	nárok	k1gInPc2	nárok
unionistů	unionista	k1gMnPc2	unionista
a	a	k8xC	a
nacionalistů	nacionalista	k1gMnPc2	nacionalista
<g/>
.	.	kIx.	.
</s>
<s>
Irští	irský	k2eAgMnPc1d1	irský
nacionalisté	nacionalista	k1gMnPc1	nacionalista
požadovali	požadovat	k5eAaImAgMnP	požadovat
Home	Home	k1gFnSc4	Home
Rule	rula	k1gFnSc6	rula
uvnitř	uvnitř	k7c2	uvnitř
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
a	a	k8xC	a
Britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
však	však	k9	však
unionisté	unionista	k1gMnPc1	unionista
důsledně	důsledně	k6eAd1	důsledně
odmítali	odmítat	k5eAaImAgMnP	odmítat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgNnPc4	tento
jednání	jednání	k1gNnPc4	jednání
ve	v	k7c6	v
slepé	slepý	k2eAgFnSc6d1	slepá
uličce	ulička	k1gFnSc6	ulička
<g/>
.	.	kIx.	.
</s>
<s>
Britská	britský	k2eAgFnSc1d1	britská
vláda	vláda	k1gFnSc1	vláda
byla	být	k5eAaImAgFnS	být
připravena	připraven	k2eAgFnSc1d1	připravena
odsouhlasit	odsouhlasit	k5eAaPmF	odsouhlasit
Home	Home	k1gFnSc4	Home
Rule	rula	k1gFnSc3	rula
popř.	popř.	kA	popř.
irskou	irský	k2eAgFnSc4d1	irská
samosprávu	samospráva	k1gFnSc4	samospráva
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
však	však	k9	však
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
jednotek	jednotka	k1gFnPc2	jednotka
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
pomyslné	pomyslný	k2eAgFnSc2d1	pomyslná
názorové	názorový	k2eAgFnSc2d1	názorová
barikády	barikáda	k1gFnSc2	barikáda
<g/>
.	.	kIx.	.
</s>
<s>
Unionisté	unionista	k1gMnPc1	unionista
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
organizaci	organizace	k1gFnSc4	organizace
Ulsterští	ulsterský	k2eAgMnPc1d1	ulsterský
dobrovolníci	dobrovolník	k1gMnPc1	dobrovolník
a	a	k8xC	a
nacionalisté	nacionalista	k1gMnPc1	nacionalista
Irští	irský	k2eAgMnPc1d1	irský
dobrovolníci	dobrovolník	k1gMnPc1	dobrovolník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Government	Government	k1gInSc1	Government
of	of	k?	of
Ireland	Ireland	k1gInSc1	Ireland
Act	Act	k1gFnSc1	Act
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
spíš	spíš	k9	spíš
známy	znám	k2eAgFnPc4d1	známa
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Third	Third	k1gInSc1	Third
Home	Hom	k1gFnSc2	Hom
Rule	rula	k1gFnSc3	rula
Act	Act	k1gFnSc3	Act
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
vydaný	vydaný	k2eAgInSc1d1	vydaný
britským	britský	k2eAgInSc7d1	britský
parlamentem	parlament	k1gInSc7	parlament
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
poskytnout	poskytnout	k5eAaPmF	poskytnout
Irsku	Irsko	k1gNnSc3	Irsko
možnost	možnost	k1gFnSc4	možnost
regionální	regionální	k2eAgFnSc2d1	regionální
samosprávy	samospráva	k1gFnSc2	samospráva
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Irska	Irsko	k1gNnSc2	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
dostalo	dostat	k5eAaPmAgNnS	dostat
souhlasu	souhlas	k1gInSc3	souhlas
krále	král	k1gMnSc2	král
v	v	k7c6	v
září	září	k1gNnSc6	září
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
platnost	platnost	k1gFnSc1	platnost
byla	být	k5eAaImAgFnS	být
odložena	odložit	k5eAaPmNgFnS	odložit
až	až	k9	až
na	na	k7c4	na
období	období	k1gNnSc4	období
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
existovaly	existovat	k5eAaImAgFnP	existovat
obavy	obava	k1gFnPc1	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
opozice	opozice	k1gFnSc1	opozice
vůči	vůči	k7c3	vůči
tomuto	tento	k3xDgInSc3	tento
zákonu	zákon	k1gInSc3	zákon
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
unionistů	unionista	k1gMnPc2	unionista
a	a	k8xC	a
zásoba	zásoba	k1gFnSc1	zásoba
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pramenila	pramenit	k5eAaImAgFnS	pramenit
z	z	k7c2	z
nelegálního	legální	k2eNgMnSc2d1	nelegální
obchod	obchod	k1gInSc4	obchod
se	s	k7c7	s
zbraněmi	zbraň	k1gFnPc7	zbraň
provozovaný	provozovaný	k2eAgMnSc1d1	provozovaný
ulsterskými	ulsterský	k2eAgMnPc7d1	ulsterský
dobrovolníky	dobrovolník	k1gMnPc7	dobrovolník
a	a	k8xC	a
irským	irský	k2eAgInSc7d1	irský
dobrovolníky	dobrovolník	k1gMnPc4	dobrovolník
mohly	moct	k5eAaImAgInP	moct
vyústit	vyústit	k5eAaPmF	vyústit
v	v	k7c4	v
občanskou	občanský	k2eAgFnSc4d1	občanská
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Začátek	začátek	k1gInSc4	začátek
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1914	[number]	k4	1914
s	s	k7c7	s
sebou	se	k3xPyFc7	se
přinesl	přinést	k5eAaPmAgMnS	přinést
krátkodobé	krátkodobý	k2eAgNnSc4d1	krátkodobé
odsunutí	odsunutí	k1gNnSc4	odsunutí
vnitroirských	vnitroirský	k2eAgInPc2d1	vnitroirský
sporů	spor	k1gInPc2	spor
do	do	k7c2	do
pozadí	pozadí	k1gNnSc2	pozadí
<g/>
.	.	kIx.	.
</s>
<s>
Irish	Irish	k1gInSc1	Irish
Volunteers	Volunteersa	k1gFnPc2	Volunteersa
se	se	k3xPyFc4	se
rozdělili	rozdělit	k5eAaPmAgMnP	rozdělit
na	na	k7c4	na
National	National	k1gFnSc4	National
Volunteers	Volunteersa	k1gFnPc2	Volunteersa
a	a	k8xC	a
Irish	Irisha	k1gFnPc2	Irisha
Volunteers	Volunteersa	k1gFnPc2	Volunteersa
<g/>
.	.	kIx.	.
</s>
<s>
National	Nationat	k5eAaPmAgInS	Nationat
Volunteers	Volunteers	k1gInSc1	Volunteers
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
okolo	okolo	k7c2	okolo
100	[number]	k4	100
000	[number]	k4	000
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
vedl	vést	k5eAaImAgMnS	vést
vůdce	vůdce	k1gMnSc1	vůdce
Irské	irský	k2eAgFnSc2d1	irská
parlamentní	parlamentní	k2eAgFnSc2d1	parlamentní
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
Irish	Irish	k1gInSc1	Irish
Parliamentary	Parliamentara	k1gFnSc2	Parliamentara
Party	parta	k1gFnSc2	parta
<g/>
)	)	kIx)	)
John	John	k1gMnSc1	John
Redmond	Redmond	k1gMnSc1	Redmond
<g/>
.	.	kIx.	.
</s>
<s>
National	Nationat	k5eAaImAgMnS	Nationat
Volunteers	Volunteers	k1gInSc4	Volunteers
věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Britové	Brit	k1gMnPc1	Brit
slib	slib	k1gInSc4	slib
o	o	k7c4	o
Home	Home	k1gFnSc4	Home
Rule	rula	k1gFnSc3	rula
dodrží	dodržet	k5eAaPmIp3nS	dodržet
a	a	k8xC	a
20	[number]	k4	20
tisíc	tisíc	k4xCgInSc4	tisíc
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
bojovalo	bojovat	k5eAaImAgNnS	bojovat
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
britských	britský	k2eAgFnPc6d1	britská
barvách	barva	k1gFnPc6	barva
<g/>
.	.	kIx.	.
</s>
<s>
Dvanáct	dvanáct	k4xCc1	dvanáct
tisíc	tisíc	k4xCgInPc2	tisíc
členů	člen	k1gInPc2	člen
Volunteers	Volunteersa	k1gFnPc2	Volunteersa
však	však	k9	však
účast	účast	k1gFnSc4	účast
v	v	k7c6	v
britských	britský	k2eAgInPc6d1	britský
válečných	válečný	k2eAgInPc6d1	válečný
oddílech	oddíl	k1gInPc6	oddíl
odmítlo	odmítnout	k5eAaPmAgNnS	odmítnout
a	a	k8xC	a
dál	daleko	k6eAd2	daleko
působilo	působit	k5eAaImAgNnS	působit
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Irish	Irisha	k1gFnPc2	Irisha
Volunteers	Volunteersa	k1gFnPc2	Volunteersa
<g/>
.	.	kIx.	.
</s>
<s>
Vedl	vést	k5eAaImAgInS	vést
je	on	k3xPp3gNnSc4	on
Eoin	Eoin	k1gNnSc4	Eoin
MacNeill	MacNeill	k1gInSc1	MacNeill
a	a	k8xC	a
dominovalo	dominovat	k5eAaImAgNnS	dominovat
jim	on	k3xPp3gMnPc3	on
Irské	irský	k2eAgNnSc1d1	irské
republikánské	republikánský	k2eAgNnSc1d1	republikánské
bratrstvo	bratrstvo	k1gNnSc1	bratrstvo
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
MacNeill	MacNeill	k1gMnSc1	MacNeill
byl	být	k5eAaImAgMnS	být
rozhodnut	rozhodnout	k5eAaPmNgMnS	rozhodnout
použít	použít	k5eAaPmF	použít
sílu	síla	k1gFnSc4	síla
jen	jen	k9	jen
v	v	k7c6	v
situaci	situace	k1gFnSc6	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
by	by	kYmCp3nS	by
hrozilo	hrozit	k5eAaImAgNnS	hrozit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
zavedena	zaveden	k2eAgFnSc1d1	zavedena
branná	branný	k2eAgFnSc1d1	Branná
povinnost	povinnost	k1gFnSc1	povinnost
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
kdyby	kdyby	kYmCp3nS	kdyby
chtěla	chtít	k5eAaImAgFnS	chtít
vláda	vláda	k1gFnSc1	vláda
irské	irský	k2eAgMnPc4d1	irský
dobrovolníky	dobrovolník	k1gMnPc4	dobrovolník
odzbrojit	odzbrojit	k5eAaPmF	odzbrojit
<g/>
,	,	kIx,	,
členové	člen	k1gMnPc1	člen
IRB	IRB	kA	IRB
zamýšleli	zamýšlet	k5eAaImAgMnP	zamýšlet
vyvolat	vyvolat	k5eAaPmF	vyvolat
povstání	povstání	k1gNnSc4	povstání
<g/>
,	,	kIx,	,
kterým	který	k3yQgNnSc7	který
sledovali	sledovat	k5eAaImAgMnP	sledovat
získání	získání	k1gNnSc4	získání
irské	irský	k2eAgFnSc2d1	irská
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Velikonoční	velikonoční	k2eAgNnPc1d1	velikonoční
povstání	povstání	k1gNnPc1	povstání
==	==	k?	==
</s>
</p>
<p>
<s>
Zbraně	zbraň	k1gFnPc1	zbraň
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
dvacet	dvacet	k4xCc4	dvacet
tisíc	tisíc	k4xCgInSc4	tisíc
pušek	puška	k1gFnPc2	puška
a	a	k8xC	a
deset	deset	k4xCc1	deset
kulometů	kulomet	k1gInPc2	kulomet
<g/>
,	,	kIx,	,
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
do	do	k7c2	do
Irska	Irsko	k1gNnSc2	Irsko
přivezeny	přivezen	k2eAgInPc4d1	přivezen
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
Aud	Aud	k1gMnSc2	Aud
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
pod	pod	k7c7	pod
záštitou	záštita	k1gFnSc7	záštita
Sira	sir	k1gMnSc2	sir
Rogera	Roger	k1gMnSc2	Roger
Casementa	Casement	k1gMnSc2	Casement
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
hlavních	hlavní	k2eAgMnPc2d1	hlavní
bojovníků	bojovník	k1gMnPc2	bojovník
za	za	k7c4	za
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
převzetí	převzetí	k1gNnSc3	převzetí
však	však	k8xC	však
kvůli	kvůli	k7c3	kvůli
nedorozumění	nedorozumění	k1gNnSc3	nedorozumění
nakonec	nakonec	k6eAd1	nakonec
nikdy	nikdy	k6eAd1	nikdy
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Povstání	povstání	k1gNnSc1	povstání
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
24	[number]	k4	24
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1916	[number]	k4	1916
<g/>
.	.	kIx.	.
</s>
<s>
Eoin	Eoin	k1gMnSc1	Eoin
MacNeill	MacNeill	k1gMnSc1	MacNeill
<g/>
,	,	kIx,	,
vůdce	vůdce	k1gMnSc1	vůdce
Volunteers	Volunteersa	k1gFnPc2	Volunteersa
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
o	o	k7c6	o
plánu	plán	k1gInSc6	plán
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
chvíli	chvíle	k1gFnSc6	chvíle
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
a	a	k8xC	a
vydal	vydat	k5eAaPmAgMnS	vydat
příkaz	příkaz	k1gInSc4	příkaz
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
nařizoval	nařizovat	k5eAaImAgMnS	nařizovat
jednotkám	jednotka	k1gFnPc3	jednotka
Volunteers	Volunteers	k1gInSc4	Volunteers
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
do	do	k7c2	do
povstání	povstání	k1gNnSc2	povstání
nezapojovaly	zapojovat	k5eNaImAgFnP	zapojovat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výsledku	výsledek	k1gInSc6	výsledek
se	se	k3xPyFc4	se
tak	tak	k9	tak
z	z	k7c2	z
dvanácti	dvanáct	k4xCc2	dvanáct
tisíc	tisíc	k4xCgInPc2	tisíc
Volunteers	Volunteersa	k1gFnPc2	Volunteersa
zúčastnily	zúčastnit	k5eAaPmAgFnP	zúčastnit
bojů	boj	k1gInPc2	boj
jen	jen	k6eAd1	jen
pouhé	pouhý	k2eAgInPc1d1	pouhý
dva	dva	k4xCgInPc1	dva
tisíce	tisíc	k4xCgInPc1	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
IRB	IRB	kA	IRB
plánovala	plánovat	k5eAaImAgFnS	plánovat
zmocnit	zmocnit	k5eAaPmF	zmocnit
se	se	k3xPyFc4	se
centra	centrum	k1gNnSc2	centrum
Dublinu	Dublin	k1gInSc6	Dublin
a	a	k8xC	a
následně	následně	k6eAd1	následně
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
vyvolat	vyvolat	k5eAaPmF	vyvolat
podobná	podobný	k2eAgNnPc4d1	podobné
povstání	povstání	k1gNnPc4	povstání
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
však	však	k9	však
nakonec	nakonec	k6eAd1	nakonec
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Britové	Brit	k1gMnPc1	Brit
nastoupili	nastoupit	k5eAaPmAgMnP	nastoupit
na	na	k7c4	na
povstalce	povstalec	k1gMnPc4	povstalec
s	s	k7c7	s
ohromujícím	ohromující	k2eAgInSc7d1	ohromující
arzenálem	arzenál	k1gInSc7	arzenál
zbraní	zbraň	k1gFnPc2	zbraň
a	a	k8xC	a
vojáků	voják	k1gMnPc2	voják
–	–	k?	–
do	do	k7c2	do
Dublinu	Dublin	k1gInSc2	Dublin
bylo	být	k5eAaImAgNnS	být
vysláno	vyslat	k5eAaPmNgNnS	vyslat
šestnáct	šestnáct	k4xCc1	šestnáct
tisíc	tisíc	k4xCgInSc1	tisíc
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
dělostřelců	dělostřelec	k1gMnPc2	dělostřelec
a	a	k8xC	a
dělový	dělový	k2eAgInSc1d1	dělový
člun	člun	k1gInSc1	člun
<g/>
.	.	kIx.	.
</s>
<s>
Povstalci	povstalec	k1gMnPc1	povstalec
se	se	k3xPyFc4	se
po	po	k7c4	po
týden	týden	k1gInSc4	týden
trvajících	trvající	k2eAgInPc6d1	trvající
bojích	boj	k1gInPc6	boj
v	v	k7c6	v
ulicích	ulice	k1gFnPc6	ulice
Dublinu	Dublin	k1gInSc2	Dublin
vzdali	vzdát	k5eAaPmAgMnP	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Povstání	povstání	k1gNnSc1	povstání
si	se	k3xPyFc3	se
vyžádalo	vyžádat	k5eAaPmAgNnS	vyžádat
500	[number]	k4	500
životů	život	k1gInPc2	život
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
polovina	polovina	k1gFnSc1	polovina
byli	být	k5eAaImAgMnP	být
civilisté	civilista	k1gMnPc1	civilista
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
povstání	povstání	k1gNnSc2	povstání
se	se	k3xPyFc4	se
Volunteers	Volunteers	k1gInSc4	Volunteers
začali	začít	k5eAaPmAgMnP	začít
označovat	označovat	k5eAaImF	označovat
za	za	k7c4	za
Irskou	irský	k2eAgFnSc4d1	irská
republikánskou	republikánský	k2eAgFnSc4d1	republikánská
armádu	armáda	k1gFnSc4	armáda
–	–	k?	–
Irish	Irish	k1gMnSc1	Irish
Republican	Republican	k1gMnSc1	Republican
Army	Arma	k1gFnSc2	Arma
<g/>
.	.	kIx.	.
<g/>
Když	když	k8xS	když
se	se	k3xPyFc4	se
vůdci	vůdce	k1gMnPc1	vůdce
povstání	povstání	k1gNnSc2	povstání
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
General	General	k1gMnPc1	General
Post	posta	k1gFnPc2	posta
Office	Office	kA	Office
<g/>
,	,	kIx,	,
vyvěsili	vyvěsit	k5eAaPmAgMnP	vyvěsit
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
zelenou	zelený	k2eAgFnSc4d1	zelená
vlajku	vlajka	k1gFnSc4	vlajka
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
Irská	irský	k2eAgFnSc1d1	irská
republika	republika	k1gFnSc1	republika
a	a	k8xC	a
vyhlásili	vyhlásit	k5eAaPmAgMnP	vyhlásit
nezávislost	nezávislost	k1gFnSc4	nezávislost
Irska	Irsko	k1gNnSc2	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
okamžik	okamžik	k1gInSc1	okamžik
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
celým	celý	k2eAgNnSc7d1	celé
povstáním	povstání	k1gNnSc7	povstání
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
letech	let	k1gInPc6	let
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yIgFnSc3	který
vzhlíželo	vzhlížet	k5eAaImAgNnS	vzhlížet
mnoho	mnoho	k4c1	mnoho
irských	irský	k2eAgMnPc2d1	irský
nacionalistů	nacionalista	k1gMnPc2	nacionalista
<g/>
.	.	kIx.	.
</s>
<s>
Volunteers	Volunteers	k1gInSc1	Volunteers
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
účastnili	účastnit	k5eAaImAgMnP	účastnit
povstání	povstání	k1gNnSc2	povstání
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
jen	jen	k9	jen
malou	malý	k2eAgFnSc7d1	malá
skupinou	skupina	k1gFnSc7	skupina
irských	irský	k2eAgMnPc2d1	irský
nacionalistů	nacionalista	k1gMnPc2	nacionalista
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
stě	sto	k4xCgFnPc1	sto
tisíc	tisíc	k4xCgInSc4	tisíc
Irů	Ir	k1gMnPc2	Ir
dokonce	dokonce	k9	dokonce
sloužilo	sloužit	k5eAaImAgNnS	sloužit
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Britů	Brit	k1gMnPc2	Brit
v	v	k7c6	v
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Irish	Irish	k1gMnSc1	Irish
Independent	independent	k1gMnSc1	independent
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgInSc1d1	hlavní
irský	irský	k2eAgInSc1d1	irský
nacionalistický	nacionalistický	k2eAgInSc1d1	nacionalistický
deník	deník	k1gInSc1	deník
<g/>
,	,	kIx,	,
volal	volat	k5eAaImAgMnS	volat
po	po	k7c6	po
popravě	poprava	k1gFnSc6	poprava
hlavních	hlavní	k2eAgMnPc2d1	hlavní
vzbouřenců	vzbouřenec	k1gMnPc2	vzbouřenec
a	a	k8xC	a
po	po	k7c6	po
jejich	jejich	k3xOp3gFnSc6	jejich
stopě	stopa	k1gFnSc6	stopa
se	se	k3xPyFc4	se
vydaly	vydat	k5eAaPmAgInP	vydat
i	i	k9	i
místní	místní	k2eAgInPc1d1	místní
úřady	úřad	k1gInPc1	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
Dubliňané	Dubliňan	k1gMnPc1	Dubliňan
se	se	k3xPyFc4	se
na	na	k7c6	na
vzbouřence	vzbouřenka	k1gFnSc6	vzbouřenka
dívali	dívat	k5eAaImAgMnP	dívat
s	s	k7c7	s
opovržením	opovržení	k1gNnSc7	opovržení
<g/>
,	,	kIx,	,
plivali	plivat	k5eAaImAgMnP	plivat
na	na	k7c4	na
ně	on	k3xPp3gNnSc4	on
<g/>
,	,	kIx,	,
házeli	házet	k5eAaImAgMnP	házet
po	po	k7c6	po
nich	on	k3xPp3gMnPc6	on
kameny	kámen	k1gInPc4	kámen
a	a	k8xC	a
polévali	polévat	k5eAaImAgMnP	polévat
je	on	k3xPp3gInPc4	on
splašky	splašky	k1gInPc4	splašky
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byli	být	k5eAaImAgMnP	být
vedeni	vést	k5eAaImNgMnP	vést
k	k	k7c3	k
transportním	transportní	k2eAgFnPc3d1	transportní
lodím	loď	k1gFnPc3	loď
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
je	on	k3xPp3gNnPc4	on
měli	mít	k5eAaImAgMnP	mít
odvést	odvést	k5eAaPmF	odvést
do	do	k7c2	do
velšských	velšský	k2eAgInPc2d1	velšský
internačních	internační	k2eAgInPc2d1	internační
táborů	tábor	k1gInPc2	tábor
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
je	on	k3xPp3gNnSc4	on
i	i	k9	i
litovali	litovat	k5eAaImAgMnP	litovat
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
následujících	následující	k2eAgNnPc2d1	následující
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
však	však	k9	však
názor	názor	k1gInSc1	názor
veřejnosti	veřejnost	k1gFnSc2	veřejnost
na	na	k7c6	na
vzbouřence	vzbouřenka	k1gFnSc6	vzbouřenka
změnil	změnit	k5eAaPmAgInS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
tomu	ten	k3xDgNnSc3	ten
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
pohoršení	pohoršení	k1gNnSc1	pohoršení
nad	nad	k7c7	nad
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yQgInSc7	jaký
Britové	Brit	k1gMnPc1	Brit
potrestali	potrestat	k5eAaPmAgMnP	potrestat
vůdce	vůdce	k1gMnPc4	vůdce
povstání	povstání	k1gNnSc2	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Popravě	poprava	k1gFnSc6	poprava
neuniklo	uniknout	k5eNaPmAgNnS	uniknout
šestnáct	šestnáct	k4xCc1	šestnáct
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Jamese	Jamese	k1gFnSc2	Jamese
Connollyho	Connolly	k1gMnSc2	Connolly
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
tak	tak	k6eAd1	tak
nemocný	mocný	k2eNgMnSc1d1	nemocný
<g/>
,	,	kIx,	,
že	že	k8xS	že
ani	ani	k8xC	ani
nemohl	moct	k5eNaImAgMnS	moct
stát	stát	k5eAaImF	stát
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jej	on	k3xPp3gMnSc4	on
museli	muset	k5eAaImAgMnP	muset
popravit	popravit	k5eAaPmF	popravit
v	v	k7c6	v
sedě	sedě	k6eAd1	sedě
<g/>
.	.	kIx.	.
</s>
<s>
Popraveni	popravit	k5eAaPmNgMnP	popravit
byli	být	k5eAaImAgMnP	být
i	i	k9	i
další	další	k2eAgMnPc1d1	další
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yQgFnPc2	který
bylo	být	k5eAaImAgNnS	být
podezření	podezření	k1gNnPc4	podezření
<g/>
,	,	kIx,	,
že	že	k8xS	že
povstání	povstání	k1gNnSc4	povstání
napomáhali	napomáhat	k5eAaImAgMnP	napomáhat
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
vlna	vlna	k1gFnSc1	vlna
odporu	odpor	k1gInSc2	odpor
vůči	vůči	k7c3	vůči
britské	britský	k2eAgFnSc3d1	britská
nadvládě	nadvláda	k1gFnSc3	nadvláda
vzešla	vzejít	k5eAaPmAgFnS	vzejít
v	v	k7c6	v
letech	let	k1gInPc6	let
1917	[number]	k4	1917
–	–	k?	–
1918	[number]	k4	1918
z	z	k7c2	z
pokusu	pokus	k1gInSc2	pokus
zavést	zavést	k5eAaPmF	zavést
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
brannou	branný	k2eAgFnSc4d1	Branná
povinnost	povinnost	k1gFnSc4	povinnost
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
zvaná	zvaný	k2eAgNnPc1d1	zvané
Conscription	Conscription	k1gInSc4	Conscription
Crisis	Crisis	k1gInSc4	Crisis
byla	být	k5eAaImAgFnS	být
vyvolaná	vyvolaný	k2eAgFnSc1d1	vyvolaná
britskou	britský	k2eAgFnSc7d1	britská
snahou	snaha	k1gFnSc7	snaha
oživit	oživit	k5eAaPmF	oživit
své	svůj	k3xOyFgFnPc4	svůj
ochabující	ochabující	k2eAgFnPc4d1	ochabující
síly	síla	k1gFnPc4	síla
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
frontě	fronta	k1gFnSc6	fronta
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
setkalo	setkat	k5eAaPmAgNnS	setkat
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
s	s	k7c7	s
ohromným	ohromný	k2eAgInSc7d1	ohromný
odporem	odpor	k1gInSc7	odpor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sinn	Sinn	k1gMnSc1	Sinn
Féin	Féin	k1gMnSc1	Féin
<g/>
,	,	kIx,	,
malá	malý	k2eAgFnSc1d1	malá
nacionalisticky	nacionalisticky	k6eAd1	nacionalisticky
orientovaná	orientovaný	k2eAgFnSc1d1	orientovaná
irská	irský	k2eAgFnSc1d1	irská
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
všeobecně	všeobecně	k6eAd1	všeobecně
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
mylně	mylně	k6eAd1	mylně
<g/>
,	,	kIx,	,
označovaná	označovaný	k2eAgFnSc1d1	označovaná
za	za	k7c4	za
strůjce	strůjce	k1gMnPc4	strůjce
povstání	povstání	k1gNnSc2	povstání
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
její	její	k3xOp3gMnSc1	její
vůdce	vůdce	k1gMnSc1	vůdce
Arthur	Arthur	k1gMnSc1	Arthur
Griffith	Griffith	k1gMnSc1	Griffith
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
podporoval	podporovat	k5eAaImAgMnS	podporovat
myšlenku	myšlenka	k1gFnSc4	myšlenka
irské	irský	k2eAgFnSc2d1	irská
samosprávy	samospráva	k1gFnSc2	samospráva
v	v	k7c6	v
dualistické	dualistický	k2eAgFnSc6d1	dualistická
monarchii	monarchie	k1gFnSc6	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
republikáni	republikán	k1gMnPc1	republikán
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
přežili	přežít	k5eAaPmAgMnP	přežít
povstání	povstání	k1gNnSc4	povstání
a	a	k8xC	a
represe	represe	k1gFnPc4	represe
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
<g/>
,	,	kIx,	,
stranu	strana	k1gFnSc4	strana
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
převzali	převzít	k5eAaPmAgMnP	převzít
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Éamona	Éamon	k1gMnSc2	Éamon
de	de	k?	de
Valery	Valera	k1gFnSc2	Valera
pak	pak	k6eAd1	pak
Sinn	Sinn	k1gInSc4	Sinn
Féin	Féina	k1gFnPc2	Féina
zasvětila	zasvětit	k5eAaPmAgFnS	zasvětit
své	svůj	k3xOyFgNnSc4	svůj
působení	působení	k1gNnSc4	působení
boji	boj	k1gInSc6	boj
za	za	k7c4	za
vznik	vznik	k1gInSc4	vznik
Irské	irský	k2eAgFnSc2d1	irská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1916	[number]	k4	1916
a	a	k8xC	a
1918	[number]	k4	1918
dvě	dva	k4xCgFnPc1	dva
dominantní	dominantní	k2eAgFnPc1d1	dominantní
síly	síla	k1gFnPc1	síla
irského	irský	k2eAgNnSc2d1	irské
nacionalistického	nacionalistický	k2eAgNnSc2d1	nacionalistické
hnutí	hnutí	k1gNnSc2	hnutí
–	–	k?	–
Sinn	Sinn	k1gMnSc1	Sinn
Féin	Féin	k1gMnSc1	Féin
a	a	k8xC	a
Irská	irský	k2eAgFnSc1d1	irská
parlamentní	parlamentní	k2eAgFnSc1d1	parlamentní
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
bojovaly	bojovat	k5eAaImAgInP	bojovat
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
v	v	k7c6	v
sérii	série	k1gFnSc6	série
bitev	bitva	k1gFnPc2	bitva
v	v	k7c6	v
doplňovacích	doplňovací	k2eAgFnPc6d1	doplňovací
volbách	volba	k1gFnPc6	volba
<g/>
.	.	kIx.	.
</s>
<s>
Žádná	žádný	k3yNgFnSc1	žádný
ze	z	k7c2	z
stran	strana	k1gFnPc2	strana
nikdy	nikdy	k6eAd1	nikdy
nevyhrála	vyhrát	k5eNaPmAgFnS	vyhrát
rozhodujícím	rozhodující	k2eAgInSc7d1	rozhodující
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
krize	krize	k1gFnSc1	krize
vyvolaná	vyvolaný	k2eAgFnSc1d1	vyvolaná
hrozbou	hrozba	k1gFnSc7	hrozba
branné	branný	k2eAgFnSc3d1	Branná
povinnosti	povinnost	k1gFnSc3	povinnost
však	však	k9	však
pomohla	pomoct	k5eAaPmAgFnS	pomoct
pohnout	pohnout	k5eAaPmF	pohnout
zatím	zatím	k6eAd1	zatím
nerozhodným	rozhodný	k2eNgInSc7d1	nerozhodný
jazýčkem	jazýček	k1gInSc7	jazýček
vah	váha	k1gFnPc2	váha
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Sinn	Sinn	k1gNnSc4	Sinn
Féin	Féino	k1gNnPc2	Féino
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc4	který
pak	pak	k6eAd1	pak
ve	v	k7c6	v
všeobecných	všeobecný	k2eAgFnPc6d1	všeobecná
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
většinu	většina	k1gFnSc4	většina
irských	irský	k2eAgNnPc2d1	irské
křesel	křeslo	k1gNnPc2	křeslo
v	v	k7c6	v
britském	britský	k2eAgInSc6d1	britský
parlamentu	parlament	k1gInSc6	parlament
(	(	kIx(	(
<g/>
73	[number]	k4	73
ze	z	k7c2	z
105	[number]	k4	105
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poslanci	poslanec	k1gMnPc1	poslanec
Sinn	Sinn	k1gMnSc1	Sinn
Féin	Féin	k1gMnSc1	Féin
však	však	k9	však
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
nezasedli	zasednout	k5eNaPmAgMnP	zasednout
<g/>
,	,	kIx,	,
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1919	[number]	k4	1919
se	se	k3xPyFc4	se
v	v	k7c6	v
Dublinu	Dublin	k1gInSc6	Dublin
prohlásili	prohlásit	k5eAaPmAgMnP	prohlásit
za	za	k7c4	za
legitimní	legitimní	k2eAgInSc4d1	legitimní
irský	irský	k2eAgInSc4d1	irský
parlament	parlament	k1gInSc4	parlament
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Dáil	Dáil	k1gMnSc1	Dáil
Éireann	Éireann	k1gMnSc1	Éireann
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Irští	irský	k2eAgMnPc1d1	irský
dobrovolníci	dobrovolník	k1gMnPc1	dobrovolník
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
počet	počet	k1gInSc1	počet
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
Conscription	Conscription	k1gInSc1	Conscription
Crisis	Crisis	k1gFnSc3	Crisis
rozrostl	rozrůst	k5eAaPmAgInS	rozrůst
na	na	k7c4	na
sto	sto	k4xCgNnSc4	sto
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
přeorganizovali	přeorganizovat	k5eAaPmAgMnP	přeorganizovat
na	na	k7c4	na
armádu	armáda	k1gFnSc4	armáda
této	tento	k3xDgFnSc3	tento
nově	nova	k1gFnSc3	nova
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
tak	tak	k9	tak
si	se	k3xPyFc3	se
začali	začít	k5eAaPmAgMnP	začít
také	také	k9	také
říkat	říkat	k5eAaImF	říkat
Irská	irský	k2eAgFnSc1d1	irská
republikánská	republikánský	k2eAgFnSc1d1	republikánská
armáda	armáda	k1gFnSc1	armáda
–	–	k?	–
Irish	Irish	k1gInSc1	Irish
Republican	Republicana	k1gFnPc2	Republicana
Army	Arma	k1gFnSc2	Arma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
IRA	Ir	k1gMnSc2	Ir
po	po	k7c6	po
Velikonočním	velikonoční	k2eAgNnSc6d1	velikonoční
povstání	povstání	k1gNnSc6	povstání
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgInPc1	první
kroky	krok	k1gInPc1	krok
k	k	k7c3	k
reorganizaci	reorganizace	k1gFnSc3	reorganizace
poražených	poražený	k1gMnPc2	poražený
irských	irský	k2eAgMnPc2d1	irský
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
se	se	k3xPyFc4	se
uskutečnily	uskutečnit	k5eAaPmAgFnP	uskutečnit
27	[number]	k4	27
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1917	[number]	k4	1917
na	na	k7c6	na
sjezdu	sjezd	k1gInSc6	sjezd
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
v	v	k7c6	v
Dublinu	Dublin	k1gInSc6	Dublin
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
sjezd	sjezd	k1gInSc1	sjezd
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
nazýván	nazývat	k5eAaImNgInS	nazývat
sjezdem	sjezd	k1gInSc7	sjezd
IRA	Ir	k1gMnSc2	Ir
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
svolán	svolat	k5eAaPmNgInS	svolat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
ve	v	k7c4	v
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
jako	jako	k8xC	jako
konference	konference	k1gFnSc1	konference
pořádaná	pořádaný	k2eAgFnSc1d1	pořádaná
stranou	strana	k1gFnSc7	strana
Sinn	Sinn	k1gMnSc1	Sinn
Féin	Féin	k1gMnSc1	Féin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
mnohým	mnohý	k2eAgMnPc3d1	mnohý
bránilo	bránit	k5eAaImAgNnS	bránit
v	v	k7c6	v
příjezdu	příjezd	k1gInSc6	příjezd
zajetí	zajetí	k1gNnSc2	zajetí
v	v	k7c6	v
internačních	internační	k2eAgInPc6d1	internační
táborech	tábor	k1gInPc6	tábor
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
sjezd	sjezd	k1gInSc1	sjezd
téměř	téměř	k6eAd1	téměř
250	[number]	k4	250
účastníků	účastník	k1gMnPc2	účastník
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
Královské	královský	k2eAgFnSc2d1	královská
ulsterské	ulsterský	k2eAgFnSc2d1	Ulsterská
policie	policie	k1gFnSc2	policie
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
162	[number]	k4	162
aktivních	aktivní	k2eAgInPc2d1	aktivní
spolků	spolek	k1gInPc2	spolek
Volunteers	Volunteersa	k1gFnPc2	Volunteersa
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
zdroje	zdroj	k1gInPc1	zdroj
však	však	k9	však
uváděly	uvádět	k5eAaImAgInP	uvádět
i	i	k9	i
390	[number]	k4	390
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
těchto	tento	k3xDgInPc2	tento
jednání	jednání	k1gNnSc3	jednání
byl	být	k5eAaImAgInS	být
Éamon	Éamon	k1gInSc1	Éamon
de	de	k?	de
Valera	Valer	k1gMnSc2	Valer
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
den	den	k1gInSc4	den
předtím	předtím	k6eAd1	předtím
zvolen	zvolit	k5eAaPmNgMnS	zvolit
prezidentem	prezident	k1gMnSc7	prezident
Sinn	Sinn	k1gMnSc1	Sinn
Féin	Féin	k1gMnSc1	Féin
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
významnými	významný	k2eAgFnPc7d1	významná
osobnostmi	osobnost	k1gFnPc7	osobnost
setkání	setkání	k1gNnSc2	setkání
byl	být	k5eAaImAgMnS	být
Cathal	Cathal	k1gMnSc1	Cathal
Brugha	Brugha	k1gMnSc1	Brugha
<g/>
,	,	kIx,	,
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
premiér	premiér	k1gMnSc1	premiér
(	(	kIx(	(
<g/>
Príomh	Príomh	k1gInSc1	Príomh
Aire	Air	k1gInSc2	Air
<g/>
)	)	kIx)	)
zvolený	zvolený	k2eAgMnSc1d1	zvolený
Dáilem	Dáilo	k1gNnSc7	Dáilo
<g/>
,	,	kIx,	,
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgMnPc2d1	další
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
už	už	k6eAd1	už
v	v	k7c6	v
předchozích	předchozí	k2eAgInPc6d1	předchozí
měsících	měsíc	k1gInPc6	měsíc
sehráli	sehrát	k5eAaPmAgMnP	sehrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
v	v	k7c6	v
reorganizaci	reorganizace	k1gFnSc6	reorganizace
Volunteers	Volunteersa	k1gFnPc2	Volunteersa
a	a	k8xC	a
strávili	strávit	k5eAaPmAgMnP	strávit
kvůli	kvůli	k7c3	kvůli
svému	svůj	k3xOyFgInSc3	svůj
boji	boj	k1gInSc3	boj
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentem	prezident	k1gMnSc7	prezident
byl	být	k5eAaImAgMnS	být
zvolen	zvolen	k2eAgMnSc1d1	zvolen
De	De	k?	De
Valera	Valero	k1gNnSc2	Valero
a	a	k8xC	a
zvolena	zvolit	k5eAaPmNgFnS	zvolit
byla	být	k5eAaImAgFnS	být
i	i	k9	i
moc	moc	k1gFnSc1	moc
výkonná	výkonný	k2eAgFnSc1d1	výkonná
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
tvořili	tvořit	k5eAaImAgMnP	tvořit
oblastní	oblastní	k2eAgMnPc1d1	oblastní
zástupci	zástupce	k1gMnPc1	zástupce
včetně	včetně	k7c2	včetně
Dublinu	Dublin	k1gInSc2	Dublin
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
byli	být	k5eAaImAgMnP	být
zvoleni	zvolen	k2eAgMnPc1d1	zvolen
ředitelé	ředitel	k1gMnPc1	ředitel
různých	různý	k2eAgInPc2d1	různý
resortů	resort	k1gInPc2	resort
IRA	Ir	k1gMnSc2	Ir
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
to	ten	k3xDgNnSc4	ten
<g/>
:	:	kIx,	:
Michael	Michael	k1gMnSc1	Michael
Collins	Collinsa	k1gFnPc2	Collinsa
(	(	kIx(	(
<g/>
ředitel	ředitel	k1gMnSc1	ředitel
pro	pro	k7c4	pro
organizaci	organizace	k1gFnSc4	organizace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Diarmuid	Diarmuid	k1gInSc1	Diarmuid
Lynch	Lynch	k1gInSc1	Lynch
(	(	kIx(	(
<g/>
ředitel	ředitel	k1gMnSc1	ředitel
pro	pro	k7c4	pro
komunikaci	komunikace	k1gFnSc4	komunikace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
Staines	Staines	k1gMnSc1	Staines
(	(	kIx(	(
<g/>
ředitel	ředitel	k1gMnSc1	ředitel
pro	pro	k7c4	pro
zásobování	zásobování	k1gNnSc4	zásobování
<g/>
)	)	kIx)	)
a	a	k8xC	a
Rory	Rora	k1gFnSc2	Rora
O	o	k7c6	o
<g/>
́	́	k?	́
<g/>
Connor	Connor	k1gMnSc1	Connor
(	(	kIx(	(
<g/>
ředitel	ředitel	k1gMnSc1	ředitel
pro	pro	k7c4	pro
technické	technický	k2eAgFnPc4d1	technická
záležitosti	záležitost	k1gFnPc4	záležitost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Seán	Seán	k1gMnSc1	Seán
McGarry	McGarra	k1gFnSc2	McGarra
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
generálním	generální	k2eAgMnSc7d1	generální
tajemníkem	tajemník	k1gMnSc7	tajemník
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Cathal	Cathal	k1gMnSc1	Cathal
Brugha	Brugha	k1gMnSc1	Brugha
dostal	dostat	k5eAaPmAgMnS	dostat
funkci	funkce	k1gFnSc4	funkce
náčelníka	náčelník	k1gMnSc2	náčelník
štábu	štáb	k1gInSc2	štáb
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dáil	Dáil	k1gMnSc1	Dáil
Éireann	Éireann	k1gMnSc1	Éireann
a	a	k8xC	a
IRA	Ir	k1gMnSc2	Ir
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
IRA	Ir	k1gMnSc4	Ir
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
armáda	armáda	k1gFnSc1	armáda
Irské	irský	k2eAgFnSc2d1	irská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
zodpovídat	zodpovídat	k5eAaImF	zodpovídat
Dáilu	Dáila	k1gFnSc4	Dáila
<g/>
,	,	kIx,	,
realita	realita	k1gFnSc1	realita
však	však	k9	však
byla	být	k5eAaImAgFnS	být
jiná	jiný	k2eAgFnSc1d1	jiná
a	a	k8xC	a
pro	pro	k7c4	pro
parlament	parlament	k1gInSc4	parlament
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
obtížné	obtížný	k2eAgNnSc1d1	obtížné
kontrolovat	kontrolovat	k5eAaImF	kontrolovat
činnost	činnost	k1gFnSc4	činnost
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
vedení	vedení	k1gNnSc1	vedení
republiky	republika	k1gFnSc2	republika
mělo	mít	k5eAaImAgNnS	mít
obavy	obava	k1gFnPc4	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
IRA	Ir	k1gMnSc4	Ir
nemusela	muset	k5eNaImAgFnS	muset
akceptovat	akceptovat	k5eAaBmF	akceptovat
jeho	jeho	k3xOp3gFnSc4	jeho
autoritu	autorita	k1gFnSc4	autorita
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
pramenilo	pramenit	k5eAaImAgNnS	pramenit
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Volunteers	Volunteers	k1gInSc4	Volunteers
byli	být	k5eAaImAgMnP	být
vázáni	vázat	k5eAaImNgMnP	vázat
vlastní	vlastní	k2eAgFnSc7d1	vlastní
interní	interní	k2eAgFnSc7d1	interní
ústavou	ústava	k1gFnSc7	ústava
poslouchat	poslouchat	k5eAaImF	poslouchat
svou	svůj	k3xOyFgFnSc4	svůj
exekutivu	exekutiva	k1gFnSc4	exekutiva
a	a	k8xC	a
nikoho	nikdo	k3yNnSc2	nikdo
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
.	.	kIx.	.
</s>
<s>
Obavy	obava	k1gFnPc1	obava
ještě	ještě	k9	ještě
vzrostly	vzrůst	k5eAaPmAgFnP	vzrůst
po	po	k7c6	po
incidentu	incident	k1gInSc6	incident
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
odehrál	odehrát	k5eAaPmAgInS	odehrát
v	v	k7c4	v
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
nový	nový	k2eAgInSc1d1	nový
národní	národní	k2eAgInSc1d1	národní
parlament	parlament	k1gInSc1	parlament
poprvé	poprvé	k6eAd1	poprvé
sejít	sejít	k5eAaPmF	sejít
<g/>
,	,	kIx,	,
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
Jednotka	jednotka	k1gFnSc1	jednotka
IRA	Ir	k1gMnSc2	Ir
z	z	k7c2	z
Jižní	jižní	k2eAgFnSc2d1	jižní
Tipperary	Tipperara	k1gFnSc2	Tipperara
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jednala	jednat	k5eAaImAgFnS	jednat
na	na	k7c4	na
vlastní	vlastní	k2eAgInSc4d1	vlastní
rozkaz	rozkaz	k1gInSc4	rozkaz
<g/>
,	,	kIx,	,
přepadla	přepadnout	k5eAaPmAgFnS	přepadnout
dva	dva	k4xCgMnPc4	dva
strážníky	strážník	k1gMnPc4	strážník
Královské	královský	k2eAgFnSc2d1	královská
ulsterské	ulsterský	k2eAgFnSc2d1	Ulsterská
policie	policie	k1gFnSc2	policie
(	(	kIx(	(
<g/>
RIC	RIC	kA	RIC
<g/>
)	)	kIx)	)
a	a	k8xC	a
zmocnila	zmocnit	k5eAaPmAgFnS	zmocnit
se	se	k3xPyFc4	se
množství	množství	k1gNnSc4	množství
gelignitu	gelignit	k1gInSc2	gelignit
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
strážníci	strážník	k1gMnPc1	strážník
přišli	přijít	k5eAaPmAgMnP	přijít
během	během	k7c2	během
útoku	útok	k1gInSc2	útok
o	o	k7c4	o
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Technicky	technicky	k6eAd1	technicky
vzato	vzat	k2eAgNnSc1d1	vzato
se	se	k3xPyFc4	se
vojáci	voják	k1gMnPc1	voják
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
za	za	k7c4	za
útok	útok	k1gInSc4	útok
zodpovědní	zodpovědný	k2eAgMnPc1d1	zodpovědný
<g/>
,	,	kIx,	,
dostali	dostat	k5eAaPmAgMnP	dostat
do	do	k7c2	do
rozporu	rozpor	k1gInSc2	rozpor
s	s	k7c7	s
disciplinárním	disciplinární	k2eAgInSc7d1	disciplinární
řádem	řád	k1gInSc7	řád
IRA	Ir	k1gMnSc2	Ir
a	a	k8xC	a
měli	mít	k5eAaImAgMnP	mít
být	být	k5eAaImF	být
souzeni	soudit	k5eAaImNgMnP	soudit
před	před	k7c7	před
válečným	válečný	k2eAgInSc7d1	válečný
soudem	soud	k1gInSc7	soud
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
však	však	k9	však
politicky	politicky	k6eAd1	politicky
výhodnější	výhodný	k2eAgMnSc1d2	výhodnější
využít	využít	k5eAaPmF	využít
incident	incident	k1gInSc4	incident
jako	jako	k8xC	jako
důkaz	důkaz	k1gInSc4	důkaz
o	o	k7c6	o
rostoucím	rostoucí	k2eAgInSc6d1	rostoucí
militarismu	militarismus	k1gInSc6	militarismus
<g/>
.	.	kIx.	.
</s>
<s>
Konflikt	konflikt	k1gInSc4	konflikt
brzo	brzo	k6eAd1	brzo
přerostl	přerůst	k5eAaPmAgMnS	přerůst
v	v	k7c4	v
partyzánské	partyzánský	k2eAgInPc4d1	partyzánský
boje	boj	k1gInPc4	boj
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c6	v
odlehlých	odlehlý	k2eAgFnPc6d1	odlehlá
oblastech	oblast	k1gFnPc6	oblast
měla	mít	k5eAaImAgFnS	mít
na	na	k7c4	na
svědomí	svědomí	k1gNnSc4	svědomí
organizace	organizace	k1gFnSc2	organizace
tehdy	tehdy	k6eAd1	tehdy
zvaná	zvaný	k2eAgFnSc1d1	zvaná
jako	jako	k8xS	jako
Flying	Flying	k1gInSc1	Flying
Columns	Columnsa	k1gFnPc2	Columnsa
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1919	[number]	k4	1919
a	a	k8xC	a
1920	[number]	k4	1920
útoky	útok	k1gInPc4	útok
na	na	k7c4	na
vzdálená	vzdálený	k2eAgNnPc4d1	vzdálené
kasárna	kasárna	k1gNnPc4	kasárna
Královské	královský	k2eAgFnSc2d1	královská
ulsterské	ulsterský	k2eAgFnSc2d1	Ulsterská
policie	policie	k1gFnSc2	policie
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
<g/>
,	,	kIx,	,
stáhly	stáhnout	k5eAaPmAgInP	stáhnout
se	se	k3xPyFc4	se
policejní	policejní	k2eAgFnSc2d1	policejní
jednotky	jednotka	k1gFnSc2	jednotka
do	do	k7c2	do
velkých	velký	k2eAgNnPc2d1	velké
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
opevnily	opevnit	k5eAaPmAgInP	opevnit
a	a	k8xC	a
přenechaly	přenechat	k5eAaPmAgInP	přenechat
tak	tak	k6eAd1	tak
venkov	venkov	k1gInSc4	venkov
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
republikánů	republikán	k1gMnPc2	republikán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Už	už	k6eAd1	už
před	před	k7c7	před
lednovým	lednový	k2eAgInSc7d1	lednový
útokem	útok	k1gInSc7	útok
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
snahy	snaha	k1gFnPc1	snaha
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
IRA	Ir	k1gMnSc2	Ir
stala	stát	k5eAaPmAgFnS	stát
armádou	armáda	k1gFnSc7	armáda
parlamentu	parlament	k1gInSc2	parlament
a	a	k8xC	a
ne	ne	k9	ne
silou	síla	k1gFnSc7	síla
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
stojí	stát	k5eAaImIp3nS	stát
proti	proti	k7c3	proti
němu	on	k3xPp3gNnSc3	on
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byly	být	k5eAaImAgFnP	být
potlačeny	potlačit	k5eAaPmNgInP	potlačit
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1919	[number]	k4	1919
vydal	vydat	k5eAaPmAgInS	vydat
útvar	útvar	k1gInSc1	útvar
IRA	Ir	k1gMnSc2	Ir
<g/>
,	,	kIx,	,
An	An	k1gMnSc2	An
tÓglách	tÓgla	k1gFnPc6	tÓgla
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Volunteer	Volunteer	k1gMnSc1	Volunteer
<g/>
"	"	kIx"	"
–	–	k?	–
dobrovolník	dobrovolník	k1gMnSc1	dobrovolník
<g/>
)	)	kIx)	)
seznam	seznam	k1gInSc1	seznam
zásad	zásada	k1gFnPc2	zásada
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yRgInPc6	který
se	se	k3xPyFc4	se
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
dva	dva	k4xCgMnPc1	dva
členové	člen	k1gMnPc1	člen
irského	irský	k2eAgInSc2d1	irský
kabinetu	kabinet	k1gInSc2	kabinet
Aireacht	Aireacht	k1gMnSc1	Aireacht
Cathal	Cathal	k1gMnSc1	Cathal
Brugha	Brugha	k1gMnSc1	Brugha
a	a	k8xC	a
Richard	Richard	k1gMnSc1	Richard
Mulcahy	Mulcaha	k1gFnSc2	Mulcaha
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
objevuje	objevovat	k5eAaImIp3nS	objevovat
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
organizace	organizace	k1gFnSc1	organizace
měla	mít	k5eAaImAgFnS	mít
chovat	chovat	k5eAaImF	chovat
k	k	k7c3	k
ozbrojeným	ozbrojený	k2eAgFnPc3d1	ozbrojená
silám	síla	k1gFnPc3	síla
nepřítele	nepřítel	k1gMnSc2	nepřítel
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k9	už
vojákům	voják	k1gMnPc3	voják
či	či	k8xC	či
policistům	policista	k1gMnPc3	policista
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
chovala	chovat	k5eAaImAgFnS	chovat
armáda	armáda	k1gFnSc1	armáda
státu	stát	k1gInSc2	stát
k	k	k7c3	k
invazní	invazní	k2eAgFnSc3d1	invazní
armádě	armáda	k1gFnSc3	armáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pozdější	pozdní	k2eAgNnPc4d2	pozdější
léta	léto	k1gNnPc4	léto
==	==	k?	==
</s>
</p>
<p>
<s>
Období	období	k1gNnSc1	období
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1922	[number]	k4	1922
a	a	k8xC	a
1923	[number]	k4	1923
bylo	být	k5eAaImAgNnS	být
obdobím	období	k1gNnSc7	období
irské	irský	k2eAgFnSc2d1	irská
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
vedené	vedený	k2eAgFnSc2d1	vedená
mezi	mezi	k7c7	mezi
IRA	Ir	k1gMnSc2	Ir
a	a	k8xC	a
armádou	armáda	k1gFnSc7	armáda
Svobodného	svobodný	k2eAgInSc2d1	svobodný
irského	irský	k2eAgInSc2d1	irský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
konfliktu	konflikt	k1gInSc6	konflikt
byly	být	k5eAaImAgInP	být
zbytky	zbytek	k1gInPc1	zbytek
původní	původní	k2eAgFnSc2d1	původní
IRA	Ir	k1gMnSc2	Ir
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
rozprášeny	rozprášen	k2eAgInPc1d1	rozprášen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Konflikt	konflikt	k1gInSc4	konflikt
v	v	k7c6	v
Severním	severní	k2eAgNnSc6d1	severní
Irsku	Irsko	k1gNnSc6	Irsko
==	==	k?	==
</s>
</p>
<p>
<s>
Rok	rok	k1gInSc1	rok
1969	[number]	k4	1969
přinesl	přinést	k5eAaPmAgInS	přinést
rozdělení	rozdělení	k1gNnSc4	rozdělení
této	tento	k3xDgFnSc2	tento
strany	strana	k1gFnSc2	strana
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
frakce	frakce	k1gFnPc4	frakce
<g/>
:	:	kIx,	:
OIRA	OIRA	kA	OIRA
(	(	kIx(	(
<g/>
oficiální	oficiální	k2eAgFnSc4d1	oficiální
linii	linie	k1gFnSc4	linie
<g/>
)	)	kIx)	)
a	a	k8xC	a
PIRA	PIRA	kA	PIRA
(	(	kIx(	(
<g/>
provizorní	provizorní	k2eAgFnSc1d1	provizorní
<g/>
,	,	kIx,	,
prozatímní	prozatímní	k2eAgFnSc1d1	prozatímní
irská	irský	k2eAgFnSc1d1	irská
republikánská	republikánský	k2eAgFnSc1d1	republikánská
armáda	armáda	k1gFnSc1	armáda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
PIRA	PIRA	kA	PIRA
dále	daleko	k6eAd2	daleko
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
v	v	k7c6	v
teroristických	teroristický	k2eAgInPc6d1	teroristický
útocích	útok	k1gInPc6	útok
<g/>
,	,	kIx,	,
jakými	jaký	k3yRgInPc7	jaký
byly	být	k5eAaImAgFnP	být
bombové	bombový	k2eAgInPc4d1	bombový
útoky	útok	k1gInPc4	útok
<g/>
,	,	kIx,	,
vraždy	vražda	k1gFnPc4	vražda
a	a	k8xC	a
vydírání	vydírání	k1gNnSc4	vydírání
<g/>
,	,	kIx,	,
zaměřené	zaměřený	k2eAgFnPc4d1	zaměřená
na	na	k7c4	na
vládní	vládní	k2eAgMnPc4d1	vládní
britské	britský	k2eAgMnPc4d1	britský
činitele	činitel	k1gMnPc4	činitel
<g/>
,	,	kIx,	,
politické	politický	k2eAgFnPc1d1	politická
a	a	k8xC	a
vojenské	vojenský	k2eAgFnPc1d1	vojenská
jednotky	jednotka	k1gFnPc1	jednotka
a	a	k8xC	a
zařízení	zařízení	k1gNnPc1	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
na	na	k7c6	na
území	území	k1gNnSc6	území
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
Severního	severní	k2eAgNnSc2d1	severní
Irska	Irsko	k1gNnSc2	Irsko
a	a	k8xC	a
Irské	irský	k2eAgFnSc2d1	irská
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jakýmsi	jakýsi	k3yIgNnSc7	jakýsi
ozbrojeným	ozbrojený	k2eAgNnSc7d1	ozbrojené
křídlem	křídlo	k1gNnSc7	křídlo
výše	výše	k1gFnSc2	výše
uvedené	uvedený	k2eAgFnSc2d1	uvedená
politické	politický	k2eAgFnSc2d1	politická
nacionální	nacionální	k2eAgFnSc2d1	nacionální
strany	strana	k1gFnSc2	strana
Sinn	Sinn	k1gMnSc1	Sinn
Féin	Féin	k1gMnSc1	Féin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Irská	irský	k2eAgFnSc1d1	irská
republikánská	republikánský	k2eAgFnSc1d1	republikánská
armáda	armáda	k1gFnSc1	armáda
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
minulých	minulý	k2eAgNnPc6d1	Minulé
letech	léto	k1gNnPc6	léto
oficiálně	oficiálně	k6eAd1	oficiálně
odzbrojena	odzbrojen	k2eAgFnSc1d1	odzbrojen
(	(	kIx(	(
<g/>
dohoda	dohoda	k1gFnSc1	dohoda
byla	být	k5eAaImAgFnS	být
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1998	[number]	k4	1998
–	–	k?	–
prošla	projít	k5eAaPmAgFnS	projít
referendem	referendum	k1gNnSc7	referendum
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
částech	část	k1gFnPc6	část
Irska	Irsko	k1gNnSc2	Irsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dobrovolně	dobrovolně	k6eAd1	dobrovolně
se	se	k3xPyFc4	se
vzdala	vzdát	k5eAaPmAgFnS	vzdát
pod	pod	k7c7	pod
dozorem	dozor	k1gInSc7	dozor
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
pozorovatelů	pozorovatel	k1gMnPc2	pozorovatel
svého	svůj	k3xOyFgInSc2	svůj
arzenálu	arzenál	k1gInSc2	arzenál
a	a	k8xC	a
ukončila	ukončit	k5eAaPmAgFnS	ukončit
svojí	svojit	k5eAaImIp3nS	svojit
ozbrojenou	ozbrojený	k2eAgFnSc4d1	ozbrojená
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
částečně	částečně	k6eAd1	částečně
legální	legální	k2eAgFnSc7d1	legální
politickou	politický	k2eAgFnSc7d1	politická
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jejím	její	k3xOp3gInSc6	její
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
částečně	částečně	k6eAd1	částečně
stojí	stát	k5eAaImIp3nS	stát
i	i	k9	i
členství	členství	k1gNnSc1	členství
zemí	zem	k1gFnPc2	zem
v	v	k7c6	v
EU	EU	kA	EU
<g/>
,	,	kIx,	,
rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
propouštění	propouštění	k1gNnSc1	propouštění
vězňů	vězeň	k1gMnPc2	vězeň
a	a	k8xC	a
příslib	příslib	k1gInSc4	příslib
ústavních	ústavní	k2eAgFnPc2d1	ústavní
změn	změna	k1gFnPc2	změna
(	(	kIx(	(
<g/>
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
podepsání	podepsání	k1gNnSc3	podepsání
dohody	dohoda	k1gFnSc2	dohoda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zaručuje	zaručovat	k5eAaImIp3nS	zaručovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jakmile	jakmile	k8xS	jakmile
vyjádří	vyjádřit	k5eAaPmIp3nS	vyjádřit
nadpoloviční	nadpoloviční	k2eAgFnSc1d1	nadpoloviční
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
Severního	severní	k2eAgNnSc2d1	severní
Irska	Irsko	k1gNnSc2	Irsko
touhu	touha	k1gFnSc4	touha
připojit	připojit	k5eAaPmF	připojit
se	se	k3xPyFc4	se
k	k	k7c3	k
Irské	irský	k2eAgFnSc3d1	irská
republice	republika	k1gFnSc3	republika
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
tak	tak	k6eAd1	tak
učiněno	učinit	k5eAaImNgNnS	učinit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
republikánů	republikán	k1gMnPc2	republikán
se	se	k3xPyFc4	se
z	z	k7c2	z
výsledky	výsledek	k1gInPc7	výsledek
Velkopáteční	velkopáteční	k2eAgFnSc2d1	velkopáteční
dohody	dohoda	k1gFnSc2	dohoda
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
neztotožnila	ztotožnit	k5eNaPmAgFnS	ztotožnit
a	a	k8xC	a
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
v	v	k7c6	v
ozbrojeném	ozbrojený	k2eAgInSc6d1	ozbrojený
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
britským	britský	k2eAgFnPc3d1	britská
silám	síla	k1gFnPc3	síla
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
dvou	dva	k4xCgFnPc2	dva
odštěpeneckých	odštěpenecký	k2eAgFnPc2d1	odštěpenecká
organizací	organizace	k1gFnPc2	organizace
<g/>
;	;	kIx,	;
Pokračující	pokračující	k2eAgFnSc2d1	pokračující
a	a	k8xC	a
Pravé	pravá	k1gFnSc2	pravá
IRA	Ir	k1gMnSc2	Ir
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Irská	irský	k2eAgFnSc1d1	irská
republikánská	republikánský	k2eAgFnSc1d1	republikánská
armáda	armáda	k1gFnSc1	armáda
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Irish	Irish	k1gMnSc1	Irish
Republican	Republican	k1gMnSc1	Republican
Army	Arma	k1gFnSc2	Arma
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Tim	Tim	k?	Tim
Pat	pat	k1gInSc1	pat
Coogan	Coogan	k1gInSc1	Coogan
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
Collins	Collins	k1gInSc1	Collins
(	(	kIx(	(
<g/>
Hutchinson	Hutchinson	k1gNnSc1	Hutchinson
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
ISBN	ISBN	kA	ISBN
0-09-174106-8	[number]	k4	0-09-174106-8
</s>
</p>
<p>
<s>
Tim	Tim	k?	Tim
Pat	pat	k1gInSc1	pat
Coogan	Coogan	k1gInSc1	Coogan
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Troubles	Troubles	k1gMnSc1	Troubles
(	(	kIx(	(
<g/>
Arrow	Arrow	k1gMnSc1	Arrow
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
ISBN	ISBN	kA	ISBN
1-57098-092-6	[number]	k4	1-57098-092-6
</s>
</p>
<p>
<s>
Tim	Tim	k?	Tim
Pat	pat	k1gInSc1	pat
Coogan	Coogan	k1gInSc1	Coogan
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
I.	I.	kA	I.
<g/>
R.	R.	kA	R.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0-00-653155-5	[number]	k4	0-00-653155-5
</s>
</p>
<p>
<s>
F.	F.	kA	F.
<g/>
S.	S.	kA	S.
<g/>
L.	L.	kA	L.
Lyons	Lyons	k1gInSc1	Lyons
<g/>
,	,	kIx,	,
Ireland	Ireland	k1gInSc1	Ireland
Since	Since	k1gFnSc2	Since
the	the	k?	the
Famine	Famin	k1gInSc5	Famin
</s>
</p>
<p>
<s>
Dorothy	Dorotha	k1gFnPc1	Dorotha
MacCardle	MacCardle	k1gFnSc2	MacCardle
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Irish	Irish	k1gMnSc1	Irish
Republic	Republice	k1gFnPc2	Republice
(	(	kIx(	(
<g/>
Corgi	Corgi	k1gNnSc1	Corgi
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
ISBN	ISBN	kA	ISBN
0-552-07862-X	[number]	k4	0-552-07862-X
</s>
</p>
<p>
<s>
Aengus	Aengus	k1gMnSc1	Aengus
Ó	Ó	kA	Ó
Snodaigh	Snodaigh	k1gMnSc1	Snodaigh
<g/>
,	,	kIx,	,
IRA	Ir	k1gMnSc2	Ir
Convention	Convention	k1gInSc1	Convention
meets	meets	k6eAd1	meets
<g/>
,	,	kIx,	,
An	An	k1gMnSc1	An
Phoblacht	Phoblacht	k1gMnSc1	Phoblacht
<g/>
/	/	kIx~	/
<g/>
Republican	Republican	k1gInSc1	Republican
News	Newsa	k1gFnPc2	Newsa
<g/>
,	,	kIx,	,
11	[number]	k4	11
May	May	k1gMnSc1	May
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Seamus	Seamus	k1gInSc1	Seamus
Fox	fox	k1gInSc1	fox
<g/>
,	,	kIx,	,
Chronology	chronolog	k1gMnPc4	chronolog
of	of	k?	of
Irish	Irish	k1gMnSc1	Irish
History	Histor	k1gInPc1	Histor
1919	[number]	k4	1919
<g/>
-	-	kIx~	-
<g/>
1923	[number]	k4	1923
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Brian	Brian	k1gMnSc1	Brian
Dooley	Doolea	k1gFnSc2	Doolea
<g/>
,	,	kIx,	,
Black	Black	k1gMnSc1	Black
and	and	k?	and
Green	Green	k1gInSc1	Green
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Fight	Fighta	k1gFnPc2	Fighta
for	forum	k1gNnPc2	forum
Civil	civil	k1gMnSc1	civil
Rights	Rights	k1gInSc1	Rights
in	in	k?	in
Northern	Northern	k1gInSc1	Northern
Ireland	Ireland	k1gInSc1	Ireland
and	and	k?	and
Black	Black	k1gMnSc1	Black
America	America	k1gMnSc1	America
(	(	kIx(	(
<g/>
London	London	k1gMnSc1	London
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Michael	Michael	k1gMnSc1	Michael
Hopkinson	Hopkinson	k1gMnSc1	Hopkinson
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Irish	Irish	k1gMnSc1	Irish
War	War	k1gMnSc1	War
of	of	k?	of
Independence	Independence	k1gFnSc2	Independence
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Ernie	Ernie	k1gFnSc1	Ernie
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Malley	Mallea	k1gMnSc2	Mallea
<g/>
,	,	kIx,	,
On	on	k3xPp3gMnSc1	on
Another	Anothra	k1gFnPc2	Anothra
Man	mana	k1gFnPc2	mana
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Wound	Wound	k1gInSc1	Wound
</s>
</p>
<p>
<s>
ME	ME	kA	ME
Collins	Collins	k1gInSc1	Collins
<g/>
,	,	kIx,	,
Ireland	Ireland	k1gInSc1	Ireland
1868-1966	[number]	k4	1868-1966
</s>
</p>
<p>
<s>
Meda	Meda	k1gMnSc1	Meda
Ryan	Ryan	k1gMnSc1	Ryan
<g/>
,	,	kIx,	,
Liam	Liam	k1gMnSc1	Liam
Lynch	Lynch	k1gMnSc1	Lynch
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Real	Real	k1gInSc1	Real
Chief	Chief	k1gMnSc1	Chief
</s>
</p>
<p>
<s>
Tom	Tom	k1gMnSc1	Tom
Barry	Barra	k1gFnSc2	Barra
<g/>
,	,	kIx,	,
Guerrilla	Guerrilla	k1gMnSc1	Guerrilla
Days	Daysa	k1gFnPc2	Daysa
in	in	k?	in
Ireland	Irelanda	k1gFnPc2	Irelanda
</s>
</p>
<p>
<s>
T.	T.	kA	T.
Ryle	Ryle	k1gFnSc1	Ryle
Dwyer	Dwyer	k1gMnSc1	Dwyer
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Squad	Squad	k1gInSc1	Squad
and	and	k?	and
the	the	k?	the
intelligence	intelligenec	k1gInSc2	intelligenec
operations	operationsa	k1gFnPc2	operationsa
of	of	k?	of
Michael	Michael	k1gMnSc1	Michael
Collins	Collinsa	k1gFnPc2	Collinsa
</s>
</p>
