<p>
<s>
Tento	tento	k3xDgInSc1	tento
článek	článek	k1gInSc1	článek
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
podrobnosti	podrobnost	k1gFnPc4	podrobnost
o	o	k7c6	o
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
africké	africký	k2eAgFnSc2d1	africká
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
od	od	k7c2	od
června	červen	k1gInSc2	červen
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Formát	formát	k1gInSc1	formát
==	==	k?	==
</s>
</p>
<p>
<s>
Této	tento	k3xDgFnSc2	tento
fáze	fáze	k1gFnSc2	fáze
se	se	k3xPyFc4	se
účastnilo	účastnit	k5eAaImAgNnS	účastnit
47	[number]	k4	47
týmů	tým	k1gInPc2	tým
sdružených	sdružený	k2eAgInPc2d1	sdružený
pod	pod	k7c4	pod
CAF	CAF	kA	CAF
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
losováním	losování	k1gNnSc7	losování
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2007	[number]	k4	2007
-	-	kIx~	-
Durban	Durban	k1gInSc1	Durban
<g/>
,	,	kIx,	,
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
zařazeny	zařadit	k5eAaPmNgInP	zařadit
podle	podle	k7c2	podle
výkonnosti	výkonnost	k1gFnSc2	výkonnost
do	do	k7c2	do
dvanácti	dvanáct	k4xCc2	dvanáct
skupin	skupina	k1gFnPc2	skupina
po	po	k7c4	po
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Kvalifikace	kvalifikace	k1gFnSc1	kvalifikace
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
stádiu	stádium	k1gNnSc6	stádium
probíhá	probíhat	k5eAaImIp3nS	probíhat
na	na	k7c6	na
principu	princip	k1gInSc6	princip
domácích	domácí	k2eAgInPc2d1	domácí
a	a	k8xC	a
venkovních	venkovní	k2eAgInPc2d1	venkovní
zápasů	zápas	k1gInPc2	zápas
<g/>
,	,	kIx,	,
systémem	systém	k1gInSc7	systém
každého	každý	k3xTgMnSc4	každý
z	z	k7c2	z
každým	každý	k3xTgMnSc7	každý
<g/>
.	.	kIx.	.
12	[number]	k4	12
vítězů	vítěz	k1gMnPc2	vítěz
skupin	skupina	k1gFnPc2	skupina
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
8	[number]	k4	8
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
z	z	k7c2	z
druhých	druhý	k4xOgNnPc2	druhý
míst	místo	k1gNnPc2	místo
postupuje	postupovat	k5eAaImIp3nS	postupovat
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
fáze	fáze	k1gFnSc2	fáze
<g/>
,	,	kIx,	,
konané	konaný	k2eAgFnSc6d1	konaná
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
kvalifikace	kvalifikace	k1gFnSc1	kvalifikace
byla	být	k5eAaImAgFnS	být
zároveň	zároveň	k6eAd1	zároveň
kvalifikací	kvalifikace	k1gFnSc7	kvalifikace
pro	pro	k7c4	pro
Africký	africký	k2eAgInSc4d1	africký
pohár	pohár	k1gInSc4	pohár
národů	národ	k1gInPc2	národ
2010	[number]	k4	2010
v	v	k7c6	v
Angole	Angola	k1gFnSc6	Angola
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
účastnil	účastnit	k5eAaImAgMnS	účastnit
jak	jak	k6eAd1	jak
tým	tým	k1gInSc4	tým
Angoly	Angola	k1gFnSc2	Angola
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
Jihoafrické	jihoafrický	k2eAgFnSc2d1	Jihoafrická
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
původních	původní	k2eAgMnPc2d1	původní
48	[number]	k4	48
účastníků	účastník	k1gMnPc2	účastník
již	již	k6eAd1	již
v	v	k7c6	v
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2008	[number]	k4	2008
vypadla	vypadnout	k5eAaPmAgFnS	vypadnout
Eritrea	Eritrea	k1gFnSc1	Eritrea
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
účasti	účast	k1gFnSc2	účast
vzdala	vzdát	k5eAaPmAgFnS	vzdát
<g/>
,	,	kIx,	,
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Středoafrická	středoafrický	k2eAgFnSc1d1	Středoafrická
republika	republika	k1gFnSc1	republika
a	a	k8xC	a
Svatý	svatý	k2eAgMnSc1d1	svatý
Tomáš	Tomáš	k1gMnSc1	Tomáš
a	a	k8xC	a
Princův	princův	k2eAgInSc1d1	princův
ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
před	před	k7c7	před
odehráním	odehrání	k1gNnSc7	odehrání
předkola	předkolo	k1gNnSc2	předkolo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dodatečná	dodatečný	k2eAgNnPc1d1	dodatečné
opatření	opatření	k1gNnPc1	opatření
==	==	k?	==
</s>
</p>
<p>
<s>
Díky	díky	k7c3	díky
odstoupení	odstoupení	k1gNnSc3	odstoupení
Eritrey	Eritrea	k1gFnSc2	Eritrea
z	z	k7c2	z
bojů	boj	k1gInPc2	boj
se	se	k3xPyFc4	se
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
11	[number]	k4	11
objevil	objevit	k5eAaPmAgMnS	objevit
problém	problém	k1gInSc4	problém
s	s	k7c7	s
body	bod	k1gInPc7	bod
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
započítávaly	započítávat	k5eAaImAgInP	započítávat
do	do	k7c2	do
tabulky	tabulka	k1gFnSc2	tabulka
týmů	tým	k1gInPc2	tým
na	na	k7c6	na
druhých	druhý	k4xOgNnPc6	druhý
místech	místo	k1gNnPc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Osm	osm	k4xCc1	osm
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
totiž	totiž	k9	totiž
postoupilo	postoupit	k5eAaPmAgNnS	postoupit
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
fáze	fáze	k1gFnSc2	fáze
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
<g/>
.	.	kIx.	.
</s>
<s>
Týmy	tým	k1gInPc1	tým
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
skupině	skupina	k1gFnSc6	skupina
tak	tak	k9	tak
mělo	mít	k5eAaImAgNnS	mít
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
zápasy	zápas	k1gInPc4	zápas
méně	málo	k6eAd2	málo
než	než	k8xS	než
ostatní	ostatní	k2eAgMnPc1d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
byl	být	k5eAaImAgInS	být
vyřešen	vyřešit	k5eAaPmNgInS	vyřešit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
druhých	druhý	k4xOgMnPc2	druhý
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
nepočítaly	počítat	k5eNaImAgInP	počítat
zápasy	zápas	k1gInPc1	zápas
s	s	k7c7	s
posledními	poslední	k2eAgInPc7d1	poslední
týmy	tým	k1gInPc7	tým
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kvůli	kvůli	k7c3	kvůli
napjaté	napjatý	k2eAgFnSc3d1	napjatá
politické	politický	k2eAgFnSc3d1	politická
situaci	situace	k1gFnSc3	situace
mezi	mezi	k7c7	mezi
Súdánem	Súdán	k1gInSc7	Súdán
a	a	k8xC	a
Čadem	Čad	k1gInSc7	Čad
se	se	k3xPyFc4	se
FIFA	FIFA	kA	FIFA
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
odložit	odložit	k5eAaPmF	odložit
vzájemná	vzájemný	k2eAgNnPc4d1	vzájemné
utkání	utkání	k1gNnPc4	utkání
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
toto	tento	k3xDgNnSc4	tento
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
zrušila	zrušit	k5eAaPmAgFnS	zrušit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Togo	Togo	k1gNnSc1	Togo
má	mít	k5eAaImIp3nS	mít
kvůli	kvůli	k7c3	kvůli
násilnostem	násilnost	k1gFnPc3	násilnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
propukly	propuknout	k5eAaPmAgFnP	propuknout
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
utkání	utkání	k1gNnSc2	utkání
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
na	na	k7c4	na
Africký	africký	k2eAgInSc4d1	africký
pohár	pohár	k1gInSc4	pohár
národů	národ	k1gInPc2	národ
2008	[number]	k4	2008
proti	proti	k7c3	proti
Mali	Mali	k1gNnSc3	Mali
<g/>
,	,	kIx,	,
uzavřené	uzavřený	k2eAgInPc4d1	uzavřený
stadiony	stadion	k1gInPc4	stadion
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FIFA	FIFA	kA	FIFA
pohrozila	pohrozit	k5eAaPmAgFnS	pohrozit
Keni	Keňa	k1gFnSc3	Keňa
vyřazením	vyřazení	k1gNnSc7	vyřazení
z	z	k7c2	z
kvalifikačních	kvalifikační	k2eAgInPc2d1	kvalifikační
bojů	boj	k1gInPc2	boj
za	za	k7c4	za
porušení	porušení	k1gNnSc4	porušení
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
,	,	kIx,	,
týkajících	týkající	k2eAgInPc2d1	týkající
se	se	k3xPyFc4	se
vnějších	vnější	k2eAgInPc2d1	vnější
zásahů	zásah	k1gInPc2	zásah
do	do	k7c2	do
vedení	vedení	k1gNnSc2	vedení
Keňského	keňský	k2eAgInSc2d1	keňský
fotbalového	fotbalový	k2eAgInSc2d1	fotbalový
svazu	svaz	k1gInSc2	svaz
KFF	KFF	kA	KFF
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Benin	Benin	k1gInSc1	Benin
<g/>
,	,	kIx,	,
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
Kongo	Kongo	k1gNnSc1	Kongo
<g/>
,	,	kIx,	,
Gambie	Gambie	k1gFnSc1	Gambie
<g/>
,	,	kIx,	,
Republika	republika	k1gFnSc1	republika
Kongo	Kongo	k1gNnSc1	Kongo
<g/>
,	,	kIx,	,
Lesotho	Lesot	k1gMnSc2	Lesot
<g/>
,	,	kIx,	,
Mali	Mali	k1gNnSc1	Mali
<g/>
,	,	kIx,	,
Namibie	Namibie	k1gFnSc1	Namibie
<g/>
,	,	kIx,	,
Pobřeží	pobřeží	k1gNnSc1	pobřeží
slonoviny	slonovina	k1gFnSc2	slonovina
<g/>
,	,	kIx,	,
Sierra	Sierra	k1gFnSc1	Sierra
Leone	Leo	k1gMnSc5	Leo
a	a	k8xC	a
Zambie	Zambie	k1gFnPc1	Zambie
budou	být	k5eAaImBp3nP	být
muset	muset	k5eAaImF	muset
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
odehrát	odehrát	k5eAaPmF	odehrát
své	svůj	k3xOyFgInPc4	svůj
zápasy	zápas	k1gInPc4	zápas
na	na	k7c6	na
neutrální	neutrální	k2eAgFnSc6d1	neutrální
půdě	půda	k1gFnSc6	půda
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
nevyhovující	vyhovující	k2eNgInSc1d1	nevyhovující
stav	stav	k1gInSc1	stav
tamních	tamní	k2eAgNnPc2d1	tamní
sportovišť	sportoviště	k1gNnPc2	sportoviště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zápasy	zápas	k1gInPc1	zápas
Čadu	Čad	k1gInSc2	Čad
a	a	k8xC	a
Súdánu	Súdán	k1gInSc2	Súdán
byly	být	k5eAaImAgInP	být
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
konfliktu	konflikt	k1gInSc2	konflikt
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgFnPc7	tento
zeměmi	zem	k1gFnPc7	zem
odehrány	odehrát	k5eAaPmNgInP	odehrát
na	na	k7c6	na
neutrální	neutrální	k2eAgFnSc6d1	neutrální
půdě	půda	k1gFnSc6	půda
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Skupiny	skupina	k1gFnPc1	skupina
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Skupina	skupina	k1gFnSc1	skupina
1	[number]	k4	1
===	===	k?	===
</s>
</p>
