<s>
Tento	tento	k3xDgInSc1	tento
článek	článek	k1gInSc1	článek
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
podrobnosti	podrobnost	k1gFnPc4	podrobnost
o	o	k7c6	o
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
africké	africký	k2eAgFnSc2d1	africká
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
od	od	k7c2	od
června	červen	k1gInSc2	červen
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
