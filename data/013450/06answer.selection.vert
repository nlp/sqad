<s>
Endoskopie	endoskopie	k1gFnSc1	endoskopie
je	být	k5eAaImIp3nS	být
lékařská	lékařský	k2eAgFnSc1d1	lékařská
metoda	metoda	k1gFnSc1	metoda
umožňující	umožňující	k2eAgFnSc1d1	umožňující
přímé	přímý	k2eAgNnSc4d1	přímé
prohlédnutí	prohlédnutí	k1gNnSc4	prohlédnutí
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
tělesných	tělesný	k2eAgFnPc2d1	tělesná
dutin	dutina	k1gFnPc2	dutina
nebo	nebo	k8xC	nebo
orgánů	orgán	k1gInPc2	orgán
pomocí	pomocí	k7c2	pomocí
speciálního	speciální	k2eAgInSc2d1	speciální
optického	optický	k2eAgInSc2d1	optický
přístroje	přístroj	k1gInSc2	přístroj
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
endoskopu	endoskop	k1gInSc2	endoskop
<g/>
.	.	kIx.	.
</s>
