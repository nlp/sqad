<p>
<s>
Endoskopie	endoskopie	k1gFnSc1	endoskopie
je	být	k5eAaImIp3nS	být
lékařská	lékařský	k2eAgFnSc1d1	lékařská
metoda	metoda	k1gFnSc1	metoda
umožňující	umožňující	k2eAgFnSc1d1	umožňující
přímé	přímý	k2eAgNnSc4d1	přímé
prohlédnutí	prohlédnutí	k1gNnSc4	prohlédnutí
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
tělesných	tělesný	k2eAgFnPc2d1	tělesná
dutin	dutina	k1gFnPc2	dutina
nebo	nebo	k8xC	nebo
orgánů	orgán	k1gInPc2	orgán
pomocí	pomocí	k7c2	pomocí
speciálního	speciální	k2eAgInSc2d1	speciální
optického	optický	k2eAgInSc2d1	optický
přístroje	přístroj	k1gInSc2	přístroj
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
endoskopu	endoskop	k1gInSc2	endoskop
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Endoskopie	endoskopie	k1gFnSc1	endoskopie
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
velkého	velký	k2eAgNnSc2d1	velké
rozšíření	rozšíření	k1gNnSc3	rozšíření
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
řadě	řada	k1gFnSc6	řada
medicínských	medicínský	k2eAgInPc2d1	medicínský
oborů	obor	k1gInPc2	obor
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
jsou	být	k5eAaImIp3nP	být
endoskopy	endoskop	k1gInPc4	endoskop
zaváděny	zaváděn	k2eAgInPc4d1	zaváděn
přirozenými	přirozený	k2eAgInPc7d1	přirozený
otvory	otvor	k1gInPc7	otvor
a	a	k8xC	a
vedeny	vést	k5eAaImNgInP	vést
přirozenými	přirozený	k2eAgFnPc7d1	přirozená
cestami	cesta	k1gFnPc7	cesta
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
při	při	k7c6	při
vyšetření	vyšetření	k1gNnSc6	vyšetření
nedochází	docházet	k5eNaImIp3nS	docházet
k	k	k7c3	k
narušení	narušení	k1gNnSc3	narušení
orgánu	orgán	k1gInSc2	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
zavedení	zavedení	k1gNnSc1	zavedení
tak	tak	k6eAd1	tak
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
řadu	řada	k1gFnSc4	řada
vyšetření	vyšetření	k1gNnSc2	vyšetření
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
dříve	dříve	k6eAd2	dříve
vyžadovala	vyžadovat	k5eAaImAgFnS	vyžadovat
operační	operační	k2eAgInSc4d1	operační
zákrok	zákrok	k1gInSc4	zákrok
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
spektrum	spektrum	k1gNnSc4	spektrum
léčebných	léčebný	k2eAgInPc2d1	léčebný
výkonů	výkon	k1gInPc2	výkon
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
lze	lze	k6eAd1	lze
endoskopicky	endoskopicky	k6eAd1	endoskopicky
provádět	provádět	k5eAaImF	provádět
<g/>
,	,	kIx,	,
a	a	k8xC	a
pacienti	pacient	k1gMnPc1	pacient
jsou	být	k5eAaImIp3nP	být
tak	tak	k9	tak
ušetřeni	ušetřen	k2eAgMnPc1d1	ušetřen
náročnějších	náročný	k2eAgInPc2d2	náročnější
otevřených	otevřený	k2eAgInPc2d1	otevřený
chirurgických	chirurgický	k2eAgInPc2d1	chirurgický
výkonů	výkon	k1gInPc2	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
metody	metoda	k1gFnPc1	metoda
(	(	kIx(	(
<g/>
např.	např.	kA	např.
laparoskopie	laparoskopie	k1gFnSc1	laparoskopie
<g/>
,	,	kIx,	,
thorakoskopie	thorakoskopie	k1gFnSc1	thorakoskopie
<g/>
,	,	kIx,	,
artroskopie	artroskopie	k1gFnSc1	artroskopie
<g/>
)	)	kIx)	)
sice	sice	k8xC	sice
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
vytvoření	vytvoření	k1gNnSc4	vytvoření
umělého	umělý	k2eAgInSc2d1	umělý
vstupu	vstup	k1gInSc2	vstup
do	do	k7c2	do
vyšetřované	vyšetřovaný	k2eAgFnSc2d1	vyšetřovaná
dutiny	dutina	k1gFnSc2	dutina
<g/>
,	,	kIx,	,
operační	operační	k2eAgFnSc1d1	operační
rána	rána	k1gFnSc1	rána
je	být	k5eAaImIp3nS	být
však	však	k9	však
významně	významně	k6eAd1	významně
menší	malý	k2eAgFnSc1d2	menší
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
otevřenou	otevřený	k2eAgFnSc7d1	otevřená
operací	operace	k1gFnSc7	operace
a	a	k8xC	a
hojení	hojení	k1gNnSc1	hojení
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Nejjednodušší	jednoduchý	k2eAgFnPc1d3	nejjednodušší
metody	metoda	k1gFnPc1	metoda
vyšetření	vyšetření	k1gNnSc2	vyšetření
dutin	dutina	k1gFnPc2	dutina
dobře	dobře	k6eAd1	dobře
přístupných	přístupný	k2eAgNnPc2d1	přístupné
pozorování	pozorování	k1gNnPc2	pozorování
(	(	kIx(	(
<g/>
např.	např.	kA	např.
dutina	dutina	k1gFnSc1	dutina
nosní	nosní	k2eAgFnSc1d1	nosní
<g/>
)	)	kIx)	)
pomocí	pomocí	k7c2	pomocí
zrcátka	zrcátko	k1gNnSc2	zrcátko
jsou	být	k5eAaImIp3nP	být
dokumentovány	dokumentovat	k5eAaBmNgInP	dokumentovat
již	již	k6eAd1	již
od	od	k7c2	od
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
pokusy	pokus	k1gInPc1	pokus
se	s	k7c7	s
zobrazením	zobrazení	k1gNnSc7	zobrazení
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
dutých	dutý	k2eAgInPc2d1	dutý
orgánů	orgán	k1gInPc2	orgán
pochází	pocházet	k5eAaImIp3nS	pocházet
již	již	k6eAd1	již
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Přístroje	přístroj	k1gInPc1	přístroj
byly	být	k5eAaImAgInP	být
rigidní	rigidní	k2eAgInPc1d1	rigidní
<g/>
,	,	kIx,	,
tvořené	tvořený	k2eAgNnSc1d1	tvořené
pevným	pevný	k2eAgInSc7d1	pevný
tubusem	tubus	k1gInSc7	tubus
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
gastroskop	gastroskop	k1gInSc4	gastroskop
k	k	k7c3	k
vyšetření	vyšetření	k1gNnSc3	vyšetření
žaludku	žaludek	k1gInSc2	žaludek
zkonstruoval	zkonstruovat	k5eAaPmAgInS	zkonstruovat
Adolph	Adolph	k1gInSc1	Adolph
Kussmaul	Kussmaul	k1gInSc1	Kussmaul
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1868	[number]	k4	1868
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
rigidní	rigidní	k2eAgInSc4d1	rigidní
tubus	tubus	k1gInSc4	tubus
<g/>
,	,	kIx,	,
při	při	k7c6	při
jehož	jehož	k3xOyRp3gFnSc6	jehož
konstrukci	konstrukce	k1gFnSc6	konstrukce
se	se	k3xPyFc4	se
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
polykači	polykač	k1gMnPc1	polykač
mečů	meč	k1gInPc2	meč
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
obtížného	obtížný	k2eAgNnSc2d1	obtížné
zavádění	zavádění	k1gNnSc2	zavádění
byl	být	k5eAaImAgInS	být
problém	problém	k1gInSc1	problém
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
dostatečně	dostatečně	k6eAd1	dostatečně
žaludek	žaludek	k1gInSc4	žaludek
osvětlit	osvětlit	k5eAaPmF	osvětlit
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
zdroj	zdroj	k1gInSc1	zdroj
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgFnP	používat
svíčky	svíčka	k1gFnPc1	svíčka
<g/>
.	.	kIx.	.
</s>
<s>
Rigidní	rigidní	k2eAgInPc1d1	rigidní
gastroskopy	gastroskop	k1gInPc1	gastroskop
se	se	k3xPyFc4	se
příležitostně	příležitostně	k6eAd1	příležitostně
používaly	používat	k5eAaImAgFnP	používat
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgInP	být
zavedeny	zavést	k5eAaPmNgInP	zavést
také	také	k9	také
semiflexibilní	semiflexibilní	k2eAgInPc1d1	semiflexibilní
přístroje	přístroj	k1gInPc1	přístroj
obsahující	obsahující	k2eAgInSc1d1	obsahující
kloub	kloub	k1gInSc4	kloub
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
usnadňoval	usnadňovat	k5eAaImAgInS	usnadňovat
zavedení	zavedení	k1gNnSc4	zavedení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
revoluci	revoluce	k1gFnSc4	revoluce
představovalo	představovat	k5eAaImAgNnS	představovat
zkonstruování	zkonstruování	k1gNnSc1	zkonstruování
flexibilních	flexibilní	k2eAgInPc2d1	flexibilní
endoskopů	endoskop	k1gInPc2	endoskop
<g/>
.	.	kIx.	.
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
ohebnou	ohebný	k2eAgFnSc7d1	ohebná
"	"	kIx"	"
<g/>
hadicí	hadice	k1gFnSc7	hadice
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc7	jenž
obraz	obraz	k1gInSc1	obraz
je	být	k5eAaImIp3nS	být
přenášen	přenášet	k5eAaImNgInS	přenášet
soustavou	soustava	k1gFnSc7	soustava
světelných	světelný	k2eAgFnPc2d1	světelná
vláken	vlákna	k1gFnPc2	vlákna
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
jevu	jev	k1gInSc2	jev
totální	totální	k2eAgFnSc2d1	totální
reflexe	reflexe	k1gFnSc2	reflexe
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zevním	zevní	k2eAgInSc6d1	zevní
konci	konec	k1gInSc6	konec
je	být	k5eAaImIp3nS	být
vybaven	vybavit	k5eAaPmNgInS	vybavit
okulárem	okulár	k1gInSc7	okulár
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
obraz	obraz	k1gInSc1	obraz
opět	opět	k6eAd1	opět
skládá	skládat	k5eAaImIp3nS	skládat
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
takový	takový	k3xDgInSc4	takový
přístroj	přístroj	k1gInSc4	přístroj
k	k	k7c3	k
vyšetření	vyšetření	k1gNnSc3	vyšetření
jícnu	jícen	k1gInSc2	jícen
a	a	k8xC	a
žaludku	žaludek	k1gInSc2	žaludek
představil	představit	k5eAaPmAgInS	představit
Basil	Basil	k1gMnSc1	Basil
Hirschowitz	Hirschowitz	k1gMnSc1	Hirschowitz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
na	na	k7c6	na
schůzi	schůze	k1gFnSc6	schůze
Americké	americký	k2eAgFnSc2d1	americká
gastroenterologické	gastroenterologický	k2eAgFnSc2d1	gastroenterologická
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
svůj	svůj	k3xOyFgInSc4	svůj
přístroj	přístroj	k1gInSc4	přístroj
zdokonalil	zdokonalit	k5eAaPmAgMnS	zdokonalit
<g/>
,	,	kIx,	,
když	když	k8xS	když
jej	on	k3xPp3gMnSc4	on
obohatil	obohatit	k5eAaPmAgMnS	obohatit
o	o	k7c4	o
zdroj	zdroj	k1gInSc4	zdroj
studeného	studený	k2eAgNnSc2d1	studené
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
konec	konec	k1gInSc4	konec
přístroje	přístroj	k1gInSc2	přístroj
přiváděno	přiváděn	k2eAgNnSc4d1	přiváděno
druhým	druhý	k4xOgNnSc7	druhý
svazkem	svazek	k1gInSc7	svazek
optických	optický	k2eAgNnPc2d1	optické
vláken	vlákno	k1gNnPc2	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Přístroj	přístroj	k1gInSc1	přístroj
byl	být	k5eAaImAgInS	být
také	také	k9	také
vybaven	vybavit	k5eAaPmNgInS	vybavit
kanálem	kanál	k1gInSc7	kanál
k	k	k7c3	k
odběru	odběr	k1gInSc3	odběr
vzorků	vzorek	k1gInPc2	vzorek
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
přístroj	přístroj	k1gInSc1	přístroj
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
dobře	dobře	k6eAd1	dobře
využitelný	využitelný	k2eAgInSc1d1	využitelný
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
byly	být	k5eAaImAgInP	být
konstruovány	konstruovat	k5eAaImNgInP	konstruovat
obdobné	obdobný	k2eAgInPc1d1	obdobný
přístroje	přístroj	k1gInPc1	přístroj
k	k	k7c3	k
vyšetřování	vyšetřování	k1gNnSc3	vyšetřování
jiných	jiný	k2eAgInPc2d1	jiný
dutých	dutý	k2eAgInPc2d1	dutý
orgánů	orgán	k1gInPc2	orgán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
první	první	k4xOgInPc1	první
videoendoskopy	videoendoskop	k1gInPc1	videoendoskop
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgInPc2	jenž
je	být	k5eAaImIp3nS	být
obraz	obraz	k1gInSc1	obraz
na	na	k7c6	na
konci	konec	k1gInSc6	konec
přístroje	přístroj	k1gInSc2	přístroj
snímán	snímat	k5eAaImNgInS	snímat
čipem	čip	k1gInSc7	čip
–	–	k?	–
CCD	CCD	kA	CCD
senzorem	senzor	k1gInSc7	senzor
a	a	k8xC	a
elektronicky	elektronicky	k6eAd1	elektronicky
přenášen	přenášen	k2eAgInSc1d1	přenášen
<g/>
.	.	kIx.	.
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
tak	tak	k9	tak
další	další	k2eAgNnSc4d1	další
zpracování	zpracování	k1gNnSc4	zpracování
obrazu	obraz	k1gInSc2	obraz
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
pohodlnější	pohodlný	k2eAgNnSc4d2	pohodlnější
sledování	sledování	k1gNnSc4	sledování
na	na	k7c6	na
monitoru	monitor	k1gInSc6	monitor
<g/>
.	.	kIx.	.
</s>
<s>
Usnadnil	usnadnit	k5eAaPmAgMnS	usnadnit
také	také	k9	také
archivaci	archivace	k1gFnSc4	archivace
obrazů	obraz	k1gInPc2	obraz
a	a	k8xC	a
edukaci	edukace	k1gFnSc4	edukace
personálu	personál	k1gInSc2	personál
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
tyto	tento	k3xDgInPc1	tento
přístroje	přístroj	k1gInPc1	přístroj
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
medicínských	medicínský	k2eAgInPc2d1	medicínský
oborů	obor	k1gInPc2	obor
postupně	postupně	k6eAd1	postupně
nahradily	nahradit	k5eAaPmAgFnP	nahradit
klasické	klasický	k2eAgInPc4d1	klasický
fibroskopy	fibroskop	k1gInPc4	fibroskop
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
i	i	k9	i
tyto	tento	k3xDgFnPc1	tento
se	se	k3xPyFc4	se
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
lokalizacích	lokalizace	k1gFnPc6	lokalizace
dodnes	dodnes	k6eAd1	dodnes
používají	používat	k5eAaImIp3nP	používat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgInSc1d1	další
rozvoj	rozvoj	k1gInSc1	rozvoj
endoskopie	endoskopie	k1gFnSc2	endoskopie
pak	pak	k6eAd1	pak
spočíval	spočívat	k5eAaImAgInS	spočívat
v	v	k7c6	v
rozšiřování	rozšiřování	k1gNnSc6	rozšiřování
spektra	spektrum	k1gNnSc2	spektrum
léčebných	léčebný	k2eAgInPc2d1	léčebný
zákroků	zákrok	k1gInPc2	zákrok
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
endoskopickou	endoskopický	k2eAgFnSc7d1	endoskopická
cestou	cesta	k1gFnSc7	cesta
provádět	provádět	k5eAaImF	provádět
<g/>
.	.	kIx.	.
</s>
<s>
Nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
již	již	k9	již
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
odběr	odběr	k1gInSc4	odběr
vzorků	vzorek	k1gInPc2	vzorek
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
úplné	úplný	k2eAgNnSc4d1	úplné
odstraňování	odstraňování	k1gNnSc4	odstraňování
abnormální	abnormální	k2eAgFnSc2d1	abnormální
tkáně	tkáň	k1gFnSc2	tkáň
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
polypů	polyp	k1gMnPc2	polyp
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zástavu	zástava	k1gFnSc4	zástava
krvácení	krvácení	k1gNnSc2	krvácení
<g/>
,	,	kIx,	,
vytahování	vytahování	k1gNnSc1	vytahování
cizích	cizí	k2eAgNnPc2d1	cizí
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
rozšiřování	rozšiřování	k1gNnSc1	rozšiřování
zúžených	zúžený	k2eAgNnPc2d1	zúžené
míst	místo	k1gNnPc2	místo
či	či	k8xC	či
zavádění	zavádění	k1gNnPc2	zavádění
trubiček	trubička	k1gFnPc2	trubička
zvaných	zvaný	k2eAgFnPc2d1	zvaná
stenty	stent	k1gInPc4	stent
do	do	k7c2	do
takových	takový	k3xDgNnPc2	takový
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
vytahování	vytahování	k1gNnSc1	vytahování
žlučových	žlučový	k2eAgInPc2d1	žlučový
kamenů	kámen	k1gInPc2	kámen
či	či	k8xC	či
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
dokonce	dokonce	k9	dokonce
operací	operace	k1gFnPc2	operace
dosud	dosud	k6eAd1	dosud
prováděných	prováděný	k2eAgFnPc2d1	prováděná
zcela	zcela	k6eAd1	zcela
jinými	jiný	k2eAgInPc7d1	jiný
přístupy	přístup	k1gInPc7	přístup
(	(	kIx(	(
<g/>
např.	např.	kA	např.
odstranění	odstranění	k1gNnSc1	odstranění
žlučníku	žlučník	k1gInSc2	žlučník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
pokrokem	pokrok	k1gInSc7	pokrok
bylo	být	k5eAaImAgNnS	být
spojení	spojení	k1gNnSc1	spojení
dvou	dva	k4xCgFnPc2	dva
metod	metoda	k1gFnPc2	metoda
–	–	k?	–
endoskopie	endoskopie	k1gFnSc2	endoskopie
a	a	k8xC	a
sonografie	sonografie	k1gFnSc2	sonografie
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
detailní	detailní	k2eAgNnSc4d1	detailní
ultrazvukové	ultrazvukový	k2eAgNnSc4d1	ultrazvukové
vyšetření	vyšetření	k1gNnSc4	vyšetření
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
drobnější	drobný	k2eAgInPc4d2	drobnější
výkony	výkon	k1gInPc4	výkon
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
dutého	dutý	k2eAgInSc2d1	dutý
orgánu	orgán	k1gInSc2	orgán
(	(	kIx(	(
<g/>
metoda	metoda	k1gFnSc1	metoda
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
endoskopická	endoskopický	k2eAgFnSc1d1	endoskopická
ultrasonografie	ultrasonografie	k1gFnSc1	ultrasonografie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
endoskopické	endoskopický	k2eAgFnPc1d1	endoskopická
metody	metoda	k1gFnPc1	metoda
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Vyšetření	vyšetření	k1gNnSc6	vyšetření
zažívacího	zažívací	k2eAgInSc2d1	zažívací
traktu	trakt	k1gInSc2	trakt
===	===	k?	===
</s>
</p>
<p>
<s>
gastroskopie	gastroskopie	k1gFnSc1	gastroskopie
k	k	k7c3	k
zobrazení	zobrazení	k1gNnSc3	zobrazení
jícnu	jícen	k1gInSc2	jícen
<g/>
,	,	kIx,	,
žaludku	žaludek	k1gInSc2	žaludek
<g/>
,	,	kIx,	,
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
dvanáctníku	dvanáctník	k1gInSc2	dvanáctník
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
byla	být	k5eAaImAgFnS	být
prováděna	provádět	k5eAaImNgFnS	provádět
rigidním	rigidní	k2eAgInSc7d1	rigidní
přístrojem	přístroj	k1gInSc7	přístroj
také	také	k9	také
esofagoskopie	esofagoskopie	k1gFnPc1	esofagoskopie
jako	jako	k8xC	jako
zobrazení	zobrazení	k1gNnSc1	zobrazení
čistě	čistě	k6eAd1	čistě
jícnu	jícen	k1gInSc2	jícen
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
endoskopická	endoskopický	k2eAgFnSc1d1	endoskopická
retrográdní	retrográdní	k2eAgFnSc1d1	retrográdní
cholangiopankreatografie	cholangiopankreatografie	k1gFnSc1	cholangiopankreatografie
(	(	kIx(	(
<g/>
ERCP	ERCP	kA	ERCP
<g/>
)	)	kIx)	)
–	–	k?	–
zobrazení	zobrazení	k1gNnSc4	zobrazení
žlučových	žlučový	k2eAgFnPc2d1	žlučová
cest	cesta	k1gFnPc2	cesta
a	a	k8xC	a
vývodu	vývod	k1gInSc2	vývod
slinivky	slinivka	k1gFnSc2	slinivka
břišní	břišní	k2eAgFnSc2d1	břišní
</s>
</p>
<p>
<s>
enteroskopie	enteroskopie	k1gFnSc1	enteroskopie
–	–	k?	–
vyšetření	vyšetření	k1gNnSc2	vyšetření
tenkého	tenký	k2eAgNnSc2d1	tenké
střeva	střevo	k1gNnSc2	střevo
–	–	k?	–
dnes	dnes	k6eAd1	dnes
často	často	k6eAd1	často
jako	jako	k8xC	jako
jedno-	jedno-	k?	jedno-
nebo	nebo	k8xC	nebo
dvojbalónová	dvojbalónový	k2eAgFnSc1d1	dvojbalónový
enteroskopie	enteroskopie	k1gFnSc1	enteroskopie
nebo	nebo	k8xC	nebo
kapslová	kapslový	k2eAgFnSc1d1	kapslová
enteroskopie	enteroskopie	k1gFnSc1	enteroskopie
</s>
</p>
<p>
<s>
koloskopie	koloskopie	k1gFnSc1	koloskopie
–	–	k?	–
vyšetření	vyšetření	k1gNnSc2	vyšetření
tlustého	tlustý	k2eAgNnSc2d1	tlusté
střeva	střevo	k1gNnSc2	střevo
<g/>
,	,	kIx,	,
kratší	krátký	k2eAgFnSc7d2	kratší
variantou	varianta	k1gFnSc7	varianta
prováděnou	prováděný	k2eAgFnSc4d1	prováděná
rigidním	rigidní	k2eAgInSc7d1	rigidní
přístrojem	přístroj	k1gInSc7	přístroj
k	k	k7c3	k
zobrazení	zobrazení	k1gNnSc3	zobrazení
jen	jen	k9	jen
konečníku	konečník	k1gInSc2	konečník
je	být	k5eAaImIp3nS	být
rektoskopie	rektoskopie	k1gFnSc1	rektoskopie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vyšetření	vyšetření	k1gNnSc4	vyšetření
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
cest	cesta	k1gFnPc2	cesta
===	===	k?	===
</s>
</p>
<p>
<s>
laryngoskopické	laryngoskopický	k2eAgNnSc1d1	laryngoskopický
zrcátko	zrcátko	k1gNnSc1	zrcátko
–	–	k?	–
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
metoda	metoda	k1gFnSc1	metoda
k	k	k7c3	k
prohlédnutí	prohlédnutí	k1gNnSc3	prohlédnutí
hrtanu	hrtan	k1gInSc2	hrtan
používaná	používaný	k2eAgFnSc1d1	používaná
v	v	k7c6	v
otorhinolaryngologii	otorhinolaryngologie	k1gFnSc6	otorhinolaryngologie
</s>
</p>
<p>
<s>
flexibilní	flexibilní	k2eAgFnSc1d1	flexibilní
laryngoskopie	laryngoskopie	k1gFnSc1	laryngoskopie
–	–	k?	–
vyšetření	vyšetření	k1gNnSc1	vyšetření
hrtanu	hrtan	k1gInSc2	hrtan
ohebným	ohebný	k2eAgInSc7d1	ohebný
přístrojem	přístroj	k1gInSc7	přístroj
</s>
</p>
<p>
<s>
bronchoskopie	bronchoskopie	k1gFnSc1	bronchoskopie
–	–	k?	–
vyšetření	vyšetření	k1gNnSc2	vyšetření
dolních	dolní	k2eAgFnPc2d1	dolní
cest	cesta	k1gFnPc2	cesta
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
</s>
</p>
<p>
<s>
===	===	k?	===
Vyšetření	vyšetření	k1gNnSc3	vyšetření
dutiny	dutina	k1gFnSc2	dutina
břišní	břišní	k2eAgFnSc2d1	břišní
===	===	k?	===
</s>
</p>
<p>
<s>
laparoskopie	laparoskopie	k1gFnSc1	laparoskopie
–	–	k?	–
optická	optický	k2eAgFnSc1d1	optická
vyšetřovací	vyšetřovací	k2eAgFnSc1d1	vyšetřovací
metoda	metoda	k1gFnSc1	metoda
dutiny	dutina	k1gFnSc2	dutina
břišní	břišní	k2eAgFnSc2d1	břišní
a	a	k8xC	a
jejích	její	k3xOp3gInPc2	její
orgánů	orgán	k1gInPc2	orgán
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
prohlédnutí	prohlédnutí	k1gNnSc4	prohlédnutí
dutiny	dutina	k1gFnSc2	dutina
břišní	břišní	k2eAgFnSc2d1	břišní
a	a	k8xC	a
řadu	řada	k1gFnSc4	řada
operačních	operační	k2eAgInPc2d1	operační
zákroků	zákrok	k1gInPc2	zákrok
po	po	k7c6	po
zavedení	zavedení	k1gNnSc6	zavedení
přístrojů	přístroj	k1gInPc2	přístroj
z	z	k7c2	z
několika	několik	k4yIc2	několik
vpichů	vpich	k1gInPc2	vpich
přes	přes	k7c4	přes
stěnu	stěna	k1gFnSc4	stěna
břišní	břišní	k2eAgFnSc4d1	břišní
a	a	k8xC	a
aplikaci	aplikace	k1gFnSc4	aplikace
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
prostoru	prostor	k1gInSc2	prostor
v	v	k7c6	v
jinak	jinak	k6eAd1	jinak
kolabované	kolabovaný	k2eAgFnSc6d1	kolabovaný
dutině	dutina	k1gFnSc6	dutina
břišní	břišní	k2eAgFnSc6d1	břišní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vyšetření	vyšetření	k1gNnSc2	vyšetření
močového	močový	k2eAgNnSc2d1	močové
a	a	k8xC	a
pohlavního	pohlavní	k2eAgNnSc2d1	pohlavní
ústrojí	ústrojí	k1gNnSc2	ústrojí
===	===	k?	===
</s>
</p>
<p>
<s>
kolposkopie	kolposkopie	k1gFnSc1	kolposkopie
–	–	k?	–
vyšetření	vyšetření	k1gNnSc1	vyšetření
pochvy	pochva	k1gFnSc2	pochva
a	a	k8xC	a
děložního	děložní	k2eAgInSc2d1	děložní
čípku	čípek	k1gInSc2	čípek
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
zvětšením	zvětšení	k1gNnSc7	zvětšení
<g/>
,	,	kIx,	,
umožní	umožnit	k5eAaPmIp3nP	umožnit
odhalení	odhalení	k1gNnSc4	odhalení
časných	časný	k2eAgFnPc2d1	časná
nádorových	nádorový	k2eAgFnPc2d1	nádorová
změn	změna	k1gFnPc2	změna
</s>
</p>
<p>
<s>
hysteroskopie	hysteroskopie	k1gFnSc1	hysteroskopie
–	–	k?	–
vyšetření	vyšetření	k1gNnSc1	vyšetření
dutiny	dutina	k1gFnSc2	dutina
děložní	děložní	k2eAgFnSc2d1	děložní
</s>
</p>
<p>
<s>
cystoskopie	cystoskopie	k1gFnSc1	cystoskopie
–	–	k?	–
vyšetření	vyšetření	k1gNnSc1	vyšetření
močového	močový	k2eAgInSc2d1	močový
měchýře	měchýř	k1gInSc2	měchýř
</s>
</p>
<p>
<s>
===	===	k?	===
Vyšetření	vyšetření	k1gNnSc2	vyšetření
hrudníku	hrudník	k1gInSc2	hrudník
===	===	k?	===
</s>
</p>
<p>
<s>
thorakoskopie	thorakoskopie	k1gFnSc1	thorakoskopie
–	–	k?	–
vyšetření	vyšetření	k1gNnSc2	vyšetření
hrudní	hrudní	k2eAgFnSc2d1	hrudní
dutiny	dutina	k1gFnSc2	dutina
po	po	k7c6	po
zavedení	zavedení	k1gNnSc6	zavedení
přístroje	přístroj	k1gInSc2	přístroj
přes	přes	k7c4	přes
hrudní	hrudní	k2eAgFnSc4d1	hrudní
stěnu	stěna	k1gFnSc4	stěna
a	a	k8xC	a
uměle	uměle	k6eAd1	uměle
vytvořeném	vytvořený	k2eAgInSc6d1	vytvořený
pneumothoraxu	pneumothorax	k1gInSc6	pneumothorax
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
vyšetření	vyšetření	k1gNnSc3	vyšetření
nemocí	nemoc	k1gFnPc2	nemoc
pohrudnice	pohrudnice	k1gFnSc2	pohrudnice
a	a	k8xC	a
plic	plíce	k1gFnPc2	plíce
a	a	k8xC	a
odběru	odběr	k1gInSc2	odběr
vzorků	vzorek	k1gInPc2	vzorek
</s>
</p>
<p>
<s>
mediastinoskopie	mediastinoskopie	k1gFnSc1	mediastinoskopie
–	–	k?	–
invazivní	invazivní	k2eAgFnSc1d1	invazivní
metoda	metoda	k1gFnSc1	metoda
umožňující	umožňující	k2eAgNnSc4d1	umožňující
vyšetření	vyšetření	k1gNnSc4	vyšetření
a	a	k8xC	a
odběr	odběr	k1gInSc4	odběr
uzlin	uzlina	k1gFnPc2	uzlina
z	z	k7c2	z
mezihrudí	mezihrudí	k1gNnSc2	mezihrudí
</s>
</p>
<p>
<s>
===	===	k?	===
Vyšetření	vyšetření	k1gNnSc3	vyšetření
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
hlavy	hlava	k1gFnSc2	hlava
===	===	k?	===
</s>
</p>
<p>
<s>
otoskopie	otoskopie	k1gFnSc1	otoskopie
–	–	k?	–
vyšetření	vyšetření	k1gNnSc1	vyšetření
zevního	zevní	k2eAgInSc2d1	zevní
zvukovodu	zvukovod	k1gInSc2	zvukovod
včetně	včetně	k7c2	včetně
bubínku	bubínek	k1gInSc2	bubínek
zrcátkem	zrcátko	k1gNnSc7	zrcátko
či	či	k8xC	či
ušním	ušní	k2eAgInSc7d1	ušní
mikroskopem	mikroskop	k1gInSc7	mikroskop
</s>
</p>
<p>
<s>
rhinoskopie	rhinoskopie	k1gFnSc1	rhinoskopie
–	–	k?	–
vyšetření	vyšetření	k1gNnSc1	vyšetření
dutiny	dutina	k1gFnSc2	dutina
nosní	nosní	k2eAgFnSc2d1	nosní
pomocí	pomoc	k1gFnSc7	pomoc
zrcátka	zrcátko	k1gNnSc2	zrcátko
</s>
</p>
<p>
<s>
oftalmoskopie	oftalmoskopie	k1gFnSc1	oftalmoskopie
–	–	k?	–
vyšetření	vyšetření	k1gNnSc2	vyšetření
očního	oční	k2eAgNnSc2d1	oční
pozadí	pozadí	k1gNnSc2	pozadí
</s>
</p>
<p>
<s>
===	===	k?	===
Vyšetření	vyšetření	k1gNnPc2	vyšetření
kloubů	kloub	k1gInPc2	kloub
===	===	k?	===
</s>
</p>
<p>
<s>
artroskopie	artroskopie	k1gFnSc1	artroskopie
–	–	k?	–
umožní	umožnit	k5eAaPmIp3nS	umožnit
nejen	nejen	k6eAd1	nejen
vyšetření	vyšetření	k1gNnSc1	vyšetření
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
operační	operační	k2eAgInPc4d1	operační
zákroky	zákrok	k1gInPc4	zákrok
na	na	k7c6	na
větších	veliký	k2eAgInPc6d2	veliký
kloubech	kloub	k1gInPc6	kloub
po	po	k7c6	po
zavedení	zavedení	k1gNnSc6	zavedení
endoskopu	endoskop	k1gInSc2	endoskop
do	do	k7c2	do
kloubní	kloubní	k2eAgFnSc2d1	kloubní
dutiny	dutina	k1gFnSc2	dutina
přes	přes	k7c4	přes
malou	malý	k2eAgFnSc4d1	malá
operační	operační	k2eAgFnSc4d1	operační
ránu	rána	k1gFnSc4	rána
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Doc.	doc.	kA	doc.
RNDr.	RNDr.	kA	RNDr.
Roman	Roman	k1gMnSc1	Roman
Kubínek	Kubínek	k1gMnSc1	Kubínek
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
<g/>
:	:	kIx,	:
Endoskopie	endoskopie	k1gFnSc1	endoskopie
</s>
</p>
