<s>
Země	země	k1gFnSc1	země
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
ostatní	ostatní	k2eAgFnPc1d1	ostatní
planety	planeta	k1gFnPc1	planeta
slunečního	sluneční	k2eAgInSc2d1	sluneční
systému	systém	k1gInSc2	systém
přibližně	přibližně	k6eAd1	přibližně
před	před	k7c7	před
4,6	[number]	k4	4,6
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
akrecí	akrece	k1gFnPc2	akrece
z	z	k7c2	z
pracho-plynného	pracholynný	k2eAgInSc2d1	pracho-plynný
disku	disk	k1gInSc2	disk
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
obíhal	obíhat	k5eAaImAgMnS	obíhat
kolem	kolem	k7c2	kolem
rodící	rodící	k2eAgFnSc2d1	rodící
se	se	k3xPyFc4	se
centrální	centrální	k2eAgFnSc2d1	centrální
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
