<s>
Sumgaitský	Sumgaitský	k2eAgInSc1d1
pogrom	pogrom	k1gInSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Sumgaitu	Sumgait	k1gInSc2
(	(	kIx(
<g/>
Sumqayitu	Sumqayit	k1gInSc2
<g/>
)	)	kIx)
v	v	k7c6
rámci	rámec	k1gInSc6
Ázerbájdžánské	ázerbájdžánský	k2eAgFnSc2d1
SSR	SSR	kA
<g/>
.	.	kIx.
</s>
<s>
Protesty	protest	k1gInPc1
v	v	k7c6
ulicích	ulice	k1gFnPc6
Jerevanu	Jerevan	k1gMnSc3
<g/>
,	,	kIx,
které	který	k3yQgNnSc4,k3yRgNnSc4,k3yIgNnSc4
po	po	k7c6
masakru	masakr	k1gInSc6
následovaly	následovat	k5eAaImAgFnP
<g/>
.	.	kIx.
</s>
<s>
Sumgaitský	Sumgaitský	k2eAgInSc1d1
pogrom	pogrom	k1gInSc1
<g/>
,	,	kIx,
též	též	k9
Sumgaitský	Sumgaitský	k2eAgInSc1d1
masakr	masakr	k1gInSc1
byl	být	k5eAaImAgInS
Ázery	Ázera	k1gFnSc2
vedený	vedený	k2eAgInSc1d1
pogrom	pogrom	k1gInSc1
zacílený	zacílený	k2eAgInSc1d1
na	na	k7c4
arménskou	arménský	k2eAgFnSc4d1
populaci	populace	k1gFnSc4
v	v	k7c6
přímořském	přímořský	k2eAgNnSc6d1
městě	město	k1gNnSc6
Sumqayı	Sumqayı	k1gFnPc2
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
Sumgait	Sumgait	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zhruba	zhruba	k6eAd1
30	#num#	k4
km	km	kA
severně	severně	k6eAd1
od	od	k7c2
Baku	Baku	k1gNnSc2
<g/>
,	,	kIx,
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
tehdejší	tehdejší	k2eAgFnSc2d1
Ázerbájdžánské	ázerbájdžánský	k2eAgFnSc2d1
SSR	SSR	kA
<g/>
,	,	kIx,
koncem	koncem	k7c2
února	únor	k1gInSc2
1988	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
tří	tři	k4xCgInPc2
dnů	den	k1gInPc2
nepokojů	nepokoj	k1gInPc2
bylo	být	k5eAaImAgNnS
zabito	zabít	k5eAaPmNgNnS
dle	dle	k7c2
oficiálních	oficiální	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
32	#num#	k4
lidí	člověk	k1gMnPc2
a	a	k8xC
zhruba	zhruba	k6eAd1
2000	#num#	k4
bylo	být	k5eAaImAgNnS
zraněno	zranit	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neoficiální	neoficiální	k2eAgInPc1d1,k2eNgInPc1d1
zdroje	zdroj	k1gInPc1
hovoří	hovořit	k5eAaImIp3nP
však	však	k9
přinejmenším	přinejmenším	k6eAd1
o	o	k7c4
200	#num#	k4
mrtvých	mrtvý	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Průběh	průběh	k1gInSc4
a	a	k8xC
následky	následek	k1gInPc4
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1988	#num#	k4
se	se	k3xPyFc4
velký	velký	k2eAgInSc1d1
dav	dav	k1gInSc1
<g/>
,	,	kIx,
z	z	k7c2
většiny	většina	k1gFnSc2
tvořený	tvořený	k2eAgInSc4d1
Ázery	Ázer	k1gInPc4
<g/>
,	,	kIx,
zformoval	zformovat	k5eAaPmAgMnS
do	do	k7c2
skupin	skupina	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
začaly	začít	k5eAaPmAgFnP
následně	následně	k6eAd1
útočit	útočit	k5eAaImF
a	a	k8xC
zabíjet	zabíjet	k5eAaImF
Armény	Armén	k1gMnPc4
v	v	k7c6
blízkých	blízký	k2eAgFnPc6d1
ulicích	ulice	k1gFnPc6
a	a	k8xC
domech	dům	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Situace	situace	k1gFnSc1
se	se	k3xPyFc4
ještě	ještě	k9
zhoršila	zhoršit	k5eAaPmAgFnS
následným	následný	k2eAgNnSc7d1
rabováním	rabování	k1gNnSc7
a	a	k8xC
nezájmem	nezájem	k1gInSc7
policie	policie	k1gFnSc2
tuto	tento	k3xDgFnSc4
situaci	situace	k1gFnSc4
řešit	řešit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Násilnosti	násilnost	k1gFnSc2
v	v	k7c6
Sumgaitu	Sumgait	k1gInSc6
představovaly	představovat	k5eAaImAgInP
na	na	k7c4
poměry	poměra	k1gFnPc4
tehdejšího	tehdejší	k2eAgMnSc2d1
SSSR	SSSR	kA
situaci	situace	k1gFnSc3
nebývalého	bývalý	k2eNgInSc2d1,k2eAgInSc2d1
rozsahu	rozsah	k1gInSc2
a	a	k8xC
přilákaly	přilákat	k5eAaPmAgFnP
velký	velký	k2eAgInSc4d1
zájem	zájem	k1gInSc4
západních	západní	k2eAgNnPc2d1
médií	médium	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
masakru	masakr	k1gInSc2
bylo	být	k5eAaImAgNnS
obviňováno	obviňovat	k5eAaImNgNnS
náhornokarabašské	náhornokarabašský	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
začínalo	začínat	k5eAaImAgNnS
být	být	k5eAaImF
v	v	k7c6
povědomí	povědomí	k1gNnSc6
v	v	k7c6
sousední	sousední	k2eAgFnSc6d1
Arménské	arménský	k2eAgFnSc6d1
SSR	SSR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oficiální	oficiální	k2eAgFnSc1d1
bilance	bilance	k1gFnSc1
masakru	masakr	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c6
základě	základ	k1gInSc6
seznamu	seznam	k1gInSc2
jmen	jméno	k1gNnPc2
obětí	oběť	k1gFnPc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
32	#num#	k4
lidí	člověk	k1gMnPc2
(	(	kIx(
<g/>
26	#num#	k4
Arménů	Armén	k1gMnPc2
a	a	k8xC
6	#num#	k4
Ázerů	Ázer	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
očití	očitý	k2eAgMnPc1d1
svědci	svědek	k1gMnPc1
hovoří	hovořit	k5eAaImIp3nP
o	o	k7c6
mnohem	mnohem	k6eAd1
větším	veliký	k2eAgInSc6d2
počtu	počet	k1gInSc6
mrtvých	mrtvý	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Mnozí	mnohý	k2eAgMnPc1d1
z	z	k7c2
nich	on	k3xPp3gMnPc2
zastvávají	zastvávat	k5eAaImIp3nP,k5eAaPmIp3nP
tvrzení	tvrzení	k1gNnSc4
<g/>
,	,	kIx,
že	že	k8xS
mrtvých	mrtvý	k1gMnPc2
bylo	být	k5eAaImAgNnS
přinejmenším	přinejmenším	k6eAd1
200	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
vstoupil	vstoupit	k5eAaPmAgMnS
do	do	k7c2
města	město	k1gNnSc2
malý	malý	k2eAgInSc4d1
kontingent	kontingent	k1gInSc4
lehce	lehko	k6eAd1
vyzbrojených	vyzbrojený	k2eAgMnPc2d1
sovětských	sovětský	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
a	a	k8xC
neúspěšně	úspěšně	k6eNd1
se	se	k3xPyFc4
pokusil	pokusit	k5eAaPmAgMnS
potlačit	potlačit	k5eAaPmF
nepokoje	nepokoj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Situace	situace	k1gFnSc1
se	se	k3xPyFc4
zmírnila	zmírnit	k5eAaPmAgFnS
až	až	k9
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
se	se	k3xPyFc4
o	o	k7c4
den	den	k1gInSc4
později	pozdě	k6eAd2
dostavily	dostavit	k5eAaPmAgFnP
více	hodně	k6eAd2
profesionální	profesionální	k2eAgFnPc1d1
vojenské	vojenský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
s	s	k7c7
tanky	tank	k1gInPc7
a	a	k8xC
dalšími	další	k2eAgNnPc7d1
obrněnými	obrněný	k2eAgNnPc7d1
vozidly	vozidlo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
vládní	vládní	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
zavedly	zavést	k5eAaPmAgFnP
v	v	k7c6
Sumgaitu	Sumgait	k1gInSc6
stanné	stanný	k2eAgNnSc1d1
právo	právo	k1gNnSc1
<g/>
,	,	kIx,
představující	představující	k2eAgInSc1d1
zákaz	zákaz	k1gInSc1
vycházení	vycházení	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
fakticky	fakticky	k6eAd1
tak	tak	k6eAd1
ukončily	ukončit	k5eAaPmAgInP
krizi	krize	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Tehdejší	tehdejší	k2eAgMnSc1d1
generální	generální	k2eAgMnSc1d1
tajemník	tajemník	k1gMnSc1
ÚV	ÚV	kA
KSSS	KSSS	kA
Michail	Michail	k1gMnSc1
Gorbačov	Gorbačov	k1gInSc4
byl	být	k5eAaImAgMnS
později	pozdě	k6eAd2
kritizován	kritizovat	k5eAaImNgMnS
za	za	k7c4
svou	svůj	k3xOyFgFnSc4
pomalou	pomalý	k2eAgFnSc4d1
reakci	reakce	k1gFnSc4
na	na	k7c4
tuto	tento	k3xDgFnSc4
situaci	situace	k1gFnSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
později	pozdě	k6eAd2
stalo	stát	k5eAaPmAgNnS
důležitou	důležitý	k2eAgFnSc7d1
součástí	součást	k1gFnSc7
mnoha	mnoho	k4c2
konspiračních	konspirační	k2eAgFnPc2d1
teorií	teorie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dle	dle	k7c2
člena	člen	k1gMnSc2
politbyra	politbyro	k1gNnSc2
Ústředního	ústřední	k2eAgInSc2d1
výboru	výbor	k1gInSc2
KSSS	KSSS	kA
Alexandra	Alexandr	k1gMnSc2
Nikolajeviče	Nikolajevič	k1gMnSc2
Jakovjeva	Jakovjev	k1gMnSc2
byl	být	k5eAaImAgInS
sumgaitský	sumgaitský	k2eAgInSc1d1
pogrom	pogrom	k1gInSc1
zkonstruován	zkonstruován	k2eAgInSc1d1
agenty	agens	k1gInPc7
KGB	KGB	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Samotný	samotný	k2eAgInSc1d1
sumgaitský	sumgaitský	k2eAgInSc1d1
pogrom	pogrom	k1gInSc1
se	se	k3xPyFc4
sovětské	sovětský	k2eAgFnSc2d1
moci	moc	k1gFnSc2
podařilo	podařit	k5eAaPmAgNnS
před	před	k7c7
světovou	světový	k2eAgFnSc7d1
veřejností	veřejnost	k1gFnSc7
úspěšně	úspěšně	k6eAd1
ututlat	ututlat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následném	následný	k2eAgInSc6d1
procesu	proces	k1gInSc6
byli	být	k5eAaImAgMnP
obviněni	obvinit	k5eAaPmNgMnP
a	a	k8xC
odsouzeni	odsoudit	k5eAaPmNgMnP,k5eAaImNgMnP
tři	tři	k4xCgMnPc1
mladíci	mladík	k1gMnPc1
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
dva	dva	k4xCgMnPc4
nezletilí	zletilý	k2eNgMnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Následkem	následkem	k7c2
této	tento	k3xDgFnSc2
situace	situace	k1gFnSc2
byla	být	k5eAaImAgFnS
celá	celý	k2eAgFnSc1d1
arménská	arménský	k2eAgFnSc1d1
populace	populace	k1gFnSc1
Sumgaitu	Sumgait	k1gInSc2
nucena	nutit	k5eAaImNgFnS
odejít	odejít	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
se	se	k3xPyFc4
později	pozdě	k6eAd2
stalo	stát	k5eAaPmAgNnS
domovem	domov	k1gInSc7
mnoha	mnoho	k4c3
uprchlíků	uprchlík	k1gMnPc2
z	z	k7c2
válkou	válka	k1gFnSc7
postižených	postižený	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Incident	incident	k1gInSc1
zanechal	zanechat	k5eAaPmAgInS
mnoho	mnoho	k4c4
otázek	otázka	k1gFnPc2
jak	jak	k8xC,k8xS
v	v	k7c6
celé	celý	k2eAgFnSc6d1
Arménii	Arménie	k1gFnSc6
<g/>
,	,	kIx,
tak	tak	k6eAd1
ve	v	k7c6
zbytku	zbytek	k1gInSc6
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
se	se	k3xPyFc4
mělo	mít	k5eAaImAgNnS
za	za	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
etnické	etnický	k2eAgInPc1d1
spory	spor	k1gInPc1
v	v	k7c6
zemi	zem	k1gFnSc6
byly	být	k5eAaImAgInP
silně	silně	k6eAd1
potlačovány	potlačován	k2eAgInPc1d1
a	a	k8xC
oficiálně	oficiálně	k6eAd1
neexistovaly	existovat	k5eNaImAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
masakr	masakr	k1gInSc4
<g/>
,	,	kIx,
společně	společně	k6eAd1
s	s	k7c7
rostoucím	rostoucí	k2eAgInSc7d1
konfliktem	konflikt	k1gInSc7
v	v	k7c6
Náhorním	náhorní	k2eAgInSc6d1
Karabachu	Karabach	k1gInSc6
<g/>
,	,	kIx,
představovaly	představovat	k5eAaImAgInP
jistou	jistý	k2eAgFnSc4d1
výzvu	výzva	k1gFnSc4
k	k	k7c3
řešení	řešení	k1gNnSc3
celého	celý	k2eAgInSc2d1
problému	problém	k1gInSc2
v	v	k7c6
rámci	rámec	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
prováděných	prováděný	k2eAgFnPc2d1
<g/>
,	,	kIx,
reforem	reforma	k1gFnPc2
perestrojky	perestrojka	k1gFnSc2
Michaila	Michaila	k1gFnSc1
Gorbačova	Gorbačův	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nepokoje	nepokoj	k1gInSc2
v	v	k7c6
Sumgaitu	Sumgait	k1gInSc6
vytyčily	vytyčit	k5eAaPmAgInP
počátek	počátek	k1gInSc4
dlouhotrvajícího	dlouhotrvající	k2eAgNnSc2d1
napětí	napětí	k1gNnSc2
mezi	mezi	k7c7
Armény	Armén	k1gMnPc7
a	a	k8xC
Ázery	Ázer	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dosud	dosud	k6eAd1
nenásilné	násilný	k2eNgInPc1d1
protesty	protest	k1gInPc1
v	v	k7c6
Arménii	Arménie	k1gFnSc6
proti	proti	k7c3
porušování	porušování	k1gNnSc3
práv	právo	k1gNnPc2
Arménů	Armén	k1gMnPc2
v	v	k7c6
Náhorním	náhorní	k2eAgInSc6d1
Karabachu	Karabach	k1gInSc6
se	se	k3xPyFc4
ukázaly	ukázat	k5eAaPmAgFnP
jako	jako	k9
neúčinné	účinný	k2eNgFnPc1d1
<g/>
,	,	kIx,
a	a	k8xC
jejich	jejich	k3xOp3gMnPc1
organizátoři	organizátor	k1gMnPc1
byli	být	k5eAaImAgMnP
postupně	postupně	k6eAd1
vystřídáni	vystřídán	k2eAgMnPc1d1
partyzány	partyzána	k1gFnSc2
a	a	k8xC
vojskem	vojsko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
propukla	propuknout	k5eAaPmAgFnS
naplno	naplno	k6eAd1
válka	válka	k1gFnSc1
v	v	k7c6
Náhorním	náhorní	k2eAgInSc6d1
Karabachu	Karabach	k1gInSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
trvala	trvat	k5eAaImAgFnS
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1994	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Aleksandr	Aleksandr	k1gInSc1
Nikolajevič	Nikolajevič	k1gMnSc1
JAKOVJEV	JAKOVJEV	kA
<g/>
,	,	kIx,
Sumerki	Sumerki	k1gNnSc1
<g/>
,	,	kIx,
Moskva	Moskva	k1gFnSc1
<g/>
,	,	kIx,
Materik	Materik	k1gMnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
5-85646-097-9	5-85646-097-9	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Náhorní	náhorní	k2eAgInSc1d1
Karabach	Karabach	k1gInSc1
</s>
<s>
Náhorno-karabašská	Náhorno-karabašský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Sumgait	Sumgait	k2eAgInSc1d1
pogrom	pogrom	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
The	The	k1gMnSc1
Nagorny	Nagorna	k1gFnSc2
Karabakh	Karabakh	k1gMnSc1
conflict	conflict	k1gMnSc1
<g/>
:	:	kIx,
origins	origins	k1gInSc1
<g/>
,	,	kIx,
dynamics	dynamics	k1gInSc1
and	and	k?
misperceptions	misperceptions	k1gInSc1
<g/>
.	.	kIx.
www.c-r.org	www.c-r.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
The	The	k1gFnSc1
Washington	Washington	k1gInSc1
Post	post	k1gInSc1
<g/>
:	:	kIx,
Hate	Hate	k1gInSc1
Runs	Runs	k1gInSc1
High	High	k1gInSc1
in	in	k?
Soviet	Soviet	k1gInSc1
Union	union	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Most	most	k1gInSc1
Explosive	Explosiev	k1gFnSc2
Ethnic	Ethnice	k1gFnPc2
Feud	feudum	k1gNnPc2
<g/>
↑	↑	k?
Aleksandr	Aleksandr	k1gInSc1
Nikolajevič	Nikolajevič	k1gMnSc1
JAKOVJEV	JAKOVJEV	kA
<g/>
,	,	kIx,
Sumerki	Sumerki	k1gNnSc1
<g/>
,	,	kIx,
Moskva	Moskva	k1gFnSc1
<g/>
,	,	kIx,
Materik	Materik	k1gMnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
5	#num#	k4
<g/>
-	-	kIx~
<g/>
85646	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
97	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
5511	#num#	k4
2	#num#	k4
http://www.stuz.cz/Zpravodaje/Zpravodaj994/simon.htm	http://www.stuz.cz/Zpravodaje/Zpravodaj994/simon.htma	k1gFnPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Aslan	Aslan	k1gInSc1
İ	İ	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sumqayı	Sumqayı	k2eAgInSc1d1
—	—	k?
SSRİ	SSRİ	k2eAgInSc1d1
süqutunun	süqutunun	k1gInSc1
başlanğ	başlanğ	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bakı	Bakı	k1gFnSc1
<g/>
:	:	kIx,
Çaşı	Çaşı	k1gInSc2
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
.	.	kIx.
204	#num#	k4
sə	sə	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978-9952-27-259-8	978-9952-27-259-8	k4
(	(	kIx(
<g/>
ázerbájdžánsky	ázerbájdžánsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Aslan	Aslan	k1gInSc1
Ismayilov	Ismayilov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sumgayit	Sumgayit	k2eAgInSc1d1
—	—	k?
Beginning	Beginning	k1gInSc1
of	of	k?
the	the	k?
Collapse	Collapse	k1gFnSc2
of	of	k?
the	the	k?
USSR	USSR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Baku	Baku	k1gNnSc1
<g/>
:	:	kIx,
Çaşı	Çaşı	k1gInSc2
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
А	А	k?
И	И	k?
<g/>
.	.	kIx.
С	С	k?
—	—	k?
н	н	k?
р	р	k?
С	С	k?
<g/>
.	.	kIx.
Б	Б	k?
<g/>
:	:	kIx,
Ч	Ч	k?
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
.	.	kIx.
220	#num#	k4
с	с	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978-9952-27-276-5	978-9952-27-276-5	k4
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
У	У	k?
Д	Д	k?
№	№	k?
18	#num#	k4
<g/>
/	/	kIx~
<g/>
55461	#num#	k4
<g/>
-	-	kIx~
<g/>
88	#num#	k4
<g/>
.	.	kIx.
С	С	k?
<g/>
.	.	kIx.
1989	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
I	i	k9
<g/>
)	)	kIx)
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
У	У	k?
Д	Д	k?
№	№	k?
18	#num#	k4
<g/>
/	/	kIx~
<g/>
55461	#num#	k4
<g/>
-	-	kIx~
<g/>
88	#num#	k4
<g/>
.	.	kIx.
С	С	k?
<g/>
.	.	kIx.
1989	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
II	II	kA
<g/>
)	)	kIx)
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Hə	Hə	k2eAgFnSc1d1
Rə	Rə	k1gFnSc1
<g/>
,	,	kIx,
Cə	Cə	k2eAgFnSc1d1
Nazxanı	Nazxanı	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sumqayı	Sumqayı	k1gMnSc1
danı	danı	k1gMnSc1
tariximizdir	tariximizdir	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sumqayı	Sumqayı	k1gInSc1
şə	şə	k2eAgInSc1d1
60	#num#	k4
illik	illikum	k1gNnPc2
yubileyinə	yubileyinə	k?
hə	hə	k1gMnSc1
olunmuş	olunmuş	k?
Biblioqrafik	Biblioqrafik	k1gMnSc1
göstə	göstə	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sumqayı	Sumqayı	k1gFnSc1
<g/>
2009	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
ázerbájdžánsky	ázerbájdžánsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
sumgayit	sumgayit	k1gInSc1
<g/>
1988	#num#	k4
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
<s>
sumqait	sumqait	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
ázerbájdžánsky	ázerbájdžánsky	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Vladimir	Vladimir	k1gInSc1
Kryuchkov	Kryuchkov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hardline	Hardlin	k1gInSc5
Soviet	Soviet	k1gInSc1
Communist	Communist	k1gMnSc1
who	who	k?
became	becam	k1gInSc5
head	heada	k1gFnPc2
of	of	k?
the	the	k?
KGB	KGB	kA
and	and	k?
led	led	k1gInSc1
a	a	k8xC
failed	failed	k1gMnSc1
plot	plot	k1gInSc4
to	ten	k3xDgNnSc1
overthrow	overthrow	k?
Mikhail	Mikhail	k1gInSc1
Gorbachev	Gorbachev	k1gFnSc1
</s>
<s>
Nekompletní	kompletní	k2eNgInSc1d1
seznam	seznam	k1gInSc1
obětí	oběť	k1gFnPc2
masakru	masakr	k1gInSc2
</s>
