<p>
<s>
Expedice	expedice	k1gFnSc1	expedice
20	[number]	k4	20
byla	být	k5eAaImAgFnS	být
dvacátou	dvacátý	k4xOgFnSc7	dvacátý
expedicí	expedice	k1gFnSc7	expedice
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
vesmírné	vesmírný	k2eAgFnSc2d1	vesmírná
stanice	stanice	k1gFnSc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Expedice	expedice	k1gFnSc1	expedice
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
příletem	přílet	k1gInSc7	přílet
kosmické	kosmický	k2eAgFnSc2d1	kosmická
lodě	loď	k1gFnSc2	loď
Sojuz	Sojuz	k1gInSc1	Sojuz
TMA-15	TMA-15	k1gMnPc2	TMA-15
ke	k	k7c3	k
stanici	stanice	k1gFnSc3	stanice
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Posádka	posádka	k1gFnSc1	posádka
Sojuzu	Sojuz	k1gInSc2	Sojuz
doplnila	doplnit	k5eAaPmAgFnS	doplnit
stávající	stávající	k2eAgFnSc4d1	stávající
stálou	stálý	k2eAgFnSc4d1	stálá
posádku	posádka	k1gFnSc4	posádka
na	na	k7c4	na
ISS	ISS	kA	ISS
(	(	kIx(	(
<g/>
Expedice	expedice	k1gFnSc1	expedice
19	[number]	k4	19
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
začalo	začít	k5eAaPmAgNnS	začít
stanici	stanice	k1gFnSc4	stanice
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
obývat	obývat	k5eAaImF	obývat
šest	šest	k4xCc4	šest
kosmonautů	kosmonaut	k1gMnPc2	kosmonaut
<g/>
.	.	kIx.	.
</s>
<s>
Velitelem	velitel	k1gMnSc7	velitel
Expedice	expedice	k1gFnSc2	expedice
20	[number]	k4	20
byl	být	k5eAaImAgMnS	být
Gennadij	Gennadij	k1gMnSc1	Gennadij
Padalka	padalka	k1gFnSc1	padalka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sojuz	Sojuz	k1gInSc1	Sojuz
TMA-14	TMA-14	k1gFnSc2	TMA-14
a	a	k8xC	a
Sojuz	Sojuz	k1gInSc1	Sojuz
TMA-15	TMA-15	k1gFnSc2	TMA-15
připojené	připojený	k2eAgFnSc2d1	připojená
k	k	k7c3	k
modulu	modul	k1gInSc3	modul
Zarja	Zarj	k1gInSc2	Zarj
sloužily	sloužit	k5eAaImAgInP	sloužit
u	u	k7c2	u
ISS	ISS	kA	ISS
jako	jako	k8xC	jako
záchranné	záchranný	k2eAgFnSc2d1	záchranná
lodě	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Posádka	posádka	k1gFnSc1	posádka
==	==	k?	==
</s>
</p>
<p>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
pro	pro	k7c4	pro
tabulku	tabulka	k1gFnSc4	tabulka
<g/>
:	:	kIx,	:
NASAV	NASAV	kA	NASAV
závorkách	závorka	k1gFnPc6	závorka
je	být	k5eAaImIp3nS	být
uveden	uveden	k2eAgInSc1d1	uveden
dosavadní	dosavadní	k2eAgInSc1d1	dosavadní
počet	počet	k1gInSc1	počet
letů	let	k1gInPc2	let
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
včetně	včetně	k7c2	včetně
této	tento	k3xDgFnSc2	tento
mise	mise	k1gFnSc2	mise
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Záložní	záložní	k2eAgFnSc1d1	záložní
posádka	posádka	k1gFnSc1	posádka
==	==	k?	==
</s>
</p>
<p>
<s>
Jeffrey	Jeffrea	k1gFnPc1	Jeffrea
Williams	Williamsa	k1gFnPc2	Williamsa
<g/>
,	,	kIx,	,
velitel	velitel	k1gMnSc1	velitel
<g/>
,	,	kIx,	,
NASA	NASA	kA	NASA
</s>
</p>
<p>
<s>
Maxim	Maxim	k1gMnSc1	Maxim
Surajev	Surajev	k1gMnSc1	Surajev
<g/>
,	,	kIx,	,
palubní	palubní	k2eAgMnSc1d1	palubní
inženýr	inženýr	k1gMnSc1	inženýr
<g/>
,	,	kIx,	,
Roskosmos	Roskosmos	k1gMnSc1	Roskosmos
(	(	kIx(	(
<g/>
CPK	CPK	kA	CPK
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sóiči	Sóič	k1gFnSc3	Sóič
Noguči	Noguč	k1gFnSc3	Noguč
<g/>
,	,	kIx,	,
palubní	palubní	k2eAgMnSc1d1	palubní
inženýr	inženýr	k1gMnSc1	inženýr
<g/>
,	,	kIx,	,
JAXA	JAXA	kA	JAXA
(	(	kIx(	(
<g/>
za	za	k7c4	za
Wakatu	Wakata	k1gFnSc4	Wakata
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Timothy	Timotha	k1gFnPc1	Timotha
Creamer	Creamra	k1gFnPc2	Creamra
<g/>
,	,	kIx,	,
palubní	palubní	k2eAgMnSc1d1	palubní
inženýr	inženýr	k1gMnSc1	inženýr
<g/>
,	,	kIx,	,
NASA	NASA	kA	NASA
(	(	kIx(	(
<g/>
za	za	k7c4	za
Kopru	kopra	k1gFnSc4	kopra
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Catherine	Catherin	k1gInSc5	Catherin
Colemanová	Colemanová	k1gFnSc1	Colemanová
<g/>
,	,	kIx,	,
palubní	palubní	k2eAgMnSc1d1	palubní
inženýr	inženýr	k1gMnSc1	inženýr
<g/>
,	,	kIx,	,
NASA	NASA	kA	NASA
(	(	kIx(	(
<g/>
za	za	k7c4	za
Stottovou	Stottová	k1gFnSc4	Stottová
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dmitrij	Dmitrít	k5eAaPmRp2nS	Dmitrít
Kondraťjev	Kondraťjev	k1gMnSc5	Kondraťjev
<g/>
,	,	kIx,	,
palubní	palubní	k2eAgMnSc1d1	palubní
inženýr	inženýr	k1gMnSc1	inženýr
<g/>
,	,	kIx,	,
Roskosmos	Roskosmos	k1gMnSc1	Roskosmos
(	(	kIx(	(
<g/>
CPK	CPK	kA	CPK
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
za	za	k7c2	za
Romaněnka	Romaněnka	k1gFnSc1	Romaněnka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Christopher	Christophra	k1gFnPc2	Christophra
Hadfield	Hadfield	k1gMnSc1	Hadfield
<g/>
,	,	kIx,	,
palubní	palubní	k2eAgMnSc1d1	palubní
inženýr	inženýr	k1gMnSc1	inženýr
<g/>
,	,	kIx,	,
CSA	CSA	kA	CSA
(	(	kIx(	(
<g/>
za	za	k7c4	za
Thirska	Thirsek	k1gMnSc4	Thirsek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
André	André	k1gMnSc1	André
Kuipers	Kuipersa	k1gFnPc2	Kuipersa
<g/>
,	,	kIx,	,
palubní	palubní	k2eAgMnSc1d1	palubní
inženýr	inženýr	k1gMnSc1	inženýr
<g/>
,	,	kIx,	,
ESA	eso	k1gNnSc2	eso
(	(	kIx(	(
<g/>
za	za	k7c4	za
De	De	k?	De
Winneho	Winne	k1gMnSc4	Winne
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Detaily	detail	k1gInPc1	detail
mise	mise	k1gFnSc2	mise
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Start	start	k1gInSc1	start
a	a	k8xC	a
připojení	připojení	k1gNnSc4	připojení
k	k	k7c3	k
ISS	ISS	kA	ISS
===	===	k?	===
</s>
</p>
<p>
<s>
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
z	z	k7c2	z
kosmodromu	kosmodrom	k1gInSc2	kosmodrom
Bajkonur	Bajkonura	k1gFnPc2	Bajkonura
kosmická	kosmický	k2eAgFnSc1d1	kosmická
loď	loď	k1gFnSc1	loď
Sojuz	Sojuz	k1gInSc1	Sojuz
TMA-	TMA-	k1gFnSc1	TMA-
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
se	se	k3xPyFc4	se
Sojuz	Sojuz	k1gInSc1	Sojuz
spojil	spojit	k5eAaPmAgInS	spojit
s	s	k7c7	s
Mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
vesmírnou	vesmírný	k2eAgFnSc7d1	vesmírná
stanicí	stanice	k1gFnSc7	stanice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
kosmonauty	kosmonaut	k1gMnPc4	kosmonaut
očekávala	očekávat	k5eAaImAgFnS	očekávat
posádka	posádka	k1gFnSc1	posádka
Expedice	expedice	k1gFnSc2	expedice
19	[number]	k4	19
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
příletu	přílet	k1gInSc6	přílet
se	se	k3xPyFc4	se
označení	označení	k1gNnSc1	označení
mise	mise	k1gFnSc2	mise
změnilo	změnit	k5eAaPmAgNnS	změnit
na	na	k7c4	na
Expedice	expedice	k1gFnPc4	expedice
20	[number]	k4	20
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
stanice	stanice	k1gFnSc2	stanice
začalo	začít	k5eAaPmAgNnS	začít
ISS	ISS	kA	ISS
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
obývat	obývat	k5eAaImF	obývat
šest	šest	k4xCc4	šest
kosmonautů	kosmonaut	k1gMnPc2	kosmonaut
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
EVA-1	EVA-1	k1gMnPc5	EVA-1
===	===	k?	===
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2009	[number]	k4	2009
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
programu	program	k1gInSc6	program
výstup	výstup	k1gInSc4	výstup
do	do	k7c2	do
kosmu	kosmos	k1gInSc2	kosmos
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
příprav	příprava	k1gFnPc2	příprava
na	na	k7c4	na
připojení	připojení	k1gNnSc4	připojení
ruského	ruský	k2eAgInSc2d1	ruský
modulu	modul	k1gInSc2	modul
MRM-2	MRM-2	k1gFnSc2	MRM-2
pro	pro	k7c4	pro
připojení	připojení	k1gNnSc4	připojení
dalších	další	k2eAgFnPc2d1	další
lodí	loď	k1gFnPc2	loď
Sojuz	Sojuz	k1gInSc1	Sojuz
a	a	k8xC	a
Progress	Progress	k1gInSc1	Progress
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
výstupu	výstup	k1gInSc2	výstup
astronauti	astronaut	k1gMnPc1	astronaut
Gennadij	Gennadij	k1gFnSc1	Gennadij
Padalka	padalka	k1gFnSc1	padalka
a	a	k8xC	a
Michael	Michael	k1gMnSc1	Michael
Barratt	Barratta	k1gFnPc2	Barratta
připevnili	připevnit	k5eAaPmAgMnP	připevnit
na	na	k7c6	na
horni	horeň	k1gFnSc6	horeň
uzel	uzel	k1gInSc1	uzel
modulu	modul	k1gInSc2	modul
Zvezda	Zvezdo	k1gNnSc2	Zvezdo
setkávací	setkávací	k2eAgInSc4d1	setkávací
terč	terč	k1gInSc4	terč
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc1	dva
sestavy	sestava	k1gFnPc1	sestava
antén	anténa	k1gFnPc2	anténa
Kurs	kurs	k1gInSc1	kurs
a	a	k8xC	a
kabeláž	kabeláž	k1gFnSc1	kabeláž
pro	pro	k7c4	pro
antény	anténa	k1gFnPc4	anténa
Kurs	kurs	k1gInSc1	kurs
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
Barratt	Barratt	k1gMnSc1	Barratt
pořídil	pořídit	k5eAaPmAgMnS	pořídit
snímky	snímka	k1gFnPc4	snímka
ruské	ruský	k2eAgFnSc2d1	ruská
části	část	k1gFnSc2	část
stanice	stanice	k1gFnSc2	stanice
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
byl	být	k5eAaImAgInS	být
upoután	upoutat	k5eAaPmNgInS	upoutat
na	na	k7c6	na
konci	konec	k1gInSc6	konec
manipulátoru	manipulátor	k1gInSc2	manipulátor
Strela-	Strela-	k1gFnSc2	Strela-
<g/>
2	[number]	k4	2
ovládaného	ovládaný	k2eAgNnSc2d1	ovládané
Padalkou	padalka	k1gFnSc7	padalka
<g/>
.	.	kIx.	.
</s>
<s>
Výstup	výstup	k1gInSc1	výstup
začal	začít	k5eAaPmAgInS	začít
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
52	[number]	k4	52
UTC	UTC	kA	UTC
a	a	k8xC	a
skončil	skončit	k5eAaPmAgInS	skončit
12	[number]	k4	12
<g/>
:	:	kIx,	:
<g/>
46	[number]	k4	46
UTC	UTC	kA	UTC
<g/>
.	.	kIx.	.
</s>
<s>
Trval	trvat	k5eAaImAgMnS	trvat
4	[number]	k4	4
hodiny	hodina	k1gFnPc1	hodina
a	a	k8xC	a
54	[number]	k4	54
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
IVA-1	IVA-1	k1gMnPc5	IVA-1
===	===	k?	===
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
už	už	k6eAd1	už
napovídá	napovídat	k5eAaBmIp3nS	napovídat
název	název	k1gInSc1	název
výstupu	výstup	k1gInSc2	výstup
(	(	kIx(	(
<g/>
Intra	Intr	k1gInSc2	Intr
Vehicular	Vehicular	k1gInSc1	Vehicular
Activity	Activita	k1gFnSc2	Activita
–	–	k?	–
Aktivita	aktivita	k1gFnSc1	aktivita
ve	v	k7c6	v
stanici	stanice	k1gFnSc6	stanice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
probíhal	probíhat	k5eAaImAgInS	probíhat
zevnitř	zevnitř	k6eAd1	zevnitř
stanice	stanice	k1gFnSc2	stanice
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
v	v	k7c6	v
přechodové	přechodový	k2eAgFnSc6d1	přechodová
části	část	k1gFnSc6	část
mezi	mezi	k7c7	mezi
moduly	modul	k1gInPc7	modul
Zvezda	Zvezdo	k1gNnSc2	Zvezdo
a	a	k8xC	a
Zarja	Zarjum	k1gNnSc2	Zarjum
(	(	kIx(	(
<g/>
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
výstupu	výstup	k1gInSc2	výstup
dehermetizované	dehermetizovaný	k2eAgFnSc2d1	dehermetizovaný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proběhl	proběhnout	k5eAaPmAgMnS	proběhnout
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
a	a	k8xC	a
astronauti	astronaut	k1gMnPc1	astronaut
měli	mít	k5eAaImAgMnP	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
vyměnit	vyměnit	k5eAaPmF	vyměnit
víko	víko	k1gNnSc4	víko
horního	horní	k2eAgInSc2d1	horní
uzlu	uzel	k1gInSc2	uzel
modulu	modul	k1gInSc2	modul
Zvezda	Zvezdo	k1gNnSc2	Zvezdo
za	za	k7c4	za
víko	víko	k1gNnSc4	víko
se	s	k7c7	s
setkávacím	setkávací	k2eAgInSc7d1	setkávací
naváděcím	naváděcí	k2eAgInSc7d1	naváděcí
kuželem	kužel	k1gInSc7	kužel
pro	pro	k7c4	pro
MRM-	MRM-	k1gFnSc4	MRM-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
byly	být	k5eAaImAgFnP	být
přípravy	příprava	k1gFnPc1	příprava
na	na	k7c4	na
připojení	připojení	k1gNnSc4	připojení
MRM-2	MRM-2	k1gFnSc2	MRM-2
hotovy	hotov	k2eAgInPc1d1	hotov
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
výstup	výstup	k1gInSc1	výstup
trval	trvat	k5eAaImAgInS	trvat
pouze	pouze	k6eAd1	pouze
12	[number]	k4	12
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
se	se	k3xPyFc4	se
ho	on	k3xPp3gNnSc4	on
Padalka	padalka	k1gFnSc1	padalka
a	a	k8xC	a
Barratt	Barratt	k1gInSc1	Barratt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Progress	Progress	k1gInSc1	Progress
M-02M	M-02M	k1gFnSc2	M-02M
===	===	k?	===
</s>
</p>
<p>
<s>
Zásobovací	zásobovací	k2eAgFnSc1d1	zásobovací
loď	loď	k1gFnSc1	loď
Progress	Progressa	k1gFnPc2	Progressa
M-02M	M-02M	k1gFnPc2	M-02M
naplněná	naplněný	k2eAgFnSc1d1	naplněná
odpadem	odpad	k1gInSc7	odpad
byla	být	k5eAaImAgFnS	být
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2009	[number]	k4	2009
v	v	k7c4	v
18	[number]	k4	18
<g/>
:	:	kIx,	:
<g/>
29	[number]	k4	29
UTC	UTC	kA	UTC
od	od	k7c2	od
komplexu	komplex	k1gInSc2	komplex
odpojena	odpojen	k2eAgFnSc1d1	odpojena
a	a	k8xC	a
přešla	přejít	k5eAaPmAgFnS	přejít
na	na	k7c4	na
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
parkovací	parkovací	k2eAgFnSc4d1	parkovací
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
ke	k	k7c3	k
stanici	stanice	k1gFnSc3	stanice
znovu	znovu	k6eAd1	znovu
přiblíží	přiblížit	k5eAaPmIp3nS	přiblížit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
otestovala	otestovat	k5eAaPmAgFnS	otestovat
nový	nový	k2eAgInSc4d1	nový
naváděcí	naváděcí	k2eAgInSc4d1	naváděcí
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
na	na	k7c4	na
modul	modul	k1gInSc4	modul
Zvezda	Zvezdo	k1gNnSc2	Zvezdo
namontovali	namontovat	k5eAaPmAgMnP	namontovat
astronauti	astronaut	k1gMnPc1	astronaut
Barratt	Barratt	k1gMnSc1	Barratt
a	a	k8xC	a
Padalka	padalka	k1gFnSc1	padalka
při	při	k7c6	při
výstupu	výstup	k1gInSc6	výstup
EVA-	EVA-	k1gFnSc2	EVA-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
bude	být	k5eAaImBp3nS	být
Progress	Progress	k1gInSc1	Progress
naveden	naveden	k2eAgInSc1d1	naveden
do	do	k7c2	do
hustých	hustý	k2eAgFnPc2d1	hustá
vrstev	vrstva	k1gFnPc2	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zanikne	zaniknout	k5eAaPmIp3nS	zaniknout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Přeparkování	Přeparkování	k1gNnSc3	Přeparkování
Sojuzu	Sojuz	k1gInSc2	Sojuz
TMA-14	TMA-14	k1gFnSc2	TMA-14
===	===	k?	===
</s>
</p>
<p>
<s>
Aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
uvolnil	uvolnit	k5eAaPmAgInS	uvolnit
port	port	k1gInSc1	port
na	na	k7c6	na
modulu	modul	k1gInSc6	modul
Zvezda	Zvezdo	k1gNnSc2	Zvezdo
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yQgInSc3	který
se	se	k3xPyFc4	se
na	na	k7c6	na
konci	konec	k1gInSc6	konec
července	červenec	k1gInSc2	červenec
2009	[number]	k4	2009
připojí	připojit	k5eAaPmIp3nS	připojit
nová	nový	k2eAgFnSc1d1	nová
zásobovací	zásobovací	k2eAgFnSc1d1	zásobovací
loď	loď	k1gFnSc1	loď
Progress	Progress	k1gInSc1	Progress
M-	M-	k1gFnSc1	M-
<g/>
67	[number]	k4	67
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
nutno	nutno	k6eAd1	nutno
přeparkovat	přeparkovat	k5eAaBmF	přeparkovat
loď	loď	k1gFnSc1	loď
Sojuz	Sojuz	k1gInSc4	Sojuz
TMA-14	TMA-14	k1gFnSc2	TMA-14
na	na	k7c4	na
nové	nový	k2eAgNnSc4d1	nové
místo	místo	k1gNnSc4	místo
u	u	k7c2	u
modulu	modul	k1gInSc2	modul
Pirs	Pirs	k1gInSc1	Pirs
(	(	kIx(	(
<g/>
uvolněný	uvolněný	k2eAgInSc1d1	uvolněný
odletem	odlet	k1gInSc7	odlet
Progressu	Progress	k1gInSc2	Progress
M-	M-	k1gFnSc7	M-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
M	M	kA	M
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
manévr	manévr	k1gInSc1	manévr
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
úspěšně	úspěšně	k6eAd1	úspěšně
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2009	[number]	k4	2009
od	od	k7c2	od
21	[number]	k4	21
<g/>
:	:	kIx,	:
<g/>
29	[number]	k4	29
do	do	k7c2	do
21	[number]	k4	21
<g/>
:	:	kIx,	:
<g/>
55	[number]	k4	55
UTC	UTC	kA	UTC
<g/>
.	.	kIx.	.
</s>
<s>
Kosmickou	kosmický	k2eAgFnSc4d1	kosmická
loď	loď	k1gFnSc4	loď
řídil	řídit	k5eAaImAgMnS	řídit
Gennadij	Gennadij	k1gMnSc1	Gennadij
Padalka	padalka	k1gFnSc1	padalka
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
byli	být	k5eAaImAgMnP	být
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
Sojuzu	Sojuz	k1gInSc2	Sojuz
astronauti	astronaut	k1gMnPc1	astronaut
Michael	Michael	k1gMnSc1	Michael
Barratt	Barratt	k1gMnSc1	Barratt
a	a	k8xC	a
Kóiči	Kóič	k1gFnSc6	Kóič
Wakata	Wakat	k1gMnSc2	Wakat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
STS-127	STS-127	k1gMnPc5	STS-127
===	===	k?	===
</s>
</p>
<p>
<s>
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2009	[number]	k4	2009
odstartoval	odstartovat	k5eAaPmAgInS	odstartovat
raketoplán	raketoplán	k1gInSc1	raketoplán
Endeavour	Endeavour	k1gInSc1	Endeavour
s	s	k7c7	s
japonskou	japonský	k2eAgFnSc7d1	japonská
plošinou	plošina	k1gFnSc7	plošina
JEF	JEF	kA	JEF
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
pak	pak	k6eAd1	pak
astronauti	astronaut	k1gMnPc1	astronaut
namontovali	namontovat	k5eAaPmAgMnP	namontovat
na	na	k7c4	na
modul	modul	k1gInSc4	modul
Kibo	Kibo	k6eAd1	Kibo
<g/>
.	.	kIx.	.
</s>
<s>
Raketoplán	raketoplán	k1gInSc1	raketoplán
se	se	k3xPyFc4	se
ke	k	k7c3	k
stanici	stanice	k1gFnSc3	stanice
připojil	připojit	k5eAaPmAgInS	připojit
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
otevření	otevření	k1gNnSc6	otevření
průlezu	průlez	k1gInSc2	průlez
mezi	mezi	k7c7	mezi
raketoplánem	raketoplán	k1gInSc7	raketoplán
a	a	k8xC	a
ISS	ISS	kA	ISS
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
stanci	stance	k1gFnSc6	stance
rekordních	rekordní	k2eAgInPc2d1	rekordní
13	[number]	k4	13
astronautů	astronaut	k1gMnPc2	astronaut
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
prvních	první	k4xOgNnPc6	první
třech	tři	k4xCgNnPc6	tři
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
výstupů	výstup	k1gInPc2	výstup
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
splnit	splnit	k5eAaPmF	splnit
všechny	všechen	k3xTgInPc4	všechen
úkoly	úkol	k1gInPc4	úkol
a	a	k8xC	a
tak	tak	k6eAd1	tak
při	při	k7c6	při
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
výstupu	výstup	k1gInSc6	výstup
astronauti	astronaut	k1gMnPc1	astronaut
měli	mít	k5eAaImAgMnP	mít
dost	dost	k6eAd1	dost
práce	práce	k1gFnPc4	práce
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
všechny	všechen	k3xTgFnPc4	všechen
práce	práce	k1gFnPc4	práce
nedodělali	dodělat	k5eNaPmAgMnP	dodělat
a	a	k8xC	a
do	do	k7c2	do
programu	program	k1gInSc2	program
byl	být	k5eAaImAgInS	být
zařazen	zařadit	k5eAaPmNgInS	zařadit
další	další	k2eAgInSc1d1	další
<g/>
,	,	kIx,	,
pátý	pátý	k4xOgInSc1	pátý
výstup	výstup	k1gInSc1	výstup
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
se	se	k3xPyFc4	se
raketoplán	raketoplán	k1gInSc1	raketoplán
od	od	k7c2	od
stanice	stanice	k1gFnSc2	stanice
odpojil	odpojit	k5eAaPmAgMnS	odpojit
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
přistáním	přistání	k1gNnSc7	přistání
se	se	k3xPyFc4	se
počítá	počítat	k5eAaImIp3nS	počítat
31	[number]	k4	31
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Expedition	Expedition	k1gInSc1	Expedition
20	[number]	k4	20
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Expedice	expedice	k1gFnSc2	expedice
20	[number]	k4	20
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Stránka	stránka	k1gFnSc1	stránka
expedice	expedice	k1gFnSc2	expedice
–	–	k?	–
NASA	NASA	kA	NASA
</s>
</p>
