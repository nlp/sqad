<s>
Sir	sir	k1gMnSc1	sir
Christopher	Christophra	k1gFnPc2	Christophra
Wren	Wren	k1gMnSc1	Wren
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1632	[number]	k4	1632
<g/>
,	,	kIx,	,
East	East	k1gInSc1	East
Knoyle	Knoyl	k1gInSc5	Knoyl
<g/>
,	,	kIx,	,
Wiltshire	Wiltshir	k1gMnSc5	Wiltshir
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc2	Anglie
-	-	kIx~	-
25	[number]	k4	25
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1723	[number]	k4	1723
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
anglický	anglický	k2eAgMnSc1d1	anglický
návrhář	návrhář	k1gMnSc1	návrhář
<g/>
,	,	kIx,	,
astronom	astronom	k1gMnSc1	astronom
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejslavnějších	slavný	k2eAgMnPc2d3	nejslavnější
anglických	anglický	k2eAgMnPc2d1	anglický
architektů	architekt	k1gMnPc2	architekt
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
Wadhamovu	Wadhamův	k2eAgFnSc4d1	Wadhamův
kolej	kolej	k1gFnSc4	kolej
Oxfordské	oxfordský	k2eAgFnSc2d1	Oxfordská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Navrhl	navrhnout	k5eAaPmAgInS	navrhnout
53	[number]	k4	53
londýnských	londýnský	k2eAgInPc2d1	londýnský
kostelů	kostel	k1gInPc2	kostel
včetně	včetně	k7c2	včetně
katedrály	katedrála	k1gFnSc2	katedrála
Sv.	sv.	kA	sv.
Pavla	Pavel	k1gMnSc2	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Věnoval	věnovat	k5eAaPmAgInS	věnovat
se	se	k3xPyFc4	se
též	též	k9	též
navrhování	navrhování	k1gNnSc3	navrhování
významných	významný	k2eAgFnPc2d1	významná
světských	světský	k2eAgFnPc2d1	světská
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
zakladatelem	zakladatel	k1gMnSc7	zakladatel
Londýnské	londýnský	k2eAgFnSc2d1	londýnská
královské	královský	k2eAgFnSc2d1	královská
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
prezidentem	prezident	k1gMnSc7	prezident
1680	[number]	k4	1680
-	-	kIx~	-
1682	[number]	k4	1682
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
vědecká	vědecký	k2eAgFnSc1d1	vědecká
práce	práce	k1gFnSc1	práce
byla	být	k5eAaImAgFnS	být
oceněna	ocenit	k5eAaPmNgFnS	ocenit
i	i	k8xC	i
takovými	takový	k3xDgMnPc7	takový
vědci	vědec	k1gMnPc7	vědec
jako	jako	k8xS	jako
například	například	k6eAd1	například
sir	sir	k1gMnSc1	sir
Isaac	Isaac	k1gFnSc4	Isaac
Newton	Newton	k1gMnSc1	Newton
či	či	k8xC	či
Blaise	Blaise	k1gFnSc1	Blaise
Pascal	pascal	k1gInSc1	pascal
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
zřejmě	zřejmě	k6eAd1	zřejmě
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
zakládajících	zakládající	k2eAgFnPc2d1	zakládající
postav	postava	k1gFnPc2	postava
Ashmolean	Ashmoleana	k1gFnPc2	Ashmoleana
museum	museum	k1gNnSc4	museum
v	v	k7c6	v
Oxfordu	Oxford	k1gInSc6	Oxford
<g/>
.	.	kIx.	.
</s>
<s>
Vinou	vinou	k7c2	vinou
Velkého	velký	k2eAgInSc2d1	velký
požáru	požár	k1gInSc2	požár
Londýna	Londýn	k1gInSc2	Londýn
roku	rok	k1gInSc2	rok
1666	[number]	k4	1666
většina	většina	k1gFnSc1	většina
tehdejších	tehdejší	k2eAgFnPc2d1	tehdejší
staveb	stavba	k1gFnPc2	stavba
lehla	lehnout	k5eAaPmAgFnS	lehnout
popelem	popel	k1gInSc7	popel
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
tedy	tedy	k9	tedy
potřeba	potřeba	k1gFnSc1	potřeba
postavit	postavit	k5eAaPmF	postavit
nové	nový	k2eAgMnPc4d1	nový
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byla	být	k5eAaImAgFnS	být
také	také	k9	také
katerála	katerála	k1gFnSc1	katerála
sv.	sv.	kA	sv.
Pavla	Pavel	k1gMnSc2	Pavel
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
sice	sice	k8xC	sice
nebyla	být	k5eNaImAgFnS	být
ohněm	oheň	k1gInSc7	oheň
poničena	poničen	k2eAgFnSc1d1	poničena
tolik	tolik	k6eAd1	tolik
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nemohla	moct	k5eNaImAgFnS	moct
být	být	k5eAaImF	být
rekonstruována	rekonstruovat	k5eAaBmNgFnS	rekonstruovat
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
však	však	k9	však
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c6	o
stavbě	stavba	k1gFnSc6	stavba
katedrály	katedrála	k1gFnSc2	katedrála
nové	nový	k2eAgFnSc2d1	nová
<g/>
.	.	kIx.	.
</s>
<s>
Návrhem	návrh	k1gInSc7	návrh
na	na	k7c4	na
novou	nový	k2eAgFnSc4d1	nová
katedrálu	katedrála	k1gFnSc4	katedrála
byl	být	k5eAaImAgMnS	být
roku	rok	k1gInSc2	rok
1668	[number]	k4	1668
pověřen	pověřit	k5eAaPmNgMnS	pověřit
právě	právě	k9	právě
Christopher	Christophra	k1gFnPc2	Christophra
Wren	Wren	k1gMnSc1	Wren
<g/>
.	.	kIx.	.
</s>
<s>
Neměl	mít	k5eNaImAgInS	mít
to	ten	k3xDgNnSc1	ten
však	však	k9	však
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
-	-	kIx~	-
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
až	až	k9	až
jeho	on	k3xPp3gInSc4	on
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
návrh	návrh	k1gInSc4	návrh
<g/>
,	,	kIx,	,
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xC	jako
variantní	variantní	k2eAgInSc1d1	variantní
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
Wrenovi	Wrenův	k2eAgMnPc1d1	Wrenův
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
příliš	příliš	k6eAd1	příliš
nezamlouval	zamlouvat	k5eNaImAgMnS	zamlouvat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
i	i	k9	i
když	když	k8xS	když
byly	být	k5eAaImAgFnP	být
stavební	stavební	k2eAgFnPc1d1	stavební
práce	práce	k1gFnPc1	práce
již	již	k6eAd1	již
započaty	započnout	k5eAaPmNgFnP	započnout
<g/>
,	,	kIx,	,
vymohl	vymoct	k5eAaPmAgMnS	vymoct
si	se	k3xPyFc3	se
souhlas	souhlas	k1gInSc4	souhlas
s	s	k7c7	s
dekorativními	dekorativní	k2eAgFnPc7d1	dekorativní
úpravami	úprava	k1gFnPc7	úprava
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterých	který	k3yRgFnPc2	který
plán	plán	k1gInSc1	plán
katedrály	katedrála	k1gFnSc2	katedrála
významně	významně	k6eAd1	významně
změnil	změnit	k5eAaPmAgInS	změnit
opět	opět	k6eAd1	opět
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
návrhu	návrh	k1gInSc3	návrh
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c4	na
Ludgate	Ludgat	k1gInSc5	Ludgat
Hill	Hill	k1gMnSc1	Hill
v	v	k7c6	v
londýnském	londýnský	k2eAgNnSc6d1	Londýnské
City	city	k1gNnSc6	city
až	až	k9	až
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Christopher	Christophra	k1gFnPc2	Christophra
Wren	Wren	k1gNnSc4	Wren
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
