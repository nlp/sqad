<p>
<s>
Elektrický	elektrický	k2eAgInSc1d1	elektrický
proud	proud	k1gInSc1	proud
je	být	k5eAaImIp3nS	být
uspořádaný	uspořádaný	k2eAgInSc1d1	uspořádaný
pohyb	pohyb	k1gInSc1	pohyb
nosičů	nosič	k1gInPc2	nosič
elektrického	elektrický	k2eAgInSc2d1	elektrický
náboje	náboj	k1gInSc2	náboj
<g/>
.	.	kIx.	.
</s>
<s>
Stejnojmenná	stejnojmenný	k2eAgFnSc1d1	stejnojmenná
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
veličina	veličina	k1gFnSc1	veličina
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
označovaná	označovaný	k2eAgFnSc1d1	označovaná
I	i	k9	i
<g/>
,	,	kIx,	,
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
jednotka	jednotka	k1gFnSc1	jednotka
je	být	k5eAaImIp3nS	být
ampér	ampér	k1gInSc1	ampér
(	(	kIx(	(
<g/>
A	A	kA	A
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
množství	množství	k1gNnSc1	množství
elektrického	elektrický	k2eAgInSc2d1	elektrický
náboje	náboj	k1gInSc2	náboj
prošlého	prošlý	k2eAgMnSc4d1	prošlý
za	za	k7c4	za
jednotku	jednotka	k1gFnSc4	jednotka
času	čas	k1gInSc2	čas
daným	daný	k2eAgInSc7d1	daný
průřezem	průřez	k1gInSc7	průřez
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
úvahách	úvaha	k1gFnPc6	úvaha
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
dohodnutý	dohodnutý	k2eAgInSc1d1	dohodnutý
směr	směr	k1gInSc1	směr
toku	tok	k1gInSc2	tok
proudu	proud	k1gInSc2	proud
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
kladného	kladný	k2eAgInSc2d1	kladný
pólu	pól	k1gInSc2	pól
zdroje	zdroj	k1gInSc2	zdroj
přes	přes	k7c4	přes
spotřebič	spotřebič	k1gInSc4	spotřebič
k	k	k7c3	k
zápornému	záporný	k2eAgInSc3d1	záporný
pólu	pól	k1gInSc3	pól
zdroje	zdroj	k1gInSc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
dohodnutý	dohodnutý	k2eAgInSc1d1	dohodnutý
směr	směr	k1gInSc1	směr
je	být	k5eAaImIp3nS	být
opačný	opačný	k2eAgInSc1d1	opačný
než	než	k8xS	než
skutečný	skutečný	k2eAgInSc1d1	skutečný
směr	směr	k1gInSc1	směr
toku	tok	k1gInSc2	tok
elektronů	elektron	k1gInPc2	elektron
ve	v	k7c6	v
vodiči	vodič	k1gInSc6	vodič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Proud	proud	k1gInSc1	proud
v	v	k7c6	v
elektrických	elektrický	k2eAgInPc6d1	elektrický
rozvodech	rozvod	k1gInPc6	rozvod
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
stejnosměrný	stejnosměrný	k2eAgMnSc1d1	stejnosměrný
(	(	kIx(	(
<g/>
značí	značit	k5eAaImIp3nS	značit
se	se	k3xPyFc4	se
ss	ss	k?	ss
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
DC	DC	kA	DC
–	–	k?	–
direct	direct	k2eAgInSc4d1	direct
current	current	k1gInSc4	current
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
střídavý	střídavý	k2eAgMnSc1d1	střídavý
(	(	kIx(	(
<g/>
značí	značit	k5eAaImIp3nS	značit
se	se	k3xPyFc4	se
stř	stř	k?	stř
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
AC	AC	kA	AC
–	–	k?	–
alternating	alternating	k1gInSc1	alternating
current	current	k1gInSc1	current
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
směr	směr	k1gInSc1	směr
toku	tok	k1gInSc2	tok
i	i	k9	i
okamžitá	okamžitý	k2eAgFnSc1d1	okamžitá
velikost	velikost	k1gFnSc1	velikost
se	se	k3xPyFc4	se
v	v	k7c6	v
čase	čas	k1gInSc6	čas
cyklicky	cyklicky	k6eAd1	cyklicky
mění	měnit	k5eAaImIp3nS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Střídavý	střídavý	k2eAgInSc1d1	střídavý
proud	proud	k1gInSc1	proud
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
harmonický	harmonický	k2eAgInSc4d1	harmonický
nebo	nebo	k8xC	nebo
obecný	obecný	k2eAgInSc4d1	obecný
průběh	průběh	k1gInSc4	průběh
<g/>
.	.	kIx.	.
</s>
<s>
Časový	časový	k2eAgInSc1d1	časový
průběh	průběh	k1gInSc1	průběh
proudu	proud	k1gInSc2	proud
s	s	k7c7	s
harmonickým	harmonický	k2eAgInSc7d1	harmonický
průběhem	průběh	k1gInSc7	průběh
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
sinusoidy	sinusoida	k1gFnSc2	sinusoida
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Elektrický	elektrický	k2eAgInSc1d1	elektrický
proud	proud	k1gInSc1	proud
je	být	k5eAaImIp3nS	být
veličina	veličina	k1gFnSc1	veličina
vhodná	vhodný	k2eAgFnSc1d1	vhodná
pro	pro	k7c4	pro
popis	popis	k1gInSc4	popis
zdrojů	zdroj	k1gInPc2	zdroj
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Elektrický	elektrický	k2eAgInSc1d1	elektrický
proud	proud	k1gInSc1	proud
jako	jako	k8xC	jako
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
veličina	veličina	k1gFnSc1	veličina
==	==	k?	==
</s>
</p>
<p>
<s>
Elektrický	elektrický	k2eAgInSc1d1	elektrický
proud	proud	k1gInSc1	proud
je	být	k5eAaImIp3nS	být
skalární	skalární	k2eAgFnSc1d1	skalární
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
veličina	veličina	k1gFnSc1	veličina
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Směrovost	směrovost	k1gFnSc1	směrovost
jeho	on	k3xPp3gInSc2	on
toku	tok	k1gInSc2	tok
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
v	v	k7c6	v
příbuzných	příbuzný	k2eAgFnPc6d1	příbuzná
vektorových	vektorový	k2eAgFnPc6d1	vektorová
veličinách	veličina	k1gFnPc6	veličina
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
hustota	hustota	k1gFnSc1	hustota
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
V	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
SI	si	k1gNnPc2	si
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
veličin	veličina	k1gFnPc2	veličina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Definice	definice	k1gFnSc2	definice
===	===	k?	===
</s>
</p>
<p>
<s>
Elektrický	elektrický	k2eAgInSc1d1	elektrický
proud	proud	k1gInSc1	proud
je	být	k5eAaImIp3nS	být
roven	roven	k2eAgInSc1d1	roven
celkovému	celkový	k2eAgNnSc3d1	celkové
množství	množství	k1gNnSc3	množství
elektrického	elektrický	k2eAgInSc2d1	elektrický
náboje	náboj	k1gInSc2	náboj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
projde	projít	k5eAaPmIp3nS	projít
průřezem	průřez	k1gInSc7	průřez
vodiče	vodič	k1gInSc2	vodič
za	za	k7c4	za
jednotku	jednotka	k1gFnSc4	jednotka
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Označení	označení	k1gNnPc1	označení
a	a	k8xC	a
jednotky	jednotka	k1gFnPc1	jednotka
===	===	k?	===
</s>
</p>
<p>
<s>
Doporučená	doporučený	k2eAgFnSc1d1	Doporučená
značka	značka	k1gFnSc1	značka
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
je	být	k5eAaImIp3nS	být
I	i	k9	i
(	(	kIx(	(
<g/>
velké	velká	k1gFnSc2	velká
I	I	kA	I
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
jednotkou	jednotka	k1gFnSc7	jednotka
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
SI	si	k1gNnSc2	si
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
ampér	ampér	k1gInSc1	ampér
<g/>
,	,	kIx,	,
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
značka	značka	k1gFnSc1	značka
"	"	kIx"	"
<g/>
A	A	kA	A
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeden	jeden	k4xCgInSc1	jeden
ampér	ampér	k1gInSc1	ampér
je	být	k5eAaImIp3nS	být
stálý	stálý	k2eAgInSc1d1	stálý
elektrický	elektrický	k2eAgInSc1d1	elektrický
proud	proud	k1gInSc1	proud
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
při	při	k7c6	při
průchodu	průchod	k1gInSc6	průchod
dvěma	dva	k4xCgInPc7	dva
přímými	přímý	k2eAgInPc7d1	přímý
rovnoběžnými	rovnoběžný	k2eAgInPc7d1	rovnoběžný
nekonečně	konečně	k6eNd1	konečně
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
vodiči	vodič	k1gInPc7	vodič
zanedbatelného	zanedbatelný	k2eAgInSc2d1	zanedbatelný
kruhového	kruhový	k2eAgInSc2d1	kruhový
průřezu	průřez	k1gInSc2	průřez
umístěnými	umístěný	k2eAgInPc7d1	umístěný
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
ve	v	k7c6	v
vzájemné	vzájemný	k2eAgFnSc6d1	vzájemná
vzdálenosti	vzdálenost	k1gFnSc3	vzdálenost
1	[number]	k4	1
metr	metr	k1gInSc1	metr
vyvolá	vyvolat	k5eAaPmIp3nS	vyvolat
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
stálou	stálý	k2eAgFnSc4d1	stálá
sílu	síla	k1gFnSc4	síla
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
7	[number]	k4	7
newtonu	newton	k1gInSc2	newton
na	na	k7c4	na
1	[number]	k4	1
metr	metr	k1gInSc4	metr
délky	délka	k1gFnSc2	délka
vodiče	vodič	k1gInSc2	vodič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Měření	měření	k1gNnPc1	měření
===	===	k?	===
</s>
</p>
<p>
<s>
Elektrický	elektrický	k2eAgInSc1d1	elektrický
proud	proud	k1gInSc1	proud
se	se	k3xPyFc4	se
měří	měřit	k5eAaImIp3nS	měřit
ampérmetrem	ampérmetr	k1gInSc7	ampérmetr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc4	druh
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
podle	podle	k7c2	podle
časového	časový	k2eAgInSc2d1	časový
průběhu	průběh	k1gInSc2	průběh
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Střídavý	střídavý	k2eAgInSc1d1	střídavý
proud	proud	k1gInSc1	proud
===	===	k?	===
</s>
</p>
<p>
<s>
Střídavý	střídavý	k2eAgInSc1d1	střídavý
proud	proud	k1gInSc1	proud
je	být	k5eAaImIp3nS	být
proud	proud	k1gInSc1	proud
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
velikost	velikost	k1gFnSc1	velikost
a	a	k8xC	a
směr	směr	k1gInSc1	směr
se	se	k3xPyFc4	se
v	v	k7c6	v
čase	čas	k1gInSc6	čas
mění	měnit	k5eAaImIp3nS	měnit
s	s	k7c7	s
určitou	určitý	k2eAgFnSc7d1	určitá
periodou	perioda	k1gFnSc7	perioda
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jeho	jeho	k3xOp3gFnSc1	jeho
střední	střední	k2eAgFnSc1d1	střední
hodnota	hodnota	k1gFnSc1	hodnota
je	být	k5eAaImIp3nS	být
nulová	nulový	k2eAgFnSc1d1	nulová
<g/>
.	.	kIx.	.
</s>
<s>
Střídavý	střídavý	k2eAgInSc1d1	střídavý
proud	proud	k1gInSc1	proud
je	být	k5eAaImIp3nS	být
proměnný	proměnný	k2eAgInSc1d1	proměnný
proud	proud	k1gInSc1	proud
typicky	typicky	k6eAd1	typicky
s	s	k7c7	s
sinusovým	sinusový	k2eAgInSc7d1	sinusový
(	(	kIx(	(
<g/>
harmonickým	harmonický	k2eAgInSc7d1	harmonický
<g/>
)	)	kIx)	)
průběhem	průběh	k1gInSc7	průběh
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
průběhy	průběh	k1gInPc1	průběh
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
například	například	k6eAd1	například
pilovité	pilovitý	k2eAgFnPc4d1	pilovitá
<g/>
,	,	kIx,	,
obdélníkové	obdélníkový	k2eAgFnPc4d1	obdélníková
nebo	nebo	k8xC	nebo
libovolné	libovolný	k2eAgFnPc4d1	libovolná
jiné	jiný	k2eAgFnPc4d1	jiná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
I	i	k9	i
</s>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
sin	sin	kA	sin
</s>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
ω	ω	k?	ω
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
φ	φ	k?	φ
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
φ	φ	k?	φ
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
i	i	k9	i
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
I_	I_	k1gMnSc1	I_
<g/>
{	{	kIx(	{
<g/>
m	m	kA	m
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
sin	sin	kA	sin
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
t	t	k?	t
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
varphi	varphi	k6eAd1	varphi
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
varphi	varphi	k6eAd1	varphi
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
<g/>
kde	kde	k6eAd1	kde
Im	Im	k1gFnSc1	Im
je	být	k5eAaImIp3nS	být
amplituda	amplituda	k1gFnSc1	amplituda
střídavého	střídavý	k2eAgInSc2d1	střídavý
proudu	proud	k1gInSc2	proud
<g/>
,	,	kIx,	,
ω	ω	k?	ω
je	být	k5eAaImIp3nS	být
úhlová	úhlový	k2eAgFnSc1d1	úhlová
frekvence	frekvence	k1gFnSc1	frekvence
<g/>
,	,	kIx,	,
φ	φ	k?	φ
<g/>
0	[number]	k4	0
je	být	k5eAaImIp3nS	být
počáteční	počáteční	k2eAgFnSc1d1	počáteční
fáze	fáze	k1gFnSc1	fáze
střídavého	střídavý	k2eAgNnSc2d1	střídavé
napětí	napětí	k1gNnSc2	napětí
<g/>
,	,	kIx,	,
φ	φ	k?	φ
je	být	k5eAaImIp3nS	být
fázový	fázový	k2eAgInSc1d1	fázový
posuv	posuv	k1gInSc1	posuv
mezi	mezi	k7c7	mezi
napětím	napětí	k1gNnSc7	napětí
a	a	k8xC	a
proudem	proud	k1gInSc7	proud
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
se	se	k3xPyFc4	se
zkráceně	zkráceně	k6eAd1	zkráceně
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
fázi	fáze	k1gFnSc6	fáze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stejnosměrný	stejnosměrný	k2eAgInSc1d1	stejnosměrný
proud	proud	k1gInSc1	proud
===	===	k?	===
</s>
</p>
<p>
<s>
Stejnosměrný	stejnosměrný	k2eAgInSc1d1	stejnosměrný
proud	proud	k1gInSc1	proud
je	být	k5eAaImIp3nS	být
takový	takový	k3xDgInSc1	takový
proud	proud	k1gInSc1	proud
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
čase	čas	k1gInSc6	čas
nemění	měnit	k5eNaImIp3nS	měnit
směr	směr	k1gInSc1	směr
svého	svůj	k3xOyFgInSc2	svůj
toku	tok	k1gInSc2	tok
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
proudu	proud	k1gInSc2	proud
se	se	k3xPyFc4	se
měnit	měnit	k5eAaImF	měnit
může	moct	k5eAaImIp3nS	moct
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stacionární	stacionární	k2eAgInSc4d1	stacionární
a	a	k8xC	a
nestacionární	stacionární	k2eNgInSc4d1	nestacionární
elektrický	elektrický	k2eAgInSc4d1	elektrický
proud	proud	k1gInSc4	proud
===	===	k?	===
</s>
</p>
<p>
<s>
Jako	jako	k8xC	jako
stacionární	stacionární	k2eAgMnSc1d1	stacionární
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
elektrický	elektrický	k2eAgInSc1d1	elektrický
proud	proud	k1gInSc1	proud
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
konstantní	konstantní	k2eAgMnSc1d1	konstantní
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
má	mít	k5eAaImIp3nS	mít
časově	časově	k6eAd1	časově
neměnnou	měnný	k2eNgFnSc4d1	neměnná
velikost	velikost	k1gFnSc4	velikost
i	i	k8xC	i
směr	směr	k1gInSc4	směr
toku	tok	k1gInSc2	tok
<g/>
.	.	kIx.	.
</s>
<s>
Stacionárním	stacionární	k2eAgInSc7d1	stacionární
proudem	proud	k1gInSc7	proud
je	být	k5eAaImIp3nS	být
generováno	generován	k2eAgNnSc1d1	generováno
stacionární	stacionární	k2eAgNnSc1d1	stacionární
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Opakem	opak	k1gInSc7	opak
stacionárního	stacionární	k2eAgInSc2d1	stacionární
proudu	proud	k1gInSc2	proud
je	být	k5eAaImIp3nS	být
proud	proud	k1gInSc1	proud
nestacionární	stacionární	k2eNgInSc1d1	nestacionární
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgMnSc1d1	zahrnující
všechny	všechen	k3xTgInPc4	všechen
případy	případ	k1gInPc4	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
proud	proud	k1gInSc1	proud
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c6	v
čase	čas	k1gInSc6	čas
buď	buď	k8xC	buď
svou	svůj	k3xOyFgFnSc4	svůj
velikost	velikost	k1gFnSc4	velikost
nebo	nebo	k8xC	nebo
směr	směr	k1gInSc4	směr
svého	svůj	k3xOyFgInSc2	svůj
toku	tok	k1gInSc2	tok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Průměrný	průměrný	k2eAgInSc1d1	průměrný
proud	proud	k1gInSc1	proud
===	===	k?	===
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
prochází	procházet	k5eAaImIp3nS	procházet
elektrický	elektrický	k2eAgInSc4d1	elektrický
náboj	náboj	k1gInSc4	náboj
průřezem	průřez	k1gInSc7	průřez
vodiče	vodič	k1gInSc2	vodič
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
<g/>
,	,	kIx,	,
definuje	definovat	k5eAaBmIp3nS	definovat
se	se	k3xPyFc4	se
průměrný	průměrný	k2eAgInSc1d1	průměrný
proud	proud	k1gInSc1	proud
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
I	i	k9	i
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Δ	Δ	k?	Δ
</s>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Δ	Δ	k?	Δ
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
I	i	k9	i
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
Q	Q	kA	Q
\	\	kIx~	\
<g/>
over	over	k1gMnSc1	over
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
t	t	k?	t
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
</p>
<p>
<s>
kde	kde	k6eAd1	kde
Q	Q	kA	Q
je	být	k5eAaImIp3nS	být
elektrický	elektrický	k2eAgInSc4d1	elektrický
náboj	náboj	k1gInSc4	náboj
<g/>
,	,	kIx,	,
t	t	k?	t
je	být	k5eAaImIp3nS	být
čas	čas	k1gInSc4	čas
</s>
</p>
<p>
<s>
===	===	k?	===
Okamžitý	okamžitý	k2eAgInSc1d1	okamžitý
elektrický	elektrický	k2eAgInSc1d1	elektrický
proud	proud	k1gInSc1	proud
===	===	k?	===
</s>
</p>
<p>
<s>
Okamžitý	okamžitý	k2eAgInSc1d1	okamžitý
elektrický	elektrický	k2eAgInSc1d1	elektrický
proud	proud	k1gInSc1	proud
je	být	k5eAaImIp3nS	být
limitním	limitní	k2eAgInSc7d1	limitní
(	(	kIx(	(
<g/>
krajním	krajní	k2eAgInSc7d1	krajní
<g/>
)	)	kIx)	)
případem	případ	k1gInSc7	případ
průměrného	průměrný	k2eAgInSc2d1	průměrný
proudu	proud	k1gInSc2	proud
<g/>
,	,	kIx,	,
definuje	definovat	k5eAaBmIp3nS	definovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
množství	množství	k1gNnSc1	množství
náboje	náboj	k1gInSc2	náboj
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
projde	projít	k5eAaPmIp3nS	projít
průřezem	průřez	k1gInSc7	průřez
vodiče	vodič	k1gInSc2	vodič
za	za	k7c4	za
infinitesimální	infinitesimální	k2eAgFnSc4d1	infinitesimální
(	(	kIx(	(
<g/>
nekonečně	konečně	k6eNd1	konečně
krátký	krátký	k2eAgInSc4d1	krátký
<g/>
)	)	kIx)	)
čas	čas	k1gInSc4	čas
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
lim	limo	k1gNnPc2	limo
</s>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Δ	Δ	k?	Δ
</s>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Δ	Δ	k?	Δ
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
i	i	k9	i
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
t	t	k?	t
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
Q	Q	kA	Q
\	\	kIx~	\
<g/>
over	over	k1gMnSc1	over
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
t	t	k?	t
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
Q	Q	kA	Q
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
t	t	k?	t
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
V	v	k7c6	v
ustáleném	ustálený	k2eAgInSc6d1	ustálený
stavu	stav	k1gInSc6	stav
protéká	protékat	k5eAaImIp3nS	protékat
všemi	všecek	k3xTgFnPc7	všecek
průřezy	průřez	k1gInPc1	průřez
vodiče	vodič	k1gInSc2	vodič
stejně	stejně	k6eAd1	stejně
velký	velký	k2eAgInSc1d1	velký
proud	proud	k1gInSc1	proud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Prostorové	prostorový	k2eAgNnSc1d1	prostorové
rozložení	rozložení	k1gNnSc1	rozložení
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Objemový	objemový	k2eAgInSc4d1	objemový
elektrický	elektrický	k2eAgInSc4d1	elektrický
proud	proud	k1gInSc4	proud
===	===	k?	===
</s>
</p>
<p>
<s>
Elektrický	elektrický	k2eAgInSc1d1	elektrický
proud	proud	k1gInSc1	proud
zpravidla	zpravidla	k6eAd1	zpravidla
protéká	protékat	k5eAaImIp3nS	protékat
celým	celý	k2eAgInSc7d1	celý
objemem	objem	k1gInSc7	objem
vodiče	vodič	k1gInSc2	vodič
<g/>
.	.	kIx.	.
</s>
<s>
Lokálně	lokálně	k6eAd1	lokálně
se	se	k3xPyFc4	se
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
jak	jak	k6eAd1	jak
množství	množství	k1gNnSc1	množství
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
rychlost	rychlost	k1gFnSc1	rychlost
nosičů	nosič	k1gInPc2	nosič
náboje	náboj	k1gInSc2	náboj
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
směr	směr	k1gInSc1	směr
s	s	k7c7	s
daným	daný	k2eAgNnSc7d1	dané
místem	místo	k1gNnSc7	místo
ve	v	k7c6	v
vodiči	vodič	k1gInSc6	vodič
měnit	měnit	k5eAaImF	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
laminárního	laminární	k2eAgNnSc2d1	laminární
proudění	proudění	k1gNnSc2	proudění
tekutin	tekutina	k1gFnPc2	tekutina
lze	lze	k6eAd1	lze
ke	k	k7c3	k
grafickému	grafický	k2eAgNnSc3d1	grafické
zobrazení	zobrazení	k1gNnSc3	zobrazení
prostorového	prostorový	k2eAgInSc2d1	prostorový
proudu	proud	k1gInSc2	proud
použít	použít	k5eAaPmF	použít
proudové	proudový	k2eAgInPc4d1	proudový
čáry	čár	k1gInPc4	čár
(	(	kIx(	(
<g/>
trajektorie	trajektorie	k1gFnPc4	trajektorie
ustáleného	ustálený	k2eAgInSc2d1	ustálený
pohybu	pohyb	k1gInSc2	pohyb
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
nosičů	nosič	k1gInPc2	nosič
náboje	náboj	k1gInSc2	náboj
<g/>
)	)	kIx)	)
a	a	k8xC	a
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
zákonu	zákon	k1gInSc3	zákon
zachování	zachování	k1gNnSc2	zachování
náboje	náboj	k1gInSc2	náboj
má	mít	k5eAaImIp3nS	mít
dobrý	dobrý	k2eAgInSc4d1	dobrý
smysl	smysl	k1gInSc4	smysl
i	i	k8xC	i
pojem	pojem	k1gInSc4	pojem
proudové	proudový	k2eAgFnSc2d1	proudová
trubice	trubice	k1gFnSc2	trubice
(	(	kIx(	(
<g/>
prostor	prostor	k1gInSc1	prostor
kolem	kolem	k7c2	kolem
proudové	proudový	k2eAgFnSc2d1	proudová
čáry	čára	k1gFnSc2	čára
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yIgFnSc4	který
lze	lze	k6eAd1	lze
aplikovat	aplikovat	k5eAaBmF	aplikovat
zákony	zákon	k1gInPc4	zákon
elektrického	elektrický	k2eAgInSc2d1	elektrický
obvodu	obvod	k1gInSc2	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
mechanického	mechanický	k2eAgNnSc2d1	mechanické
proudění	proudění	k1gNnSc2	proudění
je	být	k5eAaImIp3nS	být
však	však	k9	však
navíc	navíc	k6eAd1	navíc
nutno	nutno	k6eAd1	nutno
respektovat	respektovat	k5eAaImF	respektovat
vzájemné	vzájemný	k2eAgNnSc4d1	vzájemné
magnetické	magnetický	k2eAgNnSc4d1	magnetické
silové	silový	k2eAgNnSc4d1	silové
působení	působení	k1gNnSc4	působení
proudových	proudový	k2eAgFnPc2d1	proudová
trubic	trubice	k1gFnPc2	trubice
<g/>
,	,	kIx,	,
projevující	projevující	k2eAgInPc4d1	projevující
se	se	k3xPyFc4	se
např.	např.	kA	např.
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
pinch	pinch	k1gInSc1	pinch
efektu	efekt	k1gInSc2	efekt
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
příčné	příčný	k2eAgNnSc1d1	příčné
stlačení	stlačení	k1gNnSc1	stlačení
plazmového	plazmový	k2eAgInSc2d1	plazmový
proudu	proud	k1gInSc2	proud
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
popisu	popis	k1gInSc3	popis
lokálního	lokální	k2eAgInSc2d1	lokální
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
se	se	k3xPyFc4	se
zavádí	zavádět	k5eAaImIp3nS	zavádět
vektorová	vektorový	k2eAgFnSc1d1	vektorová
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
veličina	veličina	k1gFnSc1	veličina
hustota	hustota	k1gFnSc1	hustota
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
proudová	proudový	k2eAgFnSc1d1	proudová
hustota	hustota	k1gFnSc1	hustota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hustota	hustota	k1gFnSc1	hustota
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
má	mít	k5eAaImIp3nS	mít
doporučené	doporučený	k2eAgFnPc4d1	Doporučená
značky	značka	k1gFnPc4	značka
J	J	kA	J
nebo	nebo	k8xC	nebo
j	j	k?	j
a	a	k8xC	a
jednotku	jednotka	k1gFnSc4	jednotka
1	[number]	k4	1
ampér	ampér	k1gInSc1	ampér
na	na	k7c4	na
metr	metr	k1gInSc4	metr
čtverečný	čtverečný	k2eAgInSc4d1	čtverečný
(	(	kIx(	(
<g/>
A	a	k9	a
<g/>
/	/	kIx~	/
<g/>
m	m	kA	m
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velikost	velikost	k1gFnSc1	velikost
hustoty	hustota	k1gFnSc2	hustota
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
je	být	k5eAaImIp3nS	být
definována	definovat	k5eAaBmNgFnS	definovat
jako	jako	k8xC	jako
podíl	podíl	k1gInSc1	podíl
okamžitého	okamžitý	k2eAgInSc2d1	okamžitý
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
procházejícího	procházející	k2eAgInSc2d1	procházející
daným	daný	k2eAgInSc7d1	daný
elementem	element	k1gInSc7	element
průřezu	průřez	k1gInSc2	průřez
vodiče	vodič	k1gInSc2	vodič
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
S	s	k7c7	s
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
kolmého	kolmý	k2eAgInSc2d1	kolmý
průmětu	průmět	k1gInSc2	průmět
tohoto	tento	k3xDgInSc2	tento
elementu	element	k1gInSc2	element
průřezu	průřez	k1gInSc2	průřez
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
<s>
⊥	⊥	k?	⊥
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
S_	S_	k1gMnSc1	S_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
perp	perp	k1gInSc1	perp
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
na	na	k7c4	na
střední	střední	k2eAgInSc4d1	střední
směr	směr	k1gInSc4	směr
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc4	mathbf
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
pohybu	pohyb	k1gInSc3	pohyb
nosičů	nosič	k1gMnPc2	nosič
nábojů	náboj	k1gInPc2	náboj
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
proud	proud	k1gInSc1	proud
tvoří	tvořit	k5eAaImIp3nS	tvořit
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
na	na	k7c4	na
směr	směr	k1gInSc4	směr
tečny	tečna	k1gFnSc2	tečna
proudové	proudový	k2eAgFnSc2d1	proudová
čáry	čára	k1gFnSc2	čára
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
j	j	k?	j
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
I	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
<s>
⊥	⊥	k?	⊥
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc4	mathbf
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
I_	I_	k1gFnSc1	I_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
S	s	k7c7	s
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
S_	S_	k1gMnSc1	S_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
perp	perp	k1gInSc1	perp
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
což	což	k3yQnSc1	což
lze	lze	k6eAd1	lze
v	v	k7c6	v
integrálním	integrální	k2eAgInSc6d1	integrální
tvaru	tvar	k1gInSc6	tvar
zapsat	zapsat	k5eAaPmF	zapsat
vztahem	vztah	k1gInSc7	vztah
pro	pro	k7c4	pro
proud	proud	k1gInSc4	proud
celým	celý	k2eAgInSc7d1	celý
průřezem	průřez	k1gInSc7	průřez
vodiče	vodič	k1gInSc2	vodič
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
I	i	k9	i
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
∫	∫	k?	∫
</s>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
j	j	k?	j
</s>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
I	i	k9	i
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
int	int	k?	int
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
S	s	k7c7	s
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
S	s	k7c7	s
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
<g/>
Hustota	hustota	k1gFnSc1	hustota
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
ve	v	k7c6	v
vztazích	vztah	k1gInPc6	vztah
teorie	teorie	k1gFnSc2	teorie
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
pole	pole	k1gNnSc2	pole
formulovaných	formulovaný	k2eAgInPc2d1	formulovaný
v	v	k7c6	v
diferenciálním	diferenciální	k2eAgInSc6d1	diferenciální
tvaru	tvar	k1gInSc6	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
první	první	k4xOgFnSc1	první
Maxwellova	Maxwellův	k2eAgFnSc1d1	Maxwellova
rovnice	rovnice	k1gFnSc1	rovnice
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
rot	rota	k1gFnPc2	rota
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
j	j	k?	j
</s>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
</p>
<p>
<s>
D	D	kA	D
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
operatorname	operatornam	k1gInSc5	operatornam
{	{	kIx(	{
<g/>
rot	rota	k1gFnPc2	rota
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
H	H	kA	H
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
}	}	kIx)	}
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gMnSc1	partial
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
D	D	kA	D
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
t	t	k?	t
<g/>
}}	}}	k?	}}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
===	===	k?	===
Plošný	plošný	k2eAgInSc4d1	plošný
elektrický	elektrický	k2eAgInSc4d1	elektrický
proud	proud	k1gInSc4	proud
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
má	mít	k5eAaImIp3nS	mít
vodič	vodič	k1gMnSc1	vodič
deskovitý	deskovitý	k2eAgInSc4d1	deskovitý
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
jeho	on	k3xPp3gNnSc2	on
tloušťka	tloušťka	k1gFnSc1	tloušťka
je	být	k5eAaImIp3nS	být
zanedbatelná	zanedbatelný	k2eAgFnSc1d1	zanedbatelná
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
zbývajícím	zbývající	k2eAgInPc3d1	zbývající
rozměrům	rozměr	k1gInPc3	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Elektrický	elektrický	k2eAgInSc1d1	elektrický
proud	proud	k1gInSc1	proud
také	také	k9	také
může	moct	k5eAaImIp3nS	moct
protékat	protékat	k5eAaImF	protékat
pouze	pouze	k6eAd1	pouze
těsně	těsně	k6eAd1	těsně
u	u	k7c2	u
daného	daný	k2eAgNnSc2d1	dané
materiálového	materiálový	k2eAgNnSc2d1	materiálové
rozhraní	rozhraní	k1gNnSc2	rozhraní
(	(	kIx(	(
<g/>
jinde	jinde	k6eAd1	jinde
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
materiál	materiál	k1gInSc4	materiál
nevodivý	vodivý	k2eNgInSc4d1	nevodivý
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
pouze	pouze	k6eAd1	pouze
těsně	těsně	k6eAd1	těsně
u	u	k7c2	u
povrchu	povrch	k1gInSc2	povrch
vodiče	vodič	k1gInSc2	vodič
(	(	kIx(	(
<g/>
např.	např.	kA	např.
u	u	k7c2	u
skin	skin	k1gMnSc1	skin
efektu	efekt	k1gInSc2	efekt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
těchto	tento	k3xDgInPc6	tento
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
prostor	prostor	k1gInSc1	prostor
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgNnSc6	který
proud	proud	k1gInSc1	proud
protéká	protékat	k5eAaImIp3nS	protékat
<g/>
,	,	kIx,	,
omezen	omezit	k5eAaPmNgInS	omezit
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
tloušťce	tloušťka	k1gFnSc6	tloušťka
–	–	k?	–
hovoříme	hovořit	k5eAaImIp1nP	hovořit
pak	pak	k6eAd1	pak
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
plošném	plošný	k2eAgInSc6d1	plošný
proudu	proud	k1gInSc6	proud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
popisu	popis	k1gInSc3	popis
lokálního	lokální	k2eAgInSc2d1	lokální
plošného	plošný	k2eAgInSc2d1	plošný
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
se	se	k3xPyFc4	se
zavádí	zavádět	k5eAaImIp3nS	zavádět
vektorová	vektorový	k2eAgFnSc1d1	vektorová
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
veličina	veličina	k1gFnSc1	veličina
hustota	hustota	k1gFnSc1	hustota
plošného	plošný	k2eAgInSc2d1	plošný
(	(	kIx(	(
<g/>
elektrického	elektrický	k2eAgInSc2d1	elektrický
<g/>
)	)	kIx)	)
proudu	proud	k1gInSc2	proud
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
plošná	plošný	k2eAgFnSc1d1	plošná
proudová	proudový	k2eAgFnSc1d1	proudová
hustota	hustota	k1gFnSc1	hustota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hustota	hustota	k1gFnSc1	hustota
plošného	plošný	k2eAgInSc2d1	plošný
(	(	kIx(	(
<g/>
elektrického	elektrický	k2eAgInSc2d1	elektrický
<g/>
)	)	kIx)	)
proudu	proud	k1gInSc2	proud
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
značí	značit	k5eAaImIp3nS	značit
i	i	k8xC	i
nebo	nebo	k8xC	nebo
JS	JS	kA	JS
a	a	k8xC	a
její	její	k3xOp3gFnSc7	její
jednotkou	jednotka	k1gFnSc7	jednotka
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
ampér	ampér	k1gInSc1	ampér
na	na	k7c4	na
metr	metr	k1gInSc4	metr
(	(	kIx(	(
<g/>
A	A	kA	A
<g/>
/	/	kIx~	/
<g/>
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
definována	definovat	k5eAaBmNgFnS	definovat
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k8xC	jako
proudová	proudový	k2eAgFnSc1d1	proudová
hustota	hustota	k1gFnSc1	hustota
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
elementárním	elementární	k2eAgInSc7d1	elementární
"	"	kIx"	"
<g/>
průřezem	průřez	k1gInSc7	průřez
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
element	element	k1gInSc1	element
délky	délka	k1gFnSc2	délka
křivky	křivka	k1gFnSc2	křivka
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
l	l	kA	l
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
l	l	kA	l
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
přes	přes	k7c4	přes
který	který	k3yIgInSc4	který
proud	proud	k1gInSc4	proud
protéká	protékat	k5eAaImIp3nS	protékat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
I	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
l	l	kA	l
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
l	l	kA	l
</s>
</p>
<p>
</p>
<p>
<s>
⊥	⊥	k?	⊥
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc4	mathbf
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
I_	I_	k1gFnSc1	I_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
l	l	kA	l
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
l_	l_	k?	l_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
perp	perp	k1gInSc1	perp
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
což	což	k3yQnSc1	což
lze	lze	k6eAd1	lze
v	v	k7c6	v
integrálním	integrální	k2eAgInSc6d1	integrální
tvaru	tvar	k1gInSc6	tvar
zapsat	zapsat	k5eAaPmF	zapsat
vztahem	vztah	k1gInSc7	vztah
pro	pro	k7c4	pro
proud	proud	k1gInSc4	proud
celým	celý	k2eAgInSc7d1	celý
délkovým	délkový	k2eAgInSc7d1	délkový
"	"	kIx"	"
<g/>
průřezem	průřez	k1gInSc7	průřez
<g/>
"	"	kIx"	"
vodiče	vodič	k1gInSc2	vodič
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
I	i	k9	i
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
∫	∫	k?	∫
</s>
</p>
<p>
</p>
<p>
<s>
l	l	kA	l
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
ν	ν	k?	ν
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
l	l	kA	l
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
I	i	k9	i
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
int	int	k?	int
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
l	l	kA	l
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
l	l	kA	l
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ν	ν	k?	ν
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
jednotkový	jednotkový	k2eAgInSc4d1	jednotkový
vektor	vektor	k1gInSc4	vektor
normály	normála	k1gFnSc2	normála
ke	k	k7c3	k
křivce	křivka	k1gFnSc3	křivka
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
l	l	kA	l
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
l	l	kA	l
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
ležící	ležící	k2eAgFnSc1d1	ležící
v	v	k7c6	v
ploše	plocha	k1gFnSc6	plocha
vodiče	vodič	k1gInSc2	vodič
<g/>
.	.	kIx.	.
<g/>
Hustota	hustota	k1gFnSc1	hustota
plošného	plošný	k2eAgInSc2d1	plošný
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
ve	v	k7c6	v
vztazích	vztah	k1gInPc6	vztah
teorie	teorie	k1gFnSc2	teorie
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
pole	pole	k1gNnSc2	pole
formulovaných	formulovaný	k2eAgInPc2d1	formulovaný
v	v	k7c6	v
diferenciálním	diferenciální	k2eAgInSc6d1	diferenciální
tvaru	tvar	k1gInSc6	tvar
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
týkají	týkat	k5eAaImIp3nP	týkat
plošných	plošný	k2eAgInPc2d1	plošný
vodičů	vodič	k1gInPc2	vodič
nebo	nebo	k8xC	nebo
plošných	plošný	k2eAgNnPc2d1	plošné
rozhraní	rozhraní	k1gNnPc2	rozhraní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
rovnice	rovnice	k1gFnSc1	rovnice
pro	pro	k7c4	pro
změnu	změna	k1gFnSc4	změna
vektoru	vektor	k1gInSc2	vektor
intenzity	intenzita	k1gFnSc2	intenzita
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
na	na	k7c6	na
plošném	plošný	k2eAgNnSc6d1	plošné
rozhraní	rozhraní	k1gNnSc6	rozhraní
protékaném	protékaný	k2eAgNnSc6d1	protékané
proudem	proud	k1gInSc7	proud
o	o	k7c6	o
plošné	plošný	k2eAgFnSc6d1	plošná
proudové	proudový	k2eAgFnSc6d1	proudová
hustotě	hustota	k1gFnSc6	hustota
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc4	mathbf
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
jednotkový	jednotkový	k2eAgInSc1d1	jednotkový
vektor	vektor	k1gInSc1	vektor
normály	normála	k1gFnSc2	normála
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ν	ν	k?	ν
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
směřuje	směřovat	k5eAaImIp3nS	směřovat
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
do	do	k7c2	do
prostředí	prostředí	k1gNnSc2	prostředí
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ν	ν	k?	ν
</s>
</p>
<p>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
times	times	k1gMnSc1	times
\	\	kIx~	\
<g/>
left	left	k1gMnSc1	left
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
H	H	kA	H
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
H	H	kA	H
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc4	druh
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
podle	podle	k7c2	podle
nositelů	nositel	k1gMnPc2	nositel
náboje	náboj	k1gInSc2	náboj
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Kondukční	Kondukční	k2eAgInSc1d1	Kondukční
proud	proud	k1gInSc1	proud
===	===	k?	===
</s>
</p>
<p>
<s>
Kondukční	Kondukční	k2eAgInSc1d1	Kondukční
proud	proud	k1gInSc1	proud
(	(	kIx(	(
<g/>
vodivostní	vodivostní	k2eAgInSc1d1	vodivostní
proud	proud	k1gInSc1	proud
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
uspořádaný	uspořádaný	k2eAgInSc1d1	uspořádaný
tok	tok	k1gInSc1	tok
volných	volný	k2eAgMnPc2d1	volný
nositelů	nositel	k1gMnPc2	nositel
náboje	náboj	k1gInSc2	náboj
v	v	k7c6	v
látkovém	látkový	k2eAgNnSc6d1	látkové
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
pohyb	pohyb	k1gInSc1	pohyb
volných	volný	k2eAgInPc2d1	volný
elektronů	elektron	k1gInPc2	elektron
v	v	k7c6	v
kovech	kov	k1gInPc6	kov
<g/>
,	,	kIx,	,
iontů	ion	k1gInPc2	ion
v	v	k7c6	v
elektrolytech	elektrolyt	k1gInPc6	elektrolyt
<g/>
,	,	kIx,	,
ionizovaných	ionizovaný	k2eAgFnPc2d1	ionizovaná
molekul	molekula	k1gFnPc2	molekula
v	v	k7c6	v
plynech	plyn	k1gInPc6	plyn
<g/>
,	,	kIx,	,
děr	děra	k1gFnPc2	děra
v	v	k7c6	v
polovodičích	polovodič	k1gInPc6	polovodič
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétní	konkrétní	k2eAgFnPc1d1	konkrétní
vlastnosti	vlastnost	k1gFnPc1	vlastnost
kondukčního	kondukční	k2eAgInSc2d1	kondukční
proudu	proud	k1gInSc2	proud
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
typu	typ	k1gInSc6	typ
vlastnostech	vlastnost	k1gFnPc6	vlastnost
látkového	látkový	k2eAgNnSc2d1	látkové
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
působením	působení	k1gNnSc7	působení
elektrického	elektrický	k2eAgNnSc2d1	elektrické
pole	pole	k1gNnSc2	pole
ve	v	k7c6	v
vodiči	vodič	k1gInSc6	vodič
na	na	k7c4	na
nositele	nositel	k1gMnSc4	nositel
náboje	náboj	k1gInSc2	náboj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Konvekční	konvekční	k2eAgInSc4d1	konvekční
elektrický	elektrický	k2eAgInSc4d1	elektrický
proud	proud	k1gInSc4	proud
===	===	k?	===
</s>
</p>
<p>
<s>
Konvekční	konvekční	k2eAgInSc1d1	konvekční
elektrický	elektrický	k2eAgInSc1d1	elektrický
proud	proud	k1gInSc1	proud
je	být	k5eAaImIp3nS	být
způsoben	způsobit	k5eAaPmNgInS	způsobit
mechanickým	mechanický	k2eAgInSc7d1	mechanický
pohybem	pohyb	k1gInSc7	pohyb
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
náboj	náboj	k1gInSc1	náboj
vázán	vázat	k5eAaImNgInS	vázat
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
přenos	přenos	k1gInSc4	přenos
náboje	náboj	k1gInSc2	náboj
nabitým	nabitý	k2eAgInSc7d1	nabitý
pohyblivým	pohyblivý	k2eAgInSc7d1	pohyblivý
pásem	pás	k1gInSc7	pás
ve	v	k7c4	v
van	van	k1gInSc4	van
de	de	k?	de
Graaffově	Graaffův	k2eAgInSc6d1	Graaffův
generátoru	generátor	k1gInSc6	generátor
nebo	nebo	k8xC	nebo
pohyb	pohyb	k1gInSc1	pohyb
nabitých	nabitý	k2eAgFnPc2d1	nabitá
částic	částice	k1gFnPc2	částice
unášených	unášený	k2eAgFnPc2d1	unášená
v	v	k7c6	v
toku	tok	k1gInSc6	tok
tekutiny	tekutina	k1gFnSc2	tekutina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vázané	vázaný	k2eAgInPc1d1	vázaný
elektrické	elektrický	k2eAgInPc1d1	elektrický
proudy	proud	k1gInPc1	proud
===	===	k?	===
</s>
</p>
<p>
<s>
Výše	vysoce	k6eAd2	vysoce
uvedené	uvedený	k2eAgInPc1d1	uvedený
proudy	proud	k1gInPc1	proud
–	–	k?	–
kondukční	kondukční	k2eAgInPc1d1	kondukční
a	a	k8xC	a
konvekční	konvekční	k2eAgInSc1d1	konvekční
–	–	k?	–
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
proudy	proud	k1gInPc1	proud
volné	volný	k2eAgInPc1d1	volný
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
nositele	nositel	k1gMnPc4	nositel
náboje	náboj	k1gInPc1	náboj
mohou	moct	k5eAaImIp3nP	moct
vykonávat	vykonávat	k5eAaImF	vykonávat
makroskopické	makroskopický	k2eAgInPc4d1	makroskopický
pohyby	pohyb	k1gInPc4	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
však	však	k9	však
náboj	náboj	k1gInSc1	náboj
vázán	vázán	k2eAgInSc1d1	vázán
na	na	k7c4	na
částice	částice	k1gFnPc4	částice
vázané	vázaný	k2eAgFnPc4d1	vázaná
v	v	k7c6	v
mikroskopické	mikroskopický	k2eAgFnSc6d1	mikroskopická
struktuře	struktura	k1gFnSc6	struktura
látky	látka	k1gFnSc2	látka
–	–	k?	–
jeho	on	k3xPp3gInSc4	on
pohyb	pohyb	k1gInSc4	pohyb
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
za	za	k7c4	za
vázaný	vázaný	k2eAgInSc4d1	vázaný
elektrický	elektrický	k2eAgInSc4d1	elektrický
proud	proud	k1gInSc4	proud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vázané	vázaný	k2eAgInPc1d1	vázaný
elektrické	elektrický	k2eAgInPc1d1	elektrický
proudy	proud	k1gInPc1	proud
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
proudy	proud	k1gInPc4	proud
polarizační	polarizační	k2eAgInPc4d1	polarizační
a	a	k8xC	a
proudy	proud	k1gInPc1	proud
magnetizační	magnetizační	k2eAgInPc1d1	magnetizační
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Polarizační	polarizační	k2eAgInSc1d1	polarizační
proud	proud	k1gInSc1	proud
vzniká	vznikat	k5eAaImIp3nS	vznikat
při	při	k7c6	při
proměnné	proměnný	k2eAgFnSc6d1	proměnná
polarizaci	polarizace	k1gFnSc6	polarizace
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
P	P	kA	P
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
P	P	kA	P
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
dielektrika	dielektrikum	k1gNnSc2	dielektrikum
mikroskopickými	mikroskopický	k2eAgInPc7d1	mikroskopický
posuny	posun	k1gInPc7	posun
nabitých	nabitý	k2eAgFnPc2d1	nabitá
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Hustotu	hustota	k1gFnSc4	hustota
polarizačních	polarizační	k2eAgInPc2d1	polarizační
proudů	proud	k1gInPc2	proud
lze	lze	k6eAd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
vztahem	vztah	k1gInSc7	vztah
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
j	j	k?	j
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
<s>
o	o	k7c6	o
</s>
</p>
<p>
<s>
l	l	kA	l
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
</p>
<p>
<s>
P	P	kA	P
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc4	mathbf
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
pol	pola	k1gFnPc2	pola
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gMnSc1	partial
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
P	P	kA	P
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
t	t	k?	t
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
Magnetizační	magnetizační	k2eAgInPc1d1	magnetizační
proudy	proud	k1gInPc1	proud
jsou	být	k5eAaImIp3nP	být
mikroskopické	mikroskopický	k2eAgInPc1d1	mikroskopický
uzavřené	uzavřený	k2eAgInPc1d1	uzavřený
proudy	proud	k1gInPc1	proud
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
původcem	původce	k1gMnSc7	původce
magnetických	magnetický	k2eAgInPc2d1	magnetický
dipólových	dipólův	k2eAgInPc2d1	dipólův
momentů	moment	k1gInPc2	moment
částic	částice	k1gFnPc2	částice
ve	v	k7c6	v
struktuře	struktura	k1gFnSc6	struktura
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Magnetizačními	magnetizační	k2eAgInPc7d1	magnetizační
proudy	proud	k1gInPc7	proud
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
popisuje	popisovat	k5eAaImIp3nS	popisovat
i	i	k9	i
dipólový	dipólový	k2eAgInSc1d1	dipólový
moment	moment	k1gInSc1	moment
elementárních	elementární	k2eAgFnPc2d1	elementární
částic	částice	k1gFnPc2	částice
daný	daný	k2eAgInSc1d1	daný
jejich	jejich	k3xOp3gInSc7	jejich
nábojem	náboj	k1gInSc7	náboj
a	a	k8xC	a
spinem	spin	k1gInSc7	spin
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
ztotožnění	ztotožnění	k1gNnSc1	ztotožnění
kvantově	kvantově	k6eAd1	kvantově
mechanického	mechanický	k2eAgInSc2d1	mechanický
spinu	spin	k1gInSc2	spin
s	s	k7c7	s
"	"	kIx"	"
<g/>
rotací	rotace	k1gFnSc7	rotace
<g/>
"	"	kIx"	"
částice	částice	k1gFnSc2	částice
je	být	k5eAaImIp3nS	být
nesprávné	správný	k2eNgNnSc1d1	nesprávné
a	a	k8xC	a
zavádějící	zavádějící	k2eAgMnSc1d1	zavádějící
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
makroskopickou	makroskopický	k2eAgFnSc4d1	makroskopická
elektrodynamiku	elektrodynamika	k1gFnSc4	elektrodynamika
je	být	k5eAaImIp3nS	být
však	však	k9	však
tento	tento	k3xDgInSc1	tento
model	model	k1gInSc1	model
vyhovující	vyhovující	k2eAgInSc1d1	vyhovující
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
uzavřenosti	uzavřenost	k1gFnSc3	uzavřenost
lze	lze	k6eAd1	lze
hustotu	hustota	k1gFnSc4	hustota
magnetizačních	magnetizační	k2eAgInPc2d1	magnetizační
proudů	proud	k1gInPc2	proud
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jako	jako	k9	jako
rotaci	rotace	k1gFnSc4	rotace
vektorové	vektorový	k2eAgFnSc2d1	vektorová
veličiny	veličina	k1gFnSc2	veličina
<g/>
,	,	kIx,	,
tradičně	tradičně	k6eAd1	tradičně
zvané	zvaný	k2eAgFnPc1d1	zvaná
magnetizace	magnetizace	k1gFnPc1	magnetizace
a	a	k8xC	a
značené	značený	k2eAgFnPc1d1	značená
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
M	M	kA	M
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
j	j	k?	j
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
rot	rota	k1gFnPc2	rota
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
M	M	kA	M
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc4	mathbf
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
mag	mag	k?	mag
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
operatorname	operatornam	k1gInSc5	operatornam
{	{	kIx(	{
<g/>
rot	rota	k1gFnPc2	rota
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
na	na	k7c6	na
(	(	kIx(	(
<g/>
neuzavřené	uzavřený	k2eNgFnSc2d1	neuzavřená
<g/>
)	)	kIx)	)
polarizační	polarizační	k2eAgFnSc2d1	polarizační
a	a	k8xC	a
(	(	kIx(	(
<g/>
uzavřené	uzavřený	k2eAgInPc4d1	uzavřený
<g/>
)	)	kIx)	)
magnetizační	magnetizační	k2eAgInPc4d1	magnetizační
proudy	proud	k1gInPc4	proud
přestává	přestávat	k5eAaImIp3nS	přestávat
mít	mít	k5eAaImF	mít
smysl	smysl	k1gInSc4	smysl
pro	pro	k7c4	pro
rychle	rychle	k6eAd1	rychle
proměnná	proměnný	k2eAgNnPc4d1	proměnné
(	(	kIx(	(
<g/>
vysokofrekvenční	vysokofrekvenční	k2eAgNnPc4d1	vysokofrekvenční
<g/>
)	)	kIx)	)
elektromagnetická	elektromagnetický	k2eAgNnPc4d1	elektromagnetické
pole	pole	k1gNnPc4	pole
<g/>
;	;	kIx,	;
u	u	k7c2	u
rychlých	rychlý	k2eAgFnPc2d1	rychlá
změn	změna	k1gFnPc2	změna
nelze	lze	k6eNd1	lze
již	již	k6eAd1	již
mikroskopické	mikroskopický	k2eAgInPc4d1	mikroskopický
proudy	proud	k1gInPc4	proud
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
uzavřené	uzavřený	k2eAgFnPc4d1	uzavřená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Maxwellův	Maxwellův	k2eAgInSc1d1	Maxwellův
proud	proud	k1gInSc1	proud
===	===	k?	===
</s>
</p>
<p>
<s>
Maxwell	maxwell	k1gInSc4	maxwell
si	se	k3xPyFc3	se
jako	jako	k9	jako
první	první	k4xOgFnSc7	první
uvědomil	uvědomit	k5eAaPmAgMnS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ampérův	Ampérův	k2eAgInSc1d1	Ampérův
zákon	zákon	k1gInSc1	zákon
pro	pro	k7c4	pro
celkový	celkový	k2eAgInSc4d1	celkový
proud	proud	k1gInSc4	proud
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
rot	rota	k1gFnPc2	rota
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
B	B	kA	B
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
μ	μ	k?	μ
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
j	j	k?	j
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
operatorname	operatornam	k1gInSc5	operatornam
{	{	kIx(	{
<g/>
rot	rota	k1gFnPc2	rota
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
B	B	kA	B
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
nevyhovuje	vyhovovat	k5eNaImIp3nS	vyhovovat
zákonu	zákon	k1gInSc3	zákon
zachování	zachování	k1gNnSc4	zachování
náboje	náboj	k1gInSc2	náboj
vyjádřenému	vyjádřený	k2eAgInSc3d1	vyjádřený
rovnicí	rovnice	k1gFnSc7	rovnice
kontinuity	kontinuita	k1gFnSc2	kontinuita
<g/>
,	,	kIx,	,
budou	být	k5eAaImBp3nP	být
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
uvažovat	uvažovat	k5eAaImF	uvažovat
pouze	pouze	k6eAd1	pouze
volné	volný	k2eAgInPc4d1	volný
a	a	k8xC	a
vázané	vázaný	k2eAgInPc4d1	vázaný
proudy	proud	k1gInPc4	proud
<g/>
.	.	kIx.	.
</s>
<s>
Doplnil	doplnit	k5eAaPmAgInS	doplnit
proto	proto	k8xC	proto
celkový	celkový	k2eAgInSc1d1	celkový
proud	proud	k1gInSc1	proud
o	o	k7c4	o
nový	nový	k2eAgInSc4d1	nový
příspěvek	příspěvek	k1gInSc4	příspěvek
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Maxwellův	Maxwellův	k2eAgInSc1d1	Maxwellův
proud	proud	k1gInSc1	proud
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nemá	mít	k5eNaImIp3nS	mít
svou	svůj	k3xOyFgFnSc4	svůj
podstatu	podstata	k1gFnSc4	podstata
v	v	k7c6	v
průchodu	průchod	k1gInSc6	průchod
nosičů	nosič	k1gMnPc2	nosič
náboje	náboj	k1gInSc2	náboj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyjádření	vyjádření	k1gNnSc1	vyjádření
pomocí	pomocí	k7c2	pomocí
proudové	proudový	k2eAgFnSc2d1	proudová
hustoty	hustota	k1gFnSc2	hustota
je	být	k5eAaImIp3nS	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
j	j	k?	j
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
M	M	kA	M
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
ε	ε	k?	ε
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc4	mathbf
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
Max	Max	k1gMnSc1	Max
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gMnSc1	partial
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
E	E	kA	E
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
t	t	k?	t
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
Maxwellův	Maxwellův	k2eAgInSc1d1	Maxwellův
proud	proud	k1gInSc1	proud
nesouvisí	souviset	k5eNaImIp3nS	souviset
přímo	přímo	k6eAd1	přímo
s	s	k7c7	s
pohybem	pohyb	k1gInSc7	pohyb
nábojů	náboj	k1gInPc2	náboj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
časovou	časový	k2eAgFnSc7d1	časová
změnou	změna	k1gFnSc7	změna
elektrického	elektrický	k2eAgNnSc2d1	elektrické
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Součet	součet	k1gInSc1	součet
polarizačního	polarizační	k2eAgInSc2d1	polarizační
a	a	k8xC	a
Maxwellova	Maxwellův	k2eAgInSc2d1	Maxwellův
proudu	proud	k1gInSc2	proud
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xS	jako
posuvný	posuvný	k2eAgInSc1d1	posuvný
proud	proud	k1gInSc1	proud
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
tak	tak	k9	tak
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gFnSc4	jejich
hustotu	hustota	k1gFnSc4	hustota
lze	lze	k6eAd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
j	j	k?	j
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
M	M	kA	M
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
j	j	k?	j
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
<s>
o	o	k7c6	o
</s>
</p>
<p>
<s>
l	l	kA	l
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
ε	ε	k?	ε
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
</p>
<p>
<s>
P	P	kA	P
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
</p>
<p>
<s>
D	D	kA	D
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc4	mathbf
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
Max	Max	k1gMnSc1	Max
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
pol	pola	k1gFnPc2	pola
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gMnSc1	partial
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
E	E	kA	E
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
t	t	k?	t
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gMnSc1	partial
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
P	P	kA	P
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
t	t	k?	t
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gMnSc1	partial
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
D	D	kA	D
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
t	t	k?	t
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
<g/>
tedy	tedy	k9	tedy
jako	jako	k9	jako
změnu	změna	k1gFnSc4	změna
elektrické	elektrický	k2eAgFnSc2d1	elektrická
indukce	indukce	k1gFnSc2	indukce
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
D	D	kA	D
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
ε	ε	k?	ε
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
P	P	kA	P
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
D	D	kA	D
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
E	E	kA	E
<g/>
}	}	kIx)	}
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
P	P	kA	P
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
zvané	zvaný	k2eAgNnSc1d1	zvané
elektrické	elektrický	k2eAgNnSc1d1	elektrické
posunutí	posunutí	k1gNnSc1	posunutí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Takto	takto	k6eAd1	takto
nově	nově	k6eAd1	nově
zobecněný	zobecněný	k2eAgInSc1d1	zobecněný
celkový	celkový	k2eAgInSc1d1	celkový
proud	proud	k1gInSc1	proud
již	již	k6eAd1	již
vyhovuje	vyhovovat	k5eAaImIp3nS	vyhovovat
zákonu	zákon	k1gInSc3	zákon
zachování	zachování	k1gNnSc4	zachování
el.	el.	k?	el.
náboje	náboj	k1gInSc2	náboj
a	a	k8xC	a
plyne	plynout	k5eAaImIp3nS	plynout
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
správné	správný	k2eAgNnSc1d1	správné
zobecnění	zobecnění	k1gNnSc1	zobecnění
Ampérova	Ampérův	k2eAgInSc2d1	Ampérův
zákona	zákon	k1gInSc2	zákon
pro	pro	k7c4	pro
nestacionární	stacionární	k2eNgNnSc4d1	nestacionární
elektromagnetické	elektromagnetický	k2eAgNnSc4d1	elektromagnetické
pole	pole	k1gNnSc4	pole
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
hustota	hustota	k1gFnSc1	hustota
celkového	celkový	k2eAgInSc2d1	celkový
proudu	proud	k1gInSc2	proud
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
j	j	k?	j
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
j	j	k?	j
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
<s>
o	o	k7c6	o
</s>
</p>
<p>
<s>
l	l	kA	l
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
j	j	k?	j
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
M	M	kA	M
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
j	j	k?	j
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
<s>
o	o	k7c6	o
</s>
</p>
<p>
<s>
l	l	kA	l
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
j	j	k?	j
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc4	mathbf
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
vol	vol	k6eAd1	vol
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
Max	Max	k1gMnSc1	Max
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
pol	pola	k1gFnPc2	pola
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
mag	mag	k?	mag
<g/>
}	}	kIx)	}
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
<g/>
dostaneme	dostat	k5eAaPmIp1nP	dostat
divergencí	divergence	k1gFnSc7	divergence
Ampérova	Ampérův	k2eAgInSc2d1	Ampérův
zákona	zákon	k1gInSc2	zákon
pro	pro	k7c4	pro
celkový	celkový	k2eAgInSc4d1	celkový
proud	proud	k1gInSc4	proud
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
div	div	k1gInSc1	div
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
<s>
μ	μ	k?	μ
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
rot	rota	k1gFnPc2	rota
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
B	B	kA	B
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
div	div	k1gInSc1	div
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
j	j	k?	j
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
<s>
o	o	k7c6	o
</s>
</p>
<p>
<s>
l	l	kA	l
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
div	div	k1gInSc1	div
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
</p>
<p>
<s>
D	D	kA	D
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
div	div	k1gInSc1	div
</s>
</p>
<p>
</p>
<p>
<s>
rot	rota	k1gFnPc2	rota
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
M	M	kA	M
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
operatorname	operatornam	k1gInSc5	operatornam
{	{	kIx(	{
<g/>
div	div	k1gInSc4	div
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
left	left	k1gMnSc1	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
operatorname	operatornam	k1gInSc5	operatornam
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
rot	rota	k1gFnPc2	rota
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
B	B	kA	B
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
operatorname	operatornam	k1gInSc5	operatornam
{	{	kIx(	{
<g/>
div	div	k1gInSc4	div
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
vol	vol	k6eAd1	vol
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
operatorname	operatornam	k1gInSc5	operatornam
{	{	kIx(	{
<g/>
div	div	k1gInSc4	div
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gMnSc1	partial
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
D	D	kA	D
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
t	t	k?	t
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
operatorname	operatornam	k1gInSc5	operatornam
{	{	kIx(	{
<g/>
div	div	k1gInSc4	div
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
operatorname	operatornam	k1gInSc5	operatornam
{	{	kIx(	{
<g/>
rot	rota	k1gFnPc2	rota
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
tedy	tedy	k9	tedy
díky	díky	k7c3	díky
nulovosti	nulovost	k1gFnSc3	nulovost
divergence	divergence	k1gFnSc2	divergence
rotace	rotace	k1gFnSc2	rotace
a	a	k8xC	a
s	s	k7c7	s
uvážením	uvážení	k1gNnSc7	uvážení
třetí	třetí	k4xOgFnSc2	třetí
Maxwellovy	Maxwellův	k2eAgFnSc2d1	Maxwellova
rovnice	rovnice	k1gFnSc2	rovnice
pro	pro	k7c4	pro
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
indukci	indukce	k1gFnSc4	indukce
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
div	div	k1gInSc1	div
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
j	j	k?	j
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
<s>
o	o	k7c6	o
</s>
</p>
<p>
<s>
l	l	kA	l
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
</p>
<p>
<s>
ρ	ρ	k?	ρ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
<s>
o	o	k7c6	o
</s>
</p>
<p>
<s>
l	l	kA	l
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
0	[number]	k4	0
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
operatorname	operatornam	k1gInSc5	operatornam
{	{	kIx(	{
<g/>
div	div	k1gInSc4	div
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
vol	vol	k6eAd1	vol
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
vol	vol	k6eAd1	vol
<g/>
}	}	kIx)	}
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
t	t	k?	t
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
správná	správný	k2eAgFnSc1d1	správná
rovnice	rovnice	k1gFnSc1	rovnice
kontinuity	kontinuita	k1gFnSc2	kontinuita
<g/>
.	.	kIx.	.
<g/>
Ampérův	Ampérův	k2eAgInSc1d1	Ampérův
zákon	zákon	k1gInSc1	zákon
celkového	celkový	k2eAgInSc2d1	celkový
proudu	proud	k1gInSc2	proud
lze	lze	k6eAd1	lze
pak	pak	k6eAd1	pak
také	také	k9	také
přepsat	přepsat	k5eAaPmF	přepsat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
<s>
μ	μ	k?	μ
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
rot	rota	k1gFnPc2	rota
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
B	B	kA	B
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
j	j	k?	j
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
<s>
o	o	k7c6	o
</s>
</p>
<p>
<s>
l	l	kA	l
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
</p>
<p>
<s>
D	D	kA	D
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
rot	rota	k1gFnPc2	rota
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
M	M	kA	M
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
operatorname	operatornam	k1gInSc5	operatornam
{	{	kIx(	{
<g/>
rot	rota	k1gFnPc2	rota
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
B	B	kA	B
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
<g />
.	.	kIx.	.
</s>
<s>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc4	mathbf
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
vol	vol	k6eAd1	vol
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gMnSc1	partial
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
D	D	kA	D
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
t	t	k?	t
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
operatorname	operatornam	k1gInSc5	operatornam
{	{	kIx(	{
<g/>
rot	rota	k1gFnPc2	rota
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
tedy	tedy	k9	tedy
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
rot	rota	k1gFnPc2	rota
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
<s>
μ	μ	k?	μ
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
B	B	kA	B
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
rot	rota	k1gFnPc2	rota
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
M	M	kA	M
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
j	j	k?	j
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
<s>
o	o	k7c6	o
</s>
</p>
<p>
<s>
l	l	kA	l
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
</p>
<p>
<s>
D	D	kA	D
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
operatorname	operatornam	k1gInSc5	operatornam
{	{	kIx(	{
<g/>
rot	rota	k1gFnPc2	rota
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
left	left	k1gMnSc1	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
B	B	kA	B
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
operatorname	operatornam	k1gInSc5	operatornam
{	{	kIx(	{
<g/>
rot	rota	k1gFnPc2	rota
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
vol	vol	k6eAd1	vol
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gMnSc1	partial
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
D	D	kA	D
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
t	t	k?	t
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
tedy	tedy	k9	tedy
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
rot	rota	k1gFnPc2	rota
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
j	j	k?	j
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
<s>
o	o	k7c6	o
</s>
</p>
<p>
<s>
l	l	kA	l
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
</p>
<p>
<s>
D	D	kA	D
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
operatorname	operatornam	k1gInSc5	operatornam
{	{	kIx(	{
<g/>
rot	rota	k1gFnPc2	rota
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
H	H	kA	H
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
vol	vol	k6eAd1	vol
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gMnSc1	partial
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
D	D	kA	D
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
t	t	k?	t
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc1	první
Maxwellova	Maxwellův	k2eAgFnSc1d1	Maxwellova
rovnice	rovnice	k1gFnSc1	rovnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Horák	Horák	k1gMnSc1	Horák
Z.	Z.	kA	Z.
<g/>
,	,	kIx,	,
Krupka	Krupka	k1gMnSc1	Krupka
F.	F.	kA	F.
<g/>
:	:	kIx,	:
Fyzika	fyzik	k1gMnSc2	fyzik
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
v	v	k7c6	v
koedici	koedice	k1gFnSc6	koedice
s	s	k7c7	s
ALFA	alfa	k1gNnSc7	alfa
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1981	[number]	k4	1981
</s>
</p>
<p>
<s>
Feynman	Feynman	k1gMnSc1	Feynman
R.	R.	kA	R.
P.	P.	kA	P.
<g/>
,	,	kIx,	,
Leighton	Leighton	k1gInSc1	Leighton
R.	R.	kA	R.
B.	B.	kA	B.
<g/>
,	,	kIx,	,
Sands	Sands	k1gInSc1	Sands
M.	M.	kA	M.
<g/>
:	:	kIx,	:
Feynmanovy	Feynmanův	k2eAgFnPc4d1	Feynmanova
přednášky	přednáška	k1gFnPc4	přednáška
z	z	k7c2	z
fyziky	fyzika	k1gFnSc2	fyzika
–	–	k?	–
díl	díl	k1gInSc1	díl
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
české	český	k2eAgNnSc1d1	české
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
Fragment	fragment	k1gInSc1	fragment
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7200	[number]	k4	7200
<g/>
-	-	kIx~	-
<g/>
405	[number]	k4	405
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Feynman	Feynman	k1gMnSc1	Feynman
<g/>
,	,	kIx,	,
R.	R.	kA	R.
P.	P.	kA	P.
<g/>
,	,	kIx,	,
Leighton	Leighton	k1gInSc1	Leighton
<g/>
,	,	kIx,	,
R.	R.	kA	R.
B.	B.	kA	B.
<g/>
,	,	kIx,	,
Sands	Sands	k1gInSc1	Sands
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
:	:	kIx,	:
Feynmanovy	Feynmanův	k2eAgFnPc4d1	Feynmanova
přednášky	přednáška	k1gFnPc4	přednáška
z	z	k7c2	z
fyziky	fyzika	k1gFnSc2	fyzika
–	–	k?	–
díl	díl	k1gInSc1	díl
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
české	český	k2eAgNnSc1d1	české
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
Fragment	fragment	k1gInSc1	fragment
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7200	[number]	k4	7200
<g/>
-	-	kIx~	-
<g/>
420	[number]	k4	420
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sedlák	Sedlák	k1gMnSc1	Sedlák
B.	B.	kA	B.
<g/>
,	,	kIx,	,
Štoll	Štoll	k1gMnSc1	Štoll
I.	I.	kA	I.
<g/>
:	:	kIx,	:
Elektřina	elektřina	k1gFnSc1	elektřina
a	a	k8xC	a
magnetismus	magnetismus	k1gInSc1	magnetismus
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-200-0172-7	[number]	k4	80-200-0172-7
</s>
</p>
<p>
<s>
Odmaturuj	odmaturovat	k5eAaPmRp2nS	odmaturovat
z	z	k7c2	z
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
,	,	kIx,	,
DIDAKTIS	DIDAKTIS	kA	DIDAKTIS
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86285	[number]	k4	86285
<g/>
-	-	kIx~	-
<g/>
39	[number]	k4	39
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
<g/>
od	od	k7c2	od
kapitola	kapitola	k1gFnSc1	kapitola
5.2	[number]	k4	5.2
Elektrický	elektrický	k2eAgInSc1d1	elektrický
proud	proud	k1gInSc1	proud
v	v	k7c6	v
látkách	látka	k1gFnPc6	látka
do	do	k7c2	do
kapitoly	kapitola	k1gFnSc2	kapitola
5.6	[number]	k4	5.6
<g/>
,	,	kIx,	,
strany	strana	k1gFnPc1	strana
od	od	k7c2	od
111	[number]	k4	111
do	do	k7c2	do
152	[number]	k4	152
</s>
</p>
<p>
<s>
Kvasnica	Kvasnica	k1gMnSc1	Kvasnica
J.	J.	kA	J.
<g/>
:	:	kIx,	:
Teorie	teorie	k1gFnSc1	teorie
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
pole	pole	k1gNnSc2	pole
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Elektrická	elektrický	k2eAgFnSc1d1	elektrická
síla	síla	k1gFnSc1	síla
</s>
</p>
<p>
<s>
Elektrické	elektrický	k2eAgNnSc1d1	elektrické
napětí	napětí	k1gNnSc1	napětí
</s>
</p>
<p>
<s>
Elektrický	elektrický	k2eAgInSc1d1	elektrický
náboj	náboj	k1gInSc1	náboj
</s>
</p>
<p>
<s>
Elektrický	elektrický	k2eAgInSc1d1	elektrický
obvod	obvod	k1gInSc1	obvod
</s>
</p>
<p>
<s>
Intenzita	intenzita	k1gFnSc1	intenzita
elektrického	elektrický	k2eAgNnSc2d1	elektrické
pole	pole	k1gNnSc2	pole
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
elektrický	elektrický	k2eAgInSc4d1	elektrický
proud	proud	k1gInSc4	proud
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Multimediální	multimediální	k2eAgFnSc1d1	multimediální
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
Fyziky	fyzika	k1gFnSc2	fyzika
–	–	k?	–
ELEKTŘINA	elektřina	k1gFnSc1	elektřina
A	a	k8xC	a
MAGNETISMUS	magnetismus	k1gInSc1	magnetismus
od	od	k7c2	od
3.2	[number]	k4	3.2
VZNIK	vznik	k1gInSc1	vznik
ELEKTRICKÉHO	elektrický	k2eAgInSc2d1	elektrický
PROUDU	proud	k1gInSc2	proud
do	do	k7c2	do
3.11	[number]	k4	3.11
FYZIKÁLNÍ	fyzikální	k2eAgInPc1d1	fyzikální
ZÁKLADY	základ	k1gInPc1	základ
ELEKTRONIKY	elektronika	k1gFnSc2	elektronika
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Reichl	Reichl	k1gMnSc1	Reichl
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Všetička	Všetička	k1gFnSc1	Všetička
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Maxwellův	Maxwellův	k2eAgInSc1d1	Maxwellův
proud	proud	k1gInSc1	proud
na	na	k7c6	na
WIKI	WIKI	kA	WIKI
Matematicko-fyzikální	matematickoyzikální	k2eAgFnSc2d1	matematicko-fyzikální
fakulty	fakulta	k1gFnSc2	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
</s>
</p>
