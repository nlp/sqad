<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
terestrických	terestrický	k2eAgFnPc2d1	terestrická
planet	planeta	k1gFnPc2	planeta
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
pevný	pevný	k2eAgInSc4d1	pevný
kamenitý	kamenitý	k2eAgInSc4d1	kamenitý
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
