<s>
Severn	Severn	k1gNnSc1	Severn
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
River	River	k1gMnSc1	River
Severn	Severn	k1gMnSc1	Severn
<g/>
,	,	kIx,	,
velšsky	velšsky	k6eAd1	velšsky
Afon	Afon	k1gInSc1	Afon
Hafren	Hafrna	k1gFnPc2	Hafrna
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
a	a	k8xC	a
ve	v	k7c6	v
Walesu	Wales	k1gInSc6	Wales
(	(	kIx(	(
<g/>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
310	[number]	k4	310
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
(	(	kIx(	(
<g/>
s	s	k7c7	s
estuárem	estuár	k1gInSc7	estuár
až	až	k9	až
390	[number]	k4	390
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
nejdelší	dlouhý	k2eAgFnSc7d3	nejdelší
řekou	řeka	k1gFnSc7	řeka
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Povodí	povodí	k1gNnSc1	povodí
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
21000	[number]	k4	21000
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Pramení	pramenit	k5eAaImIp3nP	pramenit
na	na	k7c6	na
východních	východní	k2eAgInPc6d1	východní
svazích	svah	k1gInPc6	svah
Kambrijských	Kambrijský	k2eAgMnPc2d1	Kambrijský
hor.	hor.	k?	hor.
Teče	téct	k5eAaImIp3nS	téct
převážně	převážně	k6eAd1	převážně
po	po	k7c6	po
rovině	rovina	k1gFnSc6	rovina
<g/>
.	.	kIx.	.
</s>
<s>
Ústí	ústí	k1gNnSc1	ústí
do	do	k7c2	do
Bristolského	bristolský	k2eAgInSc2d1	bristolský
zálivu	záliv	k1gInSc2	záliv
v	v	k7c6	v
Keltském	keltský	k2eAgNnSc6d1	keltské
moři	moře	k1gNnSc6	moře
(	(	kIx(	(
<g/>
Atlantský	atlantský	k2eAgInSc1d1	atlantský
oceán	oceán	k1gInSc1	oceán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
dešťový	dešťový	k2eAgInSc1d1	dešťový
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
vodnosti	vodnost	k1gFnSc2	vodnost
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
a	a	k8xC	a
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
mělkou	mělký	k2eAgFnSc7d1	mělká
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc1d1	roční
průtok	průtok	k1gInSc1	průtok
vody	voda	k1gFnSc2	voda
činí	činit	k5eAaImIp3nS	činit
přibližně	přibližně	k6eAd1	přibližně
250	[number]	k4	250
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Vodní	vodní	k2eAgFnSc1d1	vodní
doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
od	od	k7c2	od
Stourportu	Stourport	k1gInSc2	Stourport
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
je	být	k5eAaImIp3nS	být
dostupná	dostupný	k2eAgFnSc1d1	dostupná
pro	pro	k7c4	pro
námořní	námořní	k2eAgFnSc4d1	námořní
lodě	loď	k1gFnPc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
kanály	kanál	k1gInPc7	kanál
s	s	k7c7	s
řekami	řeka	k1gFnPc7	řeka
Temže	Temže	k1gFnSc1	Temže
<g/>
,	,	kIx,	,
Trent	Trent	k1gInSc1	Trent
<g/>
,	,	kIx,	,
Mersey	Mersey	k1gInPc1	Mersey
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
estuár	estuár	k1gInSc4	estuár
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
postaven	postaven	k2eAgInSc4d1	postaven
vysoký	vysoký	k2eAgInSc4d1	vysoký
most	most	k1gInSc4	most
a	a	k8xC	a
nedaleko	nedaleko	k7c2	nedaleko
města	město	k1gNnSc2	město
Bristol	Bristol	k1gInSc1	Bristol
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
tunel	tunel	k1gInSc1	tunel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
leží	ležet	k5eAaImIp3nS	ležet
města	město	k1gNnSc2	město
Shrewsbury	Shrewsbura	k1gFnSc2	Shrewsbura
<g/>
,	,	kIx,	,
Worcester	Worcester	k1gInSc1	Worcester
<g/>
,	,	kIx,	,
Gloucester	Gloucester	k1gInSc1	Gloucester
a	a	k8xC	a
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
estuáru	estuár	k1gInSc2	estuár
Newport	Newport	k1gInSc1	Newport
a	a	k8xC	a
Cardiff	Cardiff	k1gInSc1	Cardiff
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
slouží	sloužit	k5eAaImIp3nS	sloužit
také	také	k9	také
k	k	k7c3	k
chlazení	chlazení	k1gNnSc3	chlazení
jaderných	jaderný	k2eAgFnPc2d1	jaderná
elektráren	elektrárna	k1gFnPc2	elektrárna
Oldbury	Oldbura	k1gFnSc2	Oldbura
a	a	k8xC	a
Berkeley	Berkelea	k1gFnSc2	Berkelea
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
С	С	k?	С
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Severn	Severna	k1gFnPc2	Severna
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
