<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
socha	socha	k1gFnSc1	socha
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
s	s	k7c7	s
dítětem	dítě	k1gNnSc7	dítě
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
Narození	narození	k1gNnSc2	narození
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
ve	v	k7c6	v
Strašíně	Strašína	k1gFnSc6	Strašína
u	u	k7c2	u
Sušice	Sušice	k1gFnSc2	Sušice
<g/>
?	?	kIx.	?
</s>
