<s>
Strašínská	Strašínský	k2eAgFnSc1d1	Strašínská
madona	madona	k1gFnSc1	madona
(	(	kIx(	(
<g/>
též	též	k9	též
Assumpta	Assumpta	k1gFnSc1	Assumpta
ze	z	k7c2	z
Strašína	Strašín	k1gInSc2	Strašín
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
socha	socha	k1gFnSc1	socha
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
s	s	k7c7	s
dítětem	dítě	k1gNnSc7	dítě
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
Narození	narození	k1gNnSc2	narození
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
ve	v	k7c6	v
Strašíně	Strašína	k1gFnSc6	Strašína
u	u	k7c2	u
Sušice	Sušice	k1gFnSc2	Sušice
<g/>
.	.	kIx.	.
</s>
