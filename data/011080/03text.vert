<p>
<s>
Strašínská	Strašínský	k2eAgFnSc1d1	Strašínská
madona	madona	k1gFnSc1	madona
(	(	kIx(	(
<g/>
též	též	k9	též
Assumpta	Assumpta	k1gFnSc1	Assumpta
ze	z	k7c2	z
Strašína	Strašín	k1gInSc2	Strašín
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
socha	socha	k1gFnSc1	socha
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
s	s	k7c7	s
dítětem	dítě	k1gNnSc7	dítě
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
Narození	narození	k1gNnSc2	narození
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
ve	v	k7c6	v
Strašíně	Strašína	k1gFnSc6	Strašína
u	u	k7c2	u
Sušice	Sušice	k1gFnSc2	Sušice
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
socha	socha	k1gFnSc1	socha
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
kopii	kopie	k1gFnSc4	kopie
té	ten	k3xDgFnSc2	ten
původní	původní	k2eAgFnSc2d1	původní
z	z	k7c2	z
konce	konec	k1gInSc2	konec
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
ukradena	ukrást	k5eAaPmNgFnS	ukrást
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
a	a	k8xC	a
dosud	dosud	k6eAd1	dosud
se	se	k3xPyFc4	se
nenašla	najít	k5eNaPmAgFnS	najít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Ikonografie	ikonografie	k1gFnSc1	ikonografie
sochy	socha	k1gFnSc2	socha
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
středověkých	středověký	k2eAgFnPc2d1	středověká
interpretací	interpretace	k1gFnPc2	interpretace
Zjevení	zjevení	k1gNnSc2	zjevení
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
popisuje	popisovat	k5eAaImIp3nS	popisovat
Pannu	Panna	k1gFnSc4	Panna
Marii	Maria	k1gFnSc4	Maria
jako	jako	k8xC	jako
ženu	žena	k1gFnSc4	žena
sluncem	slunce	k1gNnSc7	slunce
oděnou	oděný	k2eAgFnSc4d1	oděná
s	s	k7c7	s
měsícem	měsíc	k1gInSc7	měsíc
pod	pod	k7c7	pod
nohama	noha	k1gFnPc7	noha
<g/>
.	.	kIx.	.
</s>
<s>
Originální	originální	k2eAgFnSc1d1	originální
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
plastika	plastika	k1gFnSc1	plastika
madony	madona	k1gFnSc2	madona
byla	být	k5eAaImAgFnS	být
vyřezaná	vyřezaný	k2eAgFnSc1d1	vyřezaná
z	z	k7c2	z
lipového	lipový	k2eAgNnSc2d1	lipové
dřeva	dřevo	k1gNnSc2	dřevo
a	a	k8xC	a
bez	bez	k7c2	bez
korunky	korunka	k1gFnSc2	korunka
měřila	měřit	k5eAaImAgFnS	měřit
110	[number]	k4	110
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
tak	tak	k6eAd1	tak
zvanou	zvaný	k2eAgFnSc4d1	zvaná
Assumptu	Assumpta	k1gFnSc4	Assumpta
–	–	k?	–
Královnu	královna	k1gFnSc4	královna
nebes	nebesa	k1gNnPc2	nebesa
<g/>
.	.	kIx.	.
</s>
<s>
Madona	Madona	k1gFnSc1	Madona
stojící	stojící	k2eAgFnSc1d1	stojící
na	na	k7c6	na
půlměsíci	půlměsíc	k1gInSc6	půlměsíc
obráceném	obrácený	k2eAgInSc6d1	obrácený
směrem	směr	k1gInSc7	směr
dolů	dolů	k6eAd1	dolů
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
Nanebevzatí	nanebevzatý	k2eAgMnPc1d1	nanebevzatý
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
jako	jako	k8xC	jako
Královny	královna	k1gFnSc2	královna
nebes	nebesa	k1gNnPc2	nebesa
s	s	k7c7	s
žezlem	žezlo	k1gNnSc7	žezlo
v	v	k7c6	v
pravé	pravý	k2eAgFnSc6d1	pravá
ruce	ruka	k1gFnSc6	ruka
a	a	k8xC	a
s	s	k7c7	s
dítětem	dítě	k1gNnSc7	dítě
v	v	k7c6	v
náručí	náručí	k1gNnSc6	náručí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původní	původní	k2eAgInPc4d1	původní
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
vyřezávanou	vyřezávaný	k2eAgFnSc4d1	vyřezávaná
sochu	socha	k1gFnSc4	socha
madony	madona	k1gFnSc2	madona
kostelu	kostel	k1gInSc3	kostel
věnoval	věnovat	k5eAaPmAgMnS	věnovat
hradní	hradní	k2eAgMnSc1d1	hradní
pán	pán	k1gMnSc1	pán
Půta	Půta	k1gMnSc1	Půta
Švihovský	Švihovský	k2eAgMnSc1d1	Švihovský
z	z	k7c2	z
Rýzmberka	Rýzmberka	k1gFnSc1	Rýzmberka
<g/>
,	,	kIx,	,
králův	králův	k2eAgMnSc1d1	králův
rádce	rádce	k1gMnSc1	rádce
a	a	k8xC	a
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
sudí	sudí	k1gMnPc1	sudí
Království	království	k1gNnSc2	království
českého	český	k2eAgInSc2d1	český
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	let	k1gInPc6	let
1434	[number]	k4	1434
<g/>
–	–	k?	–
<g/>
1444	[number]	k4	1444
<g/>
)	)	kIx)	)
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
o	o	k7c4	o
gotickou	gotický	k2eAgFnSc4d1	gotická
přestavbu	přestavba	k1gFnSc4	přestavba
kostela	kostel	k1gInSc2	kostel
Narození	narození	k1gNnSc2	narození
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
ve	v	k7c6	v
Strašíně	Strašína	k1gFnSc6	Strašína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zhotovení	zhotovení	k1gNnSc1	zhotovení
kopie	kopie	k1gFnSc2	kopie
==	==	k?	==
</s>
</p>
<p>
<s>
Kopie	kopie	k1gFnSc1	kopie
odcizeného	odcizený	k2eAgInSc2d1	odcizený
originálu	originál	k1gInSc2	originál
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
umělci	umělec	k1gMnPc7	umělec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ovládají	ovládat	k5eAaImIp3nP	ovládat
středověké	středověký	k2eAgFnPc4d1	středověká
techniky	technika	k1gFnPc4	technika
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
byl	být	k5eAaImAgInS	být
zhotoven	zhotovit	k5eAaPmNgInS	zhotovit
hliněný	hliněný	k2eAgInSc1d1	hliněný
model	model	k1gInSc1	model
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
proveden	proveden	k2eAgInSc1d1	proveden
odlitek	odlitek	k1gInSc1	odlitek
ze	z	k7c2	z
speciální	speciální	k2eAgFnSc2d1	speciální
hmoty	hmota	k1gFnSc2	hmota
Acrystal	Acrystal	k1gFnSc2	Acrystal
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
povrchově	povrchově	k6eAd1	povrchově
upravoval	upravovat	k5eAaImAgInS	upravovat
a	a	k8xC	a
opatřil	opatřit	k5eAaPmAgInS	opatřit
křídovým	křídový	k2eAgInSc7d1	křídový
povrchem	povrch	k1gInSc7	povrch
pro	pro	k7c4	pro
nanesení	nanesení	k1gNnSc4	nanesení
zlacení	zlacení	k1gNnSc2	zlacení
a	a	k8xC	a
barevné	barevný	k2eAgFnSc2d1	barevná
polychromie	polychromie	k1gFnSc2	polychromie
<g/>
.	.	kIx.	.
</s>
<s>
Závěrečnou	závěrečný	k2eAgFnSc4d1	závěrečná
restaurátorskou	restaurátorský	k2eAgFnSc4d1	restaurátorská
úpravu	úprava	k1gFnSc4	úprava
Strašínské	Strašínský	k2eAgFnSc2d1	Strašínská
madony	madona	k1gFnSc2	madona
provedla	provést	k5eAaPmAgFnS	provést
akademická	akademický	k2eAgFnSc1d1	akademická
malířka	malířka	k1gFnSc1	malířka
a	a	k8xC	a
restaurátorka	restaurátorka	k1gFnSc1	restaurátorka
Klára	Klára	k1gFnSc1	Klára
Kolářová	Kolářová	k1gFnSc1	Kolářová
<g/>
.	.	kIx.	.
</s>
<s>
Kopie	kopie	k1gFnSc1	kopie
madony	madona	k1gFnSc2	madona
byla	být	k5eAaImAgFnS	být
osazena	osadit	k5eAaPmNgFnS	osadit
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
původního	původní	k2eAgInSc2d1	původní
orginálu	orginál	k1gInSc2	orginál
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
Poutní	poutní	k2eAgFnSc1d1	poutní
tradice	tradice	k1gFnSc1	tradice
Šumavy	Šumava	k1gFnSc2	Šumava
<g/>
,	,	kIx,	,
Pokorný	Pokorný	k1gMnSc1	Pokorný
Milan	Milan	k1gMnSc1	Milan
<g/>
,	,	kIx,	,
Sušice	Sušice	k1gFnSc1	Sušice
2002	[number]	k4	2002
</s>
</p>
<p>
<s>
Slovník	slovník	k1gInSc1	slovník
bilické	bilický	k2eAgFnSc2d1	bilický
ikonografie	ikonografie	k1gFnSc2	ikonografie
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Royt	Royt	k1gMnSc1	Royt
<g/>
,	,	kIx,	,
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
Návrat	návrat	k1gInSc1	návrat
Strašínské	Strašínský	k2eAgFnSc2d1	Strašínská
madony	madona	k1gFnSc2	madona
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
