<s>
Evropská	evropský	k2eAgFnSc1d1
atletická	atletický	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
European	European	k1gMnSc1
Athletic	Athletice	k1gFnPc2
Association	Association	k1gInSc4
–	–	k?
EAA	EAA	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
evropské	evropský	k2eAgNnSc4d1
sdružení	sdružení	k1gNnSc4
51	#num#	k4
národních	národní	k2eAgInPc2d1
lehkoatletických	lehkoatletický	k2eAgInPc2d1
svazů	svaz	k1gInPc2
napojených	napojený	k2eAgInPc2d1
na	na	k7c4
světovou	světový	k2eAgFnSc4d1
asociaci	asociace	k1gFnSc4
IAAF	IAAF	kA
<g/>
.	.	kIx.
</s>