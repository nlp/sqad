<s>
Evropská	evropský	k2eAgFnSc1d1
atletická	atletický	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
atletická	atletický	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
Motto	motto	k1gNnSc1
</s>
<s>
Your	Your	k1gInSc1
Sport	sport	k1gInSc1
for	forum	k1gNnPc2
Life	Lif	k1gFnSc2
Vznik	vznik	k1gInSc1
</s>
<s>
1932	#num#	k4
Typ	typ	k1gInSc1
</s>
<s>
sportovní	sportovní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
Právní	právní	k2eAgFnSc1d1
forma	forma	k1gFnSc1
</s>
<s>
kontinentální	kontinentální	k2eAgInSc1d1
Účel	účel	k1gInSc1
</s>
<s>
atletika	atletika	k1gFnSc1
Sídlo	sídlo	k1gNnSc1
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
Švýcarsko	Švýcarsko	k1gNnSc1
Místo	místo	k7c2
</s>
<s>
Lausanne	Lausanne	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
46	#num#	k4
<g/>
°	°	k?
<g/>
31	#num#	k4
<g/>
′	′	k?
<g/>
8,27	8,27	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
6	#num#	k4
<g/>
°	°	k?
<g/>
37	#num#	k4
<g/>
′	′	k?
<g/>
31,49	31,49	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Působnost	působnost	k1gFnSc1
</s>
<s>
Evropa	Evropa	k1gFnSc1
Úřední	úřední	k2eAgFnSc1d1
jazyk	jazyk	k1gInSc4
</s>
<s>
angličtina	angličtina	k1gFnSc1
<g/>
,	,	kIx,
francouzština	francouzština	k1gFnSc1
Členové	člen	k1gMnPc1
</s>
<s>
51	#num#	k4
národních	národní	k2eAgInPc2d1
lehkoatletických	lehkoatletický	k2eAgInPc2d1
svazů	svaz	k1gInPc2
Prezident	prezident	k1gMnSc1
</s>
<s>
Svein	Svein	k2eAgMnSc1d1
Arne	Arne	k1gMnSc1
Hansen	Hansna	k1gFnPc2
Mateřská	mateřský	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
</s>
<s>
IAAF	IAAF	kA
Oficiální	oficiální	k2eAgInSc4d1
web	web	k1gInSc4
</s>
<s>
www.european-athletics.org	www.european-athletics.org	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
atletická	atletický	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
European	European	k1gMnSc1
Athletic	Athletice	k1gFnPc2
Association	Association	k1gInSc4
–	–	k?
EAA	EAA	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
evropské	evropský	k2eAgNnSc4d1
sdružení	sdružení	k1gNnSc4
51	#num#	k4
národních	národní	k2eAgInPc2d1
lehkoatletických	lehkoatletický	k2eAgInPc2d1
svazů	svaz	k1gInPc2
napojených	napojený	k2eAgInPc2d1
na	na	k7c4
světovou	světový	k2eAgFnSc4d1
asociaci	asociace	k1gFnSc4
IAAF	IAAF	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2009	#num#	k4
používá	používat	k5eAaImIp3nS
název	název	k1gInSc1
European	Europeany	k1gInPc2
Athletics	Athleticsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1933	#num#	k4
v	v	k7c6
Berlíně	Berlín	k1gInSc6
na	na	k7c6
zasedání	zasedání	k1gNnSc6
IAAF	IAAF	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgMnSc7
předsedou	předseda	k1gMnSc7
byl	být	k5eAaImAgMnS
Szilard	Szilard	k1gMnSc1
Stankovits	Stankovitsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
mnohaleté	mnohaletý	k2eAgFnSc6d1
odmlce	odmlka	k1gFnSc6
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
asociaci	asociace	k1gFnSc4
obnovit	obnovit	k5eAaPmF
roku	rok	k1gInSc2
1969	#num#	k4
na	na	k7c6
půdě	půda	k1gFnSc6
zasedání	zasedání	k1gNnSc2
IAAF	IAAF	kA
v	v	k7c6
Bukurešti	Bukurešť	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgMnSc1
samostatný	samostatný	k2eAgInSc1d1
kongres	kongres	k1gInSc1
EAA	EAA	kA
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
zorganizovat	zorganizovat	k5eAaPmF
7	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1970	#num#	k4
v	v	k7c6
Paříži	Paříž	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gMnSc7
předsedou	předseda	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Adrian	Adrian	k1gMnSc1
Paulen	Paulen	k2eAgMnSc1d1
(	(	kIx(
<g/>
Nizozemí	Nizozemí	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
se	se	k3xPyFc4
do	do	k7c2
čela	čelo	k1gNnSc2
EAA	EAA	kA
dostal	dostat	k5eAaPmAgMnS
Švýcar	Švýcar	k1gMnSc1
Hansjörg	Hansjörg	k1gMnSc1
Wirz	Wirz	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
2009	#num#	k4
byla	být	k5eAaImAgFnS
asociace	asociace	k1gFnSc1
přejmenována	přejmenovat	k5eAaPmNgFnS
na	na	k7c4
European	European	k1gInSc4
Athletics	Athleticsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedním	jeden	k4xCgMnSc7
ze	z	k7c2
tří	tři	k4xCgMnPc2
místopředsedů	místopředseda	k1gMnPc2
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
zvolen	zvolit	k5eAaPmNgMnS
Karel	Karel	k1gMnSc1
Pilný	pilný	k2eAgMnSc1d1
z	z	k7c2
Česka	Česko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wirz	Wirz	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
roku	rok	k1gInSc2
2011	#num#	k4
na	na	k7c4
čtvrté	čtvrtý	k4xOgNnSc4
funkční	funkční	k2eAgNnSc4d1
období	období	k1gNnSc4
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
vystřídán	vystřídán	k2eAgInSc4d1
Sveinem	Svein	k1gMnSc7
Arne	Arne	k1gMnSc7
Hansenem	Hansen	k1gMnSc7
z	z	k7c2
Norska	Norsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Prezidenti	prezident	k1gMnPc1
EAA	EAA	kA
</s>
<s>
Jméno	jméno	k1gNnSc1
</s>
<s>
Stát	stát	k1gInSc1
</s>
<s>
Doba	doba	k1gFnSc1
v	v	k7c6
úřadu	úřad	k1gInSc6
</s>
<s>
Adriaan	Adriaan	k1gMnSc1
PaulenNizozemsko	PaulenNizozemsko	k1gNnSc4
Nizozemsko	Nizozemsko	k1gNnSc1
<g/>
1969	#num#	k4
až	až	k8xS
1976	#num#	k4
</s>
<s>
Arthur	Arthur	k1gMnSc1
GoldSpojené	GoldSpojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
1976	#num#	k4
až	až	k8xS
1987	#num#	k4
</s>
<s>
Carl-Olaf	Carl-Olaf	k1gMnSc1
HomenFinsko	HomenFinsko	k1gNnSc4
Finsko	Finsko	k1gNnSc1
<g/>
1987	#num#	k4
až	až	k8xS
1999	#num#	k4
</s>
<s>
Hansjörg	Hansjörg	k1gMnSc1
WirzŠvýcarsko	WirzŠvýcarsko	k1gNnSc4
Švýcarsko	Švýcarsko	k1gNnSc1
<g/>
1999	#num#	k4
až	až	k8xS
2015	#num#	k4
</s>
<s>
Svein	Svein	k2eAgMnSc1d1
Arne	Arne	k1gMnSc1
HansenNorsko	HansenNorsko	k1gNnSc4
Norsko	Norsko	k1gNnSc1
<g/>
2015	#num#	k4
až	až	k6eAd1
dosud	dosud	k6eAd1
</s>
<s>
Členství	členství	k1gNnSc1
</s>
<s>
Do	do	k7c2
asociace	asociace	k1gFnSc2
je	být	k5eAaImIp3nS
začleněno	začlenit	k5eAaPmNgNnS
51	#num#	k4
národních	národní	k2eAgInPc2d1
atletických	atletický	k2eAgInPc2d1
svazů	svaz	k1gInPc2
a	a	k8xC
federací	federace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posledním	poslední	k2eAgInSc7d1
přijatým	přijatý	k2eAgInSc7d1
členem	člen	k1gInSc7
bylo	být	k5eAaImAgNnS
v	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
Kosovo	Kosův	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Česko	Česko	k1gNnSc1
zastupuje	zastupovat	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
1993	#num#	k4
Český	český	k2eAgInSc1d1
atletický	atletický	k2eAgInSc1d1
svaz	svaz	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Mapa	mapa	k1gFnSc1
se	s	k7c7
šesti	šest	k4xCc7
kontinentálními	kontinentální	k2eAgFnPc7d1
členskými	členský	k2eAgFnPc7d1
federacemi	federace	k1gFnPc7
<g/>
,	,	kIx,
EAA	EAA	kA
je	být	k5eAaImIp3nS
modře	modro	k6eAd1
</s>
<s>
Organizované	organizovaný	k2eAgFnPc1d1
soutěže	soutěž	k1gFnPc1
</s>
<s>
EAA	EAA	kA
je	být	k5eAaImIp3nS
hlavním	hlavní	k2eAgMnSc7d1
pořadatelem	pořadatel	k1gMnSc7
řady	řada	k1gFnSc2
lehkoatletických	lehkoatletický	k2eAgFnPc2d1
soutěží	soutěž	k1gFnPc2
<g/>
,	,	kIx,
zejména	zejména	k9
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
v	v	k7c6
atletice	atletika	k1gFnSc6
</s>
<s>
Halové	halový	k2eAgNnSc1d1
mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
v	v	k7c6
atletice	atletika	k1gFnSc6
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
juniorů	junior	k1gMnPc2
v	v	k7c6
atletice	atletika	k1gFnSc6
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
v	v	k7c6
atletice	atletika	k1gFnSc6
do	do	k7c2
23	#num#	k4
let	léto	k1gNnPc2
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
v	v	k7c6
atletice	atletika	k1gFnSc6
do	do	k7c2
17	#num#	k4
let	léto	k1gNnPc2
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
v	v	k7c6
přespolním	přespolní	k2eAgInSc6d1
běhu	běh	k1gInSc6
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
družstev	družstvo	k1gNnPc2
v	v	k7c6
atletice	atletika	k1gFnSc6
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
IAAF	IAAF	kA
Council	Council	k1gInSc1
Meeting	meeting	k1gInSc1
<g/>
,	,	kIx,
Beijing	Beijing	k1gInSc1
<g/>
,	,	kIx,
15	#num#	k4
April	April	k1gInSc1
–	–	k?
Notes	notes	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
15	#num#	k4
April	April	k1gInSc1
2015	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Evropská	evropský	k2eAgFnSc1d1
atletická	atletický	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Web	web	k1gInSc1
European	Europeany	k1gInPc2
Athletics	Athleticsa	k1gFnPc2
</s>
<s>
Pilný	pilný	k2eAgInSc1d1
zvolen	zvolit	k5eAaPmNgMnS
místopředsedou	místopředseda	k1gMnSc7
EAA	EAA	kA
</s>
<s>
Rozpory	rozpor	k1gInPc1
EAA	EAA	kA
a	a	k8xC
EOV	EOV	kA
kvůli	kvůli	k7c3
Evropským	evropský	k2eAgFnPc3d1
hrám	hra	k1gFnPc3
2015	#num#	k4
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Atletické	atletický	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
a	a	k8xC
přehledy	přehled	k1gInPc1
Muži	muž	k1gMnPc1
</s>
<s>
Světové	světový	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
·	·	k?
Halové	halový	k2eAgInPc4d1
světové	světový	k2eAgInPc4d1
rekordy	rekord	k1gInPc4
·	·	k?
Juniorské	juniorský	k2eAgInPc4d1
světové	světový	k2eAgInPc4d1
rekordy	rekord	k1gInPc4
·	·	k?
Halové	halový	k2eAgInPc4d1
juniorské	juniorský	k2eAgInPc4d1
světové	světový	k2eAgInPc4d1
rekordy	rekord	k1gInPc4
·	·	k?
Evropské	evropský	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
·	·	k?
Halové	halový	k2eAgInPc1d1
evropské	evropský	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
·	·	k?
České	český	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
·	·	k?
Halové	halový	k2eAgInPc4d1
české	český	k2eAgInPc4d1
rekordy	rekord	k1gInPc4
Ženy	žena	k1gFnPc5
</s>
<s>
Světové	světový	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
·	·	k?
Halové	halový	k2eAgInPc4d1
světové	světový	k2eAgInPc4d1
rekordy	rekord	k1gInPc4
·	·	k?
Juniorské	juniorský	k2eAgInPc4d1
světové	světový	k2eAgInPc4d1
rekordy	rekord	k1gInPc4
·	·	k?
Halové	halový	k2eAgInPc4d1
juniorské	juniorský	k2eAgInPc4d1
světové	světový	k2eAgInPc4d1
rekordy	rekord	k1gInPc4
·	·	k?
Evropské	evropský	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
·	·	k?
Halové	halový	k2eAgInPc1d1
evropské	evropský	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
·	·	k?
České	český	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
·	·	k?
Halové	halový	k2eAgInPc4d1
české	český	k2eAgInPc4d1
rekordy	rekord	k1gInPc4
Olympijské	olympijský	k2eAgFnSc2d1
hry	hra	k1gFnSc2
</s>
<s>
Olympijské	olympijský	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
Mistrovství	mistrovství	k1gNnSc2
světa	svět	k1gInSc2
a	a	k8xC
Evropy	Evropa	k1gFnSc2
</s>
<s>
Rekordy	rekord	k1gInPc1
mistrovství	mistrovství	k1gNnSc2
světa	svět	k1gInSc2
(	(	kIx(
<g/>
dráha	dráha	k1gFnSc1
<g/>
)	)	kIx)
·	·	k?
Rekordy	rekord	k1gInPc1
mistrovství	mistrovství	k1gNnPc2
světa	svět	k1gInSc2
(	(	kIx(
<g/>
hala	hala	k1gFnSc1
<g/>
)	)	kIx)
·	·	k?
Rekordy	rekord	k1gInPc4
mistrovství	mistrovství	k1gNnSc2
Evropy	Evropa	k1gFnSc2
(	(	kIx(
<g/>
dráha	dráha	k1gFnSc1
<g/>
)	)	kIx)
·	·	k?
Rekordy	rekord	k1gInPc4
mistrovství	mistrovství	k1gNnSc2
Evropy	Evropa	k1gFnSc2
(	(	kIx(
<g/>
hala	hala	k1gFnSc1
<g/>
)	)	kIx)
Československo	Československo	k1gNnSc1
</s>
<s>
Přehled	přehled	k1gInSc1
na	na	k7c6
OH	OH	kA
·	·	k?
Přehled	přehled	k1gInSc1
na	na	k7c4
MS	MS	kA
·	·	k?
Přehled	přehled	k1gInSc1
na	na	k7c4
ME	ME	kA
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Přehled	přehled	k1gInSc1
na	na	k7c6
OH	OH	kA
·	·	k?
Přehled	přehled	k1gInSc1
na	na	k7c4
MS	MS	kA
·	·	k?
Přehled	přehled	k1gInSc1
na	na	k7c4
ME	ME	kA
·	·	k?
Mistrovství	mistrovství	k1gNnSc1
ČR	ČR	kA
v	v	k7c6
atletice	atletika	k1gFnSc6
Jiné	jiný	k2eAgFnPc1d1
</s>
<s>
Seznam	seznam	k1gInSc1
československých	československý	k2eAgMnPc2d1
a	a	k8xC
českých	český	k2eAgMnPc2d1
medailistů	medailista	k1gMnPc2
v	v	k7c6
atletice	atletika	k1gFnSc6
Mítinky	mítink	k1gInPc4
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1
liga	liga	k1gFnSc1
do	do	k7c2
roku	rok	k1gInSc2
2009	#num#	k4
·	·	k?
Diamantová	diamantový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
Ankety	anketa	k1gFnSc2
</s>
<s>
Atlet	atlet	k1gMnSc1
roku	rok	k1gInSc2
(	(	kIx(
<g/>
Česko	Česko	k1gNnSc1
<g/>
)	)	kIx)
·	·	k?
Atlet	atlet	k1gMnSc1
Evropy	Evropa	k1gFnSc2
·	·	k?
Atlet	atlet	k1gMnSc1
světa	svět	k1gInSc2
·	·	k?
Síň	síň	k1gFnSc1
slávy	sláva	k1gFnSc2
IAAF	IAAF	kA
IAAF	IAAF	kA
–	–	k?
(	(	kIx(
<g/>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
<g/>
)	)	kIx)
·	·	k?
AAA	AAA	kA
–	–	k?
(	(	kIx(
<g/>
Asie	Asie	k1gFnSc2
<g/>
)	)	kIx)
·	·	k?
CAA	CAA	kA
–	–	k?
(	(	kIx(
<g/>
Afrika	Afrika	k1gFnSc1
<g/>
)	)	kIx)
·	·	k?
CONSUDATLE	CONSUDATLE	kA
–	–	k?
(	(	kIx(
<g/>
Jižní	jižní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
<g/>
)	)	kIx)
·	·	k?
EAA	EAA	kA
–	–	k?
(	(	kIx(
<g/>
Evropa	Evropa	k1gFnSc1
<g/>
)	)	kIx)
·	·	k?
NACACAA	NACACAA	kA
–	–	k?
(	(	kIx(
<g/>
Severní	severní	k2eAgFnSc1d1
a	a	k8xC
Střední	střední	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
<g/>
)	)	kIx)
·	·	k?
OAA	OAA	kA
–	–	k?
(	(	kIx(
<g/>
Oceánie	Oceánie	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kn	kn	k?
<g/>
20070124001	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
5207175-3	5207175-3	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2154	#num#	k4
1824	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2012031028	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
127522792	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2012031028	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Sport	sport	k1gInSc1
|	|	kIx~
Evropa	Evropa	k1gFnSc1
|	|	kIx~
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
