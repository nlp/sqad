<s>
Víno	víno	k1gNnSc1
</s>
<s>
Na	na	k7c4
tento	tento	k3xDgInSc4
článek	článek	k1gInSc4
je	být	k5eAaImIp3nS
přesměrováno	přesměrován	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Červené	Červené	k2eAgNnSc1d1
víno	víno	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
nápoji	nápoj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránkách	stránka	k1gFnPc6
Víno	víno	k1gNnSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
Červené	Červené	k2eAgNnSc1d1
víno	víno	k1gNnSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Víno	víno	k1gNnSc1
je	být	k5eAaImIp3nS
nápoj	nápoj	k1gInSc4
získaný	získaný	k2eAgInSc4d1
úplným	úplný	k2eAgInSc7d1
nebo	nebo	k8xC
částečným	částečný	k2eAgNnSc7d1
alkoholovým	alkoholový	k2eAgNnSc7d1
kvašením	kvašení	k1gNnSc7
čerstvých	čerstvý	k2eAgInPc2d1
rozdrcených	rozdrcený	k2eAgInPc2d1
nebo	nebo	k8xC
nerozdrcených	rozdrcený	k2eNgInPc2d1
vinných	vinný	k2eAgInPc2d1
hroznů	hrozen	k1gInPc2
nebo	nebo	k8xC
hroznového	hroznový	k2eAgInSc2d1
moštu	mošt	k1gInSc2
(	(	kIx(
<g/>
definice	definice	k1gFnSc1
Mezinárodní	mezinárodní	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
pro	pro	k7c4
révu	réva	k1gFnSc4
a	a	k8xC
víno	víno	k1gNnSc1
–	–	k?
OIV	OIV	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsah	obsah	k1gInSc1
alkoholu	alkohol	k1gInSc2
ve	v	k7c6
víně	víno	k1gNnSc6
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
vyšší	vysoký	k2eAgMnSc1d2
než	než	k8xS
8,5	8,5	k4
objemových	objemový	k2eAgNnPc2d1
procent	procento	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
přihlédnutím	přihlédnutí	k1gNnSc7
ke	k	k7c3
klimatu	klima	k1gNnSc3
<g/>
,	,	kIx,
půdě	půda	k1gFnSc3
<g/>
,	,	kIx,
odrůdě	odrůda	k1gFnSc3
a	a	k8xC
zvláštním	zvláštní	k2eAgFnPc3d1
kvalitativním	kvalitativní	k2eAgFnPc3d1
vlastnostem	vlastnost	k1gFnPc3
a	a	k8xC
tradicím	tradice	k1gFnPc3
vztahujícím	vztahující	k2eAgFnPc3d1
se	se	k3xPyFc4
k	k	k7c3
určitým	určitý	k2eAgFnPc3d1
vinicím	vinice	k1gFnPc3
může	moct	k5eAaImIp3nS
příslušná	příslušný	k2eAgFnSc1d1
místní	místní	k2eAgFnSc1d1
legislativa	legislativa	k1gFnSc1
připustit	připustit	k5eAaPmF
snížení	snížení	k1gNnSc2
této	tento	k3xDgFnSc2
hranice	hranice	k1gFnSc2
až	až	k9
na	na	k7c4
4,5	4,5	k4
objemových	objemový	k2eAgNnPc2d1
procent	procento	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
Evropské	evropský	k2eAgFnSc6d1
unii	unie	k1gFnSc6
se	se	k3xPyFc4
víno	víno	k1gNnSc1
smí	smět	k5eAaImIp3nS
vyrábět	vyrábět	k5eAaImF
pouze	pouze	k6eAd1
z	z	k7c2
hroznů	hrozen	k1gInPc2
révy	réva	k1gFnSc2
vinné	vinný	k2eAgNnSc1d1
(	(	kIx(
<g/>
Vitis	Vitis	k1gFnSc1
vinifera	vinifera	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Slovo	slovo	k1gNnSc1
víno	víno	k1gNnSc1
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
názvy	název	k1gInPc1
vína	víno	k1gNnSc2
v	v	k7c6
mnoha	mnoho	k4c6
dalších	další	k2eAgInPc6d1
jazycích	jazyk	k1gInPc6
<g/>
,	,	kIx,
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
latinského	latinský	k2eAgInSc2d1
názvu	název	k1gInSc2
vína	víno	k1gNnSc2
vinum	vinum	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vědecky	vědecky	k6eAd1
se	se	k3xPyFc4
vínem	víno	k1gNnSc7
a	a	k8xC
jeho	jeho	k3xOp3gFnSc7
výrobou	výroba	k1gFnSc7
(	(	kIx(
<g/>
vinařstvím	vinařství	k1gNnSc7
<g/>
)	)	kIx)
a	a	k8xC
pěstováním	pěstování	k1gNnSc7
vinné	vinný	k2eAgFnSc2d1
révy	réva	k1gFnSc2
(	(	kIx(
<g/>
vinohradnictvím	vinohradnictví	k1gNnSc7
<g/>
)	)	kIx)
zabývá	zabývat	k5eAaImIp3nS
enologie	enologie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Hrozny	hrozen	k1gInPc1
vinné	vinný	k2eAgFnSc2d1
révy	réva	k1gFnSc2
</s>
<s>
Třídění	třídění	k1gNnSc1
vína	víno	k1gNnSc2
</s>
<s>
Víno	víno	k1gNnSc1
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
dělit	dělit	k5eAaImF
následujícími	následující	k2eAgInPc7d1
způsoby	způsob	k1gInPc7
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Základní	základní	k2eAgNnSc1d1
dělení	dělení	k1gNnSc1
vín	víno	k1gNnPc2
</s>
<s>
Víno	víno	k1gNnSc1
</s>
<s>
V	v	k7c6
užším	úzký	k2eAgInSc6d2
slova	slovo	k1gNnSc2
smyslu	smysl	k1gInSc2
tiché	tichý	k2eAgNnSc1d1
víno	víno	k1gNnSc1
–	–	k?
tedy	tedy	k8xC
víno	víno	k1gNnSc1
neperlivé	perlivý	k2eNgNnSc1d1
<g/>
,	,	kIx,
nefortifikované	fortifikovaný	k2eNgInPc1d1
(	(	kIx(
<g/>
bez	bez	k7c2
přídavku	přídavek	k1gInSc2
alkoholu	alkohol	k1gInSc2
v	v	k7c6
jakékoli	jakýkoli	k3yIgFnSc6
formě	forma	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nearomatizované	aromatizovaný	k2eNgInPc1d1
<g/>
.	.	kIx.
</s>
<s>
Likérové	likérový	k2eAgNnSc1d1
víno	víno	k1gNnSc1
</s>
<s>
Likérové	likérový	k2eAgNnSc1d1
víno	víno	k1gNnSc1
(	(	kIx(
<g/>
též	též	k9
fortifikované	fortifikovaný	k2eAgNnSc4d1
víno	víno	k1gNnSc4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
víno	víno	k1gNnSc1
<g/>
,	,	kIx,
do	do	k7c2
kterého	který	k3yQgNnSc2,k3yIgNnSc2,k3yRgNnSc2
byl	být	k5eAaImAgMnS
přidán	přidat	k5eAaPmNgMnS
samostatně	samostatně	k6eAd1
nebo	nebo	k8xC
ve	v	k7c6
směsi	směs	k1gFnSc6
alkohol	alkohol	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
však	však	k9
dle	dle	k7c2
předpisu	předpis	k1gInSc2
EU	EU	kA
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
musí	muset	k5eAaImIp3nS
pocházet	pocházet	k5eAaImF
z	z	k7c2
vína	víno	k1gNnSc2
nebo	nebo	k8xC
hroznů	hrozen	k1gInPc2
(	(	kIx(
<g/>
např.	např.	kA
vínovice	vínovice	k1gFnSc1
nebo	nebo	k8xC
matolinová	matolinový	k2eAgFnSc1d1
pálenka	pálenka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Likérové	likérový	k2eAgNnSc1d1
víno	víno	k1gNnSc1
smí	smět	k5eAaImIp3nS
obsahovat	obsahovat	k5eAaImF
15	#num#	k4
až	až	k9
22	#num#	k4
%	%	kIx~
objemových	objemový	k2eAgInPc2d1
alkoholu	alkohol	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
skupiny	skupina	k1gFnSc2
likérových	likérový	k2eAgFnPc2d1
vín	vína	k1gFnPc2
patří	patřit	k5eAaImIp3nS
např.	např.	kA
vermuty	vermut	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Šumivé	šumivý	k2eAgNnSc1d1
víno	víno	k1gNnSc1
</s>
<s>
Získává	získávat	k5eAaImIp3nS
se	s	k7c7
prvotním	prvotní	k2eAgNnSc7d1
nebo	nebo	k8xC
druhotným	druhotný	k2eAgNnSc7d1
alkoholovým	alkoholový	k2eAgNnSc7d1
kvašením	kvašení	k1gNnSc7
z	z	k7c2
čerstvých	čerstvý	k2eAgInPc2d1
vinných	vinný	k2eAgInPc2d1
hroznů	hrozen	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
hroznového	hroznový	k2eAgInSc2d1
moštu	mošt	k1gInSc2
nebo	nebo	k8xC
z	z	k7c2
vína	víno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
otevření	otevření	k1gNnSc6
nádoby	nádoba	k1gFnSc2
z	z	k7c2
něj	on	k3xPp3gMnSc2
musí	muset	k5eAaImIp3nS
unikat	unikat	k5eAaImF
oxid	oxid	k1gInSc4
uhličitý	uhličitý	k2eAgInSc4d1
pocházející	pocházející	k2eAgFnSc4d1
výhradně	výhradně	k6eAd1
z	z	k7c2
kvašení	kvašení	k1gNnSc2
a	a	k8xC
uzavřené	uzavřený	k2eAgFnSc6d1
nádobě	nádoba	k1gFnSc6
při	při	k7c6
teplotě	teplota	k1gFnSc6
20	#num#	k4
°	°	k?
<g/>
C	C	kA
vykazuje	vykazovat	k5eAaImIp3nS
přetlak	přetlak	k1gInSc1
nejméně	málo	k6eAd3
3	#num#	k4
bary	bar	k1gInPc4
<g/>
,	,	kIx,
způsobený	způsobený	k2eAgInSc1d1
rozpuštěným	rozpuštěný	k2eAgInSc7d1
oxidem	oxid	k1gInSc7
uhličitým	uhličitý	k2eAgInSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jakostní	jakostní	k2eAgFnSc1d1
šumivé	šumivý	k2eAgNnSc4d1
víno	víno	k1gNnSc4
musí	muset	k5eAaImIp3nS
vykazovat	vykazovat	k5eAaImF
přetlak	přetlak	k1gInSc4
alespoň	alespoň	k9
3,5	3,5	k4
baru	bar	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šumivé	šumivý	k2eAgNnSc1d1
víno	víno	k1gNnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
dosyceno	dosytit	k5eAaPmNgNnS
oxidem	oxid	k1gInSc7
uhličitým	uhličitý	k2eAgInSc7d1
<g/>
,	,	kIx,
tento	tento	k3xDgInSc1
údaj	údaj	k1gInSc1
však	však	k9
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
uveden	uvést	k5eAaPmNgInS
na	na	k7c6
etiketě	etiketa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
určitých	určitý	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
šumivé	šumivý	k2eAgNnSc1d1
víno	víno	k1gNnSc1
označeno	označit	k5eAaPmNgNnS
jako	jako	k8xS,k8xC
sekt	sekt	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Perlivé	perlivý	k2eAgNnSc1d1
víno	víno	k1gNnSc1
</s>
<s>
Perlivé	perlivý	k2eAgNnSc1d1
víno	víno	k1gNnSc1
vykazuje	vykazovat	k5eAaImIp3nS
v	v	k7c6
uzavřené	uzavřený	k2eAgFnSc6d1
nádobě	nádoba	k1gFnSc6
přetlak	přetlak	k1gInSc1
oxidu	oxid	k1gInSc2
uhličitého	uhličitý	k2eAgInSc2d1
1	#num#	k4
až	až	k9
2,5	2,5	k4
baru	bar	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Itálii	Itálie	k1gFnSc6
a	a	k8xC
často	často	k6eAd1
i	i	k9
jinde	jinde	k6eAd1
(	(	kIx(
<g/>
i	i	k9
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
)	)	kIx)
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
výrazem	výraz	k1gInSc7
Frizzante	Frizzant	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k6eAd1
perlivé	perlivý	k2eAgNnSc1d1
víno	víno	k1gNnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
dosyceno	dosytit	k5eAaPmNgNnS
oxidem	oxid	k1gInSc7
uhličitým	uhličitý	k2eAgInSc7d1
<g/>
.	.	kIx.
</s>
<s>
Částečně	částečně	k6eAd1
zkvašený	zkvašený	k2eAgInSc1d1
hroznový	hroznový	k2eAgInSc1d1
mošt	mošt	k1gInSc1
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS
nejméně	málo	k6eAd3
1	#num#	k4
%	%	kIx~
obj	obj	k?
<g/>
.	.	kIx.
a	a	k8xC
méně	málo	k6eAd2
než	než	k8xS
3	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
celkového	celkový	k2eAgInSc2d1
obsahu	obsah	k1gInSc2
alkoholu	alkohol	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Česku	Česko	k1gNnSc6
se	se	k3xPyFc4
částečně	částečně	k6eAd1
zkvašený	zkvašený	k2eAgInSc1d1
hroznový	hroznový	k2eAgInSc1d1
mošt	mošt	k1gInSc1
získaný	získaný	k2eAgInSc1d1
z	z	k7c2
hroznů	hrozen	k1gInPc2
vypěstovaných	vypěstovaný	k2eAgInPc2d1
výhradně	výhradně	k6eAd1
v	v	k7c6
Moravské	moravský	k2eAgFnSc6d1
nebo	nebo	k8xC
České	český	k2eAgFnSc6d1
vinařské	vinařský	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
označuje	označovat	k5eAaImIp3nS
tradičním	tradiční	k2eAgInSc7d1
výrazem	výraz	k1gInSc7
burčák	burčák	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Dle	dle	k7c2
barvy	barva	k1gFnSc2
</s>
<s>
Červené	Červené	k2eAgNnSc1d1
víno	víno	k1gNnSc1
ve	v	k7c6
sklenici	sklenice	k1gFnSc6
</s>
<s>
Tento	tento	k3xDgInSc1
odstavec	odstavec	k1gInSc1
pojednává	pojednávat	k5eAaImIp3nS
pouze	pouze	k6eAd1
o	o	k7c6
tichých	tichý	k2eAgNnPc6d1
vínech	víno	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s>
Tichá	Tichá	k1gFnSc1
vína	víno	k1gNnSc2
se	se	k3xPyFc4
na	na	k7c6
základě	základ	k1gInSc6
zbarvení	zbarvení	k1gNnPc2
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
bílá	bílý	k2eAgNnPc4d1
a	a	k8xC
červená	červený	k2eAgNnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
nim	on	k3xPp3gInPc3
přistupují	přistupovat	k5eAaImIp3nP
odvozené	odvozený	k2eAgFnPc1d1
barvy	barva	k1gFnPc1
–	–	k?
víno	víno	k1gNnSc1
růžové	růžový	k2eAgNnSc1d1
neboli	neboli	k8xC
rosé	rosý	k2eAgNnSc1d1
a	a	k8xC
víno	víno	k1gNnSc1
oranžové	oranžový	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Bílé	bílý	k2eAgNnSc1d1
víno	víno	k1gNnSc1
</s>
<s>
Základní	základní	k2eAgFnSc7d1
surovinou	surovina	k1gFnSc7
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
bílého	bílý	k2eAgNnSc2d1
vína	víno	k1gNnSc2
jsou	být	k5eAaImIp3nP
hrozny	hrozen	k1gInPc1
tzv.	tzv.	kA
bílých	bílý	k2eAgFnPc2d1
moštových	moštový	k2eAgFnPc2d1
odrůd	odrůda	k1gFnPc2
révy	réva	k1gFnSc2
vinné	vinný	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
jeho	jeho	k3xOp3gFnSc6
výrobě	výroba	k1gFnSc6
se	se	k3xPyFc4
rmut	rmut	k1gInSc1
(	(	kIx(
<g/>
rozdrcené	rozdrcený	k2eAgFnSc2d1
bobule	bobule	k1gFnSc2
<g/>
)	)	kIx)
ihned	ihned	k6eAd1
lisuje	lisovat	k5eAaImIp3nS
a	a	k8xC
získává	získávat	k5eAaImIp3nS
se	se	k3xPyFc4
čistý	čistý	k2eAgInSc1d1
mošt	mošt	k1gInSc1
ke	k	k7c3
kvašení	kvašení	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pevné	pevný	k2eAgInPc1d1
zbytky	zbytek	k1gInPc1
po	po	k7c6
lisování	lisování	k1gNnSc6
se	se	k3xPyFc4
nazývají	nazývat	k5eAaImIp3nP
matoliny	matoliny	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odrůdy	odrůda	k1gFnSc2
označované	označovaný	k2eAgFnPc1d1
jako	jako	k8xS,k8xC
červené	červený	k2eAgFnPc1d1
(	(	kIx(
<g/>
mají	mít	k5eAaImIp3nP
načervenalé	načervenalý	k2eAgFnSc2d1
bobule	bobule	k1gFnSc2
–	–	k?
např.	např.	kA
Tramín	tramín	k1gInSc1
červený	červený	k2eAgInSc1d1
<g/>
)	)	kIx)
rovněž	rovněž	k9
slouží	sloužit	k5eAaImIp3nS
k	k	k7c3
výrobě	výroba	k1gFnSc3
bílých	bílý	k2eAgFnPc2d1
vín	vína	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bílé	bílý	k2eAgNnSc1d1
víno	víno	k1gNnSc1
se	se	k3xPyFc4
však	však	k9
dá	dát	k5eAaPmIp3nS
vyrobit	vyrobit	k5eAaPmF
i	i	k9
z	z	k7c2
modrých	modrý	k2eAgFnPc2d1
odrůd	odrůda	k1gFnPc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
se	se	k3xPyFc4
zkvasí	zkvasit	k5eAaPmIp3nS
jen	jen	k9
jejich	jejich	k3xOp3gInSc1
mošt	mošt	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
u	u	k7c2
většiny	většina	k1gFnSc2
odrůd	odrůda	k1gFnPc2
neobsahuje	obsahovat	k5eNaImIp3nS
téměř	téměř	k6eAd1
žádná	žádný	k3yNgNnPc4
barviva	barvivo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bílé	bílý	k2eAgNnSc1d1
víno	víno	k1gNnSc1
vyrobené	vyrobený	k2eAgNnSc1d1
z	z	k7c2
modrých	modrý	k2eAgInPc2d1
hroznů	hrozen	k1gInPc2
bez	bez	k7c2
nakvášení	nakvášení	k1gNnSc2
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
nazývat	nazývat	k5eAaImF
klaret	klaret	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Růžové	růžový	k2eAgNnSc1d1
víno	víno	k1gNnSc1
(	(	kIx(
<g/>
rosé	rosé	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS
se	se	k3xPyFc4
z	z	k7c2
hroznů	hrozen	k1gInPc2
modrých	modrý	k2eAgFnPc2d1
moštových	moštový	k2eAgFnPc2d1
odrůd	odrůda	k1gFnPc2
jen	jen	k9
s	s	k7c7
krátkým	krátký	k2eAgNnSc7d1
nakvášením	nakvášení	k1gNnSc7
nebo	nebo	k8xC
bez	bez	k7c2
nakvášení	nakvášení	k1gNnSc2
<g/>
,	,	kIx,
takže	takže	k8xS
výsledné	výsledný	k2eAgNnSc1d1
zbarvení	zbarvení	k1gNnSc1
je	být	k5eAaImIp3nS
světle	světle	k6eAd1
červené	červený	k2eAgNnSc1d1
(	(	kIx(
<g/>
růžové	růžový	k2eAgNnSc4d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Víno	víno	k1gNnSc1
s	s	k7c7
chráněným	chráněný	k2eAgNnSc7d1
zeměpisným	zeměpisný	k2eAgNnSc7d1
označením	označení	k1gNnSc7
a	a	k8xC
víno	víno	k1gNnSc1
s	s	k7c7
chráněným	chráněný	k2eAgNnSc7d1
označením	označení	k1gNnSc7
původu	původ	k1gInSc2
se	se	k3xPyFc4
nesmí	smět	k5eNaImIp3nS
vyrábět	vyrábět	k5eAaImF
smísením	smísení	k1gNnSc7
červeného	červené	k1gNnSc2
a	a	k8xC
bílého	bílý	k2eAgNnSc2d1
vína	víno	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Červené	Červené	k2eAgNnSc1d1
víno	víno	k1gNnSc1
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS
se	se	k3xPyFc4
pouze	pouze	k6eAd1
z	z	k7c2
modrých	modrý	k2eAgInPc2d1
hroznů	hrozen	k1gInPc2
(	(	kIx(
<g/>
protože	protože	k8xS
červené	červený	k2eAgNnSc1d1
barvivo	barvivo	k1gNnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
pouze	pouze	k6eAd1
v	v	k7c6
těchto	tento	k3xDgFnPc6
odrůdách	odrůda	k1gFnPc6
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
ve	v	k7c6
slupkách	slupka	k1gFnPc6
bobulí	bobule	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
jeho	jeho	k3xOp3gFnSc6
výrobě	výroba	k1gFnSc6
se	se	k3xPyFc4
mošt	mošt	k1gInSc1
nechá	nechat	k5eAaPmIp3nS
několik	několik	k4yIc4
dní	den	k1gInPc2
kvasit	kvasit	k5eAaImF
se	s	k7c7
rmutem	rmut	k1gInSc7
(	(	kIx(
<g/>
nakvášení	nakvášení	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slupky	slupka	k1gFnSc2
tak	tak	k6eAd1
zůstávají	zůstávat	k5eAaImIp3nP
v	v	k7c6
kontaktu	kontakt	k1gInSc6
s	s	k7c7
kvasící	kvasící	k2eAgFnSc7d1
šťávou	šťáva	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvašení	kvašení	k1gNnSc1
probíhá	probíhat	k5eAaImIp3nS
delší	dlouhý	k2eAgFnSc4d2
dobu	doba	k1gFnSc4
a	a	k8xC
zpravidla	zpravidla	k6eAd1
za	za	k7c4
vyšší	vysoký	k2eAgFnPc4d2
teploty	teplota	k1gFnPc4
než	než	k8xS
u	u	k7c2
bílého	bílý	k2eAgNnSc2d1
vína	víno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červeném	červený	k2eAgNnSc6d1
vínu	víno	k1gNnSc6
jsou	být	k5eAaImIp3nP
tak	tak	k9
ve	v	k7c6
vyšší	vysoký	k2eAgFnSc6d2
míře	míra	k1gFnSc6
třísloviny	tříslovina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Oranžové	oranžový	k2eAgNnSc1d1
víno	víno	k1gNnSc1
</s>
<s>
Oranžové	oranžový	k2eAgNnSc1d1
víno	víno	k1gNnSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
podstatě	podstata	k1gFnSc6
bílé	bílý	k2eAgNnSc1d1
víno	víno	k1gNnSc1
zrající	zrající	k2eAgNnSc1d1
na	na	k7c6
slupkách	slupka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
se	se	k3xPyFc4
za	za	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
tato	tento	k3xDgFnSc1
pradávná	pradávný	k2eAgFnSc1d1
receptura	receptura	k1gFnSc1
je	být	k5eAaImIp3nS
vůbec	vůbec	k9
první	první	k4xOgInSc1
způsob	způsob	k1gInSc1
přípravy	příprava	k1gFnSc2
vína	víno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
vína	víno	k1gNnPc4
vyráběná	vyráběný	k2eAgFnSc1d1
starobylými	starobylý	k2eAgFnPc7d1
gruzínskými	gruzínský	k2eAgFnPc7d1
technologiemi	technologie	k1gFnPc7
(	(	kIx(
<g/>
tzv.	tzv.	kA
kachetinské	kachetinský	k2eAgFnPc4d1
technologie	technologie	k1gFnPc4
<g/>
)	)	kIx)
z	z	k7c2
bílých	bílý	k2eAgFnPc2d1
odrůd	odrůda	k1gFnPc2
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
víno	víno	k1gNnSc1
zůstává	zůstávat	k5eAaImIp3nS
po	po	k7c4
celou	celý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
na	na	k7c6
rmutu	rmut	k1gInSc6
nebo	nebo	k8xC
technologiemi	technologie	k1gFnPc7
experimentálními	experimentální	k2eAgFnPc7d1
<g/>
,	,	kIx,
podobnými	podobný	k2eAgInPc7d1
kachetinským	kachetinský	k2eAgMnPc3d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dle	dle	k7c2
obsahu	obsah	k1gInSc2
zbytkového	zbytkový	k2eAgInSc2d1
cukru	cukr	k1gInSc2
</s>
<s>
Tiché	Tiché	k2eAgNnSc1d1
víno	víno	k1gNnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Tiché	Tiché	k2eAgNnSc1d1
víno	víno	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
suché	suchý	k2eAgNnSc1d1
<g/>
:	:	kIx,
nejvýše	nejvýše	k6eAd1,k6eAd3
4	#num#	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
l	l	kA
<g/>
,	,	kIx,
nebo	nebo	k8xC
9	#num#	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
l	l	kA
<g/>
,	,	kIx,
pokud	pokud	k8xS
celková	celkový	k2eAgFnSc1d1
kyselost	kyselost	k1gFnSc1
vyjádřená	vyjádřený	k2eAgFnSc1d1
v	v	k7c6
gramech	gram	k1gInPc6
kyseliny	kyselina	k1gFnSc2
vinné	vinný	k2eAgFnSc2d1
na	na	k7c4
litr	litr	k1gInSc4
je	být	k5eAaImIp3nS
nejméně	málo	k6eAd3
o	o	k7c4
2	#num#	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
l	l	kA
nižší	nízký	k2eAgInPc1d2
</s>
<s>
polosuché	polosuchý	k2eAgInPc4d1
<g/>
:	:	kIx,
do	do	k7c2
12	#num#	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
l	l	kA
<g/>
,	,	kIx,
nebo	nebo	k8xC
do	do	k7c2
18	#num#	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
l	l	kA
<g/>
,	,	kIx,
pokud	pokud	k8xS
celková	celkový	k2eAgFnSc1d1
kyselost	kyselost	k1gFnSc1
vyjádřená	vyjádřený	k2eAgFnSc1d1
v	v	k7c6
gramech	gram	k1gInPc6
kyseliny	kyselina	k1gFnSc2
vinné	vinný	k2eAgFnSc2d1
na	na	k7c4
litr	litr	k1gInSc4
je	být	k5eAaImIp3nS
nejméně	málo	k6eAd3
o	o	k7c4
10	#num#	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
l	l	kA
nižší	nízký	k2eAgInPc1d2
</s>
<s>
polosladké	polosladká	k1gFnPc1
<g/>
:	:	kIx,
do	do	k7c2
45	#num#	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
l	l	kA
</s>
<s>
sladké	sladký	k2eAgFnPc1d1
<g/>
:	:	kIx,
od	od	k7c2
45	#num#	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
l	l	kA
výše	výše	k1gFnSc1
</s>
<s>
Šumivé	šumivý	k2eAgNnSc1d1
víno	víno	k1gNnSc1
</s>
<s>
přírodně	přírodně	k6eAd1
tvrdé	tvrdý	k2eAgNnSc1d1
(	(	kIx(
<g/>
brut	brut	k5eAaPmF
nature	natur	k1gMnSc5
<g/>
)	)	kIx)
<g/>
:	:	kIx,
do	do	k7c2
3	#num#	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
l.	l.	k?
Lze	lze	k6eAd1
použít	použít	k5eAaPmF
jen	jen	k9
pro	pro	k7c4
vína	víno	k1gNnPc4
<g/>
,	,	kIx,
kterým	který	k3yIgMnSc7,k3yQgMnSc7,k3yRgMnSc7
nebyl	být	k5eNaImAgMnS
po	po	k7c6
druhotném	druhotný	k2eAgNnSc6d1
kvašení	kvašení	k1gNnSc6
dodán	dodán	k2eAgInSc4d1
žádný	žádný	k3yNgInSc4
cukr	cukr	k1gInSc4
</s>
<s>
zvláště	zvláště	k6eAd1
tvrdé	tvrdý	k2eAgNnSc1d1
(	(	kIx(
<g/>
extra	extra	k2eAgInSc4d1
brut	brut	k1gInSc4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
0	#num#	k4
–	–	k?
6	#num#	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
l	l	kA
</s>
<s>
tvrdé	tvrdé	k1gNnSc1
(	(	kIx(
<g/>
brut	brut	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
do	do	k7c2
12	#num#	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
l	l	kA
</s>
<s>
zvláště	zvláště	k6eAd1
suché	suchý	k2eAgNnSc1d1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
extra	extra	k6eAd1
dry	dry	k?
<g/>
)	)	kIx)
<g/>
:	:	kIx,
12	#num#	k4
–	–	k?
17	#num#	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
l	l	kA
</s>
<s>
suché	suchý	k2eAgNnSc1d1
(	(	kIx(
<g/>
sec	sec	k0
<g/>
)	)	kIx)
<g/>
:	:	kIx,
17	#num#	k4
<g/>
–	–	k?
<g/>
32	#num#	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
l	l	kA
</s>
<s>
polosuché	polosuchý	k2eAgFnPc1d1
(	(	kIx(
<g/>
demi-sec	demi-sec	k1gInSc1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
32	#num#	k4
–	–	k?
50	#num#	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
l	l	kA
</s>
<s>
sladké	sladký	k2eAgNnSc4d1
(	(	kIx(
<g/>
doux	doux	k1gInSc4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
více	hodně	k6eAd2
než	než	k8xS
50	#num#	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
l	l	kA
</s>
<s>
Jakostní	jakostní	k2eAgFnPc1d1
třídy	třída	k1gFnPc1
dle	dle	k7c2
českého	český	k2eAgInSc2d1
zákona	zákon	k1gInSc2
o	o	k7c6
vinohradnictví	vinohradnictví	k1gNnSc6
a	a	k8xC
vinařství	vinařství	k1gNnSc6
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
tzv.	tzv.	kA
germánský	germánský	k2eAgInSc4d1
systém	systém	k1gInSc4
zatřiďování	zatřiďování	k1gNnSc2
vín	vína	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
je	být	k5eAaImIp3nS
založen	založit	k5eAaPmNgInS
na	na	k7c4
(	(	kIx(
<g/>
ne	ne	k9
vždy	vždy	k6eAd1
platném	platný	k2eAgInSc6d1
<g/>
)	)	kIx)
předpokladu	předpoklad	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
čím	čí	k3xOyQgNnSc7,k3xOyRgNnSc7
je	být	k5eAaImIp3nS
sladší	sladký	k2eAgInSc1d2
mošt	mošt	k1gInSc1
(	(	kIx(
<g/>
má	mít	k5eAaImIp3nS
vyšší	vysoký	k2eAgFnSc1d2
cukernatost	cukernatost	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
z	z	k7c2
nějž	jenž	k3xRgInSc2
se	se	k3xPyFc4
bude	být	k5eAaImBp3nS
víno	víno	k1gNnSc1
připravovat	připravovat	k5eAaImF
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
vyšší	vysoký	k2eAgFnPc4d2
kvality	kvalita	k1gFnPc4
výsledného	výsledný	k2eAgNnSc2d1
vína	víno	k1gNnSc2
se	se	k3xPyFc4
dosáhne	dosáhnout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cukernatost	cukernatost	k1gFnSc1
se	se	k3xPyFc4
měří	měřit	k5eAaImIp3nS
v	v	k7c6
tzv.	tzv.	kA
stupních	stupeň	k1gInPc6
normovaného	normovaný	k2eAgInSc2d1
moštoměru	moštoměr	k1gInSc2
(	(	kIx(
<g/>
°	°	k?
<g/>
NM	NM	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgMnSc1
°	°	k?
<g/>
NM	NM	kA
odpovídá	odpovídat	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
koncentraci	koncentrace	k1gFnSc4
10	#num#	k4
g	g	kA
cukru	cukr	k1gInSc2
v	v	k7c6
1	#num#	k4
litru	litr	k1gInSc6
moštu	mošt	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Stolní	stolní	k2eAgNnSc1d1
víno	víno	k1gNnSc1
</s>
<s>
Víno	víno	k1gNnSc1
bez	bez	k7c2
dalšího	další	k2eAgNnSc2d1
označení	označení	k1gNnSc2
(	(	kIx(
<g/>
do	do	k7c2
roku	rok	k1gInSc2
2011	#num#	k4
stolní	stolní	k2eAgInSc4d1
víno	víno	k1gNnSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
vyrábí	vyrábět	k5eAaImIp3nS
z	z	k7c2
hroznů	hrozen	k1gInPc2
sklizených	sklizený	k2eAgInPc2d1
na	na	k7c4
území	území	k1gNnSc4
kteréhokoli	kterýkoli	k3yIgInSc2
státu	stát	k1gInSc2
EU	EU	kA
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
dosáhly	dosáhnout	k5eAaPmAgFnP
cukernatosti	cukernatost	k1gFnPc1
nejméně	málo	k6eAd3
11	#num#	k4
stupňů	stupeň	k1gInPc2
normalizovaného	normalizovaný	k2eAgInSc2d1
moštoměru	moštoměr	k1gInSc2
(	(	kIx(
<g/>
°	°	k?
<g/>
NM	NM	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
rmutu	rmut	k1gInSc2
<g/>
,	,	kIx,
moštu	mošt	k1gInSc2
či	či	k8xC
vína	víno	k1gNnSc2
získaných	získaný	k2eAgInPc2d1
z	z	k7c2
hroznů	hrozen	k1gInPc2
odrůd	odrůda	k1gFnPc2
moštových	moštový	k2eAgInPc2d1
a	a	k8xC
odrůd	odrůda	k1gFnPc2
registrovaných	registrovaný	k2eAgFnPc2d1
ve	v	k7c6
kterémkoli	kterýkoli	k3yIgInSc6
státě	stát	k1gInSc6
EU	EU	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
zpracování	zpracování	k1gNnSc3
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
použity	použít	k5eAaPmNgInP
také	také	k9
hrozny	hrozen	k1gInPc4
neregistrovaných	registrovaný	k2eNgFnPc2d1
odrůd	odrůda	k1gFnPc2
vysazených	vysazený	k2eAgFnPc2d1
před	před	k7c7
1	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Víno	víno	k1gNnSc1
nesmí	smět	k5eNaImIp3nS
být	být	k5eAaImF
označováno	označovat	k5eAaImNgNnS
názvem	název	k1gInSc7
odrůdy	odrůda	k1gFnSc2
(	(	kIx(
<g/>
pokud	pokud	k8xS
není	být	k5eNaImIp3nS
zastoupení	zastoupení	k1gNnSc4
jedné	jeden	k4xCgFnSc2
odrůdy	odrůda	k1gFnSc2
alespoň	alespoň	k9
85	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
ani	ani	k8xC
názvem	název	k1gInSc7
vinařské	vinařský	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
(	(	kIx(
<g/>
pokud	pokud	k8xS
alespoň	alespoň	k9
85	#num#	k4
%	%	kIx~
nepochází	pocházet	k5eNaImIp3nS
z	z	k7c2
jedné	jeden	k4xCgFnSc2
oblasti	oblast	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Zemské	zemský	k2eAgNnSc4d1
víno	víno	k1gNnSc4
</s>
<s>
Zemské	zemský	k2eAgNnSc1d1
víno	víno	k1gNnSc1
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
vyrobeno	vyrobit	k5eAaPmNgNnS
z	z	k7c2
hroznů	hrozen	k1gInPc2
révy	réva	k1gFnSc2
vinné	vinný	k1gMnPc4
sklizených	sklizený	k2eAgNnPc2d1
na	na	k7c4
území	území	k1gNnSc4
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
jsou	být	k5eAaImIp3nP
vhodné	vhodný	k2eAgInPc1d1
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
jakostního	jakostní	k2eAgNnSc2d1
vína	víno	k1gNnSc2
stanovené	stanovený	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
nebo	nebo	k8xC
z	z	k7c2
odrůd	odrůda	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgFnP
v	v	k7c6
seznamu	seznam	k1gInSc6
odrůd	odrůda	k1gFnPc2
stanovených	stanovený	k2eAgFnPc2d1
prováděcím	prováděcí	k2eAgInSc7d1
právním	právní	k2eAgInSc7d1
předpisem	předpis	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cukernatost	cukernatost	k1gFnSc1
hroznů	hrozen	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
je	být	k5eAaImIp3nS
víno	víno	k1gNnSc1
vyrobeno	vyrobit	k5eAaPmNgNnS
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
nejméně	málo	k6eAd3
14	#num#	k4
°	°	k?
<g/>
NM	NM	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Označuje	označovat	k5eAaImIp3nS
se	se	k3xPyFc4
jako	jako	k9
Moravské	moravský	k2eAgNnSc1d1
zemské	zemský	k2eAgNnSc1d1
víno	víno	k1gNnSc1
nebo	nebo	k8xC
České	český	k2eAgNnSc1d1
zemské	zemský	k2eAgNnSc1d1
víno	víno	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Jakostní	jakostní	k2eAgNnSc1d1
víno	víno	k1gNnSc1
</s>
<s>
Jakostní	jakostní	k2eAgNnSc1d1
víno	víno	k1gNnSc1
se	se	k3xPyFc4
vyrábí	vyrábět	k5eAaImIp3nS
z	z	k7c2
vinných	vinný	k2eAgInPc2d1
hroznů	hrozen	k1gInPc2
<g/>
,	,	kIx,
sklizených	sklizený	k2eAgInPc2d1
na	na	k7c6
vinici	vinice	k1gFnSc6
vhodné	vhodný	k2eAgFnPc1d1
pro	pro	k7c4
jakostní	jakostní	k2eAgNnSc4d1
víno	víno	k1gNnSc4
stanovené	stanovený	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
a	a	k8xC
v	v	k7c6
téže	tenže	k3xDgFnSc6,k3xTgFnSc6
oblasti	oblast	k1gFnSc6
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
víno	víno	k1gNnSc4
i	i	k9
vyrobeno	vyrobit	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cukernatost	cukernatost	k1gFnSc1
hroznů	hrozen	k1gInPc2
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
nejméně	málo	k6eAd3
15	#num#	k4
°	°	k?
<g/>
NM	NM	kA
a	a	k8xC
výnos	výnos	k1gInSc1
vinice	vinice	k1gFnSc2
nesmí	smět	k5eNaImIp3nS
překročit	překročit	k5eAaPmF
14	#num#	k4
t	t	k?
<g/>
/	/	kIx~
<g/>
ha	ha	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrozny	hrozen	k1gInPc1
či	či	k8xC
mošt	mošt	k1gInSc1
pod	pod	k7c7
19	#num#	k4
<g/>
°	°	k?
<g/>
NM	NM	kA
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
dosladit	dosladit	k5eAaPmF
řepným	řepný	k2eAgInSc7d1
cukrem	cukr	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jakostní	jakostní	k2eAgInSc4d1
víno	víno	k1gNnSc1
bývá	bývat	k5eAaImIp3nS
kromě	kromě	k7c2
základních	základní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
označeno	označit	k5eAaPmNgNnS
názvem	název	k1gInSc7
vinařské	vinařský	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yRgMnPc4,k3yQgMnPc4,k3yIgMnPc4
byly	být	k5eAaImAgInP
sklizeny	sklizen	k2eAgInPc1d1
hrozny	hrozen	k1gInPc1
pro	pro	k7c4
jeho	jeho	k3xOp3gFnSc4
výrobu	výroba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
vyráběno	vyrábět	k5eAaImNgNnS
ve	v	k7c6
dvou	dva	k4xCgInPc6
druzích	druh	k1gInPc6
<g/>
:	:	kIx,
</s>
<s>
Odrůdové	odrůdový	k2eAgNnSc1d1
jakostní	jakostní	k2eAgNnSc1d1
víno	víno	k1gNnSc1
s	s	k7c7
určeným	určený	k2eAgInSc7d1
názvem	název	k1gInSc7
odrůdy	odrůda	k1gFnSc2
vína	víno	k1gNnSc2
na	na	k7c6
obalu	obal	k1gInSc6
<g/>
,	,	kIx,
ze	z	k7c2
které	který	k3yQgFnSc2,k3yRgFnSc2,k3yIgFnSc2
bylo	být	k5eAaImAgNnS
vyrobeno	vyrobit	k5eAaPmNgNnS
a	a	k8xC
musí	muset	k5eAaImIp3nS
obsahovat	obsahovat	k5eAaImF
nejméně	málo	k6eAd3
85	#num#	k4
%	%	kIx~
vína	víno	k1gNnSc2
vyrobeného	vyrobený	k2eAgNnSc2d1
z	z	k7c2
této	tento	k3xDgFnSc2
odrůdy	odrůda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Známkové	známkový	k2eAgNnSc1d1
jakostní	jakostní	k2eAgNnSc1d1
víno	víno	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
smí	smět	k5eAaImIp3nS
být	být	k5eAaImF
vyráběno	vyráběn	k2eAgNnSc4d1
smísením	smísení	k1gNnSc7
odrůdových	odrůdový	k2eAgFnPc2d1
jakostních	jakostní	k2eAgFnPc2d1
vín	vína	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Jakostní	jakostní	k2eAgNnSc1d1
víno	víno	k1gNnSc1
s	s	k7c7
přívlastkem	přívlastek	k1gInSc7
</s>
<s>
Jakostní	jakostní	k2eAgNnSc1d1
víno	víno	k1gNnSc1
s	s	k7c7
přívlastkem	přívlastek	k1gInSc7
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
vyrobeno	vyrobit	k5eAaPmNgNnS
z	z	k7c2
hroznů	hrozen	k1gInPc2
sklizených	sklizený	k2eAgInPc2d1
v	v	k7c6
jedné	jeden	k4xCgFnSc6
vinařské	vinařský	k2eAgFnSc6d1
podoblasti	podoblast	k1gFnSc6
a	a	k8xC
v	v	k7c6
téže	týž	k3xTgFnSc6,k3xDgFnSc6
oblasti	oblast	k1gFnSc6
i	i	k9
vyrobeno	vyrobit	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výnos	výnos	k1gInSc1
vinice	vinice	k1gFnSc2
nesmí	smět	k5eNaImIp3nS
překročit	překročit	k5eAaPmF
14	#num#	k4
t	t	k?
<g/>
/	/	kIx~
<g/>
ha	ha	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
kvalitu	kvalita	k1gFnSc4
vinice	vinice	k1gFnSc2
i	i	k9
kvalitu	kvalita	k1gFnSc4
výsledného	výsledný	k2eAgNnSc2d1
vína	víno	k1gNnSc2
existují	existovat	k5eAaImIp3nP
ještě	ještě	k9
další	další	k2eAgInPc4d1
požadavky	požadavek	k1gInPc4
<g/>
,	,	kIx,
na	na	k7c6
jejichž	jejichž	k3xOyRp3gNnSc6
splnění	splnění	k1gNnSc6
dohlíží	dohlížet	k5eAaImIp3nS
Státní	státní	k2eAgFnSc1d1
zemědělská	zemědělský	k2eAgFnSc1d1
a	a	k8xC
potravinářská	potravinářský	k2eAgFnSc1d1
inspekce	inspekce	k1gFnSc1
(	(	kIx(
<g/>
SZPI	SZPI	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
jediná	jediný	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
oprávněna	oprávnit	k5eAaPmNgFnS
víno	víno	k1gNnSc1
zatřídit	zatřídit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Kabinet	kabinet	k1gInSc1
<g/>
:	:	kIx,
vyrábí	vyrábět	k5eAaImIp3nS
se	se	k3xPyFc4
z	z	k7c2
hroznů	hrozen	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
dosáhly	dosáhnout	k5eAaPmAgFnP
cukernatosti	cukernatost	k1gFnPc1
nejméně	málo	k6eAd3
19	#num#	k4
°	°	k?
<g/>
NM	NM	kA
<g/>
.	.	kIx.
</s>
<s>
Pozdní	pozdní	k2eAgInSc1d1
sběr	sběr	k1gInSc1
<g/>
:	:	kIx,
vyrábí	vyrábět	k5eAaImIp3nS
se	se	k3xPyFc4
z	z	k7c2
hroznů	hrozen	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
dosáhly	dosáhnout	k5eAaPmAgInP
nejméně	málo	k6eAd3
21	#num#	k4
°	°	k?
<g/>
NM	NM	kA
<g/>
.	.	kIx.
</s>
<s>
Výběr	výběr	k1gInSc1
z	z	k7c2
hroznů	hrozen	k1gInPc2
<g/>
:	:	kIx,
vyrábí	vyrábět	k5eAaImIp3nS
se	se	k3xPyFc4
z	z	k7c2
hroznů	hrozen	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
dosáhly	dosáhnout	k5eAaPmAgInP
nejméně	málo	k6eAd3
24	#num#	k4
°	°	k?
<g/>
NM	NM	kA
<g/>
.	.	kIx.
</s>
<s>
Výběr	výběr	k1gInSc1
z	z	k7c2
bobulí	bobule	k1gFnPc2
<g/>
:	:	kIx,
vyrábí	vyrábět	k5eAaImIp3nS
se	se	k3xPyFc4
z	z	k7c2
hroznů	hrozen	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
dosáhly	dosáhnout	k5eAaPmAgInP
nejméně	málo	k6eAd3
27	#num#	k4
°	°	k?
<g/>
NM	NM	kA
<g/>
.	.	kIx.
</s>
<s>
Výběr	výběr	k1gInSc1
z	z	k7c2
cibéb	cibéba	k1gFnPc2
<g/>
:	:	kIx,
vyrábí	vyrábět	k5eAaImIp3nS
se	se	k3xPyFc4
z	z	k7c2
hroznů	hrozen	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
dosáhly	dosáhnout	k5eAaPmAgInP
nejméně	málo	k6eAd3
29-32	29-32	k4
°	°	k?
<g/>
NM	NM	kA
<g/>
.	.	kIx.
</s>
<s>
Ledové	ledový	k2eAgNnSc1d1
víno	víno	k1gNnSc1
<g/>
:	:	kIx,
vyrábí	vyrábět	k5eAaImIp3nS
se	se	k3xPyFc4
z	z	k7c2
hroznů	hrozen	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
byly	být	k5eAaImAgInP
sklizeny	sklidit	k5eAaPmNgInP
při	při	k7c6
teplotách	teplota	k1gFnPc6
minus	minus	k1gNnSc1
7	#num#	k4
°	°	k?
<g/>
C	C	kA
a	a	k8xC
nižších	nízký	k2eAgMnPc2d2
<g/>
,	,	kIx,
v	v	k7c6
průběhu	průběh	k1gInSc6
sklizně	sklizeň	k1gFnSc2
a	a	k8xC
zpracování	zpracování	k1gNnSc6
zůstaly	zůstat	k5eAaPmAgInP
zmrazeny	zmrazit	k5eAaPmNgInP
a	a	k8xC
získaný	získaný	k2eAgInSc1d1
mošt	mošt	k1gInSc1
vykazoval	vykazovat	k5eAaImAgInS
nejméně	málo	k6eAd3
27	#num#	k4
°	°	k?
<g/>
NM	NM	kA
<g/>
.	.	kIx.
</s>
<s>
Slámové	Slámové	k2eAgNnSc1d1
víno	víno	k1gNnSc1
<g/>
:	:	kIx,
vyrábí	vyrábět	k5eAaImIp3nS
se	se	k3xPyFc4
z	z	k7c2
hroznů	hrozen	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
byly	být	k5eAaImAgInP
před	před	k7c7
zpracováním	zpracování	k1gNnSc7
skladovány	skladovat	k5eAaImNgFnP
na	na	k7c6
slámě	sláma	k1gFnSc6
či	či	k8xC
rákosu	rákos	k1gInSc6
nebo	nebo	k8xC
byly	být	k5eAaImAgInP
zavěšeny	zavěsit	k5eAaPmNgInP
ve	v	k7c6
větraném	větraný	k2eAgInSc6d1
prostoru	prostor	k1gInSc6
po	po	k7c4
dobu	doba	k1gFnSc4
alespoň	alespoň	k9
3	#num#	k4
měsíců	měsíc	k1gInPc2
a	a	k8xC
získaný	získaný	k2eAgInSc1d1
mošt	mošt	k1gInSc1
vykazoval	vykazovat	k5eAaImAgInS
nejméně	málo	k6eAd3
27	#num#	k4
°	°	k?
<g/>
NM	NM	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vykazuje	vykazovat	k5eAaImIp3nS
<g/>
-li	-li	k?
však	však	k9
mošt	mošt	k1gInSc4
již	již	k6eAd1
po	po	k7c6
2	#num#	k4
měsících	měsíc	k1gInPc6
cukernatost	cukernatost	k1gFnSc4
nejméně	málo	k6eAd3
32	#num#	k4
°	°	k?
<g/>
NM	NM	kA
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
se	se	k3xPyFc4
provést	provést	k5eAaPmF
lisování	lisování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Některé	některý	k3yIgFnPc1
další	další	k2eAgFnPc1d1
tzv.	tzv.	kA
tradiční	tradiční	k2eAgInPc1d1
výrazy	výraz	k1gInPc1
užívané	užívaný	k2eAgInPc1d1
k	k	k7c3
označování	označování	k1gNnSc3
vín	vína	k1gFnPc2
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Botrytický	Botrytický	k2eAgInSc1d1
sběr	sběr	k1gInSc1
<g/>
:	:	kIx,
jakostní	jakostní	k2eAgNnSc1d1
víno	víno	k1gNnSc1
s	s	k7c7
přívlastkem	přívlastek	k1gInSc7
druhů	druh	k1gInPc2
výběr	výběr	k1gInSc1
z	z	k7c2
hroznů	hrozen	k1gInPc2
nebo	nebo	k8xC
výběr	výběr	k1gInSc1
z	z	k7c2
bobulí	bobule	k1gFnPc2
nebo	nebo	k8xC
výběr	výběr	k1gInSc1
z	z	k7c2
cibéb	cibéba	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
bylo	být	k5eAaImAgNnS
vyrobeno	vyrobit	k5eAaPmNgNnS
z	z	k7c2
hroznů	hrozen	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
byly	být	k5eAaImAgFnP
aspoň	aspoň	k9
z	z	k7c2
30	#num#	k4
%	%	kIx~
napadeny	napaden	k2eAgInPc1d1
ušlechtilou	ušlechtilý	k2eAgFnSc7d1
plísní	plíseň	k1gFnSc7
šedou	šedý	k2eAgFnSc4d1
Botrytis	Botrytis	k1gFnSc4
cinerea	cinere	k1gInSc2
P.	P.	kA
</s>
<s>
Barrique	Barrique	k1gNnSc1
<g/>
:	:	kIx,
víno	víno	k1gNnSc1
zrálo	zrát	k5eAaImAgNnS
nejméně	málo	k6eAd3
6	#num#	k4
měsíců	měsíc	k1gInPc2
v	v	k7c6
dubovém	dubový	k2eAgInSc6d1
sudu	sud	k1gInSc6
o	o	k7c6
objemu	objem	k1gInSc6
225	#num#	k4
litrů	litr	k1gInPc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
nebyl	být	k5eNaImAgInS
používán	používat	k5eAaImNgInS
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
vína	víno	k1gNnSc2
déle	dlouho	k6eAd2
než	než	k8xS
36	#num#	k4
měsíců	měsíc	k1gInPc2
</s>
<s>
Sur	Sur	k?
lie	lie	k?
nebo	nebo	k8xC
též	též	k9
zrálo	zrát	k5eAaImAgNnS
na	na	k7c6
kvasnicích	kvasnice	k1gFnPc6
nebo	nebo	k8xC
školeno	školit	k5eAaImNgNnS
na	na	k7c6
kvasnicích	kvasnice	k1gFnPc6
anebo	anebo	k8xC
krášleno	krášlit	k5eAaImNgNnS
na	na	k7c6
kvasnicích	kvasnice	k1gFnPc6
<g/>
:	:	kIx,
víno	víno	k1gNnSc1
při	při	k7c6
výrobě	výroba	k1gFnSc6
bylo	být	k5eAaImAgNnS
ponecháno	ponechat	k5eAaPmNgNnS
na	na	k7c6
kvasnicích	kvasnice	k1gFnPc6
po	po	k7c4
dobu	doba	k1gFnSc4
nejméně	málo	k6eAd3
6	#num#	k4
měsíců	měsíc	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Vína	vína	k1gFnSc1
originální	originální	k2eAgFnSc2d1
certifikace	certifikace	k1gFnSc2
(	(	kIx(
<g/>
VOC	VOC	kA
<g/>
)	)	kIx)
</s>
<s>
Jakost	jakost	k1gFnSc1
vína	víno	k1gNnSc2
je	být	k5eAaImIp3nS
zaručována	zaručovat	k5eAaImNgFnS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
víno	víno	k1gNnSc4
typické	typický	k2eAgNnSc4d1
pro	pro	k7c4
konkrétní	konkrétní	k2eAgFnSc4d1
lokalitu	lokalita	k1gFnSc4
(	(	kIx(
<g/>
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
stejná	stejný	k2eAgFnSc1d1
nebo	nebo	k8xC
menší	malý	k2eAgFnSc1d2
než	než	k8xS
vinařská	vinařský	k2eAgFnSc1d1
podoblast	podoblast	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
její	její	k3xOp3gFnPc1
podmínky	podmínka	k1gFnPc1
a	a	k8xC
prošlo	projít	k5eAaPmAgNnS
hodnocením	hodnocení	k1gNnSc7
kolegia	kolegium	k1gNnPc1
hodnotitelů	hodnotitel	k1gMnPc2
z	z	k7c2
řad	řada	k1gFnPc2
členů	člen	k1gInPc2
příslušného	příslušný	k2eAgNnSc2d1
sdružení	sdružení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povolení	povolení	k1gNnSc1
označovat	označovat	k5eAaImF
vína	víno	k1gNnSc2
jako	jako	k8xS,k8xC
VOC	VOC	kA
získávají	získávat	k5eAaImIp3nP
od	od	k7c2
Ministerstva	ministerstvo	k1gNnSc2
zemědělství	zemědělství	k1gNnSc2
vinařské	vinařský	k2eAgInPc1d1
spolky	spolek	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
pro	pro	k7c4
to	ten	k3xDgNnSc4
musí	muset	k5eAaImIp3nS
splnit	splnit	k5eAaPmF
několik	několik	k4yIc4
podmínek	podmínka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ministerstvo	ministerstvo	k1gNnSc1
zemědělství	zemědělství	k1gNnSc2
také	také	k9
činnost	činnost	k1gFnSc1
těchto	tento	k3xDgInPc2
spolků	spolek	k1gInPc2
dozoruje	dozorovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Víno	víno	k1gNnSc1
VOC	VOC	kA
je	být	k5eAaImIp3nS
typickým	typický	k2eAgMnSc7d1
představitelem	představitel	k1gMnSc7
původu	původ	k1gInSc2
hroznů	hrozen	k1gInPc2
<g/>
,	,	kIx,
místa	místo	k1gNnSc2
svého	svůj	k3xOyFgInSc2
vzniku	vznik	k1gInSc2
a	a	k8xC
vinařské	vinařský	k2eAgFnSc2d1
tradice	tradice	k1gFnSc2
<g/>
,	,	kIx,
často	často	k6eAd1
se	s	k7c7
specifickými	specifický	k2eAgNnPc7d1
pěstitelskými	pěstitelský	k2eAgNnPc7d1
a	a	k8xC
vinařskými	vinařský	k2eAgNnPc7d1
pravidly	pravidlo	k1gNnPc7
a	a	k8xC
má	mít	k5eAaImIp3nS
jedinečný	jedinečný	k2eAgInSc1d1
vzhled	vzhled	k1gInSc1
<g/>
,	,	kIx,
aroma	aroma	k1gNnSc1
i	i	k8xC
chuť	chuť	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
naprosté	naprostý	k2eAgFnSc6d1
většině	většina	k1gFnSc6
je	být	k5eAaImIp3nS
odrůdové	odrůdový	k2eAgNnSc1d1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
přípustné	přípustný	k2eAgFnSc2d1
odrůdy	odrůda	k1gFnSc2
stanoví	stanovit	k5eAaPmIp3nS
sdružení	sdružení	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vína	vína	k1gFnSc1
VOC	VOC	kA
musí	muset	k5eAaImIp3nS
odpovídat	odpovídat	k5eAaImF
alespoň	alespoň	k9
jakostním	jakostní	k2eAgInPc3d1
požadavkům	požadavek	k1gInPc3
pro	pro	k7c4
jakostní	jakostní	k2eAgNnSc4d1
víno	víno	k1gNnSc4
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gNnSc4
zatřídění	zatřídění	k1gNnSc4
ale	ale	k8xC
neprovádí	provádět	k5eNaImIp3nS
příslušný	příslušný	k2eAgInSc1d1
státní	státní	k2eAgInSc1d1
dozorový	dozorový	k2eAgInSc1d1
orgán	orgán	k1gInSc1
<g/>
,	,	kIx,
Státní	státní	k2eAgFnSc1d1
zemědělská	zemědělský	k2eAgFnSc1d1
a	a	k8xC
potravinářská	potravinářský	k2eAgFnSc1d1
inspekce	inspekce	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
zatřiďují	zatřiďovat	k5eAaImIp3nP
je	on	k3xPp3gInPc4
jednotlivé	jednotlivý	k2eAgInPc4d1
spolky	spolek	k1gInPc4
prostřednictvím	prostřednictvím	k7c2
degustátorů	degustátor	k1gMnPc2
nominovaných	nominovaný	k2eAgMnPc2d1
spolkem	spolek	k1gInSc7
za	za	k7c4
splnění	splnění	k1gNnSc4
určitých	určitý	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Víno	víno	k1gNnSc1
odpovídající	odpovídající	k2eAgFnSc2d1
charakteristice	charakteristika	k1gFnSc6
VOC	VOC	kA
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
označené	označený	k2eAgInPc1d1
VOC	VOC	kA
<g/>
,	,	kIx,
víno	víno	k1gNnSc1
jakostní	jakostní	k2eAgInPc1d1
<g/>
,	,	kIx,
případně	případně	k6eAd1
přívlastkové	přívlastkový	k2eAgNnSc1d1
<g/>
,	,	kIx,
nebo	nebo	k8xC
zemské	zemský	k2eAgNnSc1d1
víno	víno	k1gNnSc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
dokonce	dokonce	k9
i	i	k9
jen	jen	k9
víno	víno	k1gNnSc1
<g/>
,	,	kIx,
neboť	neboť	k8xC
je	být	k5eAaImIp3nS
na	na	k7c6
volbě	volba	k1gFnSc6
vinaře	vinař	k1gMnPc4
<g/>
,	,	kIx,
zda	zda	k8xS
požádá	požádat	k5eAaPmIp3nS
o	o	k7c4
zatřídění	zatřídění	k1gNnSc4
vína	víno	k1gNnSc2
odpovídajícího	odpovídající	k2eAgInSc2d1
požadavkům	požadavek	k1gInPc3
na	na	k7c4
víno	víno	k1gNnSc4
VOC	VOC	kA
spolek	spolek	k1gInSc1
VOC	VOC	kA
nebo	nebo	k8xC
Státní	státní	k2eAgFnSc4d1
zemědělskou	zemědělský	k2eAgFnSc4d1
a	a	k8xC
potravinářskou	potravinářský	k2eAgFnSc4d1
inspekci	inspekce	k1gFnSc4
<g/>
,	,	kIx,
případně	případně	k6eAd1
zda	zda	k8xS
jej	on	k3xPp3gMnSc4
zatřídit	zatřídit	k5eAaPmF
nenechá	nechat	k5eNaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Sdružení	sdružení	k1gNnSc1
VOC	VOC	kA
je	být	k5eAaImIp3nS
v	v	k7c6
současnosti	současnost	k1gFnSc6
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
ČR	ČR	kA
třináct	třináct	k4xCc1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
tato	tento	k3xDgNnPc1
(	(	kIx(
<g/>
jsou	být	k5eAaImIp3nP
uvedena	uvést	k5eAaPmNgNnP
podle	podle	k7c2
data	datum	k1gNnSc2
vzniku	vznik	k1gInSc2
<g/>
)	)	kIx)
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Znojmo	Znojmo	k1gNnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Mikulov	Mikulov	k1gInSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Modré	modrý	k2eAgFnPc1d1
Hory	hora	k1gFnPc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Pálava	Pálava	k1gFnSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Blatnice	Blatnice	k1gFnPc1
(	(	kIx(
<g/>
Cech	cech	k1gInSc1
blatnických	blatnický	k2eAgMnPc2d1
vinařů	vinař	k1gMnPc2
<g/>
)	)	kIx)
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Valtice	Valtice	k1gFnPc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Mělník	Mělník	k1gInSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Slovácko	Slovácko	k1gNnSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hustopečsko	Hustopečsko	k1gNnSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kraví	kraví	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Bzenec	Bzenec	k1gInSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Mutěnice	Mutěnice	k1gFnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vinice	vinice	k1gFnPc1
Velké	velký	k2eAgFnPc1d1
Pavlovice	Pavlovice	k1gFnPc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
vyprodukovali	vyprodukovat	k5eAaPmAgMnP
vinaři	vinař	k1gMnPc1
všech	všecek	k3xTgFnPc2
VOC	VOC	kA
spolků	spolek	k1gInPc2
410	#num#	k4
tisíc	tisíc	k4xCgInPc2
litrů	litr	k1gInPc2
vína	víno	k1gNnSc2
VOC	VOC	kA
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
na	na	k7c6
celkové	celkový	k2eAgFnSc6d1
české	český	k2eAgFnSc6d1
produkci	produkce	k1gFnSc6
o	o	k7c6
objemu	objem	k1gInSc6
68	#num#	k4
milionů	milion	k4xCgInPc2
litrů	litr	k1gInPc2
představuje	představovat	k5eAaImIp3nS
podíl	podíl	k1gInSc1
0,6	0,6	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s>
Společný	společný	k2eAgInSc1d1
systém	systém	k1gInSc1
ochrany	ochrana	k1gFnSc2
označování	označování	k1gNnSc2
jakostních	jakostní	k2eAgNnPc2d1
vín	víno	k1gNnPc2
v	v	k7c6
Evropské	evropský	k2eAgFnSc6d1
unii	unie	k1gFnSc6
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dovozovat	dovozovat	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
sousloví	sousloví	k1gNnSc1
označení	označení	k1gNnSc2
původu	původ	k1gInSc2
zprostředkovává	zprostředkovávat	k5eAaImIp3nS
informaci	informace	k1gFnSc4
o	o	k7c6
místě	místo	k1gNnSc6
pěstování	pěstování	k1gNnSc2
révy	réva	k1gFnSc2
vinné	vinný	k2eAgFnSc6d1
nebo	nebo	k8xC
místě	místo	k1gNnSc6
výroby	výroba	k1gFnSc2
vína	víno	k1gNnSc2
je	být	k5eAaImIp3nS
pravda	pravda	k1gFnSc1
jen	jen	k9
zčásti	zčásti	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Označení	označení	k1gNnSc1
původu	původ	k1gInSc2
jako	jako	k8xS,k8xC
kód	kód	k1gInSc4
nese	nést	k5eAaImIp3nS
informací	informace	k1gFnSc7
mnohem	mnohem	k6eAd1
více	hodně	k6eAd2
<g/>
,	,	kIx,
k	k	k7c3
jejich	jejich	k3xOp3gInSc3
popisu	popis	k1gInSc3
často	často	k6eAd1
nestačí	stačit	k5eNaBmIp3nP
dvě	dva	k4xCgNnPc4
<g/>
,	,	kIx,
tři	tři	k4xCgFnPc1
i	i	k8xC
čtyři	čtyři	k4xCgFnPc1
desítky	desítka	k1gFnPc1
stran	stran	k7c2
textu	text	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Systémově	systémově	k6eAd1
mohou	moct	k5eAaImIp3nP
označení	označení	k1gNnSc4
původu	původ	k1gInSc2
vzniknou	vzniknout	k5eAaPmIp3nP
Bez	bez	k7c2
žádosti	žádost	k1gFnSc2
<g/>
,	,	kIx,
tedy	tedy	k9
z	z	k7c2
vůle	vůle	k1gFnSc2
státu	stát	k1gInSc2
<g/>
,	,	kIx,
Na	na	k7c4
žádost	žádost	k1gFnSc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
na	na	k7c4
přání	přání	k1gNnSc4
žadatele	žadatel	k1gMnSc2
a	a	k8xC
se	s	k7c7
souhlasem	souhlas	k1gInSc7
státu	stát	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
Na	na	k7c4
truc	truc	k1gInSc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
na	na	k7c4
přání	přání	k1gNnSc4
autora	autor	k1gMnSc2
a	a	k8xC
bez	bez	k7c2
souhlasu	souhlas	k1gInSc2
státu	stát	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jak	jak	k6eAd1
lze	lze	k6eAd1
zjistit	zjistit	k5eAaPmF
z	z	k7c2
evropského	evropský	k2eAgInSc2d1
registru	registr	k1gInSc2
eAmbrosia	eAmbrosium	k1gNnSc2
<g/>
,	,	kIx,
jen	jen	k9
pro	pro	k7c4
víno	víno	k1gNnSc4
takových	takový	k3xDgInPc2
označení	označení	k1gNnSc1
původu	původ	k1gInSc2
má	mít	k5eAaImIp3nS
například	například	k6eAd1
Francie	Francie	k1gFnSc1
437	#num#	k4
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
524	#num#	k4
a	a	k8xC
Španělsko	Španělsko	k1gNnSc1
142	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
jejich	jejich	k3xOp3gInSc6
případě	případ	k1gInSc6
vždy	vždy	k6eAd1
Na	na	k7c4
žádost	žádost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nespornou	sporný	k2eNgFnSc7d1
výhodou	výhoda	k1gFnSc7
této	tento	k3xDgFnSc2
registrace	registrace	k1gFnSc2
je	být	k5eAaImIp3nS
ochrana	ochrana	k1gFnSc1
těchto	tento	k3xDgNnPc2
označení	označení	k1gNnPc2
původu	původ	k1gInSc2
v	v	k7c6
evropském	evropský	k2eAgNnSc6d1
i	i	k8xC
mezinárodním	mezinárodní	k2eAgNnSc6d1
měřítku	měřítko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
právní	právní	k2eAgFnSc6d1
ochraně	ochrana	k1gFnSc6
označení	označení	k1gNnSc2
původu	původ	k1gInSc2
podrobněji	podrobně	k6eAd2
v	v	k7c6
Ochrana	ochrana	k1gFnSc1
označení	označení	k1gNnSc4
původu	původ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
českých	český	k2eAgNnPc2d1
označení	označení	k1gNnPc2
původu	původ	k1gInSc2
jsou	být	k5eAaImIp3nP
v	v	k7c6
registru	registrum	k1gNnSc6
zapsané	zapsaný	k2eAgFnPc1d1
všechny	všechen	k3xTgFnPc1
vinařské	vinařský	k2eAgFnPc1d1
oblasti	oblast	k1gFnPc1
i	i	k8xC
podoblasti	podoblast	k1gFnPc1
coby	coby	k?
označení	označení	k1gNnSc1
původu	původ	k1gInSc2
Bez	bez	k7c2
žádosti	žádost	k1gFnSc2
<g/>
,	,	kIx,
jedno	jeden	k4xCgNnSc1
z	z	k7c2
celkových	celkový	k2eAgNnPc2d1
třinácti	třináct	k4xCc2
označení	označení	k1gNnPc2
původu	původ	k1gInSc2
Na	na	k7c4
žádost	žádost	k1gFnSc4
pro	pro	k7c4
vína	víno	k1gNnPc4
VOC	VOC	kA
<g/>
,	,	kIx,
a	a	k8xC
dvě	dva	k4xCgNnPc1
označení	označení	k1gNnSc1
původu	původ	k1gInSc2
Na	na	k7c4
truc	truc	k1gInSc4
<g/>
,	,	kIx,
i	i	k9
když	když	k8xS
jich	on	k3xPp3gMnPc2
v	v	k7c6
Česku	Česko	k1gNnSc6
existuje	existovat	k5eAaImIp3nS
víc	hodně	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neregistrovaná	registrovaný	k2eNgNnPc4d1
označení	označení	k1gNnSc4
původu	původ	k1gInSc2
nepožívají	požívat	k5eNaImIp3nP
tuzemské	tuzemský	k2eAgFnPc1d1
<g/>
,	,	kIx,
evropské	evropský	k2eAgFnSc2d1
ani	ani	k8xC
mezinárodní	mezinárodní	k2eAgFnSc2d1
právní	právní	k2eAgFnSc2d1
ochrany	ochrana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Označování	označování	k1gNnSc1
vína	víno	k1gNnSc2
s	s	k7c7
ohledem	ohled	k1gInSc7
na	na	k7c4
dodržení	dodržení	k1gNnSc4
postupů	postup	k1gInPc2
šetrných	šetrný	k2eAgMnPc2d1
k	k	k7c3
přírodě	příroda	k1gFnSc3
a	a	k8xC
tradicím	tradice	k1gFnPc3
</s>
<s>
Biovíno	Biovína	k1gFnSc5
(	(	kIx(
<g/>
víno	víno	k1gNnSc4
z	z	k7c2
ekologické	ekologický	k2eAgFnSc2d1
produkce	produkce	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
Evropské	evropský	k2eAgFnSc6d1
unii	unie	k1gFnSc6
se	se	k3xPyFc4
jako	jako	k9
biovíno	biovína	k1gFnSc5
smějí	smát	k5eAaImIp3nP
označovat	označovat	k5eAaImF
taková	takový	k3xDgNnPc4
vína	víno	k1gNnPc4
<g/>
,	,	kIx,
která	který	k3yRgNnPc1,k3yIgNnPc1,k3yQgNnPc1
byla	být	k5eAaImAgNnP
vyrobena	vyrobit	k5eAaPmNgNnP
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
nařízením	nařízení	k1gNnSc7
Rady	rada	k1gFnSc2
o	o	k7c6
ekologické	ekologický	k2eAgFnSc6d1
produkci	produkce	k1gFnSc6
a	a	k8xC
označování	označování	k1gNnSc6
ekologických	ekologický	k2eAgInPc2d1
produktů	produkt	k1gInPc2
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
vína	víno	k1gNnPc4
<g/>
,	,	kIx,
u	u	k7c2
kterých	který	k3yIgFnPc2,k3yQgFnPc2,k3yRgFnPc2
byly	být	k5eAaImAgFnP
při	při	k7c6
pěstování	pěstování	k1gNnSc6
révy	réva	k1gFnSc2
použity	použít	k5eAaPmNgInP
takové	takový	k3xDgInPc1
postupy	postup	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
zachovávají	zachovávat	k5eAaImIp3nP
nebo	nebo	k8xC
zvyšují	zvyšovat	k5eAaImIp3nP
obsah	obsah	k1gInSc4
organických	organický	k2eAgFnPc2d1
látek	látka	k1gFnPc2
v	v	k7c6
půdě	půda	k1gFnSc6
<g/>
,	,	kIx,
zvyšují	zvyšovat	k5eAaImIp3nP
stabilitu	stabilita	k1gFnSc4
půdy	půda	k1gFnSc2
a	a	k8xC
její	její	k3xOp3gFnSc4
biologickou	biologický	k2eAgFnSc4d1
rozmanitost	rozmanitost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hnojiva	hnojivo	k1gNnPc1
a	a	k8xC
pomocné	pomocný	k2eAgFnPc1d1
půdní	půdní	k2eAgFnPc1d1
látky	látka	k1gFnPc1
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
používat	používat	k5eAaImF
jen	jen	k9
za	za	k7c2
předpokladu	předpoklad	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
byly	být	k5eAaImAgFnP
schváleny	schválit	k5eAaPmNgFnP
pro	pro	k7c4
použití	použití	k1gNnSc4
v	v	k7c6
ekologické	ekologický	k2eAgFnSc6d1
produkci	produkce	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Minerální	minerální	k2eAgNnPc1d1
dusíkatá	dusíkatý	k2eAgNnPc1d1
hnojiva	hnojivo	k1gNnPc1
se	se	k3xPyFc4
nepoužívají	používat	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biologická	biologický	k2eAgFnSc1d1
aktivita	aktivita	k1gFnSc1
půdy	půda	k1gFnSc2
se	se	k3xPyFc4
udržuje	udržovat	k5eAaImIp3nS
a	a	k8xC
zvyšuje	zvyšovat	k5eAaImIp3nS
např.	např.	kA
používáním	používání	k1gNnSc7
chlévské	chlévský	k2eAgFnSc2d1
mrvy	mrva	k1gFnSc2
či	či	k8xC
kompostovaných	kompostovaný	k2eAgInPc2d1
organických	organický	k2eAgInPc2d1
materiálů	materiál	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prevence	prevence	k1gFnSc1
škod	škoda	k1gFnPc2
způsobených	způsobený	k2eAgFnPc2d1
škůdci	škůdce	k1gMnPc7
<g/>
,	,	kIx,
chorobami	choroba	k1gFnPc7
a	a	k8xC
plevely	plevel	k1gInPc7
je	být	k5eAaImIp3nS
založena	založen	k2eAgFnSc1d1
především	především	k9
na	na	k7c6
ochraně	ochrana	k1gFnSc6
přirozenými	přirozený	k2eAgMnPc7d1
nepřáteli	nepřítel	k1gMnPc7
<g/>
,	,	kIx,
a	a	k8xC
vhodné	vhodný	k2eAgFnSc6d1
volbě	volba	k1gFnSc6
odrůd	odrůda	k1gFnPc2
(	(	kIx(
<g/>
např.	např.	kA
tzv	tzv	kA
PIWI	PIWI	kA
odrůdy	odrůda	k1gFnSc2
–	–	k?
odrůdy	odrůda	k1gFnSc2
odolné	odolný	k2eAgFnPc1d1
vůči	vůči	k7c3
plísním	plíseň	k1gFnPc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
výroba	výroba	k1gFnSc1
vína	víno	k1gNnSc2
z	z	k7c2
hroznů	hrozen	k1gInPc2
podléhá	podléhat	k5eAaImIp3nS
omezením	omezení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvláštní	zvláštní	k2eAgFnSc1d1
prováděcí	prováděcí	k2eAgNnSc4d1
nařízení	nařízení	k1gNnSc4
Evropské	evropský	k2eAgFnSc2d1
komise	komise	k1gFnSc2
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
určuje	určovat	k5eAaImIp3nS
<g/>
,	,	kIx,
jaké	jaký	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
postupy	postup	k1gInPc4
se	se	k3xPyFc4
při	při	k7c6
výrobě	výroba	k1gFnSc6
biovína	biovín	k1gInSc2
smějí	smát	k5eAaImIp3nP
používat	používat	k5eAaImF
a	a	k8xC
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
postupy	postup	k1gInPc1
<g/>
,	,	kIx,
jinak	jinak	k6eAd1
v	v	k7c6
moderním	moderní	k2eAgNnSc6d1
vinařství	vinařství	k1gNnSc6
přípustné	přípustný	k2eAgFnPc1d1
<g/>
,	,	kIx,
se	se	k3xPyFc4
u	u	k7c2
biovín	biovína	k1gFnPc2
používat	používat	k5eAaImF
nesmějí	smát	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
je	být	k5eAaImIp3nS
v	v	k7c6
biovínech	biovín	k1gInPc6
omezeno	omezit	k5eAaPmNgNnS
přípustné	přípustný	k2eAgNnSc1d1
množství	množství	k1gNnSc1
oxidu	oxid	k1gInSc2
siřičitého	siřičitý	k2eAgInSc2d1
(	(	kIx(
<g/>
SO	So	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
nutný	nutný	k2eAgInSc1d1
ke	k	k7c3
konzervaci	konzervace	k1gFnSc3
vína	víno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Víno	víno	k1gNnSc1
z	z	k7c2
integrované	integrovaný	k2eAgFnSc2d1
produkce	produkce	k1gFnSc2
</s>
<s>
Integrovaná	integrovaný	k2eAgFnSc1d1
produkce	produkce	k1gFnSc1
je	být	k5eAaImIp3nS
mezistupněm	mezistupeň	k1gInSc7
mezi	mezi	k7c7
běžnou	běžný	k2eAgFnSc7d1
produkcí	produkce	k1gFnSc7
a	a	k8xC
ekologickou	ekologický	k2eAgFnSc7d1
produkcí	produkce	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Omezení	omezení	k1gNnSc1
se	se	k3xPyFc4
týkají	týkat	k5eAaImIp3nP
spíše	spíše	k9
pěstování	pěstování	k1gNnSc4
révy	réva	k1gFnSc2
na	na	k7c6
vinici	vinice	k1gFnSc6
než	než	k8xS
výroby	výroba	k1gFnSc2
vína	víno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používají	používat	k5eAaImIp3nP
se	se	k3xPyFc4
postupy	postup	k1gInPc1
šetrné	šetrný	k2eAgInPc1d1
k	k	k7c3
životnímu	životní	k2eAgNnSc3d1
prostředí	prostředí	k1gNnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
limity	limit	k1gInPc1
pro	pro	k7c4
používání	používání	k1gNnSc4
hnojiv	hnojivo	k1gNnPc2
a	a	k8xC
prostředků	prostředek	k1gInPc2
na	na	k7c4
ochranu	ochrana	k1gFnSc4
před	před	k7c7
škůdci	škůdce	k1gMnPc7
nejsou	být	k5eNaImIp3nP
tak	tak	k9
přísné	přísný	k2eAgFnPc1d1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
v	v	k7c6
ekologické	ekologický	k2eAgFnSc6d1
produkci	produkce	k1gFnSc6
<g/>
;	;	kIx,
vinohradník	vinohradník	k1gMnSc1
takové	takový	k3xDgInPc4
prostředky	prostředek	k1gInPc4
nepoužívá	používat	k5eNaImIp3nS
plošně	plošně	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
jen	jen	k9
je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
jich	on	k3xPp3gMnPc2
nezbytně	zbytně	k6eNd1,k6eAd1
třeba	třeba	k6eAd1
a	a	k8xC
v	v	k7c6
nezbytné	zbytný	k2eNgFnSc6d1,k2eAgFnSc6d1
míře	míra	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Autentické	autentický	k2eAgNnSc1d1
víno	víno	k1gNnSc1
</s>
<s>
Jako	jako	k8xS,k8xC
autentická	autentický	k2eAgFnSc1d1
vína	vína	k1gFnSc1
se	se	k3xPyFc4
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
označují	označovat	k5eAaImIp3nP
vína	víno	k1gNnPc4
vyrobená	vyrobený	k2eAgNnPc4d1
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
Chartou	charta	k1gFnSc7
autentistů	autentista	k1gMnPc2
–	–	k?
spolku	spolek	k1gInSc2
naturálních	naturální	k2eAgMnPc2d1
vinařů	vinař	k1gMnPc2
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autentisté	Autentista	k1gMnPc1
se	se	k3xPyFc4
řídí	řídit	k5eAaImIp3nP
zásadami	zásada	k1gFnPc7
ekologické	ekologický	k2eAgFnSc2d1
produkce	produkce	k1gFnSc2
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
odst	odst	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biovíno	Biovína	k1gFnSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
navíc	navíc	k6eAd1
dbají	dbát	k5eAaImIp3nP
na	na	k7c4
dodržování	dodržování	k1gNnSc4
tradičních	tradiční	k2eAgInPc2d1
postupů	postup	k1gInPc2
jak	jak	k8xC,k8xS
ve	v	k7c6
vinohradě	vinohrad	k1gInSc6
<g/>
,	,	kIx,
tak	tak	k6eAd1
ve	v	k7c6
sklepě	sklep	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dávají	dávat	k5eAaImIp3nP
také	také	k9
přednost	přednost	k1gFnSc4
tradičně	tradičně	k6eAd1
u	u	k7c2
nás	my	k3xPp1nPc2
pěstovaným	pěstovaný	k2eAgFnPc3d1
odrůdám	odrůda	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Meziřadí	Meziřadí	k1gNnSc1
vinic	vinice	k1gFnPc2
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
trvale	trvale	k6eAd1
ozeleněno	ozeleněn	k2eAgNnSc1d1
<g/>
,	,	kIx,
vinice	vinice	k1gFnPc1
se	se	k3xPyFc4
nesmějí	smát	k5eNaImIp3nP
uměle	uměle	k6eAd1
zavlažovat	zavlažovat	k5eAaImF
a	a	k8xC
hrozny	hrozen	k1gInPc1
se	se	k3xPyFc4
musí	muset	k5eAaImIp3nP
sklízet	sklízet	k5eAaImF
ručně	ručně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
moštu	mošt	k1gInSc2
se	se	k3xPyFc4
nesmějí	smát	k5eNaImIp3nP
dodávat	dodávat	k5eAaImF
kvasinky	kvasinka	k1gFnPc4
–	–	k?
víno	víno	k1gNnSc1
musí	muset	k5eAaImIp3nS
kvasit	kvasit	k5eAaImF
spontánně	spontánně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnohé	mnohý	k2eAgInPc1d1
postupy	postup	k1gInPc1
moderního	moderní	k2eAgNnSc2d1
vinařství	vinařství	k1gNnSc2
jsou	být	k5eAaImIp3nP
vyloučeny	vyloučen	k2eAgInPc1d1
(	(	kIx(
<g/>
např.	např.	kA
zahušťování	zahušťování	k1gNnSc1
moštu	mošt	k1gInSc2
reverzní	reverzní	k2eAgFnSc7d1
osmózou	osmóza	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autentická	autentický	k2eAgFnSc1d1
vína	vína	k1gFnSc1
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
plnit	plnit	k5eAaImF
pouze	pouze	k6eAd1
do	do	k7c2
skleněných	skleněný	k2eAgFnPc2d1
lahví	lahev	k1gFnPc2
uzavřených	uzavřený	k2eAgFnPc2d1
korkovou	korkový	k2eAgFnSc7d1
nebo	nebo	k8xC
skleněnou	skleněný	k2eAgFnSc7d1
zátkou	zátka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nesmějí	smát	k5eNaImIp3nP
se	se	k3xPyFc4
prodávat	prodávat	k5eAaImF
jako	jako	k9
sudová	sudová	k1gFnSc1
a	a	k8xC
nesmějí	smát	k5eNaImIp3nP
se	se	k3xPyFc4
dodávat	dodávat	k5eAaImF
do	do	k7c2
super-	super-	k?
a	a	k8xC
hypermarketů	hypermarket	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Biodynamické	biodynamický	k2eAgNnSc1d1
víno	víno	k1gNnSc1
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tzv.	tzv.	kA
biodynamické	biodynamický	k2eAgNnSc1d1
vinařství	vinařství	k1gNnSc1
postupuje	postupovat	k5eAaImIp3nS
podle	podle	k7c2
zásad	zásada	k1gFnPc2
biodynamického	biodynamický	k2eAgNnSc2d1
zemědělství	zemědělství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
v	v	k7c6
podstatě	podstata	k1gFnSc6
starobylé	starobylý	k2eAgInPc1d1
postupy	postup	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
byly	být	k5eAaImAgFnP
obvyklé	obvyklý	k2eAgInPc1d1
před	před	k7c7
nástupem	nástup	k1gInSc7
umělých	umělý	k2eAgNnPc2d1
hnojiv	hnojivo	k1gNnPc2
a	a	k8xC
postřiků	postřik	k1gInPc2
<g/>
,	,	kIx,
tedy	tedy	k9
zhruba	zhruba	k6eAd1
do	do	k7c2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biodynamičtí	biodynamický	k2eAgMnPc1d1
vinaři	vinař	k1gMnPc1
se	se	k3xPyFc4
řídí	řídit	k5eAaImIp3nS
tzv.	tzv.	kA
biodynamickým	biodynamický	k2eAgInSc7d1
kalendářem	kalendář	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
spoléhá	spoléhat	k5eAaImIp3nS
na	na	k7c4
přírodní	přírodní	k2eAgFnPc4d1
síly	síla	k1gFnPc4
a	a	k8xC
také	také	k9
používají	používat	k5eAaImIp3nP
postupy	postup	k1gInPc1
<g/>
,	,	kIx,
pro	pro	k7c4
které	který	k3yQgNnSc4,k3yRgNnSc4,k3yIgNnSc4
neexistuje	existovat	k5eNaImIp3nS
racionální	racionální	k2eAgNnSc4d1
zdůvodnění	zdůvodnění	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnSc2
práce	práce	k1gFnSc2
dělají	dělat	k5eAaImIp3nP
jen	jen	k9
v	v	k7c6
určitých	určitý	k2eAgFnPc6d1
fázích	fáze	k1gFnPc6
měsíce	měsíc	k1gInPc4
a	a	k8xC
podobně	podobně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Přírodní	přírodní	k2eAgNnSc1d1
víno	víno	k1gNnSc1
</s>
<s>
Dřívější	dřívější	k2eAgNnSc4d1
označení	označení	k1gNnSc4
pro	pro	k7c4
vína	víno	k1gNnPc4
přívlastková	přívlastkový	k2eAgNnPc4d1
vznikající	vznikající	k2eAgInPc4d1
bez	bez	k7c2
cukření	cukření	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mešní	mešní	k2eAgNnSc1d1
víno	víno	k1gNnSc1
<g/>
,	,	kIx,
košer	košer	k2eAgNnSc1d1
víno	víno	k1gNnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článcích	článek	k1gInPc6
Mešní	mešní	k2eAgNnPc4d1
víno	víno	k1gNnSc4
a	a	k8xC
košer	košer	k2eAgNnSc4d1
víno	víno	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Takto	takto	k6eAd1
smí	smět	k5eAaImIp3nS
být	být	k5eAaImF
víno	víno	k1gNnSc1
označeno	označit	k5eAaPmNgNnS
jen	jen	k6eAd1
pokud	pokud	k8xS
byl	být	k5eAaImAgInS
vydán	vydat	k5eAaPmNgInS
písemný	písemný	k2eAgInSc1d1
souhlas	souhlas	k1gInSc1
příslušné	příslušný	k2eAgFnSc2d1
církve	církev	k1gFnSc2
nebo	nebo	k8xC
náboženské	náboženský	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
s	s	k7c7
jeho	jeho	k3xOp3gNnSc7
uváděním	uvádění	k1gNnSc7
do	do	k7c2
oběhu	oběh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Církev	církev	k1gFnSc1
nebo	nebo	k8xC
Židovská	židovský	k2eAgFnSc1d1
obec	obec	k1gFnSc1
si	se	k3xPyFc3
určují	určovat	k5eAaImIp3nP
podmínky	podmínka	k1gFnPc4
<g/>
,	,	kIx,
za	za	k7c2
kterých	který	k3yRgInPc2,k3yIgInPc2,k3yQgInPc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
víno	víno	k1gNnSc1
vyráběno	vyráběn	k2eAgNnSc1d1
(	(	kIx(
<g/>
omezení	omezení	k1gNnSc1
nebo	nebo	k8xC
vyloučení	vyloučení	k1gNnSc1
některých	některý	k3yIgInPc2
zásahů	zásah	k1gInPc2
při	při	k7c6
výrobě	výroba	k1gFnSc6
<g/>
,	,	kIx,
dohled	dohled	k1gInSc1
náboženského	náboženský	k2eAgMnSc2d1
zmocněnce	zmocněnec	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odalkoholizované	odalkoholizovaný	k2eAgNnSc1d1
víno	víno	k1gNnSc1
a	a	k8xC
nízkoalkoholické	nízkoalkoholický	k2eAgNnSc1d1
víno	víno	k1gNnSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odalkoholizované	odalkoholizovaný	k2eAgNnSc1d1
víno	víno	k1gNnSc1
<g/>
,	,	kIx,
odalkoholizované	odalkoholizovaný	k2eAgNnSc1d1
šumivé	šumivý	k2eAgNnSc1d1
víno	víno	k1gNnSc1
nebo	nebo	k8xC
odalkoholizované	odalkoholizovaný	k2eAgNnSc1d1
perlivé	perlivý	k2eAgNnSc1d1
víno	víno	k1gNnSc1
je	být	k5eAaImIp3nS
nápoj	nápoj	k1gInSc4
z	z	k7c2
vína	víno	k1gNnSc2
<g/>
,	,	kIx,
u	u	k7c2
něhož	jenž	k3xRgNnSc2
obsah	obsah	k1gInSc1
alkoholu	alkohol	k1gInSc2
byl	být	k5eAaImAgInS
destilací	destilace	k1gFnSc7
nebo	nebo	k8xC
jinou	jiný	k2eAgFnSc7d1
technologií	technologie	k1gFnSc7
snížen	snížit	k5eAaPmNgMnS
na	na	k7c4
0,5	0,5	k4
%	%	kIx~
objemových	objemový	k2eAgInPc2d1
nebo	nebo	k8xC
méně	málo	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
výrobě	výroba	k1gFnSc6
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
přidat	přidat	k5eAaPmF
hroznový	hroznový	k2eAgInSc4d1
mošt	mošt	k1gInSc4
<g/>
,	,	kIx,
zahuštěný	zahuštěný	k2eAgInSc4d1
hroznový	hroznový	k2eAgInSc4d1
mošt	mošt	k1gInSc4
nebo	nebo	k8xC
cukr	cukr	k1gInSc4
v	v	k7c6
takovém	takový	k3xDgNnSc6
množství	množství	k1gNnSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
obsah	obsah	k1gInSc1
zbytkového	zbytkový	k2eAgInSc2d1
cukru	cukr	k1gInSc2
v	v	k7c6
konečném	konečný	k2eAgInSc6d1
produktu	produkt	k1gInSc6
byl	být	k5eAaImAgInS
nejvýše	vysoce	k6eAd3,k6eAd1
75	#num#	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
l.	l.	k?
Dále	daleko	k6eAd2
je	být	k5eAaImIp3nS
dovoleno	dovolen	k2eAgNnSc1d1
přidávat	přidávat	k5eAaImF
oxid	oxid	k1gInSc4
uhličitý	uhličitý	k2eAgInSc4d1
a	a	k8xC
přírodní	přírodní	k2eAgFnPc4d1
aromatické	aromatický	k2eAgFnPc4d1
látky	látka	k1gFnPc4
a	a	k8xC
přírodně	přírodně	k6eAd1
identické	identický	k2eAgFnPc4d1
aromatické	aromatický	k2eAgFnPc4d1
látky	látka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Odrůdy	odrůda	k1gFnPc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Odrůdy	odrůda	k1gFnSc2
vinné	vinný	k2eAgFnSc2d1
révy	réva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mapa	mapa	k1gFnSc1
konzumace	konzumace	k1gFnSc2
vína	víno	k1gNnSc2
ve	v	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čím	co	k3yQnSc7,k3yInSc7,k3yRnSc7
tmavší	tmavý	k2eAgFnSc1d2
barva	barva	k1gFnSc1
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
větší	veliký	k2eAgFnSc1d2
spotřeba	spotřeba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Réva	réva	k1gFnSc1
vinná	vinný	k2eAgFnSc1d1
(	(	kIx(
<g/>
Vitis	Vitis	k1gFnSc1
vinifera	vinifera	k1gFnSc1
<g/>
)	)	kIx)
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
značné	značný	k2eAgNnSc1d1
množství	množství	k1gNnSc1
odrůd	odrůda	k1gFnPc2
a	a	k8xC
kultivarů	kultivar	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
vznikly	vzniknout	k5eAaPmAgInP
jak	jak	k6eAd1
náhodným	náhodný	k2eAgNnSc7d1
křížením	křížení	k1gNnSc7
<g/>
,	,	kIx,
tak	tak	k9
cíleným	cílený	k2eAgNnSc7d1
pěstěním	pěstění	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc1
odrůd	odrůda	k1gFnPc2
na	na	k7c6
světě	svět	k1gInSc6
se	se	k3xPyFc4
odhaduje	odhadovat	k5eAaImIp3nS
na	na	k7c4
několik	několik	k4yIc4
tisíc	tisíc	k4xCgInSc4
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jen	jen	k6eAd1
malá	malý	k2eAgFnSc1d1
část	část	k1gFnSc1
z	z	k7c2
nich	on	k3xPp3gInPc2
se	se	k3xPyFc4
však	však	k9
používá	používat	k5eAaImIp3nS
k	k	k7c3
výrobě	výroba	k1gFnSc3
vína	víno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
nejběžnější	běžný	k2eAgFnPc4d3
odrůdy	odrůda	k1gFnPc4
pěstované	pěstovaný	k2eAgFnPc4d1
na	na	k7c6
území	území	k1gNnSc6
Česka	Česko	k1gNnSc2
patří	patřit	k5eAaImIp3nS
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bílé	bílý	k2eAgFnPc1d1
odrůdy	odrůda	k1gFnPc1
</s>
<s>
Aurelius	Aurelius	k1gMnSc1
</s>
<s>
Floriánka	Floriánek	k1gMnSc4
</s>
<s>
Chardonnay	Chardonnaa	k1gFnPc1
</s>
<s>
Irsai	Irsai	k6eAd1
Oliver	Oliver	k1gInSc1
</s>
<s>
Muškát	muškát	k1gInSc1
moravský	moravský	k2eAgInSc1d1
</s>
<s>
Müller	Müller	k1gMnSc1
Thurgau	Thurgaus	k1gInSc2
</s>
<s>
Neuburské	Neuburský	k2eAgNnSc1d1
</s>
<s>
Pálava	Pálava	k1gFnSc1
</s>
<s>
Rulandské	rulandský	k2eAgFnPc1d1
bílé	bílý	k2eAgFnPc1d1
</s>
<s>
Rulandské	rulandský	k2eAgInPc1d1
šedé	šedý	k2eAgInPc1d1
</s>
<s>
Ryzlink	ryzlink	k1gInSc1
rýnský	rýnský	k2eAgInSc1d1
</s>
<s>
Ryzlink	ryzlink	k1gInSc1
vlašský	vlašský	k2eAgInSc1d1
</s>
<s>
Sauvignon	Sauvignon	k1gMnSc1
</s>
<s>
Sylvánské	sylvánské	k1gNnSc1
zelené	zelený	k2eAgInPc1d1
</s>
<s>
Tramín	tramín	k1gInSc1
bílý	bílý	k2eAgInSc1d1
</s>
<s>
Tramín	tramín	k1gInSc1
červený	červený	k2eAgInSc1d1
</s>
<s>
Veltlínské	veltlínský	k2eAgFnPc1d1
červené	červený	k2eAgFnPc1d1
rané	raný	k2eAgFnPc1d1
</s>
<s>
Veltlínské	veltlínský	k2eAgInPc1d1
zelené	zelený	k2eAgInPc1d1
</s>
<s>
Modré	modrý	k2eAgFnPc1d1
odrůdy	odrůda	k1gFnPc1
</s>
<s>
Alibernet	Alibernet	k1gMnSc1
</s>
<s>
André	André	k1gMnSc1
</s>
<s>
Cabernet	Cabernet	k1gInSc1
Moravia	Moravium	k1gNnSc2
</s>
<s>
Cabernet	Cabernet	k1gMnSc1
Sauvignon	Sauvignon	k1gMnSc1
</s>
<s>
Dornfelder	Dornfelder	k1gMnSc1
</s>
<s>
Frankovka	frankovka	k1gFnSc1
</s>
<s>
Merlot	Merlot	k1gMnSc1
</s>
<s>
Modrý	modrý	k2eAgInSc1d1
Portugal	portugal	k1gInSc1
</s>
<s>
Rulandské	rulandský	k2eAgNnSc1d1
modré	modré	k1gNnSc1
</s>
<s>
Svatovavřinecké	svatovavřinecké	k1gNnSc1
</s>
<s>
Zweigeltrebe	Zweigeltrebat	k5eAaPmIp3nS
</s>
<s>
Ovocná	ovocný	k2eAgFnSc1d1
vína	vína	k1gFnSc1
</s>
<s>
Víno	víno	k1gNnSc1
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
rovněž	rovněž	k9
vyrábět	vyrábět	k5eAaImF
z	z	k7c2
dalších	další	k2eAgInPc2d1
druhů	druh	k1gInPc2
ovoce	ovoce	k1gNnSc2
<g/>
,	,	kIx,
případně	případně	k6eAd1
bylin	bylina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
těchto	tento	k3xDgInPc6
případech	případ	k1gInPc6
je	být	k5eAaImIp3nS
název	název	k1gInSc1
doplněn	doplnit	k5eAaPmNgInS
přívlastkem	přívlastek	k1gInSc7
<g/>
,	,	kIx,
např.	např.	kA
ovocné	ovocný	k2eAgNnSc1d1
víno	víno	k1gNnSc1
<g/>
;	;	kIx,
známé	známý	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
jablečné	jablečný	k2eAgNnSc1d1
víno	víno	k1gNnSc1
(	(	kIx(
<g/>
cider	cider	k1gInSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
hovorově	hovorově	k6eAd1
jablečňák	jablečňák	k1gMnSc1
<g/>
,	,	kIx,
jabčák	jabčák	k1gInSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
nebo	nebo	k8xC
švestkové	švestkový	k2eAgNnSc1d1
víno	víno	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
je	být	k5eAaImIp3nS
k	k	k7c3
mání	mání	k1gFnSc3
v	v	k7c6
asijských	asijský	k2eAgFnPc6d1
restauracích	restaurace	k1gFnPc6
a	a	k8xC
obchodech	obchod	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovo	slovo	k1gNnSc1
víno	víno	k1gNnSc1
bez	bez	k7c2
dalšího	další	k2eAgNnSc2d1
vždy	vždy	k6eAd1
primárně	primárně	k6eAd1
odkazuje	odkazovat	k5eAaImIp3nS
na	na	k7c4
víno	víno	k1gNnSc4
z	z	k7c2
hroznů	hrozen	k1gInPc2
révy	réva	k1gFnSc2
vinné	vinný	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Technical	Technical	k1gFnSc1
standards	standards	k1gInSc1
and	and	k?
documents	documents	k1gInSc1
<g/>
.	.	kIx.
oiv	oiv	k?
<g/>
.	.	kIx.
<g/>
int	int	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
fr	fr	k0
<g/>
,	,	kIx,
en	en	k?
<g/>
,	,	kIx,
de	de	k?
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
{{{	{{{	k?
<g/>
typ	typ	k1gInSc1
<g/>
}}}	}}}	k?
č.	č.	k?
32013R1308	32013R1308	k4
ze	z	k7c2
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Nařízení	nařízení	k1gNnPc1
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
a	a	k8xC
Rady	rada	k1gFnSc2
(	(	kIx(
<g/>
EU	EU	kA
<g/>
)	)	kIx)
č.	č.	k?
1308	#num#	k4
<g/>
/	/	kIx~
<g/>
2013	#num#	k4
ze	z	k7c2
dne	den	k1gInSc2
17	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2013	#num#	k4
,	,	kIx,
kterým	který	k3yIgNnSc7,k3yRgNnSc7,k3yQgNnSc7
se	se	k3xPyFc4
stanoví	stanovit	k5eAaPmIp3nS
společná	společný	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
trhů	trh	k1gInPc2
se	s	k7c7
zemědělskými	zemědělský	k2eAgInPc7d1
produkty	produkt	k1gInPc7
a	a	k8xC
zrušují	zrušovat	k5eAaImIp3nP
nařízení	nařízení	k1gNnPc4
Rady	rada	k1gFnSc2
(	(	kIx(
<g/>
EHS	EHS	kA
<g/>
)	)	kIx)
č.	č.	k?
922	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
EHS	EHS	kA
<g/>
)	)	kIx)
č.	č.	k?
234	#num#	k4
<g/>
/	/	kIx~
<g/>
79	#num#	k4
<g/>
,	,	kIx,
(	(	kIx(
<g/>
ES	ES	kA
<g/>
)	)	kIx)
č.	č.	k?
1037	#num#	k4
<g/>
/	/	kIx~
<g/>
2001	#num#	k4
a	a	k8xC
(	(	kIx(
<g/>
ES	ES	kA
<g/>
)	)	kIx)
č.	č.	k?
1234	#num#	k4
<g/>
/	/	kIx~
<g/>
2007	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Zákon	zákon	k1gInSc1
č.	č.	k?
321	#num#	k4
<g/>
/	/	kIx~
<g/>
2004	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
§	§	k?
201	#num#	k4
2	#num#	k4
Vyhláška	vyhláška	k1gFnSc1
č.	č.	k?
88	#num#	k4
<g/>
/	/	kIx~
<g/>
2017	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Prakticky	prakticky	k6eAd1
to	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
všechna	všechen	k3xTgNnPc1
růžová	růžový	k2eAgNnPc1d1
vína	víno	k1gNnPc1
od	od	k7c2
stupně	stupeň	k1gInSc2
„	„	k?
<g/>
zemské	zemský	k2eAgNnSc1d1
víno	víno	k1gNnSc1
<g/>
“	“	k?
výše	vysoce	k6eAd2
je	být	k5eAaImIp3nS
zakázáno	zakázat	k5eAaPmNgNnS
vyrábět	vyrábět	k5eAaImF
smícháním	smíchání	k1gNnSc7
červeného	červené	k1gNnSc2
a	a	k8xC
bílého	bílý	k2eAgNnSc2d1
vína	víno	k1gNnSc2
<g/>
.	.	kIx.
↑	↑	k?
http://www.znalecvin.cz/oranzove-vino/	http://www.znalecvin.cz/oranzove-vino/	k?
<g/>
↑	↑	k?
http://www.znalecvin.cz/kachetinske-technologie/	http://www.znalecvin.cz/kachetinske-technologie/	k?
<g/>
↑	↑	k?
JAVŮREK	Javůrek	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výjimečná	výjimečný	k2eAgFnSc1d1
vína	vína	k1gFnSc1
Česka	Česko	k1gNnSc2
a	a	k8xC
Rakouska	Rakousko	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Enfant	Enfant	k1gInSc1
terrible	terrible	k6eAd1
Origin	Origin	k2eAgMnSc1d1
<g/>
,	,	kIx,
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
http://eagri.cz/public/web/file/644471/SVZ___Vino_2019.pdf,	http://eagri.cz/public/web/file/644471/SVZ___Vino_2019.pdf,	k4
p.	p.	k?
41	#num#	k4
ad	ad	k7c4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
NAŘÍZENÍ	nařízení	k1gNnSc4
RADY	rada	k1gFnSc2
(	(	kIx(
<g/>
ES	ES	kA
<g/>
)	)	kIx)
č.	č.	k?
510	#num#	k4
<g/>
/	/	kIx~
<g/>
2006	#num#	k4
ze	z	k7c2
dne	den	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2006	#num#	k4
o	o	k7c6
ochraně	ochrana	k1gFnSc6
zeměpisných	zeměpisný	k2eAgNnPc2d1
označení	označení	k1gNnPc2
a	a	k8xC
označení	označení	k1gNnSc1
původu	původ	k1gInSc2
zemědělských	zemědělský	k2eAgInPc2d1
produktů	produkt	k1gInPc2
a	a	k8xC
potravin	potravina	k1gFnPc2
<g/>
↑	↑	k?
JAVŮREK	Javůrek	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tajemné	tajemný	k2eAgInPc1d1
původy	původ	k1gInPc1
vína	víno	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Enfant	Enfant	k1gInSc1
terrible	terrible	k6eAd1
Origin	Origin	k2eAgMnSc1d1
<g/>
,	,	kIx,
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
NAŘÍZENÍ	nařízení	k1gNnSc4
RADY	rada	k1gFnSc2
(	(	kIx(
<g/>
ES	ES	kA
<g/>
)	)	kIx)
č.	č.	k?
834	#num#	k4
<g/>
/	/	kIx~
<g/>
2007	#num#	k4
ze	z	k7c2
dne	den	k1gInSc2
28	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2007	#num#	k4
o	o	k7c6
ekologické	ekologický	k2eAgFnSc6d1
produkci	produkce	k1gFnSc6
a	a	k8xC
označování	označování	k1gNnSc6
ekologických	ekologický	k2eAgInPc2d1
produktů	produkt	k1gInPc2
a	a	k8xC
o	o	k7c4
zrušení	zrušení	k1gNnSc4
nařízení	nařízení	k1gNnSc2
(	(	kIx(
<g/>
EHS	EHS	kA
<g/>
)	)	kIx)
č.	č.	k?
2092	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
<g/>
↑	↑	k?
PROVÁDĚCÍ	prováděcí	k2eAgFnSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
NAŘÍZENÍ	nařízení	k1gNnSc1
KOMISE	komise	k1gFnSc1
(	(	kIx(
<g/>
EU	EU	kA
<g/>
)	)	kIx)
č.	č.	k?
203	#num#	k4
<g/>
/	/	kIx~
<g/>
2012	#num#	k4
ze	z	k7c2
dne	den	k1gInSc2
8	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2012	#num#	k4
<g/>
,	,	kIx,
kterým	který	k3yIgNnSc7,k3yQgNnSc7,k3yRgNnSc7
se	se	k3xPyFc4
mění	měnit	k5eAaImIp3nS
nařízení	nařízení	k1gNnSc1
(	(	kIx(
<g/>
ES	ES	kA
<g/>
)	)	kIx)
č.	č.	k?
889	#num#	k4
<g/>
/	/	kIx~
<g/>
2008	#num#	k4
<g/>
,	,	kIx,
kterým	který	k3yQgNnSc7,k3yRgNnSc7,k3yIgNnSc7
se	se	k3xPyFc4
stanoví	stanovit	k5eAaPmIp3nS
prováděcí	prováděcí	k2eAgNnPc4d1
pravidla	pravidlo	k1gNnPc4
k	k	k7c3
nařízení	nařízení	k1gNnSc3
Rady	rada	k1gFnSc2
(	(	kIx(
<g/>
ES	ES	kA
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
č.	č.	k?
834	#num#	k4
<g/>
/	/	kIx~
<g/>
2007	#num#	k4
<g/>
,	,	kIx,
pokud	pokud	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
prováděcí	prováděcí	k2eAgNnPc4d1
pravidla	pravidlo	k1gNnPc4
pro	pro	k7c4
ekologickou	ekologický	k2eAgFnSc4d1
produkci	produkce	k1gFnSc4
vína	víno	k1gNnSc2
<g/>
↑	↑	k?
Co	co	k9
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
integrovaná	integrovaný	k2eAgFnSc1d1
produkce	produkce	k1gFnSc1
<g/>
,	,	kIx,
Svaz	svaz	k1gInSc1
integrované	integrovaný	k2eAgFnSc2d1
a	a	k8xC
ekologické	ekologický	k2eAgFnSc2d1
produkce	produkce	k1gFnSc2
hroznů	hrozen	k1gInPc2
a	a	k8xC
vína	víno	k1gNnSc2
o.s.	o.s.	k?
<g/>
.	.	kIx.
www.ekovin.cz	www.ekovin.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Nařízení	nařízení	k1gNnSc4
vlády	vláda	k1gFnSc2
č.	č.	k?
79	#num#	k4
<g/>
/	/	kIx~
<g/>
2007	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
podmínkách	podmínka	k1gFnPc6
provádění	provádění	k1gNnSc2
agroenvironmentálních	agroenvironmentální	k2eAgNnPc2d1
opatření	opatření	k1gNnPc2
<g/>
↑	↑	k?
AUTENTISTA	AUTENTISTA	kA
MORAVIA	MORAVIA	kA
MAGNA	MAGNA	kA
|	|	kIx~
vinařský	vinařský	k2eAgInSc4d1
spolek	spolek	k1gInSc4
<g/>
.	.	kIx.
autentiste	autentist	k1gMnSc5
<g/>
.	.	kIx.
<g/>
eu	eu	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
↑	↑	k?
AUTENTISTA	AUTENTISTA	kA
MORAVIA	MORAVIA	kA
MAGNA	MAGNA	kA
|	|	kIx~
vinařský	vinařský	k2eAgInSc4d1
spolek	spolek	k1gInSc4
<g/>
.	.	kIx.
autentiste	autentist	k1gMnSc5
<g/>
.	.	kIx.
<g/>
eu	eu	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
↑	↑	k?
Přírodní	přírodní	k2eAgNnSc4d1
víno	víno	k1gNnSc4
|	|	kIx~
Vína	vína	k1gFnSc1
z	z	k7c2
Moravy	Morava	k1gFnSc2
a	a	k8xC
vína	víno	k1gNnSc2
z	z	k7c2
Čech	Čechy	k1gFnPc2
<g/>
.	.	kIx.
www.wineofczechrepublic.cz	www.wineofczechrepublic.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
LACOMBE	LACOMBE	kA
<g/>
,	,	kIx,
Thierry	Thierr	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Contribution	Contribution	k1gInSc1
à	à	k?
l	l	kA
<g/>
’	’	k?
<g/>
étude	étude	k6eAd1
de	de	k?
l	l	kA
<g/>
’	’	k?
<g/>
histoire	histoir	k1gInSc5
évolutive	évolutiv	k1gInSc5
de	de	k?
la	la	k1gNnSc6
vigne	vignout	k5eAaPmIp3nS,k5eAaImIp3nS
cultivée	cultivée	k1gInSc1
(	(	kIx(
<g/>
Vitis	Vitis	k1gInSc1
vinifera	vinifer	k1gMnSc2
L.	L.	kA
<g/>
)	)	kIx)
par	para	k1gFnPc2
l	l	kA
<g/>
’	’	k?
<g/>
analyse	analysa	k1gFnSc6
de	de	k?
la	la	k1gNnSc2
diversité	diversitý	k2eAgFnSc2d1
génétique	génétiqu	k1gFnSc2
neutre	neutr	k1gInSc5
et	et	k?
de	de	k?
gè	gè	k1gInSc1
d	d	k?
<g/>
’	’	k?
<g/>
intérê	intérê	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Seznam	seznam	k1gInSc1
odrůd	odrůda	k1gFnPc2
zapsaných	zapsaný	k2eAgFnPc2d1
ve	v	k7c6
Státní	státní	k2eAgFnSc6d1
odrůdové	odrůdový	k2eAgFnSc6d1
knize	kniha	k1gFnSc6
(	(	kIx(
<g/>
ÚKZÚZ	ÚKZÚZ	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
eagri	eagri	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Burčák	burčák	k1gInSc1
</s>
<s>
Dezertní	dezertní	k2eAgNnSc1d1
víno	víno	k1gNnSc1
</s>
<s>
Enologie	enologie	k1gFnSc1
</s>
<s>
Francouzský	francouzský	k2eAgInSc1d1
paradox	paradox	k1gInSc1
</s>
<s>
Koštýř	koštýř	k1gMnSc1
</s>
<s>
Servírování	servírování	k1gNnSc1
vín	víno	k1gNnPc2
</s>
<s>
Sommelier	Sommelier	k1gMnSc1
</s>
<s>
Vínovice	vínovice	k1gFnSc1
(	(	kIx(
<g/>
SSJČ	SSJČ	kA
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
Koňak	koňak	k1gInSc1
<g/>
,	,	kIx,
Brandy	brandy	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
na	na	k7c6
Moravě	Morava	k1gFnSc6
<g/>
)	)	kIx)
vinná	vinný	k2eAgFnSc1d1
pálenka	pálenka	k1gFnSc1
<g/>
:	:	kIx,
Matolinovice	matolinovice	k1gFnSc1
(	(	kIx(
<g/>
Terkelice	terkelice	k1gFnSc1
<g/>
,	,	kIx,
Grappa	Grappa	k1gFnSc1
aj.	aj.	kA
názvy	název	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
Vliv	vliv	k1gInSc1
vína	víno	k1gNnSc2
na	na	k7c4
zdraví	zdraví	k1gNnSc4
</s>
<s>
Výroba	výroba	k1gFnSc1
vína	víno	k1gNnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
víno	víno	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Téma	téma	k1gNnSc1
víno	víno	k1gNnSc4
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
víno	víno	k1gNnSc4
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Víno	víno	k1gNnSc4
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
SLÁMA	Sláma	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
počátkům	počátek	k1gInPc3
vinařství	vinařství	k1gNnSc2
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kulturní	kulturní	k2eAgInSc1d1
studia	studio	k1gNnPc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
3	#num#	k4
<g/>
–	–	k?
<g/>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
2336	#num#	k4
<g/>
-	-	kIx~
<g/>
2766	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.716	10.716	k4
<g/>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
KS	ks	kA
<g/>
.2020	.2020	k4
<g/>
.140101	.140101	k4
<g/>
.	.	kIx.
</s>
<s>
Encyklopedie	encyklopedie	k1gFnSc1
znalce	znalec	k1gMnSc2
vín	víno	k1gNnPc2
</s>
<s>
Databáze	databáze	k1gFnSc1
českých	český	k2eAgNnPc2d1
<g/>
,	,	kIx,
moravských	moravský	k2eAgNnPc2d1
a	a	k8xC
slovenských	slovenský	k2eAgNnPc2d1
vín	víno	k1gNnPc2
</s>
<s>
Historie	historie	k1gFnSc1
vína	víno	k1gNnSc2
v	v	k7c6
Itálii	Itálie	k1gFnSc6
–	–	k?
zdroj	zdroj	k1gInSc1
ČTK	ČTK	kA
<g/>
,	,	kIx,
článek	článek	k1gInSc4
Prima	prima	k2eAgInSc4d1
ZOOM	ZOOM	kA
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4065133-2	4065133-2	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
8693	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85146975	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85146975	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Gastronomie	gastronomie	k1gFnSc1
</s>
