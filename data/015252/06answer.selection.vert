<s>
Pojmy	pojem	k1gInPc1
levice	levice	k1gFnSc2
a	a	k8xC
pravice	pravice	k1gFnSc2
vznikly	vzniknout	k5eAaPmAgFnP
na	na	k7c6
konci	konec	k1gInSc6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
v	v	k7c6
období	období	k1gNnSc6
Velké	velký	k2eAgFnSc2d1
francouzské	francouzský	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
v	v	k7c6
Generálních	generální	k2eAgInPc6d1
stavech	stav	k1gInPc6
zasedali	zasedat	k5eAaImAgMnP
přívrženci	přívrženec	k1gMnPc1
nového	nový	k2eAgInSc2d1
řádu	řád	k1gInSc2
<g/>
,	,	kIx,
třetí	třetí	k4xOgInSc4
stav	stav	k1gInSc4
a	a	k8xC
radikálové	radikál	k1gMnPc1
(	(	kIx(
<g/>
Montagnardi	Montagnard	k1gMnPc1
<g/>
)	)	kIx)
nalevo	nalevo	k6eAd1
a	a	k8xC
zastánci	zastánce	k1gMnPc1
starého	starý	k2eAgInSc2d1
řádu	řád	k1gInSc2
<g/>
,	,	kIx,
reakční	reakční	k2eAgMnPc1d1
a	a	k8xC
monarchističtí	monarchistický	k2eAgMnPc1d1
aristokraté	aristokrat	k1gMnPc1
(	(	kIx(
<g/>
Feuillanti	Feuillant	k1gMnPc1
<g/>
)	)	kIx)
napravo	napravo	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
zásadě	zásada	k1gFnSc6
se	se	k3xPyFc4
levicové	levicový	k2eAgNnSc1d1
uskupení	uskupení	k1gNnSc1
řídí	řídit	k5eAaImIp3nS
heslem	heslo	k1gNnSc7
francouzské	francouzský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
Liberté	Libertý	k2eAgFnPc1d1
<g/>
,	,	kIx,
égalité	égalitý	k2eAgFnPc1d1
<g/>
,	,	kIx,
fraternité	fraternitý	k2eAgFnPc1d1
(	(	kIx(
<g/>
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
rovnost	rovnost	k1gFnSc1
<g/>
,	,	kIx,
bratrství	bratrství	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>