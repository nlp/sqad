<s>
Levice	levice	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
části	část	k1gFnSc6
politického	politický	k2eAgNnSc2d1
spektra	spektrum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Levice	levice	k1gFnSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Levice	levice	k1gFnSc1
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
vágní	vágní	k2eAgNnSc1d1
označení	označení	k1gNnSc1
používané	používaný	k2eAgNnSc1d1
pro	pro	k7c4
popis	popis	k1gInSc4
celé	celý	k2eAgFnSc2d1
řady	řada	k1gFnSc2
politických	politický	k2eAgFnPc2d1
ideologií	ideologie	k1gFnPc2
a	a	k8xC
pozic	pozice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obvykle	obvykle	k6eAd1
je	být	k5eAaImIp3nS
pojem	pojem	k1gInSc1
levice	levice	k1gFnSc2
používán	používat	k5eAaImNgInS
pro	pro	k7c4
popis	popis	k1gInSc4
skupin	skupina	k1gFnPc2
hájících	hájící	k2eAgFnPc2d1
zájmy	zájem	k1gInPc4
nižších	nízký	k2eAgFnPc2d2
a	a	k8xC
znevýhodněných	znevýhodněný	k2eAgFnPc2d1
vrstev	vrstva	k1gFnPc2
<g/>
,	,	kIx,
podporujících	podporující	k2eAgMnPc2d1
sociální	sociální	k2eAgFnSc4d1
rovnost	rovnost	k1gFnSc4
a	a	k8xC
spravedlnost	spravedlnost	k1gFnSc4
a	a	k8xC
změnu	změna	k1gFnSc4
tradičního	tradiční	k2eAgNnSc2d1
hierarchického	hierarchický	k2eAgNnSc2d1
uspořádání	uspořádání	k1gNnSc2
s	s	k7c7
cílem	cíl	k1gInSc7
rovnostářského	rovnostářský	k2eAgNnSc2d1
přerozdělování	přerozdělování	k1gNnSc2
bohatství	bohatství	k1gNnSc2
a	a	k8xC
privilegií	privilegium	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsah	obsah	k1gInSc1
pojmů	pojem	k1gInPc2
levice	levice	k1gFnSc2
a	a	k8xC
pravice	pravice	k1gFnSc2
se	se	k3xPyFc4
v	v	k7c6
různých	různý	k2eAgFnPc6d1
společnostech	společnost	k1gFnPc6
a	a	k8xC
v	v	k7c6
různých	různý	k2eAgFnPc6d1
dobách	doba	k1gFnPc6
lišil	lišit	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Pojmy	pojem	k1gInPc1
levice	levice	k1gFnSc2
a	a	k8xC
pravice	pravice	k1gFnSc2
vznikly	vzniknout	k5eAaPmAgFnP
na	na	k7c6
konci	konec	k1gInSc6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
v	v	k7c6
období	období	k1gNnSc6
Velké	velký	k2eAgFnSc2d1
francouzské	francouzský	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
v	v	k7c6
Generálních	generální	k2eAgInPc6d1
stavech	stav	k1gInPc6
zasedali	zasedat	k5eAaImAgMnP
přívrženci	přívrženec	k1gMnPc1
nového	nový	k2eAgInSc2d1
řádu	řád	k1gInSc2
<g/>
,	,	kIx,
třetí	třetí	k4xOgInSc4
stav	stav	k1gInSc4
a	a	k8xC
radikálové	radikál	k1gMnPc1
(	(	kIx(
<g/>
Montagnardi	Montagnard	k1gMnPc1
<g/>
)	)	kIx)
nalevo	nalevo	k6eAd1
a	a	k8xC
zastánci	zastánce	k1gMnPc1
starého	starý	k2eAgInSc2d1
řádu	řád	k1gInSc2
<g/>
,	,	kIx,
reakční	reakční	k2eAgMnPc1d1
a	a	k8xC
monarchističtí	monarchistický	k2eAgMnPc1d1
aristokraté	aristokrat	k1gMnPc1
(	(	kIx(
<g/>
Feuillanti	Feuillant	k1gMnPc1
<g/>
)	)	kIx)
napravo	napravo	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
zásadě	zásada	k1gFnSc6
se	se	k3xPyFc4
levicové	levicový	k2eAgNnSc1d1
uskupení	uskupení	k1gNnSc1
řídí	řídit	k5eAaImIp3nS
heslem	heslo	k1gNnSc7
francouzské	francouzský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
Liberté	Libertý	k2eAgFnPc1d1
<g/>
,	,	kIx,
égalité	égalitý	k2eAgFnPc1d1
<g/>
,	,	kIx,
fraternité	fraternitý	k2eAgFnPc1d1
(	(	kIx(
<g/>
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
rovnost	rovnost	k1gFnSc1
<g/>
,	,	kIx,
bratrství	bratrství	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zřetelně	zřetelně	k6eAd1
se	se	k3xPyFc4
politická	politický	k2eAgFnSc1d1
levice	levice	k1gFnSc1
vyvinula	vyvinout	k5eAaPmAgFnS
během	během	k7c2
Červnového	červnový	k2eAgNnSc2d1
povstání	povstání	k1gNnSc2
dělnictva	dělnictvo	k1gNnSc2
roku	rok	k1gInSc2
1848	#num#	k4
ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Organizátoři	organizátor	k1gMnPc1
První	první	k4xOgFnPc4
internacionály	internacionála	k1gFnPc4
se	se	k3xPyFc4
považovali	považovat	k5eAaImAgMnP
za	za	k7c4
nástupce	nástupce	k1gMnSc4
levice	levice	k1gFnSc2
z	z	k7c2
Velké	velký	k2eAgFnSc2d1
francouzské	francouzský	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Levicové	levicový	k2eAgFnPc1d1
ideologie	ideologie	k1gFnPc1
a	a	k8xC
strany	strana	k1gFnPc1
</s>
<s>
Levicové	levicový	k2eAgNnSc1d1
spektrum	spektrum	k1gNnSc1
se	se	k3xPyFc4
rozpíná	rozpínat	k5eAaImIp3nS
od	od	k7c2
středolevice	středolevice	k1gFnSc2
po	po	k7c6
krajní	krajní	k2eAgFnSc6d1
levici	levice	k1gFnSc6
(	(	kIx(
<g/>
ultralevici	ultralevice	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Středolevice	Středolevice	k1gFnSc1
souhrnně	souhrnně	k6eAd1
označuje	označovat	k5eAaImIp3nS
ty	ten	k3xDgFnPc4
pozice	pozice	k1gFnPc4
v	v	k7c6
rámci	rámec	k1gInSc6
levicového	levicový	k2eAgNnSc2d1
spektra	spektrum	k1gNnSc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc7
cílem	cíl	k1gInSc7
není	být	k5eNaImIp3nS
překonání	překonání	k1gNnSc1
kapitalismu	kapitalismus	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Termín	termín	k1gInSc1
krajní	krajní	k2eAgFnSc2d1
levice	levice	k1gFnSc2
naproti	naproti	k7c3
tomu	ten	k3xDgNnSc3
označuje	označovat	k5eAaImIp3nS
směry	směr	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
jsou	být	k5eAaImIp3nP
více	hodně	k6eAd2
radikální	radikální	k2eAgInSc4d1
a	a	k8xC
kapitalistický	kapitalistický	k2eAgInSc4d1
systém	systém	k1gInSc4
zásadně	zásadně	k6eAd1
odmítají	odmítat	k5eAaImIp3nP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Levice	levice	k1gFnSc1
a	a	k8xC
krajní	krajní	k2eAgFnSc1d1
levice	levice	k1gFnSc1
historicky	historicky	k6eAd1
zahrnuje	zahrnovat	k5eAaImIp3nS
řadu	řada	k1gFnSc4
revolučních	revoluční	k2eAgNnPc2d1
hnutí	hnutí	k1gNnPc2
<g/>
,	,	kIx,
zejména	zejména	k9
socialismus	socialismus	k1gInSc1
<g/>
,	,	kIx,
komunismus	komunismus	k1gInSc1
a	a	k8xC
anarchismus	anarchismus	k1gInSc1
(	(	kIx(
<g/>
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
anarchokapitalismu	anarchokapitalismus	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
středolevici	středolevice	k1gFnSc4
je	být	k5eAaImIp3nS
považována	považován	k2eAgFnSc1d1
sociální	sociální	k2eAgFnSc1d1
demokracie	demokracie	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
usiluje	usilovat	k5eAaImIp3nS
o	o	k7c4
dosažení	dosažení	k1gNnSc4
socialistických	socialistický	k2eAgInPc2d1
ideálů	ideál	k1gInPc2
v	v	k7c6
rámci	rámec	k1gInSc6
kapitalistického	kapitalistický	k2eAgInSc2d1
systému	systém	k1gInSc2
formou	forma	k1gFnSc7
regulované	regulovaný	k2eAgFnSc2d1
tržní	tržní	k2eAgFnSc2d1
ekonomiky	ekonomika	k1gFnSc2
v	v	k7c6
kombinaci	kombinace	k1gFnSc6
se	s	k7c7
silným	silný	k2eAgInSc7d1
sociálním	sociální	k2eAgInSc7d1
státem	stát	k1gInSc7
(	(	kIx(
<g/>
státem	stát	k1gInSc7
blahobytu	blahobyt	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
a	a	k8xC
v	v	k7c6
Evropě	Evropa	k1gFnSc6
(	(	kIx(
<g/>
zejména	zejména	k9
západní	západní	k2eAgFnSc1d1
<g/>
)	)	kIx)
se	se	k3xPyFc4
také	také	k9
od	od	k7c2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
prosazuje	prosazovat	k5eAaImIp3nS
tzv.	tzv.	kA
nová	nový	k2eAgFnSc1d1
levice	levice	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
prosazování	prosazování	k1gNnSc2
třídní	třídní	k2eAgFnSc2d1
rovnosti	rovnost	k1gFnSc2
<g/>
,	,	kIx,
jakožto	jakožto	k8xS
domény	doména	k1gFnPc1
„	„	k?
<g/>
staré	stará	k1gFnSc2
<g/>
/	/	kIx~
<g/>
tradiční	tradiční	k2eAgFnSc2d1
levice	levice	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
soustředí	soustředit	k5eAaPmIp3nS
zejména	zejména	k9
na	na	k7c4
rovnost	rovnost	k1gFnSc4
sociální	sociální	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
středolevici	středolevice	k1gFnSc4
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
řadí	řadit	k5eAaImIp3nS
i	i	k9
progresivismus	progresivismus	k1gInSc1
<g/>
,	,	kIx,
zelená	zelený	k2eAgFnSc1d1
politika	politika	k1gFnSc1
a	a	k8xC
někdy	někdy	k6eAd1
i	i	k9
sociální	sociální	k2eAgInSc4d1
liberalismus	liberalismus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
se	se	k3xPyFc4
jako	jako	k9
levicové	levicový	k2eAgFnPc1d1
strany	strana	k1gFnPc1
prezentují	prezentovat	k5eAaBmIp3nP
například	například	k6eAd1
KSČM	KSČM	kA
či	či	k8xC
menší	malý	k2eAgNnSc1d2
uskupení	uskupení	k1gNnSc1
jako	jako	k8xS,k8xC
Levice	levice	k1gFnSc1
nebo	nebo	k8xC
Budoucnost	budoucnost	k1gFnSc1
<g/>
,	,	kIx,
z	z	k7c2
řad	řada	k1gFnPc2
středolevice	středolevice	k1gFnSc2
pak	pak	k9
ČSSD	ČSSD	kA
či	či	k8xC
hnutí	hnutí	k1gNnSc6
Idealisté	idealista	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgInPc1d1
rysy	rys	k1gInPc1
</s>
<s>
Vyznačuje	vyznačovat	k5eAaImIp3nS
se	s	k7c7
sympatiemi	sympatie	k1gFnPc7
pro	pro	k7c4
takové	takový	k3xDgInPc4
principy	princip	k1gInPc4
<g/>
,	,	kIx,
jakými	jaký	k3yQgInPc7,k3yIgInPc7,k3yRgInPc7
jsou	být	k5eAaImIp3nP
svoboda	svoboda	k1gFnSc1
<g/>
,	,	kIx,
rovnost	rovnost	k1gFnSc1
<g/>
,	,	kIx,
solidarita	solidarita	k1gFnSc1
<g/>
,	,	kIx,
pokrok	pokrok	k1gInSc1
<g/>
,	,	kIx,
reforma	reforma	k1gFnSc1
a	a	k8xC
internacionalismus	internacionalismus	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Akcentuje	akcentovat	k5eAaImIp3nS
rovnostářství	rovnostářství	k1gNnSc1
<g/>
,	,	kIx,
prosazuje	prosazovat	k5eAaImIp3nS
sociální	sociální	k2eAgFnSc4d1
rovnost	rovnost	k1gFnSc4
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
tak	tak	k6eAd1
jako	jako	k8xC,k8xS
rovnost	rovnost	k1gFnSc1
příležitostí	příležitost	k1gFnPc2
prostřednictvím	prostřednictvím	k7c2
ochrany	ochrana	k1gFnSc2
sociálně	sociálně	k6eAd1
slabších	slabý	k2eAgFnPc2d2
a	a	k8xC
neprivilegovaných	privilegovaný	k2eNgFnPc2d1
vrstev	vrstva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
pro	pro	k7c4
náboženskou	náboženský	k2eAgFnSc4d1
rovnost	rovnost	k1gFnSc4
<g/>
,	,	kIx,
případně	případně	k6eAd1
se	se	k3xPyFc4
staví	stavit	k5eAaImIp3nS,k5eAaBmIp3nS,k5eAaPmIp3nS
proti	proti	k7c3
náboženství	náboženství	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Hájí	hájit	k5eAaImIp3nP
práva	právo	k1gNnPc4
a	a	k8xC
zájmy	zájem	k1gInPc4
marginalizovaných	marginalizovaný	k2eAgFnPc2d1
<g/>
,	,	kIx,
znevýhodněných	znevýhodněný	k2eAgFnPc2d1
a	a	k8xC
nižších	nízký	k2eAgFnPc2d2
vrstev	vrstva	k1gFnPc2
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
zvlášť	zvlášť	k6eAd1
pak	pak	k6eAd1
zájmy	zájem	k1gInPc4
zaměstnanců	zaměstnanec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podporuje	podporovat	k5eAaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
pracující	pracující	k1gMnPc1
podíleli	podílet	k5eAaImAgMnP
na	na	k7c4
rozhodování	rozhodování	k1gNnSc4
ve	v	k7c6
firmách	firma	k1gFnPc6
a	a	k8xC
podnicích	podnik	k1gInPc6
(	(	kIx(
<g/>
prostřednictvím	prostřednictvím	k7c2
odborových	odborový	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
či	či	k8xC
samosprávy	samospráva	k1gFnSc2
pracujících	pracující	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Levicové	levicový	k2eAgInPc1d1
směry	směr	k1gInPc1
jako	jako	k8xS,k8xC
socialismus	socialismus	k1gInSc1
<g/>
,	,	kIx,
komunismus	komunismus	k1gInSc1
nebo	nebo	k8xC
anarchismus	anarchismus	k1gInSc1
usilují	usilovat	k5eAaImIp3nP
o	o	k7c4
předání	předání	k1gNnSc4
výrobních	výrobní	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
do	do	k7c2
rukou	ruka	k1gFnPc2
pracujících	pracující	k1gMnPc2
<g/>
,	,	kIx,
případně	případně	k6eAd1
upřednostňují	upřednostňovat	k5eAaImIp3nP
státní	státní	k2eAgFnSc4d1
<g/>
,	,	kIx,
družstevní	družstevní	k2eAgFnSc4d1
nebo	nebo	k8xC
jinou	jiný	k2eAgFnSc4d1
formu	forma	k1gFnSc4
společného	společný	k2eAgNnSc2d1
vlastnictví	vlastnictví	k1gNnSc2
výrobních	výrobní	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
před	před	k7c7
vlastnictvím	vlastnictví	k1gNnSc7
soukromým	soukromý	k2eAgNnSc7d1
<g/>
,	,	kIx,
jakožto	jakožto	k8xS
způsob	způsob	k1gInSc1
boje	boj	k1gInSc2
proti	proti	k7c3
vykořisťování	vykořisťování	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Kritizuje	kritizovat	k5eAaImIp3nS
ekonomickou	ekonomický	k2eAgFnSc4d1
nerovnost	nerovnost	k1gFnSc4
a	a	k8xC
podporuje	podporovat	k5eAaImIp3nS
přerozdělování	přerozdělování	k1gNnSc1
s	s	k7c7
cílem	cíl	k1gInSc7
podpořit	podpořit	k5eAaPmF
sociální	sociální	k2eAgFnSc4d1
kohezi	koheze	k1gFnSc4
ve	v	k7c6
společnosti	společnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Extrémní	extrémní	k2eAgInSc1d1
kontrast	kontrast	k1gInSc1
bídy	bída	k1gFnSc2
a	a	k8xC
blahobytu	blahobyt	k1gInSc2
považuje	považovat	k5eAaImIp3nS
za	za	k7c4
amorální	amorální	k2eAgFnSc4d1
<g/>
,	,	kIx,
nemotivační	motivační	k2eNgFnSc4d1
a	a	k8xC
společensky	společensky	k6eAd1
rozkladný	rozkladný	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Způsob	způsob	k1gInSc1
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
dosáhnout	dosáhnout	k5eAaPmF
ekonomického	ekonomický	k2eAgInSc2d1
systému	systém	k1gInSc2
bez	bez	k7c2
vykořisťování	vykořisťování	k1gNnSc2
<g/>
,	,	kIx,
nadvlády	nadvláda	k1gFnSc2
a	a	k8xC
hierarchie	hierarchie	k1gFnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
však	však	k9
různí	různý	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sociální	sociální	k2eAgFnSc1d1
demokracie	demokracie	k1gFnSc1
a	a	k8xC
středolevice	středolevice	k1gFnSc1
upřednostňuje	upřednostňovat	k5eAaImIp3nS
model	model	k1gInSc1
sociálně-tržního	sociálně-tržní	k2eAgNnSc2d1
hospodářství	hospodářství	k1gNnSc2
se	se	k3xPyFc4
silným	silný	k2eAgInSc7d1
sociálním	sociální	k2eAgInSc7d1
státem	stát	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Marxisté	marxista	k1gMnPc1
<g/>
,	,	kIx,
historicky	historicky	k6eAd1
vycházející	vycházející	k2eAgFnSc1d1
z	z	k7c2
ekonomických	ekonomický	k2eAgFnPc2d1
teorií	teorie	k1gFnPc2
Karla	Karel	k1gMnSc4
Marxe	Marx	k1gMnSc4
<g/>
,	,	kIx,
vidí	vidět	k5eAaImIp3nS
cestu	cesta	k1gFnSc4
ve	v	k7c6
znárodnění	znárodnění	k1gNnSc6
a	a	k8xC
centrálně	centrálně	k6eAd1
plánovaném	plánovaný	k2eAgNnSc6d1
hospodářství	hospodářství	k1gNnSc6
<g/>
,	,	kIx,
jakožto	jakožto	k8xS
předvojem	předvoj	k1gInSc7
ke	k	k7c3
vzniku	vznik	k1gInSc3
komunistické	komunistický	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Levicový	levicový	k2eAgInSc4d1
libertarianismus	libertarianismus	k1gInSc4
<g/>
,	,	kIx,
libertariánský	libertariánský	k2eAgInSc4d1
socialismus	socialismus	k1gInSc4
a	a	k8xC
anarchismus	anarchismus	k1gInSc4
prosazují	prosazovat	k5eAaImIp3nP
decentralizovanou	decentralizovaný	k2eAgFnSc4d1
ekonomiku	ekonomika	k1gFnSc4
řízenou	řízený	k2eAgFnSc7d1
odbory	odbor	k1gInPc7
<g/>
,	,	kIx,
samosprávami	samospráva	k1gFnPc7
pracujících	pracující	k1gMnPc2
<g/>
,	,	kIx,
družstvy	družstvo	k1gNnPc7
<g/>
,	,	kIx,
obcemi	obec	k1gFnPc7
a	a	k8xC
komunami	komuna	k1gFnPc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
by	by	kYmCp3nP
byly	být	k5eAaImAgInP
výrobní	výrobní	k2eAgInPc1d1
prostředky	prostředek	k1gInPc1
ve	v	k7c6
společném	společný	k2eAgNnSc6d1
vlastnictví	vlastnictví	k1gNnSc6
a	a	k8xC
řízeny	řízen	k2eAgFnPc4d1
by	by	kYmCp3nS
byly	být	k5eAaImAgFnP
lokálně	lokálně	k6eAd1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
oponuje	oponovat	k5eAaImIp3nS
<g />
.	.	kIx.
</s>
<s hack="1">
jak	jak	k8xS,k8xC
myšlence	myšlenka	k1gFnSc3
ekonomiky	ekonomika	k1gFnSc2
řízené	řízený	k2eAgFnSc2d1
státem	stát	k1gInSc7
(	(	kIx(
<g/>
státní	státní	k2eAgInSc1d1
socialismus	socialismus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tak	tak	k9
trhem	trh	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Některé	některý	k3yIgInPc1
novější	nový	k2eAgInPc1d2
směry	směr	k1gInPc1
socialismu	socialismus	k1gInSc2
pak	pak	k6eAd1
od	od	k7c2
plánování	plánování	k1gNnSc2
zcela	zcela	k6eAd1
upouští	upouštět	k5eAaImIp3nP
a	a	k8xC
upřednostňují	upřednostňovat	k5eAaImIp3nP
tržní	tržní	k2eAgFnSc4d1
alokaci	alokace	k1gFnSc4
v	v	k7c6
rámci	rámec	k1gInSc6
ekonomiky	ekonomika	k1gFnSc2
založené	založený	k2eAgFnSc2d1
na	na	k7c6
společném	společný	k2eAgNnSc6d1
vlastnictví	vlastnictví	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Klade	klást	k5eAaImIp3nS
spíše	spíše	k9
důraz	důraz	k1gInSc4
na	na	k7c4
kolektivismus	kolektivismus	k1gInSc4
než	než	k8xS
na	na	k7c4
individualismus	individualismus	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
PLECITÁ-VLACHOVÁ	PLECITÁ-VLACHOVÁ	k1gFnSc1
<g/>
,	,	kIx,
Klára	Klára	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Levice	levice	k1gFnSc1
a	a	k8xC
pravice	pravice	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Revue	revue	k1gFnSc1
Politika	politika	k1gFnSc1
<g/>
,	,	kIx,
2001-9-20	2001-9-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
čeština	čeština	k1gFnSc1
<g/>
)	)	kIx)
↑	↑	k?
WOSHINSKY	WOSHINSKY	kA
<g/>
,	,	kIx,
Oliver	Oliver	k1gInSc1
H.	H.	kA
Explaining	Explaining	k1gInSc1
Politics	Politics	k1gInSc1
<g/>
:	:	kIx,
Culture	Cultur	k1gMnSc5
<g/>
,	,	kIx,
Institutions	Institutions	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
Political	Political	k1gMnSc1
Behavior	Behavior	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Routledge	Routledge	k1gNnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
143	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
WOSHINSKY	WOSHINSKY	kA
<g/>
,	,	kIx,
Oliver	Oliver	k1gInSc1
H.	H.	kA
Explaining	Explaining	k1gInSc1
Politics	Politics	k1gInSc1
<g/>
:	:	kIx,
Culture	Cultur	k1gMnSc5
<g/>
,	,	kIx,
Institutions	Institutions	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
Political	Political	k1gMnSc1
Behavior	Behavior	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oxon	Oxon	k1gNnSc1
<g/>
,	,	kIx,
England	England	k1gInSc1
<g/>
,	,	kIx,
UK	UK	kA
<g/>
;	;	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
USA	USA	kA
<g/>
:	:	kIx,
Routledge	Routledg	k1gMnSc2
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
203933183	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
145	#num#	k4
<g/>
-	-	kIx~
<g/>
149	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
angličtina	angličtina	k1gFnSc1
<g/>
)	)	kIx)
↑	↑	k?
HEYWOOD	HEYWOOD	kA
<g/>
,	,	kIx,
Andrew	Andrew	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Politologie	politologie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Eurolex	Eurolex	k1gInSc1
Bohemia	bohemia	k1gFnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86432	#num#	k4
<g/>
-	-	kIx~
<g/>
95	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
270,452	270,452	k4
<g/>
.	.	kIx.
↑	↑	k?
HAVEL	Havel	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Levice	levice	k1gFnSc1
a	a	k8xC
levicové	levicový	k2eAgFnPc1d1
hodnoty	hodnota	k1gFnPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Atlas	Atlas	k1gInSc1
Transformace	transformace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
GLYN	GLYN	kA
<g/>
,	,	kIx,
Andrew	Andrew	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Social	Social	k1gInSc1
Democracy	Democraca	k1gFnSc2
in	in	k?
Neoliberal	Neoliberal	k1gMnSc1
Times	Times	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Left	Left	k1gMnSc1
and	and	k?
Economic	Economic	k1gMnSc1
Policy	Polica	k1gFnSc2
since	sinec	k1gInSc2
1980	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojené	spojený	k2eAgInPc1d1
království	království	k1gNnSc1
<g/>
:	:	kIx,
Oxford	Oxford	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
-	-	kIx~
<g/>
924138	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Andrai	Andra	k1gFnSc2
<g/>
,	,	kIx,
Charles	Charles	k1gMnSc1
F.	F.	kA
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Comparative	Comparativ	k1gInSc5
Political	Political	k1gMnSc2
Systems	Systemsa	k1gFnPc2
<g/>
:	:	kIx,
Policy	Polic	k2eAgFnPc1d1
Performance	performance	k1gFnPc1
and	and	k?
Social	Social	k1gMnSc1
Change	change	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Armonk	Armonk	k1gMnSc1
<g/>
,	,	kIx,
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
M.	M.	kA
E.	E.	kA
Sharpe	sharp	k1gInSc5
<g/>
.	.	kIx.
p.	p.	k?
140	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
LONG	LONG	kA
<g/>
,	,	kIx,
Roderick	Roderick	k1gMnSc1
T.	T.	kA
Toward	Toward	k1gMnSc1
a	a	k8xC
Libertarian	Libertarian	k1gMnSc1
Theory	Theora	k1gFnSc2
of	of	k?
Class	Class	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Social	Social	k1gInSc1
Philosophy	Philosopha	k1gFnSc2
and	and	k?
Policy	Polica	k1gFnSc2
<g/>
.	.	kIx.
1998	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
15	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
305	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
S	s	k7c7
<g/>
0	#num#	k4
<g/>
265052500002028	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
BUCHANAN	BUCHANAN	kA
<g/>
,	,	kIx,
Alan	Alan	k1gMnSc1
E.	E.	kA
Ethics	Ethics	k1gInSc1
<g/>
,	,	kIx,
Efficiency	Efficienca	k1gFnPc1
and	and	k?
the	the	k?
Market	market	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
USA	USA	kA
<g/>
:	:	kIx,
Oxford	Oxford	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
US	US	kA
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
8476	#num#	k4
<g/>
-	-	kIx~
<g/>
7396	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
104	#num#	k4
<g/>
–	–	k?
<g/>
105	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ŽALOUDEK	Žaloudek	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyklopedie	encyklopedie	k1gFnSc1
politiky	politika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85983	#num#	k4
<g/>
-	-	kIx~
<g/>
75	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
228	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Nová	nový	k2eAgFnSc1d1
levice	levice	k1gFnSc1
</s>
<s>
Krajní	krajní	k2eAgFnSc1d1
levice	levice	k1gFnSc1
</s>
<s>
Středolevice	Středolevice	k1gFnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cesta	cesta	k1gFnSc1
</s>
<s>
Politická	politický	k2eAgFnSc1d1
ideologie	ideologie	k1gFnSc1
</s>
<s>
Politický	politický	k2eAgInSc1d1
kompas	kompas	k1gInSc1
</s>
<s>
Politický	politický	k2eAgInSc1d1
střed	střed	k1gInSc1
</s>
<s>
Pravice	pravice	k1gFnSc1
</s>
<s>
Regresivní	regresivní	k2eAgFnSc1d1
levice	levice	k1gFnSc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
BOBBIO	BOBBIO	kA
<g/>
,	,	kIx,
Norberto	Norberta	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravice	pravice	k1gFnSc2
a	a	k8xC
levice	levice	k1gFnSc2
<g/>
:	:	kIx,
důvod	důvod	k1gInSc1
a	a	k8xC
rozdělení	rozdělení	k1gNnSc1
politické	politický	k2eAgFnSc2d1
scény	scéna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Centrum	centrum	k1gNnSc1
pro	pro	k7c4
studium	studium	k1gNnSc4
demokracie	demokracie	k1gFnSc2
a	a	k8xC
kultury	kultura	k1gFnSc2
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
153	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7325	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
čeština	čeština	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
levice	levice	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
levice	levice	k1gFnSc2
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Levice	levice	k1gFnSc1
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Levice	levice	k1gFnSc1
a	a	k8xC
levicové	levicový	k2eAgFnPc1d1
hodnoty	hodnota	k1gFnPc1
–	–	k?
Jiří	Jiří	k1gMnSc1
Havel	Havel	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Pravice	pravice	k1gFnSc1
a	a	k8xC
levice	levice	k1gFnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Matoucí	matoucí	k2eAgInPc1d1
pojmy	pojem	k1gInPc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
–	–	k?
Jiří	Jiří	k1gMnSc1
Pehe	Peh	k1gMnSc2
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Levice	levice	k1gFnSc1
a	a	k8xC
pravice	pravice	k1gFnSc1
–	–	k?
Roman	Roman	k1gMnSc1
Joch	Joch	k?
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Leftist	Leftist	k1gMnSc1
Parties	Parties	k1gMnSc1
of	of	k?
the	the	k?
World	World	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Politické	politický	k2eAgFnSc2d1
ideologie	ideologie	k1gFnSc2
</s>
<s>
Agrarismus	agrarismus	k1gInSc1
•	•	k?
Anarchismus	anarchismus	k1gInSc1
•	•	k?
Autoritarismus	Autoritarismus	k1gInSc1
•	•	k?
Centrismus	centrismus	k1gInSc1
•	•	k?
Fašismus	fašismus	k1gInSc1
•	•	k?
Feminismus	feminismus	k1gInSc1
•	•	k?
Individualismus	individualismus	k1gInSc1
•	•	k?
Islamismus	islamismus	k1gInSc1
•	•	k?
Komunismus	komunismus	k1gInSc1
•	•	k?
Komunitarismus	Komunitarismus	k1gInSc1
•	•	k?
Konzervatismus	konzervatismus	k1gInSc1
•	•	k?
Křesťanská	křesťanský	k2eAgFnSc1d1
demokracie	demokracie	k1gFnSc1
•	•	k?
Liberalismus	liberalismus	k1gInSc1
•	•	k?
Libertarianismus	Libertarianismus	k1gInSc1
•	•	k?
Levicová	levicový	k2eAgFnSc1d1
politika	politika	k1gFnSc1
•	•	k?
Monarchismus	monarchismus	k1gInSc1
•	•	k?
Nacionalismus	nacionalismus	k1gInSc1
•	•	k?
Nacismus	nacismus	k1gInSc1
•	•	k?
Pravicová	pravicový	k2eAgFnSc1d1
politika	politika	k1gFnSc1
•	•	k?
Pirátská	pirátský	k2eAgFnSc1d1
politika	politika	k1gFnSc1
•	•	k?
Republikanismus	republikanismus	k1gInSc1
•	•	k?
Socialismus	socialismus	k1gInSc1
•	•	k?
Sociální	sociální	k2eAgFnSc1d1
demokracie	demokracie	k1gFnSc1
•	•	k?
Syndikalismus	syndikalismus	k1gInSc1
•	•	k?
Zelená	zelenat	k5eAaImIp3nS
politika	politika	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Politika	politika	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4035854-9	4035854-9	k4
</s>
