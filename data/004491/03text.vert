<s>
Tower	Tower	k1gInSc1	Tower
Bridge	Bridg	k1gInSc2	Bridg
je	být	k5eAaImIp3nS	být
zvedací	zvedací	k2eAgInSc4d1	zvedací
most	most	k1gInSc4	most
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
nad	nad	k7c7	nad
řekou	řeka	k1gFnSc7	řeka
Temží	Temže	k1gFnPc2	Temže
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
něj	on	k3xPp3gNnSc2	on
stojí	stát	k5eAaImIp3nS	stát
Tower	Tower	k1gInSc1	Tower
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
nesprávně	správně	k6eNd1	správně
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
Londýnský	londýnský	k2eAgInSc1d1	londýnský
most	most	k1gInSc1	most
(	(	kIx(	(
<g/>
London	London	k1gMnSc1	London
Bridge	Bridg	k1gFnSc2	Bridg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
hned	hned	k6eAd1	hned
další	další	k2eAgInSc4d1	další
most	most	k1gInSc4	most
proti	proti	k7c3	proti
proudu	proud	k1gInSc3	proud
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Letních	letní	k2eAgFnPc2d1	letní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
dostal	dostat	k5eAaPmAgInS	dostat
most	most	k1gInSc1	most
ozdobu	ozdoba	k1gFnSc4	ozdoba
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
připevněných	připevněný	k2eAgInPc2d1	připevněný
olympijských	olympijský	k2eAgInPc2d1	olympijský
kruhů	kruh	k1gInPc2	kruh
<g/>
,	,	kIx,	,
symbolu	symbol	k1gInSc2	symbol
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
následující	následující	k2eAgFnSc2d1	následující
paralympiády	paralympiáda	k1gFnSc2	paralympiáda
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgInP	být
kruhy	kruh	k1gInPc1	kruh
vyměněny	vyměnit	k5eAaPmNgInP	vyměnit
za	za	k7c4	za
tři	tři	k4xCgInPc4	tři
barevné	barevný	k2eAgInPc4d1	barevný
srpky	srpek	k1gInPc4	srpek
<g/>
,	,	kIx,	,
symbol	symbol	k1gInSc4	symbol
paralympijských	paralympijský	k2eAgFnPc2d1	paralympijská
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
Londýna	Londýn	k1gInSc2	Londýn
začala	začít	k5eAaPmAgFnS	začít
rozmáhat	rozmáhat	k5eAaImF	rozmáhat
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
výstavba	výstavba	k1gFnSc1	výstavba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
vybudovat	vybudovat	k5eAaPmF	vybudovat
nový	nový	k2eAgInSc4d1	nový
most	most	k1gInSc4	most
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
stál	stát	k5eAaImAgInS	stát
po	po	k7c6	po
proudu	proud	k1gInSc6	proud
řeky	řeka	k1gFnSc2	řeka
za	za	k7c7	za
mostem	most	k1gInSc7	most
London	London	k1gMnSc1	London
Bridge	Bridg	k1gMnSc2	Bridg
<g/>
.	.	kIx.	.
</s>
<s>
Nebylo	být	k5eNaImAgNnS	být
zde	zde	k6eAd1	zde
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
možné	možný	k2eAgNnSc1d1	možné
postavit	postavit	k5eAaPmF	postavit
tradiční	tradiční	k2eAgInSc4d1	tradiční
typ	typ	k1gInSc4	typ
mostu	most	k1gInSc2	most
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
by	by	kYmCp3nS	by
znemožnil	znemožnit	k5eAaPmAgInS	znemožnit
lodní	lodní	k2eAgFnSc4d1	lodní
dopravu	doprava	k1gFnSc4	doprava
do	do	k7c2	do
Londýnského	londýnský	k2eAgInSc2d1	londýnský
přístavu	přístav	k1gInSc2	přístav
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
mezi	mezi	k7c7	mezi
mostem	most	k1gInSc7	most
London	London	k1gMnSc1	London
Bridge	Bridg	k1gInPc4	Bridg
a	a	k8xC	a
Towerem	Towero	k1gNnSc7	Towero
<g/>
.	.	kIx.	.
</s>
<s>
Tunel	tunel	k1gInSc1	tunel
pod	pod	k7c7	pod
Temží	Temže	k1gFnSc7	Temže
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Tower	Tower	k1gMnSc1	Tower
Subway	Subwaa	k1gMnSc2	Subwaa
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
sice	sice	k8xC	sice
otevřen	otevřít	k5eAaPmNgInS	otevřít
roku	rok	k1gInSc3	rok
1870	[number]	k4	1870
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
určen	určit	k5eAaPmNgInS	určit
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
osobní	osobní	k2eAgFnSc4d1	osobní
dopravu	doprava	k1gFnSc4	doprava
a	a	k8xC	a
po	po	k7c6	po
přerušení	přerušení	k1gNnSc6	přerušení
provozu	provoz	k1gInSc2	provoz
tamní	tamní	k2eAgFnSc2d1	tamní
železnice	železnice	k1gFnSc2	železnice
pro	pro	k7c4	pro
pěší	pěší	k2eAgMnPc4d1	pěší
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
byla	být	k5eAaImAgFnS	být
ustavena	ustaven	k2eAgFnSc1d1	ustavena
Komise	komise	k1gFnSc1	komise
pro	pro	k7c4	pro
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
most	most	k1gInSc4	most
nebo	nebo	k8xC	nebo
tunel	tunel	k1gInSc4	tunel
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
pokusila	pokusit	k5eAaPmAgFnS	pokusit
najít	najít	k5eAaPmF	najít
řešení	řešení	k1gNnSc4	řešení
této	tento	k3xDgFnSc2	tento
dopravní	dopravní	k2eAgFnSc2d1	dopravní
hádanky	hádanka	k1gFnSc2	hádanka
<g/>
.	.	kIx.	.
</s>
<s>
Vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
soutěž	soutěž	k1gFnSc4	soutěž
na	na	k7c6	na
konstrukci	konstrukce	k1gFnSc6	konstrukce
nového	nový	k2eAgInSc2d1	nový
mostu	most	k1gInSc2	most
<g/>
.	.	kIx.	.
</s>
<s>
Zasláno	zaslán	k2eAgNnSc1d1	zasláno
bylo	být	k5eAaImAgNnS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
návrhů	návrh	k1gInPc2	návrh
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
jednoho	jeden	k4xCgNnSc2	jeden
od	od	k7c2	od
stavebního	stavební	k2eAgMnSc2d1	stavební
inženýra	inženýr	k1gMnSc2	inženýr
Josepha	Joseph	k1gMnSc2	Joseph
Bazalgetta	Bazalgett	k1gMnSc2	Bazalgett
<g/>
.	.	kIx.	.
</s>
<s>
Vyhodnocování	vyhodnocování	k1gNnSc1	vyhodnocování
návrhů	návrh	k1gInPc2	návrh
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
kontroverzní	kontroverzní	k2eAgMnSc1d1	kontroverzní
a	a	k8xC	a
vítězný	vítězný	k2eAgInSc1d1	vítězný
návrh	návrh	k1gInSc1	návrh
<g/>
,	,	kIx,	,
zaslaný	zaslaný	k2eAgInSc1d1	zaslaný
městským	městský	k2eAgInSc7d1	městský
architektem	architekt	k1gMnSc7	architekt
Horace	Horace	k1gFnSc2	Horace
Jonesem	Jones	k1gMnSc7	Jones
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
až	až	k9	až
roku	rok	k1gInSc2	rok
1884	[number]	k4	1884
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
mostu	most	k1gInSc2	most
započala	započnout	k5eAaPmAgFnS	započnout
roku	rok	k1gInSc2	rok
1886	[number]	k4	1886
a	a	k8xC	a
trvala	trvat	k5eAaImAgFnS	trvat
8	[number]	k4	8
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
práci	práce	k1gFnSc6	práce
se	se	k3xPyFc4	se
podílelo	podílet	k5eAaImAgNnS	podílet
5	[number]	k4	5
hlavních	hlavní	k2eAgMnPc2d1	hlavní
dodavatelů	dodavatel	k1gMnPc2	dodavatel
a	a	k8xC	a
432	[number]	k4	432
dělníků	dělník	k1gMnPc2	dělník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
stability	stabilita	k1gFnSc2	stabilita
mostu	most	k1gInSc2	most
byly	být	k5eAaImAgInP	být
na	na	k7c4	na
říční	říční	k2eAgNnSc4d1	říční
dno	dno	k1gNnSc4	dno
položeny	položen	k2eAgInPc1d1	položen
dva	dva	k4xCgInPc1	dva
masivní	masivní	k2eAgInPc1d1	masivní
pilíře	pilíř	k1gInPc1	pilíř
<g/>
;	;	kIx,	;
každý	každý	k3xTgInSc1	každý
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
70	[number]	k4	70
000	[number]	k4	000
tun	tuna	k1gFnPc2	tuna
betonu	beton	k1gInSc2	beton
<g/>
.	.	kIx.	.
</s>
<s>
Kostra	kostra	k1gFnSc1	kostra
věží	věž	k1gFnPc2	věž
a	a	k8xC	a
mostu	most	k1gInSc2	most
samotného	samotný	k2eAgInSc2d1	samotný
byla	být	k5eAaImAgFnS	být
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
z	z	k7c2	z
více	hodně	k6eAd2	hodně
než	než	k8xS	než
11	[number]	k4	11
000	[number]	k4	000
tun	tuna	k1gFnPc2	tuna
oceli	ocel	k1gFnSc2	ocel
a	a	k8xC	a
jako	jako	k9	jako
obložení	obložení	k1gNnSc4	obložení
mostu	most	k1gInSc2	most
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
ocelové	ocelový	k2eAgFnSc2d1	ocelová
kostry	kostra	k1gFnSc2	kostra
i	i	k9	i
pro	pro	k7c4	pro
vylepšení	vylepšení	k1gNnSc4	vylepšení
vzhledu	vzhled	k1gInSc2	vzhled
byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
cornwallská	cornwallský	k2eAgFnSc1d1	cornwallská
žula	žula	k1gFnSc1	žula
a	a	k8xC	a
portlandský	portlandský	k2eAgInSc1d1	portlandský
vápenec	vápenec	k1gInSc1	vápenec
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1887	[number]	k4	1887
Jones	Jones	k1gMnSc1	Jones
zemřel	zemřít	k5eAaPmAgMnS	zemřít
a	a	k8xC	a
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
hlavní	hlavní	k2eAgMnSc1d1	hlavní
inženýr	inženýr	k1gMnSc1	inženýr
<g/>
,	,	kIx,	,
Sir	sir	k1gMnSc1	sir
John	John	k1gMnSc1	John
Wolfe-Barry	Wolfe-Barra	k1gFnSc2	Wolfe-Barra
<g/>
.	.	kIx.	.
</s>
<s>
Nahradil	nahradit	k5eAaPmAgMnS	nahradit
Jonesův	Jonesův	k2eAgInSc4d1	Jonesův
středověký	středověký	k2eAgInSc4d1	středověký
styl	styl	k1gInSc4	styl
fasády	fasáda	k1gFnSc2	fasáda
více	hodně	k6eAd2	hodně
zdobným	zdobný	k2eAgInSc7d1	zdobný
ve	v	k7c6	v
slohu	sloh	k1gInSc6	sloh
Viktoriánské	viktoriánský	k2eAgFnSc2d1	viktoriánská
gotiky	gotika	k1gFnSc2	gotika
<g/>
.	.	kIx.	.
</s>
<s>
Most	most	k1gInSc1	most
byl	být	k5eAaImAgInS	být
otevřen	otevřen	k2eAgInSc1d1	otevřen
roku	rok	k1gInSc2	rok
1894	[number]	k4	1894
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostní	slavnostní	k2eAgFnSc1d1	slavnostní
ceremonie	ceremonie	k1gFnSc1	ceremonie
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
princem	princ	k1gMnSc7	princ
z	z	k7c2	z
Walesu	Wales	k1gInSc2	Wales
<g/>
,	,	kIx,	,
budoucím	budoucí	k2eAgMnSc7d1	budoucí
králem	král	k1gMnSc7	král
Edwardem	Edward	k1gMnSc7	Edward
VII	VII	kA	VII
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
ženou	žena	k1gFnSc7	žena
<g/>
,	,	kIx,	,
Alexandrou	Alexandra	k1gFnSc7	Alexandra
Dánskou	dánský	k2eAgFnSc7d1	dánská
<g/>
.	.	kIx.	.
</s>
<s>
Most	most	k1gInSc1	most
je	být	k5eAaImIp3nS	být
vybaven	vybavit	k5eAaPmNgInS	vybavit
dvěma	dva	k4xCgFnPc7	dva
výškovými	výškový	k2eAgFnPc7d1	výšková
lávkami	lávka	k1gFnPc7	lávka
<g/>
,	,	kIx,	,
spojujícími	spojující	k2eAgFnPc7d1	spojující
obě	dva	k4xCgFnPc1	dva
věže	věž	k1gFnPc1	věž
mostu	most	k1gInSc2	most
a	a	k8xC	a
používanými	používaný	k2eAgFnPc7d1	používaná
dříve	dříve	k6eAd2	dříve
chodci	chodec	k1gMnPc7	chodec
při	při	k7c6	při
zvednutých	zvednutý	k2eAgFnPc6d1	zvednutá
mostovkách	mostovka	k1gFnPc6	mostovka
při	při	k7c6	při
průjezdu	průjezd	k1gInSc6	průjezd
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Těm	ten	k3xDgMnPc3	ten
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
nelichotivé	lichotivý	k2eNgFnSc3d1	nelichotivá
pověsti	pověst	k1gFnSc3	pověst
ráje	rája	k1gFnSc2	rája
prostitutek	prostitutka	k1gFnPc2	prostitutka
a	a	k8xC	a
kapesních	kapesní	k2eAgMnPc2d1	kapesní
zlodějů	zloděj	k1gMnPc2	zloděj
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byly	být	k5eAaImAgFnP	být
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
uzavřeny	uzavřen	k2eAgInPc1d1	uzavřen
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
jsou	být	k5eAaImIp3nP	být
znovu	znovu	k6eAd1	znovu
otevřeny	otevřít	k5eAaPmNgFnP	otevřít
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
historické	historický	k2eAgFnSc2d1	historická
prohlídky	prohlídka	k1gFnSc2	prohlídka
mostu	most	k1gInSc2	most
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
výstavní	výstavní	k2eAgFnSc1d1	výstavní
expozice	expozice	k1gFnSc1	expozice
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
věžích	věž	k1gFnPc6	věž
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
výstavě	výstava	k1gFnSc6	výstava
lze	lze	k6eAd1	lze
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
spatřit	spatřit	k5eAaPmF	spatřit
dobové	dobový	k2eAgFnPc4d1	dobová
fotografie	fotografia	k1gFnPc4	fotografia
<g/>
,	,	kIx,	,
rukopisné	rukopisný	k2eAgInPc4d1	rukopisný
dokumenty	dokument	k1gInPc4	dokument
a	a	k8xC	a
film	film	k1gInSc4	film
o	o	k7c6	o
stavbě	stavba	k1gFnSc6	stavba
mostu	most	k1gInSc2	most
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
prohlídky	prohlídka	k1gFnSc2	prohlídka
je	být	k5eAaImIp3nS	být
také	také	k9	také
návštěva	návštěva	k1gFnSc1	návštěva
původní	původní	k2eAgFnSc2d1	původní
parní	parní	k2eAgFnSc2d1	parní
strojovny	strojovna	k1gFnSc2	strojovna
<g/>
,	,	kIx,	,
umístěné	umístěný	k2eAgFnSc2d1	umístěná
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
nedaleko	nedaleko	k7c2	nedaleko
jižního	jižní	k2eAgInSc2d1	jižní
konce	konec	k1gInSc2	konec
mostu	most	k1gInSc2	most
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
sloužila	sloužit	k5eAaImAgFnS	sloužit
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
ke	k	k7c3	k
zvedání	zvedání	k1gNnSc3	zvedání
mostovek	mostovka	k1gFnPc2	mostovka
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
si	se	k3xPyFc3	se
návštěvník	návštěvník	k1gMnSc1	návštěvník
může	moct	k5eAaImIp3nS	moct
objednat	objednat	k5eAaPmF	objednat
speciální	speciální	k2eAgFnSc4d1	speciální
prohlídku	prohlídka	k1gFnSc4	prohlídka
V	v	k7c6	v
zákulisí	zákulisí	k1gNnSc6	zákulisí
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
podívat	podívat	k5eAaImF	podívat
do	do	k7c2	do
současného	současný	k2eAgNnSc2d1	současné
řídícího	řídící	k2eAgNnSc2d1	řídící
centra	centrum	k1gNnSc2	centrum
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
nahradilo	nahradit	k5eAaPmAgNnS	nahradit
původní	původní	k2eAgInSc4d1	původní
parní	parní	k2eAgInSc4d1	parní
stroje	stroj	k1gInPc4	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Mostní	mostní	k2eAgInPc1d1	mostní
rámy	rám	k1gInPc1	rám
jsou	být	k5eAaImIp3nP	být
zvednuty	zvednout	k5eAaPmNgInP	zvednout
přibližně	přibližně	k6eAd1	přibližně
pětsetkrát	pětsetkrát	k6eAd1	pětsetkrát
do	do	k7c2	do
roka	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
lodní	lodní	k2eAgFnSc1d1	lodní
doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
menší	malý	k2eAgNnSc1d2	menší
<g/>
,	,	kIx,	,
než	než	k8xS	než
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
stavby	stavba	k1gFnSc2	stavba
mostu	most	k1gInSc2	most
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
má	mít	k5eAaImIp3nS	mít
přednost	přednost	k1gFnSc4	přednost
před	před	k7c7	před
silniční	silniční	k2eAgFnSc7d1	silniční
dopravou	doprava	k1gFnSc7	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
téměř	téměř	k6eAd1	téměř
způsobila	způsobit	k5eAaPmAgFnS	způsobit
diplomatický	diplomatický	k2eAgInSc4d1	diplomatický
incident	incident	k1gInSc4	incident
<g/>
,	,	kIx,	,
když	když	k8xS	když
prezident	prezident	k1gMnSc1	prezident
USA	USA	kA	USA
Bill	Bill	k1gMnSc1	Bill
Clinton	Clinton	k1gMnSc1	Clinton
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
kolonou	kolona	k1gFnSc7	kolona
zůstal	zůstat	k5eAaPmAgInS	zůstat
stát	stát	k1gInSc1	stát
před	před	k7c7	před
mostem	most	k1gInSc7	most
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
se	se	k3xPyFc4	se
neočekávaně	očekávaně	k6eNd1	očekávaně
zvedl	zvednout	k5eAaPmAgMnS	zvednout
<g/>
.	.	kIx.	.
</s>
<s>
Most	most	k1gInSc1	most
leží	ležet	k5eAaImIp3nS	ležet
téměř	téměř	k6eAd1	téměř
nad	nad	k7c4	nad
Tower	Tower	k1gInSc4	Tower
Subway	Subwaa	k1gMnSc2	Subwaa
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc4	první
podzemní	podzemní	k2eAgFnSc4d1	podzemní
drahou	drahá	k1gFnSc4	drahá
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
otevřena	otevřen	k2eAgFnSc1d1	otevřena
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
otevřením	otevření	k1gNnSc7	otevření
stala	stát	k5eAaPmAgFnS	stát
nejkratší	krátký	k2eAgFnSc1d3	nejkratší
komunikací	komunikace	k1gFnSc7	komunikace
<g/>
,	,	kIx,	,
spojující	spojující	k2eAgInPc1d1	spojující
oba	dva	k4xCgInPc1	dva
břehy	břeh	k1gInPc1	břeh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
nový	nový	k2eAgInSc1d1	nový
počítačový	počítačový	k2eAgInSc1d1	počítačový
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
dálkové	dálkový	k2eAgNnSc4d1	dálkové
ovládání	ovládání	k1gNnSc4	ovládání
mostu	most	k1gInSc2	most
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
zatím	zatím	k6eAd1	zatím
značně	značně	k6eAd1	značně
poruchový	poruchový	k2eAgInSc1d1	poruchový
a	a	k8xC	a
již	již	k6eAd1	již
několikrát	několikrát	k6eAd1	několikrát
způsobil	způsobit	k5eAaPmAgMnS	způsobit
zaseknutí	zaseknutí	k1gNnSc4	zaseknutí
mostu	most	k1gInSc2	most
v	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
zavřené	zavřený	k2eAgFnSc6d1	zavřená
pozici	pozice	k1gFnSc6	pozice
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
stalo	stát	k5eAaPmAgNnS	stát
např.	např.	kA	např.
2	[number]	k4	2
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgInSc1d1	severní
konec	konec	k1gInSc1	konec
mostu	most	k1gInSc2	most
je	být	k5eAaImIp3nS	být
poblíž	poblíž	k7c2	poblíž
stanice	stanice	k1gFnSc2	stanice
metra	metro	k1gNnSc2	metro
Tower	Tower	k1gMnSc1	Tower
Hill	Hill	k1gMnSc1	Hill
<g/>
,	,	kIx,	,
stanice	stanice	k1gFnSc1	stanice
DLR	DLR	kA	DLR
Tower	Tower	k1gInSc4	Tower
Gateway	Gatewaa	k1gFnSc2	Gatewaa
a	a	k8xC	a
stanice	stanice	k1gFnSc2	stanice
vlaku	vlak	k1gInSc2	vlak
Fenchurch	Fenchurch	k1gInSc1	Fenchurch
Street	Street	k1gInSc1	Street
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgInSc1d1	jižní
konec	konec	k1gInSc1	konec
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
minut	minuta	k1gFnPc2	minuta
chůze	chůze	k1gFnSc2	chůze
od	od	k7c2	od
stanice	stanice	k1gFnSc2	stanice
metra	metro	k1gNnSc2	metro
London	London	k1gMnSc1	London
Bridge	Bridg	k1gMnSc2	Bridg
<g/>
.	.	kIx.	.
</s>
<s>
Která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
vždy	vždy	k6eAd1	vždy
velmi	velmi	k6eAd1	velmi
přeplněna	přeplnit	k5eAaPmNgNnP	přeplnit
<g/>
,	,	kIx,	,
doporučuji	doporučovat	k5eAaImIp1nS	doporučovat
použít	použít	k5eAaPmF	použít
další	další	k2eAgFnSc4d1	další
stanici	stanice	k1gFnSc4	stanice
<g/>
.	.	kIx.	.
</s>
