<p>
<s>
Baroskop	Baroskop	k1gInSc1	Baroskop
je	být	k5eAaImIp3nS	být
čidlo	čidlo	k1gNnSc4	čidlo
pro	pro	k7c4	pro
dálkové	dálkový	k2eAgNnSc4d1	dálkové
měření	měření	k1gNnSc4	měření
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
měřit	měřit	k5eAaImF	měřit
tlak	tlak	k1gInSc1	tlak
dvoustavové	dvoustavový	k2eAgInPc1d1	dvoustavový
nebo	nebo	k8xC	nebo
spojitě	spojitě	k6eAd1	spojitě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
například	například	k6eAd1	například
používá	používat	k5eAaImIp3nS	používat
baroskop	baroskop	k1gInSc1	baroskop
s	s	k7c7	s
elektrickým	elektrický	k2eAgInSc7d1	elektrický
dvouhodnotovým	dvouhodnotový	k2eAgInSc7d1	dvouhodnotový
signálem	signál	k1gInSc7	signál
pro	pro	k7c4	pro
měření	měření	k1gNnSc4	měření
tlaku	tlak	k1gInSc2	tlak
oleje	olej	k1gInSc2	olej
automobilového	automobilový	k2eAgInSc2d1	automobilový
spalovacího	spalovací	k2eAgInSc2d1	spalovací
motoru	motor	k1gInSc2	motor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Tlakoměr	tlakoměr	k1gInSc1	tlakoměr
</s>
</p>
<p>
<s>
Senzor	senzor	k1gInSc1	senzor
</s>
</p>
