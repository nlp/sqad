<s>
Dačice	Dačice	k1gFnPc1	Dačice
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Datschitz	Datschitz	k1gInSc1	Datschitz
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
město	město	k1gNnSc4	město
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Jindřichův	Jindřichův	k2eAgInSc1d1	Jindřichův
Hradec	Hradec	k1gInSc1	Hradec
v	v	k7c6	v
Jihočeském	jihočeský	k2eAgInSc6d1	jihočeský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
12	[number]	k4	12
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Telče	Telč	k1gFnSc2	Telč
na	na	k7c6	na
Moravské	moravský	k2eAgFnSc6d1	Moravská
Dyji	Dyje	k1gFnSc6	Dyje
v	v	k7c6	v
nejjižnějším	jižní	k2eAgInSc6d3	nejjižnější
cípu	cíp	k1gInSc6	cíp
Českomoravské	českomoravský	k2eAgFnSc2d1	Českomoravská
vrchoviny	vrchovina	k1gFnSc2	vrchovina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
byly	být	k5eAaImAgInP	být
při	při	k7c6	při
správní	správní	k2eAgFnSc6d1	správní
reformě	reforma	k1gFnSc6	reforma
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
okolím	okolí	k1gNnSc7	okolí
začleněny	začleněn	k2eAgFnPc1d1	začleněna
do	do	k7c2	do
Jihočeského	jihočeský	k2eAgInSc2d1	jihočeský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2000	[number]	k4	2000
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
součástí	součást	k1gFnSc7	součást
samosprávného	samosprávný	k2eAgInSc2d1	samosprávný
Jihočeského	jihočeský	k2eAgInSc2d1	jihočeský
kraje	kraj	k1gInSc2	kraj
(	(	kIx(	(
<g/>
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2001	[number]	k4	2001
Budějovického	budějovický	k2eAgInSc2d1	budějovický
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
město	město	k1gNnSc1	město
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
Jihočeského	jihočeský	k2eAgInSc2d1	jihočeský
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
historickém	historický	k2eAgNnSc6d1	historické
území	území	k1gNnSc6	území
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
přes	přes	k7c4	přes
7	[number]	k4	7
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
přirozeným	přirozený	k2eAgInSc7d1	přirozený
centrem	centr	k1gInSc7	centr
nejzazšího	zadní	k2eAgInSc2d3	nejzazší
cípu	cíp	k1gInSc2	cíp
jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
nejen	nejen	k6eAd1	nejen
jejich	jejich	k3xOp3gFnSc7	jejich
polohou	poloha	k1gFnSc7	poloha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
množstvím	množství	k1gNnSc7	množství
služeb	služba	k1gFnPc2	služba
<g/>
,	,	kIx,	,
obchodů	obchod	k1gInPc2	obchod
<g/>
,	,	kIx,	,
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
i	i	k9	i
řadou	řada	k1gFnSc7	řada
kulturních	kulturní	k2eAgFnPc2d1	kulturní
a	a	k8xC	a
sportovních	sportovní	k2eAgFnPc2d1	sportovní
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
kino	kino	k1gNnSc4	kino
nebo	nebo	k8xC	nebo
moderní	moderní	k2eAgInSc4d1	moderní
sportovní	sportovní	k2eAgInSc4d1	sportovní
stadion	stadion	k1gInSc4	stadion
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
okolních	okolní	k2eAgInPc2d1	okolní
rybníků	rybník	k1gInPc2	rybník
<g/>
,	,	kIx,	,
lesy	les	k1gInPc1	les
<g/>
,	,	kIx,	,
historické	historický	k2eAgFnPc1d1	historická
památky	památka	k1gFnPc1	památka
<g/>
,	,	kIx,	,
i	i	k9	i
památná	památný	k2eAgFnSc1d1	památná
<g/>
,	,	kIx,	,
pověstmi	pověst	k1gFnPc7	pověst
proslavená	proslavený	k2eAgNnPc1d1	proslavené
místa	místo	k1gNnPc1	místo
<g/>
,	,	kIx,	,
dotvářejí	dotvářet	k5eAaImIp3nP	dotvářet
malebný	malebný	k2eAgInSc4d1	malebný
charakter	charakter	k1gInSc4	charakter
města	město	k1gNnSc2	město
a	a	k8xC	a
okolního	okolní	k2eAgInSc2d1	okolní
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
pořádá	pořádat	k5eAaImIp3nS	pořádat
také	také	k9	také
řada	řada	k1gFnSc1	řada
kulturních	kulturní	k2eAgInPc2d1	kulturní
festivalů	festival	k1gInPc2	festival
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgNnSc1d1	historické
jádro	jádro	k1gNnSc1	jádro
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
městskou	městský	k2eAgFnSc7d1	městská
památkovou	památkový	k2eAgFnSc7d1	památková
zónou	zóna	k1gFnSc7	zóna
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Dačicích	Dačice	k1gFnPc6	Dačice
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1183	[number]	k4	1183
z	z	k7c2	z
kroniky	kronika	k1gFnSc2	kronika
Jarlocha	Jarloch	k1gMnSc2	Jarloch
<g/>
,	,	kIx,	,
opata	opat	k1gMnSc2	opat
premonstrátského	premonstrátský	k2eAgInSc2d1	premonstrátský
kláštera	klášter	k1gInSc2	klášter
v	v	k7c6	v
Milevsku	Milevsko	k1gNnSc6	Milevsko
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
nechal	nechat	k5eAaPmAgMnS	nechat
znojemský	znojemský	k2eAgMnSc1d1	znojemský
kníže	kníže	k1gMnSc1	kníže
a	a	k8xC	a
moravský	moravský	k2eAgMnSc1d1	moravský
markrabě	markrabě	k1gMnSc1	markrabě
Konrád	Konrád	k1gMnSc1	Konrád
Ota	Ota	k1gMnSc1	Ota
vysvětit	vysvětit	k5eAaPmF	vysvětit
v	v	k7c6	v
Dačicích	Dačice	k1gFnPc6	Dačice
kostel	kostel	k1gInSc1	kostel
olomouckým	olomoucký	k2eAgMnSc7d1	olomoucký
biskupem	biskup	k1gMnSc7	biskup
Pelhřimem	Pelhřim	k1gMnSc7	Pelhřim
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
patřilo	patřit	k5eAaImAgNnS	patřit
město	město	k1gNnSc4	město
pánům	pan	k1gMnPc3	pan
z	z	k7c2	z
Hradce	Hradec	k1gInSc2	Hradec
(	(	kIx(	(
<g/>
Jindřichova	Jindřichův	k2eAgFnSc1d1	Jindřichova
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jejich	jejich	k3xOp3gFnSc2	jejich
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
soudit	soudit	k5eAaImF	soudit
z	z	k7c2	z
městského	městský	k2eAgInSc2d1	městský
erbu	erb	k1gInSc2	erb
se	s	k7c7	s
zlatou	zlatá	k1gFnSc7	zlatá
pětilistou	pětilistý	k2eAgFnSc7d1	pětilistá
růží	růž	k1gFnPc2	růž
<g/>
,	,	kIx,	,
staly	stát	k5eAaPmAgFnP	stát
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
připadly	připadnout	k5eAaPmAgInP	připadnout
Dačice	Dačice	k1gFnPc4	Dačice
Wolfgangu	Wolfgang	k1gMnSc3	Wolfgang
Krajířovi	Krajíř	k1gMnSc3	Krajíř
z	z	k7c2	z
Krajku	krajek	k1gInSc2	krajek
a	a	k8xC	a
na	na	k7c4	na
Landštejně	Landštejně	k1gFnSc4	Landštejně
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
na	na	k7c6	na
významu	význam	k1gInSc6	význam
a	a	k8xC	a
moci	moc	k1gFnSc3	moc
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
přicházejí	přicházet	k5eAaImIp3nP	přicházet
do	do	k7c2	do
města	město	k1gNnSc2	město
italští	italský	k2eAgMnPc1d1	italský
umělci	umělec	k1gMnPc1	umělec
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
stavitelé	stavitel	k1gMnPc1	stavitel
<g/>
.	.	kIx.	.
</s>
<s>
Gotické	gotický	k2eAgNnSc1d1	gotické
město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
změnilo	změnit	k5eAaPmAgNnS	změnit
na	na	k7c4	na
město	město	k1gNnSc4	město
renesanční	renesanční	k2eAgNnSc4d1	renesanční
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
radnice	radnice	k1gFnSc1	radnice
<g/>
,	,	kIx,	,
zámek	zámek	k1gInSc1	zámek
<g/>
,	,	kIx,	,
nový	nový	k2eAgInSc1d1	nový
zámek	zámek	k1gInSc1	zámek
<g/>
,	,	kIx,	,
věž	věž	k1gFnSc1	věž
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nového	nový	k2eAgInSc2d1	nový
zámku	zámek	k1gInSc2	zámek
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
nová	nový	k2eAgFnSc1d1	nová
městská	městský	k2eAgFnSc1d1	městská
čtvrť	čtvrť	k1gFnSc1	čtvrť
Hradecké	Hradecké	k2eAgNnSc2d1	Hradecké
předměstí	předměstí	k1gNnSc2	předměstí
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
obdařeno	obdařit	k5eAaPmNgNnS	obdařit
mnoha	mnoho	k4c7	mnoho
výsadami	výsada	k1gFnPc7	výsada
<g/>
,	,	kIx,	,
bohatne	bohatnout	k5eAaImIp3nS	bohatnout
obchody	obchod	k1gInPc7	obchod
a	a	k8xC	a
řemesly	řemeslo	k1gNnPc7	řemeslo
<g/>
,	,	kIx,	,
organizovanými	organizovaný	k2eAgFnPc7d1	organizovaná
v	v	k7c6	v
ceších	cech	k1gInPc6	cech
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
Dačice	Dačice	k1gFnPc1	Dačice
stávají	stávat	k5eAaImIp3nP	stávat
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
významných	významný	k2eAgNnPc2d1	významné
měst	město	k1gNnPc2	město
Jednoty	jednota	k1gFnSc2	jednota
bratrské	bratrský	k2eAgFnSc2d1	bratrská
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
původní	původní	k2eAgFnSc2d1	původní
vsi	ves	k1gFnSc2	ves
<g/>
,	,	kIx,	,
soustředěné	soustředěný	k2eAgFnSc2d1	soustředěná
okolo	okolo	k7c2	okolo
kostela	kostel	k1gInSc2	kostel
<g/>
,	,	kIx,	,
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
těžištěm	těžiště	k1gNnSc7	těžiště
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
velké	velký	k2eAgNnSc1d1	velké
trojúhelníkové	trojúhelníkový	k2eAgNnSc1d1	trojúhelníkové
náměstí	náměstí	k1gNnSc1	náměstí
s	s	k7c7	s
radnicí	radnice	k1gFnSc7	radnice
a	a	k8xC	a
ostatními	ostatní	k2eAgFnPc7d1	ostatní
budovami	budova	k1gFnPc7	budova
se	s	k7c7	s
správní	správní	k2eAgFnSc7d1	správní
nebo	nebo	k8xC	nebo
obchodní	obchodní	k2eAgFnSc7d1	obchodní
činností	činnost	k1gFnSc7	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Pohromou	pohroma	k1gFnSc7	pohroma
pro	pro	k7c4	pro
město	město	k1gNnSc4	město
byla	být	k5eAaImAgFnS	být
třicetiletá	třicetiletý	k2eAgFnSc1d1	třicetiletá
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1680	[number]	k4	1680
vylidnil	vylidnit	k5eAaPmAgInS	vylidnit
Dačice	Dačice	k1gFnPc1	Dačice
mor	mor	k1gInSc1	mor
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
roku	rok	k1gInSc2	rok
1690	[number]	k4	1690
obrovský	obrovský	k2eAgInSc1d1	obrovský
požár	požár	k1gInSc1	požár
zničil	zničit	k5eAaPmAgInS	zničit
na	na	k7c4	na
80	[number]	k4	80
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejvýznamnější	významný	k2eAgFnSc3d3	nejvýznamnější
barokní	barokní	k2eAgFnSc3d1	barokní
památce	památka	k1gFnSc3	památka
patří	patřit	k5eAaImIp3nS	patřit
tzv.	tzv.	kA	tzv.
františkánský	františkánský	k2eAgInSc1d1	františkánský
klášter	klášter	k1gInSc1	klášter
s	s	k7c7	s
kostelem	kostel	k1gInSc7	kostel
sv.	sv.	kA	sv.
Antonína	Antonín	k1gMnSc2	Antonín
Paduánského	paduánský	k2eAgMnSc2d1	paduánský
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
straně	strana	k1gFnSc6	strana
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
Dačice	Dačice	k1gFnPc1	Dačice
stávají	stávat	k5eAaImIp3nP	stávat
průkopníky	průkopník	k1gMnPc4	průkopník
v	v	k7c6	v
odborném	odborný	k2eAgNnSc6d1	odborné
školství	školství	k1gNnSc6	školství
a	a	k8xC	a
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1820	[number]	k4	1820
byla	být	k5eAaImAgFnS	být
péčí	péče	k1gFnSc7	péče
lesmistra	lesmistr	k1gMnSc2	lesmistr
Vincence	Vincenc	k1gMnSc2	Vincenc
Hlavy	Hlava	k1gMnSc2	Hlava
v	v	k7c6	v
Dačicích	Dačice	k1gFnPc6	Dačice
zřízena	zřídit	k5eAaPmNgFnS	zřídit
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
lesnických	lesnický	k2eAgFnPc2d1	lesnická
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1829	[number]	k4	1829
založili	založit	k5eAaPmAgMnP	založit
bratři	bratr	k1gMnPc1	bratr
Grenberové	Grenberové	k2eAgMnPc1d1	Grenberové
v	v	k7c6	v
nedalekém	daleký	k2eNgInSc6d1	nedaleký
Kostelním	kostelní	k2eAgInSc6d1	kostelní
Vydří	vydří	k2eAgInSc4d1	vydří
první	první	k4xOgInSc4	první
řepný	řepný	k2eAgInSc4d1	řepný
cukrovar	cukrovar	k1gInSc4	cukrovar
moderního	moderní	k2eAgNnSc2d1	moderní
údobí	údobí	k1gNnSc2	údobí
v	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
v	v	k7c6	v
dačické	dačický	k2eAgFnSc6d1	Dačická
rafinérii	rafinérie	k1gFnSc6	rafinérie
byl	být	k5eAaImAgMnS	být
J.	J.	kA	J.
K.	K.	kA	K.
Radem	rad	k1gInSc7	rad
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1841	[number]	k4	1841
první	první	k4xOgInSc4	první
kostkový	kostkový	k2eAgInSc4d1	kostkový
cukr	cukr	k1gInSc4	cukr
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
patentován	patentován	k2eAgInSc1d1	patentován
1843	[number]	k4	1843
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
mění	měnit	k5eAaImIp3nS	měnit
postavení	postavení	k1gNnSc4	postavení
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Dačice	Dačice	k1gFnPc1	Dačice
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
sídlem	sídlo	k1gNnSc7	sídlo
okresu	okres	k1gInSc2	okres
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zabíral	zabírat	k5eAaImAgInS	zabírat
jihozápadní	jihozápadní	k2eAgInSc4d1	jihozápadní
cíp	cíp	k1gInSc4	cíp
Moravy	Morava	k1gFnSc2	Morava
se	s	k7c7	s
180	[number]	k4	180
obcemi	obec	k1gFnPc7	obec
<g/>
.	.	kIx.	.
</s>
<s>
Sídlem	sídlo	k1gNnSc7	sídlo
okresního	okresní	k2eAgInSc2d1	okresní
úřadu	úřad	k1gInSc2	úřad
zůstali	zůstat	k5eAaPmAgMnP	zůstat
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
let	léto	k1gNnPc2	léto
válečných	válečný	k2eAgNnPc2d1	válečné
1940	[number]	k4	1940
-	-	kIx~	-
1945	[number]	k4	1945
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
stech	sto	k4xCgNnPc6	sto
letech	léto	k1gNnPc6	léto
ztratili	ztratit	k5eAaPmAgMnP	ztratit
nejen	nejen	k6eAd1	nejen
sídlo	sídlo	k1gNnSc4	sídlo
okresu	okres	k1gInSc2	okres
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ocitly	ocitnout	k5eAaPmAgFnP	ocitnout
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgNnPc7d1	další
moravskými	moravský	k2eAgNnPc7d1	Moravské
městy	město	k1gNnPc7	město
a	a	k8xC	a
obcemi	obec	k1gFnPc7	obec
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1939-1945	[number]	k4	1939-1945
přišly	přijít	k5eAaPmAgFnP	přijít
Dačice	Dačice	k1gFnPc1	Dačice
o	o	k7c4	o
své	svůj	k3xOyFgMnPc4	svůj
židovské	židovský	k2eAgMnPc4d1	židovský
obyvatele	obyvatel	k1gMnPc4	obyvatel
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
polském	polský	k2eAgInSc6d1	polský
Lublinu	Lublin	k1gInSc6	Lublin
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
odsunu	odsun	k1gInSc3	odsun
několika	několik	k4yIc2	několik
německých	německý	k2eAgFnPc2d1	německá
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
vystavěna	vystavěn	k2eAgFnSc1d1	vystavěna
nemocnice	nemocnice	k1gFnSc1	nemocnice
a	a	k8xC	a
závod	závod	k1gInSc1	závod
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
autodílů	autodíl	k1gInPc2	autodíl
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
největší	veliký	k2eAgMnSc1d3	veliký
zaměstnavatel	zaměstnavatel	k1gMnSc1	zaměstnavatel
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
moravské	moravský	k2eAgFnPc4d1	Moravská
Dačice	Dačice	k1gFnPc4	Dačice
sídlem	sídlo	k1gNnSc7	sídlo
tzv.	tzv.	kA	tzv.
malého	malý	k2eAgInSc2d1	malý
okresu	okres	k1gInSc2	okres
v	v	k7c6	v
Jihočeském	jihočeský	k2eAgInSc6d1	jihočeský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Dačicích	Dačice	k1gFnPc6	Dačice
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
zámek	zámek	k1gInSc1	zámek
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
vzpomínaný	vzpomínaný	k2eAgMnSc1d1	vzpomínaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1591	[number]	k4	1591
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
empírové	empírový	k2eAgFnSc6d1	empírová
podobě	podoba	k1gFnSc6	podoba
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1831	[number]	k4	1831
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgFnSc1d1	národní
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
správě	správa	k1gFnSc6	správa
jihočeského	jihočeský	k2eAgNnSc2d1	Jihočeské
NPÚ	NPÚ	kA	NPÚ
<g/>
,	,	kIx,	,
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
křídle	křídlo	k1gNnSc6	křídlo
zámku	zámek	k1gInSc2	zámek
také	také	k9	také
sídlo	sídlo	k1gNnSc4	sídlo
Městského	městský	k2eAgNnSc2d1	Městské
muzea	muzeum	k1gNnSc2	muzeum
a	a	k8xC	a
galerie	galerie	k1gFnSc2	galerie
<g/>
.	.	kIx.	.
</s>
<s>
Starý	starý	k2eAgInSc1d1	starý
zámek	zámek	k1gInSc1	zámek
<g/>
,	,	kIx,	,
renesanční	renesanční	k2eAgInSc1d1	renesanční
palác	palác	k1gInSc1	palác
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1572	[number]	k4	1572
<g/>
-	-	kIx~	-
<g/>
1579	[number]	k4	1579
<g/>
.	.	kIx.	.
</s>
<s>
Sídlo	sídlo	k1gNnSc4	sídlo
Městského	městský	k2eAgInSc2d1	městský
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1775-1785	[number]	k4	1775-1785
Renesanční	renesanční	k2eAgFnSc1d1	renesanční
věž	věž	k1gFnSc1	věž
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1586-1592	[number]	k4	1586-1592
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
51	[number]	k4	51
m	m	kA	m
a	a	k8xC	a
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
barokní	barokní	k2eAgInSc4d1	barokní
kostel	kostel	k1gInSc4	kostel
sv.	sv.	kA	sv.
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
umístěn	umístit	k5eAaPmNgInS	umístit
sv.	sv.	kA	sv.
Vavřinec	Vavřinec	k1gMnSc1	Vavřinec
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1483	[number]	k4	1483
<g/>
.	.	kIx.	.
</s>
<s>
Františkánský	františkánský	k2eAgInSc1d1	františkánský
klášter	klášter	k1gInSc1	klášter
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1660	[number]	k4	1660
<g/>
-	-	kIx~	-
<g/>
1664	[number]	k4	1664
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
patří	patřit	k5eAaImIp3nS	patřit
řádu	řád	k1gInSc2	řád
bosých	bosý	k2eAgFnPc2d1	bosá
karmelitek	karmelitka	k1gFnPc2	karmelitka
<g/>
,	,	kIx,	,
s	s	k7c7	s
kostelem	kostel	k1gInSc7	kostel
sv.	sv.	kA	sv.
Antonína	Antonín	k1gMnSc2	Antonín
Paduánského	paduánský	k2eAgMnSc2d1	paduánský
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1672-1677	[number]	k4	1672-1677
Kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Rocha	Rocha	k1gFnSc1	Rocha
a	a	k8xC	a
sv.	sv.	kA	sv.
Šebestiána	Šebestián	k1gMnSc4	Šebestián
z	z	k7c2	z
konce	konec	k1gInSc2	konec
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
Mariánský	mariánský	k2eAgInSc1d1	mariánský
sloup	sloup	k1gInSc1	sloup
na	na	k7c6	na
Havlíčkově	Havlíčkův	k2eAgNnSc6d1	Havlíčkovo
náměstí	náměstí	k1gNnSc6	náměstí
s	s	k7c7	s
barokním	barokní	k2eAgNnSc7d1	barokní
sousoším	sousoší	k1gNnSc7	sousoší
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1725	[number]	k4	1725
(	(	kIx(	(
<g/>
sochy	socha	k1gFnSc2	socha
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Prokopa	Prokop	k1gMnSc2	Prokop
<g/>
,	,	kIx,	,
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Neposkvrněného	poskvrněný	k2eNgNnSc2d1	neposkvrněné
početí	početí	k1gNnSc2	početí
<g/>
)	)	kIx)	)
Ozdobou	ozdoba	k1gFnSc7	ozdoba
sousoší	sousoší	k1gNnSc4	sousoší
je	být	k5eAaImIp3nS	být
šestiboká	šestiboký	k2eAgFnSc1d1	šestiboká
empírová	empírový	k2eAgFnSc1d1	empírová
kašna	kašna	k1gFnSc1	kašna
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
radnice	radnice	k1gFnSc1	radnice
Toužínské	Toužínský	k2eAgFnSc2d1	Toužínský
stráně	stráň	k1gFnSc2	stráň
a	a	k8xC	a
Dubová	dubový	k2eAgFnSc1d1	dubová
stráň	stráň	k1gFnSc1	stráň
-	-	kIx~	-
přírodní	přírodní	k2eAgFnPc4d1	přírodní
památky	památka	k1gFnPc4	památka
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
před	před	k7c7	před
věží	věž	k1gFnSc7	věž
farního	farní	k2eAgInSc2d1	farní
kostela	kostel	k1gInSc2	kostel
se	se	k3xPyFc4	se
nacházející	nacházející	k2eAgInSc1d1	nacházející
žulový	žulový	k2eAgInSc1d1	žulový
pomník	pomník	k1gInSc1	pomník
kostky	kostka	k1gFnSc2	kostka
cukru	cukr	k1gInSc2	cukr
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
ve	v	k7c6	v
zdejší	zdejší	k2eAgFnSc6d1	zdejší
rafinerii	rafinerie	k1gFnSc6	rafinerie
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1841	[number]	k4	1841
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1843	[number]	k4	1843
patentován	patentovat	k5eAaBmNgInS	patentovat
J.	J.	kA	J.
K.	K.	kA	K.
Radem	rad	k1gInSc7	rad
první	první	k4xOgInSc4	první
kostkový	kostkový	k2eAgInSc4d1	kostkový
cukr	cukr	k1gInSc4	cukr
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Samospráva	samospráva	k1gFnSc1	samospráva
města	město	k1gNnSc2	město
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
vyvěšuje	vyvěšovat	k5eAaImIp3nS	vyvěšovat
5	[number]	k4	5
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
moravskou	moravský	k2eAgFnSc4d1	Moravská
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Dačice	Dačice	k1gFnPc1	Dačice
se	se	k3xPyFc4	se
člení	členit	k5eAaImIp3nP	členit
na	na	k7c4	na
šestnáct	šestnáct	k4xCc4	šestnáct
místních	místní	k2eAgFnPc2d1	místní
částí	část	k1gFnPc2	část
na	na	k7c6	na
jedenácti	jedenáct	k4xCc6	jedenáct
katastrálních	katastrální	k2eAgNnPc6d1	katastrální
územích	území	k1gNnPc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Bílkov	Bílkov	k1gInSc1	Bílkov
(	(	kIx(	(
<g/>
i	i	k9	i
název	název	k1gInSc1	název
k.	k.	k?	k.
ú.	ú.	k?	ú.
<g/>
)	)	kIx)	)
Borek	Borek	k1gMnSc1	Borek
(	(	kIx(	(
<g/>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Borek	borka	k1gFnPc2	borka
u	u	k7c2	u
Dačic	Dačice	k1gFnPc2	Dačice
<g/>
)	)	kIx)	)
město	město	k1gNnSc1	město
Dačice	Dačice	k1gFnPc1	Dačice
a	a	k8xC	a
Toužín	Toužín	k1gInSc1	Toužín
(	(	kIx(	(
<g/>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Dačice	Dačice	k1gFnPc1	Dačice
<g/>
)	)	kIx)	)
Dolní	dolní	k2eAgFnPc1d1	dolní
Němčice	Němčice	k1gFnPc1	Němčice
(	(	kIx(	(
<g/>
i	i	k9	i
název	název	k1gInSc1	název
k.	k.	k?	k.
ú.	ú.	k?	ú.
<g/>
)	)	kIx)	)
Hostkovice	Hostkovice	k1gFnSc1	Hostkovice
(	(	kIx(	(
<g/>
Dačice	Dačice	k1gFnPc1	Dačice
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
k.	k.	k?	k.
ú.	ú.	k?	ú.
<g />
.	.	kIx.	.
</s>
<s>
Hostkovice	Hostkovice	k1gFnSc1	Hostkovice
u	u	k7c2	u
Dolních	dolní	k2eAgFnPc2d1	dolní
Němčic	Němčice	k1gFnPc2	Němčice
<g/>
)	)	kIx)	)
Hradišťko	Hradišťko	k1gNnSc1	Hradišťko
(	(	kIx(	(
<g/>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Hradišťko	Hradišťko	k1gNnSc1	Hradišťko
u	u	k7c2	u
Dačic	Dačice	k1gFnPc2	Dačice
<g/>
)	)	kIx)	)
Chlumec	Chlumec	k1gInSc1	Chlumec
(	(	kIx(	(
<g/>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Chlumec	Chlumec	k1gInSc1	Chlumec
u	u	k7c2	u
Dačic	Dačice	k1gFnPc2	Dačice
)	)	kIx)	)
Lipolec	Lipolec	k1gInSc1	Lipolec
(	(	kIx(	(
<g/>
i	i	k9	i
název	název	k1gInSc1	název
k.	k.	k?	k.
ú.	ú.	k?	ú.
<g/>
)	)	kIx)	)
Malý	Malý	k1gMnSc1	Malý
Pěčín	Pěčín	k1gMnSc1	Pěčín
(	(	kIx(	(
<g/>
i	i	k8xC	i
název	název	k1gInSc1	název
k.	k.	k?	k.
ú.	ú.	k?	ú.
<g/>
)	)	kIx)	)
Prostřední	prostřednět	k5eAaImIp3nS	prostřednět
Vydří	vydří	k2eAgNnSc1d1	vydří
(	(	kIx(	(
<g/>
i	i	k9	i
název	název	k1gInSc1	název
k.	k.	k?	k.
ú.	ú.	k?	ú.
<g/>
)	)	kIx)	)
Velký	velký	k2eAgInSc1d1	velký
Pěčín	Pěčín	k1gInSc1	Pěčín
(	(	kIx(	(
<g/>
i	i	k9	i
název	název	k1gInSc1	název
k.	k.	k?	k.
ú.	ú.	k?	ú.
<g/>
)	)	kIx)	)
Vlastní	vlastní	k2eAgNnSc1d1	vlastní
město	město	k1gNnSc1	město
Dačice	Dačice	k1gFnPc1	Dačice
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
šest	šest	k4xCc4	šest
částí	část	k1gFnPc2	část
označovaných	označovaný	k2eAgFnPc2d1	označovaná
římskými	římský	k2eAgNnPc7d1	římské
čísly	číslo	k1gNnPc7	číslo
v	v	k7c6	v
uvedeném	uvedený	k2eAgNnSc6d1	uvedené
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
:	:	kIx,	:
I.	I.	kA	I.
Město	město	k1gNnSc1	město
(	(	kIx(	(
<g/>
dolní	dolní	k2eAgNnPc1d1	dolní
a	a	k8xC	a
horní	horní	k2eAgNnPc1d1	horní
náměstí	náměstí	k1gNnPc1	náměstí
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
<g/>
)	)	kIx)	)
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Antonínské	Antonínský	k2eAgNnSc1d1	Antonínský
předměstí	předměstí	k1gNnSc1	předměstí
(	(	kIx(	(
<g/>
k	k	k7c3	k
nemocnici	nemocnice	k1gFnSc3	nemocnice
<g/>
)	)	kIx)	)
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
řekou	řeka	k1gFnSc7	řeka
Dyjí	Dyje	k1gFnPc2	Dyje
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
Stráňské	Stráňský	k2eAgNnSc1d1	Stráňský
předměstí	předměstí	k1gNnSc1	předměstí
(	(	kIx(	(
<g/>
Za	za	k7c7	za
lávkami	lávka	k1gFnPc7	lávka
<g/>
,	,	kIx,	,
Kapetova	Kapetův	k2eAgFnSc1d1	Kapetova
<g/>
,	,	kIx,	,
k	k	k7c3	k
Hradišťku	Hradišťko	k1gNnSc3	Hradišťko
<g/>
)	)	kIx)	)
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Hradecké	Hradecké	k2eAgNnSc1d1	Hradecké
Předměstí	předměstí	k1gNnSc1	předměstí
(	(	kIx(	(
<g/>
Péráček	Péráček	k1gInSc1	Péráček
<g/>
,	,	kIx,	,
Nivy	niva	k1gFnPc1	niva
<g/>
)	)	kIx)	)
V.	V.	kA	V.
Červený	červený	k2eAgInSc1d1	červený
vrch	vrch	k1gInSc1	vrch
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
Toužín	Toužín	k1gInSc1	Toužín
(	(	kIx(	(
<g/>
do	do	k7c2	do
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
samostatné	samostatný	k2eAgFnSc2d1	samostatná
k.ú.	k.ú.	k?	k.ú.
<g/>
)	)	kIx)	)
Prostřední	prostřednět	k5eAaImIp3nS	prostřednět
Vydří	vydří	k2eAgFnSc1d1	vydří
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
odděleném	oddělený	k2eAgNnSc6d1	oddělené
území	území	k1gNnSc6	území
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
tak	tak	k6eAd1	tak
exklávu	exkláva	k1gFnSc4	exkláva
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Ivo	Ivo	k1gMnSc1	Ivo
Jahelka	jahelka	k1gFnSc1	jahelka
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
folkový	folkový	k2eAgMnSc1d1	folkový
písničkář	písničkář	k1gMnSc1	písničkář
Lenka	lenka	k1gFnSc2	lenka
Lanczová	Lanczová	k1gFnSc1	Lanczová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Matěj	Matěj	k1gMnSc1	Matěj
Mikšíček	Mikšíček	k1gMnSc1	Mikšíček
(	(	kIx(	(
<g/>
1815	[number]	k4	1815
<g/>
-	-	kIx~	-
<g/>
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
Jakub	Jakub	k1gMnSc1	Jakub
Kryštof	Kryštof	k1gMnSc1	Kryštof
Rad	rada	k1gFnPc2	rada
(	(	kIx(	(
<g/>
1799	[number]	k4	1799
<g/>
-	-	kIx~	-
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1871	[number]	k4	1871
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
místního	místní	k2eAgInSc2d1	místní
cukrovaru	cukrovar	k1gInSc2	cukrovar
<g/>
,	,	kIx,	,
vynálezce	vynálezce	k1gMnSc1	vynálezce
kostkového	kostkový	k2eAgInSc2d1	kostkový
cukru	cukr	k1gInSc2	cukr
Pavel	Pavel	k1gMnSc1	Pavel
Spurný	Spurný	k1gMnSc1	Spurný
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
astronom	astronom	k1gMnSc1	astronom
František	František	k1gMnSc1	František
Stejskal-Lažanský	Stejskal-Lažanský	k2eAgMnSc1d1	Stejskal-Lažanský
(	(	kIx(	(
<g/>
1844	[number]	k4	1844
<g/>
-	-	kIx~	-
<g/>
1887	[number]	k4	1887
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
Dan	Dan	k1gMnSc1	Dan
Svátek	Svátek	k1gMnSc1	Svátek
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
a	a	k8xC	a
herec	herec	k1gMnSc1	herec
Michal	Michal	k1gMnSc1	Michal
Stehlík	Stehlík	k1gMnSc1	Stehlík
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
děkan	děkan	k1gMnSc1	děkan
FF	ff	kA	ff
UK	UK	kA	UK
Ludvík	Ludvík	k1gMnSc1	Ludvík
Štěpán	Štěpán	k1gMnSc1	Štěpán
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
-	-	kIx~	-
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
akademický	akademický	k2eAgMnSc1d1	akademický
profesor	profesor	k1gMnSc1	profesor
polonistiky	polonistika	k1gFnSc2	polonistika
<g/>
,	,	kIx,	,
bohemista	bohemista	k1gMnSc1	bohemista
<g/>
,	,	kIx,	,
překladatel	překladatel	k1gMnSc1	překladatel
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
Jan	Jan	k1gMnSc1	Jan
Bistřický	Bistřický	k2eAgMnSc1d1	Bistřický
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
-	-	kIx~	-
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
archivář	archivář	k1gMnSc1	archivář
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
a	a	k8xC	a
mj.	mj.	kA	mj.
i	i	k8xC	i
zakladatel	zakladatel	k1gMnSc1	zakladatel
a	a	k8xC	a
předseda	předseda	k1gMnSc1	předseda
dačického	dačický	k2eAgInSc2d1	dačický
spolku	spolek	k1gInSc2	spolek
přátel	přítel	k1gMnPc2	přítel
muzea	muzeum	k1gNnSc2	muzeum
Groß-Siegharts	Groß-Sieghartsa	k1gFnPc2	Groß-Sieghartsa
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
Urtenen-Schönbühl	Urtenen-Schönbühl	k1gFnSc2	Urtenen-Schönbühl
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
</s>
