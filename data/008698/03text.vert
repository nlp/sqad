<p>
<s>
Sahara	Sahara	k1gFnSc1	Sahara
je	být	k5eAaImIp3nS	být
poušť	poušť	k1gFnSc4	poušť
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
9	[number]	k4	9
269	[number]	k4	269
000	[number]	k4	000
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc7d3	veliký
pouští	poušť	k1gFnSc7	poušť
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
jsou	být	k5eAaImIp3nP	být
za	za	k7c4	za
poušť	poušť	k1gFnSc4	poušť
označovány	označovat	k5eAaImNgInP	označovat
i	i	k9	i
polární	polární	k2eAgFnPc1d1	polární
pustiny	pustina	k1gFnPc1	pustina
Antarktidy	Antarktida	k1gFnSc2	Antarktida
a	a	k8xC	a
Arktidy	Arktida	k1gFnSc2	Arktida
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
Sahara	Sahara	k1gFnSc1	Sahara
rozlohou	rozloha	k1gFnSc7	rozloha
až	až	k8xS	až
třetí	třetí	k4xOgMnSc1	třetí
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
téměř	téměř	k6eAd1	téměř
stejnou	stejný	k2eAgFnSc4d1	stejná
plochu	plocha	k1gFnSc4	plocha
jako	jako	k8xS	jako
Čína	Čína	k1gFnSc1	Čína
nebo	nebo	k8xC	nebo
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
od	od	k7c2	od
severoafrického	severoafrický	k2eAgNnSc2d1	severoafrické
pobřeží	pobřeží	k1gNnSc2	pobřeží
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
až	až	k9	až
k	k	k7c3	k
pobřeží	pobřeží	k1gNnSc3	pobřeží
Rudého	rudý	k2eAgNnSc2d1	Rudé
moře	moře	k1gNnSc2	moře
na	na	k7c6	na
území	území	k1gNnSc6	území
10	[number]	k4	10
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
Egypt	Egypt	k1gInSc1	Egypt
<g/>
,	,	kIx,	,
Libye	Libye	k1gFnSc1	Libye
<g/>
,	,	kIx,	,
Alžírsko	Alžírsko	k1gNnSc1	Alžírsko
<g/>
,	,	kIx,	,
Tunisko	Tunisko	k1gNnSc1	Tunisko
<g/>
,	,	kIx,	,
Maroko	Maroko	k1gNnSc1	Maroko
<g/>
,	,	kIx,	,
Mauritánie	Mauritánie	k1gFnSc1	Mauritánie
<g/>
,	,	kIx,	,
Niger	Niger	k1gInSc1	Niger
<g/>
,	,	kIx,	,
Čad	Čad	k1gInSc1	Čad
<g/>
,	,	kIx,	,
Mali	Mali	k1gNnSc1	Mali
a	a	k8xC	a
Súdán	Súdán	k1gInSc1	Súdán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Plocha	plocha	k1gFnSc1	plocha
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
Sahara	Sahara	k1gFnSc1	Sahara
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
zhruba	zhruba	k6eAd1	zhruba
tvar	tvar	k1gInSc4	tvar
lichoběžníku	lichoběžník	k1gInSc2	lichoběžník
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
4500	[number]	k4	4500
<g/>
–	–	k?	–
<g/>
5500	[number]	k4	5500
km	km	kA	km
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
západ	západ	k1gInSc1	západ
<g/>
–	–	k?	–
<g/>
východ	východ	k1gInSc1	východ
a	a	k8xC	a
šířce	šířka	k1gFnSc6	šířka
1500	[number]	k4	1500
<g/>
–	–	k?	–
<g/>
2000	[number]	k4	2000
km	km	kA	km
od	od	k7c2	od
severu	sever	k1gInSc2	sever
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
arabštině	arabština	k1gFnSc6	arabština
ص	ص	k?	ص
<g/>
,	,	kIx,	,
ṣ	ṣ	k?	ṣ
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
poušť	poušť	k1gFnSc1	poušť
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografická	geografický	k2eAgFnSc1d1	geografická
charakteristika	charakteristika	k1gFnSc1	charakteristika
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
Sahara	Sahara	k1gFnSc1	Sahara
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
až	až	k9	až
k	k	k7c3	k
úpatí	úpatí	k1gNnSc3	úpatí
pohoří	pohořet	k5eAaPmIp3nS	pohořet
Atlas	Atlas	k1gInSc1	Atlas
<g/>
,	,	kIx,	,
v	v	k7c6	v
Libyi	Libye	k1gFnSc6	Libye
a	a	k8xC	a
Egyptě	Egypt	k1gInSc6	Egypt
až	až	k8xS	až
k	k	k7c3	k
pobřeží	pobřeží	k1gNnSc3	pobřeží
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
tzv.	tzv.	kA	tzv.
Sahelu	Sahela	k1gFnSc4	Sahela
přechází	přecházet	k5eAaImIp3nS	přecházet
poušť	poušť	k1gFnSc1	poušť
do	do	k7c2	do
pásma	pásmo	k1gNnSc2	pásmo
stepí	step	k1gFnPc2	step
a	a	k8xC	a
savan	savana	k1gFnPc2	savana
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc4d1	jižní
hranici	hranice	k1gFnSc4	hranice
tvoří	tvořit	k5eAaImIp3nP	tvořit
přibližně	přibližně	k6eAd1	přibližně
linie	linie	k1gFnPc1	linie
vedoucí	vedoucí	k2eAgFnPc1d1	vedoucí
od	od	k7c2	od
západu	západ	k1gInSc2	západ
k	k	k7c3	k
východu	východ	k1gInSc3	východ
přes	přes	k7c4	přes
tok	tok	k1gInSc4	tok
řeky	řeka	k1gFnSc2	řeka
Senegal	Senegal	k1gInSc4	Senegal
<g/>
,	,	kIx,	,
střední	střední	k2eAgInSc4d1	střední
tok	tok	k1gInSc4	tok
Nigeru	Niger	k1gInSc2	Niger
<g/>
,	,	kIx,	,
Čadské	čadský	k2eAgNnSc1d1	Čadské
jezero	jezero	k1gNnSc1	jezero
<g/>
,	,	kIx,	,
oblast	oblast	k1gFnSc1	oblast
súdánského	súdánský	k2eAgInSc2d1	súdánský
Dárfúru	Dárfúr	k1gInSc2	Dárfúr
a	a	k8xC	a
soutok	soutok	k1gInSc1	soutok
Nilu	Nil	k1gInSc2	Nil
s	s	k7c7	s
Atbarou	Atbara	k1gFnSc7	Atbara
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Povrch	povrch	k1gInSc1	povrch
Sahary	Sahara	k1gFnSc2	Sahara
tvoří	tvořit	k5eAaImIp3nS	tvořit
převážně	převážně	k6eAd1	převážně
kamenitá	kamenitý	k2eAgFnSc1d1	kamenitá
poušť	poušť	k1gFnSc1	poušť
<g/>
:	:	kIx,	:
skalnatá	skalnatý	k2eAgFnSc1d1	skalnatá
(	(	kIx(	(
<g/>
hamada	hamada	k1gFnSc1	hamada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oblázková	oblázkový	k2eAgFnSc1d1	oblázková
či	či	k8xC	či
štěrková	štěrkový	k2eAgFnSc1d1	štěrková
(	(	kIx(	(
<g/>
reg	reg	k?	reg
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
hlinitá	hlinitý	k2eAgFnSc1d1	hlinitá
(	(	kIx(	(
<g/>
sebh	sebh	k1gMnSc1	sebh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgFnSc4d2	menší
část	část	k1gFnSc4	část
zabírají	zabírat	k5eAaImIp3nP	zabírat
pouště	poušť	k1gFnPc4	poušť
písečné	písečný	k2eAgInPc1d1	písečný
(	(	kIx(	(
<g/>
ergy	erg	k1gInPc1	erg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
se	se	k3xPyFc4	se
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
na	na	k7c6	na
plošinách	plošina	k1gFnPc6	plošina
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
do	do	k7c2	do
500	[number]	k4	500
m	m	kA	m
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
jih	jih	k1gInSc4	jih
se	se	k3xPyFc4	se
podél	podél	k7c2	podél
libyjského	libyjský	k2eAgNnSc2d1	Libyjské
pobřeží	pobřeží	k1gNnSc2	pobřeží
plošina	plošina	k1gFnSc1	plošina
zvedá	zvedat	k5eAaImIp3nS	zvedat
až	až	k9	až
o	o	k7c4	o
796	[number]	k4	796
m	m	kA	m
a	a	k8xC	a
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
dále	daleko	k6eAd2	daleko
jako	jako	k9	jako
písečná	písečný	k2eAgFnSc1d1	písečná
a	a	k8xC	a
kamenitá	kamenitý	k2eAgFnSc1d1	kamenitá
planina	planina	k1gFnSc1	planina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
centrálních	centrální	k2eAgFnPc6d1	centrální
oblastech	oblast	k1gFnPc6	oblast
Sahary	Sahara	k1gFnSc2	Sahara
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
několik	několik	k4yIc1	několik
pohoří	pohoří	k1gNnPc2	pohoří
sopečného	sopečný	k2eAgInSc2d1	sopečný
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
vrcholy	vrchol	k1gInPc1	vrchol
přesahují	přesahovat	k5eAaImIp3nP	přesahovat
tři	tři	k4xCgInPc4	tři
tisíce	tisíc	k4xCgInPc4	tisíc
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Ahaggar	Ahaggar	k1gInSc1	Ahaggar
<g/>
,	,	kIx,	,
Tassili	Tassili	k1gFnPc1	Tassili
nebo	nebo	k8xC	nebo
Tibesti	Tibest	k1gFnPc1	Tibest
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrcholem	vrchol	k1gInSc7	vrchol
je	být	k5eAaImIp3nS	být
neaktivní	aktivní	k2eNgInSc1d1	neaktivní
vulkán	vulkán	k1gInSc1	vulkán
Emi	Emi	k1gMnSc3	Emi
Koussi	Kouss	k1gMnSc3	Kouss
(	(	kIx(	(
<g/>
3415	[number]	k4	3415
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgInPc1d1	vodní
zdroje	zdroj	k1gInPc1	zdroj
těchto	tento	k3xDgFnPc2	tento
oblastí	oblast	k1gFnPc2	oblast
kolem	kolem	k6eAd1	kolem
sebe	sebe	k3xPyFc4	sebe
soustřeďují	soustřeďovat	k5eAaImIp3nP	soustřeďovat
většinu	většina	k1gFnSc4	většina
saharských	saharský	k2eAgFnPc2d1	saharská
oáz	oáza	k1gFnPc2	oáza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
řadě	řada	k1gFnSc6	řada
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
horských	horský	k2eAgFnPc6d1	horská
a	a	k8xC	a
podhorských	podhorský	k2eAgFnPc6d1	podhorská
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
četná	četný	k2eAgNnPc4d1	četné
koryta	koryto	k1gNnPc4	koryto
pouštních	pouštní	k2eAgNnPc2d1	pouštní
vádí	vádí	k1gNnPc2	vádí
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgFnSc1d1	východní
část	část	k1gFnSc1	část
protíná	protínat	k5eAaImIp3nS	protínat
od	od	k7c2	od
jihu	jih	k1gInSc2	jih
k	k	k7c3	k
severu	sever	k1gInSc3	sever
řeka	řeka	k1gFnSc1	řeka
Nil	Nil	k1gInSc1	Nil
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
do	do	k7c2	do
pouště	poušť	k1gFnSc2	poušť
svým	svůj	k3xOyFgInSc7	svůj
severním	severní	k2eAgInSc7d1	severní
obloukem	oblouk	k1gInSc7	oblouk
řeka	řeka	k1gFnSc1	řeka
Niger	Niger	k1gInSc1	Niger
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bezodtokých	bezodtoký	k2eAgFnPc6d1	bezodtoká
oblastech	oblast	k1gFnPc6	oblast
na	na	k7c6	na
severu	sever	k1gInSc6	sever
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
slaná	slaný	k2eAgNnPc1d1	slané
jezera	jezero	k1gNnPc1	jezero
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
šoty	šot	k1gInPc1	šot
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
patří	patřit	k5eAaImIp3nS	patřit
Šot	šot	k1gInSc1	šot
Djerid	Djerid	k1gInSc1	Djerid
a	a	k8xC	a
Šot	šot	k1gInSc1	šot
Melrhir	Melrhira	k1gFnPc2	Melrhira
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klimatické	klimatický	k2eAgFnPc1d1	klimatická
podmínky	podmínka	k1gFnPc1	podmínka
==	==	k?	==
</s>
</p>
<p>
<s>
Klima	klima	k1gNnSc1	klima
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
ráz	ráz	k1gInSc1	ráz
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
ohledech	ohled	k1gInPc6	ohled
podobný	podobný	k2eAgMnSc1d1	podobný
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
navzdory	navzdory	k6eAd1	navzdory
očekáváním	očekávání	k1gNnSc7	očekávání
rozmanité	rozmanitý	k2eAgFnSc2d1	rozmanitá
díky	díky	k7c3	díky
rozsáhlosti	rozsáhlost	k1gFnSc3	rozsáhlost
Sahary	Sahara	k1gFnSc2	Sahara
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
pouštní	pouštní	k2eAgFnPc4d1	pouštní
oblasti	oblast	k1gFnPc4	oblast
jsou	být	k5eAaImIp3nP	být
typické	typický	k2eAgFnPc4d1	typická
velmi	velmi	k6eAd1	velmi
nízké	nízký	k2eAgFnPc4d1	nízká
srážky	srážka	k1gFnPc4	srážka
–	–	k?	–
na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
Asuánu	Asuán	k1gInSc6	Asuán
<g/>
)	)	kIx)	)
nemusí	muset	k5eNaImIp3nS	muset
pršet	pršet	k5eAaImF	pršet
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
za	za	k7c4	za
rok	rok	k1gInSc4	rok
není	být	k5eNaImIp3nS	být
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
100	[number]	k4	100
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
již	již	k9	již
srážky	srážka	k1gFnPc1	srážka
přijdou	přijít	k5eAaPmIp3nP	přijít
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
formu	forma	k1gFnSc4	forma
průtrže	průtrž	k1gFnSc2	průtrž
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
povodeň	povodeň	k1gFnSc4	povodeň
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
letní	letní	k2eAgFnSc1d1	letní
teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dne	den	k1gInSc2	den
šplhá	šplhat	k5eAaImIp3nS	šplhat
k	k	k7c3	k
50	[number]	k4	50
°	°	k?	°
<g/>
C	C	kA	C
ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
za	za	k7c4	za
nejteplejší	teplý	k2eAgFnSc4d3	nejteplejší
lze	lze	k6eAd1	lze
označit	označit	k5eAaPmF	označit
oblast	oblast	k1gFnSc4	oblast
mezi	mezi	k7c7	mezi
13	[number]	k4	13
<g/>
°	°	k?	°
až	až	k9	až
25	[number]	k4	25
<g/>
°	°	k?	°
rovnoběžkou	rovnoběžka	k1gFnSc7	rovnoběžka
<g/>
.	.	kIx.	.
</s>
<s>
Města	město	k1gNnPc1	město
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Asuán	Asuán	k1gInSc1	Asuán
<g/>
,	,	kIx,	,
Niamey	Niamey	k1gInPc1	Niamey
<g/>
,	,	kIx,	,
Bamako	Bamako	k1gNnSc1	Bamako
<g/>
,	,	kIx,	,
Chartúm	Chartúm	k1gInSc1	Chartúm
<g/>
,	,	kIx,	,
Džíbútí	Džíbútí	k1gNnPc1	Džíbútí
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
roční	roční	k2eAgFnSc4d1	roční
průměrnou	průměrný	k2eAgFnSc4d1	průměrná
teplotu	teplota	k1gFnSc4	teplota
okolo	okolo	k7c2	okolo
30	[number]	k4	30
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejteplejším	teplý	k2eAgNnPc3d3	nejteplejší
místům	místo	k1gNnPc3	místo
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
zmíněné	zmíněný	k2eAgFnSc2d1	zmíněná
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
zimní	zimní	k2eAgFnSc1d1	zimní
teplota	teplota	k1gFnSc1	teplota
neklesá	klesat	k5eNaImIp3nS	klesat
pod	pod	k7c4	pod
10	[number]	k4	10
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
severnější	severní	k2eAgFnSc6d2	severnější
části	část	k1gFnSc6	část
Sahary	Sahara	k1gFnSc2	Sahara
jsou	být	k5eAaImIp3nP	být
zimy	zima	k1gFnPc1	zima
mnohem	mnohem	k6eAd1	mnohem
chladnější	chladný	k2eAgFnPc1d2	chladnější
<g/>
,	,	kIx,	,
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
může	moct	k5eAaImIp3nS	moct
teplota	teplota	k1gFnSc1	teplota
klesnout	klesnout	k5eAaPmF	klesnout
i	i	k9	i
pod	pod	k7c4	pod
bod	bod	k1gInSc4	bod
mrazu	mráz	k1gInSc2	mráz
a	a	k8xC	a
průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
20	[number]	k4	20
°	°	k?	°
<g/>
C	C	kA	C
až	až	k9	až
25	[number]	k4	25
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
letní	letní	k2eAgFnSc1d1	letní
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgFnSc6d1	celá
oblasti	oblast	k1gFnSc6	oblast
Sahary	Sahara	k1gFnSc2	Sahara
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
nad	nad	k7c7	nad
30	[number]	k4	30
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Asuán	Asuán	k1gInSc4	Asuán
35	[number]	k4	35
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
Džíbútí	Džíbútí	k1gNnSc2	Džíbútí
36	[number]	k4	36
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
Dallol	Dallol	k1gInSc1	Dallol
(	(	kIx(	(
<g/>
sídlo	sídlo	k1gNnSc1	sídlo
v	v	k7c6	v
Etiopii	Etiopie	k1gFnSc6	Etiopie
<g/>
)	)	kIx)	)
39	[number]	k4	39
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
Adrar	Adrar	k1gInSc1	Adrar
(	(	kIx(	(
<g/>
Alžírsko	Alžírsko	k1gNnSc1	Alžírsko
<g/>
)	)	kIx)	)
38	[number]	k4	38
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
Tozeur	Tozeur	k1gMnSc1	Tozeur
(	(	kIx(	(
<g/>
Tunisko	Tunisko	k1gNnSc1	Tunisko
<g/>
)	)	kIx)	)
31	[number]	k4	31
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
Port	port	k1gInSc1	port
Sudan	sudan	k1gInSc1	sudan
35	[number]	k4	35
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
===	===	k?	===
Dezertifikace	Dezertifikace	k1gFnSc2	Dezertifikace
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
Sahary	Sahara	k1gFnSc2	Sahara
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
proces	proces	k1gInSc1	proces
desertifikace	desertifikace	k1gFnSc2	desertifikace
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
degradace	degradace	k1gFnSc1	degradace
stepí	step	k1gFnPc2	step
a	a	k8xC	a
savan	savana	k1gFnPc2	savana
v	v	k7c4	v
poušť	poušť	k1gFnSc4	poušť
nebo	nebo	k8xC	nebo
zasypávání	zasypávání	k1gNnSc4	zasypávání
míst	místo	k1gNnPc2	místo
s	s	k7c7	s
vegetací	vegetace	k1gFnSc7	vegetace
saharským	saharský	k2eAgInSc7d1	saharský
pískem	písek	k1gInSc7	písek
<g/>
.	.	kIx.	.
</s>
<s>
Dezertifikace	Dezertifikace	k1gFnSc1	Dezertifikace
je	být	k5eAaImIp3nS	být
důsledkem	důsledek	k1gInSc7	důsledek
nadměrného	nadměrný	k2eAgNnSc2d1	nadměrné
spásání	spásání	k1gNnSc2	spásání
vegetace	vegetace	k1gFnSc2	vegetace
dobytkem	dobytek	k1gInSc7	dobytek
a	a	k8xC	a
zvěří	zvěř	k1gFnSc7	zvěř
<g/>
,	,	kIx,	,
kácení	kácení	k1gNnSc4	kácení
zeleně	zeleň	k1gFnSc2	zeleň
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
vlivů	vliv	k1gInPc2	vliv
včetně	včetně	k7c2	včetně
klimatických	klimatický	k2eAgFnPc2d1	klimatická
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
se	s	k7c7	s
státy	stát	k1gInPc7	stát
oblasti	oblast	k1gFnSc2	oblast
Sahelu	Sahel	k1gInSc2	Sahel
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
OSN	OSN	kA	OSN
snaží	snažit	k5eAaImIp3nS	snažit
o	o	k7c6	o
zastavení	zastavení	k1gNnSc6	zastavení
nebo	nebo	k8xC	nebo
alespoň	alespoň	k9	alespoň
o	o	k7c4	o
zpomalení	zpomalení	k1gNnSc4	zpomalení
tohoto	tento	k3xDgInSc2	tento
procesu	proces	k1gInSc2	proces
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
výzkumů	výzkum	k1gInPc2	výzkum
archeologů	archeolog	k1gMnPc2	archeolog
a	a	k8xC	a
paleontologů	paleontolog	k1gMnPc2	paleontolog
bylo	být	k5eAaImAgNnS	být
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
5000	[number]	k4	5000
<g/>
–	–	k?	–
<g/>
7000	[number]	k4	7000
lety	léto	k1gNnPc7	léto
území	území	k1gNnSc2	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Sahary	Sahara	k1gFnSc2	Sahara
poměrně	poměrně	k6eAd1	poměrně
úrodnou	úrodný	k2eAgFnSc7d1	úrodná
oblastí	oblast	k1gFnSc7	oblast
<g/>
,	,	kIx,	,
vhodnou	vhodný	k2eAgFnSc7d1	vhodná
pro	pro	k7c4	pro
pěstování	pěstování	k1gNnSc4	pěstování
plodin	plodina	k1gFnPc2	plodina
a	a	k8xC	a
pastevectví	pastevectví	k1gNnSc2	pastevectví
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
pronikají	pronikat	k5eAaImIp3nP	pronikat
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
Sahary	Sahara	k1gFnSc2	Sahara
Arabové	Arab	k1gMnPc1	Arab
a	a	k8xC	a
šíří	šířit	k5eAaImIp3nP	šířit
zde	zde	k6eAd1	zde
islám	islám	k1gInSc4	islám
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
výrazně	výrazně	k6eAd1	výrazně
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
původní	původní	k2eAgInSc1d1	původní
etnika	etnikum	k1gNnSc2	etnikum
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
kulturu	kultura	k1gFnSc4	kultura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Díky	díky	k7c3	díky
precesnímu	precesní	k2eAgInSc3d1	precesní
cyklu	cyklus	k1gInSc3	cyklus
dlouhému	dlouhý	k2eAgInSc3d1	dlouhý
25	[number]	k4	25
800	[number]	k4	800
let	léto	k1gNnPc2	léto
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
vlhké	vlhký	k2eAgNnSc1d1	vlhké
monzunové	monzunový	k2eAgNnSc1d1	monzunové
období	období	k1gNnSc1	období
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
15	[number]	k4	15
000	[number]	k4	000
let	léto	k1gNnPc2	léto
na	na	k7c4	na
Saharu	Sahara	k1gFnSc4	Sahara
opět	opět	k6eAd1	opět
vrátit	vrátit	k5eAaPmF	vrátit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
tvoří	tvořit	k5eAaImIp3nP	tvořit
několik	několik	k4yIc4	několik
etnických	etnický	k2eAgFnPc2d1	etnická
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
patří	patřit	k5eAaImIp3nP	patřit
především	především	k9	především
Tuaregové	Tuareg	k1gMnPc1	Tuareg
<g/>
,	,	kIx,	,
Berbeři	Berber	k1gMnPc1	Berber
<g/>
,	,	kIx,	,
Arabové	Arab	k1gMnPc1	Arab
a	a	k8xC	a
v	v	k7c6	v
hodně	hodně	k6eAd1	hodně
menší	malý	k2eAgFnSc3d2	menší
míře	míra	k1gFnSc3	míra
i	i	k9	i
některé	některý	k3yIgMnPc4	některý
černošské	černošský	k2eAgMnPc4d1	černošský
kmeny	kmen	k1gInPc4	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejhustěji	husto	k6eAd3	husto
obydlené	obydlený	k2eAgFnSc6d1	obydlená
oblasti	oblast	k1gFnSc6	oblast
patří	patřit	k5eAaImIp3nS	patřit
pobřeží	pobřeží	k1gNnSc4	pobřeží
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
oblast	oblast	k1gFnSc4	oblast
dolního	dolní	k2eAgInSc2d1	dolní
toku	tok	k1gInSc2	tok
Nilu	Nil	k1gInSc2	Nil
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgNnSc1d1	severní
pobřeží	pobřeží	k1gNnSc1	pobřeží
je	být	k5eAaImIp3nS	být
poseto	posít	k5eAaPmNgNnS	posít
památkami	památka	k1gFnPc7	památka
z	z	k7c2	z
období	období	k1gNnSc2	období
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hospodářství	hospodářství	k1gNnSc1	hospodářství
==	==	k?	==
</s>
</p>
<p>
<s>
Hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
život	život	k1gInSc1	život
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
části	část	k1gFnSc2	část
Sahary	Sahara	k1gFnSc2	Sahara
je	být	k5eAaImIp3nS	být
soustředěn	soustředit	k5eAaPmNgInS	soustředit
do	do	k7c2	do
řady	řada	k1gFnSc2	řada
oáz	oáza	k1gFnPc2	oáza
s	s	k7c7	s
artézskými	artézský	k2eAgFnPc7d1	artézská
studnami	studna	k1gFnPc7	studna
<g/>
.	.	kIx.	.
</s>
<s>
Pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
např.	např.	kA	např.
datlová	datlový	k2eAgFnSc1d1	datlová
palma	palma	k1gFnSc1	palma
<g/>
,	,	kIx,	,
zelenina	zelenina	k1gFnSc1	zelenina
a	a	k8xC	a
obilniny	obilnina	k1gFnPc1	obilnina
<g/>
.	.	kIx.	.
</s>
<s>
Rozšířen	rozšířen	k2eAgInSc1d1	rozšířen
je	být	k5eAaImIp3nS	být
chov	chov	k1gInSc1	chov
velbloudů	velbloud	k1gMnPc2	velbloud
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
oblast	oblast	k1gFnSc1	oblast
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgNnSc7d1	významné
nalezištěm	naleziště	k1gNnSc7	naleziště
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
,	,	kIx,	,
železné	železný	k2eAgFnSc2d1	železná
rudy	ruda	k1gFnSc2	ruda
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
nerostných	nerostný	k2eAgFnPc2d1	nerostná
surovin	surovina	k1gFnPc2	surovina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
živočichů	živočich	k1gMnPc2	živočich
==	==	k?	==
</s>
</p>
<p>
<s>
Gepard	gepard	k1gMnSc1	gepard
štíhlý	štíhlý	k2eAgMnSc1d1	štíhlý
(	(	kIx(	(
<g/>
Acinonyx	Acinonyx	k1gInSc1	Acinonyx
jubatus	jubatus	k1gInSc1	jubatus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
poddruh	poddruh	k1gInSc1	poddruh
g.	g.	k?	g.
š.	š.	k?	š.
severoafrický	severoafrický	k2eAgInSc1d1	severoafrický
(	(	kIx(	(
<g/>
Acinonyx	Acinonyx	k1gInSc1	Acinonyx
jubatus	jubatus	k1gInSc1	jubatus
hecki	heck	k1gFnSc2	heck
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dle	dle	k7c2	dle
IUCN	IUCN	kA	IUCN
kriticky	kriticky	k6eAd1	kriticky
ohrožený	ohrožený	k2eAgMnSc1d1	ohrožený
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
bledší	bledý	k2eAgFnSc1d2	bledší
a	a	k8xC	a
kratší	krátký	k2eAgFnSc1d2	kratší
srst	srst	k1gFnSc1	srst
než	než	k8xS	než
poddruhy	poddruh	k1gInPc1	poddruh
žijící	žijící	k2eAgInPc1d1	žijící
v	v	k7c6	v
savaně	savana	k1gFnSc6	savana
<g/>
,	,	kIx,	,
štíhlejší	štíhlý	k2eAgNnSc4d2	štíhlejší
tělo	tělo	k1gNnSc4	tělo
a	a	k8xC	a
menší	malý	k2eAgFnSc4d2	menší
hlavu	hlava	k1gFnSc4	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
žije	žít	k5eAaImIp3nS	žít
pouhých	pouhý	k2eAgInPc2d1	pouhý
200	[number]	k4	200
<g/>
–	–	k?	–
<g/>
250	[number]	k4	250
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Antilopa	antilopa	k1gFnSc1	antilopa
addax	addax	k1gInSc1	addax
(	(	kIx(	(
<g/>
Addax	Addax	k1gInSc1	Addax
nasomaculatus	nasomaculatus	k1gInSc1	nasomaculatus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dle	dle	k7c2	dle
IUCN	IUCN	kA	IUCN
kriticky	kriticky	k6eAd1	kriticky
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
je	být	k5eAaImIp3nS	být
adax	adax	k1gInSc1	adax
téměř	téměř	k6eAd1	téměř
vyhubený	vyhubený	k2eAgInSc1d1	vyhubený
<g/>
,	,	kIx,	,
přežívá	přežívat	k5eAaImIp3nS	přežívat
posledních	poslední	k2eAgInPc2d1	poslední
200	[number]	k4	200
<g/>
–	–	k?	–
<g/>
300	[number]	k4	300
kusů	kus	k1gInPc2	kus
v	v	k7c6	v
subsaharské	subsaharský	k2eAgFnSc6d1	subsaharská
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
přizpůsoben	přizpůsobit	k5eAaPmNgInS	přizpůsobit
životu	život	k1gInSc3	život
v	v	k7c6	v
poušti	poušť	k1gFnSc6	poušť
<g/>
,	,	kIx,	,
vydrží	vydržet	k5eAaPmIp3nS	vydržet
bez	bez	k7c2	bez
vody	voda	k1gFnSc2	voda
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
extrémními	extrémní	k2eAgFnPc7d1	extrémní
teplotami	teplota	k1gFnPc7	teplota
a	a	k8xC	a
úhrnem	úhrn	k1gInSc7	úhrn
srážek	srážka	k1gFnPc2	srážka
do	do	k7c2	do
100	[number]	k4	100
mm	mm	kA	mm
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fenek	fenek	k1gMnSc1	fenek
berberský	berberský	k2eAgMnSc1d1	berberský
(	(	kIx(	(
<g/>
Vulpes	Vulpes	k1gMnSc1	Vulpes
zerda	zerda	k1gMnSc1	zerda
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dokonale	dokonale	k6eAd1	dokonale
přizpůsoben	přizpůsoben	k2eAgInSc1d1	přizpůsoben
životu	život	k1gInSc3	život
na	na	k7c6	na
Sahaře	Sahara	k1gFnSc6	Sahara
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
preferuje	preferovat	k5eAaImIp3nS	preferovat
písčitou	písčitý	k2eAgFnSc4d1	písčitá
poušť	poušť	k1gFnSc4	poušť
(	(	kIx(	(
<g/>
obývá	obývat	k5eAaImIp3nS	obývat
stabilní	stabilní	k2eAgFnPc4d1	stabilní
písečné	písečný	k2eAgFnPc4d1	písečná
duny	duna	k1gFnPc4	duna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
zaměňován	zaměňován	k2eAgMnSc1d1	zaměňován
s	s	k7c7	s
větší	veliký	k2eAgFnSc7d2	veliký
liškou	liška	k1gFnSc7	liška
pouštní	pouštní	k2eAgInSc4d1	pouštní
(	(	kIx(	(
<g/>
Vulpes	Vulpes	k1gInSc4	Vulpes
rueppelli	rueppelle	k1gFnSc4	rueppelle
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
větší	veliký	k2eAgFnSc7d2	veliký
liškou	liška	k1gFnSc7	liška
písečnou	písečný	k2eAgFnSc7d1	písečná
(	(	kIx(	(
<g/>
Vulpes	Vulpesa	k1gFnPc2	Vulpesa
pallida	pallida	k1gFnSc1	pallida
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zmije	zmije	k1gFnSc1	zmije
rohatá	rohatý	k2eAgFnSc1d1	rohatá
(	(	kIx(	(
<g/>
Cerastes	Cerastes	k1gInSc1	Cerastes
vipera	vipero	k1gNnSc2	vipero
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
také	také	k9	také
nazývána	nazýván	k2eAgFnSc1d1	nazývána
zmije	zmije	k1gFnSc1	zmije
saharská	saharský	k2eAgFnSc1d1	saharská
<g/>
,	,	kIx,	,
měří	měřit	k5eAaImIp3nS	měřit
asi	asi	k9	asi
30	[number]	k4	30
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vázána	vázat	k5eAaImNgFnS	vázat
na	na	k7c4	na
písečné	písečný	k2eAgFnPc4d1	písečná
pouště	poušť	k1gFnPc4	poušť
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgFnSc2d1	celá
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Aktivnější	aktivní	k2eAgFnSc1d2	aktivnější
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
a	a	k8xC	a
ráno	ráno	k6eAd1	ráno
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
času	čas	k1gInSc2	čas
je	být	k5eAaImIp3nS	být
zahrabána	zahrabat	k5eAaPmNgFnS	zahrabat
těsně	těsně	k6eAd1	těsně
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
písku	písek	k1gInSc2	písek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
číhá	číhat	k5eAaImIp3nS	číhat
na	na	k7c4	na
kořist	kořist	k1gFnSc4	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Okolí	okolí	k1gNnSc1	okolí
pozoruje	pozorovat	k5eAaImIp3nS	pozorovat
navrch	navrch	k6eAd1	navrch
vystouplýma	vystouplý	k2eAgNnPc7d1	vystouplé
očima	oko	k1gNnPc7	oko
<g/>
.	.	kIx.	.
</s>
<s>
Útočí	útočit	k5eAaImIp3nS	útočit
náhle	náhle	k6eAd1	náhle
<g/>
.	.	kIx.	.
</s>
<s>
Kousnutí	kousnutí	k1gNnSc1	kousnutí
je	být	k5eAaImIp3nS	být
bolestivé	bolestivý	k2eAgNnSc1d1	bolestivé
<g/>
,	,	kIx,	,
málokdy	málokdy	k6eAd1	málokdy
ohrožující	ohrožující	k2eAgMnSc1d1	ohrožující
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
<g/>
:	:	kIx,	:
Mauritánie	Mauritánie	k1gFnSc1	Mauritánie
<g/>
,	,	kIx,	,
Maroko	Maroko	k1gNnSc1	Maroko
<g/>
,	,	kIx,	,
Alžírsko	Alžírsko	k1gNnSc1	Alžírsko
<g/>
,	,	kIx,	,
Libye	Libye	k1gFnSc1	Libye
<g/>
,	,	kIx,	,
Tunisko	Tunisko	k1gNnSc1	Tunisko
<g/>
,	,	kIx,	,
Mali	Mali	k1gNnSc1	Mali
<g/>
,	,	kIx,	,
Niger	Niger	k1gInSc1	Niger
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Varan	varan	k1gMnSc1	varan
pustinný	pustinný	k2eAgMnSc1d1	pustinný
(	(	kIx(	(
<g/>
Varanus	Varanus	k1gMnSc1	Varanus
griseus	griseus	k1gMnSc1	griseus
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
dorůst	dorůst	k5eAaPmF	dorůst
délky	délka	k1gFnSc2	délka
1,5	[number]	k4	1,5
až	až	k9	až
2	[number]	k4	2
m.	m.	k?	m.
Samci	Samek	k1gMnPc1	Samek
bojují	bojovat	k5eAaImIp3nP	bojovat
o	o	k7c4	o
právo	právo	k1gNnSc4	právo
pářit	pářit	k5eAaImF	pářit
se	se	k3xPyFc4	se
–	–	k?	–
vzpřímí	vzpřímit	k5eAaPmIp3nS	vzpřímit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
strkají	strkat	k5eAaImIp3nP	strkat
do	do	k7c2	do
sebe	se	k3xPyFc2	se
předními	přední	k2eAgFnPc7d1	přední
končetinami	končetina	k1gFnPc7	končetina
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nP	snažit
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
převalit	převalit	k5eAaPmF	převalit
<g/>
.	.	kIx.	.
</s>
<s>
Údajně	údajně	k6eAd1	údajně
kvůli	kvůli	k7c3	kvůli
postoji	postoj	k1gInSc3	postoj
na	na	k7c6	na
zadních	zadní	k2eAgFnPc6d1	zadní
končetinách	končetina	k1gFnPc6	končetina
a	a	k8xC	a
sledování	sledování	k1gNnSc4	sledování
okolí	okolí	k1gNnSc2	okolí
získal	získat	k5eAaPmAgMnS	získat
původní	původní	k2eAgMnSc1d1	původní
arabské	arabský	k2eAgNnSc4d1	arabské
jméno	jméno	k1gNnSc4	jméno
waral	waral	k1gMnSc1	waral
و	و	k?	و
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
volně	volně	k6eAd1	volně
přeloženo	přeložen	k2eAgNnSc1d1	přeloženo
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Štír	štír	k1gMnSc1	štír
nejjedovatější	jedovatý	k2eAgMnSc1d3	nejjedovatější
(	(	kIx(	(
<g/>
Leiurus	Leiurus	k1gMnSc1	Leiurus
quinquestriatus	quinquestriatus	k1gMnSc1	quinquestriatus
<g/>
)	)	kIx)	)
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
délky	délka	k1gFnPc4	délka
do	do	k7c2	do
10	[number]	k4	10
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
prudký	prudký	k2eAgInSc1d1	prudký
jed	jed	k1gInSc1	jed
srovnatelný	srovnatelný	k2eAgInSc1d1	srovnatelný
s	s	k7c7	s
toxinem	toxin	k1gInSc7	toxin
kobry	kobra	k1gFnSc2	kobra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
protože	protože	k8xS	protože
kapacita	kapacita	k1gFnSc1	kapacita
jedového	jedový	k2eAgInSc2d1	jedový
váčku	váček	k1gInSc2	váček
uvnitř	uvnitř	k7c2	uvnitř
telsonu	telson	k1gInSc2	telson
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
<g/>
,	,	kIx,	,
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
méně	málo	k6eAd2	málo
úmrtí	úmrtí	k1gNnSc2	úmrtí
než	než	k8xS	než
štír	štír	k1gMnSc1	štír
tlustorepý	tlustorepý	k2eAgMnSc1d1	tlustorepý
(	(	kIx(	(
<g/>
Androctonus	Androctonus	k1gInSc1	Androctonus
australis	australis	k1gFnSc2	australis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
tak	tak	k9	tak
často	často	k6eAd1	často
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
člověka	člověk	k1gMnSc2	člověk
jako	jako	k8xC	jako
štír	štír	k1gMnSc1	štír
tlustorepý	tlustorepý	k2eAgMnSc1d1	tlustorepý
a	a	k8xC	a
nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
písečných	písečný	k2eAgFnPc6d1	písečná
dunách	duna	k1gFnPc6	duna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Štír	štír	k1gMnSc1	štír
tlustorepý	tlustorepý	k2eAgMnSc1d1	tlustorepý
(	(	kIx(	(
<g/>
Androctonus	Androctonus	k1gInSc1	Androctonus
australis	australis	k1gFnSc2	australis
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
často	často	k6eAd1	často
zahrabává	zahrabávat	k5eAaImIp3nS	zahrabávat
do	do	k7c2	do
písku	písek	k1gInSc2	písek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
bosí	bosý	k2eAgMnPc1d1	bosý
domorodci	domorodec	k1gMnPc1	domorodec
mohou	moct	k5eAaImIp3nP	moct
nevědomky	nevědomky	k6eAd1	nevědomky
šlápnout	šlápnout	k5eAaPmF	šlápnout
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
kapacita	kapacita	k1gFnSc1	kapacita
jedového	jedový	k2eAgInSc2d1	jedový
váčku	váček	k1gInSc2	váček
uvnitř	uvnitř	k7c2	uvnitř
telsonu	telson	k1gInSc2	telson
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
<g/>
,	,	kIx,	,
80	[number]	k4	80
%	%	kIx~	%
úmrtí	úmrtí	k1gNnSc2	úmrtí
po	po	k7c6	po
bodnutí	bodnutí	k1gNnSc1	bodnutí
štírem	štír	k1gMnSc7	štír
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
právě	právě	k9	právě
štírem	štír	k1gMnSc7	štír
tlustorepým	tlustorepý	k2eAgMnSc7d1	tlustorepý
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
jeho	jeho	k3xOp3gInSc1	jeho
jed	jed	k1gInSc1	jed
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
prudký	prudký	k2eAgInSc1d1	prudký
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
<g/>
Zdomácnělými	zdomácnělý	k2eAgNnPc7d1	zdomácnělé
zvířaty	zvíře	k1gNnPc7	zvíře
jsou	být	k5eAaImIp3nP	být
velbloudi	velbloud	k1gMnPc1	velbloud
jednohrbí	jednohrbý	k2eAgMnPc1d1	jednohrbý
(	(	kIx(	(
<g/>
dromedáři	dromedár	k1gMnPc1	dromedár
<g/>
)	)	kIx)	)
a	a	k8xC	a
kozy	koza	k1gFnSc2	koza
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
svým	svůj	k3xOyFgFnPc3	svůj
kvalitám	kvalita	k1gFnPc3	kvalita
(	(	kIx(	(
<g/>
trpělivosti	trpělivost	k1gFnSc3	trpělivost
<g/>
,	,	kIx,	,
výdrži	výdrž	k1gFnSc3	výdrž
a	a	k8xC	a
rychlosti	rychlost	k1gFnSc3	rychlost
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
velbloudi	velbloud	k1gMnPc1	velbloud
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
dopravním	dopravní	k2eAgInSc7d1	dopravní
prostředkem	prostředek	k1gInSc7	prostředek
nomádů	nomád	k1gMnPc2	nomád
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
chováni	chovat	k5eAaImNgMnP	chovat
jako	jako	k9	jako
jezdecká	jezdecký	k2eAgNnPc4d1	jezdecké
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
,	,	kIx,	,
nosiči	nosič	k1gInSc6	nosič
nákladů	náklad	k1gInPc2	náklad
i	i	k9	i
pro	pro	k7c4	pro
maso	maso	k1gNnSc4	maso
a	a	k8xC	a
kvalitní	kvalitní	k2eAgFnSc4d1	kvalitní
vlnu	vlna	k1gFnSc4	vlna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Poušť	poušť	k1gFnSc1	poušť
</s>
</p>
<p>
<s>
Regiony	region	k1gInPc1	region
Afriky	Afrika	k1gFnSc2	Afrika
</s>
</p>
<p>
<s>
Severní	severní	k2eAgFnSc1d1	severní
Afrika	Afrika	k1gFnSc1	Afrika
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Sahara	Sahara	k1gFnSc1	Sahara
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Sahara	Sahara	k1gFnSc1	Sahara
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Sahara	Sahara	k1gFnSc1	Sahara
:	:	kIx,	:
Flora	Flora	k1gFnSc1	Flora
–	–	k?	–
Fauna	fauna	k1gFnSc1	fauna
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
ze	z	k7c2	z
Sahary	Sahara	k1gFnSc2	Sahara
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
Scientific	Scientifice	k1gFnPc2	Scientifice
American	American	k1gInSc1	American
Blog	Blog	k1gMnSc1	Blog
Network	network	k1gInSc1	network
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
BioLib	BioLiba	k1gFnPc2	BioLiba
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
]	]	kIx)	]
</s>
</p>
