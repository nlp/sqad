<s>
Hlava	hlava	k1gFnSc1	hlava
je	být	k5eAaImIp3nS	být
horní	horní	k2eAgFnSc1d1	horní
část	část	k1gFnSc1	část
lidského	lidský	k2eAgNnSc2d1	lidské
těla	tělo	k1gNnSc2	tělo
nad	nad	k7c7	nad
krkem	krk	k1gInSc7	krk
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
většina	většina	k1gFnSc1	většina
smyslových	smyslový	k2eAgInPc2d1	smyslový
orgánů	orgán	k1gInPc2	orgán
společně	společně	k6eAd1	společně
s	s	k7c7	s
centrem	centrum	k1gNnSc7	centrum
nervové	nervový	k2eAgFnSc2d1	nervová
soustavy	soustava	k1gFnSc2	soustava
-	-	kIx~	-
mozkem	mozek	k1gInSc7	mozek
<g/>
.	.	kIx.	.
</s>
