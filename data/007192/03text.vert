<s>
Hlava	hlava	k1gFnSc1	hlava
je	být	k5eAaImIp3nS	být
horní	horní	k2eAgFnSc1d1	horní
část	část	k1gFnSc1	část
lidského	lidský	k2eAgNnSc2d1	lidské
těla	tělo	k1gNnSc2	tělo
nad	nad	k7c7	nad
krkem	krk	k1gInSc7	krk
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
většina	většina	k1gFnSc1	většina
smyslových	smyslový	k2eAgInPc2d1	smyslový
orgánů	orgán	k1gInPc2	orgán
společně	společně	k6eAd1	společně
s	s	k7c7	s
centrem	centrum	k1gNnSc7	centrum
nervové	nervový	k2eAgFnSc2d1	nervová
soustavy	soustava	k1gFnSc2	soustava
-	-	kIx~	-
mozkem	mozek	k1gInSc7	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
centra	centrum	k1gNnSc2	centrum
zraku	zrak	k1gInSc2	zrak
(	(	kIx(	(
<g/>
oči	oko	k1gNnPc1	oko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sluchu	sluch	k1gInSc3	sluch
(	(	kIx(	(
<g/>
uši	ucho	k1gNnPc1	ucho
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chuti	chuť	k1gFnSc2	chuť
(	(	kIx(	(
<g/>
chuťové	chuťový	k2eAgInPc1d1	chuťový
pohárky	pohárek	k1gInPc1	pohárek
na	na	k7c6	na
jazyku	jazyk	k1gInSc6	jazyk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čichu	čich	k1gInSc2	čich
(	(	kIx(	(
<g/>
nos	nos	k1gInSc1	nos
<g/>
)	)	kIx)	)
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
jemné	jemný	k2eAgNnSc4d1	jemné
hmatové	hmatový	k2eAgNnSc4d1	hmatové
zakončení	zakončení	k1gNnSc4	zakončení
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
obličeji	obličej	k1gInSc6	obličej
(	(	kIx(	(
<g/>
nejvíce	nejvíce	k6eAd1	nejvíce
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
úst	ústa	k1gNnPc2	ústa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
těchto	tento	k3xDgNnPc2	tento
center	centrum	k1gNnPc2	centrum
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
obličeje	obličej	k1gInSc2	obličej
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
i	i	k9	i
otvor	otvor	k1gInSc4	otvor
pro	pro	k7c4	pro
přijímání	přijímání	k1gNnSc4	přijímání
potravy	potrava	k1gFnSc2	potrava
(	(	kIx(	(
<g/>
ústa	ústa	k1gNnPc1	ústa
<g/>
)	)	kIx)	)
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
pro	pro	k7c4	pro
dýchání	dýchání	k1gNnSc4	dýchání
(	(	kIx(	(
<g/>
nos	nos	k1gInSc4	nos
a	a	k8xC	a
ústa	ústa	k1gNnPc4	ústa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
z	z	k7c2	z
28	[number]	k4	28
samostatných	samostatný	k2eAgFnPc2d1	samostatná
kostí	kost	k1gFnPc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
kosti	kost	k1gFnPc1	kost
dohromady	dohromady	k6eAd1	dohromady
tvoří	tvořit	k5eAaImIp3nP	tvořit
lebku	lebka	k1gFnSc4	lebka
<g/>
.	.	kIx.	.
</s>
<s>
Nejhornější	horní	k2eAgFnSc1d3	nejhornější
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
nejpevnější	pevný	k2eAgFnSc7d3	nejpevnější
oblastí	oblast	k1gFnSc7	oblast
je	být	k5eAaImIp3nS	být
horní	horní	k2eAgFnSc1d1	horní
část	část	k1gFnSc1	část
lebky	lebka	k1gFnSc2	lebka
zvaná	zvaný	k2eAgFnSc1d1	zvaná
kranium	kranium	k1gNnSc1	kranium
čili	čili	k8xC	čili
mozková	mozkový	k2eAgFnSc1d1	mozková
schránka	schránka	k1gFnSc1	schránka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mozek	mozek	k1gInSc1	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Mimika	mimika	k1gFnSc1	mimika
obličeje	obličej	k1gInSc2	obličej
je	být	k5eAaImIp3nS	být
zajišťována	zajišťovat	k5eAaImNgFnS	zajišťovat
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
30	[number]	k4	30
svalů	sval	k1gInPc2	sval
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
lidem	člověk	k1gMnPc3	člověk
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
emoce	emoce	k1gFnPc1	emoce
pomocí	pomocí	k7c2	pomocí
změn	změna	k1gFnPc2	změna
v	v	k7c6	v
obličeji	obličej	k1gInSc6	obličej
(	(	kIx(	(
<g/>
např.	např.	kA	např.
úsměv	úsměv	k1gInSc4	úsměv
či	či	k8xC	či
mračení	mračení	k1gNnSc4	mračení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
hmotnost	hmotnost	k1gFnSc1	hmotnost
hlavy	hlava	k1gFnSc2	hlava
dospělého	dospělý	k2eAgMnSc4d1	dospělý
člověka	člověk	k1gMnSc4	člověk
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
4,6	[number]	k4	4,6
kilogramů	kilogram	k1gInPc2	kilogram
a	a	k8xC	a
hustota	hustota	k1gFnSc1	hustota
cca	cca	kA	cca
1200	[number]	k4	1200
kg	kg	kA	kg
<g/>
.	.	kIx.	.
<g/>
m-	m-	k?	m-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
se	se	k3xPyFc4	se
podél	podél	k7c2	podél
osy	osa	k1gFnSc2	osa
páteře	páteř	k1gFnSc2	páteř
může	moct	k5eAaImIp3nS	moct
otáčet	otáčet	k5eAaImF	otáčet
o	o	k7c4	o
úhel	úhel	k1gInSc4	úhel
asi	asi	k9	asi
150	[number]	k4	150
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgFnSc1d1	horní
část	část	k1gFnSc1	část
hlavy	hlava	k1gFnSc2	hlava
je	být	k5eAaImIp3nS	být
chráněna	chránit	k5eAaImNgFnS	chránit
vlasy	vlas	k1gInPc7	vlas
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k8xC	jako
přirozená	přirozený	k2eAgFnSc1d1	přirozená
termoregulace	termoregulace	k1gFnSc1	termoregulace
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
musculi	muscule	k1gFnSc3	muscule
capitis	capitis	k1gFnSc1	capitis
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
2	[number]	k4	2
skupiny	skupina	k1gFnSc2	skupina
:	:	kIx,	:
svaly	sval	k1gInPc1	sval
žvýkací	žvýkací	k2eAgFnSc2d1	žvýkací
a	a	k8xC	a
mimické	mimický	k2eAgFnSc2d1	mimická
(	(	kIx(	(
<g/>
m.	m.	k?	m.
masticatorii	masticatorie	k1gFnSc4	masticatorie
<g/>
,	,	kIx,	,
m.	m.	k?	m.
faciales	faciales	k1gMnSc1	faciales
<g/>
)	)	kIx)	)
M.	M.	kA	M.
masticatorii	masticatorie	k1gFnSc6	masticatorie
<g/>
:	:	kIx,	:
m.	m.	k?	m.
temporalis	temporalis	k1gInSc1	temporalis
(	(	kIx(	(
<g/>
sval	sval	k1gInSc1	sval
spánkový	spánkový	k2eAgInSc1d1	spánkový
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
m.	m.	k?	m.
masseter	masseter	k1gInSc1	masseter
(	(	kIx(	(
<g/>
zevní	zevní	k2eAgInSc1d1	zevní
sval	sval	k1gInSc1	sval
žvýkací	žvýkací	k2eAgInSc1d1	žvýkací
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
m.	m.	k?	m.
pterygoideus	pterygoideus	k1gInSc1	pterygoideus
<g />
.	.	kIx.	.
</s>
<s>
medialis	medialis	k1gFnSc1	medialis
<g/>
,	,	kIx,	,
m.	m.	k?	m.
pterygoideus	pterygoideus	k1gInSc1	pterygoideus
lateralis	lateralis	k1gInSc1	lateralis
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
M.	M.	kA	M.
faciales	faciales	k1gMnSc1	faciales
<g/>
:	:	kIx,	:
m.	m.	k?	m.
orbicularis	orbicularis	k1gInSc1	orbicularis
oculi	ocule	k1gFnSc3	ocule
(	(	kIx(	(
<g/>
oční	oční	k2eAgInSc4d1	oční
sval	sval	k1gInSc4	sval
kruhový	kruhový	k2eAgInSc4d1	kruhový
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
m.	m.	k?	m.
orbicularis	orbicularis	k1gInSc1	orbicularis
oris	oris	k1gInSc1	oris
(	(	kIx(	(
<g/>
kruhový	kruhový	k2eAgInSc4d1	kruhový
sval	sval	k1gInSc4	sval
ústní	ústní	k2eAgInSc4d1	ústní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
m.	m.	k?	m.
nasalis	nasalis	k1gInSc1	nasalis
(	(	kIx(	(
<g/>
sval	sval	k1gInSc1	sval
nosu	nos	k1gInSc2	nos
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
m.	m.	k?	m.
buccinator	buccinator	k1gInSc1	buccinator
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
sval	sval	k1gInSc1	sval
tvářový	tvářový	k2eAgInSc1d1	tvářový
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
Např.	např.	kA	např.
platysma	platysma	k1gFnSc1	platysma
(	(	kIx(	(
<g/>
plochý	plochý	k2eAgInSc1d1	plochý
podkožní	podkožní	k2eAgInSc1d1	podkožní
sval	sval	k1gInSc1	sval
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
m.	m.	k?	m.
suprahyoidei	suprahyoide	k1gFnPc1	suprahyoide
(	(	kIx(	(
<g/>
svaly	sval	k1gInPc1	sval
nadjazykové	nadjazyková	k1gFnSc2	nadjazyková
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
m.	m.	k?	m.
infrahyoidei	infrahyoide	k1gFnPc1	infrahyoide
(	(	kIx(	(
<g/>
svaly	sval	k1gInPc1	sval
podjazykové	podjazykový	k2eAgInPc1d1	podjazykový
<g/>
)	)	kIx)	)
Hlava	hlava	k1gFnSc1	hlava
–	–	k?	–
obecnější	obecní	k2eAgInSc4d2	obecní
článek	článek	k1gInSc4	článek
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
o	o	k7c6	o
lidské	lidský	k2eAgFnSc6d1	lidská
hlavě	hlava	k1gFnSc6	hlava
Hlava	hlava	k1gFnSc1	hlava
(	(	kIx(	(
<g/>
rozcestník	rozcestník	k1gInSc1	rozcestník
<g/>
)	)	kIx)	)
–	–	k?	–
rozcestník	rozcestník	k1gInSc1	rozcestník
s	s	k7c7	s
přenesenými	přenesený	k2eAgInPc7d1	přenesený
významy	význam	k1gInPc7	význam
slova	slovo	k1gNnSc2	slovo
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
hlava	hlava	k1gFnSc1	hlava
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
