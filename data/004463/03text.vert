<s>
Lysá	lysý	k2eAgFnSc1d1	Lysá
hora	hora	k1gFnSc1	hora
(	(	kIx(	(
<g/>
1323	[number]	k4	1323
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
zdrojích	zdroj	k1gInPc6	zdroj
1324	[number]	k4	1324
m.	m.	k?	m.
n.	n.	k?	n.
m.	m.	k?	m.
-	-	kIx~	-
o	o	k7c4	o
výšku	výška	k1gFnSc4	výška
se	se	k3xPyFc4	se
již	již	k6eAd1	již
dlouho	dlouho	k6eAd1	dlouho
vedou	vést	k5eAaImIp3nP	vést
spory	spor	k1gInPc1	spor
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
vrchol	vrchol	k1gInSc4	vrchol
Moravskoslezských	moravskoslezský	k2eAgFnPc2d1	Moravskoslezská
Beskyd	Beskydy	k1gFnPc2	Beskydy
a	a	k8xC	a
Těšínska	Těšínsko	k1gNnSc2	Těšínsko
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c4	na
rozhraní	rozhraní	k1gNnSc4	rozhraní
obcí	obec	k1gFnPc2	obec
Krásná	krásný	k2eAgFnSc1d1	krásná
<g/>
,	,	kIx,	,
Malenovice	Malenovice	k1gFnSc1	Malenovice
<g/>
,	,	kIx,	,
Ostravice	Ostravice	k1gFnSc1	Ostravice
a	a	k8xC	a
Staré	Staré	k2eAgInPc1d1	Staré
Hamry	Hamry	k1gInPc1	Hamry
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vrch	vrch	k1gInSc1	vrch
byl	být	k5eAaImAgInS	být
také	také	k9	také
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1939-1945	[number]	k4	1939-1945
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
Protektorátu	protektorát	k1gInSc2	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
holém	holý	k2eAgMnSc6d1	holý
(	(	kIx(	(
<g/>
lysém	lysý	k2eAgInSc6d1	lysý
<g/>
)	)	kIx)	)
vrcholu	vrchol	k1gInSc6	vrchol
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yIgInSc4	který
vede	vést	k5eAaImIp3nS	vést
silnice	silnice	k1gFnSc1	silnice
z	z	k7c2	z
Krásné	krásný	k2eAgFnSc2d1	krásná
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
telekomunikační	telekomunikační	k2eAgInSc1d1	telekomunikační
vysílač	vysílač	k1gInSc1	vysílač
<g/>
,	,	kIx,	,
meteorologická	meteorologický	k2eAgFnSc1d1	meteorologická
stanice	stanice	k1gFnSc1	stanice
a	a	k8xC	a
několik	několik	k4yIc4	několik
horských	horský	k2eAgFnPc2d1	horská
chat	chata	k1gFnPc2	chata
<g/>
.	.	kIx.	.
</s>
<s>
Samotnému	samotný	k2eAgInSc3d1	samotný
vrcholu	vrchol	k1gInSc3	vrchol
se	se	k3xPyFc4	se
přezdívá	přezdívat	k5eAaImIp3nS	přezdívat
Gigula	Gigula	k1gFnSc1	Gigula
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vrcholu	vrchol	k1gInSc2	vrchol
Lysé	Lysé	k2eAgFnSc2d1	Lysé
hory	hora	k1gFnSc2	hora
vybíhají	vybíhat	k5eAaImIp3nP	vybíhat
čtyři	čtyři	k4xCgInPc4	čtyři
rozsochy	rozsoch	k1gInPc4	rozsoch
<g/>
:	:	kIx,	:
Malchor	Malchor	k1gMnSc1	Malchor
<g/>
,	,	kIx,	,
Zimný	zimný	k2eAgMnSc1d1	zimný
(	(	kIx(	(
<g/>
Zimní	zimní	k2eAgFnSc1d1	zimní
Polana	polana	k1gFnSc1	polana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kobylanka	Kobylanka	k1gFnSc1	Kobylanka
(	(	kIx(	(
<g/>
Velký	velký	k2eAgInSc1d1	velký
Kobylík	Kobylík	k1gInSc1	Kobylík
<g/>
)	)	kIx)	)
a	a	k8xC	a
Lukšinec	Lukšinec	k1gInSc4	Lukšinec
<g/>
.	.	kIx.	.
</s>
<s>
Vrchol	vrchol	k1gInSc1	vrchol
představuje	představovat	k5eAaImIp3nS	představovat
významnou	významný	k2eAgFnSc4d1	významná
křižovatku	křižovatka	k1gFnSc4	křižovatka
turistických	turistický	k2eAgFnPc2d1	turistická
cest	cesta	k1gFnPc2	cesta
a	a	k8xC	a
dvakrát	dvakrát	k6eAd1	dvakrát
týdně	týdně	k6eAd1	týdně
(	(	kIx(	(
<g/>
ve	v	k7c4	v
středu	středa	k1gFnSc4	středa
a	a	k8xC	a
pátek	pátek	k1gInSc4	pátek
<g/>
)	)	kIx)	)
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
dojíždí	dojíždět	k5eAaImIp3nS	dojíždět
autobusový	autobusový	k2eAgInSc1d1	autobusový
spoj	spoj	k1gInSc1	spoj
Raškovice	Raškovice	k1gFnSc2	Raškovice
–	–	k?	–
Lysá	Lysá	k1gFnSc1	Lysá
hora	hora	k1gFnSc1	hora
určený	určený	k2eAgInSc4d1	určený
původně	původně	k6eAd1	původně
pro	pro	k7c4	pro
tělesně	tělesně	k6eAd1	tělesně
postižené	postižený	k2eAgMnPc4d1	postižený
a	a	k8xC	a
důchodce	důchodce	k1gMnPc4	důchodce
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
využívaný	využívaný	k2eAgInSc1d1	využívaný
hojně	hojně	k6eAd1	hojně
i	i	k8xC	i
dalšími	další	k2eAgMnPc7d1	další
návštěvníky	návštěvník	k1gMnPc7	návštěvník
Beskyd	Beskydy	k1gFnPc2	Beskydy
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
masívu	masív	k1gInSc6	masív
Lysé	Lysá	k1gFnSc2	Lysá
hory	hora	k1gFnSc2	hora
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
několik	několik	k4yIc1	několik
přírodních	přírodní	k2eAgFnPc2d1	přírodní
rezervací	rezervace	k1gFnPc2	rezervace
a	a	k8xC	a
památek	památka	k1gFnPc2	památka
<g/>
:	:	kIx,	:
Mazák	mazák	k1gInSc1	mazák
a	a	k8xC	a
Mazácký	mazácký	k2eAgInSc1d1	mazácký
Grúnik	Grúnik	k1gInSc1	Grúnik
<g/>
,	,	kIx,	,
Malenovický	Malenovický	k2eAgInSc1d1	Malenovický
kotel	kotel	k1gInSc1	kotel
<g/>
,	,	kIx,	,
Vodopády	vodopád	k1gInPc1	vodopád
Satiny	Satin	k2eAgInPc1d1	Satin
<g/>
,	,	kIx,	,
Koryto	koryto	k1gNnSc1	koryto
řeky	řeka	k1gFnSc2	řeka
Ostravice	Ostravice	k1gFnSc2	Ostravice
<g/>
,	,	kIx,	,
Ondrášovy	Ondrášův	k2eAgFnSc2d1	Ondrášova
díry	díra	k1gFnSc2	díra
(	(	kIx(	(
<g/>
lašsky	lašsky	k6eAd1	lašsky
Ondrašovy	Ondrašův	k2eAgFnSc2d1	Ondrašův
ďury	ďura	k1gFnSc2	ďura
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnSc1d1	další
menší	malý	k2eAgFnSc1d2	menší
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
masívu	masív	k1gInSc2	masív
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
ochrany	ochrana	k1gFnSc2	ochrana
jsou	být	k5eAaImIp3nP	být
smrčiny	smrčina	k1gFnPc1	smrčina
s	s	k7c7	s
příměsí	příměs	k1gFnSc7	příměs
jedlobukových	jedlobukový	k2eAgInPc2d1	jedlobukový
porostů	porost	k1gInPc2	porost
a	a	k8xC	a
oblasti	oblast	k1gFnSc2	oblast
svahových	svahový	k2eAgInPc2d1	svahový
sesuvů	sesuv	k1gInPc2	sesuv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tzv.	tzv.	kA	tzv.
Ondrašovych	Ondrašovych	k1gMnSc1	Ondrašovych
ďurach	ďurach	k1gMnSc1	ďurach
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
pseudokrasové	pseudokrasové	k2eAgFnSc1d1	pseudokrasové
jeskyně	jeskyně	k1gFnSc1	jeskyně
a	a	k8xC	a
pukliny	puklina	k1gFnPc1	puklina
hojné	hojný	k2eAgFnPc1d1	hojná
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
částech	část	k1gFnPc6	část
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
potoka	potok	k1gInSc2	potok
Mazák	mazák	k1gInSc1	mazák
a	a	k8xC	a
Řehucí	Řehuce	k1gFnSc7	Řehuce
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
menší	malý	k2eAgInPc1d2	menší
skalní	skalní	k2eAgInPc1d1	skalní
útvary	útvar	k1gInPc1	útvar
a	a	k8xC	a
pro	pro	k7c4	pro
potok	potok	k1gInSc4	potok
Satinu	Satin	k1gInSc2	Satin
charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
vodopády	vodopád	k1gInPc1	vodopád
<g/>
.	.	kIx.	.
</s>
<s>
Koryto	koryto	k1gNnSc1	koryto
řeky	řeka	k1gFnSc2	řeka
Ostravice	Ostravice	k1gFnSc2	Ostravice
je	být	k5eAaImIp3nS	být
chráněno	chránit	k5eAaImNgNnS	chránit
kvůli	kvůli	k7c3	kvůli
geologické	geologický	k2eAgFnSc3d1	geologická
kvalitě	kvalita	k1gFnSc3	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Vrchol	vrchol	k1gInSc1	vrchol
Lysé	Lysé	k2eAgFnSc2d1	Lysé
hory	hora	k1gFnSc2	hora
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
vážně	vážně	k6eAd1	vážně
poškodila	poškodit	k5eAaPmAgFnS	poškodit
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
těžba	těžba	k1gFnSc1	těžba
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Lysá	lysý	k2eAgFnSc1d1	Lysá
hora	hora	k1gFnSc1	hora
je	být	k5eAaImIp3nS	být
vlivem	vliv	k1gInSc7	vliv
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
přesahující	přesahující	k2eAgInSc4d1	přesahující
1300	[number]	k4	1300
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejchladnějších	chladný	k2eAgNnPc2d3	nejchladnější
<g/>
,	,	kIx,	,
nejdeštivějších	deštivý	k2eAgNnPc2d3	nejdeštivější
a	a	k8xC	a
největrnějších	větrný	k2eAgNnPc2d3	největrnější
míst	místo	k1gNnPc2	místo
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
činí	činit	k5eAaImIp3nS	činit
2,6	[number]	k4	2,6
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
nejchladnějším	chladný	k2eAgInSc6d3	nejchladnější
lednu	leden	k1gInSc6	leden
je	být	k5eAaImIp3nS	být
průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
–	–	k?	–
<g/>
6,2	[number]	k4	6,2
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
v	v	k7c6	v
nejteplejším	teplý	k2eAgInSc6d3	nejteplejší
červenci	červenec	k1gInSc6	červenec
11,3	[number]	k4	11,3
°	°	k?	°
<g/>
C.	C.	kA	C.
Průměrné	průměrný	k2eAgFnPc1d1	průměrná
roční	roční	k2eAgFnPc1d1	roční
srážky	srážka	k1gFnPc1	srážka
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
hodnotu	hodnota	k1gFnSc4	hodnota
1390,8	[number]	k4	1390,8
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Nejdeštivější	deštivý	k2eAgInSc1d3	nejdeštivější
je	být	k5eAaImIp3nS	být
červenec	červenec	k1gInSc1	červenec
(	(	kIx(	(
<g/>
196,8	[number]	k4	196,8
mm	mm	kA	mm
<g/>
)	)	kIx)	)
a	a	k8xC	a
nejsušší	suchý	k2eAgInSc1d3	nejsušší
březen	březen	k1gInSc1	březen
(	(	kIx(	(
<g/>
76,8	[number]	k4	76,8
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lysá	lysý	k2eAgFnSc1d1	Lysá
hora	hora	k1gFnSc1	hora
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
bohatá	bohatý	k2eAgFnSc1d1	bohatá
na	na	k7c4	na
sníh	sníh	k1gInSc4	sníh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
kulminuje	kulminovat	k5eAaImIp3nS	kulminovat
sněhová	sněhový	k2eAgFnSc1d1	sněhová
pokrývka	pokrývka	k1gFnSc1	pokrývka
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
2	[number]	k4	2
m.	m.	k?	m.
Sněhová	sněhový	k2eAgFnSc1d1	sněhová
pokrývka	pokrývka	k1gFnSc1	pokrývka
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
každoročně	každoročně	k6eAd1	každoročně
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
do	do	k7c2	do
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Sněžení	sněžení	k1gNnSc1	sněžení
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
objevit	objevit	k5eAaPmF	objevit
i	i	k9	i
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
a	a	k8xC	a
srpnu	srpen	k1gInSc6	srpen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1880	[number]	k4	1880
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
Lysé	Lysá	k1gFnSc6	Lysá
hoře	hora	k1gFnSc6	hora
útulna	útulna	k1gFnSc1	útulna
Albrechta	Albrecht	k1gMnSc2	Albrecht
Habsburského	habsburský	k2eAgMnSc2d1	habsburský
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
zdrojem	zdroj	k1gInSc7	zdroj
určité	určitý	k2eAgFnSc2d1	určitá
závisti	závist	k1gFnSc2	závist
části	část	k1gFnSc2	část
českých	český	k2eAgMnPc2d1	český
turistů	turist	k1gMnPc2	turist
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
"	"	kIx"	"
<g/>
německou	německý	k2eAgFnSc7d1	německá
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
míněno	mínit	k5eAaImNgNnS	mínit
habsburskou	habsburský	k2eAgFnSc4d1	habsburská
<g/>
)	)	kIx)	)
chatu	chata	k1gFnSc4	chata
ignorovali	ignorovat	k5eAaImAgMnP	ignorovat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
chata	chata	k1gFnSc1	chata
Klubu	klub	k1gInSc2	klub
českých	český	k2eAgMnPc2d1	český
turistů	turist	k1gMnPc2	turist
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
podle	podle	k7c2	podle
Petra	Petr	k1gMnSc2	Petr
Bezruče	Bezruč	k1gFnSc2	Bezruč
Bezručova	Bezručův	k2eAgFnSc1d1	Bezručova
chata	chata	k1gFnSc1	chata
(	(	kIx(	(
<g/>
na	na	k7c6	na
Lysé	Lysé	k2eAgFnSc6d1	Lysé
hoře	hora	k1gFnSc6	hora
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
sám	sám	k3xTgMnSc1	sám
Petr	Petr	k1gMnSc1	Petr
Bezruč	Bezruč	k1gMnSc1	Bezruč
navštívil	navštívit	k5eAaPmAgMnS	navštívit
pouze	pouze	k6eAd1	pouze
"	"	kIx"	"
<g/>
německou	německý	k2eAgFnSc4d1	německá
<g/>
"	"	kIx"	"
chatu	chata	k1gFnSc4	chata
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
"	"	kIx"	"
<g/>
české	český	k2eAgFnSc6d1	Česká
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
i	i	k9	i
účastnit	účastnit	k5eAaImF	účastnit
jejího	její	k3xOp3gNnSc2	její
otevření	otevření	k1gNnSc2	otevření
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
byly	být	k5eAaImAgInP	být
obě	dva	k4xCgFnPc4	dva
znárodněny	znárodněn	k2eAgFnPc4d1	znárodněna
(	(	kIx(	(
<g/>
chata	chata	k1gFnSc1	chata
Albrechta	Albrecht	k1gMnSc2	Albrecht
Habsburského	habsburský	k2eAgInSc2d1	habsburský
přejmenována	přejmenovat	k5eAaPmNgNnP	přejmenovat
na	na	k7c4	na
Slezskou	slezský	k2eAgFnSc4d1	Slezská
chatu	chata	k1gFnSc4	chata
<g/>
)	)	kIx)	)
posléze	posléze	k6eAd1	posléze
sjednoceny	sjednocen	k2eAgFnPc1d1	sjednocena
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1972	[number]	k4	1972
a	a	k8xC	a
1978	[number]	k4	1978
postupně	postupně	k6eAd1	postupně
obě	dva	k4xCgFnPc1	dva
do	do	k7c2	do
základů	základ	k1gInPc2	základ
vyhořely	vyhořet	k5eAaPmAgFnP	vyhořet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
poskytovaly	poskytovat	k5eAaImAgFnP	poskytovat
turistům	turist	k1gMnPc3	turist
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
služby	služba	k1gFnSc2	služba
bufet	bufet	k1gNnSc1	bufet
Šantán	šantán	k1gInSc1	šantán
<g/>
,	,	kIx,	,
turistická	turistický	k2eAgFnSc1d1	turistická
ubytovna	ubytovna	k1gFnSc1	ubytovna
Kameňák	Kameňák	k?	Kameňák
a	a	k8xC	a
Horská	horský	k2eAgFnSc1d1	horská
chata	chata	k1gFnSc1	chata
Lysá	Lysá	k1gFnSc1	Lysá
hora	hora	k1gFnSc1	hora
zvaná	zvaný	k2eAgFnSc1d1	zvaná
"	"	kIx"	"
<g/>
Plesnivka	plesnivka	k1gMnSc1	plesnivka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
rozebrána	rozebrat	k5eAaPmNgFnS	rozebrat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
2012	[number]	k4	2012
<g/>
–	–	k?	–
<g/>
2015	[number]	k4	2015
byla	být	k5eAaImAgFnS	být
Klubem	klub	k1gInSc7	klub
českých	český	k2eAgMnPc2d1	český
turistů	turist	k1gMnPc2	turist
nákladem	náklad	k1gInSc7	náklad
32	[number]	k4	32
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
znovu	znovu	k6eAd1	znovu
postavena	postavit	k5eAaPmNgFnS	postavit
a	a	k8xC	a
v	v	k7c6	v
září	září	k1gNnSc6	září
2015	[number]	k4	2015
otevřena	otevřen	k2eAgFnSc1d1	otevřena
Bezručova	Bezručův	k2eAgFnSc1d1	Bezručova
chata	chata	k1gFnSc1	chata
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
byl	být	k5eAaImAgInS	být
zbourán	zbourat	k5eAaPmNgInS	zbourat
známý	známý	k2eAgInSc1d1	známý
Šantán	šantán	k1gInSc1	šantán
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
provizorní	provizorní	k2eAgFnSc7d1	provizorní
náhradou	náhrada	k1gFnSc7	náhrada
vyhořelé	vyhořelý	k2eAgFnSc2d1	vyhořelá
původní	původní	k2eAgFnSc2d1	původní
Bezručovy	Bezručův	k2eAgFnSc2d1	Bezručova
chaty	chata	k1gFnSc2	chata
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
bývalé	bývalý	k2eAgMnPc4d1	bývalý
Plesnivky	plesnivka	k1gMnPc4	plesnivka
stojí	stát	k5eAaImIp3nS	stát
nyní	nyní	k6eAd1	nyní
nová	nový	k2eAgFnSc1d1	nová
Chata	chata	k1gFnSc1	chata
Emila	Emil	k1gMnSc2	Emil
Zátopka	Zátopek	k1gMnSc2	Zátopek
–	–	k?	–
Maratón	maratón	k1gInSc1	maratón
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Lysé	Lysé	k2eAgFnSc6d1	Lysé
hoře	hora	k1gFnSc6	hora
stojí	stát	k5eAaImIp3nS	stát
i	i	k9	i
stanice	stanice	k1gFnSc1	stanice
horské	horský	k2eAgFnSc2d1	horská
služby	služba	k1gFnSc2	služba
s	s	k7c7	s
provozem	provoz	k1gInSc7	provoz
o	o	k7c6	o
víkendu	víkend	k1gInSc6	víkend
a	a	k8xC	a
svátcích	svátek	k1gInPc6	svátek
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1897	[number]	k4	1897
se	se	k3xPyFc4	se
provádějí	provádět	k5eAaImIp3nP	provádět
meteorologická	meteorologický	k2eAgNnPc1d1	meteorologické
měření	měření	k1gNnPc1	měření
(	(	kIx(	(
<g/>
nová	nový	k2eAgFnSc1d1	nová
budova	budova	k1gFnSc1	budova
českého	český	k2eAgInSc2d1	český
hydrometeorologického	hydrometeorologický	k2eAgInSc2d1	hydrometeorologický
ústavu	ústav	k1gInSc2	ústav
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
funguje	fungovat	k5eAaImIp3nS	fungovat
i	i	k9	i
telekomunikační	telekomunikační	k2eAgMnSc1d1	telekomunikační
vysílač	vysílač	k1gMnSc1	vysílač
vysoký	vysoký	k2eAgMnSc1d1	vysoký
78	[number]	k4	78
metrů	metr	k1gInPc2	metr
tvořící	tvořící	k2eAgFnSc4d1	tvořící
charakteristickou	charakteristický	k2eAgFnSc4d1	charakteristická
dominantu	dominanta	k1gFnSc4	dominanta
Lysé	Lysé	k2eAgFnSc2d1	Lysé
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hřebeni	hřeben	k1gInSc6	hřeben
pod	pod	k7c7	pod
Lysou	lysý	k2eAgFnSc7d1	Lysá
horou	hora	k1gFnSc7	hora
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mohyla	mohyla	k1gFnSc1	mohyla
Ivančena	Ivančen	k2eAgFnSc1d1	Ivančena
(	(	kIx(	(
<g/>
925	[number]	k4	925
m	m	kA	m
<g/>
)	)	kIx)	)
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
popravených	popravený	k2eAgMnPc2d1	popravený
skautů	skaut	k1gMnPc2	skaut
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
cvičného	cvičný	k2eAgInSc2d1	cvičný
letu	let	k1gInSc2	let
prolétávala	prolétávat	k5eAaImAgFnS	prolétávat
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1964	[number]	k4	1964
kolem	kolem	k7c2	kolem
Lysé	Lysé	k2eAgFnSc2d1	Lysé
hory	hora	k1gFnSc2	hora
dvě	dva	k4xCgNnPc1	dva
vojenská	vojenský	k2eAgNnPc1d1	vojenské
letadla	letadlo	k1gNnPc1	letadlo
MIG-	MIG-	k1gFnSc2	MIG-
<g/>
19	[number]	k4	19
<g/>
S.	S.	kA	S.
Jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
v	v	k7c6	v
mlze	mlha	k1gFnSc6	mlha
narazilo	narazit	k5eAaPmAgNnS	narazit
do	do	k7c2	do
bočního	boční	k2eAgInSc2d1	boční
hřebene	hřeben	k1gInSc2	hřeben
Lysé	Lysá	k1gFnSc2	Lysá
<g/>
.	.	kIx.	.
</s>
<s>
Třiatřicetiletý	třiatřicetiletý	k2eAgMnSc1d1	třiatřicetiletý
pilot	pilot	k1gMnSc1	pilot
<g/>
,	,	kIx,	,
major	major	k1gMnSc1	major
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Zaydlar	Zaydlar	k1gMnSc1	Zaydlar
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nestačil	stačit	k5eNaBmAgInS	stačit
katapultovat	katapultovat	k5eAaBmF	katapultovat
a	a	k8xC	a
při	při	k7c6	při
neštěstí	neštěstí	k1gNnSc6	neštěstí
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Lysá	lysý	k2eAgFnSc1d1	Lysá
hora	hora	k1gFnSc1	hora
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
historické	historický	k2eAgFnSc6d1	historická
zemi	zem	k1gFnSc6	zem
Slezsko	Slezsko	k1gNnSc1	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Lysá	lysý	k2eAgFnSc1d1	Lysá
hora	hora	k1gFnSc1	hora
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
Těšínského	Těšínského	k2eAgNnSc2d1	Těšínského
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Lysá	lysý	k2eAgFnSc1d1	Lysá
hora	hora	k1gFnSc1	hora
byla	být	k5eAaImAgFnS	být
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
Protektorátu	protektorát	k1gInSc2	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Lysá	lysý	k2eAgFnSc1d1	Lysá
hora	hora	k1gFnSc1	hora
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgFnSc7	třetí
nejprominentnější	prominentní	k2eAgFnSc7d3	nejprominentnější
českou	český	k2eAgFnSc7d1	Česká
horou	hora	k1gFnSc7	hora
<g/>
.	.	kIx.	.
</s>
<s>
Úplně	úplně	k6eAd1	úplně
celá	celý	k2eAgFnSc1d1	celá
hora	hora	k1gFnSc1	hora
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
řeky	řeka	k1gFnSc2	řeka
Ostravice	Ostravice	k1gFnSc2	Ostravice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Lysé	Lysé	k2eAgFnSc6d1	Lysé
hoře	hora	k1gFnSc6	hora
byla	být	k5eAaImAgFnS	být
zaznamenána	zaznamenat	k5eAaPmNgFnS	zaznamenat
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
sněhová	sněhový	k2eAgFnSc1d1	sněhová
pokrývka	pokrývka	k1gFnSc1	pokrývka
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
měřila	měřit	k5eAaImAgFnS	měřit
491	[number]	k4	491
cm	cm	kA	cm
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
pověsti	pověst	k1gFnSc2	pověst
se	se	k3xPyFc4	se
na	na	k7c6	na
Lysé	Lysá	k1gFnSc6	Lysá
hoře	hoře	k6eAd1	hoře
slétaly	slétat	k5eAaImAgFnP	slétat
čarodějnice	čarodějnice	k1gFnPc4	čarodějnice
z	z	k7c2	z
celého	celý	k2eAgNnSc2d1	celé
Slezska	Slezsko	k1gNnSc2	Slezsko
a	a	k8xC	a
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
utancovaly	utancovat	k5eAaPmAgInP	utancovat
každého	každý	k3xTgMnSc4	každý
zabloudilce	zabloudilec	k1gMnSc2	zabloudilec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
připletl	připlést	k5eAaPmAgMnS	připlést
k	k	k7c3	k
jejich	jejich	k3xOp3gInPc3	jejich
rejům	rej	k1gInPc3	rej
<g/>
.	.	kIx.	.
</s>
<s>
Českého	český	k2eAgMnSc4d1	český
básníka	básník	k1gMnSc4	básník
Petra	Petr	k1gMnSc4	Petr
Bezruče	Bezruč	k1gInSc2	Bezruč
hora	hora	k1gFnSc1	hora
a	a	k8xC	a
kraj	kraj	k1gInSc1	kraj
Beskyd	Beskydy	k1gInPc2	Beskydy
inspirovaly	inspirovat	k5eAaBmAgInP	inspirovat
k	k	k7c3	k
příznačným	příznačný	k2eAgFnPc3d1	příznačná
básnickým	básnický	k2eAgFnPc3d1	básnická
sbírkám	sbírka	k1gFnPc3	sbírka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Bezručovi	Bezruč	k1gMnSc6	Bezruč
je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
i	i	k8xC	i
nejstarší	starý	k2eAgInSc1d3	nejstarší
turistický	turistický	k2eAgInSc1d1	turistický
výšlap	výšlap	k1gInSc1	výšlap
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Bezručův	Bezručův	k2eAgInSc1d1	Bezručův
výplaz	výplaz	k1gInSc1	výplaz
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
konal	konat	k5eAaImAgInS	konat
již	již	k6eAd1	již
popadesáté	popadesátý	k2eAgFnPc4d1	popadesátý
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Pražmo	pražmo	k1gNnSc1	pražmo
<g/>
,	,	kIx,	,
asi	asi	k9	asi
7	[number]	k4	7
km	km	kA	km
severo-severovýchodně	severoeverovýchodně	k6eAd1	severo-severovýchodně
od	od	k7c2	od
vrcholu	vrchol	k1gInSc2	vrchol
Lysé	Lysé	k2eAgFnSc2d1	Lysé
hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pochována	pochovat	k5eAaPmNgFnS	pochovat
Maryčka	Maryčka	k1gFnSc1	Maryčka
Magdónová	Magdónový	k2eAgFnSc1d1	Magdónová
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
z	z	k7c2	z
několika	několik	k4yIc2	několik
jeho	jeho	k3xOp3gMnPc2	jeho
básní	básnit	k5eAaImIp3nP	básnit
<g/>
.	.	kIx.	.
</s>
