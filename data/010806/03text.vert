<p>
<s>
Křišťál	křišťál	k1gInSc1	křišťál
je	být	k5eAaImIp3nS	být
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
odrůda	odrůda	k1gFnSc1	odrůda
křemene	křemen	k1gInSc2	křemen
(	(	kIx(	(
<g/>
SiO	SiO	k1gFnSc1	SiO
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
krystalů	krystal	k1gInPc2	krystal
v	v	k7c6	v
šesterečné	šesterečný	k2eAgFnSc6d1	šesterečná
krystalové	krystalový	k2eAgFnSc6d1	krystalová
soustavě	soustava	k1gFnSc6	soustava
s	s	k7c7	s
tvrdostí	tvrdost	k1gFnSc7	tvrdost
6,5	[number]	k4	6,5
<g/>
–	–	k?	–
<g/>
7,5	[number]	k4	7,5
Mohsovy	Mohsův	k2eAgFnSc2d1	Mohsova
stupnice	stupnice	k1gFnSc2	stupnice
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
jako	jako	k9	jako
většina	většina	k1gFnSc1	většina
křemičitanů	křemičitan	k1gInPc2	křemičitan
vykrystalizováním	vykrystalizování	k1gNnPc3	vykrystalizování
z	z	k7c2	z
magmatu	magma	k1gNnSc2	magma
<g/>
.	.	kIx.	.
</s>
<s>
Křišťál	křišťál	k1gInSc1	křišťál
je	být	k5eAaImIp3nS	být
hojně	hojně	k6eAd1	hojně
využívaným	využívaný	k2eAgInSc7d1	využívaný
kamenem	kámen	k1gInSc7	kámen
ve	v	k7c6	v
šperkařství	šperkařství	k1gNnSc6	šperkařství
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
s	s	k7c7	s
vrostlicemi	vrostlice	k1gFnPc7	vrostlice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
laserové	laserový	k2eAgFnSc6d1	laserová
technice	technika	k1gFnSc6	technika
a	a	k8xC	a
radiotechnice	radiotechnika	k1gFnSc6	radiotechnika
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
optických	optický	k2eAgInPc2d1	optický
přístrojů	přístroj	k1gInPc2	přístroj
a	a	k8xC	a
ve	v	k7c6	v
sklářském	sklářský	k2eAgInSc6d1	sklářský
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbený	oblíbený	k2eAgInSc1d1	oblíbený
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
sbírkový	sbírkový	k2eAgInSc1d1	sbírkový
kámen	kámen	k1gInSc1	kámen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Formy	forma	k1gFnSc2	forma
křišťálu	křišťál	k1gInSc2	křišťál
==	==	k?	==
</s>
</p>
<p>
<s>
Křišťál	křišťál	k1gInSc1	křišťál
je	být	k5eAaImIp3nS	být
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
odrůda	odrůda	k1gFnSc1	odrůda
oxidu	oxid	k1gInSc2	oxid
křemičitého	křemičitý	k2eAgInSc2d1	křemičitý
<g/>
.	.	kIx.	.
</s>
<s>
Křišťál	křišťál	k1gInSc1	křišťál
tvoří	tvořit	k5eAaImIp3nS	tvořit
krystaly	krystal	k1gInPc4	krystal
i	i	k8xC	i
jejich	jejich	k3xOp3gFnPc4	jejich
srostlice	srostlice	k1gFnPc4	srostlice
<g/>
.	.	kIx.	.
</s>
<s>
Vzácnějšími	vzácný	k2eAgInPc7d2	vzácnější
útvary	útvar	k1gInPc7	útvar
křišťálu	křišťál	k1gInSc2	křišťál
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
žezlový	žezlový	k2eAgInSc1d1	žezlový
křišťál	křišťál	k1gInSc1	křišťál
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
krystaly	krystal	k1gInPc4	krystal
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
vytváření	vytváření	k1gNnSc4	vytváření
krystalu	krystal	k1gInSc2	krystal
na	na	k7c6	na
nižších	nízký	k2eAgFnPc6d2	nižší
úrovních	úroveň	k1gFnPc6	úroveň
obaleny	obalen	k2eAgInPc1d1	obalen
okolními	okolní	k2eAgFnPc7d1	okolní
příměsemi	příměse	k1gFnPc7	příměse
a	a	k8xC	a
zastavily	zastavit	k5eAaPmAgFnP	zastavit
jeho	jeho	k3xOp3gFnPc4	jeho
růst	růst	k5eAaImF	růst
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
mohl	moct	k5eAaImAgInS	moct
růst	růst	k1gInSc4	růst
krystalu	krystal	k1gInSc2	krystal
pokračovat	pokračovat	k5eAaImF	pokračovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
jediném	jediný	k2eAgInSc6d1	jediný
bodě	bod	k1gInSc6	bod
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
nejaktivnějším	aktivní	k2eAgInSc6d3	nejaktivnější
místě-vrcholu	místěrchol	k1gInSc6	místě-vrchol
krystalu	krystal	k1gInSc6	krystal
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomuto	tento	k3xDgMnSc3	tento
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
útvar	útvar	k1gInSc1	útvar
tvaru	tvar	k1gInSc2	tvar
žezla	žezlo	k1gNnSc2	žezlo
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
útvar	útvar	k1gInSc1	útvar
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
pro	pro	k7c4	pro
ametyst	ametyst	k1gInSc4	ametyst
<g/>
,	,	kIx,	,
u	u	k7c2	u
něhož	jenž	k3xRgMnSc2	jenž
je	být	k5eAaImIp3nS	být
důvodem	důvod	k1gInSc7	důvod
zastavení	zastavení	k1gNnSc1	zastavení
růstu	růst	k1gInSc2	růst
železo	železo	k1gNnSc1	železo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
přísadou	přísada	k1gFnSc7	přísada
v	v	k7c6	v
křišťálu	křišťál	k1gInSc6	křišťál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Křišťál	křišťál	k1gInSc1	křišťál
také	také	k9	také
tvoří	tvořit	k5eAaImIp3nS	tvořit
vlasatce	vlasatec	k1gMnPc4	vlasatec
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
krystaly	krystal	k1gInPc1	krystal
křišťálu	křišťál	k1gInSc2	křišťál
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgNnPc6	který
jsou	být	k5eAaImIp3nP	být
vrostlice	vrostlice	k1gFnPc1	vrostlice
rutilu	rutil	k1gInSc2	rutil
–	–	k?	–
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gFnSc2	jeho
barvy	barva	k1gFnSc2	barva
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
krystal	krystal	k1gInSc1	krystal
nazývá	nazývat	k5eAaImIp3nS	nazývat
Amorovy	Amorův	k2eAgInPc4d1	Amorův
šípy	šíp	k1gInPc4	šíp
nebo	nebo	k8xC	nebo
Venušiny	Venušin	k2eAgInPc4d1	Venušin
vlasy	vlas	k1gInPc4	vlas
<g/>
.	.	kIx.	.
</s>
<s>
Nejkrásnější	krásný	k2eAgMnPc4d3	nejkrásnější
vlasatce	vlasatec	k1gMnPc4	vlasatec
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
Brazílie	Brazílie	k1gFnSc2	Brazílie
a	a	k8xC	a
dolů	dolů	k6eAd1	dolů
v	v	k7c6	v
Uralu	Ural	k1gInSc6	Ural
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc1d1	podobný
jev	jev	k1gInSc1	jev
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
u	u	k7c2	u
citrínu	citrínu	k?	citrínu
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
struktuře	struktura	k1gFnSc6	struktura
plynné	plynný	k2eAgFnSc2d1	plynná
nebo	nebo	k8xC	nebo
vodní	vodní	k2eAgFnSc2d1	vodní
vrostlice	vrostlice	k1gFnSc2	vrostlice
a	a	k8xC	a
při	pře	k1gFnSc4	pře
nasvícení	nasvícení	k1gNnSc2	nasvícení
proti	proti	k7c3	proti
přímému	přímý	k2eAgNnSc3d1	přímé
světlu	světlo	k1gNnSc3	světlo
vypadají	vypadat	k5eAaImIp3nP	vypadat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
zlatý	zlatý	k2eAgInSc1d1	zlatý
déšť	déšť	k1gInSc1	déšť
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Asi	asi	k9	asi
tím	ten	k3xDgInSc7	ten
nejhezčím	hezký	k2eAgInSc7d3	nejhezčí
a	a	k8xC	a
nejzajímavějším	zajímavý	k2eAgInSc7d3	nejzajímavější
útvarem	útvar	k1gInSc7	útvar
u	u	k7c2	u
křišťálu	křišťál	k1gInSc2	křišťál
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
fantomy	fantom	k1gInPc1	fantom
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
etapě	etapa	k1gFnSc6	etapa
růstu	růst	k1gInSc2	růst
sloužil	sloužit	k5eAaImAgInS	sloužit
křišťál	křišťál	k1gInSc1	křišťál
jako	jako	k8xS	jako
základ	základ	k1gInSc1	základ
pro	pro	k7c4	pro
růst	růst	k1gInSc4	růst
krystalků	krystalek	k1gInPc2	krystalek
jiného	jiný	k2eAgInSc2d1	jiný
minerálu	minerál	k1gInSc2	minerál
<g/>
.	.	kIx.	.
</s>
<s>
Rostoucí	rostoucí	k2eAgInSc1d1	rostoucí
krystal	krystal	k1gInSc1	krystal
křišťálu	křišťál	k1gInSc2	křišťál
poté	poté	k6eAd1	poté
některé	některý	k3yIgFnPc4	některý
krystalky	krystalka	k1gFnPc4	krystalka
odhodil	odhodit	k5eAaPmAgMnS	odhodit
a	a	k8xC	a
jiné	jiné	k1gNnSc4	jiné
obalil	obalit	k5eAaPmAgMnS	obalit
nebo	nebo	k8xC	nebo
vstřebal	vstřebat	k5eAaPmAgMnS	vstřebat
<g/>
,	,	kIx,	,
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
krystalky	krystalka	k1gFnPc1	krystalka
součástí	součást	k1gFnPc2	součást
–	–	k?	–
uzavřeninami	uzavřenina	k1gFnPc7	uzavřenina
<g/>
,	,	kIx,	,
označením	označení	k1gNnSc7	označení
růstové	růstový	k2eAgFnSc2d1	růstová
zóny	zóna	k1gFnSc2	zóna
krystalu	krystal	k1gInSc2	krystal
<g/>
.	.	kIx.	.
</s>
<s>
Fantomy	fantom	k1gInPc1	fantom
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
minerálech	minerál	k1gInPc6	minerál
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
kalcitu	kalcit	k1gInSc6	kalcit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Křišťál	křišťál	k1gInSc1	křišťál
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
vědci	vědec	k1gMnPc1	vědec
považovali	považovat	k5eAaImAgMnP	považovat
křišťál	křišťál	k1gInSc4	křišťál
za	za	k7c4	za
zkamenělý	zkamenělý	k2eAgInSc4d1	zkamenělý
led	led	k1gInSc4	led
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
patřil	patřit	k5eAaImAgMnS	patřit
také	také	k9	také
Plinius	Plinius	k1gMnSc1	Plinius
starší	starší	k1gMnSc1	starší
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
tvrzení	tvrzení	k1gNnSc2	tvrzení
bylo	být	k5eAaImAgNnS	být
odvozeno	odvozen	k2eAgNnSc4d1	odvozeno
slovo	slovo	k1gNnSc4	slovo
křišťál	křišťál	k1gInSc1	křišťál
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
výrazu	výraz	k1gInSc2	výraz
Krustallos	Krustallos	k1gMnSc1	Krustallos
–	–	k?	–
led	led	k1gInSc1	led
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
již	již	k6eAd1	již
v	v	k7c6	v
neolitu	neolit	k1gInSc6	neolit
používali	používat	k5eAaImAgMnP	používat
úlomků	úlomek	k1gInPc2	úlomek
křišťálu	křišťál	k1gInSc2	křišťál
jako	jako	k8xC	jako
ostré	ostrý	k2eAgInPc4d1	ostrý
hroty	hrot	k1gInPc4	hrot
šípů	šíp	k1gInPc2	šíp
a	a	k8xC	a
kopí	kopí	k1gNnSc2	kopí
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
antických	antický	k2eAgFnPc2d1	antická
legend	legenda	k1gFnPc2	legenda
bohové	bůh	k1gMnPc1	bůh
pili	pít	k5eAaImAgMnP	pít
víno	víno	k1gNnSc4	víno
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
křišťálových	křišťálový	k2eAgInPc2d1	křišťálový
pohárů	pohár	k1gInPc2	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
antickém	antický	k2eAgInSc6d1	antický
Římě	Řím	k1gInSc6	Řím
považovány	považován	k2eAgInPc4d1	považován
křišťálové	křišťálový	k2eAgInPc4d1	křišťálový
džbány	džbán	k1gInPc4	džbán
a	a	k8xC	a
číše	číš	k1gFnPc4	číš
za	za	k7c4	za
luxusní	luxusní	k2eAgInPc4d1	luxusní
předměty	předmět	k1gInPc4	předmět
a	a	k8xC	a
pyšnili	pyšnit	k5eAaImAgMnP	pyšnit
se	se	k3xPyFc4	se
jimi	on	k3xPp3gFnPc7	on
jen	jen	k6eAd1	jen
nejbohatší	bohatý	k2eAgFnSc1d3	nejbohatší
a	a	k8xC	a
císaři	císař	k1gMnSc3	císař
<g/>
.	.	kIx.	.
</s>
<s>
Povídá	povídat	k5eAaImIp3nS	povídat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
císař	císař	k1gMnSc1	císař
Nero	Nero	k1gMnSc1	Nero
začal	začít	k5eAaPmAgMnS	začít
ztrácet	ztrácet	k5eAaImF	ztrácet
moc	moc	k6eAd1	moc
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
v	v	k7c6	v
záchvatu	záchvat	k1gInSc6	záchvat
hněvu	hněv	k1gInSc2	hněv
rozbil	rozbít	k5eAaPmAgInS	rozbít
svou	svůj	k3xOyFgFnSc4	svůj
sbírku	sbírka	k1gFnSc4	sbírka
křišťálových	křišťálový	k2eAgFnPc2d1	Křišťálová
číší	číš	k1gFnPc2	číš
<g/>
.	.	kIx.	.
<g/>
Čirý	čirý	k2eAgInSc1d1	čirý
a	a	k8xC	a
průsvitný	průsvitný	k2eAgInSc1d1	průsvitný
křišťál	křišťál	k1gInSc1	křišťál
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
dávných	dávný	k2eAgFnPc2d1	dávná
dob	doba	k1gFnPc2	doba
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
symbol	symbol	k1gInSc4	symbol
rovnováhy	rovnováha	k1gFnSc2	rovnováha
a	a	k8xC	a
čistoty	čistota	k1gFnSc2	čistota
a	a	k8xC	a
používán	používat	k5eAaImNgInS	používat
na	na	k7c4	na
talismany	talisman	k1gInPc4	talisman
<g/>
.	.	kIx.	.
</s>
<s>
Křišťálové	křišťálový	k2eAgInPc1d1	křišťálový
předměty	předmět	k1gInPc1	předmět
se	se	k3xPyFc4	se
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
cenily	cenit	k5eAaImAgFnP	cenit
velmi	velmi	k6eAd1	velmi
vysoko	vysoko	k6eAd1	vysoko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zbrojnici	zbrojnice	k1gFnSc6	zbrojnice
Moskevského	moskevský	k2eAgInSc2d1	moskevský
kremlu	kreml	k1gInSc2	kreml
je	být	k5eAaImIp3nS	být
uložen	uložen	k2eAgInSc4d1	uložen
křišťálový	křišťálový	k2eAgInSc4d1	křišťálový
samovar	samovar	k1gInSc4	samovar
vybroušený	vybroušený	k2eAgInSc4d1	vybroušený
z	z	k7c2	z
jediného	jediný	k2eAgInSc2d1	jediný
kusu	kus	k1gInSc2	kus
křišťálu	křišťál	k1gInSc2	křišťál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
patřil	patřit	k5eAaImAgInS	patřit
Petru	Petra	k1gFnSc4	Petra
I.	I.	kA	I.
Křišťálové	křišťálový	k2eAgFnSc2d1	Křišťálová
koule	koule	k1gFnSc2	koule
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
zemích	zem	k1gFnPc6	zem
nezbytnou	nezbytný	k2eAgFnSc7d1	nezbytná
součástí	součást	k1gFnSc7	součást
říšských	říšský	k2eAgFnPc2d1	říšská
insignií	insignie	k1gFnPc2	insignie
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgFnPc1	takový
koule	koule	k1gFnPc1	koule
byly	být	k5eAaImAgFnP	být
součástí	součást	k1gFnSc7	součást
žezla	žezlo	k1gNnSc2	žezlo
skotských	skotský	k2eAgMnPc2d1	skotský
králů	král	k1gMnPc2	král
nebo	nebo	k8xC	nebo
byly	být	k5eAaImAgInP	být
používány	používat	k5eAaImNgInP	používat
samostatně	samostatně	k6eAd1	samostatně
jako	jako	k8xS	jako
jablko	jablko	k1gNnSc4	jablko
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
říšské	říšský	k2eAgNnSc4d1	říšské
jablko	jablko	k1gNnSc4	jablko
ruských	ruský	k2eAgMnPc2d1	ruský
carů	car	k1gMnPc2	car
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Ďuda	Ďuda	k1gFnSc1	Ďuda
R.	R.	kA	R.
<g/>
,	,	kIx,	,
Rejl	Rejl	k1gInSc1	Rejl
L.	L.	kA	L.
Svět	svět	k1gInSc1	svět
drahých	drahý	k2eAgInPc2d1	drahý
kamenů	kámen	k1gInPc2	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Vydání	vydání	k1gNnSc1	vydání
třetí	třetí	k4xOgFnSc1	třetí
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Granit	granit	k1gInSc1	granit
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7296-018-0	[number]	k4	80-7296-018-0
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
křišťál	křišťál	k1gInSc1	křišťál
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
křišťál	křišťál	k1gInSc1	křišťál
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
