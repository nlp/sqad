<p>
<s>
Obratník	obratník	k1gInSc1	obratník
Kozoroha	Kozoroh	k1gMnSc2	Kozoroh
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
obratník	obratník	k1gInSc4	obratník
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c4	na
23	[number]	k4	23
<g/>
°	°	k?	°
26	[number]	k4	26
<g/>
'	'	kIx"	'
14.440	[number]	k4	14.440
<g/>
"	"	kIx"	"
jižní	jižní	k2eAgFnSc2d1	jižní
šířky	šířka	k1gFnSc2	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejjižnější	jižní	k2eAgFnSc4d3	nejjižnější
rovnoběžku	rovnoběžka	k1gFnSc4	rovnoběžka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Slunce	slunce	k1gNnSc1	slunce
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
zenitu	zenit	k1gInSc6	zenit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
zde	zde	k6eAd1	zde
dochází	docházet	k5eAaImIp3nS	docházet
právě	právě	k9	právě
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
době	doba	k1gFnSc6	doba
zimního	zimní	k2eAgInSc2d1	zimní
slunovratu	slunovrat	k1gInSc2	slunovrat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
obratníku	obratník	k1gInSc2	obratník
Kozoroha	Kozoroh	k1gMnSc2	Kozoroh
se	se	k3xPyFc4	se
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
obratníku	obratník	k1gInSc2	obratník
Raka	rak	k1gMnSc2	rak
nachází	nacházet	k5eAaImIp3nS	nacházet
pásmo	pásmo	k1gNnSc4	pásmo
pouští	poušť	k1gFnPc2	poušť
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétně	konkrétně	k6eAd1	konkrétně
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
Atacama	Atacama	k1gNnSc4	Atacama
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
Kalahari	Kalahari	k1gNnPc7	Kalahari
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
pouště	poušť	k1gFnPc4	poušť
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Státy	stát	k1gInPc1	stát
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
relativně	relativně	k6eAd1	relativně
sporého	sporý	k2eAgInSc2d1	sporý
výskytu	výskyt	k1gInSc2	výskyt
souše	souš	k1gFnSc2	souš
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
zemské	zemský	k2eAgFnSc6d1	zemská
polokouli	polokoule	k1gFnSc6	polokoule
existují	existovat	k5eAaImIp3nP	existovat
pouze	pouze	k6eAd1	pouze
čtyři	čtyři	k4xCgInPc4	čtyři
státy	stát	k1gInPc4	stát
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc2d1	ležící
celé	celá	k1gFnSc2	celá
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
obratníku	obratník	k1gInSc2	obratník
Kozoroha	Kozoroh	k1gMnSc2	Kozoroh
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Lesotho	Lesot	k1gMnSc4	Lesot
Lesotho	Lesot	k1gMnSc4	Lesot
</s>
</p>
<p>
<s>
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
</s>
</p>
<p>
<s>
Svazijsko	Svazijsko	k1gNnSc1	Svazijsko
Svazijsko	Svazijsko	k1gNnSc1	Svazijsko
</s>
</p>
<p>
<s>
Uruguay	Uruguay	k1gFnSc1	Uruguay
UruguaySvá	UruguaySvý	k2eAgNnPc1d1	UruguaySvý
hlavní	hlavní	k2eAgNnPc1d1	hlavní
města	město	k1gNnPc1	město
mají	mít	k5eAaImIp3nP	mít
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
obratníku	obratník	k1gInSc2	obratník
ještě	ještě	k9	ještě
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
,	,	kIx,	,
Botswana	Botswana	k1gFnSc1	Botswana
<g/>
,	,	kIx,	,
Chile	Chile	k1gNnSc1	Chile
<g/>
,	,	kIx,	,
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
Mosambik	Mosambik	k1gInSc1	Mosambik
a	a	k8xC	a
Paraguay	Paraguay	k1gFnSc1	Paraguay
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
obratníku	obratník	k1gInSc6	obratník
leží	ležet	k5eAaImIp3nS	ležet
brazilská	brazilský	k2eAgFnSc1d1	brazilská
megalopole	megalopole	k1gFnSc1	megalopole
Sã	Sã	k1gFnSc2	Sã
Paulo	Paula	k1gFnSc5	Paula
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Obratník	obratník	k1gInSc1	obratník
Raka	rak	k1gMnSc2	rak
</s>
</p>
<p>
<s>
Rovník	rovník	k1gInSc1	rovník
</s>
</p>
<p>
<s>
Tropický	tropický	k2eAgInSc1d1	tropický
pás	pás	k1gInSc1	pás
</s>
</p>
