<s>
Obratník	obratník	k1gInSc1	obratník
Kozoroha	Kozoroh	k1gMnSc2	Kozoroh
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
obratník	obratník	k1gInSc4	obratník
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c4	na
23	[number]	k4	23
<g/>
°	°	k?	°
26	[number]	k4	26
<g/>
'	'	kIx"	'
14.440	[number]	k4	14.440
<g/>
"	"	kIx"	"
jižní	jižní	k2eAgFnSc2d1	jižní
šířky	šířka	k1gFnSc2	šířka
<g/>
.	.	kIx.	.
</s>
