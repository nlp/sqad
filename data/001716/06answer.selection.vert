<s>
Britské	britský	k2eAgFnPc1d1	britská
rockové	rockový	k2eAgFnPc1d1	rocková
skupiny	skupina	k1gFnPc1	skupina
jako	jako	k9	jako
například	například	k6eAd1	například
The	The	k1gFnSc1	The
Rolling	Rolling	k1gInSc1	Rolling
Stones	Stones	k1gInSc1	Stones
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Beatles	beatles	k1gMnSc1	beatles
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Yardbirds	Yardbirds	k1gInSc1	Yardbirds
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Who	Who	k1gFnSc1	Who
a	a	k8xC	a
The	The	k1gFnSc1	The
Kinks	Kinksa	k1gFnPc2	Kinksa
modifikovaly	modifikovat	k5eAaBmAgFnP	modifikovat
standardní	standardní	k2eAgInSc4d1	standardní
rock	rock	k1gInSc4	rock
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
roll	roll	k1gMnSc1	roll
přidáváním	přidávání	k1gNnSc7	přidávání
tvrdšího	tvrdý	k2eAgInSc2d2	tvrdší
zvuku	zvuk	k1gInSc2	zvuk
<g/>
,	,	kIx,	,
těžšími	těžký	k2eAgInPc7d2	těžší
kytarovými	kytarový	k2eAgInPc7d1	kytarový
riffy	riff	k1gInPc7	riff
<g/>
,	,	kIx,	,
dunivými	dunivý	k2eAgInPc7d1	dunivý
bicími	bicí	k2eAgInPc7d1	bicí
a	a	k8xC	a
hlasitým	hlasitý	k2eAgInSc7d1	hlasitý
zpěvem	zpěv	k1gInSc7	zpěv
<g/>
.	.	kIx.	.
</s>
