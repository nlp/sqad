<s>
Horečka	horečka	k1gFnSc1	horečka
dengue	dengue	k6eAd1	dengue
je	být	k5eAaImIp3nS	být
infekce	infekce	k1gFnSc1	infekce
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
virus	virus	k1gInSc1	virus
dengue	dengue	k1gInSc1	dengue
<g/>
,	,	kIx,	,
přenášený	přenášený	k2eAgInSc1d1	přenášený
komáry	komár	k1gMnPc7	komár
<g/>
.	.	kIx.	.
</s>
<s>
Horečka	horečka	k1gFnSc1	horečka
dengue	dengu	k1gFnSc2	dengu
je	být	k5eAaImIp3nS	být
také	také	k9	také
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
horečka	horečka	k1gFnSc1	horečka
lámající	lámající	k2eAgFnSc2d1	lámající
kosti	kost	k1gFnSc2	kost
<g/>
"	"	kIx"	"
−	−	k?	−
může	moct	k5eAaImIp3nS	moct
totiž	totiž	k9	totiž
působit	působit	k5eAaImF	působit
lidem	lid	k1gInSc7	lid
takovou	takový	k3xDgFnSc4	takový
bolest	bolest	k1gFnSc4	bolest
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
připadá	připadat	k5eAaImIp3nS	připadat
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
lámaly	lámat	k5eAaImAgInP	lámat
kosti	kost	k1gFnPc4	kost
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
symptomům	symptom	k1gInPc3	symptom
nemoci	nemoc	k1gFnSc2	nemoc
patří	patřit	k5eAaImIp3nS	patřit
horečka	horečka	k1gFnSc1	horečka
<g/>
,	,	kIx,	,
bolesti	bolest	k1gFnPc1	bolest
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
kožní	kožní	k2eAgFnSc1d1	kožní
vyrážka	vyrážka	k1gFnSc1	vyrážka
podobná	podobný	k2eAgFnSc1d1	podobná
spalničkám	spalničky	k1gFnPc3	spalničky
a	a	k8xC	a
bolesti	bolest	k1gFnPc1	bolest
svalů	sval	k1gInPc2	sval
a	a	k8xC	a
kloubů	kloub	k1gInPc2	kloub
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
lidí	člověk	k1gMnPc2	člověk
se	se	k3xPyFc4	se
horečka	horečka	k1gFnSc1	horečka
dengue	dengue	k6eAd1	dengue
může	moct	k5eAaImIp3nS	moct
rozvinout	rozvinout	k5eAaPmF	rozvinout
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
život	život	k1gInSc4	život
ohrožujících	ohrožující	k2eAgFnPc2d1	ohrožující
forem	forma	k1gFnPc2	forma
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
hemoragická	hemoragický	k2eAgFnSc1d1	hemoragická
horečka	horečka	k1gFnSc1	horečka
dengue	dengue	k1gFnSc1	dengue
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
krvácení	krvácení	k1gNnSc4	krvácení
a	a	k8xC	a
výrazné	výrazný	k2eAgNnSc4d1	výrazné
snížení	snížení	k1gNnSc4	snížení
hladiny	hladina	k1gFnSc2	hladina
krevních	krevní	k2eAgFnPc2d1	krevní
destiček	destička	k1gFnPc2	destička
(	(	kIx(	(
<g/>
umožňujících	umožňující	k2eAgNnPc2d1	umožňující
srážení	srážení	k1gNnPc2	srážení
krve	krev	k1gFnSc2	krev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
formou	forma	k1gFnSc7	forma
je	být	k5eAaImIp3nS	být
šokový	šokový	k2eAgInSc1d1	šokový
syndrom	syndrom	k1gInSc1	syndrom
dengue	dengu	k1gInSc2	dengu
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
příčinou	příčina	k1gFnSc7	příčina
nebezpečně	bezpečně	k6eNd1	bezpečně
nízkého	nízký	k2eAgInSc2d1	nízký
krevního	krevní	k2eAgInSc2d1	krevní
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
čtyři	čtyři	k4xCgInPc1	čtyři
odlišné	odlišný	k2eAgInPc1d1	odlišný
typy	typ	k1gInPc1	typ
viru	vir	k1gInSc2	vir
dengue	dengu	k1gInSc2	dengu
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
nakazí	nakazit	k5eAaPmIp3nS	nakazit
jedním	jeden	k4xCgInSc7	jeden
druhem	druh	k1gInSc7	druh
viru	vir	k1gInSc2	vir
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
chráněn	chránit	k5eAaImNgInS	chránit
před	před	k7c7	před
tímto	tento	k3xDgInSc7	tento
typem	typ	k1gInSc7	typ
po	po	k7c4	po
zbytek	zbytek	k1gInSc4	zbytek
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Ochranu	ochrana	k1gFnSc4	ochrana
před	před	k7c7	před
ostatními	ostatní	k2eAgInPc7d1	ostatní
třemi	tři	k4xCgInPc7	tři
druhy	druh	k1gInPc7	druh
viru	vir	k1gInSc2	vir
mu	on	k3xPp3gInSc3	on
to	ten	k3xDgNnSc1	ten
však	však	k9	však
poskytne	poskytnout	k5eAaPmIp3nS	poskytnout
jen	jen	k9	jen
na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
nakazí	nakazit	k5eAaPmIp3nS	nakazit
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
je	on	k3xPp3gMnPc4	on
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
vážné	vážný	k2eAgFnPc4d1	vážná
zdravotní	zdravotní	k2eAgFnPc4d1	zdravotní
potíže	potíž	k1gFnPc4	potíž
<g/>
.	.	kIx.	.
</s>
<s>
Neexistuje	existovat	k5eNaImIp3nS	existovat
žádná	žádný	k3yNgFnSc1	žádný
účinná	účinný	k2eAgFnSc1d1	účinná
vakcína	vakcína	k1gFnSc1	vakcína
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
člověka	člověk	k1gMnSc4	člověk
před	před	k7c7	před
nakažením	nakažení	k1gNnSc7	nakažení
chránila	chránit	k5eAaImAgFnS	chránit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
možné	možný	k2eAgNnSc1d1	možné
provést	provést	k5eAaPmF	provést
opatření	opatření	k1gNnPc4	opatření
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc1	jenž
mohou	moct	k5eAaImIp3nP	moct
infikování	infikování	k1gNnSc4	infikování
člověka	člověk	k1gMnSc2	člověk
zabránit	zabránit	k5eAaPmF	zabránit
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
před	před	k7c7	před
komáry	komár	k1gMnPc7	komár
chránit	chránit	k5eAaImF	chránit
a	a	k8xC	a
snažit	snažit	k5eAaImF	snažit
se	se	k3xPyFc4	se
omezit	omezit	k5eAaPmF	omezit
počet	počet	k1gInSc4	počet
štípnutí	štípnutí	k1gNnSc2	štípnutí
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
rovněž	rovněž	k9	rovněž
navrhují	navrhovat	k5eAaImIp3nP	navrhovat
zmenšení	zmenšení	k1gNnSc4	zmenšení
rozsahu	rozsah	k1gInSc2	rozsah
komářích	komáří	k2eAgInPc2d1	komáří
habitatů	habitat	k1gInPc2	habitat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
počtu	počet	k1gInSc2	počet
komárů	komár	k1gMnPc2	komár
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
člověk	člověk	k1gMnSc1	člověk
onemocní	onemocnět	k5eAaPmIp3nS	onemocnět
horečkou	horečka	k1gFnSc7	horečka
dengue	dengu	k1gFnSc2	dengu
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
uzdraví	uzdravit	k5eAaPmIp3nP	uzdravit
pouze	pouze	k6eAd1	pouze
díky	díky	k7c3	díky
přijímání	přijímání	k1gNnSc3	přijímání
dostatečného	dostatečný	k2eAgNnSc2d1	dostatečné
množství	množství	k1gNnSc2	množství
tekutin	tekutina	k1gFnPc2	tekutina
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
mírnou	mírný	k2eAgFnSc4d1	mírná
nebo	nebo	k8xC	nebo
středně	středně	k6eAd1	středně
těžkou	těžký	k2eAgFnSc4d1	těžká
formu	forma	k1gFnSc4	forma
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
závažnější	závažný	k2eAgFnSc2d2	závažnější
formy	forma	k1gFnSc2	forma
horečky	horečka	k1gFnSc2	horečka
dengue	dengu	k1gFnSc2	dengu
mohou	moct	k5eAaImIp3nP	moct
pacienti	pacient	k1gMnPc1	pacient
potřebovat	potřebovat	k5eAaImF	potřebovat
aplikaci	aplikace	k1gFnSc4	aplikace
nitrožilního	nitrožilní	k2eAgInSc2d1	nitrožilní
roztoku	roztok	k1gInSc2	roztok
nebo	nebo	k8xC	nebo
krevní	krevní	k2eAgFnSc4d1	krevní
transfúzi	transfúze	k1gFnSc4	transfúze
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vzrůstá	vzrůstat	k5eAaImIp3nS	vzrůstat
počet	počet	k1gInSc1	počet
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
horečkou	horečka	k1gFnSc7	horečka
dengue	dengue	k1gInSc4	dengue
onemocněli	onemocnět	k5eAaPmAgMnP	onemocnět
<g/>
.	.	kIx.	.
</s>
<s>
Nemoc	nemoc	k1gFnSc1	nemoc
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
celosvětovým	celosvětový	k2eAgInSc7d1	celosvětový
problémem	problém	k1gInSc7	problém
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
běžná	běžný	k2eAgFnSc1d1	běžná
ve	v	k7c4	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
110	[number]	k4	110
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
onemocní	onemocnět	k5eAaPmIp3nS	onemocnět
touto	tento	k3xDgFnSc7	tento
chorobou	choroba	k1gFnSc7	choroba
asi	asi	k9	asi
50	[number]	k4	50
<g/>
–	–	k?	–
<g/>
100	[number]	k4	100
miliónů	milión	k4xCgInPc2	milión
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
pracují	pracovat	k5eAaImIp3nP	pracovat
na	na	k7c6	na
vývoji	vývoj	k1gInSc6	vývoj
vakcíny	vakcína	k1gFnSc2	vakcína
a	a	k8xC	a
léků	lék	k1gInPc2	lék
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
by	by	kYmCp3nS	by
působily	působit	k5eAaImAgFnP	působit
přímo	přímo	k6eAd1	přímo
proti	proti	k7c3	proti
viru	vir	k1gInSc3	vir
dengue	dengu	k1gFnSc2	dengu
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
záznamy	záznam	k1gInPc1	záznam
o	o	k7c6	o
horečce	horečka	k1gFnSc6	horečka
dengue	dengue	k6eAd1	dengue
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1779	[number]	k4	1779
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	stoletý	k2eAgMnPc1d1	stoletý
vědci	vědec	k1gMnPc1	vědec
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemoc	nemoc	k1gFnSc1	nemoc
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
virus	virus	k1gInSc4	virus
dengue	dengu	k1gInSc2	dengu
přenášený	přenášený	k2eAgInSc4d1	přenášený
komáry	komár	k1gMnPc7	komár
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
80	[number]	k4	80
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
nakazí	nakazit	k5eAaPmIp3nP	nakazit
virem	vir	k1gInSc7	vir
dengue	dengu	k1gFnSc2	dengu
<g/>
,	,	kIx,	,
nevykazuje	vykazovat	k5eNaImIp3nS	vykazovat
žádné	žádný	k3yNgNnSc1	žádný
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
mírné	mírný	k2eAgInPc1d1	mírný
symptomy	symptom	k1gInPc1	symptom
(	(	kIx(	(
<g/>
např.	např.	kA	např.
prostá	prostý	k2eAgFnSc1d1	prostá
horečka	horečka	k1gFnSc1	horečka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
přibližně	přibližně	k6eAd1	přibližně
5	[number]	k4	5
%	%	kIx~	%
nakažených	nakažený	k2eAgFnPc2d1	nakažená
osob	osoba	k1gFnPc2	osoba
má	mít	k5eAaImIp3nS	mít
nemoc	nemoc	k1gFnSc4	nemoc
závažnější	závažný	k2eAgInSc4d2	závažnější
průběh	průběh	k1gInSc4	průběh
<g/>
.	.	kIx.	.
</s>
<s>
Nízký	nízký	k2eAgInSc1d1	nízký
počet	počet	k1gInSc1	počet
nemocných	nemocný	k1gMnPc2	nemocný
je	být	k5eAaImIp3nS	být
chorobou	choroba	k1gFnSc7	choroba
ohrožen	ohrozit	k5eAaPmNgInS	ohrozit
na	na	k7c6	na
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Příznaky	příznak	k1gInPc1	příznak
se	se	k3xPyFc4	se
projevují	projevovat	k5eAaImIp3nP	projevovat
mezi	mezi	k7c7	mezi
3	[number]	k4	3
a	a	k8xC	a
14	[number]	k4	14
dny	den	k1gInPc7	den
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
nemocný	nemocný	k1gMnSc1	nemocný
viru	vir	k1gInSc2	vir
dengue	denguat	k5eAaPmIp3nS	denguat
vystaven	vystavit	k5eAaPmNgInS	vystavit
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
mezi	mezi	k7c7	mezi
4	[number]	k4	4
<g/>
.	.	kIx.	.
a	a	k8xC	a
7	[number]	k4	7
<g/>
.	.	kIx.	.
dnem	den	k1gInSc7	den
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
vrátil	vrátit	k5eAaPmAgMnS	vrátit
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
horečka	horečka	k1gFnSc1	horečka
dengue	dengue	k6eAd1	dengue
běžná	běžný	k2eAgFnSc1d1	běžná
<g/>
,	,	kIx,	,
a	a	k8xC	a
horečka	horečka	k1gFnSc1	horečka
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgInPc1d1	jiný
příznaky	příznak	k1gInPc1	příznak
se	se	k3xPyFc4	se
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
objeví	objevit	k5eAaPmIp3nS	objevit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
14	[number]	k4	14
dní	den	k1gInPc2	den
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejsou	být	k5eNaImIp3nP	být
způsobeny	způsobit	k5eAaPmNgFnP	způsobit
virem	vir	k1gInSc7	vir
dengue	dengue	k1gFnPc2	dengue
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
horečkou	horečka	k1gFnSc7	horečka
dengue	dengu	k1gInSc2	dengu
onemocní	onemocnět	k5eAaPmIp3nP	onemocnět
děti	dítě	k1gFnPc1	dítě
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
jsou	být	k5eAaImIp3nP	být
příznaky	příznak	k1gInPc4	příznak
totožné	totožný	k2eAgInPc4d1	totožný
se	s	k7c7	s
symptomy	symptom	k1gInPc7	symptom
nachlazení	nachlazení	k1gNnSc2	nachlazení
nebo	nebo	k8xC	nebo
gastroenteritidy	gastroenteritis	k1gFnSc2	gastroenteritis
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
střevní	střevní	k2eAgFnSc2d1	střevní
chřipky	chřipka	k1gFnSc2	chřipka
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
zvracení	zvracení	k1gNnSc1	zvracení
a	a	k8xC	a
průjem	průjem	k1gInSc1	průjem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
horečkou	horečka	k1gFnSc7	horečka
dengue	dengu	k1gFnSc2	dengu
onemocněly	onemocnět	k5eAaPmAgFnP	onemocnět
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
mít	mít	k5eAaImF	mít
závažné	závažný	k2eAgInPc4d1	závažný
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Klasickým	klasický	k2eAgInSc7d1	klasický
příznakem	příznak	k1gInSc7	příznak
horečky	horečka	k1gFnSc2	horečka
dengue	dengu	k1gFnSc2	dengu
je	být	k5eAaImIp3nS	být
náhlá	náhlý	k2eAgFnSc1d1	náhlá
horečka	horečka	k1gFnSc1	horečka
<g/>
,	,	kIx,	,
bolesti	bolest	k1gFnPc1	bolest
hlavy	hlava	k1gFnSc2	hlava
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
za	za	k7c7	za
očima	oko	k1gNnPc7	oko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyrážka	vyrážka	k1gFnSc1	vyrážka
a	a	k8xC	a
bolesti	bolest	k1gFnPc1	bolest
svalů	sval	k1gInPc2	sval
a	a	k8xC	a
kloubů	kloub	k1gInPc2	kloub
<g/>
.	.	kIx.	.
</s>
<s>
Lidový	lidový	k2eAgInSc1d1	lidový
název	název	k1gInSc1	název
nemoci	nemoc	k1gFnSc2	nemoc
−	−	k?	−
"	"	kIx"	"
<g/>
horečka	horečka	k1gFnSc1	horečka
lámající	lámající	k2eAgFnSc2d1	lámající
kosti	kost	k1gFnSc2	kost
<g/>
"	"	kIx"	"
−	−	k?	−
vystihuje	vystihovat	k5eAaImIp3nS	vystihovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
prudká	prudký	k2eAgFnSc1d1	prudká
bolest	bolest	k1gFnSc1	bolest
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
<g/>
.	.	kIx.	.
</s>
<s>
Horečka	horečka	k1gFnSc1	horečka
dengue	dengue	k1gNnSc2	dengue
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
fáze	fáze	k1gFnPc4	fáze
−	−	k?	−
febrilní	febrilní	k2eAgFnSc4d1	febrilní
<g/>
,	,	kIx,	,
kritickou	kritický	k2eAgFnSc4d1	kritická
a	a	k8xC	a
fázi	fáze	k1gFnSc4	fáze
rekonvalescence	rekonvalescence	k1gFnSc2	rekonvalescence
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
febrilní	febrilní	k2eAgFnSc2d1	febrilní
fáze	fáze	k1gFnSc2	fáze
obvykle	obvykle	k6eAd1	obvykle
nemocný	nemocný	k1gMnSc1	nemocný
trpí	trpět	k5eAaImIp3nS	trpět
vysokou	vysoký	k2eAgFnSc7d1	vysoká
horečkou	horečka	k1gFnSc7	horečka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
často	často	k6eAd1	často
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
40	[number]	k4	40
stupňů	stupeň	k1gInPc2	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
dostavit	dostavit	k5eAaPmF	dostavit
bolesti	bolest	k1gFnSc3	bolest
hlavy	hlava	k1gFnSc2	hlava
aj.	aj.	kA	aj.
Febrilní	febrilní	k2eAgFnSc2d1	febrilní
fáze	fáze	k1gFnSc2	fáze
trvá	trvat	k5eAaImIp3nS	trvat
2	[number]	k4	2
až	až	k9	až
7	[number]	k4	7
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
zhruba	zhruba	k6eAd1	zhruba
50	[number]	k4	50
až	až	k9	až
80	[number]	k4	80
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
symptomy	symptom	k1gInPc7	symptom
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
fázi	fáze	k1gFnSc6	fáze
objeví	objevit	k5eAaPmIp3nS	objevit
vyrážka	vyrážka	k1gFnSc1	vyrážka
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
vypadá	vypadat	k5eAaImIp3nS	vypadat
první	první	k4xOgInSc4	první
nebo	nebo	k8xC	nebo
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
jako	jako	k8xC	jako
zarudnutí	zarudnutí	k1gNnSc4	zarudnutí
kůže	kůže	k1gFnSc2	kůže
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
až	až	k9	až
7	[number]	k4	7
<g/>
.	.	kIx.	.
den	den	k1gInSc4	den
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
spalničky	spalničky	k1gFnPc1	spalničky
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
kůži	kůže	k1gFnSc6	kůže
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
objevit	objevit	k5eAaPmF	objevit
malé	malý	k2eAgFnPc1d1	malá
rudé	rudý	k2eAgFnPc1d1	rudá
skvrnky	skvrnka	k1gFnPc1	skvrnka
(	(	kIx(	(
<g/>
petechie	petechie	k1gFnPc1	petechie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
po	po	k7c6	po
zatlačení	zatlačení	k1gNnSc6	zatlačení
na	na	k7c4	na
kůži	kůže	k1gFnSc4	kůže
nemizí	mizet	k5eNaImIp3nS	mizet
<g/>
.	.	kIx.	.
</s>
<s>
Vznikají	vznikat	k5eAaImIp3nP	vznikat
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
prasklých	prasklý	k2eAgFnPc2d1	prasklá
kapilár	kapilára	k1gFnPc2	kapilára
<g/>
.	.	kIx.	.
</s>
<s>
Nemocný	mocný	k2eNgMnSc1d1	nemocný
může	moct	k5eAaImIp3nS	moct
trpět	trpět	k5eAaImF	trpět
i	i	k8xC	i
mírným	mírný	k2eAgNnSc7d1	mírné
krvácením	krvácení	k1gNnSc7	krvácení
ze	z	k7c2	z
sliznice	sliznice	k1gFnSc2	sliznice
nosu	nos	k1gInSc2	nos
a	a	k8xC	a
dutiny	dutina	k1gFnSc2	dutina
ústní	ústní	k2eAgFnSc2d1	ústní
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
horečka	horečka	k1gFnSc1	horečka
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
zlepší	zlepšit	k5eAaPmIp3nS	zlepšit
a	a	k8xC	a
pak	pak	k6eAd1	pak
znovu	znovu	k6eAd1	znovu
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
či	či	k8xC	či
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
vrátí	vrátit	k5eAaPmIp3nP	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Vzorec	vzorec	k1gInSc1	vzorec
průběhu	průběh	k1gInSc2	průběh
nemoci	nemoc	k1gFnSc2	nemoc
je	být	k5eAaImIp3nS	být
však	však	k9	však
u	u	k7c2	u
každého	každý	k3xTgMnSc2	každý
pacienta	pacient	k1gMnSc2	pacient
jiný	jiný	k2eAgInSc1d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
lidí	člověk	k1gMnPc2	člověk
se	se	k3xPyFc4	se
nemoc	nemoc	k1gFnSc1	nemoc
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
horečky	horečka	k1gFnSc2	horečka
rozvine	rozvinout	k5eAaPmIp3nS	rozvinout
do	do	k7c2	do
kritické	kritický	k2eAgFnSc2d1	kritická
fáze	fáze	k1gFnSc2	fáze
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
trvá	trvat	k5eAaImIp3nS	trvat
1	[number]	k4	1
až	až	k9	až
2	[number]	k4	2
dny	den	k1gInPc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
fáze	fáze	k1gFnSc2	fáze
se	se	k3xPyFc4	se
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
hrudi	hruď	k1gFnSc2	hruď
a	a	k8xC	a
břicha	břicho	k1gNnSc2	břicho
může	moct	k5eAaImIp3nS	moct
hromadit	hromadit	k5eAaImF	hromadit
tekutina	tekutina	k1gFnSc1	tekutina
unikající	unikající	k2eAgFnSc1d1	unikající
z	z	k7c2	z
malých	malý	k2eAgFnPc2d1	malá
cév	céva	k1gFnPc2	céva
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
již	již	k9	již
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
neobíhá	obíhat	k5eNaImIp3nS	obíhat
a	a	k8xC	a
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
pak	pak	k6eAd1	pak
nejsou	být	k5eNaImIp3nP	být
životně	životně	k6eAd1	životně
důležité	důležitý	k2eAgInPc1d1	důležitý
orgány	orgán	k1gInPc1	orgán
dostatečně	dostatečně	k6eAd1	dostatečně
zásobovány	zásobovat	k5eAaImNgInP	zásobovat
krví	krev	k1gFnSc7	krev
<g/>
;	;	kIx,	;
nemohou	moct	k5eNaImIp3nP	moct
tedy	tedy	k9	tedy
normálně	normálně	k6eAd1	normálně
fungovat	fungovat	k5eAaImF	fungovat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nemocného	nemocný	k1gMnSc2	nemocný
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
dostavit	dostavit	k5eAaPmF	dostavit
těžké	těžký	k2eAgNnSc4d1	těžké
krvácení	krvácení	k1gNnSc4	krvácení
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
z	z	k7c2	z
gastrointestinálního	gastrointestinální	k2eAgInSc2d1	gastrointestinální
traktu	trakt	k1gInSc2	trakt
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Selháním	selhání	k1gNnSc7	selhání
oběhového	oběhový	k2eAgInSc2d1	oběhový
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
šokovým	šokový	k2eAgInSc7d1	šokový
syndromem	syndrom	k1gInSc7	syndrom
dengue	dengu	k1gFnSc2	dengu
nebo	nebo	k8xC	nebo
hemoragickou	hemoragický	k2eAgFnSc7d1	hemoragická
horečkou	horečka	k1gFnSc7	horečka
dengue	dengu	k1gFnSc2	dengu
je	být	k5eAaImIp3nS	být
postiženo	postihnout	k5eAaPmNgNnS	postihnout
méně	málo	k6eAd2	málo
než	než	k8xS	než
5	[number]	k4	5
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
infikovaných	infikovaný	k2eAgMnPc2d1	infikovaný
virem	vir	k1gInSc7	vir
dengue	dengue	k1gFnSc7	dengue
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
nemocný	mocný	k2eNgMnSc1d1	nemocný
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
nakazil	nakazit	k5eAaPmAgInS	nakazit
jiným	jiný	k2eAgInSc7d1	jiný
typem	typ	k1gInSc7	typ
viru	vir	k1gInSc2	vir
dengue	dengu	k1gFnSc2	dengu
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
sekundární	sekundární	k2eAgFnSc1d1	sekundární
infekce	infekce	k1gFnSc1	infekce
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
tyto	tento	k3xDgFnPc4	tento
závažné	závažný	k2eAgFnPc4d1	závažná
potíže	potíž	k1gFnPc4	potíž
objeví	objevit	k5eAaPmIp3nS	objevit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
fázi	fáze	k1gFnSc6	fáze
rekonvalescence	rekonvalescence	k1gFnSc2	rekonvalescence
se	se	k3xPyFc4	se
tekutina	tekutina	k1gFnSc1	tekutina
uniklá	uniklý	k2eAgFnSc1d1	uniklá
z	z	k7c2	z
cév	céva	k1gFnPc2	céva
opět	opět	k6eAd1	opět
vstřebává	vstřebávat	k5eAaImIp3nS	vstřebávat
do	do	k7c2	do
krevního	krevní	k2eAgNnSc2d1	krevní
řečiště	řečiště	k1gNnSc2	řečiště
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
fáze	fáze	k1gFnSc1	fáze
trvá	trvat	k5eAaImIp3nS	trvat
2	[number]	k4	2
až	až	k9	až
3	[number]	k4	3
dny	den	k1gInPc4	den
a	a	k8xC	a
stav	stav	k1gInSc4	stav
nemocného	nemocný	k1gMnSc2	nemocný
se	se	k3xPyFc4	se
během	během	k7c2	během
ní	on	k3xPp3gFnSc2	on
často	často	k6eAd1	často
výrazně	výrazně	k6eAd1	výrazně
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
<g/>
.	.	kIx.	.
</s>
<s>
Pacienti	pacient	k1gMnPc1	pacient
ale	ale	k9	ale
mohou	moct	k5eAaImIp3nP	moct
trpět	trpět	k5eAaImF	trpět
těžkým	těžký	k2eAgNnSc7d1	těžké
svěděním	svědění	k1gNnSc7	svědění
a	a	k8xC	a
nízkou	nízký	k2eAgFnSc7d1	nízká
srdeční	srdeční	k2eAgFnSc7d1	srdeční
frekvencí	frekvence	k1gFnSc7	frekvence
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
fázi	fáze	k1gFnSc6	fáze
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
člověk	člověk	k1gMnSc1	člověk
dostat	dostat	k5eAaPmF	dostat
do	do	k7c2	do
stavu	stav	k1gInSc2	stav
přetížení	přetížení	k1gNnSc4	přetížení
oběhového	oběhový	k2eAgInSc2d1	oběhový
systému	systém	k1gInSc2	systém
tekutinou	tekutina	k1gFnSc7	tekutina
(	(	kIx(	(
<g/>
velký	velký	k2eAgInSc1d1	velký
objem	objem	k1gInSc1	objem
tekutin	tekutina	k1gFnPc2	tekutina
se	se	k3xPyFc4	se
vstřebává	vstřebávat	k5eAaImIp3nS	vstřebávat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
oběhu	oběh	k1gInSc2	oběh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
zasažen	zasažen	k2eAgInSc4d1	zasažen
mozek	mozek	k1gInSc4	mozek
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
záchvatům	záchvat	k1gInPc3	záchvat
nebo	nebo	k8xC	nebo
poruchám	porucha	k1gFnPc3	porucha
vědomí	vědomí	k1gNnSc2	vědomí
<g/>
.	.	kIx.	.
</s>
<s>
Virus	virus	k1gInSc1	virus
dengue	dengu	k1gFnSc2	dengu
může	moct	k5eAaImIp3nS	moct
někdy	někdy	k6eAd1	někdy
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
i	i	k9	i
další	další	k2eAgInPc4d1	další
tělesné	tělesný	k2eAgInPc4d1	tělesný
systémy	systém	k1gInPc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
trpět	trpět	k5eAaImF	trpět
pouze	pouze	k6eAd1	pouze
symptomy	symptom	k1gInPc1	symptom
těchto	tento	k3xDgFnPc2	tento
potíží	potíž	k1gFnPc2	potíž
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gFnPc2	jejich
kombinací	kombinace	k1gFnPc2	kombinace
s	s	k7c7	s
klasickými	klasický	k2eAgInPc7d1	klasický
příznaky	příznak	k1gInPc7	příznak
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Poruchy	poruch	k1gInPc1	poruch
vědomí	vědomí	k1gNnSc2	vědomí
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
u	u	k7c2	u
0,5	[number]	k4	0,5
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
%	%	kIx~	%
závažných	závažný	k2eAgInPc2d1	závažný
případů	případ	k1gInPc2	případ
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nP	docházet
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
virus	virus	k1gInSc1	virus
způsobil	způsobit	k5eAaPmAgInS	způsobit
infekci	infekce	k1gFnSc3	infekce
mozku	mozek	k1gInSc2	mozek
a	a	k8xC	a
také	také	k9	také
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
životně	životně	k6eAd1	životně
důležité	důležitý	k2eAgInPc1d1	důležitý
orgány	orgán	k1gInPc1	orgán
<g/>
,	,	kIx,	,
např.	např.	kA	např.
játra	játra	k1gNnPc4	játra
<g/>
,	,	kIx,	,
nefungují	fungovat	k5eNaImIp3nP	fungovat
normálně	normálně	k6eAd1	normálně
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
trpících	trpící	k2eAgMnPc2d1	trpící
horečkou	horečka	k1gFnSc7	horečka
dengue	dengu	k1gFnSc2	dengu
byla	být	k5eAaImAgFnS	být
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
i	i	k9	i
jiná	jiný	k2eAgNnPc1d1	jiné
neurologická	urologický	k2eNgNnPc1d1	neurologické
onemocnění	onemocnění	k1gNnPc1	onemocnění
(	(	kIx(	(
<g/>
onemocnění	onemocnění	k1gNnSc1	onemocnění
postihující	postihující	k2eAgNnSc1d1	postihující
mozek	mozek	k1gInSc4	mozek
a	a	k8xC	a
nervy	nerv	k1gInPc4	nerv
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Choroba	choroba	k1gFnSc1	choroba
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
transverzální	transverzální	k2eAgFnSc4d1	transverzální
myelitidu	myelitida	k1gFnSc4	myelitida
a	a	k8xC	a
Guillain-Barrého	Guillain-Barrý	k2eAgInSc2d1	Guillain-Barrý
syndrom	syndrom	k1gInSc4	syndrom
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
takové	takový	k3xDgInPc1	takový
případy	případ	k1gInPc1	případ
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
zřídka	zřídka	k6eAd1	zřídka
<g/>
,	,	kIx,	,
virus	virus	k1gInSc1	virus
může	moct	k5eAaImIp3nS	moct
zapříčinit	zapříčinit	k5eAaPmF	zapříčinit
i	i	k9	i
srdeční	srdeční	k2eAgFnSc4d1	srdeční
infekci	infekce	k1gFnSc4	infekce
a	a	k8xC	a
akutní	akutní	k2eAgFnPc4d1	akutní
jaterní	jaterní	k2eAgNnPc4d1	jaterní
selhání	selhání	k1gNnPc4	selhání
<g/>
.	.	kIx.	.
</s>
<s>
Horečka	horečka	k1gFnSc1	horečka
dengue	dengue	k1gFnPc2	dengue
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
virem	vir	k1gInSc7	vir
dengue	dengu	k1gFnSc2	dengu
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vědeckém	vědecký	k2eAgInSc6d1	vědecký
systému	systém	k1gInSc6	systém
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pojmenovává	pojmenovávat	k5eAaImIp3nS	pojmenovávat
a	a	k8xC	a
klasifikuje	klasifikovat	k5eAaImIp3nS	klasifikovat
viry	vir	k1gInPc4	vir
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
virus	virus	k1gInSc1	virus
dengue	dengu	k1gFnSc2	dengu
do	do	k7c2	do
čeledi	čeleď	k1gFnSc2	čeleď
Flaviviridae	Flavivirida	k1gFnSc2	Flavivirida
a	a	k8xC	a
rodu	rod	k1gInSc2	rod
Flavivirus	Flavivirus	k1gMnSc1	Flavivirus
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
téže	tenže	k3xDgFnSc3	tenže
čeledi	čeleď	k1gFnSc3	čeleď
náleží	náležet	k5eAaImIp3nP	náležet
i	i	k9	i
jiné	jiný	k2eAgInPc1d1	jiný
viry	vir	k1gInPc1	vir
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
další	další	k2eAgFnSc2d1	další
závažné	závažný	k2eAgFnSc2d1	závažná
choroby	choroba	k1gFnSc2	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
například	například	k6eAd1	například
o	o	k7c4	o
virus	virus	k1gInSc4	virus
žluté	žlutý	k2eAgFnSc2d1	žlutá
zimnice	zimnice	k1gFnSc2	zimnice
<g/>
,	,	kIx,	,
západonilský	západonilský	k2eAgInSc1d1	západonilský
virus	virus	k1gInSc1	virus
<g/>
,	,	kIx,	,
virus	virus	k1gInSc1	virus
encefalitidy	encefalitida	k1gFnSc2	encefalitida
St.	st.	kA	st.
Louis	Louis	k1gMnSc1	Louis
<g/>
,	,	kIx,	,
virus	virus	k1gInSc1	virus
japonské	japonský	k2eAgFnSc2d1	japonská
encefalitidy	encefalitida	k1gFnSc2	encefalitida
<g/>
,	,	kIx,	,
virus	virus	k1gInSc4	virus
klíšťové	klíšťový	k2eAgFnSc2d1	klíšťová
encefalitidy	encefalitida	k1gFnSc2	encefalitida
<g/>
,	,	kIx,	,
virus	virus	k1gInSc4	virus
nemoci	nemoc	k1gFnSc2	nemoc
kjasanurského	kjasanurský	k2eAgInSc2d1	kjasanurský
lesa	les	k1gInSc2	les
a	a	k8xC	a
virus	virus	k1gInSc1	virus
omské	omský	k2eAgFnSc2d1	Omská
hemoragické	hemoragický	k2eAgFnSc2d1	hemoragická
horečky	horečka	k1gFnSc2	horečka
−	−	k?	−
uvedené	uvedený	k2eAgInPc1d1	uvedený
viry	vir	k1gInPc1	vir
řadíme	řadit	k5eAaImIp1nP	řadit
do	do	k7c2	do
čeledi	čeleď	k1gFnSc2	čeleď
Flaviviridae	Flavivirida	k1gFnSc2	Flavivirida
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
těchto	tento	k3xDgInPc2	tento
virů	vir	k1gInPc2	vir
přenášejí	přenášet	k5eAaImIp3nP	přenášet
komáři	komár	k1gMnPc1	komár
nebo	nebo	k8xC	nebo
klíšťata	klíště	k1gNnPc1	klíště
<g/>
.	.	kIx.	.
</s>
<s>
Virus	virus	k1gInSc1	virus
dengue	dengu	k1gFnSc2	dengu
je	být	k5eAaImIp3nS	být
přenášen	přenášet	k5eAaImNgInS	přenášet
většinou	většinou	k6eAd1	většinou
komáry	komár	k1gMnPc4	komár
druhu	druh	k1gInSc2	druh
Aedes	Aedes	k1gMnSc1	Aedes
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Aedes	Aedes	k1gInSc1	Aedes
aegypti	aegypť	k1gFnSc2	aegypť
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
komárů	komár	k1gMnPc2	komár
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
mezi	mezi	k7c7	mezi
35	[number]	k4	35
<g/>
°	°	k?	°
severní	severní	k2eAgFnSc2d1	severní
šířky	šířka	k1gFnSc2	šířka
a	a	k8xC	a
35	[number]	k4	35
<g/>
°	°	k?	°
jižní	jižní	k2eAgFnSc2d1	jižní
šířky	šířka	k1gFnSc2	šířka
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
do	do	k7c2	do
1000	[number]	k4	1000
m.	m.	k?	m.
Útočí	útočit	k5eAaImIp3nP	útočit
většinou	většina	k1gFnSc7	většina
během	během	k7c2	během
dne	den	k1gInSc2	den
a	a	k8xC	a
člověka	člověk	k1gMnSc2	člověk
mohou	moct	k5eAaImIp3nP	moct
nakazit	nakazit	k5eAaPmF	nakazit
jediným	jediný	k2eAgNnSc7d1	jediné
štípnutím	štípnutí	k1gNnSc7	štípnutí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
komáři	komár	k1gMnPc1	komár
nakazit	nakazit	k5eAaPmF	nakazit
virem	vir	k1gInSc7	vir
dengue	dengu	k1gInSc2	dengu
od	od	k7c2	od
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
samice	samice	k1gFnSc1	samice
komára	komár	k1gMnSc2	komár
štípne	štípnout	k5eAaPmIp3nS	štípnout
nakaženého	nakažený	k2eAgMnSc4d1	nakažený
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
virem	vir	k1gInSc7	vir
infikovat	infikovat	k5eAaBmF	infikovat
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
virus	virus	k1gInSc1	virus
přežívá	přežívat	k5eAaImIp3nS	přežívat
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
vystýlají	vystýlat	k5eAaImIp3nP	vystýlat
trávicí	trávicí	k2eAgNnSc4d1	trávicí
ústrojí	ústrojí	k1gNnSc4	ústrojí
komára	komár	k1gMnSc2	komár
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
8	[number]	k4	8
až	až	k9	až
10	[number]	k4	10
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
virus	virus	k1gInSc1	virus
rozšíří	rozšířit	k5eAaPmIp3nS	rozšířit
do	do	k7c2	do
slinných	slinný	k2eAgFnPc2d1	slinná
žláz	žláza	k1gFnPc2	žláza
komára	komár	k1gMnSc2	komár
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
pak	pak	k6eAd1	pak
komár	komár	k1gMnSc1	komár
štípne	štípnout	k5eAaPmIp3nS	štípnout
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnPc4	jeho
sliny	slina	k1gFnPc4	slina
se	se	k3xPyFc4	se
dostanou	dostat	k5eAaPmIp3nP	dostat
do	do	k7c2	do
lidského	lidský	k2eAgNnSc2d1	lidské
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
dotyčného	dotyčný	k2eAgMnSc2d1	dotyčný
nakazit	nakazit	k5eAaPmF	nakazit
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
virus	virus	k1gInSc1	virus
nepůsobí	působit	k5eNaImIp3nS	působit
nakaženým	nakažený	k2eAgMnPc3d1	nakažený
komárům	komár	k1gMnPc3	komár
žádné	žádný	k3yNgFnPc1	žádný
zvláštní	zvláštní	k2eAgFnPc1d1	zvláštní
potíže	potíž	k1gFnPc1	potíž
−	−	k?	−
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
přenašeči	přenašeč	k1gMnPc1	přenašeč
viru	vir	k1gInSc2	vir
do	do	k7c2	do
konce	konec	k1gInSc2	konec
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Komáři	komár	k1gMnPc1	komár
Aedes	Aedesa	k1gFnPc2	Aedesa
aegypti	aegypti	k1gNnSc2	aegypti
jsou	být	k5eAaImIp3nP	být
nejpravděpodobnějšími	pravděpodobný	k2eAgMnPc7d3	nejpravděpodobnější
přenašeči	přenašeč	k1gMnPc7	přenašeč
viru	vir	k1gInSc2	vir
dengue	denguat	k5eAaPmIp3nS	denguat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
napadají	napadat	k5eAaBmIp3nP	napadat
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Vajíčka	vajíčko	k1gNnPc1	vajíčko
kladou	klást	k5eAaImIp3nP	klást
do	do	k7c2	do
uměle	uměle	k6eAd1	uměle
vzniklých	vzniklý	k2eAgFnPc2d1	vzniklá
vodních	vodní	k2eAgFnPc2d1	vodní
nádrží	nádrž	k1gFnPc2	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
Virus	virus	k1gInSc1	virus
dengue	dengu	k1gFnSc2	dengu
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
šířit	šířit	k5eAaImF	šířit
i	i	k9	i
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
infikovaných	infikovaný	k2eAgInPc2d1	infikovaný
krevních	krevní	k2eAgInPc2d1	krevní
výrobků	výrobek	k1gInPc2	výrobek
a	a	k8xC	a
dárcovství	dárcovství	k1gNnSc2	dárcovství
orgánů	orgán	k1gInPc2	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
člověk	člověk	k1gMnSc1	člověk
nakažený	nakažený	k2eAgInSc1d1	nakažený
virem	vir	k1gInSc7	vir
dengue	dengu	k1gInSc2	dengu
daruje	darovat	k5eAaPmIp3nS	darovat
krev	krev	k1gFnSc4	krev
nebo	nebo	k8xC	nebo
tělesný	tělesný	k2eAgInSc4d1	tělesný
orgán	orgán	k1gInSc4	orgán
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
příjemce	příjemce	k1gMnPc4	příjemce
virem	vir	k1gInSc7	vir
infikován	infikovat	k5eAaBmNgInS	infikovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
Singapuru	Singapur	k1gInSc6	Singapur
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
horečka	horečka	k1gFnSc1	horečka
dengue	dengue	k6eAd1	dengue
běžná	běžný	k2eAgFnSc1d1	běžná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takových	takový	k3xDgInPc6	takový
státech	stát	k1gInPc6	stát
je	být	k5eAaImIp3nS	být
virem	vir	k1gInSc7	vir
infikováno	infikován	k2eAgNnSc4d1	infikováno
1,6	[number]	k4	1,6
až	až	k9	až
6	[number]	k4	6
transfúzí	transfúze	k1gFnPc2	transfúze
z	z	k7c2	z
každých	každý	k3xTgInPc2	každý
10	[number]	k4	10
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Virus	virus	k1gInSc1	virus
se	se	k3xPyFc4	se
také	také	k9	také
může	moct	k5eAaImIp3nS	moct
přenést	přenést	k5eAaPmF	přenést
z	z	k7c2	z
matky	matka	k1gFnSc2	matka
na	na	k7c4	na
dítě	dítě	k1gNnSc4	dítě
během	během	k7c2	během
těhotenství	těhotenství	k1gNnSc2	těhotenství
nebo	nebo	k8xC	nebo
při	při	k7c6	při
porodu	porod	k1gInSc6	porod
<g/>
.	.	kIx.	.
</s>
<s>
Žádnou	žádný	k3yNgFnSc7	žádný
jinou	jiný	k2eAgFnSc7d1	jiná
formou	forma	k1gFnSc7	forma
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
nešíří	šířit	k5eNaImIp3nS	šířit
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nemluvňat	nemluvně	k1gNnPc2	nemluvně
a	a	k8xC	a
malých	malý	k2eAgFnPc2d1	malá
dětí	dítě	k1gFnPc2	dítě
s	s	k7c7	s
horečkou	horečka	k1gFnSc7	horečka
dengue	dengu	k1gInSc2	dengu
existuje	existovat	k5eAaImIp3nS	existovat
větší	veliký	k2eAgFnSc1d2	veliký
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
závažného	závažný	k2eAgNnSc2d1	závažné
onemocnění	onemocnění	k1gNnSc2	onemocnění
než	než	k8xS	než
u	u	k7c2	u
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
mnohem	mnohem	k6eAd1	mnohem
spíše	spíše	k9	spíše
těžce	těžce	k6eAd1	těžce
onemocní	onemocnět	k5eAaPmIp3nP	onemocnět
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
dobrém	dobrý	k2eAgInSc6d1	dobrý
fyzickém	fyzický	k2eAgInSc6d1	fyzický
stavu	stav	k1gInSc6	stav
(	(	kIx(	(
<g/>
zdravé	zdravý	k2eAgNnSc1d1	zdravé
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
živené	živený	k2eAgNnSc1d1	živené
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
odlišné	odlišný	k2eAgNnSc1d1	odlišné
od	od	k7c2	od
mnoha	mnoho	k4c2	mnoho
ostatních	ostatní	k2eAgFnPc2d1	ostatní
infekcí	infekce	k1gFnPc2	infekce
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yQgFnPc2	který
ke	k	k7c3	k
zhoršení	zhoršení	k1gNnSc3	zhoršení
obvykle	obvykle	k6eAd1	obvykle
dochází	docházet	k5eAaImIp3nS	docházet
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
podvyživených	podvyživený	k2eAgMnPc2d1	podvyživený
či	či	k8xC	či
nemocných	nemocný	k2eAgMnPc2d1	nemocný
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
vážného	vážný	k2eAgNnSc2d1	vážné
onemocnění	onemocnění	k1gNnSc2	onemocnění
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgMnSc1d2	vyšší
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
než	než	k8xS	než
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Horečka	horečka	k1gFnSc1	horečka
dengue	dengu	k1gInSc2	dengu
může	moct	k5eAaImIp3nS	moct
ohrožovat	ohrožovat	k5eAaImF	ohrožovat
život	život	k1gInSc4	život
osob	osoba	k1gFnPc2	osoba
s	s	k7c7	s
chronickými	chronický	k2eAgFnPc7d1	chronická
nemocemi	nemoc	k1gFnPc7	nemoc
<g/>
,	,	kIx,	,
např.	např.	kA	např.
cukrovkou	cukrovka	k1gFnSc7	cukrovka
a	a	k8xC	a
astmatem	astma	k1gNnSc7	astma
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
sání	sání	k1gNnSc2	sání
komára	komár	k1gMnSc2	komár
<g/>
,	,	kIx,	,
virus	virus	k1gInSc1	virus
proniká	pronikat	k5eAaImIp3nS	pronikat
společně	společně	k6eAd1	společně
s	s	k7c7	s
komářími	komáří	k2eAgFnPc7d1	komáří
slinami	slina	k1gFnPc7	slina
do	do	k7c2	do
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
průniku	průnik	k1gInSc6	průnik
virus	virus	k1gInSc1	virus
adheruje	adherovat	k5eAaBmIp3nS	adherovat
k	k	k7c3	k
bílé	bílý	k2eAgFnSc3d1	bílá
krvince	krvinka	k1gFnSc3	krvinka
a	a	k8xC	a
vstoupí	vstoupit	k5eAaPmIp3nS	vstoupit
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
bílé	bílý	k2eAgFnPc4d1	bílá
krvinky	krvinka	k1gFnPc4	krvinka
putují	putovat	k5eAaImIp3nP	putovat
tělem	tělo	k1gNnSc7	tělo
<g/>
,	,	kIx,	,
virus	virus	k1gInSc1	virus
se	se	k3xPyFc4	se
množí	množit	k5eAaImIp3nS	množit
<g/>
.	.	kIx.	.
</s>
<s>
Bílé	bílý	k2eAgFnPc1d1	bílá
krvinky	krvinka	k1gFnPc1	krvinka
reagují	reagovat	k5eAaBmIp3nP	reagovat
tvorbou	tvorba	k1gFnSc7	tvorba
množství	množství	k1gNnSc2	množství
signálních	signální	k2eAgInPc2d1	signální
proteinů	protein	k1gInPc2	protein
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
cytokinů	cytokin	k1gInPc2	cytokin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
interleukinů	interleukin	k1gInPc2	interleukin
<g/>
,	,	kIx,	,
interferonů	interferon	k1gInPc2	interferon
a	a	k8xC	a
faktorů	faktor	k1gInPc2	faktor
nádorové	nádorový	k2eAgFnPc1d1	nádorová
nekrózy	nekróza	k1gFnPc1	nekróza
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
proteiny	protein	k1gInPc1	protein
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
horečku	horečka	k1gFnSc4	horečka
<g/>
,	,	kIx,	,
chřipkové	chřipkový	k2eAgInPc1d1	chřipkový
symptomy	symptom	k1gInPc1	symptom
a	a	k8xC	a
silné	silný	k2eAgFnPc1d1	silná
bolesti	bolest	k1gFnPc1	bolest
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
horečku	horečka	k1gFnSc4	horečka
dengue	dengue	k6eAd1	dengue
typické	typický	k2eAgNnSc1d1	typické
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
osoba	osoba	k1gFnSc1	osoba
nakažena	nakažen	k2eAgFnSc1d1	nakažena
vážnou	vážný	k2eAgFnSc7d1	vážná
infekcí	infekce	k1gFnSc7	infekce
<g/>
,	,	kIx,	,
virus	virus	k1gInSc1	virus
se	se	k3xPyFc4	se
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
reprodukuje	reprodukovat	k5eAaBmIp3nS	reprodukovat
mnohem	mnohem	k6eAd1	mnohem
rychleji	rychle	k6eAd2	rychle
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
množství	množství	k1gNnSc1	množství
virů	vir	k1gInPc2	vir
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
podstatně	podstatně	k6eAd1	podstatně
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
,	,	kIx,	,
zasaženo	zasažen	k2eAgNnSc1d1	zasaženo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
více	hodně	k6eAd2	hodně
orgánů	orgán	k1gInPc2	orgán
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
játra	játra	k1gNnPc4	játra
a	a	k8xC	a
kostní	kostní	k2eAgFnSc4d1	kostní
dřeň	dřeň	k1gFnSc4	dřeň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
stěn	stěna	k1gFnPc2	stěna
malých	malý	k2eAgFnPc2d1	malá
cév	céva	k1gFnPc2	céva
v	v	k7c6	v
krevním	krevní	k2eAgNnSc6d1	krevní
řečišti	řečiště	k1gNnSc6	řečiště
uniká	unikat	k5eAaImIp3nS	unikat
krev	krev	k1gFnSc4	krev
do	do	k7c2	do
tělních	tělní	k2eAgFnPc2d1	tělní
dutin	dutina	k1gFnPc2	dutina
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
koluje	kolovat	k5eAaImIp3nS	kolovat
v	v	k7c6	v
žilách	žíla	k1gFnPc6	žíla
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
těle	tělo	k1gNnSc6	tělo
méně	málo	k6eAd2	málo
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Krevní	krevní	k2eAgInSc1d1	krevní
tlak	tlak	k1gInSc1	tlak
klesne	klesnout	k5eAaPmIp3nS	klesnout
tak	tak	k6eAd1	tak
nízko	nízko	k6eAd1	nízko
<g/>
,	,	kIx,	,
že	že	k8xS	že
srdce	srdce	k1gNnSc1	srdce
nezvládá	zvládat	k5eNaImIp3nS	zvládat
zásobovat	zásobovat	k5eAaImF	zásobovat
životně	životně	k6eAd1	životně
důležité	důležitý	k2eAgInPc4d1	důležitý
orgány	orgán	k1gInPc4	orgán
dostatkem	dostatek	k1gInSc7	dostatek
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Kostní	kostní	k2eAgFnSc1d1	kostní
dřeň	dřeň	k1gFnSc1	dřeň
rovněž	rovněž	k9	rovněž
není	být	k5eNaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
vytvářet	vytvářet	k5eAaImF	vytvářet
dostatek	dostatek	k1gInSc4	dostatek
krevních	krevní	k2eAgFnPc2d1	krevní
destiček	destička	k1gFnPc2	destička
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
potřebné	potřebné	k1gNnSc4	potřebné
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
krev	krev	k1gFnSc1	krev
správně	správně	k6eAd1	správně
srážela	srážet	k5eAaImAgFnS	srážet
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
dostatku	dostatek	k1gInSc2	dostatek
krevních	krevní	k2eAgFnPc2d1	krevní
destiček	destička	k1gFnPc2	destička
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
člověk	člověk	k1gMnSc1	člověk
mnohem	mnohem	k6eAd1	mnohem
náchylnější	náchylný	k2eAgFnPc1d2	náchylnější
k	k	k7c3	k
problémům	problém	k1gInPc3	problém
s	s	k7c7	s
krvácením	krvácení	k1gNnSc7	krvácení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
komplikací	komplikace	k1gFnSc7	komplikace
provázející	provázející	k2eAgFnSc4d1	provázející
horečku	horečka	k1gFnSc4	horečka
dengue	dengu	k1gFnSc2	dengu
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
choroba	choroba	k1gFnSc1	choroba
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
rané	raný	k2eAgFnSc6d1	raná
fázi	fáze	k1gFnSc6	fáze
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
ji	on	k3xPp3gFnSc4	on
odlišit	odlišit	k5eAaPmF	odlišit
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
virových	virový	k2eAgFnPc2d1	virová
infekcí	infekce	k1gFnPc2	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Nemocný	nemocný	k2eAgMnSc1d1	nemocný
má	mít	k5eAaImIp3nS	mít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
dengue	dengue	k6eAd1	dengue
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
trpí	trpět	k5eAaImIp3nS	trpět
horečkou	horečka	k1gFnSc7	horečka
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
dvěma	dva	k4xCgInPc7	dva
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
symptomů	symptom	k1gInPc2	symptom
−	−	k?	−
nevolností	nevolnost	k1gFnPc2	nevolnost
a	a	k8xC	a
zvracením	zvracení	k1gNnSc7	zvracení
<g/>
,	,	kIx,	,
vyrážkou	vyrážka	k1gFnSc7	vyrážka
<g/>
,	,	kIx,	,
celkovou	celkový	k2eAgFnSc7d1	celková
bolestí	bolest	k1gFnSc7	bolest
a	a	k8xC	a
nízkým	nízký	k2eAgInSc7d1	nízký
počtem	počet	k1gInSc7	počet
bílých	bílý	k2eAgFnPc2d1	bílá
krvinek	krvinka	k1gFnPc2	krvinka
nebo	nebo	k8xC	nebo
má	mít	k5eAaImIp3nS	mít
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
turniketový	turniketový	k2eAgInSc4d1	turniketový
test	test	k1gInSc4	test
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
horečka	horečka	k1gFnSc1	horečka
dengue	dengue	k6eAd1	dengue
běžná	běžný	k2eAgFnSc1d1	běžná
<g/>
,	,	kIx,	,
kterýkoli	kterýkoli	k3yIgMnSc1	kterýkoli
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
varovných	varovný	k2eAgInPc2d1	varovný
příznaků	příznak	k1gInPc2	příznak
společně	společně	k6eAd1	společně
s	s	k7c7	s
horečkou	horečka	k1gFnSc7	horečka
obvykle	obvykle	k6eAd1	obvykle
signalizují	signalizovat	k5eAaImIp3nP	signalizovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
osoba	osoba	k1gFnSc1	osoba
má	mít	k5eAaImIp3nS	mít
horečku	horečka	k1gFnSc4	horečka
dengue	dengu	k1gFnSc2	dengu
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
zhoršením	zhoršení	k1gNnSc7	zhoršení
stavu	stav	k1gInSc2	stav
nemocného	nemocný	k1gMnSc2	nemocný
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
objeví	objevit	k5eAaPmIp3nP	objevit
varovné	varovný	k2eAgInPc1d1	varovný
příznaky	příznak	k1gInPc1	příznak
<g/>
.	.	kIx.	.
</s>
<s>
Turniketový	turniketový	k2eAgInSc1d1	turniketový
test	test	k1gInSc1	test
je	být	k5eAaImIp3nS	být
vhodný	vhodný	k2eAgInSc1d1	vhodný
<g/>
,	,	kIx,	,
když	když	k8xS	když
nelze	lze	k6eNd1	lze
provést	provést	k5eAaPmF	provést
žádný	žádný	k3yNgInSc4	žádný
laboratorní	laboratorní	k2eAgInSc4d1	laboratorní
test	test	k1gInSc4	test
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
turniketovém	turniketový	k2eAgInSc6d1	turniketový
testu	test	k1gInSc6	test
lékař	lékař	k1gMnSc1	lékař
na	na	k7c6	na
5	[number]	k4	5
minut	minuta	k1gFnPc2	minuta
omotá	omotat	k5eAaPmIp3nS	omotat
škrtidlo	škrtidlo	k1gNnSc1	škrtidlo
kolem	kolem	k7c2	kolem
paže	paže	k1gFnSc2	paže
vyšetřované	vyšetřovaný	k2eAgFnSc2d1	vyšetřovaná
osoby	osoba	k1gFnSc2	osoba
a	a	k8xC	a
poté	poté	k6eAd1	poté
spočítá	spočítat	k5eAaPmIp3nS	spočítat
malé	malý	k2eAgFnPc4d1	malá
červené	červený	k2eAgFnPc4d1	červená
skvrnky	skvrnka	k1gFnPc4	skvrnka
na	na	k7c4	na
kůži	kůže	k1gFnSc4	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgInSc1d2	vyšší
počet	počet	k1gInSc1	počet
skvrnek	skvrnka	k1gFnPc2	skvrnka
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
osoba	osoba	k1gFnSc1	osoba
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nakazila	nakazit	k5eAaPmAgFnS	nakazit
horečkou	horečka	k1gFnSc7	horečka
dengue	dengue	k1gNnSc2	dengue
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
odlišit	odlišit	k5eAaPmF	odlišit
horečku	horečka	k1gFnSc4	horečka
dengue	dengu	k1gInSc2	dengu
a	a	k8xC	a
nemoc	nemoc	k1gFnSc4	nemoc
chikungunya	chikungunyum	k1gNnSc2	chikungunyum
<g/>
.	.	kIx.	.
</s>
<s>
Chikungunya	Chikungunya	k1gFnSc1	Chikungunya
je	být	k5eAaImIp3nS	být
podobná	podobný	k2eAgFnSc1d1	podobná
virová	virový	k2eAgFnSc1d1	virová
infekce	infekce	k1gFnSc1	infekce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
s	s	k7c7	s
horečkou	horečka	k1gFnSc7	horečka
dengue	dengue	k6eAd1	dengue
společných	společný	k2eAgInPc2d1	společný
mnoho	mnoho	k4c4	mnoho
symptomů	symptom	k1gInPc2	symptom
a	a	k8xC	a
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
stejných	stejný	k2eAgFnPc6d1	stejná
částech	část	k1gFnPc6	část
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Horečka	horečka	k1gFnSc1	horečka
dengue	dengu	k1gFnSc2	dengu
může	moct	k5eAaImIp3nS	moct
také	také	k9	také
mít	mít	k5eAaImF	mít
stejné	stejný	k2eAgInPc4d1	stejný
symptomy	symptom	k1gInPc4	symptom
jako	jako	k8xC	jako
některé	některý	k3yIgFnPc4	některý
jiné	jiný	k2eAgFnPc4d1	jiná
nemoci	nemoc	k1gFnPc4	nemoc
<g/>
,	,	kIx,	,
např.	např.	kA	např.
malárie	malárie	k1gFnSc2	malárie
<g/>
,	,	kIx,	,
leptospiróza	leptospiróza	k1gFnSc1	leptospiróza
<g/>
,	,	kIx,	,
tyfová	tyfový	k2eAgFnSc1d1	tyfová
horečka	horečka	k1gFnSc1	horečka
a	a	k8xC	a
meningokokové	meningokokový	k2eAgNnSc1d1	meningokokové
onemocnění	onemocnění	k1gNnSc1	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
stanovením	stanovení	k1gNnSc7	stanovení
diagnózy	diagnóza	k1gFnSc2	diagnóza
horečky	horečka	k1gFnSc2	horečka
dengue	dengu	k1gInSc2	dengu
lékaři	lékař	k1gMnPc1	lékař
provádějí	provádět	k5eAaImIp3nP	provádět
testy	test	k1gInPc4	test
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
člověk	člověk	k1gMnSc1	člověk
horečku	horečka	k1gFnSc4	horečka
dengue	dengu	k1gFnSc2	dengu
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
změn	změna	k1gFnPc2	změna
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
testy	test	k1gInPc4	test
odhalí	odhalit	k5eAaPmIp3nS	odhalit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nízký	nízký	k2eAgInSc1d1	nízký
počet	počet	k1gInSc1	počet
bílých	bílý	k2eAgFnPc2d1	bílá
krvinek	krvinka	k1gFnPc2	krvinka
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
příznaky	příznak	k1gInPc7	příznak
jsou	být	k5eAaImIp3nP	být
nízký	nízký	k2eAgInSc1d1	nízký
počet	počet	k1gInSc1	počet
krevních	krevní	k2eAgFnPc2d1	krevní
destiček	destička	k1gFnPc2	destička
a	a	k8xC	a
metabolická	metabolický	k2eAgFnSc1d1	metabolická
acidóza	acidóza	k1gFnSc1	acidóza
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
osoba	osoba	k1gFnSc1	osoba
trpí	trpět	k5eAaImIp3nP	trpět
vážnou	vážný	k2eAgFnSc7d1	vážná
formou	forma	k1gFnSc7	forma
horečky	horečka	k1gFnSc2	horečka
dengue	dengu	k1gFnSc2	dengu
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
z	z	k7c2	z
vyšetření	vyšetření	k1gNnSc2	vyšetření
krve	krev	k1gFnSc2	krev
vypozorovat	vypozorovat	k5eAaPmF	vypozorovat
další	další	k2eAgFnPc4d1	další
změny	změna	k1gFnPc4	změna
<g/>
.	.	kIx.	.
</s>
<s>
Závažná	závažný	k2eAgFnSc1d1	závažná
forma	forma	k1gFnSc1	forma
choroby	choroba	k1gFnSc2	choroba
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
únik	únik	k1gInSc1	únik
tekutiny	tekutina	k1gFnSc2	tekutina
z	z	k7c2	z
krevního	krevní	k2eAgNnSc2d1	krevní
řečiště	řečiště	k1gNnSc2	řečiště
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
hemokoncentraci	hemokoncentrace	k1gFnSc3	hemokoncentrace
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
méně	málo	k6eAd2	málo
krevní	krevní	k2eAgFnSc2d1	krevní
plazmy	plazma	k1gFnSc2	plazma
a	a	k8xC	a
více	hodně	k6eAd2	hodně
červených	červený	k2eAgFnPc2d1	červená
krvinek	krvinka	k1gFnPc2	krvinka
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
)	)	kIx)	)
a	a	k8xC	a
nízké	nízký	k2eAgFnSc6d1	nízká
hladině	hladina	k1gFnSc6	hladina
albuminu	albumin	k1gInSc2	albumin
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
.	.	kIx.	.
</s>
<s>
Vážná	vážný	k2eAgFnSc1d1	vážná
forma	forma	k1gFnSc1	forma
horečky	horečka	k1gFnSc2	horečka
dengue	dengue	k6eAd1	dengue
někdy	někdy	k6eAd1	někdy
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
hromadění	hromadění	k1gNnSc4	hromadění
pleurálního	pleurální	k2eAgInSc2d1	pleurální
výpotku	výpotek	k1gInSc2	výpotek
(	(	kIx(	(
<g/>
uniklá	uniklý	k2eAgFnSc1d1	uniklá
tekutina	tekutina	k1gFnSc1	tekutina
se	se	k3xPyFc4	se
hromadí	hromadit	k5eAaImIp3nS	hromadit
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
plic	plíce	k1gFnPc2	plíce
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
ascites	ascites	k1gInSc1	ascites
(	(	kIx(	(
<g/>
tekutina	tekutina	k1gFnSc1	tekutina
se	se	k3xPyFc4	se
hromadí	hromadit	k5eAaImIp3nS	hromadit
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
břicha	břicho	k1gNnSc2	břicho
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
tekutiny	tekutina	k1gFnSc2	tekutina
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
lékař	lékař	k1gMnSc1	lékař
tento	tento	k3xDgInSc4	tento
stav	stav	k1gInSc4	stav
diagnostikovat	diagnostikovat	k5eAaBmF	diagnostikovat
už	už	k6eAd1	už
při	při	k7c6	při
vyšetření	vyšetření	k1gNnSc6	vyšetření
osoby	osoba	k1gFnSc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Šokový	šokový	k2eAgInSc1d1	šokový
syndrom	syndrom	k1gInSc1	syndrom
dengue	dengue	k6eAd1	dengue
může	moct	k5eAaImIp3nS	moct
zdravotník	zdravotník	k1gMnSc1	zdravotník
včas	včas	k6eAd1	včas
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
pro	pro	k7c4	pro
identifikaci	identifikace	k1gFnSc4	identifikace
tekutiny	tekutina	k1gFnSc2	tekutina
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
lékařský	lékařský	k2eAgInSc1d1	lékařský
ultrazvuk	ultrazvuk	k1gInSc1	ultrazvuk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
horečka	horečka	k1gFnSc1	horečka
dengue	dengue	k6eAd1	dengue
běžná	běžný	k2eAgFnSc1d1	běžná
<g/>
,	,	kIx,	,
však	však	k9	však
většina	většina	k1gFnSc1	většina
zdravotníků	zdravotník	k1gMnPc2	zdravotník
a	a	k8xC	a
klinik	klinik	k1gMnSc1	klinik
ultrazvukové	ultrazvukový	k2eAgInPc4d1	ultrazvukový
přístroje	přístroj	k1gInPc4	přístroj
nemá	mít	k5eNaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
Světová	světový	k2eAgFnSc1d1	světová
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
organizace	organizace	k1gFnSc1	organizace
(	(	kIx(	(
<g/>
WHO	WHO	kA	WHO
<g/>
)	)	kIx)	)
horečku	horečka	k1gFnSc4	horečka
dengue	denguat	k5eAaPmIp3nS	denguat
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
typy	typ	k1gInPc4	typ
<g/>
:	:	kIx,	:
nekomplikovaný	komplikovaný	k2eNgInSc4d1	nekomplikovaný
a	a	k8xC	a
závažný	závažný	k2eAgInSc4d1	závažný
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
WHO	WHO	kA	WHO
nemoc	nemoc	k1gFnSc1	nemoc
dělila	dělit	k5eAaImAgFnS	dělit
na	na	k7c4	na
nediferencovanou	diferencovaný	k2eNgFnSc4d1	nediferencovaná
horečku	horečka	k1gFnSc4	horečka
<g/>
,	,	kIx,	,
horečku	horečka	k1gFnSc4	horečka
dengue	dengue	k1gFnSc4	dengue
a	a	k8xC	a
hemoragickou	hemoragický	k2eAgFnSc4d1	hemoragická
horečku	horečka	k1gFnSc4	horečka
dengue	dengu	k1gInSc2	dengu
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
staré	starý	k2eAgNnSc4d1	staré
členění	členění	k1gNnSc4	členění
zjednodušila	zjednodušit	k5eAaPmAgFnS	zjednodušit
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
se	se	k3xPyFc4	se
shodla	shodnout	k5eAaBmAgFnS	shodnout
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
staré	starý	k2eAgNnSc1d1	staré
členění	členění	k1gNnSc1	členění
bylo	být	k5eAaImAgNnS	být
omezující	omezující	k2eAgMnSc1d1	omezující
−	−	k?	−
nezahrnovalo	zahrnovat	k5eNaImAgNnS	zahrnovat
všechny	všechen	k3xTgFnPc4	všechen
formy	forma	k1gFnPc4	forma
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgFnPc6	který
se	se	k3xPyFc4	se
horečka	horečka	k1gFnSc1	horečka
dengue	dengue	k6eAd1	dengue
může	moct	k5eAaImIp3nS	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
byla	být	k5eAaImAgFnS	být
klasifikace	klasifikace	k1gFnSc1	klasifikace
nemoci	nemoc	k1gFnSc2	nemoc
oficiálně	oficiálně	k6eAd1	oficiálně
změněna	změněn	k2eAgFnSc1d1	změněna
<g/>
,	,	kIx,	,
starší	starý	k2eAgInSc1d2	starší
způsob	způsob	k1gInSc1	způsob
třídění	třídění	k1gNnSc2	třídění
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
starého	starý	k2eAgInSc2d1	starý
klasifikačního	klasifikační	k2eAgInSc2d1	klasifikační
systému	systém	k1gInSc2	systém
WHO	WHO	kA	WHO
byla	být	k5eAaImAgFnS	být
hemoragická	hemoragický	k2eAgFnSc1d1	hemoragická
horečka	horečka	k1gFnSc1	horečka
dengue	denguat	k5eAaPmIp3nS	denguat
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
čtyř	čtyři	k4xCgFnPc2	čtyři
fází	fáze	k1gFnPc2	fáze
zvaných	zvaný	k2eAgFnPc2d1	zvaná
stupeň	stupeň	k1gInSc4	stupeň
I	i	k9	i
<g/>
–	–	k?	–
<g/>
IV	Iva	k1gFnPc2	Iva
<g/>
:	:	kIx,	:
Stupeň	stupeň	k1gInSc1	stupeň
I	i	k8xC	i
–	–	k?	–
člověk	člověk	k1gMnSc1	člověk
má	mít	k5eAaImIp3nS	mít
horečku	horečka	k1gFnSc4	horečka
a	a	k8xC	a
snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
tvoří	tvořit	k5eAaImIp3nS	tvořit
modřiny	modřina	k1gFnPc4	modřina
nebo	nebo	k8xC	nebo
má	mít	k5eAaImIp3nS	mít
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
turniketový	turniketový	k2eAgInSc4d1	turniketový
test	test	k1gInSc4	test
<g/>
.	.	kIx.	.
</s>
<s>
Stupeň	stupeň	k1gInSc1	stupeň
II	II	kA	II
−	−	k?	−
člověk	člověk	k1gMnSc1	člověk
krvácí	krvácet	k5eAaImIp3nS	krvácet
do	do	k7c2	do
kůže	kůže	k1gFnSc2	kůže
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
částí	část	k1gFnPc2	část
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Stupeň	stupeň	k1gInSc1	stupeň
III	III	kA	III
−	−	k?	−
člověk	člověk	k1gMnSc1	člověk
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
známky	známka	k1gFnSc2	známka
oběhového	oběhový	k2eAgInSc2d1	oběhový
šoku	šok	k1gInSc2	šok
<g/>
.	.	kIx.	.
</s>
<s>
Stupeň	stupeň	k1gInSc1	stupeň
IV	IV	kA	IV
–	–	k?	–
člověk	člověk	k1gMnSc1	člověk
má	mít	k5eAaImIp3nS	mít
tak	tak	k6eAd1	tak
vážný	vážný	k2eAgInSc1d1	vážný
šok	šok	k1gInSc1	šok
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
krevní	krevní	k2eAgInSc1d1	krevní
tlak	tlak	k1gInSc1	tlak
klesá	klesat	k5eAaImIp3nS	klesat
na	na	k7c4	na
minimum	minimum	k1gNnSc4	minimum
a	a	k8xC	a
nelze	lze	k6eNd1	lze
nahmatat	nahmatat	k5eAaPmF	nahmatat
srdeční	srdeční	k2eAgInSc4d1	srdeční
tep	tep	k1gInSc4	tep
<g/>
.	.	kIx.	.
</s>
<s>
Stupně	stupeň	k1gInPc1	stupeň
III	III	kA	III
a	a	k8xC	a
IV	IV	kA	IV
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
"	"	kIx"	"
<g/>
šokový	šokový	k2eAgInSc4d1	šokový
syndrom	syndrom	k1gInSc4	syndrom
dengue	dengu	k1gFnSc2	dengu
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Horečku	horečka	k1gFnSc4	horečka
dengue	denguat	k5eAaPmIp3nS	denguat
lze	lze	k6eAd1	lze
diagnostikovat	diagnostikovat	k5eAaBmF	diagnostikovat
pomocí	pomocí	k7c2	pomocí
několika	několik	k4yIc2	několik
typů	typ	k1gInPc2	typ
mikrobiologických	mikrobiologický	k2eAgInPc2d1	mikrobiologický
laboratorních	laboratorní	k2eAgInPc2d1	laboratorní
testů	test	k1gInPc2	test
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
testů	test	k1gInPc2	test
(	(	kIx(	(
<g/>
izolace	izolace	k1gFnSc1	izolace
viru	vir	k1gInSc2	vir
<g/>
)	)	kIx)	)
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
virus	virus	k1gInSc1	virus
dengue	dengu	k1gFnSc2	dengu
v	v	k7c6	v
buněčných	buněčný	k2eAgFnPc6d1	buněčná
kulturách	kultura	k1gFnPc6	kultura
(	(	kIx(	(
<g/>
vzorcích	vzorec	k1gInPc6	vzorec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
z	z	k7c2	z
testů	test	k1gInPc2	test
(	(	kIx(	(
<g/>
detekce	detekce	k1gFnSc1	detekce
nukleových	nukleový	k2eAgFnPc2d1	nukleová
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
k	k	k7c3	k
vyhledávání	vyhledávání	k1gNnSc3	vyhledávání
nukleových	nukleový	k2eAgFnPc2d1	nukleová
kyselin	kyselina	k1gFnPc2	kyselina
viru	vir	k1gInSc2	vir
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
techniky	technika	k1gFnSc2	technika
zvané	zvaný	k2eAgFnSc2d1	zvaná
polymerázová	polymerázová	k1gFnSc1	polymerázová
řetězová	řetězový	k2eAgFnSc1d1	řetězová
reakce	reakce	k1gFnSc1	reakce
(	(	kIx(	(
<g/>
PCR	PCR	kA	PCR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc1	třetí
test	test	k1gInSc1	test
(	(	kIx(	(
<g/>
detekce	detekce	k1gFnSc1	detekce
antigenů	antigen	k1gInPc2	antigen
<g/>
)	)	kIx)	)
vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
antigeny	antigen	k1gInPc4	antigen
viru	vir	k1gInSc2	vir
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
test	test	k1gInSc1	test
určuje	určovat	k5eAaImIp3nS	určovat
protilátky	protilátka	k1gFnPc4	protilátka
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
tělo	tělo	k1gNnSc1	tělo
pro	pro	k7c4	pro
boj	boj	k1gInSc4	boj
s	s	k7c7	s
virem	vir	k1gInSc7	vir
dengue	dengue	k6eAd1	dengue
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
<g/>
.	.	kIx.	.
</s>
<s>
Izolace	izolace	k1gFnSc1	izolace
viru	vir	k1gInSc2	vir
a	a	k8xC	a
detekce	detekce	k1gFnSc2	detekce
nukleových	nukleový	k2eAgFnPc2d1	nukleová
kyselin	kyselina	k1gFnPc2	kyselina
fungují	fungovat	k5eAaImIp3nP	fungovat
lépe	dobře	k6eAd2	dobře
než	než	k8xS	než
detekce	detekce	k1gFnSc1	detekce
antigenu	antigen	k1gInSc2	antigen
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
testy	test	k1gInPc1	test
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
nákladnější	nákladný	k2eAgFnPc1d2	nákladnější
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
nejsou	být	k5eNaImIp3nP	být
dostupné	dostupný	k2eAgFnPc1d1	dostupná
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
horečka	horečka	k1gFnSc1	horečka
dengue	dengu	k1gFnSc2	dengu
v	v	k7c6	v
rané	raný	k2eAgFnSc6d1	raná
fázi	fáze	k1gFnSc6	fáze
<g/>
,	,	kIx,	,
žádný	žádný	k3yNgInSc1	žádný
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
testů	test	k1gInPc2	test
nemusí	muset	k5eNaImIp3nS	muset
prokázat	prokázat	k5eAaPmF	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
osoba	osoba	k1gFnSc1	osoba
nemocná	nemocný	k2eAgFnSc1d1	nemocná
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
testů	test	k1gInPc2	test
na	na	k7c4	na
protilátky	protilátka	k1gFnPc4	protilátka
mohou	moct	k5eAaImIp3nP	moct
laboratorní	laboratorní	k2eAgInPc1d1	laboratorní
testy	test	k1gInPc1	test
pomoci	pomoc	k1gFnSc2	pomoc
diagnostikovat	diagnostikovat	k5eAaBmF	diagnostikovat
horečku	horečka	k1gFnSc4	horečka
dengue	dengue	k6eAd1	dengue
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
akutní	akutní	k2eAgFnSc6d1	akutní
(	(	kIx(	(
<g/>
počáteční	počáteční	k2eAgFnSc6d1	počáteční
<g/>
)	)	kIx)	)
fázi	fáze	k1gFnSc6	fáze
<g/>
.	.	kIx.	.
</s>
<s>
Testy	test	k1gInPc1	test
na	na	k7c4	na
protilátky	protilátka	k1gFnPc4	protilátka
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
potvrdit	potvrdit	k5eAaPmF	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
má	mít	k5eAaImIp3nS	mít
horečku	horečka	k1gFnSc4	horečka
dengue	dengu	k1gFnSc2	dengu
i	i	k9	i
v	v	k7c6	v
pozdějších	pozdní	k2eAgFnPc6d2	pozdější
fázích	fáze	k1gFnPc6	fáze
infekce	infekce	k1gFnSc2	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Protilátky	protilátka	k1gFnPc1	protilátka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
bojují	bojovat	k5eAaImIp3nP	bojovat
s	s	k7c7	s
virem	vir	k1gInSc7	vir
dengue	dengu	k1gInSc2	dengu
<g/>
,	,	kIx,	,
lidské	lidský	k2eAgNnSc1d1	lidské
tělo	tělo	k1gNnSc1	tělo
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
po	po	k7c4	po
5	[number]	k4	5
až	až	k9	až
7	[number]	k4	7
dnech	den	k1gInPc6	den
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
nákaze	nákaza	k1gFnSc3	nákaza
virem	vir	k1gInSc7	vir
dengue	dengue	k6eAd1	dengue
neexistují	existovat	k5eNaImIp3nP	existovat
žádné	žádný	k3yNgFnPc4	žádný
schválené	schválený	k2eAgFnPc4d1	schválená
vakcíny	vakcína	k1gFnPc4	vakcína
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
prevenci	prevence	k1gFnSc4	prevence
před	před	k7c7	před
infekcí	infekce	k1gFnSc7	infekce
Světová	světový	k2eAgFnSc1d1	světová
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
organizace	organizace	k1gFnSc1	organizace
(	(	kIx(	(
<g/>
WHO	WHO	kA	WHO
<g/>
)	)	kIx)	)
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
regulaci	regulace	k1gFnSc4	regulace
populace	populace	k1gFnSc2	populace
komárů	komár	k1gMnPc2	komár
a	a	k8xC	a
ochranu	ochrana	k1gFnSc4	ochrana
před	před	k7c7	před
štípnutím	štípnutí	k1gNnSc7	štípnutí
komáry	komár	k1gMnPc4	komár
<g/>
.	.	kIx.	.
</s>
<s>
WHO	WHO	kA	WHO
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
program	program	k1gInSc4	program
prevence	prevence	k1gFnSc2	prevence
proti	proti	k7c3	proti
horečce	horečka	k1gFnSc3	horečka
dengue	dengue	k1gFnPc3	dengue
(	(	kIx(	(
<g/>
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
"	"	kIx"	"
<g/>
Integrated	Integrated	k1gInSc1	Integrated
Vector	Vector	k1gInSc1	Vector
Control	Control	k1gInSc4	Control
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
pěti	pět	k4xCc2	pět
odlišných	odlišný	k2eAgFnPc2d1	odlišná
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
Prosazování	prosazování	k1gNnSc1	prosazování
důležitých	důležitý	k2eAgNnPc2d1	důležité
opatření	opatření	k1gNnPc2	opatření
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgFnSc1d1	sociální
mobilizace	mobilizace	k1gFnSc1	mobilizace
a	a	k8xC	a
legislativa	legislativa	k1gFnSc1	legislativa
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
vést	vést	k5eAaImF	vést
k	k	k7c3	k
posílení	posílení	k1gNnSc3	posílení
zdravotnických	zdravotnický	k2eAgFnPc2d1	zdravotnická
organizací	organizace	k1gFnPc2	organizace
a	a	k8xC	a
komunit	komunita	k1gFnPc2	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
složky	složka	k1gFnPc1	složka
společnosti	společnost	k1gFnSc2	společnost
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
veřejný	veřejný	k2eAgInSc1d1	veřejný
sektor	sektor	k1gInSc1	sektor
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vládu	vláda	k1gFnSc4	vláda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
soukromý	soukromý	k2eAgInSc1d1	soukromý
sektor	sektor	k1gInSc1	sektor
(	(	kIx(	(
<g/>
např.	např.	kA	např.
podniky	podnik	k1gInPc1	podnik
<g/>
)	)	kIx)	)
a	a	k8xC	a
sektor	sektor	k1gInSc1	sektor
zdravotnické	zdravotnický	k2eAgFnSc2d1	zdravotnická
péče	péče	k1gFnSc2	péče
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
způsoby	způsob	k1gInPc1	způsob
regulace	regulace	k1gFnSc2	regulace
nemoci	nemoc	k1gFnSc2	nemoc
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
integrovány	integrovat	k5eAaBmNgFnP	integrovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
dostupné	dostupný	k2eAgInPc1d1	dostupný
zdroje	zdroj	k1gInPc1	zdroj
měly	mít	k5eAaImAgInP	mít
co	co	k9	co
největší	veliký	k2eAgInSc4d3	veliký
účinek	účinek	k1gInSc4	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
by	by	kYmCp3nP	by
měla	mít	k5eAaImAgNnP	mít
být	být	k5eAaImF	být
činěna	činit	k5eAaImNgNnP	činit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
prokazatelných	prokazatelný	k2eAgInPc2d1	prokazatelný
faktů	fakt	k1gInPc2	fakt
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
boj	boj	k1gInSc1	boj
s	s	k7c7	s
horečkou	horečka	k1gFnSc7	horečka
dengue	dengue	k1gInSc4	dengue
účinný	účinný	k2eAgInSc4d1	účinný
<g/>
.	.	kIx.	.
</s>
<s>
Postiženým	postižený	k2eAgFnPc3d1	postižená
oblastem	oblast	k1gFnPc3	oblast
by	by	kYmCp3nP	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
poskytnuta	poskytnut	k2eAgFnSc1d1	poskytnuta
pomoc	pomoc	k1gFnSc1	pomoc
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgFnP	být
schopny	schopen	k2eAgFnPc1d1	schopna
na	na	k7c4	na
potíže	potíž	k1gFnPc4	potíž
související	související	k2eAgFnPc4d1	související
s	s	k7c7	s
nemocí	nemoc	k1gFnSc7	nemoc
reagovat	reagovat	k5eAaBmF	reagovat
samy	sám	k3xTgMnPc4	sám
<g/>
.	.	kIx.	.
</s>
<s>
WHO	WHO	kA	WHO
rovněž	rovněž	k9	rovněž
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
učinit	učinit	k5eAaPmF	učinit
specifické	specifický	k2eAgInPc4d1	specifický
kroky	krok	k1gInPc4	krok
pro	pro	k7c4	pro
regulaci	regulace	k1gFnSc4	regulace
počtu	počet	k1gInSc2	počet
komárů	komár	k1gMnPc2	komár
a	a	k8xC	a
zajištění	zajištění	k1gNnSc4	zajištění
ochrany	ochrana	k1gFnSc2	ochrana
před	před	k7c7	před
štípnutím	štípnutí	k1gNnSc7	štípnutí
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepším	dobrý	k2eAgInSc7d3	nejlepší
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
regulovat	regulovat	k5eAaImF	regulovat
počet	počet	k1gInSc4	počet
komárů	komár	k1gMnPc2	komár
druhu	druh	k1gInSc2	druh
"	"	kIx"	"
<g/>
Aedes	Aedes	k1gInSc1	Aedes
aegypti	aegypť	k1gFnSc2	aegypť
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zbavit	zbavit	k5eAaPmF	zbavit
se	se	k3xPyFc4	se
míst	místo	k1gNnPc2	místo
jejich	jejich	k3xOp3gInSc2	jejich
výskytu	výskyt	k1gInSc2	výskyt
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
by	by	kYmCp3nP	by
měli	mít	k5eAaImAgMnP	mít
vyprazdňovat	vyprazdňovat	k5eAaImF	vyprazdňovat
otevřené	otevřený	k2eAgFnSc2d1	otevřená
vodní	vodní	k2eAgFnSc2d1	vodní
nádrže	nádrž	k1gFnSc2	nádrž
(	(	kIx(	(
<g/>
aby	aby	kYmCp3nP	aby
do	do	k7c2	do
nich	on	k3xPp3gInPc2	on
komáři	komár	k1gMnPc1	komár
nemohli	moct	k5eNaImAgMnP	moct
naklást	naklást	k5eAaPmF	naklást
vajíčka	vajíčko	k1gNnPc4	vajíčko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
regulaci	regulace	k1gFnSc4	regulace
komárů	komár	k1gMnPc2	komár
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
oblastech	oblast	k1gFnPc6	oblast
lze	lze	k6eAd1	lze
rovněž	rovněž	k9	rovněž
použít	použít	k5eAaPmF	použít
insekticidy	insekticid	k1gInPc4	insekticid
nebo	nebo	k8xC	nebo
prostředky	prostředek	k1gInPc4	prostředek
biologické	biologický	k2eAgFnSc2d1	biologická
regulace	regulace	k1gFnSc2	regulace
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
postřiky	postřik	k1gInPc7	postřik
organofosfátovými	organofosfátův	k2eAgInPc7d1	organofosfátův
nebo	nebo	k8xC	nebo
pyrethroidovými	pyrethroidový	k2eAgInPc7d1	pyrethroidový
insekticidy	insekticid	k1gInPc7	insekticid
nepomáhají	pomáhat	k5eNaImIp3nP	pomáhat
<g/>
.	.	kIx.	.
</s>
<s>
Plochy	plocha	k1gFnPc1	plocha
stojatých	stojatý	k2eAgFnPc2d1	stojatá
vod	voda	k1gFnPc2	voda
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
odstraněny	odstranit	k5eAaPmNgInP	odstranit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
komáry	komár	k1gMnPc4	komár
přitahují	přitahovat	k5eAaImIp3nP	přitahovat
<g/>
;	;	kIx,	;
lidé	člověk	k1gMnPc1	člověk
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
rovněž	rovněž	k9	rovněž
zdravotní	zdravotní	k2eAgInPc4d1	zdravotní
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
insekticidy	insekticid	k1gInPc7	insekticid
začnou	začít	k5eAaPmIp3nP	začít
ve	v	k7c6	v
stojaté	stojatý	k2eAgFnSc6d1	stojatá
vodě	voda	k1gFnSc6	voda
usazovat	usazovat	k5eAaImF	usazovat
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
prevenci	prevence	k1gFnSc4	prevence
před	před	k7c7	před
napadením	napadení	k1gNnSc7	napadení
komáry	komár	k1gMnPc7	komár
mohou	moct	k5eAaImIp3nP	moct
lidé	člověk	k1gMnPc1	člověk
nosit	nosit	k5eAaImF	nosit
oblečení	oblečení	k1gNnSc4	oblečení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zcela	zcela	k6eAd1	zcela
zakrývá	zakrývat	k5eAaImIp3nS	zakrývat
pokožku	pokožka	k1gFnSc4	pokožka
<g/>
,	,	kIx,	,
a	a	k8xC	a
používat	používat	k5eAaImF	používat
repelent	repelent	k1gInSc4	repelent
proti	proti	k7c3	proti
hmyzu	hmyz	k1gInSc3	hmyz
(	(	kIx(	(
<g/>
nejlépe	dobře	k6eAd3	dobře
funguje	fungovat	k5eAaImIp3nS	fungovat
DEET	DEET	kA	DEET
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
odpočinku	odpočinek	k1gInSc6	odpočinek
mohou	moct	k5eAaImIp3nP	moct
lidé	člověk	k1gMnPc1	člověk
rovněž	rovněž	k9	rovněž
používat	používat	k5eAaImF	používat
sítě	síť	k1gFnPc4	síť
proti	proti	k7c3	proti
moskytům	moskyt	k1gMnPc3	moskyt
<g/>
.	.	kIx.	.
</s>
<s>
Jednotný	jednotný	k2eAgInSc1d1	jednotný
způsob	způsob	k1gInSc1	způsob
léčby	léčba	k1gFnSc2	léčba
horečky	horečka	k1gFnSc2	horečka
dengue	dengue	k6eAd1	dengue
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
lidí	člověk	k1gMnPc2	člověk
postačí	postačit	k5eAaPmIp3nS	postačit
domácí	domácí	k2eAgNnSc4d1	domácí
léčení	léčení	k1gNnSc4	léčení
a	a	k8xC	a
dostatečný	dostatečný	k2eAgInSc4d1	dostatečný
příjem	příjem	k1gInSc4	příjem
tekutin	tekutina	k1gFnPc2	tekutina
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
lékaře	lékař	k1gMnSc2	lékař
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jiní	jiný	k2eAgMnPc1d1	jiný
mohou	moct	k5eAaImIp3nP	moct
potřebovat	potřebovat	k5eAaImF	potřebovat
podání	podání	k1gNnSc4	podání
nitrožilního	nitrožilní	k2eAgInSc2d1	nitrožilní
roztoku	roztok	k1gInSc2	roztok
a	a	k8xC	a
krevní	krevní	k2eAgFnSc4d1	krevní
transfúzi	transfúze	k1gFnSc4	transfúze
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
vyskytnou	vyskytnout	k5eAaPmIp3nP	vyskytnout
závažné	závažný	k2eAgInPc4d1	závažný
varovné	varovný	k2eAgInPc4d1	varovný
příznaky	příznak	k1gInPc4	příznak
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokud	pokud	k8xS	pokud
trpí	trpět	k5eAaImIp3nS	trpět
chronickými	chronický	k2eAgInPc7d1	chronický
zdravotními	zdravotní	k2eAgInPc7d1	zdravotní
problémy	problém	k1gInPc7	problém
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
lékař	lékař	k1gMnSc1	lékař
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
hospitalizaci	hospitalizace	k1gFnSc6	hospitalizace
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
nakažené	nakažený	k2eAgFnPc1d1	nakažená
osoby	osoba	k1gFnPc1	osoba
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
podání	podání	k1gNnSc4	podání
nitrožilního	nitrožilní	k2eAgInSc2d1	nitrožilní
roztoku	roztok	k1gInSc2	roztok
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
obvykle	obvykle	k6eAd1	obvykle
pouze	pouze	k6eAd1	pouze
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
jednoho	jeden	k4xCgInSc2	jeden
či	či	k8xC	či
dvou	dva	k4xCgInPc2	dva
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Lékař	lékař	k1gMnSc1	lékař
množství	množství	k1gNnSc2	množství
podávaného	podávaný	k2eAgInSc2d1	podávaný
roztoku	roztok	k1gInSc2	roztok
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
člověk	člověk	k1gMnSc1	člověk
vylučoval	vylučovat	k5eAaImAgMnS	vylučovat
určité	určitý	k2eAgNnSc4d1	určité
množství	množství	k1gNnSc4	množství
moči	moč	k1gFnSc2	moč
(	(	kIx(	(
<g/>
0,5	[number]	k4	0,5
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
ml	ml	kA	ml
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
hod	hod	k1gInSc1	hod
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
podávaného	podávaný	k2eAgInSc2d1	podávaný
roztoku	roztok	k1gInSc2	roztok
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
hematokrit	hematokrit	k1gInSc1	hematokrit
(	(	kIx(	(
<g/>
množství	množství	k1gNnSc1	množství
železa	železo	k1gNnSc2	železo
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
)	)	kIx)	)
a	a	k8xC	a
vitální	vitální	k2eAgInPc1d1	vitální
znaky	znak	k1gInPc1	znak
nemocného	nemocný	k1gMnSc2	nemocný
nevrátí	vrátit	k5eNaPmIp3nP	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
normálu	normál	k1gInSc2	normál
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
riziku	riziko	k1gNnSc3	riziko
krvácení	krvácení	k1gNnSc2	krvácení
se	se	k3xPyFc4	se
lékaři	lékař	k1gMnPc1	lékař
snaží	snažit	k5eAaImIp3nP	snažit
vyhnout	vyhnout	k5eAaPmF	vyhnout
invazivním	invazivní	k2eAgInPc3d1	invazivní
lékařským	lékařský	k2eAgInPc3d1	lékařský
zákrokům	zákrok	k1gInPc3	zákrok
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
nasogastrická	nasogastrický	k2eAgFnSc1d1	nasogastrická
intubace	intubace	k1gFnSc1	intubace
(	(	kIx(	(
<g/>
zavedení	zavedení	k1gNnSc1	zavedení
sondy	sonda	k1gFnSc2	sonda
do	do	k7c2	do
žaludku	žaludek	k1gInSc2	žaludek
přes	přes	k7c4	přes
nosní	nosní	k2eAgFnSc4d1	nosní
dutinu	dutina	k1gFnSc4	dutina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
intramuskulární	intramuskulární	k2eAgFnPc1d1	intramuskulární
injekce	injekce	k1gFnPc1	injekce
(	(	kIx(	(
<g/>
podání	podání	k1gNnSc1	podání
léku	lék	k1gInSc2	lék
injekcí	injekce	k1gFnSc7	injekce
vpichovanou	vpichovaný	k2eAgFnSc7d1	vpichovaná
do	do	k7c2	do
svalu	sval	k1gInSc2	sval
<g/>
)	)	kIx)	)
a	a	k8xC	a
arteriální	arteriální	k2eAgFnSc1d1	arteriální
punkce	punkce	k1gFnSc1	punkce
(	(	kIx(	(
<g/>
vpíchnutí	vpíchnutí	k1gNnSc1	vpíchnutí
jehly	jehla	k1gFnSc2	jehla
do	do	k7c2	do
tepny	tepna	k1gFnSc2	tepna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
horečku	horečka	k1gFnSc4	horečka
a	a	k8xC	a
bolest	bolest	k1gFnSc4	bolest
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
podávat	podávat	k5eAaImF	podávat
acetaminofen	acetaminofen	k2eAgInSc4d1	acetaminofen
(	(	kIx(	(
<g/>
Tylenol	Tylenol	k1gInSc4	Tylenol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protizánětlivé	protizánětlivý	k2eAgInPc1d1	protizánětlivý
léky	lék	k1gInPc1	lék
typu	typ	k1gInSc2	typ
NSAID	NSAID	kA	NSAID
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
například	například	k6eAd1	například
ibuprofen	ibuprofen	k1gInSc1	ibuprofen
a	a	k8xC	a
aspirin	aspirin	k1gInSc1	aspirin
<g/>
)	)	kIx)	)
by	by	kYmCp3nP	by
neměly	mít	k5eNaImAgInP	mít
být	být	k5eAaImF	být
podávány	podáván	k2eAgInPc1d1	podáván
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
pravděpodobnost	pravděpodobnost	k1gFnSc4	pravděpodobnost
krvácení	krvácení	k1gNnSc2	krvácení
<g/>
.	.	kIx.	.
</s>
<s>
Podávání	podávání	k1gNnSc1	podávání
krevních	krevní	k2eAgFnPc2d1	krevní
transfúzí	transfúze	k1gFnPc2	transfúze
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
zahájeno	zahájit	k5eAaPmNgNnS	zahájit
ihned	ihned	k6eAd1	ihned
<g/>
,	,	kIx,	,
jakmile	jakmile	k8xS	jakmile
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
odchylce	odchylka	k1gFnSc3	odchylka
vitálních	vitální	k2eAgInPc2d1	vitální
znaků	znak	k1gInPc2	znak
od	od	k7c2	od
normálu	normál	k1gInSc2	normál
nebo	nebo	k8xC	nebo
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
začne	začít	k5eAaPmIp3nS	začít
snižovat	snižovat	k5eAaImF	snižovat
množství	množství	k1gNnSc4	množství
červených	červený	k2eAgFnPc2d1	červená
krvinek	krvinka	k1gFnPc2	krvinka
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
krevní	krevní	k2eAgFnSc2d1	krevní
transfúze	transfúze	k1gFnSc2	transfúze
by	by	kYmCp3nS	by
člověku	člověk	k1gMnSc3	člověk
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
podána	podat	k5eAaPmNgFnS	podat
plná	plný	k2eAgFnSc1d1	plná
krev	krev	k1gFnSc1	krev
(	(	kIx(	(
<g/>
krev	krev	k1gFnSc1	krev
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nebyla	být	k5eNaImAgFnS	být
separována	separovat	k5eAaBmNgFnS	separovat
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
složky	složka	k1gFnPc4	složka
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
červené	červený	k2eAgFnPc4d1	červená
krvinky	krvinka	k1gFnPc4	krvinka
<g/>
.	.	kIx.	.
</s>
<s>
Transfúze	transfúze	k1gFnSc1	transfúze
krevních	krevní	k2eAgFnPc2d1	krevní
destiček	destička	k1gFnPc2	destička
(	(	kIx(	(
<g/>
separovaných	separovaný	k2eAgMnPc2d1	separovaný
z	z	k7c2	z
plné	plný	k2eAgFnSc2d1	plná
krve	krev	k1gFnSc2	krev
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
čerstvě	čerstvě	k6eAd1	čerstvě
zmrazené	zmrazený	k2eAgFnPc1d1	zmrazená
krevní	krevní	k2eAgFnPc1d1	krevní
plazmy	plazma	k1gFnPc1	plazma
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
nedoporučuje	doporučovat	k5eNaImIp3nS	doporučovat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
fázi	fáze	k1gFnSc6	fáze
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
nemocný	nemocný	k2eAgMnSc1d1	nemocný
z	z	k7c2	z
horečky	horečka	k1gFnSc2	horečka
dengue	dengue	k6eAd1	dengue
již	již	k6eAd1	již
zotavuje	zotavovat	k5eAaImIp3nS	zotavovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nitrožilní	nitrožilní	k2eAgInSc1d1	nitrožilní
roztok	roztok	k1gInSc1	roztok
dále	daleko	k6eAd2	daleko
nepodává	podávat	k5eNaImIp3nS	podávat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
přetížení	přetížení	k1gNnSc3	přetížení
oběhu	oběh	k1gInSc2	oběh
tekutinou	tekutina	k1gFnSc7	tekutina
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
nastane	nastat	k5eAaPmIp3nS	nastat
a	a	k8xC	a
vitální	vitální	k2eAgInPc1d1	vitální
znaky	znak	k1gInPc1	znak
osoby	osoba	k1gFnSc2	osoba
jsou	být	k5eAaImIp3nP	být
stabilní	stabilní	k2eAgFnPc1d1	stabilní
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
stačí	stačit	k5eAaBmIp3nS	stačit
pozastavit	pozastavit	k5eAaPmF	pozastavit
příjem	příjem	k1gInSc4	příjem
dalších	další	k2eAgFnPc2d1	další
tekutin	tekutina	k1gFnPc2	tekutina
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
nenachází	nacházet	k5eNaImIp3nS	nacházet
v	v	k7c6	v
kritické	kritický	k2eAgFnSc6d1	kritická
fázi	fáze	k1gFnSc6	fáze
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
mu	on	k3xPp3gMnSc3	on
podat	podat	k5eAaPmF	podat
kličková	kličkový	k2eAgNnPc1d1	Kličkové
diuretika	diuretikum	k1gNnPc1	diuretikum
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
furosemid	furosemid	k1gInSc1	furosemid
(	(	kIx(	(
<g/>
Lasix	Lasix	k1gInSc1	Lasix
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pomohou	pomoct	k5eAaPmIp3nP	pomoct
vyloučit	vyloučit	k5eAaPmF	vyloučit
z	z	k7c2	z
krevního	krevní	k2eAgInSc2d1	krevní
oběhu	oběh	k1gInSc2	oběh
nadbytečné	nadbytečný	k2eAgFnSc2d1	nadbytečná
tekutiny	tekutina	k1gFnSc2	tekutina
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
lidí	člověk	k1gMnPc2	člověk
se	se	k3xPyFc4	se
z	z	k7c2	z
horečky	horečka	k1gFnSc2	horečka
dengue	dengue	k6eAd1	dengue
zotaví	zotavit	k5eAaPmIp3nP	zotavit
bez	bez	k7c2	bez
jakýchkoli	jakýkoli	k3yIgFnPc2	jakýkoli
následných	následný	k2eAgFnPc2d1	následná
potíží	potíž	k1gFnPc2	potíž
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
léčby	léčba	k1gFnSc2	léčba
na	na	k7c4	na
horečku	horečka	k1gFnSc4	horečka
dengue	dengue	k6eAd1	dengue
umírá	umírat	k5eAaImIp3nS	umírat
asi	asi	k9	asi
1	[number]	k4	1
až	až	k9	až
5	[number]	k4	5
%	%	kIx~	%
nakažených	nakažený	k2eAgFnPc2d1	nakažená
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
správné	správný	k2eAgFnSc2d1	správná
léčby	léčba	k1gFnSc2	léčba
umírá	umírat	k5eAaImIp3nS	umírat
méně	málo	k6eAd2	málo
než	než	k8xS	než
1	[number]	k4	1
%	%	kIx~	%
nakažených	nakažený	k2eAgFnPc2d1	nakažená
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
lidí	člověk	k1gMnPc2	člověk
infikovaných	infikovaný	k2eAgFnPc2d1	infikovaná
závažnou	závažný	k2eAgFnSc7d1	závažná
formou	forma	k1gFnSc7	forma
horečky	horečka	k1gFnSc2	horečka
dengue	dengu	k1gFnSc2	dengu
však	však	k9	však
umírá	umírat	k5eAaImIp3nS	umírat
celých	celý	k2eAgInPc2d1	celý
26	[number]	k4	26
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Horečka	horečka	k1gFnSc1	horečka
dengue	dengue	k6eAd1	dengue
je	být	k5eAaImIp3nS	být
běžným	běžný	k2eAgNnSc7d1	běžné
onemocněním	onemocnění	k1gNnSc7	onemocnění
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
110	[number]	k4	110
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
nakazí	nakazit	k5eAaPmIp3nS	nakazit
50	[number]	k4	50
až	až	k9	až
100	[number]	k4	100
miliónů	milión	k4xCgInPc2	milión
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
hospitalizováno	hospitalizován	k2eAgNnSc1d1	hospitalizováno
je	být	k5eAaImIp3nS	být
kvůli	kvůli	k7c3	kvůli
ní	on	k3xPp3gFnSc3	on
přibližně	přibližně	k6eAd1	přibližně
půl	půl	k1xP	půl
miliónu	milión	k4xCgInSc2	milión
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
12	[number]	k4	12
500	[number]	k4	500
až	až	k9	až
25	[number]	k4	25
000	[number]	k4	000
nakažených	nakažený	k2eAgMnPc2d1	nakažený
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
nemoc	nemoc	k1gFnSc4	nemoc
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
.	.	kIx.	.
</s>
<s>
Horečka	horečka	k1gFnSc1	horečka
dengue	dengue	k6eAd1	dengue
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejběžnější	běžný	k2eAgNnPc4d3	nejběžnější
virová	virový	k2eAgNnPc4d1	virové
onemocnění	onemocnění	k1gNnPc4	onemocnění
šířená	šířený	k2eAgNnPc4d1	šířené
členovci	členovec	k1gMnPc7	členovec
<g/>
.	.	kIx.	.
</s>
<s>
Přisuzuje	přisuzovat	k5eAaImIp3nS	přisuzovat
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
zátěž	zátěž	k1gFnSc4	zátěž
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
600	[number]	k4	600
let	léto	k1gNnPc2	léto
života	život	k1gInSc2	život
ztracených	ztracený	k2eAgFnPc2d1	ztracená
nemocí	nemoc	k1gFnPc2	nemoc
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
milión	milión	k4xCgInSc4	milión
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
stejný	stejný	k2eAgInSc4d1	stejný
dopad	dopad	k1gInSc4	dopad
<g/>
,	,	kIx,	,
jaký	jaký	k3yIgInSc4	jaký
mají	mít	k5eAaImIp3nP	mít
ostatní	ostatní	k2eAgFnSc2d1	ostatní
dětské	dětský	k2eAgFnSc2d1	dětská
a	a	k8xC	a
tropické	tropický	k2eAgFnSc2d1	tropická
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
Horečka	horečka	k1gFnSc1	horečka
dengue	dengue	k6eAd1	dengue
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
malárii	malárie	k1gFnSc6	malárie
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
druhou	druhý	k4xOgFnSc4	druhý
nejvýznamnější	významný	k2eAgFnSc4d3	nejvýznamnější
tropickou	tropický	k2eAgFnSc4d1	tropická
nemoc	nemoc	k1gFnSc4	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Světová	světový	k2eAgFnSc1d1	světová
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
organizace	organizace	k1gFnSc1	organizace
horečku	horečka	k1gFnSc4	horečka
dengue	dengu	k1gInSc2	dengu
rovněž	rovněž	k9	rovněž
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
16	[number]	k4	16
opomíjených	opomíjený	k2eAgFnPc2d1	opomíjená
tropických	tropický	k2eAgFnPc2d1	tropická
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Horečka	horečka	k1gFnSc1	horečka
dengue	dengue	k6eAd1	dengue
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
stále	stále	k6eAd1	stále
častější	častý	k2eAgInSc1d2	častější
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
byla	být	k5eAaImAgFnS	být
třicetkrát	třicetkrát	k6eAd1	třicetkrát
běžnější	běžný	k2eAgFnSc1d2	běžnější
než	než	k8xS	než
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
Nárůst	nárůst	k1gInSc1	nárůst
je	být	k5eAaImIp3nS	být
přičítán	přičítán	k2eAgInSc1d1	přičítán
několika	několik	k4yIc2	několik
faktorům	faktor	k1gMnPc3	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
lidí	člověk	k1gMnPc2	člověk
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
světová	světový	k2eAgFnSc1d1	světová
populace	populace	k1gFnSc1	populace
neustále	neustále	k6eAd1	neustále
roste	růst	k5eAaImIp3nS	růst
a	a	k8xC	a
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
lidí	člověk	k1gMnPc2	člověk
cestuje	cestovat	k5eAaImIp3nS	cestovat
do	do	k7c2	do
cizích	cizí	k2eAgFnPc2d1	cizí
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
se	se	k3xPyFc4	se
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
větším	veliký	k2eAgNnSc6d2	veliký
rozšíření	rozšíření	k1gNnSc6	rozšíření
nemoci	nemoc	k1gFnSc2	nemoc
hraje	hrát	k5eAaImIp3nS	hrát
svou	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
i	i	k8xC	i
globální	globální	k2eAgNnSc4d1	globální
oteplování	oteplování	k1gNnSc4	oteplování
<g/>
.	.	kIx.	.
</s>
<s>
Horečka	horečka	k1gFnSc1	horečka
dengue	dengue	k1gFnPc2	dengue
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
kolem	kolem	k7c2	kolem
rovníku	rovník	k1gInSc2	rovník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
je	být	k5eAaImIp3nS	být
běžná	běžný	k2eAgFnSc1d1	běžná
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
přibližně	přibližně	k6eAd1	přibližně
2,5	[number]	k4	2,5
miliardy	miliarda	k4xCgFnSc2	miliarda
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
70	[number]	k4	70
%	%	kIx~	%
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
Tichomoří	Tichomoří	k1gNnSc6	Tichomoří
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
se	s	k7c7	s
2,9	[number]	k4	2,9
až	až	k9	až
8	[number]	k4	8
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
z	z	k7c2	z
cest	cesta	k1gFnPc2	cesta
po	po	k7c6	po
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
výskytem	výskyt	k1gInSc7	výskyt
horečky	horečka	k1gFnSc2	horečka
dengue	dengue	k6eAd1	dengue
a	a	k8xC	a
kteří	který	k3yRgMnPc1	který
jí	on	k3xPp3gFnSc3	on
onemocněli	onemocnět	k5eAaPmAgMnP	onemocnět
<g/>
,	,	kIx,	,
nakazilo	nakazit	k5eAaPmAgNnS	nakazit
během	během	k7c2	během
cestování	cestování	k1gNnSc2	cestování
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
osob	osoba	k1gFnPc2	osoba
je	být	k5eAaImIp3nS	být
horečka	horečka	k1gFnSc1	horečka
dengue	dengu	k1gFnSc2	dengu
po	po	k7c4	po
malárii	malárie	k1gFnSc4	malárie
druhou	druhý	k4xOgFnSc4	druhý
nejběžněji	běžně	k6eAd3	běžně
diagnostikovanou	diagnostikovaný	k2eAgFnSc7d1	diagnostikovaná
infekcí	infekce	k1gFnSc7	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Horečka	horečka	k1gFnSc1	horečka
dengue	dengue	k6eAd1	dengue
byla	být	k5eAaImAgFnS	být
popsána	popsat	k5eAaPmNgFnS	popsat
již	již	k6eAd1	již
velmi	velmi	k6eAd1	velmi
dávno	dávno	k6eAd1	dávno
<g/>
.	.	kIx.	.
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1	čínská
lékařská	lékařský	k2eAgFnSc1d1	lékařská
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
dynastie	dynastie	k1gFnSc2	dynastie
Jin	Jin	k1gFnSc2	Jin
(	(	kIx(	(
<g/>
z	z	k7c2	z
období	období	k1gNnSc2	období
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
265	[number]	k4	265
<g/>
–	–	k?	–
<g/>
420	[number]	k4	420
<g/>
)	)	kIx)	)
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
horečkou	horečka	k1gFnSc7	horečka
dengue	dengu	k1gFnSc2	dengu
onemocněl	onemocnět	k5eAaPmAgMnS	onemocnět
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c4	o
"	"	kIx"	"
<g/>
otravě	otrava	k1gFnSc3	otrava
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
související	související	k2eAgInPc1d1	související
s	s	k7c7	s
létajícím	létající	k2eAgInSc7d1	létající
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
rovněž	rovněž	k9	rovněž
písemné	písemný	k2eAgInPc4d1	písemný
záznamy	záznam	k1gInPc4	záznam
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
o	o	k7c6	o
událostech	událost	k1gFnPc6	událost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
epidemií	epidemie	k1gFnSc7	epidemie
horečky	horečka	k1gFnSc2	horečka
dengue	dengu	k1gFnSc2	dengu
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejstarší	starý	k2eAgFnPc1d3	nejstarší
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
epidemii	epidemie	k1gFnSc6	epidemie
této	tento	k3xDgFnSc2	tento
horečky	horečka	k1gFnSc2	horečka
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1779	[number]	k4	1779
a	a	k8xC	a
1780	[number]	k4	1780
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c4	o
epidemii	epidemie	k1gFnSc4	epidemie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zachvátila	zachvátit	k5eAaPmAgFnS	zachvátit
Asii	Asie	k1gFnSc4	Asie
<g/>
,	,	kIx,	,
Afriku	Afrika	k1gFnSc4	Afrika
a	a	k8xC	a
Severní	severní	k2eAgFnSc4d1	severní
Ameriku	Amerika	k1gFnSc4	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
k	k	k7c3	k
mnoha	mnoho	k4c3	mnoho
dalším	další	k2eAgFnPc3d1	další
epidemiím	epidemie	k1gFnPc3	epidemie
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
vědci	vědec	k1gMnPc1	vědec
dokázali	dokázat	k5eAaPmAgMnP	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemoc	nemoc	k1gFnSc1	nemoc
na	na	k7c4	na
lidi	člověk	k1gMnPc4	člověk
přenášejí	přenášet	k5eAaImIp3nP	přenášet
komáři	komár	k1gMnPc1	komár
rodu	rod	k1gInSc2	rod
"	"	kIx"	"
<g/>
Aedes	Aedes	k1gInSc1	Aedes
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
byl	být	k5eAaImAgMnS	být
za	za	k7c2	za
původce	původce	k1gMnSc2	původce
horečky	horečka	k1gFnSc2	horečka
dengue	dengu	k1gInSc2	dengu
označen	označit	k5eAaPmNgInS	označit
virus	virus	k1gInSc1	virus
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
teprve	teprve	k6eAd1	teprve
druhá	druhý	k4xOgFnSc1	druhý
nemoc	nemoc	k1gFnSc1	nemoc
<g/>
,	,	kIx,	,
u	u	k7c2	u
které	který	k3yRgFnSc2	který
se	se	k3xPyFc4	se
prokázalo	prokázat	k5eAaPmAgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
virem	vir	k1gInSc7	vir
(	(	kIx(	(
<g/>
virový	virový	k2eAgInSc4d1	virový
původ	původ	k1gInSc4	původ
nemoci	nemoc	k1gFnSc2	nemoc
již	již	k9	již
vědci	vědec	k1gMnPc1	vědec
prokázali	prokázat	k5eAaPmAgMnP	prokázat
u	u	k7c2	u
žluté	žlutý	k2eAgFnSc2d1	žlutá
zimnice	zimnice	k1gFnSc2	zimnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Burton	Burton	k1gInSc4	Burton
Cleland	Cleland	k1gInSc1	Cleland
a	a	k8xC	a
Joseph	Joseph	k1gInSc1	Joseph
Franklin	Franklina	k1gFnPc2	Franklina
Siler	Silero	k1gNnPc2	Silero
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
ve	v	k7c6	v
výzkumu	výzkum	k1gInSc6	výzkum
viru	vir	k1gInSc2	vir
způsobujícího	způsobující	k2eAgNnSc2d1	způsobující
horečku	horečka	k1gFnSc4	horečka
dengue	dengue	k1gFnPc2	dengue
a	a	k8xC	a
určili	určit	k5eAaPmAgMnP	určit
základní	základní	k2eAgFnPc4d1	základní
charakteristiky	charakteristika	k1gFnPc4	charakteristika
jeho	jeho	k3xOp3gNnSc4	jeho
šíření	šíření	k1gNnSc4	šíření
<g/>
.	.	kIx.	.
</s>
<s>
Horečka	horečka	k1gFnSc1	horečka
dengue	dengue	k1gFnSc1	dengue
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
mnohem	mnohem	k6eAd1	mnohem
rychleji	rychle	k6eAd2	rychle
šířit	šířit	k5eAaImF	šířit
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
je	být	k5eAaImIp3nS	být
přičítána	přičítán	k2eAgFnSc1d1	přičítána
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
směrech	směr	k1gInPc6	směr
změnila	změnit	k5eAaPmAgFnS	změnit
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
nových	nový	k2eAgFnPc2d1	nová
oblastí	oblast	k1gFnPc2	oblast
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
různé	různý	k2eAgInPc4d1	různý
typy	typ	k1gInPc4	typ
horečky	horečka	k1gFnSc2	horečka
dengue	dengue	k1gFnSc3	dengue
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
poprvé	poprvé	k6eAd1	poprvé
onemocněli	onemocnět	k5eAaPmAgMnP	onemocnět
hemoragickou	hemoragický	k2eAgFnSc7d1	hemoragická
horečkou	horečka	k1gFnSc7	horečka
dengue	dengu	k1gFnSc2	dengu
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vážná	vážný	k2eAgFnSc1d1	vážná
forma	forma	k1gFnSc1	forma
onemocnění	onemocnění	k1gNnSc2	onemocnění
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
zaznamenána	zaznamenat	k5eAaPmNgFnS	zaznamenat
na	na	k7c6	na
Filipínách	Filipíny	k1gFnPc6	Filipíny
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
a	a	k8xC	a
do	do	k7c2	do
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
příčinou	příčina	k1gFnSc7	příčina
dětských	dětský	k2eAgNnPc2d1	dětské
úmrtí	úmrtí	k1gNnPc2	úmrtí
<g/>
.	.	kIx.	.
</s>
<s>
Začala	začít	k5eAaPmAgFnS	začít
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
šířit	šířit	k5eAaImF	šířit
v	v	k7c6	v
Tichomoří	Tichomoří	k1gNnSc6	Tichomoří
a	a	k8xC	a
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
Amerikách	Amerika	k1gFnPc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
hemoragické	hemoragický	k2eAgFnSc2d1	hemoragická
horečky	horečka	k1gFnSc2	horečka
dengue	dengu	k1gFnSc2	dengu
a	a	k8xC	a
šokového	šokový	k2eAgInSc2d1	šokový
syndromu	syndrom	k1gInSc2	syndrom
dengue	dengue	k6eAd1	dengue
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
poprvé	poprvé	k6eAd1	poprvé
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
si	se	k3xPyFc3	se
lékaři	lékař	k1gMnPc1	lékař
všimli	všimnout	k5eAaPmAgMnP	všimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
již	již	k6eAd1	již
prodělali	prodělat	k5eAaPmAgMnP	prodělat
nákazu	nákaza	k1gFnSc4	nákaza
virem	vir	k1gInSc7	vir
dengue	dengu	k1gInSc2	dengu
typu	typ	k1gInSc2	typ
1	[number]	k4	1
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
o	o	k7c4	o
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
objevila	objevit	k5eAaPmAgFnS	objevit
i	i	k9	i
nákaza	nákaza	k1gFnSc1	nákaza
virem	vir	k1gInSc7	vir
dengue	dengu	k1gFnSc2	dengu
typu	typ	k1gInSc2	typ
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
dengue	dengue	k1gInSc1	dengue
<g/>
"	"	kIx"	"
pochází	pocházet	k5eAaImIp3nS	pocházet
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
názorů	názor	k1gInPc2	názor
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
svahilského	svahilský	k2eAgNnSc2d1	svahilské
sousloví	sousloví	k1gNnSc2	sousloví
"	"	kIx"	"
<g/>
Ka-dinga	Kainga	k1gFnSc1	Ka-dinga
pepo	pepo	k1gMnSc1	pepo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
hovoří	hovořit	k5eAaImIp3nP	hovořit
o	o	k7c4	o
nemoci	nemoc	k1gFnPc4	nemoc
způsobené	způsobený	k2eAgFnSc2d1	způsobená
zlým	zlý	k1gMnSc7	zlý
duchem	duch	k1gMnSc7	duch
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
se	se	k3xPyFc4	se
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
svahilské	svahilský	k2eAgNnSc1d1	svahilské
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
dinga	dingo	k1gMnSc2	dingo
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
ze	z	k7c2	z
španělského	španělský	k2eAgInSc2d1	španělský
"	"	kIx"	"
<g/>
dengue	dengu	k1gInSc2	dengu
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
slovo	slovo	k1gNnSc1	slovo
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
opatrný	opatrný	k2eAgMnSc1d1	opatrný
<g/>
"	"	kIx"	"
−	−	k?	−
mohlo	moct	k5eAaImAgNnS	moct
tedy	tedy	k9	tedy
být	být	k5eAaImF	být
používáno	používán	k2eAgNnSc4d1	používáno
k	k	k7c3	k
popisu	popis	k1gInSc3	popis
osoby	osoba	k1gFnSc2	osoba
trpící	trpící	k2eAgFnSc2d1	trpící
bolestmi	bolest	k1gFnPc7	bolest
kostí	kost	k1gFnSc7	kost
a	a	k8xC	a
horečkou	horečka	k1gFnSc7	horečka
dengue	dengu	k1gFnSc2	dengu
<g/>
;	;	kIx,	;
bolesti	bolest	k1gFnPc1	bolest
nutily	nutit	k5eAaImAgFnP	nutit
člověka	člověk	k1gMnSc2	člověk
chodit	chodit	k5eAaImF	chodit
opatrně	opatrně	k6eAd1	opatrně
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
rovněž	rovněž	k9	rovněž
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
španělské	španělský	k2eAgNnSc1d1	španělské
slovo	slovo	k1gNnSc1	slovo
bylo	být	k5eAaImAgNnS	být
převzato	převzít	k5eAaPmNgNnS	převzít
ze	z	k7c2	z
svahilštiny	svahilština	k1gFnSc2	svahilština
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
názorů	názor	k1gInPc2	názor
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
dengue	dengue	k1gInSc1	dengue
<g/>
"	"	kIx"	"
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
Západní	západní	k2eAgFnSc2d1	západní
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
o	o	k7c6	o
otrocích	otrok	k1gMnPc6	otrok
s	s	k7c7	s
horečkou	horečka	k1gFnSc7	horečka
dengue	dengue	k6eAd1	dengue
říkalo	říkat	k5eAaImAgNnS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
stojí	stát	k5eAaImIp3nS	stát
a	a	k8xC	a
chodí	chodit	k5eAaImIp3nS	chodit
jako	jako	k9	jako
"	"	kIx"	"
<g/>
panáci	panák	k1gMnPc1	panák
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
nemoci	nemoc	k1gFnSc3	nemoc
rovněž	rovněž	k9	rovněž
říkalo	říkat	k5eAaImAgNnS	říkat
"	"	kIx"	"
<g/>
panáková	panákový	k2eAgFnSc1d1	panáková
horečka	horečka	k1gFnSc1	horečka
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
dandy	dandy	k1gMnSc1	dandy
fever	fever	k1gMnSc1	fever
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
horečka	horečka	k1gFnSc1	horečka
lámající	lámající	k2eAgFnSc2d1	lámající
kosti	kost	k1gFnSc2	kost
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgMnS	být
poprvé	poprvé	k6eAd1	poprvé
použit	použít	k5eAaPmNgMnS	použít
Benjaminem	Benjamin	k1gMnSc7	Benjamin
Rushem	Rush	k1gInSc7	Rush
<g/>
,	,	kIx,	,
lékařem	lékař	k1gMnSc7	lékař
a	a	k8xC	a
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
Otců	otec	k1gMnPc2	otec
zakladatelů	zakladatel	k1gMnPc2	zakladatel
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1789	[number]	k4	1789
Rush	Rusha	k1gFnPc2	Rusha
tento	tento	k3xDgInSc4	tento
název	název	k1gInSc4	název
použil	použít	k5eAaPmAgMnS	použít
ve	v	k7c6	v
zprávě	zpráva	k1gFnSc6	zpráva
o	o	k7c4	o
epidemii	epidemie	k1gFnSc4	epidemie
horečky	horečka	k1gFnSc2	horečka
dengue	dengu	k1gFnSc2	dengu
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
propukla	propuknout	k5eAaPmAgFnS	propuknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1780	[number]	k4	1780
ve	v	k7c6	v
Philadelphii	Philadelphia	k1gFnSc6	Philadelphia
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zprávě	zpráva	k1gFnSc6	zpráva
Rush	Rush	k1gMnSc1	Rush
povětšinou	povětšinou	k6eAd1	povětšinou
používal	používat	k5eAaImAgMnS	používat
formálnější	formální	k2eAgNnSc4d2	formálnější
pojmenování	pojmenování	k1gNnSc4	pojmenování
"	"	kIx"	"
<g/>
biliózní	biliózní	k2eAgFnSc1d1	biliózní
střídavá	střídavý	k2eAgFnSc1d1	střídavá
horečka	horečka	k1gFnSc1	horečka
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
bilious	bilious	k1gInSc1	bilious
remitting	remitting	k1gInSc1	remitting
fever	fever	k1gInSc1	fever
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
horečka	horečka	k1gFnSc1	horečka
dengue	dengu	k1gMnSc2	dengu
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
běžně	běžně	k6eAd1	běžně
objevovat	objevovat	k5eAaImF	objevovat
až	až	k9	až
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1828	[number]	k4	1828
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
byly	být	k5eAaImAgFnP	být
k	k	k7c3	k
pojmenování	pojmenování	k1gNnSc3	pojmenování
nemoci	nemoc	k1gFnSc3	nemoc
používány	používat	k5eAaImNgInP	používat
různé	různý	k2eAgInPc1d1	různý
názvy	název	k1gInPc1	název
–	–	k?	–
říkalo	říkat	k5eAaImAgNnS	říkat
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
např.	např.	kA	např.
"	"	kIx"	"
<g/>
srdcelomná	srdcelomný	k2eAgFnSc1d1	srdcelomný
horečka	horečka	k1gFnSc1	horečka
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
breakheart	breakheart	k1gInSc1	breakheart
fever	fever	k1gInSc1	fever
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
la	la	k1gNnSc3	la
dengue	dengue	k1gNnSc3	dengue
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
závažnou	závažný	k2eAgFnSc4d1	závažná
formu	forma	k1gFnSc4	forma
horečky	horečka	k1gFnSc2	horečka
dengue	dengue	k6eAd1	dengue
byly	být	k5eAaImAgInP	být
používány	používat	k5eAaImNgInP	používat
další	další	k2eAgInPc1d1	další
výrazy	výraz	k1gInPc1	výraz
<g/>
,	,	kIx,	,
např.	např.	kA	např.
infekční	infekční	k2eAgFnSc1d1	infekční
trombocytopenická	trombocytopenický	k2eAgFnSc1d1	trombocytopenická
purpura	purpura	k1gFnSc1	purpura
<g/>
,	,	kIx,	,
filipínská	filipínský	k2eAgFnSc1d1	filipínská
<g/>
,	,	kIx,	,
thajská	thajský	k2eAgFnSc1d1	thajská
a	a	k8xC	a
singapurská	singapurský	k2eAgFnSc1d1	Singapurská
hemoragická	hemoragický	k2eAgFnSc1d1	hemoragická
horečka	horečka	k1gFnSc1	horečka
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
neustále	neustále	k6eAd1	neustále
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
ve	v	k7c6	v
výzkumu	výzkum	k1gInSc6	výzkum
způsobů	způsob	k1gInPc2	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
horečce	horečka	k1gFnSc3	horečka
dengue	dengue	k6eAd1	dengue
předcházet	předcházet	k5eAaImF	předcházet
a	a	k8xC	a
jak	jak	k6eAd1	jak
ji	on	k3xPp3gFnSc4	on
léčit	léčit	k5eAaImF	léčit
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
se	se	k3xPyFc4	se
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c4	na
regulaci	regulace	k1gFnSc4	regulace
počtu	počet	k1gInSc2	počet
komárů	komár	k1gMnPc2	komár
<g/>
,	,	kIx,	,
vývoji	vývoj	k1gInSc3	vývoj
vakcíny	vakcína	k1gFnSc2	vakcína
a	a	k8xC	a
léků	lék	k1gInPc2	lék
pro	pro	k7c4	pro
boj	boj	k1gInSc4	boj
s	s	k7c7	s
virem	vir	k1gInSc7	vir
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
regulaci	regulace	k1gFnSc4	regulace
množství	množství	k1gNnSc2	množství
komárů	komár	k1gMnPc2	komár
bylo	být	k5eAaImAgNnS	být
provedeno	provést	k5eAaPmNgNnS	provést
mnoho	mnoho	k4c1	mnoho
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
kroků	krok	k1gInPc2	krok
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
jsou	být	k5eAaImIp3nP	být
účinné	účinný	k2eAgInPc1d1	účinný
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
stojatých	stojatý	k2eAgFnPc2d1	stojatá
vod	voda	k1gFnPc2	voda
lze	lze	k6eAd1	lze
například	například	k6eAd1	například
nasadit	nasadit	k5eAaPmF	nasadit
živorodky	živorodka	k1gFnPc4	živorodka
duhové	duhový	k2eAgFnPc4d1	Duhová
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Poecilia	Poecilia	k1gFnSc1	Poecilia
reticulate	reticulat	k1gInSc5	reticulat
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
klanonožce	klanonožka	k1gFnSc3	klanonožka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
požírali	požírat	k5eAaImAgMnP	požírat
komáří	komáří	k2eAgFnPc4d1	komáří
larvy	larva	k1gFnPc4	larva
(	(	kIx(	(
<g/>
vajíčka	vajíčko	k1gNnPc4	vajíčko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
také	také	k9	také
pracují	pracovat	k5eAaImIp3nP	pracovat
na	na	k7c6	na
vývoji	vývoj	k1gInSc6	vývoj
vakcíny	vakcína	k1gFnSc2	vakcína
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
člověka	člověk	k1gMnSc4	člověk
chránila	chránit	k5eAaImAgFnS	chránit
před	před	k7c7	před
všemi	všecek	k3xTgFnPc7	všecek
čtyřmi	čtyři	k4xCgFnPc7	čtyři
typy	typ	k1gInPc4	typ
horečky	horečka	k1gFnSc2	horečka
dengue	dengue	k1gFnSc3	dengue
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
obávají	obávat	k5eAaImIp3nP	obávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
vakcína	vakcína	k1gFnSc1	vakcína
mohla	moct	k5eAaImAgFnS	moct
zvýšit	zvýšit	k5eAaPmF	zvýšit
riziko	riziko	k1gNnSc4	riziko
závažného	závažný	k2eAgNnSc2d1	závažné
onemocnění	onemocnění	k1gNnSc2	onemocnění
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
zvýšené	zvýšený	k2eAgFnSc2d1	zvýšená
infektivity	infektivita	k1gFnSc2	infektivita
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
protilátek	protilátka	k1gFnPc2	protilátka
(	(	kIx(	(
<g/>
ADE	ADE	kA	ADE
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
možná	možný	k2eAgFnSc1d1	možná
vakcína	vakcína	k1gFnSc1	vakcína
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
splňovat	splňovat	k5eAaImF	splňovat
několik	několik	k4yIc4	několik
požadavků	požadavek	k1gInPc2	požadavek
<g/>
.	.	kIx.	.
</s>
<s>
Zaprvé	zaprvé	k4xO	zaprvé
by	by	kYmCp3nP	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
bezpečná	bezpečný	k2eAgFnSc1d1	bezpečná
<g/>
,	,	kIx,	,
zadruhé	zadruhé	k4xO	zadruhé
by	by	kYmCp3nP	by
měla	mít	k5eAaImAgFnS	mít
fungovat	fungovat	k5eAaImF	fungovat
po	po	k7c6	po
jedné	jeden	k4xCgFnSc6	jeden
či	či	k8xC	či
dvou	dva	k4xCgMnPc6	dva
injekcích	injekce	k1gFnPc6	injekce
a	a	k8xC	a
zatřetí	zatřetí	k4xO	zatřetí
by	by	kYmCp3nP	by
měla	mít	k5eAaImAgFnS	mít
chránit	chránit	k5eAaImF	chránit
před	před	k7c7	před
všemi	všecek	k3xTgInPc7	všecek
typy	typ	k1gInPc7	typ
viru	vir	k1gInSc2	vir
dengue	dengu	k1gInSc2	dengu
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
by	by	kYmCp3nS	by
neměla	mít	k5eNaImAgFnS	mít
vést	vést	k5eAaImF	vést
k	k	k7c3	k
ADE	ADE	kA	ADE
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
by	by	kYmCp3nS	by
být	být	k5eAaImF	být
snadné	snadný	k2eAgNnSc1d1	snadné
ji	on	k3xPp3gFnSc4	on
přenášet	přenášet	k5eAaImF	přenášet
a	a	k8xC	a
uchovávat	uchovávat	k5eAaImF	uchovávat
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
výroba	výroba	k1gFnSc1	výroba
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
vakcín	vakcína	k1gFnPc2	vakcína
bylo	být	k5eAaImAgNnS	být
testováno	testovat	k5eAaImNgNnS	testovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
doufají	doufat	k5eAaImIp3nP	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
první	první	k4xOgMnSc1	první
komerčně	komerčně	k6eAd1	komerčně
dostupné	dostupný	k2eAgFnPc1d1	dostupná
vakcíny	vakcína	k1gFnPc1	vakcína
budou	být	k5eAaImBp3nP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
rovněž	rovněž	k9	rovněž
pracují	pracovat	k5eAaImIp3nP	pracovat
na	na	k7c4	na
vytvoření	vytvoření	k1gNnSc4	vytvoření
antivirových	antivirový	k2eAgInPc2d1	antivirový
léků	lék	k1gInPc2	lék
pro	pro	k7c4	pro
léčbu	léčba	k1gFnSc4	léčba
horečky	horečka	k1gFnSc2	horečka
dengue	dengue	k1gFnPc2	dengue
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
zamezily	zamezit	k5eAaPmAgFnP	zamezit
vzniku	vznik	k1gInSc3	vznik
závažných	závažný	k2eAgFnPc2d1	závažná
zdravotních	zdravotní	k2eAgFnPc2d1	zdravotní
komplikací	komplikace	k1gFnPc2	komplikace
<g/>
,	,	kIx,	,
i	i	k9	i
na	na	k7c4	na
rozluštění	rozluštění	k1gNnSc4	rozluštění
struktury	struktura	k1gFnSc2	struktura
proteinů	protein	k1gInPc2	protein
viru	vir	k1gInSc2	vir
dengue	denguat	k5eAaPmIp3nS	denguat
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
by	by	kYmCp3nS	by
jim	on	k3xPp3gMnPc3	on
mohlo	moct	k5eAaImAgNnS	moct
pomoci	pomoct	k5eAaPmF	pomoct
vyvinout	vyvinout	k5eAaPmF	vyvinout
účinné	účinný	k2eAgInPc4d1	účinný
léky	lék	k1gInPc4	lék
proti	proti	k7c3	proti
horečce	horečka	k1gFnSc3	horečka
dengue	dengu	k1gInSc2	dengu
<g/>
.	.	kIx.	.
</s>
<s>
Gubler	Gubler	k1gMnSc1	Gubler
DJ	DJ	kA	DJ
<g/>
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Dengue	Dengue	k1gFnSc1	Dengue
viruses	viruses	k1gMnSc1	viruses
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
<g/>
in	in	k?	in
Mahy	Mahy	k?	Mahy
BWJ	BWJ	kA	BWJ
<g/>
,	,	kIx,	,
Van	van	k1gInSc1	van
Regenmortel	Regenmortel	k1gInSc1	Regenmortel
MHV	MHV	kA	MHV
<g/>
:	:	kIx,	:
Desk	deska	k1gFnPc2	deska
Encyclopedia	Encyclopedium	k1gNnSc2	Encyclopedium
of	of	k?	of
Human	Humany	k1gInPc2	Humany
and	and	k?	and
Medical	Medical	k1gFnPc2	Medical
Virology	virolog	k1gMnPc4	virolog
<g/>
.	.	kIx.	.
</s>
<s>
Boston	Boston	k1gInSc1	Boston
<g/>
:	:	kIx,	:
Academic	Academic	k1gMnSc1	Academic
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
372	[number]	k4	372
<g/>
–	–	k?	–
<g/>
82	[number]	k4	82
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
375147	[number]	k4	375147
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
WHO	WHO	kA	WHO
<g/>
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dengue	Denguat	k5eAaPmIp3nS	Denguat
Guidelines	Guidelines	k1gInSc1	Guidelines
for	forum	k1gNnPc2	forum
Diagnosis	Diagnosis	k1gFnSc2	Diagnosis
<g/>
,	,	kIx,	,
Treatment	Treatment	k1gInSc1	Treatment
<g/>
,	,	kIx,	,
Prevention	Prevention	k1gInSc1	Prevention
and	and	k?	and
Control	Control	k1gInSc1	Control
<g/>
.	.	kIx.	.
</s>
<s>
Geneva	Geneva	k1gFnSc1	Geneva
<g/>
:	:	kIx,	:
World	World	k1gInSc1	World
Health	Health	k1gMnSc1	Health
Organization	Organization	k1gInSc1	Organization
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
9241547871	[number]	k4	9241547871
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
dengue	dengu	k1gFnSc2	dengu
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Horečka	horečka	k1gFnSc1	horečka
dengue	dengue	k1gFnSc1	dengue
–	–	k?	–
nejčastější	častý	k2eAgFnSc1d3	nejčastější
exotická	exotický	k2eAgFnSc1d1	exotická
viróza	viróza	k1gFnSc1	viróza
u	u	k7c2	u
českých	český	k2eAgMnPc2d1	český
cestovatelů	cestovatel	k1gMnPc2	cestovatel
<g/>
,	,	kIx,	,
Hedvabnastezka	Hedvabnastezka	k1gFnSc1	Hedvabnastezka
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
