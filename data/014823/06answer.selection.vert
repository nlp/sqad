<s desamb="1">
Etičtí	etický	k2eAgMnPc1d1
vegani	vegan	k1gMnPc1
nekonzumují	konzumovat	k5eNaBmIp3nP
mléčné	mléčný	k2eAgInPc4d1
výrobky	výrobek	k1gInPc4
a	a	k8xC
vejce	vejce	k1gNnPc4
<g/>
,	,	kIx,
protože	protože	k8xS
uvádějí	uvádět	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
jejich	jejich	k3xOp3gFnSc1
výroba	výroba	k1gFnSc1
způsobuje	způsobovat	k5eAaImIp3nS
zvířatům	zvíře	k1gNnPc3
utrpení	utrpení	k1gNnSc4
nebo	nebo	k8xC
předčasnou	předčasný	k2eAgFnSc4d1
smrt	smrt	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
114	#num#	k4
<g/>
]	]	kIx)
</s>