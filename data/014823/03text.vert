<s>
Vegetariánství	vegetariánství	k1gNnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
pojednává	pojednávat	k5eAaImIp3nS
o	o	k7c6
způsobu	způsob	k1gInSc6
stravování	stravování	k1gNnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
nejí	jíst	k5eNaImIp3nS
maso	maso	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Možná	možná	k6eAd1
hledáte	hledat	k5eAaImIp2nP
<g/>
:	:	kIx,
veganství	veganství	k1gNnSc4
–	–	k?
odmítnutí	odmítnutí	k1gNnSc2
všech	všecek	k3xTgInPc2
živočišných	živočišný	k2eAgInPc2d1
produktů	produkt	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Vegetariánská	vegetariánský	k2eAgFnSc1d1
strava	strava	k1gFnSc1
je	být	k5eAaImIp3nS
odvozena	odvodit	k5eAaPmNgFnS
od	od	k7c2
rostlin	rostlina	k1gFnPc2
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
s	s	k7c7
mléčnými	mléčný	k2eAgInPc7d1
výrobky	výrobek	k1gInPc7
<g/>
/	/	kIx~
<g/>
vejci	vejce	k1gNnPc7
a	a	k8xC
nebo	nebo	k8xC
bez	bez	k7c2
nich	on	k3xPp3gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
bez	bez	k7c2
masa	maso	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Různé	různý	k2eAgFnPc1d1
veganské	veganský	k2eAgFnPc1d1
a	a	k8xC
vegetariánské	vegetariánský	k2eAgFnPc1d1
lahůdky	lahůdka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Vegetariánství	vegetariánství	k1gNnSc1
je	být	k5eAaImIp3nS
způsob	způsob	k1gInSc4
lidského	lidský	k2eAgNnSc2d1
stravování	stravování	k1gNnSc2
(	(	kIx(
<g/>
diety	dieta	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
z	z	k7c2
jídelníčku	jídelníček	k1gInSc2
vyloučeno	vyloučen	k2eAgNnSc1d1
maso	maso	k1gNnSc1
(	(	kIx(
<g/>
červené	červený	k2eAgNnSc1d1
maso	maso	k1gNnSc1
<g/>
,	,	kIx,
drůbež	drůbež	k1gFnSc1
<g/>
,	,	kIx,
mořské	mořský	k2eAgInPc4d1
plody	plod	k1gInPc4
<g/>
,	,	kIx,
ryby	ryba	k1gFnPc4
a	a	k8xC
maso	maso	k1gNnSc4
z	z	k7c2
jiných	jiný	k2eAgMnPc2d1
živočichů	živočich	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Může	moct	k5eAaImIp3nS
zahrnovat	zahrnovat	k5eAaImF
i	i	k9
vyloučení	vyloučení	k1gNnSc4
dalších	další	k2eAgInPc2d1
produktů	produkt	k1gInPc2
z	z	k7c2
porážky	porážka	k1gFnSc2
zvířat	zvíře	k1gNnPc2
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
vnitřní	vnitřní	k2eAgInPc4d1
orgány	orgán	k1gInPc4
<g/>
,	,	kIx,
kůže	kůže	k1gFnSc1
a	a	k8xC
kožní	kožní	k2eAgInPc1d1
deriváty	derivát	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Dobře	dobře	k6eAd1
plánovaná	plánovaný	k2eAgFnSc1d1
vegetariánská	vegetariánský	k2eAgFnSc1d1
strava	strava	k1gFnSc1
poskytuje	poskytovat	k5eAaImIp3nS
ve	v	k7c6
všech	všecek	k3xTgFnPc6
fázích	fáze	k1gFnPc6
života	život	k1gInSc2
všechny	všechen	k3xTgFnPc1
živiny	živina	k1gFnPc1
v	v	k7c6
potravě	potrava	k1gFnSc6
na	na	k7c6
stejné	stejný	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
jako	jako	k9
mají	mít	k5eAaImIp3nP
jedlíci	jedlík	k1gMnPc1
masa	maso	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
K	k	k7c3
vegetariánství	vegetariánství	k1gNnSc3
se	se	k3xPyFc4
lidé	člověk	k1gMnPc1
hlásí	hlásit	k5eAaImIp3nP
z	z	k7c2
různých	různý	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k6eAd1
lidí	člověk	k1gMnPc2
nejí	jíst	k5eNaImIp3nS
maso	maso	k1gNnSc1
z	z	k7c2
etických	etický	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
úcty	úcta	k1gFnSc2
ke	k	k7c3
zvířatům	zvíře	k1gNnPc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
vyloučili	vyloučit	k5eAaPmAgMnP
jejich	jejich	k3xOp3gFnSc4
bolest	bolest	k1gFnSc4
a	a	k8xC
stres	stres	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc4
etické	etický	k2eAgFnPc4d1
motivace	motivace	k1gFnPc4
uzákoňují	uzákoňovat	k5eAaImIp3nP
různá	různý	k2eAgNnPc1d1
náboženská	náboženský	k2eAgNnPc1d1
vyznání	vyznání	k1gNnPc1
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
stanoveny	stanovit	k5eAaPmNgInP
různými	různý	k2eAgInPc7d1
obránci	obránce	k1gMnSc3
práv	právo	k1gNnPc2
zvířat	zvíře	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnPc4d1
motivace	motivace	k1gFnPc4
pro	pro	k7c4
vegetariánství	vegetariánství	k1gNnSc4
souvisí	souviset	k5eAaImIp3nS
se	s	k7c7
zdravím	zdraví	k1gNnSc7
a	a	k8xC
s	s	k7c7
ekologickými	ekologický	k2eAgFnPc7d1
<g/>
,	,	kIx,
kulturními	kulturní	k2eAgInPc7d1
<g/>
,	,	kIx,
estetickými	estetický	k2eAgInPc7d1
<g/>
,	,	kIx,
ekonomickými	ekonomický	k2eAgInPc7d1
<g/>
,	,	kIx,
zdravotními	zdravotní	k2eAgFnPc7d1
nebo	nebo	k8xC
osobními	osobní	k2eAgFnPc7d1
preferencemi	preference	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existují	existovat	k5eAaImIp3nP
rozdílné	rozdílný	k2eAgInPc1d1
druhy	druh	k1gInPc1
vegeteriánské	vegeteriánský	k2eAgFnSc2d1
stravy	strava	k1gFnSc2
jako	jako	k8xC,k8xS
ovo-vegetariánská	ovo-vegetariánský	k2eAgFnSc1d1
strava	strava	k1gFnSc1
(	(	kIx(
<g/>
zahrnuje	zahrnovat	k5eAaImIp3nS
vejce	vejce	k1gNnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
ne	ne	k9
mléčné	mléčný	k2eAgInPc4d1
výrobky	výrobek	k1gInPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
lakto-vegetariánská	lakto-vegetariánský	k2eAgFnSc1d1
strava	strava	k1gFnSc1
(	(	kIx(
<g/>
zahrnuje	zahrnovat	k5eAaImIp3nS
mléčné	mléčný	k2eAgInPc4d1
výrobky	výrobek	k1gInPc4
<g/>
,	,	kIx,
ale	ale	k8xC
ne	ne	k9
vejce	vejce	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
ovo-lakto	ovo-lakto	k1gNnSc4
vegetariánská	vegetariánský	k2eAgFnSc1d1
strava	strava	k1gFnSc1
(	(	kIx(
<g/>
zahrnuje	zahrnovat	k5eAaImIp3nS
jak	jak	k6eAd1
vejce	vejce	k1gNnPc4
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
mléčné	mléčný	k2eAgInPc4d1
výrobky	výrobek	k1gInPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veganská	Veganský	k2eAgFnSc1d1
strava	strava	k1gFnSc1
vylučuje	vylučovat	k5eAaImIp3nS
všechny	všechen	k3xTgInPc4
produkty	produkt	k1gInPc4
živočišného	živočišný	k2eAgInSc2d1
původu	původ	k1gInSc2
včetně	včetně	k7c2
vajec	vejce	k1gNnPc2
<g/>
,	,	kIx,
mléčných	mléčný	k2eAgInPc2d1
výrobků	výrobek	k1gInPc2
a	a	k8xC
medu	med	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
vegani	vegan	k1gMnPc1
se	se	k3xPyFc4
také	také	k9
vyhýbají	vyhýbat	k5eAaImIp3nP
dalším	další	k2eAgInPc3d1
živočišným	živočišný	k2eAgInPc3d1
produktům	produkt	k1gInPc3
jako	jako	k9
jsou	být	k5eAaImIp3nP
včelí	včelí	k2eAgInSc4d1
vosk	vosk	k1gInSc4
<g/>
,	,	kIx,
oděvy	oděv	k1gInPc4
z	z	k7c2
kůže	kůže	k1gFnSc2
nebo	nebo	k8xC
hedvábí	hedvábí	k1gNnSc4
nebo	nebo	k8xC
krém	krém	k1gInSc4
na	na	k7c4
boty	bota	k1gFnPc4
z	z	k7c2
husího	husí	k2eAgInSc2d1
tuku	tuk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
moderní	moderní	k2eAgFnSc6d1
konzumní	konzumní	k2eAgFnSc6d1
společnosti	společnost	k1gFnSc6
vegetariáni	vegetarián	k1gMnPc1
musí	muset	k5eAaImIp3nP
<g/>
,	,	kIx,
často	často	k6eAd1
složitě	složitě	k6eAd1
<g/>
,	,	kIx,
zjišťovat	zjišťovat	k5eAaImF
složení	složení	k1gNnSc4
řady	řada	k1gFnSc2
výrobků	výrobek	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
si	se	k3xPyFc3
kupují	kupovat	k5eAaImIp3nP
nebo	nebo	k8xC
konzumují	konzumovat	k5eAaBmIp3nP
<g/>
,	,	kIx,
na	na	k7c4
přítomnost	přítomnost	k1gFnSc4
živočišných	živočišný	k2eAgFnPc2d1
složek	složka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stopy	stopa	k1gFnPc1
či	či	k8xC
větší	veliký	k2eAgNnSc1d2
množství	množství	k1gNnSc1
masných	masný	k2eAgInPc2d1
produktů	produkt	k1gInPc2
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
vyskytovat	vyskytovat	k5eAaImF
v	v	k7c6
různých	různý	k2eAgFnPc6d1
<g/>
,	,	kIx,
na	na	k7c4
první	první	k4xOgInSc4
pohled	pohled	k1gInSc4
<g/>
,	,	kIx,
nečekaných	čekaný	k2eNgInPc6d1
výrobcích	výrobek	k1gInPc6
<g/>
:	:	kIx,
různé	různý	k2eAgFnPc4d1
balené	balený	k2eAgFnPc4d1
či	či	k8xC
zpracované	zpracovaný	k2eAgFnPc4d1
potraviny	potravina	k1gFnPc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
například	například	k6eAd1
koláče	koláč	k1gInPc1
<g/>
,	,	kIx,
sušenky	sušenka	k1gFnPc1
<g/>
,	,	kIx,
bonbóny	bonbón	k1gInPc1
<g/>
,	,	kIx,
čokoláda	čokoláda	k1gFnSc1
nebo	nebo	k8xC
jogurt	jogurt	k1gInSc1
<g/>
,	,	kIx,
často	často	k6eAd1
obsahují	obsahovat	k5eAaImIp3nP
nepřiznané	přiznaný	k2eNgFnPc4d1
živočišné	živočišný	k2eAgFnPc4d1
složky	složka	k1gFnPc4
(	(	kIx(
<g/>
například	například	k6eAd1
barviva	barvivo	k1gNnSc2
<g/>
,	,	kIx,
leštidla	leštidlo	k1gNnSc2
<g/>
,	,	kIx,
syřidla	syřidlo	k1gNnSc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
I	i	k9
když	když	k8xS
v	v	k7c6
Evropské	evropský	k2eAgFnSc6d1
unii	unie	k1gFnSc6
platí	platit	k5eAaImIp3nS
povinné	povinný	k2eAgNnSc4d1
uvádění	uvádění	k1gNnSc4
složení	složení	k1gNnSc2
potravin	potravina	k1gFnPc2
<g/>
,	,	kIx,
špatně	špatně	k6eAd1
se	se	k3xPyFc4
složení	složení	k1gNnSc1
odhaluje	odhalovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vegetariáni	vegetarián	k1gMnPc1
se	se	k3xPyFc4
nicméně	nicméně	k8xC
liší	lišit	k5eAaImIp3nP
ve	v	k7c6
svých	svůj	k3xOyFgInPc6
postojích	postoj	k1gInPc6
týkajících	týkající	k2eAgNnPc6d1
se	se	k3xPyFc4
těchto	tento	k3xDgFnPc2
složek	složka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
někteří	některý	k3yIgMnPc1
vegetariáni	vegetarián	k1gMnPc1
si	se	k3xPyFc3
nemusí	muset	k5eNaImIp3nP
být	být	k5eAaImF
vědomi	vědom	k2eAgMnPc1d1
role	role	k1gFnPc4
zvířecího	zvířecí	k2eAgNnSc2d1
syřidla	syřidlo	k1gNnSc2
obvyklého	obvyklý	k2eAgNnSc2d1
při	při	k7c6
výrobě	výroba	k1gFnSc6
sýrů	sýr	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
mohou	moct	k5eAaImIp3nP
tyto	tento	k3xDgInPc4
výrobky	výrobek	k1gInPc4
nevědomky	nevědomky	k6eAd1
spotřebovávat	spotřebovávat	k5eAaImF
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
ale	ale	k8xC
jiní	jiný	k2eAgMnPc1d1
vegetariáni	vegetarián	k1gMnPc1
nemusí	muset	k5eNaImIp3nP
mít	mít	k5eAaImF
s	s	k7c7
jeho	jeho	k3xOp3gFnSc7
spotřebou	spotřeba	k1gFnSc7
problém	problém	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Polovegetariánská	Polovegetariánský	k2eAgFnSc1d1
(	(	kIx(
<g/>
semivegetariánská	semivegetariánský	k2eAgFnSc1d1
<g/>
,	,	kIx,
částečně	částečně	k6eAd1
bezmasá	bezmasý	k2eAgFnSc1d1
<g/>
)	)	kIx)
strava	strava	k1gFnSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
převážně	převážně	k6eAd1
z	z	k7c2
vegetariánských	vegetariánský	k2eAgNnPc2d1
jídel	jídlo	k1gNnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
může	moct	k5eAaImIp3nS
zahrnovat	zahrnovat	k5eAaImF
i	i	k9
občasnou	občasný	k2eAgFnSc4d1
konzumaci	konzumace	k1gFnSc4
ryb	ryba	k1gFnPc2
<g/>
,	,	kIx,
drůbeže	drůbež	k1gFnSc2
nebo	nebo	k8xC
někdy	někdy	k6eAd1
i	i	k9
jiných	jiný	k2eAgInPc2d1
druhů	druh	k1gInPc2
masa	maso	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc1
strava	strava	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
ryby	ryba	k1gFnPc4
nebo	nebo	k8xC
drůbež	drůbež	k1gFnSc4
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
definují	definovat	k5eAaBmIp3nP
pojem	pojem	k1gInSc1
maso	maso	k1gNnSc4
pouze	pouze	k6eAd1
jako	jako	k8xS,k8xC
maso	maso	k1gNnSc4
savců	savec	k1gMnPc2
a	a	k8xC
mohou	moct	k5eAaImIp3nP
se	se	k3xPyFc4
identifikovat	identifikovat	k5eAaBmF
s	s	k7c7
vegetariánstvím	vegetariánství	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Pescetariánská	Pescetariánský	k2eAgFnSc1d1
strava	strava	k1gFnSc1
je	být	k5eAaImIp3nS
popisována	popisován	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
„	„	k?
<g/>
ryby	ryba	k1gFnPc4
ano	ano	k9
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
ale	ale	k8xC
ne	ne	k9
žádné	žádný	k3yNgNnSc4
jiné	jiný	k2eAgNnSc4d1
maso	maso	k1gNnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Hovorové	hovorový	k2eAgNnSc1d1
využívání	využívání	k1gNnSc1
asociací	asociace	k1gFnPc2
mezi	mezi	k7c7
těmito	tento	k3xDgFnPc7
životosprávami	životospráva	k1gFnPc7
a	a	k8xC
vegetariánstvím	vegetariánství	k1gNnSc7
vedlo	vést	k5eAaImAgNnS
různé	různý	k2eAgFnPc4d1
vegetariánské	vegetariánský	k2eAgFnPc4d1
skupiny	skupina	k1gFnPc4
jako	jako	k8xS,k8xC
například	například	k6eAd1
Vegetariánskou	vegetariánský	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
ke	k	k7c3
konstatování	konstatování	k1gNnSc3
<g/>
,	,	kIx,
že	že	k8xS
strava	strava	k1gFnSc1
obsahující	obsahující	k2eAgNnSc1d1
tyto	tento	k3xDgFnPc4
složky	složka	k1gFnPc4
není	být	k5eNaImIp3nS
vegetariánská	vegetariánský	k2eAgFnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
ryby	ryba	k1gFnPc1
a	a	k8xC
ptáci	pták	k1gMnPc1
jsou	být	k5eAaImIp3nP
také	také	k9
živočichové	živočich	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Etymologie	etymologie	k1gFnSc1
</s>
<s>
V	v	k7c6
angličtině	angličtina	k1gFnSc6
se	se	k3xPyFc4
slovo	slovo	k1gNnSc1
vegetarian	vegetariana	k1gFnPc2
používá	používat	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
1839	#num#	k4
a	a	k8xC
významem	význam	k1gInSc7
odpovídá	odpovídat	k5eAaImIp3nS
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
se	se	k3xPyFc4
dříve	dříve	k6eAd2
označovalo	označovat	k5eAaImAgNnS
jako	jako	k9
„	„	k?
<g/>
rostlinná	rostlinný	k2eAgFnSc1d1
strava	strava	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
se	se	k3xPyFc4
za	za	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
spojení	spojení	k1gNnSc4
anglického	anglický	k2eAgNnSc2d1
slova	slovo	k1gNnSc2
vegetable	vegetable	k6eAd1
(	(	kIx(
<g/>
rostlina	rostlina	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
přípony	přípona	k1gFnPc1
-arian	-ariana	k1gFnPc2
(	(	kIx(
<g/>
jako	jako	k8xS,k8xC
v	v	k7c4
agrarian	agrarian	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
John	John	k1gMnSc1
Davis	Davis	k1gFnSc7
dokazuje	dokazovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
pravděpodobně	pravděpodobně	k6eAd1
nebylo	být	k5eNaImAgNnS
odvozeno	odvodit	k5eAaPmNgNnS
z	z	k7c2
latinského	latinský	k2eAgNnSc2d1
slova	slovo	k1gNnSc2
vegetus	vegetus	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
</s>
<s>
Termín	termín	k1gInSc1
byl	být	k5eAaImAgInS
zpopularizován	zpopularizován	k2eAgMnSc1d1
<g/>
,	,	kIx,
když	když	k8xS
byla	být	k5eAaImAgFnS
založena	založen	k2eAgFnSc1d1
vegetariánská	vegetariánský	k2eAgFnSc1d1
společnosti	společnost	k1gFnPc1
v	v	k7c6
Manchesteru	Manchester	k1gInSc6
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
Británii	Británie	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1847	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Nejranější	raný	k2eAgInPc1d3
výskyty	výskyt	k1gInPc1
termínu	termín	k1gInSc2
se	se	k3xPyFc4
zdají	zdát	k5eAaImIp3nP,k5eAaPmIp3nP
mít	mít	k5eAaImF
souvislost	souvislost	k1gFnSc4
s	s	k7c7
Alcottovým	Alcottův	k2eAgInSc7d1
domem	dům	k1gInSc7
<g/>
,	,	kIx,
školou	škola	k1gFnSc7
na	na	k7c6
severu	sever	k1gInSc6
londýnské	londýnský	k2eAgFnSc2d1
části	část	k1gFnSc2
Ham	ham	k0
Common	Common	k1gInSc1
<g/>
,	,	kIx,
otevřeným	otevřený	k2eAgNnSc7d1
v	v	k7c6
červenci	červenec	k1gInSc6
1838	#num#	k4
Jamesem	James	k1gMnSc7
Pierrepontem	Pierrepont	k1gMnSc7
Greavesem	Greaves	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1841	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgMnS
známý	známý	k2eAgMnSc1d1
jako	jako	k9
Concordium	Concordium	k1gNnSc1
nebo	nebo	k8xC
Industry	Industra	k1gFnPc1
Harmony	Harmona	k1gFnSc2
College	College	k1gFnSc1
<g/>
,	,	kIx,
instituce	instituce	k1gFnSc1
vydávala	vydávat	k5eAaPmAgFnS,k5eAaImAgFnS
vlastní	vlastní	k2eAgFnPc4d1
brožury	brožura	k1gFnPc4
„	„	k?
<g/>
The	The	k1gMnSc1
Healthian	Healthian	k1gMnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
obsahují	obsahovat	k5eAaImIp3nP
jeden	jeden	k4xCgInSc4
z	z	k7c2
prvních	první	k4xOgInPc2
výskytů	výskyt	k1gInPc2
výrazu	výraz	k1gInSc2
„	„	k?
<g/>
vegetarian	vegetarian	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Do	do	k7c2
češtiny	čeština	k1gFnSc2
bylo	být	k5eAaImAgNnS
slovo	slovo	k1gNnSc1
převzato	převzít	k5eAaPmNgNnS
a	a	k8xC
doplněno	doplnit	k5eAaPmNgNnS
o	o	k7c4
čárku	čárka	k1gFnSc4
nad	nad	k7c7
druhým	druhý	k4xOgNnSc7
a	a	k8xC
–	–	k?
tím	ten	k3xDgNnSc7
vzniklo	vzniknout	k5eAaPmAgNnS
české	český	k2eAgNnSc4d1
slovo	slovo	k1gNnSc4
vegetarián	vegetarián	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Vegetariánský	vegetariánský	k2eAgInSc4d1
oběd	oběd	k1gInSc4
v	v	k7c6
indické	indický	k2eAgFnSc6d1
Mysore	Mysor	k1gInSc5
</s>
<s>
Dějiny	dějiny	k1gFnPc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Historie	historie	k1gFnSc1
vegetariánství	vegetariánství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Nejstarší	starý	k2eAgInSc1d3
záznam	záznam	k1gInSc1
o	o	k7c6
vegetariánství	vegetariánství	k1gNnSc6
pochází	pocházet	k5eAaImIp3nS
již	již	k6eAd1
ze	z	k7c2
7	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
před	před	k7c7
naším	náš	k3xOp1gInSc7
letopočtem	letopočet	k1gInSc7
<g/>
;	;	kIx,
jasné	jasný	k2eAgNnSc1d1
vyjádření	vyjádření	k1gNnSc1
ideje	idea	k1gFnSc2
nezabíjení	nezabíjení	k1gNnSc1
zvířat	zvíře	k1gNnPc2
se	se	k3xPyFc4
objevilo	objevit	k5eAaPmAgNnS
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
odklonem	odklon	k1gInSc7
od	od	k7c2
védského	védský	k2eAgNnSc2d1
náboženství	náboženství	k1gNnSc2
k	k	k7c3
hinduismu	hinduismus	k1gInSc3
a	a	k8xC
buddhismu	buddhismus	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
vštěpoval	vštěpovat	k5eAaBmAgInS
toleranci	tolerance	k1gFnSc4
vůči	vůči	k7c3
všem	všecek	k3xTgFnPc3
živým	živý	k2eAgFnPc3d1
bytostem	bytost	k1gFnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vegetariánství	vegetariánství	k1gNnSc1
bylo	být	k5eAaImAgNnS
praktikováno	praktikovat	k5eAaImNgNnS
také	také	k9
přibližně	přibližně	k6eAd1
o	o	k7c4
sedm	sedm	k4xCc4
století	století	k1gNnPc2
později	pozdě	k6eAd2
(	(	kIx(
<g/>
mezi	mezi	k7c7
lety	let	k1gInPc7
30	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
a	a	k8xC
50	#num#	k4
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
v	v	k7c6
severním	severní	k2eAgInSc6d1
thráckém	thrácký	k2eAgInSc6d1
regionu	region	k1gInSc6
<g/>
:	:	kIx,
příslušníci	příslušník	k1gMnPc1
kmene	kmen	k1gInSc2
Mezů	Mez	k1gMnPc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
obýval	obývat	k5eAaImAgMnS
dnešní	dnešní	k2eAgNnSc4d1
Srbsko	Srbsko	k1gNnSc4
a	a	k8xC
Bulharsko	Bulharsko	k1gNnSc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
živili	živit	k5eAaImAgMnP
medem	med	k1gInSc7
<g/>
,	,	kIx,
mlékem	mléko	k1gNnSc7
a	a	k8xC
sýry	sýr	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
indické	indický	k2eAgFnSc6d1
kultuře	kultura	k1gFnSc6
byla	být	k5eAaImAgFnS
(	(	kIx(
<g/>
bezmasá	bezmasý	k2eAgFnSc1d1
<g/>
)	)	kIx)
strava	strava	k1gFnSc1
úzce	úzko	k6eAd1
spojena	spojit	k5eAaPmNgFnS
s	s	k7c7
myšlenkou	myšlenka	k1gFnSc7
nenásilí	nenásilí	k1gNnSc2
ke	k	k7c3
zvířatům	zvíře	k1gNnPc3
(	(	kIx(
<g/>
nazývané	nazývaný	k2eAgInPc1d1
v	v	k7c6
Indii	Indie	k1gFnSc6
ahinsá	ahinsat	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
)	)	kIx)
a	a	k8xC
byla	být	k5eAaImAgFnS
podporována	podporovat	k5eAaImNgFnS
náboženskými	náboženský	k2eAgFnPc7d1
skupinami	skupina	k1gFnPc7
a	a	k8xC
filozofy	filozof	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
Starověké	starověký	k2eAgNnSc1d1
indické	indický	k2eAgNnSc1d1
dílo	dílo	k1gNnSc1
Tirukkural	Tirukkural	k1gMnPc2
výslovně	výslovně	k6eAd1
a	a	k8xC
jednoznačně	jednoznačně	k6eAd1
klade	klást	k5eAaImIp3nS
důraz	důraz	k1gInSc4
na	na	k7c4
vegetariánství	vegetariánství	k1gNnSc4
<g />
.	.	kIx.
</s>
<s hack="1">
a	a	k8xC
nezabíjení	nezabíjení	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Dvacátá	dvacátý	k4xOgFnSc1
šestá	šestý	k4xOgFnSc1
kapitola	kapitola	k1gFnSc1
Tirukkuralu	Tirukkural	k1gInSc2
se	se	k3xPyFc4
od	od	k7c2
dvojverší	dvojverší	k1gNnPc2
251	#num#	k4
až	až	k6eAd1
po	po	k7c6
260	#num#	k4
zabývá	zabývat	k5eAaImIp3nS
výhradně	výhradně	k6eAd1
vegetariánstvím	vegetariánství	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Význam	význam	k1gInSc1
lékařský	lékařský	k2eAgInSc1d1
nebo	nebo	k8xC
jako	jako	k9
očistný	očistný	k2eAgInSc4d1
rituál	rituál	k1gInSc4
mělo	mít	k5eAaImAgNnS
vegetariánství	vegetariánství	k1gNnSc1
mezi	mezi	k7c7
Řeky	Řek	k1gMnPc7
<g/>
,	,	kIx,
starověkými	starověký	k2eAgMnPc7d1
Egypťany	Egypťan	k1gMnPc7
a	a	k8xC
dalšími	další	k2eAgMnPc7d1
<g/>
.	.	kIx.
</s>
<s>
Indický	indický	k2eAgMnSc1d1
císař	císař	k1gMnSc1
Ašóka	Ašóka	k1gMnSc1
prosazoval	prosazovat	k5eAaImAgMnS
ochranu	ochrana	k1gFnSc4
fauny	fauna	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
</s>
<s>
Bohům	bůh	k1gMnPc3
milý	milý	k2eAgMnSc1d1
král	král	k1gMnSc1
Prijadaršin	Prijadaršina	k1gFnPc2
takto	takto	k6eAd1
praví	pravit	k5eAaBmIp3nS,k5eAaImIp3nS
<g/>
:	:	kIx,
když	když	k8xS
jsem	být	k5eAaImIp1nS
byl	být	k5eAaImAgMnS
pomazán	pomazán	k2eAgMnSc1d1
26	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
zakázal	zakázat	k5eAaPmAgMnS
jsem	být	k5eAaImIp1nS
zabíjet	zabíjet	k5eAaImF
tyto	tento	k3xDgInPc4
druhy	druh	k1gInPc4
zvířat	zvíře	k1gNnPc2
<g/>
,	,	kIx,
(	(	kIx(
<g/>
totiž	totiž	k9
<g/>
)	)	kIx)
<g/>
:	:	kIx,
papoušky	papoušek	k1gMnPc4
<g/>
,	,	kIx,
špačky	špaček	k1gMnPc4
<g/>
,	,	kIx,
aruna	aruna	k6eAd1
(	(	kIx(
<g/>
červený	červený	k2eAgMnSc1d1
pták	pták	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
čakraváky	čakravák	k1gInPc1
<g/>
,	,	kIx,
labutě	labuť	k1gFnPc1
<g/>
,	,	kIx,
namdimukly	namdimuknout	k5eAaPmAgFnP
<g/>
,	,	kIx,
geláty	gelát	k1gInPc1
(	(	kIx(
<g/>
druhy	druh	k1gInPc1
vodních	vodní	k2eAgMnPc2d1
ptáků	pták	k1gMnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
netopýry	netopýr	k1gMnPc4
<g/>
,	,	kIx,
vodní	vodní	k2eAgMnPc4d1
mravence	mravenec	k1gMnPc4
<g/>
,	,	kIx,
želvy	želva	k1gFnPc4
<g/>
,	,	kIx,
bezkosté	bezkostý	k2eAgFnPc4d1
ryby	ryba	k1gFnPc4
<g/>
,	,	kIx,
védavéjaky	védavéjak	k1gMnPc4
<g/>
,	,	kIx,
gangapuputaky	gangapuputak	k1gMnPc4
<g/>
,	,	kIx,
samkudžamuchy	samkudžamuch	k1gMnPc4
(	(	kIx(
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
<g/>
,	,	kIx,
želvy	želva	k1gFnPc4
a	a	k8xC
dikobrazy	dikobraz	k1gMnPc4
<g/>
,	,	kIx,
veverky	veverek	k1gMnPc4
<g/>
,	,	kIx,
(	(	kIx(
<g/>
velikého	veliký	k2eAgMnSc2d1
<g/>
)	)	kIx)
jelena	jelen	k1gMnSc2
<g/>
,	,	kIx,
volně	volně	k6eAd1
pobíhající	pobíhající	k2eAgMnPc4d1
býky	býk	k1gMnPc4
<g/>
,	,	kIx,
havěť	havěť	k1gFnSc1
v	v	k7c6
domácnosti	domácnost	k1gFnSc6
(	(	kIx(
<g/>
tj.	tj.	kA
kočky	kočka	k1gFnSc2
<g/>
,	,	kIx,
myši	myš	k1gFnSc2
<g/>
,	,	kIx,
iguany	iguana	k1gFnSc2
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nosorožce	nosorožec	k1gMnSc4
<g/>
,	,	kIx,
šedé	šedý	k2eAgMnPc4d1
holuby	holub	k1gMnPc4
<g/>
,	,	kIx,
vesnické	vesnický	k2eAgMnPc4d1
holuby	holub	k1gMnPc4
a	a	k8xC
všechny	všechen	k3xTgMnPc4
čtyřnožce	čtyřnožec	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
nejsou	být	k5eNaImIp3nP
k	k	k7c3
užitku	užitek	k1gInSc3
ani	ani	k8xC
k	k	k7c3
jídlu	jídlo	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Kozy	koza	k1gFnPc1
<g/>
,	,	kIx,
ovce	ovce	k1gFnPc1
ani	ani	k8xC
prasnice	prasnice	k1gFnPc1
březí	březí	k2eAgFnPc1d1
nebo	nebo	k8xC
krmící	krmící	k2eAgFnPc1d1
mladé	mladá	k1gFnPc1
nesmějí	smát	k5eNaImIp3nP
být	být	k5eAaImF
zabíjeny	zabíjen	k2eAgFnPc1d1
ani	ani	k8xC
jejich	jejich	k3xOp3gFnPc1
mladé	mladá	k1gFnPc1
do	do	k7c2
6	#num#	k4
měsíců	měsíc	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
kohoutů	kohout	k1gInPc2
se	se	k3xPyFc4
nesmějí	smát	k5eNaImIp3nP
dělat	dělat	k5eAaImF
kapouni	kapoun	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nebudou	být	k5eNaImBp3nP
páleny	pálit	k5eAaImNgFnP
slupky	slupka	k1gFnPc1
(	(	kIx(
<g/>
řezanka	řezanka	k1gFnSc1
<g/>
)	)	kIx)
s	s	k7c7
živými	živý	k2eAgMnPc7d1
tvory	tvor	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lesy	les	k1gInPc1
nebudou	být	k5eNaImBp3nP
vypalovány	vypalován	k2eAgInPc1d1
ani	ani	k8xC
bezdůvodně	bezdůvodně	k6eAd1
<g/>
,	,	kIx,
ani	ani	k8xC
kvůli	kvůli	k7c3
mýcení	mýcení	k1gNnSc3
<g/>
,	,	kIx,
živý	živý	k2eAgMnSc1d1
tvor	tvor	k1gMnSc1
se	se	k3xPyFc4
nemá	mít	k5eNaImIp3nS
živit	živit	k5eAaImF
jiným	jiný	k2eAgMnSc7d1
živým	živý	k1gMnSc7
tvorem	tvor	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Ašókovy	Ašókův	k2eAgInPc4d1
edikty	edikt	k1gInPc4
<g/>
,	,	kIx,
pátý	pátý	k4xOgInSc4
pilíř	pilíř	k1gInSc4
<g/>
;	;	kIx,
český	český	k2eAgInSc4d1
překlad	překlad	k1gInSc4
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Povinné	povinný	k2eAgNnSc1d1
označení	označení	k1gNnSc1
pro	pro	k7c4
rozlišení	rozlišení	k1gNnSc4
vegetariánských	vegetariánský	k2eAgInPc2d1
výrobků	výrobek	k1gInPc2
(	(	kIx(
<g/>
zeleně	zeleně	k6eAd1
<g/>
)	)	kIx)
od	od	k7c2
nevegetariánských	vegetariánský	k2eNgFnPc2d1
(	(	kIx(
<g/>
červeně	červeně	k6eAd1
<g/>
)	)	kIx)
v	v	k7c6
Indii	Indie	k1gFnSc6
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
návaznosti	návaznost	k1gFnSc6
na	na	k7c6
pokřesťanštění	pokřesťanštění	k1gNnSc6
Římské	římský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
v	v	k7c6
pozdní	pozdní	k2eAgFnSc6d1
antice	antika	k1gFnSc6
vegetariánství	vegetariánství	k1gNnSc2
z	z	k7c2
Evropy	Evropa	k1gFnSc2
a	a	k8xC
podobně	podobně	k6eAd1
i	i	k9
z	z	k7c2
jiných	jiný	k2eAgInPc2d1
kontinentů	kontinent	k1gInPc2
prakticky	prakticky	k6eAd1
zmizelo	zmizet	k5eAaPmAgNnS
<g/>
,	,	kIx,
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
Indie	Indie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
středověké	středověký	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
několik	několik	k4yIc4
mnišských	mnišský	k2eAgInPc2d1
řádů	řád	k1gInPc2
omezovalo	omezovat	k5eAaImAgNnS
nebo	nebo	k8xC
zakazovalo	zakazovat	k5eAaImAgNnS
konzumaci	konzumace	k1gFnSc4
masa	maso	k1gNnSc2
z	z	k7c2
asketických	asketický	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
žádný	žádný	k3yNgMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
z	z	k7c2
nich	on	k3xPp3gMnPc2
nevylučoval	vylučovat	k5eNaImAgMnS
konzumaci	konzumace	k1gFnSc4
ryb	ryba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
Středověká	středověký	k2eAgFnSc1d1
definice	definice	k1gFnSc1
"	"	kIx"
<g/>
ryb	ryba	k1gFnPc2
<g/>
"	"	kIx"
zahrnovala	zahrnovat	k5eAaImAgFnS
taková	takový	k3xDgNnPc4
zvířata	zvíře	k1gNnPc4
jako	jako	k8xS,k8xC
tuleně	tuleň	k1gMnPc4
<g/>
,	,	kIx,
sviňuchy	sviňucha	k1gFnPc4
<g/>
,	,	kIx,
delfíny	delfín	k1gMnPc4
<g/>
,	,	kIx,
bernešky	berneška	k1gFnPc4
<g/>
,	,	kIx,
papuchalky	papuchalka	k1gFnPc4
či	či	k8xC
bobry	bobr	k1gMnPc4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
Vegetariánství	vegetariánství	k1gNnSc6
se	se	k3xPyFc4
znovu	znovu	k6eAd1
objevilo	objevit	k5eAaPmAgNnS
během	během	k7c2
renesance	renesance	k1gFnSc2
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
stále	stále	k6eAd1
rozšířenějším	rozšířený	k2eAgNnSc7d2
v	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
ve	v	k7c6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1847	#num#	k4
byla	být	k5eAaImAgFnS
ve	v	k7c6
Spojeném	spojený	k2eAgNnSc6d1
království	království	k1gNnSc6
založena	založit	k5eAaPmNgFnS
první	první	k4xOgFnSc1
vegetariánská	vegetariánský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
;	;	kIx,
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
Německo	Německo	k1gNnSc1
<g/>
,	,	kIx,
Nizozemsko	Nizozemsko	k1gNnSc1
a	a	k8xC
další	další	k2eAgFnPc1d1
země	zem	k1gFnPc1
následovaly	následovat	k5eAaImAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
národních	národní	k2eAgFnPc2d1
společností	společnost	k1gFnPc2
Mezinárodní	mezinárodní	k2eAgFnSc1d1
vegetariánská	vegetariánský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1908	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
západním	západní	k2eAgInSc6d1
světě	svět	k1gInSc6
rostla	růst	k5eAaImAgFnS
v	v	k7c6
důsledku	důsledek	k1gInSc6
výživových	výživový	k2eAgFnPc2d1
<g/>
,	,	kIx,
etických	etický	k2eAgFnPc2d1
a	a	k8xC
později	pozdě	k6eAd2
i	i	k9
ekologických	ekologický	k2eAgFnPc2d1
a	a	k8xC
ekonomických	ekonomický	k2eAgFnPc2d1
otázek	otázka	k1gFnPc2
popularita	popularita	k1gFnSc1
vegetariánství	vegetariánství	k1gNnSc2
v	v	k7c6
průběhu	průběh	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Typy	typ	k1gInPc1
</s>
<s>
Silniční	silniční	k2eAgFnSc1d1
kavárna	kavárna	k1gFnSc1
v	v	k7c6
Kullu	Kull	k1gInSc6
v	v	k7c6
Indii	Indie	k1gFnSc6
</s>
<s>
Existuje	existovat	k5eAaImIp3nS
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
typů	typ	k1gInPc2
vegetariánské	vegetariánský	k2eAgFnSc2d1
stravy	strava	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
vylučují	vylučovat	k5eAaImIp3nP
nebo	nebo	k8xC
naopak	naopak	k6eAd1
zahrnují	zahrnovat	k5eAaImIp3nP
různé	různý	k2eAgFnPc4d1
potraviny	potravina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Srovnání	srovnání	k1gNnSc1
hlavních	hlavní	k2eAgInPc2d1
typů	typ	k1gInPc2
vegetariánské	vegetariánský	k2eAgFnSc2d1
stravy	strava	k1gFnSc2
</s>
<s>
masovejcemléčné	masovejcemléčný	k2eAgInPc1d1
produkty	produkt	k1gInPc1
</s>
<s>
Laktoovovegetariánství	Laktoovovegetariánství	k1gNnSc1
</s>
<s>
NeAnoAno	NeAnoAno	k6eAd1
</s>
<s>
Ovovegetariánství	Ovovegetariánství	k1gNnSc1
</s>
<s>
NeAnoNe	NeAnoNe	k6eAd1
</s>
<s>
Laktovegetariánství	Laktovegetariánství	k1gNnSc1
</s>
<s>
NeNeAno	NeNeAno	k6eAd1
</s>
<s>
Veganství	Veganství	k1gNnSc1
</s>
<s>
NeNeNe	NeNeNe	k6eAd1
</s>
<s>
Buddhistické	buddhistický	k2eAgNnSc1d1
vegetariánství	vegetariánství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Různé	různý	k2eAgFnPc1d1
buddhistické	buddhistický	k2eAgFnPc1d1
tradice	tradice	k1gFnPc1
mají	mít	k5eAaImIp3nP
odlišné	odlišný	k2eAgNnSc4d1
učení	učení	k1gNnSc4
o	o	k7c6
stravě	strava	k1gFnSc6
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
ostatními	ostatní	k2eAgMnPc7d1
také	také	k9
mohou	moct	k5eAaImIp3nP
lišit	lišit	k5eAaImF
nařízeními	nařízení	k1gNnPc7
pro	pro	k7c4
mnichy	mnich	k1gInPc4
a	a	k8xC
jeptišky	jeptiška	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k6eAd1
lidí	člověk	k1gMnPc2
interpretuje	interpretovat	k5eAaBmIp3nS
přikázání	přikázání	k1gNnSc1
„	„	k?
<g/>
nezabiješ	zabít	k5eNaPmIp2nS
<g/>
“	“	k?
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
požadují	požadovat	k5eAaImIp3nP
abstinenci	abstinence	k1gFnSc4
od	od	k7c2
masa	maso	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
ne	ne	k9
všichni	všechen	k3xTgMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Tchaj-wanu	Tchaj-wan	k1gInSc6
vegetariánství	vegetariánství	k1gNnSc2
vylučuje	vylučovat	k5eAaImIp3nS
nejen	nejen	k6eAd1
všechny	všechen	k3xTgInPc4
produkty	produkt	k1gInPc4
živočišného	živočišný	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
zeleninu	zelenina	k1gFnSc4
z	z	k7c2
rodu	rod	k1gInSc2
česnek	česnek	k1gInSc1
(	(	kIx(
<g/>
zelenina	zelenina	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
má	mít	k5eAaImIp3nS
charakteristickou	charakteristický	k2eAgFnSc4d1
vůni	vůně	k1gFnSc4
cibule	cibule	k1gFnSc2
a	a	k8xC
česneku	česnek	k1gInSc2
<g/>
)	)	kIx)
<g/>
:	:	kIx,
cibuli	cibule	k1gFnSc4
<g/>
,	,	kIx,
česnek	česnek	k1gInSc4
<g/>
,	,	kIx,
jarní	jarní	k2eAgFnSc4d1
cibulku	cibulka	k1gFnSc4
<g/>
,	,	kIx,
pórek	pórek	k1gInSc4
<g/>
,	,	kIx,
pažitku	pažitka	k1gFnSc4
nebo	nebo	k8xC
šalotku	šalotka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Frutariánství	Frutariánství	k1gNnSc1
povoluje	povolovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
ovoce	ovoce	k1gNnSc1
<g/>
,	,	kIx,
ořechy	ořech	k1gInPc1
<g/>
,	,	kIx,
semena	semeno	k1gNnPc1
a	a	k8xC
jiné	jiný	k2eAgFnPc1d1
části	část	k1gFnPc1
rostlin	rostlina	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
získány	získat	k5eAaPmNgFnP
bez	bez	k7c2
poškození	poškození	k1gNnSc4
rostliny	rostlina	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Džinistické	Džinistický	k2eAgNnSc1d1
vegetariánství	vegetariánství	k1gNnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
mléčné	mléčný	k2eAgInPc4d1
výrobky	výrobek	k1gInPc4
<g/>
,	,	kIx,
ale	ale	k8xC
vylučuje	vylučovat	k5eAaImIp3nS
vejce	vejce	k1gNnSc4
a	a	k8xC
med	med	k1gInSc4
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
kořenovou	kořenový	k2eAgFnSc4d1
zeleninu	zelenina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Makrobiotická	makrobiotický	k2eAgFnSc1d1
dieta	dieta	k1gFnSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
převážně	převážně	k6eAd1
z	z	k7c2
celozrnných	celozrnný	k2eAgFnPc2d1
obilovin	obilovina	k1gFnPc2
a	a	k8xC
bobovitých	bobovitý	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
(	(	kIx(
<g/>
luštěnin	luštěnina	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Laktovegetariánství	Laktovegetariánství	k1gNnSc1
dovoluje	dovolovat	k5eAaImIp3nS
mléčné	mléčný	k2eAgInPc4d1
výrobky	výrobek	k1gInPc4
<g/>
,	,	kIx,
ale	ale	k8xC
ne	ne	k9
vejce	vejce	k1gNnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ovovegetariánství	Ovovegetariánství	k1gNnSc1
dovoluje	dovolovat	k5eAaImIp3nS
vejce	vejce	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
ne	ne	k9
mléčné	mléčný	k2eAgInPc4d1
výrobky	výrobek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Laktoovovegetariánství	Laktoovovegetariánství	k1gNnSc1
(	(	kIx(
<g/>
nebo	nebo	k8xC
lakto-ovo	lakto-ův	k2eAgNnSc1d1
vegetariánství	vegetariánství	k1gNnSc1
<g/>
)	)	kIx)
dovoluje	dovolovat	k5eAaImIp3nS
zvířecí	zvířecí	k2eAgInPc4d1
výrobky	výrobek	k1gInPc4
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
vejce	vejce	k1gNnPc1
<g/>
,	,	kIx,
mléko	mléko	k1gNnSc1
a	a	k8xC
med	med	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Sattvická	Sattvický	k2eAgFnSc1d1
strava	strava	k1gFnSc1
(	(	kIx(
<g/>
také	také	k9
známá	známý	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
jogínská	jogínský	k2eAgFnSc1d1
strava	strava	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
založená	založený	k2eAgFnSc1d1
rostlinné	rostlinný	k2eAgFnSc3d1
stravě	strava	k1gFnSc3
a	a	k8xC
může	moct	k5eAaImIp3nS
rovněž	rovněž	k9
zahrnovat	zahrnovat	k5eAaImF
mléčné	mléčný	k2eAgInPc4d1
výrobky	výrobek	k1gInPc4
(	(	kIx(
<g/>
ne	ne	k9
vejce	vejce	k1gNnSc2
<g/>
)	)	kIx)
a	a	k8xC
med	med	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
vylučuje	vylučovat	k5eAaImIp3nS
vše	všechen	k3xTgNnSc1
z	z	k7c2
rodu	rod	k1gInSc2
česnek	česnek	k1gInSc1
(	(	kIx(
<g/>
cibule	cibule	k1gFnSc1
<g/>
,	,	kIx,
česnek	česnek	k1gInSc1
<g/>
,	,	kIx,
pažitka	pažitka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
červenou	červený	k2eAgFnSc4d1
čočku	čočka	k1gFnSc4
<g/>
,	,	kIx,
duriánové	durián	k1gMnPc1
ovoce	ovoce	k1gNnSc2
<g/>
,	,	kIx,
houby	houba	k1gFnSc2
<g/>
,	,	kIx,
modré	modrý	k2eAgInPc1d1
sýry	sýr	k1gInPc1
<g/>
,	,	kIx,
fermentované	fermentovaný	k2eAgFnPc1d1
potraviny	potravina	k1gFnPc1
nebo	nebo	k8xC
omáčky	omáčka	k1gFnPc1
a	a	k8xC
alkoholické	alkoholický	k2eAgInPc1d1
nápoje	nápoj	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
také	také	k9
vylučuje	vylučovat	k5eAaImIp3nS
kávu	káva	k1gFnSc4
<g/>
,	,	kIx,
černý	černý	k2eAgInSc4d1
či	či	k8xC
zelený	zelený	k2eAgInSc4d1
čaj	čaj	k1gInSc4
<g/>
,	,	kIx,
čokoládu	čokoláda	k1gFnSc4
<g/>
,	,	kIx,
muškátový	muškátový	k2eAgInSc4d1
oříšek	oříšek	k1gInSc4
nebo	nebo	k8xC
jakýkoli	jakýkoli	k3yIgInSc4
jiný	jiný	k2eAgInSc4d1
typ	typ	k1gInSc4
povzbuzujícího	povzbuzující	k2eAgInSc2d1
prostředku	prostředek	k1gInSc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
například	například	k6eAd1
nadměrně	nadměrně	k6eAd1
ostré	ostrý	k2eAgNnSc4d1
koření	koření	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Veganství	Veganství	k1gNnSc1
vylučuje	vylučovat	k5eAaImIp3nS
veškeré	veškerý	k3xTgNnSc4
zvířecí	zvířecí	k2eAgNnSc4d1
maso	maso	k1gNnSc4
a	a	k8xC
vedlejší	vedlejší	k2eAgInPc4d1
produkty	produkt	k1gInPc4
živočišného	živočišný	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
mléko	mléko	k1gNnSc4
<g/>
,	,	kIx,
med	med	k1gInSc4
a	a	k8xC
vejce	vejce	k1gNnSc4
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
položky	položka	k1gFnSc2
rafinované	rafinovaný	k2eAgFnSc2d1
(	(	kIx(
<g/>
čištěné	čištěný	k2eAgFnSc2d1
<g/>
)	)	kIx)
nebo	nebo	k8xC
vyráběné	vyráběný	k2eAgFnPc1d1
prostřednictvím	prostřednictvím	k7c2
jakéhokoli	jakýkoli	k3yIgInSc2
takového	takový	k3xDgInSc2
produktu	produkt	k1gInSc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
například	například	k6eAd1
živočišným	živočišný	k2eAgNnSc7d1
uhlím	uhlí	k1gNnSc7
rafinovaný	rafinovaný	k2eAgMnSc1d1
bílý	bílý	k1gMnSc1
cukr	cukr	k1gInSc1
nebo	nebo	k8xC
na	na	k7c6
živočiších	živočich	k1gMnPc6
testovanou	testovaný	k2eAgFnSc7d1
jedlou	jedlý	k2eAgFnSc7d1
sodou	soda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Vitariánství	Vitariánství	k1gNnSc1
(	(	kIx(
<g/>
syrové	syrový	k2eAgNnSc1d1
veganství	veganství	k1gNnSc1
<g/>
)	)	kIx)
zahrnuje	zahrnovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
čerstvé	čerstvý	k2eAgNnSc1d1
a	a	k8xC
nevařené	vařený	k2eNgNnSc1d1
ovoce	ovoce	k1gNnSc1
<g/>
,	,	kIx,
ořechy	ořech	k1gInPc1
<g/>
,	,	kIx,
semena	semeno	k1gNnPc1
a	a	k8xC
zeleninu	zelenina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zelenina	zelenina	k1gFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
ohřívána	ohřívat	k5eAaImNgFnS
pouze	pouze	k6eAd1
na	na	k7c4
určitou	určitý	k2eAgFnSc4d1
teplotu	teplota	k1gFnSc4
<g/>
,	,	kIx,
například	například	k6eAd1
za	za	k7c4
použití	použití	k1gNnSc4
sušičky	sušička	k1gFnSc2
na	na	k7c4
ovoce	ovoce	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
rámci	rámec	k1gInSc6
tzv.	tzv.	kA
"	"	kIx"
<g/>
ovo-	ovo-	k?
<g/>
"	"	kIx"
skupin	skupina	k1gFnPc2
<g/>
,	,	kIx,
existuje	existovat	k5eAaImIp3nS
mnoho	mnoho	k4c1
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
odmítají	odmítat	k5eAaImIp3nP
konzumovat	konzumovat	k5eAaBmF
oplodněná	oplodněný	k2eAgNnPc1d1
vajíčka	vajíčko	k1gNnPc1
(	(	kIx(
<g/>
ta	ten	k3xDgFnSc1
s	s	k7c7
balutem	balut	k1gInSc7
jsou	být	k5eAaImIp3nP
extrémní	extrémní	k2eAgInSc4d1
příklad	příklad	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
takový	takový	k3xDgInSc1
jemný	jemný	k2eAgInSc1d1
rozdíl	rozdíl	k1gInSc1
není	být	k5eNaImIp3nS
obvykle	obvykle	k6eAd1
specificky	specificky	k6eAd1
řešen	řešit	k5eAaImNgInS
<g/>
.	.	kIx.
</s>
<s>
Někteří	některý	k3yIgMnPc1
vegetariáni	vegetarián	k1gMnPc1
se	se	k3xPyFc4
také	také	k6eAd1
vyhýbají	vyhýbat	k5eAaImIp3nP
potravinám	potravina	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
by	by	kYmCp3nP
mohly	moct	k5eAaImAgFnP
obsahovat	obsahovat	k5eAaImF
zvířecí	zvířecí	k2eAgFnPc1d1
přísady	přísada	k1gFnPc1
nevyznačené	vyznačený	k2eNgFnPc1d1
na	na	k7c6
štítcích	štítek	k1gInPc6
<g/>
,	,	kIx,
a	a	k8xC
potravinám	potravina	k1gFnPc3
<g/>
,	,	kIx,
při	při	k7c6
jejichž	jejichž	k3xOyRp3gFnSc6
výrobě	výroba	k1gFnSc6
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
živočišné	živočišný	k2eAgInPc1d1
produkty	produkt	k1gInPc1
<g/>
;	;	kIx,
například	například	k6eAd1
cukru	cukr	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
bělený	bělený	k2eAgInSc1d1
živočišným	živočišný	k2eAgNnSc7d1
uhlím	uhlí	k1gNnSc7
<g/>
,	,	kIx,
sýrům	sýr	k1gInPc3
<g/>
,	,	kIx,
vyráběným	vyráběný	k2eAgInSc7d1
s	s	k7c7
použitím	použití	k1gNnSc7
zvířecích	zvířecí	k2eAgNnPc2d1
syřidel	syřidlo	k1gNnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
enzymů	enzym	k1gInPc2
ze	z	k7c2
žaludeční	žaludeční	k2eAgFnSc2d1
sliznice	sliznice	k1gFnSc2
zvířat	zvíře	k1gNnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
želatině	želatina	k1gFnSc6
(	(	kIx(
<g/>
získané	získaný	k2eAgInPc1d1
z	z	k7c2
kolagenu	kolagen	k1gInSc2
ze	z	k7c2
zvířecích	zvířecí	k2eAgFnPc2d1
kůží	kůže	k1gFnPc2
<g/>
,	,	kIx,
kostí	kost	k1gFnPc2
a	a	k8xC
pojivových	pojivový	k2eAgFnPc2d1
tkání	tkáň	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
některému	některý	k3yIgInSc3
třtinovému	třtinový	k2eAgInSc3d1
cukru	cukr	k1gInSc3
(	(	kIx(
<g/>
ale	ale	k8xC
ne	ne	k9
cukru	cukr	k1gInSc2
z	z	k7c2
cukrové	cukrový	k2eAgFnSc2d1
řepy	řepa	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
jablečnému	jablečný	k2eAgInSc3d1
džusu	džus	k1gInSc3
<g/>
/	/	kIx~
<g/>
alkoholu	alkohol	k1gInSc2
čištěnému	čištěný	k2eAgInSc3d1
želatinou	želatina	k1gFnSc7
nebo	nebo	k8xC
drcenými	drcený	k2eAgMnPc7d1
měkkýši	měkkýš	k1gMnPc7
a	a	k8xC
jeseterem	jeseter	k1gMnSc7
<g/>
;	;	kIx,
zatímco	zatímco	k8xS
ostatní	ostatní	k2eAgMnPc1d1
vegetariáni	vegetarián	k1gMnPc1
si	se	k3xPyFc3
toho	ten	k3xDgNnSc2
nejsou	být	k5eNaImIp3nP
vědomi	vědom	k2eAgMnPc1d1
<g/>
,	,	kIx,
nebo	nebo	k8xC
jim	on	k3xPp3gMnPc3
takové	takový	k3xDgFnPc4
přísady	přísada	k1gFnPc4
nevadí	vadit	k5eNaImIp3nP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Někdo	někdo	k3yInSc1
se	se	k3xPyFc4
sám	sám	k3xTgMnSc1
označuje	označovat	k5eAaImIp3nS
za	za	k7c2
„	„	k?
<g/>
vegetariána	vegetarián	k1gMnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
praktikuje	praktikovat	k5eAaImIp3nS
polovegetariánství	polovegetariánství	k1gNnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
protože	protože	k8xS
některé	některý	k3yIgFnPc1
slovníkové	slovníkový	k2eAgFnPc1d1
definice	definice	k1gFnPc1
připouštějí	připouštět	k5eAaImIp3nP
u	u	k7c2
vegetariánství	vegetariánství	k1gNnSc2
možnost	možnost	k1gFnSc4
konzumace	konzumace	k1gFnSc2
ryb	ryba	k1gFnPc2
<g/>
,	,	kIx,
případně	případně	k6eAd1
za	za	k7c4
zakázané	zakázaný	k2eAgNnSc4d1
<g />
.	.	kIx.
</s>
<s hack="1">
považují	považovat	k5eAaImIp3nP
pouze	pouze	k6eAd1
maso	maso	k1gNnSc1
savců	savec	k1gMnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
jiné	jiný	k2eAgFnPc1d1
definice	definice	k1gFnPc1
vylučují	vylučovat	k5eAaImIp3nP
ryby	ryba	k1gFnPc4
a	a	k8xC
jakékoliv	jakýkoliv	k3yIgNnSc4
zvířecí	zvířecí	k2eAgNnSc4d1
maso	maso	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
ostatních	ostatní	k2eAgInPc6d1
případech	případ	k1gInPc6
někteří	některý	k3yIgMnPc1
jedinci	jedinec	k1gMnPc1
označují	označovat	k5eAaImIp3nP
jako	jako	k8xC,k8xS
„	„	k?
<g/>
polovegetariáni	polovegetarián	k1gMnPc1
<g/>
“	“	k?
(	(	kIx(
<g/>
semivegetariáni	semivegetarián	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
jiných	jiný	k2eAgInPc6d1
případech	případ	k1gInPc6
se	se	k3xPyFc4
jedinci	jedinec	k1gMnPc1
mohou	moct	k5eAaImIp3nP
označovat	označovat	k5eAaImF
za	za	k7c4
„	„	k?
<g/>
flexitariány	flexitarián	k1gInPc4
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Takto	takto	k6eAd1
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
stravovat	stravovat	k5eAaImF
i	i	k9
ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
snížili	snížit	k5eAaPmAgMnP
spotřebu	spotřeba	k1gFnSc4
zvířecího	zvířecí	k2eAgNnSc2d1
masa	maso	k1gNnSc2
jako	jako	k8xC,k8xS
způsob	způsob	k1gInSc1
přechodu	přechod	k1gInSc2
ke	k	k7c3
kompletní	kompletní	k2eAgFnSc3d1
vegetariánské	vegetariánský	k2eAgFnSc3d1
stravě	strava	k1gFnSc3
nebo	nebo	k8xC
kvůli	kvůli	k7c3
zdraví	zdraví	k1gNnSc3
<g/>
,	,	kIx,
etice	etika	k1gFnSc3
<g/>
,	,	kIx,
z	z	k7c2
ekologických	ekologický	k2eAgInPc2d1
nebo	nebo	k8xC
jiných	jiný	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c4
polovegetariánskou	polovegetariánský	k2eAgFnSc4d1
stravu	strava	k1gFnSc4
patří	patřit	k5eAaImIp3nS
<g/>
:	:	kIx,
</s>
<s>
Makrobiotická	makrobiotický	k2eAgFnSc1d1
strava	strava	k1gFnSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
převážně	převážně	k6eAd1
z	z	k7c2
celozrnných	celozrnný	k2eAgFnPc2d1
obilovin	obilovina	k1gFnPc2
a	a	k8xC
bobovitých	bobovitý	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
(	(	kIx(
<g/>
luštěnin	luštěnina	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
může	moct	k5eAaImIp3nS
někdy	někdy	k6eAd1
obsahovat	obsahovat	k5eAaImF
ryby	ryba	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Rybo-vegetariánství	Rybo-vegetariánství	k1gNnSc1
(	(	kIx(
<g/>
pescetariánství	pescetariánství	k1gNnSc1
<g/>
)	)	kIx)
zahrnuje	zahrnovat	k5eAaImIp3nS
ryby	ryba	k1gFnPc4
a	a	k8xC
případně	případně	k6eAd1
jiné	jiný	k2eAgFnPc4d1
formy	forma	k1gFnPc4
tzv.	tzv.	kA
plodů	plod	k1gInPc2
moře	moře	k1gNnSc2
<g/>
;	;	kIx,
</s>
<s>
„	„	k?
<g/>
Kuře-rybovegetariánství	Kuře-rybovegetariánství	k1gNnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
pescopollovegetariánství	pescopollovegetariánství	k1gNnSc1
<g/>
,	,	kIx,
pollopescetariánství	pollopescetariánství	k1gNnSc1
<g/>
)	)	kIx)
zahrnuje	zahrnovat	k5eAaImIp3nS
drůbež	drůbež	k1gFnSc4
a	a	k8xC
ryby	ryba	k1gFnPc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
pouze	pouze	k6eAd1
„	„	k?
<g/>
bílé	bílý	k2eAgNnSc1d1
maso	maso	k1gNnSc1
<g/>
“	“	k?
<g/>
;	;	kIx,
</s>
<s>
Kuře-vegetariánství	Kuře-vegetariánství	k1gNnSc1
(	(	kIx(
<g/>
pollotariánství	pollotariánství	k1gNnSc1
<g/>
)	)	kIx)
zahrnuje	zahrnovat	k5eAaImIp3nS
kuřecí	kuřecí	k2eAgNnSc4d1
maso	maso	k1gNnSc4
a	a	k8xC
případně	případně	k6eAd1
i	i	k9
další	další	k2eAgFnSc4d1
drůbež	drůbež	k1gFnSc4
<g/>
;	;	kIx,
</s>
<s>
Polovegetariánství	Polovegetariánství	k1gNnSc1
je	být	k5eAaImIp3nS
zpochybňováno	zpochybňovat	k5eAaImNgNnS
vegetariánskými	vegetariánský	k2eAgFnPc7d1
skupinami	skupina	k1gFnPc7
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
Vegetariánská	vegetariánský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
vegetariánství	vegetariánství	k1gNnSc1
vylučuje	vylučovat	k5eAaImIp3nS
konzumaci	konzumace	k1gFnSc4
jakéhokoli	jakýkoli	k3yIgNnSc2
zvířecího	zvířecí	k2eAgNnSc2d1
masa	maso	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zdravotní	zdravotní	k2eAgInPc1d1
účinky	účinek	k1gInPc1
</s>
<s>
V	v	k7c6
průměru	průměr	k1gInSc6
vegetariáni	vegetarián	k1gMnPc1
konzumují	konzumovat	k5eAaBmIp3nP
nižší	nízký	k2eAgInSc4d2
podíl	podíl	k1gInSc4
kilojoulů	kilojoule	k1gInPc2
z	z	k7c2
tuku	tuk	k1gInSc2
(	(	kIx(
<g/>
zejména	zejména	k9
nasycených	nasycený	k2eAgFnPc2d1
mastných	mastný	k2eAgFnPc2d1
kyselin	kyselina	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
méně	málo	k6eAd2
celkových	celkový	k2eAgInPc2d1
kilojoulů	kilojoule	k1gInPc2
<g/>
,	,	kIx,
více	hodně	k6eAd2
vlákniny	vláknina	k1gFnSc2
<g/>
,	,	kIx,
draslíku	draslík	k1gInSc2
a	a	k8xC
vitamínu	vitamín	k1gInSc2
C	C	kA
než	než	k8xS
nevegetariány	nevegetarián	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vegetariáni	vegetarián	k1gMnPc1
mají	mít	k5eAaImIp3nP
obecně	obecně	k6eAd1
nižší	nízký	k2eAgInSc4d2
index	index	k1gInSc4
tělesné	tělesný	k2eAgFnSc2d1
hmotnosti	hmotnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
charakteristiky	charakteristika	k1gFnPc1
a	a	k8xC
jiné	jiný	k2eAgInPc1d1
faktory	faktor	k1gInPc1
životního	životní	k2eAgInSc2d1
stylu	styl	k1gInSc2
související	související	k2eAgFnSc1d1
s	s	k7c7
vegetariánskou	vegetariánský	k2eAgFnSc7d1
stravy	strava	k1gFnSc2
mohou	moct	k5eAaImIp3nP
přispět	přispět	k5eAaPmF
k	k	k7c3
pozitivním	pozitivní	k2eAgInPc3d1
výsledkům	výsledek	k1gInPc3
v	v	k7c6
oblasti	oblast	k1gFnSc6
zdraví	zdraví	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
byly	být	k5eAaImAgFnP
identifikovány	identifikovat	k5eAaBmNgInP
mezi	mezi	k7c7
vegetariány	vegetarián	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
Dietary	Dietara	k1gFnPc1
Guidelines	Guidelinesa	k1gFnPc2
for	forum	k1gNnPc2
Americans	Americansa	k1gFnPc2
<g/>
,	,	kIx,
2010	#num#	k4
–	–	k?
A	a	k8xC
report	report	k1gInSc1
issued	issued	k1gInSc4
by	by	k9
the	the	k?
U.	U.	kA
<g/>
S.	S.	kA
Department	department	k1gInSc1
of	of	k?
Agriculture	Agricultur	k1gMnSc5
and	and	k?
the	the	k?
U.	U.	kA
<g/>
S.	S.	kA
Department	department	k1gInSc1
of	of	k?
Health	Health	k1gMnSc1
and	and	k?
Human	Human	k1gMnSc1
Services	Services	k1gMnSc1
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odborníci	odborník	k1gMnPc1
a	a	k8xC
lékaři	lékař	k1gMnPc1
se	se	k3xPyFc4
zcela	zcela	k6eAd1
neshodují	shodovat	k5eNaImIp3nP
na	na	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
jaké	jaký	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
jsou	být	k5eAaImIp3nP
účinky	účinek	k1gInPc7
vegetariánství	vegetariánství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnPc1
studie	studie	k1gFnPc1
ukazují	ukazovat	k5eAaImIp3nP
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
že	že	k8xS
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
všežravci	všežravec	k1gMnPc7
[	[	kIx(
<g/>
sic	sic	k6eAd1
<g/>
]	]	kIx)
má	mít	k5eAaImIp3nS
vegetariánská	vegetariánský	k2eAgFnSc1d1
populace	populace	k1gFnSc1
celkově	celkově	k6eAd1
nižší	nízký	k2eAgFnSc4d2
celkovou	celkový	k2eAgFnSc4d1
úmrtnost	úmrtnost	k1gFnSc4
a	a	k8xC
zejména	zejména	k9
těží	těžet	k5eAaImIp3nS
ze	z	k7c2
sníženého	snížený	k2eAgInSc2d1
výskytu	výskyt	k1gInSc2
mnoha	mnoho	k4c2
nepřenosných	přenosný	k2eNgNnPc2d1
onemocnění	onemocnění	k1gNnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
srdečních	srdeční	k2eAgFnPc2d1
chorob	choroba	k1gFnPc2
<g/>
,	,	kIx,
cukrovky	cukrovka	k1gFnSc2
2	#num#	k4
<g/>
.	.	kIx.
typu	typ	k1gInSc2
a	a	k8xC
cévně	cévně	k6eAd1
mozkových	mozkový	k2eAgNnPc2d1
onemocnění	onemocnění	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vegetariánská	vegetariánský	k2eAgFnSc1d1
strava	strava	k1gFnSc1
podle	podle	k7c2
této	tento	k3xDgFnSc2
publikace	publikace	k1gFnSc2
snižuje	snižovat	k5eAaImIp3nS
riziko	riziko	k1gNnSc1
rakoviny	rakovina	k1gFnSc2
<g/>
,	,	kIx,
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
rakoviny	rakovina	k1gFnSc2
prsu	prs	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1
studie	studie	k1gFnSc1
však	však	k9
ukázala	ukázat	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
dospělí	dospělý	k2eAgMnPc1d1
vegetariáni	vegetarián	k1gMnPc1
v	v	k7c6
Rakousku	Rakousko	k1gNnSc6
mají	mít	k5eAaImIp3nP
obecně	obecně	k6eAd1
horší	zlý	k2eAgNnSc4d2
zdraví	zdraví	k1gNnSc4
(	(	kIx(
<g/>
ve	v	k7c6
smyslu	smysl	k1gInSc6
rakoviny	rakovina	k1gFnSc2
<g/>
,	,	kIx,
alergií	alergie	k1gFnPc2
a	a	k8xC
duševních	duševní	k2eAgFnPc2d1
poruch	porucha	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
také	také	k9
potřebují	potřebovat	k5eAaImIp3nP
více	hodně	k6eAd2
zdravotní	zdravotní	k2eAgFnSc2d1
péče	péče	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
studovaném	studovaný	k2eAgInSc6d1
vzorku	vzorek	k1gInSc6
rakouské	rakouský	k2eAgFnSc2d1
populace	populace	k1gFnSc2
měli	mít	k5eAaImAgMnP
vegetariáni	vegetarián	k1gMnPc1
rovněž	rovněž	k9
horší	horšit	k5eAaImIp3nP
sociální	sociální	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
a	a	k8xC
obecně	obecně	k6eAd1
kvalitu	kvalita	k1gFnSc4
života	život	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
Takovéto	takovýto	k3xDgInPc4
dopady	dopad	k1gInPc4
má	mít	k5eAaImIp3nS
špatná	špatný	k2eAgFnSc1d1
vegetariánská	vegetariánský	k2eAgFnSc1d1
strava	strava	k1gFnSc1
zapříčiněná	zapříčiněný	k2eAgFnSc1d1
jednotvárnou	jednotvárný	k2eAgFnSc7d1
stravou	strava	k1gFnSc7
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
vegetariáni	vegetarián	k1gMnPc1
jí	on	k3xPp3gFnSc3
převážně	převážně	k6eAd1
pouze	pouze	k6eAd1
"	"	kIx"
<g/>
přílohy	příloha	k1gFnPc1
<g/>
"	"	kIx"
a	a	k8xC
maso	maso	k1gNnSc1
nenahradí	nahradit	k5eNaPmIp3nS
pestrou	pestrý	k2eAgFnSc7d1
stravou	strava	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takovéto	takovýto	k3xDgNnSc1
nezodpovědné	zodpovědný	k2eNgNnSc1d1
chování	chování	k1gNnSc1
bývá	bývat	k5eAaImIp3nS
způsobeno	způsobit	k5eAaPmNgNnS
nízkou	nízký	k2eAgFnSc7d1
informovaností	informovanost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Špatná	špatný	k2eAgFnSc1d1
vegetariánská	vegetariánský	k2eAgFnSc1d1
strava	strava	k1gFnSc1
může	moct	k5eAaImIp3nS
vést	vést	k5eAaImF
k	k	k7c3
hyperhomocysteinémii	hyperhomocysteinémie	k1gFnSc3
a	a	k8xC
poruchám	porucha	k1gFnPc3
krevních	krevní	k2eAgFnPc2d1
destiček	destička	k1gFnPc2
<g/>
;	;	kIx,
toto	tento	k3xDgNnSc1
riziko	riziko	k1gNnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
kompenzováno	kompenzovat	k5eAaBmNgNnS
dostatečnou	dostatečný	k2eAgFnSc7d1
konzumací	konzumace	k1gFnSc7
potravin	potravina	k1gFnPc2
s	s	k7c7
vyšším	vysoký	k2eAgInSc7d2
obsahem	obsah	k1gInSc7
vitaminu	vitamin	k1gInSc2
B12	B12	k1gFnSc2
a	a	k8xC
polynenasycených	polynenasycený	k2eAgFnPc2d1
mastných	mastný	k2eAgFnPc2d1
kyselin	kyselina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Akademie	akademie	k1gFnSc1
výživy	výživa	k1gFnSc2
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
a	a	k8xC
Asociace	asociace	k1gFnSc1
kanadských	kanadský	k2eAgMnPc2d1
dietologů	dietolog	k1gMnPc2
uvádějí	uvádět	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
vhodně	vhodně	k6eAd1
rozvržená	rozvržený	k2eAgFnSc1d1
vegetariánská	vegetariánský	k2eAgFnSc1d1
strava	strava	k1gFnSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
všech	všecek	k3xTgFnPc6
fázích	fáze	k1gFnPc6
života	život	k1gInSc2
člověka	člověk	k1gMnSc4
„	„	k?
<g/>
zdravá	zdravá	k1gFnSc1
<g/>
,	,	kIx,
nutričně	nutričně	k6eAd1
vyvážená	vyvážený	k2eAgFnSc1d1
a	a	k8xC
poskytuje	poskytovat	k5eAaImIp3nS
zdravotní	zdravotní	k2eAgInSc4d1
přínos	přínos	k1gInSc4
v	v	k7c6
prevenci	prevence	k1gFnSc6
a	a	k8xC
léčbě	léčba	k1gFnSc6
některých	některý	k3yIgFnPc2
chorob	choroba	k1gFnPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
]	]	kIx)
Rozsáhlé	rozsáhlý	k2eAgFnPc1d1
studie	studie	k1gFnPc1
prokázaly	prokázat	k5eAaPmAgFnP
<g/>
,	,	kIx,
že	že	k8xS
úmrtnost	úmrtnost	k1gFnSc1
na	na	k7c4
ischemickou	ischemický	k2eAgFnSc4d1
chorobu	choroba	k1gFnSc4
srdeční	srdeční	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
u	u	k7c2
vegetariánských	vegetariánský	k2eAgMnPc2d1
mužů	muž	k1gMnPc2
o	o	k7c4
30	#num#	k4
%	%	kIx~
a	a	k8xC
u	u	k7c2
vegetariánských	vegetariánský	k2eAgFnPc2d1
žen	žena	k1gFnPc2
o	o	k7c4
20	#num#	k4
%	%	kIx~
nižší	nízký	k2eAgFnSc1d2
<g/>
,	,	kIx,
než	než	k8xS
u	u	k7c2
nevegetariánů	nevegetarián	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
Vegetariánská	vegetariánský	k2eAgFnSc1d1
strava	strava	k1gFnSc1
znamená	znamenat	k5eAaImIp3nS
nižší	nízký	k2eAgFnSc1d2
<g />
.	.	kIx.
</s>
<s hack="1">
množství	množství	k1gNnSc1
nasycených	nasycený	k2eAgInPc2d1
tuků	tuk	k1gInPc2
<g/>
,	,	kIx,
cholesterolu	cholesterol	k1gInSc2
a	a	k8xC
živočišných	živočišný	k2eAgFnPc2d1
bílkovin	bílkovina	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
vyšší	vysoký	k2eAgInSc1d2
obsah	obsah	k1gInSc1
sacharidů	sacharid	k1gInPc2
<g/>
,	,	kIx,
vlákniny	vláknina	k1gFnSc2
<g/>
,	,	kIx,
hořčíku	hořčík	k1gInSc2
<g/>
,	,	kIx,
draslíku	draslík	k1gInSc2
<g/>
,	,	kIx,
kyseliny	kyselina	k1gFnSc2
listové	listový	k2eAgFnSc2d1
a	a	k8xC
antioxidantů	antioxidant	k1gInPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
vitamin	vitamin	k1gInSc1
C	C	kA
a	a	k8xC
E	E	kA
<g/>
,	,	kIx,
a	a	k8xC
fytochemikálií	fytochemikálie	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
<g/>
Vegetariánská	vegetariánský	k2eAgFnSc1d1
strava	strava	k1gFnSc1
může	moct	k5eAaImIp3nS
splnit	splnit	k5eAaPmF
požadavky	požadavek	k1gInPc4
pro	pro	k7c4
léčbu	léčba	k1gFnSc4
cukrovky	cukrovka	k1gFnSc2
a	a	k8xC
některé	některý	k3yIgInPc1
výzkumy	výzkum	k1gInPc1
naznačují	naznačovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
strava	strava	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
více	hodně	k6eAd2
založena	založit	k5eAaPmNgFnS
na	na	k7c6
rostlinném	rostlinný	k2eAgInSc6d1
základu	základ	k1gInSc6
<g/>
,	,	kIx,
snižuje	snižovat	k5eAaImIp3nS
riziko	riziko	k1gNnSc1
vzniku	vznik	k1gInSc2
cukrovky	cukrovka	k1gFnSc2
2	#num#	k4
<g/>
.	.	kIx.
typu	typ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Míra	Míra	k1gFnSc1
výskytu	výskyt	k1gInSc2
cukrovky	cukrovka	k1gFnSc2
založená	založený	k2eAgFnSc1d1
na	na	k7c6
vlastních	vlastní	k2eAgInPc6d1
hlášeních	hlášení	k1gNnPc6
Adventistů	adventista	k1gMnPc2
sedmého	sedmý	k4xOgInSc2
dne	den	k1gInSc2
(	(	kIx(
<g/>
ASD	ASD	kA
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
méně	málo	k6eAd2
než	než	k8xS
poloviční	poloviční	k2eAgInPc1d1
oproti	oproti	k7c3
běžné	běžný	k2eAgFnSc3d1
populaci	populace	k1gFnSc3
<g/>
,	,	kIx,
a	a	k8xC
mezi	mezi	k7c7
samotnými	samotný	k2eAgNnPc7d1
ASD	ASD	kA
měli	mít	k5eAaImAgMnP
vegetariáni	vegetarián	k1gMnPc1
nižší	nízký	k2eAgFnSc2d2
výskyt	výskyt	k1gInSc1
cukrovky	cukrovka	k1gFnPc1
než	než	k8xS
nevegetariáni	nevegetarián	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
možným	možný	k2eAgNnPc3d1
vysvětlením	vysvětlení	k1gNnPc3
ochranného	ochranný	k2eAgInSc2d1
účinku	účinek	k1gInSc2
vegetariánské	vegetariánský	k2eAgFnSc2d1
stravy	strava	k1gFnSc2
patří	patřit	k5eAaImIp3nS
nižší	nízký	k2eAgInSc1d2
index	index	k1gInSc1
tělesné	tělesný	k2eAgFnSc2d1
hmotnosti	hmotnost	k1gFnSc2
vegetariánů	vegetarián	k1gMnPc2
a	a	k8xC
vyšší	vysoký	k2eAgInSc4d2
příjem	příjem	k1gInSc4
vlákniny	vláknina	k1gFnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
obě	dva	k4xCgNnPc1
zlepšují	zlepšovat	k5eAaImIp3nP
citlivost	citlivost	k1gFnSc4
na	na	k7c4
inzulín	inzulín	k1gInSc4
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vztah	vztah	k1gInSc1
mezi	mezi	k7c7
vegetariánskou	vegetariánský	k2eAgFnSc7d1
stravou	strava	k1gFnSc7
a	a	k8xC
zdravím	zdraví	k1gNnSc7
kostí	kost	k1gFnPc2
zůstává	zůstávat	k5eAaImIp3nS
nejasný	jasný	k2eNgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
některých	některý	k3yIgNnPc2
studií	studio	k1gNnPc2
vegetariánský	vegetariánský	k2eAgInSc1d1
životní	životní	k2eAgInSc4d1
styl	styl	k1gInSc4
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
spojen	spojit	k5eAaPmNgInS
s	s	k7c7
nedostatkem	nedostatek	k1gInSc7
vitaminu	vitamin	k1gInSc2
B12	B12	k1gMnPc2
a	a	k8xC
nízkou	nízký	k2eAgFnSc7d1
hustotou	hustota	k1gFnSc7
minerálů	minerál	k1gInPc2
v	v	k7c6
kostech	kost	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
Avšak	avšak	k8xC
studie	studie	k1gFnPc1
vegetariánské	vegetariánský	k2eAgFnPc1d1
a	a	k8xC
nevegetariánské	vegetariánský	k2eNgFnPc1d1
dospělé	dospělý	k2eAgFnPc1d1
populace	populace	k1gFnPc1
na	na	k7c6
Tchaj-wanu	Tchaj-wan	k1gInSc6
nenalezla	nalézt	k5eNaBmAgFnS,k5eNaPmAgFnS
mezi	mezi	k7c7
oběma	dva	k4xCgFnPc7
skupinami	skupina	k1gFnPc7
žádný	žádný	k3yNgInSc4
významný	významný	k2eAgInSc4d1
rozdíl	rozdíl	k1gInSc4
v	v	k7c6
hustotě	hustota	k1gFnSc6
minerálů	minerál	k1gInPc2
v	v	k7c6
kostech	kost	k1gFnPc6
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
Další	další	k2eAgFnPc1d1
studie	studie	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
zkoumaly	zkoumat	k5eAaImAgFnP
negativní	negativní	k2eAgInPc4d1
dopady	dopad	k1gInPc4
živočišných	živočišný	k2eAgFnPc2d1
bílkovin	bílkovina	k1gFnPc2
na	na	k7c4
zdraví	zdraví	k1gNnSc4
kostí	kost	k1gFnPc2
<g/>
,	,	kIx,
naznačují	naznačovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
vegetariáni	vegetarián	k1gMnPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
méně	málo	k6eAd2
náchylní	náchylný	k2eAgMnPc1d1
k	k	k7c3
osteoporóze	osteoporóza	k1gFnSc3
než	než	k8xS
všežravci	všežravec	k1gMnSc3
<g/>
,	,	kIx,
vegetariánské	vegetariánský	k2eAgFnPc1d1
osoby	osoba	k1gFnPc1
měly	mít	k5eAaImAgFnP
větší	veliký	k2eAgFnSc4d2
hustotu	hustota	k1gFnSc4
minerálů	minerál	k1gInPc2
v	v	k7c6
kostech	kost	k1gFnPc6
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
zvýšenou	zvýšený	k2eAgFnSc4d1
tvorbu	tvorba	k1gFnSc4
kostí	kost	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Projekt	projekt	k1gInSc1
„	„	k?
<g/>
China-Cornell-Oxford	China-Cornell-Oxford	k1gInSc1
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
dvacetiletá	dvacetiletý	k2eAgFnSc1d1
studie	studie	k1gFnSc1
prováděná	prováděný	k2eAgFnSc1d1
společně	společně	k6eAd1
Cornellovou	Cornellův	k2eAgFnSc7d1
univerzitou	univerzita	k1gFnSc7
<g/>
,	,	kIx,
Oxfordskou	oxfordský	k2eAgFnSc7d1
univerzitou	univerzita	k1gFnSc7
a	a	k8xC
čínskou	čínský	k2eAgFnSc7d1
vládou	vláda	k1gFnSc7
prokázal	prokázat	k5eAaPmAgInS
vztah	vztah	k1gInSc1
mezi	mezi	k7c7
konzumací	konzumace	k1gFnSc7
živočišných	živočišný	k2eAgInPc2d1
produktů	produkt	k1gInPc2
a	a	k8xC
různými	různý	k2eAgNnPc7d1
chronickými	chronický	k2eAgNnPc7d1
onemocněními	onemocnění	k1gNnPc7
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
například	například	k6eAd1
ischemická	ischemický	k2eAgFnSc1d1
choroba	choroba	k1gFnSc1
srdeční	srdeční	k2eAgFnSc1d1
<g/>
,	,	kIx,
cukrovka	cukrovka	k1gFnSc1
a	a	k8xC
rakovina	rakovina	k1gFnSc1
prsu	prs	k1gInSc2
<g/>
,	,	kIx,
prostaty	prostata	k1gFnSc2
a	a	k8xC
střev	střevo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výživa	výživa	k1gFnSc1
</s>
<s>
Stánek	stánek	k1gInSc1
s	s	k7c7
ovocem	ovoce	k1gNnSc7
v	v	k7c6
Barceloně	Barcelona	k1gFnSc6
</s>
<s>
Západní	západní	k2eAgFnSc1d1
vegetariánská	vegetariánský	k2eAgFnSc1d1
strava	strava	k1gFnSc1
je	být	k5eAaImIp3nS
typická	typický	k2eAgNnPc4d1
vysokým	vysoký	k2eAgInSc7d1
obsahem	obsah	k1gInSc7
karotenoidů	karotenoid	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
relativně	relativně	k6eAd1
nízkým	nízký	k2eAgInSc7d1
obsahem	obsah	k1gInSc7
omega-	omega-	k?
<g/>
3	#num#	k4
mastných	mastný	k2eAgFnPc2d1
kyselin	kyselina	k1gFnPc2
a	a	k8xC
vitaminu	vitamin	k1gInSc2
B	B	kA
<g/>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vegani	Vegan	k1gMnPc1
mohou	moct	k5eAaImIp3nP
mít	mít	k5eAaImF
zvláště	zvláště	k6eAd1
nízký	nízký	k2eAgInSc4d1
příjem	příjem	k1gInSc4
vitamínu	vitamín	k1gInSc2
B	B	kA
a	a	k8xC
vápníku	vápník	k1gInSc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
nemají	mít	k5eNaImIp3nP
ve	v	k7c6
stravě	strava	k1gFnSc6
dostatek	dostatek	k1gInSc1
položek	položka	k1gFnPc2
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
brukev	brukev	k1gFnSc1
zelná	zelný	k2eAgFnSc1d1
<g/>
,	,	kIx,
listová	listový	k2eAgFnSc1d1
zelenina	zelenina	k1gFnSc1
<g/>
,	,	kIx,
tempeh	tempeh	k1gInSc1
a	a	k8xC
tofu	tofu	k1gNnSc1
(	(	kIx(
<g/>
sójové	sójový	k2eAgNnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
Vysoké	vysoký	k2eAgFnSc2d1
hladiny	hladina	k1gFnSc2
vlákniny	vláknina	k1gFnSc2
<g/>
,	,	kIx,
kyseliny	kyselina	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
listové	listový	k2eAgFnPc4d1
<g/>
,	,	kIx,
vitaminů	vitamin	k1gInPc2
C	C	kA
a	a	k8xC
E	E	kA
<g/>
,	,	kIx,
a	a	k8xC
hořčíku	hořčík	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
nízká	nízký	k2eAgFnSc1d1
spotřeba	spotřeba	k1gFnSc1
nasycených	nasycený	k2eAgInPc2d1
tuků	tuk	k1gInPc2
jsou	být	k5eAaImIp3nP
považovány	považován	k2eAgInPc1d1
za	za	k7c4
prospěšné	prospěšný	k2eAgInPc4d1
aspekty	aspekt	k1gInPc4
vegetariánské	vegetariánský	k2eAgFnSc2d1
stravy	strava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
Dobře	dobře	k6eAd1
plánovaná	plánovaný	k2eAgFnSc1d1
vegetariánská	vegetariánský	k2eAgFnSc1d1
strava	strava	k1gFnSc1
poskytuje	poskytovat	k5eAaImIp3nS
ve	v	k7c6
všech	všecek	k3xTgFnPc6
fázích	fáze	k1gFnPc6
života	život	k1gInSc2
všechny	všechen	k3xTgFnPc1
živiny	živina	k1gFnPc1
v	v	k7c6
potravě	potrava	k1gFnSc6
na	na	k7c6
stejné	stejný	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
jako	jako	k9
mají	mít	k5eAaImIp3nP
jedlíci	jedlík	k1gMnPc1
masa	maso	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bílkoviny	bílkovina	k1gFnPc1
</s>
<s>
Příjem	příjem	k1gInSc1
bílkovin	bílkovina	k1gFnPc2
je	být	k5eAaImIp3nS
ve	v	k7c6
vegetariánské	vegetariánský	k2eAgFnSc6d1
stravě	strava	k1gFnSc6
nižší	nízký	k2eAgFnSc6d2
než	než	k8xS
při	při	k7c6
konzumaci	konzumace	k1gFnSc6
masa	maso	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
pro	pro	k7c4
většinu	většina	k1gFnSc4
lidí	člověk	k1gMnPc2
plní	plnit	k5eAaImIp3nS
limit	limit	k1gInSc1
denní	denní	k2eAgFnSc2d1
potřeby	potřeba	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
Studie	studie	k1gFnSc1
na	na	k7c6
Harvardově	Harvardův	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
,	,	kIx,
jakož	jakož	k8xC
i	i	k9
další	další	k2eAgFnPc1d1
klinické	klinický	k2eAgFnPc1d1
studie	studie	k1gFnPc1
provedené	provedený	k2eAgFnPc1d1
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
amerických	americký	k2eAgInPc6d1
<g/>
,	,	kIx,
Velké	velký	k2eAgFnSc6d1
Británii	Británie	k1gFnSc6
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Kanadě	Kanada	k1gFnSc3
<g/>
,	,	kIx,
Austrálii	Austrálie	k1gFnSc6
<g/>
,	,	kIx,
na	na	k7c6
Novém	nový	k2eAgInSc6d1
Zélandu	Zéland	k1gInSc6
a	a	k8xC
v	v	k7c6
různých	různý	k2eAgFnPc6d1
evropských	evropský	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
potvrdily	potvrdit	k5eAaPmAgFnP
<g/>
,	,	kIx,
že	že	k8xS
vegetariánská	vegetariánský	k2eAgFnSc1d1
strava	strava	k1gFnSc1
poskytuje	poskytovat	k5eAaImIp3nS
dostatečný	dostatečný	k2eAgInSc4d1
příjem	příjem	k1gInSc4
bílkovin	bílkovina	k1gFnPc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
a	a	k8xC
jí	jíst	k5eAaImIp3nS
se	se	k3xPyFc4
celá	celý	k2eAgFnSc1d1
paleta	paleta	k1gFnSc1
nejrůznějších	různý	k2eAgInPc2d3
rostlinných	rostlinný	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
Velkými	velký	k2eAgInPc7d1
zdroji	zdroj	k1gInPc7
bílkovin	bílkovina	k1gFnPc2
pro	pro	k7c4
vegetariány	vegetarián	k1gMnPc4
jsou	být	k5eAaImIp3nP
dýňová	dýňový	k2eAgNnPc4d1
semínka	semínko	k1gNnPc4
<g/>
,	,	kIx,
arašídové	arašídový	k2eAgNnSc4d1
máslo	máslo	k1gNnSc4
<g/>
,	,	kIx,
konopné	konopný	k2eAgNnSc4d1
semeno	semeno	k1gNnSc4
<g/>
,	,	kIx,
mandle	mandle	k1gFnPc4
<g/>
,	,	kIx,
pistácie	pistácie	k1gFnPc4
<g/>
,	,	kIx,
lněná	lněný	k2eAgNnPc4d1
semínka	semínko	k1gNnPc4
<g/>
,	,	kIx,
tofu	tofu	k1gNnPc4
<g/>
,	,	kIx,
ovesné	ovesný	k2eAgFnPc4d1
vločky	vločka	k1gFnPc4
<g/>
,	,	kIx,
sojové	sojový	k2eAgInPc4d1
boby	bob	k1gInPc4
a	a	k8xC
vlašské	vlašský	k2eAgInPc4d1
ořechy	ořech	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bílkoviny	bílkovina	k1gFnPc1
jsou	být	k5eAaImIp3nP
složeny	složit	k5eAaPmNgFnP
z	z	k7c2
aminokyselin	aminokyselina	k1gFnPc2
a	a	k8xC
obecný	obecný	k2eAgInSc4d1
problém	problém	k1gInSc4
s	s	k7c7
bílkovinami	bílkovina	k1gFnPc7
získaných	získaný	k2eAgFnPc2d1
z	z	k7c2
rostlinných	rostlinný	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
je	být	k5eAaImIp3nS
adekvátní	adekvátní	k2eAgInSc1d1
příjem	příjem	k1gInSc1
esenciálních	esenciální	k2eAgFnPc2d1
aminokyselin	aminokyselina	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
nemohou	moct	k5eNaImIp3nP
být	být	k5eAaImF
syntetizovány	syntetizován	k2eAgInPc1d1
v	v	k7c6
lidském	lidský	k2eAgNnSc6d1
těle	tělo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
mléčné	mléčný	k2eAgInPc4d1
a	a	k8xC
vaječné	vaječný	k2eAgInPc4d1
výrobky	výrobek	k1gInPc4
poskytují	poskytovat	k5eAaImIp3nP
komplexní	komplexní	k2eAgInPc1d1
zdroje	zdroj	k1gInPc1
bílkovin	bílkovina	k1gFnPc2
pro	pro	k7c4
ovo-lakto	ovo-lakto	k1gNnSc4
vegetariány	vegetarián	k1gMnPc4
<g/>
,	,	kIx,
významné	významný	k2eAgNnSc4d1
množství	množství	k1gNnSc4
všech	všecek	k3xTgInPc2
osmi	osm	k4xCc2
druhů	druh	k1gInPc2
esenciálních	esenciální	k2eAgFnPc2d1
aminokyselin	aminokyselina	k1gFnPc2
má	mít	k5eAaImIp3nS
jen	jen	k9
několik	několik	k4yIc4
zdrojů	zdroj	k1gInPc2
rostlin	rostlina	k1gFnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
fazolí	fazole	k1gFnPc2
<g/>
,	,	kIx,
sóji	sója	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
konopných	konopný	k2eAgNnPc2d1
semen	semeno	k1gNnPc2
<g/>
,	,	kIx,
Chia	Chi	k1gInSc2
semínek	semínko	k1gNnPc2
<g/>
[	[	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
58	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
laskavce	laskavec	k1gInPc1
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
pohanky	pohanka	k1gFnSc2
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
dýňových	dýňový	k2eAgNnPc2d1
semen	semeno	k1gNnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
spiruliny	spirulin	k2eAgInPc1d1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
pistácií	pistácie	k1gFnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
merlíku	merlík	k1gInSc2
chilského	chilský	k2eAgInSc2d1
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nicméně	nicméně	k8xC
esenciální	esenciální	k2eAgFnPc1d1
aminokyseliny	aminokyselina	k1gFnPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
také	také	k9
získány	získán	k2eAgFnPc1d1
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jídelníček	jídelníček	k1gInSc1
obohatí	obohatit	k5eAaPmIp3nS
o	o	k7c4
celou	celý	k2eAgFnSc4d1
řadu	řada	k1gFnSc4
rostlinných	rostlinný	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
v	v	k7c6
kombinaci	kombinace	k1gFnSc6
poskytují	poskytovat	k5eAaImIp3nP
všech	všecek	k3xTgFnPc2
osm	osm	k4xCc1
esenciálních	esenciální	k2eAgFnPc2d1
aminokyselin	aminokyselina	k1gFnPc2
(	(	kIx(
<g/>
např.	např.	kA
hnědá	hnědý	k2eAgFnSc1d1
rýže	rýže	k1gFnSc1
a	a	k8xC
fazole	fazole	k1gFnSc1
nebo	nebo	k8xC
hummus	hummus	k1gInSc1
a	a	k8xC
celozrnná	celozrnný	k2eAgFnSc1d1
pita	pit	k2eAgFnSc1d1
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
kombinovat	kombinovat	k5eAaImF
bílkoviny	bílkovina	k1gFnPc4
ve	v	k7c6
stejném	stejný	k2eAgNnSc6d1
jídle	jídlo	k1gNnSc6
není	být	k5eNaImIp3nS
nutné	nutný	k2eAgNnSc1d1
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studie	studie	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1994	#num#	k4
zjistila	zjistit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
pestrý	pestrý	k2eAgInSc1d1
příjem	příjem	k1gInSc1
těchto	tento	k3xDgInPc2
zdrojů	zdroj	k1gInPc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
dostačující	dostačující	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Železo	železo	k1gNnSc1
</s>
<s>
Vegetariánská	vegetariánský	k2eAgFnSc1d1
strava	strava	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
podobné	podobný	k2eAgNnSc4d1
množství	množství	k1gNnSc4
železa	železo	k1gNnSc2
jako	jako	k8xS,k8xC
nevegetariánská	vegetariánský	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
železo	železo	k1gNnSc1
z	z	k7c2
vegetariánských	vegetariánský	k2eAgNnPc2d1
jídel	jídlo	k1gNnPc2
má	mít	k5eAaImIp3nS
nižší	nízký	k2eAgFnSc4d2
biologickou	biologický	k2eAgFnSc4d1
dostupnost	dostupnost	k1gFnSc4
než	než	k8xS
železo	železo	k1gNnSc4
z	z	k7c2
masa	maso	k1gNnSc2
a	a	k8xC
jeho	jeho	k3xOp3gNnSc4
vstřebávání	vstřebávání	k1gNnSc4
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
brzděno	brzděn	k2eAgNnSc1d1
dalšími	další	k2eAgFnPc7d1
potravinovými	potravinový	k2eAgFnPc7d1
složkami	složka	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
Vegetariánské	vegetariánský	k2eAgFnSc2d1
výzkumné	výzkumný	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
je	být	k5eAaImIp3nS
konzumace	konzumace	k1gFnSc1
potravin	potravina	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
obsahují	obsahovat	k5eAaImIp3nP
vitamin	vitamin	k1gInSc4
C	C	kA
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
například	například	k6eAd1
citrusových	citrusový	k2eAgInPc2d1
plodů	plod	k1gInPc2
<g />
.	.	kIx.
</s>
<s hack="1">
nebo	nebo	k8xC
šťáv	šťáva	k1gFnPc2
<g/>
,	,	kIx,
rajčat	rajče	k1gNnPc2
nebo	nebo	k8xC
brokolice	brokolice	k1gFnSc2
<g/>
,	,	kIx,
dobrým	dobrý	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
zvýšit	zvýšit	k5eAaPmF
množství	množství	k1gNnSc4
železa	železo	k1gNnSc2
vstřebávaného	vstřebávaný	k2eAgNnSc2d1
při	při	k7c6
jídle	jídlo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
Vegetariánská	vegetariánský	k2eAgFnSc1d1
strava	strava	k1gFnSc1
bohatá	bohatý	k2eAgFnSc1d1
na	na	k7c4
železo	železo	k1gNnSc4
zahrnuje	zahrnovat	k5eAaImIp3nS
černé	černý	k2eAgFnSc2d1
fazole	fazole	k1gFnSc2
<g/>
,	,	kIx,
kešu	kešus	k1gInSc2
<g/>
,	,	kIx,
konopná	konopný	k2eAgNnPc4d1
semena	semeno	k1gNnPc4
<g/>
,	,	kIx,
fazole	fazole	k1gFnPc4
<g/>
,	,	kIx,
brokolici	brokolice	k1gFnSc4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
čočku	čočka	k1gFnSc4
<g/>
,	,	kIx,
ovesné	ovesný	k2eAgFnPc4d1
vločky	vločka	k1gFnPc4
<g/>
,	,	kIx,
rozinky	rozinka	k1gFnPc4
<g/>
,	,	kIx,
špenát	špenát	k1gInSc1
<g/>
,	,	kIx,
hlávkové	hlávkový	k2eAgNnSc1d1
zelí	zelí	k1gNnSc1
<g/>
,	,	kIx,
salát	salát	k1gInSc1
<g/>
,	,	kIx,
hrách	hrách	k1gInSc1
černé	černá	k1gFnSc6
oči	oko	k1gNnPc4
<g/>
,	,	kIx,
sóju	sója	k1gFnSc4
<g/>
,	,	kIx,
řada	řada	k1gFnSc1
snídaňových	snídaňový	k2eAgFnPc2d1
obilovin	obilovina	k1gFnPc2
<g/>
,	,	kIx,
slunečnicová	slunečnicový	k2eAgNnPc4d1
semena	semeno	k1gNnPc4
<g/>
,	,	kIx,
cizrnu	cizrna	k1gFnSc4
<g/>
,	,	kIx,
rajčatovou	rajčatový	k2eAgFnSc4d1
šťávu	šťáva	k1gFnSc4
<g/>
,	,	kIx,
tempeh	tempeh	k1gInSc4
<g/>
,	,	kIx,
melasu	melasa	k1gFnSc4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
tymián	tymián	k1gInSc1
a	a	k8xC
celozrnný	celozrnný	k2eAgInSc1d1
chléb	chléb	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
Související	související	k2eAgFnSc1d1
veganská	veganský	k2eAgFnSc1d1
strava	strava	k1gFnSc1
může	moct	k5eAaImIp3nS
mít	mít	k5eAaImF
často	často	k6eAd1
vyšší	vysoký	k2eAgInSc4d2
obsah	obsah	k1gInSc4
železa	železo	k1gNnSc2
než	než	k8xS
vegetariánská	vegetariánský	k2eAgFnSc1d1
strava	strava	k1gFnSc1
<g/>
,	,	kIx,
protože	protože	k8xS
mléčné	mléčný	k2eAgInPc1d1
výrobky	výrobek	k1gInPc1
mají	mít	k5eAaImIp3nP
nízký	nízký	k2eAgInSc4d1
obsah	obsah	k1gInSc4
železa	železo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
Ukládání	ukládání	k1gNnSc1
železa	železo	k1gNnSc2
má	mít	k5eAaImIp3nS
často	často	k6eAd1
tendenci	tendence	k1gFnSc4
být	být	k5eAaImF
nižší	nízký	k2eAgFnSc1d2
u	u	k7c2
vegetariánů	vegetarián	k1gMnPc2
než	než	k8xS
nevegetariánů	nevegetarián	k1gMnPc2
a	a	k8xC
několik	několik	k4yIc4
malých	malý	k2eAgFnPc2d1
studií	studie	k1gFnPc2
hlásí	hlásit	k5eAaImIp3nP
velmi	velmi	k6eAd1
vysokou	vysoký	k2eAgFnSc4d1
míru	míra	k1gFnSc4
nedostatku	nedostatek	k1gInSc2
železa	železo	k1gNnSc2
(	(	kIx(
<g/>
až	až	k6eAd1
u	u	k7c2
40	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
u	u	k7c2
58	#num#	k4
%	%	kIx~
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
z	z	k7c2
příslušné	příslušný	k2eAgFnSc2d1
vegetariánské	vegetariánský	k2eAgFnSc2d1
či	či	k8xC
veganské	veganský	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
Americká	americký	k2eAgFnSc1d1
dietetická	dietetický	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
nedostatek	nedostatek	k1gInSc1
železa	železo	k1gNnSc2
není	být	k5eNaImIp3nS
častější	častý	k2eAgInSc1d2
u	u	k7c2
vegetariánů	vegetarián	k1gMnPc2
než	než	k8xS
nevegetariánů	nevegetarián	k1gMnPc2
(	(	kIx(
<g/>
dospělí	dospělý	k2eAgMnPc1d1
muži	muž	k1gMnPc1
mají	mít	k5eAaImIp3nP
jen	jen	k9
zřídka	zřídka	k6eAd1
nedostatek	nedostatek	k1gInSc1
železa	železo	k1gNnSc2
<g/>
)	)	kIx)
<g/>
;	;	kIx,
nedostatkem	nedostatek	k1gInSc7
železa	železo	k1gNnSc2
způsobená	způsobený	k2eAgFnSc1d1
chudokrevnost	chudokrevnost	k1gFnSc1
je	být	k5eAaImIp3nS
vzácná	vzácný	k2eAgFnSc1d1
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
stravu	strava	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vitamin	vitamin	k1gInSc1
B12	B12	k1gFnSc2
</s>
<s>
Podle	podle	k7c2
Národního	národní	k2eAgInSc2d1
institutu	institut	k1gInSc2
pro	pro	k7c4
zdraví	zdraví	k1gNnSc4
ze	z	k7c2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgFnPc2d1
není	být	k5eNaImIp3nS
vitamin	vitamin	k1gInSc4
B12	B12	k1gFnSc1
přítomen	přítomno	k1gNnPc2
v	v	k7c6
rostlinách	rostlina	k1gFnPc6
a	a	k8xC
přirozeně	přirozeně	k6eAd1
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
v	v	k7c6
potravinách	potravina	k1gFnPc6
živočišného	živočišný	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
Lakto-ovo	Lakto-ovo	k1gNnSc4
vegetariáni	vegetarián	k1gMnPc1
mohou	moct	k5eAaImIp3nP
získat	získat	k5eAaPmF
vitamín	vitamín	k1gInSc4
B12	B12	k1gFnPc2
z	z	k7c2
mléčných	mléčný	k2eAgInPc2d1
výrobků	výrobek	k1gInPc2
a	a	k8xC
vajec	vejce	k1gNnPc2
a	a	k8xC
vegani	vegan	k1gMnPc1
jej	on	k3xPp3gMnSc4
mohou	moct	k5eAaImIp3nP
získat	získat	k5eAaPmF
z	z	k7c2
obohacených	obohacený	k2eAgFnPc2d1
potravin	potravina	k1gFnPc2
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
včetně	včetně	k7c2
některých	některý	k3yIgMnPc2
sójových	sójový	k2eAgMnPc2d1
výrobků	výrobek	k1gInPc2
a	a	k8xC
některých	některý	k3yIgFnPc2
snídaňových	snídaňový	k2eAgFnPc2d1
obilovin	obilovina	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
potravinových	potravinový	k2eAgInPc2d1
doplňků	doplněk	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
Vitamin	vitamin	k1gInSc1
B12	B12	k1gFnSc2
může	moct	k5eAaImIp3nS
také	také	k9
být	být	k5eAaImF
získán	získat	k5eAaPmNgInS
z	z	k7c2
obohacených	obohacený	k2eAgInPc2d1
výtažků	výtažek	k1gInPc2
z	z	k7c2
kvasnicových	kvasnicový	k2eAgInPc2d1
produktů	produkt	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Doporučený	doporučený	k2eAgInSc1d1
denní	denní	k2eAgInSc1d1
příjem	příjem	k1gInSc1
vitamínu	vitamín	k1gInSc2
B12	B12	k1gFnPc2
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
na	na	k7c4
den	den	k1gInSc4
je	být	k5eAaImIp3nS
0,4	0,4	k4
μ	μ	k1gFnPc2
(	(	kIx(
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
měsíců	měsíc	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zvýšený	zvýšený	k2eAgInSc4d1
na	na	k7c4
1,8	1,8	k4
μ	μ	k1gFnPc2
(	(	kIx(
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2,4	2,4	k4
μ	μ	k1gFnPc2
(	(	kIx(
<g/>
14	#num#	k4
let	léto	k1gNnPc2
a	a	k8xC
starší	starý	k2eAgMnPc1d2
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
a	a	k8xC
2,8	2,8	k4
μ	μ	k1gFnPc2
(	(	kIx(
<g/>
pro	pro	k7c4
kojící	kojící	k2eAgFnPc4d1
ženy	žena	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
I	i	k9
když	když	k8xS
denní	denní	k2eAgInSc1d1
požadavek	požadavek	k1gInSc1
těla	tělo	k1gNnSc2
na	na	k7c4
vitamin	vitamin	k1gInSc4
B12	B12	k1gFnSc2
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
malý	malý	k2eAgInSc1d1
<g/>
,	,	kIx,
nedostatek	nedostatek	k1gInSc1
vitamínu	vitamín	k1gInSc2
způsobuje	způsobovat	k5eAaImIp3nS
anémii	anémie	k1gFnSc4
a	a	k8xC
nevratné	vratný	k2eNgNnSc4d1
poškození	poškození	k1gNnSc4
nervů	nerv	k1gInPc2
a	a	k8xC
může	moct	k5eAaImIp3nS
tak	tak	k6eAd1
velmi	velmi	k6eAd1
negativně	negativně	k6eAd1
ovlivnit	ovlivnit	k5eAaPmF
zdraví	zdraví	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mastné	mastný	k2eAgFnPc1d1
kyseliny	kyselina	k1gFnPc1
</s>
<s>
Mezi	mezi	k7c4
zdroje	zdroj	k1gInPc4
omega-	omega-	k?
<g/>
3	#num#	k4
nenasycených	nasycený	k2eNgFnPc2d1
mastných	mastný	k2eAgFnPc2d1
kyselin	kyselina	k1gFnPc2
na	na	k7c6
rostlinné	rostlinný	k2eAgFnSc6d1
nebo	nebo	k8xC
vegetariánské	vegetariánský	k2eAgFnSc6d1
bázi	báze	k1gFnSc6
patří	patřit	k5eAaImIp3nS
soja	soja	k1gFnSc1
<g/>
,	,	kIx,
ořechy	ořech	k1gInPc1
<g/>
,	,	kIx,
dýňová	dýňový	k2eAgNnPc1d1
semínka	semínko	k1gNnPc1
<g/>
,	,	kIx,
řepkový	řepkový	k2eAgInSc1d1
olej	olej	k1gInSc1
<g/>
,	,	kIx,
kiwi	kiwi	k1gNnSc1
<g/>
,	,	kIx,
konopné	konopný	k2eAgNnSc1d1
semeno	semeno	k1gNnSc1
<g/>
,	,	kIx,
řasy	řasa	k1gFnPc1
<g/>
,	,	kIx,
chia	chia	k6eAd1
semeno	semeno	k1gNnSc4
<g/>
,	,	kIx,
lněný	lněný	k2eAgInSc1d1
olej	olej	k1gInSc1
<g/>
,	,	kIx,
hadincové	hadincový	k2eAgNnSc1d1
semeno	semeno	k1gNnSc1
a	a	k8xC
listová	listový	k2eAgFnSc1d1
zelenina	zelenina	k1gFnSc1
jako	jako	k9
je	být	k5eAaImIp3nS
hlávkový	hlávkový	k2eAgInSc1d1
salát	salát	k1gInSc1
<g/>
,	,	kIx,
špenát	špenát	k1gInSc1
<g/>
,	,	kIx,
zelí	zelí	k1gNnSc1
a	a	k8xC
šrucha	šrucha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šrucha	šrucha	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
více	hodně	k6eAd2
omega-	omega-	k?
<g/>
3	#num#	k4
nenasycených	nasycený	k2eNgFnPc2d1
mastných	mastný	k2eAgFnPc2d1
kyselin	kyselina	k1gFnPc2
než	než	k8xS
jakýkoli	jakýkoli	k3yIgInSc4
jiný	jiný	k2eAgInSc4d1
známý	známý	k2eAgInSc4d1
druh	druh	k1gInSc4
listové	listový	k2eAgFnSc2d1
zeleniny	zelenina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olivy	oliva	k1gFnPc1
(	(	kIx(
<g/>
a	a	k8xC
olivový	olivový	k2eAgInSc4d1
olej	olej	k1gInSc4
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
dalším	další	k2eAgInSc7d1
důležitým	důležitý	k2eAgInSc7d1
rostlinným	rostlinný	k2eAgInSc7d1
zdrojem	zdroj	k1gInSc7
nenasycených	nasycený	k2eNgFnPc2d1
mastných	mastný	k2eAgFnPc2d1
kyselin	kyselina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rostlinná	rostlinný	k2eAgFnSc1d1
strava	strava	k1gFnSc1
může	moct	k5eAaImIp3nS
poskytnout	poskytnout	k5eAaPmF
kyselinu	kyselina	k1gFnSc4
alfa-linolenovou	alfa-linolenový	k2eAgFnSc4d1
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
lidské	lidský	k2eAgNnSc1d1
tělo	tělo	k1gNnSc1
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
k	k	k7c3
syntéze	syntéza	k1gFnSc3
dlouhých	dlouhý	k2eAgInPc2d1
řetězců	řetězec	k1gInPc2
n-	n-	k?
<g/>
3	#num#	k4
mastných	mastný	k2eAgFnPc2d1
kyselin	kyselina	k1gFnPc2
EPA	EPA	kA
a	a	k8xC
DHA	DHA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
EPA	EPA	kA
a	a	k8xC
DHA	DHA	kA
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
přímo	přímo	k6eAd1
ve	v	k7c6
vysokých	vysoký	k2eAgNnPc6d1
množstvích	množství	k1gNnPc6
získány	získán	k2eAgInPc1d1
od	od	k7c2
olejovitých	olejovitý	k2eAgFnPc2d1
ryb	ryba	k1gFnPc2
nebo	nebo	k8xC
z	z	k7c2
rybího	rybí	k2eAgInSc2d1
oleje	olej	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vegetariáni	vegetarián	k1gMnPc1
a	a	k8xC
obzvláště	obzvláště	k6eAd1
vegani	vegan	k1gMnPc1
zvláště	zvláště	k6eAd1
mají	mít	k5eAaImIp3nP
nižší	nízký	k2eAgFnSc4d2
hladinu	hladina	k1gFnSc4
EPA	EPA	kA
a	a	k8xC
DHA	DHA	kA
než	než	k8xS
jedlíci	jedlík	k1gMnPc1
masa	maso	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
zdravotní	zdravotní	k2eAgInPc4d1
účinky	účinek	k1gInPc4
nízké	nízký	k2eAgFnSc2d1
hladiny	hladina	k1gFnSc2
EPA	EPA	kA
a	a	k8xC
DHA	DHA	kA
nejsou	být	k5eNaImIp3nP
známy	znám	k2eAgFnPc1d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
nepravděpodobné	pravděpodobný	k2eNgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
dodatečný	dodatečný	k2eAgInSc4d1
přísun	přísun	k1gInSc4
(	(	kIx(
<g/>
suplementace	suplementace	k1gFnSc2
<g/>
)	)	kIx)
kyseliny	kyselina	k1gFnSc2
alfa-linolenové	alfa-linolenová	k1gFnSc2
výrazně	výrazně	k6eAd1
zvýší	zvýšit	k5eAaPmIp3nS
jejich	jejich	k3xOp3gFnPc4
hladiny	hladina	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
[	[	kIx(
<g/>
ujasnit	ujasnit	k5eAaPmF
<g/>
]	]	kIx)
V	v	k7c6
poslední	poslední	k2eAgFnSc6d1
době	doba	k1gFnSc6
některé	některý	k3yIgFnPc1
společnosti	společnost	k1gFnPc1
začaly	začít	k5eAaPmAgFnP
nabízet	nabízet	k5eAaImF
vegetariánské	vegetariánský	k2eAgFnPc1d1
DHA	DHA	kA
potravní	potravní	k2eAgInPc1d1
doplňky	doplněk	k1gInPc1
obsahující	obsahující	k2eAgInPc1d1
výtažky	výtažek	k1gInPc4
z	z	k7c2
mořských	mořský	k2eAgFnPc2d1
řas	řasa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobně	podobně	k6eAd1
se	se	k3xPyFc4
také	také	k9
na	na	k7c6
trhu	trh	k1gInSc6
začaly	začít	k5eAaPmAgInP
objevovat	objevovat	k5eAaImF
doplňky	doplněk	k1gInPc7
jak	jak	k8xS,k8xC
s	s	k7c7
DHA	DHA	kA
<g/>
,	,	kIx,
tak	tak	k6eAd1
i	i	k8xC
s	s	k7c7
EPA	EPA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
81	#num#	k4
<g/>
]	]	kIx)
Celé	celý	k2eAgFnPc1d1
mořské	mořský	k2eAgFnPc1d1
řasy	řasa	k1gFnPc1
nejsou	být	k5eNaImIp3nP
vhodné	vhodný	k2eAgFnPc1d1
pro	pro	k7c4
dodatečný	dodatečný	k2eAgInSc4d1
přísun	přísun	k1gInSc4
<g/>
,	,	kIx,
protože	protože	k8xS
jejich	jejich	k3xOp3gInSc1
vysoký	vysoký	k2eAgInSc1d1
obsah	obsah	k1gInSc1
jodu	jod	k1gInSc2
omezuje	omezovat	k5eAaImIp3nS
maximální	maximální	k2eAgNnSc4d1
množství	množství	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
bezpečně	bezpečně	k6eAd1
snědeno	sněden	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnSc2
řasy	řasa	k1gFnSc2
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
spirulina	spirulin	k2eAgFnSc1d1
jsou	být	k5eAaImIp3nP
dobrým	dobrý	k2eAgInSc7d1
zdrojem	zdroj	k1gInSc7
gama-linolenové	gama-linolenový	k2eAgFnSc2d1
kyseliny	kyselina	k1gFnSc2
(	(	kIx(
<g/>
GLA	GLA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
alfa-linolenové	alfa-linolenový	k2eAgFnSc2d1
kyseliny	kyselina	k1gFnSc2
(	(	kIx(
<g/>
ALA	ala	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kyseliny	kyselina	k1gFnSc2
linolové	linolový	k2eAgFnSc2d1
(	(	kIx(
<g/>
LA	la	k1gNnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kyseliny	kyselina	k1gFnSc2
stearidonové	stearidonová	k1gFnSc2
(	(	kIx(
<g/>
SDA	SDA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
eikosapentaenová	eikosapentaenová	k1gFnSc1
kyseliny	kyselina	k1gFnSc2
(	(	kIx(
<g/>
EPA	EPA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dokosahexaenová	dokosahexaenová	k1gFnSc1
kyseliny	kyselina	k1gFnSc2
(	(	kIx(
<g/>
DHA	DHA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
kyselina	kyselina	k1gFnSc1
arachidonové	arachidonová	k1gFnSc2
(	(	kIx(
<g/>
AA	AA	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
82	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
83	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vápník	vápník	k1gInSc1
</s>
<s>
Při	při	k7c6
správně	správně	k6eAd1
naplánované	naplánovaný	k2eAgFnSc6d1
stravě	strava	k1gFnSc6
mají	mít	k5eAaImIp3nP
vegetariáni	vegetarián	k1gMnPc1
a	a	k8xC
vegani	vegaň	k1gFnSc3
podobný	podobný	k2eAgInSc4d1
příjem	příjem	k1gInSc4
vápníku	vápník	k1gInSc2
jako	jako	k8xS,k8xC
nevegetariáni	nevegetarián	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
Laktoovo	Laktoovo	k1gNnSc4
vegetariáni	vegetarián	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
jedí	jíst	k5eAaImIp3nP
mléčné	mléčný	k2eAgInPc4d1
výrobky	výrobek	k1gInPc4
<g/>
,	,	kIx,
mohou	moct	k5eAaImIp3nP
získat	získat	k5eAaPmF
vápník	vápník	k1gInSc4
z	z	k7c2
mléčných	mléčný	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
mléko	mléko	k1gNnSc1
<g/>
,	,	kIx,
jogurt	jogurt	k1gInSc1
a	a	k8xC
sýry	sýr	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mléka	mléko	k1gNnPc1
rostlinného	rostlinný	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yQgNnPc1,k3yRgNnPc1
jsou	být	k5eAaImIp3nP
obohacená	obohacený	k2eAgFnSc1d1
vápníkem	vápník	k1gInSc7
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
sojové	sojový	k2eAgNnSc1d1
a	a	k8xC
mandlové	mandlový	k2eAgNnSc1d1
mléko	mléko	k1gNnSc1
<g/>
,	,	kIx,
mohou	moct	k5eAaImIp3nP
přispět	přispět	k5eAaPmF
významným	významný	k2eAgNnSc7d1
množstvím	množství	k1gNnSc7
vápníku	vápník	k1gInSc2
v	v	k7c6
potravě	potrava	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
86	#num#	k4
<g/>
]	]	kIx)
Bylo	být	k5eAaImAgNnS
také	také	k9
zjištěno	zjistit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
vápník	vápník	k1gInSc4
obsažený	obsažený	k2eAgInSc4d1
v	v	k7c6
brokolici	brokolice	k1gFnSc6
<g/>
,	,	kIx,
čínském	čínský	k2eAgNnSc6d1
zelí	zelí	k1gNnSc6
a	a	k8xC
v	v	k7c6
kapustě	kapusta	k1gFnSc6
<g />
.	.	kIx.
</s>
<s hack="1">
je	být	k5eAaImIp3nS
v	v	k7c6
těle	tělo	k1gNnSc6
dobře	dobře	k6eAd1
absorbován	absorbován	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
87	#num#	k4
<g/>
]	]	kIx)
I	i	k9
když	když	k8xS
je	být	k5eAaImIp3nS
obsah	obsah	k1gInSc4
vápníku	vápník	k1gInSc2
v	v	k7c6
jedné	jeden	k4xCgFnSc6
porci	porce	k1gFnSc6
nižší	nízký	k2eAgFnSc1d2
v	v	k7c6
těchto	tento	k3xDgInPc6
druzích	druh	k1gInPc6
zeleniny	zelenina	k1gFnSc2
než	než	k8xS
ve	v	k7c6
sklenici	sklenice	k1gFnSc6
mléka	mléko	k1gNnSc2
<g/>
,	,	kIx,
vstřebávání	vstřebávání	k1gNnSc2
vápníku	vápník	k1gInSc2
do	do	k7c2
těla	tělo	k1gNnSc2
je	být	k5eAaImIp3nS
vyšší	vysoký	k2eAgFnSc1d2
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
87	#num#	k4
<g/>
]	]	kIx)
Mezi	mezi	k7c4
ostatní	ostatní	k2eAgFnPc4d1
potraviny	potravina	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
obsahují	obsahovat	k5eAaImIp3nP
vápník	vápník	k1gInSc4
<g/>
,	,	kIx,
patří	patřit	k5eAaImIp3nS
tofu	tofu	k1gNnSc1
<g/>
,	,	kIx,
melasa	melasa	k1gFnSc1
<g/>
,	,	kIx,
vodnice	vodnice	k1gFnSc1
<g/>
,	,	kIx,
hořčice	hořčice	k1gFnSc1
<g/>
,	,	kIx,
sojové	sojový	k2eAgInPc4d1
boby	bob	k1gInPc4
<g/>
,	,	kIx,
tempeh	tempeh	k1gInSc4
<g/>
,	,	kIx,
mandle	mandle	k1gFnPc4
<g/>
,	,	kIx,
ibiškovec	ibiškovec	k1gInSc4
jedlý	jedlý	k2eAgInSc4d1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
okra	okrum	k1gNnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
sušené	sušený	k2eAgInPc4d1
fíky	fík	k1gInPc4
<g/>
,	,	kIx,
a	a	k8xC
tahini	tahin	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
86	#num#	k4
<g/>
]	]	kIx)
I	i	k9
když	když	k8xS
vápník	vápník	k1gInSc4
lze	lze	k6eAd1
nalézt	nalézt	k5eAaBmF,k5eAaPmF
ve	v	k7c6
špenátu	špenát	k1gInSc6
<g/>
,	,	kIx,
mangoldu	mangold	k1gInSc6
<g/>
,	,	kIx,
fazolích	fazole	k1gFnPc6
a	a	k8xC
listech	list	k1gInPc6
červené	červený	k2eAgFnSc2d1
řepy	řepa	k1gFnSc2
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
ty	ten	k3xDgMnPc4
obecně	obecně	k6eAd1
nejsou	být	k5eNaImIp3nP
považovány	považován	k2eAgInPc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
za	za	k7c4
dobrý	dobrý	k2eAgInSc4d1
zdroj	zdroj	k1gInSc4
vápníku	vápník	k1gInSc2
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
tam	tam	k6eAd1
váže	vázat	k5eAaImIp3nS
na	na	k7c4
kyselinu	kyselina	k1gFnSc4
šťavelovou	šťavelový	k2eAgFnSc4d1
a	a	k8xC
je	být	k5eAaImIp3nS
špatně	špatně	k6eAd1
vstřebáván	vstřebávat	k5eAaImNgMnS
do	do	k7c2
těla	tělo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
Kyselina	kyselina	k1gFnSc1
fytová	fytová	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
ořeších	ořech	k1gInPc6
<g/>
,	,	kIx,
semenech	semeno	k1gNnPc6
a	a	k8xC
fazolích	fazole	k1gFnPc6
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
také	také	k9
ovlivnit	ovlivnit	k5eAaPmF
úroveň	úroveň	k1gFnSc4
vstřebávání	vstřebávání	k1gNnSc2
vápníku	vápník	k1gInSc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
Podívejte	podívat	k5eAaImRp2nP,k5eAaPmRp2nP
se	se	k3xPyFc4
na	na	k7c4
informace	informace	k1gFnPc4
National	National	k1gMnPc2
Institutes	Institutesa	k1gFnPc2
of	of	k?
Health	Healtha	k1gFnPc2
Office	Office	kA
of	of	k?
Dietary	Dietara	k1gFnSc2
Supplements	Supplementsa	k1gFnPc2
na	na	k7c4
potřebu	potřeba	k1gFnSc4
vápníku	vápník	k1gInSc2
pro	pro	k7c4
různé	různý	k2eAgFnPc4d1
věkové	věkový	k2eAgFnPc4d1
kategorie	kategorie	k1gFnPc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
dále	daleko	k6eAd2
na	na	k7c4
informace	informace	k1gFnPc4
Vegetariánské	vegetariánský	k2eAgFnSc2d1
výzkumné	výzkumný	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
<g/>
[	[	kIx(
<g/>
86	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
výživového	výživový	k2eAgInSc2d1
soupis	soupis	k1gInSc4
vápníku	vápník	k1gInSc2
Akademie	akademie	k1gFnSc2
výživy	výživa	k1gFnSc2
a	a	k8xC
diety	dieta	k1gFnSc2
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
pro	pro	k7c4
další	další	k2eAgNnPc4d1
specifika	specifikon	k1gNnPc4
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
získat	získat	k5eAaPmF
dostatečný	dostatečný	k2eAgInSc4d1
příjem	příjem	k1gInSc4
vápníku	vápník	k1gInSc2
při	při	k7c6
vegetariánské	vegetariánský	k2eAgFnSc6d1
nebo	nebo	k8xC
veganské	veganský	k2eAgFnSc6d1
stravě	strava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Vitamin	vitamin	k1gInSc1
D	D	kA
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Vitamin	vitamin	k1gInSc1
D.	D.	kA
</s>
<s>
Potřebu	potřeba	k1gFnSc4
vitaminu	vitamin	k1gInSc2
D	D	kA
lze	lze	k6eAd1
dosáhnout	dosáhnout	k5eAaPmF
prostřednictvím	prostřednictvím	k7c2
tvorby	tvorba	k1gFnSc2
ve	v	k7c6
vlastním	vlastní	k2eAgNnSc6d1
těle	tělo	k1gNnSc6
při	při	k7c6
dostatečném	dostatečný	k2eAgNnSc6d1
a	a	k8xC
rozumném	rozumný	k2eAgNnSc6d1
vystavení	vystavení	k1gNnSc6
ultrafialovému	ultrafialový	k2eAgNnSc3d1
záření	záření	k1gNnSc3
ze	z	k7c2
slunečního	sluneční	k2eAgNnSc2d1
světla	světlo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
88	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
89	#num#	k4
<g/>
]	]	kIx)
Výrobky	výrobek	k1gInPc4
obsahující	obsahující	k2eAgNnSc4d1
mléko	mléko	k1gNnSc4
<g/>
,	,	kIx,
sójové	sójový	k2eAgNnSc4d1
mléko	mléko	k1gNnSc4
a	a	k8xC
obilné	obilný	k2eAgNnSc4d1
zrna	zrno	k1gNnPc4
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
ochuceny	ochutit	k5eAaPmNgInP
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
slouží	sloužit	k5eAaImIp3nS
jako	jako	k9
zdroj	zdroj	k1gInSc1
vitamínu	vitamín	k1gInSc2
D.	D.	kA
<g/>
[	[	kIx(
<g/>
90	#num#	k4
<g/>
]	]	kIx)
Pro	pro	k7c4
ty	ten	k3xDgMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
nejsou	být	k5eNaImIp3nP
vystaveni	vystavit	k5eAaPmNgMnP
adekvátní	adekvátní	k2eAgFnSc7d1
expozici	expozice	k1gFnSc4
slunečnímu	sluneční	k2eAgNnSc3d1
záření	záření	k1gNnSc3
nebo	nebo	k8xC
zdrojům	zdroj	k1gInPc3
potravy	potrava	k1gFnSc2
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
nezbytný	zbytný	k2eNgInSc4d1,k2eAgInSc4d1
dodatečný	dodatečný	k2eAgInSc4d1
přísun	přísun	k1gInSc4
vitamin	vitamin	k1gInSc1
D.	D.	kA
</s>
<s>
Vitamin	vitamin	k1gInSc1
D2	D2	k1gFnSc2
</s>
<s>
Vitamin	vitamin	k1gInSc1
D2	D2	k1gFnSc2
neboli	neboli	k8xC
ergokalciferol	ergokalciferol	k1gInSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
houbách	houba	k1gFnPc6
a	a	k8xC
v	v	k7c6
rostlině	rostlina	k1gFnSc6
vojtěšce	vojtěška	k1gFnSc6
(	(	kIx(
<g/>
Medicago	Medicago	k6eAd1
sativa	sativa	k1gFnSc1
subsp	subsp	k1gInSc1
sativa	sativa	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tvoří	tvořit	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
pokud	pokud	k8xS
ultrafialové	ultrafialový	k2eAgNnSc1d1
světlo	světlo	k1gNnSc1
aktivuje	aktivovat	k5eAaBmIp3nS
ergosterol	ergosterol	k1gInSc4
(	(	kIx(
<g/>
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
houbách	houba	k1gFnPc6
a	a	k8xC
je	být	k5eAaImIp3nS
pojmenovaný	pojmenovaný	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
sterol	sterol	k1gInSc1
z	z	k7c2
námelu	námel	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jakékoliv	jakýkoliv	k3yIgInPc4
ultrafialovým	ultrafialový	k2eAgNnSc7d1
zářením	záření	k1gNnSc7
osvícené	osvícený	k2eAgFnSc2d1
houby	houba	k1gFnSc2
obsahují	obsahovat	k5eAaImIp3nP
kvasinkovou	kvasinkový	k2eAgFnSc4d1
formu	forma	k1gFnSc4
vitamínu	vitamín	k1gInSc2
D	D	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
91	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Koncentrace	koncentrace	k1gFnSc1
vitamínu	vitamín	k1gInSc2
D2	D2	k1gFnSc2
v	v	k7c6
houbách	houba	k1gFnPc6
je	být	k5eAaImIp3nS
např.	např.	kA
u	u	k7c2
syrové	syrový	k2eAgFnSc2d1
pečárky	pečárka	k1gFnSc2
dvouvýtrusé	dvouvýtrusý	k2eAgFnSc2d1
0,3	0,3	k4
μ	μ	k1gFnPc2
na	na	k7c4
1	#num#	k4
kg	kg	kA
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
vystavena	vystavit	k5eAaPmNgFnS
ultrafialovému	ultrafialový	k2eAgNnSc3d1
světlu	světlo	k1gNnSc3
a	a	k8xC
syrová	syrový	k2eAgFnSc1d1
<g/>
,	,	kIx,
tak	tak	k6eAd1
11,2	11,2	k4
μ	μ	k1gFnPc2
a	a	k8xC
u	u	k7c2
grilované	grilovaný	k2eAgFnSc2d1
13,1	13,1	k4
μ	μ	k1gFnPc2
na	na	k7c4
1	#num#	k4
kg	kg	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
92	#num#	k4
<g/>
]	]	kIx)
Výhonky	výhonek	k1gInPc1
vojtěšky	vojtěška	k1gFnSc2
obsahují	obsahovat	k5eAaImIp3nP
4,8	4,8	k4
μ	μ	k1gFnPc2
vitamínu	vitamín	k1gInSc2
D	D	kA
<g/>
2	#num#	k4
<g/>
,	,	kIx,
0,1	0,1	k4
ug	ug	k?
(	(	kIx(
<g/>
4	#num#	k4
IU	IU	kA
<g/>
)	)	kIx)
vitaminu	vitamin	k1gInSc6
D	D	kA
<g/>
3	#num#	k4
<g/>
na	na	k7c4
1	#num#	k4
kg	kg	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
93	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dlouhověkost	dlouhověkost	k1gFnSc1
</s>
<s>
Metaanalýza	Metaanalýza	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1999	#num#	k4
kombinovala	kombinovat	k5eAaImAgFnS
údaje	údaj	k1gInPc4
z	z	k7c2
pěti	pět	k4xCc2
studií	studio	k1gNnPc2
ze	z	k7c2
západních	západní	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
94	#num#	k4
<g/>
]	]	kIx)
Metaanalýza	Metaanalýza	k1gFnSc1
publikovala	publikovat	k5eAaBmAgFnS
poměry	poměra	k1gFnPc4
úmrtnosti	úmrtnost	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
nižší	nízký	k2eAgNnPc1d2
čísla	číslo	k1gNnPc1
ukazují	ukazovat	k5eAaImIp3nP
méně	málo	k6eAd2
úmrtí	úmrtí	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
<g/>
:	:	kIx,
pro	pro	k7c4
jedlíky	jedlík	k1gMnPc4
ryb	ryba	k1gFnPc2
0,82	0,82	k4
<g/>
,	,	kIx,
pro	pro	k7c4
vegetariány	vegetarián	k1gMnPc7
0,84	0,84	k4
a	a	k8xC
příležitostné	příležitostný	k2eAgMnPc4d1
masožravce	masožravec	k1gMnPc4
(	(	kIx(
<g/>
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
jedí	jíst	k5eAaImIp3nP
maso	maso	k1gNnSc4
méně	málo	k6eAd2
než	než	k8xS
jednou	jeden	k4xCgFnSc7
týdně	týdně	k6eAd1
<g/>
)	)	kIx)
0,84	0,84	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravidelní	pravidelní	k2eAgMnPc1d1
konzumenti	konzument	k1gMnPc1
masa	maso	k1gNnSc2
měli	mít	k5eAaImAgMnP
úroveň	úroveň	k1gFnSc4
úmrtnosti	úmrtnost	k1gFnSc2
1,0	1,0	k4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
údaj	údaj	k1gInSc1
pro	pro	k7c4
vegany	vegan	k1gInPc4
byl	být	k5eAaImAgInS
velmi	velmi	k6eAd1
nejistý	jistý	k2eNgInSc1d1
(	(	kIx(
<g/>
kdekoliv	kdekoliv	k6eAd1
mezi	mezi	k7c7
0,7	0,7	k4
a	a	k8xC
1,44	1,44	k4
<g/>
)	)	kIx)
v	v	k7c6
důsledku	důsledek	k1gInSc6
příliš	příliš	k6eAd1
mála	málo	k4c2
datových	datový	k2eAgInPc2d1
bodů	bod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studie	studie	k1gFnSc1
uveřejnila	uveřejnit	k5eAaPmAgFnS
počty	počet	k1gInPc4
úmrtí	úmrtí	k1gNnSc2
v	v	k7c6
každé	každý	k3xTgFnSc6
kategorii	kategorie	k1gFnSc6
a	a	k8xC
očekávané	očekávaný	k2eAgFnSc2d1
úrovně	úroveň	k1gFnSc2
chyb	chyba	k1gFnPc2
pro	pro	k7c4
každý	každý	k3xTgInSc4
poměr	poměr	k1gInSc4
a	a	k8xC
také	také	k9
úpravy	úprava	k1gFnPc4
provedené	provedený	k2eAgFnPc4d1
na	na	k7c6
datech	datum	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
„	„	k?
<g/>
nižší	nízký	k2eAgFnSc1d2
úmrtnost	úmrtnost	k1gFnSc1
byla	být	k5eAaImAgFnS
způsobena	způsobit	k5eAaPmNgFnS
do	do	k7c2
značné	značný	k2eAgFnSc2d1
míry	míra	k1gFnSc2
relativně	relativně	k6eAd1
nízkou	nízký	k2eAgFnSc7d1
mírou	míra	k1gFnSc7
rozšíření	rozšíření	k1gNnSc4
(	(	kIx(
<g/>
prevalencí	prevalence	k1gFnSc7
<g/>
)	)	kIx)
kouření	kouření	k1gNnSc1
v	v	k7c6
těchto	tento	k3xDgFnPc6
[	[	kIx(
<g/>
vegetariánských	vegetariánský	k2eAgFnPc6d1
<g/>
]	]	kIx)
kohortách	kohorta	k1gFnPc6
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
hlavních	hlavní	k2eAgFnPc2d1
příčin	příčina	k1gFnPc2
studovaných	studovaný	k2eAgFnPc2d1
úmrtí	úmrtí	k1gNnSc4
pouze	pouze	k6eAd1
jediný	jediný	k2eAgInSc1d1
rozdíl	rozdíl	k1gInSc1
v	v	k7c6
míře	míra	k1gFnSc6
úmrtnosti	úmrtnost	k1gFnSc2
byl	být	k5eAaImAgMnS
přičítán	přičítat	k5eAaImNgMnS
k	k	k7c3
rozdílům	rozdíl	k1gInPc3
ve	v	k7c6
stravování	stravování	k1gNnSc6
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
závěr	závěr	k1gInSc1
se	se	k3xPyFc4
uvádí	uvádět	k5eAaImIp3nS
<g/>
:	:	kIx,
„	„	k?
<g/>
...	...	k?
vegetariáni	vegetarián	k1gMnPc1
měli	mít	k5eAaImAgMnP
o	o	k7c4
24	#num#	k4
%	%	kIx~
nižší	nízký	k2eAgFnSc1d2
úmrtnost	úmrtnost	k1gFnSc1
na	na	k7c4
ischemickou	ischemický	k2eAgFnSc4d1
chorobu	choroba	k1gFnSc4
srdeční	srdeční	k2eAgInSc4d1
než	než	k8xS
nevegetariáni	nevegetarián	k1gMnPc1
<g/>
,	,	kIx,
nebyla	být	k5eNaImAgFnS
ale	ale	k9
prokázána	prokázat	k5eAaPmNgFnS
souvislost	souvislost	k1gFnSc1
vegetariánské	vegetariánský	k2eAgFnSc2d1
stravy	strava	k1gFnSc2
s	s	k7c7
dalšími	další	k2eAgFnPc7d1
hlavními	hlavní	k2eAgFnPc7d1
příčinami	příčina	k1gFnPc7
úmrtí	úmrtí	k1gNnSc2
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
94	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
zprávě	zpráva	k1gFnSc6
Úmrtnost	úmrtnost	k1gFnSc1
britských	britský	k2eAgMnPc2d1
vegetariánů	vegetarián	k1gMnPc2
<g/>
[	[	kIx(
<g/>
95	#num#	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
nastíněn	nastíněn	k2eAgInSc4d1
podobný	podobný	k2eAgInSc4d1
závěr	závěr	k1gInSc4
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
<g/>
Britští	britský	k2eAgMnPc1d1
vegetariáni	vegetarián	k1gMnPc1
mají	mít	k5eAaImIp3nP
nižší	nízký	k2eAgFnSc4d2
úmrtnost	úmrtnost	k1gFnSc4
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
běžnou	běžný	k2eAgFnSc7d1
populací	populace	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
úmrtnost	úmrtnost	k1gFnSc1
je	být	k5eAaImIp3nS
podobná	podobný	k2eAgFnSc1d1
jako	jako	k9
u	u	k7c2
srovnatelných	srovnatelný	k2eAgInPc2d1
nevegetariánů	nevegetarián	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
naznačuje	naznačovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
velká	velký	k2eAgFnSc1d1
část	část	k1gFnSc1
této	tento	k3xDgFnSc2
výhody	výhoda	k1gFnSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
přičítáno	přičítat	k5eAaImNgNnS
nestravovacím	stravovací	k2eNgInSc7d1
faktorům	faktor	k1gInPc3
životního	životní	k2eAgInSc2d1
stylu	styl	k1gInSc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
například	například	k6eAd1
nízké	nízký	k2eAgNnSc1d1
rozšíření	rozšíření	k1gNnSc1
kouření	kouření	k1gNnSc2
a	a	k8xC
obecně	obecně	k6eAd1
vysoce	vysoce	k6eAd1
sociálně-ekonomický	sociálně-ekonomický	k2eAgInSc4d1
status	status	k1gInSc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
těm	ten	k3xDgInPc3
aspektům	aspekt	k1gInPc3
stravy	strava	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
nesouvisí	souviset	k5eNaImIp3nS
s	s	k7c7
omezením	omezení	k1gNnSc7
konzumace	konzumace	k1gFnSc2
masa	maso	k1gNnSc2
a	a	k8xC
ryb	ryba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
96	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Adventistická	adventistický	k2eAgFnSc1d1
zdravotnická	zdravotnický	k2eAgFnSc1d1
studie	studie	k1gFnSc1
je	být	k5eAaImIp3nS
probíhající	probíhající	k2eAgInSc4d1
výzkum	výzkum	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
dokumentuje	dokumentovat	k5eAaBmIp3nS
délku	délka	k1gFnSc4
života	život	k1gInSc2
u	u	k7c2
Adventistů	adventista	k1gMnPc2
sedmého	sedmý	k4xOgInSc2
dne	den	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
jediná	jediný	k2eAgFnSc1d1
studie	studie	k1gFnSc1
mezi	mezi	k7c7
ostatními	ostatní	k2eAgMnPc7d1
s	s	k7c7
podobnou	podobný	k2eAgFnSc7d1
metodikou	metodika	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
měla	mít	k5eAaImAgFnS
pozitivní	pozitivní	k2eAgFnSc4d1
indikaci	indikace	k1gFnSc4
k	k	k7c3
vegetariánství	vegetariánství	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vědci	vědec	k1gMnPc1
zjistili	zjistit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
kombinace	kombinace	k1gFnSc1
různých	různý	k2eAgInPc2d1
životních	životní	k2eAgInPc2d1
stylů	styl	k1gInPc2
by	by	kYmCp3nS
mohla	moct	k5eAaImAgFnS
ovlivnit	ovlivnit	k5eAaPmF
délku	délka	k1gFnSc4
života	život	k1gInSc2
až	až	k9
o	o	k7c4
10	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
volbami	volba	k1gFnPc7
životního	životní	k2eAgInSc2d1
stylu	styl	k1gInSc2
se	se	k3xPyFc4
odhaduje	odhadovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
vegetariánský	vegetariánský	k2eAgInSc1d1
strava	strava	k1gFnSc1
přináší	přinášet	k5eAaImIp3nS
navíc	navíc	k6eAd1
1	#num#	k4
a	a	k8xC
půl	půl	k6eAd1
let	léto	k1gNnPc2
až	až	k6eAd1
2	#num#	k4
roky	rok	k1gInPc4
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výzkumníci	výzkumník	k1gMnPc1
došli	dojít	k5eAaPmAgMnP
k	k	k7c3
závěru	závěr	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
délka	délka	k1gFnSc1
života	život	k1gInSc2
kalifornských	kalifornský	k2eAgMnPc2d1
adventistických	adventistický	k2eAgMnPc2d1
mužů	muž	k1gMnPc2
a	a	k8xC
žen	žena	k1gFnPc2
je	být	k5eAaImIp3nS
vyšší	vysoký	k2eAgFnSc1d2
než	než	k8xS
u	u	k7c2
jakékoliv	jakýkoliv	k3yIgFnSc2
jiné	jiná	k1gFnSc2
dobře	dobře	k6eAd1
popsané	popsaný	k2eAgFnSc2d1
přirozené	přirozený	k2eAgFnSc2d1
populace	populace	k1gFnSc2
<g/>
“	“	k?
na	na	k7c6
úrovni	úroveň	k1gFnSc6
78,5	78,5	k4
let	léto	k1gNnPc2
u	u	k7c2
mužů	muž	k1gMnPc2
a	a	k8xC
82,3	82,3	k4
let	léto	k1gNnPc2
u	u	k7c2
žen	žena	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Střední	střední	k2eAgFnSc1d1
délka	délka	k1gFnSc1
života	život	k1gInSc2
kalifornských	kalifornský	k2eAgMnPc2d1
Adventistů	adventista	k1gMnPc2
přeživší	přeživší	k2eAgInPc1d1
30	#num#	k4
let	léto	k1gNnPc2
byla	být	k5eAaImAgFnS
83,3	83,3	k4
let	léto	k1gNnPc2
u	u	k7c2
mužů	muž	k1gMnPc2
a	a	k8xC
85,7	85,7	k4
let	léto	k1gNnPc2
u	u	k7c2
žen	žena	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
97	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zdravotní	zdravotní	k2eAgFnSc1d1
studie	studie	k1gFnSc1
Adventistů	adventista	k1gMnPc2
sedmého	sedmý	k4xOgNnSc2
dne	den	k1gInSc2
byla	být	k5eAaImAgNnP
opět	opět	k6eAd1
začleněna	začlenit	k5eAaPmNgNnP
do	do	k7c2
metastudie	metastudie	k1gFnSc2
s	s	k7c7
názvem	název	k1gInSc7
„	„	k?
<g/>
Prodlužuje	prodlužovat	k5eAaImIp3nS
nízká	nízký	k2eAgFnSc1d1
spotřeba	spotřeba	k1gFnSc1
masa	masa	k1gFnSc1
střední	střední	k2eAgFnSc4d1
délku	délka	k1gFnSc4
života	život	k1gInSc2
lidí	člověk	k1gMnPc2
<g/>
“	“	k?
publikovaná	publikovaný	k2eAgFnSc1d1
v	v	k7c6
časopise	časopis	k1gInSc6
American	American	k1gMnSc1
Journal	Journal	k1gMnSc1
of	of	k?
Clinical	Clinical	k1gFnSc1
Nutrition	Nutrition	k1gInSc1
<g/>
,	,	kIx,
podle	podle	k7c2
něhož	jenž	k3xRgInSc2
malé	malý	k2eAgNnSc1d1
jedení	jedení	k1gNnSc1
masa	maso	k1gNnSc2
(	(	kIx(
<g/>
méně	málo	k6eAd2
než	než	k8xS
jednou	jeden	k4xCgFnSc7
týdně	týdně	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
další	další	k2eAgFnPc1d1
volby	volba	k1gFnPc1
životního	životní	k2eAgInSc2d1
stylu	styl	k1gInSc2
výrazně	výrazně	k6eAd1
zvyšují	zvyšovat	k5eAaImIp3nP
průměrnou	průměrný	k2eAgFnSc4d1
délku	délka	k1gFnSc4
života	život	k1gInSc2
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
se	s	k7c7
skupinou	skupina	k1gFnSc7
s	s	k7c7
vysokým	vysoký	k2eAgInSc7d1
příjmem	příjem	k1gInSc7
masa	maso	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studie	studie	k1gFnSc1
dospěla	dochvít	k5eAaPmAgFnS
k	k	k7c3
závěru	závěr	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
Poznatky	poznatek	k1gInPc4
z	z	k7c2
jedné	jeden	k4xCgFnSc2
kohorty	kohorta	k1gFnSc2
zdravých	zdravý	k2eAgMnPc2d1
dospělých	dospělí	k1gMnPc2
zvyšuje	zvyšovat	k5eAaImIp3nS
možnost	možnost	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
dlouhodobé	dlouhodobý	k2eAgNnSc1d1
(	(	kIx(
<g/>
≥	≥	k?
2	#num#	k4
desetiletí	desetiletí	k1gNnSc2
<g/>
)	)	kIx)
dodržování	dodržování	k1gNnSc1
vegetariánské	vegetariánský	k2eAgFnSc2d1
stravy	strava	k1gFnSc2
může	moct	k5eAaImIp3nS
dále	daleko	k6eAd2
vyvolávat	vyvolávat	k5eAaImF
významný	významný	k2eAgInSc4d1
3,6	3,6	k4
<g/>
letý	letý	k2eAgInSc4d1
nárůst	nárůst	k1gInSc4
střední	střední	k2eAgFnSc2d1
délky	délka	k1gFnSc2
života	život	k1gInSc2
<g/>
.	.	kIx.
<g/>
“	“	k?
Studie	studie	k1gFnSc1
však	však	k9
také	také	k9
dospěla	dochvít	k5eAaPmAgFnS
<g />
.	.	kIx.
</s>
<s hack="1">
k	k	k7c3
závěru	závěr	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
<g/>
:	:	kIx,
„	„	k?
<g/>
Některé	některý	k3yIgInPc4
rozdíly	rozdíl	k1gInPc4
ve	v	k7c6
výhodě	výhoda	k1gFnSc6
pro	pro	k7c4
přežití	přežití	k1gNnSc4
u	u	k7c2
vegetariánů	vegetarián	k1gMnPc2
mohou	moct	k5eAaImIp3nP
byli	být	k5eAaImAgMnP
kvůli	kvůli	k7c3
výrazným	výrazný	k2eAgInPc3d1
rozdílům	rozdíl	k1gInPc3
mezi	mezi	k7c7
studiemi	studie	k1gFnPc7
v	v	k7c6
úpravě	úprava	k1gFnSc6
pro	pro	k7c4
zkreslující	zkreslující	k2eAgInPc4d1
jevy	jev	k1gInPc4
<g/>
,	,	kIx,
definici	definice	k1gFnSc4
vegetariána	vegetarián	k1gMnSc2
<g/>
,	,	kIx,
chyby	chyba	k1gFnPc1
měření	měření	k1gNnSc1
<g/>
,	,	kIx,
rozložení	rozložení	k1gNnSc1
věku	věk	k1gInSc2
<g/>
,	,	kIx,
efektu	efekt	k1gInSc2
zdravých	zdravý	k2eAgMnPc2d1
dobrovolníků	dobrovolník	k1gMnPc2
a	a	k8xC
příjmu	příjem	k1gInSc2
specifických	specifický	k2eAgFnPc2d1
potravin	potravina	k1gFnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
rostlinného	rostlinný	k2eAgInSc2d1
původu	původ	k1gInSc2
ze	z	k7c2
strany	strana	k1gFnSc2
vegetariánů	vegetarián	k1gMnPc2
<g/>
.	.	kIx.
“	“	k?
Dále	daleko	k6eAd2
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
<g/>
:	:	kIx,
„	„	k?
<g/>
To	ten	k3xDgNnSc1
vyvolává	vyvolávat	k5eAaImIp3nS
možnost	možnost	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
málo	málo	k1gNnSc1
masa	maso	k1gNnSc2
<g/>
,	,	kIx,
vysoce	vysoce	k6eAd1
rostlinná	rostlinný	k2eAgFnSc1d1
strava	strava	k1gFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
skutečnou	skutečný	k2eAgFnSc7d1
příčinou	příčina	k1gFnSc7
ochranného	ochranný	k2eAgInSc2d1
faktoru	faktor	k1gInSc2
spíše	spíše	k9
než	než	k8xS
jednoduše	jednoduše	k6eAd1
vyloučení	vyloučení	k1gNnSc1
masa	maso	k1gNnSc2
ze	z	k7c2
stravy	strava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
“	“	k?
V	v	k7c6
nedávném	dávný	k2eNgInSc6d1
přehledu	přehled	k1gInSc6
<g />
.	.	kIx.
</s>
<s hack="1">
studií	studie	k1gFnPc2
týkajících	týkající	k2eAgFnPc2d1
se	se	k3xPyFc4
vztahu	vztah	k1gInSc6
stravovacích	stravovací	k2eAgInPc2d1
návyků	návyk	k1gInPc2
s	s	k7c7
nízkým	nízký	k2eAgInSc7d1
obsahem	obsah	k1gInSc7
masa	maso	k1gNnSc2
ke	k	k7c3
všem	všecek	k3xTgFnPc3
příčinám	příčina	k1gFnPc3
úmrtnosti	úmrtnost	k1gFnSc2
<g/>
,	,	kIx,
autor	autor	k1gMnSc1
studie	studie	k1gFnSc2
Singh	Singh	k1gMnSc1
poznamenal	poznamenat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
5	#num#	k4
z	z	k7c2
5	#num#	k4
studií	studio	k1gNnPc2
ukázaly	ukázat	k5eAaPmAgInP
<g/>
,	,	kIx,
že	že	k8xS
u	u	k7c2
dospělých	dospělí	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
jedli	jíst	k5eAaImAgMnP
málo	málo	k1gNnSc4
masa	maso	k1gNnSc2
a	a	k8xC
vysoce	vysoce	k6eAd1
rostlinnou	rostlinný	k2eAgFnSc4d1
stravu	strava	k1gFnSc4
<g/>
,	,	kIx,
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
úmrtnosti	úmrtnost	k1gFnSc6
k	k	k7c3
významnému	významný	k2eAgInSc3d1
nebo	nebo	k8xC
málo	málo	k6eAd1
významnému	významný	k2eAgNnSc3d1
snížení	snížení	k1gNnSc3
rizika	riziko	k1gNnSc2
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
ostatními	ostatní	k2eAgInPc7d1
vzory	vzor	k1gInPc7
příjmu	příjem	k1gInSc2
potravy	potrava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
98	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Statistické	statistický	k2eAgFnPc1d1
studie	studie	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
například	například	k6eAd1
porovnávaly	porovnávat	k5eAaImAgFnP
průměrné	průměrný	k2eAgFnPc4d1
délky	délka	k1gFnPc4
života	život	k1gInSc2
v	v	k7c6
regionálních	regionální	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
a	a	k8xC
místní	místní	k2eAgFnSc4d1
stravu	strava	k1gFnSc4
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
také	také	k9
zjistily	zjistit	k5eAaPmAgFnP
<g/>
,	,	kIx,
že	že	k8xS
střední	střední	k2eAgFnSc1d1
délka	délka	k1gFnSc1
života	život	k1gInSc2
je	být	k5eAaImIp3nS
výrazně	výrazně	k6eAd1
vyšší	vysoký	k2eAgFnSc1d2
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
Francii	Francie	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
rozšířená	rozšířený	k2eAgFnSc1d1
středomořská	středomořský	k2eAgFnSc1d1
strava	strava	k1gFnSc1
s	s	k7c7
málem	málo	k1gNnSc7
masa	maso	k1gNnSc2
a	a	k8xC
bohatá	bohatý	k2eAgFnSc1d1
na	na	k7c4
rostliny	rostlina	k1gFnPc4
<g/>
,	,	kIx,
než	než	k8xS
v	v	k7c6
severní	severní	k2eAgFnSc6d1
Francii	Francie	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
častější	častý	k2eAgFnSc1d2
strava	strava	k1gFnSc1
s	s	k7c7
vysokým	vysoký	k2eAgInSc7d1
podílem	podíl	k1gInSc7
masa	maso	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
99	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Studie	studie	k1gFnSc1
vypracovaná	vypracovaný	k2eAgFnSc1d1
Ústavem	ústav	k1gInSc7
preventivní	preventivní	k2eAgFnSc2d1
a	a	k8xC
klinické	klinický	k2eAgFnSc2d1
medicíny	medicína	k1gFnSc2
(	(	kIx(
<g/>
Bratislava	Bratislava	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
Ústavem	ústav	k1gInSc7
fyziologické	fyziologický	k2eAgFnSc2d1
chemie	chemie	k1gFnSc2
(	(	kIx(
<g/>
Würzburg	Würzburg	k1gInSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
podívala	podívat	k5eAaPmAgFnS,k5eAaImAgFnS
na	na	k7c4
skupinu	skupina	k1gFnSc4
19	#num#	k4
vegetariánů	vegetarián	k1gMnPc2
(	(	kIx(
<g/>
lakto-ovo	lakto-ovo	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
použila	použít	k5eAaPmAgFnS
ji	on	k3xPp3gFnSc4
pro	pro	k7c4
srovnání	srovnání	k1gNnSc4
se	s	k7c7
skupinou	skupina	k1gFnSc7
19	#num#	k4
všežravců	všežravec	k1gMnPc2
vybraných	vybraný	k2eAgMnPc2d1
ze	z	k7c2
stejného	stejný	k2eAgInSc2d1
regionu	region	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studie	studie	k1gFnSc1
zjistila	zjistit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
tato	tento	k3xDgFnSc1
skupina	skupina	k1gFnSc1
vegetariánů	vegetarián	k1gMnPc2
(	(	kIx(
<g/>
lakto-ovo	lakto-ovo	k1gNnSc1
<g/>
)	)	kIx)
má	mít	k5eAaImIp3nS
významně	významně	k6eAd1
vyšší	vysoký	k2eAgNnSc4d2
množství	množství	k1gNnSc4
plazmy	plazma	k1gFnSc2
karboxymethyllysinu	karboxymethyllysina	k1gFnSc4
a	a	k8xC
konečné	konečný	k2eAgInPc1d1
produkty	produkt	k1gInPc1
pokročilé	pokročilý	k2eAgFnSc2d1
glykace	glykace	k1gFnSc2
v	v	k7c6
porovnání	porovnání	k1gNnSc6
se	s	k7c7
skupinou	skupina	k1gFnSc7
nevegetariánů	nevegetarián	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
100	#num#	k4
<g/>
]	]	kIx)
Karboxymethyllysin	Karboxymethyllysin	k1gInSc1
je	být	k5eAaImIp3nS
glykační	glykační	k2eAgInSc1d1
produkt	produkt	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
představuje	představovat	k5eAaImIp3nS
„	„	k?
<g/>
obecný	obecný	k2eAgInSc1d1
marker	marker	k1gInSc1
oxidačního	oxidační	k2eAgInSc2d1
<g />
.	.	kIx.
</s>
<s hack="1">
stresu	stres	k1gInSc2
a	a	k8xC
dlouhodobého	dlouhodobý	k2eAgNnSc2d1
poškození	poškození	k1gNnSc2
proteinů	protein	k1gInPc2
v	v	k7c6
procesu	proces	k1gInSc6
stárnutí	stárnutí	k1gNnSc2
<g/>
,	,	kIx,
aterosklerózy	ateroskleróza	k1gFnSc2
a	a	k8xC
cukrovky	cukrovka	k1gFnSc2
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
konečné	konečný	k2eAgInPc1d1
produkty	produkt	k1gInPc1
pokročilé	pokročilý	k2eAgFnSc2d1
glykace	glykace	k1gFnSc2
mohou	moct	k5eAaImIp3nP
hrát	hrát	k5eAaImF
významnou	významný	k2eAgFnSc4d1
negativní	negativní	k2eAgFnSc4d1
roli	role	k1gFnSc4
v	v	k7c6
procesu	proces	k1gInSc6
aterosklerózy	ateroskleróza	k1gFnSc2
<g/>
,	,	kIx,
cukrovky	cukrovka	k1gFnSc2
<g/>
,	,	kIx,
stárnutí	stárnutí	k1gNnSc2
a	a	k8xC
chronického	chronický	k2eAgNnSc2d1
selhání	selhání	k1gNnSc2
ledvin	ledvina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
100	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Lékařské	lékařský	k2eAgNnSc1d1
použití	použití	k1gNnSc1
</s>
<s>
V	v	k7c6
západní	západní	k2eAgFnSc6d1
medicíně	medicína	k1gFnSc6
se	se	k3xPyFc4
pacientům	pacient	k1gMnPc3
někdy	někdy	k6eAd1
doporučuje	doporučovat	k5eAaImIp3nS
dodržovat	dodržovat	k5eAaImF
vegetariánskou	vegetariánský	k2eAgFnSc4d1
stravu	strava	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
101	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
studií	studie	k1gFnPc2
vědeckého	vědecký	k2eAgInSc2d1
časopisu	časopis	k1gInSc2
Permanente	permanent	k1gInSc5
Journal	Journal	k1gFnSc2
a	a	k8xC
Národního	národní	k2eAgInSc2d1
institutu	institut	k1gInSc2
pro	pro	k7c4
zdraví	zdraví	k1gNnSc4
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
vegetariánská	vegetariánský	k2eAgFnSc1d1
strava	strava	k1gFnSc1
cenově	cenově	k6eAd1
dostupná	dostupný	k2eAgFnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
přispět	přispět	k5eAaPmF
ke	k	k7c3
snížení	snížení	k1gNnSc3
zdravotních	zdravotní	k2eAgNnPc2d1
rizik	riziko	k1gNnPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
vysoký	vysoký	k2eAgInSc4d1
krevní	krevní	k2eAgInSc4d1
tlak	tlak	k1gInSc4
<g/>
,	,	kIx,
kardiovaskulární	kardiovaskulární	k2eAgNnPc1d1
onemocnění	onemocnění	k1gNnPc1
a	a	k8xC
vysoká	vysoký	k2eAgFnSc1d1
hladina	hladina	k1gFnSc1
cholesterolu	cholesterol	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strava	strava	k1gFnSc1
založená	založený	k2eAgFnSc1d1
na	na	k7c6
rostlinném	rostlinný	k2eAgInSc6d1
původu	původ	k1gInSc6
má	mít	k5eAaImIp3nS
potenciál	potenciál	k1gInSc1
ke	k	k7c3
snížení	snížení	k1gNnSc3
rizika	riziko	k1gNnSc2
srdečních	srdeční	k2eAgNnPc2d1
onemocnění	onemocnění	k1gNnPc2
<g/>
,	,	kIx,
jakož	jakož	k8xC
i	i	k9
snížení	snížení	k1gNnSc1
množství	množství	k1gNnSc1
léků	lék	k1gInPc2
předepsaných	předepsaný	k2eAgInPc2d1
v	v	k7c6
případech	případ	k1gInPc6
chronických	chronický	k2eAgFnPc2d1
onemocnění	onemocnění	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Změna	změna	k1gFnSc1
jídelníčku	jídelníček	k1gInSc2
na	na	k7c4
rostlinnou	rostlinný	k2eAgFnSc4d1
bázi	báze	k1gFnSc4
nebo	nebo	k8xC
vegetariánství	vegetariánství	k1gNnSc4
má	mít	k5eAaImIp3nS
dramaticky	dramaticky	k6eAd1
pozitivní	pozitivní	k2eAgInSc4d1
vliv	vliv	k1gInSc4
na	na	k7c4
zdraví	zdraví	k1gNnSc4
pacientů	pacient	k1gMnPc2
s	s	k7c7
chronickými	chronický	k2eAgFnPc7d1
nemocemi	nemoc	k1gFnPc7
<g/>
,	,	kIx,
mnohem	mnohem	k6eAd1
větší	veliký	k2eAgNnSc4d2
než	než	k8xS
samotné	samotný	k2eAgNnSc4d1
cvičení	cvičení	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
102	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vegetariánská	vegetariánský	k2eAgFnSc1d1
dieta	dieta	k1gFnSc1
byla	být	k5eAaImAgFnS
použita	použít	k5eAaPmNgFnS
jako	jako	k9
léčba	léčba	k1gFnSc1
revmatoidní	revmatoidní	k2eAgFnSc2d1
artritidy	artritida	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
důkazy	důkaz	k1gInPc4
<g/>
,	,	kIx,
zda	zda	k8xS
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
efektivní	efektivní	k2eAgFnSc1d1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
neprůkazné	průkazný	k2eNgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
103	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Některé	některý	k3yIgFnPc1
alternativní	alternativní	k2eAgFnPc1d1
medicíny	medicína	k1gFnPc1
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
ájurvéda	ájurvéda	k1gFnSc1
a	a	k8xC
siddha	siddha	k1gFnSc1
předepisují	předepisovat	k5eAaImIp3nP
vegetariánskou	vegetariánský	k2eAgFnSc4d1
stravu	strava	k1gFnSc4
jako	jako	k8xC,k8xS
běžný	běžný	k2eAgInSc4d1
postup	postup	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maya	Maya	k?
Tiwari	Tiwari	k1gNnSc1
konstatuje	konstatovat	k5eAaBmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
ájurvéda	ájurvéda	k1gFnSc1
doporučuje	doporučovat	k5eAaImIp3nS
malé	malý	k2eAgFnPc4d1
porce	porce	k1gFnPc4
masa	maso	k1gNnSc2
pro	pro	k7c4
některé	některý	k3yIgMnPc4
lidi	člověk	k1gMnPc4
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
"	"	kIx"
<g/>
pravidla	pravidlo	k1gNnPc1
lovu	lov	k1gInSc2
a	a	k8xC
zabíjení	zabíjení	k1gNnSc2
zvířete	zvíře	k1gNnSc2
<g/>
,	,	kIx,
praktikované	praktikovaný	k2eAgNnSc1d1
původními	původní	k2eAgMnPc7d1
obyvateli	obyvatel	k1gMnPc7
byli	být	k5eAaImAgMnP
velmi	velmi	k6eAd1
specifická	specifický	k2eAgNnPc1d1
a	a	k8xC
podrobná	podrobný	k2eAgNnPc1d1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
takové	takový	k3xDgFnPc4
metody	metoda	k1gFnPc4
lovu	lov	k1gInSc2
a	a	k8xC
zabíjení	zabíjení	k1gNnSc2
již	již	k6eAd1
nejsou	být	k5eNaImIp3nP
dodržovány	dodržovat	k5eAaImNgFnP
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
ona	onen	k3xDgNnPc4
nedoporučuje	doporučovat	k5eNaImIp3nS
používání	používání	k1gNnSc1
"	"	kIx"
<g/>
jakékoli	jakýkoli	k3yIgNnSc4
zvířecí	zvířecí	k2eAgNnSc4d1
maso	maso	k1gNnSc4
jako	jako	k8xS,k8xC
potravinu	potravina	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
ani	ani	k8xC
pro	pro	k7c4
lidi	člověk	k1gMnPc4
typu	typ	k1gInSc2
váta	vát	k5eAaImNgFnS
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
104	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Fyziologie	fyziologie	k1gFnSc1
</s>
<s>
Lidé	člověk	k1gMnPc1
jsou	být	k5eAaImIp3nP
všežravci	všežravec	k1gMnPc1
<g/>
,	,	kIx,
schopní	schopný	k2eAgMnPc1d1
konzumovat	konzumovat	k5eAaBmF
širokou	široký	k2eAgFnSc4d1
škálu	škála	k1gFnSc4
rostlinných	rostlinný	k2eAgInPc2d1
i	i	k8xC
živočišných	živočišný	k2eAgInPc2d1
materiálů	materiál	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
105	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
106	#num#	k4
<g/>
]	]	kIx)
Existují	existovat	k5eAaImIp3nP
domněnky	domněnka	k1gFnPc1
<g/>
,	,	kIx,
že	že	k8xS
raní	raný	k2eAgMnPc1d1
hominidé	hominid	k1gMnPc1
se	se	k3xPyFc4
vyvinuli	vyvinout	k5eAaPmAgMnP
do	do	k7c2
pojídačů	pojídač	k1gMnPc2
masa	maso	k1gNnSc2
v	v	k7c6
důsledku	důsledek	k1gInSc6
velkých	velký	k2eAgFnPc2d1
klimatických	klimatický	k2eAgFnPc2d1
změn	změna	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
trvaly	trvat	k5eAaImAgFnP
v	v	k7c6
době	doba	k1gFnSc6
před	před	k7c7
třemi	tři	k4xCgInPc7
až	až	k9
čtyřmi	čtyři	k4xCgInPc7
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
pralesy	prales	k1gInPc1
a	a	k8xC
džungle	džungle	k1gFnPc1
vyschly	vyschnout	k5eAaPmAgFnP
a	a	k8xC
staly	stát	k5eAaPmAgFnP
se	se	k3xPyFc4
otevřenými	otevřený	k2eAgFnPc7d1
pastvinami	pastvina	k1gFnPc7
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
otevřelo	otevřít	k5eAaPmAgNnS
příležitosti	příležitost	k1gFnSc6
k	k	k7c3
lovu	lov	k1gInSc3
a	a	k8xC
mrchožroutství	mrchožroutství	k1gNnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
107	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
108	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Poruchy	poruch	k1gInPc1
příjmu	příjem	k1gInSc2
potravy	potrava	k1gFnSc2
</s>
<s>
Americká	americký	k2eAgFnSc1d1
dietetická	dietetický	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
naznačuje	naznačovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
vegetariánské	vegetariánský	k2eAgFnPc1d1
diety	dieta	k1gFnPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
častější	častý	k2eAgFnPc1d2
mezi	mezi	k7c4
adolescenty	adolescent	k1gMnPc4
s	s	k7c7
poruchami	porucha	k1gFnPc7
příjmu	příjem	k1gInSc2
potravy	potrava	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
důkazy	důkaz	k1gInPc1
naznačují	naznačovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
samotné	samotný	k2eAgNnSc1d1
přijetí	přijetí	k1gNnSc1
vegetariánské	vegetariánský	k2eAgFnSc2d1
stravy	strava	k1gFnSc2
nevede	vést	k5eNaImIp3nS
k	k	k7c3
poruchám	porucha	k1gFnPc3
příjmu	příjem	k1gInSc2
potravy	potrava	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
spíše	spíše	k9
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
vegetariánská	vegetariánský	k2eAgFnSc1d1
strava	strava	k1gFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
vybrána	vybrat	k5eAaPmNgFnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
maskovala	maskovat	k5eAaBmAgFnS
existující	existující	k2eAgFnPc4d1
poruchy	porucha	k1gFnPc4
příjmu	příjem	k1gInSc2
potravy	potrava	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
109	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
110	#num#	k4
<g/>
]	]	kIx)
Další	další	k2eAgFnSc1d1
studie	studie	k1gFnSc1
a	a	k8xC
prohlášení	prohlášení	k1gNnSc1
dietiků	dietik	k1gMnPc2
a	a	k8xC
poradců	poradce	k1gMnPc2
podporují	podporovat	k5eAaImIp3nP
tento	tento	k3xDgInSc4
závěr	závěr	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
111	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Etika	etika	k1gFnSc1
a	a	k8xC
strava	strava	k1gFnSc1
</s>
<s>
Pro	pro	k7c4
vegetariánství	vegetariánství	k1gNnSc4
se	se	k3xPyFc4
uvádějí	uvádět	k5eAaImIp3nP
různé	různý	k2eAgInPc1d1
etické	etický	k2eAgInPc1d1
důvody	důvod	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
jsou	být	k5eAaImIp3nP
obvykle	obvykle	k6eAd1
jsou	být	k5eAaImIp3nP
založeny	založit	k5eAaPmNgFnP
na	na	k7c6
zájmu	zájem	k1gInSc6
o	o	k7c4
živočichy	živočich	k1gMnPc4
odlišné	odlišný	k2eAgInPc1d1
od	od	k7c2
člověka	člověk	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
mnoha	mnoho	k4c6
společnostech	společnost	k1gFnPc6
se	se	k3xPyFc4
objevují	objevovat	k5eAaImIp3nP
spory	spor	k1gInPc1
a	a	k8xC
debaty	debata	k1gFnPc1
nad	nad	k7c7
etikou	etika	k1gFnSc7
pojídání	pojídání	k1gNnPc2
zvířat	zvíře	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
lidé	člověk	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
nejsou	být	k5eNaImIp3nP
vegetariány	vegetarián	k1gMnPc7
<g/>
,	,	kIx,
odmítají	odmítat	k5eAaImIp3nP
jíst	jíst	k5eAaImF
maso	maso	k1gNnSc4
některých	některý	k3yIgNnPc2
zvířat	zvíře	k1gNnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
kočky	kočka	k1gFnPc1
<g/>
,	,	kIx,
psi	pes	k1gMnPc1
<g/>
,	,	kIx,
koně	kůň	k1gMnPc1
nebo	nebo	k8xC
králíci	králík	k1gMnPc1
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
kulturním	kulturní	k2eAgNnPc3d1
tabu	tabu	k1gNnPc3
<g/>
;	;	kIx,
známé	známý	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
zákazy	zákaz	k1gInPc1
pojídání	pojídání	k1gNnSc2
hovězího	hovězí	k1gNnSc2
masa	maso	k1gNnSc2
u	u	k7c2
hinduistů	hinduista	k1gMnPc2
a	a	k8xC
vepřového	vepřové	k1gNnSc2
u	u	k7c2
muslimů	muslim	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiní	jiný	k1gMnPc1
obhajují	obhajovat	k5eAaImIp3nP
konzumaci	konzumace	k1gFnSc4
masa	maso	k1gNnPc4
z	z	k7c2
vědeckých	vědecký	k2eAgInPc2d1
<g/>
,	,	kIx,
výživových	výživový	k2eAgInPc2d1
a	a	k8xC
kulturních	kulturní	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
těch	ten	k3xDgFnPc2
náboženských	náboženský	k2eAgFnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
masožravci	masožravec	k1gMnPc1
se	se	k3xPyFc4
vyhýbají	vyhýbat	k5eAaImIp3nP
masu	masa	k1gFnSc4
ze	z	k7c2
zvířat	zvíře	k1gNnPc2
chovaných	chovaný	k2eAgMnPc2d1
v	v	k7c6
určitých	určitý	k2eAgFnPc6d1
podmínkách	podmínka	k1gFnPc6
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
například	například	k6eAd1
velkochovy	velkochov	k1gInPc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
se	se	k3xPyFc4
vyhýbají	vyhýbat	k5eAaImIp3nP
určitým	určitý	k2eAgFnPc3d1
potravinám	potravina	k1gFnPc3
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
například	například	k6eAd1
telecí	telecí	k2eAgFnSc1d1
nebo	nebo	k8xC
foie	foie	k6eAd1
gras	gras	k6eAd1
(	(	kIx(
<g/>
ztučnělá	ztučnělý	k2eAgNnPc4d1
játra	játra	k1gNnPc4
vodních	vodní	k2eAgMnPc2d1
ptáků	pták	k1gMnPc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Někteří	některý	k3yIgMnPc1
lidé	člověk	k1gMnPc1
dodržují	dodržovat	k5eAaImIp3nP
vegetariánskou	vegetariánský	k2eAgFnSc4d1
či	či	k8xC
veganskou	veganský	k2eAgFnSc4d1
stravu	strava	k1gFnSc4
ne	ne	k9
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
mají	mít	k5eAaImIp3nP
morální	morální	k2eAgFnPc1d1
obavy	obava	k1gFnPc1
zahrnující	zahrnující	k2eAgFnPc1d1
chov	chov	k1gInSc4
nebo	nebo	k8xC
spotřebu	spotřeba	k1gFnSc4
zvířat	zvíře	k1gNnPc2
obecně	obecně	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
kvůli	kvůli	k7c3
obavám	obava	k1gFnPc3
o	o	k7c4
určitý	určitý	k2eAgInSc4d1
způsob	způsob	k1gInSc4
úpravy	úprava	k1gFnSc2
a	a	k8xC
praxe	praxe	k1gFnSc2
zahrnující	zahrnující	k2eAgInSc4d1
chov	chov	k1gInSc4
a	a	k8xC
porážku	porážka	k1gFnSc4
zvířat	zvíře	k1gNnPc2
<g/>
,	,	kIx,
tedy	tedy	k9
velkochovy	velkochov	k1gInPc4
a	a	k8xC
industrializaci	industrializace	k1gFnSc4
porážek	porážka	k1gFnPc2
zvířat	zvíře	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiní	jiný	k2eAgMnPc1d1
se	se	k3xPyFc4
trvale	trvale	k6eAd1
vyhýbají	vyhýbat	k5eAaImIp3nP
masu	masa	k1gFnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
produkce	produkce	k1gFnSc1
masa	maso	k1gNnSc2
má	mít	k5eAaImIp3nS
větší	veliký	k2eAgFnSc4d2
zátěž	zátěž	k1gFnSc4
na	na	k7c4
životní	životní	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
než	než	k8xS
zajištění	zajištění	k1gNnSc4
ekvivalentního	ekvivalentní	k2eAgNnSc2d1
množství	množství	k1gNnSc2
rostlinných	rostlinný	k2eAgFnPc2d1
bílkovin	bílkovina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Etické	etický	k2eAgFnPc1d1
námitky	námitka	k1gFnPc1
založené	založený	k2eAgFnPc1d1
na	na	k7c6
úctě	úcta	k1gFnSc6
ke	k	k7c3
zvířatům	zvíře	k1gNnPc3
jdou	jít	k5eAaImIp3nP
rozdělit	rozdělit	k5eAaPmF
na	na	k7c4
odpor	odpor	k1gInSc4
k	k	k7c3
aktu	akt	k1gInSc3
zabíjení	zabíjení	k1gNnSc2
obecně	obecně	k6eAd1
<g/>
,	,	kIx,
a	a	k8xC
na	na	k7c4
odpor	odpor	k1gInSc4
k	k	k7c3
některým	některý	k3yIgFnPc3
zemědělským	zemědělský	k2eAgFnPc3d1
praktikám	praktika	k1gFnPc3
spojených	spojený	k2eAgInPc2d1
s	s	k7c7
produkcí	produkce	k1gFnSc7
masa	maso	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Etika	etika	k1gFnSc1
zabíjení	zabíjení	k1gNnSc2
pro	pro	k7c4
jídlo	jídlo	k1gNnSc4
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Bioetika	Bioetika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Peter	Peter	k1gMnSc1
Singer	Singer	k1gMnSc1
<g/>
,	,	kIx,
profesor	profesor	k1gMnSc1
Princetonské	Princetonský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
a	a	k8xC
zakladatel	zakladatel	k1gMnSc1
hnutí	hnutí	k1gNnSc2
za	za	k7c2
práva	právo	k1gNnSc2
zvířat	zvíře	k1gNnPc2
<g/>
,	,	kIx,
věří	věřit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
existují	existovat	k5eAaImIp3nP
<g/>
-li	-li	k?
alternativní	alternativní	k2eAgInPc4d1
prostředky	prostředek	k1gInPc4
k	k	k7c3
přežití	přežití	k1gNnSc3
<g/>
,	,	kIx,
každý	každý	k3xTgInSc4
by	by	kYmCp3nS
měl	mít	k5eAaImAgInS
zvolit	zvolit	k5eAaPmF
tu	ten	k3xDgFnSc4
možnost	možnost	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
nezpůsobuje	způsobovat	k5eNaImIp3nS
zvířatům	zvíře	k1gNnPc3
zbytečnou	zbytečný	k2eAgFnSc4d1
újmu	újma	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
etických	etický	k2eAgMnPc2d1
vegetariánů	vegetarián	k1gMnPc2
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jsou	být	k5eAaImIp3nP
proti	proti	k7c3
zabíjení	zabíjení	k1gNnSc3
zvířat	zvíře	k1gNnPc2
k	k	k7c3
jídlu	jídlo	k1gNnSc3
ze	z	k7c2
stejných	stejný	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
jako	jako	k8xC,k8xS
proti	proti	k7c3
zabíjení	zabíjení	k1gNnSc3
lidí	člověk	k1gMnPc2
k	k	k7c3
jídlu	jídlo	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Singer	Singra	k1gFnPc2
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
knize	kniha	k1gFnSc6
Osvobození	osvobození	k1gNnSc1
zvířat	zvíře	k1gNnPc2
uvádí	uvádět	k5eAaImIp3nS
možné	možný	k2eAgFnPc4d1
schopnosti	schopnost	k1gFnPc4
vnímání	vnímání	k1gNnSc2
existující	existující	k2eAgFnPc4d1
u	u	k7c2
jiných	jiný	k2eAgFnPc2d1
než	než	k8xS
lidských	lidský	k2eAgFnPc2d1
bytosti	bytost	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
dávají	dávat	k5eAaImIp3nP
dostatečný	dostatečný	k2eAgInSc4d1
podklad	podklad	k1gInSc4
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
k	k	k7c3
nim	on	k3xPp3gFnPc3
přistupovalo	přistupovat	k5eAaImAgNnS
podle	podle	k7c2
utilitaristické	utilitaristický	k2eAgFnSc2d1
etiky	etika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
tento	tento	k3xDgInSc4
argument	argument	k1gInSc4
široce	široko	k6eAd1
odkazují	odkazovat	k5eAaImIp3nP
aktivisté	aktivista	k1gMnPc1
za	za	k7c2
práva	právo	k1gNnSc2
zvířat	zvíře	k1gNnPc2
a	a	k8xC
vegetariáni	vegetarián	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Etičtí	etický	k2eAgMnPc1d1
vegetariáni	vegetarián	k1gMnPc1
také	také	k9
věří	věřit	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
zabití	zabití	k1gNnSc1
zvířete	zvíře	k1gNnSc2
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
zabití	zabití	k1gNnSc1
člověka	člověk	k1gMnSc2
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
odůvodněno	odůvodnit	k5eAaPmNgNnS
jen	jen	k9
v	v	k7c6
krajních	krajní	k2eAgInPc6d1
případech	případ	k1gInPc6
a	a	k8xC
že	že	k8xS
konzumace	konzumace	k1gFnSc1
živého	živý	k2eAgMnSc2d1
tvora	tvor	k1gMnSc2
pro	pro	k7c4
jeho	jeho	k3xOp3gFnSc4
příjemnější	příjemný	k2eAgFnSc4d2
chuť	chuť	k1gFnSc4
<g/>
,	,	kIx,
pohodlí	pohodlí	k1gNnSc4
nebo	nebo	k8xC
výživové	výživový	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
není	být	k5eNaImIp3nS
dostatečným	dostatečný	k2eAgInSc7d1
důvodem	důvod	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc1d1
rozšířený	rozšířený	k2eAgInSc1d1
názor	názor	k1gInSc1
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
lidé	člověk	k1gMnPc1
si	se	k3xPyFc3
jsou	být	k5eAaImIp3nP
morálně	morálně	k6eAd1
vědomi	vědom	k2eAgMnPc1d1
takového	takový	k3xDgNnSc2
svého	svůj	k3xOyFgNnSc2
chování	chování	k1gNnSc2
<g/>
,	,	kIx,
kterého	který	k3yQgMnSc4,k3yIgMnSc4,k3yRgMnSc4
ostatní	ostatní	k2eAgNnPc1d1
zvířata	zvíře	k1gNnPc1
nejsou	být	k5eNaImIp3nP
schopni	schopen	k2eAgMnPc1d1
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
podléhají	podléhat	k5eAaImIp3nP
přísnějším	přísný	k2eAgFnPc3d2
normám	norma	k1gFnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
112	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odpůrci	odpůrce	k1gMnPc1
etického	etický	k2eAgNnSc2d1
vegetariánství	vegetariánství	k1gNnSc2
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
zvířata	zvíře	k1gNnPc1
nejsou	být	k5eNaImIp3nP
morálně	morálně	k6eAd1
rovná	rovnat	k5eAaImIp3nS
člověku	člověk	k1gMnSc3
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
považují	považovat	k5eAaImIp3nP
za	za	k7c4
mylné	mylný	k2eAgNnSc4d1
srovnávat	srovnávat	k5eAaImF
pojídání	pojídání	k1gNnSc2
dobytka	dobytek	k1gInSc2
se	s	k7c7
zabíjením	zabíjení	k1gNnSc7
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
pohled	pohled	k1gInSc4
neomlouvá	omlouvat	k5eNaImIp3nS
krutost	krutost	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
zvířata	zvíře	k1gNnPc1
nemají	mít	k5eNaImIp3nP
práva	právo	k1gNnPc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc4,k3yRgNnSc4,k3yQgNnSc4
má	mít	k5eAaImIp3nS
člověk	člověk	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
113	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mléko	mléko	k1gNnSc1
a	a	k8xC
vejce	vejce	k1gNnSc1
</s>
<s>
Jedním	jeden	k4xCgInSc7
z	z	k7c2
hlavních	hlavní	k2eAgInPc2d1
rozdílů	rozdíl	k1gInPc2
mezi	mezi	k7c7
vegany	vegan	k1gMnPc7
a	a	k8xC
typickou	typický	k2eAgFnSc7d1
vegetariánskou	vegetariánský	k2eAgFnSc7d1
stravou	strava	k1gFnSc7
je	být	k5eAaImIp3nS
vyhýbání	vyhýbání	k1gNnSc1
se	se	k3xPyFc4
vejcím	vejce	k1gNnPc3
a	a	k8xC
mléčným	mléčný	k2eAgInPc3d1
výrobkům	výrobek	k1gInPc3
jako	jako	k9
je	být	k5eAaImIp3nS
mléko	mléko	k1gNnSc1
<g/>
,	,	kIx,
sýr	sýr	k1gInSc1
<g/>
,	,	kIx,
máslo	máslo	k1gNnSc1
a	a	k8xC
jogurt	jogurt	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Etičtí	etický	k2eAgMnPc1d1
vegani	vegan	k1gMnPc1
nekonzumují	konzumovat	k5eNaBmIp3nP
mléčné	mléčný	k2eAgInPc4d1
výrobky	výrobek	k1gInPc4
a	a	k8xC
vejce	vejce	k1gNnPc4
<g/>
,	,	kIx,
protože	protože	k8xS
uvádějí	uvádět	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
jejich	jejich	k3xOp3gFnSc1
výroba	výroba	k1gFnSc1
způsobuje	způsobovat	k5eAaImIp3nS
zvířatům	zvíře	k1gNnPc3
utrpení	utrpení	k1gNnSc4
nebo	nebo	k8xC
předčasnou	předčasný	k2eAgFnSc4d1
smrt	smrt	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
114	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Při	při	k7c6
průmyslové	průmyslový	k2eAgFnSc6d1
produkci	produkce	k1gFnSc6
mléka	mléko	k1gNnSc2
jsou	být	k5eAaImIp3nP
telata	tele	k1gNnPc4
oddělena	oddělen	k2eAgNnPc4d1
od	od	k7c2
svých	svůj	k3xOyFgFnPc2
matek	matka	k1gFnPc2
brzy	brzy	k6eAd1
po	po	k7c6
narození	narození	k1gNnSc6
a	a	k8xC
buď	buď	k8xC
poražena	poražen	k2eAgFnSc1d1
<g/>
,	,	kIx,
nebo	nebo	k8xC
krmena	krmen	k2eAgFnSc1d1
mléčnou	mléčný	k2eAgFnSc7d1
náhražkou	náhražka	k1gFnSc7
za	za	k7c7
účelem	účel	k1gInSc7
zachování	zachování	k1gNnSc2
kravského	kravský	k2eAgNnSc2d1
mléka	mléko	k1gNnSc2
určeného	určený	k2eAgInSc2d1
k	k	k7c3
lidské	lidský	k2eAgFnSc3d1
spotřebě	spotřeba	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
115	#num#	k4
<g/>
]	]	kIx)
Vegani	Vegan	k1gMnPc1
konstatují	konstatovat	k5eAaBmIp3nP
<g/>
,	,	kIx,
že	že	k8xS
to	ten	k3xDgNnSc1
porušuje	porušovat	k5eAaImIp3nS
přirozenou	přirozený	k2eAgFnSc4d1
vazbu	vazba	k1gFnSc4
mezi	mezi	k7c7
matkou	matka	k1gFnSc7
a	a	k8xC
<g />
.	.	kIx.
</s>
<s hack="1">
teletem	tele	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
115	#num#	k4
<g/>
]	]	kIx)
Nechtění	chtěný	k2eNgMnPc1d1
býčci	býček	k1gMnPc1
jsou	být	k5eAaImIp3nP
buď	buď	k8xC
poraženi	porazit	k5eAaPmNgMnP
hned	hned	k6eAd1
po	po	k7c6
narození	narození	k1gNnSc1
nebo	nebo	k8xC
vykrmeni	vykrmen	k2eAgMnPc1d1
na	na	k7c4
telecí	telecí	k2eAgNnSc4d1
maso	maso	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
115	#num#	k4
<g/>
]	]	kIx)
Aby	aby	kYmCp3nS
se	se	k3xPyFc4
prodloužila	prodloužit	k5eAaPmAgFnS
laktace	laktace	k1gFnSc1
<g/>
,	,	kIx,
dojnice	dojnice	k1gFnPc1
jsou	být	k5eAaImIp3nP
pomocí	pomocí	k7c2
inseminace	inseminace	k1gFnSc2
udržovány	udržován	k2eAgFnPc1d1
téměř	téměř	k6eAd1
trvale	trvale	k6eAd1
březí	březí	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
115	#num#	k4
<g/>
]	]	kIx)
Asi	asi	k9
po	po	k7c6
pěti	pět	k4xCc6
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
jakmile	jakmile	k8xS
produkce	produkce	k1gFnSc1
mléka	mléko	k1gNnSc2
z	z	k7c2
krávy	kráva	k1gFnSc2
poklesne	poklesnout	k5eAaPmIp3nS
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
považovány	považován	k2eAgInPc1d1
za	za	k7c4
„	„	k?
<g/>
vyčerpané	vyčerpaný	k2eAgNnSc1d1
<g/>
“	“	k?
a	a	k8xC
jsou	být	k5eAaImIp3nP
poslány	poslat	k5eAaPmNgInP
na	na	k7c4
porážku	porážka	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
z	z	k7c2
nich	on	k3xPp3gMnPc2
získá	získat	k5eAaPmIp3nS
hovězí	hovězí	k2eAgNnSc1d1
maso	maso	k1gNnSc1
a	a	k8xC
kůže	kůže	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přirozená	přirozený	k2eAgFnSc1d1
délka	délka	k1gFnSc1
života	život	k1gInSc2
dojnice	dojnice	k1gFnSc2
je	být	k5eAaImIp3nS
asi	asi	k9
dvacet	dvacet	k4xCc4
roků	rok	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
114	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Při	při	k7c6
produkci	produkce	k1gFnSc6
vajec	vejce	k1gNnPc2
v	v	k7c6
klecích	klec	k1gFnPc6
i	i	k8xC
ve	v	k7c6
volném	volný	k2eAgInSc6d1
výběhu	výběh	k1gInSc6
jsou	být	k5eAaImIp3nP
nechtěná	chtěný	k2eNgNnPc1d1
samčí	samčí	k2eAgNnPc1d1
kuřata	kuře	k1gNnPc1
krátce	krátce	k6eAd1
po	po	k7c6
vylíhnutí	vylíhnutí	k1gNnSc6
zlikvidována	zlikvidován	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
116	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zacházení	zacházení	k1gNnSc1
se	s	k7c7
zvířaty	zvíře	k1gNnPc7
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Práva	právo	k1gNnSc2
zvířat	zvíře	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Etické	etický	k2eAgNnSc1d1
vegetariánství	vegetariánství	k1gNnSc1
si	se	k3xPyFc3
získalo	získat	k5eAaPmAgNnS
popularitu	popularita	k1gFnSc4
ve	v	k7c6
vyspělých	vyspělý	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
,	,	kIx,
zejména	zejména	k9
kvůli	kvůli	k7c3
rozšíření	rozšíření	k1gNnSc3
velkochovů	velkochov	k1gInPc2
<g/>
,	,	kIx,
rychlejší	rychlý	k2eAgFnSc3d2
komunikaci	komunikace	k1gFnSc3
a	a	k8xC
nárůstu	nárůst	k1gInSc3
povědomí	povědomí	k1gNnSc2
o	o	k7c6
životním	životní	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
lidé	člověk	k1gMnPc1
se	se	k3xPyFc4
domnívají	domnívat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
současná	současný	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
poptávky	poptávka	k1gFnSc2
po	po	k7c6
mase	masa	k1gFnSc6
nemůže	moct	k5eNaImIp3nS
být	být	k5eAaImF
splněna	splnit	k5eAaPmNgFnS
bez	bez	k7c2
masové	masový	k2eAgFnSc2d1
produkce	produkce	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
ignoruje	ignorovat	k5eAaImIp3nS
zacházení	zacházení	k1gNnSc4
se	s	k7c7
zvířaty	zvíře	k1gNnPc7
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
jiní	jiný	k2eAgMnPc1d1
věří	věřit	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
praktiky	praktika	k1gFnPc1
jako	jako	k9
dobře	dobře	k6eAd1
řízený	řízený	k2eAgInSc1d1
volný	volný	k2eAgInSc1d1
výběh	výběh	k1gInSc1
a	a	k8xC
porážka	porážka	k1gFnSc1
zvěře	zvěř	k1gFnSc2
<g/>
,	,	kIx,
zejména	zejména	k9
z	z	k7c2
druhů	druh	k1gInPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnPc1
přírodní	přírodní	k2eAgMnPc1d1
predátoři	predátor	k1gMnPc1
byli	být	k5eAaImAgMnP
významně	významně	k6eAd1
eliminováni	eliminován	k2eAgMnPc1d1
<g/>
,	,	kIx,
mohou	moct	k5eAaImIp3nP
podstatně	podstatně	k6eAd1
zmírnit	zmírnit	k5eAaPmF
požadavek	požadavek	k1gInSc4
na	na	k7c4
masovou	masový	k2eAgFnSc4d1
produkci	produkce	k1gFnSc4
masa	maso	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Klasická	klasický	k2eAgFnSc1d1
řecká	řecký	k2eAgFnSc1d1
a	a	k8xC
římská	římský	k2eAgFnSc1d1
filozofie	filozofie	k1gFnSc1
</s>
<s>
Rubensův	Rubensův	k2eAgInSc1d1
obraz	obraz	k1gInSc1
Pythagoras	Pythagoras	k1gMnSc1
obhajuje	obhajovat	k5eAaImIp3nS
vegetariánství	vegetariánství	k1gNnSc4
</s>
<s>
Starověká	starověký	k2eAgFnSc1d1
řecká	řecký	k2eAgFnSc1d1
filozofie	filozofie	k1gFnSc1
měla	mít	k5eAaImAgFnS
dlouhou	dlouhý	k2eAgFnSc4d1
tradici	tradice	k1gFnSc4
vegetariánství	vegetariánství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pythagoras	Pythagoras	k1gMnSc1
byl	být	k5eAaImAgMnS
údajně	údajně	k6eAd1
vegetarián	vegetarián	k1gMnSc1
(	(	kIx(
<g/>
a	a	k8xC
studoval	studovat	k5eAaImAgMnS
na	na	k7c6
hoře	hora	k1gFnSc6
Karmel	Karmela	k1gFnPc2
<g/>
,	,	kIx,
o	o	k7c4
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
někteří	některý	k3yIgMnPc1
historici	historik	k1gMnPc1
tvrdí	tvrdit	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
hostila	hostit	k5eAaImAgFnS
vegetariánskou	vegetariánský	k2eAgFnSc4d1
komunitu	komunita	k1gFnSc4
<g/>
)	)	kIx)
a	a	k8xC
očekávalo	očekávat	k5eAaImAgNnS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gMnPc1
následovníci	následovník	k1gMnPc1
budou	být	k5eAaImBp3nP
také	také	k9
vegetariáni	vegetarián	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
117	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římský	římský	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
Ovidius	Ovidius	k1gMnSc1
uzavřel	uzavřít	k5eAaPmAgMnS
své	svůj	k3xOyFgNnSc4
životní	životní	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
Proměny	proměna	k1gFnSc2
částečně	částečně	k6eAd1
vzrušenou	vzrušený	k2eAgFnSc7d1
argumentací	argumentace	k1gFnSc7
(	(	kIx(
<g/>
pronesenou	pronesený	k2eAgFnSc7d1
postavou	postava	k1gFnSc7
Pythagora	Pythagoras	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
zájmu	zájem	k1gInSc6
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
lidstvo	lidstvo	k1gNnSc1
změnilo	změnit	k5eAaPmAgNnS
v	v	k7c4
lepší	dobrý	k2eAgInSc4d2
<g/>
,	,	kIx,
harmonický	harmonický	k2eAgInSc4d1
druh	druh	k1gInSc4
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nS
usilovat	usilovat	k5eAaImF
o	o	k7c4
humánnější	humánní	k2eAgInPc4d2
sklony	sklon	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vegetariánství	vegetariánství	k1gNnPc2
uvedl	uvést	k5eAaPmAgMnS
jako	jako	k9
zásadní	zásadní	k2eAgNnSc4d1
rozhodnutí	rozhodnutí	k1gNnSc4
v	v	k7c6
této	tento	k3xDgFnSc6
proměně	proměna	k1gFnSc6
<g/>
,	,	kIx,
vysvětluje	vysvětlovat	k5eAaImIp3nS
své	svůj	k3xOyFgNnSc4
přesvědčení	přesvědčení	k1gNnSc4
<g/>
,	,	kIx,
že	že	k8xS
lidský	lidský	k2eAgInSc4d1
život	život	k1gInSc4
a	a	k8xC
život	život	k1gInSc4
zvířat	zvíře	k1gNnPc2
jsou	být	k5eAaImIp3nP
tak	tak	k6eAd1
propletené	propletený	k2eAgFnPc1d1
<g/>
,	,	kIx,
že	že	k8xS
zabití	zabití	k1gNnSc1
zvířete	zvíře	k1gNnSc2
je	být	k5eAaImIp3nS
prakticky	prakticky	k6eAd1
stejné	stejný	k2eAgNnSc1d1
jako	jako	k8xC,k8xS
zabití	zabití	k1gNnSc1
bližního	bližní	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
</s>
<s>
Všecko	všecek	k3xTgNnSc1
se	se	k3xPyFc4
stále	stále	k6eAd1
mění	měnit	k5eAaImIp3nS
<g/>
,	,	kIx,
nic	nic	k3yNnSc1
nezhyne	zhynout	k5eNaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bloudě	bloudit	k5eAaImSgInS
přichází	přicházet	k5eAaImIp3nS
odtamtud	odtamtud	k6eAd1
duch	duch	k1gMnSc1
sem	sem	k6eAd1
<g/>
,	,	kIx,
tam	tam	k6eAd1
odtud	odtud	k6eAd1
<g/>
,	,	kIx,
pak	pak	k6eAd1
zase	zase	k9
jinde	jinde	k6eAd1
v	v	k7c4
údy	úd	k1gMnPc4
vniká	vnikat	k5eAaImIp3nS
<g/>
,	,	kIx,
teď	teď	k6eAd1
ze	z	k7c2
zvířat	zvíře	k1gNnPc2
lidská	lidský	k2eAgNnPc1d1
těla	tělo	k1gNnPc1
vchází	vcházet	k5eAaImIp3nP
<g/>
,	,	kIx,
do	do	k7c2
zvířat	zvíře	k1gNnPc2
pak	pak	k6eAd1
z	z	k7c2
nás	my	k3xPp1nPc2
a	a	k8xC
nepodléhá	podléhat	k5eNaImIp3nS
smrti	smrt	k1gFnSc3
nikdy	nikdy	k6eAd1
<g/>
…	…	k?
Nuž	nuž	k9
<g/>
,	,	kIx,
by	by	kYmCp3nS
nepodlehl	podlehnout	k5eNaPmAgInS
cit	cit	k1gInSc4
lidský	lidský	k2eAgInSc4d1
žádosti	žádost	k1gFnSc3
žaludku	žaludek	k1gInSc2
<g/>
,	,	kIx,
ustaňtež	ustaňtež	k1gFnSc1
<g/>
,	,	kIx,
hlásám	hlásat	k5eAaImIp1nS
<g/>
,	,	kIx,
duše	duše	k1gFnSc1
příbuzné	příbuzná	k1gFnSc2
z	z	k7c2
těla	tělo	k1gNnSc2
mrzkou	mrzký	k2eAgFnSc4d1
vyplašovat	vyplašovat	k5eAaImF
vraždou	vražda	k1gFnSc7
a	a	k8xC
krví	krvit	k5eAaImIp3nS
krev	krev	k1gFnSc4
dále	daleko	k6eAd2
neživte	živit	k5eNaImRp2nP
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Ovidius	Ovidius	k1gMnSc1
<g/>
:	:	kIx,
Proměny	proměna	k1gFnPc1
<g/>
,	,	kIx,
kniha	kniha	k1gFnSc1
XV	XV	kA
<g/>
[	[	kIx(
<g/>
118	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Náboženství	náboženství	k1gNnSc1
a	a	k8xC
dieta	dieta	k1gFnSc1
</s>
<s>
Indická	indický	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
nabízí	nabízet	k5eAaImIp3nS
širokou	široký	k2eAgFnSc4d1
škálu	škála	k1gFnSc4
vegetariánských	vegetariánský	k2eAgFnPc2d1
lahůdek	lahůdka	k1gFnPc2
<g/>
,	,	kIx,
protože	protože	k8xS
dvě	dva	k4xCgFnPc1
hlavní	hlavní	k2eAgFnPc1d1
sekty	sekta	k1gFnPc1
hinduismu	hinduismus	k1gInSc2
cvičí	cvičit	k5eAaImIp3nP
většinou	většinou	k6eAd1
obyvatel	obyvatel	k1gMnPc2
Indie	Indie	k1gFnSc2
v	v	k7c6
podpoře	podpora	k1gFnSc6
vegetariánské	vegetariánský	k2eAgFnSc2d1
stravy	strava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
je	být	k5eAaImIp3nS
ukázaná	ukázaný	k2eAgFnSc1d1
vegetariánská	vegetariánský	k2eAgFnSc1d1
thali	thale	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Džinismus	Džinismus	k1gInSc1
učí	učit	k5eAaImIp3nS
o	o	k7c6
vegetariánství	vegetariánství	k1gNnSc6
jako	jako	k8xS,k8xC
o	o	k7c6
morálním	morální	k2eAgNnSc6d1
chování	chování	k1gNnSc6
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k9
to	ten	k3xDgNnSc4
dělají	dělat	k5eAaImIp3nP
některé	některý	k3yIgInPc4
hlavní	hlavní	k2eAgInPc4d1
<g/>
[	[	kIx(
<g/>
119	#num#	k4
<g/>
]	]	kIx)
skupiny	skupina	k1gFnSc2
v	v	k7c6
hinduismu	hinduismus	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Buddhismus	buddhismus	k1gInSc1
obecně	obecně	k6eAd1
nezakazuje	zakazovat	k5eNaImIp3nS
konzumaci	konzumace	k1gFnSc4
masa	maso	k1gNnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
mahájánský	mahájánský	k2eAgInSc1d1
buddhismus	buddhismus	k1gInSc1
podporuje	podporovat	k5eAaImIp3nS
vegetariánství	vegetariánství	k1gNnSc4
jako	jako	k8xC,k8xS
příznivé	příznivý	k2eAgNnSc4d1
pro	pro	k7c4
rozvoj	rozvoj	k1gInSc4
soucitu	soucit	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
120	#num#	k4
<g/>
]	]	kIx)
Mezi	mezi	k7c4
další	další	k2eAgNnPc4d1
náboženská	náboženský	k2eAgNnPc4d1
vyznání	vyznání	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
podporují	podporovat	k5eAaImIp3nP
vegetariánskou	vegetariánský	k2eAgFnSc4d1
stravu	strava	k1gFnSc4
<g/>
,	,	kIx,
patří	patřit	k5eAaImIp3nP
Adventisté	adventista	k1gMnPc1
sedmého	sedmý	k4xOgInSc2
dne	den	k1gInSc2
<g/>
,	,	kIx,
hnutí	hnutí	k1gNnSc2
Rastafari	Rastafari	k1gNnSc2
<g/>
,	,	kIx,
hnutí	hnutí	k1gNnSc2
Ananda	Anand	k1gMnSc4
Marga	Marg	k1gMnSc4
a	a	k8xC
Hare	Hare	k1gFnSc4
Krišna	Krišna	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sikhismus	Sikhismus	k1gInSc1
<g/>
[	[	kIx(
<g/>
121	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
122	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
123	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
124	#num#	k4
<g/>
]	]	kIx)
neporovnává	porovnávat	k5eNaImIp3nS
spiritualitu	spiritualita	k1gFnSc4
se	s	k7c7
stravou	strava	k1gFnSc7
a	a	k8xC
neurčuje	určovat	k5eNaImIp3nS
<g/>
,	,	kIx,
zdali	zdali	k8xS
strava	strava	k1gFnSc1
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
vegetariánská	vegetariánský	k2eAgFnSc1d1
nebo	nebo	k8xC
masitá	masitý	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
125	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bahá	Bahat	k5eAaImIp3nS,k5eAaPmIp3nS,k5eAaBmIp3nS
<g/>
'	'	kIx"
<g/>
í	í	k0
</s>
<s>
I	i	k9
když	když	k8xS
v	v	k7c6
Bahá	Bahá	k1gFnSc1
<g/>
'	'	kIx"
<g/>
í	í	k0
nejsou	být	k5eNaImIp3nP
žádné	žádný	k3yNgNnSc4
dietní	dietní	k2eAgNnSc4d1
omezení	omezení	k1gNnSc4
<g/>
,	,	kIx,
tak	tak	k9
'	'	kIx"
<g/>
Abdu	Abda	k1gFnSc4
<g/>
'	'	kIx"
<g/>
l-Bahá	l-Bahý	k2eAgFnSc1d1
<g/>
,	,	kIx,
syn	syn	k1gMnSc1
zakladatele	zakladatel	k1gMnSc2
náboženství	náboženství	k1gNnSc2
<g/>
,	,	kIx,
poznamenal	poznamenat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
vegetariánská	vegetariánský	k2eAgFnSc1d1
strava	strava	k1gFnSc1
skládající	skládající	k2eAgFnSc1d1
se	se	k3xPyFc4
z	z	k7c2
ovoce	ovoce	k1gNnSc2
a	a	k8xC
obilovin	obilovina	k1gFnPc2
by	by	kYmCp3nP
byla	být	k5eAaImAgFnS
žádoucí	žádoucí	k2eAgFnSc1d1
<g/>
,	,	kIx,
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
pro	pro	k7c4
lidi	člověk	k1gMnPc4
se	se	k3xPyFc4
<g />
.	.	kIx.
</s>
<s hack="1">
slabým	slabý	k2eAgInSc7d1
kořínkem	kořínek	k1gInSc7
nebo	nebo	k8xC
nemocné	nemocný	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
126	#num#	k4
<g/>
]	]	kIx)
Prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
neexistují	existovat	k5eNaImIp3nP
žádné	žádný	k3yNgInPc4
požadavky	požadavek	k1gInPc4
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
bahá	bahá	k1gFnSc1
<g/>
'	'	kIx"
<g/>
íové	íové	k6eAd1
stali	stát	k5eAaPmAgMnP
vegetariáni	vegetarián	k1gMnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
že	že	k8xS
budoucí	budoucí	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
by	by	kYmCp3nS
se	se	k3xPyFc4
postupně	postupně	k6eAd1
měla	mít	k5eAaImAgFnS
stát	stát	k5eAaImF,k5eAaPmF
vegetariánskou	vegetariánský	k2eAgFnSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
126	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
127	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
128	#num#	k4
<g/>
]	]	kIx)
'	'	kIx"
<g/>
Abdu	Abda	k1gFnSc4
<g/>
'	'	kIx"
<g/>
l-Bahá	l-Bahat	k5eAaImIp3nS,k5eAaPmIp3nS
také	také	k9
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
zabíjení	zabíjení	k1gNnSc1
zvířat	zvíře	k1gNnPc2
je	být	k5eAaImIp3nS
v	v	k7c6
rozporu	rozpor	k1gInSc6
se	s	k7c7
soucitem	soucit	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
Shoghi	Shogh	k1gFnSc3
Effendi	Effend	k1gMnPc1
<g/>
,	,	kIx,
hlava	hlava	k1gFnSc1
víry	víra	k1gFnSc2
Bahá	Bahá	k1gFnSc1
<g/>
'	'	kIx"
<g/>
í	í	k0
v	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
čistě	čistě	k6eAd1
vegetariánská	vegetariánský	k2eAgFnSc1d1
strava	strava	k1gFnSc1
by	by	kYmCp3nS
byla	být	k5eAaImAgFnS
vhodnější	vhodný	k2eAgFnSc1d2
<g/>
,	,	kIx,
neboť	neboť	k8xC
se	se	k3xPyFc4
vyhýbá	vyhýbat	k5eAaImIp3nS
zabíjení	zabíjení	k1gNnSc2
zvířat	zvíře	k1gNnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
129	#num#	k4
<g/>
]	]	kIx)
tak	tak	k6eAd1
on	on	k3xPp3gMnSc1
i	i	k8xC
Světový	světový	k2eAgMnSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
dům	dům	k1gInSc1
spravedlnosti	spravedlnost	k1gFnSc2
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgInSc1d3
správní	správní	k2eAgInSc1d1
orgán	orgán	k1gInSc1
Bahá	Bahá	k1gFnSc1
<g/>
'	'	kIx"
<g/>
ísmu	ísmu	k5eAaPmIp1nS
<g/>
,	,	kIx,
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
tyto	tento	k3xDgFnPc1
nauky	nauka	k1gFnPc1
nepředstavují	představovat	k5eNaImIp3nP
praxi	praxe	k1gFnSc4
Bahá	Bahá	k1gFnSc1
<g/>
'	'	kIx"
<g/>
í	í	k0
a	a	k8xC
že	že	k8xS
vyznavači	vyznavač	k1gMnPc1
Bahá	Bahá	k1gFnSc1
<g/>
'	'	kIx"
<g/>
í	í	k0
si	se	k3xPyFc3
mohou	moct	k5eAaImIp3nP
vybrat	vybrat	k5eAaPmF
k	k	k7c3
jídlu	jídlo	k1gNnSc3
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
chtějí	chtít	k5eAaImIp3nP
<g/>
,	,	kIx,
ale	ale	k8xC
měli	mít	k5eAaImAgMnP
by	by	kYmCp3nP
respektovat	respektovat	k5eAaImF
přesvědčení	přesvědčení	k1gNnSc3
druhých	druhý	k4xOgMnPc2
<g/>
.	.	kIx.
</s>
<s>
Buddhismus	buddhismus	k1gInSc1
</s>
<s>
Vegetariánská	vegetariánský	k2eAgFnSc1d1
večeře	večeře	k1gFnSc1
v	v	k7c6
japonském	japonský	k2eAgInSc6d1
buddhistickém	buddhistický	k2eAgInSc6d1
chrámu	chrám	k1gInSc6
</s>
<s>
Théravádisté	Théravádista	k1gMnPc1
obecně	obecně	k6eAd1
jedí	jíst	k5eAaImIp3nP
maso	maso	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
130	#num#	k4
<g/>
]	]	kIx)
Pokud	pokud	k8xS
buddhističtí	buddhistický	k2eAgMnPc1d1
mniši	mnich	k1gMnPc1
„	„	k?
<g/>
vidí	vidět	k5eAaImIp3nP
<g/>
,	,	kIx,
slyší	slyšet	k5eAaImIp3nP
nebo	nebo	k8xC
vědí	vědět	k5eAaImIp3nP
<g/>
“	“	k?
<g/>
,	,	kIx,
že	že	k8xS
živé	živý	k2eAgNnSc1d1
zvíře	zvíře	k1gNnSc1
bylo	být	k5eAaImAgNnS
zabito	zabít	k5eAaPmNgNnS
speciálně	speciálně	k6eAd1
pro	pro	k7c4
ně	on	k3xPp3gMnPc4
k	k	k7c3
jídlu	jídlo	k1gNnSc3
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nP
ho	on	k3xPp3gMnSc4
odmítnout	odmítnout	k5eAaPmF
<g/>
,	,	kIx,
jinak	jinak	k6eAd1
spáchají	spáchat	k5eAaPmIp3nP
přestupek	přestupek	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
131	#num#	k4
<g/>
]	]	kIx)
Nicméně	nicméně	k8xC
toto	tento	k3xDgNnSc1
nezahrnuje	zahrnovat	k5eNaImIp3nS
jedení	jedení	k1gNnSc4
masa	maso	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
bylo	být	k5eAaImAgNnS
dáno	dát	k5eAaPmNgNnS
jako	jako	k9
almužna	almužna	k1gFnSc1
nebo	nebo	k8xC
komerčně	komerčně	k6eAd1
zakoupeno	zakoupen	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
théravádském	théravádský	k2eAgInSc6d1
kánonu	kánon	k1gInSc6
Buddha	Buddha	k1gMnSc1
nedával	dávat	k5eNaImAgMnS
žádný	žádný	k3yNgInSc4
komentář	komentář	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
odrazuje	odrazovat	k5eAaImIp3nS
od	od	k7c2
jedení	jedení	k1gNnSc2
masa	maso	k1gNnSc2
(	(	kIx(
<g/>
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
masa	maso	k1gNnSc2
specifických	specifický	k2eAgInPc2d1
druhů	druh	k1gInPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
člověk	člověk	k1gMnSc1
<g/>
,	,	kIx,
slon	slon	k1gMnSc1
<g/>
,	,	kIx,
kůň	kůň	k1gMnSc1
<g/>
,	,	kIx,
pes	pes	k1gMnSc1
<g/>
,	,	kIx,
had	had	k1gMnSc1
<g/>
,	,	kIx,
lev	lev	k1gMnSc1
<g/>
,	,	kIx,
tygr	tygr	k1gMnSc1
<g/>
,	,	kIx,
levhart	levhart	k1gMnSc1
<g/>
,	,	kIx,
medvěd	medvěd	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
hyena	hyena	k1gFnSc1
<g/>
[	[	kIx(
<g/>
132	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
specificky	specificky	k6eAd1
odmítl	odmítnout	k5eAaPmAgMnS
ustanovit	ustanovit	k5eAaPmF
vegetariánství	vegetariánství	k1gNnSc4
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
řádovém	řádový	k2eAgInSc6d1
kodexu	kodex	k1gInSc6
<g/>
,	,	kIx,
když	když	k8xS
byl	být	k5eAaImAgInS
učiněn	učinit	k5eAaImNgInS,k5eAaPmNgInS
tento	tento	k3xDgInSc1
návrh	návrh	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
133	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
134	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
několika	několik	k4yIc6
sanskrtských	sanskrtský	k2eAgInPc6d1
textech	text	k1gInPc6
v	v	k7c6
mahájánském	mahájánský	k2eAgInSc6d1
buddhismu	buddhismus	k1gInSc6
Buddha	Buddha	k1gMnSc1
instruuje	instruovat	k5eAaBmIp3nS
své	svůj	k3xOyFgMnPc4
následovníky	následovník	k1gMnPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
vyvarovali	vyvarovat	k5eAaPmAgMnP
pojídání	pojídání	k1gNnSc3
masa	maso	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
135	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
136	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
137	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
138	#num#	k4
<g/>
]	]	kIx)
Nicméně	nicméně	k8xC
každá	každý	k3xTgFnSc1
větev	větev	k1gFnSc1
mahájánského	mahájánský	k2eAgInSc2d1
buddhismu	buddhismus	k1gInSc2
si	se	k3xPyFc3
vybírá	vybírat	k5eAaImIp3nS
sútru	sútra	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
bude	být	k5eAaImBp3nS
následovat	následovat	k5eAaImF
<g/>
;	;	kIx,
některé	některý	k3yIgFnPc1
větve	větev	k1gFnPc1
<g/>
,	,	kIx,
včetně	včetně	k7c2
většiny	většina	k1gFnSc2
tibetských	tibetský	k2eAgMnPc2d1
a	a	k8xC
japonských	japonský	k2eAgMnPc2d1
buddhistů	buddhista	k1gMnPc2
<g/>
,	,	kIx,
jedí	jíst	k5eAaImIp3nP
maso	maso	k1gNnSc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
mnoho	mnoho	k4c1
čínských	čínský	k2eAgFnPc2d1
buddhistických	buddhistický	k2eAgFnPc2d1
větví	větev	k1gFnPc2
ne	ne	k9
<g/>
.	.	kIx.
</s>
<s>
Křesťanství	křesťanství	k1gNnSc1
</s>
<s>
Neexistuje	existovat	k5eNaImIp3nS
žádné	žádný	k3yNgNnSc1
specifické	specifický	k2eAgNnSc1d1
učení	učení	k1gNnSc1
o	o	k7c6
stravování	stravování	k1gNnSc6
v	v	k7c6
tradičním	tradiční	k2eAgNnSc6d1
křesťanství	křesťanství	k1gNnSc6
<g/>
,	,	kIx,
kromě	kromě	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
nic	nic	k3yNnSc1
není	být	k5eNaImIp3nS
zakázáno	zakázat	k5eAaPmNgNnS
z	z	k7c2
náboženských	náboženský	k2eAgInPc2d1
principů	princip	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedení	jedení	k1gNnSc1
masa	maso	k1gNnSc2
není	být	k5eNaImIp3nS
ani	ani	k8xC
podporováno	podporován	k2eAgNnSc1d1
ani	ani	k8xC
od	od	k7c2
něj	on	k3xPp3gNnSc2
není	být	k5eNaImIp3nS
odrazováno	odrazován	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křesťané	křesťan	k1gMnPc1
se	se	k3xPyFc4
vždy	vždy	k6eAd1
mohli	moct	k5eAaImAgMnP
svobodně	svobodně	k6eAd1
rozhodnout	rozhodnout	k5eAaPmF
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
budou	být	k5eAaImBp3nP
jíst	jíst	k5eAaImF
<g/>
;	;	kIx,
avšak	avšak	k8xC
uvnitř	uvnitř	k7c2
křesťanství	křesťanství	k1gNnSc2
existují	existovat	k5eAaImIp3nP
skupiny	skupina	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
z	z	k7c2
různých	různý	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
praktikují	praktikovat	k5eAaImIp3nP
zvláštní	zvláštní	k2eAgNnSc4d1
stravovací	stravovací	k2eAgNnSc4d1
omezení	omezení	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
139	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Časná	časný	k2eAgFnSc1d1
křesťanská	křesťanský	k2eAgFnSc1d1
sekta	sekta	k1gFnSc1
známá	známý	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
ebionité	ebionitý	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
považována	považován	k2eAgFnSc1d1
za	za	k7c4
vegetariány	vegetarián	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zachované	zachovaný	k2eAgInPc1d1
fragmenty	fragment	k1gInPc1
z	z	k7c2
jejich	jejich	k3xOp3gNnSc2
evangelia	evangelium	k1gNnSc2
ukazují	ukazovat	k5eAaImIp3nP
jejich	jejich	k3xOp3gFnSc4
víru	víra	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
–	–	k?
jako	jako	k8xS,k8xC
Kristus	Kristus	k1gMnSc1
je	být	k5eAaImIp3nS
velikonoční	velikonoční	k2eAgFnPc4d1
oběti	oběť	k1gFnPc4
a	a	k8xC
pojídání	pojídání	k1gNnSc1
velikonočního	velikonoční	k2eAgMnSc2d1
beránka	beránek	k1gMnSc2
již	již	k6eAd1
není	být	k5eNaImIp3nS
potřeba	potřeba	k1gFnSc1
–	–	k?
tak	tak	k6eAd1
vegetariánská	vegetariánský	k2eAgFnSc1d1
strava	strava	k1gFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
(	(	kIx(
<g/>
a	a	k8xC
nebo	nebo	k8xC
měla	mít	k5eAaImAgNnP
by	by	kYmCp3nP
být	být	k5eAaImF
<g/>
)	)	kIx)
dodržována	dodržován	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
ortodoxní	ortodoxní	k2eAgNnSc4d1
křesťanství	křesťanství	k1gNnSc4
nepřijímá	přijímat	k5eNaImIp3nS
jejich	jejich	k3xOp3gNnSc4
učení	učení	k1gNnSc4
jako	jako	k8xC,k8xS
autentické	autentický	k2eAgInPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
jejich	jejich	k3xOp3gInSc1
konkrétní	konkrétní	k2eAgInSc1d1
příkaz	příkaz	k1gInSc1
k	k	k7c3
přísnému	přísný	k2eAgNnSc3d1
vegetariánství	vegetariánství	k1gNnSc3
byl	být	k5eAaImAgInS
uváděn	uváděn	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
jeden	jeden	k4xCgMnSc1
z	z	k7c2
„	„	k?
<g/>
omylů	omyl	k1gInPc2
<g/>
“	“	k?
ebionitů	ebionit	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
140	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
141	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mnohem	mnohem	k6eAd1
později	pozdě	k6eAd2
se	se	k3xPyFc4
vegetariánské	vegetariánský	k2eAgFnSc2d1
stravy	strava	k1gFnSc2
držela	držet	k5eAaImAgFnS
Biblická	biblický	k2eAgFnSc1d1
křesťanská	křesťanský	k2eAgFnSc1d1
církev	církev	k1gFnSc1
založena	založit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1809	#num#	k4
reverendem	reverend	k1gMnSc7
Williamem	William	k1gInSc7
Cowherdem	Cowherd	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
142	#num#	k4
<g/>
]	]	kIx)
Cowherd	Cowherd	k1gMnSc1
byl	být	k5eAaImAgMnS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
filozofických	filozofický	k2eAgMnPc2d1
předchůdců	předchůdce	k1gMnPc2
Vegetariánské	vegetariánský	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
143	#num#	k4
<g/>
]	]	kIx)
Povzbuzoval	povzbuzovat	k5eAaImAgInS
členy	člen	k1gMnPc4
zdržet	zdržet	k5eAaPmF
se	se	k3xPyFc4
jedení	jedení	k1gNnSc2
masa	maso	k1gNnSc2
jako	jako	k8xC,k8xS
formy	forma	k1gFnSc2
střídmosti	střídmost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
144	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Adventisté	adventista	k1gMnPc1
sedmého	sedmý	k4xOgInSc2
dne	den	k1gInSc2
(	(	kIx(
<g/>
ASD	ASD	kA
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
vyzýváni	vyzývat	k5eAaImNgMnP
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
zapojili	zapojit	k5eAaPmAgMnP
do	do	k7c2
zdravých	zdravý	k2eAgFnPc2d1
stravovacích	stravovací	k2eAgFnPc2d1
praktik	praktika	k1gFnPc2
a	a	k8xC
ovolaktovegetariánská	ovolaktovegetariánský	k2eAgFnSc1d1
strava	strava	k1gFnSc1
je	být	k5eAaImIp3nS
doporučovány	doporučovat	k5eAaImNgFnP
Výživovou	výživový	k2eAgFnSc7d1
radou	rada	k1gFnSc7
Generální	generální	k2eAgFnSc2d1
konference	konference	k1gFnSc2
adventistů	adventista	k1gMnPc2
sedmého	sedmý	k4xOgInSc2
dne	den	k1gInSc2
(	(	kIx(
<g/>
GCNC	GCNC	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sponzorují	sponzorovat	k5eAaImIp3nP
a	a	k8xC
podílí	podílet	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c6
mnoha	mnoho	k4c6
vědeckých	vědecký	k2eAgFnPc6d1
studiích	studie	k1gFnPc6
zkoumajících	zkoumající	k2eAgMnPc6d1
vliv	vliv	k1gInSc4
stravovacích	stravovací	k2eAgNnPc2d1
rozhodnutí	rozhodnutí	k1gNnPc2
na	na	k7c6
výsledcích	výsledek	k1gInPc6
zdravotní	zdravotní	k2eAgFnSc2d1
péče	péče	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
145	#num#	k4
<g/>
]	]	kIx)
GCNC	GCNC	kA
si	se	k3xPyFc3
navíc	navíc	k6eAd1
upravila	upravit	k5eAaPmAgFnS
potravinovou	potravinový	k2eAgFnSc4d1
pyramidu	pyramida	k1gFnSc4
Ministerstva	ministerstvo	k1gNnSc2
zemědělství	zemědělství	k1gNnSc2
USA	USA	kA
pro	pro	k7c4
svůj	svůj	k3xOyFgInSc4
vegetariánský	vegetariánský	k2eAgInSc4d1
stravovací	stravovací	k2eAgInSc4d1
přístup	přístup	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
145	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
146	#num#	k4
<g/>
]	]	kIx)
Nicméně	nicméně	k8xC
jediné	jediný	k2eAgInPc4d1
druhy	druh	k1gInPc4
masa	maso	k1gNnSc2
konkrétně	konkrétně	k6eAd1
odsuzované	odsuzovaný	k2eAgFnPc1d1
ve	v	k7c6
zdravotní	zdravotní	k2eAgFnSc6d1
zprávě	zpráva	k1gFnSc6
ASD	ASD	kA
je	být	k5eAaImIp3nS
nečisté	čistý	k2eNgNnSc4d1
maso	maso	k1gNnSc4
a	a	k8xC
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
je	být	k5eAaImIp3nS
zakázáno	zakázat	k5eAaPmNgNnS
v	v	k7c6
Písmu	písmo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
147	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Některé	některý	k3yIgInPc1
řeholní	řeholní	k2eAgInPc1d1
řády	řád	k1gInPc1
dodržují	dodržovat	k5eAaImIp3nP
vegetariánskou	vegetariánský	k2eAgFnSc4d1
stravu	strava	k1gFnSc4
a	a	k8xC
členové	člen	k1gMnPc1
pravoslavné	pravoslavný	k2eAgFnSc2d1
církve	církev	k1gFnSc2
jedí	jíst	k5eAaImIp3nP
během	během	k7c2
půstů	půst	k1gInPc2
veganskou	veganský	k2eAgFnSc4d1
stravu	strava	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
148	#num#	k4
<g/>
]	]	kIx)
Existuje	existovat	k5eAaImIp3nS
také	také	k9
silný	silný	k2eAgInSc1d1
vztah	vztah	k1gInSc1
mezi	mezi	k7c7
Kvakery	kvaker	k1gMnPc7
a	a	k8xC
vegetariánstvím	vegetariánství	k1gNnPc3
sahající	sahající	k2eAgInPc4d1
přinejmenším	přinejmenším	k6eAd1
do	do	k7c2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
vztah	vztah	k1gInSc1
zesílil	zesílit	k5eAaPmAgInS
během	během	k7c2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
společně	společně	k6eAd1
s	s	k7c7
rostoucími	rostoucí	k2eAgFnPc7d1
obavami	obava	k1gFnPc7
kvakerů	kvaker	k1gMnPc2
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
konzumací	konzumace	k1gFnSc7
alkoholu	alkohol	k1gInSc2
<g/>
,	,	kIx,
antivivisekci	antivivisekce	k1gFnSc6
a	a	k8xC
sociální	sociální	k2eAgFnSc7d1
čistotou	čistota	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojení	spojení	k1gNnSc1
mezi	mezi	k7c4
tradici	tradice	k1gFnSc4
kvakerů	kvaker	k1gMnPc2
a	a	k8xC
vegetariánstvím	vegetariánství	k1gNnSc7
se	se	k3xPyFc4
nicméně	nicméně	k8xC
stalo	stát	k5eAaPmAgNnS
nejvýznamnější	významný	k2eAgNnSc1d3
se	s	k7c7
založením	založení	k1gNnSc7
Přátelské	přátelský	k2eAgFnSc2d1
vegetariánské	vegetariánský	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1902	#num#	k4
za	za	k7c7
účelem	účel	k1gInSc7
<g/>
,	,	kIx,
„	„	k?
<g/>
aby	aby	kYmCp3nS
rozšířila	rozšířit	k5eAaPmAgFnS
svůj	svůj	k3xOyFgInSc4
příznivější	příznivý	k2eAgInSc4d2
způsob	způsob	k1gInSc4
života	život	k1gInSc2
mezi	mezi	k7c4
Společnosti	společnost	k1gFnPc4
přátel	přítel	k1gMnPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
149	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podle	podle	k7c2
kanonického	kanonický	k2eAgNnSc2d1
práva	právo	k1gNnSc2
jsou	být	k5eAaImIp3nP
římští	římský	k2eAgMnPc1d1
katolíci	katolík	k1gMnPc1
povinni	povinen	k2eAgMnPc1d1
zdržet	zdržet	k5eAaPmF
se	se	k3xPyFc4
masa	masa	k1gFnSc1
(	(	kIx(
<g/>
definováno	definovat	k5eAaBmNgNnS
jako	jako	k9
všechno	všechen	k3xTgNnSc1
zvířecí	zvířecí	k2eAgNnSc1d1
maso	maso	k1gNnSc1
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
vodních	vodní	k2eAgMnPc2d1
živočichů	živočich	k1gMnPc2
<g/>
)	)	kIx)
na	na	k7c4
Popeleční	popeleční	k2eAgFnSc4d1
středu	středa	k1gFnSc4
a	a	k8xC
všechny	všechen	k3xTgInPc1
pátky	pátek	k1gInPc1
postní	postní	k2eAgFnSc2d1
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
Velkého	velký	k2eAgInSc2d1
pátku	pátek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kanonické	kanonický	k2eAgNnSc4d1
právo	právo	k1gNnSc4
rovněž	rovněž	k9
ukládá	ukládat	k5eAaImIp3nS
katolíkům	katolík	k1gMnPc3
zdržet	zdržet	k5eAaPmF
se	se	k3xPyFc4
masa	maso	k1gNnSc2
v	v	k7c4
pátek	pátek	k1gInSc4
mimo	mimo	k6eAd1
postní	postní	k2eAgFnSc1d1
(	(	kIx(
<g/>
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
některých	některý	k3yIgInPc2
svátků	svátek	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pokud	pokud	k8xS
nejsou	být	k5eNaImIp3nP
s	s	k7c7
povolením	povolení	k1gNnSc7
místní	místní	k2eAgFnSc2d1
konference	konference	k1gFnSc2
biskupů	biskup	k1gMnPc2
nahrazeny	nahrazen	k2eAgFnPc1d1
jiným	jiný	k1gMnSc7
kajícím	kající	k2eAgMnSc7d1
se	se	k3xPyFc4
úkonem	úkon	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Omezení	omezení	k1gNnSc1
týkající	týkající	k2eAgFnSc2d1
se	se	k3xPyFc4
konzumace	konzumace	k1gFnSc2
masa	maso	k1gNnSc2
v	v	k7c6
těchto	tento	k3xDgInPc6
dnech	den	k1gInPc6
je	být	k5eAaImIp3nS
pouze	pouze	k6eAd1
aktem	akt	k1gInSc7
pokání	pokání	k1gNnSc2
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInSc7
důvodem	důvod	k1gInSc7
nejsou	být	k5eNaImIp3nP
náboženské	náboženský	k2eAgFnPc4d1
námitky	námitka	k1gFnPc4
proti	proti	k7c3
pojídání	pojídání	k1gNnSc3
masa	maso	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
150	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hinduismus	hinduismus	k1gInSc1
</s>
<s>
Obchod	obchod	k1gInSc1
s	s	k7c7
ovocem	ovoce	k1gNnSc7
a	a	k8xC
zeleninou	zelenina	k1gFnSc7
v	v	k7c6
Meppadi	Meppad	k1gMnPc5
</s>
<s>
Ačkoli	ačkoli	k8xS
v	v	k7c6
hinduismu	hinduismus	k1gInSc6
neexistuje	existovat	k5eNaImIp3nS
žádné	žádný	k3yNgNnSc1
přísné	přísný	k2eAgNnSc1d1
pravidlo	pravidlo	k1gNnSc1
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
konzumovat	konzumovat	k5eAaBmF
a	a	k8xC
co	co	k9
ne	ne	k9
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInSc1
různé	různý	k2eAgFnPc1d1
cesty	cesta	k1gFnPc1
dodržují	dodržovat	k5eAaImIp3nP
vegetariánství	vegetariánství	k1gNnSc4
jako	jako	k8xC,k8xS
ideál	ideál	k1gInSc4
ovlivněný	ovlivněný	k2eAgInSc4d1
džinismem	džinismus	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc1
důvody	důvod	k1gInPc1
jsou	být	k5eAaImIp3nP
<g/>
:	:	kIx,
princip	princip	k1gInSc1
nenásilí	nenásilí	k1gNnSc1
(	(	kIx(
<g/>
ahinsá	ahinsý	k2eAgFnSc1d1
<g/>
)	)	kIx)
aplikovány	aplikovat	k5eAaBmNgFnP
na	na	k7c4
zvířata	zvíře	k1gNnPc4
<g/>
;	;	kIx,
<g/>
[	[	kIx(
<g/>
151	#num#	k4
<g/>
]	]	kIx)
záměr	záměr	k1gInSc4
nabídnout	nabídnout	k5eAaPmF
božstvu	božstvo	k1gNnSc6
pouze	pouze	k6eAd1
„	„	k?
<g/>
čisté	čistá	k1gFnPc4
<g/>
“	“	k?
(	(	kIx(
<g/>
vegetariánské	vegetariánský	k2eAgFnPc4d1
<g/>
)	)	kIx)
potraviny	potravina	k1gFnPc4
a	a	k8xC
pak	pak	k6eAd1
je	být	k5eAaImIp3nS
přijmout	přijmout	k5eAaPmF
zpět	zpět	k6eAd1
jako	jako	k9
prasád	prasáda	k1gFnPc2
<g/>
;	;	kIx,
a	a	k8xC
přesvědčení	přesvědčení	k1gNnSc1
<g/>
,	,	kIx,
že	že	k8xS
sattvická	sattvický	k2eAgFnSc1d1
strava	strava	k1gFnSc1
je	být	k5eAaImIp3nS
prospěšná	prospěšný	k2eAgFnSc1d1
pro	pro	k7c4
zdravé	zdravý	k2eAgNnSc4d1
tělo	tělo	k1gNnSc4
a	a	k8xC
mysl	mysl	k1gFnSc4
a	a	k8xC
že	že	k8xS
nevegetariánská	vegetariánský	k2eNgFnSc1d1
strava	strava	k1gFnSc1
se	se	k3xPyFc4
nedoporučuje	doporučovat	k5eNaImIp3nS
pro	pro	k7c4
lepší	dobrý	k2eAgFnSc4d2
mysl	mysl	k1gFnSc4
a	a	k8xC
duchovní	duchovní	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiné	jiný	k2eAgInPc1d1
důvody	důvod	k1gInPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
nedostatek	nedostatek	k1gInSc4
dostupnosti	dostupnost	k1gFnSc2
masa	maso	k1gNnSc2
a	a	k8xC
panovníky	panovník	k1gMnPc7
vynucené	vynucený	k2eAgFnSc2d1
diety	dieta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Stravovací	stravovací	k2eAgInPc4d1
návyky	návyk	k1gInPc4
hinduistů	hinduista	k1gMnPc2
se	se	k3xPyFc4
však	však	k9
liší	lišit	k5eAaImIp3nP
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
jejich	jejich	k3xOp3gFnSc6
komunitě	komunita	k1gFnSc6
<g/>
,	,	kIx,
umístění	umístění	k1gNnSc6
<g/>
,	,	kIx,
zvycích	zvyk	k1gInPc6
a	a	k8xC
měnících	měnící	k2eAgFnPc6d1
se	se	k3xPyFc4
tradicích	tradice	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historicky	historicky	k6eAd1
a	a	k8xC
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
jsou	být	k5eAaImIp3nP
hinduisté	hinduista	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
jedí	jíst	k5eAaImIp3nP
maso	maso	k1gNnSc4
ze	z	k7c2
zvířat	zvíře	k1gNnPc2
zabitých	zabitý	k1gMnPc2
rychle	rychle	k6eAd1
(	(	kIx(
<g/>
jhatka	jhatka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
152	#num#	k4
<g/>
]	]	kIx)
zatímco	zatímco	k8xS
někteří	některý	k3yIgMnPc1
hinduisté	hinduista	k1gMnPc1
věří	věřit	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
kráva	kráva	k1gFnSc1
je	být	k5eAaImIp3nS
posvátné	posvátný	k2eAgNnSc4d1
zvíře	zvíře	k1gNnSc4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
porážka	porážka	k1gFnSc1
na	na	k7c4
maso	maso	k1gNnSc4
je	být	k5eAaImIp3nS
zakázána	zakázán	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
regionu	region	k1gInSc6
se	se	k3xPyFc4
tato	tento	k3xDgFnSc1
víra	víra	k1gFnSc1
liší	lišit	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
153	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Islám	islám	k1gInSc1
</s>
<s>
Někteří	některý	k3yIgMnPc1
stoupenci	stoupenec	k1gMnPc1
islámu	islám	k1gInSc2
nebo	nebo	k8xC
muslimové	muslim	k1gMnPc1
se	se	k3xPyFc4
rozhodli	rozhodnout	k5eAaPmAgMnP
být	být	k5eAaImF
vegetariáni	vegetarián	k1gMnPc1
ze	z	k7c2
zdravotních	zdravotní	k2eAgInPc2d1
etických	etický	k2eAgInPc2d1
nebo	nebo	k8xC
osobních	osobní	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
volba	volba	k1gFnSc1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
stali	stát	k5eAaPmAgMnP
vegetariány	vegetarián	k1gMnPc4
z	z	k7c2
nezdravotních	zdravotní	k2eNgInPc2d1
důvodů	důvod	k1gInPc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
někdy	někdy	k6eAd1
kontroverzní	kontroverzní	k2eAgMnSc1d1
kvůli	kvůli	k7c3
konfliktu	konflikt	k1gInSc3
fatwy	fatwa	k1gFnSc2
a	a	k8xC
rozdílných	rozdílný	k2eAgFnPc2d1
interpretací	interpretace	k1gFnPc2
koránu	korán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
někteří	některý	k3yIgMnPc1
více	hodně	k6eAd2
tradiční	tradiční	k2eAgMnPc1d1
muslimové	muslim	k1gMnPc1
mohou	moct	k5eAaImIp3nP
mlčet	mlčet	k5eAaImF
o	o	k7c6
své	svůj	k3xOyFgFnSc6
vegetariánské	vegetariánský	k2eAgFnSc6d1
stravě	strava	k1gFnSc6
<g/>
,	,	kIx,
počet	počet	k1gInSc1
vegetariánských	vegetariánský	k2eAgMnPc2d1
muslimů	muslim	k1gMnPc2
se	se	k3xPyFc4
zvyšuje	zvyšovat	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
154	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
155	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vegetariánství	vegetariánství	k1gNnSc1
bylo	být	k5eAaImAgNnS
praktikováno	praktikovat	k5eAaImNgNnS
některými	některý	k3yIgMnPc7
vlivnými	vlivný	k2eAgMnPc7d1
muslimy	muslim	k1gMnPc7
včetně	včetně	k7c2
irácké	irácký	k2eAgFnSc2d1
teoložky	teoložka	k1gFnSc2
<g/>
,	,	kIx,
ženské	ženský	k2eAgFnSc2d1
mystičky	mystička	k1gFnSc2
a	a	k8xC
básnířky	básnířka	k1gFnSc2
Rabi	rabi	k1gMnSc1
<g/>
'	'	kIx"
<g/>
a	a	k8xC
al	ala	k1gFnPc2
<g/>
'	'	kIx"
<g/>
Adawiyya	Adawiyya	k1gFnSc1
z	z	k7c2
Basry	Basra	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
zemřela	zemřít	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
801	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
srílanského	srílanský	k2eAgMnSc4d1
sufíského	sufíský	k1gMnSc4
mistra	mistr	k1gMnSc4
Bawa	Bawus	k1gMnSc4
Muhaiyaddeena	Muhaiyaddeen	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
založil	založit	k5eAaPmAgMnS
společnství	společnství	k1gNnSc4
Bawa	Bawum	k1gNnSc2
Muhaiyaddeen	Muhaiyaddena	k1gFnPc2
ve	v	k7c6
Filadelfii	Filadelfie	k1gFnSc6
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bývalý	bývalý	k2eAgMnSc1d1
indický	indický	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Abdul	Abdul	k1gMnSc1
Kalám	kala	k1gFnPc3
byl	být	k5eAaImAgInS
také	také	k9
vegetarián	vegetarián	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
156	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
lednu	leden	k1gInSc6
1996	#num#	k4
oznámila	oznámit	k5eAaPmAgFnS
Mezinárodní	mezinárodní	k2eAgFnSc1d1
vegetariánská	vegetariánský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
vytvoření	vytvoření	k1gNnSc2
Muslimské	muslimský	k2eAgFnSc2d1
vegetariánské	vegetariánský	k2eAgFnSc2d1
<g/>
/	/	kIx~
<g/>
veganské	veganský	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
157	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mnoho	mnoho	k4c1
nevegetariánských	vegetariánský	k2eNgMnPc2d1
muslimů	muslim	k1gMnPc2
si	se	k3xPyFc3
vybere	vybrat	k5eAaPmIp3nS
vegetariánské	vegetariánský	k2eAgNnSc1d1
(	(	kIx(
<g/>
nebo	nebo	k8xC
mořské	mořský	k2eAgInPc1d1
plody	plod	k1gInPc1
<g/>
)	)	kIx)
možnosti	možnost	k1gFnPc1
při	při	k7c6
stolování	stolování	k1gNnSc6
v	v	k7c6
jiných	jiný	k2eAgFnPc6d1
než	než	k8xS
halal	halat	k5eAaBmAgInS,k5eAaPmAgInS,k5eAaImAgInS
restauracích	restaurace	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
otázka	otázka	k1gFnSc1
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
nemají	mít	k5eNaImIp3nP
k	k	k7c3
dispozici	dispozice	k1gFnSc3
správný	správný	k2eAgInSc4d1
druh	druh	k1gInSc4
masa	maso	k1gNnSc2
<g/>
,	,	kIx,
než	než	k8xS
že	že	k8xS
by	by	kYmCp3nP
celkově	celkově	k6eAd1
nejedli	jíst	k5eNaImAgMnP
maso	maso	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
155	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Džinismus	Džinismus	k1gInSc1
</s>
<s>
Potravinová	potravinový	k2eAgFnSc1d1
pyramida	pyramida	k1gFnSc1
džinistů	džinista	k1gMnPc2
na	na	k7c6
základě	základ	k1gInSc6
úrovně	úroveň	k1gFnSc2
ahimsa	ahimsa	k1gFnSc1
(	(	kIx(
<g/>
nenásilí	nenásilí	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Stoupenci	stoupenec	k1gMnPc1
džinismu	džinismus	k1gInSc2
věří	věřit	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
všechny	všechen	k3xTgInPc1
živé	živý	k2eAgInPc1d1
organismy	organismus	k1gInPc1
<g/>
,	,	kIx,
včetně	včetně	k7c2
mikroorganismů	mikroorganismus	k1gInPc2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
živé	živý	k2eAgInPc1d1
a	a	k8xC
mají	mít	k5eAaImIp3nP
duši	duše	k1gFnSc4
a	a	k8xC
mají	mít	k5eAaImIp3nP
jeden	jeden	k4xCgMnSc1
nebo	nebo	k8xC
více	hodně	k6eAd2
z	z	k7c2
celkových	celkový	k2eAgInPc2d1
pěti	pět	k4xCc2
smyslů	smysl	k1gInPc2
a	a	k8xC
jdou	jít	k5eAaImIp3nP
do	do	k7c2
krajnosti	krajnost	k1gFnSc2
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
minimalizovala	minimalizovat	k5eAaBmAgFnS
újma	újma	k1gFnSc1
jakémukoliv	jakýkoliv	k3yIgInSc3
živému	živý	k2eAgInSc3d1
organismu	organismus	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvíce	hodně	k6eAd3,k6eAd1
džinistů	džinista	k1gMnPc2
jsou	být	k5eAaImIp3nP
laktovegetariáni	laktovegetarián	k1gMnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
více	hodně	k6eAd2
oddaní	oddaný	k2eAgMnPc1d1
džinisté	džinista	k1gMnPc1
nejedí	jíst	k5eNaImIp3nP
kořenovou	kořenový	k2eAgFnSc4d1
zeleninu	zelenina	k1gFnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
věří	věřit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
kořenová	kořenový	k2eAgFnSc1d1
zelenina	zelenina	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
mnohem	mnohem	k6eAd1
více	hodně	k6eAd2
mikroorganismů	mikroorganismus	k1gInPc2
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
jinou	jiný	k2eAgFnSc7d1
zeleninou	zelenina	k1gFnSc7
<g/>
,	,	kIx,
a	a	k8xC
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
jedí	jíst	k5eAaImIp3nP
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
nevyhnutelné	vyhnutelný	k2eNgNnSc1d1
násilí	násilí	k1gNnSc1
na	na	k7c6
těchto	tento	k3xDgInPc6
mikroorganismech	mikroorganismus	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takže	takže	k8xS
se	se	k3xPyFc4
zaměřují	zaměřovat	k5eAaImIp3nP
na	na	k7c4
jedení	jedení	k1gNnSc4
fazolí	fazole	k1gFnPc2
a	a	k8xC
ovoce	ovoce	k1gNnSc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc1
pěstování	pěstování	k1gNnSc1
nezahrnuje	zahrnovat	k5eNaImIp3nS
zabíjení	zabíjení	k1gNnSc4
mnoha	mnoho	k4c2
mikroorganismů	mikroorganismus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejsou	být	k5eNaImIp3nP
povoleny	povolen	k2eAgInPc1d1
produkty	produkt	k1gInPc1
získané	získaný	k2eAgInPc1d1
z	z	k7c2
mrtvých	mrtvý	k2eAgNnPc2d1
zvířat	zvíře	k1gNnPc2
<g/>
,	,	kIx,
protože	protože	k8xS
když	když	k8xS
živá	živý	k2eAgFnSc1d1
bytost	bytost	k1gFnSc1
zemře	zemřít	k5eAaPmIp3nS
<g/>
,	,	kIx,
mnoho	mnoho	k4c4
mikroorganismů	mikroorganismus	k1gInPc2
(	(	kIx(
<g/>
označovaných	označovaný	k2eAgMnPc2d1
jako	jako	k8xS,k8xC
rozkladači	rozkladač	k1gMnPc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
bude	být	k5eAaImBp3nS
rozmnožovat	rozmnožovat	k5eAaImF
v	v	k7c6
procesu	proces	k1gInSc6
rozkladu	rozklad	k1gInSc2
a	a	k8xC
pojídání	pojídání	k1gNnSc2
mrtvoly	mrtvola	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
je	být	k5eAaImIp3nS
tak	tak	k6eAd1
nevyhnutelné	vyhnutelný	k2eNgNnSc4d1
násilí	násilí	k1gNnSc4
na	na	k7c6
rozkladačích	rozkladač	k1gInPc6
při	při	k7c6
pojídání	pojídání	k1gNnSc6
mrtvého	mrtvý	k2eAgNnSc2d1
těla	tělo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Džinističtí	Džinistický	k2eAgMnPc1d1
mniši	mnich	k1gMnPc1
obvykle	obvykle	k6eAd1
dodržují	dodržovat	k5eAaImIp3nP
mnoho	mnoho	k4c4
půstů	půst	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
když	když	k8xS
se	se	k3xPyFc4
přes	přes	k7c4
duchovní	duchovní	k2eAgFnSc4d1
sily	sít	k5eAaImAgFnP
dozvědí	dozvědět	k5eAaPmIp3nP
<g/>
,	,	kIx,
že	že	k8xS
jejich	jejich	k3xOp3gInSc1
život	život	k1gInSc1
bude	být	k5eAaImBp3nS
již	již	k6eAd1
velmi	velmi	k6eAd1
krátký	krátký	k2eAgInSc1d1
<g/>
,	,	kIx,
začnou	začít	k5eAaPmIp3nP
půst	půst	k1gInSc4
až	až	k9
do	do	k7c2
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
158	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
159	#num#	k4
<g/>
]	]	kIx)
Někteří	některý	k3yIgMnPc1
obzvláště	obzvláště	k6eAd1
oddaní	oddaný	k2eAgMnPc1d1
jedinci	jedinec	k1gMnPc1
jsou	být	k5eAaImIp3nP
frutariáni	frutarián	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
160	#num#	k4
<g/>
]	]	kIx)
Med	med	k1gInSc1
je	být	k5eAaImIp3nS
zakázán	zakázat	k5eAaPmNgInS
<g/>
,	,	kIx,
protože	protože	k8xS
med	med	k1gInSc1
je	být	k5eAaImIp3nS
včelami	včela	k1gFnPc7
natrávený	natrávený	k2eAgInSc4d1
nektar	nektar	k1gInSc4
<g/>
[	[	kIx(
<g/>
161	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
může	moct	k5eAaImIp3nS
také	také	k9
obsahovat	obsahovat	k5eAaImF
vejce	vejce	k1gNnSc4
<g/>
,	,	kIx,
výkaly	výkal	k1gInPc4
a	a	k8xC
mrtvé	mrtvý	k2eAgFnPc4d1
včely	včela	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
džinisté	džinista	k1gMnPc1
nejedí	jíst	k5eNaImIp3nP
části	část	k1gFnPc4
rostlin	rostlina	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
rostou	růst	k5eAaImIp3nP
pod	pod	k7c7
zemí	zem	k1gFnSc7
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
kořeny	kořen	k1gInPc4
a	a	k8xC
bulvy	bulva	k1gFnPc4
<g/>
,	,	kIx,
protože	protože	k8xS
při	při	k7c6
vytahování	vytahování	k1gNnSc6
rostlin	rostlina	k1gFnPc2
nahoru	nahoru	k6eAd1
<g/>
,	,	kIx,
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
zabita	zabit	k2eAgNnPc4d1
malá	malý	k2eAgNnPc4d1
zvířata	zvíře	k1gNnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
162	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Judaismus	judaismus	k1gInSc1
</s>
<s>
Mísa	mísa	k1gFnSc1
čerstvého	čerstvý	k2eAgNnSc2d1
ovoce	ovoce	k1gNnSc2
a	a	k8xC
zeleniny	zelenina	k1gFnSc2
pěstované	pěstovaný	k2eAgFnSc2d1
v	v	k7c6
Izraeli	Izrael	k1gInSc6
</s>
<s>
I	i	k9
když	když	k8xS
to	ten	k3xDgNnSc1
není	být	k5eNaImIp3nS
nutné	nutný	k2eAgNnSc1d1
(	(	kIx(
<g/>
požadováno	požadován	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
podle	podle	k7c2
některých	některý	k3yIgFnPc2
tradic	tradice	k1gFnPc2
pouze	pouze	k6eAd1
při	při	k7c6
speciálních	speciální	k2eAgInPc6d1
svátcích	svátek	k1gInPc6
[	[	kIx(
<g/>
Pessach	Pessach	k1gMnSc1
<g/>
,	,	kIx,
Sukot	Sukot	k1gMnSc1
a	a	k8xC
Šavuot	Šavuot	k1gMnSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
ne	ne	k9
o	o	k7c6
šabatu	šabat	k1gInSc6
[	[	kIx(
<g/>
pátek	pátek	k1gInSc4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
vyžadován	vyžadován	k2eAgInSc4d1
jen	jen	k9
chléb	chléb	k1gInSc4
a	a	k8xC
víno	víno	k1gNnSc4
<g/>
/	/	kIx~
<g/>
hroznové	hroznový	k2eAgFnSc2d1
šťávy	šťáva	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ani	ani	k8xC
není	být	k5eNaImIp3nS
Židům	Žid	k1gMnPc3
zakázáno	zakázat	k5eAaPmNgNnS
jíst	jíst	k5eAaImF
maso	maso	k1gNnSc4
<g/>
,	,	kIx,
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
středověkých	středověký	k2eAgMnPc2d1
rabínů	rabín	k1gMnPc2
(	(	kIx(
<g/>
např.	např.	kA
Josef	Josef	k1gMnSc1
Albo	alba	k1gFnSc5
a	a	k8xC
Izák	Izák	k1gMnSc1
Arama	Aram	k1gMnSc2
<g/>
)	)	kIx)
si	se	k3xPyFc3
vážilo	vážit	k5eAaImAgNnS
vegetariánství	vegetariánství	k1gNnSc1
jako	jako	k8xS,k8xC
morálního	morální	k2eAgInSc2d1
ideálu	ideál	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
nejen	nejen	k6eAd1
z	z	k7c2
důvodu	důvod	k1gInSc2
obavy	obava	k1gFnSc2
o	o	k7c4
blaho	blaho	k1gNnSc4
zvířat	zvíře	k1gNnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
porážení	porážení	k1gNnSc1
zvířat	zvíře	k1gNnPc2
může	moct	k5eAaImIp3nS
provádět	provádět	k5eAaImF
fyzická	fyzický	k2eAgFnSc1d1
osoba	osoba	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
konáním	konání	k1gNnSc7
takových	takový	k3xDgInPc2
činů	čin	k1gInPc2
rozvíjí	rozvíjet	k5eAaImIp3nS
své	svůj	k3xOyFgFnPc4
negativní	negativní	k2eAgFnPc4d1
charakterové	charakterový	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgMnSc1
z	z	k7c2
moderních	moderní	k2eAgMnPc2d1
vědců	vědec	k1gMnPc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
ve	v	k7c4
zastánce	zastánce	k1gMnPc4
vegetariánství	vegetariánství	k1gNnSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
rabín	rabín	k1gMnSc1
Abraham	Abraham	k1gMnSc1
Isaac	Isaac	k1gFnSc4
Kook	Kooka	k1gFnPc2
vrchní	vrchní	k2eAgMnSc1d1
rabín	rabín	k1gMnSc1
britské	britský	k2eAgFnSc2d1
mandátní	mandátní	k2eAgFnSc2d1
Palestiny	Palestina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
svých	svůj	k3xOyFgInPc6
spisech	spis	k1gInPc6
rabín	rabín	k1gMnSc1
Kook	Kook	k1gMnSc1
mluví	mluvit	k5eAaImIp3nS
o	o	k7c6
vegetariánství	vegetariánství	k1gNnSc6
jako	jako	k8xS,k8xC
ideálu	ideál	k1gInSc6
a	a	k8xC
poukazuje	poukazovat	k5eAaImIp3nS
na	na	k7c4
skutečnost	skutečnost	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
Adam	Adam	k1gMnSc1
neměl	mít	k5eNaImAgMnS
vzít	vzít	k5eAaPmF
maso	maso	k1gNnSc4
z	z	k7c2
těla	tělo	k1gNnSc2
zvířat	zvíře	k1gNnPc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
všichni	všechen	k3xTgMnPc1
lidé	člověk	k1gMnPc1
a	a	k8xC
zvířata	zvíře	k1gNnPc1
měla	mít	k5eAaImAgNnP
původně	původně	k6eAd1
Bohem	bůh	k1gMnSc7
přikázána	přikázán	k2eAgMnSc4d1
jíst	jíst	k5eAaImF
pouze	pouze	k6eAd1
rostliny	rostlina	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
163	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
souvislostech	souvislost	k1gFnPc6
rabín	rabín	k1gMnSc1
Kook	Kook	k1gMnSc1
vegetariánství	vegetariánství	k1gNnSc4
označuje	označovat	k5eAaImIp3nS
za	za	k7c4
eschatologický	eschatologický	k2eAgInSc4d1
ideál	ideál	k1gInSc4
<g/>
,	,	kIx,
ke	k	k7c3
kterému	který	k3yIgNnSc3,k3yRgNnSc3,k3yQgNnSc3
by	by	kYmCp3nS
se	se	k3xPyFc4
lidé	člověk	k1gMnPc1
měli	mít	k5eAaImAgMnP
vrátit	vrátit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
on	on	k3xPp3gMnSc1
osobně	osobně	k6eAd1
se	se	k3xPyFc4
zdržel	zdržet	k5eAaPmAgMnS
jedení	jedení	k1gNnSc4
masa	maso	k1gNnSc2
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
v	v	k7c4
sobotu	sobota	k1gFnSc4
a	a	k8xC
o	o	k7c6
festivalech	festival	k1gInPc6
a	a	k8xC
jeden	jeden	k4xCgMnSc1
z	z	k7c2
jeho	jeho	k3xOp3gMnPc2
hlavních	hlavní	k2eAgMnPc2d1
učedníků	učedník	k1gMnPc2
<g/>
,	,	kIx,
rabín	rabín	k1gMnSc1
David	David	k1gMnSc1
Cohen	Cohen	k2eAgMnSc1d1
<g/>
,	,	kIx,
známý	známý	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
„	„	k?
<g/>
nazarejský	nazarejský	k2eAgInSc1d1
<g/>
“	“	k?
z	z	k7c2
Jeruzaléma	Jeruzalém	k1gInSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
oddaný	oddaný	k2eAgMnSc1d1
vegetarián	vegetarián	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Několik	několik	k4yIc1
dalších	další	k2eAgInPc2d1
členů	člen	k1gInPc2
okruhu	okruh	k1gInSc2
rabína	rabín	k1gMnSc2
Kooka	Kooek	k1gMnSc2
bylo	být	k5eAaImAgNnS
také	také	k9
vegetariáni	vegetarián	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
některých	některý	k3yIgMnPc2
kabalistů	kabalista	k1gMnPc2
pouze	pouze	k6eAd1
mystikovi	mystikův	k2eAgMnPc1d1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
je	být	k5eAaImIp3nS
schopen	schopen	k2eAgMnSc1d1
vnímat	vnímat	k5eAaImF
a	a	k8xC
povýšit	povýšit	k5eAaPmF
reinkarnovanou	reinkarnovaný	k2eAgFnSc4d1
lidskou	lidský	k2eAgFnSc4d1
duši	duše	k1gFnSc4
a	a	k8xC
„	„	k?
<g/>
božské	božský	k2eAgFnSc2d1
jiskry	jiskra	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
povoleno	povolen	k2eAgNnSc1d1
konzumovat	konzumovat	k5eAaBmF
maso	maso	k1gNnSc4
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
jíst	jíst	k5eAaImF
maso	maso	k1gNnSc4
zvířete	zvíře	k1gNnSc2
by	by	kYmCp3nP
mohlo	moct	k5eAaImAgNnS
ještě	ještě	k6eAd1
způsobit	způsobit	k5eAaPmF
duchovní	duchovní	k2eAgFnPc4d1
škody	škoda	k1gFnPc4
na	na	k7c6
duši	duše	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řada	řada	k1gFnSc1
ortodoxních	ortodoxní	k2eAgFnPc2d1
židovských	židovská	k1gFnPc2
vegetariánských	vegetariánský	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
a	a	k8xC
aktivistů	aktivista	k1gMnPc2
podporuje	podporovat	k5eAaImIp3nS
tyto	tento	k3xDgFnPc4
myšlenky	myšlenka	k1gFnPc4
a	a	k8xC
věří	věřit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
halachické	halachický	k2eAgNnSc4d1
povolení	povolení	k1gNnSc4
pojídat	pojídat	k5eAaImF
maso	maso	k1gNnSc4
je	být	k5eAaImIp3nS
dočasná	dočasný	k2eAgFnSc1d1
shovívavost	shovívavost	k1gFnSc1
pro	pro	k7c4
ty	ten	k3xDgMnPc4
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
nejsou	být	k5eNaImIp3nP
hotovi	hotov	k2eAgMnPc1d1
přijmout	přijmout	k5eAaPmF
vegetariánskou	vegetariánský	k2eAgFnSc4d1
stravu	strava	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
164	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
165	#num#	k4
<g/>
]	]	kIx)
Židovský	židovský	k2eAgInSc1d1
zákon	zákon	k1gInSc1
také	také	k9
přikazuje	přikazovat	k5eAaImIp3nS
lidem	člověk	k1gMnPc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
rituálně	rituálně	k6eAd1
poráželi	porážet	k5eAaImAgMnP
zvířata	zvíře	k1gNnPc4
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yRgNnPc1,k3yQgNnPc1
zabíjí	zabíjet	k5eAaImIp3nP
<g/>
,	,	kIx,
a	a	k8xC
způsob	způsob	k1gInSc1
porážky	porážka	k1gFnSc2
detailně	detailně	k6eAd1
popisuje	popisovat	k5eAaImIp3nS
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
zvířecí	zvířecí	k2eAgFnPc1d1
oběti	oběť	k1gFnPc1
<g/>
,	,	kIx,
tak	tak	k9
běžné	běžný	k2eAgFnPc4d1
porážky	porážka	k1gFnPc4
(	(	kIx(
<g/>
šchita	šchita	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
středověkého	středověký	k2eAgMnSc2d1
mudrce	mudrc	k1gMnSc2
<g/>
,	,	kIx,
rabína	rabín	k1gMnSc2
Šlomo	Šloma	k1gFnSc5
Efraima	Efraimum	k1gNnPc1
Lunčice	Lunčice	k1gFnSc2
<g/>
,	,	kIx,
autora	autor	k1gMnSc2
komentářů	komentář	k1gInPc2
k	k	k7c3
toře	toře	k?
Kli	Kli	k1gFnSc3
jakar	jakar	k1gMnSc1
<g/>
,	,	kIx,
složitost	složitost	k1gFnSc1
těchto	tento	k3xDgInPc2
zákonů	zákon	k1gInPc2
byla	být	k5eAaImAgFnS
zamýšlena	zamýšlet	k5eAaImNgFnS
pro	pro	k7c4
odrazení	odrazení	k1gNnSc4
od	od	k7c2
konzumace	konzumace	k1gFnSc2
masa	maso	k1gNnSc2
a	a	k8xC
činění	činění	k1gNnSc2
méně	málo	k6eAd2
bolesti	bolest	k1gFnPc1
zvířatům	zvíře	k1gNnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
166	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rastafariánství	Rastafariánství	k1gNnSc1
</s>
<s>
V	v	k7c6
rámci	rámec	k1gInSc6
afrokaribské	afrokaribský	k2eAgFnSc2d1
komunity	komunita	k1gFnSc2
jsou	být	k5eAaImIp3nP
menšinou	menšina	k1gFnSc7
rastafariánci	rastafariánek	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
dodržují	dodržovat	k5eAaImIp3nP
dietní	dietní	k2eAgInPc4d1
předpisy	předpis	k1gInPc4
s	s	k7c7
různým	různý	k2eAgInSc7d1
stupněm	stupeň	k1gInSc7
přísnosti	přísnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvíce	hodně	k6eAd3,k6eAd1
ortodoxní	ortodoxní	k2eAgInPc1d1
jedí	jíst	k5eAaImIp3nP
jen	jen	k9
stravu	strava	k1gFnSc4
zvanou	zvaný	k2eAgFnSc4d1
„	„	k?
<g/>
ital	itala	k1gFnPc2
<g/>
“	“	k?
nebo	nebo	k8xC
přírodní	přírodní	k2eAgFnPc4d1
potraviny	potravina	k1gFnPc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
přiřazování	přiřazování	k1gNnSc1
bylin	bylina	k1gFnPc2
nebo	nebo	k8xC
koření	kořenit	k5eAaImIp3nS
k	k	k7c3
zelenině	zelenina	k1gFnSc3
je	být	k5eAaImIp3nS
výsledkem	výsledek	k1gInSc7
dlouhé	dlouhý	k2eAgFnSc2d1
tradice	tradice	k1gFnSc2
pocházející	pocházející	k2eAgFnSc6d1
z	z	k7c2
afrického	africký	k2eAgInSc2d1
původu	původ	k1gInSc2
a	a	k8xC
kulturního	kulturní	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
rastafariánství	rastafariánství	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
167	#num#	k4
<g/>
]	]	kIx)
„	„	k?
<g/>
Ital	Ital	k1gMnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
odvozen	odvodit	k5eAaPmNgInS
od	od	k7c2
slova	slovo	k1gNnSc2
zásadní	zásadní	k2eAgInPc4d1
<g/>
,	,	kIx,
znamená	znamenat	k5eAaImIp3nS
zásadní	zásadní	k2eAgInSc4d1
význam	význam	k1gInSc4
pro	pro	k7c4
lidskou	lidský	k2eAgFnSc4d1
existence	existence	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vaření	vaření	k1gNnPc2
jídel	jídlo	k1gNnPc2
„	„	k?
<g/>
ital	ital	k1gMnSc1
<g/>
“	“	k?
v	v	k7c6
užším	úzký	k2eAgInSc6d2
podobě	podoba	k1gFnSc6
zakazuje	zakazovat	k5eAaImIp3nS
používání	používání	k1gNnSc1
soli	sůl	k1gFnSc2
<g/>
,	,	kIx,
masa	maso	k1gNnSc2
(	(	kIx(
<g/>
především	především	k6eAd1
vepřového	vepřový	k2eAgNnSc2d1
masa	maso	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
konzervačních	konzervační	k2eAgFnPc2d1
látek	látka	k1gFnPc2
<g/>
,	,	kIx,
barviv	barvivo	k1gNnPc2
<g/>
,	,	kIx,
látek	látka	k1gFnPc2
určené	určený	k2eAgInPc4d1
k	k	k7c3
aromatizaci	aromatizace	k1gFnSc3
a	a	k8xC
všeho	všecek	k3xTgNnSc2
umělého	umělý	k2eAgNnSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
168	#num#	k4
<g/>
]	]	kIx)
Nejvíce	hodně	k6eAd3,k6eAd1
rastafariánců	rastafariánec	k1gMnPc2
jsou	být	k5eAaImIp3nP
vegetariáni	vegetarián	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
169	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sikhismus	Sikhismus	k1gInSc1
</s>
<s>
Principy	princip	k1gInPc1
sikhismu	sikhismus	k1gInSc2
neprosazují	prosazovat	k5eNaImIp3nP
zvláštní	zvláštní	k2eAgInSc4d1
postoj	postoj	k1gInSc4
k	k	k7c3
vegetariánství	vegetariánství	k1gNnSc3
<g/>
,	,	kIx,
ani	ani	k8xC
ke	k	k7c3
konzumaci	konzumace	k1gFnSc3
masa	maso	k1gNnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
170	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
171	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
172	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
173	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
ponechávají	ponechávat	k5eAaImIp3nP
na	na	k7c6
jednotlivci	jednotlivec	k1gMnSc6
rozhodnutí	rozhodnutí	k1gNnSc3
o	o	k7c6
jeho	jeho	k3xOp3gFnSc6
stravě	strava	k1gFnSc6
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
174	#num#	k4
<g/>
]	]	kIx)
Desátý	desátý	k4xOgMnSc1
guru	guru	k1gMnSc1
Guru	guru	k1gMnSc1
Gobind	Gobind	k1gMnSc1
Singh	Singh	k1gMnSc1
na	na	k7c6
přelomu	přelom	k1gInSc6
17	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
avšak	avšak	k8xC
zakázal	zakázat	k5eAaPmAgInS
„	„	k?
<g/>
amritdharským	amritdharský	k2eAgFnPc3d1
<g/>
“	“	k?
Sikhům	sikh	k1gMnPc3
nebo	nebo	k8xC
těm	ten	k3xDgFnPc3
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
následují	následovat	k5eAaImIp3nP
Sikh	sikh	k1gMnSc1
Rehat	Rehat	k1gInSc4
marjádá	marjádat	k5eAaPmIp3nS,k5eAaImIp3nS
(	(	kIx(
<g/>
oficiální	oficiální	k2eAgInSc1d1
sikhský	sikhský	k2eAgInSc1d1
etický	etický	k2eAgInSc1d1
kodex	kodex	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
175	#num#	k4
<g/>
]	]	kIx)
jíst	jíst	k5eAaImF
tzv.	tzv.	kA
kutha	kutha	k1gFnSc1
maso	maso	k1gNnSc1
nebo	nebo	k8xC
maso	maso	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
bylo	být	k5eAaImAgNnS
získáno	získat	k5eAaPmNgNnS
ze	z	k7c2
zvířat	zvíře	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yQgNnPc1,k3yRgNnPc1
mají	mít	k5eAaImIp3nP
byl	být	k5eAaImAgInS
zabity	zabit	k2eAgMnPc4d1
rituálním	rituální	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
je	být	k5eAaImIp3nS
chápáno	chápat	k5eAaImNgNnS
jako	jako	k8xC,k8xS
politický	politický	k2eAgInSc1d1
důvod	důvod	k1gInSc1
pro	pro	k7c4
zachování	zachování	k1gNnSc4
nezávislosti	nezávislost	k1gFnSc2
na	na	k7c6
tehdy	tehdy	k6eAd1
nové	nový	k2eAgFnSc3d1
muslimské	muslimský	k2eAgFnSc3d1
nadvládě	nadvláda	k1gFnSc3
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
muslimové	muslim	k1gMnPc1
začali	začít	k5eAaPmAgMnP
do	do	k7c2
značné	značný	k2eAgFnSc2d1
míry	míra	k1gFnSc2
držet	držet	k5eAaImF
rituální	rituální	k2eAgInSc4d1
halal	halal	k1gInSc4
dietu	dieta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Životní	životní	k2eAgNnSc1d1
prostředí	prostředí	k1gNnSc1
<g/>
,	,	kIx,
klima	klima	k1gNnSc1
a	a	k8xC
stravování	stravování	k1gNnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Dopad	dopad	k1gInSc1
produkce	produkce	k1gFnSc2
masa	maso	k1gNnSc2
na	na	k7c4
životní	životní	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Produkce	produkce	k1gFnSc1
emisí	emise	k1gFnPc2
skleníkových	skleníkový	k2eAgInPc2d1
plynů	plyn	k1gInPc2
pro	pro	k7c4
různé	různý	k2eAgInPc4d1
typy	typ	k1gInPc4
výživy	výživa	k1gFnSc2
ve	v	k7c6
Spojeném	spojený	k2eAgNnSc6d1
království	království	k1gNnSc6
<g/>
[	[	kIx(
<g/>
176	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
SkupinaEmise	SkupinaEmise	k1gFnSc1
na	na	k7c4
den	den	k1gInSc4
<g/>
[	[	kIx(
<g/>
kg	kg	kA
CO	co	k8xS
<g/>
2	#num#	k4
<g/>
-ekvivalent	-ekvivalent	k1gInSc1
<g/>
]	]	kIx)
</s>
<s>
vysocí	vysoký	k2eAgMnPc1d1
spotřebitelé	spotřebitel	k1gMnPc1
masa	maso	k1gNnSc2
(	(	kIx(
<g/>
≥	≥	k?
100	#num#	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
d	d	k?
<g/>
)	)	kIx)
</s>
<s>
7,2	7,2	k4
</s>
<s>
střední	střední	k2eAgMnPc1d1
spotřebitelé	spotřebitel	k1gMnPc1
masa	maso	k1gNnSc2
(	(	kIx(
<g/>
50	#num#	k4
<g/>
–	–	k?
<g/>
99	#num#	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
d	d	k?
<g/>
)	)	kIx)
</s>
<s>
5,6	5,6	k4
</s>
<s>
spotřebitelé	spotřebitel	k1gMnPc1
málo	málo	k1gNnSc4
masa	maso	k1gNnSc2
(	(	kIx(
<g/>
<	<	kIx(
50	#num#	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
d	d	k?
<g/>
)	)	kIx)
</s>
<s>
4,7	4,7	k4
</s>
<s>
jedlíci	jedlík	k1gMnPc1
ryb	ryba	k1gFnPc2
</s>
<s>
3,9	3,9	k4
</s>
<s>
vegetariáni	vegetarián	k1gMnPc1
</s>
<s>
3,8	3,8	k4
</s>
<s>
vegani	vegan	k1gMnPc1
</s>
<s>
2,9	2,9	k4
</s>
<s>
Vegetariánství	vegetariánství	k1gNnSc1
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
(	(	kIx(
<g/>
environmentální	environmentální	k2eAgNnSc1d1
vegetariánství	vegetariánství	k1gNnSc1
<g/>
)	)	kIx)
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
obavy	obava	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
výroba	výroba	k1gFnSc1
masa	maso	k1gNnSc2
a	a	k8xC
živočišných	živočišný	k2eAgInPc2d1
produktů	produkt	k1gInPc2
pro	pro	k7c4
masovou	masový	k2eAgFnSc4d1
spotřebu	spotřeba	k1gFnSc4
<g/>
,	,	kIx,
zejména	zejména	k9
prostřednictvím	prostřednictvím	k7c2
velkochovů	velkochov	k1gInPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
z	z	k7c2
hlediska	hledisko	k1gNnSc2
ochrany	ochrana	k1gFnSc2
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
neudržitelná	udržitelný	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
iniciativy	iniciativa	k1gFnSc2
Organizace	organizace	k1gFnSc2
spojených	spojený	k2eAgInPc2d1
národů	národ	k1gInPc2
z	z	k7c2
roku	rok	k1gInSc2
2006	#num#	k4
je	být	k5eAaImIp3nS
živočišná	živočišný	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
jedním	jeden	k4xCgMnSc7
z	z	k7c2
největších	veliký	k2eAgMnPc2d3
přispěvatelů	přispěvatel	k1gMnPc2
ke	k	k7c3
zhoršování	zhoršování	k1gNnSc3
stavu	stav	k1gInSc2
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
na	na	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
a	a	k8xC
moderní	moderní	k2eAgInSc4d1
postupy	postup	k1gInPc4
chovu	chov	k1gInSc2
zvířat	zvíře	k1gNnPc2
na	na	k7c4
potraviny	potravina	k1gFnPc4
přispívají	přispívat	k5eAaImIp3nP
v	v	k7c6
„	„	k?
<g/>
masovém	masový	k2eAgNnSc6d1
měřítku	měřítko	k1gNnSc6
<g/>
“	“	k?
ke	k	k7c3
znečišťování	znečišťování	k1gNnSc3
vzduchu	vzduch	k1gInSc2
a	a	k8xC
vody	voda	k1gFnSc2
<g/>
,	,	kIx,
znehodnocování	znehodnocování	k1gNnSc4
půdy	půda	k1gFnSc2
<g/>
,	,	kIx,
změně	změna	k1gFnSc3
klimatu	klima	k1gNnSc2
a	a	k8xC
ztrátě	ztráta	k1gFnSc3
biodiverzity	biodiverzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Iniciativa	iniciativa	k1gFnSc1
došla	dojít	k5eAaPmAgFnS
k	k	k7c3
závěru	závěr	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
sektor	sektor	k1gInSc4
živočišné	živočišný	k2eAgFnSc2d1
výroby	výroba	k1gFnSc2
se	se	k3xPyFc4
jeví	jevit	k5eAaImIp3nS
jako	jako	k9
jeden	jeden	k4xCgInSc1
ze	z	k7c2
dvou	dva	k4xCgNnPc2
nebo	nebo	k8xC
ze	z	k7c2
tří	tři	k4xCgMnPc2
nejvýznamnějších	významný	k2eAgMnPc2d3
přispěvatelů	přispěvatel	k1gMnPc2
k	k	k7c3
nejvážnějším	vážní	k2eAgInPc3d3
problémům	problém	k1gInPc3
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
na	na	k7c6
všech	všecek	k3xTgFnPc6
úrovních	úroveň	k1gFnPc6
<g/>
,	,	kIx,
od	od	k7c2
místní	místní	k2eAgFnSc2d1
až	až	k6eAd1
po	po	k7c6
globální	globální	k2eAgFnSc6d1
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
177	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kromě	kromě	k7c2
toho	ten	k3xDgNnSc2
je	být	k5eAaImIp3nS
živočišná	živočišný	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
velkým	velký	k2eAgInSc7d1
zdrojem	zdroj	k1gInSc7
emisí	emise	k1gFnPc2
skleníkových	skleníkový	k2eAgInPc2d1
plynů	plyn	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
zprávy	zpráva	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
2006	#num#	k4
je	být	k5eAaImIp3nS
zodpovědná	zodpovědný	k2eAgFnSc1d1
za	za	k7c4
18	#num#	k4
%	%	kIx~
emisí	emise	k1gFnPc2
skleníkových	skleníkový	k2eAgInPc2d1
plynů	plyn	k1gInPc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
bylo	být	k5eAaImAgNnS
odhadnuto	odhadnout	k5eAaPmNgNnS
pro	pro	k7c4
stoleté	stoletý	k2eAgMnPc4d1
CO2	CO2	k1gMnPc4
ekvivalenty	ekvivalent	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdroje	zdroj	k1gInSc2
chovu	chov	k1gInSc2
hospodářských	hospodářský	k2eAgNnPc2d1
zvířat	zvíře	k1gNnPc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
střevní	střevní	k2eAgFnSc2d1
fermentace	fermentace	k1gFnSc2
a	a	k8xC
hnoje	hnůj	k1gInSc2
<g/>
)	)	kIx)
tvoří	tvořit	k5eAaImIp3nS
asi	asi	k9
3,1	3,1	k4
procenta	procento	k1gNnSc2
amerických	americký	k2eAgFnPc2d1
antropogenních	antropogenní	k2eAgFnPc2d1
emisí	emise	k1gFnPc2
skleníkových	skleníkový	k2eAgInPc2d1
plynů	plyn	k1gInPc2
<g/>
,	,	kIx,
vyjádřených	vyjádřený	k2eAgNnPc6d1
v	v	k7c6
ekvivalentu	ekvivalent	k1gInSc6
oxidu	oxid	k1gInSc2
uhličitého	uhličitý	k2eAgInSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
178	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgInSc1
odhad	odhad	k1gInSc1
Agentury	agentura	k1gFnSc2
pro	pro	k7c4
ochranu	ochrana	k1gFnSc4
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
je	být	k5eAaImIp3nS
založen	založit	k5eAaPmNgInS
na	na	k7c6
metodách	metoda	k1gFnPc6
odsouhlasených	odsouhlasený	k2eAgFnPc2d1
Rámcovou	rámcový	k2eAgFnSc7d1
úmluvou	úmluva	k1gFnSc7
OSN	OSN	kA
o	o	k7c6
změně	změna	k1gFnSc6
klimatu	klima	k1gNnSc2
se	s	k7c7
100	#num#	k4
<g/>
letými	letý	k2eAgInPc7d1
potenciály	potenciál	k1gInPc7
globálního	globální	k2eAgNnSc2d1
oteplování	oteplování	k1gNnSc2
z	z	k7c2
tzv.	tzv.	kA
Druhé	druhý	k4xOgFnPc1
hodnotící	hodnotící	k2eAgFnPc1d1
zprávy	zpráva	k1gFnPc1
Mezivládního	mezivládní	k2eAgInSc2d1
panelu	panel	k1gInSc2
pro	pro	k7c4
změnu	změna	k1gFnSc4
klimatu	klima	k1gNnSc2
používané	používaný	k2eAgInPc4d1
při	při	k7c6
odhadu	odhad	k1gInSc6
emisí	emise	k1gFnPc2
skleníkových	skleníkový	k2eAgInPc2d1
plynů	plyn	k1gInPc2
v	v	k7c6
ekvivalentech	ekvivalent	k1gInPc6
oxidu	oxid	k1gInSc2
uhličitého	uhličitý	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s>
Zvláštní	zvláštní	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
Mezivládního	mezivládní	k2eAgInSc2d1
panelu	panel	k1gInSc2
pro	pro	k7c4
změny	změna	k1gFnPc4
klimatu	klima	k1gNnSc2
o	o	k7c6
změně	změna	k1gFnSc6
klimatu	klima	k1gNnSc6
<g/>
,	,	kIx,
krajině	krajina	k1gFnSc6
a	a	k8xC
půdě	půda	k1gFnSc6
schválené	schválený	k2eAgFnSc6d1
v	v	k7c6
srpnu	srpen	k1gInSc6
2019	#num#	k4
mluví	mluvit	k5eAaImIp3nS
o	o	k7c6
spotřebě	spotřeba	k1gFnSc6
masa	maso	k1gNnSc2
na	na	k7c6
Zemi	zem	k1gFnSc6
<g/>
,	,	kIx,
o	o	k7c6
emisích	emise	k1gFnPc6
metanu	metan	k1gInSc2
z	z	k7c2
chovu	chov	k1gInSc2
přežvýkavců	přežvýkavec	k1gMnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
oproti	oproti	k7c3
roku	rok	k1gInSc3
1961	#num#	k4
zvýšily	zvýšit	k5eAaPmAgInP
1,7	1,7	k4
<g/>
krát	krát	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
179	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
]	]	kIx)
Pamela	Pamela	k1gFnSc1
McElwee	McElwee	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgMnSc7
ze	z	k7c2
spoluautorů	spoluautor	k1gMnPc2
kapitoly	kapitola	k1gFnSc2
6	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
pracuje	pracovat	k5eAaImIp3nS
na	na	k7c6
Katedře	katedra	k1gFnSc6
lidské	lidský	k2eAgFnSc2d1
ekologie	ekologie	k1gFnSc2
Rutgersovy	Rutgersův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
na	na	k7c6
Fakultě	fakulta	k1gFnSc6
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
a	a	k8xC
biologických	biologický	k2eAgFnPc2d1
věd	věda	k1gFnPc2
uvedla	uvést	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
změna	změna	k1gFnSc1
stravovacích	stravovací	k2eAgInPc2d1
návyků	návyk	k1gInPc2
<g/>
“	“	k?
-	-	kIx~
„	„	k?
<g/>
zejména	zejména	k9
ve	v	k7c6
vyspělých	vyspělý	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
Západu	západ	k1gInSc2
<g/>
“	“	k?
<g />
.	.	kIx.
</s>
<s hack="1">
-	-	kIx~
může	moct	k5eAaImIp3nS
vést	vést	k5eAaImF
ke	k	k7c3
snižování	snižování	k1gNnSc3
„	„	k?
<g/>
nadměrné	nadměrný	k2eAgFnSc2d1
<g/>
“	“	k?
spotřeby	spotřeba	k1gFnSc2
<g/>
,	,	kIx,
k	k	k7c3
omezení	omezení	k1gNnSc3
„	„	k?
<g/>
jídelníčků	jídelníček	k1gInPc2
s	s	k7c7
vysokými	vysoký	k2eAgFnPc7d1
emisemi	emise	k1gFnPc7
skleníkových	skleníkový	k2eAgInPc2d1
plynů	plyn	k1gInPc2
<g/>
“	“	k?
s	s	k7c7
vysokou	vysoký	k2eAgFnSc7d1
spotřebou	spotřeba	k1gFnSc7
jehněčího	jehněčí	k1gNnSc2
a	a	k8xC
hovězího	hovězí	k1gNnSc2
je	být	k5eAaImIp3nS
„	„	k?
<g/>
oboustranně	oboustranně	k6eAd1
výhodné	výhodný	k2eAgNnSc4d1
<g/>
“	“	k?
<g/>
,	,	kIx,
protože	protože	k8xS
řeší	řešit	k5eAaImIp3nP
současně	současně	k6eAd1
klimatickou	klimatický	k2eAgFnSc4d1
krizi	krize	k1gFnSc4
i	i	k8xC
zdravotní	zdravotní	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
180	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
181	#num#	k4
<g/>
]	]	kIx)
McElwee	McElwee	k1gFnSc1
řekla	říct	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
bezpečnost	bezpečnost	k1gFnSc1
potravin	potravina	k1gFnPc2
je	být	k5eAaImIp3nS
hlavním	hlavní	k2eAgInSc7d1
problémem	problém	k1gInSc7
<g/>
,	,	kIx,
protože	protože	k8xS
existuje	existovat	k5eAaImIp3nS
potenciál	potenciál	k1gInSc4
pro	pro	k7c4
„	„	k?
<g/>
potravinové	potravinový	k2eAgFnPc4d1
krize	krize	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
mohou	moct	k5eAaImIp3nP
vzniknout	vzniknout	k5eAaPmF
<g/>
“	“	k?
na	na	k7c6
několika	několik	k4yIc6
kontinentech	kontinent	k1gInPc6
najednou	najednou	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpráva	zpráva	k1gFnSc1
navrhuje	navrhovat	k5eAaImIp3nS
řešení	řešení	k1gNnSc1
zahrnující	zahrnující	k2eAgNnSc1d1
„	„	k?
<g/>
zvýšení	zvýšení	k1gNnSc1
produktivity	produktivita	k1gFnSc2
půdy	půda	k1gFnSc2
<g/>
,	,	kIx,
menší	malý	k2eAgNnSc1d2
plýtvání	plýtvání	k1gNnSc1
potravinami	potravina	k1gFnPc7
a	a	k8xC
přesvědčování	přesvědčování	k1gNnSc1
více	hodně	k6eAd2
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
přesunuli	přesunout	k5eAaPmAgMnP
svůj	svůj	k3xOyFgInSc4
jídelníček	jídelníček	k1gInSc4
od	od	k7c2
hovězího	hovězí	k1gNnSc2
a	a	k8xC
jiných	jiný	k2eAgInPc2d1
druhů	druh	k1gInPc2
masa	maso	k1gNnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
181	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
zprávy	zpráva	k1gFnSc2
z	z	k7c2
8	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2019	#num#	k4
PBS	PBS	kA
NewsHour	NewsHoura	k1gFnPc2
by	by	kYmCp3nS
asi	asi	k9
15	#num#	k4
%	%	kIx~
současných	současný	k2eAgFnPc2d1
emisí	emise	k1gFnPc2
mohlo	moct	k5eAaImAgNnS
být	být	k5eAaImF
sníženo	snížit	k5eAaPmNgNnS
do	do	k7c2
poloviny	polovina	k1gFnSc2
století	století	k1gNnSc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
„	„	k?
<g/>
lidé	člověk	k1gMnPc1
změní	změnit	k5eAaPmIp3nP
stravu	strava	k1gFnSc4
<g/>
,	,	kIx,
sníží	snížit	k5eAaPmIp3nS
červené	červený	k2eAgNnSc1d1
maso	maso	k1gNnSc1
a	a	k8xC
zvýší	zvýšit	k5eAaPmIp3nS
rostlinnou	rostlinný	k2eAgFnSc4d1
stravu	strava	k1gFnSc4
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
ovoce	ovoce	k1gNnSc1
<g/>
,	,	kIx,
zelenina	zelenina	k1gFnSc1
a	a	k8xC
semena	semeno	k1gNnPc4
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
182	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Maso	maso	k1gNnSc4
vyrobené	vyrobený	k2eAgNnSc4d1
v	v	k7c6
laboratoři	laboratoř	k1gFnSc6
(	(	kIx(
<g/>
nazývané	nazývaný	k2eAgFnSc6d1
in	in	k?
vitro	vitro	k6eAd1
maso	maso	k1gNnSc1
<g/>
)	)	kIx)
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
udržitelnější	udržitelný	k2eAgFnSc1d2
než	než	k8xS
regulární	regulární	k2eAgFnSc1d1
produkce	produkce	k1gFnSc1
masa	maso	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
183	#num#	k4
<g/>
]	]	kIx)
Reakce	reakce	k1gFnPc1
vegetariánů	vegetarián	k1gMnPc2
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
184	#num#	k4
<g/>
]	]	kIx)
Odchov	odchov	k1gInSc4
relativně	relativně	k6eAd1
malého	malý	k2eAgInSc2d1
počtu	počet	k1gInSc2
pasoucích	pasoucí	k2eAgNnPc2d1
se	se	k3xPyFc4
zvířat	zvíře	k1gNnPc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
prospěšný	prospěšný	k2eAgMnSc1d1
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
jak	jak	k8xS,k8xC
říká	říkat	k5eAaImIp3nS
zpráva	zpráva	k1gFnSc1
Food	Fooda	k1gFnPc2
Climate	Climat	k1gInSc5
Research	Resear	k1gFnPc6
Network	network	k1gInSc1
z	z	k7c2
anglické	anglický	k2eAgFnSc2d1
Univerzity	univerzita	k1gFnSc2
v	v	k7c4
Surrey	Surrey	k1gInPc4
<g/>
:	:	kIx,
„	„	k?
<g/>
Trocha	trocha	k1gFnSc1
živočišné	živočišný	k2eAgFnSc2d1
výroby	výroba	k1gFnSc2
je	být	k5eAaImIp3nS
pravděpodobně	pravděpodobně	k6eAd1
dobrá	dobrý	k2eAgFnSc1d1
pro	pro	k7c4
životní	životní	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
185	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
květnu	květen	k1gInSc6
2009	#num#	k4
byl	být	k5eAaImAgMnS
belgický	belgický	k2eAgMnSc1d1
Gent	Gent	k1gMnSc1
vyhlášen	vyhlásit	k5eAaPmNgMnS
jako	jako	k9
„	„	k?
<g/>
první	první	k4xOgNnSc4
[	[	kIx(
<g/>
město	město	k1gNnSc1
<g/>
]	]	kIx)
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
z	z	k7c2
ekologických	ekologický	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
provozuje	provozovat	k5eAaImIp3nS
vegetariánství	vegetariánství	k1gNnSc1
alespoň	alespoň	k9
jednou	jednou	k6eAd1
týdně	týdně	k6eAd1
<g/>
“	“	k?
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
místní	místní	k2eAgInPc1d1
úřady	úřad	k1gInPc1
se	se	k3xPyFc4
rozhodly	rozhodnout	k5eAaPmAgInP
zavést	zavést	k5eAaPmF
„	„	k?
<g/>
bezmasý	bezmasý	k2eAgInSc4d1
den	den	k1gInSc4
jednou	jednou	k6eAd1
týdně	týdně	k6eAd1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Státní	státní	k2eAgMnPc1d1
úředníci	úředník	k1gMnPc1
by	by	kYmCp3nP
mohli	moct	k5eAaImAgMnP
jíst	jíst	k5eAaImF
vegetariánská	vegetariánský	k2eAgNnPc4d1
jídla	jídlo	k1gNnPc4
jeden	jeden	k4xCgInSc4
den	den	k1gInSc4
v	v	k7c6
týdnu	týden	k1gInSc6
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
uznává	uznávat	k5eAaImIp3nS
zpráva	zpráva	k1gFnSc1
OSN	OSN	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místní	místní	k2eAgInPc1d1
orgány	orgán	k1gInPc1
představily	představit	k5eAaPmAgInP
plakáty	plakát	k1gInPc1
s	s	k7c7
cílem	cíl	k1gInSc7
podpořit	podpořit	k5eAaPmF
obyvatelstvo	obyvatelstvo	k1gNnSc4
k	k	k7c3
účasti	účast	k1gFnSc3
na	na	k7c6
vegetariánských	vegetariánský	k2eAgInPc6d1
dnech	den	k1gInPc6
a	a	k8xC
byla	být	k5eAaImAgFnS
vytištěna	vytištěn	k2eAgFnSc1d1
„	„	k?
<g/>
vegetariánská	vegetariánský	k2eAgFnSc1d1
uliční	uliční	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
<g/>
“	“	k?
pro	pro	k7c4
propagaci	propagace	k1gFnSc4
vegetariánských	vegetariánský	k2eAgFnPc2d1
restaurací	restaurace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Školy	škola	k1gFnSc2
v	v	k7c6
Gentu	Gent	k1gInSc6
byly	být	k5eAaImAgFnP
zapojeny	zapojit	k5eAaPmNgFnP
do	do	k7c2
týdenního	týdenní	k2eAgNnSc2d1
veggiedag	veggiedaga	k1gFnPc2
(	(	kIx(
<g/>
„	„	k?
<g/>
vegetariánský	vegetariánský	k2eAgInSc4d1
den	den	k1gInSc4
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
186	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
187	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podle	podle	k7c2
časopisu	časopis	k1gInSc2
The	The	k1gMnSc1
American	American	k1gMnSc1
Journal	Journal	k1gMnSc1
of	of	k?
Clinical	Clinical	k1gFnSc1
Nutrition	Nutrition	k1gInSc1
spotřebuje	spotřebovat	k5eAaPmIp3nS
živočišná	živočišný	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
<g/>
,	,	kIx,
zejména	zejména	k9
výroba	výroba	k1gFnSc1
masa	maso	k1gNnSc2
<g/>
,	,	kIx,
17	#num#	k4
<g/>
krát	krát	k6eAd1
více	hodně	k6eAd2
půdy	půda	k1gFnSc2
<g/>
,	,	kIx,
14	#num#	k4
<g/>
krát	krát	k6eAd1
více	hodně	k6eAd2
vody	voda	k1gFnSc2
a	a	k8xC
10	#num#	k4
<g/>
krát	krát	k6eAd1
více	hodně	k6eAd2
energie	energie	k1gFnSc1
než	než	k8xS
výroba	výroba	k1gFnSc1
vegetariánských	vegetariánský	k2eAgFnPc2d1
a	a	k8xC
veganských	veganský	k2eAgFnPc2d1
potravin	potravina	k1gFnPc2
srovnatelného	srovnatelný	k2eAgInSc2d1
rozsahu	rozsah	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
188	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pracovní	pracovní	k2eAgFnPc1d1
podmínky	podmínka	k1gFnPc1
a	a	k8xC
stravování	stravování	k1gNnSc1
</s>
<s>
Některé	některý	k3yIgFnPc1
skupiny	skupina	k1gFnPc1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
například	například	k6eAd1
PETA	PETA	kA
<g/>
,	,	kIx,
propagují	propagovat	k5eAaImIp3nP
vegetariánství	vegetariánství	k1gNnSc4
jako	jako	k8xC,k8xS
způsob	způsob	k1gInSc4
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
kompenzovat	kompenzovat	k5eAaBmF
špatné	špatný	k2eAgNnSc4d1
zacházení	zacházení	k1gNnSc4
a	a	k8xC
špatné	špatný	k2eAgFnPc4d1
pracovní	pracovní	k2eAgFnPc4d1
podmínky	podmínka	k1gFnPc4
pracovníků	pracovník	k1gMnPc2
v	v	k7c6
současném	současný	k2eAgInSc6d1
masném	masný	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
189	#num#	k4
<g/>
]	]	kIx)
Tyto	tento	k3xDgFnPc1
skupiny	skupina	k1gFnPc1
citují	citovat	k5eAaBmIp3nP
studie	studie	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
ukazují	ukazovat	k5eAaImIp3nP
psychologické	psychologický	k2eAgFnPc4d1
škody	škoda	k1gFnPc4
způsobené	způsobený	k2eAgFnPc4d1
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
že	že	k8xS
lidé	člověk	k1gMnPc1
pracují	pracovat	k5eAaImIp3nP
v	v	k7c6
masném	masný	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
zejména	zejména	k9
ve	v	k7c6
výrobním	výrobní	k2eAgInSc6d1
a	a	k8xC
průmyslovém	průmyslový	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
<g/>
,	,	kIx,
a	a	k8xC
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
masný	masný	k2eAgInSc1d1
průmysl	průmysl	k1gInSc1
porušuje	porušovat	k5eAaImIp3nS
lidská	lidský	k2eAgNnPc4d1
práva	právo	k1gNnPc4
svých	svůj	k3xOyFgMnPc2
dělníků	dělník	k1gMnPc2
„	„	k?
<g/>
přidělením	přidělení	k1gNnSc7
obtížných	obtížný	k2eAgInPc2d1
a	a	k8xC
stresujících	stresující	k2eAgInPc2d1
úkolů	úkol	k1gInPc2
bez	bez	k7c2
adekvátního	adekvátní	k2eAgNnSc2d1
poradenství	poradenství	k1gNnSc2
<g/>
,	,	kIx,
školení	školení	k1gNnSc2
a	a	k8xC
zpětného	zpětný	k2eAgInSc2d1
rozboru	rozbor	k1gInSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
190	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
191	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
192	#num#	k4
<g/>
]	]	kIx)
Nicméně	nicméně	k8xC
pracovní	pracovní	k2eAgFnPc4d1
podmínky	podmínka	k1gFnPc4
pracovníků	pracovník	k1gMnPc2
<g/>
,	,	kIx,
zejména	zejména	k9
nestálých	stálý	k2eNgMnPc2d1
pracovníků	pracovník	k1gMnPc2
<g/>
,	,	kIx,
v	v	k7c6
zemědělství	zemědělství	k1gNnSc6
jako	jako	k8xC,k8xS
celku	celek	k1gInSc6
zůstávají	zůstávat	k5eAaImIp3nP
slabé	slabý	k2eAgFnPc1d1
a	a	k8xC
hluboko	hluboko	k6eAd1
pod	pod	k7c7
podmínkami	podmínka	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
panují	panovat	k5eAaImIp3nP
v	v	k7c6
jiných	jiný	k2eAgNnPc6d1
hospodářských	hospodářský	k2eAgNnPc6d1
odvětvích	odvětví	k1gNnPc6
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
[	[	kIx(
<g/>
193	#num#	k4
<g/>
]	]	kIx)
Nehody	nehoda	k1gFnSc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
otrav	otrava	k1gFnPc2
pesticidy	pesticid	k1gInPc1
<g/>
,	,	kIx,
mezi	mezi	k7c7
zemědělci	zemědělec	k1gMnPc7
a	a	k8xC
pracovníky	pracovník	k1gMnPc7
na	na	k7c6
plantážích	plantáž	k1gFnPc6
přispívají	přispívat	k5eAaImIp3nP
ke	k	k7c3
zvýšení	zvýšení	k1gNnSc3
zdravotních	zdravotní	k2eAgNnPc2d1
rizik	riziko	k1gNnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
zvýšené	zvýšený	k2eAgFnSc2d1
úmrtnosti	úmrtnost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
194	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
Mezinárodní	mezinárodní	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
práce	práce	k1gFnSc2
je	být	k5eAaImIp3nS
zemědělství	zemědělství	k1gNnSc1
jedním	jeden	k4xCgInSc7
ze	z	k7c2
tří	tři	k4xCgNnPc2
nejnebezpečnějších	bezpečný	k2eNgNnPc2d3
pracovních	pracovní	k2eAgNnPc2d1
míst	místo	k1gNnPc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
195	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1
a	a	k8xC
strava	strava	k1gFnSc1
</s>
<s>
Koncept	koncept	k1gInSc1
ekonomického	ekonomický	k2eAgNnSc2d1
vegetariánství	vegetariánství	k1gNnSc2
je	být	k5eAaImIp3nS
podobný	podobný	k2eAgInSc1d1
environmentálnímu	environmentální	k2eAgNnSc3d1
vegetariánství	vegetariánství	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ekonomický	ekonomický	k2eAgInSc1d1
vegetarián	vegetarián	k1gMnSc1
je	být	k5eAaImIp3nS
někdo	někdo	k3yInSc1
<g/>
,	,	kIx,
kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
praktikuje	praktikovat	k5eAaImIp3nS
vegetariánství	vegetariánství	k1gNnSc4
buď	buď	k8xC
z	z	k7c2
filozofického	filozofický	k2eAgInSc2d1
hledisko	hledisko	k1gNnSc1
týkající	týkající	k2eAgMnSc1d1
se	se	k3xPyFc4
otázek	otázka	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
veřejné	veřejný	k2eAgNnSc4d1
zdraví	zdraví	k1gNnSc4
a	a	k8xC
zamezení	zamezení	k1gNnSc4
světového	světový	k2eAgNnSc2d1
hladovění	hladovění	k1gNnSc2
<g/>
,	,	kIx,
či	či	k8xC
z	z	k7c2
přesvědčení	přesvědčení	k1gNnSc2
<g/>
,	,	kIx,
že	že	k8xS
konzumace	konzumace	k1gFnSc1
masa	maso	k1gNnSc2
je	být	k5eAaImIp3nS
ekonomicky	ekonomicky	k6eAd1
nezdravá	zdravý	k2eNgFnSc1d1
<g/>
,	,	kIx,
součásti	součást	k1gFnPc1
vědomé	vědomý	k2eAgFnSc2d1
strategie	strategie	k1gFnSc2
žití	žití	k1gNnSc2
v	v	k7c6
jednoduchosti	jednoduchost	k1gFnSc6
nebo	nebo	k8xC
jen	jen	k9
z	z	k7c2
nutnosti	nutnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Worldwatch	Worldwatch	k1gInSc1
Institute	institut	k1gInSc5
říká	říkat	k5eAaImIp3nS
<g/>
:	:	kIx,
„	„	k?
<g/>
Masivní	masivní	k2eAgNnSc4d1
snížení	snížení	k1gNnSc4
spotřeby	spotřeba	k1gFnSc2
masa	maso	k1gNnSc2
v	v	k7c6
průmyslových	průmyslový	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
zmírní	zmírnit	k5eAaPmIp3nS
zátěž	zátěž	k1gFnSc1
jejich	jejich	k3xOp3gFnSc2
zdravotní	zdravotní	k2eAgFnSc2d1
péče	péče	k1gFnSc2
při	při	k7c6
současném	současný	k2eAgNnSc6d1
zlepšení	zlepšení	k1gNnSc6
veřejného	veřejný	k2eAgNnSc2d1
zdraví	zdraví	k1gNnSc2
<g/>
;	;	kIx,
klesající	klesající	k2eAgNnPc4d1
stáda	stádo	k1gNnPc4
hospodářských	hospodářský	k2eAgNnPc2d1
zvířat	zvíře	k1gNnPc2
bude	být	k5eAaImBp3nS
snižovat	snižovat	k5eAaImF
tlak	tlak	k1gInSc4
na	na	k7c4
pastviny	pastvina	k1gFnPc4
a	a	k8xC
na	na	k7c6
tzv.	tzv.	kA
grainlands	grainlands	k6eAd1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
základní	základní	k2eAgInSc1d1
zemědělský	zemědělský	k2eAgInSc1d1
prostředek	prostředek	k1gInSc1
k	k	k7c3
omlazení	omlazení	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
populace	populace	k1gFnSc1
roste	růst	k5eAaImIp3nS
<g/>
,	,	kIx,
nižší	nízký	k2eAgFnSc1d2
spotřeba	spotřeba	k1gFnSc1
masa	maso	k1gNnSc2
na	na	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
umožní	umožnit	k5eAaPmIp3nS
efektivnější	efektivní	k2eAgNnSc1d2
využití	využití	k1gNnSc1
při	při	k7c6
snížení	snížení	k1gNnSc6
dostupné	dostupný	k2eAgFnSc2d1
půdy	půda	k1gFnSc2
a	a	k8xC
vodních	vodní	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
na	na	k7c4
hlavu	hlava	k1gFnSc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
ve	v	k7c4
stejnou	stejný	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
lze	lze	k6eAd1
pěstovat	pěstovat	k5eAaImF
obilí	obilí	k1gNnSc4
levněji	levně	k6eAd2
pro	pro	k7c4
chronicky	chronicky	k6eAd1
nejhladovější	hladový	k2eAgFnSc4d3
z	z	k7c2
celého	celý	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
196	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tchajwanská	tchajwanský	k2eAgFnSc1d1
buddhistická	buddhistický	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
</s>
<s>
Demografická	demografický	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
</s>
<s>
Výzkumná	výzkumný	k2eAgFnSc1d1
studie	studie	k1gFnSc1
provedená	provedený	k2eAgFnSc1d1
na	na	k7c4
více	hodně	k6eAd2
než	než	k8xS
8000	#num#	k4
lidech	člověk	k1gMnPc6
publikovaná	publikovaný	k2eAgNnPc1d1
v	v	k7c6
časopise	časopis	k1gInSc6
British	British	k1gMnSc1
Medical	Medical	k1gMnSc1
Journal	Journal	k1gMnSc1
naznačuje	naznačovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
děti	dítě	k1gFnPc1
s	s	k7c7
nadprůměrným	nadprůměrný	k2eAgInSc7d1
IQ	iq	kA
mohou	moct	k5eAaImIp3nP
mít	mít	k5eAaImF
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
dospělosti	dospělost	k1gFnSc6
vyšší	vysoký	k2eAgFnSc4d2
šanci	šance	k1gFnSc4
stát	stát	k5eAaPmF,k5eAaImF
se	se	k3xPyFc4
vegetariány	vegetarián	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
197	#num#	k4
<g/>
]	]	kIx)
Vegetariáni	vegetarián	k1gMnPc1
měli	mít	k5eAaImAgMnP
průměrné	průměrný	k2eAgNnSc4d1
dětské	dětský	k2eAgNnSc4d1
IQ	iq	kA
105	#num#	k4
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
IQ	iq	kA
100	#num#	k4
nevegetariánů	nevegetarián	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nebyly	být	k5eNaImAgInP
zjištěny	zjištěn	k2eAgInPc1d1
žádné	žádný	k3yNgInPc4
rozdíl	rozdíl	k1gInSc4
v	v	k7c6
IQ	iq	kA
skóre	skóre	k1gNnSc1
mezi	mezi	k7c7
přísnými	přísný	k2eAgMnPc7d1
vegetariány	vegetarián	k1gMnPc7
a	a	k8xC
těmi	ten	k3xDgMnPc7
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
uvedli	uvést	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
jedí	jíst	k5eAaImIp3nP
ryby	ryba	k1gFnPc1
nebo	nebo	k8xC
kuřata	kuře	k1gNnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
198	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výzkumník	výzkumník	k1gMnSc1
Gordon	Gordon	k1gMnSc1
Hodson	Hodson	k1gMnSc1
podotýká	podotýkat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
vegetariáni	vegetarián	k1gMnPc1
a	a	k8xC
vegani	vegan	k1gMnPc1
často	často	k6eAd1
setkávají	setkávat	k5eAaImIp3nP
s	s	k7c7
diskriminací	diskriminace	k1gFnSc7
tam	tam	k6eAd1
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
jedení	jedení	k1gNnSc2
masa	maso	k1gNnSc2
udržováno	udržovat	k5eAaImNgNnS
jako	jako	k8xS,k8xC
kulturní	kulturní	k2eAgFnSc1d1
norma	norma	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
199	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pohlaví	pohlaví	k1gNnSc1
</s>
<s>
Výzkumná	výzkumný	k2eAgFnSc1d1
studie	studie	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1992	#num#	k4
provedená	provedený	k2eAgFnSc1d1
Yankelovičovým	Yankelovičův	k2eAgNnSc7d1
centrem	centrum	k1gNnSc7
pro	pro	k7c4
výzkum	výzkum	k1gInSc4
sociálních	sociální	k2eAgFnPc2d1
věd	věda	k1gFnPc2
(	(	kIx(
<g/>
The	The	k1gMnSc1
Yankelovich	Yankelovich	k1gMnSc1
Center	centrum	k1gNnPc2
for	forum	k1gNnPc2
Social	Social	k1gMnSc1
Science	Science	k1gFnSc2
Research	Research	k1gMnSc1
<g/>
)	)	kIx)
došla	dojít	k5eAaPmAgFnS
k	k	k7c3
závěru	závěr	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
z	z	k7c2
12,4	12,4	k4
milionů	milion	k4xCgInPc2
lidí	člověk	k1gMnPc2
[	[	kIx(
<g/>
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
]	]	kIx)
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
si	se	k3xPyFc3
říkají	říkat	k5eAaImIp3nP
vegetariáni	vegetarián	k1gMnPc1
<g/>
,	,	kIx,
68	#num#	k4
%	%	kIx~
tvoří	tvořit	k5eAaImIp3nP
ženy	žena	k1gFnPc1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
pouze	pouze	k6eAd1
32	#num#	k4
%	%	kIx~
jsou	být	k5eAaImIp3nP
muži	muž	k1gMnPc1
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
200	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Alespoň	alespoň	k9
jedna	jeden	k4xCgFnSc1
studie	studie	k1gFnSc1
ukazuje	ukazovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
vegetariánky	vegetariánka	k1gFnPc1
mají	mít	k5eAaImIp3nP
větší	veliký	k2eAgFnSc4d2
pravděpodobnost	pravděpodobnost	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
budou	být	k5eAaImBp3nP
mít	mít	k5eAaImF
děti	dítě	k1gFnPc4
ženského	ženský	k2eAgNnSc2d1
pohlaví	pohlaví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britská	britský	k2eAgFnSc1d1
studie	studie	k1gFnSc1
6000	#num#	k4
těhotných	těhotný	k2eAgFnPc2d1
žen	žena	k1gFnPc2
z	z	k7c2
roku	rok	k1gInSc2
1998	#num#	k4
zjistila	zjistit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
u	u	k7c2
vegetariánských	vegetariánský	k2eAgFnPc2d1
matek	matka	k1gFnPc2
je	být	k5eAaImIp3nS
poměr	poměr	k1gInSc1
jen	jen	k9
85	#num#	k4
chlapců	chlapec	k1gMnPc2
na	na	k7c4
100	#num#	k4
<g/>
,	,	kIx,
i	i	k9
když	když	k8xS
celostátní	celostátní	k2eAgInSc1d1
poměr	poměr	k1gInSc1
činí	činit	k5eAaImIp3nS
106	#num#	k4
narozených	narozený	k2eAgMnPc2d1
chlapců	chlapec	k1gMnPc2
na	na	k7c4
každých	každý	k3xTgFnPc2
100	#num#	k4
dívek	dívka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zástupkyně	zástupkyně	k1gFnSc1
Britské	britský	k2eAgFnSc2d1
dietetické	dietetický	k2eAgFnSc2d1
asociace	asociace	k1gFnSc2
<g/>
,	,	kIx,
Catherine	Catherin	k1gInSc5
Collins	Collins	k1gInSc1
<g/>
,	,	kIx,
toto	tento	k3xDgNnSc1
odmítla	odmítnout	k5eAaPmAgFnS
jako	jako	k8xC,k8xS
„	„	k?
<g/>
statistickou	statistický	k2eAgFnSc4d1
náhodu	náhoda	k1gFnSc4
<g/>
“	“	k?
vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
o	o	k7c4
pohlaví	pohlaví	k1gNnSc4
dítěte	dítě	k1gNnSc2
rozhoduje	rozhodovat	k5eAaImIp3nS
genetický	genetický	k2eAgInSc4d1
příspěvek	příspěvek	k1gInSc4
muže	muž	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
201	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Fyzická	fyzický	k2eAgFnSc1d1
zdatnost	zdatnost	k1gFnSc1
a	a	k8xC
vegetariánská	vegetariánský	k2eAgFnSc1d1
strava	strava	k1gFnSc1
</s>
<s>
Dle	dle	k7c2
studie	studie	k1gFnSc2
uveřejněné	uveřejněný	k2eAgFnSc2d1
v	v	k7c6
Americkém	americký	k2eAgInSc6d1
žurnálu	žurnál	k1gInSc6
klinické	klinický	k2eAgFnSc2d1
výživy	výživa	k1gFnSc2
(	(	kIx(
<g/>
The	The	k1gMnSc1
American	American	k1gMnSc1
Journal	Journal	k1gMnSc1
of	of	k?
Clinical	Clinical	k1gFnSc1
Nutrition	Nutrition	k1gInSc1
<g/>
)	)	kIx)
není	být	k5eNaImIp3nS
potvrzen	potvrdit	k5eAaPmNgInS
ani	ani	k8xC
vyvrácen	vyvrácen	k2eAgInSc1d1
vliv	vliv	k1gInSc1
vegetariánské	vegetariánský	k2eAgFnSc2d1
stravy	strava	k1gFnSc2
na	na	k7c4
sportovní	sportovní	k2eAgFnSc4d1
výkonnost	výkonnost	k1gFnSc4
<g/>
,	,	kIx,
zejména	zejména	k9
je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
prováděna	prováděn	k2eAgFnSc1d1
kontrola	kontrola	k1gFnSc1
přísunu	přísun	k1gInSc2
cukrů	cukr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Případné	případný	k2eAgInPc1d1
problémy	problém	k1gInPc1
s	s	k7c7
nerovnováhou	nerovnováha	k1gFnSc7
živin	živina	k1gFnPc2
lze	lze	k6eAd1
kompenzovat	kompenzovat	k5eAaBmF
dobře	dobře	k6eAd1
navrženým	navržený	k2eAgInSc7d1
vegetariánským	vegetariánský	k2eAgInSc7d1
jídelníčkem	jídelníček	k1gInSc7
v	v	k7c6
kontextu	kontext	k1gInSc6
tréninku	trénink	k1gInSc2
a	a	k8xC
sportovních	sportovní	k2eAgInPc2d1
výkonů	výkon	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
202	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
významných	významný	k2eAgMnPc2d1
sportovců	sportovec	k1gMnPc2
se	se	k3xPyFc4
hlásila	hlásit	k5eAaImAgFnS
a	a	k8xC
hlásí	hlásit	k5eAaImIp3nS
k	k	k7c3
vegetariánství	vegetariánství	k1gNnSc3
či	či	k8xC
veganství	veganství	k1gNnSc4
<g/>
,	,	kIx,
např.	např.	kA
legendární	legendární	k2eAgMnSc1d1
finský	finský	k2eAgMnSc1d1
běžec	běžec	k1gMnSc1
Paavo	Paavo	k1gNnSc1
Nurmi	Nur	k1gFnPc7
(	(	kIx(
<g/>
devět	devět	k4xCc4
zlatých	zlatý	k2eAgFnPc2d1
medailí	medaile	k1gFnPc2
z	z	k7c2
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
americký	americký	k2eAgMnSc1d1
legendární	legendární	k2eAgMnSc1d1
atlet	atlet	k1gMnSc1
Carl	Carl	k1gMnSc1
Lewis	Lewis	k1gInSc1
(	(	kIx(
<g/>
deset	deset	k4xCc4
olympijských	olympijský	k2eAgFnPc2d1
medailí	medaile	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
australský	australský	k2eAgMnSc1d1
plavec	plavec	k1gMnSc1
Murray	Murraa	k1gFnSc2
Rose	Rose	k1gMnSc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
čtyři	čtyři	k4xCgFnPc4
zlaté	zlatý	k2eAgFnPc4d1
medaile	medaile	k1gFnPc4
z	z	k7c2
OH	OH	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
českoamerická	českoamerický	k2eAgFnSc1d1
tenistka	tenistka	k1gFnSc1
Martina	Martina	k1gFnSc1
Navrátilová	Navrátilová	k1gFnSc1
<g/>
,	,	kIx,
francouzská	francouzský	k2eAgFnSc1d1
krasobruslařka	krasobruslařka	k1gFnSc1
Surya	Surya	k1gFnSc1
Bonalyová	Bonalyová	k1gFnSc1
(	(	kIx(
<g/>
pětinásobná	pětinásobný	k2eAgFnSc1d1
mistryně	mistryně	k1gFnSc1
Evropy	Evropa	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
legendární	legendární	k2eAgMnSc1d1
italský	italský	k2eAgMnSc1d1
cyklista	cyklista	k1gMnSc1
Fausto	Fausta	k1gMnSc5
Coppi	Copp	k1gMnSc5
<g/>
,	,	kIx,
italský	italský	k2eAgMnSc1d1
fotbalista	fotbalista	k1gMnSc1
Gianluca	Gianluca	k1gMnSc1
Vialli	Vialle	k1gFnSc4
<g/>
,	,	kIx,
americké	americký	k2eAgFnPc4d1
tenistky	tenistka	k1gFnPc4
Chris	Chris	k1gFnSc2
Evert	Everta	k1gFnPc2
(	(	kIx(
<g/>
nejlepší	dobrý	k2eAgFnSc1d3
hráčka	hráčka	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
a	a	k8xC
Billie	Billie	k1gFnSc2
Jean	Jean	k1gMnSc1
Kingová	Kingový	k2eAgFnSc1d1
<g/>
,	,	kIx,
běžec	běžec	k1gMnSc1
Edwin	Edwin	k1gMnSc1
Corley	Corlea	k1gFnSc2
Moses	Moses	k1gMnSc1
(	(	kIx(
<g/>
dvojnásobný	dvojnásobný	k2eAgMnSc1d1
mistr	mistr	k1gMnSc1
světa	svět	k1gInSc2
v	v	k7c6
překážkovém	překážkový	k2eAgInSc6d1
běhu	běh	k1gInSc6
<g/>
,	,	kIx,
dvě	dva	k4xCgFnPc4
zlaté	zlatý	k2eAgFnPc4d1
medaile	medaile	k1gFnPc4
z	z	k7c2
OH	OH	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
basketbalista	basketbalista	k1gMnSc1
Robert	Robert	k1gMnSc1
Parish	Parish	k1gMnSc1
či	či	k8xC
kulturista	kulturista	k1gMnSc1
Bill	Bill	k1gMnSc1
Pearl	Pearl	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
203	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vegetariánské	vegetariánský	k2eAgFnPc1d1
organizace	organizace	k1gFnPc1
</s>
<s>
Na	na	k7c6
světě	svět	k1gInSc6
existuje	existovat	k5eAaImIp3nS
spousta	spousta	k1gFnSc1
organizací	organizace	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
sdružují	sdružovat	k5eAaImIp3nP
vegetariány	vegetarián	k1gMnPc4
a	a	k8xC
především	především	k9
propagují	propagovat	k5eAaImIp3nP
vegetariánský	vegetariánský	k2eAgInSc4d1
způsob	způsob	k1gInSc4
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zastřešující	zastřešující	k2eAgFnSc7d1
světovou	světový	k2eAgFnSc7d1
organizací	organizace	k1gFnSc7
je	být	k5eAaImIp3nS
Mezinárodní	mezinárodní	k2eAgFnSc1d1
vegetariánská	vegetariánský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
<g/>
[	[	kIx(
<g/>
204	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
nezisková	ziskový	k2eNgFnSc1d1
organizace	organizace	k1gFnSc1
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc7
součástí	součást	k1gFnSc7
jsou	být	k5eAaImIp3nP
neziskové	ziskový	k2eNgFnPc1d1
organizace	organizace	k1gFnPc1
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc7
hlavním	hlavní	k2eAgInSc7d1
cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
propagace	propagace	k1gFnSc1
vegetariánství	vegetariánství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
mezinárodní	mezinárodní	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
působí	působit	k5eAaImIp3nS
také	také	k9
Evropská	evropský	k2eAgFnSc1d1
vegetariánská	vegetariánský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
<g/>
[	[	kIx(
<g/>
205	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
zastřešuje	zastřešovat	k5eAaImIp3nS
vegetariánské	vegetariánský	k2eAgFnSc3d1
společnosti	společnost	k1gFnSc3
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existuje	existovat	k5eAaImIp3nS
také	také	k9
spousta	spousta	k1gFnSc1
vegetariánských	vegetariánský	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
na	na	k7c6
úrovni	úroveň	k1gFnSc6
států	stát	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
jsou	být	k5eAaImIp3nP
často	často	k6eAd1
členy	člen	k1gMnPc4
IVU	Iva	k1gFnSc4
nebo	nebo	k8xC
EVU	Eva	k1gFnSc4
<g/>
[	[	kIx(
<g/>
206	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
:	:	kIx,
Česká	český	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
pro	pro	k7c4
výživu	výživa	k1gFnSc4
a	a	k8xC
vegetariánství	vegetariánství	k1gNnSc4
<g/>
[	[	kIx(
<g/>
207	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Svoboda	svoboda	k1gFnSc1
zvířat	zvíře	k1gNnPc2
<g/>
[	[	kIx(
<g/>
208	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
amerických	americký	k2eAgInPc6d1
<g/>
:	:	kIx,
PETA	PETA	kA
<g/>
[	[	kIx(
<g/>
209	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
Británii	Británie	k1gFnSc6
<g/>
:	:	kIx,
Vegetarian	Vegetarian	k1gInSc1
Society	societa	k1gFnSc2
of	of	k?
the	the	k?
UK	UK	kA
<g/>
[	[	kIx(
<g/>
210	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
v	v	k7c6
Německu	Německo	k1gNnSc6
<g/>
:	:	kIx,
Vegetarierbund	Vegetarierbund	k1gInSc1
Deutschland	Deutschland	k1gInSc1
<g/>
[	[	kIx(
<g/>
211	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Vegetarische	Vegetarische	k1gFnSc1
Initiative	Initiativ	k1gInSc5
<g/>
[	[	kIx(
<g/>
212	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
v	v	k7c6
Rakousku	Rakousko	k1gNnSc6
<g/>
:	:	kIx,
Österreichische	Österreichische	k1gFnSc1
Vegetarier	Vegetarier	k1gMnSc1
Union	union	k1gInSc1
<g/>
[	[	kIx(
<g/>
213	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
v	v	k7c6
Polsku	Polsko	k1gNnSc6
<g/>
:	:	kIx,
Viva	Viva	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
Akcja	Akcja	k1gMnSc1
Dla	Dla	k1gMnSc1
Zwierząt	Zwierząt	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
[	[	kIx(
<g/>
214	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Stowarzyszenie	Stowarzyszenie	k1gFnSc1
Empatia	Empatia	k1gFnSc1
<g/>
[	[	kIx(
<g/>
215	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Stowarzyszenie	Stowarzyszenie	k1gFnSc1
Empatia	Empatia	k1gFnSc1
<g/>
[	[	kIx(
<g/>
216	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Stowarzyszenie	Stowarzyszenie	k1gFnSc1
Otwarte	Otwart	k1gInSc5
Klatki	Klatk	k1gMnSc6
<g/>
[	[	kIx(
<g/>
217	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vegetariánské	vegetariánský	k2eAgFnPc1d1
akce	akce	k1gFnPc1
</s>
<s>
Veggie	Veggie	k1gFnSc1
Pride	Prid	k1gInSc5
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
v	v	k7c6
Paříži	Paříž	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Vegetariáni	vegetarián	k1gMnPc1
a	a	k8xC
vegetariánské	vegetariánský	k2eAgFnPc1d1
organizace	organizace	k1gFnPc1
pořádají	pořádat	k5eAaImIp3nP
různé	různý	k2eAgFnPc1d1
akce	akce	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejdůležitější	důležitý	k2eAgInSc1d3
z	z	k7c2
nich	on	k3xPp3gInPc2
je	být	k5eAaImIp3nS
Světový	světový	k2eAgInSc1d1
den	den	k1gInSc1
vegetariánství	vegetariánství	k1gNnSc2
slavený	slavený	k2eAgInSc1d1
1	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
218	#num#	k4
<g/>
]	]	kIx)
O	o	k7c4
měsíc	měsíc	k1gInSc4
později	pozdě	k6eAd2
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
<g/>
,	,	kIx,
bývá	bývat	k5eAaImIp3nS
pořádán	pořádán	k2eAgInSc1d1
Světový	světový	k2eAgInSc1d1
den	den	k1gInSc1
veganství	veganství	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
219	#num#	k4
<g/>
]	]	kIx)
<g/>
Obě	dva	k4xCgFnPc1
akce	akce	k1gFnPc1
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nP
upozornit	upozornit	k5eAaPmF
na	na	k7c4
výhody	výhoda	k1gFnPc4
daného	daný	k2eAgInSc2d1
způsobu	způsob	k1gInSc2
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgFnSc7d1
vegetariánskou	vegetariánský	k2eAgFnSc7d1
akcí	akce	k1gFnSc7
je	být	k5eAaImIp3nS
Veggie	Veggie	k1gFnSc1
Pride	Prid	k1gInSc5
<g/>
[	[	kIx(
<g/>
220	#num#	k4
<g/>
]	]	kIx)
–	–	k?
každoroční	každoroční	k2eAgFnSc2d1
mezinárodní	mezinárodní	k2eAgFnSc2d1
akce	akce	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
oslavuje	oslavovat	k5eAaImIp3nS
a	a	k8xC
podporuje	podporovat	k5eAaImIp3nS
vegetariánství	vegetariánství	k1gNnSc4
a	a	k8xC
veganství	veganství	k1gNnSc4
<g/>
;	;	kIx,
v	v	k7c6
Česku	Česko	k1gNnSc6
je	být	k5eAaImIp3nS
akce	akce	k1gFnSc1
organizovaná	organizovaný	k2eAgFnSc1d1
pod	pod	k7c7
názvem	název	k1gInSc7
Veggie	Veggie	k1gFnSc2
Parade	Parad	k1gInSc5
<g/>
[	[	kIx(
<g/>
221	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gNnSc7
posláním	poslání	k1gNnSc7
je	být	k5eAaImIp3nS
oslava	oslava	k1gFnSc1
etického	etický	k2eAgInSc2d1
způsobu	způsob	k1gInSc2
života	život	k1gInSc2
<g/>
,	,	kIx,
snaha	snaha	k1gFnSc1
o	o	k7c6
porozumění	porozumění	k1gNnSc6
mezi	mezi	k7c7
vegetariány	vegetarián	k1gMnPc7
<g/>
,	,	kIx,
vegany	vegan	k1gMnPc7
a	a	k8xC
konzumenty	konzument	k1gMnPc7
živočišných	živočišný	k2eAgInPc2d1
produktů	produkt	k1gInPc2
<g/>
,	,	kIx,
seznámení	seznámení	k1gNnSc3
veřejnosti	veřejnost	k1gFnSc2
s	s	k7c7
podmínkami	podmínka	k1gFnPc7
života	život	k1gInSc2
zvířat	zvíře	k1gNnPc2
na	na	k7c6
farmách	farma	k1gFnPc6
a	a	k8xC
ve	v	k7c6
velkochovech	velkochov	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
proběhla	proběhnout	k5eAaPmAgFnS
první	první	k4xOgFnSc1
Veggie	Veggie	k1gFnSc1
Parade	Parad	k1gInSc5
v	v	k7c6
květnu	květen	k1gInSc6
2009	#num#	k4
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
poslední	poslední	k2eAgFnSc1d1
se	se	k3xPyFc4
konala	konat	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
222	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Existují	existovat	k5eAaImIp3nP
také	také	k9
kampaně	kampaň	k1gFnSc2
zaměřené	zaměřený	k2eAgFnSc2d1
na	na	k7c4
propagaci	propagace	k1gFnSc4
částečného	částečný	k2eAgNnSc2d1
vegetariánství	vegetariánství	k1gNnSc2
<g/>
,	,	kIx,
např.	např.	kA
bezmasé	bezmasý	k2eAgInPc1d1
pondělky	pondělek	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
223	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Informace	informace	k1gFnPc1
specifické	specifický	k2eAgFnPc1d1
pro	pro	k7c4
konkrétní	konkrétní	k2eAgFnPc4d1
země	zem	k1gFnPc4
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Vegetariánství	vegetariánství	k1gNnSc2
podle	podle	k7c2
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Vegetarianism	Vegetarianisma	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Frequently	Frequently	k1gFnSc1
Asked	Asked	k1gMnSc1
Questions	Questions	k1gInSc1
–	–	k?
Definitions	Definitions	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
International	International	k1gMnSc1
Vegetarian	Vegetarian	k1gMnSc1
Union	union	k1gInSc1
(	(	kIx(
<g/>
IVU	Iva	k1gFnSc4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
↑	↑	k?
What	What	k1gInSc1
is	is	k?
a	a	k8xC
vegetarian	vegetarian	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Vegetarian	Vegetariana	k1gFnPc2
Society	societa	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Frequently	Frequently	k1gFnSc1
Asked	Asked	k1gMnSc1
Questions	Questions	k1gInSc1
-	-	kIx~
Ingredients	Ingredients	k1gInSc1
1	#num#	k4
<g/>
:	:	kIx,
Gelatine	Gelatin	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
International	International	k1gMnSc1
Vegetarian	Vegetarian	k1gMnSc1
Union	union	k1gInSc1
(	(	kIx(
<g/>
IVU	Iva	k1gFnSc4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Why	Why	k1gMnPc2
Avoid	Avoid	k1gInSc4
Hidden	Hiddna	k1gFnPc2
Animal	animal	k1gMnSc1
Ingredients	Ingredients	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
North	North	k1gMnSc1
American	American	k1gMnSc1
Vegetarian	Vegetarian	k1gMnSc1
Society	societa	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
FORREST	FORREST	kA
<g/>
,	,	kIx,
Jamie	Jamie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Is	Is	k1gFnSc1
Cheese	Cheese	k1gFnSc2
Vegetarian	Vegetariana	k1gFnPc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Serious	Serious	k1gInSc1
Eats	Eats	k1gInSc4
<g/>
,	,	kIx,
2007-12-18	2007-12-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
některé	některý	k3yIgMnPc4
vegetariány	vegetarián	k1gMnPc4
je	být	k5eAaImIp3nS
v	v	k7c6
pořádku	pořádek	k1gInSc6
jíst	jíst	k5eAaImF
sýry	sýr	k1gInPc1
vyrobené	vyrobený	k2eAgInPc1d1
za	za	k7c7
pomocí	pomoc	k1gFnSc7
živočišného	živočišný	k2eAgNnSc2d1
syřidla	syřidlo	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
mnozí	mnohý	k2eAgMnPc1d1
hledají	hledat	k5eAaImIp3nP
ty	ten	k3xDgFnPc1
vyrobené	vyrobený	k2eAgFnPc1d1
s	s	k7c7
vegetariánskými	vegetariánský	k2eAgNnPc7d1
syřidly	syřidlo	k1gNnPc7
<g/>
,	,	kIx,
zejména	zejména	k9
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
ta	ten	k3xDgNnPc1
už	už	k6eAd1
jsou	být	k5eAaImIp3nP
v	v	k7c6
dnešní	dnešní	k2eAgFnSc6d1
době	doba	k1gFnSc6
velmi	velmi	k6eAd1
rozšířená	rozšířený	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Vegetarian	Vegetarian	k1gMnSc1
and	and	k?
vegan	vegan	k1gInSc1
eating	eating	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Department	department	k1gInSc1
of	of	k?
Health	Health	k1gMnSc1
and	and	k?
Human	Human	k1gMnSc1
Services	Services	k1gMnSc1
<g/>
,	,	kIx,
State	status	k1gInSc5
Government	Government	k1gInSc4
of	of	k?
Victoria	Victorium	k1gNnSc2
<g/>
,	,	kIx,
Australia	Australia	k1gFnSc1
(	(	kIx(
<g/>
Better	Better	k1gMnSc1
Health	Health	k1gMnSc1
Channel	Channel	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2012-10	2012-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Things	Thingsa	k1gFnPc2
to	ten	k3xDgNnSc4
look	look	k6eAd1
out	out	k?
for	forum	k1gNnPc2
if	if	k?
you	you	k?
are	ar	k1gInSc5
a	a	k8xC
vegetarian	vegetarian	k1gMnSc1
<g/>
/	/	kIx~
<g/>
vegan	vegan	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Vegetarian	Vegetariana	k1gFnPc2
Society	societa	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
KEEVICAN	KEEVICAN	kA
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
What	What	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
in	in	k?
Your	Your	k1gInSc1
Cheese	Cheese	k1gFnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vegetarian	Vegetarian	k1gInSc4
Resource	Resourka	k1gFnSc3
Group	Group	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Many	mana	k1gFnSc2
vegetarians	vegetariansa	k1gFnPc2
don	don	k1gMnSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
consider	consider	k1gInSc1
that	that	k1gMnSc1
some	some	k1gFnPc2
of	of	k?
the	the	k?
cheeses	cheeses	k1gInSc4
they	thea	k1gFnSc2
are	ar	k1gInSc5
eating	eating	k1gInSc4
could	could	k1gMnSc1
actually	actualla	k1gFnSc2
contain	contain	k1gMnSc1
unfamiliar	unfamiliar	k1gMnSc1
animal	animal	k1gMnSc1
ingredients	ingredients	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Frequently	Frequently	k1gFnSc1
Asked	Asked	k1gMnSc1
Questions	Questions	k1gInSc1
—	—	k?
Food	Food	k1gInSc1
Ingredients	Ingredients	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vegetarian	Vegetarian	k1gInSc4
Resource	Resourka	k1gFnSc3
Group	Group	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
One	One	k1gFnSc1
of	of	k?
the	the	k?
most	most	k1gInSc1
frequently	frequentnout	k5eAaPmAgInP
asked	asked	k1gInSc4
questions	questionsa	k1gFnPc2
is	is	k?
<g/>
:	:	kIx,
Why	Why	k1gMnSc1
are	ar	k1gInSc5
some	some	k1gNnSc1
cheeses	cheeses	k1gInSc1
labeled	labeled	k1gMnSc1
as	as	k1gInSc1
"	"	kIx"
<g/>
vegetarian	vegetarian	k1gInSc1
cheese	cheese	k1gFnSc2
<g/>
"	"	kIx"
<g/>
?	?	kIx.
</s>
<s desamb="1">
Why	Why	k1gMnSc1
wouldn	wouldn	k1gMnSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
cheese	cheese	k1gFnSc2
be	be	k?
vegetarian	vegetarian	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
What	What	k1gMnSc1
is	is	k?
rennet	rennet	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Shorter	Shortra	k1gFnPc2
Oxford	Oxford	k1gInSc4
English	English	k1gInSc4
Dictionary	Dictionara	k1gFnSc2
(	(	kIx(
<g/>
2002	#num#	k4
a	a	k8xC
2007	#num#	k4
<g/>
)	)	kIx)
definuje	definovat	k5eAaBmIp3nS
„	„	k?
<g/>
vegetariána	vegetarián	k1gMnSc2
<g/>
"	"	kIx"
(	(	kIx(
<g/>
podstatné	podstatný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
<g/>
)	)	kIx)
jako	jako	k9
"	"	kIx"
<g/>
osobu	osoba	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
z	z	k7c2
principu	princip	k1gInSc2
vyhýbá	vyhýbat	k5eAaImIp3nS
zvířecí	zvířecí	k2eAgInSc1d1
stravě	strava	k1gFnSc3
pro	pro	k7c4
zvířata	zvíře	k1gNnPc4
<g/>
,	,	kIx,
resp.	resp.	kA
kdo	kdo	k3yQnSc1,k3yInSc1,k3yRnSc1
se	se	k3xPyFc4
vyhýbá	vyhýbat	k5eAaImIp3nS
masu	masa	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
bude	být	k5eAaImBp3nS
jíst	jíst	k5eAaImF
mléčné	mléčný	k2eAgInPc4d1
výrobky	výrobek	k1gInPc4
a	a	k8xC
vejce	vejce	k1gNnPc4
a	a	k8xC
někdy	někdy	k6eAd1
i	i	k9
ryby	ryba	k1gFnSc2
(	(	kIx(
srv	srv	k?
<g/>
.	.	kIx.
vegan	vegan	k1gMnSc1
podstatné	podstatný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
1	#num#	k4
2	#num#	k4
Perceptions	Perceptionsa	k1gFnPc2
and	and	k?
practices	practices	k1gMnSc1
of	of	k?
self-defined	self-defined	k1gMnSc1
current	current	k1gMnSc1
vegetarian	vegetarian	k1gMnSc1
and	and	k?
nonvegetarian	nonvegetarian	k1gMnSc1
women	women	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
the	the	k?
American	American	k1gInSc1
Dietetic	Dietetice	k1gFnPc2
Association	Association	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
March	March	k1gInSc1
2002	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
102	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
3	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
354	#num#	k4
<g/>
–	–	k?
<g/>
360	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
S	s	k7c7
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
8223	#num#	k4
<g/>
(	(	kIx(
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
90083	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
11902368	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Pescetarian	Pescetarian	k1gInSc1
-	-	kIx~
Definition	Definition	k1gInSc1
and	and	k?
More	mor	k1gInSc5
from	froma	k1gFnPc2
the	the	k?
Free	Free	k1gNnSc4
Merriam-Webster	Merriam-Webstrum	k1gNnPc2
Dictionary	Dictionara	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Merriam-webster	Merriam-webster	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
Vegetarians	Vegetariansa	k1gFnPc2
don	dona	k1gFnPc2
<g/>
'	'	kIx"
<g/>
t	t	k?
eat	eat	k?
fish	fish	k1gMnSc1
<g/>
,	,	kIx,
shellfish	shellfish	k1gMnSc1
or	or	k?
crustacea	crustacea	k1gMnSc1
<g/>
,	,	kIx,
but	but	k?
they	thea	k1gFnSc2
can	can	k?
still	still	k1gMnSc1
enjoy	enjoa	k1gFnSc2
one	one	k?
of	of	k?
the	the	k?
healthiest	healthiest	k5eAaPmF
diets	diets	k6eAd1
available	available	k6eAd1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vegetarian	Vegetarian	k1gInSc1
Society	societa	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
John	John	k1gMnSc1
Davis	Davis	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vegsource	Vegsourka	k1gFnSc6
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
VegSource	VegSourka	k1gFnSc3
Interactive	Interactiv	k1gInSc5
<g/>
,	,	kIx,
Inc	Inc	k1gMnSc5
<g/>
.	.	kIx.
<g/>
,	,	kIx,
June	jun	k1gMnSc5
1	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
OED	OED	kA
vol	vol	k6eAd1
<g/>
.	.	kIx.
19	#num#	k4
<g/>
,	,	kIx,
second	second	k1gInSc1
edition	edition	k1gInSc1
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
476	#num#	k4
<g/>
;	;	kIx,
Webster	Webster	k1gInSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Third	Third	k1gMnSc1
New	New	k1gMnSc1
International	International	k1gMnSc1
Dictionary	Dictionara	k1gFnSc2
str	str	kA
<g/>
.	.	kIx.
2537	#num#	k4
<g/>
;	;	kIx,
The	The	k1gFnPc1
Oxford	Oxford	k1gInSc4
Dictionary	Dictionara	k1gFnSc2
of	of	k?
English	English	k1gMnSc1
Etymology	etymolog	k1gMnPc4
<g/>
,	,	kIx,
Oxford	Oxford	k1gInSc1
<g/>
,	,	kIx,
1966	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
972	#num#	k4
<g/>
;	;	kIx,
The	The	k1gFnSc6
Barnhart	Barnharta	k1gFnPc2
Dictionary	Dictionara	k1gFnSc2
of	of	k?
Etymology	etymolog	k1gMnPc7
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
1196	#num#	k4
<g/>
;	;	kIx,
Colin	Colin	k1gMnSc1
Spencer	Spencer	k1gMnSc1
<g/>
,	,	kIx,
The	The	k1gMnSc1
Heretic	Heretice	k1gFnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Feast	Feast	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
History	Histor	k1gInPc1
of	of	k?
Vegetarianism	Vegetarianism	k1gInSc1
<g/>
,	,	kIx,
London	London	k1gMnSc1
1993	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
252	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OED	OED	kA
píše	psát	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
slovo	slovo	k1gNnSc1
vstoupilo	vstoupit	k5eAaPmAgNnS
do	do	k7c2
obecné	obecná	k1gFnSc2
použití	použití	k1gNnSc2
po	po	k7c6
vzniku	vznik	k1gInSc6
Vegetariánské	vegetariánský	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
v	v	k7c6
Ramsgate	Ramsgat	k1gInSc5
v	v	k7c6
roce	rok	k1gInSc6
1847	#num#	k4
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
nabízí	nabízet	k5eAaImIp3nS
dva	dva	k4xCgInPc4
příklady	příklad	k1gInPc4
použití	použití	k1gNnSc2
z	z	k7c2
roku	rok	k1gInSc2
1839	#num#	k4
a	a	k8xC
1842	#num#	k4
<g/>
:	:	kIx,
</s>
<s>
1839	#num#	k4
<g/>
:	:	kIx,
„	„	k?
<g/>
Kdybych	kdyby	kYmCp1nS
musel	muset	k5eAaImAgMnS
být	být	k5eAaImF
sám	sám	k3xTgMnSc1
sobě	se	k3xPyFc3
kuchařem	kuchař	k1gMnSc7
<g/>
,	,	kIx,
nevyhnutelně	vyhnutelně	k6eNd1
se	se	k3xPyFc4
stanu	stanout	k5eAaPmIp1nS
vegetariánem	vegetarián	k1gMnSc7
<g/>
.	.	kIx.
<g/>
“	“	k?
(	(	kIx(
<g/>
FA	fa	kA
Kemble	Kemble	k1gMnSc1
<g/>
,	,	kIx,
Jrnl	Jrnl	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Residence	residence	k1gFnSc2
on	on	k3xPp3gMnSc1
Georgian	Georgian	k1gMnSc1
Plantation	Plantation	k1gInSc1
(	(	kIx(
<g/>
1863	#num#	k4
<g/>
)	)	kIx)
251	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1842	#num#	k4
<g/>
:	:	kIx,
„	„	k?
<g/>
To	to	k9
tell	tell	k1gInSc4
a	a	k8xC
healthy	health	k1gInPc4
vegetarian	vegetariana	k1gFnPc2
that	thata	k1gFnPc2
his	his	k1gNnSc2
diet	dieta	k1gFnPc2
is	is	k?
very	vera	k1gFnSc2
uncongenial	uncongenial	k1gMnSc1
with	with	k1gMnSc1
the	the	k?
wants	wants	k1gInSc1
of	of	k?
his	his	k1gNnSc1
nature	natur	k1gMnSc5
<g/>
.	.	kIx.
<g/>
“	“	k?
(	(	kIx(
<g/>
Healthian	Healthian	k1gInSc1
<g/>
,	,	kIx,
duben	duben	k1gInSc1
34	#num#	k4
<g/>
)	)	kIx)
Výskyt	výskyt	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1839	#num#	k4
je	být	k5eAaImIp3nS
stále	stále	k6eAd1
předmětem	předmět	k1gInSc7
diskuse	diskuse	k1gFnSc2
<g/>
,	,	kIx,
zdroj	zdroj	k1gInSc4
pro	pro	k7c4
rok	rok	k1gInSc4
1839	#num#	k4
v	v	k7c6
Oxfordském	oxfordský	k2eAgInSc6d1
anglickém	anglický	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
je	být	k5eAaImIp3nS
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
publikace	publikace	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
1863	#num#	k4
<g/>
:	:	kIx,
Fanny	Fanen	k2eAgFnPc1d1
Kemble	Kemble	k1gFnPc1
<g/>
,	,	kIx,
Journal	Journal	k1gFnPc1
of	of	k?
a	a	k8xC
Residence	residence	k1gFnSc1
on	on	k3xPp3gMnSc1
a	a	k8xC
Georgian	Georgiany	k1gInPc2
Plantation	Plantation	k1gInSc1
1838	#num#	k4
<g/>
–	–	k?
<g/>
1839	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgInSc1d1
rukopis	rukopis	k1gInSc1
nebyl	být	k5eNaImAgInS
lokalizován	lokalizován	k2eAgInSc1d1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
History	Histor	k1gInPc1
of	of	k?
Vegetarianism	Vegetarianism	k1gInSc1
-	-	kIx~
Extracts	Extracts	k1gInSc1
from	from	k6eAd1
some	somat	k5eAaPmIp3nS
journals	journals	k6eAd1
1843-48	1843-48	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ivu	Iva	k1gFnSc4
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Výpisky	výpisek	k1gInPc1
z	z	k7c2
některých	některý	k3yIgInPc2
časopisů	časopis	k1gInPc2
1842	#num#	k4
<g/>
–	–	k?
<g/>
48	#num#	k4
–	–	k?
nejdříve	dříve	k6eAd3
známé	známý	k2eAgNnSc1d1
použití	použití	k1gNnSc1
slova	slovo	k1gNnSc2
„	„	k?
<g/>
vegetariánský	vegetariánský	k2eAgInSc4d1
<g/>
“	“	k?
vytvořené	vytvořený	k2eAgNnSc1d1
Johnem	John	k1gMnSc7
Davisem	Davis	k1gInSc7
<g/>
.	.	kIx.
<g/>
↑	↑	k?
History	Histor	k1gInPc1
of	of	k?
Vegetarianism	Vegetarianism	k1gInSc1
-	-	kIx~
Extracts	Extracts	k1gInSc1
from	from	k6eAd1
some	somat	k5eAaPmIp3nS
journals	journals	k6eAd1
1843-48	1843-48	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ivu	Iva	k1gFnSc4
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SPENCER	SPENCER	kA
<g/>
,	,	kIx,
Colin	Colin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Heretic	Heretice	k1gFnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Feast	Feast	k1gInSc1
<g/>
:	:	kIx,
A	a	k8xC
History	Histor	k1gInPc1
of	of	k?
Vegetarianism	Vegetarianism	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Fourth	Fourth	k1gInSc1
Estate	Estat	k1gInSc5
Classic	Classice	k1gFnPc2
House	house	k1gNnSc1
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
874517606	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
75	#num#	k4
<g/>
–	–	k?
<g/>
76	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Encyclopedia	Encyclopedium	k1gNnSc2
of	of	k?
religion	religion	k1gInSc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Religious	Religious	k1gMnSc1
Vegetarianism	Vegetarianism	k1gMnSc1
From	From	k1gMnSc1
Hesiod	Hesiod	k1gInSc4
to	ten	k3xDgNnSc1
the	the	k?
Dalai	Dalai	k1gNnSc1
Lama	lama	k1gMnSc1
<g/>
'	'	kIx"
<g/>
,	,	kIx,
<g/>
'	'	kIx"
ed	ed	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kerry	Kerra	k1gMnSc2
S.	S.	kA
Walters	Walters	k1gInSc1
a	a	k8xC
Lisa	Lisa	k1gFnSc1
Portmess	Portmess	k1gInSc1
<g/>
,	,	kIx,
Albany	Alban	k1gInPc1
2001	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
13	#num#	k4
<g/>
-	-	kIx~
<g/>
46.1	46.1	k4
2	#num#	k4
GU	GU	kA
Pope	pop	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Thirukkural	Thirukkural	k1gFnSc1
English	Englisha	k1gFnPc2
Translation	Translation	k1gInSc4
and	and	k?
Commentary	Commentara	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
W.H.	W.H.	k1gMnSc1
Allen	Allen	k1gMnSc1
<g/>
,	,	kIx,
&	&	k?
Co	co	k9
<g/>
,	,	kIx,
1886	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
160	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
HOLUB	Holub	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jitřní	jitřní	k2eAgFnSc1d1
země	země	k1gFnSc1
<g/>
.	.	kIx.
www.jitrnizeme.cz	www.jitrnizeme.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Veg	Veg	k1gMnSc1
or	or	k?
non-veg	non-veg	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Labels	Labels	k1gInSc1
now	now	k?
make	make	k1gInSc1
it	it	k?
clear	clear	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bennett	Bennett	k1gMnSc1
<g/>
,	,	kIx,
Coleman	Coleman	k1gMnSc1
&	&	k?
Co	co	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ltd	ltd	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
2011-08-06	2011-08-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Passmore	Passmor	k1gInSc5
John	John	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Treatment	Treatment	k1gMnSc1
of	of	k?
Animals	Animals	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
the	the	k?
History	Histor	k1gInPc1
of	of	k?
Ideas	Ideas	k1gInSc1
<g/>
.	.	kIx.
1975	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
36	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
196	#num#	k4
<g/>
–	–	k?
<g/>
201	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.230	10.230	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
2708924	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Lutterbach	Lutterbach	k1gInSc1
<g/>
,	,	kIx,
Hubertus	hubertus	k1gInSc1
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Der	drát	k5eAaImRp2nS
Fleischverzicht	Fleischverzicht	k1gInSc1
im	im	k?
Christentum	Christentum	k1gNnSc1
<g/>
,	,	kIx,
<g/>
"	"	kIx"
Saeculum	Saeculum	k1gInSc1
50	#num#	k4
/	/	kIx~
II	II	kA
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
202	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Mortimer	Mortimer	k1gMnSc1
<g/>
,	,	kIx,
Ian	Ian	k1gMnSc1
<g/>
.	.	kIx.
"	"	kIx"
<g/>
The	The	k1gMnSc1
Time	Tim	k1gFnSc2
Traveler	Traveler	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Guide	Guid	k1gInSc5
to	ten	k3xDgNnSc1
Medieval	medieval	k1gInSc4
England	England	k1gInSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
184	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Spencer	Spencer	k1gInSc1
p.	p.	k?
180	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Spencer	Spencer	k1gInSc1
p.	p.	k?
252	#num#	k4
<g/>
-	-	kIx~
<g/>
253	#num#	k4
<g/>
,	,	kIx,
261	#num#	k4
<g/>
-	-	kIx~
<g/>
262	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Position	Position	k1gInSc1
of	of	k?
the	the	k?
American	American	k1gInSc1
Dietetic	Dietetice	k1gFnPc2
Association	Association	k1gInSc1
<g/>
:	:	kIx,
Vegetarian	Vegetarian	k1gInSc1
Diets	Dietsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
the	the	k?
Academy	Academa	k1gFnSc2
of	of	k?
Nutrition	Nutrition	k1gInSc1
and	and	k?
Dietetics	Dietetics	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
July	Jula	k1gFnSc2
2009	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
109	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
7	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1266	#num#	k4
<g/>
–	–	k?
<g/>
1282	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
6	#num#	k4
January	Januara	k1gFnSc2
2016	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
j.	j.	k?
<g/>
jada	jada	k6eAd1
<g/>
.2009	.2009	k4
<g/>
.05	.05	k4
<g/>
.027	.027	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
19562864	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KAUCKÁ	KAUCKÁ	kA
<g/>
,	,	kIx,
Tereza	Tereza	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vliv	vliv	k1gInSc1
vegetariánství	vegetariánství	k1gNnSc2
na	na	k7c4
zdravotní	zdravotní	k2eAgInSc4d1
stav	stav	k1gInSc4
člověka	člověk	k1gMnSc2
<g/>
.	.	kIx.
,	,	kIx,
2006	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
76	#num#	k4
s.	s.	k?
Bakalářská	bakalářský	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedoucí	vedoucí	k2eAgFnSc1d1
práce	práce	k1gFnSc1
Václav	Václav	k1gMnSc1
Břicháček	Břicháček	k?
<g/>
.	.	kIx.
s.	s.	k?
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Vegetarian	Vegetarian	k1gMnSc1
Frequently	Frequently	k1gMnSc1
Asked	Asked	k1gMnSc1
Questions	Questionsa	k1gFnPc2
-Definitions	-Definitions	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ivu	Iva	k1gFnSc4
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
Yabroff	Yabroff	k1gMnSc1
<g/>
,	,	kIx,
Jennie	Jennie	k1gFnSc1
<g/>
.	.	kIx.
"	"	kIx"
<g/>
No	no	k9
More	mor	k1gInSc5
Sacred	Sacred	k1gMnSc1
Cows	Cows	k1gInSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
""	""	k?
Newsweek	Newsweek	k6eAd1
""	""	k?
<g/>
,	,	kIx,
31	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2009	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Gale	Gale	k1gMnSc1
<g/>
,	,	kIx,
Catharine	Catharin	k1gInSc5
R.	R.	kA
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Http://www.edu-lib.us/bmj.com/cgi/content/abstract/bmj.39030.675069.55v1?hrss=1	Http://www.edu-lib.us/bmj.com/cgi/content/abstract/bmj.39030.675069.55v1?hrss=1	k1gFnSc1
</s>
<s>
"	"	kIx"
<g/>
IQ	iq	kA
in	in	k?
childhood	childhood	k1gInSc1
and	and	k?
vegetarianism	vegetarianism	k1gInSc1
in	in	k?
adulthood	adulthood	k1gInSc1
<g/>
:	:	kIx,
1970	#num#	k4
British	Britisha	k1gFnPc2
cohort	cohort	k1gInSc1
study	stud	k1gInPc1
<g/>
"	"	kIx"
<g/>
]	]	kIx)
Archivováno	archivovat	k5eAaBmNgNnS
4	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
British	British	k1gMnSc1
Medial	Medial	k1gMnSc1
Journal	Journal	k1gMnSc1
<g/>
,	,	kIx,
15	#num#	k4
prosinec	prosinec	k1gInSc1
2006	#num#	k4
<g/>
,	,	kIx,
vol	vol	k6eAd1
333	#num#	k4
<g/>
,	,	kIx,
issue	issue	k1gFnSc1
7581	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
245	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
SCHMID	SCHMID	kA
<g/>
,	,	kIx,
Rudolf	Rudolf	k1gMnSc1
<g/>
;	;	kIx,
HOPKINS	HOPKINS	kA
<g/>
,	,	kIx,
D.	D.	kA
J.	J.	kA
<g/>
;	;	kIx,
MERRIAM-WEBSTER	MERRIAM-WEBSTER	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Merriam-Webster	Merriam-Webster	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Geographical	Geographical	k1gFnSc7
Dictionary	Dictionara	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taxon	taxon	k1gInSc1
<g/>
.	.	kIx.
1998	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
47	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
535	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
40	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
262	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.230	10.230	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
1223820	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
2003	#num#	k4
Words	Wordsa	k1gFnPc2
of	of	k?
the	the	k?
Year	Year	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
American	American	k1gInSc1
Dialect	Dialect	k1gInSc4
Society	societa	k1gFnSc2
<g/>
,	,	kIx,
January	Januara	k1gFnSc2
13	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
DietaryGuidelines	DietaryGuidelines	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
3	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
.	.	kIx.
(	(	kIx(
<g/>
PDF	PDF	kA
<g/>
)	)	kIx)
.	.	kIx.
</s>
<s desamb="1">
Retrieved	Retrieved	k1gInSc1
on	on	k3xPp3gMnSc1
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
25.1	25.1	k4
2	#num#	k4
</s>
<s>
LI	li	k9
<g/>
,	,	kIx,
Duo	duo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Effect	Effect	k1gMnSc1
of	of	k?
the	the	k?
vegetarian	vegetarian	k1gInSc1
diet	dieta	k1gFnPc2
on	on	k3xPp3gMnSc1
non-communicable	non-communicable	k6eAd1
diseases	diseasesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
169	#num#	k4
<g/>
–	–	k?
<g/>
173	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
the	the	k?
Science	Science	k1gFnSc1
of	of	k?
Food	Food	k1gInSc1
and	and	k?
Agriculture	Agricultur	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
PubMed	PubMed	k1gMnSc1
<g/>
,	,	kIx,
2014-01	2014-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
169	#num#	k4
<g/>
–	–	k?
<g/>
173	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1097	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
jsfa	jsf	k1gInSc2
<g/>
.6362	.6362	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
23965907	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
↑	↑	k?
</s>
<s>
Institute	institut	k1gInSc5
of	of	k?
Social	Social	k1gMnSc1
Medicine	Medicin	k1gInSc5
and	and	k?
Epidemiology	epidemiolog	k1gMnPc7
<g/>
,	,	kIx,
Medical	Medical	k1gMnPc7
University	universita	k1gFnSc2
Graz	Graz	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nutrition	Nutrition	k1gInSc1
and	and	k?
Health	Health	k1gInSc1
–	–	k?
The	The	k1gFnSc1
Association	Association	k1gInSc1
between	between	k1gInSc1
Eating	Eating	k1gInSc1
Behavior	Behavior	k1gInSc1
and	and	k?
Various	Various	k1gInSc1
Health	Health	k1gMnSc1
Parameters	Parameters	k1gInSc1
<g/>
:	:	kIx,
A	a	k9
Matched	Matched	k1gInSc1
Sample	Sample	k1gFnSc2
Study	stud	k1gInPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-02	2014-02	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
↑	↑	k?
Position	Position	k1gInSc1
of	of	k?
the	the	k?
American	American	k1gInSc1
Dietetic	Dietetice	k1gFnPc2
Association	Association	k1gInSc1
<g/>
:	:	kIx,
Vegetarian	Vegetarian	k1gInSc1
diets	diets	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
July	Jula	k1gFnSc2
2009	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Mortality	mortalita	k1gFnSc2
in	in	k?
vegetarians	vegetarians	k1gInSc1
and	and	k?
nonvegetarians	nonvegetarians	k1gInSc1
<g/>
:	:	kIx,
detailed	detailed	k1gInSc1
findings	findings	k6eAd1
from	from	k6eAd1
a	a	k8xC
collaborative	collaborativ	k1gInSc5
analysis	analysis	k1gInSc4
of	of	k?
5	#num#	k4
prospective	prospectiv	k1gInSc5
studies	studies	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
American	American	k1gMnSc1
Journal	Journal	k1gMnSc1
of	of	k?
Clinical	Clinical	k1gFnSc1
Nutrition	Nutrition	k1gInSc1
<g/>
.	.	kIx.
1999	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
70	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
3	#num#	k4
Suppl	Suppl	k1gFnPc2
<g/>
,	,	kIx,
s.	s.	k?
516	#num#	k4
<g/>
S	s	k7c7
<g/>
–	–	k?
<g/>
524	#num#	k4
<g/>
S.	S.	kA
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.107	10.107	k4
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
phn	phn	k?
<g/>
19980006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
10479225	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Rejecting	Rejecting	k1gInSc1
meat	meata	k1gFnPc2
'	'	kIx"
<g/>
keeps	keepsit	k5eAaPmRp2nS
weight	weight	k1gInSc1
low	low	k?
<g/>
'	'	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
News	Newsa	k1gFnPc2
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Position	Position	k1gInSc1
of	of	k?
the	the	k?
American	American	k1gInSc1
Dietetic	Dietetice	k1gFnPc2
Association	Association	k1gInSc4
and	and	k?
Dietitians	Dietitians	k1gInSc1
of	of	k?
Canada	Canada	k1gFnSc1
<g/>
:	:	kIx,
Vegetarian	Vegetarian	k1gInSc1
diets	dietsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
the	the	k?
American	American	k1gInSc1
Dietetic	Dietetice	k1gFnPc2
Association	Association	k1gInSc1
<g/>
.	.	kIx.
2003	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
103	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
6	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
748	#num#	k4
<g/>
–	–	k?
<g/>
65	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
PDF	PDF	kA
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.105	10.105	k4
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
jada	jada	k6eAd1
<g/>
.2003	.2003	k4
<g/>
.50142	.50142	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
12778049	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Fraser	Frasero	k1gNnPc2
GE	GE	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vegetarian	Vegetarian	k1gInSc1
diets	diets	k1gInSc4
<g/>
:	:	kIx,
what	what	k1gMnSc1
do	do	k7c2
we	we	k?
know	know	k?
of	of	k?
their	their	k1gMnSc1
effects	effects	k6eAd1
on	on	k3xPp3gMnSc1
common	common	k1gMnSc1
chronic	chronice	k1gFnPc2
diseases	diseases	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
American	American	k1gMnSc1
Journal	Journal	k1gMnSc1
of	of	k?
Clinical	Clinical	k1gFnSc1
Nutrition	Nutrition	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
89	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
5	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1607	#num#	k4
<g/>
S	s	k7c7
<g/>
–	–	k?
<g/>
1612	#num#	k4
<g/>
S.	S.	kA
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.394	10.394	k4
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
ajcn	ajcn	k1gInSc1
<g/>
.2009	.2009	k4
<g/>
.26736	.26736	k4
<g/>
K.	K.	kA
<g />
.	.	kIx.
</s>
<s hack="1">
PMID	PMID	kA
19321569	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Position	Position	k1gInSc1
of	of	k?
the	the	k?
american	american	k1gInSc1
dietetic	dietetice	k1gFnPc2
association	association	k1gInSc4
and	and	k?
dietitians	dietitians	k1gInSc1
of	of	k?
Canada	Canada	k1gFnSc1
<g/>
:	:	kIx,
Vegetarian	Vegetarian	k1gInSc1
diets	diets	k1gInSc1
<g/>
..	..	k?
Journal	Journal	k1gFnSc1
of	of	k?
the	the	k?
American	American	k1gInSc1
Dietetic	Dietetice	k1gFnPc2
Association	Association	k1gInSc1
<g/>
.	.	kIx.
2003	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
103	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
6	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
748	#num#	k4
<g/>
–	–	k?
<g/>
65	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Vitamin	vitamin	k1gInSc1
B12	B12	k1gMnSc1
Linked	Linked	k1gMnSc1
to	ten	k3xDgNnSc4
Osteoporosis	Osteoporosis	k1gFnSc1
and	and	k?
Bone	bon	k1gInSc5
Loss	Loss	k1gInSc4
in	in	k?
Vegetarians	Vegetarians	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
April	April	k1gInSc1
29	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Bone	bon	k1gInSc5
mineral	minerat	k5eAaImAgInS,k5eAaPmAgInS
density	densit	k1gInPc1
of	of	k?
vegetarian	vegetarian	k1gInSc1
and	and	k?
non-vegetarian	non-vegetarian	k1gInSc1
adults	adults	k1gInSc1
in	in	k?
Taiwan	Taiwan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asia	Asia	k1gFnSc1
Pac	pac	k1gFnSc2
J	J	kA
Clin	Clin	k1gMnSc1
Nutr	Nutr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
National	National	k1gMnSc1
Institutes	Institutes	k1gMnSc1
of	of	k?
Health	Health	k1gMnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
17	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
101	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
18364334	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Incidence	incidence	k1gFnSc1
of	of	k?
osteoporosis	osteoporosis	k1gInSc1
in	in	k?
vegetarians	vegetarians	k1gInSc1
and	and	k?
omnivores	omnivores	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
American	American	k1gMnSc1
Journal	Journal	k1gMnSc1
of	of	k?
Clinical	Clinical	k1gFnSc1
Nutrition	Nutrition	k1gInSc1
<g/>
.	.	kIx.
1972	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
25	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
6	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
555	#num#	k4
<g/>
–	–	k?
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
5033736	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Boning	Boning	k1gInSc1
Up	Up	k1gFnSc2
on	on	k3xPp3gInSc1
Osteoporosis	Osteoporosis	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ars	Ars	k1gFnSc1
<g/>
.	.	kIx.
<g/>
usda	usda	k1gFnSc1
<g/>
.	.	kIx.
<g/>
gov	gov	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
China-Cornell-Oxford	China-Cornell-Oxford	k1gInSc1
Projekt	projekt	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cornell	Cornell	k1gInSc1
University	universita	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
BRODY	Brod	k1gInPc1
<g/>
,	,	kIx,
Jane	Jan	k1gMnSc5
E.	E.	kA
Huge	Hug	k1gFnPc4
Study	stud	k1gInPc4
Of	Of	k1gFnSc2
Diet	dieta	k1gFnPc2
Indicts	Indicts	k1gInSc1
Fat	fatum	k1gNnPc2
And	Anda	k1gFnPc2
Meat	Meata	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc1
York	York	k1gInSc1
Times	Times	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc1
York	York	k1gInSc1
Times	Times	k1gInSc1
<g/>
,	,	kIx,
May	May	k1gMnSc1
8	#num#	k4
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
CRAIG	CRAIG	kA
<g/>
,	,	kIx,
Winston	Winston	k1gInSc1
J.	J.	kA
Health	Health	k1gInSc1
effects	effects	k1gInSc1
of	of	k?
vegan	vegan	k1gInSc1
diets	diets	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
American	American	k1gMnSc1
Journal	Journal	k1gMnSc1
of	of	k?
Clinical	Clinical	k1gFnSc1
Nutrition	Nutrition	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
89	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
5	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1627	#num#	k4
<g/>
S	s	k7c7
<g/>
–	–	k?
<g/>
1633	#num#	k4
<g/>
S.	S.	kA
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
9165	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.394	10.394	k4
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
ajcn	ajcn	k1gInSc1
<g/>
.2009	.2009	k4
<g/>
.26736	.26736	k4
<g/>
n.	n.	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Health	Health	k1gInSc1
effects	effects	k1gInSc1
of	of	k?
vegetarian	vegetarian	k1gInSc1
and	and	k?
vegan	vegan	k1gInSc1
diets	diets	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proceedings	Proceedings	k1gInSc1
of	of	k?
the	the	k?
Nutrition	Nutrition	k1gInSc4
Society	societa	k1gFnSc2
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
65	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
35	#num#	k4
<g/>
–	–	k?
<g/>
41	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.107	10.107	k4
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
PNS	PNS	kA
<g/>
2005481	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
16441942	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
EPIC-Oxford	EPIC-Oxforda	k1gFnPc2
<g/>
:	:	kIx,
lifestyle	lifestyl	k1gInSc5
characteristics	characteristics	k6eAd1
and	and	k?
nutrient	nutrient	k1gMnSc1
intakes	intakes	k1gMnSc1
in	in	k?
a	a	k8xC
cohort	cohort	k1gInSc1
of	of	k?
33	#num#	k4
883	#num#	k4
meat-eaters	meat-eatersa	k1gFnPc2
and	and	k?
31	#num#	k4
546	#num#	k4
non	non	k?
meat-eaters	meat-eaters	k1gInSc1
in	in	k?
the	the	k?
UK	UK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Public	publicum	k1gNnPc2
Health	Healtha	k1gFnPc2
Nutrition	Nutrition	k1gInSc1
<g/>
.	.	kIx.
2003	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
6	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
3	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
259	#num#	k4
<g/>
–	–	k?
<g/>
69	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.107	10.107	k4
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
PHN	PHN	kA
<g/>
2002430	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
12740075	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
PETER	Petra	k1gFnPc2
EMERY	EMERY	kA
<g/>
,	,	kIx,
Tom	Tom	k1gMnSc1
Sanders	Sanders	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Molecular	Molecular	k1gMnSc1
Basis	Basis	k?
of	of	k?
Human	Human	k1gInSc1
Nutrition	Nutrition	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Taylor	Taylor	k1gMnSc1
&	&	k?
Francis	Francis	k1gFnSc2
Ltd	ltd	kA
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7484	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
753	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
32	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
BRENDA	BRENDA	kA
DAVIS	DAVIS	kA
<g/>
,	,	kIx,
Vesanto	Vesanta	k1gFnSc5
Melina	Melino	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc1
Becoming	Becoming	k1gInSc4
Vegetarian	Vegetarian	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Book	Book	k1gInSc1
Publishing	Publishing	k1gInSc1
Company	Compana	k1gFnSc2
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
57067	#num#	k4
<g/>
-	-	kIx~
<g/>
144	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
57	#num#	k4
<g/>
–	–	k?
<g/>
58	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Soybeans	Soybeans	k1gInSc1
<g/>
,	,	kIx,
mature	matur	k1gMnSc5
seeds	seeds	k1gInSc1
<g/>
,	,	kIx,
raw	raw	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NutritionPata	NutritionPata	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Seeds	Seeds	k1gInSc1
<g/>
,	,	kIx,
chia	chia	k6eAd1
seeds	seeds	k1gInSc1
<g/>
,	,	kIx,
dried	dried	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NutritionPata	NutritionPata	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Amaranth	Amaranth	k1gInSc1
<g/>
,	,	kIx,
uncooked	uncooked	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NutritionPata	NutritionPata	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Buckwheat	Buckwheat	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NutritionPata	NutritionPata	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Pumpkin	Pumpkin	k2eAgInSc4d1
Seeds	Seeds	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NutritionPata	NutritionPata	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Spirulina	Spirulin	k2eAgMnSc2d1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NutritionPata	NutritionPata	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Pistachio	Pistachio	k1gNnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NutritionPata	NutritionPata	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
http	http	k1gInSc1
<g/>
:	:	kIx,
//	//	k?
<g/>
nutritiondata	nutritiondata	k1gFnSc1
<g/>
.	.	kIx.
<g/>
self	self	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
/	/	kIx~
<g/>
facts	facts	k1gInSc1
<g/>
/	/	kIx~
<g/>
nut-and-seed-products	nut-and-seed-products	k1gInSc1
<g/>
/	/	kIx~
<g/>
3135	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Quinoa	Quinoa	k1gMnSc1
<g/>
,	,	kIx,
cooked	cooked	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NutritionPata	NutritionPata	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Plant	planta	k1gFnPc2
proteins	proteins	k6eAd1
in	in	k?
relation	relation	k1gInSc1
to	ten	k3xDgNnSc1
human	human	k1gInSc1
protein	protein	k1gInSc1
and	and	k?
amino	amino	k6eAd1
acid	acid	k6eAd1
nutrition	nutrition	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Am	Am	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
J.	J.	kA
Clinical	Clinical	k1gFnSc1
Nutrition	Nutrition	k1gInSc1
<g/>
.	.	kIx.
1994	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
59	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
5	#num#	k4
Suppl	Suppl	k1gFnPc2
<g/>
,	,	kIx,
s.	s.	k?
1203	#num#	k4
<g/>
S	s	k7c7
<g/>
–	–	k?
<g/>
1212	#num#	k4
<g/>
S.	S.	kA
PMID	PMID	kA
8172124	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Vegetarian	Vegetarian	k1gInSc1
Society	societa	k1gFnSc2
-	-	kIx~
Factsheet	Factsheet	k1gInSc1
-	-	kIx~
Iron	iron	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vegsoc	Vegsoc	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
,	,	kIx,
2014-09-22	2014-09-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Vegetarianism	Vegetarianism	k1gInSc1
in	in	k?
a	a	k8xC
Nutshell	Nutshell	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrg	Vrg	k1gFnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
//	//	k?
Health	Health	k1gMnSc1
Issues	Issues	k1gMnSc1
//	//	k?
Optimal	Optimal	k1gMnSc1
Vegan	Vegan	k1gMnSc1
Nutrition	Nutrition	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Goveg	Goveg	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Dietary	Dietara	k1gFnSc2
Iron	iron	k1gInSc4
Intake	Intak	k1gFnSc2
and	and	k?
Iron	iron	k1gInSc1
Status	status	k1gInSc1
of	of	k?
German	German	k1gMnSc1
Female	Femala	k1gFnSc3
Vegans	Vegans	k1gInSc1
<g/>
:	:	kIx,
Results	Results	k1gInSc1
of	of	k?
the	the	k?
German	German	k1gMnSc1
Vegan	Vegan	k1gMnSc1
Study	stud	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ann	Ann	k1gMnSc1
Nutr	Nutr	k1gMnSc1
Metab	Metab	k1gMnSc1
<g/>
.	.	kIx.
2004	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
48	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
103	#num#	k4
<g/>
–	–	k?
<g/>
108	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.115	10.115	k4
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
77045	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
14988640	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Influence	influence	k1gFnSc2
of	of	k?
vegetarian	vegetarian	k1gMnSc1
and	and	k?
mixed	mixed	k1gMnSc1
nutrition	nutrition	k1gInSc4
on	on	k3xPp3gMnSc1
selected	selected	k1gMnSc1
haematological	haematologicat	k5eAaPmAgMnS
and	and	k?
biochemical	biochemicat	k5eAaPmAgInS
parameters	parameters	k1gInSc1
in	in	k?
children	childrna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nahrung	Nahrung	k1gInSc1
<g/>
.	.	kIx.
1997	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
41	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
5	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
311	#num#	k4
<g/>
–	–	k?
<g/>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
food	food	k6eAd1
<g/>
.19970410513	.19970410513	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
9399258	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Position	Position	k1gInSc1
of	of	k?
the	the	k?
American	American	k1gInSc1
Dietetic	Dietetice	k1gFnPc2
Association	Association	k1gInSc1
<g/>
:	:	kIx,
vegetarian	vegetarian	k1gInSc1
diets	dietsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
the	the	k?
American	American	k1gInSc1
Dietetic	Dietetice	k1gFnPc2
Association	Association	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
109	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
7	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1266	#num#	k4
<g/>
–	–	k?
<g/>
82	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
j.	j.	k?
<g/>
jada	jada	k6eAd1
<g/>
.2009	.2009	k4
<g/>
.05	.05	k4
<g/>
.027	.027	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
19562864	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Dietary	Dietar	k1gInPc7
Supplement	Supplement	k1gMnSc1
Fact	Fact	k1gMnSc1
Sheet	Sheet	k1gMnSc1
<g/>
:	:	kIx,
Vitamin	vitamin	k1gInSc1
B12	B12	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
National	National	k1gMnSc1
Institutes	Institutes	k1gMnSc1
of	of	k?
Health	Health	k1gMnSc1
<g/>
:	:	kIx,
Office	Office	kA
of	of	k?
Dietary	Dietar	k1gInPc1
Supplements	Supplements	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Food	Food	k1gInSc1
and	and	k?
description	description	k1gInSc1
[	[	kIx(
<g/>
PDF	PDF	kA
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mext	Mext	k1gInSc1
<g/>
.	.	kIx.
<g/>
go	go	k?
<g/>
.	.	kIx.
<g/>
jp	jp	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
<g/>
.	.	kIx.
↑	↑	k?
（	（	k?
<g/>
科	科	k?
<g/>
・	・	k?
<g/>
学	学	k?
<g/>
）	）	k?
<g/>
.	.	kIx.
五	五	k?
<g/>
：	：	k?
<g/>
文	文	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mext	Mext	k1gInSc1
<g/>
.	.	kIx.
<g/>
go	go	k?
<g/>
.	.	kIx.
<g/>
jp	jp	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
CiNii	CiNie	k1gFnSc3
Articles	Articles	k1gMnSc1
-	-	kIx~
Vegans	Vegans	k1gInSc1
(	(	kIx(
<g/>
pure	pure	k1gNnSc1
vegetarians	vegetariansa	k1gFnPc2
<g/>
)	)	kIx)
and	and	k?
vitamin	vitamin	k1gInSc1
B_	B_	k1gFnSc2
<g/>
12	#num#	k4
deficiency	deficienca	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ci	ci	k0
<g/>
.	.	kIx.
<g/>
nii	nii	k?
<g/>
.	.	kIx.
<g/>
ac	ac	k?
<g/>
.	.	kIx.
<g/>
jp	jp	k?
<g/>
,	,	kIx,
2003-12-20	2003-12-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
What	What	k1gMnSc1
Every	Evera	k1gFnSc2
Vegan	Vegan	k1gMnSc1
Should	Should	k1gMnSc1
Know	Know	k1gMnSc1
About	About	k2eAgInSc4d1
Vitamin	vitamin	k1gInSc4
B12	B12	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vegan	Vegan	k1gInSc1
Society	societa	k1gFnSc2
<g/>
,	,	kIx,
October	October	k1gInSc1
31	#num#	k4
<g/>
,	,	kIx,
2001	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
</s>
<s>
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Donaldson	Donaldson	k1gInSc1
MS.	MS.	k1gMnSc1
Metabolic	Metabolice	k1gFnPc2
vitamin	vitamin	k1gInSc1
B12	B12	k1gMnSc1
status	status	k1gInSc1
on	on	k3xPp3gMnSc1
a	a	k8xC
mostly	mostnout	k5eAaPmAgFnP
raw	raw	k?
vegan	vegan	k1gInSc1
diet	dieta	k1gFnPc2
with	with	k1gMnSc1
follow-up	follow-up	k1gMnSc1
using	using	k1gMnSc1
tablets	tablets	k6eAd1
<g/>
,	,	kIx,
nutritional	nutritionat	k5eAaPmAgMnS,k5eAaImAgMnS
yeast	yeast	k1gMnSc1
<g/>
,	,	kIx,
or	or	k?
probiotic	probiotice	k1gFnPc2
supplements	supplements	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annals	Annals	k1gInSc1
of	of	k?
Nutrition	Nutrition	k1gInSc1
&	&	k?
Metabolism	Metabolism	k1gInSc1
<g/>
.	.	kIx.
2000	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
44	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
5	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
229	#num#	k4
<g/>
–	–	k?
<g/>
34	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.115	10.115	k4
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
46689	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
11146329	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Vitamins	Vitamins	k1gInSc1
and	and	k?
minerals	minerals	k1gInSc1
-	-	kIx~
NHS	NHS	kA
Choices	Choices	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nhs	Nhs	k1gFnSc1
<g/>
.	.	kIx.
<g/>
uk	uk	k?
<g/>
,	,	kIx,
2015-02-18	2015-02-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
MOZAFAR	MOZAFAR	kA
<g/>
,	,	kIx,
A.	A.	kA
Is	Is	k1gFnSc1
there	ther	k1gInSc5
vitamin	vitamin	k1gInSc1
B12	B12	k1gFnSc1
in	in	k?
plants	plants	k6eAd1
or	or	k?
not	nota	k1gFnPc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
A	A	kA
plant	planta	k1gFnPc2
nutritionist	nutritionist	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
view	view	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vegetarian	Vegetarian	k1gInSc1
Nutrition	Nutrition	k1gInSc4
<g/>
:	:	kIx,
an	an	k?
International	International	k1gMnSc1
Journal	Journal	k1gMnSc1
<g/>
.	.	kIx.
1997	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
50	#num#	k4
<g/>
–	–	k?
<g/>
52	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Long-chain	Long-chain	k1gInSc1
n-	n-	k?
<g/>
3	#num#	k4
polyunsaturated	polyunsaturated	k1gInSc1
fatty	fatta	k1gFnSc2
acids	acidsa	k1gFnPc2
in	in	k?
plasma	plasma	k1gNnSc4
in	in	k?
British	British	k1gInSc1
meat-eating	meat-eating	k1gInSc1
<g/>
,	,	kIx,
vegetarian	vegetarian	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
vegan	vegan	k1gInSc1
men	men	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Am	Am	k1gFnPc2
J	J	kA
Clin	Clin	k1gMnSc1
Nutr	Nutr	k1gMnSc1
<g/>
.	.	kIx.
2003	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
82	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
327	#num#	k4
<g/>
–	–	k?
<g/>
34	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
16087975	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Water	Water	k1gMnSc1
<g/>
4	#num#	k4
<g/>
life	life	k1gInSc1
<g/>
:	:	kIx,
health-giving	health-giving	k1gInSc1
vegetarian	vegetariana	k1gFnPc2
dietary	dietara	k1gFnSc2
supplements	supplements	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Babadzhanov	Babadzhanov	k1gInSc1
A	a	k9
<g/>
;	;	kIx,
Abdusamatova	Abdusamatův	k2eAgFnSc1d1
N	N	kA
<g/>
;	;	kIx,
Yusupova	Yusupův	k2eAgFnSc1d1
F	F	kA
<g/>
;	;	kIx,
Faizullaeva	Faizullaeva	k1gFnSc1
N.	N.	kA
Chemical	Chemical	k1gFnSc1
Composition	Composition	k1gInSc1
of	of	k?
Spirulina	Spirulin	k2eAgFnSc1d1
platensis	platensis	k1gFnSc1
Cultivated	Cultivated	k1gInSc1
in	in	k?
Uzbekistan	Uzbekistan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chemistry	Chemistr	k1gMnPc4
of	of	k?
Natural	Natural	k?
Compounds	Compounds	k1gInSc1
<g/>
.	.	kIx.
2004	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
40	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
3	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
276	#num#	k4
<g/>
–	–	k?
<g/>
279	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.102	10.102	k4
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
B	B	kA
<g/>
:	:	kIx,
<g/>
CONC	CONC	kA
<g/>
.0000039141	.0000039141	k4
<g/>
.98247	.98247	k4
<g/>
.	.	kIx.
<g/>
e	e	k0
<g/>
8	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Biomass	Biomass	k1gInSc1
Nutrient	Nutrient	k1gMnSc1
Profiles	Profiles	k1gMnSc1
of	of	k?
Three	Three	k1gFnSc1
Microalgae	Microalgae	k1gFnSc1
<g/>
:	:	kIx,
Spirulina	Spirulin	k2eAgFnSc1d1
platensis	platensis	k1gFnSc1
<g/>
,	,	kIx,
Chlorella	Chlorella	k1gFnSc1
vulgaris	vulgaris	k1gFnSc1
<g/>
,	,	kIx,
and	and	k?
Isochrisis	Isochrisis	k1gFnSc1
galena	galena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
Food	Food	k1gInSc1
Science	Science	k1gFnSc1
<g/>
.	.	kIx.
2003	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
68	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
4	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1144	#num#	k4
<g/>
–	–	k?
<g/>
1148	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.111	10.111	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
j	j	k?
<g/>
.1365	.1365	k4
<g/>
-	-	kIx~
<g/>
2621.2003	2621.2003	k4
<g/>
.	.	kIx.
<g/>
tb	tb	k?
<g/>
0	#num#	k4
<g/>
9615	#num#	k4
<g/>
.	.	kIx.
<g/>
x.	x.	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Meeting	meeting	k1gInSc1
Calcium	Calcium	k1gNnSc1
Recommendations	Recommendations	k1gInSc1
on	on	k3xPp3gMnSc1
a	a	k8xC
Vegan	Vegan	k1gInSc1
Diet	dieta	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Academy	Academa	k1gFnSc2
of	of	k?
Nutrition	Nutrition	k1gInSc1
and	and	k?
Dietetics	Dietetics	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
Calcium	Calcium	k1gNnSc4
Fact	Fact	k2eAgInSc1d1
Sheet	Sheet	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
MANGELS	MANGELS	kA
<g/>
,	,	kIx,
Reed	Reed	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Calcium	Calcium	k1gNnSc1
in	in	k?
the	the	k?
Vegan	Vegan	k1gInSc1
Diet	dieta	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
National	National	k1gMnSc1
Institutes	Institutes	k1gMnSc1
of	of	k?
Health	Health	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Overview	Overview	k1gFnSc1
of	of	k?
Calcium	Calcium	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dietary	Dietar	k1gMnPc7
Reference	reference	k1gFnSc2
Intakes	Intakes	k1gInSc1
for	forum	k1gNnPc2
Calcium	Calcium	k1gNnSc4
and	and	k?
Vitamin	vitamin	k1gInSc1
D.	D.	kA
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
29	#num#	k4
April	April	k1gInSc1
2014	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Vitamin	vitamin	k1gInSc1
D	D	kA
is	is	k?
Synthesized	Synthesized	k1gInSc1
From	From	k1gMnSc1
Cholesterol	cholesterol	k1gInSc1
and	and	k?
Found	Found	k1gInSc1
in	in	k?
Cholesterol-Rich	Cholesterol-Rich	k1gInSc1
Foods	Foods	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cholesterol	cholesterol	k1gInSc1
and	and	k?
Health	Health	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Serum	Serum	k1gInSc1
concentrations	concentrations	k1gInSc1
of	of	k?
lipids	lipids	k1gInSc1
<g/>
,	,	kIx,
vitamin	vitamin	k1gInSc1
D	D	kA
metabolites	metabolites	k1gInSc1
<g/>
,	,	kIx,
retinol	retinol	k1gInSc1
<g/>
,	,	kIx,
retinyl	retinyl	k1gInSc1
esters	esters	k1gInSc1
<g/>
,	,	kIx,
tocopherols	tocopherols	k1gInSc1
and	and	k?
selected	selected	k1gInSc1
carotenoids	carotenoids	k1gInSc1
in	in	k?
twelve	twelvat	k5eAaPmIp3nS
captive	captiv	k1gInSc5
wild	wild	k6eAd1
felid	felid	k1gInSc1
species	species	k1gFnSc2
at	at	k?
four	four	k1gInSc1
zoos	zoos	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Journal	Journal	k1gMnSc1
of	of	k?
Nutrition	Nutrition	k1gInSc1
<g/>
.	.	kIx.
2003	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
133	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
160	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
12514284	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Dietary	Dietara	k1gFnSc2
Supplement	Supplement	k1gMnSc1
Fact	Fact	k1gMnSc1
Sheet	Sheet	k1gMnSc1
<g/>
:	:	kIx,
Vitamin	vitamin	k1gInSc1
D	D	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
National	National	k1gMnSc1
Institutes	Institutes	k1gMnSc1
of	of	k?
Health	Health	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
BOWERMAN	BOWERMAN	kA
<g/>
,	,	kIx,
Susan	Susan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
If	If	k1gFnSc1
mushrooms	mushroomsa	k1gFnPc2
see	see	k?
the	the	k?
light	light	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Los	los	k1gMnSc1
Angeles	Angeles	k1gMnSc1
Times	Times	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
March	March	k1gInSc1
31	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
March	March	k1gInSc1
25	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
USDA	USDA	kA
nutrient	nutrient	k1gMnSc1
database	database	k6eAd1
–	–	k?
use	usus	k1gInSc5
the	the	k?
keyword	keyword	k1gInSc1
'	'	kIx"
<g/>
portabella	portabella	k1gFnSc1
<g/>
'	'	kIx"
and	and	k?
then	then	k1gInSc1
click	click	k1gMnSc1
submit	submit	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Dr	dr	kA
<g/>
.	.	kIx.
Duke	Duk	k1gMnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Phytochemical	Phytochemical	k1gFnSc7
and	and	k?
Ethnobotanical	Ethnobotanical	k1gMnSc1
Databases	Databases	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sun	sun	k1gInSc1
<g/>
.	.	kIx.
<g/>
ars-grin	ars-grin	k1gInSc1
<g/>
.	.	kIx.
<g/>
gov	gov	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Mortality	mortalita	k1gFnSc2
in	in	k?
vegetarians	vegetarians	k1gInSc1
and	and	k?
non-vegetarians	non-vegetarians	k1gInSc1
<g/>
:	:	kIx,
detailed	detailed	k1gInSc1
findings	findings	k6eAd1
from	from	k6eAd1
a	a	k8xC
collaborative	collaborativ	k1gInSc5
analysis	analysis	k1gInSc4
of	of	k?
5	#num#	k4
prospective	prospectiv	k1gInSc5
studies	studiesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
American	American	k1gMnSc1
Journal	Journal	k1gMnSc1
of	of	k?
Clinical	Clinical	k1gMnSc4
Nutrition	Nutrition	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
September	September	k1gInSc1
1999	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
70	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
3	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
516	#num#	k4
<g/>
S	s	k7c7
<g/>
–	–	k?
<g/>
524	#num#	k4
<g/>
S.	S.	kA
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
October	October	k1gInSc1
30	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.107	10.107	k4
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
phn	phn	k?
<g/>
19980006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
10479225	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Mortality	mortalita	k1gFnSc2
in	in	k?
British	British	k1gInSc1
vegetarians	vegetarians	k1gInSc1
<g/>
:	:	kIx,
review	review	k?
and	and	k?
preliminary	preliminara	k1gFnSc2
results	resultsa	k1gFnPc2
from	from	k1gMnSc1
EPIC-Oxford	EPIC-Oxford	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
American	American	k1gMnSc1
Journal	Journal	k1gMnSc1
of	of	k?
Clinical	Clinical	k1gFnSc1
Nutrition	Nutrition	k1gInSc1
<g/>
.	.	kIx.
2003	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
78	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
3	#num#	k4
Suppl	Suppl	k1gFnPc2
<g/>
,	,	kIx,
s.	s.	k?
533	#num#	k4
<g/>
S	s	k7c7
<g/>
–	–	k?
<g/>
538	#num#	k4
<g/>
S.	S.	kA
PMID	PMID	kA
12936946	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Mortality	mortalita	k1gFnSc2
in	in	k?
British	British	k1gInSc1
vegetarians	vegetarians	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Public	publicum	k1gNnPc2
health	healtha	k1gFnPc2
nutrition	nutrition	k1gInSc1
<g/>
.	.	kIx.
2002	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
5	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
29	#num#	k4
<g/>
–	–	k?
<g/>
36	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.107	10.107	k4
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
PHN	PHN	kA
<g/>
2001248	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
12001975	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Loma	Lom	k2eAgFnSc1d1
Linda	Linda	k1gFnSc1
University	universita	k1gFnSc2
Adventist	Adventist	k1gMnSc1
Health	Health	k1gMnSc1
Sciences	Sciences	k1gMnSc1
Center	centrum	k1gNnPc2
<g/>
,	,	kIx,
New	New	k1gMnSc1
Adventist	Adventist	k1gMnSc1
Health	Health	k1gMnSc1
Study	stud	k1gInPc7
research	research	k1gMnSc1
noted	noted	k1gMnSc1
in	in	k?
Archives	Archives	k1gMnSc1
of	of	k?
Internal	Internal	k1gMnSc5
Medicine	Medicin	k1gMnSc5
<g/>
,	,	kIx,
Loma	Lom	k2eAgFnSc1d1
Linda	Linda	k1gFnSc1
University	universita	k1gFnSc2
<g/>
,	,	kIx,
26	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Citováno	citovat	k5eAaBmNgNnS
9.1	9.1	k4
<g/>
.2010	.2010	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Does	Does	k1gInSc1
low	low	k?
meat	meat	k2eAgInSc1d1
consumption	consumption	k1gInSc1
increase	increase	k6eAd1
life	lifat	k5eAaPmIp3nS
expectancy	expectanca	k1gFnPc4
in	in	k?
humans	humans	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Am	Am	k1gFnPc2
J	J	kA
Clin	Clin	k1gMnSc1
Nutr	Nutr	k1gMnSc1
<g/>
.	.	kIx.
2003	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
78	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
3	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
526	#num#	k4
<g/>
S	s	k7c7
<g/>
–	–	k?
<g/>
532	#num#	k4
<g/>
S.	S.	kA
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
28	#num#	k4
September	September	k1gInSc1
2012	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
12936945	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Modified	Modified	k1gInSc1
Mediterranean	Mediterranean	k1gMnSc1
diet	dieta	k1gFnPc2
and	and	k?
survival	survivat	k5eAaBmAgMnS,k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
:	:	kIx,
EPIC-elderly	EPIC-elderl	k1gInPc1
prospective	prospectiv	k1gInSc5
cohort	cohort	k1gInSc4
study	stud	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BMJ	BMJ	kA
<g/>
.	.	kIx.
2005	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
330	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
7498	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
991	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.113	10.113	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
bmj	bmj	k?
<g/>
.38415	.38415	k4
<g/>
.644155	.644155	k4
<g/>
.8	.8	k4
<g/>
F.	F.	kA
PMID	PMID	kA
15820966	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Advanced	Advanced	k1gInSc1
Glycation	Glycation	k1gInSc4
End	End	k1gFnSc2
Products	Products	k1gInSc1
and	and	k?
Nutrition	Nutrition	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
L	L	kA
M	M	kA
TIERNEY	TIERNEY	kA
<g/>
,	,	kIx,
S	s	k7c7
J	J	kA
MCPHEE	MCPHEE	kA
<g/>
,	,	kIx,
M	M	kA
A	a	k8xC
Papadakis	Papadakis	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Current	Current	k1gInSc1
medical	medicat	k5eAaPmAgInS
Diagnosis	Diagnosis	k1gFnPc4
&	&	k?
Treatment	Treatment	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
International	International	k1gMnSc2
edition	edition	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Lange	Lange	k1gFnSc1
Medical	Medical	k1gMnSc1
Books	Books	k1gInSc1
<g/>
/	/	kIx~
<g/>
McGraw-Hill	McGraw-Hill	k1gInSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
137688	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Tuso	Tuso	k1gMnSc1
<g/>
,	,	kIx,
Philip	Philip	k1gMnSc1
J.	J.	kA
Nutritional	Nutritional	k1gFnSc1
Update	update	k1gInSc1
for	forum	k1gNnPc2
Physicians	Physicians	k1gInSc1
<g/>
:	:	kIx,
Plant-Based	Plant-Based	k1gInSc1
Diets	Dietsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Permanente	permanent	k1gInSc5
Journal	Journal	k1gMnSc6
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
17.2	17.2	k4
<g/>
,	,	kIx,
s.	s.	k?
61	#num#	k4
<g/>
–	–	k?
<g/>
66	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Hagen	Hagen	k1gInSc1
KB	kb	kA
<g/>
,	,	kIx,
Byfuglien	Byfuglien	k2eAgMnSc1d1
MG	mg	kA
<g/>
,	,	kIx,
Falzon	Falzon	k1gNnSc1
L	L	kA
<g/>
,	,	kIx,
Olsen	Olsen	k2eAgMnSc1d1
SU	SU	k?
<g/>
,	,	kIx,
Smedslund	Smedslund	k1gInSc1
G.	G.	kA
Dietary	Dietara	k1gFnSc2
interventions	interventions	k6eAd1
for	forum	k1gNnPc2
rheumatoid	rheumatoid	k1gInSc1
arthritis	arthritis	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cochrane	Cochran	k1gInSc5
Database	Databas	k1gInSc6
Syst	Syst	k1gMnSc1
Rev	Rev	k1gMnSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
CD	CD	kA
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
6400	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
14651858	#num#	k4
<g/>
.	.	kIx.
<g/>
CD	CD	kA
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
6400	#num#	k4
<g/>
.	.	kIx.
<g/>
pub	pub	k?
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
19160281	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Maya	Maya	k?
Tiwari	Tiwar	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ájurvéda	Ájurvédo	k1gNnPc1
<g/>
:	:	kIx,
A	a	k9
Life	Life	k1gFnSc1
Balance	balance	k1gFnSc1
Healing	Healing	k1gInSc4
Arts	Arts	k1gInSc1
Press	Press	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rochester	Rochester	k1gInSc1
<g/>
,	,	kIx,
VT	VT	kA
<g/>
.	.	kIx.
1995	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Haenel	Haenlo	k1gNnPc2
H.	H.	kA
Phylogenesis	Phylogenesis	k1gFnSc2
and	and	k?
nutrition	nutrition	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nahrung	Nahrung	k1gInSc1
<g/>
.	.	kIx.
1989	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
33	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
9	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
867	#num#	k4
<g/>
–	–	k?
<g/>
87	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
2697806	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Cordain	Cordaina	k1gFnPc2
<g/>
,	,	kIx,
Loren	Lorna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evolution	Evolution	k1gInSc1
of	of	k?
the	the	k?
human	human	k1gInSc1
diet	dieta	k1gFnPc2
<g/>
:	:	kIx,
the	the	k?
known	known	k1gInSc1
<g/>
,	,	kIx,
the	the	k?
unknown	unknown	k1gInSc1
and	and	k?
the	the	k?
unknowable	unknowable	k6eAd1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Implications	Implicationsa	k1gFnPc2
of	of	k?
Plio-pleistocene	Plio-pleistocen	k1gInSc5
diets	dietsa	k1gFnPc2
for	forum	k1gNnPc2
modern	modern	k1gMnSc1
humans	humans	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
264	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Milton	Milton	k1gInSc1
<g/>
,	,	kIx,
Katharine	Katharin	k1gInSc5
<g/>
,	,	kIx,
""	""	k?
<g/>
A	a	k8xC
hypothesis	hypothesis	k1gInSc1
to	ten	k3xDgNnSc4
explain	explain	k1gMnSc1
the	the	k?
role	role	k1gFnSc2
of	of	k?
meat-eating	meat-eating	k1gInSc1
in	in	k?
human	human	k1gInSc1
evolution	evolution	k1gInSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
Evolutionary	Evolutionar	k1gInPc1
Anthropology	Anthropolog	k1gMnPc4
<g/>
:	:	kIx,
Issues	Issues	k1gInSc1
<g/>
,	,	kIx,
News	News	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
Reviews	Reviews	k1gInSc1
Volume	volum	k1gInSc5
8	#num#	k4
<g/>
,	,	kIx,
Issue	Issue	k1gFnSc1
1	#num#	k4
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
,	,	kIx,
stránky	stránka	k1gFnPc1
<g/>
:	:	kIx,
11	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
↑	↑	k?
ABC	ABC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ABC	ABC	kA
<g/>
,	,	kIx,
2003-02-25	2003-02-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Position	Position	k1gInSc1
of	of	k?
the	the	k?
American	American	k1gInSc1
Dietetic	Dietetice	k1gFnPc2
Association	Association	k1gInSc1
<g/>
:	:	kIx,
vegetarian	vegetarian	k1gInSc1
diets	dietsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
J	J	kA
Am	Am	k1gFnPc2
Diet	dieta	k1gFnPc2
Assoc	Assoc	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
109	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
7	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1266	#num#	k4
<g/>
–	–	k?
<g/>
1282	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
j.	j.	k?
<g/>
jada	jada	k6eAd1
<g/>
.2009	.2009	k4
<g/>
.05	.05	k4
<g/>
.027	.027	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
19562864	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archivováno	archivován	k2eAgNnSc4d1
26	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
KAUCKÁ	KAUCKÁ	kA
<g/>
,	,	kIx,
Tereza	Tereza	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vliv	vliv	k1gInSc1
vegetariánství	vegetariánství	k1gNnSc2
na	na	k7c4
zdravotní	zdravotní	k2eAgInSc4d1
stav	stav	k1gInSc4
člověka	člověk	k1gMnSc2
<g/>
.	.	kIx.
,	,	kIx,
2006	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
76	#num#	k4
s.	s.	k?
Bakalářská	bakalářský	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedoucí	vedoucí	k2eAgFnSc1d1
práce	práce	k1gFnSc1
Václav	Václav	k1gMnSc1
Břicháček	Břicháček	k?
<g/>
.	.	kIx.
s.	s.	k?
32	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Vegetarianism	Vegetarianism	k1gInSc1
in	in	k?
anorexia	anorexia	k1gFnSc1
nervosa	nervosa	k1gFnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
A	a	k8xC
review	review	k?
of	of	k?
116	#num#	k4
consecutive	consecutiv	k1gInSc5
cases	casesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Med	med	k1gInSc1
J	J	kA
Aust	Aust	k1gInSc1
<g/>
.	.	kIx.
1987	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
147	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
11	#num#	k4
<g/>
–	–	k?
<g/>
12	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
540	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
3696039	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
David	David	k1gMnSc1
Benatar	Benatar	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Why	Why	k1gFnSc1
the	the	k?
Naive	Naiev	k1gFnSc2
Argument	argument	k1gInSc1
against	against	k1gMnSc1
Moral	Moral	k1gMnSc1
Vegetarianism	Vegetarianism	k1gMnSc1
Really	Realla	k1gFnSc2
is	is	k?
Naive	Naiev	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Environmental	Environmental	k1gMnSc1
Values	Values	k1gMnSc1
<g/>
.	.	kIx.
2001	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
10	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
103	#num#	k4
<g/>
–	–	k?
<g/>
112	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.319	10.319	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
96327101129340769	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
↑	↑	k?
Animals	Animals	k1gInSc1
and	and	k?
Ethics	Ethics	k1gInSc1
[	[	kIx(
<g/>
Internet	Internet	k1gInSc1
Encyclopedia	Encyclopedium	k1gNnSc2
of	of	k?
Philosophy	Philosopha	k1gMnSc2
<g/>
]	]	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Iep	Iep	k1gFnSc1
<g/>
.	.	kIx.
<g/>
utm	utm	k?
<g/>
.	.	kIx.
<g/>
edu	edu	k?
<g/>
,	,	kIx,
2010-01-13	2010-01-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Erik	Erik	k1gMnSc1
Marcus	Marcus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vegan	Vegan	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
New	New	k1gMnSc1
Ethics	Ethics	k1gInSc1
of	of	k?
Eating	Eating	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9781590133446	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Vegetarian	Vegetariany	k1gInPc2
Society	societa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dairy	Daira	k1gFnSc2
Cows	Cowsa	k1gFnPc2
&	&	k?
Welfare	Welfar	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Vegetarian	Vegetarian	k1gInSc1
Society	societa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Egg	Egg	k1gFnPc2
Production	Production	k1gInSc4
&	&	k?
Welfare	Welfar	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Beans	Beans	k1gInSc1
and	and	k?
Greens	Greens	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
History	Histor	k1gInPc1
of	of	k?
Vegetarianism	Vegetarianism	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
HISTORY	HISTORY	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Ovidius	Ovidius	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proměny	proměna	k1gFnPc1
<g/>
,	,	kIx,
kniha	kniha	k1gFnSc1
XV	XV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Jan	Jan	k1gMnSc1
Červenka	Červenka	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
A.	A.	kA
Storch	Storch	k1gMnSc1
syn	syn	k1gMnSc1
<g/>
,	,	kIx,
1895	#num#	k4
<g/>
.	.	kIx.
289	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
XV	XV	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
s.	s.	k?
258	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Kochhal	Kochhal	k1gMnSc1
<g/>
,	,	kIx,
M.	M.	kA
Vegetarianism	Vegetarianism	k1gInSc1
<g/>
:	:	kIx,
Jainism	Jainism	k1gInSc1
and	and	k?
vegetarianism	vegetarianism	k1gInSc1
(	(	kIx(
<g/>
ahisma	ahisma	k1gFnSc1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
October	October	k1gInSc1
2004	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Teachings	Teachings	k1gInSc1
on	on	k3xPp3gMnSc1
Love	lov	k1gInSc5
<g/>
,	,	kIx,
Thich	Thich	k1gMnSc1
Nhat	Nhat	k1gMnSc1
Hanh	Hanh	k1gMnSc1
<g/>
,	,	kIx,
Berkeley	Berkeley	k1gInPc1
<g/>
:	:	kIx,
Parallax	Parallax	k1gInSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Junior	junior	k1gMnSc1
Encyclopaedia	Encyclopaedium	k1gNnSc2
of	of	k?
Sikhism	Sikhism	k1gMnSc1
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
l	l	kA
HS	HS	kA
Singha	Singha	k1gFnSc1
<g/>
;	;	kIx,
p.	p.	k?
124	#num#	k4
ISBN	ISBN	kA
0-7069-2844-X	0-7069-2844-X	k4
/	/	kIx~
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7069	#num#	k4
<g/>
-	-	kIx~
<g/>
2844	#num#	k4
<g/>
-X	-X	k?
<g/>
↑	↑	k?
KAKSHI	KAKSHI	kA
<g/>
,	,	kIx,
S.	S.	kA
<g/>
R.	R.	kA
Punjab	Punjab	k1gMnSc1
Through	Through	k1gMnSc1
the	the	k?
Ages	Ages	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gFnSc1
Delhi	Delh	k1gFnSc2
<g/>
:	:	kIx,
Sarup	Sarup	k1gInSc1
and	and	k?
Sons	Sons	k1gInSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Through	Through	k1gInSc1
the	the	k?
Ages	Agesa	k1gFnPc2
By	by	k9
S.	S.	kA
<g/>
R.	R.	kA
Bakshi	Bakshi	k1gNnSc4
<g/>
%	%	kIx~
<g/>
2	#num#	k4
<g/>
C	C	kA
Rashmi	Rash	k1gFnPc7
Pathak	Pathak	k1gMnSc1
<g/>
%	%	kIx~
<g/>
2	#num#	k4
<g/>
C	C	kA
Rashmi	Rash	k1gFnPc7
Pathak	Pathak	k1gInSc1
volume	volum	k1gInSc5
4	#num#	k4
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
81	#num#	k4
<g/>
-	-	kIx~
<g/>
7625	#num#	k4
<g/>
-	-	kIx~
<g/>
738	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
12	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
241	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
SINGHA	SINGHA	kA
<g/>
,	,	kIx,
H.	H.	kA
S.	S.	kA
Junior	junior	k1gMnSc1
encyclopaedia	encyclopaedium	k1gNnSc2
of	of	k?
Sikhism	Sikhism	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Vikas	Vikas	k1gMnSc1
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7069	#num#	k4
<g/>
-	-	kIx~
<g/>
2844	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
S.	S.	kA
124	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Shiromani	Shiroman	k1gMnPc1
Gurudwara	Gurudwara	k1gFnSc1
Prabhandhak	Prabhandhak	k1gMnSc1
Committee	Committee	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sgpc	Sgpc	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
The	The	k1gMnSc1
Sikhism	Sikhism	k1gInPc3
Home	Hom	k1gInSc2
Page	Page	k1gNnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sikhs	Sikhs	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
,	,	kIx,
1980-02-15	1980-02-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
SMITH	SMITH	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
concise	concise	k1gFnPc1
encyclopedia	encyclopedium	k1gNnSc2
of	of	k?
the	the	k?
Bahá	Bahá	k1gFnSc1
<g/>
'	'	kIx"
<g/>
í	í	k0
Faith	Faith	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Diet	dieta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oxford	Oxford	k1gInSc1
<g/>
:	:	kIx,
Oneworld	Oneworld	k1gInSc1
Publications	Publicationsa	k1gFnPc2
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
85168	#num#	k4
<g/>
-	-	kIx~
<g/>
184	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
121	#num#	k4
<g/>
–	–	k?
<g/>
122	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
encyclopedia	encyclopedium	k1gNnPc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Esslemont	Esslemont	k1gInSc1
<g/>
,	,	kIx,
J.E.	J.E.	k1gFnSc1
Bahá	Bahá	k1gFnSc1
<g/>
'	'	kIx"
<g/>
u	u	k7c2
<g/>
'	'	kIx"
<g/>
lláh	lláh	k1gInSc1
and	and	k?
the	the	k?
New	New	k1gMnSc1
Era	Era	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wilmette	Wilmett	k1gInSc5
<g/>
,	,	kIx,
Illinois	Illinois	k1gFnPc7
<g/>
,	,	kIx,
USA	USA	kA
<g/>
:	:	kIx,
Bahá	Bahá	k1gFnSc1
<g/>
'	'	kIx"
<g/>
í	í	k0
Publishing	Publishing	k1gInSc1
Trust	trust	k1gInSc1
<g/>
,	,	kIx,
1980	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
87743	#num#	k4
<g/>
-	-	kIx~
<g/>
160	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
`	`	kIx"
<g/>
ABDU	ABDU	kA
<g/>
'	'	kIx"
<g/>
L-BAHÁ	L-BAHÁ	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnPc2
Promulgation	Promulgation	k1gInSc4
of	of	k?
Universal	Universal	k1gMnSc2
Peace	Peace	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wilmette	Wilmett	k1gInSc5
<g/>
,	,	kIx,
Illinois	Illinois	k1gFnPc7
<g/>
,	,	kIx,
US	US	kA
<g/>
:	:	kIx,
Bahá	Bahá	k1gFnSc1
<g/>
'	'	kIx"
<g/>
í	í	k0
Publishing	Publishing	k1gInSc1
Trust	trust	k1gInSc1
<g/>
,	,	kIx,
1912	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
87743	#num#	k4
<g/>
-	-	kIx~
<g/>
172	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Research	Research	k1gInSc1
Department	department	k1gInSc1
<g/>
,	,	kIx,
Universal	Universal	k1gFnPc1
House	house	k1gNnSc1
of	of	k?
Justice	justice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Writings	Writings	k1gInSc1
Concerning	Concerning	k1gInSc1
Health	Health	k1gInSc1
<g/>
,	,	kIx,
Healing	Healing	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
Nutrition	Nutrition	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Buddhist	Buddhist	k1gMnSc1
Studies	Studies	k1gMnSc1
<g/>
:	:	kIx,
Vegetarianism	Vegetarianism	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Buddhanet	Buddhanet	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Archivováno	archivován	k2eAgNnSc1d1
7	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
Mahavagga	Mahavagga	k1gFnSc1
Pali	Pal	k1gFnSc2
-	-	kIx~
Bhesajjakkhandhaka	Bhesajjakkhandhak	k1gMnSc2
-	-	kIx~
Vinaya	Vinayus	k1gMnSc2
Pitaka	Pitak	k1gMnSc2
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Buddhism	Buddhism	k1gMnSc1
and	and	k?
Vegetarianism	Vegetarianism	k1gMnSc1
<g/>
,	,	kIx,
The	The	k1gMnSc1
Rationale	Rationale	k1gMnSc1
for	forum	k1gNnPc2
the	the	k?
Buddha	Buddha	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Views	Views	k1gInSc1
on	on	k3xPp3gMnSc1
the	the	k?
Consumption	Consumption	k1gInSc1
of	of	k?
Meat	Meat	k1gInSc1
<g/>
"	"	kIx"
by	by	k9
Dr	dr	kA
V.	V.	kA
A.	A.	kA
Gunasekara	Gunasekara	k1gFnSc1
<g/>
"	"	kIx"
'	'	kIx"
<g/>
The	The	k1gFnSc2
rule	rula	k1gFnSc3
of	of	k?
vegetarianism	vegetarianism	k1gInSc1
was	was	k?
the	the	k?
fifth	fifth	k1gInSc1
of	of	k?
a	a	k8xC
list	list	k1gInSc1
of	of	k?
rules	rules	k1gInSc1
which	which	k1gMnSc1
Devadatta	Devadatta	k1gMnSc1
had	had	k1gMnSc1
proposed	proposed	k1gMnSc1
to	ten	k3xDgNnSc4
the	the	k?
Buddha	Buddha	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Devadatta	Devadatta	k1gFnSc1
was	was	k?
the	the	k?
founder	founder	k1gInSc1
of	of	k?
the	the	k?
tapasa	tapasa	k1gFnSc1
movement	movement	k1gInSc1
in	in	k?
Buddhism	Buddhism	k1gInSc1
and	and	k?
his	his	k1gNnSc1
special	special	k1gMnSc1
rules	rules	k1gMnSc1
involved	involved	k1gMnSc1
ascetic	ascetice	k1gFnPc2
and	and	k?
austere	auster	k1gInSc5
practices	practices	k1gInSc1
(	(	kIx(
<g/>
forest-dwelling	forest-dwelling	k1gInSc1
<g/>
,	,	kIx,
wearing	wearing	k1gInSc1
only	onla	k1gFnSc2
rags	ragsa	k1gFnPc2
<g/>
,	,	kIx,
etc	etc	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Buddha	Buddha	k1gMnSc1
rejected	rejected	k1gMnSc1
all	all	k?
the	the	k?
proposed	proposed	k1gInSc1
revisions	revisions	k1gInSc1
of	of	k?
Devadatta	Devadatta	k1gFnSc1
<g/>
,	,	kIx,
and	and	k?
it	it	k?
was	was	k?
in	in	k?
this	this	k6eAd1
context	context	k2eAgInSc4d1
that	that	k1gInSc4
he	he	k0
reiterated	reiterated	k1gMnSc1
the	the	k?
tikoiparisuddha	tikoiparisuddha	k1gMnSc1
rule	rula	k1gFnSc3
<g/>
.	.	kIx.
(	(	kIx(
<g/>
On	on	k3xPp3gInSc1
this	this	k1gInSc1
see	see	k?
the	the	k?
author	author	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Western	Western	kA
Buddhism	Buddhism	k1gInSc1
and	and	k?
a	a	k8xC
Theravada	Theravada	k1gFnSc1
heterodoxy	heterodox	k1gInPc4
<g/>
,	,	kIx,
BSQ	BSQ	kA
Tracts	Tracts	k1gInSc4
on	on	k3xPp3gMnSc1
Buddhism	Buddhism	k1gMnSc1
<g/>
'	'	kIx"
<g/>
↑	↑	k?
Buddhism	Buddhism	k1gInSc1
and	and	k?
Eating	Eating	k1gInSc1
Meat	Meat	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Urbandharma	Urbandharma	k1gNnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Life	Lif	k1gInSc2
as	as	k1gNnPc2
a	a	k8xC
Vegetarian	Vegetariana	k1gFnPc2
Tibetan	Tibetan	k1gInSc1
Buddhist	Buddhist	k1gMnSc1
Practitioner	Practitioner	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Serv-online	Serv-onlin	k1gInSc5
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Apparitions	Apparitions	k1gInSc1
of	of	k?
the	the	k?
Self	Self	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Secret	Secret	k1gMnSc1
Autobiographies	Autobiographies	k1gMnSc1
of	of	k?
a	a	k8xC
Tibetan	Tibetan	k1gInSc1
Visionary	Visionara	k1gFnSc2
-	-	kIx~
Google	Google	k1gInSc1
Books	Books	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Books	Books	k1gInSc1
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
google	google	k1gNnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
The	The	k1gMnSc1
Life	Lif	k1gFnSc2
of	of	k?
Shabkar	Shabkar	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Autobiography	Autobiographa	k1gFnSc2
of	of	k?
a	a	k8xC
Tibetan	Tibetan	k1gInSc1
Yogin	Yogin	k2eAgInSc1d1
-	-	kIx~
Google	Google	k1gInSc1
Books	Books	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Books	Books	k1gInSc1
<g/>
.	.	kIx.
<g/>
google	google	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Archivováno	archivován	k2eAgNnSc1d1
5	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
Code	Cod	k1gInPc1
of	of	k?
Canon	Canon	kA
Law	Law	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
vatican	vatican	k1gInSc1
<g/>
.	.	kIx.
<g/>
va	va	k0wR
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Epiphanius	Epiphanius	k1gInSc1
<g/>
,	,	kIx,
Panarion	Panarion	k1gInSc1
<g/>
,	,	kIx,
30.22	30.22	k4
<g/>
.4	.4	k4
<g/>
↑	↑	k?
Isidore	Isidor	k1gMnSc5
Seville	Sevilla	k1gFnSc6
<g/>
,	,	kIx,
Etymologie	etymologie	k1gFnSc1
<g/>
,	,	kIx,
VIII	VIII	kA
<g/>
.	.	kIx.
<g/>
v	v	k7c6
<g/>
.36	.36	k4
<g/>
↑	↑	k?
The	The	k1gMnSc1
Bible	bible	k1gFnSc2
Christian	Christian	k1gMnSc1
Church	Church	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
International	International	k1gMnSc1
Vegetarian	Vegetarian	k1gMnSc1
Union	union	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
History	Histor	k1gInPc1
of	of	k?
Vegetarianism	Vegetarianism	k1gInSc1
–	–	k?
Early	earl	k1gMnPc4
Ideas	Ideasa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Vegetarian	Vegetariana	k1gFnPc2
Society	societa	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
;	;	kIx,
Gregory	Gregor	k1gMnPc4
<g/>
,	,	kIx,
James	James	k1gMnSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
Z	z	k7c2
Victorians	Victoriansa	k1gFnPc2
a	a	k8xC
vegetariány	vegetarián	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
IB	IB	kA
Tauris	Tauris	k1gInSc1
pp	pp	k?
30	#num#	k4
<g/>
-	-	kIx~
<g/>
35	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
William	William	k1gInSc1
Cowherd	Cowherd	k1gInSc1
(	(	kIx(
<g/>
brief	brief	k1gInSc1
information	information	k1gInSc1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oxford	Oxford	k1gInSc1
Dictionary	Dictionara	k1gFnSc2
of	of	k?
National	National	k1gMnSc2
Biography	Biographa	k1gMnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Position	Position	k1gInSc1
Statement	Statement	k1gInSc4
on	on	k3xPp3gMnSc1
Vegetarian	Vegetarian	k1gMnSc1
Diet	dieta	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sdada	Sdada	k1gFnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Archivováno	archivován	k2eAgNnSc1d1
13	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
The	The	k1gMnSc1
Seventh-day	Seventh-daa	k1gFnSc2
Adventist	Adventist	k1gMnSc1
Health	Health	k1gMnSc1
Message	Message	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sdada	Sdada	k1gFnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Living	Living	k1gInSc1
an	an	k?
Orthodox	Orthodox	k1gInSc1
Life	Life	k1gFnSc1
<g/>
:	:	kIx,
Fasting	Fasting	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Orthodoxinfo	Orthodoxinfo	k1gNnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
1997-05-27	1997-05-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
The	The	k1gFnSc1
Great	Great	k1gInSc1
War	War	k1gFnSc1
and	and	k?
the	the	k?
Interwar	Interwar	k1gInSc1
Period	perioda	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
ivu	ivu	k?
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Fast	Fast	k1gInSc1
and	and	k?
Abstinence	abstinence	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
EWTN	EWTN	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Tähtinen	Tähtinen	k1gInSc1
<g/>
,	,	kIx,
Unto	Unto	k1gNnSc1
<g/>
:	:	kIx,
Ahimsa	Ahimsa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Non-Violence	Non-Violence	k1gFnSc1
in	in	k?
Indian	Indiana	k1gFnPc2
Tradition	Tradition	k1gInSc1
<g/>
,	,	kIx,
Londýn	Londýn	k1gInSc1
1976	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
107	#num#	k4
<g/>
-	-	kIx~
<g/>
109	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
The	The	k1gMnSc3
Hindu	hind	k1gMnSc3
:	:	kIx,
Sci	Sci	k1gMnSc3
Tech	Tech	k?
/	/	kIx~
Speaking	Speaking	k1gInSc1
Of	Of	k1gMnSc2
Science	Scienec	k1gMnSc2
:	:	kIx,
Changes	Changes	k1gInSc1
in	in	k?
the	the	k?
Indian	Indiana	k1gFnPc2
menu	menu	k1gNnSc1
over	over	k1gMnSc1
the	the	k?
ages	ages	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hinduonnet	Hinduonnet	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
October	October	k1gInSc1
21	#num#	k4
<g/>
,	,	kIx,
2004	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
The	The	k1gMnSc1
states	states	k1gMnSc1
where	wher	k1gInSc5
cow	cow	k?
slaughter	slaughter	k1gMnSc1
is	is	k?
legal	legal	k1gMnSc1
in	in	k?
India	indium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnPc2
Indian	Indiana	k1gFnPc2
Express	express	k1gInSc1
<g/>
.	.	kIx.
8	#num#	k4
October	October	k1gInSc1
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Vegetarian	Vegetarian	k1gMnSc1
Muslim	muslim	k1gMnSc1
<g/>
:	:	kIx,
Turning	Turning	k1gInSc1
Away	Awaa	k1gFnSc2
From	Froma	k1gFnPc2
a	a	k8xC
Meat-Based	Meat-Based	k1gInSc1
Diet	dieta	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Huffington	Huffington	k1gInSc1
Post	post	k1gInSc4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Muslims	Muslimsa	k1gFnPc2
can	can	k?
<g/>
'	'	kIx"
<g/>
t	t	k?
be	be	k?
Vegetarian	Vegetarian	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
:	:	kIx,
Islam	Islam	k1gInSc1
:	:	kIx,
Dietery	Dieter	k1gInPc1
Law	Law	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ipaki	Ipaki	k1gNnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
lokpriya	lokpriya	k1gFnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lokpriya	Lokpriya	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
IVU	Iva	k1gFnSc4
News	Newsa	k1gFnPc2
–	–	k?
Islam	Islam	k1gInSc1
and	and	k?
Vegetarianism	Vegetarianism	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ivu	Iva	k1gFnSc4
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
"	"	kIx"
<g/>
Vegetarianism	Vegetarianism	k1gInSc1
Good	Gooda	k1gFnPc2
For	forum	k1gNnPc2
The	The	k1gMnSc1
Self	Self	k1gMnSc1
And	Anda	k1gFnPc2
Good	Gooda	k1gFnPc2
For	forum	k1gNnPc2
The	The	k1gMnSc1
Environment	Environment	k1gMnSc1
<g/>
"	"	kIx"
Archivováno	archivovat	k5eAaBmNgNnS
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
The	The	k1gMnPc1
Jain	Jaino	k1gNnPc2
Study	stud	k1gInPc4
Circle	Circle	k1gNnSc4
<g/>
↑	↑	k?
Spiritual	Spiritual	k1gInSc1
Traditions	Traditions	k1gInSc1
and	and	k?
Vegetarianism	Vegetarianism	k1gInSc1
<g/>
"	"	kIx"
at	at	k?
the	the	k?
Vegetarian	Vegetarian	k1gMnSc1
Society	societa	k1gFnSc2
of	of	k?
Colorado	Colorado	k1gNnSc1
website	websit	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archivováno	archivován	k2eAgNnSc4d1
1	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
Matthews	Matthewsa	k1gFnPc2
<g/>
,	,	kIx,
Warren	Warrna	k1gFnPc2
<g/>
:	:	kIx,
World	World	k1gInSc1
Religions	Religionsa	k1gFnPc2
<g/>
,	,	kIx,
4	#num#	k4
<g/>
th	th	k?
edition	edition	k1gInSc1
<g/>
,	,	kIx,
Belmont	Belmont	k1gInSc1
<g/>
:	:	kIx,
Thomson	Thomson	k1gInSc1
<g/>
/	/	kIx~
<g/>
Wadsworth	Wadsworth	k1gInSc1
2005	#num#	k4
<g/>
,	,	kIx,
p.	p.	k?
180	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
534	#num#	k4
<g/>
-	-	kIx~
<g/>
52762	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
↑	↑	k?
Noah	Noah	k1gMnSc1
Lewis	Lewis	k1gFnSc2
<g/>
.	.	kIx.
vegetus	vegetus	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Archivováno	archivován	k2eAgNnSc1d1
2	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
KOOK	KOOK	kA
<g/>
,	,	kIx,
Avraham	Avraham	k1gInSc1
Yitzhak	Yitzhak	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
Vision	vision	k1gInSc1
of	of	k?
Vegetarianism	Vegetarianism	k1gInSc1
and	and	k?
Peace	Peace	k1gFnSc1
<g/>
.	.	kIx.
jewishveg	jewishveg	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
1961	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Judaism	Judaism	k1gInSc1
&	&	k?
Vegetarianism	Vegetarianism	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jewishveg	Jewishveg	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ERRP	ERRP	kA
|	|	kIx~
Expired	Expired	k1gInSc1
Registration	Registration	k1gInSc1
Recovery	Recovera	k1gFnSc2
Policy	Polica	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Europeanvegetarian	Europeanvegetarian	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
"	"	kIx"
<g/>
The	The	k1gMnSc2
Vision	vision	k1gInSc1
of	of	k?
Eden	Eden	k1gInSc1
<g/>
:	:	kIx,
Animal	animal	k1gMnSc1
Welfare	Welfar	k1gMnSc5
and	and	k?
Vegetarianism	Vegetarianismo	k1gNnPc2
<g/>
"	"	kIx"
in	in	k?
"	"	kIx"
<g/>
Jewish	Jewish	k1gMnSc1
Law	Law	k1gMnSc1
and	and	k?
Mysticism	Mysticism	k1gMnSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
Orot	Orot	k2eAgMnSc1d1
2003	#num#	k4
<g/>
↑	↑	k?
Osborne	Osborn	k1gInSc5
<g/>
,	,	kIx,
L	L	kA
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
The	The	k1gFnSc1
Cookbook	Cookbook	k1gInSc1
Rasta	Rasta	k1gFnSc1
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
ed	ed	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mac	Mac	kA
Donald	Donald	k1gMnSc1
<g/>
,	,	kIx,
London	London	k1gMnSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Ital	Ital	k1gMnSc1
Cooking	Cooking	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eat	Eat	k1gMnSc1
Jamaican	Jamaican	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Kebede	Kebed	k1gMnSc5
<g/>
,	,	kIx,
A.	A.	kA
<g/>
,	,	kIx,
&	&	k?
Knotternus	Knotternus	k1gInSc1
<g/>
,	,	kIx,
D.	D.	kA
Beyond	Beyond	k1gInSc1
the	the	k?
pales	pales	k1gInSc1
of	of	k?
babylon	babylon	k1gInSc1
<g/>
:	:	kIx,
the	the	k?
ideational	ideationat	k5eAaImAgMnS,k5eAaPmAgMnS
components	components	k6eAd1
and	and	k?
social	social	k1gMnSc1
psychological	psychologicat	k5eAaPmAgMnS
foundations	foundations	k6eAd1
of	of	k?
rastafari	rastafar	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sociological	Sociological	k1gMnSc1
Perspectives	Perspectives	k1gMnSc1
<g/>
.	.	kIx.
1998	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
41	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
3	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
499	#num#	k4
<g/>
–	–	k?
<g/>
517	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.230	10.230	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
1389561	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Sikhism	Sikhism	k1gInSc1
Religion	religion	k1gInSc1
of	of	k?
the	the	k?
Sikh	sikh	k1gMnSc1
People	People	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sikhs	Sikhs	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
I.J.	I.J.	k1gMnSc1
Singh	Singh	k1gMnSc1
<g/>
,	,	kIx,
Sikhs	Sikhs	k1gInSc1
and	and	k?
Sikhism	Sikhism	k1gInSc1
<g/>
,	,	kIx,
Manohar	Manohar	k1gInSc1
<g/>
,	,	kIx,
Delhi	Delh	k1gFnPc1
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
81	#num#	k4
<g/>
-	-	kIx~
<g/>
7304	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
58	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Throughout	Throughout	k1gMnSc1
Sikh	sikh	k1gMnSc1
history	histor	k1gInPc4
<g/>
,	,	kIx,
there	ther	k1gMnSc5
have	havus	k1gMnSc5
been	beeno	k1gNnPc2
movements	movements	k1gInSc1
or	or	k?
subsects	subsects	k1gInSc1
of	of	k?
Sikhism	Sikhism	k1gInSc1
which	which	k1gMnSc1
have	have	k1gFnPc2
espoused	espoused	k1gMnSc1
vegetarianism	vegetarianism	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
think	think	k6eAd1
there	ther	k1gInSc5
is	is	k?
no	no	k9
basis	basis	k?
for	forum	k1gNnPc2
such	sucho	k1gNnPc2
dogma	dogma	k1gNnSc4
or	or	k?
practice	practice	k1gFnSc2
in	in	k?
Sikhism	Sikhism	k1gMnSc1
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
↑	↑	k?
Surindar	Surindar	k1gMnSc1
Singh	Singh	k1gMnSc1
Kohli	Kohle	k1gFnSc4
<g/>
,	,	kIx,
Guru	guru	k1gMnSc1
Granth	Granth	k1gMnSc1
Sahib	sahib	k1gMnSc1
<g/>
,	,	kIx,
An	An	k1gMnSc1
Analytical	Analytical	k1gFnSc2
Study	stud	k1gInPc4
<g/>
,	,	kIx,
Singh	Singh	k1gInSc4
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Amritsar	Amritsar	k1gInSc1
ISBN	ISBN	kA
81	#num#	k4
<g/>
-	-	kIx~
<g/>
7205	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
60	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
:	:	kIx,
""	""	k?
<g/>
The	The	k1gMnSc1
ideas	ideas	k1gMnSc1
of	of	k?
devotion	devotion	k1gInSc1
and	and	k?
service	service	k1gFnSc2
in	in	k?
Vaishnavism	Vaishnavism	k1gMnSc1
have	hav	k1gFnSc2
been	been	k1gMnSc1
accepted	accepted	k1gMnSc1
by	by	kYmCp3nS
Adi	Adi	k1gMnSc3
Granth	Granth	k1gInSc1
<g/>
,	,	kIx,
but	but	k?
the	the	k?
insistence	insistence	k1gFnSc1
of	of	k?
Vaishnavas	Vaishnavas	k1gInSc4
on	on	k3xPp3gMnSc1
vegetarian	vegetarian	k1gMnSc1
diet	dieta	k1gFnPc2
has	hasit	k5eAaImRp2nS
been	been	k1gMnSc1
rejected	rejected	k1gMnSc1
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
↑	↑	k?
Gopal	Gopal	k1gMnSc1
Singh	Singh	k1gMnSc1
<g/>
,	,	kIx,
History	Histor	k1gMnPc4
of	of	k?
the	the	k?
Sikh	sikh	k1gMnSc1
People	People	k1gMnSc1
<g/>
,	,	kIx,
World	World	k1gMnSc1
Sikh	sikh	k1gMnSc1
Univ	Univ	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Press	Press	k1gInSc1
<g/>
,	,	kIx,
Delhi	Delhi	k1gNnSc1
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
81	#num#	k4
<g/>
-	-	kIx~
<g/>
7023	#num#	k4
<g/>
-	-	kIx~
<g/>
139	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Nowadays	Nowadays	k1gInSc1
in	in	k?
the	the	k?
Community	Communita	k1gFnSc2
Kitchen	Kitchen	k2eAgMnSc1d1
attached	attached	k1gMnSc1
to	ten	k3xDgNnSc4
the	the	k?
Sikh	sikh	k1gMnSc1
temples	temples	k1gMnSc1
<g/>
,	,	kIx,
and	and	k?
called	called	k1gMnSc1
the	the	k?
Guru	guru	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Kitchen	Kitchna	k1gFnPc2
(	(	kIx(
<g/>
or	or	k?
Guru-ka-langar	Guru-ka-langar	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
meat	meat	k2eAgInSc1d1
dishes	dishes	k1gInSc1
are	ar	k1gInSc5
not	nota	k1gFnPc2
served	served	k1gInSc4
at	at	k?
all	all	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maybe	Mayb	k1gInSc5
it	it	k?
is	is	k?
on	on	k3xPp3gMnSc1
account	account	k1gMnSc1
of	of	k?
its	its	k?
being	being	k1gInSc1
<g/>
,	,	kIx,
perhaps	perhaps	k1gInSc1
<g/>
,	,	kIx,
expensive	expensivat	k5eAaPmIp3nS
or	or	k?
not	nota	k1gFnPc2
easy	easy	k1gInPc1
to	ten	k3xDgNnSc1
keep	keep	k1gInSc1
for	forum	k1gNnPc2
long	longa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Or	Or	k1gFnPc2
perhaps	perhaps	k6eAd1
the	the	k?
Vaishnava	Vaishnava	k1gFnSc1
tradition	tradition	k1gInSc1
is	is	k?
too	too	k?
strong	strong	k1gInSc1
to	ten	k3xDgNnSc1
be	be	k?
shaken	shaken	k1gInSc1
off	off	k?
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
↑	↑	k?
Randip	Randip	k1gMnSc1
Singh	Singh	k1gMnSc1
<g/>
,	,	kIx,
Fools	Fools	k1gInSc1
Who	Who	k1gFnSc2
Wrangle	Wrangle	k1gFnSc2
Over	Over	k1gMnSc1
Flesh	Flesh	k1gMnSc1
<g/>
,	,	kIx,
Sikh	sikh	k1gMnSc1
filozofie	filozofie	k1gFnSc2
síť	síť	k1gFnSc1
<g/>
,	,	kIx,
0	#num#	k4
<g/>
7.12	7.12	k4
<g/>
.2006	.2006	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Získaný	získaný	k2eAgInSc4d1
15	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2010	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Sikh	sikh	k1gMnSc1
Reht	Reht	k1gMnSc1
Maryada	Maryada	k1gFnSc1
<g/>
,	,	kIx,
The	The	k1gFnSc1
Definition	Definition	k1gInSc1
of	of	k?
Sikh	sikh	k1gMnSc1
<g/>
,	,	kIx,
Sikh	sikh	k1gMnSc1
Conduct	Conduct	k1gMnSc1
&	&	k?
Conventions	Conventions	k1gInSc1
<g/>
,	,	kIx,
Sikh	sikh	k1gMnSc1
Religion	religion	k1gInSc1
Living	Living	k1gInSc4
<g/>
,	,	kIx,
India	indium	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
sgpc	sgpc	k1gFnSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
SCARBOROUGH	SCARBOROUGH	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
;	;	kIx,
APPLEBY	APPLEBY	kA
<g/>
,	,	kIx,
Paul	Paul	k1gMnSc1
N.	N.	kA
<g/>
;	;	kIx,
MIZDRAK	MIZDRAK	kA
<g/>
,	,	kIx,
Anja	Anjum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dietary	Dietara	k1gFnPc4
greenhouse	greenhouse	k1gFnSc2
gas	gas	k?
emissions	emissions	k1gInSc1
of	of	k?
meat-eaters	meat-eaters	k1gInSc1
<g/>
,	,	kIx,
fish-eaters	fish-eaters	k1gInSc1
<g/>
,	,	kIx,
vegetarians	vegetarians	k1gInSc1
and	and	k?
vegans	vegans	k1gInSc1
in	in	k?
the	the	k?
UK	UK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Climatic	Climatice	k1gFnPc2
Change	change	k1gFnSc2
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
125	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
179	#num#	k4
<g/>
–	–	k?
<g/>
192	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
165	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
10584	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
14	#num#	k4
<g/>
-	-	kIx~
<g/>
1169	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
25834298	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Livestock	Livestock	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Long	Long	k1gMnSc1
Shadow	Shadow	k1gMnSc1
–	–	k?
Environmental	Environmental	k1gMnSc1
issues	issues	k1gMnSc1
and	and	k?
options	options	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fao	Fao	k1gFnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
EPA	EPA	kA
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Inventory	Inventor	k1gInPc7
of	of	k?
U.	U.	kA
<g/>
S.	S.	kA
greenhouse	greenhouse	k1gFnSc1
gas	gas	k?
emissions	emissions	k1gInSc1
and	and	k?
sinks	sinks	k1gInSc1
<g/>
:	:	kIx,
1990	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
United	United	k1gMnSc1
States	States	k1gMnSc1
Environmental	Environmental	k1gMnSc1
Protection	Protection	k1gInSc4
Agency	Agenca	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
EPA	EPA	kA
430	#num#	k4
<g/>
-R-	-R-	k?
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
459	#num#	k4
pp	pp	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Special	Special	k1gInSc1
Report	report	k1gInSc1
on	on	k3xPp3gMnSc1
climate	climat	k1gInSc5
change	change	k1gFnSc1
<g/>
,	,	kIx,
desertification	desertification	k1gInSc1
<g/>
,	,	kIx,
land	land	k1gInSc1
degradation	degradation	k1gInSc1
<g/>
,	,	kIx,
sustainable	sustainable	k6eAd1
land	land	k6eAd1
management	management	k1gInSc1
<g/>
,	,	kIx,
food	food	k1gInSc1
security	securita	k1gFnSc2
<g/>
,	,	kIx,
and	and	k?
greenhouse	greenhouse	k1gFnSc1
gas	gas	k?
fluxes	fluxes	k1gInSc1
in	in	k?
terrestrial	terrestrial	k1gInSc1
ecosystems	ecosystems	k1gInSc1
(	(	kIx(
<g/>
SRCCL	SRCCL	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
IPCC	IPCC	kA
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Supersizing	Supersizing	k1gInSc1
Climate	Climat	k1gInSc5
Change	change	k1gFnSc1
<g/>
:	:	kIx,
U.	U.	kA
<g/>
N.	N.	kA
Says	Says	k1gInSc4
Meat	Meat	k2eAgInSc1d1
Production	Production	k1gInSc1
Destroys	Destroysa	k1gFnPc2
Land	Land	k1gMnSc1
&	&	k?
Diminishes	Diminishes	k1gMnSc1
Key	Key	k1gMnSc1
Water	Water	k1gMnSc1
Sources	Sources	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Democracy	Democrac	k2eAgFnPc4d1
Now	Now	k1gFnPc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
FLAVELLE	FLAVELLE	kA
<g/>
,	,	kIx,
Christopher	Christophra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Climate	Climat	k1gInSc5
Change	change	k1gFnPc1
Threatens	Threatens	k1gInSc1
the	the	k?
World	World	k1gInSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Food	Food	k1gMnSc1
Supply	Supply	k1gMnSc1
<g/>
,	,	kIx,
United	United	k1gMnSc1
Nations	Nationsa	k1gFnPc2
Warns	Warnsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc2
York	York	k1gInSc1
Times	Times	k1gMnSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
362	#num#	k4
<g/>
-	-	kIx~
<g/>
4331	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
United	United	k1gInSc1
Nations	Nations	k1gInSc1
predicts	predicts	k1gInSc4
hungry	hungra	k1gFnSc2
future	futur	k1gMnSc5
<g/>
,	,	kIx,
due	due	k?
to	ten	k3xDgNnSc1
global	globat	k5eAaImAgInS
warming	warming	k1gInSc1
<g/>
,	,	kIx,
that	that	k1gInSc1
can	can	k?
be	be	k?
avoided	avoided	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
PBS	PBS	kA
NewsHour	NewsHoura	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-08-08	2019-08-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
OLSSON	OLSSON	kA
<g/>
,	,	kIx,
Anna	Anna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Comment	Comment	k1gMnSc1
<g/>
:	:	kIx,
Lab-grown	Lab-grown	k1gMnSc1
meat	meat	k1gMnSc1
could	could	k1gMnSc1
ease	easat	k5eAaPmIp3nS
food	food	k6eAd1
shortage	shortage	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
Scientist	Scientist	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
November	November	k1gInSc1
17	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Could	Could	k1gInSc1
vegetarians	vegetarians	k1gInSc1
eat	eat	k?
a	a	k8xC
'	'	kIx"
<g/>
test	test	k1gInSc1
tube	tubus	k1gInSc5
<g/>
'	'	kIx"
burger	burgra	k1gFnPc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
-	-	kIx~
BBC	BBC	kA
News	News	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bbc	Bbc	k1gFnSc1
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Why	Why	k1gFnSc1
eating	eating	k1gInSc1
less	less	k6eAd1
meat	meat	k2eAgInSc1d1
could	could	k1gInSc1
cut	cut	k?
global	globat	k5eAaImAgInS
warming	warming	k1gInSc1
|	|	kIx~
Environment	Environment	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Guardian	Guardian	k1gInSc1
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
<g/>
,	,	kIx,
2007-11-10	2007-11-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
MASON	mason	k1gMnSc1
<g/>
,	,	kIx,
Chris	Chris	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Europe	Europ	k1gInSc5
|	|	kIx~
Belgian	Belgian	k1gInSc1
city	city	k1gNnSc1
plans	plans	k1gInSc1
'	'	kIx"
<g/>
veggie	veggie	k1gFnSc1
<g/>
'	'	kIx"
days	days	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
News	News	k1gInSc1
<g/>
.	.	kIx.
<g/>
bbc	bbc	k?
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
This	This	k1gInSc1
meat-loving	meat-loving	k1gInSc1
city	city	k1gFnSc1
is	is	k?
now	now	k?
the	the	k?
unlikely	unlikela	k1gFnSc2
vegetarian	vegetarian	k1gMnSc1
capital	capital	k1gMnSc1
of	of	k?
Europe	Europ	k1gInSc5
<g/>
.	.	kIx.
mic	mic	k?
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ŠMÍDOVÁ	Šmídová	k1gFnSc1
<g/>
,	,	kIx,
Jana	Jana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lednička	lednička	k1gFnSc1
se	se	k3xPyFc4
stává	stávat	k5eAaImIp3nS
bitevním	bitevní	k2eAgNnSc7d1
polem	pole	k1gNnSc7
ve	v	k7c6
válce	válka	k1gFnSc6
o	o	k7c4
záchranu	záchrana	k1gFnSc4
planety	planeta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvítězí	zvítězit	k5eAaPmIp3nP
vegani	vegan	k1gMnPc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svět	svět	k1gInSc1
ve	v	k7c6
20	#num#	k4
minutách	minuta	k1gFnPc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
2017-06-15	2017-06-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Killing	Killing	k1gInSc1
for	forum	k1gNnPc2
a	a	k8xC
Living	Living	k1gInSc1
<g/>
:	:	kIx,
How	How	k1gMnSc1
the	the	k?
Meat	Meat	k1gMnSc1
Industry	Industra	k1gFnSc2
Exploits	Exploits	k1gInSc1
Workers	Workers	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Worker	Worker	k1gMnSc1
Health	Health	k1gMnSc1
and	and	k?
Safety	Safeta	k1gFnSc2
in	in	k?
the	the	k?
Meat	Meat	k1gMnSc1
and	and	k?
Poultry	Poultr	k1gInPc1
Industry	Industra	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrw	Hrw	k1gFnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Food	Food	k1gInSc1
Safety	Safeta	k1gFnSc2
<g/>
,	,	kIx,
the	the	k?
Slaughterhouse	Slaughterhous	k1gMnSc5
<g/>
,	,	kIx,
and	and	k?
Rights	Rights	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ncrlc	Ncrlc	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
March	March	k1gInSc1
30	#num#	k4
<g/>
,	,	kIx,
2004	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
December	December	k1gMnSc1
23	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Positive	positiv	k1gInSc5
Safety	Safet	k1gInPc1
Culture	Cultur	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
key	key	k?
to	ten	k3xDgNnSc4
a	a	k8xC
safer	safer	k1gMnSc1
meat	meat	k1gInSc4
industry	industra	k1gFnSc2
Archivováno	archivovat	k5eAaBmNgNnS
12	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
<g/>
,	,	kIx,
literature	literatur	k1gMnSc5
review	review	k?
July	Jula	k1gFnPc4
2000	#num#	k4
<g/>
,	,	kIx,
safework	safework	k1gInSc1
<g/>
.	.	kIx.
<g/>
sa	sa	k?
<g/>
.	.	kIx.
<g/>
gov	gov	k?
<g/>
.	.	kIx.
<g/>
au	au	k0
<g/>
↑	↑	k?
Sectoral	Sectoral	k1gFnSc1
Policies	Policies	k1gMnSc1
Department	department	k1gInSc1
(	(	kIx(
<g/>
SECTOR	SECTOR	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ilo	Ilo	k1gFnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Archivováno	archivován	k2eAgNnSc1d1
3	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
World	World	k1gMnSc1
Development	Development	k1gMnSc1
Report	report	k1gInSc4
2008	#num#	k4
<g/>
:	:	kIx,
Agriculture	Agricultur	k1gMnSc5
for	forum	k1gNnPc2
Development	Development	k1gInSc1
<g/>
,	,	kIx,
Published	Published	k1gInSc1
by	by	kYmCp3nS
World	World	k1gInSc4
Bank	banka	k1gFnPc2
Publications	Publicationsa	k1gFnPc2
str	str	kA
<g/>
.	.	kIx.
207	#num#	k4
<g/>
↑	↑	k?
Archived	Archived	k1gInSc1
copy	cop	k1gInPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
May	May	k1gMnSc1
17	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
University	universita	k1gFnSc2
of	of	k?
Southampton	Southampton	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soton	Soton	k1gNnSc1
<g/>
.	.	kIx.
<g/>
ac	ac	k?
<g/>
.	.	kIx.
<g/>
uk	uk	k?
<g/>
,	,	kIx,
2006-12-15	2006-12-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
DEC	DEC	kA
15	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
High	High	k1gInSc1
IQ	iq	kA
link	link	k6eAd1
to	ten	k3xDgNnSc4
being	being	k1gMnSc1
vegetarian	vegetarian	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
News	Newsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
September	September	k1gInSc1
12	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HODSON	HODSON	kA
<g/>
,	,	kIx,
Gordon	Gordon	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prejudice	prejudice	k1gFnSc1
Against	Against	k1gFnSc1
"	"	kIx"
<g/>
Group	Group	k1gInSc1
X	X	kA
<g/>
"	"	kIx"
(	(	kIx(
<g/>
Asexuals	Asexuals	k1gInSc1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Psychology	psycholog	k1gMnPc7
Today	Todaa	k1gFnSc2
<g/>
,	,	kIx,
September	September	k1gInSc1
1	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Joanne	Joann	k1gInSc5
McAllister	McAllistrum	k1gNnPc2
Smart	Smarta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
gender	gender	k1gMnSc1
gap	gap	k?
<g/>
:	:	kIx,
if	if	k?
you	you	k?
<g/>
'	'	kIx"
<g/>
re	re	k9
a	a	k8xC
vegetarian	vegetarian	k1gMnSc1
<g/>
,	,	kIx,
odds	odds	k6eAd1
are	ar	k1gInSc5
you	you	k?
<g/>
'	'	kIx"
<g/>
re	re	k9
a	a	k8xC
woman	woman	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Why	Why	k1gFnPc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vegetarian	Vegetarian	k1gMnSc1
Times	Times	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Active	Actiev	k1gFnSc2
Interest	Interest	k1gFnSc1
Media	medium	k1gNnPc1
<g/>
,	,	kIx,
Inc	Inc	k1gFnPc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
February	Februar	k1gInPc1
1995	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
210	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
74	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
12	#num#	k4
January	Januara	k1gFnSc2
2016	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
164	#num#	k4
<g/>
-	-	kIx~
<g/>
8497	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
'	'	kIx"
<g/>
More	mor	k1gInSc5
girl	girl	k1gFnSc3
babies	babies	k1gInSc1
<g/>
'	'	kIx"
for	forum	k1gNnPc2
vegetarians	vegetariansa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
News	Newsa	k1gFnPc2
<g/>
,	,	kIx,
August	August	k1gMnSc1
7	#num#	k4
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
August	August	k1gMnSc1
9	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
NIEMAN	NIEMAN	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
C.	C.	kA
Physical	Physical	k1gMnSc1
fitness	fitness	k6eAd1
and	and	k?
vegetarian	vegetarian	k1gInSc1
diets	diets	k1gInSc1
<g/>
:	:	kIx,
is	is	k?
there	ther	k1gInSc5
a	a	k8xC
relation	relation	k1gInSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
The	The	k1gMnSc1
American	American	k1gMnSc1
Journal	Journal	k1gMnSc1
of	of	k?
Clinical	Clinical	k1gFnSc1
Nutrition	Nutrition	k1gInSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Překlad	překlad	k1gInSc1
dostupný	dostupný	k2eAgInSc1d1
na	na	k7c6
http://natura.baf.cz/natura/tex/20050301.pdf.	http://natura.baf.cz/natura/tex/20050301.pdf.	k4
↑	↑	k?
I	i	k8xC
špičkoví	špičkový	k2eAgMnPc1d1
sportovci	sportovec	k1gMnPc1
žijí	žít	k5eAaImIp3nP
zdravě	zdravě	k6eAd1
-	-	kIx~
Vitariánství	Vitariánství	k1gNnSc1
<g/>
,	,	kIx,
živá	živý	k2eAgFnSc1d1
strava	strava	k1gFnSc1
<g/>
,	,	kIx,
RAW	RAW	kA
<g/>
.	.	kIx.
www.vitarianstvi.cz	www.vitarianstvi.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ALEX	Alex	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
IVU	Iva	k1gFnSc4
-	-	kIx~
IVU	Iva	k1gFnSc4
-	-	kIx~
International	International	k1gFnSc1
Vegetarian	Vegetarian	k1gMnSc1
Union	union	k1gInSc1
<g/>
.	.	kIx.
www.ivu.org	www.ivu.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
European	European	k1gInSc1
Vegetarian	Vegetarian	k1gInSc1
Union	union	k1gInSc4
-	-	kIx~
EVU	Eva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
European	European	k1gMnSc1
Vegetarian	Vegetarian	k1gMnSc1
Union	union	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Members	Members	k1gInSc1
-	-	kIx~
European	European	k1gInSc1
Vegetarian	Vegetarian	k1gMnSc1
Union	union	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
European	European	k1gMnSc1
Vegetarian	Vegetarian	k1gMnSc1
Union	union	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ČSVV	ČSVV	kA
-	-	kIx~
ÚVOD	úvod	k1gInSc1
<g/>
.	.	kIx.
www.csvv.cz	www.csvv.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Svoboda	svoboda	k1gFnSc1
zvířat	zvíře	k1gNnPc2
|	|	kIx~
organizace	organizace	k1gFnSc1
na	na	k7c4
ochranu	ochrana	k1gFnSc4
zvířat	zvíře	k1gNnPc2
<g/>
.	.	kIx.
www.svobodazvirat.cz	www.svobodazvirat.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
People	People	k1gFnSc1
for	forum	k1gNnPc2
the	the	k?
Ethical	Ethical	k1gMnSc1
Treatment	Treatment	k1gMnSc1
of	of	k?
Animals	Animals	k1gInSc1
(	(	kIx(
<g/>
PETA	PETA	kA
<g/>
)	)	kIx)
<g/>
:	:	kIx,
The	The	k1gMnSc1
Largest	Largest	k1gMnSc1
Animal	animal	k1gMnSc1
Rights	Rightsa	k1gFnPc2
Organization	Organization	k1gInSc4
in	in	k?
the	the	k?
World	World	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
PETA	PETA	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Vegetarian	Vegetarian	k1gInSc1
Society	societa	k1gFnSc2
Home	Hom	k1gFnSc2
<g/>
.	.	kIx.
www.vegsoc.org	www.vegsoc.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
VEBU	Veba	k1gFnSc4
-	-	kIx~
Die	Die	k1gMnSc1
Zukunft	Zukunft	k1gMnSc1
isst	isst	k1gMnSc1
pflanzlich	pflanzlich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
VEBU	Veba	k1gFnSc4
–	–	k?
Vegetarierbund	Vegetarierbund	k1gInSc1
Deutschland	Deutschland	k1gInSc1
e.V.	e.V.	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
E.V.	E.V.	k1gFnSc1
<g/>
,	,	kIx,
Vegetarische	Vegetarische	k1gFnSc1
Initiative	Initiativ	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
VEGETARISCHE	VEGETARISCHE	kA
INITIATIVE	INITIATIVE	kA
e.V.	e.V.	k?
-	-	kIx~
gesund	gesund	k1gMnSc1
essen	essna	k1gFnPc2
leicht	leicht	k1gMnSc1
gemacht	gemacht	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
www.vegetarische-initiative.de	www.vegetarische-initiative.de	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
vegetarisch	vegetarisch	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
.	.	kIx.
www.vegetarisch.org	www.vegetarisch.org	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Strona	Stron	k1gMnSc4
główna	główn	k1gMnSc4
<g/>
.	.	kIx.
www.viva.org.pl	www.viva.org.pnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Weganizm	Weganizm	k1gMnSc1
<g/>
,	,	kIx,
prawa	prawa	k1gMnSc1
zwierząt	zwierząt	k1gMnSc1
-	-	kIx~
Etyka	Etyka	k1gMnSc1
i	i	k8xC
zwierzęta	zwierzęta	k1gMnSc1
-	-	kIx~
Stowarzyszenie	Stowarzyszenie	k1gFnSc1
Empatia	Empatia	k1gFnSc1
<g/>
.	.	kIx.
empatia	empatia	k1gFnSc1
<g/>
.	.	kIx.
<g/>
pl	pl	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
polsky	polsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Stowarzyszenie	Stowarzyszenie	k1gFnSc1
Empatia	Empatia	k1gFnSc1
<g/>
..	..	k?
<g/>
↑	↑	k?
Otwarte	Otwart	k1gMnSc5
Klatki	Klatk	k1gMnSc5
-	-	kIx~
Twórz	Twórz	k1gInSc1
z	z	k7c2
nami	nam	k1gFnSc2
świat	świat	k5eAaBmF,k5eAaImF,k5eAaPmF
wolny	woln	k1gInPc4
od	od	k7c2
cierpienia	cierpienium	k1gNnSc2
zwierząt	zwierząta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Otwarte	Otwart	k1gMnSc5
Klatki	Klatk	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
polsky	polsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Světový	světový	k2eAgInSc4d1
den	den	k1gInSc4
vegetariánství	vegetariánství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soucitně	soucitně	k6eAd1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
1.11	1.11	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Světový	světový	k2eAgInSc4d1
den	den	k1gInSc4
veganství	veganství	k1gNnSc2
|	|	kIx~
Vegan	Vegan	k1gMnSc1
Fighter	fighter	k1gMnSc1
<g/>
.	.	kIx.
www.vegan-fighter.com	www.vegan-fighter.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Veggie	Veggie	k1gFnSc1
Pride	Prid	k1gInSc5
<g/>
↑	↑	k?
Veggie	Veggie	k1gFnPc1
Parade	Parad	k1gInSc5
<g/>
.	.	kIx.
veggie-parade	veggie-parad	k1gInSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
WWW.PEVAT.COM	WWW.PEVAT.COM	k1gFnPc2
<g/>
,	,	kIx,
vytvořili	vytvořit	k5eAaPmAgMnP
v.	v.	k?
Svoboda	svoboda	k1gFnSc1
zvířat	zvíře	k1gNnPc2
na	na	k7c4
Veggie	Veggie	k1gFnPc4
Parade	Parad	k1gInSc5
v	v	k7c6
Praze	Praha	k1gFnSc6
|	|	kIx~
Svoboda	svoboda	k1gFnSc1
zvířat	zvíře	k1gNnPc2
<g/>
.	.	kIx.
www.svobodazvirat.cz	www.svobodazvirat.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
HANDMADE	HANDMADE	kA
<g/>
,	,	kIx,
html	html	k1gMnSc1
-	-	kIx~
Oldyk	Oldyk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vegetarian	Vegetarian	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
-	-	kIx~
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
www.vegetarian.cz	www.vegetarian.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
SHPRINTZEN	SHPRINTZEN	kA
<g/>
,	,	kIx,
Adam	Adam	k1gMnSc1
D.	D.	kA
The	The	k1gMnSc1
Vegetarian	Vegetarian	k1gMnSc1
Crusade	Crusad	k1gInSc5
<g/>
:	:	kIx,
The	The	k1gFnSc3
Rise	Rise	k1gNnSc2
of	of	k?
an	an	k?
American	American	k1gInSc1
Reform	Reform	k1gInSc1
Movement	Movement	k1gInSc1
<g/>
,	,	kIx,
1817	#num#	k4
<g/>
-	-	kIx~
<g/>
1921	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
The	The	k1gFnSc1
University	universita	k1gFnSc2
of	of	k?
North	Northa	k1gFnPc2
Carolina	Carolina	k1gFnSc1
Press	Press	k1gInSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
4696	#num#	k4
<g/>
-	-	kIx~
<g/>
2652	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
MEDŘICKÁ	MEDŘICKÁ	kA
<g/>
,	,	kIx,
Jitka	Jitka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Psychologické	psychologický	k2eAgInPc1d1
a	a	k8xC
sociální	sociální	k2eAgInPc1d1
aspekty	aspekt	k1gInPc1
vegetariánství	vegetariánství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2008	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Diplomová	diplomový	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Filosofická	filosofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
v	v	k7c6
Praze	Praha	k1gFnSc6
Katedra	katedra	k1gFnSc1
psychologie	psychologie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedoucí	vedoucí	k1gFnSc1
práce	práce	k1gFnSc1
Tamara	Tamara	k1gFnSc1
Hrachovinová	hrachovinový	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
čeština	čeština	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ZEZULKA	ZEZULKA	k?
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přednášky	přednáška	k1gFnSc2
I.	I.	kA
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Vegetářství	vegetářství	k1gNnSc2
<g/>
,	,	kIx,
s.	s.	k?
5	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
čeština	čeština	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Veganství	Veganství	k1gNnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
vegetariánství	vegetariánství	k1gNnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc7
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
vegetariánství	vegetariánství	k1gNnSc1
</s>
<s>
Vegetarian	Vegetarian	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.vegetarian.cz	www.vegetarian.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Česká	český	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
pro	pro	k7c4
výživu	výživa	k1gFnSc4
a	a	k8xC
vegetariánství	vegetariánství	k1gNnSc4
(	(	kIx(
<g/>
ČSVV	ČSVV	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
www.csvv.cz	www.csvv.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
BRÖMS	BRÖMS	kA
<g/>
,	,	kIx,
Elias	Elias	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
odpovídat	odpovídat	k5eAaImF
masožravcům	masožravec	k1gMnPc3
<g/>
.	.	kIx.
www.burfi.net	www.burfi.net	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
The	The	k?
Vegetarian	Vegetarian	k1gInSc4
Resource	Resourka	k1gFnSc3
Group	Group	k1gInSc1
(	(	kIx(
<g/>
VRG	VRG	kA
<g/>
)	)	kIx)
(	(	kIx(
<g/>
Vegetariánská	vegetariánský	k2eAgFnSc1d1
výzkumná	výzkumný	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
www.vrg.org	www.vrg.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Plně	plně	k6eAd1
bezmasá	bezmasý	k2eAgFnSc1d1
strava	strava	k1gFnSc1
vegetariánství	vegetariánství	k1gNnSc2
</s>
<s>
laktovegetariánství	laktovegetariánství	k1gNnSc1
•	•	k?
laktoovovegetariánství	laktoovovegetariánství	k1gNnSc1
•	•	k?
ovovegetariánství	ovovegetariánství	k1gNnPc2
veganství	veganství	k1gNnPc2
</s>
<s>
vitariánství	vitariánství	k1gNnSc1
•	•	k?
frutariánství	frutariánství	k1gNnSc1
informace	informace	k1gFnSc2
</s>
<s>
historie	historie	k1gFnSc1
vegetariánství	vegetariánství	k1gNnSc2
•	•	k?
vegetariánství	vegetariánství	k1gNnSc2
podle	podle	k7c2
zemí	zem	k1gFnPc2
veganské	veganský	k2eAgFnSc2d1
náhražky	náhražka	k1gFnSc2
</s>
<s>
seitan	seitan	k1gInSc1
•	•	k?
sojové	sojový	k2eAgNnSc1d1
mléko	mléko	k1gNnSc1
•	•	k?
tempeh	tempeh	k1gMnSc1
•	•	k?
tofu	tofu	k1gNnSc2
částečně	částečně	k6eAd1
bezmasá	bezmasý	k2eAgFnSc1d1
strava	strava	k1gFnSc1
(	(	kIx(
<g/>
makrobiotika	makrobiotika	k1gFnSc1
<g/>
,	,	kIx,
pescovegetariánství	pescovegetariánství	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
práva	právo	k1gNnSc2
zvířat	zvíře	k1gNnPc2
•	•	k?
potrava	potrava	k1gFnSc1
•	•	k?
lidská	lidský	k2eAgFnSc1d1
výživa	výživa	k1gFnSc1
</s>
<s>
Potravové	potravový	k2eAgFnPc1d1
strategie	strategie	k1gFnPc1
a	a	k8xC
specializace	specializace	k1gFnSc1
rostlinná	rostlinný	k2eAgFnSc1d1
<g/>
:	:	kIx,
herbivor	herbivor	k1gMnSc1
(	(	kIx(
<g/>
býložravec	býložravec	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
folivor	folivor	k1gInSc1
(	(	kIx(
<g/>
listy	list	k1gInPc1
<g/>
)	)	kIx)
•	•	k?
frugivor	frugivor	k1gInSc1
(	(	kIx(
<g/>
plody	plod	k1gInPc1
<g/>
)	)	kIx)
•	•	k?
graminivor	graminivor	k1gInSc1
(	(	kIx(
<g/>
tráva	tráva	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
granivor	granivora	k1gFnPc2
(	(	kIx(
<g/>
semena	semeno	k1gNnPc1
<g/>
)	)	kIx)
•	•	k?
nektarivor	nektarivor	k1gInSc1
(	(	kIx(
<g/>
nektar	nektar	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
palynivor-melitofág	palynivor-melitofág	k1gInSc1
(	(	kIx(
<g/>
pyl	pyl	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
xylovor	xylovor	k1gInSc1
(	(	kIx(
<g/>
dřevo	dřevo	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
fungivor	fungivor	k1gInSc1
(	(	kIx(
<g/>
houby	houba	k1gFnPc1
<g/>
)	)	kIx)
živočišná	živočišný	k2eAgFnSc1d1
<g/>
:	:	kIx,
karnivor	karnivor	k1gMnSc1
(	(	kIx(
<g/>
masožravec	masožravec	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
hematovor	hematovor	k1gMnSc1
<g/>
,	,	kIx,
sangvivor	sangvivor	k1gMnSc1
(	(	kIx(
<g/>
krev	krev	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
insektivor	insektivor	k1gMnSc1
(	(	kIx(
<g/>
hmyzožravec	hmyzožravec	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
myrmekovor	myrmekovor	k1gInSc1
(	(	kIx(
<g/>
mravenci	mravenec	k1gMnPc1
<g/>
)	)	kIx)
•	•	k?
piscivor	piscivor	k1gInSc1
(	(	kIx(
<g/>
ryby	ryba	k1gFnPc1
<g/>
)	)	kIx)
•	•	k?
lepidovor	lepidovor	k1gInSc1
(	(	kIx(
<g/>
šupiny	šupina	k1gFnPc1
<g/>
)	)	kIx)
•	•	k?
ofiovor	ofiovor	k1gInSc1
(	(	kIx(
<g/>
hadi	had	k1gMnPc1
<g/>
)	)	kIx)
•	•	k?
moluskivor	moluskivor	k1gInSc1
(	(	kIx(
<g/>
měkkýši	měkkýš	k1gMnPc1
<g/>
)	)	kIx)
•	•	k?
durovor	durovor	k1gInSc1
(	(	kIx(
<g/>
skořápkatí	skořápkatý	k2eAgMnPc1d1
měkkýši	měkkýš	k1gMnPc1
<g/>
,	,	kIx,
korýši	korýš	k1gMnPc1
a	a	k8xC
korály	korál	k1gInPc7
<g/>
)	)	kIx)
•	•	k?
spongivor	spongivora	k1gFnPc2
(	(	kIx(
<g/>
houbovci	houbovec	k1gInPc7
<g/>
)	)	kIx)
•	•	k?
mukovor	mukovor	k1gInSc1
(	(	kIx(
<g/>
hlen	hlen	k1gInSc1
<g/>
,	,	kIx,
paraziticky	paraziticky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
kanibalistické	kanibalistický	k2eAgNnSc1d1
</s>
<s>
autokanibalismus	autokanibalismus	k1gInSc1
(	(	kIx(
<g/>
sebepožírání	sebepožírání	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
sexuální	sexuální	k2eAgInSc1d1
kanibalismus	kanibalismus	k1gInSc1
•	•	k?
oofágie	oofágie	k1gFnPc1
(	(	kIx(
<g/>
konkurenční	konkurenční	k2eAgNnPc1d1
embrya	embryo	k1gNnPc1
<g/>
)	)	kIx)
•	•	k?
ovofágie	ovofágie	k1gFnSc1
(	(	kIx(
<g/>
konkurenční	konkurenční	k2eAgInSc1d1
plod	plod	k1gInSc1
<g/>
,	,	kIx,
fétus	fétus	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
pedofágie	pedofágie	k1gFnSc1
<g/>
/	/	kIx~
<g/>
infanticida	infanticida	k1gFnSc1
(	(	kIx(
<g/>
mláďata	mládě	k1gNnPc1
vlastního	vlastní	k2eAgInSc2d1
druhu	druh	k1gInSc2
<g/>
:	:	kIx,
ne	ne	k9
<g/>
/	/	kIx~
<g/>
příbuzná	příbuzný	k2eAgNnPc4d1
<g/>
)	)	kIx)
</s>
<s>
libovolná	libovolný	k2eAgFnSc1d1
<g/>
:	:	kIx,
omnivor	omnivor	k1gMnSc1
(	(	kIx(
<g/>
všežravec	všežravec	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
z	z	k7c2
principu	princip	k1gInSc2
bez	bez	k7c2
specializací	specializace	k1gFnPc2
pozůstatky	pozůstatek	k1gInPc1
<g/>
:	:	kIx,
saprofág	saprofág	k1gMnSc1
(	(	kIx(
<g/>
rozkladač	rozkladač	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
saprovor	saprovor	k1gMnSc1
(	(	kIx(
<g/>
mrchožrout	mrchožrout	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
detritovor	detritovor	k1gInSc1
(	(	kIx(
<g/>
hnilobné	hnilobný	k2eAgInPc1d1
zbytky	zbytek	k1gInPc1
<g/>
)	)	kIx)
•	•	k?
koprofág	koprofág	k1gMnSc1
(	(	kIx(
<g/>
výkaly	výkal	k1gInPc1
<g/>
)	)	kIx)
mikroskopické	mikroskopický	k2eAgFnPc1d1
</s>
<s>
bakterivor	bakterivor	k1gInSc1
(	(	kIx(
<g/>
bakterie	bakterie	k1gFnSc1
<g/>
)	)	kIx)
míra	míra	k1gFnSc1
specializace	specializace	k1gFnSc2
na	na	k7c4
zdroj	zdroj	k1gInSc4
</s>
<s>
polyfág	polyfág	k1gMnSc1
•	•	k?
oligofág	oligofág	k1gMnSc1
•	•	k?
monofág	monofág	k1gMnSc1
(	(	kIx(
<g/>
parazitismus	parazitismus	k1gInSc1
<g/>
)	)	kIx)
metody	metoda	k1gFnPc1
</s>
<s>
pastva	pastva	k1gFnSc1
•	•	k?
lov	lov	k1gInSc4
•	•	k?
filtrování	filtrování	k1gNnSc2
•	•	k?
sběr	sběr	k1gInSc4
•	•	k?
skladování	skladování	k1gNnSc2
•	•	k?
pěstování	pěstování	k1gNnSc2
•	•	k?
chov	chov	k1gInSc1
•	•	k?
trofalaxe	trofalaxe	k1gFnSc1
(	(	kIx(
<g/>
dojení	dojení	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
fagocytóza	fagocytóza	k1gFnSc1
(	(	kIx(
<g/>
do	do	k7c2
vakuol	vakuola	k1gFnPc2
<g/>
)	)	kIx)
•	•	k?
symbióza	symbióza	k1gFnSc1
•	•	k?
cizopasení	cizopasení	k1gNnSc2
•	•	k?
kleptoparazitismus	kleptoparazitismus	k1gInSc4
(	(	kIx(
<g/>
krádež	krádež	k1gFnSc4
<g/>
)	)	kIx)
•	•	k?
mutualismus	mutualismus	k1gInSc1
(	(	kIx(
<g/>
spolupráce	spolupráce	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
altruismus	altruismus	k1gInSc1
(	(	kIx(
<g/>
vyvrhování	vyvrhování	k1gNnSc1
<g/>
)	)	kIx)
etologie	etologie	k1gFnSc1
</s>
<s>
bentičnost	bentičnost	k1gFnSc1
(	(	kIx(
<g/>
žití	žití	k1gNnSc1
při	při	k7c6
dně	dno	k1gNnSc6
<g/>
,	,	kIx,
měkkýši	měkkýš	k1gMnPc1
<g/>
,	,	kIx,
korýši	korýš	k1gMnPc1
<g/>
)	)	kIx)
•	•	k?
včelí	včelí	k2eAgNnSc4d1
sbírání	sbírání	k1gNnSc4
•	•	k?
předávání	předávání	k1gNnSc2
mezi	mezi	k7c7
mravenci	mravenec	k1gMnPc7
•	•	k?
dojení	dojení	k1gNnSc4
medovice	medovice	k1gFnSc2
mšic	mšice	k1gFnPc2
•	•	k?
australští	australský	k2eAgMnPc1d1
mravenci	mravenec	k1gMnPc1
medonosní	medonosný	k2eAgMnPc1d1
•	•	k?
houby	houba	k1gFnPc4
v	v	k7c6
mraveništi	mraveniště	k1gNnSc6
•	•	k?
řasy	řasa	k1gFnSc2
na	na	k7c6
korálech	korál	k1gInPc6
•	•	k?
levhart	levhart	k1gMnSc1
zaháněný	zaháněný	k2eAgMnSc1d1
lvy	lev	k1gInPc1
•	•	k?
vzájemné	vzájemný	k2eAgNnSc1d1
krmení	krmení	k1gNnSc1
upírů	upír	k1gMnPc2
</s>
<s>
savčí	savčí	k2eAgInSc1d1
</s>
<s>
laktace-kojení	laktace-kojení	k1gNnSc1
(	(	kIx(
<g/>
sání	sání	k1gNnSc1
mléka	mléko	k1gNnSc2
<g/>
)	)	kIx)
•	•	k?
odstavení	odstavení	k1gNnSc4
•	•	k?
placentofágie	placentofágie	k1gFnSc1
(	(	kIx(
<g/>
pozření	pozření	k1gNnSc1
placenty	placenta	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
lidské	lidský	k2eAgFnPc1d1
</s>
<s>
vegetariánství	vegetariánství	k1gNnSc1
(	(	kIx(
<g/>
veganství	veganství	k1gNnSc1
<g/>
,	,	kIx,
frutariánství	frutariánství	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
rozlišování	rozlišování	k1gNnSc1
původu	původ	k1gInSc2
potravy	potrava	k1gFnSc2
(	(	kIx(
<g/>
lokavor	lokavor	k1gInSc1
(	(	kIx(
<g/>
místní	místní	k2eAgMnPc1d1
<g/>
)	)	kIx)
/	/	kIx~
invazivor	invazivor	k1gInSc1
(	(	kIx(
<g/>
z	z	k7c2
daleka	daleko	k1gNnSc2
<g/>
))	))	k?
</s>
<s>
zvláštní	zvláštní	k2eAgMnSc1d1
</s>
<s>
mukofágie	mukofágie	k1gFnSc1
(	(	kIx(
<g/>
hleny	hlen	k1gInPc1
a	a	k8xC
slizy	sliz	k1gInPc1
<g/>
)	)	kIx)
•	•	k?
osteofágie	osteofágie	k1gFnSc2
•	•	k?
geofágie	geofágie	k1gFnSc2
(	(	kIx(
<g/>
horniny	hornina	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
evoluce	evoluce	k1gFnSc1
</s>
<s>
predátor	predátor	k1gMnSc1
•	•	k?
kořist	kořist	k1gFnSc1
•	•	k?
ochranné	ochranný	k2eAgNnSc4d1
přizpůsobení	přizpůsobení	k1gNnSc4
(	(	kIx(
<g/>
adaptace	adaptace	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
obranné	obranný	k2eAgNnSc1d1
chování	chování	k1gNnSc1
(	(	kIx(
<g/>
mimikry	mimikry	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
koevoluce	koevoluce	k1gFnSc1
(	(	kIx(
<g/>
efekt	efekt	k1gInSc1
červené	červený	k2eAgFnSc2d1
královny	královna	k1gFnSc2
<g/>
)	)	kIx)
trávení	trávení	k1gNnSc1
•	•	k?
potravní	potravní	k2eAgInSc1d1
řetězec	řetězec	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ph	ph	kA
<g/>
127004	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4187457-2	4187457-2	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
12780	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85142520	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85142520	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Gastronomie	gastronomie	k1gFnSc1
</s>
