<s>
Utopie	utopie	k1gFnSc1	utopie
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
ο	ο	k?	ο
τ	τ	k?	τ
ú-topos	úopos	k1gInSc1	ú-topos
<g/>
,	,	kIx,	,
žádné	žádný	k3yNgNnSc4	žádný
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
nikde	nikde	k6eAd1	nikde
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
představa	představa	k1gFnSc1	představa
ideální	ideální	k2eAgFnSc2d1	ideální
lidské	lidský	k2eAgFnSc2d1	lidská
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
obce	obec	k1gFnSc2	obec
nebo	nebo	k8xC	nebo
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc4	slovo
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
anglický	anglický	k2eAgMnSc1d1	anglický
humanistický	humanistický	k2eAgMnSc1d1	humanistický
myslitel	myslitel	k1gMnSc1	myslitel
Thomas	Thomas	k1gMnSc1	Thomas
More	mor	k1gInSc5	mor
jako	jako	k9	jako
název	název	k1gInSc4	název
své	svůj	k3xOyFgFnSc2	svůj
knihy	kniha	k1gFnSc2	kniha
Utopie	utopie	k1gFnSc2	utopie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1516	[number]	k4	1516
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
významu	význam	k1gInSc6	význam
označuje	označovat	k5eAaImIp3nS	označovat
něco	něco	k3yInSc1	něco
žádoucího	žádoucí	k2eAgNnSc2d1	žádoucí
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
možná	možná	k9	možná
nedosažitelného	dosažitelný	k2eNgNnSc2d1	nedosažitelné
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
také	také	k9	také
utopický	utopický	k2eAgInSc4d1	utopický
<g/>
,	,	kIx,	,
idealizovaný	idealizovaný	k2eAgInSc4d1	idealizovaný
a	a	k8xC	a
utopismus	utopismus	k1gInSc1	utopismus
<g/>
,	,	kIx,	,
idealizace	idealizace	k1gFnSc1	idealizace
nebo	nebo	k8xC	nebo
naivita	naivita	k1gFnSc1	naivita
v	v	k7c6	v
politickém	politický	k2eAgInSc6d1	politický
nebo	nebo	k8xC	nebo
společenském	společenský	k2eAgInSc6d1	společenský
kontextu	kontext	k1gInSc6	kontext
<g/>
.	.	kIx.	.
</s>
<s>
Paradoxní	paradoxní	k2eAgFnSc4d1	paradoxní
definici	definice	k1gFnSc4	definice
nabízí	nabízet	k5eAaImIp3nS	nabízet
Patrik	Patrik	k1gMnSc1	Patrik
Ouředník	Ouředník	k1gMnSc1	Ouředník
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Utopie	utopie	k1gFnSc1	utopie
je	být	k5eAaImIp3nS	být
virtuální	virtuální	k2eAgInSc4d1	virtuální
prostor	prostor	k1gInSc4	prostor
<g/>
,	,	kIx,	,
nereálná	reálný	k2eNgFnSc1d1	nereálná
realita	realita	k1gFnSc1	realita
<g/>
,	,	kIx,	,
nepřítomná	přítomný	k2eNgFnSc1d1	nepřítomná
přítomnost	přítomnost	k1gFnSc1	přítomnost
<g/>
,	,	kIx,	,
alterita	alterita	k1gFnSc1	alterita
bez	bez	k7c2	bez
identity	identita	k1gFnSc2	identita
<g/>
.	.	kIx.	.
</s>
<s>
Utopie	utopie	k1gFnSc1	utopie
je	být	k5eAaImIp3nS	být
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
na	na	k7c6	na
jiném	jiný	k2eAgNnSc6d1	jiné
místě	místo	k1gNnSc6	místo
nebo	nebo	k8xC	nebo
v	v	k7c6	v
jiném	jiný	k2eAgInSc6d1	jiný
čase	čas	k1gInSc6	čas
<g/>
,	,	kIx,	,
příslib	příslib	k1gInSc1	příslib
<g/>
,	,	kIx,	,
náznak	náznak	k1gInSc1	náznak
<g/>
,	,	kIx,	,
naděje	naděje	k1gFnSc1	naděje
<g/>
,	,	kIx,	,
noční	noční	k2eAgFnSc1d1	noční
můra	můra	k1gFnSc1	můra
<g/>
,	,	kIx,	,
zlý	zlý	k2eAgInSc1d1	zlý
sen	sen	k1gInSc1	sen
<g/>
.	.	kIx.	.
</s>
<s>
Utopie	utopie	k1gFnSc1	utopie
je	být	k5eAaImIp3nS	být
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
nám	my	k3xPp1nPc3	my
navždy	navždy	k6eAd1	navždy
uzavřen	uzavřen	k2eAgInSc1d1	uzavřen
<g/>
,	,	kIx,	,
stav	stav	k1gInSc1	stav
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgInSc3	jenž
nevyhnutelně	vyhnutelně	k6eNd1	vyhnutelně
směřujeme	směřovat	k5eAaImIp1nP	směřovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Slovo	slovo	k1gNnSc1	slovo
Utopie	utopie	k1gFnSc2	utopie
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
moderního	moderní	k2eAgInSc2d1	moderní
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
žánr	žánr	k1gInSc1	žánr
je	být	k5eAaImIp3nS	být
však	však	k9	však
daleko	daleko	k6eAd1	daleko
starší	starý	k2eAgMnSc1d2	starší
<g/>
.	.	kIx.	.
</s>
<s>
Utopické	utopický	k2eAgInPc4d1	utopický
prvky	prvek	k1gInPc4	prvek
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
už	už	k6eAd1	už
v	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
před	před	k7c7	před
n.	n.	k?	n.
l.	l.	k?	l.
v	v	k7c6	v
Izajášově	Izajášův	k2eAgNnSc6d1	Izajášovo
proroctví	proroctví	k1gNnSc6	proroctví
nebo	nebo	k8xC	nebo
Homérově	Homérův	k2eAgInSc6d1	Homérův
Oddyseji	Oddysej	k1gInSc6	Oddysej
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
ucelenější	ucelený	k2eAgFnSc1d2	ucelenější
utopie	utopie	k1gFnSc1	utopie
je	být	k5eAaImIp3nS	být
Platónův	Platónův	k2eAgInSc1d1	Platónův
pozdní	pozdní	k2eAgInSc1d1	pozdní
spis	spis	k1gInSc1	spis
"	"	kIx"	"
<g/>
Zákony	zákon	k1gInPc1	zákon
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Nomoi	Nomoi	k1gNnSc1	Nomoi
<g/>
,	,	kIx,	,
Leges	Leges	k1gInSc1	Leges
<g/>
)	)	kIx)	)
a	a	k8xC	a
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
i	i	k8xC	i
"	"	kIx"	"
<g/>
Ústava	ústava	k1gFnSc1	ústava
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Politeia	Politeia	k1gFnSc1	Politeia
<g/>
,	,	kIx,	,
Republica	Republica	k1gFnSc1	Republica
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Platón	Platón	k1gMnSc1	Platón
obě	dva	k4xCgFnPc4	dva
knihy	kniha	k1gFnPc4	kniha
napsal	napsat	k5eAaBmAgMnS	napsat
pod	pod	k7c7	pod
bezprostředním	bezprostřední	k2eAgInSc7d1	bezprostřední
dojmem	dojem	k1gInSc7	dojem
ze	z	k7c2	z
ztroskotání	ztroskotání	k1gNnSc2	ztroskotání
athénské	athénský	k2eAgFnSc2d1	Athénská
demokracie	demokracie	k1gFnSc2	demokracie
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
z	z	k7c2	z
něho	on	k3xPp3gMnSc2	on
vyvodit	vyvodit	k5eAaBmF	vyvodit
velmi	velmi	k6eAd1	velmi
podrobné	podrobný	k2eAgNnSc1d1	podrobné
poučení	poučení	k1gNnSc1	poučení
<g/>
.	.	kIx.	.
</s>
<s>
Budoucí	budoucí	k2eAgFnSc1d1	budoucí
obec	obec	k1gFnSc1	obec
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
správnou	správný	k2eAgFnSc4d1	správná
velikost	velikost	k1gFnSc4	velikost
a	a	k8xC	a
polohu	poloha	k1gFnSc4	poloha
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
více	hodně	k6eAd2	hodně
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
<g/>
,	,	kIx,	,
omezovat	omezovat	k5eAaImF	omezovat
podnikání	podnikání	k1gNnSc4	podnikání
pro	pro	k7c4	pro
zisk	zisk	k1gInSc4	zisk
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
zvláštní	zvláštní	k2eAgFnPc4d1	zvláštní
kasty	kasta	k1gFnPc4	kasta
"	"	kIx"	"
<g/>
strážců	strážce	k1gMnPc2	strážce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
nesmějí	smát	k5eNaImIp3nP	smát
mít	mít	k5eAaImF	mít
majetek	majetek	k1gInSc4	majetek
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
žít	žít	k5eAaImF	žít
společně	společně	k6eAd1	společně
a	a	k8xC	a
musí	muset	k5eAaImIp3nS	muset
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
být	být	k5eAaImF	být
zvlášť	zvlášť	k6eAd1	zvlášť
vychováváni	vychovávat	k5eAaImNgMnP	vychovávat
<g/>
.	.	kIx.	.
</s>
<s>
Klíčové	klíčový	k2eAgInPc1d1	klíčový
jsou	být	k5eAaImIp3nP	být
přísné	přísný	k2eAgInPc1d1	přísný
zákony	zákon	k1gInPc1	zákon
a	a	k8xC	a
společná	společný	k2eAgFnSc1d1	společná
výchova	výchova	k1gFnSc1	výchova
všech	všecek	k3xTgFnPc2	všecek
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Utopické	utopický	k2eAgInPc4d1	utopický
prvky	prvek	k1gInPc4	prvek
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
biblická	biblický	k2eAgFnSc1d1	biblická
Apokalypsa	apokalypsa	k1gFnSc1	apokalypsa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ovšem	ovšem	k9	ovšem
popisuje	popisovat	k5eAaImIp3nS	popisovat
očekávání	očekávání	k1gNnSc4	očekávání
budoucího	budoucí	k2eAgInSc2d1	budoucí
"	"	kIx"	"
<g/>
Nového	Nového	k2eAgInSc2d1	Nového
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Prototypem	prototyp	k1gInSc7	prototyp
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
utopií	utopie	k1gFnPc2	utopie
je	být	k5eAaImIp3nS	být
Augustinovo	Augustinův	k2eAgNnSc1d1	Augustinovo
dílo	dílo	k1gNnSc1	dílo
"	"	kIx"	"
<g/>
Boží	boží	k2eAgFnSc1d1	boží
obec	obec	k1gFnSc1	obec
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
420	[number]	k4	420
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
základ	základ	k1gInSc1	základ
politické	politický	k2eAgFnSc2d1	politická
teorie	teorie	k1gFnSc2	teorie
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Moreova	Moreův	k2eAgFnSc1d1	Moreův
kniha	kniha	k1gFnSc1	kniha
"	"	kIx"	"
<g/>
O	o	k7c6	o
nejlepším	dobrý	k2eAgInSc6d3	nejlepší
stavu	stav	k1gInSc6	stav
obce	obec	k1gFnSc2	obec
a	a	k8xC	a
o	o	k7c6	o
ostrově	ostrov	k1gInSc6	ostrov
Utopie	utopie	k1gFnSc2	utopie
<g/>
"	"	kIx"	"
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
Platónovu	Platónův	k2eAgFnSc4d1	Platónova
Ústavu	ústava	k1gFnSc4	ústava
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
inspirována	inspirovat	k5eAaBmNgFnS	inspirovat
zámořskými	zámořský	k2eAgInPc7d1	zámořský
objevy	objev	k1gInPc7	objev
–	–	k?	–
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
se	se	k3xPyFc4	se
na	na	k7c6	na
neznámém	známý	k2eNgInSc6d1	neznámý
ostrově	ostrov	k1gInSc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
More	mor	k1gInSc5	mor
používá	používat	k5eAaImIp3nS	používat
také	také	k9	také
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Eutopie	Eutopie	k1gFnSc1	Eutopie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zní	znět	k5eAaImIp3nS	znět
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
stejně	stejně	k6eAd1	stejně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
Dobré	dobrý	k2eAgNnSc1d1	dobré
místo	místo	k1gNnSc1	místo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
Utopie	utopie	k1gFnSc1	utopie
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
zároveň	zároveň	k6eAd1	zároveň
popis	popis	k1gInSc4	popis
ideální	ideální	k2eAgFnSc2d1	ideální
obce	obec	k1gFnSc2	obec
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc4	jejíž
přednosti	přednost	k1gFnPc4	přednost
(	(	kIx(	(
<g/>
rovnost	rovnost	k1gFnSc1	rovnost
občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,	,
pacifismus	pacifismus	k1gInSc1	pacifismus
<g/>
,	,	kIx,	,
náboženská	náboženský	k2eAgFnSc1d1	náboženská
tolerance	tolerance	k1gFnSc1	tolerance
<g/>
)	)	kIx)	)
zároveň	zároveň	k6eAd1	zároveň
kritizují	kritizovat	k5eAaImIp3nP	kritizovat
současnou	současný	k2eAgFnSc4d1	současná
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgFnPc1d1	významná
utopie	utopie	k1gFnPc1	utopie
–	–	k?	–
kromě	kromě	k7c2	kromě
Mora	mora	k1gFnSc1	mora
–	–	k?	–
napsali	napsat	k5eAaBmAgMnP	napsat
například	například	k6eAd1	například
Tommaso	Tommasa	k1gFnSc5	Tommasa
Campanella	Campanella	k1gMnSc1	Campanella
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
O	o	k7c6	o
slunečním	sluneční	k2eAgInSc6d1	sluneční
státě	stát	k1gInSc6	stát
<g/>
"	"	kIx"	"
1623	[number]	k4	1623
<g/>
)	)	kIx)	)
a	a	k8xC	a
Francis	Francis	k1gFnSc1	Francis
Bacon	Bacona	k1gFnPc2	Bacona
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Nová	nový	k2eAgFnSc1d1	nová
Atlantis	Atlantis	k1gFnSc1	Atlantis
<g/>
"	"	kIx"	"
1627	[number]	k4	1627
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kritickou	kritický	k2eAgFnSc4d1	kritická
stránku	stránka	k1gFnSc4	stránka
zdůrazňují	zdůrazňovat	k5eAaImIp3nP	zdůrazňovat
autoři	autor	k1gMnPc1	autor
negativních	negativní	k2eAgFnPc2d1	negativní
utopií	utopie	k1gFnPc2	utopie
čili	čili	k8xC	čili
dystopií	dystopie	k1gFnPc2	dystopie
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
Labyrint	labyrint	k1gInSc1	labyrint
světa	svět	k1gInSc2	svět
a	a	k8xC	a
ráj	ráj	k1gInSc1	ráj
srdce	srdce	k1gNnSc2	srdce
<g/>
"	"	kIx"	"
J.	J.	kA	J.
A.	A.	kA	A.
Komenského	Komenský	k1gMnSc2	Komenský
(	(	kIx(	(
<g/>
1631	[number]	k4	1631
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Jiný	jiný	k2eAgInSc1d1	jiný
svět	svět	k1gInSc1	svět
<g/>
"	"	kIx"	"
Cyrana	Cyrano	k1gMnSc2	Cyrano
z	z	k7c2	z
Bergeracu	Bergeracus	k1gInSc2	Bergeracus
(	(	kIx(	(
<g/>
1657	[number]	k4	1657
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
satira	satira	k1gFnSc1	satira
"	"	kIx"	"
<g/>
Gulliverovy	Gulliverův	k2eAgFnSc2d1	Gulliverova
cesty	cesta	k1gFnSc2	cesta
<g/>
"	"	kIx"	"
Jonathana	Jonathan	k1gMnSc2	Jonathan
Swifta	Swift	k1gMnSc2	Swift
(	(	kIx(	(
<g/>
1721	[number]	k4	1721
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Utopické	utopický	k2eAgFnPc1d1	utopická
pasáže	pasáž	k1gFnPc1	pasáž
a	a	k8xC	a
prvky	prvek	k1gInPc1	prvek
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
u	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgMnPc2d1	další
autorů	autor	k1gMnPc2	autor
(	(	kIx(	(
<g/>
François	François	k1gFnSc1	François
Rabelais	Rabelais	k1gFnSc1	Rabelais
<g/>
,	,	kIx,	,
Daniel	Daniel	k1gMnSc1	Daniel
Defoe	Defo	k1gFnSc2	Defo
<g/>
,	,	kIx,	,
Voltaire	Voltair	k1gInSc5	Voltair
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Utopie	utopie	k1gFnSc1	utopie
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
Robert	Robert	k1gMnSc1	Robert
Owen	Owen	k1gMnSc1	Owen
"	"	kIx"	"
<g/>
Nový	nový	k2eAgInSc1d1	nový
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
společnost	společnost	k1gFnSc4	společnost
<g/>
"	"	kIx"	"
1813	[number]	k4	1813
<g/>
;	;	kIx,	;
Charles	Charles	k1gMnSc1	Charles
Fourier	Fourier	k1gMnSc1	Fourier
"	"	kIx"	"
<g/>
Nový	nový	k2eAgInSc1d1	nový
průmyslový	průmyslový	k2eAgInSc1d1	průmyslový
svět	svět	k1gInSc1	svět
<g/>
"	"	kIx"	"
1829	[number]	k4	1829
<g/>
;	;	kIx,	;
Bernard	Bernard	k1gMnSc1	Bernard
Bolzano	Bolzana	k1gFnSc5	Bolzana
"	"	kIx"	"
<g/>
O	o	k7c6	o
nejlepším	dobrý	k2eAgInSc6d3	nejlepší
státě	stát	k1gInSc6	stát
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
1832	[number]	k4	1832
<g/>
;	;	kIx,	;
H.	H.	kA	H.
G.	G.	kA	G.
Wells	Wellsa	k1gFnPc2	Wellsa
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
Moderní	moderní	k2eAgFnPc1d1	moderní
utopie	utopie	k1gFnPc1	utopie
<g/>
"	"	kIx"	"
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
považují	považovat	k5eAaImIp3nP	považovat
většinou	většina	k1gFnSc7	většina
za	za	k7c4	za
uskutečnitelné	uskutečnitelný	k2eAgNnSc4d1	uskutečnitelné
a	a	k8xC	a
Owen	Owen	k1gNnSc4	Owen
i	i	k9	i
Fourier	Fourier	k1gInSc1	Fourier
je	být	k5eAaImIp3nS	být
také	také	k9	také
realizovali	realizovat	k5eAaBmAgMnP	realizovat
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
převládají	převládat	k5eAaImIp3nP	převládat
utopie	utopie	k1gFnPc4	utopie
negativní	negativní	k2eAgFnPc4d1	negativní
a	a	k8xC	a
varovné	varovný	k2eAgFnPc4d1	varovná
(	(	kIx(	(
<g/>
antiutopie	antiutopie	k1gFnPc4	antiutopie
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Jevgenij	Jevgenij	k1gFnSc1	Jevgenij
Zamjatin	Zamjatina	k1gFnPc2	Zamjatina
"	"	kIx"	"
<g/>
My	my	k3xPp1nPc1	my
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Aldous	Aldous	k1gMnSc1	Aldous
Huxley	Huxlea	k1gFnSc2	Huxlea
"	"	kIx"	"
<g/>
Krásný	krásný	k2eAgInSc1d1	krásný
nový	nový	k2eAgInSc1d1	nový
svět	svět	k1gInSc1	svět
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
George	George	k1gFnSc1	George
Orwell	Orwell	k1gMnSc1	Orwell
"	"	kIx"	"
<g/>
Farma	farma	k1gFnSc1	farma
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
1984	[number]	k4	1984
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Utopie	utopie	k1gFnPc1	utopie
jsou	být	k5eAaImIp3nP	být
vesměs	vesměs	k6eAd1	vesměs
racionální	racionální	k2eAgInPc1d1	racionální
projekty	projekt	k1gInPc1	projekt
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
snažili	snažit	k5eAaImAgMnP	snažit
postihnout	postihnout	k5eAaPmF	postihnout
hlavní	hlavní	k2eAgInPc4d1	hlavní
neduhy	neduh	k1gInPc4	neduh
svých	svůj	k3xOyFgFnPc2	svůj
společností	společnost	k1gFnPc2	společnost
a	a	k8xC	a
většinou	většinou	k6eAd1	většinou
proti	proti	k7c3	proti
nim	on	k3xPp3gInPc3	on
postavit	postavit	k5eAaPmF	postavit
své	svůj	k3xOyFgInPc4	svůj
ideály	ideál	k1gInPc4	ideál
<g/>
.	.	kIx.	.
</s>
<s>
Měly	mít	k5eAaImAgFnP	mít
tedy	tedy	k9	tedy
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
pro	pro	k7c4	pro
myšlení	myšlení	k1gNnSc4	myšlení
o	o	k7c6	o
společnosti	společnost	k1gFnSc6	společnost
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
moderních	moderní	k2eAgInPc2d1	moderní
ideálů	ideál	k1gInPc2	ideál
(	(	kIx(	(
<g/>
právní	právní	k2eAgInSc1d1	právní
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
rovnost	rovnost	k1gFnSc1	rovnost
ras	rasa	k1gFnPc2	rasa
a	a	k8xC	a
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
,	,	kIx,	,
potřeba	potřeba	k1gFnSc1	potřeba
míru	mír	k1gInSc2	mír
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgFnSc2d1	sociální
péče	péče	k1gFnSc2	péče
<g/>
,	,	kIx,	,
tolerance	tolerance	k1gFnSc2	tolerance
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
právě	právě	k9	právě
v	v	k7c6	v
utopiích	utopie	k1gFnPc6	utopie
<g/>
.	.	kIx.	.
</s>
<s>
Německý	německý	k2eAgMnSc1d1	německý
sociolog	sociolog	k1gMnSc1	sociolog
Karl	Karla	k1gFnPc2	Karla
Mannheim	Mannheim	k1gInSc4	Mannheim
proto	proto	k8xC	proto
postavil	postavit	k5eAaPmAgMnS	postavit
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
ideologie	ideologie	k1gFnSc2	ideologie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
stávající	stávající	k2eAgFnSc1d1	stávající
společnost	společnost	k1gFnSc1	společnost
podporují	podporovat	k5eAaImIp3nP	podporovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
utopie	utopie	k1gFnPc1	utopie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jej	on	k3xPp3gMnSc4	on
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
změnit	změnit	k5eAaPmF	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
však	však	k9	však
vzniká	vznikat	k5eAaImIp3nS	vznikat
i	i	k9	i
ostrá	ostrý	k2eAgFnSc1d1	ostrá
kritika	kritika	k1gFnSc1	kritika
utopií	utopie	k1gFnPc2	utopie
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
například	například	k6eAd1	například
Karl	Karl	k1gMnSc1	Karl
Raimund	Raimunda	k1gFnPc2	Raimunda
Popper	Popper	k1gMnSc1	Popper
spojoval	spojovat	k5eAaImAgMnS	spojovat
s	s	k7c7	s
totalitními	totalitní	k2eAgInPc7d1	totalitní
režimy	režim	k1gInPc7	režim
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
historiky	historik	k1gMnPc4	historik
mají	mít	k5eAaImIp3nP	mít
utopie	utopie	k1gFnPc4	utopie
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
jako	jako	k8xS	jako
výrazy	výraz	k1gInPc4	výraz
starostí	starost	k1gFnPc2	starost
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
ideálů	ideál	k1gInPc2	ideál
a	a	k8xC	a
hodnot	hodnota	k1gFnPc2	hodnota
různých	různý	k2eAgFnPc2d1	různá
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
šlo	jít	k5eAaImAgNnS	jít
Platónovi	Platón	k1gMnSc3	Platón
o	o	k7c4	o
posílení	posílení	k1gNnSc4	posílení
a	a	k8xC	a
udržení	udržení	k1gNnSc4	udržení
obce	obec	k1gFnSc2	obec
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
Augustinovi	Augustin	k1gMnSc3	Augustin
o	o	k7c4	o
spásu	spása	k1gFnSc4	spása
duší	duše	k1gFnPc2	duše
a	a	k8xC	a
Boží	boží	k2eAgNnSc4d1	boží
království	království	k1gNnSc4	království
<g/>
.	.	kIx.	.
</s>
<s>
More	mor	k1gInSc5	mor
hledal	hledat	k5eAaImAgInS	hledat
rovnost	rovnost	k1gFnSc4	rovnost
<g/>
,	,	kIx,	,
mír	mír	k1gInSc4	mír
a	a	k8xC	a
toleranci	tolerance	k1gFnSc4	tolerance
<g/>
,	,	kIx,	,
Bacon	Bacon	k1gInSc4	Bacon
využití	využití	k1gNnSc4	využití
vědy	věda	k1gFnSc2	věda
k	k	k7c3	k
prospěchu	prospěch	k1gInSc3	prospěch
všech	všecek	k3xTgMnPc2	všecek
a	a	k8xC	a
Komenský	Komenský	k1gMnSc1	Komenský
vybízel	vybízet	k5eAaImAgMnS	vybízet
k	k	k7c3	k
soustředění	soustředění	k1gNnSc3	soustředění
na	na	k7c4	na
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
život	život	k1gInSc4	život
duše	duše	k1gFnSc2	duše
<g/>
.	.	kIx.	.
</s>
<s>
Utopisté	utopista	k1gMnPc1	utopista
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
hledali	hledat	k5eAaImAgMnP	hledat
konkrétní	konkrétní	k2eAgFnPc4d1	konkrétní
možnosti	možnost	k1gFnPc4	možnost
jak	jak	k8xC	jak
zlepšit	zlepšit	k5eAaPmF	zlepšit
život	život	k1gInSc4	život
dělníků	dělník	k1gMnPc2	dělník
a	a	k8xC	a
chudých	chudý	k2eAgMnPc2d1	chudý
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
si	se	k3xPyFc3	se
představovali	představovat	k5eAaImAgMnP	představovat
velké	velký	k2eAgFnPc4d1	velká
možnosti	možnost	k1gFnPc4	možnost
technického	technický	k2eAgInSc2d1	technický
pokroku	pokrok	k1gInSc2	pokrok
(	(	kIx(	(
<g/>
Jules	Jules	k1gInSc1	Jules
Verne	Vern	k1gInSc5	Vern
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
převládají	převládat	k5eAaImIp3nP	převládat
varování	varování	k1gNnPc4	varování
před	před	k7c7	před
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
mocí	moc	k1gFnSc7	moc
lidí	člověk	k1gMnPc2	člověk
i	i	k8xC	i
států	stát	k1gInPc2	stát
a	a	k8xC	a
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
na	na	k7c6	na
utopie	utopie	k1gFnPc4	utopie
navazuje	navazovat	k5eAaImIp3nS	navazovat
science-fiction	scienceiction	k1gInSc1	science-fiction
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
si	se	k3xPyFc3	se
všímá	všímat	k5eAaImIp3nS	všímat
spíše	spíše	k9	spíše
technických	technický	k2eAgFnPc2d1	technická
možností	možnost	k1gFnPc2	možnost
než	než	k8xS	než
uspořádání	uspořádání	k1gNnSc2	uspořádání
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
