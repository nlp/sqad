<s>
William	William	k1gInSc1	William
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
(	(	kIx(	(
<g/>
pokřtěn	pokřtěn	k2eAgMnSc1d1	pokřtěn
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1564	[number]	k4	1564
–	–	k?	–
zemřel	zemřít	k5eAaPmAgInS	zemřít
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1616	[number]	k4	1616
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
anglický	anglický	k2eAgMnSc1d1	anglický
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
široce	široko	k6eAd1	široko
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
největšího	veliký	k2eAgMnSc4d3	veliký
anglicky	anglicky	k6eAd1	anglicky
píšícího	píšící	k2eAgMnSc2d1	píšící
spisovatele	spisovatel	k1gMnSc2	spisovatel
a	a	k8xC	a
celosvětově	celosvětově	k6eAd1	celosvětově
nejpřednějšího	přední	k2eAgMnSc2d3	nejpřednější
dramatika	dramatik	k1gMnSc2	dramatik
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
nazýván	nazývat	k5eAaImNgInS	nazývat
anglickým	anglický	k2eAgMnSc7d1	anglický
národním	národní	k2eAgMnSc7d1	národní
básníkem	básník	k1gMnSc7	básník
a	a	k8xC	a
"	"	kIx"	"
<g/>
bardem	bard	k1gMnSc7	bard
z	z	k7c2	z
Avonu	Avon	k1gInSc2	Avon
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dochovalo	dochovat	k5eAaPmAgNnS	dochovat
se	se	k3xPyFc4	se
jemu	on	k3xPp3gMnSc3	on
připsaných	připsaný	k2eAgNnPc2d1	připsané
přibližně	přibližně	k6eAd1	přibližně
38	[number]	k4	38
her	hra	k1gFnPc2	hra
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
těch	ten	k3xDgFnPc2	ten
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yQgFnPc6	který
asi	asi	k9	asi
s	s	k7c7	s
někým	někdo	k3yInSc7	někdo
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
154	[number]	k4	154
sonetů	sonet	k1gInPc2	sonet
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc4	dva
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
epické	epický	k2eAgFnPc4d1	epická
básně	báseň	k1gFnPc4	báseň
a	a	k8xC	a
několik	několik	k4yIc4	několik
dalších	další	k2eAgFnPc2d1	další
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některé	některý	k3yIgFnPc1	některý
jsou	být	k5eAaImIp3nP	být
nejistého	jistý	k2eNgNnSc2d1	nejisté
autorství	autorství	k1gNnSc2	autorství
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
hry	hra	k1gFnPc1	hra
byly	být	k5eAaImAgFnP	být
přeloženy	přeložit	k5eAaPmNgFnP	přeložit
do	do	k7c2	do
všech	všecek	k3xTgInPc2	všecek
hlavních	hlavní	k2eAgInPc2d1	hlavní
živých	živý	k2eAgInPc2d1	živý
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
uváděny	uvádět	k5eAaImNgInP	uvádět
častěji	často	k6eAd2	často
než	než	k8xS	než
hry	hra	k1gFnSc2	hra
jakéhokoli	jakýkoli	k3yIgMnSc2	jakýkoli
jiného	jiný	k2eAgMnSc2d1	jiný
dramatika	dramatik	k1gMnSc2	dramatik
<g/>
.	.	kIx.	.
</s>
<s>
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
a	a	k8xC	a
vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
ve	v	k7c6	v
Stratfordu	Stratford	k1gInSc6	Stratford
nad	nad	k7c7	nad
Avonou	Avona	k1gFnSc7	Avona
<g/>
,	,	kIx,	,
v	v	k7c6	v
anglickém	anglický	k2eAgNnSc6d1	anglické
hrabství	hrabství	k1gNnSc6	hrabství
Warwickshire	Warwickshir	k1gMnSc5	Warwickshir
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věku	věk	k1gInSc6	věk
18	[number]	k4	18
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Anne	Anne	k1gFnSc7	Anne
Hathawayovou	Hathawayový	k2eAgFnSc7d1	Hathawayová
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
měl	mít	k5eAaImAgMnS	mít
tři	tři	k4xCgFnPc4	tři
děti	dítě	k1gFnPc4	dítě
<g/>
:	:	kIx,	:
dceru	dcera	k1gFnSc4	dcera
Susannu	Susann	k1gInSc2	Susann
a	a	k8xC	a
dvojčata	dvojče	k1gNnPc1	dvojče
syna	syn	k1gMnSc2	syn
Hamneta	Hamnet	k1gMnSc2	Hamnet
a	a	k8xC	a
dceru	dcera	k1gFnSc4	dcera
Judith	Juditha	k1gFnPc2	Juditha
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1585	[number]	k4	1585
a	a	k8xC	a
1592	[number]	k4	1592
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
kariéru	kariéra	k1gFnSc4	kariéra
jako	jako	k8xS	jako
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
částečný	částečný	k2eAgMnSc1d1	částečný
vlastník	vlastník	k1gMnSc1	vlastník
herecké	herecký	k2eAgFnSc2d1	herecká
společnosti	společnost	k1gFnSc2	společnost
s	s	k7c7	s
názvem	název	k1gInSc7	název
Služebníci	služebník	k1gMnPc1	služebník
lorda	lord	k1gMnSc2	lord
komořího	komoří	k1gMnSc2	komoří
(	(	kIx(	(
<g/>
Lord	lord	k1gMnSc1	lord
Chamberlain	Chamberlain	k1gMnSc1	Chamberlain
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Men	Men	k1gFnSc7	Men
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
známé	známý	k2eAgFnPc1d1	známá
jako	jako	k8xC	jako
Královská	královský	k2eAgFnSc1d1	královská
společnost	společnost	k1gFnSc1	společnost
(	(	kIx(	(
<g/>
King	King	k1gMnSc1	King
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Men	Men	k1gFnSc7	Men
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1613	[number]	k4	1613
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
49	[number]	k4	49
let	léto	k1gNnPc2	léto
odešel	odejít	k5eAaPmAgMnS	odejít
na	na	k7c4	na
odpočinek	odpočinek	k1gInSc4	odpočinek
do	do	k7c2	do
Stratfordu	Stratford	k1gInSc2	Stratford
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zemřel	zemřít	k5eAaPmAgMnS	zemřít
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Dochovalo	dochovat	k5eAaPmAgNnS	dochovat
se	se	k3xPyFc4	se
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
málo	málo	k4c4	málo
záznamů	záznam	k1gInPc2	záznam
ze	z	k7c2	z
Shakespearova	Shakespearův	k2eAgInSc2d1	Shakespearův
soukromého	soukromý	k2eAgInSc2d1	soukromý
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
podnítilo	podnítit	k5eAaPmAgNnS	podnítit
značné	značný	k2eAgFnPc4d1	značná
spekulace	spekulace	k1gFnPc4	spekulace
o	o	k7c6	o
takových	takový	k3xDgFnPc6	takový
věcech	věc	k1gFnPc6	věc
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
jeho	on	k3xPp3gInSc4	on
fyzický	fyzický	k2eAgInSc4d1	fyzický
vzhled	vzhled	k1gInSc4	vzhled
<g/>
,	,	kIx,	,
sexuální	sexuální	k2eAgNnSc4d1	sexuální
zaměření	zaměření	k1gNnSc4	zaměření
a	a	k8xC	a
náboženské	náboženský	k2eAgNnSc4d1	náboženské
přesvědčení	přesvědčení	k1gNnSc4	přesvědčení
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
zda	zda	k8xS	zda
práce	práce	k1gFnPc1	práce
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
jemu	on	k3xPp3gMnSc3	on
přisuzovány	přisuzovat	k5eAaImNgFnP	přisuzovat
<g/>
,	,	kIx,	,
nebyly	být	k5eNaImAgFnP	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
napsány	napsán	k2eAgInPc1d1	napsán
někým	někdo	k3yInSc7	někdo
jiným	jiný	k2eAgNnSc7d1	jiné
<g/>
.	.	kIx.	.
</s>
<s>
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
většinu	většina	k1gFnSc4	většina
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
známých	známý	k2eAgFnPc2d1	známá
prací	práce	k1gFnPc2	práce
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1589	[number]	k4	1589
a	a	k8xC	a
1613	[number]	k4	1613
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
jeho	jeho	k3xOp3gFnPc1	jeho
rané	raný	k2eAgFnPc1d1	raná
hry	hra	k1gFnPc1	hra
byly	být	k5eAaImAgFnP	být
komedie	komedie	k1gFnPc4	komedie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalším	další	k2eAgNnSc6d1	další
období	období	k1gNnSc6	období
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
mnoho	mnoho	k6eAd1	mnoho
her	hra	k1gFnPc2	hra
na	na	k7c4	na
historické	historický	k2eAgInPc4d1	historický
náměty	námět	k1gInPc4	námět
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
anglických	anglický	k2eAgFnPc2d1	anglická
dějin	dějiny	k1gFnPc2	dějiny
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
např.	např.	kA	např.
velké	velký	k2eAgNnSc1d1	velké
drama	drama	k1gNnSc1	drama
Julius	Julius	k1gMnSc1	Julius
Caesar	Caesar	k1gMnSc1	Caesar
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
asi	asi	k9	asi
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1608	[number]	k4	1608
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
psal	psát	k5eAaImAgInS	psát
své	svůj	k3xOyFgFnPc4	svůj
slavné	slavný	k2eAgFnPc4d1	slavná
tragédie	tragédie	k1gFnPc4	tragédie
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
Hamlet	Hamlet	k1gMnSc1	Hamlet
<g/>
,	,	kIx,	,
Othello	Othello	k1gMnSc1	Othello
<g/>
,	,	kIx,	,
Král	Král	k1gMnSc1	Král
Lear	Lear	k1gMnSc1	Lear
a	a	k8xC	a
Macbeth	Macbetha	k1gFnPc2	Macbetha
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgMnPc4d1	považován
za	za	k7c4	za
vrcholná	vrcholný	k2eAgNnPc4d1	vrcholné
díla	dílo	k1gNnPc4	dílo
anglickojazyčné	anglickojazyčný	k2eAgFnSc2d1	anglickojazyčná
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
fázi	fáze	k1gFnSc6	fáze
vytvářel	vytvářet	k5eAaImAgMnS	vytvářet
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
jinými	jiný	k2eAgMnPc7d1	jiný
autory	autor	k1gMnPc7	autor
tragikomedie	tragikomedie	k1gFnSc2	tragikomedie
<g/>
,	,	kIx,	,
známé	známý	k2eAgNnSc1d1	známé
též	též	k9	též
jako	jako	k8xC	jako
romance	romance	k1gFnSc1	romance
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
ze	z	k7c2	z
Shakespearových	Shakespearových	k2eAgFnPc2d1	Shakespearových
her	hra	k1gFnPc2	hra
bylo	být	k5eAaImAgNnS	být
publikováno	publikovat	k5eAaBmNgNnS	publikovat
již	již	k9	již
během	během	k7c2	během
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
ve	v	k7c6	v
vydáních	vydání	k1gNnPc6	vydání
různé	různý	k2eAgFnSc2d1	různá
kvality	kvalita	k1gFnSc2	kvalita
a	a	k8xC	a
s	s	k7c7	s
různou	různý	k2eAgFnSc7d1	různá
přesností	přesnost	k1gFnSc7	přesnost
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1623	[number]	k4	1623
John	John	k1gMnSc1	John
Heminges	Heminges	k1gMnSc1	Heminges
a	a	k8xC	a
Henry	Henry	k1gMnSc1	Henry
Condell	Condell	k1gMnSc1	Condell
<g/>
,	,	kIx,	,
dva	dva	k4xCgMnPc1	dva
Shakespearovi	Shakespearův	k2eAgMnPc1d1	Shakespearův
přátelé	přítel	k1gMnPc1	přítel
a	a	k8xC	a
herečtí	herecký	k2eAgMnPc1d1	herecký
kolegové	kolega	k1gMnPc1	kolega
<g/>
,	,	kIx,	,
publikovali	publikovat	k5eAaBmAgMnP	publikovat
takzvané	takzvaný	k2eAgNnSc4d1	takzvané
První	první	k4xOgNnSc4	první
folio	folio	k1gNnSc4	folio
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc1	první
konečné	konečný	k2eAgNnSc1d1	konečné
a	a	k8xC	a
souhrnné	souhrnný	k2eAgNnSc1d1	souhrnné
znění	znění	k1gNnSc1	znění
Shakespearových	Shakespearových	k2eAgFnPc2d1	Shakespearových
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
posmrtné	posmrtný	k2eAgNnSc1d1	posmrtné
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
až	až	k9	až
na	na	k7c4	na
dvě	dva	k4xCgNnPc4	dva
všechny	všechen	k3xTgFnPc4	všechen
hry	hra	k1gFnPc4	hra
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
připisované	připisovaný	k2eAgInPc1d1	připisovaný
Shakespearovi	Shakespearovi	k1gRnPc1	Shakespearovi
<g/>
.	.	kIx.	.
</s>
<s>
Předmluvou	předmluva	k1gFnSc7	předmluva
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
vydání	vydání	k1gNnSc3	vydání
byla	být	k5eAaImAgFnS	být
poema	poema	k1gFnSc1	poema
od	od	k7c2	od
Bena	Bena	k?	Bena
Jonsona	Jonsona	k1gFnSc1	Jonsona
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
je	být	k5eAaImIp3nS	být
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
prozíravě	prozíravě	k6eAd1	prozíravě
oslavován	oslavován	k2eAgMnSc1d1	oslavován
jako	jako	k8xS	jako
člověk	člověk	k1gMnSc1	člověk
"	"	kIx"	"
<g/>
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
navěky	navěky	k6eAd1	navěky
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
počínajícím	počínající	k2eAgInPc3d1	počínající
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgFnP	být
Shakespearovy	Shakespearův	k2eAgFnPc1d1	Shakespearova
práce	práce	k1gFnPc1	práce
opakovaně	opakovaně	k6eAd1	opakovaně
znovuobjevovány	znovuobjevován	k2eAgInPc4d1	znovuobjevován
a	a	k8xC	a
upravovány	upravován	k2eAgInPc4d1	upravován
na	na	k7c6	na
podkladě	podklad	k1gInSc6	podklad
nových	nový	k2eAgInPc2d1	nový
směrů	směr	k1gInPc2	směr
bádání	bádání	k1gNnSc2	bádání
a	a	k8xC	a
inscenování	inscenování	k1gNnSc2	inscenování
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
hry	hra	k1gFnPc1	hra
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgFnPc1d1	populární
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
neustále	neustále	k6eAd1	neustále
studovány	studován	k2eAgInPc4d1	studován
<g/>
,	,	kIx,	,
uváděny	uváděn	k2eAgInPc4d1	uváděn
a	a	k8xC	a
interpretovány	interpretován	k2eAgInPc4d1	interpretován
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
kulturních	kulturní	k2eAgFnPc6d1	kulturní
a	a	k8xC	a
politických	politický	k2eAgFnPc6d1	politická
souvislostech	souvislost	k1gFnPc6	souvislost
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
uplynulo	uplynout	k5eAaPmAgNnS	uplynout
400	[number]	k4	400
let	léto	k1gNnPc2	léto
od	od	k7c2	od
dramatikova	dramatikův	k2eAgNnSc2d1	dramatikovo
úmrtí	úmrtí	k1gNnSc2	úmrtí
<g/>
,	,	kIx,	,
probíhaly	probíhat	k5eAaImAgFnP	probíhat
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
oslavy	oslava	k1gFnSc2	oslava
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
a	a	k8xC	a
jeho	on	k3xPp3gNnSc2	on
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
William	William	k6eAd1	William
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
a	a	k8xC	a
vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
v	v	k7c6	v
městečku	městečko	k1gNnSc6	městečko
Stratford	Stratforda	k1gFnPc2	Stratforda
nad	nad	k7c7	nad
Avonou	Avona	k1gFnSc7	Avona
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
synem	syn	k1gMnSc7	syn
Johna	John	k1gMnSc2	John
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
<g/>
,	,	kIx,	,
úspěšného	úspěšný	k2eAgMnSc2d1	úspěšný
rukavičkáře	rukavičkář	k1gMnSc2	rukavičkář
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
radního	radní	k2eAgNnSc2d1	radní
města	město	k1gNnSc2	město
Stratfordu	Stratford	k1gInSc2	Stratford
<g/>
,	,	kIx,	,
pocházejícího	pocházející	k2eAgInSc2d1	pocházející
ze	z	k7c2	z
Snitterfieldu	Snitterfield	k1gInSc2	Snitterfield
<g/>
,	,	kIx,	,
a	a	k8xC	a
Mary	Mary	k1gFnSc1	Mary
Ardenové	Ardenová	k1gFnSc2	Ardenová
<g/>
,	,	kIx,	,
dcery	dcera	k1gFnSc2	dcera
bohatého	bohatý	k2eAgMnSc2d1	bohatý
velkostatkáře	velkostatkář	k1gMnSc2	velkostatkář
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Stratfordu	Stratford	k1gInSc6	Stratford
bydlela	bydlet	k5eAaImAgFnS	bydlet
rodina	rodina	k1gFnSc1	rodina
v	v	k7c4	v
Henley	Henlea	k1gFnPc4	Henlea
Street	Streeta	k1gFnPc2	Streeta
<g/>
.	.	kIx.	.
</s>
<s>
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
ve	v	k7c6	v
Stratfordu	Stratford	k1gInSc6	Stratford
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
pokřtěn	pokřtít	k5eAaPmNgInS	pokřtít
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1564	[number]	k4	1564
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jeho	jeho	k3xOp3gInSc4	jeho
datum	datum	k1gInSc4	datum
narození	narození	k1gNnSc2	narození
je	být	k5eAaImIp3nS	být
některými	některý	k3yIgMnPc7	některý
autory	autor	k1gMnPc7	autor
považován	považován	k2eAgInSc4d1	považován
23	[number]	k4	23
<g/>
.	.	kIx.	.
duben	duben	k1gInSc1	duben
<g/>
,	,	kIx,	,
den	den	k1gInSc1	den
Svatého	svatý	k2eAgMnSc2d1	svatý
Jiří	Jiří	k1gMnSc2	Jiří
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
zvykem	zvyk	k1gInSc7	zvyk
křtít	křtít	k5eAaImF	křtít
chlapce	chlapec	k1gMnPc4	chlapec
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
po	po	k7c6	po
jejich	jejich	k3xOp3gNnSc6	jejich
narození	narození	k1gNnSc6	narození
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
však	však	k9	však
spekulace	spekulace	k1gFnPc1	spekulace
<g/>
,	,	kIx,	,
že	že	k8xS	že
badatel	badatel	k1gMnSc1	badatel
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
toto	tento	k3xDgNnSc4	tento
datum	datum	k1gNnSc4	datum
uvedl	uvést	k5eAaPmAgMnS	uvést
jako	jako	k9	jako
první	první	k4xOgInSc4	první
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgMnS	moct
zmýlit	zmýlit	k5eAaPmF	zmýlit
(	(	kIx(	(
<g/>
protože	protože	k8xS	protože
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
zemřel	zemřít	k5eAaPmAgMnS	zemřít
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1616	[number]	k4	1616
<g/>
.	.	kIx.	.
</s>
<s>
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
jako	jako	k9	jako
třetí	třetí	k4xOgNnSc4	třetí
dítě	dítě	k1gNnSc4	dítě
z	z	k7c2	z
osmi	osm	k4xCc2	osm
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
nejstarším	starý	k2eAgMnSc7d3	nejstarší
synem	syn	k1gMnSc7	syn
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
přežil	přežít	k5eAaPmAgMnS	přežít
do	do	k7c2	do
dospělosti	dospělost	k1gFnSc2	dospělost
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
nedochovaly	dochovat	k5eNaPmAgInP	dochovat
žádné	žádný	k3yNgInPc1	žádný
záznamy	záznam	k1gInPc1	záznam
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
životopisců	životopisec	k1gMnPc2	životopisec
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
rodném	rodný	k2eAgNnSc6d1	rodné
městě	město	k1gNnSc6	město
gymnázium	gymnázium	k1gNnSc1	gymnázium
(	(	kIx(	(
<g/>
King	King	k1gMnSc1	King
Edward	Edward	k1gMnSc1	Edward
VI	VI	kA	VI
Grammar	Grammar	k1gInSc1	Grammar
School	School	k1gInSc1	School
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
roku	rok	k1gInSc2	rok
1553	[number]	k4	1553
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
královny	královna	k1gFnSc2	královna
Alžběty	Alžběta	k1gFnSc2	Alžběta
I.	I.	kA	I.
měla	mít	k5eAaImAgFnS	mít
gymnázia	gymnázium	k1gNnSc2	gymnázium
různou	různý	k2eAgFnSc4d1	různá
kvalitu	kvalita	k1gFnSc4	kvalita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
osnovy	osnova	k1gFnPc1	osnova
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Anglii	Anglie	k1gFnSc6	Anglie
určoval	určovat	k5eAaImAgInS	určovat
zákon	zákon	k1gInSc1	zákon
a	a	k8xC	a
školy	škola	k1gFnPc1	škola
měly	mít	k5eAaImAgFnP	mít
poskytovat	poskytovat	k5eAaImF	poskytovat
intenzivní	intenzivní	k2eAgFnSc4d1	intenzivní
výuku	výuka	k1gFnSc4	výuka
latiny	latina	k1gFnSc2	latina
a	a	k8xC	a
klasického	klasický	k2eAgNnSc2d1	klasické
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
26	[number]	k4	26
<g/>
letou	letý	k2eAgFnSc7d1	letá
Anne	Anne	k1gFnSc7	Anne
Hathaway	Hathawaa	k1gFnSc2	Hathawaa
<g/>
.	.	kIx.	.
</s>
<s>
Povolení	povolení	k1gNnSc2	povolení
k	k	k7c3	k
jejich	jejich	k3xOp3gInSc3	jejich
sňatku	sňatek	k1gInSc3	sňatek
vydal	vydat	k5eAaPmAgMnS	vydat
církevní	církevní	k2eAgInSc4d1	církevní
soud	soud	k1gInSc4	soud
diecéze	diecéze	k1gFnSc2	diecéze
ve	v	k7c6	v
Worcesteru	Worcester	k1gInSc6	Worcester
dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1582	[number]	k4	1582
<g/>
.	.	kIx.	.
</s>
<s>
Obřad	obřad	k1gInSc1	obřad
byl	být	k5eAaImAgInS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
připraven	připravit	k5eAaPmNgInS	připravit
ve	v	k7c6	v
spěchu	spěch	k1gInSc6	spěch
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
kancléř	kancléř	k1gMnSc1	kancléř
diecéze	diecéze	k1gFnSc2	diecéze
povolil	povolit	k5eAaPmAgMnS	povolit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
ohlášky	ohláška	k1gFnPc1	ohláška
četly	číst	k5eAaImAgFnP	číst
pouze	pouze	k6eAd1	pouze
jednou	jeden	k4xCgFnSc7	jeden
namísto	namísto	k7c2	namísto
třikrát	třikrát	k6eAd1	třikrát
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
bylo	být	k5eAaImAgNnS	být
zvykem	zvyk	k1gInSc7	zvyk
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
spěšné	spěšný	k2eAgFnSc2d1	spěšná
svatby	svatba	k1gFnSc2	svatba
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
těhotenství	těhotenství	k1gNnSc1	těhotenství
Anne	Ann	k1gInSc2	Ann
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
šest	šest	k4xCc4	šest
měsíců	měsíc	k1gInPc2	měsíc
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
dcera	dcera	k1gFnSc1	dcera
Susanna	Susanna	k1gFnSc1	Susanna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
pokřtěna	pokřtěn	k2eAgFnSc1d1	pokřtěna
26	[number]	k4	26
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1583	[number]	k4	1583
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
dvě	dva	k4xCgFnPc1	dva
děti	dítě	k1gFnPc1	dítě
byla	být	k5eAaImAgNnP	být
dvojčata	dvojče	k1gNnPc1	dvojče
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Hamnet	Hamnet	k1gMnSc1	Hamnet
a	a	k8xC	a
dcera	dcera	k1gFnSc1	dcera
Judith	Juditha	k1gFnPc2	Juditha
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
necelé	celý	k2eNgInPc4d1	necelý
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
poté	poté	k6eAd1	poté
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
pokřtěna	pokřtěn	k2eAgFnSc1d1	pokřtěna
dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1585	[number]	k4	1585
<g/>
.	.	kIx.	.
</s>
<s>
Hamnet	Hamnet	k1gMnSc1	Hamnet
však	však	k9	však
zemřel	zemřít	k5eAaPmAgMnS	zemřít
z	z	k7c2	z
neznámých	známý	k2eNgFnPc2d1	neznámá
příčin	příčina	k1gFnPc2	příčina
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
jedenácti	jedenáct	k4xCc2	jedenáct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
možná	možná	k9	možná
poznamenalo	poznamenat	k5eAaPmAgNnS	poznamenat
Shakespearovu	Shakespearův	k2eAgFnSc4d1	Shakespearova
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Hamnetův	Hamnetův	k2eAgInSc1d1	Hamnetův
pohřeb	pohřeb	k1gInSc1	pohřeb
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
11	[number]	k4	11
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1596	[number]	k4	1596
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
narození	narození	k1gNnSc6	narození
dvojčat	dvojče	k1gNnPc2	dvojče
Hamneta	Hamneto	k1gNnSc2	Hamneto
a	a	k8xC	a
Judithy	Juditha	k1gFnSc2	Juditha
následuje	následovat	k5eAaImIp3nS	následovat
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgInSc2	který
o	o	k7c4	o
Shakespearovi	Shakespearův	k2eAgMnPc1d1	Shakespearův
nemáme	mít	k5eNaImIp1nP	mít
prakticky	prakticky	k6eAd1	prakticky
žádné	žádný	k3yNgFnPc4	žádný
ověřené	ověřený	k2eAgFnPc4d1	ověřená
informace	informace	k1gFnPc4	informace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
Shakespearova	Shakespearův	k2eAgInSc2d1	Shakespearův
života	život	k1gInSc2	život
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1585	[number]	k4	1585
a	a	k8xC	a
1592	[number]	k4	1592
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
ztracená	ztracený	k2eAgNnPc4d1	ztracené
léta	léto	k1gNnPc4	léto
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Životopisci	životopisec	k1gMnPc1	životopisec
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
toto	tento	k3xDgNnSc4	tento
období	období	k1gNnSc4	období
mnoha	mnoho	k4c7	mnoho
spornými	sporný	k2eAgInPc7d1	sporný
příběhy	příběh	k1gInPc7	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Nicholas	Nicholas	k1gMnSc1	Nicholas
Rowe	Row	k1gInSc2	Row
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
životopisec	životopisec	k1gMnSc1	životopisec
zabývající	zabývající	k2eAgMnPc1d1	zabývající
se	s	k7c7	s
Shakespearem	Shakespeare	k1gMnSc7	Shakespeare
<g/>
,	,	kIx,	,
popsal	popsat	k5eAaPmAgMnS	popsat
zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1709	[number]	k4	1709
stratfordskou	stratfordský	k2eAgFnSc4d1	stratfordský
pověst	pověst	k1gFnSc4	pověst
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yIgFnSc2	který
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
uprchl	uprchnout	k5eAaPmAgInS	uprchnout
ze	z	k7c2	z
Stratfordu	Stratford	k1gInSc2	Stratford
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vyhnul	vyhnout	k5eAaPmAgMnS	vyhnout
stíhání	stíhání	k1gNnSc4	stíhání
kvůli	kvůli	k7c3	kvůli
pytláctví	pytláctví	k1gNnSc3	pytláctví
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiného	jiný	k2eAgInSc2d1	jiný
příběhu	příběh	k1gInSc2	příběh
sepsaného	sepsaný	k2eAgInSc2d1	sepsaný
v	v	k7c4	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začal	začít	k5eAaPmAgMnS	začít
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
svou	svůj	k3xOyFgFnSc4	svůj
divadelní	divadelní	k2eAgFnSc4d1	divadelní
kariéru	kariéra	k1gFnSc4	kariéra
hlídáním	hlídání	k1gNnPc3	hlídání
koní	kůň	k1gMnPc2	kůň
návštěvníků	návštěvník	k1gMnPc2	návštěvník
londýnského	londýnský	k2eAgNnSc2d1	Londýnské
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Anglický	anglický	k2eAgMnSc1d1	anglický
starožitník	starožitník	k1gMnSc1	starožitník
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
John	John	k1gMnSc1	John
Aubrey	Aubrea	k1gFnSc2	Aubrea
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
byl	být	k5eAaImAgMnS	být
učitelem	učitel	k1gMnSc7	učitel
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
vědců	vědec	k1gMnPc2	vědec
ve	v	k7c4	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
přišlo	přijít	k5eAaPmAgNnS	přijít
s	s	k7c7	s
teorií	teorie	k1gFnSc7	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
zaměstnán	zaměstnat	k5eAaPmNgMnS	zaměstnat
jako	jako	k9	jako
učitel	učitel	k1gMnSc1	učitel
Alexandrem	Alexandr	k1gMnSc7	Alexandr
Hoghtonem	Hoghton	k1gInSc7	Hoghton
z	z	k7c2	z
hrabství	hrabství	k1gNnSc2	hrabství
Lancashire	Lancashir	k1gInSc5	Lancashir
<g/>
,	,	kIx,	,
katolickým	katolický	k2eAgMnSc7d1	katolický
statkářem	statkář	k1gMnSc7	statkář
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
jistého	jistý	k2eAgMnSc2d1	jistý
"	"	kIx"	"
<g/>
Williama	William	k1gMnSc2	William
Shakeshafte	Shakeshaft	k1gInSc5	Shakeshaft
<g/>
"	"	kIx"	"
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
závěti	závěť	k1gFnSc6	závěť
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
pověstí	pověst	k1gFnPc2	pověst
shromážděných	shromážděný	k2eAgFnPc2d1	shromážděná
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
není	být	k5eNaImIp3nS	být
žádný	žádný	k3yNgInSc1	žádný
důkaz	důkaz	k1gInSc1	důkaz
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
podkládal	podkládat	k5eAaImAgInS	podkládat
tyto	tento	k3xDgInPc4	tento
příběhy	příběh	k1gInPc4	příběh
<g/>
,	,	kIx,	,
a	a	k8xC	a
jméno	jméno	k1gNnSc1	jméno
Shakeshafte	Shakeshaft	k1gInSc5	Shakeshaft
bylo	být	k5eAaImAgNnS	být
celkem	celkem	k6eAd1	celkem
běžné	běžný	k2eAgNnSc1d1	běžné
v	v	k7c6	v
hrabství	hrabství	k1gNnSc6	hrabství
Lancashire	Lancashir	k1gInSc5	Lancashir
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
přesně	přesně	k6eAd1	přesně
se	se	k3xPyFc4	se
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
a	a	k8xC	a
kdy	kdy	k6eAd1	kdy
zde	zde	k6eAd1	zde
začala	začít	k5eAaPmAgFnS	začít
jeho	jeho	k3xOp3gFnSc1	jeho
divadelní	divadelní	k2eAgFnSc1d1	divadelní
kariéra	kariéra	k1gFnSc1	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
životě	život	k1gInSc6	život
se	se	k3xPyFc4	se
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
málo	málo	k4c1	málo
informací	informace	k1gFnPc2	informace
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
vyřčeno	vyřknout	k5eAaPmNgNnS	vyřknout
mnoho	mnoho	k4c1	mnoho
spekulací	spekulace	k1gFnPc2	spekulace
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
o	o	k7c6	o
skutečném	skutečný	k2eAgNnSc6d1	skutečné
autorství	autorství	k1gNnSc6	autorství
jeho	jeho	k3xOp3gFnPc2	jeho
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Neví	vědět	k5eNaImIp3nS	vědět
se	se	k3xPyFc4	se
ani	ani	k9	ani
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
přesně	přesně	k6eAd1	přesně
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
začal	začít	k5eAaPmAgMnS	začít
psát	psát	k5eAaImF	psát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dobové	dobový	k2eAgInPc1d1	dobový
odkazy	odkaz	k1gInPc1	odkaz
a	a	k8xC	a
záznamy	záznam	k1gInPc1	záznam
vystoupení	vystoupení	k1gNnSc2	vystoupení
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
několik	několik	k4yIc1	několik
jeho	jeho	k3xOp3gFnPc2	jeho
her	hra	k1gFnPc2	hra
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
londýnské	londýnský	k2eAgFnSc6d1	londýnská
scéně	scéna	k1gFnSc6	scéna
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1592	[number]	k4	1592
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
kritika	kritika	k1gFnSc1	kritika
vyslovená	vyslovený	k2eAgFnSc1d1	vyslovená
dramatikem	dramatik	k1gMnSc7	dramatik
Robertem	Robert	k1gMnSc7	Robert
Greenem	Green	k1gInSc7	Green
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
tiskovině	tiskovina	k1gFnSc6	tiskovina
vydané	vydaný	k2eAgNnSc4d1	vydané
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
...	...	k?	...
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
jedna	jeden	k4xCgFnSc1	jeden
čerstvě	čerstvě	k6eAd1	čerstvě
povýšená	povýšený	k2eAgFnSc1d1	povýšená
Vrána	vrána	k1gFnSc1	vrána
<g/>
,	,	kIx,	,
ozdobená	ozdobený	k2eAgFnSc1d1	ozdobená
naším	náš	k3xOp1gMnSc7	náš
peřím	peřit	k5eAaImIp1nS	peřit
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
si	se	k3xPyFc3	se
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
tygřím	tygří	k2eAgNnSc7d1	tygří
srdcem	srdce	k1gNnSc7	srdce
skrytým	skrytý	k2eAgNnSc7d1	skryté
v	v	k7c6	v
kůži	kůže	k1gFnSc6	kůže
herce	herec	k1gMnSc2	herec
namýšlí	namýšlet	k5eAaImIp3nS	namýšlet
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
že	že	k8xS	že
umí	umět	k5eAaImIp3nS	umět
nadouvat	nadouvat	k5eAaImF	nadouvat
blankvers	blankvers	k1gInSc1	blankvers
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
vy	vy	k3xPp2nPc1	vy
všichni	všechen	k3xTgMnPc1	všechen
<g/>
,	,	kIx,	,
a	a	k8xC	a
jako	jako	k9	jako
naprostý	naprostý	k2eAgInSc4d1	naprostý
Johannes	Johannes	k1gInSc4	Johannes
Fac	Fac	k1gFnSc1	Fac
Totum	Totum	k1gInSc1	Totum
se	se	k3xPyFc4	se
pokládá	pokládat	k5eAaImIp3nS	pokládat
za	za	k7c4	za
jediného	jediný	k2eAgMnSc4d1	jediný
scénotřasa	scénotřas	k1gMnSc4	scénotřas
(	(	kIx(	(
<g/>
shake-scene	shakecen	k1gInSc5	shake-scen
<g/>
)	)	kIx)	)
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Literární	literární	k2eAgMnPc1d1	literární
badatelé	badatel	k1gMnPc1	badatel
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
liší	lišit	k5eAaImIp3nP	lišit
v	v	k7c6	v
názorech	názor	k1gInPc6	názor
<g/>
,	,	kIx,	,
jaký	jaký	k3yRgInSc1	jaký
byl	být	k5eAaImAgInS	být
přesný	přesný	k2eAgInSc4d1	přesný
smysl	smysl	k1gInSc4	smysl
znění	znění	k1gNnSc3	znění
Greenových	Greenová	k1gFnPc2	Greenová
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
shoduje	shodovat	k5eAaImIp3nS	shodovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Greene	Green	k1gInSc5	Green
vyčítal	vyčítat	k5eAaImAgInS	vyčítat
Shakespearovi	Shakespearův	k2eAgMnPc1d1	Shakespearův
snahu	snaha	k1gFnSc4	snaha
být	být	k5eAaImF	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
autora	autor	k1gMnSc4	autor
stejné	stejný	k2eAgFnSc2d1	stejná
úrovně	úroveň	k1gFnSc2	úroveň
jako	jako	k8xC	jako
spisovatelé	spisovatel	k1gMnPc1	spisovatel
s	s	k7c7	s
univerzitním	univerzitní	k2eAgNnSc7d1	univerzitní
vzděláním	vzdělání	k1gNnSc7	vzdělání
<g/>
,	,	kIx,	,
kterými	který	k3yIgFnPc7	který
byli	být	k5eAaImAgMnP	být
například	například	k6eAd1	například
Christopher	Christophra	k1gFnPc2	Christophra
Marlowe	Marlowe	k1gInSc1	Marlowe
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
Nashe	Nash	k1gFnSc2	Nash
a	a	k8xC	a
sám	sám	k3xTgInSc1	sám
Greene	Green	k1gInSc5	Green
<g/>
.	.	kIx.	.
</s>
<s>
Fráze	fráze	k1gFnSc1	fráze
zapsaná	zapsaný	k2eAgFnSc1d1	zapsaná
kurzívou	kurzíva	k1gFnSc7	kurzíva
(	(	kIx(	(
<g/>
tygřím	tygří	k2eAgNnSc7d1	tygří
srdcem	srdce	k1gNnSc7	srdce
skrytým	skrytý	k2eAgNnSc7d1	skryté
v	v	k7c6	v
kůži	kůže	k1gFnSc6	kůže
herce	herec	k1gMnSc2	herec
<g/>
,	,	kIx,	,
v	v	k7c6	v
originálu	originál	k1gInSc6	originál
Tiger	Tigra	k1gFnPc2	Tigra
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
heart	heart	k1gInSc1	heart
wrapped	wrapped	k1gInSc4	wrapped
in	in	k?	in
a	a	k8xC	a
Player	Playero	k1gNnPc2	Playero
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
hide	hide	k1gNnSc7	hide
<g/>
)	)	kIx)	)
paroduje	parodovat	k5eAaImIp3nS	parodovat
verš	verš	k1gInSc1	verš
"	"	kIx"	"
<g/>
Oh	oh	k0	oh
<g/>
,	,	kIx,	,
tiger	tiger	k1gMnSc1	tiger
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
heart	heart	k1gInSc1	heart
wrapped	wrapped	k1gInSc4	wrapped
in	in	k?	in
a	a	k8xC	a
woman	woman	k1gInSc1	woman
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
hide	hide	k1gNnSc7	hide
<g/>
"	"	kIx"	"
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
zvané	zvaný	k2eAgFnSc2d1	zvaná
The	The	k1gFnSc2	The
True	Tru	k1gFnSc2	Tru
Tragedie	tragedie	k1gFnSc2	tragedie
of	of	k?	of
Richard	Richard	k1gMnSc1	Richard
<g/>
,	,	kIx,	,
Duke	Duke	k1gFnSc1	Duke
of	of	k?	of
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
&	&	k?	&
<g/>
c.	c.	k?	c.
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
převzal	převzít	k5eAaPmAgMnS	převzít
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
hry	hra	k1gFnSc2	hra
Jindřich	Jindřich	k1gMnSc1	Jindřich
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgFnSc1	třetí
část	část	k1gFnSc1	část
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
fráze	fráze	k1gFnSc1	fráze
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
se	s	k7c7	s
slovní	slovní	k2eAgFnSc7d1	slovní
hříčkou	hříčka	k1gFnSc7	hříčka
"	"	kIx"	"
<g/>
shake-scene	shakecen	k1gMnSc5	shake-scen
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
důkaz	důkaz	k1gInSc4	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
cílem	cíl	k1gInSc7	cíl
Greenových	Greenův	k2eAgFnPc2d1	Greenova
narážek	narážka	k1gFnPc2	narážka
byl	být	k5eAaImAgMnS	být
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
<g/>
.	.	kIx.	.
</s>
<s>
Greenův	Greenův	k2eAgInSc1d1	Greenův
útok	útok	k1gInSc1	útok
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc7	první
zmínkou	zmínka	k1gFnSc7	zmínka
o	o	k7c6	o
Shakespearově	Shakespearův	k2eAgFnSc6d1	Shakespearova
divadelní	divadelní	k2eAgFnSc6d1	divadelní
kariéře	kariéra	k1gFnSc6	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Životopisci	životopisec	k1gMnPc1	životopisec
se	se	k3xPyFc4	se
dohadují	dohadovat	k5eAaImIp3nP	dohadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
kariéra	kariéra	k1gFnSc1	kariéra
mohla	moct	k5eAaImAgFnS	moct
začít	začít	k5eAaPmF	začít
kdykoliv	kdykoliv	k6eAd1	kdykoliv
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
do	do	k7c2	do
Greenovy	Greenův	k2eAgFnSc2d1	Greenova
zmínky	zmínka	k1gFnSc2	zmínka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1593	[number]	k4	1593
až	až	k9	až
1594	[number]	k4	1594
byla	být	k5eAaImAgFnS	být
londýnská	londýnský	k2eAgNnPc1d1	Londýnské
divadla	divadlo	k1gNnPc4	divadlo
zavřená	zavřený	k2eAgNnPc4d1	zavřené
kvůli	kvůli	k7c3	kvůli
epidemii	epidemie	k1gFnSc3	epidemie
moru	mor	k1gInSc2	mor
<g/>
,	,	kIx,	,
a	a	k8xC	a
tehdy	tehdy	k6eAd1	tehdy
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
psal	psát	k5eAaImAgMnS	psát
poezii	poezie	k1gFnSc4	poezie
pod	pod	k7c7	pod
ochranou	ochrana	k1gFnSc7	ochrana
hraběte	hrabě	k1gMnSc2	hrabě
ze	z	k7c2	z
Southamptonu	Southampton	k1gInSc2	Southampton
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
epidemii	epidemie	k1gFnSc6	epidemie
se	se	k3xPyFc4	se
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
nové	nový	k2eAgFnSc2d1	nová
divadelní	divadelní	k2eAgFnSc2d1	divadelní
společnosti	společnost	k1gFnSc2	společnost
Služebníci	služebník	k1gMnPc1	služebník
lorda	lord	k1gMnSc2	lord
komořího	komoří	k1gMnSc2	komoří
(	(	kIx(	(
<g/>
Lord	lord	k1gMnSc1	lord
Chamberlain	Chamberlain	k1gMnSc1	Chamberlain
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Men	Men	k1gFnSc7	Men
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
působil	působit	k5eAaImAgMnS	působit
jako	jako	k8xS	jako
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
Služebníci	služebník	k1gMnPc1	služebník
stali	stát	k5eAaPmAgMnP	stát
přední	přední	k2eAgFnSc7d1	přední
dramatickou	dramatický	k2eAgFnSc7d1	dramatická
společností	společnost	k1gFnSc7	společnost
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
královny	královna	k1gFnSc2	královna
Alžběty	Alžběta	k1gFnSc2	Alžběta
I.	I.	kA	I.
roku	rok	k1gInSc2	rok
1603	[number]	k4	1603
získala	získat	k5eAaPmAgFnS	získat
tato	tento	k3xDgFnSc1	tento
společnost	společnost	k1gFnSc1	společnost
od	od	k7c2	od
nového	nový	k2eAgMnSc2d1	nový
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
Stuartovce	Stuartovec	k1gMnSc2	Stuartovec
Jakuba	Jakub	k1gMnSc2	Jakub
I.	I.	kA	I.
<g/>
,	,	kIx,	,
privilegium	privilegium	k1gNnSc1	privilegium
a	a	k8xC	a
název	název	k1gInSc1	název
společnosti	společnost	k1gFnSc2	společnost
byl	být	k5eAaImAgInS	být
změněn	změnit	k5eAaPmNgInS	změnit
na	na	k7c6	na
Královská	královský	k2eAgFnSc1d1	královská
společnost	společnost	k1gFnSc1	společnost
(	(	kIx(	(
<g/>
King	King	k1gMnSc1	King
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Men	Men	k1gFnSc7	Men
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
jemu	on	k3xPp3gMnSc3	on
připisovaných	připisovaný	k2eAgFnPc2d1	připisovaná
her	hra	k1gFnPc2	hra
napsal	napsat	k5eAaPmAgMnS	napsat
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
asi	asi	k9	asi
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1590	[number]	k4	1590
<g/>
–	–	k?	–
<g/>
1604	[number]	k4	1604
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
psal	psát	k5eAaImAgMnS	psát
hlavně	hlavně	k9	hlavně
komedie	komedie	k1gFnSc2	komedie
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
také	také	k9	také
historické	historický	k2eAgFnSc2d1	historická
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
byly	být	k5eAaImAgInP	být
žánry	žánr	k1gInPc1	žánr
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
vyzdvihl	vyzdvihnout	k5eAaPmAgMnS	vyzdvihnout
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1598	[number]	k4	1598
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
rychlém	rychlý	k2eAgInSc6d1	rychlý
sledu	sled	k1gInSc6	sled
uveřejňovány	uveřejňovat	k5eAaImNgInP	uveřejňovat
v	v	k7c6	v
tištěné	tištěný	k2eAgFnSc6d1	tištěná
podobě	podoba	k1gFnSc6	podoba
převážně	převážně	k6eAd1	převážně
jeho	jeho	k3xOp3gFnSc2	jeho
tragédie	tragédie	k1gFnSc2	tragédie
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
slavných	slavný	k2eAgFnPc2d1	slavná
her	hra	k1gFnPc2	hra
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
Hamlet	Hamlet	k1gMnSc1	Hamlet
<g/>
,	,	kIx,	,
Král	Král	k1gMnSc1	Král
Lear	Lear	k1gMnSc1	Lear
a	a	k8xC	a
Macbeth	Macbeth	k1gMnSc1	Macbeth
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1623	[number]	k4	1623
<g/>
,	,	kIx,	,
sedm	sedm	k4xCc1	sedm
let	léto	k1gNnPc2	léto
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Williama	William	k1gMnSc2	William
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
ze	z	k7c2	z
Stratfordu	Stratford	k1gInSc2	Stratford
<g/>
,	,	kIx,	,
vydali	vydat	k5eAaPmAgMnP	vydat
dva	dva	k4xCgInPc4	dva
jeho	jeho	k3xOp3gFnSc4	jeho
divadelní	divadelní	k2eAgMnPc1d1	divadelní
přátelé	přítel	k1gMnPc1	přítel
s	s	k7c7	s
finanční	finanční	k2eAgFnSc7d1	finanční
a	a	k8xC	a
morální	morální	k2eAgFnSc7d1	morální
podporou	podpora	k1gFnSc7	podpora
příbuzných	příbuzná	k1gFnPc2	příbuzná
Edwarda	Edward	k1gMnSc2	Edward
de	de	k?	de
Vere	Ver	k1gMnSc2	Ver
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
.	.	kIx.	.
hraběte	hrabě	k1gMnSc4	hrabě
z	z	k7c2	z
Oxfordu	Oxford	k1gInSc2	Oxford
<g/>
,	,	kIx,	,
takzvané	takzvaný	k2eAgNnSc4d1	takzvané
První	první	k4xOgNnSc4	první
folio	folio	k1gNnSc4	folio
<g/>
,	,	kIx,	,
slavnou	slavný	k2eAgFnSc4d1	slavná
kolekci	kolekce	k1gFnSc4	kolekce
všech	všecek	k3xTgFnPc2	všecek
Shakespearových	Shakespearových	k2eAgFnPc2d1	Shakespearových
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
zhruba	zhruba	k6eAd1	zhruba
20	[number]	k4	20
děl	dělo	k1gNnPc2	dělo
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nepublikovaných	publikovaný	k2eNgMnPc2d1	nepublikovaný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1599	[number]	k4	1599
se	se	k3xPyFc4	se
společnost	společnost	k1gFnSc1	společnost
Lord	lord	k1gMnSc1	lord
Chamberlain	Chamberlain	k1gMnSc1	Chamberlain
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Men	Men	k1gFnSc7	Men
přemístila	přemístit	k5eAaPmAgFnS	přemístit
do	do	k7c2	do
divadla	divadlo	k1gNnSc2	divadlo
Globe	globus	k1gInSc5	globus
a	a	k8xC	a
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
vlastníkem	vlastník	k1gMnSc7	vlastník
jedné	jeden	k4xCgFnSc2	jeden
její	její	k3xOp3gFnSc2	její
desetiny	desetina	k1gFnSc2	desetina
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
byl	být	k5eAaImAgInS	být
výrazně	výrazně	k6eAd1	výrazně
lépe	dobře	k6eAd2	dobře
zajištěn	zajištěn	k2eAgInSc1d1	zajištěn
–	–	k?	–
předtím	předtím	k6eAd1	předtím
údajně	údajně	k6eAd1	údajně
dostával	dostávat	k5eAaImAgMnS	dostávat
za	za	k7c4	za
hru	hra	k1gFnSc4	hra
asi	asi	k9	asi
6	[number]	k4	6
liber	libra	k1gFnPc2	libra
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
roku	rok	k1gInSc2	rok
1613	[number]	k4	1613
divadlo	divadlo	k1gNnSc4	divadlo
Globe	globus	k1gInSc5	globus
vyhořelo	vyhořet	k5eAaPmAgNnS	vyhořet
<g/>
,	,	kIx,	,
přesunula	přesunout	k5eAaPmAgFnS	přesunout
se	se	k3xPyFc4	se
společnost	společnost	k1gFnSc1	společnost
do	do	k7c2	do
divadla	divadlo	k1gNnSc2	divadlo
Blackfriars	Blackfriarsa	k1gFnPc2	Blackfriarsa
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
Shakespearovy	Shakespearův	k2eAgFnPc1d1	Shakespearova
hry	hra	k1gFnPc1	hra
byly	být	k5eAaImAgFnP	být
vydány	vydat	k5eAaPmNgFnP	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1594	[number]	k4	1594
v	v	k7c6	v
knižních	knižní	k2eAgFnPc6d1	knižní
edicích	edice	k1gFnPc6	edice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1598	[number]	k4	1598
jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
získalo	získat	k5eAaPmAgNnS	získat
značný	značný	k2eAgInSc4d1	značný
věhlas	věhlas	k1gInSc4	věhlas
a	a	k8xC	a
začalo	začít	k5eAaPmAgNnS	začít
se	se	k3xPyFc4	se
objevovat	objevovat	k5eAaImF	objevovat
na	na	k7c6	na
titulních	titulní	k2eAgFnPc6d1	titulní
stranách	strana	k1gFnPc6	strana
vydání	vydání	k1gNnSc2	vydání
autorových	autorův	k2eAgFnPc2d1	autorova
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
však	však	k9	však
pod	pod	k7c7	pod
formou	forma	k1gFnSc7	forma
jména	jméno	k1gNnSc2	jméno
"	"	kIx"	"
<g/>
Shake-Speare	Shake-Spear	k1gMnSc5	Shake-Spear
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
forma	forma	k1gFnSc1	forma
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
také	také	k9	také
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
vydání	vydání	k1gNnSc6	vydání
slavných	slavný	k2eAgInPc2d1	slavný
Sonetů	sonet	k1gInPc2	sonet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1609	[number]	k4	1609
<g/>
.	.	kIx.	.
</s>
<s>
Absence	absence	k1gFnSc1	absence
Shakespearova	Shakespearův	k2eAgNnSc2d1	Shakespearovo
jména	jméno	k1gNnSc2	jméno
v	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
účinkujících	účinkující	k1gMnPc2	účinkující
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
Bena	Bena	k?	Bena
Jonsona	Jonsona	k1gFnSc1	Jonsona
Volpone	Volpon	k1gInSc5	Volpon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1605	[number]	k4	1605
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
badatelů	badatel	k1gMnPc2	badatel
náznakem	náznak	k1gInSc7	náznak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
již	již	k9	již
jeho	jeho	k3xOp3gFnSc1	jeho
herecká	herecký	k2eAgFnSc1d1	herecká
kariéra	kariéra	k1gFnSc1	kariéra
blížila	blížit	k5eAaImAgFnS	blížit
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
úmrtním	úmrtní	k2eAgInSc6d1	úmrtní
roce	rok	k1gInSc6	rok
1616	[number]	k4	1616
byl	být	k5eAaImAgMnS	být
však	však	k9	však
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
jmenován	jmenován	k2eAgMnSc1d1	jmenován
v	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
účinkujících	účinkující	k1gMnPc2	účinkující
Jonsonových	Jonsonův	k2eAgFnPc2d1	Jonsonova
her	hra	k1gFnPc2	hra
Every	Evera	k1gFnSc2	Evera
Man	Man	k1gMnSc1	Man
in	in	k?	in
His	his	k1gNnSc2	his
Humour	Humoura	k1gFnPc2	Humoura
a	a	k8xC	a
Sejanus	Sejanus	k1gInSc1	Sejanus
<g/>
,	,	kIx,	,
His	his	k1gNnSc1	his
Fall	Falla	k1gFnPc2	Falla
při	při	k7c6	při
souhrnném	souhrnný	k2eAgNnSc6d1	souhrnné
vydání	vydání	k1gNnSc6	vydání
díla	dílo	k1gNnSc2	dílo
tohoto	tento	k3xDgMnSc2	tento
dramatika	dramatik	k1gMnSc2	dramatik
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
souhrnné	souhrnný	k2eAgNnSc1d1	souhrnné
vydání	vydání	k1gNnSc1	vydání
jeho	jeho	k3xOp3gFnPc2	jeho
vlastních	vlastní	k2eAgFnPc2d1	vlastní
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
První	první	k4xOgNnSc1	první
folio	folio	k1gNnSc1	folio
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1623	[number]	k4	1623
<g/>
,	,	kIx,	,
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Shakespeara	Shakespeare	k1gMnSc4	Shakespeare
jako	jako	k8xS	jako
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgMnPc2d1	hlavní
účinkujících	účinkující	k1gMnPc2	účinkující
při	při	k7c6	při
představeních	představení	k1gNnPc6	představení
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
některé	některý	k3yIgInPc1	některý
ze	z	k7c2	z
Shakespearových	Shakespearová	k1gFnPc2	Shakespearová
her	hra	k1gFnPc2	hra
poprvé	poprvé	k6eAd1	poprvé
hrány	hrát	k5eAaImNgInP	hrát
až	až	k9	až
po	po	k7c6	po
Jonsonově	Jonsonův	k2eAgNnSc6d1	Jonsonův
dramatu	drama	k1gNnSc6	drama
Volpone	Volpon	k1gMnSc5	Volpon
<g/>
.	.	kIx.	.
</s>
<s>
Nemůžeme	moct	k5eNaImIp1nP	moct
tedy	tedy	k9	tedy
přesně	přesně	k6eAd1	přesně
určit	určit	k5eAaPmF	určit
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
role	role	k1gFnPc4	role
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
vlastně	vlastně	k9	vlastně
hrál	hrát	k5eAaImAgMnS	hrát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1610	[number]	k4	1610
John	John	k1gMnSc1	John
Davies	Davies	k1gMnSc1	Davies
z	z	k7c2	z
Herefordu	Hereford	k1gInSc2	Hereford
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
dobrý	dobrý	k2eAgInSc1d1	dobrý
Will	Will	k1gInSc1	Will
<g/>
"	"	kIx"	"
hrál	hrát	k5eAaImAgInS	hrát
"	"	kIx"	"
<g/>
královské	královský	k2eAgFnPc4d1	královská
<g/>
"	"	kIx"	"
role	role	k1gFnPc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1709	[number]	k4	1709
se	se	k3xPyFc4	se
Rowe	Rowe	k1gNnSc2	Rowe
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
hrál	hrát	k5eAaImAgMnS	hrát
ducha	duch	k1gMnSc4	duch
Hamletova	Hamletův	k2eAgMnSc4d1	Hamletův
otce	otec	k1gMnSc4	otec
v	v	k7c6	v
dramatu	drama	k1gNnSc6	drama
Hamlet	Hamlet	k1gMnSc1	Hamlet
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
pozdějšího	pozdní	k2eAgInSc2d2	pozdější
názoru	názor	k1gInSc2	názor
hrál	hrát	k5eAaImAgMnS	hrát
také	také	k6eAd1	také
Adama	Adam	k1gMnSc4	Adam
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
vám	vy	k3xPp2nPc3	vy
líbí	líbit	k5eAaImIp3nS	líbit
<g/>
.	.	kIx.	.
</s>
<s>
Badatelé	badatel	k1gMnPc1	badatel
mají	mít	k5eAaImIp3nP	mít
určité	určitý	k2eAgFnPc4d1	určitá
pochyby	pochyba	k1gFnPc4	pochyba
o	o	k7c6	o
správnosti	správnost	k1gFnSc6	správnost
těchto	tento	k3xDgFnPc2	tento
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
a	a	k8xC	a
ve	v	k7c6	v
Stratfordu	Stratford	k1gInSc6	Stratford
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1596	[number]	k4	1596
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
rok	rok	k1gInSc4	rok
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
koupil	koupit	k5eAaPmAgMnS	koupit
New	New	k1gFnSc6	New
Place	plac	k1gInSc6	plac
<g/>
,	,	kIx,	,
nový	nový	k2eAgInSc1d1	nový
domov	domov	k1gInSc1	domov
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
rodinu	rodina	k1gFnSc4	rodina
ve	v	k7c6	v
Stratfordu	Stratford	k1gInSc6	Stratford
<g/>
,	,	kIx,	,
žil	žít	k5eAaImAgMnS	žít
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
na	na	k7c6	na
farnosti	farnost	k1gFnSc6	farnost
Svaté	svatý	k2eAgFnSc2d1	svatá
Heleny	Helena	k1gFnSc2	Helena
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Bishopsgate	Bishopsgat	k1gMnSc5	Bishopsgat
<g/>
,	,	kIx,	,
severně	severně	k6eAd1	severně
od	od	k7c2	od
Temže	Temže	k1gFnSc2	Temže
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1599	[number]	k4	1599
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
břeh	břeh	k1gInSc4	břeh
řeky	řeka	k1gFnSc2	řeka
do	do	k7c2	do
Southwarku	Southwark	k1gInSc2	Southwark
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
jeho	jeho	k3xOp3gFnSc1	jeho
společnost	společnost	k1gFnSc1	společnost
postavila	postavit	k5eAaPmAgFnS	postavit
divadlo	divadlo	k1gNnSc4	divadlo
Globe	globus	k1gInSc5	globus
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1604	[number]	k4	1604
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
nazpět	nazpět	k6eAd1	nazpět
na	na	k7c4	na
severní	severní	k2eAgInSc4d1	severní
břeh	břeh	k1gInSc4	břeh
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
si	se	k3xPyFc3	se
pronajal	pronajmout	k5eAaPmAgMnS	pronajmout
byt	byt	k1gInSc4	byt
od	od	k7c2	od
francouzského	francouzský	k2eAgMnSc2d1	francouzský
hugenota	hugenot	k1gMnSc2	hugenot
jménem	jméno	k1gNnSc7	jméno
Christopher	Christophra	k1gFnPc2	Christophra
Mountjoy	Mountjoa	k1gMnSc2	Mountjoa
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
výrobcem	výrobce	k1gMnSc7	výrobce
dámských	dámský	k2eAgFnPc2d1	dámská
paruk	paruka	k1gFnPc2	paruka
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
pokrývek	pokrývka	k1gFnPc2	pokrývka
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Shakespearových	Shakespearových	k2eAgInPc2d1	Shakespearových
nákupů	nákup	k1gInPc2	nákup
a	a	k8xC	a
investic	investice	k1gFnPc2	investice
můžeme	moct	k5eAaImIp1nP	moct
usoudit	usoudit	k5eAaPmF	usoudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
stal	stát	k5eAaPmAgMnS	stát
vcelku	vcelku	k6eAd1	vcelku
bohatý	bohatý	k2eAgMnSc1d1	bohatý
muž	muž	k1gMnSc1	muž
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1597	[number]	k4	1597
koupil	koupit	k5eAaPmAgMnS	koupit
druhý	druhý	k4xOgInSc4	druhý
největší	veliký	k2eAgInSc4d3	veliký
dům	dům	k1gInSc4	dům
ve	v	k7c6	v
Stratfordu	Stratford	k1gInSc6	Stratford
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
New	New	k1gFnSc6	New
Place	plac	k1gInSc6	plac
<g/>
,	,	kIx,	,
a	a	k8xC	a
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1613	[number]	k4	1613
se	se	k3xPyFc4	se
dochovala	dochovat	k5eAaPmAgFnS	dochovat
smlouva	smlouva	k1gFnSc1	smlouva
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
dvěma	dva	k4xCgInPc7	dva
podpisy	podpis	k1gInPc7	podpis
na	na	k7c6	na
koupi	koupě	k1gFnSc6	koupě
domu	dům	k1gInSc2	dům
v	v	k7c6	v
londýnském	londýnský	k2eAgNnSc6d1	Londýnské
předměstí	předměstí	k1gNnSc6	předměstí
Blackfriars	Blackfriarsa	k1gFnPc2	Blackfriarsa
<g/>
.	.	kIx.	.
</s>
<s>
Shakespearova	Shakespearův	k2eAgFnSc1d1	Shakespearova
dramatická	dramatický	k2eAgFnSc1d1	dramatická
tvorba	tvorba	k1gFnSc1	tvorba
však	však	k9	však
skončila	skončit	k5eAaPmAgFnS	skončit
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1613	[number]	k4	1613
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
za	za	k7c7	za
manželkou	manželka	k1gFnSc7	manželka
a	a	k8xC	a
dcerami	dcera	k1gFnPc7	dcera
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
rodného	rodný	k2eAgNnSc2d1	rodné
městečka	městečko	k1gNnSc2	městečko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zůstal	zůstat	k5eAaPmAgInS	zůstat
až	až	k9	až
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnPc1d1	poslední
tři	tři	k4xCgFnPc4	tři
hry	hra	k1gFnPc4	hra
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
nenapsal	napsat	k5eNaPmAgMnS	napsat
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
na	na	k7c6	na
nich	on	k3xPp3gInPc6	on
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
s	s	k7c7	s
Johnem	John	k1gMnSc7	John
Fletcherem	Fletcher	k1gMnSc7	Fletcher
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1613	[number]	k4	1613
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
rodného	rodný	k2eAgNnSc2d1	rodné
městečka	městečko	k1gNnSc2	městečko
<g/>
.	.	kIx.	.
</s>
<s>
Nicholas	Nicholas	k1gMnSc1	Nicholas
Rowe	Rowe	k1gInSc4	Rowe
byl	být	k5eAaImAgMnS	být
první	první	k4xOgMnSc1	první
životopisec	životopisec	k1gMnSc1	životopisec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
několik	několik	k4yIc4	několik
let	let	k1gInSc4	let
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
odešel	odejít	k5eAaPmAgMnS	odejít
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Stratfordu	Stratford	k1gInSc2	Stratford
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
nadále	nadále	k6eAd1	nadále
sporadicky	sporadicky	k6eAd1	sporadicky
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
Londýn	Londýn	k1gInSc1	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1613	[number]	k4	1613
koupil	koupit	k5eAaPmAgInS	koupit
strážní	strážní	k2eAgInSc1d1	strážní
domek	domek	k1gInSc1	domek
v	v	k7c6	v
Blackfriarském	Blackfriarský	k2eAgNnSc6d1	Blackfriarský
převorství	převorství	k1gNnSc6	převorství
a	a	k8xC	a
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1614	[number]	k4	1614
byl	být	k5eAaImAgMnS	být
několik	několik	k4yIc1	několik
týdnů	týden	k1gInPc2	týden
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
zetěm	zeť	k1gMnSc7	zeť
Johnem	John	k1gMnSc7	John
Hallem	Hall	k1gMnSc7	Hall
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
Shakespearova	Shakespearův	k2eAgFnSc1d1	Shakespearova
starší	starý	k2eAgFnSc1d2	starší
dcera	dcera	k1gFnSc1	dcera
Susanna	Susann	k1gInSc2	Susann
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1607	[number]	k4	1607
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c4	za
lékaře	lékař	k1gMnSc4	lékař
Johna	John	k1gMnSc4	John
Halla	Hall	k1gMnSc4	Hall
<g/>
.	.	kIx.	.
</s>
<s>
Mladší	mladý	k2eAgFnSc1d2	mladší
dcera	dcera	k1gFnSc1	dcera
Judith	Juditha	k1gFnPc2	Juditha
se	se	k3xPyFc4	se
dva	dva	k4xCgInPc1	dva
měsíce	měsíc	k1gInPc1	měsíc
před	před	k7c7	před
Shakespearovou	Shakespearův	k2eAgFnSc7d1	Shakespearova
smrtí	smrt	k1gFnSc7	smrt
vdala	vdát	k5eAaPmAgFnS	vdát
za	za	k7c2	za
Thomase	Thomas	k1gMnSc2	Thomas
Quineyho	Quiney	k1gMnSc2	Quiney
<g/>
,	,	kIx,	,
obchodníka	obchodník	k1gMnSc2	obchodník
s	s	k7c7	s
vínem	víno	k1gNnSc7	víno
<g/>
.	.	kIx.	.
</s>
<s>
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
zemřel	zemřít	k5eAaPmAgMnS	zemřít
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1616	[number]	k4	1616
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
poslední	poslední	k2eAgFnSc6d1	poslední
vůli	vůle	k1gFnSc6	vůle
zanechal	zanechat	k5eAaPmAgInS	zanechat
své	svůj	k3xOyFgNnSc4	svůj
starší	starý	k2eAgInSc1d2	starší
dceři	dcera	k1gFnSc3	dcera
Susanně	Susanně	k1gFnSc2	Susanně
značné	značný	k2eAgNnSc4d1	značné
množství	množství	k1gNnSc4	množství
majetku	majetek	k1gInSc2	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Podmínkou	podmínka	k1gFnSc7	podmínka
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jej	on	k3xPp3gMnSc4	on
předala	předat	k5eAaPmAgFnS	předat
nedotčený	dotčený	k2eNgInSc1d1	nedotčený
svému	svůj	k3xOyFgInSc3	svůj
prvnímu	první	k4xOgNnSc3	první
synovi	syn	k1gMnSc3	syn
<g/>
.	.	kIx.	.
</s>
<s>
Shakespearova	Shakespearův	k2eAgFnSc1d1	Shakespearova
manželka	manželka	k1gFnSc1	manželka
<g/>
,	,	kIx,	,
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
většinou	většinou	k6eAd1	většinou
zvaná	zvaný	k2eAgNnPc1d1	zvané
Anne	Anne	k1gNnPc1	Anne
Hathaway	Hathawaa	k1gFnSc2	Hathawaa
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
dochované	dochovaný	k2eAgFnSc6d1	dochovaná
závěti	závěť	k1gFnSc6	závěť
zmíněna	zmíněn	k2eAgFnSc1d1	zmíněna
jen	jen	k6eAd1	jen
jako	jako	k8xC	jako
dědička	dědička	k1gFnSc1	dědička
"	"	kIx"	"
<g/>
druhé	druhý	k4xOgFnSc2	druhý
nejlepší	dobrý	k2eAgFnSc2d3	nejlepší
postele	postel	k1gFnSc2	postel
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Manželé	manžel	k1gMnPc1	manžel
Hallovi	Hallův	k2eAgMnPc1d1	Hallův
měli	mít	k5eAaImAgMnP	mít
jen	jen	k9	jen
jedno	jeden	k4xCgNnSc4	jeden
dítě	dítě	k1gNnSc4	dítě
<g/>
,	,	kIx,	,
dceru	dcera	k1gFnSc4	dcera
Alžbětu	Alžběta	k1gFnSc4	Alžběta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
dvakrát	dvakrát	k6eAd1	dvakrát
provdala	provdat	k5eAaPmAgFnS	provdat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zemřela	zemřít	k5eAaPmAgFnS	zemřít
bezdětná	bezdětný	k2eAgFnSc1d1	bezdětná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1670	[number]	k4	1670
<g/>
.	.	kIx.	.
</s>
<s>
Quineyovi	Quineya	k1gMnSc3	Quineya
měli	mít	k5eAaImAgMnP	mít
tři	tři	k4xCgFnPc4	tři
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
všechny	všechen	k3xTgFnPc1	všechen
zemřely	zemřít	k5eAaPmAgFnP	zemřít
bez	bez	k7c2	bez
potomků	potomek	k1gMnPc2	potomek
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1670	[number]	k4	1670
Shakespearův	Shakespearův	k2eAgInSc1d1	Shakespearův
rod	rod	k1gInSc1	rod
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
byl	být	k5eAaImAgMnS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
před	před	k7c7	před
oltářem	oltář	k1gInSc7	oltář
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
"	"	kIx"	"
<g/>
Holy	hola	k1gFnSc2	hola
Trinity	Trinita	k1gFnSc2	Trinita
Church	Church	k1gMnSc1	Church
<g/>
"	"	kIx"	"
ve	v	k7c6	v
Stratfordu	Stratford	k1gInSc6	Stratford
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Možná	možná	k9	možná
již	již	k6eAd1	již
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1623	[number]	k4	1623
byl	být	k5eAaImAgInS	být
pořízen	pořídit	k5eAaPmNgInS	pořídit
jeho	jeho	k3xOp3gInSc1	jeho
památník	památník	k1gInSc1	památník
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgMnSc6	který
byl	být	k5eAaImAgMnS	být
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
až	až	k9	až
později	pozdě	k6eAd2	pozdě
vyobrazen	vyobrazit	k5eAaPmNgInS	vyobrazit
v	v	k7c6	v
gestu	gest	k1gInSc6	gest
psaní	psaní	k1gNnSc2	psaní
<g/>
.	.	kIx.	.
</s>
<s>
Plaketa	plaketa	k1gFnSc1	plaketa
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
jej	on	k3xPp3gMnSc4	on
srovnává	srovnávat	k5eAaImIp3nS	srovnávat
s	s	k7c7	s
postavou	postava	k1gFnSc7	postava
řecké	řecký	k2eAgFnSc2d1	řecká
mytologie	mytologie	k1gFnSc2	mytologie
Nestorem	Nestor	k1gMnSc7	Nestor
<g/>
,	,	kIx,	,
filozofem	filozof	k1gMnSc7	filozof
Sokratem	Sokrates	k1gMnSc7	Sokrates
a	a	k8xC	a
básníkem	básník	k1gMnSc7	básník
Vergiliem	Vergilius	k1gMnSc7	Vergilius
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
se	se	k3xPyFc4	se
žádným	žádný	k3yNgFnPc3	žádný
tehdy	tehdy	k6eAd1	tehdy
známým	známý	k2eAgMnSc7d1	známý
slavným	slavný	k2eAgMnSc7d1	slavný
dramatikem	dramatik	k1gMnSc7	dramatik
<g/>
.	.	kIx.	.
</s>
<s>
Shakespearovi	Shakespeare	k1gMnSc3	Shakespeare
je	být	k5eAaImIp3nS	být
věnováno	věnovat	k5eAaPmNgNnS	věnovat
mnoho	mnoho	k4c1	mnoho
jiných	jiný	k2eAgInPc2d1	jiný
památníků	památník	k1gInPc2	památník
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
dramatiků	dramatik	k1gMnPc2	dramatik
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc1	období
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
okamžiku	okamžik	k1gInSc6	okamžik
své	svůj	k3xOyFgFnSc2	svůj
tvorby	tvorba	k1gFnSc2	tvorba
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
<g/>
,	,	kIx,	,
a	a	k8xC	a
kritici	kritik	k1gMnPc1	kritik
se	se	k3xPyFc4	se
shodují	shodovat	k5eAaImIp3nP	shodovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
dělal	dělat	k5eAaImAgMnS	dělat
totéž	týž	k3xTgNnSc4	týž
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
většinou	většinou	k6eAd1	většinou
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
přípisy	přípis	k1gInPc1	přípis
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Titus	Titus	k1gMnSc1	Titus
Andronicus	Andronicus	k1gMnSc1	Andronicus
a	a	k8xC	a
rané	raný	k2eAgFnPc4d1	raná
historické	historický	k2eAgFnPc4d1	historická
hry	hra	k1gFnPc4	hra
<g/>
,	,	kIx,	,
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
kontroverzní	kontroverzní	k2eAgInPc1d1	kontroverzní
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Dva	dva	k4xCgMnPc1	dva
vznešení	vznešený	k2eAgMnPc1d1	vznešený
příbuzní	příbuzný	k1gMnPc1	příbuzný
a	a	k8xC	a
ztracený	ztracený	k2eAgMnSc1d1	ztracený
Cardenio	Cardenio	k1gMnSc1	Cardenio
mají	mít	k5eAaImIp3nP	mít
dobře	dobře	k6eAd1	dobře
doloženou	doložený	k2eAgFnSc4d1	doložená
soudobou	soudobý	k2eAgFnSc4d1	soudobá
dokumentaci	dokumentace	k1gFnSc4	dokumentace
<g/>
.	.	kIx.	.
</s>
<s>
Textový	textový	k2eAgInSc1d1	textový
rozbor	rozbor	k1gInSc1	rozbor
rovněž	rovněž	k9	rovněž
podporuje	podporovat	k5eAaImIp3nS	podporovat
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
několik	několik	k4yIc1	několik
her	hra	k1gFnPc2	hra
bylo	být	k5eAaImAgNnS	být
revidováno	revidovat	k5eAaImNgNnS	revidovat
jinými	jiný	k2eAgMnPc7d1	jiný
spisovateli	spisovatel	k1gMnPc7	spisovatel
po	po	k7c6	po
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k8xS	co
byly	být	k5eAaImAgFnP	být
původně	původně	k6eAd1	původně
napsány	napsán	k2eAgFnPc1d1	napsána
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgNnPc1d3	nejstarší
dochovaná	dochovaný	k2eAgNnPc1d1	dochované
Shakespearova	Shakespearův	k2eAgNnPc1d1	Shakespearovo
díla	dílo	k1gNnPc1	dílo
jsou	být	k5eAaImIp3nP	být
Richard	Richard	k1gMnSc1	Richard
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
tři	tři	k4xCgInPc4	tři
díly	díl	k1gInPc4	díl
Jindřicha	Jindřich	k1gMnSc4	Jindřich
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
napsané	napsaný	k2eAgInPc1d1	napsaný
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
během	během	k7c2	během
módy	móda	k1gFnSc2	móda
historických	historický	k2eAgNnPc2d1	historické
dramat	drama	k1gNnPc2	drama
<g/>
.	.	kIx.	.
</s>
<s>
Shakespearovy	Shakespearův	k2eAgFnPc4d1	Shakespearova
hry	hra	k1gFnPc4	hra
je	být	k5eAaImIp3nS	být
náročné	náročný	k2eAgNnSc1d1	náročné
datovat	datovat	k5eAaImF	datovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
studie	studie	k1gFnPc1	studie
textů	text	k1gInPc2	text
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Titus	Titus	k1gMnSc1	Titus
Andronicus	Andronicus	k1gMnSc1	Andronicus
<g/>
,	,	kIx,	,
Komedie	komedie	k1gFnSc1	komedie
omylů	omyl	k1gInPc2	omyl
<g/>
,	,	kIx,	,
Zkrocení	zkrocení	k1gNnSc1	zkrocení
zlé	zlý	k2eAgFnSc2d1	zlá
ženy	žena	k1gFnSc2	žena
a	a	k8xC	a
Dva	dva	k4xCgMnPc1	dva
šlechtici	šlechtic	k1gMnPc1	šlechtic
z	z	k7c2	z
Verony	Verona	k1gFnSc2	Verona
také	také	k9	také
mohou	moct	k5eAaImIp3nP	moct
také	také	k9	také
patřit	patřit	k5eAaImF	patřit
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
nejranějšímu	raný	k2eAgNnSc3d3	nejranější
Shakespearovu	Shakespearův	k2eAgNnSc3d1	Shakespearovo
období	období	k1gNnSc3	období
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
první	první	k4xOgFnPc1	první
historické	historický	k2eAgFnPc1d1	historická
hry	hra	k1gFnPc1	hra
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
silně	silně	k6eAd1	silně
těží	těžet	k5eAaImIp3nP	těžet
z	z	k7c2	z
publikace	publikace	k1gFnSc2	publikace
Kronika	kronika	k1gFnSc1	kronika
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
Skotska	Skotsko	k1gNnSc2	Skotsko
a	a	k8xC	a
Irska	Irsko	k1gNnSc2	Irsko
Raphaela	Raphael	k1gMnSc2	Raphael
Holinsheda	Holinshed	k1gMnSc2	Holinshed
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1587	[number]	k4	1587
<g/>
,	,	kIx,	,
dramatizují	dramatizovat	k5eAaBmIp3nP	dramatizovat
destruktivní	destruktivní	k2eAgInPc1d1	destruktivní
následky	následek	k1gInPc1	následek
slabé	slabý	k2eAgFnSc2d1	slabá
či	či	k8xC	či
zkorumpované	zkorumpovaný	k2eAgFnSc2d1	zkorumpovaná
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
později	pozdě	k6eAd2	pozdě
interpretovány	interpretován	k2eAgFnPc1d1	interpretována
jako	jako	k8xS	jako
ospravedlnění	ospravedlnění	k1gNnSc1	ospravedlnění
pro	pro	k7c4	pro
nástup	nástup	k1gInSc4	nástup
dynastie	dynastie	k1gFnSc2	dynastie
Tudorovců	Tudorovec	k1gMnPc2	Tudorovec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
raných	raný	k2eAgFnPc6d1	raná
hrách	hra	k1gFnPc6	hra
byl	být	k5eAaImAgMnS	být
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
pracemi	práce	k1gFnPc7	práce
jiných	jiný	k2eAgMnPc2d1	jiný
alžbětinských	alžbětinský	k2eAgMnPc2d1	alžbětinský
dramatiků	dramatik	k1gMnPc2	dramatik
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Thomase	Thomas	k1gMnSc4	Thomas
Kyda	Kydus	k1gMnSc4	Kydus
a	a	k8xC	a
Christophera	Christopher	k1gMnSc4	Christopher
Marlowa	Marlowus	k1gMnSc4	Marlowus
<g/>
,	,	kIx,	,
tradicemi	tradice	k1gFnPc7	tradice
středověkého	středověký	k2eAgNnSc2d1	středověké
dramatu	drama	k1gNnSc2	drama
a	a	k8xC	a
Senecovými	Senecův	k2eAgFnPc7d1	Senecova
hrami	hra	k1gFnPc7	hra
<g/>
.	.	kIx.	.
</s>
<s>
Komedie	komedie	k1gFnSc1	komedie
omylů	omyl	k1gInPc2	omyl
také	také	k9	také
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
klasických	klasický	k2eAgInPc2d1	klasický
modelů	model	k1gInPc2	model
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
např.	např.	kA	např.
nebyl	být	k5eNaImAgInS	být
nalezen	naleznout	k5eAaPmNgInS	naleznout
žádný	žádný	k3yNgInSc1	žádný
zdroj	zdroj	k1gInSc1	zdroj
pro	pro	k7c4	pro
Zkrocení	zkrocení	k1gNnSc4	zkrocení
zlé	zlý	k2eAgFnSc2d1	zlá
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
ta	ten	k3xDgFnSc1	ten
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
jinou	jiný	k2eAgFnSc7d1	jiná
hrou	hra	k1gFnSc7	hra
stejného	stejný	k2eAgNnSc2d1	stejné
jména	jméno	k1gNnSc2	jméno
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
pocházet	pocházet	k5eAaImF	pocházet
ze	z	k7c2	z
lidového	lidový	k2eAgInSc2d1	lidový
příběhu	příběh	k1gInSc2	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Dva	dva	k4xCgMnPc1	dva
šlechtici	šlechtic	k1gMnPc1	šlechtic
z	z	k7c2	z
Verony	Verona	k1gFnSc2	Verona
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
dva	dva	k4xCgMnPc1	dva
kamarádi	kamarád	k1gMnPc1	kamarád
schvalující	schvalující	k2eAgMnPc1d1	schvalující
znásilnění	znásilnění	k1gNnSc2	znásilnění
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
Zkrocení	zkrocení	k1gNnSc4	zkrocení
zlé	zlý	k2eAgFnSc2d1	zlá
ženy	žena	k1gFnSc2	žena
je	být	k5eAaImIp3nS	být
příběh	příběh	k1gInSc1	příběh
o	o	k7c4	o
zkrocení	zkrocení	k1gNnSc4	zkrocení
ženy	žena	k1gFnSc2	žena
nezávislého	závislý	k2eNgMnSc2d1	nezávislý
ducha	duch	k1gMnSc2	duch
mužem	muž	k1gMnSc7	muž
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
někdy	někdy	k6eAd1	někdy
trápí	trápit	k5eAaImIp3nS	trápit
moderní	moderní	k2eAgMnPc4d1	moderní
kritiky	kritik	k1gMnPc4	kritik
a	a	k8xC	a
režiséry	režisér	k1gMnPc4	režisér
<g/>
.	.	kIx.	.
</s>
<s>
Shakespearovy	Shakespearův	k2eAgFnPc1d1	Shakespearova
časné	časný	k2eAgFnPc1d1	časná
klasické	klasický	k2eAgFnPc1d1	klasická
a	a	k8xC	a
italské	italský	k2eAgFnPc1d1	italská
komedie	komedie	k1gFnPc1	komedie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
úzké	úzký	k2eAgFnSc2d1	úzká
dvojité	dvojitý	k2eAgFnSc2d1	dvojitá
zápletky	zápletka	k1gFnSc2	zápletka
a	a	k8xC	a
přesné	přesný	k2eAgFnPc1d1	přesná
komické	komický	k2eAgFnPc1d1	komická
sekvence	sekvence	k1gFnPc1	sekvence
<g/>
,	,	kIx,	,
ustupují	ustupovat	k5eAaImIp3nP	ustupovat
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
romantické	romantický	k2eAgFnSc6d1	romantická
atmosféře	atmosféra	k1gFnSc6	atmosféra
jeho	jeho	k3xOp3gFnPc2	jeho
nejuznávanějších	uznávaný	k2eAgFnPc2d3	nejuznávanější
komedií	komedie	k1gFnPc2	komedie
<g/>
.	.	kIx.	.
</s>
<s>
Sen	sen	k1gInSc1	sen
noci	noc	k1gFnSc2	noc
svatojánské	svatojánský	k2eAgFnSc2d1	Svatojánská
je	být	k5eAaImIp3nS	být
vtipná	vtipný	k2eAgFnSc1d1	vtipná
směs	směs	k1gFnSc1	směs
romantiky	romantika	k1gFnSc2	romantika
<g/>
,	,	kIx,	,
pohádkové	pohádkový	k2eAgFnSc2d1	pohádková
magie	magie	k1gFnSc2	magie
a	a	k8xC	a
komických	komický	k2eAgFnPc2d1	komická
scén	scéna	k1gFnPc2	scéna
z	z	k7c2	z
podsvětí	podsvětí	k1gNnSc2	podsvětí
<g/>
.	.	kIx.	.
</s>
<s>
Shakespearova	Shakespearův	k2eAgFnSc1d1	Shakespearova
další	další	k2eAgFnSc1d1	další
<g/>
,	,	kIx,	,
neméně	málo	k6eNd2	málo
romantická	romantický	k2eAgFnSc1d1	romantická
komedie	komedie	k1gFnSc1	komedie
<g/>
,	,	kIx,	,
Kupec	kupec	k1gMnSc1	kupec
benátský	benátský	k2eAgMnSc1d1	benátský
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
ztvárnění	ztvárnění	k1gNnSc4	ztvárnění
pomstychtivého	pomstychtivý	k2eAgMnSc2d1	pomstychtivý
židovského	židovský	k2eAgMnSc2d1	židovský
lichváře	lichvář	k1gMnSc2	lichvář
Shylocka	Shylocko	k1gNnSc2	Shylocko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
odráží	odrážet	k5eAaImIp3nS	odrážet
alžbětinský	alžbětinský	k2eAgInSc1d1	alžbětinský
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
Židy	Žid	k1gMnPc4	Žid
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
moderní	moderní	k2eAgNnSc4d1	moderní
publikum	publikum	k1gNnSc4	publikum
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
zdát	zdát	k5eAaPmF	zdát
hanlivé	hanlivý	k2eAgNnSc1d1	hanlivé
<g/>
.	.	kIx.	.
</s>
<s>
Vtipy	vtip	k1gInPc1	vtip
a	a	k8xC	a
slovní	slovní	k2eAgFnPc1d1	slovní
hříčky	hříčka	k1gFnPc1	hříčka
z	z	k7c2	z
Mnoho	mnoho	k6eAd1	mnoho
povyku	povyk	k1gInSc2	povyk
pro	pro	k7c4	pro
nic	nic	k3yNnSc4	nic
<g/>
,	,	kIx,	,
okouzlující	okouzlující	k2eAgNnSc4d1	okouzlující
venkovské	venkovský	k2eAgNnSc4d1	venkovské
prostředí	prostředí	k1gNnSc4	prostředí
Jak	jak	k8xC	jak
se	se	k3xPyFc4	se
vám	vy	k3xPp2nPc3	vy
líbí	líbit	k5eAaImIp3nS	líbit
a	a	k8xC	a
živé	živý	k2eAgNnSc1d1	živé
veselí	veselí	k1gNnSc1	veselí
z	z	k7c2	z
Večeru	večer	k1gInSc3	večer
tříkrálového	tříkrálový	k2eAgNnSc2d1	Tříkrálové
kompletuje	kompletovat	k5eAaBmIp3nS	kompletovat
sled	sled	k1gInSc1	sled
Shakespearových	Shakespearových	k2eAgFnPc2d1	Shakespearových
velkých	velký	k2eAgFnPc2d1	velká
komedií	komedie	k1gFnPc2	komedie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
lyrickém	lyrický	k2eAgInSc6d1	lyrický
Richardu	Richard	k1gMnSc3	Richard
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
napsaném	napsaný	k2eAgNnSc6d1	napsané
téměř	téměř	k6eAd1	téměř
kompletně	kompletně	k6eAd1	kompletně
ve	v	k7c6	v
verších	verš	k1gInPc6	verš
<g/>
,	,	kIx,	,
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
v	v	k7c6	v
historických	historický	k2eAgFnPc6d1	historická
hrách	hra	k1gFnPc6	hra
pozdních	pozdní	k2eAgFnPc2d1	pozdní
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
představil	představit	k5eAaPmAgInS	představit
komediální	komediální	k2eAgFnSc4d1	komediální
prózu	próza	k1gFnSc4	próza
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
díla	dílo	k1gNnPc4	dílo
Jindřich	Jindřich	k1gMnSc1	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
1	[number]	k4	1
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
2	[number]	k4	2
a	a	k8xC	a
Jindřich	Jindřich	k1gMnSc1	Jindřich
V.	V.	kA	V.
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
postavy	postava	k1gFnPc1	postava
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
složitější	složitý	k2eAgInPc1d2	složitější
a	a	k8xC	a
citlivější	citlivý	k2eAgInPc1d2	citlivější
<g/>
,	,	kIx,	,
když	když	k8xS	když
obratně	obratně	k6eAd1	obratně
přepíná	přepínat	k5eAaImIp3nS	přepínat
mezi	mezi	k7c7	mezi
komickými	komický	k2eAgFnPc7d1	komická
a	a	k8xC	a
vážnými	vážný	k2eAgFnPc7d1	vážná
scénami	scéna	k1gFnPc7	scéna
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
prózou	próza	k1gFnSc7	próza
a	a	k8xC	a
poezií	poezie	k1gFnSc7	poezie
<g/>
,	,	kIx,	,
a	a	k8xC	a
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
tím	ten	k3xDgNnSc7	ten
příběhové	příběhový	k2eAgFnSc2d1	příběhová
rozmanitosti	rozmanitost	k1gFnSc3	rozmanitost
vyzrálého	vyzrálý	k2eAgNnSc2d1	vyzrálé
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
začíná	začínat	k5eAaImIp3nS	začínat
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
dvěma	dva	k4xCgFnPc7	dva
tragédiemi	tragédie	k1gFnPc7	tragédie
<g/>
.	.	kIx.	.
</s>
<s>
Romeo	Romeo	k1gMnSc1	Romeo
a	a	k8xC	a
Julie	Julie	k1gFnSc1	Julie
je	být	k5eAaImIp3nS	být
slavná	slavný	k2eAgFnSc1d1	slavná
romantická	romantický	k2eAgFnSc1d1	romantická
tragédie	tragédie	k1gFnSc1	tragédie
sexualitou	sexualita	k1gFnSc7	sexualita
nabitého	nabitý	k2eAgNnSc2d1	nabité
dospívání	dospívání	k1gNnSc2	dospívání
<g/>
,	,	kIx,	,
lásky	láska	k1gFnSc2	láska
a	a	k8xC	a
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Julius	Julius	k1gMnSc1	Julius
Caesar	Caesar	k1gMnSc1	Caesar
–	–	k?	–
založený	založený	k2eAgMnSc1d1	založený
na	na	k7c6	na
překladu	překlad	k1gInSc6	překlad
Plutarchových	Plutarchův	k2eAgInPc2d1	Plutarchův
Životopisů	životopis	k1gInPc2	životopis
slavných	slavný	k2eAgMnPc2d1	slavný
Řeků	Řek	k1gMnPc2	Řek
a	a	k8xC	a
Římanů	Říman	k1gMnPc2	Říman
Thomasem	Thomas	k1gMnSc7	Thomas
Northem	North	k1gInSc7	North
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1579	[number]	k4	1579
–	–	k?	–
zavádí	zavádět	k5eAaImIp3nS	zavádět
nový	nový	k2eAgInSc4d1	nový
druh	druh	k1gInSc4	druh
dramatu	drama	k1gNnSc2	drama
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
shakespearovského	shakespearovský	k2eAgMnSc2d1	shakespearovský
vědce	vědec	k1gMnSc2	vědec
Jamese	Jamese	k1gFnSc1	Jamese
Shapira	Shapira	k1gFnSc1	Shapira
v	v	k7c6	v
Juliu	Julius	k1gMnSc6	Julius
Caesarovi	Caesar	k1gMnSc6	Caesar
"	"	kIx"	"
<g/>
různé	různý	k2eAgInPc4d1	různý
prvky	prvek	k1gInPc4	prvek
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
charaktery	charakter	k1gInPc1	charakter
<g/>
,	,	kIx,	,
niternosti	niternost	k1gFnPc1	niternost
<g/>
,	,	kIx,	,
tehdejší	tehdejší	k2eAgMnPc1d1	tehdejší
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
i	i	k9	i
Shakespearovy	Shakespearův	k2eAgFnPc1d1	Shakespearova
vlastní	vlastní	k2eAgFnPc1d1	vlastní
reflexe	reflexe	k1gFnPc1	reflexe
o	o	k7c6	o
aktu	akt	k1gInSc6	akt
psaní	psaní	k1gNnSc2	psaní
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
navzájem	navzájem	k6eAd1	navzájem
prostupovat	prostupovat	k5eAaImF	prostupovat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
napsal	napsat	k5eAaPmAgMnS	napsat
takzvané	takzvaný	k2eAgNnSc4d1	takzvané
"	"	kIx"	"
<g/>
problémové	problémový	k2eAgFnSc2d1	problémová
hry	hra	k1gFnSc2	hra
<g/>
"	"	kIx"	"
Něco	něco	k3yInSc1	něco
za	za	k7c4	za
něco	něco	k3yInSc4	něco
<g/>
,	,	kIx,	,
Troilus	Troilus	k1gInSc1	Troilus
a	a	k8xC	a
Kressida	Kressida	k1gFnSc1	Kressida
<g/>
,	,	kIx,	,
Dobrý	dobrý	k2eAgInSc1d1	dobrý
konec	konec	k1gInSc1	konec
vše	všechen	k3xTgNnSc4	všechen
napraví	napravit	k5eAaPmIp3nS	napravit
a	a	k8xC	a
také	také	k9	také
několik	několik	k4yIc4	několik
svých	svůj	k3xOyFgFnPc2	svůj
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
tragédií	tragédie	k1gFnPc2	tragédie
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
kritiků	kritik	k1gMnPc2	kritik
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Shakespearovy	Shakespearův	k2eAgFnPc1d1	Shakespearova
největší	veliký	k2eAgFnPc1d3	veliký
tragédie	tragédie	k1gFnPc1	tragédie
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
vrchol	vrchol	k1gInSc4	vrchol
jeho	on	k3xPp3gNnSc2	on
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Titulární	titulární	k2eAgMnSc1d1	titulární
hrdina	hrdina	k1gMnSc1	hrdina
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
Shakespearových	Shakespearových	k2eAgFnPc2d1	Shakespearových
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
tragédií	tragédie	k1gFnPc2	tragédie
<g/>
,	,	kIx,	,
Hamlet	Hamlet	k1gMnSc1	Hamlet
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
diskutován	diskutovat	k5eAaImNgInS	diskutovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
kterákoliv	kterýkoliv	k3yIgFnSc1	kterýkoliv
jiná	jiný	k2eAgFnSc1d1	jiná
Shakespearova	Shakespearův	k2eAgFnSc1d1	Shakespearova
postava	postava	k1gFnSc1	postava
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
jeho	on	k3xPp3gInSc4	on
slavný	slavný	k2eAgInSc4d1	slavný
monolog	monolog	k1gInSc4	monolog
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
začíná	začínat	k5eAaImIp3nS	začínat
"	"	kIx"	"
<g/>
Být	být	k5eAaImF	být
či	či	k8xC	či
nebýt	být	k5eNaImF	být
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
otázka	otázka	k1gFnSc1	otázka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
introvertního	introvertní	k2eAgMnSc2d1	introvertní
Hamleta	Hamlet	k1gMnSc2	Hamlet
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
fatální	fatální	k2eAgFnSc7d1	fatální
chybou	chyba	k1gFnSc7	chyba
je	být	k5eAaImIp3nS	být
váhání	váhání	k1gNnSc1	váhání
<g/>
,	,	kIx,	,
hrdinové	hrdina	k1gMnPc1	hrdina
tragédií	tragédie	k1gFnPc2	tragédie
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
následovaly	následovat	k5eAaImAgInP	následovat
<g/>
,	,	kIx,	,
Othello	Othello	k1gMnSc1	Othello
a	a	k8xC	a
král	král	k1gMnSc1	král
Lear	Lear	k1gMnSc1	Lear
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zničeni	zničit	k5eAaPmNgMnP	zničit
ukvapenými	ukvapený	k2eAgFnPc7d1	ukvapená
chybami	chyba	k1gFnPc7	chyba
v	v	k7c6	v
úsudku	úsudek	k1gInSc6	úsudek
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
Shakespearových	Shakespearových	k2eAgFnPc2d1	Shakespearových
tragédií	tragédie	k1gFnPc2	tragédie
často	často	k6eAd1	často
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
fatálních	fatální	k2eAgFnPc6d1	fatální
chybách	chyba	k1gFnPc6	chyba
či	či	k8xC	či
nedostatcích	nedostatek	k1gInPc6	nedostatek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
převrátí	převrátit	k5eAaPmIp3nP	převrátit
řád	řád	k1gInSc4	řád
a	a	k8xC	a
zničí	zničit	k5eAaPmIp3nP	zničit
hrdinu	hrdina	k1gMnSc4	hrdina
a	a	k8xC	a
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
miluje	milovat	k5eAaImIp3nS	milovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Othellovi	Othello	k1gMnSc6	Othello
darebák	darebák	k1gMnSc1	darebák
Jago	Jaga	k1gFnSc5	Jaga
podněcuje	podněcovat	k5eAaImIp3nS	podněcovat
Othellovu	Othellův	k2eAgFnSc4d1	Othellova
žárlivost	žárlivost	k1gFnSc4	žárlivost
až	až	k9	až
do	do	k7c2	do
bodu	bod	k1gInSc2	bod
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zavraždí	zavraždit	k5eAaPmIp3nS	zavraždit
nevinnou	vinný	k2eNgFnSc4d1	nevinná
ženu	žena	k1gFnSc4	žena
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ho	on	k3xPp3gMnSc4	on
miluje	milovat	k5eAaImIp3nS	milovat
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
Králi	Král	k1gMnSc3	Král
Learovi	Lear	k1gMnSc3	Lear
se	se	k3xPyFc4	se
starý	starý	k2eAgMnSc1d1	starý
král	král	k1gMnSc1	král
dopouští	dopouštět	k5eAaImIp3nS	dopouštět
tragické	tragický	k2eAgFnSc2d1	tragická
chyby	chyba	k1gFnSc2	chyba
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vzdá	vzdát	k5eAaPmIp3nS	vzdát
svých	svůj	k3xOyFgFnPc2	svůj
pravomocí	pravomoc	k1gFnPc2	pravomoc
a	a	k8xC	a
uvede	uvést	k5eAaPmIp3nS	uvést
tak	tak	k6eAd1	tak
do	do	k7c2	do
pohybu	pohyb	k1gInSc2	pohyb
sled	sled	k1gInSc1	sled
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
mučení	mučení	k1gNnSc3	mučení
a	a	k8xC	a
oslepení	oslepení	k1gNnSc4	oslepení
hraběte	hrabě	k1gMnSc2	hrabě
z	z	k7c2	z
Gloucesteru	Gloucester	k1gInSc2	Gloucester
a	a	k8xC	a
k	k	k7c3	k
vraždě	vražda	k1gFnSc3	vražda
Learovy	Learův	k2eAgFnSc2d1	Learova
nejmladší	mladý	k2eAgFnSc2d3	nejmladší
dcery	dcera	k1gFnSc2	dcera
Cordelie	Cordelie	k1gFnSc2	Cordelie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
kritika	kritik	k1gMnSc2	kritik
Franka	Frank	k1gMnSc2	Frank
Kermoda	Kermod	k1gMnSc2	Kermod
"	"	kIx"	"
<g/>
hra	hra	k1gFnSc1	hra
nemá	mít	k5eNaImIp3nS	mít
kladné	kladný	k2eAgFnPc4d1	kladná
postavy	postava	k1gFnPc4	postava
ani	ani	k8xC	ani
nenabízí	nabízet	k5eNaImIp3nP	nabízet
publiku	publikum	k1gNnSc6	publikum
jakoukoliv	jakýkoliv	k3yIgFnSc4	jakýkoliv
úlevu	úleva	k1gFnSc4	úleva
od	od	k7c2	od
své	svůj	k3xOyFgFnSc2	svůj
krutosti	krutost	k1gFnSc2	krutost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Macbethovi	Macbeth	k1gMnSc6	Macbeth
<g/>
,	,	kIx,	,
nejkratší	krátký	k2eAgFnSc6d3	nejkratší
a	a	k8xC	a
nejzhuštěnější	zhuštěný	k2eAgFnSc6d3	zhuštěný
Shakespearově	Shakespearův	k2eAgFnSc6d1	Shakespearova
tragédií	tragédie	k1gFnSc7	tragédie
<g/>
,	,	kIx,	,
nezřízená	zřízený	k2eNgFnSc1d1	nezřízená
ctižádost	ctižádost	k1gFnSc1	ctižádost
dovede	dovést	k5eAaPmIp3nS	dovést
Macbetha	Macbeth	k1gMnSc4	Macbeth
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
manželku	manželka	k1gFnSc4	manželka
<g/>
,	,	kIx,	,
lady	lady	k1gFnSc4	lady
Macbeth	Macbetha	k1gFnPc2	Macbetha
<g/>
,	,	kIx,	,
k	k	k7c3	k
vraždě	vražda	k1gFnSc3	vražda
právoplatného	právoplatný	k2eAgMnSc2d1	právoplatný
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Trůn	trůn	k1gInSc1	trůn
uchvátí	uchvátit	k5eAaPmIp3nS	uchvátit
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	on	k3xPp3gMnPc4	on
zničí	zničit	k5eAaPmIp3nS	zničit
na	na	k7c4	na
oplátku	oplátka	k1gFnSc4	oplátka
jejich	jejich	k3xOp3gFnSc1	jejich
vlastní	vlastní	k2eAgFnSc1d1	vlastní
vina	vina	k1gFnSc1	vina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
hře	hra	k1gFnSc6	hra
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
přidává	přidávat	k5eAaImIp3nS	přidávat
ke	k	k7c3	k
tragické	tragický	k2eAgFnSc3d1	tragická
struktuře	struktura	k1gFnSc3	struktura
nadpřirozený	nadpřirozený	k2eAgInSc4d1	nadpřirozený
prvek	prvek	k1gInSc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
poslední	poslední	k2eAgFnPc1d1	poslední
velké	velká	k1gFnPc1	velká
tragédie	tragédie	k1gFnSc2	tragédie
Antonius	Antonius	k1gMnSc1	Antonius
a	a	k8xC	a
Kleopatra	Kleopatra	k1gFnSc1	Kleopatra
a	a	k8xC	a
Coriolanus	Coriolanus	k1gInSc1	Coriolanus
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
některé	některý	k3yIgInPc4	některý
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
veršů	verš	k1gInPc2	verš
<g/>
.	.	kIx.	.
</s>
<s>
Básník	básník	k1gMnSc1	básník
a	a	k8xC	a
kritik	kritik	k1gMnSc1	kritik
T.	T.	kA	T.
S.	S.	kA	S.
Eliot	Eliot	k1gMnSc1	Eliot
je	on	k3xPp3gNnPc4	on
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
Shakespearovy	Shakespearův	k2eAgFnPc4d1	Shakespearova
nejvydařenější	vydařený	k2eAgFnPc4d3	nejvydařenější
tragédie	tragédie	k1gFnPc4	tragédie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
posledním	poslední	k2eAgNnSc6d1	poslední
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
vrátil	vrátit	k5eAaPmAgMnS	vrátit
k	k	k7c3	k
romantice	romantika	k1gFnSc3	romantika
a	a	k8xC	a
tragikomediím	tragikomedie	k1gFnPc3	tragikomedie
a	a	k8xC	a
dokončil	dokončit	k5eAaPmAgMnS	dokončit
tři	tři	k4xCgFnPc4	tři
další	další	k2eAgFnPc4d1	další
velké	velký	k2eAgFnPc4d1	velká
hry	hra	k1gFnPc4	hra
<g/>
:	:	kIx,	:
Cymbelína	Cymbelín	k1gMnSc2	Cymbelín
<g/>
,	,	kIx,	,
Zimní	zimní	k2eAgFnSc4d1	zimní
pohádku	pohádka	k1gFnSc4	pohádka
a	a	k8xC	a
Bouři	bouře	k1gFnSc4	bouře
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
pak	pak	k6eAd1	pak
ještě	ještě	k6eAd1	ještě
Perikla	Perikla	k1gMnPc2	Perikla
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
čtyři	čtyři	k4xCgFnPc1	čtyři
hry	hra	k1gFnPc1	hra
mají	mít	k5eAaImIp3nP	mít
sice	sice	k8xC	sice
vážnější	vážní	k2eAgInSc1d2	vážnější
tón	tón	k1gInSc1	tón
než	než	k8xS	než
komedie	komedie	k1gFnSc1	komedie
z	z	k7c2	z
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ony	onen	k3xDgFnPc1	onen
skončí	skončit	k5eAaPmIp3nP	skončit
usmířením	usmíření	k1gNnSc7	usmíření
a	a	k8xC	a
odpuštěním	odpuštění	k1gNnSc7	odpuštění
potenciálně	potenciálně	k6eAd1	potenciálně
tragických	tragický	k2eAgNnPc2d1	tragické
pochybení	pochybení	k1gNnPc2	pochybení
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
kritikové	kritik	k1gMnPc1	kritik
viděli	vidět	k5eAaImAgMnP	vidět
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
změně	změna	k1gFnSc6	změna
nálad	nálada	k1gFnPc2	nálada
důkaz	důkaz	k1gInSc1	důkaz
klidnějšího	klidný	k2eAgInSc2d2	klidnější
pohledu	pohled	k1gInSc2	pohled
na	na	k7c4	na
život	život	k1gInSc4	život
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
jít	jít	k5eAaImF	jít
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
odraz	odraz	k1gInSc4	odraz
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
divadelní	divadelní	k2eAgFnSc2d1	divadelní
módy	móda	k1gFnSc2	móda
<g/>
.	.	kIx.	.
</s>
<s>
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
na	na	k7c6	na
dvou	dva	k4xCgFnPc6	dva
dalších	další	k2eAgFnPc6d1	další
dochovaných	dochovaný	k2eAgFnPc6d1	dochovaná
hrách	hra	k1gFnPc6	hra
<g/>
,	,	kIx,	,
Jindřichovi	Jindřich	k1gMnSc3	Jindřich
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
a	a	k8xC	a
Dvou	dva	k4xCgFnPc2	dva
vznešených	vznešený	k2eAgFnPc2d1	vznešená
příbuzných	příbuzná	k1gFnPc2	příbuzná
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
s	s	k7c7	s
Johnem	John	k1gMnSc7	John
Fletcherem	Fletcher	k1gMnSc7	Fletcher
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yIgFnPc4	který
společnosti	společnost	k1gFnPc4	společnost
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
napsal	napsat	k5eAaBmAgMnS	napsat
své	svůj	k3xOyFgFnPc4	svůj
rané	raný	k2eAgFnPc4d1	raná
hry	hra	k1gFnPc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Titulní	titulní	k2eAgFnSc1d1	titulní
strana	strana	k1gFnSc1	strana
vydání	vydání	k1gNnSc2	vydání
Tita	Tit	k2eAgFnSc1d1	Tita
Andronica	Andronica	k1gFnSc1	Andronica
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1594	[number]	k4	1594
odhaluje	odhalovat	k5eAaImIp3nS	odhalovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hra	hra	k1gFnSc1	hra
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
třemi	tři	k4xCgFnPc7	tři
různými	různý	k2eAgFnPc7d1	různá
hereckými	herecký	k2eAgFnPc7d1	herecká
společnostmi	společnost	k1gFnPc7	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
morové	morový	k2eAgFnSc2d1	morová
epidemie	epidemie	k1gFnSc2	epidemie
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1592-3	[number]	k4	1592-3
byly	být	k5eAaImAgFnP	být
Shakespearovy	Shakespearův	k2eAgFnPc1d1	Shakespearova
hry	hra	k1gFnPc1	hra
prováděny	prováděn	k2eAgFnPc1d1	prováděna
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
vlastní	vlastní	k2eAgFnSc6d1	vlastní
společnosti	společnost	k1gFnSc6	společnost
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
The	The	k1gFnSc2	The
Theatre	Theatr	k1gInSc5	Theatr
a	a	k8xC	a
v	v	k7c6	v
dalším	další	k2eAgNnSc6d1	další
divadle	divadlo	k1gNnSc6	divadlo
Curtain	Curtaina	k1gFnPc2	Curtaina
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Shoreditch	Shoreditch	k1gInSc4	Shoreditch
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgInSc4d1	ležící
severně	severně	k6eAd1	severně
od	od	k7c2	od
Temže	Temže	k1gFnSc2	Temže
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
společnost	společnost	k1gFnSc1	společnost
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
ve	v	k7c6	v
sporu	spor	k1gInSc6	spor
s	s	k7c7	s
majitelem	majitel	k1gMnSc7	majitel
pozemků	pozemek	k1gInPc2	pozemek
<g/>
,	,	kIx,	,
zbořila	zbořit	k5eAaPmAgFnS	zbořit
The	The	k1gFnSc1	The
Theatre	Theatr	k1gInSc5	Theatr
a	a	k8xC	a
použila	použít	k5eAaPmAgFnS	použít
trámy	trám	k1gInPc4	trám
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
Divadla	divadlo	k1gNnSc2	divadlo
Globe	globus	k1gInSc5	globus
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
první	první	k4xOgFnSc4	první
divadelní	divadelní	k2eAgFnSc4d1	divadelní
budovu	budova	k1gFnSc4	budova
postavenou	postavený	k2eAgFnSc4d1	postavená
herci	herec	k1gMnPc1	herec
pro	pro	k7c4	pro
herce	herc	k1gInPc4	herc
<g/>
,	,	kIx,	,
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Temže	Temže	k1gFnSc2	Temže
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Southwark	Southwark	k1gInSc4	Southwark
<g/>
.	.	kIx.	.
</s>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
Globe	globus	k1gInSc5	globus
bylo	být	k5eAaImAgNnS	být
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1599	[number]	k4	1599
<g/>
,	,	kIx,	,
hra	hra	k1gFnSc1	hra
Julius	Julius	k1gMnSc1	Julius
Caesar	Caesar	k1gMnSc1	Caesar
byla	být	k5eAaImAgFnS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
her	hra	k1gFnPc2	hra
zde	zde	k6eAd1	zde
inscenovaných	inscenovaný	k2eAgFnPc2d1	inscenovaná
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
Shakespearových	Shakespearových	k2eAgFnPc2d1	Shakespearových
největších	veliký	k2eAgFnPc2d3	veliký
her	hra	k1gFnPc2	hra
napsaných	napsaný	k2eAgFnPc2d1	napsaná
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1599	[number]	k4	1599
byla	být	k5eAaImAgFnS	být
určena	určit	k5eAaPmNgFnS	určit
pro	pro	k7c4	pro
Globe	globus	k1gInSc5	globus
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Hamleta	Hamlet	k1gMnSc2	Hamlet
<g/>
,	,	kIx,	,
Othella	Othello	k1gMnSc2	Othello
a	a	k8xC	a
Krále	Král	k1gMnSc2	Král
Leara	Lear	k1gMnSc2	Lear
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
divadelní	divadelní	k2eAgFnSc4d1	divadelní
společnost	společnost	k1gFnSc4	společnost
Služebníci	služebník	k1gMnPc1	služebník
lorda	lord	k1gMnSc2	lord
komořího	komoří	k1gMnSc2	komoří
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1603	[number]	k4	1603
přejmenována	přejmenovat	k5eAaPmNgFnS	přejmenovat
na	na	k7c4	na
Královskou	královský	k2eAgFnSc4d1	královská
společnost	společnost	k1gFnSc4	společnost
<g/>
,	,	kIx,	,
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
zvláštního	zvláštní	k2eAgInSc2d1	zvláštní
vztahu	vztah	k1gInSc2	vztah
s	s	k7c7	s
novým	nový	k2eAgMnSc7d1	nový
králem	král	k1gMnSc7	král
Jakubem	Jakub	k1gMnSc7	Jakub
I.	I.	kA	I.
Stuartem	Stuart	k1gMnSc7	Stuart
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
záznamy	záznam	k1gInPc1	záznam
provedení	provedení	k1gNnSc2	provedení
jsou	být	k5eAaImIp3nP	být
nejednotné	jednotný	k2eNgFnPc1d1	nejednotná
<g/>
,	,	kIx,	,
Královská	královský	k2eAgFnSc1d1	královská
společnost	společnost	k1gFnSc1	společnost
předvedla	předvést	k5eAaPmAgFnS	předvést
na	na	k7c6	na
jevišti	jeviště	k1gNnSc6	jeviště
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1604	[number]	k4	1604
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1605	[number]	k4	1605
sedm	sedm	k4xCc1	sedm
Shakespearových	Shakespearových	k2eAgFnPc2d1	Shakespearových
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
dvou	dva	k4xCgNnPc2	dva
představení	představení	k1gNnPc2	představení
Kupce	kupec	k1gMnSc4	kupec
benátského	benátský	k2eAgMnSc4d1	benátský
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1608	[number]	k4	1608
hrála	hrát	k5eAaImAgFnS	hrát
během	během	k7c2	během
zimy	zima	k1gFnSc2	zima
v	v	k7c6	v
krytém	krytý	k2eAgNnSc6d1	kryté
divadle	divadlo	k1gNnSc6	divadlo
Blackfriars	Blackfriarsa	k1gFnPc2	Blackfriarsa
a	a	k8xC	a
v	v	k7c6	v
Globe	globus	k1gInSc5	globus
v	v	k7c6	v
letním	letní	k2eAgNnSc6d1	letní
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
vybavení	vybavení	k1gNnSc1	vybavení
<g/>
,	,	kIx,	,
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
jakubovskou	jakubovský	k2eAgFnSc7d1	Jakubovská
módou	móda	k1gFnSc7	móda
pro	pro	k7c4	pro
bohaté	bohatý	k2eAgFnPc4d1	bohatá
divadelní	divadelní	k2eAgFnPc4d1	divadelní
masky	maska	k1gFnPc4	maska
<g/>
,	,	kIx,	,
dovolovalo	dovolovat	k5eAaImAgNnS	dovolovat
Shakespearovi	Shakespeare	k1gMnSc3	Shakespeare
zavést	zavést	k5eAaPmF	zavést
propracovanější	propracovaný	k2eAgFnPc4d2	propracovanější
jevištní	jevištní	k2eAgNnPc4d1	jevištní
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
Cymbelínovi	Cymbelín	k1gMnSc6	Cymbelín
Jupiter	Jupiter	k1gMnSc1	Jupiter
sestupuje	sestupovat	k5eAaImIp3nS	sestupovat
"	"	kIx"	"
<g/>
v	v	k7c4	v
hromy	hrom	k1gInPc4	hrom
a	a	k8xC	a
blesky	blesk	k1gInPc4	blesk
<g/>
,	,	kIx,	,
sedíce	sedit	k5eAaImSgFnP	sedit
na	na	k7c6	na
orlu	orel	k1gMnSc6	orel
<g/>
:	:	kIx,	:
on	on	k3xPp3gMnSc1	on
svrhne	svrhnout	k5eAaPmIp3nS	svrhnout
blesk	blesk	k1gInSc4	blesk
<g/>
.	.	kIx.	.
</s>
<s>
Duchové	duch	k1gMnPc1	duch
padnou	padnout	k5eAaImIp3nP	padnout
na	na	k7c4	na
kolena	koleno	k1gNnPc4	koleno
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Mezi	mezi	k7c4	mezi
slavné	slavný	k2eAgMnPc4d1	slavný
herce	herec	k1gMnPc4	herec
Shakespearovy	Shakespearův	k2eAgFnSc2d1	Shakespearova
společnosti	společnost	k1gFnSc2	společnost
patřili	patřit	k5eAaImAgMnP	patřit
Richard	Richard	k1gMnSc1	Richard
Burbage	Burbage	k1gInSc4	Burbage
<g/>
,	,	kIx,	,
William	William	k1gInSc4	William
Kempe	kemp	k1gInSc5	kemp
<g/>
,	,	kIx,	,
Henry	Henry	k1gMnSc1	Henry
Condell	Condell	k1gMnSc1	Condell
a	a	k8xC	a
John	John	k1gMnSc1	John
Heminges	Heminges	k1gMnSc1	Heminges
<g/>
.	.	kIx.	.
</s>
<s>
Burbage	Burbage	k1gInSc1	Burbage
hrál	hrát	k5eAaImAgInS	hrát
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
v	v	k7c6	v
prvních	první	k4xOgNnPc6	první
představeních	představení	k1gNnPc6	představení
mnoha	mnoho	k4c2	mnoho
Shakespearových	Shakespearových	k2eAgFnPc2d1	Shakespearových
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Richarda	Richard	k1gMnSc2	Richard
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Hamleta	Hamlet	k1gMnSc2	Hamlet
<g/>
,	,	kIx,	,
Othella	Othello	k1gMnSc2	Othello
a	a	k8xC	a
Krále	Král	k1gMnSc2	Král
Leara	Lear	k1gMnSc2	Lear
<g/>
.	.	kIx.	.
</s>
<s>
Populární	populární	k2eAgMnSc1d1	populární
komik	komik	k1gMnSc1	komik
Will	Will	k1gMnSc1	Will
Kempe	kemp	k1gInSc5	kemp
hrál	hrát	k5eAaImAgInS	hrát
služebníka	služebník	k1gMnSc4	služebník
Petra	Petr	k1gMnSc4	Petr
v	v	k7c6	v
Romeovi	Romeo	k1gMnSc6	Romeo
a	a	k8xC	a
Julii	Julie	k1gFnSc6	Julie
a	a	k8xC	a
Dogberry	Dogberra	k1gFnSc2	Dogberra
hrál	hrát	k5eAaImAgInS	hrát
kromě	kromě	k7c2	kromě
dalších	další	k2eAgFnPc2d1	další
postav	postava	k1gFnPc2	postava
v	v	k7c6	v
Mnoho	mnoho	k6eAd1	mnoho
povyku	povyk	k1gInSc2	povyk
pro	pro	k7c4	pro
nic	nic	k3yNnSc4	nic
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1600	[number]	k4	1600
Robertem	Robert	k1gMnSc7	Robert
Arminem	Armin	k1gMnSc7	Armin
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
hrál	hrát	k5eAaImAgMnS	hrát
role	role	k1gFnSc2	role
jako	jako	k8xS	jako
např.	např.	kA	např.
Touchstona	Touchstona	k1gFnSc1	Touchstona
v	v	k7c6	v
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
vám	vy	k3xPp2nPc3	vy
líbí	líbit	k5eAaImIp3nS	líbit
a	a	k8xC	a
šaška	šaška	k1gFnSc1	šaška
v	v	k7c6	v
Králi	Král	k1gMnSc6	Král
Learovi	Lear	k1gMnSc6	Lear
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1613	[number]	k4	1613
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
diplomat	diplomat	k1gMnSc1	diplomat
Henry	Henry	k1gMnSc1	Henry
Wotton	Wotton	k1gInSc4	Wotton
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jindřich	Jindřich	k1gMnSc1	Jindřich
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
s	s	k7c7	s
mnoha	mnoho	k4c2	mnoho
mimořádnými	mimořádný	k2eAgFnPc7d1	mimořádná
okolnostmi	okolnost	k1gFnPc7	okolnost
<g/>
,	,	kIx,	,
okázalostí	okázalost	k1gFnSc7	okázalost
a	a	k8xC	a
obřadností	obřadnost	k1gFnSc7	obřadnost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1613	[number]	k4	1613
však	však	k9	však
výstřel	výstřel	k1gInSc1	výstřel
z	z	k7c2	z
děla	dělo	k1gNnSc2	dělo
použitého	použitý	k2eAgNnSc2d1	Použité
při	při	k7c6	při
představení	představení	k1gNnSc6	představení
zapálil	zapálit	k5eAaPmAgMnS	zapálit
doškovou	doškový	k2eAgFnSc4d1	došková
střechu	střecha	k1gFnSc4	střecha
divadla	divadlo	k1gNnSc2	divadlo
Globe	globus	k1gInSc5	globus
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
do	do	k7c2	do
základů	základ	k1gInPc2	základ
vyhořelo	vyhořet	k5eAaPmAgNnS	vyhořet
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
událost	událost	k1gFnSc4	událost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
určuje	určovat	k5eAaImIp3nS	určovat
datum	datum	k1gNnSc4	datum
uvádění	uvádění	k1gNnSc2	uvádění
této	tento	k3xDgFnSc2	tento
Shakespearovy	Shakespearův	k2eAgFnSc2d1	Shakespearova
hry	hra	k1gFnSc2	hra
s	s	k7c7	s
vzácnou	vzácný	k2eAgFnSc7d1	vzácná
přesností	přesnost	k1gFnSc7	přesnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1623	[number]	k4	1623
John	John	k1gMnSc1	John
Heminges	Heminges	k1gMnSc1	Heminges
a	a	k8xC	a
Henry	Henry	k1gMnSc1	Henry
Condell	Condell	k1gMnSc1	Condell
<g/>
,	,	kIx,	,
dva	dva	k4xCgMnPc1	dva
Shakespearovi	Shakespearův	k2eAgMnPc1d1	Shakespearův
přátelé	přítel	k1gMnPc1	přítel
z	z	k7c2	z
Královské	královský	k2eAgFnSc2d1	královská
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
publikovali	publikovat	k5eAaBmAgMnP	publikovat
tzv.	tzv.	kA	tzv.
První	první	k4xOgNnSc1	první
folio	folio	k1gNnSc1	folio
<g/>
,	,	kIx,	,
sebrané	sebraný	k2eAgNnSc1d1	sebrané
vydání	vydání	k1gNnSc1	vydání
Shakespearových	Shakespearových	k2eAgFnPc2d1	Shakespearových
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
36	[number]	k4	36
textů	text	k1gInPc2	text
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
18	[number]	k4	18
vytištěných	vytištěný	k2eAgFnPc2d1	vytištěná
úplně	úplně	k6eAd1	úplně
poprvé	poprvé	k6eAd1	poprvé
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc4d1	mnohý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
her	hra	k1gFnPc2	hra
se	se	k3xPyFc4	se
už	už	k6eAd1	už
předtím	předtím	k6eAd1	předtím
objevily	objevit	k5eAaPmAgFnP	objevit
v	v	k7c6	v
quarto	quarta	k1gFnSc5	quarta
verzích	verze	k1gFnPc6	verze
(	(	kIx(	(
<g/>
formát	formát	k1gInSc4	formát
velikosti	velikost	k1gFnSc2	velikost
cca	cca	kA	cca
A	a	k9	a
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
–	–	k?	–
chatrné	chatrný	k2eAgFnPc4d1	chatrná
knihy	kniha	k1gFnPc4	kniha
vyrobené	vyrobený	k2eAgFnPc4d1	vyrobená
z	z	k7c2	z
listů	list	k1gInPc2	list
papíru	papír	k1gInSc2	papír
skládaného	skládaný	k2eAgInSc2d1	skládaný
dvakrát	dvakrát	k6eAd1	dvakrát
tak	tak	k9	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
čtyři	čtyři	k4xCgInPc1	čtyři
listy	list	k1gInPc1	list
<g/>
.	.	kIx.	.
</s>
<s>
Žádné	žádný	k3yNgInPc1	žádný
důkazy	důkaz	k1gInPc1	důkaz
nenaznačují	naznačovat	k5eNaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
schválil	schválit	k5eAaPmAgMnS	schválit
tyto	tento	k3xDgFnPc4	tento
edice	edice	k1gFnPc4	edice
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
První	první	k4xOgNnSc1	první
folio	folio	k1gNnSc1	folio
kvalifikuje	kvalifikovat	k5eAaBmIp3nS	kvalifikovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
ukradené	ukradený	k2eAgFnPc4d1	ukradená
a	a	k8xC	a
skryté	skrytý	k2eAgFnPc4d1	skrytá
kopie	kopie	k1gFnPc4	kopie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Anglický	anglický	k2eAgMnSc1d1	anglický
bibliograf	bibliograf	k1gMnSc1	bibliograf
Alfred	Alfred	k1gMnSc1	Alfred
Pollard	Pollard	k1gMnSc1	Pollard
nazval	nazvat	k5eAaBmAgMnS	nazvat
některé	některý	k3yIgFnPc4	některý
z	z	k7c2	z
těchto	tento	k3xDgNnPc2	tento
vydání	vydání	k1gNnPc2	vydání
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1623	[number]	k4	1623
"	"	kIx"	"
<g/>
špatná	špatný	k2eAgFnSc1d1	špatná
quarta	quarta	k1gFnSc1	quarta
<g/>
"	"	kIx"	"
kvůli	kvůli	k7c3	kvůli
jejich	jejich	k3xOp3gInPc3	jejich
upraveným	upravený	k2eAgInPc3d1	upravený
<g/>
,	,	kIx,	,
parafrázovaným	parafrázovaný	k2eAgInPc3d1	parafrázovaný
nebo	nebo	k8xC	nebo
poškozeným	poškozený	k2eAgInPc3d1	poškozený
textům	text	k1gInPc3	text
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
na	na	k7c6	na
řadě	řada	k1gFnSc6	řada
míst	místo	k1gNnPc2	místo
rekonstruovány	rekonstruován	k2eAgFnPc4d1	rekonstruována
z	z	k7c2	z
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
několik	několik	k4yIc1	několik
verzí	verze	k1gFnPc2	verze
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
každá	každý	k3xTgFnSc1	každý
navzájem	navzájem	k6eAd1	navzájem
liší	lišit	k5eAaImIp3nS	lišit
jedna	jeden	k4xCgFnSc1	jeden
od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
rozdíly	rozdíl	k1gInPc1	rozdíl
mohou	moct	k5eAaImIp3nP	moct
plynout	plynout	k5eAaImF	plynout
z	z	k7c2	z
chyb	chyba	k1gFnPc2	chyba
přebíraných	přebíraný	k2eAgFnPc2d1	přebíraná
při	při	k7c6	při
kopírování	kopírování	k1gNnSc6	kopírování
z	z	k7c2	z
poznámek	poznámka	k1gFnPc2	poznámka
herců	herec	k1gMnPc2	herec
nebo	nebo	k8xC	nebo
členů	člen	k1gMnPc2	člen
publika	publikum	k1gNnSc2	publikum
či	či	k8xC	či
ze	z	k7c2	z
Shakespearových	Shakespearových	k2eAgFnPc2d1	Shakespearových
vlastních	vlastní	k2eAgFnPc2d1	vlastní
pracovních	pracovní	k2eAgFnPc2d1	pracovní
poznámek	poznámka	k1gFnPc2	poznámka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
Hamletovi	Hamlet	k1gMnSc6	Hamlet
<g/>
,	,	kIx,	,
Troilovi	Troil	k1gMnSc6	Troil
a	a	k8xC	a
Kressidě	Kressida	k1gFnSc6	Kressida
a	a	k8xC	a
Othellovi	Othellův	k2eAgMnPc1d1	Othellův
<g/>
,	,	kIx,	,
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
mohl	moct	k5eAaImAgMnS	moct
mezi	mezi	k7c7	mezi
vydáními	vydání	k1gNnPc7	vydání
quarto	quarta	k1gFnSc5	quarta
a	a	k8xC	a
folio	folio	k1gNnSc4	folio
změnit	změnit	k5eAaPmF	změnit
znění	znění	k1gNnSc4	znění
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Krále	Král	k1gMnSc2	Král
Leara	Lear	k1gMnSc2	Lear
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yRgNnSc2	který
většina	většina	k1gFnSc1	většina
moderních	moderní	k2eAgNnPc2d1	moderní
vydání	vydání	k1gNnPc2	vydání
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
verzí	verze	k1gFnSc7	verze
folia	folio	k1gNnSc2	folio
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1623	[number]	k4	1623
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
odlišnost	odlišnost	k1gFnSc1	odlišnost
od	od	k7c2	od
vydání	vydání	k1gNnSc2	vydání
quarto	quarta	k1gFnSc5	quarta
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1608	[number]	k4	1608
způsobila	způsobit	k5eAaPmAgFnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
známém	známý	k2eAgNnSc6d1	známé
Oxfordském	oxfordský	k2eAgNnSc6d1	Oxfordské
vydání	vydání	k1gNnSc6	vydání
byly	být	k5eAaImAgFnP	být
otištěny	otisknout	k5eAaPmNgFnP	otisknout
obě	dva	k4xCgFnPc1	dva
verze	verze	k1gFnPc1	verze
s	s	k7c7	s
argumentem	argument	k1gInSc7	argument
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemohou	moct	k5eNaImIp3nP	moct
být	být	k5eAaImF	být
beze	beze	k7c2	beze
zmatku	zmatek	k1gInSc2	zmatek
sjednoceny	sjednotit	k5eAaPmNgFnP	sjednotit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1593	[number]	k4	1593
a	a	k8xC	a
1594	[number]	k4	1594
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
divadla	divadlo	k1gNnSc2	divadlo
kvůli	kvůli	k7c3	kvůli
moru	mor	k1gInSc3	mor
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
<g/>
,	,	kIx,	,
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
publikoval	publikovat	k5eAaBmAgMnS	publikovat
dvě	dva	k4xCgFnPc4	dva
narativní	narativní	k2eAgFnPc4d1	narativní
básně	báseň	k1gFnPc4	báseň
s	s	k7c7	s
erotickou	erotický	k2eAgFnSc7d1	erotická
tematikou	tematika	k1gFnSc7	tematika
<g/>
,	,	kIx,	,
Venuše	Venuše	k1gFnSc1	Venuše
a	a	k8xC	a
Adonis	Adonis	k1gFnSc1	Adonis
a	a	k8xC	a
Znásilnění	znásilnění	k1gNnSc1	znásilnění
Lukrécie	Lukrécie	k1gFnSc2	Lukrécie
<g/>
.	.	kIx.	.
</s>
<s>
Věnoval	věnovat	k5eAaImAgMnS	věnovat
je	být	k5eAaImIp3nS	být
Henrymu	Henry	k1gMnSc3	Henry
Wriothesleymu	Wriothesleym	k1gInSc2	Wriothesleym
<g/>
,	,	kIx,	,
hraběti	hrabě	k1gMnSc3	hrabě
ze	z	k7c2	z
Southamptonu	Southampton	k1gInSc2	Southampton
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
Venuši	Venuše	k1gFnSc4	Venuše
a	a	k8xC	a
Adonisovi	Adonis	k1gMnSc3	Adonis
nevinný	vinný	k2eNgInSc4d1	nevinný
Adonis	Adonis	k1gInSc4	Adonis
odmítá	odmítat	k5eAaImIp3nS	odmítat
sexuální	sexuální	k2eAgInPc1d1	sexuální
návrhy	návrh	k1gInPc1	návrh
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ve	v	k7c6	v
Znásilnění	znásilnění	k1gNnSc6	znásilnění
Lukrecie	Lukrecie	k1gFnSc2	Lukrecie
je	být	k5eAaImIp3nS	být
ctnostná	ctnostný	k2eAgFnSc1d1	ctnostná
žena	žena	k1gFnSc1	žena
Lukrecie	Lukrecie	k1gFnSc2	Lukrecie
znásilněna	znásilnit	k5eAaPmNgFnS	znásilnit
chlípným	chlípný	k2eAgInSc7d1	chlípný
Tarquinem	Tarquin	k1gInSc7	Tarquin
<g/>
.	.	kIx.	.
</s>
<s>
Ovlivněny	ovlivněn	k2eAgInPc1d1	ovlivněn
Ovidiovými	Ovidiův	k2eAgFnPc7d1	Ovidiova
Proměnami	proměna	k1gFnPc7	proměna
<g/>
,	,	kIx,	,
básně	báseň	k1gFnPc1	báseň
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
vinu	vina	k1gFnSc4	vina
a	a	k8xC	a
morální	morální	k2eAgInSc4d1	morální
zmatek	zmatek	k1gInSc4	zmatek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
z	z	k7c2	z
nekontrolovaného	kontrolovaný	k2eNgInSc2d1	nekontrolovaný
chtíče	chtíč	k1gInSc2	chtíč
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
se	se	k3xPyFc4	se
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
jako	jako	k9	jako
populární	populární	k2eAgFnPc1d1	populární
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
Shakespearova	Shakespearův	k2eAgInSc2d1	Shakespearův
života	život	k1gInSc2	život
často	často	k6eAd1	často
přetiskovány	přetiskován	k2eAgFnPc1d1	přetiskován
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc4	třetí
narativní	narativní	k2eAgFnSc4d1	narativní
báseň	báseň	k1gFnSc4	báseň
Milenčin	Milenčin	k2eAgInSc4d1	Milenčin
nářek	nářek	k1gInSc4	nářek
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
si	se	k3xPyFc3	se
mladá	mladý	k2eAgFnSc1d1	mladá
žena	žena	k1gFnSc1	žena
stěžuje	stěžovat	k5eAaImIp3nS	stěžovat
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
svedení	svedení	k1gNnSc4	svedení
přesvědčivým	přesvědčivý	k2eAgMnSc7d1	přesvědčivý
nápadníkem	nápadník	k1gMnSc7	nápadník
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
vytištěna	vytištěn	k2eAgFnSc1d1	vytištěna
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
vydání	vydání	k1gNnSc6	vydání
Sonetů	sonet	k1gInPc2	sonet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1609	[number]	k4	1609
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
vědců	vědec	k1gMnPc2	vědec
už	už	k6eAd1	už
nyní	nyní	k6eAd1	nyní
připouští	připouštět	k5eAaImIp3nS	připouštět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
je	být	k5eAaImIp3nS	být
autor	autor	k1gMnSc1	autor
Milenčina	Milenčin	k2eAgInSc2d1	Milenčin
nářku	nářek	k1gInSc2	nářek
<g/>
.	.	kIx.	.
</s>
<s>
Kritici	kritik	k1gMnPc1	kritik
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dobré	dobrý	k2eAgFnPc1d1	dobrá
vlastnosti	vlastnost	k1gFnPc1	vlastnost
básně	báseň	k1gFnSc2	báseň
jsou	být	k5eAaImIp3nP	být
pokaženy	pokažen	k2eAgInPc1d1	pokažen
nudnými	nudný	k2eAgInPc7d1	nudný
efekty	efekt	k1gInPc7	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Báseň	báseň	k1gFnSc1	báseň
Fénix	fénix	k1gMnSc1	fénix
a	a	k8xC	a
hrdlička	hrdlička	k1gFnSc1	hrdlička
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vytiskl	vytisknout	k5eAaPmAgMnS	vytisknout
Robert	Robert	k1gMnSc1	Robert
Chester	Chester	k1gMnSc1	Chester
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1601	[number]	k4	1601
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Love	lov	k1gInSc5	lov
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Martyr	martyr	k1gMnSc1	martyr
<g/>
,	,	kIx,	,
oplakává	oplakávat	k5eAaImIp3nS	oplakávat
smrt	smrt	k1gFnSc4	smrt
legendárního	legendární	k2eAgMnSc2d1	legendární
Fénixe	fénix	k1gMnSc2	fénix
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
milenky	milenka	k1gFnSc2	milenka
<g/>
,	,	kIx,	,
věrné	věrný	k2eAgFnSc2d1	věrná
hrdličky	hrdlička	k1gFnSc2	hrdlička
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1599	[number]	k4	1599
se	se	k3xPyFc4	se
dvě	dva	k4xCgFnPc1	dva
rané	raný	k2eAgFnPc1d1	raná
verze	verze	k1gFnPc1	verze
sonetů	sonet	k1gInPc2	sonet
138	[number]	k4	138
a	a	k8xC	a
144	[number]	k4	144
objevily	objevit	k5eAaPmAgFnP	objevit
ve	v	k7c6	v
Vášnivém	vášnivý	k2eAgMnSc6d1	vášnivý
poutníku	poutník	k1gMnSc6	poutník
<g/>
,	,	kIx,	,
publikovaným	publikovaný	k2eAgInSc7d1	publikovaný
pod	pod	k7c7	pod
Shakespearovým	Shakespearův	k2eAgNnSc7d1	Shakespearovo
jménem	jméno	k1gNnSc7	jméno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bez	bez	k7c2	bez
jeho	on	k3xPp3gNnSc2	on
svolení	svolení	k1gNnSc2	svolení
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Sonety	sonet	k1gInPc1	sonet
(	(	kIx(	(
<g/>
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sonety	sonet	k1gInPc1	sonet
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vyšly	vyjít	k5eAaPmAgInP	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1609	[number]	k4	1609
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
posledním	poslední	k2eAgNnSc7d1	poslední
Shakespearovým	Shakespearův	k2eAgNnSc7d1	Shakespearovo
nedramatickým	dramatický	k2eNgNnSc7d1	nedramatické
dílem	dílo	k1gNnSc7	dílo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
vytištěno	vytištěn	k2eAgNnSc1d1	vytištěno
<g/>
.	.	kIx.	.
</s>
<s>
Učenci	učenec	k1gMnPc1	učenec
si	se	k3xPyFc3	se
nejsou	být	k5eNaImIp3nP	být
jisti	jist	k2eAgMnPc1d1	jist
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
154	[number]	k4	154
sonetů	sonet	k1gInPc2	sonet
složen	složen	k2eAgMnSc1d1	složen
<g/>
,	,	kIx,	,
důkazy	důkaz	k1gInPc4	důkaz
ale	ale	k8xC	ale
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
psal	psát	k5eAaImAgMnS	psát
sonety	sonet	k1gInPc4	sonet
po	po	k7c4	po
celou	celá	k1gFnSc4	celá
svou	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
pro	pro	k7c4	pro
soukromého	soukromý	k2eAgMnSc4d1	soukromý
čtenáře	čtenář	k1gMnSc4	čtenář
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
dva	dva	k4xCgInPc1	dva
neautorizované	autorizovaný	k2eNgInPc1d1	neautorizovaný
sonety	sonet	k1gInPc1	sonet
objevily	objevit	k5eAaPmAgInP	objevit
ve	v	k7c6	v
Vášnivém	vášnivý	k2eAgMnSc6d1	vášnivý
poutníku	poutník	k1gMnSc6	poutník
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1599	[number]	k4	1599
<g/>
,	,	kIx,	,
Francis	Francis	k1gInSc1	Francis
Meres	Meresa	k1gFnPc2	Meresa
odkazoval	odkazovat	k5eAaImAgInS	odkazovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1598	[number]	k4	1598
mezi	mezi	k7c7	mezi
svými	svůj	k3xOyFgMnPc7	svůj
soukromými	soukromý	k2eAgMnPc7d1	soukromý
přáteli	přítel	k1gMnPc7	přítel
na	na	k7c4	na
"	"	kIx"	"
<g/>
Shakespearovy	Shakespearův	k2eAgInPc4d1	Shakespearův
sladké	sladký	k2eAgInPc4d1	sladký
sonety	sonet	k1gInPc4	sonet
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
analytiků	analytik	k1gMnPc2	analytik
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
publikování	publikování	k1gNnSc1	publikování
sbírky	sbírka	k1gFnSc2	sbírka
následuje	následovat	k5eAaImIp3nS	následovat
takto	takto	k6eAd1	takto
určenou	určený	k2eAgFnSc4d1	určená
Shakespearovu	Shakespearův	k2eAgFnSc4d1	Shakespearova
posloupnost	posloupnost	k1gFnSc4	posloupnost
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
byly	být	k5eAaImAgFnP	být
plánovány	plánovat	k5eAaImNgFnP	plánovat
dvě	dva	k4xCgFnPc1	dva
kontrastní	kontrastní	k2eAgFnPc1d1	kontrastní
série	série	k1gFnPc1	série
<g/>
:	:	kIx,	:
jedna	jeden	k4xCgFnSc1	jeden
o	o	k7c6	o
nekontrolovatelné	kontrolovatelný	k2eNgFnSc3d1	nekontrolovatelná
touze	touha	k1gFnSc3	touha
po	po	k7c6	po
vdané	vdaný	k2eAgFnSc6d1	vdaná
ženě	žena	k1gFnSc6	žena
tmavé	tmavý	k2eAgFnSc2d1	tmavá
pleti	pleť	k1gFnSc2	pleť
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
temná	temný	k2eAgFnSc1d1	temná
dáma	dáma	k1gFnSc1	dáma
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
o	o	k7c6	o
konfliktní	konfliktní	k2eAgFnSc6d1	konfliktní
lásce	láska	k1gFnSc6	láska
ke	k	k7c3	k
krásnému	krásný	k2eAgNnSc3d1	krásné
mladému	mladé	k1gNnSc3	mladé
muži	muž	k1gMnPc1	muž
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
krásný	krásný	k2eAgInSc1d1	krásný
mladík	mladík	k1gInSc1	mladík
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zůstává	zůstávat	k5eAaImIp3nS	zůstávat
nejasné	jasný	k2eNgNnSc1d1	nejasné
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
tyto	tento	k3xDgFnPc1	tento
figury	figura	k1gFnPc1	figura
představují	představovat	k5eAaImIp3nP	představovat
skutečné	skutečný	k2eAgMnPc4d1	skutečný
jedince	jedinec	k1gMnPc4	jedinec
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
autorské	autorský	k2eAgNnSc1d1	autorské
"	"	kIx"	"
<g/>
I	i	k9	i
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterými	který	k3yRgInPc7	který
je	být	k5eAaImIp3nS	být
oslovuje	oslovovat	k5eAaImIp3nS	oslovovat
<g/>
,	,	kIx,	,
představuje	představovat	k5eAaImIp3nS	představovat
Shakespeara	Shakespeare	k1gMnSc4	Shakespeare
samotného	samotný	k2eAgMnSc4d1	samotný
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
básník	básník	k1gMnSc1	básník
Wordsworth	Wordsworth	k1gMnSc1	Wordsworth
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
sonety	sonet	k1gInPc4	sonet
"	"	kIx"	"
<g/>
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
odemkl	odemknout	k5eAaPmAgMnS	odemknout
své	svůj	k3xOyFgNnSc4	svůj
srdce	srdce	k1gNnSc4	srdce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vydání	vydání	k1gNnSc1	vydání
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1609	[number]	k4	1609
bylo	být	k5eAaImAgNnS	být
věnováno	věnovat	k5eAaPmNgNnS	věnovat
"	"	kIx"	"
<g/>
panu	pan	k1gMnSc3	pan
W.	W.	kA	W.
H.	H.	kA	H.
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
uváděnému	uváděný	k2eAgMnSc3d1	uváděný
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
jediný	jediný	k2eAgMnSc1d1	jediný
původce	původce	k1gMnSc1	původce
<g/>
"	"	kIx"	"
básní	báseň	k1gFnPc2	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
to	ten	k3xDgNnSc1	ten
napsal	napsat	k5eAaPmAgMnS	napsat
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
sám	sám	k3xTgMnSc1	sám
a	a	k8xC	a
nebo	nebo	k8xC	nebo
vydavatel	vydavatel	k1gMnSc1	vydavatel
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
Thorpe	Thorp	k1gMnSc5	Thorp
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc1	jehož
iniciály	iniciála	k1gFnPc1	iniciála
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
v	v	k7c6	v
zápatí	zápatí	k1gNnSc6	zápatí
stránky	stránka	k1gFnSc2	stránka
s	s	k7c7	s
věnováním	věnování	k1gNnSc7	věnování
<g/>
;	;	kIx,	;
ani	ani	k8xC	ani
není	být	k5eNaImIp3nS	být
navzdory	navzdory	k7c3	navzdory
četným	četný	k2eAgFnPc3d1	četná
teoriím	teorie	k1gFnPc3	teorie
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
pan	pan	k1gMnSc1	pan
W.	W.	kA	W.
H.	H.	kA	H.
byl	být	k5eAaImAgInS	být
nebo	nebo	k8xC	nebo
zda	zda	k8xS	zda
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
dokonce	dokonce	k9	dokonce
autorizoval	autorizovat	k5eAaBmAgMnS	autorizovat
publikaci	publikace	k1gFnSc4	publikace
<g/>
.	.	kIx.	.
</s>
<s>
Kritici	kritik	k1gMnPc1	kritik
vychvalují	vychvalovat	k5eAaImIp3nP	vychvalovat
Sonety	sonet	k1gInPc4	sonet
jako	jako	k8xC	jako
hluboké	hluboký	k2eAgFnPc4d1	hluboká
meditace	meditace	k1gFnPc4	meditace
o	o	k7c6	o
povaze	povaha	k1gFnSc6	povaha
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
sexuální	sexuální	k2eAgFnSc2d1	sexuální
vášně	vášeň	k1gFnSc2	vášeň
<g/>
,	,	kIx,	,
plození	plození	k1gNnSc2	plození
<g/>
,	,	kIx,	,
smrti	smrt	k1gFnSc2	smrt
a	a	k8xC	a
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
Shakespearovy	Shakespearův	k2eAgFnPc1d1	Shakespearova
hry	hra	k1gFnPc1	hra
byly	být	k5eAaImAgFnP	být
psány	psát	k5eAaImNgFnP	psát
v	v	k7c6	v
tehdejším	tehdejší	k2eAgInSc6d1	tehdejší
tradičním	tradiční	k2eAgInSc6d1	tradiční
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaPmAgMnS	napsat
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
stylizovaném	stylizovaný	k2eAgInSc6d1	stylizovaný
jazyce	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
přirozeně	přirozeně	k6eAd1	přirozeně
vyvěrá	vyvěrat	k5eAaImIp3nS	vyvěrat
z	z	k7c2	z
potřeb	potřeba	k1gFnPc2	potřeba
postav	postava	k1gFnPc2	postava
či	či	k8xC	či
dramatu	drama	k1gNnSc2	drama
<g/>
.	.	kIx.	.
</s>
<s>
Poezie	poezie	k1gFnSc1	poezie
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c4	na
dosti	dosti	k6eAd1	dosti
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
komplikovaných	komplikovaný	k2eAgFnPc6d1	komplikovaná
metaforách	metafora	k1gFnPc6	metafora
a	a	k8xC	a
přirovnáních	přirovnání	k1gNnPc6	přirovnání
<g/>
,	,	kIx,	,
a	a	k8xC	a
jazyk	jazyk	k1gInSc1	jazyk
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
rétorický	rétorický	k2eAgInSc1d1	rétorický
napsaný	napsaný	k2eAgInSc1d1	napsaný
pro	pro	k7c4	pro
herce	herec	k1gMnPc4	herec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
spíše	spíše	k9	spíše
deklamovali	deklamovat	k5eAaImAgMnP	deklamovat
než	než	k8xS	než
mluvili	mluvit	k5eAaImAgMnP	mluvit
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
velkolepé	velkolepý	k2eAgInPc1d1	velkolepý
projevy	projev	k1gInPc1	projev
v	v	k7c4	v
Titu	Tita	k1gFnSc4	Tita
Andronicovi	Andronicův	k2eAgMnPc1d1	Andronicův
z	z	k7c2	z
pohledů	pohled	k1gInPc2	pohled
některých	některý	k3yIgMnPc2	některý
kritiků	kritik	k1gMnPc2	kritik
často	často	k6eAd1	často
jen	jen	k9	jen
podpírají	podpírat	k5eAaImIp3nP	podpírat
dění	dění	k1gNnSc1	dění
a	a	k8xC	a
verše	verš	k1gInPc1	verš
ve	v	k7c6	v
Dvou	dva	k4xCgMnPc6	dva
šlechticích	šlechtic	k1gMnPc6	šlechtic
z	z	k7c2	z
Verony	Verona	k1gFnSc2	Verona
jsou	být	k5eAaImIp3nP	být
popisovány	popisován	k2eAgInPc1d1	popisován
jako	jako	k8xS	jako
strnulé	strnulý	k2eAgInPc1d1	strnulý
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
si	se	k3xPyFc3	se
však	však	k9	však
začal	začít	k5eAaPmAgMnS	začít
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
tradiční	tradiční	k2eAgInSc4d1	tradiční
styl	styl	k1gInSc4	styl
přizpůsobovat	přizpůsobovat	k5eAaImF	přizpůsobovat
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
vlastní	vlastní	k2eAgInPc4d1	vlastní
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Úvodní	úvodní	k2eAgInSc1d1	úvodní
monolog	monolog	k1gInSc1	monolog
Richarda	Richard	k1gMnSc2	Richard
III	III	kA	III
<g/>
.	.	kIx.	.
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgInPc4	svůj
kořeny	kořen	k1gInPc4	kořen
ve	v	k7c6	v
vlastním	vlastní	k2eAgNnSc6d1	vlastní
prohlášení	prohlášení	k1gNnSc6	prohlášení
Neřesti	neřest	k1gFnSc2	neřest
ve	v	k7c6	v
středověkém	středověký	k2eAgNnSc6d1	středověké
dramatu	drama	k1gNnSc6	drama
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
Richardovo	Richardův	k2eAgNnSc1d1	Richardovo
sugestivní	sugestivní	k2eAgNnSc1d1	sugestivní
sebeuvědomění	sebeuvědomění	k1gNnSc1	sebeuvědomění
už	už	k6eAd1	už
netrpělivě	trpělivě	k6eNd1	trpělivě
očekává	očekávat	k5eAaImIp3nS	očekávat
monology	monolog	k1gInPc4	monolog
zralých	zralý	k2eAgFnPc2d1	zralá
Shakespearových	Shakespearových	k2eAgFnPc2d1	Shakespearových
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Žádná	žádný	k3yNgFnSc1	žádný
jednotlivá	jednotlivý	k2eAgFnSc1d1	jednotlivá
hra	hra	k1gFnSc1	hra
nepředstavuje	představovat	k5eNaImIp3nS	představovat
změnu	změna	k1gFnSc4	změna
od	od	k7c2	od
tradičního	tradiční	k2eAgMnSc2d1	tradiční
k	k	k7c3	k
volnějšímu	volný	k2eAgInSc3d2	volnější
stylu	styl	k1gInSc3	styl
<g/>
.	.	kIx.	.
</s>
<s>
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
během	během	k7c2	během
celé	celá	k1gFnSc2	celá
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
tyto	tento	k3xDgInPc4	tento
dva	dva	k4xCgInPc4	dva
styly	styl	k1gInPc4	styl
propojoval	propojovat	k5eAaImAgInS	propojovat
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Romeo	Romeo	k1gMnSc1	Romeo
a	a	k8xC	a
Julie	Julie	k1gFnSc1	Julie
je	být	k5eAaImIp3nS	být
snad	snad	k9	snad
nejlepším	dobrý	k2eAgInSc7d3	nejlepší
příkladem	příklad	k1gInSc7	příklad
tohoto	tento	k3xDgInSc2	tento
přístupu	přístup	k1gInSc2	přístup
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
vytvoření	vytvoření	k1gNnSc2	vytvoření
Romea	Romeo	k1gMnSc2	Romeo
a	a	k8xC	a
Julie	Julie	k1gFnSc2	Julie
<g/>
,	,	kIx,	,
Richarda	Richard	k1gMnSc2	Richard
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
Snu	sen	k1gInSc2	sen
noci	noc	k1gFnSc2	noc
svatojánské	svatojánský	k2eAgFnSc2d1	Svatojánská
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začal	začít	k5eAaPmAgMnS	začít
psát	psát	k5eAaImF	psát
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
více	hodně	k6eAd2	hodně
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
poezii	poezie	k1gFnSc4	poezie
<g/>
.	.	kIx.	.
</s>
<s>
Metafory	metafora	k1gFnPc1	metafora
a	a	k8xC	a
obrazy	obraz	k1gInPc1	obraz
ve	v	k7c6	v
zvýšené	zvýšený	k2eAgFnSc6d1	zvýšená
míře	míra	k1gFnSc6	míra
obrátil	obrátit	k5eAaPmAgMnS	obrátit
k	k	k7c3	k
potřebám	potřeba	k1gFnPc3	potřeba
samotného	samotný	k2eAgNnSc2d1	samotné
dramatu	drama	k1gNnSc2	drama
<g/>
.	.	kIx.	.
</s>
<s>
Standardní	standardní	k2eAgNnSc1d1	standardní
básnickou	básnický	k2eAgFnSc7d1	básnická
formou	forma	k1gFnSc7	forma
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
byl	být	k5eAaImAgMnS	být
blankvers	blankvers	k1gInSc4	blankvers
<g/>
,	,	kIx,	,
složený	složený	k2eAgInSc4d1	složený
v	v	k7c6	v
jambickém	jambický	k2eAgInSc6d1	jambický
pětistopém	pětistopý	k2eAgInSc6d1	pětistopý
verši	verš	k1gInSc6	verš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
to	ten	k3xDgNnSc1	ten
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
verše	verš	k1gInPc1	verš
byly	být	k5eAaImAgInP	být
obvykle	obvykle	k6eAd1	obvykle
bez	bez	k7c2	bez
rytmu	rytmus	k1gInSc2	rytmus
a	a	k8xC	a
verš	verš	k1gInSc4	verš
tvořilo	tvořit	k5eAaImAgNnS	tvořit
deset	deset	k4xCc1	deset
slabik	slabika	k1gFnPc2	slabika
<g/>
,	,	kIx,	,
přednášených	přednášený	k2eAgInPc2d1	přednášený
s	s	k7c7	s
důrazem	důraz	k1gInSc7	důraz
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
druhé	druhý	k4xOgFnSc6	druhý
slabice	slabika	k1gFnSc6	slabika
<g/>
.	.	kIx.	.
</s>
<s>
Blankvers	blankvers	k1gInSc1	blankvers
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
raných	raný	k2eAgFnPc2d1	raná
her	hra	k1gFnPc2	hra
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
odlišný	odlišný	k2eAgInSc1d1	odlišný
od	od	k7c2	od
těch	ten	k3xDgInPc2	ten
pozdějších	pozdní	k2eAgInPc2d2	pozdější
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
krásný	krásný	k2eAgInSc1d1	krásný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
věty	věta	k1gFnPc1	věta
tíhnou	tíhnout	k5eAaImIp3nP	tíhnout
k	k	k7c3	k
začátku	začátek	k1gInSc2	začátek
<g/>
,	,	kIx,	,
pozastavení	pozastavení	k1gNnSc2	pozastavení
a	a	k8xC	a
ukončení	ukončení	k1gNnSc2	ukončení
na	na	k7c6	na
konci	konec	k1gInSc6	konec
verše	verš	k1gInSc2	verš
s	s	k7c7	s
rizikem	riziko	k1gNnSc7	riziko
jednotvárnosti	jednotvárnost	k1gFnSc2	jednotvárnost
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k9	co
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
zvládl	zvládnout	k5eAaPmAgMnS	zvládnout
tradiční	tradiční	k2eAgInSc4d1	tradiční
blankvers	blankvers	k1gInSc4	blankvers
<g/>
,	,	kIx,	,
jej	on	k3xPp3gInSc4	on
začal	začít	k5eAaPmAgMnS	začít
narušovat	narušovat	k5eAaImF	narušovat
a	a	k8xC	a
měnit	měnit	k5eAaImF	měnit
jeho	jeho	k3xOp3gInSc4	jeho
průběh	průběh	k1gInSc4	průběh
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
technika	technika	k1gFnSc1	technika
ve	v	k7c6	v
hrách	hra	k1gFnPc6	hra
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
Julius	Julius	k1gMnSc1	Julius
Caesar	Caesar	k1gMnSc1	Caesar
a	a	k8xC	a
Hamlet	Hamlet	k1gMnSc1	Hamlet
uvolnila	uvolnit	k5eAaPmAgFnS	uvolnit
novou	nový	k2eAgFnSc4d1	nová
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
pružnost	pružnost	k1gFnSc4	pružnost
poezie	poezie	k1gFnSc2	poezie
<g/>
.	.	kIx.	.
</s>
<s>
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
ji	on	k3xPp3gFnSc4	on
používá	používat	k5eAaImIp3nS	používat
například	například	k6eAd1	například
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zprostředkoval	zprostředkovat	k5eAaPmAgInS	zprostředkovat
zmatek	zmatek	k1gInSc4	zmatek
v	v	k7c6	v
Hamletově	Hamletův	k2eAgFnSc6d1	Hamletova
mysli	mysl	k1gFnSc6	mysl
<g/>
:	:	kIx,	:
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Hamletovi	Hamlet	k1gMnSc6	Hamlet
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
dále	daleko	k6eAd2	daleko
měnil	měnit	k5eAaImAgMnS	měnit
svůj	svůj	k3xOyFgInSc4	svůj
poetický	poetický	k2eAgInSc4d1	poetický
styl	styl	k1gInSc4	styl
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
v	v	k7c6	v
emocionálnějších	emocionální	k2eAgFnPc6d2	emocionálnější
pasážích	pasáž	k1gFnPc6	pasáž
pozdních	pozdní	k2eAgFnPc2d1	pozdní
tragédií	tragédie	k1gFnPc2	tragédie
<g/>
.	.	kIx.	.
</s>
<s>
Literární	literární	k2eAgMnSc1d1	literární
kritik	kritik	k1gMnSc1	kritik
A.	A.	kA	A.
C.	C.	kA	C.
Bradley	Bradlea	k1gFnSc2	Bradlea
popsal	popsat	k5eAaPmAgInS	popsat
tento	tento	k3xDgInSc1	tento
styl	styl	k1gInSc1	styl
jako	jako	k9	jako
"	"	kIx"	"
<g/>
více	hodně	k6eAd2	hodně
koncentrovaný	koncentrovaný	k2eAgInSc1d1	koncentrovaný
<g/>
,	,	kIx,	,
rychlý	rychlý	k2eAgInSc1d1	rychlý
<g/>
,	,	kIx,	,
pestrý	pestrý	k2eAgInSc1d1	pestrý
a	a	k8xC	a
ve	v	k7c6	v
stavbě	stavba	k1gFnSc6	stavba
méně	málo	k6eAd2	málo
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
<g/>
,	,	kIx,	,
nezřídka	nezřídka	k6eAd1	nezřídka
kroucený	kroucený	k2eAgMnSc1d1	kroucený
nebo	nebo	k8xC	nebo
náznakový	náznakový	k2eAgMnSc1d1	náznakový
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
období	období	k1gNnSc6	období
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
používal	používat	k5eAaImAgMnS	používat
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
mnoho	mnoho	k6eAd1	mnoho
technik	technik	k1gMnSc1	technik
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
tohoto	tento	k3xDgInSc2	tento
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
techniky	technika	k1gFnPc1	technika
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
neúplné	úplný	k2eNgFnPc1d1	neúplná
syntaxe	syntax	k1gFnPc1	syntax
na	na	k7c6	na
konci	konec	k1gInSc6	konec
věty	věta	k1gFnSc2	věta
<g/>
,	,	kIx,	,
nepravidelné	pravidelný	k2eNgFnPc1d1	nepravidelná
pauzy	pauza	k1gFnPc1	pauza
a	a	k8xC	a
ukončení	ukončení	k1gNnSc1	ukončení
a	a	k8xC	a
extrémní	extrémní	k2eAgNnSc1d1	extrémní
kolísání	kolísání	k1gNnSc1	kolísání
větné	větný	k2eAgFnSc2d1	větná
struktury	struktura	k1gFnSc2	struktura
a	a	k8xC	a
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Macbethovi	Macbeth	k1gMnSc6	Macbeth
například	například	k6eAd1	například
jazyk	jazyk	k1gInSc1	jazyk
přeskakuje	přeskakovat	k5eAaImIp3nS	přeskakovat
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
nepříbuzné	příbuzný	k2eNgFnSc2d1	nepříbuzná
metafory	metafora	k1gFnSc2	metafora
či	či	k8xC	či
přirovnání	přirovnání	k1gNnSc2	přirovnání
k	k	k7c3	k
druhé	druhý	k4xOgNnSc1	druhý
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
was	was	k?	was
the	the	k?	the
hope	hope	k1gInSc1	hope
drunk	drunk	k1gMnSc1	drunk
<g/>
/	/	kIx~	/
Wherein	Wherein	k1gMnSc1	Wherein
you	you	k?	you
dressed	dressed	k1gMnSc1	dressed
yourself	yourself	k1gMnSc1	yourself
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1.7	[number]	k4	1.7
<g/>
.35	.35	k4	.35
<g/>
-	-	kIx~	-
<g/>
38	[number]	k4	38
<g/>
)	)	kIx)	)
-	-	kIx~	-
česky	česky	k6eAd1	česky
"	"	kIx"	"
<g/>
A	a	k8xC	a
což	což	k3yRnSc1	což
ta	ten	k3xDgNnPc1	ten
naděj	nadát	k5eAaBmRp2nS	nadát
byla	být	k5eAaImAgFnS	být
opilou	opilý	k2eAgFnSc7d1	opilá
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
oblékl	obléct	k5eAaPmAgMnS	obléct
jsi	být	k5eAaImIp2nS	být
dřív	dříve	k6eAd2	dříve
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
"	"	kIx"	"
<g/>
...	...	k?	...
pity	pit	k2eAgFnPc4d1	pita
<g/>
,	,	kIx,	,
like	like	k6eAd1	like
a	a	k8xC	a
naked	naked	k1gMnSc1	naked
new-born	neworn	k1gMnSc1	new-born
babe	babit	k5eAaImSgInS	babit
<g/>
/	/	kIx~	/
Striding	Striding	k1gInSc1	Striding
the	the	k?	the
blast	blast	k1gInSc1	blast
<g/>
,	,	kIx,	,
or	or	k?	or
heaven	heavna	k1gFnPc2	heavna
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
cherubim	cherubim	k1gInSc1	cherubim
<g/>
,	,	kIx,	,
hors	hors	k1gInSc1	hors
<g/>
'	'	kIx"	'
<g/>
d	d	k?	d
<g/>
/	/	kIx~	/
Upon	Upon	k1gMnSc1	Upon
the	the	k?	the
sightless	sightless	k1gInSc1	sightless
couriers	couriers	k1gInSc1	couriers
of	of	k?	of
the	the	k?	the
air	air	k?	air
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
1.7	[number]	k4	1.7
<g/>
.21	.21	k4	.21
<g/>
-	-	kIx~	-
<g/>
25	[number]	k4	25
<g/>
)	)	kIx)	)
-	-	kIx~	-
česky	česky	k6eAd1	česky
"	"	kIx"	"
<g/>
...	...	k?	...
a	a	k8xC	a
žel	žel	k9	žel
jak	jak	k6eAd1	jak
nahé	nahý	k2eAgNnSc1d1	nahé
novorozeně	novorozeně	k1gNnSc1	novorozeně
na	na	k7c6	na
vichru	vichr	k1gInSc6	vichr
jedoucí	jedoucí	k2eAgNnSc1d1	jedoucí
<g/>
,	,	kIx,	,
neb	neb	k8xC	neb
jak	jak	k8xC	jak
cherub	cherub	k1gMnSc1	cherub
na	na	k7c6	na
vzdušných	vzdušný	k2eAgMnPc6d1	vzdušný
ořích	oř	k1gMnPc6	oř
neviditelných	viditelný	k2eNgFnPc2d1	neviditelná
ten	ten	k3xDgInSc4	ten
hrůzný	hrůzný	k2eAgInSc4d1	hrůzný
čin	čin	k1gInSc4	čin
v	v	k7c4	v
zrak	zrak	k1gInSc4	zrak
každý	každý	k3xTgInSc1	každý
zadují	zadout	k5eAaPmIp3nP	zadout
<g/>
...	...	k?	...
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Posluchač	posluchač	k1gMnSc1	posluchač
je	být	k5eAaImIp3nS	být
vyzýván	vyzývat	k5eAaImNgMnS	vyzývat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
sám	sám	k3xTgMnSc1	sám
dokončil	dokončit	k5eAaPmAgMnS	dokončit
smysl	smysl	k1gInSc4	smysl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdních	pozdní	k2eAgFnPc6d1	pozdní
romancích	romance	k1gFnPc6	romance
(	(	kIx(	(
<g/>
tragikomediích	tragikomedie	k1gFnPc6	tragikomedie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pracuje	pracovat	k5eAaImIp3nS	pracovat
s	s	k7c7	s
posuny	posun	k1gInPc7	posun
v	v	k7c6	v
čase	čas	k1gInSc6	čas
a	a	k8xC	a
překvapivými	překvapivý	k2eAgInPc7d1	překvapivý
obraty	obrat	k1gInPc7	obrat
zápletek	zápletka	k1gFnPc2	zápletka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
inspiroval	inspirovat	k5eAaBmAgInS	inspirovat
nejnovějším	nový	k2eAgInSc7d3	nejnovější
poetickým	poetický	k2eAgInSc7d1	poetický
stylem	styl	k1gInSc7	styl
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
jsou	být	k5eAaImIp3nP	být
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
a	a	k8xC	a
krátké	krátký	k2eAgFnPc1d1	krátká
věty	věta	k1gFnPc1	věta
nastaveny	nastavit	k5eAaPmNgFnP	nastavit
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
věty	věta	k1gFnPc1	věta
jsou	být	k5eAaImIp3nP	být
nakupeny	nakupen	k2eAgFnPc1d1	nakupena
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přehozen	přehozen	k2eAgInSc1d1	přehozen
předmět	předmět	k1gInSc1	předmět
a	a	k8xC	a
objekt	objekt	k1gInSc1	objekt
a	a	k8xC	a
slova	slovo	k1gNnPc1	slovo
jsou	být	k5eAaImIp3nP	být
vynechávána	vynecháván	k2eAgNnPc1d1	vynecháváno
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
vytvářen	vytvářet	k5eAaImNgInS	vytvářet
efekt	efekt	k1gInSc1	efekt
spontánnosti	spontánnost	k1gFnSc2	spontánnost
<g/>
.	.	kIx.	.
</s>
<s>
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
v	v	k7c6	v
sobě	se	k3xPyFc3	se
kombinoval	kombinovat	k5eAaImAgInS	kombinovat
básnického	básnický	k2eAgMnSc4d1	básnický
génia	génius	k1gMnSc4	génius
a	a	k8xC	a
praktický	praktický	k2eAgInSc4d1	praktický
smysl	smysl	k1gInSc4	smysl
pro	pro	k7c4	pro
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
všichni	všechen	k3xTgMnPc1	všechen
autoři	autor	k1gMnPc1	autor
divadelních	divadelní	k2eAgFnPc2d1	divadelní
her	hra	k1gFnPc2	hra
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
dramatizoval	dramatizovat	k5eAaBmAgMnS	dramatizovat
příběhy	příběh	k1gInPc4	příběh
ze	z	k7c2	z
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byl	být	k5eAaImAgMnS	být
historik	historik	k1gMnSc1	historik
Plútarchos	Plútarchos	k1gMnSc1	Plútarchos
nebo	nebo	k8xC	nebo
soudobý	soudobý	k2eAgMnSc1d1	soudobý
anglický	anglický	k2eAgMnSc1d1	anglický
kronikář	kronikář	k1gMnSc1	kronikář
Raphael	Raphael	k1gMnSc1	Raphael
Holinshed	Holinshed	k1gMnSc1	Holinshed
<g/>
.	.	kIx.	.
</s>
<s>
Přetvořil	přetvořit	k5eAaPmAgMnS	přetvořit
každou	každý	k3xTgFnSc4	každý
zápletku	zápletka	k1gFnSc4	zápletka
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
vytvořit	vytvořit	k5eAaPmF	vytvořit
několik	několik	k4yIc4	několik
center	centrum	k1gNnPc2	centrum
zájmu	zájem	k1gInSc2	zájem
a	a	k8xC	a
ukázat	ukázat	k5eAaPmF	ukázat
obecenstvu	obecenstvo	k1gNnSc3	obecenstvo
co	co	k8xS	co
nejvíce	hodně	k6eAd3	hodně
různých	různý	k2eAgFnPc2d1	různá
fazet	fazeta	k1gFnPc2	fazeta
příběhu	příběh	k1gInSc2	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
pevná	pevný	k2eAgFnSc1d1	pevná
konstrukce	konstrukce	k1gFnSc1	konstrukce
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Shakespearova	Shakespearův	k2eAgFnSc1d1	Shakespearova
hra	hra	k1gFnSc1	hra
může	moct	k5eAaImIp3nS	moct
přežít	přežít	k5eAaPmF	přežít
překlad	překlad	k1gInSc4	překlad
<g/>
,	,	kIx,	,
rozřezání	rozřezání	k1gNnSc4	rozřezání
i	i	k8xC	i
širokou	široký	k2eAgFnSc4d1	široká
interpretaci	interpretace	k1gFnSc4	interpretace
bez	bez	k7c2	bez
ztráty	ztráta	k1gFnSc2	ztráta
své	svůj	k3xOyFgFnPc4	svůj
základní	základní	k2eAgFnPc4d1	základní
dramatičnosti	dramatičnost	k1gFnPc4	dramatičnost
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xS	jak
Shakespearovo	Shakespearův	k2eAgNnSc1d1	Shakespearovo
mistrovství	mistrovství	k1gNnSc1	mistrovství
rostlo	růst	k5eAaImAgNnS	růst
<g/>
,	,	kIx,	,
dával	dávat	k5eAaImAgMnS	dávat
svým	svůj	k3xOyFgMnPc3	svůj
postavám	postava	k1gFnPc3	postava
jasnější	jasný	k2eAgFnSc4d2	jasnější
a	a	k8xC	a
pestřejší	pestrý	k2eAgFnSc4d2	pestřejší
motivaci	motivace	k1gFnSc4	motivace
a	a	k8xC	a
charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
vzorce	vzorec	k1gInPc1	vzorec
projevu	projev	k1gInSc2	projev
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
i	i	k9	i
v	v	k7c6	v
pozdějších	pozdní	k2eAgFnPc6d2	pozdější
hrách	hra	k1gFnPc6	hra
zachoval	zachovat	k5eAaPmAgInS	zachovat
ohled	ohled	k1gInSc1	ohled
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
dřívější	dřívější	k2eAgInSc4d1	dřívější
styl	styl	k1gInSc4	styl
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
pozdních	pozdní	k2eAgFnPc6d1	pozdní
romancích	romance	k1gFnPc6	romance
se	se	k3xPyFc4	se
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
asi	asi	k9	asi
záměrně	záměrně	k6eAd1	záměrně
vrátil	vrátit	k5eAaPmAgInS	vrátit
ke	k	k7c3	k
strojenějšímu	strojený	k2eAgInSc3d2	strojený
stylu	styl	k1gInSc3	styl
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zdůraznil	zdůraznit	k5eAaPmAgMnS	zdůraznit
iluzi	iluze	k1gFnSc4	iluze
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Shakespearovo	Shakespearův	k2eAgNnSc1d1	Shakespearovo
dílo	dílo	k1gNnSc1	dílo
mělo	mít	k5eAaImAgNnS	mít
trvalý	trvalý	k2eAgInSc4d1	trvalý
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
pozdější	pozdní	k2eAgNnSc4d2	pozdější
divadlo	divadlo	k1gNnSc4	divadlo
a	a	k8xC	a
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Obzvláště	obzvláště	k6eAd1	obzvláště
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
dramatický	dramatický	k2eAgInSc4d1	dramatický
potenciál	potenciál	k1gInSc4	potenciál
popisu	popis	k1gInSc2	popis
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
zápletek	zápletka	k1gFnPc2	zápletka
<g/>
,	,	kIx,	,
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
žánru	žánr	k1gInSc2	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
až	až	k9	až
do	do	k7c2	do
Romea	Romeo	k1gMnSc2	Romeo
a	a	k8xC	a
Julie	Julie	k1gFnSc1	Julie
romantika	romantika	k1gFnSc1	romantika
(	(	kIx(	(
<g/>
milostný	milostný	k2eAgInSc1d1	milostný
poměr	poměr	k1gInSc1	poměr
<g/>
,	,	kIx,	,
romance	romance	k1gFnSc1	romance
<g/>
)	)	kIx)	)
nebyla	být	k5eNaImAgFnS	být
vnímána	vnímat	k5eAaImNgFnS	vnímat
jako	jako	k9	jako
téma	téma	k1gNnSc4	téma
hodné	hodný	k2eAgNnSc4d1	hodné
pro	pro	k7c4	pro
tragédii	tragédie	k1gFnSc4	tragédie
<g/>
.	.	kIx.	.
</s>
<s>
Monology	monolog	k1gInPc1	monolog
byly	být	k5eAaImAgInP	být
používány	používat	k5eAaImNgInP	používat
hlavně	hlavně	k6eAd1	hlavně
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zprostředkovaly	zprostředkovat	k5eAaPmAgFnP	zprostředkovat
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
postavách	postava	k1gFnPc6	postava
nebo	nebo	k8xC	nebo
o	o	k7c6	o
událostech	událost	k1gFnPc6	událost
<g/>
;	;	kIx,	;
ale	ale	k8xC	ale
až	až	k9	až
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
je	být	k5eAaImIp3nS	být
používal	používat	k5eAaImAgMnS	používat
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
prozkoumal	prozkoumat	k5eAaPmAgInS	prozkoumat
mysl	mysl	k1gFnSc4	mysl
svých	svůj	k3xOyFgFnPc2	svůj
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
silně	silně	k6eAd1	silně
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
pozdější	pozdní	k2eAgFnSc4d2	pozdější
poezii	poezie	k1gFnSc4	poezie
<g/>
.	.	kIx.	.
</s>
<s>
Romantičtí	romantický	k2eAgMnPc1d1	romantický
básníci	básník	k1gMnPc1	básník
se	se	k3xPyFc4	se
pokusili	pokusit	k5eAaPmAgMnP	pokusit
oživit	oživit	k5eAaPmF	oživit
shakespearovské	shakespearovský	k2eAgNnSc4d1	shakespearovské
veršované	veršovaný	k2eAgNnSc4d1	veršované
drama	drama	k1gNnSc4	drama
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
s	s	k7c7	s
malým	malý	k2eAgInSc7d1	malý
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Kritik	kritik	k1gMnSc1	kritik
George	Georg	k1gMnSc2	Georg
Steiner	Steiner	k1gMnSc1	Steiner
popsal	popsat	k5eAaPmAgMnS	popsat
všechna	všechen	k3xTgNnPc4	všechen
anglická	anglický	k2eAgNnPc4d1	anglické
veršovaná	veršovaný	k2eAgNnPc4d1	veršované
dramata	drama	k1gNnPc4	drama
od	od	k7c2	od
Coleridge	Coleridg	k1gFnSc2	Coleridg
po	po	k7c4	po
Tennysona	Tennyson	k1gMnSc4	Tennyson
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
slabé	slabý	k2eAgFnPc4d1	slabá
variace	variace	k1gFnPc4	variace
na	na	k7c4	na
shakespearovské	shakespearovský	k2eAgInPc4d1	shakespearovský
motivy	motiv	k1gInPc4	motiv
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
ovlivnil	ovlivnit	k5eAaPmAgMnS	ovlivnit
takové	takový	k3xDgMnPc4	takový
romanopisce	romanopisec	k1gMnPc4	romanopisec
jakými	jaký	k3yRgMnPc7	jaký
byli	být	k5eAaImAgMnP	být
Thomas	Thomas	k1gMnSc1	Thomas
Hardy	Harda	k1gFnSc2	Harda
<g/>
,	,	kIx,	,
William	William	k1gInSc4	William
Faulkner	Faulknra	k1gFnPc2	Faulknra
a	a	k8xC	a
Charles	Charles	k1gMnSc1	Charles
Dickens	Dickensa	k1gFnPc2	Dickensa
<g/>
.	.	kIx.	.
</s>
<s>
Monology	monolog	k1gInPc1	monolog
amerického	americký	k2eAgMnSc2d1	americký
spisovatele	spisovatel	k1gMnSc2	spisovatel
Hermana	Herman	k1gMnSc2	Herman
Melvilleho	Melville	k1gMnSc2	Melville
jsou	být	k5eAaImIp3nP	být
silně	silně	k6eAd1	silně
inspirovány	inspirovat	k5eAaBmNgInP	inspirovat
Shakespearem	Shakespeare	k1gMnSc7	Shakespeare
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gMnSc1	jeho
kapitán	kapitán	k1gMnSc1	kapitán
Achab	Achab	k1gMnSc1	Achab
v	v	k7c6	v
Bílé	bílý	k2eAgFnSc6d1	bílá
velrybě	velryba	k1gFnSc6	velryba
je	být	k5eAaImIp3nS	být
klasickým	klasický	k2eAgMnSc7d1	klasický
tragickým	tragický	k2eAgMnSc7d1	tragický
hrdinou	hrdina	k1gMnSc7	hrdina
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
Krále	Král	k1gMnSc2	Král
Leara	Lear	k1gMnSc2	Lear
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
identifikovali	identifikovat	k5eAaBmAgMnP	identifikovat
20	[number]	k4	20
000	[number]	k4	000
hudebních	hudební	k2eAgNnPc2d1	hudební
děl	dělo	k1gNnPc2	dělo
spojených	spojený	k2eAgMnPc2d1	spojený
se	s	k7c7	s
Shakespearovými	Shakespearův	k2eAgInPc7d1	Shakespearův
díly	díl	k1gInPc7	díl
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gNnPc4	on
dvě	dva	k4xCgNnPc4	dva
i	i	k9	i
opery	opera	k1gFnSc2	opera
Giuseppe	Giusepp	k1gInSc5	Giusepp
Verdiho	Verdi	k1gMnSc2	Verdi
Otello	Otello	k1gNnSc1	Otello
a	a	k8xC	a
Falstaff	Falstaff	k1gInSc1	Falstaff
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
kritika	kritika	k1gFnSc1	kritika
vydrží	vydržet	k5eAaPmIp3nS	vydržet
srovnání	srovnání	k1gNnSc4	srovnání
se	se	k3xPyFc4	se
s	s	k7c7	s
jejich	jejich	k3xOp3gFnPc7	jejich
předlohami	předloha	k1gFnPc7	předloha
<g/>
.	.	kIx.	.
</s>
<s>
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
také	také	k9	také
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
mnoho	mnoho	k4c4	mnoho
malířů	malíř	k1gMnPc2	malíř
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
romantiků	romantik	k1gMnPc2	romantik
a	a	k8xC	a
prerafaelitů	prerafaelita	k1gMnPc2	prerafaelita
<g/>
.	.	kIx.	.
</s>
<s>
Švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
romantický	romantický	k2eAgMnSc1d1	romantický
umělec	umělec	k1gMnSc1	umělec
Henry	henry	k1gInSc4	henry
Fuseli	Fusel	k1gInPc7	Fusel
<g/>
,	,	kIx,	,
přítel	přítel	k1gMnSc1	přítel
Williama	William	k1gMnSc2	William
Blake	Blake	k1gNnSc2	Blake
dokonce	dokonce	k9	dokonce
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Macbetha	Macbeth	k1gMnSc4	Macbeth
do	do	k7c2	do
němčiny	němčina	k1gFnSc2	němčina
<g/>
.	.	kIx.	.
</s>
<s>
Psychoanalýza	psychoanalýza	k1gFnSc1	psychoanalýza
Sigmunda	Sigmund	k1gMnSc2	Sigmund
Freuda	Freud	k1gMnSc2	Freud
čerpala	čerpat	k5eAaImAgFnS	čerpat
ze	z	k7c2	z
Shakespearovské	shakespearovský	k2eAgFnSc2d1	shakespearovská
psychologie	psychologie	k1gFnSc2	psychologie
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
z	z	k7c2	z
Hamleta	Hamlet	k1gMnSc2	Hamlet
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
teorii	teorie	k1gFnSc4	teorie
lidské	lidský	k2eAgFnSc2d1	lidská
povahy	povaha	k1gFnSc2	povaha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Shakespearově	Shakespearův	k2eAgFnSc6d1	Shakespearova
době	doba	k1gFnSc6	doba
anglická	anglický	k2eAgFnSc1d1	anglická
gramatika	gramatika	k1gFnSc1	gramatika
<g/>
,	,	kIx,	,
hláskování	hláskování	k1gNnSc1	hláskování
a	a	k8xC	a
výslovnost	výslovnost	k1gFnSc1	výslovnost
byly	být	k5eAaImAgFnP	být
méně	málo	k6eAd2	málo
standardizovány	standardizován	k2eAgFnPc1d1	standardizována
než	než	k8xS	než
jsou	být	k5eAaImIp3nP	být
teď	teď	k6eAd1	teď
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc3	jeho
použití	použití	k1gNnSc3	použití
jazyka	jazyk	k1gInSc2	jazyk
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
utvářet	utvářet	k5eAaImF	utvářet
moderní	moderní	k2eAgFnSc4d1	moderní
angličtinu	angličtina	k1gFnSc4	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Samuel	Samuel	k1gMnSc1	Samuel
Johnson	Johnson	k1gMnSc1	Johnson
ho	on	k3xPp3gMnSc4	on
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
Slovníku	slovník	k1gInSc6	slovník
anglického	anglický	k2eAgInSc2d1	anglický
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc4	první
seriózní	seriózní	k2eAgFnSc4d1	seriózní
práci	práce	k1gFnSc4	práce
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1755	[number]	k4	1755
<g/>
,	,	kIx,	,
citoval	citovat	k5eAaBmAgMnS	citovat
častěji	často	k6eAd2	často
než	než	k8xS	než
kteréhokoli	kterýkoli	k3yIgMnSc2	kterýkoli
jiného	jiný	k2eAgMnSc2d1	jiný
autora	autor	k1gMnSc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Výrazy	výraz	k1gInPc1	výraz
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
with	with	k1gMnSc1	with
bated	bated	k1gMnSc1	bated
breath	breath	k1gMnSc1	breath
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
se	s	k7c7	s
zatajeným	zatajený	k2eAgInSc7d1	zatajený
dechem	dech	k1gInSc7	dech
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Kupec	kupec	k1gMnSc1	kupec
benátský	benátský	k2eAgMnSc1d1	benátský
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
foregone	foregon	k1gInSc5	foregon
conclusion	conclusion	k1gInSc4	conclusion
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
předem	předem	k6eAd1	předem
učiněný	učiněný	k2eAgInSc1d1	učiněný
závěr	závěr	k1gInSc1	závěr
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Othello	Othello	k1gMnSc1	Othello
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
našly	najít	k5eAaPmAgFnP	najít
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
každodenní	každodenní	k2eAgFnSc2d1	každodenní
anglické	anglický	k2eAgFnSc2d1	anglická
řeči	řeč	k1gFnSc2	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
nebyl	být	k5eNaImAgMnS	být
sice	sice	k8xC	sice
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
uctíván	uctívat	k5eAaImNgMnS	uctívat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dostalo	dostat	k5eAaPmAgNnS	dostat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
řady	řada	k1gFnPc1	řada
uznání	uznání	k1gNnSc2	uznání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1598	[number]	k4	1598
ho	on	k3xPp3gInSc4	on
kněz	kněz	k1gMnSc1	kněz
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
Francis	Francis	k1gFnSc2	Francis
Meres	Meres	k1gMnSc1	Meres
vyčlenil	vyčlenit	k5eAaPmAgMnS	vyčlenit
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
anglických	anglický	k2eAgMnPc2d1	anglický
spisovatelů	spisovatel	k1gMnPc2	spisovatel
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
nejskvělejšího	skvělý	k2eAgNnSc2d3	nejskvělejší
<g/>
"	"	kIx"	"
i	i	k9	i
v	v	k7c6	v
komediích	komedie	k1gFnPc6	komedie
a	a	k8xC	a
tragédiích	tragédie	k1gFnPc6	tragédie
<g/>
.	.	kIx.	.
</s>
<s>
Autoři	autor	k1gMnPc1	autor
tzv.	tzv.	kA	tzv.
Parnaských	parnaský	k2eAgFnPc2d1	parnaský
her	hra	k1gFnPc2	hra
(	(	kIx(	(
<g/>
Parnassus	Parnassus	k1gInSc1	Parnassus
plays	plays	k1gInSc1	plays
<g/>
)	)	kIx)	)
uváděných	uváděný	k2eAgInPc2d1	uváděný
v	v	k7c6	v
koleji	kolej	k1gFnSc6	kolej
St	St	kA	St
John	John	k1gMnSc1	John
College	Colleg	k1gFnSc2	Colleg
v	v	k7c6	v
Cambridge	Cambridge	k1gFnSc1	Cambridge
ho	on	k3xPp3gMnSc4	on
řadili	řadit	k5eAaImAgMnP	řadit
k	k	k7c3	k
Chaucerovi	Chaucer	k1gMnSc3	Chaucer
<g/>
,	,	kIx,	,
Gowerovi	Gower	k1gMnSc3	Gower
a	a	k8xC	a
Spenserovi	Spenser	k1gMnSc3	Spenser
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Prvním	první	k4xOgNnSc6	první
foliu	folio	k1gNnSc6	folio
Ben	Ben	k1gInSc4	Ben
Jonson	Jonson	k1gMnSc1	Jonson
nazval	nazvat	k5eAaPmAgMnS	nazvat
Shakespeara	Shakespeare	k1gMnSc4	Shakespeare
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Duchem	duch	k1gMnSc7	duch
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
potlesku	potlesk	k1gInSc2	potlesk
<g/>
,	,	kIx,	,
potěšení	potěšení	k1gNnSc2	potěšení
<g/>
,	,	kIx,	,
zázrakem	zázrak	k1gInSc7	zázrak
naší	náš	k3xOp1gFnSc2	náš
scény	scéna	k1gFnSc2	scéna
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
návratu	návrat	k1gInSc6	návrat
monarchie	monarchie	k1gFnSc2	monarchie
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1660	[number]	k4	1660
a	a	k8xC	a
koncem	koncem	k7c2	koncem
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
módě	móda	k1gFnSc6	móda
klasické	klasický	k2eAgFnSc2d1	klasická
myšlenky	myšlenka	k1gFnSc2	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
kritici	kritik	k1gMnPc1	kritik
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
většinou	většinou	k6eAd1	většinou
hodnotí	hodnotit	k5eAaImIp3nS	hodnotit
Shakespeara	Shakespeare	k1gMnSc4	Shakespeare
podle	podle	k7c2	podle
Johna	John	k1gMnSc2	John
Fletchera	Fletcher	k1gMnSc2	Fletcher
a	a	k8xC	a
Bena	Bena	k?	Bena
Jonsona	Jonsona	k1gFnSc1	Jonsona
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Thomas	Thomas	k1gMnSc1	Thomas
Rymer	Rymer	k1gMnSc1	Rymer
odsoudil	odsoudit	k5eAaPmAgMnS	odsoudit
Shakespeara	Shakespeare	k1gMnSc4	Shakespeare
pro	pro	k7c4	pro
míchání	míchání	k1gNnPc4	míchání
komického	komický	k2eAgMnSc2d1	komický
s	s	k7c7	s
tragickým	tragický	k2eAgMnSc7d1	tragický
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
kritik	kritik	k1gMnSc1	kritik
John	John	k1gMnSc1	John
Dryden	Drydno	k1gNnPc2	Drydno
velmi	velmi	k6eAd1	velmi
Shakespeara	Shakespeare	k1gMnSc4	Shakespeare
cenil	cenit	k5eAaImAgMnS	cenit
a	a	k8xC	a
řekl	říct	k5eAaPmAgMnS	říct
na	na	k7c4	na
adresu	adresa	k1gFnSc4	adresa
Jonsona	Jonson	k1gMnSc2	Jonson
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
já	já	k3xPp1nSc1	já
ho	on	k3xPp3gNnSc4	on
obdivuji	obdivovat	k5eAaImIp1nS	obdivovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mám	mít	k5eAaImIp1nS	mít
rád	rád	k6eAd1	rád
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
několik	několik	k4yIc4	několik
desetiletí	desetiletí	k1gNnSc2	desetiletí
měl	mít	k5eAaImAgInS	mít
nadvládu	nadvláda	k1gFnSc4	nadvláda
Rymerův	Rymerův	k2eAgInSc1d1	Rymerův
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
během	během	k7c2	během
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	stoletý	k2eAgMnPc1d1	stoletý
kritici	kritik	k1gMnPc1	kritik
začali	začít	k5eAaPmAgMnP	začít
reagovat	reagovat	k5eAaBmF	reagovat
na	na	k7c4	na
Shakespearovy	Shakespearův	k2eAgInPc4d1	Shakespearův
vlastní	vlastní	k2eAgInPc4d1	vlastní
výrazy	výraz	k1gInPc4	výraz
a	a	k8xC	a
uznávali	uznávat	k5eAaImAgMnP	uznávat
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
nazvali	nazvat	k5eAaPmAgMnP	nazvat
jeho	jeho	k3xOp3gFnSc7	jeho
přirozenou	přirozený	k2eAgFnSc7d1	přirozená
genialitou	genialita	k1gFnSc7	genialita
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
vědeckých	vědecký	k2eAgInPc2d1	vědecký
edicíjeho	edicíjeze	k6eAd1	edicíjeze
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Samuela	Samuela	k1gFnSc1	Samuela
Johnsona	Johnsona	k1gFnSc1	Johnsona
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1765	[number]	k4	1765
a	a	k8xC	a
Edmonda	Edmond	k1gMnSc4	Edmond
Malone	Malon	k1gInSc5	Malon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1790	[number]	k4	1790
<g/>
,	,	kIx,	,
přispěla	přispět	k5eAaPmAgFnS	přispět
k	k	k7c3	k
nárůstu	nárůst	k1gInSc3	nárůst
jeho	jeho	k3xOp3gMnPc2	jeho
pověsti	pověst	k1gFnSc2	pověst
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1800	[number]	k4	1800
byl	být	k5eAaImAgInS	být
už	už	k6eAd1	už
pevně	pevně	k6eAd1	pevně
zakotven	zakotvit	k5eAaPmNgInS	zakotvit
jako	jako	k8xS	jako
národní	národní	k2eAgMnSc1d1	národní
básník	básník	k1gMnSc1	básník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
pověst	pověst	k1gFnSc1	pověst
začala	začít	k5eAaPmAgFnS	začít
šířit	šířit	k5eAaImF	šířit
také	také	k9	také
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
těmi	ten	k3xDgMnPc7	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
bojovali	bojovat	k5eAaImAgMnP	bojovat
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
spisovatelé	spisovatel	k1gMnPc1	spisovatel
Voltaire	Voltair	k1gInSc5	Voltair
<g/>
,	,	kIx,	,
Goethe	Goethe	k1gFnSc5	Goethe
<g/>
,	,	kIx,	,
Stendhal	Stendhal	k1gMnSc1	Stendhal
a	a	k8xC	a
Victor	Victor	k1gMnSc1	Victor
Hugo	Hugo	k1gMnSc1	Hugo
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
éry	éra	k1gFnSc2	éra
romantismu	romantismus	k1gInSc2	romantismus
byl	být	k5eAaImAgMnS	být
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
chválen	chválen	k2eAgMnSc1d1	chválen
básníkem	básník	k1gMnSc7	básník
a	a	k8xC	a
literárním	literární	k2eAgMnSc7d1	literární
filozofem	filozof	k1gMnSc7	filozof
Samuelem	Samuel	k1gMnSc7	Samuel
Taylorem	Taylor	k1gMnSc7	Taylor
Coleridgem	Coleridg	k1gMnSc7	Coleridg
<g/>
.	.	kIx.	.
</s>
<s>
Kritik	kritik	k1gMnSc1	kritik
August	August	k1gMnSc1	August
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Schlegel	Schlegel	k1gMnSc1	Schlegel
přeložil	přeložit	k5eAaPmAgMnS	přeložit
jeho	jeho	k3xOp3gFnPc4	jeho
hry	hra	k1gFnPc4	hra
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
německého	německý	k2eAgInSc2d1	německý
romantismu	romantismus	k1gInSc2	romantismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
kritický	kritický	k2eAgInSc4d1	kritický
obdiv	obdiv	k1gInSc4	obdiv
k	k	k7c3	k
Shakespearovu	Shakespearův	k2eAgMnSc3d1	Shakespearův
géniovi	génius	k1gMnSc3	génius
často	často	k6eAd1	často
hraničí	hraničit	k5eAaImIp3nS	hraničit
s	s	k7c7	s
pochlebováním	pochlebování	k1gNnSc7	pochlebování
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
král	král	k1gMnSc1	král
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaPmAgMnS	napsat
esejista	esejista	k1gMnSc1	esejista
Thomas	Thomas	k1gMnSc1	Thomas
Carlyle	Carlyl	k1gInSc5	Carlyl
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1840	[number]	k4	1840
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
nejen	nejen	k6eAd1	nejen
že	že	k8xS	že
záři	záře	k1gFnSc4	záře
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
korunuje	korunovat	k5eAaBmIp3nS	korunovat
svrchovanost	svrchovanost	k1gFnSc4	svrchovanost
nad	nad	k7c7	nad
námi	my	k3xPp1nPc7	my
všemi	všecek	k3xTgFnPc7	všecek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
nejušlechtilejší	ušlechtilý	k2eAgFnSc1d3	nejušlechtilejší
<g/>
,	,	kIx,	,
nejjemnější	jemný	k2eAgFnSc1d3	nejjemnější
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
nejsilnější	silný	k2eAgFnSc1d3	nejsilnější
ze	z	k7c2	z
shromážděných	shromážděný	k2eAgFnPc2d1	shromážděná
<g/>
;	;	kIx,	;
nezničitelný	zničitelný	k2eNgMnSc1d1	nezničitelný
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Ve	v	k7c6	v
viktoriánském	viktoriánský	k2eAgNnSc6d1	viktoriánské
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnPc1	jeho
hry	hra	k1gFnPc1	hra
uváděly	uvádět	k5eAaImAgFnP	uvádět
jako	jako	k9	jako
opulentní	opulentní	k2eAgFnPc1d1	opulentní
podívané	podívaná	k1gFnPc1	podívaná
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
.	.	kIx.	.
</s>
<s>
Dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
kritik	kritik	k1gMnSc1	kritik
George	Georg	k1gMnSc2	Georg
Bernard	Bernard	k1gMnSc1	Bernard
Shaw	Shaw	k1gMnSc1	Shaw
zesměšňoval	zesměšňovat	k5eAaImAgMnS	zesměšňovat
kult	kult	k1gInSc4	kult
uctívání	uctívání	k1gNnSc2	uctívání
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
bardolatrii	bardolatrie	k1gFnSc4	bardolatrie
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
parafráze	parafráze	k1gFnSc1	parafráze
na	na	k7c4	na
idolatrie	idolatrie	k1gFnPc4	idolatrie
=	=	kIx~	=
modloslužebnictví	modloslužebnictví	k1gNnSc1	modloslužebnictví
<g/>
)	)	kIx)	)
s	s	k7c7	s
tvrzením	tvrzení	k1gNnSc7	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
nový	nový	k2eAgInSc1d1	nový
naturalismus	naturalismus	k1gInSc1	naturalismus
Ibsenových	Ibsenových	k2eAgFnPc2d1	Ibsenových
her	hra	k1gFnPc2	hra
učinil	učinit	k5eAaPmAgInS	učinit
Shakespearovy	Shakespearův	k2eAgFnPc4d1	Shakespearova
hry	hra	k1gFnPc4	hra
zastaralými	zastaralý	k2eAgMnPc7d1	zastaralý
<g/>
.	.	kIx.	.
</s>
<s>
Modernistické	modernistický	k2eAgFnPc1d1	modernistická
revoluce	revoluce	k1gFnPc1	revoluce
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Shakespeara	Shakespeare	k1gMnSc4	Shakespeare
odvrhly	odvrhnout	k5eAaPmAgInP	odvrhnout
<g/>
,	,	kIx,	,
nadšeně	nadšeně	k6eAd1	nadšeně
zapojily	zapojit	k5eAaPmAgInP	zapojit
jeho	jeho	k3xOp3gFnSc4	jeho
díla	dílo	k1gNnPc4	dílo
do	do	k7c2	do
služeb	služba	k1gFnPc2	služba
avantgardy	avantgarda	k1gFnSc2	avantgarda
<g/>
.	.	kIx.	.
</s>
<s>
Expresionisté	expresionista	k1gMnPc1	expresionista
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
futuristé	futurista	k1gMnPc1	futurista
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
uváděli	uvádět	k5eAaImAgMnP	uvádět
vlastní	vlastní	k2eAgFnPc4d1	vlastní
inscenace	inscenace	k1gFnPc4	inscenace
jeho	jeho	k3xOp3gFnPc2	jeho
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Marxistický	marxistický	k2eAgMnSc1d1	marxistický
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
Bertolt	Bertolt	k1gMnSc1	Bertolt
Brecht	Brecht	k1gMnSc1	Brecht
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
epické	epický	k2eAgNnSc4d1	epické
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Básník	básník	k1gMnSc1	básník
a	a	k8xC	a
kritik	kritik	k1gMnSc1	kritik
T.	T.	kA	T.
S.	S.	kA	S.
Eliot	Eliot	k1gMnSc1	Eliot
argumentoval	argumentovat	k5eAaImAgMnS	argumentovat
proti	proti	k7c3	proti
Shawovi	Shawa	k1gMnSc3	Shawa
<g/>
,	,	kIx,	,
že	že	k8xS	že
Shakespearova	Shakespearův	k2eAgFnSc1d1	Shakespearova
"	"	kIx"	"
<g/>
surovost	surovost	k1gFnSc1	surovost
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
primitivnost	primitivnost	k1gFnSc4	primitivnost
<g/>
)	)	kIx)	)
ho	on	k3xPp3gNnSc2	on
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
dělá	dělat	k5eAaImIp3nS	dělat
moderním	moderní	k2eAgInSc7d1	moderní
<g/>
.	.	kIx.	.
</s>
<s>
Eliot	Eliot	k1gMnSc1	Eliot
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
literárním	literární	k2eAgMnSc7d1	literární
kritikem	kritik	k1gMnSc7	kritik
G.	G.	kA	G.
Wilson	Wilsona	k1gFnPc2	Wilsona
Knightem	Knight	k1gInSc7	Knight
a	a	k8xC	a
školou	škola	k1gFnSc7	škola
Nové	Nové	k2eAgFnSc2d1	Nové
kritiky	kritika	k1gFnSc2	kritika
vedl	vést	k5eAaImAgMnS	vést
hnutí	hnutí	k1gNnPc4	hnutí
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
užšímu	úzký	k2eAgInSc3d2	užší
výkladu	výklad	k1gInSc3	výklad
Shakespearovy	Shakespearův	k2eAgFnSc2d1	Shakespearova
obraznosti	obraznost	k1gFnSc2	obraznost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
vlna	vlna	k1gFnSc1	vlna
nových	nový	k2eAgInPc2d1	nový
kritických	kritický	k2eAgInPc2d1	kritický
přístupů	přístup	k1gInPc2	přístup
nahradila	nahradit	k5eAaPmAgFnS	nahradit
modernismus	modernismus	k1gInSc4	modernismus
a	a	k8xC	a
vydláždila	vydláždit	k5eAaPmAgFnS	vydláždit
cestu	cesta	k1gFnSc4	cesta
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
postmoderní	postmoderní	k2eAgInSc4d1	postmoderní
<g/>
"	"	kIx"	"
studie	studie	k1gFnSc1	studie
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byla	být	k5eAaImAgNnP	být
Shakespearovská	shakespearovský	k2eAgNnPc1d1	shakespearovské
studia	studio	k1gNnPc1	studio
otevřena	otevřít	k5eAaPmNgNnP	otevřít
i	i	k9	i
pro	pro	k7c4	pro
hnutí	hnutí	k1gNnPc4	hnutí
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
strukturalismus	strukturalismus	k1gInSc1	strukturalismus
<g/>
,	,	kIx,	,
feminismus	feminismus	k1gInSc1	feminismus
<g/>
,	,	kIx,	,
nový	nový	k2eAgInSc1d1	nový
historismus	historismus	k1gInSc1	historismus
<g/>
,	,	kIx,	,
afroamerická	afroamerický	k2eAgNnPc1d1	afroamerické
studia	studio	k1gNnPc1	studio
a	a	k8xC	a
queer	queer	k1gInSc1	queer
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obsáhlém	obsáhlý	k2eAgInSc6d1	obsáhlý
výkladu	výklad	k1gInSc6	výklad
Shakespearových	Shakespearových	k2eAgNnPc2d1	Shakespearových
děl	dělo	k1gNnPc2	dělo
a	a	k8xC	a
při	při	k7c6	při
přirovnání	přirovnání	k1gNnSc6	přirovnání
Shakespearových	Shakespearových	k2eAgInPc2d1	Shakespearových
literárních	literární	k2eAgInPc2d1	literární
úspěchů	úspěch	k1gInPc2	úspěch
k	k	k7c3	k
úspěchům	úspěch	k1gInPc3	úspěch
mezi	mezi	k7c7	mezi
vůdčími	vůdčí	k2eAgFnPc7d1	vůdčí
postavami	postava	k1gFnPc7	postava
filozofie	filozofie	k1gFnSc2	filozofie
a	a	k8xC	a
teologie	teologie	k1gFnSc2	teologie
literární	literární	k2eAgMnSc1d1	literární
kritik	kritik	k1gMnSc1	kritik
Harold	Harold	k1gMnSc1	Harold
Bloom	Bloom	k1gInSc4	Bloom
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
byl	být	k5eAaImAgMnS	být
větší	veliký	k2eAgInSc4d2	veliký
než	než	k8xS	než
Platón	platón	k1gInSc4	platón
a	a	k8xC	a
než	než	k8xS	než
Svatý	svatý	k2eAgMnSc1d1	svatý
Augustin	Augustin	k1gMnSc1	Augustin
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gInSc1	on
nás	my	k3xPp1nPc4	my
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
chápeme	chápat	k5eAaImIp1nP	chápat
jeho	jeho	k3xOp3gNnPc4	jeho
základní	základní	k2eAgNnPc4d1	základní
vnímání	vnímání	k1gNnPc4	vnímání
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
byl	být	k5eAaImAgMnS	být
již	již	k6eAd1	již
za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
významný	významný	k2eAgMnSc1d1	významný
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
tehdy	tehdy	k6eAd1	tehdy
údajně	údajně	k6eAd1	údajně
známý	známý	k2eAgInSc1d1	známý
dokonce	dokonce	k9	dokonce
i	i	k9	i
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
českému	český	k2eAgInSc3d1	český
překladu	překlad	k1gInSc3	překlad
(	(	kIx(	(
<g/>
adaptaci	adaptace	k1gFnSc4	adaptace
<g/>
)	)	kIx)	)
Shakespearova	Shakespearův	k2eAgNnPc4d1	Shakespearovo
díla	dílo	k1gNnPc4	dílo
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1786	[number]	k4	1786
zásluhou	zásluhou	k7c2	zásluhou
Karla	Karel	k1gMnSc2	Karel
Ignáce	Ignác	k1gMnSc2	Ignác
Tháma	Thám	k1gMnSc2	Thám
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Macbetha	Macbeth	k1gMnSc4	Macbeth
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejstarším	starý	k2eAgMnPc3d3	nejstarší
překladatelům	překladatel	k1gMnPc3	překladatel
patřili	patřit	k5eAaImAgMnP	patřit
dále	daleko	k6eAd2	daleko
Josef	Josef	k1gMnSc1	Josef
Kajetán	Kajetán	k1gMnSc1	Kajetán
Tyl	Tyl	k1gMnSc1	Tyl
a	a	k8xC	a
Josef	Josef	k1gMnSc1	Josef
Jiří	Jiří	k1gMnSc1	Jiří
Kolár	Kolár	k1gMnSc1	Kolár
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tzv.	tzv.	kA	tzv.
druhé	druhý	k4xOgFnPc4	druhý
generace	generace	k1gFnPc1	generace
překladatelů	překladatel	k1gMnPc2	překladatel
<g/>
,	,	kIx,	,
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
František	František	k1gMnSc1	František
Doucha	Doucha	k1gMnSc1	Doucha
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Josef	Josef	k1gMnSc1	Josef
Čejka	Čejka	k1gMnSc1	Čejka
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Ladislav	Ladislav	k1gMnSc1	Ladislav
Čelakovský	Čelakovský	k2eAgMnSc1d1	Čelakovský
a	a	k8xC	a
Jakub	Jakub	k1gMnSc1	Jakub
Malý	Malý	k1gMnSc1	Malý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
překládali	překládat	k5eAaImAgMnP	překládat
Josef	Josef	k1gMnSc1	Josef
Václav	Václav	k1gMnSc1	Václav
Frič	Frič	k1gMnSc1	Frič
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Václav	Václav	k1gMnSc1	Václav
Sládek	Sládek	k1gMnSc1	Sládek
a	a	k8xC	a
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Vrchlický	Vrchlický	k1gMnSc1	Vrchlický
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgMnS	být
Otokar	Otokar	k1gMnSc1	Otokar
Fischer	Fischer	k1gMnSc1	Fischer
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Fencl	Fencl	k1gMnSc1	Fencl
a	a	k8xC	a
Bohumil	Bohumil	k1gMnSc1	Bohumil
Štěpánek	Štěpánek	k1gMnSc1	Štěpánek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
generaci	generace	k1gFnSc3	generace
<g/>
,	,	kIx,	,
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
překládali	překládat	k5eAaImAgMnP	překládat
Erik	Erik	k1gMnSc1	Erik
Adolf	Adolf	k1gMnSc1	Adolf
Saudek	Saudek	k1gMnSc1	Saudek
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Valja	Valja	k1gMnSc1	Valja
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Nevrla	nevrla	k1gMnSc1	nevrla
<g/>
,	,	kIx,	,
Otto	Otto	k1gMnSc1	Otto
František	František	k1gMnSc1	František
Babler	Babler	k1gMnSc1	Babler
<g/>
,	,	kIx,	,
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
generace	generace	k1gFnSc2	generace
<g/>
,	,	kIx,	,
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
a	a	k8xC	a
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc4	ten
byli	být	k5eAaImAgMnP	být
Aloys	Aloys	k1gInSc4	Aloys
Skoumal	Skoumal	k1gMnSc1	Skoumal
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Urbánek	Urbánek	k1gMnSc1	Urbánek
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Topol	Topol	k1gMnSc1	Topol
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Renč	Renč	k?	Renč
a	a	k8xC	a
Břetislav	Břetislav	k1gMnSc1	Břetislav
Hodek	Hodek	k1gMnSc1	Hodek
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tzv.	tzv.	kA	tzv.
moderním	moderní	k2eAgMnPc3d1	moderní
překladatelům	překladatel	k1gMnPc3	překladatel
patří	patřit	k5eAaImIp3nP	patřit
Alois	Alois	k1gMnSc1	Alois
Bejblík	Bejblík	k1gMnSc1	Bejblík
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Hilský	Hilský	k1gMnSc1	Hilský
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Josek	Josek	k1gMnSc1	Josek
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Lukeš	Lukeš	k1gMnSc1	Lukeš
a	a	k8xC	a
Antonín	Antonín	k1gMnSc1	Antonín
Přidal	přidat	k5eAaPmAgMnS	přidat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
tvorba	tvorba	k1gFnSc1	tvorba
bývá	bývat	k5eAaImIp3nS	bývat
dělena	dělit	k5eAaImNgFnS	dělit
do	do	k7c2	do
tří	tři	k4xCgNnPc2	tři
období	období	k1gNnPc2	období
<g/>
:	:	kIx,	:
1591	[number]	k4	1591
<g/>
–	–	k?	–
<g/>
1600	[number]	k4	1600
<g/>
:	:	kIx,	:
Psal	psát	k5eAaImAgMnS	psát
především	především	k6eAd1	především
komedie	komedie	k1gFnSc2	komedie
a	a	k8xC	a
historická	historický	k2eAgNnPc1d1	historické
dramata	drama	k1gNnPc1	drama
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
zpracovával	zpracovávat	k5eAaImAgMnS	zpracovávat
staré	starý	k2eAgInPc4d1	starý
náměty	námět	k1gInPc4	námět
z	z	k7c2	z
anglické	anglický	k2eAgFnSc2d1	anglická
minulosti	minulost	k1gFnSc2	minulost
a	a	k8xC	a
z	z	k7c2	z
antiky	antika	k1gFnSc2	antika
<g/>
.	.	kIx.	.
1601	[number]	k4	1601
<g/>
–	–	k?	–
<g/>
1608	[number]	k4	1608
<g/>
:	:	kIx,	:
Přichází	přicházet	k5eAaImIp3nS	přicházet
zklamání	zklamání	k1gNnSc1	zklamání
a	a	k8xC	a
rozčarování	rozčarování	k1gNnSc1	rozčarování
nad	nad	k7c7	nad
vývojem	vývoj	k1gInSc7	vývoj
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
tvorby	tvorba	k1gFnSc2	tvorba
vniká	vnikat	k5eAaImIp3nS	vnikat
pesimismus	pesimismus	k1gInSc4	pesimismus
a	a	k8xC	a
píše	psát	k5eAaImIp3nS	psát
tragédie	tragédie	k1gFnSc1	tragédie
a	a	k8xC	a
sonety	sonet	k1gInPc1	sonet
<g/>
.	.	kIx.	.
1608	[number]	k4	1608
<g/>
–	–	k?	–
<g/>
1612	[number]	k4	1612
<g/>
:	:	kIx,	:
Smiřuje	smiřovat	k5eAaImIp3nS	smiřovat
se	se	k3xPyFc4	se
s	s	k7c7	s
životem	život	k1gInSc7	život
a	a	k8xC	a
píše	psát	k5eAaImIp3nS	psát
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
mající	mající	k2eAgInSc1d1	mající
charakter	charakter	k1gInSc1	charakter
tzv.	tzv.	kA	tzv.
romance	romance	k1gFnSc2	romance
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
obsahující	obsahující	k2eAgInPc1d1	obsahující
jak	jak	k8xC	jak
prvky	prvek	k1gInPc1	prvek
tragédie	tragédie	k1gFnSc2	tragédie
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
prvky	prvek	k1gInPc4	prvek
komedie	komedie	k1gFnSc2	komedie
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Jan	Jan	k1gMnSc1	Jan
(	(	kIx(	(
<g/>
1596	[number]	k4	1596
<g/>
–	–	k?	–
<g/>
1597	[number]	k4	1597
<g/>
,	,	kIx,	,
King	King	k1gMnSc1	King
John	John	k1gMnSc1	John
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1595	[number]	k4	1595
<g/>
–	–	k?	–
<g/>
1596	[number]	k4	1596
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1597	[number]	k4	1597
<g/>
–	–	k?	–
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1598	[number]	k4	1598
<g/>
,	,	kIx,	,
Henry	henry	k1gInSc1	henry
IV	Iva	k1gFnPc2	Iva
<g/>
,	,	kIx,	,
Part	part	k1gInSc1	part
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1597	[number]	k4	1597
<g/>
–	–	k?	–
<g/>
1598	[number]	k4	1598
<g/>
,	,	kIx,	,
Henry	henry	k1gInSc1	henry
IV	Iva	k1gFnPc2	Iva
<g/>
,	,	kIx,	,
Part	part	k1gInSc1	part
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
V.	V.	kA	V.
(	(	kIx(	(
<g/>
1598	[number]	k4	1598
<g/>
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
<g/>
1599	[number]	k4	1599
<g/>
,	,	kIx,	,
Henry	Henry	k1gMnSc1	Henry
V	V	kA	V
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
VI	VI	kA	VI
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1591	[number]	k4	1591
<g/>
–	–	k?	–
<g/>
1592	[number]	k4	1592
<g/>
,	,	kIx,	,
Henry	henry	k1gInSc1	henry
VI	VI	kA	VI
<g/>
,	,	kIx,	,
Part	part	k1gInSc1	part
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
VI	VI	kA	VI
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
<g />
.	.	kIx.	.
</s>
<s>
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
((	((	k?	((
<g/>
1590	[number]	k4	1590
<g/>
–	–	k?	–
<g/>
1591	[number]	k4	1591
<g/>
,	,	kIx,	,
Henry	henry	k1gInSc1	henry
VI	VI	kA	VI
<g/>
,	,	kIx,	,
Part	part	k1gInSc1	part
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
VI	VI	kA	VI
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1590	[number]	k4	1590
<g/>
–	–	k?	–
<g/>
1591	[number]	k4	1591
<g/>
,	,	kIx,	,
Henry	henry	k1gInSc1	henry
VI	VI	kA	VI
<g/>
,	,	kIx,	,
Part	part	k1gInSc1	part
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1592	[number]	k4	1592
<g/>
–	–	k?	–
<g/>
1593	[number]	k4	1593
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1612	[number]	k4	1612
<g/>
–	–	k?	–
<g/>
1613	[number]	k4	1613
<g/>
,	,	kIx,	,
Henry	Henry	k1gMnSc1	Henry
VIII	VIII	kA	VIII
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
Johnem	John	k1gMnSc7	John
Fletcherem	Fletcher	k1gMnSc7	Fletcher
<g/>
.	.	kIx.	.
</s>
<s>
Komedie	komedie	k1gFnSc1	komedie
omylů	omyl	k1gInPc2	omyl
(	(	kIx(	(
<g/>
1593	[number]	k4	1593
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Comedy	Comeda	k1gMnSc2	Comeda
of	of	k?	of
Errors	Errors	k1gInSc1	Errors
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zkrocení	zkrocení	k1gNnSc1	zkrocení
zlé	zlý	k2eAgFnSc2d1	zlá
ženy	žena	k1gFnSc2	žena
(	(	kIx(	(
<g/>
1594	[number]	k4	1594
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Taming	Taming	k1gInSc1	Taming
of	of	k?	of
the	the	k?	the
Shrew	Shrew	k1gFnSc2	Shrew
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dva	dva	k4xCgMnPc1	dva
šlechtici	šlechtic	k1gMnPc1	šlechtic
z	z	k7c2	z
Verony	Verona	k1gFnSc2	Verona
(	(	kIx(	(
<g/>
1595	[number]	k4	1595
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Two	Two	k1gMnSc1	Two
Gentlemen	Gentlemen	k1gInSc1	Gentlemen
of	of	k?	of
Verona	Verona	k1gFnSc1	Verona
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Marná	marný	k2eAgFnSc1d1	marná
lásky	láska	k1gFnSc2	láska
snaha	snaha	k1gFnSc1	snaha
(	(	kIx(	(
<g/>
1595	[number]	k4	1595
<g/>
,	,	kIx,	,
Love	lov	k1gInSc5	lov
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Labor	Labora	k1gFnPc2	Labora
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Lost	Losta	k1gFnPc2	Losta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sen	sen	k1gInSc1	sen
noci	noc	k1gFnSc2	noc
svatojánské	svatojánský	k2eAgFnSc2d1	Svatojánská
(	(	kIx(	(
<g/>
1596	[number]	k4	1596
<g/>
,	,	kIx,	,
A	a	k9	a
Midsummer-night	Midsummeright	k1gInSc1	Midsummer-night
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Dream	Dream	k1gInSc1	Dream
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kupec	kupec	k1gMnSc1	kupec
benátský	benátský	k2eAgMnSc1d1	benátský
(	(	kIx(	(
<g/>
1597	[number]	k4	1597
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
<g />
.	.	kIx.	.
</s>
<s>
Merchant	Merchant	k1gInSc1	Merchant
of	of	k?	of
Venice	Venice	k1gFnSc2	Venice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mnoho	mnoho	k6eAd1	mnoho
povyku	povyk	k1gInSc2	povyk
pro	pro	k7c4	pro
nic	nic	k6eAd1	nic
(	(	kIx(	(
<g/>
1599	[number]	k4	1599
<g/>
,	,	kIx,	,
Much	moucha	k1gFnPc2	moucha
Ado	Ado	k1gFnPc2	Ado
About	About	k2eAgInSc4d1	About
Nothing	Nothing	k1gInSc4	Nothing
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Veselé	Veselé	k2eAgFnSc2d1	Veselé
paničky	panička	k1gFnSc2	panička
windsorské	windsorský	k2eAgFnSc2d1	Windsorská
(	(	kIx(	(
<g/>
1601	[number]	k4	1601
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Merry	Merra	k1gFnSc2	Merra
Wives	Wives	k1gMnSc1	Wives
of	of	k?	of
Windsor	Windsor	k1gInSc1	Windsor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jak	jak	k8xC	jak
se	se	k3xPyFc4	se
vám	vy	k3xPp2nPc3	vy
líbí	líbit	k5eAaImIp3nS	líbit
(	(	kIx(	(
<g/>
1601	[number]	k4	1601
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
As	as	k9	as
You	You	k1gFnSc1	You
Like	Like	k1gFnSc1	Like
It	It	k1gFnSc1	It
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Večer	večer	k6eAd1	večer
tříkrálový	tříkrálový	k2eAgMnSc1d1	tříkrálový
(	(	kIx(	(
<g/>
1602	[number]	k4	1602
<g/>
,	,	kIx,	,
Twelfth	Twelfth	k1gMnSc1	Twelfth
Night	Night	k1gMnSc1	Night
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dobrý	dobrý	k2eAgInSc1d1	dobrý
konec	konec	k1gInSc1	konec
vše	všechen	k3xTgNnSc4	všechen
napraví	napravit	k5eAaPmIp3nS	napravit
(	(	kIx(	(
<g/>
1603	[number]	k4	1603
<g/>
,	,	kIx,	,
All	All	k1gFnSc1	All
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Well	Well	k1gInSc1	Well
That	That	k1gMnSc1	That
Ends	Endsa	k1gFnPc2	Endsa
Well	Well	k1gMnSc1	Well
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Něco	něco	k3yInSc1	něco
za	za	k7c4	za
něco	něco	k3yInSc4	něco
(	(	kIx(	(
<g/>
1606	[number]	k4	1606
<g/>
,	,	kIx,	,
Measure	Measur	k1gMnSc5	Measur
for	forum	k1gNnPc2	forum
Measure	Measur	k1gMnSc5	Measur
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Titus	Titus	k1gMnSc1	Titus
Andronicus	Andronicus	k1gMnSc1	Andronicus
(	(	kIx(	(
<g/>
1594	[number]	k4	1594
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Romeo	Romeo	k1gMnSc1	Romeo
a	a	k8xC	a
Julie	Julie	k1gFnSc1	Julie
(	(	kIx(	(
<g/>
1595	[number]	k4	1595
<g/>
,	,	kIx,	,
Romeo	Romeo	k1gMnSc1	Romeo
and	and	k?	and
Juliet	Juliet	k1gMnSc1	Juliet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Julius	Julius	k1gMnSc1	Julius
Caesar	Caesar	k1gMnSc1	Caesar
(	(	kIx(	(
<g/>
1599	[number]	k4	1599
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hamlet	Hamlet	k1gMnSc1	Hamlet
(	(	kIx(	(
<g/>
1604	[number]	k4	1604
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Othello	Othello	k1gMnSc1	Othello
(	(	kIx(	(
<g/>
1604	[number]	k4	1604
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Macbeth	Macbeth	k1gMnSc1	Macbeth
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1606	[number]	k4	1606
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Král	Král	k1gMnSc1	Král
Lear	Lear	k1gMnSc1	Lear
(	(	kIx(	(
<g/>
1606	[number]	k4	1606
<g/>
,	,	kIx,	,
King	King	k1gMnSc1	King
Lear	Lear	k1gMnSc1	Lear
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Antonius	Antonius	k1gInSc1	Antonius
a	a	k8xC	a
Kleopatra	Kleopatra	k1gFnSc1	Kleopatra
(	(	kIx(	(
<g/>
1607	[number]	k4	1607
<g/>
,	,	kIx,	,
Antonius	Antonius	k1gInSc1	Antonius
and	and	k?	and
Cleopatra	Cleopatra	k1gFnSc1	Cleopatra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Coriolanus	Coriolanus	k1gMnSc1	Coriolanus
(	(	kIx(	(
<g/>
1608	[number]	k4	1608
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Timon	Timon	k1gMnSc1	Timon
Athénský	athénský	k2eAgMnSc1d1	athénský
(	(	kIx(	(
<g/>
1608	[number]	k4	1608
<g/>
,	,	kIx,	,
Timon	Timon	k1gInSc1	Timon
of	of	k?	of
Athens	Athens	k1gInSc1	Athens
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Troilus	Troilus	k1gInSc1	Troilus
a	a	k8xC	a
Kressida	Kressida	k1gFnSc1	Kressida
(	(	kIx(	(
<g/>
1608	[number]	k4	1608
<g/>
,	,	kIx,	,
Troilus	Troilus	k1gInSc1	Troilus
and	and	k?	and
Cressida	Cressida	k1gFnSc1	Cressida
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cymbelín	Cymbelín	k1gInSc1	Cymbelín
(	(	kIx(	(
<g/>
1609	[number]	k4	1609
<g/>
,	,	kIx,	,
Cymbeline	Cymbelin	k1gInSc5	Cymbelin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zimní	zimní	k2eAgFnSc1d1	zimní
pohádka	pohádka	k1gFnSc1	pohádka
(	(	kIx(	(
<g/>
1611	[number]	k4	1611
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Winter	Winter	k1gMnSc1	Winter
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Tale	Tale	k1gFnSc7	Tale
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Perikles	Perikles	k1gMnSc1	Perikles
(	(	kIx(	(
<g/>
1609	[number]	k4	1609
<g/>
,	,	kIx,	,
Pericles	Pericles	k1gInSc1	Pericles
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bouře	bouře	k1gFnSc1	bouře
(	(	kIx(	(
<g/>
1612	[number]	k4	1612
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Tempest	Tempest	k1gFnSc1	Tempest
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dva	dva	k4xCgMnPc1	dva
vznešení	vznešený	k2eAgMnPc1d1	vznešený
příbuzní	příbuzný	k1gMnPc1	příbuzný
(	(	kIx(	(
<g/>
1613	[number]	k4	1613
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Two	Two	k1gFnSc1	Two
Noble	Noble	k1gInSc4	Noble
Kinsmen	Kinsmen	k1gInSc1	Kinsmen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
Johnem	John	k1gMnSc7	John
Fletcherem	Fletcher	k1gMnSc7	Fletcher
<g/>
,	,	kIx,	,
Cardenio	Cardenio	k1gNnSc1	Cardenio
(	(	kIx(	(
<g/>
provedena	proveden	k2eAgFnSc1d1	provedena
1613	[number]	k4	1613
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
Johnem	John	k1gMnSc7	John
Fletcherem	Fletcher	k1gMnSc7	Fletcher
<g/>
,	,	kIx,	,
hra	hra	k1gFnSc1	hra
se	se	k3xPyFc4	se
nedochovala	dochovat	k5eNaPmAgFnS	dochovat
<g/>
.	.	kIx.	.
</s>
<s>
Venuše	Venuše	k1gFnSc1	Venuše
a	a	k8xC	a
Adonis	Adonis	k1gFnSc1	Adonis
(	(	kIx(	(
<g/>
1593	[number]	k4	1593
<g/>
,	,	kIx,	,
Venus	Venus	k1gInSc1	Venus
and	and	k?	and
Adonis	Adonis	k1gInSc1	Adonis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Znásilnění	znásilnění	k1gNnSc1	znásilnění
Lukrecie	Lukrecie	k1gFnSc2	Lukrecie
(	(	kIx(	(
<g/>
1594	[number]	k4	1594
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Rape	rape	k1gNnSc2	rape
of	of	k?	of
Lucrece	Lucrece	k1gMnSc1	Lucrece
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vášnivý	vášnivý	k2eAgMnSc1d1	vášnivý
poutník	poutník	k1gMnSc1	poutník
(	(	kIx(	(
<g/>
1599	[number]	k4	1599
<g/>
,	,	kIx,	,
The	The	k1gMnSc5	The
Passionate	Passionat	k1gMnSc5	Passionat
Pilgrim	Pilgrimo	k1gNnPc2	Pilgrimo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Fénix	fénix	k1gMnSc1	fénix
a	a	k8xC	a
hrdlička	hrdlička	k1gFnSc1	hrdlička
(	(	kIx(	(
<g/>
1601	[number]	k4	1601
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
The	The	k1gFnPc1	The
Phoenix	Phoenix	k1gInSc1	Phoenix
and	and	k?	and
the	the	k?	the
Turtle	Turtle	k1gFnSc2	Turtle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Shakespearovy	Shakespearův	k2eAgInPc4d1	Shakespearův
<g/>
)	)	kIx)	)
Sonety	sonet	k1gInPc4	sonet
(	(	kIx(	(
<g/>
vydány	vydán	k2eAgFnPc4d1	vydána
1609	[number]	k4	1609
<g/>
,	,	kIx,	,
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Shake-Speares	Shake-Speares	k1gMnSc1	Shake-Speares
Sonnets	Sonnets	k1gInSc1	Sonnets
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nářek	nářek	k1gInSc1	nářek
milenčin	milenčin	k2eAgInSc1d1	milenčin
(	(	kIx(	(
<g/>
1609	[number]	k4	1609
<g/>
,	,	kIx,	,
A	a	k9	a
Lover	Lover	k1gInSc1	Lover
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Complaint	Complainta	k1gFnPc2	Complainta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Locrine	Locrin	k1gInSc5	Locrin
(	(	kIx(	(
<g/>
1594	[number]	k4	1594
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tragédie	tragédie	k1gFnSc2	tragédie
<g/>
,	,	kIx,	,
Sir	sir	k1gMnSc1	sir
Thomas	Thomas	k1gMnSc1	Thomas
More	mor	k1gInSc5	mor
(	(	kIx(	(
<g/>
1591	[number]	k4	1591
<g/>
–	–	k?	–
<g/>
1596	[number]	k4	1596
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historická	historický	k2eAgFnSc1d1	historická
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
podíleli	podílet	k5eAaImAgMnP	podílet
ještě	ještě	k9	ještě
Thomas	Thomas	k1gMnSc1	Thomas
Dekker	Dekker	k1gMnSc1	Dekker
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
Heywood	Heywooda	k1gFnPc2	Heywooda
<g/>
,	,	kIx,	,
Anthony	Anthona	k1gFnSc2	Anthona
Munday	Mundaa	k1gFnSc2	Mundaa
a	a	k8xC	a
Henry	Henry	k1gMnSc1	Henry
Chettle	Chettle	k1gFnSc2	Chettle
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Edward	Edward	k1gMnSc1	Edward
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anonymně	anonymně	k6eAd1	anonymně
1596	[number]	k4	1596
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historická	historický	k2eAgFnSc1d1	historická
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
společně	společně	k6eAd1	společně
s	s	k7c7	s
Thomasem	Thomas	k1gMnSc7	Thomas
Kydem	kyd	k1gInSc7	kyd
<g/>
,	,	kIx,	,
Sir	sir	k1gMnSc1	sir
John	John	k1gMnSc1	John
Oldcastle	Oldcastle	k1gMnSc1	Oldcastle
(	(	kIx(	(
<g/>
anonymně	anonymně	k6eAd1	anonymně
1600	[number]	k4	1600
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historická	historický	k2eAgFnSc1d1	historická
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
Lord	lord	k1gMnSc1	lord
Cromwell	Cromwell	k1gMnSc1	Cromwell
(	(	kIx(	(
<g/>
uvedeno	uvést	k5eAaPmNgNnS	uvést
1602	[number]	k4	1602
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Londýnský	londýnský	k2eAgMnSc1d1	londýnský
marnotratník	marnotratník	k1gMnSc1	marnotratník
(	(	kIx(	(
<g/>
tiskem	tisk	k1gInSc7	tisk
1605	[number]	k4	1605
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
London	London	k1gMnSc1	London
Prodigal	Prodigal	k1gMnSc1	Prodigal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
komedie	komedie	k1gFnSc1	komedie
<g/>
,	,	kIx,	,
Merlinovo	Merlinův	k2eAgNnSc1d1	Merlinovo
narození	narození	k1gNnSc1	narození
(	(	kIx(	(
<g/>
prvně	prvně	k?	prvně
uvedeno	uvést	k5eAaPmNgNnS	uvést
1622	[number]	k4	1622
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Birth	Birth	k1gInSc1	Birth
of	of	k?	of
Merlin	Merlin	k1gInSc1	Merlin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
komedie	komedie	k1gFnSc2	komedie
<g/>
,	,	kIx,	,
připisováno	připisovat	k5eAaImNgNnS	připisovat
Shakespearovi	Shakespeare	k1gMnSc3	Shakespeare
a	a	k8xC	a
Williamovi	William	k1gMnSc3	William
Rowleyovi	Rowleya	k1gMnSc3	Rowleya
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Spor	spor	k1gInSc4	spor
o	o	k7c4	o
Shakespearovo	Shakespearův	k2eAgNnSc4d1	Shakespearovo
autorství	autorství	k1gNnSc4	autorství
<g/>
.	.	kIx.	.
</s>
<s>
Autorství	autorství	k1gNnSc1	autorství
děl	dělo	k1gNnPc2	dělo
připisovaných	připisovaný	k2eAgFnPc2d1	připisovaná
Williamu	William	k1gInSc2	William
Shakespearovi	Shakespeare	k1gMnSc3	Shakespeare
bylo	být	k5eAaImAgNnS	být
vícekrát	vícekrát	k6eAd1	vícekrát
zpochybňováno	zpochybňován	k2eAgNnSc1d1	zpochybňováno
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
antistratfordiánů	antistratfordián	k1gInPc2	antistratfordián
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
kolektivní	kolektivní	k2eAgNnSc4d1	kolektivní
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
příznivce	příznivec	k1gMnPc4	příznivec
všech	všecek	k3xTgFnPc2	všecek
alternativních	alternativní	k2eAgFnPc2d1	alternativní
teorií	teorie	k1gFnPc2	teorie
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
ze	z	k7c2	z
Stratfordu	Stratford	k1gInSc2	Stratford
"	"	kIx"	"
<g/>
nastrčenou	nastrčený	k2eAgFnSc7d1	nastrčená
postavou	postava	k1gFnSc7	postava
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
krýt	krýt	k5eAaImF	krýt
totožnost	totožnost	k1gFnSc1	totožnost
skutečného	skutečný	k2eAgMnSc2d1	skutečný
autora	autor	k1gMnSc2	autor
nebo	nebo	k8xC	nebo
autorů	autor	k1gMnPc2	autor
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
z	z	k7c2	z
nějakého	nějaký	k3yIgInSc2	nějaký
důvodu	důvod	k1gInSc2	důvod
chtěli	chtít	k5eAaImAgMnP	chtít
vyhnout	vyhnout	k5eAaPmF	vyhnout
veřejné	veřejný	k2eAgFnSc2d1	veřejná
známosti	známost	k1gFnSc2	známost
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
spojenému	spojený	k2eAgNnSc3d1	spojené
uznání	uznání	k1gNnSc3	uznání
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
je	on	k3xPp3gNnPc4	on
nemohli	moct	k5eNaImAgMnP	moct
či	či	k8xC	či
nesměli	smět	k5eNaImAgMnP	smět
přijmout	přijmout	k5eAaPmF	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
tato	tento	k3xDgFnSc1	tento
myšlenka	myšlenka	k1gFnSc1	myšlenka
získala	získat	k5eAaPmAgFnS	získat
značnou	značný	k2eAgFnSc4d1	značná
pozornost	pozornost	k1gFnSc4	pozornost
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
,	,	kIx,	,
jen	jen	k9	jen
menšina	menšina	k1gFnSc1	menšina
odborníků	odborník	k1gMnPc2	odborník
zaměřených	zaměřený	k2eAgFnPc2d1	zaměřená
na	na	k7c4	na
výzkum	výzkum	k1gInSc4	výzkum
Shakespearova	Shakespearův	k2eAgNnSc2d1	Shakespearovo
díla	dílo	k1gNnSc2	dílo
a	a	k8xC	a
literárních	literární	k2eAgMnPc2d1	literární
historiků	historik	k1gMnPc2	historik
ji	on	k3xPp3gFnSc4	on
pokládá	pokládat	k5eAaImIp3nS	pokládat
za	za	k7c4	za
hodnověrnou	hodnověrný	k2eAgFnSc4d1	hodnověrná
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
shakespearologů	shakespearolog	k1gMnPc2	shakespearolog
jsou	být	k5eAaImIp3nP	být
zastánci	zastánce	k1gMnPc1	zastánce
tradičního	tradiční	k2eAgInSc2d1	tradiční
názoru	názor	k1gInSc2	názor
a	a	k8xC	a
zabývají	zabývat	k5eAaImIp3nP	zabývat
se	se	k3xPyFc4	se
alternativními	alternativní	k2eAgFnPc7d1	alternativní
teoriemi	teorie	k1gFnPc7	teorie
jen	jen	k9	jen
s	s	k7c7	s
úmyslem	úmysl	k1gInSc7	úmysl
je	být	k5eAaImIp3nS	být
vyvrátit	vyvrátit	k5eAaPmF	vyvrátit
<g/>
.	.	kIx.	.
</s>
<s>
Stratfordiáni	Stratfordián	k1gMnPc1	Stratfordián
odmítají	odmítat	k5eAaImIp3nP	odmítat
všechny	všechen	k3xTgInPc4	všechen
argumenty	argument	k1gInPc4	argument
podávané	podávaný	k2eAgInPc4d1	podávaný
zastánci	zastánce	k1gMnPc1	zastánce
jiných	jiný	k2eAgFnPc2d1	jiná
teorií	teorie	k1gFnPc2	teorie
o	o	k7c6	o
autorství	autorství	k1gNnSc6	autorství
Shakespearova	Shakespearův	k2eAgInSc2d1	Shakespearův
kánonu	kánon	k1gInSc2	kánon
jako	jako	k8xS	jako
neopodstatněné	opodstatněný	k2eNgFnPc1d1	neopodstatněná
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
nejpopulárnější	populární	k2eAgFnSc4d3	nejpopulárnější
alternativní	alternativní	k2eAgFnSc4d1	alternativní
teorii	teorie	k1gFnSc4	teorie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
připisuje	připisovat	k5eAaImIp3nS	připisovat
toto	tento	k3xDgNnSc4	tento
autorství	autorství	k1gNnSc4	autorství
Edwardovi	Edwardův	k2eAgMnPc1d1	Edwardův
de	de	k?	de
Vere	Ver	k1gFnPc4	Ver
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
.	.	kIx.	.
hraběti	hrabě	k1gMnSc6	hrabě
z	z	k7c2	z
Oxfordu	Oxford	k1gInSc2	Oxford
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c1	mnoho
zastánců	zastánce	k1gMnPc2	zastánce
stratfordské	stratfordský	k2eAgFnSc2d1	stratfordský
teorie	teorie	k1gFnSc2	teorie
donedávna	donedávna	k6eAd1	donedávna
téměř	téměř	k6eAd1	téměř
ignorovalo	ignorovat	k5eAaImAgNnS	ignorovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
však	však	k9	však
diskuse	diskuse	k1gFnPc1	diskuse
mezi	mezi	k7c7	mezi
ortodoxní	ortodoxní	k2eAgFnSc7d1	ortodoxní
stratfordskou	stratfordský	k2eAgFnSc7d1	stratfordský
obcí	obec	k1gFnSc7	obec
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
oxfordiány	oxfordián	k1gInPc7	oxfordián
znatelně	znatelně	k6eAd1	znatelně
přibrala	přibrat	k5eAaPmAgFnS	přibrat
na	na	k7c6	na
intenzitě	intenzita	k1gFnSc6	intenzita
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
mnohdy	mnohdy	k6eAd1	mnohdy
děje	dít	k5eAaImIp3nS	dít
nepřímo	přímo	k6eNd1	přímo
<g/>
.	.	kIx.	.
</s>
<s>
Shakespearovo	Shakespearův	k2eAgNnSc1d1	Shakespearovo
autorství	autorství	k1gNnSc1	autorství
bylo	být	k5eAaImAgNnS	být
prokazatelně	prokazatelně	k6eAd1	prokazatelně
poprvé	poprvé	k6eAd1	poprvé
zpochybněno	zpochybnit	k5eAaPmNgNnS	zpochybnit
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
oslavování	oslavování	k1gNnSc1	oslavování
Williama	William	k1gMnSc2	William
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
jako	jako	k8xS	jako
největšího	veliký	k2eAgMnSc2d3	veliký
spisovatele	spisovatel	k1gMnSc2	spisovatel
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Okolnosti	okolnost	k1gFnPc1	okolnost
života	život	k1gInSc2	život
Williama	William	k1gMnSc2	William
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
ze	z	k7c2	z
Stratfordu	Stratford	k1gInSc2	Stratford
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
jeho	jeho	k3xOp3gInSc4	jeho
skromný	skromný	k2eAgInSc4d1	skromný
původ	původ	k1gInSc4	původ
a	a	k8xC	a
četná	četný	k2eAgNnPc4d1	četné
temná	temný	k2eAgNnPc4d1	temné
místa	místo	k1gNnPc4	místo
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
biografii	biografie	k1gFnSc4	biografie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
však	však	k9	však
podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
badatelů	badatel	k1gMnPc2	badatel
neslučovaly	slučovat	k5eNaImAgFnP	slučovat
s	s	k7c7	s
neobyčejným	obyčejný	k2eNgNnSc7d1	neobyčejné
básnickým	básnický	k2eAgNnSc7d1	básnické
nadáním	nadání	k1gNnSc7	nadání
autora	autor	k1gMnSc2	autor
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc2	jeho
reputací	reputace	k1gFnPc2	reputace
jako	jako	k8xC	jako
génia	génius	k1gMnSc2	génius
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
podnítilo	podnítit	k5eAaPmAgNnS	podnítit
podezření	podezření	k1gNnSc3	podezření
<g/>
,	,	kIx,	,
že	že	k8xS	že
William	William	k1gInSc4	William
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
ze	z	k7c2	z
Stratfordu	Stratford	k1gInSc2	Stratford
možná	možná	k9	možná
nenapsal	napsat	k5eNaPmAgMnS	napsat
díla	dílo	k1gNnPc4	dílo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
jsou	být	k5eAaImIp3nP	být
mu	on	k3xPp3gNnSc3	on
připisována	připisován	k2eAgFnSc1d1	připisována
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kontroverze	kontroverze	k1gFnSc1	kontroverze
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
části	část	k1gFnSc2	část
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
doby	doba	k1gFnSc2	doba
bylo	být	k5eAaImAgNnS	být
postupně	postupně	k6eAd1	postupně
více	hodně	k6eAd2	hodně
než	než	k8xS	než
70	[number]	k4	70
historických	historický	k2eAgFnPc2d1	historická
postav	postava	k1gFnPc2	postava
navrženo	navrhnout	k5eAaPmNgNnS	navrhnout
jako	jako	k8xS	jako
možní	možný	k2eAgMnPc1d1	možný
kandidáti	kandidát	k1gMnPc1	kandidát
autorství	autorství	k1gNnSc2	autorství
<g/>
.	.	kIx.	.
</s>
<s>
Značnou	značný	k2eAgFnSc4d1	značná
pozornost	pozornost	k1gFnSc4	pozornost
získali	získat	k5eAaPmAgMnP	získat
však	však	k9	však
jen	jen	k9	jen
Francis	Francis	k1gFnPc1	Francis
Bacon	Bacon	k1gInSc1	Bacon
<g/>
,	,	kIx,	,
William	William	k1gInSc1	William
Stanley	Stanlea	k1gFnSc2	Stanlea
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
.	.	kIx.	.
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Derby	derby	k1gNnSc2	derby
<g/>
,	,	kIx,	,
Christopher	Christophra	k1gFnPc2	Christophra
Marlowe	Marlow	k1gInSc2	Marlow
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
až	až	k9	až
do	do	k7c2	do
dneška	dnešek	k1gInSc2	dnešek
především	především	k9	především
Edward	Edward	k1gMnSc1	Edward
de	de	k?	de
Vere	Vere	k1gInSc1	Vere
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
.	.	kIx.	.
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Oxfordu	Oxford	k1gInSc2	Oxford
<g/>
.	.	kIx.	.
</s>
<s>
Badatelé	badatel	k1gMnPc1	badatel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
podporují	podporovat	k5eAaImIp3nP	podporovat
alternativní	alternativní	k2eAgMnPc4d1	alternativní
kandidáty	kandidát	k1gMnPc4	kandidát
<g/>
,	,	kIx,	,
považují	považovat	k5eAaImIp3nP	považovat
ony	onen	k3xDgFnPc4	onen
postavy	postava	k1gFnPc4	postava
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnPc3	který
dávají	dávat	k5eAaImIp3nP	dávat
přednost	přednost	k1gFnSc4	přednost
<g/>
,	,	kIx,	,
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
pohledu	pohled	k1gInSc2	pohled
za	za	k7c4	za
mnohem	mnohem	k6eAd1	mnohem
vhodnější	vhodný	k2eAgMnPc4d2	vhodnější
autory	autor	k1gMnPc4	autor
Shakespearova	Shakespearův	k2eAgInSc2d1	Shakespearův
kánonu	kánon	k1gInSc2	kánon
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
William	William	k1gInSc4	William
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
ze	z	k7c2	z
Stratfordu	Stratford	k1gInSc2	Stratford
<g/>
.	.	kIx.	.
</s>
<s>
Uvádějí	uvádět	k5eAaImIp3nP	uvádět
ale	ale	k9	ale
celkem	celkem	k6eAd1	celkem
shodně	shodně	k6eAd1	shodně
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgMnSc1	tento
muž	muž	k1gMnSc1	muž
postrádal	postrádat	k5eAaImAgMnS	postrádat
mj.	mj.	kA	mj.
vzdělání	vzdělání	k1gNnSc4	vzdělání
a	a	k8xC	a
znalost	znalost	k1gFnSc4	znalost
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Oxfordiáni	Oxfordián	k1gMnPc1	Oxfordián
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
chybějící	chybějící	k2eAgFnPc4d1	chybějící
potřebné	potřebný	k2eAgFnPc4d1	potřebná
zkušenosti	zkušenost	k1gFnPc4	zkušenost
z	z	k7c2	z
pobytů	pobyt	k1gInPc2	pobyt
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
16	[number]	k4	16
Shakespearových	Shakespearových	k2eAgFnPc2d1	Shakespearových
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
zkušenosti	zkušenost	k1gFnPc1	zkušenost
však	však	k9	však
měl	mít	k5eAaImAgMnS	mít
Edward	Edward	k1gMnSc1	Edward
de	de	k?	de
Vere	Vere	k1gInSc1	Vere
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
upírají	upírat	k5eAaImIp3nP	upírat
tito	tento	k3xDgMnPc1	tento
badatelé	badatel	k1gMnPc1	badatel
Williamu	William	k1gInSc2	William
Shakespearovi	Shakespeare	k1gMnSc3	Shakespeare
znalost	znalost	k1gFnSc1	znalost
anglického	anglický	k2eAgInSc2d1	anglický
královského	královský	k2eAgInSc2d1	královský
dvora	dvůr	k1gInSc2	dvůr
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
téměř	téměř	k6eAd1	téměř
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
aristokratické	aristokratický	k2eAgInPc1d1	aristokratický
zvyky	zvyk	k1gInPc1	zvyk
<g/>
,	,	kIx,	,
noblesa	noblesa	k1gFnSc1	noblesa
a	a	k8xC	a
citlivost	citlivost	k1gFnSc1	citlivost
a	a	k8xC	a
všechny	všechen	k3xTgFnPc1	všechen
předešlé	předešlý	k2eAgFnPc1d1	předešlá
znalosti	znalost	k1gFnPc1	znalost
a	a	k8xC	a
zkušenosti	zkušenost	k1gFnPc1	zkušenost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
projevují	projevovat	k5eAaImIp3nP	projevovat
v	v	k7c6	v
Shakespearově	Shakespearův	k2eAgNnSc6d1	Shakespearovo
díle	dílo	k1gNnSc6	dílo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nemohl	moct	k5eNaImAgMnS	moct
William	William	k1gInSc4	William
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
ze	z	k7c2	z
Stratfordu	Stratford	k1gInSc2	Stratford
podle	podle	k7c2	podle
nich	on	k3xPp3gFnPc2	on
mít	mít	k5eAaImF	mít
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Tyto	tento	k3xDgFnPc4	tento
dvě	dva	k4xCgFnPc4	dva
představy	představa	k1gFnPc4	představa
–	–	k?	–
že	že	k8xS	že
Shakespearův	Shakespearův	k2eAgInSc1d1	Shakespearův
kánon	kánon	k1gInSc1	kánon
představuje	představovat	k5eAaImIp3nS	představovat
vrchol	vrchol	k1gInSc4	vrchol
lidské	lidský	k2eAgFnSc2d1	lidská
<g />
.	.	kIx.	.
</s>
<s>
kultury	kultura	k1gFnPc1	kultura
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
William	William	k1gInSc1	William
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
byl	být	k5eAaImAgMnS	být
naprosto	naprosto	k6eAd1	naprosto
nevzdělaný	vzdělaný	k2eNgMnSc1d1	nevzdělaný
venkovan	venkovan	k1gMnSc1	venkovan
–	–	k?	–
se	se	k3xPyFc4	se
spojily	spojit	k5eAaPmAgFnP	spojit
a	a	k8xC	a
přesvědčily	přesvědčit	k5eAaPmAgFnP	přesvědčit
Delii	Delie	k1gFnSc4	Delie
Baconovou	Baconová	k1gFnSc4	Baconová
a	a	k8xC	a
její	její	k3xOp3gMnPc4	její
následovníky	následovník	k1gMnPc4	následovník
<g/>
,	,	kIx,	,
že	že	k8xS	že
titulní	titulní	k2eAgFnSc1d1	titulní
strana	strana	k1gFnSc1	strana
Prvního	první	k4xOgNnSc2	první
folia	folio	k1gNnSc2	folio
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
úvody	úvod	k1gInPc1	úvod
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
jen	jen	k9	jen
součástí	součást	k1gFnSc7	součást
skvěle	skvěle	k6eAd1	skvěle
vypracované	vypracovaný	k2eAgFnSc2d1	vypracovaná
šarády	šaráda	k1gFnSc2	šaráda
<g/>
,	,	kIx,	,
zosnované	zosnovaný	k2eAgFnSc2d1	zosnovaná
nějakou	nějaký	k3yIgFnSc7	nějaký
vysoce	vysoce	k6eAd1	vysoce
postavenou	postavený	k2eAgFnSc7d1	postavená
osobností	osobnost	k1gFnSc7	osobnost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Zastánci	zastánce	k1gMnPc1	zastánce
většinové	většinový	k2eAgFnSc2d1	většinová
stratfordiánské	stratfordiánský	k2eAgFnSc2d1	stratfordiánský
pozice	pozice	k1gFnSc2	pozice
naopak	naopak	k6eAd1	naopak
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
biografické	biografický	k2eAgFnPc1d1	biografická
interpretace	interpretace	k1gFnPc1	interpretace
literatury	literatura	k1gFnSc2	literatura
nejsou	být	k5eNaImIp3nP	být
spolehlivé	spolehlivý	k2eAgInPc1d1	spolehlivý
pro	pro	k7c4	pro
určení	určení	k1gNnSc4	určení
autorství	autorství	k1gNnSc2	autorství
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ovšem	ovšem	k9	ovšem
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
pro	pro	k7c4	pro
Williama	William	k1gMnSc4	William
Shakespeara	Shakespeare	k1gMnSc4	Shakespeare
ze	z	k7c2	z
Stratfordu	Stratford	k1gInSc2	Stratford
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
uváděno	uvádět	k5eAaImNgNnS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
souběh	souběh	k1gInSc4	souběh
známých	známý	k2eAgInPc2d1	známý
důkazů	důkaz	k1gInPc2	důkaz
autorství	autorství	k1gNnSc2	autorství
Williama	William	k1gMnSc2	William
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
ze	z	k7c2	z
Stratfordu	Stratford	k1gInSc2	Stratford
–	–	k?	–
za	za	k7c4	za
které	který	k3yIgFnPc4	který
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgFnPc4d1	považována
titulní	titulní	k2eAgFnPc4d1	titulní
stránky	stránka	k1gFnPc4	stránka
vydaných	vydaný	k2eAgNnPc2d1	vydané
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
výpovědi	výpověď	k1gFnSc2	výpověď
některých	některý	k3yIgMnPc2	některý
básníků	básník	k1gMnPc2	básník
a	a	k8xC	a
historiků	historik	k1gMnPc2	historik
za	za	k7c2	za
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
a	a	k8xC	a
v	v	k7c6	v
období	období	k1gNnSc6	období
po	po	k7c6	po
zveřejnění	zveřejnění	k1gNnSc6	zveřejnění
Prvního	první	k4xOgMnSc2	první
fólia	fólius	k1gMnSc2	fólius
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
některé	některý	k3yIgInPc1	některý
oficiální	oficiální	k2eAgInPc1d1	oficiální
záznamy	záznam	k1gInPc1	záznam
–	–	k?	–
je	být	k5eAaImIp3nS	být
stejný	stejný	k2eAgInSc1d1	stejný
jako	jako	k8xS	jako
u	u	k7c2	u
ostatních	ostatní	k2eAgFnPc2d1	ostatní
autorských	autorský	k2eAgFnPc2d1	autorská
atribucí	atribuce	k1gFnPc2	atribuce
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Důkazy	důkaz	k1gInPc1	důkaz
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
nebyly	být	k5eNaImAgFnP	být
nalezeny	naleznout	k5eAaPmNgFnP	naleznout
pro	pro	k7c4	pro
jakéhokoli	jakýkoli	k3yIgMnSc4	jakýkoli
jiného	jiný	k2eAgMnSc4d1	jiný
kandidáta	kandidát	k1gMnSc4	kandidát
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
argumentace	argumentace	k1gFnSc2	argumentace
stratfordiánů	stratfordián	k1gInPc2	stratfordián
nebylo	být	k5eNaImAgNnS	být
autorství	autorství	k1gNnSc1	autorství
Williama	William	k1gMnSc2	William
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
zpochybněno	zpochybnit	k5eAaPmNgNnS	zpochybnit
ani	ani	k8xC	ani
za	za	k7c2	za
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
několika	několik	k4yIc3	několik
století	století	k1gNnPc2	století
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
těmto	tento	k3xDgNnPc3	tento
tvrzením	tvrzení	k1gNnPc3	tvrzení
byly	být	k5eAaImAgFnP	být
však	však	k9	však
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
vysloveny	vyslovit	k5eAaPmNgInP	vyslovit
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
zastánců	zastánce	k1gMnPc2	zastánce
oxfordské	oxfordský	k2eAgFnSc2d1	Oxfordská
teorie	teorie	k1gFnSc2	teorie
značné	značný	k2eAgFnSc2d1	značná
výhrady	výhrada	k1gFnSc2	výhrada
<g/>
.	.	kIx.	.
</s>
<s>
Doposud	doposud	k6eAd1	doposud
existuje	existovat	k5eAaImIp3nS	existovat
konsenzus	konsenzus	k1gInSc1	konsenzus
převážné	převážný	k2eAgFnSc2d1	převážná
části	část	k1gFnSc2	část
vědecké	vědecký	k2eAgFnSc2d1	vědecká
obce	obec	k1gFnSc2	obec
<g/>
,	,	kIx,	,
že	že	k8xS	že
William	William	k1gInSc4	William
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
ze	z	k7c2	z
Stratfordu	Stratford	k1gInSc2	Stratford
byl	být	k5eAaImAgMnS	být
autorem	autor	k1gMnSc7	autor
shakespearovského	shakespearovský	k2eAgInSc2d1	shakespearovský
kánonu	kánon	k1gInSc2	kánon
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
skeptiků	skeptik	k1gMnPc2	skeptik
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zastávají	zastávat	k5eAaImIp3nP	zastávat
odlišné	odlišný	k2eAgFnPc4d1	odlišná
teorie	teorie	k1gFnPc4	teorie
o	o	k7c6	o
autorství	autorství	k1gNnSc6	autorství
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
sice	sice	k8xC	sice
doposud	doposud	k6eAd1	doposud
relativně	relativně	k6eAd1	relativně
malý	malý	k2eAgInSc1d1	malý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
letech	let	k1gInPc6	let
roste	růst	k5eAaImIp3nS	růst
aktivní	aktivní	k2eAgFnSc1d1	aktivní
skupina	skupina	k1gFnSc1	skupina
odpůrců	odpůrce	k1gMnPc2	odpůrce
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
prominentních	prominentní	k2eAgInPc2d1	prominentní
<g/>
,	,	kIx,	,
veřejně	veřejně	k6eAd1	veřejně
známých	známý	k2eAgFnPc2d1	známá
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
toto	tento	k3xDgNnSc4	tento
konvenční	konvenční	k2eAgNnSc4d1	konvenční
připsání	připsání	k1gNnSc4	připsání
autorství	autorství	k1gNnSc2	autorství
Williamu	William	k1gInSc2	William
Shakespearovi	Shakespeare	k1gMnSc6	Shakespeare
ze	z	k7c2	z
Stratfordu	Stratford	k1gInSc2	Stratford
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
zpochybňují	zpochybňovat	k5eAaImIp3nP	zpochybňovat
<g/>
.	.	kIx.	.
</s>
<s>
Snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
o	o	k7c4	o
všeobecné	všeobecný	k2eAgNnSc4d1	všeobecné
uznání	uznání	k1gNnSc4	uznání
autorské	autorský	k2eAgFnSc2d1	autorská
otázky	otázka	k1gFnSc2	otázka
jako	jako	k8xC	jako
legitimního	legitimní	k2eAgNnSc2d1	legitimní
pole	pole	k1gNnSc2	pole
působnosti	působnost	k1gFnSc2	působnost
vědecké	vědecký	k2eAgFnSc2d1	vědecká
obce	obec	k1gFnSc2	obec
a	a	k8xC	a
v	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
důsledku	důsledek	k1gInSc6	důsledek
o	o	k7c6	o
přijetí	přijetí	k1gNnSc6	přijetí
některého	některý	k3yIgMnSc2	některý
z	z	k7c2	z
alternativních	alternativní	k2eAgMnPc2d1	alternativní
kandidátů	kandidát	k1gMnPc2	kandidát
za	za	k7c4	za
pravého	pravý	k2eAgMnSc4d1	pravý
autora	autor	k1gMnSc4	autor
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
některých	některý	k3yIgFnPc2	některý
pasáží	pasáž	k1gFnPc2	pasáž
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
hrách	hra	k1gFnPc6	hra
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Sonetech	sonet	k1gInPc6	sonet
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
část	část	k1gFnSc1	část
podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
autorů	autor	k1gMnPc2	autor
popisuje	popisovat	k5eAaImIp3nS	popisovat
přátelský	přátelský	k2eAgInSc1d1	přátelský
nebo	nebo	k8xC	nebo
milostný	milostný	k2eAgInSc1d1	milostný
vztah	vztah	k1gInSc1	vztah
autora	autor	k1gMnSc2	autor
k	k	k7c3	k
jinému	jiný	k1gMnSc3	jiný
nejmenovanému	nejmenovaný	k1gMnSc3	nejmenovaný
muži	muž	k1gMnSc3	muž
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
spekuluje	spekulovat	k5eAaImIp3nS	spekulovat
také	také	k9	také
o	o	k7c6	o
Shakespearově	Shakespearův	k2eAgFnSc6d1	Shakespearova
homosexualitě	homosexualita	k1gFnSc6	homosexualita
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
bisexualitě	bisexualita	k1gFnSc3	bisexualita
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
spekulace	spekulace	k1gFnPc1	spekulace
nebyly	být	k5eNaImAgFnP	být
nikdy	nikdy	k6eAd1	nikdy
definitivně	definitivně	k6eAd1	definitivně
potvrzeny	potvrdit	k5eAaPmNgInP	potvrdit
ani	ani	k8xC	ani
vyvráceny	vyvrátit	k5eAaPmNgInP	vyvrátit
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnPc1	jejich
odpůrci	odpůrce	k1gMnPc1	odpůrce
však	však	k9	však
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
argumentují	argumentovat	k5eAaImIp3nP	argumentovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
psát	psát	k5eAaImF	psát
o	o	k7c4	o
příteli	přítel	k1gMnSc3	přítel
v	v	k7c6	v
termínech	termín	k1gInPc6	termín
milostné	milostný	k2eAgFnSc2d1	milostná
poezie	poezie	k1gFnSc2	poezie
patřilo	patřit	k5eAaImAgNnS	patřit
k	k	k7c3	k
dobové	dobový	k2eAgFnSc3d1	dobová
literární	literární	k2eAgFnSc3d1	literární
konvenci	konvence	k1gFnSc3	konvence
<g/>
.	.	kIx.	.
</s>
<s>
Jiný	jiný	k2eAgInSc4d1	jiný
významný	významný	k2eAgInSc4d1	významný
argument	argument	k1gInSc4	argument
uvedl	uvést	k5eAaPmAgMnS	uvést
Martin	Martin	k1gMnSc1	Martin
Hilský	Hilský	k1gMnSc1	Hilský
s	s	k7c7	s
poukazem	poukaz	k1gInSc7	poukaz
na	na	k7c4	na
strukturu	struktura	k1gFnSc4	struktura
angličtiny	angličtina	k1gFnSc2	angličtina
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
poznámkách	poznámka	k1gFnPc6	poznámka
k	k	k7c3	k
vydání	vydání	k1gNnSc3	vydání
Shakespearových	Shakespearových	k2eAgInPc2d1	Shakespearových
Sonetů	sonet	k1gInPc2	sonet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
(	(	kIx(	(
<g/>
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
překladem	překlad	k1gInSc7	překlad
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Hilského	Hilské	k1gNnSc2	Hilské
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
velká	velký	k2eAgFnSc1d1	velká
většina	většina	k1gFnSc1	většina
Shakespearových	Shakespearových	k2eAgInPc2d1	Shakespearových
sonetů	sonet	k1gInPc2	sonet
dvojznačná	dvojznačný	k2eAgFnSc1d1	dvojznačná
či	či	k8xC	či
ambivalentní	ambivalentní	k2eAgFnSc1d1	ambivalentní
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
rod	rod	k1gInSc4	rod
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
s	s	k7c7	s
odvoláním	odvolání	k1gNnSc7	odvolání
na	na	k7c4	na
autorku	autorka	k1gFnSc4	autorka
a	a	k8xC	a
specialistku	specialistka	k1gFnSc4	specialistka
na	na	k7c4	na
renesanční	renesanční	k2eAgFnSc4d1	renesanční
literaturu	literatura	k1gFnSc4	literatura
a	a	k8xC	a
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
Margretu	Margret	k1gInSc2	Margret
de	de	k?	de
Grazia	Grazia	k1gFnSc1	Grazia
uvedeno	uvést	k5eAaPmNgNnS	uvést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Skutečným	skutečný	k2eAgInSc7d1	skutečný
'	'	kIx"	'
<g/>
skandálem	skandál	k1gInSc7	skandál
Sonetů	sonet	k1gInPc2	sonet
<g/>
'	'	kIx"	'
je	být	k5eAaImIp3nS	být
vztah	vztah	k1gInSc4	vztah
mluvčího	mluvčí	k1gMnSc2	mluvčí
k	k	k7c3	k
černé	černý	k2eAgFnSc3d1	černá
dámě	dáma	k1gFnSc3	dáma
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
k	k	k7c3	k
mladému	mladý	k2eAgMnSc3d1	mladý
aristokratovi	aristokrat	k1gMnSc3	aristokrat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Tato	tento	k3xDgFnSc1	tento
ambivalentnost	ambivalentnost	k1gFnSc1	ambivalentnost
textu	text	k1gInSc2	text
sonetů	sonet	k1gInPc2	sonet
a	a	k8xC	a
mnohé	mnohý	k2eAgFnPc1d1	mnohá
další	další	k2eAgFnPc1d1	další
okolnosti	okolnost	k1gFnPc1	okolnost
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
výklady	výklad	k1gInPc1	výklad
jejich	jejich	k3xOp3gInSc2	jejich
smyslu	smysl	k1gInSc2	smysl
se	se	k3xPyFc4	se
naprosto	naprosto	k6eAd1	naprosto
diametrálně	diametrálně	k6eAd1	diametrálně
rozcházejí	rozcházet	k5eAaImIp3nP	rozcházet
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
rozpory	rozpor	k1gInPc1	rozpor
jsou	být	k5eAaImIp3nP	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důležitých	důležitý	k2eAgInPc2d1	důležitý
prvků	prvek	k1gInPc2	prvek
současných	současný	k2eAgFnPc2d1	současná
bouřlivých	bouřlivý	k2eAgFnPc2d1	bouřlivá
diskusí	diskuse	k1gFnPc2	diskuse
o	o	k7c6	o
autorství	autorství	k1gNnSc6	autorství
a	a	k8xC	a
o	o	k7c4	o
výklad	výklad	k1gInSc4	výklad
celého	celý	k2eAgNnSc2d1	celé
Shakespearova	Shakespearův	k2eAgNnSc2d1	Shakespearovo
díla	dílo	k1gNnSc2	dílo
(	(	kIx(	(
<g/>
kánonu	kánon	k1gInSc2	kánon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
