<p>
<s>
John	John	k1gMnSc1	John
Adams	Adamsa	k1gFnPc2	Adamsa
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1735	[number]	k4	1735
<g/>
,	,	kIx,	,
Braintree	Braintree	k1gFnSc1	Braintree
<g/>
,	,	kIx,	,
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1826	[number]	k4	1826
<g/>
,	,	kIx,	,
Quincy	Quinca	k1gFnPc1	Quinca
<g/>
,	,	kIx,	,
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
diplomat	diplomat	k1gMnSc1	diplomat
působící	působící	k2eAgMnSc1d1	působící
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
v	v	k7c6	v
letech	let	k1gInPc6	let
1778	[number]	k4	1778
<g/>
–	–	k?	–
<g/>
1788	[number]	k4	1788
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
viceprezident	viceprezident	k1gMnSc1	viceprezident
(	(	kIx(	(
<g/>
1789	[number]	k4	1789
<g/>
–	–	k?	–
<g/>
1797	[number]	k4	1797
<g/>
)	)	kIx)	)
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
prezident	prezident	k1gMnSc1	prezident
USA	USA	kA	USA
(	(	kIx(	(
<g/>
1797	[number]	k4	1797
<g/>
–	–	k?	–
<g/>
1801	[number]	k4	1801
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
ženatý	ženatý	k2eAgMnSc1d1	ženatý
s	s	k7c7	s
Abigail	Abigail	k1gMnSc1	Abigail
Adamsovou	Adamsová	k1gFnSc4	Adamsová
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
syn	syn	k1gMnSc1	syn
byl	být	k5eAaImAgMnS	být
pozdější	pozdní	k2eAgMnSc1d2	pozdější
6	[number]	k4	6
<g/>
.	.	kIx.	.
prezident	prezident	k1gMnSc1	prezident
John	John	k1gMnSc1	John
Quincy	Quinca	k1gFnSc2	Quinca
Adams	Adams	k1gInSc1	Adams
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
studiích	studio	k1gNnPc6	studio
na	na	k7c6	na
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
Braintree	Braintree	k1gFnSc6	Braintree
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
bez	bez	k7c2	bez
problémů	problém	k1gInPc2	problém
na	na	k7c4	na
Harvard	Harvard	k1gInSc4	Harvard
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
ve	v	k7c4	v
svých	svůj	k3xOyFgNnPc2	svůj
20	[number]	k4	20
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
přední	přední	k2eAgFnSc7d1	přední
osobou	osoba	k1gFnSc7	osoba
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
kolkovému	kolkový	k2eAgInSc3d1	kolkový
zákonu	zákon	k1gInSc3	zákon
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
kterému	který	k3yRgNnSc3	který
napsal	napsat	k5eAaBmAgMnS	napsat
2	[number]	k4	2
statě	stať	k1gFnSc2	stať
–	–	k?	–
Essay	Essaa	k1gFnSc2	Essaa
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Canon	Canon	kA	Canon
a	a	k8xC	a
Feudal	Feudal	k1gMnSc1	Feudal
Law	Law	k1gMnSc1	Law
<g/>
.	.	kIx.	.
</s>
<s>
Oženil	oženit	k5eAaPmAgInS	oženit
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1764	[number]	k4	1764
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
Bostonu	Boston	k1gInSc2	Boston
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Valného	valný	k2eAgNnSc2d1	Valné
shromáždění	shromáždění	k1gNnSc2	shromáždění
Massachusetts	Massachusetts	k1gNnSc2	Massachusetts
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1770	[number]	k4	1770
a	a	k8xC	a
také	také	k6eAd1	také
byl	být	k5eAaImAgInS	být
mezi	mezi	k7c7	mezi
5	[number]	k4	5
zvolenými	zvolený	k2eAgInPc7d1	zvolený
členy	člen	k1gInPc7	člen
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
reprezentoval	reprezentovat	k5eAaImAgInS	reprezentovat
kolonii	kolonie	k1gFnSc4	kolonie
na	na	k7c6	na
Prvním	první	k4xOgInSc6	první
kontinentálním	kontinentální	k2eAgInSc6d1	kontinentální
kongresu	kongres	k1gInSc6	kongres
roku	rok	k1gInSc2	rok
1774	[number]	k4	1774
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
velmi	velmi	k6eAd1	velmi
aktivní	aktivní	k2eAgMnSc1d1	aktivní
člen	člen	k1gMnSc1	člen
Kongresu	kongres	k1gInSc2	kongres
<g/>
,	,	kIx,	,
účastnil	účastnit	k5eAaImAgMnS	účastnit
se	se	k3xPyFc4	se
až	až	k9	až
90	[number]	k4	90
komisí	komise	k1gFnPc2	komise
<g/>
,	,	kIx,	,
25	[number]	k4	25
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
předsedal	předsedat	k5eAaImAgInS	předsedat
během	během	k7c2	během
Druhého	druhý	k4xOgInSc2	druhý
kontinentálního	kontinentální	k2eAgInSc2d1	kontinentální
kongresu	kongres	k1gInSc2	kongres
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1776	[number]	k4	1776
přednesl	přednést	k5eAaPmAgMnS	přednést
usnesení	usnesení	k1gNnSc3	usnesení
na	na	k7c6	na
vytvoření	vytvoření	k1gNnSc6	vytvoření
deklarace	deklarace	k1gFnSc2	deklarace
nezávislosti	nezávislost	k1gFnSc2	nezávislost
na	na	k7c6	na
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
vášnivým	vášnivý	k2eAgMnSc7d1	vášnivý
zastáncem	zastánce	k1gMnSc7	zastánce
Deklarace	deklarace	k1gFnSc2	deklarace
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Thomas	Thomas	k1gMnSc1	Thomas
Jefferson	Jefferson	k1gMnSc1	Jefferson
<g/>
.	.	kIx.	.
</s>
<s>
Kongres	kongres	k1gInSc1	kongres
jej	on	k3xPp3gInSc4	on
poté	poté	k6eAd1	poté
vyslal	vyslat	k5eAaPmAgMnS	vyslat
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tam	tam	k6eAd1	tam
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
správní	správní	k2eAgFnSc2d1	správní
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1779	[number]	k4	1779
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
politickém	politický	k2eAgInSc6d1	politický
životě	život	k1gInSc6	život
v	v	k7c6	v
Massachusetts	Massachusetts	k1gNnSc6	Massachusetts
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
také	také	k9	také
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
signatářů	signatář	k1gMnPc2	signatář
Deklarace	deklarace	k1gFnSc2	deklarace
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
to	ten	k3xDgNnSc1	ten
federalista	federalista	k1gMnSc1	federalista
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
starorepublikána	starorepublikán	k1gMnSc2	starorepublikán
Thomase	Thomas	k1gMnSc2	Thomas
Jeffersona	Jefferson	k1gMnSc2	Jefferson
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
jejich	jejich	k3xOp3gFnSc4	jejich
dlouhodobou	dlouhodobý	k2eAgFnSc4d1	dlouhodobá
vzájemnou	vzájemný	k2eAgFnSc4d1	vzájemná
rivalitu	rivalita	k1gFnSc4	rivalita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1826	[number]	k4	1826
<g/>
,	,	kIx,	,
právě	právě	k6eAd1	právě
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
umíral	umírat	k5eAaImAgMnS	umírat
i	i	k9	i
Jefferson	Jefferson	k1gMnSc1	Jefferson
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
poslední	poslední	k2eAgInSc1d1	poslední
přípitek	přípitek	k1gInSc1	přípitek
na	na	k7c4	na
onen	onen	k3xDgInSc1	onen
4	[number]	k4	4
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc1	červenec
zněl	znět	k5eAaImAgInS	znět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Provždy	provždy	k6eAd1	provždy
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Independence	Independence	k1gFnSc1	Independence
Forever	Forevra	k1gFnPc2	Forevra
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Výrok	výrok	k1gInSc1	výrok
==	==	k?	==
</s>
</p>
<p>
<s>
Jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc4	dva
možnosti	možnost	k1gFnPc4	možnost
jak	jak	k8xC	jak
si	se	k3xPyFc3	se
podrobit	podrobit	k5eAaPmF	podrobit
národ	národ	k1gInSc4	národ
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
je	být	k5eAaImIp3nS	být
zbraněmi	zbraň	k1gFnPc7	zbraň
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
dluhu	dluh	k1gInSc2	dluh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vláda	vláda	k1gFnSc1	vláda
Johna	John	k1gMnSc2	John
Adamse	Adams	k1gMnSc2	Adams
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
John	John	k1gMnSc1	John
Adams	Adamsa	k1gFnPc2	Adamsa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
http://encarta.msn.com/	[url]	k?	http://encarta.msn.com/
KINDIG	KINDIG	kA	KINDIG
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
<g/>
.	.	kIx.	.
</s>
<s>
Signers	Signers	k6eAd1	Signers
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Adams	Adamsa	k1gFnPc2	Adamsa
</s>
</p>
<p>
<s>
http://www.ushistory.org/declaration/signers/adams_j.htm	[url]	k6eAd1	http://www.ushistory.org/declaration/signers/adams_j.htm
</s>
</p>
<p>
<s>
===	===	k?	===
Filmografie	filmografie	k1gFnSc2	filmografie
===	===	k?	===
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
Adams	Adams	k1gInSc1	Adams
(	(	kIx(	(
<g/>
TV	TV	kA	TV
seriál	seriál	k1gInSc4	seriál
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
