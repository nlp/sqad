<s>
John	John	k1gMnSc1	John
Adams	Adamsa	k1gFnPc2	Adamsa
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1735	[number]	k4	1735
<g/>
,	,	kIx,	,
Braintree	Braintree	k1gFnSc1	Braintree
<g/>
,	,	kIx,	,
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1826	[number]	k4	1826
<g/>
,	,	kIx,	,
Quincy	Quinca	k1gFnPc1	Quinca
<g/>
,	,	kIx,	,
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
diplomat	diplomat	k1gMnSc1	diplomat
působící	působící	k2eAgMnSc1d1	působící
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
v	v	k7c6	v
letech	let	k1gInPc6	let
1778	[number]	k4	1778
<g/>
–	–	k?	–
<g/>
1788	[number]	k4	1788
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
viceprezident	viceprezident	k1gMnSc1	viceprezident
(	(	kIx(	(
<g/>
1789	[number]	k4	1789
<g/>
–	–	k?	–
<g/>
1797	[number]	k4	1797
<g/>
)	)	kIx)	)
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
prezident	prezident	k1gMnSc1	prezident
USA	USA	kA	USA
(	(	kIx(	(
<g/>
1797	[number]	k4	1797
<g/>
–	–	k?	–
<g/>
1801	[number]	k4	1801
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
