<s>
Commonwealth	Commonwealth	k1gInSc1
(	(	kIx(
<g/>
[	[	kIx(
<g/>
ˈ	ˈ	k?
<g/>
]	]	kIx)
<g/>
;	;	kIx,
celým	celý	k2eAgNnSc7d1
označením	označení	k1gNnSc7
Commonwealth	Commonwealth	k1gInSc1
of	of	k?
Nations	Nations	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
českém	český	k2eAgInSc6d1
překladu	překlad	k1gInSc6
Společenství	společenství	k1gNnSc2
národů	národ	k1gInPc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
volné	volný	k2eAgNnSc1d1
sdružení	sdružení	k1gNnSc1
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Severního	severní	k2eAgNnSc2d1
Irska	Irsko	k1gNnSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnPc2
bývalých	bývalý	k2eAgFnPc2d1
dominií	dominie	k1gFnPc2
a	a	k8xC
kolonií	kolonie	k1gFnPc2
<g/>
.	.	kIx.
</s>