<s>
Commonwealth	Commonwealth	k1gMnSc1
</s>
<s>
Členské	členský	k2eAgInPc1d1
státy	stát	k1gInPc1
Commonwealthu	Commonwealth	k1gInSc2
</s>
<s>
Commonwealth	Commonwealth	k1gInSc1
(	(	kIx(
<g/>
[	[	kIx(
<g/>
ˈ	ˈ	k?
<g/>
]	]	kIx)
<g/>
;	;	kIx,
celým	celý	k2eAgNnSc7d1
označením	označení	k1gNnSc7
Commonwealth	Commonwealth	k1gInSc1
of	of	k?
Nations	Nations	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
českém	český	k2eAgInSc6d1
překladu	překlad	k1gInSc6
Společenství	společenství	k1gNnSc2
národů	národ	k1gInPc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
volné	volný	k2eAgNnSc1d1
sdružení	sdružení	k1gNnSc1
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Severního	severní	k2eAgNnSc2d1
Irska	Irsko	k1gNnSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnPc2
bývalých	bývalý	k2eAgFnPc2d1
dominií	dominie	k1gFnPc2
a	a	k8xC
kolonií	kolonie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
1931	#num#	k4
nesl	nést	k5eAaImAgMnS
Commonwealth	Commonwealth	k1gMnSc1
označení	označení	k1gNnSc2
Britské	britský	k2eAgNnSc1d1
impérium	impérium	k1gNnSc1
<g/>
,	,	kIx,
v	v	k7c6
letech	let	k1gInPc6
1931	#num#	k4
<g/>
–	–	k?
<g/>
1947	#num#	k4
pak	pak	k6eAd1
Britské	britský	k2eAgNnSc1d1
společenství	společenství	k1gNnSc1
národů	národ	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
bývají	bývat	k5eAaImIp3nP
tyto	tento	k3xDgInPc1
termíny	termín	k1gInPc1
zaměňovány	zaměňován	k2eAgInPc1d1
a	a	k8xC
zejména	zejména	k9
označení	označení	k1gNnSc1
Commonwealth	Commonwealtha	k1gFnPc2
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
zpětně	zpětně	k6eAd1
na	na	k7c4
celou	celý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
existence	existence	k1gFnSc2
tohoto	tento	k3xDgNnSc2
společenství	společenství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc7
formální	formální	k2eAgFnSc7d1
hlavou	hlava	k1gFnSc7
je	být	k5eAaImIp3nS
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
královna	královna	k1gFnSc1
Alžběta	Alžběta	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
bylo	být	k5eAaImAgNnS
rozhodnuto	rozhodnout	k5eAaPmNgNnS
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
v	v	k7c6
této	tento	k3xDgFnSc6
funkci	funkce	k1gFnSc6
-	-	kIx~
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
vladařských	vladařský	k2eAgInPc2d1
titulů	titul	k1gInPc2
není	být	k5eNaImIp3nS
automaticky	automaticky	k6eAd1
dědičná	dědičný	k2eAgFnSc1d1
-	-	kIx~
nahradí	nahradit	k5eAaPmIp3nS
po	po	k7c6
její	její	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
princ	princ	k1gMnSc1
Charles	Charles	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Britské	britský	k2eAgNnSc1d1
impérium	impérium	k1gNnSc1
</s>
<s>
Koncem	koncem	k7c2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
za	za	k7c4
vlády	vláda	k1gFnPc4
britské	britský	k2eAgFnSc2d1
královny	královna	k1gFnSc2
Viktorie	Viktoria	k1gFnSc2
se	se	k3xPyFc4
dostalo	dostat	k5eAaPmAgNnS
Britské	britský	k2eAgNnSc1d1
impérium	impérium	k1gNnSc1
do	do	k7c2
fáze	fáze	k1gFnSc2
svého	svůj	k3xOyFgInSc2
největšího	veliký	k2eAgInSc2d3
územního	územní	k2eAgInSc2d1
i	i	k8xC
mocenského	mocenský	k2eAgInSc2d1
rozmachu	rozmach	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
mateřské	mateřský	k2eAgFnSc2d1
země	zem	k1gFnSc2
(	(	kIx(
<g/>
tehdy	tehdy	k6eAd1
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Irska	Irsko	k1gNnSc2
<g/>
)	)	kIx)
zaujímala	zaujímat	k5eAaImAgFnS
tato	tento	k3xDgFnSc1
říše	říše	k1gFnSc1
obrovská	obrovský	k2eAgFnSc1d1
zámořská	zámořský	k2eAgNnPc4d1
území	území	k1gNnSc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
především	především	k9
<g/>
:	:	kIx,
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
(	(	kIx(
<g/>
od	od	k7c2
1901	#num#	k4
dominium	dominium	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Barma	Barma	k1gFnSc1
(	(	kIx(
<g/>
dnešní	dnešní	k2eAgInSc1d1
Myanmar	Myanmar	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Britské	britský	k2eAgNnSc1d1
Bečuánsko	Bečuánsko	k1gNnSc1
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
Botswana	Botswana	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Britská	britský	k2eAgFnSc1d1
Indie	Indie	k1gFnSc1
(	(	kIx(
<g/>
dnešní	dnešní	k2eAgFnSc1d1
Indie	Indie	k1gFnSc1
<g/>
,	,	kIx,
Pákistán	Pákistán	k1gInSc1
a	a	k8xC
Bangladéš	Bangladéš	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Britská	britský	k2eAgFnSc1d1
Východní	východní	k2eAgFnSc1d1
Afrika	Afrika	k1gFnSc1
(	(	kIx(
<g/>
dnešní	dnešní	k2eAgFnSc1d1
Keňa	Keňa	k1gFnSc1
a	a	k8xC
Uganda	Uganda	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Britská	britský	k2eAgFnSc1d1
Západní	západní	k2eAgFnSc1d1
Afrika	Afrika	k1gFnSc1
(	(	kIx(
<g/>
dnešní	dnešní	k2eAgFnSc1d1
Gambie	Gambie	k1gFnSc1
<g/>
,	,	kIx,
Ghana	Ghana	k1gFnSc1
<g/>
,	,	kIx,
Nigérie	Nigérie	k1gFnSc1
a	a	k8xC
Sierra	Sierra	k1gFnSc1
Leone	Leo	k1gMnSc5
<g/>
)	)	kIx)
</s>
<s>
Cejlon	Cejlon	k1gInSc1
(	(	kIx(
<g/>
dnešní	dnešní	k2eAgFnSc1d1
Srí	Srí	k1gFnSc1
Lanka	lanko	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Kanada	Kanada	k1gFnSc1
(	(	kIx(
<g/>
od	od	k7c2
1867	#num#	k4
dominium	dominium	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Kapsko	Kapsko	k1gNnSc1
(	(	kIx(
<g/>
od	od	k7c2
1910	#num#	k4
součástí	součást	k1gFnPc2
dominia	dominion	k1gNnSc2
Jihoafrická	jihoafrický	k2eAgFnSc1d1
unie	unie	k1gFnSc1
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Kypr	Kypr	k1gInSc1
</s>
<s>
Malajsie	Malajsie	k1gFnSc1
</s>
<s>
Natal	Natal	k1gInSc1
(	(	kIx(
<g/>
od	od	k7c2
1910	#num#	k4
součástí	součást	k1gFnPc2
dominia	dominion	k1gNnSc2
Jihoafrická	jihoafrický	k2eAgFnSc1d1
unie	unie	k1gFnSc1
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Newfoundland	Newfoundland	k1gInSc1
a	a	k8xC
Labrador	Labrador	k1gInSc1
(	(	kIx(
<g/>
od	od	k7c2
1917	#num#	k4
dominium	dominium	k1gNnSc1
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
součást	součást	k1gFnSc1
Kanady	Kanada	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
(	(	kIx(
<g/>
od	od	k7c2
1907	#num#	k4
dominium	dominium	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Rhodesie	Rhodesie	k1gFnSc1
(	(	kIx(
<g/>
dnešní	dnešní	k2eAgFnSc1d1
Zambie	Zambie	k1gFnSc1
a	a	k8xC
Zimbabwe	Zimbabwe	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
a	a	k8xC
další	další	k2eAgInPc1d1
menší	malý	k2eAgInPc1d2
územní	územní	k2eAgInPc1d1
celky	celek	k1gInPc1
</s>
<s>
Po	po	k7c6
skončení	skončení	k1gNnSc6
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
se	se	k3xPyFc4
dostala	dostat	k5eAaPmAgFnS
do	do	k7c2
správy	správa	k1gFnSc2
Britského	britský	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
také	také	k9
mandátní	mandátní	k2eAgNnSc4d1
území	území	k1gNnSc4
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
byla	být	k5eAaImAgFnS
před	před	k7c7
válkou	válka	k1gFnSc7
německými	německý	k2eAgFnPc7d1
koloniemi	kolonie	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
před	před	k7c7
touto	tento	k3xDgFnSc7
válkou	válka	k1gFnSc7
však	však	k9
započaly	započnout	k5eAaPmAgFnP
změny	změna	k1gFnPc1
v	v	k7c6
soustavě	soustava	k1gFnSc6
společenství	společenství	k1gNnSc2
vzhledem	vzhledem	k7c3
k	k	k7c3
vzniku	vznik	k1gInSc3
statutu	statut	k1gInSc2
dominií	dominie	k1gFnPc2
u	u	k7c2
některých	některý	k3yIgFnPc2
dřívějších	dřívější	k2eAgFnPc2d1
kolonií	kolonie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Commonwealth	Commonwealth	k1gInSc1
of	of	k?
Nations	Nations	k1gInSc1
</s>
<s>
Po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
začal	začít	k5eAaPmAgInS
celosvětový	celosvětový	k2eAgInSc1d1
proces	proces	k1gInSc1
dekolonizace	dekolonizace	k1gFnSc2
a	a	k8xC
s	s	k7c7
tím	ten	k3xDgNnSc7
související	související	k2eAgFnSc1d1
rozpad	rozpad	k1gInSc4
Commonwealthu	Commonwealth	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
získala	získat	k5eAaPmAgFnS
samostatnost	samostatnost	k1gFnSc4
většina	většina	k1gFnSc1
bývalých	bývalý	k2eAgFnPc2d1
britských	britský	k2eAgFnPc2d1
kolonií	kolonie	k1gFnPc2
<g/>
,	,	kIx,
tyto	tento	k3xDgFnPc4
samostatné	samostatný	k2eAgFnPc4d1
země	zem	k1gFnPc4
ale	ale	k8xC
povětšinou	povětšinou	k6eAd1
zůstaly	zůstat	k5eAaPmAgFnP
členy	člen	k1gMnPc7
společenství	společenství	k1gNnPc2
i	i	k8xC
nadále	nadále	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Dnes	dnes	k6eAd1
je	být	k5eAaImIp3nS
členem	člen	k1gMnSc7
Commonwealthu	Commonwealth	k1gInSc2
53	#num#	k4
samostatných	samostatný	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
a	a	k8xC
dále	daleko	k6eAd2
závislá	závislý	k2eAgNnPc1d1
území	území	k1gNnPc1
a	a	k8xC
teritoria	teritorium	k1gNnPc1
pod	pod	k7c7
správou	správa	k1gFnSc7
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
,	,	kIx,
Austrálie	Austrálie	k1gFnSc2
a	a	k8xC
Nového	Nového	k2eAgInSc2d1
Zélandu	Zéland	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
celém	celý	k2eAgInSc6d1
Commonwealthu	Commonwealth	k1gInSc6
žije	žít	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
2,2	2,2	k4
miliardy	miliarda	k4xCgFnSc2
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgInSc7d1
úřadem	úřad	k1gInSc7
společenství	společenství	k1gNnSc2
je	být	k5eAaImIp3nS
Sekretariát	sekretariát	k1gInSc1
Commonwealthu	Commonwealth	k1gInSc2
(	(	kIx(
<g/>
Commonwealth	Commonwealth	k1gInSc1
Secretariat	Secretariat	k1gInSc1
<g/>
)	)	kIx)
se	s	k7c7
sídlem	sídlo	k1gNnSc7
v	v	k7c6
Londýně	Londýn	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Commonwealth	Commonwealth	k1gMnSc1
realms	realms	k6eAd1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Commonwealth	Commonwealth	k1gMnSc1
realm	realm	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Kromě	kromě	k7c2
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
je	být	k5eAaImIp3nS
britský	britský	k2eAgMnSc1d1
panovník	panovník	k1gMnSc1
v	v	k7c6
dalších	další	k2eAgInPc6d1
15	#num#	k4
státech	stát	k1gInPc6
respektován	respektovat	k5eAaImNgMnS
jako	jako	k9
hlava	hlava	k1gFnSc1
státu	stát	k1gInSc2
(	(	kIx(
<g/>
za	za	k7c4
hlavu	hlava	k1gFnSc4
Commonwealthu	Commonwealth	k1gInSc2
je	být	k5eAaImIp3nS
uznáván	uznávat	k5eAaImNgInS
všemi	všecek	k3xTgFnPc7
jeho	jeho	k3xOp3gMnPc7
členy	člen	k1gMnPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
některých	některý	k3yIgFnPc6
těchto	tento	k3xDgFnPc6
zemích	zem	k1gFnPc6
je	být	k5eAaImIp3nS
britský	britský	k2eAgMnSc1d1
panovník	panovník	k1gMnSc1
uznáván	uznávat	k5eAaImNgMnS
jako	jako	k9
takový	takový	k3xDgMnSc1
<g/>
,	,	kIx,
tedy	tedy	k8xC
britský	britský	k2eAgMnSc1d1
král	král	k1gMnSc1
resp.	resp.	kA
královna	královna	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
několika	několik	k4yIc6
málo	málo	k6eAd1
případech	případ	k1gInPc6
však	však	k9
nese	nést	k5eAaImIp3nS
navíc	navíc	k6eAd1
i	i	k9
titul	titul	k1gInSc4
krále	král	k1gMnPc4
té	ten	k3xDgFnSc2
země	zem	k1gFnSc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
např.	např.	kA
v	v	k7c6
Austrálii	Austrálie	k1gFnSc6
nebo	nebo	k8xC
v	v	k7c6
Kanadě	Kanada	k1gFnSc6
(	(	kIx(
<g/>
např.	např.	kA
Her	hra	k1gFnPc2
Majesty	Majest	k1gInPc4
Queen	Queen	k2eAgInSc4d1
Elisabeth	Elisabeth	k1gInSc4
II	II	kA
<g/>
,	,	kIx,
Queen	Queen	k2eAgMnSc1d1
of	of	k?
Australia	Australium	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
země	zem	k1gFnPc1
bývají	bývat	k5eAaImIp3nP
označovány	označovat	k5eAaImNgFnP
také	také	k9
jako	jako	k8xC,k8xS
Commonwealth	Commonwealth	k1gMnSc1
realms	realmsa	k1gFnPc2
(	(	kIx(
<g/>
[	[	kIx(
<g/>
relmz	relmz	k1gInSc1
<g/>
]	]	kIx)
<g/>
;	;	kIx,
pl.	pl.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Členské	členský	k2eAgFnPc1d1
země	zem	k1gFnPc1
Commonwealthu	Commonwealth	k1gInSc2
</s>
<s>
Současné	současný	k2eAgFnPc1d1
členské	členský	k2eAgFnPc1d1
země	zem	k1gFnPc1
Commonwealthu	Commonwealth	k1gInSc2
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
v	v	k7c6
následující	následující	k2eAgFnSc6d1
tabulce	tabulka	k1gFnSc6
(	(	kIx(
<g/>
seřazeno	seřazen	k2eAgNnSc1d1
podle	podle	k7c2
světadílů	světadíl	k1gInPc2
a	a	k8xC
podle	podle	k7c2
roku	rok	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
země	zem	k1gFnPc1
po	po	k7c6
získání	získání	k1gNnSc6
své	svůj	k3xOyFgFnSc2
samostatnosti	samostatnost	k1gFnSc2
přistoupily	přistoupit	k5eAaPmAgFnP
ke	k	k7c3
společenství	společenství	k1gNnSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tabulce	tabulka	k1gFnSc6
jsou	být	k5eAaImIp3nP
uvedena	uveden	k2eAgNnPc1d1
dnešní	dnešní	k2eAgNnPc1d1
jména	jméno	k1gNnPc1
53	#num#	k4
států	stát	k1gInPc2
(	(	kIx(
<g/>
případná	případný	k2eAgNnPc1d1
historická	historický	k2eAgNnPc1d1
jména	jméno	k1gNnPc1
viz	vidět	k5eAaImRp2nS
výše	výše	k1gFnSc2,k1gFnSc2wB
nebo	nebo	k8xC
v	v	k7c6
článcích	článek	k1gInPc6
k	k	k7c3
jednotlivým	jednotlivý	k2eAgFnPc3d1
zemím	zem	k1gFnPc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Členské	členský	k2eAgFnPc1d1
země	zem	k1gFnPc1
Commonwealthu	Commonwealth	k1gInSc2
podle	podle	k7c2
světadílů	světadíl	k1gInPc2
</s>
<s>
EvropaAustrálie	EvropaAustrálie	k1gFnSc1
a	a	k8xC
OceánieAfrika	OceánieAfrika	k1gFnSc1
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
(	(	kIx(
<g/>
1931	#num#	k4
<g/>
)	)	kIx)
<g/>
Austrálie	Austrálie	k1gFnSc2
Austrálie	Austrálie	k1gFnSc2
(	(	kIx(
<g/>
1931	#num#	k4
<g/>
)	)	kIx)
<g/>
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
1931	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kypr	Kypr	k1gInSc1
Kypr	Kypr	k1gInSc1
(	(	kIx(
<g/>
1961	#num#	k4
<g/>
)	)	kIx)
<g/>
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
(	(	kIx(
<g/>
1931	#num#	k4
<g/>
)	)	kIx)
<g/>
Ghana	Ghana	k1gFnSc1
Ghana	Ghana	k1gFnSc1
(	(	kIx(
<g/>
1957	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Malta	Malta	k1gFnSc1
Malta	Malta	k1gFnSc1
(	(	kIx(
<g/>
1964	#num#	k4
<g/>
)	)	kIx)
<g/>
Samoa	Samoa	k1gFnSc1
Samoa	Samoa	k1gFnSc1
(	(	kIx(
<g/>
1970	#num#	k4
<g/>
)	)	kIx)
<g/>
Nigérie	Nigérie	k1gFnSc1
Nigérie	Nigérie	k1gFnSc1
(	(	kIx(
<g/>
1960	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Severní	severní	k2eAgFnSc1d1
a	a	k8xC
Střední	střední	k2eAgFnSc1d1
AmerikaTonga	AmerikaTonga	k1gFnSc1
Tonga	Tonga	k1gFnSc1
(	(	kIx(
<g/>
1970	#num#	k4
<g/>
)	)	kIx)
<g/>
Sierra	Sierra	k1gFnSc1
Leone	Leo	k1gMnSc5
Sierra	Sierra	k1gFnSc1
Leone	Leo	k1gMnSc5
(	(	kIx(
<g/>
1961	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kanada	Kanada	k1gFnSc1
Kanada	Kanada	k1gFnSc1
(	(	kIx(
<g/>
1931	#num#	k4
<g/>
)	)	kIx)
<g/>
Papua	Papu	k2eAgFnSc1d1
Nová	nový	k2eAgFnSc1d1
Guinea	Guinea	k1gFnSc1
Papua	Papu	k2eAgFnSc1d1
Nová	nový	k2eAgFnSc1d1
Guinea	Guinea	k1gFnSc1
(	(	kIx(
<g/>
1975	#num#	k4
<g/>
)	)	kIx)
<g/>
Tanzanie	Tanzanie	k1gFnSc1
Tanzanie	Tanzanie	k1gFnSc1
(	(	kIx(
<g/>
1961	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jamajka	Jamajka	k1gFnSc1
Jamajka	Jamajka	k1gFnSc1
(	(	kIx(
<g/>
1962	#num#	k4
<g/>
)	)	kIx)
<g/>
Šalomounovy	Šalomounův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
Šalomounovy	Šalomounův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
)	)	kIx)
<g/>
Uganda	Uganda	k1gFnSc1
Uganda	Uganda	k1gFnSc1
(	(	kIx(
<g/>
1962	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Trinidad	Trinidad	k1gInSc1
a	a	k8xC
Tobago	Tobago	k6eAd1
Trinidad	Trinidad	k1gInSc1
a	a	k8xC
Tobago	Tobago	k1gNnSc1
(	(	kIx(
<g/>
1962	#num#	k4
<g/>
)	)	kIx)
<g/>
Tuvalu	Tuval	k1gInSc3
Tuvalu	Tuval	k1gInSc2
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
)	)	kIx)
<g/>
Keňa	Keňa	k1gFnSc1
Keňa	Keňa	k1gFnSc1
(	(	kIx(
<g/>
1963	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Barbados	Barbados	k1gMnSc1
Barbados	Barbados	k1gMnSc1
(	(	kIx(
<g/>
1966	#num#	k4
<g/>
)	)	kIx)
<g/>
Kiribati	Kiribati	k1gMnSc1
Kiribati	Kiribati	k1gMnSc1
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
)	)	kIx)
<g/>
Malawi	Malawi	k1gNnSc3
Malawi	Malawi	k1gNnSc3
(	(	kIx(
<g/>
1964	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Bahamy	Bahamy	k1gFnPc1
Bahamy	Bahamy	k1gFnPc1
(	(	kIx(
<g/>
1973	#num#	k4
<g/>
)	)	kIx)
<g/>
Vanuatu	Vanuat	k1gInSc3
Vanuatu	Vanuat	k1gInSc2
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
)	)	kIx)
<g/>
Zambie	Zambie	k1gFnSc1
Zambie	Zambie	k1gFnSc1
(	(	kIx(
<g/>
1964	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Grenada	Grenada	k1gFnSc1
Grenada	Grenada	k1gFnSc1
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
)	)	kIx)
<g/>
Nauru	Nauro	k1gNnSc3
Nauru	Nauro	k1gNnSc3
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
<g/>
Botswana	Botswana	k1gFnSc1
Botswana	Botswana	k1gFnSc1
(	(	kIx(
<g/>
1966	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Dominika	Dominik	k1gMnSc4
Dominika	Dominik	k1gMnSc4
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
)	)	kIx)
<g/>
Fidži	Fidž	k1gFnSc6
Fidži	Fidž	k1gFnSc6
(	(	kIx(
<g/>
1970	#num#	k4
<g/>
)	)	kIx)
<g/>
Lesotho	Lesot	k1gMnSc4
Lesotho	Lesot	k1gMnSc4
(	(	kIx(
<g/>
1966	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Svatá	svatý	k2eAgFnSc1d1
Lucie	Lucie	k1gFnSc1
Svatá	svatý	k2eAgFnSc1d1
Lucie	Lucie	k1gFnSc1
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
)	)	kIx)
<g/>
AsieMauricius	AsieMauricius	k1gMnSc1
Mauricius	Mauricius	k1gMnSc1
(	(	kIx(
<g/>
1968	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1
Vincenc	Vincenc	k1gMnSc1
a	a	k8xC
Grenadiny	grenadina	k1gFnSc2
Svatý	svatý	k1gMnSc1
Vincenc	Vincenc	k1gMnSc1
a	a	k8xC
Grenadiny	grenadina	k1gFnPc1
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
)	)	kIx)
<g/>
Indie	Indie	k1gFnSc2
Indie	Indie	k1gFnSc2
(	(	kIx(
<g/>
1947	#num#	k4
<g/>
)	)	kIx)
<g/>
Svazijsko	Svazijsko	k1gNnSc1
Svazijsko	Svazijsko	k1gNnSc1
(	(	kIx(
<g/>
1968	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Antigua	Antigua	k1gMnSc1
a	a	k8xC
Barbuda	Barbuda	k1gMnSc1
Antigua	Antigua	k1gMnSc1
a	a	k8xC
Barbuda	Barbuda	k1gMnSc1
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
<g/>
Pákistán	Pákistán	k1gInSc1
Pákistán	Pákistán	k1gInSc1
(	(	kIx(
<g/>
1947	#num#	k4
<g/>
)	)	kIx)
<g/>
Seychely	Seychely	k1gFnPc1
Seychely	Seychely	k1gFnPc1
(	(	kIx(
<g/>
1976	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Belize	Belize	k1gFnSc1
Belize	Belize	k1gFnSc1
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
<g/>
Srí	Srí	k1gFnSc3
Lanka	lanko	k1gNnSc2
Srí	Srí	k1gFnSc2
Lanka	lanko	k1gNnSc2
(	(	kIx(
<g/>
1948	#num#	k4
<g/>
)	)	kIx)
<g/>
Namibie	Namibie	k1gFnSc1
Namibie	Namibie	k1gFnSc1
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1
Kryštof	Kryštof	k1gMnSc1
a	a	k8xC
Nevis	viset	k5eNaImRp2nS
Svatý	svatý	k1gMnSc1
Kryštof	Kryštof	k1gMnSc1
a	a	k8xC
Nevis	viset	k5eNaImRp2nS
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
)	)	kIx)
<g/>
Malajsie	Malajsie	k1gFnSc1
Malajsie	Malajsie	k1gFnSc1
(	(	kIx(
<g/>
1957	#num#	k4
<g/>
)	)	kIx)
<g/>
Mosambik	Mosambik	k1gInSc1
Mosambik	Mosambik	k1gInSc1
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jižní	jižní	k2eAgMnSc1d1
AmerikaSingapur	AmerikaSingapur	k1gMnSc1
Singapur	Singapur	k1gInSc1
(	(	kIx(
<g/>
1965	#num#	k4
<g/>
)	)	kIx)
<g/>
Kamerun	Kamerun	k1gInSc1
Kamerun	Kamerun	k1gInSc1
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Guyana	Guyana	k1gFnSc1
Guyana	Guyana	k1gFnSc1
(	(	kIx(
<g/>
1966	#num#	k4
<g/>
)	)	kIx)
<g/>
Bangladéš	Bangladéš	k1gInSc1
Bangladéš	Bangladéš	k1gInSc1
(	(	kIx(
<g/>
1972	#num#	k4
<g/>
)	)	kIx)
<g/>
Rwanda	Rwanda	k1gFnSc1
Rwanda	Rwanda	k1gFnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
</s>
<s>
Brunej	Brunat	k5eAaPmRp2nS,k5eAaImRp2nS
Brunej	Brunej	k1gFnSc1
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
<g/>
Gambie	Gambie	k1gFnSc1
Gambie	Gambie	k1gFnSc1
(	(	kIx(
<g/>
1965	#num#	k4
<g/>
,	,	kIx,
opětovně	opětovně	k6eAd1
2018	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Poznámky	poznámka	k1gFnPc1
k	k	k7c3
členství	členství	k1gNnSc3
ve	v	k7c6
společenství	společenství	k1gNnSc6
</s>
<s>
Složení	složení	k1gNnSc1
Commonwealthu	Commonwealth	k1gInSc2
se	se	k3xPyFc4
mění	měnit	k5eAaImIp3nS
<g/>
,	,	kIx,
některé	některý	k3yIgInPc1
státy	stát	k1gInPc1
vystupují	vystupovat	k5eAaImIp3nP
<g/>
,	,	kIx,
některé	některý	k3yIgInPc4
přistupují	přistupovat	k5eAaImIp3nP
<g/>
,	,	kIx,
některé	některý	k3yIgFnPc1
se	se	k3xPyFc4
vrací	vracet	k5eAaImIp3nS
<g/>
,	,	kIx,
některým	některý	k3yIgFnPc3
bylo	být	k5eAaImAgNnS
pozastaveno	pozastaven	k2eAgNnSc4d1
členství	členství	k1gNnSc4
a	a	k8xC
některým	některý	k3yIgNnSc7
vráceno	vrátit	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgInSc2
některá	některý	k3yIgNnPc1
území	území	k1gNnPc1
jsou	být	k5eAaImIp3nP
členy	člen	k1gMnPc4
společenství	společenství	k1gNnSc2
nepřímo	přímo	k6eNd1
<g/>
,	,	kIx,
svou	svůj	k3xOyFgFnSc7
závislostí	závislost	k1gFnSc7
na	na	k7c6
členech	člen	k1gInPc6
společenství	společenství	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výchozí	výchozí	k2eAgInSc1d1
stav	stav	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
členy	člen	k1gMnPc4
se	se	k3xPyFc4
staly	stát	k5eAaPmAgFnP
britské	britský	k2eAgFnPc1d1
kolonie	kolonie	k1gFnPc1
<g/>
,	,	kIx,
také	také	k9
není	být	k5eNaImIp3nS
jednoznačný	jednoznačný	k2eAgInSc1d1
–	–	k?
některé	některý	k3yIgFnPc4
kolonie	kolonie	k1gFnPc4
po	po	k7c4
získání	získání	k1gNnSc4
samostatnosti	samostatnost	k1gFnSc2
do	do	k7c2
společenství	společenství	k1gNnSc2
nevstoupily	vstoupit	k5eNaPmAgFnP
<g/>
,	,	kIx,
některé	některý	k3yIgFnPc1
již	již	k9
členy	člen	k1gMnPc7
nejsou	být	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc1
členské	členský	k2eAgInPc1d1
státy	stát	k1gInPc1
britskými	britský	k2eAgFnPc7d1
koloniemi	kolonie	k1gFnPc7
ani	ani	k8xC
nikdy	nikdy	k6eAd1
nebyly	být	k5eNaImAgFnP
<g/>
.	.	kIx.
</s>
<s>
Mimo	mimo	k7c4
zde	zde	k6eAd1
uvedené	uvedený	k2eAgFnPc4d1
země	zem	k1gFnPc4
jsou	být	k5eAaImIp3nP
nepřímo	přímo	k6eNd1
členy	člen	k1gMnPc4
Commonwealthu	Commonwealth	k1gInSc2
i	i	k8xC
závislá	závislý	k2eAgNnPc1d1
území	území	k1gNnPc1
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
(	(	kIx(
<g/>
britské	britský	k2eAgFnSc2d1
korunní	korunní	k2eAgFnSc2d1
kolonie	kolonie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
závislá	závislý	k2eAgNnPc1d1
území	území	k1gNnPc1
Austrálie	Austrálie	k1gFnSc2
a	a	k8xC
Nového	Nového	k2eAgInSc2d1
Zélandu	Zéland	k1gInSc2
(	(	kIx(
<g/>
teritoria	teritorium	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zvláštní	zvláštní	k2eAgInSc1d1
status	status	k1gInSc1
(	(	kIx(
<g/>
asociovaného	asociovaný	k2eAgNnSc2d1
resp.	resp.	kA
přidruženého	přidružený	k2eAgNnSc2d1
členství	členství	k1gNnSc2
<g/>
)	)	kIx)
mají	mít	k5eAaImIp3nP
i	i	k9
Cookovy	Cookův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
a	a	k8xC
ostrov	ostrov	k1gInSc1
Niue	Niu	k1gInSc2
(	(	kIx(
<g/>
svaz	svaz	k1gInSc1
s	s	k7c7
Novým	nový	k2eAgInSc7d1
Zélandem	Zéland	k1gInSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Některé	některý	k3yIgFnPc4
země	zem	k1gFnPc4
své	svůj	k3xOyFgNnSc4
členství	členství	k1gNnSc4
přechodně	přechodně	k6eAd1
přerušily	přerušit	k5eAaPmAgFnP
vystoupením	vystoupení	k1gNnPc3
ze	z	k7c2
společenství	společenství	k1gNnSc2
<g/>
:	:	kIx,
Pákistán	Pákistán	k1gInSc1
(	(	kIx(
<g/>
1972	#num#	k4
<g/>
–	–	k?
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Fidži	Fidž	k1gFnSc6
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
–	–	k?
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
1961	#num#	k4
<g/>
–	–	k?
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Členství	členství	k1gNnSc1
bylo	být	k5eAaImAgNnS
přechodně	přechodně	k6eAd1
suspendováno	suspendovat	k5eAaPmNgNnS
u	u	k7c2
Fidži	Fidž	k1gFnSc6
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
–	–	k?
<g/>
2001	#num#	k4
a	a	k8xC
po	po	k7c6
vojenském	vojenský	k2eAgInSc6d1
převratu	převrat	k1gInSc6
v	v	k7c6
letech	let	k1gInPc6
2006	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
u	u	k7c2
Pákistánu	Pákistán	k1gInSc2
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
–	–	k?
<g/>
2004	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
–	–	k?
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
u	u	k7c2
Nigérie	Nigérie	k1gFnSc2
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
–	–	k?
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Zimbabwe	Zimbabwe	k1gNnSc1
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
2002	#num#	k4
do	do	k7c2
vystoupení	vystoupení	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Společenství	společenství	k1gNnSc4
opustily	opustit	k5eAaPmAgFnP
např.	např.	kA
následující	následující	k2eAgFnPc4d1
země	zem	k1gFnPc4
<g/>
:	:	kIx,
Irsko	Irsko	k1gNnSc1
(	(	kIx(
<g/>
1949	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Zimbabwe	Zimbabwe	k1gNnSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Gambie	Gambie	k1gFnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Maledivy	Maledivy	k1gFnPc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
také	také	k9
Nový	nový	k2eAgInSc4d1
Foundland	Foundland	k1gInSc4
(	(	kIx(
<g/>
přidružením	přidružení	k1gNnSc7
ke	k	k7c3
Kanadě	Kanada	k1gFnSc3
roku	rok	k1gInSc2
1949	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Gambie	Gambie	k1gFnSc1
ze	z	k7c2
Společenství	společenství	k1gNnSc2
vystoupila	vystoupit	k5eAaPmAgFnS
roku	rok	k1gInSc2
2013	#num#	k4
za	za	k7c2
vlády	vláda	k1gFnSc2
prezidenta	prezident	k1gMnSc2
Jammeha	Jammeh	k1gMnSc2
<g/>
,	,	kIx,
znovu	znovu	k6eAd1
byla	být	k5eAaImAgNnP
přijata	přijmout	k5eAaPmNgNnP
v	v	k7c6
únoru	únor	k1gInSc6
2018	#num#	k4
na	na	k7c4
žádost	žádost	k1gFnSc4
nového	nový	k2eAgMnSc2d1
prezidenta	prezident	k1gMnSc2
Barrowa	Barrowus	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Některé	některý	k3yIgFnPc1
britské	britský	k2eAgFnPc1d1
kolonie	kolonie	k1gFnPc1
se	se	k3xPyFc4
po	po	k7c6
vyhlášení	vyhlášení	k1gNnSc6
samostatnosti	samostatnost	k1gFnSc2
nestaly	stát	k5eNaPmAgFnP
členy	člen	k1gInPc7
Společenství	společenství	k1gNnSc1
(	(	kIx(
<g/>
mezi	mezi	k7c4
ty	ten	k3xDgFnPc4
patří	patřit	k5eAaImIp3nS
skupina	skupina	k1gFnSc1
států	stát	k1gInPc2
tvořících	tvořící	k2eAgInPc2d1
dnes	dnes	k6eAd1
USA	USA	kA
nebo	nebo	k8xC
např.	např.	kA
Barma	Barma	k1gFnSc1
<g/>
,	,	kIx,
dnešní	dnešní	k2eAgInSc1d1
Myanmar	Myanmar	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Některá	některý	k3yIgNnPc1
území	území	k1gNnPc1
dříve	dříve	k6eAd2
závislá	závislý	k2eAgFnSc1d1
na	na	k7c6
Velké	velký	k2eAgFnSc6d1
Británii	Británie	k1gFnSc6
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
např.	např.	kA
Egypt	Egypt	k1gInSc1
<g/>
,	,	kIx,
Izrael	Izrael	k1gInSc1
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
mandátní	mandátní	k2eAgNnSc1d1
území	území	k1gNnSc1
Palestina	Palestina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Irák	Irák	k1gInSc1
a	a	k8xC
Jordánsko	Jordánsko	k1gNnSc1
<g/>
,	,	kIx,
nebyla	být	k5eNaImAgNnP
nikdy	nikdy	k6eAd1
součástí	součást	k1gFnSc7
Commonwealthu	Commonwealth	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
pouze	pouze	k6eAd1
britským	britský	k2eAgInSc7d1
protektorátem	protektorát	k1gInSc7
nebo	nebo	k8xC
mandátním	mandátní	k2eAgNnSc7d1
územím	území	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Mosambik	Mosambik	k1gInSc1
a	a	k8xC
Rwanda	Rwanda	k1gFnSc1
jsou	být	k5eAaImIp3nP
členy	člen	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
nikdy	nikdy	k6eAd1
formálně	formálně	k6eAd1
nebyly	být	k5eNaImAgFnP
britskou	britský	k2eAgFnSc7d1
kolonií	kolonie	k1gFnSc7
(	(	kIx(
<g/>
Mosambik	Mosambik	k1gInSc1
byl	být	k5eAaImAgInS
portugalskou	portugalský	k2eAgFnSc7d1
kolonií	kolonie	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
WELLE	WELLE	kA
(	(	kIx(
<g/>
WWW.DW.COM	WWW.DW.COM	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Deutsche	Deutsche	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prince	princ	k1gMnSc2
Charles	Charles	k1gMnSc1
to	ten	k3xDgNnSc1
succeed	succeed	k1gInSc1
Queen	Queen	k2eAgInSc1d1
as	as	k1gInSc1
head	head	k1gInSc1
of	of	k?
the	the	k?
Commonwealth	Commonwealth	k1gMnSc1
|	|	kIx~
DW	DW	kA
|	|	kIx~
20.04	20.04	k4
<g/>
.2018	.2018	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DW	DW	kA
<g/>
.	.	kIx.
<g/>
COM	COM	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Reuters	Reutersa	k1gFnPc2
<g/>
;	;	kIx,
mil	míle	k1gFnPc2
<g/>
;	;	kIx,
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britský	britský	k2eAgInSc1d1
Commonwealth	Commonwealth	k1gInSc1
se	se	k3xPyFc4
rozšířil	rozšířit	k5eAaPmAgInS
<g/>
,	,	kIx,
54	#num#	k4
<g/>
.	.	kIx.
členskou	členský	k2eAgFnSc7d1
zemí	zem	k1gFnSc7
je	být	k5eAaImIp3nS
Rwanda	Rwanda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zahraniční	zahraniční	k2eAgInSc1d1
<g/>
.	.	kIx.
iHNed	ihned	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Economia	Economia	k1gFnSc1
<g/>
,	,	kIx,
a.s.	a.s.	k?
<g/>
,	,	kIx,
29	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
2009	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1213	#num#	k4
<g/>
-	-	kIx~
<g/>
7693	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
FREEMAN	FREEMAN	kA
<g/>
,	,	kIx,
Colin	Colin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gambia	Gambia	k1gFnSc1
Leaves	Leavesa	k1gFnPc2
Commonwealth	Commonwealtha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
World	Worlda	k1gFnPc2
news	ws	k6eNd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Daily	Daila	k1gFnSc2
Telegraph	Telegrapha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Telegraph	Telegraph	k1gInSc1
Media	medium	k1gNnSc2
Group	Group	k1gInSc1
Ltd	ltd	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
307	#num#	k4
<g/>
-	-	kIx~
<g/>
1235	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Gambie	Gambie	k1gFnSc1
se	se	k3xPyFc4
opět	opět	k6eAd1
stala	stát	k5eAaPmAgFnS
členem	člen	k1gInSc7
Commonwealthu	Commonwealth	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářské	hospodářský	k2eAgFnPc1d1
noviny	novina	k1gFnPc1
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc1
2018	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
29	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Článek	článek	k1gInSc1
na	na	k7c6
webu	web	k1gInSc6
dostupný	dostupný	k2eAgMnSc1d1
pro	pro	k7c4
předplatitele	předplatitel	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1211	#num#	k4
<g/>
-	-	kIx~
<g/>
2119	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Britské	britský	k2eAgNnSc1d1
impérium	impérium	k1gNnSc1
</s>
<s>
Dějiny	dějiny	k1gFnPc1
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
</s>
<s>
Evropské	evropský	k2eAgFnSc2d1
monarchie	monarchie	k1gFnSc2
</s>
<s>
Vlajka	vlajka	k1gFnSc1
Commonwealthu	Commonwealth	k1gInSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Commonwealth	Commonwealtha	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Úřední	úřední	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
sekretariátu	sekretariát	k1gInSc2
Commonwealthu	Commonwealth	k1gInSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Commonwealth	Commonwealth	k1gInSc1
(	(	kIx(
<g/>
Společenství	společenství	k1gNnSc1
národů	národ	k1gInPc2
<g/>
)	)	kIx)
Řádné	řádný	k2eAgFnPc1d1
členské	členský	k2eAgFnPc1d1
země	zem	k1gFnPc1
</s>
<s>
Antigua	Antigua	k1gMnSc1
a	a	k8xC
Barbuda	Barbuda	k1gMnSc1
•	•	k?
Austrálie	Austrálie	k1gFnSc2
•	•	k?
Bahamy	Bahamy	k1gFnPc4
•	•	k?
Bangladéš	Bangladéš	k1gInSc1
•	•	k?
Barbados	Barbados	k1gInSc1
•	•	k?
Belize	Belize	k1gFnSc2
•	•	k?
Botswana	Botswana	k1gFnSc1
•	•	k?
Brunej	Brunej	k1gMnSc2
•	•	k?
Dominika	Dominik	k1gMnSc2
•	•	k?
Fidži	Fidž	k1gFnSc6
•	•	k?
Gambie	Gambie	k1gFnSc2
•	•	k?
Ghana	Ghana	k1gFnSc1
•	•	k?
Grenada	Grenada	k1gFnSc1
•	•	k?
Guyana	Guyana	k1gFnSc1
•	•	k?
Indie	Indie	k1gFnSc2
•	•	k?
Jamajka	Jamajka	k1gFnSc1
•	•	k?
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
Kamerun	Kamerun	k1gInSc1
•	•	k?
Kanada	Kanada	k1gFnSc1
•	•	k?
Keňa	Keňa	k1gFnSc1
•	•	k?
Kiribati	Kiribati	k1gFnSc2
•	•	k?
Kypr	Kypr	k1gInSc1
•	•	k?
Lesotho	Lesot	k1gMnSc2
•	•	k?
Malajsie	Malajsie	k1gFnSc2
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Malawi	Malawi	k1gNnSc4
•	•	k?
Malta	Malta	k1gFnSc1
•	•	k?
Mauricius	Mauricius	k1gInSc1
•	•	k?
Mosambik	Mosambik	k1gInSc1
•	•	k?
Namibie	Namibie	k1gFnSc2
•	•	k?
Nauru	Naur	k1gInSc2
•	•	k?
Nigérie	Nigérie	k1gFnSc2
•	•	k?
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
•	•	k?
Pákistán	Pákistán	k1gInSc1
•	•	k?
Papua	Papua	k1gFnSc1
Nová	nový	k2eAgFnSc1d1
Guinea	guinea	k1gFnSc7
•	•	k?
Rwanda	Rwanda	k1gFnSc1
•	•	k?
Samoa	Samoa	k1gFnSc1
•	•	k?
Seychely	Seychely	k1gFnPc4
•	•	k?
Sierra	Sierra	k1gFnSc1
Leone	Leo	k1gMnSc5
•	•	k?
Singapur	Singapur	k1gInSc4
•	•	k?
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
•	•	k?
Sv.	sv.	kA
Kryštof	Kryštof	k1gMnSc1
a	a	k8xC
Nevis	viset	k5eNaImRp2nS
•	•	k?
Svatá	svatý	k2eAgFnSc1d1
Lucie	Lucie	k1gFnSc1
•	•	k?
Sv.	sv.	kA
Vincenc	Vincenc	k1gMnSc1
a	a	k8xC
Grenadiny	grenadina	k1gFnPc1
•	•	k?
Svazijsko	Svazijsko	k1gNnSc1
•	•	k?
Šalamounovy	Šalamounův	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
•	•	k?
Srí	Srí	k1gFnSc2
Lanka	lanko	k1gNnSc2
•	•	k?
Tanzanie	Tanzanie	k1gFnSc1
•	•	k?
Tonga	Tonga	k1gFnSc1
•	•	k?
Trinidad	Trinidad	k1gInSc1
a	a	k8xC
Tobago	Tobago	k1gNnSc1
•	•	k?
Tuvalu	Tuval	k1gInSc2
•	•	k?
Uganda	Uganda	k1gFnSc1
•	•	k?
Vanuatu	Vanuat	k1gInSc2
•	•	k?
Zambie	Zambie	k1gFnSc1
Bývalí	bývalý	k2eAgMnPc1d1
členové	člen	k1gMnPc1
</s>
<s>
Irsko	Irsko	k1gNnSc1
•	•	k?
Maledivy	Maledivy	k1gFnPc4
•	•	k?
Zimbabwe	Zimbabwe	k1gFnSc1
Závislá	závislý	k2eAgFnSc1d1
území	území	k1gNnSc4
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Akrotiri	Akrotiri	k6eAd1
a	a	k8xC
Dekelia	Dekelia	k1gFnSc1
•	•	k?
Anguilla	Anguilla	k1gFnSc1
•	•	k?
Bermudy	Bermudy	k1gFnPc4
•	•	k?
Britské	britský	k2eAgNnSc1d1
antarktické	antarktický	k2eAgNnSc1d1
území	území	k1gNnSc1
•	•	k?
Britské	britský	k2eAgNnSc1d1
indickooceánské	indickooceánský	k2eAgNnSc1d1
území	území	k1gNnSc1
•	•	k?
Britské	britský	k2eAgInPc4d1
Panenské	panenský	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
•	•	k?
Falklandy	Falklanda	k1gFnSc2
•	•	k?
Gibraltar	Gibraltar	k1gInSc1
•	•	k?
Jižní	jižní	k2eAgFnSc2d1
Georgie	Georgie	k1gFnSc2
a	a	k8xC
Jižní	jižní	k2eAgInPc1d1
Sandwichovy	Sandwichův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
•	•	k?
Kajmanské	Kajmanský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
•	•	k?
Montserrat	Montserrat	k1gInSc4
•	•	k?
Pitcairnovy	Pitcairnův	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
•	•	k?
Svatá	svatá	k1gFnSc1
Helena	Helena	k1gFnSc1
<g/>
,	,	kIx,
Ascension	Ascension	k1gInSc1
a	a	k8xC
Tristan	Tristan	k1gInSc1
da	da	k?
Cunha	Cunha	k1gFnSc1
•	•	k?
Turks	Turks	k1gInSc1
a	a	k8xC
Caicos	Caicos	k1gInSc1
Austrálie	Austrálie	k1gFnSc2
</s>
<s>
Ashmorův	Ashmorův	k2eAgInSc1d1
a	a	k8xC
Cartierův	Cartierův	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
•	•	k?
Australské	australský	k2eAgNnSc1d1
antarktické	antarktický	k2eAgNnSc1d1
území	území	k1gNnSc1
•	•	k?
Kokosové	kokosový	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
•	•	k?
Heardův	Heardův	k2eAgMnSc1d1
a	a	k8xC
MacDonaldovy	Macdonaldův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
•	•	k?
Ostrovy	ostrov	k1gInPc4
Korálového	korálový	k2eAgNnSc2d1
moře	moře	k1gNnSc2
•	•	k?
Norfolk	Norfolk	k1gInSc1
•	•	k?
Vánoční	vánoční	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
</s>
<s>
Cookovy	Cookův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
•	•	k?
Niue	Niu	k1gInSc2
•	•	k?
Rossova	Rossův	k2eAgFnSc1d1
dependence	dependence	k1gFnSc1
•	•	k?
Tokelau	Tokelaus	k1gInSc2
</s>
<s>
Monarchie	monarchie	k1gFnSc1
Afrika	Afrika	k1gFnSc1
</s>
<s>
Lesotho	Lesotze	k6eAd1
•	•	k?
Maroko	Maroko	k1gNnSc1
•	•	k?
Svazijsko	Svazijsko	k1gNnSc1
Evropa	Evropa	k1gFnSc1
</s>
<s>
Andorra	Andorra	k1gFnSc1
•	•	k?
Belgie	Belgie	k1gFnSc1
•	•	k?
Dánsko	Dánsko	k1gNnSc1
•	•	k?
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1
•	•	k?
Lucembursko	Lucembursko	k1gNnSc1
•	•	k?
Monako	Monako	k1gNnSc1
•	•	k?
Nizozemsko	Nizozemsko	k1gNnSc1
•	•	k?
Norsko	Norsko	k1gNnSc1
•	•	k?
Španělsko	Španělsko	k1gNnSc1
•	•	k?
Švédsko	Švédsko	k1gNnSc4
•	•	k?
Vatikán	Vatikán	k1gInSc1
Asie	Asie	k1gFnSc2
</s>
<s>
Bahrajn	Bahrajn	k1gInSc1
•	•	k?
Bhútán	Bhútán	k1gInSc1
•	•	k?
Brunej	Brunej	k1gFnSc2
•	•	k?
Kambodža	Kambodža	k1gFnSc1
•	•	k?
Japonsko	Japonsko	k1gNnSc1
•	•	k?
Jordánsko	Jordánsko	k1gNnSc4
•	•	k?
Kuvajt	Kuvajt	k1gInSc1
•	•	k?
Malajsie	Malajsie	k1gFnSc2
•	•	k?
Omán	Omán	k1gInSc1
•	•	k?
Katar	katar	k1gInSc1
•	•	k?
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
•	•	k?
Thajsko	Thajsko	k1gNnSc1
•	•	k?
Spojené	spojený	k2eAgInPc4d1
arabské	arabský	k2eAgInPc4d1
emiráty	emirát	k1gInPc4
Oceánie	Oceánie	k1gFnSc2
</s>
<s>
Tonga	Tonga	k1gFnSc1
Commonwealthrealm	Commonwealthrealma	k1gFnPc2
</s>
<s>
Antigua	Antigua	k1gMnSc1
a	a	k8xC
Barbuda	Barbuda	k1gMnSc1
•	•	k?
Austrálie	Austrálie	k1gFnSc2
•	•	k?
Bahamy	Bahamy	k1gFnPc4
•	•	k?
Barbados	Barbados	k1gMnSc1
•	•	k?
Belize	Belize	k1gFnSc2
•	•	k?
Grenada	Grenada	k1gFnSc1
•	•	k?
Jamajka	Jamajka	k1gFnSc1
•	•	k?
Kanada	Kanada	k1gFnSc1
•	•	k?
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
•	•	k?
Papua	Papuum	k1gNnSc2
Nová	nový	k2eAgFnSc1d1
Guinea	Guinea	k1gFnSc1
•	•	k?
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
•	•	k?
Svatá	svatat	k5eAaImIp3nS
Lucie	Lucie	k1gFnSc1
•	•	k?
Svatý	svatý	k2eAgMnSc1d1
Kryštof	Kryštof	k1gMnSc1
a	a	k8xC
Nevis	viset	k5eNaImRp2nS
•	•	k?
Svatý	svatý	k2eAgMnSc1d1
Vincenc	Vincenc	k1gMnSc1
a	a	k8xC
Grenadiny	grenadina	k1gFnPc1
•	•	k?
Šalomounovy	Šalomounův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
•	•	k?
Tuvalu	Tuval	k1gInSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Kolonialismus	kolonialismus	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
207251	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4010437-0	4010437-0	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
92054696	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
144390698	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
92054696	#num#	k4
</s>
