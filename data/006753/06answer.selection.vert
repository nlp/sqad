<s>
Příčiny	příčina	k1gFnPc1	příčina
války	válka	k1gFnSc2	válka
bývají	bývat	k5eAaImIp3nP	bývat
hledány	hledat	k5eAaImNgFnP	hledat
v	v	k7c6	v
důsledcích	důsledek	k1gInPc6	důsledek
ideologií	ideologie	k1gFnPc2	ideologie
a	a	k8xC	a
politik	politika	k1gFnPc2	politika
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
nacionalismus	nacionalismus	k1gInSc4	nacionalismus
a	a	k8xC	a
imperialismus	imperialismus	k1gInSc4	imperialismus
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
historiků	historik	k1gMnPc2	historik
v	v	k7c6	v
nespokojenosti	nespokojenost	k1gFnSc6	nespokojenost
s	s	k7c7	s
Versailleskou	versailleský	k2eAgFnSc7d1	Versailleská
smlouvou	smlouva	k1gFnSc7	smlouva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
prohloubit	prohloubit	k5eAaPmF	prohloubit
pocit	pocit	k1gInSc4	pocit
ponížení	ponížení	k1gNnSc2	ponížení
v	v	k7c6	v
poražených	poražený	k2eAgInPc6d1	poražený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
dopadech	dopad	k1gInPc6	dopad
velké	velký	k2eAgFnSc2d1	velká
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
dvacátých	dvacátý	k4xOgNnPc2	dvacátý
a	a	k8xC	a
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
