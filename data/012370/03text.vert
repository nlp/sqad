<p>
<s>
Drsek	drsek	k1gMnSc1	drsek
větší	veliký	k2eAgMnSc1d2	veliký
(	(	kIx(	(
<g/>
Zingel	Zingel	k1gMnSc1	Zingel
zingel	zingel	k1gMnSc1	zingel
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
sladkovodní	sladkovodní	k2eAgFnSc1d1	sladkovodní
ryba	ryba	k1gFnSc1	ryba
paprskoploutvá	paprskoploutvá	k1gFnSc1	paprskoploutvá
ryba	ryba	k1gFnSc1	ryba
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
okounovitých	okounovitý	k2eAgMnPc2d1	okounovitý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgMnSc1d2	veliký
než	než	k8xS	než
drsek	drsek	k1gMnSc1	drsek
menší	malý	k2eAgMnSc1d2	menší
<g/>
,	,	kIx,	,
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
velikosti	velikost	k1gFnPc4	velikost
až	až	k9	až
48	[number]	k4	48
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
15	[number]	k4	15
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
protáhlé	protáhlý	k2eAgNnSc1d1	protáhlé
a	a	k8xC	a
vřetenovité	vřetenovitý	k2eAgNnSc1d1	vřetenovité
tělo	tělo	k1gNnSc1	tělo
a	a	k8xC	a
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
ocasní	ocasní	k2eAgInSc1d1	ocasní
násadec	násadec	k1gInSc1	násadec
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
temeni	temeno	k1gNnSc6	temeno
ošupená	ošupený	k2eAgFnSc1d1	ošupený
až	až	k8xS	až
po	po	k7c4	po
špičku	špička	k1gFnSc4	špička
rypce	rypec	k1gInSc2	rypec
<g/>
.	.	kIx.	.
</s>
<s>
Břicho	břicho	k1gNnSc1	břicho
je	být	k5eAaImIp3nS	být
až	až	k9	až
k	k	k7c3	k
řitnímu	řitní	k2eAgInSc3d1	řitní
otvoru	otvor	k1gInSc3	otvor
bez	bez	k7c2	bez
šupin	šupina	k1gFnPc2	šupina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
drsek	drsek	k1gMnSc1	drsek
větší	veliký	k2eAgMnSc1d2	veliký
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Fishbase	Fishbase	k6eAd1	Fishbase
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
–	–	k?	–
Zingel	Zingel	k1gMnSc1	Zingel
zingel	zingel	k1gMnSc1	zingel
</s>
</p>
