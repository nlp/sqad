<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Fuks	Fuks	k1gMnSc1	Fuks
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1923	[number]	k4	1923
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1994	[number]	k4	1994
tamtéž	tamtéž	k6eAd1	tamtéž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
prozaik	prozaik	k1gMnSc1	prozaik
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
především	především	k9	především
psychologické	psychologický	k2eAgFnSc2d1	psychologická
prózy	próza	k1gFnSc2	próza
s	s	k7c7	s
tématem	téma	k1gNnSc7	téma
úzkosti	úzkost	k1gFnSc2	úzkost
člověka	člověk	k1gMnSc2	člověk
ohrožovaného	ohrožovaný	k2eAgMnSc2d1	ohrožovaný
nesvobodou	nesvoboda	k1gFnSc7	nesvoboda
a	a	k8xC	a
násilím	násilí	k1gNnSc7	násilí
<g/>
.	.	kIx.	.
</s>
