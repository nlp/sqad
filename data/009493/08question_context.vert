<s>
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
byl	být	k5eAaImAgInS	být
globální	globální	k2eAgInSc1d1	globální
vojenský	vojenský	k2eAgInSc1d1	vojenský
konflikt	konflikt	k1gInSc1	konflikt
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
se	se	k3xPyFc4	se
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
většina	většina	k1gFnSc1	většina
států	stát	k1gInPc2	stát
světa	svět	k1gInSc2	svět
a	a	k8xC	a
jenž	jenž	k3xRgInSc1	jenž
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
60	[number]	k4	60
miliony	milion	k4xCgInPc4	milion
obětí	oběť	k1gFnPc2	oběť
dosud	dosud	k6eAd1	dosud
největším	veliký	k2eAgMnPc3d3	veliký
a	a	k8xC	a
nejvíc	nejvíc	k6eAd1	nejvíc
zničujícím	zničující	k2eAgNnSc7d1	zničující
válečným	válečný	k2eAgNnSc7d1	válečné
střetnutím	střetnutí	k1gNnSc7	střetnutí
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
začala	začít	k5eAaPmAgFnS	začít
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
když	když	k8xS	když
nacistické	nacistický	k2eAgNnSc1d1	nacistické
Německo	Německo	k1gNnSc1	Německo
napadlo	napadnout	k5eAaPmAgNnS	napadnout
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
.	.	kIx.	.
</s>
