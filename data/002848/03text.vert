<s>
LA4	LA4	k4	LA4
(	(	kIx(	(
<g/>
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Martin	Martina	k1gFnPc2	Martina
Lasky	Laska	k1gFnSc2	Laska
<g/>
,	,	kIx,	,
*	*	kIx~	*
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
rapper	rapper	k1gMnSc1	rapper
a	a	k8xC	a
writer	writer	k1gMnSc1	writer
<g/>
.	.	kIx.	.
</s>
<s>
LA4	LA4	k4	LA4
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
člen	člen	k1gMnSc1	člen
kapely	kapela	k1gFnSc2	kapela
Indy	Indus	k1gInPc4	Indus
&	&	k?	&
Wich	Wich	k1gInSc4	Wich
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kterou	který	k3yIgFnSc4	který
i	i	k9	i
v	v	k7c6	v
dnešních	dnešní	k2eAgFnPc6d1	dnešní
dobách	doba	k1gFnPc6	doba
často	často	k6eAd1	často
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
mu	on	k3xPp3gMnSc3	on
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
jeho	jeho	k3xOp3gFnSc4	jeho
první	první	k4xOgNnSc1	první
sólové	sólový	k2eAgNnSc1d1	sólové
album	album	k1gNnSc1	album
Panoptikum	panoptikum	k1gNnSc1	panoptikum
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
za	za	k7c2	za
něj	on	k3xPp3gInSc2	on
hudební	hudební	k2eAgFnPc4d1	hudební
cenu	cena	k1gFnSc4	cena
Anděla	Anděl	k1gMnSc2	Anděl
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Album	album	k1gNnSc1	album
roku	rok	k1gInSc2	rok
Hip	hip	k0	hip
Hop	hop	k0	hop
&	&	k?	&
R	R	kA	R
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
B.	B.	kA	B.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
vydal	vydat	k5eAaPmAgInS	vydat
album	album	k1gNnSc4	album
Gyzmo	Gyzma	k1gFnSc5	Gyzma
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
pražského	pražský	k2eAgInSc2d1	pražský
labelu	label	k1gInSc2	label
Bigg	Bigg	k1gMnSc1	Bigg
Boss	boss	k1gMnSc1	boss
<g/>
.	.	kIx.	.
</s>
<s>
Panorama	panorama	k1gNnSc1	panorama
(	(	kIx(	(
<g/>
DJ	DJ	kA	DJ
Wich	Wich	k1gMnSc1	Wich
<g/>
/	/	kIx~	/
<g/>
2014	[number]	k4	2014
<g/>
/	/	kIx~	/
<g/>
Golden	Goldna	k1gFnPc2	Goldna
Touch	Touch	k1gInSc1	Touch
<g/>
)	)	kIx)	)
Nadzemí	nadzemí	k1gNnSc1	nadzemí
(	(	kIx(	(
<g/>
James	James	k1gInSc1	James
Cole	cola	k1gFnSc3	cola
<g/>
,	,	kIx,	,
DJ	DJ	kA	DJ
Mike	Mike	k1gFnSc4	Mike
trafik	trafika	k1gFnPc2	trafika
<g/>
/	/	kIx~	/
<g/>
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
Big	Big	k1gMnSc1	Big
Boss	boss	k1gMnSc1	boss
<g/>
)	)	kIx)	)
Gyzmo	Gyzma	k1gFnSc5	Gyzma
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
/	/	kIx~	/
<g/>
Big	Big	k1gMnSc1	Big
Boss	boss	k1gMnSc1	boss
<g/>
)	)	kIx)	)
Panoptikum	panoptikum	k1gNnSc1	panoptikum
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
2007	[number]	k4	2007
<g/>
/	/	kIx~	/
<g/>
Big	Big	k1gMnSc1	Big
Boss	boss	k1gMnSc1	boss
<g/>
)	)	kIx)	)
El	Ela	k1gFnPc2	Ela
A	a	k8xC	a
Pro	pro	k7c4	pro
<g/>
/	/	kIx~	/
<g/>
Na	na	k7c4	na
světlo	světlo	k1gNnSc4	světlo
denní	denní	k2eAgNnSc4d1	denní
(	(	kIx(	(
<g/>
singl	singl	k1gInSc4	singl
<g/>
/	/	kIx~	/
<g/>
2004	[number]	k4	2004
<g/>
/	/	kIx~	/
<g/>
Maddrum	Maddrum	k1gInSc1	Maddrum
<g/>
)	)	kIx)	)
My	my	k3xPp1nPc1	my
3	[number]	k4	3
(	(	kIx(	(
<g/>
Indy	Indus	k1gInPc1	Indus
&	&	k?	&
Wich	Wich	k1gInSc1	Wich
<g/>
/	/	kIx~	/
<g/>
2002	[number]	k4	2002
<g/>
/	/	kIx~	/
<g/>
Maddrum	Maddrum	k1gInSc1	Maddrum
<g/>
)	)	kIx)	)
20ERS	[number]	k4	20ERS
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Česká	český	k2eAgFnSc1d1	Česká
RAPublika	RAPublika	k1gFnSc1	RAPublika
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Indy	Indus	k1gInPc1	Indus
&	&	k?	&
Wich	Wich	k1gInSc4	Wich
-	-	kIx~	-
Kids	Kids	k1gInSc4	Kids
On	on	k3xPp3gMnSc1	on
The	The	k1gMnSc1	The
Click	Click	k1gMnSc1	Click
Tour	Tour	k1gMnSc1	Tour
2007	[number]	k4	2007
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
http://www.myspace.com/lafor	[url]	k1gInSc1	http://www.myspace.com/lafor
</s>
