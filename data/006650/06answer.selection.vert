<s>
V	v	k7c6	v
rukách	ruka	k1gFnPc6	ruka
zkušeného	zkušený	k2eAgMnSc2d1	zkušený
psovoda	psovod	k1gMnSc2	psovod
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
dobrým	dobrý	k2eAgMnSc7d1	dobrý
pracovním	pracovní	k2eAgMnSc7d1	pracovní
psem	pes	k1gMnSc7	pes
<g/>
,	,	kIx,	,
vynikající	vynikající	k2eAgInPc1d1	vynikající
v	v	k7c6	v
dynamických	dynamický	k2eAgInPc6d1	dynamický
sportech	sport	k1gInPc6	sport
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
obrana	obrana	k1gFnSc1	obrana
nebo	nebo	k8xC	nebo
agility	agilita	k1gFnPc1	agilita
<g/>
,	,	kIx,	,
při	při	k7c6	při
špatném	špatný	k2eAgNnSc6d1	špatné
vedení	vedení	k1gNnSc6	vedení
se	se	k3xPyFc4	se
z	z	k7c2	z
takového	takový	k3xDgMnSc2	takový
jedince	jedinec	k1gMnSc2	jedinec
snadno	snadno	k6eAd1	snadno
stane	stanout	k5eAaPmIp3nS	stanout
nezvládnutelný	zvládnutelný	k2eNgMnSc1d1	nezvládnutelný
pes	pes	k1gMnSc1	pes
<g/>
.	.	kIx.	.
</s>
