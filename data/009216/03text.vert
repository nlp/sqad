<p>
<s>
HK	HK	kA	HK
Mogiljov	Mogiljov	k1gInSc1	Mogiljov
je	být	k5eAaImIp3nS	být
hokejový	hokejový	k2eAgInSc1d1	hokejový
klub	klub	k1gInSc1	klub
z	z	k7c2	z
Mohylevu	Mohylev	k1gInSc2	Mohylev
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
hraje	hrát	k5eAaImIp3nS	hrát
Běloruskou	běloruský	k2eAgFnSc4d1	Běloruská
hokejovou	hokejový	k2eAgFnSc4d1	hokejová
ligu	liga	k1gFnSc4	liga
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
byl	být	k5eAaImAgInS	být
založen	založen	k2eAgInSc1d1	založen
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
domovským	domovský	k2eAgInSc7d1	domovský
stadionem	stadion	k1gInSc7	stadion
je	být	k5eAaImIp3nS	být
Mogilev	Mogilev	k1gFnSc1	Mogilev
Sports	Sports	k1gInSc4	Sports
Palace	Palace	k1gFnSc2	Palace
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
3048	[number]	k4	3048
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
HK	HK	kA	HK
Mogiljov	Mogiljovo	k1gNnPc2	Mogiljovo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
klubu	klub	k1gInSc2	klub
</s>
</p>
