<s>
Antimon	antimon	k1gInSc1	antimon
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Sb	sb	kA	sb
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Stibium	stibium	k1gNnSc1	stibium
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
polokovy	polokův	k2eAgFnPc4d1	polokův
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
lidstvu	lidstvo	k1gNnSc3	lidstvo
již	již	k9	již
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
<g/>
.	.	kIx.	.
</s>
