<s>
Československá	československý	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
ČSLA	ČSLA	kA
<g/>
;	;	kIx,
též	též	k9
slovensky	slovensky	k6eAd1
Československá	československý	k2eAgFnSc1d1
ľudová	ľudový	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
<g/>
,	,	kIx,
zkratka	zkratka	k1gFnSc1
ČSĽA	ČSĽA	kA
<g/>
)	)	kIx)
byly	být	k5eAaImAgFnP
armádní	armádní	k2eAgFnPc1d1
síly	síla	k1gFnPc1
Komunistické	komunistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
Československa	Československo	k1gNnSc2
(	(	kIx(
<g/>
KSČ	KSČ	kA
<g/>
)	)	kIx)
a	a	k8xC
Československé	československý	k2eAgFnSc2d1
socialistické	socialistický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
od	od	k7c2
roku	rok	k1gInSc2
1954	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
1990	#num#	k4
<g/>
.	.	kIx.
</s>