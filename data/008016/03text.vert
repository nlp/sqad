<s>
U	u	k7c2	u
domácí	domácí	k2eAgFnSc2d1	domácí
drůbeže	drůbež	k1gFnSc2	drůbež
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
hrabavá	hrabavý	k2eAgFnSc1d1	hrabavá
drůbež	drůbež	k1gFnSc1	drůbež
(	(	kIx(	(
<g/>
kur	kur	k1gMnSc1	kur
domácí	domácí	k1gMnSc1	domácí
<g/>
,	,	kIx,	,
krůty	krůta	k1gFnPc1	krůta
<g/>
)	)	kIx)	)
a	a	k8xC	a
vodní	vodní	k2eAgFnSc4d1	vodní
drůbež	drůbež	k1gFnSc4	drůbež
(	(	kIx(	(
<g/>
kachny	kachna	k1gFnPc4	kachna
a	a	k8xC	a
husy	husa	k1gFnPc4	husa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fylogeneticky	fylogeneticky	k6eAd1	fylogeneticky
tvoří	tvořit	k5eAaImIp3nP	tvořit
drůbež	drůbež	k1gFnSc4	drůbež
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
klady	klad	k1gInPc4	klad
vrubozobí	vrubozobí	k1gMnPc1	vrubozobí
a	a	k8xC	a
hrabaví	hrabavý	k2eAgMnPc1d1	hrabavý
<g/>
)	)	kIx)	)
dobře	dobře	k6eAd1	dobře
podloženou	podložený	k2eAgFnSc4d1	podložená
monofyletickou	monofyletický	k2eAgFnSc4d1	monofyletická
skupinu	skupina	k1gFnSc4	skupina
na	na	k7c6	na
samé	samý	k3xTgFnSc6	samý
bázi	báze	k1gFnSc6	báze
žijících	žijící	k2eAgMnPc2d1	žijící
létavých	létavý	k2eAgMnPc2d1	létavý
ptáků	pták	k1gMnPc2	pták
(	(	kIx(	(
<g/>
Neognathae	Neognathae	k1gNnSc1	Neognathae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zvanou	zvaný	k2eAgFnSc4d1	zvaná
Galloanserae	Galloanserae	k1gFnSc4	Galloanserae
Mezi	mezi	k7c4	mezi
hrabavou	hrabavý	k2eAgFnSc4d1	hrabavá
drůbež	drůbež	k1gFnSc4	drůbež
je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
zařazován	zařazován	k2eAgMnSc1d1	zařazován
kur	kur	k1gMnSc1	kur
domácí	domácí	k2eAgFnSc2d1	domácí
a	a	k8xC	a
krůty	krůta	k1gFnSc2	krůta
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
zoologicky	zoologicky	k6eAd1	zoologicky
do	do	k7c2	do
řádu	řád	k1gInSc2	řád
Galliformes	Galliformesa	k1gFnPc2	Galliformesa
(	(	kIx(	(
<g/>
hrabaví	hrabavý	k2eAgMnPc1d1	hrabavý
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nP	patřit
i	i	k9	i
další	další	k2eAgInPc1d1	další
druhy	druh	k1gInPc1	druh
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
perliček	perlička	k1gFnPc2	perlička
<g/>
,	,	kIx,	,
pávů	páv	k1gMnPc2	páv
<g/>
,	,	kIx,	,
bažantů	bažant	k1gMnPc2	bažant
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
okrasných	okrasný	k2eAgInPc2d1	okrasný
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
řádu	řád	k1gInSc2	řád
Galliformes	Galliformes	k1gInSc1	Galliformes
jsou	být	k5eAaImIp3nP	být
ptáci	pták	k1gMnPc1	pták
od	od	k7c2	od
velikosti	velikost	k1gFnSc2	velikost
křepelky	křepelka	k1gFnSc2	křepelka
do	do	k7c2	do
velikosti	velikost	k1gFnSc2	velikost
krocana	krocan	k1gMnSc2	krocan
<g/>
.	.	kIx.	.
</s>
<s>
Zobák	zobák	k1gInSc4	zobák
mají	mít	k5eAaImIp3nP	mít
silný	silný	k2eAgInSc4d1	silný
<g/>
,	,	kIx,	,
krátký	krátký	k2eAgInSc4d1	krátký
a	a	k8xC	a
jen	jen	k9	jen
mírně	mírně	k6eAd1	mírně
zahnutý	zahnutý	k2eAgMnSc1d1	zahnutý
<g/>
.	.	kIx.	.
</s>
<s>
Nohy	noha	k1gFnPc1	noha
jsou	být	k5eAaImIp3nP	být
silné	silný	k2eAgFnPc1d1	silná
<g/>
,	,	kIx,	,
uzpůsobené	uzpůsobený	k2eAgFnPc1d1	uzpůsobená
k	k	k7c3	k
hrabání	hrabání	k1gNnSc3	hrabání
<g/>
;	;	kIx,	;
drápy	dráp	k1gInPc1	dráp
jsou	být	k5eAaImIp3nP	být
tupé	tupý	k2eAgInPc1d1	tupý
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
vole	vole	k1gNnSc4	vole
<g/>
,	,	kIx,	,
silný	silný	k2eAgInSc4d1	silný
svalnatý	svalnatý	k2eAgInSc4d1	svalnatý
žaludek	žaludek	k1gInSc4	žaludek
<g/>
,	,	kIx,	,
dlouhá	dlouhý	k2eAgNnPc1d1	dlouhé
slepá	slepý	k2eAgNnPc1d1	slepé
střeva	střevo	k1gNnPc1	střevo
<g/>
,	,	kIx,	,
krátká	krátký	k2eAgNnPc1d1	krátké
křídla	křídlo	k1gNnPc1	křídlo
a	a	k8xC	a
samec	samec	k1gInSc1	samec
má	mít	k5eAaImIp3nS	mít
často	často	k6eAd1	často
na	na	k7c6	na
běháku	běhák	k1gInSc6	běhák
ostruhu	ostruh	k1gInSc2	ostruh
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
semeny	semeno	k1gNnPc7	semeno
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
zelenými	zelený	k2eAgFnPc7d1	zelená
částmi	část	k1gFnPc7	část
rostlin	rostlina	k1gFnPc2	rostlina
nebo	nebo	k8xC	nebo
hmyzem	hmyz	k1gInSc7	hmyz
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc7	jeho
larvami	larva	k1gFnPc7	larva
<g/>
.	.	kIx.	.
</s>
<s>
Neplavou	plavat	k5eNaImIp3nP	plavat
a	a	k8xC	a
nekoupou	koupat	k5eNaImIp3nP	koupat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
se	se	k3xPyFc4	se
často	často	k6eAd1	často
popelí	popelit	k5eAaImIp3nS	popelit
(	(	kIx(	(
<g/>
popelová	popelový	k2eAgFnSc1d1	popelová
lázeň	lázeň	k1gFnSc1	lázeň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Peří	peří	k1gNnSc1	peří
je	být	k5eAaImIp3nS	být
tuhé	tuhý	k2eAgNnSc1d1	tuhé
<g/>
,	,	kIx,	,
prachové	prachový	k2eAgNnSc1d1	prachové
peří	peří	k1gNnSc1	peří
chybí	chybět	k5eAaImIp3nS	chybět
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
se	se	k3xPyFc4	se
výrazným	výrazný	k2eAgInSc7d1	výrazný
pohlavním	pohlavní	k2eAgInSc7d1	pohlavní
dimorfismem	dimorfismus	k1gInSc7	dimorfismus
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
ptáci	pták	k1gMnPc1	pták
nekrmiví	krmivý	k2eNgMnPc1d1	krmivý
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
jsou	být	k5eAaImIp3nP	být
po	po	k7c4	po
opuštění	opuštění	k1gNnSc4	opuštění
hnízda	hnízdo	k1gNnSc2	hnízdo
velmi	velmi	k6eAd1	velmi
samostatná	samostatný	k2eAgFnSc1d1	samostatná
a	a	k8xC	a
sama	sám	k3xTgFnSc1	sám
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
významné	významný	k2eAgInPc1d1	významný
druhy	druh	k1gInPc1	druh
vodní	vodní	k2eAgFnSc2d1	vodní
drůbeže	drůbež	k1gFnSc2	drůbež
(	(	kIx(	(
<g/>
kachna	kachna	k1gFnSc1	kachna
domácí	domácí	k2eAgFnSc1d1	domácí
<g/>
,	,	kIx,	,
kachna	kachna	k1gFnSc1	kachna
pižmová	pižmový	k2eAgFnSc1d1	pižmová
<g/>
,	,	kIx,	,
husa	husa	k1gFnSc1	husa
domácí	domácí	k2eAgFnSc1d1	domácí
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
zoologicky	zoologicky	k6eAd1	zoologicky
do	do	k7c2	do
řádu	řád	k1gInSc2	řád
Anseriformes	Anseriformesa	k1gFnPc2	Anseriformesa
(	(	kIx(	(
<g/>
vrubozobí	vrubozobí	k1gMnPc1	vrubozobí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
vrubozobých	vrubozobí	k1gMnPc2	vrubozobí
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
kraje	kraj	k1gInPc1	kraj
obou	dva	k4xCgFnPc2	dva
dílů	díl	k1gInPc2	díl
zploštělého	zploštělý	k2eAgInSc2d1	zploštělý
zobáku	zobák	k1gInSc2	zobák
kulisovitě	kulisovitě	k6eAd1	kulisovitě
postavené	postavený	k2eAgFnPc1d1	postavená
rohovité	rohovitý	k2eAgFnPc1d1	rohovitá
lamely	lamela	k1gFnPc1	lamela
nebo	nebo	k8xC	nebo
zoubky	zoubek	k1gInPc1	zoubek
<g/>
,	,	kIx,	,
kterými	který	k3yRgInPc7	který
svírají	svírat	k5eAaImIp3nP	svírat
potravu	potrava	k1gFnSc4	potrava
jako	jako	k9	jako
kleštěmi	kleště	k1gFnPc7	kleště
nebo	nebo	k8xC	nebo
přecezují	přecezovat	k5eAaImIp3nP	přecezovat
bahno	bahno	k1gNnSc4	bahno
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
obsah	obsah	k1gInSc1	obsah
polykají	polykat	k5eAaImIp3nP	polykat
<g/>
.	.	kIx.	.
</s>
<s>
Vrubozobí	vrubozobí	k1gMnPc1	vrubozobí
mají	mít	k5eAaImIp3nP	mít
proto	proto	k8xC	proto
v	v	k7c6	v
dutině	dutina	k1gFnSc6	dutina
zobáku	zobák	k1gInSc2	zobák
četná	četný	k2eAgNnPc4d1	četné
smyslová	smyslový	k2eAgNnPc4d1	smyslové
tělíska	tělísko	k1gNnPc4	tělísko
<g/>
.	.	kIx.	.
</s>
<s>
Přední	přední	k2eAgInPc4d1	přední
3	[number]	k4	3
prsty	prst	k1gInPc4	prst
běháku	běhák	k1gInSc2	běhák
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgFnP	spojit
plovací	plovací	k2eAgFnSc7d1	plovací
blanou	blána	k1gFnSc7	blána
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
je	být	k5eAaImIp3nS	být
přizpůsobeno	přizpůsobit	k5eAaPmNgNnS	přizpůsobit
k	k	k7c3	k
životu	život	k1gInSc3	život
na	na	k7c6	na
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
hustě	hustě	k6eAd1	hustě
pokryto	pokrýt	k5eAaPmNgNnS	pokrýt
obrysovým	obrysový	k2eAgNnSc7d1	obrysové
peřím	peří	k1gNnSc7	peří
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nepropouští	propouštět	k5eNaImIp3nP	propouštět
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
ním	on	k3xPp3gInSc7	on
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
celé	celý	k2eAgFnSc6d1	celá
ploše	plocha	k1gFnSc6	plocha
těla	tělo	k1gNnSc2	tělo
vrstva	vrstva	k1gFnSc1	vrstva
prachového	prachový	k2eAgNnSc2d1	prachové
peří	peří	k1gNnSc2	peří
<g/>
.	.	kIx.	.
</s>
<s>
Kostrční	kostrční	k2eAgFnSc1d1	kostrční
žláza	žláza	k1gFnSc1	žláza
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
a	a	k8xC	a
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
hojně	hojně	k6eAd1	hojně
olejovitého	olejovitý	k2eAgInSc2d1	olejovitý
sekretu	sekret	k1gInSc2	sekret
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
si	se	k3xPyFc3	se
ptáci	pták	k1gMnPc1	pták
zobákem	zobák	k1gInSc7	zobák
promašťují	promašťovat	k5eAaImIp3nP	promašťovat
peří	peří	k1gNnSc4	peří
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
ptáci	pták	k1gMnPc1	pták
nekrmiví	krmivý	k2eNgMnPc1d1	krmivý
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnPc1	jejich
mláďata	mládě	k1gNnPc1	mládě
jsou	být	k5eAaImIp3nP	být
schopna	schopen	k2eAgNnPc1d1	schopno
samostatné	samostatný	k2eAgFnPc4d1	samostatná
výživy	výživa	k1gFnSc2	výživa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vylíhnutí	vylíhnutí	k1gNnSc6	vylíhnutí
jsou	být	k5eAaImIp3nP	být
hustě	hustě	k6eAd1	hustě
pokryta	pokrýt	k5eAaPmNgNnP	pokrýt
pápěřím	pápěří	k1gNnSc7	pápěří
a	a	k8xC	a
po	po	k7c6	po
uschnutí	uschnutí	k1gNnSc6	uschnutí
jsou	být	k5eAaImIp3nP	být
schopna	schopen	k2eAgFnSc1d1	schopna
chůze	chůze	k1gFnSc1	chůze
i	i	k8xC	i
plavání	plavání	k1gNnSc1	plavání
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
již	již	k9	již
vysoce	vysoce	k6eAd1	vysoce
vyvinutou	vyvinutý	k2eAgFnSc4d1	vyvinutá
regulaci	regulace	k1gFnSc4	regulace
tělesné	tělesný	k2eAgFnSc2d1	tělesná
teploty	teplota	k1gFnSc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Výměna	výměna	k1gFnSc1	výměna
peří	peří	k1gNnSc2	peří
je	být	k5eAaImIp3nS	být
buď	buď	k8xC	buď
jednou	jednou	k6eAd1	jednou
v	v	k7c6	v
roce	rok	k1gInSc6	rok
(	(	kIx(	(
<g/>
husy	husa	k1gFnSc2	husa
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
dvakrát	dvakrát	k6eAd1	dvakrát
do	do	k7c2	do
roka	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
kachny	kachna	k1gFnSc2	kachna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
mají	mít	k5eAaImIp3nP	mít
částečně	částečně	k6eAd1	částečně
vyvinutý	vyvinutý	k2eAgInSc4d1	vyvinutý
penis	penis	k1gInSc4	penis
<g/>
.	.	kIx.	.
</s>
<s>
Názory	názor	k1gInPc1	názor
na	na	k7c4	na
původ	původ	k1gInSc4	původ
a	a	k8xC	a
historii	historie	k1gFnSc4	historie
vzniku	vznik	k1gInSc2	vznik
hlavních	hlavní	k2eAgInPc2d1	hlavní
druhů	druh	k1gInPc2	druh
domácí	domácí	k2eAgFnSc2d1	domácí
drůbeže	drůbež	k1gFnSc2	drůbež
nejsou	být	k5eNaImIp3nP	být
jednotné	jednotný	k2eAgFnPc1d1	jednotná
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
domácích	domácí	k2eAgMnPc2d1	domácí
savců	savec	k1gMnPc2	savec
nečiní	činit	k5eNaImIp3nS	činit
u	u	k7c2	u
domácí	domácí	k2eAgFnSc2d1	domácí
drůbeže	drůbež	k1gFnSc2	drůbež
obtíže	obtíž	k1gFnSc2	obtíž
jejich	jejich	k3xOp3gNnSc2	jejich
odvození	odvození	k1gNnSc2	odvození
od	od	k7c2	od
divokých	divoký	k2eAgInPc2d1	divoký
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mnohem	mnohem	k6eAd1	mnohem
méně	málo	k6eAd2	málo
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
doba	doba	k1gFnSc1	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
k	k	k7c3	k
domestikaci	domestikace	k1gFnSc3	domestikace
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Příslušníci	příslušník	k1gMnPc1	příslušník
rodu	rod	k1gInSc2	rod
Gallus	Gallus	k1gInSc1	Gallus
(	(	kIx(	(
<g/>
kur	kur	k1gMnSc1	kur
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
několika	několik	k4yIc6	několik
druzích	druh	k1gInPc6	druh
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
poddruzích	poddruh	k1gInPc6	poddruh
či	či	k8xC	či
varietách	varieta	k1gFnPc6	varieta
(	(	kIx(	(
<g/>
názvosloví	názvosloví	k1gNnSc6	názvosloví
i	i	k8xC	i
systematické	systematický	k2eAgNnSc1d1	systematické
členění	členění	k1gNnSc1	členění
příslušníku	příslušník	k1gMnSc3	příslušník
rodu	rod	k1gInSc2	rod
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
různých	různý	k2eAgMnPc2d1	různý
autorů	autor	k1gMnPc2	autor
různí	různit	k5eAaImIp3nS	různit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kur	kur	k1gMnSc1	kur
vidličnatý	vidličnatý	k2eAgMnSc1d1	vidličnatý
(	(	kIx(	(
<g/>
k.	k.	k?	k.
měnlivý	měnlivý	k2eAgInSc1d1	měnlivý
<g/>
,	,	kIx,	,
k.	k.	k?	k.
zelený	zelený	k2eAgInSc1d1	zelený
<g/>
)	)	kIx)	)
-	-	kIx~	-
Gallus	Gallus	k1gMnSc1	Gallus
varius	varius	k1gMnSc1	varius
(	(	kIx(	(
<g/>
Shaw	Shaw	k1gMnSc1	Shaw
<g/>
,	,	kIx,	,
1798	[number]	k4	1798
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
Jávě	Jáva	k1gFnSc6	Jáva
<g/>
,	,	kIx,	,
ostrově	ostrov	k1gInSc6	ostrov
Bali	Bali	k1gNnSc2	Bali
a	a	k8xC	a
Malých	Malých	k2eAgNnSc2d1	Malých
Sundách	Sundách	k?	Sundách
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
jeden	jeden	k4xCgInSc4	jeden
lalůček	lalůček	k1gInSc4	lalůček
na	na	k7c6	na
spodině	spodina	k1gFnSc6	spodina
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
kmenovou	kmenový	k2eAgFnSc7d1	kmenová
formou	forma	k1gFnSc7	forma
kura	kur	k1gMnSc2	kur
domácího	domácí	k1gMnSc2	domácí
<g/>
.	.	kIx.	.
</s>
<s>
Kur	kur	k1gMnSc1	kur
čárkovaný	čárkovaný	k2eAgMnSc1d1	čárkovaný
(	(	kIx(	(
<g/>
šedý	šedý	k2eAgInSc1d1	šedý
k.	k.	k?	k.
džunglový	džunglový	k2eAgInSc1d1	džunglový
<g/>
,	,	kIx,	,
k.	k.	k?	k.
Sonneratův	Sonneratův	k2eAgInSc1d1	Sonneratův
<g/>
)	)	kIx)	)
-	-	kIx~	-
Gallus	Gallus	k1gInSc4	Gallus
sonnerati	sonnerat	k5eAaImF	sonnerat
(	(	kIx(	(
<g/>
Temminek	Temminek	k1gInSc1	Temminek
<g/>
,	,	kIx,	,
1813	[number]	k4	1813
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
jižnější	jižní	k2eAgFnSc6d2	jižnější
části	část	k1gFnSc6	část
Přední	přední	k2eAgFnSc2d1	přední
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
kurem	kur	k1gMnSc7	kur
domácím	domácí	k2eAgMnSc7d1	domácí
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
páří	pářit	k5eAaImIp3nS	pářit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kříženci	kříženec	k1gMnPc1	kříženec
jsou	být	k5eAaImIp3nP	být
neplodní	plodní	k2eNgMnPc1d1	neplodní
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
není	být	k5eNaImIp3nS	být
kmenovou	kmenový	k2eAgFnSc7d1	kmenová
formou	forma	k1gFnSc7	forma
kura	kur	k1gMnSc2	kur
domácího	domácí	k1gMnSc2	domácí
<g/>
.	.	kIx.	.
</s>
<s>
Kur	kur	k1gMnSc1	kur
džunglový	džunglový	k2eAgMnSc1d1	džunglový
(	(	kIx(	(
<g/>
cejlonský	cejlonský	k2eAgInSc1d1	cejlonský
k.	k.	k?	k.
džunglový	džunglový	k2eAgInSc1d1	džunglový
<g/>
,	,	kIx,	,
k.	k.	k?	k.
žlutý	žlutý	k2eAgInSc1d1	žlutý
<g/>
,	,	kIx,	,
k.	k.	k?	k.
Lafayetův	Lafayetův	k2eAgInSc1d1	Lafayetův
<g/>
)	)	kIx)	)
-	-	kIx~	-
Gallus	Gallus	k1gInSc1	Gallus
laffayetti	laffayetti	k1gNnSc1	laffayetti
(	(	kIx(	(
<g/>
Lesson	Lesson	k1gNnSc1	Lesson
<g/>
,	,	kIx,	,
1831	[number]	k4	1831
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
pralesích	prales	k1gInPc6	prales
ostrova	ostrov	k1gInSc2	ostrov
Cejlonu	Cejlon	k1gInSc2	Cejlon
<g/>
.	.	kIx.	.
</s>
<s>
Třebaže	třebaže	k8xS	třebaže
se	se	k3xPyFc4	se
plodně	plodně	k6eAd1	plodně
kříží	křížit	k5eAaImIp3nP	křížit
s	s	k7c7	s
kurem	kur	k1gMnSc7	kur
domácím	domácí	k2eAgMnSc7d1	domácí
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
jeho	on	k3xPp3gNnSc2	on
předkem	předek	k1gInSc7	předek
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
další	další	k2eAgInSc1d1	další
druh	druh	k1gInSc1	druh
-	-	kIx~	-
černý	černý	k2eAgMnSc1d1	černý
kur	kur	k1gMnSc1	kur
džunglový	džunglový	k2eAgMnSc1d1	džunglový
-	-	kIx~	-
Gallus	Gallus	k1gInSc1	Gallus
murghi	murgh	k1gFnSc2	murgh
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
předka	předek	k1gMnSc4	předek
kura	kur	k1gMnSc4	kur
domácího	domácí	k1gMnSc4	domácí
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
považován	považován	k2eAgMnSc1d1	považován
kur	kur	k1gMnSc1	kur
divoký	divoký	k2eAgMnSc1d1	divoký
(	(	kIx(	(
<g/>
k.	k.	k?	k.
obecný	obecný	k2eAgInSc1d1	obecný
<g/>
,	,	kIx,	,
k.	k.	k?	k.
hřebenatý	hřebenatý	k2eAgInSc1d1	hřebenatý
<g/>
,	,	kIx,	,
k.	k.	k?	k.
bankivský	bankivský	k2eAgInSc1d1	bankivský
<g/>
,	,	kIx,	,
asijský	asijský	k2eAgInSc1d1	asijský
k.	k.	k?	k.
divoký	divoký	k2eAgInSc1d1	divoký
<g/>
,	,	kIx,	,
červený	červený	k2eAgInSc1d1	červený
k.	k.	k?	k.
džunglový	džunglový	k2eAgInSc1d1	džunglový
<g/>
)	)	kIx)	)
-	-	kIx~	-
Gallus	Gallus	k1gInSc1	Gallus
gallus	gallus	k1gInSc1	gallus
(	(	kIx(	(
<g/>
Linné	Linné	k1gNnSc1	Linné
<g/>
,	,	kIx,	,
1758	[number]	k4	1758
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Páří	pářit	k5eAaImIp3nS	pářit
se	se	k3xPyFc4	se
neomezeně	omezeně	k6eNd1	omezeně
s	s	k7c7	s
kurem	kur	k1gMnSc7	kur
domácím	domácí	k1gMnSc7	domácí
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
kříženci	kříženec	k1gMnPc1	kříženec
jsou	být	k5eAaImIp3nP	být
plně	plně	k6eAd1	plně
plodní	plodní	k2eAgFnPc1d1	plodní
<g/>
.	.	kIx.	.
</s>
<s>
Kur	kur	k1gMnSc1	kur
obecný	obecný	k2eAgMnSc1d1	obecný
osídlil	osídlit	k5eAaPmAgMnS	osídlit
celou	celý	k2eAgFnSc4d1	celá
Indii	Indie	k1gFnSc4	Indie
od	od	k7c2	od
Kašmíru	Kašmír	k1gInSc2	Kašmír
na	na	k7c6	na
západě	západ	k1gInSc6	západ
po	po	k7c4	po
Tonkin	Tonkin	k1gInSc4	Tonkin
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
od	od	k7c2	od
nížinných	nížinný	k2eAgInPc2d1	nížinný
lesů	les	k1gInPc2	les
až	až	k9	až
do	do	k7c2	do
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
ostrovy	ostrov	k1gInPc1	ostrov
Sumatru	Sumatra	k1gFnSc4	Sumatra
<g/>
,	,	kIx,	,
Jávu	Jáva	k1gFnSc4	Jáva
<g/>
,	,	kIx,	,
Celebes	Celebes	k1gInSc4	Celebes
<g/>
,	,	kIx,	,
Filipíny	Filipíny	k1gFnPc4	Filipíny
aj.	aj.	kA	aj.
Podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
autorů	autor	k1gMnPc2	autor
existují	existovat	k5eAaImIp3nP	existovat
podruhy	podruh	k1gMnPc4	podruh
kura	kur	k1gMnSc4	kur
obecného	obecný	k2eAgMnSc4d1	obecný
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Gallus	Gallus	k1gMnSc1	Gallus
gallus	gallus	k1gMnSc1	gallus
ssp	ssp	k?	ssp
<g/>
.	.	kIx.	.
bankiva	bankiv	k1gMnSc2	bankiv
-	-	kIx~	-
kur	kur	k1gMnSc1	kur
bankivský	bankivský	k2eAgMnSc1d1	bankivský
<g/>
.	.	kIx.	.
</s>
<s>
Kur	kur	k1gMnSc1	kur
domácí	domácí	k1gMnSc1	domácí
-	-	kIx~	-
Gallus	Gallus	k1gMnSc1	Gallus
gallus	gallus	k1gMnSc1	gallus
f.	f.	k?	f.
domestica	domestica	k1gFnSc1	domestica
(	(	kIx(	(
<g/>
Linné	Linné	k1gNnSc1	Linné
<g/>
,	,	kIx,	,
1758	[number]	k4	1758
<g/>
)	)	kIx)	)
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
domestikací	domestikace	k1gFnSc7	domestikace
kura	kur	k1gMnSc2	kur
divokého	divoký	k2eAgMnSc2d1	divoký
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
z	z	k7c2	z
některých	některý	k3yIgFnPc2	některý
jeho	jeho	k3xOp3gFnPc2	jeho
variet	varieta	k1gFnPc2	varieta
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
populací	populace	k1gFnPc2	populace
žijících	žijící	k2eAgFnPc2d1	žijící
v	v	k7c6	v
Přední	přední	k2eAgFnSc6d1	přední
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
domestikací	domestikace	k1gFnSc7	domestikace
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
zřejmě	zřejmě	k6eAd1	zřejmě
postupně	postupně	k6eAd1	postupně
<g/>
.	.	kIx.	.
</s>
<s>
Pospolitý	pospolitý	k2eAgInSc1d1	pospolitý
způsob	způsob	k1gInSc1	způsob
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
velká	velký	k2eAgFnSc1d1	velká
ekologická	ekologický	k2eAgFnSc1d1	ekologická
valence	valence	k1gFnSc1	valence
kmenového	kmenový	k2eAgInSc2d1	kmenový
druhu	druh	k1gInSc2	druh
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
pronikání	pronikání	k1gNnSc4	pronikání
až	až	k9	až
k	k	k7c3	k
lidským	lidský	k2eAgNnPc3d1	lidské
obydlím	obydlí	k1gNnPc3	obydlí
dávaly	dávat	k5eAaImAgInP	dávat
podnět	podnět	k1gInSc4	podnět
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
držení	držení	k1gNnSc3	držení
při	při	k7c6	při
lidských	lidský	k2eAgNnPc6d1	lidské
bydlištích	bydliště	k1gNnPc6	bydliště
na	na	k7c6	na
malé	malý	k2eAgFnSc6d1	malá
ploše	plocha	k1gFnSc6	plocha
a	a	k8xC	a
v	v	k7c6	v
primitivních	primitivní	k2eAgFnPc6d1	primitivní
podmínkách	podmínka	k1gFnPc6	podmínka
existence	existence	k1gFnSc2	existence
domorodců	domorodec	k1gMnPc2	domorodec
<g/>
.	.	kIx.	.
</s>
<s>
Zprvu	zprvu	k6eAd1	zprvu
se	se	k3xPyFc4	se
tak	tak	k9	tak
asi	asi	k9	asi
dálo	dít	k5eAaImAgNnS	dít
z	z	k7c2	z
pověrčivosti	pověrčivost	k1gFnSc2	pověrčivost
<g/>
.	.	kIx.	.
</s>
<s>
Kur	kur	k1gMnSc1	kur
totiž	totiž	k9	totiž
svým	svůj	k3xOyFgInSc7	svůj
hlasem	hlas	k1gInSc7	hlas
hlásil	hlásit	k5eAaImAgInS	hlásit
nástup	nástup	k1gInSc1	nástup
nového	nový	k2eAgInSc2d1	nový
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
zřejmě	zřejmě	k6eAd1	zřejmě
stal	stát	k5eAaPmAgInS	stát
symbolem	symbol	k1gInSc7	symbol
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
života	život	k1gInSc2	život
a	a	k8xC	a
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
chování	chování	k1gNnSc4	chování
byl	být	k5eAaImAgInS	být
symbolem	symbol	k1gInSc7	symbol
erotiky	erotika	k1gFnSc2	erotika
a	a	k8xC	a
plodnosti	plodnost	k1gFnSc2	plodnost
a	a	k8xC	a
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
bojovnost	bojovnost	k1gFnSc4	bojovnost
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
symbolem	symbol	k1gInSc7	symbol
mužnosti	mužnost	k1gFnSc2	mužnost
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
jej	on	k3xPp3gMnSc4	on
lidé	člověk	k1gMnPc1	člověk
nejdříve	dříve	k6eAd3	dříve
používali	používat	k5eAaImAgMnP	používat
k	k	k7c3	k
rituálním	rituální	k2eAgInPc3d1	rituální
obřadům	obřad	k1gInPc3	obřad
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kohoutí	kohoutí	k2eAgInPc4d1	kohoutí
zápasy	zápas	k1gInPc4	zápas
a	a	k8xC	a
jako	jako	k9	jako
hospodářsky	hospodářsky	k6eAd1	hospodářsky
cenný	cenný	k2eAgInSc1d1	cenný
byl	být	k5eAaImAgInS	být
využit	využit	k2eAgInSc1d1	využit
teprve	teprve	k6eAd1	teprve
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
jako	jako	k8xC	jako
první	první	k4xOgMnPc1	první
objevili	objevit	k5eAaPmAgMnP	objevit
jeho	jeho	k3xOp3gFnPc4	jeho
schopnosti	schopnost	k1gFnPc4	schopnost
produkovat	produkovat	k5eAaImF	produkovat
maso	maso	k1gNnSc4	maso
pro	pro	k7c4	pro
lidskou	lidský	k2eAgFnSc4d1	lidská
výživu	výživa	k1gFnSc4	výživa
<g/>
.	.	kIx.	.
</s>
<s>
Vejce	vejce	k1gNnSc1	vejce
jako	jako	k8xC	jako
potravina	potravina	k1gFnSc1	potravina
se	se	k3xPyFc4	se
konzumovala	konzumovat	k5eAaBmAgFnS	konzumovat
až	až	k9	až
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
pestře	pestro	k6eAd1	pestro
zbarvené	zbarvený	k2eAgNnSc4d1	zbarvené
nebo	nebo	k8xC	nebo
zkadeřené	zkadeřený	k2eAgNnSc4d1	zkadeřené
či	či	k8xC	či
hedvábné	hedvábný	k2eAgNnSc4d1	hedvábné
peří	peří	k1gNnSc4	peří
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
několik	několik	k4yIc4	několik
metrů	metr	k1gInPc2	metr
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
ocasní	ocasní	k2eAgNnPc4d1	ocasní
pera	pero	k1gNnPc4	pero
<g/>
,	,	kIx,	,
chocholy	chochol	k1gInPc1	chochol
<g/>
,	,	kIx,	,
vousy	vous	k1gInPc1	vous
a	a	k8xC	a
rousy	rous	k1gInPc1	rous
se	se	k3xPyFc4	se
choval	chovat	k5eAaImAgMnS	chovat
také	také	k9	také
pro	pro	k7c4	pro
okrasu	okrasa	k1gFnSc4	okrasa
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dlouhého	dlouhý	k2eAgInSc2d1	dlouhý
procesu	proces	k1gInSc2	proces
domestikace	domestikace	k1gFnSc2	domestikace
bylo	být	k5eAaImAgNnS	být
vyšlechtěno	vyšlechtit	k5eAaPmNgNnS	vyšlechtit
několik	několik	k4yIc1	několik
set	sto	k4xCgNnPc2	sto
různých	různý	k2eAgNnPc2d1	různé
plemen	plemeno	k1gNnPc2	plemeno
<g/>
,	,	kIx,	,
u	u	k7c2	u
některých	některý	k3yIgNnPc2	některý
plemen	plemeno	k1gNnPc2	plemeno
ještě	ještě	k9	ještě
řada	řada	k1gFnSc1	řada
barevných	barevný	k2eAgInPc2d1	barevný
rázů	ráz	k1gInPc2	ráz
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc1	počátek
domestikace	domestikace	k1gFnSc2	domestikace
kura	kur	k1gMnSc2	kur
domácího	domácí	k1gMnSc2	domácí
sahají	sahat	k5eAaImIp3nP	sahat
asi	asi	k9	asi
až	až	k9	až
do	do	k7c2	do
5	[number]	k4	5
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
<g/>
K.	K.	kA	K.
a	a	k8xC	a
za	za	k7c4	za
kolébku	kolébka	k1gFnSc4	kolébka
domestikace	domestikace	k1gFnSc2	domestikace
se	se	k3xPyFc4	se
zřejmě	zřejmě	k6eAd1	zřejmě
může	moct	k5eAaImIp3nS	moct
považovat	považovat	k5eAaImF	považovat
Přední	přední	k2eAgFnSc1d1	přední
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
povodí	povodí	k1gNnSc2	povodí
řeky	řeka	k1gFnSc2	řeka
Indus	Indus	k1gInSc1	Indus
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Indočína	Indočína	k1gFnSc1	Indočína
<g/>
,	,	kIx,	,
Malajsko	Malajsko	k1gNnSc1	Malajsko
a	a	k8xC	a
Indonésie	Indonésie	k1gFnSc1	Indonésie
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
kur	kur	k1gMnSc1	kur
domácí	domácí	k1gMnSc1	domácí
poměrně	poměrně	k6eAd1	poměrně
rychle	rychle	k6eAd1	rychle
šířil	šířit	k5eAaImAgInS	šířit
do	do	k7c2	do
sousedních	sousední	k2eAgFnPc2d1	sousední
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
i	i	k9	i
na	na	k7c4	na
západ	západ	k1gInSc4	západ
do	do	k7c2	do
Íránu	Írán	k1gInSc2	Írán
<g/>
,	,	kIx,	,
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
aj.	aj.	kA	aj.
Ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
<g/>
K.	K.	kA	K.
jej	on	k3xPp3gMnSc4	on
chovali	chovat	k5eAaImAgMnP	chovat
již	již	k6eAd1	již
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zprvu	zprvu	k6eAd1	zprvu
jako	jako	k8xC	jako
posvátného	posvátný	k2eAgMnSc4d1	posvátný
ptáka	pták	k1gMnSc4	pták
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
K.	K.	kA	K.
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
jako	jako	k8xC	jako
hospodářské	hospodářský	k2eAgNnSc1d1	hospodářské
zvíře	zvíře	k1gNnSc1	zvíře
(	(	kIx(	(
<g/>
umělé	umělý	k2eAgNnSc1d1	umělé
líhnutí	líhnutí	k1gNnSc1	líhnutí
kuřat	kuře	k1gNnPc2	kuře
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
<g/>
K.	K.	kA	K.
jej	on	k3xPp3gMnSc4	on
znají	znát	k5eAaImIp3nP	znát
již	již	k9	již
Řekové	Řek	k1gMnPc1	Řek
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
perského	perský	k2eAgMnSc2d1	perský
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
medijského	medijský	k2eAgMnSc2d1	medijský
<g/>
"	"	kIx"	"
ptáka	pták	k1gMnSc2	pták
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zřejmě	zřejmě	k6eAd1	zřejmě
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
cestami	cesta	k1gFnPc7	cesta
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgInPc7	jenž
se	se	k3xPyFc4	se
kur	kur	k1gMnSc1	kur
domácí	domácí	k1gMnSc1	domácí
dostával	dostávat	k5eAaImAgMnS	dostávat
na	na	k7c4	na
západ	západ	k1gInSc4	západ
již	již	k6eAd1	již
před	před	k7c7	před
politickým	politický	k2eAgNnSc7d1	politické
panstvím	panství	k1gNnSc7	panství
Peršanů	Peršan	k1gMnPc2	Peršan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
<g/>
K.	K.	kA	K.
byl	být	k5eAaImAgMnS	být
kur	kur	k1gMnSc1	kur
domácí	domácí	k1gMnSc1	domácí
rozšířen	rozšířit	k5eAaPmNgMnS	rozšířit
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
Středomoří	středomoří	k1gNnSc6	středomoří
a	a	k8xC	a
národy	národ	k1gInPc4	národ
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
seznámily	seznámit	k5eAaPmAgInP	seznámit
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
doby	doba	k1gFnSc2	doba
železné	železný	k2eAgFnSc2d1	železná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
K.	K.	kA	K.
jej	on	k3xPp3gMnSc4	on
znali	znát	k5eAaImAgMnP	znát
již	již	k9	již
Keltové	Kelt	k1gMnPc1	Kelt
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
široké	široký	k2eAgNnSc1d1	široké
uplatnění	uplatnění	k1gNnSc1	uplatnění
je	být	k5eAaImIp3nS	být
dílem	dílem	k6eAd1	dílem
Římanů	Říman	k1gMnPc2	Říman
<g/>
,	,	kIx,	,
např.	např.	kA	např.
římský	římský	k2eAgMnSc1d1	římský
letopisec	letopisec	k1gMnSc1	letopisec
Collumela	Collumel	k1gMnSc2	Collumel
vydal	vydat	k5eAaPmAgInS	vydat
spis	spis	k1gInSc1	spis
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
zásady	zásada	k1gFnPc4	zásada
chovu	chov	k1gInSc2	chov
a	a	k8xC	a
krmení	krmení	k1gNnSc1	krmení
domácích	domácí	k2eAgMnPc2d1	domácí
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Středověk	středověk	k1gInSc1	středověk
podtrhl	podtrhnout	k5eAaPmAgInS	podtrhnout
ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
význam	význam	k1gInSc1	význam
kura	kur	k1gMnSc2	kur
domácího	domácí	k1gMnSc2	domácí
jako	jako	k8xS	jako
zdroje	zdroj	k1gInSc2	zdroj
postních	postní	k2eAgNnPc2d1	postní
jídel	jídlo	k1gNnPc2	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Značný	značný	k2eAgInSc4d1	značný
význam	význam	k1gInSc4	význam
pro	pro	k7c4	pro
domestikaci	domestikace	k1gFnSc4	domestikace
měla	mít	k5eAaImAgFnS	mít
nenáročnost	nenáročnost	k1gFnSc1	nenáročnost
kura	kur	k1gMnSc2	kur
domácího	domácí	k1gMnSc2	domácí
a	a	k8xC	a
přizpůsobivost	přizpůsobivost	k1gFnSc4	přizpůsobivost
primitivním	primitivní	k2eAgFnPc3d1	primitivní
podmínkám	podmínka	k1gFnPc3	podmínka
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
chovů	chov	k1gInPc2	chov
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
část	část	k1gFnSc1	část
potravy	potrava	k1gFnSc2	potrava
si	se	k3xPyFc3	se
kur	kur	k1gMnSc1	kur
obstarával	obstarávat	k5eAaImAgMnS	obstarávat
sám	sám	k3xTgMnSc1	sám
z	z	k7c2	z
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
i	i	k8xC	i
možnost	možnost	k1gFnSc1	možnost
jej	on	k3xPp3gMnSc4	on
všestranně	všestranně	k6eAd1	všestranně
zužitkovat	zužitkovat	k5eAaPmF	zužitkovat
(	(	kIx(	(
<g/>
peří	peří	k1gNnSc1	peří
<g/>
,	,	kIx,	,
vejce	vejce	k1gNnSc1	vejce
<g/>
,	,	kIx,	,
maso	maso	k1gNnSc1	maso
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Krůta	krůta	k1gFnSc1	krůta
domácí	domácí	k2eAgFnSc1d1	domácí
-	-	kIx~	-
Meleagris	Meleagris	k1gFnSc1	Meleagris
gallopavo	gallopava	k1gFnSc5	gallopava
f.	f.	k?	f.
domestica	domestica	k1gFnSc1	domestica
(	(	kIx(	(
<g/>
Linné	Linné	k1gNnSc1	Linné
<g/>
,	,	kIx,	,
1758	[number]	k4	1758
<g/>
)	)	kIx)	)
-	-	kIx~	-
byla	být	k5eAaImAgFnS	být
domestikován	domestikován	k2eAgInSc4d1	domestikován
již	již	k6eAd1	již
indiánskými	indiánský	k2eAgInPc7d1	indiánský
kmeny	kmen	k1gInPc7	kmen
(	(	kIx(	(
<g/>
Aztéky	Azték	k1gMnPc7	Azték
<g/>
)	)	kIx)	)
dávno	dávno	k6eAd1	dávno
před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
Evropanů	Evropan	k1gMnPc2	Evropan
z	z	k7c2	z
mexické	mexický	k2eAgFnSc2d1	mexická
formy	forma	k1gFnSc2	forma
krůty	krůta	k1gFnSc2	krůta
divoké	divoký	k2eAgFnSc2d1	divoká
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
žije	žít	k5eAaImIp3nS	žít
dodnes	dodnes	k6eAd1	dodnes
volně	volně	k6eAd1	volně
v	v	k7c6	v
málo	málo	k6eAd1	málo
obydlených	obydlený	k2eAgFnPc6d1	obydlená
oblastech	oblast	k1gFnPc6	oblast
Severní	severní	k2eAgFnSc2d1	severní
a	a	k8xC	a
Střední	střední	k2eAgFnSc2d1	střední
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
byla	být	k5eAaImAgFnS	být
dovezena	dovezen	k2eAgFnSc1d1	dovezena
Španěly	Španěl	k1gMnPc4	Španěl
r.	r.	kA	r.
1523	[number]	k4	1523
<g/>
-	-	kIx~	-
<g/>
24	[number]	k4	24
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejdříve	dříve	k6eAd3	dříve
samci	samec	k1gMnPc1	samec
<g/>
;	;	kIx,	;
ještě	ještě	k9	ještě
v	v	k7c6	v
r.	r.	kA	r.
1530	[number]	k4	1530
nebyly	být	k5eNaImAgInP	být
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
známy	znám	k2eAgFnPc1d1	známa
samice	samice	k1gFnPc1	samice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1550-60	[number]	k4	1550-60
se	se	k3xPyFc4	se
však	však	k9	však
už	už	k6eAd1	už
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
pro	pro	k7c4	pro
chutné	chutný	k2eAgNnSc4d1	chutné
maso	maso	k1gNnSc4	maso
a	a	k8xC	a
dostává	dostávat	k5eAaImIp3nS	dostávat
se	se	k3xPyFc4	se
i	i	k9	i
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
tradičním	tradiční	k2eAgNnSc7d1	tradiční
oblíbeným	oblíbený	k2eAgNnSc7d1	oblíbené
jídlem	jídlo	k1gNnSc7	jídlo
o	o	k7c6	o
vánocích	vánoce	k1gFnPc6	vánoce
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nám	my	k3xPp1nPc3	my
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
až	až	k9	až
o	o	k7c4	o
100	[number]	k4	100
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Perlička	perlička	k1gFnSc1	perlička
domácí	domácí	k2eAgFnSc1d1	domácí
-	-	kIx~	-
Numida	Numida	k1gFnSc1	Numida
meleagris	meleagris	k1gFnSc2	meleagris
f.	f.	k?	f.
domestica	domestica	k1gFnSc1	domestica
(	(	kIx(	(
<g/>
Linné	Linné	k1gNnSc1	Linné
<g/>
,	,	kIx,	,
1758	[number]	k4	1758
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
zdomácněna	zdomácnit	k5eAaPmNgFnS	zdomácnit
již	již	k6eAd1	již
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Východiskem	východisko	k1gNnSc7	východisko
k	k	k7c3	k
domestikaci	domestikace	k1gFnSc3	domestikace
byla	být	k5eAaImAgFnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
perlička	perlička	k1gFnSc1	perlička
kropenatá	kropenatý	k2eAgFnSc1d1	kropenatá
-	-	kIx~	-
N.	N.	kA	N.
meleagris	meleagris	k1gFnSc1	meleagris
(	(	kIx(	(
<g/>
Linné	Linné	k1gNnSc1	Linné
<g/>
,	,	kIx,	,
1758	[number]	k4	1758
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
typickou	typický	k2eAgFnSc7d1	typická
přilbou	přilba	k1gFnSc7	přilba
(	(	kIx(	(
<g/>
kostěný	kostěný	k2eAgInSc1d1	kostěný
výrůstek	výrůstek	k1gInSc1	výrůstek
na	na	k7c6	na
lebce	lebka	k1gFnSc6	lebka
<g/>
)	)	kIx)	)
a	a	k8xC	a
žije	žít	k5eAaImIp3nS	žít
dodnes	dodnes	k6eAd1	dodnes
divoce	divoce	k6eAd1	divoce
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
řadě	řada	k1gFnSc6	řada
variet	varieta	k1gFnPc2	varieta
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavní	pohlavní	k2eAgInSc1d1	pohlavní
dimorfismus	dimorfismus	k1gInSc1	dimorfismus
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgInSc1d1	malý
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
byla	být	k5eAaImAgFnS	být
dovezena	dovézt	k5eAaPmNgFnS	dovézt
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nadvakrát	nadvakrát	k6eAd1	nadvakrát
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
její	její	k3xOp3gFnSc4	její
domestikovanou	domestikovaný	k2eAgFnSc4d1	domestikovaná
formu	forma	k1gFnSc4	forma
získali	získat	k5eAaPmAgMnP	získat
od	od	k7c2	od
Féničanů	Féničan	k1gMnPc2	Féničan
Řekové	Řek	k1gMnPc1	Řek
kolem	kolem	k7c2	kolem
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
<g/>
K.	K.	kA	K.
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yRgFnPc4	který
byla	být	k5eAaImAgFnS	být
posvátným	posvátný	k2eAgMnSc7d1	posvátný
ptákem	pták	k1gMnSc7	pták
<g/>
,	,	kIx,	,
používaným	používaný	k2eAgMnSc7d1	používaný
k	k	k7c3	k
liturgickým	liturgický	k2eAgInPc3d1	liturgický
obřadům	obřad	k1gInPc3	obřad
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
počátku	počátek	k1gInSc2	počátek
naší	náš	k3xOp1gFnSc2	náš
éry	éra	k1gFnSc2	éra
ji	on	k3xPp3gFnSc4	on
znali	znát	k5eAaImAgMnP	znát
i	i	k9	i
Římané	Říman	k1gMnPc1	Říman
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
cenili	cenit	k5eAaImAgMnP	cenit
zvláště	zvláště	k6eAd1	zvláště
jejího	její	k3xOp3gNnSc2	její
chutného	chutný	k2eAgNnSc2d1	chutné
masa	maso	k1gNnSc2	maso
a	a	k8xC	a
vajec	vejce	k1gNnPc2	vejce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
její	její	k3xOp3gInSc4	její
chov	chov	k1gInSc4	chov
upadl	upadnout	k5eAaPmAgMnS	upadnout
do	do	k7c2	do
zapomenutí	zapomenutí	k1gNnSc2	zapomenutí
<g/>
.	.	kIx.	.
</s>
<s>
Podruhé	podruhé	k6eAd1	podruhé
byla	být	k5eAaImAgNnP	být
dovezena	dovézt	k5eAaPmNgNnP	dovézt
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
kolem	kolem	k7c2	kolem
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgInP	být
do	do	k7c2	do
Španělska	Španělsko	k1gNnSc2	Španělsko
dovezeny	dovézt	k5eAaPmNgFnP	dovézt
ze	z	k7c2	z
západní	západní	k2eAgFnSc2d1	západní
Afriky	Afrika	k1gFnSc2	Afrika
(	(	kIx(	(
<g/>
Guinea	Guinea	k1gFnSc1	Guinea
<g/>
)	)	kIx)	)
již	již	k6eAd1	již
polodomestikované	polodomestikovaný	k2eAgFnSc2d1	polodomestikovaný
perličky	perlička	k1gFnSc2	perlička
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
částečně	částečně	k6eAd1	částečně
zdivočela	zdivočet	k5eAaPmAgFnS	zdivočet
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
byla	být	k5eAaImAgFnS	být
úplně	úplně	k6eAd1	úplně
domestikována	domestikován	k2eAgFnSc1d1	domestikována
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
zpětně	zpětně	k6eAd1	zpětně
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
perlička	perlička	k1gFnSc1	perlička
nechovala	chovat	k5eNaImAgFnS	chovat
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
jako	jako	k8xS	jako
domácí	domácí	k2eAgNnSc4d1	domácí
zvíře	zvíře	k1gNnSc4	zvíře
a	a	k8xC	a
proč	proč	k6eAd1	proč
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
nejmladší	mladý	k2eAgNnSc1d3	nejmladší
naše	náš	k3xOp1gNnSc4	náš
domestikované	domestikovaný	k2eAgNnSc4d1	domestikované
zvíře	zvíře	k1gNnSc4	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Chová	chovat	k5eAaImIp3nS	chovat
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
vynikající	vynikající	k2eAgNnSc4d1	vynikající
maso	maso	k1gNnSc4	maso
a	a	k8xC	a
vejce	vejce	k1gNnSc4	vejce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
skladovaná	skladovaný	k2eAgFnSc1d1	skladovaná
za	za	k7c2	za
obvyklých	obvyklý	k2eAgFnPc2d1	obvyklá
podmínek	podmínka	k1gFnPc2	podmínka
si	se	k3xPyFc3	se
dlouho	dlouho	k6eAd1	dlouho
podržují	podržovat	k5eAaImIp3nP	podržovat
charakter	charakter	k1gInSc4	charakter
čerstvých	čerstvý	k2eAgNnPc2d1	čerstvé
vajec	vejce	k1gNnPc2	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc4	její
chov	chov	k1gInSc4	chov
nikdy	nikdy	k6eAd1	nikdy
nedosáhl	dosáhnout	k5eNaPmAgMnS	dosáhnout
významu	význam	k1gInSc2	význam
kura	kur	k1gMnSc2	kur
domácího	domácí	k2eAgMnSc2d1	domácí
<g/>
.	.	kIx.	.
</s>
<s>
Domestikací	domestikace	k1gFnPc2	domestikace
se	se	k3xPyFc4	se
perličky	perlička	k1gFnPc1	perlička
také	také	k9	také
mnoho	mnoho	k6eAd1	mnoho
nezměnily	změnit	k5eNaPmAgFnP	změnit
od	od	k7c2	od
svých	svůj	k3xOyFgInPc2	svůj
divokých	divoký	k2eAgInPc2d1	divoký
předků	předek	k1gInPc2	předek
-	-	kIx~	-
jsou	být	k5eAaImIp3nP	být
plaché	plachý	k2eAgFnPc1d1	plachá
<g/>
,	,	kIx,	,
bojovné	bojovný	k2eAgFnPc1d1	bojovná
<g/>
,	,	kIx,	,
toulají	toulat	k5eAaImIp3nP	toulat
se	se	k3xPyFc4	se
a	a	k8xC	a
nesnášejí	snášet	k5eNaImIp3nP	snášet
se	se	k3xPyFc4	se
s	s	k7c7	s
ostatní	ostatní	k2eAgFnSc7d1	ostatní
drůbeží	drůbež	k1gFnSc7	drůbež
<g/>
.	.	kIx.	.
</s>
<s>
Pronikavý	pronikavý	k2eAgInSc1d1	pronikavý
hlas	hlas	k1gInSc1	hlas
perliček	perlička	k1gFnPc2	perlička
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
příčinou	příčina	k1gFnSc7	příčina
i	i	k8xC	i
jejich	jejich	k3xOp3gNnSc2	jejich
menšího	malý	k2eAgNnSc2d2	menší
rozšíření	rozšíření	k1gNnSc2	rozšíření
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
zobák	zobák	k1gInSc4	zobák
na	na	k7c6	na
konci	konec	k1gInSc6	konec
vždy	vždy	k6eAd1	vždy
plochý	plochý	k2eAgInSc1d1	plochý
<g/>
,	,	kIx,	,
zakončený	zakončený	k2eAgInSc1d1	zakončený
malým	malý	k2eAgInSc7d1	malý
nehtem	nehet	k1gInSc7	nehet
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
okraji	okraj	k1gInSc6	okraj
jsou	být	k5eAaImIp3nP	být
vyvinuty	vyvinut	k2eAgFnPc1d1	vyvinuta
rohovité	rohovitý	k2eAgFnPc1d1	rohovitá
lamely	lamela	k1gFnPc1	lamela
<g/>
.	.	kIx.	.
</s>
<s>
Běhák	běhák	k1gInSc1	běhák
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
boku	bok	k1gInSc2	bok
stlačen	stlačen	k2eAgMnSc1d1	stlačen
a	a	k8xC	a
vpředu	vpříst	k5eAaPmIp1nS	vpříst
pokryt	pokrýt	k5eAaPmNgInS	pokrýt
příčnými	příčný	k2eAgInPc7d1	příčný
rohovitými	rohovitý	k2eAgInPc7d1	rohovitý
štítky	štítek	k1gInPc7	štítek
<g/>
.	.	kIx.	.
</s>
<s>
Ryby	Ryby	k1gFnPc1	Ryby
neloví	lovit	k5eNaImIp3nP	lovit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
rybničnímu	rybniční	k2eAgNnSc3d1	rybniční
hospodářství	hospodářství	k1gNnSc3	hospodářství
spíše	spíše	k9	spíše
prospívají	prospívat	k5eAaImIp3nP	prospívat
<g/>
.	.	kIx.	.
</s>
<s>
Účastní	účastnit	k5eAaImIp3nP	účastnit
se	se	k3xPyFc4	se
převodu	převod	k1gInSc3	převod
živin	živina	k1gFnPc2	živina
ze	z	k7c2	z
souše	souš	k1gFnSc2	souš
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
hnojením	hnojení	k1gNnSc7	hnojení
rybníků	rybník	k1gInPc2	rybník
svým	svůj	k3xOyFgInSc7	svůj
trusem	trus	k1gInSc7	trus
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
využívalo	využívat	k5eAaPmAgNnS	využívat
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
kaprokachním	kaprokachnět	k5eAaPmIp1nS	kaprokachnět
hospodářství	hospodářství	k1gNnSc1	hospodářství
(	(	kIx(	(
<g/>
ryby	ryba	k1gFnPc1	ryba
požírají	požírat	k5eAaImIp3nP	požírat
i	i	k9	i
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
strávené	strávený	k2eAgFnSc2d1	strávená
části	část	k1gFnSc2	část
potravy	potrava	k1gFnSc2	potrava
obsažené	obsažený	k2eAgFnSc2d1	obsažená
v	v	k7c6	v
trusu	trus	k1gInSc6	trus
a	a	k8xC	a
trus	trus	k1gInSc1	trus
napomáhá	napomáhat	k5eAaImIp3nS	napomáhat
rozvoji	rozvoj	k1gInSc3	rozvoj
planktonu	plankton	k1gInSc2	plankton
a	a	k8xC	a
bentosu	bentosa	k1gFnSc4	bentosa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kachna	kachna	k1gFnSc1	kachna
domácí	domácí	k2eAgFnSc1d1	domácí
-	-	kIx~	-
Anas	Anas	k1gInSc1	Anas
platyrhynchos	platyrhynchos	k1gInSc1	platyrhynchos
f.	f.	k?	f.
domestica	domestica	k1gFnSc1	domestica
(	(	kIx(	(
<g/>
Linné	Linné	k1gNnSc1	Linné
<g/>
,	,	kIx,	,
1758	[number]	k4	1758
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyšlechtěna	vyšlechtěn	k2eAgFnSc1d1	vyšlechtěna
z	z	k7c2	z
kachny	kachna	k1gFnSc2	kachna
divoké	divoký	k2eAgFnSc2d1	divoká
tzv.	tzv.	kA	tzv.
březňačky	březňačka	k1gFnSc2	březňačka
(	(	kIx(	(
<g/>
Anas	Anas	k1gInSc1	Anas
platyrhynchos	platyrhynchosa	k1gFnPc2	platyrhynchosa
Linné	Linná	k1gFnSc2	Linná
<g/>
,	,	kIx,	,
1758	[number]	k4	1758
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
poznali	poznat	k5eAaPmAgMnP	poznat
kachnu	kachna	k1gFnSc4	kachna
domácí	domácí	k2eAgMnPc1d1	domácí
u	u	k7c2	u
národů	národ	k1gInPc2	národ
sídlících	sídlící	k2eAgInPc2d1	sídlící
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
Eufratu	Eufrat	k1gInSc2	Eufrat
a	a	k8xC	a
Tigridu	Tigris	k1gInSc2	Tigris
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
chovaly	chovat	k5eAaImAgFnP	chovat
kachny	kachna	k1gFnPc4	kachna
již	již	k9	již
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Tamní	tamní	k2eAgNnSc1d1	tamní
domestikační	domestikační	k2eAgNnSc1d1	domestikační
centrum	centrum	k1gNnSc1	centrum
navazovalo	navazovat	k5eAaImAgNnS	navazovat
na	na	k7c4	na
domestikační	domestikační	k2eAgNnSc4d1	domestikační
centrum	centrum	k1gNnSc4	centrum
kachny	kachna	k1gFnSc2	kachna
domácí	domácí	k2eAgFnSc2d1	domácí
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
a	a	k8xC	a
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Římanům	Říman	k1gMnPc3	Říman
patří	patřit	k5eAaImIp3nS	patřit
zásluha	zásluha	k1gFnSc1	zásluha
<g/>
,	,	kIx,	,
že	že	k8xS	že
znalost	znalost	k1gFnSc1	znalost
chovu	chov	k1gInSc2	chov
kachny	kachna	k1gFnSc2	kachna
domácí	domácí	k1gMnPc4	domácí
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
na	na	k7c6	na
území	území	k1gNnSc6	území
římského	římský	k2eAgNnSc2d1	římské
impéria	impérium	k1gNnSc2	impérium
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
do	do	k7c2	do
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Domestikací	domestikace	k1gFnSc7	domestikace
kachny	kachna	k1gFnSc2	kachna
divoké	divoký	k2eAgFnSc2d1	divoká
se	se	k3xPyFc4	se
podstatně	podstatně	k6eAd1	podstatně
nezměnil	změnit	k5eNaPmAgInS	změnit
její	její	k3xOp3gInSc4	její
tělesný	tělesný	k2eAgInSc4d1	tělesný
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
mnohdy	mnohdy	k6eAd1	mnohdy
ani	ani	k8xC	ani
divoké	divoký	k2eAgNnSc1d1	divoké
zbarvení	zbarvení	k1gNnSc1	zbarvení
jejích	její	k3xOp3gMnPc2	její
předků	předek	k1gMnPc2	předek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pozměnily	pozměnit	k5eAaPmAgFnP	pozměnit
se	se	k3xPyFc4	se
její	její	k3xOp3gFnPc1	její
vlastnosti	vlastnost	k1gFnPc1	vlastnost
fyziologické	fyziologický	k2eAgFnPc1d1	fyziologická
a	a	k8xC	a
psychické	psychický	k2eAgFnPc1d1	psychická
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
původně	původně	k6eAd1	původně
monogamní	monogamní	k2eAgFnSc2d1	monogamní
formy	forma	k1gFnSc2	forma
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
forma	forma	k1gFnSc1	forma
polygamní	polygamní	k2eAgFnSc2d1	polygamní
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
potlačeny	potlačen	k2eAgInPc1d1	potlačen
rodičovské	rodičovský	k2eAgInPc1d1	rodičovský
instinkty	instinkt	k1gInPc1	instinkt
(	(	kIx(	(
<g/>
nesedí	sedit	k5eNaImIp3nP	sedit
na	na	k7c6	na
vejcích	vejce	k1gNnPc6	vejce
<g/>
,	,	kIx,	,
nestaví	stavit	k5eNaImIp3nP	stavit
hnízda	hnízdo	k1gNnPc1	hnízdo
<g/>
,	,	kIx,	,
nevystýlá	vystýlat	k5eNaImIp3nS	vystýlat
je	být	k5eAaImIp3nS	být
prachovým	prachový	k2eAgNnSc7d1	prachové
peřím	peří	k1gNnSc7	peří
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
narušena	narušit	k5eAaPmNgFnS	narušit
periodicita	periodicita	k1gFnSc1	periodicita
snášky	snáška	k1gFnSc2	snáška
vajec	vejce	k1gNnPc2	vejce
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
potlačen	potlačen	k2eAgInSc1d1	potlačen
stěhovací	stěhovací	k2eAgInSc1d1	stěhovací
pud	pud	k1gInSc1	pud
<g/>
,	,	kIx,	,
rytmus	rytmus	k1gInSc1	rytmus
života	život	k1gInSc2	život
změněn	změnit	k5eAaPmNgInS	změnit
na	na	k7c4	na
denní	denní	k2eAgNnSc4d1	denní
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Domestikaci	domestikace	k1gFnSc4	domestikace
kachny	kachna	k1gFnSc2	kachna
divoké	divoký	k2eAgFnSc2d1	divoká
na	na	k7c4	na
kachnu	kachna	k1gFnSc4	kachna
domácí	domácí	k1gFnSc2	domácí
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
několik	několik	k4yIc1	několik
okolností	okolnost	k1gFnPc2	okolnost
<g/>
,	,	kIx,	,
např.	např.	kA	např.
i	i	k8xC	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
dobře	dobře	k6eAd1	dobře
snáší	snášet	k5eAaImIp3nS	snášet
lidská	lidský	k2eAgNnPc4d1	lidské
sídliště	sídliště	k1gNnPc4	sídliště
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
silný	silný	k2eAgInSc1d1	silný
sdružovací	sdružovací	k2eAgInSc1d1	sdružovací
pud	pud	k1gInSc1	pud
s	s	k7c7	s
příslušníky	příslušník	k1gMnPc7	příslušník
svého	své	k1gNnSc2	své
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
jinými	jiný	k2eAgMnPc7d1	jiný
ptáky	pták	k1gMnPc7	pták
i	i	k8xC	i
savci	savec	k1gMnPc7	savec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
všežravá	všežravý	k2eAgFnSc1d1	všežravá
a	a	k8xC	a
nenáročná	náročný	k2eNgFnSc1d1	nenáročná
na	na	k7c4	na
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
byla	být	k5eAaImAgFnS	být
kachna	kachna	k1gFnSc1	kachna
zdomácněna	zdomácněn	k2eAgFnSc1d1	zdomácněna
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
kachny	kachna	k1gFnPc1	kachna
tamního	tamní	k2eAgInSc2d1	tamní
původu	původ	k1gInSc2	původ
domestikací	domestikace	k1gFnPc2	domestikace
více	hodně	k6eAd2	hodně
změněny	změněn	k2eAgFnPc1d1	změněna
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
kachny	kachna	k1gFnSc2	kachna
pekingské	pekingský	k2eAgFnSc2d1	Pekingská
mají	mít	k5eAaImIp3nP	mít
bílé	bílý	k2eAgNnSc1d1	bílé
peří	peří	k1gNnSc1	peří
<g/>
,	,	kIx,	,
chodí	chodit	k5eAaImIp3nS	chodit
vzpřímeněji	vzpřímeně	k6eAd2	vzpřímeně
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
odolnější	odolný	k2eAgInSc4d2	odolnější
proti	proti	k7c3	proti
nepříznivým	příznivý	k2eNgFnPc3d1	nepříznivá
podmínkám	podmínka	k1gFnPc3	podmínka
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
rostou	růst	k5eAaImIp3nP	růst
rychleji	rychle	k6eAd2	rychle
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
maso	maso	k1gNnSc4	maso
a	a	k8xC	a
peří	peří	k1gNnSc4	peří
jsou	být	k5eAaImIp3nP	být
dobré	dobrý	k2eAgFnPc4d1	dobrá
kvality	kvalita	k1gFnPc4	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1873	[number]	k4	1873
byly	být	k5eAaImAgFnP	být
převezeny	převézt	k5eAaPmNgFnP	převézt
do	do	k7c2	do
Sev	Sev	k1gFnSc2	Sev
<g/>
.	.	kIx.	.
</s>
<s>
Ameriky	Amerika	k1gFnPc1	Amerika
a	a	k8xC	a
tam	tam	k6eAd1	tam
dále	daleko	k6eAd2	daleko
prošlechtěny	prošlechtěn	k2eAgFnPc1d1	prošlechtěn
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc7	třetí
domestikační	domestikační	k2eAgFnSc7d1	domestikační
oblastí	oblast	k1gFnSc7	oblast
kachny	kachna	k1gFnSc2	kachna
je	být	k5eAaImIp3nS	být
indická	indický	k2eAgFnSc1d1	indická
kulturní	kulturní	k2eAgFnSc1d1	kulturní
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
forma	forma	k1gFnSc1	forma
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
indický	indický	k2eAgMnSc1d1	indický
běžec	běžec	k1gMnSc1	běžec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
kachna	kachna	k1gFnSc1	kachna
lehkého	lehký	k2eAgNnSc2d1	lehké
vzpřímeného	vzpřímený	k2eAgNnSc2d1	vzpřímené
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
s	s	k7c7	s
hlavou	hlava	k1gFnSc7	hlava
vysoko	vysoko	k6eAd1	vysoko
nesenou	nesený	k2eAgFnSc7d1	nesená
na	na	k7c6	na
štíhlém	štíhlý	k2eAgInSc6d1	štíhlý
krku	krk	k1gInSc6	krk
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	se	k3xPyFc4	se
celoroční	celoroční	k2eAgFnSc7d1	celoroční
snáškou	snáška	k1gFnSc7	snáška
(	(	kIx(	(
<g/>
150	[number]	k4	150
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
vajec	vejce	k1gNnPc2	vejce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
i	i	k9	i
do	do	k7c2	do
okolních	okolní	k2eAgFnPc2d1	okolní
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
do	do	k7c2	do
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
holá	holý	k2eAgFnSc1d1	holá
červená	červená	k1gFnSc1	červená
místa	místo	k1gNnSc2	místo
páchnoucí	páchnoucí	k2eAgFnSc6d1	páchnoucí
pižmem	pižmo	k1gNnSc7	pižmo
<g/>
.	.	kIx.	.
</s>
<s>
Pocházejí	pocházet	k5eAaImIp3nP	pocházet
většinou	většinou	k6eAd1	většinou
z	z	k7c2	z
tropických	tropický	k2eAgFnPc2d1	tropická
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Pižmovka	pižmovka	k1gFnSc1	pižmovka
domácí	domácí	k2eAgFnSc1d1	domácí
-	-	kIx~	-
Cairina	Cairin	k2eAgFnSc1d1	Cairina
moschata	moschata	k1gFnSc1	moschata
f.	f.	k?	f.
domestica	domestica	k1gFnSc1	domestica
(	(	kIx(	(
<g/>
Linné	Linné	k1gNnSc1	Linné
<g/>
,	,	kIx,	,
1758	[number]	k4	1758
<g/>
)	)	kIx)	)
bývá	bývat	k5eAaImIp3nS	bývat
v	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
praxi	praxe	k1gFnSc6	praxe
označována	označován	k2eAgFnSc1d1	označována
jako	jako	k8xC	jako
kachna	kachna	k1gFnSc1	kachna
pižmová	pižmový	k2eAgFnSc1d1	pižmová
<g/>
.	.	kIx.	.
</s>
<s>
Kmenovou	kmenový	k2eAgFnSc7d1	kmenová
formou	forma	k1gFnSc7	forma
pižmovky	pižmovka	k1gFnSc2	pižmovka
domácí	domácí	k1gFnSc2	domácí
je	být	k5eAaImIp3nS	být
pižmovka	pižmovka	k1gFnSc1	pižmovka
velká	velká	k1gFnSc1	velká
(	(	kIx(	(
<g/>
C.	C.	kA	C.
moschata	moschata	k1gFnSc1	moschata
Linné	Linná	k1gFnSc2	Linná
<g/>
,	,	kIx,	,
1758	[number]	k4	1758
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dosud	dosud	k6eAd1	dosud
divoce	divoce	k6eAd1	divoce
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
od	od	k7c2	od
Mexika	Mexiko	k1gNnSc2	Mexiko
až	až	k9	až
po	po	k7c4	po
Paraguay	Paraguay	k1gFnSc4	Paraguay
<g/>
.	.	kIx.	.
</s>
<s>
Peří	peří	k1gNnSc1	peří
má	mít	k5eAaImIp3nS	mít
černé	černý	k2eAgNnSc4d1	černé
a	a	k8xC	a
typickým	typický	k2eAgInSc7d1	typický
znakem	znak	k1gInSc7	znak
je	být	k5eAaImIp3nS	být
bradavičnatost	bradavičnatost	k1gFnSc1	bradavičnatost
u	u	k7c2	u
kořene	kořen	k1gInSc2	kořen
horní	horní	k2eAgFnSc2d1	horní
čelisti	čelist	k1gFnSc2	čelist
zobáku	zobák	k1gInSc2	zobák
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nP	živit
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
zelenou	zelený	k2eAgFnSc7d1	zelená
potravou	potrava	k1gFnSc7	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
menší	malý	k2eAgInPc4d2	menší
nároky	nárok	k1gInPc4	nárok
na	na	k7c4	na
vodní	vodní	k2eAgFnPc4d1	vodní
nádrže	nádrž	k1gFnPc4	nádrž
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnPc1	její
peří	peřit	k5eAaImIp3nP	peřit
je	on	k3xPp3gInPc4	on
méně	málo	k6eAd2	málo
mastné	mastný	k2eAgFnPc1d1	mastná
než	než	k8xS	než
u	u	k7c2	u
kachen	kachna	k1gFnPc2	kachna
<g/>
;	;	kIx,	;
množstvím	množství	k1gNnSc7	množství
prachu	prach	k1gInSc2	prach
se	se	k3xPyFc4	se
vyrovná	vyrovnat	k5eAaBmIp3nS	vyrovnat
peří	peří	k1gNnSc1	peří
hus	husa	k1gFnPc2	husa
a	a	k8xC	a
nemá	mít	k5eNaImIp3nS	mít
také	také	k9	také
kachní	kachní	k2eAgInSc1d1	kachní
pach	pach	k1gInSc1	pach
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vejcích	vejce	k1gNnPc6	vejce
sedí	sedit	k5eAaImIp3nP	sedit
35	[number]	k4	35
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
vož	vož	k?	vož
je	být	k5eAaImIp3nS	být
nejdéle	dlouho	k6eAd3	dlouho
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
druhů	druh	k1gInPc2	druh
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pižmovkou	pižmovka	k1gFnSc7	pižmovka
domácí	domácí	k2eAgFnSc2d1	domácí
se	se	k3xPyFc4	se
setkali	setkat	k5eAaPmAgMnP	setkat
Evropané	Evropan	k1gMnPc1	Evropan
jako	jako	k9	jako
s	s	k7c7	s
ptákem	pták	k1gMnSc7	pták
zdomácnělým	zdomácnělý	k2eAgMnSc7d1	zdomácnělý
již	již	k9	již
Indiány	Indián	k1gMnPc7	Indián
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
byla	být	k5eAaImAgFnS	být
převezena	převézt	k5eAaPmNgFnS	převézt
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
její	její	k3xOp3gFnSc2	její
domestikace	domestikace	k1gFnSc2	domestikace
však	však	k9	však
není	být	k5eNaImIp3nS	být
ukončen	ukončen	k2eAgMnSc1d1	ukončen
(	(	kIx(	(
<g/>
létání	létání	k1gNnSc1	létání
<g/>
,	,	kIx,	,
zanášení	zanášení	k1gNnSc1	zanášení
vajec	vejce	k1gNnPc2	vejce
<g/>
,	,	kIx,	,
původní	původní	k2eAgNnSc4d1	původní
zbarvení	zbarvení	k1gNnSc4	zbarvení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	se	k3xPyFc4	se
také	také	k9	také
antagonismem	antagonismus	k1gInSc7	antagonismus
k	k	k7c3	k
ostatní	ostatní	k2eAgFnSc3d1	ostatní
drůbeži	drůbež	k1gFnSc3	drůbež
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
nezpůsobilá	způsobilý	k2eNgFnSc1d1	nezpůsobilá
do	do	k7c2	do
velkochovů	velkochov	k1gInPc2	velkochov
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
má	mít	k5eAaImIp3nS	mít
vynikající	vynikající	k2eAgNnSc1d1	vynikající
maso	maso	k1gNnSc1	maso
<g/>
,	,	kIx,	,
dobré	dobrý	k2eAgNnSc1d1	dobré
peří	peří	k1gNnSc1	peří
<g/>
,	,	kIx,	,
rychle	rychle	k6eAd1	rychle
roste	růst	k5eAaImIp3nS	růst
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
náročná	náročný	k2eAgFnSc1d1	náročná
na	na	k7c4	na
potravu	potrava	k1gFnSc4	potrava
ani	ani	k8xC	ani
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Páří	pářit	k5eAaImIp3nS	pářit
se	se	k3xPyFc4	se
s	s	k7c7	s
kachnou	kachna	k1gFnSc7	kachna
domácí	domácí	k2eAgFnSc7d1	domácí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
potomci	potomek	k1gMnPc1	potomek
jsou	být	k5eAaImIp3nP	být
neplodní	plodní	k2eNgMnPc1d1	neplodní
<g/>
.	.	kIx.	.
</s>
<s>
Husy	husa	k1gFnPc1	husa
mají	mít	k5eAaImIp3nP	mít
poměrně	poměrně	k6eAd1	poměrně
vysoký	vysoký	k2eAgInSc4d1	vysoký
zobák	zobák	k1gInSc4	zobák
ukončený	ukončený	k2eAgInSc4d1	ukončený
nehtem	nehet	k1gInSc7	nehet
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
kryje	krýt	k5eAaImIp3nS	krýt
celou	celý	k2eAgFnSc4d1	celá
plochu	plocha	k1gFnSc4	plocha
konce	konec	k1gInSc2	konec
zobáku	zobák	k1gInSc2	zobák
<g/>
.	.	kIx.	.
</s>
<s>
Běháky	běhák	k1gInPc1	běhák
jsou	být	k5eAaImIp3nP	být
pokryty	pokrýt	k5eAaPmNgInP	pokrýt
štítky	štítek	k1gInPc1	štítek
tvaru	tvar	k1gInSc2	tvar
šestiúhelníků	šestiúhelník	k1gMnPc2	šestiúhelník
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
vysoké	vysoký	k2eAgFnPc1d1	vysoká
a	a	k8xC	a
silné	silný	k2eAgFnPc1d1	silná
<g/>
.	.	kIx.	.
</s>
<s>
Husy	Hus	k1gMnPc4	Hus
jsou	být	k5eAaImIp3nP	být
býložravci	býložravec	k1gMnPc1	býložravec
<g/>
;	;	kIx,	;
vedou	vést	k5eAaImIp3nP	vést
denní	denní	k2eAgInSc4d1	denní
způsob	způsob	k1gInSc4	způsob
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
domestikace	domestikace	k1gFnSc2	domestikace
není	být	k5eNaImIp3nS	být
přesně	přesně	k6eAd1	přesně
znám	znám	k2eAgMnSc1d1	znám
<g/>
.	.	kIx.	.
</s>
<s>
Usuzuje	usuzovat	k5eAaImIp3nS	usuzovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
husa	husa	k1gFnSc1	husa
domácí	domácí	k2eAgFnSc1d1	domácí
odchovala	odchovat	k5eAaPmAgFnS	odchovat
z	z	k7c2	z
husy	husa	k1gFnSc2	husa
velké	velký	k2eAgFnSc2d1	velká
<g/>
,	,	kIx,	,
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
z	z	k7c2	z
divoké	divoký	k2eAgFnSc2d1	divoká
husy	husa	k1gFnSc2	husa
nilské	nilský	k2eAgFnSc2d1	nilská
a	a	k8xC	a
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
a	a	k8xC	a
Japonsku	Japonsko	k1gNnSc6	Japonsko
z	z	k7c2	z
divoké	divoký	k2eAgFnSc2d1	divoká
husy	husa	k1gFnSc2	husa
labutí	labuť	k1gFnPc2	labuť
<g/>
.	.	kIx.	.
</s>
<s>
Husa	Husa	k1gMnSc1	Husa
domácí	domácí	k1gMnSc1	domácí
-	-	kIx~	-
Anser	Anser	k1gMnSc1	Anser
anser	anser	k1gMnSc1	anser
f.	f.	k?	f.
domestica	domestica	k1gFnSc1	domestica
(	(	kIx(	(
<g/>
Linné	Linné	k1gNnSc1	Linné
<g/>
,	,	kIx,	,
1758	[number]	k4	1758
<g/>
)	)	kIx)	)
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
holubu	holub	k1gMnSc6	holub
domácím	domácí	k1gMnSc6	domácí
nejstarší	starý	k2eAgMnSc1d3	nejstarší
domestikovaný	domestikovaný	k2eAgMnSc1d1	domestikovaný
pták	pták	k1gMnSc1	pták
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
kmenovou	kmenový	k2eAgFnSc7d1	kmenová
formou	forma	k1gFnSc7	forma
je	být	k5eAaImIp3nS	být
husa	husa	k1gFnSc1	husa
velká	velká	k1gFnSc1	velká
(	(	kIx(	(
<g/>
Anser	Anser	k1gInSc1	Anser
anser	ansra	k1gFnPc2	ansra
Linné	Linná	k1gFnSc2	Linná
<g/>
,	,	kIx,	,
1758	[number]	k4	1758
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vyobrazena	vyobrazit	k5eAaPmNgFnS	vyobrazit
na	na	k7c6	na
egyptských	egyptský	k2eAgFnPc6d1	egyptská
malbách	malba	k1gFnPc6	malba
již	již	k6eAd1	již
z	z	k7c2	z
3	[number]	k4	3
<g/>
.	.	kIx.	.
tisícíletí	tisícíletí	k1gNnSc2	tisícíletí
př	př	kA	př
<g/>
.	.	kIx.	.
K.	K.	kA	K.
Střediskem	středisko	k1gNnSc7	středisko
domestikace	domestikace	k1gFnSc2	domestikace
husy	husa	k1gFnSc2	husa
domácí	domácí	k2eAgFnSc1d1	domácí
byla	být	k5eAaImAgFnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
jihovýchodní	jihovýchodní	k2eAgFnSc1d1	jihovýchodní
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
oblast	oblast	k1gFnSc1	oblast
staré	starý	k2eAgFnSc2d1	stará
řecké	řecký	k2eAgFnSc2d1	řecká
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Přední	přední	k2eAgFnSc1d1	přední
Asie	Asie	k1gFnSc1	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Husa	Husa	k1gMnSc1	Husa
jako	jako	k8xS	jako
domácí	domácí	k2eAgMnSc1d1	domácí
pták	pták	k1gMnSc1	pták
byla	být	k5eAaImAgFnS	být
známa	znám	k2eAgNnPc1d1	známo
Řekům	Řek	k1gMnPc3	Řek
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Homérově	Homérův	k2eAgFnSc6d1	Homérova
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
v	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
K.	K.	kA	K.
Všechny	všechen	k3xTgInPc4	všechen
indoevropské	indoevropský	k2eAgInPc4d1	indoevropský
národy	národ	k1gInPc4	národ
při	při	k7c6	při
osídlování	osídlování	k1gNnSc4	osídlování
Evropy	Evropa	k1gFnSc2	Evropa
ji	on	k3xPp3gFnSc4	on
již	již	k6eAd1	již
znaly	znát	k5eAaImAgFnP	znát
<g/>
.	.	kIx.	.
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1	rozšíření
husy	husa	k1gFnSc2	husa
domácí	domácí	k2eAgInSc4d1	domácí
do	do	k7c2	do
tehdy	tehdy	k6eAd1	tehdy
známého	známý	k2eAgInSc2d1	známý
světa	svět	k1gInSc2	svět
je	být	k5eAaImIp3nS	být
dílem	dílem	k6eAd1	dílem
především	především	k6eAd1	především
Římanů	Říman	k1gMnPc2	Říman
<g/>
.	.	kIx.	.
podněty	podnět	k1gInPc1	podnět
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
domestikaci	domestikace	k1gFnSc3	domestikace
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
přesně	přesně	k6eAd1	přesně
známy	znám	k2eAgFnPc1d1	známa
<g/>
.	.	kIx.	.
</s>
<s>
Určitý	určitý	k2eAgInSc4d1	určitý
význam	význam	k1gInSc4	význam
měly	mít	k5eAaImAgFnP	mít
nepochybně	pochybně	k6eNd1	pochybně
náboženské	náboženský	k2eAgFnPc1d1	náboženská
otázky	otázka	k1gFnPc1	otázka
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
hospodářské	hospodářský	k2eAgNnSc1d1	hospodářské
hledisko	hledisko	k1gNnSc1	hledisko
se	se	k3xPyFc4	se
asi	asi	k9	asi
uplatnilo	uplatnit	k5eAaPmAgNnS	uplatnit
teprve	teprve	k6eAd1	teprve
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
její	její	k3xOp3gFnSc3	její
domestikaci	domestikace	k1gFnSc3	domestikace
přispěly	přispět	k5eAaPmAgFnP	přispět
skutečnosti	skutečnost	k1gFnPc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
housata	house	k1gNnPc1	house
husy	husa	k1gFnSc2	husa
velké	velká	k1gFnSc2	velká
snadno	snadno	k6eAd1	snadno
přilnou	přilnout	k5eAaPmIp3nP	přilnout
k	k	k7c3	k
člověku	člověk	k1gMnSc3	člověk
<g/>
,	,	kIx,	,
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
přijímají	přijímat	k5eAaImIp3nP	přijímat
ochotně	ochotně	k6eAd1	ochotně
potravu	potrava	k1gFnSc4	potrava
a	a	k8xC	a
již	již	k6eAd1	již
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
pokolení	pokolení	k1gNnSc6	pokolení
ztrácejí	ztrácet	k5eAaImIp3nP	ztrácet
divokost	divokost	k1gFnSc4	divokost
<g/>
.	.	kIx.	.
</s>
<s>
Vedou	vést	k5eAaImIp3nP	vést
denní	denní	k2eAgInSc4d1	denní
způsob	způsob	k1gInSc4	způsob
života	život	k1gInSc2	život
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
pasou	pást	k5eAaImIp3nP	pást
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
vysoce	vysoce	k6eAd1	vysoce
vyvinutý	vyvinutý	k2eAgInSc4d1	vyvinutý
pud	pud	k1gInSc4	pud
pospolitosti	pospolitost	k1gFnSc2	pospolitost
jak	jak	k8xS	jak
mezi	mezi	k7c7	mezi
příslušníky	příslušník	k1gMnPc7	příslušník
svého	své	k1gNnSc2	své
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
s	s	k7c7	s
jinými	jiný	k2eAgNnPc7d1	jiné
zvířaty	zvíře	k1gNnPc7	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Domestikací	domestikace	k1gFnSc7	domestikace
se	se	k3xPyFc4	se
pozměnily	pozměnit	k5eAaPmAgFnP	pozměnit
některé	některý	k3yIgFnPc1	některý
původní	původní	k2eAgFnPc1d1	původní
vlastnosti	vlastnost	k1gFnPc1	vlastnost
divokého	divoký	k2eAgMnSc2d1	divoký
předka	předek	k1gMnSc2	předek
-	-	kIx~	-
z	z	k7c2	z
monogamního	monogamní	k2eAgMnSc2d1	monogamní
ptáka	pták	k1gMnSc2	pták
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
pták	pták	k1gMnSc1	pták
polygamní	polygamní	k2eAgMnSc1d1	polygamní
<g/>
,	,	kIx,	,
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
se	se	k3xPyFc4	se
snáška	snáška	k1gFnSc1	snáška
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
4-7	[number]	k4	4-7
vajec	vejce	k1gNnPc2	vejce
ročně	ročně	k6eAd1	ročně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
vejcích	vejce	k1gNnPc6	vejce
sedí	sedit	k5eAaImIp3nS	sedit
jen	jen	k9	jen
husa	husa	k1gFnSc1	husa
<g/>
,	,	kIx,	,
stěhovací	stěhovací	k2eAgInSc1d1	stěhovací
instinkt	instinkt	k1gInSc1	instinkt
byl	být	k5eAaImAgInS	být
silně	silně	k6eAd1	silně
potlačen	potlačit	k5eAaPmNgInS	potlačit
a	a	k8xC	a
uspíšilo	uspíšit	k5eAaPmAgNnS	uspíšit
se	se	k3xPyFc4	se
pohlavní	pohlavní	k2eAgNnSc1d1	pohlavní
dospívání	dospívání	k1gNnSc1	dospívání
<g/>
.	.	kIx.	.
</s>
<s>
Husa	husa	k1gFnSc1	husa
čínská	čínský	k2eAgFnSc1d1	čínská
-	-	kIx~	-
Anser	Anser	k1gMnSc1	Anser
cygnoides	cygnoides	k1gMnSc1	cygnoides
f.	f.	k?	f.
domestica	domestica	k1gMnSc1	domestica
(	(	kIx(	(
<g/>
Pallas	Pallas	k1gMnSc1	Pallas
<g/>
,	,	kIx,	,
1776	[number]	k4	1776
<g/>
)	)	kIx)	)
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
domestikovanou	domestikovaný	k2eAgFnSc7d1	domestikovaná
formou	forma	k1gFnSc7	forma
husy	husa	k1gFnSc2	husa
labutí	labutí	k2eAgFnSc2d1	labutí
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
žije	žít	k5eAaImIp3nS	žít
divoce	divoce	k6eAd1	divoce
v	v	k7c6	v
jižnější	jižní	k2eAgFnSc6d2	jižnější
části	část	k1gFnSc6	část
Sibiře	Sibiř	k1gFnSc2	Sibiř
<g/>
,	,	kIx,	,
Mandžuska	Mandžusko	k1gNnSc2	Mandžusko
a	a	k8xC	a
přilehlých	přilehlý	k2eAgFnPc2d1	přilehlá
krajin	krajina	k1gFnPc2	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
vzhledem	vzhled	k1gInSc7	vzhled
připomíná	připomínat	k5eAaImIp3nS	připomínat
labuť	labuť	k1gFnSc1	labuť
<g/>
.	.	kIx.	.
</s>
<s>
Domestikačním	domestikační	k2eAgNnSc7d1	domestikační
střediskem	středisko	k1gNnSc7	středisko
husy	husa	k1gFnSc2	husa
čínské	čínský	k2eAgFnSc2d1	čínská
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
chována	chovat	k5eAaImNgFnS	chovat
již	již	k6eAd1	již
před	před	k7c7	před
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
3000	[number]	k4	3000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
Indie	Indie	k1gFnSc2	Indie
a	a	k8xC	a
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Husa	husa	k1gFnSc1	husa
nilská	nilský	k2eAgFnSc1d1	nilská
-	-	kIx~	-
Alopochen	Alopochen	k2eAgInSc4d1	Alopochen
aegyptiacus	aegyptiacus	k1gInSc4	aegyptiacus
byla	být	k5eAaImAgFnS	být
zdomácněna	zdomácnit	k5eAaPmNgFnS	zdomácnit
Egypťany	Egypťan	k1gMnPc7	Egypťan
již	již	k9	již
asi	asi	k9	asi
2500	[number]	k4	2500
let	léto	k1gNnPc2	léto
př	př	kA	př
<g/>
.	.	kIx.	.
<g/>
K.	K.	kA	K.
S	s	k7c7	s
úpadkem	úpadek	k1gInSc7	úpadek
egyptské	egyptský	k2eAgFnSc2d1	egyptská
kultury	kultura	k1gFnSc2	kultura
pozbyla	pozbýt	k5eAaPmAgFnS	pozbýt
významu	význam	k1gInSc3	význam
<g/>
.	.	kIx.	.
</s>
<s>
VESELOVSKÝ	Veselovský	k1gMnSc1	Veselovský
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Obecná	obecný	k2eAgFnSc1d1	obecná
ornitologie	ornitologie	k1gFnSc1	ornitologie
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
356	[number]	k4	356
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
857	[number]	k4	857
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
HAVLÍN	Havlín	k1gMnSc1	Havlín
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
.	.	kIx.	.
</s>
<s>
Domácí	domácí	k2eAgInSc1d1	domácí
chov	chov	k1gInSc1	chov
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
SZN	SZN	kA	SZN
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
408	[number]	k4	408
s.	s.	k?	s.
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
domácí	domácí	k2eAgFnSc1d1	domácí
drůbež	drůbež	k1gFnSc1	drůbež
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
drůbež	drůbež	k1gFnSc1	drůbež
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
