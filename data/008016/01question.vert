<s>
Jak	jak	k6eAd1	jak
obecně	obecně	k6eAd1	obecně
označujeme	označovat	k5eAaImIp1nP	označovat
skupinu	skupina	k1gFnSc4	skupina
zvířat	zvíře	k1gNnPc2	zvíře
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
kur	kur	k1gMnSc1	kur
domácí	domácí	k2eAgMnSc1d1	domácí
<g/>
,	,	kIx,	,
krůta	krůta	k1gFnSc1	krůta
<g/>
,	,	kIx,	,
kachna	kachna	k1gFnSc1	kachna
a	a	k8xC	a
husu	husa	k1gFnSc4	husa
<g/>
?	?	kIx.	?
</s>
