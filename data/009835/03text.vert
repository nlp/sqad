<p>
<s>
Luke	Luke	k1gFnSc1	Luke
Skywalker	Skywalkra	k1gFnPc2	Skywalkra
je	být	k5eAaImIp3nS	být
fiktivní	fiktivní	k2eAgFnSc1d1	fiktivní
postava	postava	k1gFnSc1	postava
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
filmové	filmový	k2eAgFnSc2d1	filmová
trilogie	trilogie	k1gFnSc2	trilogie
Star	star	k1gFnPc2	star
Wars	Warsa	k1gFnPc2	Warsa
ztvárněná	ztvárněný	k2eAgFnSc1d1	ztvárněná
americkým	americký	k2eAgMnSc7d1	americký
hercem	herec	k1gMnSc7	herec
Markem	Marek	k1gMnSc7	Marek
Hamillem	Hamill	k1gMnSc7	Hamill
<g/>
.	.	kIx.	.
</s>
<s>
Luke	Luke	k1gFnSc1	Luke
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
dvojčat	dvojče	k1gNnPc2	dvojče
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
ze	z	k7c2	z
svazku	svazek	k1gInSc2	svazek
Anakina	Anakin	k2eAgNnSc2d1	Anakin
Skywalkera	Skywalkero	k1gNnSc2	Skywalkero
a	a	k8xC	a
vládkyně	vládkyně	k1gFnSc2	vládkyně
Naboo	Naboo	k6eAd1	Naboo
Padmé	Padmý	k2eAgMnPc4d1	Padmý
Amidaly	Amidal	k1gMnPc4	Amidal
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgFnPc1d2	pozdější
senátorky	senátorka	k1gFnPc1	senátorka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
legendární	legendární	k2eAgMnSc1d1	legendární
voják	voják	k1gMnSc1	voják
a	a	k8xC	a
mistr	mistr	k1gMnSc1	mistr
Jedi	Jed	k1gFnSc2	Jed
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
pomohl	pomoct	k5eAaPmAgMnS	pomoct
porazit	porazit	k5eAaPmF	porazit
Galaktické	galaktický	k2eAgNnSc4d1	Galaktické
Impérium	impérium	k1gNnSc4	impérium
a	a	k8xC	a
založit	založit	k5eAaPmF	založit
Novou	nový	k2eAgFnSc4d1	nová
Republiku	republika	k1gFnSc4	republika
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
Nový	nový	k2eAgInSc1d1	nový
Řád	řád	k1gInSc1	řád
Jedi	Jed	k1gFnSc2	Jed
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
povedlo	povést	k5eAaPmAgNnS	povést
probudit	probudit	k5eAaPmF	probudit
v	v	k7c4	v
Darth	Darth	k1gInSc4	Darth
Vaderovi	Vaderův	k2eAgMnPc1d1	Vaderův
jeho	jeho	k3xOp3gNnSc4	jeho
dávno	dávno	k6eAd1	dávno
ztracenou	ztracený	k2eAgFnSc4d1	ztracená
světlou	světlý	k2eAgFnSc4d1	světlá
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Epizoda	epizoda	k1gFnSc1	epizoda
3	[number]	k4	3
-	-	kIx~	-
Pomsta	pomsta	k1gFnSc1	pomsta
Sithů	Sith	k1gInPc2	Sith
===	===	k?	===
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
19	[number]	k4	19
BBY	BBY	kA	BBY
jako	jako	k8xS	jako
syn	syn	k1gMnSc1	syn
padlého	padlý	k2eAgMnSc2d1	padlý
rytíře	rytíř	k1gMnSc2	rytíř
Jedi	Jed	k1gFnSc2	Jed
Anakina	Anakin	k1gMnSc2	Anakin
Skywalkera	Skywalker	k1gMnSc2	Skywalker
a	a	k8xC	a
bývalé	bývalý	k2eAgFnSc2d1	bývalá
královny	královna	k1gFnSc2	královna
Naboo	Naboo	k6eAd1	Naboo
Padmé	Padmý	k2eAgMnPc4d1	Padmý
Amidaly	Amidal	k1gMnPc4	Amidal
<g/>
.	.	kIx.	.
</s>
<s>
Vyrostl	vyrůst	k5eAaPmAgMnS	vyrůst
v	v	k7c6	v
izolaci	izolace	k1gFnSc6	izolace
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
Tatooine	Tatooin	k1gInSc5	Tatooin
u	u	k7c2	u
jedné	jeden	k4xCgFnSc2	jeden
místní	místní	k2eAgFnSc2d1	místní
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
skrytý	skrytý	k2eAgMnSc1d1	skrytý
před	před	k7c7	před
Císařem	Císař	k1gMnSc7	Císař
Palpatinem	Palpatin	k1gMnSc7	Palpatin
a	a	k8xC	a
vlastním	vlastní	k2eAgMnSc7d1	vlastní
otcem	otec	k1gMnSc7	otec
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
známým	známý	k2eAgMnSc7d1	známý
jako	jako	k8xS	jako
Darth	Darth	k1gMnSc1	Darth
Vader	Vader	k1gMnSc1	Vader
<g/>
,	,	kIx,	,
Temný	temný	k2eAgMnSc1d1	temný
Lord	lord	k1gMnSc1	lord
ze	z	k7c2	z
Sithu	Sith	k1gInSc2	Sith
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Epizoda	epizoda	k1gFnSc1	epizoda
4	[number]	k4	4
-	-	kIx~	-
Nová	nový	k2eAgFnSc1d1	nová
naděje	naděje	k1gFnSc1	naděje
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
0	[number]	k4	0
BBY	BBY	kA	BBY
se	se	k3xPyFc4	se
však	však	k9	však
jeho	jeho	k3xOp3gInSc1	jeho
život	život	k1gInSc1	život
změnil	změnit	k5eAaPmAgInS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Koupě	koupě	k1gFnSc1	koupě
dvou	dva	k4xCgMnPc2	dva
droidů	droid	k1gMnPc2	droid
<g/>
,	,	kIx,	,
R2-D2	R2-D2	k1gFnSc7	R2-D2
a	a	k8xC	a
C-	C-	k1gFnSc7	C-
<g/>
3	[number]	k4	3
<g/>
PO	Po	kA	Po
<g/>
,	,	kIx,	,
ho	on	k3xPp3gMnSc4	on
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
setkání	setkání	k1gNnSc3	setkání
s	s	k7c7	s
Hanem	Han	k1gMnSc7	Han
Solem	sol	k1gInSc7	sol
<g/>
,	,	kIx,	,
princeznou	princezna	k1gFnSc7	princezna
Leiou	Leia	k1gFnSc7	Leia
Organou	Organa	k1gFnSc7	Organa
a	a	k8xC	a
Obi-Wanem	Obi-Wan	k1gInSc7	Obi-Wan
(	(	kIx(	(
<g/>
Benem	Ben	k1gInSc7	Ben
<g/>
)	)	kIx)	)
Kenobim	Kenobim	k1gInSc1	Kenobim
<g/>
.	.	kIx.	.
</s>
<s>
Skywalker	Skywalker	k1gMnSc1	Skywalker
později	pozdě	k6eAd2	pozdě
jako	jako	k9	jako
člen	člen	k1gMnSc1	člen
eskadry	eskadra	k1gFnSc2	eskadra
Rogue	Rogue	k1gInSc1	Rogue
zničil	zničit	k5eAaPmAgInS	zničit
první	první	k4xOgFnSc4	první
Hvězdu	hvězda	k1gFnSc4	hvězda
smrti	smrt	k1gFnSc2	smrt
a	a	k8xC	a
připojil	připojit	k5eAaPmAgMnS	připojit
se	se	k3xPyFc4	se
k	k	k7c3	k
povstalecké	povstalecký	k2eAgFnSc3d1	povstalecká
Alianci	aliance	k1gFnSc3	aliance
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Epizoda	epizoda	k1gFnSc1	epizoda
5	[number]	k4	5
-	-	kIx~	-
Impérium	impérium	k1gNnSc1	impérium
vrací	vracet	k5eAaImIp3nS	vracet
úder	úder	k1gInSc4	úder
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
příštích	příští	k2eAgNnPc2d1	příští
pár	pár	k4xCyI	pár
let	léto	k1gNnPc2	léto
bojoval	bojovat	k5eAaImAgMnS	bojovat
Luke	Luke	k1gInSc4	Luke
Skywalker	Skywalkra	k1gFnPc2	Skywalkra
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
důležitých	důležitý	k2eAgFnPc2d1	důležitá
bitev	bitva	k1gFnPc2	bitva
v	v	k7c6	v
Galaktické	galaktický	k2eAgFnSc6d1	Galaktická
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
3	[number]	k4	3
ABY	aby	kYmCp3nS	aby
napadlo	napadnout	k5eAaPmAgNnS	napadnout
Impérium	impérium	k1gNnSc1	impérium
povstaleckou	povstalecký	k2eAgFnSc4d1	povstalecká
základnu	základna	k1gFnSc4	základna
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
Hoth	Hotha	k1gFnPc2	Hotha
a	a	k8xC	a
Luke	Luke	k1gFnPc2	Luke
odlétá	odlétat	k5eAaPmIp3nS	odlétat
na	na	k7c4	na
Dagobah	Dagobah	k1gInSc4	Dagobah
za	za	k7c7	za
mistrem	mistr	k1gMnSc7	mistr
Yodou	Yoda	k1gMnSc7	Yoda
s	s	k7c7	s
přáním	přání	k1gNnSc7	přání
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gInSc4	on
vycvičil	vycvičit	k5eAaPmAgInS	vycvičit
<g/>
.	.	kIx.	.
</s>
<s>
Luke	Luke	k6eAd1	Luke
se	se	k3xPyFc4	se
vydá	vydat	k5eAaPmIp3nS	vydat
bojovat	bojovat	k5eAaImF	bojovat
s	s	k7c7	s
Darth	Darth	k1gMnSc1	Darth
Vaderem	Vader	k1gMnSc7	Vader
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
mu	on	k3xPp3gMnSc3	on
usekne	useknout	k5eAaPmIp3nS	useknout
ruku	ruka	k1gFnSc4	ruka
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
mu	on	k3xPp3gMnSc3	on
prozradí	prozradit	k5eAaPmIp3nP	prozradit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
.	.	kIx.	.
</s>
<s>
Luke	Luke	k1gInSc1	Luke
kvůli	kvůli	k7c3	kvůli
useknuté	useknutý	k2eAgFnSc3d1	useknutá
ruce	ruka	k1gFnSc3	ruka
ztratí	ztratit	k5eAaPmIp3nS	ztratit
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
světelný	světelný	k2eAgInSc4d1	světelný
meč	meč	k1gInSc4	meč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Epizoda	epizoda	k1gFnSc1	epizoda
6	[number]	k4	6
-	-	kIx~	-
Návrat	návrat	k1gInSc1	návrat
Jediho	Jedi	k1gMnSc2	Jedi
===	===	k?	===
</s>
</p>
<p>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
bojuje	bojovat	k5eAaImIp3nS	bojovat
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c4	o
Endor	Endor	k1gInSc4	Endor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nechá	nechat	k5eAaPmIp3nS	nechat
zajmout	zajmout	k5eAaPmF	zajmout
imperiálními	imperiální	k2eAgMnPc7d1	imperiální
vojáky	voják	k1gMnPc7	voják
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohl	moct	k5eAaImAgInS	moct
bojovat	bojovat	k5eAaImF	bojovat
s	s	k7c7	s
Darth	Darth	k1gInSc1	Darth
Vaderem	Vader	k1gInSc7	Vader
<g/>
.	.	kIx.	.
</s>
<s>
Svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
porazí	porazit	k5eAaPmIp3nS	porazit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nechce	chtít	k5eNaImIp3nS	chtít
se	se	k3xPyFc4	se
přidat	přidat	k5eAaPmF	přidat
k	k	k7c3	k
temné	temný	k2eAgFnSc3d1	temná
straně	strana	k1gFnSc3	strana
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
jej	on	k3xPp3gMnSc4	on
císař	císař	k1gMnSc1	císař
Palpatine	Palpatin	k1gInSc5	Palpatin
(	(	kIx(	(
<g/>
Darth	Darth	k1gMnSc1	Darth
Sidious	Sidious	k1gMnSc1	Sidious
<g/>
)	)	kIx)	)
chce	chtít	k5eAaImIp3nS	chtít
zabít	zabít	k5eAaPmF	zabít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Darth	Darth	k1gMnSc1	Darth
Vader	Vader	k1gMnSc1	Vader
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
Lukem	luk	k1gInSc7	luk
<g/>
,	,	kIx,	,
svým	svůj	k3xOyFgMnSc7	svůj
synem	syn	k1gMnSc7	syn
<g/>
,	,	kIx,	,
slituje	slitovat	k5eAaPmIp3nS	slitovat
a	a	k8xC	a
hodí	hodit	k5eAaPmIp3nS	hodit
Palpatina	Palpatina	k1gFnSc1	Palpatina
do	do	k7c2	do
generátoru	generátor	k1gInSc2	generátor
Hvězdy	hvězda	k1gFnSc2	hvězda
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Luke	Luke	k6eAd1	Luke
tedy	tedy	k9	tedy
uspěl	uspět	k5eAaPmAgMnS	uspět
v	v	k7c4	v
obrácení	obrácení	k1gNnSc4	obrácení
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
světlou	světlý	k2eAgFnSc4d1	světlá
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Darth	Darth	k1gMnSc1	Darth
Vader	Vader	k1gMnSc1	Vader
později	pozdě	k6eAd2	pozdě
zemře	zemřít	k5eAaPmIp3nS	zemřít
na	na	k7c4	na
poškození	poškození	k1gNnSc4	poškození
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
mu	on	k3xPp3gMnSc3	on
způsobil	způsobit	k5eAaPmAgInS	způsobit
Palpatine	Palpatin	k1gInSc5	Palpatin
svými	svůj	k3xOyFgInPc7	svůj
blesky	blesk	k1gInPc7	blesk
<g/>
.	.	kIx.	.
</s>
<s>
Luke	Luke	k1gFnSc1	Luke
Skywalker	Skywalkra	k1gFnPc2	Skywalkra
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
chvíli	chvíle	k1gFnSc6	chvíle
odletí	odletět	k5eAaPmIp3nP	odletět
z	z	k7c2	z
vybuchující	vybuchující	k2eAgFnSc2d1	vybuchující
Hvězdy	hvězda	k1gFnSc2	hvězda
smrti	smrt	k1gFnSc2	smrt
i	i	k9	i
s	s	k7c7	s
mrtvým	mrtvý	k2eAgNnSc7d1	mrtvé
tělem	tělo	k1gNnSc7	tělo
Darth	Dartha	k1gFnPc2	Dartha
Vadera	Vadero	k1gNnSc2	Vadero
na	na	k7c4	na
měsíc	měsíc	k1gInSc4	měsíc
Endor	Endora	k1gFnPc2	Endora
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
spálí	spálit	k5eAaPmIp3nS	spálit
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
Lukovi	Luka	k1gMnSc3	Luka
Skywalkerovi	Skywalker	k1gMnSc3	Skywalker
byl	být	k5eAaImAgMnS	být
Palpatine	Palpatin	k1gInSc5	Palpatin
alias	alias	k9	alias
Darth	Dartha	k1gFnPc2	Dartha
Sidious	Sidious	k1gMnSc1	Sidious
zabit	zabít	k5eAaPmNgMnS	zabít
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
Impériu	impérium	k1gNnSc6	impérium
zasadilo	zasadit	k5eAaPmAgNnS	zasadit
zničující	zničující	k2eAgInSc4d1	zničující
úder	úder	k1gInSc4	úder
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Epizoda	epizoda	k1gFnSc1	epizoda
7	[number]	k4	7
-	-	kIx~	-
Síla	síla	k1gFnSc1	síla
se	se	k3xPyFc4	se
probouzí	probouzet	k5eAaImIp3nS	probouzet
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Impéria	impérium	k1gNnSc2	impérium
a	a	k8xC	a
obnovení	obnovení	k1gNnSc2	obnovení
republiky	republika	k1gFnSc2	republika
založí	založit	k5eAaPmIp3nP	založit
Luke	Luke	k1gNnSc7	Luke
Skywalker	Skywalker	k1gInSc1	Skywalker
chrám	chrám	k1gInSc1	chrám
Jediů	Jedi	k1gMnPc2	Jedi
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
členů	člen	k1gInPc2	člen
Ben	Ben	k1gInSc4	Ben
Solo	Solo	k1gMnSc1	Solo
(	(	kIx(	(
<g/>
syn	syn	k1gMnSc1	syn
Lei	lei	k1gInPc2	lei
a	a	k8xC	a
Hana	Hana	k1gFnSc1	Hana
<g/>
)	)	kIx)	)
však	však	k9	však
Luka	luka	k1gNnPc1	luka
zradí	zradit	k5eAaPmIp3nP	zradit
a	a	k8xC	a
přidá	přidat	k5eAaPmIp3nS	přidat
se	se	k3xPyFc4	se
k	k	k7c3	k
uskupení	uskupení	k1gNnSc3	uskupení
jménem	jméno	k1gNnSc7	jméno
První	první	k4xOgInSc4	první
řád	řád	k1gInSc4	řád
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
z	z	k7c2	z
pozůstatků	pozůstatek	k1gInPc2	pozůstatek
Galaktického	galaktický	k2eAgNnSc2d1	Galaktické
impéria	impérium	k1gNnSc2	impérium
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
vyvraždí	vyvraždit	k5eAaPmIp3nP	vyvraždit
děti	dítě	k1gFnPc1	dítě
z	z	k7c2	z
chrámu	chrám	k1gInSc2	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Zklamaný	zklamaný	k2eAgInSc1d1	zklamaný
Luke	Luke	k1gInSc1	Luke
poté	poté	k6eAd1	poté
zmizí	zmizet	k5eAaPmIp3nS	zmizet
<g/>
.	.	kIx.	.
</s>
<s>
Zanechá	zanechat	k5eAaPmIp3nS	zanechat
pouze	pouze	k6eAd1	pouze
mapu	mapa	k1gFnSc4	mapa
cesty	cesta	k1gFnSc2	cesta
k	k	k7c3	k
místu	místo	k1gNnSc3	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
zbytek	zbytek	k1gInSc4	zbytek
sedmé	sedmý	k4xOgFnSc2	sedmý
epizody	epizoda	k1gFnSc2	epizoda
je	být	k5eAaImIp3nS	být
Skywalker	Skywalker	k1gMnSc1	Skywalker
hledán	hledat	k5eAaImNgMnS	hledat
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
až	až	k9	až
na	na	k7c6	na
konci	konec	k1gInSc6	konec
dílu	díl	k1gInSc2	díl
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jej	on	k3xPp3gMnSc4	on
nalezne	naleznout	k5eAaPmIp3nS	naleznout
Rey	Rea	k1gFnSc2	Rea
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
mu	on	k3xPp3gMnSc3	on
vrátí	vrátit	k5eAaPmIp3nS	vrátit
jeho	jeho	k3xOp3gInSc4	jeho
starý	starý	k2eAgInSc4d1	starý
světelný	světelný	k2eAgInSc4d1	světelný
meč	meč	k1gInSc4	meč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Epizoda	epizoda	k1gFnSc1	epizoda
8	[number]	k4	8
-	-	kIx~	-
Poslední	poslední	k2eAgInPc1d1	poslední
z	z	k7c2	z
Jediů	Jedi	k1gInPc2	Jedi
===	===	k?	===
</s>
</p>
<p>
<s>
Luke	Luke	k6eAd1	Luke
se	se	k3xPyFc4	se
skrývá	skrývat	k5eAaImIp3nS	skrývat
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
v	v	k7c6	v
nejzápadnějším	západní	k2eAgInSc6d3	nejzápadnější
koutě	kout	k1gInSc6	kout
Galaxie	galaxie	k1gFnSc2	galaxie
a	a	k8xC	a
jako	jako	k9	jako
poslední	poslední	k2eAgFnSc4d1	poslední
Jedi	Jede	k1gFnSc4	Jede
je	být	k5eAaImIp3nS	být
rozhodnut	rozhodnut	k2eAgMnSc1d1	rozhodnut
nechat	nechat	k5eAaPmF	nechat
řád	řád	k1gInSc4	řád
zaniknout	zaniknout	k5eAaPmF	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
jej	on	k3xPp3gMnSc4	on
vyhledá	vyhledat	k5eAaPmIp3nS	vyhledat
Rey	Rea	k1gFnPc4	Rea
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Luke	Luke	k1gFnSc1	Luke
ji	on	k3xPp3gFnSc4	on
odmítá	odmítat	k5eAaImIp3nS	odmítat
pomoci	pomoct	k5eAaPmF	pomoct
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
svůj	svůj	k3xOyFgInSc4	svůj
odpor	odpor	k1gInSc4	odpor
k	k	k7c3	k
pokračování	pokračování	k1gNnSc3	pokračování
řádu	řád	k1gInSc2	řád
Jedi	Jed	k1gFnSc2	Jed
cvičí	cvičit	k5eAaImIp3nP	cvičit
Rey	Rea	k1gFnPc1	Rea
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c4	o
poslední	poslední	k2eAgInSc4d1	poslední
úkryt	úkryt	k1gInSc4	úkryt
povstalců	povstalec	k1gMnPc2	povstalec
se	se	k3xPyFc4	se
utká	utkat	k5eAaPmIp3nS	utkat
s	s	k7c7	s
Benem	Ben	k1gInSc7	Ben
na	na	k7c6	na
solné	solný	k2eAgFnSc6d1	solná
pláni	pláň	k1gFnSc6	pláň
<g/>
.	.	kIx.	.
</s>
<s>
Souboj	souboj	k1gInSc1	souboj
na	na	k7c4	na
světelné	světelný	k2eAgInPc4d1	světelný
meče	meč	k1gInPc4	meč
vyhraje	vyhrát	k5eAaPmIp3nS	vyhrát
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
jeho	jeho	k3xOp3gMnSc1	jeho
učitel	učitel	k1gMnSc1	učitel
Obi-Wan	Obi-Wan	k1gMnSc1	Obi-Wan
s	s	k7c7	s
Darth	Darth	k1gInSc1	Darth
Vaderem	Vader	k1gInSc7	Vader
na	na	k7c6	na
Hvězdě	hvězda	k1gFnSc6	hvězda
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
však	však	k9	však
Luke	Luke	k1gFnSc1	Luke
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Luke	Luk	k1gInSc2	Luk
Skywalker	Skywalkra	k1gFnPc2	Skywalkra
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
