<p>
<s>
Les	les	k1gInSc4	les
Řáholec	Řáholec	k1gInSc1	Řáholec
je	být	k5eAaImIp3nS	být
fiktivní	fiktivní	k2eAgInSc1d1	fiktivní
les	les	k1gInSc1	les
z	z	k7c2	z
pohádek	pohádka	k1gFnPc2	pohádka
o	o	k7c6	o
loupežníku	loupežník	k1gMnSc6	loupežník
Rumcajsovi	Rumcajs	k1gMnSc6	Rumcajs
Václava	Václava	k1gFnSc1	Václava
Čtvrtka	čtvrtka	k1gFnSc1	čtvrtka
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Čtvrtek	čtvrtka	k1gFnPc2	čtvrtka
říkal	říkat	k5eAaImAgMnS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Řáholec	Řáholec	k1gInSc1	Řáholec
je	být	k5eAaImIp3nS	být
les	les	k1gInSc1	les
pohádkový	pohádkový	k2eAgInSc1d1	pohádkový
a	a	k8xC	a
leží	ležet	k5eAaImIp3nS	ležet
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gMnSc4	on
chce	chtít	k5eAaImIp3nS	chtít
dotyčný	dotyčný	k2eAgMnSc1d1	dotyčný
čtenář	čtenář	k1gMnSc1	čtenář
mít	mít	k5eAaImF	mít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Inspirací	inspirace	k1gFnSc7	inspirace
k	k	k7c3	k
novotvaru	novotvar	k1gInSc3	novotvar
Řáholec	Řáholec	k1gInSc1	Řáholec
snad	snad	k9	snad
bylo	být	k5eAaImAgNnS	být
sloveso	sloveso	k1gNnSc1	sloveso
"	"	kIx"	"
<g/>
řáholiti	řáholit	k5eAaImF	řáholit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
hlasitě	hlasitě	k6eAd1	hlasitě
se	se	k3xPyFc4	se
veseliti	veselit	k5eAaImF	veselit
<g/>
,	,	kIx,	,
smáti	smát	k5eAaImF	smát
se	se	k3xPyFc4	se
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
starší	starý	k2eAgFnSc4d2	starší
z	z	k7c2	z
jičínska	jičínsko	k1gNnSc2	jičínsko
pocházející	pocházející	k2eAgFnSc1d1	pocházející
nářeční	nářeční	k2eAgFnSc1d1	nářeční
podoba	podoba	k1gFnSc1	podoba
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
řeholiti	řeholit	k5eAaImF	řeholit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
však	však	k9	však
původ	původ	k1gInSc4	původ
názvu	název	k1gInSc2	název
nesdělil	sdělit	k5eNaPmAgInS	sdělit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
pohádek	pohádka	k1gFnPc2	pohádka
o	o	k7c6	o
Rumcajsovi	Rumcajs	k1gMnSc6	Rumcajs
a	a	k8xC	a
z	z	k7c2	z
recese	recese	k1gFnSc2	recese
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
lesnických	lesnický	k2eAgFnPc6d1	lesnická
mapách	mapa	k1gFnPc6	mapa
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1969	[number]	k4	1969
a	a	k8xC	a
1996	[number]	k4	1996
název	název	k1gInSc1	název
Řáholec	Řáholec	k1gInSc4	Řáholec
přiřknut	přiřknout	k5eAaPmNgInS	přiřknout
lesíku	lesík	k1gInSc6	lesík
pod	pod	k7c7	pod
vrchem	vrch	k1gInSc7	vrch
Veliší	Veliší	k1gNnSc1	Veliší
mezi	mezi	k7c7	mezi
Vokšicemi	Vokšice	k1gFnPc7	Vokšice
a	a	k8xC	a
Šlikovou	Šlikův	k2eAgFnSc7d1	Šlikova
Vsí	ves	k1gFnSc7	ves
<g/>
,	,	kIx,	,
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
jičínskému	jičínský	k2eAgNnSc3d1	Jičínské
polesí	polesí	k1gNnSc3	polesí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
O	o	k7c6	o
loupežníku	loupežník	k1gMnSc6	loupežník
Rumcajsovi	Rumcajs	k1gMnSc6	Rumcajs
</s>
</p>
<p>
<s>
Rumcajs	Rumcajs	k6eAd1	Rumcajs
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
