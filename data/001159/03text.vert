<s>
Skagitský	Skagitský	k2eAgInSc1d1	Skagitský
záliv	záliv	k1gInSc1	záliv
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Skagit	Skagit	k1gMnSc1	Skagit
Bay	Bay	k1gMnSc1	Bay
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
státě	stát	k1gInSc6	stát
Washington	Washington	k1gInSc1	Washington
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
částí	část	k1gFnSc7	část
Pugetova	Pugetův	k2eAgInSc2d1	Pugetův
zálivu	záliv	k1gInSc2	záliv
a	a	k8xC	a
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
něj	on	k3xPp3gMnSc4	on
řeka	řek	k1gMnSc4	řek
Skagit	Skagit	k1gInSc4	Skagit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
je	být	k5eAaImIp3nS	být
záliv	záliv	k1gInSc1	záliv
spojený	spojený	k2eAgInSc1d1	spojený
se	s	k7c7	s
zbytkem	zbytek	k1gInSc7	zbytek
Pugetova	Pugetův	k2eAgInSc2d1	Pugetův
zálivu	záliv	k1gInSc2	záliv
Saratožským	Saratožský	k2eAgInSc7d1	Saratožský
průlivem	průliv	k1gInSc7	průliv
a	a	k8xC	a
zálivem	záliv	k1gInSc7	záliv
Possession	Possession	k1gInSc1	Possession
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
Skagitovým	Skagitový	k2eAgInSc7d1	Skagitový
zálivem	záliv	k1gInSc7	záliv
a	a	k8xC	a
Saratožským	Saratožský	k2eAgInSc7d1	Saratožský
průlivem	průliv	k1gInSc7	průliv
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c4	mezi
Ponell	Ponell	k1gInSc4	Ponell
Pointem	Pointem	k?	Pointem
na	na	k7c6	na
Whidbeyově	Whidbeyův	k2eAgInSc6d1	Whidbeyův
ostrově	ostrov	k1gInSc6	ostrov
a	a	k8xC	a
Rocky	rock	k1gInPc1	rock
Pointem	Pointem	k?	Pointem
na	na	k7c6	na
Caamañ	Caamañ	k1gFnSc6	Caamañ
ostrově	ostrov	k1gInSc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
spojuje	spojovat	k5eAaImIp3nS	spojovat
záliv	záliv	k1gInSc1	záliv
s	s	k7c7	s
úžinou	úžina	k1gFnSc7	úžina
Juana	Juan	k1gMnSc2	Juan
de	de	k?	de
Fucy	Fuca	k1gMnSc2	Fuca
Klamný	klamný	k2eAgInSc4d1	klamný
průliv	průliv	k1gInSc4	průliv
<g/>
.	.	kIx.	.
</s>
<s>
Třetím	třetí	k4xOgNnSc7	třetí
spojením	spojení	k1gNnSc7	spojení
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Swinomišský	Swinomišský	k2eAgInSc4d1	Swinomišský
kanál	kanál	k1gInSc4	kanál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
záliv	záliv	k1gInSc1	záliv
s	s	k7c7	s
Padillovým	Padillův	k2eAgInSc7d1	Padillův
zálivem	záliv	k1gInSc7	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Skagitský	Skagitský	k2eAgInSc1d1	Skagitský
záliv	záliv	k1gInSc1	záliv
omývá	omývat	k5eAaImIp3nS	omývat
na	na	k7c6	na
severu	sever	k1gInSc6	sever
břehy	břeh	k1gInPc4	břeh
Fidalgova	Fidalgův	k2eAgInSc2d1	Fidalgův
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
Whidbeyho	Whidbey	k1gMnSc2	Whidbey
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Caamañ	Caamañ	k1gFnSc2	Caamañ
ostrova	ostrov	k1gInSc2	ostrov
a	a	k8xC	a
na	na	k7c6	na
východě	východ	k1gInSc6	východ
pevninu	pevnina	k1gFnSc4	pevnina
<g/>
.	.	kIx.	.
</s>
<s>
Břeh	břeh	k1gInSc1	břeh
pevniny	pevnina	k1gFnSc2	pevnina
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
především	především	k9	především
z	z	k7c2	z
delty	delta	k1gFnSc2	delta
řeky	řeka	k1gFnSc2	řeka
Skagit	Skagita	k1gFnPc2	Skagita
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
jejímiž	jejíž	k3xOyRp3gInPc7	jejíž
dvěma	dva	k4xCgInPc7	dva
rameny	rameno	k1gNnPc7	rameno
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Jedlový	jedlový	k2eAgInSc1d1	jedlový
ostrov	ostrov	k1gInSc1	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Nejsevernější	severní	k2eAgInSc1d3	nejsevernější
konec	konec	k1gInSc1	konec
Skagitského	Skagitský	k2eAgInSc2d1	Skagitský
zálivu	záliv	k1gInSc2	záliv
nese	nést	k5eAaImIp3nS	nést
jméno	jméno	k1gNnSc4	jméno
Similk	Similk	k1gMnSc1	Similk
Bay	Bay	k1gMnSc1	Bay
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
dva	dva	k4xCgInPc4	dva
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
ostrov	ostrov	k1gInSc4	ostrov
Naděje	naděje	k1gFnSc2	naděje
a	a	k8xC	a
Skagitský	Skagitský	k2eAgInSc4d1	Skagitský
ostrov	ostrov	k1gInSc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Indiánská	indiánský	k2eAgFnSc1d1	indiánská
rezervace	rezervace	k1gFnSc1	rezervace
kmene	kmen	k1gInSc2	kmen
Swinomišů	Swinomiš	k1gMnPc2	Swinomiš
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
Fidalgově	Fidalgův	k2eAgInSc6d1	Fidalgův
ostrově	ostrov	k1gInSc6	ostrov
mezi	mezi	k7c4	mezi
Similk	Similk	k1gInSc4	Similk
Bay	Bay	k1gFnSc2	Bay
a	a	k8xC	a
Swinomišským	Swinomišský	k2eAgInSc7d1	Swinomišský
kanálem	kanál	k1gInSc7	kanál
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
Vancouverovy	Vancouverův	k2eAgFnSc2d1	Vancouverův
expedice	expedice	k1gFnSc2	expedice
roku	rok	k1gInSc2	rok
1792	[number]	k4	1792
byli	být	k5eAaImAgMnP	být
prvními	první	k4xOgMnPc7	první
nepůvodními	původní	k2eNgMnPc7d1	nepůvodní
obyvateli	obyvatel	k1gMnPc7	obyvatel
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
prozkoumali	prozkoumat	k5eAaPmAgMnP	prozkoumat
Skagitský	Skagitský	k2eAgInSc4d1	Skagitský
záliv	záliv	k1gInSc4	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
května	květen	k1gInSc2	květen
1792	[number]	k4	1792
vedl	vést	k5eAaImAgMnS	vést
Joseph	Joseph	k1gMnSc1	Joseph
Whidbey	Whidbea	k1gFnSc2	Whidbea
svůj	svůj	k3xOyFgInSc4	svůj
tým	tým	k1gInSc4	tým
severně	severně	k6eAd1	severně
Saratožským	Saratožský	k2eAgInSc7d1	Saratožský
průlivem	průliv	k1gInSc7	průliv
do	do	k7c2	do
Skagitského	Skagitský	k2eAgInSc2d1	Skagitský
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
strávili	strávit	k5eAaPmAgMnP	strávit
den	den	k1gInSc4	den
průzkumem	průzkum	k1gInSc7	průzkum
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedokázali	dokázat	k5eNaPmAgMnP	dokázat
najít	najít	k5eAaPmF	najít
Klamný	klamný	k2eAgInSc4d1	klamný
průliv	průliv	k1gInSc4	průliv
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
si	se	k3xPyFc3	se
mysleli	myslet	k5eAaImAgMnP	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
záliv	záliv	k1gInSc1	záliv
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
poté	poté	k6eAd1	poté
ale	ale	k8xC	ale
George	George	k1gInSc1	George
Vancouver	Vancouvra	k1gFnPc2	Vancouvra
našel	najít	k5eAaPmAgInS	najít
bažinatý	bažinatý	k2eAgInSc1d1	bažinatý
průliv	průliv	k1gInSc1	průliv
spojující	spojující	k2eAgInSc1d1	spojující
záliv	záliv	k1gInSc4	záliv
se	s	k7c7	s
Susaninou	Susanin	k2eAgFnSc7d1	Susanina
zátokou	zátoka	k1gFnSc7	zátoka
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Caamañ	Caamañ	k1gFnSc2	Caamañ
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
o	o	k7c4	o
týden	týden	k1gInSc4	týden
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
června	červen	k1gInSc2	červen
1792	[number]	k4	1792
proplula	proplout	k5eAaPmAgFnS	proplout
Vancouverova	Vancouverův	k2eAgFnSc1d1	Vancouverův
flotila	flotila	k1gFnSc1	flotila
Klamným	klamný	k2eAgInSc7d1	klamný
průlivem	průliv	k1gInSc7	průliv
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
<g/>
.	.	kIx.	.
</s>
<s>
Peter	Peter	k1gMnSc1	Peter
Puget	Puget	k?	Puget
a	a	k8xC	a
Joseph	Joseph	k1gMnSc1	Joseph
Whidbey	Whidbea	k1gFnSc2	Whidbea
pluli	plout	k5eAaImAgMnP	plout
vyšetřit	vyšetřit	k5eAaPmF	vyšetřit
průliv	průliv	k1gInSc4	průliv
a	a	k8xC	a
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
dá	dát	k5eAaPmIp3nS	dát
proplout	proplout	k5eAaPmF	proplout
právě	právě	k9	právě
do	do	k7c2	do
Skagitova	Skagitův	k2eAgInSc2d1	Skagitův
zálivu	záliv	k1gInSc2	záliv
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
expedici	expedice	k1gFnSc4	expedice
přesvědčilo	přesvědčit	k5eAaPmAgNnS	přesvědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Whidbeyho	Whidbey	k1gMnSc2	Whidbey
ostrov	ostrov	k1gInSc1	ostrov
je	být	k5eAaImIp3nS	být
namísto	namísto	k7c2	namísto
poloostrova	poloostrov	k1gInSc2	poloostrov
ostrovem	ostrov	k1gInSc7	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Skagit	Skagit	k1gMnSc1	Skagit
Bay	Bay	k1gMnSc1	Bay
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
