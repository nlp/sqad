<s>
Bílý	bílý	k2eAgInSc1d1	bílý
dům	dům	k1gInSc1	dům
je	být	k5eAaImIp3nS	být
oficiálním	oficiální	k2eAgNnSc7d1	oficiální
sídlem	sídlo	k1gNnSc7	sídlo
a	a	k8xC	a
pracovištěm	pracoviště	k1gNnSc7	pracoviště
prezidenta	prezident	k1gMnSc2	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kterým	který	k3yRgFnPc3	který
zde	zde	k6eAd1	zde
žije	žít	k5eAaImIp3nS	žít
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
stavbu	stavba	k1gFnSc4	stavba
v	v	k7c6	v
pozdně	pozdně	k6eAd1	pozdně
georgiánském	georgiánský	k2eAgInSc6d1	georgiánský
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
městě	město	k1gNnSc6	město
Washington	Washington	k1gInSc1	Washington
<g/>
,	,	kIx,	,
D.C.	D.C.	k1gFnSc1	D.C.
<g/>
,	,	kIx,	,
na	na	k7c6	na
adrese	adresa	k1gFnSc6	adresa
1600	[number]	k4	1600
Pennsylvania	Pennsylvanium	k1gNnSc2	Pennsylvanium
Avenue	avenue	k1gFnPc2	avenue
NW	NW	kA	NW
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
Bílý	bílý	k2eAgInSc1d1	bílý
dům	dům	k1gInSc1	dům
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
používá	používat	k5eAaImIp3nS	používat
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
samotnou	samotný	k2eAgFnSc4d1	samotná
stavbu	stavba	k1gFnSc4	stavba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
pro	pro	k7c4	pro
administrativu	administrativa	k1gFnSc4	administrativa
prezidenta	prezident	k1gMnSc2	prezident
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
je	být	k5eAaImIp3nS	být
starý	starý	k2eAgInSc1d1	starý
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
200	[number]	k4	200
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
prvním	první	k4xOgMnSc7	první
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
sídlil	sídlit	k5eAaImAgInS	sídlit
v	v	k7c6	v
Bílém	bílý	k2eAgInSc6d1	bílý
domě	dům	k1gInSc6	dům
byl	být	k5eAaImAgMnS	být
John	John	k1gMnSc1	John
Adams	Adamsa	k1gFnPc2	Adamsa
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
druhým	druhý	k4xOgMnSc7	druhý
prezidentem	prezident	k1gMnSc7	prezident
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
Bílého	bílý	k2eAgInSc2d1	bílý
domu	dům	k1gInSc2	dům
začala	začít	k5eAaPmAgFnS	začít
13	[number]	k4	13
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1792	[number]	k4	1792
a	a	k8xC	a
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
mnoho	mnoho	k4c1	mnoho
renovací	renovace	k1gFnPc2	renovace
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
vymalován	vymalovat	k5eAaPmNgInS	vymalovat
bílou	bílý	k2eAgFnSc7d1	bílá
barvou	barva	k1gFnSc7	barva
po	po	k7c6	po
Britsko-americké	britskomerický	k2eAgFnSc6d1	britsko-americká
válce	válka	k1gFnSc6	válka
(	(	kIx(	(
<g/>
též	též	k9	též
válka	válka	k1gFnSc1	válka
roku	rok	k1gInSc2	rok
1812	[number]	k4	1812
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
americká	americký	k2eAgFnSc1d1	americká
válka	válka	k1gFnSc1	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Britové	Brit	k1gMnPc1	Brit
Bílý	bílý	k2eAgInSc1d1	bílý
dům	dům	k1gInSc1	dům
24	[number]	k4	24
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
1814	[number]	k4	1814
podpálili	podpálit	k5eAaPmAgMnP	podpálit
<g/>
.	.	kIx.	.
</s>
<s>
Zásadní	zásadní	k2eAgFnSc1d1	zásadní
oprava	oprava	k1gFnSc1	oprava
nastala	nastat	k5eAaPmAgFnS	nastat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Jacqueline	Jacquelin	k1gInSc5	Jacquelin
Kennedyová	Kennedyová	k1gFnSc1	Kennedyová
nakoupila	nakoupit	k5eAaPmAgFnS	nakoupit
mnoho	mnoho	k6eAd1	mnoho
historických	historický	k2eAgNnPc2d1	historické
i	i	k8xC	i
nových	nový	k2eAgNnPc2d1	nové
bytových	bytový	k2eAgNnPc2d1	bytové
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc4d1	celý
dům	dům	k1gInSc4	dům
zvládla	zvládnout	k5eAaPmAgFnS	zvládnout
zrenovovat	zrenovovat	k5eAaPmF	zrenovovat
během	během	k7c2	během
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
zde	zde	k6eAd1	zde
muzeum	muzeum	k1gNnSc4	muzeum
americké	americký	k2eAgFnSc2d1	americká
historie	historie	k1gFnSc2	historie
se	s	k7c7	s
sbírkou	sbírka	k1gFnSc7	sbírka
prezidentských	prezidentský	k2eAgInPc2d1	prezidentský
portrétů	portrét	k1gInPc2	portrét
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
Bílý	bílý	k2eAgInSc1d1	bílý
dům	dům	k1gInSc1	dům
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
lékařskou	lékařský	k2eAgFnSc4d1	lékařská
a	a	k8xC	a
zubní	zubní	k2eAgFnSc4d1	zubní
ordinaci	ordinace	k1gFnSc4	ordinace
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgNnSc4d1	televizní
studio	studio	k1gNnSc4	studio
<g/>
,	,	kIx,	,
solárium	solárium	k1gNnSc4	solárium
<g/>
,	,	kIx,	,
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
bazén	bazén	k1gInSc4	bazén
a	a	k8xC	a
úkryt	úkryt	k1gInSc4	úkryt
proti	proti	k7c3	proti
jaderným	jaderný	k2eAgFnPc3d1	jaderná
bombám	bomba	k1gFnPc3	bomba
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
bydlí	bydlet	k5eAaImIp3nS	bydlet
44	[number]	k4	44
<g/>
.	.	kIx.	.
prezident	prezident	k1gMnSc1	prezident
USA	USA	kA	USA
Barack	Barack	k1gMnSc1	Barack
Obama	Obama	k?	Obama
<g/>
.	.	kIx.	.
</s>
<s>
Bílý	bílý	k2eAgInSc1d1	bílý
dům	dům	k1gInSc1	dům
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
irského	irský	k2eAgMnSc2d1	irský
architekta	architekt	k1gMnSc2	architekt
Jamese	Jamese	k1gFnSc2	Jamese
Hobana	Hoban	k1gMnSc2	Hoban
postaven	postavit	k5eAaPmNgInS	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1792	[number]	k4	1792
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úpravách	úprava	k1gFnPc6	úprava
a	a	k8xC	a
rozšířeních	rozšíření	k1gNnPc6	rozšíření
dnes	dnes	k6eAd1	dnes
zabírá	zabírat	k5eAaImIp3nS	zabírat
plochu	plocha	k1gFnSc4	plocha
přes	přes	k7c4	přes
5100	[number]	k4	5100
metrů	metr	k1gInPc2	metr
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
na	na	k7c6	na
pozemku	pozemek	k1gInSc6	pozemek
o	o	k7c6	o
výměře	výměra	k1gFnSc6	výměra
více	hodně	k6eAd2	hodně
než	než	k8xS	než
sedmi	sedm	k4xCc2	sedm
hektarů	hektar	k1gInPc2	hektar
<g/>
.	.	kIx.	.
</s>
<s>
Adresa	adresa	k1gFnSc1	adresa
Bílého	bílý	k2eAgInSc2d1	bílý
domu	dům	k1gInSc2	dům
je	být	k5eAaImIp3nS	být
<g/>
:	:	kIx,	:
1600	[number]	k4	1600
Pennsylvania	Pennsylvanium	k1gNnSc2	Pennsylvanium
Ave	ave	k1gNnSc2	ave
NW	NW	kA	NW
Washington	Washington	k1gInSc1	Washington
<g/>
,	,	kIx,	,
D.C.	D.C.	k1gFnSc1	D.C.
20500	[number]	k4	20500
United	United	k1gMnSc1	United
States	States	k1gMnSc1	States
of	of	k?	of
America	America	k1gMnSc1	America
</s>
<s>
Bílý	bílý	k2eAgInSc1d1	bílý
dům	dům	k1gInSc1	dům
má	mít	k5eAaImIp3nS	mít
132	[number]	k4	132
pokojů	pokoj	k1gInPc2	pokoj
<g/>
,	,	kIx,	,
tři	tři	k4xCgFnPc4	tři
kuchyně	kuchyně	k1gFnPc4	kuchyně
<g/>
,	,	kIx,	,
35	[number]	k4	35
koupelen	koupelna	k1gFnPc2	koupelna
<g/>
,	,	kIx,	,
16	[number]	k4	16
ložnic	ložnice	k1gFnPc2	ložnice
<g/>
,	,	kIx,	,
412	[number]	k4	412
dveří	dveře	k1gFnPc2	dveře
<g/>
,	,	kIx,	,
147	[number]	k4	147
oken	okno	k1gNnPc2	okno
<g/>
,	,	kIx,	,
28	[number]	k4	28
krbů	krb	k1gInPc2	krb
<g/>
,	,	kIx,	,
12	[number]	k4	12
komínů	komín	k1gInPc2	komín
<g/>
,	,	kIx,	,
osm	osm	k4xCc4	osm
schodišť	schodiště	k1gNnPc2	schodiště
a	a	k8xC	a
tři	tři	k4xCgInPc1	tři
výtahy	výtah	k1gInPc1	výtah
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pět	pět	k4xCc4	pět
celodenních	celodenní	k2eAgMnPc2d1	celodenní
kuchařů	kuchař	k1gMnPc2	kuchař
<g/>
,	,	kIx,	,
5000	[number]	k4	5000
návštěvníků	návštěvník	k1gMnPc2	návštěvník
denně	denně	k6eAd1	denně
a	a	k8xC	a
1	[number]	k4	1
825	[number]	k4	825
000	[number]	k4	000
návštěvníků	návštěvník	k1gMnPc2	návštěvník
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
tenisové	tenisový	k2eAgNnSc4d1	tenisové
hřiště	hřiště	k1gNnSc4	hřiště
<g/>
,	,	kIx,	,
bowlingovou	bowlingový	k2eAgFnSc4d1	bowlingová
dráhu	dráha	k1gFnSc4	dráha
,	,	kIx,	,
kino	kino	k1gNnSc4	kino
<g/>
,	,	kIx,	,
plavecký	plavecký	k2eAgInSc4d1	plavecký
bazén	bazén	k1gInSc4	bazén
a	a	k8xC	a
podzemní	podzemní	k2eAgInSc4d1	podzemní
úkryt	úkryt	k1gInSc4	úkryt
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
pokojem	pokoj	k1gInSc7	pokoj
je	být	k5eAaImIp3nS	být
Oválná	oválný	k2eAgFnSc1d1	oválná
pracovna	pracovna	k1gFnSc1	pracovna
-	-	kIx~	-
oficiální	oficiální	k2eAgFnSc1d1	oficiální
pracovna	pracovna	k1gFnSc1	pracovna
prezidenta	prezident	k1gMnSc2	prezident
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zahradě	zahrada	k1gFnSc6	zahrada
je	být	k5eAaImIp3nS	být
vysazeno	vysadit	k5eAaPmNgNnS	vysadit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
300	[number]	k4	300
druhů	druh	k1gInPc2	druh
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
přes	přes	k7c4	přes
678	[number]	k4	678
květin	květina	k1gFnPc2	květina
<g/>
.	.	kIx.	.
</s>
<s>
Zahrada	zahrada	k1gFnSc1	zahrada
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
celý	celý	k2eAgInSc4d1	celý
objekt	objekt	k1gInSc4	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
se	se	k3xPyFc4	se
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
na	na	k7c4	na
Velikonoční	velikonoční	k2eAgNnSc4d1	velikonoční
pondělí	pondělí	k1gNnSc4	pondělí
účastní	účastnit	k5eAaImIp3nS	účastnit
tradičního	tradiční	k2eAgNnSc2d1	tradiční
koulení	koulení	k1gNnSc2	koulení
vajíček	vajíčko	k1gNnPc2	vajíčko
na	na	k7c6	na
trávě	tráva	k1gFnSc6	tráva
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
Bílým	bílý	k2eAgInSc7d1	bílý
domem	dům	k1gInSc7	dům
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
šest	šest	k4xCc1	šest
metrů	metr	k1gInPc2	metr
široký	široký	k2eAgInSc4d1	široký
altán	altán	k1gInSc4	altán
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
před	před	k7c7	před
hlavním	hlavní	k2eAgInSc7d1	hlavní
vchodem	vchod	k1gInSc7	vchod
ve	v	k7c6	v
"	"	kIx"	"
<g/>
Vchodové	vchodový	k2eAgFnSc6d1	vchodová
zahradě	zahrada	k1gFnSc6	zahrada
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bílý	bílý	k2eAgInSc4d1	bílý
dům	dům	k1gInSc4	dům
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgInSc1d1	oficiální
web	web	k1gInSc1	web
Bílého	bílý	k2eAgInSc2d1	bílý
domu	dům	k1gInSc2	dům
</s>
