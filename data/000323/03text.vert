<s>
Labe	Labe	k1gNnSc1	Labe
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Elbe	Elb	k1gMnSc2	Elb
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
řek	řeka	k1gFnPc2	řeka
a	a	k8xC	a
vodních	vodní	k2eAgFnPc2d1	vodní
cest	cesta	k1gFnPc2	cesta
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Pramení	pramenit	k5eAaImIp3nS	pramenit
v	v	k7c6	v
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
protéká	protékat	k5eAaImIp3nS	protékat
Německem	Německo	k1gNnSc7	Německo
a	a	k8xC	a
ústí	ústit	k5eAaImIp3nS	ústit
estuárem	estuár	k1gInSc7	estuár
do	do	k7c2	do
Severního	severní	k2eAgNnSc2d1	severní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
skoro	skoro	k6eAd1	skoro
jediná	jediný	k2eAgFnSc1d1	jediná
česká	český	k2eAgFnSc1d1	Česká
řeka	řeka	k1gFnSc1	řeka
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
český	český	k2eAgInSc4d1	český
název	název	k1gInSc4	název
není	být	k5eNaImIp3nS	být
ženského	ženský	k2eAgInSc2d1	ženský
rodu	rod	k1gInSc2	rod
(	(	kIx(	(
<g/>
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
však	však	k9	však
je	být	k5eAaImIp3nS	být
ženského	ženský	k2eAgInSc2d1	ženský
rodu	rod	k1gInSc2	rod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
1094	[number]	k4	1094
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
(	(	kIx(	(
<g/>
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
370,74	[number]	k4	370,74
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
její	její	k3xOp3gNnSc1	její
povodí	povodí	k1gNnSc1	povodí
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
148	[number]	k4	148
268	[number]	k4	268
km2	km2	k4	km2
(	(	kIx(	(
<g/>
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
49	[number]	k4	49
933	[number]	k4	933
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
soutoku	soutok	k1gInSc6	soutok
s	s	k7c7	s
Vltavou	Vltava	k1gFnSc7	Vltava
má	mít	k5eAaImIp3nS	mít
nižší	nízký	k2eAgInSc4d2	nižší
průtok	průtok	k1gInSc4	průtok
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
pramene	pramen	k1gInSc2	pramen
kratší	krátký	k2eAgInPc1d2	kratší
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
nepovažuje	považovat	k5eNaImIp3nS	považovat
za	za	k7c4	za
její	její	k3xOp3gInSc4	její
přítok	přítok	k1gInSc4	přítok
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
toku	tok	k1gInSc2	tok
Černého	Černého	k2eAgInSc2d1	Černého
potoka	potok	k1gInSc2	potok
<g/>
,	,	kIx,	,
Teplé	Teplé	k2eAgFnSc2d1	Teplé
Vltavy	Vltava	k1gFnSc2	Vltava
<g/>
,	,	kIx,	,
Vltavy	Vltava	k1gFnSc2	Vltava
a	a	k8xC	a
Labe	Labe	k1gNnSc2	Labe
od	od	k7c2	od
soutoku	soutok	k1gInSc2	soutok
s	s	k7c7	s
Vltavou	Vltava	k1gFnSc7	Vltava
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
činí	činit	k5eAaImIp3nS	činit
1329	[number]	k4	1329
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
dali	dát	k5eAaPmAgMnP	dát
řece	řeka	k1gFnSc3	řeka
snad	snad	k9	snad
Keltové	Kelt	k1gMnPc1	Kelt
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
její	její	k3xOp3gNnSc4	její
povodí	povodit	k5eAaPmIp3nP	povodit
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
českého	český	k2eAgNnSc2d1	české
území	území	k1gNnSc2	území
<g/>
)	)	kIx)	)
obývali	obývat	k5eAaImAgMnP	obývat
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgNnSc1d1	původní
keltské	keltský	k2eAgNnSc1d1	keltské
slovo	slovo	k1gNnSc1	slovo
(	(	kIx(	(
<g/>
snad	snad	k9	snad
elb	elb	k?	elb
<g/>
)	)	kIx)	)
znamenalo	znamenat	k5eAaImAgNnS	znamenat
prostě	prostě	k6eAd1	prostě
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
velká	velký	k2eAgFnSc1d1	velká
<g/>
)	)	kIx)	)
řeka	řeka	k1gFnSc1	řeka
<g/>
"	"	kIx"	"
a	a	k8xC	a
objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
v	v	k7c6	v
názvech	název	k1gInPc6	název
mnoha	mnoho	k4c2	mnoho
řek	řeka	k1gFnPc2	řeka
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
(	(	kIx(	(
<g/>
älv	älv	k?	älv
v	v	k7c6	v
Ume	um	k1gInSc5	um
älv	älv	k?	älv
<g/>
,	,	kIx,	,
Lule	lula	k1gFnSc3	lula
älv	älv	k?	älv
<g/>
,	,	kIx,	,
Pite	Pite	k1gFnSc1	Pite
älv	älv	k?	älv
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Keltské	keltský	k2eAgNnSc1d1	keltské
slovo	slovo	k1gNnSc1	slovo
Albth	Albtha	k1gFnPc2	Albtha
zase	zase	k9	zase
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
bílá	bílý	k2eAgFnSc1d1	bílá
řeka	řeka	k1gFnSc1	řeka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
podle	podle	k7c2	podle
zbarvení	zbarvení	k1gNnSc2	zbarvení
peřejí	peřej	k1gFnPc2	peřej
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Krkonoš	Krkonoše	k1gFnPc2	Krkonoše
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
protéká	protékat	k5eAaImIp3nS	protékat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
pramenů	pramen	k1gInPc2	pramen
má	mít	k5eAaImIp3nS	mít
jméno	jméno	k1gNnSc4	jméno
původ	původ	k1gInSc1	původ
v	v	k7c6	v
indoevropském	indoevropský	k2eAgInSc6d1	indoevropský
základu	základ	k1gInSc6	základ
<g/>
,	,	kIx,	,
znamenajícím	znamenající	k2eAgInSc6d1	znamenající
"	"	kIx"	"
<g/>
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
světlý	světlý	k2eAgInSc1d1	světlý
<g/>
,	,	kIx,	,
čistý	čistý	k2eAgInSc1d1	čistý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
antických	antický	k2eAgInPc6d1	antický
pramenech	pramen	k1gInPc6	pramen
se	se	k3xPyFc4	se
řeka	řeka	k1gFnSc1	řeka
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
jako	jako	k9	jako
Albis	Albis	k1gFnSc1	Albis
<g/>
,	,	kIx,	,
také	také	k9	také
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
bílý	bílý	k1gMnSc1	bílý
<g/>
,	,	kIx,	,
světlý	světlý	k2eAgMnSc1d1	světlý
<g/>
,	,	kIx,	,
čistý	čistý	k2eAgInSc1d1	čistý
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
z	z	k7c2	z
germánského	germánský	k2eAgNnSc2d1	germánské
Alba	album	k1gNnSc2	album
pomocí	pomocí	k7c2	pomocí
přesmyčky	přesmyčka	k1gFnSc2	přesmyčka
<g/>
.	.	kIx.	.
</s>
<s>
Labe	Labe	k1gNnSc1	Labe
je	být	k5eAaImIp3nS	být
označováno	označovat	k5eAaImNgNnS	označovat
za	za	k7c4	za
jedinou	jediný	k2eAgFnSc4d1	jediná
českou	český	k2eAgFnSc4d1	Česká
řeku	řeka	k1gFnSc4	řeka
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
název	název	k1gInSc1	název
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
ženském	ženský	k2eAgInSc6d1	ženský
rodě	rod	k1gInSc6	rod
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
Keltům	Kelt	k1gMnPc3	Kelt
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
také	také	k9	také
Vltava	Vltava	k1gFnSc1	Vltava
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
přítok	přítok	k1gInSc4	přítok
Labe	Labe	k1gNnSc2	Labe
<g/>
,	,	kIx,	,
a	a	k8xC	a
ne	ne	k9	ne
obráceně	obráceně	k6eAd1	obráceně
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
je	být	k5eAaImIp3nS	být
Vltava	Vltava	k1gFnSc1	Vltava
od	od	k7c2	od
pramenů	pramen	k1gInPc2	pramen
k	k	k7c3	k
soutoku	soutok	k1gInSc3	soutok
s	s	k7c7	s
Labem	Labe	k1gNnSc7	Labe
zřetelně	zřetelně	k6eAd1	zřetelně
delší	dlouhý	k2eAgMnSc1d2	delší
a	a	k8xC	a
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
soutoku	soutok	k1gInSc2	soutok
má	mít	k5eAaImIp3nS	mít
též	též	k9	též
značně	značně	k6eAd1	značně
vyšší	vysoký	k2eAgInSc4d2	vyšší
průtok	průtok	k1gInSc4	průtok
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc4	název
řece	řeka	k1gFnSc3	řeka
daly	dát	k5eAaPmAgInP	dát
germánské	germánský	k2eAgInPc1d1	germánský
kmeny	kmen	k1gInPc1	kmen
<g/>
:	:	kIx,	:
Wildachva	Wildachva	k1gFnSc1	Wildachva
či	či	k8xC	či
Wild	Wild	k1gInSc1	Wild
achva	achva	k6eAd1	achva
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
"	"	kIx"	"
<g/>
divoká	divoký	k2eAgFnSc1d1	divoká
voda	voda	k1gFnSc1	voda
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místě	místo	k1gNnSc6	místo
začátku	začátek	k1gInSc2	začátek
řeky	řeka	k1gFnSc2	řeka
na	na	k7c6	na
Šumavě	Šumava	k1gFnSc6	Šumava
poblíž	poblíž	k6eAd1	poblíž
dnešních	dnešní	k2eAgFnPc2d1	dnešní
německých	německý	k2eAgFnPc2d1	německá
hranic	hranice	k1gFnPc2	hranice
má	mít	k5eAaImIp3nS	mít
Vltava	Vltava	k1gFnSc1	Vltava
totiž	totiž	k9	totiž
divoký	divoký	k2eAgInSc4d1	divoký
průtok	průtok	k1gInSc4	průtok
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
peřeje	peřej	k1gFnPc4	peřej
<g/>
.	.	kIx.	.
</s>
<s>
Kilometráž	kilometráž	k1gFnSc1	kilometráž
Labe	Labe	k1gNnSc2	Labe
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Vltava	Vltava	k1gFnSc1	Vltava
byla	být	k5eAaImAgFnS	být
staničena	staničit	k5eAaImNgFnS	staničit
po	po	k7c6	po
proudu	proud	k1gInSc6	proud
<g/>
.	.	kIx.	.
</s>
<s>
Labe	Labe	k1gNnSc1	Labe
bylo	být	k5eAaImAgNnS	být
bráno	brát	k5eAaImNgNnS	brát
jako	jako	k8xC	jako
navazující	navazující	k2eAgFnSc1d1	navazující
vodní	vodní	k2eAgFnSc1d1	vodní
cesta	cesta	k1gFnSc1	cesta
a	a	k8xC	a
od	od	k7c2	od
Mělníka	Mělník	k1gInSc2	Mělník
po	po	k7c6	po
proudu	proud	k1gInSc6	proud
bylo	být	k5eAaImAgNnS	být
staničeno	staničit	k5eAaImNgNnS	staničit
od	od	k7c2	od
nuly	nula	k1gFnSc2	nula
<g/>
,	,	kIx,	,
úsek	úsek	k1gInSc4	úsek
Labe	Labe	k1gNnSc2	Labe
nad	nad	k7c7	nad
soutokem	soutok	k1gInSc7	soutok
byl	být	k5eAaImAgInS	být
využíván	využívat	k5eAaPmNgInS	využívat
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
jen	jen	k9	jen
k	k	k7c3	k
zámku	zámek	k1gInSc3	zámek
v	v	k7c6	v
Obříství	Obříství	k1gNnSc6	Obříství
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
splavňování	splavňování	k1gNnSc4	splavňování
středního	střední	k2eAgNnSc2d1	střední
Labe	Labe	k1gNnSc2	Labe
<g/>
,	,	kIx,	,
a	a	k8xC	a
aby	aby	kYmCp3nS	aby
nemusel	muset	k5eNaImAgInS	muset
být	být	k5eAaImF	být
přeznačován	přeznačován	k2eAgInSc1d1	přeznačován
úsek	úsek	k1gInSc1	úsek
pod	pod	k7c7	pod
Mělníkem	Mělník	k1gInSc7	Mělník
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
střední	střední	k2eAgNnSc1d1	střední
Labe	Labe	k1gNnSc1	Labe
značeno	značen	k2eAgNnSc1d1	značeno
rovněž	rovněž	k9	rovněž
od	od	k7c2	od
Mělníka	Mělník	k1gInSc2	Mělník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
proti	proti	k7c3	proti
proudu	proud	k1gInSc3	proud
-	-	kIx~	-
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
bylo	být	k5eAaImAgNnS	být
staničení	staničení	k1gNnSc1	staničení
vyznačeno	vyznačit	k5eAaPmNgNnS	vyznačit
až	až	k6eAd1	až
do	do	k7c2	do
Kolína	Kolín	k1gInSc2	Kolín
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
poté	poté	k6eAd1	poté
bylo	být	k5eAaImAgNnS	být
otočeno	otočit	k5eAaPmNgNnS	otočit
i	i	k9	i
staničení	staničení	k1gNnSc1	staničení
Vltavy	Vltava	k1gFnSc2	Vltava
do	do	k7c2	do
směru	směr	k1gInSc2	směr
proti	proti	k7c3	proti
proudu	proud	k1gInSc3	proud
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
soutok	soutok	k1gInSc1	soutok
u	u	k7c2	u
Mělníka	Mělník	k1gInSc2	Mělník
stal	stát	k5eAaPmAgInS	stát
nulovým	nulový	k2eAgInSc7d1	nulový
bodem	bod	k1gInSc7	bod
již	již	k6eAd1	již
třetího	třetí	k4xOgInSc2	třetí
úseku	úsek	k1gInSc2	úsek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
úseku	úsek	k1gInSc6	úsek
Labe	Labe	k1gNnSc2	Labe
se	se	k3xPyFc4	se
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
používalo	používat	k5eAaImAgNnS	používat
pět	pět	k4xCc1	pět
různých	různý	k2eAgFnPc2d1	různá
kilometráží	kilometráž	k1gFnPc2	kilometráž
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
názvy	název	k1gInPc1	název
nejstarší	starý	k2eAgInPc1d3	nejstarší
<g/>
,	,	kIx,	,
plavební	plavební	k2eAgFnSc1d1	plavební
<g/>
,	,	kIx,	,
jednotná	jednotný	k2eAgFnSc1d1	jednotná
říční	říční	k2eAgNnSc4d1	říční
<g/>
,	,	kIx,	,
administrativní	administrativní	k2eAgNnSc4d1	administrativní
a	a	k8xC	a
digitální	digitální	k2eAgNnSc4d1	digitální
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
kilometráž	kilometráž	k1gFnSc1	kilometráž
Labe	Labe	k1gNnSc2	Labe
má	mít	k5eAaImIp3nS	mít
nulový	nulový	k2eAgInSc4d1	nulový
bod	bod	k1gInSc4	bod
v	v	k7c6	v
úrovni	úroveň	k1gFnSc6	úroveň
špičky	špička	k1gFnSc2	špička
ostrohu	ostroh	k1gInSc2	ostroh
mezi	mezi	k7c7	mezi
Labem	Labe	k1gNnSc7	Labe
a	a	k8xC	a
Vltavou	Vltava	k1gFnSc7	Vltava
na	na	k7c6	na
jejich	jejich	k3xOp3gInSc6	jejich
soutoku	soutok	k1gInSc6	soutok
<g/>
,	,	kIx,	,
na	na	k7c6	na
(	(	kIx(	(
<g/>
českém	český	k2eAgInSc6d1	český
<g/>
)	)	kIx)	)
dolním	dolní	k2eAgNnSc6d1	dolní
Labi	Labe	k1gNnSc6	Labe
je	být	k5eAaImIp3nS	být
vedena	vést	k5eAaImNgFnS	vést
po	po	k7c6	po
směru	směr	k1gInSc6	směr
toku	tok	k1gInSc2	tok
až	až	k9	až
do	do	k7c2	do
km	km	kA	km
109,27	[number]	k4	109,27
v	v	k7c6	v
Hřensku	Hřensko	k1gNnSc6	Hřensko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
státní	státní	k2eAgFnSc1d1	státní
hranice	hranice	k1gFnSc1	hranice
protíná	protínat	k5eAaImIp3nS	protínat
pravý	pravý	k2eAgInSc4d1	pravý
břeh	břeh	k1gInSc4	břeh
<g/>
,	,	kIx,	,
na	na	k7c6	na
středním	střední	k2eAgNnSc6d1	střední
Labi	Labe	k1gNnSc6	Labe
proti	proti	k7c3	proti
toku	tok	k1gInSc3	tok
<g/>
.	.	kIx.	.
</s>
<s>
Podkladem	podklad	k1gInSc7	podklad
byl	být	k5eAaImAgMnS	být
Podélný	podélný	k2eAgInSc4d1	podélný
profil	profil	k1gInSc4	profil
řeky	řeka	k1gFnSc2	řeka
Labe	Labe	k1gNnSc2	Labe
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
vydala	vydat	k5eAaPmAgFnS	vydat
Geodesie	geodesie	k1gFnSc1	geodesie
Praha	Praha	k1gFnSc1	Praha
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kilometráž	kilometráž	k1gFnSc1	kilometráž
byla	být	k5eAaImAgFnS	být
značena	značit	k5eAaImNgFnS	značit
betonovými	betonový	k2eAgInPc7d1	betonový
bloky	blok	k1gInPc7	blok
kolem	kolem	k7c2	kolem
břehové	břehový	k2eAgFnSc2d1	Břehová
hrany	hrana	k1gFnSc2	hrana
<g/>
,	,	kIx,	,
hektometrovníky	hektometrovník	k1gInPc1	hektometrovník
byly	být	k5eAaImAgInP	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
betonovými	betonový	k2eAgInPc7d1	betonový
trámci	trámec	k1gInPc7	trámec
a	a	k8xC	a
půlkilometrovníky	půlkilometrovník	k1gInPc7	půlkilometrovník
a	a	k8xC	a
kilometrovníky	kilometrovník	k1gInPc1	kilometrovník
měly	mít	k5eAaImAgInP	mít
na	na	k7c4	na
sobě	se	k3xPyFc3	se
výškový	výškový	k2eAgInSc4d1	výškový
bod	bod	k1gInSc4	bod
nivelace	nivelace	k1gFnSc2	nivelace
jadranského	jadranský	k2eAgInSc2d1	jadranský
systému	systém	k1gInSc2	systém
<g/>
;	;	kIx,	;
značení	značení	k1gNnSc1	značení
končilo	končit	k5eAaImAgNnS	končit
pod	pod	k7c7	pod
jezem	jez	k1gInSc7	jez
ve	v	k7c6	v
Veletově	Veletův	k2eAgNnSc6d1	Veletův
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
toto	tento	k3xDgNnSc1	tento
značení	značení	k1gNnSc1	značení
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
udržováno	udržován	k2eAgNnSc1d1	udržováno
<g/>
.	.	kIx.	.
</s>
<s>
Plavební	plavební	k2eAgFnSc1d1	plavební
kilometráž	kilometráž	k1gFnSc1	kilometráž
Labe	Labe	k1gNnSc2	Labe
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1976	[number]	k4	1976
<g/>
-	-	kIx~	-
<g/>
1978	[number]	k4	1978
vyznačena	vyznačit	k5eAaPmNgNnP	vyznačit
tabulemi	tabule	k1gFnPc7	tabule
(	(	kIx(	(
<g/>
kilometrovníky	kilometrovník	k1gInPc7	kilometrovník
<g/>
,	,	kIx,	,
půlkilometrovníky	půlkilometrovník	k1gMnPc7	půlkilometrovník
a	a	k8xC	a
hektometrovníky	hektometrovník	k1gMnPc7	hektometrovník
<g/>
)	)	kIx)	)
na	na	k7c6	na
sloupech	sloup	k1gInPc6	sloup
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
úseku	úsek	k1gInSc6	úsek
středního	střední	k2eAgNnSc2d1	střední
Labe	Labe	k1gNnSc2	Labe
po	po	k7c6	po
úpravách	úprava	k1gFnPc6	úprava
řeky	řeka	k1gFnSc2	řeka
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
úseku	úsek	k1gInSc3	úsek
Veletov	Veletovo	k1gNnPc2	Veletovo
-	-	kIx~	-
Týnec	Týnec	k1gInSc1	Týnec
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
provedených	provedený	k2eAgFnPc2d1	provedená
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1974	[number]	k4	1974
<g/>
-	-	kIx~	-
<g/>
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
Hřensko	Hřensko	k1gNnSc1	Hřensko
-	-	kIx~	-
Veletov	Veletov	k1gInSc1	Veletov
se	se	k3xPyFc4	se
shoduje	shodovat	k5eAaImIp3nS	shodovat
s	s	k7c7	s
nejstarší	starý	k2eAgFnSc7d3	nejstarší
kilometráží	kilometráž	k1gFnSc7	kilometráž
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
středním	střední	k2eAgNnSc6d1	střední
Labi	Labe	k1gNnSc6	Labe
byla	být	k5eAaImAgNnP	být
vyznačena	vyznačit	k5eAaPmNgNnP	vyznačit
až	až	k6eAd1	až
po	po	k7c6	po
řkm	řkm	k?	řkm
102,10	[number]	k4	102,10
na	na	k7c6	na
konci	konec	k1gInSc6	konec
přístavu	přístav	k1gInSc2	přístav
Chvaletice	Chvaletice	k1gFnPc1	Chvaletice
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vyznačena	vyznačit	k5eAaPmNgFnS	vyznačit
na	na	k7c6	na
plavebních	plavební	k2eAgFnPc6d1	plavební
mapách	mapa	k1gFnPc6	mapa
Labe	Labe	k1gNnSc2	Labe
z	z	k7c2	z
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
Kartografie	kartografie	k1gFnSc2	kartografie
Praha	Praha	k1gFnSc1	Praha
v	v	k7c6	v
měřítku	měřítko	k1gNnSc6	měřítko
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
5000	[number]	k4	5000
(	(	kIx(	(
<g/>
Chvaletice	Chvaletice	k1gFnPc1	Chvaletice
<g/>
-	-	kIx~	-
<g/>
Mělník	Mělník	k1gInSc1	Mělník
vydána	vydán	k2eAgFnSc1d1	vydána
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
Mělník	Mělník	k1gInSc1	Mělník
<g/>
-	-	kIx~	-
<g/>
Hřensko	Hřensko	k1gNnSc1	Hřensko
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kilometráž	kilometráž	k1gFnSc1	kilometráž
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
Chvaletice	Chvaletice	k1gFnPc1	Chvaletice
-	-	kIx~	-
Kunětice	Kunětice	k1gFnSc1	Kunětice
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
terénu	terén	k1gInSc6	terén
vyznačena	vyznačit	k5eAaPmNgNnP	vyznačit
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Jednotná	jednotný	k2eAgFnSc1d1	jednotná
říční	říční	k2eAgFnSc1d1	říční
kilometráž	kilometráž	k1gFnSc1	kilometráž
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
ředitelem	ředitel	k1gMnSc7	ředitel
tehdejší	tehdejší	k2eAgMnPc1d1	tehdejší
státem	stát	k1gInSc7	stát
vlastněné	vlastněný	k2eAgFnSc2d1	vlastněná
akciové	akciový	k2eAgFnSc2d1	akciová
společnost	společnost	k1gFnSc4	společnost
Povodí	povodí	k1gNnSc2	povodí
Labe	Labe	k1gNnSc2	Labe
příkazem	příkaz	k1gInSc7	příkaz
s	s	k7c7	s
platností	platnost	k1gFnSc7	platnost
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1997	[number]	k4	1997
na	na	k7c6	na
základě	základ	k1gInSc6	základ
doporučení	doporučení	k1gNnSc2	doporučení
skupiny	skupina	k1gFnSc2	skupina
MKOL	MKOL	kA	MKOL
<g/>
.	.	kIx.	.
</s>
<s>
Nulu	nula	k1gFnSc4	nula
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
dosavadního	dosavadní	k2eAgInSc2d1	dosavadní
kilometru	kilometr	k1gInSc2	kilometr
109,270	[number]	k4	109,270
nejstarší	starý	k2eAgFnSc2d3	nejstarší
kilometráže	kilometráž	k1gFnSc2	kilometráž
v	v	k7c6	v
Hřensku	Hřensko	k1gNnSc6	Hřensko
a	a	k8xC	a
celá	celý	k2eAgFnSc1d1	celá
je	být	k5eAaImIp3nS	být
počítána	počítán	k2eAgFnSc1d1	počítána
proti	proti	k7c3	proti
toku	tok	k1gInSc3	tok
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
dolním	dolní	k2eAgNnSc6d1	dolní
Labi	Labe	k1gNnSc6	Labe
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
obrátil	obrátit	k5eAaPmAgInS	obrátit
směr	směr	k1gInSc1	směr
staničení	staničení	k1gNnSc2	staničení
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
nominální	nominální	k2eAgFnSc1d1	nominální
délka	délka	k1gFnSc1	délka
úseku	úsek	k1gInSc2	úsek
o	o	k7c4	o
necelý	celý	k2eNgInSc4d1	necelý
kilometr	kilometr	k1gInSc4	kilometr
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
následkem	následkem	k7c2	následkem
přeměření	přeměření	k1gNnSc2	přeměření
<g/>
,	,	kIx,	,
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
středním	střední	k2eAgNnSc6d1	střední
Labi	Labe	k1gNnSc6	Labe
se	se	k3xPyFc4	se
dosavadní	dosavadní	k2eAgFnPc1d1	dosavadní
hodnoty	hodnota	k1gFnPc1	hodnota
zvýšily	zvýšit	k5eAaPmAgFnP	zvýšit
o	o	k7c4	o
konstantu	konstanta	k1gFnSc4	konstanta
110,035	[number]	k4	110,035
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Jednotná	jednotný	k2eAgFnSc1d1	jednotná
říční	říční	k2eAgFnSc1d1	říční
kilometráž	kilometráž	k1gFnSc1	kilometráž
však	však	k9	však
nebyla	být	k5eNaImAgFnS	být
vyznačena	vyznačit	k5eAaPmNgFnS	vyznačit
v	v	k7c6	v
terénu	terén	k1gInSc6	terén
<g/>
.	.	kIx.	.
</s>
<s>
Administrativní	administrativní	k2eAgFnSc1d1	administrativní
kilometráž	kilometráž	k1gFnSc1	kilometráž
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
Jednotné	jednotný	k2eAgFnSc2d1	jednotná
říční	říční	k2eAgFnSc2d1	říční
kilometráže	kilometráž	k1gFnSc2	kilometráž
a	a	k8xC	a
z	z	k7c2	z
technicko-provozní	technickorovozní	k2eAgFnSc2d1	technicko-provozní
evidence	evidence	k1gFnSc2	evidence
Labe	Labe	k1gNnSc2	Labe
(	(	kIx(	(
<g/>
TPE	TPE	kA	TPE
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
zpracovalo	zpracovat	k5eAaPmAgNnS	zpracovat
Povodí	povodí	k1gNnSc1	povodí
Labe	Labe	k1gNnSc2	Labe
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1968	[number]	k4	1968
<g/>
-	-	kIx~	-
<g/>
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
</s>
<s>
Počátek	počátek	k1gInSc1	počátek
má	mít	k5eAaImIp3nS	mít
u	u	k7c2	u
Hřenska	Hřensko	k1gNnSc2	Hřensko
v	v	k7c6	v
bodě	bod	k1gInSc6	bod
109,240	[number]	k4	109,240
plavební	plavební	k2eAgFnPc1d1	plavební
kilometráže	kilometráž	k1gFnPc1	kilometráž
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
administrativní	administrativní	k2eAgFnSc4d1	administrativní
kilometráž	kilometráž	k1gFnSc4	kilometráž
je	být	k5eAaImIp3nS	být
navázána	navázán	k2eAgFnSc1d1	navázána
technicko-provozní	technickorovozní	k2eAgFnSc1d1	technicko-provozní
evidence	evidence	k1gFnSc1	evidence
jezů	jez	k1gInPc2	jez
<g/>
,	,	kIx,	,
plavebních	plavební	k2eAgFnPc2d1	plavební
komor	komora	k1gFnPc2	komora
<g/>
,	,	kIx,	,
elektráren	elektrárna	k1gFnPc2	elektrárna
<g/>
,	,	kIx,	,
mostů	most	k1gInPc2	most
<g/>
,	,	kIx,	,
zaústění	zaústění	k1gNnSc2	zaústění
<g/>
,	,	kIx,	,
uvazovacích	uvazovací	k2eAgInPc2d1	uvazovací
kruhů	kruh	k1gInPc2	kruh
atd.	atd.	kA	atd.
Digitální	digitální	k2eAgFnSc1d1	digitální
kilometráž	kilometráž	k1gFnSc1	kilometráž
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
digitálně	digitálně	k6eAd1	digitálně
zkonstruované	zkonstruovaný	k2eAgFnSc2d1	zkonstruovaná
osy	osa	k1gFnSc2	osa
vodního	vodní	k2eAgInSc2d1	vodní
toku	tok	k1gInSc2	tok
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
jen	jen	k9	jen
pro	pro	k7c4	pro
hydrotechnické	hydrotechnický	k2eAgInPc4d1	hydrotechnický
výpočty	výpočet	k1gInPc4	výpočet
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
značení	značení	k1gNnSc4	značení
v	v	k7c6	v
terénu	terén	k1gInSc6	terén
a	a	k8xC	a
evidenci	evidence	k1gFnSc3	evidence
objektů	objekt	k1gInPc2	objekt
je	být	k5eAaImIp3nS	být
kvůli	kvůli	k7c3	kvůli
proměnlivosti	proměnlivost	k1gFnSc3	proměnlivost
nevhodná	vhodný	k2eNgFnSc1d1	nevhodná
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
kilometráž	kilometráž	k1gFnSc1	kilometráž
má	mít	k5eAaImIp3nS	mít
nulu	nula	k1gFnSc4	nula
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
vyústění	vyústění	k1gNnSc2	vyústění
řeky	řeka	k1gFnSc2	řeka
Labe	Labe	k1gNnSc2	Labe
do	do	k7c2	do
Severního	severní	k2eAgNnSc2d1	severní
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
směřuje	směřovat	k5eAaImIp3nS	směřovat
proti	proti	k7c3	proti
toku	tok	k1gInSc3	tok
až	až	k9	až
k	k	k7c3	k
prameni	pramen	k1gInSc3	pramen
Labe	Labe	k1gNnSc2	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
kilometráž	kilometráž	k1gFnSc1	kilometráž
začíná	začínat	k5eAaImIp3nS	začínat
u	u	k7c2	u
severního	severní	k2eAgInSc2d1	severní
patníku	patník	k1gInSc2	patník
státní	státní	k2eAgFnSc2d1	státní
hranice	hranice	k1gFnSc2	hranice
<g/>
,	,	kIx,	,
v	v	k7c6	v
řkm	řkm	k?	řkm
726,26	[number]	k4	726,26
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jako	jako	k9	jako
předávací	předávací	k2eAgInSc1d1	předávací
bod	bod	k1gInSc1	bod
byl	být	k5eAaImAgInS	být
ČR	ČR	kA	ČR
a	a	k8xC	a
SRN	SRN	kA	SRN
dohodnut	dohodnut	k2eAgInSc4d1	dohodnut
km	km	kA	km
730	[number]	k4	730
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
dosavadního	dosavadní	k2eAgInSc2d1	dosavadní
německého	německý	k2eAgInSc2d1	německý
kilometru	kilometr	k1gInSc2	kilometr
0	[number]	k4	0
<g/>
,	,	kIx,	,
u	u	k7c2	u
jižního	jižní	k2eAgInSc2d1	jižní
patníku	patník	k1gInSc2	patník
státní	státní	k2eAgFnSc2d1	státní
hranice	hranice	k1gFnSc2	hranice
<g/>
;	;	kIx,	;
dosavadní	dosavadní	k2eAgFnSc3d1	dosavadní
nule	nula	k1gFnSc3	nula
u	u	k7c2	u
Mělníka	Mělník	k1gInSc2	Mělník
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
hodnota	hodnota	k1gFnSc1	hodnota
837,37	[number]	k4	837,37
a	a	k8xC	a
přístavu	přístav	k1gInSc2	přístav
Chvaletice	Chvaletice	k1gFnPc1	Chvaletice
940,15	[number]	k4	940,15
(	(	kIx(	(
<g/>
940,20	[number]	k4	940,20
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
osa	osa	k1gFnSc1	osa
kilometráže	kilometráž	k1gFnSc2	kilometráž
byla	být	k5eAaImAgFnS	být
definována	definovat	k5eAaBmNgFnS	definovat
jako	jako	k9	jako
osa	osa	k1gFnSc1	osa
plavební	plavební	k2eAgFnPc1d1	plavební
dráhy	dráha	k1gFnPc1	dráha
a	a	k8xC	a
v	v	k7c6	v
terénu	terén	k1gInSc6	terén
bude	být	k5eAaImBp3nS	být
až	až	k9	až
po	po	k7c6	po
Kunětice	Kunětika	k1gFnSc6	Kunětika
vyznačena	vyznačen	k2eAgFnSc1d1	vyznačena
novými	nový	k2eAgFnPc7d1	nová
tabulemi	tabule	k1gFnPc7	tabule
(	(	kIx(	(
<g/>
kilometrovníky	kilometrovník	k1gInPc1	kilometrovník
<g/>
,	,	kIx,	,
půlkilometrovníky	půlkilometrovník	k1gInPc1	půlkilometrovník
a	a	k8xC	a
hektometrovníky	hektometrovník	k1gInPc1	hektometrovník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
přednostně	přednostně	k6eAd1	přednostně
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
<g/>
,	,	kIx,	,
a	a	k8xC	a
kde	kde	k6eAd1	kde
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
na	na	k7c6	na
pravém	pravý	k2eAgNnSc6d1	pravé
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
přeznačeny	přeznačen	k2eAgFnPc4d1	přeznačena
kilometráže	kilometráž	k1gFnPc4	kilometráž
velínů	velín	k1gInPc2	velín
plavebních	plavební	k2eAgFnPc2d1	plavební
komor	komora	k1gFnPc2	komora
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
kilometráž	kilometráž	k1gFnSc1	kilometráž
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
úseku	úsek	k1gInSc6	úsek
platná	platný	k2eAgFnSc1d1	platná
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
přeznačování	přeznačování	k1gNnSc1	přeznačování
bylo	být	k5eAaImAgNnS	být
zahájeno	zahájit	k5eAaPmNgNnS	zahájit
přípravnými	přípravný	k2eAgFnPc7d1	přípravná
pracemi	práce	k1gFnPc7	práce
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
a	a	k8xC	a
fakticky	fakticky	k6eAd1	fakticky
začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Kilometráž	kilometráž	k1gFnSc1	kilometráž
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
na	na	k7c6	na
základě	základ	k1gInSc6	základ
požadavku	požadavek	k1gInSc2	požadavek
Směrnice	směrnice	k1gFnSc2	směrnice
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
a	a	k8xC	a
Rady	rada	k1gFnSc2	rada
2005	[number]	k4	2005
<g/>
/	/	kIx~	/
<g/>
44	[number]	k4	44
<g/>
/	/	kIx~	/
<g/>
ES	ES	kA	ES
ze	z	k7c2	z
dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2005	[number]	k4	2005
o	o	k7c6	o
harmonizovaných	harmonizovaný	k2eAgFnPc6d1	harmonizovaná
říčních	říční	k2eAgFnPc6d1	říční
informačních	informační	k2eAgFnPc6d1	informační
službách	služba	k1gFnPc6	služba
(	(	kIx(	(
<g/>
RIS	RIS	kA	RIS
<g/>
)	)	kIx)	)
na	na	k7c6	na
vnitrozemských	vnitrozemský	k2eAgFnPc6d1	vnitrozemská
vodních	vodní	k2eAgFnPc6d1	vodní
cestách	cesta	k1gFnPc6	cesta
ve	v	k7c6	v
Společenství	společenství	k1gNnSc6	společenství
<g/>
.	.	kIx.	.
</s>
<s>
Německý	německý	k2eAgInSc1d1	německý
úsek	úsek	k1gInSc1	úsek
zůstane	zůstat	k5eAaPmIp3nS	zůstat
prozatím	prozatím	k6eAd1	prozatím
značen	značit	k5eAaImNgInS	značit
po	po	k7c6	po
proudu	proud	k1gInSc6	proud
od	od	k7c2	od
jižního	jižní	k2eAgInSc2d1	jižní
patníku	patník	k1gInSc2	patník
státní	státní	k2eAgFnSc2d1	státní
hranice	hranice	k1gFnSc2	hranice
<g/>
;	;	kIx,	;
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
po	po	k7c6	po
Labi	Labe	k1gNnSc6	Labe
vede	vést	k5eAaImIp3nS	vést
státní	státní	k2eAgFnSc1d1	státní
hranice	hranice	k1gFnSc1	hranice
<g/>
,	,	kIx,	,
zůstane	zůstat	k5eAaPmIp3nS	zůstat
jen	jen	k9	jen
německé	německý	k2eAgNnSc1d1	německé
značení	značení	k1gNnSc1	značení
<g/>
.	.	kIx.	.
</s>
<s>
Plavební	plavební	k2eAgInPc1d1	plavební
kanály	kanál	k1gInPc1	kanál
mají	mít	k5eAaImIp3nP	mít
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
kilometráž	kilometráž	k1gFnSc4	kilometráž
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
vedena	vést	k5eAaImNgFnS	vést
od	od	k7c2	od
špičky	špička	k1gFnSc2	špička
ostrova	ostrov	k1gInSc2	ostrov
směrem	směr	k1gInSc7	směr
proti	proti	k7c3	proti
toku	tok	k1gInSc3	tok
řeky	řeka	k1gFnSc2	řeka
a	a	k8xC	a
zpravidla	zpravidla	k6eAd1	zpravidla
vyznačena	vyznačen	k2eAgFnSc1d1	vyznačena
i	i	k9	i
v	v	k7c6	v
terénu	terén	k1gInSc6	terén
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
betonovými	betonový	k2eAgInPc7d1	betonový
pásky	pásek	k1gInPc7	pásek
v	v	k7c6	v
břehovém	břehový	k2eAgNnSc6d1	břehové
opevnění	opevnění	k1gNnSc6	opevnění
<g/>
.	.	kIx.	.
</s>
<s>
Pramení	pramenit	k5eAaImIp3nS	pramenit
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
1387	[number]	k4	1387
m	m	kA	m
v	v	k7c6	v
rašeliništi	rašeliniště	k1gNnSc6	rašeliniště
na	na	k7c6	na
Labské	labský	k2eAgFnSc6d1	Labská
louce	louka	k1gFnSc6	louka
<g/>
,	,	kIx,	,
v	v	k7c6	v
těsném	těsný	k2eAgNnSc6d1	těsné
sousedství	sousedství	k1gNnSc6	sousedství
státní	státní	k2eAgFnSc2d1	státní
hranice	hranice	k1gFnSc2	hranice
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
Labskou	labský	k2eAgFnSc7d1	Labská
boudou	bouda	k1gFnSc7	bouda
spadá	spadat	k5eAaPmIp3nS	spadat
Labským	labský	k2eAgInSc7d1	labský
vodopádem	vodopád	k1gInSc7	vodopád
do	do	k7c2	do
Labského	labský	k2eAgInSc2d1	labský
dolu	dol	k1gInSc2	dol
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
městy	město	k1gNnPc7	město
Špindlerův	Špindlerův	k2eAgInSc4d1	Špindlerův
Mlýn	mlýn	k1gInSc4	mlýn
a	a	k8xC	a
Vrchlabí	Vrchlabí	k1gNnSc1	Vrchlabí
protéká	protékat	k5eAaImIp3nS	protékat
úzkou	úzký	k2eAgFnSc7d1	úzká
Labskou	labský	k2eAgFnSc7d1	Labská
soutěskou	soutěska	k1gFnSc7	soutěska
<g/>
,	,	kIx,	,
víceméně	víceméně	k9	víceméně
jižním	jižní	k2eAgInSc7d1	jižní
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
po	po	k7c4	po
Jaroměř	Jaroměř	k1gFnSc4	Jaroměř
je	být	k5eAaImIp3nS	být
charakter	charakter	k1gInSc1	charakter
krajiny	krajina	k1gFnSc2	krajina
podhorský	podhorský	k2eAgMnSc1d1	podhorský
a	a	k8xC	a
řeka	řeka	k1gFnSc1	řeka
teče	téct	k5eAaImIp3nS	téct
směrem	směr	k1gInSc7	směr
jihovýchodním	jihovýchodní	k2eAgInSc7d1	jihovýchodní
<g/>
.	.	kIx.	.
</s>
<s>
Významnějšími	významný	k2eAgInPc7d2	významnější
přítoky	přítok	k1gInPc7	přítok
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
horním	horní	k2eAgInSc6d1	horní
toku	tok	k1gInSc6	tok
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
Bílé	bílý	k2eAgNnSc4d1	bílé
Labe	Labe	k1gNnSc4	Labe
<g/>
,	,	kIx,	,
Malé	Malé	k2eAgNnSc4d1	Malé
Labe	Labe	k1gNnSc4	Labe
<g/>
,	,	kIx,	,
Čistá	čistá	k1gFnSc1	čistá
a	a	k8xC	a
Pilníkovský	Pilníkovský	k2eAgInSc1d1	Pilníkovský
potok	potok	k1gInSc1	potok
(	(	kIx(	(
<g/>
všechny	všechen	k3xTgFnPc4	všechen
levostranné	levostranný	k2eAgFnPc4d1	levostranná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	nedaleko	k7c2	nedaleko
Dvora	Dvůr	k1gInSc2	Dvůr
Králové	Králová	k1gFnSc2	Králová
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
přehrada	přehrada	k1gFnSc1	přehrada
Les	les	k1gInSc4	les
Království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
pozoruhodná	pozoruhodný	k2eAgFnSc1d1	pozoruhodná
zejména	zejména	k6eAd1	zejména
stylovým	stylový	k2eAgNnPc3d1	stylové
stavebním	stavební	k2eAgNnPc3d1	stavební
provedením	provedení	k1gNnPc3	provedení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jaroměři	Jaroměř	k1gFnSc6	Jaroměř
se	se	k3xPyFc4	se
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
260	[number]	k4	260
m	m	kA	m
do	do	k7c2	do
Labe	Labe	k1gNnSc2	Labe
zleva	zleva	k6eAd1	zleva
vlévá	vlévat	k5eAaImIp3nS	vlévat
řeka	řeka	k1gFnSc1	řeka
Úpa	Úpa	k1gFnSc1	Úpa
a	a	k8xC	a
nedaleko	daleko	k6eNd1	daleko
pod	pod	k7c7	pod
ní	on	k3xPp3gFnSc7	on
<g/>
,	,	kIx,	,
u	u	k7c2	u
Josefova	Josefov	k1gInSc2	Josefov
<g/>
,	,	kIx,	,
řeka	řeka	k1gFnSc1	řeka
Metuje	Metuje	k1gFnSc1	Metuje
<g/>
.	.	kIx.	.
</s>
<s>
Údolí	údolí	k1gNnSc1	údolí
řeky	řeka	k1gFnSc2	řeka
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
a	a	k8xC	a
až	až	k9	až
po	po	k7c4	po
soutok	soutok	k1gInSc4	soutok
s	s	k7c7	s
Ohří	Ohře	k1gFnSc7	Ohře
u	u	k7c2	u
Litoměřic	Litoměřice	k1gInPc2	Litoměřice
se	se	k3xPyFc4	se
obecně	obecně	k6eAd1	obecně
nazývá	nazývat	k5eAaImIp3nS	nazývat
Polabím	Polabí	k1gNnSc7	Polabí
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
levými	levý	k2eAgInPc7d1	levý
přítoky	přítok	k1gInPc7	přítok
jsou	být	k5eAaImIp3nP	být
Orlice	Orlice	k1gFnPc4	Orlice
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
(	(	kIx(	(
<g/>
ve	v	k7c6	v
městě	město	k1gNnSc6	město
pozoruhodná	pozoruhodný	k2eAgFnSc1d1	pozoruhodná
vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
s	s	k7c7	s
mostem	most	k1gInSc7	most
a	a	k8xC	a
oblíbené	oblíbený	k2eAgFnPc4d1	oblíbená
Jiráskovy	Jiráskův	k2eAgFnPc4d1	Jiráskova
sady	sada	k1gFnPc4	sada
s	s	k7c7	s
přeneseným	přenesený	k2eAgInSc7d1	přenesený
roubeným	roubený	k2eAgInSc7d1	roubený
kostelíkem	kostelík	k1gInSc7	kostelík
z	z	k7c2	z
východního	východní	k2eAgNnSc2d1	východní
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Loučná	Loučný	k2eAgFnSc1d1	Loučná
u	u	k7c2	u
Sezemic	Sezemice	k1gFnPc2	Sezemice
nedaleko	nedaleko	k7c2	nedaleko
Kunětické	kunětický	k2eAgFnSc2d1	Kunětická
hory	hora	k1gFnSc2	hora
a	a	k8xC	a
Chrudimka	Chrudimka	k1gFnSc1	Chrudimka
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Jaroměře	Jaroměř	k1gFnSc2	Jaroměř
do	do	k7c2	do
Pardubic	Pardubice	k1gInPc2	Pardubice
sleduje	sledovat	k5eAaImIp3nS	sledovat
tok	tok	k1gInSc1	tok
řeky	řeka	k1gFnSc2	řeka
Labe	Labe	k1gNnSc2	Labe
obecně	obecně	k6eAd1	obecně
jižní	jižní	k2eAgInSc1d1	jižní
směr	směr	k1gInSc1	směr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
se	se	k3xPyFc4	se
řeka	řeka	k1gFnSc1	řeka
stáčí	stáčet	k5eAaImIp3nS	stáčet
k	k	k7c3	k
západu	západ	k1gInSc2	západ
a	a	k8xC	a
tímto	tento	k3xDgInSc7	tento
směrem	směr	k1gInSc7	směr
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
až	až	k9	až
do	do	k7c2	do
Kolína	Kolín	k1gInSc2	Kolín
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
opět	opět	k6eAd1	opět
mění	měnit	k5eAaImIp3nP	měnit
směr	směr	k1gInSc4	směr
k	k	k7c3	k
severozápadu	severozápad	k1gInSc3	severozápad
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
směrem	směr	k1gInSc7	směr
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
obecně	obecně	k6eAd1	obecně
ubírá	ubírat	k5eAaImIp3nS	ubírat
až	až	k9	až
k	k	k7c3	k
Ústí	ústí	k1gNnSc3	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Chvaletic	Chvaletice	k1gFnPc2	Chvaletice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
přístav	přístav	k1gInSc1	přístav
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
sloužící	sloužící	k1gFnSc1	sloužící
dovozu	dovoz	k1gInSc2	dovoz
paliva	palivo	k1gNnSc2	palivo
pro	pro	k7c4	pro
místní	místní	k2eAgFnSc4d1	místní
uhelnou	uhelný	k2eAgFnSc4d1	uhelná
elektrárnu	elektrárna	k1gFnSc4	elektrárna
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
výrazně	výrazně	k6eAd1	výrazně
regulována	regulován	k2eAgFnSc1d1	regulována
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
Pardubicemi	Pardubice	k1gInPc7	Pardubice
a	a	k8xC	a
Mělníkem	Mělník	k1gInSc7	Mělník
též	též	k6eAd1	též
přibírá	přibírat	k5eAaImIp3nS	přibírat
několik	několik	k4yIc1	několik
menších	malý	k2eAgInPc2d2	menší
levostranných	levostranný	k2eAgInPc2d1	levostranný
přítoků	přítok	k1gInPc2	přítok
<g/>
:	:	kIx,	:
Doubravu	Doubrava	k1gFnSc4	Doubrava
u	u	k7c2	u
Záboří	Záboří	k1gNnSc2	Záboří
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
;	;	kIx,	;
Klejnárku	Klejnárka	k1gFnSc4	Klejnárka
u	u	k7c2	u
Starého	Starého	k2eAgInSc2d1	Starého
Kolína	Kolín	k1gInSc2	Kolín
<g/>
;	;	kIx,	;
Výrovku	výrovka	k1gFnSc4	výrovka
nedaleko	nedaleko	k7c2	nedaleko
Nymburka	Nymburk	k1gInSc2	Nymburk
<g/>
;	;	kIx,	;
a	a	k8xC	a
Výmolu	výmol	k1gInSc2	výmol
před	před	k7c7	před
Čelákovicemi	Čelákovice	k1gFnPc7	Čelákovice
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
významným	významný	k2eAgInSc7d1	významný
pravostranným	pravostranný	k2eAgInSc7d1	pravostranný
přítokem	přítok	k1gInSc7	přítok
Labe	Labe	k1gNnSc2	Labe
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
řeka	řeka	k1gFnSc1	řeka
Cidlina	Cidlina	k1gFnSc1	Cidlina
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
tohoto	tento	k3xDgInSc2	tento
soutoku	soutok	k1gInSc2	soutok
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
národní	národní	k2eAgFnSc2d1	národní
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
Libický	Libický	k2eAgInSc1d1	Libický
luh	luh	k1gInSc1	luh
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
představuje	představovat	k5eAaImIp3nS	představovat
největší	veliký	k2eAgInSc4d3	veliký
souvislý	souvislý	k2eAgInSc4d1	souvislý
komplex	komplex	k1gInSc4	komplex
úvalového	úvalový	k2eAgInSc2d1	úvalový
lužního	lužní	k2eAgInSc2d1	lužní
lesa	les	k1gInSc2	les
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
(	(	kIx(	(
<g/>
asi	asi	k9	asi
500	[number]	k4	500
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Nymburce	Nymburk	k1gInSc6	Nymburk
se	se	k3xPyFc4	se
do	do	k7c2	do
Labe	Labe	k1gNnSc2	Labe
zprava	zprava	k6eAd1	zprava
vlévá	vlévat	k5eAaImIp3nS	vlévat
říčka	říčka	k1gFnSc1	říčka
Mrlina	Mrlina	k1gFnSc1	Mrlina
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
kolem	kolem	k7c2	kolem
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
tvoří	tvořit	k5eAaImIp3nS	tvořit
oblouk	oblouk	k1gInSc1	oblouk
připomínající	připomínající	k2eAgInSc1d1	připomínající
vodní	vodní	k2eAgInSc1d1	vodní
příkop	příkop	k1gInSc1	příkop
a	a	k8xC	a
jež	jenž	k3xRgFnSc1	jenž
chránila	chránit	k5eAaImAgFnS	chránit
zdejší	zdejší	k2eAgFnSc4d1	zdejší
původní	původní	k2eAgFnSc4d1	původní
slovanskou	slovanský	k2eAgFnSc4d1	Slovanská
osadu	osada	k1gFnSc4	osada
Ústí	ústí	k1gNnSc2	ústí
(	(	kIx(	(
<g/>
Usk	Usk	k1gFnSc1	Usk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
soutoku	soutok	k1gInSc6	soutok
s	s	k7c7	s
pravostrannou	pravostranný	k2eAgFnSc7d1	pravostranná
Jizerou	Jizera	k1gFnSc7	Jizera
poblíž	poblíž	k6eAd1	poblíž
Staré	Staré	k2eAgFnSc6d1	Staré
Boleslavi	Boleslaev	k1gFnSc6	Boleslaev
se	se	k3xPyFc4	se
prostírají	prostírat	k5eAaImIp3nP	prostírat
rozsáhlé	rozsáhlý	k2eAgInPc1d1	rozsáhlý
lesy	les	k1gInPc1	les
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
leží	ležet	k5eAaImIp3nS	ležet
i	i	k9	i
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
zdrojů	zdroj	k1gInPc2	zdroj
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
pro	pro	k7c4	pro
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Prahu	Praha	k1gFnSc4	Praha
<g/>
,	,	kIx,	,
vodárenský	vodárenský	k2eAgInSc4d1	vodárenský
komplex	komplex	k1gInSc4	komplex
Káraný	káraný	k2eAgInSc4d1	káraný
<g/>
,	,	kIx,	,
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
soutoku	soutok	k1gInSc2	soutok
Labe	Labe	k1gNnSc2	Labe
s	s	k7c7	s
Vltavou	Vltava	k1gFnSc7	Vltava
se	se	k3xPyFc4	se
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Mělníka	Mělník	k1gInSc2	Mělník
vlévají	vlévat	k5eAaImIp3nP	vlévat
též	též	k9	též
dva	dva	k4xCgInPc4	dva
pravostranné	pravostranný	k2eAgInPc4d1	pravostranný
přítoky	přítok	k1gInPc4	přítok
regionálního	regionální	k2eAgNnSc2d1	regionální
<g/>
,	,	kIx,	,
především	především	k9	především
turistického	turistický	k2eAgInSc2d1	turistický
a	a	k8xC	a
vodárenského	vodárenský	k2eAgInSc2d1	vodárenský
významu	význam	k1gInSc2	význam
<g/>
:	:	kIx,	:
říčky	říčka	k1gFnSc2	říčka
Pšovka	Pšovka	k1gMnSc1	Pšovka
(	(	kIx(	(
<g/>
protéká	protékat	k5eAaImIp3nS	protékat
Kokořínským	Kokořínský	k2eAgInSc7d1	Kokořínský
dolem	dol	k1gInSc7	dol
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
osu	osa	k1gFnSc4	osa
Chráněné	chráněný	k2eAgFnSc2d1	chráněná
krajinné	krajinný	k2eAgFnSc2d1	krajinná
oblasti	oblast	k1gFnSc2	oblast
Kokořínsko	Kokořínsko	k1gNnSc1	Kokořínsko
<g/>
)	)	kIx)	)
a	a	k8xC	a
Liběchovka	Liběchovka	k1gFnSc1	Liběchovka
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
říčky	říčka	k1gFnPc1	říčka
tvoří	tvořit	k5eAaImIp3nP	tvořit
výrazná	výrazný	k2eAgNnPc1d1	výrazné
hluboká	hluboký	k2eAgNnPc1d1	hluboké
údolí	údolí	k1gNnPc1	údolí
s	s	k7c7	s
rozsáhlými	rozsáhlý	k2eAgInPc7d1	rozsáhlý
luhy	luh	k1gInPc7	luh
<g/>
.	.	kIx.	.
</s>
<s>
Širé	širý	k2eAgNnSc1d1	širé
a	a	k8xC	a
úrodné	úrodný	k2eAgNnSc1d1	úrodné
Polabí	Polabí	k1gNnSc1	Polabí
uzavírá	uzavírat	k5eAaImIp3nS	uzavírat
soutok	soutok	k1gInSc4	soutok
Labe	Labe	k1gNnSc2	Labe
s	s	k7c7	s
Ohří	Ohře	k1gFnSc7	Ohře
proti	proti	k7c3	proti
Litoměřicím	Litoměřice	k1gInPc3	Litoměřice
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Lovosic	Lovosice	k1gInPc2	Lovosice
se	se	k3xPyFc4	se
Labe	Labe	k1gNnSc2	Labe
noří	nořit	k5eAaImIp3nP	nořit
do	do	k7c2	do
hlubokého	hluboký	k2eAgInSc2d1	hluboký
kaňonu	kaňon	k1gInSc2	kaňon
<g/>
,	,	kIx,	,
zvaného	zvaný	k2eAgInSc2d1	zvaný
Česká	český	k2eAgFnSc1d1	Česká
brána	brána	k1gFnSc1	brána
(	(	kIx(	(
<g/>
Porta	porta	k1gFnSc1	porta
Bohemica	Bohemica	k1gMnSc1	Bohemica
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
prochází	procházet	k5eAaImIp3nS	procházet
napříč	napříč	k7c7	napříč
Českým	český	k2eAgNnSc7d1	české
středohořím	středohoří	k1gNnSc7	středohoří
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
u	u	k7c2	u
Libochovan	Libochovan	k1gMnSc1	Libochovan
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Labe	Labe	k1gNnSc4	Labe
na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
nejširší	široký	k2eAgInPc1d3	nejširší
-	-	kIx~	-
až	až	k9	až
320	[number]	k4	320
m.	m.	k?	m.
V	v	k7c6	v
Ústí	ústí	k1gNnSc6	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
řeka	řeka	k1gFnSc1	řeka
přibírá	přibírat	k5eAaImIp3nS	přibírat
levostranný	levostranný	k2eAgInSc4d1	levostranný
přítok	přítok	k1gInSc4	přítok
Bílinu	Bílina	k1gFnSc4	Bílina
<g/>
;	;	kIx,	;
v	v	k7c6	v
Děčíně	Děčín	k1gInSc6	Děčín
zprava	zprava	k6eAd1	zprava
Ploučnici	Ploučnice	k1gFnSc3	Ploučnice
a	a	k8xC	a
v	v	k7c6	v
Hřensku	Hřensko	k1gNnSc6	Hřensko
zprava	zprava	k6eAd1	zprava
Kamenici	Kamenice	k1gFnSc4	Kamenice
<g/>
.	.	kIx.	.
</s>
<s>
Poměrně	poměrně	k6eAd1	poměrně
úzkým	úzký	k2eAgNnSc7d1	úzké
a	a	k8xC	a
sevřeným	sevřený	k2eAgNnSc7d1	sevřené
údolím	údolí	k1gNnSc7	údolí
pak	pak	k6eAd1	pak
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
až	až	k9	až
ke	k	k7c3	k
státní	státní	k2eAgFnSc3d1	státní
hranici	hranice	k1gFnSc3	hranice
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
u	u	k7c2	u
Hřenska	Hřensko	k1gNnSc2	Hřensko
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
je	být	k5eAaImIp3nS	být
nejníže	nízce	k6eAd3	nízce
položeným	položený	k2eAgInSc7d1	položený
bodem	bod	k1gInSc7	bod
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
115	[number]	k4	115
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
Labe	Labe	k1gNnSc2	Labe
opouští	opouštět	k5eAaImIp3nS	opouštět
hlubokou	hluboký	k2eAgFnSc7d1	hluboká
roklí	rokle	k1gFnSc7	rokle
tvořenou	tvořený	k2eAgFnSc7d1	tvořená
Labskými	labský	k2eAgInPc7d1	labský
pískovci	pískovec	k1gInPc7	pískovec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
straně	strana	k1gFnSc6	strana
Labe	Labe	k1gNnSc2	Labe
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
údolím	údolí	k1gNnSc7	údolí
Saského	saský	k2eAgNnSc2d1	Saské
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
přes	přes	k7c4	přes
Pirnu	Pirna	k1gFnSc4	Pirna
severozápadním	severozápadní	k2eAgInSc7d1	severozápadní
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Pirny	Pirna	k1gFnSc2	Pirna
se	se	k3xPyFc4	se
Labská	labský	k2eAgFnSc1d1	Labská
kotlina	kotlina	k1gFnSc1	kotlina
začíná	začínat	k5eAaImIp3nS	začínat
rozšiřovat	rozšiřovat	k5eAaImF	rozšiřovat
<g/>
,	,	kIx,	,
řeka	řeka	k1gFnSc1	řeka
protéká	protékat	k5eAaImIp3nS	protékat
Drážďany	Drážďany	k1gInPc4	Drážďany
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
široká	široký	k2eAgFnSc1d1	široká
100	[number]	k4	100
až	až	k9	až
150	[number]	k4	150
m	m	kA	m
<g/>
,	,	kIx,	,
Míšní	míšní	k2eAgFnSc1d1	míšní
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
cestě	cesta	k1gFnSc6	cesta
vstupem	vstup	k1gInSc7	vstup
do	do	k7c2	do
Severoněmecké	severoněmecký	k2eAgFnSc2d1	Severoněmecká
nížiny	nížina	k1gFnSc2	nížina
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
četné	četný	k2eAgInPc4d1	četný
meandry	meandr	k1gInPc4	meandr
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
takto	takto	k6eAd1	takto
formují	formovat	k5eAaImIp3nP	formovat
tok	tok	k1gInSc4	tok
až	až	k9	až
k	k	k7c3	k
Hamburku	Hamburk	k1gInSc3	Hamburk
<g/>
.	.	kIx.	.
</s>
<s>
Labe	Labe	k1gNnSc1	Labe
protéká	protékat	k5eAaImIp3nS	protékat
městy	město	k1gNnPc7	město
Torgau	Torgaus	k1gInSc2	Torgaus
<g/>
,	,	kIx,	,
Dessau	Dessaa	k1gFnSc4	Dessaa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
části	část	k1gFnSc6	část
se	se	k3xPyFc4	se
do	do	k7c2	do
Labe	Labe	k1gNnSc2	Labe
vlévají	vlévat	k5eAaImIp3nP	vlévat
její	její	k3xOp3gInPc1	její
nejmohutnější	mohutný	k2eAgInPc1d3	nejmohutnější
levostranné	levostranný	k2eAgInPc1d1	levostranný
přítoky	přítok	k1gInPc1	přítok
<g/>
,	,	kIx,	,
Mulda	mulda	k1gFnSc1	mulda
a	a	k8xC	a
Sála	Sála	k1gFnSc1	Sála
<g/>
.	.	kIx.	.
</s>
<s>
Severně	severně	k6eAd1	severně
od	od	k7c2	od
Magdeburgu	Magdeburg	k1gInSc2	Magdeburg
se	se	k3xPyFc4	se
Labe	Labe	k1gNnSc1	Labe
pravostranně	pravostranně	k6eAd1	pravostranně
propojuje	propojovat	k5eAaImIp3nS	propojovat
plavebním	plavební	k2eAgInSc7d1	plavební
kanálem	kanál	k1gInSc7	kanál
s	s	k7c7	s
Havolou	Havola	k1gFnSc7	Havola
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
teče	téct	k5eAaImIp3nS	téct
několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
km	km	kA	km
severním	severní	k2eAgInSc7d1	severní
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
pravostranném	pravostranný	k2eAgInSc6d1	pravostranný
soutoku	soutok	k1gInSc6	soutok
s	s	k7c7	s
Havolou	Havola	k1gFnSc7	Havola
opět	opět	k6eAd1	opět
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
severozápadní	severozápadní	k2eAgFnPc4d1	severozápadní
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
městem	město	k1gNnSc7	město
Wittenberge	Wittenberg	k1gInSc2	Wittenberg
tvořila	tvořit	k5eAaImAgFnS	tvořit
řeka	řeka	k1gFnSc1	řeka
na	na	k7c6	na
dvou	dva	k4xCgNnPc6	dva
místech	místo	k1gNnPc6	místo
střeženou	střežený	k2eAgFnSc4d1	střežená
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
bývalým	bývalý	k2eAgNnSc7d1	bývalé
Východním	východní	k2eAgNnSc7d1	východní
a	a	k8xC	a
Západním	západní	k2eAgNnSc7d1	západní
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
Hamburgem	Hamburg	k1gInSc7	Hamburg
se	se	k3xPyFc4	se
řeka	řeka	k1gFnSc1	řeka
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
na	na	k7c4	na
Severní	severní	k2eAgNnSc4d1	severní
Labe	Labe	k1gNnSc4	Labe
<g/>
,	,	kIx,	,
protékající	protékající	k2eAgFnSc4d1	protékající
přes	přes	k7c4	přes
hamburský	hamburský	k2eAgInSc4d1	hamburský
přístav	přístav	k1gInSc4	přístav
a	a	k8xC	a
Jižní	jižní	k2eAgNnSc4d1	jižní
Labe	Labe	k1gNnSc4	Labe
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
přístavu	přístav	k1gInSc2	přístav
vyhýbá	vyhýbat	k5eAaImIp3nS	vyhýbat
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
tak	tak	k6eAd1	tak
snadnější	snadný	k2eAgNnSc4d2	snazší
proplutí	proplutí	k1gNnSc4	proplutí
lodí	loď	k1gFnPc2	loď
přes	přes	k7c4	přes
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Labe	Labe	k1gNnSc1	Labe
pak	pak	k6eAd1	pak
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
severozápad	severozápad	k1gInSc4	severozápad
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
koryto	koryto	k1gNnSc1	koryto
široké	široký	k2eAgNnSc1d1	široké
300	[number]	k4	300
až	až	k9	až
500	[number]	k4	500
m	m	kA	m
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
až	až	k8xS	až
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c4	v
estuár	estuár	k1gInSc4	estuár
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
pravostrannou	pravostranný	k2eAgFnSc7d1	pravostranná
spojkou	spojka	k1gFnSc7	spojka
je	být	k5eAaImIp3nS	být
plavební	plavební	k2eAgInSc1d1	plavební
kanál	kanál	k1gInSc1	kanál
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k1gFnSc1	vedoucí
přes	přes	k7c4	přes
Kiel	Kiel	k1gInSc4	Kiel
do	do	k7c2	do
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
přístavu	přístav	k1gInSc2	přístav
Cuxhaven	Cuxhavna	k1gFnPc2	Cuxhavna
ústí	ústit	k5eAaImIp3nS	ústit
Labe	Labe	k1gNnSc4	Labe
do	do	k7c2	do
Severního	severní	k2eAgNnSc2d1	severní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Estuár	estuár	k1gInSc1	estuár
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
zhruba	zhruba	k6eAd1	zhruba
100	[number]	k4	100
km	km	kA	km
a	a	k8xC	a
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
šířky	šířka	k1gFnSc2	šířka
17	[number]	k4	17
km	km	kA	km
<g/>
.	.	kIx.	.
zleva	zleva	k6eAd1	zleva
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
-	-	kIx~	-
Mratínský	Mratínský	k2eAgInSc1d1	Mratínský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Podlava	Podlava	k1gFnSc1	Podlava
<g/>
,	,	kIx,	,
Dvorský	dvorský	k2eAgInSc1d1	dvorský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Medvědí	medvědí	k2eAgInSc1d1	medvědí
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Bílé	bílý	k2eAgNnSc1d1	bílé
Labe	Labe	k1gNnSc1	Labe
<g/>
,	,	kIx,	,
Svatopetrský	svatopetrský	k2eAgInSc1d1	svatopetrský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Tabulový	tabulový	k2eAgInSc1d1	tabulový
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Dřevařský	dřevařský	k2eAgInSc1d1	dřevařský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Richterova	Richterův	k2eAgFnSc1d1	Richterova
strouha	strouha	k1gFnSc1	strouha
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Vápenický	vápenický	k2eAgInSc4d1	vápenický
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
Malé	Malé	k2eAgNnSc4d1	Malé
Labe	Labe	k1gNnSc4	Labe
<g/>
,	,	kIx,	,
<g/>
Čistá	čistá	k1gFnSc1	čistá
<g/>
,	,	kIx,	,
Východní	východní	k2eAgInSc1d1	východní
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Pilníkovský	Pilníkovský	k2eAgInSc1d1	Pilníkovský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Kateřinský	kateřinský	k2eAgInSc1d1	kateřinský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
<g/>
Černý	černý	k2eAgInSc1d1	černý
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Hartský	Hartský	k2eAgInSc1d1	Hartský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Kocbeřský	Kocbeřský	k2eAgInSc1d1	Kocbeřský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Drahyně	Drahyně	k1gFnSc1	Drahyně
<g/>
,	,	kIx,	,
Běluňka	Běluňka	k1gFnSc1	Běluňka
<g/>
,	,	kIx,	,
Úpa	Úpa	k1gFnSc1	Úpa
<g/>
,	,	kIx,	,
Metuje	Metuje	k1gFnSc1	Metuje
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Smržovský	Smržovský	k2eAgInSc1d1	Smržovský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Malostranský	malostranský	k2eAgInSc1d1	malostranský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Orlice	Orlice	k1gFnSc1	Orlice
<g/>
,	,	kIx,	,
Piletický	Piletický	k2eAgInSc1d1	Piletický
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Biřička	biřička	k1gFnSc1	biřička
<g/>
,	,	kIx,	,
Bohumilečský	Bohumilečský	k2eAgInSc1d1	Bohumilečský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Ředický	Ředický	k2eAgInSc1d1	Ředický
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Loučná	Loučné	k1gNnPc1	Loučné
<g/>
,	,	kIx,	,
Chrudimka	Chrudimka	k1gFnSc1	Chrudimka
<g/>
,	,	kIx,	,
Jesenčanský	Jesenčanský	k2eAgInSc1d1	Jesenčanský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Bylanka	Bylanka	k1gFnSc1	Bylanka
<g/>
,	,	kIx,	,
Podolský	podolský	k2eAgInSc1d1	podolský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Lánský	lánský	k2eAgInSc1d1	lánský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Struha	struha	k1gFnSc1	struha
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Švarcava	Švarcava	k1gFnSc1	Švarcava
<g/>
,	,	kIx,	,
Brložský	Brložský	k2eAgInSc1d1	Brložský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Spytovický	Spytovický	k2eAgInSc1d1	Spytovický
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Morašický	Morašický	k2eAgInSc1d1	Morašický
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Doubrava	Doubrava	k1gFnSc1	Doubrava
<g/>
,	,	kIx,	,
Klejnárka	Klejnárka	k1gFnSc1	Klejnárka
<g/>
,	,	kIx,	,
Hořanský	Hořanský	k2eAgInSc1d1	Hořanský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Polepka	Polepka	k1gFnSc1	Polepka
<g/>
,	,	kIx,	,
Pekelský	pekelský	k2eAgInSc1d1	pekelský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Bedřichovská	Bedřichovský	k2eAgFnSc1d1	Bedřichovská
svodnice	svodnice	k1gFnSc1	svodnice
<g/>
,	,	kIx,	,
Nouzovský	Nouzovský	k2eAgInSc1d1	Nouzovský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Pňovka	Pňovka	k1gFnSc1	Pňovka
<g/>
,	,	kIx,	,
Klipecká	Klipecká	k1gFnSc1	Klipecká
<g/>
,	,	kIx,	,
Sokolečská	Sokolečský	k2eAgFnSc1d1	Sokolečský
strouha	strouha	k1gFnSc1	strouha
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Potůček	potůček	k1gInSc1	potůček
<g/>
,	,	kIx,	,
Výrovka	výrovka	k1gFnSc1	výrovka
<g/>
,	,	kIx,	,
Mlýnský	mlýnský	k2eAgInSc1d1	mlýnský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Smradlák	Smradlák	k1gInSc1	Smradlák
<g/>
,	,	kIx,	,
Kounický	Kounický	k2eAgInSc1d1	Kounický
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Výmola	Výmola	k1gFnSc1	Výmola
<g/>
,	,	kIx,	,
Mlýnský	mlýnský	k2eAgInSc1d1	mlýnský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Vltava	Vltava	k1gFnSc1	Vltava
<g/>
,	,	kIx,	,
Ohře	Ohře	k1gFnSc1	Ohře
<g/>
,	,	kIx,	,
Modla	modla	k1gFnSc1	modla
<g/>
,	,	kIx,	,
Bílina	Bílina	k1gFnSc1	Bílina
<g/>
,	,	kIx,	,
Jílovský	jílovský	k2eAgInSc1d1	jílovský
potok	potok	k1gInSc1	potok
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
-	-	kIx~	-
Biela	Biela	k1gFnSc1	Biela
<g/>
,	,	kIx,	,
Rybný	rybný	k2eAgInSc1d1	rybný
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Gottleuba	Gottleuba	k1gFnSc1	Gottleuba
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Müglitz	Müglitz	k1gMnSc1	Müglitz
<g/>
,	,	kIx,	,
Weißeritz	Weißeritz	k1gMnSc1	Weißeritz
<g/>
,	,	kIx,	,
Rehbockbach	Rehbockbach	k1gMnSc1	Rehbockbach
<g/>
,	,	kIx,	,
Triebisch	Triebisch	k1gMnSc1	Triebisch
<g/>
,	,	kIx,	,
Jahnabach	Jahnabach	k1gMnSc1	Jahnabach
<g/>
,	,	kIx,	,
Ketzerbach	Ketzerbach	k1gMnSc1	Ketzerbach
<g/>
,	,	kIx,	,
Mulda	mulda	k1gFnSc1	mulda
<g/>
,	,	kIx,	,
Sála	Sála	k1gFnSc1	Sála
<g/>
,	,	kIx,	,
Ohre	Ohre	k1gFnSc1	Ohre
<g/>
,	,	kIx,	,
Tanger	Tanger	k1gMnSc1	Tanger
<g/>
,	,	kIx,	,
Aland	Aland	k1gMnSc1	Aland
<g/>
,	,	kIx,	,
Jeetzel	Jeetzel	k1gMnSc1	Jeetzel
<g/>
,	,	kIx,	,
Ilmenau	Ilmenaus	k1gInSc2	Ilmenaus
<g/>
,	,	kIx,	,
Seeve	Seev	k1gMnSc5	Seev
<g/>
,	,	kIx,	,
Este	Est	k1gMnSc5	Est
<g/>
,	,	kIx,	,
Lühe	Lühus	k1gMnSc5	Lühus
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Schwinge	Schwinge	k1gFnSc7	Schwinge
<g/>
,	,	kIx,	,
Oste	Oste	k1gFnSc7	Oste
<g/>
,	,	kIx,	,
Medem	med	k1gInSc7	med
zprava	zprava	k6eAd1	zprava
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
-	-	kIx~	-
Pančava	pančava	k1gFnSc1	pančava
<g/>
,	,	kIx,	,
Žlábský	Žlábský	k2eAgInSc1d1	Žlábský
ručej	ručej	k1gInSc1	ručej
<g/>
,	,	kIx,	,
Korytová	korytový	k2eAgFnSc1d1	Korytová
strouha	strouha	k1gFnSc1	strouha
<g/>
,	,	kIx,	,
Medvědí	medvědí	k2eAgFnSc1d1	medvědí
ručej	ručej	k1gFnSc1	ručej
<g/>
,	,	kIx,	,
Krakonošova	Krakonošův	k2eAgFnSc1d1	Krakonošova
strouha	strouha	k1gFnSc1	strouha
<g/>
,	,	kIx,	,
Honzova	Honzův	k2eAgFnSc1d1	Honzova
strouha	strouha	k1gFnSc1	strouha
<g/>
,	,	kIx,	,
Vojákův	vojákův	k2eAgInSc1d1	vojákův
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Čerstvá	čerstvý	k2eAgFnSc1d1	čerstvá
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
Sachrův	Sachrův	k2eAgInSc1d1	Sachrův
ručej	ručej	k1gInSc1	ručej
<g/>
,	,	kIx,	,
Budská	Budský	k2eAgFnSc1d1	Budský
strouha	strouha	k1gFnSc1	strouha
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Šindelova	Šindelův	k2eAgFnSc1d1	Šindelova
strouha	strouha	k1gFnSc1	strouha
<g/>
,	,	kIx,	,
Hlemýždí	hlemýždí	k2eAgInSc1d1	hlemýždí
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Hamerský	hamerský	k2eAgInSc1d1	hamerský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Bělá	bělat	k5eAaImIp3nS	bělat
<g/>
,	,	kIx,	,
Principálek	principálek	k1gMnSc1	principálek
<g/>
,	,	kIx,	,
Sovinka	Sovinka	k1gFnSc1	Sovinka
<g/>
,	,	kIx,	,
Kalenský	Kalenský	k2eAgInSc1d1	Kalenský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Debrnský	Debrnský	k2eAgInSc1d1	Debrnský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Borecký	borecký	k2eAgInSc1d1	borecký
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Brusnický	Brusnický	k2eAgInSc1d1	Brusnický
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Netřeba	netřeba	k6eAd1	netřeba
<g/>
,	,	kIx,	,
Jezbinský	Jezbinský	k2eAgInSc1d1	Jezbinský
potok	potok	k1gInSc1	potok
Jordán	Jordán	k1gInSc1	Jordán
<g/>
,	,	kIx,	,
Trotina	Trotina	k1gFnSc1	Trotina
<g/>
,	,	kIx,	,
Olšovka	Olšovka	k1gFnSc1	Olšovka
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Strašovský	Strašovský	k2eAgInSc1d1	Strašovský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Svárava	Svárava	k1gFnSc1	Svárava
<g/>
,	,	kIx,	,
Veletovský	Veletovský	k2eAgInSc1d1	Veletovský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Hluboký	hluboký	k2eAgInSc1d1	hluboký
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Bačovka	Bačovka	k1gFnSc1	Bačovka
<g/>
,	,	kIx,	,
Cidlina	Cidlina	k1gFnSc1	Cidlina
<g/>
,	,	kIx,	,
Mrlina	Mrlina	k1gFnSc1	Mrlina
<g/>
,	,	kIx,	,
Vlkava	Vlkava	k1gFnSc1	Vlkava
<g/>
,	,	kIx,	,
Mlynařice	Mlynařice	k1gFnSc1	Mlynařice
<g/>
,	,	kIx,	,
Jizera	Jizera	k1gFnSc1	Jizera
<g/>
,	,	kIx,	,
Pšovka	Pšovka	k1gFnSc1	Pšovka
<g/>
,	,	kIx,	,
Liběchovka	Liběchovka	k1gFnSc1	Liběchovka
<g/>
,	,	kIx,	,
Úštěcký	úštěcký	k2eAgInSc1d1	úštěcký
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Blatenský	blatenský	k2eAgInSc1d1	blatenský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Luční	luční	k2eAgInSc1d1	luční
potok	potok	k1gInSc1	potok
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Pokratický	Pokratický	k2eAgInSc1d1	Pokratický
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Tlučeňský	Tlučeňský	k2eAgInSc1d1	Tlučeňský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Rytina	rytina	k1gFnSc1	rytina
<g/>
,	,	kIx,	,
Němčický	němčický	k2eAgInSc1d1	němčický
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Průčelský	Průčelský	k2eAgInSc1d1	Průčelský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Novoveský	Novoveský	k2eAgInSc1d1	Novoveský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Kojetický	Kojetický	k2eAgInSc1d1	Kojetický
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Bahniště	bahniště	k1gNnPc1	bahniště
<g/>
,	,	kIx,	,
Olešnický	olešnický	k2eAgInSc1d1	olešnický
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Homolský	Homolský	k2eAgInSc1d1	Homolský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Luční	luční	k2eAgInSc1d1	luční
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Těchlovický	Těchlovický	k2eAgInSc1d1	Těchlovický
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Kamenička	kamenička	k1gFnSc1	kamenička
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Ploučnice	Ploučnice	k1gFnSc1	Ploučnice
<g/>
,	,	kIx,	,
Kamenice	Kamenice	k1gFnSc1	Kamenice
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
-	-	kIx~	-
Křinice	Křinice	k1gFnSc1	Křinice
<g/>
,	,	kIx,	,
Lachsbach	Lachsbach	k1gMnSc1	Lachsbach
<g/>
,	,	kIx,	,
Wesenitz	Wesenitz	k1gMnSc1	Wesenitz
<g/>
,	,	kIx,	,
Prießnitz	Prießnitz	k1gMnSc1	Prießnitz
<g/>
,	,	kIx,	,
Lößnitzbach	Lößnitzbach	k1gMnSc1	Lößnitzbach
<g/>
,	,	kIx,	,
Gosebach	Gosebach	k1gMnSc1	Gosebach
<g/>
,	,	kIx,	,
Černý	Černý	k1gMnSc1	Černý
Halštrov	Halštrov	k1gInSc1	Halštrov
<g/>
,	,	kIx,	,
Havola	Havola	k1gFnSc1	Havola
<g/>
,	,	kIx,	,
Rossel	Rossel	k1gMnSc1	Rossel
<g/>
,	,	kIx,	,
Stepenitz	Stepenitz	k1gMnSc1	Stepenitz
<g/>
,	,	kIx,	,
Löcknitz	Löcknitz	k1gMnSc1	Löcknitz
<g/>
,	,	kIx,	,
Sude	sud	k1gInSc5	sud
<g/>
,	,	kIx,	,
Elde	Eld	k1gMnSc4	Eld
<g/>
,	,	kIx,	,
Delvenau	Delvenaa	k1gMnSc4	Delvenaa
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Bille	Bill	k1gMnSc5	Bill
<g/>
,	,	kIx,	,
Alster	Alster	k1gMnSc1	Alster
<g/>
,	,	kIx,	,
Flottbek	Flottbek	k1gMnSc1	Flottbek
<g/>
,	,	kIx,	,
Wedeler	Wedeler	k1gMnSc1	Wedeler
Au	au	k0	au
<g/>
,	,	kIx,	,
Pinnau	Pinnaa	k1gFnSc4	Pinnaa
<g/>
,	,	kIx,	,
Krückau	Krückaa	k1gFnSc4	Krückaa
<g/>
,	,	kIx,	,
Stör	Stör	k1gInSc4	Stör
<g/>
,	,	kIx,	,
Ramme	Ramm	k1gMnSc5	Ramm
<g/>
,	,	kIx,	,
Mehe	Mehus	k1gMnSc5	Mehus
<g/>
,	,	kIx,	,
Mehe-Aue	Mehe-Auus	k1gMnSc5	Mehe-Auus
<g/>
,	,	kIx,	,
Twiste	twist	k1gInSc5	twist
<g/>
,	,	kIx,	,
Bever	Bever	k1gInSc1	Bever
Špindlerův	Špindlerův	k2eAgInSc1d1	Špindlerův
Mlýn	mlýn	k1gInSc1	mlýn
<g/>
,	,	kIx,	,
Vrchlabí	Vrchlabí	k1gNnSc1	Vrchlabí
<g/>
,	,	kIx,	,
Hostinné	hostinný	k2eAgNnSc1d1	Hostinné
<g/>
,	,	kIx,	,
Dvůr	Dvůr	k1gInSc1	Dvůr
Králové	Králová	k1gFnSc2	Králová
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Jaroměř	Jaroměř	k1gFnSc1	Jaroměř
<g/>
,	,	kIx,	,
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
,	,	kIx,	,
Přelouč	Přelouč	k1gFnSc1	Přelouč
<g/>
,	,	kIx,	,
Kolín	Kolín	k1gInSc1	Kolín
<g/>
,	,	kIx,	,
Poděbrady	Poděbrady	k1gInPc1	Poděbrady
<g/>
,	,	kIx,	,
Nymburk	Nymburk	k1gInSc1	Nymburk
<g/>
,	,	kIx,	,
Lysá	Lysá	k1gFnSc1	Lysá
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
Čelákovice	Čelákovice	k1gFnPc1	Čelákovice
<g/>
,	,	kIx,	,
Brandýs	Brandýs	k1gInSc1	Brandýs
nad	nad	k7c4	nad
Labem-Stará	Labem-Starý	k2eAgNnPc4d1	Labem-Starý
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
,	,	kIx,	,
Neratovice	Neratovice	k1gFnSc1	Neratovice
<g/>
,	,	kIx,	,
Mělník	Mělník	k1gInSc1	Mělník
<g/>
,	,	kIx,	,
Štětí	štětit	k5eAaImIp3nS	štětit
<g/>
,	,	kIx,	,
Roudnice	Roudnice	k1gFnSc1	Roudnice
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Litoměřice	Litoměřice	k1gInPc1	Litoměřice
<g/>
,	,	kIx,	,
Lovosice	Lovosice	k1gInPc1	Lovosice
<g/>
,	,	kIx,	,
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
Děčín	Děčín	k1gInSc1	Děčín
<g/>
,	,	kIx,	,
Bad	Bad	k1gFnSc1	Bad
Schandau	Schandaus	k1gInSc2	Schandaus
<g/>
,	,	kIx,	,
Pirna	Pirn	k1gInSc2	Pirn
<g/>
,	,	kIx,	,
Drážďany	Drážďany	k1gInPc4	Drážďany
<g/>
,	,	kIx,	,
Míšeň	Míšeň	k1gFnSc4	Míšeň
<g/>
,	,	kIx,	,
Torgau	Torgaa	k1gFnSc4	Torgaa
<g/>
,	,	kIx,	,
Lutherstadt	Lutherstadt	k2eAgInSc4d1	Lutherstadt
Wittenberg	Wittenberg	k1gInSc4	Wittenberg
<g/>
,	,	kIx,	,
Dessau	Dessaa	k1gFnSc4	Dessaa
<g/>
,	,	kIx,	,
Magdeburg	Magdeburg	k1gInSc1	Magdeburg
<g/>
,	,	kIx,	,
Wittenberge	Wittenberge	k1gInSc1	Wittenberge
<g/>
,	,	kIx,	,
Hamburg	Hamburg	k1gInSc1	Hamburg
<g/>
,	,	kIx,	,
Cuxhaven	Cuxhaven	k2eAgInSc1d1	Cuxhaven
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
V	v	k7c6	v
závorce	závorka	k1gFnSc6	závorka
uvedena	uvést	k5eAaPmNgFnS	uvést
kilometráž	kilometráž	k1gFnSc1	kilometráž
od	od	k7c2	od
Mělníka	Mělník	k1gInSc2	Mělník
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
závorky	závorka	k1gFnSc2	závorka
od	od	k7c2	od
státní	státní	k2eAgFnSc2d1	státní
hranice	hranice	k1gFnSc2	hranice
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
řkm	řkm	k?	řkm
(	(	kIx(	(
<g/>
249,0	[number]	k4	249,0
<g/>
)	)	kIx)	)
přehrada	přehrada	k1gFnSc1	přehrada
Labská	labský	k2eAgFnSc1d1	Labská
<g/>
,	,	kIx,	,
Špindlerův	Špindlerův	k2eAgInSc1d1	Špindlerův
Mlýn	mlýn	k1gInSc1	mlýn
řkm	řkm	k?	řkm
(	(	kIx(	(
<g/>
248,9	[number]	k4	248,9
<g/>
)	)	kIx)	)
jez	jez	k1gInSc1	jez
řkm	řkm	k?	řkm
(	(	kIx(	(
<g/>
248,7	[number]	k4	248,7
<g/>
)	)	kIx)	)
jez	jez	k1gInSc1	jez
řkm	řkm	k?	řkm
(	(	kIx(	(
<g/>
242,5	[number]	k4	242,5
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
jez	jez	k1gInSc1	jez
řkm	řkm	k?	řkm
(	(	kIx(	(
<g/>
241,7	[number]	k4	241,7
<g/>
)	)	kIx)	)
jez	jez	k1gInSc1	jez
(	(	kIx(	(
<g/>
Herlíkovice	Herlíkovice	k1gFnSc1	Herlíkovice
<g/>
)	)	kIx)	)
řkm	řkm	k?	řkm
(	(	kIx(	(
<g/>
240,9	[number]	k4	240,9
<g/>
)	)	kIx)	)
jez	jez	k1gInSc1	jez
(	(	kIx(	(
<g/>
Podžalý	Podžalý	k2eAgInSc1d1	Podžalý
<g/>
)	)	kIx)	)
řkm	řkm	k?	řkm
(	(	kIx(	(
<g/>
239,9	[number]	k4	239,9
<g/>
)	)	kIx)	)
jez	jez	k1gInSc1	jez
řkm	řkm	k?	řkm
(	(	kIx(	(
<g/>
239,8	[number]	k4	239,8
<g/>
)	)	kIx)	)
jez	jez	k1gInSc1	jez
(	(	kIx(	(
<g/>
Hořejší	Hořejší	k2eAgNnSc1d1	Hořejší
Vrchlabí	Vrchlabí	k1gNnSc1	Vrchlabí
<g/>
)	)	kIx)	)
řkm	řkm	k?	řkm
(	(	kIx(	(
<g/>
239,3	[number]	k4	239,3
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
jez	jez	k1gInSc1	jez
řkm	řkm	k?	řkm
(	(	kIx(	(
<g/>
237,9	[number]	k4	237,9
<g/>
)	)	kIx)	)
jez	jez	k1gInSc1	jez
nad	nad	k7c7	nad
Vrchlabskou	vrchlabský	k2eAgFnSc7d1	vrchlabská
soutěskou	soutěska	k1gFnSc7	soutěska
řkm	řkm	k?	řkm
(	(	kIx(	(
<g/>
236,8	[number]	k4	236,8
<g/>
)	)	kIx)	)
vysoký	vysoký	k2eAgInSc1d1	vysoký
jez	jez	k1gInSc1	jez
(	(	kIx(	(
<g/>
Vrchlabí	Vrchlabí	k1gNnSc1	Vrchlabí
<g/>
)	)	kIx)	)
řkm	řkm	k?	řkm
(	(	kIx(	(
<g/>
236,0	[number]	k4	236,0
<g/>
)	)	kIx)	)
vysoký	vysoký	k2eAgInSc1d1	vysoký
jez	jez	k1gInSc1	jez
(	(	kIx(	(
<g/>
Vrchlabí	Vrchlabí	k1gNnSc1	Vrchlabí
<g/>
)	)	kIx)	)
řkm	řkm	k?	řkm
(	(	kIx(	(
<g/>
235,2	[number]	k4	235,2
<g/>
)	)	kIx)	)
jez	jez	k1gInSc1	jez
řkm	řkm	k?	řkm
(	(	kIx(	(
<g/>
233,8	[number]	k4	233,8
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
jez	jez	k1gInSc1	jez
(	(	kIx(	(
<g/>
Podhůří	Podhůří	k1gNnSc1	Podhůří
<g/>
)	)	kIx)	)
řkm	řkm	k?	řkm
(	(	kIx(	(
<g/>
232,2	[number]	k4	232,2
<g/>
)	)	kIx)	)
jez	jez	k1gInSc1	jez
Dolní	dolní	k2eAgFnSc1d1	dolní
Branná	branný	k2eAgFnSc1d1	Branná
řkm	řkm	k?	řkm
(	(	kIx(	(
<g/>
231,3	[number]	k4	231,3
<g/>
)	)	kIx)	)
jez	jez	k1gInSc1	jez
řkm	řkm	k?	řkm
(	(	kIx(	(
<g/>
227,4	[number]	k4	227,4
<g/>
)	)	kIx)	)
jez	jez	k1gInSc1	jez
Klášterská	klášterský	k2eAgFnSc1d1	Klášterská
Lhota	Lhota	k1gFnSc1	Lhota
řkm	řkm	k?	řkm
(	(	kIx(	(
<g/>
223,9	[number]	k4	223,9
<g/>
)	)	kIx)	)
jez	jez	k1gInSc1	jez
(	(	kIx(	(
<g/>
tento	tento	k3xDgMnSc1	tento
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
jezy	jez	k1gInPc4	jez
v	v	k7c6	v
Hostinném	hostinný	k2eAgMnSc6d1	hostinný
<g/>
)	)	kIx)	)
řkm	řkm	k?	řkm
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
222,7	[number]	k4	222,7
<g/>
)	)	kIx)	)
jez	jez	k1gInSc1	jez
řkm	řkm	k?	řkm
(	(	kIx(	(
<g/>
222,3	[number]	k4	222,3
<g/>
)	)	kIx)	)
jez	jez	k1gInSc1	jez
řkm	řkm	k?	řkm
(	(	kIx(	(
<g/>
221,8	[number]	k4	221,8
<g/>
)	)	kIx)	)
jez	jez	k1gInSc1	jez
řkm	řkm	k?	řkm
(	(	kIx(	(
<g/>
219,7	[number]	k4	219,7
<g/>
)	)	kIx)	)
jez	jez	k1gInSc1	jez
řkm	řkm	k?	řkm
(	(	kIx(	(
<g/>
218,7	[number]	k4	218,7
<g/>
)	)	kIx)	)
jez	jez	k1gInSc1	jez
řkm	řkm	k?	řkm
(	(	kIx(	(
<g/>
216,7	[number]	k4	216,7
<g/>
)	)	kIx)	)
jez	jez	k1gInSc1	jez
řkm	řkm	k?	řkm
(	(	kIx(	(
<g/>
214,2	[number]	k4	214,2
<g/>
)	)	kIx)	)
jez	jez	k1gInSc1	jez
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
poškozený	poškozený	k1gMnSc1	poškozený
<g/>
)	)	kIx)	)
řkm	řkm	k?	řkm
(	(	kIx(	(
<g/>
206,9	[number]	k4	206,9
<g/>
)	)	kIx)	)
přehrada	přehrada	k1gFnSc1	přehrada
Les	les	k1gInSc4	les
Království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
Dvůr	Dvůr	k1gInSc1	Dvůr
Králové	Králová	k1gFnSc2	Králová
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
řkm	řkm	k?	řkm
(	(	kIx(	(
<g/>
206,8	[number]	k4	206,8
<g/>
)	)	kIx)	)
jez	jez	k1gInSc1	jez
řkm	řkm	k?	řkm
(	(	kIx(	(
<g/>
202,0	[number]	k4	202,0
<g/>
)	)	kIx)	)
vysoký	vysoký	k2eAgInSc1d1	vysoký
jez	jez	k1gInSc1	jez
(	(	kIx(	(
<g/>
Dvůr	Dvůr	k1gInSc1	Dvůr
Králové	Králová	k1gFnSc2	Králová
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
)	)	kIx)	)
řkm	řkm	k?	řkm
(	(	kIx(	(
<g/>
201,7	[number]	k4	201,7
<g/>
)	)	kIx)	)
menší	malý	k2eAgInSc1d2	menší
jez	jez	k1gInSc1	jez
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Dvůr	Dvůr	k1gInSc1	Dvůr
Králové	Králová	k1gFnSc2	Králová
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
)	)	kIx)	)
řkm	řkm	k?	řkm
(	(	kIx(	(
<g/>
200,4	[number]	k4	200,4
<g/>
)	)	kIx)	)
jez	jez	k1gInSc1	jez
řkm	řkm	k?	řkm
(	(	kIx(	(
<g/>
195,1	[number]	k4	195,1
<g/>
)	)	kIx)	)
jez	jez	k1gInSc1	jez
(	(	kIx(	(
<g/>
Žireč	Žireč	k1gInSc1	Žireč
<g/>
)	)	kIx)	)
řkm	řkm	k?	řkm
(	(	kIx(	(
<g/>
191,9	[number]	k4	191,9
<g/>
)	)	kIx)	)
jez	jez	k1gInSc1	jez
Stanovice	Stanovice	k1gFnSc2	Stanovice
řkm	řkm	k?	řkm
(	(	kIx(	(
<g/>
186,2	[number]	k4	186,2
<g/>
)	)	kIx)	)
jez	jez	k1gInSc1	jez
Heřmanice	Heřmanice	k1gFnSc2	Heřmanice
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
řkm	řkm	k?	řkm
(	(	kIx(	(
<g/>
183,6	[number]	k4	183,6
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
jez	jez	k1gInSc1	jez
(	(	kIx(	(
<g/>
rozvalený	rozvalený	k2eAgInSc1d1	rozvalený
<g/>
)	)	kIx)	)
řkm	řkm	k?	řkm
(	(	kIx(	(
<g/>
181,7	[number]	k4	181,7
<g/>
)	)	kIx)	)
jez	jez	k1gInSc1	jez
(	(	kIx(	(
<g/>
Jaroměř	Jaroměř	k1gFnSc1	Jaroměř
<g/>
)	)	kIx)	)
řkm	řkm	k?	řkm
(	(	kIx(	(
<g/>
181,3	[number]	k4	181,3
<g/>
)	)	kIx)	)
jez	jez	k1gInSc1	jez
(	(	kIx(	(
<g/>
Jaroměř	Jaroměř	k1gFnSc1	Jaroměř
<g/>
)	)	kIx)	)
řkm	řkm	k?	řkm
(	(	kIx(	(
<g/>
180,3	[number]	k4	180,3
<g/>
)	)	kIx)	)
jez	jez	k1gInSc1	jez
Jaroměř	Jaroměř	k1gFnSc4	Jaroměř
centrum	centrum	k1gNnSc1	centrum
řkm	řkm	k?	řkm
(	(	kIx(	(
<g/>
177,8	[number]	k4	177,8
<g/>
)	)	kIx)	)
jez	jez	k1gInSc1	jez
Jaroměř	Jaroměř	k1gFnSc1	Jaroměř
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
<g />
.	.	kIx.	.
</s>
<s>
soutokem	soutok	k1gInSc7	soutok
s	s	k7c7	s
Metují	Metuje	k1gFnSc7	Metuje
řkm	řkm	k?	řkm
281,763	[number]	k4	281,763
(	(	kIx(	(
<g/>
171,7	[number]	k4	171,7
<g/>
)	)	kIx)	)
jez	jíst	k5eAaImRp2nS	jíst
Smiřice	Smiřice	k1gFnPc4	Smiřice
řkm	řkm	k?	řkm
274,315	[number]	k4	274,315
(	(	kIx(	(
<g/>
164,4	[number]	k4	164,4
<g/>
)	)	kIx)	)
jez	jez	k1gInSc1	jez
Předměřice	Předměřice	k1gFnSc2	Předměřice
řkm	řkm	k?	řkm
268,444	[number]	k4	268,444
(	(	kIx(	(
<g/>
158,4	[number]	k4	158,4
<g/>
)	)	kIx)	)
jez	jez	k1gInSc1	jez
Hučák	Hučák	k1gInSc1	Hučák
<g/>
,	,	kIx,	,
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
řkm	řkm	k?	řkm
(	(	kIx(	(
<g/>
152,6	[number]	k4	152,6
<g/>
)	)	kIx)	)
jez	jez	k1gInSc1	jez
u	u	k7c2	u
Opatovického	opatovický	k2eAgInSc2d1	opatovický
kanálu	kanál	k1gInSc2	kanál
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
<g />
.	.	kIx.	.
</s>
<s>
Splavnění	splavnění	k1gNnSc1	splavnění
Labe	Labe	k1gNnSc2	Labe
z	z	k7c2	z
Chvaletic	Chvaletice	k1gFnPc2	Chvaletice
do	do	k7c2	do
Pardubic	Pardubice	k1gInPc2	Pardubice
a	a	k8xC	a
Střední	střední	k2eAgNnSc1d1	střední
Labe	Labe	k1gNnSc1	Labe
(	(	kIx(	(
<g/>
vodní	vodní	k2eAgFnSc1d1	vodní
cesta	cesta	k1gFnSc1	cesta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Uvedena	uveden	k2eAgFnSc1d1	uvedena
Evropská	evropský	k2eAgFnSc1d1	Evropská
kilometráž	kilometráž	k1gFnSc1	kilometráž
s	s	k7c7	s
nulou	nula	k1gFnSc7	nula
při	při	k7c6	při
ústí	ústí	k1gNnSc6	ústí
Labe	Labe	k1gNnSc2	Labe
do	do	k7c2	do
Severního	severní	k2eAgNnSc2d1	severní
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
v	v	k7c6	v
závorce	závorka	k1gFnSc6	závorka
uvedena	uvést	k5eAaPmNgFnS	uvést
kilometráž	kilometráž	k1gFnSc1	kilometráž
od	od	k7c2	od
státní	státní	k2eAgFnSc2d1	státní
hranice	hranice	k1gFnSc2	hranice
a	a	k8xC	a
od	od	k7c2	od
Mělníka	Mělník	k1gInSc2	Mělník
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
řkm	řkm	k?	řkm
968,153	[number]	k4	968,153
-	-	kIx~	-
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
240,818	[number]	k4	240,818
<g/>
)	)	kIx)	)
-	-	kIx~	-
(	(	kIx(	(
<g/>
130,783	[number]	k4	130,783
<g/>
)	)	kIx)	)
-	-	kIx~	-
zdymadlo	zdymadlo	k1gNnSc4	zdymadlo
Pardubice	Pardubice	k1gInPc1	Pardubice
řkm	řkm	k?	řkm
961,524	[number]	k4	961,524
-	-	kIx~	-
(	(	kIx(	(
<g/>
234,818	[number]	k4	234,818
<g/>
)	)	kIx)	)
-	-	kIx~	-
(	(	kIx(	(
<g/>
124,154	[number]	k4	124,154
<g/>
)	)	kIx)	)
-	-	kIx~	-
zdymadlo	zdymadlo	k1gNnSc1	zdymadlo
Srnojedy	Srnojeda	k1gMnSc2	Srnojeda
řkm	řkm	k?	řkm
951,905	[number]	k4	951,905
-	-	kIx~	-
(	(	kIx(	(
<g/>
224,570	[number]	k4	224,570
<g/>
)	)	kIx)	)
-	-	kIx~	-
(	(	kIx(	(
<g/>
114,535	[number]	k4	114,535
<g/>
)	)	kIx)	)
-	-	kIx~	-
zdymadlo	zdymadlo	k1gNnSc4	zdymadlo
Přelouč	Přelouč	k1gFnSc1	Přelouč
řkm	řkm	k?	řkm
932,260	[number]	k4	932,260
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
205,280	[number]	k4	205,280
<g/>
)	)	kIx)	)
-	-	kIx~	-
(	(	kIx(	(
<g/>
92,225	[number]	k4	92,225
<g/>
)	)	kIx)	)
-	-	kIx~	-
zdymadlo	zdymadlo	k1gNnSc4	zdymadlo
Týnec	Týnec	k1gInSc1	Týnec
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
řkm	řkm	k?	řkm
929,130	[number]	k4	929,130
-	-	kIx~	-
(	(	kIx(	(
<g/>
201,736	[number]	k4	201,736
<g/>
)	)	kIx)	)
-	-	kIx~	-
(	(	kIx(	(
<g/>
91,701	[number]	k4	91,701
<g/>
)	)	kIx)	)
-	-	kIx~	-
zdymadlo	zdymadlo	k1gNnSc1	zdymadlo
Veletov	Veletov	k1gInSc1	Veletov
řkm	řkm	k?	řkm
920,690	[number]	k4	920,690
-	-	kIx~	-
(	(	kIx(	(
<g/>
193,228	[number]	k4	193,228
<g/>
)	)	kIx)	)
-	-	kIx~	-
(	(	kIx(	(
<g/>
83,193	[number]	k4	83,193
<g/>
)	)	kIx)	)
-	-	kIx~	-
zdymadlo	zdymadlo	k1gNnSc1	zdymadlo
Kolín	Kolín	k1gInSc1	Kolín
<g />
.	.	kIx.	.
</s>
<s>
řkm	řkm	k?	řkm
916,540	[number]	k4	916,540
-	-	kIx~	-
(	(	kIx(	(
<g/>
189,192	[number]	k4	189,192
<g/>
)	)	kIx)	)
-	-	kIx~	-
(	(	kIx(	(
<g/>
79,157	[number]	k4	79,157
<g/>
)	)	kIx)	)
-	-	kIx~	-
zdymadlo	zdymadlo	k1gNnSc4	zdymadlo
Klavary	Klavar	k1gInPc1	Klavar
řkm	řkm	k?	řkm
911,770	[number]	k4	911,770
-	-	kIx~	-
(	(	kIx(	(
<g/>
184,368	[number]	k4	184,368
<g/>
)	)	kIx)	)
-	-	kIx~	-
(	(	kIx(	(
<g/>
74,350	[number]	k4	74,350
<g/>
)	)	kIx)	)
-	-	kIx~	-
zdymadlo	zdymadlo	k1gNnSc4	zdymadlo
Velký	velký	k2eAgInSc1d1	velký
Osek	Osek	k1gInSc1	Osek
řkm	řkm	k?	řkm
904,570	[number]	k4	904,570
-	-	kIx~	-
(	(	kIx(	(
<g/>
177,158	[number]	k4	177,158
<g/>
)	)	kIx)	)
-	-	kIx~	-
(	(	kIx(	(
<g/>
67,123	[number]	k4	67,123
<g/>
)	)	kIx)	)
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
zdymadlo	zdymadlo	k1gNnSc4	zdymadlo
Poděbrady	Poděbrady	k1gInPc1	Poděbrady
řkm	řkm	k?	řkm
896,384	[number]	k4	896,384
-	-	kIx~	-
(	(	kIx(	(
<g/>
169,035	[number]	k4	169,035
<g/>
)	)	kIx)	)
-	-	kIx~	-
(	(	kIx(	(
<g/>
59,009	[number]	k4	59,009
<g/>
)	)	kIx)	)
-	-	kIx~	-
zdymadlo	zdymadlo	k1gNnSc1	zdymadlo
Nymburk	Nymburk	k1gInSc1	Nymburk
řkm	řkm	k?	řkm
891,440	[number]	k4	891,440
-	-	kIx~	-
(	(	kIx(	(
<g/>
164,015	[number]	k4	164,015
<g/>
)	)	kIx)	)
-	-	kIx~	-
(	(	kIx(	(
<g/>
53,980	[number]	k4	53,980
<g/>
)	)	kIx)	)
-	-	kIx~	-
zdymadlo	zdymadlo	k1gNnSc1	zdymadlo
Kostomlátky	Kostomlátko	k1gNnPc7	Kostomlátko
řkm	řkm	k?	řkm
887,580	[number]	k4	887,580
-	-	kIx~	-
(	(	kIx(	(
<g/>
160,173	[number]	k4	160,173
<g/>
)	)	kIx)	)
-	-	kIx~	-
(	(	kIx(	(
<g/>
50,138	[number]	k4	50,138
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
zdymadlo	zdymadlo	k1gNnSc1	zdymadlo
Hradištko	Hradištka	k1gFnSc5	Hradištka
řkm	řkm	k?	řkm
878,050	[number]	k4	878,050
-	-	kIx~	-
(	(	kIx(	(
<g/>
150,698	[number]	k4	150,698
<g/>
)	)	kIx)	)
-	-	kIx~	-
(	(	kIx(	(
<g/>
40,663	[number]	k4	40,663
<g/>
)	)	kIx)	)
-	-	kIx~	-
zdymadlo	zdymadlo	k1gNnSc1	zdymadlo
Lysá	lysat	k5eAaImIp3nS	lysat
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc7	jeho
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
i	i	k9	i
rybí	rybí	k2eAgInSc1d1	rybí
přechod	přechod	k1gInSc1	přechod
řkm	řkm	k?	řkm
872,280	[number]	k4	872,280
-	-	kIx~	-
(	(	kIx(	(
<g/>
144,980	[number]	k4	144,980
<g/>
)	)	kIx)	)
-	-	kIx~	-
(	(	kIx(	(
<g/>
34,950	[number]	k4	34,950
<g/>
)	)	kIx)	)
-	-	kIx~	-
zdymadlo	zdymadlo	k1gNnSc1	zdymadlo
Čelákovice	Čelákovice	k1gFnPc1	Čelákovice
řkm	řkm	k?	řkm
865,080	[number]	k4	865,080
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
137,913	[number]	k4	137,913
<g/>
)	)	kIx)	)
-	-	kIx~	-
(	(	kIx(	(
<g/>
27,878	[number]	k4	27,878
<g/>
)	)	kIx)	)
-	-	kIx~	-
zdymadlo	zdymadlo	k1gNnSc4	zdymadlo
Brandýs	Brandýs	k1gInSc1	Brandýs
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
řkm	řkm	k?	řkm
857,420	[number]	k4	857,420
-	-	kIx~	-
(	(	kIx(	(
<g/>
130,158	[number]	k4	130,158
<g/>
)	)	kIx)	)
-	-	kIx~	-
(	(	kIx(	(
<g/>
20,127	[number]	k4	20,127
<g/>
)	)	kIx)	)
-	-	kIx~	-
zdymadlo	zdymadlo	k1gNnSc4	zdymadlo
Kostelec	Kostelec	k1gInSc1	Kostelec
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
řkm	řkm	k?	řkm
850,320	[number]	k4	850,320
-	-	kIx~	-
(	(	kIx(	(
<g/>
123,015	[number]	k4	123,015
<g/>
)	)	kIx)	)
-	-	kIx~	-
(	(	kIx(	(
<g/>
12,997	[number]	k4	12,997
<g/>
)	)	kIx)	)
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
zdymadlo	zdymadlo	k1gNnSc4	zdymadlo
Lobkovice	Lobkovice	k1gInPc1	Lobkovice
řkm	řkm	k?	řkm
843,133	[number]	k4	843,133
-	-	kIx~	-
(	(	kIx(	(
<g/>
116,181	[number]	k4	116,181
<g/>
)	)	kIx)	)
-	-	kIx~	-
(	(	kIx(	(
<g/>
6,146	[number]	k4	6,146
<g/>
)	)	kIx)	)
-	-	kIx~	-
zdymadlo	zdymadlo	k1gNnSc1	zdymadlo
Obříství	Obříství	k1gNnSc2	Obříství
řkm	řkm	k?	řkm
839,535	[number]	k4	839,535
-	-	kIx~	-
(	(	kIx(	(
<g/>
112,235	[number]	k4	112,235
<g/>
)	)	kIx)	)
-	-	kIx~	-
(	(	kIx(	(
<g/>
2,165	[number]	k4	2,165
<g/>
)	)	kIx)	)
-	-	kIx~	-
bývalé	bývalý	k2eAgNnSc1d1	bývalé
zdymadlo	zdymadlo	k1gNnSc1	zdymadlo
Hadík	Hadík	k1gInSc1	Hadík
(	(	kIx(	(
<g/>
Uvedena	uveden	k2eAgFnSc1d1	uvedena
Evropská	evropský	k2eAgFnSc1d1	Evropská
kilometráž	kilometráž	k1gFnSc1	kilometráž
s	s	k7c7	s
nulou	nula	k1gFnSc7	nula
při	při	k7c6	při
ústí	ústí	k1gNnSc6	ústí
Labe	Labe	k1gNnSc2	Labe
do	do	k7c2	do
Severního	severní	k2eAgNnSc2d1	severní
moře	moře	k1gNnSc2	moře
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
v	v	k7c6	v
závorkách	závorka	k1gFnPc6	závorka
kilometráž	kilometráž	k1gFnSc1	kilometráž
od	od	k7c2	od
Mělníka	Mělník	k1gInSc2	Mělník
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
po	po	k7c6	po
proudu	proud	k1gInSc6	proud
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
řkm	řkm	k?	řkm
830,530	[number]	k4	830,530
-	-	kIx~	-
(	(	kIx(	(
<g/>
6,675	[number]	k4	6,675
<g/>
)	)	kIx)	)
-	-	kIx~	-
zdymadlo	zdymadlo	k1gNnSc1	zdymadlo
Dolní	dolní	k2eAgFnSc2d1	dolní
Beřkovice	Beřkovice	k1gFnSc2	Beřkovice
řkm	řkm	k?	řkm
818,720	[number]	k4	818,720
-	-	kIx~	-
(	(	kIx(	(
<g/>
18,165	[number]	k4	18,165
<g/>
)	)	kIx)	)
-	-	kIx~	-
zdymadlo	zdymadlo	k1gNnSc1	zdymadlo
Štětí	štětit	k5eAaImIp3nS	štětit
řkm	řkm	k?	řkm
808,790	[number]	k4	808,790
-	-	kIx~	-
(	(	kIx(	(
<g/>
27,310	[number]	k4	27,310
<g/>
)	)	kIx)	)
-	-	kIx~	-
zdymadlo	zdymadlo	k1gNnSc1	zdymadlo
Roudnice	Roudnice	k1gFnSc2	Roudnice
nad	nad	k7c7	nad
<g />
.	.	kIx.	.
</s>
<s>
Labem	Labe	k1gNnSc7	Labe
řkm	řkm	k?	řkm
795,330	[number]	k4	795,330
-	-	kIx~	-
(	(	kIx(	(
<g/>
41,210	[number]	k4	41,210
<g/>
)	)	kIx)	)
-	-	kIx~	-
zdymadlo	zdymadlo	k1gNnSc1	zdymadlo
České	český	k2eAgInPc1d1	český
Kopisty	kopist	k1gInPc1	kopist
řkm	řkm	k?	řkm
787,430	[number]	k4	787,430
-	-	kIx~	-
(	(	kIx(	(
<g/>
49,295	[number]	k4	49,295
<g/>
)	)	kIx)	)
-	-	kIx~	-
zdymadlo	zdymadlo	k1gNnSc4	zdymadlo
Lovosice	Lovosice	k1gInPc1	Lovosice
řkm	řkm	k?	řkm
767,484	[number]	k4	767,484
-	-	kIx~	-
(	(	kIx(	(
<g/>
68,870	[number]	k4	68,870
<g/>
)	)	kIx)	)
-	-	kIx~	-
zdymadlo	zdymadlo	k1gNnSc1	zdymadlo
Střekov	Střekov	k1gInSc4	Střekov
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc7	jeho
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
i	i	k9	i
rybí	rybí	k2eAgInSc1d1	rybí
přechod	přechod	k1gInSc1	přechod
(	(	kIx(	(
<g/>
Uvedena	uveden	k2eAgFnSc1d1	uvedena
kilometráž	kilometráž	k1gFnSc1	kilometráž
od	od	k7c2	od
Státní	státní	k2eAgFnSc2d1	státní
hranice	hranice	k1gFnSc2	hranice
s	s	k7c7	s
ČR	ČR	kA	ČR
po	po	k7c6	po
proudu	proud	k1gInSc6	proud
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
řkm	řkm	k?	řkm
585,860	[number]	k4	585,860
-	-	kIx~	-
zdymadlo	zdymadlo	k1gNnSc1	zdymadlo
Geesthacht	Geesthachta	k1gFnPc2	Geesthachta
Nejvyšších	vysoký	k2eAgInPc2d3	Nejvyšší
vodních	vodní	k2eAgInPc2d1	vodní
stavů	stav	k1gInPc2	stav
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
tající	tající	k2eAgInSc1d1	tající
sníh	sníh	k1gInSc1	sníh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
hladina	hladina	k1gFnSc1	hladina
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
dešťové	dešťový	k2eAgFnPc1d1	dešťová
srážky	srážka	k1gFnPc1	srážka
mohou	moct	k5eAaImIp3nP	moct
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
způsobit	způsobit	k5eAaPmF	způsobit
náhlé	náhlý	k2eAgInPc4d1	náhlý
vzestupy	vzestup	k1gInPc4	vzestup
hladiny	hladina	k1gFnSc2	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
zbývající	zbývající	k2eAgFnSc4d1	zbývající
část	část	k1gFnSc4	část
roku	rok	k1gInSc2	rok
je	být	k5eAaImIp3nS	být
úroveň	úroveň	k1gFnSc1	úroveň
hladiny	hladina	k1gFnSc2	hladina
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
průtok	průtok	k1gInSc1	průtok
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
česko-německé	českoěmecký	k2eAgFnSc6d1	česko-německá
hranici	hranice	k1gFnSc6	hranice
činí	činit	k5eAaImIp3nS	činit
311	[number]	k4	311
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
a	a	k8xC	a
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
(	(	kIx(	(
<g/>
ve	v	k7c6	v
vodoměrné	vodoměrný	k2eAgFnSc6d1	vodoměrná
stanici	stanice	k1gFnSc6	stanice
Neu	Neu	k1gFnSc2	Neu
Darchau	Darchaus	k1gInSc2	Darchaus
<g/>
)	)	kIx)	)
711	[number]	k4	711
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Mořský	mořský	k2eAgInSc1d1	mořský
příliv	příliv	k1gInSc1	příliv
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
141,8	[number]	k4	141,8
km	km	kA	km
proti	proti	k7c3	proti
toku	tok	k1gInSc3	tok
<g/>
.	.	kIx.	.
</s>
<s>
Kolísání	kolísání	k1gNnSc1	kolísání
hladiny	hladina	k1gFnSc2	hladina
řeky	řeka	k1gFnSc2	řeka
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
slapovém	slapový	k2eAgInSc6d1	slapový
úseku	úsek	k1gInSc6	úsek
a	a	k8xC	a
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vzdutí	vzdutí	k1gNnSc2	vzdutí
následkem	následkem	k7c2	následkem
silného	silný	k2eAgInSc2d1	silný
větru	vítr	k1gInSc2	vítr
může	moct	k5eAaImIp3nS	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
až	až	k9	až
9	[number]	k4	9
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Zámrz	zámrz	k1gInSc1	zámrz
na	na	k7c6	na
úsecích	úsek	k1gInPc6	úsek
řeky	řeka	k1gFnSc2	řeka
Labe	Labe	k1gNnSc2	Labe
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
mj.	mj.	kA	mj.
teplotní	teplotní	k2eAgInSc4d1	teplotní
průběh	průběh	k1gInSc4	průběh
<g/>
,	,	kIx,	,
velikost	velikost	k1gFnSc4	velikost
odtoku	odtok	k1gInSc2	odtok
a	a	k8xC	a
využívání	využívání	k1gNnSc2	využívání
toku	tok	k1gInSc2	tok
pro	pro	k7c4	pro
lodní	lodní	k2eAgFnSc4d1	lodní
dopravu	doprava	k1gFnSc4	doprava
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
proti	proti	k7c3	proti
proudu	proud	k1gInSc3	proud
toku	tok	k1gInSc2	tok
počet	počet	k1gInSc4	počet
dní	den	k1gInPc2	den
zámrzu	zámrz	k1gInSc2	zámrz
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgFnSc1d1	vodní
doprava	doprava	k1gFnSc1	doprava
na	na	k7c6	na
Labi	Labe	k1gNnSc6	Labe
a	a	k8xC	a
Vltavě	Vltava	k1gFnSc3	Vltava
je	být	k5eAaImIp3nS	být
doložena	doložit	k5eAaPmNgFnS	doložit
již	již	k6eAd1	již
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
bronzové	bronzový	k2eAgFnSc2d1	bronzová
<g/>
.	.	kIx.	.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
moderní	moderní	k2eAgFnSc2d1	moderní
vodní	vodní	k2eAgFnSc2d1	vodní
dopravy	doprava	k1gFnSc2	doprava
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1764	[number]	k4	1764
zřídila	zřídit	k5eAaPmAgFnS	zřídit
rakouská	rakouský	k2eAgFnSc1d1	rakouská
vláda	vláda	k1gFnSc1	vláda
plavební	plavební	k2eAgFnSc4d1	plavební
komisi	komise	k1gFnSc4	komise
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1766	[number]	k4	1766
plavební	plavební	k2eAgInSc1d1	plavební
fond	fond	k1gInSc1	fond
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1770	[number]	k4	1770
Plavební	plavební	k2eAgMnPc1d1	plavební
vodní	vodní	k2eAgMnPc1d1	vodní
ředitelství	ředitelství	k1gNnSc2	ředitelství
<g/>
,	,	kIx,	,
z	z	k7c2	z
nějž	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
Ředitelství	ředitelství	k1gNnSc1	ředitelství
vodních	vodní	k2eAgFnPc2d1	vodní
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1777	[number]	k4	1777
vydala	vydat	k5eAaPmAgFnS	vydat
císařovna	císařovna	k1gFnSc1	císařovna
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
Navigační	navigační	k2eAgInSc4d1	navigační
patent	patent	k1gInSc4	patent
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nadřadil	nadřadit	k5eAaPmAgInS	nadřadit
plavbu	plavba	k1gFnSc4	plavba
jiným	jiný	k2eAgInPc3d1	jiný
způsobům	způsob	k1gInPc3	způsob
využívání	využívání	k1gNnSc2	využívání
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
splavné	splavný	k2eAgInPc4d1	splavný
toky	tok	k1gInPc4	tok
za	za	k7c4	za
majetek	majetek	k1gInSc4	majetek
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
stát	stát	k1gInSc1	stát
zavázal	zavázat	k5eAaPmAgInS	zavázat
k	k	k7c3	k
nesení	nesení	k1gNnSc3	nesení
nákladů	náklad	k1gInPc2	náklad
za	za	k7c4	za
rozvoj	rozvoj	k1gInSc4	rozvoj
a	a	k8xC	a
údržbu	údržba	k1gFnSc4	údržba
vodních	vodní	k2eAgFnPc2d1	vodní
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Děčíně	Děčín	k1gInSc6	Děčín
v	v	k7c6	v
Rozbělesích	Rozbělese	k1gFnPc6	Rozbělese
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
roku	rok	k1gInSc2	rok
1857	[number]	k4	1857
ochranný	ochranný	k2eAgInSc1d1	ochranný
přístav	přístav	k1gInSc1	přístav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1897	[number]	k4	1897
<g/>
-	-	kIx~	-
<g/>
1913	[number]	k4	1913
byla	být	k5eAaImAgFnS	být
budována	budován	k2eAgFnSc1d1	budována
soustava	soustava	k1gFnSc1	soustava
zdymadel	zdymadlo	k1gNnPc2	zdymadlo
na	na	k7c6	na
Vltavě	Vltava	k1gFnSc6	Vltava
i	i	k8xC	i
Labi	Labe	k1gNnSc6	Labe
a	a	k8xC	a
laterální	laterální	k2eAgInSc1d1	laterální
plavební	plavební	k2eAgInSc1d1	plavební
kanál	kanál	k1gInSc1	kanál
u	u	k7c2	u
Hořína	Hořín	k1gInSc2	Hořín
<g/>
;	;	kIx,	;
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
"	"	kIx"	"
<g/>
Komise	komise	k1gFnSc1	komise
pro	pro	k7c4	pro
kanalizování	kanalizování	k1gNnSc4	kanalizování
Vltavy	Vltava	k1gFnSc2	Vltava
a	a	k8xC	a
Labe	Labe	k1gNnSc2	Labe
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1901	[number]	k4	1901
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
říšský	říšský	k2eAgInSc1d1	říšský
Vodocestný	vodocestný	k2eAgInSc1d1	vodocestný
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
umožnil	umožnit	k5eAaPmAgInS	umožnit
kanalizovat	kanalizovat	k5eAaBmF	kanalizovat
Labe	Labe	k1gNnSc4	Labe
z	z	k7c2	z
Mělníka	Mělník	k1gInSc2	Mělník
ke	k	k7c3	k
Střekovu	Střekov	k1gInSc3	Střekov
a	a	k8xC	a
splavnit	splavnit	k5eAaPmF	splavnit
Labe	Labe	k1gNnSc4	Labe
z	z	k7c2	z
Mělníka	Mělník	k1gInSc2	Mělník
do	do	k7c2	do
Brandýsa	Brandýs	k1gInSc2	Brandýs
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
plánech	plán	k1gInPc6	plán
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
i	i	k9	i
Československo	Československo	k1gNnSc1	Československo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
roku	rok	k1gInSc3	rok
1936	[number]	k4	1936
dokončilo	dokončit	k5eAaPmAgNnS	dokončit
zdymadlo	zdymadlo	k1gNnSc1	zdymadlo
u	u	k7c2	u
Střekova	Střekov	k1gInSc2	Střekov
<g/>
.	.	kIx.	.
</s>
<s>
Smlouvou	smlouva	k1gFnSc7	smlouva
z	z	k7c2	z
Versailles	Versailles	k1gFnSc2	Versailles
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
plavba	plavba	k1gFnSc1	plavba
na	na	k7c6	na
Labi	Labe	k1gNnSc6	Labe
předmětem	předmět	k1gInSc7	předmět
mezinárodní	mezinárodní	k2eAgFnPc1d1	mezinárodní
Labské	labský	k2eAgFnPc1d1	Labská
komise	komise	k1gFnPc1	komise
<g/>
,	,	kIx,	,
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Drážďanech	Drážďany	k1gInPc6	Drážďany
<g/>
.	.	kIx.	.
</s>
<s>
Statut	statut	k1gInSc1	statut
Komise	komise	k1gFnSc2	komise
byl	být	k5eAaImAgInS	být
podepsán	podepsat	k5eAaPmNgInS	podepsat
v	v	k7c6	v
Drážďanech	Drážďany	k1gInPc6	Drážďany
22	[number]	k4	22
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1922	[number]	k4	1922
<g/>
.	.	kIx.	.
</s>
<s>
Články	článek	k1gInPc1	článek
363	[number]	k4	363
a	a	k8xC	a
364	[number]	k4	364
Versaillské	Versaillský	k2eAgFnSc2d1	Versaillská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
Československo	Československo	k1gNnSc1	Československo
oprávněno	oprávněn	k2eAgNnSc1d1	oprávněno
k	k	k7c3	k
pronájmu	pronájem	k1gInSc3	pronájem
vlastního	vlastní	k2eAgNnSc2d1	vlastní
přístavního	přístavní	k2eAgNnSc2d1	přístavní
pásma	pásmo	k1gNnSc2	pásmo
<g/>
,	,	kIx,	,
Moldauhafen	Moldauhafen	k1gInSc1	Moldauhafen
v	v	k7c6	v
Hamburku	Hamburk	k1gInSc6	Hamburk
<g/>
.	.	kIx.	.
</s>
<s>
Nájemní	nájemní	k2eAgFnSc1d1	nájemní
smlouva	smlouva	k1gFnSc1	smlouva
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
byla	být	k5eAaImAgFnS	být
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
podepsána	podepsán	k2eAgFnSc1d1	podepsána
dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1929	[number]	k4	1929
a	a	k8xC	a
skončí	skončit	k5eAaPmIp3nS	skončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2028	[number]	k4	2028
<g/>
;	;	kIx,	;
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
drží	držet	k5eAaImIp3nS	držet
Česko	Česko	k1gNnSc4	Česko
právní	právní	k2eAgNnSc1d1	právní
postavení	postavení	k1gNnSc1	postavení
bývalého	bývalý	k2eAgNnSc2d1	bývalé
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vodní	vodní	k2eAgFnSc4d1	vodní
dopravu	doprava	k1gFnSc4	doprava
je	být	k5eAaImIp3nS	být
Labe	Labe	k1gNnSc4	Labe
splavné	splavný	k2eAgNnSc4d1	splavné
od	od	k7c2	od
ústí	ústí	k1gNnSc2	ústí
do	do	k7c2	do
Severního	severní	k2eAgNnSc2d1	severní
moře	moře	k1gNnSc2	moře
po	po	k7c4	po
Přelouč	Přelouč	k1gFnSc4	Přelouč
v	v	k7c6	v
celkové	celkový	k2eAgFnSc6d1	celková
délce	délka	k1gFnSc6	délka
950	[number]	k4	950
km	km	kA	km
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
je	být	k5eAaImIp3nS	být
připravováno	připravován	k2eAgNnSc4d1	připravováno
průběžné	průběžný	k2eAgNnSc4d1	průběžné
splavnění	splavnění	k1gNnSc4	splavnění
o	o	k7c4	o
dalších	další	k2eAgFnPc2d1	další
24	[number]	k4	24
km	km	kA	km
až	až	k6eAd1	až
do	do	k7c2	do
Pardubic	Pardubice	k1gInPc2	Pardubice
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Hamburku	Hamburk	k1gInSc2	Hamburk
je	být	k5eAaImIp3nS	být
splavné	splavný	k2eAgNnSc4d1	splavné
pro	pro	k7c4	pro
námořní	námořní	k2eAgFnPc4d1	námořní
lodě	loď	k1gFnPc4	loď
<g/>
,	,	kIx,	,
po	po	k7c6	po
odbočení	odbočení	k1gNnSc6	odbočení
Labského	labský	k2eAgInSc2d1	labský
laterálního	laterální	k2eAgInSc2d1	laterální
průplavu	průplav	k1gInSc2	průplav
je	být	k5eAaImIp3nS	být
kanalizovaným	kanalizovaný	k2eAgInSc7d1	kanalizovaný
tokem	tok	k1gInSc7	tok
třídy	třída	k1gFnSc2	třída
VIb	VIb	k1gFnSc2	VIb
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
klasifikace	klasifikace	k1gFnSc2	klasifikace
vnitrozemských	vnitrozemský	k2eAgFnPc2d1	vnitrozemská
vodních	vodní	k2eAgFnPc2d1	vodní
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
proti	proti	k7c3	proti
proudu	proud	k1gInSc3	proud
je	být	k5eAaImIp3nS	být
většina	většina	k1gFnSc1	většina
jeho	jeho	k3xOp3gInSc2	jeho
toku	tok	k1gInSc2	tok
regulována	regulován	k2eAgFnSc1d1	regulována
<g/>
,	,	kIx,	,
od	od	k7c2	od
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
kanalizována	kanalizován	k2eAgFnSc1d1	kanalizována
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
Mělník	Mělník	k1gInSc4	Mělník
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
vodní	vodní	k2eAgNnSc4d1	vodní
cestou	cestou	k7c2	cestou
třídy	třída	k1gFnSc2	třída
Va	va	k0wR	va
a	a	k8xC	a
po	po	k7c4	po
Přelouč	Přelouč	k1gFnSc4	Přelouč
resp.	resp.	kA	resp.
Pardubice	Pardubice	k1gInPc4	Pardubice
pak	pak	k6eAd1	pak
vodní	vodní	k2eAgFnSc7d1	vodní
cestou	cesta	k1gFnSc7	cesta
třídy	třída	k1gFnSc2	třída
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Systémem	systém	k1gInSc7	systém
kanálů	kanál	k1gInPc2	kanál
je	být	k5eAaImIp3nS	být
spojené	spojený	k2eAgNnSc1d1	spojené
s	s	k7c7	s
Baltským	baltský	k2eAgNnSc7d1	Baltské
mořem	moře	k1gNnSc7	moře
<g/>
,	,	kIx,	,
Rýnem	Rýn	k1gInSc7	Rýn
<g/>
,	,	kIx,	,
Vezerou	Vezera	k1gFnSc7	Vezera
<g/>
,	,	kIx,	,
Emží	Emž	k1gFnSc7	Emž
a	a	k8xC	a
Odrou	Odra	k1gFnSc7	Odra
<g/>
.	.	kIx.	.
</s>
<s>
Labe	Labe	k1gNnSc4	Labe
je	být	k5eAaImIp3nS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
spojnicí	spojnice	k1gFnSc7	spojnice
českých	český	k2eAgFnPc2d1	Česká
vodních	vodní	k2eAgFnPc2d1	vodní
cest	cesta	k1gFnPc2	cesta
vltavsko-labského	vltavskoabský	k2eAgInSc2d1	vltavsko-labský
systému	systém	k1gInSc2	systém
se	s	k7c7	s
sítí	síť	k1gFnSc7	síť
evropských	evropský	k2eAgFnPc2d1	Evropská
vodních	vodní	k2eAgFnPc2d1	vodní
cest	cesta	k1gFnPc2	cesta
a	a	k8xC	a
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
plavebních	plavební	k2eAgNnPc2d1	plavební
akt	akta	k1gNnPc2	akta
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1821	[number]	k4	1821
z	z	k7c2	z
Mělníka	Mělník	k1gInSc2	Mělník
po	po	k7c6	po
ústí	ústí	k1gNnSc6	ústí
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
prohlášeno	prohlášen	k2eAgNnSc1d1	prohlášeno
za	za	k7c4	za
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
vodní	vodní	k2eAgFnSc4d1	vodní
cestu	cesta	k1gFnSc4	cesta
se	s	k7c7	s
svobodným	svobodný	k2eAgInSc7d1	svobodný
přístupem	přístup	k1gInSc7	přístup
plavidel	plavidlo	k1gNnPc2	plavidlo
všech	všecek	k3xTgFnPc2	všecek
národností	národnost	k1gFnPc2	národnost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
naší	náš	k3xOp1gFnSc7	náš
jedinou	jediný	k2eAgFnSc7d1	jediná
svobodnou	svobodný	k2eAgFnSc7d1	svobodná
spojnicí	spojnice	k1gFnSc7	spojnice
s	s	k7c7	s
mořem	moře	k1gNnSc7	moře
a	a	k8xC	a
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
jeho	jeho	k3xOp3gFnPc2	jeho
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
vod	voda	k1gFnPc2	voda
prakticky	prakticky	k6eAd1	prakticky
s	s	k7c7	s
celým	celý	k2eAgInSc7d1	celý
světem	svět	k1gInSc7	svět
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
z	z	k7c2	z
Česka	Česko	k1gNnSc2	Česko
vyvážet	vyvážet	k5eAaImF	vyvážet
a	a	k8xC	a
do	do	k7c2	do
nich	on	k3xPp3gInPc2	on
dovážet	dovážet	k5eAaImF	dovážet
zboží	zboží	k1gNnSc4	zboží
nezatížené	zatížený	k2eNgNnSc4d1	nezatížené
přepravními	přepravní	k2eAgInPc7d1	přepravní
poplatky	poplatek	k1gInPc7	poplatek
cizích	cizí	k2eAgInPc2d1	cizí
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejichž	jejichž	k3xOyRp3gFnSc6	jejichž
výši	výše	k1gFnSc6	výše
nemá	mít	k5eNaImIp3nS	mít
Česko	Česko	k1gNnSc1	Česko
vliv	vliv	k1gInSc1	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
labská	labský	k2eAgFnSc1d1	Labská
plavba	plavba	k1gFnSc1	plavba
pouhou	pouhý	k2eAgFnSc4d1	pouhá
svou	svůj	k3xOyFgFnSc7	svůj
existencí	existence	k1gFnSc7	existence
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
regulátor	regulátor	k1gInSc1	regulátor
ceny	cena	k1gFnSc2	cena
českého	český	k2eAgInSc2d1	český
exportu	export	k1gInSc2	export
a	a	k8xC	a
importu	import	k1gInSc2	import
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
přispívá	přispívat	k5eAaImIp3nS	přispívat
ke	k	k7c3	k
konkurenceschopnosti	konkurenceschopnost	k1gFnSc3	konkurenceschopnost
české	český	k2eAgFnSc2d1	Česká
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
prudké	prudký	k2eAgFnSc3d1	prudká
změně	změna	k1gFnSc3	změna
sklonu	sklona	k1gFnSc4	sklona
dna	dno	k1gNnSc2	dno
u	u	k7c2	u
Dolního	dolní	k2eAgInSc2d1	dolní
Žlebu	žleb	k1gInSc2	žleb
pod	pod	k7c7	pod
Děčínem	Děčín	k1gInSc7	Děčín
má	mít	k5eAaImIp3nS	mít
český	český	k2eAgInSc4d1	český
regulovaný	regulovaný	k2eAgInSc4d1	regulovaný
úsek	úsek	k1gInSc4	úsek
mnohem	mnohem	k6eAd1	mnohem
horší	zlý	k2eAgFnPc1d2	horší
plavební	plavební	k2eAgFnPc1d1	plavební
podmínky	podmínka	k1gFnPc1	podmínka
než	než	k8xS	než
zbytek	zbytek	k1gInSc1	zbytek
toku	tok	k1gInSc2	tok
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
nespolehlivost	nespolehlivost	k1gFnSc4	nespolehlivost
vodní	vodní	k2eAgFnSc2d1	vodní
dopravy	doprava	k1gFnSc2	doprava
kvůli	kvůli	k7c3	kvůli
dlouhým	dlouhý	k2eAgNnPc3d1	dlouhé
obdobím	období	k1gNnPc3	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
plavba	plavba	k1gFnSc1	plavba
zastavena	zastaven	k2eAgFnSc1d1	zastavena
pro	pro	k7c4	pro
nízký	nízký	k2eAgInSc4d1	nízký
přípustný	přípustný	k2eAgInSc4d1	přípustný
ponor	ponor	k1gInSc4	ponor
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Střední	střední	k2eAgNnSc1d1	střední
Labe	Labe	k1gNnSc1	Labe
(	(	kIx(	(
<g/>
vodní	vodní	k2eAgFnSc1d1	vodní
cesta	cesta	k1gFnSc1	cesta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
výraznému	výrazný	k2eAgNnSc3d1	výrazné
zlepšení	zlepšení	k1gNnSc3	zlepšení
čistoty	čistota	k1gFnSc2	čistota
vody	voda	k1gFnSc2	voda
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
se	se	k3xPyFc4	se
do	do	k7c2	do
Labe	Labe	k1gNnSc2	Labe
po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
době	doba	k1gFnSc6	doba
vrátila	vrátit	k5eAaPmAgFnS	vrátit
dříve	dříve	k6eAd2	dříve
vzácná	vzácný	k2eAgFnSc1d1	vzácná
fauna	fauna	k1gFnSc1	fauna
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
losos	losos	k1gMnSc1	losos
<g/>
,	,	kIx,	,
vydra	vydra	k1gFnSc1	vydra
říční	říční	k2eAgMnPc1d1	říční
a	a	k8xC	a
bobr	bobr	k1gMnSc1	bobr
evropský	evropský	k2eAgMnSc1d1	evropský
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ptáky	pták	k1gMnPc4	pták
obývající	obývající	k2eAgNnSc1d1	obývající
údolí	údolí	k1gNnSc1	údolí
Labe	Labe	k1gNnSc2	Labe
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
potápka	potápka	k1gFnSc1	potápka
malá	malá	k1gFnSc1	malá
<g/>
,	,	kIx,	,
volavka	volavka	k1gFnSc1	volavka
popelavá	popelavý	k2eAgFnSc1d1	popelavá
<g/>
,	,	kIx,	,
volavka	volavka	k1gFnSc1	volavka
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
kormorán	kormorán	k1gMnSc1	kormorán
velký	velký	k2eAgMnSc1d1	velký
<g/>
,	,	kIx,	,
ledňáček	ledňáček	k1gMnSc1	ledňáček
říční	říční	k2eAgFnSc2d1	říční
<g/>
,	,	kIx,	,
břehule	břehule	k1gFnSc2	břehule
říční	říční	k2eAgFnSc2d1	říční
<g/>
,	,	kIx,	,
kulík	kulík	k1gMnSc1	kulík
říční	říční	k2eAgMnSc1d1	říční
a	a	k8xC	a
morčák	morčák	k1gMnSc1	morčák
velký	velký	k2eAgMnSc1d1	velký
<g/>
.	.	kIx.	.
</s>
<s>
Velkoplošná	velkoplošný	k2eAgNnPc1d1	velkoplošné
chráněná	chráněný	k2eAgNnPc1d1	chráněné
území	území	k1gNnPc1	území
přírody	příroda	k1gFnSc2	příroda
podél	podél	k7c2	podél
českého	český	k2eAgInSc2d1	český
toku	tok	k1gInSc2	tok
Labe	Labe	k1gNnSc2	Labe
-	-	kIx~	-
Krkonošský	krkonošský	k2eAgInSc1d1	krkonošský
národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
CHKO	CHKO	kA	CHKO
České	český	k2eAgNnSc1d1	české
středohoří	středohoří	k1gNnSc1	středohoří
<g/>
,	,	kIx,	,
CHKO	CHKO	kA	CHKO
Labské	labský	k2eAgInPc1d1	labský
pískovce	pískovec	k1gInPc1	pískovec
<g/>
,	,	kIx,	,
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
České	český	k2eAgFnSc2d1	Česká
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
</s>
