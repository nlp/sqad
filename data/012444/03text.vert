<p>
<s>
Versailleská	versailleský	k2eAgFnSc1d1	Versailleská
mírová	mírový	k2eAgFnSc1d1	mírová
smlouva	smlouva	k1gFnSc1	smlouva
byla	být	k5eAaImAgFnS	být
nejdůležitější	důležitý	k2eAgFnSc1d3	nejdůležitější
ze	z	k7c2	z
šesti	šest	k4xCc2	šest
mírových	mírový	k2eAgFnPc2d1	mírová
smluv	smlouva	k1gFnPc2	smlouva
uzavřených	uzavřený	k2eAgFnPc2d1	uzavřená
roku	rok	k1gInSc3	rok
1919	[number]	k4	1919
jako	jako	k9	jako
výsledek	výsledek	k1gInSc1	výsledek
šestiměsíčního	šestiměsíční	k2eAgNnSc2d1	šestiměsíční
jednání	jednání	k1gNnSc2	jednání
na	na	k7c6	na
Pařížské	pařížský	k2eAgFnSc6d1	Pařížská
mírové	mírový	k2eAgFnSc6d1	mírová
konferenci	konference	k1gFnSc6	konference
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc7	který
byla	být	k5eAaImAgFnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
ukončena	ukončit	k5eAaPmNgFnS	ukončit
první	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
mezi	mezi	k7c7	mezi
Státy	stát	k1gInPc7	stát
Dohody	dohoda	k1gFnSc2	dohoda
a	a	k8xC	a
Centrálními	centrální	k2eAgFnPc7d1	centrální
mocnostmi	mocnost	k1gFnPc7	mocnost
<g/>
.	.	kIx.	.
</s>
<s>
Jednání	jednání	k1gNnSc1	jednání
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
smlouvě	smlouva	k1gFnSc6	smlouva
navazovalo	navazovat	k5eAaImAgNnS	navazovat
na	na	k7c4	na
příměří	příměří	k1gNnSc4	příměří
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
uzavřeno	uzavřít	k5eAaPmNgNnS	uzavřít
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1918	[number]	k4	1918
v	v	k7c6	v
Compiè	Compiè	k1gFnSc6	Compiè
lese	les	k1gInSc6	les
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Smlouva	smlouva	k1gFnSc1	smlouva
v	v	k7c6	v
článku	článek	k1gInSc6	článek
231	[number]	k4	231
stanovila	stanovit	k5eAaPmAgFnS	stanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Německá	německý	k2eAgFnSc1d1	německá
říše	říše	k1gFnSc1	říše
a	a	k8xC	a
její	její	k3xOp3gMnPc1	její
spojenci	spojenec	k1gMnPc1	spojenec
jsou	být	k5eAaImIp3nP	být
jako	jako	k9	jako
původci	původce	k1gMnPc1	původce
války	válka	k1gFnSc2	válka
odpovědni	odpověden	k2eAgMnPc1d1	odpověden
za	za	k7c4	za
všechny	všechen	k3xTgFnPc4	všechen
ztráty	ztráta	k1gFnPc4	ztráta
a	a	k8xC	a
škody	škoda	k1gFnPc4	škoda
na	na	k7c6	na
civilním	civilní	k2eAgInSc6d1	civilní
majetku	majetek	k1gInSc6	majetek
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
utrpěly	utrpět	k5eAaPmAgInP	utrpět
státy	stát	k1gInPc1	stát
spojené	spojený	k2eAgInPc1d1	spojený
a	a	k8xC	a
sdružené	sdružený	k2eAgInPc4d1	sdružený
v	v	k7c6	v
Dohodě	dohoda	k1gFnSc6	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Německu	Německo	k1gNnSc3	Německo
bylo	být	k5eAaImAgNnS	být
uloženo	uložit	k5eAaPmNgNnS	uložit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zaplatilo	zaplatit	k5eAaPmAgNnS	zaplatit
státům	stát	k1gInPc3	stát
Dohody	dohoda	k1gFnSc2	dohoda
značné	značný	k2eAgFnSc2d1	značná
válečné	válečný	k2eAgFnSc2d1	válečná
reparace	reparace	k1gFnSc2	reparace
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
mělo	mít	k5eAaImAgNnS	mít
Německo	Německo	k1gNnSc4	Německo
splácet	splácet	k5eAaImF	splácet
132	[number]	k4	132
miliard	miliarda	k4xCgFnPc2	miliarda
zlatých	zlatá	k1gFnPc2	zlatá
marek	marka	k1gFnPc2	marka
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
podle	podle	k7c2	podle
Londýnského	londýnský	k2eAgInSc2d1	londýnský
rozvrhu	rozvrh	k1gInSc2	rozvrh
plateb	platba	k1gFnPc2	platba
(	(	kIx(	(
<g/>
London	London	k1gMnSc1	London
Schedule	Schedule	k1gFnSc2	Schedule
of	of	k?	of
Payments	Payments	k1gInSc1	Payments
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
33	[number]	k4	33
miliardám	miliarda	k4xCgFnPc3	miliarda
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
však	však	k9	však
byla	být	k5eAaImAgFnS	být
Německu	Německo	k1gNnSc6	Německo
část	část	k1gFnSc1	část
uložených	uložený	k2eAgFnPc2d1	uložená
reparací	reparace	k1gFnPc2	reparace
prominuta	prominut	k2eAgFnSc1d1	prominuta
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
ztratila	ztratit	k5eAaPmAgFnS	ztratit
Německá	německý	k2eAgFnSc1d1	německá
říše	říše	k1gFnSc1	říše
část	část	k1gFnSc1	část
svého	svůj	k3xOyFgNnSc2	svůj
území	území	k1gNnSc2	území
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
všechny	všechen	k3xTgFnPc4	všechen
své	svůj	k3xOyFgFnPc4	svůj
dosavadní	dosavadní	k2eAgFnPc4d1	dosavadní
kolonie	kolonie	k1gFnPc4	kolonie
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
Oceánii	Oceánie	k1gFnSc6	Oceánie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
smlouvě	smlouva	k1gFnSc6	smlouva
byly	být	k5eAaImAgFnP	být
poválečné	poválečný	k2eAgFnPc1d1	poválečná
hranice	hranice	k1gFnPc1	hranice
Německa	Německo	k1gNnSc2	Německo
přesně	přesně	k6eAd1	přesně
definovány	definovat	k5eAaBmNgFnP	definovat
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
byla	být	k5eAaImAgFnS	být
německá	německý	k2eAgFnSc1d1	německá
armáda	armáda	k1gFnSc1	armáda
(	(	kIx(	(
<g/>
Reichswehr	Reichswehr	k1gInSc1	Reichswehr
<g/>
)	)	kIx)	)
podmínkami	podmínka	k1gFnPc7	podmínka
smlouvy	smlouva	k1gFnSc2	smlouva
omezena	omezit	k5eAaPmNgFnS	omezit
na	na	k7c4	na
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Smlouva	smlouva	k1gFnSc1	smlouva
byla	být	k5eAaImAgFnS	být
podepsána	podepsán	k2eAgFnSc1d1	podepsána
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1919	[number]	k4	1919
v	v	k7c6	v
Zrcadlovém	zrcadlový	k2eAgInSc6d1	zrcadlový
sále	sál	k1gInSc6	sál
zámku	zámek	k1gInSc2	zámek
ve	v	k7c6	v
Versailles	Versailles	k1gFnSc6	Versailles
blízko	blízko	k7c2	blízko
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
posléze	posléze	k6eAd1	posléze
10	[number]	k4	10
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1920	[number]	k4	1920
ratifikována	ratifikovat	k5eAaBmNgFnS	ratifikovat
nově	nově	k6eAd1	nově
vzniklou	vzniklý	k2eAgFnSc7d1	vzniklá
Společností	společnost	k1gFnSc7	společnost
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
členem	člen	k1gInSc7	člen
Německo	Německo	k1gNnSc1	Německo
tehdy	tehdy	k6eAd1	tehdy
nebylo	být	k5eNaImAgNnS	být
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
Československé	československý	k2eAgFnSc6d1	Československá
republice	republika	k1gFnSc6	republika
byl	být	k5eAaImAgInS	být
text	text	k1gInSc1	text
smlouvy	smlouva	k1gFnSc2	smlouva
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
ve	v	k7c6	v
Sbírce	sbírka	k1gFnSc6	sbírka
zákonů	zákon	k1gInPc2	zákon
pod	pod	k7c7	pod
č.	č.	k?	č.
217	[number]	k4	217
<g/>
/	/	kIx~	/
<g/>
1921	[number]	k4	1921
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podmínky	podmínka	k1gFnPc1	podmínka
smlouvy	smlouva	k1gFnSc2	smlouva
==	==	k?	==
</s>
</p>
<p>
<s>
Smlouva	smlouva	k1gFnSc1	smlouva
ustanovila	ustanovit	k5eAaPmAgFnS	ustanovit
vznik	vznik	k1gInSc4	vznik
Společnosti	společnost	k1gFnSc2	společnost
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgInSc1d1	hlavní
cíl	cíl	k1gInSc1	cíl
amerického	americký	k2eAgMnSc2d1	americký
prezidenta	prezident	k1gMnSc2	prezident
Woodrowa	Woodrowus	k1gMnSc2	Woodrowus
Wilsona	Wilson	k1gMnSc2	Wilson
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
národů	národ	k1gInPc2	národ
měla	mít	k5eAaImAgFnS	mít
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
arbitrážní	arbitrážní	k2eAgMnSc1d1	arbitrážní
soudce	soudce	k1gMnSc1	soudce
mezi	mezi	k7c7	mezi
národy	národ	k1gInPc7	národ
a	a	k8xC	a
předejít	předejít	k5eAaPmF	předejít
tak	tak	k8xS	tak
vzniku	vznik	k1gInSc3	vznik
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podmínky	podmínka	k1gFnPc1	podmínka
smlouvy	smlouva	k1gFnSc2	smlouva
zahrnovaly	zahrnovat	k5eAaImAgFnP	zahrnovat
ztrátu	ztráta	k1gFnSc4	ztráta
německých	německý	k2eAgFnPc2d1	německá
kolonií	kolonie	k1gFnPc2	kolonie
a	a	k8xC	a
ztrátu	ztráta	k1gFnSc4	ztráta
německého	německý	k2eAgNnSc2d1	německé
území	území	k1gNnSc2	území
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Alsasko-Lotrinsko	Alsasko-Lotrinsko	k1gNnSc1	Alsasko-Lotrinsko
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Alsace-Lorraine	Alsace-Lorrain	k1gMnSc5	Alsace-Lorrain
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Elsass-Lothringen	Elsass-Lothringen	k1gInSc1	Elsass-Lothringen
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
předáno	předat	k5eAaPmNgNnS	předat
Francii	Francie	k1gFnSc6	Francie
(	(	kIx(	(
<g/>
rozloha	rozloha	k1gFnSc1	rozloha
14	[number]	k4	14
522	[number]	k4	522
km2	km2	k4	km2
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
1	[number]	k4	1
815	[number]	k4	815
000	[number]	k4	000
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Alsasko-Lotrinsko	Alsasko-Lotrinsko	k1gNnSc1	Alsasko-Lotrinsko
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
územím	území	k1gNnSc7	území
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Severní	severní	k2eAgNnSc4d1	severní
Šlesvicko	Šlesvicko	k1gNnSc4	Šlesvicko
(	(	kIx(	(
<g/>
dánsky	dánsky	k6eAd1	dánsky
Nordslesvig	Nordslesvig	k1gInSc1	Nordslesvig
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Nordschleswig	Nordschleswiga	k1gFnPc2	Nordschleswiga
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
šlesvických	šlesvický	k2eAgInPc6d1	šlesvický
plebiscitech	plebiscit	k1gInPc6	plebiscit
předáno	předat	k5eAaPmNgNnS	předat
Dánsku	Dánsko	k1gNnSc3	Dánsko
(	(	kIx(	(
<g/>
3	[number]	k4	3
228	[number]	k4	228
km2	km2	k4	km2
nebo	nebo	k8xC	nebo
3	[number]	k4	3
938	[number]	k4	938
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Velkopolském	velkopolský	k2eAgNnSc6d1	Velkopolské
povstání	povstání	k1gNnSc6	povstání
připadla	připadnout	k5eAaPmAgFnS	připadnout
Polsku	Polska	k1gFnSc4	Polska
většina	většina	k1gFnSc1	většina
území	území	k1gNnSc2	území
provincie	provincie	k1gFnSc2	provincie
Poznaňska	Poznaňsk	k1gInSc2	Poznaňsk
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
prowincja	prowincj	k2eAgFnSc1d1	prowincj
Poznańska	Poznańska	k1gFnSc1	Poznańska
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Provinz	Provinz	k1gInSc1	Provinz
Posen	Posna	k1gFnPc2	Posna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
Polsku	Polska	k1gFnSc4	Polska
připadla	připadnout	k5eAaPmAgFnS	připadnout
většina	většina	k1gFnSc1	většina
Západního	západní	k2eAgNnSc2d1	západní
Pruska	Prusko	k1gNnSc2	Prusko
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
Polsko	Polsko	k1gNnSc1	Polsko
získalo	získat	k5eAaPmAgNnS	získat
53	[number]	k4	53
800	[number]	k4	800
km2	km2	k4	km2
a	a	k8xC	a
4	[number]	k4	4
224	[number]	k4	224
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
počítaje	počítat	k5eAaImSgMnS	počítat
v	v	k7c6	v
to	ten	k3xDgNnSc1	ten
510	[number]	k4	510
km2	km2	k4	km2
a	a	k8xC	a
26	[number]	k4	26
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
z	z	k7c2	z
Horního	horní	k2eAgNnSc2d1	horní
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Východní	východní	k2eAgFnSc1d1	východní
část	část	k1gFnSc1	část
Horního	horní	k2eAgNnSc2d1	horní
Slezska	Slezsko	k1gNnSc2	Slezsko
po	po	k7c6	po
Hornoslezských	hornoslezský	k2eAgNnPc6d1	hornoslezské
povstáních	povstání	k1gNnPc6	povstání
a	a	k8xC	a
plebiscitu	plebiscit	k1gInSc2	plebiscit
připadla	připadnout	k5eAaPmAgFnS	připadnout
Polsku	Polska	k1gFnSc4	Polska
(	(	kIx(	(
<g/>
rozloha	rozloha	k1gFnSc1	rozloha
3	[number]	k4	3
214	[number]	k4	214
km2	km2	k4	km2
počet	počet	k1gInSc4	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
965	[number]	k4	965
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlučínsko	Hlučínsko	k1gNnSc1	Hlučínsko
(	(	kIx(	(
<g/>
oblast	oblast	k1gFnSc1	oblast
Horního	horní	k2eAgNnSc2d1	horní
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
)	)	kIx)	)
připadlo	připadnout	k5eAaPmAgNnS	připadnout
Československu	Československo	k1gNnSc6	Československo
(	(	kIx(	(
<g/>
316	[number]	k4	316
nebo	nebo	k8xC	nebo
333	[number]	k4	333
km2	km2	k4	km2
a	a	k8xC	a
49	[number]	k4	49
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Německá	německý	k2eAgNnPc1d1	německé
města	město	k1gNnPc1	město
Eupen	Eupna	k1gFnPc2	Eupna
a	a	k8xC	a
Malmedy	Malmeda	k1gMnSc2	Malmeda
připadla	připadnout	k5eAaPmAgFnS	připadnout
Belgii	Belgie	k1gFnSc3	Belgie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
Działdowo	Działdowo	k1gNnSc1	Działdowo
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Soldau	Soldaa	k1gFnSc4	Soldaa
<g/>
,	,	kIx,	,
železniční	železniční	k2eAgFnPc4d1	železniční
stanice	stanice	k1gFnPc4	stanice
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
Varšava	Varšava	k1gFnSc1	Varšava
<g/>
–	–	k?	–
<g/>
Gdaňsk	Gdaňsk	k1gInSc4	Gdaňsk
<g/>
)	)	kIx)	)
ve	v	k7c6	v
Východním	východní	k2eAgNnSc6d1	východní
Prusku	Prusko	k1gNnSc6	Prusko
připadlo	připadnout	k5eAaPmAgNnS	připadnout
Polsku	Polska	k1gFnSc4	Polska
(	(	kIx(	(
<g/>
plocha	plocha	k1gFnSc1	plocha
492	[number]	k4	492
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
Východního	východní	k2eAgNnSc2d1	východní
Pruska	Prusko	k1gNnSc2	Prusko
(	(	kIx(	(
<g/>
Klaipė	Klaipė	k1gFnSc1	Klaipė
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
dána	dát	k5eAaPmNgFnS	dát
pod	pod	k7c4	pod
kontrolu	kontrola	k1gFnSc4	kontrola
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
stala	stát	k5eAaPmAgFnS	stát
částí	část	k1gFnSc7	část
Litvy	Litva	k1gFnSc2	Litva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Plebiscit	plebiscit	k1gInSc1	plebiscit
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
Západního	západní	k2eAgNnSc2d1	západní
Pruska	Prusko	k1gNnSc2	Prusko
a	a	k8xC	a
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
Východního	východní	k2eAgNnSc2d1	východní
Pruska	Prusko	k1gNnSc2	Prusko
(	(	kIx(	(
<g/>
Varmie	Varmie	k1gFnSc1	Varmie
a	a	k8xC	a
Mazursko	Mazursko	k1gNnSc1	Mazursko
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
Varmijsko-mazurské	Varmijskoazurský	k2eAgNnSc1d1	Varmijsko-mazurský
vojvodství	vojvodství	k1gNnSc1	vojvodství
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
několik	několik	k4yIc1	několik
vesnic	vesnice	k1gFnPc2	vesnice
připadlo	připadnout	k5eAaPmAgNnS	připadnout
Polsku	Polska	k1gFnSc4	Polska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oblast	oblast	k1gFnSc1	oblast
Sárska	Sársko	k1gNnSc2	Sársko
byla	být	k5eAaImAgFnS	být
dána	dát	k5eAaPmNgFnS	dát
pod	pod	k7c4	pod
kontrolu	kontrola	k1gFnSc4	kontrola
Společnosti	společnost	k1gFnSc2	společnost
národů	národ	k1gInPc2	národ
na	na	k7c4	na
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
plebiscit	plebiscit	k1gInSc1	plebiscit
mezi	mezi	k7c7	mezi
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
Gdaňsk	Gdaňsk	k1gInSc1	Gdaňsk
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Danzig	Danzig	k1gInSc1	Danzig
<g/>
)	)	kIx)	)
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
deltou	delta	k1gFnSc7	delta
řeky	řeka	k1gFnSc2	řeka
Visly	Visla	k1gFnSc2	Visla
bylo	být	k5eAaImAgNnS	být
ustanoveno	ustanovit	k5eAaPmNgNnS	ustanovit
jako	jako	k8xS	jako
Svobodné	svobodný	k2eAgNnSc1d1	svobodné
město	město	k1gNnSc1	město
Gdaňsk	Gdaňsk	k1gInSc1	Gdaňsk
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Freie	Freie	k1gFnSc1	Freie
Stadt	Stadt	k1gMnSc1	Stadt
Danzig	Danzig	k1gMnSc1	Danzig
<g/>
)	)	kIx)	)
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
Společnosti	společnost	k1gFnSc2	společnost
národů	národ	k1gInPc2	národ
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
polské	polský	k2eAgFnSc2d1	polská
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
rozloha	rozloha	k1gFnSc1	rozloha
1	[number]	k4	1
893	[number]	k4	893
km2	km2	k4	km2
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
408	[number]	k4	408
000	[number]	k4	000
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Článek	článek	k1gInSc1	článek
156	[number]	k4	156
smlouvy	smlouva	k1gFnSc2	smlouva
převedl	převést	k5eAaPmAgMnS	převést
německé	německý	k2eAgFnPc4d1	německá
koncese	koncese	k1gFnPc4	koncese
v	v	k7c6	v
čínské	čínský	k2eAgFnSc6d1	čínská
provincii	provincie	k1gFnSc6	provincie
Šan-tung	Šanung	k1gInSc1	Šan-tung
na	na	k7c4	na
Japonsko	Japonsko	k1gNnSc4	Japonsko
<g/>
,	,	kIx,	,
než	než	k8xS	než
aby	aby	kYmCp3nS	aby
navrátil	navrátit	k5eAaPmAgMnS	navrátit
suverenitu	suverenita	k1gFnSc4	suverenita
Číně	Čína	k1gFnSc3	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Znechucení	znechucení	k1gNnSc1	znechucení
Číňanů	Číňan	k1gMnPc2	Číňan
nad	nad	k7c7	nad
tímto	tento	k3xDgNnSc7	tento
ustanovením	ustanovení	k1gNnSc7	ustanovení
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
demonstracím	demonstrace	k1gFnPc3	demonstrace
a	a	k8xC	a
ke	k	k7c3	k
Hnutí	hnutí	k1gNnSc3	hnutí
4	[number]	k4	4
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
May	May	k1gMnSc1	May
Fourth	Fourth	k1gMnSc1	Fourth
Movement	Movement	k1gMnSc1	Movement
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
nakonec	nakonec	k6eAd1	nakonec
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Čína	Čína	k1gFnSc1	Čína
nepodepsala	podepsat	k5eNaPmAgFnS	podepsat
Versailleskou	versailleský	k2eAgFnSc4d1	Versailleská
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
deklarovala	deklarovat	k5eAaBmAgFnS	deklarovat
konec	konec	k1gInSc4	konec
války	válka	k1gFnSc2	válka
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
až	až	k9	až
v	v	k7c6	v
září	září	k1gNnSc6	září
1919	[number]	k4	1919
a	a	k8xC	a
podepsala	podepsat	k5eAaPmAgFnS	podepsat
separátní	separátní	k2eAgFnSc4d1	separátní
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VII	VII	kA	VII
<g/>
.	.	kIx.	.
oddíl	oddíl	k1gInSc1	oddíl
smlouvy	smlouva	k1gFnSc2	smlouva
(	(	kIx(	(
<g/>
články	článek	k1gInPc7	článek
81	[number]	k4	81
<g/>
-	-	kIx~	-
<g/>
86	[number]	k4	86
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
týkal	týkat	k5eAaImAgInS	týkat
nově	nově	k6eAd1	nově
ustaveného	ustavený	k2eAgNnSc2d1	ustavené
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
se	se	k3xPyFc4	se
zavázalo	zavázat	k5eAaPmAgNnS	zavázat
uznat	uznat	k5eAaPmF	uznat
Československou	československý	k2eAgFnSc4d1	Československá
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
<s>
Československo	Československo	k1gNnSc4	Československo
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
svolilo	svolit	k5eAaPmAgNnS	svolit
respektovat	respektovat	k5eAaImF	respektovat
ustanovení	ustanovení	k1gNnPc4	ustanovení
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
by	by	kYmCp3nP	by
vítězné	vítězný	k2eAgFnPc1d1	vítězná
mocnosti	mocnost	k1gFnPc1	mocnost
udělaly	udělat	k5eAaPmAgFnP	udělat
v	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
ochran	ochrana	k1gFnPc2	ochrana
menšin	menšina	k1gFnPc2	menšina
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Důležitými	důležitý	k2eAgNnPc7d1	důležité
ustanoveními	ustanovení	k1gNnPc7	ustanovení
Versailleské	versailleský	k2eAgFnPc1d1	Versailleská
smlouvy	smlouva	k1gFnPc1	smlouva
byly	být	k5eAaImAgFnP	být
přesné	přesný	k2eAgFnPc1d1	přesná
definice	definice	k1gFnPc1	definice
československých	československý	k2eAgFnPc2d1	Československá
hranic	hranice	k1gFnPc2	hranice
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
a	a	k8xC	a
Rakouskem	Rakousko	k1gNnSc7	Rakousko
<g/>
.	.	kIx.	.
<g/>
Vojenské	vojenský	k2eAgFnSc2d1	vojenská
podmínky	podmínka	k1gFnSc2	podmínka
Versailleské	versailleský	k2eAgFnSc2d1	Versailleská
smlouvy	smlouva	k1gFnSc2	smlouva
vůči	vůči	k7c3	vůči
Německu	Německo	k1gNnSc3	Německo
byly	být	k5eAaImAgInP	být
tvrdé	tvrdý	k2eAgInPc1d1	tvrdý
a	a	k8xC	a
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Francie	Francie	k1gFnSc2	Francie
byly	být	k5eAaImAgInP	být
motivovány	motivovat	k5eAaBmNgInP	motivovat
nejenom	nejenom	k6eAd1	nejenom
obavami	obava	k1gFnPc7	obava
z	z	k7c2	z
příštího	příští	k2eAgInSc2d1	příští
vývoje	vývoj	k1gInSc2	vývoj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
historiků	historik	k1gMnPc2	historik
také	také	k9	také
touhou	touha	k1gFnSc7	touha
po	po	k7c6	po
odplatě	odplata	k1gFnSc6	odplata
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
revanšismem	revanšismus	k1gInSc7	revanšismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
francouzsko-pruské	francouzskoruský	k2eAgFnSc2d1	francouzsko-pruská
války	válka	k1gFnSc2	válka
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1870-1871	[number]	k4	1870-1871
byla	být	k5eAaImAgFnS	být
Francie	Francie	k1gFnSc1	Francie
poražena	porazit	k5eAaPmNgFnS	porazit
a	a	k8xC	a
ztratila	ztratit	k5eAaPmAgFnS	ztratit
Alsasko-Lotrinsko	Alsasko-Lotrinsko	k1gNnSc4	Alsasko-Lotrinsko
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
negativní	negativní	k2eAgInSc4d1	negativní
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
postoje	postoj	k1gInPc4	postoj
značné	značný	k2eAgFnSc2d1	značná
části	část	k1gFnSc2	část
francouzské	francouzský	k2eAgFnSc2d1	francouzská
veřejnosti	veřejnost	k1gFnSc2	veřejnost
vůči	vůči	k7c3	vůči
Německému	německý	k2eAgNnSc3d1	německé
císařství	císařství	k1gNnSc3	císařství
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
utvrzení	utvrzení	k1gNnSc3	utvrzení
touhy	touha	k1gFnSc2	touha
po	po	k7c6	po
odplatě	odplata	k1gFnSc6	odplata
přispěly	přispět	k5eAaPmAgInP	přispět
také	také	k6eAd1	také
obrovský	obrovský	k2eAgInSc4d1	obrovský
počet	počet	k1gInSc4	počet
padlých	padlý	k1gMnPc2	padlý
a	a	k8xC	a
zraněných	zraněný	k2eAgMnPc2d1	zraněný
francouzských	francouzský	k2eAgMnPc2d1	francouzský
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
civilistů	civilista	k1gMnPc2	civilista
a	a	k8xC	a
nesmírné	smírný	k2eNgFnPc4d1	nesmírná
ekonomické	ekonomický	k2eAgFnPc4d1	ekonomická
škody	škoda	k1gFnPc4	škoda
způsobené	způsobený	k2eAgFnPc4d1	způsobená
první	první	k4xOgFnSc7	první
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
na	na	k7c6	na
území	území	k1gNnSc6	území
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
Německa	Německo	k1gNnSc2	Německo
neprobíhaly	probíhat	k5eNaImAgFnP	probíhat
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
válce	válka	k1gFnSc6	válka
naopak	naopak	k6eAd1	naopak
téměř	téměř	k6eAd1	téměř
žádné	žádný	k3yNgFnPc4	žádný
vojenské	vojenský	k2eAgFnPc4d1	vojenská
operace	operace	k1gFnPc4	operace
<g/>
.	.	kIx.	.
</s>
<s>
Obavy	obava	k1gFnPc1	obava
z	z	k7c2	z
budoucí	budoucí	k2eAgFnSc2d1	budoucí
německé	německý	k2eAgFnSc2d1	německá
agrese	agrese	k1gFnSc2	agrese
určovaly	určovat	k5eAaImAgFnP	určovat
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
poválečného	poválečný	k2eAgNnSc2d1	poválečné
jednání	jednání	k1gNnSc2	jednání
Francie	Francie	k1gFnSc2	Francie
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Německá	německý	k2eAgFnSc1d1	německá
armáda	armáda	k1gFnSc1	armáda
byla	být	k5eAaImAgFnS	být
ustanoveními	ustanovení	k1gNnPc7	ustanovení
Versailleské	versailleský	k2eAgFnSc2d1	Versailleská
smlouvy	smlouva	k1gFnSc2	smlouva
omezena	omezit	k5eAaPmNgFnS	omezit
na	na	k7c4	na
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
branná	branný	k2eAgFnSc1d1	Branná
povinnost	povinnost	k1gFnSc1	povinnost
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
byla	být	k5eAaImAgFnS	být
zakázána	zakázat	k5eAaPmNgFnS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
nesmělo	smět	k5eNaImAgNnS	smět
mít	mít	k5eAaImF	mít
žádné	žádný	k3yNgInPc4	žádný
tanky	tank	k1gInPc4	tank
<g/>
,	,	kIx,	,
žádné	žádný	k3yNgNnSc1	žádný
těžké	těžký	k2eAgNnSc1d1	těžké
dělostřelectvo	dělostřelectvo	k1gNnSc1	dělostřelectvo
a	a	k8xC	a
žádný	žádný	k3yNgInSc1	žádný
generální	generální	k2eAgInSc1d1	generální
štáb	štáb	k1gInSc1	štáb
(	(	kIx(	(
<g/>
Generalstab	Generalstab	k1gInSc1	Generalstab
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Německé	německý	k2eAgNnSc1d1	německé
vojenské	vojenský	k2eAgNnSc1d1	vojenské
námořnictvo	námořnictvo	k1gNnSc1	námořnictvo
bylo	být	k5eAaImAgNnS	být
omezeno	omezit	k5eAaPmNgNnS	omezit
na	na	k7c4	na
15	[number]	k4	15
tisíc	tisíc	k4xCgInPc2	tisíc
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
mohlo	moct	k5eAaImAgNnS	moct
mít	mít	k5eAaImF	mít
pouze	pouze	k6eAd1	pouze
šest	šest	k4xCc4	šest
bitevních	bitevní	k2eAgFnPc2d1	bitevní
lodí	loď	k1gFnPc2	loď
(	(	kIx(	(
<g/>
s	s	k7c7	s
výtlakem	výtlak	k1gInSc7	výtlak
méně	málo	k6eAd2	málo
než	než	k8xS	než
10	[number]	k4	10
tisíc	tisíc	k4xCgInSc4	tisíc
tun	tuna	k1gFnPc2	tuna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šest	šest	k4xCc4	šest
křižníků	křižník	k1gInPc2	křižník
a	a	k8xC	a
dvanáct	dvanáct	k4xCc1	dvanáct
torpédoborců	torpédoborec	k1gMnPc2	torpédoborec
<g/>
.	.	kIx.	.
</s>
<s>
Ponorky	ponorka	k1gFnPc1	ponorka
byly	být	k5eAaImAgFnP	být
Německu	Německo	k1gNnSc6	Německo
zcela	zcela	k6eAd1	zcela
zakázány	zakázat	k5eAaPmNgInP	zakázat
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
jako	jako	k8xS	jako
vojenské	vojenský	k2eAgNnSc1d1	vojenské
letectvo	letectvo	k1gNnSc1	letectvo
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
smlouva	smlouva	k1gFnSc1	smlouva
stanovila	stanovit	k5eAaPmAgFnS	stanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
řadoví	řadový	k2eAgMnPc1d1	řadový
vojáci	voják	k1gMnPc1	voják
nesmí	smět	k5eNaImIp3nP	smět
sloužit	sloužit	k5eAaImF	sloužit
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
kratší	krátký	k2eAgFnSc4d2	kratší
než	než	k8xS	než
12	[number]	k4	12
let	léto	k1gNnPc2	léto
a	a	k8xC	a
důstojníci	důstojník	k1gMnPc1	důstojník
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
kratší	krátký	k2eAgFnSc4d2	kratší
než	než	k8xS	než
25	[number]	k4	25
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
mít	mít	k5eAaImF	mít
ten	ten	k3xDgInSc4	ten
účinek	účinek	k1gInSc4	účinek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohlo	moct	k5eAaImAgNnS	moct
pouze	pouze	k6eAd1	pouze
omezené	omezený	k2eAgNnSc1d1	omezené
množství	množství	k1gNnSc1	množství
mužů	muž	k1gMnPc2	muž
procházet	procházet	k5eAaImF	procházet
vojenským	vojenský	k2eAgInSc7d1	vojenský
výcvikem	výcvik	k1gInSc7	výcvik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
231	[number]	k4	231
smlouvy	smlouva	k1gFnSc2	smlouva
(	(	kIx(	(
<g/>
klausule	klausule	k1gFnSc1	klausule
o	o	k7c6	o
válečné	válečný	k2eAgFnSc6d1	válečná
vině	vina	k1gFnSc6	vina
<g/>
)	)	kIx)	)
připisovala	připisovat	k5eAaImAgFnS	připisovat
Německu	Německo	k1gNnSc6	Německo
zodpovědnost	zodpovědnost	k1gFnSc4	zodpovědnost
za	za	k7c4	za
"	"	kIx"	"
<g/>
ztráty	ztráta	k1gFnSc2	ztráta
a	a	k8xC	a
utrpení	utrpení	k1gNnSc2	utrpení
<g/>
"	"	kIx"	"
států	stát	k1gInPc2	stát
Dohody	dohoda	k1gFnSc2	dohoda
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
článek	článek	k1gInSc1	článek
pak	pak	k6eAd1	pak
poskytl	poskytnout	k5eAaPmAgInS	poskytnout
základ	základ	k1gInSc1	základ
pro	pro	k7c4	pro
válečné	válečný	k2eAgFnPc4d1	válečná
reparace	reparace	k1gFnPc4	reparace
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
celková	celkový	k2eAgFnSc1d1	celková
suma	suma	k1gFnSc1	suma
byla	být	k5eAaImAgFnS	být
určena	určit	k5eAaPmNgFnS	určit
komisí	komise	k1gFnSc7	komise
zastupující	zastupující	k2eAgInPc1d1	zastupující
Dohodové	dohodový	k2eAgInPc1d1	dohodový
státy	stát	k1gInPc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1921	[number]	k4	1921
byly	být	k5eAaImAgFnP	být
reparace	reparace	k1gFnPc1	reparace
vyčísleny	vyčíslen	k2eAgFnPc1d1	vyčíslena
na	na	k7c4	na
269	[number]	k4	269
miliard	miliarda	k4xCgFnPc2	miliarda
zlatých	zlatá	k1gFnPc2	zlatá
marek	marka	k1gFnPc2	marka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
většina	většina	k1gFnSc1	většina
ekonomů	ekonom	k1gMnPc2	ekonom
považovala	považovat	k5eAaImAgFnS	považovat
za	za	k7c4	za
přehnané	přehnaný	k2eAgFnPc4d1	přehnaná
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
částka	částka	k1gFnSc1	částka
zredukována	zredukovat	k5eAaPmNgFnS	zredukovat
na	na	k7c4	na
132	[number]	k4	132
miliard	miliarda	k4xCgFnPc2	miliarda
zlatých	zlatá	k1gFnPc2	zlatá
marek	marka	k1gFnPc2	marka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
většina	většina	k1gFnSc1	většina
Němců	Němec	k1gMnPc2	Němec
považovala	považovat	k5eAaImAgFnS	považovat
za	za	k7c4	za
neúměrnou	úměrný	k2eNgFnSc4d1	neúměrná
sumu	suma	k1gFnSc4	suma
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomické	ekonomický	k2eAgInPc1d1	ekonomický
problémy	problém	k1gInPc1	problém
způsobené	způsobený	k2eAgInPc1d1	způsobený
placením	placení	k1gNnSc7	placení
reparací	reparace	k1gFnPc2	reparace
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
odporem	odpor	k1gInSc7	odpor
Němců	Němec	k1gMnPc2	Němec
vůbec	vůbec	k9	vůbec
je	být	k5eAaImIp3nS	být
platit	platit	k5eAaImF	platit
jsou	být	k5eAaImIp3nP	být
některými	některý	k3yIgMnPc7	některý
historiky	historik	k1gMnPc7	historik
uváděny	uvádět	k5eAaImNgFnP	uvádět
jako	jako	k9	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
příčin	příčina	k1gFnPc2	příčina
politické	politický	k2eAgFnSc2d1	politická
a	a	k8xC	a
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
Výmarské	výmarský	k2eAgFnSc2d1	Výmarská
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
následného	následný	k2eAgNnSc2d1	následné
převzetí	převzetí	k1gNnSc2	převzetí
moci	moc	k1gFnSc2	moc
Adolfem	Adolf	k1gMnSc7	Adolf
Hitlerem	Hitler	k1gMnSc7	Hitler
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k8xC	i
vypuknutí	vypuknutí	k1gNnSc4	vypuknutí
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Za	za	k7c4	za
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
americké	americký	k2eAgFnSc2d1	americká
sice	sice	k8xC	sice
prezident	prezident	k1gMnSc1	prezident
Woodrow	Woodrow	k1gMnSc1	Woodrow
Wilson	Wilson	k1gMnSc1	Wilson
Versailleskou	versailleský	k2eAgFnSc4d1	Versailleská
smlouvu	smlouva	k1gFnSc4	smlouva
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
podepsal	podepsat	k5eAaPmAgMnS	podepsat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
Senát	senát	k1gInSc1	senát
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
ji	on	k3xPp3gFnSc4	on
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
ratifikovat	ratifikovat	k5eAaBmF	ratifikovat
<g/>
.	.	kIx.	.
</s>
<s>
Pensylvánský	pensylvánský	k2eAgMnSc1d1	pensylvánský
senátor	senátor	k1gMnSc1	senátor
Philander	Philander	k1gMnSc1	Philander
Knox	Knox	k1gInSc4	Knox
Wilsonovi	Wilson	k1gMnSc3	Wilson
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Pane	Pan	k1gMnSc5	Pan
prezidente	prezident	k1gMnSc5	prezident
<g/>
,	,	kIx,	,
po	po	k7c6	po
důkladném	důkladný	k2eAgNnSc6d1	důkladné
zvážení	zvážení	k1gNnSc6	zvážení
jsem	být	k5eAaImIp1nS	být
dospěl	dochvít	k5eAaPmAgMnS	dochvít
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
smlouva	smlouva	k1gFnSc1	smlouva
není	být	k5eNaImIp3nS	být
předzvěstí	předzvěst	k1gFnSc7	předzvěst
míru	mír	k1gInSc2	mír
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
bude	být	k5eAaImBp3nS	být
mnohem	mnohem	k6eAd1	mnohem
ničivější	ničivý	k2eAgFnSc1d2	ničivější
než	než	k8xS	než
ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
již	jenž	k3xRgFnSc4	jenž
jsme	být	k5eAaImIp1nP	být
právě	právě	k6eAd1	právě
ukončili	ukončit	k5eAaPmAgMnP	ukončit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Volby	volba	k1gFnSc2	volba
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
Republikánská	republikánský	k2eAgFnSc1d1	republikánská
strana	strana	k1gFnSc1	strana
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
získala	získat	k5eAaPmAgFnS	získat
převahu	převaha	k1gFnSc4	převaha
v	v	k7c6	v
Senátě	senát	k1gInSc6	senát
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
byla	být	k5eAaImAgFnS	být
republikány	republikán	k1gMnPc4	republikán
v	v	k7c6	v
Senátě	senát	k1gInSc6	senát
ratifikace	ratifikace	k1gFnSc2	ratifikace
smlouvy	smlouva	k1gFnSc2	smlouva
dvakrát	dvakrát	k6eAd1	dvakrát
zablokována	zablokován	k2eAgFnSc1d1	zablokována
(	(	kIx(	(
<g/>
podruhé	podruhé	k6eAd1	podruhé
19	[number]	k4	19
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
upřednostňování	upřednostňování	k1gNnSc2	upřednostňování
izolacionismu	izolacionismus	k1gInSc2	izolacionismus
spojeného	spojený	k2eAgInSc2d1	spojený
s	s	k7c7	s
neuznáváním	neuznávání	k1gNnSc7	neuznávání
Společnosti	společnost	k1gFnSc2	společnost
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
kvůli	kvůli	k7c3	kvůli
z	z	k7c2	z
jejich	jejich	k3xOp3gNnSc2	jejich
hlediska	hledisko	k1gNnSc2	hledisko
neúměrné	úměrný	k2eNgFnSc6d1	neúměrná
výšce	výška	k1gFnSc6	výška
reparací	reparace	k1gFnPc2	reparace
uložených	uložený	k2eAgFnPc2d1	uložená
Německu	Německo	k1gNnSc3	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
se	se	k3xPyFc4	se
k	k	k7c3	k
předválečné	předválečný	k2eAgFnSc3d1	předválečná
Společnosti	společnost	k1gFnSc3	společnost
národů	národ	k1gInPc2	národ
nikdy	nikdy	k6eAd1	nikdy
nepřipojily	připojit	k5eNaPmAgFnP	připojit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
podepsaly	podepsat	k5eAaPmAgInP	podepsat
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
Berlínskou	berlínský	k2eAgFnSc4d1	Berlínská
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Německou	německý	k2eAgFnSc7d1	německá
říší	říš	k1gFnSc7	říš
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
sice	sice	k8xC	sice
byly	být	k5eAaImAgInP	být
potvrzeny	potvrdit	k5eAaPmNgInP	potvrdit
stanovené	stanovený	k2eAgInPc1d1	stanovený
reparace	reparace	k1gFnPc4	reparace
a	a	k8xC	a
další	další	k2eAgNnPc4d1	další
ustanovení	ustanovení	k1gNnPc4	ustanovení
Versailleské	versailleský	k2eAgFnSc2d1	Versailleská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebyly	být	k5eNaImAgInP	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
obsaženy	obsáhnout	k5eAaPmNgFnP	obsáhnout
žádné	žádný	k3yNgFnPc1	žádný
pasáže	pasáž	k1gFnPc1	pasáž
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
pojednávaly	pojednávat	k5eAaImAgFnP	pojednávat
o	o	k7c6	o
Společnosti	společnost	k1gFnSc6	společnost
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nevyhovující	vyhovující	k2eNgInSc1d1	nevyhovující
kompromis	kompromis	k1gInSc1	kompromis
mezi	mezi	k7c7	mezi
vítězi	vítěz	k1gMnPc7	vítěz
==	==	k?	==
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Velká	velký	k2eAgFnSc1d1	velká
Trojka	trojka	k1gFnSc1	trojka
<g/>
"	"	kIx"	"
sestávala	sestávat	k5eAaImAgFnS	sestávat
z	z	k7c2	z
premiéra	premiér	k1gMnSc2	premiér
Francie	Francie	k1gFnSc1	Francie
Georgese	Georgese	k1gFnSc1	Georgese
Clemenceaua	Clemenceaua	k1gFnSc1	Clemenceaua
<g/>
,	,	kIx,	,
premiéra	premiéra	k1gFnSc1	premiéra
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
Davida	David	k1gMnSc2	David
Lloyda	Lloyd	k1gMnSc2	Lloyd
Georgea	Georgeus	k1gMnSc2	Georgeus
a	a	k8xC	a
prezidenta	prezident	k1gMnSc2	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
Woodrowa	Woodrowa	k1gFnSc1	Woodrowa
Wilsona	Wilsona	k1gFnSc1	Wilsona
<g/>
.	.	kIx.	.
</s>
<s>
Italský	italský	k2eAgMnSc1d1	italský
premiér	premiér	k1gMnSc1	premiér
Vittorio	Vittorio	k1gMnSc1	Vittorio
Orlando	Orlanda	k1gFnSc5	Orlanda
byl	být	k5eAaImAgInS	být
přítomen	přítomen	k2eAgMnSc1d1	přítomen
jako	jako	k8xS	jako
poradce	poradce	k1gMnSc1	poradce
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
hrabě	hrabě	k1gMnSc1	hrabě
Makino	Makino	k1gNnSc4	Makino
byl	být	k5eAaImAgMnS	být
vyslán	vyslat	k5eAaPmNgMnS	vyslat
Japonskem	Japonsko	k1gNnSc7	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jednání	jednání	k1gNnPc2	jednání
bylo	být	k5eAaImAgNnS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
shody	shoda	k1gFnPc4	shoda
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
tedy	tedy	k9	tedy
byl	být	k5eAaImAgInS	být
kompromis	kompromis	k1gInSc1	kompromis
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
neuspokojil	uspokojit	k5eNaPmAgInS	uspokojit
nikoho	nikdo	k3yNnSc4	nikdo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Francie	Francie	k1gFnSc1	Francie
měla	mít	k5eAaImAgFnS	mít
během	během	k7c2	během
války	válka	k1gFnSc2	válka
velké	velký	k2eAgFnSc2d1	velká
ztráty	ztráta	k1gFnSc2	ztráta
na	na	k7c6	na
životech	život	k1gInPc6	život
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
1,24	[number]	k4	1,24
milionů	milion	k4xCgInPc2	milion
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
40	[number]	k4	40
000	[number]	k4	000
civilistů	civilista	k1gMnPc2	civilista
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
bojována	bojovat	k5eAaImNgFnS	bojovat
na	na	k7c6	na
francouzském	francouzský	k2eAgNnSc6d1	francouzské
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
a	a	k8xC	a
Francie	Francie	k1gFnSc1	Francie
tedy	tedy	k9	tedy
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
ruinách	ruina	k1gFnPc6	ruina
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
francouzský	francouzský	k2eAgMnSc1d1	francouzský
premiér	premiér	k1gMnSc1	premiér
Georges	Georges	k1gMnSc1	Georges
Clemenceau	Clemenceaa	k1gMnSc4	Clemenceaa
chtěl	chtít	k5eAaImAgMnS	chtít
po	po	k7c6	po
Německu	Německo	k1gNnSc6	Německo
válečné	válečný	k2eAgFnSc2d1	válečná
reparace	reparace	k1gFnSc2	reparace
na	na	k7c4	na
obnovu	obnova	k1gFnSc4	obnova
válkou	válka	k1gFnSc7	válka
zničené	zničený	k2eAgFnPc1d1	zničená
země	zem	k1gFnPc1	zem
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
750	[number]	k4	750
000	[number]	k4	000
domů	dům	k1gInPc2	dům
a	a	k8xC	a
23	[number]	k4	23
000	[number]	k4	000
továren	továrna	k1gFnPc2	továrna
bylo	být	k5eAaImAgNnS	být
zničeno	zničen	k2eAgNnSc1d1	zničeno
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
tedy	tedy	k9	tedy
potřeba	potřeba	k6eAd1	potřeba
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
peněz	peníze	k1gInPc2	peníze
na	na	k7c4	na
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Clemenceau	Clemenceau	k5eAaPmIp1nS	Clemenceau
se	se	k3xPyFc4	se
také	také	k9	také
chtěl	chtít	k5eAaImAgMnS	chtít
pojistit	pojistit	k5eAaPmF	pojistit
proti	proti	k7c3	proti
dalšímu	další	k2eAgInSc3d1	další
možnému	možný	k2eAgInSc3d1	možný
útoku	útok	k1gInSc3	útok
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
požadoval	požadovat	k5eAaImAgInS	požadovat
demilitarizaci	demilitarizace	k1gFnSc4	demilitarizace
Porýní	Porýní	k1gNnSc2	Porýní
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Rheinland	Rheinland	k1gInSc1	Rheinland
<g/>
,	,	kIx,	,
okolí	okolí	k1gNnSc1	okolí
řeky	řeka	k1gFnSc2	řeka
Rýn	rýna	k1gFnPc2	rýna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
požadoval	požadovat	k5eAaImAgMnS	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vojska	vojsko	k1gNnPc1	vojsko
Dohody	dohoda	k1gFnSc2	dohoda
kontrolovala	kontrolovat	k5eAaImAgNnP	kontrolovat
moře	moře	k1gNnPc1	moře
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
aby	aby	kYmCp3nS	aby
Francie	Francie	k1gFnSc1	Francie
mohla	moct	k5eAaImAgFnS	moct
kontrolovat	kontrolovat	k5eAaImF	kontrolovat
zámořský	zámořský	k2eAgInSc4d1	zámořský
obchod	obchod	k1gInSc4	obchod
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
chtěl	chtít	k5eAaImAgMnS	chtít
značné	značný	k2eAgNnSc4d1	značné
omezení	omezení	k1gNnSc4	omezení
německé	německý	k2eAgFnSc2d1	německá
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
válečných	válečný	k2eAgFnPc2d1	válečná
reparací	reparace	k1gFnPc2	reparace
požadoval	požadovat	k5eAaImAgMnS	požadovat
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
mnoha	mnoho	k4c7	mnoho
německými	německý	k2eAgFnPc7d1	německá
továrnami	továrna	k1gFnPc7	továrna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Francie	Francie	k1gFnSc1	Francie
chtěla	chtít	k5eAaImAgFnS	chtít
nejenom	nejenom	k6eAd1	nejenom
vážně	vážně	k6eAd1	vážně
zdecimovat	zdecimovat	k5eAaPmF	zdecimovat
Německo	Německo	k1gNnSc4	Německo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
chtěla	chtít	k5eAaImAgFnS	chtít
zachovat	zachovat	k5eAaPmF	zachovat
své	svůj	k3xOyFgNnSc4	svůj
velké	velký	k2eAgNnSc4d1	velké
impérium	impérium	k1gNnSc4	impérium
a	a	k8xC	a
své	své	k1gNnSc4	své
kolonie	kolonie	k1gFnSc2	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgFnPc1d1	americká
prosazovaly	prosazovat	k5eAaImAgFnP	prosazovat
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
právo	právo	k1gNnSc4	právo
národů	národ	k1gInPc2	národ
na	na	k7c4	na
sebeurčení	sebeurčení	k1gNnSc4	sebeurčení
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
a	a	k8xC	a
Velká	velká	k1gFnSc1	velká
Británie	Británie	k1gFnSc2	Británie
si	se	k3xPyFc3	se
chtěly	chtít	k5eAaImAgInP	chtít
zachovat	zachovat	k5eAaPmF	zachovat
svá	svůj	k3xOyFgNnPc4	svůj
impéria	impérium	k1gNnPc4	impérium
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výsledku	výsledek	k1gInSc6	výsledek
chtěl	chtít	k5eAaImAgMnS	chtít
Clemenceau	Clemenceaa	k1gFnSc4	Clemenceaa
(	(	kIx(	(
<g/>
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
mnoho	mnoho	k4c1	mnoho
Francouzů	Francouz	k1gMnPc2	Francouz
<g/>
)	)	kIx)	)
takové	takový	k3xDgFnPc4	takový
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zlomí	zlomit	k5eAaPmIp3nP	zlomit
Německo	Německo	k1gNnSc4	Německo
vojensky	vojensky	k6eAd1	vojensky
<g/>
,	,	kIx,	,
politicky	politicky	k6eAd1	politicky
a	a	k8xC	a
ekonomicky	ekonomicky	k6eAd1	ekonomicky
<g/>
.	.	kIx.	.
</s>
<s>
Clemenceau	Clemenceau	k6eAd1	Clemenceau
byl	být	k5eAaImAgMnS	být
nejradikálnější	radikální	k2eAgInSc4d3	nejradikálnější
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
trojky	trojka	k1gFnSc2	trojka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mu	on	k3xPp3gMnSc3	on
vyneslo	vynést	k5eAaPmAgNnS	vynést
přezdívku	přezdívka	k1gFnSc4	přezdívka
Tygr	tygr	k1gMnSc1	tygr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Požadavky	požadavek	k1gInPc1	požadavek
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
byly	být	k5eAaImAgInP	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
menší	malý	k2eAgMnSc1d2	menší
než	než	k8xS	než
požadavky	požadavek	k1gInPc1	požadavek
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
nebyla	být	k5eNaImAgFnS	být
napadena	napadnout	k5eAaPmNgFnS	napadnout
a	a	k8xC	a
neválčilo	válčit	k5eNaImAgNnS	válčit
se	se	k3xPyFc4	se
na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
mnoho	mnoho	k6eAd1	mnoho
britských	britský	k2eAgMnPc2d1	britský
vojáků	voják	k1gMnPc2	voják
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
během	během	k7c2	během
bojů	boj	k1gInPc2	boj
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
Britové	Brit	k1gMnPc1	Brit
chtěli	chtít	k5eAaImAgMnP	chtít
pomstít	pomstít	k5eAaPmF	pomstít
Německu	Německo	k1gNnSc6	Německo
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
jako	jako	k8xS	jako
Francouzi	Francouz	k1gMnPc1	Francouz
<g/>
.	.	kIx.	.
</s>
<s>
Britský	britský	k2eAgMnSc1d1	britský
premiér	premiér	k1gMnSc1	premiér
David	David	k1gMnSc1	David
Lloyd	Lloyd	k1gMnSc1	Lloyd
George	Georg	k1gMnSc4	Georg
také	také	k9	také
požadoval	požadovat	k5eAaImAgInS	požadovat
vysoké	vysoká	k1gFnSc3	vysoká
válečné	válečný	k2eAgFnSc2d1	válečná
reparace	reparace	k1gFnSc2	reparace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
Francie	Francie	k1gFnSc1	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Obával	obávat	k5eAaImAgInS	obávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
francouzské	francouzský	k2eAgFnPc1d1	francouzská
podmínky	podmínka	k1gFnPc1	podmínka
budou	být	k5eAaImBp3nP	být
splněny	splněn	k2eAgFnPc1d1	splněna
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
nesmírně	smírně	k6eNd1	smírně
mocnou	mocný	k2eAgFnSc4d1	mocná
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
nerovnováze	nerovnováha	k1gFnSc3	nerovnováha
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
Woodrow	Woodrow	k1gMnSc1	Woodrow
Wilson	Wilson	k1gMnSc1	Wilson
měl	mít	k5eAaImAgMnS	mít
velice	velice	k6eAd1	velice
rozdílný	rozdílný	k2eAgInSc4d1	rozdílný
pohled	pohled	k1gInSc4	pohled
v	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
potrestat	potrestat	k5eAaPmF	potrestat
Německo	Německo	k1gNnSc4	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
před	před	k7c7	před
koncem	konec	k1gInSc7	konec
války	válka	k1gFnSc2	válka
vypracoval	vypracovat	k5eAaPmAgInS	vypracovat
čtrnáct	čtrnáct	k4xCc4	čtrnáct
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nebyly	být	k5eNaImAgInP	být
tak	tak	k6eAd1	tak
tvrdé	tvrdý	k2eAgInPc1d1	tvrdý
jako	jako	k8xC	jako
požadavky	požadavek	k1gInPc1	požadavek
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
podobná	podobný	k2eAgFnSc1d1	podobná
válka	válka	k1gFnSc1	válka
nemohla	moct	k5eNaImAgFnS	moct
již	již	k6eAd1	již
opakovat	opakovat	k5eAaImF	opakovat
<g/>
,	,	kIx,	,
Woodrow	Woodrow	k1gMnSc1	Woodrow
Wilson	Wilson	k1gMnSc1	Wilson
chtěl	chtít	k5eAaImAgMnS	chtít
vytvořit	vytvořit	k5eAaPmF	vytvořit
Společnost	společnost	k1gFnSc4	společnost
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
myšlenka	myšlenka	k1gFnSc1	myšlenka
byla	být	k5eAaImAgFnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
slabší	slabý	k2eAgInSc1d2	slabší
národ	národ	k1gInSc1	národ
je	být	k5eAaImIp3nS	být
napaden	napaden	k2eAgMnSc1d1	napaden
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgMnSc1d1	ostatní
by	by	kYmCp3nP	by
mu	on	k3xPp3gMnSc3	on
zajistili	zajistit	k5eAaPmAgMnP	zajistit
ochranu	ochrana	k1gFnSc4	ochrana
před	před	k7c7	před
útočníkem	útočník	k1gMnSc7	útočník
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
Wilson	Wilson	k1gMnSc1	Wilson
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
právo	právo	k1gNnSc4	právo
národů	národ	k1gInPc2	národ
na	na	k7c4	na
sebeurčení	sebeurčení	k1gNnSc4	sebeurčení
(	(	kIx(	(
<g/>
alespoň	alespoň	k9	alespoň
mimo	mimo	k7c4	mimo
území	území	k1gNnSc4	území
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
nelíbilo	líbit	k5eNaImAgNnS	líbit
Francii	Francie	k1gFnSc3	Francie
a	a	k8xC	a
Velké	velký	k2eAgFnSc3d1	velká
Británii	Británie	k1gFnSc3	Británie
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
to	ten	k3xDgNnSc1	ten
ohrožovalo	ohrožovat	k5eAaImAgNnS	ohrožovat
existenci	existence	k1gFnSc4	existence
jejich	jejich	k3xOp3gNnPc2	jejich
impérií	impérium	k1gNnPc2	impérium
<g/>
.	.	kIx.	.
</s>
<s>
Wilson	Wilson	k1gMnSc1	Wilson
pochopitelně	pochopitelně	k6eAd1	pochopitelně
neměl	mít	k5eNaImAgMnS	mít
zájem	zájem	k1gInSc4	zájem
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zvětšovala	zvětšovat	k5eAaImAgFnS	zvětšovat
velikost	velikost	k1gFnSc1	velikost
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
Británie	Británie	k1gFnSc2	Británie
či	či	k8xC	či
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
požadavek	požadavek	k1gInSc4	požadavek
práva	právo	k1gNnSc2	právo
národů	národ	k1gInPc2	národ
na	na	k7c4	na
sebeurčení	sebeurčení	k1gNnSc4	sebeurčení
byl	být	k5eAaImAgMnS	být
vítanou	vítaný	k2eAgFnSc7d1	vítaná
podporou	podpora	k1gFnSc7	podpora
pro	pro	k7c4	pro
národy	národ	k1gInPc4	národ
toužící	toužící	k2eAgInPc4d1	toužící
po	po	k7c6	po
samostatnosti	samostatnost	k1gFnSc6	samostatnost
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výsledku	výsledek	k1gInSc6	výsledek
to	ten	k3xDgNnSc1	ten
přispělo	přispět	k5eAaPmAgNnS	přispět
k	k	k7c3	k
pádu	pád	k1gInSc3	pád
Rakouska-Uherska	Rakouska-Uhersko	k1gNnSc2	Rakouska-Uhersko
a	a	k8xC	a
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vyhlášením	vyhlášení	k1gNnSc7	vyhlášení
samostatnosti	samostatnost	k1gFnSc2	samostatnost
nových	nový	k2eAgInPc2d1	nový
států	stát	k1gInPc2	stát
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velká	velký	k2eAgFnSc1d1	velká
trojka	trojka	k1gFnSc1	trojka
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
chtěla	chtít	k5eAaImAgFnS	chtít
potrestat	potrestat	k5eAaPmF	potrestat
Německo	Německo	k1gNnSc4	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
chtěla	chtít	k5eAaImAgFnS	chtít
odplatu	odplata	k1gFnSc4	odplata
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
chtěla	chtít	k5eAaImAgFnS	chtít
relativně	relativně	k6eAd1	relativně
ekonomicky	ekonomicky	k6eAd1	ekonomicky
silné	silný	k2eAgNnSc4d1	silné
Německo	Německo	k1gNnSc4	Německo
jako	jako	k8xC	jako
protiváhu	protiváha	k1gFnSc4	protiváha
francouzské	francouzský	k2eAgFnSc3d1	francouzská
dominanci	dominance	k1gFnSc3	dominance
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgFnPc1d1	americká
chtěly	chtít	k5eAaImAgFnP	chtít
rozbít	rozbít	k5eAaPmF	rozbít
stará	starý	k2eAgNnPc4d1	staré
impéria	impérium	k1gNnPc4	impérium
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
byla	být	k5eAaImAgFnS	být
smlouva	smlouva	k1gFnSc1	smlouva
vycházející	vycházející	k2eAgFnSc1d1	vycházející
z	z	k7c2	z
francouzských	francouzský	k2eAgInPc2d1	francouzský
požadavků	požadavek	k1gInPc2	požadavek
<g/>
,	,	kIx,	,
s	s	k7c7	s
minimálním	minimální	k2eAgInSc7d1	minimální
ohledem	ohled	k1gInSc7	ohled
k	k	k7c3	k
ostatním	ostatní	k2eAgMnPc3d1	ostatní
účastníkům	účastník	k1gMnPc3	účastník
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
mnohé	mnohé	k1gNnSc4	mnohé
kritiky	kritika	k1gFnSc2	kritika
<g/>
,	,	kIx,	,
uznávaný	uznávaný	k2eAgMnSc1d1	uznávaný
ekonom	ekonom	k1gMnSc1	ekonom
John	John	k1gMnSc1	John
Maynard	Maynard	k1gMnSc1	Maynard
Keynes	Keynes	k1gMnSc1	Keynes
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
na	na	k7c6	na
konferenci	konference	k1gFnSc6	konference
zastupoval	zastupovat	k5eAaImAgMnS	zastupovat
britské	britský	k2eAgNnSc4d1	Britské
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
financí	finance	k1gFnPc2	finance
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pokud	pokud	k8xS	pokud
přijmeme	přijmout	k5eAaPmIp1nP	přijmout
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
Německo	Německo	k1gNnSc1	Německo
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
ožebračeno	ožebračen	k2eAgNnSc1d1	ožebračen
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
děti	dítě	k1gFnPc1	dítě
musí	muset	k5eAaImIp3nP	muset
hladovět	hladovět	k5eAaImF	hladovět
<g/>
,	,	kIx,	,
odvažuji	odvažovat	k5eAaImIp1nS	odvažovat
se	se	k3xPyFc4	se
tvrdit	tvrdit	k5eAaImF	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
pomsta	pomsta	k1gFnSc1	pomsta
nenechá	nechat	k5eNaPmIp3nS	nechat
čekat	čekat	k5eAaImF	čekat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Následky	následek	k1gInPc4	následek
==	==	k?	==
</s>
</p>
<p>
<s>
Německo	Německo	k1gNnSc1	Německo
nebylo	být	k5eNaImAgNnS	být
Versailleskou	versailleský	k2eAgFnSc7d1	Versailleská
smlouvou	smlouva	k1gFnSc7	smlouva
ani	ani	k8xC	ani
zničeno	zničen	k2eAgNnSc1d1	zničeno
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
usmířeno	usmířen	k2eAgNnSc1d1	usmířeno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
omezeno	omezit	k5eAaPmNgNnS	omezit
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
akčním	akční	k2eAgInSc6d1	akční
rádiu	rádius	k1gInSc6	rádius
<g/>
.	.	kIx.	.
</s>
<s>
Versailleská	versailleský	k2eAgFnSc1d1	Versailleská
smlouva	smlouva	k1gFnSc1	smlouva
způsobila	způsobit	k5eAaPmAgFnS	způsobit
značné	značný	k2eAgNnSc4d1	značné
ochromení	ochromení	k1gNnSc4	ochromení
německé	německý	k2eAgFnSc2d1	německá
ekonomiky	ekonomika	k1gFnSc2	ekonomika
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
dvacátých	dvacátý	k4xOgNnPc2	dvacátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
;	;	kIx,	;
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
míry	míra	k1gFnSc2	míra
tak	tak	k6eAd1	tak
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
podmínky	podmínka	k1gFnPc1	podmínka
pro	pro	k7c4	pro
hyperinflaci	hyperinflace	k1gFnSc4	hyperinflace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
tehdy	tehdy	k6eAd1	tehdy
Německo	Německo	k1gNnSc4	Německo
zachvátila	zachvátit	k5eAaPmAgFnS	zachvátit
<g/>
.	.	kIx.	.
</s>
<s>
Podmínky	podmínka	k1gFnPc1	podmínka
smlouvy	smlouva	k1gFnSc2	smlouva
vedly	vést	k5eAaImAgFnP	vést
i	i	k9	i
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
německá	německý	k2eAgFnSc1d1	německá
ekonomika	ekonomika	k1gFnSc1	ekonomika
zůstala	zůstat	k5eAaPmAgFnS	zůstat
zranitelná	zranitelný	k2eAgFnSc1d1	zranitelná
během	během	k7c2	během
zničující	zničující	k2eAgFnSc2d1	zničující
velké	velký	k2eAgFnSc2d1	velká
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
až	až	k9	až
do	do	k7c2	do
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
historiků	historik	k1gMnPc2	historik
tak	tak	k6eAd1	tak
byly	být	k5eAaImAgInP	být
zapříčiněny	zapříčiněn	k2eAgInPc1d1	zapříčiněn
nástup	nástup	k1gInSc4	nástup
nacismu	nacismus	k1gInSc2	nacismus
a	a	k8xC	a
následné	následný	k2eAgNnSc4d1	následné
převzetí	převzetí	k1gNnSc4	převzetí
moci	moc	k1gFnSc2	moc
Adolfem	Adolf	k1gMnSc7	Adolf
Hitlerem	Hitler	k1gMnSc7	Hitler
<g/>
.	.	kIx.	.
<g/>
Francie	Francie	k1gFnSc1	Francie
prosadila	prosadit	k5eAaPmAgFnS	prosadit
smlouvu	smlouva	k1gFnSc4	smlouva
ponižující	ponižující	k2eAgNnSc1d1	ponižující
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedokázala	dokázat	k5eNaPmAgFnS	dokázat
na	na	k7c4	na
její	její	k3xOp3gNnSc4	její
plnění	plnění	k1gNnSc4	plnění
natrvalo	natrvalo	k6eAd1	natrvalo
důsledně	důsledně	k6eAd1	důsledně
dohlížet	dohlížet	k5eAaImF	dohlížet
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
Francie	Francie	k1gFnSc1	Francie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
když	když	k8xS	když
Hitler	Hitler	k1gMnSc1	Hitler
vojensky	vojensky	k6eAd1	vojensky
obsadil	obsadit	k5eAaPmAgMnS	obsadit
demilitarizované	demilitarizovaný	k2eAgNnSc4d1	demilitarizované
Porýní	Porýní	k1gNnSc4	Porýní
<g/>
,	,	kIx,	,
nezmohla	zmoct	k5eNaPmAgFnS	zmoct
ani	ani	k9	ani
na	na	k7c4	na
diplomatický	diplomatický	k2eAgInSc4d1	diplomatický
odpor	odpor	k1gInSc4	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
její	její	k3xOp3gFnSc1	její
nečinnost	nečinnost	k1gFnSc1	nečinnost
byla	být	k5eAaImAgFnS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
příčin	příčina	k1gFnPc2	příčina
dále	daleko	k6eAd2	daleko
vzrůstající	vzrůstající	k2eAgFnSc2d1	vzrůstající
německé	německý	k2eAgFnSc2d1	německá
agresivity	agresivita	k1gFnSc2	agresivita
a	a	k8xC	a
v	v	k7c6	v
konečném	konečný	k2eAgInSc6d1	konečný
důsledku	důsledek	k1gInSc6	důsledek
i	i	k8xC	i
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
DEJMEK	Dejmek	k1gMnSc1	Dejmek
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Zrod	zrod	k1gInSc1	zrod
nové	nový	k2eAgFnSc2d1	nová
Evropy	Evropa	k1gFnSc2	Evropa
:	:	kIx,	:
Versailles	Versailles	k1gFnSc1	Versailles
<g/>
,	,	kIx,	,
St.	st.	kA	st.
<g/>
-Germain	-Germain	k1gInSc1	-Germain
<g/>
,	,	kIx,	,
Trianon	Trianon	k1gInSc1	Trianon
a	a	k8xC	a
dotváření	dotváření	k1gNnSc1	dotváření
poválečného	poválečný	k2eAgInSc2d1	poválečný
mírového	mírový	k2eAgInSc2d1	mírový
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Historický	historický	k2eAgInSc1d1	historický
ústav	ústav	k1gInSc1	ústav
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
518	[number]	k4	518
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7286	[number]	k4	7286
<g/>
-	-	kIx~	-
<g/>
188	[number]	k4	188
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PLATTHAUS	PLATTHAUS	kA	PLATTHAUS
<g/>
,	,	kIx,	,
Andreas	Andreas	k1gInSc1	Andreas
<g/>
.	.	kIx.	.
</s>
<s>
Der	drát	k5eAaImRp2nS	drát
Krieg	Krieg	k1gInSc1	Krieg
nach	nach	k1gInSc1	nach
dem	dem	k?	dem
Krieg	Krieg	k1gInSc1	Krieg
-	-	kIx~	-
Deutschland	Deutschland	k1gInSc1	Deutschland
zwischen	zwischen	k2eAgInSc1d1	zwischen
Revolution	Revolution	k1gInSc1	Revolution
und	und	k?	und
Versailles	Versailles	k1gFnSc2	Versailles
1918	[number]	k4	1918
<g/>
/	/	kIx~	/
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
</s>
<s>
Berlín	Berlín	k1gInSc1	Berlín
<g/>
:	:	kIx,	:
Rowohlt	Rowohlt	k1gInSc1	Rowohlt
<g/>
,	,	kIx,	,
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
448	[number]	k4	448
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
87134	[number]	k4	87134
<g/>
-	-	kIx~	-
<g/>
786	[number]	k4	786
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
</s>
</p>
<p>
<s>
Společnost	společnost	k1gFnSc1	společnost
národů	národ	k1gInPc2	národ
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Versailleská	versailleský	k2eAgFnSc1d1	Versailleská
smlouva	smlouva	k1gFnSc1	smlouva
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc4	dílo
Versailleská	versailleský	k2eAgFnSc1d1	Versailleská
smlouva	smlouva	k1gFnSc1	smlouva
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
