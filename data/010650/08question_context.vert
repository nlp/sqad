<s>
Smrkovník	smrkovník	k1gInSc1	smrkovník
plazivý	plazivý	k2eAgInSc1d1	plazivý
(	(	kIx(	(
<g/>
Goodyera	Goodyera	k1gFnSc1	Goodyera
repens	repens	k1gInSc1	repens
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nenápadná	nápadný	k2eNgFnSc1d1	nenápadná
orchidej	orchidej	k1gFnSc1	orchidej
s	s	k7c7	s
drobnými	drobný	k2eAgInPc7d1	drobný
květy	květ	k1gInPc7	květ
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
obvykle	obvykle	k6eAd1	obvykle
roste	růst	k5eAaImIp3nS	růst
ve	v	k7c6	v
skupinkách	skupinka	k1gFnPc6	skupinka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgInSc7d1	jediný
druhem	druh	k1gInSc7	druh
rodu	rod	k1gInSc2	rod
smrkovník	smrkovník	k1gInSc4	smrkovník
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
přírodě	příroda	k1gFnSc6	příroda
roste	růst	k5eAaImIp3nS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
orchideje	orchidea	k1gFnPc4	orchidea
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
vyrostlé	vyrostlý	k2eAgFnSc2d1	vyrostlá
listové	listový	k2eAgFnSc2d1	listová
růžice	růžice	k1gFnSc2	růžice
přečkají	přečkat	k5eAaPmIp3nP	přečkat
bez	bez	k7c2	bez
úhony	úhona	k1gFnSc2	úhona
zimu	zima	k1gFnSc4	zima
a	a	k8xC	a
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
v	v	k7c6	v
růstu	růst	k1gInSc6	růst
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
objevoval	objevovat	k5eAaImAgInS	objevovat
roztroušeně	roztroušeně	k6eAd1	roztroušeně
s	s	k7c7	s
hlavním	hlavní	k2eAgInSc7d1	hlavní
výskytem	výskyt	k1gInSc7	výskyt
ve	v	k7c6	v
středních	střední	k2eAgFnPc6d1	střední
polohách	poloha	k1gFnPc6	poloha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
značně	značně	k6eAd1	značně
ustoupil	ustoupit	k5eAaPmAgInS	ustoupit
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
rostlinou	rostlina	k1gFnSc7	rostlina
ohroženou	ohrožený	k2eAgFnSc7d1	ohrožená
vyhynutím	vyhynutí	k1gNnSc7	vyhynutí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Druh	druh	k1gInSc1	druh
je	být	k5eAaImIp3nS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
sice	sice	k8xC	sice
po	po	k7c6	po
velkém	velký	k2eAgNnSc6d1	velké
cirkumboreálním	cirkumboreální	k2eAgNnSc6d1	cirkumboreální
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
však	však	k9	však
roste	růst	k5eAaImIp3nS	růst
jen	jen	k9	jen
roztříštěně	roztříštěně	k6eAd1	roztříštěně
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
blízkosti	blízkost	k1gFnPc4	blízkost
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
až	až	k9	až
po	po	k7c4	po
Ural	Ural	k1gInSc4	Ural
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
východním	východní	k2eAgInSc7d1	východní
směrem	směr	k1gInSc7	směr
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
zemí	zem	k1gFnPc2	zem
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
a	a	k8xC	a
přes	přes	k7c4	přes
Afghánistán	Afghánistán	k1gInSc4	Afghánistán
<g/>
,	,	kIx,	,
Kazachstán	Kazachstán	k1gInSc4	Kazachstán
<g/>
,	,	kIx,	,
Kyrgyzstán	Kyrgyzstán	k1gInSc4	Kyrgyzstán
a	a	k8xC	a
Altajskou	altajský	k2eAgFnSc4d1	Altajská
republiku	republika	k1gFnSc4	republika
až	až	k9	až
na	na	k7c4	na
Dálný	dálný	k2eAgInSc4d1	dálný
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
arelu	arela	k1gFnSc4	arela
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
Kamčatka	Kamčatka	k1gFnSc1	Kamčatka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
nadmořských	nadmořský	k2eAgFnPc6d1	nadmořská
výškách	výška	k1gFnPc6	výška
proniká	pronikat	k5eAaImIp3nS	pronikat
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
jih	jih	k1gInSc4	jih
než	než	k8xS	než
v	v	k7c6	v
nižších	nízký	k2eAgFnPc6d2	nižší
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
polokouli	polokoule	k1gFnSc6	polokoule
je	být	k5eAaImIp3nS	být
rozšířen	rozšířit	k5eAaPmNgMnS	rozšířit
od	od	k7c2	od
Aljašky	Aljaška	k1gFnSc2	Aljaška
a	a	k8xC	a
Yukonu	Yukon	k1gInSc2	Yukon
na	na	k7c6	na
severu	sever	k1gInSc6	sever
přes	přes	k7c4	přes
celou	celá	k1gFnSc4	celá
Kanada	Kanada	k1gFnSc1	Kanada
až	až	k6eAd1	až
po	po	k7c6	po
Arizona	Arizona	k1gFnSc1	Arizona
a	a	k8xC	a
Nové	Nové	k2eAgNnSc1d1	Nové
Mexiko	Mexiko	k1gNnSc1	Mexiko
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekologie	ekologie	k1gFnSc1	ekologie
==	==	k?	==
</s>
</p>
<p>
<s>
Stanoviště	stanoviště	k1gNnSc1	stanoviště
smrkovníku	smrkovník	k1gInSc2	smrkovník
plazivého	plazivý	k2eAgInSc2d1	plazivý
jsou	být	k5eAaImIp3nP	být
nejčastěji	často	k6eAd3	často
polostinné	polostinný	k2eAgInPc1d1	polostinný
<g/>
,	,	kIx,	,
mechové	mechový	k2eAgInPc1d1	mechový
<g/>
,	,	kIx,	,
jehličnaté	jehličnatý	k2eAgInPc1d1	jehličnatý
či	či	k8xC	či
smíšené	smíšený	k2eAgInPc1d1	smíšený
lesy	les	k1gInPc1	les
nebo	nebo	k8xC	nebo
jejích	její	k3xOp3gInPc2	její
okraje	okraj	k1gInSc2	okraj
<g/>
.	.	kIx.	.
</s>
<s>
Bylinám	bylina	k1gFnPc3	bylina
nejlépe	dobře	k6eAd3	dobře
vyhovují	vyhovovat	k5eAaImIp3nP	vyhovovat
mírně	mírně	k6eAd1	mírně
zásadité	zásaditý	k2eAgFnPc1d1	zásaditá
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
suché	suchý	k2eAgInPc1d1	suchý
a	a	k8xC	a
hodně	hodně	k6eAd1	hodně
humózní	humózní	k2eAgFnSc2d1	humózní
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
do	do	k7c2	do
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
až	až	k9	až
3000	[number]	k4	3000
m.	m.	k?	m.
Rostou	růst	k5eAaImIp3nP	růst
v	v	k7c6	v
lesích	les	k1gInPc6	les
starších	starší	k1gMnPc2	starší
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
mezi	mezi	k7c7	mezi
stromy	strom	k1gInPc7	strom
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
nejméně	málo	k6eAd3	málo
70	[number]	k4	70
let	léto	k1gNnPc2	léto
a	a	k8xC	a
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
<g/>
,	,	kIx,	,
drobná	drobný	k2eAgFnSc1d1	drobná
<g/>
,	,	kIx,	,
světle	světle	k6eAd1	světle
zelená	zelený	k2eAgFnSc1d1	zelená
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
,	,	kIx,	,
vysoká	vysoký	k2eAgFnSc1d1	vysoká
od	od	k7c2	od
5	[number]	k4	5
do	do	k7c2	do
25	[number]	k4	25
cm	cm	kA	cm
<g/>
,	,	kIx,	,
vyskytující	vyskytující	k2eAgMnSc1d1	vyskytující
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
ve	v	k7c6	v
skupinkách	skupinka	k1gFnPc6	skupinka
<g/>
.	.	kIx.	.
</s>
<s>
Lodyha	lodyha	k1gFnSc1	lodyha
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
nevětvená	větvený	k2eNgFnSc1d1	nevětvená
<g/>
,	,	kIx,	,
vystoupavá	vystoupavý	k2eAgFnSc1d1	vystoupavá
až	až	k6eAd1	až
přímá	přímý	k2eAgFnSc1d1	přímá
<g/>
,	,	kIx,	,
roste	růst	k5eAaImIp3nS	růst
z	z	k7c2	z
husté	hustý	k2eAgFnSc2d1	hustá
listové	listový	k2eAgFnSc2d1	listová
růžice	růžice	k1gFnSc2	růžice
a	a	k8xC	a
výše	výše	k1gFnSc2	výše
je	být	k5eAaImIp3nS	být
žláznatě	žláznatě	k6eAd1	žláznatě
chlupatá	chlupatý	k2eAgFnSc1d1	chlupatá
a	a	k8xC	a
šupinatá	šupinatý	k2eAgFnSc1d1	šupinatá
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pupenu	pupen	k1gInSc2	pupen
dlouze	dlouho	k6eAd1	dlouho
plazivého	plazivý	k2eAgInSc2d1	plazivý
oddenku	oddenek	k1gInSc2	oddenek
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
listová	listový	k2eAgFnSc1d1	listová
růžice	růžice	k1gFnSc1	růžice
<g/>
,	,	kIx,	,
přezimuje	přezimovat	k5eAaBmIp3nS	přezimovat
a	a	k8xC	a
na	na	k7c4	na
jaře	jař	k1gFnPc4	jař
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
vyraší	vyrašit	k5eAaPmIp3nS	vyrašit
květná	květný	k2eAgFnSc1d1	Květná
lodyha	lodyha	k1gFnSc1	lodyha
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
kvetení	kvetení	k1gNnSc2	kvetení
již	již	k9	již
růžice	růžice	k1gFnSc1	růžice
usychá	usychat	k5eAaImIp3nS	usychat
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
v	v	k7c6	v
růžici	růžice	k1gFnSc6	růžice
s	s	k7c7	s
krátkými	krátký	k2eAgFnPc7d1	krátká
<g/>
,	,	kIx,	,
pochvatými	pochvatý	k2eAgInPc7d1	pochvatý
řapíky	řapík	k1gInPc7	řapík
jsou	být	k5eAaImIp3nP	být
podlouhle	podlouhle	k6eAd1	podlouhle
vejčité	vejčitý	k2eAgInPc1d1	vejčitý
až	až	k9	až
široce	široko	k6eAd1	široko
eliptické	eliptický	k2eAgNnSc1d1	eliptické
<g/>
,	,	kIx,	,
na	na	k7c6	na
konci	konec	k1gInSc6	konec
zašpičatělé	zašpičatělý	k2eAgNnSc1d1	zašpičatělé
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
nápadně	nápadně	k6eAd1	nápadně
síťnatou	síťnatý	k2eAgFnSc4d1	síťnatá
žilnatinu	žilnatina	k1gFnSc4	žilnatina
a	a	k8xC	a
bývají	bývat	k5eAaImIp3nP	bývat
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
2	[number]	k4	2
až	až	k9	až
4	[number]	k4	4
cm	cm	kA	cm
a	a	k8xC	a
široké	široký	k2eAgNnSc1d1	široké
1	[number]	k4	1
až	až	k9	až
2	[number]	k4	2
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Koncové	koncový	k2eAgNnSc1d1	koncové
květenství	květenství	k1gNnSc1	květenství
bývá	bývat	k5eAaImIp3nS	bývat
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
z	z	k7c2	z
10	[number]	k4	10
až	až	k9	až
15	[number]	k4	15
drobných	drobný	k2eAgInPc2d1	drobný
<g/>
,	,	kIx,	,
odstálých	odstálý	k2eAgInPc2d1	odstálý
<g/>
,	,	kIx,	,
bělavých	bělavý	k2eAgInPc2d1	bělavý
<g/>
,	,	kIx,	,
nicích	nicích	k?	nicích
a	a	k8xC	a
slabě	slabě	k6eAd1	slabě
vonných	vonný	k2eAgInPc2d1	vonný
květů	květ	k1gInPc2	květ
s	s	k7c7	s
čárkovitými	čárkovitý	k2eAgInPc7d1	čárkovitý
listeny	listen	k1gInPc7	listen
<g/>
.	.	kIx.	.
</s>
<s>
Vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
hustý	hustý	k2eAgInSc4d1	hustý
<g/>
,	,	kIx,	,
jednostranný	jednostranný	k2eAgInSc4d1	jednostranný
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
slabě	slabě	k6eAd1	slabě
šroubovitě	šroubovitě	k6eAd1	šroubovitě
stočený	stočený	k2eAgInSc1d1	stočený
klas	klas	k1gInSc1	klas
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
3	[number]	k4	3
až	až	k8xS	až
4	[number]	k4	4
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Kališní	kališní	k2eAgInPc1d1	kališní
lístky	lístek	k1gInPc1	lístek
jsou	být	k5eAaImIp3nP	být
zevně	zevně	k6eAd1	zevně
jemně	jemně	k6eAd1	jemně
žláznaté	žláznatý	k2eAgFnPc1d1	žláznatá
<g/>
,	,	kIx,	,
vejčité	vejčitý	k2eAgFnPc1d1	vejčitá
<g/>
,	,	kIx,	,
nazelenalé	nazelenalý	k2eAgFnPc1d1	nazelenalá
nebo	nebo	k8xC	nebo
bílé	bílý	k2eAgFnPc1d1	bílá
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
sobě	se	k3xPyFc3	se
skloněné	skloněný	k2eAgInPc4d1	skloněný
korunní	korunní	k2eAgInPc4d1	korunní
lístky	lístek	k1gInPc4	lístek
jsou	být	k5eAaImIp3nP	být
úzce	úzko	k6eAd1	úzko
kopinaté	kopinatý	k2eAgFnPc1d1	kopinatá
<g/>
,	,	kIx,	,
špičaté	špičatý	k2eAgFnPc1d1	špičatá
<g/>
,	,	kIx,	,
bílé	bílý	k2eAgFnPc1d1	bílá
a	a	k8xC	a
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
okolo	okolo	k7c2	okolo
4	[number]	k4	4
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Dvoudílný	dvoudílný	k2eAgInSc1d1	dvoudílný
pysk	pysk	k1gInSc1	pysk
bez	bez	k7c2	bez
ostruhy	ostruha	k1gFnSc2	ostruha
mívá	mívat	k5eAaImIp3nS	mívat
asi	asi	k9	asi
3	[number]	k4	3
mm	mm	kA	mm
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
zadní	zadní	k2eAgFnSc1d1	zadní
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
bradavičnatá	bradavičnatý	k2eAgFnSc1d1	bradavičnatá
a	a	k8xC	a
polokulovitě	polokulovitě	k6eAd1	polokulovitě
vydutá	vydutý	k2eAgFnSc1d1	vydutá
a	a	k8xC	a
přední	přední	k2eAgFnSc1d1	přední
trojhranná	trojhranný	k2eAgFnSc1d1	trojhranná
je	být	k5eAaImIp3nS	být
plochá	plochý	k2eAgFnSc1d1	plochá
<g/>
.	.	kIx.	.
</s>
<s>
Stopkatý	stopkatý	k2eAgMnSc1d1	stopkatý
<g/>
,	,	kIx,	,
válcovitý	válcovitý	k2eAgInSc1d1	válcovitý
semeník	semeník	k1gInSc1	semeník
je	být	k5eAaImIp3nS	být
vřetenovitě	vřetenovitě	k6eAd1	vřetenovitě
stočený	stočený	k2eAgInSc1d1	stočený
<g/>
.	.	kIx.	.
</s>
<s>
Protandrické	protandrický	k2eAgInPc1d1	protandrický
květy	květ	k1gInPc1	květ
rozkvétají	rozkvétat	k5eAaImIp3nP	rozkvétat
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
července	červenec	k1gInSc2	červenec
do	do	k7c2	do
konce	konec	k1gInSc2	konec
srpna	srpen	k1gInSc2	srpen
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
opylovány	opylovat	k5eAaImNgFnP	opylovat
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
čmeláky	čmelák	k1gMnPc4	čmelák
<g/>
.	.	kIx.	.
</s>
<s>
Opylovačům	Opylovač	k1gMnPc3	Opylovač
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
nektar	nektar	k1gInSc4	nektar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Plod	plod	k1gInSc1	plod
je	být	k5eAaImIp3nS	být
vejčitá	vejčitý	k2eAgFnSc1d1	vejčitá
tobolka	tobolka	k1gFnSc1	tobolka
až	až	k9	až
12	[number]	k4	12
mm	mm	kA	mm
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
malými	malý	k2eAgNnPc7d1	malé
a	a	k8xC	a
lehkými	lehký	k2eAgNnPc7d1	lehké
semeny	semeno	k1gNnPc7	semeno
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
si	se	k3xPyFc3	se
podržují	podržovat	k5eAaImIp3nP	podržovat
klíčivost	klíčivost	k1gFnSc4	klíčivost
jen	jen	k9	jen
po	po	k7c4	po
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
lépe	dobře	k6eAd2	dobře
klíčí	klíčit	k5eAaImIp3nS	klíčit
na	na	k7c6	na
světle	světlo	k1gNnSc6	světlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
==	==	k?	==
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
orchidej	orchidej	k1gFnSc1	orchidej
se	se	k3xPyFc4	se
rozmnožuje	rozmnožovat	k5eAaImIp3nS	rozmnožovat
jak	jak	k6eAd1	jak
oddenky	oddenek	k1gInPc4	oddenek
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k8xC	i
semeny	semeno	k1gNnPc7	semeno
rozptýlenými	rozptýlený	k2eAgNnPc7d1	rozptýlené
větrem	vítr	k1gInSc7	vítr
<g/>
.	.	kIx.	.
</s>
