<s>
Apollo	Apollo	k1gNnSc1	Apollo
11	[number]	k4	11
byl	být	k5eAaImAgInS	být
americký	americký	k2eAgInSc1d1	americký
pilotovaný	pilotovaný	k2eAgInSc1d1	pilotovaný
kosmický	kosmický	k2eAgInSc1d1	kosmický
let	let	k1gInSc1	let
programu	program	k1gInSc2	program
Apollo	Apollo	k1gNnSc4	Apollo
<g/>
,	,	kIx,	,
během	během	k7c2	během
něhož	jenž	k3xRgNnSc2	jenž
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1969	[number]	k4	1969
lidé	člověk	k1gMnPc1	člověk
poprvé	poprvé	k6eAd1	poprvé
stanuli	stanout	k5eAaPmAgMnP	stanout
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Trojici	trojice	k1gFnSc3	trojice
astronautů	astronaut	k1gMnPc2	astronaut
–	–	k?	–
velitele	velitel	k1gMnSc4	velitel
Neila	Neil	k1gMnSc4	Neil
Armstronga	Armstrong	k1gMnSc4	Armstrong
<g/>
,	,	kIx,	,
pilota	pilot	k1gMnSc4	pilot
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
Edwina	Edwina	k1gFnSc1	Edwina
"	"	kIx"	"
<g/>
Buzze	Buzze	k1gFnSc1	Buzze
<g/>
"	"	kIx"	"
Aldrina	Aldrina	k1gFnSc1	Aldrina
a	a	k8xC	a
pilota	pilota	k1gFnSc1	pilota
velitelského	velitelský	k2eAgInSc2d1	velitelský
modulu	modul	k1gInSc2	modul
Michaela	Michael	k1gMnSc2	Michael
Collinse	Collinse	k1gFnSc2	Collinse
–	–	k?	–
v	v	k7c6	v
kosmické	kosmický	k2eAgFnSc6d1	kosmická
lodi	loď	k1gFnSc6	loď
Apollo	Apollo	k1gNnSc1	Apollo
11	[number]	k4	11
vynesla	vynést	k5eAaPmAgFnS	vynést
16	[number]	k4	16
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1969	[number]	k4	1969
raketa	raketa	k1gFnSc1	raketa
Saturn	Saturn	k1gInSc1	Saturn
V	V	kA	V
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
programu	program	k1gInSc6	program
Apollo	Apollo	k1gMnSc1	Apollo
to	ten	k3xDgNnSc4	ten
byl	být	k5eAaImAgMnS	být
pátý	pátý	k4xOgInSc4	pátý
pilotovaný	pilotovaný	k2eAgInSc4d1	pilotovaný
let	let	k1gInSc4	let
a	a	k8xC	a
třetí	třetí	k4xOgInSc4	třetí
let	let	k1gInSc4	let
k	k	k7c3	k
Měsíci	měsíc	k1gInSc3	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
,	,	kIx,	,
přiletěli	přiletět	k5eAaPmAgMnP	přiletět
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1969	[number]	k4	1969
se	se	k3xPyFc4	se
Armstrong	Armstrong	k1gMnSc1	Armstrong
a	a	k8xC	a
Aldrin	aldrin	k1gInSc1	aldrin
v	v	k7c6	v
lunárním	lunární	k2eAgInSc6d1	lunární
modulu	modul	k1gInSc6	modul
spustili	spustit	k5eAaPmAgMnP	spustit
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
Měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
ve	v	k7c6	v
20	[number]	k4	20
<g/>
:	:	kIx,	:
<g/>
17	[number]	k4	17
<g/>
:	:	kIx,	:
<g/>
40	[number]	k4	40
UTC	UTC	kA	UTC
přistáli	přistát	k5eAaPmAgMnP	přistát
v	v	k7c6	v
Moři	moře	k1gNnSc6	moře
klidu	klid	k1gInSc2	klid
(	(	kIx(	(
<g/>
Mare	Mare	k1gFnSc1	Mare
Tranquillitatis	Tranquillitatis	k1gFnSc2	Tranquillitatis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
šest	šest	k4xCc4	šest
hodin	hodina	k1gFnPc2	hodina
později	pozdě	k6eAd2	pozdě
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
Armstrong	Armstrong	k1gMnSc1	Armstrong
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
člověk	člověk	k1gMnSc1	člověk
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
Aldrinem	aldrin	k1gInSc7	aldrin
během	během	k7c2	během
dvou	dva	k4xCgNnPc2	dva
a	a	k8xC	a
půlhodinové	půlhodinový	k2eAgFnSc2d1	půlhodinová
vycházky	vycházka	k1gFnSc2	vycházka
nasbírali	nasbírat	k5eAaPmAgMnP	nasbírat
22	[number]	k4	22
kg	kg	kA	kg
měsíčních	měsíční	k2eAgFnPc2d1	měsíční
hornin	hornina	k1gFnPc2	hornina
a	a	k8xC	a
po	po	k7c6	po
21	[number]	k4	21
hodinách	hodina	k1gFnPc6	hodina
a	a	k8xC	a
31	[number]	k4	31
minutách	minuta	k1gFnPc6	minuta
pobytu	pobyt	k1gInSc2	pobyt
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
odstartovali	odstartovat	k5eAaPmAgMnP	odstartovat
zpět	zpět	k6eAd1	zpět
ke	k	k7c3	k
Collinsovi	Collins	k1gMnSc3	Collins
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zatím	zatím	k6eAd1	zatím
čekal	čekat	k5eAaImAgMnS	čekat
ve	v	k7c6	v
velitelském	velitelský	k2eAgInSc6d1	velitelský
modulu	modul	k1gInSc6	modul
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
velitelským	velitelský	k2eAgInSc7d1	velitelský
modulem	modul	k1gInSc7	modul
se	se	k3xPyFc4	se
všichni	všechen	k3xTgMnPc1	všechen
vydali	vydat	k5eAaPmAgMnP	vydat
na	na	k7c4	na
zpáteční	zpáteční	k2eAgFnSc4d1	zpáteční
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
Přistáli	přistát	k5eAaPmAgMnP	přistát
v	v	k7c6	v
Tichém	tichý	k2eAgInSc6d1	tichý
oceánu	oceán	k1gInSc6	oceán
24	[number]	k4	24
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
<s>
Dosažením	dosažení	k1gNnSc7	dosažení
povrchu	povrch	k1gInSc2	povrch
Měsíce	měsíc	k1gInSc2	měsíc
posádka	posádka	k1gFnSc1	posádka
Apolla	Apollo	k1gNnSc2	Apollo
11	[number]	k4	11
splnila	splnit	k5eAaPmAgFnS	splnit
cíl	cíl	k1gInSc1	cíl
vyhlášený	vyhlášený	k2eAgInSc1d1	vyhlášený
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1961	[number]	k4	1961
prezidentem	prezident	k1gMnSc7	prezident
USA	USA	kA	USA
Johnem	John	k1gMnSc7	John
F.	F.	kA	F.
Kennedym	Kennedymum	k1gNnPc2	Kennedymum
–	–	k?	–
do	do	k7c2	do
konce	konec	k1gInSc2	konec
dekády	dekáda	k1gFnSc2	dekáda
stanout	stanout	k5eAaPmF	stanout
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
bezpečně	bezpečně	k6eAd1	bezpečně
se	se	k3xPyFc4	se
vrátit	vrátit	k5eAaPmF	vrátit
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
letu	let	k1gInSc2	let
Apolla	Apollo	k1gNnSc2	Apollo
11	[number]	k4	11
bylo	být	k5eAaImAgNnS	být
doletět	doletět	k5eAaPmF	doletět
k	k	k7c3	k
Měsíci	měsíc	k1gInSc3	měsíc
<g/>
,	,	kIx,	,
přistát	přistát	k5eAaPmF	přistát
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
povrchu	povrch	k1gInSc6	povrch
a	a	k8xC	a
bezpečně	bezpečně	k6eAd1	bezpečně
se	se	k3xPyFc4	se
vrátit	vrátit	k5eAaPmF	vrátit
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
splněn	splnit	k5eAaPmNgInS	splnit
úkol	úkol	k1gInSc1	úkol
vytyčený	vytyčený	k2eAgInSc1d1	vytyčený
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1961	[number]	k4	1961
prezidentem	prezident	k1gMnSc7	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
Johnem	John	k1gMnSc7	John
F.	F.	kA	F.
Kennedym	Kennedym	k1gInSc4	Kennedym
<g/>
:	:	kIx,	:
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
desetiletí	desetiletí	k1gNnSc4	desetiletí
dopravit	dopravit	k5eAaPmF	dopravit
astronauty	astronaut	k1gMnPc4	astronaut
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
a	a	k8xC	a
bezpečně	bezpečně	k6eAd1	bezpečně
je	být	k5eAaImIp3nS	být
vrátit	vrátit	k5eAaPmF	vrátit
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Posádka	posádka	k1gFnSc1	posádka
dostala	dostat	k5eAaPmAgFnS	dostat
i	i	k9	i
několik	několik	k4yIc4	několik
vedlejších	vedlejší	k2eAgInPc2d1	vedlejší
úkolů	úkol	k1gInPc2	úkol
<g/>
.	.	kIx.	.
</s>
<s>
Přivézt	přivézt	k5eAaPmF	přivézt
vzorky	vzorek	k1gInPc4	vzorek
měsíčního	měsíční	k2eAgInSc2d1	měsíční
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
umístit	umístit	k5eAaPmF	umístit
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Měsíce	měsíc	k1gInSc2	měsíc
televizní	televizní	k2eAgFnSc4d1	televizní
kameru	kamera	k1gFnSc4	kamera
a	a	k8xC	a
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
několik	několik	k4yIc4	několik
vědeckých	vědecký	k2eAgInPc2d1	vědecký
experimentů	experiment	k1gInPc2	experiment
–	–	k?	–
provést	provést	k5eAaPmF	provést
měření	měření	k1gNnSc1	měření
toku	tok	k1gInSc2	tok
slunečního	sluneční	k2eAgInSc2d1	sluneční
větru	vítr	k1gInSc2	vítr
pomocí	pomocí	k7c2	pomocí
hliníkové	hliníkový	k2eAgFnSc2d1	hliníková
fólie	fólie	k1gFnSc2	fólie
zachycující	zachycující	k2eAgFnSc2d1	zachycující
částice	částice	k1gFnSc2	částice
<g/>
;	;	kIx,	;
zanechat	zanechat	k5eAaPmF	zanechat
zde	zde	k6eAd1	zde
laserový	laserový	k2eAgInSc4d1	laserový
koutový	koutový	k2eAgInSc4d1	koutový
odražeč	odražeč	k1gInSc4	odražeč
k	k	k7c3	k
přesnému	přesný	k2eAgNnSc3d1	přesné
měření	měření	k1gNnSc3	měření
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
Země	zem	k1gFnSc2	zem
<g/>
–	–	k?	–
<g/>
Měsíc	měsíc	k1gInSc1	měsíc
a	a	k8xC	a
seismometr	seismometr	k1gInSc1	seismometr
<g/>
.	.	kIx.	.
</s>
<s>
Neil	Neil	k1gMnSc1	Neil
Armstrong	Armstrong	k1gMnSc1	Armstrong
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
-	-	kIx~	-
velitel	velitel	k1gMnSc1	velitel
Michael	Michael	k1gMnSc1	Michael
Collins	Collinsa	k1gFnPc2	Collinsa
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
-	-	kIx~	-
pilot	pilot	k1gMnSc1	pilot
velitelského	velitelský	k2eAgInSc2d1	velitelský
modulu	modul	k1gInSc2	modul
Buzz	Buzz	k1gInSc1	Buzz
Aldrin	aldrin	k1gInSc1	aldrin
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
-	-	kIx~	-
pilot	pilot	k1gMnSc1	pilot
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
V	v	k7c6	v
závorkách	závorka	k1gFnPc6	závorka
je	být	k5eAaImIp3nS	být
uvedený	uvedený	k2eAgInSc1d1	uvedený
dosavadní	dosavadní	k2eAgInSc1d1	dosavadní
počet	počet	k1gInSc1	počet
letů	let	k1gInPc2	let
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
včetně	včetně	k7c2	včetně
této	tento	k3xDgFnSc2	tento
mise	mise	k1gFnSc2	mise
James	James	k1gMnSc1	James
Lovell	Lovell	k1gMnSc1	Lovell
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
-	-	kIx~	-
velitel	velitel	k1gMnSc1	velitel
William	William	k1gInSc4	William
Anders	Anders	k1gInSc1	Anders
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
-	-	kIx~	-
pilot	pilot	k1gMnSc1	pilot
velitelského	velitelský	k2eAgInSc2d1	velitelský
modulu	modul	k1gInSc2	modul
Fred	Fred	k1gMnSc1	Fred
Haise	Hais	k1gMnSc2	Hais
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
-	-	kIx~	-
pilot	pilot	k1gMnSc1	pilot
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
V	v	k7c6	v
závorkách	závorka	k1gFnPc6	závorka
je	být	k5eAaImIp3nS	být
uvedený	uvedený	k2eAgInSc1d1	uvedený
dosavadní	dosavadní	k2eAgInSc1d1	dosavadní
počet	počet	k1gInSc1	počet
letů	let	k1gInPc2	let
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
Posádky	posádka	k1gFnSc2	posádka
pro	pro	k7c4	pro
Apollo	Apollo	k1gNnSc4	Apollo
11	[number]	k4	11
byly	být	k5eAaImAgFnP	být
jmenovány	jmenovat	k5eAaImNgInP	jmenovat
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zavedeného	zavedený	k2eAgInSc2d1	zavedený
zvyku	zvyk	k1gInSc2	zvyk
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posádky	posádka	k1gFnSc2	posádka
postoupila	postoupit	k5eAaPmAgFnS	postoupit
záloha	záloha	k1gFnSc1	záloha
letu	let	k1gInSc6	let
o	o	k7c4	o
tři	tři	k4xCgNnPc4	tři
čísla	číslo	k1gNnSc2	číslo
nižšího	nízký	k2eAgNnSc2d2	nižší
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
Apolla	Apollo	k1gNnSc2	Apollo
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Tvořili	tvořit	k5eAaImAgMnP	tvořit
ji	on	k3xPp3gFnSc4	on
Neil	Neil	k1gMnSc1	Neil
Armstrong	Armstrong	k1gMnSc1	Armstrong
<g/>
,	,	kIx,	,
Fred	Fred	k1gMnSc1	Fred
Haise	Haise	k1gFnSc2	Haise
a	a	k8xC	a
Buzz	Buzz	k1gMnSc1	Buzz
Aldrin	aldrin	k1gInSc1	aldrin
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
záložní	záložní	k2eAgFnSc2d1	záložní
posádky	posádka	k1gFnSc2	posádka
přišli	přijít	k5eAaPmAgMnP	přijít
James	James	k1gMnSc1	James
Lovell	Lovell	k1gMnSc1	Lovell
a	a	k8xC	a
William	William	k1gInSc1	William
Anders	Andersa	k1gFnPc2	Andersa
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
místo	místo	k7c2	místo
pilota	pilot	k1gMnSc2	pilot
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
zatím	zatím	k6eAd1	zatím
neobsazené	obsazený	k2eNgNnSc1d1	neobsazené
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1968	[number]	k4	1968
vypadl	vypadnout	k5eAaPmAgMnS	vypadnout
z	z	k7c2	z
posádky	posádka	k1gFnSc2	posádka
Apolla	Apollo	k1gNnSc2	Apollo
9	[number]	k4	9
Michael	Michaela	k1gFnPc2	Michaela
Collins	Collinsa	k1gFnPc2	Collinsa
kvůli	kvůli	k7c3	kvůli
operaci	operace	k1gFnSc3	operace
kostních	kostní	k2eAgInPc2d1	kostní
výrůstků	výrůstek	k1gInPc2	výrůstek
na	na	k7c6	na
páteři	páteř	k1gFnSc6	páteř
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
získal	získat	k5eAaPmAgInS	získat
zpět	zpět	k6eAd1	zpět
letový	letový	k2eAgInSc4d1	letový
status	status	k1gInSc4	status
a	a	k8xC	a
v	v	k7c6	v
Armstrongově	Armstrongův	k2eAgFnSc6d1	Armstrongova
posádce	posádka	k1gFnSc6	posádka
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Freda	Freda	k1gMnSc1	Freda
Haise	Hais	k1gMnSc2	Hais
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
přesunul	přesunout	k5eAaPmAgMnS	přesunout
do	do	k7c2	do
záložní	záložní	k2eAgFnSc2d1	záložní
posádky	posádka	k1gFnSc2	posádka
<g/>
.	.	kIx.	.
</s>
<s>
NASA	NASA	kA	NASA
informaci	informace	k1gFnSc4	informace
o	o	k7c6	o
přesunu	přesun	k1gInSc6	přesun
oficiálně	oficiálně	k6eAd1	oficiálně
oznámila	oznámit	k5eAaPmAgFnS	oznámit
9	[number]	k4	9
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
<s>
Podpůrnou	podpůrný	k2eAgFnSc4d1	podpůrná
posádku	posádka	k1gFnSc4	posádka
tvořili	tvořit	k5eAaImAgMnP	tvořit
Ken	Ken	k1gMnPc1	Ken
Mattingly	Mattingla	k1gFnSc2	Mattingla
<g/>
,	,	kIx,	,
Ronald	Ronald	k1gMnSc1	Ronald
Evans	Evans	k1gInSc1	Evans
a	a	k8xC	a
William	William	k1gInSc1	William
Pogue	Pogu	k1gInSc2	Pogu
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
se	se	k3xPyFc4	se
Bill	Bill	k1gMnSc1	Bill
Anders	Andersa	k1gFnPc2	Andersa
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
odejde	odejít	k5eAaPmIp3nS	odejít
do	do	k7c2	do
Národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
pro	pro	k7c4	pro
letectví	letectví	k1gNnSc4	letectví
a	a	k8xC	a
vesmír	vesmír	k1gInSc4	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Ken	Ken	k?	Ken
Mattingly	Mattingl	k1gInPc1	Mattingl
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
připravoval	připravovat	k5eAaImAgMnS	připravovat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gMnSc4	on
mohl	moct	k5eAaImAgMnS	moct
nahradit	nahradit	k5eAaPmF	nahradit
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
byl	být	k5eAaImAgInS	být
let	let	k1gInSc4	let
Apolla	Apollo	k1gNnSc2	Apollo
11	[number]	k4	11
odložen	odložit	k5eAaPmNgInS	odložit
<g/>
.	.	kIx.	.
</s>
<s>
Emblém	emblém	k1gInSc1	emblém
mise	mise	k1gFnSc2	mise
Apolla	Apollo	k1gNnSc2	Apollo
11	[number]	k4	11
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
Collins	Collins	k1gInSc4	Collins
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
symbolizovat	symbolizovat	k5eAaImF	symbolizovat
mírumilovný	mírumilovný	k2eAgInSc4d1	mírumilovný
a	a	k8xC	a
americký	americký	k2eAgInSc4d1	americký
charakter	charakter	k1gInSc4	charakter
prvního	první	k4xOgNnSc2	první
přistání	přistání	k1gNnSc2	přistání
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Lovell	Lovell	k1gMnSc1	Lovell
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
použít	použít	k5eAaPmF	použít
orla	orel	k1gMnSc4	orel
bělohlavého	bělohlavý	k2eAgMnSc4d1	bělohlavý
<g/>
,	,	kIx,	,
symbolu	symbol	k1gInSc2	symbol
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
z	z	k7c2	z
amerického	americký	k2eAgInSc2d1	americký
státního	státní	k2eAgInSc2d1	státní
znaku	znak	k1gInSc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Collins	Collins	k6eAd1	Collins
poté	poté	k6eAd1	poté
nakreslil	nakreslit	k5eAaPmAgMnS	nakreslit
obraz	obraz	k1gInSc4	obraz
orla	orel	k1gMnSc2	orel
<g/>
,	,	kIx,	,
vznášejícího	vznášející	k2eAgMnSc2d1	vznášející
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
Měsíce	měsíc	k1gInSc2	měsíc
se	s	k7c7	s
Zemí	zem	k1gFnSc7	zem
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
a	a	k8xC	a
nápisem	nápis	k1gInSc7	nápis
Apollo	Apollo	k1gNnSc4	Apollo
Eleven	Elevna	k1gFnPc2	Elevna
<g/>
.	.	kIx.	.
</s>
<s>
Armstrong	Armstrong	k1gMnSc1	Armstrong
zamítl	zamítnout	k5eAaPmAgMnS	zamítnout
pro	pro	k7c4	pro
cizince	cizinec	k1gMnSc4	cizinec
nesrozumitelné	srozumitelný	k2eNgNnSc1d1	nesrozumitelné
anglické	anglický	k2eAgNnSc1d1	anglické
slovo	slovo	k1gNnSc1	slovo
eleven	elevna	k1gFnPc2	elevna
<g/>
,	,	kIx,	,
po	po	k7c6	po
váhání	váhání	k1gNnSc6	váhání
nad	nad	k7c7	nad
římskými	římský	k2eAgFnPc7d1	římská
číslicemi	číslice	k1gFnPc7	číslice
se	se	k3xPyFc4	se
shodli	shodnout	k5eAaPmAgMnP	shodnout
na	na	k7c4	na
Apollo	Apollo	k1gNnSc4	Apollo
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
Instruktor	instruktor	k1gMnSc1	instruktor
Tom	Tom	k1gMnSc1	Tom
Wilson	Wilson	k1gMnSc1	Wilson
astronautům	astronaut	k1gMnPc3	astronaut
poradil	poradit	k5eAaPmAgMnS	poradit
olivovou	olivový	k2eAgFnSc4d1	olivová
ratolest	ratolest	k1gFnSc4	ratolest
jako	jako	k8xS	jako
symbol	symbol	k1gInSc4	symbol
mírumilovnosti	mírumilovnost	k1gFnSc2	mírumilovnost
výpravy	výprava	k1gFnSc2	výprava
<g/>
,	,	kIx,	,
umístili	umístit	k5eAaPmAgMnP	umístit
ji	on	k3xPp3gFnSc4	on
orlovi	orel	k1gMnSc6	orel
do	do	k7c2	do
zobáku	zobák	k1gInSc2	zobák
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
NASA	NASA	kA	NASA
usoudilo	usoudit	k5eAaPmAgNnS	usoudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
prázdné	prázdný	k2eAgInPc1d1	prázdný
orlovy	orlův	k2eAgInPc1d1	orlův
drápy	dráp	k1gInPc1	dráp
nad	nad	k7c7	nad
Měsícem	měsíc	k1gInSc7	měsíc
vypadají	vypadat	k5eAaPmIp3nP	vypadat
příliš	příliš	k6eAd1	příliš
uchvatitelsky	uchvatitelsky	k6eAd1	uchvatitelsky
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
olivovou	olivový	k2eAgFnSc4d1	olivová
větev	větev	k1gFnSc4	větev
přesunulo	přesunout	k5eAaPmAgNnS	přesunout
do	do	k7c2	do
nich	on	k3xPp3gMnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Posádka	posádka	k1gFnSc1	posádka
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
neuvést	uvést	k5eNaPmF	uvést
svá	svůj	k3xOyFgNnPc4	svůj
jména	jméno	k1gNnPc4	jméno
na	na	k7c6	na
emblému	emblém	k1gInSc6	emblém
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
reprezentoval	reprezentovat	k5eAaImAgMnS	reprezentovat
všechny	všechen	k3xTgMnPc4	všechen
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
přispěli	přispět	k5eAaPmAgMnP	přispět
k	k	k7c3	k
přistání	přistání	k1gNnSc3	přistání
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k9	co
posádka	posádka	k1gFnSc1	posádka
Apolla	Apollo	k1gNnSc2	Apollo
10	[number]	k4	10
pojmenovala	pojmenovat	k5eAaPmAgFnS	pojmenovat
svou	svůj	k3xOyFgFnSc4	svůj
loď	loď	k1gFnSc1	loď
Charlie	Charlie	k1gMnSc1	Charlie
Brown	Brown	k1gMnSc1	Brown
(	(	kIx(	(
<g/>
velitelský	velitelský	k2eAgInSc1d1	velitelský
modul	modul	k1gInSc1	modul
<g/>
)	)	kIx)	)
a	a	k8xC	a
Snoopy	Snoop	k1gInPc1	Snoop
(	(	kIx(	(
<g/>
lunární	lunární	k2eAgInSc1d1	lunární
modul	modul	k1gInSc1	modul
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
postav	postava	k1gFnPc2	postava
z	z	k7c2	z
komiksu	komiks	k1gInSc2	komiks
Peanuts	Peanutsa	k1gFnPc2	Peanutsa
<g/>
,	,	kIx,	,
asistent	asistent	k1gMnSc1	asistent
manažera	manažer	k1gMnSc2	manažer
pro	pro	k7c4	pro
styk	styk	k1gInSc4	styk
s	s	k7c7	s
veřejností	veřejnost	k1gFnSc7	veřejnost
Julian	Julian	k1gMnSc1	Julian
Scheer	Scheer	k1gMnSc1	Scheer
napsal	napsat	k5eAaBmAgMnS	napsat
řediteli	ředitel	k1gMnSc3	ředitel
Střediska	středisko	k1gNnSc2	středisko
pilotovaných	pilotovaný	k2eAgInPc2d1	pilotovaný
letů	let	k1gInPc2	let
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Johnsonovo	Johnsonův	k2eAgNnSc1d1	Johnsonovo
vesmírné	vesmírný	k2eAgNnSc1d1	vesmírné
středisko	středisko	k1gNnSc1	středisko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
požádal	požádat	k5eAaPmAgInS	požádat
posádku	posádka	k1gFnSc4	posádka
Apolla	Apollo	k1gNnSc2	Apollo
11	[number]	k4	11
o	o	k7c4	o
serióznější	seriózní	k2eAgNnSc4d2	serióznější
pojmenování	pojmenování	k1gNnSc4	pojmenování
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Zprvu	zprvu	k6eAd1	zprvu
byla	být	k5eAaImAgNnP	být
používána	používán	k2eAgNnPc1d1	používáno
jména	jméno	k1gNnPc1	jméno
Snowcone	Snowcon	k1gInSc5	Snowcon
a	a	k8xC	a
Haystack	Haystacko	k1gNnPc2	Haystacko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
posádka	posádka	k1gFnSc1	posádka
se	se	k3xPyFc4	se
je	být	k5eAaImIp3nS	být
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
změnit	změnit	k5eAaPmF	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
velitelský	velitelský	k2eAgInSc4d1	velitelský
modul	modul	k1gInSc4	modul
vybrala	vybrat	k5eAaPmAgFnS	vybrat
jméno	jméno	k1gNnSc1	jméno
Columbia	Columbia	k1gFnSc1	Columbia
<g/>
,	,	kIx,	,
po	po	k7c6	po
Kolumbiadě	Kolumbiada	k1gFnSc6	Kolumbiada
<g/>
,	,	kIx,	,
obřím	obří	k2eAgNnSc6d1	obří
děle	dělo	k1gNnSc6	dělo
z	z	k7c2	z
románu	román	k1gInSc2	román
Julese	Julese	k1gFnSc1	Julese
Verna	Verna	k1gFnSc1	Verna
Ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
románu	román	k1gInSc6	román
dělo	dít	k5eAaImAgNnS	dít
<g/>
,	,	kIx,	,
umístěné	umístěný	k2eAgFnPc1d1	umístěná
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
<g/>
,	,	kIx,	,
vystřelí	vystřelit	k5eAaPmIp3nS	vystřelit
náboj	náboj	k1gInSc1	náboj
se	s	k7c7	s
třemi	tři	k4xCgMnPc7	tři
dobrodruhy	dobrodruh	k1gMnPc7	dobrodruh
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
obletí	obletět	k5eAaPmIp3nP	obletět
Měsíc	měsíc	k1gInSc4	měsíc
a	a	k8xC	a
vrátí	vrátit	k5eAaPmIp3nS	vrátit
se	se	k3xPyFc4	se
na	na	k7c4	na
Zem	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Columbia	Columbia	k1gFnSc1	Columbia
je	být	k5eAaImIp3nS	být
také	také	k9	také
tradiční	tradiční	k2eAgFnSc1d1	tradiční
ženská	ženský	k2eAgFnSc1d1	ženská
personifikace	personifikace	k1gFnSc1	personifikace
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Lunární	lunární	k2eAgInSc1d1	lunární
modul	modul	k1gInSc1	modul
dostal	dostat	k5eAaPmAgInS	dostat
jméno	jméno	k1gNnSc4	jméno
Eagle	Eagle	k1gFnSc2	Eagle
<g/>
,	,	kIx,	,
orel	orel	k1gMnSc1	orel
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
orla	orel	k1gMnSc2	orel
v	v	k7c6	v
emblému	emblém	k1gInSc6	emblém
výpravy	výprava	k1gFnSc2	výprava
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
místo	místo	k7c2	místo
přistání	přistání	k1gNnSc2	přistání
pro	pro	k7c4	pro
lunární	lunární	k2eAgInSc4d1	lunární
modul	modul	k1gInSc4	modul
programu	program	k1gInSc2	program
Apollo	Apollo	k1gMnSc1	Apollo
<g/>
,	,	kIx,	,
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
Moře	moře	k1gNnSc2	moře
klidu	klid	k1gInSc2	klid
(	(	kIx(	(
<g/>
Mare	Mare	k1gFnSc1	Mare
Tranquillitatis	Tranquillitatis	k1gFnSc1	Tranquillitatis
<g/>
)	)	kIx)	)
okolo	okolo	k7c2	okolo
20	[number]	k4	20
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
kráteru	kráter	k1gInSc2	kráter
Sabine	Sabin	k1gInSc5	Sabin
D	D	kA	D
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
vybráno	vybrat	k5eAaPmNgNnS	vybrat
zčásti	zčásti	k6eAd1	zčásti
i	i	k9	i
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
bylo	být	k5eAaImAgNnS	být
automatickými	automatický	k2eAgFnPc7d1	automatická
povrchovými	povrchový	k2eAgFnPc7d1	povrchová
sondami	sonda	k1gFnPc7	sonda
Ranger	Rangero	k1gNnPc2	Rangero
8	[number]	k4	8
a	a	k8xC	a
Surveyor	Surveyor	k1gMnSc1	Surveyor
5	[number]	k4	5
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
i	i	k9	i
orbitální	orbitální	k2eAgFnSc1d1	orbitální
mapovacími	mapovací	k2eAgFnPc7d1	mapovací
sondami	sonda	k1gFnPc7	sonda
Lunar	Lunara	k1gFnPc2	Lunara
Orbiter	Orbitra	k1gFnPc2	Orbitra
charakterizováno	charakterizovat	k5eAaBmNgNnS	charakterizovat
jako	jako	k8xS	jako
relativně	relativně	k6eAd1	relativně
ploché	plochý	k2eAgFnPc1d1	plochá
a	a	k8xC	a
hladké	hladký	k2eAgFnPc1d1	hladká
a	a	k8xC	a
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
snadné	snadný	k2eAgNnSc1d1	snadné
pro	pro	k7c4	pro
přistání	přistání	k1gNnSc4	přistání
i	i	k9	i
pro	pro	k7c4	pro
výstup	výstup	k1gInSc4	výstup
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Armstrong	Armstrong	k1gMnSc1	Armstrong
ho	on	k3xPp3gMnSc4	on
pohotově	pohotově	k6eAd1	pohotově
hned	hned	k6eAd1	hned
po	po	k7c6	po
přistání	přistání	k1gNnSc6	přistání
nazval	nazvat	k5eAaPmAgMnS	nazvat
"	"	kIx"	"
<g/>
Tranquillity	Tranquillit	k2eAgFnPc1d1	Tranquillit
Base	basa	k1gFnSc3	basa
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Základna	základna	k1gFnSc1	základna
Tranquility	Tranquilita	k1gFnSc2	Tranquilita
<g/>
/	/	kIx~	/
<g/>
Základna	základna	k1gFnSc1	základna
klidu	klid	k1gInSc2	klid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Raketa	raketa	k1gFnSc1	raketa
Saturn	Saturn	k1gMnSc1	Saturn
V	V	kA	V
s	s	k7c7	s
Apollem	Apollo	k1gNnSc7	Apollo
11	[number]	k4	11
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
ze	z	k7c2	z
startovacího	startovací	k2eAgInSc2d1	startovací
komplexu	komplex	k1gInSc2	komplex
39	[number]	k4	39
Kennedyho	Kennedy	k1gMnSc2	Kennedy
vesmírného	vesmírný	k2eAgNnSc2d1	vesmírné
střediska	středisko	k1gNnSc2	středisko
16	[number]	k4	16
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1969	[number]	k4	1969
v	v	k7c4	v
13	[number]	k4	13
<g/>
:	:	kIx,	:
<g/>
32	[number]	k4	32
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
UTC	UTC	kA	UTC
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
:	:	kIx,	:
<g/>
32	[number]	k4	32
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
místního	místní	k2eAgInSc2d1	místní
času	čas	k1gInSc2	čas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
po	po	k7c6	po
12	[number]	k4	12
minutách	minuta	k1gFnPc6	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jednom	jeden	k4xCgNnSc6	jeden
a	a	k8xC	a
půl	půl	k1xP	půl
obletu	oblet	k1gInSc2	oblet
Země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
v	v	k7c6	v
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
22	[number]	k4	22
<g/>
:	:	kIx,	:
<g/>
13	[number]	k4	13
UTC	UTC	kA	UTC
znovu	znovu	k6eAd1	znovu
na	na	k7c4	na
šest	šest	k4xCc4	šest
minut	minuta	k1gFnPc2	minuta
zapálil	zapálit	k5eAaPmAgInS	zapálit
třetí	třetí	k4xOgInSc1	třetí
stupeň	stupeň	k1gInSc1	stupeň
Saturnu	Saturn	k1gInSc2	Saturn
(	(	kIx(	(
<g/>
S-IVB	S-IVB	k1gFnSc1	S-IVB
<g/>
)	)	kIx)	)
a	a	k8xC	a
uvedl	uvést	k5eAaPmAgMnS	uvést
Apollo	Apollo	k1gMnSc1	Apollo
na	na	k7c4	na
dráhu	dráha	k1gFnSc4	dráha
k	k	k7c3	k
Měsíci	měsíc	k1gInSc3	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
půl	půl	k1xP	půl
hodiny	hodina	k1gFnSc2	hodina
(	(	kIx(	(
<g/>
od	od	k7c2	od
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
49	[number]	k4	49
UTC	UTC	kA	UTC
<g/>
)	)	kIx)	)
astronauti	astronaut	k1gMnPc1	astronaut
přestavěli	přestavět	k5eAaPmAgMnP	přestavět
loď	loď	k1gFnSc4	loď
–	–	k?	–
s	s	k7c7	s
velitelským	velitelský	k2eAgInSc7d1	velitelský
modulem	modul	k1gInSc7	modul
se	se	k3xPyFc4	se
odpojili	odpojit	k5eAaPmAgMnP	odpojit
<g/>
,	,	kIx,	,
otočili	otočit	k5eAaPmAgMnP	otočit
ho	on	k3xPp3gNnSc4	on
a	a	k8xC	a
připojili	připojit	k5eAaPmAgMnP	připojit
k	k	k7c3	k
lunárnímu	lunární	k2eAgInSc3d1	lunární
modulu	modul	k1gInSc3	modul
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
v	v	k7c6	v
17	[number]	k4	17
<g/>
:	:	kIx,	:
<g/>
49	[number]	k4	49
<g/>
,	,	kIx,	,
odhodili	odhodit	k5eAaPmAgMnP	odhodit
nepotřebný	potřebný	k2eNgInSc4d1	nepotřebný
třetí	třetí	k4xOgInSc4	třetí
stupeň	stupeň	k1gInSc4	stupeň
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Let	let	k1gInSc1	let
k	k	k7c3	k
Měsíci	měsíc	k1gInSc3	měsíc
trval	trvat	k5eAaImAgInS	trvat
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Navedení	navedení	k1gNnSc1	navedení
na	na	k7c4	na
dráhu	dráha	k1gFnSc4	dráha
k	k	k7c3	k
Měsíci	měsíc	k1gInSc3	měsíc
bylo	být	k5eAaImAgNnS	být
přesné	přesný	k2eAgNnSc1d1	přesné
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
místo	místo	k7c2	místo
plánovaných	plánovaný	k2eAgFnPc2d1	plánovaná
čtyř	čtyři	k4xCgFnPc2	čtyři
korekcí	korekce	k1gFnPc2	korekce
dráhy	dráha	k1gFnSc2	dráha
astronauti	astronaut	k1gMnPc1	astronaut
provedli	provést	k5eAaPmAgMnP	provést
pouze	pouze	k6eAd1	pouze
jedinou	jediný	k2eAgFnSc4d1	jediná
asi	asi	k9	asi
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
cesty	cesta	k1gFnSc2	cesta
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
v	v	k7c6	v
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
17	[number]	k4	17
UTC	UTC	kA	UTC
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
cestě	cesta	k1gFnSc6	cesta
vyprávěla	vyprávět	k5eAaImAgFnS	vyprávět
posádka	posádka	k1gFnSc1	posádka
divákům	divák	k1gMnPc3	divák
v	v	k7c6	v
několika	několik	k4yIc6	několik
televizních	televizní	k2eAgFnPc6d1	televizní
reportážích	reportáž	k1gFnPc6	reportáž
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
třikrát	třikrát	k6eAd1	třikrát
a	a	k8xC	a
počtvrté	počtvrté	k4xO	počtvrté
na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
17	[number]	k4	17
<g/>
:	:	kIx,	:
<g/>
21	[number]	k4	21
UTC	UTC	kA	UTC
19	[number]	k4	19
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
nad	nad	k7c7	nad
odvrácenou	odvrácený	k2eAgFnSc7d1	odvrácená
stranou	strana	k1gFnSc7	strana
Měsíce	měsíc	k1gInSc2	měsíc
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
motorů	motor	k1gInPc2	motor
Apolla	Apollo	k1gNnSc2	Apollo
přešli	přejít	k5eAaPmAgMnP	přejít
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
17	[number]	k4	17
<g/>
:	:	kIx,	:
<g/>
44	[number]	k4	44
UTC	UTC	kA	UTC
Armstrong	Armstrong	k1gMnSc1	Armstrong
a	a	k8xC	a
Aldrin	aldrin	k1gInSc1	aldrin
v	v	k7c6	v
lunárním	lunární	k2eAgInSc6d1	lunární
modulu	modul	k1gInSc6	modul
odpojili	odpojit	k5eAaPmAgMnP	odpojit
od	od	k7c2	od
velitelského	velitelský	k2eAgInSc2d1	velitelský
modulu	modul	k1gInSc2	modul
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
zůstal	zůstat	k5eAaPmAgInS	zůstat
Collins	Collins	k1gInSc1	Collins
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
Armstrong	Armstrong	k1gMnSc1	Armstrong
a	a	k8xC	a
Aldrin	aldrin	k1gInSc4	aldrin
zažehli	zažehnout	k5eAaPmAgMnP	zažehnout
motor	motor	k1gInSc4	motor
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
(	(	kIx(	(
<g/>
DPS	DPS	kA	DPS
<g/>
)	)	kIx)	)
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
svůj	svůj	k3xOyFgInSc4	svůj
sestup	sestup	k1gInSc4	sestup
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
zážehem	zážeh	k1gInSc7	zážeh
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
na	na	k7c4	na
sestupovou	sestupový	k2eAgFnSc4d1	sestupová
dráhu	dráha	k1gFnSc4	dráha
s	s	k7c7	s
nejnižším	nízký	k2eAgInSc7d3	nejnižší
bodem	bod	k1gInSc7	bod
15	[number]	k4	15
km	km	kA	km
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
ve	v	k7c6	v
20	[number]	k4	20
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
zapálili	zapálit	k5eAaPmAgMnP	zapálit
DPS	DPS	kA	DPS
podruhé	podruhé	k6eAd1	podruhé
a	a	k8xC	a
zahájili	zahájit	k5eAaPmAgMnP	zahájit
vlastní	vlastní	k2eAgInSc4d1	vlastní
sestup	sestup	k1gInSc4	sestup
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
však	však	k9	však
zpozorovali	zpozorovat	k5eAaPmAgMnP	zpozorovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
motory	motor	k1gInPc1	motor
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
činnosti	činnost	k1gFnSc6	činnost
déle	dlouho	k6eAd2	dlouho
<g/>
,	,	kIx,	,
míjejí	míjet	k5eAaImIp3nP	míjet
orientační	orientační	k2eAgInPc1d1	orientační
body	bod	k1gInPc1	bod
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
sekundy	sekund	k1gInPc4	sekund
před	před	k7c7	před
plánovanou	plánovaný	k2eAgFnSc7d1	plánovaná
dobou	doba	k1gFnSc7	doba
a	a	k8xC	a
přistávají	přistávat	k5eAaImIp3nP	přistávat
na	na	k7c4	na
kilometry	kilometr	k1gInPc4	kilometr
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
původně	původně	k6eAd1	původně
plánovaného	plánovaný	k2eAgNnSc2d1	plánované
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Navigační	navigační	k2eAgInSc4d1	navigační
a	a	k8xC	a
řídící	řídící	k2eAgInSc4d1	řídící
počítač	počítač	k1gInSc4	počítač
hlásily	hlásit	k5eAaImAgFnP	hlásit
hned	hned	k6eAd1	hned
několik	několik	k4yIc4	několik
poplachů	poplach	k1gInPc2	poplach
způsobených	způsobený	k2eAgFnPc2d1	způsobená
vychýlením	vychýlení	k1gNnSc7	vychýlení
z	z	k7c2	z
plánované	plánovaný	k2eAgFnSc2d1	plánovaná
trasy	trasa	k1gFnSc2	trasa
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
přinutilo	přinutit	k5eAaPmAgNnS	přinutit
posádku	posádka	k1gFnSc4	posádka
kontrolovat	kontrolovat	k5eAaImF	kontrolovat
řízení	řízení	k1gNnSc4	řízení
modulu	modul	k1gInSc2	modul
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řídícím	řídící	k2eAgNnSc6d1	řídící
středisku	středisko	k1gNnSc6	středisko
NASA	NASA	kA	NASA
mladý	mladý	k1gMnSc1	mladý
operátor	operátor	k1gMnSc1	operátor
Steve	Steve	k1gMnSc1	Steve
Bales	Bales	k1gMnSc1	Bales
oznámil	oznámit	k5eAaPmAgMnS	oznámit
řediteli	ředitel	k1gMnSc3	ředitel
letu	let	k1gInSc2	let
<g/>
,	,	kIx,	,
že	že	k8xS	že
přes	přes	k7c4	přes
poplachy	poplach	k1gMnPc4	poplach
je	být	k5eAaImIp3nS	být
bezpečné	bezpečný	k2eAgNnSc1d1	bezpečné
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
sestupu	sestup	k1gInSc6	sestup
<g/>
.	.	kIx.	.
</s>
<s>
Poplachy	poplach	k1gInPc1	poplach
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
generoval	generovat	k5eAaImAgInS	generovat
program	program	k1gInSc1	program
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
<g/>
,	,	kIx,	,
signalizovaly	signalizovat	k5eAaImAgFnP	signalizovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
počítač	počítač	k1gInSc1	počítač
není	být	k5eNaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
dokončit	dokončit	k5eAaPmF	dokončit
výpočty	výpočet	k1gInPc4	výpočet
ve	v	k7c6	v
vyhrazeném	vyhrazený	k2eAgInSc6d1	vyhrazený
čase	čas	k1gInSc6	čas
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
executive	executiv	k1gInSc5	executiv
overflows	overflows	k1gInSc4	overflows
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
počítač	počítač	k1gInSc1	počítač
strávil	strávit	k5eAaPmAgInS	strávit
neplánovaný	plánovaný	k2eNgInSc4d1	neplánovaný
čas	čas	k1gInSc4	čas
obsluhou	obsluha	k1gFnSc7	obsluha
potkávacího	potkávací	k2eAgInSc2d1	potkávací
radaru	radar	k1gInSc2	radar
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
během	během	k7c2	během
sestupu	sestup	k1gInSc2	sestup
nepoužíval	používat	k5eNaImAgMnS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Steve	Steve	k1gMnSc1	Steve
Bales	Bales	k1gMnSc1	Bales
za	za	k7c4	za
svoje	svůj	k3xOyFgNnSc4	svůj
"	"	kIx"	"
<g/>
pokračujeme	pokračovat	k5eAaImIp1nP	pokračovat
<g/>
"	"	kIx"	"
v	v	k7c6	v
napjaté	napjatý	k2eAgFnSc6d1	napjatá
chvíli	chvíle	k1gFnSc6	chvíle
sestupu	sestup	k1gInSc2	sestup
obdržel	obdržet	k5eAaPmAgInS	obdržet
Medaili	medaile	k1gFnSc4	medaile
svobody	svoboda	k1gFnSc2	svoboda
(	(	kIx(	(
<g/>
Medal	Medal	k1gInSc1	Medal
of	of	k?	of
Freedom	Freedom	k1gInSc1	Freedom
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
jak	jak	k8xC	jak
posádka	posádka	k1gFnSc1	posádka
mohla	moct	k5eAaImAgFnS	moct
obrátit	obrátit	k5eAaPmF	obrátit
svou	svůj	k3xOyFgFnSc4	svůj
pozornost	pozornost	k1gFnSc4	pozornost
zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
výhledu	výhled	k1gInSc3	výhled
ven	ven	k6eAd1	ven
<g/>
,	,	kIx,	,
astronauti	astronaut	k1gMnPc1	astronaut
viděli	vidět	k5eAaImAgMnP	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
počítač	počítač	k1gInSc1	počítač
je	on	k3xPp3gNnSc4	on
vede	vést	k5eAaImIp3nS	vést
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
plné	plný	k2eAgFnSc2d1	plná
velkých	velký	k2eAgInPc2d1	velký
kamenů	kámen	k1gInPc2	kámen
u	u	k7c2	u
velkého	velký	k2eAgInSc2d1	velký
kráteru	kráter	k1gInSc2	kráter
<g/>
.	.	kIx.	.
</s>
<s>
Armstrong	Armstrong	k1gMnSc1	Armstrong
převzal	převzít	k5eAaPmAgMnS	převzít
ruční	ruční	k2eAgNnSc4d1	ruční
řízení	řízení	k1gNnSc4	řízení
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
a	a	k8xC	a
navedl	navést	k5eAaPmAgMnS	navést
jej	on	k3xPp3gMnSc4	on
na	na	k7c4	na
přistání	přistání	k1gNnSc4	přistání
v	v	k7c6	v
20	[number]	k4	20
<g/>
:	:	kIx,	:
<g/>
17	[number]	k4	17
<g/>
:	:	kIx,	:
<g/>
43	[number]	k4	43
UTC	UTC	kA	UTC
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nádržích	nádrž	k1gFnPc6	nádrž
sestupného	sestupný	k2eAgInSc2d1	sestupný
stupně	stupeň	k1gInSc2	stupeň
zbylo	zbýt	k5eAaPmAgNnS	zbýt
palivo	palivo	k1gNnSc1	palivo
na	na	k7c6	na
méně	málo	k6eAd2	málo
než	než	k8xS	než
30	[number]	k4	30
sekund	sekunda	k1gFnPc2	sekunda
letu	let	k1gInSc2	let
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
první	první	k4xOgInSc4	první
slova	slovo	k1gNnPc1	slovo
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
Měsíce	měsíc	k1gInSc2	měsíc
se	se	k3xPyFc4	se
všeobecně	všeobecně	k6eAd1	všeobecně
považují	považovat	k5eAaImIp3nP	považovat
slova	slovo	k1gNnPc1	slovo
Neila	Neil	k1gMnSc2	Neil
Armstronga	Armstrong	k1gMnSc2	Armstrong
"	"	kIx"	"
<g/>
Houstone	Houston	k1gInSc5	Houston
<g/>
,	,	kIx,	,
tady	tady	k6eAd1	tady
je	být	k5eAaImIp3nS	být
základna	základna	k1gFnSc1	základna
Tranquility	Tranquilita	k1gFnSc2	Tranquilita
<g/>
.	.	kIx.	.
</s>
<s>
Orel	Orel	k1gMnSc1	Orel
přistál	přistát	k5eAaImAgMnS	přistát
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
fakticky	fakticky	k6eAd1	fakticky
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgNnP	být
Aldrinova	Aldrinův	k2eAgNnPc1d1	Aldrinův
slova	slovo	k1gNnPc1	slovo
"	"	kIx"	"
<g/>
Signál	signál	k1gInSc1	signál
kontakt	kontakt	k1gInSc1	kontakt
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Contact	Contact	k2eAgInSc1d1	Contact
Light	Light	k1gInSc1	Light
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
se	se	k3xPyFc4	se
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
přistávacích	přistávací	k2eAgFnPc2d1	přistávací
sond	sonda	k1gFnPc2	sonda
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
dotkla	dotknout	k5eAaPmAgFnS	dotknout
povrchu	povrch	k1gInSc3	povrch
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
plánu	plán	k1gInSc2	plán
měli	mít	k5eAaImAgMnP	mít
astronauti	astronaut	k1gMnPc1	astronaut
před	před	k7c7	před
vycházkou	vycházka	k1gFnSc7	vycházka
pět	pět	k4xCc4	pět
hodin	hodina	k1gFnPc2	hodina
spát	spát	k5eAaImF	spát
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
totiž	totiž	k9	totiž
vzhůru	vzhůru	k6eAd1	vzhůru
od	od	k7c2	od
časného	časný	k2eAgNnSc2d1	časné
rána	ráno	k1gNnSc2	ráno
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
však	však	k9	však
Armstrong	Armstrong	k1gMnSc1	Armstrong
s	s	k7c7	s
Aldrinem	aldrin	k1gInSc7	aldrin
usoudili	usoudit	k5eAaPmAgMnP	usoudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
neusnuli	usnout	k5eNaPmAgMnP	usnout
<g/>
;	;	kIx,	;
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
rovnou	rovnou	k6eAd1	rovnou
zahájit	zahájit	k5eAaPmF	zahájit
přípravu	příprava	k1gFnSc4	příprava
k	k	k7c3	k
výstupu	výstup	k1gInSc3	výstup
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
místo	místo	k7c2	místo
plánovaných	plánovaný	k2eAgFnPc2d1	plánovaná
dvou	dva	k4xCgFnPc2	dva
hodin	hodina	k1gFnPc2	hodina
protáhla	protáhnout	k5eAaPmAgFnS	protáhnout
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
a	a	k8xC	a
půl	půl	k1xP	půl
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
dvojici	dvojice	k1gFnSc4	dvojice
trojúhelníkových	trojúhelníkový	k2eAgNnPc2d1	trojúhelníkové
oken	okno	k1gNnPc2	okno
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yQgMnPc2	který
měli	mít	k5eAaImAgMnP	mít
60	[number]	k4	60
<g/>
°	°	k?	°
výhled	výhled	k1gInSc1	výhled
<g/>
,	,	kIx,	,
astronauti	astronaut	k1gMnPc1	astronaut
vybírali	vybírat	k5eAaImAgMnP	vybírat
místo	místo	k1gNnSc4	místo
pro	pro	k7c4	pro
balík	balík	k1gInSc4	balík
s	s	k7c7	s
vědeckým	vědecký	k2eAgNnSc7d1	vědecké
vybavením	vybavení	k1gNnSc7	vybavení
(	(	kIx(	(
<g/>
Early	earl	k1gMnPc4	earl
Apollo	Apollo	k1gMnSc1	Apollo
Scientific	Scientific	k1gMnSc1	Scientific
Experiment	experiment	k1gInSc4	experiment
Package	Packag	k1gFnSc2	Packag
<g/>
,	,	kIx,	,
EASEP	EASEP	kA	EASEP
<g/>
)	)	kIx)	)
a	a	k8xC	a
vlajku	vlajka	k1gFnSc4	vlajka
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
hodin	hodina	k1gFnPc2	hodina
před	před	k7c7	před
samotným	samotný	k2eAgInSc7d1	samotný
výstupem	výstup	k1gInSc7	výstup
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
Měsíce	měsíc	k1gInSc2	měsíc
ještě	ještě	k9	ještě
vyndal	vyndat	k5eAaPmAgInS	vyndat
Aldrin	aldrin	k1gInSc1	aldrin
malou	malý	k2eAgFnSc4d1	malá
plastovou	plastový	k2eAgFnSc4d1	plastová
nádobu	nádoba	k1gFnSc4	nádoba
s	s	k7c7	s
mešním	mešní	k2eAgNnSc7d1	mešní
vínem	víno	k1gNnSc7	víno
a	a	k8xC	a
kousek	kousek	k1gInSc1	kousek
hostie	hostie	k1gFnSc2	hostie
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
přijal	přijmout	k5eAaPmAgMnS	přijmout
eucharistii	eucharistie	k1gFnSc4	eucharistie
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výstupu	výstup	k1gInSc6	výstup
z	z	k7c2	z
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
měl	mít	k5eAaImAgMnS	mít
Armstrong	Armstrong	k1gMnSc1	Armstrong
zpočátku	zpočátku	k6eAd1	zpočátku
problém	problém	k1gInSc4	problém
projít	projít	k5eAaPmF	projít
průlezem	průlez	k1gInSc7	průlez
se	s	k7c7	s
svým	svůj	k1gMnSc7	svůj
PLSS	PLSS	kA	PLSS
(	(	kIx(	(
<g/>
Portable	portable	k1gInSc1	portable
Life-Support	Life-Support	k1gInSc1	Life-Support
System	Systo	k1gNnSc7	Systo
<g/>
,	,	kIx,	,
příruční	příruční	k2eAgInSc4d1	příruční
systém	systém	k1gInSc4	systém
pro	pro	k7c4	pro
zabezpečení	zabezpečení	k1gNnSc4	zabezpečení
životních	životní	k2eAgFnPc2d1	životní
podmínek	podmínka	k1gFnPc2	podmínka
–	–	k?	–
"	"	kIx"	"
<g/>
batoh	batoh	k1gInSc1	batoh
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
měli	mít	k5eAaImAgMnP	mít
astronauti	astronaut	k1gMnPc1	astronaut
na	na	k7c6	na
zádech	záda	k1gNnPc6	záda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
veterána	veterán	k1gMnSc2	veterán
letů	let	k1gInPc2	let
Apollo	Apollo	k1gMnSc1	Apollo
Johna	John	k1gMnSc2	John
Younga	Young	k1gMnSc2	Young
při	při	k7c6	při
návrhu	návrh	k1gInSc6	návrh
batohu	batoh	k1gInSc2	batoh
s	s	k7c7	s
PLSS	PLSS	kA	PLSS
nebylo	být	k5eNaImAgNnS	být
zohledněno	zohledněn	k2eAgNnSc1d1	zohledněno
pozdější	pozdní	k2eAgNnSc1d2	pozdější
zmenšení	zmenšení	k1gNnSc1	zmenšení
průlezu	průlez	k1gInSc2	průlez
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
<g/>
;	;	kIx,	;
takže	takže	k8xS	takže
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
pulz	pulz	k1gInSc1	pulz
astronautů	astronaut	k1gMnPc2	astronaut
byl	být	k5eAaImAgInS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
vstupů	vstup	k1gInPc2	vstup
a	a	k8xC	a
výstupů	výstup	k1gInPc2	výstup
z	z	k7c2	z
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
39	[number]	k4	39
UTC	UTC	kA	UTC
21	[number]	k4	21
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
Armstrong	Armstrong	k1gMnSc1	Armstrong
otevřel	otevřít	k5eAaPmAgMnS	otevřít
průlez	průlez	k1gInSc4	průlez
<g/>
,	,	kIx,	,
vylezl	vylézt	k5eAaPmAgMnS	vylézt
ven	ven	k6eAd1	ven
a	a	k8xC	a
ve	v	k7c6	v
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
51	[number]	k4	51
UTC	UTC	kA	UTC
začal	začít	k5eAaPmAgInS	začít
sestupovat	sestupovat	k5eAaImF	sestupovat
po	po	k7c6	po
žebříku	žebřík	k1gInSc6	žebřík
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
kontrolnímu	kontrolní	k2eAgMnSc3d1	kontrolní
a	a	k8xC	a
ovládacímu	ovládací	k2eAgInSc3d1	ovládací
panelu	panel	k1gInSc3	panel
skafandru	skafandr	k1gInSc2	skafandr
<g/>
,	,	kIx,	,
připnutému	připnutý	k2eAgInSc3d1	připnutý
na	na	k7c6	na
hrudi	hruď	k1gFnSc6	hruď
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
neviděl	vidět	k5eNaImAgMnS	vidět
na	na	k7c4	na
nohy	noha	k1gFnPc4	noha
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
sestupu	sestup	k1gInSc2	sestup
po	po	k7c6	po
devítischodovém	devítischodový	k2eAgInSc6d1	devítischodový
žebříku	žebřík	k1gInSc6	žebřík
Armstrong	Armstrong	k1gMnSc1	Armstrong
zatáhl	zatáhnout	k5eAaPmAgMnS	zatáhnout
za	za	k7c4	za
ovladač	ovladač	k1gInSc4	ovladač
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vyklopil	vyklopit	k5eAaPmAgInS	vyklopit
oddíl	oddíl	k1gInSc1	oddíl
s	s	k7c7	s
vybavením	vybavení	k1gNnSc7	vybavení
(	(	kIx(	(
<g/>
Modular	Modular	k1gMnSc1	Modular
Equipment	Equipment	k1gMnSc1	Equipment
Stowage	Stowage	k1gFnPc2	Stowage
Assembly	Assembly	k1gMnSc1	Assembly
<g/>
,	,	kIx,	,
MESA	MESA	kA	MESA
<g/>
)	)	kIx)	)
složený	složený	k2eAgInSc1d1	složený
na	na	k7c6	na
boku	bok	k1gInSc6	bok
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
a	a	k8xC	a
aktivoval	aktivovat	k5eAaBmAgInS	aktivovat
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
umístěnou	umístěný	k2eAgFnSc4d1	umístěná
televizní	televizní	k2eAgFnSc4d1	televizní
kameru	kamera	k1gFnSc4	kamera
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
byly	být	k5eAaImAgInP	být
nasnímány	nasnímat	k5eAaPmNgInP	nasnímat
systémem	systém	k1gInSc7	systém
pomaloběžné	pomaloběžný	k2eAgFnSc2d1	pomaloběžný
televize	televize	k1gFnSc2	televize
(	(	kIx(	(
<g/>
Slow-scan	Slowcan	k1gInSc1	Slow-scan
television	television	k1gInSc1	television
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
pozemních	pozemní	k2eAgNnPc6d1	pozemní
střediscích	středisko	k1gNnPc6	středisko
je	být	k5eAaImIp3nS	být
přes	přes	k7c4	přes
obrazovku	obrazovka	k1gFnSc4	obrazovka
snímaly	snímat	k5eAaImAgFnP	snímat
kamery	kamera	k1gFnPc1	kamera
běžného	běžný	k2eAgInSc2d1	běžný
televizního	televizní	k2eAgInSc2d1	televizní
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
způsobovalo	způsobovat	k5eAaImAgNnS	způsobovat
značnou	značný	k2eAgFnSc4d1	značná
ztrátu	ztráta	k1gFnSc4	ztráta
kvality	kvalita	k1gFnSc2	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Signály	signál	k1gInPc1	signál
byly	být	k5eAaImAgInP	být
zachyceny	zachytit	k5eAaPmNgInP	zachytit
observatoří	observatoř	k1gFnSc7	observatoř
Goldstone	Goldston	k1gInSc5	Goldston
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
v	v	k7c6	v
australském	australský	k2eAgInSc6d1	australský
Honeysuckle	Honeysuckl	k1gInSc5	Honeysuckl
Creek	Creek	k1gMnSc1	Creek
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
několik	několik	k4yIc4	několik
minut	minuta	k1gFnPc2	minuta
později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
televizní	televizní	k2eAgNnSc4d1	televizní
vysílání	vysílání	k1gNnSc4	vysílání
přepnuto	přepnut	k2eAgNnSc4d1	přepnuto
na	na	k7c4	na
signál	signál	k1gInSc4	signál
z	z	k7c2	z
citlivějšího	citlivý	k2eAgInSc2d2	citlivější
radioteleskopu	radioteleskop	k1gInSc2	radioteleskop
v	v	k7c6	v
australské	australský	k2eAgFnSc6d1	australská
Observatoři	observatoř	k1gFnSc6	observatoř
Parkes	Parkesa	k1gFnPc2	Parkesa
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
některé	některý	k3yIgFnPc4	některý
technické	technický	k2eAgFnPc4d1	technická
a	a	k8xC	a
povětrnostní	povětrnostní	k2eAgFnPc4d1	povětrnostní
těžkosti	těžkost	k1gFnPc4	těžkost
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
rozostřený	rozostřený	k2eAgInSc4d1	rozostřený
černo-bílý	černoílý	k2eAgInSc4d1	černo-bílý
obraz	obraz	k1gInSc4	obraz
první	první	k4xOgFnSc2	první
procházky	procházka	k1gFnSc2	procházka
po	po	k7c6	po
povrchu	povrch	k1gInSc6	povrch
Měsíce	měsíc	k1gInSc2	měsíc
zachycen	zachytit	k5eAaPmNgInS	zachytit
a	a	k8xC	a
ihned	ihned	k6eAd1	ihned
vysílán	vysílán	k2eAgMnSc1d1	vysílán
pro	pro	k7c4	pro
nejméně	málo	k6eAd3	málo
600	[number]	k4	600
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
popisu	popis	k1gInSc6	popis
povrchu	povrch	k1gInSc2	povrch
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
vypadá	vypadat	k5eAaPmIp3nS	vypadat
jako	jako	k9	jako
velmi	velmi	k6eAd1	velmi
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
jemně	jemně	k6eAd1	jemně
zrnitý	zrnitý	k2eAgInSc1d1	zrnitý
<g/>
...	...	k?	...
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
skoro	skoro	k6eAd1	skoro
prach	prach	k1gInSc1	prach
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Neil	Neil	k1gMnSc1	Neil
Armstrong	Armstrong	k1gMnSc1	Armstrong
sestoupil	sestoupit	k5eAaPmAgMnS	sestoupit
ze	z	k7c2	z
žebříku	žebřík	k1gInSc2	žebřík
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
a	a	k8xC	a
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
člověk	člověk	k1gMnSc1	člověk
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
vkročil	vkročit	k5eAaPmAgMnS	vkročit
do	do	k7c2	do
jiného	jiný	k2eAgInSc2d1	jiný
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g />
.	.	kIx.	.
</s>
<s>
ve	v	k7c6	v
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
56	[number]	k4	56
<g/>
:	:	kIx,	:
<g/>
15	[number]	k4	15
UTC	UTC	kA	UTC
<g/>
,	,	kIx,	,
šest	šest	k4xCc4	šest
a	a	k8xC	a
půl	půl	k1xP	půl
hodiny	hodina	k1gFnSc2	hodina
po	po	k7c6	po
přistání	přistání	k1gNnSc6	přistání
<g/>
,	,	kIx,	,
vkročil	vkročit	k5eAaPmAgMnS	vkročit
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
Měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
pronesl	pronést	k5eAaPmAgMnS	pronést
památnou	památný	k2eAgFnSc4d1	památná
větu	věta	k1gFnSc4	věta
<g/>
:	:	kIx,	:
Kromě	kromě	k7c2	kromě
splnění	splnění	k1gNnSc2	splnění
primárního	primární	k2eAgInSc2d1	primární
cíle	cíl	k1gInSc2	cíl
vysloveného	vyslovený	k2eAgNnSc2d1	vyslovené
prezidentem	prezident	k1gMnSc7	prezident
Kennedym	Kennedym	k1gInSc4	Kennedym
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
přistát	přistát	k5eAaImF	přistát
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
s	s	k7c7	s
lidskou	lidský	k2eAgFnSc7d1	lidská
posádkou	posádka	k1gFnSc7	posádka
do	do	k7c2	do
konce	konec	k1gInSc2	konec
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
Apollo	Apollo	k1gNnSc1	Apollo
11	[number]	k4	11
zároveň	zároveň	k6eAd1	zároveň
testem	test	k1gInSc7	test
všech	všecek	k3xTgInPc2	všecek
systémů	systém	k1gInPc2	systém
Apollo	Apollo	k1gMnSc1	Apollo
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
Armstrong	Armstrong	k1gMnSc1	Armstrong
vyfotografoval	vyfotografovat	k5eAaPmAgMnS	vyfotografovat
lunární	lunární	k2eAgInSc4d1	lunární
modul	modul	k1gInSc4	modul
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
konstruktéři	konstruktér	k1gMnPc1	konstruktér
byli	být	k5eAaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
posoudit	posoudit	k5eAaPmF	posoudit
stav	stav	k1gInSc1	stav
modulu	modul	k1gInSc2	modul
po	po	k7c6	po
přistání	přistání	k1gNnSc6	přistání
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
nabral	nabrat	k5eAaPmAgInS	nabrat
první	první	k4xOgInSc1	první
vzorek	vzorek	k1gInSc1	vzorek
měsíčního	měsíční	k2eAgInSc2d1	měsíční
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
uložil	uložit	k5eAaPmAgInS	uložit
do	do	k7c2	do
kapsy	kapsa	k1gFnSc2	kapsa
<g/>
.	.	kIx.	.
</s>
<s>
Odmontoval	odmontovat	k5eAaPmAgInS	odmontovat
televizní	televizní	k2eAgFnSc4d1	televizní
kameru	kamera	k1gFnSc4	kamera
z	z	k7c2	z
MESA	MESA	kA	MESA
<g/>
,	,	kIx,	,
udělal	udělat	k5eAaPmAgMnS	udělat
panoramatický	panoramatický	k2eAgInSc4d1	panoramatický
záběr	záběr	k1gInSc4	záběr
a	a	k8xC	a
připevnil	připevnit	k5eAaPmAgMnS	připevnit
ji	on	k3xPp3gFnSc4	on
na	na	k7c4	na
dvanáctimetrový	dvanáctimetrový	k2eAgInSc4d1	dvanáctimetrový
stojan	stojan	k1gInSc4	stojan
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
<g/>
.	.	kIx.	.
</s>
<s>
Kabel	kabel	k1gInSc1	kabel
od	od	k7c2	od
kamery	kamera	k1gFnSc2	kamera
zůstal	zůstat	k5eAaPmAgMnS	zůstat
trochu	trochu	k6eAd1	trochu
ušpiněný	ušpiněný	k2eAgMnSc1d1	ušpiněný
a	a	k8xC	a
představoval	představovat	k5eAaImAgInS	představovat
potenciální	potenciální	k2eAgNnSc4d1	potenciální
riziko	riziko	k1gNnSc4	riziko
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
celého	celý	k2eAgInSc2d1	celý
výstupu	výstup	k1gInSc2	výstup
<g/>
.	.	kIx.	.
</s>
<s>
Aldrin	aldrin	k1gInSc1	aldrin
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
brzy	brzy	k6eAd1	brzy
přidal	přidat	k5eAaPmAgMnS	přidat
a	a	k8xC	a
oba	dva	k4xCgMnPc1	dva
strávili	strávit	k5eAaPmAgMnP	strávit
dvě	dva	k4xCgFnPc4	dva
a	a	k8xC	a
půl	půl	k1xP	půl
hodiny	hodina	k1gFnSc2	hodina
navrtáváním	navrtávání	k1gNnSc7	navrtávání
měsíčního	měsíční	k2eAgInSc2d1	měsíční
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
fotografováním	fotografování	k1gNnSc7	fotografování
všeho	všecek	k3xTgNnSc2	všecek
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
viděli	vidět	k5eAaImAgMnP	vidět
<g/>
,	,	kIx,	,
a	a	k8xC	a
sbíráním	sbírání	k1gNnSc7	sbírání
kamenů	kámen	k1gInPc2	kámen
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
vzorků	vzorek	k1gInPc2	vzorek
<g/>
.	.	kIx.	.
</s>
<s>
Armstrong	Armstrong	k1gMnSc1	Armstrong
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Aldrinem	aldrin	k1gInSc7	aldrin
zkoušeli	zkoušet	k5eAaImAgMnP	zkoušet
různé	různý	k2eAgFnPc4d1	různá
možnosti	možnost	k1gFnPc4	možnost
pohybu	pohyb	k1gInSc2	pohyb
po	po	k7c6	po
měsíčním	měsíční	k2eAgInSc6d1	měsíční
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
skákání	skákání	k1gNnSc2	skákání
snožmo	snožmo	k6eAd1	snožmo
<g/>
.	.	kIx.	.
</s>
<s>
Batoh	batoh	k1gInSc1	batoh
s	s	k7c7	s
PLSS	PLSS	kA	PLSS
měl	mít	k5eAaImAgMnS	mít
tendenci	tendence	k1gFnSc4	tendence
je	on	k3xPp3gFnPc4	on
převažovat	převažovat	k5eAaImF	převažovat
dozadu	dozadu	k6eAd1	dozadu
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
astronautů	astronaut	k1gMnPc2	astronaut
neměl	mít	k5eNaImAgMnS	mít
s	s	k7c7	s
udržením	udržení	k1gNnSc7	udržení
stability	stabilita	k1gFnSc2	stabilita
vážné	vážný	k2eAgInPc4d1	vážný
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Pohyb	pohyb	k1gInSc1	pohyb
při	při	k7c6	při
měsíční	měsíční	k2eAgFnSc6d1	měsíční
gravitaci	gravitace	k1gFnSc6	gravitace
<g/>
,	,	kIx,	,
šestinové	šestinový	k2eAgNnSc4d1	šestinový
oproti	oproti	k7c3	oproti
gravitaci	gravitace	k1gFnSc3	gravitace
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
Armstrong	Armstrong	k1gMnSc1	Armstrong
popsal	popsat	k5eAaPmAgMnS	popsat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
možná	možná	k9	možná
ještě	ještě	k6eAd1	ještě
lehčí	lehký	k2eAgMnSc1d2	lehčí
než	než	k8xS	než
během	během	k7c2	během
simulací	simulace	k1gFnPc2	simulace
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Preferovaným	preferovaný	k2eAgInSc7d1	preferovaný
způsobem	způsob	k1gInSc7	způsob
pohybu	pohyb	k1gInSc2	pohyb
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
"	"	kIx"	"
<g/>
cval	cval	k1gInSc1	cval
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Astronauti	astronaut	k1gMnPc1	astronaut
hlásili	hlásit	k5eAaImAgMnP	hlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
naplánovat	naplánovat	k5eAaBmF	naplánovat
pohyby	pohyb	k1gInPc1	pohyb
na	na	k7c4	na
šest	šest	k4xCc4	šest
až	až	k9	až
sedm	sedm	k4xCc4	sedm
kroků	krok	k1gInPc2	krok
dopředu	dopředu	k6eAd1	dopředu
<g/>
.	.	kIx.	.
</s>
<s>
Jemná	jemný	k2eAgFnSc1d1	jemná
půda	půda	k1gFnSc1	půda
byla	být	k5eAaImAgFnS	být
celkem	celkem	k6eAd1	celkem
kluzká	kluzký	k2eAgFnSc1d1	kluzká
<g/>
.	.	kIx.	.
</s>
<s>
Aldrin	aldrin	k1gInSc1	aldrin
poznamenal	poznamenat	k5eAaPmAgInS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
při	při	k7c6	při
přesunu	přesun	k1gInSc6	přesun
ze	z	k7c2	z
Sluncem	slunce	k1gNnSc7	slunce
osvětleného	osvětlený	k2eAgNnSc2d1	osvětlené
místa	místo	k1gNnSc2	místo
do	do	k7c2	do
stínu	stín	k1gInSc2	stín
Eaglu	Eagl	k1gInSc2	Eagl
se	se	k3xPyFc4	se
teplota	teplota	k1gFnSc1	teplota
uvnitř	uvnitř	k7c2	uvnitř
skafandru	skafandr	k1gInSc2	skafandr
nezměnila	změnit	k5eNaPmAgFnS	změnit
a	a	k8xC	a
když	když	k8xS	když
na	na	k7c6	na
Slunci	slunce	k1gNnSc6	slunce
byla	být	k5eAaImAgFnS	být
helma	helma	k1gFnSc1	helma
více	hodně	k6eAd2	hodně
ohřívána	ohříván	k2eAgFnSc1d1	ohřívána
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
se	se	k3xPyFc4	se
cítil	cítit	k5eAaImAgMnS	cítit
chladněji	chladně	k6eAd2	chladně
<g/>
.	.	kIx.	.
</s>
<s>
Astronauti	astronaut	k1gMnPc1	astronaut
spolu	spolu	k6eAd1	spolu
vztyčili	vztyčit	k5eAaPmAgMnP	vztyčit
vlajku	vlajka	k1gFnSc4	vlajka
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
–	–	k?	–
povrch	povrch	k1gInSc1	povrch
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
nepodařilo	podařit	k5eNaPmAgNnS	podařit
tyč	tyč	k1gFnSc4	tyč
zasunout	zasunout	k5eAaPmF	zasunout
hlouběji	hluboko	k6eAd2	hluboko
než	než	k8xS	než
20	[number]	k4	20
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
přijali	přijmout	k5eAaPmAgMnP	přijmout
telefonát	telefonát	k1gInSc4	telefonát
od	od	k7c2	od
prezidenta	prezident	k1gMnSc2	prezident
Richarda	Richard	k1gMnSc2	Richard
Nixona	Nixon	k1gMnSc2	Nixon
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
telefonátu	telefonát	k1gInSc6	telefonát
nainstalovali	nainstalovat	k5eAaPmAgMnP	nainstalovat
soupravu	souprava	k1gFnSc4	souprava
vědeckých	vědecký	k2eAgInPc2d1	vědecký
přístrojů	přístroj	k1gInPc2	přístroj
EASEP	EASEP	kA	EASEP
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
pasivní	pasivní	k2eAgInSc4d1	pasivní
seismograf	seismograf	k1gInSc4	seismograf
a	a	k8xC	a
laserový	laserový	k2eAgInSc4d1	laserový
koutový	koutový	k2eAgInSc4d1	koutový
odrážeč	odrážeč	k1gInSc4	odrážeč
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
Armstrong	Armstrong	k1gMnSc1	Armstrong
"	"	kIx"	"
<g/>
odcválal	odcválat	k5eAaPmAgInS	odcválat
<g/>
"	"	kIx"	"
asi	asi	k9	asi
na	na	k7c4	na
120	[number]	k4	120
m	m	kA	m
od	od	k7c2	od
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
udělal	udělat	k5eAaPmAgMnS	udělat
fotografie	fotografia	k1gFnPc4	fotografia
z	z	k7c2	z
okraje	okraj	k1gInSc2	okraj
Východního	východní	k2eAgInSc2d1	východní
kráteru	kráter	k1gInSc2	kráter
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
Aldrin	aldrin	k1gInSc4	aldrin
sbíral	sbírat	k5eAaImAgMnS	sbírat
vzorky	vzorek	k1gInPc7	vzorek
<g/>
.	.	kIx.	.
</s>
<s>
Použil	použít	k5eAaPmAgMnS	použít
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
geologické	geologický	k2eAgNnSc4d1	geologické
kladivo	kladivo	k1gNnSc4	kladivo
–	–	k?	–
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
jediné	jediný	k2eAgNnSc1d1	jediné
použití	použití	k1gNnSc1	použití
tohoto	tento	k3xDgInSc2	tento
nástroje	nástroj	k1gInSc2	nástroj
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
celé	celý	k2eAgFnSc2d1	celá
mise	mise	k1gFnSc2	mise
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
potom	potom	k6eAd1	potom
do	do	k7c2	do
nádob	nádoba	k1gFnPc2	nádoba
posbírali	posbírat	k5eAaPmAgMnP	posbírat
vzorky	vzorek	k1gInPc4	vzorek
kamenů	kámen	k1gInPc2	kámen
pomocí	pomocí	k7c2	pomocí
prodloužených	prodloužený	k2eAgFnPc2d1	prodloužená
kleští	kleště	k1gFnPc2	kleště
<g/>
.	.	kIx.	.
</s>
<s>
Pracovali	pracovat	k5eAaImAgMnP	pracovat
pomaleji	pomale	k6eAd2	pomale
<g/>
,	,	kIx,	,
než	než	k8xS	než
očekávali	očekávat	k5eAaImAgMnP	očekávat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
museli	muset	k5eAaImAgMnP	muset
zastavit	zastavit	k5eAaPmF	zastavit
dokumentaci	dokumentace	k1gFnSc4	dokumentace
vzorků	vzorek	k1gInPc2	vzorek
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
vyhrazených	vyhrazený	k2eAgFnPc2d1	vyhrazená
34	[number]	k4	34
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
měli	mít	k5eAaImAgMnP	mít
málo	málo	k1gNnSc4	málo
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
Armstrong	Armstrong	k1gMnSc1	Armstrong
doslova	doslova	k6eAd1	doslova
skákal	skákat	k5eAaImAgMnS	skákat
od	od	k7c2	od
jedné	jeden	k4xCgFnSc2	jeden
úlohy	úloha	k1gFnSc2	úloha
ke	k	k7c3	k
druhé	druhý	k4xOgFnSc3	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Řídící	řídící	k2eAgNnSc1d1	řídící
středisko	středisko	k1gNnSc1	středisko
mu	on	k3xPp3gMnSc3	on
doporučilo	doporučit	k5eAaPmAgNnS	doporučit
zpomalit	zpomalit	k5eAaPmF	zpomalit
a	a	k8xC	a
poté	poté	k6eAd1	poté
jim	on	k3xPp3gMnPc3	on
povolilo	povolit	k5eAaPmAgNnS	povolit
patnáctiminutové	patnáctiminutový	k2eAgNnSc4d1	patnáctiminutové
prodloužení	prodloužení	k1gNnSc4	prodloužení
vycházky	vycházka	k1gFnSc2	vycházka
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
malými	malý	k2eAgInPc7d1	malý
problémy	problém	k1gInPc7	problém
astronauti	astronaut	k1gMnPc1	astronaut
pomocí	pomoc	k1gFnPc2	pomoc
zařízení	zařízení	k1gNnPc2	zařízení
Lunar	Lunar	k1gMnSc1	Lunar
Equipment	Equipment	k1gMnSc1	Equipment
Conveyor	Conveyor	k1gMnSc1	Conveyor
(	(	kIx(	(
<g/>
jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
lanovky	lanovka	k1gFnPc1	lanovka
pro	pro	k7c4	pro
dopravu	doprava	k1gFnSc4	doprava
předmětů	předmět	k1gInPc2	předmět
vzhůru	vzhůru	k6eAd1	vzhůru
do	do	k7c2	do
úložného	úložný	k2eAgInSc2d1	úložný
prostoru	prostor	k1gInSc2	prostor
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
<g/>
)	)	kIx)	)
naložili	naložit	k5eAaPmAgMnP	naložit
film	film	k1gInSc4	film
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
krabice	krabice	k1gFnPc4	krabice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
obsahovaly	obsahovat	k5eAaImAgFnP	obsahovat
22	[number]	k4	22
kg	kg	kA	kg
vzorků	vzorek	k1gInPc2	vzorek
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Eaglu	Eagl	k1gInSc2	Eagl
se	se	k3xPyFc4	se
astronauti	astronaut	k1gMnPc1	astronaut
vrátili	vrátit	k5eAaPmAgMnP	vrátit
v	v	k7c6	v
opačném	opačný	k2eAgNnSc6d1	opačné
pořadí	pořadí	k1gNnSc6	pořadí
–	–	k?	–
Aldrin	aldrin	k1gInSc1	aldrin
jako	jako	k8xC	jako
první	první	k4xOgFnSc1	první
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
Armstrong	Armstrong	k1gMnSc1	Armstrong
vyskočil	vyskočit	k5eAaPmAgMnS	vyskočit
na	na	k7c4	na
třetí	třetí	k4xOgInSc4	třetí
stupínek	stupínek	k1gInSc4	stupínek
žebříku	žebřík	k1gInSc2	žebřík
a	a	k8xC	a
vyšplhal	vyšplhat	k5eAaPmAgMnS	vyšplhat
do	do	k7c2	do
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
astronauti	astronaut	k1gMnPc1	astronaut
zanechali	zanechat	k5eAaPmAgMnP	zanechat
kromě	kromě	k7c2	kromě
vědeckých	vědecký	k2eAgInPc2d1	vědecký
přístrojů	přístroj	k1gInPc2	přístroj
také	také	k9	také
plaketu	plaketa	k1gFnSc4	plaketa
(	(	kIx(	(
<g/>
připevněnou	připevněný	k2eAgFnSc4d1	připevněná
na	na	k7c6	na
žebříku	žebřík	k1gInSc6	žebřík
sestupové	sestupový	k2eAgFnSc2d1	sestupová
části	část	k1gFnSc2	část
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
<g/>
)	)	kIx)	)
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
kresbami	kresba	k1gFnPc7	kresba
Země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
západní	západní	k2eAgFnSc2d1	západní
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
polokoule	polokoule	k1gFnSc2	polokoule
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nápisem	nápis	k1gInSc7	nápis
a	a	k8xC	a
podpisy	podpis	k1gInPc1	podpis
astronautů	astronaut	k1gMnPc2	astronaut
a	a	k8xC	a
prezidenta	prezident	k1gMnSc2	prezident
Richarda	Richard	k1gMnSc2	Richard
Nixona	Nixon	k1gMnSc2	Nixon
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
plaketě	plaketa	k1gFnSc6	plaketa
je	být	k5eAaImIp3nS	být
napsáno	napsat	k5eAaBmNgNnS	napsat
<g/>
:	:	kIx,	:
V	v	k7c6	v
českém	český	k2eAgInSc6d1	český
překladu	překlad	k1gInSc6	překlad
<g/>
:	:	kIx,	:
Po	po	k7c6	po
přesunu	přesun	k1gInSc6	přesun
do	do	k7c2	do
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
astronauti	astronaut	k1gMnPc1	astronaut
zapnuli	zapnout	k5eAaPmAgMnP	zapnout
návratový	návratový	k2eAgInSc4d1	návratový
stupeň	stupeň	k1gInSc4	stupeň
a	a	k8xC	a
zbavili	zbavit	k5eAaPmAgMnP	zbavit
se	se	k3xPyFc4	se
svých	svůj	k3xOyFgInPc2	svůj
batohů	batoh	k1gInPc2	batoh
s	s	k7c7	s
PLSS	PLSS	kA	PLSS
<g/>
,	,	kIx,	,
jedné	jeden	k4xCgFnSc2	jeden
kamery	kamera	k1gFnSc2	kamera
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
už	už	k6eAd1	už
nepotřebných	potřebný	k2eNgFnPc2d1	nepotřebná
věcí	věc	k1gFnPc2	věc
<g/>
.	.	kIx.	.
</s>
<s>
Start	start	k1gInSc1	start
návratového	návratový	k2eAgInSc2d1	návratový
stupně	stupeň	k1gInSc2	stupeň
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
bez	bez	k7c2	bez
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Astronauti	astronaut	k1gMnPc1	astronaut
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
na	na	k7c4	na
lunární	lunární	k2eAgFnSc4d1	lunární
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
a	a	k8xC	a
tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
spojili	spojit	k5eAaPmAgMnP	spojit
s	s	k7c7	s
velitelským	velitelský	k2eAgInSc7d1	velitelský
modulem	modul	k1gInSc7	modul
Columbia	Columbia	k1gFnSc1	Columbia
<g/>
,	,	kIx,	,
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
kterého	který	k3yIgInSc2	který
čekal	čekat	k5eAaImAgMnS	čekat
Michael	Michael	k1gMnSc1	Michael
Collins	Collinsa	k1gFnPc2	Collinsa
<g/>
.	.	kIx.	.
</s>
<s>
Eagle	Eagle	k6eAd1	Eagle
po	po	k7c6	po
odpojení	odpojení	k1gNnSc6	odpojení
od	od	k7c2	od
Columbie	Columbia	k1gFnSc2	Columbia
zůstal	zůstat	k5eAaPmAgMnS	zůstat
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
okolo	okolo	k7c2	okolo
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
NASA	NASA	kA	NASA
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
dráha	dráha	k1gFnSc1	dráha
nebyla	být	k5eNaImAgFnS	být
stabilní	stabilní	k2eAgFnSc1d1	stabilní
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vyústilo	vyústit	k5eAaPmAgNnS	vyústit
v	v	k7c4	v
pád	pád	k1gInSc4	pád
Eaglu	Eagl	k1gInSc2	Eagl
na	na	k7c4	na
neznámé	známý	k2eNgNnSc4d1	neznámé
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Astronauti	astronaut	k1gMnPc1	astronaut
přistáli	přistát	k5eAaPmAgMnP	přistát
24	[number]	k4	24
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
na	na	k7c6	na
hladině	hladina	k1gFnSc6	hladina
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
na	na	k7c4	na
13	[number]	k4	13
<g/>
°	°	k?	°
19	[number]	k4	19
<g/>
'	'	kIx"	'
N	N	kA	N
a	a	k8xC	a
169	[number]	k4	169
<g/>
°	°	k?	°
9	[number]	k4	9
<g/>
'	'	kIx"	'
W	W	kA	W
<g/>
,	,	kIx,	,
2	[number]	k4	2
660	[number]	k4	660
km	km	kA	km
východně	východně	k6eAd1	východně
od	od	k7c2	od
ostrova	ostrov	k1gInSc2	ostrov
Wake	Wak	k1gInSc2	Wak
<g/>
,	,	kIx,	,
380	[number]	k4	380
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Johnsonova	Johnsonův	k2eAgInSc2d1	Johnsonův
atolu	atol	k1gInSc2	atol
a	a	k8xC	a
24	[number]	k4	24
km	km	kA	km
od	od	k7c2	od
letadlové	letadlový	k2eAgFnSc2d1	letadlová
lodi	loď	k1gFnSc2	loď
USS	USS	kA	USS
Hornet	Hornet	k1gInSc1	Hornet
<g/>
.	.	kIx.	.
</s>
<s>
Velitelský	velitelský	k2eAgInSc1d1	velitelský
modul	modul	k1gInSc1	modul
zprvu	zprvu	k6eAd1	zprvu
přistál	přistát	k5eAaPmAgInS	přistát
vzhůru	vzhůru	k6eAd1	vzhůru
dnem	den	k1gInSc7	den
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
během	během	k7c2	během
několika	několik	k4yIc2	několik
minut	minuta	k1gFnPc2	minuta
se	se	k3xPyFc4	se
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
nafukovacích	nafukovací	k2eAgInPc2d1	nafukovací
balónů	balón	k1gInPc2	balón
otočil	otočit	k5eAaPmAgInS	otočit
do	do	k7c2	do
správné	správný	k2eAgFnSc2d1	správná
polohy	poloha	k1gFnSc2	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vrtulníků	vrtulník	k1gInPc2	vrtulník
seskočili	seskočit	k5eAaPmAgMnP	seskočit
čtyři	čtyři	k4xCgMnPc1	čtyři
potápěči	potápěč	k1gMnPc1	potápěč
v	v	k7c6	v
ochranných	ochranný	k2eAgInPc6d1	ochranný
oděvech	oděv	k1gInPc6	oděv
(	(	kIx(	(
<g/>
Biological	Biological	k1gFnSc1	Biological
Isolation	Isolation	k1gInSc1	Isolation
Garments	Garments	k1gInSc1	Garments
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
stabilizovali	stabilizovat	k5eAaBmAgMnP	stabilizovat
Apollo	Apollo	k1gNnSc4	Apollo
pomocí	pomocí	k7c2	pomocí
nafukovacích	nafukovací	k2eAgInPc2d1	nafukovací
vaků	vak	k1gInPc2	vak
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
otevřel	otevřít	k5eAaPmAgMnS	otevřít
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
lodi	loď	k1gFnSc2	loď
a	a	k8xC	a
podal	podat	k5eAaPmAgMnS	podat
astronautům	astronaut	k1gMnPc3	astronaut
tytéž	týž	k3xTgInPc4	týž
oděvy	oděv	k1gInPc4	oděv
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
do	do	k7c2	do
nich	on	k3xPp3gMnPc2	on
převlékli	převléknout	k5eAaPmAgMnP	převléknout
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byla	být	k5eAaImAgFnS	být
obava	obava	k1gFnSc1	obava
z	z	k7c2	z
možného	možný	k2eAgNnSc2d1	možné
zavlečení	zavlečení	k1gNnSc2	zavlečení
mimozemského	mimozemský	k2eAgInSc2d1	mimozemský
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
astronauty	astronaut	k1gMnPc7	astronaut
vyzvedli	vyzvednout	k5eAaPmAgMnP	vyzvednout
do	do	k7c2	do
vrtulníku	vrtulník	k1gInSc2	vrtulník
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
odvezl	odvézt	k5eAaPmAgMnS	odvézt
na	na	k7c4	na
Hornet	Hornet	k1gInSc4	Hornet
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
byli	být	k5eAaImAgMnP	být
zavřeni	zavřen	k2eAgMnPc1d1	zavřen
v	v	k7c6	v
izolovaném	izolovaný	k2eAgInSc6d1	izolovaný
kontejneru	kontejner	k1gInSc6	kontejner
<g/>
.	.	kIx.	.
</s>
<s>
Karanténa	karanténa	k1gFnSc1	karanténa
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
i	i	k9	i
na	na	k7c6	na
pevnině	pevnina	k1gFnSc6	pevnina
až	až	k9	až
do	do	k7c2	do
11	[number]	k4	11
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Hornetu	Hornet	k1gInSc2	Hornet
se	se	k3xPyFc4	se
s	s	k7c7	s
astronauty	astronaut	k1gMnPc7	astronaut
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
okno	okno	k1gNnSc4	okno
karantény	karanténa	k1gFnSc2	karanténa
<g/>
,	,	kIx,	,
pozdravili	pozdravit	k5eAaPmAgMnP	pozdravit
prezident	prezident	k1gMnSc1	prezident
Nixon	Nixon	k1gMnSc1	Nixon
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
NASA	NASA	kA	NASA
Thomas	Thomas	k1gMnSc1	Thomas
Paine	Pain	k1gInSc5	Pain
a	a	k8xC	a
kolega	kolega	k1gMnSc1	kolega
Frank	Frank	k1gMnSc1	Frank
Borman	Borman	k1gMnSc1	Borman
<g/>
.	.	kIx.	.
</s>
<s>
Hornet	Hornet	k1gInSc1	Hornet
doplul	doplout	k5eAaPmAgInS	doplout
do	do	k7c2	do
Pearl	Pearla	k1gFnPc2	Pearla
Harboru	Harbor	k1gInSc2	Harbor
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
astronauti	astronaut	k1gMnPc1	astronaut
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
letecky	letecky	k6eAd1	letecky
do	do	k7c2	do
Johnsonova	Johnsonův	k2eAgNnSc2d1	Johnsonovo
vesmírného	vesmírný	k2eAgNnSc2d1	vesmírné
střediska	středisko	k1gNnSc2	středisko
v	v	k7c6	v
Houstonu	Houston	k1gInSc6	Houston
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
karantény	karanténa	k1gFnSc2	karanténa
měli	mít	k5eAaImAgMnP	mít
den	den	k1gInSc4	den
volna	volno	k1gNnSc2	volno
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
strávili	strávit	k5eAaPmAgMnP	strávit
s	s	k7c7	s
rodinami	rodina	k1gFnPc7	rodina
<g/>
,	,	kIx,	,
a	a	k8xC	a
od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
vyrazili	vyrazit	k5eAaPmAgMnP	vyrazit
na	na	k7c4	na
slavnostní	slavnostní	k2eAgNnSc4d1	slavnostní
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
později	pozdě	k6eAd2	pozdě
zavítali	zavítat	k5eAaPmAgMnP	zavítat
do	do	k7c2	do
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
byli	být	k5eAaImAgMnP	být
přijati	přijmout	k5eAaPmNgMnP	přijmout
v	v	k7c6	v
Kongresu	kongres	k1gInSc6	kongres
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
následující	následující	k2eAgFnSc2d1	následující
pětačtyřicetidenní	pětačtyřicetidenní	k2eAgFnSc2d1	pětačtyřicetidenní
cesty	cesta	k1gFnSc2	cesta
projeli	projet	k5eAaPmAgMnP	projet
25	[number]	k4	25
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Představitelé	představitel	k1gMnPc1	představitel
NASA	NASA	kA	NASA
opakovaně	opakovaně	k6eAd1	opakovaně
zdůrazňovali	zdůrazňovat	k5eAaImAgMnP	zdůrazňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
letu	let	k1gInSc2	let
kosmické	kosmický	k2eAgFnSc2d1	kosmická
lodi	loď	k1gFnSc2	loď
Apollo	Apollo	k1gNnSc1	Apollo
11	[number]	k4	11
bylo	být	k5eAaImAgNnS	být
řešení	řešení	k1gNnSc1	řešení
technických	technický	k2eAgFnPc2d1	technická
otázek	otázka	k1gFnPc2	otázka
<g/>
,	,	kIx,	,
ne	ne	k9	ne
výzkum	výzkum	k1gInSc4	výzkum
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
techniku	technika	k1gFnSc4	technika
<g/>
,	,	kIx,	,
hlavním	hlavní	k2eAgInSc7d1	hlavní
výsledkem	výsledek	k1gInSc7	výsledek
mise	mise	k1gFnSc2	mise
bylo	být	k5eAaImAgNnS	být
prokázání	prokázání	k1gNnSc4	prokázání
účinnosti	účinnost	k1gFnSc2	účinnost
zvoleného	zvolený	k2eAgInSc2d1	zvolený
způsobu	způsob	k1gInSc2	způsob
letu	let	k1gInSc2	let
<g/>
,	,	kIx,	,
přistání	přistání	k1gNnSc4	přistání
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
a	a	k8xC	a
startu	start	k1gInSc6	start
z	z	k7c2	z
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
prokázání	prokázání	k1gNnSc4	prokázání
schopnosti	schopnost	k1gFnSc2	schopnost
posádky	posádka	k1gFnSc2	posádka
pohybovat	pohybovat	k5eAaImF	pohybovat
se	se	k3xPyFc4	se
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
provádět	provádět	k5eAaImF	provádět
zde	zde	k6eAd1	zde
výzkum	výzkum	k1gInSc4	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
měla	mít	k5eAaImAgFnS	mít
expedice	expedice	k1gFnPc4	expedice
velký	velký	k2eAgInSc4d1	velký
vědecký	vědecký	k2eAgInSc4d1	vědecký
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
dopravila	dopravit	k5eAaPmAgFnS	dopravit
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
první	první	k4xOgInPc1	první
vzorky	vzorek	k1gInPc1	vzorek
měsíčního	měsíční	k2eAgInSc2d1	měsíční
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
21,55	[number]	k4	21,55
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
letu	let	k1gInSc6	let
k	k	k7c3	k
Měsíci	měsíc	k1gInSc3	měsíc
(	(	kIx(	(
<g/>
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
<g/>
)	)	kIx)	)
astronauti	astronaut	k1gMnPc1	astronaut
ve	v	k7c6	v
volných	volný	k2eAgFnPc6d1	volná
chvílích	chvíle	k1gFnPc6	chvíle
poslouchali	poslouchat	k5eAaImAgMnP	poslouchat
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Neil	Neil	k1gMnSc1	Neil
Armstrong	Armstrong	k1gMnSc1	Armstrong
si	se	k3xPyFc3	se
za	za	k7c7	za
tímto	tento	k3xDgInSc7	tento
účelem	účel	k1gInSc7	účel
vyžádal	vyžádat	k5eAaPmAgMnS	vyžádat
dvě	dva	k4xCgFnPc4	dva
skladby	skladba	k1gFnPc4	skladba
–	–	k?	–
Novosvětskou	novosvětský	k2eAgFnSc4d1	Novosvětská
symfonii	symfonie	k1gFnSc4	symfonie
Antonína	Antonín	k1gMnSc2	Antonín
Dvořáka	Dvořák	k1gMnSc2	Dvořák
a	a	k8xC	a
album	album	k1gNnSc1	album
Music	Musice	k1gFnPc2	Musice
out	out	k?	out
of	of	k?	of
the	the	k?	the
Moon	Moon	k1gInSc1	Moon
Samuela	Samuel	k1gMnSc2	Samuel
Hoffmana	Hoffman	k1gMnSc2	Hoffman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
posádka	posádka	k1gFnSc1	posádka
Apolla	Apollo	k1gNnSc2	Apollo
11	[number]	k4	11
pobývala	pobývat	k5eAaImAgFnS	pobývat
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
na	na	k7c4	na
jeho	on	k3xPp3gInSc4	on
povrch	povrch	k1gInSc4	povrch
zřítila	zřítit	k5eAaPmAgFnS	zřítit
neúspěšná	úspěšný	k2eNgFnSc1d1	neúspěšná
sovětská	sovětský	k2eAgFnSc1d1	sovětská
sonda	sonda	k1gFnSc1	sonda
Luna	luna	k1gFnSc1	luna
15	[number]	k4	15
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
součást	součást	k1gFnSc1	součást
závodů	závod	k1gInPc2	závod
o	o	k7c4	o
dosažení	dosažení	k1gNnSc4	dosažení
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Úkolem	úkol	k1gInSc7	úkol
sondy	sonda	k1gFnSc2	sonda
bylo	být	k5eAaImAgNnS	být
donést	donést	k5eAaPmF	donést
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
vzorky	vzorek	k1gInPc1	vzorek
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
podaří	podařit	k5eAaPmIp3nS	podařit
Američanům	Američan	k1gMnPc3	Američan
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
přistání	přistání	k1gNnSc6	přistání
<g/>
,	,	kIx,	,
předtím	předtím	k6eAd1	předtím
než	než	k8xS	než
začala	začít	k5eAaPmAgFnS	začít
příprava	příprava	k1gFnSc1	příprava
na	na	k7c4	na
"	"	kIx"	"
<g/>
procházku	procházka	k1gFnSc4	procházka
<g/>
"	"	kIx"	"
po	po	k7c6	po
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
Aldrin	aldrin	k1gInSc1	aldrin
odvysílal	odvysílat	k5eAaPmAgInS	odvysílat
<g/>
:	:	kIx,	:
Potom	potom	k6eAd1	potom
v	v	k7c6	v
soukromí	soukromí	k1gNnSc6	soukromí
přijal	přijmout	k5eAaPmAgInS	přijmout
eucharistii	eucharistie	k1gFnSc4	eucharistie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
NASA	NASA	kA	NASA
stále	stále	k6eAd1	stále
soudila	soudit	k5eAaImAgFnS	soudit
s	s	k7c7	s
Madalyn	Madalyn	k1gMnSc1	Madalyn
Murray	Murraa	k1gFnPc4	Murraa
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Hairovou	Hairová	k1gFnSc4	Hairová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
protestovala	protestovat	k5eAaBmAgFnS	protestovat
proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
posádka	posádka	k1gFnSc1	posádka
Apolla	Apollo	k1gNnSc2	Apollo
8	[number]	k4	8
četla	číst	k5eAaImAgFnS	číst
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
Genesis	Genesis	k1gFnSc1	Genesis
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
astronauti	astronaut	k1gMnPc1	astronaut
se	se	k3xPyFc4	se
nezdrželi	zdržet	k5eNaPmAgMnP	zdržet
náboženských	náboženský	k2eAgFnPc2d1	náboženská
aktivit	aktivita	k1gFnPc2	aktivita
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Aldrin	aldrin	k1gInSc1	aldrin
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
nikdy	nikdy	k6eAd1	nikdy
nevzpomínal	vzpomínat	k5eNaImAgMnS	vzpomínat
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
to	ten	k3xDgNnSc1	ten
nepověděl	povědět	k5eNaPmAgInS	povědět
ani	ani	k8xC	ani
své	svůj	k3xOyFgFnSc3	svůj
manželce	manželka	k1gFnSc3	manželka
a	a	k8xC	a
několik	několik	k4yIc1	několik
let	léto	k1gNnPc2	léto
trvalo	trvat	k5eAaImAgNnS	trvat
<g/>
,	,	kIx,	,
než	než	k8xS	než
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
vyšel	vyjít	k5eAaPmAgInS	vyjít
na	na	k7c4	na
veřejnost	veřejnost	k1gFnSc4	veřejnost
(	(	kIx(	(
<g/>
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
autobiografii	autobiografie	k1gFnSc6	autobiografie
Return	Return	k1gMnSc1	Return
to	ten	k3xDgNnSc4	ten
Earth	Earth	k1gMnSc1	Earth
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Replika	replika	k1gFnSc1	replika
stopy	stopa	k1gFnSc2	stopa
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
zanechal	zanechat	k5eAaPmAgMnS	zanechat
Neil	Neil	k1gMnSc1	Neil
Armstrong	Armstrong	k1gMnSc1	Armstrong
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
umístěná	umístěný	k2eAgFnSc1d1	umístěná
v	v	k7c4	v
Tranquility	Tranquilit	k1gInPc4	Tranquilit
Parku	park	k1gInSc2	park
v	v	k7c6	v
Houstonu	Houston	k1gInSc6	Houston
<g/>
;	;	kIx,	;
park	park	k1gInSc1	park
byl	být	k5eAaImAgInS	být
slavnostně	slavnostně	k6eAd1	slavnostně
otevřen	otevřít	k5eAaPmNgInS	otevřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
desátého	desátý	k4xOgInSc2	desátý
výročí	výročí	k1gNnSc2	výročí
prvního	první	k4xOgNnSc2	první
přistání	přistání	k1gNnSc2	přistání
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Armstrong	Armstrong	k1gMnSc1	Armstrong
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
svůj	svůj	k3xOyFgInSc4	svůj
slavný	slavný	k2eAgInSc4d1	slavný
výrok	výrok	k1gInSc4	výrok
řekl	říct	k5eAaPmAgMnS	říct
jako	jako	k8xS	jako
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
That	That	k1gInSc1	That
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
one	one	k?	one
small	smalnout	k5eAaPmAgInS	smalnout
step	step	k1gInSc1	step
for	forum	k1gNnPc2	forum
a	a	k8xC	a
man	mana	k1gFnPc2	mana
<g/>
,	,	kIx,	,
one	one	k?	one
giant	giant	k1gMnSc1	giant
leap	leap	k1gMnSc1	leap
for	forum	k1gNnPc2	forum
mankind	mankind	k1gInSc1	mankind
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
"	"	kIx"	"
<g/>
a	a	k8xC	a
<g/>
"	"	kIx"	"
nebylo	být	k5eNaImAgNnS	být
zachyceno	zachytit	k5eAaPmNgNnS	zachytit
pozemní	pozemní	k2eAgFnSc7d1	pozemní
kontrolou	kontrola	k1gFnSc7	kontrola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
výraz	výraz	k1gInSc1	výraz
"	"	kIx"	"
<g/>
man	man	k1gMnSc1	man
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
bez	bez	k1gInSc4	bez
a	a	k8xC	a
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
lidstvo	lidstvo	k1gNnSc1	lidstvo
<g/>
"	"	kIx"	"
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
"	"	kIx"	"
<g/>
mankind	mankind	k1gMnSc1	mankind
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
"	"	kIx"	"
<g/>
a	a	k8xC	a
man	man	k1gMnSc1	man
<g/>
"	"	kIx"	"
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
člověk	člověk	k1gMnSc1	člověk
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
výrok	výrok	k1gInSc1	výrok
bez	bez	k1gInSc1	bez
"	"	kIx"	"
<g/>
a	a	k8xC	a
<g/>
"	"	kIx"	"
by	by	kYmCp3nS	by
neměl	mít	k5eNaImAgMnS	mít
smysl	smysl	k1gInSc4	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgInPc1d2	pozdější
důkladné	důkladný	k2eAgInPc1d1	důkladný
rozbory	rozbor	k1gInPc1	rozbor
záznamu	záznam	k1gInSc2	záznam
stopu	stop	k1gInSc2	stop
po	po	k7c6	po
chybějícím	chybějící	k2eAgMnSc6d1	chybějící
"	"	kIx"	"
<g/>
a	a	k8xC	a
<g/>
"	"	kIx"	"
nenašly	najít	k5eNaPmAgFnP	najít
<g/>
.	.	kIx.	.
</s>
<s>
Nejpravděpodobnější	pravděpodobný	k2eAgNnSc1d3	nejpravděpodobnější
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
okamžiku	okamžik	k1gInSc2	okamžik
Armstrong	Armstrong	k1gMnSc1	Armstrong
"	"	kIx"	"
<g/>
a	a	k8xC	a
<g/>
"	"	kIx"	"
nechtěně	chtěně	k6eNd1	chtěně
vynechal	vynechat	k5eAaPmAgInS	vynechat
<g/>
.	.	kIx.	.
</s>
<s>
Perličkou	perlička	k1gFnSc7	perlička
je	být	k5eAaImIp3nS	být
srovnání	srovnání	k1gNnSc1	srovnání
letu	let	k1gInSc2	let
Apolla	Apollo	k1gNnSc2	Apollo
11	[number]	k4	11
s	s	k7c7	s
románem	román	k1gInSc7	román
Julesa	Julesa	k1gFnSc1	Julesa
Verna	Vern	k1gMnSc2	Vern
Ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Verne	Vernout	k5eAaPmIp3nS	Vernout
se	se	k3xPyFc4	se
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
přesností	přesnost	k1gFnSc7	přesnost
trefil	trefit	k5eAaPmAgMnS	trefit
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
skutečností	skutečnost	k1gFnPc2	skutečnost
skutečného	skutečný	k2eAgInSc2d1	skutečný
letu	let	k1gInSc2	let
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místě	místo	k1gNnSc6	místo
startu	start	k1gInSc2	start
se	se	k3xPyFc4	se
spletl	splést	k5eAaPmAgMnS	splést
jen	jen	k6eAd1	jen
o	o	k7c4	o
asi	asi	k9	asi
100	[number]	k4	100
km	km	kA	km
<g/>
,	,	kIx,	,
dělo	dělo	k1gNnSc4	dělo
<g/>
,	,	kIx,	,
kterým	který	k3yRgNnSc7	který
vyslal	vyslat	k5eAaPmAgMnS	vyslat
své	svůj	k3xOyFgMnPc4	svůj
hrdiny	hrdina	k1gMnPc4	hrdina
se	se	k3xPyFc4	se
jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
Kolumbiada	Kolumbiada	k1gFnSc1	Kolumbiada
<g/>
,	,	kIx,	,
posádku	posádka	k1gFnSc4	posádka
tvořili	tvořit	k5eAaImAgMnP	tvořit
tři	tři	k4xCgMnPc1	tři
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
ke	k	k7c3	k
korekci	korekce	k1gFnSc3	korekce
dráhy	dráha	k1gFnSc2	dráha
používali	používat	k5eAaImAgMnP	používat
rakety	raketa	k1gFnPc4	raketa
<g/>
,	,	kIx,	,
návrat	návrat	k1gInSc1	návrat
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
na	na	k7c4	na
mořskou	mořský	k2eAgFnSc4d1	mořská
hladinu	hladina	k1gFnSc4	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
internetového	internetový	k2eAgInSc2d1	internetový
obchodu	obchod	k1gInSc2	obchod
Amazon	amazona	k1gFnPc2	amazona
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
Jeff	Jeff	k1gMnSc1	Jeff
Bezos	Bezos	k1gMnSc1	Bezos
při	při	k7c6	při
podmořské	podmořský	k2eAgFnSc6d1	podmořská
expedici	expedice	k1gFnSc6	expedice
našel	najít	k5eAaPmAgMnS	najít
pomocí	pomocí	k7c2	pomocí
sonaru	sonar	k1gInSc2	sonar
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
motorů	motor	k1gInPc2	motor
Saturnu	Saturn	k1gInSc2	Saturn
V.	V.	kA	V.
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
odhozen	odhodit	k5eAaPmNgInS	odhodit
po	po	k7c6	po
startu	start	k1gInSc6	start
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
ležel	ležet	k5eAaImAgMnS	ležet
na	na	k7c6	na
mořském	mořský	k2eAgInSc6d1	mořský
dnu	den	k1gInSc6	den
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
4	[number]	k4	4
267	[number]	k4	267
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Jeff	Jeff	k1gMnSc1	Jeff
Bezos	Bezos	k1gMnSc1	Bezos
plánoval	plánovat	k5eAaImAgMnS	plánovat
vyzvednutí	vyzvednutí	k1gNnSc4	vyzvednutí
motoru	motor	k1gInSc2	motor
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc2	jeho
umístění	umístění	k1gNnSc2	umístění
v	v	k7c6	v
muzeu	muzeum	k1gNnSc6	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
plány	plán	k1gInPc7	plán
souhlasí	souhlasit	k5eAaImIp3nP	souhlasit
i	i	k9	i
NASA	NASA	kA	NASA
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
dosud	dosud	k6eAd1	dosud
majitelkou	majitelka	k1gFnSc7	majitelka
motoru	motor	k1gInSc2	motor
<g/>
.	.	kIx.	.
</s>
<s>
Říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
pobytu	pobyt	k1gInSc2	pobyt
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
Armstrong	Armstrong	k1gMnSc1	Armstrong
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Hodně	hodně	k6eAd1	hodně
štěstí	štěstit	k5eAaImIp3nS	štěstit
<g/>
,	,	kIx,	,
pane	pan	k1gMnSc5	pan
Gorski	Gorsk	k1gMnSc5	Gorsk
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
poté	poté	k6eAd1	poté
pan	pan	k1gMnSc1	pan
Gorski	Gorske	k1gFnSc4	Gorske
zemřel	zemřít	k5eAaPmAgMnS	zemřít
a	a	k8xC	a
tak	tak	k6eAd1	tak
mohl	moct	k5eAaImAgMnS	moct
Armstrong	Armstrong	k1gMnSc1	Armstrong
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
<g/>
,	,	kIx,	,
co	co	k8xS	co
tím	ten	k3xDgNnSc7	ten
myslel	myslet	k5eAaImAgMnS	myslet
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
malý	malý	k2eAgMnSc1d1	malý
chlapec	chlapec	k1gMnSc1	chlapec
<g/>
,	,	kIx,	,
slyšel	slyšet	k5eAaImAgMnS	slyšet
hádku	hádka	k1gFnSc4	hádka
sousedů	soused	k1gMnPc2	soused
<g/>
.	.	kIx.	.
</s>
<s>
Paní	paní	k1gFnSc1	paní
Gorská	Gorská	k1gFnSc1	Gorská
křičela	křičet	k5eAaImAgFnS	křičet
na	na	k7c4	na
pana	pan	k1gMnSc4	pan
Gorského	Gorský	k1gMnSc4	Gorský
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Orální	orální	k2eAgInSc4d1	orální
sex	sex	k1gInSc4	sex
<g/>
?	?	kIx.	?
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
bys	by	kYmCp2nS	by
chtěl	chtít	k5eAaImAgMnS	chtít
orální	orální	k2eAgInSc4d1	orální
sex	sex	k1gInSc4	sex
<g/>
?!	?!	k?	?!
Tak	tak	k6eAd1	tak
ten	ten	k3xDgMnSc1	ten
budeš	být	k5eAaImBp2nS	být
mít	mít	k5eAaImF	mít
<g/>
,	,	kIx,	,
až	až	k8xS	až
ten	ten	k3xDgMnSc1	ten
sousedovic	sousedovice	k1gFnPc2	sousedovice
kluk	kluk	k1gMnSc1	kluk
bude	být	k5eAaImBp3nS	být
chodit	chodit	k5eAaImF	chodit
po	po	k7c6	po
Měsíci	měsíc	k1gInSc6	měsíc
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Tato	tento	k3xDgFnSc1	tento
historka	historka	k1gFnSc1	historka
je	být	k5eAaImIp3nS	být
smyšlená	smyšlený	k2eAgFnSc1d1	smyšlená
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Armstrong	Armstrong	k1gMnSc1	Armstrong
se	se	k3xPyFc4	se
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
poprvé	poprvé	k6eAd1	poprvé
slyšel	slyšet	k5eAaImAgMnS	slyšet
od	od	k7c2	od
jednoho	jeden	k4xCgMnSc2	jeden
komika	komik	k1gMnSc2	komik
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
fotografiích	fotografia	k1gFnPc6	fotografia
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Měsíce	měsíc	k1gInSc2	měsíc
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
Aldrin	aldrin	k1gInSc4	aldrin
a	a	k8xC	a
na	na	k7c4	na
žádné	žádný	k3yNgNnSc4	žádný
Armstrong	Armstrong	k1gMnSc1	Armstrong
neboť	neboť	k8xC	neboť
údajně	údajně	k6eAd1	údajně
<g/>
,	,	kIx,	,
když	když	k8xS	když
Aldrin	aldrin	k1gInSc1	aldrin
vytahoval	vytahovat	k5eAaImAgInS	vytahovat
fotografický	fotografický	k2eAgInSc4d1	fotografický
film	film	k1gInSc4	film
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
hasselbladu	hasselblad	k1gInSc2	hasselblad
<g/>
,	,	kIx,	,
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
než	než	k8xS	než
jej	on	k3xPp3gMnSc4	on
vložil	vložit	k5eAaPmAgMnS	vložit
do	do	k7c2	do
kapsy	kapsa	k1gFnSc2	kapsa
skafandru	skafandr	k1gInSc2	skafandr
<g/>
,	,	kIx,	,
jej	on	k3xPp3gInSc4	on
položil	položit	k5eAaPmAgMnS	položit
na	na	k7c4	na
podstavec	podstavec	k1gInSc4	podstavec
lunárního	lunární	k2eAgInSc2d1	lunární
modulu	modul	k1gInSc2	modul
a	a	k8xC	a
nešťastnou	šťastný	k2eNgFnSc7d1	nešťastná
shodou	shoda	k1gFnSc7	shoda
okolností	okolnost	k1gFnPc2	okolnost
jej	on	k3xPp3gNnSc4	on
tam	tam	k6eAd1	tam
zapomněl	zapomnět	k5eAaImAgMnS	zapomnět
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
Aldrin	aldrin	k1gInSc1	aldrin
žádný	žádný	k3yNgInSc4	žádný
vlastní	vlastní	k2eAgInSc4d1	vlastní
fotoaparát	fotoaparát	k1gInSc4	fotoaparát
neměl	mít	k5eNaImAgMnS	mít
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
měl	mít	k5eAaImAgMnS	mít
na	na	k7c6	na
starosti	starost	k1gFnSc6	starost
jiné	jiný	k2eAgInPc4d1	jiný
úkony	úkon	k1gInPc4	úkon
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
Armstrong	Armstrong	k1gMnSc1	Armstrong
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
fotografii	fotografia	k1gFnSc6	fotografia
(	(	kIx(	(
<g/>
č.	č.	k?	č.
AS	as	k9	as
<g/>
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
40	[number]	k4	40
<g/>
-	-	kIx~	-
<g/>
5886	[number]	k4	5886
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
jednoho	jeden	k4xCgInSc2	jeden
Aldrinova	Aldrinův	k2eAgInSc2d1	Aldrinův
panoramata	panorama	k1gNnPc1	panorama
pořízeného	pořízený	k2eAgInSc2d1	pořízený
fotoaparátem	fotoaparát	k1gInSc7	fotoaparát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
mu	on	k3xPp3gMnSc3	on
za	za	k7c7	za
tímto	tento	k3xDgInSc7	tento
účelem	účel	k1gInSc7	účel
Armstrong	Armstrong	k1gMnSc1	Armstrong
na	na	k7c4	na
chvíli	chvíle	k1gFnSc4	chvíle
předal	předat	k5eAaPmAgMnS	předat
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Konspirační	konspirační	k2eAgFnSc2d1	konspirační
teorie	teorie	k1gFnSc2	teorie
o	o	k7c4	o
přistání	přistání	k1gNnSc4	přistání
Apolla	Apollo	k1gNnSc2	Apollo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
konspirační	konspirační	k2eAgFnPc1d1	konspirační
teorie	teorie	k1gFnPc1	teorie
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
tvrdily	tvrdit	k5eAaImAgInP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
přistání	přistání	k1gNnSc4	přistání
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
byl	být	k5eAaImAgInS	být
podvod	podvod	k1gInSc1	podvod
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
zastánců	zastánce	k1gMnPc2	zastánce
těchto	tento	k3xDgFnPc2	tento
teorií	teorie	k1gFnPc2	teorie
byl	být	k5eAaImAgMnS	být
Bill	Bill	k1gMnSc1	Bill
Kaysing	Kaysing	k1gInSc4	Kaysing
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
své	svůj	k3xOyFgInPc4	svůj
názory	názor	k1gInPc4	názor
vyložil	vyložit	k5eAaPmAgMnS	vyložit
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
We	We	k1gMnPc2	We
Never	Nevra	k1gFnPc2	Nevra
Went	Went	k2eAgMnSc1d1	Went
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Moon	Moon	k1gNnSc1	Moon
vydané	vydaný	k2eAgNnSc1d1	vydané
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
šíření	šíření	k1gNnSc3	šíření
teorií	teorie	k1gFnPc2	teorie
přispěl	přispět	k5eAaPmAgMnS	přispět
i	i	k9	i
vědeckofantastický	vědeckofantastický	k2eAgInSc4d1	vědeckofantastický
film	film	k1gInSc4	film
Kozoroh	Kozoroh	k1gMnSc1	Kozoroh
1	[number]	k4	1
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
o	o	k7c6	o
fiktivním	fiktivní	k2eAgNnSc6d1	fiktivní
falešném	falešný	k2eAgNnSc6d1	falešné
přistání	přistání	k1gNnSc6	přistání
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
veřejného	veřejný	k2eAgNnSc2d1	veřejné
mínění	mínění	k1gNnSc2	mínění
si	se	k3xPyFc3	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
šest	šest	k4xCc1	šest
procent	procento	k1gNnPc2	procento
Američanů	Američan	k1gMnPc2	Američan
myslelo	myslet	k5eAaImAgNnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
přistání	přistání	k1gNnSc4	přistání
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
byl	být	k5eAaImAgInS	být
podvrh	podvrh	k1gInSc1	podvrh
<g/>
.	.	kIx.	.
</s>
