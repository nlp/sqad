<s>
Sezonní	sezonní	k2eAgInPc1d1	sezonní
pohyby	pohyb	k1gInPc1	pohyb
nomádů	nomád	k1gMnPc2	nomád
nejsou	být	k5eNaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
migraci	migrace	k1gFnSc4	migrace
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jim	on	k3xPp3gMnPc3	on
chybí	chybit	k5eAaPmIp3nS	chybit
záměr	záměr	k1gInSc1	záměr
usadit	usadit	k5eAaPmF	usadit
se	se	k3xPyFc4	se
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
