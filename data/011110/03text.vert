<p>
<s>
Lidská	lidský	k2eAgFnSc1d1	lidská
migrace	migrace	k1gFnSc1	migrace
je	být	k5eAaImIp3nS	být
pohyb	pohyb	k1gInSc1	pohyb
lidí	člověk	k1gMnPc2	člověk
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
oblasti	oblast	k1gFnSc2	oblast
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
s	s	k7c7	s
úmyslem	úmysl	k1gInSc7	úmysl
se	se	k3xPyFc4	se
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
oblasti	oblast	k1gFnSc6	oblast
dočasně	dočasně	k6eAd1	dočasně
či	či	k8xC	či
trvale	trvale	k6eAd1	trvale
usadit	usadit	k5eAaPmF	usadit
<g/>
.	.	kIx.	.
</s>
<s>
Pohyb	pohyb	k1gInSc1	pohyb
lidí	člověk	k1gMnPc2	člověk
obvykle	obvykle	k6eAd1	obvykle
probíhá	probíhat	k5eAaImIp3nS	probíhat
na	na	k7c6	na
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
překračuje	překračovat	k5eAaImIp3nS	překračovat
hranice	hranice	k1gFnSc1	hranice
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
migrace	migrace	k1gFnSc1	migrace
je	být	k5eAaImIp3nS	být
také	také	k9	také
možná	možný	k2eAgFnSc1d1	možná
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
migrace	migrace	k1gFnSc2	migrace
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
jednotlivci	jednotlivec	k1gMnPc1	jednotlivec
<g/>
,	,	kIx,	,
rodiny	rodina	k1gFnPc1	rodina
anebo	anebo	k8xC	anebo
větší	veliký	k2eAgFnPc1d2	veliký
skupiny	skupina	k1gFnPc1	skupina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sezonní	sezonní	k2eAgInPc1d1	sezonní
pohyby	pohyb	k1gInPc1	pohyb
nomádů	nomád	k1gMnPc2	nomád
nejsou	být	k5eNaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
migraci	migrace	k1gFnSc4	migrace
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jim	on	k3xPp3gMnPc3	on
chybí	chybět	k5eAaImIp3nS	chybět
záměr	záměr	k1gInSc1	záměr
usadit	usadit	k5eAaPmF	usadit
se	se	k3xPyFc4	se
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
málo	málo	k4c4	málo
nomádských	nomádský	k2eAgNnPc2d1	nomádské
společenstev	společenstvo	k1gNnPc2	společenstvo
si	se	k3xPyFc3	se
udrželo	udržet	k5eAaPmAgNnS	udržet
svůj	svůj	k3xOyFgInSc4	svůj
způsob	způsob	k1gInSc4	způsob
života	život	k1gInSc2	život
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
migraci	migrace	k1gFnSc4	migrace
také	také	k9	také
nejsou	být	k5eNaImIp3nP	být
zahrnovány	zahrnován	k2eAgInPc1d1	zahrnován
dočasné	dočasný	k2eAgInPc1d1	dočasný
pohyby	pohyb	k1gInPc1	pohyb
lidí	člověk	k1gMnPc2	člověk
za	za	k7c4	za
účely	účel	k1gInPc4	účel
cestování	cestování	k1gNnSc2	cestování
<g/>
,	,	kIx,	,
turistiky	turistika	k1gFnSc2	turistika
<g/>
,	,	kIx,	,
náboženských	náboženský	k2eAgFnPc2d1	náboženská
poutí	pouť	k1gFnPc2	pouť
nebo	nebo	k8xC	nebo
dojíždění	dojíždění	k1gNnPc2	dojíždění
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
opět	opět	k6eAd1	opět
chybí	chybit	k5eAaPmIp3nS	chybit
záměr	záměr	k1gInSc1	záměr
se	se	k3xPyFc4	se
na	na	k7c6	na
novém	nový	k2eAgNnSc6d1	nové
místě	místo	k1gNnSc6	místo
usadit	usadit	k5eAaPmF	usadit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Demografie	demografie	k1gFnSc2	demografie
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
demografického	demografický	k2eAgNnSc2d1	demografické
hlediska	hledisko	k1gNnSc2	hledisko
lze	lze	k6eAd1	lze
migrace	migrace	k1gFnSc1	migrace
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
několika	několik	k4yIc2	několik
ukazateli	ukazatel	k1gInPc7	ukazatel
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
migrační	migrační	k2eAgNnSc1d1	migrační
saldo	saldo	k1gNnSc1	saldo
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
též	též	k9	též
"	"	kIx"	"
<g/>
čistá	čistý	k2eAgFnSc1d1	čistá
migrace	migrace	k1gFnSc1	migrace
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
rozdíl	rozdíl	k1gInSc1	rozdíl
počtu	počet	k1gInSc2	počet
přistěhovalých	přistěhovalý	k2eAgInPc2d1	přistěhovalý
(	(	kIx(	(
<g/>
imigrantů	imigrant	k1gMnPc2	imigrant
<g/>
)	)	kIx)	)
a	a	k8xC	a
vystěhovalých	vystěhovalý	k2eAgMnPc2d1	vystěhovalý
(	(	kIx(	(
<g/>
emigrantů	emigrant	k1gMnPc2	emigrant
<g/>
)	)	kIx)	)
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
výsledku	výsledek	k1gInSc6	výsledek
pak	pak	k6eAd1	pak
hovoříme	hovořit	k5eAaImIp1nP	hovořit
buď	buď	k8xC	buď
o	o	k7c6	o
migračním	migrační	k2eAgInSc6d1	migrační
růstu	růst	k1gInSc6	růst
<g/>
/	/	kIx~	/
<g/>
zisku	zisk	k1gInSc2	zisk
či	či	k8xC	či
o	o	k7c6	o
migračním	migrační	k2eAgInSc6d1	migrační
úbytku	úbytek	k1gInSc6	úbytek
<g/>
/	/	kIx~	/
<g/>
ztrátě	ztráta	k1gFnSc6	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
Vypočítá	vypočítat	k5eAaPmIp3nS	vypočítat
se	se	k3xPyFc4	se
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
M	M	kA	M
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
I	i	k9	i
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M	M	kA	M
<g/>
=	=	kIx~	=
<g/>
I-E	I-E	k1gMnSc1	I-E
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
kde	kde	k6eAd1	kde
M	M	kA	M
je	být	k5eAaImIp3nS	být
migrační	migrační	k2eAgNnSc1d1	migrační
saldo	saldo	k1gNnSc1	saldo
<g/>
,	,	kIx,	,
I	i	k9	i
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
přistěhovalých	přistěhovalý	k2eAgFnPc2d1	přistěhovalá
a	a	k8xC	a
E	E	kA	E
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc4	počet
vystěhovalých	vystěhovalý	k2eAgMnPc2d1	vystěhovalý
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
migrantů	migrant	k1gMnPc2	migrant
je	být	k5eAaImIp3nS	být
uváděn	uvádět	k5eAaImNgInS	uvádět
jako	jako	k8xS	jako
migrační	migrační	k2eAgInSc1d1	migrační
objem	objem	k1gInSc1	objem
či	či	k8xC	či
migrační	migrační	k2eAgInSc1d1	migrační
obrat	obrat	k1gInSc1	obrat
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
součtem	součet	k1gInSc7	součet
počtu	počet	k1gInSc2	počet
přistěhovalých	přistěhovalý	k2eAgMnPc2d1	přistěhovalý
a	a	k8xC	a
vystěhovalých	vystěhovalý	k2eAgMnPc2d1	vystěhovalý
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
stejných	stejný	k2eAgFnPc6d1	stejná
proměnných	proměnná	k1gFnPc6	proměnná
se	se	k3xPyFc4	se
vypočítá	vypočítat	k5eAaPmIp3nS	vypočítat
jako	jako	k9	jako
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
M	M	kA	M
</s>
</p>
<p>
</p>
<p>
<s>
o	o	k7c6	o
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
I	i	k9	i
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M_	M_	k1gMnPc7	M_
<g/>
{	{	kIx(	{
<g/>
o	o	k7c4	o
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
I	i	k9	i
<g/>
+	+	kIx~	+
<g/>
E	E	kA	E
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Podíl	podíl	k1gInSc1	podíl
migračního	migrační	k2eAgNnSc2d1	migrační
salda	saldo	k1gNnSc2	saldo
a	a	k8xC	a
migračního	migrační	k2eAgInSc2d1	migrační
objemu	objem	k1gInSc2	objem
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
index	index	k1gInSc1	index
migračního	migrační	k2eAgNnSc2d1	migrační
salda	saldo	k1gNnSc2	saldo
či	či	k8xC	či
migrační	migrační	k2eAgFnSc4d1	migrační
účinnost	účinnost	k1gFnSc4	účinnost
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
ukazatel	ukazatel	k1gInSc1	ukazatel
označován	označovat	k5eAaImNgInS	označovat
i	i	k8xC	i
jako	jako	k8xC	jako
index	index	k1gInSc1	index
atraktivity	atraktivita	k1gFnSc2	atraktivita
nebo	nebo	k8xC	nebo
index	index	k1gInSc1	index
efektivity	efektivita	k1gFnSc2	efektivita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
hodnot	hodnota	k1gFnPc2	hodnota
od	od	k7c2	od
-1,0	-1,0	k4	-1,0
do	do	k7c2	do
1,0	[number]	k4	1,0
<g/>
.	.	kIx.	.
</s>
<s>
Vypočítá	vypočítat	k5eAaPmIp3nS	vypočítat
se	se	k3xPyFc4	se
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
M	M	kA	M
</s>
</p>
<p>
</p>
<p>
<s>
I	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
I	i	k9	i
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
I	i	k9	i
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
100	[number]	k4	100
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rm	rm	k?	rm
{	{	kIx(	{
<g/>
M_	M_	k1gMnSc1	M_
<g/>
{	{	kIx(	{
<g/>
I	I	kA	I
<g/>
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
I-E	I-E	k1gFnSc1	I-E
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
I	i	k9	i
<g/>
+	+	kIx~	+
<g/>
E	E	kA	E
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
100	[number]	k4	100
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Imigrace	imigrace	k1gFnSc2	imigrace
===	===	k?	===
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
výraz	výraz	k1gInSc1	výraz
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
cíle	cíl	k1gInSc2	cíl
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
jak	jak	k8xS	jak
to	ten	k3xDgNnSc4	ten
vidíme	vidět	k5eAaImIp1nP	vidět
jako	jako	k8xS	jako
číslo	číslo	k1gNnSc1	číslo
jedna	jeden	k4xCgFnSc1	jeden
na	na	k7c6	na
ilustraci	ilustrace	k1gFnSc6	ilustrace
imigraci	imigrace	k1gFnSc6	imigrace
-	-	kIx~	-
přistěhování	přistěhování	k1gNnSc1	přistěhování
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
o	o	k7c4	o
imigraci	imigrace	k1gFnSc4	imigrace
mluvíme	mluvit	k5eAaImIp1nP	mluvit
<g/>
,	,	kIx,	,
když	když	k8xS	když
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
pohyb	pohyb	k1gInSc4	pohyb
přes	přes	k7c4	přes
hranice	hranice	k1gFnPc4	hranice
státu	stát	k1gInSc2	stát
či	či	k8xC	či
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
podobné	podobný	k2eAgFnPc4d1	podobná
či	či	k8xC	či
stejné	stejný	k2eAgFnPc4d1	stejná
stejné	stejný	k2eAgFnPc4d1	stejná
ekonomické	ekonomický	k2eAgFnPc4d1	ekonomická
podmínky	podmínka	k1gFnPc4	podmínka
či	či	k8xC	či
platí	platit	k5eAaImIp3nS	platit
podobné	podobný	k2eAgInPc4d1	podobný
či	či	k8xC	či
stejné	stejný	k2eAgInPc4d1	stejný
politické	politický	k2eAgInPc4d1	politický
či	či	k8xC	či
náboženské	náboženský	k2eAgInPc4d1	náboženský
zákony	zákon	k1gInPc4	zákon
do	do	k7c2	do
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
jsou	být	k5eAaImIp3nP	být
podmínky	podmínka	k1gFnPc4	podmínka
z	z	k7c2	z
některého	některý	k3yIgInSc2	některý
zmíněného	zmíněný	k2eAgInSc2d1	zmíněný
důvodu	důvod	k1gInSc2	důvod
výrazně	výrazně	k6eAd1	výrazně
výhodnější	výhodný	k2eAgFnSc1d2	výhodnější
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
můžeme	moct	k5eAaImIp1nP	moct
mluvit	mluvit	k5eAaImF	mluvit
o	o	k7c6	o
irské	irský	k2eAgFnSc6d1	irská
imigraci	imigrace	k1gFnSc6	imigrace
-	-	kIx~	-
připlutí	připlutí	k1gNnSc1	připlutí
lidí	člověk	k1gMnPc2	člověk
z	z	k7c2	z
Irska	Irsko	k1gNnSc2	Irsko
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
jako	jako	k8xC	jako
o	o	k7c6	o
lidech	člověk	k1gMnPc6	člověk
připluvších	připluvší	k2eAgInPc2d1	připluvší
z	z	k7c2	z
Irska	Irsko	k1gNnSc2	Irsko
<g/>
,	,	kIx,	,
o	o	k7c6	o
mexické	mexický	k2eAgFnSc6d1	mexická
imigraci	imigrace	k1gFnSc6	imigrace
-	-	kIx~	-
příchodu	příchod	k1gInSc6	příchod
lidí	člověk	k1gMnPc2	člověk
z	z	k7c2	z
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
jako	jako	k8xC	jako
o	o	k7c6	o
lidech	člověk	k1gMnPc6	člověk
přišedších	přišedší	k2eAgInPc2d1	přišedší
z	z	k7c2	z
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
skupinách	skupina	k1gFnPc6	skupina
lidí	člověk	k1gMnPc2	člověk
přišedších	přišedší	k2eAgMnPc2d1	přišedší
ze	z	k7c2	z
stejné	stejný	k2eAgFnSc2d1	stejná
země	zem	k1gFnSc2	zem
či	či	k8xC	či
části	část	k1gFnSc2	část
země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
mluví	mluvit	k5eAaImIp3nS	mluvit
jako	jako	k9	jako
o	o	k7c6	o
lidech	člověk	k1gMnPc6	člověk
nějakého	nějaký	k3yIgInSc2	nějaký
původu	původ	k1gInSc2	původ
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
na	na	k7c6	na
příkladu	příklad	k1gInSc6	příklad
opět	opět	k6eAd1	opět
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
můžeme	moct	k5eAaImIp1nP	moct
mluvit	mluvit	k5eAaImF	mluvit
o	o	k7c6	o
lidech	lido	k1gNnPc6	lido
irského	irský	k2eAgInSc2d1	irský
či	či	k8xC	či
mexického	mexický	k2eAgInSc2d1	mexický
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Emigrace	emigrace	k1gFnSc2	emigrace
===	===	k?	===
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
výraz	výraz	k1gInSc1	výraz
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
zdroje	zdroj	k1gInSc2	zdroj
pohybu	pohyb	k1gInSc2	pohyb
(	(	kIx(	(
<g/>
odkud	odkud	k6eAd1	odkud
vychází	vycházet	k5eAaImIp3nS	vycházet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
jak	jak	k8xS	jak
to	ten	k3xDgNnSc4	ten
vidíme	vidět	k5eAaImIp1nP	vidět
jako	jako	k8xC	jako
číslo	číslo	k1gNnSc4	číslo
dva	dva	k4xCgInPc1	dva
na	na	k7c6	na
ilustraci	ilustrace	k1gFnSc6	ilustrace
emigraci	emigrace	k1gFnSc4	emigrace
<g/>
,	,	kIx,	,
vystěhování	vystěhování	k1gNnSc4	vystěhování
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
emigraci	emigrace	k1gFnSc6	emigrace
mluvíme	mluvit	k5eAaImIp1nP	mluvit
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
prvek	prvek	k1gInSc1	prvek
vůle	vůle	k1gFnSc2	vůle
-	-	kIx~	-
když	když	k8xS	když
lidé	člověk	k1gMnPc1	člověk
mají	mít	k5eAaImIp3nP	mít
na	na	k7c4	na
vybranou	vybraný	k2eAgFnSc4d1	vybraná
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
akt	akt	k1gInSc4	akt
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
lidé	člověk	k1gMnPc1	člověk
nemohou	moct	k5eNaImIp3nP	moct
vybrat	vybrat	k5eAaPmF	vybrat
nebo	nebo	k8xC	nebo
by	by	kYmCp3nP	by
museli	muset	k5eAaImAgMnP	muset
změnit	změnit	k5eAaPmF	změnit
například	například	k6eAd1	například
náboženství	náboženství	k1gNnSc3	náboženství
<g/>
,	,	kIx,	,
mluvíme	mluvit	k5eAaImIp1nP	mluvit
o	o	k7c6	o
vyhnání	vyhnání	k1gNnSc6	vyhnání
a	a	k8xC	a
o	o	k7c6	o
exulantech	exulant	k1gMnPc6	exulant
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
souvislostech	souvislost	k1gFnPc6	souvislost
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
používá	používat	k5eAaImIp3nS	používat
termín	termín	k1gInSc1	termín
vyhnanci	vyhnanec	k1gMnPc1	vyhnanec
<g/>
.	.	kIx.	.
</s>
<s>
Příčiny	příčina	k1gFnPc1	příčina
vystěhování	vystěhování	k1gNnSc2	vystěhování
opět	opět	k6eAd1	opět
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
ekonomické	ekonomický	k2eAgFnPc1d1	ekonomická
<g/>
,	,	kIx,	,
politické	politický	k2eAgFnPc1d1	politická
nebo	nebo	k8xC	nebo
náboženské	náboženský	k2eAgFnPc1d1	náboženská
či	či	k8xC	či
kombinací	kombinace	k1gFnPc2	kombinace
několika	několik	k4yIc2	několik
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
příkladu	příklad	k1gInSc6	příklad
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
můžeme	moct	k5eAaImIp1nP	moct
mluvit	mluvit	k5eAaImF	mluvit
o	o	k7c6	o
československé	československý	k2eAgFnSc6d1	Československá
emigraci	emigrace	k1gFnSc6	emigrace
<g/>
,	,	kIx,	,
či	či	k8xC	či
několika	několik	k4yIc6	několik
československých	československý	k2eAgFnPc6d1	Československá
emigracích	emigrace	k1gFnPc6	emigrace
-	-	kIx~	-
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
máme	mít	k5eAaImIp1nP	mít
na	na	k7c6	na
mysli	mysl	k1gFnSc6	mysl
souhrn	souhrn	k1gInSc4	souhrn
či	či	k8xC	či
je	on	k3xPp3gMnPc4	on
chceme	chtít	k5eAaImIp1nP	chtít
členit	členit	k5eAaImF	členit
podle	podle	k7c2	podle
dat	datum	k1gNnPc2	datum
či	či	k8xC	či
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
před	před	k7c7	před
kterými	který	k3yRgInPc7	který
nebo	nebo	k8xC	nebo
po	po	k7c6	po
kterých	který	k3yQgInPc6	který
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgInP	konat
(	(	kIx(	(
<g/>
předválečnou	předválečný	k2eAgFnSc7d1	předválečná
<g/>
,	,	kIx,	,
poúnorovou	poúnorový	k2eAgFnSc7d1	poúnorová
<g/>
,	,	kIx,	,
posrpnovou	posrpnový	k2eAgFnSc7d1	posrpnová
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
můžeme	moct	k5eAaImIp1nP	moct
mít	mít	k5eAaImF	mít
na	na	k7c6	na
mysli	mysl	k1gFnSc6	mysl
samotný	samotný	k2eAgInSc1d1	samotný
odchod	odchod	k1gInSc1	odchod
(	(	kIx(	(
<g/>
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
pohyb	pohyb	k1gInSc1	pohyb
<g/>
)	)	kIx)	)
či	či	k8xC	či
lidi	člověk	k1gMnPc4	člověk
odešedší	odešedší	k2eAgFnSc3d1	odešedší
z	z	k7c2	z
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
se	se	k3xPyFc4	se
například	například	k6eAd1	například
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
polské	polský	k2eAgFnSc6d1	polská
<g/>
,	,	kIx,	,
o	o	k7c6	o
ruské	ruský	k2eAgFnSc6d1	ruská
či	či	k8xC	či
francouzské	francouzský	k2eAgFnSc6d1	francouzská
šlechtické	šlechtický	k2eAgFnSc6d1	šlechtická
emigraci	emigrace	k1gFnSc6	emigrace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Reemigrace	reemigrace	k1gFnSc2	reemigrace
===	===	k?	===
</s>
</p>
<p>
<s>
Třetí	třetí	k4xOgInSc1	třetí
případ	případ	k1gInSc1	případ
je	být	k5eAaImIp3nS	být
reemigrace	reemigrace	k1gFnSc1	reemigrace
<g/>
,	,	kIx,	,
pohyb	pohyb	k1gInSc1	pohyb
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
emigraci	emigrace	k1gFnSc4	emigrace
a	a	k8xC	a
zpětnou	zpětný	k2eAgFnSc4d1	zpětná
imigraci	imigrace	k1gFnSc4	imigrace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Migrace	migrace	k1gFnSc1	migrace
z	z	k7c2	z
oblastí	oblast	k1gFnPc2	oblast
konfliktů	konflikt	k1gInPc2	konflikt
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
konfliktů	konflikt	k1gInPc2	konflikt
<g/>
,	,	kIx,	,
společenských	společenský	k2eAgInPc2d1	společenský
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
politických	politický	k2eAgFnPc2d1	politická
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
ekonomických	ekonomický	k2eAgInPc2d1	ekonomický
rozdílů	rozdíl	k1gInPc2	rozdíl
migrace	migrace	k1gFnSc2	migrace
představují	představovat	k5eAaImIp3nP	představovat
závažný	závažný	k2eAgInSc4d1	závažný
socioekonomický	socioekonomický	k2eAgInSc4d1	socioekonomický
problém	problém	k1gInSc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Migrace	migrace	k1gFnPc1	migrace
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
dobrovolné	dobrovolný	k2eAgFnPc1d1	dobrovolná
<g/>
,	,	kIx,	,
za	za	k7c7	za
prací	práce	k1gFnSc7	práce
či	či	k8xC	či
za	za	k7c7	za
příbuznými	příbuzný	k1gMnPc7	příbuzný
<g/>
,	,	kIx,	,
či	či	k8xC	či
nucené	nucený	k2eAgNnSc1d1	nucené
(	(	kIx(	(
<g/>
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
příčinou	příčina	k1gFnSc7	příčina
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
silné	silný	k2eAgNnSc1d1	silné
zhoršení	zhoršení	k1gNnSc1	zhoršení
životních	životní	k2eAgFnPc2d1	životní
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
,	,	kIx,	,
válečný	válečný	k2eAgInSc4d1	válečný
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
diktatury	diktatura	k1gFnPc4	diktatura
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
nelze	lze	k6eNd1	lze
tyto	tento	k3xDgInPc4	tento
dva	dva	k4xCgInPc4	dva
druhy	druh	k1gInPc4	druh
jednoznačně	jednoznačně	k6eAd1	jednoznačně
odlišit	odlišit	k5eAaPmF	odlišit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
nejvíc	nejvíc	k6eAd1	nejvíc
emigrantů	emigrant	k1gMnPc2	emigrant
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Afghánistánu	Afghánistán	k1gInSc2	Afghánistán
(	(	kIx(	(
<g/>
4	[number]	k4	4
500	[number]	k4	500
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
a	a	k8xC	a
Palestiny	Palestina	k1gFnSc2	Palestina
(	(	kIx(	(
<g/>
4	[number]	k4	4
123	[number]	k4	123
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
jiném	jiný	k2eAgInSc6d1	jiný
státě	stát	k1gInSc6	stát
<g/>
,	,	kIx,	,
než	než	k8xS	než
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
narodili	narodit	k5eAaPmAgMnP	narodit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
150	[number]	k4	150
000	[number]	k4	000
000	[number]	k4	000
až	až	k9	až
185	[number]	k4	185
000	[number]	k4	000
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Toto	tento	k3xDgNnSc1	tento
číslo	číslo	k1gNnSc1	číslo
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
způsobu	způsob	k1gInSc6	způsob
výpočtu	výpočet	k1gInSc2	výpočet
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
všechno	všechen	k3xTgNnSc1	všechen
je	být	k5eAaImIp3nS	být
pod	pod	k7c4	pod
pojem	pojem	k1gInSc4	pojem
"	"	kIx"	"
<g/>
žijí	žít	k5eAaImIp3nP	žít
<g/>
"	"	kIx"	"
zahrnut	zahrnout	k5eAaPmNgInS	zahrnout
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
podle	podle	k7c2	podle
statistik	statistika	k1gFnPc2	statistika
USA	USA	kA	USA
stačí	stačit	k5eAaBmIp3nS	stačit
pobyt	pobyt	k1gInSc4	pobyt
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Před	před	k7c7	před
30	[number]	k4	30
lety	léto	k1gNnPc7	léto
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
číslo	číslo	k1gNnSc1	číslo
zhruba	zhruba	k6eAd1	zhruba
poloviční	poloviční	k2eAgMnSc1d1	poloviční
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnSc1	populace
některých	některý	k3yIgInPc2	některý
států	stát	k1gInPc2	stát
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
právě	právě	k9	právě
těmito	tento	k3xDgMnPc7	tento
migranty	migrant	k1gMnPc7	migrant
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Austrálie	Austrálie	k1gFnSc2	Austrálie
(	(	kIx(	(
<g/>
24	[number]	k4	24
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
(	(	kIx(	(
<g/>
17	[number]	k4	17
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Švédsko	Švédsko	k1gNnSc1	Švédsko
(	(	kIx(	(
<g/>
12	[number]	k4	12
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
USA	USA	kA	USA
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
10	[number]	k4	10
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
migrujícími	migrující	k2eAgMnPc7d1	migrující
lidmi	člověk	k1gMnPc7	člověk
výrazně	výrazně	k6eAd1	výrazně
převládají	převládat	k5eAaImIp3nP	převládat
migrace	migrace	k1gFnPc1	migrace
dobrovolné	dobrovolný	k2eAgFnPc1d1	dobrovolná
<g/>
.	.	kIx.	.
</s>
<s>
Uprchlíků	uprchlík	k1gMnPc2	uprchlík
a	a	k8xC	a
lidí	člověk	k1gMnPc2	člověk
žádajících	žádající	k2eAgMnPc2d1	žádající
o	o	k7c4	o
azyl	azyl	k1gInSc4	azyl
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
asi	asi	k9	asi
15	[number]	k4	15
milionů	milion	k4xCgInPc2	milion
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc4	tento
číslo	číslo	k1gNnSc4	číslo
proti	proti	k7c3	proti
stavu	stav	k1gInSc3	stav
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
mírně	mírně	k6eAd1	mírně
pokleslo	poklesnout	k5eAaPmAgNnS	poklesnout
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
16,3	[number]	k4	16,3
milionu	milion	k4xCgInSc2	milion
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
OSN	OSN	kA	OSN
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
počet	počet	k1gInSc4	počet
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
nežijí	žít	k5eNaImIp3nP	žít
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
svého	svůj	k3xOyFgNnSc2	svůj
narození	narození	k1gNnSc2	narození
<g/>
,	,	kIx,	,
překonal	překonat	k5eAaPmAgMnS	překonat
rekord	rekord	k1gInSc4	rekord
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
jejich	jejich	k3xOp3gNnPc2	jejich
dat	datum	k1gNnPc2	datum
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
244	[number]	k4	244
milionů	milion	k4xCgInPc2	milion
migrantů	migrant	k1gMnPc2	migrant
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
států	stát	k1gInPc2	stát
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
obtížné	obtížný	k2eAgNnSc1d1	obtížné
získat	získat	k5eAaPmF	získat
azyl	azyl	k1gInSc4	azyl
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jejich	jejich	k3xOp3gFnSc1	jejich
azylová	azylový	k2eAgFnSc1d1	azylová
politika	politika	k1gFnSc1	politika
je	být	k5eAaImIp3nS	být
postavena	postavit	k5eAaPmNgFnS	postavit
na	na	k7c6	na
velmi	velmi	k6eAd1	velmi
důkladném	důkladný	k2eAgNnSc6d1	důkladné
zkoumání	zkoumání	k1gNnSc6	zkoumání
důvodů	důvod	k1gInPc2	důvod
emigrace	emigrace	k1gFnSc2	emigrace
a	a	k8xC	a
úspěšnost	úspěšnost	k1gFnSc4	úspěšnost
žádostí	žádost	k1gFnPc2	žádost
o	o	k7c4	o
azyl	azyl	k1gInSc4	azyl
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
jednotkách	jednotka	k1gFnPc6	jednotka
procent	procento	k1gNnPc2	procento
<g/>
,	,	kIx,	,
především	především	k9	především
z	z	k7c2	z
politických	politický	k2eAgInPc2d1	politický
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
vyřizování	vyřizování	k1gNnSc4	vyřizování
žádostí	žádost	k1gFnPc2	žádost
žijí	žít	k5eAaImIp3nP	žít
lidé	člověk	k1gMnPc1	člověk
ve	v	k7c6	v
zvláštních	zvláštní	k2eAgNnPc6d1	zvláštní
zařízeních	zařízení	k1gNnPc6	zařízení
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
úroveň	úroveň	k1gFnSc1	úroveň
zde	zde	k6eAd1	zde
poskytovaných	poskytovaný	k2eAgFnPc2d1	poskytovaná
služeb	služba	k1gFnPc2	služba
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
vyspělých	vyspělý	k2eAgInPc2d1	vyspělý
států	stát	k1gInPc2	stát
včetně	včetně	k7c2	včetně
USA	USA	kA	USA
velice	velice	k6eAd1	velice
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c6	na
klesající	klesající	k2eAgFnSc6d1	klesající
populaci	populace	k1gFnSc6	populace
většiny	většina	k1gFnSc2	většina
vyspělých	vyspělý	k2eAgInPc2d1	vyspělý
států	stát	k1gInPc2	stát
se	se	k3xPyFc4	se
naskýtá	naskýtat	k5eAaImIp3nS	naskýtat
otázka	otázka	k1gFnSc1	otázka
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
řízená	řízený	k2eAgFnSc1d1	řízená
migrace	migrace	k1gFnSc1	migrace
zejména	zejména	k9	zejména
vzdělanějších	vzdělaný	k2eAgFnPc2d2	vzdělanější
vrstev	vrstva	k1gFnPc2	vrstva
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
není	být	k5eNaImIp3nS	být
řešením	řešení	k1gNnSc7	řešení
hrozícího	hrozící	k2eAgNnSc2d1	hrozící
stárnutí	stárnutí	k1gNnSc2	stárnutí
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
zde	zde	k6eAd1	zde
ovšem	ovšem	k9	ovšem
rozdíly	rozdíl	k1gInPc1	rozdíl
kulturní	kulturní	k2eAgInPc1d1	kulturní
a	a	k8xC	a
náboženské	náboženský	k2eAgInPc1d1	náboženský
a	a	k8xC	a
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
případů	případ	k1gInPc2	případ
též	tenž	k3xDgFnSc2	tenž
xenofobní	xenofobní	k2eAgFnSc2d1	xenofobní
nálady	nálada	k1gFnSc2	nálada
domácího	domácí	k2eAgNnSc2d1	domácí
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
obávajícího	obávající	k2eAgNnSc2d1	obávající
se	s	k7c7	s
ztráty	ztráta	k1gFnPc4	ztráta
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
a	a	k8xC	a
nárůstu	nárůst	k1gInSc2	nárůst
kriminality	kriminalita	k1gFnSc2	kriminalita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Uprchlík	uprchlík	k1gMnSc1	uprchlík
</s>
</p>
<p>
<s>
Environmentální	environmentální	k2eAgFnSc1d1	environmentální
migrace	migrace	k1gFnSc1	migrace
</s>
</p>
<p>
<s>
Emigrace	emigrace	k1gFnSc1	emigrace
</s>
</p>
<p>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
horečka	horečka	k1gFnSc1	horečka
</s>
</p>
<p>
<s>
Otrokářství	otrokářství	k1gNnSc1	otrokářství
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
příklad	příklad	k1gInSc1	příklad
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
nucené	nucený	k2eAgFnSc2d1	nucená
migrace	migrace	k1gFnSc2	migrace
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
migrace	migrace	k1gFnSc2	migrace
obyvatel	obyvatel	k1gMnPc2	obyvatel
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
International	Internationat	k5eAaPmAgInS	Internationat
Migration	Migration	k1gInSc1	Migration
Statistics	Statistics	k1gInSc1	Statistics
</s>
</p>
<p>
<s>
Migraceonline	Migraceonlin	k1gInSc5	Migraceonlin
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
web	web	k1gInSc1	web
o	o	k7c4	o
migraci	migrace	k1gFnSc4	migrace
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
</s>
</p>
<p>
<s>
Migration	Migration	k1gInSc1	Migration
<g/>
4	[number]	k4	4
<g/>
media	medium	k1gNnSc2	medium
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
-	-	kIx~	-
web	web	k1gInSc1	web
s	s	k7c7	s
informacemi	informace	k1gFnPc7	informace
o	o	k7c4	o
migraci	migrace	k1gFnSc4	migrace
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
</s>
</p>
