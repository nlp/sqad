<s>
Důl	důl	k1gInSc1
je	být	k5eAaImIp3nS
závod	závod	k1gInSc4
či	či	k8xC
prostor	prostor	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
těží	těžet	k5eAaImIp3nP
nerostné	nerostný	k2eAgFnPc1d1
suroviny	surovina	k1gFnPc1
<g/>
,	,	kIx,
například	například	k6eAd1
uhlí	uhlí	k1gNnSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
rudy	ruda	k1gFnPc1
<g/>
.	.	kIx.
</s>