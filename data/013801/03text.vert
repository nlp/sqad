<s>
Důl	důl	k1gInSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
těžebním	těžební	k2eAgInSc6d1
prostoru	prostor	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Důl	důl	k1gInSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Pohled	pohled	k1gInSc1
na	na	k7c4
dnes	dnes	k6eAd1
již	již	k6eAd1
nepoužívaný	používaný	k2eNgInSc4d1
Ševčinský	Ševčinský	k2eAgInSc4d1
rudný	rudný	k2eAgInSc4d1
důl	důl	k1gInSc4
na	na	k7c6
Březových	březový	k2eAgFnPc6d1
Horách	hora	k1gFnPc6
v	v	k7c6
Příbrami	Příbram	k1gFnSc6
</s>
<s>
Důl	důl	k1gInSc1
je	být	k5eAaImIp3nS
závod	závod	k1gInSc4
či	či	k8xC
prostor	prostor	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
těží	těžet	k5eAaImIp3nP
nerostné	nerostný	k2eAgFnPc1d1
suroviny	surovina	k1gFnPc1
<g/>
,	,	kIx,
například	například	k6eAd1
uhlí	uhlí	k1gNnSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
rudy	ruda	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
je	být	k5eAaImIp3nS
těžen	těžen	k2eAgInSc4d1
kámen	kámen	k1gInSc4
<g/>
,	,	kIx,
mluvíme	mluvit	k5eAaImIp1nP
o	o	k7c6
lomu	lom	k1gInSc6
<g/>
,	,	kIx,
případně	případně	k6eAd1
kamenolomu	kamenolom	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Druhy	druh	k1gInPc1
dolů	dol	k1gInPc2
</s>
<s>
povrchový	povrchový	k2eAgInSc1d1
důl	důl	k1gInSc1
–	–	k?
těží	těžet	k5eAaImIp3nS
se	se	k3xPyFc4
odkrýváním	odkrývání	k1gNnSc7
jednotlivých	jednotlivý	k2eAgFnPc2d1
vrstev	vrstva	k1gFnPc2
zeminy	zemina	k1gFnSc2
z	z	k7c2
povrchu	povrch	k1gInSc2
(	(	kIx(
<g/>
například	například	k6eAd1
těžba	těžba	k1gFnSc1
hnědého	hnědý	k2eAgNnSc2d1
uhlí	uhlí	k1gNnSc2
v	v	k7c6
severních	severní	k2eAgFnPc6d1
Čechách	Čechy	k1gFnPc6
<g/>
)	)	kIx)
</s>
<s>
hlubinný	hlubinný	k2eAgInSc1d1
důl	důl	k1gInSc1
–	–	k?
těží	těžet	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
podzemním	podzemní	k2eAgInSc6d1
systému	systém	k1gInSc6
svislých	svislý	k2eAgFnPc2d1
šachet	šachta	k1gFnPc2
a	a	k8xC
vodorovných	vodorovný	k2eAgFnPc2d1
štol	štola	k1gFnPc2
</s>
<s>
tímto	tento	k3xDgInSc7
způsobem	způsob	k1gInSc7
se	se	k3xPyFc4
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
těží	těžet	k5eAaImIp3nS
například	například	k6eAd1
černé	černý	k2eAgNnSc1d1
uhlí	uhlí	k1gNnSc1
na	na	k7c6
Karvinsku	Karvinsko	k1gNnSc6
(	(	kIx(
<g/>
na	na	k7c6
Ostravsku	Ostravsko	k1gNnSc6
byla	být	k5eAaImAgFnS
těžba	těžba	k1gFnSc1
uhlí	uhlí	k1gNnSc2
ukončena	ukončen	k2eAgFnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
existuje	existovat	k5eAaImIp3nS
ale	ale	k9
i	i	k9
řada	řada	k1gFnSc1
rudných	rudný	k2eAgInPc2d1
dolů	dol	k1gInPc2
pracujících	pracující	k2eAgInPc2d1
na	na	k7c6
tomto	tento	k3xDgInSc6
principu	princip	k1gInSc6
<g/>
,	,	kIx,
např.	např.	kA
rudné	rudný	k2eAgInPc1d1
doly	dol	k1gInPc1
v	v	k7c6
okolí	okolí	k1gNnSc6
Příbrami	Příbram	k1gFnSc2
</s>
<s>
Alternativní	alternativní	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP
se	se	k3xPyFc4
i	i	k9
alternativní	alternativní	k2eAgInPc4d1
synonymické	synonymický	k2eAgInPc4d1
názvy	název	k1gInPc4
pro	pro	k7c4
důl	důl	k1gInSc4
<g/>
:	:	kIx,
</s>
<s>
šachta	šachta	k1gFnSc1
</s>
<s>
jáma	jáma	k1gFnSc1
</s>
<s>
štola	štola	k1gFnSc1
</s>
<s>
Kombinovaná	kombinovaný	k2eAgFnSc1d1
těžba	těžba	k1gFnSc1
</s>
<s>
Existují	existovat	k5eAaImIp3nP
i	i	k9
kombinované	kombinovaný	k2eAgFnPc1d1
metody	metoda	k1gFnPc1
těžby	těžba	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
těženo	těžit	k5eAaImNgNnS
svrchu	svrchu	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
vytěžená	vytěžený	k2eAgFnSc1d1
hornina	hornina	k1gFnSc1
se	se	k3xPyFc4
odváží	odvážet	k5eAaImIp3nS,k5eAaPmIp3nS
vodorovnou	vodorovný	k2eAgFnSc7d1
štolou	štola	k1gFnSc7
pod	pod	k7c7
povrchem	povrch	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgInSc7
způsobem	způsob	k1gInSc7
se	se	k3xPyFc4
například	například	k6eAd1
v	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
těžil	těžit	k5eAaImAgMnS
vápenec	vápenec	k1gInSc4
v	v	k7c6
Českém	český	k2eAgInSc6d1
krasu	kras	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
severních	severní	k2eAgInPc6d1
a	a	k8xC
v	v	k7c6
severozápadních	severozápadní	k2eAgFnPc6d1
Čechách	Čechy	k1gFnPc6
převažovaly	převažovat	k5eAaImAgInP
v	v	k7c6
prvopočátku	prvopočátek	k1gInSc6
těžby	těžba	k1gFnSc2
uhlí	uhlí	k1gNnSc2
také	také	k9
hlubinné	hlubinný	k2eAgInPc4d1
doly	dol	k1gInPc4
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
technický	technický	k2eAgInSc1d1
rozvoj	rozvoj	k1gInSc1
ve	v	k7c6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
postupně	postupně	k6eAd1
umožnil	umožnit	k5eAaPmAgInS
přejít	přejít	k5eAaPmF
na	na	k7c4
povrchové	povrchový	k2eAgFnPc4d1
metody	metoda	k1gFnPc4
těžby	těžba	k1gFnSc2
prakticky	prakticky	k6eAd1
ve	v	k7c6
všech	všecek	k3xTgFnPc6
částech	část	k1gFnPc6
obou	dva	k4xCgMnPc2
našich	naši	k1gMnPc2
hnědouhelných	hnědouhelný	k2eAgInPc2d1
revírů	revír	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
lom	lom	k1gInSc1
</s>
<s>
pískovna	pískovna	k1gFnSc1
</s>
<s>
těžní	těžní	k2eAgFnSc1d1
věž	věž	k1gFnSc1
</s>
<s>
těžba	těžba	k1gFnSc1
uranu	uran	k1gInSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
důl	důl	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
důl	důl	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4122070-5	4122070-5	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
11459	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Těžba	těžba	k1gFnSc1
</s>
