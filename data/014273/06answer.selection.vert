<s>
Stejnosměrný	stejnosměrný	k2eAgInSc1d1
elektrický	elektrický	k2eAgInSc1d1
proud	proud	k1gInSc1
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
označovaný	označovaný	k2eAgInSc1d1
ss	ss	k?
nebo	nebo	k8xC
DC	DC	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
direct	direct	k2eAgInSc1d1
current	current	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
elektrický	elektrický	k2eAgInSc4d1
proud	proud	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
protéká	protékat	k5eAaImIp3nS
obvodem	obvod	k1gInSc7
stále	stále	k6eAd1
stejným	stejný	k2eAgInSc7d1
směrem	směr	k1gInSc7
<g/>
,	,	kIx,
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
proudu	proud	k1gInSc2
střídavého	střídavý	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>