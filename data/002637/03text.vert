<s>
Kontrafakt	Kontrafakt	k1gInSc1	Kontrafakt
je	být	k5eAaImIp3nS	být
slovenské	slovenský	k2eAgNnSc4d1	slovenské
hiphopové	hiphopový	k2eAgNnSc4d1	hiphopové
uskupení	uskupení	k1gNnSc4	uskupení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
členy	člen	k1gInPc1	člen
jsou	být	k5eAaImIp3nP	být
Rytmus	rytmus	k1gInSc4	rytmus
(	(	kIx(	(
<g/>
Patrik	Patrik	k1gMnSc1	Patrik
Vrbovský	Vrbovský	k1gMnSc1	Vrbovský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ego	ego	k1gNnSc1	ego
(	(	kIx(	(
<g/>
Michal	Michal	k1gMnSc1	Michal
Straka	Straka	k1gMnSc1	Straka
<g/>
)	)	kIx)	)
a	a	k8xC	a
DJ	DJ	kA	DJ
Anys	Anys	k1gInSc1	Anys
(	(	kIx(	(
<g/>
Ondrej	Ondrej	k1gFnSc1	Ondrej
Beňačka	Beňačka	k1gFnSc1	Beňačka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Koncertují	koncertovat	k5eAaImIp3nP	koncertovat
nejen	nejen	k6eAd1	nejen
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
texty	text	k1gInPc1	text
jsou	být	k5eAaImIp3nP	být
prosyceny	prosycen	k2eAgInPc1d1	prosycen
expresivními	expresivní	k2eAgInPc7d1	expresivní
výrazy	výraz	k1gInPc7	výraz
<g/>
,	,	kIx,	,
nezřídka	nezřídka	k6eAd1	nezřídka
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
i	i	k9	i
hrubé	hrubý	k2eAgInPc1d1	hrubý
vulgarismy	vulgarismus	k1gInPc1	vulgarismus
<g/>
.	.	kIx.	.
</s>
<s>
Dáva	Dáva	k6eAd1	Dáva
mi	já	k3xPp1nSc3	já
<g/>
/	/	kIx~	/
<g/>
Dobré	dobrý	k2eAgNnSc1d1	dobré
veci	veci	k1gNnSc1	veci
<g/>
/	/	kIx~	/
<g/>
Moja	Moja	k?	Moja
rola	rola	k1gFnSc1	rola
12	[number]	k4	12
<g/>
"	"	kIx"	"
singl	singl	k1gInSc1	singl
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Tri	Tri	k1gFnSc1	Tri
špinavé	špinavý	k2eAgNnSc1d1	špinavé
mená	menat	k5eAaBmIp3nS	menat
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
E.	E.	kA	E.
<g/>
R.	R.	kA	R.
<g/>
A.	A.	kA	A.
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Sony	Sony	kA	Sony
BMG	BMG	kA	BMG
<g/>
)	)	kIx)	)
Murdardo	Murdardo	k1gNnSc1	Murdardo
Mulano	Mulana	k1gFnSc5	Mulana
Tour	Tour	k1gInSc1	Tour
2005	[number]	k4	2005
(	(	kIx(	(
<g/>
Sony	Sony	kA	Sony
BMG	BMG	kA	BMG
<g/>
)	)	kIx)	)
Bozk	Bozk	k1gMnSc1	Bozk
na	na	k7c6	na
rozlúčku	rozlúček	k1gInSc6	rozlúček
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Kontrafakt	Kontrafakt	k1gInSc1	Kontrafakt
Navždy	navždy	k6eAd1	navždy
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Biografie	biografie	k1gFnSc1	biografie
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Diskografie	diskografie	k1gFnSc1	diskografie
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
Rozhovor	rozhovor	k1gInSc1	rozhovor
s	s	k7c7	s
Rytmusem	Rytmus	k1gInSc7	Rytmus
</s>
