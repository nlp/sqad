<p>
<s>
Limone	limon	k1gInSc5	limon
sul	sout	k5eAaImAgInS	sout
Garda	garda	k1gFnSc1	garda
je	být	k5eAaImIp3nS	být
obec	obec	k1gFnSc1	obec
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Brescia	Brescium	k1gNnSc2	Brescium
v	v	k7c6	v
italském	italský	k2eAgInSc6d1	italský
regionu	region	k1gInSc6	region
Lombardie	Lombardie	k1gFnSc2	Lombardie
na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
břehu	břeh	k1gInSc6	břeh
Gardského	Gardský	k2eAgNnSc2d1	Gardské
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Navzdory	navzdory	k7c3	navzdory
současnému	současný	k2eAgNnSc3d1	současné
slavnému	slavný	k2eAgNnSc3d1	slavné
pěstování	pěstování	k1gNnSc3	pěstování
citrónů	citrón	k1gInPc2	citrón
(	(	kIx(	(
<g/>
limone	limon	k1gInSc5	limon
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
italštině	italština	k1gFnSc6	italština
citrón	citrón	k1gInSc1	citrón
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
název	název	k1gInSc1	název
městečka	městečko	k1gNnSc2	městečko
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
starobylého	starobylý	k2eAgNnSc2d1	starobylé
lemos	lemosa	k1gFnPc2	lemosa
(	(	kIx(	(
<g/>
elm	elm	k?	elm
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
limes	limes	k1gInSc1	limes
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
v	v	k7c6	v
latině	latina	k1gFnSc6	latina
znamená	znamenat	k5eAaImIp3nS	znamenat
hranici	hranice	k1gFnSc4	hranice
–	–	k?	–
mělo	mít	k5eAaImAgNnS	mít
to	ten	k3xDgNnSc1	ten
odkazovat	odkazovat	k5eAaImF	odkazovat
na	na	k7c4	na
město	město	k1gNnSc4	město
Brescia	Brescium	k1gNnSc2	Brescium
a	a	k8xC	a
Trentské	Trentský	k2eAgNnSc4d1	Trentský
biskupství	biskupství	k1gNnSc4	biskupství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1863	[number]	k4	1863
<g/>
–	–	k?	–
<g/>
1905	[number]	k4	1905
název	název	k1gInSc1	název
zněl	znět	k5eAaImAgInS	znět
Limone	limon	k1gInSc5	limon
San	San	k1gFnPc1	San
Giovanni	Giovann	k1gMnPc1	Giovann
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
bylo	být	k5eAaImAgNnS	být
městečko	městečko	k1gNnSc1	městečko
přístupné	přístupný	k2eAgNnSc1d1	přístupné
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
jezera	jezero	k1gNnSc2	jezero
nebo	nebo	k8xC	nebo
horskými	horský	k2eAgFnPc7d1	horská
cestami	cesta	k1gFnPc7	cesta
<g/>
,	,	kIx,	,
silnice	silnice	k1gFnPc1	silnice
do	do	k7c2	do
Riva	Riv	k1gInSc2	Riv
del	del	k?	del
Garda	garda	k1gFnSc1	garda
byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
však	však	k9	však
Limone	limon	k1gInSc5	limon
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgNnPc2d3	nejznámější
turistických	turistický	k2eAgNnPc2d1	turistické
letovisek	letovisko	k1gNnPc2	letovisko
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Gardského	Gardský	k2eAgNnSc2d1	Gardské
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zdraví	zdraví	k1gNnSc1	zdraví
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
vědci	vědec	k1gMnPc1	vědec
objevili	objevit	k5eAaPmAgMnP	objevit
<g/>
,	,	kIx,	,
že	že	k8xS	že
řada	řada	k1gFnSc1	řada
obyvatel	obyvatel	k1gMnPc2	obyvatel
Limone	limon	k1gInSc5	limon
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
přítomnou	přítomný	k2eAgFnSc4d1	přítomná
zmutovanou	zmutovaný	k2eAgFnSc4d1	zmutovaná
formu	forma	k1gFnSc4	forma
apolipoproteinu	apolipoprotein	k1gInSc2	apolipoprotein
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
zvanou	zvaný	k2eAgFnSc4d1	zvaná
ApoA-	ApoA-	k1gFnSc4	ApoA-
<g/>
1	[number]	k4	1
Milano	Milana	k1gFnSc5	Milana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
zdravou	zdravý	k2eAgFnSc4d1	zdravá
formu	forma	k1gFnSc4	forma
vysokodenzitního	vysokodenzitní	k2eAgInSc2d1	vysokodenzitní
lipoproteinu	lipoprotein	k1gInSc2	lipoprotein
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
znamená	znamenat	k5eAaImIp3nS	znamenat
nižší	nízký	k2eAgNnSc4d2	nižší
riziko	riziko	k1gNnSc4	riziko
aterosklerózy	ateroskleróza	k1gFnSc2	ateroskleróza
a	a	k8xC	a
ostatních	ostatní	k2eAgNnPc2d1	ostatní
kardiovaskulárních	kardiovaskulární	k2eAgNnPc2d1	kardiovaskulární
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
bílkovina	bílkovina	k1gFnSc1	bílkovina
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
častá	častý	k2eAgFnSc1d1	častá
dlouhověkost	dlouhověkost	k1gFnSc1	dlouhověkost
–	–	k?	–
dvanácti	dvanáct	k4xCc2	dvanáct
místním	místní	k2eAgNnSc6d1	místní
obyvatelů	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
přes	přes	k7c4	přes
100	[number]	k4	100
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
z	z	k7c2	z
cca	cca	kA	cca
1000	[number]	k4	1000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
obyvatelé	obyvatel	k1gMnPc1	obyvatel
města	město	k1gNnSc2	město
podstoupili	podstoupit	k5eAaPmAgMnP	podstoupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
krevní	krevní	k2eAgInPc1d1	krevní
testy	test	k1gInPc1	test
a	a	k8xC	a
výsledky	výsledek	k1gInPc1	výsledek
byly	být	k5eAaImAgInP	být
překvapivé	překvapivý	k2eAgInPc1d1	překvapivý
<g/>
:	:	kIx,	:
mnozí	mnohý	k2eAgMnPc1d1	mnohý
obyvatelé	obyvatel	k1gMnPc1	obyvatel
nesli	nést	k5eAaImAgMnP	nést
tento	tento	k3xDgInSc4	tento
gen.	gen.	kA	gen.
Výzkumníci	výzkumník	k1gMnPc1	výzkumník
obnovili	obnovit	k5eAaPmAgMnP	obnovit
rodokmen	rodokmen	k1gInSc4	rodokmen
nosičů	nosič	k1gInPc2	nosič
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
městským	městský	k2eAgInPc3d1	městský
a	a	k8xC	a
církevním	církevní	k2eAgInPc3d1	církevní
záznamům	záznam	k1gInPc3	záznam
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
všichni	všechen	k3xTgMnPc1	všechen
nositelé	nositel	k1gMnPc1	nositel
tohoto	tento	k3xDgInSc2	tento
genu	gen	k1gInSc2	gen
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
dvojice	dvojice	k1gFnSc2	dvojice
Cristoforo	Cristofora	k1gFnSc5	Cristofora
Pomaroli	Pomarole	k1gFnSc6	Pomarole
a	a	k8xC	a
Rosa	Rosa	k1gMnSc1	Rosa
Giovanelli	Giovanell	k1gMnPc1	Giovanell
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
vzali	vzít	k5eAaPmAgMnP	vzít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1644	[number]	k4	1644
<g/>
,	,	kIx,	,
v	v	k7c4	v
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
gen	gen	k1gInSc1	gen
mohl	moct	k5eAaImAgInS	moct
poprvé	poprvé	k6eAd1	poprvé
objevit	objevit	k5eAaPmF	objevit
<g/>
.	.	kIx.	.
</s>
<s>
Gen	gen	k1gInSc1	gen
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
manželstvích	manželství	k1gNnPc6	manželství
mezi	mezi	k7c7	mezi
pokrevními	pokrevní	k2eAgFnPc7d1	pokrevní
příbuznými	příbuzná	k1gFnPc7	příbuzná
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
často	často	k6eAd1	často
vyskytovalo	vyskytovat	k5eAaImAgNnS	vyskytovat
v	v	k7c6	v
Limone	limon	k1gInSc5	limon
až	až	k9	až
do	do	k7c2	do
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
silnice	silnice	k1gFnSc1	silnice
Gardesana	Gardesana	k1gFnSc1	Gardesana
slavnostně	slavnostně	k6eAd1	slavnostně
otevřena	otevřít	k5eAaPmNgFnS	otevřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobnosti	osobnost	k1gFnPc4	osobnost
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
Limone	limon	k1gInSc5	limon
==	==	k?	==
</s>
</p>
<p>
<s>
Ettore	Ettor	k1gMnSc5	Ettor
Colombo	Colomba	k1gMnSc5	Colomba
(	(	kIx(	(
<g/>
1831	[number]	k4	1831
<g/>
–	–	k?	–
<g/>
1881	[number]	k4	1881
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podnikatel	podnikatel	k1gMnSc1	podnikatel
</s>
</p>
<p>
<s>
Giuseppe	Giuseppat	k5eAaPmIp3nS	Giuseppat
Leonardi	Leonard	k1gMnPc1	Leonard
(	(	kIx(	(
<g/>
1840	[number]	k4	1840
<g/>
–	–	k?	–
<g/>
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vlastenec	vlastenec	k1gMnSc1	vlastenec
<g/>
,	,	kIx,	,
dobrovolník	dobrovolník	k1gMnSc1	dobrovolník
Expedice	expedice	k1gFnSc2	expedice
tisíce	tisíc	k4xCgInPc1	tisíc
<g/>
,	,	kIx,	,
tridentský	tridentský	k2eAgMnSc1d1	tridentský
iredentista	iredentista	k1gMnSc1	iredentista
</s>
</p>
<p>
<s>
Daniele	Daniela	k1gFnSc3	Daniela
Comboni	Comboň	k1gFnSc3	Comboň
(	(	kIx(	(
<g/>
1831	[number]	k4	1831
<g/>
–	–	k?	–
<g/>
1881	[number]	k4	1881
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
misionář	misionář	k1gMnSc1	misionář
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
katolický	katolický	k2eAgMnSc1d1	katolický
biskup	biskup	k1gMnSc1	biskup
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
světec	světec	k1gMnSc1	světec
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Limone	limon	k1gInSc5	limon
sul	sout	k5eAaImAgInS	sout
Garda	garda	k1gFnSc1	garda
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
Limone	limon	k1gInSc5	limon
sul	sout	k5eAaImAgInS	sout
Garda	garda	k1gFnSc1	garda
na	na	k7c6	na
italské	italský	k2eAgFnSc6d1	italská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Limone	limon	k1gInSc5	limon
sul	sout	k5eAaImAgInS	sout
Garda	garda	k1gFnSc1	garda
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Neoficiální	neoficiální	k2eAgFnPc4d1	neoficiální
stránky	stránka	k1gFnPc4	stránka
(	(	kIx(	(
<g/>
ubytování	ubytování	k1gNnSc4	ubytování
v	v	k7c6	v
Limone	limon	k1gInSc5	limon
<g/>
)	)	kIx)	)
</s>
</p>
