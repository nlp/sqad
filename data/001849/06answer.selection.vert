<s>
Roku	rok	k1gInSc2	rok
1872	[number]	k4	1872
vydal	vydat	k5eAaPmAgInS	vydat
Nietzsche	Nietzsch	k1gInSc2	Nietzsch
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
větší	veliký	k2eAgFnSc4d2	veliký
práci	práce	k1gFnSc4	práce
Zrození	zrození	k1gNnSc2	zrození
tragédie	tragédie	k1gFnSc2	tragédie
z	z	k7c2	z
ducha	duch	k1gMnSc2	duch
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
exaktní	exaktní	k2eAgFnSc2d1	exaktní
filologické	filologický	k2eAgFnSc2d1	filologická
metody	metoda	k1gFnSc2	metoda
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
filosofická	filosofický	k2eAgFnSc1d1	filosofická
spekulace	spekulace	k1gFnSc1	spekulace
<g/>
.	.	kIx.	.
</s>
