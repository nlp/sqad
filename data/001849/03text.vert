<s>
Friedrich	Friedrich	k1gMnSc1	Friedrich
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Nietzsche	Nietzsche	k1gFnSc1	Nietzsche
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1844	[number]	k4	1844
Röcken	Röckna	k1gFnPc2	Röckna
u	u	k7c2	u
Lützenu	Lützen	k2eAgFnSc4d1	Lützen
poblíž	poblíž	k7c2	poblíž
Lipska	Lipsko	k1gNnSc2	Lipsko
-	-	kIx~	-
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1900	[number]	k4	1900
Výmar	Výmar	k1gInSc1	Výmar
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
filosof	filosof	k1gMnSc1	filosof
<g/>
,	,	kIx,	,
klasický	klasický	k2eAgMnSc1d1	klasický
filolog	filolog	k1gMnSc1	filolog
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
skladatel	skladatel	k1gMnSc1	skladatel
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
jeho	jeho	k3xOp3gFnSc1	jeho
poezie	poezie	k1gFnSc1	poezie
a	a	k8xC	a
hudební	hudební	k2eAgFnPc1d1	hudební
kompozice	kompozice	k1gFnPc1	kompozice
byly	být	k5eAaImAgFnP	být
zastíněny	zastínit	k5eAaPmNgFnP	zastínit
jeho	jeho	k3xOp3gInPc7	jeho
pozdějšími	pozdní	k2eAgInPc7d2	pozdější
filosofickými	filosofický	k2eAgInPc7d1	filosofický
spisy	spis	k1gInPc7	spis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
výborný	výborný	k2eAgMnSc1d1	výborný
student	student	k1gMnSc1	student
se	se	k3xPyFc4	se
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1869	[number]	k4	1869
stal	stát	k5eAaPmAgMnS	stát
profesorem	profesor	k1gMnSc7	profesor
klasické	klasický	k2eAgFnSc2d1	klasická
filologie	filologie	k1gFnSc2	filologie
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
roku	rok	k1gInSc2	rok
1879	[number]	k4	1879
musel	muset	k5eAaImAgMnS	muset
ze	z	k7c2	z
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
důvodů	důvod	k1gInPc2	důvod
školu	škola	k1gFnSc4	škola
opustit	opustit	k5eAaPmF	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Následujících	následující	k2eAgNnPc2d1	následující
10	[number]	k4	10
let	léto	k1gNnPc2	léto
strávil	strávit	k5eAaPmAgMnS	strávit
na	na	k7c6	na
cestách	cesta	k1gFnPc6	cesta
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
napsal	napsat	k5eAaBmAgMnS	napsat
většinu	většina	k1gFnSc4	většina
svých	svůj	k3xOyFgNnPc2	svůj
velkých	velký	k2eAgNnPc2d1	velké
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1889	[number]	k4	1889
v	v	k7c6	v
Turíně	Turín	k1gInSc6	Turín
se	se	k3xPyFc4	se
nervově	nervově	k6eAd1	nervově
zhroutil	zhroutit	k5eAaPmAgInS	zhroutit
a	a	k8xC	a
zbytek	zbytek	k1gInSc1	zbytek
života	život	k1gInSc2	život
strávil	strávit	k5eAaPmAgInS	strávit
v	v	k7c6	v
polovědomém	polovědomý	k2eAgInSc6d1	polovědomý
stavu	stav	k1gInSc6	stav
jako	jako	k8xC	jako
duševně	duševně	k6eAd1	duševně
nemocný	nemocný	k2eAgMnSc1d1	nemocný
(	(	kIx(	(
<g/>
progresivní	progresivní	k2eAgFnSc1d1	progresivní
paralýza	paralýza	k1gFnSc1	paralýza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nietzsche	Nietzsche	k1gInSc1	Nietzsche
byl	být	k5eAaImAgInS	být
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
mladohegelovci	mladohegelovec	k1gMnSc3	mladohegelovec
(	(	kIx(	(
<g/>
Ludwig	Ludwig	k1gMnSc1	Ludwig
Feuerbach	Feuerbach	k1gMnSc1	Feuerbach
<g/>
,	,	kIx,	,
Bruno	Bruno	k1gMnSc1	Bruno
Bauer	Bauer	k1gMnSc1	Bauer
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Strauss	Straussa	k1gFnPc2	Straussa
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
řeckou	řecký	k2eAgFnSc7d1	řecká
filosofií	filosofie	k1gFnSc7	filosofie
a	a	k8xC	a
dílem	dílo	k1gNnSc7	dílo
Arthura	Arthur	k1gMnSc2	Arthur
Schopenhauera	Schopenhauer	k1gMnSc2	Schopenhauer
a	a	k8xC	a
Richarda	Richard	k1gMnSc2	Richard
Wagnera	Wagner	k1gMnSc2	Wagner
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgFnPc7	jenž
se	se	k3xPyFc4	se
ale	ale	k9	ale
později	pozdě	k6eAd2	pozdě
rozešel	rozejít	k5eAaPmAgMnS	rozejít
<g/>
.	.	kIx.	.
</s>
<s>
Sepsal	sepsat	k5eAaPmAgMnS	sepsat
řadu	řada	k1gFnSc4	řada
filosofických	filosofický	k2eAgNnPc2d1	filosofické
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
myšlení	myšlení	k1gNnSc4	myšlení
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
hlavním	hlavní	k2eAgNnSc7d1	hlavní
tématem	téma	k1gNnSc7	téma
byla	být	k5eAaImAgFnS	být
ostrá	ostrý	k2eAgFnSc1d1	ostrá
kritika	kritika	k1gFnSc1	kritika
evropského	evropský	k2eAgNnSc2d1	Evropské
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
platónské	platónský	k2eAgFnSc2d1	platónská
tradice	tradice	k1gFnSc2	tradice
a	a	k8xC	a
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
morálky	morálka	k1gFnSc2	morálka
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgMnPc3	jenž
Nietzsche	Nietzsche	k1gInSc1	Nietzsche
vytýkal	vytýkat	k5eAaImAgInS	vytýkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
staví	stavit	k5eAaImIp3nS	stavit
proti	proti	k7c3	proti
síle	síla	k1gFnSc3	síla
tvořivého	tvořivý	k2eAgInSc2d1	tvořivý
života	život	k1gInSc2	život
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
nihilismu	nihilismus	k1gInSc3	nihilismus
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
často	často	k6eAd1	často
aforisticky	aforisticky	k6eAd1	aforisticky
pojednávaná	pojednávaný	k2eAgNnPc1d1	pojednávané
témata	téma	k1gNnPc1	téma
"	"	kIx"	"
<g/>
smrti	smrt	k1gFnSc2	smrt
Boha	bůh	k1gMnSc2	bůh
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
vůle	vůle	k1gFnSc1	vůle
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
nadčlověka	nadčlověk	k1gMnSc2	nadčlověk
<g/>
"	"	kIx"	"
jsou	být	k5eAaImIp3nP	být
dodnes	dodnes	k6eAd1	dodnes
předmětem	předmět	k1gInSc7	předmět
diskusí	diskuse	k1gFnPc2	diskuse
a	a	k8xC	a
polemik	polemika	k1gFnPc2	polemika
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
nejznámějším	známý	k2eAgInSc7d3	nejznámější
spisem	spis	k1gInSc7	spis
je	být	k5eAaImIp3nS	být
Tak	tak	k9	tak
pravil	pravit	k5eAaBmAgMnS	pravit
Zarathustra	Zarathustr	k1gMnSc4	Zarathustr
<g/>
.	.	kIx.	.
</s>
<s>
Friedrich	Friedrich	k1gMnSc1	Friedrich
Nietzsche	Nietzsch	k1gFnSc2	Nietzsch
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
15	[number]	k4	15
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1844	[number]	k4	1844
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
obci	obec	k1gFnSc6	obec
Röcken	Röckna	k1gFnPc2	Röckna
<g/>
,	,	kIx,	,
asi	asi	k9	asi
20	[number]	k4	20
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Lipska	Lipsko	k1gNnSc2	Lipsko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
byli	být	k5eAaImAgMnP	být
luteránský	luteránský	k2eAgMnSc1d1	luteránský
farář	farář	k1gMnSc1	farář
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
vychovatel	vychovatel	k1gMnSc1	vychovatel
na	na	k7c6	na
altenburském	altenburský	k2eAgInSc6d1	altenburský
dvoře	dvůr	k1gInSc6	dvůr
Carl	Carl	k1gMnSc1	Carl
Ludwig	Ludwig	k1gMnSc1	Ludwig
Nietzsche	Nietzsche	k1gFnSc1	Nietzsche
(	(	kIx(	(
<g/>
1813	[number]	k4	1813
<g/>
-	-	kIx~	-
<g/>
1849	[number]	k4	1849
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
Franziska	Franzisko	k1gNnSc2	Franzisko
rozená	rozený	k2eAgFnSc1d1	rozená
Oehler	Oehler	k1gInSc1	Oehler
(	(	kIx(	(
<g/>
1826	[number]	k4	1826
<g/>
-	-	kIx~	-
<g/>
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
rodinách	rodina	k1gFnPc6	rodina
byla	být	k5eAaImAgFnS	být
řada	řada	k1gFnSc1	řada
protestantských	protestantský	k2eAgMnPc2d1	protestantský
duchovních	duchovní	k1gMnPc2	duchovní
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Friedrich	Friedrich	k1gMnSc1	Friedrich
dostal	dostat	k5eAaPmAgInS	dostat
Nietzsche	Nietzsche	k1gFnSc4	Nietzsche
po	po	k7c6	po
pruském	pruský	k2eAgMnSc6d1	pruský
králi	král	k1gMnSc6	král
Friedrichu	Friedrich	k1gMnSc6	Friedrich
Wilhelmu	Wilhelmo	k1gNnSc3	Wilhelmo
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
narozeniny	narozeniny	k1gFnPc4	narozeniny
ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1846	[number]	k4	1846
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
jeho	jeho	k3xOp3gFnSc1	jeho
sestra	sestra	k1gFnSc1	sestra
Elisabeth	Elisabetha	k1gFnPc2	Elisabetha
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
otce	otec	k1gMnSc2	otec
v	v	k7c6	v
r.	r.	kA	r.
1849	[number]	k4	1849
a	a	k8xC	a
mladšího	mladý	k2eAgMnSc2d2	mladší
bratra	bratr	k1gMnSc2	bratr
Ludwiga	Ludwig	k1gMnSc2	Ludwig
Josefa	Josef	k1gMnSc2	Josef
(	(	kIx(	(
<g/>
1848	[number]	k4	1848
<g/>
-	-	kIx~	-
<g/>
1850	[number]	k4	1850
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
Naumburgu	Naumburg	k1gInSc2	Naumburg
nad	nad	k7c7	nad
Saalou	Saala	k1gFnSc7	Saala
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1850	[number]	k4	1850
<g/>
-	-	kIx~	-
<g/>
1856	[number]	k4	1856
žil	žít	k5eAaImAgMnS	žít
Nietzsche	Nietzsche	k1gNnSc3	Nietzsche
v	v	k7c6	v
naumburské	naumburský	k2eAgFnSc6d1	naumburský
"	"	kIx"	"
<g/>
ženské	ženský	k2eAgFnSc6d1	ženská
domácnosti	domácnost	k1gFnSc6	domácnost
<g/>
"	"	kIx"	"
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
a	a	k8xC	a
sestrou	sestra	k1gFnSc7	sestra
<g/>
,	,	kIx,	,
s	s	k7c7	s
babičkou	babička	k1gFnSc7	babička
<g/>
,	,	kIx,	,
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
neprovdanými	provdaný	k2eNgFnPc7d1	neprovdaná
tetičkami	tetička	k1gFnPc7	tetička
z	z	k7c2	z
otcovy	otcův	k2eAgFnSc2d1	otcova
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
se	s	k7c7	s
služebnou	služebna	k1gFnSc7	služebna
<g/>
;	;	kIx,	;
teprve	teprve	k6eAd1	teprve
s	s	k7c7	s
dědictvím	dědictví	k1gNnSc7	dědictví
po	po	k7c6	po
babičce	babička	k1gFnSc6	babička
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zemřela	zemřít	k5eAaPmAgFnS	zemřít
v	v	k7c6	v
r.	r.	kA	r.
1856	[number]	k4	1856
<g/>
,	,	kIx,	,
mohla	moct	k5eAaImAgFnS	moct
matka	matka	k1gFnSc1	matka
pořídit	pořídit	k5eAaPmF	pořídit
vlastní	vlastní	k2eAgNnSc4d1	vlastní
bydlení	bydlení	k1gNnSc4	bydlení
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
chodil	chodit	k5eAaImAgMnS	chodit
Nietzsche	Nietzsche	k1gInSc4	Nietzsche
do	do	k7c2	do
všeobecné	všeobecný	k2eAgFnSc2d1	všeobecná
chlapecké	chlapecký	k2eAgFnSc2d1	chlapecká
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ale	ale	k9	ale
cítil	cítit	k5eAaImAgMnS	cítit
osamělý	osamělý	k2eAgMnSc1d1	osamělý
<g/>
;	;	kIx,	;
pak	pak	k6eAd1	pak
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
soukromou	soukromý	k2eAgFnSc4d1	soukromá
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
navázal	navázat	k5eAaPmAgInS	navázat
svá	svůj	k3xOyFgNnPc4	svůj
první	první	k4xOgNnPc4	první
přátelství	přátelství	k1gNnPc4	přátelství
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
s	s	k7c7	s
Gustavem	Gustav	k1gMnSc7	Gustav
Krugem	Krug	k1gMnSc7	Krug
a	a	k8xC	a
Wilhelmem	Wilhelm	k1gMnSc7	Wilhelm
Pinderem	Pinder	k1gMnSc7	Pinder
-	-	kIx~	-
oba	dva	k4xCgMnPc1	dva
pocházeli	pocházet	k5eAaImAgMnP	pocházet
z	z	k7c2	z
vážených	vážený	k2eAgFnPc2d1	Vážená
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
soukromou	soukromý	k2eAgFnSc4d1	soukromá
školu	škola	k1gFnSc4	škola
navázalo	navázat	k5eAaPmAgNnS	navázat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1854	[number]	k4	1854
studium	studium	k1gNnSc4	studium
na	na	k7c6	na
církevní	církevní	k2eAgFnSc6d1	církevní
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
Naumburgu	Naumburg	k1gInSc6	Naumburg
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
vyniklo	vyniknout	k5eAaPmAgNnS	vyniknout
jeho	jeho	k3xOp3gNnSc4	jeho
jedinečné	jedinečný	k2eAgNnSc4d1	jedinečné
hudební	hudební	k2eAgNnSc4d1	hudební
a	a	k8xC	a
jazykové	jazykový	k2eAgNnSc4d1	jazykové
nadání	nadání	k1gNnSc4	nadání
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Nietzsche	Nietzsche	k1gInSc1	Nietzsche
mohl	moct	k5eAaImAgInS	moct
pokračovat	pokračovat	k5eAaImF	pokračovat
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
na	na	k7c6	na
uznávané	uznávaný	k2eAgFnSc6d1	uznávaná
internátní	internátní	k2eAgFnSc6d1	internátní
škole	škola	k1gFnSc6	škola
Schulpforta	Schulpfort	k1gMnSc4	Schulpfort
(	(	kIx(	(
<g/>
léta	léto	k1gNnSc2	léto
1858	[number]	k4	1858
<g/>
-	-	kIx~	-
<g/>
1864	[number]	k4	1864
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
poznal	poznat	k5eAaPmAgMnS	poznat
jako	jako	k8xC	jako
trvalé	trvalý	k2eAgMnPc4d1	trvalý
přátele	přítel	k1gMnPc4	přítel
Paula	Paul	k1gMnSc4	Paul
Deussena	Deussen	k2eAgMnSc4d1	Deussen
a	a	k8xC	a
Carla	Carl	k1gMnSc4	Carl
von	von	k1gInSc4	von
Gresdorfa	Gresdorf	k1gMnSc2	Gresdorf
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
studijní	studijní	k2eAgInSc1d1	studijní
prospěch	prospěch	k1gInSc1	prospěch
byl	být	k5eAaImAgInS	být
velice	velice	k6eAd1	velice
dobrý	dobrý	k2eAgInSc1d1	dobrý
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
si	se	k3xPyFc3	se
našel	najít	k5eAaPmAgMnS	najít
ještě	ještě	k9	ještě
čas	čas	k1gInSc4	čas
na	na	k7c4	na
psaní	psaní	k1gNnSc4	psaní
básní	báseň	k1gFnPc2	báseň
a	a	k8xC	a
komponování	komponování	k1gNnSc6	komponování
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
úrovni	úroveň	k1gFnSc3	úroveň
školy	škola	k1gFnSc2	škola
i	i	k8xC	i
vyučování	vyučování	k1gNnSc2	vyučování
a	a	k8xC	a
dostupnosti	dostupnost	k1gFnSc2	dostupnost
literatury	literatura	k1gFnSc2	literatura
zde	zde	k6eAd1	zde
mohl	moct	k5eAaImAgInS	moct
značně	značně	k6eAd1	značně
rozšířit	rozšířit	k5eAaPmF	rozšířit
svoje	svůj	k3xOyFgFnPc4	svůj
vědomosti	vědomost	k1gFnPc4	vědomost
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
zde	zde	k6eAd1	zde
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jeho	jeho	k3xOp3gFnSc1	jeho
představa	představa	k1gFnSc1	představa
o	o	k7c6	o
antice	antika	k1gFnSc6	antika
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
vytvářet	vytvářet	k5eAaImF	vytvářet
odstup	odstup	k1gInSc4	odstup
od	od	k7c2	od
spíše	spíše	k9	spíše
maloměštácko-křesťanského	maloměštáckořesťanský	k2eAgInSc2d1	maloměštácko-křesťanský
světa	svět	k1gInSc2	svět
jeho	jeho	k3xOp3gFnSc2	jeho
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1864	[number]	k4	1864
začal	začít	k5eAaPmAgInS	začít
Nietzsche	Nietzsche	k1gNnSc4	Nietzsche
studium	studium	k1gNnSc4	studium
protestantské	protestantský	k2eAgFnSc2d1	protestantská
teologie	teologie	k1gFnSc2	teologie
a	a	k8xC	a
klasické	klasický	k2eAgFnSc2d1	klasická
filologie	filologie	k1gFnSc2	filologie
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Bonnu	Bonn	k1gInSc6	Bonn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Deussenem	Deussen	k1gInSc7	Deussen
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
buršáckého	buršácký	k2eAgInSc2d1	buršácký
spolku	spolek	k1gInSc2	spolek
Frankonia	Frankonium	k1gNnSc2	Frankonium
<g/>
;	;	kIx,	;
spolkový	spolkový	k2eAgInSc1d1	spolkový
život	život	k1gInSc1	život
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
ale	ale	k9	ale
nezamlouval	zamlouvat	k5eNaImAgInS	zamlouvat
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
spolek	spolek	k1gInSc1	spolek
opustil	opustit	k5eAaPmAgInS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Pilně	pilně	k6eAd1	pilně
četl	číst	k5eAaImAgMnS	číst
polemiky	polemika	k1gFnPc4	polemika
"	"	kIx"	"
<g/>
mladohegeliánů	mladohegelián	k1gMnPc2	mladohegelián
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Ludwiga	Ludwig	k1gMnSc2	Ludwig
Feuerbacha	Feuerbach	k1gMnSc2	Feuerbach
<g/>
,	,	kIx,	,
D.	D.	kA	D.
F.	F.	kA	F.
Strausse	Strausse	k1gFnSc1	Strausse
a	a	k8xC	a
Bruno	Bruno	k1gMnSc1	Bruno
Bauera	Bauer	k1gMnSc2	Bauer
proti	proti	k7c3	proti
tradičnímu	tradiční	k2eAgNnSc3d1	tradiční
křesťanství	křesťanství	k1gNnSc3	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
po	po	k7c6	po
prvním	první	k4xOgInSc6	první
semestru	semestr	k1gInSc6	semestr
zanechal	zanechat	k5eAaPmAgMnS	zanechat
studia	studio	k1gNnPc4	studio
teologie	teologie	k1gFnSc2	teologie
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
matce	matka	k1gFnSc3	matka
vůbec	vůbec	k9	vůbec
nelíbilo	líbit	k5eNaImAgNnS	líbit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tím	ten	k3xDgNnSc7	ten
víc	hodně	k6eAd2	hodně
se	se	k3xPyFc4	se
nadchl	nadchnout	k5eAaPmAgMnS	nadchnout
pro	pro	k7c4	pro
filologii	filologie	k1gFnSc4	filologie
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgMnS	studovat
ji	on	k3xPp3gFnSc4	on
u	u	k7c2	u
Friedricha	Friedrich	k1gMnSc2	Friedrich
Ritschela	Ritschel	k1gMnSc2	Ritschel
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
roku	rok	k1gInSc2	rok
1865	[number]	k4	1865
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Lipska	Lipsko	k1gNnSc2	Lipsko
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgMnSc7d1	důležitý
přítelem	přítel	k1gMnSc7	přítel
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
Nietzscheho	Nietzsche	k1gMnSc4	Nietzsche
stal	stát	k5eAaPmAgMnS	stát
Erwin	Erwin	k1gMnSc1	Erwin
Rohde	Rohd	k1gMnSc5	Rohd
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
patřil	patřit	k5eAaImAgMnS	patřit
do	do	k7c2	do
nejužšího	úzký	k2eAgInSc2d3	nejužší
studentského	studentský	k2eAgInSc2d1	studentský
kruhu	kruh	k1gInSc2	kruh
kolem	kolem	k7c2	kolem
Ritschela	Ritschel	k1gMnSc2	Ritschel
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
pocházejí	pocházet	k5eAaImIp3nP	pocházet
jeho	jeho	k3xOp3gFnPc1	jeho
první	první	k4xOgFnPc1	první
zveřejněné	zveřejněný	k2eAgFnPc1d1	zveřejněná
filologické	filologický	k2eAgFnPc1d1	filologická
práce	práce	k1gFnPc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1865	[number]	k4	1865
se	se	k3xPyFc4	se
Nietzsche	Nietzsche	k1gNnSc2	Nietzsche
patrně	patrně	k6eAd1	patrně
seznámil	seznámit	k5eAaPmAgMnS	seznámit
se	s	k7c7	s
solipsismem	solipsismus	k1gInSc7	solipsismus
Maxe	Max	k1gMnSc2	Max
Stirnera	Stirner	k1gMnSc2	Stirner
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přiklonil	přiklonit	k5eAaPmAgMnS	přiklonit
se	se	k3xPyFc4	se
k	k	k7c3	k
filosofii	filosofie	k1gFnSc3	filosofie
Arthura	Arthur	k1gMnSc2	Arthur
Schopenhauera	Schopenhauer	k1gMnSc2	Schopenhauer
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1866	[number]	k4	1866
četl	číst	k5eAaImAgMnS	číst
"	"	kIx"	"
<g/>
Dějiny	dějiny	k1gFnPc1	dějiny
materialismu	materialismus	k1gInSc2	materialismus
<g/>
"	"	kIx"	"
F.	F.	kA	F.
A.	A.	kA	A.
Langelse	Langels	k1gMnSc2	Langels
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
čerpal	čerpat	k5eAaImAgInS	čerpat
filosofické	filosofický	k2eAgInPc4d1	filosofický
nápady	nápad	k1gInPc4	nápad
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
neomezoval	omezovat	k5eNaImAgMnS	omezovat
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
filologii	filologie	k1gFnSc4	filologie
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
pilně	pilně	k6eAd1	pilně
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
<g/>
.	.	kIx.	.
</s>
<s>
Prusko-rakouské	pruskoakouský	k2eAgInPc1d1	prusko-rakouský
válce	válec	k1gInPc1	válec
1866	[number]	k4	1866
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
Prusové	Prus	k1gMnPc1	Prus
obsadili	obsadit	k5eAaPmAgMnP	obsadit
i	i	k9	i
Lipsko	Lipsko	k1gNnSc4	Lipsko
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
ještě	ještě	k6eAd1	ještě
vyhnout	vyhnout	k5eAaPmF	vyhnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1867	[number]	k4	1867
byl	být	k5eAaImAgInS	být
odveden	odvést	k5eAaPmNgInS	odvést
a	a	k8xC	a
sloužil	sloužit	k5eAaImAgInS	sloužit
rok	rok	k1gInSc1	rok
jako	jako	k8xS	jako
dobrovolník	dobrovolník	k1gMnSc1	dobrovolník
pruského	pruský	k2eAgNnSc2d1	pruské
dělostřelectva	dělostřelectvo	k1gNnSc2	dělostřelectvo
v	v	k7c6	v
Norimberku	Norimberk	k1gInSc6	Norimberk
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
těžkém	těžký	k2eAgInSc6d1	těžký
pádu	pád	k1gInSc6	pád
z	z	k7c2	z
koně	kůň	k1gMnSc2	kůň
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1868	[number]	k4	1868
byl	být	k5eAaImAgInS	být
uznán	uznat	k5eAaPmNgInS	uznat
neschopným	schopný	k2eNgFnPc3d1	neschopná
další	další	k2eAgMnSc1d1	další
služby	služba	k1gFnPc4	služba
<g/>
;	;	kIx,	;
dobu	doba	k1gFnSc4	doba
léčení	léčení	k1gNnSc2	léčení
využil	využít	k5eAaPmAgInS	využít
k	k	k7c3	k
další	další	k2eAgFnSc3d1	další
filologické	filologický	k2eAgFnSc3d1	filologická
práci	práce	k1gFnSc3	práce
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
také	také	k9	také
v	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
roce	rok	k1gInSc6	rok
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
budoucnost	budoucnost	k1gFnSc4	budoucnost
bylo	být	k5eAaImAgNnS	být
významné	významný	k2eAgNnSc1d1	významné
jeho	jeho	k3xOp3gNnSc4	jeho
první	první	k4xOgNnSc4	první
setkání	setkání	k1gNnSc4	setkání
s	s	k7c7	s
Richardem	Richard	k1gMnSc7	Richard
Wagnerem	Wagner	k1gMnSc7	Wagner
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Ritschlovy	Ritschlův	k2eAgFnSc2d1	Ritschlův
podpory	podpora	k1gFnSc2	podpora
a	a	k8xC	a
s	s	k7c7	s
přímluvou	přímluva	k1gFnSc7	přímluva
W.	W.	kA	W.
Vischer-Bilfingera	Vischer-Bilfinger	k1gMnSc2	Vischer-Bilfinger
se	se	k3xPyFc4	se
Nietzsche	Nietzsche	k1gNnSc3	Nietzsche
roku	rok	k1gInSc2	rok
1868	[number]	k4	1868
stal	stát	k5eAaPmAgInS	stát
mimořádným	mimořádný	k2eAgMnSc7d1	mimořádný
profesorem	profesor	k1gMnSc7	profesor
klasické	klasický	k2eAgFnSc2d1	klasická
filologie	filologie	k1gFnSc2	filologie
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ještě	ještě	k9	ještě
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
promocí	promoce	k1gFnSc7	promoce
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
vyučoval	vyučovat	k5eAaImAgInS	vyučovat
na	na	k7c6	na
basilejském	basilejský	k2eAgNnSc6d1	Basilejské
"	"	kIx"	"
<g/>
Paedagogiu	Paedagogium	k1gNnSc6	Paedagogium
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejdůležitější	důležitý	k2eAgInSc4d3	nejdůležitější
poznatek	poznatek	k1gInSc4	poznatek
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
pokládal	pokládat	k5eAaImAgMnS	pokládat
zjištění	zjištění	k1gNnPc4	zjištění
<g/>
,	,	kIx,	,
že	že	k8xS	že
antická	antický	k2eAgFnSc1d1	antická
metrika	metrika	k1gFnSc1	metrika
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
jen	jen	k9	jen
na	na	k7c6	na
délce	délka	k1gFnSc6	délka
slabik	slabika	k1gFnPc2	slabika
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
moderní	moderní	k2eAgFnSc2d1	moderní
<g/>
,	,	kIx,	,
postavené	postavený	k2eAgFnSc2d1	postavená
na	na	k7c6	na
přízvuku	přízvuk	k1gInSc6	přízvuk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
žádost	žádost	k1gFnSc4	žádost
byl	být	k5eAaImAgInS	být
Nietzsche	Nietzsche	k1gInSc1	Nietzsche
po	po	k7c6	po
přestěhování	přestěhování	k1gNnSc6	přestěhování
do	do	k7c2	do
Basileje	Basilej	k1gFnSc2	Basilej
propuštěn	propustit	k5eAaPmNgInS	propustit
z	z	k7c2	z
pruského	pruský	k2eAgNnSc2d1	pruské
státního	státní	k2eAgNnSc2d1	státní
občanství	občanství	k1gNnSc2	občanství
a	a	k8xC	a
do	do	k7c2	do
konce	konec	k1gInSc2	konec
života	život	k1gInSc2	život
zůstal	zůstat	k5eAaPmAgMnS	zůstat
bez	bez	k7c2	bez
státní	státní	k2eAgFnSc2d1	státní
příslušnosti	příslušnost	k1gFnSc2	příslušnost
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
krátce	krátce	k6eAd1	krátce
sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k9	jako
zdravotník	zdravotník	k1gMnSc1	zdravotník
v	v	k7c6	v
Prusko-francouzské	pruskorancouzský	k2eAgFnSc6d1	prusko-francouzská
válce	válka	k1gFnSc6	válka
1870	[number]	k4	1870
<g/>
-	-	kIx~	-
<g/>
1871	[number]	k4	1871
<g/>
,	,	kIx,	,
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Založení	založení	k1gNnSc1	založení
Německé	německý	k2eAgFnSc2d1	německá
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
následující	následující	k2eAgFnSc4d1	následující
éru	éra	k1gFnSc4	éra
Bismarkovu	Bismarkův	k2eAgFnSc4d1	Bismarkova
vnímal	vnímat	k5eAaImAgMnS	vnímat
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
skepticky	skepticky	k6eAd1	skepticky
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
Nietzscheho	Nietzsche	k1gMnSc2	Nietzsche
přátelství	přátelství	k1gNnSc2	přátelství
s	s	k7c7	s
kolegou	kolega	k1gMnSc7	kolega
<g/>
,	,	kIx,	,
ateistickým	ateistický	k2eAgMnSc7d1	ateistický
profesorem	profesor	k1gMnSc7	profesor
teologie	teologie	k1gFnSc2	teologie
Franzem	Franz	k1gMnSc7	Franz
Overbeckem	Overbeck	k1gInSc7	Overbeck
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
trvalo	trvat	k5eAaImAgNnS	trvat
až	až	k9	až
do	do	k7c2	do
Nietzscheho	Nietzsche	k1gMnSc2	Nietzsche
zhroucení	zhroucení	k1gNnSc2	zhroucení
<g/>
.	.	kIx.	.
</s>
<s>
Starším	starý	k2eAgMnPc3d2	starší
a	a	k8xC	a
váženým	vážený	k2eAgMnSc7d1	vážený
kolegou	kolega	k1gMnSc7	kolega
byl	být	k5eAaImAgMnS	být
historik	historik	k1gMnSc1	historik
Jacob	Jacoba	k1gFnPc2	Jacoba
Burckhardt	Burckhardt	k1gMnSc1	Burckhardt
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
však	však	k9	však
byl	být	k5eAaImAgMnS	být
vůči	vůči	k7c3	vůči
Nietzschemu	Nietzschem	k1gInSc3	Nietzschem
spíše	spíše	k9	spíše
rezervovaný	rezervovaný	k2eAgInSc4d1	rezervovaný
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
v	v	k7c6	v
r.	r.	kA	r.
1868	[number]	k4	1868
se	se	k3xPyFc4	se
Nietzsche	Nietzsche	k1gFnSc1	Nietzsche
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Richardem	Richard	k1gMnSc7	Richard
Wagnerem	Wagner	k1gMnSc7	Wagner
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
pozdější	pozdní	k2eAgFnSc7d2	pozdější
ženou	žena	k1gFnSc7	žena
Cosimou	Cosimý	k2eAgFnSc7d1	Cosimý
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
velmi	velmi	k6eAd1	velmi
uctíval	uctívat	k5eAaImAgInS	uctívat
a	a	k8xC	a
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
svého	svůj	k3xOyFgInSc2	svůj
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
byl	být	k5eAaImAgInS	být
častým	častý	k2eAgMnSc7d1	častý
hostem	host	k1gMnSc7	host
v	v	k7c6	v
mistrově	mistrův	k2eAgInSc6d1	mistrův
domě	dům	k1gInSc6	dům
v	v	k7c6	v
Tribschenu	Tribschen	k1gInSc6	Tribschen
u	u	k7c2	u
Luzernu	Luzerna	k1gFnSc4	Luzerna
<g/>
.	.	kIx.	.
</s>
<s>
Wagner	Wagner	k1gMnSc1	Wagner
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
pro	pro	k7c4	pro
něho	on	k3xPp3gMnSc4	on
po	po	k7c4	po
jistou	jistý	k2eAgFnSc4d1	jistá
dobu	doba	k1gFnSc4	doba
"	"	kIx"	"
<g/>
otcovskou	otcovský	k2eAgFnSc7d1	otcovská
postavou	postava	k1gFnSc7	postava
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ho	on	k3xPp3gMnSc4	on
občas	občas	k6eAd1	občas
zval	zvát	k5eAaImAgMnS	zvát
do	do	k7c2	do
svých	svůj	k3xOyFgInPc2	svůj
nejužších	úzký	k2eAgInPc2d3	nejužší
kruhů	kruh	k1gInPc2	kruh
<g/>
,	,	kIx,	,
především	především	k9	především
však	však	k9	však
spoléhal	spoléhat	k5eAaImAgMnS	spoléhat
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
pomoc	pomoc	k1gFnSc4	pomoc
při	při	k7c6	při
propagaci	propagace	k1gFnSc6	propagace
založení	založení	k1gNnSc2	založení
Festspielhausu	Festspielhaus	k1gInSc2	Festspielhaus
v	v	k7c6	v
Bayreuthu	Bayreuth	k1gInSc6	Bayreuth
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1872	[number]	k4	1872
vydal	vydat	k5eAaPmAgInS	vydat
Nietzsche	Nietzsch	k1gInSc2	Nietzsch
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
větší	veliký	k2eAgFnSc4d2	veliký
práci	práce	k1gFnSc4	práce
Zrození	zrození	k1gNnSc2	zrození
tragédie	tragédie	k1gFnSc2	tragédie
z	z	k7c2	z
ducha	duch	k1gMnSc2	duch
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
exaktní	exaktní	k2eAgFnSc2d1	exaktní
filologické	filologický	k2eAgFnSc2d1	filologická
metody	metoda	k1gFnSc2	metoda
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
filosofická	filosofický	k2eAgFnSc1d1	filosofická
spekulace	spekulace	k1gFnSc1	spekulace
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
filologičtí	filologický	k2eAgMnPc1d1	filologický
kolegové	kolega	k1gMnPc1	kolega
včetně	včetně	k7c2	včetně
Ritschela	Ritschela	k1gFnSc1	Ritschela
ji	on	k3xPp3gFnSc4	on
většinou	většinou	k6eAd1	většinou
nepochopili	pochopit	k5eNaPmAgMnP	pochopit
a	a	k8xC	a
mlčky	mlčky	k6eAd1	mlčky
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
díky	díky	k7c3	díky
polemickému	polemický	k2eAgInSc3d1	polemický
článku	článek	k1gInSc3	článek
Filologie	filologie	k1gFnSc2	filologie
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
!	!	kIx.	!
</s>
<s>
od	od	k7c2	od
Ulricha	Ulrich	k1gMnSc2	Ulrich
von	von	k1gInSc1	von
Wilamowitz-Moellendorffa	Wilamowitz-Moellendorff	k1gMnSc4	Wilamowitz-Moellendorff
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
krátký	krátký	k2eAgInSc1d1	krátký
veřejný	veřejný	k2eAgInSc1d1	veřejný
spor	spor	k1gInSc1	spor
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
Rohde	Rohd	k1gInSc5	Rohd
<g/>
,	,	kIx,	,
toho	ten	k3xDgInSc2	ten
času	čas	k1gInSc2	čas
profesor	profesor	k1gMnSc1	profesor
v	v	k7c6	v
Kielu	Kiel	k1gInSc6	Kiel
<g/>
,	,	kIx,	,
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
Wagner	Wagner	k1gMnSc1	Wagner
Nietzscheho	Nietzsche	k1gMnSc2	Nietzsche
podpořili	podpořit	k5eAaPmAgMnP	podpořit
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
si	se	k3xPyFc3	se
samozřejmě	samozřejmě	k6eAd1	samozřejmě
všiml	všimnout	k5eAaPmAgMnS	všimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
izolace	izolace	k1gFnSc1	izolace
ve	v	k7c6	v
filologické	filologický	k2eAgFnSc6d1	filologická
oblasti	oblast	k1gFnSc6	oblast
prohlubuje	prohlubovat	k5eAaImIp3nS	prohlubovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
už	už	k9	už
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
se	se	k3xPyFc4	se
neúspěšně	úspěšně	k6eNd1	úspěšně
ucházel	ucházet	k5eAaImAgMnS	ucházet
o	o	k7c4	o
místo	místo	k1gNnSc4	místo
profesora	profesor	k1gMnSc2	profesor
filosofie	filosofie	k1gFnSc2	filosofie
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
získal	získat	k5eAaPmAgMnS	získat
Rudolf	Rudolf	k1gMnSc1	Rudolf
Christoph	Christoph	k1gMnSc1	Christoph
Eucken	Euckno	k1gNnPc2	Euckno
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
jeho	jeho	k3xOp3gFnPc1	jeho
čtyři	čtyři	k4xCgFnPc1	čtyři
Nečasové	Nečasové	k2eAgFnPc1d1	Nečasové
úvahy	úvaha	k1gFnPc1	úvaha
(	(	kIx(	(
<g/>
1873	[number]	k4	1873
<g/>
-	-	kIx~	-
<g/>
1876	[number]	k4	1876
<g/>
)	)	kIx)	)
s	s	k7c7	s
kritikou	kritika	k1gFnSc7	kritika
kultury	kultura	k1gFnSc2	kultura
v	v	k7c6	v
linii	linie	k1gFnSc6	linie
Schopenhauerově	Schopenhauerův	k2eAgFnSc6d1	Schopenhauerova
a	a	k8xC	a
Wagnerově	Wagnerův	k2eAgFnSc6d1	Wagnerova
neměly	mít	k5eNaImAgFnP	mít
očekávaný	očekávaný	k2eAgInSc4d1	očekávaný
ohlas	ohlas	k1gInSc4	ohlas
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Wagnerových	Wagnerových	k2eAgInPc6d1	Wagnerových
kruzích	kruh	k1gInPc6	kruh
se	se	k3xPyFc4	se
Nietzsche	Nietzsche	k1gInSc1	Nietzsche
seznámil	seznámit	k5eAaPmAgInS	seznámit
s	s	k7c7	s
Malwidou	Malwida	k1gFnSc7	Malwida
von	von	k1gInSc1	von
Meysenburg	Meysenburg	k1gInSc4	Meysenburg
<g/>
,	,	kIx,	,
s	s	k7c7	s
Hansem	Hans	k1gMnSc7	Hans
Guidem	Guid	k1gMnSc7	Guid
von	von	k1gInSc4	von
Bülow	Bülow	k1gFnSc2	Bülow
a	a	k8xC	a
spřátelil	spřátelit	k5eAaPmAgMnS	spřátelit
se	se	k3xPyFc4	se
s	s	k7c7	s
Paulem	Paul	k1gMnSc7	Paul
Rée	Rée	k1gMnSc7	Rée
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
jehož	jehož	k3xOyRp3gInSc7	jehož
vlivem	vliv	k1gInSc7	vliv
opustil	opustit	k5eAaPmAgMnS	opustit
kulturní	kulturní	k2eAgInSc4d1	kulturní
pesimismus	pesimismus	k1gInSc4	pesimismus
svých	svůj	k3xOyFgFnPc2	svůj
prvních	první	k4xOgFnPc2	první
prací	práce	k1gFnPc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Zklamán	zklamán	k2eAgMnSc1d1	zklamán
z	z	k7c2	z
prvního	první	k4xOgInSc2	první
festivalu	festival	k1gInSc2	festival
v	v	k7c6	v
Bayreuthu	Bayreuth	k1gInSc6	Bayreuth
roku	rok	k1gInSc2	rok
1876	[number]	k4	1876
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gNnSc4	on
odpudila	odpudit	k5eAaPmAgFnS	odpudit
banalita	banalita	k1gFnSc1	banalita
činohry	činohra	k1gFnSc2	činohra
a	a	k8xC	a
nízká	nízký	k2eAgFnSc1d1	nízká
úroveň	úroveň	k1gFnSc1	úroveň
publika	publikum	k1gNnSc2	publikum
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
rozejít	rozejít	k5eAaPmF	rozejít
se	se	k3xPyFc4	se
s	s	k7c7	s
Wagnerem	Wagner	k1gMnSc7	Wagner
a	a	k8xC	a
z	z	k7c2	z
nadšeného	nadšený	k2eAgMnSc2d1	nadšený
ctitele	ctitel	k1gMnSc2	ctitel
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc7	jeho
rozhodným	rozhodný	k2eAgMnSc7d1	rozhodný
odpůrcem	odpůrce	k1gMnSc7	odpůrce
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
Lidské	lidský	k2eAgNnSc1d1	lidské
<g/>
,	,	kIx,	,
příliš	příliš	k6eAd1	příliš
lidské	lidský	k2eAgNnSc1d1	lidské
(	(	kIx(	(
<g/>
1878	[number]	k4	1878
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
rozchod	rozchod	k1gInSc4	rozchod
s	s	k7c7	s
Wagnerovou	Wagnerův	k2eAgFnSc7d1	Wagnerova
a	a	k8xC	a
Schopenhauerovou	Schopenhauerův	k2eAgFnSc7d1	Schopenhauerova
filosofií	filosofie	k1gFnSc7	filosofie
zřejmý	zřejmý	k2eAgMnSc1d1	zřejmý
a	a	k8xC	a
také	také	k9	také
přátelství	přátelství	k1gNnSc3	přátelství
k	k	k7c3	k
Deussenovi	Deussen	k1gMnSc3	Deussen
a	a	k8xC	a
Rohdemu	Rohdem	k1gMnSc3	Rohdem
viditelně	viditelně	k6eAd1	viditelně
ochladlo	ochladnout	k5eAaPmAgNnS	ochladnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
podnikl	podniknout	k5eAaPmAgMnS	podniknout
Nietzsche	Nietzsche	k1gFnSc4	Nietzsche
několik	několik	k4yIc4	několik
pokusů	pokus	k1gInPc2	pokus
najít	najít	k5eAaPmF	najít
mladou	mladý	k2eAgFnSc4d1	mladá
majetnou	majetný	k2eAgFnSc4d1	majetná
ženu	žena	k1gFnSc4	žena
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
chtěla	chtít	k5eAaImAgFnS	chtít
vdávat	vdávat	k5eAaImF	vdávat
<g/>
,	,	kIx,	,
v	v	k7c6	v
čemž	což	k3yQnSc6	což
ho	on	k3xPp3gMnSc2	on
podporovala	podporovat	k5eAaImAgFnS	podporovat
hlavně	hlavně	k9	hlavně
von	von	k1gInSc4	von
Meysenburgová	Meysenburgový	k2eAgFnSc1d1	Meysenburgový
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
plány	plán	k1gInPc1	plán
se	se	k3xPyFc4	se
ale	ale	k9	ale
nevydařily	vydařit	k5eNaPmAgFnP	vydařit
<g/>
.	.	kIx.	.
</s>
<s>
Nemoci	nemoc	k1gFnPc4	nemoc
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jej	on	k3xPp3gNnSc4	on
sužovaly	sužovat	k5eAaImAgInP	sužovat
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
-	-	kIx~	-
silná	silný	k2eAgFnSc1d1	silná
krátkozrakost	krátkozrakost	k1gFnSc1	krátkozrakost
vedoucí	vedoucí	k1gMnSc1	vedoucí
prakticky	prakticky	k6eAd1	prakticky
až	až	k9	až
ke	k	k7c3	k
slepotě	slepota	k1gFnSc3	slepota
<g/>
,	,	kIx,	,
záchvaty	záchvat	k1gInPc1	záchvat
migrény	migréna	k1gFnSc2	migréna
a	a	k8xC	a
žaludeční	žaludeční	k2eAgFnPc1d1	žaludeční
potíže	potíž	k1gFnPc1	potíž
sílily	sílit	k5eAaImAgFnP	sílit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
si	se	k3xPyFc3	se
musel	muset	k5eAaImAgMnS	muset
brát	brát	k5eAaImF	brát
delší	dlouhý	k2eAgNnSc4d2	delší
a	a	k8xC	a
delší	dlouhý	k2eAgNnSc4d2	delší
volno	volno	k1gNnSc4	volno
od	od	k7c2	od
své	svůj	k3xOyFgFnSc2	svůj
profesorské	profesorský	k2eAgFnSc2d1	profesorská
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1879	[number]	k4	1879
musel	muset	k5eAaImAgMnS	muset
kvůli	kvůli	k7c3	kvůli
prudkým	prudký	k2eAgFnPc3d1	prudká
bolestem	bolest	k1gFnPc3	bolest
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
očí	oko	k1gNnPc2	oko
a	a	k8xC	a
častému	častý	k2eAgNnSc3d1	časté
zvracení	zvracení	k1gNnSc3	zvracení
předčasně	předčasně	k6eAd1	předčasně
odejít	odejít	k5eAaPmF	odejít
do	do	k7c2	do
penze	penze	k1gFnSc2	penze
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
Nietzsche	Nietzsche	k1gInSc1	Nietzsche
hledal	hledat	k5eAaImAgInS	hledat
příznivé	příznivý	k2eAgNnSc4d1	příznivé
podnebí	podnebí	k1gNnSc4	podnebí
pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
choroby	choroba	k1gFnPc4	choroba
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1879	[number]	k4	1879
hodně	hodně	k6eAd1	hodně
cestoval	cestovat	k5eAaImAgMnS	cestovat
a	a	k8xC	a
žil	žít	k5eAaImAgMnS	žít
na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
jako	jako	k8xS	jako
autor	autor	k1gMnSc1	autor
na	na	k7c6	na
volné	volný	k2eAgFnSc6d1	volná
noze	noha	k1gFnSc6	noha
<g/>
;	;	kIx,	;
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
Sils-Maria	Sils-Marium	k1gNnPc4	Sils-Marium
v	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
(	(	kIx(	(
<g/>
Janov	Janov	k1gInSc1	Janov
<g/>
,	,	kIx,	,
Rapallo	Rapallo	k1gNnSc1	Rapallo
<g/>
,	,	kIx,	,
Turín	Turín	k1gInSc1	Turín
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
Nice	Nice	k1gFnSc6	Nice
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
také	také	k6eAd1	také
navštívil	navštívit	k5eAaPmAgMnS	navštívit
svoji	svůj	k3xOyFgFnSc4	svůj
rodinu	rodina	k1gFnSc4	rodina
v	v	k7c6	v
Naumburgu	Naumburg	k1gInSc6	Naumburg
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
střídavě	střídavě	k6eAd1	střídavě
přel	přít	k5eAaImAgMnS	přít
a	a	k8xC	a
smiřoval	smiřovat	k5eAaImAgMnS	smiřovat
se	se	k3xPyFc4	se
sestrou	sestra	k1gFnSc7	sestra
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
bývalý	bývalý	k2eAgMnSc1d1	bývalý
žák	žák	k1gMnSc1	žák
Peter	Peter	k1gMnSc1	Peter
Gast	Gast	k1gMnSc1	Gast
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
mu	on	k3xPp3gMnSc3	on
dělal	dělat	k5eAaImAgMnS	dělat
jakéhosi	jakýsi	k3yIgInSc2	jakýsi
sekretáře	sekretář	k1gInSc2	sekretář
<g/>
,	,	kIx,	,
a	a	k8xC	a
Franz	Franz	k1gMnSc1	Franz
Overbeck	Overbeck	k1gMnSc1	Overbeck
zůstali	zůstat	k5eAaPmAgMnP	zůstat
jeho	jeho	k3xOp3gMnPc7	jeho
nejvěrnějšími	věrný	k2eAgMnPc7d3	nejvěrnější
přáteli	přítel	k1gMnPc7	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
Ranní	ranní	k2eAgInPc1d1	ranní
červánky	červánek	k1gInPc1	červánek
a	a	k8xC	a
Radostná	radostný	k2eAgFnSc1d1	radostná
věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
další	další	k2eAgInPc4d1	další
spisy	spis	k1gInPc4	spis
v	v	k7c6	v
aforistickém	aforistický	k2eAgInSc6d1	aforistický
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1882	[number]	k4	1882
se	se	k3xPyFc4	se
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
rusko-německou	ruskoěmecký	k2eAgFnSc7d1	rusko-německá
spisovatelkou	spisovatelka	k1gFnSc7	spisovatelka
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
psychoanalytičkou	psychoanalytička	k1gFnSc7	psychoanalytička
Lou	Lou	k1gFnPc2	Lou
Andreas-Salomé	Andreas-Salomý	k2eAgNnSc1d1	Andreas-Salomý
<g/>
,	,	kIx,	,
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
a	a	k8xC	a
Gastovým	Gastův	k2eAgNnSc7d1	Gastův
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
ji	on	k3xPp3gFnSc4	on
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
ruku	ruka	k1gFnSc4	ruka
<g/>
,	,	kIx,	,
Lou	Lou	k1gFnPc4	Lou
Salomé	Salomý	k2eAgFnPc4d1	Salomý
však	však	k9	však
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
předtím	předtím	k6eAd1	předtím
rozešel	rozejít	k5eAaPmAgMnS	rozejít
ve	v	k7c6	v
zlém	zlé	k1gNnSc6	zlé
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
a	a	k8xC	a
sestrou	sestra	k1gFnSc7	sestra
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
také	také	k9	také
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
sestra	sestra	k1gFnSc1	sestra
se	se	k3xPyFc4	se
přičinila	přičinit	k5eAaPmAgFnS	přičinit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
Lou	Lou	k1gMnSc7	Lou
Salomé	Salomý	k2eAgInPc4d1	Salomý
rozešla	rozejít	k5eAaPmAgFnS	rozejít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pomýšlel	pomýšlet	k5eAaImAgInS	pomýšlet
na	na	k7c4	na
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Rapalla	Rapallo	k1gNnSc2	Rapallo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
během	během	k7c2	během
deseti	deset	k4xCc2	deset
dnů	den	k1gInPc2	den
napsal	napsat	k5eAaPmAgInS	napsat
první	první	k4xOgInSc4	první
díl	díl	k1gInSc4	díl
Tak	tak	k6eAd1	tak
pravil	pravit	k5eAaImAgMnS	pravit
Zarathustra	Zarathustr	k1gMnSc4	Zarathustr
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
však	však	k9	však
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
rozpaky	rozpak	k1gInPc7	rozpak
i	i	k8xC	i
u	u	k7c2	u
nejbližších	blízký	k2eAgMnPc2d3	nejbližší
přátel	přítel	k1gMnPc2	přítel
a	a	k8xC	a
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
vůbec	vůbec	k9	vůbec
neprodával	prodávat	k5eNaImAgMnS	prodávat
<g/>
;	;	kIx,	;
čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
díl	díl	k1gInSc1	díl
vydal	vydat	k5eAaPmAgInS	vydat
Nietzsche	Nietzsche	k1gInSc4	Nietzsche
jen	jen	k9	jen
ve	v	k7c6	v
40	[number]	k4	40
výtiscích	výtisk	k1gInPc6	výtisk
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
rozdával	rozdávat	k5eAaImAgMnS	rozdávat
jako	jako	k8xS	jako
dárky	dárek	k1gInPc4	dárek
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1886	[number]	k4	1886
vydal	vydat	k5eAaPmAgInS	vydat
vlastním	vlastní	k2eAgInSc7d1	vlastní
nákladem	náklad	k1gInSc7	náklad
Mimo	mimo	k7c4	mimo
dobro	dobro	k1gNnSc4	dobro
a	a	k8xC	a
zlo	zlo	k1gNnSc4	zlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tři	tři	k4xCgFnPc1	tři
předchozí	předchozí	k2eAgFnPc1d1	předchozí
práce	práce	k1gFnPc1	práce
vyšly	vyjít	k5eAaPmAgFnP	vyjít
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
vydání	vydání	k1gNnSc6	vydání
a	a	k8xC	a
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
Nietzscheho	Nietzsche	k1gMnSc4	Nietzsche
ze	z	k7c2	z
začal	začít	k5eAaPmAgInS	začít
pomalu	pomalu	k6eAd1	pomalu
probouzet	probouzet	k5eAaImF	probouzet
<g/>
.	.	kIx.	.
</s>
<s>
Setkal	setkat	k5eAaPmAgInS	setkat
se	se	k3xPyFc4	se
se	s	k7c7	s
spisovatelem	spisovatel	k1gMnSc7	spisovatel
Gottfriedem	Gottfried	k1gMnSc7	Gottfried
Kellerem	Keller	k1gMnSc7	Keller
a	a	k8xC	a
vyměnil	vyměnit	k5eAaPmAgInS	vyměnit
si	se	k3xPyFc3	se
dopisy	dopis	k1gInPc4	dopis
s	s	k7c7	s
Hippolytem	Hippolyt	k1gInSc7	Hippolyt
Taine	Tain	k1gInSc5	Tain
a	a	k8xC	a
s	s	k7c7	s
Georgem	Georg	k1gMnSc7	Georg
Brandesem	Brandes	k1gMnSc7	Brandes
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
začal	začít	k5eAaPmAgInS	začít
roku	rok	k1gInSc2	rok
1888	[number]	k4	1888
v	v	k7c6	v
Kodani	Kodaň	k1gFnSc6	Kodaň
o	o	k7c6	o
Nietzscheovi	Nietzscheus	k1gMnSc6	Nietzscheus
přednášet	přednášet	k5eAaImF	přednášet
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1886	[number]	k4	1886
jeho	jeho	k3xOp3gNnSc1	jeho
sestra	sestra	k1gFnSc1	sestra
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
vdaná	vdaný	k2eAgFnSc1d1	vdaná
za	za	k7c4	za
antisemitu	antisemita	k1gMnSc4	antisemita
Bernharda	Bernhard	k1gMnSc4	Bernhard
Förstera	Förster	k1gMnSc4	Förster
<g/>
,	,	kIx,	,
odjela	odjet	k5eAaPmAgFnS	odjet
do	do	k7c2	do
Paraguaye	Paraguay	k1gFnSc2	Paraguay
zakládat	zakládat	k5eAaImF	zakládat
"	"	kIx"	"
<g/>
germánskou	germánský	k2eAgFnSc4d1	germánská
<g/>
"	"	kIx"	"
kolonii	kolonie	k1gFnSc4	kolonie
-	-	kIx~	-
což	což	k3yQnSc4	což
Nietzsche	Nietzsche	k1gInSc1	Nietzsche
považoval	považovat	k5eAaImAgInS	považovat
za	za	k7c4	za
směšné	směšné	k1gNnSc4	směšné
<g/>
.	.	kIx.	.
</s>
<s>
Nietzsche	Nietzschat	k5eAaPmIp3nS	Nietzschat
ovšem	ovšem	k9	ovšem
stále	stále	k6eAd1	stále
zápasil	zápasit	k5eAaImAgInS	zápasit
s	s	k7c7	s
vracejícími	vracející	k2eAgFnPc7d1	vracející
se	se	k3xPyFc4	se
bolestivými	bolestivý	k2eAgFnPc7d1	bolestivá
záchvaty	záchvat	k1gInPc1	záchvat
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
znemožňovaly	znemožňovat	k5eAaImAgInP	znemožňovat
delší	dlouhý	k2eAgFnSc4d2	delší
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
krátkém	krátký	k2eAgInSc6d1	krátký
čase	čas	k1gInSc6	čas
napsal	napsat	k5eAaBmAgInS	napsat
roku	rok	k1gInSc2	rok
1887	[number]	k4	1887
polemický	polemický	k2eAgInSc1d1	polemický
spis	spis	k1gInSc1	spis
Genealogie	genealogie	k1gFnSc2	genealogie
morálky	morálka	k1gFnSc2	morálka
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1888	[number]	k4	1888
pět	pět	k4xCc4	pět
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
z	z	k7c2	z
obsáhlých	obsáhlý	k2eAgFnPc2d1	obsáhlá
poznámek	poznámka	k1gFnPc2	poznámka
k	k	k7c3	k
plánované	plánovaný	k2eAgFnSc3d1	plánovaná
práci	práce	k1gFnSc3	práce
Vůle	vůle	k1gFnSc2	vůle
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
<g/>
,	,	kIx,	,
a	a	k8xC	a
zdálo	zdát	k5eAaImAgNnS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gNnSc4	on
jeho	jeho	k3xOp3gNnSc4	jeho
nemoci	nemoc	k1gFnPc1	nemoc
opustily	opustit	k5eAaPmAgFnP	opustit
<g/>
;	;	kIx,	;
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
měl	mít	k5eAaImAgInS	mít
výbornou	výborný	k2eAgFnSc4d1	výborná
náladu	nálada	k1gFnSc4	nálada
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
podzimu	podzim	k1gInSc2	podzim
roku	rok	k1gInSc2	rok
1888	[number]	k4	1888
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
spisech	spis	k1gInPc6	spis
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
příznaky	příznak	k1gInPc1	příznak
narůstajícího	narůstající	k2eAgNnSc2d1	narůstající
megalomanství	megalomanství	k1gNnSc2	megalomanství
<g/>
;	;	kIx,	;
rostoucí	rostoucí	k2eAgInSc1d1	rostoucí
počet	počet	k1gInSc1	počet
reakcí	reakce	k1gFnPc2	reakce
na	na	k7c4	na
jeho	jeho	k3xOp3gInPc4	jeho
spisy	spis	k1gInPc4	spis
<g/>
,	,	kIx,	,
především	především	k9	především
na	na	k7c4	na
polemiku	polemika	k1gFnSc4	polemika
Případ	případ	k1gInSc1	případ
Wagner	Wagner	k1gMnSc1	Wagner
z	z	k7c2	z
jara	jaro	k1gNnSc2	jaro
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
příliš	příliš	k6eAd1	příliš
přeceňoval	přeceňovat	k5eAaImAgMnS	přeceňovat
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
svých	svůj	k3xOyFgNnPc2	svůj
44	[number]	k4	44
<g/>
.	.	kIx.	.
narozeninách	narozeniny	k1gFnPc6	narozeniny
se	se	k3xPyFc4	se
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
Soumraku	soumrak	k1gInSc2	soumrak
model	model	k1gInSc1	model
a	a	k8xC	a
zatím	zatím	k6eAd1	zatím
staženého	stažený	k2eAgMnSc4d1	stažený
Antikrista	Antikrist	k1gMnSc4	Antikrist
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
napsat	napsat	k5eAaPmF	napsat
autobiografii	autobiografie	k1gFnSc4	autobiografie
Ecce	Ecce	k1gNnSc2	Ecce
Homo	Homo	k6eAd1	Homo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
začala	začít	k5eAaPmAgFnS	začít
korespondence	korespondence	k1gFnSc1	korespondence
s	s	k7c7	s
Augustem	August	k1gMnSc7	August
Strindbergem	Strindberg	k1gMnSc7	Strindberg
a	a	k8xC	a
Nietzsche	Nietzsche	k1gNnSc7	Nietzsche
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
stojí	stát	k5eAaImIp3nS	stát
před	před	k7c7	před
mezinárodním	mezinárodní	k2eAgInSc7d1	mezinárodní
průlomem	průlom	k1gInSc7	průlom
<g/>
.	.	kIx.	.
</s>
<s>
Pokusil	pokusit	k5eAaPmAgMnS	pokusit
se	se	k3xPyFc4	se
odkoupit	odkoupit	k5eAaPmF	odkoupit
od	od	k7c2	od
svých	svůj	k3xOyFgMnPc2	svůj
prvních	první	k4xOgMnPc2	první
nakladatelů	nakladatel	k1gMnPc2	nakladatel
své	svůj	k3xOyFgFnSc2	svůj
staré	stará	k1gFnSc2	stará
spisy	spis	k1gInPc4	spis
a	a	k8xC	a
chtěl	chtít	k5eAaImAgMnS	chtít
prodávat	prodávat	k5eAaImF	prodávat
překlady	překlad	k1gInPc4	překlad
do	do	k7c2	do
nejdůležitějších	důležitý	k2eAgInPc2d3	nejdůležitější
evropských	evropský	k2eAgInPc2d1	evropský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
také	také	k9	také
plánoval	plánovat	k5eAaImAgMnS	plánovat
zveřejnění	zveřejnění	k1gNnSc4	zveřejnění
překladu	překlad	k1gInSc2	překlad
Nietzsche	Nietzsch	k1gMnSc2	Nietzsch
contra	contr	k1gMnSc2	contr
Wagner	Wagner	k1gMnSc1	Wagner
a	a	k8xC	a
básně	báseň	k1gFnPc1	báseň
Dionysos-Dithyramben	Dionysos-Dithyrambna	k1gFnPc2	Dionysos-Dithyrambna
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
února	únor	k1gInSc2	únor
1889	[number]	k4	1889
se	se	k3xPyFc4	se
Nietzsche	Nietzsche	k1gFnSc1	Nietzsche
v	v	k7c6	v
Turíně	Turín	k1gInSc6	Turín
duševně	duševně	k6eAd1	duševně
zhroutil	zhroutit	k5eAaPmAgMnS	zhroutit
<g/>
;	;	kIx,	;
lístky	lístek	k1gInPc1	lístek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
posílal	posílat	k5eAaImAgInS	posílat
blízkým	blízký	k2eAgMnPc3d1	blízký
přátelům	přítel	k1gMnPc3	přítel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
například	například	k6eAd1	například
Cosimě	Cosima	k1gFnSc3	Cosima
Wagnerové	Wagnerová	k1gFnSc2	Wagnerová
a	a	k8xC	a
Jacobu	Jacoba	k1gFnSc4	Jacoba
Burkhardtovi	Burkhardtův	k2eAgMnPc1d1	Burkhardtův
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
zjevně	zjevně	k6eAd1	zjevně
pomatené	pomatený	k2eAgFnPc1d1	pomatená
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
byl	být	k5eAaImAgMnS	být
odvezen	odvézt	k5eAaPmNgMnS	odvézt
do	do	k7c2	do
ústavu	ústav	k1gInSc2	ústav
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
a	a	k8xC	a
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
matky	matka	k1gFnSc2	matka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
žil	žít	k5eAaImAgMnS	žít
ve	v	k7c6	v
Výmaru	Výmar	k1gInSc6	Výmar
ve	v	k7c6	v
Vile	vila	k1gFnSc6	vila
Silberblick	Silberblick	k1gInSc1	Silberblick
(	(	kIx(	(
<g/>
Stříbrný	stříbrný	k2eAgInSc1d1	stříbrný
pohled	pohled	k1gInSc1	pohled
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
prodělal	prodělat	k5eAaPmAgMnS	prodělat
několik	několik	k4yIc1	několik
infarktů	infarkt	k1gInPc2	infarkt
a	a	k8xC	a
dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1900	[number]	k4	1900
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
zápal	zápal	k1gInSc4	zápal
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
příčinu	příčina	k1gFnSc4	příčina
jeho	jeho	k3xOp3gNnSc2	jeho
zhroucení	zhroucení	k1gNnSc2	zhroucení
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
pokládá	pokládat	k5eAaImIp3nS	pokládat
progresivní	progresivní	k2eAgFnSc1d1	progresivní
paralýza	paralýza	k1gFnSc1	paralýza
jako	jako	k8xS	jako
důsledek	důsledek	k1gInSc1	důsledek
syfilis	syfilis	k1gFnSc1	syfilis
<g/>
;	;	kIx,	;
diagnóza	diagnóza	k1gFnSc1	diagnóza
je	být	k5eAaImIp3nS	být
však	však	k9	však
nejistá	jistý	k2eNgFnSc1d1	nejistá
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
autoři	autor	k1gMnPc1	autor
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
pochybují	pochybovat	k5eAaImIp3nP	pochybovat
<g/>
.	.	kIx.	.
</s>
<s>
Nietzscheho	Nietzscheze	k6eAd1	Nietzscheze
emotivní	emotivní	k2eAgNnSc1d1	emotivní
a	a	k8xC	a
místy	místo	k1gNnPc7	místo
až	až	k6eAd1	až
vášnivá	vášnivý	k2eAgFnSc1d1	vášnivá
filosofie	filosofie	k1gFnSc1	filosofie
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
poznamenána	poznamenat	k5eAaPmNgFnS	poznamenat
jeho	jeho	k3xOp3gInSc7	jeho
životním	životní	k2eAgInSc7d1	životní
osudem	osud	k1gInSc7	osud
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
stále	stále	k6eAd1	stále
silnějším	silný	k2eAgInSc7d2	silnější
pocitem	pocit	k1gInSc7	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
on	on	k3xPp3gMnSc1	on
musí	muset	k5eAaImIp3nS	muset
něco	něco	k3yInSc4	něco
podniknout	podniknout	k5eAaPmF	podniknout
pro	pro	k7c4	pro
záchranu	záchrana	k1gFnSc4	záchrana
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
ohroženého	ohrožený	k2eAgMnSc4d1	ohrožený
"	"	kIx"	"
<g/>
nihilismem	nihilismus	k1gInSc7	nihilismus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
"	"	kIx"	"
<g/>
smrti	smrt	k1gFnSc6	smrt
Boha	bůh	k1gMnSc4	bůh
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
připravila	připravit	k5eAaPmAgFnS	připravit
západní	západní	k2eAgFnSc1d1	západní
tradice	tradice	k1gFnSc1	tradice
platónismu	platónismus	k1gInSc2	platónismus
a	a	k8xC	a
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
,	,	kIx,	,
žijeme	žít	k5eAaImIp1nP	žít
všichni	všechen	k3xTgMnPc1	všechen
v	v	k7c6	v
hluboké	hluboký	k2eAgFnSc6d1	hluboká
lži	lež	k1gFnSc6	lež
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
předstíráme	předstírat	k5eAaImIp1nP	předstírat
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
kdyby	kdyby	kYmCp3nS	kdyby
stále	stále	k6eAd1	stále
platila	platit	k5eAaImAgFnS	platit
křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
morálka	morálka	k1gFnSc1	morálka
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
v	v	k7c4	v
ni	on	k3xPp3gFnSc4	on
v	v	k7c6	v
hloubi	hloub	k1gFnSc6	hloub
duše	duše	k1gFnSc2	duše
nevěříme	věřit	k5eNaImIp1nP	věřit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zoufalství	zoufalství	k1gNnSc6	zoufalství
nad	nad	k7c7	nad
ztrátou	ztráta	k1gFnSc7	ztráta
životních	životní	k2eAgFnPc2d1	životní
hodnot	hodnota	k1gFnPc2	hodnota
se	se	k3xPyFc4	se
Nietzsche	Nietzsche	k1gInSc1	Nietzsche
nadchnul	nadchnout	k5eAaPmAgInS	nadchnout
romantickou	romantický	k2eAgFnSc7d1	romantická
představou	představa	k1gFnSc7	představa
"	"	kIx"	"
<g/>
řeckého	řecký	k2eAgMnSc2d1	řecký
ducha	duch	k1gMnSc2	duch
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
domníval	domnívat	k5eAaImAgMnS	domnívat
nalézt	nalézt	k5eAaBmF	nalézt
v	v	k7c6	v
antické	antický	k2eAgFnSc6d1	antická
tragedii	tragedie	k1gFnSc6	tragedie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
Dionýsos	Dionýsos	k1gMnSc1	Dionýsos
<g/>
,	,	kIx,	,
řecký	řecký	k2eAgMnSc1d1	řecký
bůh	bůh	k1gMnSc1	bůh
nespoutané	spoutaný	k2eNgFnSc2d1	nespoutaná
životní	životní	k2eAgFnSc2d1	životní
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pro	pro	k7c4	pro
něho	on	k3xPp3gMnSc4	on
pravdivě	pravdivě	k6eAd1	pravdivě
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
skutečné	skutečný	k2eAgNnSc4d1	skutečné
postavení	postavení	k1gNnSc4	postavení
člověka	člověk	k1gMnSc2	člověk
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
může	moct	k5eAaImIp3nS	moct
žít	žít	k5eAaImF	žít
z	z	k7c2	z
mnohoznačné	mnohoznačný	k2eAgFnSc2d1	mnohoznačná
krásy	krása	k1gFnSc2	krása
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
ovšem	ovšem	k9	ovšem
přijme	přijmout	k5eAaPmIp3nS	přijmout
život	život	k1gInSc4	život
jako	jako	k8xS	jako
nelítostný	lítostný	k2eNgInSc4d1	nelítostný
boj	boj	k1gInSc4	boj
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
nakonec	nakonec	k6eAd1	nakonec
musí	muset	k5eAaImIp3nS	muset
podlehnout	podlehnout	k5eAaPmF	podlehnout
"	"	kIx"	"
<g/>
temnotě	temnota	k1gFnSc3	temnota
<g/>
"	"	kIx"	"
smrti	smrt	k1gFnSc3	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
člověka	člověk	k1gMnSc2	člověk
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
může	moct	k5eAaImIp3nS	moct
hrdě	hrdě	k6eAd1	hrdě
vzdorovat	vzdorovat	k5eAaImF	vzdorovat
a	a	k8xC	a
bezohledně	bezohledně	k6eAd1	bezohledně
prosazovat	prosazovat	k5eAaImF	prosazovat
svoji	svůj	k3xOyFgFnSc4	svůj
"	"	kIx"	"
<g/>
vůli	vůle	k1gFnSc4	vůle
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
cílem	cíl	k1gInSc7	cíl
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
překonání	překonání	k1gNnSc3	překonání
člověka	člověk	k1gMnSc2	člověk
čili	čili	k8xC	čili
nadčlověk	nadčlověk	k1gMnSc1	nadčlověk
<g/>
.	.	kIx.	.
</s>
<s>
Počátek	počátek	k1gInSc1	počátek
zkázy	zkáza	k1gFnSc2	zkáza
evropského	evropský	k2eAgNnSc2d1	Evropské
lidství	lidství	k1gNnSc2	lidství
viděl	vidět	k5eAaImAgMnS	vidět
Nietzsche	Nietzsche	k1gFnPc7	Nietzsche
v	v	k7c6	v
jednoznačném	jednoznačný	k2eAgInSc6d1	jednoznačný
Platónově	Platónův	k2eAgInSc6d1	Platónův
resp.	resp.	kA	resp.
Sókratově	Sókratův	k2eAgNnSc6d1	Sókratovo
rozhodnutí	rozhodnutí	k1gNnSc6	rozhodnutí
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
slunečního	sluneční	k2eAgMnSc2d1	sluneční
boha	bůh	k1gMnSc2	bůh
Apollóna	Apollón	k1gMnSc2	Apollón
a	a	k8xC	a
lživé	lživý	k2eAgFnSc2d1	lživá
představy	představa	k1gFnSc2	představa
"	"	kIx"	"
<g/>
pravého	pravý	k2eAgInSc2d1	pravý
světa	svět	k1gInSc2	svět
<g/>
"	"	kIx"	"
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
od	od	k7c2	od
člověka	člověk	k1gMnSc2	člověk
naopak	naopak	k6eAd1	naopak
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
sebeomezení	sebeomezení	k1gNnSc4	sebeomezení
<g/>
,	,	kIx,	,
soucit	soucit	k1gInSc4	soucit
se	s	k7c7	s
slabšími	slabý	k2eAgMnPc7d2	slabší
a	a	k8xC	a
domnělou	domnělý	k2eAgFnSc4d1	domnělá
oddanost	oddanost	k1gFnSc4	oddanost
pravdě	pravda	k1gFnSc3	pravda
jako	jako	k8xS	jako
dennímu	denní	k2eAgNnSc3d1	denní
světlu	světlo	k1gNnSc3	světlo
rozumu	rozum	k1gInSc2	rozum
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
lživou	lživý	k2eAgFnSc4d1	lživá
orientaci	orientace	k1gFnSc4	orientace
na	na	k7c4	na
domnělé	domnělý	k2eAgNnSc4d1	domnělé
dobro	dobro	k1gNnSc4	dobro
a	a	k8xC	a
pravdu	pravda	k1gFnSc4	pravda
podle	podle	k7c2	podle
Nietscheho	Nietsche	k1gMnSc2	Nietsche
převzalo	převzít	k5eAaPmAgNnS	převzít
a	a	k8xC	a
mezi	mezi	k7c4	mezi
lid	lid	k1gInSc4	lid
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
křesťanství	křesťanství	k1gNnSc1	křesťanství
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
se	se	k3xPyFc4	se
tak	tak	k9	tak
stalo	stát	k5eAaPmAgNnS	stát
silou	síla	k1gFnSc7	síla
nepřátelskou	přátelský	k2eNgFnSc7d1	nepřátelská
životu	život	k1gInSc3	život
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
tisíciletého	tisíciletý	k2eAgNnSc2d1	tisícileté
působení	působení	k1gNnSc2	působení
tohoto	tento	k3xDgInSc2	tento
"	"	kIx"	"
<g/>
platónismu	platónismus	k1gInSc2	platónismus
pro	pro	k7c4	pro
lid	lid	k1gInSc4	lid
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
současný	současný	k2eAgInSc1d1	současný
nihilismus	nihilismus	k1gInSc1	nihilismus
-	-	kIx~	-
popírání	popírání	k1gNnSc1	popírání
sil	síla	k1gFnPc2	síla
života	život	k1gInSc2	život
pod	pod	k7c7	pod
záminkou	záminka	k1gFnSc7	záminka
morálky	morálka	k1gFnSc2	morálka
a	a	k8xC	a
pravdy	pravda	k1gFnSc2	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Základním	základní	k2eAgInSc7d1	základní
principem	princip	k1gInSc7	princip
Nietzscheho	Nietzsche	k1gMnSc2	Nietzsche
velmi	velmi	k6eAd1	velmi
nesystematického	systematický	k2eNgNnSc2d1	nesystematické
filosofování	filosofování	k1gNnSc2	filosofování
je	být	k5eAaImIp3nS	být
nelítostná	lítostný	k2eNgFnSc1d1	nelítostná
poctivost	poctivost	k1gFnSc1	poctivost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
si	se	k3xPyFc3	se
nezastírá	zastírat	k5eNaImIp3nS	zastírat
beznadějnost	beznadějnost	k1gFnSc4	beznadějnost
lidského	lidský	k2eAgInSc2d1	lidský
osudu	osud	k1gInSc2	osud
a	a	k8xC	a
"	"	kIx"	"
<g/>
překonává	překonávat	k5eAaImIp3nS	překonávat
<g/>
"	"	kIx"	"
ji	on	k3xPp3gFnSc4	on
statečným	statečný	k2eAgNnSc7d1	statečné
přijetím	přijetí	k1gNnSc7	přijetí
té	ten	k3xDgFnSc2	ten
"	"	kIx"	"
<g/>
nejtěžší	těžký	k2eAgFnSc2d3	nejtěžší
myšlenky	myšlenka	k1gFnSc2	myšlenka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
epikurejského	epikurejský	k2eAgInSc2d1	epikurejský
"	"	kIx"	"
<g/>
věčného	věčný	k2eAgInSc2d1	věčný
návratu	návrat	k1gInSc2	návrat
téhož	týž	k3xTgMnSc2	týž
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
jelikož	jelikož	k8xS	jelikož
svět	svět	k1gInSc1	svět
nemá	mít	k5eNaImIp3nS	mít
smysl	smysl	k1gInSc4	smysl
a	a	k8xC	a
jen	jen	k9	jen
věčně	věčně	k6eAd1	věčně
trvá	trvat	k5eAaImIp3nS	trvat
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
smířit	smířit	k5eAaPmF	smířit
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
všechno	všechen	k3xTgNnSc4	všechen
opakuje	opakovat	k5eAaImIp3nS	opakovat
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
vzepřít	vzepřít	k5eAaPmF	vzepřít
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
přitaká	přitakat	k5eAaPmIp3nS	přitakat
své	svůj	k3xOyFgFnSc3	svůj
vůli	vůle	k1gFnSc3	vůle
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
svoji	svůj	k3xOyFgFnSc4	svůj
tvořivost	tvořivost	k1gFnSc4	tvořivost
<g/>
,	,	kIx,	,
že	že	k8xS	že
tvoří	tvořit	k5eAaImIp3nP	tvořit
své	svůj	k3xOyFgFnPc4	svůj
vlastní	vlastní	k2eAgFnPc4d1	vlastní
hodnoty	hodnota	k1gFnPc4	hodnota
a	a	k8xC	a
tak	tak	k6eAd1	tak
připravuje	připravovat	k5eAaImIp3nS	připravovat
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
nadčlověku	nadčlověk	k1gMnSc3	nadčlověk
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
Nietzscheho	Nietzsche	k1gMnSc2	Nietzsche
titanismus	titanismus	k1gInSc4	titanismus
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
života	život	k1gInSc2	život
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c6	v
megalomanii	megalomanie	k1gFnSc6	megalomanie
<g/>
,	,	kIx,	,
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
o	o	k7c6	o
vlastním	vlastní	k2eAgNnSc6d1	vlastní
spasitelském	spasitelský	k2eAgNnSc6d1	spasitelské
poslání	poslání	k1gNnSc6	poslání
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
svých	svůj	k3xOyFgFnPc6	svůj
schopnostech	schopnost	k1gFnPc6	schopnost
i	i	k8xC	i
o	o	k7c6	o
svém	svůj	k3xOyFgNnSc6	svůj
poslání	poslání	k1gNnSc6	poslání
měl	mít	k5eAaImAgInS	mít
Nietzsche	Nietzsche	k1gInSc4	Nietzsche
zřetelné	zřetelný	k2eAgFnPc1d1	zřetelná
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
někdy	někdy	k6eAd1	někdy
přemrštěné	přemrštěný	k2eAgFnPc1d1	přemrštěná
představy	představa	k1gFnPc1	představa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c4	o
"	"	kIx"	"
<g/>
průlom	průlom	k1gInSc4	průlom
<g/>
"	"	kIx"	"
a	a	k8xC	a
čekal	čekat	k5eAaImAgMnS	čekat
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odezva	odezva	k1gFnSc1	odezva
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
myšlení	myšlení	k1gNnSc6	myšlení
začíná	začínat	k5eAaImIp3nS	začínat
právě	právě	k9	právě
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ji	on	k3xPp3gFnSc4	on
už	už	k9	už
nemohl	moct	k5eNaImAgMnS	moct
vnímat	vnímat	k5eAaImF	vnímat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
pozůstalosti	pozůstalost	k1gFnPc1	pozůstalost
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
ujal	ujmout	k5eAaPmAgMnS	ujmout
přítel	přítel	k1gMnSc1	přítel
Peter	Peter	k1gMnSc1	Peter
Gast	Gast	k1gMnSc1	Gast
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
však	však	k9	však
velmi	velmi	k6eAd1	velmi
brzy	brzy	k6eAd1	brzy
"	"	kIx"	"
<g/>
odstavila	odstavit	k5eAaPmAgFnS	odstavit
<g/>
"	"	kIx"	"
paní	paní	k1gFnSc1	paní
Elisabeth	Elisabeth	k1gInSc1	Elisabeth
Förster-Nietzscheová	Förster-Nietzscheová	k1gFnSc1	Förster-Nietzscheová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
si	se	k3xPyFc3	se
z	z	k7c2	z
bratrova	bratrův	k2eAgNnSc2d1	bratrovo
díla	dílo	k1gNnSc2	dílo
udělala	udělat	k5eAaPmAgFnS	udělat
monopol	monopol	k1gInSc4	monopol
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
do	do	k7c2	do
něho	on	k3xPp3gMnSc2	on
zasahovala	zasahovat	k5eAaImAgFnS	zasahovat
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnSc2	některý
části	část	k1gFnSc2	část
přepisovala	přepisovat	k5eAaImAgFnS	přepisovat
a	a	k8xC	a
jiné	jiná	k1gFnSc2	jiná
ničila	ničit	k5eAaImAgFnS	ničit
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
posmrtně	posmrtně	k6eAd1	posmrtně
vydaná	vydaný	k2eAgFnSc1d1	vydaná
kniha	kniha	k1gFnSc1	kniha
aforismů	aforismus	k1gInPc2	aforismus
Vůle	vůle	k1gFnSc2	vůle
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
)	)	kIx)	)
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
řadu	řada	k1gFnSc4	řada
jejích	její	k3xOp3gFnPc2	její
"	"	kIx"	"
<g/>
úprav	úprava	k1gFnPc2	úprava
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
byly	být	k5eAaImAgFnP	být
odhaleny	odhalit	k5eAaPmNgFnP	odhalit
až	až	k6eAd1	až
koncem	koncem	k7c2	koncem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Provedla	provést	k5eAaPmAgFnS	provést
také	také	k9	také
opisy	opis	k1gInPc1	opis
dopisů	dopis	k1gInPc2	dopis
svého	svůj	k3xOyFgMnSc2	svůj
bratra	bratr	k1gMnSc2	bratr
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Urabschriften	Urabschriften	k2eAgMnSc1d1	Urabschriften
(	(	kIx(	(
<g/>
prvotní	prvotní	k2eAgInPc4d1	prvotní
opisy	opis	k1gInPc4	opis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
texty	text	k1gInPc1	text
úmyslně	úmyslně	k6eAd1	úmyslně
krátila	krátit	k5eAaImAgFnS	krátit
<g/>
,	,	kIx,	,
opatřovala	opatřovat	k5eAaImAgFnS	opatřovat
dopisy	dopis	k1gInPc4	dopis
jiným	jiný	k2eAgNnSc7d1	jiné
datem	datum	k1gNnSc7	datum
a	a	k8xC	a
snažila	snažit	k5eAaImAgFnS	snažit
se	se	k3xPyFc4	se
vzbudit	vzbudit	k5eAaPmF	vzbudit
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
těchto	tento	k3xDgInPc2	tento
dopisů	dopis	k1gInPc2	dopis
byla	být	k5eAaImAgFnS	být
určena	určit	k5eAaPmNgFnS	určit
právě	právě	k9	právě
jen	jen	k9	jen
jí	on	k3xPp3gFnSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
vypršela	vypršet	k5eAaPmAgFnS	vypršet
autorská	autorský	k2eAgNnPc1d1	autorské
práva	právo	k1gNnPc4	právo
na	na	k7c4	na
knihy	kniha	k1gFnPc4	kniha
Fridricha	Fridrich	k1gMnSc2	Fridrich
Nietzscheho	Nietzsche	k1gMnSc2	Nietzsche
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nacionálněsocialistický	nacionálněsocialistický	k2eAgMnSc1d1	nacionálněsocialistický
ministr	ministr	k1gMnSc1	ministr
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Frick	Frick	k1gMnSc1	Frick
se	se	k3xPyFc4	se
postaral	postarat	k5eAaPmAgMnS	postarat
o	o	k7c4	o
další	další	k2eAgFnSc4d1	další
finanční	finanční	k2eAgFnSc4d1	finanční
podporu	podpora	k1gFnSc4	podpora
Elisabeth	Elisabetha	k1gFnPc2	Elisabetha
Förster-Nietzscheové	Förster-Nietzscheová	k1gFnSc2	Förster-Nietzscheová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
povolala	povolat	k5eAaPmAgFnS	povolat
Elisabeth	Elisabeth	k1gInSc4	Elisabeth
Förster-Nietzscheová	Förster-Nietzscheová	k1gFnSc1	Förster-Nietzscheová
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
Nietzscheho	Nietzsche	k1gMnSc2	Nietzsche
archivu	archiv	k1gInSc2	archiv
nacionálněsocialisticky	nacionálněsocialisticky	k6eAd1	nacionálněsocialisticky
smýšlejícího	smýšlející	k2eAgMnSc4d1	smýšlející
profesora	profesor	k1gMnSc4	profesor
filozofie	filozofie	k1gFnSc2	filozofie
práva	práv	k2eAgMnSc4d1	práv
Carla	Carl	k1gMnSc4	Carl
Augusta	August	k1gMnSc2	August
Emgeho	Emge	k1gMnSc2	Emge
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
ostatní	ostatní	k2eAgMnPc1d1	ostatní
spolupracovníci	spolupracovník	k1gMnPc1	spolupracovník
archivu	archiv	k1gInSc2	archiv
byli	být	k5eAaImAgMnP	být
nadšenými	nadšený	k2eAgMnPc7d1	nadšený
nacisty	nacista	k1gMnPc7	nacista
<g/>
.	.	kIx.	.
</s>
<s>
Nacisté	nacista	k1gMnPc1	nacista
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
nechávali	nechávat	k5eAaImAgMnP	nechávat
inspirovat	inspirovat	k5eAaBmF	inspirovat
některými	některý	k3yIgFnPc7	některý
myšlenkami	myšlenka	k1gFnPc7	myšlenka
Fridricha	Fridrich	k1gMnSc2	Fridrich
Nietzscheho	Nietzsche	k1gMnSc2	Nietzsche
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
je	on	k3xPp3gNnSc4	on
zneužívat	zneužívat	k5eAaImF	zneužívat
ke	k	k7c3	k
svým	svůj	k3xOyFgInPc3	svůj
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Kurt	Kurt	k1gMnSc1	Kurt
Tucholsky	Tucholsky	k1gMnSc1	Tucholsky
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nietzscheho	Nietzsche	k1gMnSc2	Nietzsche
archiv	archiv	k1gInSc1	archiv
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
kolem	kolem	k7c2	kolem
něho	on	k3xPp3gNnSc2	on
jsou	být	k5eAaImIp3nP	být
vinni	vinen	k2eAgMnPc1d1	vinen
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Nietzscheho	Nietzsche	k1gMnSc4	Nietzsche
světonázor	světonázor	k1gInSc4	světonázor
poskytli	poskytnout	k5eAaPmAgMnP	poskytnout
německým	německý	k2eAgMnPc3d1	německý
válečným	válečný	k2eAgMnPc3d1	válečný
štváčům	štváč	k1gMnPc3	štváč
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kteréžto	kteréžto	k?	kteréžto
interpretaci	interpretace	k1gFnSc4	interpretace
ovšem	ovšem	k9	ovšem
dopomohla	dopomoct	k5eAaPmAgFnS	dopomoct
vágnost	vágnost	k1gFnSc1	vágnost
myslitelových	myslitelův	k2eAgInPc2d1	myslitelův
textů	text	k1gInPc2	text
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
přislíbil	přislíbit	k5eAaPmAgMnS	přislíbit
během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
návštěvy	návštěva	k1gFnSc2	návštěva
Nietzscheho	Nietzsche	k1gMnSc2	Nietzsche
archivu	archiv	k1gInSc2	archiv
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
finanční	finanční	k2eAgInPc4d1	finanční
příspěvky	příspěvek	k1gInPc4	příspěvek
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
soukromých	soukromý	k2eAgInPc2d1	soukromý
zdrojů	zdroj	k1gInPc2	zdroj
a	a	k8xC	a
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
nařídil	nařídit	k5eAaPmAgMnS	nařídit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
archivu	archiv	k1gInSc2	archiv
byly	být	k5eAaImAgFnP	být
poskytnuty	poskytnout	k5eAaPmNgInP	poskytnout
i	i	k9	i
veřejné	veřejný	k2eAgInPc1d1	veřejný
finanční	finanční	k2eAgInPc1d1	finanční
prostředky	prostředek	k1gInPc1	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
Nietzsche	Nietzsche	k1gInSc1	Nietzsche
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
ocenění	ocenění	k1gNnSc2	ocenění
nedočkal	dočkat	k5eNaPmAgMnS	dočkat
<g/>
,	,	kIx,	,
jeho	on	k3xPp3gInSc4	on
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
myšlení	myšlení	k1gNnSc4	myšlení
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
mimořádný	mimořádný	k2eAgMnSc1d1	mimořádný
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
také	také	k9	také
mnohoznačný	mnohoznačný	k2eAgMnSc1d1	mnohoznačný
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
si	se	k3xPyFc3	se
autor	autor	k1gMnSc1	autor
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
"	"	kIx"	"
<g/>
prorazit	prorazit	k5eAaPmF	prorazit
<g/>
"	"	kIx"	"
liboval	libovat	k5eAaImAgMnS	libovat
ve	v	k7c6	v
vyhrocených	vyhrocený	k2eAgFnPc6d1	vyhrocená
<g/>
,	,	kIx,	,
provokativních	provokativní	k2eAgFnPc6d1	provokativní
formulacích	formulace	k1gFnPc6	formulace
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
aforistický	aforistický	k2eAgInSc1d1	aforistický
styl	styl	k1gInSc1	styl
umožňoval	umožňovat	k5eAaImAgInS	umožňovat
vytrhávání	vytrhávání	k1gNnSc4	vytrhávání
z	z	k7c2	z
kontextu	kontext	k1gInSc2	kontext
a	a	k8xC	a
případné	případný	k2eAgNnSc4d1	případné
zneužití	zneužití	k1gNnSc4	zneužití
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
Nietzsche	Nietzsche	k1gFnSc1	Nietzsche
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
antisemitům	antisemita	k1gMnPc3	antisemita
vysmívá	vysmívat	k5eAaImIp3nS	vysmívat
<g/>
,	,	kIx,	,
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
druhé	druhý	k4xOgFnPc1	druhý
se	se	k3xPyFc4	se
však	však	k9	však
u	u	k7c2	u
něho	on	k3xPp3gMnSc2	on
najdou	najít	k5eAaPmIp3nP	najít
pasáže	pasáž	k1gFnPc1	pasáž
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
někdy	někdy	k6eAd1	někdy
bývají	bývat	k5eAaImIp3nP	bývat
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
kritičnost	kritičnost	k1gFnSc4	kritičnost
k	k	k7c3	k
židovsko-křesťanské	židovskořesťanský	k2eAgFnSc3d1	židovsko-křesťanská
tradici	tradice	k1gFnSc3	tradice
interpretovány	interpretován	k2eAgFnPc1d1	interpretována
jako	jako	k8xC	jako
antisemitské	antisemitský	k2eAgFnPc1d1	antisemitská
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
jeho	jeho	k3xOp3gInPc4	jeho
výroky	výrok	k1gInPc4	výrok
nacionalistické	nacionalistický	k2eAgNnSc1d1	nacionalistické
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
antinacionalistické	antinacionalistický	k2eAgNnSc1d1	antinacionalistický
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Býti	být	k5eAaImF	být
dobrým	dobrý	k2eAgMnSc7d1	dobrý
Němcem	Němec	k1gMnSc7	Němec
znamená	znamenat	k5eAaImIp3nS	znamenat
odněmčit	odněmčit	k5eAaPmF	odněmčit
se	s	k7c7	s
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kurt	Kurt	k1gMnSc1	Kurt
Tucholsky	Tucholsky	k1gMnSc1	Tucholsky
již	již	k6eAd1	již
v	v	k7c6	v
r.	r.	kA	r.
1932	[number]	k4	1932
poukázal	poukázat	k5eAaPmAgMnS	poukázat
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
paradox	paradox	k1gInSc4	paradox
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Řekni	říct	k5eAaPmRp2nS	říct
mi	já	k3xPp1nSc3	já
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
potřebuješ	potřebovat	k5eAaImIp2nS	potřebovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
já	já	k3xPp1nSc1	já
ti	ty	k3xPp2nSc3	ty
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
obstarám	obstarat	k5eAaPmIp1nS	obstarat
citát	citát	k1gInSc1	citát
z	z	k7c2	z
Nietzscheho	Nietzsche	k1gMnSc2	Nietzsche
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc4d1	velké
oživení	oživení	k1gNnSc4	oživení
zájmu	zájem	k1gInSc2	zájem
o	o	k7c4	o
Nietzscheho	Nietzsche	k1gMnSc4	Nietzsche
přinesla	přinést	k5eAaPmAgFnS	přinést
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
hlavní	hlavní	k2eAgFnSc7d1	hlavní
inspirací	inspirace	k1gFnSc7	inspirace
tzv.	tzv.	kA	tzv.
postmoderního	postmoderní	k2eAgNnSc2d1	postmoderní
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Tomáš	Tomáš	k1gMnSc1	Tomáš
Halík	Halík	k1gMnSc1	Halík
si	se	k3xPyFc3	se
jej	on	k3xPp3gMnSc4	on
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
cení	cenit	k5eAaImIp3nS	cenit
jako	jako	k9	jako
myslitele	myslitel	k1gMnPc4	myslitel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
"	"	kIx"	"
<g/>
byl	být	k5eAaImAgMnS	být
vynikajícím	vynikající	k2eAgMnSc7d1	vynikající
demystifikátorem	demystifikátor	k1gMnSc7	demystifikátor
<g/>
,	,	kIx,	,
vášnivě	vášnivě	k6eAd1	vášnivě
strhávajícím	strhávající	k2eAgInSc7d1	strhávající
masky	maska	k1gFnPc1	maska
iluzí	iluze	k1gFnPc2	iluze
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
mistrem	mistr	k1gMnSc7	mistr
v	v	k7c6	v
ukazování	ukazování	k1gNnSc6	ukazování
věcí	věc	k1gFnPc2	věc
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
v	v	k7c6	v
objevování	objevování	k1gNnSc6	objevování
opaku	opak	k1gInSc2	opak
toho	ten	k3xDgInSc2	ten
<g/>
,	,	kIx,	,
čím	co	k3yQnSc7	co
se	se	k3xPyFc4	se
mnohé	mnohý	k2eAgFnPc1d1	mnohá
věci	věc	k1gFnPc1	věc
zdály	zdát	k5eAaImAgFnP	zdát
být	být	k5eAaImF	být
a	a	k8xC	a
zač	zač	k6eAd1	zač
se	se	k3xPyFc4	se
vydávaly	vydávat	k5eAaImAgInP	vydávat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
psychoanalýzu	psychoanalýza	k1gFnSc4	psychoanalýza
na	na	k7c4	na
Sigmunda	Sigmund	k1gMnSc4	Sigmund
Freuda	Freud	k1gMnSc2	Freud
na	na	k7c4	na
C.	C.	kA	C.
G.	G.	kA	G.
Junga	Jung	k1gMnSc2	Jung
na	na	k7c4	na
Ludvíka	Ludvík	k1gMnSc4	Ludvík
Binswangera	Binswanger	k1gMnSc4	Binswanger
Na	na	k7c4	na
filosofii	filosofie	k1gFnSc4	filosofie
<g/>
:	:	kIx,	:
na	na	k7c4	na
existencialismus	existencialismus	k1gInSc4	existencialismus
na	na	k7c6	na
fenomenologii	fenomenologie	k1gFnSc6	fenomenologie
<g/>
,	,	kIx,	,
především	především	k9	především
na	na	k7c4	na
Martina	Martin	k1gMnSc4	Martin
Heideggera	Heidegger	k1gMnSc4	Heidegger
na	na	k7c6	na
postmoderní	postmoderní	k2eAgFnSc6d1	postmoderní
filosofii	filosofie	k1gFnSc6	filosofie
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c4	na
Michela	Michel	k1gMnSc4	Michel
Foucaulta	Foucault	k1gMnSc4	Foucault
nebo	nebo	k8xC	nebo
Richarda	Richard	k1gMnSc4	Richard
Rortyho	Rorty	k1gMnSc4	Rorty
Na	na	k7c4	na
literaturu	literatura	k1gFnSc4	literatura
<g/>
:	:	kIx,	:
na	na	k7c4	na
Ladislava	Ladislav	k1gMnSc4	Ladislav
Klímu	Klíma	k1gMnSc4	Klíma
na	na	k7c4	na
Williama	William	k1gMnSc4	William
Gayleyho	Gayley	k1gMnSc4	Gayley
Simpsona	Simpson	k1gMnSc4	Simpson
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Which	Which	k1gMnSc1	Which
Way	Way	k1gMnSc1	Way
Western	Western	kA	Western
Man	Man	k1gMnSc1	Man
<g/>
?	?	kIx.	?
</s>
<s>
Na	na	k7c4	na
texty	text	k1gInPc4	text
hudby	hudba	k1gFnSc2	hudba
na	na	k7c4	na
Jima	Jim	k2eAgMnSc4d1	Jim
Morrisona	Morrison	k1gMnSc4	Morrison
na	na	k7c4	na
Marilyna	Marilyen	k2eAgMnSc4d1	Marilyen
Mansona	Manson	k1gMnSc4	Manson
na	na	k7c4	na
Maniaca	Maniacus	k1gMnSc4	Maniacus
Nietzscheho	Nietzsche	k1gMnSc2	Nietzsche
myšlení	myšlení	k1gNnSc1	myšlení
rozhodně	rozhodně	k6eAd1	rozhodně
není	být	k5eNaImIp3nS	být
systematické	systematický	k2eAgNnSc1d1	systematické
a	a	k8xC	a
lze	lze	k6eAd1	lze
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
najít	najít	k5eAaPmF	najít
množství	množství	k1gNnSc1	množství
rozporů	rozpor	k1gInPc2	rozpor
<g/>
;	;	kIx,	;
tak	tak	k9	tak
představa	představa	k1gFnSc1	představa
"	"	kIx"	"
<g/>
nadčlověka	nadčlověk	k1gMnSc2	nadčlověk
<g/>
"	"	kIx"	"
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
nějaký	nějaký	k3yIgInSc4	nějaký
vývoj	vývoj	k1gInSc4	vývoj
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
sporu	spor	k1gInSc6	spor
s	s	k7c7	s
"	"	kIx"	"
<g/>
věčným	věčný	k2eAgInSc7d1	věčný
návratem	návrat	k1gInSc7	návrat
téhož	týž	k3xTgInSc2	týž
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
poctivost	poctivost	k1gFnSc4	poctivost
se	se	k3xPyFc4	se
nutně	nutně	k6eAd1	nutně
opírá	opírat	k5eAaImIp3nS	opírat
o	o	k7c4	o
ideu	idea	k1gFnSc4	idea
pravdy	pravda	k1gFnSc2	pravda
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Nietzsche	Nietzsche	k1gFnSc4	Nietzsche
odmítá	odmítat	k5eAaImIp3nS	odmítat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
ničivé	ničivý	k2eAgFnSc6d1	ničivá
kritice	kritika	k1gFnSc6	kritika
všeho	všecek	k3xTgNnSc2	všecek
<g/>
,	,	kIx,	,
čeho	co	k3yInSc2	co
si	se	k3xPyFc3	se
jeho	jeho	k3xOp3gMnPc1	jeho
současníci	současník	k1gMnPc1	současník
vážili	vážit	k5eAaImAgMnP	vážit
a	a	k8xC	a
v	v	k7c4	v
co	co	k3yQnSc4	co
věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zřejmý	zřejmý	k2eAgInSc1d1	zřejmý
prvek	prvek	k1gInSc1	prvek
jakéhosi	jakýsi	k3yIgInSc2	jakýsi
myšlenkového	myšlenkový	k2eAgInSc2d1	myšlenkový
masochismu	masochismus	k1gInSc2	masochismus
<g/>
.	.	kIx.	.
</s>
<s>
Nietzscheho	Nietzscheze	k6eAd1	Nietzscheze
myšlení	myšlení	k1gNnSc4	myšlení
i	i	k8xC	i
spisy	spis	k1gInPc4	spis
jsou	být	k5eAaImIp3nP	být
pochopitelně	pochopitelně	k6eAd1	pochopitelně
silně	silně	k6eAd1	silně
ovlivněny	ovlivnit	k5eAaPmNgInP	ovlivnit
dobou	doba	k1gFnSc7	doba
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
žil	žít	k5eAaImAgMnS	žít
<g/>
:	:	kIx,	:
rozpadem	rozpad	k1gInSc7	rozpad
Hegelova	Hegelův	k2eAgInSc2d1	Hegelův
světa	svět	k1gInSc2	svět
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
sebeuskutečňování	sebeuskutečňování	k1gNnSc1	sebeuskutečňování
Ducha	duch	k1gMnSc2	duch
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
materialismem	materialismus	k1gInSc7	materialismus
mladohegelovců	mladohegelovec	k1gMnPc2	mladohegelovec
<g/>
,	,	kIx,	,
naivně	naivně	k6eAd1	naivně
jednoduchým	jednoduchý	k2eAgInSc7d1	jednoduchý
darwinismem	darwinismus	k1gInSc7	darwinismus
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
šířil	šířit	k5eAaImAgMnS	šířit
Ernest	Ernest	k1gMnSc1	Ernest
Haeckel	Haeckel	k1gMnSc1	Haeckel
<g/>
,	,	kIx,	,
i	i	k9	i
nepoctivostí	nepoctivost	k1gFnPc2	nepoctivost
wagnerovské	wagnerovský	k2eAgFnSc2d1	wagnerovská
předstírané	předstíraný	k2eAgFnSc2d1	předstíraná
"	"	kIx"	"
<g/>
velikosti	velikost	k1gFnSc2	velikost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc4	jeho
literární	literární	k2eAgInSc4d1	literární
styl	styl	k1gInSc4	styl
-	-	kIx~	-
Nietzsche	Nietzsche	k1gInSc1	Nietzsche
dlouho	dlouho	k6eAd1	dlouho
váhal	váhat	k5eAaImAgInS	váhat
mezi	mezi	k7c7	mezi
filosofií	filosofie	k1gFnSc7	filosofie
a	a	k8xC	a
literaturou	literatura	k1gFnSc7	literatura
-	-	kIx~	-
silně	silně	k6eAd1	silně
poplatný	poplatný	k2eAgInSc1d1	poplatný
romantismu	romantismus	k1gInSc2	romantismus
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Tak	tak	k9	tak
pravil	pravit	k5eAaImAgMnS	pravit
Zarathustra	Zarathustr	k1gMnSc4	Zarathustr
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nápadně	nápadně	k6eAd1	nápadně
podobá	podobat	k5eAaImIp3nS	podobat
jazyku	jazyk	k1gInSc3	jazyk
Lutherova	Lutherův	k2eAgInSc2d1	Lutherův
překladu	překlad	k1gInSc2	překlad
Bible	bible	k1gFnSc2	bible
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
Nietzscheho	Nietzsche	k1gMnSc2	Nietzsche
dílo	dílo	k1gNnSc1	dílo
dodnes	dodnes	k6eAd1	dodnes
velmi	velmi	k6eAd1	velmi
živé	živý	k2eAgNnSc1d1	živé
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
přinejmenším	přinejmenším	k6eAd1	přinejmenším
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
důvodů	důvod	k1gInPc2	důvod
<g/>
:	:	kIx,	:
Nietzscheho	Nietzsche	k1gMnSc2	Nietzsche
nelítostná	lítostný	k2eNgFnSc1d1	nelítostná
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
sebezničující	sebezničující	k2eAgFnSc4d1	sebezničující
"	"	kIx"	"
<g/>
poctivost	poctivost	k1gFnSc4	poctivost
<g/>
"	"	kIx"	"
skutečně	skutečně	k6eAd1	skutečně
ukázala	ukázat	k5eAaPmAgFnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
evropském	evropský	k2eAgInSc6d1	evropský
duchovním	duchovní	k2eAgInSc6d1	duchovní
životě	život	k1gInSc6	život
posledních	poslední	k2eAgNnPc2d1	poslední
staletí	staletí	k1gNnPc2	staletí
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
předstírání	předstírání	k1gNnPc2	předstírání
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
Nietzscheho	Nietzsche	k1gMnSc2	Nietzsche
řečeno	říct	k5eAaPmNgNnS	říct
"	"	kIx"	"
<g/>
model	model	k1gInSc1	model
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nietzsche	Nietzsche	k1gInSc1	Nietzsche
byl	být	k5eAaImAgInS	být
mimořádně	mimořádně	k6eAd1	mimořádně
citlivý	citlivý	k2eAgMnSc1d1	citlivý
člověk	člověk	k1gMnSc1	člověk
a	a	k8xC	a
viděl	vidět	k5eAaImAgMnS	vidět
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
jiní	jiný	k1gMnPc1	jiný
začali	začít	k5eAaPmAgMnP	začít
objevovat	objevovat	k5eAaImF	objevovat
až	až	k9	až
mnohem	mnohem	k6eAd1	mnohem
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gFnSc3	jeho
inspiraci	inspirace	k1gFnSc3	inspirace
<g/>
;	;	kIx,	;
sem	sem	k6eAd1	sem
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
psychoanalýza	psychoanalýza	k1gFnSc1	psychoanalýza
<g/>
,	,	kIx,	,
kritika	kritika	k1gFnSc1	kritika
moderního	moderní	k2eAgInSc2d1	moderní
státu	stát	k1gInSc2	stát
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
studeného	studený	k2eAgNnSc2d1	studené
monstra	monstrum	k1gNnSc2	monstrum
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
hrůza	hrůza	k1gFnSc1	hrůza
z	z	k7c2	z
přicházejícího	přicházející	k2eAgMnSc2d1	přicházející
"	"	kIx"	"
<g/>
nihilismu	nihilismus	k1gInSc2	nihilismus
<g/>
"	"	kIx"	"
apod.	apod.	kA	apod.
Když	když	k8xS	když
Nietzsche	Nietzsche	k1gFnSc1	Nietzsche
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
filosofuje	filosofovat	k5eAaImIp3nS	filosofovat
kladivem	kladivo	k1gNnSc7	kladivo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
chce	chtít	k5eAaImIp3nS	chtít
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
zkouší	zkoušet	k5eAaImIp3nP	zkoušet
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
v	v	k7c6	v
evropském	evropský	k2eAgNnSc6d1	Evropské
myšlenkovém	myšlenkový	k2eAgNnSc6d1	myšlenkové
dědictví	dědictví	k1gNnSc6	dědictví
tuto	tento	k3xDgFnSc4	tento
zkoušku	zkouška	k1gFnSc4	zkouška
přežije	přežít	k5eAaPmIp3nS	přežít
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
sto	sto	k4xCgNnSc4	sto
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
toho	ten	k3xDgMnSc4	ten
není	být	k5eNaImIp3nS	být
tak	tak	k6eAd1	tak
málo	málo	k6eAd1	málo
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
němu	on	k3xPp3gNnSc3	on
si	se	k3xPyFc3	se
však	však	k9	však
filosofové	filosof	k1gMnPc1	filosof
i	i	k8xC	i
kazatelé	kazatel	k1gMnPc1	kazatel
začali	začít	k5eAaPmAgMnP	začít
dávat	dávat	k5eAaImF	dávat
větší	veliký	k2eAgInSc4d2	veliký
pozor	pozor	k1gInSc4	pozor
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
říkají	říkat	k5eAaImIp3nP	říkat
a	a	k8xC	a
slibují	slibovat	k5eAaImIp3nP	slibovat
<g/>
.	.	kIx.	.
</s>
<s>
Ostrá	ostrý	k2eAgFnSc1d1	ostrá
kritika	kritika	k1gFnSc1	kritika
morálky	morálka	k1gFnSc2	morálka
jistě	jistě	k9	jistě
přispěla	přispět	k5eAaPmAgFnS	přispět
k	k	k7c3	k
mravnímu	mravní	k2eAgNnSc3d1	mravní
uvolnění	uvolnění	k1gNnSc3	uvolnění
<g/>
,	,	kIx,	,
znamenala	znamenat	k5eAaImAgFnS	znamenat
však	však	k9	však
také	také	k9	také
velké	velký	k2eAgNnSc4d1	velké
osvobození	osvobození	k1gNnSc4	osvobození
a	a	k8xC	a
vyčištění	vyčištění	k1gNnSc4	vyčištění
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
všechen	všechen	k3xTgInSc4	všechen
zdánlivý	zdánlivý	k2eAgInSc4d1	zdánlivý
materialismus	materialismus	k1gInSc4	materialismus
Nietzsche	Nietzsch	k1gFnSc2	Nietzsch
dává	dávat	k5eAaImIp3nS	dávat
jasně	jasně	k6eAd1	jasně
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
není	být	k5eNaImIp3nS	být
cílem	cíl	k1gInSc7	cíl
sám	sám	k3xTgInSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
jeho	jeho	k3xOp3gNnSc7	jeho
posláním	poslání	k1gNnSc7	poslání
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
překročit	překročit	k5eAaPmF	překročit
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Konečně	konečně	k6eAd1	konečně
Nietzsche	Nietzsche	k1gFnSc4	Nietzsche
také	také	k9	také
připomněl	připomnět	k5eAaPmAgMnS	připomnět
<g/>
,	,	kIx,	,
že	že	k8xS	že
život	život	k1gInSc1	život
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
povahy	povaha	k1gFnSc2	povaha
věci	věc	k1gFnSc2	věc
nebezpečný	bezpečný	k2eNgInSc4d1	nebezpečný
a	a	k8xC	a
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
vždycky	vždycky	k6eAd1	vždycky
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
odvahu	odvaha	k1gFnSc4	odvaha
a	a	k8xC	a
statečnost	statečnost	k1gFnSc4	statečnost
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yQnSc1	kdo
to	ten	k3xDgNnSc1	ten
odmítá	odmítat	k5eAaImIp3nS	odmítat
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
toho	ten	k3xDgMnSc4	ten
má	mít	k5eAaImIp3nS	mít
Nietzsche	Nietzsche	k1gFnSc4	Nietzsche
jen	jen	k9	jen
své	svůj	k3xOyFgNnSc4	svůj
"	"	kIx"	"
<g/>
pohrdání	pohrdání	k1gNnSc4	pohrdání
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Die	Die	k?	Die
Geburt	Geburt	k1gInSc1	Geburt
der	drát	k5eAaImRp2nS	drát
Tragödie	Tragödie	k1gFnSc2	Tragödie
aus	aus	k?	aus
dem	dem	k?	dem
Geiste	Geist	k1gMnSc5	Geist
der	drát	k5eAaImRp2nS	drát
Musik	musika	k1gFnPc2	musika
(	(	kIx(	(
<g/>
Zrození	zrození	k1gNnSc1	zrození
tragédie	tragédie	k1gFnSc2	tragédie
z	z	k7c2	z
ducha	duch	k1gMnSc2	duch
hudby	hudba	k1gFnSc2	hudba
<g/>
)	)	kIx)	)
1872	[number]	k4	1872
Unzeitgemässe	Unzeitgemässe	k1gFnSc2	Unzeitgemässe
betrachtungen	betrachtungen	k1gInSc1	betrachtungen
(	(	kIx(	(
<g/>
Nečasové	Nečasové	k2eAgFnPc1d1	Nečasové
úvahy	úvaha	k1gFnPc1	úvaha
<g/>
)	)	kIx)	)
1872	[number]	k4	1872
Menschliches	Menschlichesa	k1gFnPc2	Menschlichesa
<g/>
,	,	kIx,	,
Allzumenschliches	Allzumenschlichesa	k1gFnPc2	Allzumenschlichesa
(	(	kIx(	(
<g/>
Lidské	lidský	k2eAgInPc1d1	lidský
<g/>
,	,	kIx,	,
příliš	příliš	k6eAd1	příliš
lidské	lidský	k2eAgNnSc1d1	lidské
<g/>
)	)	kIx)	)
1878	[number]	k4	1878
Morgenröte	Morgenröt	k1gInSc5	Morgenröt
(	(	kIx(	(
<g/>
Ranní	ranní	k2eAgInPc1d1	ranní
červánky	červánek	k1gInPc1	červánek
<g/>
)	)	kIx)	)
1881	[number]	k4	1881
<g />
.	.	kIx.	.
</s>
<s>
Die	Die	k?	Die
fröhliche	fröhlichat	k5eAaPmIp3nS	fröhlichat
Wissenschaft	Wissenschaft	k1gInSc1	Wissenschaft
(	(	kIx(	(
<g/>
Radostná	radostný	k2eAgFnSc1d1	radostná
věda	věda	k1gFnSc1	věda
<g/>
)	)	kIx)	)
1882	[number]	k4	1882
Also	Also	k1gNnSc1	Also
sprach	spracha	k1gFnPc2	spracha
Zarathustra	Zarathustrum	k1gNnSc2	Zarathustrum
(	(	kIx(	(
<g/>
Tak	tak	k6eAd1	tak
pravil	pravit	k5eAaImAgMnS	pravit
Zarathustra	Zarathustr	k1gMnSc4	Zarathustr
<g/>
)	)	kIx)	)
1885	[number]	k4	1885
Jenseits	Jenseits	k1gInSc1	Jenseits
von	von	k1gInSc4	von
Gut	Gut	k1gFnSc2	Gut
und	und	k?	und
Böse	Böse	k1gInSc1	Böse
(	(	kIx(	(
<g/>
Mimo	mimo	k7c4	mimo
dobro	dobro	k1gNnSc4	dobro
a	a	k8xC	a
zlo	zlo	k1gNnSc4	zlo
<g/>
)	)	kIx)	)
1886	[number]	k4	1886
Zur	Zur	k1gFnSc1	Zur
Genealogie	genealogie	k1gFnSc1	genealogie
der	drát	k5eAaImRp2nS	drát
Moral	Moral	k1gInSc1	Moral
(	(	kIx(	(
<g/>
Genealogie	genealogie	k1gFnSc1	genealogie
morálky	morálka	k1gFnSc2	morálka
<g/>
)	)	kIx)	)
1887	[number]	k4	1887
Der	drát	k5eAaImRp2nS	drát
Fall	Fall	k1gMnSc1	Fall
Wagner	Wagner	k1gMnSc1	Wagner
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Případ	případ	k1gInSc1	případ
Wagner	Wagner	k1gMnSc1	Wagner
<g/>
)	)	kIx)	)
1888	[number]	k4	1888
Götzen-Dämmnerung	Götzen-Dämmnerung	k1gInSc1	Götzen-Dämmnerung
(	(	kIx(	(
<g/>
Soumrak	soumrak	k1gInSc1	soumrak
model	model	k1gInSc1	model
aneb	aneb	k?	aneb
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
filosofuje	filosofovat	k5eAaImIp3nS	filosofovat
kladivem	kladivo	k1gNnSc7	kladivo
<g/>
)	)	kIx)	)
1889	[number]	k4	1889
Nietzsche	Nietzsche	k1gNnSc2	Nietzsche
contra	contrum	k1gNnSc2	contrum
Wagner	Wagner	k1gMnSc1	Wagner
1889	[number]	k4	1889
Dionysos-Dithyramben	Dionysos-Dithyrambna	k1gFnPc2	Dionysos-Dithyrambna
1889	[number]	k4	1889
Der	drát	k5eAaImRp2nS	drát
Antichrist	Antichrist	k1gMnSc1	Antichrist
(	(	kIx(	(
<g/>
Antikrist	Antikrist	k1gMnSc1	Antikrist
<g/>
)	)	kIx)	)
1895	[number]	k4	1895
Ecce	Ecc	k1gInSc2	Ecc
Homo	Homo	k6eAd1	Homo
1898	[number]	k4	1898
-	-	kIx~	-
Pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
autobiografii	autobiografie	k1gFnSc4	autobiografie
<g/>
,	,	kIx,	,
titul	titul	k1gInSc4	titul
je	být	k5eAaImIp3nS	být
narážka	narážka	k1gFnSc1	narážka
na	na	k7c4	na
slova	slovo	k1gNnPc4	slovo
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgInPc7	jenž
Pontius	Pontius	k1gMnSc1	Pontius
Pilatus	Pilatus	k1gMnSc1	Pilatus
představoval	představovat	k5eAaImAgMnS	představovat
davu	dav	k1gInSc3	dav
zbičovaného	zbičovaný	k2eAgMnSc2d1	zbičovaný
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Kritické	kritický	k2eAgNnSc1d1	kritické
vydání	vydání	k1gNnSc1	vydání
Nietzscheova	Nietzscheův	k2eAgNnSc2d1	Nietzscheovo
díla	dílo	k1gNnSc2	dílo
začalo	začít	k5eAaPmAgNnS	začít
vycházet	vycházet	k5eAaImF	vycházet
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
<s>
Vydavatelé	vydavatel	k1gMnPc1	vydavatel
Coli	Coli	k1gNnSc2	Coli
a	a	k8xC	a
Montinari	Montinar	k1gMnPc1	Montinar
je	on	k3xPp3gMnPc4	on
rozdělili	rozdělit	k5eAaPmAgMnP	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
řad	řada	k1gFnPc2	řada
<g/>
,	,	kIx,	,
Dílo	dílo	k1gNnSc1	dílo
(	(	kIx(	(
<g/>
KGW	KGW	kA	KGW
<g/>
,	,	kIx,	,
18	[number]	k4	18
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
a	a	k8xC	a
Dopisy	dopis	k1gInPc1	dopis
(	(	kIx(	(
<g/>
KGB	KGB	kA	KGB
<g/>
,	,	kIx,	,
23	[number]	k4	23
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
kratší	krátký	k2eAgNnSc1d2	kratší
studijní	studijní	k2eAgNnSc1d1	studijní
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
zkratkou	zkratka	k1gFnSc7	zkratka
KSA	KSA	kA	KSA
a	a	k8xC	a
KSB	KSB	kA	KSB
<g/>
.	.	kIx.	.
</s>
<s>
Sebrané	sebraný	k2eAgInPc4d1	sebraný
spisy	spis	k1gInPc4	spis
-	-	kIx~	-
úplné	úplný	k2eAgFnPc4d1	úplná
<g/>
,	,	kIx,	,
bohatě	bohatě	k6eAd1	bohatě
komentované	komentovaný	k2eAgNnSc1d1	komentované
vydání	vydání	k1gNnSc1	vydání
<g/>
:	:	kIx,	:
Werke	Werke	k1gNnSc1	Werke
<g/>
.	.	kIx.	.
</s>
<s>
Kritische	Kritischus	k1gMnSc5	Kritischus
Gesamtausgabe	Gesamtausgab	k1gMnSc5	Gesamtausgab
<g/>
,	,	kIx,	,
zkratka	zkratka	k1gFnSc1	zkratka
<g/>
:	:	kIx,	:
KGW	KGW	kA	KGW
(	(	kIx(	(
<g/>
také	také	k9	také
KGA	KGA	kA	KGA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Giorgio	Giorgio	k6eAd1	Giorgio
Colli	Colle	k1gFnSc4	Colle
a	a	k8xC	a
Mazzino	Mazzin	k2eAgNnSc4d1	Mazzin
Montinari	Montinari	k1gNnSc4	Montinari
<g/>
.	.	kIx.	.
</s>
<s>
Berlin	berlina	k1gFnPc2	berlina
a	a	k8xC	a
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
1967	[number]	k4	1967
<g/>
nn	nn	k?	nn
<g/>
.	.	kIx.	.
</s>
<s>
Briefe	Brief	k1gMnSc5	Brief
<g/>
.	.	kIx.	.
</s>
<s>
Kritische	Kritischus	k1gMnSc5	Kritischus
Gesamtausgabe	Gesamtausgab	k1gMnSc5	Gesamtausgab
<g/>
,	,	kIx,	,
zkratka	zkratka	k1gFnSc1	zkratka
<g/>
:	:	kIx,	:
KGB	KGB	kA	KGB
<g/>
,	,	kIx,	,
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Giorgio	Giorgio	k6eAd1	Giorgio
Colli	Colle	k1gFnSc4	Colle
a	a	k8xC	a
Mazzino	Mazzin	k2eAgNnSc4d1	Mazzin
Montinari	Montinari	k1gNnSc4	Montinari
<g/>
.	.	kIx.	.
</s>
<s>
Berlin	berlina	k1gFnPc2	berlina
a	a	k8xC	a
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
1975	[number]	k4	1975
<g/>
-	-	kIx~	-
<g/>
2004	[number]	k4	2004
Studijní	studijní	k2eAgInSc4d1	studijní
vydání	vydání	k1gNnSc1	vydání
-	-	kIx~	-
paperback	paperback	k1gInSc1	paperback
<g/>
:	:	kIx,	:
Sämtliche	Sämtliche	k1gInSc1	Sämtliche
Werke	Werk	k1gInSc2	Werk
<g/>
.	.	kIx.	.
</s>
<s>
Kritische	Kritischus	k1gMnSc5	Kritischus
Studienausgabe	Studienausgab	k1gMnSc5	Studienausgab
in	in	k?	in
15	[number]	k4	15
Bänden	Bändna	k1gFnPc2	Bändna
<g/>
,	,	kIx,	,
zkratka	zkratka	k1gFnSc1	zkratka
<g/>
:	:	kIx,	:
KSA	KSA	kA	KSA
<g/>
,	,	kIx,	,
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Giorgio	Giorgio	k6eAd1	Giorgio
Colli	Colle	k1gFnSc4	Colle
a	a	k8xC	a
Mazzino	Mazzin	k2eAgNnSc4d1	Mazzin
Montinari	Montinari	k1gNnSc4	Montinari
<g/>
.	.	kIx.	.
</s>
<s>
München	München	k1gInSc1	München
a	a	k8xC	a
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
3-423-59065-3	[number]	k4	3-423-59065-3
Sämtliche	Sämtlichus	k1gMnSc5	Sämtlichus
Briefe	Brief	k1gMnSc5	Brief
<g/>
.	.	kIx.	.
</s>
<s>
Kritische	Kritischus	k1gMnSc5	Kritischus
Studienausgabe	Studienausgab	k1gMnSc5	Studienausgab
<g/>
,	,	kIx,	,
zkratka	zkratka	k1gFnSc1	zkratka
<g/>
:	:	kIx,	:
KSB	KSB	kA	KSB
<g/>
,	,	kIx,	,
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Giorgio	Giorgio	k6eAd1	Giorgio
Colli	Colle	k1gFnSc4	Colle
a	a	k8xC	a
Mazzino	Mazzin	k2eAgNnSc4d1	Mazzin
Montinari	Montinari	k1gNnSc4	Montinari
<g/>
.	.	kIx.	.
</s>
<s>
München	München	k1gInSc1	München
a	a	k8xC	a
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
3-423-59063-7	[number]	k4	3-423-59063-7
Nietzschesource	Nietzschesourka	k1gFnSc6	Nietzschesourka
-	-	kIx~	-
úplné	úplný	k2eAgNnSc1d1	úplné
vydání	vydání	k1gNnSc1	vydání
Coli-Montinari	Coli-Montinar	k1gFnSc2	Coli-Montinar
s	s	k7c7	s
vyhledávačem	vyhledávač	k1gMnSc7	vyhledávač
Unzeitgemässe	Unzeitgemässe	k1gFnSc2	Unzeitgemässe
Betrachtungen	Betrachtungen	k1gInSc1	Betrachtungen
<g/>
,	,	kIx,	,
1873	[number]	k4	1873
<g/>
-	-	kIx~	-
<g/>
1876	[number]	k4	1876
KSA	KSA	kA	KSA
1	[number]	k4	1
und	und	k?	und
2	[number]	k4	2
David	David	k1gMnSc1	David
Strauß	Strauß	k1gMnSc1	Strauß
<g/>
,	,	kIx,	,
der	drát	k5eAaImRp2nS	drát
Bekenner	Bekenner	k1gInSc1	Bekenner
und	und	k?	und
der	drát	k5eAaImRp2nS	drát
Schriftsteller	Schriftsteller	k1gMnSc1	Schriftsteller
<g/>
,	,	kIx,	,
1873	[number]	k4	1873
Vom	Vom	k1gFnPc2	Vom
Nutzen	Nutzna	k1gFnPc2	Nutzna
und	und	k?	und
Nachteil	Nachteil	k1gMnSc1	Nachteil
der	drát	k5eAaImRp2nS	drát
Historie	historie	k1gFnSc2	historie
für	für	k?	für
das	das	k?	das
Leben	Lebna	k1gFnPc2	Lebna
<g/>
,	,	kIx,	,
1874	[number]	k4	1874
Schopenhauer	Schopenhaura	k1gFnPc2	Schopenhaura
als	als	k?	als
Erzieher	Erziehra	k1gFnPc2	Erziehra
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
1874	[number]	k4	1874
Richard	Richard	k1gMnSc1	Richard
Wagner	Wagner	k1gMnSc1	Wagner
in	in	k?	in
Bayreuth	Bayreuth	k1gMnSc1	Bayreuth
<g/>
,	,	kIx,	,
1876	[number]	k4	1876
Menschliches	Menschlichesa	k1gFnPc2	Menschlichesa
<g/>
,	,	kIx,	,
Allzumenschliches	Allzumenschlichesa	k1gFnPc2	Allzumenschlichesa
-	-	kIx~	-
Ein	Ein	k1gFnSc1	Ein
Buch	buch	k1gInSc1	buch
für	für	k?	für
freie	freie	k1gFnSc2	freie
Geister	Geistra	k1gFnPc2	Geistra
(	(	kIx(	(
<g/>
mit	mit	k?	mit
zwei	zwei	k1gNnPc2	zwei
Fortsetzungen	Fortsetzungen	k1gInSc1	Fortsetzungen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1878	[number]	k4	1878
<g/>
-	-	kIx~	-
<g/>
1880	[number]	k4	1880
KSA	KSA	kA	KSA
2	[number]	k4	2
Morgenröte	Morgenröt	k1gInSc5	Morgenröt
-	-	kIx~	-
Gedanken	Gedanken	k2eAgInSc1d1	Gedanken
über	über	k1gInSc1	über
die	die	k?	die
moralischen	moralischen	k1gInSc1	moralischen
Vorurteile	Vorurteila	k1gFnSc3	Vorurteila
<g/>
,	,	kIx,	,
1881	[number]	k4	1881
KSA	KSA	kA	KSA
3	[number]	k4	3
Idyllen	Idyllen	k2eAgInSc1d1	Idyllen
aus	aus	k?	aus
Messina	Messina	k1gFnSc1	Messina
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
1882	[number]	k4	1882
KSA	KSA	kA	KSA
3	[number]	k4	3
Die	Die	k1gFnSc2	Die
fröhliche	fröhliche	k1gFnSc1	fröhliche
Wissenschaft	Wissenschaft	k1gMnSc1	Wissenschaft
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
la	la	k1gNnPc1	la
gaya	gaya	k?	gaya
scienza	scienz	k1gMnSc2	scienz
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
,	,	kIx,	,
1882	[number]	k4	1882
KSA	KSA	kA	KSA
3	[number]	k4	3
Also	Also	k1gMnSc1	Also
sprach	sprach	k1gMnSc1	sprach
Zarathustra	Zarathustra	k1gFnSc1	Zarathustra
-	-	kIx~	-
Ein	Ein	k1gFnSc1	Ein
Buch	buch	k1gInSc1	buch
für	für	k?	für
Alle	Alle	k1gInSc1	Alle
und	und	k?	und
Keinen	Keinen	k1gInSc1	Keinen
<g/>
,	,	kIx,	,
1883	[number]	k4	1883
<g/>
-	-	kIx~	-
<g/>
1885	[number]	k4	1885
KSA	KSA	kA	KSA
4	[number]	k4	4
Jenseits	Jenseits	k1gInSc1	Jenseits
von	von	k1gInSc4	von
Gut	Gut	k1gFnSc2	Gut
und	und	k?	und
Böse	Bös	k1gFnSc2	Bös
-	-	kIx~	-
Vorspiel	Vorspiel	k1gMnSc1	Vorspiel
einer	einer	k1gMnSc1	einer
<g />
.	.	kIx.	.
</s>
<s>
Philosophie	Philosophie	k1gFnSc1	Philosophie
der	drát	k5eAaImRp2nS	drát
Zukunft	Zukunft	k1gMnSc1	Zukunft
<g/>
,	,	kIx,	,
1886	[number]	k4	1886
KSA	KSA	kA	KSA
5	[number]	k4	5
Zur	Zur	k1gFnSc2	Zur
Genealogie	genealogie	k1gFnSc2	genealogie
der	drát	k5eAaImRp2nS	drát
Moral	Moral	k1gInSc1	Moral
-	-	kIx~	-
Eine	Eine	k1gInSc1	Eine
Streitschrift	Streitschrifta	k1gFnPc2	Streitschrifta
<g/>
,	,	kIx,	,
1887	[number]	k4	1887
KSA	KSA	kA	KSA
5	[number]	k4	5
Der	drát	k5eAaImRp2nS	drát
Fall	Fall	k1gMnSc1	Fall
Wagner	Wagner	k1gMnSc1	Wagner
-	-	kIx~	-
Ein	Ein	k1gMnSc7	Ein
Musikanten-Problem	Musikanten-Probl	k1gMnSc7	Musikanten-Probl
<g/>
,	,	kIx,	,
1888	[number]	k4	1888
KSA	KSA	kA	KSA
6	[number]	k4	6
Götzen-Dämmerung	Götzen-Dämmerunga	k1gFnPc2	Götzen-Dämmerunga
oder	odra	k1gFnPc2	odra
Wie	Wie	k1gFnSc2	Wie
man	man	k1gMnSc1	man
mit	mit	k?	mit
dem	dem	k?	dem
Hammer	Hammer	k1gMnSc1	Hammer
philosophiert	philosophiert	k1gMnSc1	philosophiert
<g/>
,	,	kIx,	,
1889	[number]	k4	1889
KSA	KSA	kA	KSA
6	[number]	k4	6
Der	drát	k5eAaImRp2nS	drát
Antichrist	Antichrist	k1gMnSc1	Antichrist
-	-	kIx~	-
Fluch	Fluch	k1gMnSc1	Fluch
auf	auf	k?	auf
das	das	k?	das
Christentum	Christentum	k1gNnSc1	Christentum
<g/>
,	,	kIx,	,
1895	[number]	k4	1895
KSA	KSA	kA	KSA
6	[number]	k4	6
Nietzsche	Nietzsche	k1gNnSc2	Nietzsche
contra	contrum	k1gNnSc2	contrum
Wagner	Wagner	k1gMnSc1	Wagner
<g/>
,	,	kIx,	,
1895	[number]	k4	1895
KSA	KSA	kA	KSA
6	[number]	k4	6
Ecce	Ecc	k1gInSc2	Ecc
Homo	Homo	k1gMnSc1	Homo
-	-	kIx~	-
Wie	Wie	k1gMnSc1	Wie
man	man	k1gMnSc1	man
wird	wird	k1gMnSc1	wird
<g/>
,	,	kIx,	,
was	was	k?	was
man	man	k1gMnSc1	man
ist	ist	k?	ist
<g/>
,	,	kIx,	,
1908	[number]	k4	1908
KSA	KSA	kA	KSA
6	[number]	k4	6
</s>
