<p>
<s>
Čeleď	čeleď	k1gFnSc1	čeleď
Stenostiridae	Stenostirida	k1gInSc2	Stenostirida
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
čeleď	čeleď	k1gFnSc1	čeleď
zpěvných	zpěvný	k2eAgMnPc2d1	zpěvný
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgFnPc1d1	zahrnující
několik	několik	k4yIc4	několik
afrických	africký	k2eAgInPc2d1	africký
a	a	k8xC	a
asijských	asijský	k2eAgInPc2d1	asijský
"	"	kIx"	"
<g/>
lejsků	lejsek	k1gMnPc2	lejsek
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
řazených	řazený	k2eAgFnPc2d1	řazená
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
různých	různý	k2eAgFnPc2d1	různá
nadčeledí	nadčeleď	k1gFnPc2	nadčeleď
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
tvoří	tvořit	k5eAaImIp3nS	tvořit
tuto	tento	k3xDgFnSc4	tento
čeleď	čeleď	k1gFnSc4	čeleď
9	[number]	k4	9
druhů	druh	k1gInPc2	druh
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
rodech	rod	k1gInPc6	rod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fylogeneze	fylogeneze	k1gFnSc1	fylogeneze
a	a	k8xC	a
taxonomie	taxonomie	k1gFnSc1	taxonomie
==	==	k?	==
</s>
</p>
<p>
<s>
Čeleď	čeleď	k1gFnSc1	čeleď
Stenostiridae	Stenostirida	k1gFnSc2	Stenostirida
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
malého	malý	k2eAgInSc2d1	malý
kladu	klad	k1gInSc2	klad
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
nadčeledi	nadčeleď	k1gFnSc2	nadčeleď
Sylvioidea	Sylvioideum	k1gNnSc2	Sylvioideum
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yRgInSc2	který
dále	daleko	k6eAd2	daleko
patří	patřit	k5eAaImIp3nS	patřit
čeledi	čeleď	k1gFnSc2	čeleď
moudivláčkovití	moudivláčkovitý	k2eAgMnPc1d1	moudivláčkovitý
(	(	kIx(	(
<g/>
Remizidae	Remizidae	k1gNnSc7	Remizidae
<g/>
)	)	kIx)	)
a	a	k8xC	a
sýkorovití	sýkorovitý	k2eAgMnPc1d1	sýkorovitý
(	(	kIx(	(
<g/>
Paridae	Paridae	k1gNnSc7	Paridae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
výzkumů	výzkum	k1gInPc2	výzkum
jsou	být	k5eAaImIp3nP	být
sesterským	sesterský	k2eAgInSc7d1	sesterský
kladem	klad	k1gInSc7	klad
všech	všecek	k3xTgFnPc2	všecek
ostatních	ostatní	k2eAgFnPc2d1	ostatní
skupin	skupina	k1gFnPc2	skupina
Sylvioidea	Sylvioide	k1gInSc2	Sylvioide
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
dokonce	dokonce	k9	dokonce
jako	jako	k9	jako
samostatná	samostatný	k2eAgFnSc1d1	samostatná
nadčeleď	nadčeleď	k1gFnSc1	nadčeleď
Paroidea	Paroidea	k1gFnSc1	Paroidea
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čeleď	čeleď	k1gFnSc1	čeleď
Stenostiridae	Stenostiridae	k1gNnSc2	Stenostiridae
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
tři	tři	k4xCgInPc4	tři
rody	rod	k1gInPc4	rod
<g/>
,	,	kIx,	,
řazené	řazený	k2eAgInPc4d1	řazený
původně	původně	k6eAd1	původně
do	do	k7c2	do
jiných	jiný	k2eAgFnPc2d1	jiná
čeledí	čeleď	k1gFnPc2	čeleď
a	a	k8xC	a
nadčeledí	nadčeleď	k1gFnPc2	nadčeleď
–	–	k?	–
rod	rod	k1gInSc1	rod
Stenostira	Stenostir	k1gMnSc2	Stenostir
mezi	mezi	k7c4	mezi
pěnicovité	pěnicovitý	k2eAgMnPc4d1	pěnicovitý
(	(	kIx(	(
<g/>
Sylviidae	Sylviidae	k1gInSc1	Sylviidae
<g/>
,	,	kIx,	,
Sylvioidea	Sylvioidea	k1gFnSc1	Sylvioidea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rod	rod	k1gInSc1	rod
Elminia	Elminium	k1gNnSc2	Elminium
mezi	mezi	k7c4	mezi
lejskovcovité	lejskovcovitý	k2eAgInPc4d1	lejskovcovitý
(	(	kIx(	(
<g/>
Monarchidae	Monarchida	k1gInPc4	Monarchida
<g/>
,	,	kIx,	,
Corvoidea	Corvoidea	k1gFnSc1	Corvoidea
<g/>
)	)	kIx)	)
a	a	k8xC	a
rod	rod	k1gInSc1	rod
Culicicapa	Culicicap	k1gMnSc2	Culicicap
mezi	mezi	k7c4	mezi
lejskovité	lejskovitý	k2eAgMnPc4d1	lejskovitý
(	(	kIx(	(
<g/>
Muscicapidae	Muscicapidae	k1gInSc1	Muscicapidae
<g/>
,	,	kIx,	,
Muscicapoidea	Muscicapoidea	k1gFnSc1	Muscicapoidea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc1	tři
druhy	druh	k1gInPc1	druh
rodu	rod	k1gInSc2	rod
Elminia	Elminium	k1gNnSc2	Elminium
přitom	přitom	k6eAd1	přitom
byly	být	k5eAaImAgFnP	být
řazeny	řadit	k5eAaImNgFnP	řadit
do	do	k7c2	do
samostatného	samostatný	k2eAgInSc2d1	samostatný
rodu	rod	k1gInSc2	rod
Trochocercus	Trochocercus	k1gMnSc1	Trochocercus
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
novějších	nový	k2eAgFnPc6d2	novější
analýzách	analýza	k1gFnPc6	analýza
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
druhy	druh	k1gInPc4	druh
této	tento	k3xDgFnSc2	tento
nové	nový	k2eAgFnSc2d1	nová
čeledi	čeleď	k1gFnSc2	čeleď
jen	jen	k9	jen
vzdáleně	vzdáleně	k6eAd1	vzdáleně
spřízněné	spřízněný	k2eAgNnSc1d1	spřízněné
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
zpěvnými	zpěvný	k2eAgMnPc7d1	zpěvný
ptáky	pták	k1gMnPc7	pták
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nedávné	dávný	k2eNgInPc1d1	nedávný
výzkumy	výzkum	k1gInPc1	výzkum
objevily	objevit	k5eAaPmAgInP	objevit
devátý	devátý	k4xOgInSc4	devátý
druh	druh	k1gInSc4	druh
této	tento	k3xDgFnSc2	tento
malé	malý	k2eAgFnSc2d1	malá
čeledi	čeleď	k1gFnSc2	čeleď
-	-	kIx~	-
pávíka	pávík	k1gMnSc2	pávík
žlutobřichého	žlutobřichý	k2eAgMnSc2d1	žlutobřichý
(	(	kIx(	(
<g/>
Chelidorhynx	Chelidorhynx	k1gInSc1	Chelidorhynx
hypoxantha	hypoxanth	k1gMnSc2	hypoxanth
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
byl	být	k5eAaImAgInS	být
řazen	řadit	k5eAaImNgInS	řadit
do	do	k7c2	do
rodu	rod	k1gInSc2	rod
Rhipidura	Rhipidur	k1gMnSc2	Rhipidur
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
byl	být	k5eAaImAgMnS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
neobvyklého	obvyklý	k2eNgMnSc4d1	neobvyklý
člena	člen	k1gMnSc4	člen
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
je	být	k5eAaImIp3nS	být
sesterským	sesterský	k2eAgInSc7d1	sesterský
druhem	druh	k1gInSc7	druh
krejčiříka	krejčiřík	k1gMnSc2	krejčiřík
ťuhýkovitého	ťuhýkovitý	k2eAgMnSc2d1	ťuhýkovitý
(	(	kIx(	(
<g/>
Stenostira	Stenostir	k1gMnSc2	Stenostir
scita	scit	k1gMnSc2	scit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
oba	dva	k4xCgInPc1	dva
hlavní	hlavní	k2eAgInPc1d1	hlavní
klady	klad	k1gInPc1	klad
čeledi	čeleď	k1gFnSc2	čeleď
(	(	kIx(	(
<g/>
Chelidorhynx	Chelidorhynx	k1gInSc1	Chelidorhynx
<g/>
/	/	kIx~	/
<g/>
Stenostira	Stenostir	k1gMnSc2	Stenostir
a	a	k8xC	a
Culicicapa	Culicicap	k1gMnSc2	Culicicap
<g/>
/	/	kIx~	/
<g/>
Elminia	Elminium	k1gNnSc2	Elminium
<g/>
)	)	kIx)	)
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
vždy	vždy	k6eAd1	vždy
africký	africký	k2eAgInSc4d1	africký
a	a	k8xC	a
asijský	asijský	k2eAgInSc4d1	asijský
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
oběma	dva	k4xCgFnPc3	dva
rozštěpením	rozštěpení	k1gNnSc7	rozštěpení
došlo	dojít	k5eAaPmAgNnS	dojít
zhruba	zhruba	k6eAd1	zhruba
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
<g/>
;	;	kIx,	;
podobné	podobný	k2eAgInPc1d1	podobný
případy	případ	k1gInPc1	případ
africko-asijského	africkosijský	k2eAgNnSc2d1	africko-asijský
rozšíření	rozšíření	k1gNnSc2	rozšíření
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
i	i	k9	i
u	u	k7c2	u
jiných	jiný	k2eAgFnPc2d1	jiná
ptačích	ptačí	k2eAgFnPc2d1	ptačí
čeledí	čeleď	k1gFnPc2	čeleď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kladogram	Kladogram	k1gInSc1	Kladogram
a	a	k8xC	a
druhy	druh	k1gInPc1	druh
čeledi	čeleď	k1gFnSc2	čeleď
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Stenostiridae	Stenostirida	k1gFnSc2	Stenostirida
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Stenostiridae	Stenostiridae	k1gInSc1	Stenostiridae
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
