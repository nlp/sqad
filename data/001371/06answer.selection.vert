<s>
Thallium	Thallium	k1gNnSc1	Thallium
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Tl	Tl	k1gFnSc1	Tl
<g/>
,	,	kIx,	,
lat.	lat.	k?	lat.
Thallium	Thallium	k1gNnSc1	Thallium
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
toxický	toxický	k2eAgInSc1d1	toxický
měkký	měkký	k2eAgInSc1d1	měkký
<g/>
,	,	kIx,	,
lesklý	lesklý	k2eAgInSc1d1	lesklý
kov	kov	k1gInSc1	kov
bílé	bílý	k2eAgFnSc2d1	bílá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
