<s>
Jeho	jeho	k3xOp3gInPc1	jeho
jednoznačně	jednoznačně	k6eAd1	jednoznačně
určujícím	určující	k2eAgInSc7d1	určující
znakem	znak	k1gInSc7	znak
je	být	k5eAaImIp3nS	být
schopnost	schopnost	k1gFnSc1	schopnost
fluorescence	fluorescence	k1gFnSc2	fluorescence
pod	pod	k7c7	pod
krátkými	krátký	k2eAgFnPc7d1	krátká
vlnami	vlna	k1gFnPc7	vlna
UV	UV	kA	UV
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
září	zářit	k5eAaImIp3nS	zářit
silně	silně	k6eAd1	silně
modrou	modrý	k2eAgFnSc7d1	modrá
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
