<s>
Čtveráci	čtverák	k1gMnPc1	čtverák
je	být	k5eAaImIp3nS	být
historický	historický	k2eAgInSc4d1	historický
román	román	k1gInSc4	román
z	z	k7c2	z
období	období	k1gNnSc2	období
počátku	počátek	k1gInSc2	počátek
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Kaplický	Kaplický	k2eAgMnSc1d1	Kaplický
jej	on	k3xPp3gMnSc4	on
napsal	napsat	k5eAaPmAgMnS	napsat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1950	[number]	k4	1950
až	až	k9	až
1951	[number]	k4	1951
<g/>
.	.	kIx.	.
</s>
<s>
Román	Román	k1gMnSc1	Román
popisuje	popisovat	k5eAaImIp3nS	popisovat
příčiny	příčina	k1gFnPc4	příčina
a	a	k8xC	a
vznik	vznik	k1gInSc4	vznik
povstání	povstání	k1gNnSc2	povstání
sedláků	sedlák	k1gMnPc2	sedlák
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc4	jejich
snahu	snaha	k1gFnSc4	snaha
o	o	k7c4	o
zrušení	zrušení	k1gNnSc4	zrušení
poddanství	poddanství	k1gNnSc2	poddanství
a	a	k8xC	a
uznání	uznání	k1gNnSc4	uznání
sedláků	sedlák	k1gMnPc2	sedlák
jako	jako	k8xS	jako
čtvrtého	čtvrtý	k4xOgInSc2	čtvrtý
stavu	stav	k1gInSc2	stav
-	-	kIx~	-
odtud	odtud	k6eAd1	odtud
pochází	pocházet	k5eAaImIp3nS	pocházet
také	také	k9	také
název	název	k1gInSc1	název
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Časový	časový	k2eAgInSc1d1	časový
rámec	rámec	k1gInSc1	rámec
děje	děj	k1gInSc2	děj
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1618	[number]	k4	1618
pražskou	pražský	k2eAgFnSc7d1	Pražská
defenestrací	defenestrace	k1gFnSc7	defenestrace
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1621	[number]	k4	1621
dobytím	dobytí	k1gNnSc7	dobytí
města	město	k1gNnSc2	město
Tábor	Tábor	k1gInSc1	Tábor
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
hlavních	hlavní	k2eAgFnPc2d1	hlavní
historických	historický	k2eAgFnPc2d1	historická
událostí	událost	k1gFnPc2	událost
je	být	k5eAaImIp3nS	být
zobrazen	zobrazit	k5eAaPmNgInS	zobrazit
život	život	k1gInSc1	život
nájemného	nájemné	k1gNnSc2	nájemné
vojáka	voják	k1gMnSc2	voják
Vaňka	Vaněk	k1gMnSc2	Vaněk
Bezoucha	Bezouch	k1gMnSc2	Bezouch
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
naverbován	naverbovat	k5eAaPmNgInS	naverbovat
do	do	k7c2	do
stavovského	stavovský	k2eAgNnSc2d1	Stavovské
vojska	vojsko	k1gNnSc2	vojsko
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
přidává	přidávat	k5eAaImIp3nS	přidávat
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
sedláků	sedlák	k1gMnPc2	sedlák
<g/>
.	.	kIx.	.
</s>
