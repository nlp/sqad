<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Sobotka	Sobotka	k1gMnSc1	Sobotka
(	(	kIx(	(
<g/>
*	*	kIx~	*
23	[number]	k4	23
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1971	[number]	k4	1971
Telnice	Telnice	k1gFnSc1	Telnice
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2014	[number]	k4	2014
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
od	od	k7c2	od
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
předseda	předseda	k1gMnSc1	předseda
České	český	k2eAgFnSc2d1	Česká
strany	strana	k1gFnSc2	strana
sociálně	sociálně	k6eAd1	sociálně
demokratické	demokratický	k2eAgFnPc1d1	demokratická
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
zastával	zastávat	k5eAaImAgMnS	zastávat
funkci	funkce	k1gFnSc4	funkce
1	[number]	k4	1
<g/>
.	.	kIx.	.
místopředsedy	místopředseda	k1gMnPc7	místopředseda
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
ministra	ministr	k1gMnSc2	ministr
financí	finance	k1gFnPc2	finance
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
parlamentních	parlamentní	k2eAgFnPc2d1	parlamentní
voleb	volba	k1gFnPc2	volba
2010	[number]	k4	2010
do	do	k7c2	do
zvolení	zvolení	k1gNnSc2	zvolení
předsedou	předseda	k1gMnSc7	předseda
byl	být	k5eAaImAgMnS	být
pověřen	pověřit	k5eAaPmNgMnS	pověřit
vedením	vedení	k1gNnSc7	vedení
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Telnice	Telnice	k1gFnSc2	Telnice
u	u	k7c2	u
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
do	do	k7c2	do
Slavkova	Slavkov	k1gInSc2	Slavkov
u	u	k7c2	u
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
ZŠ	ZŠ	kA	ZŠ
Tyršova	Tyršův	k2eAgInSc2d1	Tyršův
<g/>
.	.	kIx.	.
</s>
<s>
Vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
dvěma	dva	k4xCgMnPc7	dva
sourozenci	sourozenec	k1gMnPc7	sourozenec
a	a	k8xC	a
matkou	matka	k1gFnSc7	matka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1986	[number]	k4	1986
až	až	k9	až
1990	[number]	k4	1990
studoval	studovat	k5eAaImAgInS	studovat
Gymnázium	gymnázium	k1gNnSc1	gymnázium
Bučovice	Bučovice	k1gFnPc1	Bučovice
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
časopisu	časopis	k1gInSc2	časopis
Respekt	respekt	k1gInSc1	respekt
zde	zde	k6eAd1	zde
spoluzaložil	spoluzaložit	k5eAaPmAgInS	spoluzaložit
studentský	studentský	k2eAgInSc1d1	studentský
časopis	časopis	k1gInSc1	časopis
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
fejetony	fejeton	k1gInPc4	fejeton
zesměšňující	zesměšňující	k2eAgFnSc2d1	zesměšňující
tehdejší	tehdejší	k2eAgFnPc4d1	tehdejší
manifestace	manifestace	k1gFnPc4	manifestace
mu	on	k3xPp3gMnSc3	on
mělo	mít	k5eAaImAgNnS	mít
hrozit	hrozit	k5eAaImF	hrozit
vyloučení	vyloučení	k1gNnSc1	vyloučení
ze	z	k7c2	z
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1990	[number]	k4	1990
až	až	k9	až
1995	[number]	k4	1995
studoval	studovat	k5eAaImAgInS	studovat
Právnickou	právnický	k2eAgFnSc4d1	právnická
fakultu	fakulta	k1gFnSc4	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
poslance	poslanec	k1gMnSc2	poslanec
ČSSD	ČSSD	kA	ČSSD
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Koudelky	koudelka	k1gFnSc2	koudelka
obhájil	obhájit	k5eAaPmAgInS	obhájit
diplomovou	diplomový	k2eAgFnSc4d1	Diplomová
práci	práce	k1gFnSc4	práce
Vývoj	vývoj	k1gInSc1	vývoj
sociální	sociální	k2eAgFnSc2d1	sociální
demokracie	demokracie	k1gFnSc2	demokracie
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
52	[number]	k4	52
listů	list	k1gInPc2	list
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
Mgr.	Mgr.	kA	Mgr.
Jako	jako	k8xC	jako
člen	člen	k1gMnSc1	člen
ČSSD	ČSSD	kA	ČSSD
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
1989	[number]	k4	1989
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgInS	podílet
na	na	k7c4	na
obnovení	obnovení	k1gNnSc4	obnovení
činnosti	činnost	k1gFnSc2	činnost
ČSSD	ČSSD	kA	ČSSD
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Vyškov	Vyškov	k1gInSc1	Vyškov
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
spoluzakládajícím	spoluzakládající	k2eAgInSc7d1	spoluzakládající
členem	člen	k1gInSc7	člen
Mladých	mladý	k2eAgMnPc2d1	mladý
sociálních	sociální	k2eAgMnPc2d1	sociální
demokratů	demokrat	k1gMnPc2	demokrat
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
na	na	k7c6	na
kandidátce	kandidátka	k1gFnSc6	kandidátka
ČSSD	ČSSD	kA	ČSSD
za	za	k7c4	za
Jihomoravský	jihomoravský	k2eAgInSc4d1	jihomoravský
kraj	kraj	k1gInSc4	kraj
<g/>
,	,	kIx,	,
při	při	k7c6	při
kandidatuře	kandidatura	k1gFnSc6	kandidatura
ho	on	k3xPp3gNnSc4	on
podpořil	podpořit	k5eAaPmAgMnS	podpořit
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
předseda	předseda	k1gMnSc1	předseda
jihomoravské	jihomoravský	k2eAgFnSc2d1	Jihomoravská
ČSSD	ČSSD	kA	ČSSD
Václav	Václav	k1gMnSc1	Václav
Grulich	Grulich	k1gMnSc1	Grulich
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
května	květen	k1gInSc2	květen
2001	[number]	k4	2001
do	do	k7c2	do
července	červenec	k1gInSc2	červenec
2002	[number]	k4	2002
byl	být	k5eAaImAgMnS	být
předsedou	předseda	k1gMnSc7	předseda
poslaneckého	poslanecký	k2eAgInSc2d1	poslanecký
klubu	klub	k1gInSc2	klub
ČSSD	ČSSD	kA	ČSSD
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
členem	člen	k1gInSc7	člen
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
rozpočtového	rozpočtový	k2eAgInSc2d1	rozpočtový
<g/>
,	,	kIx,	,
mandátového	mandátový	k2eAgInSc2d1	mandátový
a	a	k8xC	a
imunitního	imunitní	k2eAgInSc2d1	imunitní
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1998	[number]	k4	1998
až	až	k9	až
2010	[number]	k4	2010
byl	být	k5eAaImAgInS	být
členem	člen	k1gMnSc7	člen
městského	městský	k2eAgNnSc2d1	Městské
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
ve	v	k7c6	v
Slavkově	Slavkov	k1gInSc6	Slavkov
u	u	k7c2	u
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
byl	být	k5eAaImAgInS	být
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2002	[number]	k4	2002
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
ministrem	ministr	k1gMnSc7	ministr
financí	finance	k1gFnPc2	finance
ve	v	k7c6	v
Špidlově	Špidlův	k2eAgFnSc6d1	Špidlova
vládě	vláda	k1gFnSc6	vláda
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgFnPc4	který
od	od	k7c2	od
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2003	[number]	k4	2003
zastával	zastávat	k5eAaImAgMnS	zastávat
také	také	k9	také
post	post	k1gInSc4	post
místopředsedy	místopředseda	k1gMnSc2	místopředseda
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Funkci	funkce	k1gFnSc4	funkce
ministra	ministr	k1gMnSc2	ministr
financí	finance	k1gFnPc2	finance
zastával	zastávat	k5eAaImAgMnS	zastávat
i	i	k9	i
ve	v	k7c6	v
vládách	vláda	k1gFnPc6	vláda
Stanislava	Stanislav	k1gMnSc2	Stanislav
Grosse	Gross	k1gMnSc2	Gross
a	a	k8xC	a
Jiřího	Jiří	k1gMnSc2	Jiří
Paroubka	Paroubek	k1gMnSc2	Paroubek
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
kabinetu	kabinet	k1gInSc6	kabinet
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
současně	současně	k6eAd1	současně
1	[number]	k4	1
<g/>
.	.	kIx.	.
místopředsedou	místopředseda	k1gMnSc7	místopředseda
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
doby	doba	k1gFnSc2	doba
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc4d1	znám
jeho	jeho	k3xOp3gInSc4	jeho
výrok	výrok	k1gInSc4	výrok
o	o	k7c6	o
nereálnosti	nereálnost	k1gFnSc6	nereálnost
předvolebních	předvolební	k2eAgInPc2d1	předvolební
slibů	slib	k1gInPc2	slib
ČSSD	ČSSD	kA	ČSSD
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ty	ten	k3xDgInPc1	ten
sliby	slib	k1gInPc1	slib
nebyly	být	k5eNaImAgInP	být
zasazeny	zasadit	k5eAaPmNgInP	zasadit
do	do	k7c2	do
reálného	reálný	k2eAgInSc2d1	reálný
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
rámce	rámec	k1gInSc2	rámec
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
konfrontaci	konfrontace	k1gFnSc6	konfrontace
s	s	k7c7	s
realitou	realita	k1gFnSc7	realita
neobstojí	obstát	k5eNaPmIp3nS	obstát
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Na	na	k7c6	na
ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
financí	finance	k1gFnPc2	finance
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
poradní	poradní	k2eAgInSc1d1	poradní
orgán	orgán	k1gInSc1	orgán
složený	složený	k2eAgInSc1d1	složený
z	z	k7c2	z
liberálních	liberální	k2eAgMnPc2d1	liberální
ekonomů	ekonom	k1gMnPc2	ekonom
<g/>
,	,	kIx,	,
předchůdce	předchůdce	k1gMnSc2	předchůdce
pozdějšího	pozdní	k2eAgInSc2d2	pozdější
NERVu	nerv	k1gInSc2	nerv
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Vladimírem	Vladimír	k1gMnSc7	Vladimír
Špidlou	Špidla	k1gMnSc7	Špidla
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
podobnou	podobný	k2eAgFnSc4d1	podobná
úspornou	úsporný	k2eAgFnSc4d1	úsporná
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
kritizovali	kritizovat	k5eAaImAgMnP	kritizovat
(	(	kIx(	(
<g/>
propouštění	propouštění	k1gNnSc1	propouštění
státních	státní	k2eAgMnPc2d1	státní
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
,	,	kIx,	,
omezení	omezení	k1gNnSc1	omezení
podpory	podpora	k1gFnSc2	podpora
stavebního	stavební	k2eAgNnSc2d1	stavební
spoření	spoření	k1gNnSc2	spoření
a	a	k8xC	a
nemocenských	mocenský	k2eNgFnPc2d1	mocenský
dávek	dávka	k1gFnPc2	dávka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
Jiřího	Jiří	k1gMnSc2	Jiří
Paroubka	Paroubek	k1gMnSc2	Paroubek
měl	mít	k5eAaImAgMnS	mít
podle	podle	k7c2	podle
týdeníku	týdeník	k1gInSc2	týdeník
Respekt	respekt	k1gInSc1	respekt
naopak	naopak	k6eAd1	naopak
přistoupit	přistoupit	k5eAaPmF	přistoupit
na	na	k7c4	na
výrazné	výrazný	k2eAgNnSc4d1	výrazné
zvýšení	zvýšení	k1gNnSc4	zvýšení
schodku	schodek	k1gInSc2	schodek
a	a	k8xC	a
omezení	omezení	k1gNnSc1	omezení
svých	svůj	k3xOyFgFnPc2	svůj
dřívějších	dřívější	k2eAgFnPc2d1	dřívější
restrikcí	restrikce	k1gFnPc2	restrikce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2003	[number]	k4	2003
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
stranického	stranický	k2eAgMnSc4d1	stranický
kolegu	kolega	k1gMnSc4	kolega
Jaroslava	Jaroslav	k1gMnSc4	Jaroslav
Tvrdíka	Tvrdík	k1gMnSc4	Tvrdík
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
Českých	český	k2eAgFnPc2d1	Česká
aerolinií	aerolinie	k1gFnPc2	aerolinie
bez	bez	k7c2	bez
výběrového	výběrový	k2eAgNnSc2d1	výběrové
řízení	řízení	k1gNnSc2	řízení
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2003	[number]	k4	2003
nechal	nechat	k5eAaPmAgMnS	nechat
odvolat	odvolat	k5eAaPmF	odvolat
šéfa	šéf	k1gMnSc4	šéf
společnosti	společnost	k1gFnSc2	společnost
ČEZ	ČEZ	kA	ČEZ
Jaroslava	Jaroslava	k1gFnSc1	Jaroslava
Míla	Míla	k1gMnSc1	Míla
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
se	s	k7c7	s
Sobotkovým	Sobotkův	k2eAgNnSc7d1	Sobotkovo
doporučením	doporučení	k1gNnSc7	doporučení
podal	podat	k5eAaPmAgInS	podat
přihlášku	přihláška	k1gFnSc4	přihláška
do	do	k7c2	do
tendru	tendr	k1gInSc2	tendr
na	na	k7c6	na
koupi	koupě	k1gFnSc6	koupě
akcií	akcie	k1gFnPc2	akcie
Severočeských	severočeský	k2eAgInPc2d1	severočeský
dolů	dol	k1gInPc2	dol
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
měl	mít	k5eAaImAgInS	mít
ČEZ	ČEZ	kA	ČEZ
podíl	podíl	k1gInSc4	podíl
37	[number]	k4	37
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
financí	finance	k1gFnPc2	finance
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Bohuslava	Bohuslav	k1gMnSc2	Bohuslav
Sobotky	Sobotka	k1gMnSc2	Sobotka
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
Vlastimila	Vlastimila	k1gFnSc1	Vlastimila
Tlustého	tlusté	k1gNnSc2	tlusté
poškodilo	poškodit	k5eAaPmAgNnS	poškodit
politickou	politický	k2eAgFnSc4d1	politická
stranu	strana	k1gFnSc4	strana
SNK-ED	SNK-ED	k1gFnSc2	SNK-ED
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
odmítalo	odmítat	k5eAaImAgNnS	odmítat
proplatit	proplatit	k5eAaPmF	proplatit
státní	státní	k2eAgInSc4d1	státní
příspěvek	příspěvek	k1gInSc4	příspěvek
na	na	k7c4	na
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Řadě	řada	k1gFnSc3	řada
firem	firma	k1gFnPc2	firma
odpustil	odpustit	k5eAaPmAgMnS	odpustit
nedoplatky	nedoplatek	k1gInPc1	nedoplatek
daně	daň	k1gFnSc2	daň
z	z	k7c2	z
příjmů	příjem	k1gInPc2	příjem
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Hospodářských	hospodářský	k2eAgFnPc2d1	hospodářská
novin	novina	k1gFnPc2	novina
není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
jaký	jaký	k3yQgInSc1	jaký
byl	být	k5eAaImAgInS	být
motiv	motiv	k1gInSc4	motiv
tohoto	tento	k3xDgNnSc2	tento
jednání	jednání	k1gNnSc2	jednání
<g/>
.	.	kIx.	.
</s>
<s>
Společnosti	společnost	k1gFnSc3	společnost
Bekaert	Bekaert	k1gInSc4	Bekaert
Petrovice	Petrovice	k1gFnSc2	Petrovice
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
a	a	k8xC	a
Hasil	hasit	k5eAaImAgMnS	hasit
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
uvést	uvést	k5eAaPmF	uvést
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
jim	on	k3xPp3gMnPc3	on
daně	daň	k1gFnPc1	daň
byly	být	k5eAaImAgFnP	být
odpuštěny	odpustit	k5eAaPmNgFnP	odpustit
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
Lindab	Lindaba	k1gFnPc2	Lindaba
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nevyjádřila	vyjádřit	k5eNaPmAgFnS	vyjádřit
<g/>
.	.	kIx.	.
</s>
<s>
Dluhy	dluh	k1gInPc1	dluh
byly	být	k5eAaImAgInP	být
také	také	k9	také
odpuštěny	odpuštěn	k2eAgFnPc1d1	odpuštěna
společnosti	společnost	k1gFnPc1	společnost
Zbrojovka	zbrojovka	k1gFnSc1	zbrojovka
Vsetín	Vsetín	k1gInSc1	Vsetín
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Sobotka	Sobotka	k1gMnSc1	Sobotka
odpustil	odpustit	k5eAaPmAgMnS	odpustit
dluhy	dluh	k1gInPc4	dluh
za	za	k7c4	za
115	[number]	k4	115
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	on	k3xPp3gNnSc2	on
působení	působení	k1gNnSc2	působení
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
ministra	ministr	k1gMnSc2	ministr
financí	finance	k1gFnPc2	finance
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
rozpočtové	rozpočtový	k2eAgInPc1d1	rozpočtový
schodky	schodek	k1gInPc1	schodek
v	v	k7c6	v
celkové	celkový	k2eAgFnSc6d1	celková
výši	výše	k1gFnSc6	výše
406,6	[number]	k4	406,6
miliard	miliarda	k4xCgFnPc2	miliarda
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
čehož	což	k3yQnSc2	což
se	se	k3xPyFc4	se
státní	státní	k2eAgInSc4d1	státní
dluh	dluh	k1gInSc4	dluh
více	hodně	k6eAd2	hodně
než	než	k8xS	než
zdvojnásobil	zdvojnásobit	k5eAaPmAgInS	zdvojnásobit
<g/>
.	.	kIx.	.
</s>
<s>
Výše	výše	k1gFnSc1	výše
veřejného	veřejný	k2eAgInSc2d1	veřejný
dluhu	dluh	k1gInSc2	dluh
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
k	k	k7c3	k
HDP	HDP	kA	HDP
se	se	k3xPyFc4	se
snížila	snížit	k5eAaPmAgFnS	snížit
z	z	k7c2	z
28,6	[number]	k4	28,6
%	%	kIx~	%
HDP	HDP	kA	HDP
na	na	k7c4	na
28,3	[number]	k4	28,3
%	%	kIx~	%
HDP	HDP	kA	HDP
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
27,9	[number]	k4	27,9
%	%	kIx~	%
HDP	HDP	kA	HDP
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
(	(	kIx(	(
<g/>
státní	státní	k2eAgInSc1d1	státní
rozpočet	rozpočet	k1gInSc1	rozpočet
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
rok	rok	k1gInSc4	rok
ještě	ještě	k6eAd1	ještě
připravoval	připravovat	k5eAaImAgMnS	připravovat
B.	B.	kA	B.
Sobotka	Sobotka	k1gMnSc1	Sobotka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
B.	B.	kA	B.
Sobotka	Sobotka	k1gMnSc1	Sobotka
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
jediným	jediný	k2eAgMnSc7d1	jediný
ministrem	ministr	k1gMnSc7	ministr
financí	finance	k1gFnPc2	finance
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterého	který	k3yQgMnSc4	který
se	se	k3xPyFc4	se
české	český	k2eAgNnSc1d1	české
veřejné	veřejný	k2eAgNnSc1d1	veřejné
reálné	reálný	k2eAgNnSc1d1	reálné
zadlužení	zadlužení	k1gNnSc1	zadlužení
snížilo	snížit	k5eAaPmAgNnS	snížit
<g/>
,	,	kIx,	,
vládní	vládní	k2eAgInPc1d1	vládní
výdaje	výdaj	k1gInPc1	výdaj
však	však	k9	však
rostly	růst	k5eAaImAgFnP	růst
rekordním	rekordní	k2eAgNnSc7d1	rekordní
tempem	tempo	k1gNnSc7	tempo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	jeho	k3xOp3gInSc2	jeho
nástupu	nástup	k1gInSc2	nástup
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
ministra	ministr	k1gMnSc2	ministr
financí	finance	k1gFnPc2	finance
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
činil	činit	k5eAaImAgInS	činit
státní	státní	k2eAgInSc1d1	státní
dluh	dluh	k1gInSc1	dluh
395,6	[number]	k4	395,6
miliard	miliarda	k4xCgFnPc2	miliarda
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
po	po	k7c6	po
celé	celá	k1gFnSc6	celá
jeho	jeho	k3xOp3gNnSc4	jeho
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
zažívala	zažívat	k5eAaImAgFnS	zažívat
ekonomika	ekonomika	k1gFnSc1	ekonomika
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
permanentní	permanentní	k2eAgInSc4d1	permanentní
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
růst	růst	k1gInSc4	růst
(	(	kIx(	(
<g/>
r.	r.	kA	r.
2003	[number]	k4	2003
+3,6	+3,6	k4	+3,6
%	%	kIx~	%
<g/>
,	,	kIx,	,
r.	r.	kA	r.
2004	[number]	k4	2004
+4,5	+4,5	k4	+4,5
%	%	kIx~	%
<g/>
,	,	kIx,	,
r.	r.	kA	r.
2005	[number]	k4	2005
+6,3	+6,3	k4	+6,3
%	%	kIx~	%
<g/>
,	,	kIx,	,
r.	r.	kA	r.
2006	[number]	k4	2006
+6,8	+6,8	k4	+6,8
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sobotka	Sobotka	k1gMnSc1	Sobotka
zvýšil	zvýšit	k5eAaPmAgMnS	zvýšit
vládní	vládní	k2eAgInPc4d1	vládní
výdaje	výdaj	k1gInPc4	výdaj
jako	jako	k8xC	jako
žádný	žádný	k3yNgMnSc1	žádný
jiný	jiný	k2eAgMnSc1d1	jiný
polistopadový	polistopadový	k2eAgMnSc1d1	polistopadový
ministr	ministr	k1gMnSc1	ministr
financí	finance	k1gFnPc2	finance
(	(	kIx(	(
<g/>
750	[number]	k4	750
mld.	mld.	k?	mld.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
809	[number]	k4	809
mld.	mld.	k?	mld.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
863	[number]	k4	863
mld.	mld.	k?	mld.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
923	[number]	k4	923
mld.	mld.	k?	mld.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
1021	[number]	k4	1021
mld.	mld.	k?	mld.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
navzdory	navzdory	k7c3	navzdory
6,8	[number]	k4	6,8
<g/>
%	%	kIx~	%
růstu	růst	k1gInSc2	růst
HDP	HDP	kA	HDP
české	český	k2eAgFnSc2d1	Česká
ekonomiky	ekonomika	k1gFnSc2	ekonomika
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
rekordní	rekordní	k2eAgInSc1d1	rekordní
deficit	deficit	k1gInSc1	deficit
111,3	[number]	k4	111,3
mld.	mld.	k?	mld.
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
Sobotkova	Sobotkův	k2eAgInSc2d1	Sobotkův
mandátu	mandát	k1gInSc2	mandát
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
činil	činit	k5eAaImAgInS	činit
státní	státní	k2eAgInSc4d1	státní
dluh	dluh	k1gInSc4	dluh
802,5	[number]	k4	802,5
mld.	mld.	k?	mld.
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgInS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvojnásobek	dvojnásobek	k1gInSc4	dvojnásobek
státního	státní	k2eAgInSc2d1	státní
dluhu	dluh	k1gInSc2	dluh
nakumulovaného	nakumulovaný	k2eAgInSc2d1	nakumulovaný
v	v	k7c6	v
období	období	k1gNnSc6	období
1993	[number]	k4	1993
<g/>
-	-	kIx~	-
<g/>
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Sobotkova	Sobotkův	k2eAgNnSc2d1	Sobotkovo
působení	působení	k1gNnSc2	působení
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
ministra	ministr	k1gMnSc2	ministr
financí	finance	k1gFnPc2	finance
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
privatizaci	privatizace	k1gFnSc3	privatizace
společnosti	společnost	k1gFnSc2	společnost
Unipetrol	Unipetrola	k1gFnPc2	Unipetrola
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgMnSc1d1	původní
vítěz	vítěz	k1gMnSc1	vítěz
privatizace	privatizace	k1gFnSc2	privatizace
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
totiž	totiž	k9	totiž
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
od	od	k7c2	od
smlouvy	smlouva	k1gFnSc2	smlouva
a	a	k8xC	a
zamýšlený	zamýšlený	k2eAgInSc1d1	zamýšlený
převod	převod	k1gInSc1	převod
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
dokončit	dokončit	k5eAaPmF	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
původních	původní	k2eAgMnPc2d1	původní
sedmi	sedm	k4xCc2	sedm
uchazečů	uchazeč	k1gMnPc2	uchazeč
postoupily	postoupit	k5eAaPmAgFnP	postoupit
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
tři	tři	k4xCgFnPc4	tři
firmy	firma	k1gFnPc1	firma
-	-	kIx~	-
PKN	PKN	kA	PKN
Orlen	Orlna	k1gFnPc2	Orlna
<g/>
,	,	kIx,	,
MOL	molo	k1gNnPc2	molo
a	a	k8xC	a
Shell	Shella	k1gFnPc2	Shella
<g/>
.	.	kIx.	.
</s>
<s>
Kazašská	kazašský	k2eAgFnSc1d1	kazašská
firma	firma	k1gFnSc1	firma
KazMunaiGaz	KazMunaiGaz	k1gInSc1	KazMunaiGaz
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
podle	podle	k7c2	podle
privatizačního	privatizační	k2eAgMnSc2d1	privatizační
poradce	poradce	k1gMnSc2	poradce
vlády	vláda	k1gFnSc2	vláda
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
cenu	cena	k1gFnSc4	cena
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
vyřazena	vyřadit	k5eAaPmNgFnS	vyřadit
<g/>
.	.	kIx.	.
</s>
<s>
Sobotka	Sobotka	k1gMnSc1	Sobotka
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
zdůvodnil	zdůvodnit	k5eAaPmAgMnS	zdůvodnit
strategickými	strategický	k2eAgInPc7d1	strategický
a	a	k8xC	a
bezpečnostními	bezpečnostní	k2eAgInPc7d1	bezpečnostní
zájmy	zájem	k1gInPc7	zájem
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
předložila	předložit	k5eAaPmAgFnS	předložit
PKN	PKN	kA	PKN
Orlen	Orlen	k1gInSc4	Orlen
jako	jako	k8xS	jako
jediná	jediný	k2eAgFnSc1d1	jediná
nabídku	nabídka	k1gFnSc4	nabídka
v	v	k7c6	v
termínu	termín	k1gInSc6	termín
a	a	k8xC	a
ve	v	k7c6	v
stanovené	stanovený	k2eAgFnSc6d1	stanovená
cenové	cenový	k2eAgFnSc6d1	cenová
relaci	relace	k1gFnSc6	relace
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
obvinil	obvinit	k5eAaPmAgMnS	obvinit
lobbista	lobbista	k1gMnSc1	lobbista
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
neúspěšných	úspěšný	k2eNgFnPc2d1	neúspěšná
firem	firma	k1gFnPc2	firma
Jacek	Jacek	k1gMnSc1	Jacek
Spyra	Spyra	k1gMnSc1	Spyra
Jiřího	Jiří	k1gMnSc4	Jiří
Paroubka	Paroubek	k1gMnSc4	Paroubek
<g/>
,	,	kIx,	,
Bohuslava	Bohuslav	k1gMnSc4	Bohuslav
Sobotku	Sobotka	k1gMnSc4	Sobotka
<g/>
,	,	kIx,	,
Stanislava	Stanislav	k1gMnSc4	Stanislav
Grosse	Gross	k1gMnSc4	Gross
a	a	k8xC	a
Miroslava	Miroslav	k1gMnSc4	Miroslav
Kalouska	Kalousek	k1gMnSc4	Kalousek
z	z	k7c2	z
přijetí	přijetí	k1gNnSc2	přijetí
úplatku	úplatek	k1gInSc2	úplatek
od	od	k7c2	od
podnikatele	podnikatel	k1gMnSc2	podnikatel
Andreje	Andrej	k1gMnSc2	Andrej
Babiše	Babiš	k1gMnSc2	Babiš
<g/>
.	.	kIx.	.
</s>
<s>
Babiš	Babiš	k1gInSc1	Babiš
měl	mít	k5eAaImAgInS	mít
zajistit	zajistit	k5eAaPmF	zajistit
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
podporu	podpora	k1gFnSc4	podpora
pro	pro	k7c4	pro
PKN	PKN	kA	PKN
Orlen	Orlen	k1gInSc4	Orlen
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
zisk	zisk	k1gInSc4	zisk
části	část	k1gFnSc2	část
podniku	podnik	k1gInSc2	podnik
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
privatizace	privatizace	k1gFnSc2	privatizace
<g/>
.	.	kIx.	.
</s>
<s>
Sobotka	Sobotka	k1gMnSc1	Sobotka
i	i	k8xC	i
Babiš	Babiš	k1gMnSc1	Babiš
takové	takový	k3xDgNnSc4	takový
jednání	jednání	k1gNnSc4	jednání
popřeli	popřít	k5eAaPmAgMnP	popřít
<g/>
,	,	kIx,	,
polská	polský	k2eAgFnSc1d1	polská
parlamentní	parlamentní	k2eAgFnSc1d1	parlamentní
komise	komise	k1gFnSc1	komise
naopak	naopak	k6eAd1	naopak
došla	dojít	k5eAaPmAgFnS	dojít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
česká	český	k2eAgFnSc1d1	Česká
vláda	vláda	k1gFnSc1	vláda
jednala	jednat	k5eAaImAgFnS	jednat
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
Babiše	Babiš	k1gInSc2	Babiš
<g/>
.	.	kIx.	.
</s>
<s>
Spyra	Spyra	k1gMnSc1	Spyra
byl	být	k5eAaImAgMnS	být
později	pozdě	k6eAd2	pozdě
zatčen	zatknout	k5eAaPmNgMnS	zatknout
<g/>
,	,	kIx,	,
když	když	k8xS	když
požadoval	požadovat	k5eAaImAgMnS	požadovat
od	od	k7c2	od
Babiše	Babiš	k1gInSc2	Babiš
úplatek	úplatek	k1gInSc4	úplatek
za	za	k7c4	za
zastavení	zastavení	k1gNnSc4	zastavení
trestního	trestní	k2eAgNnSc2d1	trestní
stíhání	stíhání	k1gNnSc2	stíhání
proti	proti	k7c3	proti
jeho	jeho	k3xOp3gFnSc3	jeho
osobě	osoba	k1gFnSc3	osoba
<g/>
,	,	kIx,	,
a	a	k8xC	a
odsouzen	odsouzet	k5eAaImNgInS	odsouzet
k	k	k7c3	k
podmíněnému	podmíněný	k2eAgInSc3d1	podmíněný
trestu	trest	k1gInSc3	trest
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
obvinění	obvinění	k1gNnSc3	obvinění
z	z	k7c2	z
nevýhodného	výhodný	k2eNgInSc2d1	nevýhodný
prodeje	prodej	k1gInSc2	prodej
bytového	bytový	k2eAgInSc2d1	bytový
fondu	fond	k1gInSc2	fond
společnosti	společnost	k1gFnSc2	společnost
OKD	OKD	kA	OKD
společnosti	společnost	k1gFnSc2	společnost
Karbon	karbon	k1gInSc1	karbon
Invest	Invest	k1gFnSc4	Invest
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jej	on	k3xPp3gMnSc4	on
následně	následně	k6eAd1	následně
prodala	prodat	k5eAaPmAgFnS	prodat
firmě	firma	k1gFnSc3	firma
podnikatele	podnikatel	k1gMnSc2	podnikatel
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Bakaly	Bakal	k1gMnPc7	Bakal
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Sobotka	Sobotka	k1gFnSc1	Sobotka
hájí	hájit	k5eAaImIp3nS	hájit
poukazem	poukaz	k1gInSc7	poukaz
na	na	k7c4	na
znalecké	znalecký	k2eAgInPc4d1	znalecký
posudky	posudek	k1gInPc4	posudek
a	a	k8xC	a
zanesením	zanesení	k1gNnSc7	zanesení
předkupních	předkupní	k2eAgNnPc2d1	předkupní
práv	právo	k1gNnPc2	právo
nájemníků	nájemník	k1gMnPc2	nájemník
do	do	k7c2	do
smlouvy	smlouva	k1gFnSc2	smlouva
o	o	k7c6	o
prodeji	prodej	k1gInSc6	prodej
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
ČR	ČR	kA	ČR
na	na	k7c6	na
kandidátce	kandidátka	k1gFnSc6	kandidátka
ČSSD	ČSSD	kA	ČSSD
za	za	k7c4	za
Jihomoravský	jihomoravský	k2eAgInSc4d1	jihomoravský
kraj	kraj	k1gInSc4	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
z	z	k7c2	z
peněz	peníze	k1gInPc2	peníze
určených	určený	k2eAgInPc2d1	určený
pro	pro	k7c4	pro
poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
náhrady	náhrada	k1gFnSc2	náhrada
ušetřil	ušetřit	k5eAaPmAgInS	ušetřit
5	[number]	k4	5
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
následně	následně	k6eAd1	následně
použil	použít	k5eAaPmAgMnS	použít
ke	k	k7c3	k
koupi	koupě	k1gFnSc3	koupě
luxusního	luxusní	k2eAgInSc2d1	luxusní
bytu	byt	k1gInSc2	byt
za	za	k7c4	za
7	[number]	k4	7
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Sobotka	Sobotka	k1gMnSc1	Sobotka
odmítá	odmítat	k5eAaImIp3nS	odmítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
náhrady	náhrada	k1gFnPc4	náhrada
zneužil	zneužít	k5eAaPmAgMnS	zneužít
<g/>
.	.	kIx.	.
</s>
<s>
Náhrady	náhrada	k1gFnPc1	náhrada
přitom	přitom	k6eAd1	přitom
mají	mít	k5eAaImIp3nP	mít
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
uhrazení	uhrazení	k1gNnSc4	uhrazení
nákladů	náklad	k1gInPc2	náklad
za	za	k7c4	za
odborné	odborný	k2eAgFnPc4d1	odborná
studie	studie	k1gFnPc4	studie
<g/>
,	,	kIx,	,
dopravu	doprava	k1gFnSc4	doprava
nebo	nebo	k8xC	nebo
bydlení	bydlení	k1gNnSc4	bydlení
<g/>
,	,	kIx,	,
vypláceny	vyplácet	k5eAaImNgFnP	vyplácet
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
byly	být	k5eAaImAgInP	být
dohromady	dohromady	k6eAd1	dohromady
s	s	k7c7	s
platem	plat	k1gInSc7	plat
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
na	na	k7c6	na
sjezdu	sjezd	k1gInSc6	sjezd
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
předsedou	předseda	k1gMnSc7	předseda
ČSSD	ČSSD	kA	ČSSD
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
volbě	volba	k1gFnSc6	volba
získal	získat	k5eAaPmAgInS	získat
304	[number]	k4	304
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
porazil	porazit	k5eAaPmAgMnS	porazit
protikandidáta	protikandidát	k1gMnSc4	protikandidát
Michala	Michal	k1gMnSc4	Michal
Haška	Hašek	k1gMnSc4	Hašek
o	o	k7c4	o
19	[number]	k4	19
delegátských	delegátský	k2eAgInPc2d1	delegátský
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
lídrem	lídr	k1gMnSc7	lídr
opozice	opozice	k1gFnSc2	opozice
vůči	vůči	k7c3	vůči
vládě	vláda	k1gFnSc3	vláda
Petra	Petr	k1gMnSc2	Petr
Nečase	Nečas	k1gMnSc2	Nečas
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Respektu	respekt	k1gInSc2	respekt
ve	v	k7c6	v
straně	strana	k1gFnSc6	strana
nejprve	nejprve	k6eAd1	nejprve
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
vize	vize	k1gFnPc4	vize
levicových	levicový	k2eAgMnPc2d1	levicový
intelektuálů	intelektuál	k1gMnPc2	intelektuál
z	z	k7c2	z
okruhu	okruh	k1gInSc2	okruh
Jiřího	Jiří	k1gMnSc2	Jiří
Pehe	Peh	k1gFnSc2	Peh
a	a	k8xC	a
Centra	centrum	k1gNnSc2	centrum
pro	pro	k7c4	pro
sociálně-tržní	sociálněržní	k2eAgFnSc4d1	sociálně-tržní
ekonomiku	ekonomika	k1gFnSc4	ekonomika
a	a	k8xC	a
otevřenou	otevřený	k2eAgFnSc4d1	otevřená
demokracii	demokracie	k1gFnSc4	demokracie
(	(	kIx(	(
<g/>
CESTA	cesta	k1gFnSc1	cesta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
však	však	k9	však
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
stoupenců	stoupenec	k1gMnPc2	stoupenec
Michala	Michal	k1gMnSc2	Michal
Haška	Hašek	k1gMnSc2	Hašek
a	a	k8xC	a
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
kontakty	kontakt	k1gInPc4	kontakt
přerušil	přerušit	k5eAaPmAgMnS	přerušit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
před	před	k7c7	před
hlasováním	hlasování	k1gNnSc7	hlasování
o	o	k7c6	o
důvěře	důvěra	k1gFnSc6	důvěra
vládě	vláda	k1gFnSc6	vláda
Jiřího	Jiří	k1gMnSc2	Jiří
Rusnoka	Rusnoek	k1gMnSc2	Rusnoek
nejdříve	dříve	k6eAd3	dříve
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
s	s	k7c7	s
její	její	k3xOp3gFnSc7	její
podporou	podpora	k1gFnSc7	podpora
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
podřídil	podřídit	k5eAaPmAgMnS	podřídit
většinovému	většinový	k2eAgInSc3d1	většinový
názoru	názor	k1gInSc2	názor
svého	svůj	k3xOyFgInSc2	svůj
klubu	klub	k1gInSc2	klub
a	a	k8xC	a
prosadil	prosadit	k5eAaPmAgMnS	prosadit
její	její	k3xOp3gFnSc4	její
podporu	podpora	k1gFnSc4	podpora
všemi	všecek	k3xTgMnPc7	všecek
sociálnědemokratickými	sociálnědemokratický	k2eAgMnPc7d1	sociálnědemokratický
poslanci	poslanec	k1gMnPc7	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vláda	vláda	k1gFnSc1	vláda
Bohuslava	Bohuslava	k1gFnSc1	Bohuslava
Sobotky	Sobotka	k1gFnSc2	Sobotka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
PČR	PČR	kA	PČR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
jako	jako	k9	jako
lídr	lídr	k1gMnSc1	lídr
ČSSD	ČSSD	kA	ČSSD
<g/>
.	.	kIx.	.
</s>
<s>
Stranu	strana	k1gFnSc4	strana
přivedl	přivést	k5eAaPmAgMnS	přivést
k	k	k7c3	k
zisku	zisk	k1gInSc3	zisk
20,45	[number]	k4	20,45
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
se	se	k3xPyFc4	se
po	po	k7c6	po
předčasných	předčasný	k2eAgFnPc6d1	předčasná
volbách	volba	k1gFnPc6	volba
dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2013	[number]	k4	2013
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Lány	lán	k1gInPc4	lán
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
částí	část	k1gFnSc7	část
vedení	vedení	k1gNnSc2	vedení
ČSSD	ČSSD	kA	ČSSD
(	(	kIx(	(
<g/>
Haškem	Hašek	k1gMnSc7	Hašek
<g/>
,	,	kIx,	,
Tejcem	Tejce	k1gMnSc7	Tejce
<g/>
,	,	kIx,	,
Škromachem	Škromach	k1gMnSc7	Škromach
<g/>
,	,	kIx,	,
Chovancem	Chovanec	k1gMnSc7	Chovanec
a	a	k8xC	a
Zimolou	Zimola	k1gMnSc7	Zimola
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přitom	přitom	k6eAd1	přitom
řádně	řádně	k6eAd1	řádně
zvolený	zvolený	k2eAgMnSc1d1	zvolený
a	a	k8xC	a
úřadující	úřadující	k2eAgMnSc1d1	úřadující
předseda	předseda	k1gMnSc1	předseda
strany	strana	k1gFnSc2	strana
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Sobotka	Sobotka	k1gMnSc1	Sobotka
přizván	přizvat	k5eAaPmNgMnS	přizvat
nebyl	být	k5eNaImAgMnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Výkonný	výkonný	k2eAgInSc1d1	výkonný
výbor	výbor	k1gInSc1	výbor
ČSSD	ČSSD	kA	ČSSD
pak	pak	k6eAd1	pak
jednal	jednat	k5eAaImAgMnS	jednat
v	v	k7c6	v
Lidovém	lidový	k2eAgInSc6d1	lidový
domě	dům	k1gInSc6	dům
o	o	k7c6	o
výzvě	výzva	k1gFnSc6	výzva
k	k	k7c3	k
odstoupení	odstoupení	k1gNnSc3	odstoupení
Sobotky	Sobotka	k1gFnSc2	Sobotka
z	z	k7c2	z
čela	čelo	k1gNnSc2	čelo
ČSSD	ČSSD	kA	ČSSD
<g/>
.	.	kIx.	.
</s>
<s>
Hašek	Hašek	k1gMnSc1	Hašek
i	i	k9	i
další	další	k2eAgMnPc1d1	další
účastníci	účastník	k1gMnPc1	účastník
několikrát	několikrát	k6eAd1	několikrát
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
popřeli	popřít	k5eAaPmAgMnP	popřít
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
schůzka	schůzka	k1gFnSc1	schůzka
konala	konat	k5eAaImAgFnS	konat
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
její	její	k3xOp3gMnSc1	její
účastník	účastník	k1gMnSc1	účastník
Chovanec	Chovanec	k1gMnSc1	Chovanec
médiím	médium	k1gNnPc3	médium
svou	svůj	k3xOyFgFnSc4	svůj
účast	účast	k1gFnSc4	účast
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
tam	tam	k6eAd1	tam
prý	prý	k9	prý
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
schůzku	schůzka	k1gFnSc4	schůzka
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
Hašek	Hašek	k1gMnSc1	Hašek
a	a	k8xC	a
společníci	společník	k1gMnPc1	společník
<g/>
.	.	kIx.	.
</s>
<s>
Hašek	Hašek	k1gMnSc1	Hašek
<g/>
,	,	kIx,	,
Tejc	Tejc	k1gFnSc1	Tejc
<g/>
,	,	kIx,	,
Škromach	Škromach	k1gInSc1	Škromach
a	a	k8xC	a
Zimola	Zimola	k1gFnSc1	Zimola
nakonec	nakonec	k6eAd1	nakonec
pod	pod	k7c7	pod
mediálním	mediální	k2eAgInSc7d1	mediální
tlakem	tlak	k1gInSc7	tlak
schůzku	schůzka	k1gFnSc4	schůzka
potvrdili	potvrdit	k5eAaPmAgMnP	potvrdit
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
rezignovali	rezignovat	k5eAaBmAgMnP	rezignovat
na	na	k7c4	na
stranické	stranický	k2eAgFnPc4d1	stranická
funkce	funkce	k1gFnPc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Zeman	Zeman	k1gMnSc1	Zeman
poté	poté	k6eAd1	poté
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
komické	komický	k2eAgNnSc4d1	komické
utajovat	utajovat	k5eAaImF	utajovat
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
schůzku	schůzka	k1gFnSc4	schůzka
a	a	k8xC	a
současně	současně	k6eAd1	současně
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
pokus	pokus	k1gInSc4	pokus
o	o	k7c4	o
převrat	převrat	k1gInSc4	převrat
ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
ČSSD	ČSSD	kA	ČSSD
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
dnech	den	k1gInPc6	den
pověřil	pověřit	k5eAaPmAgMnS	pověřit
Zeman	Zeman	k1gMnSc1	Zeman
Sobotku	Sobotka	k1gFnSc4	Sobotka
sestavením	sestavení	k1gNnSc7	sestavení
nové	nový	k2eAgFnSc2d1	nová
české	český	k2eAgFnSc2d1	Česká
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2014	[number]	k4	2014
jej	on	k3xPp3gInSc4	on
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
České	český	k2eAgFnPc1d1	Česká
republiky	republika	k1gFnPc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
funkce	funkce	k1gFnSc2	funkce
byl	být	k5eAaImAgInS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
až	až	k9	až
83	[number]	k4	83
dnů	den	k1gInPc2	den
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
ČR	ČR	kA	ČR
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
doba	doba	k1gFnSc1	doba
mezi	mezi	k7c7	mezi
volbami	volba	k1gFnPc7	volba
a	a	k8xC	a
jmenováním	jmenování	k1gNnSc7	jmenování
premiéra	premiér	k1gMnSc2	premiér
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
38	[number]	k4	38
<g/>
.	.	kIx.	.
sjezdu	sjezd	k1gInSc2	sjezd
ČSSD	ČSSD	kA	ČSSD
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2015	[number]	k4	2015
již	již	k6eAd1	již
po	po	k7c6	po
třetí	třetí	k4xOgFnSc6	třetí
zvolen	zvolit	k5eAaPmNgMnS	zvolit
předsedou	předseda	k1gMnSc7	předseda
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgMnS	získat
606	[number]	k4	606
hlasů	hlas	k1gInPc2	hlas
od	od	k7c2	od
713	[number]	k4	713
delegátů	delegát	k1gMnPc2	delegát
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
85	[number]	k4	85
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2015	[number]	k4	2015
byl	být	k5eAaImAgInS	být
napaden	napaden	k2eAgInSc1d1	napaden
Sobotkův	Sobotkův	k2eAgInSc1d1	Sobotkův
účet	účet	k1gInSc1	účet
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2016	[number]	k4	2016
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgNnP	objevit
a	a	k8xC	a
následně	následně	k6eAd1	následně
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
hackeři	hacker	k1gMnPc1	hacker
internetového	internetový	k2eAgInSc2d1	internetový
portálu	portál	k1gInSc2	portál
White	Whit	k1gInSc5	Whit
Media	medium	k1gNnPc4	medium
dostali	dostat	k5eAaPmAgMnP	dostat
do	do	k7c2	do
premiérovy	premiérův	k2eAgFnSc2d1	premiérova
soukromé	soukromý	k2eAgFnSc2d1	soukromá
emailové	emailový	k2eAgFnSc2d1	emailová
schránky	schránka	k1gFnSc2	schránka
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
jeho	jeho	k3xOp3gFnSc2	jeho
korespondence	korespondence	k1gFnSc2	korespondence
byla	být	k5eAaImAgFnS	být
zveřejněna	zveřejnit	k5eAaPmNgFnS	zveřejnit
<g/>
.	.	kIx.	.
</s>
<s>
Premiér	premiér	k1gMnSc1	premiér
se	se	k3xPyFc4	se
k	k	k7c3	k
celé	celý	k2eAgFnSc3d1	celá
věci	věc	k1gFnSc3	věc
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
policejní	policejní	k2eAgNnSc4d1	policejní
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ženatý	ženatý	k2eAgMnSc1d1	ženatý
<g/>
,	,	kIx,	,
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Olgou	Olga	k1gFnSc7	Olga
má	mít	k5eAaImIp3nS	mít
syny	syn	k1gMnPc4	syn
Davida	David	k1gMnSc4	David
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
a	a	k8xC	a
Martina	Martina	k1gFnSc1	Martina
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
o	o	k7c4	o
rok	rok	k1gInSc4	rok
a	a	k8xC	a
půl	půl	k6eAd1	půl
mladší	mladý	k2eAgFnSc1d2	mladší
sestra	sestra	k1gFnSc1	sestra
Marcela	Marcela	k1gFnSc1	Marcela
Sobotková	Sobotková	k1gFnSc1	Sobotková
je	být	k5eAaImIp3nS	být
televizní	televizní	k2eAgFnSc1d1	televizní
dramaturgyně	dramaturgyně	k1gFnSc1	dramaturgyně
a	a	k8xC	a
reportérka	reportérka	k1gFnSc1	reportérka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
TV	TV	kA	TV
Prima	prima	k6eAd1	prima
Očima	oko	k1gNnPc7	oko
Josefa	Josef	k1gMnSc2	Josef
Klímy	Klíma	k1gMnSc2	Klíma
nebo	nebo	k8xC	nebo
dříve	dříve	k6eAd2	dříve
Na	na	k7c4	na
vlastní	vlastní	k2eAgNnPc4d1	vlastní
oči	oko	k1gNnPc4	oko
na	na	k7c6	na
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
