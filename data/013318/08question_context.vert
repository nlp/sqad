<s>
Trailer	Trailer	k1gInSc1	Trailer
(	(	kIx(	(
<g/>
kinoupoutávka	kinoupoutávka	k1gFnSc1	kinoupoutávka
<g/>
,	,	kIx,	,
kinotrailer	kinotrailer	k1gInSc1	kinotrailer
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
krátká	krátký	k2eAgFnSc1d1	krátká
<g/>
,	,	kIx,	,
ne	ne	k9	ne
více	hodně	k6eAd2	hodně
než	než	k8xS	než
tři	tři	k4xCgFnPc4	tři
minuty	minuta	k1gFnPc4	minuta
trvající	trvající	k2eAgFnSc1d1	trvající
upoutávka	upoutávka	k1gFnSc1	upoutávka
na	na	k7c4	na
budoucí	budoucí	k2eAgInSc4d1	budoucí
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
uvolněná	uvolněný	k2eAgFnSc1d1	uvolněná
až	až	k9	až
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
před	před	k7c7	před
premiérou	premiéra	k1gFnSc7	premiéra
a	a	k8xC	a
většinou	většina	k1gFnSc7	většina
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
sestřihu	sestřih	k1gInSc2	sestřih
stěžejních	stěžejní	k2eAgFnPc2d1	stěžejní
nebo	nebo	k8xC	nebo
vizuálně	vizuálně	k6eAd1	vizuálně
nejzajímavějších	zajímavý	k2eAgFnPc2d3	nejzajímavější
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
vlastně	vlastně	k9	vlastně
o	o	k7c4	o
reklamu	reklama	k1gFnSc4	reklama
na	na	k7c4	na
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
určenou	určený	k2eAgFnSc4d1	určená
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
kinosály	kinosál	k1gInPc4	kinosál
<g/>
.	.	kIx.	.
</s>
