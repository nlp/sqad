<p>
<s>
Trailer	Trailer	k1gInSc1	Trailer
(	(	kIx(	(
<g/>
kinoupoutávka	kinoupoutávka	k1gFnSc1	kinoupoutávka
<g/>
,	,	kIx,	,
kinotrailer	kinotrailer	k1gInSc1	kinotrailer
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
krátká	krátký	k2eAgFnSc1d1	krátká
<g/>
,	,	kIx,	,
ne	ne	k9	ne
více	hodně	k6eAd2	hodně
než	než	k8xS	než
tři	tři	k4xCgFnPc4	tři
minuty	minuta	k1gFnPc4	minuta
trvající	trvající	k2eAgFnSc1d1	trvající
upoutávka	upoutávka	k1gFnSc1	upoutávka
na	na	k7c4	na
budoucí	budoucí	k2eAgInSc4d1	budoucí
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
uvolněná	uvolněný	k2eAgFnSc1d1	uvolněná
až	až	k9	až
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
před	před	k7c7	před
premiérou	premiéra	k1gFnSc7	premiéra
a	a	k8xC	a
většinou	většina	k1gFnSc7	většina
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
sestřihu	sestřih	k1gInSc2	sestřih
stěžejních	stěžejní	k2eAgFnPc2d1	stěžejní
nebo	nebo	k8xC	nebo
vizuálně	vizuálně	k6eAd1	vizuálně
nejzajímavějších	zajímavý	k2eAgFnPc2d3	nejzajímavější
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
vlastně	vlastně	k9	vlastně
o	o	k7c4	o
reklamu	reklama	k1gFnSc4	reklama
na	na	k7c4	na
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
určenou	určený	k2eAgFnSc4d1	určená
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
kinosály	kinosál	k1gInPc4	kinosál
<g/>
.	.	kIx.	.
</s>
<s>
Kinoupoutávky	Kinoupoutávka	k1gFnPc1	Kinoupoutávka
jsou	být	k5eAaImIp3nP	být
poté	poté	k6eAd1	poté
zpravidla	zpravidla	k6eAd1	zpravidla
využity	využít	k5eAaPmNgInP	využít
ještě	ještě	k9	ještě
jednou	jednou	k6eAd1	jednou
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
bonusové	bonusový	k2eAgInPc1d1	bonusový
materiály	materiál	k1gInPc1	materiál
na	na	k7c6	na
DVD	DVD	kA	DVD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
hollywoodským	hollywoodský	k2eAgInPc3d1	hollywoodský
filmům	film	k1gInPc3	film
s	s	k7c7	s
vyššími	vysoký	k2eAgInPc7d2	vyšší
rozpočty	rozpočet	k1gInPc7	rozpočet
vzniká	vznikat	k5eAaImIp3nS	vznikat
ne	ne	k9	ne
jeden	jeden	k4xCgInSc1	jeden
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
několik	několik	k4yIc4	několik
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
i	i	k9	i
přes	přes	k7c4	přes
20	[number]	k4	20
<g/>
)	)	kIx)	)
kinoupoutávek	kinoupoutávka	k1gFnPc2	kinoupoutávka
<g/>
,	,	kIx,	,
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
jaké	jaký	k3yIgNnSc4	jaký
promítání	promítání	k1gNnSc4	promítání
jsou	být	k5eAaImIp3nP	být
určeny	určen	k2eAgInPc1d1	určen
(	(	kIx(	(
<g/>
např.	např.	kA	např.
před	před	k7c7	před
promítáním	promítání	k1gNnSc7	promítání
filmu	film	k1gInSc2	film
hodnoceného	hodnocený	k2eAgMnSc2d1	hodnocený
PG-13	PG-13	k1gMnSc2	PG-13
se	se	k3xPyFc4	se
v	v	k7c6	v
kinoupoutávce	kinoupoutávka	k1gFnSc6	kinoupoutávka
nesmí	smět	k5eNaImIp3nS	smět
objevit	objevit	k5eAaPmF	objevit
záběry	záběr	k1gInPc4	záběr
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
by	by	kYmCp3nP	by
podléhaly	podléhat	k5eAaImAgInP	podléhat
hodnocení	hodnocení	k1gNnSc4	hodnocení
R	R	kA	R
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
taková	takový	k3xDgNnPc4	takový
představení	představení	k1gNnPc4	představení
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
speciální	speciální	k2eAgFnPc4d1	speciální
kinoupoutávky	kinoupoutávka	k1gFnPc4	kinoupoutávka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
se	se	k3xPyFc4	se
nevhodné	vhodný	k2eNgFnSc2d1	nevhodná
scény	scéna	k1gFnSc2	scéna
vypustí	vypustit	k5eAaPmIp3nS	vypustit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
kinoupoutávky	kinoupoutávka	k1gFnSc2	kinoupoutávka
k	k	k7c3	k
vysokorozpočtovým	vysokorozpočtův	k2eAgInPc3d1	vysokorozpočtův
filmům	film	k1gInPc3	film
mohou	moct	k5eAaImIp3nP	moct
dalece	dalece	k?	dalece
přesahovat	přesahovat	k5eAaImF	přesahovat
milion	milion	k4xCgInSc1	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kategorií	kategorie	k1gFnPc2	kategorie
samou	samý	k3xTgFnSc4	samý
o	o	k7c4	o
sobě	se	k3xPyFc3	se
jsou	být	k5eAaImIp3nP	být
televizní	televizní	k2eAgFnPc4d1	televizní
upoutávky	upoutávka	k1gFnPc4	upoutávka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
bývají	bývat	k5eAaImIp3nP	bývat
mnohem	mnohem	k6eAd1	mnohem
kratší	krátký	k2eAgFnPc1d2	kratší
než	než	k8xS	než
plnohodnotné	plnohodnotný	k2eAgFnPc1d1	plnohodnotná
kinoupoutávky	kinoupoutávka	k1gFnPc1	kinoupoutávka
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
sekund	sekunda	k1gFnPc2	sekunda
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
Austrálii	Austrálie	k1gFnSc6	Austrálie
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
i	i	k9	i
nezbytnou	nezbytný	k2eAgFnSc4d1	nezbytná
informaci	informace	k1gFnSc4	informace
o	o	k7c4	o
kategorizaci	kategorizace	k1gFnSc4	kategorizace
filmů	film	k1gInPc2	film
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
MPAA	MPAA	kA	MPAA
<g/>
:	:	kIx,	:
G	G	kA	G
<g/>
,	,	kIx,	,
PG	PG	kA	PG
<g/>
,	,	kIx,	,
PG-	PG-	k1gFnSc1	PG-
<g/>
13	[number]	k4	13
<g/>
,	,	kIx,	,
R	R	kA	R
<g/>
,	,	kIx,	,
NC-	NC-	k1gFnSc1	NC-
<g/>
17	[number]	k4	17
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
trailery	trailer	k1gInPc1	trailer
i	i	k8xC	i
TV	TV	kA	TV
spoty	spot	k1gInPc1	spot
pod	pod	k7c4	pod
upoutávky	upoutávka	k1gFnPc4	upoutávka
řadí	řadit	k5eAaImIp3nP	řadit
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
dvě	dva	k4xCgFnPc4	dva
různé	různý	k2eAgFnPc4d1	různá
kategorie	kategorie	k1gFnPc4	kategorie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
by	by	kYmCp3nP	by
neměly	mít	k5eNaImAgFnP	mít
být	být	k5eAaImF	být
zaměňovány	zaměňován	k2eAgFnPc1d1	zaměňována
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kinoupoutávky	Kinoupoutávka	k1gFnPc1	Kinoupoutávka
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
a	a	k8xC	a
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
internetu	internet	k1gInSc2	internet
<g/>
)	)	kIx)	)
stávají	stávat	k5eAaImIp3nP	stávat
velmi	velmi	k6eAd1	velmi
důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
v	v	k7c6	v
reklamě	reklama	k1gFnSc6	reklama
filmů	film	k1gInPc2	film
a	a	k8xC	a
mnohé	mnohý	k2eAgInPc1d1	mnohý
jsou	být	k5eAaImIp3nP	být
zdarma	zdarma	k6eAd1	zdarma
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Teaser	Teaser	k1gMnSc1	Teaser
</s>
</p>
<p>
<s>
Immediate	Immediat	k1gMnSc5	Immediat
Music	Music	k1gMnSc1	Music
</s>
</p>
<p>
<s>
X-Ray	X-Raa	k1gFnPc1	X-Raa
Dog	doga	k1gFnPc2	doga
</s>
</p>
<p>
<s>
Two	Two	k?	Two
Steps	Steps	k1gInSc1	Steps
from	from	k1gMnSc1	from
Hell	Hell	k1gMnSc1	Hell
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
trailer	trailer	k1gInSc1	trailer
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Stránky	stránka	k1gFnPc1	stránka
věnující	věnující	k2eAgFnPc1d1	věnující
se	se	k3xPyFc4	se
trailerům	trailer	k1gInPc3	trailer
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Seznam	seznam	k1gInSc1	seznam
kalifornských	kalifornský	k2eAgFnPc2d1	kalifornská
firem	firma	k1gFnPc2	firma
produkujících	produkující	k2eAgFnPc2d1	produkující
trailery	trailer	k1gInPc4	trailer
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Jak	jak	k8xC	jak
tě	ty	k3xPp2nSc4	ty
trailer	trailer	k1gMnSc1	trailer
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
kina	kino	k1gNnSc2	kino
-	-	kIx~	-
pořad	pořad	k1gInSc4	pořad
Čajovna	čajovna	k1gFnSc1	čajovna
o	o	k7c6	o
standardech	standard	k1gInPc6	standard
českých	český	k2eAgInPc2d1	český
i	i	k8xC	i
hollywoodských	hollywoodský	k2eAgInPc2d1	hollywoodský
trailerů	trailer	k1gInPc2	trailer
na	na	k7c6	na
Českém	český	k2eAgInSc6d1	český
rozhlase	rozhlas	k1gInSc6	rozhlas
Vltava	Vltava	k1gFnSc1	Vltava
</s>
</p>
