<s>
Měšťák	měšťák	k1gMnSc1	měšťák
šlechticem	šlechtic	k1gMnSc7	šlechtic
(	(	kIx(	(
<g/>
Le	Le	k1gFnSc1	Le
Bourgeois	Bourgeois	k1gFnSc2	Bourgeois
gentilhomme	gentilhomit	k5eAaPmRp1nP	gentilhomit
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
komedie	komedie	k1gFnSc1	komedie
–	–	k?	–
balet	balet	k1gInSc1	balet
o	o	k7c6	o
šesti	šest	k4xCc6	šest
jednáních	jednání	k1gNnPc6	jednání
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
pět	pět	k4xCc4	pět
jednání	jednání	k1gNnPc2	jednání
a	a	k8xC	a
závěrečný	závěrečný	k2eAgInSc1d1	závěrečný
"	"	kIx"	"
<g/>
balet	balet	k1gInSc1	balet
národů	národ	k1gInPc2	národ
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Představení	představení	k1gNnSc1	představení
na	na	k7c4	na
objednávku	objednávka	k1gFnSc4	objednávka
francouzského	francouzský	k2eAgMnSc2d1	francouzský
krále	král	k1gMnSc2	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
Moliè	Moliè	k1gMnPc1	Moliè
a	a	k8xC	a
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
Lully	Lullo	k1gNnPc7	Lullo
<g/>
.	.	kIx.	.
</s>
