<p>
<s>
Měšťák	měšťák	k1gMnSc1	měšťák
šlechticem	šlechtic	k1gMnSc7	šlechtic
(	(	kIx(	(
<g/>
Le	Le	k1gFnSc1	Le
Bourgeois	Bourgeois	k1gFnSc2	Bourgeois
gentilhomme	gentilhomit	k5eAaPmRp1nP	gentilhomit
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
komedie	komedie	k1gFnSc1	komedie
–	–	k?	–
balet	balet	k1gInSc1	balet
o	o	k7c6	o
šesti	šest	k4xCc6	šest
jednáních	jednání	k1gNnPc6	jednání
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
pět	pět	k4xCc4	pět
jednání	jednání	k1gNnPc2	jednání
a	a	k8xC	a
závěrečný	závěrečný	k2eAgInSc1d1	závěrečný
"	"	kIx"	"
<g/>
balet	balet	k1gInSc1	balet
národů	národ	k1gInPc2	národ
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Představení	představení	k1gNnSc1	představení
na	na	k7c4	na
objednávku	objednávka	k1gFnSc4	objednávka
francouzského	francouzský	k2eAgMnSc2d1	francouzský
krále	král	k1gMnSc2	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
Moliè	Moliè	k1gMnPc1	Moliè
a	a	k8xC	a
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
Lully	Lull	k1gMnPc7	Lull
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
a	a	k8xC	a
historie	historie	k1gFnSc1	historie
díla	dílo	k1gNnSc2	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1669	[number]	k4	1669
navštívil	navštívit	k5eAaPmAgInS	navštívit
dvůr	dvůr	k1gInSc1	dvůr
francouzského	francouzský	k2eAgMnSc2d1	francouzský
krále	král	k1gMnSc2	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
vyslanec	vyslanec	k1gMnSc1	vyslanec
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
Porty	porta	k1gFnSc2	porta
<g/>
,	,	kIx,	,
Soliman	Soliman	k1gMnSc1	Soliman
Aga	aga	k1gMnSc1	aga
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
počest	počest	k1gFnSc4	počest
uspořádal	uspořádat	k5eAaPmAgMnS	uspořádat
velkolepé	velkolepý	k2eAgFnSc2d1	velkolepá
slavnosti	slavnost	k1gFnSc2	slavnost
<g/>
.	.	kIx.	.
</s>
<s>
Vyslanec	vyslanec	k1gMnSc1	vyslanec
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
choval	chovat	k5eAaImAgMnS	chovat
přezíravě	přezíravě	k6eAd1	přezíravě
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
krále	král	k1gMnSc4	král
velmi	velmi	k6eAd1	velmi
rozladil	rozladit	k5eAaPmAgMnS	rozladit
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
u	u	k7c2	u
Moliè	Moliè	k1gMnSc2	Moliè
a	a	k8xC	a
Lullyho	Lully	k1gMnSc2	Lully
objednal	objednat	k5eAaPmAgInS	objednat
divadelní	divadelní	k2eAgInSc1d1	divadelní
představení	představení	k1gNnSc2	představení
se	s	k7c7	s
"	"	kIx"	"
<g/>
směšným	směšný	k2eAgInSc7d1	směšný
tureckým	turecký	k2eAgInSc7d1	turecký
baletem	balet	k1gInSc7	balet
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vyslance	vyslanec	k1gMnSc4	vyslanec
zesměšnil	zesměšnit	k5eAaPmAgInS	zesměšnit
a	a	k8xC	a
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
pomsta	pomsta	k1gFnSc1	pomsta
zapsala	zapsat	k5eAaPmAgFnS	zapsat
do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Autoři	autor	k1gMnPc1	autor
sepsali	sepsat	k5eAaPmAgMnP	sepsat
komedii	komedie	k1gFnSc4	komedie
–	–	k?	–
balet	balet	k1gInSc1	balet
Měšťák	měšťák	k1gMnSc1	měšťák
šlechticem	šlechtic	k1gMnSc7	šlechtic
<g/>
,	,	kIx,	,
příběh	příběh	k1gInSc1	příběh
komického	komický	k2eAgMnSc2d1	komický
měšťáckého	měšťácký	k2eAgMnSc2d1	měšťácký
zbohatlíka	zbohatlík	k1gMnSc2	zbohatlík
Jourdaina	Jourdain	k1gMnSc2	Jourdain
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
stát	stát	k5eAaPmF	stát
se	se	k3xPyFc4	se
šlechticem	šlechtic	k1gMnSc7	šlechtic
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
karikovanou	karikovaný	k2eAgFnSc7d1	karikovaná
postavou	postava	k1gFnSc7	postava
je	být	k5eAaImIp3nS	být
hrabě	hrabě	k1gMnSc1	hrabě
Dorant	Dorant	k1gMnSc1	Dorant
<g/>
,	,	kIx,	,
zchudlý	zchudlý	k2eAgMnSc1d1	zchudlý
šlechtic	šlechtic	k1gMnSc1	šlechtic
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
podbízí	podbízet	k5eAaImIp3nS	podbízet
bohatému	bohatý	k2eAgMnSc3d1	bohatý
měšťanovi	měšťan	k1gMnSc3	měšťan
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
od	od	k7c2	od
něho	on	k3xPp3gInSc2	on
mohl	moct	k5eAaImAgInS	moct
vypůjčovat	vypůjčovat	k5eAaImF	vypůjčovat
peníze	peníz	k1gInPc4	peníz
<g/>
.	.	kIx.	.
</s>
<s>
Představení	představení	k1gNnSc1	představení
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
předvedeno	předvést	k5eAaPmNgNnS	předvést
králi	král	k1gMnSc3	král
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1670	[number]	k4	1670
<g/>
,	,	kIx,	,
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Chambord	Chambord	k1gInSc1	Chambord
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
autoři	autor	k1gMnPc1	autor
v	v	k7c6	v
představení	představení	k1gNnSc6	představení
i	i	k8xC	i
hráli	hrát	k5eAaImAgMnP	hrát
–	–	k?	–
Moliè	Moliè	k1gMnSc1	Moliè
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
hlavní	hlavní	k2eAgFnSc4d1	hlavní
postavu	postava	k1gFnSc4	postava
pana	pan	k1gMnSc2	pan
Jourdaina	Jourdain	k1gMnSc2	Jourdain
<g/>
,	,	kIx,	,
Lully	Lulla	k1gMnSc2	Lulla
hrál	hrát	k5eAaImAgMnS	hrát
Muftího	muftí	k1gMnSc4	muftí
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
byl	být	k5eAaImAgMnS	být
představením	představení	k1gNnSc7	představení
nadšen	nadchnout	k5eAaPmNgMnS	nadchnout
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
si	se	k3xPyFc3	se
jej	on	k3xPp3gInSc4	on
pětkrát	pětkrát	k6eAd1	pětkrát
opakovat	opakovat	k5eAaImF	opakovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1670	[number]	k4	1670
bylo	být	k5eAaImAgNnS	být
pak	pak	k6eAd1	pak
představení	představení	k1gNnSc1	představení
uvedeno	uvést	k5eAaPmNgNnS	uvést
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Formálně	formálně	k6eAd1	formálně
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
spojení	spojení	k1gNnSc4	spojení
divadelní	divadelní	k2eAgFnSc2d1	divadelní
hry	hra	k1gFnSc2	hra
(	(	kIx(	(
<g/>
komedie	komedie	k1gFnSc2	komedie
<g/>
)	)	kIx)	)
se	s	k7c7	s
zpěváckými	zpěvácký	k2eAgFnPc7d1	zpěvácká
a	a	k8xC	a
tanečními	taneční	k2eAgFnPc7d1	taneční
scénami	scéna	k1gFnPc7	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
tři	tři	k4xCgFnPc1	tři
složky	složka	k1gFnPc1	složka
jsou	být	k5eAaImIp3nP	být
organicky	organicky	k6eAd1	organicky
propojeny	propojit	k5eAaPmNgFnP	propojit
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
hra	hra	k1gFnSc1	hra
uváděna	uvádět	k5eAaImNgFnS	uvádět
i	i	k9	i
jako	jako	k9	jako
samostatné	samostatný	k2eAgNnSc4d1	samostatné
činoherní	činoherní	k2eAgNnSc4d1	činoherní
představení	představení	k1gNnSc4	představení
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
v	v	k7c6	v
Mahenově	Mahenův	k2eAgNnSc6d1	Mahenovo
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
staré	starý	k2eAgFnSc2d1	stará
hudby	hudba	k1gFnSc2	hudba
Oude	oud	k1gInSc5	oud
Muziek	Muziko	k1gNnPc2	Muziko
Utrecht	Utrecht	k2eAgMnSc1d1	Utrecht
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
představení	představení	k1gNnSc1	představení
uvedeno	uvést	k5eAaPmNgNnS	uvést
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
poprvé	poprvé	k6eAd1	poprvé
<g/>
,	,	kIx,	,
v	v	k7c6	v
plném	plný	k2eAgInSc6d1	plný
rozsahu	rozsah	k1gInSc6	rozsah
původní	původní	k2eAgFnSc2d1	původní
předlohy	předloha	k1gFnSc2	předloha
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1670	[number]	k4	1670
<g/>
,	,	kIx,	,
v	v	k7c6	v
podání	podání	k1gNnSc6	podání
francouzského	francouzský	k2eAgInSc2d1	francouzský
orchestru	orchestr	k1gInSc2	orchestr
Le	Le	k1gMnSc2	Le
Poè	Poè	k1gMnSc2	Poè
Harmonique	Harmoniqu	k1gMnSc2	Harmoniqu
s	s	k7c7	s
dirigentem	dirigent	k1gMnSc7	dirigent
Vincentem	Vincent	k1gMnSc7	Vincent
Dumestre	Dumestr	k1gInSc5	Dumestr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osoby	osoba	k1gFnSc2	osoba
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	dít	k5eAaBmRp2nS	dít
opery	opera	k1gFnSc2	opera
==	==	k?	==
</s>
</p>
<p>
<s>
Odehrává	odehrávat	k5eAaImIp3nS	odehrávat
se	se	k3xPyFc4	se
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
v	v	k7c6	v
domě	dům	k1gInSc6	dům
pana	pan	k1gMnSc2	pan
Jourdaina	Jourdain	k1gMnSc2	Jourdain
<g/>
,	,	kIx,	,
měšťana	měšťan	k1gMnSc2	měšťan
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zbohatl	zbohatnout	k5eAaPmAgMnS	zbohatnout
na	na	k7c6	na
obchodu	obchod	k1gInSc6	obchod
se	s	k7c7	s
suknem	sukno	k1gNnSc7	sukno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Prolog	prolog	k1gInSc4	prolog
===	===	k?	===
</s>
</p>
<p>
<s>
Žák	Žák	k1gMnSc1	Žák
učitele	učitel	k1gMnSc2	učitel
hudby	hudba	k1gFnSc2	hudba
skládá	skládat	k5eAaImIp3nS	skládat
serenádu	serenáda	k1gFnSc4	serenáda
na	na	k7c4	na
objednávku	objednávka	k1gFnSc4	objednávka
pana	pan	k1gMnSc2	pan
Jourdaina	Jourdain	k1gMnSc2	Jourdain
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1	[number]	k4	1
<g/>
.	.	kIx.	.
jednání	jednání	k1gNnSc2	jednání
===	===	k?	===
</s>
</p>
<p>
<s>
Učitel	učitel	k1gMnSc1	učitel
zpěvu	zpěv	k1gInSc2	zpěv
a	a	k8xC	a
učitel	učitel	k1gMnSc1	učitel
tance	tanec	k1gInSc2	tanec
pana	pan	k1gMnSc4	pan
Jourdaina	Jourdain	k1gMnSc4	Jourdain
si	se	k3xPyFc3	se
stěžují	stěžovat	k5eAaImIp3nP	stěžovat
na	na	k7c4	na
směšnost	směšnost	k1gFnSc4	směšnost
jeho	jeho	k3xOp3gFnPc2	jeho
snah	snaha	k1gFnPc2	snaha
stát	stát	k5eAaImF	stát
se	se	k3xPyFc4	se
šlechticem	šlechtic	k1gMnSc7	šlechtic
<g/>
.	.	kIx.	.
</s>
<s>
Shodnou	shodnout	k5eAaPmIp3nP	shodnout
se	se	k3xPyFc4	se
ale	ale	k9	ale
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
kladné	kladný	k2eAgFnSc6d1	kladná
stránce	stránka	k1gFnSc6	stránka
a	a	k8xC	a
tou	ten	k3xDgFnSc7	ten
je	být	k5eAaImIp3nS	být
měšec	měšec	k1gInSc1	měšec
pana	pan	k1gMnSc2	pan
Jourdaina	Jourdain	k1gMnSc2	Jourdain
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
a	a	k8xC	a
učitelé	učitel	k1gMnPc1	učitel
před	před	k7c7	před
ním	on	k3xPp3gNnSc7	on
vedou	vést	k5eAaImIp3nP	vést
spor	spor	k1gInSc4	spor
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
významnější	významný	k2eAgInSc4d2	významnější
zpěv	zpěv	k1gInSc4	zpěv
či	či	k8xC	či
tanec	tanec	k1gInSc4	tanec
<g/>
,	,	kIx,	,
dokládají	dokládat	k5eAaImIp3nP	dokládat
svá	svůj	k3xOyFgNnPc4	svůj
tvrzení	tvrzení	k1gNnPc4	tvrzení
ukázkami	ukázka	k1gFnPc7	ukázka
árií	árie	k1gFnPc2	árie
a	a	k8xC	a
tanců	tanec	k1gInPc2	tanec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2	[number]	k4	2
<g/>
.	.	kIx.	.
jednání	jednání	k1gNnSc2	jednání
===	===	k?	===
</s>
</p>
<p>
<s>
Přichází	přicházet	k5eAaImIp3nS	přicházet
učitel	učitel	k1gMnSc1	učitel
šermu	šerm	k1gInSc2	šerm
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
opovrhuje	opovrhovat	k5eAaImIp3nS	opovrhovat
předešlými	předešlý	k2eAgMnPc7d1	předešlý
učiteli	učitel	k1gMnPc7	učitel
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zbraně	zbraň	k1gFnPc1	zbraň
mají	mít	k5eAaImIp3nP	mít
převahu	převaha	k1gFnSc4	převaha
nad	nad	k7c7	nad
neužitečnými	užitečný	k2eNgFnPc7d1	neužitečná
naukami	nauka	k1gFnPc7	nauka
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
hudba	hudba	k1gFnSc1	hudba
a	a	k8xC	a
tanec	tanec	k1gInSc1	tanec
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
přichází	přicházet	k5eAaImIp3nS	přicházet
učitel	učitel	k1gMnSc1	učitel
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
hledí	hledět	k5eAaImIp3nS	hledět
svrchu	svrchu	k6eAd1	svrchu
na	na	k7c4	na
všechny	všechen	k3xTgMnPc4	všechen
tři	tři	k4xCgMnPc4	tři
předešlé	předešlý	k2eAgMnPc4d1	předešlý
<g/>
.	.	kIx.	.
</s>
<s>
Popudí	popudit	k5eAaPmIp3nS	popudit
je	on	k3xPp3gInPc4	on
a	a	k8xC	a
dostane	dostat	k5eAaPmIp3nS	dostat
od	od	k7c2	od
nich	on	k3xPp3gMnPc2	on
výprask	výprask	k1gInSc4	výprask
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
následující	následující	k2eAgFnSc6d1	následující
hodině	hodina	k1gFnSc6	hodina
filosofie	filosofie	k1gFnSc2	filosofie
předvádí	předvádět	k5eAaImIp3nS	předvádět
učitel	učitel	k1gMnSc1	učitel
filosofie	filosofie	k1gFnSc2	filosofie
groteskní	groteskní	k2eAgInSc1d1	groteskní
výklad	výklad	k1gInSc1	výklad
o	o	k7c6	o
samohláskách	samohláska	k1gFnPc6	samohláska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
přichází	přicházet	k5eAaImIp3nS	přicházet
krejčí	krejčí	k1gMnSc1	krejčí
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
tovaryši	tovaryš	k1gMnPc1	tovaryš
s	s	k7c7	s
novými	nový	k2eAgInPc7d1	nový
šaty	šat	k1gInPc7	šat
<g/>
,	,	kIx,	,
hodnými	hodný	k2eAgInPc7d1	hodný
šlechtice	šlechtic	k1gMnPc4	šlechtic
<g/>
.	.	kIx.	.
</s>
<s>
Krejčovští	krejčovský	k2eAgMnPc1d1	krejčovský
tovaryši	tovaryš	k1gMnPc1	tovaryš
oblékají	oblékat	k5eAaImIp3nP	oblékat
pana	pan	k1gMnSc2	pan
Jourdaina	Jourdain	k1gMnSc2	Jourdain
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
za	za	k7c2	za
jejich	jejich	k3xOp3gNnSc2	jejich
pochlebování	pochlebování	k1gNnSc2	pochlebování
odměňuje	odměňovat	k5eAaImIp3nS	odměňovat
bohatým	bohatý	k2eAgNnSc7d1	bohaté
spropitným	spropitné	k1gNnSc7	spropitné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
3	[number]	k4	3
<g/>
.	.	kIx.	.
jednání	jednání	k1gNnSc2	jednání
===	===	k?	===
</s>
</p>
<p>
<s>
Pan	Pan	k1gMnSc1	Pan
Jourdain	Jourdain	k1gMnSc1	Jourdain
předvádí	předvádět	k5eAaImIp3nS	předvádět
manželce	manželka	k1gFnSc3	manželka
a	a	k8xC	a
služce	služka	k1gFnSc3	služka
Nicole	Nicola	k1gFnSc3	Nicola
své	svůj	k3xOyFgInPc4	svůj
nové	nový	k2eAgInPc4d1	nový
šaty	šat	k1gInPc4	šat
<g/>
.	.	kIx.	.
</s>
<s>
Sklidí	sklidit	k5eAaPmIp3nS	sklidit
ale	ale	k9	ale
jejich	jejich	k3xOp3gInSc4	jejich
posměch	posměch	k1gInSc4	posměch
a	a	k8xC	a
odsudek	odsudek	k1gInSc4	odsudek
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
marnivost	marnivost	k1gFnSc4	marnivost
<g/>
.	.	kIx.	.
</s>
<s>
Přichází	přicházet	k5eAaImIp3nS	přicházet
hrabě	hrabě	k1gMnSc1	hrabě
Dorant	Dorant	k1gMnSc1	Dorant
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
zastává	zastávat	k5eAaImIp3nS	zastávat
a	a	k8xC	a
zato	zato	k6eAd1	zato
se	se	k3xPyFc4	se
mu	on	k3xPp3gInSc3	on
podaří	podařit	k5eAaPmIp3nS	podařit
od	od	k7c2	od
něho	on	k3xPp3gMnSc2	on
získat	získat	k5eAaPmF	získat
další	další	k2eAgFnSc4d1	další
finanční	finanční	k2eAgFnSc4d1	finanční
půjčku	půjčka	k1gFnSc4	půjčka
<g/>
.	.	kIx.	.
</s>
<s>
Slibuje	slibovat	k5eAaImIp3nS	slibovat
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
Jourdainovi	Jourdainův	k2eAgMnPc1d1	Jourdainův
dostaveníčko	dostaveníčko	k1gNnSc4	dostaveníčko
s	s	k7c7	s
krásnou	krásný	k2eAgFnSc7d1	krásná
markýzou	markýza	k1gFnSc7	markýza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
měšťan	měšťan	k1gMnSc1	měšťan
Kléont	Kléont	k1gMnSc1	Kléont
se	se	k3xPyFc4	se
přichází	přicházet	k5eAaImIp3nS	přicházet
ucházet	ucházet	k5eAaImF	ucházet
o	o	k7c4	o
Jourdainovu	Jourdainův	k2eAgFnSc4d1	Jourdainův
dceru	dcera	k1gFnSc4	dcera
Lucile	Lucila	k1gFnSc3	Lucila
<g/>
.	.	kIx.	.
</s>
<s>
Paní	paní	k1gFnSc1	paní
Jourdainová	Jourdainová	k1gFnSc1	Jourdainová
je	být	k5eAaImIp3nS	být
svazku	svazek	k1gInSc6	svazek
nakloněna	naklonit	k5eAaPmNgFnS	naklonit
<g/>
.	.	kIx.	.
</s>
<s>
Pan	Pan	k1gMnSc1	Pan
Jourdain	Jourdain	k1gMnSc1	Jourdain
jej	on	k3xPp3gNnSc4	on
ale	ale	k9	ale
odmítá	odmítat	k5eAaImIp3nS	odmítat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
rozhodnut	rozhodnout	k5eAaPmNgMnS	rozhodnout
provdat	provdat	k5eAaPmF	provdat
dceru	dcera	k1gFnSc4	dcera
jedině	jedině	k6eAd1	jedině
za	za	k7c4	za
šlechtice	šlechtic	k1gMnPc4	šlechtic
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
bohatá	bohatý	k2eAgFnSc1d1	bohatá
hostina	hostina	k1gFnSc1	hostina
<g/>
,	,	kIx,	,
doprovázená	doprovázený	k2eAgFnSc1d1	doprovázená
zpěvy	zpěv	k1gInPc7	zpěv
a	a	k8xC	a
tanečními	taneční	k2eAgNnPc7d1	taneční
vystoupeními	vystoupení	k1gNnPc7	vystoupení
zpěváků	zpěvák	k1gMnPc2	zpěvák
<g/>
,	,	kIx,	,
tanečníků	tanečník	k1gMnPc2	tanečník
a	a	k8xC	a
kuchtíků	kuchtík	k1gMnPc2	kuchtík
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
hostinu	hostina	k1gFnSc4	hostina
přivedl	přivést	k5eAaPmAgMnS	přivést
hrabě	hrabě	k1gMnSc1	hrabě
Dorant	Dorant	k1gMnSc1	Dorant
markýzu	markýza	k1gFnSc4	markýza
Dorimè	Dorimè	k1gFnSc2	Dorimè
<g/>
.	.	kIx.	.
</s>
<s>
Dvoření	dvoření	k1gNnSc1	dvoření
pana	pan	k1gMnSc2	pan
Jourdaina	Jourdain	k1gMnSc2	Jourdain
ale	ale	k8xC	ale
vyznívá	vyznívat	k5eAaImIp3nS	vyznívat
zcela	zcela	k6eAd1	zcela
komicky	komicky	k6eAd1	komicky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
4	[number]	k4	4
<g/>
.	.	kIx.	.
dějství	dějství	k1gNnSc6	dějství
===	===	k?	===
</s>
</p>
<p>
<s>
Přichází	přicházet	k5eAaImIp3nS	přicházet
paní	paní	k1gFnSc1	paní
Jourdainová	Jourdainový	k2eAgFnSc1d1	Jourdainový
a	a	k8xC	a
rázně	rázně	k6eAd1	rázně
ukončuje	ukončovat	k5eAaImIp3nS	ukončovat
hostinu	hostina	k1gFnSc4	hostina
a	a	k8xC	a
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
hosty	host	k1gMnPc4	host
z	z	k7c2	z
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
i	i	k9	i
zmařena	zmařen	k2eAgFnSc1d1	zmařena
snaha	snaha	k1gFnSc1	snaha
pana	pan	k1gMnSc2	pan
Jourdaina	Jourdain	k1gMnSc2	Jourdain
o	o	k7c4	o
dostaveníčko	dostaveníčko	k1gNnSc4	dostaveníčko
s	s	k7c7	s
markýzou	markýza	k1gFnSc7	markýza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c7	za
osamělým	osamělý	k2eAgMnSc7d1	osamělý
panem	pan	k1gMnSc7	pan
Jourdainem	Jourdain	k1gMnSc7	Jourdain
přichází	přicházet	k5eAaImIp3nS	přicházet
Corvielle	Corvielle	k1gInSc1	Corvielle
převlečený	převlečený	k2eAgInSc1d1	převlečený
za	za	k7c4	za
posla	posel	k1gMnSc4	posel
Velikého	veliký	k2eAgMnSc2d1	veliký
Turka	Turek	k1gMnSc2	Turek
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
syn	syn	k1gMnSc1	syn
se	se	k3xPyFc4	se
údajně	údajně	k6eAd1	údajně
uchází	ucházet	k5eAaImIp3nS	ucházet
o	o	k7c4	o
Jourdainovu	Jourdainův	k2eAgFnSc4d1	Jourdainův
dceru	dcera	k1gFnSc4	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
svatba	svatba	k1gFnSc1	svatba
mohla	moct	k5eAaImAgFnS	moct
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
prý	prý	k9	prý
ale	ale	k8xC	ale
nutné	nutný	k2eAgFnPc1d1	nutná
pana	pan	k1gMnSc2	pan
Jourdaina	Jourdaina	k1gFnSc1	Jourdaina
rovněž	rovněž	k9	rovněž
povýšit	povýšit	k5eAaPmF	povýšit
do	do	k7c2	do
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Přichází	přicházet	k5eAaImIp3nS	přicházet
"	"	kIx"	"
<g/>
Muftí	muftí	k1gMnSc1	muftí
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
převlečený	převlečený	k2eAgInSc4d1	převlečený
Kléont	Kléont	k1gInSc4	Kléont
<g/>
)	)	kIx)	)
a	a	k8xC	a
během	během	k7c2	během
groteskního	groteskní	k2eAgInSc2d1	groteskní
obřadu	obřad	k1gInSc2	obřad
(	(	kIx(	(
<g/>
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
celý	celý	k2eAgInSc1d1	celý
zpíván	zpíván	k2eAgInSc1d1	zpíván
sabirem	sabir	k1gMnSc7	sabir
<g/>
)	)	kIx)	)
z	z	k7c2	z
pana	pan	k1gMnSc2	pan
Jourdaina	Jourdain	k1gMnSc2	Jourdain
udělá	udělat	k5eAaPmIp3nS	udělat
tureckého	turecký	k2eAgMnSc2d1	turecký
"	"	kIx"	"
<g/>
šlechtice	šlechtic	k1gMnSc2	šlechtic
<g/>
"	"	kIx"	"
jménem	jméno	k1gNnSc7	jméno
Mamamouchi	Mamamouch	k1gFnSc2	Mamamouch
<g/>
.	.	kIx.	.
</s>
<s>
Jourdainova	Jourdainův	k2eAgFnSc1d1	Jourdainův
životní	životní	k2eAgFnSc1d1	životní
touha	touha	k1gFnSc1	touha
je	být	k5eAaImIp3nS	být
konečně	konečně	k6eAd1	konečně
splněna	splnit	k5eAaPmNgFnS	splnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
5	[number]	k4	5
<g/>
.	.	kIx.	.
dějství	dějství	k1gNnSc6	dějství
===	===	k?	===
</s>
</p>
<p>
<s>
Pan	Pan	k1gMnSc1	Pan
Jourdain	Jourdain	k1gMnSc1	Jourdain
přichází	přicházet	k5eAaImIp3nS	přicházet
za	za	k7c7	za
manželkou	manželka	k1gFnSc7	manželka
a	a	k8xC	a
nařizuje	nařizovat	k5eAaImIp3nS	nařizovat
přípravu	příprava	k1gFnSc4	příprava
svatby	svatba	k1gFnSc2	svatba
dcery	dcera	k1gFnSc2	dcera
Lucile	Lucila	k1gFnSc3	Lucila
se	s	k7c7	s
synem	syn	k1gMnSc7	syn
Velkého	velký	k2eAgInSc2d1	velký
Turka	turek	k1gInSc2	turek
<g/>
.	.	kIx.	.
</s>
<s>
Paní	paní	k1gFnSc1	paní
Jourdainová	Jourdainová	k1gFnSc1	Jourdainová
je	být	k5eAaImIp3nS	být
zasvěcena	zasvětit	k5eAaPmNgFnS	zasvětit
do	do	k7c2	do
celé	celý	k2eAgFnSc2d1	celá
komedie	komedie	k1gFnSc2	komedie
a	a	k8xC	a
přidává	přidávat	k5eAaImIp3nS	přidávat
se	se	k3xPyFc4	se
ke	k	k7c3	k
"	"	kIx"	"
<g/>
spiklencům	spiklenec	k1gMnPc3	spiklenec
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Proběhne	proběhnout	k5eAaPmIp3nS	proběhnout
trojnásobná	trojnásobný	k2eAgFnSc1d1	trojnásobná
svatba	svatba	k1gFnSc1	svatba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
vezmou	vzít	k5eAaPmIp3nP	vzít
Lucile	Lucila	k1gFnSc3	Lucila
a	a	k8xC	a
Kléont	Kléont	k1gInSc1	Kléont
<g/>
,	,	kIx,	,
služka	služka	k1gFnSc1	služka
Nicole	Nicole	k1gFnSc1	Nicole
a	a	k8xC	a
sluha	sluha	k1gMnSc1	sluha
Covielle	Covielle	k1gFnSc1	Covielle
a	a	k8xC	a
markýza	markýza	k1gFnSc1	markýza
Dorimè	Dorimè	k1gFnPc2	Dorimè
uzavře	uzavřít	k5eAaPmIp3nS	uzavřít
sňatek	sňatek	k1gInSc1	sňatek
s	s	k7c7	s
hrabětem	hrabě	k1gMnSc7	hrabě
Dorantem	Dorant	k1gMnSc7	Dorant
<g/>
.	.	kIx.	.
</s>
<s>
Jediná	jediný	k2eAgFnSc1d1	jediná
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
netuší	tušit	k5eNaImIp3nS	tušit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
obětí	oběť	k1gFnSc7	oběť
spiknutí	spiknutí	k1gNnSc2	spiknutí
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pan	pan	k1gMnSc1	pan
Jourdain	Jourdain	k1gMnSc1	Jourdain
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
6	[number]	k4	6
<g/>
.	.	kIx.	.
dějství	dějství	k1gNnSc6	dějství
===	===	k?	===
</s>
</p>
<p>
<s>
Šesté	šestý	k4xOgNnSc1	šestý
dějství	dějství	k1gNnSc1	dějství
bývá	bývat	k5eAaImIp3nS	bývat
nazýváno	nazýván	k2eAgNnSc1d1	nazýváno
také	také	k9	také
Balet	balet	k1gInSc4	balet
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
Ballet	Ballet	k1gInSc1	Ballet
des	des	k1gNnSc1	des
Nations	Nations	k1gInSc1	Nations
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
společnost	společnost	k1gFnSc1	společnost
odchází	odcházet	k5eAaImIp3nS	odcházet
do	do	k7c2	do
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
divadlem	divadlo	k1gNnSc7	divadlo
se	se	k3xPyFc4	se
tlačí	tlačit	k5eAaImIp3nS	tlačit
dav	dav	k1gInSc1	dav
návštěvníků	návštěvník	k1gMnPc2	návštěvník
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
francouzských	francouzský	k2eAgInPc2d1	francouzský
krajů	kraj	k1gInPc2	kraj
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
kterými	který	k3yIgInPc7	který
se	se	k3xPyFc4	se
prodírá	prodírat	k5eAaImIp3nS	prodírat
prodavač	prodavač	k1gMnSc1	prodavač
divadelních	divadelní	k2eAgMnPc2d1	divadelní
programů	program	k1gInPc2	program
sužovaný	sužovaný	k2eAgInSc1d1	sužovaný
dotěrnými	dotěrný	k2eAgInPc7d1	dotěrný
uličníky	uličník	k1gInPc7	uličník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Balet	balet	k1gInSc1	balet
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
části	část	k1gFnPc4	část
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
španělský	španělský	k2eAgInSc1d1	španělský
balet	balet	k1gInSc1	balet
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
italský	italský	k2eAgInSc1d1	italský
balet	balet	k1gInSc1	balet
s	s	k7c7	s
prvky	prvek	k1gInPc7	prvek
commedie	commedie	k1gFnSc2	commedie
dell	dell	k1gInSc1	dell
<g/>
́	́	k?	́
<g/>
arte	artat	k5eAaPmIp3nS	artat
<g/>
,	,	kIx,	,
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
zde	zde	k6eAd1	zde
Scaramouche	Scaramouch	k1gInSc2	Scaramouch
a	a	k8xC	a
Harlekýn	harlekýn	k1gMnSc1	harlekýn
</s>
</p>
<p>
<s>
francouzský	francouzský	k2eAgInSc1d1	francouzský
balet	balet	k1gInSc1	balet
s	s	k7c7	s
lidovými	lidový	k2eAgInPc7d1	lidový
prvky	prvek	k1gInPc7	prvek
tanců	tanec	k1gInPc2	tanec
z	z	k7c2	z
kraje	kraj	k1gInSc2	kraj
PoitouVšechny	PoitouVšechen	k2eAgInPc1d1	PoitouVšechen
národy	národ	k1gInPc1	národ
i	i	k8xC	i
divadelní	divadelní	k2eAgFnPc1d1	divadelní
postavy	postava	k1gFnPc1	postava
se	se	k3xPyFc4	se
promíchají	promíchat	k5eAaPmIp3nP	promíchat
ve	v	k7c6	v
finální	finální	k2eAgFnSc6d1	finální
sborové	sborový	k2eAgFnSc6d1	sborová
scéně	scéna	k1gFnSc6	scéna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Měšťák	měšťák	k1gMnSc1	měšťák
šlechticem	šlechtic	k1gMnSc7	šlechtic
na	na	k7c6	na
českých	český	k2eAgFnPc6d1	Česká
scénách	scéna	k1gFnPc6	scéna
==	==	k?	==
</s>
</p>
<p>
<s>
Hra	hra	k1gFnSc1	hra
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1926	[number]	k4	1926
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Bařtipán	bařtipán	k1gMnSc1	bařtipán
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikoliv	nikoliv	k9	nikoliv
s	s	k7c7	s
Lullyho	Lullyha	k1gFnSc5	Lullyha
hudbou	hudba	k1gFnSc7	hudba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
hudbou	hudba	k1gFnSc7	hudba
Ervína	Ervín	k1gMnSc2	Ervín
Schulhoffa	Schulhoff	k1gMnSc2	Schulhoff
<g/>
.	.	kIx.	.
</s>
<s>
Scénu	scéna	k1gFnSc4	scéna
a	a	k8xC	a
kostýmy	kostým	k1gInPc4	kostým
pro	pro	k7c4	pro
hru	hra	k1gFnSc4	hra
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
Josef	Josef	k1gMnSc1	Josef
Čapek	Čapek	k1gMnSc1	Čapek
<g/>
.	.	kIx.	.
</s>
<s>
Táž	týž	k3xTgFnSc1	týž
úprava	úprava	k1gFnSc1	úprava
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
hrána	hrát	k5eAaImNgFnS	hrát
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
<g/>
Činohra	činohra	k1gFnSc1	činohra
Měšťák	měšťák	k1gMnSc1	měšťák
šlechticem	šlechtic	k1gMnSc7	šlechtic
byla	být	k5eAaImAgNnP	být
hrána	hrát	k5eAaImNgNnP	hrát
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
na	na	k7c4	na
Veveří	veveří	k2eAgFnSc4d1	veveří
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
<g/>
:	:	kIx,	:
Karel	Karel	k1gMnSc1	Karel
M.	M.	kA	M.
Klos	Klos	k1gInSc1	Klos
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Vladimír	Vladimír	k1gMnSc1	Vladimír
Šimáček	Šimáček	k1gMnSc1	Šimáček
<g/>
,	,	kIx,	,
premiéra	premiéra	k1gFnSc1	premiéra
<g/>
:	:	kIx,	:
24	[number]	k4	24
<g/>
.	.	kIx.	.
duben	duben	k1gInSc1	duben
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
derniéra	derniéra	k1gFnSc1	derniéra
<g/>
:	:	kIx,	:
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1926	[number]	k4	1926
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
na	na	k7c6	na
Výstavišti	výstaviště	k1gNnSc6	výstaviště
–	–	k?	–
Výstaviště	výstaviště	k1gNnSc4	výstaviště
Pisárky	Pisárka	k1gFnSc2	Pisárka
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
<g/>
:	:	kIx,	:
Jiří	Jiří	k1gMnSc1	Jiří
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Horan	Horan	k1gMnSc1	Horan
<g/>
,	,	kIx,	,
premiéra	premiéra	k1gFnSc1	premiéra
<g/>
:	:	kIx,	:
24	[number]	k4	24
<g/>
.	.	kIx.	.
červen	červen	k1gInSc1	červen
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
derniéra	derniéra	k1gFnSc1	derniéra
<g/>
:	:	kIx,	:
17	[number]	k4	17
<g/>
.	.	kIx.	.
červen	červen	k1gInSc1	červen
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
<g/>
:	:	kIx,	:
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Kadlec	Kadlec	k1gMnSc1	Kadlec
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Gustav	Gustav	k1gMnSc1	Gustav
Skála	Skála	k1gMnSc1	Skála
<g/>
,	,	kIx,	,
premiéra	premiéra	k1gFnSc1	premiéra
<g/>
:	:	kIx,	:
20	[number]	k4	20
<g/>
.	.	kIx.	.
duben	duben	k1gInSc1	duben
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2006	[number]	k4	2006
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
Stavovském	stavovský	k2eAgNnSc6d1	Stavovské
divadle	divadlo	k1gNnSc6	divadlo
uvedeno	uveden	k2eAgNnSc1d1	uvedeno
představení	představení	k1gNnSc1	představení
v	v	k7c6	v
nastudování	nastudování	k1gNnSc6	nastudování
francouzského	francouzský	k2eAgInSc2d1	francouzský
souboru	soubor	k1gInSc2	soubor
Le	Le	k1gMnSc2	Le
Poè	Poè	k1gMnSc2	Poè
Harmonique	Harmoniqu	k1gMnSc2	Harmoniqu
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Vincenta	Vincent	k1gMnSc2	Vincent
Dumestre	Dumestr	k1gInSc5	Dumestr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
realizaci	realizace	k1gFnSc6	realizace
tohoto	tento	k3xDgNnSc2	tento
představení	představení	k1gNnSc2	představení
se	se	k3xPyFc4	se
podíleli	podílet	k5eAaImAgMnP	podílet
i	i	k9	i
čeští	český	k2eAgMnPc1d1	český
hudebníci	hudebník	k1gMnPc1	hudebník
z	z	k7c2	z
orchestru	orchestr	k1gInSc2	orchestr
Musica	Musicus	k1gMnSc2	Musicus
Florea	Floreus	k1gMnSc2	Floreus
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Marka	Marek	k1gMnSc2	Marek
Štryncla	Štryncl	k1gMnSc2	Štryncl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nahrávky	nahrávka	k1gFnSc2	nahrávka
==	==	k?	==
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
–	–	k?	–
Le	Le	k1gMnSc4	Le
Poè	Poè	k1gFnSc2	Poè
Harmonique	Harmoniqu	k1gMnSc4	Harmoniqu
dirigent	dirigent	k1gMnSc1	dirigent
<g/>
:	:	kIx,	:
Vincent	Vincent	k1gMnSc1	Vincent
Dumestre	Dumestr	k1gInSc5	Dumestr
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Benjamin	Benjamin	k1gMnSc1	Benjamin
Lazar	Lazar	k1gMnSc1	Lazar
<g/>
,	,	kIx,	,
choreografie	choreografie	k1gFnSc1	choreografie
<g/>
:	:	kIx,	:
Cécile	Cécila	k1gFnSc3	Cécila
Roussat	Roussat	k1gFnSc2	Roussat
<g/>
,	,	kIx,	,
scéna	scéna	k1gFnSc1	scéna
<g/>
:	:	kIx,	:
Adeline	Adelin	k1gInSc5	Adelin
Caron	Caron	k1gNnSc1	Caron
<g/>
,	,	kIx,	,
kostýmy	kostým	k1gInPc1	kostým
<g/>
:	:	kIx,	:
Alain	Alain	k2eAgInSc1d1	Alain
Blanchot	Blanchot	k1gInSc1	Blanchot
<g/>
,	,	kIx,	,
masky	maska	k1gFnPc1	maska
<g/>
:	:	kIx,	:
Mathilde	Mathild	k1gInSc5	Mathild
Benmoussa	Benmoussa	k1gFnSc1	Benmoussa
<g/>
,	,	kIx,	,
světla	světlo	k1gNnPc1	světlo
<g/>
:	:	kIx,	:
Christophe	Christophe	k1gFnSc1	Christophe
Naillet	Naillet	k1gInSc1	Naillet
<g/>
.	.	kIx.	.
</s>
<s>
Alpha	Alpha	k1gFnSc1	Alpha
700	[number]	k4	700
<g/>
.	.	kIx.	.
</s>
<s>
Nahrávka	nahrávka	k1gFnSc1	nahrávka
byla	být	k5eAaImAgFnS	být
oceněna	ocenit	k5eAaPmNgFnS	ocenit
cenou	cena	k1gFnSc7	cena
le	le	k?	le
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
du	du	k?	du
Disque	Disque	k1gInSc1	Disque
et	et	k?	et
du	du	k?	du
DVD	DVD	kA	DVD
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Académie	Académie	k1gFnSc1	Académie
Charles-Cros	Charles-Crosa	k1gFnPc2	Charles-Crosa
<g/>
,	,	kIx,	,
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
barokní	barokní	k2eAgFnSc2d1	barokní
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Vincent	Vincent	k1gMnSc1	Vincent
Dumestre	Dumestr	k1gInSc5	Dumestr
<g/>
,	,	kIx,	,
Benjamin	Benjamin	k1gMnSc1	Benjamin
Lazar	Lazar	k1gMnSc1	Lazar
<g/>
,	,	kIx,	,
Cécile	Cécila	k1gFnSc3	Cécila
Roussat	Roussat	k1gFnSc2	Roussat
<g/>
:	:	kIx,	:
Měšťák	měšťák	k1gMnSc1	měšťák
šlechticem	šlechtic	k1gMnSc7	šlechtic
/	/	kIx~	/
Le	Le	k1gMnSc2	Le
Bourgeois	Bourgeois	k1gFnSc7	Bourgeois
gentilhomme	gentilhomit	k5eAaPmRp1nP	gentilhomit
<g/>
,	,	kIx,	,
texty	text	k1gInPc4	text
divadelního	divadelní	k2eAgInSc2d1	divadelní
programu	program	k1gInSc2	program
<g/>
,	,	kIx,	,
Národní	národní	k2eAgNnSc4d1	národní
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
24	[number]	k4	24
<g/>
.	.	kIx.	.
březen	březen	k1gInSc4	březen
2006	[number]	k4	2006
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Měšťák	měšťák	k1gMnSc1	měšťák
šlechticem	šlechtic	k1gMnSc7	šlechtic
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc4	dílo
Le	Le	k1gFnSc2	Le
Bourgeois	Bourgeois	k1gFnSc2	Bourgeois
gentilhomme	gentilhomit	k5eAaPmRp1nP	gentilhomit
(	(	kIx(	(
<g/>
Imprimerie	Imprimerie	k1gFnPc4	Imprimerie
nationale	nationale	k6eAd1	nationale
<g/>
)	)	kIx)	)
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Měšťák	měšťák	k1gMnSc1	měšťák
šlechticem	šlechtic	k1gMnSc7	šlechtic
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
</s>
</p>
