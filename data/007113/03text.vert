<s>
Jídelní	jídelní	k2eAgInSc4d1	jídelní
lístek	lístek	k1gInSc4	lístek
(	(	kIx(	(
<g/>
Nápojový	nápojový	k2eAgInSc4d1	nápojový
lístek	lístek	k1gInSc4	lístek
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
přehled	přehled	k1gInSc4	přehled
produktů	produkt	k1gInPc2	produkt
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
nabízí	nabízet	k5eAaImIp3nS	nabízet
jídelna	jídelna	k1gFnSc1	jídelna
<g/>
,	,	kIx,	,
restaurace	restaurace	k1gFnSc1	restaurace
nebo	nebo	k8xC	nebo
bufet	bufet	k1gInSc1	bufet
<g/>
.	.	kIx.	.
</s>
<s>
Informuje	informovat	k5eAaBmIp3nS	informovat
hosty	host	k1gMnPc4	host
o	o	k7c6	o
nabídce	nabídka	k1gFnSc6	nabídka
a	a	k8xC	a
cenách	cena	k1gFnPc6	cena
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jídelní	jídelní	k2eAgInSc4d1	jídelní
lístek	lístek	k1gInSc4	lístek
seznam	seznam	k1gInSc4	seznam
jídel	jídlo	k1gNnPc2	jídlo
<g/>
,	,	kIx,	,
nápojů	nápoj	k1gInPc2	nápoj
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
celých	celý	k2eAgNnPc2d1	celé
menu	menu	k1gNnPc2	menu
<g/>
.	.	kIx.	.
</s>
<s>
Položky	položka	k1gFnPc1	položka
jídelního	jídelní	k2eAgInSc2d1	jídelní
lístku	lístek	k1gInSc2	lístek
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
do	do	k7c2	do
skupin	skupina	k1gFnPc2	skupina
(	(	kIx(	(
<g/>
např.	např.	kA	např.
bezmasé	bezmasý	k2eAgInPc4d1	bezmasý
pokrmy	pokrm	k1gInPc4	pokrm
<g/>
,	,	kIx,	,
drůbež	drůbež	k1gFnSc4	drůbež
<g/>
,	,	kIx,	,
nealkoholické	alkoholický	k2eNgInPc4d1	nealkoholický
nápoje	nápoj	k1gInPc4	nápoj
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
Jídelní	jídelní	k2eAgInSc4d1	jídelní
lístek	lístek	k1gInSc4	lístek
je	být	k5eAaImIp3nS	být
seznam	seznam	k1gInSc1	seznam
nabízených	nabízený	k2eAgInPc2d1	nabízený
pokrmů	pokrm	k1gInPc2	pokrm
a	a	k8xC	a
příloh	příloha	k1gFnPc2	příloha
<g/>
.	.	kIx.	.
</s>
<s>
Svojí	svůj	k3xOyFgFnSc7	svůj
formou	forma	k1gFnSc7	forma
i	i	k8xC	i
obsahem	obsah	k1gInSc7	obsah
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
především	především	k9	především
kuchaře	kuchař	k1gMnPc4	kuchař
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ostatní	ostatní	k2eAgMnPc4d1	ostatní
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
včetně	včetně	k7c2	včetně
podnikatele	podnikatel	k1gMnSc2	podnikatel
<g/>
.	.	kIx.	.
</s>
<s>
Základním	základní	k2eAgInSc7d1	základní
požadavkem	požadavek	k1gInSc7	požadavek
na	na	k7c4	na
jídelní	jídelní	k2eAgInSc4d1	jídelní
lístek	lístek	k1gInSc4	lístek
je	být	k5eAaImIp3nS	být
pestrý	pestrý	k2eAgInSc1d1	pestrý
výběr	výběr	k1gInSc1	výběr
pokrmů	pokrm	k1gInPc2	pokrm
a	a	k8xC	a
příloh	příloha	k1gFnPc2	příloha
<g/>
,	,	kIx,	,
dobrá	dobrý	k2eAgFnSc1d1	dobrá
přehlednost	přehlednost	k1gFnSc1	přehlednost
<g/>
,	,	kIx,	,
čitelnost	čitelnost	k1gFnSc1	čitelnost
a	a	k8xC	a
vkusná	vkusný	k2eAgFnSc1d1	vkusná
úprava	úprava	k1gFnSc1	úprava
<g/>
.	.	kIx.	.
</s>
<s>
Jídelní	jídelní	k2eAgInSc1d1	jídelní
lístek	lístek	k1gInSc1	lístek
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
nabídku	nabídka	k1gFnSc4	nabídka
<g/>
,	,	kIx,	,
objednávání	objednávání	k1gNnSc4	objednávání
<g/>
,	,	kIx,	,
bonování	bonování	k1gNnSc4	bonování
<g/>
,	,	kIx,	,
expedici	expedice	k1gFnSc6	expedice
<g/>
,	,	kIx,	,
zúčtování	zúčtování	k1gNnSc6	zúčtování
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
i	i	k9	i
jako	jako	k9	jako
kontrolní	kontrolní	k2eAgInSc4d1	kontrolní
podklad	podklad	k1gInSc4	podklad
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jídelní	jídelní	k2eAgInSc4d1	jídelní
lístek	lístek	k1gInSc4	lístek
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
