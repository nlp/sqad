<s>
Ruchadlo	ruchadlo	k1gNnSc1
</s>
<s>
Ruchadlo	ruchadlo	k1gNnSc1
bratranců	bratranec	k1gMnPc2
Veverkových	Veverkových	k2eAgMnPc2d1
</s>
<s>
Rodný	rodný	k2eAgInSc1d1
domek	domek	k1gInSc1
Václava	Václav	k1gMnSc2
Veverky	Veverka	k1gMnSc2
<g/>
,	,	kIx,
kovárna	kovárna	k1gFnSc1
v	v	k7c6
Rybitví	Rybitví	k1gNnSc6
</s>
<s>
Pomník	pomník	k1gInSc1
bratranců	bratranec	k1gMnPc2
Veverkových	Veverkových	k2eAgMnSc1d1
v	v	k7c6
Pardubicích	Pardubice	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Zde	zde	k6eAd1
byla	být	k5eAaImAgFnS
vyorána	vyorat	k5eAaPmNgFnS
první	první	k4xOgFnSc1
brázda	brázda	k1gFnSc1
ruchadlem	ruchadlo	k1gNnSc7
</s>
<s>
Ruchadlo	ruchadlo	k1gNnSc1
je	být	k5eAaImIp3nS
pluh	pluh	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
půdu	půda	k1gFnSc4
nejen	nejen	k6eAd1
rozrušuje	rozrušovat	k5eAaImIp3nS
a	a	k8xC
provzdušňuje	provzdušňovat	k5eAaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
převrací	převracet	k5eAaImIp3nP
ji	on	k3xPp3gFnSc4
tak	tak	k9
<g/>
,	,	kIx,
že	že	k8xS
vrchní	vrchní	k2eAgFnSc1d1
vrstva	vrstva	k1gFnSc1
se	se	k3xPyFc4
dostává	dostávat	k5eAaImIp3nS
dolů	dolů	k6eAd1
a	a	k8xC
spodní	spodní	k2eAgFnSc1d1
vrstva	vrstva	k1gFnSc1
odpočaté	odpočatý	k2eAgFnSc2d1
půdy	půda	k1gFnSc2
navrch	navrch	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nadto	nadto	k6eAd1
je	být	k5eAaImIp3nS
orba	orba	k1gFnSc1
méně	málo	k6eAd2
namáhavá	namáhavý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vynalezli	vynaleznout	k5eAaPmAgMnP
jej	on	k3xPp3gInSc2
roku	rok	k1gInSc2
1827	#num#	k4
bratranci	bratranec	k1gMnPc1
Václav	Václav	k1gMnSc1
a	a	k8xC
František	František	k1gMnSc1
Veverkovi	Veverka	k2eAgMnPc1d1
z	z	k7c2
Rybitví	Rybitví	k1gNnPc2
u	u	k7c2
Pardubic	Pardubice	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
o	o	k7c4
vynález	vynález	k1gInSc4
byl	být	k5eAaImAgInS
mezi	mezi	k7c7
zemědělci	zemědělec	k1gMnPc7
velký	velký	k2eAgInSc4d1
zájem	zájem	k1gInSc4
<g/>
,	,	kIx,
Veverkové	Veverkové	k2eAgInPc4d1
na	na	k7c6
něm	on	k3xPp3gMnSc6
neprofitovali	profitovat	k5eNaBmAgMnP
<g/>
,	,	kIx,
ke	k	k7c3
svému	svůj	k3xOyFgInSc3
prospěchu	prospěch	k1gInSc3
ho	on	k3xPp3gMnSc4
využili	využít	k5eAaPmAgMnP
jiní	jiný	k1gMnPc1
a	a	k8xC
původ	původ	k1gInSc1
vynálezu	vynález	k1gInSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
ve	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnPc2
předmětem	předmět	k1gInSc7
národnostního	národnostní	k2eAgInSc2d1
boje	boj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Vynález	vynález	k1gInSc1
ruchadla	ruchadlo	k1gNnSc2
</s>
<s>
Když	když	k8xS
se	se	k3xPyFc4
František	František	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1823	#num#	k4
oženil	oženit	k5eAaPmAgInS
a	a	k8xC
ujal	ujmout	k5eAaPmAgInS
hospodaření	hospodaření	k1gNnSc4
<g/>
,	,	kIx,
poznal	poznat	k5eAaPmAgMnS
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
obtížné	obtížný	k2eAgNnSc1d1
a	a	k8xC
nedokonalé	dokonalý	k2eNgNnSc1d1
je	být	k5eAaImIp3nS
rozrušování	rozrušování	k1gNnSc1
půdy	půda	k1gFnSc2
pro	pro	k7c4
přípravu	příprava	k1gFnSc4
setby	setba	k1gFnSc2
dosavadním	dosavadní	k2eAgNnSc7d1
nářadím	nářadí	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úpravami	úprava	k1gFnPc7
tehdejšího	tehdejší	k2eAgInSc2d1
tzv.	tzv.	kA
záhonového	záhonový	k2eAgInSc2d1
pluhu	pluh	k1gInSc2
<g/>
,	,	kIx,
po	po	k7c6
mnoha	mnoho	k4c6
neúspěšných	úspěšný	k2eNgFnPc6d1
zkouškách	zkouška	k1gFnPc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
oběma	dva	k4xCgMnPc7
bratrancům	bratranec	k1gMnPc3
společně	společně	k6eAd1
podařilo	podařit	k5eAaPmAgNnS
vytvořit	vytvořit	k5eAaPmF
pluh	pluh	k1gInSc4
pracující	pracující	k2eAgInPc4d1
podle	podle	k7c2
jejich	jejich	k3xOp3gFnPc2
představ	představa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
dosavadní	dosavadní	k2eAgNnPc1d1
oradla	oradlo	k1gNnPc1
půdu	půda	k1gFnSc4
pouze	pouze	k6eAd1
rozrušovala	rozrušovat	k5eAaImAgFnS
a	a	k8xC
musela	muset	k5eAaImAgFnS
se	se	k3xPyFc4
v	v	k7c6
patřičné	patřičný	k2eAgFnSc6d1
hloubce	hloubka	k1gFnSc6
v	v	k7c6
zemi	zem	k1gFnSc6
udržovat	udržovat	k5eAaImF
silou	síla	k1gFnSc7
oráče	oráč	k1gMnSc2
<g/>
,	,	kIx,
vhodně	vhodně	k6eAd1
tvarovaný	tvarovaný	k2eAgMnSc1d1
a	a	k8xC
na	na	k7c6
kolečkách	koleček	k1gInPc6
upevněný	upevněný	k2eAgInSc4d1
pluh	pluh	k1gInSc4
Veverkových	Veverkových	k2eAgFnSc4d1
půdu	půda	k1gFnSc4
odkrajoval	odkrajovat	k5eAaImAgMnS
<g/>
,	,	kIx,
rozrušoval	rozrušovat	k5eAaImAgMnS
<g/>
,	,	kIx,
drolil	drolit	k5eAaImAgMnS
a	a	k8xC
obracel	obracet	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hloubka	hloubka	k1gFnSc1
orby	orba	k1gFnSc2
se	se	k3xPyFc4
dala	dát	k5eAaPmAgFnS
řídit	řídit	k5eAaImF
na	na	k7c6
hřídeli	hřídel	k1gInSc6
opřeném	opřený	k2eAgInSc6d1
o	o	k7c4
kolečka	kolečko	k1gNnPc4
vpředu	vpředu	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidé	člověk	k1gMnPc1
nový	nový	k2eAgInSc4d1
pluh	pluh	k1gInSc4
jmenovali	jmenovat	k5eAaBmAgMnP,k5eAaImAgMnP
„	„	k?
<g/>
veverče	veverče	k1gNnSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
sami	sám	k3xTgMnPc1
vynálezci	vynálezce	k1gMnPc1
ho	on	k3xPp3gNnSc4
však	však	k9
nazvali	nazvat	k5eAaPmAgMnP,k5eAaBmAgMnP
ruchadlem	ruchadlo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
brázdu	brázda	k1gFnSc4
novým	nový	k2eAgNnSc7d1
ruchadlem	ruchadlo	k1gNnSc7
vyorali	vyorat	k5eAaPmAgMnP
bratranci	bratranec	k1gMnPc1
za	za	k7c7
kovárnou	kovárna	k1gFnSc7
v	v	k7c6
Rybitví	Rybitví	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
1827	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Využití	využití	k1gNnSc1
vynálezu	vynález	k1gInSc2
</s>
<s>
Bratranci	bratranec	k1gMnPc1
Veverkovi	Veverkův	k2eAgMnPc1d1
byli	být	k5eAaImAgMnP
prostí	prostý	k2eAgMnPc1d1
venkovští	venkovský	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těšilo	těšit	k5eAaImAgNnS
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jejich	jejich	k3xOp3gNnSc1
řešení	řešení	k1gNnSc1
funguje	fungovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
pomáhá	pomáhat	k5eAaImIp3nS
lidem	člověk	k1gMnPc3
ulehčit	ulehčit	k5eAaPmF
jejich	jejich	k3xOp3gFnSc4
práci	práce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevěděli	vědět	k5eNaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
vynález	vynález	k1gInSc4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gNnSc1
patentování	patentování	k1gNnSc1
a	a	k8xC
obchodní	obchodní	k2eAgNnSc1d1
využití	využití	k1gNnSc1
by	by	kYmCp3nS
mohlo	moct	k5eAaImAgNnS
řešit	řešit	k5eAaImF
jejich	jejich	k3xOp3gFnSc4
svízelnou	svízelný	k2eAgFnSc4d1
finanční	finanční	k2eAgFnSc4d1
situaci	situace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dávali	dávat	k5eAaImAgMnP
své	své	k1gNnSc4
řešení	řešení	k1gNnSc2
ruchadla	ruchadlo	k1gNnSc2
k	k	k7c3
dispozici	dispozice	k1gFnSc3
každému	každý	k3xTgMnSc3
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
o	o	k7c4
ně	on	k3xPp3gMnPc4
požádal	požádat	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Omezený	omezený	k2eAgInSc1d1
počet	počet	k1gInSc1
ruchadel	ruchadlo	k1gNnPc2
vyráběl	vyrábět	k5eAaImAgMnS
Václav	Václav	k1gMnSc1
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
kovárně	kovárna	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
to	ten	k3xDgNnSc1
zdaleka	zdaleka	k6eAd1
nestačilo	stačit	k5eNaBmAgNnS
poptávce	poptávka	k1gFnSc3
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k8xC,k8xS
se	se	k3xPyFc4
výroby	výroba	k1gFnSc2
a	a	k8xC
prodeje	prodej	k1gFnPc4
ruchadel	ruchadlo	k1gNnPc2
ujímali	ujímat	k5eAaImAgMnP
jiní	jiný	k2eAgMnPc1d1
<g/>
,	,	kIx,
obchodně	obchodně	k6eAd1
zdatnější	zdatný	k2eAgMnPc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Např.	např.	kA
Karel	Karel	k1gMnSc1
Podhajský	Podhajský	k2eAgMnSc1d1
<g/>
,	,	kIx,
obchodník	obchodník	k1gMnSc1
z	z	k7c2
Bohdanče	Bohdanec	k1gMnSc5
<g/>
,	,	kIx,
s	s	k7c7
vědomím	vědomí	k1gNnSc7
bratranců	bratranec	k1gMnPc2
Veverkových	Veverkových	k2eAgFnPc2d1
a	a	k8xC
s	s	k7c7
použitím	použití	k1gNnSc7
dřevěného	dřevěný	k2eAgInSc2d1
modelu	model	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
mu	on	k3xPp3gMnSc3
poskytli	poskytnout	k5eAaPmAgMnP
<g/>
,	,	kIx,
objednal	objednat	k5eAaPmAgMnS
výrobu	výroba	k1gFnSc4
většího	veliký	k2eAgNnSc2d2
množství	množství	k1gNnSc2
nových	nový	k2eAgInPc2d1
pluhů	pluh	k1gInPc2
ve	v	k7c6
slévárně	slévárna	k1gFnSc6
Václava	Václav	k1gMnSc2
Svobody	Svoboda	k1gMnSc2
v	v	k7c6
Třemošnici-Hedvíkově	Třemošnici-Hedvíkův	k2eAgFnSc6d1
a	a	k8xC
zajišťoval	zajišťovat	k5eAaImAgMnS
jejich	jejich	k3xOp3gInSc4
prodej	prodej	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Nepatentovaný	patentovaný	k2eNgInSc1d1
vynález	vynález	k1gInSc1
však	však	k9
byl	být	k5eAaImAgInS
k	k	k7c3
využití	využití	k1gNnSc3
i	i	k9
lidem	člověk	k1gMnPc3
nečestným	čestný	k2eNgMnPc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářský	hospodářský	k2eAgMnSc1d1
úředník	úředník	k1gMnSc1
Jan	Jan	k1gMnSc1
Kainz	Kainz	k1gMnSc1
z	z	k7c2
Choltic	Choltice	k1gFnPc2
nechal	nechat	k5eAaPmAgMnS
ruchadlo	ruchadlo	k1gNnSc4
podle	podle	k7c2
vzoru	vzor	k1gInSc2
vyrobit	vyrobit	k5eAaPmF
u	u	k7c2
choltického	choltický	k2eAgMnSc2d1
kováře	kovář	k1gMnSc2
Jana	Jan	k1gMnSc2
Pechmana	Pechman	k1gMnSc2
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1832	#num#	k4
ho	on	k3xPp3gMnSc4
poslal	poslat	k5eAaPmAgMnS
na	na	k7c4
hospodářskou	hospodářský	k2eAgFnSc4d1
výstavu	výstava	k1gFnSc4
do	do	k7c2
Prahy	Praha	k1gFnSc2
s	s	k7c7
označením	označení	k1gNnSc7
Kainzpflug	Kainzpfluga	k1gFnPc2
(	(	kIx(
<g/>
Kainzův	Kainzův	k2eAgInSc1d1
pluh	pluh	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
svůj	svůj	k3xOyFgInSc4
vynález	vynález	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruchadlo	ruchadlo	k1gNnSc1
se	se	k3xPyFc4
na	na	k7c6
výstavě	výstava	k1gFnSc6
setkalo	setkat	k5eAaPmAgNnS
s	s	k7c7
velkým	velký	k2eAgInSc7d1
ohlasem	ohlas	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pražský	pražský	k2eAgInSc1d1
německý	německý	k2eAgInSc1d1
tisk	tisk	k1gInSc1
nešetřil	šetřit	k5eNaImAgInS
obdivem	obdiv	k1gInSc7
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
velký	velký	k2eAgInSc1d1
vynález	vynález	k1gInSc1
Němec	Němec	k1gMnSc1
Kainz	Kainz	k1gMnSc1
vytvořil	vytvořit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevadilo	vadit	k5eNaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
ruchadlo	ruchadlo	k1gNnSc4
<g/>
,	,	kIx,
o	o	k7c6
kterém	který	k3yRgNnSc6,k3yQgNnSc6,k3yIgNnSc6
již	již	k9
rok	rok	k1gInSc4
předtím	předtím	k6eAd1
psal	psát	k5eAaImAgMnS
básník	básník	k1gMnSc1
Josef	Josef	k1gMnSc1
Jaroslav	Jaroslav	k1gMnSc1
Langer	Langer	k1gMnSc1
v	v	k7c6
časopise	časopis	k1gInSc6
Čechoslav	Čechoslava	k1gFnPc2
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
vynález	vynález	k1gInSc4
Veverkových	Veverkových	k2eAgInSc4d1
z	z	k7c2
Rybitví	Rybitví	k1gNnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
celém	celý	k2eAgNnSc6d1
okolí	okolí	k1gNnSc6
už	už	k6eAd1
několik	několik	k4yIc4
let	léto	k1gNnPc2
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
ke	k	k7c3
kultivaci	kultivace	k1gFnSc3
půdy	půda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Spory	spor	k1gInPc1
o	o	k7c6
autorství	autorství	k1gNnSc6
</s>
<s>
O	o	k7c6
původcovství	původcovství	k1gNnSc6
vynálezu	vynález	k1gInSc2
se	se	k3xPyFc4
vedl	vést	k5eAaImAgInS
spor	spor	k1gInSc1
s	s	k7c7
národnostním	národnostní	k2eAgInSc7d1
podtextem	podtext	k1gInSc7
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
nečestný	čestný	k2eNgMnSc1d1
Němec	Němec	k1gMnSc1
připravil	připravit	k5eAaPmAgMnS
prosté	prostý	k2eAgMnPc4d1
české	český	k2eAgMnPc4d1
vynálezce	vynálezce	k1gMnPc4
o	o	k7c6
autorství	autorství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedoucím	vedoucí	k2eAgInSc7d1
činitelem	činitel	k1gInSc7
tohoto	tento	k3xDgInSc2
boje	boj	k1gInSc2
byl	být	k5eAaImAgInS
nejdříve	dříve	k6eAd3
bohdanečský	bohdanečský	k2eAgMnSc1d1
rodák	rodák	k1gMnSc1
<g/>
,	,	kIx,
básník	básník	k1gMnSc1
Josef	Josef	k1gMnSc1
Jaroslav	Jaroslav	k1gMnSc1
Langer	Langer	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
s	s	k7c7
J.	J.	kA
J.	J.	kA
Langrem	Langer	k1gMnSc7
se	se	k3xPyFc4
zastával	zastávat	k5eAaImAgInS
bratranců	bratranec	k1gMnPc2
Veverkových	Veverková	k1gFnPc2
a	a	k8xC
jejich	jejich	k3xOp3gNnSc2
autorství	autorství	k1gNnSc2
k	k	k7c3
ruchadlu	ruchadlo	k1gNnSc3
Ignác	Ignác	k1gMnSc1
Lhotský	lhotský	k2eAgMnSc1d1
<g/>
,	,	kIx,
profesor	profesor	k1gMnSc1
hospodářství	hospodářství	k1gNnSc2
na	na	k7c6
bohosloveckém	bohoslovecký	k2eAgNnSc6d1
lyceu	lyceum	k1gNnSc6
v	v	k7c6
Hradci	Hradec	k1gInSc6
Králové	Králová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původ	původ	k1gInSc1
vynálezu	vynález	k1gInSc2
ověřil	ověřit	k5eAaPmAgInS
spolu	spolu	k6eAd1
s	s	k7c7
Langrem	Langer	k1gMnSc7
výslechem	výslech	k1gInSc7
řady	řada	k1gFnSc2
svědků	svědek	k1gMnPc2
v	v	k7c6
Rybitví	Rybitví	k1gNnSc6
a	a	k8xC
okolí	okolí	k1gNnSc6
a	a	k8xC
podal	podat	k5eAaPmAgMnS
o	o	k7c6
tom	ten	k3xDgNnSc6
v	v	k7c6
lednu	leden	k1gInSc6
1835	#num#	k4
zprávu	zpráva	k1gFnSc4
tehdejší	tehdejší	k2eAgFnSc2d1
Hospodářské	hospodářský	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
ale	ale	k9
údajně	údajně	k6eAd1
spis	spis	k1gInSc4
ztratil	ztratit	k5eAaPmAgMnS
a	a	k8xC
věc	věc	k1gFnSc1
nebyla	být	k5eNaImAgFnS
vůbec	vůbec	k9
řešena	řešit	k5eAaImNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratranci	bratranec	k1gMnPc7
Veverkové	Veverková	k1gFnSc2
i	i	k8xC
J.	J.	kA
J.	J.	kA
Langer	Langer	k1gMnSc1
pak	pak	k6eAd1
koncem	koncem	k7c2
čtyřicátých	čtyřicátý	k4xOgNnPc2
let	léto	k1gNnPc2
zemřeli	zemřít	k5eAaPmAgMnP
a	a	k8xC
úředního	úřední	k2eAgNnSc2d1
uznání	uznání	k1gNnSc2
vynálezu	vynález	k1gInSc2
se	se	k3xPyFc4
nedočkali	dočkat	k5eNaPmAgMnP
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1868	#num#	k4
se	se	k3xPyFc4
vyšetřování	vyšetřování	k1gNnSc2
autorství	autorství	k1gNnSc2
vynálezu	vynález	k1gInSc2
ujal	ujmout	k5eAaPmAgMnS
redaktor	redaktor	k1gMnSc1
Hospodářských	hospodářský	k2eAgFnPc2d1
novin	novina	k1gFnPc2
Karel	Karel	k1gMnSc1
Procházka	Procházka	k1gMnSc1
a	a	k8xC
znovu	znovu	k6eAd1
si	se	k3xPyFc3
vyžádal	vyžádat	k5eAaPmAgInS
v	v	k7c6
Rybitví	Rybitví	k1gNnSc6
svědectví	svědectví	k1gNnSc2
několika	několik	k4yIc2
pamětníků	pamětník	k1gInPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
mu	on	k3xPp3gMnSc3
původ	původ	k1gInSc4
vynálezu	vynález	k1gInSc2
bratranců	bratranec	k1gMnPc2
Veverkových	Veverková	k1gFnPc2
potvrdili	potvrdit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úřední	úřední	k2eAgInSc4d1
ověření	ověření	k1gNnSc1
se	se	k3xPyFc4
ale	ale	k8xC
ani	ani	k8xC
jemu	on	k3xPp3gMnSc3
nezdařilo	zdařit	k5eNaPmAgNnS
získat	získat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
1882	#num#	k4
věc	věc	k1gFnSc1
byla	být	k5eAaImAgFnS
znovu	znovu	k6eAd1
šetřena	šetřen	k2eAgFnSc1d1
<g/>
,	,	kIx,
tentokrát	tentokrát	k6eAd1
jednatelem	jednatel	k1gMnSc7
Hospodářského	hospodářský	k2eAgInSc2d1
spolku	spolek	k1gInSc2
v	v	k7c6
Pardubicích	Pardubice	k1gInPc6
Františkem	František	k1gMnSc7
Vratislavem	Vratislav	k1gMnSc7
Sovou	Sova	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
spisy	spis	k1gInPc4
Lhotského	Lhotský	k1gMnSc4
i	i	k8xC
Procházky	Procházka	k1gMnPc4
našel	najít	k5eAaPmAgMnS
<g/>
,	,	kIx,
prostudoval	prostudovat	k5eAaPmAgMnS
<g/>
,	,	kIx,
znovu	znovu	k6eAd1
vyslechl	vyslechnout	k5eAaPmAgMnS
svědky	svědek	k1gMnPc4
v	v	k7c6
Rybitví	Rybitví	k1gNnSc6
a	a	k8xC
na	na	k7c6
těchto	tento	k3xDgInPc6
základech	základ	k1gInPc6
vydal	vydat	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1883	#num#	k4
knížku	knížka	k1gFnSc4
nazvanou	nazvaný	k2eAgFnSc4d1
Vynálezci	vynálezce	k1gMnPc7
ruchadla	ruchadlo	k1gNnSc2
a	a	k8xC
autorství	autorství	k1gNnSc6
Veverkových	Veverková	k1gFnPc2
prokázal	prokázat	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ještě	ještě	k9
v	v	k7c6
témže	týž	k3xTgInSc6
roce	rok	k1gInSc6
<g/>
,	,	kIx,
8	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
9	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1883	#num#	k4
<g/>
,	,	kIx,
se	se	k3xPyFc4
konala	konat	k5eAaImAgFnS
v	v	k7c6
Pardubicích	Pardubice	k1gInPc6
a	a	k8xC
v	v	k7c6
Rybitví	Rybitví	k1gNnSc6
velká	velký	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
slavnost	slavnost	k1gFnSc1
k	k	k7c3
odhalení	odhalení	k1gNnSc3
pomníků	pomník	k1gInPc2
bratrancům	bratranec	k1gMnPc3
Veverkovým	Veverkovi	k1gRnPc3
v	v	k7c6
obou	dva	k4xCgNnPc6
místech	místo	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
již	již	k6eAd1
autorství	autorství	k1gNnSc2
vynálezu	vynález	k1gInSc2
nikdo	nikdo	k3yNnSc1
nezpochybňoval	zpochybňovat	k5eNaImAgMnS
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
SOVA	Sova	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vynálezci	vynálezce	k1gMnPc1
ruchadla	ruchadlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pardubice	Pardubice	k1gInPc4
<g/>
:	:	kIx,
F.	F.	kA
&	&	k?
V.	V.	kA
Hoblík	hoblík	k1gInSc1
<g/>
,	,	kIx,
1883	#num#	k4
<g/>
.	.	kIx.
135	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
SOVA	Sova	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vynálezci	vynálezce	k1gMnPc1
ruchadla	ruchadlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pardubice	Pardubice	k1gInPc4
<g/>
:	:	kIx,
F.	F.	kA
&	&	k?
V.	V.	kA
Hoblík	hoblík	k1gInSc1
<g/>
,	,	kIx,
1883	#num#	k4
<g/>
.	.	kIx.
135	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Josef	Josef	k1gMnSc1
Jungwirth	Jungwirth	k1gMnSc1
<g/>
:	:	kIx,
Ruchadlo	ruchadlo	k1gNnSc1
–	–	k?
vynález	vynález	k1gInSc4
bratranců	bratranec	k1gMnPc2
Veverkových	Veverkových	k2eAgFnSc2d1
<g/>
,	,	kIx,
Vydavatelství	vydavatelství	k1gNnSc6
MZLVH	MZLVH	kA
1964	#num#	k4
</s>
<s>
Zprávy	zpráva	k1gFnPc1
Klubu	klub	k1gInSc2
přátel	přítel	k1gMnPc2
Pardubicka	Pardubicko	k1gNnSc2
č.	č.	k?
5	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2007	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2003	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
/	/	kIx~
<g/>
1978	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
ŠEBEK	Šebek	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
:	:	kIx,
Bratranci	bratranec	k1gMnPc1
Veverkové	Veverkové	k2eAgMnPc1d1
a	a	k8xC
jejich	jejich	k3xOp3gInSc1
vynález	vynález	k1gInSc1
ruchadla	ruchadlo	k1gNnSc2
<g/>
,	,	kIx,
oficiální	oficiální	k2eAgFnSc2d1
webové	webový	k2eAgFnSc2d1
stránky	stránka	k1gFnSc2
obce	obec	k1gFnSc2
Rybitví	Rybitví	k1gNnSc2
(	(	kIx(
<g/>
archivní	archivní	k2eAgInSc1d1
odkaz	odkaz	k1gInSc1
<g/>
)	)	kIx)
</s>
