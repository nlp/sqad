<s>
Ruchadlo	ruchadlo	k1gNnSc1
je	být	k5eAaImIp3nS
pluh	pluh	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
půdu	půda	k1gFnSc4
nejen	nejen	k6eAd1
rozrušuje	rozrušovat	k5eAaImIp3nS
a	a	k8xC
provzdušňuje	provzdušňovat	k5eAaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
převrací	převracet	k5eAaImIp3nP
ji	on	k3xPp3gFnSc4
tak	tak	k9
<g/>
,	,	kIx,
že	že	k8xS
vrchní	vrchní	k2eAgFnSc1d1
vrstva	vrstva	k1gFnSc1
se	se	k3xPyFc4
dostává	dostávat	k5eAaImIp3nS
dolů	dolů	k6eAd1
a	a	k8xC
spodní	spodní	k2eAgFnSc1d1
vrstva	vrstva	k1gFnSc1
odpočaté	odpočatý	k2eAgFnSc2d1
půdy	půda	k1gFnSc2
navrch	navrch	k6eAd1
<g/>
.	.	kIx.
</s>