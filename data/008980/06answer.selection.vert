<s>
Dánština	dánština	k1gFnSc1	dánština
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
dánsky	dánsky	k6eAd1	dánsky
dansk	dansk	k1gInSc1	dansk
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
severogermánský	severogermánský	k2eAgInSc1d1	severogermánský
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnSc7	který
hovoří	hovořit	k5eAaImIp3nS	hovořit
okolo	okolo	k7c2	okolo
5,5	[number]	k4	5,5
milionu	milion	k4xCgInSc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
<g/>
.	.	kIx.	.
</s>
