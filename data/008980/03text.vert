<p>
<s>
Dánština	dánština	k1gFnSc1	dánština
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
dánsky	dánsky	k6eAd1	dánsky
dansk	dansk	k1gInSc1	dansk
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
severogermánský	severogermánský	k2eAgInSc1d1	severogermánský
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnSc7	který
hovoří	hovořit	k5eAaImIp3nS	hovořit
okolo	okolo	k7c2	okolo
5,5	[number]	k4	5,5
milionu	milion	k4xCgInSc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
<g/>
.	.	kIx.	.
</s>
<s>
Početné	početný	k2eAgFnPc1d1	početná
jazykové	jazykový	k2eAgFnPc1d1	jazyková
menšiny	menšina	k1gFnPc1	menšina
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
severoněmecké	severoněmecký	k2eAgFnSc6d1	Severoněmecká
spolkové	spolkový	k2eAgFnSc6d1	spolková
zemi	zem	k1gFnSc6	zem
Šlesvicko-Holštýnsko	Šlesvicko-Holštýnsko	k1gNnSc1	Šlesvicko-Holštýnsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
Faerských	Faerský	k2eAgInPc6d1	Faerský
ostrovech	ostrov	k1gInPc6	ostrov
a	a	k8xC	a
v	v	k7c6	v
Grónsku	Grónsko	k1gNnSc6	Grónsko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
blízce	blízce	k6eAd1	blízce
příbuzná	příbuzný	k2eAgFnSc1d1	příbuzná
s	s	k7c7	s
norštinou	norština	k1gFnSc7	norština
a	a	k8xC	a
švédštinou	švédština	k1gFnSc7	švédština
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgInPc7	jenž
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
vzájemně	vzájemně	k6eAd1	vzájemně
srozumitelná	srozumitelný	k2eAgFnSc1d1	srozumitelná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dánština	dánština	k1gFnSc1	dánština
se	se	k3xPyFc4	se
od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
skandinávských	skandinávský	k2eAgInPc2d1	skandinávský
jazyků	jazyk	k1gInPc2	jazyk
začala	začít	k5eAaPmAgFnS	začít
oddělovat	oddělovat	k5eAaImF	oddělovat
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1000	[number]	k4	1000
a	a	k8xC	a
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
prošla	projít	k5eAaPmAgFnS	projít
největšími	veliký	k2eAgFnPc7d3	veliký
změnami	změna	k1gFnPc7	změna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
Hansy	hansa	k1gFnSc2	hansa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
založila	založit	k5eAaPmAgFnS	založit
pobočky	pobočka	k1gFnPc4	pobočka
i	i	k8xC	i
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
<g/>
,	,	kIx,	,
převzala	převzít	k5eAaPmAgFnS	převzít
z	z	k7c2	z
dolnoněmčiny	dolnoněmčina	k1gFnSc2	dolnoněmčina
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejenom	nejenom	k6eAd1	nejenom
slovník	slovník	k1gInSc1	slovník
oborů	obor	k1gInPc2	obor
a	a	k8xC	a
řemesel	řemeslo	k1gNnPc2	řemeslo
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
přišly	přijít	k5eAaPmAgFnP	přijít
s	s	k7c7	s
Hansou	hansa	k1gFnSc7	hansa
do	do	k7c2	do
Dánska	Dánsko	k1gNnSc2	Dánsko
(	(	kIx(	(
<g/>
jakož	jakož	k8xC	jakož
i	i	k9	i
do	do	k7c2	do
celé	celý	k2eAgFnSc2d1	celá
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dolnoněmecká	dolnoněmecký	k2eAgNnPc1d1	dolnoněmecké
slova	slovo	k1gNnPc1	slovo
pronikla	proniknout	k5eAaPmAgNnP	proniknout
až	až	k9	až
do	do	k7c2	do
základního	základní	k2eAgInSc2d1	základní
slovníku	slovník	k1gInSc2	slovník
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jazykového	jazykový	k2eAgInSc2d1	jazykový
vývoje	vývoj	k1gInSc2	vývoj
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
zjednodušila	zjednodušit	k5eAaPmAgFnS	zjednodušit
gramatika	gramatika	k1gFnSc1	gramatika
<g/>
,	,	kIx,	,
především	především	k9	především
skloňování	skloňování	k1gNnSc4	skloňování
a	a	k8xC	a
časování	časování	k1gNnSc4	časování
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
dánština	dánština	k1gFnSc1	dánština
má	mít	k5eAaImIp3nS	mít
převažující	převažující	k2eAgInSc4d1	převažující
charakter	charakter	k1gInSc4	charakter
analytického	analytický	k2eAgInSc2d1	analytický
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klasifikace	klasifikace	k1gFnSc2	klasifikace
==	==	k?	==
</s>
</p>
<p>
<s>
Dánština	dánština	k1gFnSc1	dánština
je	být	k5eAaImIp3nS	být
indoevropský	indoevropský	k2eAgInSc4d1	indoevropský
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
severogermánské	severogermánský	k2eAgFnSc2d1	severogermánský
větve	větev	k1gFnSc2	větev
germánských	germánský	k2eAgInPc2d1	germánský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
švédštinou	švédština	k1gFnSc7	švédština
a	a	k8xC	a
bokmå	bokmå	k?	bokmå
(	(	kIx(	(
<g/>
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
variant	varianta	k1gFnPc2	varianta
norštiny	norština	k1gFnSc2	norština
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
východoskandinávské	východoskandinávský	k2eAgInPc4d1	východoskandinávský
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
norština	norština	k1gFnSc1	norština
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
varianta	varianta	k1gFnSc1	varianta
označovaná	označovaný	k2eAgFnSc1d1	označovaná
jako	jako	k8xS	jako
nynorsk	nynorsk	k1gInSc1	nynorsk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
faerština	faerština	k1gFnSc1	faerština
a	a	k8xC	a
islandština	islandština	k1gFnSc1	islandština
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
západoskandinávské	západoskandinávský	k2eAgNnSc4d1	západoskandinávský
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
rozdělení	rozdělení	k1gNnSc1	rozdělení
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
z	z	k7c2	z
fonologických	fonologický	k2eAgInPc2d1	fonologický
rozdílů	rozdíl	k1gInPc2	rozdíl
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mají	mít	k5eAaImIp3nP	mít
základ	základ	k1gInSc4	základ
již	již	k9	již
v	v	k7c6	v
době	doba	k1gFnSc6	doba
nářečního	nářeční	k2eAgNnSc2d1	nářeční
štěpení	štěpení	k1gNnSc2	štěpení
staré	starý	k2eAgFnSc2d1	stará
severštiny	severština	k1gFnSc2	severština
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
východoskandinávských	východoskandinávský	k2eAgInPc6d1	východoskandinávský
jazycích	jazyk	k1gInPc6	jazyk
například	například	k6eAd1	například
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
původní	původní	k2eAgFnPc1d1	původní
dvojhlásky	dvojhláska	k1gFnPc1	dvojhláska
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
západoskandinávské	západoskandinávský	k2eAgInPc1d1	západoskandinávský
jazyky	jazyk	k1gInPc1	jazyk
tyto	tento	k3xDgFnPc1	tento
dvojhlásky	dvojhláska	k1gFnPc1	dvojhláska
zachovaly	zachovat	k5eAaPmAgFnP	zachovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jiné	jiný	k2eAgNnSc1d1	jiné
rozdělení	rozdělení	k1gNnSc1	rozdělení
řadí	řadit	k5eAaImIp3nS	řadit
dánštinu	dánština	k1gFnSc4	dánština
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
švédštinou	švédština	k1gFnSc7	švédština
a	a	k8xC	a
norštinou	norština	k1gFnSc7	norština
mezi	mezi	k7c4	mezi
kontinentální	kontinentální	k2eAgInPc4d1	kontinentální
skandinávské	skandinávský	k2eAgInPc4d1	skandinávský
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
charakter	charakter	k1gInSc1	charakter
analytických	analytický	k2eAgInPc2d1	analytický
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Ostrovní	ostrovní	k2eAgInPc1d1	ostrovní
skandinávské	skandinávský	k2eAgInPc1d1	skandinávský
jazyky	jazyk	k1gInPc1	jazyk
(	(	kIx(	(
<g/>
faerština	faerština	k1gFnSc1	faerština
<g/>
,	,	kIx,	,
islandština	islandština	k1gFnSc1	islandština
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
uchovaly	uchovat	k5eAaPmAgFnP	uchovat
charakter	charakter	k1gInSc4	charakter
flektivních	flektivní	k2eAgInPc2d1	flektivní
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fonetika	fonetika	k1gFnSc1	fonetika
==	==	k?	==
</s>
</p>
<p>
<s>
Nejvýznamnější	významný	k2eAgFnPc1d3	nejvýznamnější
fonetické	fonetický	k2eAgFnPc1d1	fonetická
charakteristiky	charakteristika	k1gFnPc1	charakteristika
dánského	dánský	k2eAgInSc2d1	dánský
jazyka	jazyk	k1gInSc2	jazyk
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc4	tento
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
a	a	k8xC	a
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
[	[	kIx(	[
<g/>
æ	æ	k?	æ
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
panamakanal	panamakanat	k5eAaPmAgInS	panamakanat
[	[	kIx(	[
<g/>
pæ	pæ	k?	pæ
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
<g/>
r	r	kA	r
se	se	k3xPyFc4	se
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
uvulárně	uvulárně	k6eAd1	uvulárně
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
vzadu	vzadu	k6eAd1	vzadu
u	u	k7c2	u
čípku	čípek	k1gInSc2	čípek
<g/>
,	,	kIx,	,
při	při	k7c6	při
čemž	což	k3yRnSc6	což
se	se	k3xPyFc4	se
na	na	k7c6	na
konci	konec	k1gInSc6	konec
slabiky	slabika	k1gFnSc2	slabika
vokalizuje	vokalizovat	k5eAaBmIp3nS	vokalizovat
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
předchozí	předchozí	k2eAgFnSc1d1	předchozí
samohláska	samohláska	k1gFnSc1	samohláska
se	se	k3xPyFc4	se
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
otevřeněji	otevřeně	k6eAd2	otevřeně
<g/>
:	:	kIx,	:
Der	drát	k5eAaImRp2nS	drát
lø	lø	k?	lø
<g />
.	.	kIx.	.
</s>
<s>
en	en	k?	en
lø	lø	k?	lø
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
dea	dea	k?	dea
lø	lø	k?	lø
<g/>
'	'	kIx"	'
<g/>
ba	ba	k9	ba
en	en	k?	en
lø	lø	k?	lø
<g/>
]	]	kIx)	]
'	'	kIx"	'
<g/>
Tam	tam	k6eAd1	tam
běží	běžet	k5eAaImIp3nS	běžet
nějaký	nějaký	k3yIgMnSc1	nějaký
běžec	běžec	k1gMnSc1	běžec
<g/>
.	.	kIx.	.
<g/>
'	'	kIx"	'
<g/>
Neznělé	znělý	k2eNgFnSc2d1	neznělá
souhlásky	souhláska	k1gFnSc2	souhláska
p	p	k?	p
<g/>
,	,	kIx,	,
t	t	k?	t
<g/>
,	,	kIx,	,
k	k	k7c3	k
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
následovány	následovat	k5eAaImNgFnP	následovat
samohláskami	samohláska	k1gFnPc7	samohláska
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
změnily	změnit	k5eAaPmAgInP	změnit
v	v	k7c4	v
znělé	znělý	k2eAgNnSc4d1	znělé
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
např.	např.	kA	např.
<g/>
:	:	kIx,	:
ikke	ikk	k1gMnSc2	ikk
'	'	kIx"	'
<g/>
ne	ne	k9	ne
<g/>
'	'	kIx"	'
[	[	kIx(	[
<g/>
'	'	kIx"	'
<g/>
egə	egə	k?	egə
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
kø	kø	k?	kø
'	'	kIx"	'
<g/>
koupit	koupit	k5eAaPmF	koupit
<g/>
'	'	kIx"	'
(	(	kIx(	(
<g/>
srov.	srov.	kA	srov.
no	no	k9	no
<g/>
.	.	kIx.	.
kjø	kjø	k?	kjø
<g/>
,	,	kIx,	,
šv	šv	k?	šv
<g/>
.	.	kIx.	.
köpa	köpa	k1gMnSc1	köpa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sæ	sæ	k?	sæ
'	'	kIx"	'
<g/>
posadit	posadit	k5eAaPmF	posadit
<g/>
'	'	kIx"	'
[	[	kIx(	[
<g/>
sedə	sedə	k?	sedə
<g />
.	.	kIx.	.
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
<g/>
g	g	kA	g
mezi	mezi	k7c4	mezi
vokály	vokál	k1gInPc4	vokál
se	se	k3xPyFc4	se
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
jako	jako	k8xC	jako
[	[	kIx(	[
<g/>
j	j	k?	j
<g/>
]	]	kIx)	]
anebo	anebo	k8xC	anebo
odpadá	odpadat	k5eAaImIp3nS	odpadat
<g/>
:	:	kIx,	:
pige	pige	k1gFnSc1	pige
'	'	kIx"	'
<g/>
holka	holka	k1gFnSc1	holka
<g/>
'	'	kIx"	'
[	[	kIx(	[
<g/>
pijə	pijə	k?	pijə
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
uge	uge	k?	uge
'	'	kIx"	'
<g/>
týden	týden	k1gInSc4	týden
<g/>
'	'	kIx"	'
[	[	kIx(	[
<g/>
uə	uə	k?	uə
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
<g/>
Intervokalické	intervokalický	k2eAgInPc1d1	intervokalický
a	a	k8xC	a
koncové	koncový	k2eAgInPc1d1	koncový
<g />
.	.	kIx.	.
</s>
<s>
d	d	k?	d
se	se	k3xPyFc4	se
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
téměř	téměř	k6eAd1	téměř
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
anglické	anglický	k2eAgFnSc2d1	anglická
th	th	k?	th
<g/>
,	,	kIx,	,
při	při	k7c6	při
čemž	což	k3yQnSc6	což
se	se	k3xPyFc4	se
však	však	k9	však
jazyk	jazyk	k1gInSc1	jazyk
nasadí	nasadit	k5eAaPmIp3nS	nasadit
na	na	k7c4	na
dolní	dolní	k2eAgFnSc4d1	dolní
zubní	zubní	k2eAgFnSc4d1	zubní
řadu	řada	k1gFnSc4	řada
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
rø	rø	k?	rø
grø	grø	k?	grø
med	med	k1gInSc1	med
flø	flø	k?	flø
'	'	kIx"	'
<g/>
červená	červený	k2eAgFnSc1d1	červená
krupičná	krupičný	k2eAgFnSc1d1	krupičná
kaše	kaše	k1gFnSc1	kaše
se	s	k7c7	s
šlehačkou	šlehačka	k1gFnSc7	šlehačka
<g/>
'	'	kIx"	'
[	[	kIx(	[
<g/>
Rø	Rø	k1gFnSc1	Rø
gRø	gRø	k?	gRø
með	með	k?	með
flø	flø	k?	flø
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
na	na	k7c4	na
srovnání	srovnání	k1gNnSc4	srovnání
norsky	norsky	k6eAd1	norsky
<g/>
:	:	kIx,	:
rø	rø	k?	rø
grø	grø	k?	grø
med	med	k1gInSc1	med
flø	flø	k?	flø
[	[	kIx(	[
<g/>
rø	rø	k?	rø
grø	grø	k?	grø
me	me	k?	me
flø	flø	k?	flø
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
hvad	hvad	k1gMnSc1	hvad
'	'	kIx"	'
<g/>
co	co	k3yQnSc4	co
<g/>
'	'	kIx"	'
[	[	kIx(	[
<g/>
vað	vað	k?	vað
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
<g/>
Koncové	koncový	k2eAgInPc4d1	koncový
kdysi	kdysi	k6eAd1	kdysi
plné	plný	k2eAgInPc4d1	plný
vokály	vokál	k1gInPc4	vokál
nahradila	nahradit	k5eAaPmAgFnS	nahradit
schwa	schwa	k1gFnSc1	schwa
<g/>
.	.	kIx.	.
</s>
<s>
Srov.	srov.	kA	srov.
šv	šv	k?	šv
<g/>
.	.	kIx.	.
krona	krona	k1gFnSc1	krona
'	'	kIx"	'
<g/>
koruna	koruna	k1gFnSc1	koruna
<g/>
'	'	kIx"	'
-	-	kIx~	-
pl.	pl.	k?	pl.
kronor	kronor	k1gInSc1	kronor
[	[	kIx(	[
<g/>
kruna	kruna	k1gFnSc1	kruna
<g/>
,	,	kIx,	,
krunur	krunur	k1gMnSc1	krunur
<g/>
]	]	kIx)	]
=	=	kIx~	=
dán	dát	k5eAaPmNgInS	dát
<g/>
.	.	kIx.	.
krone	kronout	k5eAaImIp3nS	kronout
<g/>
,	,	kIx,	,
pl.	pl.	k?	pl.
kroner	kroner	k1gInSc1	kroner
[	[	kIx(	[
<g/>
kRonə	kRonə	k?	kRonə
<g/>
,	,	kIx,	,
kRona	kRona	k1gFnSc1	kRona
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
hláskovém	hláskový	k2eAgInSc6d1	hláskový
systému	systém	k1gInSc6	systém
hraje	hrát	k5eAaImIp3nS	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
ráz	ráz	k1gInSc1	ráz
(	(	kIx(	(
<g/>
stø	stø	k?	stø
[	[	kIx(	[
<g/>
stø	stø	k?	stø
<g/>
'	'	kIx"	'
<g/>
ð	ð	k?	ð
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
známý	známý	k1gMnSc1	známý
např.	např.	kA	např.
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
hláskami	hláska	k1gFnPc7	hláska
e	e	k0	e
ve	v	k7c6	v
slově	slovo	k1gNnSc6	slovo
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dánštině	dánština	k1gFnSc6	dánština
se	se	k3xPyFc4	se
však	však	k9	však
ráz	ráz	k1gInSc4	ráz
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
uvnitř	uvnitř	k7c2	uvnitř
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
mnohdy	mnohdy	k6eAd1	mnohdy
v	v	k7c6	v
jednoslabičných	jednoslabičný	k2eAgFnPc6d1	jednoslabičná
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
fonematický	fonematický	k2eAgInSc4d1	fonematický
charakter	charakter	k1gInSc4	charakter
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
významy	význam	k1gInPc4	význam
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
<g/>
hun	hun	k?	hun
[	[	kIx(	[
<g/>
hun	hun	k?	hun
<g/>
]	]	kIx)	]
'	'	kIx"	'
<g/>
ona	onen	k3xDgFnSc1	onen
<g/>
'	'	kIx"	'
≠	≠	k?	≠
hund	hund	k1gInSc1	hund
[	[	kIx(	[
<g/>
hu	hu	k0	hu
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
]	]	kIx)	]
'	'	kIx"	'
<g/>
pes	pes	k1gMnSc1	pes
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
lø	lø	k?	lø
[	[	kIx(	[
<g/>
lø	lø	k?	lø
<g/>
]	]	kIx)	]
'	'	kIx"	'
<g/>
běžec	běžec	k1gMnSc1	běžec
<g/>
'	'	kIx"	'
≠	≠	k?	≠
lø	lø	k?	lø
[	[	kIx(	[
<g/>
lø	lø	k?	lø
<g/>
'	'	kIx"	'
<g/>
ba	ba	k9	ba
<g/>
]	]	kIx)	]
'	'	kIx"	'
<g/>
běží	běžet	k5eAaImIp3nS	běžet
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
annen	annen	k1gInSc1	annen
[	[	kIx(	[
<g/>
æ	æ	k?	æ
<g/>
]	]	kIx)	]
'	'	kIx"	'
<g/>
jiný	jiný	k2eAgMnSc1d1	jiný
<g/>
'	'	kIx"	'
≠	≠	k?	≠
anden	anden	k1gInSc1	anden
[	[	kIx(	[
<g/>
æ	æ	k?	æ
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
]	]	kIx)	]
'	'	kIx"	'
<g/>
kachna	kachna	k1gFnSc1	kachna
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Gramatika	gramatika	k1gFnSc1	gramatika
==	==	k?	==
</s>
</p>
<p>
<s>
Gramatická	gramatický	k2eAgFnSc1d1	gramatická
struktura	struktura	k1gFnSc1	struktura
dánštiny	dánština	k1gFnSc2	dánština
je	být	k5eAaImIp3nS	být
podobná	podobný	k2eAgFnSc1d1	podobná
norštině	norština	k1gFnSc3	norština
a	a	k8xC	a
švédštině	švédština	k1gFnSc3	švédština
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
norštině	norština	k1gFnSc3	norština
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
její	její	k3xOp3gFnSc3	její
spisovné	spisovný	k2eAgFnSc3d1	spisovná
variantě	varianta	k1gFnSc3	varianta
bokmå	bokmå	k?	bokmå
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
ze	z	k7c2	z
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
Norsko	Norsko	k1gNnSc1	Norsko
bylo	být	k5eAaImAgNnS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1380	[number]	k4	1380
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1814	[number]	k4	1814
pod	pod	k7c7	pod
dánskou	dánský	k2eAgFnSc7d1	dánská
vládou	vláda	k1gFnSc7	vláda
a	a	k8xC	a
dánština	dánština	k1gFnSc1	dánština
tam	tam	k6eAd1	tam
byla	být	k5eAaImAgFnS	být
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
úředním	úřední	k2eAgNnSc7d1	úřední
a	a	k8xC	a
spisovným	spisovný	k2eAgInSc7d1	spisovný
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Bokmå	Bokmå	k?	Bokmå
byl	být	k5eAaImAgInS	být
kodifikován	kodifikovat	k5eAaBmNgInS	kodifikovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
jazyka	jazyk	k1gInSc2	jazyk
dánsko-norského	dánskoorský	k2eAgInSc2d1	dánsko-norský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
morfologii	morfologie	k1gFnSc6	morfologie
dánštiny	dánština	k1gFnSc2	dánština
došlo	dojít	k5eAaPmAgNnS	dojít
během	během	k7c2	během
historického	historický	k2eAgInSc2d1	historický
vývoje	vývoj	k1gInSc2	vývoj
ke	k	k7c3	k
značným	značný	k2eAgNnPc3d1	značné
zjednodušením	zjednodušení	k1gNnPc3	zjednodušení
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
ohýbání	ohýbání	k1gNnSc2	ohýbání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jí	jíst	k5eAaImIp3nS	jíst
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
dává	dávat	k5eAaImIp3nS	dávat
charakter	charakter	k1gInSc4	charakter
analytického	analytický	k2eAgInSc2d1	analytický
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jmenný	jmenný	k2eAgInSc1d1	jmenný
rod	rod	k1gInSc1	rod
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
dánštině	dánština	k1gFnSc6	dánština
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
splynutí	splynutí	k1gNnSc3	splynutí
mužského	mužský	k2eAgInSc2d1	mužský
a	a	k8xC	a
ženského	ženský	k2eAgInSc2d1	ženský
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
moderní	moderní	k2eAgFnSc1d1	moderní
dánština	dánština	k1gFnSc1	dánština
má	mít	k5eAaImIp3nS	mít
2	[number]	k4	2
rody	rod	k1gInPc7	rod
–	–	k?	–
společný	společný	k2eAgInSc4d1	společný
a	a	k8xC	a
střední	střední	k2eAgInSc4d1	střední
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Podstatná	podstatný	k2eAgNnPc1d1	podstatné
jména	jméno	k1gNnPc1	jméno
===	===	k?	===
</s>
</p>
<p>
<s>
Z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
systému	systém	k1gInSc2	systém
4	[number]	k4	4
pádů	pád	k1gInPc2	pád
se	se	k3xPyFc4	se
u	u	k7c2	u
podstatných	podstatný	k2eAgNnPc2d1	podstatné
jmen	jméno	k1gNnPc2	jméno
zachoval	zachovat	k5eAaPmAgMnS	zachovat
nominativ	nominativ	k1gInSc4	nominativ
a	a	k8xC	a
genitiv	genitiv	k1gInSc4	genitiv
(	(	kIx(	(
<g/>
zakončení	zakončení	k1gNnSc1	zakončení
-s	-s	k?	-s
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
funkci	funkce	k1gFnSc4	funkce
výlučně	výlučně	k6eAd1	výlučně
přivlastňovací	přivlastňovací	k2eAgFnSc4d1	přivlastňovací
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
podstatné	podstatný	k2eAgNnSc4d1	podstatné
jméno	jméno	k1gNnSc4	jméno
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
nominativu	nominativ	k1gInSc2	nominativ
<g/>
.	.	kIx.	.
</s>
<s>
Syntaktické	syntaktický	k2eAgInPc1d1	syntaktický
vztahy	vztah	k1gInPc1	vztah
se	se	k3xPyFc4	se
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
pomocí	pomocí	k7c2	pomocí
předložek	předložka	k1gFnPc2	předložka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
skandinávských	skandinávský	k2eAgInPc6d1	skandinávský
jazycích	jazyk	k1gInPc6	jazyk
se	se	k3xPyFc4	se
určitý	určitý	k2eAgInSc1d1	určitý
člen	člen	k1gInSc1	člen
připojuje	připojovat	k5eAaImIp3nS	připojovat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
koncovky	koncovka	k1gFnSc2	koncovka
k	k	k7c3	k
podstatnému	podstatný	k2eAgNnSc3d1	podstatné
jménu	jméno	k1gNnSc3	jméno
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
-	-	kIx~	-
mand	mand	k?	mand
"	"	kIx"	"
<g/>
muž	muž	k1gMnSc1	muž
<g/>
"	"	kIx"	"
→	→	k?	→
manden	mandna	k1gFnPc2	mandna
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
ten	ten	k3xDgMnSc1	ten
<g/>
)	)	kIx)	)
muž	muž	k1gMnSc1	muž
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
-	-	kIx~	-
pige	pige	k1gNnPc6	pige
"	"	kIx"	"
<g/>
holka	holka	k1gFnSc1	holka
<g/>
"	"	kIx"	"
→	→	k?	→
pigen	pigen	k1gInSc1	pigen
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
ta	ten	k3xDgFnSc1	ten
<g/>
)	)	kIx)	)
holka	holka	k1gFnSc1	holka
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
-	-	kIx~	-
hus	husa	k1gFnPc2	husa
"	"	kIx"	"
<g/>
dům	dům	k1gInSc1	dům
<g/>
"	"	kIx"	"
→	→	k?	→
huset	huset	k1gInSc1	huset
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
ten	ten	k3xDgInSc1	ten
<g/>
)	)	kIx)	)
dům	dům	k1gInSc1	dům
<g/>
"	"	kIx"	"
<g/>
Množné	množný	k2eAgNnSc1d1	množné
číslo	číslo	k1gNnSc1	číslo
</s>
</p>
<p>
<s>
-	-	kIx~	-
mæ	mæ	k?	mæ
"	"	kIx"	"
<g/>
muži	muž	k1gMnPc1	muž
<g/>
"	"	kIx"	"
→	→	k?	→
mæ	mæ	k?	mæ
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
ti	ten	k3xDgMnPc1	ten
<g/>
)	)	kIx)	)
muži	muž	k1gMnPc1	muž
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
-	-	kIx~	-
piger	piger	k1gInSc1	piger
"	"	kIx"	"
<g/>
holky	holka	k1gFnPc1	holka
<g/>
"	"	kIx"	"
→	→	k?	→
pigerne	pigernout	k5eAaPmIp3nS	pigernout
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
ty	ten	k3xDgFnPc4	ten
<g/>
)	)	kIx)	)
holky	holka	k1gFnPc1	holka
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
-	-	kIx~	-
huse	husa	k1gFnSc3	husa
"	"	kIx"	"
<g/>
domy	dům	k1gInPc4	dům
<g/>
"	"	kIx"	"
→	→	k?	→
husene	husen	k1gInSc5	husen
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
ty	ten	k3xDgInPc4	ten
<g/>
)	)	kIx)	)
domy	dům	k1gInPc1	dům
<g/>
"	"	kIx"	"
<g/>
Množné	množný	k2eAgNnSc1d1	množné
číslo	číslo	k1gNnSc1	číslo
končí	končit	k5eAaImIp3nS	končit
většinou	většina	k1gFnSc7	většina
na	na	k7c6	na
-er	-er	k?	-er
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
některá	některý	k3yIgNnPc1	některý
slova	slovo	k1gNnPc1	slovo
přehlasují	přehlasovat	k5eAaBmIp3nP	přehlasovat
kmenovou	kmenový	k2eAgFnSc4d1	kmenová
samohlásku	samohláska	k1gFnSc4	samohláska
<g/>
:	:	kIx,	:
tand	tand	k1gInSc4	tand
'	'	kIx"	'
<g/>
zub	zub	k1gInSc4	zub
<g/>
'	'	kIx"	'
→	→	k?	→
tæ	tæ	k?	tæ
'	'	kIx"	'
<g/>
zuby	zub	k1gInPc4	zub
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Ojediněle	ojediněle	k6eAd1	ojediněle
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
nepravidelnostem	nepravidelnost	k1gFnPc3	nepravidelnost
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
barn	barn	k1gMnSc1	barn
'	'	kIx"	'
<g/>
dítě	dítě	k1gNnSc1	dítě
<g/>
'	'	kIx"	'
→	→	k?	→
bø	bø	k?	bø
'	'	kIx"	'
<g/>
děti	dítě	k1gFnPc1	dítě
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednoslabičná	jednoslabičný	k2eAgNnPc1d1	jednoslabičné
neutra	neutrum	k1gNnPc1	neutrum
a	a	k8xC	a
některá	některý	k3yIgNnPc1	některý
jednoslabičná	jednoslabičný	k2eAgNnPc1d1	jednoslabičné
maskulina	maskulinum	k1gNnPc1	maskulinum
mají	mít	k5eAaImIp3nP	mít
většinou	většina	k1gFnSc7	většina
v	v	k7c6	v
množném	množný	k2eAgNnSc6d1	množné
čísle	číslo	k1gNnSc6	číslo
-e	-e	k?	-e
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
hus	husa	k1gFnPc2	husa
'	'	kIx"	'
<g/>
dům	dům	k1gInSc1	dům
<g/>
'	'	kIx"	'
→	→	k?	→
huse	husa	k1gFnSc3	husa
'	'	kIx"	'
<g/>
domy	dům	k1gInPc4	dům
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
brev	brev	k1gInSc1	brev
'	'	kIx"	'
<g/>
dopis	dopis	k1gInSc1	dopis
<g/>
'	'	kIx"	'
→	→	k?	→
breve	breve	k1gNnSc1	breve
'	'	kIx"	'
<g/>
dopisy	dopis	k1gInPc1	dopis
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
hest	hest	k1gMnSc1	hest
'	'	kIx"	'
<g/>
kůň	kůň	k1gMnSc1	kůň
<g/>
'	'	kIx"	'
→	→	k?	→
heste	heste	k5eAaPmIp2nP	heste
'	'	kIx"	'
<g/>
koně	kůň	k1gMnPc4	kůň
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Slovesa	sloveso	k1gNnSc2	sloveso
===	===	k?	===
</s>
</p>
<p>
<s>
Systém	systém	k1gInSc1	systém
sloves	sloveso	k1gNnPc2	sloveso
má	mít	k5eAaImIp3nS	mít
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
i	i	k9	i
v	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
germánských	germánský	k2eAgInPc6d1	germánský
jazycích	jazyk	k1gInPc6	jazyk
<g/>
,	,	kIx,	,
jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
tvary	tvar	k1gInPc1	tvar
v	v	k7c6	v
přítomném	přítomný	k2eAgInSc6d1	přítomný
a	a	k8xC	a
minulém	minulý	k2eAgInSc6d1	minulý
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
časy	čas	k1gInPc1	čas
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
pomocnými	pomocný	k2eAgNnPc7d1	pomocné
slovesy	sloveso	k1gNnPc7	sloveso
<g/>
.	.	kIx.	.
</s>
<s>
Slovesa	sloveso	k1gNnPc1	sloveso
se	se	k3xPyFc4	se
neobejdou	obejít	k5eNaPmIp3nP	obejít
bez	bez	k7c2	bez
osobních	osobní	k2eAgNnPc2d1	osobní
zájmen	zájmeno	k1gNnPc2	zájmeno
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pozbyla	pozbýt	k5eAaPmAgFnS	pozbýt
skoro	skoro	k6eAd1	skoro
všech	všecek	k3xTgFnPc2	všecek
odlišujících	odlišující	k2eAgFnPc2d1	odlišující
osobních	osobní	k2eAgFnPc2d1	osobní
koncovek	koncovka	k1gFnPc2	koncovka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přítomném	přítomný	k2eAgInSc6d1	přítomný
čase	čas	k1gInSc6	čas
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
pouze	pouze	k6eAd1	pouze
koncovka	koncovka	k1gFnSc1	koncovka
-r	-r	k?	-r
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
původně	původně	k6eAd1	původně
označovala	označovat	k5eAaImAgFnS	označovat
druhou	druhý	k4xOgFnSc4	druhý
a	a	k8xC	a
třetí	třetí	k4xOgFnSc4	třetí
osobu	osoba	k1gFnSc4	osoba
jednotného	jednotný	k2eAgNnSc2d1	jednotné
čísla	číslo	k1gNnSc2	číslo
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
islandštině	islandština	k1gFnSc6	islandština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
at	at	k?	at
bo	bo	k?	bo
'	'	kIx"	'
<g/>
bydlet	bydlet	k5eAaImF	bydlet
<g/>
'	'	kIx"	'
-	-	kIx~	-
jeg	jeg	k?	jeg
bor	bor	k1gInSc1	bor
'	'	kIx"	'
<g/>
bydlím	bydlet	k5eAaImIp1nS	bydlet
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
at	at	k?	at
tale	tale	k1gInSc1	tale
'	'	kIx"	'
<g/>
mluvit	mluvit	k5eAaImF	mluvit
-	-	kIx~	-
vi	vi	k?	vi
taler	taler	k1gInSc1	taler
'	'	kIx"	'
<g/>
mluvíme	mluvit	k5eAaImIp1nP	mluvit
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
<g/>
Jako	jako	k8xC	jako
všechny	všechen	k3xTgInPc1	všechen
germánské	germánský	k2eAgInPc1d1	germánský
jazyky	jazyk	k1gInPc1	jazyk
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
i	i	k9	i
skandinávské	skandinávský	k2eAgInPc4d1	skandinávský
jazyky	jazyk	k1gInPc4	jazyk
slabá	slabý	k2eAgFnSc1d1	slabá
a	a	k8xC	a
silná	silný	k2eAgNnPc1d1	silné
slovesa	sloveso	k1gNnPc1	sloveso
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Slabá	Slabá	k1gFnSc1	Slabá
slovesa	sloveso	k1gNnSc2	sloveso
</s>
</p>
<p>
<s>
at	at	k?	at
bo	bo	k?	bo
'	'	kIx"	'
<g/>
bydlet	bydlet	k5eAaImF	bydlet
<g/>
'	'	kIx"	'
–	–	k?	–
(	(	kIx(	(
<g/>
pres	pres	k1gInSc1	pres
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
bor	bor	k1gInSc1	bor
–	–	k?	–
(	(	kIx(	(
<g/>
pret	pret	k1gMnSc1	pret
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
boede	boede	k6eAd1	boede
–	–	k?	–
(	(	kIx(	(
<g/>
part	part	k1gInSc4	part
<g/>
.	.	kIx.	.
perf	perf	k1gInSc4	perf
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
boet	boet	k1gInSc1	boet
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
at	at	k?	at
huske	huske	k1gInSc1	huske
'	'	kIx"	'
<g/>
vzpomínat	vzpomínat	k5eAaImF	vzpomínat
si	se	k3xPyFc3	se
<g/>
'	'	kIx"	'
–	–	k?	–
huskede	huskíst	k5eAaPmIp3nS	huskíst
–	–	k?	–
husket	husket	k1gInSc1	husket
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
at	at	k?	at
kø	kø	k?	kø
'	'	kIx"	'
<g/>
koupit	koupit	k5eAaPmF	koupit
<g/>
'	'	kIx"	'
–	–	k?	–
kø	kø	k?	kø
–	–	k?	–
kø	kø	k?	kø
<g/>
.	.	kIx.	.
<g/>
Silná	silný	k2eAgNnPc1d1	silné
slovesa	sloveso	k1gNnPc1	sloveso
</s>
</p>
<p>
<s>
at	at	k?	at
se	se	k3xPyFc4	se
'	'	kIx"	'
<g/>
vidět	vidět	k5eAaImF	vidět
<g/>
'	'	kIx"	'
–	–	k?	–
ser	srát	k5eAaImRp2nS	srát
–	–	k?	–
så	så	k?	så
–	–	k?	–
set	set	k1gInSc4	set
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
at	at	k?	at
få	få	k?	få
'	'	kIx"	'
<g/>
dostat	dostat	k5eAaPmF	dostat
<g/>
;	;	kIx,	;
smět	smět	k5eAaImF	smět
<g/>
'	'	kIx"	'
–	–	k?	–
få	få	k?	få
–	–	k?	–
fik	fik	k0	fik
–	–	k?	–
få	få	k?	få
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
at	at	k?	at
træ	træ	k?	træ
'	'	kIx"	'
<g/>
táhnout	táhnout	k5eAaImF	táhnout
<g/>
'	'	kIx"	'
–	–	k?	–
træ	træ	k?	træ
–	–	k?	–
trak	trak	k?	trak
–	–	k?	–
trukket	trukket	k1gInSc1	trukket
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Přídavná	přídavný	k2eAgNnPc1d1	přídavné
jména	jméno	k1gNnPc1	jméno
===	===	k?	===
</s>
</p>
<p>
<s>
Přídavná	přídavný	k2eAgNnPc1d1	přídavné
jména	jméno	k1gNnPc1	jméno
mají	mít	k5eAaImIp3nP	mít
určitý	určitý	k2eAgInSc4d1	určitý
a	a	k8xC	a
neurčitý	určitý	k2eNgInSc4d1	neurčitý
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Neurčitý	určitý	k2eNgInSc1d1	neurčitý
tvar	tvar	k1gInSc1	tvar
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
s	s	k7c7	s
neurčitým	určitý	k2eNgInSc7d1	neurčitý
členem	člen	k1gInSc7	člen
a	a	k8xC	a
při	při	k7c6	při
používání	používání	k1gNnSc6	používání
bez	bez	k7c2	bez
členu	člen	k1gInSc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středním	střední	k2eAgInSc6d1	střední
rodě	rod	k1gInSc6	rod
přibírá	přibírat	k5eAaImIp3nS	přibírat
přídavné	přídavný	k2eAgNnSc4d1	přídavné
jméno	jméno	k1gNnSc4	jméno
koncovku	koncovka	k1gFnSc4	koncovka
-t	-t	k?	-t
(	(	kIx(	(
<g/>
tento	tento	k3xDgInSc1	tento
tvar	tvar	k1gInSc1	tvar
často	často	k6eAd1	často
funguje	fungovat	k5eAaImIp3nS	fungovat
i	i	k9	i
jako	jako	k9	jako
příslovce	příslovce	k1gNnSc1	příslovce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
množném	množný	k2eAgNnSc6d1	množné
čísle	číslo	k1gNnSc6	číslo
mají	mít	k5eAaImIp3nP	mít
koncovku	koncovka	k1gFnSc4	koncovka
-	-	kIx~	-
<g/>
e.	e.	k?	e.
Např.	např.	kA	např.
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
en	en	k?	en
dyr	dyr	k?	dyr
bil	bít	k5eAaImAgMnS	bít
'	'	kIx"	'
<g/>
(	(	kIx(	(
<g/>
nějaké	nějaký	k3yIgNnSc1	nějaký
<g/>
)	)	kIx)	)
drahé	drahý	k2eAgNnSc1d1	drahé
auto	auto	k1gNnSc1	auto
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
et	et	k?	et
gammelt	gammelt	k1gInSc1	gammelt
hus	husa	k1gFnPc2	husa
'	'	kIx"	'
<g/>
(	(	kIx(	(
<g/>
nějaký	nějaký	k3yIgInSc1	nějaký
<g/>
)	)	kIx)	)
starý	starý	k2eAgInSc1d1	starý
dům	dům	k1gInSc1	dům
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
skittent	skittent	k1gMnSc1	skittent
vand	vand	k1gMnSc1	vand
'	'	kIx"	'
<g/>
špinavá	špinavý	k2eAgFnSc1d1	špinavá
voda	voda	k1gFnSc1	voda
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
rø	rø	k?	rø
klæ	klæ	k?	klæ
'	'	kIx"	'
<g/>
červené	červený	k2eAgInPc4d1	červený
šaty	šat	k1gInPc4	šat
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
tvaru	tvar	k1gInSc6	tvar
se	se	k3xPyFc4	se
připojuje	připojovat	k5eAaImIp3nS	připojovat
koncovka	koncovka	k1gFnSc1	koncovka
-e	-e	k?	-e
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
rodech	rod	k1gInPc6	rod
v	v	k7c6	v
jednotném	jednotný	k2eAgNnSc6d1	jednotné
i	i	k8xC	i
množném	množný	k2eAgNnSc6d1	množné
čísle	číslo	k1gNnSc6	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Určitý	určitý	k2eAgInSc4d1	určitý
tvar	tvar	k1gInSc4	tvar
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
s	s	k7c7	s
určitým	určitý	k2eAgInSc7d1	určitý
členem	člen	k1gInSc7	člen
(	(	kIx(	(
<g/>
den	den	k1gInSc1	den
pro	pro	k7c4	pro
společný	společný	k2eAgInSc4d1	společný
rod	rod	k1gInSc4	rod
<g/>
,	,	kIx,	,
det	det	k?	det
pro	pro	k7c4	pro
střední	střední	k2eAgInSc4d1	střední
rod	rod	k1gInSc4	rod
<g/>
,	,	kIx,	,
v	v	k7c6	v
množném	množný	k2eAgNnSc6d1	množné
čísle	číslo	k1gNnSc6	číslo
pro	pro	k7c4	pro
oba	dva	k4xCgInPc4	dva
rody	rod	k1gInPc4	rod
de	de	k?	de
[	[	kIx(	[
<g/>
di	di	k?	di
<g/>
:	:	kIx,	:
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
stojí	stát	k5eAaImIp3nS	stát
před	před	k7c7	před
přídavným	přídavný	k2eAgNnSc7d1	přídavné
jménem	jméno	k1gNnSc7	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Připojený	připojený	k2eAgMnSc1d1	připojený
člen	člen	k1gMnSc1	člen
přitom	přitom	k6eAd1	přitom
odpadá	odpadat	k5eAaImIp3nS	odpadat
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
den	den	k1gInSc4	den
dyre	dyre	k6eAd1	dyre
bil	bít	k5eAaImAgMnS	bít
'	'	kIx"	'
<g/>
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
<g/>
)	)	kIx)	)
drahé	drahý	k2eAgNnSc1d1	drahé
auto	auto	k1gNnSc1	auto
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
det	det	k?	det
gamle	gamle	k1gInSc1	gamle
hus	husa	k1gFnPc2	husa
(	(	kIx(	(
<g/>
ten	ten	k3xDgInSc1	ten
<g/>
)	)	kIx)	)
starý	starý	k2eAgInSc1d1	starý
dům	dům	k1gInSc1	dům
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
de	de	k?	de
rø	rø	k?	rø
klæ	klæ	k?	klæ
'	'	kIx"	'
<g/>
(	(	kIx(	(
<g/>
ty	ten	k3xDgInPc1	ten
<g/>
)	)	kIx)	)
červené	červený	k2eAgInPc1d1	červený
šaty	šat	k1gInPc1	šat
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
<g/>
Některá	některý	k3yIgNnPc1	některý
adjektiva	adjektivum	k1gNnPc1	adjektivum
se	se	k3xPyFc4	se
neskloňují	skloňovat	k5eNaImIp3nP	skloňovat
<g/>
:	:	kIx,	:
bra	bra	k?	bra
'	'	kIx"	'
<g/>
dobrý	dobrý	k2eAgMnSc1d1	dobrý
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
moderne	modernout	k5eAaPmIp3nS	modernout
'	'	kIx"	'
<g/>
moderní	moderní	k2eAgFnSc4d1	moderní
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
tilfreds	tilfreds	k1gInSc1	tilfreds
'	'	kIx"	'
<g/>
spokojený	spokojený	k2eAgMnSc1d1	spokojený
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
stakkars	stakkars	k1gInSc1	stakkars
'	'	kIx"	'
<g/>
ubohý	ubohý	k2eAgMnSc1d1	ubohý
<g/>
'	'	kIx"	'
aj.	aj.	kA	aj.
</s>
</p>
<p>
<s>
Přídavné	přídavný	k2eAgNnSc1d1	přídavné
jméno	jméno	k1gNnSc1	jméno
se	se	k3xPyFc4	se
přizpůsobuje	přizpůsobovat	k5eAaImIp3nS	přizpůsobovat
podstatnému	podstatný	k2eAgNnSc3d1	podstatné
jménu	jméno	k1gNnSc3	jméno
i	i	k8xC	i
v	v	k7c6	v
přísudkové	přísudkový	k2eAgFnSc6d1	přísudková
pozici	pozice	k1gFnSc6	pozice
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Bilen	Bilen	k1gInSc1	Bilen
er	er	k?	er
dyr	dyr	k?	dyr
<g/>
.	.	kIx.	.
'	'	kIx"	'
<g/>
Auto	auto	k1gNnSc1	auto
je	být	k5eAaImIp3nS	být
drahé	drahý	k2eAgNnSc1d1	drahé
<g/>
.	.	kIx.	.
<g/>
'	'	kIx"	'
</s>
</p>
<p>
<s>
Bilerne	Bilernout	k5eAaPmIp3nS	Bilernout
er	er	k?	er
dyre	dyre	k1gFnSc1	dyre
<g/>
.	.	kIx.	.
'	'	kIx"	'
<g/>
Auta	auto	k1gNnPc1	auto
jsou	být	k5eAaImIp3nP	být
drahá	drahý	k2eAgNnPc1d1	drahé
<g/>
.	.	kIx.	.
<g/>
'	'	kIx"	'
</s>
</p>
<p>
<s>
Huset	Huset	k1gMnSc1	Huset
er	er	k?	er
gammelt	gammelt	k1gMnSc1	gammelt
<g/>
.	.	kIx.	.
'	'	kIx"	'
<g/>
Dům	dům	k1gInSc1	dům
je	být	k5eAaImIp3nS	být
starý	starý	k2eAgInSc1d1	starý
<g/>
.	.	kIx.	.
<g/>
'	'	kIx"	'
</s>
</p>
<p>
<s>
Husene	Husen	k1gMnSc5	Husen
er	er	k?	er
gamle	gamle	k1gNnPc4	gamle
<g/>
.	.	kIx.	.
'	'	kIx"	'
<g/>
Domy	dům	k1gInPc1	dům
jsou	být	k5eAaImIp3nP	být
staré	starý	k2eAgMnPc4d1	starý
<g/>
.	.	kIx.	.
<g/>
'	'	kIx"	'
</s>
</p>
<p>
<s>
==	==	k?	==
Abeceda	abeceda	k1gFnSc1	abeceda
==	==	k?	==
</s>
</p>
<p>
<s>
Dánština	dánština	k1gFnSc1	dánština
používá	používat	k5eAaImIp3nS	používat
anglickou	anglický	k2eAgFnSc4d1	anglická
abecedu	abeceda	k1gFnSc4	abeceda
bez	bez	k7c2	bez
písmene	písmeno	k1gNnSc2	písmeno
Q.	Q.	kA	Q.
Navíc	navíc	k6eAd1	navíc
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
následující	následující	k2eAgNnPc4d1	následující
písmena	písmeno	k1gNnPc4	písmeno
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
umísťují	umísťovat	k5eAaImIp3nP	umísťovat
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
pořadí	pořadí	k1gNnSc6	pořadí
na	na	k7c4	na
konec	konec	k1gInSc4	konec
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
æ	æ	k?	æ
Æ	Æ	k?	Æ
</s>
</p>
<p>
<s>
ø	ø	k?	ø
Ø	Ø	k?	Ø
</s>
</p>
<p>
<s>
å	å	k?	å
Å	Å	k?	Å
c	c	k0	c
<g/>
,	,	kIx,	,
w	w	k?	w
<g/>
,	,	kIx,	,
x	x	k?	x
<g/>
,	,	kIx,	,
z	z	k7c2	z
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
jen	jen	k9	jen
ve	v	k7c6	v
slovech	slovo	k1gNnPc6	slovo
přejatých	přejatý	k2eAgNnPc6d1	přejaté
(	(	kIx(	(
<g/>
např.	např.	kA	např.
cykler	cykler	k1gInSc1	cykler
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
cyklista	cyklista	k1gMnSc1	cyklista
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
češtiny	čeština	k1gFnSc2	čeština
je	být	k5eAaImIp3nS	být
spřežka	spřežka	k1gFnSc1	spřežka
ch	ch	k0	ch
–	–	k?	–
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
dánštině	dánština	k1gFnSc6	dánština
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jen	jen	k9	jen
v	v	k7c6	v
cizích	cizí	k2eAgNnPc6d1	cizí
slovech	slovo	k1gNnPc6	slovo
–	–	k?	–
chápána	chápat	k5eAaImNgFnS	chápat
jako	jako	k8xS	jako
dvě	dva	k4xCgNnPc4	dva
písmena	písmeno	k1gNnPc4	písmeno
c	c	k0	c
a	a	k8xC	a
h	h	k?	h
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
řazeno	řazen	k2eAgNnSc1d1	řazeno
v	v	k7c6	v
abecedě	abeceda	k1gFnSc6	abeceda
mezi	mezi	k7c7	mezi
ce	ce	k?	ce
a	a	k8xC	a
ci	ci	k0	ci
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nářečí	nářečí	k1gNnSc2	nářečí
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
hovoru	hovor	k1gInSc6	hovor
Dánové	Dán	k1gMnPc1	Dán
používají	používat	k5eAaImIp3nP	používat
nářečí	nářečí	k1gNnPc4	nářečí
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
do	do	k7c2	do
3	[number]	k4	3
základních	základní	k2eAgFnPc2d1	základní
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
ostrovní	ostrovní	k2eAgNnSc1d1	ostrovní
nářečí	nářečí	k1gNnSc1	nářečí
(	(	kIx(	(
<g/>
ø	ø	k?	ø
<g/>
)	)	kIx)	)
–	–	k?	–
oblast	oblast	k1gFnSc4	oblast
ostrovů	ostrov	k1gInPc2	ostrov
Zéland	Zéland	k1gInSc1	Zéland
<g/>
,	,	kIx,	,
Funen	Funen	k1gInSc1	Funen
<g/>
,	,	kIx,	,
Lolland	Lolland	k1gInSc1	Lolland
<g/>
,	,	kIx,	,
Falster	Falster	k1gInSc1	Falster
a	a	k8xC	a
Mø	Mø	k1gFnSc1	Mø
<g/>
;	;	kIx,	;
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
nářečí	nářečí	k1gNnSc4	nářečí
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Kodaně	Kodaň	k1gFnSc2	Kodaň
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
vychází	vycházet	k5eAaImIp3nS	vycházet
základ	základ	k1gInSc1	základ
spisovné	spisovný	k2eAgFnSc2d1	spisovná
dánštiny	dánština	k1gFnSc2	dánština
(	(	kIx(	(
<g/>
rigsdansk	rigsdansk	k1gInSc1	rigsdansk
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
jutská	jutský	k2eAgNnPc1d1	jutský
nářečí	nářečí	k1gNnPc1	nářečí
(	(	kIx(	(
<g/>
jysk	jysk	k1gInSc1	jysk
<g/>
)	)	kIx)	)
–	–	k?	–
oblast	oblast	k1gFnSc4	oblast
Jutského	jutský	k2eAgInSc2d1	jutský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
;	;	kIx,	;
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
<g/>
,	,	kIx,	,
východní	východní	k2eAgFnSc6d1	východní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc6d1	jižní
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
bornholmské	bornholmský	k2eAgNnSc1d1	bornholmský
nářečí	nářečí	k1gNnSc1	nářečí
(	(	kIx(	(
<g/>
bornholmsk	bornholmsk	k1gInSc1	bornholmsk
<g/>
)	)	kIx)	)
–	–	k?	–
ostrov	ostrov	k1gInSc1	ostrov
Bornholm	Bornholm	k1gInSc1	Bornholm
<g/>
;	;	kIx,	;
jediný	jediný	k2eAgMnSc1d1	jediný
zástupce	zástupce	k1gMnSc1	zástupce
východánských	východánský	k2eAgFnPc2d1	východánský
nářečí	nářečí	k1gNnPc2	nářečí
<g/>
,	,	kIx,	,
kterými	který	k3yRgNnPc7	který
se	se	k3xPyFc4	se
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
hovořilo	hovořit	k5eAaImAgNnS	hovořit
také	také	k9	také
v	v	k7c6	v
jižních	jižní	k2eAgFnPc6d1	jižní
provinciích	provincie	k1gFnPc6	provincie
Skandinávského	skandinávský	k2eAgInSc2d1	skandinávský
poloostrova	poloostrov	k1gInSc2	poloostrov
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
připadly	připadnout	k5eAaPmAgFnP	připadnout
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
rozlišujícím	rozlišující	k2eAgInSc7d1	rozlišující
rysem	rys	k1gInSc7	rys
nářečí	nářečí	k1gNnSc2	nářečí
je	být	k5eAaImIp3nS	být
přítomnost	přítomnost	k1gFnSc1	přítomnost
rázu	ráz	k1gInSc2	ráz
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
nářečí	nářečí	k1gNnSc2	nářečí
má	mít	k5eAaImIp3nS	mít
ráz	ráz	k1gInSc4	ráz
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
rázu	ráz	k1gInSc2	ráz
jsou	být	k5eAaImIp3nP	být
nářečí	nářečí	k1gNnSc4	nářečí
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
těchto	tento	k3xDgNnPc2	tento
nářečí	nářečí	k1gNnPc2	nářečí
má	mít	k5eAaImIp3nS	mít
melodický	melodický	k2eAgInSc1d1	melodický
přízvuk	přízvuk	k1gInSc1	přízvuk
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
švédština	švédština	k1gFnSc1	švédština
a	a	k8xC	a
norština	norština	k1gFnSc1	norština
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
korelaci	korelace	k1gFnSc6	korelace
s	s	k7c7	s
rázem	ráz	k1gInSc7	ráz
v	v	k7c6	v
jiných	jiný	k2eAgNnPc6d1	jiné
nářečích	nářečí	k1gNnPc6	nářečí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nářečí	nářečí	k1gNnPc1	nářečí
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
také	také	k9	také
počtem	počet	k1gInSc7	počet
mluvnických	mluvnický	k2eAgInPc2d1	mluvnický
rodů	rod	k1gInPc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
nářečí	nářečí	k1gNnPc1	nářečí
mají	mít	k5eAaImIp3nP	mít
dva	dva	k4xCgInPc1	dva
rody	rod	k1gInPc1	rod
(	(	kIx(	(
<g/>
společný	společný	k2eAgInSc1d1	společný
a	a	k8xC	a
střední	střední	k2eAgFnSc1d1	střední
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
spisovná	spisovný	k2eAgFnSc1d1	spisovná
dánština	dánština	k1gFnSc1	dánština
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
nářečí	nářečí	k1gNnPc4	nářečí
však	však	k9	však
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
systém	systém	k1gInSc4	systém
tří	tři	k4xCgInPc2	tři
rodů	rod	k1gInPc2	rod
(	(	kIx(	(
<g/>
mužský	mužský	k2eAgInSc4d1	mužský
<g/>
,	,	kIx,	,
ženský	ženský	k2eAgInSc4d1	ženský
a	a	k8xC	a
střední	střední	k2eAgInSc4d1	střední
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nářečí	nářečí	k1gNnSc1	nářečí
na	na	k7c6	na
západě	západ	k1gInSc6	západ
země	zem	k1gFnSc2	zem
rody	rod	k1gInPc1	rod
vůbec	vůbec	k9	vůbec
nerozlišují	rozlišovat	k5eNaImIp3nP	rozlišovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nářečí	nářečí	k1gNnSc1	nářečí
na	na	k7c6	na
západě	západ	k1gInSc6	západ
a	a	k8xC	a
jihu	jih	k1gInSc6	jih
Jutska	Jutsko	k1gNnSc2	Jutsko
také	také	k9	také
nemají	mít	k5eNaImIp3nP	mít
připojený	připojený	k2eAgInSc4d1	připojený
určitý	určitý	k2eAgInSc4d1	určitý
člen	člen	k1gInSc4	člen
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
vždy	vždy	k6eAd1	vždy
stojí	stát	k5eAaImIp3nS	stát
před	před	k7c7	před
podstatným	podstatný	k2eAgNnSc7d1	podstatné
jménem	jméno	k1gNnSc7	jméno
<g/>
,	,	kIx,	,
např.	např.	kA	např.
æ	æ	k?	æ
hus	husa	k1gFnPc2	husa
–	–	k?	–
spisovně	spisovně	k6eAd1	spisovně
huset	huset	k5eAaImF	huset
(	(	kIx(	(
<g/>
ten	ten	k3xDgInSc4	ten
dům	dům	k1gInSc4	dům
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BERKOV	BERKOV	kA	BERKOV
<g/>
,	,	kIx,	,
Valerij	Valerij	k1gMnSc1	Valerij
Pavlovič	Pavlovič	k1gMnSc1	Pavlovič
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgInPc1d1	současný
germánské	germánský	k2eAgInPc1d1	germánský
jazyky	jazyk	k1gInPc1	jazyk
=	=	kIx~	=
Sovremennyje	Sovremennyje	k1gFnSc1	Sovremennyje
germanskije	germanskít	k5eAaPmIp3nS	germanskít
jazyki	jazyki	k6eAd1	jazyki
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Renata	Renata	k1gFnSc1	Renata
Blatná	blatný	k2eAgFnSc1d1	Blatná
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
246	[number]	k4	246
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
268	[number]	k4	268
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skautrup	Skautrup	k1gMnSc1	Skautrup
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
<g/>
:	:	kIx,	:
Det	Det	k1gMnSc1	Det
danske	danskat	k5eAaPmIp3nS	danskat
sprogs	sprogs	k1gInSc4	sprogs
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
<s>
Kø	Kø	k?	Kø
1944	[number]	k4	1944
</s>
</p>
<p>
<s>
===	===	k?	===
Slovníky	slovník	k1gInPc4	slovník
===	===	k?	===
</s>
</p>
<p>
<s>
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
Ottův	Ottův	k2eAgInSc4d1	Ottův
slovník	slovník	k1gInSc4	slovník
naučný	naučný	k2eAgInSc4d1	naučný
VII	VII	kA	VII
<g/>
,	,	kIx,	,
pp	pp	k?	pp
<g/>
.	.	kIx.	.
13	[number]	k4	13
–	–	k?	–
14	[number]	k4	14
</s>
</p>
<p>
<s>
Blanka	Blanka	k1gFnSc1	Blanka
Kirsteinová	Kirsteinová	k1gFnSc1	Kirsteinová
<g/>
,	,	kIx,	,
Blanka	Blanka	k1gFnSc1	Blanka
Borg	Borg	k1gMnSc1	Borg
<g/>
:	:	kIx,	:
Dánsko-český	dánsko-český	k2eAgInSc1d1	dánsko-český
slovník	slovník	k1gInSc1	slovník
<g/>
;	;	kIx,	;
Dansk-Tjekkisk	Dansk-Tjekkisk	k1gInSc1	Dansk-Tjekkisk
ordbog	ordboga	k1gFnPc2	ordboga
<g/>
,	,	kIx,	,
Leda	leda	k6eAd1	leda
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-85927-55-1	[number]	k4	80-85927-55-1
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Kurfürst	Kurfürst	k1gMnSc1	Kurfürst
<g/>
:	:	kIx,	:
Dánský	dánský	k2eAgInSc1d1	dánský
slovník	slovník	k1gInSc1	slovník
<g/>
;	;	kIx,	;
Tjekkisk	Tjekkisk	k1gInSc1	Tjekkisk
ordbog	ordbog	k1gInSc1	ordbog
<g/>
,	,	kIx,	,
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
–	–	k?	–
Forlaget	Forlaget	k1gMnSc1	Forlaget
V	v	k7c6	v
RÁJI	rája	k1gFnSc6	rája
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-85894-72-6	[number]	k4	80-85894-72-6
</s>
</p>
<p>
<s>
Dánsko-český	dánsko-český	k2eAgInSc1d1	dánsko-český
slovník	slovník	k1gInSc1	slovník
online	onlinout	k5eAaPmIp3nS	onlinout
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
dánština	dánština	k1gFnSc1	dánština
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
dánština	dánština	k1gFnSc1	dánština
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnSc1	kategorie
Dánština	dánština	k1gFnSc1	dánština
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Anglické	anglický	k2eAgFnPc1d1	anglická
stránky	stránka	k1gFnPc1	stránka
o	o	k7c6	o
dánské	dánský	k2eAgFnSc6d1	dánská
gramatice	gramatika	k1gFnSc6	gramatika
</s>
</p>
<p>
<s>
Stručná	stručný	k2eAgFnSc1d1	stručná
česko-dánská	českoánský	k2eAgFnSc1d1	česko-dánská
konverzace	konverzace	k1gFnSc1	konverzace
volně	volně	k6eAd1	volně
k	k	k7c3	k
vytištění	vytištění	k1gNnSc3	vytištění
–	–	k?	–
můžete	moct	k5eAaImIp2nP	moct
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
také	také	k9	také
online	onlinout	k5eAaPmIp3nS	onlinout
nakombinovat	nakombinovat	k5eAaPmF	nakombinovat
s	s	k7c7	s
některým	některý	k3yIgInSc7	některý
z	z	k7c2	z
dalších	další	k2eAgInPc2d1	další
45	[number]	k4	45
jazyků	jazyk	k1gInPc2	jazyk
</s>
</p>
