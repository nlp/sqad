<s>
Andělské	andělský	k2eAgInPc1d1
schody	schod	k1gInPc1
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuPřírodní	infoboxuPřírodní	k2eAgFnSc3d1
rezervaceAndělské	rezervaceAndělský	k2eAgFnPc4d1
schodyIUCN	schodyIUCN	k?
kategorie	kategorie	k1gFnPc4
IV	IV	kA
(	(	kIx(
<g/>
Oblast	oblast	k1gFnSc1
výskytu	výskyt	k1gInSc2
druhu	druh	k1gInSc2
<g/>
)	)	kIx)
Jedna	jeden	k4xCgFnSc1
ze	z	k7c2
dvou	dva	k4xCgFnPc2
luk	louka	k1gFnPc2
v	v	k7c6
PR	pr	k0
Andělské	andělský	k2eAgFnPc1d1
schodyZákladní	schodyZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Vyhlášení	vyhlášení	k1gNnSc1
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2002	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Nadm	Nadm	k1gInSc1
<g/>
.	.	kIx.
výška	výška	k1gFnSc1
</s>
<s>
383	#num#	k4
-	-	kIx~
402	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Rozloha	rozloha	k1gFnSc1
</s>
<s>
13,65	13,65	k4
ha	ha	kA
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Okres	okres	k1gInSc1
</s>
<s>
Příbram	Příbram	k1gFnSc1
Umístění	umístění	k1gNnSc2
</s>
<s>
Voznice	voznice	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
49	#num#	k4
<g/>
′	′	k?
<g/>
29,33	29,33	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
13	#num#	k4
<g/>
′	′	k?
<g/>
34,37	34,37	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Andělské	andělský	k2eAgInPc1d1
schody	schod	k1gInPc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Kód	kód	k1gInSc4
</s>
<s>
2155	#num#	k4
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
Andělské	andělský	k2eAgInPc4d1
schody	schod	k1gInPc4
je	být	k5eAaImIp3nS
chráněné	chráněný	k2eAgNnSc1d1
území	území	k1gNnSc1
nacházející	nacházející	k2eAgMnSc1d1
se	se	k3xPyFc4
severně	severně	k6eAd1
až	až	k9
severovýchodně	severovýchodně	k6eAd1
od	od	k7c2
obce	obec	k1gFnSc2
Voznice	voznice	k1gFnSc2
a	a	k8xC
5	#num#	k4
km	km	kA
severovýchodně	severovýchodně	k6eAd1
od	od	k7c2
města	město	k1gNnSc2
Dobříš	Dobříš	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předmětem	předmět	k1gInSc7
ochrany	ochrana	k1gFnSc2
je	být	k5eAaImIp3nS
uchování	uchování	k1gNnSc4
dvou	dva	k4xCgFnPc2
lesních	lesní	k2eAgFnPc2d1
luk	louka	k1gFnPc2
s	s	k7c7
bohatým	bohatý	k2eAgInSc7d1
výskytem	výskyt	k1gInSc7
vzácných	vzácný	k2eAgInPc2d1
a	a	k8xC
zvláště	zvláště	k6eAd1
chráněných	chráněný	k2eAgInPc2d1
druhů	druh	k1gInPc2
rostlin	rostlina	k1gFnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
výskytu	výskyt	k1gInSc2
vzácných	vzácný	k2eAgInPc2d1
a	a	k8xC
ohrožených	ohrožený	k2eAgInPc2d1
druhů	druh	k1gInPc2
bezobratlých	bezobratlý	k2eAgMnPc2d1
živočichů	živočich	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Díky	díky	k7c3
zvlněnému	zvlněný	k2eAgInSc3d1
terénu	terén	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
stupňovitě	stupňovitě	k6eAd1
svažuje	svažovat	k5eAaImIp3nS
od	od	k7c2
Malé	Malé	k2eAgFnSc2d1
Svaté	svatý	k2eAgFnSc2d1
Hory	hora	k1gFnSc2
směrem	směr	k1gInSc7
k	k	k7c3
Dobříši	Dobříš	k1gFnSc3
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
dostala	dostat	k5eAaPmAgFnS
tato	tento	k3xDgFnSc1
lokalita	lokalita	k1gFnSc1
název	název	k1gInSc4
„	„	k?
<g/>
Andělské	andělský	k2eAgInPc4d1
schody	schod	k1gInPc4
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Prudké	Prudké	k2eAgInPc1d1
svahy	svah	k1gInPc1
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
především	především	k9
na	na	k7c6
levém	levý	k2eAgInSc6d1
břehu	břeh	k1gInSc6
Voznického	Voznický	k2eAgInSc2d1
potoka	potok	k1gInSc2
a	a	k8xC
nad	nad	k7c7
voznickým	voznický	k2eAgInSc7d1
rybníkem	rybník	k1gInSc7
Velký	velký	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Území	území	k1gNnSc1
bylo	být	k5eAaImAgNnS
vyhlášeno	vyhlásit	k5eAaPmNgNnS
30	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2002	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Lokalita	lokalita	k1gFnSc1
</s>
<s>
Oblast	oblast	k1gFnSc1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Přírodní	přírodní	k2eAgFnSc1d1
rezervace	rezervace	k1gFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
lesních	lesní	k2eAgFnPc6d1
loukách	louka	k1gFnPc6
severovýchodního	severovýchodní	k2eAgNnSc2d1
Podbrdska	Podbrdsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Andělské	andělský	k2eAgInPc1d1
schody	schod	k1gInPc1
jsou	být	k5eAaImIp3nP
na	na	k7c6
pomezí	pomezí	k1gNnSc6
jednotek	jednotka	k1gFnPc2
Benešovská	benešovský	k2eAgFnSc1d1
pahorkatina	pahorkatina	k1gFnSc1
a	a	k8xC
Brdská	brdský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
lokalita	lokalita	k1gFnSc1
leží	ležet	k5eAaImIp3nS
mezi	mezi	k7c7
městem	město	k1gNnSc7
Mníšek	Mníšek	k1gInSc4
pod	pod	k7c7
Brdy	Brdy	k1gInPc7
a	a	k8xC
obcí	obec	k1gFnSc7
Voznice	voznice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přírodní	přírodní	k2eAgFnSc1d1
rezervace	rezervace	k1gFnSc1
je	být	k5eAaImIp3nS
položena	položit	k5eAaPmNgFnS
ve	v	k7c6
383	#num#	k4
až	až	k9
402	#num#	k4
metrech	metr	k1gInPc6
nad	nad	k7c7
mořem	moře	k1gNnSc7
a	a	k8xC
má	mít	k5eAaImIp3nS
rozlohu	rozloha	k1gFnSc4
13,250	13,250	k4
<g/>
8	#num#	k4
ha	ha	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Celé	celý	k2eAgInPc4d1
toto	tento	k3xDgNnSc1
území	území	k1gNnSc1
je	být	k5eAaImIp3nS
odvodňováno	odvodňovat	k5eAaImNgNnS
Voznický	Voznický	k2eAgInSc1d1
potokem	potok	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
tvoří	tvořit	k5eAaImIp3nS
zachovalé	zachovalý	k2eAgFnPc4d1
potoční	potoční	k2eAgFnPc4d1
nivy	niva	k1gFnPc4
s	s	k7c7
přirozeně	přirozeně	k6eAd1
meandrujícími	meandrující	k2eAgInPc7d1
toky	tok	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
nebyly	být	k5eNaImAgInP
nikdy	nikdy	k6eAd1
regulovány	regulován	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potok	potok	k1gInSc1
se	se	k3xPyFc4
u	u	k7c2
Nového	Nového	k2eAgInSc2d1
Knína	Knín	k1gInSc2
vlévá	vlévat	k5eAaImIp3nS
do	do	k7c2
Kocáby	kocába	k1gFnSc2
a	a	k8xC
ta	ten	k3xDgFnSc1
poté	poté	k6eAd1
do	do	k7c2
Vltavy	Vltava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Ocún	ocún	k1gInSc1
jesenní	jesenní	k2eAgInSc1d1
(	(	kIx(
<g/>
Colchicum	Colchicum	k1gInSc1
autumnale	autumnale	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Andělské	andělský	k2eAgInPc1d1
schody	schod	k1gInPc1
poutaly	poutat	k5eAaImAgInP
pozornost	pozornost	k1gFnSc4
botaniků	botanik	k1gMnPc2
již	již	k6eAd1
před	před	k7c7
několika	několik	k4yIc7
desetiletími	desetiletí	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc4
podnět	podnět	k1gInSc4
na	na	k7c4
ochranu	ochrana	k1gFnSc4
luk	louka	k1gFnPc2
na	na	k7c6
Andělských	andělský	k2eAgInPc6d1
schodech	schod	k1gInPc6
pochází	pocházet	k5eAaImIp3nS
od	od	k7c2
Vladimíra	Vladimír	k1gMnSc2
Skalického	Skalický	k1gMnSc2
a	a	k8xC
Jiřího	Jiří	k1gMnSc2
Manychy	Manycha	k1gMnSc2
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přípravy	příprava	k1gFnPc1
na	na	k7c4
vyhlášení	vyhlášení	k1gNnSc4
zvláště	zvláště	k6eAd1
chráněného	chráněný	k2eAgNnSc2d1
území	území	k1gNnSc2
začaly	začít	k5eAaPmAgFnP
až	až	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
po	po	k7c6
třech	tři	k4xCgNnPc6
letech	léto	k1gNnPc6
byly	být	k5eAaImAgFnP
30	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2002	#num#	k4
Andělské	andělský	k2eAgInPc1d1
schody	schod	k1gInPc1
vyhlášené	vyhlášený	k2eAgInPc1d1
za	za	k7c4
přírodní	přírodní	k2eAgFnSc4d1
rezervaci	rezervace	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
této	tento	k3xDgFnSc2
doby	doba	k1gFnSc2
bylo	být	k5eAaImAgNnS
toto	tento	k3xDgNnSc1
území	území	k1gNnSc1
několikrát	několikrát	k6eAd1
vážně	vážně	k6eAd1
poškozené	poškozený	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
zde	zde	k6eAd1
docházelo	docházet	k5eAaImAgNnS
opakovaně	opakovaně	k6eAd1
k	k	k7c3
rozorání	rozorání	k1gNnSc3
1	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
plochy	plocha	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
nechala	nechat	k5eAaPmAgFnS
ladem	lad	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
části	část	k1gFnSc6
louky	louka	k1gFnSc2
se	se	k3xPyFc4
nacházela	nacházet	k5eAaImAgFnS
největší	veliký	k2eAgFnSc1d3
koncentrace	koncentrace	k1gFnSc1
hořce	hořec	k1gInSc2
hořepníku	hořepník	k1gInSc2
(	(	kIx(
<g/>
Gentiana	Gentiana	k1gFnSc1
pneumonanthe	pneumonanthat	k5eAaPmIp3nS
<g/>
)	)	kIx)
a	a	k8xC
prstnatce	prstnatec	k1gMnSc2
májového	májový	k2eAgMnSc2d1
(	(	kIx(
<g/>
Dactylorhiza	Dactylorhiz	k1gMnSc2
majalis	majalis	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc1d1
negativní	negativní	k2eAgInSc1d1
jev	jev	k1gInSc1
byl	být	k5eAaImAgInS
též	též	k9
v	v	k7c6
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
vyvážení	vyvážení	k1gNnSc1
lidských	lidský	k2eAgFnPc2d1
fekálií	fekálie	k1gFnPc2
na	na	k7c4
značnou	značný	k2eAgFnSc4d1
část	část	k1gFnSc4
lesních	lesní	k2eAgFnPc2d1
luk	louka	k1gFnPc2
okolí	okolí	k1gNnSc2
Dobříše	Dobříš	k1gFnSc2
a	a	k8xC
tedy	tedy	k9
i	i	k9
na	na	k7c4
louky	louka	k1gFnPc4
na	na	k7c6
Andělských	andělský	k2eAgInPc6d1
schodech	schod	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
vyvážení	vyvážení	k1gNnSc1
bylo	být	k5eAaImAgNnS
nelegální	legální	k2eNgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
byl	být	k5eAaImAgInS
podán	podat	k5eAaPmNgInS
návrh	návrh	k1gInSc1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
Andělské	andělský	k2eAgInPc1d1
schody	schod	k1gInPc1
staly	stát	k5eAaPmAgInP
přírodní	přírodní	k2eAgNnSc4d1
památkou	památka	k1gFnSc7
z	z	k7c2
důvodu	důvod	k1gInSc2
rozšíření	rozšíření	k1gNnSc1
chráněného	chráněný	k2eAgNnSc2d1
území	území	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
návrh	návrh	k1gInSc1
zatím	zatím	k6eAd1
nebyl	být	k5eNaImAgInS
přijat	přijmout	k5eAaPmNgInS
z	z	k7c2
důvodu	důvod	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
jeden	jeden	k4xCgMnSc1
z	z	k7c2
majitelů	majitel	k1gMnPc2
pozemku	pozemek	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yIgNnSc6,k3yRgNnSc6,k3yQgNnSc6
by	by	kYmCp3nS
se	se	k3xPyFc4
nacházela	nacházet	k5eAaImAgFnS
tato	tento	k3xDgFnSc1
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
se	se	k3xPyFc4
odvolal	odvolat	k5eAaPmAgMnS
<g/>
,	,	kIx,
protože	protože	k8xS
s	s	k7c7
tímto	tento	k3xDgInSc7
návrhem	návrh	k1gInSc7
nesouhlasil	souhlasit	k5eNaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nyní	nyní	k6eAd1
ministerstvo	ministerstvo	k1gNnSc1
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
bude	být	k5eAaImBp3nS
prozkoumávat	prozkoumávat	k5eAaImF
<g/>
,	,	kIx,
jestli	jestli	k8xS
nedošlo	dojít	k5eNaPmAgNnS
k	k	k7c3
pochybení	pochybení	k1gNnSc3
při	při	k7c6
navrhování	navrhování	k1gNnSc6
PP	PP	kA
Andělské	andělský	k2eAgInPc1d1
schody	schod	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
prokáže	prokázat	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
k	k	k7c3
žádnému	žádný	k3yNgNnSc3
pochybení	pochybení	k1gNnSc3
nedošlo	dojít	k5eNaPmAgNnS
<g/>
,	,	kIx,
nebude	být	k5eNaImBp3nS
nic	nic	k6eAd1
bránit	bránit	k5eAaImF
k	k	k7c3
vyhlášení	vyhlášení	k1gNnSc3
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
Andělské	andělský	k2eAgInPc4d1
schody	schod	k1gInPc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Dne	den	k1gInSc2
30	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2013	#num#	k4
byla	být	k5eAaImAgFnS
vyhlášena	vyhlásit	k5eAaPmNgFnS
rozsáhlá	rozsáhlý	k2eAgFnSc1d1
(	(	kIx(
<g/>
173,24	173,24	k4
ha	ha	kA
<g/>
)	)	kIx)
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
Andělské	andělský	k2eAgInPc1d1
schody	schod	k1gInPc1
<g/>
,	,	kIx,
avšak	avšak	k8xC
stávající	stávající	k2eAgFnSc1d1
rezervace	rezervace	k1gFnSc1
byla	být	k5eAaImAgFnS
zachována	zachovat	k5eAaPmNgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přírodní	přírodní	k2eAgInPc1d1
poměry	poměr	k1gInPc1
</s>
<s>
Geologie	geologie	k1gFnSc1
</s>
<s>
Andělské	andělský	k2eAgInPc1d1
schody	schod	k1gInPc1
jsou	být	k5eAaImIp3nP
tvořeny	tvořit	k5eAaImNgInP
proterozoickými	proterozoický	k2eAgFnPc7d1
horninami	hornina	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvíce	hodně	k6eAd3,k6eAd1
jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
tmavé	tmavý	k2eAgFnPc1d1
břidlice	břidlice	k1gFnPc1
<g/>
,	,	kIx,
droby	droba	k1gFnPc1
a	a	k8xC
prachovce	prachovka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ojediněle	ojediněle	k6eAd1
se	se	k3xPyFc4
zde	zde	k6eAd1
nachází	nacházet	k5eAaImIp3nS
vložky	vložka	k1gFnPc4
spilitů	spilit	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pedologie	pedologie	k1gFnSc1
</s>
<s>
Na	na	k7c6
tomto	tento	k3xDgInSc6
území	území	k1gNnSc6
je	být	k5eAaImIp3nS
unikátní	unikátní	k2eAgInSc1d1
postglaciální	postglaciální	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlivem	vlivem	k7c2
působení	působení	k1gNnSc2
větru	vítr	k1gInSc2
zde	zde	k6eAd1
nebyl	být	k5eNaImAgInS
umožněn	umožnit	k5eAaPmNgInS
vývoj	vývoj	k1gInSc1
půd	půda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mocnost	mocnost	k1gFnSc1
tohoto	tento	k3xDgInSc2
profilu	profil	k1gInSc2
je	být	k5eAaImIp3nS
pouhých	pouhý	k2eAgInPc2d1
5	#num#	k4
až	až	k9
10	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlubší	hluboký	k2eAgFnPc4d2
půdy	půda	k1gFnPc4
vznikaly	vznikat	k5eAaImAgFnP
ve	v	k7c6
sníženinách	sníženina	k1gFnPc6
na	na	k7c6
deluviofluviálních	deluviofluviální	k2eAgInPc6d1
sedimentech	sediment	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
hlubší	hluboký	k2eAgFnPc1d2
půdy	půda	k1gFnPc1
mají	mít	k5eAaImIp3nP
charakter	charakter	k1gInSc4
jílovitý	jílovitý	k2eAgInSc4d1
<g/>
,	,	kIx,
pro	pro	k7c4
ně	on	k3xPp3gMnPc4
typický	typický	k2eAgInSc1d1
vysýchavý	vysýchavý	k2eAgInSc1d1
vodní	vodní	k2eAgInSc1d1
režim	režim	k1gInSc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c6
jaře	jaro	k1gNnSc6
jsou	být	k5eAaImIp3nP
silně	silně	k6eAd1
podmáčené	podmáčený	k2eAgFnPc1d1
a	a	k8xC
během	během	k7c2
léta	léto	k1gNnSc2
vysychají	vysychat	k5eAaImIp3nP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pohled	pohled	k1gInSc1
na	na	k7c4
louku	louka	k1gFnSc4
v	v	k7c6
PR	pr	k0
Andelske	Andelske	k1gNnPc2
schody	schod	k1gInPc1
</s>
<s>
Flora	Flora	k1gFnSc1
</s>
<s>
Lesní	lesní	k2eAgFnSc1d1
vegetace	vegetace	k1gFnSc1
je	být	k5eAaImIp3nS
zastoupena	zastoupit	k5eAaPmNgFnS
bazifilní	bazifilní	k2eAgFnSc7d1
doubravou	doubrava	k1gFnSc7
a	a	k8xC
na	na	k7c6
mělkých	mělký	k2eAgFnPc6d1
půdách	půda	k1gFnPc6
rostou	růst	k5eAaImIp3nP
acidofilní	acidofilní	k2eAgFnPc1d1
doubravy	doubrava	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
většině	většina	k1gFnSc6
plochy	plocha	k1gFnSc2
rostou	růst	k5eAaImIp3nP
hercynské	hercynský	k2eAgFnPc1d1
dubohabřiny	dubohabřina	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyskytuje	vyskytovat	k5eAaImIp3nS
se	se	k3xPyFc4
zde	zde	k6eAd1
i	i	k9
jedle	jedle	k6eAd1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
zapříčiněno	zapříčinit	k5eAaPmNgNnS
srážkovým	srážkový	k2eAgInSc7d1
stínem	stín	k1gInSc7
a	a	k8xC
lesní	lesní	k2eAgFnSc7d1
pastvou	pastva	k1gFnSc7
v	v	k7c6
minulosti	minulost	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pohled	pohled	k1gInSc1
na	na	k7c4
louku	louka	k1gFnSc4
v	v	k7c6
PR	pr	k0
Andelske	Andelske	k1gInSc4
schody	schod	k1gInPc1
</s>
<s>
Luční	luční	k2eAgFnSc4d1
vegetaci	vegetace	k1gFnSc4
tvoří	tvořit	k5eAaImIp3nS
bohatá	bohatý	k2eAgFnSc1d1
škála	škála	k1gFnSc1
mokřadních	mokřadní	k2eAgNnPc2d1
<g/>
,	,	kIx,
vysýchavých	vysýchavý	k2eAgNnPc2d1
a	a	k8xC
suchomilných	suchomilný	k2eAgNnPc2d1
společenstev	společenstvo	k1gNnPc2
<g/>
,	,	kIx,
především	především	k9
bezkolencových	bezkolencův	k2eAgFnPc2d1
luk	louka	k1gFnPc2
svazu	svaz	k1gInSc2
Molinion	Molinion	k1gInSc4
s	s	k7c7
přechody	přechod	k1gInPc7
do	do	k7c2
různých	různý	k2eAgNnPc2d1
jiných	jiný	k2eAgNnPc2d1
lučních	luční	k2eAgNnPc2d1
společenstev	společenstvo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Velmi	velmi	k6eAd1
cenná	cenný	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
suchomilnější	suchomilný	k2eAgFnSc1d2
vegetace	vegetace	k1gFnSc1
<g/>
,	,	kIx,
řazená	řazený	k2eAgFnSc1d1
k	k	k7c3
ovsíkovým	ovsíkův	k2eAgFnPc3d1
loukám	louka	k1gFnPc3
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
silně	silně	k6eAd1
ohrožený	ohrožený	k2eAgInSc1d1
vstavač	vstavač	k1gInSc1
kukačka	kukačka	k1gFnSc1
(	(	kIx(
<g/>
Orchis	Orchis	k1gFnSc1
morio	morio	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
potočních	potoční	k2eAgFnPc6d1
nivách	niva	k1gFnPc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
ostřice	ostřice	k1gFnSc1
Hartmanova	Hartmanův	k2eAgFnSc1d1
(	(	kIx(
<g/>
Carex	Carex	k1gInSc1
hartmanii	hartmanie	k1gFnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dále	daleko	k6eAd2
se	se	k3xPyFc4
tu	tu	k6eAd1
vyskytuje	vyskytovat	k5eAaImIp3nS
hadí	hadí	k2eAgInSc1d1
mord	mord	k1gInSc1
nízký	nízký	k2eAgInSc1d1
(	(	kIx(
<g/>
Scorzonera	Scorzoner	k1gMnSc2
humilis	humilis	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Luční	luční	k2eAgInSc1d1
biotop	biotop	k1gInSc1
reprezentují	reprezentovat	k5eAaImIp3nP
hořec	hořec	k1gInSc4
hořepník	hořepník	k1gInSc1
(	(	kIx(
<g/>
Gentiana	Gentiana	k1gFnSc1
pneumonanthe	pneumonanth	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kosatec	kosatec	k1gInSc1
sibiřský	sibiřský	k2eAgInSc1d1
(	(	kIx(
<g/>
Iris	iris	k1gInSc1
sibirica	sibiric	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
upolín	upolín	k1gInSc1
nejvyšší	vysoký	k2eAgInSc1d3
(	(	kIx(
<g/>
Trollius	Trollius	k1gInSc1
altissimus	altissimus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
hvozdík	hvozdík	k1gInSc4
lesní	lesní	k2eAgInSc4d1
(	(	kIx(
<g/>
Dianthus	Dianthus	k1gInSc1
sylvaticus	sylvaticus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
srpice	srpice	k1gFnSc1
barvířská	barvířský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Serratula	Serratula	k1gFnSc1
tinctoria	tinctorium	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nejvlhčích	vlhký	k2eAgNnPc6d3
místech	místo	k1gNnPc6
roste	růst	k5eAaImIp3nS
prstnatec	prstnatec	k1gMnSc1
májový	májový	k2eAgMnSc1d1
(	(	kIx(
<g/>
Dactylorhiza	Dactylorhiza	k1gFnSc1
majalis	majalis	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
suchopýr	suchopýr	k1gInSc1
úzkolistý	úzkolistý	k2eAgInSc1d1
(	(	kIx(
<g/>
Eriophorum	Eriophorum	k1gInSc1
angustifolium	angustifolium	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podzimní	podzimní	k2eAgInSc1d1
druh	druh	k1gInSc1
ocún	ocún	k1gInSc1
jesenní	jesenní	k2eAgInSc1d1
(	(	kIx(
<g/>
Colchicum	Colchicum	k1gInSc1
autumnale	autumnale	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pro	pro	k7c4
mochnové	mochnový	k2eAgFnPc4d1
louky	louka	k1gFnPc4
a	a	k8xC
doubravy	doubrava	k1gFnPc4
je	být	k5eAaImIp3nS
typická	typický	k2eAgFnSc1d1
mochna	mochna	k1gFnSc1
bílá	bílý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Potentilla	Potentilla	k1gFnSc1
alba	album	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Fauna	fauna	k1gFnSc1
</s>
<s>
Jak	jak	k6eAd1
bylo	být	k5eAaImAgNnS
zmíněno	zmínit	k5eAaPmNgNnS
v	v	k7c6
úvodu	úvod	k1gInSc6
<g/>
,	,	kIx,
žije	žít	k5eAaImIp3nS
tady	tady	k6eAd1
mnoho	mnoho	k4c1
druhů	druh	k1gInPc2
bezobratlých	bezobratlý	k2eAgMnPc2d1
živočichů	živočich	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
měkkýšů	měkkýš	k1gMnPc2
se	se	k3xPyFc4
zde	zde	k6eAd1
velmi	velmi	k6eAd1
hojně	hojně	k6eAd1
vyskytuje	vyskytovat	k5eAaImIp3nS
vrkoč	vrkoč	k1gInSc1
mnohozubý	mnohozubý	k2eAgInSc1d1
(	(	kIx(
<g/>
Vertigo	Vertigo	k6eAd1
antivertigo	antivertigo	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
dále	daleko	k6eAd2
vrkoč	vrkoč	k1gInSc1
útlý	útlý	k2eAgInSc1d1
(	(	kIx(
<g/>
Vertigo	Vertigo	k1gMnSc1
angustior	angustior	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
patří	patřit	k5eAaImIp3nS
do	do	k7c2
NATURA	NATURA	kA
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posledním	poslední	k2eAgMnSc7d1
zástupcem	zástupce	k1gMnSc7
tohoto	tento	k3xDgInSc2
druhu	druh	k1gInSc2
je	být	k5eAaImIp3nS
vrkoč	vrkoč	k1gInSc1
rýhovaný	rýhovaný	k2eAgInSc1d1
(	(	kIx(
<g/>
Vertigo	Vertigo	k6eAd1
substriata	substriat	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Hmyz	hmyz	k1gInSc1
v	v	k7c6
této	tento	k3xDgFnSc6
lokalitě	lokalita	k1gFnSc6
reprezentuje	reprezentovat	k5eAaImIp3nS
modrásek	modrásek	k1gMnSc1
očkovaný	očkovaný	k2eAgMnSc1d1
(	(	kIx(
<g/>
Maculinea	Maculinea	k1gMnSc1
telleius	telleius	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
velkou	velký	k2eAgFnSc7d1
měrou	míra	k1gFnSc7wR
podílel	podílet	k5eAaImAgMnS
na	na	k7c4
vyhlášení	vyhlášení	k1gNnSc4
tohoto	tento	k3xDgNnSc2
území	území	k1gNnSc2
za	za	k7c4
přírodní	přírodní	k2eAgFnSc4d1
rezervaci	rezervace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
s	s	k7c7
modráskem	modrásek	k1gMnSc7
bahenním	bahenní	k2eAgFnPc3d1
(	(	kIx(
<g/>
Maculinea	Maculinea	k1gFnSc1
nausithous	nausithous	k1gMnSc1
<g/>
)	)	kIx)
patří	patřit	k5eAaImIp3nS
do	do	k7c2
NATURA	NATURA	kA
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgMnSc7d1
zástupcem	zástupce	k1gMnSc7
hmyzu	hmyz	k1gInSc2
je	být	k5eAaImIp3nS
zlatokřídlec	zlatokřídlec	k1gInSc4
vzácný	vzácný	k2eAgInSc4d1
(	(	kIx(
<g/>
Jodia	Jodia	k1gFnSc1
croceago	croceago	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Andělské	andělský	k2eAgInPc1d1
schody	schod	k1gInPc1
jsou	být	k5eAaImIp3nP
jedinou	jediný	k2eAgFnSc7d1
lokalitou	lokalita	k1gFnSc7
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
tento	tento	k3xDgInSc1
druh	druh	k1gInSc1
vyskytuje	vyskytovat	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Mezi	mezi	k7c4
zajímavé	zajímavý	k2eAgMnPc4d1
brouky	brouk	k1gMnPc4
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
zde	zde	k6eAd1
můžem	můžem	k?
najít	najít	k5eAaPmF
patří	patřit	k5eAaImIp3nS
chráněný	chráněný	k2eAgMnSc1d1
svižník	svižník	k1gMnSc1
polní	polní	k2eAgMnSc1d1
(	(	kIx(
<g/>
Cicindela	Cicindela	k1gFnSc1
campestris	campestris	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
také	také	k9
chráněný	chráněný	k2eAgInSc1d1
zlatohlávek	zlatohlávek	k1gInSc1
skvrnitý	skvrnitý	k2eAgInSc1d1
(	(	kIx(
<g/>
Oxythyrea	Oxythyre	k2eAgFnSc1d1
funesta	funesta	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
přírodní	přírodní	k2eAgFnSc6d1
rezervaci	rezervace	k1gFnSc6
se	se	k3xPyFc4
také	také	k9
nachází	nacházet	k5eAaImIp3nS
velký	velký	k2eAgInSc1d1
počet	počet	k1gInSc1
obratlovců	obratlovec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
plazů	plaz	k1gMnPc2
a	a	k8xC
obojživelníků	obojživelník	k1gMnPc2
se	se	k3xPyFc4
zde	zde	k6eAd1
nachází	nacházet	k5eAaImIp3nS
několik	několik	k4yIc1
velmi	velmi	k6eAd1
zajímavých	zajímavý	k2eAgMnPc2d1
zástupců	zástupce	k1gMnPc2
<g/>
.	.	kIx.
<g/>
Z	z	k7c2
obojživelníků	obojživelník	k1gMnPc2
můžeme	moct	k5eAaImIp1nP
narazit	narazit	k5eAaPmF
na	na	k7c4
skokana	skokan	k1gMnSc4
štíhlého	štíhlý	k2eAgInSc2d1
(	(	kIx(
<g/>
Rana	Rana	k1gFnSc1
dalmatina	dalmatin	k1gMnSc2
<g/>
)	)	kIx)
a	a	k8xC
pak	pak	k6eAd1
také	také	k9
na	na	k7c4
skokana	skokan	k1gMnSc4
hnědého	hnědý	k2eAgInSc2d1
(	(	kIx(
<g/>
Rana	Rana	k1gFnSc1
temporaria	temporarium	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
běžných	běžný	k2eAgInPc2d1
druhů	druh	k1gInPc2
zde	zde	k6eAd1
můžeme	moct	k5eAaImIp1nP
vidět	vidět	k5eAaImF
ropuchu	ropucha	k1gFnSc4
obecnou	obecný	k2eAgFnSc4d1
(	(	kIx(
<g/>
Bufo	bufa	k1gFnSc5
bufo	bufa	k1gFnSc5
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plazi	plaz	k1gMnPc1
hojně	hojně	k6eAd1
zastupuje	zastupovat	k5eAaImIp3nS
slepýš	slepýš	k1gMnSc1
křehký	křehký	k2eAgMnSc1d1
(	(	kIx(
<g/>
Anguis	Anguis	k1gInSc1
fragilis	fragilis	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
užovka	užovka	k1gFnSc1
obojková	obojkový	k2eAgFnSc1d1
(	(	kIx(
<g/>
Natrix	Natrix	k1gInSc1
natrix	natrix	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
ještěrka	ještěrka	k1gFnSc1
obecná	obecná	k1gFnSc1
(	(	kIx(
<g/>
Lacerta	Lacerta	k1gFnSc1
agilis	agilis	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Našlo	najít	k5eAaPmAgNnS
se	s	k7c7
zde	zde	k6eAd1
i	i	k8xC
pár	pár	k4xCyI
jedinců	jedinec	k1gMnPc2
čolka	čolek	k1gMnSc2
horského	horský	k2eAgMnSc2d1
(	(	kIx(
<g/>
Mesotriton	Mesotriton	k1gInSc1
alpestris	alpestris	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
V	v	k7c6
minulých	minulý	k2eAgNnPc6d1
letech	léto	k1gNnPc6
tu	tu	k6eAd1
byl	být	k5eAaImAgInS
ještě	ještě	k6eAd1
zaznamenán	zaznamenat	k5eAaPmNgInS
výskyt	výskyt	k1gInSc1
čolka	čolek	k1gMnSc2
obecného	obecný	k2eAgMnSc2d1
(	(	kIx(
<g/>
Lissotriton	Lissotriton	k1gInSc1
vulgaris	vulgaris	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
zajímavé	zajímavý	k2eAgMnPc4d1
savce	savec	k1gMnPc4
lze	lze	k6eAd1
zařadit	zařadit	k5eAaPmF
výskyt	výskyt	k1gInSc4
dvou	dva	k4xCgInPc2
nepůvodních	původní	k2eNgInPc2d1
druhů	druh	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
nutrie	nutrie	k1gFnPc4
říční	říční	k2eAgFnPc4d1
(	(	kIx(
<g/>
Myocastor	Myocastor	k1gMnSc1
coypus	coypus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
chovali	chovat	k5eAaImAgMnP
na	na	k7c6
levostranném	levostranný	k2eAgInSc6d1
přítoku	přítok	k1gInSc6
Velkého	velký	k2eAgInSc2d1
voznického	voznický	k2eAgInSc2d1
rybníka	rybník	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
průběhu	průběh	k1gInSc6
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
až	až	k9
do	do	k7c2
počátku	počátek	k1gInSc2
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
nacházela	nacházet	k5eAaImAgFnS
nutrijí	nutrít	k5eAaPmIp3nP
farma	farma	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
ukončení	ukončení	k1gNnSc6
chovu	chov	k1gInSc2
se	se	k3xPyFc4
několik	několik	k4yIc1
jedinců	jedinec	k1gMnPc2
vyskytovalo	vyskytovat	k5eAaImAgNnS
volně	volně	k6eAd1
v	v	k7c6
okolí	okolí	k1gNnSc6
rybníka	rybník	k1gInSc2
Velký	velký	k2eAgMnSc1d1
a	a	k8xC
tedy	tedy	k9
i	i	k9
v	v	k7c6
okolí	okolí	k1gNnSc6
rezervace	rezervace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhým	druhý	k4xOgInSc7
nepůvodním	původní	k2eNgInSc7d1
druhem	druh	k1gInSc7
je	být	k5eAaImIp3nS
jelenec	jelenec	k1gInSc1
viržinský	viržinský	k2eAgInSc1d1
(	(	kIx(
<g/>
Odocoileus	Odocoileus	k1gInSc1
virginianus	virginianus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1890	#num#	k4
vysazen	vysadit	k5eAaPmNgInS
do	do	k7c2
obory	obora	k1gFnSc2
na	na	k7c6
Dobříši	Dobříš	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
zrušení	zrušení	k1gNnSc6
obory	obora	k1gFnSc2
se	se	k3xPyFc4
jelenec	jelenec	k1gInSc1
viržinský	viržinský	k2eAgInSc1d1
(	(	kIx(
<g/>
Odocoileus	Odocoileus	k1gInSc1
virginianus	virginianus	k1gInSc1
<g/>
)	)	kIx)
vypustil	vypustit	k5eAaPmAgInS
do	do	k7c2
volné	volný	k2eAgFnSc2d1
přírody	příroda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nyní	nyní	k6eAd1
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
v	v	k7c6
lesích	les	k1gInPc6
Středočeského	středočeský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ochrana	ochrana	k1gFnSc1
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1
cílem	cíl	k1gInSc7
v	v	k7c6
lesních	lesní	k2eAgInPc6d1
biotopech	biotop	k1gInPc6
je	být	k5eAaImIp3nS
dosáhnout	dosáhnout	k5eAaPmF
přírodě	příroda	k1gFnSc3
blízká	blízký	k2eAgNnPc4d1
lesní	lesní	k2eAgNnPc4d1
společenstva	společenstvo	k1gNnPc4
s	s	k7c7
dominancí	dominance	k1gFnSc7
dubu	dub	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
tedy	tedy	k9
nutné	nutný	k2eAgNnSc1d1
omezit	omezit	k5eAaPmF
nárosty	nárost	k1gInPc4
habru	habr	k1gInSc2
a	a	k8xC
lípy	lípa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
v	v	k7c6
rámci	rámec	k1gInSc6
možnosti	možnost	k1gFnSc2
podporovat	podporovat	k5eAaImF
šíření	šíření	k1gNnSc4
jedle	jedle	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sečení	sečení	k1gNnSc1
hraje	hrát	k5eAaImIp3nS
důležitou	důležitý	k2eAgFnSc4d1
roli	role	k1gFnSc4
při	při	k7c6
zachování	zachování	k1gNnSc6
diverzity	diverzita	k1gFnSc2
na	na	k7c6
lučních	luční	k2eAgInPc6d1
biotopech	biotop	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Frekvence	frekvence	k1gFnSc1
tohoto	tento	k3xDgNnSc2
sečení	sečení	k1gNnSc2
by	by	kYmCp3nS
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
alespoň	alespoň	k9
2	#num#	k4
<g/>
x	x	k?
za	za	k7c4
rok	rok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc2
kosení	kosení	k1gNnPc2
by	by	kYmCp3nP
se	se	k3xPyFc4
mělo	mít	k5eAaImAgNnS
provádět	provádět	k5eAaImF
koncem	koncem	k7c2
června	červen	k1gInSc2
až	až	k6eAd1
začátkem	začátkem	k7c2
července	červenec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhý	druhý	k4xOgInSc1
termín	termín	k1gInSc1
kosení	kosení	k1gNnSc2
by	by	kYmCp3nS
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
v	v	k7c6
polovině	polovina	k1gFnSc6
září	září	k1gNnSc2
<g/>
.	.	kIx.
<g/>
V	v	k7c6
minulosti	minulost	k1gFnSc6
obě	dva	k4xCgFnPc1
louky	louka	k1gFnPc1
byly	být	k5eAaImAgFnP
pouze	pouze	k6eAd1
ručně	ručně	k6eAd1
koseny	kosen	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pastva	pastva	k1gFnSc1
zde	zde	k6eAd1
neprobíhala	probíhat	k5eNaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Louky	louka	k1gFnPc1
před	před	k7c7
senosečí	senoseč	k1gFnSc7
se	se	k3xPyFc4
rozkolíkovaly	rozkolíkovat	k5eAaPmAgInP,k5eAaImAgInP,k5eAaBmAgInP
a	a	k8xC
díly	díl	k1gInPc4
připadly	připadnout	k5eAaPmAgInP
ke	k	k7c3
kosení	kosení	k1gNnSc3
jednotlivým	jednotlivý	k2eAgFnPc3d1
rodinám	rodina	k1gFnPc3
z	z	k7c2
vesnice	vesnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
způsob	způsob	k1gInSc1
obhospodařování	obhospodařování	k1gNnSc2
zde	zde	k6eAd1
probíhal	probíhat	k5eAaImAgInS
až	až	k9
do	do	k7c2
poloviny	polovina	k1gFnSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c4
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
se	se	k3xPyFc4
o	o	k7c4
toto	tento	k3xDgNnSc4
sekání	sekání	k1gNnSc4
staraly	starat	k5eAaImAgInP
Lesy	les	k1gInPc1
ČR	ČR	kA
pomocí	pomocí	k7c2
sekačky	sekačka	k1gFnSc2
tažené	tažený	k2eAgNnSc1d1
lehčím	lehký	k2eAgInSc7d2
traktorem	traktor	k1gInSc7
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
bylo	být	k5eAaImAgNnS
dobré	dobrý	k2eAgNnSc1d1
i	i	k9
pro	pro	k7c4
nepatrnou	nepatrný	k2eAgFnSc4d1,k2eNgFnSc4d1
disturbanci	disturbance	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgFnSc1d1
3	#num#	k4
roky	rok	k1gInPc4
se	se	k3xPyFc4
o	o	k7c4
louky	louka	k1gFnPc4
staral	starat	k5eAaImAgMnS
pan	pan	k1gMnSc1
T.	T.	kA
Bíba	Bíba	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
většinu	většina	k1gFnSc4
plochy	plocha	k1gFnSc2
sekal	sekat	k5eAaImAgMnS
ručně	ručně	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Blatouch	blatouch	k1gInSc1
bahenní	bahenní	k2eAgInSc1d1
(	(	kIx(
<g/>
Caltha	Caltha	k1gFnSc1
palustris	palustris	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Upolín	upolín	k1gInSc1
evropský	evropský	k2eAgInSc1d1
(	(	kIx(
<g/>
Trollius	Trollius	k1gInSc1
europaeus	europaeus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Prstnatec	Prstnatec	k1gMnSc1
májový	májový	k2eAgMnSc1d1
(	(	kIx(
<g/>
Dactylorhiza	Dactylorhiza	k1gFnSc1
majalis	majalis	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Hadí	hadí	k2eAgInSc1d1
mord	mord	k1gInSc1
nízký	nízký	k2eAgInSc1d1
(	(	kIx(
<g/>
Scorzonera	Scorzoner	k1gMnSc2
humilis	humilis	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Zběhovec	zběhovec	k1gInSc1
plazivý	plazivý	k2eAgInSc1d1
(	(	kIx(
<g/>
Ajuga	Ajuga	k1gFnSc1
reptans	reptansa	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Kuklík	Kuklík	k1gInSc1
potoční	potoční	k2eAgInSc1d1
(	(	kIx(
<g/>
Geum	Geum	k1gInSc1
rivale	rival	k1gMnSc5
<g/>
)	)	kIx)
</s>
<s>
Maloočka	Maloočka	k1gFnSc1
smaragdová	smaragdový	k2eAgFnSc1d1
(	(	kIx(
<g/>
Micrommata	Micromma	k1gNnPc1
virescens	virescensa	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
PR	pr	k0
Andělské	andělský	k2eAgInPc1d1
schody	schod	k1gInPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biolib	Biolib	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Otevřená	otevřený	k2eAgFnSc1d1
data	datum	k1gNnSc2
AOPK	AOPK	kA
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
LOŽEK	LOŽEK	kA
V.	V.	kA
<g/>
,	,	kIx,
KUBÍKOVÁ	Kubíková	k1gFnSc1
J.	J.	kA
<g/>
,	,	kIx,
ŠPRYŇAR	ŠPRYŇAR	kA
P.	P.	kA
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
Andělské	andělský	k2eAgInPc4d1
schody	schod	k1gInPc4
–	–	k?
Chráněná	chráněný	k2eAgNnPc4d1
území	území	k1gNnSc4
<g />
.	.	kIx.
</s>
<s hack="1">
ČR	ČR	kA
<g/>
,	,	kIx,
svazek	svazek	k1gInSc1
XIII	XIII	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
499.1	499.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
PR	pr	k0
Andělské	andělský	k2eAgInPc1d1
schody	schod	k1gInPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
kr-stredocesky	kr-stredocesky	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
PR	pr	k0
Andělské	andělský	k2eAgInPc1d1
schody	schod	k1gInPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
http://drusop.nature.cz/ost/chrobjekty/zchru/index.php?frame&	http://drusop.nature.cz/ost/chrobjekty/zchru/index.php?frame&	k?
<g/>
↑	↑	k?
http://www.obecvoznice.cz/navrh-na-vyhlaseni-maloplosneho-zvlaste-chraneneho-uzemi-prirodni-pamatky-andelske-schody/d-12671	http://www.obecvoznice.cz/navrh-na-vyhlaseni-maloplosneho-zvlaste-chraneneho-uzemi-prirodni-pamatky-andelske-schody/d-12671	k4
2	#num#	k4
Andělské	andělský	k2eAgInPc1d1
schody	schod	k1gInPc1
–	–	k?
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rozhlas	rozhlas	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
LOŽEK	LOŽEK	kA
V.	V.	kA
<g/>
,	,	kIx,
KUBÍKOVÁ	Kubíková	k1gFnSc1
J.	J.	kA
<g/>
,	,	kIx,
ŠPRYŇAR	ŠPRYŇAR	kA
P.	P.	kA
A	a	k9
KOL	kol	k6eAd1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
Andělské	andělský	k2eAgInPc4d1
schody	schod	k1gInPc4
–	–	k?
Chráněná	chráněný	k2eAgNnPc4d1
území	území	k1gNnPc4
ČR	ČR	kA
<g/>
,	,	kIx,
svazek	svazek	k1gInSc1
XIII	XIII	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
498	#num#	k4
↑	↑	k?
LOŽEK	LOŽEK	kA
V.	V.	kA
<g/>
,	,	kIx,
KUBÍKOVÁ	Kubíková	k1gFnSc1
J.	J.	kA
<g/>
,	,	kIx,
ŠPRYŇAR	ŠPRYŇAR	kA
P.	P.	kA
A	a	k9
KOL	kol	k6eAd1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
Andělské	andělský	k2eAgInPc4d1
schody	schod	k1gInPc4
–	–	k?
Chráněná	chráněný	k2eAgNnPc4d1
území	území	k1gNnPc4
ČR	ČR	kA
<g/>
,	,	kIx,
svazek	svazek	k1gInSc1
XIII	XIII	kA
<g/>
.	.	kIx.
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
498	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Andělské	andělský	k2eAgInPc1d1
schody	schod	k1gInPc1
–	–	k?
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Salvia-os	Salvia-os	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
LOŽEK	LOŽEK	kA
V.	V.	kA
<g/>
,	,	kIx,
KUBÍKOVÁ	Kubíková	k1gFnSc1
J.	J.	kA
<g/>
,	,	kIx,
ŠPRYŇAR	ŠPRYŇAR	kA
P.	P.	kA
a	a	k8xC
kol	kolo	k1gNnPc2
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
Andělské	andělský	k2eAgInPc4d1
schody	schod	k1gInPc4
–	–	k?
Chráněná	chráněný	k2eAgNnPc4d1
území	území	k1gNnPc4
ČR	ČR	kA
<g/>
,	,	kIx,
svazek	svazek	k1gInSc1
XIII	XIII	kA
<g/>
.	.	kIx.
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
499	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
jelenec-virzinsky	jelenec-virzinsky	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
lsv	lsv	k?
<g/>
.	.	kIx.
<g/>
mypage	mypage	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
LOŽEK	LOŽEK	kA
V.	V.	kA
<g/>
,	,	kIx,
KUBÍKOVÁ	Kubíková	k1gFnSc1
J.	J.	kA
<g/>
,	,	kIx,
ŠPRYŇAR	ŠPRYŇAR	kA
P.	P.	kA
A	a	k9
KOL	kol	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Střední	střední	k2eAgFnPc4d1
Čechy	Čechy	k1gFnPc4
v	v	k7c6
<g/>
:	:	kIx,
Mackovčin	Mackovčin	k2eAgMnSc1d1
P.	P.	kA
a	a	k8xC
Sedláček	Sedláček	k1gMnSc1
M.	M.	kA
(	(	kIx(
<g/>
eds	eds	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Chráněná	chráněný	k2eAgNnPc4d1
území	území	k1gNnPc4
ČR	ČR	kA
<g/>
,	,	kIx,
svazek	svazek	k1gInSc1
XIII	XIII	kA
<g/>
..	..	k?
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Agentura	agentura	k1gFnSc1
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
a	a	k8xC
krajiny	krajina	k1gFnSc2
ČR	ČR	kA
a	a	k8xC
EkoCentrum	EkoCentrum	k1gNnSc1
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
904	#num#	k4
s.	s.	k?
Kapitola	kapitola	k1gFnSc1
Andělské	andělský	k2eAgFnPc1d1
schody	schod	k1gInPc4
<g/>
,	,	kIx,
s.	s.	k?
498	#num#	k4
<g/>
,	,	kIx,
499	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ČURNOVÁ	ČURNOVÁ	kA
<g/>
,	,	kIx,
Alexandra	Alexandra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plán	plán	k1gInSc1
péče	péče	k1gFnSc2
pro	pro	k7c4
období	období	k1gNnSc4
2009	#num#	k4
<g/>
–	–	k?
<g/>
2018	#num#	k4
přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
Andělské	andělský	k2eAgFnPc1d1
schody	schod	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
:	:	kIx,
EIA	EIA	kA
servis	servis	k1gInSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Andělské	andělský	k2eAgInPc1d1
schody	schod	k1gInPc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Informace	informace	k1gFnPc1
na	na	k7c4
Biolib	Biolib	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Článek	článek	k1gInSc1
na	na	k7c4
Rozhlas	rozhlas	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Příbram	Příbram	k1gFnSc4
Přírodní	přírodní	k2eAgInPc1d1
parky	park	k1gInPc1
</s>
<s>
Hřebeny	hřeben	k1gInPc1
•	•	k?
Petrovicko	Petrovicko	k1gNnSc4
•	•	k?
Třemšín	Třemšín	k1gInSc1
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Brdy	Brdy	k1gInPc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Drbákov	Drbákov	k1gInSc4
–	–	k?
Albertovy	Albertův	k2eAgFnSc2d1
skály	skála	k1gFnSc2
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Andělské	andělský	k2eAgInPc4d1
schody	schod	k1gInPc4
•	•	k?
Getsemanka	Getsemanka	k1gFnSc1
•	•	k?
Hradec	Hradec	k1gInSc1
•	•	k?
Jezero	jezero	k1gNnSc1
•	•	k?
Kuchyňka	Kuchyňka	k1gMnSc1
•	•	k?
Na	na	k7c6
skalách	skála	k1gFnPc6
•	•	k?
Vymyšlenská	Vymyšlenský	k2eAgFnSc1d1
pěšina	pěšina	k1gFnSc1
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Andělské	andělský	k2eAgInPc1d1
schody	schod	k1gInPc1
•	•	k?
Bezděkovský	Bezděkovský	k2eAgInSc1d1
lom	lom	k1gInSc1
•	•	k?
Bohostice	Bohostika	k1gFnSc6
•	•	k?
Březnice	Březnice	k1gFnSc2
–	–	k?
Oblouček	oblouček	k1gInSc1
•	•	k?
Dobříšský	dobříšský	k2eAgInSc1d1
park	park	k1gInSc1
•	•	k?
Dobříšský	dobříšský	k2eAgInSc1d1
zámek	zámek	k1gInSc1
•	•	k?
Horní	horní	k2eAgInSc1d1
a	a	k8xC
Dolní	dolní	k2eAgInSc1d1
obděnický	obděnický	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Horní	horní	k2eAgInSc1d1
solopyský	solopyský	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Hřebenec	Hřebenec	k1gInSc1
•	•	k?
Husova	Husův	k2eAgFnSc1d1
kazatelna	kazatelna	k1gFnSc1
•	•	k?
Jablonná	Jablonná	k1gFnSc1
–	–	k?
mokřad	mokřad	k1gInSc1
•	•	k?
Jezera	jezero	k1gNnSc2
•	•	k?
Jezírko	jezírko	k1gNnSc1
u	u	k7c2
Dobříše	Dobříš	k1gFnSc2
•	•	k?
Kosova	Kosův	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
•	•	k?
Louky	louka	k1gFnSc2
u	u	k7c2
Drahlína	Drahlín	k1gInSc2
•	•	k?
Na	na	k7c6
horách	hora	k1gFnPc6
•	•	k?
Pařezitý	Pařezitý	k2eAgInSc1d1
•	•	k?
Rožmitál	Rožmitál	k1gInSc1
pod	pod	k7c7
Třemšínem	Třemšín	k1gMnSc7
•	•	k?
Starý	starý	k2eAgMnSc1d1
u	u	k7c2
Líchov	Líchovo	k1gNnPc2
•	•	k?
Rybník	rybník	k1gInSc1
Vočert	Vočert	k1gMnSc1
a	a	k8xC
Lazy	Lazy	k?
•	•	k?
Štola	štola	k1gFnSc1
Jarnice	Jarnice	k1gFnSc2
•	•	k?
Třemešný	Třemešný	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Vápenické	vápenický	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
•	•	k?
Velký	velký	k2eAgInSc1d1
Raputovský	Raputovský	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Vinice	vinice	k1gFnSc2
•	•	k?
Vrškámen	Vrškámen	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Příroda	příroda	k1gFnSc1
</s>
