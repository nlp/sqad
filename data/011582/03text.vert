<p>
<s>
Briliant	briliant	k1gInSc1	briliant
je	být	k5eAaImIp3nS	být
diamant	diamant	k1gInSc4	diamant
či	či	k8xC	či
jiný	jiný	k2eAgInSc4d1	jiný
drahokam	drahokam	k1gInSc4	drahokam
<g/>
,	,	kIx,	,
vybroušený	vybroušený	k2eAgInSc4d1	vybroušený
do	do	k7c2	do
zvláštní	zvláštní	k2eAgFnSc2d1	zvláštní
formy	forma	k1gFnSc2	forma
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
ploškami	ploška	k1gFnPc7	ploška
tak	tak	k9	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vynikl	vyniknout	k5eAaPmAgInS	vyniknout
jeho	jeho	k3xOp3gInSc4	jeho
lesk	lesk	k1gInSc4	lesk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Brilantový	brilantový	k2eAgInSc1d1	brilantový
(	(	kIx(	(
<g/>
od	od	k7c2	od
francouzštiny	francouzština	k1gFnSc2	francouzština
Brilliant	Brilliant	k1gInSc1	Brilliant
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
zářící	zářící	k2eAgMnSc1d1	zářící
<g/>
,	,	kIx,	,
jasný	jasný	k2eAgMnSc1d1	jasný
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
diamant	diamant	k1gInSc1	diamant
s	s	k7c7	s
puncem	punc	k1gInSc7	punc
výjimečnosti	výjimečnost	k1gFnSc2	výjimečnost
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
dva	dva	k4xCgInPc1	dva
pojmy	pojem	k1gInPc1	pojem
"	"	kIx"	"
<g/>
diamant	diamant	k1gInSc1	diamant
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
briliantový	briliantový	k2eAgMnSc1d1	briliantový
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
často	často	k6eAd1	často
mylně	mylně	k6eAd1	mylně
zaměňují	zaměňovat	k5eAaImIp3nP	zaměňovat
<g/>
:	:	kIx,	:
</s>
<s>
Diamant	diamant	k1gInSc1	diamant
je	být	k5eAaImIp3nS	být
nerost	nerost	k1gInSc4	nerost
<g/>
,	,	kIx,	,
materiál	materiál	k1gInSc4	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
pravý	pravý	k2eAgInSc1d1	pravý
briliant	briliant	k1gInSc1	briliant
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
označen	označit	k5eAaPmNgInS	označit
pouze	pouze	k6eAd1	pouze
kulatý	kulatý	k2eAgInSc1d1	kulatý
diamant	diamant	k1gInSc1	diamant
(	(	kIx(	(
<g/>
round	round	k1gInSc1	round
brilliant	brilliant	k1gInSc1	brilliant
<g/>
)	)	kIx)	)
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
brusem	brus	k1gInSc7	brus
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
kameny	kámen	k1gInPc1	kámen
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
brusem	brus	k1gInSc7	brus
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
nanejvýše	nanejvýše	k6eAd1	nanejvýše
nést	nést	k5eAaImF	nést
přívlastkové	přívlastkový	k2eAgNnSc4d1	přívlastkové
označení	označení	k1gNnSc4	označení
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
zirkon	zirkon	k1gInSc1	zirkon
s	s	k7c7	s
brilantovým	brilantový	k2eAgInSc7d1	brilantový
brusem	brus	k1gInSc7	brus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Briliantový	briliantový	k2eAgInSc1d1	briliantový
brus	brus	k1gInSc1	brus
==	==	k?	==
</s>
</p>
<p>
<s>
Brilantový	brilantový	k2eAgInSc1d1	brilantový
brus	brus	k1gInSc1	brus
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
asi	asi	k9	asi
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
celkem	celkem	k6eAd1	celkem
56	[number]	k4	56
(	(	kIx(	(
<g/>
57	[number]	k4	57
<g/>
)	)	kIx)	)
faset	faseta	k1gFnPc2	faseta
(	(	kIx(	(
<g/>
plošek	ploška	k1gFnPc2	ploška
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
autorem	autor	k1gMnSc7	autor
výpočtu	výpočet	k1gInSc2	výpočet
moderního	moderní	k2eAgInSc2d1	moderní
briliantového	briliantový	k2eAgInSc2d1	briliantový
brusu	brus	k1gInSc2	brus
je	být	k5eAaImIp3nS	být
Marcel	Marcel	k1gMnSc1	Marcel
Tolkowski	Tolkowsk	k1gFnSc2	Tolkowsk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
dokázal	dokázat	k5eAaPmAgInS	dokázat
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
spočítat	spočítat	k5eAaPmF	spočítat
ideální	ideální	k2eAgInSc4d1	ideální
sklon	sklon	k1gInSc4	sklon
úhlů	úhel	k1gInPc2	úhel
faset	faseta	k1gFnPc2	faseta
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
světlo	světlo	k1gNnSc1	světlo
procházející	procházející	k2eAgNnSc1d1	procházející
diamantem	diamant	k1gInSc7	diamant
odráželo	odrážet	k5eAaImAgNnS	odrážet
co	co	k9	co
nejlepším	dobrý	k2eAgInSc7d3	nejlepší
způsobem	způsob	k1gInSc7	způsob
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
barevném	barevný	k2eAgNnSc6d1	barevné
spektru	spektrum	k1gNnSc6	spektrum
a	a	k8xC	a
maximální	maximální	k2eAgFnSc6d1	maximální
intenzitě	intenzita	k1gFnSc6	intenzita
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dobře	dobře	k6eAd1	dobře
vybroušených	vybroušený	k2eAgInPc6d1	vybroušený
briliantech	briliant	k1gInPc6	briliant
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
obdivován	obdivován	k2eAgMnSc1d1	obdivován
jejich	jejich	k3xOp3gNnSc4	jejich
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
černý	černý	k2eAgInSc1d1	černý
plamen	plamen	k1gInSc1	plamen
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
optický	optický	k2eAgInSc4d1	optický
jev	jev	k1gInSc4	jev
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
vnitřku	vnitřek	k1gInSc2	vnitřek
vrcholu	vrchol	k1gInSc2	vrchol
<g/>
,	,	kIx,	,
kolmo	kolmo	k6eAd1	kolmo
skrz	skrz	k7c4	skrz
základovou	základový	k2eAgFnSc4d1	základová
plochu	plocha	k1gFnSc4	plocha
<g/>
:	:	kIx,	:
Uvnitř	uvnitř	k6eAd1	uvnitř
totiž	totiž	k9	totiž
vrchol	vrchol	k1gInSc4	vrchol
zafunguje	zafungovat	k5eAaPmIp3nS	zafungovat
obráceně	obráceně	k6eAd1	obráceně
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
kout	kout	k5eAaImF	kout
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
jako	jako	k9	jako
reflektor	reflektor	k1gInSc1	reflektor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ideální	ideální	k2eAgInSc1d1	ideální
brus	brus	k1gInSc1	brus
má	mít	k5eAaImIp3nS	mít
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
32	[number]	k4	32
horních	horní	k2eAgFnPc2d1	horní
faset	faseta	k1gFnPc2	faseta
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
horní	horní	k2eAgFnSc4d1	horní
plošku	ploška	k1gFnSc4	ploška
(	(	kIx(	(
<g/>
tou	ten	k3xDgFnSc7	ten
se	se	k3xPyFc4	se
při	při	k7c6	při
broušení	broušení	k1gNnSc6	broušení
začíná	začínat	k5eAaImIp3nS	začínat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
spodní	spodní	k2eAgFnSc1d1	spodní
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
vybroušena	vybrousit	k5eAaPmNgFnS	vybrousit
do	do	k7c2	do
24	[number]	k4	24
faset	faseta	k1gFnPc2	faseta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
ploška	ploška	k1gFnSc1	ploška
navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
tolerovaná	tolerovaný	k2eAgFnSc1d1	tolerovaná
<g/>
:	:	kIx,	:
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
hrot	hrot	k1gInSc4	hrot
jehlanu	jehlan	k1gInSc2	jehlan
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
ideálně	ideálně	k6eAd1	ideálně
sbíhat	sbíhat	k5eAaImF	sbíhat
hned	hned	k6eAd1	hned
hned	hned	k6eAd1	hned
několik	několik	k4yIc4	několik
bočních	boční	k2eAgFnPc2d1	boční
plošek	ploška	k1gFnPc2	ploška
do	do	k7c2	do
jediného	jediný	k2eAgInSc2d1	jediný
hrotu	hrot	k1gInSc2	hrot
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
takřka	takřka	k6eAd1	takřka
nemožný	možný	k2eNgInSc4d1	nemožný
úkol	úkol	k1gInSc4	úkol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Náhrady	náhrada	k1gFnPc4	náhrada
briliantů	briliant	k1gInPc2	briliant
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
obchodech	obchod	k1gInPc6	obchod
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
setkáte	setkat	k5eAaPmIp2nP	setkat
s	s	k7c7	s
kubickými	kubický	k2eAgInPc7d1	kubický
zirkony	zirkon	k1gInPc7	zirkon
(	(	kIx(	(
<g/>
označeni	označen	k2eAgMnPc1d1	označen
CZ-cubic	CZubic	k1gMnSc1	CZ-cubic
zirkonia	zirkonium	k1gNnSc2	zirkonium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
dobrou	dobrý	k2eAgFnSc4d1	dobrá
napodobeninu	napodobenina	k1gFnSc4	napodobenina
pro	pro	k7c4	pro
šperkařské	šperkařský	k2eAgInPc4d1	šperkařský
účely	účel	k1gInPc4	účel
<g/>
:	:	kIx,	:
za	za	k7c4	za
velmi	velmi	k6eAd1	velmi
přijatelnou	přijatelný	k2eAgFnSc4d1	přijatelná
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
občas	občas	k6eAd1	občas
vidět	vidět	k5eAaImF	vidět
tzv.	tzv.	kA	tzv.
syntetické	syntetický	k2eAgInPc1d1	syntetický
diamanty	diamant	k1gInPc1	diamant
–	–	k?	–
"	"	kIx"	"
<g/>
moissanity	moissanita	k1gFnSc2	moissanita
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
těžko	těžko	k6eAd1	těžko
rozpoznatelné	rozpoznatelný	k2eAgNnSc1d1	rozpoznatelné
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
jejich	jejich	k3xOp3gFnSc1	jejich
cena	cena	k1gFnSc1	cena
je	být	k5eAaImIp3nS	být
podstatně	podstatně	k6eAd1	podstatně
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
pravých	pravý	k2eAgInPc2d1	pravý
kamenů	kámen	k1gInPc2	kámen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Diamant	diamant	k1gInSc4	diamant
<g/>
,	,	kIx,	,
mýty	mýtus	k1gInPc4	mýtus
a	a	k8xC	a
fakta	faktum	k1gNnPc4	faktum
<g/>
.	.	kIx.	.
<g/>
;	;	kIx,	;
drahokam	drahokam	k1gInSc1	drahokam
<g/>
.	.	kIx.	.
<g/>
wordpress	wordpress	k1gInSc1	wordpress
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
;	;	kIx,	;
navštíveno	navštívit	k5eAaPmNgNnS	navštívit
2018	[number]	k4	2018
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
