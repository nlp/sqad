<s>
Country	country	k2eAgMnSc1d1
Music	Music	k1gMnSc1
Association	Association	k1gInSc4
</s>
<s>
Country	country	k2eAgMnSc1d1
Music	Music	k1gMnSc1
Association	Association	k1gInSc4
Zkratka	zkratka	k1gFnSc1
</s>
<s>
CMA	CMA	kA
Vznik	vznik	k1gInSc1
</s>
<s>
1958	#num#	k4
Typ	typ	k1gInSc1
</s>
<s>
Hudební	hudební	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Nashville	Nashville	k1gFnSc1
<g/>
,	,	kIx,
Tennessee	Tennessee	k1gFnSc1
<g/>
,	,	kIx,
USA	USA	kA
Předseda	předseda	k1gMnSc1
</s>
<s>
Gary	Gara	k1gMnSc2
Overton	Overton	k1gInSc1
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.cmaworld.com	www.cmaworld.com	k1gInSc1
Některá	některý	k3yIgNnPc4
data	datum	k1gNnPc4
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hudební	hudební	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
Country	country	k1gInPc1
Music	Musice	k1gFnPc1
Association	Association	k1gFnPc1
(	(	kIx(
<g/>
CMA	CMA	kA
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1958	#num#	k4
ve	v	k7c6
městě	město	k1gNnSc6
Nashville	Nashville	k1gMnSc1
ve	v	k7c6
státě	stát	k1gInSc6
Tennessee	Tennesse	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
se	se	k3xPyFc4
skládala	skládat	k5eAaImAgFnS
pouze	pouze	k6eAd1
z	z	k7c2
233	#num#	k4
členů	člen	k1gMnPc2
a	a	k8xC
byla	být	k5eAaImAgFnS
první	první	k4xOgFnSc7
obchodní	obchodní	k2eAgFnSc7d1
organizací	organizace	k1gFnSc7
vytvořenou	vytvořený	k2eAgFnSc4d1
za	za	k7c7
účelem	účel	k1gInSc7
propagace	propagace	k1gFnSc1
hudebního	hudební	k2eAgInSc2d1
žánru	žánr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cíle	cíl	k1gInSc2
organizace	organizace	k1gFnSc2
jsou	být	k5eAaImIp3nP
pomáhat	pomáhat	k5eAaImF
a	a	k8xC
zlepšovat	zlepšovat	k5eAaImF
rozvoj	rozvoj	k1gInSc4
country	country	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
;	;	kIx,
prezentovat	prezentovat	k5eAaBmF
ji	on	k3xPp3gFnSc4
jako	jako	k9
životaschopné	životaschopný	k2eAgNnSc1d1
medium	medium	k1gNnSc1
jak	jak	k8xS,k8xC
pro	pro	k7c4
inzerenty	inzerent	k1gMnPc4
<g/>
,	,	kIx,
tak	tak	k9
pro	pro	k7c4
spotřebitele	spotřebitel	k1gMnPc4
i	i	k8xC
média	médium	k1gNnPc1
<g/>
;	;	kIx,
a	a	k8xC
snažit	snažit	k5eAaImF
se	se	k3xPyFc4
sjednotit	sjednotit	k5eAaPmF
cíle	cíl	k1gInPc4
celého	celý	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
okolo	okolo	k7c2
country	country	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
fanouškům	fanoušek	k1gMnPc3
country	country	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
CMA	CMA	kA
nejvíce	nejvíce	k6eAd1,k6eAd3
známa	znám	k2eAgFnSc1d1
díky	díky	k7c3
jejím	její	k3xOp3gNnSc7
každoročním	každoroční	k2eAgNnSc7d1
živým	živý	k2eAgNnSc7d1
televizním	televizní	k2eAgNnSc7d1
vysíláním	vysílání	k1gNnSc7
Country	country	k2eAgInPc2d1
Music	Musice	k1gInPc2
Association	Association	k1gInSc1
Awards	Awards	k1gInSc4
(	(	kIx(
<g/>
obvykle	obvykle	k6eAd1
v	v	k7c6
říjnu	říjen	k1gInSc6
nebo	nebo	k8xC
listopadu	listopad	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Původní	původní	k2eAgNnSc1d1
složení	složení	k1gNnSc1
představenstva	představenstvo	k1gNnSc2
CMA	CMA	kA
sestávalo	sestávat	k5eAaImAgNnS
z	z	k7c2
devíti	devět	k4xCc2
ředitelů	ředitel	k1gMnPc2
a	a	k8xC
pěti	pět	k4xCc2
referentů	referent	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgMnSc7
předsedou	předseda	k1gMnSc7
představenstva	představenstvo	k1gNnSc2
CMA	CMA	kA
byl	být	k5eAaImAgMnS
Wesley	Wesle	k2eAgMnPc4d1
Rose	Ros	k1gMnPc4
<g/>
,	,	kIx,
prezident	prezident	k1gMnSc1
Acuff-Rose	Acuff-Rosa	k1gFnSc3
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
Inc	Inc	k1gMnSc1
<g/>
..	..	k?
Mediální	mediální	k2eAgMnSc1d1
magnát	magnát	k1gMnSc1
a	a	k8xC
vedoucí	vedoucí	k2eAgMnSc1d1
pracovník	pracovník	k1gMnSc1
Connie	Connie	k1gFnSc2
B.	B.	kA
Gay	gay	k1gMnSc1
pak	pak	k6eAd1
stal	stát	k5eAaPmAgMnS
zakládajícím	zakládající	k2eAgMnSc7d1
prezidentem	prezident	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Původní	původní	k2eAgInSc1d1
počet	počet	k1gInSc1
členských	členský	k2eAgFnPc2d1
kategorií	kategorie	k1gFnPc2
byl	být	k5eAaImAgInS
devět	devět	k4xCc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
je	být	k5eAaImIp3nS
těchto	tento	k3xDgInPc6
kategorií	kategorie	k1gFnSc7
15	#num#	k4
a	a	k8xC
reprezentují	reprezentovat	k5eAaImIp3nP
všechny	všechen	k3xTgInPc4
aspekty	aspekt	k1gInPc4
hudebního	hudební	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
také	také	k9
členství	členství	k1gNnSc4
pro	pro	k7c4
organizace	organizace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Členové	člen	k1gMnPc1
CMA	CMA	kA
jsou	být	k5eAaImIp3nP
osoby	osoba	k1gFnPc1
a	a	k8xC
organizace	organizace	k1gFnPc1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
jsou	být	k5eAaImIp3nP
přímo	přímo	k6eAd1
zapojeny	zapojit	k5eAaPmNgFnP
do	do	k7c2
průmyslu	průmysl	k1gInSc2
okolo	okolo	k7c2
country	country	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnSc1
CMA	CMA	kA
Awards	Awards	k1gInSc1
ceremoniál	ceremoniál	k1gInSc1
se	se	k3xPyFc4
uskutečnil	uskutečnit	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1967	#num#	k4
v	v	k7c6
Nashvillu	Nashvill	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moderátory	moderátor	k1gInPc1
večera	večer	k1gInSc2
byli	být	k5eAaImAgMnP
Sonny	Sonna	k1gFnPc4
James	Jamesa	k1gFnPc2
a	a	k8xC
Bobbie	Bobbie	k1gFnSc2
Gentry	Gentr	k1gInPc1
a	a	k8xC
událost	událost	k1gFnSc1
nebyla	být	k5eNaImAgFnS
vysílána	vysílat	k5eAaImNgFnS
v	v	k7c6
televizi	televize	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítězem	vítěz	k1gMnSc7
v	v	k7c6
kategorii	kategorie	k1gFnSc6
"	"	kIx"
<g/>
Entertainer	Entertainer	k1gInSc1
of	of	k?
the	the	k?
Year	Year	k1gInSc1
<g/>
"	"	kIx"
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
zpěvák	zpěvák	k1gMnSc1
Eddy	Edda	k1gFnSc2
Arnold	Arnold	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cenu	cena	k1gFnSc4
pro	pro	k7c4
"	"	kIx"
<g/>
Male	male	k6eAd1
Vocalist	Vocalist	k1gInSc1
of	of	k?
the	the	k?
Year	Year	k1gMnSc1
<g/>
"	"	kIx"
obdržel	obdržet	k5eAaPmAgMnS
Jack	Jack	k1gMnSc1
Greene	Green	k1gInSc5
a	a	k8xC
"	"	kIx"
<g/>
Female	Femala	k1gFnSc3
Vocalist	Vocalist	k1gInSc1
of	of	k?
the	the	k?
Year	Year	k1gInSc1
<g/>
"	"	kIx"
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
Loretta	Loretta	k1gFnSc1
Lynnová	Lynnová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1968	#num#	k4
byli	být	k5eAaImAgMnP
moderátory	moderátor	k1gMnPc7
ceremoniálu	ceremoniál	k1gInSc2
Roy	Roy	k1gMnSc1
Rogers	Rogersa	k1gFnPc2
a	a	k8xC
Dale	Dale	k1gFnPc2
Evans	Evansa	k1gFnPc2
a	a	k8xC
událost	událost	k1gFnSc1
se	se	k3xPyFc4
konala	konat	k5eAaImAgFnS
v	v	k7c4
Ryman	Ryman	k1gInSc4
Auditorium	auditorium	k1gNnSc4
v	v	k7c6
Nashvillu	Nashvill	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
pár	pár	k4xCyI
týdnů	týden	k1gInPc2
později	pozdě	k6eAd2
pak	pak	k6eAd1
byl	být	k5eAaImAgInS
celý	celý	k2eAgInSc1d1
ceremoniál	ceremoniál	k1gInSc1
vysílán	vysílán	k2eAgInSc1d1
na	na	k7c4
stanici	stanice	k1gFnSc4
NBC	NBC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgNnSc1
živé	živý	k2eAgNnSc1d1
vysílání	vysílání	k1gNnSc1
proběhlo	proběhnout	k5eAaPmAgNnS
již	již	k6eAd1
v	v	k7c6
následujícím	následující	k2eAgInSc6d1
roce	rok	k1gInSc6
–	–	k?
1969	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Každoroční	každoroční	k2eAgNnSc4d1
ocenění	ocenění	k1gNnSc4
jsou	být	k5eAaImIp3nP
udílena	udílen	k2eAgFnSc1d1
ve	v	k7c6
dvanácti	dvanáct	k4xCc6
následujících	následující	k2eAgFnPc6d1
kategoriích	kategorie	k1gFnPc6
<g/>
:	:	kIx,
Entertainer	Entertainra	k1gFnPc2
of	of	k?
the	the	k?
Year	Year	k1gInSc1
<g/>
,	,	kIx,
Male	male	k6eAd1
Vocalist	Vocalist	k1gInSc4
<g/>
,	,	kIx,
Female	Femala	k1gFnSc3
Vocalist	Vocalist	k1gMnSc1
<g/>
,	,	kIx,
New	New	k1gMnSc1
Artist	Artist	k1gMnSc1
of	of	k?
the	the	k?
Year	Year	k1gInSc1
(	(	kIx(
<g/>
původně	původně	k6eAd1
Horizon	Horizon	k1gMnSc1
Award	Award	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Vocal	Vocal	k1gMnSc1
Group	Group	k1gMnSc1
<g/>
,	,	kIx,
Vocal	Vocal	k1gMnSc1
Duo	duo	k1gNnSc1
<g/>
,	,	kIx,
Single	singl	k1gInSc5
<g/>
,	,	kIx,
Album	album	k1gNnSc1
<g/>
,	,	kIx,
Song	song	k1gInSc1
<g/>
,	,	kIx,
Music	Music	k1gMnSc1
Event	Event	k1gMnSc1
<g/>
,	,	kIx,
Music	Music	k1gMnSc1
Video	video	k1gNnSc1
and	and	k?
Musician	Musiciany	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
CMA	CMA	kA
uděluje	udělovat	k5eAaImIp3nS
každoročně	každoročně	k6eAd1
také	také	k9
"	"	kIx"
<g/>
CMA	CMA	kA
Broadcast	Broadcast	k1gMnSc1
Award	Award	k1gMnSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
určena	určit	k5eAaPmNgFnS
pro	pro	k7c4
country	country	k2eAgFnPc4d1
zaměřené	zaměřený	k2eAgFnPc4d1
rozhlasové	rozhlasový	k2eAgFnPc4d1
stanice	stanice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Broadcast	Broadcast	k1gFnSc1
Awards	Awardsa	k1gFnPc2
jsou	být	k5eAaImIp3nP
rozděleny	rozdělit	k5eAaPmNgInP
podle	podle	k7c2
velikosti	velikost	k1gFnSc2
tržního	tržní	k2eAgInSc2d1
podílu	podíl	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žádná	žádný	k3yNgFnSc1
stanice	stanice	k1gFnPc4
nemůže	moct	k5eNaImIp3nS
získat	získat	k5eAaPmF
ocenění	ocenění	k1gNnSc4
dvakrát	dvakrát	k6eAd1
po	po	k7c6
sobě	se	k3xPyFc3
v	v	k7c6
následujících	následující	k2eAgNnPc6d1
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s>
Seznam	seznam	k1gInSc1
vítězů	vítěz	k1gMnPc2
CMA	CMA	kA
Award	Award	k1gInSc4
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Country	country	k2eAgInPc2d1
Music	Musice	k1gInPc2
Association	Association	k1gInSc1
Awards	Awards	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Academy	Academa	k1gFnPc1
of	of	k?
Country	country	k2eAgMnSc1d1
Music	Music	k1gMnSc1
–	–	k?
samostatná	samostatný	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
nezávislá	závislý	k2eNgFnSc1d1
na	na	k7c6
CMA	CMA	kA
</s>
<s>
CMT	CMT	kA
</s>
<s>
Country	country	k2eAgMnSc1d1
Music	Music	k1gMnSc1
Hall	Hall	k1gMnSc1
of	of	k?
Fame	Fame	k1gInSc1
</s>
<s>
Grand	grand	k1gMnSc1
Ole	Ola	k1gFnSc3
Opry	Opra	k1gFnPc4
</s>
<s>
CMA	CMA	kA
Music	Music	k1gMnSc1
Festival	festival	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Country	country	k2eAgMnSc1d1
Music	Music	k1gMnSc1
Association	Association	k1gInSc4
</s>
<s>
CMA	CMA	kA
Awards	Awards	k1gInSc1
</s>
<s>
CMA	CMA	kA
Music	Music	k1gMnSc1
Festival	festival	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2323	#num#	k4
9135	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
83029521	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
132463615	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
83029521	#num#	k4
</s>
