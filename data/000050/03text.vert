<p>
<s>
Metr	metr	k1gInSc1	metr
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc1d1	základní
jednotka	jednotka	k1gFnSc1	jednotka
délky	délka	k1gFnSc2	délka
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
standardní	standardní	k2eAgFnSc1d1	standardní
značka	značka	k1gFnSc1	značka
je	být	k5eAaImIp3nS	být
m	m	kA	m
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
na	na	k7c6	na
konci	konec	k1gInSc6	konec
18.	[number]	k4	18.
století	století	k1gNnSc2	století
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
délka	délka	k1gFnSc1	délka
je	být	k5eAaImIp3nS	být
odvozena	odvodit	k5eAaPmNgFnS	odvodit
od	od	k7c2	od
jedné	jeden	k4xCgFnSc2	jeden
desetimilióntiny	desetimilióntin	k2eAgFnSc2d1	desetimilióntin
délky	délka	k1gFnSc2	délka
poledníku	poledník	k1gInSc2	poledník
od	od	k7c2	od
pólu	pól	k1gInSc2	pól
k	k	k7c3	k
rovníku	rovník	k1gInSc3	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
metru	metro	k1gNnSc6	metro
byla	být	k5eAaImAgFnS	být
současně	současně	k6eAd1	současně
založena	založit	k5eAaPmNgFnS	založit
metrická	metrický	k2eAgFnSc1d1	metrická
soustava	soustava	k1gFnSc1	soustava
fyzikálních	fyzikální	k2eAgFnPc2d1	fyzikální
veličin	veličina	k1gFnPc2	veličina
<g/>
.	.	kIx.	.
</s>
<s>
Nejběžnější	běžný	k2eAgFnPc1d3	nejběžnější
odvozené	odvozený	k2eAgFnPc1d1	odvozená
jednotky	jednotka	k1gFnPc1	jednotka
jsou	být	k5eAaImIp3nP	být
milimetr	milimetr	k1gInSc4	milimetr
(	(	kIx(	(
<g/>
mm	mm	kA	mm
<g/>
,	,	kIx,	,
tisícina	tisícina	k1gFnSc1	tisícina
metru	metr	k1gInSc2	metr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
centimetr	centimetr	k1gInSc1	centimetr
(	(	kIx(	(
<g/>
cm	cm	kA	cm
<g/>
,	,	kIx,	,
setina	setina	k1gFnSc1	setina
metru	metr	k1gInSc2	metr
<g/>
)	)	kIx)	)
a	a	k8xC	a
kilometr	kilometr	k1gInSc4	kilometr
(	(	kIx(	(
<g/>
km	km	kA	km
<g/>
,	,	kIx,	,
1000	[number]	k4	1000
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Definice	definice	k1gFnSc1	definice
metru	metr	k1gInSc2	metr
podle	podle	k7c2	podle
soustavy	soustava	k1gFnSc2	soustava
SI	si	k1gNnSc2	si
<g/>
:	:	kIx,	:
Metr	metr	k1gInSc1	metr
je	být	k5eAaImIp3nS	být
délka	délka	k1gFnSc1	délka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
urazí	urazit	k5eAaPmIp3nS	urazit
světlo	světlo	k1gNnSc1	světlo
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
za	za	k7c4	za
1/299 792 458	[number]	k4	1/299 792 458
s	s	k7c7	s
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Násobky	násobek	k1gInPc1	násobek
a	a	k8xC	a
díly	díl	k1gInPc1	díl
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
násobků	násobek	k1gInPc2	násobek
a	a	k8xC	a
dílů	díl	k1gInPc2	díl
metru	metr	k1gInSc2	metr
se	se	k3xPyFc4	se
častěji	často	k6eAd2	často
používají	používat	k5eAaImIp3nP	používat
následující	následující	k2eAgInPc1d1	následující
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
velikosti	velikost	k1gFnSc2	velikost
od	od	k7c2	od
nejmenších	malý	k2eAgFnPc2d3	nejmenší
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Yoktometr	Yoktometr	k1gInSc1	Yoktometr
</s>
</p>
<p>
<s>
Značka	značka	k1gFnSc1	značka
ym	ym	k?	ym
</s>
</p>
<p>
<s>
Zeptometr	Zeptometr	k1gInSc1	Zeptometr
</s>
</p>
<p>
<s>
Značka	značka	k1gFnSc1	značka
zm	zm	k?	zm
</s>
</p>
<p>
</p>
<p>
<s>
AttometrAttometr	AttometrAttometr	k1gInSc1	AttometrAttometr
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
am	am	k?	am
<g/>
)	)	kIx)	)
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
18	[number]	k4	18
neboli	neboli	k8xC	neboli
jedna	jeden	k4xCgFnSc1	jeden
triliontina	triliontina	k1gFnSc1	triliontina
metru	metr	k1gInSc2	metr
<g/>
,	,	kIx,	,
řádem	řád	k1gInSc7	řád
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
velikosti	velikost	k1gFnPc4	velikost
kvarku	kvark	k1gInSc2	kvark
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
FemtometrFemtometr	FemtometrFemtometr	k1gInSc1	FemtometrFemtometr
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
fm	fm	k?	fm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
15	[number]	k4	15
metru	metro	k1gNnSc6	metro
neboli	neboli	k8xC	neboli
1	[number]	k4	1
biliardtina	biliardtin	k2eAgInSc2d1	biliardtin
metru	metr	k1gInSc2	metr
je	být	k5eAaImIp3nS	být
délková	délkový	k2eAgFnSc1d1	délková
jednotka	jednotka	k1gFnSc1	jednotka
používaná	používaný	k2eAgFnSc1d1	používaná
především	především	k9	především
v	v	k7c6	v
jaderné	jaderný	k2eAgFnSc6d1	jaderná
fyzice	fyzika	k1gFnSc6	fyzika
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
řádem	řád	k1gInSc7	řád
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
rozměrům	rozměr	k1gInPc3	rozměr
atomového	atomový	k2eAgNnSc2d1	atomové
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Jaderní	jaderní	k2eAgMnPc1d1	jaderní
fyzici	fyzik	k1gMnPc1	fyzik
ji	on	k3xPp3gFnSc4	on
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
Enrica	Enricus	k1gMnSc2	Enricus
Fermiho	Fermi	k1gMnSc2	Fermi
neoficiálně	neoficiálně	k6eAd1	neoficiálně
nazývají	nazývat	k5eAaImIp3nP	nazývat
fermi	fer	k1gFnPc7	fer
(	(	kIx(	(
<g/>
aniž	aniž	k8xC	aniž
by	by	kYmCp3nP	by
museli	muset	k5eAaImAgMnP	muset
zavádět	zavádět	k5eAaImF	zavádět
novou	nový	k2eAgFnSc4d1	nová
značku	značka	k1gFnSc4	značka
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
"	"	kIx"	"
<g/>
fm	fm	k?	fm
<g/>
"	"	kIx"	"
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
oběma	dva	k4xCgMnPc7	dva
názvům	název	k1gInPc3	název
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
PikometrPikometr	PikometrPikometr	k1gInSc1	PikometrPikometr
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
pm	pm	k?	pm
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
délková	délkový	k2eAgFnSc1d1	délková
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
12	[number]	k4	12
neboli	neboli	k8xC	neboli
biliontina	biliontina	k1gFnSc1	biliontina
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Například	například	k6eAd1	například
poloměr	poloměr	k1gInSc1	poloměr
atomu	atom	k1gInSc2	atom
hélia	hélium	k1gNnSc2	hélium
je	být	k5eAaImIp3nS	být
31	[number]	k4	31
pm	pm	k?	pm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
NanometrNanometr	NanometrNanometr	k1gInSc1	NanometrNanometr
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
nm	nm	k?	nm
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
délková	délkový	k2eAgFnSc1d1	délková
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
9	[number]	k4	9
neboli	neboli	k8xC	neboli
1	[number]	k4	1
miliardtina	miliardtina	k1gFnSc1	miliardtina
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Například	například	k6eAd1	například
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
atomů	atom	k1gInPc2	atom
v	v	k7c6	v
pevných	pevný	k2eAgFnPc6d1	pevná
látkách	látka	k1gFnPc6	látka
jsou	být	k5eAaImIp3nP	být
řádově	řádově	k6eAd1	řádově
zlomky	zlomek	k1gInPc1	zlomek
(	(	kIx(	(
<g/>
typicky	typicky	k6eAd1	typicky
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
až	až	k8xS	až
pětina	pětina	k1gFnSc1	pětina
<g/>
)	)	kIx)	)
nanometru	nanometr	k1gInSc2	nanometr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
MikrometrMikrometr	MikrometrMikrometr	k1gInSc1	MikrometrMikrometr
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
μ	μ	k?	μ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
délková	délkový	k2eAgFnSc1d1	délková
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
6	[number]	k4	6
neboli	neboli	k8xC	neboli
1	[number]	k4	1
milióntina	milióntina	k1gFnSc1	milióntina
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Například	například	k6eAd1	například
kapka	kapka	k1gFnSc1	kapka
mlhy	mlha	k1gFnSc2	mlha
má	mít	k5eAaImIp3nS	mít
cca	cca	kA	cca
10	[number]	k4	10
μ	μ	k?	μ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
MilimetrMilimetr	MilimetrMilimetr	k1gInSc1	MilimetrMilimetr
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
mm	mm	kA	mm
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
délková	délkový	k2eAgFnSc1d1	délková
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
3	[number]	k4	3
neboli	neboli	k8xC	neboli
1	[number]	k4	1
tisícina	tisícina	k1gFnSc1	tisícina
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Udávání	udávání	k1gNnSc1	udávání
rozměrů	rozměr	k1gInPc2	rozměr
v	v	k7c6	v
milimetrech	milimetr	k1gInPc6	milimetr
je	být	k5eAaImIp3nS	být
běžné	běžný	k2eAgNnSc1d1	běžné
například	například	k6eAd1	například
ve	v	k7c6	v
strojnictví	strojnictví	k1gNnSc6	strojnictví
<g/>
,	,	kIx,	,
stavebnictví	stavebnictví	k1gNnSc6	stavebnictví
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
všech	všecek	k3xTgInPc6	všecek
výrobních	výrobní	k2eAgInPc6d1	výrobní
oborech	obor	k1gInPc6	obor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
milimetrech	milimetr	k1gInPc6	milimetr
za	za	k7c4	za
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
také	také	k9	také
udává	udávat	k5eAaImIp3nS	udávat
množství	množství	k1gNnSc1	množství
srážek	srážka	k1gFnPc2	srážka
v	v	k7c6	v
meteorologii	meteorologie	k1gFnSc6	meteorologie
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
milimetr	milimetr	k1gInSc1	milimetr
srážek	srážka	k1gFnPc2	srážka
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
každý	každý	k3xTgInSc4	každý
metr	metr	k1gInSc4	metr
čtvereční	čtvereční	k2eAgInSc4d1	čtvereční
napršel	napršet	k5eAaPmAgInS	napršet
jeden	jeden	k4xCgInSc1	jeden
litr	litr	k1gInSc1	litr
srážek	srážka	k1gFnPc2	srážka
(	(	kIx(	(
<g/>
neboť	neboť	k8xC	neboť
1	[number]	k4	1
mm	mm	kA	mm
×	×	k?	×
1	[number]	k4	1
m2	m2	k4	m2
=	=	kIx~	=
1	[number]	k4	1
dm3	dm3	k4	dm3
=	=	kIx~	=
1	[number]	k4	1
l	l	kA	l
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
CentimetrCentimetr	CentimetrCentimetr	k1gInSc1	CentimetrCentimetr
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
cm	cm	kA	cm
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
délková	délkový	k2eAgFnSc1d1	délková
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
2	[number]	k4	2
neboli	neboli	k8xC	neboli
1	[number]	k4	1
setina	setina	k1gFnSc1	setina
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Udávání	udávání	k1gNnSc1	udávání
rozměrů	rozměr	k1gInPc2	rozměr
v	v	k7c6	v
centimetrech	centimetr	k1gInPc6	centimetr
bývalo	bývat	k5eAaImAgNnS	bývat
běžné	běžný	k2eAgNnSc1d1	běžné
například	například	k6eAd1	například
ve	v	k7c6	v
stavebnictví	stavebnictví	k1gNnSc6	stavebnictví
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
20.	[number]	k4	20.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
však	však	k9	však
při	při	k7c6	při
vyhotovování	vyhotovování	k1gNnSc6	vyhotovování
stavebních	stavební	k2eAgInPc2d1	stavební
plánů	plán	k1gInPc2	plán
přešlo	přejít	k5eAaPmAgNnS	přejít
na	na	k7c4	na
milimetry	milimetr	k1gInPc4	milimetr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
centimetrech	centimetr	k1gInPc6	centimetr
se	se	k3xPyFc4	se
také	také	k9	také
udávají	udávat	k5eAaImIp3nP	udávat
výšky	výška	k1gFnSc2	výška
hladin	hladina	k1gFnPc2	hladina
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
<g/>
,	,	kIx,	,
rozměry	rozměr	k1gInPc1	rozměr
živočichů	živočich	k1gMnPc2	živočich
vč	vč	k?	vč
<g/>
.	.	kIx.	.
člověka	člověk	k1gMnSc2	člověk
apod.	apod.	kA	apod.
</s>
</p>
<p>
</p>
<p>
<s>
Decimetecimetr	Decimetecimetr	k1gInSc1	Decimetecimetr
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
dm	dm	kA	dm
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
délková	délkový	k2eAgFnSc1d1	délková
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
1	[number]	k4	1
neboli	neboli	k8xC	neboli
1	[number]	k4	1
desetina	desetina	k1gFnSc1	desetina
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
HektometrHektometr	HektometrHektometr	k1gInSc1	HektometrHektometr
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
hm	hm	k?	hm
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
délková	délkový	k2eAgFnSc1d1	délková
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
,	,	kIx,	,
102	[number]	k4	102
neboli	neboli	k8xC	neboli
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
</s>
</p>
<p>
<s>
Hektometr	hektometr	k1gInSc1	hektometr
se	se	k3xPyFc4	se
prakticky	prakticky	k6eAd1	prakticky
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
na	na	k7c6	na
železnici	železnice	k1gFnSc6	železnice
je	být	k5eAaImIp3nS	být
zaveden	zavést	k5eAaPmNgInS	zavést
jako	jako	k8xC	jako
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
dvou	dva	k4xCgInPc2	dva
hektometrovníků	hektometrovník	k1gInPc2	hektometrovník
<g/>
.	.	kIx.	.
</s>
<s>
Hektometrovníky	Hektometrovník	k1gInPc1	Hektometrovník
jsou	být	k5eAaImIp3nP	být
kamenné	kamenný	k2eAgInPc1d1	kamenný
(	(	kIx(	(
<g/>
betonové	betonový	k2eAgInPc1d1	betonový
<g/>
)	)	kIx)	)
patníky	patník	k1gInPc1	patník
rozmístěné	rozmístěný	k2eAgInPc1d1	rozmístěný
kolem	kolem	k7c2	kolem
trati	trať	k1gFnSc2	trať
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
nich	on	k3xPp3gMnPc6	on
vyznačena	vyznačen	k2eAgFnSc1d1	vyznačena
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
poloha	poloha	k1gFnSc1	poloha
bodu	bod	k1gInSc2	bod
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
se	se	k3xPyFc4	se
ale	ale	k9	ale
udává	udávat	k5eAaImIp3nS	udávat
v	v	k7c6	v
kilometrech	kilometr	k1gInPc6	kilometr
s	s	k7c7	s
přesností	přesnost	k1gFnSc7	přesnost
na	na	k7c4	na
tisíciny	tisícina	k1gFnPc4	tisícina
(	(	kIx(	(
<g/>
fakticky	fakticky	k6eAd1	fakticky
tedy	tedy	k9	tedy
na	na	k7c4	na
metry	metr	k1gInPc4	metr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
např.	např.	kA	např.
"	"	kIx"	"
<g/>
silniční	silniční	k2eAgInSc1d1	silniční
přejezd	přejezd	k1gInSc1	přejezd
km	km	kA	km
1,456	[number]	k4	1,456
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
"	"	kIx"	"
<g/>
návěstidlo	návěstidlo	k1gNnSc1	návěstidlo
v	v	k7c6	v
km	km	kA	km
44,500	[number]	k4	44,500
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
KilometrKilometr	KilometrKilometr	k1gInSc1	KilometrKilometr
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
km	km	kA	km
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
délková	délkový	k2eAgFnSc1d1	délková
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
,	,	kIx,	,
103	[number]	k4	103
neboli	neboli	k8xC	neboli
1	[number]	k4	1
tisíc	tisíc	k4xCgInPc2	tisíc
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Udávání	udávání	k1gNnSc1	udávání
vzdáleností	vzdálenost	k1gFnPc2	vzdálenost
v	v	k7c6	v
kilometrech	kilometr	k1gInPc6	kilometr
je	být	k5eAaImIp3nS	být
běžné	běžný	k2eAgNnSc1d1	běžné
například	například	k6eAd1	například
v	v	k7c6	v
dopravě	doprava	k1gFnSc6	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
udávání	udávání	k1gNnSc6	udávání
polohy	poloha	k1gFnSc2	poloha
na	na	k7c6	na
silnicích	silnice	k1gFnPc6	silnice
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
půltých	půltý	k2eAgInPc6d1	půltý
kilometrech	kilometr	k1gInPc6	kilometr
<g/>
,	,	kIx,	,
např.	např.	kA	např.
"	"	kIx"	"
<g/>
nehoda	nehoda	k1gFnSc1	nehoda
na	na	k7c4	na
44.	[number]	k4	44.
a	a	k8xC	a
půltém	půltém	k1gInSc1	půltém
kilometru	kilometr	k1gInSc2	kilometr
dálnice	dálnice	k1gFnSc2	dálnice
D1	D1	k1gFnSc2	D1
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Vyšší	vysoký	k2eAgInPc1d2	vyšší
násobkyVyšší	násobkyVyšší	k2eAgInPc1d1	násobkyVyšší
násobky	násobek	k1gInPc1	násobek
vyjádřené	vyjádřený	k2eAgInPc1d1	vyjádřený
předponami	předpona	k1gFnPc7	předpona
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
megametr	megametr	k1gInSc1	megametr
<g/>
,	,	kIx,	,
gigametr	gigametr	k1gInSc1	gigametr
apod.	apod.	kA	apod.
se	se	k3xPyFc4	se
nepoužívají	používat	k5eNaImIp3nP	používat
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
takové	takový	k3xDgInPc4	takový
větší	veliký	k2eAgInPc4d2	veliký
rozměry	rozměr	k1gInPc4	rozměr
a	a	k8xC	a
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nP	mluvit
o	o	k7c6	o
prostých	prostý	k2eAgInPc6d1	prostý
násobcích	násobek	k1gInPc6	násobek
(	(	kIx(	(
<g/>
tisících	tisící	k4xOgMnPc6	tisící
<g/>
,	,	kIx,	,
milionech	milion	k4xCgInPc6	milion
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
se	se	k3xPyFc4	se
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
jinými	jiný	k2eAgNnPc7d1	jiné
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
astronomickými	astronomický	k2eAgFnPc7d1	astronomická
délkovými	délkový	k2eAgFnPc7d1	délková
jednotkami	jednotka	k1gFnPc7	jednotka
-	-	kIx~	-
parsek	parsek	k1gInSc1	parsek
<g/>
,	,	kIx,	,
světelný	světelný	k2eAgInSc1d1	světelný
rok	rok	k1gInSc1	rok
apod.	apod.	kA	apod.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Kyvadlový	kyvadlový	k2eAgInSc4d1	kyvadlový
metr	metr	k1gInSc4	metr
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1668	[number]	k4	1668
anglický	anglický	k2eAgMnSc1d1	anglický
klerik	klerik	k1gMnSc1	klerik
a	a	k8xC	a
filozof	filozof	k1gMnSc1	filozof
John	John	k1gMnSc1	John
Wilkins	Wilkins	k1gInSc4	Wilkins
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
v	v	k7c6	v
eseji	esej	k1gFnSc6	esej
desítkovou	desítkový	k2eAgFnSc4d1	desítková
soustavu	soustava	k1gFnSc4	soustava
měření	měření	k1gNnSc2	měření
délek	délka	k1gFnPc2	délka
<g/>
,	,	kIx,	,
inspirován	inspirován	k2eAgInSc1d1	inspirován
Christopherem	Christopher	k1gInSc7	Christopher
Wrenem	Wren	k1gInSc7	Wren
k	k	k7c3	k
použití	použití	k1gNnSc3	použití
délky	délka	k1gFnSc2	délka
kyvadla	kyvadlo	k1gNnSc2	kyvadlo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
půlperioda	půlperioda	k1gFnSc1	půlperioda
(	(	kIx(	(
<g/>
kyv	kyv	k1gInSc1	kyv
<g/>
,	,	kIx,	,
půl	půl	k6eAd1	půl
kmitu	kmit	k1gInSc3	kmit
<g/>
)	)	kIx)	)
trvá	trvat	k5eAaImIp3nS	trvat
jednu	jeden	k4xCgFnSc4	jeden
vteřinu	vteřina	k1gFnSc4	vteřina
<g/>
,	,	kIx,	,
známého	známý	k2eAgMnSc2d1	známý
jako	jako	k8xC	jako
sekundové	sekundový	k2eAgNnSc1d1	sekundové
kyvadlo	kyvadlo	k1gNnSc1	kyvadlo
<g/>
.	.	kIx.	.
</s>
<s>
Christiaan	Christiaan	k1gInSc1	Christiaan
Huygens	Huygens	k1gInSc1	Huygens
změřil	změřit	k5eAaPmAgInS	změřit
tuto	tento	k3xDgFnSc4	tento
délku	délka	k1gFnSc4	délka
jako	jako	k9	jako
38	[number]	k4	38
královských	královský	k2eAgInPc2d1	královský
(	(	kIx(	(
<g/>
nizozemských	nizozemský	k2eAgInPc2d1	nizozemský
<g/>
)	)	kIx)	)
palců	palec	k1gInPc2	palec
čili	čili	k8xC	čili
39,26	[number]	k4	39,26
anglických	anglický	k2eAgInPc2d1	anglický
palců	palec	k1gInPc2	palec
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
rovná	rovnat	k5eAaImIp3nS	rovnat
997	[number]	k4	997
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
nikdo	nikdo	k3yNnSc1	nikdo
nereagoval	reagovat	k5eNaBmAgMnS	reagovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1670	[number]	k4	1670
lyonský	lyonský	k2eAgInSc1d1	lyonský
biskup	biskup	k1gInSc1	biskup
Gabriel	Gabriel	k1gMnSc1	Gabriel
Mouton	Mouton	k1gInSc1	Mouton
rovněž	rovněž	k9	rovněž
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
univerzální	univerzální	k2eAgFnSc4d1	univerzální
délkovou	délkový	k2eAgFnSc4d1	délková
jednotku	jednotka	k1gFnSc4	jednotka
opřenou	opřený	k2eAgFnSc4d1	opřená
o	o	k7c4	o
desítkovou	desítkový	k2eAgFnSc4d1	desítková
číselnou	číselný	k2eAgFnSc4d1	číselná
soustavu	soustava	k1gFnSc4	soustava
založenou	založený	k2eAgFnSc4d1	založená
na	na	k7c6	na
zemské	zemský	k2eAgFnSc6d1	zemská
souřadnicové	souřadnicový	k2eAgFnSc6d1	souřadnicová
minutě	minuta	k1gFnSc6	minuta
resp.	resp.	kA	resp.
na	na	k7c6	na
délce	délka	k1gFnSc6	délka
kyvadla	kyvadlo	k1gNnSc2	kyvadlo
s	s	k7c7	s
periodou	perioda	k1gFnSc7	perioda
jedné	jeden	k4xCgFnSc2	jeden
vteřiny	vteřina	k1gFnSc2	vteřina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
A	a	k9	a
roku	rok	k1gInSc2	rok
1675	[number]	k4	1675
italský	italský	k2eAgInSc1d1	italský
vědec	vědec	k1gMnSc1	vědec
Tito	tento	k3xDgMnPc1	tento
Livio	Livio	k6eAd1	Livio
Burattini	Burattin	k2eAgMnPc1d1	Burattin
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
Misura	Misura	k1gFnSc1	Misura
Universale	Universala	k1gFnSc3	Universala
(	(	kIx(	(
<g/>
Univerzální	univerzální	k2eAgFnSc1d1	univerzální
míra	míra	k1gFnSc1	míra
<g/>
)	)	kIx)	)
použil	použít	k5eAaPmAgInS	použít
výraz	výraz	k1gInSc1	výraz
"	"	kIx"	"
<g/>
metro	metro	k1gNnSc1	metro
cattolico	cattolico	k1gNnSc1	cattolico
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
obecná	obecný	k2eAgFnSc1d1	obecná
míra	míra	k1gFnSc1	míra
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odvozené	odvozený	k2eAgInPc1d1	odvozený
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
μ	μ	k?	μ
κ	κ	k?	κ
(	(	kIx(	(
<g/>
métron	métron	k1gMnSc1	métron
katholikón	katholikón	k1gMnSc1	katholikón
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
normové	normový	k2eAgFnSc2d1	normová
délky	délka	k1gFnSc2	délka
odvozené	odvozený	k2eAgInPc4d1	odvozený
z	z	k7c2	z
kyvadla	kyvadlo	k1gNnSc2	kyvadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Poledníkový	poledníkový	k2eAgInSc4d1	poledníkový
metr	metr	k1gInSc4	metr
===	===	k?	===
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
výsledek	výsledek	k1gInSc1	výsledek
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
francouzská	francouzský	k2eAgFnSc1d1	francouzská
Akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
ustavila	ustavit	k5eAaPmAgFnS	ustavit
komisi	komise	k1gFnSc4	komise
pro	pro	k7c4	pro
stanovení	stanovení	k1gNnSc4	stanovení
prosté	prostý	k2eAgFnSc2d1	prostá
stupnice	stupnice	k1gFnSc2	stupnice
všech	všecek	k3xTgFnPc2	všecek
měr	míra	k1gFnPc2	míra
<g/>
.	.	kIx.	.
7.	[number]	k4	7.
října	říjen	k1gInSc2	říjen
1790	[number]	k4	1790
komise	komise	k1gFnSc1	komise
doporučila	doporučit	k5eAaPmAgFnS	doporučit
desítkový	desítkový	k2eAgInSc4d1	desítkový
číselný	číselný	k2eAgInSc4d1	číselný
systém	systém	k1gInSc4	systém
a	a	k8xC	a
19.	[number]	k4	19.
března	březen	k1gInSc2	březen
1791	[number]	k4	1791
doporučila	doporučit	k5eAaPmAgFnS	doporučit
přijetí	přijetí	k1gNnSc4	přijetí
termínu	termín	k1gInSc2	termín
"	"	kIx"	"
<g/>
mè	mè	k?	mè
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
míra	míra	k1gFnSc1	míra
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
základní	základní	k2eAgFnSc2d1	základní
jednotky	jednotka	k1gFnSc2	jednotka
délky	délka	k1gFnSc2	délka
definovanou	definovaný	k2eAgFnSc7d1	definovaná
jako	jako	k8xS	jako
desetimilióntinu	desetimilióntina	k1gFnSc4	desetimilióntina
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
severního	severní	k2eAgInSc2d1	severní
pólu	pól	k1gInSc2	pól
a	a	k8xC	a
rovníku	rovník	k1gInSc2	rovník
po	po	k7c6	po
pařížském	pařížský	k2eAgInSc6d1	pařížský
poledníku	poledník	k1gInSc6	poledník
<g/>
.	.	kIx.	.
</s>
<s>
Obvod	obvod	k1gInSc1	obvod
Země	zem	k1gFnSc2	zem
tedy	tedy	k8xC	tedy
činil	činit	k5eAaImAgInS	činit
přesně	přesně	k6eAd1	přesně
40 000	[number]	k4	40 000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1793	[number]	k4	1793
Národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
Francouzů	Francouz	k1gMnPc2	Francouz
přijalo	přijmout	k5eAaPmAgNnS	přijmout
tento	tento	k3xDgInSc4	tento
návrh	návrh	k1gInSc4	návrh
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
metr	metr	k1gInSc1	metr
začaly	začít	k5eAaPmAgInP	začít
používat	používat	k5eAaImF	používat
i	i	k9	i
jiné	jiný	k2eAgFnPc4d1	jiná
země	zem	k1gFnPc4	zem
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Anglie	Anglie	k1gFnSc1	Anglie
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1797	[number]	k4	1797
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
podobě	podoba	k1gFnSc6	podoba
poledníkové	poledníkový	k2eAgFnSc2d1	Poledníková
definice	definice	k1gFnSc2	definice
se	se	k3xPyFc4	se
metr	metr	k1gInSc1	metr
stal	stát	k5eAaPmAgInS	stát
základem	základ	k1gInSc7	základ
metrické	metrický	k2eAgFnSc2d1	metrická
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Tyčový	tyčový	k2eAgInSc4d1	tyčový
metr	metr	k1gInSc4	metr
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
19.	[number]	k4	19.
století	století	k1gNnSc2	století
ve	v	k7c6	v
světle	světlo	k1gNnSc6	světlo
novodobé	novodobý	k2eAgFnSc2d1	novodobá
přesnosti	přesnost	k1gFnSc2	přesnost
se	se	k3xPyFc4	se
zabývala	zabývat	k5eAaImAgFnS	zabývat
metrickým	metrický	k2eAgInSc7d1	metrický
standardem	standard	k1gInSc7	standard
řada	řada	k1gFnSc1	řada
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
konferencí	konference	k1gFnPc2	konference
<g/>
.	.	kIx.	.
</s>
<s>
Metrická	metrický	k2eAgFnSc1d1	metrická
dohoda	dohoda	k1gFnSc1	dohoda
(	(	kIx(	(
<g/>
Convention	Convention	k1gInSc1	Convention
du	du	k?	du
Mè	Mè	k1gFnSc2	Mè
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1875	[number]	k4	1875
pověřila	pověřit	k5eAaPmAgFnS	pověřit
správou	správa	k1gFnSc7	správa
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
úřad	úřad	k1gInSc1	úřad
vah	váha	k1gFnPc2	váha
a	a	k8xC	a
měr	míra	k1gFnPc2	míra
(	(	kIx(	(
<g/>
BIPM	BIPM	kA	BIPM
<g/>
:	:	kIx,	:
Bureau	Burea	k2eAgFnSc4d1	Burea
International	International	k1gFnSc4	International
des	des	k1gNnSc2	des
Poids	Poidsa	k1gFnPc2	Poidsa
et	et	k?	et
Mesures	Mesures	k1gMnSc1	Mesures
<g/>
)	)	kIx)	)
v	v	k7c6	v
Sè	Sè	k1gFnSc6	Sè
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
nově	nově	k6eAd1	nově
ustavená	ustavený	k2eAgFnSc1d1	ustavená
organizace	organizace	k1gFnSc1	organizace
uchovávala	uchovávat	k5eAaImAgFnS	uchovávat
prvotní	prvotní	k2eAgFnSc4d1	prvotní
metrovou	metrový	k2eAgFnSc4d1	metrová
tyč	tyč	k1gFnSc4	tyč
<g/>
,	,	kIx,	,
poskytovala	poskytovat	k5eAaImAgFnS	poskytovat
národní	národní	k2eAgInPc4d1	národní
prototypy	prototyp	k1gInPc4	prototyp
a	a	k8xC	a
udržovala	udržovat	k5eAaImAgFnS	udržovat
převody	převod	k1gInPc4	převod
s	s	k7c7	s
nemetrickými	metrický	k2eNgFnPc7d1	metrický
normami	norma	k1gFnPc7	norma
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
organizace	organizace	k1gFnSc1	organizace
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
tyčový	tyčový	k2eAgInSc4d1	tyčový
etalon	etalon	k1gInSc4	etalon
roku	rok	k1gInSc2	rok
1889	[number]	k4	1889
na	na	k7c6	na
první	první	k4xOgFnSc6	první
Všeobecné	všeobecný	k2eAgFnSc6d1	všeobecná
konferenci	konference	k1gFnSc6	konference
vah	váha	k1gFnPc2	váha
a	a	k8xC	a
měr	míra	k1gFnPc2	míra
(	(	kIx(	(
<g/>
CGPM	CGPM	kA	CGPM
<g/>
:	:	kIx,	:
Conférence	Conférenec	k1gMnSc2	Conférenec
Générale	Général	k1gMnSc5	Général
des	des	k1gNnSc7	des
Poids	Poids	k1gInSc1	Poids
et	et	k?	et
Mesures	Mesures	k1gInSc1	Mesures
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
ustavila	ustavit	k5eAaPmAgFnS	ustavit
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
prototyp	prototyp	k1gInSc4	prototyp
metru	metr	k1gInSc2	metr
jako	jako	k8xS	jako
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
ryskami	ryska	k1gFnPc7	ryska
normové	normový	k2eAgFnSc2d1	normová
tyče	tyč	k1gFnSc2	tyč
ze	z	k7c2	z
slitiny	slitina	k1gFnSc2	slitina
90	[number]	k4	90
<g/>
%	%	kIx~	%
platiny	platina	k1gFnSc2	platina
a	a	k8xC	a
10	[number]	k4	10
<g/>
%	%	kIx~	%
iridia	iridium	k1gNnPc4	iridium
při	při	k7c6	při
tavném	tavný	k2eAgInSc6d1	tavný
bodu	bod	k1gInSc6	bod
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
první	první	k4xOgInSc1	první
vzor	vzor	k1gInSc1	vzor
byl	být	k5eAaImAgInS	být
o	o	k7c4	o
200	[number]	k4	200
mikrometrů	mikrometr	k1gInPc2	mikrometr
kratší	krátký	k2eAgMnSc1d2	kratší
kvůli	kvůli	k7c3	kvůli
chybě	chyba	k1gFnSc3	chyba
výpočtu	výpočet	k1gInSc2	výpočet
zploštění	zploštění	k1gNnSc2	zploštění
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
tato	tento	k3xDgFnSc1	tento
délka	délka	k1gFnSc1	délka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
normou	norma	k1gFnSc7	norma
<g/>
,	,	kIx,	,
standardem	standard	k1gInSc7	standard
<g/>
.	.	kIx.	.
</s>
<s>
Prvotní	prvotní	k2eAgInSc1d1	prvotní
vzor	vzor	k1gInSc1	vzor
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
v	v	k7c6	v
definovaných	definovaný	k2eAgFnPc6d1	definovaná
podmínkách	podmínka	k1gFnPc6	podmínka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1889	[number]	k4	1889
nadále	nadále	k6eAd1	nadále
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Vlnový	vlnový	k2eAgInSc4d1	vlnový
metr	metr	k1gInSc4	metr
===	===	k?	===
</s>
</p>
<p>
<s>
Pozdější	pozdní	k2eAgFnPc1d2	pozdější
fyzikální	fyzikální	k2eAgFnPc1d1	fyzikální
definice	definice	k1gFnPc1	definice
odstranily	odstranit	k5eAaPmAgFnP	odstranit
závislost	závislost	k1gFnSc4	závislost
na	na	k7c6	na
prototypu	prototyp	k1gInSc6	prototyp
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
délku	délka	k1gFnSc4	délka
metru	metr	k1gInSc2	metr
vyjádřily	vyjádřit	k5eAaPmAgFnP	vyjádřit
pomocí	pomocí	k7c2	pomocí
fyzikálních	fyzikální	k2eAgFnPc2d1	fyzikální
konstant	konstanta	k1gFnPc2	konstanta
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
taková	takový	k3xDgFnSc1	takový
definice	definice	k1gFnPc4	definice
byla	být	k5eAaImAgFnS	být
schválena	schválen	k2eAgFnSc1d1	schválena
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
a	a	k8xC	a
zněla	znět	k5eAaImAgFnS	znět
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Metr	metr	k1gInSc1	metr
je	být	k5eAaImIp3nS	být
délka	délka	k1gFnSc1	délka
<g/>
,	,	kIx,	,
rovnající	rovnající	k2eAgFnSc1d1	rovnající
se	se	k3xPyFc4	se
1 650 763,73	[number]	k4	1 650 763,73
násobku	násobek	k1gInSc2	násobek
vlnové	vlnový	k2eAgFnSc2d1	vlnová
délky	délka	k1gFnSc2	délka
záření	záření	k1gNnSc2	záření
šířícího	šířící	k2eAgNnSc2d1	šířící
se	se	k3xPyFc4	se
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
přísluší	příslušet	k5eAaImIp3nS	příslušet
přechodu	přechod	k1gInSc3	přechod
mezi	mezi	k7c7	mezi
energetickými	energetický	k2eAgFnPc7d1	energetická
hladinami	hladina	k1gFnPc7	hladina
2p10	[number]	k4	2p10
a	a	k8xC	a
5d5	[number]	k4	5d5
atomu	atom	k1gInSc2	atom
kryptonu	krypton	k1gInSc2	krypton
86.	[number]	k4	86.
</s>
</p>
<p>
<s>
Nejnovější	nový	k2eAgFnSc1d3	nejnovější
definice	definice	k1gFnSc1	definice
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
svázala	svázat	k5eAaPmAgFnS	svázat
délku	délka	k1gFnSc4	délka
metru	metr	k1gInSc2	metr
přes	přes	k7c4	přes
rychlost	rychlost	k1gFnSc4	rychlost
světla	světlo	k1gNnSc2	světlo
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
s	s	k7c7	s
velikostí	velikost	k1gFnSc7	velikost
sekundy	sekunda	k1gFnSc2	sekunda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zpřesňováním	zpřesňování	k1gNnSc7	zpřesňování
měření	měření	k1gNnSc2	měření
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
zpřesňuje	zpřesňovat	k5eAaImIp3nS	zpřesňovat
také	také	k9	také
velikost	velikost	k1gFnSc1	velikost
metru	metr	k1gInSc2	metr
(	(	kIx(	(
<g/>
praktická	praktický	k2eAgFnSc1d1	praktická
realizace	realizace	k1gFnSc1	realizace
měřením	měření	k1gNnSc7	měření
je	být	k5eAaImIp3nS	být
však	však	k9	však
méně	málo	k6eAd2	málo
přesná	přesný	k2eAgFnSc1d1	přesná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hodnota	hodnota	k1gFnSc1	hodnota
rychlosti	rychlost	k1gFnSc2	rychlost
světla	světlo	k1gNnSc2	světlo
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
je	být	k5eAaImIp3nS	být
nadále	nadále	k6eAd1	nadále
neměnná	měnný	k2eNgFnSc1d1	neměnná
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
je	být	k5eAaImIp3nS	být
metr	metr	k1gInSc1	metr
definován	definovat	k5eAaBmNgInS	definovat
jako	jako	k8xC	jako
jednotka	jednotka	k1gFnSc1	jednotka
vlastní	vlastní	k2eAgFnSc2d1	vlastní
délky	délka	k1gFnSc2	délka
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
sekunda	sekunda	k1gFnSc1	sekunda
definována	definovat	k5eAaBmNgFnS	definovat
jako	jako	k9	jako
jednotka	jednotka	k1gFnSc1	jednotka
pro	pro	k7c4	pro
vlastní	vlastní	k2eAgInSc4d1	vlastní
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
definice	definice	k1gFnSc2	definice
v	v	k7c6	v
čase	čas	k1gInSc6	čas
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
podrobně	podrobně	k6eAd1	podrobně
též	též	k9	též
v	v	k7c6	v
hesle	heslo	k1gNnSc6	heslo
metrická	metrický	k2eAgFnSc1d1	metrická
soustava	soustava	k1gFnSc1	soustava
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
8.	[number]	k4	8.
května	květen	k1gInSc2	květen
1790	[number]	k4	1790
–	–	k?	–
francouzské	francouzský	k2eAgNnSc1d1	francouzské
národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
délka	délka	k1gFnSc1	délka
nového	nový	k2eAgInSc2d1	nový
metru	metr	k1gInSc2	metr
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
stejná	stejný	k2eAgFnSc1d1	stejná
jako	jako	k8xC	jako
délka	délka	k1gFnSc1	délka
kyvadla	kyvadlo	k1gNnSc2	kyvadlo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
doba	doba	k1gFnSc1	doba
půlkyvu	půlkyv	k1gInSc2	půlkyv
je	být	k5eAaImIp3nS	být
rovna	roven	k2eAgFnSc1d1	rovna
jedné	jeden	k4xCgFnSc6	jeden
sekundě	sekunda	k1gFnSc6	sekunda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
30.	[number]	k4	30.
března	březen	k1gInSc2	březen
1791	[number]	k4	1791
–	–	k?	–
francouzské	francouzský	k2eAgNnSc1d1	francouzské
národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
přijalo	přijmout	k5eAaPmAgNnS	přijmout
návrh	návrh	k1gInSc4	návrh
francouzské	francouzský	k2eAgFnSc2d1	francouzská
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
<g/>
:	:	kIx,	:
metr	metr	k1gInSc1	metr
=	=	kIx~	=
jedna	jeden	k4xCgFnSc1	jeden
desetimilióntina	desetimilióntina	k1gFnSc1	desetimilióntina
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
od	od	k7c2	od
rovníku	rovník	k1gInSc2	rovník
k	k	k7c3	k
severnímu	severní	k2eAgInSc3d1	severní
pólu	pól	k1gInSc3	pól
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1795	[number]	k4	1795
–	–	k?	–
zhotovena	zhotoven	k2eAgFnSc1d1	zhotovena
provizorní	provizorní	k2eAgFnSc1d1	provizorní
metrová	metrový	k2eAgFnSc1d1	metrová
tyč	tyč	k1gFnSc1	tyč
z	z	k7c2	z
mosazi	mosaz	k1gFnSc2	mosaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
10.	[number]	k4	10.
prosince	prosinec	k1gInSc2	prosinec
1799	[number]	k4	1799
–	–	k?	–
francouzské	francouzský	k2eAgNnSc1d1	francouzské
národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
určilo	určit	k5eAaPmAgNnS	určit
<g/>
,	,	kIx,	,
že	že	k8xS	že
metrová	metrový	k2eAgFnSc1d1	metrová
tyč	tyč	k1gFnSc1	tyč
(	(	kIx(	(
<g/>
etalon	etalon	k1gInSc1	etalon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
dne	den	k1gInSc2	den
23.	[number]	k4	23.
června	červen	k1gInSc2	červen
1799	[number]	k4	1799
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
uložena	uložit	k5eAaPmNgFnS	uložit
v	v	k7c6	v
národním	národní	k2eAgInSc6d1	národní
archivu	archiv	k1gInSc6	archiv
jako	jako	k8xS	jako
finální	finální	k2eAgFnSc1d1	finální
podoba	podoba	k1gFnSc1	podoba
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
28.	[number]	k4	28.
září	září	k1gNnSc1	září
1889	[number]	k4	1889
–	–	k?	–
první	první	k4xOgNnPc1	první
jednání	jednání	k1gNnPc1	jednání
o	o	k7c6	o
váhách	váha	k1gFnPc6	váha
a	a	k8xC	a
mírách	míra	k1gFnPc6	míra
(	(	kIx(	(
<g/>
CGPM	CGPM	kA	CGPM
<g/>
)	)	kIx)	)
definovalo	definovat	k5eAaBmAgNnS	definovat
délku	délka	k1gFnSc4	délka
jednoho	jeden	k4xCgInSc2	jeden
metru	metr	k1gInSc2	metr
jako	jako	k9	jako
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
linkami	linka	k1gFnPc7	linka
na	na	k7c6	na
standardizované	standardizovaný	k2eAgFnSc6d1	standardizovaná
tyči	tyč	k1gFnSc6	tyč
ze	z	k7c2	z
slitiny	slitina	k1gFnSc2	slitina
platiny	platina	k1gFnSc2	platina
a	a	k8xC	a
deseti	deset	k4xCc2	deset
procent	procento	k1gNnPc2	procento
iridia	iridium	k1gNnSc2	iridium
<g/>
,	,	kIx,	,
měřeno	měřit	k5eAaImNgNnS	měřit
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
tání	tání	k1gNnSc2	tání
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
6.	[number]	k4	6.
října	říjen	k1gInSc2	říjen
1927	[number]	k4	1927
–	–	k?	–
sedmé	sedmý	k4xOgFnSc2	sedmý
CGPM	CGPM	kA	CGPM
upřesnilo	upřesnit	k5eAaPmAgNnS	upřesnit
definici	definice	k1gFnSc4	definice
metru	metr	k1gInSc2	metr
jako	jako	k8xC	jako
délku	délka	k1gFnSc4	délka
měřenou	měřený	k2eAgFnSc4d1	měřená
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
osami	osa	k1gFnPc7	osa
dvou	dva	k4xCgFnPc2	dva
linek	linka	k1gFnPc2	linka
vyznačených	vyznačený	k2eAgFnPc2d1	vyznačená
na	na	k7c6	na
prototypu	prototyp	k1gInSc6	prototyp
metru	metr	k1gInSc2	metr
ze	z	k7c2	z
slitiny	slitina	k1gFnSc2	slitina
platiny	platina	k1gFnSc2	platina
z	z	k7c2	z
90	[number]	k4	90
%	%	kIx~	%
a	a	k8xC	a
iridia	iridium	k1gNnSc2	iridium
10	[number]	k4	10
%	%	kIx~	%
<g/>
,	,	kIx,	,
za	za	k7c2	za
normálního	normální	k2eAgInSc2d1	normální
atmosférického	atmosférický	k2eAgInSc2d1	atmosférický
tlaku	tlak	k1gInSc2	tlak
a	a	k8xC	a
podepřeném	podepřený	k2eAgInSc6d1	podepřený
dvěma	dva	k4xCgInPc7	dva
válci	válec	k1gInPc7	válec
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
nejméně	málo	k6eAd3	málo
jeden	jeden	k4xCgInSc4	jeden
centimetr	centimetr	k1gInSc4	centimetr
umístěnými	umístěný	k2eAgMnPc7d1	umístěný
symetricky	symetricky	k6eAd1	symetricky
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
výšce	výška	k1gFnSc6	výška
a	a	k8xC	a
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
571	[number]	k4	571
mm	mm	kA	mm
jeden	jeden	k4xCgMnSc1	jeden
od	od	k7c2	od
druhého	druhý	k4xOgNnSc2	druhý
</s>
</p>
<p>
<s>
20.	[number]	k4	20.
října	říjen	k1gInSc2	říjen
1960	[number]	k4	1960
–	–	k?	–
jedenácté	jedenáctý	k4xOgNnSc1	jedenáctý
CGPM	CGPM	kA	CGPM
definovalo	definovat	k5eAaBmAgNnS	definovat
metr	metr	k1gInSc4	metr
jako	jako	k9	jako
1 650 763,73	[number]	k4	1 650 763,73
násobku	násobek	k1gInSc2	násobek
vlnové	vlnový	k2eAgFnSc2d1	vlnová
délky	délka	k1gFnSc2	délka
radiace	radiace	k1gFnSc2	radiace
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
přechodu	přechod	k1gInSc3	přechod
mezi	mezi	k7c4	mezi
2p10	[number]	k4	2p10
a	a	k8xC	a
5d5	[number]	k4	5d5
kvantové	kvantový	k2eAgFnSc2d1	kvantová
úrovně	úroveň	k1gFnSc2	úroveň
atomu	atom	k1gInSc2	atom
kryptonu-86	kryptonu-86	k4	kryptonu-86
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
21.	[number]	k4	21.
října	říjen	k1gInSc2	říjen
1983	[number]	k4	1983
–	–	k?	–
sedmnácté	sedmnáctý	k4xOgFnSc6	sedmnáctý
CGPM	CGPM	kA	CGPM
definovalo	definovat	k5eAaBmAgNnS	definovat
délku	délka	k1gFnSc4	délka
jako	jako	k8xS	jako
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
urazí	urazit	k5eAaPmIp3nS	urazit
světlo	světlo	k1gNnSc1	světlo
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
během	během	k7c2	během
časového	časový	k2eAgInSc2d1	časový
intervalu	interval	k1gInSc2	interval
1/299 792 458	[number]	k4	1/299 792 458
sekundy	sekunda	k1gFnSc2	sekunda
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
světlo	světlo	k1gNnSc1	světlo
urazí	urazit	k5eAaPmIp3nS	urazit
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
přesně	přesně	k6eAd1	přesně
299 792 458	[number]	k4	299 792 458
metrů	metr	k1gInPc2	metr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgNnSc1d1	oficiální
francouzské	francouzský	k2eAgNnSc1d1	francouzské
znění	znění	k1gNnSc1	znění
je	být	k5eAaImIp3nS	být
<g/>
:	:	kIx,	:
Le	Le	k1gFnSc1	Le
mè	mè	k?	mè
est	est	k?	est
la	la	k1gNnSc1	la
longueur	longueur	k1gMnSc1	longueur
du	du	k?	du
trajet	trajet	k1gMnSc1	trajet
parcouru	parcour	k1gInSc2	parcour
dans	dans	k1gInSc1	dans
le	le	k?	le
vide	vid	k1gInSc5	vid
par	para	k1gFnPc2	para
la	la	k1gNnSc2	la
lumiè	lumiè	k?	lumiè
pendant	pendant	k1gInSc1	pendant
une	une	k?	une
durée	durée	k1gInSc1	durée
de	de	k?	de
1/299 792 458	[number]	k4	1/299 792 458
de	de	k?	de
seconde	second	k1gInSc5	second
<g/>
..	..	k?	..
Přesněji	přesně	k6eAd2	přesně
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
překlad	překlad	k1gInSc1	překlad
"	"	kIx"	"
<g/>
vakuum	vakuum	k1gNnSc1	vakuum
<g/>
"	"	kIx"	"
chápat	chápat	k5eAaImF	chápat
jako	jako	k8xS	jako
prázdný	prázdný	k2eAgInSc1d1	prázdný
prostor	prostor	k1gInSc1	prostor
nebo	nebo	k8xC	nebo
prázdnota	prázdnota	k1gFnSc1	prázdnota
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
void	void	k6eAd1	void
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
obvyklé	obvyklý	k2eAgFnSc2d1	obvyklá
značky	značka	k1gFnSc2	značka
m	m	kA	m
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
např.	např.	kA	např.
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
setkat	setkat	k5eAaPmF	setkat
se	s	k7c7	s
značením	značení	k1gNnSc7	značení
mt	mt	k?	mt
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
užívajících	užívající	k2eAgFnPc2d1	užívající
míle	míle	k1gFnSc1	míle
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
užívána	užíván	k2eAgFnSc1d1	užívána
zkratka	zkratka	k1gFnSc1	zkratka
"	"	kIx"	"
<g/>
m	m	kA	m
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
užívání	užívání	k1gNnSc1	užívání
zkratky	zkratka	k1gFnSc2	zkratka
"	"	kIx"	"
<g/>
m	m	kA	m
<g/>
"	"	kIx"	"
bývá	bývat	k5eAaImIp3nS	bývat
nahrazováno	nahrazován	k2eAgNnSc1d1	nahrazováno
"	"	kIx"	"
<g/>
mt	mt	k?	mt
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letecké	letecký	k2eAgFnSc6d1	letecká
dopravě	doprava	k1gFnSc6	doprava
se	se	k3xPyFc4	se
celosvětově	celosvětově	k6eAd1	celosvětově
užívá	užívat	k5eAaImIp3nS	užívat
pro	pro	k7c4	pro
určení	určení	k1gNnSc4	určení
výšky	výška	k1gFnSc2	výška
jednotka	jednotka	k1gFnSc1	jednotka
stopa	stopa	k1gFnSc1	stopa
místo	místo	k7c2	místo
metru	metr	k1gInSc2	metr
<g/>
;	;	kIx,	;
obdobně	obdobně	k6eAd1	obdobně
vodorovné	vodorovný	k2eAgFnSc2d1	vodorovná
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
v	v	k7c6	v
námořnictvu	námořnictvo	k1gNnSc6	námořnictvo
a	a	k8xC	a
letectvu	letectvo	k1gNnSc6	letectvo
se	se	k3xPyFc4	se
určují	určovat	k5eAaImIp3nP	určovat
v	v	k7c6	v
námořních	námořní	k2eAgFnPc6d1	námořní
mílích	míle	k1gFnPc6	míle
(	(	kIx(	(
<g/>
její	její	k3xOp3gFnSc1	její
definice	definice	k1gFnSc1	definice
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
odvozena	odvodit	k5eAaPmNgFnS	odvodit
z	z	k7c2	z
úhlového	úhlový	k2eAgInSc2d1	úhlový
stupně	stupeň	k1gInSc2	stupeň
na	na	k7c6	na
zemském	zemský	k2eAgInSc6d1	zemský
poledníku	poledník	k1gInSc6	poledník
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
jednotkách	jednotka	k1gFnPc6	jednotka
SI	si	k1gNnSc2	si
<g/>
,	,	kIx,	,
a	a	k8xC	a
1	[number]	k4	1
NM	NM	kA	NM
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
přesně	přesně	k6eAd1	přesně
rovna	roven	k2eAgFnSc1d1	rovna
1 852	[number]	k4	1 852
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byť	byť	k8xS	byť
ICAO	ICAO	kA	ICAO
sice	sice	k8xC	sice
hodlá	hodlat	k5eAaImIp3nS	hodlat
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
přejít	přejít	k5eAaPmF	přejít
na	na	k7c6	na
jednotky	jednotka	k1gFnPc4	jednotka
SI	si	k1gNnSc2	si
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgInS	být
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
změnu	změna	k1gFnSc4	změna
stanoven	stanoven	k2eAgInSc4d1	stanoven
termín	termín	k1gInSc4	termín
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
zachování	zachování	k1gNnSc3	zachování
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
složitý	složitý	k2eAgInSc4d1	složitý
a	a	k8xC	a
krajně	krajně	k6eAd1	krajně
nebezpečný	bezpečný	k2eNgInSc1d1	nebezpečný
proces	proces	k1gInSc1	proces
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
reálný	reálný	k2eAgInSc1d1	reálný
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
přechod	přechod	k1gInSc1	přechod
patrně	patrně	k6eAd1	patrně
až	až	k9	až
po	po	k7c6	po
plné	plný	k2eAgFnSc6d1	plná
automatizaci	automatizace	k1gFnSc6	automatizace
řízení	řízení	k1gNnSc2	řízení
vzdušného	vzdušný	k2eAgInSc2d1	vzdušný
provozu	provoz	k1gInSc2	provoz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Nemetrické	metrický	k2eNgFnPc1d1	metrický
délkové	délkový	k2eAgFnPc1d1	délková
jednotky	jednotka	k1gFnPc1	jednotka
===	===	k?	===
</s>
</p>
<p>
<s>
míle	míle	k1gFnSc1	míle
</s>
</p>
<p>
<s>
sáh	sáh	k1gInSc1	sáh
</s>
</p>
<p>
<s>
loket	loket	k1gInSc1	loket
</s>
</p>
<p>
<s>
stopa	stopa	k1gFnSc1	stopa
</s>
</p>
<p>
<s>
palec	palec	k1gInSc1	palec
</s>
</p>
<p>
<s>
yard	yard	k1gInSc1	yard
</s>
</p>
<p>
<s>
astronomická	astronomický	k2eAgFnSc1d1	astronomická
jednotka	jednotka	k1gFnSc1	jednotka
</s>
</p>
<p>
<s>
parsek	parsek	k1gInSc1	parsek
</s>
</p>
<p>
<s>
světelný	světelný	k2eAgInSc4d1	světelný
rok	rok	k1gInSc4	rok
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
metr	metr	k1gInSc4	metr
čtvereční	čtvereční	k2eAgInSc4d1	čtvereční
</s>
</p>
<p>
<s>
metr	metr	k1gInSc4	metr
krychlový	krychlový	k2eAgInSc4d1	krychlový
</s>
</p>
<p>
<s>
metr	metr	k1gInSc4	metr
na	na	k7c4	na
třetí	třetí	k4xOgNnSc4	třetí
</s>
</p>
<p>
<s>
metr	metr	k1gInSc4	metr
na	na	k7c4	na
čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
</s>
</p>
<p>
<s>
běžný	běžný	k2eAgInSc4d1	běžný
metr	metr	k1gInSc4	metr
</s>
</p>
<p>
<s>
plnometr	plnometr	k1gInSc1	plnometr
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
metr	metr	k1gInSc1	metr
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
metr	metr	k1gInSc4	metr
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
