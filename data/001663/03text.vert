<s>
Hans	Hans	k1gMnSc1	Hans
Albrecht	Albrecht	k1gMnSc1	Albrecht
Bethe	Bethe	k1gFnSc1	Bethe
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
Štrasburk	Štrasburk	k1gInSc1	Štrasburk
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
-	-	kIx~	-
6	[number]	k4	6
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
Ithaca	Ithaca	k1gFnSc1	Ithaca
(	(	kIx(	(
<g/>
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
fyziků	fyzik	k1gMnPc2	fyzik
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
principy	princip	k1gInPc4	princip
fungování	fungování	k1gNnSc2	fungování
termonukleárních	termonukleární	k2eAgFnPc2d1	termonukleární
reakcí	reakce	k1gFnPc2	reakce
ve	v	k7c6	v
hvězdách	hvězda	k1gFnPc6	hvězda
<g/>
,	,	kIx,	,
podílel	podílet	k5eAaImAgMnS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
vývoji	vývoj	k1gInSc6	vývoj
atomové	atomový	k2eAgFnSc2d1	atomová
bomby	bomba	k1gFnSc2	bomba
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
známým	známý	k2eAgMnSc7d1	známý
bojovníkem	bojovník	k1gMnSc7	bojovník
proti	proti	k7c3	proti
šíření	šíření	k1gNnSc3	šíření
jaderných	jaderný	k2eAgFnPc2d1	jaderná
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Bethe	Bethat	k5eAaPmIp3nS	Bethat
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1906	[number]	k4	1906
ve	v	k7c6	v
Štrasburku	Štrasburk	k1gInSc6	Štrasburk
jako	jako	k8xS	jako
jediné	jediný	k2eAgNnSc1d1	jediné
dítě	dítě	k1gNnSc1	dítě
Albrechta	Albrecht	k1gMnSc2	Albrecht
a	a	k8xC	a
Anny	Anna	k1gFnSc2	Anna
Betheových	Betheův	k2eAgMnPc2d1	Betheův
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
docentem	docent	k1gMnSc7	docent
fyziologie	fyziologie	k1gFnSc2	fyziologie
na	na	k7c4	na
Université	Universitý	k2eAgNnSc4d1	Université
de	de	k?	de
Strasbourg	Strasbourg	k1gInSc4	Strasbourg
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
protestantka	protestantka	k1gFnSc1	protestantka
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
otec	otec	k1gMnSc1	otec
židovského	židovský	k2eAgNnSc2d1	Židovské
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
.	.	kIx.	.
</s>
<s>
Bethe	Bethat	k5eAaPmIp3nS	Bethat
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
nejdřív	dříve	k6eAd3	dříve
rovněž	rovněž	k9	rovněž
pokládal	pokládat	k5eAaImAgMnS	pokládat
za	za	k7c4	za
žida	žid	k1gMnSc4	žid
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
ale	ale	k9	ale
přestal	přestat	k5eAaPmAgInS	přestat
věřit	věřit	k5eAaImF	věřit
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
ateistou	ateista	k1gMnSc7	ateista
<g/>
.	.	kIx.	.
,	,	kIx,	,
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
získal	získat	k5eAaPmAgMnS	získat
Betheho	Bethe	k1gMnSc2	Bethe
otec	otec	k1gMnSc1	otec
profesorské	profesorský	k2eAgNnSc4d1	profesorské
místo	místo	k1gNnSc4	místo
na	na	k7c4	na
Christian-Albrechts-Universität	Christian-Albrechts-Universität	k2eAgInSc4d1	Christian-Albrechts-Universität
zu	zu	k?	zu
Kiel	Kiel	k1gInSc4	Kiel
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
stěhovala	stěhovat	k5eAaImAgFnS	stěhovat
o	o	k7c4	o
3	[number]	k4	3
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
Albrecht	Albrecht	k1gMnSc1	Albrecht
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
profesorem	profesor	k1gMnSc7	profesor
na	na	k7c4	na
univerzitu	univerzita	k1gFnSc4	univerzita
Johanna	Johann	k1gMnSc2	Johann
Wolfganga	Wolfgang	k1gMnSc2	Wolfgang
Goetheho	Goethe	k1gMnSc2	Goethe
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
<g/>
.	.	kIx.	.
</s>
<s>
Bethe	Bethe	k1gInSc1	Bethe
zde	zde	k6eAd1	zde
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
gymnázium	gymnázium	k1gNnSc4	gymnázium
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
studium	studium	k1gNnSc1	studium
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
několik	několik	k4yIc4	několik
let	let	k1gInSc4	let
přerušeno	přerušit	k5eAaPmNgNnS	přerušit
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
nakazil	nakazit	k5eAaPmAgMnS	nakazit
tuberkulozou	tuberkuloza	k1gFnSc7	tuberkuloza
<g/>
.	.	kIx.	.
</s>
<s>
Školu	škola	k1gFnSc4	škola
dokončil	dokončit	k5eAaPmAgMnS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
šel	jít	k5eAaImAgMnS	jít
na	na	k7c4	na
univerzitu	univerzita	k1gFnSc4	univerzita
do	do	k7c2	do
Frankfurtu	Frankfurt	k1gInSc2	Frankfurt
studovat	studovat	k5eAaImF	studovat
chemii	chemie	k1gFnSc4	chemie
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
již	již	k6eAd1	již
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
přešel	přejít	k5eAaPmAgInS	přejít
na	na	k7c4	na
Mnichovskou	mnichovský	k2eAgFnSc4d1	Mnichovská
univerzitu	univerzita	k1gFnSc4	univerzita
kde	kde	k6eAd1	kde
studoval	studovat	k5eAaImAgMnS	studovat
fyziku	fyzika	k1gFnSc4	fyzika
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Arnolda	Arnold	k1gMnSc2	Arnold
Sommerfelda	Sommerfeld	k1gMnSc2	Sommerfeld
<g/>
.	.	kIx.	.
</s>
<s>
Tématem	téma	k1gNnSc7	téma
jeho	jeho	k3xOp3gFnSc2	jeho
disertační	disertační	k2eAgFnSc2d1	disertační
prácee	práce	k1gFnSc2	práce
byla	být	k5eAaImAgFnS	být
difrakce	difrakce	k1gFnSc1	difrakce
elektronů	elektron	k1gInPc2	elektron
na	na	k7c6	na
krystalech	krystal	k1gInPc6	krystal
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
doktorátu	doktorát	k1gInSc2	doktorát
začal	začít	k5eAaPmAgMnS	začít
Bethe	Bethe	k1gNnSc4	Bethe
pracovat	pracovat	k5eAaImF	pracovat
jako	jako	k8xS	jako
asistent	asistent	k1gMnSc1	asistent
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebyl	být	k5eNaImAgMnS	být
zde	zde	k6eAd1	zde
spokojen	spokojen	k2eAgMnSc1d1	spokojen
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
přijal	přijmout	k5eAaPmAgInS	přijmout
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
Technische	Technische	k1gNnSc6	Technische
Hochschule	Hochschule	k1gFnSc2	Hochschule
ve	v	k7c6	v
Stuttgartu	Stuttgart	k1gInSc6	Stuttgart
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
zde	zde	k6eAd1	zde
řešit	řešit	k5eAaImF	řešit
problém	problém	k1gInSc4	problém
kolize	kolize	k1gFnSc2	kolize
částic	částice	k1gFnPc2	částice
s	s	k7c7	s
hmotou	hmota	k1gFnSc7	hmota
a	a	k8xC	a
přišel	přijít	k5eAaPmAgInS	přijít
na	na	k7c4	na
rovnici	rovnice	k1gFnSc4	rovnice
dnes	dnes	k6eAd1	dnes
nazývanou	nazývaný	k2eAgFnSc4d1	nazývaná
Betheho	Bethe	k1gMnSc4	Bethe
formule	formule	k1gFnSc1	formule
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
tento	tento	k3xDgInSc4	tento
problém	problém	k1gInSc1	problém
zjednodušuje	zjednodušovat	k5eAaImIp3nS	zjednodušovat
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
práci	práce	k1gFnSc3	práce
se	se	k3xPyFc4	se
habilitoval	habilitovat	k5eAaBmAgMnS	habilitovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
odešel	odejít	k5eAaPmAgMnS	odejít
na	na	k7c4	na
doporučení	doporučení	k1gNnSc4	doporučení
Sommerfelda	Sommerfeldo	k1gNnSc2	Sommerfeldo
do	do	k7c2	do
Cavendishovy	Cavendishův	k2eAgFnSc2d1	Cavendishova
laboratoře	laboratoř	k1gFnSc2	laboratoř
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Cambridgi	Cambridge	k1gFnSc6	Cambridge
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pracoval	pracovat	k5eAaImAgMnS	pracovat
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
R.	R.	kA	R.
H.	H.	kA	H.
Fowlera	Fowler	k1gMnSc2	Fowler
<g/>
.	.	kIx.	.
</s>
<s>
Přišel	přijít	k5eAaPmAgMnS	přijít
zde	zde	k6eAd1	zde
na	na	k7c4	na
relativistickou	relativistický	k2eAgFnSc4d1	relativistická
formulaci	formulace	k1gFnSc4	formulace
Betheho	Bethe	k1gMnSc2	Bethe
formule	formule	k1gFnSc2	formule
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
znám	znám	k2eAgInSc1d1	znám
svým	svůj	k3xOyFgInSc7	svůj
velkým	velký	k2eAgInSc7d1	velký
smyslem	smysl	k1gInSc7	smysl
pro	pro	k7c4	pro
humor	humor	k1gInSc4	humor
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
například	například	k6eAd1	například
fiktivní	fiktivní	k2eAgFnSc4d1	fiktivní
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
počítal	počítat	k5eAaImAgInS	počítat
hodnotu	hodnota	k1gFnSc4	hodnota
konstanty	konstanta	k1gFnSc2	konstanta
jemné	jemný	k2eAgFnSc2d1	jemná
struktury	struktura	k1gFnSc2	struktura
z	z	k7c2	z
teploty	teplota	k1gFnSc2	teplota
absolutní	absolutní	k2eAgFnSc2d1	absolutní
nuly	nula	k1gFnSc2	nula
v	v	k7c6	v
Celsiově	Celsiův	k2eAgFnSc6d1	Celsiova
stupnici	stupnice	k1gFnSc6	stupnice
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
dalším	další	k2eAgNnSc7d1	další
pracovištěm	pracoviště	k1gNnSc7	pracoviště
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
stal	stát	k5eAaPmAgInS	stát
Řím	Řím	k1gInSc1	Řím
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mu	on	k3xPp3gMnSc3	on
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
místo	místo	k7c2	místo
Enrico	Enrico	k6eAd1	Enrico
Fermi	Fer	k1gFnPc7	Fer
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
krátce	krátce	k6eAd1	krátce
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
jako	jako	k8xC	jako
docent	docent	k1gMnSc1	docent
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mluvil	mluvit	k5eAaImAgMnS	mluvit
plynně	plynně	k6eAd1	plynně
anglicky	anglicky	k6eAd1	anglicky
<g/>
,	,	kIx,	,
dohlížel	dohlížet	k5eAaImAgMnS	dohlížet
na	na	k7c4	na
všechny	všechen	k3xTgInPc4	všechen
anglicky	anglicky	k6eAd1	anglicky
mluvící	mluvící	k2eAgInPc4d1	mluvící
postdoktorandy	postdoktorand	k1gInPc4	postdoktorand
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
napsal	napsat	k5eAaBmAgMnS	napsat
článek	článek	k1gInSc4	článek
o	o	k7c6	o
kvantově	kvantově	k6eAd1	kvantově
mechanickém	mechanický	k2eAgInSc6d1	mechanický
pohledu	pohled	k1gInSc6	pohled
na	na	k7c4	na
atomy	atom	k1gInPc4	atom
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
helia	helium	k1gNnSc2	helium
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
článek	článek	k1gInSc1	článek
se	se	k3xPyFc4	se
týkal	týkat	k5eAaImAgInS	týkat
elektronů	elektron	k1gInPc2	elektron
v	v	k7c6	v
kovech	kov	k1gInPc6	kov
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
článek	článek	k1gInSc1	článek
položil	položit	k5eAaPmAgInS	položit
základy	základ	k1gInPc4	základ
pozdější	pozdní	k2eAgFnSc3d2	pozdější
polovidčové	polovidčový	k2eAgFnSc3d1	polovidčový
fyzice	fyzika	k1gFnSc3	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Fermim	Fermi	k1gNnSc7	Fermi
napsal	napsat	k5eAaBmAgMnS	napsat
při	při	k7c6	při
dalším	další	k2eAgInSc6d1	další
pobytu	pobyt	k1gInSc6	pobyt
v	v	k7c6	v
itálii	itálie	k1gFnSc6	itálie
článek	článek	k1gInSc1	článek
o	o	k7c6	o
kvantové	kvantový	k2eAgFnSc6d1	kvantová
elektrodynamice	elektrodynamika	k1gFnSc6	elektrodynamika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
popisuje	popisovat	k5eAaImIp3nS	popisovat
relativistické	relativistický	k2eAgFnPc4d1	relativistická
interakce	interakce	k1gFnPc4	interakce
nabitých	nabitý	k2eAgFnPc2d1	nabitá
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
přijal	přijmout	k5eAaPmAgMnS	přijmout
místo	místo	k7c2	místo
asistenta	asistent	k1gMnSc2	asistent
profesora	profesor	k1gMnSc2	profesor
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
Tübingen	Tübingen	k1gInSc4	Tübingen
<g/>
,	,	kIx,	,
profesorem	profesor	k1gMnSc7	profesor
experimentální	experimentální	k2eAgFnSc2d1	experimentální
fyziky	fyzika	k1gFnSc2	fyzika
byl	být	k5eAaImAgMnS	být
Hans	Hans	k1gMnSc1	Hans
Geiger	Geiger	k1gMnSc1	Geiger
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
NSDAP	NSDAP	kA	NSDAP
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
ale	ale	k8xC	ale
Bethe	Bethe	k1gFnSc1	Bethe
kvůli	kvůli	k7c3	kvůli
svému	svůj	k3xOyFgInSc3	svůj
židovskému	židovský	k2eAgInSc3d1	židovský
původu	původ	k1gInSc3	původ
o	o	k7c4	o
místo	místo	k1gNnSc4	místo
přišel	přijít	k5eAaPmAgMnS	přijít
<g/>
.	.	kIx.	.
</s>
<s>
Geiger	Geiger	k1gMnSc1	Geiger
mu	on	k3xPp3gMnSc3	on
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
pomoci	pomoct	k5eAaPmF	pomoct
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Sommerfeld	Sommerfeld	k1gInSc1	Sommerfeld
ho	on	k3xPp3gInSc2	on
novu	nov	k1gInSc2	nov
zaměstnal	zaměstnat	k5eAaPmAgMnS	zaměstnat
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
ale	ale	k8xC	ale
Bethe	Bethe	k1gInSc1	Bethe
emigroval	emigrovat	k5eAaBmAgInS	emigrovat
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
díky	díky	k7c3	díky
Sommerfeldově	Sommerfeldův	k2eAgFnSc3d1	Sommerfeldův
známosti	známost	k1gFnSc3	známost
s	s	k7c7	s
Williamem	William	k1gInSc7	William
Braggem	Bragg	k1gInSc7	Bragg
získal	získat	k5eAaPmAgInS	získat
práci	práce	k1gFnSc4	práce
na	na	k7c4	na
The	The	k1gFnSc4	The
University	universita	k1gFnSc2	universita
of	of	k?	of
Manchester	Manchester	k1gInSc1	Manchester
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
odešel	odejít	k5eAaPmAgMnS	odejít
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
přítelem	přítel	k1gMnSc7	přítel
Rudolfem	Rudolf	k1gMnSc7	Rudolf
Peierlsem	Peierls	k1gMnSc7	Peierls
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
rovněž	rovněž	k9	rovněž
kvůli	kvůli	k7c3	kvůli
židovskému	židovský	k2eAgInSc3d1	židovský
původu	původ	k1gInSc3	původ
přišel	přijít	k5eAaPmAgInS	přijít
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
o	o	k7c4	o
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k9	co
Maurice	Maurika	k1gFnSc3	Maurika
Goldhaber	Goldhaber	k1gMnSc1	Goldhaber
a	a	k8xC	a
James	James	k1gMnSc1	James
Chadwick	Chadwick	k1gMnSc1	Chadwick
objevili	objevit	k5eAaPmAgMnP	objevit
fotodisintegraci	fotodisintegrace	k1gFnSc4	fotodisintegrace
deuteria	deuterium	k1gNnSc2	deuterium
<g/>
,	,	kIx,	,
dostali	dostat	k5eAaPmAgMnP	dostat
Peierls	Peierls	k1gInSc4	Peierls
a	a	k8xC	a
Bethe	Bethe	k1gInSc4	Bethe
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
tento	tento	k3xDgInSc4	tento
jev	jev	k1gInSc4	jev
teoreticky	teoreticky	k6eAd1	teoreticky
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
dokázali	dokázat	k5eAaPmAgMnP	dokázat
během	během	k7c2	během
čtyřhodinové	čtyřhodinový	k2eAgFnSc2d1	čtyřhodinová
jízdy	jízda	k1gFnSc2	jízda
vlakem	vlak	k1gInSc7	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Bethe	Bethe	k6eAd1	Bethe
tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
zkoumal	zkoumat	k5eAaImAgInS	zkoumat
i	i	k9	i
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
získal	získat	k5eAaPmAgMnS	získat
na	na	k7c4	na
doporučení	doporučení	k1gNnSc4	doporučení
Lloyda	Lloydo	k1gNnSc2	Lloydo
Smithe	Smith	k1gMnSc2	Smith
místo	místo	k7c2	místo
profesora	profesor	k1gMnSc2	profesor
na	na	k7c6	na
Cornellově	Cornellův	k2eAgFnSc6d1	Cornellova
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
sháněla	shánět	k5eAaImAgFnS	shánět
nového	nový	k2eAgMnSc4d1	nový
teoretického	teoretický	k2eAgMnSc4d1	teoretický
fyzika	fyzik	k1gMnSc4	fyzik
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
předtím	předtím	k6eAd1	předtím
ale	ale	k8xC	ale
krátce	krátce	k6eAd1	krátce
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c4	na
University	universita	k1gFnPc4	universita
of	of	k?	of
Bristol	Bristol	k1gInSc1	Bristol
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgInS	působit
Nevill	Nevill	k1gInSc1	Nevill
Mott	motto	k1gNnPc2	motto
a	a	k8xC	a
v	v	k7c6	v
Kodani	Kodaň	k1gFnSc6	Kodaň
v	v	k7c6	v
institutu	institut	k1gInSc6	institut
Nielse	Niels	k1gMnSc2	Niels
Bohra	Bohr	k1gMnSc2	Bohr
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
USA	USA	kA	USA
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
<s>
Cornellova	Cornellův	k2eAgFnSc1d1	Cornellova
univerzita	univerzita	k1gFnSc1	univerzita
tehdy	tehdy	k6eAd1	tehdy
budovala	budovat	k5eAaImAgFnS	budovat
novou	nový	k2eAgFnSc4d1	nová
skupinu	skupina	k1gFnSc4	skupina
fyziků	fyzik	k1gMnPc2	fyzik
a	a	k8xC	a
potřebovala	potřebovat	k5eAaImAgFnS	potřebovat
experimentátora	experimentátor	k1gMnSc4	experimentátor
<g/>
.	.	kIx.	.
</s>
<s>
Bethe	Bethe	k1gInSc1	Bethe
doporučil	doporučit	k5eAaPmAgInS	doporučit
Roberta	Robert	k1gMnSc4	Robert
Bachera	Bacher	k1gMnSc4	Bacher
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
byla	být	k5eAaImAgFnS	být
najat	najat	k2eAgInSc4d1	najat
Stanley	Stanley	k1gInPc1	Stanley
Livingston	Livingston	k1gInSc4	Livingston
aby	aby	kYmCp3nS	aby
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
vybudoval	vybudovat	k5eAaPmAgInS	vybudovat
urychlovač	urychlovač	k1gInSc4	urychlovač
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
dostal	dostat	k5eAaPmAgInS	dostat
nabídku	nabídka	k1gFnSc4	nabídka
z	z	k7c2	z
Univerzita	univerzita	k1gFnSc1	univerzita
Illinois	Illinois	k1gInSc4	Illinois
v	v	k7c4	v
Urbana	Urban	k1gMnSc4	Urban
Champaign	Champaign	k1gInSc4	Champaign
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Cornell	Cornell	k1gInSc1	Cornell
mu	on	k3xPp3gMnSc3	on
dvakrát	dvakrát	k6eAd1	dvakrát
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
plat	plat	k1gInSc4	plat
a	a	k8xC	a
Bethe	Bethe	k1gInSc4	Bethe
proto	proto	k8xC	proto
zůstal	zůstat	k5eAaPmAgMnS	zůstat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Cornellově	Cornellův	k2eAgFnSc6d1	Cornellova
univerzitě	univerzita	k1gFnSc6	univerzita
publikoval	publikovat	k5eAaBmAgMnS	publikovat
společně	společně	k6eAd1	společně
s	s	k7c7	s
Livingstonem	Livingston	k1gInSc7	Livingston
a	a	k8xC	a
Bacherem	Bacher	k1gInSc7	Bacher
sérii	série	k1gFnSc4	série
článků	článek	k1gInPc2	článek
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
co	co	k9	co
víme	vědět	k5eAaImIp1nP	vědět
o	o	k7c6	o
jaderné	jaderný	k2eAgFnSc6d1	jaderná
fyzice	fyzika	k1gFnSc6	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
výroční	výroční	k2eAgFnSc2d1	výroční
konference	konference	k1gFnSc2	konference
o	o	k7c6	o
teoretické	teoretický	k2eAgFnSc6d1	teoretická
fyzice	fyzika	k1gFnSc6	fyzika
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
George	George	k1gFnSc4	George
Gamow	Gamow	k1gFnSc2	Gamow
a	a	k8xC	a
Subrahmanyan	Subrahmanyan	k1gMnSc1	Subrahmanyan
Chandrasekhar	Chandrasekhar	k1gMnSc1	Chandrasekhar
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
von	von	k1gInSc4	von
Neumann	Neumann	k1gMnSc1	Neumann
<g/>
,	,	kIx,	,
Gregory	Gregor	k1gMnPc4	Gregor
Breit	Breita	k1gFnPc2	Breita
nebo	nebo	k8xC	nebo
Edward	Edward	k1gMnSc1	Edward
Teller	Teller	k1gMnSc1	Teller
<g/>
.	.	kIx.	.
</s>
<s>
Bethe	Bethat	k5eAaPmIp3nS	Bethat
nejprve	nejprve	k6eAd1	nejprve
účast	účast	k1gFnSc4	účast
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
o	o	k7c4	o
hvězdnou	hvězdný	k2eAgFnSc4d1	hvězdná
nukleosyntézu	nukleosyntéza	k1gFnSc4	nukleosyntéza
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
tématem	téma	k1gNnSc7	téma
nezajímal	zajímat	k5eNaImAgMnS	zajímat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Teller	Teller	k1gMnSc1	Teller
ho	on	k3xPp3gNnSc4	on
k	k	k7c3	k
účasti	účast	k1gFnSc3	účast
přemluvil	přemluvit	k5eAaPmAgMnS	přemluvit
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
řečníků	řečník	k1gMnPc2	řečník
Bengt	Bengt	k1gInSc4	Bengt
Strömgren	Strömgrna	k1gFnPc2	Strömgrna
uvedl	uvést	k5eAaPmAgMnS	uvést
na	na	k7c6	na
konferenci	konference	k1gFnSc6	konference
co	co	k3yRnSc1	co
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
o	o	k7c6	o
teplotě	teplota	k1gFnSc6	teplota
<g/>
,	,	kIx,	,
hustotě	hustota	k1gFnSc6	hustota
a	a	k8xC	a
chemickém	chemický	k2eAgNnSc6d1	chemické
složení	složení	k1gNnSc6	složení
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
nabádal	nabádat	k5eAaBmAgInS	nabádat
fyziky	fyzik	k1gMnPc4	fyzik
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
přišli	přijít	k5eAaPmAgMnP	přijít
s	s	k7c7	s
vysvětlením	vysvětlení	k1gNnSc7	vysvětlení
jak	jak	k8xC	jak
Slunce	slunce	k1gNnSc1	slunce
funguje	fungovat	k5eAaImIp3nS	fungovat
<g/>
.	.	kIx.	.
</s>
<s>
Gamow	Gamow	k?	Gamow
a	a	k8xC	a
Carl	Carl	k1gMnSc1	Carl
Friedrich	Friedrich	k1gMnSc1	Friedrich
von	von	k1gInSc4	von
Weizsäcker	Weizsäckero	k1gNnPc2	Weizsäckero
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
Slunci	slunce	k1gNnSc6	slunce
probíhá	probíhat	k5eAaImIp3nS	probíhat
proton-protonová	protonrotonový	k2eAgFnSc1d1	proton-protonový
reakce	reakce	k1gFnSc1	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Nedokázali	dokázat	k5eNaPmAgMnP	dokázat
ale	ale	k8xC	ale
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
vzik	vzik	k6eAd1	vzik
prvů	prv	k1gInPc2	prv
těžších	těžký	k2eAgNnPc2d2	těžší
než	než	k8xS	než
helium	helium	k1gNnSc4	helium
<g/>
.	.	kIx.	.
</s>
<s>
Bethe	Bethat	k5eAaPmIp3nS	Bethat
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
otázkou	otázka	k1gFnSc7	otázka
reakcí	reakce	k1gFnPc2	reakce
ve	v	k7c6	v
hvězdách	hvězda	k1gFnPc6	hvězda
zabývat	zabývat	k5eAaImF	zabývat
a	a	k8xC	a
s	s	k7c7	s
Charlesem	Charles	k1gMnSc7	Charles
Critchfieldem	Critchfield	k1gMnSc7	Critchfield
objevil	objevit	k5eAaPmAgMnS	objevit
ještě	ještě	k6eAd1	ještě
do	do	k7c2	do
skončení	skončení	k1gNnSc2	skončení
konference	konference	k1gFnSc2	konference
reakce	reakce	k1gFnSc2	reakce
při	při	k7c6	při
nichž	jenž	k3xRgNnPc6	jenž
vznikají	vznikat	k5eAaImIp3nP	vznikat
těžší	těžký	k2eAgInPc1d2	těžší
prvky	prvek	k1gInPc1	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
na	na	k7c4	na
Cornell	Cornell	k1gInSc4	Cornell
objevil	objevit	k5eAaPmAgMnS	objevit
rovněž	rovněž	k9	rovněž
CNO	CNO	kA	CNO
(	(	kIx(	(
<g/>
uhlík-dusík-kyslík	uhlíkusíkyslík	k1gMnSc1	uhlík-dusík-kyslík
<g/>
)	)	kIx)	)
cyklus	cyklus	k1gInSc1	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
křišťálové	křišťálový	k2eAgFnSc6d1	Křišťálová
noci	noc	k1gFnSc6	noc
měl	mít	k5eAaImAgInS	mít
stále	stále	k6eAd1	stále
větší	veliký	k2eAgFnPc4d2	veliký
obavy	obava	k1gFnPc4	obava
o	o	k7c4	o
matku	matka	k1gFnSc4	matka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zůstala	zůstat	k5eAaPmAgFnS	zůstat
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Využil	využít	k5eAaPmAgMnS	využít
proto	proto	k8xC	proto
peníze	peníz	k1gInPc4	peníz
získané	získaný	k2eAgInPc4d1	získaný
za	za	k7c4	za
o	o	k7c6	o
CNO	CNO	kA	CNO
cyklu	cyklus	k1gInSc2	cyklus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
cenu	cena	k1gFnSc4	cena
pro	pro	k7c4	pro
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
článek	článek	k1gInSc4	článek
o	o	k7c6	o
sluneční	sluneční	k2eAgFnSc6d1	sluneční
a	a	k8xC	a
hvězdné	hvězdný	k2eAgFnSc3d1	hvězdná
energii	energie	k1gFnSc3	energie
a	a	k8xC	a
pomohl	pomoct	k5eAaPmAgMnS	pomoct
matce	matka	k1gFnSc3	matka
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
Betheho	Bethe	k1gMnSc4	Bethe
článek	článek	k1gInSc1	článek
znamenal	znamenat	k5eAaImAgInS	znamenat
přelom	přelom	k1gInSc4	přelom
v	v	k7c6	v
chápání	chápání	k1gNnSc6	chápání
hvězd	hvězda	k1gFnPc2	hvězda
a	a	k8xC	a
Bethe	Bethe	k1gFnPc2	Bethe
za	za	k7c2	za
něj	on	k3xPp3gMnSc2	on
získal	získat	k5eAaPmAgMnS	získat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Rose	Rose	k1gMnSc1	Rose
Ewald	Ewald	k1gMnSc1	Ewald
<g/>
.	.	kIx.	.
</s>
<s>
Měli	mít	k5eAaImAgMnP	mít
spolu	spolu	k6eAd1	spolu
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
1941	[number]	k4	1941
získal	získat	k5eAaPmAgInS	získat
americké	americký	k2eAgNnSc4d1	americké
státní	státní	k2eAgNnSc4d1	státní
občanství	občanství	k1gNnSc4	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k9	co
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
se	se	k3xPyFc4	se
Bethe	Bethe	k1gInSc4	Bethe
začal	začít	k5eAaPmAgInS	začít
podílet	podílet	k5eAaImF	podílet
na	na	k7c6	na
válečných	válečný	k2eAgInPc6d1	válečný
projektech	projekt	k1gInPc6	projekt
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
do	do	k7c2	do
zisku	zisk	k1gInSc2	zisk
občanství	občanství	k1gNnSc2	občanství
byl	být	k5eAaImAgMnS	být
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
za	za	k7c4	za
neschopného	schopný	k2eNgMnSc4d1	neschopný
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Tellerem	Teller	k1gInSc7	Teller
ale	ale	k8xC	ale
pracovali	pracovat	k5eAaImAgMnP	pracovat
na	na	k7c4	na
teorii	teorie	k1gFnSc4	teorie
rázových	rázový	k2eAgFnPc2d1	rázová
vln	vlna	k1gFnPc2	vlna
generovaných	generovaný	k2eAgFnPc2d1	generovaná
při	při	k7c6	při
průchodu	průchod	k1gInSc6	průchod
projektilu	projektil	k1gInSc2	projektil
skrz	skrz	k7c4	skrz
plyn	plyn	k1gInSc4	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
problematikou	problematika	k1gFnSc7	problematika
obrněných	obrněný	k2eAgFnPc2d1	obrněná
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tato	tento	k3xDgFnSc1	tento
práce	práce	k1gFnSc1	práce
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
zakázána	zakázán	k2eAgFnSc1d1	zakázána
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
ještě	ještě	k6eAd1	ještě
nebyl	být	k5eNaImAgInS	být
občanem	občan	k1gMnSc7	občan
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obdržení	obdržení	k1gNnSc6	obdržení
občanství	občanství	k1gNnSc2	občanství
a	a	k8xC	a
bezpečnostní	bezpečnostní	k2eAgFnSc2d1	bezpečnostní
prověrky	prověrka	k1gFnSc2	prověrka
pracoval	pracovat	k5eAaImAgInS	pracovat
v	v	k7c6	v
radiologických	radiologický	k2eAgFnPc6d1	radiologická
laboratořích	laboratoř	k1gFnPc6	laboratoř
Massachusettského	massachusettský	k2eAgInSc2d1	massachusettský
technologického	technologický	k2eAgInSc2d1	technologický
institutu	institut	k1gInSc2	institut
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
objevil	objevit	k5eAaPmAgMnS	objevit
nový	nový	k2eAgInSc4d1	nový
typ	typ	k1gInSc4	typ
vlnovodu	vlnovod	k1gInSc2	vlnovod
používaný	používaný	k2eAgInSc4d1	používaný
v	v	k7c6	v
radarech	radar	k1gInPc6	radar
a	a	k8xC	a
na	na	k7c6	na
Kalifornské	kalifornský	k2eAgFnSc6d1	kalifornská
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c4	v
Berkeley	Berkeley	k1gInPc4	Berkeley
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Robertem	Robert	k1gMnSc7	Robert
Oppenheimerem	Oppenheimer	k1gMnSc7	Oppenheimer
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
atomové	atomový	k2eAgFnSc6d1	atomová
bombě	bomba	k1gFnSc6	bomba
<g/>
.	.	kIx.	.
</s>
<s>
Diskutovali	diskutovat	k5eAaImAgMnP	diskutovat
o	o	k7c4	o
možnosti	možnost	k1gFnPc4	možnost
použití	použití	k1gNnSc2	použití
plutonia	plutonium	k1gNnSc2	plutonium
a	a	k8xC	a
uranu	uran	k1gInSc2	uran
<g/>
.	.	kIx.	.
</s>
<s>
Teller	Teller	k1gInSc1	Teller
rovněž	rovněž	k9	rovněž
představil	představit	k5eAaPmAgInS	představit
svůj	svůj	k3xOyFgInSc4	svůj
návrh	návrh	k1gInSc4	návrh
superbomby	superbomba	k1gFnSc2	superbomba
a	a	k8xC	a
pověřil	pověřit	k5eAaPmAgMnS	pověřit
Betheho	Bethe	k1gMnSc4	Bethe
<g/>
,	,	kIx,	,
výpočtem	výpočet	k1gInSc7	výpočet
zda	zda	k8xS	zda
by	by	kYmCp3nS	by
tato	tento	k3xDgFnSc1	tento
zbraň	zbraň	k1gFnSc1	zbraň
mohla	moct	k5eAaImAgFnS	moct
zapálit	zapálit	k5eAaPmF	zapálit
dusík	dusík	k1gInSc4	dusík
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Bethe	Bethe	k6eAd1	Bethe
dokázal	dokázat	k5eAaPmAgMnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
tajných	tajný	k2eAgInPc2d1	tajný
objektů	objekt	k1gInPc2	objekt
v	v	k7c4	v
Los	los	k1gInSc4	los
Alamos	Alamosa	k1gFnPc2	Alamosa
se	se	k3xPyFc4	se
Bethe	Bethe	k1gInSc1	Bethe
stal	stát	k5eAaPmAgInS	stát
vedoucím	vedoucí	k1gFnPc3	vedoucí
teoretické	teoretický	k2eAgFnSc2d1	teoretická
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
nejmenší	malý	k2eAgFnSc2d3	nejmenší
avšak	avšak	k8xC	avšak
nejprestižnější	prestižní	k2eAgFnSc2d3	nejprestižnější
divize	divize	k1gFnSc2	divize
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
podráždilo	podráždit	k5eAaPmAgNnS	podráždit
zkušenější	zkušený	k2eAgMnPc4d2	zkušenější
kolegy	kolega	k1gMnPc4	kolega
Tellera	Teller	k1gMnSc4	Teller
a	a	k8xC	a
Blocha	Bloch	k1gMnSc4	Bloch
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
Teller	Teller	k1gMnSc1	Teller
měl	mít	k5eAaImAgMnS	mít
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Betheho	Bethe	k1gMnSc2	Bethe
řadu	řad	k1gInSc2	řad
sporů	spor	k1gInPc2	spor
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
přeřazení	přeřazení	k1gNnSc3	přeřazení
Tellerovy	Tellerův	k2eAgFnSc2d1	Tellerův
skupiny	skupina	k1gFnSc2	skupina
přímo	přímo	k6eAd1	přímo
pod	pod	k7c4	pod
Oppenheimera	Oppenheimer	k1gMnSc4	Oppenheimer
<g/>
.	.	kIx.	.
</s>
<s>
Bethe	Bethe	k6eAd1	Bethe
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
problematice	problematika	k1gFnSc6	problematika
štěpné	štěpný	k2eAgFnSc2d1	štěpná
bomby	bomba	k1gFnSc2	bomba
založené	založený	k2eAgFnSc2d1	založená
na	na	k7c6	na
uranu	uran	k1gInSc6	uran
<g/>
,	,	kIx,	,
počítal	počítat	k5eAaImAgInS	počítat
kritické	kritický	k2eAgNnSc4d1	kritické
množství	množství	k1gNnSc4	množství
pro	pro	k7c4	pro
explozi	exploze	k1gFnSc4	exploze
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Richardem	Richard	k1gMnSc7	Richard
Feynmanem	Feynman	k1gMnSc7	Feynman
rovněž	rovněž	k9	rovněž
určili	určit	k5eAaPmAgMnP	určit
rovnici	rovnice	k1gFnSc4	rovnice
pro	pro	k7c4	pro
explozivní	explozivní	k2eAgInSc4d1	explozivní
výtěžek	výtěžek	k1gInSc4	výtěžek
bomby	bomba	k1gFnSc2	bomba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
přeorientovala	přeorientovat	k5eAaPmAgFnS	přeorientovat
na	na	k7c4	na
problémy	problém	k1gInPc4	problém
spojené	spojený	k2eAgInPc4d1	spojený
s	s	k7c7	s
implozí	imploze	k1gFnSc7	imploze
plutoniové	plutoniový	k2eAgFnSc2d1	plutoniová
bomby	bomba	k1gFnSc2	bomba
<g/>
.	.	kIx.	.
</s>
<s>
Pracoval	pracovat	k5eAaImAgInS	pracovat
například	například	k6eAd1	například
na	na	k7c6	na
hydrodynamických	hydrodynamický	k2eAgInPc6d1	hydrodynamický
aspektech	aspekt	k1gInPc6	aspekt
imploze	imploze	k1gFnSc2	imploze
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
jadenrý	jadenrý	k2eAgInSc1d1	jadenrý
test	test	k1gInSc1	test
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
když	když	k8xS	když
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
silnější	silný	k2eAgFnSc6d2	silnější
termojaderné	termojaderný	k2eAgFnSc6d1	termojaderná
bombě	bomba	k1gFnSc6	bomba
<g/>
,	,	kIx,	,
Bethe	Bethe	k1gInSc1	Bethe
argumentoval	argumentovat	k5eAaImAgInS	argumentovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
tato	tento	k3xDgFnSc1	tento
neměla	mít	k5eNaImAgFnS	mít
být	být	k5eAaImF	být
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
projekt	projekt	k1gInSc1	projekt
již	již	k6eAd1	již
běžel	běžet	k5eAaImAgInS	běžet
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
z	z	k7c2	z
vyšších	vysoký	k2eAgNnPc2d2	vyšší
míst	místo	k1gNnPc2	místo
a	a	k8xC	a
Bethe	Bethe	k1gFnPc2	Bethe
sehrál	sehrát	k5eAaPmAgInS	sehrát
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
nové	nový	k2eAgFnSc2d1	nová
zbraně	zbraň	k1gFnSc2	zbraň
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
roli	role	k1gFnSc4	role
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
ještě	ještě	k9	ještě
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
dokončením	dokončení	k1gNnSc7	dokončení
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
sestrojit	sestrojit	k5eAaPmF	sestrojit
tuto	tento	k3xDgFnSc4	tento
zbraň	zbraň	k1gFnSc4	zbraň
bude	být	k5eAaImBp3nS	být
nemožné	možný	k2eNgNnSc1d1	nemožné
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
Bethe	Bethe	k1gInSc1	Bethe
vypovídal	vypovídat	k5eAaPmAgInS	vypovídat
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Oppenheimera	Oppenheimero	k1gNnSc2	Oppenheimero
v	v	k7c6	v
politickém	politický	k2eAgInSc6d1	politický
procesu	proces	k1gInSc6	proces
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
Edward	Edward	k1gMnSc1	Edward
Teller	Teller	k1gMnSc1	Teller
vypovídal	vypovídat	k5eAaImAgMnS	vypovídat
proti	proti	k7c3	proti
i	i	k9	i
přes	přes	k7c4	přes
Betheovu	Betheův	k2eAgFnSc4d1	Betheův
snahu	snaha	k1gFnSc4	snaha
ho	on	k3xPp3gMnSc4	on
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
k	k	k7c3	k
výpovědi	výpověď	k1gFnSc3	výpověď
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Oppenheimera	Oppenheimero	k1gNnSc2	Oppenheimero
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
kvůli	kvůli	k7c3	kvůli
tomuto	tento	k3xDgNnSc3	tento
svědectví	svědectví	k1gNnSc3	svědectví
byla	být	k5eAaImAgFnS	být
Oppenheimerovi	Oppenheimerův	k2eAgMnPc1d1	Oppenheimerův
odňata	odňat	k2eAgFnSc1d1	odňata
bezpečnostní	bezpečnostní	k2eAgFnSc1d1	bezpečnostní
prověrka	prověrka	k1gFnSc1	prověrka
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
epizoda	epizoda	k1gFnSc1	epizoda
trvale	trvale	k6eAd1	trvale
poznamenala	poznamenat	k5eAaPmAgFnS	poznamenat
vztah	vztah	k1gInSc4	vztah
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgMnPc7	dva
vědci	vědec	k1gMnPc7	vědec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
se	se	k3xPyFc4	se
Bethe	Bethe	k1gInSc1	Bethe
vrátil	vrátit	k5eAaPmAgInS	vrátit
na	na	k7c4	na
Cornell	Cornell	k1gInSc4	Cornell
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
Lambův	Lambův	k2eAgInSc4d1	Lambův
posun	posun	k1gInSc4	posun
ve	v	k7c6	v
spektru	spektrum	k1gNnSc6	spektrum
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
položil	položit	k5eAaPmAgMnS	položit
základ	základ	k1gInSc4	základ
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
moderní	moderní	k2eAgFnSc2d1	moderní
kvantové	kvantový	k2eAgFnSc2d1	kvantová
elektrodynamiky	elektrodynamika	k1gFnSc2	elektrodynamika
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
podepsán	podepsat	k5eAaPmNgInS	podepsat
pod	pod	k7c7	pod
takzvaným	takzvaný	k2eAgInSc7d1	takzvaný
alfa-beta-gama	alfaetaamum	k1gNnPc4	alfa-beta-gamum
modelem	model	k1gInSc7	model
<g/>
,	,	kIx,	,
předpovídajícím	předpovídající	k2eAgInSc7d1	předpovídající
reliktní	reliktní	k2eAgMnPc1d1	reliktní
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
autorů	autor	k1gMnPc2	autor
Ralpha	Ralph	k1gMnSc4	Ralph
Alphera	Alpher	k1gMnSc4	Alpher
<g/>
,	,	kIx,	,
Betheho	Bethe	k1gMnSc4	Bethe
a	a	k8xC	a
Gamowa	Gamowus	k1gMnSc4	Gamowus
<g/>
.	.	kIx.	.
</s>
<s>
Bethe	Bethat	k5eAaPmIp3nS	Bethat
se	se	k3xPyFc4	se
na	na	k7c4	na
práci	práce	k1gFnSc4	práce
ovšem	ovšem	k9	ovšem
nepodílel	podílet	k5eNaImAgMnS	podílet
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
jako	jako	k9	jako
autor	autor	k1gMnSc1	autor
dopsán	dopsán	k2eAgMnSc1d1	dopsán
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jména	jméno	k1gNnPc4	jméno
autorů	autor	k1gMnPc2	autor
tvořila	tvořit	k5eAaImAgFnS	tvořit
zmíněná	zmíněný	k2eAgFnSc1d1	zmíněná
první	první	k4xOgNnPc1	první
tři	tři	k4xCgNnPc1	tři
písmena	písmeno	k1gNnPc1	písmeno
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
z	z	k7c2	z
autorů	autor	k1gMnPc2	autor
Robert	Robert	k1gMnSc1	Robert
Herman	Herman	k1gMnSc1	Herman
byl	být	k5eAaImAgMnS	být
kvůli	kvůli	k7c3	kvůli
nevyhovujícímu	vyhovující	k2eNgNnSc3d1	nevyhovující
jménu	jméno	k1gNnSc3	jméno
vynechán	vynechat	k5eAaPmNgInS	vynechat
zcela	zcela	k6eAd1	zcela
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
Gamowa	Gamow	k1gInSc2	Gamow
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
nechat	nechat	k5eAaPmF	nechat
přejmenovat	přejmenovat	k5eAaPmF	přejmenovat
na	na	k7c4	na
Deltera	Delter	k1gMnSc4	Delter
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
výzkumu	výzkum	k1gInSc2	výzkum
v	v	k7c6	v
astrofyzice	astrofyzika	k1gFnSc6	astrofyzika
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
supernovám	supernova	k1gFnPc3	supernova
<g/>
,	,	kIx,	,
neutronovým	neutronový	k2eAgFnPc3d1	neutronová
hvězdám	hvězda	k1gFnPc3	hvězda
a	a	k8xC	a
černým	černý	k2eAgFnPc3d1	černá
dírám	díra	k1gFnPc3	díra
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgMnS	studovat
gravitačního	gravitační	k2eAgMnSc4d1	gravitační
kolaps	kolaps	k1gInSc4	kolaps
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
napsal	napsat	k5eAaPmAgInS	napsat
důležitý	důležitý	k2eAgInSc1d1	důležitý
článek	článek	k1gInSc1	článek
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
chybějících	chybějící	k2eAgNnPc2d1	chybějící
solárních	solární	k2eAgNnPc2d1	solární
neutrin	neutrino	k1gNnPc2	neutrino
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pomohl	pomoct	k5eAaPmAgInS	pomoct
vymyslel	vymyslet	k5eAaPmAgInS	vymyslet
mechanismus	mechanismus	k1gInSc1	mechanismus
konverze	konverze	k1gFnSc2	konverze
z	z	k7c2	z
elektronového	elektronový	k2eAgInSc2d1	elektronový
na	na	k7c6	na
mionové	mionová	k1gFnSc6	mionová
neutrino	neutrino	k1gNnSc1	neutrino
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
efekt	efekt	k1gInSc1	efekt
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
oscilace	oscilace	k1gFnSc1	oscilace
neutrin	neutrino	k1gNnPc2	neutrino
byla	být	k5eAaImAgFnS	být
skutečně	skutečně	k6eAd1	skutečně
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
jeho	jeho	k3xOp3gInPc7	jeho
95	[number]	k4	95
narozeninami	narozeniny	k1gFnPc7	narozeniny
na	na	k7c4	na
Sudbury	Sudbura	k1gFnPc4	Sudbura
Neutrino	neutrino	k1gNnSc4	neutrino
Observatory	Observator	k1gMnPc7	Observator
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
pomohl	pomoct	k5eAaPmAgMnS	pomoct
Bethe	Bethe	k1gFnSc4	Bethe
Kip	Kip	k1gMnSc3	Kip
Thorneovi	Thorneus	k1gMnSc3	Thorneus
s	s	k7c7	s
výpočty	výpočet	k1gInPc7	výpočet
ohledně	ohledně	k7c2	ohledně
nového	nový	k2eAgInSc2d1	nový
vznikajícího	vznikající	k2eAgInSc2d1	vznikající
projektu	projekt	k1gInSc2	projekt
na	na	k7c4	na
interferometrickou	interferometrický	k2eAgFnSc4d1	interferometrická
detekci	detekce	k1gFnSc4	detekce
gravitačních	gravitační	k2eAgFnPc2d1	gravitační
vln	vlna	k1gFnPc2	vlna
LIGO	liga	k1gFnSc5	liga
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
byla	být	k5eAaImAgFnS	být
práce	práce	k1gFnSc1	práce
vydaná	vydaný	k2eAgFnSc1d1	vydaná
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
týkající	týkající	k2eAgFnSc1d1	týkající
se	se	k3xPyFc4	se
evoluce	evoluce	k1gFnSc1	evoluce
binárních	binární	k2eAgInPc2d1	binární
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
6	[number]	k4	6
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2005	[number]	k4	2005
v	v	k7c6	v
Ithace	Ithaka	k1gFnSc6	Ithaka
na	na	k7c4	na
selhání	selhání	k1gNnSc4	selhání
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
98	[number]	k4	98
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
planetka	planetka	k1gFnSc1	planetka
(	(	kIx(	(
<g/>
30828	[number]	k4	30828
<g/>
)	)	kIx)	)
Bethe	Beth	k1gInSc2	Beth
<g/>
.	.	kIx.	.
</s>
<s>
Lubomír	Lubomír	k1gMnSc1	Lubomír
Sodomka	Sodomka	k1gFnSc1	Sodomka
<g/>
,	,	kIx,	,
Magdalena	Magdalena	k1gFnSc1	Magdalena
Sodomková	Sodomková	k1gFnSc1	Sodomková
<g/>
,	,	kIx,	,
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
SET	set	k1gInSc1	set
OUT	OUT	kA	OUT
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-902058-5-2	[number]	k4	80-902058-5-2
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Hans	hansa	k1gFnPc2	hansa
Bethe	Bethe	k1gNnPc2	Bethe
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Galerie	galerie	k1gFnSc1	galerie
Hans	hansa	k1gFnPc2	hansa
Bethe	Bethe	k1gFnSc1	Bethe
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc4	Commons
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Hans	hansa	k1gFnPc2	hansa
Bethe	Beth	k1gFnSc2	Beth
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Hans	Hans	k1gMnSc1	Hans
Bethe	Beth	k1gFnSc2	Beth
Biografie	biografie	k1gFnSc2	biografie
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Los	los	k1gMnSc1	los
Alamos	Alamos	k1gMnSc1	Alamos
National	National	k1gFnSc1	National
Laboratory	Laborator	k1gInPc1	Laborator
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
ceny	cena	k1gFnPc1	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1967	[number]	k4	1967
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Nekrolog	nekrolog	k1gInSc1	nekrolog
na	na	k7c6	na
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
